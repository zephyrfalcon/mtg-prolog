% Portal Second Age

set('PO2').
set_name('PO2', 'Portal Second Age').
set_release_date('PO2', '1998-06-01').
set_border('PO2', 'black').
set_type('PO2', 'starter').

card_in_set('abyssal nightstalker', 'PO2').
card_original_type('abyssal nightstalker'/'PO2', 'Creature — Nightstalker').
card_original_text('abyssal nightstalker'/'PO2', 'If Abyssal Nightstalker attacks and isn\'t blocked, your opponent chooses and discards a card from his or her hand. (Ignore this effect if your opponent doesn\'t have any cards.)').
card_first_print('abyssal nightstalker', 'PO2').
card_image_name('abyssal nightstalker'/'PO2', 'abyssal nightstalker').
card_uid('abyssal nightstalker'/'PO2', 'PO2:Abyssal Nightstalker:abyssal nightstalker').
card_rarity('abyssal nightstalker'/'PO2', 'Uncommon').
card_artist('abyssal nightstalker'/'PO2', 'Sam Wood').
card_multiverse_id('abyssal nightstalker'/'PO2', '6567').

card_in_set('air elemental', 'PO2').
card_original_type('air elemental'/'PO2', 'Creature — Elemental').
card_original_text('air elemental'/'PO2', 'Flying').
card_image_name('air elemental'/'PO2', 'air elemental').
card_uid('air elemental'/'PO2', 'PO2:Air Elemental:air elemental').
card_rarity('air elemental'/'PO2', 'Uncommon').
card_artist('air elemental'/'PO2', 'Doug Chaffee').
card_flavor_text('air elemental'/'PO2', 'As insubstantial, and as powerful, as the wind that carries it.').
card_multiverse_id('air elemental'/'PO2', '6536').

card_in_set('alaborn cavalier', 'PO2').
card_original_type('alaborn cavalier'/'PO2', 'Creature — Knight').
card_original_text('alaborn cavalier'/'PO2', 'If Alaborn Cavalier attacks, you may choose to tap any one creature. (Tapped creatures can\'t block.)').
card_first_print('alaborn cavalier', 'PO2').
card_image_name('alaborn cavalier'/'PO2', 'alaborn cavalier').
card_uid('alaborn cavalier'/'PO2', 'PO2:Alaborn Cavalier:alaborn cavalier').
card_rarity('alaborn cavalier'/'PO2', 'Uncommon').
card_artist('alaborn cavalier'/'PO2', 'Kev Walker').
card_flavor_text('alaborn cavalier'/'PO2', '\"Course he ran! I wouldn\'t want to stare down that barrel, either!\"\n—Alaborn soldier').
card_multiverse_id('alaborn cavalier'/'PO2', '6506').

card_in_set('alaborn grenadier', 'PO2').
card_original_type('alaborn grenadier'/'PO2', 'Creature — Soldier').
card_original_text('alaborn grenadier'/'PO2', 'Attacking doesn\'t cause Alaborn Grenadier to tap.').
card_first_print('alaborn grenadier', 'PO2').
card_image_name('alaborn grenadier'/'PO2', 'alaborn grenadier').
card_uid('alaborn grenadier'/'PO2', 'PO2:Alaborn Grenadier:alaborn grenadier').
card_rarity('alaborn grenadier'/'PO2', 'Common').
card_artist('alaborn grenadier'/'PO2', 'David A. Cherry').
card_flavor_text('alaborn grenadier'/'PO2', '\"Ever proud, ever vigilant.\"\n—Grenadier motto').
card_multiverse_id('alaborn grenadier'/'PO2', '6508').

card_in_set('alaborn musketeer', 'PO2').
card_original_type('alaborn musketeer'/'PO2', 'Creature — Soldier').
card_original_text('alaborn musketeer'/'PO2', 'Alaborn Musketeer can block creatures with flying.').
card_first_print('alaborn musketeer', 'PO2').
card_image_name('alaborn musketeer'/'PO2', 'alaborn musketeer').
card_uid('alaborn musketeer'/'PO2', 'PO2:Alaborn Musketeer:alaborn musketeer').
card_rarity('alaborn musketeer'/'PO2', 'Common').
card_artist('alaborn musketeer'/'PO2', 'Heather Hudson').
card_flavor_text('alaborn musketeer'/'PO2', 'Muskets gave the Alaborn army an advantage it had long lacked—air defense.').
card_multiverse_id('alaborn musketeer'/'PO2', '6499').

card_in_set('alaborn trooper', 'PO2').
card_original_type('alaborn trooper'/'PO2', 'Creature — Soldier').
card_original_text('alaborn trooper'/'PO2', '').
card_first_print('alaborn trooper', 'PO2').
card_image_name('alaborn trooper'/'PO2', 'alaborn trooper').
card_uid('alaborn trooper'/'PO2', 'PO2:Alaborn Trooper:alaborn trooper').
card_rarity('alaborn trooper'/'PO2', 'Common').
card_artist('alaborn trooper'/'PO2', 'Lubov').
card_flavor_text('alaborn trooper'/'PO2', '\"I dedicate my body to my country\nAnd my life to my King.\"\n—Alaborn Soldier\'s Oath').
card_multiverse_id('alaborn trooper'/'PO2', '6493').

card_in_set('alaborn veteran', 'PO2').
card_original_type('alaborn veteran'/'PO2', 'Creature — Knight').
card_original_text('alaborn veteran'/'PO2', 'On your turn, before you attack, you may tap Alaborn Veteran to give any one creature +2/+2 until the end of the turn.').
card_first_print('alaborn veteran', 'PO2').
card_image_name('alaborn veteran'/'PO2', 'alaborn veteran').
card_uid('alaborn veteran'/'PO2', 'PO2:Alaborn Veteran:alaborn veteran').
card_rarity('alaborn veteran'/'PO2', 'Rare').
card_artist('alaborn veteran'/'PO2', 'Henry Van Der Linde').
card_multiverse_id('alaborn veteran'/'PO2', '6516').

card_in_set('alaborn zealot', 'PO2').
card_original_type('alaborn zealot'/'PO2', 'Creature — Soldier').
card_original_text('alaborn zealot'/'PO2', 'If Alaborn Zealot blocks, destroy both Alaborn Zealot and the creature it blocks. (Destroy both creatures before you deal damage.)').
card_first_print('alaborn zealot', 'PO2').
card_image_name('alaborn zealot'/'PO2', 'alaborn zealot').
card_uid('alaborn zealot'/'PO2', 'PO2:Alaborn Zealot:alaborn zealot').
card_rarity('alaborn zealot'/'PO2', 'Uncommon').
card_artist('alaborn zealot'/'PO2', 'David Horne').
card_multiverse_id('alaborn zealot'/'PO2', '6492').

card_in_set('alluring scent', 'PO2').
card_original_type('alluring scent'/'PO2', 'Sorcery').
card_original_text('alluring scent'/'PO2', 'Choose any one creature. This turn, all creatures able to block it do so.').
card_image_name('alluring scent'/'PO2', 'alluring scent').
card_uid('alluring scent'/'PO2', 'PO2:Alluring Scent:alluring scent').
card_rarity('alluring scent'/'PO2', 'Rare').
card_artist('alluring scent'/'PO2', 'Melissa A. Benson').
card_flavor_text('alluring scent'/'PO2', 'No earthly armor is protection against this sweetness.').
card_multiverse_id('alluring scent'/'PO2', '6639').

card_in_set('ancient craving', 'PO2').
card_original_type('ancient craving'/'PO2', 'Sorcery').
card_original_text('ancient craving'/'PO2', 'Draw 3 cards. You lose 3 life.').
card_first_print('ancient craving', 'PO2').
card_image_name('ancient craving'/'PO2', 'ancient craving').
card_uid('ancient craving'/'PO2', 'PO2:Ancient Craving:ancient craving').
card_rarity('ancient craving'/'PO2', 'Rare').
card_artist('ancient craving'/'PO2', 'Rob Alexander').
card_flavor_text('ancient craving'/'PO2', '\"Knowledge demands sacrifice.\"\n—Tojira, swamp queen').
card_multiverse_id('ancient craving'/'PO2', '6579').

card_in_set('angel of fury', 'PO2').
card_original_type('angel of fury'/'PO2', 'Creature — Angel').
card_original_text('angel of fury'/'PO2', 'Flying\nIf Angel of Fury is put into your graveyard from play, you may choose to shuffle Angel of Fury into your library.').
card_first_print('angel of fury', 'PO2').
card_image_name('angel of fury'/'PO2', 'angel of fury').
card_uid('angel of fury'/'PO2', 'PO2:Angel of Fury:angel of fury').
card_rarity('angel of fury'/'PO2', 'Rare').
card_artist('angel of fury'/'PO2', 'Keith Parkinson').
card_multiverse_id('angel of fury'/'PO2', '6515').

card_in_set('angel of mercy', 'PO2').
card_original_type('angel of mercy'/'PO2', 'Creature — Angel').
card_original_text('angel of mercy'/'PO2', 'Flying\nWhen Angel of Mercy comes into play from your hand, you gain 3 life.').
card_first_print('angel of mercy', 'PO2').
card_image_name('angel of mercy'/'PO2', 'angel of mercy').
card_uid('angel of mercy'/'PO2', 'PO2:Angel of Mercy:angel of mercy').
card_rarity('angel of mercy'/'PO2', 'Uncommon').
card_artist('angel of mercy'/'PO2', 'Melissa A. Benson').
card_multiverse_id('angel of mercy'/'PO2', '6505').

card_in_set('angelic blessing', 'PO2').
card_original_type('angelic blessing'/'PO2', 'Sorcery').
card_original_text('angelic blessing'/'PO2', 'Any one creature gets +3/+3 and gains flying until the end of the turn.').
card_image_name('angelic blessing'/'PO2', 'angelic blessing').
card_uid('angelic blessing'/'PO2', 'PO2:Angelic Blessing:angelic blessing').
card_rarity('angelic blessing'/'PO2', 'Common').
card_artist('angelic blessing'/'PO2', 'Henry Van Der Linde').
card_flavor_text('angelic blessing'/'PO2', 'The man who rose up to fight was not the same man knocked down.').
card_multiverse_id('angelic blessing'/'PO2', '6500').

card_in_set('angelic wall', 'PO2').
card_original_type('angelic wall'/'PO2', 'Creature — Wall').
card_original_text('angelic wall'/'PO2', 'Flying \nWalls can\'t attack.').
card_first_print('angelic wall', 'PO2').
card_image_name('angelic wall'/'PO2', 'angelic wall').
card_uid('angelic wall'/'PO2', 'PO2:Angelic Wall:angelic wall').
card_rarity('angelic wall'/'PO2', 'Common').
card_artist('angelic wall'/'PO2', 'Rebecca Guay').
card_flavor_text('angelic wall'/'PO2', 'Be at peace, and observe its beauty. Be at war, and feel its justice.').
card_multiverse_id('angelic wall'/'PO2', '6498').

card_in_set('apprentice sorcerer', 'PO2').
card_original_type('apprentice sorcerer'/'PO2', 'Creature — Wizard').
card_original_text('apprentice sorcerer'/'PO2', 'On your turn, before you attack, you may tap Apprentice Sorcerer to have it deal 1 damage to any one creature or player.').
card_first_print('apprentice sorcerer', 'PO2').
card_image_name('apprentice sorcerer'/'PO2', 'apprentice sorcerer').
card_uid('apprentice sorcerer'/'PO2', 'PO2:Apprentice Sorcerer:apprentice sorcerer').
card_rarity('apprentice sorcerer'/'PO2', 'Uncommon').
card_artist('apprentice sorcerer'/'PO2', 'Christopher Rush').
card_multiverse_id('apprentice sorcerer'/'PO2', '6535').

card_in_set('archangel', 'PO2').
card_original_type('archangel'/'PO2', 'Creature — Angel').
card_original_text('archangel'/'PO2', 'Flying\nAttacking doesn\'t cause Archangel to tap.').
card_image_name('archangel'/'PO2', 'archangel').
card_uid('archangel'/'PO2', 'PO2:Archangel:archangel').
card_rarity('archangel'/'PO2', 'Rare').
card_artist('archangel'/'PO2', 'rk post').
card_multiverse_id('archangel'/'PO2', '6514').

card_in_set('armageddon', 'PO2').
card_original_type('armageddon'/'PO2', 'Sorcery').
card_original_text('armageddon'/'PO2', 'Destroy all lands. (This includes your lands.)').
card_image_name('armageddon'/'PO2', 'armageddon').
card_uid('armageddon'/'PO2', 'PO2:Armageddon:armageddon').
card_rarity('armageddon'/'PO2', 'Rare').
card_artist('armageddon'/'PO2', 'Rob Alexander').
card_flavor_text('armageddon'/'PO2', 'War destroys more than just the land. War destroys hope.').
card_multiverse_id('armageddon'/'PO2', '6520').

card_in_set('armored galleon', 'PO2').
card_original_type('armored galleon'/'PO2', 'Creature — Ship').
card_original_text('armored galleon'/'PO2', 'Armored Galleon can\'t attack unless the defending player has an island in play.').
card_first_print('armored galleon', 'PO2').
card_image_name('armored galleon'/'PO2', 'armored galleon').
card_uid('armored galleon'/'PO2', 'PO2:Armored Galleon:armored galleon').
card_rarity('armored galleon'/'PO2', 'Uncommon').
card_artist('armored galleon'/'PO2', 'Doug Chaffee').
card_flavor_text('armored galleon'/'PO2', '\"Information equals profits. Our merchants sell it, or our pirates use it.\"\n—Jefan, Talas ship captain').
card_multiverse_id('armored galleon'/'PO2', '6537').

card_in_set('armored griffin', 'PO2').
card_original_type('armored griffin'/'PO2', 'Creature — Griffin').
card_original_text('armored griffin'/'PO2', 'Flying\nAttacking doesn\'t cause Armored Griffin to tap.').
card_first_print('armored griffin', 'PO2').
card_image_name('armored griffin'/'PO2', 'armored griffin').
card_uid('armored griffin'/'PO2', 'PO2:Armored Griffin:armored griffin').
card_rarity('armored griffin'/'PO2', 'Uncommon').
card_artist('armored griffin'/'PO2', 'Bradley Williams').
card_multiverse_id('armored griffin'/'PO2', '6509').

card_in_set('barbtooth wurm', 'PO2').
card_original_type('barbtooth wurm'/'PO2', 'Creature — Wurm').
card_original_text('barbtooth wurm'/'PO2', '').
card_first_print('barbtooth wurm', 'PO2').
card_image_name('barbtooth wurm'/'PO2', 'barbtooth wurm').
card_uid('barbtooth wurm'/'PO2', 'PO2:Barbtooth Wurm:barbtooth wurm').
card_rarity('barbtooth wurm'/'PO2', 'Common').
card_artist('barbtooth wurm'/'PO2', 'Rebecca Guay').
card_flavor_text('barbtooth wurm'/'PO2', 'In its lair lies a carpet of bones.').
card_multiverse_id('barbtooth wurm'/'PO2', '6627').

card_in_set('bargain', 'PO2').
card_original_type('bargain'/'PO2', 'Sorcery').
card_original_text('bargain'/'PO2', 'Your opponent draws a card.\nYou gain 7 life.').
card_first_print('bargain', 'PO2').
card_image_name('bargain'/'PO2', 'bargain').
card_uid('bargain'/'PO2', 'PO2:Bargain:bargain').
card_rarity('bargain'/'PO2', 'Uncommon').
card_artist('bargain'/'PO2', 'Phil Foglio').
card_flavor_text('bargain'/'PO2', 'Bargaining with a goblin is like trading with a child; both believe they already own everything.').
card_multiverse_id('bargain'/'PO2', '6510').

card_in_set('bear cub', 'PO2').
card_original_type('bear cub'/'PO2', 'Creature — Bear').
card_original_text('bear cub'/'PO2', '').
card_first_print('bear cub', 'PO2').
card_image_name('bear cub'/'PO2', 'bear cub').
card_uid('bear cub'/'PO2', 'PO2:Bear Cub:bear cub').
card_rarity('bear cub'/'PO2', 'Common').
card_artist('bear cub'/'PO2', 'Ron Spencer').
card_flavor_text('bear cub'/'PO2', 'Every little cub has its mother\'s teeth to guard it.\n—Elvish saying').
card_multiverse_id('bear cub'/'PO2', '6613').

card_in_set('bee sting', 'PO2').
card_original_type('bee sting'/'PO2', 'Sorcery').
card_original_text('bee sting'/'PO2', 'Bee Sting deals 2 damage to any one creature or player.').
card_image_name('bee sting'/'PO2', 'bee sting').
card_uid('bee sting'/'PO2', 'PO2:Bee Sting:bee sting').
card_rarity('bee sting'/'PO2', 'Uncommon').
card_artist('bee sting'/'PO2', 'Cris Dornaus').
card_flavor_text('bee sting'/'PO2', 'Busy as a bumblebee, blistered as a goblin.\n—Alaborn saying').
card_multiverse_id('bee sting'/'PO2', '6633').

card_in_set('blaze', 'PO2').
card_original_type('blaze'/'PO2', 'Sorcery').
card_original_text('blaze'/'PO2', 'Blaze deals X damage to any one creature or player.').
card_image_name('blaze'/'PO2', 'blaze').
card_uid('blaze'/'PO2', 'PO2:Blaze:blaze').
card_rarity('blaze'/'PO2', 'Uncommon').
card_artist('blaze'/'PO2', 'David A. Cherry').
card_flavor_text('blaze'/'PO2', '\"This isn\'t what I meant by ‘facing the heat of battle!\'\"\n—Jefan, Talas ship captain').
card_multiverse_id('blaze'/'PO2', '6600').

card_in_set('bloodcurdling scream', 'PO2').
card_original_type('bloodcurdling scream'/'PO2', 'Sorcery').
card_original_text('bloodcurdling scream'/'PO2', 'Any one creature gets +X/+0 until the end of the turn.').
card_first_print('bloodcurdling scream', 'PO2').
card_image_name('bloodcurdling scream'/'PO2', 'bloodcurdling scream').
card_uid('bloodcurdling scream'/'PO2', 'PO2:Bloodcurdling Scream:bloodcurdling scream').
card_rarity('bloodcurdling scream'/'PO2', 'Uncommon').
card_artist('bloodcurdling scream'/'PO2', 'Dan Frazier').
card_flavor_text('bloodcurdling scream'/'PO2', '\"I have all the weapons my enemies have—and far deeper rage.\"\n—Tojira, swamp queen').
card_multiverse_id('bloodcurdling scream'/'PO2', '6573').

card_in_set('breath of life', 'PO2').
card_original_type('breath of life'/'PO2', 'Sorcery').
card_original_text('breath of life'/'PO2', 'Take any one creature card from your graveyard and put that creature into play. Treat it as though you just played it from your hand.').
card_image_name('breath of life'/'PO2', 'breath of life').
card_uid('breath of life'/'PO2', 'PO2:Breath of Life:breath of life').
card_rarity('breath of life'/'PO2', 'Common').
card_artist('breath of life'/'PO2', 'Lubov').
card_multiverse_id('breath of life'/'PO2', '6501').

card_in_set('brimstone dragon', 'PO2').
card_original_type('brimstone dragon'/'PO2', 'Creature — Dragon').
card_original_text('brimstone dragon'/'PO2', 'Flying\nBrimstone Dragon is unaffected by summoning sickness.').
card_first_print('brimstone dragon', 'PO2').
card_image_name('brimstone dragon'/'PO2', 'brimstone dragon').
card_uid('brimstone dragon'/'PO2', 'PO2:Brimstone Dragon:brimstone dragon').
card_rarity('brimstone dragon'/'PO2', 'Rare').
card_artist('brimstone dragon'/'PO2', 'David A. Cherry').
card_multiverse_id('brimstone dragon'/'PO2', '6605').

card_in_set('brutal nightstalker', 'PO2').
card_original_type('brutal nightstalker'/'PO2', 'Creature — Nightstalker').
card_original_text('brutal nightstalker'/'PO2', 'When Brutal Nightstalker comes into play from your hand, you may force your opponent to choose and discard a card from his or her hand.').
card_first_print('brutal nightstalker', 'PO2').
card_image_name('brutal nightstalker'/'PO2', 'brutal nightstalker').
card_uid('brutal nightstalker'/'PO2', 'PO2:Brutal Nightstalker:brutal nightstalker').
card_rarity('brutal nightstalker'/'PO2', 'Uncommon').
card_artist('brutal nightstalker'/'PO2', 'Brom').
card_multiverse_id('brutal nightstalker'/'PO2', '6566').

card_in_set('chorus of woe', 'PO2').
card_original_type('chorus of woe'/'PO2', 'Sorcery').
card_original_text('chorus of woe'/'PO2', 'All your creatures get +1/+0 until the end of the turn.').
card_first_print('chorus of woe', 'PO2').
card_image_name('chorus of woe'/'PO2', 'chorus of woe').
card_uid('chorus of woe'/'PO2', 'PO2:Chorus of Woe:chorus of woe').
card_rarity('chorus of woe'/'PO2', 'Common').
card_artist('chorus of woe'/'PO2', 'Randy Gallegos').
card_flavor_text('chorus of woe'/'PO2', 'When nightstalkers sing, nothing in creation sleeps.').
card_multiverse_id('chorus of woe'/'PO2', '6564').

card_in_set('coastal wizard', 'PO2').
card_original_type('coastal wizard'/'PO2', 'Creature — Wizard').
card_original_text('coastal wizard'/'PO2', 'On your turn, before you attack, you may tap Coastal Wizard to return it and any one other creature to their owners\' hands.').
card_first_print('coastal wizard', 'PO2').
card_image_name('coastal wizard'/'PO2', 'coastal wizard').
card_uid('coastal wizard'/'PO2', 'PO2:Coastal Wizard:coastal wizard').
card_rarity('coastal wizard'/'PO2', 'Rare').
card_artist('coastal wizard'/'PO2', 'Edward P. Beard, Jr.').
card_multiverse_id('coastal wizard'/'PO2', '6544').

card_in_set('coercion', 'PO2').
card_original_type('coercion'/'PO2', 'Sorcery').
card_original_text('coercion'/'PO2', 'Look at your opponent\'s hand and choose a card. Your opponent discards that card.').
card_image_name('coercion'/'PO2', 'coercion').
card_uid('coercion'/'PO2', 'PO2:Coercion:coercion').
card_rarity('coercion'/'PO2', 'Uncommon').
card_artist('coercion'/'PO2', 'Jeffrey R. Busch').
card_flavor_text('coercion'/'PO2', '\"Human tenderness is simply weakness in a pretty package.\"\n—Tojira, swamp queen').
card_multiverse_id('coercion'/'PO2', '6570').

card_in_set('cruel edict', 'PO2').
card_original_type('cruel edict'/'PO2', 'Sorcery').
card_original_text('cruel edict'/'PO2', 'Your opponent chooses one of his or her creatures. Destroy that creature.').
card_first_print('cruel edict', 'PO2').
card_image_name('cruel edict'/'PO2', 'cruel edict').
card_uid('cruel edict'/'PO2', 'PO2:Cruel Edict:cruel edict').
card_rarity('cruel edict'/'PO2', 'Common').
card_artist('cruel edict'/'PO2', 'Jeffrey R. Busch').
card_flavor_text('cruel edict'/'PO2', '\"Such a fragile organ, the brain.\"\n—Tojira, swamp queen').
card_multiverse_id('cruel edict'/'PO2', '6561').

card_in_set('cunning giant', 'PO2').
card_original_type('cunning giant'/'PO2', 'Creature — Giant').
card_original_text('cunning giant'/'PO2', 'If Cunning Giant attacks and isn\'t blocked, you may choose to have it deal its damage to any one of your opponent\'s creatures instead of to him or her.').
card_first_print('cunning giant', 'PO2').
card_image_name('cunning giant'/'PO2', 'cunning giant').
card_uid('cunning giant'/'PO2', 'PO2:Cunning Giant:cunning giant').
card_rarity('cunning giant'/'PO2', 'Rare').
card_artist('cunning giant'/'PO2', 'Jeffrey R. Busch').
card_multiverse_id('cunning giant'/'PO2', '6606').

card_in_set('dakmor bat', 'PO2').
card_original_type('dakmor bat'/'PO2', 'Creature — Bat').
card_original_text('dakmor bat'/'PO2', 'Flying').
card_first_print('dakmor bat', 'PO2').
card_image_name('dakmor bat'/'PO2', 'dakmor bat').
card_uid('dakmor bat'/'PO2', 'PO2:Dakmor Bat:dakmor bat').
card_rarity('dakmor bat'/'PO2', 'Common').
card_artist('dakmor bat'/'PO2', 'Una Fricker').
card_flavor_text('dakmor bat'/'PO2', 'The bat thrives on what underestimates it.').
card_multiverse_id('dakmor bat'/'PO2', '6553').

card_in_set('dakmor plague', 'PO2').
card_original_type('dakmor plague'/'PO2', 'Sorcery').
card_original_text('dakmor plague'/'PO2', 'Dakmor Plague deals 3 damage to each creature and player. (This includes your creatures and you.)').
card_first_print('dakmor plague', 'PO2').
card_image_name('dakmor plague'/'PO2', 'dakmor plague').
card_uid('dakmor plague'/'PO2', 'PO2:Dakmor Plague:dakmor plague').
card_rarity('dakmor plague'/'PO2', 'Uncommon').
card_artist('dakmor plague'/'PO2', 'Jeff Miracola').
card_flavor_text('dakmor plague'/'PO2', 'The tiniest cough can be deadlier than the fiercest dragon.').
card_multiverse_id('dakmor plague'/'PO2', '6571').

card_in_set('dakmor scorpion', 'PO2').
card_original_type('dakmor scorpion'/'PO2', 'Creature — Scorpion').
card_original_text('dakmor scorpion'/'PO2', '').
card_first_print('dakmor scorpion', 'PO2').
card_image_name('dakmor scorpion'/'PO2', 'dakmor scorpion').
card_uid('dakmor scorpion'/'PO2', 'PO2:Dakmor Scorpion:dakmor scorpion').
card_rarity('dakmor scorpion'/'PO2', 'Common').
card_artist('dakmor scorpion'/'PO2', 'Randy Gallegos').
card_flavor_text('dakmor scorpion'/'PO2', 'A scorpion this big you won\'t find curled up in your boot. Maybe around your boot, but not in it.').
card_multiverse_id('dakmor scorpion'/'PO2', '6552').

card_in_set('dakmor sorceress', 'PO2').
card_original_type('dakmor sorceress'/'PO2', 'Creature — Wizard').
card_original_text('dakmor sorceress'/'PO2', 'Dakmor Sorceress has power equal to the number of swamp cards you have in play. (This includes both tapped and untapped swamp cards.)').
card_first_print('dakmor sorceress', 'PO2').
card_image_name('dakmor sorceress'/'PO2', 'dakmor sorceress').
card_uid('dakmor sorceress'/'PO2', 'PO2:Dakmor Sorceress:dakmor sorceress').
card_rarity('dakmor sorceress'/'PO2', 'Rare').
card_artist('dakmor sorceress'/'PO2', 'Matthew D. Wilson').
card_multiverse_id('dakmor sorceress'/'PO2', '6576').

card_in_set('dark offering', 'PO2').
card_original_type('dark offering'/'PO2', 'Sorcery').
card_original_text('dark offering'/'PO2', 'Destroy any one creature that isn\'t black. You gain 3 life.').
card_first_print('dark offering', 'PO2').
card_image_name('dark offering'/'PO2', 'dark offering').
card_uid('dark offering'/'PO2', 'PO2:Dark Offering:dark offering').
card_rarity('dark offering'/'PO2', 'Uncommon').
card_artist('dark offering'/'PO2', 'Edward P. Beard, Jr.').
card_flavor_text('dark offering'/'PO2', '\"Our greatest hope has become our enemy\'s greatest triumph.\"\n—Restela, Alaborn marshal').
card_multiverse_id('dark offering'/'PO2', '6569').

card_in_set('deathcoil wurm', 'PO2').
card_original_type('deathcoil wurm'/'PO2', 'Creature — Wurm').
card_original_text('deathcoil wurm'/'PO2', 'If Deathcoil Wurm attacks and is blocked, you may choose to have it deal its damage to the defending player instead of to the creatures blocking it.').
card_first_print('deathcoil wurm', 'PO2').
card_image_name('deathcoil wurm'/'PO2', 'deathcoil wurm').
card_uid('deathcoil wurm'/'PO2', 'PO2:Deathcoil Wurm:deathcoil wurm').
card_rarity('deathcoil wurm'/'PO2', 'Rare').
card_artist('deathcoil wurm'/'PO2', 'Rebecca Guay').
card_multiverse_id('deathcoil wurm'/'PO2', '6637').

card_in_set('deep wood', 'PO2').
card_original_type('deep wood'/'PO2', 'Sorcery').
card_original_text('deep wood'/'PO2', 'Play Deep Wood only after you\'re attacked, before you declare blockers.\nThis turn, all damage dealt to you by attacking creatures is reduced to 0.').
card_image_name('deep wood'/'PO2', 'deep wood').
card_uid('deep wood'/'PO2', 'PO2:Deep Wood:deep wood').
card_rarity('deep wood'/'PO2', 'Uncommon').
card_artist('deep wood'/'PO2', 'Jeff Miracola').
card_multiverse_id('deep wood'/'PO2', '6631').

card_in_set('déjà vu', 'PO2').
card_original_type('déjà vu'/'PO2', 'Sorcery').
card_original_text('déjà vu'/'PO2', 'Return any one sorcery card from your graveyard to your hand.').
card_image_name('déjà vu'/'PO2', 'deja vu').
card_uid('déjà vu'/'PO2', 'PO2:Déjà Vu:deja vu').
card_rarity('déjà vu'/'PO2', 'Common').
card_artist('déjà vu'/'PO2', 'David Horne').
card_flavor_text('déjà vu'/'PO2', 'History is the study of repetition.').
card_multiverse_id('déjà vu'/'PO2', '6528').

card_in_set('denizen of the deep', 'PO2').
card_original_type('denizen of the deep'/'PO2', 'Creature — Serpent').
card_original_text('denizen of the deep'/'PO2', 'When Denizen of the Deep comes into play from your hand, return all your other creatures from play to your hand.').
card_first_print('denizen of the deep', 'PO2').
card_image_name('denizen of the deep'/'PO2', 'denizen of the deep').
card_uid('denizen of the deep'/'PO2', 'PO2:Denizen of the Deep:denizen of the deep').
card_rarity('denizen of the deep'/'PO2', 'Rare').
card_artist('denizen of the deep'/'PO2', 'Anson Maddocks').
card_multiverse_id('denizen of the deep'/'PO2', '6546').

card_in_set('earthquake', 'PO2').
card_original_type('earthquake'/'PO2', 'Sorcery').
card_original_text('earthquake'/'PO2', 'Earthquake deals X damage to each player and each creature without flying. (This includes you and your creatures without flying.)').
card_image_name('earthquake'/'PO2', 'earthquake').
card_uid('earthquake'/'PO2', 'PO2:Earthquake:earthquake').
card_rarity('earthquake'/'PO2', 'Rare').
card_artist('earthquake'/'PO2', 'Jeffrey R. Busch').
card_multiverse_id('earthquake'/'PO2', '6609').

card_in_set('exhaustion', 'PO2').
card_original_type('exhaustion'/'PO2', 'Sorcery').
card_original_text('exhaustion'/'PO2', 'At the beginning of your opponent\'s next turn, he or she skips untapping creatures and lands.').
card_image_name('exhaustion'/'PO2', 'exhaustion').
card_uid('exhaustion'/'PO2', 'PO2:Exhaustion:exhaustion').
card_rarity('exhaustion'/'PO2', 'Rare').
card_artist('exhaustion'/'PO2', 'Kaja Foglio').
card_multiverse_id('exhaustion'/'PO2', '6549').

card_in_set('extinguish', 'PO2').
card_original_type('extinguish'/'PO2', 'Sorcery').
card_original_text('extinguish'/'PO2', 'Play Extinguish only in response to another player playing a sorcery. That sorcery has no effect, and that player puts it into his or her graveyard.').
card_first_print('extinguish', 'PO2').
card_image_name('extinguish'/'PO2', 'extinguish').
card_uid('extinguish'/'PO2', 'PO2:Extinguish:extinguish').
card_rarity('extinguish'/'PO2', 'Common').
card_artist('extinguish'/'PO2', 'Douglas Shuler').
card_multiverse_id('extinguish'/'PO2', '6534').

card_in_set('eye spy', 'PO2').
card_original_type('eye spy'/'PO2', 'Sorcery').
card_original_text('eye spy'/'PO2', 'Look at the top card of any player\'s library. You may choose to put that card back on top of that library or into that player\'s graveyard.').
card_first_print('eye spy', 'PO2').
card_image_name('eye spy'/'PO2', 'eye spy').
card_uid('eye spy'/'PO2', 'PO2:Eye Spy:eye spy').
card_rarity('eye spy'/'PO2', 'Uncommon').
card_artist('eye spy'/'PO2', 'DiTerlizzi').
card_multiverse_id('eye spy'/'PO2', '6538').

card_in_set('false summoning', 'PO2').
card_original_type('false summoning'/'PO2', 'Sorcery').
card_original_text('false summoning'/'PO2', 'Play False Summoning only in response to another player playing a creature. That creature card has no effect, and that player puts it into his or her graveyard.').
card_first_print('false summoning', 'PO2').
card_image_name('false summoning'/'PO2', 'false summoning').
card_uid('false summoning'/'PO2', 'PO2:False Summoning:false summoning').
card_rarity('false summoning'/'PO2', 'Common').
card_artist('false summoning'/'PO2', 'DiTerlizzi').
card_multiverse_id('false summoning'/'PO2', '6533').

card_in_set('festival of trokin', 'PO2').
card_original_type('festival of trokin'/'PO2', 'Sorcery').
card_original_text('festival of trokin'/'PO2', 'For each creature you have in play, you gain 2 life.').
card_first_print('festival of trokin', 'PO2').
card_image_name('festival of trokin'/'PO2', 'festival of trokin').
card_uid('festival of trokin'/'PO2', 'PO2:Festival of Trokin:festival of trokin').
card_rarity('festival of trokin'/'PO2', 'Common').
card_artist('festival of trokin'/'PO2', 'Jeffrey R. Busch').
card_flavor_text('festival of trokin'/'PO2', 'Everyone loves a good sale.').
card_multiverse_id('festival of trokin'/'PO2', '6504').

card_in_set('forest', 'PO2').
card_original_type('forest'/'PO2', 'Land').
card_original_text('forest'/'PO2', 'G').
card_image_name('forest'/'PO2', 'forest1').
card_uid('forest'/'PO2', 'PO2:Forest:forest1').
card_rarity('forest'/'PO2', 'Basic Land').
card_artist('forest'/'PO2', 'Quinton Hoover').
card_multiverse_id('forest'/'PO2', '8409').

card_in_set('forest', 'PO2').
card_original_type('forest'/'PO2', 'Land').
card_original_text('forest'/'PO2', 'G').
card_image_name('forest'/'PO2', 'forest2').
card_uid('forest'/'PO2', 'PO2:Forest:forest2').
card_rarity('forest'/'PO2', 'Basic Land').
card_artist('forest'/'PO2', 'Quinton Hoover').
card_multiverse_id('forest'/'PO2', '8410').

card_in_set('forest', 'PO2').
card_original_type('forest'/'PO2', 'Land').
card_original_text('forest'/'PO2', 'G').
card_image_name('forest'/'PO2', 'forest3').
card_uid('forest'/'PO2', 'PO2:Forest:forest3').
card_rarity('forest'/'PO2', 'Basic Land').
card_artist('forest'/'PO2', 'Quinton Hoover').
card_multiverse_id('forest'/'PO2', '8408').

card_in_set('foul spirit', 'PO2').
card_original_type('foul spirit'/'PO2', 'Creature — Spirit').
card_original_text('foul spirit'/'PO2', 'Flying\nWhen Foul Spirit comes into play from your hand, destroy one of your lands.').
card_first_print('foul spirit', 'PO2').
card_image_name('foul spirit'/'PO2', 'foul spirit').
card_uid('foul spirit'/'PO2', 'PO2:Foul Spirit:foul spirit').
card_rarity('foul spirit'/'PO2', 'Uncommon').
card_artist('foul spirit'/'PO2', 'rk post').
card_multiverse_id('foul spirit'/'PO2', '6568').

card_in_set('goblin cavaliers', 'PO2').
card_original_type('goblin cavaliers'/'PO2', 'Creature — Goblins').
card_original_text('goblin cavaliers'/'PO2', '').
card_first_print('goblin cavaliers', 'PO2').
card_image_name('goblin cavaliers'/'PO2', 'goblin cavaliers').
card_uid('goblin cavaliers'/'PO2', 'PO2:Goblin Cavaliers:goblin cavaliers').
card_rarity('goblin cavaliers'/'PO2', 'Common').
card_artist('goblin cavaliers'/'PO2', 'DiTerlizzi').
card_flavor_text('goblin cavaliers'/'PO2', 'They get along so well with their goats because they\'re practically goats themselves.').
card_multiverse_id('goblin cavaliers'/'PO2', '6587').

card_in_set('goblin firestarter', 'PO2').
card_original_type('goblin firestarter'/'PO2', 'Creature — Goblin').
card_original_text('goblin firestarter'/'PO2', 'On your turn, before you attack, you may destroy Goblin Firestarter to have it deal 1 damage to any one creature or player.').
card_first_print('goblin firestarter', 'PO2').
card_image_name('goblin firestarter'/'PO2', 'goblin firestarter').
card_uid('goblin firestarter'/'PO2', 'PO2:Goblin Firestarter:goblin firestarter').
card_rarity('goblin firestarter'/'PO2', 'Uncommon').
card_artist('goblin firestarter'/'PO2', 'Keith Parkinson').
card_multiverse_id('goblin firestarter'/'PO2', '6598').

card_in_set('goblin general', 'PO2').
card_original_type('goblin general'/'PO2', 'Creature — Goblin').
card_original_text('goblin general'/'PO2', 'When Goblin General attacks, all your Goblins get +1/+1 until the end of the turn.').
card_first_print('goblin general', 'PO2').
card_image_name('goblin general'/'PO2', 'goblin general').
card_uid('goblin general'/'PO2', 'PO2:Goblin General:goblin general').
card_rarity('goblin general'/'PO2', 'Rare').
card_artist('goblin general'/'PO2', 'Keith Parkinson').
card_flavor_text('goblin general'/'PO2', 'Lead, follow, or run around like crazy.').
card_multiverse_id('goblin general'/'PO2', '6604').

card_in_set('goblin glider', 'PO2').
card_original_type('goblin glider'/'PO2', 'Creature — Goblin').
card_original_text('goblin glider'/'PO2', 'Flying\nGoblin Glider can\'t block.').
card_first_print('goblin glider', 'PO2').
card_image_name('goblin glider'/'PO2', 'goblin glider').
card_uid('goblin glider'/'PO2', 'PO2:Goblin Glider:goblin glider').
card_rarity('goblin glider'/'PO2', 'Common').
card_artist('goblin glider'/'PO2', 'Pete Venters').
card_flavor_text('goblin glider'/'PO2', 'The goblins call the gliders \"death from above.\" Everyone else calls them \"clods in the clouds.\"').
card_multiverse_id('goblin glider'/'PO2', '6584').

card_in_set('goblin lore', 'PO2').
card_original_type('goblin lore'/'PO2', 'Sorcery').
card_original_text('goblin lore'/'PO2', 'Draw four cards and put them into your hand. Then discard three cards at random from your hand.').
card_first_print('goblin lore', 'PO2').
card_image_name('goblin lore'/'PO2', 'goblin lore').
card_uid('goblin lore'/'PO2', 'PO2:Goblin Lore:goblin lore').
card_rarity('goblin lore'/'PO2', 'Uncommon').
card_artist('goblin lore'/'PO2', 'D. Alexander Gregory').
card_flavor_text('goblin lore'/'PO2', '\"I done forgot more than you\'ll ever know, pipsqueak.\"\n\"Yeah—that\'s your problem.\"').
card_multiverse_id('goblin lore'/'PO2', '6602').

card_in_set('goblin matron', 'PO2').
card_original_type('goblin matron'/'PO2', 'Creature — Goblin').
card_original_text('goblin matron'/'PO2', 'When Goblin Matron comes into play from your hand, search your library for a Goblin card and put that card into your hand. Shuffle your library afterwards.').
card_first_print('goblin matron', 'PO2').
card_image_name('goblin matron'/'PO2', 'goblin matron').
card_uid('goblin matron'/'PO2', 'PO2:Goblin Matron:goblin matron').
card_rarity('goblin matron'/'PO2', 'Uncommon').
card_artist('goblin matron'/'PO2', 'Daniel Gelon').
card_multiverse_id('goblin matron'/'PO2', '6596').

card_in_set('goblin mountaineer', 'PO2').
card_original_type('goblin mountaineer'/'PO2', 'Creature — Goblin').
card_original_text('goblin mountaineer'/'PO2', 'Mountainwalk (If defending player has any mountains in play, Goblin Mountaineer can\'t be blocked.)').
card_first_print('goblin mountaineer', 'PO2').
card_image_name('goblin mountaineer'/'PO2', 'goblin mountaineer').
card_uid('goblin mountaineer'/'PO2', 'PO2:Goblin Mountaineer:goblin mountaineer').
card_rarity('goblin mountaineer'/'PO2', 'Common').
card_artist('goblin mountaineer'/'PO2', 'DiTerlizzi').
card_flavor_text('goblin mountaineer'/'PO2', 'Goblin mountaineer, barely keeps his family fed.').
card_multiverse_id('goblin mountaineer'/'PO2', '6585').

card_in_set('goblin piker', 'PO2').
card_original_type('goblin piker'/'PO2', 'Creature — Goblin').
card_original_text('goblin piker'/'PO2', '').
card_first_print('goblin piker', 'PO2').
card_image_name('goblin piker'/'PO2', 'goblin piker').
card_uid('goblin piker'/'PO2', 'PO2:Goblin Piker:goblin piker').
card_rarity('goblin piker'/'PO2', 'Common').
card_artist('goblin piker'/'PO2', 'DiTerlizzi').
card_flavor_text('goblin piker'/'PO2', 'Once he\'d worked out which end of the thing was sharp, he was promoted to guard duty.').
card_multiverse_id('goblin piker'/'PO2', '6582').

card_in_set('goblin raider', 'PO2').
card_original_type('goblin raider'/'PO2', 'Creature — Goblin').
card_original_text('goblin raider'/'PO2', 'Goblin Raider can\'t block.').
card_first_print('goblin raider', 'PO2').
card_image_name('goblin raider'/'PO2', 'goblin raider').
card_uid('goblin raider'/'PO2', 'PO2:Goblin Raider:goblin raider').
card_rarity('goblin raider'/'PO2', 'Common').
card_artist('goblin raider'/'PO2', 'Matt Stawicki').
card_flavor_text('goblin raider'/'PO2', '\"I seen lots o\' fights. To win ya gotta bash hard and not get bashed back.\"\n—Goblin grunt').
card_multiverse_id('goblin raider'/'PO2', '6583').

card_in_set('goblin war cry', 'PO2').
card_original_type('goblin war cry'/'PO2', 'Sorcery').
card_original_text('goblin war cry'/'PO2', 'Your opponent chooses one of his or her creatures. Only that creature can block this turn.').
card_first_print('goblin war cry', 'PO2').
card_image_name('goblin war cry'/'PO2', 'goblin war cry').
card_uid('goblin war cry'/'PO2', 'PO2:Goblin War Cry:goblin war cry').
card_rarity('goblin war cry'/'PO2', 'Uncommon').
card_artist('goblin war cry'/'PO2', 'Michael Weaver').
card_flavor_text('goblin war cry'/'PO2', '\"Goblins cry, soldiers fly.\nStones fly, soldiers die!\"').
card_multiverse_id('goblin war cry'/'PO2', '6603').

card_in_set('goblin war strike', 'PO2').
card_original_type('goblin war strike'/'PO2', 'Sorcery').
card_original_text('goblin war strike'/'PO2', 'Goblin War Strike deals to your opponent damage equal to the number of Goblin cards you have in play. (This includes both tapped and untapped Goblin cards.)').
card_first_print('goblin war strike', 'PO2').
card_image_name('goblin war strike'/'PO2', 'goblin war strike').
card_uid('goblin war strike'/'PO2', 'PO2:Goblin War Strike:goblin war strike').
card_rarity('goblin war strike'/'PO2', 'Common').
card_artist('goblin war strike'/'PO2', 'Michael Weaver').
card_multiverse_id('goblin war strike'/'PO2', '6594').

card_in_set('golden bear', 'PO2').
card_original_type('golden bear'/'PO2', 'Creature — Bear').
card_original_text('golden bear'/'PO2', '').
card_first_print('golden bear', 'PO2').
card_image_name('golden bear'/'PO2', 'golden bear').
card_uid('golden bear'/'PO2', 'PO2:Golden Bear:golden bear').
card_rarity('golden bear'/'PO2', 'Common').
card_artist('golden bear'/'PO2', 'Una Fricker').
card_flavor_text('golden bear'/'PO2', 'The elvish scout was scrupulous about the truth. She told the traders that gold lay nearby.').
card_multiverse_id('golden bear'/'PO2', '6616').

card_in_set('hand of death', 'PO2').
card_original_type('hand of death'/'PO2', 'Sorcery').
card_original_text('hand of death'/'PO2', 'Destroy any one creature that isn\'t black.').
card_image_name('hand of death'/'PO2', 'hand of death').
card_uid('hand of death'/'PO2', 'PO2:Hand of Death:hand of death').
card_rarity('hand of death'/'PO2', 'Common').
card_artist('hand of death'/'PO2', 'Heather Hudson').
card_flavor_text('hand of death'/'PO2', 'The touch of death is never gentle.').
card_multiverse_id('hand of death'/'PO2', '6563').

card_in_set('harmony of nature', 'PO2').
card_original_type('harmony of nature'/'PO2', 'Sorcery').
card_original_text('harmony of nature'/'PO2', 'Tap any number of your creatures. You gain 4 life for each creature tapped this way. (Tapped creatures can\'t block.)').
card_first_print('harmony of nature', 'PO2').
card_image_name('harmony of nature'/'PO2', 'harmony of nature').
card_uid('harmony of nature'/'PO2', 'PO2:Harmony of Nature:harmony of nature').
card_rarity('harmony of nature'/'PO2', 'Uncommon').
card_artist('harmony of nature'/'PO2', 'Kaja Foglio').
card_multiverse_id('harmony of nature'/'PO2', '6632').

card_in_set('hidden horror', 'PO2').
card_original_type('hidden horror'/'PO2', 'Creature — Horror').
card_original_text('hidden horror'/'PO2', 'When Hidden Horror comes into play from your hand, choose and discard a creature card from your hand or destroy Hidden Horror.').
card_image_name('hidden horror'/'PO2', 'hidden horror').
card_uid('hidden horror'/'PO2', 'PO2:Hidden Horror:hidden horror').
card_rarity('hidden horror'/'PO2', 'Rare').
card_artist('hidden horror'/'PO2', 'Brom').
card_multiverse_id('hidden horror'/'PO2', '6574').

card_in_set('hurricane', 'PO2').
card_original_type('hurricane'/'PO2', 'Sorcery').
card_original_text('hurricane'/'PO2', 'Hurricane deals X damage to each player and each creature with flying. (This includes you and your creatures with flying.)').
card_image_name('hurricane'/'PO2', 'hurricane').
card_uid('hurricane'/'PO2', 'PO2:Hurricane:hurricane').
card_rarity('hurricane'/'PO2', 'Rare').
card_artist('hurricane'/'PO2', 'Rob Alexander').
card_multiverse_id('hurricane'/'PO2', '6640').

card_in_set('ironhoof ox', 'PO2').
card_original_type('ironhoof ox'/'PO2', 'Creature — Ox').
card_original_text('ironhoof ox'/'PO2', 'Ironhoof Ox can\'t be blocked by more than one creature.').
card_first_print('ironhoof ox', 'PO2').
card_image_name('ironhoof ox'/'PO2', 'ironhoof ox').
card_uid('ironhoof ox'/'PO2', 'PO2:Ironhoof Ox:ironhoof ox').
card_rarity('ironhoof ox'/'PO2', 'Uncommon').
card_artist('ironhoof ox'/'PO2', 'Una Fricker').
card_flavor_text('ironhoof ox'/'PO2', 'The good news is it\'s vegetarian. The bad news is it just doesn\'t like you.').
card_multiverse_id('ironhoof ox'/'PO2', '6628').

card_in_set('island', 'PO2').
card_original_type('island'/'PO2', 'Land').
card_original_text('island'/'PO2', 'U').
card_image_name('island'/'PO2', 'island1').
card_uid('island'/'PO2', 'PO2:Island:island1').
card_rarity('island'/'PO2', 'Basic Land').
card_artist('island'/'PO2', 'John Avon').
card_multiverse_id('island'/'PO2', '8387').

card_in_set('island', 'PO2').
card_original_type('island'/'PO2', 'Land').
card_original_text('island'/'PO2', 'U').
card_image_name('island'/'PO2', 'island2').
card_uid('island'/'PO2', 'PO2:Island:island2').
card_rarity('island'/'PO2', 'Basic Land').
card_artist('island'/'PO2', 'John Avon').
card_multiverse_id('island'/'PO2', '8390').

card_in_set('island', 'PO2').
card_original_type('island'/'PO2', 'Land').
card_original_text('island'/'PO2', 'U').
card_image_name('island'/'PO2', 'island3').
card_uid('island'/'PO2', 'PO2:Island:island3').
card_rarity('island'/'PO2', 'Basic Land').
card_artist('island'/'PO2', 'John Avon').
card_multiverse_id('island'/'PO2', '8391').

card_in_set('jagged lightning', 'PO2').
card_original_type('jagged lightning'/'PO2', 'Sorcery').
card_original_text('jagged lightning'/'PO2', 'Choose any two creatures. Jagged Lightning deals 3 damage to each of them. (You can\'t play Jagged Lightning unless you can choose two creatures to damage.)').
card_first_print('jagged lightning', 'PO2').
card_image_name('jagged lightning'/'PO2', 'jagged lightning').
card_uid('jagged lightning'/'PO2', 'PO2:Jagged Lightning:jagged lightning').
card_rarity('jagged lightning'/'PO2', 'Uncommon').
card_artist('jagged lightning'/'PO2', 'Michael Weaver').
card_multiverse_id('jagged lightning'/'PO2', '6601').

card_in_set('just fate', 'PO2').
card_original_type('just fate'/'PO2', 'Sorcery').
card_original_text('just fate'/'PO2', 'Play Just Fate only after you\'re attacked, before you declare blockers.\nDestroy any one attacking creature.').
card_first_print('just fate', 'PO2').
card_image_name('just fate'/'PO2', 'just fate').
card_uid('just fate'/'PO2', 'PO2:Just Fate:just fate').
card_rarity('just fate'/'PO2', 'Rare').
card_artist('just fate'/'PO2', 'Bradley Williams').
card_multiverse_id('just fate'/'PO2', '6519').

card_in_set('kiss of death', 'PO2').
card_original_type('kiss of death'/'PO2', 'Sorcery').
card_original_text('kiss of death'/'PO2', 'Kiss of Death deals 4 damage to your opponent. You gain 4 life.').
card_first_print('kiss of death', 'PO2').
card_image_name('kiss of death'/'PO2', 'kiss of death').
card_uid('kiss of death'/'PO2', 'PO2:Kiss of Death:kiss of death').
card_rarity('kiss of death'/'PO2', 'Uncommon').
card_artist('kiss of death'/'PO2', 'Melissa A. Benson').
card_flavor_text('kiss of death'/'PO2', '\"I\'d sooner lock lips with a viper. At least I might walk away from that.\"\n—Elvish scout').
card_multiverse_id('kiss of death'/'PO2', '6572').

card_in_set('lava axe', 'PO2').
card_original_type('lava axe'/'PO2', 'Sorcery').
card_original_text('lava axe'/'PO2', 'Lava Axe deals 5 damage to your opponent.').
card_image_name('lava axe'/'PO2', 'lava axe').
card_uid('lava axe'/'PO2', 'PO2:Lava Axe:lava axe').
card_rarity('lava axe'/'PO2', 'Common').
card_artist('lava axe'/'PO2', 'Edward P. Beard, Jr.').
card_flavor_text('lava axe'/'PO2', 'Meant to cut through the body and burn straight to the soul.').
card_multiverse_id('lava axe'/'PO2', '6591').

card_in_set('lone wolf', 'PO2').
card_original_type('lone wolf'/'PO2', 'Creature — Wolf').
card_original_text('lone wolf'/'PO2', 'If Lone Wolf attacks and is blocked, you may choose to have it deal its damage to the defending player instead of to the creatures blocking it.').
card_first_print('lone wolf', 'PO2').
card_image_name('lone wolf'/'PO2', 'lone wolf').
card_uid('lone wolf'/'PO2', 'PO2:Lone Wolf:lone wolf').
card_rarity('lone wolf'/'PO2', 'Uncommon').
card_artist('lone wolf'/'PO2', 'Michael Weaver').
card_multiverse_id('lone wolf'/'PO2', '6614').

card_in_set('lurking nightstalker', 'PO2').
card_original_type('lurking nightstalker'/'PO2', 'Creature — Nightstalker').
card_original_text('lurking nightstalker'/'PO2', 'If Lurking Nightstalker attacks, it gets +2/+0 until the end of the turn.').
card_first_print('lurking nightstalker', 'PO2').
card_image_name('lurking nightstalker'/'PO2', 'lurking nightstalker').
card_uid('lurking nightstalker'/'PO2', 'PO2:Lurking Nightstalker:lurking nightstalker').
card_rarity('lurking nightstalker'/'PO2', 'Common').
card_artist('lurking nightstalker'/'PO2', 'Kev Walker').
card_flavor_text('lurking nightstalker'/'PO2', 'The shadows know.').
card_multiverse_id('lurking nightstalker'/'PO2', '6557').

card_in_set('lynx', 'PO2').
card_original_type('lynx'/'PO2', 'Creature — Cat').
card_original_text('lynx'/'PO2', 'Forestwalk (If defending player has any forests in play, Lynx can\'t be blocked.)').
card_first_print('lynx', 'PO2').
card_image_name('lynx'/'PO2', 'lynx').
card_uid('lynx'/'PO2', 'PO2:Lynx:lynx').
card_rarity('lynx'/'PO2', 'Common').
card_artist('lynx'/'PO2', 'Rebecca Guay').
card_flavor_text('lynx'/'PO2', 'Rarely seen, hardly heard, never caught.').
card_multiverse_id('lynx'/'PO2', '6611').

card_in_set('magma giant', 'PO2').
card_original_type('magma giant'/'PO2', 'Creature — Giant').
card_original_text('magma giant'/'PO2', 'When Magma Giant comes into play from your hand, it deals 2 damage to each creature and player. (This includes you and your creatures, including Magma Giant.)').
card_first_print('magma giant', 'PO2').
card_image_name('magma giant'/'PO2', 'magma giant').
card_uid('magma giant'/'PO2', 'PO2:Magma Giant:magma giant').
card_rarity('magma giant'/'PO2', 'Rare').
card_artist('magma giant'/'PO2', 'Michael Weaver').
card_multiverse_id('magma giant'/'PO2', '6607').

card_in_set('mind rot', 'PO2').
card_original_type('mind rot'/'PO2', 'Sorcery').
card_original_text('mind rot'/'PO2', 'Your opponent chooses and discards two cards from his or her hand. (If your opponent has only one card, he or she discards it.)').
card_image_name('mind rot'/'PO2', 'mind rot').
card_uid('mind rot'/'PO2', 'PO2:Mind Rot:mind rot').
card_rarity('mind rot'/'PO2', 'Common').
card_artist('mind rot'/'PO2', 'Douglas Shuler').
card_multiverse_id('mind rot'/'PO2', '6562').

card_in_set('moaning spirit', 'PO2').
card_original_type('moaning spirit'/'PO2', 'Creature — Spirit').
card_original_text('moaning spirit'/'PO2', 'Flying').
card_first_print('moaning spirit', 'PO2').
card_image_name('moaning spirit'/'PO2', 'moaning spirit').
card_uid('moaning spirit'/'PO2', 'PO2:Moaning Spirit:moaning spirit').
card_rarity('moaning spirit'/'PO2', 'Common').
card_artist('moaning spirit'/'PO2', 'Rebecca Guay').
card_flavor_text('moaning spirit'/'PO2', 'The dead sing their own lullabies.').
card_multiverse_id('moaning spirit'/'PO2', '6554').

card_in_set('monstrous growth', 'PO2').
card_original_type('monstrous growth'/'PO2', 'Sorcery').
card_original_text('monstrous growth'/'PO2', 'Any one creature gets +4/+4 until the end of the turn.').
card_image_name('monstrous growth'/'PO2', 'monstrous growth').
card_uid('monstrous growth'/'PO2', 'PO2:Monstrous Growth:monstrous growth').
card_rarity('monstrous growth'/'PO2', 'Common').
card_artist('monstrous growth'/'PO2', 'Una Fricker').
card_flavor_text('monstrous growth'/'PO2', 'The nightstalkers\' little game of tease-the-squirrel suddenly took an unexpected turn.').
card_multiverse_id('monstrous growth'/'PO2', '6621').

card_in_set('mountain', 'PO2').
card_original_type('mountain'/'PO2', 'Land').
card_original_text('mountain'/'PO2', 'R').
card_image_name('mountain'/'PO2', 'mountain1').
card_uid('mountain'/'PO2', 'PO2:Mountain:mountain1').
card_rarity('mountain'/'PO2', 'Basic Land').
card_artist('mountain'/'PO2', 'Rob Alexander').
card_multiverse_id('mountain'/'PO2', '8395').

card_in_set('mountain', 'PO2').
card_original_type('mountain'/'PO2', 'Land').
card_original_text('mountain'/'PO2', 'R').
card_image_name('mountain'/'PO2', 'mountain2').
card_uid('mountain'/'PO2', 'PO2:Mountain:mountain2').
card_rarity('mountain'/'PO2', 'Basic Land').
card_artist('mountain'/'PO2', 'Rob Alexander').
card_multiverse_id('mountain'/'PO2', '8406').

card_in_set('mountain', 'PO2').
card_original_type('mountain'/'PO2', 'Land').
card_original_text('mountain'/'PO2', 'R').
card_image_name('mountain'/'PO2', 'mountain3').
card_uid('mountain'/'PO2', 'PO2:Mountain:mountain3').
card_rarity('mountain'/'PO2', 'Basic Land').
card_artist('mountain'/'PO2', 'Rob Alexander').
card_multiverse_id('mountain'/'PO2', '8407').

card_in_set('muck rats', 'PO2').
card_original_type('muck rats'/'PO2', 'Creature — Rats').
card_original_text('muck rats'/'PO2', '').
card_image_name('muck rats'/'PO2', 'muck rats').
card_uid('muck rats'/'PO2', 'PO2:Muck Rats:muck rats').
card_rarity('muck rats'/'PO2', 'Common').
card_artist('muck rats'/'PO2', 'Una Fricker').
card_flavor_text('muck rats'/'PO2', '\"Just as some fool said it couldn\'t get worse, the rats began to show up.\"\n—Alaborn soldier').
card_multiverse_id('muck rats'/'PO2', '6551').

card_in_set('mystic denial', 'PO2').
card_original_type('mystic denial'/'PO2', 'Sorcery').
card_original_text('mystic denial'/'PO2', 'Play Mystic Denial only in response to another player playing a creature or a sorcery. That card has no effect, and that player puts it into his or her graveyard.').
card_image_name('mystic denial'/'PO2', 'mystic denial').
card_uid('mystic denial'/'PO2', 'PO2:Mystic Denial:mystic denial').
card_rarity('mystic denial'/'PO2', 'Uncommon').
card_artist('mystic denial'/'PO2', 'DiTerlizzi').
card_multiverse_id('mystic denial'/'PO2', '6542').

card_in_set('natural spring', 'PO2').
card_original_type('natural spring'/'PO2', 'Sorcery').
card_original_text('natural spring'/'PO2', 'You gain 8 life.').
card_image_name('natural spring'/'PO2', 'natural spring').
card_uid('natural spring'/'PO2', 'PO2:Natural Spring:natural spring').
card_rarity('natural spring'/'PO2', 'Common').
card_artist('natural spring'/'PO2', 'Jeffrey R. Busch').
card_flavor_text('natural spring'/'PO2', 'The dying ask for water because they know it contains life.').
card_multiverse_id('natural spring'/'PO2', '6624').

card_in_set('nature\'s lore', 'PO2').
card_original_type('nature\'s lore'/'PO2', 'Sorcery').
card_original_text('nature\'s lore'/'PO2', 'Search your library for a forest card and put that forest into play. Shuffle your library afterwards.').
card_image_name('nature\'s lore'/'PO2', 'nature\'s lore').
card_uid('nature\'s lore'/'PO2', 'PO2:Nature\'s Lore:nature\'s lore').
card_rarity('nature\'s lore'/'PO2', 'Common').
card_artist('nature\'s lore'/'PO2', 'Lubov').
card_flavor_text('nature\'s lore'/'PO2', '\"Every seed planted is another lesson the forest can teach us.\"\n—Arathel, elvish queen').
card_multiverse_id('nature\'s lore'/'PO2', '6623').

card_in_set('nightstalker engine', 'PO2').
card_original_type('nightstalker engine'/'PO2', 'Creature — Nightstalker').
card_original_text('nightstalker engine'/'PO2', 'Nightstalker Engine has power equal to the number of creature cards in your graveyard.').
card_first_print('nightstalker engine', 'PO2').
card_image_name('nightstalker engine'/'PO2', 'nightstalker engine').
card_uid('nightstalker engine'/'PO2', 'PO2:Nightstalker Engine:nightstalker engine').
card_rarity('nightstalker engine'/'PO2', 'Rare').
card_artist('nightstalker engine'/'PO2', 'Mark Tedin').
card_flavor_text('nightstalker engine'/'PO2', 'Part metal, part bone, and all death.').
card_multiverse_id('nightstalker engine'/'PO2', '6577').

card_in_set('norwood archers', 'PO2').
card_original_type('norwood archers'/'PO2', 'Creature — Elves').
card_original_text('norwood archers'/'PO2', 'Norwood Archers can block creatures with flying.').
card_first_print('norwood archers', 'PO2').
card_image_name('norwood archers'/'PO2', 'norwood archers').
card_uid('norwood archers'/'PO2', 'PO2:Norwood Archers:norwood archers').
card_rarity('norwood archers'/'PO2', 'Common').
card_artist('norwood archers'/'PO2', 'Rebecca Guay').
card_flavor_text('norwood archers'/'PO2', '\"‘Air superiority?\' Not while our archers scan the skies.\"\n—Elvish scout').
card_multiverse_id('norwood archers'/'PO2', '6618').

card_in_set('norwood priestess', 'PO2').
card_original_type('norwood priestess'/'PO2', 'Creature — Elf').
card_original_text('norwood priestess'/'PO2', 'On your turn, before you attack, you may tap Norwood Priestess to put any green creature from your hand into play without paying for it.').
card_first_print('norwood priestess', 'PO2').
card_image_name('norwood priestess'/'PO2', 'norwood priestess').
card_uid('norwood priestess'/'PO2', 'PO2:Norwood Priestess:norwood priestess').
card_rarity('norwood priestess'/'PO2', 'Rare').
card_artist('norwood priestess'/'PO2', 'Melissa A. Benson').
card_multiverse_id('norwood priestess'/'PO2', '6634').

card_in_set('norwood ranger', 'PO2').
card_original_type('norwood ranger'/'PO2', 'Creature — Elf').
card_original_text('norwood ranger'/'PO2', '').
card_first_print('norwood ranger', 'PO2').
card_image_name('norwood ranger'/'PO2', 'norwood ranger').
card_uid('norwood ranger'/'PO2', 'PO2:Norwood Ranger:norwood ranger').
card_rarity('norwood ranger'/'PO2', 'Common').
card_artist('norwood ranger'/'PO2', 'Ron Spencer').
card_flavor_text('norwood ranger'/'PO2', 'Some trees bear deadly fruit.').
card_multiverse_id('norwood ranger'/'PO2', '6636').

card_in_set('norwood riders', 'PO2').
card_original_type('norwood riders'/'PO2', 'Creature — Elves').
card_original_text('norwood riders'/'PO2', 'Norwood Riders can\'t be blocked by more than one creature.').
card_first_print('norwood riders', 'PO2').
card_image_name('norwood riders'/'PO2', 'norwood riders').
card_uid('norwood riders'/'PO2', 'PO2:Norwood Riders:norwood riders').
card_rarity('norwood riders'/'PO2', 'Common').
card_artist('norwood riders'/'PO2', 'Rebecca Guay').
card_flavor_text('norwood riders'/'PO2', '\"Trade my moose? Sure—when I find a horse that can spear ten goblins at a time!\"').
card_multiverse_id('norwood riders'/'PO2', '6615').

card_in_set('norwood warrior', 'PO2').
card_original_type('norwood warrior'/'PO2', 'Creature — Elf').
card_original_text('norwood warrior'/'PO2', 'If Norwood Warrior attacks and is blocked, it gets +1/+1 until the end of the turn.').
card_first_print('norwood warrior', 'PO2').
card_image_name('norwood warrior'/'PO2', 'norwood warrior').
card_uid('norwood warrior'/'PO2', 'PO2:Norwood Warrior:norwood warrior').
card_rarity('norwood warrior'/'PO2', 'Common').
card_artist('norwood warrior'/'PO2', 'Rebecca Guay').
card_flavor_text('norwood warrior'/'PO2', '\"The woods hold peace for those who desire it, but those who seek battle will find me.\"').
card_multiverse_id('norwood warrior'/'PO2', '6620').

card_in_set('obsidian giant', 'PO2').
card_original_type('obsidian giant'/'PO2', 'Creature — Giant').
card_original_text('obsidian giant'/'PO2', '').
card_first_print('obsidian giant', 'PO2').
card_image_name('obsidian giant'/'PO2', 'obsidian giant').
card_uid('obsidian giant'/'PO2', 'PO2:Obsidian Giant:obsidian giant').
card_rarity('obsidian giant'/'PO2', 'Uncommon').
card_artist('obsidian giant'/'PO2', 'David A. Cherry').
card_flavor_text('obsidian giant'/'PO2', 'After a while, ships stopped sailing within his reach.').
card_multiverse_id('obsidian giant'/'PO2', '6599').

card_in_set('ogre arsonist', 'PO2').
card_original_type('ogre arsonist'/'PO2', 'Creature — Ogre').
card_original_text('ogre arsonist'/'PO2', 'When Ogre Arsonist comes into play from your hand, destroy any one land. (If you\'re the only one with lands, destroy one of them.)').
card_image_name('ogre arsonist'/'PO2', 'ogre arsonist').
card_uid('ogre arsonist'/'PO2', 'PO2:Ogre Arsonist:ogre arsonist').
card_rarity('ogre arsonist'/'PO2', 'Uncommon').
card_artist('ogre arsonist'/'PO2', 'Jeffrey R. Busch').
card_multiverse_id('ogre arsonist'/'PO2', '6595').

card_in_set('ogre berserker', 'PO2').
card_original_type('ogre berserker'/'PO2', 'Creature — Ogre').
card_original_text('ogre berserker'/'PO2', 'Ogre Berserker is unaffected by summoning sickness.').
card_first_print('ogre berserker', 'PO2').
card_image_name('ogre berserker'/'PO2', 'ogre berserker').
card_uid('ogre berserker'/'PO2', 'PO2:Ogre Berserker:ogre berserker').
card_rarity('ogre berserker'/'PO2', 'Common').
card_artist('ogre berserker'/'PO2', 'David A. Cherry').
card_flavor_text('ogre berserker'/'PO2', 'The machine does the growling, so the ogre\'s free to smile.').
card_multiverse_id('ogre berserker'/'PO2', '6586').

card_in_set('ogre taskmaster', 'PO2').
card_original_type('ogre taskmaster'/'PO2', 'Creature — Ogre').
card_original_text('ogre taskmaster'/'PO2', 'Ogre Taskmaster can\'t block.').
card_first_print('ogre taskmaster', 'PO2').
card_image_name('ogre taskmaster'/'PO2', 'ogre taskmaster').
card_uid('ogre taskmaster'/'PO2', 'PO2:Ogre Taskmaster:ogre taskmaster').
card_rarity('ogre taskmaster'/'PO2', 'Uncommon').
card_artist('ogre taskmaster'/'PO2', 'Dan Frazier').
card_flavor_text('ogre taskmaster'/'PO2', 'Three little goblins, enjoying their brew. One bumped an ogre, and then there were two.').
card_multiverse_id('ogre taskmaster'/'PO2', '6588').

card_in_set('ogre warrior', 'PO2').
card_original_type('ogre warrior'/'PO2', 'Creature — Ogre').
card_original_text('ogre warrior'/'PO2', '').
card_first_print('ogre warrior', 'PO2').
card_image_name('ogre warrior'/'PO2', 'ogre warrior').
card_uid('ogre warrior'/'PO2', 'PO2:Ogre Warrior:ogre warrior').
card_rarity('ogre warrior'/'PO2', 'Common').
card_artist('ogre warrior'/'PO2', 'Jeff Miracola').
card_flavor_text('ogre warrior'/'PO2', 'Assault and battery included.').
card_multiverse_id('ogre warrior'/'PO2', '6597').

card_in_set('path of peace', 'PO2').
card_original_type('path of peace'/'PO2', 'Sorcery').
card_original_text('path of peace'/'PO2', 'Destroy any one creature. That creature\'s owner gains 4 life.').
card_image_name('path of peace'/'PO2', 'path of peace').
card_uid('path of peace'/'PO2', 'PO2:Path of Peace:path of peace').
card_rarity('path of peace'/'PO2', 'Common').
card_artist('path of peace'/'PO2', 'David A. Cherry').
card_flavor_text('path of peace'/'PO2', '\"You could die famous or you could die old. I chose old.\"\n—Former soldier').
card_multiverse_id('path of peace'/'PO2', '6502').

card_in_set('piracy', 'PO2').
card_original_type('piracy'/'PO2', 'Sorcery').
card_original_text('piracy'/'PO2', 'This turn, you may tap your opponent\'s lands to help pay for your spells.').
card_first_print('piracy', 'PO2').
card_image_name('piracy'/'PO2', 'piracy').
card_uid('piracy'/'PO2', 'PO2:Piracy:piracy').
card_rarity('piracy'/'PO2', 'Rare').
card_artist('piracy'/'PO2', 'Bradley Williams').
card_flavor_text('piracy'/'PO2', 'The sea gives, and the sea takes. Some just help with the taking.').
card_multiverse_id('piracy'/'PO2', '6550').

card_in_set('plains', 'PO2').
card_original_type('plains'/'PO2', 'Land').
card_original_text('plains'/'PO2', 'W').
card_image_name('plains'/'PO2', 'plains1').
card_uid('plains'/'PO2', 'PO2:Plains:plains1').
card_rarity('plains'/'PO2', 'Basic Land').
card_artist('plains'/'PO2', 'Fred Fields').
card_multiverse_id('plains'/'PO2', '8380').

card_in_set('plains', 'PO2').
card_original_type('plains'/'PO2', 'Land').
card_original_text('plains'/'PO2', 'W').
card_image_name('plains'/'PO2', 'plains2').
card_uid('plains'/'PO2', 'PO2:Plains:plains2').
card_rarity('plains'/'PO2', 'Basic Land').
card_artist('plains'/'PO2', 'Fred Fields').
card_multiverse_id('plains'/'PO2', '8374').

card_in_set('plains', 'PO2').
card_original_type('plains'/'PO2', 'Land').
card_original_text('plains'/'PO2', 'W').
card_image_name('plains'/'PO2', 'plains3').
card_uid('plains'/'PO2', 'PO2:Plains:plains3').
card_rarity('plains'/'PO2', 'Basic Land').
card_artist('plains'/'PO2', 'Fred Fields').
card_multiverse_id('plains'/'PO2', '8354').

card_in_set('plated wurm', 'PO2').
card_original_type('plated wurm'/'PO2', 'Creature — Wurm').
card_original_text('plated wurm'/'PO2', '').
card_first_print('plated wurm', 'PO2').
card_image_name('plated wurm'/'PO2', 'plated wurm').
card_uid('plated wurm'/'PO2', 'PO2:Plated Wurm:plated wurm').
card_rarity('plated wurm'/'PO2', 'Common').
card_artist('plated wurm'/'PO2', 'Daniel Gelon').
card_flavor_text('plated wurm'/'PO2', '\"I\'d hate to see the bird that could get this wurm!\"\n—Alaborn soldier').
card_multiverse_id('plated wurm'/'PO2', '6617').

card_in_set('predatory nightstalker', 'PO2').
card_original_type('predatory nightstalker'/'PO2', 'Creature — Nightstalker').
card_original_text('predatory nightstalker'/'PO2', 'When Predatory Nightstalker comes into play from your hand, you may force your opponent to destroy any one of his or her creatures. (Your opponent chooses the creature.)').
card_first_print('predatory nightstalker', 'PO2').
card_image_name('predatory nightstalker'/'PO2', 'predatory nightstalker').
card_uid('predatory nightstalker'/'PO2', 'PO2:Predatory Nightstalker:predatory nightstalker').
card_rarity('predatory nightstalker'/'PO2', 'Uncommon').
card_artist('predatory nightstalker'/'PO2', 'D. Alexander Gregory').
card_multiverse_id('predatory nightstalker'/'PO2', '6565').

card_in_set('prowling nightstalker', 'PO2').
card_original_type('prowling nightstalker'/'PO2', 'Creature — Nightstalker').
card_original_text('prowling nightstalker'/'PO2', 'Prowling Nightstalker can\'t be blocked except by other black creatures.').
card_first_print('prowling nightstalker', 'PO2').
card_image_name('prowling nightstalker'/'PO2', 'prowling nightstalker').
card_uid('prowling nightstalker'/'PO2', 'PO2:Prowling Nightstalker:prowling nightstalker').
card_rarity('prowling nightstalker'/'PO2', 'Common').
card_artist('prowling nightstalker'/'PO2', 'Keith Parkinson').
card_flavor_text('prowling nightstalker'/'PO2', 'Silent as a serpent, twisted as a lone bog tree, evil as Tojira\'s heart.').
card_multiverse_id('prowling nightstalker'/'PO2', '6556').

card_in_set('raging goblin', 'PO2').
card_original_type('raging goblin'/'PO2', 'Creature — Goblin').
card_original_text('raging goblin'/'PO2', 'Raging Goblin is unaffected by summoning sickness.').
card_image_name('raging goblin'/'PO2', 'raging goblin').
card_uid('raging goblin'/'PO2', 'PO2:Raging Goblin:raging goblin').
card_rarity('raging goblin'/'PO2', 'Common').
card_artist('raging goblin'/'PO2', 'Jeff Miracola').
card_flavor_text('raging goblin'/'PO2', 'He raged at the world, at his family, at his life. But mostly he just raged.').
card_multiverse_id('raging goblin'/'PO2', '6581').

card_in_set('raiding nightstalker', 'PO2').
card_original_type('raiding nightstalker'/'PO2', 'Creature — Nightstalker').
card_original_text('raiding nightstalker'/'PO2', 'Swampwalk (If defending player has any swamps in play, Raiding Nightstalker can\'t be blocked.)').
card_first_print('raiding nightstalker', 'PO2').
card_image_name('raiding nightstalker'/'PO2', 'raiding nightstalker').
card_uid('raiding nightstalker'/'PO2', 'PO2:Raiding Nightstalker:raiding nightstalker').
card_rarity('raiding nightstalker'/'PO2', 'Common').
card_artist('raiding nightstalker'/'PO2', 'Pete Venters').
card_flavor_text('raiding nightstalker'/'PO2', '\"Our steeds need food, water, stabling. Theirs just need prey.\"\n—Restela, Alaborn marshal').
card_multiverse_id('raiding nightstalker'/'PO2', '6558').

card_in_set('rain of daggers', 'PO2').
card_original_type('rain of daggers'/'PO2', 'Sorcery').
card_original_text('rain of daggers'/'PO2', 'Destroy all your opponent\'s creatures. For each creature destroyed this way, you lose 2 life.').
card_first_print('rain of daggers', 'PO2').
card_image_name('rain of daggers'/'PO2', 'rain of daggers').
card_uid('rain of daggers'/'PO2', 'PO2:Rain of Daggers:rain of daggers').
card_rarity('rain of daggers'/'PO2', 'Rare').
card_artist('rain of daggers'/'PO2', 'Melissa A. Benson').
card_flavor_text('rain of daggers'/'PO2', 'Knives in the sky, cries in the air, and blood on the ground.').
card_multiverse_id('rain of daggers'/'PO2', '6580').

card_in_set('raise dead', 'PO2').
card_original_type('raise dead'/'PO2', 'Sorcery').
card_original_text('raise dead'/'PO2', 'Return any one creature from your graveyard to your hand.').
card_image_name('raise dead'/'PO2', 'raise dead').
card_uid('raise dead'/'PO2', 'PO2:Raise Dead:raise dead').
card_rarity('raise dead'/'PO2', 'Common').
card_artist('raise dead'/'PO2', 'Anson Maddocks').
card_flavor_text('raise dead'/'PO2', '\"Life from death is part of the natural cycle. This is merely a shortcut.\"\n—Tojira, swamp queen').
card_multiverse_id('raise dead'/'PO2', '6560').

card_in_set('rally the troops', 'PO2').
card_original_type('rally the troops'/'PO2', 'Sorcery').
card_original_text('rally the troops'/'PO2', 'Play Rally the Troops only after you\'re attacked, before you declare blockers.\nUntap all your creatures.').
card_first_print('rally the troops', 'PO2').
card_image_name('rally the troops'/'PO2', 'rally the troops').
card_uid('rally the troops'/'PO2', 'PO2:Rally the Troops:rally the troops').
card_rarity('rally the troops'/'PO2', 'Uncommon').
card_artist('rally the troops'/'PO2', 'Mark Poole').
card_multiverse_id('rally the troops'/'PO2', '6511').

card_in_set('ravenous rats', 'PO2').
card_original_type('ravenous rats'/'PO2', 'Creature — Rats').
card_original_text('ravenous rats'/'PO2', 'When Ravenous Rats comes into play from your hand, your opponent chooses and discards a card from his or her hand. (Ignore this effect if your opponent doesn\'t have any cards.)').
card_first_print('ravenous rats', 'PO2').
card_image_name('ravenous rats'/'PO2', 'ravenous rats').
card_uid('ravenous rats'/'PO2', 'PO2:Ravenous Rats:ravenous rats').
card_rarity('ravenous rats'/'PO2', 'Common').
card_artist('ravenous rats'/'PO2', 'Edward P. Beard, Jr.').
card_multiverse_id('ravenous rats'/'PO2', '6559').

card_in_set('razorclaw bear', 'PO2').
card_original_type('razorclaw bear'/'PO2', 'Creature — Bear').
card_original_text('razorclaw bear'/'PO2', 'If Razorclaw Bear attacks and is blocked, it gets +2/+2 until the end of the turn.').
card_first_print('razorclaw bear', 'PO2').
card_image_name('razorclaw bear'/'PO2', 'razorclaw bear').
card_uid('razorclaw bear'/'PO2', 'PO2:Razorclaw Bear:razorclaw bear').
card_rarity('razorclaw bear'/'PO2', 'Rare').
card_artist('razorclaw bear'/'PO2', 'Heather Hudson').
card_flavor_text('razorclaw bear'/'PO2', 'One razorclaw is three bears too many.\n—Elvish saying').
card_multiverse_id('razorclaw bear'/'PO2', '6638').

card_in_set('relentless assault', 'PO2').
card_original_type('relentless assault'/'PO2', 'Sorcery').
card_original_text('relentless assault'/'PO2', 'Untap all your creatures that attacked this turn. You may declare an additional attack this turn.').
card_image_name('relentless assault'/'PO2', 'relentless assault').
card_uid('relentless assault'/'PO2', 'PO2:Relentless Assault:relentless assault').
card_rarity('relentless assault'/'PO2', 'Rare').
card_artist('relentless assault'/'PO2', 'Tom Wänerstrand').
card_multiverse_id('relentless assault'/'PO2', '6608').

card_in_set('remove', 'PO2').
card_original_type('remove'/'PO2', 'Sorcery').
card_original_text('remove'/'PO2', 'Play Remove only after you\'re attacked, before you declare blockers.\nReturn any one attacking creature to its owner\'s hand.').
card_first_print('remove', 'PO2').
card_image_name('remove'/'PO2', 'remove').
card_uid('remove'/'PO2', 'PO2:Remove:remove').
card_rarity('remove'/'PO2', 'Uncommon').
card_artist('remove'/'PO2', 'DiTerlizzi').
card_multiverse_id('remove'/'PO2', '6541').

card_in_set('renewing touch', 'PO2').
card_original_type('renewing touch'/'PO2', 'Sorcery').
card_original_text('renewing touch'/'PO2', 'Choose any number of creature cards in your graveyard and shuffle them into your library.').
card_first_print('renewing touch', 'PO2').
card_image_name('renewing touch'/'PO2', 'renewing touch').
card_uid('renewing touch'/'PO2', 'PO2:Renewing Touch:renewing touch').
card_rarity('renewing touch'/'PO2', 'Uncommon').
card_artist('renewing touch'/'PO2', 'Rebecca Guay').
card_flavor_text('renewing touch'/'PO2', 'Death just encourages life the more.').
card_multiverse_id('renewing touch'/'PO2', '6629').

card_in_set('return of the nightstalkers', 'PO2').
card_original_type('return of the nightstalkers'/'PO2', 'Sorcery').
card_original_text('return of the nightstalkers'/'PO2', 'Return all the Nightstalker cards from your graveyard to play. Then destroy all your swamps. (Treat these Nightstalkers as though they just came into play from your hand.)').
card_first_print('return of the nightstalkers', 'PO2').
card_image_name('return of the nightstalkers'/'PO2', 'return of the nightstalkers').
card_uid('return of the nightstalkers'/'PO2', 'PO2:Return of the Nightstalkers:return of the nightstalkers').
card_rarity('return of the nightstalkers'/'PO2', 'Rare').
card_artist('return of the nightstalkers'/'PO2', 'Randy Gallegos').
card_multiverse_id('return of the nightstalkers'/'PO2', '6578').

card_in_set('righteous charge', 'PO2').
card_original_type('righteous charge'/'PO2', 'Sorcery').
card_original_text('righteous charge'/'PO2', 'All your creatures get +2/+2 until the end of the turn.').
card_first_print('righteous charge', 'PO2').
card_image_name('righteous charge'/'PO2', 'righteous charge').
card_uid('righteous charge'/'PO2', 'PO2:Righteous Charge:righteous charge').
card_rarity('righteous charge'/'PO2', 'Common').
card_artist('righteous charge'/'PO2', 'Jeffrey R. Busch').
card_flavor_text('righteous charge'/'PO2', '\"Bravery shines brightest in a pure soul.\"\n—Restela, Alaborn marshal').
card_multiverse_id('righteous charge'/'PO2', '6503').

card_in_set('righteous fury', 'PO2').
card_original_type('righteous fury'/'PO2', 'Sorcery').
card_original_text('righteous fury'/'PO2', 'Destroy all tapped creatures. For each creature destroyed this way, you gain 2 life. (This includes your creatures.)').
card_first_print('righteous fury', 'PO2').
card_image_name('righteous fury'/'PO2', 'righteous fury').
card_uid('righteous fury'/'PO2', 'PO2:Righteous Fury:righteous fury').
card_rarity('righteous fury'/'PO2', 'Rare').
card_artist('righteous fury'/'PO2', 'Edward P. Beard, Jr.').
card_multiverse_id('righteous fury'/'PO2', '6518').

card_in_set('river bear', 'PO2').
card_original_type('river bear'/'PO2', 'Creature — Bear').
card_original_text('river bear'/'PO2', 'Islandwalk (If defending player has any islands in play, River Bear can\'t be blocked.)').
card_first_print('river bear', 'PO2').
card_image_name('river bear'/'PO2', 'river bear').
card_uid('river bear'/'PO2', 'PO2:River Bear:river bear').
card_rarity('river bear'/'PO2', 'Uncommon').
card_artist('river bear'/'PO2', 'Una Fricker').
card_flavor_text('river bear'/'PO2', 'The bears had been isolated on an island for hundreds of years, until the island sank and the bears didn\'t.').
card_multiverse_id('river bear'/'PO2', '6626').

card_in_set('salvage', 'PO2').
card_original_type('salvage'/'PO2', 'Sorcery').
card_original_text('salvage'/'PO2', 'Take any one card from your graveyard and put that card on the top of your library.').
card_first_print('salvage', 'PO2').
card_image_name('salvage'/'PO2', 'salvage').
card_uid('salvage'/'PO2', 'PO2:Salvage:salvage').
card_rarity('salvage'/'PO2', 'Common').
card_artist('salvage'/'PO2', 'Keith Parkinson').
card_flavor_text('salvage'/'PO2', '\"What was taken shall be restored.\"\n—Arathel, elvish queen').
card_multiverse_id('salvage'/'PO2', '6622').

card_in_set('screeching drake', 'PO2').
card_original_type('screeching drake'/'PO2', 'Creature — Drake').
card_original_text('screeching drake'/'PO2', 'Flying\nWhen Screeching Drake comes into play from your hand, draw a card, then choose and discard a card from your hand.').
card_first_print('screeching drake', 'PO2').
card_image_name('screeching drake'/'PO2', 'screeching drake').
card_uid('screeching drake'/'PO2', 'PO2:Screeching Drake:screeching drake').
card_rarity('screeching drake'/'PO2', 'Common').
card_artist('screeching drake'/'PO2', 'Anson Maddocks').
card_multiverse_id('screeching drake'/'PO2', '6523').

card_in_set('sea drake', 'PO2').
card_original_type('sea drake'/'PO2', 'Creature — Drake').
card_original_text('sea drake'/'PO2', 'Flying\nWhen Sea Drake comes into play from your hand, return any two of your lands from play to your hand.').
card_first_print('sea drake', 'PO2').
card_image_name('sea drake'/'PO2', 'sea drake').
card_uid('sea drake'/'PO2', 'PO2:Sea Drake:sea drake').
card_rarity('sea drake'/'PO2', 'Uncommon').
card_artist('sea drake'/'PO2', 'Rebecca Guay').
card_multiverse_id('sea drake'/'PO2', '6539').

card_in_set('sleight of hand', 'PO2').
card_original_type('sleight of hand'/'PO2', 'Sorcery').
card_original_text('sleight of hand'/'PO2', 'Look at the top two cards of your library. Put one of them into your hand and the other on the bottom of your library.').
card_first_print('sleight of hand', 'PO2').
card_image_name('sleight of hand'/'PO2', 'sleight of hand').
card_uid('sleight of hand'/'PO2', 'PO2:Sleight of Hand:sleight of hand').
card_rarity('sleight of hand'/'PO2', 'Common').
card_artist('sleight of hand'/'PO2', 'Phil Foglio').
card_multiverse_id('sleight of hand'/'PO2', '6529').

card_in_set('spitting earth', 'PO2').
card_original_type('spitting earth'/'PO2', 'Sorcery').
card_original_text('spitting earth'/'PO2', 'Spitting Earth deals to any one creature damage equal to the number of mountains you have in play. (This includes both tapped and untapped mountains.)').
card_image_name('spitting earth'/'PO2', 'spitting earth').
card_uid('spitting earth'/'PO2', 'PO2:Spitting Earth:spitting earth').
card_rarity('spitting earth'/'PO2', 'Common').
card_artist('spitting earth'/'PO2', 'David Horne').
card_multiverse_id('spitting earth'/'PO2', '6589').

card_in_set('steam catapult', 'PO2').
card_original_type('steam catapult'/'PO2', 'Creature — Soldiers').
card_original_text('steam catapult'/'PO2', 'On your turn, before you attack, you may tap Steam Catapult to destroy any one tapped creature.').
card_first_print('steam catapult', 'PO2').
card_image_name('steam catapult'/'PO2', 'steam catapult').
card_uid('steam catapult'/'PO2', 'PO2:Steam Catapult:steam catapult').
card_rarity('steam catapult'/'PO2', 'Rare').
card_artist('steam catapult'/'PO2', 'Mark Tedin').
card_flavor_text('steam catapult'/'PO2', '\"You idiots! Turn it around! Turn it around!\"').
card_multiverse_id('steam catapult'/'PO2', '6517').

card_in_set('steam frigate', 'PO2').
card_original_type('steam frigate'/'PO2', 'Creature — Ship').
card_original_text('steam frigate'/'PO2', 'Steam Frigate can\'t attack unless the defending player has an island in play.').
card_first_print('steam frigate', 'PO2').
card_image_name('steam frigate'/'PO2', 'steam frigate').
card_uid('steam frigate'/'PO2', 'PO2:Steam Frigate:steam frigate').
card_rarity('steam frigate'/'PO2', 'Common').
card_artist('steam frigate'/'PO2', 'Mark Tedin').
card_flavor_text('steam frigate'/'PO2', '\"Is it merchants or is it pirates?\"\n\"It\'s Talas—there\'s no difference.\"').
card_multiverse_id('steam frigate'/'PO2', '6526').

card_in_set('stone rain', 'PO2').
card_original_type('stone rain'/'PO2', 'Sorcery').
card_original_text('stone rain'/'PO2', 'Destroy any one land.').
card_image_name('stone rain'/'PO2', 'stone rain').
card_uid('stone rain'/'PO2', 'PO2:Stone Rain:stone rain').
card_rarity('stone rain'/'PO2', 'Common').
card_artist('stone rain'/'PO2', 'Doug Chaffee').
card_flavor_text('stone rain'/'PO2', 'There goes the neighborhood!').
card_multiverse_id('stone rain'/'PO2', '6590').

card_in_set('swamp', 'PO2').
card_original_type('swamp'/'PO2', 'Land').
card_original_text('swamp'/'PO2', 'B').
card_image_name('swamp'/'PO2', 'swamp1').
card_uid('swamp'/'PO2', 'PO2:Swamp:swamp1').
card_rarity('swamp'/'PO2', 'Basic Land').
card_artist('swamp'/'PO2', 'Susan Van Camp').
card_multiverse_id('swamp'/'PO2', '8394').

card_in_set('swamp', 'PO2').
card_original_type('swamp'/'PO2', 'Land').
card_original_text('swamp'/'PO2', 'B').
card_image_name('swamp'/'PO2', 'swamp2').
card_uid('swamp'/'PO2', 'PO2:Swamp:swamp2').
card_rarity('swamp'/'PO2', 'Basic Land').
card_artist('swamp'/'PO2', 'Susan Van Camp').
card_multiverse_id('swamp'/'PO2', '8392').

card_in_set('swamp', 'PO2').
card_original_type('swamp'/'PO2', 'Land').
card_original_text('swamp'/'PO2', 'B').
card_image_name('swamp'/'PO2', 'swamp3').
card_uid('swamp'/'PO2', 'PO2:Swamp:swamp3').
card_rarity('swamp'/'PO2', 'Basic Land').
card_artist('swamp'/'PO2', 'Susan Van Camp').
card_multiverse_id('swamp'/'PO2', '8393').

card_in_set('swarm of rats', 'PO2').
card_original_type('swarm of rats'/'PO2', 'Creature — Rats').
card_original_text('swarm of rats'/'PO2', 'Swarm of Rats has power equal to the number of Rat cards you have in play. (This includes both tapped and untapped Rat cards.)').
card_first_print('swarm of rats', 'PO2').
card_image_name('swarm of rats'/'PO2', 'swarm of rats').
card_uid('swarm of rats'/'PO2', 'PO2:Swarm of Rats:swarm of rats').
card_rarity('swarm of rats'/'PO2', 'Common').
card_artist('swarm of rats'/'PO2', 'Kev Walker').
card_multiverse_id('swarm of rats'/'PO2', '6555').

card_in_set('sylvan basilisk', 'PO2').
card_original_type('sylvan basilisk'/'PO2', 'Creature — Basilisk').
card_original_text('sylvan basilisk'/'PO2', 'If Sylvan Basilisk attacks and is blocked, destroy all creatures blocking it. (Destroy the creatures before they deal damage. The Basilisk doesn\'t damage your opponent.)').
card_first_print('sylvan basilisk', 'PO2').
card_image_name('sylvan basilisk'/'PO2', 'sylvan basilisk').
card_uid('sylvan basilisk'/'PO2', 'PO2:Sylvan Basilisk:sylvan basilisk').
card_rarity('sylvan basilisk'/'PO2', 'Rare').
card_artist('sylvan basilisk'/'PO2', 'Ron Spencer').
card_multiverse_id('sylvan basilisk'/'PO2', '8905').

card_in_set('sylvan yeti', 'PO2').
card_original_type('sylvan yeti'/'PO2', 'Creature — Beast').
card_original_text('sylvan yeti'/'PO2', 'Sylvan Yeti has power equal to the number of cards you have in your hand.').
card_first_print('sylvan yeti', 'PO2').
card_image_name('sylvan yeti'/'PO2', 'sylvan yeti').
card_uid('sylvan yeti'/'PO2', 'PO2:Sylvan Yeti:sylvan yeti').
card_rarity('sylvan yeti'/'PO2', 'Rare').
card_artist('sylvan yeti'/'PO2', 'Brom').
card_flavor_text('sylvan yeti'/'PO2', 'The deeper the wood, the greater its strength.').
card_multiverse_id('sylvan yeti'/'PO2', '6635').

card_in_set('talas air ship', 'PO2').
card_original_type('talas air ship'/'PO2', 'Creature — Ship').
card_original_text('talas air ship'/'PO2', 'Flying').
card_first_print('talas air ship', 'PO2').
card_image_name('talas air ship'/'PO2', 'talas air ship').
card_uid('talas air ship'/'PO2', 'PO2:Talas Air Ship:talas air ship').
card_rarity('talas air ship'/'PO2', 'Common').
card_artist('talas air ship'/'PO2', 'Mark Tedin').
card_flavor_text('talas air ship'/'PO2', '\"Their ships pollute our air as they themselves pollute our forest.\"\n—Arathel, elvish queen').
card_multiverse_id('talas air ship'/'PO2', '6524').

card_in_set('talas explorer', 'PO2').
card_original_type('talas explorer'/'PO2', 'Creature — Merchant').
card_original_text('talas explorer'/'PO2', 'Flying\nWhen Talas Explorer comes into play from your hand, look at your opponent\'s hand.').
card_first_print('talas explorer', 'PO2').
card_image_name('talas explorer'/'PO2', 'talas explorer').
card_uid('talas explorer'/'PO2', 'PO2:Talas Explorer:talas explorer').
card_rarity('talas explorer'/'PO2', 'Common').
card_artist('talas explorer'/'PO2', 'Douglas Shuler').
card_multiverse_id('talas explorer'/'PO2', '6522').

card_in_set('talas merchant', 'PO2').
card_original_type('talas merchant'/'PO2', 'Creature — Merchant').
card_original_text('talas merchant'/'PO2', '').
card_first_print('talas merchant', 'PO2').
card_image_name('talas merchant'/'PO2', 'talas merchant').
card_uid('talas merchant'/'PO2', 'PO2:Talas Merchant:talas merchant').
card_rarity('talas merchant'/'PO2', 'Common').
card_artist('talas merchant'/'PO2', 'Lubov').
card_flavor_text('talas merchant'/'PO2', 'The trader let loose a laugh that made all around him check their purses.').
card_multiverse_id('talas merchant'/'PO2', '6525').

card_in_set('talas researcher', 'PO2').
card_original_type('talas researcher'/'PO2', 'Creature — Wizard').
card_original_text('talas researcher'/'PO2', 'On your turn, before you attack, you may tap Talas Researcher to draw a card.').
card_first_print('talas researcher', 'PO2').
card_image_name('talas researcher'/'PO2', 'talas researcher').
card_uid('talas researcher'/'PO2', 'PO2:Talas Researcher:talas researcher').
card_rarity('talas researcher'/'PO2', 'Rare').
card_artist('talas researcher'/'PO2', 'Kaja Foglio').
card_flavor_text('talas researcher'/'PO2', 'From time, knowledge.\nFrom knowledge, power.').
card_multiverse_id('talas researcher'/'PO2', '6547').

card_in_set('talas scout', 'PO2').
card_original_type('talas scout'/'PO2', 'Creature — Pirate').
card_original_text('talas scout'/'PO2', 'Flying').
card_first_print('talas scout', 'PO2').
card_image_name('talas scout'/'PO2', 'talas scout').
card_uid('talas scout'/'PO2', 'PO2:Talas Scout:talas scout').
card_rarity('talas scout'/'PO2', 'Common').
card_artist('talas scout'/'PO2', 'Heather Hudson').
card_flavor_text('talas scout'/'PO2', '\"Scouting a battle before you fight it is just good business.\"\n—Jefan, Talas ship captain').
card_multiverse_id('talas scout'/'PO2', '6521').

card_in_set('talas warrior', 'PO2').
card_original_type('talas warrior'/'PO2', 'Creature — Pirate').
card_original_text('talas warrior'/'PO2', 'Talas Warrior can\'t be blocked.').
card_first_print('talas warrior', 'PO2').
card_image_name('talas warrior'/'PO2', 'talas warrior').
card_uid('talas warrior'/'PO2', 'PO2:Talas Warrior:talas warrior').
card_rarity('talas warrior'/'PO2', 'Rare').
card_artist('talas warrior'/'PO2', 'Douglas Shuler').
card_flavor_text('talas warrior'/'PO2', 'The sea is in their blood. Literally.').
card_multiverse_id('talas warrior'/'PO2', '6545').

card_in_set('temple acolyte', 'PO2').
card_original_type('temple acolyte'/'PO2', 'Creature — Cleric').
card_original_text('temple acolyte'/'PO2', 'When Temple Acolyte comes into play from your hand, you gain 3 life.').
card_first_print('temple acolyte', 'PO2').
card_image_name('temple acolyte'/'PO2', 'temple acolyte').
card_uid('temple acolyte'/'PO2', 'PO2:Temple Acolyte:temple acolyte').
card_rarity('temple acolyte'/'PO2', 'Common').
card_artist('temple acolyte'/'PO2', 'Lubov').
card_flavor_text('temple acolyte'/'PO2', 'Young, yes. Inexperienced, yes. Weak? Don\'t count on it.').
card_multiverse_id('temple acolyte'/'PO2', '6495').

card_in_set('temple elder', 'PO2').
card_original_type('temple elder'/'PO2', 'Creature — Cleric').
card_original_text('temple elder'/'PO2', 'On your turn, before you attack, you may tap Temple Elder to gain 1 life.').
card_first_print('temple elder', 'PO2').
card_image_name('temple elder'/'PO2', 'temple elder').
card_uid('temple elder'/'PO2', 'PO2:Temple Elder:temple elder').
card_rarity('temple elder'/'PO2', 'Uncommon').
card_artist('temple elder'/'PO2', 'David Horne').
card_flavor_text('temple elder'/'PO2', '\"Give us breath. Give us life. Prepare us for the day we make.\"\n—The Alaborn \"Rite of Battle\"').
card_multiverse_id('temple elder'/'PO2', '6507').

card_in_set('temporal manipulation', 'PO2').
card_original_type('temporal manipulation'/'PO2', 'Sorcery').
card_original_text('temporal manipulation'/'PO2', 'You take another turn after this one.').
card_first_print('temporal manipulation', 'PO2').
card_image_name('temporal manipulation'/'PO2', 'temporal manipulation').
card_uid('temporal manipulation'/'PO2', 'PO2:Temporal Manipulation:temporal manipulation').
card_rarity('temporal manipulation'/'PO2', 'Rare').
card_artist('temporal manipulation'/'PO2', 'Anson Maddocks').
card_flavor_text('temporal manipulation'/'PO2', 'Doing something at the last minute isn\'t so bad when you can make that minute last.').
card_multiverse_id('temporal manipulation'/'PO2', '6548').

card_in_set('theft of dreams', 'PO2').
card_original_type('theft of dreams'/'PO2', 'Sorcery').
card_original_text('theft of dreams'/'PO2', 'For each tapped creature your opponent has in play, you draw a card.').
card_image_name('theft of dreams'/'PO2', 'theft of dreams').
card_uid('theft of dreams'/'PO2', 'PO2:Theft of Dreams:theft of dreams').
card_rarity('theft of dreams'/'PO2', 'Uncommon').
card_artist('theft of dreams'/'PO2', 'Randy Gallegos').
card_flavor_text('theft of dreams'/'PO2', '\"First they fight, then they sleep, then they dream. And finally I receive my reward.\"').
card_multiverse_id('theft of dreams'/'PO2', '6543').

card_in_set('tidal surge', 'PO2').
card_original_type('tidal surge'/'PO2', 'Sorcery').
card_original_text('tidal surge'/'PO2', 'Tap any one, two, or three creatures without flying. (Tapped creatures can\'t block.)').
card_image_name('tidal surge'/'PO2', 'tidal surge').
card_uid('tidal surge'/'PO2', 'PO2:Tidal Surge:tidal surge').
card_rarity('tidal surge'/'PO2', 'Common').
card_artist('tidal surge'/'PO2', 'Phil Foglio').
card_flavor_text('tidal surge'/'PO2', 'Four little goblins, trapped by the sea. One teased a serpent, and then there were three.').
card_multiverse_id('tidal surge'/'PO2', '6531').

card_in_set('time ebb', 'PO2').
card_original_type('time ebb'/'PO2', 'Sorcery').
card_original_text('time ebb'/'PO2', 'Return any one creature from play to the top of its owner\'s library.').
card_image_name('time ebb'/'PO2', 'time ebb').
card_uid('time ebb'/'PO2', 'PO2:Time Ebb:time ebb').
card_rarity('time ebb'/'PO2', 'Common').
card_artist('time ebb'/'PO2', 'Sam Wood').
card_flavor_text('time ebb'/'PO2', '\"The ogre had come from out of the blue, so that\'s where I sent it back.\"\n—Talas wizard').
card_multiverse_id('time ebb'/'PO2', '6532').

card_in_set('touch of brilliance', 'PO2').
card_original_type('touch of brilliance'/'PO2', 'Sorcery').
card_original_text('touch of brilliance'/'PO2', 'Draw two cards.').
card_image_name('touch of brilliance'/'PO2', 'touch of brilliance').
card_uid('touch of brilliance'/'PO2', 'PO2:Touch of Brilliance:touch of brilliance').
card_rarity('touch of brilliance'/'PO2', 'Common').
card_artist('touch of brilliance'/'PO2', 'Kaja Foglio').
card_flavor_text('touch of brilliance'/'PO2', '\"I don\'t want what I don\'t have. I want more of what I do have.\"').
card_multiverse_id('touch of brilliance'/'PO2', '6527').

card_in_set('town sentry', 'PO2').
card_original_type('town sentry'/'PO2', 'Creature — Soldier').
card_original_text('town sentry'/'PO2', 'If Town Sentry blocks, it gets +0/+2 until the end of the turn.').
card_first_print('town sentry', 'PO2').
card_image_name('town sentry'/'PO2', 'town sentry').
card_uid('town sentry'/'PO2', 'PO2:Town Sentry:town sentry').
card_rarity('town sentry'/'PO2', 'Common').
card_artist('town sentry'/'PO2', 'Bradley Williams').
card_flavor_text('town sentry'/'PO2', '\"So he gots a sword. Big deal.\"\n\"Yeah, it\'s a big deal—it\'s a big sword!\"\n—Two goblins outside the gates of Trokin').
card_multiverse_id('town sentry'/'PO2', '6494').

card_in_set('tree monkey', 'PO2').
card_original_type('tree monkey'/'PO2', 'Creature — Monkey').
card_original_text('tree monkey'/'PO2', 'Tree Monkey can block creatures with flying.').
card_first_print('tree monkey', 'PO2').
card_image_name('tree monkey'/'PO2', 'tree monkey').
card_uid('tree monkey'/'PO2', 'PO2:Tree Monkey:tree monkey').
card_rarity('tree monkey'/'PO2', 'Common').
card_artist('tree monkey'/'PO2', 'Una Fricker').
card_flavor_text('tree monkey'/'PO2', '\"They serve the world best on a platter with shallots and onions.\"\n—Talas sailor').
card_multiverse_id('tree monkey'/'PO2', '6619').

card_in_set('tremor', 'PO2').
card_original_type('tremor'/'PO2', 'Sorcery').
card_original_text('tremor'/'PO2', 'Tremor deals 1 damage to each creature without flying. (This includes your creatures without flying.)').
card_image_name('tremor'/'PO2', 'tremor').
card_uid('tremor'/'PO2', 'PO2:Tremor:tremor').
card_rarity('tremor'/'PO2', 'Common').
card_artist('tremor'/'PO2', 'Pete Venters').
card_flavor_text('tremor'/'PO2', 'One little goblin shook up the ground. When the dust cleared, no one was found.').
card_multiverse_id('tremor'/'PO2', '6592').

card_in_set('trokin high guard', 'PO2').
card_original_type('trokin high guard'/'PO2', 'Creature — Knight').
card_original_text('trokin high guard'/'PO2', '').
card_first_print('trokin high guard', 'PO2').
card_image_name('trokin high guard'/'PO2', 'trokin high guard').
card_uid('trokin high guard'/'PO2', 'PO2:Trokin High Guard:trokin high guard').
card_rarity('trokin high guard'/'PO2', 'Common').
card_artist('trokin high guard'/'PO2', 'Ron Spencer').
card_flavor_text('trokin high guard'/'PO2', 'Battle tempers soldiers as fire tempers steel.').
card_multiverse_id('trokin high guard'/'PO2', '6496').

card_in_set('undo', 'PO2').
card_original_type('undo'/'PO2', 'Sorcery').
card_original_text('undo'/'PO2', 'Return any two creatures from play to their owner\'s hand. (You can\'t play Undo unless you can choose two creatures to return.)').
card_image_name('undo'/'PO2', 'undo').
card_uid('undo'/'PO2', 'PO2:Undo:undo').
card_rarity('undo'/'PO2', 'Uncommon').
card_artist('undo'/'PO2', 'Henry Van Der Linde').
card_multiverse_id('undo'/'PO2', '6540').

card_in_set('untamed wilds', 'PO2').
card_original_type('untamed wilds'/'PO2', 'Sorcery').
card_original_text('untamed wilds'/'PO2', 'Search your library for a plains, island, swamp, mountain, or forest card and put that land into play. Shuffle your library afterwards.').
card_image_name('untamed wilds'/'PO2', 'untamed wilds').
card_uid('untamed wilds'/'PO2', 'PO2:Untamed Wilds:untamed wilds').
card_rarity('untamed wilds'/'PO2', 'Uncommon').
card_artist('untamed wilds'/'PO2', 'Jeffrey R. Busch').
card_multiverse_id('untamed wilds'/'PO2', '6630').

card_in_set('vampiric spirit', 'PO2').
card_original_type('vampiric spirit'/'PO2', 'Creature — Spirit').
card_original_text('vampiric spirit'/'PO2', 'Flying\nWhen Vampiric Spirit comes into play from your hand, you lose 4 life. (The person who plays Vampiric Spirit loses the life.)').
card_first_print('vampiric spirit', 'PO2').
card_image_name('vampiric spirit'/'PO2', 'vampiric spirit').
card_uid('vampiric spirit'/'PO2', 'PO2:Vampiric Spirit:vampiric spirit').
card_rarity('vampiric spirit'/'PO2', 'Rare').
card_artist('vampiric spirit'/'PO2', 'Anson Maddocks').
card_multiverse_id('vampiric spirit'/'PO2', '6575').

card_in_set('vengeance', 'PO2').
card_original_type('vengeance'/'PO2', 'Sorcery').
card_original_text('vengeance'/'PO2', 'Destroy any one tapped creature.').
card_image_name('vengeance'/'PO2', 'vengeance').
card_uid('vengeance'/'PO2', 'PO2:Vengeance:vengeance').
card_rarity('vengeance'/'PO2', 'Uncommon').
card_artist('vengeance'/'PO2', 'Keith Parkinson').
card_flavor_text('vengeance'/'PO2', 'A bullet renders all sizes equal.').
card_multiverse_id('vengeance'/'PO2', '6513').

card_in_set('volcanic hammer', 'PO2').
card_original_type('volcanic hammer'/'PO2', 'Sorcery').
card_original_text('volcanic hammer'/'PO2', 'Volcanic Hammer deals 3 damage to any one creature or player.').
card_image_name('volcanic hammer'/'PO2', 'volcanic hammer').
card_uid('volcanic hammer'/'PO2', 'PO2:Volcanic Hammer:volcanic hammer').
card_rarity('volcanic hammer'/'PO2', 'Common').
card_artist('volcanic hammer'/'PO2', 'Edward P. Beard, Jr.').
card_flavor_text('volcanic hammer'/'PO2', 'Fire finds its form in the heat of the forge.').
card_multiverse_id('volcanic hammer'/'PO2', '6593').

card_in_set('volunteer militia', 'PO2').
card_original_type('volunteer militia'/'PO2', 'Creature — Soldier').
card_original_text('volunteer militia'/'PO2', '').
card_first_print('volunteer militia', 'PO2').
card_image_name('volunteer militia'/'PO2', 'volunteer militia').
card_uid('volunteer militia'/'PO2', 'PO2:Volunteer Militia:volunteer militia').
card_rarity('volunteer militia'/'PO2', 'Common').
card_artist('volunteer militia'/'PO2', 'Keith Parkinson').
card_flavor_text('volunteer militia'/'PO2', 'People fight hardest on their own soil.').
card_multiverse_id('volunteer militia'/'PO2', '6491').

card_in_set('warrior\'s stand', 'PO2').
card_original_type('warrior\'s stand'/'PO2', 'Sorcery').
card_original_text('warrior\'s stand'/'PO2', 'Play Warrior\'s Stand only after you\'re attacked, before you declare blockers.\nAll your creatures get +2/+2 until the end of the turn.').
card_first_print('warrior\'s stand', 'PO2').
card_image_name('warrior\'s stand'/'PO2', 'warrior\'s stand').
card_uid('warrior\'s stand'/'PO2', 'PO2:Warrior\'s Stand:warrior\'s stand').
card_rarity('warrior\'s stand'/'PO2', 'Uncommon').
card_artist('warrior\'s stand'/'PO2', 'Keith Parkinson').
card_multiverse_id('warrior\'s stand'/'PO2', '6512').

card_in_set('wild griffin', 'PO2').
card_original_type('wild griffin'/'PO2', 'Creature — Griffin').
card_original_text('wild griffin'/'PO2', 'Flying').
card_first_print('wild griffin', 'PO2').
card_image_name('wild griffin'/'PO2', 'wild griffin').
card_uid('wild griffin'/'PO2', 'PO2:Wild Griffin:wild griffin').
card_rarity('wild griffin'/'PO2', 'Common').
card_artist('wild griffin'/'PO2', 'Jeff Miracola').
card_flavor_text('wild griffin'/'PO2', 'Two little goblins, out in the sun. Down came a griffin, and then there was one.').
card_multiverse_id('wild griffin'/'PO2', '6497').

card_in_set('wild ox', 'PO2').
card_original_type('wild ox'/'PO2', 'Creature — Ox').
card_original_text('wild ox'/'PO2', 'Swampwalk (If defending player has any swamps in play, Wild Ox can\'t be blocked.)').
card_first_print('wild ox', 'PO2').
card_image_name('wild ox'/'PO2', 'wild ox').
card_uid('wild ox'/'PO2', 'PO2:Wild Ox:wild ox').
card_rarity('wild ox'/'PO2', 'Uncommon').
card_artist('wild ox'/'PO2', 'Jeffrey R. Busch').
card_flavor_text('wild ox'/'PO2', 'It has the run of the swamps, and it knows it.').
card_multiverse_id('wild ox'/'PO2', '6625').

card_in_set('wildfire', 'PO2').
card_original_type('wildfire'/'PO2', 'Sorcery').
card_original_text('wildfire'/'PO2', 'You destroy four of your lands and your opponent destroys four of his or her lands. Then Wildfire deals 4 damage to each creature. (This includes your creatures.)').
card_first_print('wildfire', 'PO2').
card_image_name('wildfire'/'PO2', 'wildfire').
card_uid('wildfire'/'PO2', 'PO2:Wildfire:wildfire').
card_rarity('wildfire'/'PO2', 'Rare').
card_artist('wildfire'/'PO2', 'Rob Alexander').
card_multiverse_id('wildfire'/'PO2', '6610').

card_in_set('wind sail', 'PO2').
card_original_type('wind sail'/'PO2', 'Sorcery').
card_original_text('wind sail'/'PO2', 'Any one or two creatures gain flying until the end of the turn.').
card_first_print('wind sail', 'PO2').
card_image_name('wind sail'/'PO2', 'wind sail').
card_uid('wind sail'/'PO2', 'PO2:Wind Sail:wind sail').
card_rarity('wind sail'/'PO2', 'Common').
card_artist('wind sail'/'PO2', 'Matt Stawicki').
card_flavor_text('wind sail'/'PO2', 'It pays to be at home both on the sea and in the sky.').
card_multiverse_id('wind sail'/'PO2', '6530').
