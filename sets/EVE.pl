% Eventide

set('EVE').
set_name('EVE', 'Eventide').
set_release_date('EVE', '2008-07-25').
set_border('EVE', 'black').
set_type('EVE', 'expansion').
set_block('EVE', 'Shadowmoor').

card_in_set('aerie ouphes', 'EVE').
card_original_type('aerie ouphes'/'EVE', 'Creature — Ouphe').
card_original_text('aerie ouphes'/'EVE', 'Sacrifice Aerie Ouphes: Aerie Ouphes deals damage equal to its power to target creature with flying.\nPersist (When this creature is put into a graveyard from play, if it had no -1/-1 counters on it, return it to play under its owner\'s control with a -1/-1 counter on it.)').
card_first_print('aerie ouphes', 'EVE').
card_image_name('aerie ouphes'/'EVE', 'aerie ouphes').
card_uid('aerie ouphes'/'EVE', 'EVE:Aerie Ouphes:aerie ouphes').
card_rarity('aerie ouphes'/'EVE', 'Common').
card_artist('aerie ouphes'/'EVE', 'Jesper Ejsing').
card_number('aerie ouphes'/'EVE', '65').
card_multiverse_id('aerie ouphes'/'EVE', '153427').

card_in_set('altar golem', 'EVE').
card_original_type('altar golem'/'EVE', 'Artifact Creature — Golem').
card_original_text('altar golem'/'EVE', 'Trample\nAltar Golem\'s power and toughness are each equal to the number of creatures in play.\nAltar Golem doesn\'t untap during its controller\'s untap step.\nTap five untapped creatures you control: Untap Altar Golem.').
card_first_print('altar golem', 'EVE').
card_image_name('altar golem'/'EVE', 'altar golem').
card_uid('altar golem'/'EVE', 'EVE:Altar Golem:altar golem').
card_rarity('altar golem'/'EVE', 'Rare').
card_artist('altar golem'/'EVE', 'Rob Alexander').
card_number('altar golem'/'EVE', '166').
card_multiverse_id('altar golem'/'EVE', '157974').

card_in_set('antler skulkin', 'EVE').
card_original_type('antler skulkin'/'EVE', 'Artifact Creature — Scarecrow').
card_original_text('antler skulkin'/'EVE', '{2}: Target white creature gains persist until end of turn. (When it\'s put into a graveyard from play, if it had no -1/-1 counters on it, return it to play under its owner\'s control with a -1/-1 counter on it.)').
card_first_print('antler skulkin', 'EVE').
card_image_name('antler skulkin'/'EVE', 'antler skulkin').
card_uid('antler skulkin'/'EVE', 'EVE:Antler Skulkin:antler skulkin').
card_rarity('antler skulkin'/'EVE', 'Common').
card_artist('antler skulkin'/'EVE', 'Dave Kendall').
card_number('antler skulkin'/'EVE', '167').
card_flavor_text('antler skulkin'/'EVE', 'Every skull has a tale to tell, and a skulkin is many dire tales cobbled together.').
card_multiverse_id('antler skulkin'/'EVE', '158293').

card_in_set('archon of justice', 'EVE').
card_original_type('archon of justice'/'EVE', 'Creature — Archon').
card_original_text('archon of justice'/'EVE', 'Flying\nWhen Archon of Justice is put into a graveyard from play, remove target permanent from the game.').
card_first_print('archon of justice', 'EVE').
card_image_name('archon of justice'/'EVE', 'archon of justice').
card_uid('archon of justice'/'EVE', 'EVE:Archon of Justice:archon of justice').
card_rarity('archon of justice'/'EVE', 'Rare').
card_artist('archon of justice'/'EVE', 'Jason Chan').
card_number('archon of justice'/'EVE', '1').
card_flavor_text('archon of justice'/'EVE', 'In dark times, Truth bears a blade.').
card_multiverse_id('archon of justice'/'EVE', '146006').

card_in_set('ashling, the extinguisher', 'EVE').
card_original_type('ashling, the extinguisher'/'EVE', 'Legendary Creature — Elemental Shaman').
card_original_text('ashling, the extinguisher'/'EVE', 'Whenever Ashling, the Extinguisher deals combat damage to a player, choose target creature that player controls. He or she sacrifices that creature.').
card_first_print('ashling, the extinguisher', 'EVE').
card_image_name('ashling, the extinguisher'/'EVE', 'ashling, the extinguisher').
card_uid('ashling, the extinguisher'/'EVE', 'EVE:Ashling, the Extinguisher:ashling, the extinguisher').
card_rarity('ashling, the extinguisher'/'EVE', 'Rare').
card_artist('ashling, the extinguisher'/'EVE', 'Wayne Reynolds').
card_number('ashling, the extinguisher'/'EVE', '33').
card_flavor_text('ashling, the extinguisher'/'EVE', 'The Aurora had transformed her into a relentless force of destruction.').
card_multiverse_id('ashling, the extinguisher'/'EVE', '151137').

card_in_set('balefire liege', 'EVE').
card_original_type('balefire liege'/'EVE', 'Creature — Spirit Horror').
card_original_text('balefire liege'/'EVE', 'Other red creatures you control get +1/+1.\nOther white creatures you control get +1/+1.\nWhenever you play a red spell, Balefire Liege deals 3 damage to target player.\nWhenever you play a white spell, you gain 3 life.').
card_first_print('balefire liege', 'EVE').
card_image_name('balefire liege'/'EVE', 'balefire liege').
card_uid('balefire liege'/'EVE', 'EVE:Balefire Liege:balefire liege').
card_rarity('balefire liege'/'EVE', 'Rare').
card_artist('balefire liege'/'EVE', 'Ralph Horsley').
card_number('balefire liege'/'EVE', '132').
card_multiverse_id('balefire liege'/'EVE', '158104').

card_in_set('ballynock trapper', 'EVE').
card_original_type('ballynock trapper'/'EVE', 'Creature — Kithkin Soldier').
card_original_text('ballynock trapper'/'EVE', '{T}: Tap target creature.\nWhenever you play a white spell, you may untap Ballynock Trapper.').
card_first_print('ballynock trapper', 'EVE').
card_image_name('ballynock trapper'/'EVE', 'ballynock trapper').
card_uid('ballynock trapper'/'EVE', 'EVE:Ballynock Trapper:ballynock trapper').
card_rarity('ballynock trapper'/'EVE', 'Common').
card_artist('ballynock trapper'/'EVE', 'Randy Gallegos').
card_number('ballynock trapper'/'EVE', '2').
card_flavor_text('ballynock trapper'/'EVE', 'No matter how large or small, fierce or fleeting, the kithkin have a trap for it.').
card_multiverse_id('ballynock trapper'/'EVE', '153480').

card_in_set('banishing knack', 'EVE').
card_original_type('banishing knack'/'EVE', 'Instant').
card_original_text('banishing knack'/'EVE', 'Until end of turn, target creature gains \"{T}: Return target nonland permanent to its owner\'s hand.\"').
card_first_print('banishing knack', 'EVE').
card_image_name('banishing knack'/'EVE', 'banishing knack').
card_uid('banishing knack'/'EVE', 'EVE:Banishing Knack:banishing knack').
card_rarity('banishing knack'/'EVE', 'Common').
card_artist('banishing knack'/'EVE', 'Howard Lyon').
card_number('banishing knack'/'EVE', '17').
card_flavor_text('banishing knack'/'EVE', 'Suddenly he realized that everything was connected by invisible strings, strings that could be pulled—or cut.').
card_multiverse_id('banishing knack'/'EVE', '154350').

card_in_set('battlegate mimic', 'EVE').
card_original_type('battlegate mimic'/'EVE', 'Creature — Shapeshifter').
card_original_text('battlegate mimic'/'EVE', 'Whenever you play a spell that\'s both red and white, Battlegate Mimic becomes 4/2 and gains first strike until end of turn.').
card_first_print('battlegate mimic', 'EVE').
card_image_name('battlegate mimic'/'EVE', 'battlegate mimic').
card_uid('battlegate mimic'/'EVE', 'EVE:Battlegate Mimic:battlegate mimic').
card_rarity('battlegate mimic'/'EVE', 'Common').
card_artist('battlegate mimic'/'EVE', 'Franz Vohwinkel').
card_number('battlegate mimic'/'EVE', '133').
card_flavor_text('battlegate mimic'/'EVE', 'Mimics don\'t need perfect disguises. They need only the perfect victims: the naive, the young, or the poor of sight.').
card_multiverse_id('battlegate mimic'/'EVE', '151165').

card_in_set('batwing brume', 'EVE').
card_original_type('batwing brume'/'EVE', 'Instant').
card_original_text('batwing brume'/'EVE', 'Prevent all combat damage that would be dealt this turn if {W} was spent to play Batwing Brume. Each player loses 1 life for each attacking creature he or she controls if {B} was spent to play Batwing Brume. (Do both if {W}{B} was spent.)').
card_first_print('batwing brume', 'EVE').
card_image_name('batwing brume'/'EVE', 'batwing brume').
card_uid('batwing brume'/'EVE', 'EVE:Batwing Brume:batwing brume').
card_rarity('batwing brume'/'EVE', 'Uncommon').
card_artist('batwing brume'/'EVE', 'Richard Kane Ferguson').
card_number('batwing brume'/'EVE', '81').
card_multiverse_id('batwing brume'/'EVE', '157407').

card_in_set('beckon apparition', 'EVE').
card_original_type('beckon apparition'/'EVE', 'Instant').
card_original_text('beckon apparition'/'EVE', 'Remove target card in a graveyard from the game. Put a 1/1 white and black Spirit creature token with flying into play.').
card_first_print('beckon apparition', 'EVE').
card_image_name('beckon apparition'/'EVE', 'beckon apparition').
card_uid('beckon apparition'/'EVE', 'EVE:Beckon Apparition:beckon apparition').
card_rarity('beckon apparition'/'EVE', 'Common').
card_artist('beckon apparition'/'EVE', 'Larry MacDougall').
card_number('beckon apparition'/'EVE', '82').
card_flavor_text('beckon apparition'/'EVE', 'When a graveyard is full, the gwyllions rattle the tombs and unleash the spirits into the sky.').
card_multiverse_id('beckon apparition'/'EVE', '157415').

card_in_set('belligerent hatchling', 'EVE').
card_original_type('belligerent hatchling'/'EVE', 'Creature — Elemental').
card_original_text('belligerent hatchling'/'EVE', 'First strike\nBelligerent Hatchling comes into play with four -1/-1 counters on it.\nWhenever you play a red spell, remove a -1/-1 counter from Belligerent Hatchling.\nWhenever you play a white spell, remove a -1/-1 counter from Belligerent Hatchling.').
card_first_print('belligerent hatchling', 'EVE').
card_image_name('belligerent hatchling'/'EVE', 'belligerent hatchling').
card_uid('belligerent hatchling'/'EVE', 'EVE:Belligerent Hatchling:belligerent hatchling').
card_rarity('belligerent hatchling'/'EVE', 'Uncommon').
card_artist('belligerent hatchling'/'EVE', 'Steve Prescott').
card_number('belligerent hatchling'/'EVE', '134').
card_multiverse_id('belligerent hatchling'/'EVE', '153478').

card_in_set('bloodied ghost', 'EVE').
card_original_type('bloodied ghost'/'EVE', 'Creature — Spirit').
card_original_text('bloodied ghost'/'EVE', 'Flying\nBloodied Ghost comes into play with a -1/-1 counter on it.').
card_first_print('bloodied ghost', 'EVE').
card_image_name('bloodied ghost'/'EVE', 'bloodied ghost').
card_uid('bloodied ghost'/'EVE', 'EVE:Bloodied Ghost:bloodied ghost').
card_rarity('bloodied ghost'/'EVE', 'Uncommon').
card_artist('bloodied ghost'/'EVE', 'Mike Dringenberg').
card_number('bloodied ghost'/'EVE', '83').
card_flavor_text('bloodied ghost'/'EVE', '\"Only a knife left for a fortnight in the hollow of a ghastbark tree can inflict a wound so grave.\"\n—Kithkin superstition').
card_multiverse_id('bloodied ghost'/'EVE', '151098').

card_in_set('bloom tender', 'EVE').
card_original_type('bloom tender'/'EVE', 'Creature — Elf Druid').
card_original_text('bloom tender'/'EVE', '{T}: For each color among permanents you control, add one mana of that color to your mana pool.').
card_first_print('bloom tender', 'EVE').
card_image_name('bloom tender'/'EVE', 'bloom tender').
card_uid('bloom tender'/'EVE', 'EVE:Bloom Tender:bloom tender').
card_rarity('bloom tender'/'EVE', 'Rare').
card_artist('bloom tender'/'EVE', 'Chippy').
card_number('bloom tender'/'EVE', '66').
card_flavor_text('bloom tender'/'EVE', 'Beauty is a seed, waiting to blossom under her capable hands.').
card_multiverse_id('bloom tender'/'EVE', '158901').

card_in_set('cache raiders', 'EVE').
card_original_type('cache raiders'/'EVE', 'Creature — Merfolk Rogue').
card_original_text('cache raiders'/'EVE', 'At the beginning of your upkeep, return a permanent you control to its owner\'s hand.').
card_first_print('cache raiders', 'EVE').
card_image_name('cache raiders'/'EVE', 'cache raiders').
card_uid('cache raiders'/'EVE', 'EVE:Cache Raiders:cache raiders').
card_rarity('cache raiders'/'EVE', 'Uncommon').
card_artist('cache raiders'/'EVE', 'Pete Venters').
card_number('cache raiders'/'EVE', '18').
card_flavor_text('cache raiders'/'EVE', 'What the current doesn\'t take, they do.').
card_multiverse_id('cache raiders'/'EVE', '153419').

card_in_set('call the skybreaker', 'EVE').
card_original_type('call the skybreaker'/'EVE', 'Sorcery').
card_original_text('call the skybreaker'/'EVE', 'Put a 5/5 blue and red Elemental creature token with flying into play.\nRetrace (You may play this card from your graveyard by discarding a land card in addition to paying its other costs.)').
card_first_print('call the skybreaker', 'EVE').
card_image_name('call the skybreaker'/'EVE', 'call the skybreaker').
card_uid('call the skybreaker'/'EVE', 'EVE:Call the Skybreaker:call the skybreaker').
card_rarity('call the skybreaker'/'EVE', 'Rare').
card_artist('call the skybreaker'/'EVE', 'Randy Gallegos').
card_number('call the skybreaker'/'EVE', '98').
card_flavor_text('call the skybreaker'/'EVE', 'It hears and answers every orison across Shadowmoor.').
card_multiverse_id('call the skybreaker'/'EVE', '157400').

card_in_set('canker abomination', 'EVE').
card_original_type('canker abomination'/'EVE', 'Creature — Treefolk Horror').
card_original_text('canker abomination'/'EVE', 'As Canker Abomination comes into play, choose an opponent. Canker Abomination comes into play with a -1/-1 counter on it for each creature that player controls.').
card_first_print('canker abomination', 'EVE').
card_image_name('canker abomination'/'EVE', 'canker abomination').
card_uid('canker abomination'/'EVE', 'EVE:Canker Abomination:canker abomination').
card_rarity('canker abomination'/'EVE', 'Uncommon').
card_artist('canker abomination'/'EVE', 'Brandon Kitkouski').
card_number('canker abomination'/'EVE', '115').
card_flavor_text('canker abomination'/'EVE', 'Once the hideous disease takes hold, it spreads through both bark and mind.').
card_multiverse_id('canker abomination'/'EVE', '152033').

card_in_set('cankerous thirst', 'EVE').
card_original_type('cankerous thirst'/'EVE', 'Instant').
card_original_text('cankerous thirst'/'EVE', 'If {B} was spent to play Cankerous Thirst, you may have target creature get -3/-3 until end of turn. If {G} was spent to play Cankerous Thirst, you may have target creature get +3/+3 until end of turn. (Do both if {B}{G} was spent.)').
card_first_print('cankerous thirst', 'EVE').
card_image_name('cankerous thirst'/'EVE', 'cankerous thirst').
card_uid('cankerous thirst'/'EVE', 'EVE:Cankerous Thirst:cankerous thirst').
card_rarity('cankerous thirst'/'EVE', 'Uncommon').
card_artist('cankerous thirst'/'EVE', 'Carl Frank').
card_number('cankerous thirst'/'EVE', '116').
card_flavor_text('cankerous thirst'/'EVE', 'Nothing warms the bark on a chilly day like a draught of elf.').
card_multiverse_id('cankerous thirst'/'EVE', '157393').

card_in_set('cascade bluffs', 'EVE').
card_original_type('cascade bluffs'/'EVE', 'Land').
card_original_text('cascade bluffs'/'EVE', '{T}: Add {1} to your mana pool.\n{U/R}, {T}: Add {U}{U}, {U}{R}, or {R}{R} to your mana pool.').
card_first_print('cascade bluffs', 'EVE').
card_image_name('cascade bluffs'/'EVE', 'cascade bluffs').
card_uid('cascade bluffs'/'EVE', 'EVE:Cascade Bluffs:cascade bluffs').
card_rarity('cascade bluffs'/'EVE', 'Rare').
card_artist('cascade bluffs'/'EVE', 'Brandon Kitkouski').
card_number('cascade bluffs'/'EVE', '175').
card_flavor_text('cascade bluffs'/'EVE', 'Travelers bathe in the falls to wash away curses from the pucas who lurk nearby.').
card_multiverse_id('cascade bluffs'/'EVE', '153433').

card_in_set('cauldron haze', 'EVE').
card_original_type('cauldron haze'/'EVE', 'Instant').
card_original_text('cauldron haze'/'EVE', 'Choose any number of target creatures. Each of those creatures gains persist until end of turn. (When it\'s put into a graveyard from play, if it had no -1/-1 counters on it, return it to play under its owner\'s control with a -1/-1 counter on it.)').
card_first_print('cauldron haze', 'EVE').
card_image_name('cauldron haze'/'EVE', 'cauldron haze').
card_uid('cauldron haze'/'EVE', 'EVE:Cauldron Haze:cauldron haze').
card_rarity('cauldron haze'/'EVE', 'Uncommon').
card_artist('cauldron haze'/'EVE', 'Brandon Kitkouski').
card_number('cauldron haze'/'EVE', '84').
card_multiverse_id('cauldron haze'/'EVE', '151060').

card_in_set('cenn\'s enlistment', 'EVE').
card_original_type('cenn\'s enlistment'/'EVE', 'Sorcery').
card_original_text('cenn\'s enlistment'/'EVE', 'Put two 1/1 white Kithkin Soldier creature tokens into play.\nRetrace (You may play this card from your graveyard by discarding a land card in addition to paying its other costs.)').
card_first_print('cenn\'s enlistment', 'EVE').
card_image_name('cenn\'s enlistment'/'EVE', 'cenn\'s enlistment').
card_uid('cenn\'s enlistment'/'EVE', 'EVE:Cenn\'s Enlistment:cenn\'s enlistment').
card_rarity('cenn\'s enlistment'/'EVE', 'Common').
card_artist('cenn\'s enlistment'/'EVE', 'Matt Cavotta').
card_number('cenn\'s enlistment'/'EVE', '3').
card_flavor_text('cenn\'s enlistment'/'EVE', 'The more the scarier.').
card_multiverse_id('cenn\'s enlistment'/'EVE', '153469').

card_in_set('chaotic backlash', 'EVE').
card_original_type('chaotic backlash'/'EVE', 'Instant').
card_original_text('chaotic backlash'/'EVE', 'Chaotic Backlash deals damage to target player equal to twice the number of white and/or blue permanents he or she controls.').
card_first_print('chaotic backlash', 'EVE').
card_image_name('chaotic backlash'/'EVE', 'chaotic backlash').
card_uid('chaotic backlash'/'EVE', 'EVE:Chaotic Backlash:chaotic backlash').
card_rarity('chaotic backlash'/'EVE', 'Uncommon').
card_artist('chaotic backlash'/'EVE', 'Carl Frank').
card_number('chaotic backlash'/'EVE', '49').
card_flavor_text('chaotic backlash'/'EVE', 'The concept of thoughtweft fascinates the Nighthearth cult. Imagine, bringing agony to an entire people by torturing just one.').
card_multiverse_id('chaotic backlash'/'EVE', '158903').

card_in_set('cinder pyromancer', 'EVE').
card_original_type('cinder pyromancer'/'EVE', 'Creature — Elemental Shaman').
card_original_text('cinder pyromancer'/'EVE', '{T}: Cinder Pyromancer deals 1 damage to target player.\nWhenever you play a red spell, you may untap Cinder Pyromancer.').
card_first_print('cinder pyromancer', 'EVE').
card_image_name('cinder pyromancer'/'EVE', 'cinder pyromancer').
card_uid('cinder pyromancer'/'EVE', 'EVE:Cinder Pyromancer:cinder pyromancer').
card_rarity('cinder pyromancer'/'EVE', 'Common').
card_artist('cinder pyromancer'/'EVE', 'Greg Staples').
card_number('cinder pyromancer'/'EVE', '50').
card_flavor_text('cinder pyromancer'/'EVE', '\"If the whole world burns, I\'ll never grow cold.\"').
card_multiverse_id('cinder pyromancer'/'EVE', '151067').

card_in_set('clout of the dominus', 'EVE').
card_original_type('clout of the dominus'/'EVE', 'Enchantment — Aura').
card_original_text('clout of the dominus'/'EVE', 'Enchant creature\nAs long as enchanted creature is blue, it gets +1/+1 and has shroud. (It can\'t be the target of spells or abilities.)\nAs long as enchanted creature is red, it gets +1/+1 and has haste.').
card_first_print('clout of the dominus', 'EVE').
card_image_name('clout of the dominus'/'EVE', 'clout of the dominus').
card_uid('clout of the dominus'/'EVE', 'EVE:Clout of the Dominus:clout of the dominus').
card_rarity('clout of the dominus'/'EVE', 'Common').
card_artist('clout of the dominus'/'EVE', 'Jaime Jones').
card_number('clout of the dominus'/'EVE', '99').
card_multiverse_id('clout of the dominus'/'EVE', '152092').

card_in_set('cold-eyed selkie', 'EVE').
card_original_type('cold-eyed selkie'/'EVE', 'Creature — Merfolk Rogue').
card_original_text('cold-eyed selkie'/'EVE', 'Islandwalk\nWhenever Cold-Eyed Selkie deals combat damage to a player, you may draw that many cards.').
card_first_print('cold-eyed selkie', 'EVE').
card_image_name('cold-eyed selkie'/'EVE', 'cold-eyed selkie').
card_uid('cold-eyed selkie'/'EVE', 'EVE:Cold-Eyed Selkie:cold-eyed selkie').
card_rarity('cold-eyed selkie'/'EVE', 'Rare').
card_artist('cold-eyed selkie'/'EVE', 'Jaime Jones').
card_number('cold-eyed selkie'/'EVE', '149').
card_flavor_text('cold-eyed selkie'/'EVE', 'She follows the old tributaries built when merrows still shaped the tides.').
card_multiverse_id('cold-eyed selkie'/'EVE', '153471').

card_in_set('crackleburr', 'EVE').
card_original_type('crackleburr'/'EVE', 'Creature — Elemental').
card_original_text('crackleburr'/'EVE', '{U/R}{U/R}, {T}, Tap two untapped red creatures you control: Crackleburr deals 3 damage to target creature or player.\n{U/R}{U/R}, {Q}, Untap two tapped blue creatures you control: Return target creature to its owner\'s hand. ({Q} is the untap symbol.)').
card_first_print('crackleburr', 'EVE').
card_image_name('crackleburr'/'EVE', 'crackleburr').
card_uid('crackleburr'/'EVE', 'EVE:Crackleburr:crackleburr').
card_rarity('crackleburr'/'EVE', 'Rare').
card_artist('crackleburr'/'EVE', 'Mike Dringenberg').
card_number('crackleburr'/'EVE', '100').
card_multiverse_id('crackleburr'/'EVE', '157420').

card_in_set('crag puca', 'EVE').
card_original_type('crag puca'/'EVE', 'Creature — Shapeshifter').
card_original_text('crag puca'/'EVE', '{U/R}: Switch Crag Puca\'s power and toughness until end of turn.').
card_first_print('crag puca', 'EVE').
card_image_name('crag puca'/'EVE', 'crag puca').
card_uid('crag puca'/'EVE', 'EVE:Crag Puca:crag puca').
card_rarity('crag puca'/'EVE', 'Uncommon').
card_artist('crag puca'/'EVE', 'John Howe').
card_number('crag puca'/'EVE', '101').
card_flavor_text('crag puca'/'EVE', '\"Be happy when you see a puca shaped like mischief. It\'s far better than one shaped like death.\"\n—Dindun of Kulrath Mine').
card_multiverse_id('crag puca'/'EVE', '157285').

card_in_set('creakwood ghoul', 'EVE').
card_original_type('creakwood ghoul'/'EVE', 'Creature — Plant Zombie').
card_original_text('creakwood ghoul'/'EVE', '{B/G}{B/G}: Remove target card in a graveyard from the game. You gain 1 life.').
card_first_print('creakwood ghoul', 'EVE').
card_image_name('creakwood ghoul'/'EVE', 'creakwood ghoul').
card_uid('creakwood ghoul'/'EVE', 'EVE:Creakwood Ghoul:creakwood ghoul').
card_rarity('creakwood ghoul'/'EVE', 'Uncommon').
card_artist('creakwood ghoul'/'EVE', 'Thomas M. Baxa').
card_number('creakwood ghoul'/'EVE', '34').
card_flavor_text('creakwood ghoul'/'EVE', 'Some eat the living. Some eat the dead. It eats the past.').
card_multiverse_id('creakwood ghoul'/'EVE', '157206').

card_in_set('creakwood liege', 'EVE').
card_original_type('creakwood liege'/'EVE', 'Creature — Horror').
card_original_text('creakwood liege'/'EVE', 'Other black creatures you control get +1/+1.\nOther green creatures you control get +1/+1.\nAt the beginning of your upkeep, you may put a 1/1 black and green Worm creature token into play.').
card_first_print('creakwood liege', 'EVE').
card_image_name('creakwood liege'/'EVE', 'creakwood liege').
card_uid('creakwood liege'/'EVE', 'EVE:Creakwood Liege:creakwood liege').
card_rarity('creakwood liege'/'EVE', 'Rare').
card_artist('creakwood liege'/'EVE', 'Cole Eastburn').
card_number('creakwood liege'/'EVE', '117').
card_multiverse_id('creakwood liege'/'EVE', '157406').

card_in_set('crumbling ashes', 'EVE').
card_original_type('crumbling ashes'/'EVE', 'Enchantment').
card_original_text('crumbling ashes'/'EVE', 'At the beginning of your upkeep, destroy target creature with a -1/-1 counter on it.').
card_first_print('crumbling ashes', 'EVE').
card_image_name('crumbling ashes'/'EVE', 'crumbling ashes').
card_uid('crumbling ashes'/'EVE', 'EVE:Crumbling Ashes:crumbling ashes').
card_rarity('crumbling ashes'/'EVE', 'Uncommon').
card_artist('crumbling ashes'/'EVE', 'Chuck Lukacs').
card_number('crumbling ashes'/'EVE', '35').
card_flavor_text('crumbling ashes'/'EVE', '\"I was there to watch my brothers\' quenching. I drew the slightest warmth from their dying rage.\"\n—Illulia of Nighthearth').
card_multiverse_id('crumbling ashes'/'EVE', '158103').

card_in_set('deathbringer liege', 'EVE').
card_original_type('deathbringer liege'/'EVE', 'Creature — Horror').
card_original_text('deathbringer liege'/'EVE', 'Other white creatures you control get +1/+1.\nOther black creatures you control get +1/+1.\nWhenever you play a white spell, you may tap target creature.\nWhenever you play a black spell, you may destroy target creature if it\'s tapped.').
card_first_print('deathbringer liege', 'EVE').
card_image_name('deathbringer liege'/'EVE', 'deathbringer liege').
card_uid('deathbringer liege'/'EVE', 'EVE:Deathbringer Liege:deathbringer liege').
card_rarity('deathbringer liege'/'EVE', 'Rare').
card_artist('deathbringer liege'/'EVE', 'Drew Tucker').
card_number('deathbringer liege'/'EVE', '85').
card_multiverse_id('deathbringer liege'/'EVE', '157288').

card_in_set('deity of scars', 'EVE').
card_original_type('deity of scars'/'EVE', 'Creature — Spirit Avatar').
card_original_text('deity of scars'/'EVE', 'Trample\nDeity of Scars comes into play with two -1/-1 counters on it.\n{B/G}, Remove a -1/-1 counter from Deity of Scars: Regenerate Deity of Scars.').
card_first_print('deity of scars', 'EVE').
card_image_name('deity of scars'/'EVE', 'deity of scars').
card_uid('deity of scars'/'EVE', 'EVE:Deity of Scars:deity of scars').
card_rarity('deity of scars'/'EVE', 'Rare').
card_artist('deity of scars'/'EVE', 'Steve Prescott').
card_number('deity of scars'/'EVE', '118').
card_flavor_text('deity of scars'/'EVE', '\"His skin cuts swords.\"\n—The Seer\'s Parables').
card_multiverse_id('deity of scars'/'EVE', '153488').

card_in_set('desecrator hag', 'EVE').
card_original_type('desecrator hag'/'EVE', 'Creature — Hag').
card_original_text('desecrator hag'/'EVE', 'When Desecrator Hag comes into play, return to your hand the creature card in your graveyard with the highest power. If two or more cards are tied for highest power, you choose one of them.').
card_first_print('desecrator hag', 'EVE').
card_image_name('desecrator hag'/'EVE', 'desecrator hag').
card_uid('desecrator hag'/'EVE', 'EVE:Desecrator Hag:desecrator hag').
card_rarity('desecrator hag'/'EVE', 'Common').
card_artist('desecrator hag'/'EVE', 'Fred Harper').
card_number('desecrator hag'/'EVE', '119').
card_flavor_text('desecrator hag'/'EVE', 'Loose earth is not a burial but an appetizer.').
card_multiverse_id('desecrator hag'/'EVE', '157419').

card_in_set('divinity of pride', 'EVE').
card_original_type('divinity of pride'/'EVE', 'Creature — Spirit Avatar').
card_original_text('divinity of pride'/'EVE', 'Flying, lifelink\nDivinity of Pride gets +4/+4 as long as you have 25 or more life.').
card_first_print('divinity of pride', 'EVE').
card_image_name('divinity of pride'/'EVE', 'divinity of pride').
card_uid('divinity of pride'/'EVE', 'EVE:Divinity of Pride:divinity of pride').
card_rarity('divinity of pride'/'EVE', 'Rare').
card_artist('divinity of pride'/'EVE', 'Greg Staples').
card_number('divinity of pride'/'EVE', '86').
card_flavor_text('divinity of pride'/'EVE', '\"She spoke of mystery and portent, of such unfathomable things that my mind screamed for pity.\"\n—The Seer\'s Parables').
card_multiverse_id('divinity of pride'/'EVE', '150995').

card_in_set('dominus of fealty', 'EVE').
card_original_type('dominus of fealty'/'EVE', 'Creature — Spirit Avatar').
card_original_text('dominus of fealty'/'EVE', 'Flying\nAt the beginning of your upkeep, you may gain control of target permanent until end of turn. If you do, untap it and it gains haste until end of turn.').
card_first_print('dominus of fealty', 'EVE').
card_image_name('dominus of fealty'/'EVE', 'dominus of fealty').
card_uid('dominus of fealty'/'EVE', 'EVE:Dominus of Fealty:dominus of fealty').
card_rarity('dominus of fealty'/'EVE', 'Rare').
card_artist('dominus of fealty'/'EVE', 'Kev Walker').
card_number('dominus of fealty'/'EVE', '102').
card_flavor_text('dominus of fealty'/'EVE', '\"Nothing is truly your own. It is his, whether you know it or not.\"\n—The Seer\'s Parables').
card_multiverse_id('dominus of fealty'/'EVE', '151154').

card_in_set('doomgape', 'EVE').
card_original_type('doomgape'/'EVE', 'Creature — Elemental').
card_original_text('doomgape'/'EVE', 'Trample\nAt the beginning of your upkeep, sacrifice a creature. You gain life equal to that creature\'s toughness.').
card_first_print('doomgape', 'EVE').
card_image_name('doomgape'/'EVE', 'doomgape').
card_uid('doomgape'/'EVE', 'EVE:Doomgape:doomgape').
card_rarity('doomgape'/'EVE', 'Rare').
card_artist('doomgape'/'EVE', 'Dave Allsop').
card_number('doomgape'/'EVE', '120').
card_flavor_text('doomgape'/'EVE', 'It eats and eats until the juiciest morsel it can find is itself.').
card_multiverse_id('doomgape'/'EVE', '146010').

card_in_set('double cleave', 'EVE').
card_original_type('double cleave'/'EVE', 'Instant').
card_original_text('double cleave'/'EVE', 'Target creature gains double strike until end of turn. (It deals both first-strike and regular combat damage.)').
card_first_print('double cleave', 'EVE').
card_image_name('double cleave'/'EVE', 'double cleave').
card_uid('double cleave'/'EVE', 'EVE:Double Cleave:double cleave').
card_rarity('double cleave'/'EVE', 'Common').
card_artist('double cleave'/'EVE', 'rk post').
card_number('double cleave'/'EVE', '135').
card_flavor_text('double cleave'/'EVE', '\"When in doubt, kill \'em twice.\"').
card_multiverse_id('double cleave'/'EVE', '153039').

card_in_set('drain the well', 'EVE').
card_original_type('drain the well'/'EVE', 'Sorcery').
card_original_text('drain the well'/'EVE', 'Destroy target land. You gain 2 life.').
card_first_print('drain the well', 'EVE').
card_image_name('drain the well'/'EVE', 'drain the well').
card_uid('drain the well'/'EVE', 'EVE:Drain the Well:drain the well').
card_rarity('drain the well'/'EVE', 'Common').
card_artist('drain the well'/'EVE', 'Warren Mahy').
card_number('drain the well'/'EVE', '121').
card_flavor_text('drain the well'/'EVE', 'Trows have learned that it\'s not wise to eat cinders without a bucket or two of water nearby.').
card_multiverse_id('drain the well'/'EVE', '151079').

card_in_set('dream fracture', 'EVE').
card_original_type('dream fracture'/'EVE', 'Instant').
card_original_text('dream fracture'/'EVE', 'Counter target spell. Its controller draws a card.\nDraw a card.').
card_first_print('dream fracture', 'EVE').
card_image_name('dream fracture'/'EVE', 'dream fracture').
card_uid('dream fracture'/'EVE', 'EVE:Dream Fracture:dream fracture').
card_rarity('dream fracture'/'EVE', 'Uncommon').
card_artist('dream fracture'/'EVE', 'Howard Lyon').
card_number('dream fracture'/'EVE', '19').
card_flavor_text('dream fracture'/'EVE', '\"Creation is a paradox. It hatches from its opposite.\"\n—Olka, Mistmeadow witch').
card_multiverse_id('dream fracture'/'EVE', '153036').

card_in_set('dream thief', 'EVE').
card_original_type('dream thief'/'EVE', 'Creature — Faerie Rogue').
card_original_text('dream thief'/'EVE', 'Flying\nWhen Dream Thief comes into play, draw a card if you played another blue spell this turn.').
card_first_print('dream thief', 'EVE').
card_image_name('dream thief'/'EVE', 'dream thief').
card_uid('dream thief'/'EVE', 'EVE:Dream Thief:dream thief').
card_rarity('dream thief'/'EVE', 'Common').
card_artist('dream thief'/'EVE', 'Howard Lyon').
card_number('dream thief'/'EVE', '20').
card_flavor_text('dream thief'/'EVE', 'Awylla\'s salvaged dreams would finally be put to good use.').
card_multiverse_id('dream thief'/'EVE', '157207').

card_in_set('duergar assailant', 'EVE').
card_original_type('duergar assailant'/'EVE', 'Creature — Dwarf Soldier').
card_original_text('duergar assailant'/'EVE', 'Sacrifice Duergar Assailant: Duergar Assailant deals 1 damage to target attacking or blocking creature.').
card_first_print('duergar assailant', 'EVE').
card_image_name('duergar assailant'/'EVE', 'duergar assailant').
card_uid('duergar assailant'/'EVE', 'EVE:Duergar Assailant:duergar assailant').
card_rarity('duergar assailant'/'EVE', 'Common').
card_artist('duergar assailant'/'EVE', 'Paul Bonner').
card_number('duergar assailant'/'EVE', '136').
card_flavor_text('duergar assailant'/'EVE', 'Some duergars dismiss the world above as a fable. They react violently when their comfortable illusions are dispelled.').
card_multiverse_id('duergar assailant'/'EVE', '150975').

card_in_set('duergar cave-guard', 'EVE').
card_original_type('duergar cave-guard'/'EVE', 'Creature — Dwarf Warrior').
card_original_text('duergar cave-guard'/'EVE', 'Wither (This deals damage to creatures in the form of -1/-1 counters.)\n{R/W}: Duergar Cave-Guard gets +1/+0 until end of turn.').
card_first_print('duergar cave-guard', 'EVE').
card_image_name('duergar cave-guard'/'EVE', 'duergar cave-guard').
card_uid('duergar cave-guard'/'EVE', 'EVE:Duergar Cave-Guard:duergar cave-guard').
card_rarity('duergar cave-guard'/'EVE', 'Uncommon').
card_artist('duergar cave-guard'/'EVE', 'Dave Allsop').
card_number('duergar cave-guard'/'EVE', '51').
card_flavor_text('duergar cave-guard'/'EVE', 'Duergars who grow too large to live in the tunnels are sent topside to defend the entrances from intruders.').
card_multiverse_id('duergar cave-guard'/'EVE', '153437').

card_in_set('duergar hedge-mage', 'EVE').
card_original_type('duergar hedge-mage'/'EVE', 'Creature — Dwarf Shaman').
card_original_text('duergar hedge-mage'/'EVE', 'When Duergar Hedge-Mage comes into play, if you control two or more Mountains, you may destroy target artifact.\nWhen Duergar Hedge-Mage comes into play, if you control two or more Plains, you may destroy target enchantment.').
card_image_name('duergar hedge-mage'/'EVE', 'duergar hedge-mage').
card_uid('duergar hedge-mage'/'EVE', 'EVE:Duergar Hedge-Mage:duergar hedge-mage').
card_rarity('duergar hedge-mage'/'EVE', 'Uncommon').
card_artist('duergar hedge-mage'/'EVE', 'Dave Allsop').
card_number('duergar hedge-mage'/'EVE', '137').
card_multiverse_id('duergar hedge-mage'/'EVE', '159068').

card_in_set('duergar mine-captain', 'EVE').
card_original_type('duergar mine-captain'/'EVE', 'Creature — Dwarf Soldier').
card_original_text('duergar mine-captain'/'EVE', '{1}{R/W}, {Q}: Attacking creatures get +1/+0 until end of turn. ({Q} is the untap symbol.)').
card_first_print('duergar mine-captain', 'EVE').
card_image_name('duergar mine-captain'/'EVE', 'duergar mine-captain').
card_uid('duergar mine-captain'/'EVE', 'EVE:Duergar Mine-Captain:duergar mine-captain').
card_rarity('duergar mine-captain'/'EVE', 'Uncommon').
card_artist('duergar mine-captain'/'EVE', 'Matt Cavotta').
card_number('duergar mine-captain'/'EVE', '138').
card_flavor_text('duergar mine-captain'/'EVE', 'Duergars are prodigious workers with long memories. Some still fulfill intricately detailed orders given decades, if not centuries, before.').
card_multiverse_id('duergar mine-captain'/'EVE', '151106').

card_in_set('duskdale wurm', 'EVE').
card_original_type('duskdale wurm'/'EVE', 'Creature — Wurm').
card_original_text('duskdale wurm'/'EVE', 'Trample').
card_first_print('duskdale wurm', 'EVE').
card_image_name('duskdale wurm'/'EVE', 'duskdale wurm').
card_uid('duskdale wurm'/'EVE', 'EVE:Duskdale Wurm:duskdale wurm').
card_rarity('duskdale wurm'/'EVE', 'Uncommon').
card_artist('duskdale wurm'/'EVE', 'Dan Dos Santos').
card_number('duskdale wurm'/'EVE', '67').
card_flavor_text('duskdale wurm'/'EVE', '\"Last time, it tore up the Wilt-Leaf, turned Mistmeadow into a mudhole, and made the river jump its banks. On the bright side, we were eating venison for weeks.\"\n—Donal Alloway, cenn of Kinscaer').
card_multiverse_id('duskdale wurm'/'EVE', '151164').

card_in_set('edge of the divinity', 'EVE').
card_original_type('edge of the divinity'/'EVE', 'Enchantment — Aura').
card_original_text('edge of the divinity'/'EVE', 'Enchant creature\nAs long as enchanted creature is white, it gets +1/+2.\nAs long as enchanted creature is black, it gets +2/+1.').
card_first_print('edge of the divinity', 'EVE').
card_image_name('edge of the divinity'/'EVE', 'edge of the divinity').
card_uid('edge of the divinity'/'EVE', 'EVE:Edge of the Divinity:edge of the divinity').
card_rarity('edge of the divinity'/'EVE', 'Common').
card_artist('edge of the divinity'/'EVE', 'Dan Scott').
card_number('edge of the divinity'/'EVE', '87').
card_flavor_text('edge of the divinity'/'EVE', 'The divinity\'s patronage confers both wicked grace and graceful wickedness.').
card_multiverse_id('edge of the divinity'/'EVE', '151155').

card_in_set('endless horizons', 'EVE').
card_original_type('endless horizons'/'EVE', 'Enchantment').
card_original_text('endless horizons'/'EVE', 'When Endless Horizons comes into play, search your library for any number of Plains cards and remove them from the game. Then shuffle your library.\nAt the beginning of your upkeep, you may put a card you own removed from the game with Endless Horizons into your hand.').
card_first_print('endless horizons', 'EVE').
card_image_name('endless horizons'/'EVE', 'endless horizons').
card_uid('endless horizons'/'EVE', 'EVE:Endless Horizons:endless horizons').
card_rarity('endless horizons'/'EVE', 'Rare').
card_artist('endless horizons'/'EVE', 'Joshua Hagler').
card_number('endless horizons'/'EVE', '4').
card_multiverse_id('endless horizons'/'EVE', '157975').

card_in_set('endure', 'EVE').
card_original_type('endure'/'EVE', 'Instant').
card_original_text('endure'/'EVE', 'Prevent all damage that would be dealt to you and permanents you control this turn.').
card_first_print('endure', 'EVE').
card_image_name('endure'/'EVE', 'endure').
card_uid('endure'/'EVE', 'EVE:Endure:endure').
card_rarity('endure'/'EVE', 'Uncommon').
card_artist('endure'/'EVE', 'Ralph Horsley').
card_number('endure'/'EVE', '5').
card_flavor_text('endure'/'EVE', 'The Aurora had lessened the kithkins\' kindness, deepened their paranoia, and dulled their sense of pain.').
card_multiverse_id('endure'/'EVE', '157199').

card_in_set('evershrike', 'EVE').
card_original_type('evershrike'/'EVE', 'Creature — Elemental Spirit').
card_original_text('evershrike'/'EVE', 'Flying\nEvershrike gets +2/+2 for each Aura attached to it.\n{X}{W/B}{W/B}: Return Evershrike from your graveyard to play. You may put an Aura card with converted mana cost X or less from your hand into play attached to it. If you don\'t, remove Evershrike from the game.').
card_first_print('evershrike', 'EVE').
card_image_name('evershrike'/'EVE', 'evershrike').
card_uid('evershrike'/'EVE', 'EVE:Evershrike:evershrike').
card_rarity('evershrike'/'EVE', 'Rare').
card_artist('evershrike'/'EVE', 'Dan Dos Santos').
card_number('evershrike'/'EVE', '88').
card_multiverse_id('evershrike'/'EVE', '157978').

card_in_set('fable of wolf and owl', 'EVE').
card_original_type('fable of wolf and owl'/'EVE', 'Enchantment').
card_original_text('fable of wolf and owl'/'EVE', 'Whenever you play a green spell, you may put a 2/2 green Wolf creature token into play.\nWhenever you play a blue spell, you may put a 1/1 blue Bird creature token with flying into play.').
card_first_print('fable of wolf and owl', 'EVE').
card_image_name('fable of wolf and owl'/'EVE', 'fable of wolf and owl').
card_uid('fable of wolf and owl'/'EVE', 'EVE:Fable of Wolf and Owl:fable of wolf and owl').
card_rarity('fable of wolf and owl'/'EVE', 'Rare').
card_artist('fable of wolf and owl'/'EVE', 'Heather Hudson').
card_number('fable of wolf and owl'/'EVE', '150').
card_flavor_text('fable of wolf and owl'/'EVE', 'The forest grew fangs. The sky spread its wings.').
card_multiverse_id('fable of wolf and owl'/'EVE', '152087').

card_in_set('fang skulkin', 'EVE').
card_original_type('fang skulkin'/'EVE', 'Artifact Creature — Scarecrow').
card_original_text('fang skulkin'/'EVE', '{2}: Target black creature gains wither until end of turn. (It deals damage to creatures in the form of -1/-1 counters.)').
card_first_print('fang skulkin', 'EVE').
card_image_name('fang skulkin'/'EVE', 'fang skulkin').
card_uid('fang skulkin'/'EVE', 'EVE:Fang Skulkin:fang skulkin').
card_rarity('fang skulkin'/'EVE', 'Common').
card_artist('fang skulkin'/'EVE', 'Ron Brown').
card_number('fang skulkin'/'EVE', '168').
card_flavor_text('fang skulkin'/'EVE', '\"Everything falls apart. But living things fall apart in the most fascinating ways.\"\n—Mowagh the Gwyllion').
card_multiverse_id('fang skulkin'/'EVE', '158296').

card_in_set('favor of the overbeing', 'EVE').
card_original_type('favor of the overbeing'/'EVE', 'Enchantment — Aura').
card_original_text('favor of the overbeing'/'EVE', 'Enchant creature\nAs long as enchanted creature is green, it gets +1/+1 and has vigilance.\nAs long as enchanted creature is blue, it gets +1/+1 and has flying.').
card_first_print('favor of the overbeing', 'EVE').
card_image_name('favor of the overbeing'/'EVE', 'favor of the overbeing').
card_uid('favor of the overbeing'/'EVE', 'EVE:Favor of the Overbeing:favor of the overbeing').
card_rarity('favor of the overbeing'/'EVE', 'Common').
card_artist('favor of the overbeing'/'EVE', 'Chippy').
card_number('favor of the overbeing'/'EVE', '151').
card_flavor_text('favor of the overbeing'/'EVE', 'Enlightenment is the mundane seen from the vantage point of the divine.').
card_multiverse_id('favor of the overbeing'/'EVE', '151118').

card_in_set('fetid heath', 'EVE').
card_original_type('fetid heath'/'EVE', 'Land').
card_original_text('fetid heath'/'EVE', '{T}: Add {1} to your mana pool.\n{W/B}, {T}: Add {W}{W}, {W}{B}, or {B}{B} to your mana pool.').
card_first_print('fetid heath', 'EVE').
card_image_name('fetid heath'/'EVE', 'fetid heath').
card_uid('fetid heath'/'EVE', 'EVE:Fetid Heath:fetid heath').
card_rarity('fetid heath'/'EVE', 'Rare').
card_artist('fetid heath'/'EVE', 'Daarken').
card_number('fetid heath'/'EVE', '176').
card_flavor_text('fetid heath'/'EVE', '\"Do not linger in such places, child. There the gwyllions dance. If they find you, you will join in their revels for eternity.\"\n—Talara, elvish safewright').
card_multiverse_id('fetid heath'/'EVE', '153446').

card_in_set('fiery bombardment', 'EVE').
card_original_type('fiery bombardment'/'EVE', 'Enchantment').
card_original_text('fiery bombardment'/'EVE', 'Chroma {2}, Sacrifice a creature: Fiery Bombardment deals damage to target creature or player equal to the number of red mana symbols in the sacrificed creature\'s mana cost.').
card_first_print('fiery bombardment', 'EVE').
card_image_name('fiery bombardment'/'EVE', 'fiery bombardment').
card_uid('fiery bombardment'/'EVE', 'EVE:Fiery Bombardment:fiery bombardment').
card_rarity('fiery bombardment'/'EVE', 'Rare').
card_artist('fiery bombardment'/'EVE', 'Nils Hamm').
card_number('fiery bombardment'/'EVE', '52').
card_flavor_text('fiery bombardment'/'EVE', 'It kills two beasts with one thrown.').
card_multiverse_id('fiery bombardment'/'EVE', '157200').

card_in_set('figure of destiny', 'EVE').
card_original_type('figure of destiny'/'EVE', 'Creature — Kithkin').
card_original_text('figure of destiny'/'EVE', '{R/W}: Figure of Destiny becomes a 2/2 Kithkin Spirit.\n{R/W}{R/W}{R/W}: If Figure of Destiny is a Spirit, it becomes a 4/4 Kithkin Spirit Warrior.\n{R/W}{R/W}{R/W}{R/W}{R/W}{R/W}: If Figure of Destiny is a Warrior, it becomes an 8/8 Kithkin Spirit Warrior Avatar with flying and first strike.').
card_image_name('figure of destiny'/'EVE', 'figure of destiny').
card_uid('figure of destiny'/'EVE', 'EVE:Figure of Destiny:figure of destiny').
card_rarity('figure of destiny'/'EVE', 'Rare').
card_artist('figure of destiny'/'EVE', 'Scott M. Fischer').
card_number('figure of destiny'/'EVE', '139').
card_multiverse_id('figure of destiny'/'EVE', '158106').

card_in_set('fire at will', 'EVE').
card_original_type('fire at will'/'EVE', 'Instant').
card_original_text('fire at will'/'EVE', 'Fire at Will deals 3 damage divided as you choose among any number of target attacking or blocking creatures.').
card_first_print('fire at will', 'EVE').
card_image_name('fire at will'/'EVE', 'fire at will').
card_uid('fire at will'/'EVE', 'EVE:Fire at Will:fire at will').
card_rarity('fire at will'/'EVE', 'Common').
card_artist('fire at will'/'EVE', 'Pete Venters').
card_number('fire at will'/'EVE', '140').
card_flavor_text('fire at will'/'EVE', '\"Make these shots count. The more foes we drop now, the fewer we\'ll have trying to kill us later.\"').
card_multiverse_id('fire at will'/'EVE', '151149').

card_in_set('flame jab', 'EVE').
card_original_type('flame jab'/'EVE', 'Sorcery').
card_original_text('flame jab'/'EVE', 'Flame Jab deals 1 damage to target creature or player.\nRetrace (You may play this card from your graveyard by discarding a land card in addition to paying its other costs.)').
card_first_print('flame jab', 'EVE').
card_image_name('flame jab'/'EVE', 'flame jab').
card_uid('flame jab'/'EVE', 'EVE:Flame Jab:flame jab').
card_rarity('flame jab'/'EVE', 'Common').
card_artist('flame jab'/'EVE', 'rk post').
card_number('flame jab'/'EVE', '53').
card_flavor_text('flame jab'/'EVE', '\"Fire\'s out. Should be safe now.\"\n—Gnarltrunk').
card_multiverse_id('flame jab'/'EVE', '153432').

card_in_set('flickerwisp', 'EVE').
card_original_type('flickerwisp'/'EVE', 'Creature — Elemental').
card_original_text('flickerwisp'/'EVE', 'Flying\nWhen Flickerwisp comes into play, remove another target permanent from the game. Return that card to play under its owner\'s control at end of turn.').
card_first_print('flickerwisp', 'EVE').
card_image_name('flickerwisp'/'EVE', 'flickerwisp').
card_uid('flickerwisp'/'EVE', 'EVE:Flickerwisp:flickerwisp').
card_rarity('flickerwisp'/'EVE', 'Uncommon').
card_artist('flickerwisp'/'EVE', 'Jeremy Enecio').
card_number('flickerwisp'/'EVE', '6').
card_flavor_text('flickerwisp'/'EVE', 'Its wings disturb more than air.').
card_multiverse_id('flickerwisp'/'EVE', '151089').

card_in_set('flooded grove', 'EVE').
card_original_type('flooded grove'/'EVE', 'Land').
card_original_text('flooded grove'/'EVE', '{T}: Add {1} to your mana pool.\n{G/U}, {T}: Add {G}{G}, {G}{U}, or {U}{U} to your mana pool.').
card_first_print('flooded grove', 'EVE').
card_image_name('flooded grove'/'EVE', 'flooded grove').
card_uid('flooded grove'/'EVE', 'EVE:Flooded Grove:flooded grove').
card_rarity('flooded grove'/'EVE', 'Rare').
card_artist('flooded grove'/'EVE', 'Dave Kendall').
card_number('flooded grove'/'EVE', '177').
card_flavor_text('flooded grove'/'EVE', 'At full moon, the river seeps over its banks and crawls through the woods like a blanket of serpents.').
card_multiverse_id('flooded grove'/'EVE', '153481').

card_in_set('gift of the deity', 'EVE').
card_original_type('gift of the deity'/'EVE', 'Enchantment — Aura').
card_original_text('gift of the deity'/'EVE', 'Enchant creature\nAs long as enchanted creature is black, it gets +1/+1 and has deathtouch. (Whenever it deals damage to a creature, destroy that creature.)\nAs long as enchanted creature is green, it gets +1/+1 and all creatures able to block it do so.').
card_first_print('gift of the deity', 'EVE').
card_image_name('gift of the deity'/'EVE', 'gift of the deity').
card_uid('gift of the deity'/'EVE', 'EVE:Gift of the Deity:gift of the deity').
card_rarity('gift of the deity'/'EVE', 'Common').
card_artist('gift of the deity'/'EVE', 'Steve Prescott').
card_number('gift of the deity'/'EVE', '122').
card_multiverse_id('gift of the deity'/'EVE', '151153').

card_in_set('gilder bairn', 'EVE').
card_original_type('gilder bairn'/'EVE', 'Creature — Ouphe').
card_original_text('gilder bairn'/'EVE', '{2}{G/U}, {Q}: For each counter on target permanent, put another of those counters on that permanent. ({Q} is the untap symbol.)').
card_first_print('gilder bairn', 'EVE').
card_image_name('gilder bairn'/'EVE', 'gilder bairn').
card_uid('gilder bairn'/'EVE', 'EVE:Gilder Bairn:gilder bairn').
card_rarity('gilder bairn'/'EVE', 'Uncommon').
card_artist('gilder bairn'/'EVE', 'Nils Hamm').
card_number('gilder bairn'/'EVE', '152').
card_flavor_text('gilder bairn'/'EVE', 'Do the glowing trinkets show it the way home, or do they set a twisted path for someone else to follow?').
card_multiverse_id('gilder bairn'/'EVE', '152046').

card_in_set('glamerdye', 'EVE').
card_original_type('glamerdye'/'EVE', 'Instant').
card_original_text('glamerdye'/'EVE', 'Change the text of target spell or permanent by replacing all instances of one color word with another.\nRetrace (You may play this card from your graveyard by discarding a land card in addition to paying its other costs.)').
card_first_print('glamerdye', 'EVE').
card_image_name('glamerdye'/'EVE', 'glamerdye').
card_uid('glamerdye'/'EVE', 'EVE:Glamerdye:glamerdye').
card_rarity('glamerdye'/'EVE', 'Rare').
card_artist('glamerdye'/'EVE', 'Ralph Horsley').
card_number('glamerdye'/'EVE', '21').
card_multiverse_id('glamerdye'/'EVE', '153439').

card_in_set('glen elendra archmage', 'EVE').
card_original_type('glen elendra archmage'/'EVE', 'Creature — Faerie Wizard').
card_original_text('glen elendra archmage'/'EVE', 'Flying\n{U}, Sacrifice Glen Elendra Archmage: Counter target noncreature spell.\nPersist (When this creature is put into a graveyard from play, if it had no -1/-1 counters on it, return it to play under its owner\'s control with a -1/-1 counter on it.)').
card_first_print('glen elendra archmage', 'EVE').
card_image_name('glen elendra archmage'/'EVE', 'glen elendra archmage').
card_uid('glen elendra archmage'/'EVE', 'EVE:Glen Elendra Archmage:glen elendra archmage').
card_rarity('glen elendra archmage'/'EVE', 'Rare').
card_artist('glen elendra archmage'/'EVE', 'Warren Mahy').
card_number('glen elendra archmage'/'EVE', '22').
card_multiverse_id('glen elendra archmage'/'EVE', '157977').

card_in_set('grazing kelpie', 'EVE').
card_original_type('grazing kelpie'/'EVE', 'Creature — Beast').
card_original_text('grazing kelpie'/'EVE', '{G/U}, Sacrifice Grazing Kelpie: Put target card in a graveyard on the bottom of its owner\'s library.\nPersist (When this creature is put into a graveyard from play, if it had no -1/-1 counters on it, return it to play under its owner\'s control with a -1/-1 counter on it.)').
card_first_print('grazing kelpie', 'EVE').
card_image_name('grazing kelpie'/'EVE', 'grazing kelpie').
card_uid('grazing kelpie'/'EVE', 'EVE:Grazing Kelpie:grazing kelpie').
card_rarity('grazing kelpie'/'EVE', 'Common').
card_artist('grazing kelpie'/'EVE', 'Drew Tucker').
card_number('grazing kelpie'/'EVE', '153').
card_multiverse_id('grazing kelpie'/'EVE', '153428').

card_in_set('groundling pouncer', 'EVE').
card_original_type('groundling pouncer'/'EVE', 'Creature — Faerie').
card_original_text('groundling pouncer'/'EVE', '{G/U}: Groundling Pouncer gets +1/+3 and gains flying until end of turn. Play this ability only once each turn and only if an opponent controls a creature with flying.').
card_first_print('groundling pouncer', 'EVE').
card_image_name('groundling pouncer'/'EVE', 'groundling pouncer').
card_uid('groundling pouncer'/'EVE', 'EVE:Groundling Pouncer:groundling pouncer').
card_rarity('groundling pouncer'/'EVE', 'Uncommon').
card_artist('groundling pouncer'/'EVE', 'Richard Whitters').
card_number('groundling pouncer'/'EVE', '154').
card_flavor_text('groundling pouncer'/'EVE', 'Endry was determined to teach his groundlings a few winged-faerie tricks.').
card_multiverse_id('groundling pouncer'/'EVE', '153056').

card_in_set('gwyllion hedge-mage', 'EVE').
card_original_type('gwyllion hedge-mage'/'EVE', 'Creature — Hag Wizard').
card_original_text('gwyllion hedge-mage'/'EVE', 'When Gwyllion Hedge-Mage comes into play, if you control two or more Plains, you may put a 1/1 white Kithkin Soldier creature token into play.\nWhen Gwyllion Hedge-Mage comes into play, if you control two or more Swamps, you may put a -1/-1 counter on target creature.').
card_first_print('gwyllion hedge-mage', 'EVE').
card_image_name('gwyllion hedge-mage'/'EVE', 'gwyllion hedge-mage').
card_uid('gwyllion hedge-mage'/'EVE', 'EVE:Gwyllion Hedge-Mage:gwyllion hedge-mage').
card_rarity('gwyllion hedge-mage'/'EVE', 'Uncommon').
card_artist('gwyllion hedge-mage'/'EVE', 'Todd Lockwood').
card_number('gwyllion hedge-mage'/'EVE', '89').
card_multiverse_id('gwyllion hedge-mage'/'EVE', '159069').

card_in_set('hag hedge-mage', 'EVE').
card_original_type('hag hedge-mage'/'EVE', 'Creature — Hag Shaman').
card_original_text('hag hedge-mage'/'EVE', 'When Hag Hedge-Mage comes into play, if you control two or more Swamps, you may have target player discard a card.\nWhen Hag Hedge-Mage comes into play, if you control two or more Forests, you may put target card in your graveyard on top of your library.').
card_first_print('hag hedge-mage', 'EVE').
card_image_name('hag hedge-mage'/'EVE', 'hag hedge-mage').
card_uid('hag hedge-mage'/'EVE', 'EVE:Hag Hedge-Mage:hag hedge-mage').
card_rarity('hag hedge-mage'/'EVE', 'Uncommon').
card_artist('hag hedge-mage'/'EVE', 'Scott Altmann').
card_number('hag hedge-mage'/'EVE', '123').
card_multiverse_id('hag hedge-mage'/'EVE', '157980').

card_in_set('hallowed burial', 'EVE').
card_original_type('hallowed burial'/'EVE', 'Sorcery').
card_original_text('hallowed burial'/'EVE', 'Put all creatures on the bottom of their owners\' libraries.').
card_first_print('hallowed burial', 'EVE').
card_image_name('hallowed burial'/'EVE', 'hallowed burial').
card_uid('hallowed burial'/'EVE', 'EVE:Hallowed Burial:hallowed burial').
card_rarity('hallowed burial'/'EVE', 'Rare').
card_artist('hallowed burial'/'EVE', 'Dave Kendall').
card_number('hallowed burial'/'EVE', '7').
card_flavor_text('hallowed burial'/'EVE', '\"I\'d rather hear the screams of battle than the quiet that follows.\"\n—Talara, elvish safewright').
card_multiverse_id('hallowed burial'/'EVE', '157394').

card_in_set('harvest gwyllion', 'EVE').
card_original_type('harvest gwyllion'/'EVE', 'Creature — Hag').
card_original_text('harvest gwyllion'/'EVE', 'Wither (This deals damage to creatures in the form of -1/-1 counters.)').
card_first_print('harvest gwyllion', 'EVE').
card_image_name('harvest gwyllion'/'EVE', 'harvest gwyllion').
card_uid('harvest gwyllion'/'EVE', 'EVE:Harvest Gwyllion:harvest gwyllion').
card_rarity('harvest gwyllion'/'EVE', 'Common').
card_artist('harvest gwyllion'/'EVE', 'Nils Hamm').
card_number('harvest gwyllion'/'EVE', '90').
card_flavor_text('harvest gwyllion'/'EVE', 'She speaks only in vile epithets, spitting out curses and obscenities that harm others just by the hearing.').
card_multiverse_id('harvest gwyllion'/'EVE', '152089').

card_in_set('hatchet bully', 'EVE').
card_original_type('hatchet bully'/'EVE', 'Creature — Goblin Warrior').
card_original_text('hatchet bully'/'EVE', '{2}{R}, {T}, Put a -1/-1 counter on a creature you control: Hatchet Bully deals 2 damage to target creature or player.').
card_first_print('hatchet bully', 'EVE').
card_image_name('hatchet bully'/'EVE', 'hatchet bully').
card_uid('hatchet bully'/'EVE', 'EVE:Hatchet Bully:hatchet bully').
card_rarity('hatchet bully'/'EVE', 'Uncommon').
card_artist('hatchet bully'/'EVE', 'Paul Bonner').
card_number('hatchet bully'/'EVE', '54').
card_flavor_text('hatchet bully'/'EVE', '\"It\'s not much for eating, but I think we can find somethin\' to do with it.\"').
card_multiverse_id('hatchet bully'/'EVE', '147413').

card_in_set('hateflayer', 'EVE').
card_original_type('hateflayer'/'EVE', 'Creature — Elemental').
card_original_text('hateflayer'/'EVE', 'Wither (This deals damage to creatures in the form of -1/-1 counters.)\n{2}{R}, {Q}: Hateflayer deals damage equal to its power to target creature or player. ({Q} is the untap symbol.)').
card_first_print('hateflayer', 'EVE').
card_image_name('hateflayer'/'EVE', 'hateflayer').
card_uid('hateflayer'/'EVE', 'EVE:Hateflayer:hateflayer').
card_rarity('hateflayer'/'EVE', 'Rare').
card_artist('hateflayer'/'EVE', 'Thomas M. Baxa').
card_number('hateflayer'/'EVE', '55').
card_flavor_text('hateflayer'/'EVE', 'It has no safe side.').
card_multiverse_id('hateflayer'/'EVE', '152155').

card_in_set('hearthfire hobgoblin', 'EVE').
card_original_type('hearthfire hobgoblin'/'EVE', 'Creature — Goblin Soldier').
card_original_text('hearthfire hobgoblin'/'EVE', 'Double strike').
card_first_print('hearthfire hobgoblin', 'EVE').
card_image_name('hearthfire hobgoblin'/'EVE', 'hearthfire hobgoblin').
card_uid('hearthfire hobgoblin'/'EVE', 'EVE:Hearthfire Hobgoblin:hearthfire hobgoblin').
card_rarity('hearthfire hobgoblin'/'EVE', 'Uncommon').
card_artist('hearthfire hobgoblin'/'EVE', 'Steven Belledin').
card_number('hearthfire hobgoblin'/'EVE', '141').
card_flavor_text('hearthfire hobgoblin'/'EVE', 'Hobgoblins are best left alone. They sharpen their farm implements far more than is necessary for their work in the fields.').
card_multiverse_id('hearthfire hobgoblin'/'EVE', '157201').

card_in_set('heartlash cinder', 'EVE').
card_original_type('heartlash cinder'/'EVE', 'Creature — Elemental Warrior').
card_original_text('heartlash cinder'/'EVE', 'Haste\nChroma When Heartlash Cinder comes into play, it gets +X/+0 until end of turn, where X is the number of red mana symbols in the mana costs of permanents you control.').
card_first_print('heartlash cinder', 'EVE').
card_image_name('heartlash cinder'/'EVE', 'heartlash cinder').
card_uid('heartlash cinder'/'EVE', 'EVE:Heartlash Cinder:heartlash cinder').
card_rarity('heartlash cinder'/'EVE', 'Common').
card_artist('heartlash cinder'/'EVE', 'Steve Prescott').
card_number('heartlash cinder'/'EVE', '56').
card_multiverse_id('heartlash cinder'/'EVE', '141942').

card_in_set('helix pinnacle', 'EVE').
card_original_type('helix pinnacle'/'EVE', 'Enchantment').
card_original_text('helix pinnacle'/'EVE', 'Shroud\n{X}: Put X tower counters on Helix Pinnacle.\nAt the beginning of your upkeep, if there are 100 or more tower counters on Helix Pinnacle, you win the game.').
card_first_print('helix pinnacle', 'EVE').
card_image_name('helix pinnacle'/'EVE', 'helix pinnacle').
card_uid('helix pinnacle'/'EVE', 'EVE:Helix Pinnacle:helix pinnacle').
card_rarity('helix pinnacle'/'EVE', 'Rare').
card_artist('helix pinnacle'/'EVE', 'Dan Scott').
card_number('helix pinnacle'/'EVE', '68').
card_multiverse_id('helix pinnacle'/'EVE', '150992').

card_in_set('hobgoblin dragoon', 'EVE').
card_original_type('hobgoblin dragoon'/'EVE', 'Creature — Goblin Knight').
card_original_text('hobgoblin dragoon'/'EVE', 'Flying, first strike').
card_first_print('hobgoblin dragoon', 'EVE').
card_image_name('hobgoblin dragoon'/'EVE', 'hobgoblin dragoon').
card_uid('hobgoblin dragoon'/'EVE', 'EVE:Hobgoblin Dragoon:hobgoblin dragoon').
card_rarity('hobgoblin dragoon'/'EVE', 'Common').
card_artist('hobgoblin dragoon'/'EVE', 'Matt Cavotta').
card_number('hobgoblin dragoon'/'EVE', '142').
card_flavor_text('hobgoblin dragoon'/'EVE', 'The faeries snickered at first. A common hob in Oona\'s skies? But his lance ended their mockery, and now the sound of a cicada\'s wings brings a shudder to the faerie heart.').
card_multiverse_id('hobgoblin dragoon'/'EVE', '151090').

card_in_set('hoof skulkin', 'EVE').
card_original_type('hoof skulkin'/'EVE', 'Artifact Creature — Scarecrow').
card_original_text('hoof skulkin'/'EVE', '{3}: Target green creature gets +1/+1 until end of turn.').
card_first_print('hoof skulkin', 'EVE').
card_image_name('hoof skulkin'/'EVE', 'hoof skulkin').
card_uid('hoof skulkin'/'EVE', 'EVE:Hoof Skulkin:hoof skulkin').
card_rarity('hoof skulkin'/'EVE', 'Common').
card_artist('hoof skulkin'/'EVE', 'Joshua Hagler').
card_number('hoof skulkin'/'EVE', '169').
card_flavor_text('hoof skulkin'/'EVE', 'It hunts with the memory of a long lost herd.').
card_multiverse_id('hoof skulkin'/'EVE', '158294').

card_in_set('hotheaded giant', 'EVE').
card_original_type('hotheaded giant'/'EVE', 'Creature — Giant Warrior').
card_original_text('hotheaded giant'/'EVE', 'Haste\nHotheaded Giant comes into play with two -1/-1 counters on it unless you played another red spell this turn.').
card_first_print('hotheaded giant', 'EVE').
card_image_name('hotheaded giant'/'EVE', 'hotheaded giant').
card_uid('hotheaded giant'/'EVE', 'EVE:Hotheaded Giant:hotheaded giant').
card_rarity('hotheaded giant'/'EVE', 'Common').
card_artist('hotheaded giant'/'EVE', 'Fred Harper').
card_number('hotheaded giant'/'EVE', '57').
card_flavor_text('hotheaded giant'/'EVE', 'As he races the moon each night, he draws a glowing equator across the world.').
card_multiverse_id('hotheaded giant'/'EVE', '153426').

card_in_set('idle thoughts', 'EVE').
card_original_type('idle thoughts'/'EVE', 'Enchantment').
card_original_text('idle thoughts'/'EVE', '{2}: Draw a card if you have no cards in hand.').
card_first_print('idle thoughts', 'EVE').
card_image_name('idle thoughts'/'EVE', 'idle thoughts').
card_uid('idle thoughts'/'EVE', 'EVE:Idle Thoughts:idle thoughts').
card_rarity('idle thoughts'/'EVE', 'Uncommon').
card_artist('idle thoughts'/'EVE', 'Steven Belledin').
card_number('idle thoughts'/'EVE', '23').
card_flavor_text('idle thoughts'/'EVE', 'Inspiration strikes the uncluttered mind.').
card_multiverse_id('idle thoughts'/'EVE', '157412').

card_in_set('impelled giant', 'EVE').
card_original_type('impelled giant'/'EVE', 'Creature — Giant Warrior').
card_original_text('impelled giant'/'EVE', 'Trample\nTap an untapped red creature you control other than Impelled Giant: Impelled Giant gets +X/+0 until end of turn, where X is the power of the creature tapped this way.').
card_first_print('impelled giant', 'EVE').
card_image_name('impelled giant'/'EVE', 'impelled giant').
card_uid('impelled giant'/'EVE', 'EVE:Impelled Giant:impelled giant').
card_rarity('impelled giant'/'EVE', 'Uncommon').
card_artist('impelled giant'/'EVE', 'Mark Zug').
card_number('impelled giant'/'EVE', '58').
card_multiverse_id('impelled giant'/'EVE', '152049').

card_in_set('indigo faerie', 'EVE').
card_original_type('indigo faerie'/'EVE', 'Creature — Faerie Wizard').
card_original_text('indigo faerie'/'EVE', 'Flying\n{U}: Target permanent becomes blue in addition to its other colors until end of turn.').
card_first_print('indigo faerie', 'EVE').
card_image_name('indigo faerie'/'EVE', 'indigo faerie').
card_uid('indigo faerie'/'EVE', 'EVE:Indigo Faerie:indigo faerie').
card_rarity('indigo faerie'/'EVE', 'Uncommon').
card_artist('indigo faerie'/'EVE', 'Steve Prescott').
card_number('indigo faerie'/'EVE', '24').
card_flavor_text('indigo faerie'/'EVE', '\"You should see your face right now.\"').
card_multiverse_id('indigo faerie'/'EVE', '153422').

card_in_set('inside out', 'EVE').
card_original_type('inside out'/'EVE', 'Instant').
card_original_text('inside out'/'EVE', 'Switch target creature\'s power and toughness until end of turn.\nDraw a card.').
card_first_print('inside out', 'EVE').
card_image_name('inside out'/'EVE', 'inside out').
card_uid('inside out'/'EVE', 'EVE:Inside Out:inside out').
card_rarity('inside out'/'EVE', 'Common').
card_artist('inside out'/'EVE', 'Zoltan Boros & Gabor Szikszai').
card_number('inside out'/'EVE', '103').
card_flavor_text('inside out'/'EVE', 'Seyden\'s temperament didn\'t take well to the puca\'s magic. Nor did his face.').
card_multiverse_id('inside out'/'EVE', '154263').

card_in_set('inundate', 'EVE').
card_original_type('inundate'/'EVE', 'Sorcery').
card_original_text('inundate'/'EVE', 'Return all nonblue creatures to their owners\' hands.').
card_first_print('inundate', 'EVE').
card_image_name('inundate'/'EVE', 'inundate').
card_uid('inundate'/'EVE', 'EVE:Inundate:inundate').
card_rarity('inundate'/'EVE', 'Rare').
card_artist('inundate'/'EVE', 'Mark Zug').
card_number('inundate'/'EVE', '25').
card_flavor_text('inundate'/'EVE', '\"For years, the landfolk have emptied their refuse into the waters. It\'s time to return the favor.\"').
card_multiverse_id('inundate'/'EVE', '142010').

card_in_set('invert the skies', 'EVE').
card_original_type('invert the skies'/'EVE', 'Instant').
card_original_text('invert the skies'/'EVE', 'Creatures your opponents control lose flying until end of turn if {G} was spent to play Invert the Skies, and creatures you control gain flying until end of turn if {U} was spent to play it. (Do both if {G}{U} was spent.)').
card_first_print('invert the skies', 'EVE').
card_image_name('invert the skies'/'EVE', 'invert the skies').
card_uid('invert the skies'/'EVE', 'EVE:Invert the Skies:invert the skies').
card_rarity('invert the skies'/'EVE', 'Uncommon').
card_artist('invert the skies'/'EVE', 'Randy Gallegos').
card_number('invert the skies'/'EVE', '155').
card_multiverse_id('invert the skies'/'EVE', '151108').

card_in_set('jawbone skulkin', 'EVE').
card_original_type('jawbone skulkin'/'EVE', 'Artifact Creature — Scarecrow').
card_original_text('jawbone skulkin'/'EVE', '{2}: Target red creature gains haste until end of turn.').
card_first_print('jawbone skulkin', 'EVE').
card_image_name('jawbone skulkin'/'EVE', 'jawbone skulkin').
card_uid('jawbone skulkin'/'EVE', 'EVE:Jawbone Skulkin:jawbone skulkin').
card_rarity('jawbone skulkin'/'EVE', 'Common').
card_artist('jawbone skulkin'/'EVE', 'Jeff Easley').
card_number('jawbone skulkin'/'EVE', '170').
card_flavor_text('jawbone skulkin'/'EVE', '\"Ah, skull of shrew—a very potent substance. In powdered form, it gives thrice the zing of noggle hoof pulp.\"\n—Boghald, Barrenton medic').
card_multiverse_id('jawbone skulkin'/'EVE', '158295').

card_in_set('kithkin spellduster', 'EVE').
card_original_type('kithkin spellduster'/'EVE', 'Creature — Kithkin Wizard').
card_original_text('kithkin spellduster'/'EVE', 'Flying\n{1}{W}, Sacrifice Kithkin Spellduster: Destroy target enchantment.\nPersist (When this creature is put into a graveyard from play, if it had no -1/-1 counters on it, return it to play under its owner\'s control with a -1/-1 counter on it.)').
card_first_print('kithkin spellduster', 'EVE').
card_image_name('kithkin spellduster'/'EVE', 'kithkin spellduster').
card_uid('kithkin spellduster'/'EVE', 'EVE:Kithkin Spellduster:kithkin spellduster').
card_rarity('kithkin spellduster'/'EVE', 'Common').
card_artist('kithkin spellduster'/'EVE', 'Trevor Hairsine').
card_number('kithkin spellduster'/'EVE', '8').
card_multiverse_id('kithkin spellduster'/'EVE', '152096').

card_in_set('kithkin zealot', 'EVE').
card_original_type('kithkin zealot'/'EVE', 'Creature — Kithkin Cleric').
card_original_text('kithkin zealot'/'EVE', 'When Kithkin Zealot comes into play, you gain 1 life for each black and/or red permanent target opponent controls.').
card_first_print('kithkin zealot', 'EVE').
card_image_name('kithkin zealot'/'EVE', 'kithkin zealot').
card_uid('kithkin zealot'/'EVE', 'EVE:Kithkin Zealot:kithkin zealot').
card_rarity('kithkin zealot'/'EVE', 'Common').
card_artist('kithkin zealot'/'EVE', 'Scott Altmann').
card_number('kithkin zealot'/'EVE', '9').
card_flavor_text('kithkin zealot'/'EVE', 'The more his flock fears, the more power he wields.').
card_multiverse_id('kithkin zealot'/'EVE', '154346').

card_in_set('leering emblem', 'EVE').
card_original_type('leering emblem'/'EVE', 'Artifact — Equipment').
card_original_text('leering emblem'/'EVE', 'Whenever you play a spell, equipped creature gets +2/+2 until end of turn.\nEquip {2}').
card_first_print('leering emblem', 'EVE').
card_image_name('leering emblem'/'EVE', 'leering emblem').
card_uid('leering emblem'/'EVE', 'EVE:Leering Emblem:leering emblem').
card_rarity('leering emblem'/'EVE', 'Rare').
card_artist('leering emblem'/'EVE', 'Ron Brown').
card_number('leering emblem'/'EVE', '171').
card_flavor_text('leering emblem'/'EVE', 'The banner was not a symbol of victory, but a herald of the carnage to come.').
card_multiverse_id('leering emblem'/'EVE', '157971').

card_in_set('light from within', 'EVE').
card_original_type('light from within'/'EVE', 'Enchantment').
card_original_text('light from within'/'EVE', 'Chroma Each creature you control gets +1/+1 for each white mana symbol in its mana cost.').
card_first_print('light from within', 'EVE').
card_image_name('light from within'/'EVE', 'light from within').
card_uid('light from within'/'EVE', 'EVE:Light from Within:light from within').
card_rarity('light from within'/'EVE', 'Rare').
card_artist('light from within'/'EVE', 'Aleksi Briclot').
card_number('light from within'/'EVE', '10').
card_flavor_text('light from within'/'EVE', '\"Muscle does not make one strong. It only masks the soul\'s light. True strength lies in purity of purpose.\"\n—Donal Alloway, cenn of Kinscaer').
card_multiverse_id('light from within'/'EVE', '151139').

card_in_set('lingering tormentor', 'EVE').
card_original_type('lingering tormentor'/'EVE', 'Creature — Spirit').
card_original_text('lingering tormentor'/'EVE', 'Fear\nPersist (When this creature is put into a graveyard from play, if it had no -1/-1 counters on it, return it to play under its owner\'s control with a -1/-1 counter on it.)').
card_first_print('lingering tormentor', 'EVE').
card_image_name('lingering tormentor'/'EVE', 'lingering tormentor').
card_uid('lingering tormentor'/'EVE', 'EVE:Lingering Tormentor:lingering tormentor').
card_rarity('lingering tormentor'/'EVE', 'Uncommon').
card_artist('lingering tormentor'/'EVE', 'Joshua Hagler').
card_number('lingering tormentor'/'EVE', '36').
card_flavor_text('lingering tormentor'/'EVE', 'The bogeyman doesn\'t disappear when you close your eyes.').
card_multiverse_id('lingering tormentor'/'EVE', '147437').

card_in_set('loyal gyrfalcon', 'EVE').
card_original_type('loyal gyrfalcon'/'EVE', 'Creature — Bird').
card_original_text('loyal gyrfalcon'/'EVE', 'Defender, flying\nWhenever you play a white spell, Loyal Gyrfalcon loses defender until end of turn.').
card_first_print('loyal gyrfalcon', 'EVE').
card_image_name('loyal gyrfalcon'/'EVE', 'loyal gyrfalcon').
card_uid('loyal gyrfalcon'/'EVE', 'EVE:Loyal Gyrfalcon:loyal gyrfalcon').
card_rarity('loyal gyrfalcon'/'EVE', 'Uncommon').
card_artist('loyal gyrfalcon'/'EVE', 'Darrell Riche').
card_number('loyal gyrfalcon'/'EVE', '11').
card_flavor_text('loyal gyrfalcon'/'EVE', 'It\'s tethered not to keep it from fleeing but to keep it from eating everything in sight.').
card_multiverse_id('loyal gyrfalcon'/'EVE', '151059').

card_in_set('marshdrinker giant', 'EVE').
card_original_type('marshdrinker giant'/'EVE', 'Creature — Giant Warrior').
card_original_text('marshdrinker giant'/'EVE', 'When Marshdrinker Giant comes into play, destroy target Island or Swamp an opponent controls.').
card_first_print('marshdrinker giant', 'EVE').
card_image_name('marshdrinker giant'/'EVE', 'marshdrinker giant').
card_uid('marshdrinker giant'/'EVE', 'EVE:Marshdrinker Giant:marshdrinker giant').
card_rarity('marshdrinker giant'/'EVE', 'Uncommon').
card_artist('marshdrinker giant'/'EVE', 'Daarken').
card_number('marshdrinker giant'/'EVE', '69').
card_flavor_text('marshdrinker giant'/'EVE', 'When giants rise from their long slumbers, they rarely remember how fragile the waking world can be.').
card_multiverse_id('marshdrinker giant'/'EVE', '142015').

card_in_set('merrow bonegnawer', 'EVE').
card_original_type('merrow bonegnawer'/'EVE', 'Creature — Merfolk Rogue').
card_original_text('merrow bonegnawer'/'EVE', '{T}: Target player removes a card in his or her graveyard from the game. \nWhenever you play a black spell, you may untap Merrow Bonegnawer.').
card_first_print('merrow bonegnawer', 'EVE').
card_image_name('merrow bonegnawer'/'EVE', 'merrow bonegnawer').
card_uid('merrow bonegnawer'/'EVE', 'EVE:Merrow Bonegnawer:merrow bonegnawer').
card_rarity('merrow bonegnawer'/'EVE', 'Common').
card_artist('merrow bonegnawer'/'EVE', 'Jim Nelson').
card_number('merrow bonegnawer'/'EVE', '37').
card_flavor_text('merrow bonegnawer'/'EVE', 'Banished to the Dark Meanders for hideous crimes, some merrows find a taste for the darkness . . . and the dead.').
card_multiverse_id('merrow bonegnawer'/'EVE', '151158').

card_in_set('merrow levitator', 'EVE').
card_original_type('merrow levitator'/'EVE', 'Creature — Merfolk Wizard').
card_original_text('merrow levitator'/'EVE', '{T}: Target creature gains flying until end of turn.\nWhenever you play a blue spell, you may untap Merrow Levitator.').
card_first_print('merrow levitator', 'EVE').
card_image_name('merrow levitator'/'EVE', 'merrow levitator').
card_uid('merrow levitator'/'EVE', 'EVE:Merrow Levitator:merrow levitator').
card_rarity('merrow levitator'/'EVE', 'Common').
card_artist('merrow levitator'/'EVE', 'Jim Pavelec').
card_number('merrow levitator'/'EVE', '26').
card_flavor_text('merrow levitator'/'EVE', 'He fancies himself a master thief, claiming to \"steal the ground\" from his victims.').
card_multiverse_id('merrow levitator'/'EVE', '151088').

card_in_set('mindwrack liege', 'EVE').
card_original_type('mindwrack liege'/'EVE', 'Creature — Horror').
card_original_text('mindwrack liege'/'EVE', 'Other blue creatures you control get +1/+1.\nOther red creatures you control get +1/+1.\n{U/R}{U/R}{U/R}{U/R}: You may put a blue or red creature card from your hand into play.').
card_first_print('mindwrack liege', 'EVE').
card_image_name('mindwrack liege'/'EVE', 'mindwrack liege').
card_uid('mindwrack liege'/'EVE', 'EVE:Mindwrack Liege:mindwrack liege').
card_rarity('mindwrack liege'/'EVE', 'Rare').
card_artist('mindwrack liege'/'EVE', 'Richard Whitters').
card_number('mindwrack liege'/'EVE', '104').
card_multiverse_id('mindwrack liege'/'EVE', '151132').

card_in_set('mirror sheen', 'EVE').
card_original_type('mirror sheen'/'EVE', 'Enchantment').
card_original_text('mirror sheen'/'EVE', '{1}{U/R}{U/R}: Copy target instant or sorcery spell that targets you. You may choose new targets for the copy.').
card_first_print('mirror sheen', 'EVE').
card_image_name('mirror sheen'/'EVE', 'mirror sheen').
card_uid('mirror sheen'/'EVE', 'EVE:Mirror Sheen:mirror sheen').
card_rarity('mirror sheen'/'EVE', 'Rare').
card_artist('mirror sheen'/'EVE', 'John Avon').
card_number('mirror sheen'/'EVE', '105').
card_flavor_text('mirror sheen'/'EVE', 'Even treefolk witches steer clear of the unpredictable woods near Glen Elendra. Spells refract off the glamer-polished trees and the eyes of lurking pucas.').
card_multiverse_id('mirror sheen'/'EVE', '152106').

card_in_set('monstrify', 'EVE').
card_original_type('monstrify'/'EVE', 'Sorcery').
card_original_text('monstrify'/'EVE', 'Target creature gets +4/+4 until end of turn.\nRetrace (You may play this card from your graveyard by discarding a land card in addition to paying its other costs.)').
card_first_print('monstrify', 'EVE').
card_image_name('monstrify'/'EVE', 'monstrify').
card_uid('monstrify'/'EVE', 'EVE:Monstrify:monstrify').
card_rarity('monstrify'/'EVE', 'Common').
card_artist('monstrify'/'EVE', 'Lars Grant-West').
card_number('monstrify'/'EVE', '70').
card_flavor_text('monstrify'/'EVE', 'Gackt came to regret keeping his pet toads on his head.').
card_multiverse_id('monstrify'/'EVE', '153441').

card_in_set('moonhold', 'EVE').
card_original_type('moonhold'/'EVE', 'Instant').
card_original_text('moonhold'/'EVE', 'Target player can\'t play land cards this turn if {R} was spent to play Moonhold and can\'t play creature cards this turn if {W} was spent to play it. (Do both if {R}{W} was spent.)').
card_first_print('moonhold', 'EVE').
card_image_name('moonhold'/'EVE', 'moonhold').
card_uid('moonhold'/'EVE', 'EVE:Moonhold:moonhold').
card_rarity('moonhold'/'EVE', 'Uncommon').
card_artist('moonhold'/'EVE', 'Mike Dringenberg').
card_number('moonhold'/'EVE', '143').
card_multiverse_id('moonhold'/'EVE', '157403').

card_in_set('murkfiend liege', 'EVE').
card_original_type('murkfiend liege'/'EVE', 'Creature — Horror').
card_original_text('murkfiend liege'/'EVE', 'Other green creatures you control get +1/+1.\nOther blue creatures you control get +1/+1.\nUntap all green and/or blue creatures you control during each other player\'s untap step.').
card_first_print('murkfiend liege', 'EVE').
card_image_name('murkfiend liege'/'EVE', 'murkfiend liege').
card_uid('murkfiend liege'/'EVE', 'EVE:Murkfiend Liege:murkfiend liege').
card_rarity('murkfiend liege'/'EVE', 'Rare').
card_artist('murkfiend liege'/'EVE', 'Carl Critchlow').
card_number('murkfiend liege'/'EVE', '156').
card_multiverse_id('murkfiend liege'/'EVE', '152091').

card_in_set('necroskitter', 'EVE').
card_original_type('necroskitter'/'EVE', 'Creature — Elemental').
card_original_text('necroskitter'/'EVE', 'Wither (This deals damage to creatures in the form of -1/-1 counters.)\nWhenever a creature an opponent controls with a -1/-1 counter on it is put into a graveyard, you may return that card to play under your control.').
card_first_print('necroskitter', 'EVE').
card_image_name('necroskitter'/'EVE', 'necroskitter').
card_uid('necroskitter'/'EVE', 'EVE:Necroskitter:necroskitter').
card_rarity('necroskitter'/'EVE', 'Rare').
card_artist('necroskitter'/'EVE', 'Jaime Jones').
card_number('necroskitter'/'EVE', '38').
card_multiverse_id('necroskitter'/'EVE', '152080').

card_in_set('needle specter', 'EVE').
card_original_type('needle specter'/'EVE', 'Creature — Specter').
card_original_text('needle specter'/'EVE', 'Flying\nWither (This deals damage to creatures in the form of -1/-1 counters.)\nWhenever Needle Specter deals combat damage to a player, that player discards that many cards.').
card_first_print('needle specter', 'EVE').
card_image_name('needle specter'/'EVE', 'needle specter').
card_uid('needle specter'/'EVE', 'EVE:Needle Specter:needle specter').
card_rarity('needle specter'/'EVE', 'Rare').
card_artist('needle specter'/'EVE', 'Christopher Moeller').
card_number('needle specter'/'EVE', '39').
card_multiverse_id('needle specter'/'EVE', '153474').

card_in_set('nettle sentinel', 'EVE').
card_original_type('nettle sentinel'/'EVE', 'Creature — Elf Warrior').
card_original_text('nettle sentinel'/'EVE', 'Nettle Sentinel doesn\'t untap during its controller\'s untap step.\nWhenever you play a green spell, you may untap Nettle Sentinel.').
card_first_print('nettle sentinel', 'EVE').
card_image_name('nettle sentinel'/'EVE', 'nettle sentinel').
card_uid('nettle sentinel'/'EVE', 'EVE:Nettle Sentinel:nettle sentinel').
card_rarity('nettle sentinel'/'EVE', 'Common').
card_artist('nettle sentinel'/'EVE', 'Kev Walker').
card_number('nettle sentinel'/'EVE', '71').
card_flavor_text('nettle sentinel'/'EVE', 'Though Shadowmoor\'s monster-haunted wilds beckon, she never leaves her post.').
card_multiverse_id('nettle sentinel'/'EVE', '151095').

card_in_set('nightmare incursion', 'EVE').
card_original_type('nightmare incursion'/'EVE', 'Sorcery').
card_original_text('nightmare incursion'/'EVE', 'Search target player\'s library for up to X cards, where X is the number of Swamps you control, and remove them from the game. Then that player shuffles his or her library.').
card_first_print('nightmare incursion', 'EVE').
card_image_name('nightmare incursion'/'EVE', 'nightmare incursion').
card_uid('nightmare incursion'/'EVE', 'EVE:Nightmare Incursion:nightmare incursion').
card_rarity('nightmare incursion'/'EVE', 'Rare').
card_artist('nightmare incursion'/'EVE', 'Chuck Lukacs').
card_number('nightmare incursion'/'EVE', '40').
card_multiverse_id('nightmare incursion'/'EVE', '157414').

card_in_set('nightsky mimic', 'EVE').
card_original_type('nightsky mimic'/'EVE', 'Creature — Shapeshifter').
card_original_text('nightsky mimic'/'EVE', 'Whenever you play a spell that\'s both white and black, Nightsky Mimic becomes 4/4 and gains flying until end of turn.').
card_first_print('nightsky mimic', 'EVE').
card_image_name('nightsky mimic'/'EVE', 'nightsky mimic').
card_uid('nightsky mimic'/'EVE', 'EVE:Nightsky Mimic:nightsky mimic').
card_rarity('nightsky mimic'/'EVE', 'Common').
card_artist('nightsky mimic'/'EVE', 'Franz Vohwinkel').
card_number('nightsky mimic'/'EVE', '91').
card_flavor_text('nightsky mimic'/'EVE', 'A mimic need only touch a being to learn its shape, but touching usually leads to mauling and messily eating.').
card_multiverse_id('nightsky mimic'/'EVE', '151099').

card_in_set('nip gwyllion', 'EVE').
card_original_type('nip gwyllion'/'EVE', 'Creature — Hag').
card_original_text('nip gwyllion'/'EVE', 'Lifelink (Whenever this creature deals damage, you gain that much life.)').
card_first_print('nip gwyllion', 'EVE').
card_image_name('nip gwyllion'/'EVE', 'nip gwyllion').
card_uid('nip gwyllion'/'EVE', 'EVE:Nip Gwyllion:nip gwyllion').
card_rarity('nip gwyllion'/'EVE', 'Common').
card_artist('nip gwyllion'/'EVE', 'Alex Horley-Orlandelli').
card_number('nip gwyllion'/'EVE', '92').
card_flavor_text('nip gwyllion'/'EVE', 'Once your life and all your worldly possessions have been surrendered to her, a gwyllion can be surprisingly civilized.').
card_multiverse_id('nip gwyllion'/'EVE', '150986').

card_in_set('nobilis of war', 'EVE').
card_original_type('nobilis of war'/'EVE', 'Creature — Spirit Avatar').
card_original_text('nobilis of war'/'EVE', 'Flying\nAttacking creatures you control get +2/+0.').
card_first_print('nobilis of war', 'EVE').
card_image_name('nobilis of war'/'EVE', 'nobilis of war').
card_uid('nobilis of war'/'EVE', 'EVE:Nobilis of War:nobilis of war').
card_rarity('nobilis of war'/'EVE', 'Rare').
card_artist('nobilis of war'/'EVE', 'Christopher Moeller').
card_number('nobilis of war'/'EVE', '144').
card_flavor_text('nobilis of war'/'EVE', '\"A great siege is a banquet to him; a long and terrible battle, the most exquisite delicacy.\"\n—The Seer\'s Parables').
card_multiverse_id('nobilis of war'/'EVE', '154258').

card_in_set('noggle bandit', 'EVE').
card_original_type('noggle bandit'/'EVE', 'Creature — Noggle Rogue').
card_original_text('noggle bandit'/'EVE', 'Noggle Bandit can\'t be blocked except by creatures with defender.').
card_first_print('noggle bandit', 'EVE').
card_image_name('noggle bandit'/'EVE', 'noggle bandit').
card_uid('noggle bandit'/'EVE', 'EVE:Noggle Bandit:noggle bandit').
card_rarity('noggle bandit'/'EVE', 'Common').
card_artist('noggle bandit'/'EVE', 'Thomas Denmark').
card_number('noggle bandit'/'EVE', '106').
card_flavor_text('noggle bandit'/'EVE', 'Noggles believe that they were the first race ever to walk Shadowmoor. They don\'t \"steal.\" They just take back what\'s rightfully theirs.').
card_multiverse_id('noggle bandit'/'EVE', '157286').

card_in_set('noggle bridgebreaker', 'EVE').
card_original_type('noggle bridgebreaker'/'EVE', 'Creature — Noggle Rogue').
card_original_text('noggle bridgebreaker'/'EVE', 'When Noggle Bridgebreaker comes into play, return a land you control to its owner\'s hand.').
card_first_print('noggle bridgebreaker', 'EVE').
card_image_name('noggle bridgebreaker'/'EVE', 'noggle bridgebreaker').
card_uid('noggle bridgebreaker'/'EVE', 'EVE:Noggle Bridgebreaker:noggle bridgebreaker').
card_rarity('noggle bridgebreaker'/'EVE', 'Common').
card_artist('noggle bridgebreaker'/'EVE', 'Thomas M. Baxa').
card_number('noggle bridgebreaker'/'EVE', '107').
card_flavor_text('noggle bridgebreaker'/'EVE', '\"It\'s always funny till someone gets hurt. Then it\'s hilarious.\"').
card_multiverse_id('noggle bridgebreaker'/'EVE', '158899').

card_in_set('noggle hedge-mage', 'EVE').
card_original_type('noggle hedge-mage'/'EVE', 'Creature — Noggle Wizard').
card_original_text('noggle hedge-mage'/'EVE', 'When Noggle Hedge-Mage comes into play, if you control two or more Islands, you may tap two target permanents.\nWhen Noggle Hedge-Mage comes into play, if you control two or more Mountains, you may have Noggle Hedge-Mage deal 2 damage to target player.').
card_first_print('noggle hedge-mage', 'EVE').
card_image_name('noggle hedge-mage'/'EVE', 'noggle hedge-mage').
card_uid('noggle hedge-mage'/'EVE', 'EVE:Noggle Hedge-Mage:noggle hedge-mage').
card_rarity('noggle hedge-mage'/'EVE', 'Uncommon').
card_artist('noggle hedge-mage'/'EVE', 'Larry MacDougall').
card_number('noggle hedge-mage'/'EVE', '108').
card_multiverse_id('noggle hedge-mage'/'EVE', '159066').

card_in_set('noggle ransacker', 'EVE').
card_original_type('noggle ransacker'/'EVE', 'Creature — Noggle Rogue').
card_original_text('noggle ransacker'/'EVE', 'When Noggle Ransacker comes into play, each player draws two cards, then discards a card at random.').
card_first_print('noggle ransacker', 'EVE').
card_image_name('noggle ransacker'/'EVE', 'noggle ransacker').
card_uid('noggle ransacker'/'EVE', 'EVE:Noggle Ransacker:noggle ransacker').
card_rarity('noggle ransacker'/'EVE', 'Uncommon').
card_artist('noggle ransacker'/'EVE', 'Alex Horley-Orlandelli').
card_number('noggle ransacker'/'EVE', '109').
card_flavor_text('noggle ransacker'/'EVE', 'Noggles live purely by what they can scavenge. There is not a single thing a noggle eats, wears, or uses that did not once belong to another.').
card_multiverse_id('noggle ransacker'/'EVE', '152032').

card_in_set('noxious hatchling', 'EVE').
card_original_type('noxious hatchling'/'EVE', 'Creature — Elemental').
card_original_text('noxious hatchling'/'EVE', 'Noxious Hatchling comes into play with four -1/-1 counters on it.\nWither (This deals damage to creatures in the form of -1/-1 counters.)\nWhenever you play a black spell, remove a -1/-1 counter from Noxious Hatchling.\nWhenever you play a green spell, remove a -1/-1 counter from Noxious Hatchling.').
card_first_print('noxious hatchling', 'EVE').
card_image_name('noxious hatchling'/'EVE', 'noxious hatchling').
card_uid('noxious hatchling'/'EVE', 'EVE:Noxious Hatchling:noxious hatchling').
card_rarity('noxious hatchling'/'EVE', 'Uncommon').
card_artist('noxious hatchling'/'EVE', 'Nils Hamm').
card_number('noxious hatchling'/'EVE', '124').
card_multiverse_id('noxious hatchling'/'EVE', '153448').

card_in_set('nucklavee', 'EVE').
card_original_type('nucklavee'/'EVE', 'Creature — Beast').
card_original_text('nucklavee'/'EVE', 'When Nucklavee comes into play, you may return target red sorcery card from your graveyard to your hand.\nWhen Nucklavee comes into play, you may return target blue instant card from your graveyard to your hand.').
card_first_print('nucklavee', 'EVE').
card_image_name('nucklavee'/'EVE', 'nucklavee').
card_uid('nucklavee'/'EVE', 'EVE:Nucklavee:nucklavee').
card_rarity('nucklavee'/'EVE', 'Uncommon').
card_artist('nucklavee'/'EVE', 'Trevor Hairsine').
card_number('nucklavee'/'EVE', '110').
card_flavor_text('nucklavee'/'EVE', 'It loathes all tastes but two: spells and flesh.').
card_multiverse_id('nucklavee'/'EVE', '153058').

card_in_set('odious trow', 'EVE').
card_original_type('odious trow'/'EVE', 'Creature — Troll').
card_original_text('odious trow'/'EVE', '{1}{B/G}: Regenerate Odious Trow.').
card_first_print('odious trow', 'EVE').
card_image_name('odious trow'/'EVE', 'odious trow').
card_uid('odious trow'/'EVE', 'EVE:Odious Trow:odious trow').
card_rarity('odious trow'/'EVE', 'Common').
card_artist('odious trow'/'EVE', 'Warren Mahy').
card_number('odious trow'/'EVE', '125').
card_flavor_text('odious trow'/'EVE', 'Trows are the vile trolls that lurk in the bogs of Shadowmoor. They reappear each night to frighten the living and gnaw the dead.').
card_multiverse_id('odious trow'/'EVE', '150977').

card_in_set('oona\'s grace', 'EVE').
card_original_type('oona\'s grace'/'EVE', 'Instant').
card_original_text('oona\'s grace'/'EVE', 'Target player draws a card.\nRetrace (You may play this card from your graveyard by discarding a land card in addition to paying its other costs.)').
card_first_print('oona\'s grace', 'EVE').
card_image_name('oona\'s grace'/'EVE', 'oona\'s grace').
card_uid('oona\'s grace'/'EVE', 'EVE:Oona\'s Grace:oona\'s grace').
card_rarity('oona\'s grace'/'EVE', 'Common').
card_artist('oona\'s grace'/'EVE', 'Rebecca Guay').
card_number('oona\'s grace'/'EVE', '27').
card_flavor_text('oona\'s grace'/'EVE', 'Oona grants her favored subjects a dream from her own guarded heart.').
card_multiverse_id('oona\'s grace'/'EVE', '153475').

card_in_set('outrage shaman', 'EVE').
card_original_type('outrage shaman'/'EVE', 'Creature — Goblin Shaman').
card_original_text('outrage shaman'/'EVE', 'Chroma When Outrage Shaman comes into play, it deals damage to target creature equal to the number of red mana symbols in the mana costs of permanents you control.').
card_first_print('outrage shaman', 'EVE').
card_image_name('outrage shaman'/'EVE', 'outrage shaman').
card_uid('outrage shaman'/'EVE', 'EVE:Outrage Shaman:outrage shaman').
card_rarity('outrage shaman'/'EVE', 'Uncommon').
card_artist('outrage shaman'/'EVE', 'Cole Eastburn').
card_number('outrage shaman'/'EVE', '59').
card_flavor_text('outrage shaman'/'EVE', 'A boggart\'s mind is a tinderbox of volatile thoughts, hungry for a spark.').
card_multiverse_id('outrage shaman'/'EVE', '153473').

card_in_set('overbeing of myth', 'EVE').
card_original_type('overbeing of myth'/'EVE', 'Creature — Spirit Avatar').
card_original_text('overbeing of myth'/'EVE', 'Overbeing of Myth\'s power and toughness are each equal to the number of cards in your hand.\nAt the beginning of your draw step, draw a card.').
card_image_name('overbeing of myth'/'EVE', 'overbeing of myth').
card_uid('overbeing of myth'/'EVE', 'EVE:Overbeing of Myth:overbeing of myth').
card_rarity('overbeing of myth'/'EVE', 'Rare').
card_artist('overbeing of myth'/'EVE', 'Chippy').
card_number('overbeing of myth'/'EVE', '157').
card_flavor_text('overbeing of myth'/'EVE', '\"She walks among us unseen, learning from our imperfections.\"\n—The Seer\'s Parables').
card_multiverse_id('overbeing of myth'/'EVE', '151151').

card_in_set('patrol signaler', 'EVE').
card_original_type('patrol signaler'/'EVE', 'Creature — Kithkin Soldier').
card_original_text('patrol signaler'/'EVE', '{1}{W}, {Q}: Put a 1/1 white Kithkin Soldier creature token into play. ({Q} is the untap symbol.)').
card_first_print('patrol signaler', 'EVE').
card_image_name('patrol signaler'/'EVE', 'patrol signaler').
card_uid('patrol signaler'/'EVE', 'EVE:Patrol Signaler:patrol signaler').
card_rarity('patrol signaler'/'EVE', 'Uncommon').
card_artist('patrol signaler'/'EVE', 'Steve Prescott').
card_number('patrol signaler'/'EVE', '12').
card_flavor_text('patrol signaler'/'EVE', 'Many a keen-eared bogle has run afoul of a clever and silently orchestrated kithkin ambush.').
card_multiverse_id('patrol signaler'/'EVE', '157210').

card_in_set('phosphorescent feast', 'EVE').
card_original_type('phosphorescent feast'/'EVE', 'Sorcery').
card_original_text('phosphorescent feast'/'EVE', 'Chroma Reveal any number of cards in your hand. You gain 2 life for each green mana symbol in those cards\' mana costs.').
card_image_name('phosphorescent feast'/'EVE', 'phosphorescent feast').
card_uid('phosphorescent feast'/'EVE', 'EVE:Phosphorescent Feast:phosphorescent feast').
card_rarity('phosphorescent feast'/'EVE', 'Uncommon').
card_artist('phosphorescent feast'/'EVE', 'Ron Spencer').
card_number('phosphorescent feast'/'EVE', '72').
card_flavor_text('phosphorescent feast'/'EVE', 'The boggarts gorged themselves until their bellies glowed.').
card_multiverse_id('phosphorescent feast'/'EVE', '151113').

card_in_set('primalcrux', 'EVE').
card_original_type('primalcrux'/'EVE', 'Creature — Elemental').
card_original_text('primalcrux'/'EVE', 'Trample\nChroma Primalcrux\'s power and toughness are each equal to the number of green mana symbols in the mana costs of permanents you control.').
card_first_print('primalcrux', 'EVE').
card_image_name('primalcrux'/'EVE', 'primalcrux').
card_uid('primalcrux'/'EVE', 'EVE:Primalcrux:primalcrux').
card_rarity('primalcrux'/'EVE', 'Rare').
card_artist('primalcrux'/'EVE', 'Wayne Reynolds').
card_number('primalcrux'/'EVE', '73').
card_flavor_text('primalcrux'/'EVE', 'When nature is backed into a corner, just like its children, it lashes out.').
card_multiverse_id('primalcrux'/'EVE', '153431').

card_in_set('puncture blast', 'EVE').
card_original_type('puncture blast'/'EVE', 'Instant').
card_original_text('puncture blast'/'EVE', 'Wither (This deals damage to creatures in the form of -1/-1 counters.)\nPuncture Blast deals 3 damage to target creature or player.').
card_first_print('puncture blast', 'EVE').
card_image_name('puncture blast'/'EVE', 'puncture blast').
card_uid('puncture blast'/'EVE', 'EVE:Puncture Blast:puncture blast').
card_rarity('puncture blast'/'EVE', 'Common').
card_artist('puncture blast'/'EVE', 'Carl Critchlow').
card_number('puncture blast'/'EVE', '60').
card_flavor_text('puncture blast'/'EVE', 'Gnarltrunk didn\'t remember having quite that many knotholes.').
card_multiverse_id('puncture blast'/'EVE', '152034').

card_in_set('pyrrhic revival', 'EVE').
card_original_type('pyrrhic revival'/'EVE', 'Sorcery').
card_original_text('pyrrhic revival'/'EVE', 'Each player returns each creature card in his or her graveyard to play with a -1/-1 counter on it.').
card_first_print('pyrrhic revival', 'EVE').
card_image_name('pyrrhic revival'/'EVE', 'pyrrhic revival').
card_uid('pyrrhic revival'/'EVE', 'EVE:Pyrrhic Revival:pyrrhic revival').
card_rarity('pyrrhic revival'/'EVE', 'Rare').
card_artist('pyrrhic revival'/'EVE', 'Richard Kane Ferguson').
card_number('pyrrhic revival'/'EVE', '93').
card_flavor_text('pyrrhic revival'/'EVE', 'They found themselves alive again, still bearing their mortal wounds.').
card_multiverse_id('pyrrhic revival'/'EVE', '157397').

card_in_set('quillspike', 'EVE').
card_original_type('quillspike'/'EVE', 'Creature — Beast').
card_original_text('quillspike'/'EVE', '{B/G}, Remove a -1/-1 counter from a creature you control: Quillspike gets +3/+3 until end of turn.').
card_first_print('quillspike', 'EVE').
card_image_name('quillspike'/'EVE', 'quillspike').
card_uid('quillspike'/'EVE', 'EVE:Quillspike:quillspike').
card_rarity('quillspike'/'EVE', 'Uncommon').
card_artist('quillspike'/'EVE', 'Carl Critchlow').
card_number('quillspike'/'EVE', '126').
card_flavor_text('quillspike'/'EVE', 'While it\'s true that a quillspike does heal the sick, it\'s just because it finds them tastier that way.').
card_multiverse_id('quillspike'/'EVE', '152086').

card_in_set('raven\'s crime', 'EVE').
card_original_type('raven\'s crime'/'EVE', 'Sorcery').
card_original_text('raven\'s crime'/'EVE', 'Target player discards a card.\nRetrace (You may play this card from your graveyard by discarding a land card in addition to paying its other costs.)').
card_first_print('raven\'s crime', 'EVE').
card_image_name('raven\'s crime'/'EVE', 'raven\'s crime').
card_uid('raven\'s crime'/'EVE', 'EVE:Raven\'s Crime:raven\'s crime').
card_rarity('raven\'s crime'/'EVE', 'Common').
card_artist('raven\'s crime'/'EVE', 'Warren Mahy').
card_number('raven\'s crime'/'EVE', '41').
card_flavor_text('raven\'s crime'/'EVE', 'It plucks away memories like choice bits of carrion.').
card_multiverse_id('raven\'s crime'/'EVE', '153487').

card_in_set('razorfin abolisher', 'EVE').
card_original_type('razorfin abolisher'/'EVE', 'Creature — Merfolk Wizard').
card_original_text('razorfin abolisher'/'EVE', '{1}{U}, {T}: Return target creature with a counter on it to its owner\'s hand.').
card_first_print('razorfin abolisher', 'EVE').
card_image_name('razorfin abolisher'/'EVE', 'razorfin abolisher').
card_uid('razorfin abolisher'/'EVE', 'EVE:Razorfin Abolisher:razorfin abolisher').
card_rarity('razorfin abolisher'/'EVE', 'Uncommon').
card_artist('razorfin abolisher'/'EVE', 'William O\'Connor').
card_number('razorfin abolisher'/'EVE', '28').
card_flavor_text('razorfin abolisher'/'EVE', 'Once you have been marked by the merrow, you are not long for their waters.').
card_multiverse_id('razorfin abolisher'/'EVE', '153442').

card_in_set('recumbent bliss', 'EVE').
card_original_type('recumbent bliss'/'EVE', 'Enchantment — Aura').
card_original_text('recumbent bliss'/'EVE', 'Enchant creature\nEnchanted creature can\'t attack or block.\nAt the beginning of your upkeep, you may gain 1 life.').
card_first_print('recumbent bliss', 'EVE').
card_image_name('recumbent bliss'/'EVE', 'recumbent bliss').
card_uid('recumbent bliss'/'EVE', 'EVE:Recumbent Bliss:recumbent bliss').
card_rarity('recumbent bliss'/'EVE', 'Common').
card_artist('recumbent bliss'/'EVE', 'Todd Lockwood').
card_number('recumbent bliss'/'EVE', '13').
card_flavor_text('recumbent bliss'/'EVE', 'Kithkin somnomancers enjoy the peaceful dreams wafting from their victims.').
card_multiverse_id('recumbent bliss'/'EVE', '153435').

card_in_set('regal force', 'EVE').
card_original_type('regal force'/'EVE', 'Creature — Elemental').
card_original_text('regal force'/'EVE', 'When Regal Force comes into play, draw a card for each green creature you control.').
card_first_print('regal force', 'EVE').
card_image_name('regal force'/'EVE', 'regal force').
card_uid('regal force'/'EVE', 'EVE:Regal Force:regal force').
card_rarity('regal force'/'EVE', 'Rare').
card_artist('regal force'/'EVE', 'Brandon Kitkouski').
card_number('regal force'/'EVE', '74').
card_flavor_text('regal force'/'EVE', 'It rarely moves, standing stock still in its clearing. When it does stir, ravens stop their cawing and crickets their chirping. All of nature watches in awed silence.').
card_multiverse_id('regal force'/'EVE', '147373').

card_in_set('rekindled flame', 'EVE').
card_original_type('rekindled flame'/'EVE', 'Sorcery').
card_original_text('rekindled flame'/'EVE', 'Rekindled Flame deals 4 damage to target creature or player.\nAt the beginning of your upkeep, if an opponent has no cards in hand, you may return Rekindled Flame from your graveyard to your hand.').
card_first_print('rekindled flame', 'EVE').
card_image_name('rekindled flame'/'EVE', 'rekindled flame').
card_uid('rekindled flame'/'EVE', 'EVE:Rekindled Flame:rekindled flame').
card_rarity('rekindled flame'/'EVE', 'Rare').
card_artist('rekindled flame'/'EVE', 'Zoltan Boros & Gabor Szikszai').
card_number('rekindled flame'/'EVE', '61').
card_multiverse_id('rekindled flame'/'EVE', '158108').

card_in_set('rendclaw trow', 'EVE').
card_original_type('rendclaw trow'/'EVE', 'Creature — Troll').
card_original_text('rendclaw trow'/'EVE', 'Wither (This deals damage to creatures in the form of -1/-1 counters.)\nPersist (When this creature is put into a graveyard from play, if it had no -1/-1 counters on it, return it to play under its owner\'s control with a -1/-1 counter on it.)').
card_first_print('rendclaw trow', 'EVE').
card_image_name('rendclaw trow'/'EVE', 'rendclaw trow').
card_uid('rendclaw trow'/'EVE', 'EVE:Rendclaw Trow:rendclaw trow').
card_rarity('rendclaw trow'/'EVE', 'Common').
card_artist('rendclaw trow'/'EVE', 'Warren Mahy').
card_number('rendclaw trow'/'EVE', '127').
card_multiverse_id('rendclaw trow'/'EVE', '158902').

card_in_set('restless apparition', 'EVE').
card_original_type('restless apparition'/'EVE', 'Creature — Spirit').
card_original_text('restless apparition'/'EVE', '{W/B}{W/B}{W/B}: Restless Apparition gets +3/+3 until end of turn.\nPersist (When this creature is put into a graveyard from play, if it had no -1/-1 counters on it, return it to play under its owner\'s control with a -1/-1 counter on it.)').
card_first_print('restless apparition', 'EVE').
card_image_name('restless apparition'/'EVE', 'restless apparition').
card_uid('restless apparition'/'EVE', 'EVE:Restless Apparition:restless apparition').
card_rarity('restless apparition'/'EVE', 'Uncommon').
card_artist('restless apparition'/'EVE', 'Jeff Easley').
card_number('restless apparition'/'EVE', '94').
card_multiverse_id('restless apparition'/'EVE', '157291').

card_in_set('rise of the hobgoblins', 'EVE').
card_original_type('rise of the hobgoblins'/'EVE', 'Enchantment').
card_original_text('rise of the hobgoblins'/'EVE', 'When Rise of the Hobgoblins comes into play, you may pay {X}. If you do, put X 1/1 red and white Goblin Soldier creature tokens into play.\n{R/W}: Red creatures and white creatures you control gain first strike until end of turn.').
card_first_print('rise of the hobgoblins', 'EVE').
card_image_name('rise of the hobgoblins'/'EVE', 'rise of the hobgoblins').
card_uid('rise of the hobgoblins'/'EVE', 'EVE:Rise of the Hobgoblins:rise of the hobgoblins').
card_rarity('rise of the hobgoblins'/'EVE', 'Rare').
card_artist('rise of the hobgoblins'/'EVE', 'Jeff Miracola').
card_number('rise of the hobgoblins'/'EVE', '145').
card_flavor_text('rise of the hobgoblins'/'EVE', 'A river even the merrow dare not cross.').
card_multiverse_id('rise of the hobgoblins'/'EVE', '151114').

card_in_set('riverfall mimic', 'EVE').
card_original_type('riverfall mimic'/'EVE', 'Creature — Shapeshifter').
card_original_text('riverfall mimic'/'EVE', 'Whenever you play a spell that\'s both blue and red, Riverfall Mimic becomes 3/3 and is unblockable until end of turn.').
card_first_print('riverfall mimic', 'EVE').
card_image_name('riverfall mimic'/'EVE', 'riverfall mimic').
card_uid('riverfall mimic'/'EVE', 'EVE:Riverfall Mimic:riverfall mimic').
card_rarity('riverfall mimic'/'EVE', 'Common').
card_artist('riverfall mimic'/'EVE', 'Franz Vohwinkel').
card_number('riverfall mimic'/'EVE', '111').
card_flavor_text('riverfall mimic'/'EVE', 'Mimics don\'t hide their natural form well, but their disguises work well enough on boggarts.').
card_multiverse_id('riverfall mimic'/'EVE', '151068').

card_in_set('rugged prairie', 'EVE').
card_original_type('rugged prairie'/'EVE', 'Land').
card_original_text('rugged prairie'/'EVE', '{T}: Add {1} to your mana pool.\n{R/W}, {T}: Add {R}{R}, {R}{W}, or {W}{W} to your mana pool.').
card_first_print('rugged prairie', 'EVE').
card_image_name('rugged prairie'/'EVE', 'rugged prairie').
card_uid('rugged prairie'/'EVE', 'EVE:Rugged Prairie:rugged prairie').
card_rarity('rugged prairie'/'EVE', 'Rare').
card_artist('rugged prairie'/'EVE', 'Fred Fields').
card_number('rugged prairie'/'EVE', '178').
card_flavor_text('rugged prairie'/'EVE', 'Hobs bury their kin far from home. They believe the dry, open ground keeps hags from stealing the bones and gwyllions from stealing the spirits.').
card_multiverse_id('rugged prairie'/'EVE', '153434').

card_in_set('sanity grinding', 'EVE').
card_original_type('sanity grinding'/'EVE', 'Sorcery').
card_original_text('sanity grinding'/'EVE', 'Chroma Reveal the top ten cards of your library. For each blue mana symbol in the mana costs of the revealed cards, target opponent puts the top card of his or her library into his or her graveyard. Then put the cards you revealed this way on the bottom of your library in any order.').
card_first_print('sanity grinding', 'EVE').
card_image_name('sanity grinding'/'EVE', 'sanity grinding').
card_uid('sanity grinding'/'EVE', 'EVE:Sanity Grinding:sanity grinding').
card_rarity('sanity grinding'/'EVE', 'Rare').
card_artist('sanity grinding'/'EVE', 'Lars Grant-West').
card_number('sanity grinding'/'EVE', '29').
card_multiverse_id('sanity grinding'/'EVE', '157204').

card_in_set('sapling of colfenor', 'EVE').
card_original_type('sapling of colfenor'/'EVE', 'Legendary Creature — Treefolk Shaman').
card_original_text('sapling of colfenor'/'EVE', 'Sapling of Colfenor is indestructible.\nWhenever Sapling of Colfenor attacks, reveal the top card of your library. If it\'s a creature card, you gain life equal to that card\'s toughness, lose life equal to its power, then put it into your hand.').
card_first_print('sapling of colfenor', 'EVE').
card_image_name('sapling of colfenor'/'EVE', 'sapling of colfenor').
card_uid('sapling of colfenor'/'EVE', 'EVE:Sapling of Colfenor:sapling of colfenor').
card_rarity('sapling of colfenor'/'EVE', 'Rare').
card_artist('sapling of colfenor'/'EVE', 'John Avon').
card_number('sapling of colfenor'/'EVE', '128').
card_flavor_text('sapling of colfenor'/'EVE', 'Her plans may yet bear fruit.').
card_multiverse_id('sapling of colfenor'/'EVE', '157973').

card_in_set('savage conception', 'EVE').
card_original_type('savage conception'/'EVE', 'Sorcery').
card_original_text('savage conception'/'EVE', 'Put a 3/3 green Beast creature token into play.\nRetrace (You may play this card from your graveyard by discarding a land card in addition to paying its other costs.)').
card_first_print('savage conception', 'EVE').
card_image_name('savage conception'/'EVE', 'savage conception').
card_uid('savage conception'/'EVE', 'EVE:Savage Conception:savage conception').
card_rarity('savage conception'/'EVE', 'Uncommon').
card_artist('savage conception'/'EVE', 'William O\'Connor').
card_number('savage conception'/'EVE', '75').
card_flavor_text('savage conception'/'EVE', 'Some beasts are born not of a mother, but of magic.').
card_multiverse_id('savage conception'/'EVE', '153438').

card_in_set('scarecrone', 'EVE').
card_original_type('scarecrone'/'EVE', 'Artifact Creature — Scarecrow').
card_original_text('scarecrone'/'EVE', '{1}, Sacrifice a Scarecrow: Draw a card.\n{4}, {T}: Return target artifact creature card from your graveyard to play.').
card_first_print('scarecrone', 'EVE').
card_image_name('scarecrone'/'EVE', 'scarecrone').
card_uid('scarecrone'/'EVE', 'EVE:Scarecrone:scarecrone').
card_rarity('scarecrone'/'EVE', 'Rare').
card_artist('scarecrone'/'EVE', 'Jesper Ejsing').
card_number('scarecrone'/'EVE', '172').
card_flavor_text('scarecrone'/'EVE', 'Her poppets bring joy to the truly depraved.').
card_multiverse_id('scarecrone'/'EVE', '157979').

card_in_set('scourge of the nobilis', 'EVE').
card_original_type('scourge of the nobilis'/'EVE', 'Enchantment — Aura').
card_original_text('scourge of the nobilis'/'EVE', 'Enchant creature\nAs long as enchanted creature is red, it gets +1/+1 and has \"{R/W}: This creature gets +1/+0 until end of turn.\"\nAs long as enchanted creature is white, it gets +1/+1 and has lifelink. (Whenever it deals damage, its controller gains that much life.)').
card_first_print('scourge of the nobilis', 'EVE').
card_image_name('scourge of the nobilis'/'EVE', 'scourge of the nobilis').
card_uid('scourge of the nobilis'/'EVE', 'EVE:Scourge of the Nobilis:scourge of the nobilis').
card_rarity('scourge of the nobilis'/'EVE', 'Common').
card_artist('scourge of the nobilis'/'EVE', 'Dan Scott').
card_number('scourge of the nobilis'/'EVE', '146').
card_multiverse_id('scourge of the nobilis'/'EVE', '152015').

card_in_set('selkie hedge-mage', 'EVE').
card_original_type('selkie hedge-mage'/'EVE', 'Creature — Merfolk Wizard').
card_original_text('selkie hedge-mage'/'EVE', 'When Selkie Hedge-Mage comes into play, if you control two or more Forests, you may gain 3 life.\nWhen Selkie Hedge-Mage comes into play, if you control two or more Islands, you may return target tapped creature to its owner\'s hand.').
card_image_name('selkie hedge-mage'/'EVE', 'selkie hedge-mage').
card_uid('selkie hedge-mage'/'EVE', 'EVE:Selkie Hedge-Mage:selkie hedge-mage').
card_rarity('selkie hedge-mage'/'EVE', 'Uncommon').
card_artist('selkie hedge-mage'/'EVE', 'Larry MacDougall').
card_number('selkie hedge-mage'/'EVE', '158').
card_multiverse_id('selkie hedge-mage'/'EVE', '159067').

card_in_set('shell skulkin', 'EVE').
card_original_type('shell skulkin'/'EVE', 'Artifact Creature — Scarecrow').
card_original_text('shell skulkin'/'EVE', '{3}: Target blue creature gains shroud until end of turn.').
card_first_print('shell skulkin', 'EVE').
card_image_name('shell skulkin'/'EVE', 'shell skulkin').
card_uid('shell skulkin'/'EVE', 'EVE:Shell Skulkin:shell skulkin').
card_rarity('shell skulkin'/'EVE', 'Common').
card_artist('shell skulkin'/'EVE', 'Cole Eastburn').
card_number('shell skulkin'/'EVE', '173').
card_flavor_text('shell skulkin'/'EVE', 'Skulkins confer strange benefits on unsuspecting patrons.').
card_multiverse_id('shell skulkin'/'EVE', '158297').

card_in_set('shorecrasher mimic', 'EVE').
card_original_type('shorecrasher mimic'/'EVE', 'Creature — Shapeshifter').
card_original_text('shorecrasher mimic'/'EVE', 'Whenever you play a spell that\'s both green and blue, Shorecrasher Mimic becomes 5/3 and gains trample until end of turn.').
card_first_print('shorecrasher mimic', 'EVE').
card_image_name('shorecrasher mimic'/'EVE', 'shorecrasher mimic').
card_uid('shorecrasher mimic'/'EVE', 'EVE:Shorecrasher Mimic:shorecrasher mimic').
card_rarity('shorecrasher mimic'/'EVE', 'Common').
card_artist('shorecrasher mimic'/'EVE', 'Franz Vohwinkel').
card_number('shorecrasher mimic'/'EVE', '159').
card_flavor_text('shorecrasher mimic'/'EVE', 'The Aurora replaced the changelings\' innocence with malice and their curiosity with hunger.').
card_multiverse_id('shorecrasher mimic'/'EVE', '151147').

card_in_set('shrewd hatchling', 'EVE').
card_original_type('shrewd hatchling'/'EVE', 'Creature — Elemental').
card_original_text('shrewd hatchling'/'EVE', 'Shrewd Hatchling comes into play with four -1/-1 counters on it.\n{U/R}: Target creature can\'t block Shrewd Hatchling this turn.\nWhenever you play a blue spell, remove a -1/-1 counter from Shrewd Hatchling.\nWhenever you play a red spell, remove a -1/-1 counter from Shrewd Hatchling.').
card_first_print('shrewd hatchling', 'EVE').
card_image_name('shrewd hatchling'/'EVE', 'shrewd hatchling').
card_uid('shrewd hatchling'/'EVE', 'EVE:Shrewd Hatchling:shrewd hatchling').
card_rarity('shrewd hatchling'/'EVE', 'Uncommon').
card_artist('shrewd hatchling'/'EVE', 'Carl Frank').
card_number('shrewd hatchling'/'EVE', '112').
card_multiverse_id('shrewd hatchling'/'EVE', '153486').

card_in_set('slippery bogle', 'EVE').
card_original_type('slippery bogle'/'EVE', 'Creature — Beast').
card_original_text('slippery bogle'/'EVE', 'Slippery Bogle can\'t be the target of spells or abilities your opponents control.').
card_first_print('slippery bogle', 'EVE').
card_image_name('slippery bogle'/'EVE', 'slippery bogle').
card_uid('slippery bogle'/'EVE', 'EVE:Slippery Bogle:slippery bogle').
card_rarity('slippery bogle'/'EVE', 'Common').
card_artist('slippery bogle'/'EVE', 'Dave Allsop').
card_number('slippery bogle'/'EVE', '160').
card_flavor_text('slippery bogle'/'EVE', 'Bogles are very tasty if you can get the skin off. It\'s getting a blade on them that\'s the problem.').
card_multiverse_id('slippery bogle'/'EVE', '150999').

card_in_set('smoldering butcher', 'EVE').
card_original_type('smoldering butcher'/'EVE', 'Creature — Elemental Warrior').
card_original_text('smoldering butcher'/'EVE', 'Wither (This deals damage to creatures in the form of -1/-1 counters.)').
card_first_print('smoldering butcher', 'EVE').
card_image_name('smoldering butcher'/'EVE', 'smoldering butcher').
card_uid('smoldering butcher'/'EVE', 'EVE:Smoldering Butcher:smoldering butcher').
card_rarity('smoldering butcher'/'EVE', 'Common').
card_artist('smoldering butcher'/'EVE', 'Zoltan Boros & Gabor Szikszai').
card_number('smoldering butcher'/'EVE', '42').
card_flavor_text('smoldering butcher'/'EVE', 'A cinder\'s axe bears no marks from parrying a foe\'s blade.').
card_multiverse_id('smoldering butcher'/'EVE', '157211').

card_in_set('snakeform', 'EVE').
card_original_type('snakeform'/'EVE', 'Instant').
card_original_text('snakeform'/'EVE', 'Target creature loses all abilities and becomes a 1/1 green Snake until end of turn.\nDraw a card.').
card_first_print('snakeform', 'EVE').
card_image_name('snakeform'/'EVE', 'snakeform').
card_uid('snakeform'/'EVE', 'EVE:Snakeform:snakeform').
card_rarity('snakeform'/'EVE', 'Common').
card_artist('snakeform'/'EVE', 'Jim Nelson').
card_number('snakeform'/'EVE', '161').
card_flavor_text('snakeform'/'EVE', 'Losing his possessions distressed him, but the lack of limbs was strangely liberating.').
card_multiverse_id('snakeform'/'EVE', '157401').

card_in_set('soot imp', 'EVE').
card_original_type('soot imp'/'EVE', 'Creature — Imp').
card_original_text('soot imp'/'EVE', 'Flying\nWhenever a player plays a nonblack spell, that player loses 1 life.').
card_first_print('soot imp', 'EVE').
card_image_name('soot imp'/'EVE', 'soot imp').
card_uid('soot imp'/'EVE', 'EVE:Soot Imp:soot imp').
card_rarity('soot imp'/'EVE', 'Uncommon').
card_artist('soot imp'/'EVE', 'Jesper Ejsing').
card_number('soot imp'/'EVE', '43').
card_flavor_text('soot imp'/'EVE', '\"If one gets in your chimney, you\'re going to need a long wick and a barrel of bangstuff to get it out.\"\n—Hob Heddil').
card_multiverse_id('soot imp'/'EVE', '158900').

card_in_set('soul reap', 'EVE').
card_original_type('soul reap'/'EVE', 'Sorcery').
card_original_text('soul reap'/'EVE', 'Destroy target nongreen creature. Its controller loses 3 life if you played another black spell this turn.').
card_first_print('soul reap', 'EVE').
card_image_name('soul reap'/'EVE', 'soul reap').
card_uid('soul reap'/'EVE', 'EVE:Soul Reap:soul reap').
card_rarity('soul reap'/'EVE', 'Common').
card_artist('soul reap'/'EVE', 'Izzy').
card_number('soul reap'/'EVE', '44').
card_flavor_text('soul reap'/'EVE', 'Their thoughtweft carried Darial\'s last thoughts back to his doun. They sorely wished it had not.').
card_multiverse_id('soul reap'/'EVE', '151150').

card_in_set('soul snuffers', 'EVE').
card_original_type('soul snuffers'/'EVE', 'Creature — Elemental Shaman').
card_original_text('soul snuffers'/'EVE', 'When Soul Snuffers comes into play, put a -1/-1 counter on each creature.').
card_first_print('soul snuffers', 'EVE').
card_image_name('soul snuffers'/'EVE', 'soul snuffers').
card_uid('soul snuffers'/'EVE', 'EVE:Soul Snuffers:soul snuffers').
card_rarity('soul snuffers'/'EVE', 'Uncommon').
card_artist('soul snuffers'/'EVE', 'Izzy').
card_number('soul snuffers'/'EVE', '45').
card_flavor_text('soul snuffers'/'EVE', 'They once sought to unite with the flame of transcendence. Now they seek only to extinguish the lights of others.').
card_multiverse_id('soul snuffers'/'EVE', '142054').

card_in_set('spirit of the hearth', 'EVE').
card_original_type('spirit of the hearth'/'EVE', 'Creature — Cat Spirit').
card_original_text('spirit of the hearth'/'EVE', 'Flying\nYou can\'t be the target of spells or abilities your opponents control.').
card_first_print('spirit of the hearth', 'EVE').
card_image_name('spirit of the hearth'/'EVE', 'spirit of the hearth').
card_uid('spirit of the hearth'/'EVE', 'EVE:Spirit of the Hearth:spirit of the hearth').
card_rarity('spirit of the hearth'/'EVE', 'Rare').
card_artist('spirit of the hearth'/'EVE', 'Jason Chan').
card_number('spirit of the hearth'/'EVE', '14').
card_flavor_text('spirit of the hearth'/'EVE', 'Thieves know that a snarl in the night means, \"Rob another house.\"').
card_multiverse_id('spirit of the hearth'/'EVE', '151169').

card_in_set('spitemare', 'EVE').
card_original_type('spitemare'/'EVE', 'Creature — Elemental').
card_original_text('spitemare'/'EVE', 'Whenever Spitemare is dealt damage, it deals that much damage to target creature or player.').
card_first_print('spitemare', 'EVE').
card_image_name('spitemare'/'EVE', 'spitemare').
card_uid('spitemare'/'EVE', 'EVE:Spitemare:spitemare').
card_rarity('spitemare'/'EVE', 'Uncommon').
card_artist('spitemare'/'EVE', 'Matt Cavotta').
card_number('spitemare'/'EVE', '147').
card_flavor_text('spitemare'/'EVE', '\"I knew a creature carved from a dream of wild freedom. Any attempt to leash or exploit it failed, and at terrible cost.\"\n—Ashling').
card_multiverse_id('spitemare'/'EVE', '152044').

card_in_set('spitting image', 'EVE').
card_original_type('spitting image'/'EVE', 'Sorcery').
card_original_text('spitting image'/'EVE', 'Put a token into play that\'s a copy of target creature.\nRetrace (You may play this card from your graveyard by discarding a land card in addition to paying its other costs.)').
card_first_print('spitting image', 'EVE').
card_image_name('spitting image'/'EVE', 'spitting image').
card_uid('spitting image'/'EVE', 'EVE:Spitting Image:spitting image').
card_rarity('spitting image'/'EVE', 'Rare').
card_artist('spitting image'/'EVE', 'Jim Nelson').
card_number('spitting image'/'EVE', '162').
card_flavor_text('spitting image'/'EVE', 'Spitting is the customary greeting between a creature and its magical impostor.').
card_multiverse_id('spitting image'/'EVE', '154261').

card_in_set('springjack pasture', 'EVE').
card_original_type('springjack pasture'/'EVE', 'Land').
card_original_text('springjack pasture'/'EVE', '{T}: Add {1} to your mana pool.\n{4}, {T}: Put a 0/1 white Goat creature token into play.\n{T}, Sacrifice X Goats: Add X mana of any one color to your mana pool. You gain X life.').
card_first_print('springjack pasture', 'EVE').
card_image_name('springjack pasture'/'EVE', 'springjack pasture').
card_uid('springjack pasture'/'EVE', 'EVE:Springjack Pasture:springjack pasture').
card_rarity('springjack pasture'/'EVE', 'Rare').
card_artist('springjack pasture'/'EVE', 'Terese Nielsen').
card_number('springjack pasture'/'EVE', '179').
card_multiverse_id('springjack pasture'/'EVE', '157976').

card_in_set('springjack shepherd', 'EVE').
card_original_type('springjack shepherd'/'EVE', 'Creature — Kithkin Wizard').
card_original_text('springjack shepherd'/'EVE', 'Chroma When Springjack Shepherd comes into play, put a 0/1 white Goat creature token into play for each white mana symbol in the mana costs of permanents you control.').
card_first_print('springjack shepherd', 'EVE').
card_image_name('springjack shepherd'/'EVE', 'springjack shepherd').
card_uid('springjack shepherd'/'EVE', 'EVE:Springjack Shepherd:springjack shepherd').
card_rarity('springjack shepherd'/'EVE', 'Uncommon').
card_artist('springjack shepherd'/'EVE', 'Thomas Denmark').
card_number('springjack shepherd'/'EVE', '15').
card_flavor_text('springjack shepherd'/'EVE', '\"Better for a doun to lose its cenn than its springjacks.\"').
card_multiverse_id('springjack shepherd'/'EVE', '153032').

card_in_set('stalker hag', 'EVE').
card_original_type('stalker hag'/'EVE', 'Creature — Hag').
card_original_text('stalker hag'/'EVE', 'Swampwalk, forestwalk').
card_first_print('stalker hag', 'EVE').
card_image_name('stalker hag'/'EVE', 'stalker hag').
card_uid('stalker hag'/'EVE', 'EVE:Stalker Hag:stalker hag').
card_rarity('stalker hag'/'EVE', 'Uncommon').
card_artist('stalker hag'/'EVE', 'Dave Kendall').
card_number('stalker hag'/'EVE', '129').
card_flavor_text('stalker hag'/'EVE', 'Her hunt is silent until a bone snaps or flesh tears. Her victims never know to scream.').
card_multiverse_id('stalker hag'/'EVE', '151120').

card_in_set('stigma lasher', 'EVE').
card_original_type('stigma lasher'/'EVE', 'Creature — Elemental Shaman').
card_original_text('stigma lasher'/'EVE', 'Wither (This deals damage to creatures in the form of -1/-1 counters.)\nWhenever Stigma Lasher deals damage to a player, that player can\'t gain life for the rest of the game.').
card_first_print('stigma lasher', 'EVE').
card_image_name('stigma lasher'/'EVE', 'stigma lasher').
card_uid('stigma lasher'/'EVE', 'EVE:Stigma Lasher:stigma lasher').
card_rarity('stigma lasher'/'EVE', 'Rare').
card_artist('stigma lasher'/'EVE', 'Aleksi Briclot').
card_number('stigma lasher'/'EVE', '62').
card_flavor_text('stigma lasher'/'EVE', 'The Extinguisher\'s curse lasted long after her departure.').
card_multiverse_id('stigma lasher'/'EVE', '153049').

card_in_set('stillmoon cavalier', 'EVE').
card_original_type('stillmoon cavalier'/'EVE', 'Creature — Zombie Knight').
card_original_text('stillmoon cavalier'/'EVE', 'Protection from white and from black\n{W/B}: Stillmoon Cavalier gains flying until end of turn.\n{W/B}: Stillmoon Cavalier gains first strike until end of turn.\n{W/B}{W/B}: Stillmoon Cavalier gets +1/+0 until end of turn.').
card_first_print('stillmoon cavalier', 'EVE').
card_image_name('stillmoon cavalier'/'EVE', 'stillmoon cavalier').
card_uid('stillmoon cavalier'/'EVE', 'EVE:Stillmoon Cavalier:stillmoon cavalier').
card_rarity('stillmoon cavalier'/'EVE', 'Rare').
card_artist('stillmoon cavalier'/'EVE', 'Christopher Moeller').
card_number('stillmoon cavalier'/'EVE', '95').
card_multiverse_id('stillmoon cavalier'/'EVE', '153037').

card_in_set('stream hopper', 'EVE').
card_original_type('stream hopper'/'EVE', 'Creature — Goblin').
card_original_text('stream hopper'/'EVE', '{U/R}: Stream Hopper gains flying until end of turn.').
card_first_print('stream hopper', 'EVE').
card_image_name('stream hopper'/'EVE', 'stream hopper').
card_uid('stream hopper'/'EVE', 'EVE:Stream Hopper:stream hopper').
card_rarity('stream hopper'/'EVE', 'Common').
card_artist('stream hopper'/'EVE', 'Dave Allsop').
card_number('stream hopper'/'EVE', '113').
card_flavor_text('stream hopper'/'EVE', 'Like many who dwell in Shadowmoor now, he was robbed of his better half by the Aurora.').
card_multiverse_id('stream hopper'/'EVE', '151124').

card_in_set('sturdy hatchling', 'EVE').
card_original_type('sturdy hatchling'/'EVE', 'Creature — Elemental').
card_original_text('sturdy hatchling'/'EVE', 'Sturdy Hatchling comes into play with four -1/-1 counters on it.\n{G/U}: Sturdy Hatchling gains shroud until end of turn.\nWhenever you play a green spell, remove a -1/-1 counter from Sturdy Hatchling.\nWhenever you play a blue spell, remove a -1/-1 counter from Sturdy Hatchling.').
card_first_print('sturdy hatchling', 'EVE').
card_image_name('sturdy hatchling'/'EVE', 'sturdy hatchling').
card_uid('sturdy hatchling'/'EVE', 'EVE:Sturdy Hatchling:sturdy hatchling').
card_rarity('sturdy hatchling'/'EVE', 'Uncommon').
card_artist('sturdy hatchling'/'EVE', 'Darrell Riche').
card_number('sturdy hatchling'/'EVE', '163').
card_multiverse_id('sturdy hatchling'/'EVE', '153477').

card_in_set('suture spirit', 'EVE').
card_original_type('suture spirit'/'EVE', 'Creature — Spirit').
card_original_text('suture spirit'/'EVE', 'Flying\n{W/B}{W/B}{W/B}: Regenerate target creature.').
card_first_print('suture spirit', 'EVE').
card_image_name('suture spirit'/'EVE', 'suture spirit').
card_uid('suture spirit'/'EVE', 'EVE:Suture Spirit:suture spirit').
card_rarity('suture spirit'/'EVE', 'Uncommon').
card_artist('suture spirit'/'EVE', 'Larry MacDougall').
card_number('suture spirit'/'EVE', '16').
card_flavor_text('suture spirit'/'EVE', 'Is her need to heal born out of compassion or penance?').
card_multiverse_id('suture spirit'/'EVE', '153420').

card_in_set('swirling spriggan', 'EVE').
card_original_type('swirling spriggan'/'EVE', 'Creature — Goblin Shaman').
card_original_text('swirling spriggan'/'EVE', '{G/U}{G/U}: Target creature you control becomes the color or colors of your choice until end of turn.').
card_first_print('swirling spriggan', 'EVE').
card_image_name('swirling spriggan'/'EVE', 'swirling spriggan').
card_uid('swirling spriggan'/'EVE', 'EVE:Swirling Spriggan:swirling spriggan').
card_rarity('swirling spriggan'/'EVE', 'Uncommon').
card_artist('swirling spriggan'/'EVE', 'Thomas M. Baxa').
card_number('swirling spriggan'/'EVE', '76').
card_flavor_text('swirling spriggan'/'EVE', 'After its encounter with the spriggan, the patrol was red with anger, green with envy, white with fear, and painfully black and blue.').
card_multiverse_id('swirling spriggan'/'EVE', '154348').

card_in_set('syphon life', 'EVE').
card_original_type('syphon life'/'EVE', 'Sorcery').
card_original_text('syphon life'/'EVE', 'Target player loses 2 life and you gain 2 life.\nRetrace (You may play this card from your graveyard by discarding a land card in addition to paying its other costs.)').
card_first_print('syphon life', 'EVE').
card_image_name('syphon life'/'EVE', 'syphon life').
card_uid('syphon life'/'EVE', 'EVE:Syphon Life:syphon life').
card_rarity('syphon life'/'EVE', 'Uncommon').
card_artist('syphon life'/'EVE', 'Dan Seagrave').
card_number('syphon life'/'EVE', '46').
card_flavor_text('syphon life'/'EVE', 'Leeches never tire of feeding.').
card_multiverse_id('syphon life'/'EVE', '153447').

card_in_set('talara\'s bane', 'EVE').
card_original_type('talara\'s bane'/'EVE', 'Sorcery').
card_original_text('talara\'s bane'/'EVE', 'Target opponent reveals his or her hand. Choose a green or white creature card from it. You gain life equal that creature card\'s toughness, then that player discards that card.').
card_first_print('talara\'s bane', 'EVE').
card_image_name('talara\'s bane'/'EVE', 'talara\'s bane').
card_uid('talara\'s bane'/'EVE', 'EVE:Talara\'s Bane:talara\'s bane').
card_rarity('talara\'s bane'/'EVE', 'Common').
card_artist('talara\'s bane'/'EVE', 'Joshua Hagler').
card_number('talara\'s bane'/'EVE', '47').
card_flavor_text('talara\'s bane'/'EVE', '\"Your thoughts betray you.\"\n—Illulia of Nighthearth').
card_multiverse_id('talara\'s bane'/'EVE', '151142').

card_in_set('talara\'s battalion', 'EVE').
card_original_type('talara\'s battalion'/'EVE', 'Creature — Elf Warrior').
card_original_text('talara\'s battalion'/'EVE', 'Trample\nPlay Talara\'s Battalion only if you played another green spell this turn.').
card_first_print('talara\'s battalion', 'EVE').
card_image_name('talara\'s battalion'/'EVE', 'talara\'s battalion').
card_uid('talara\'s battalion'/'EVE', 'EVE:Talara\'s Battalion:talara\'s battalion').
card_rarity('talara\'s battalion'/'EVE', 'Rare').
card_artist('talara\'s battalion'/'EVE', 'Todd Lockwood').
card_number('talara\'s battalion'/'EVE', '77').
card_flavor_text('talara\'s battalion'/'EVE', 'The safewright Talara protects a delicate woodland waterfall. She has vowed to send wave after wave of warriors to slay any who would dare disturb its beauty.').
card_multiverse_id('talara\'s battalion'/'EVE', '157395').

card_in_set('talonrend', 'EVE').
card_original_type('talonrend'/'EVE', 'Creature — Elemental').
card_original_text('talonrend'/'EVE', 'Flying\n{U/R}: Talonrend gets +1/-1 until end of turn.').
card_first_print('talonrend', 'EVE').
card_image_name('talonrend'/'EVE', 'talonrend').
card_uid('talonrend'/'EVE', 'EVE:Talonrend:talonrend').
card_rarity('talonrend'/'EVE', 'Uncommon').
card_artist('talonrend'/'EVE', 'Nils Hamm').
card_number('talonrend'/'EVE', '30').
card_flavor_text('talonrend'/'EVE', 'Change is a denizen of Shadowmoor as much as ouphes and noggles are.').
card_multiverse_id('talonrend'/'EVE', '153430').

card_in_set('thunderblust', 'EVE').
card_original_type('thunderblust'/'EVE', 'Creature — Elemental').
card_original_text('thunderblust'/'EVE', 'Haste\nThunderblust has trample as long as it has a -1/-1 counter on it.\nPersist (When this creature is put into a graveyard from play, if it had no -1/-1 counters on it, return it to play under its owner\'s control with a -1/-1 counter on it.)').
card_first_print('thunderblust', 'EVE').
card_image_name('thunderblust'/'EVE', 'thunderblust').
card_uid('thunderblust'/'EVE', 'EVE:Thunderblust:thunderblust').
card_rarity('thunderblust'/'EVE', 'Rare').
card_artist('thunderblust'/'EVE', 'Dan Scott').
card_number('thunderblust'/'EVE', '63').
card_multiverse_id('thunderblust'/'EVE', '157411').

card_in_set('tilling treefolk', 'EVE').
card_original_type('tilling treefolk'/'EVE', 'Creature — Treefolk Druid').
card_original_text('tilling treefolk'/'EVE', 'When Tilling Treefolk comes into play, you may return up to two target land cards from your graveyard to your hand.').
card_first_print('tilling treefolk', 'EVE').
card_image_name('tilling treefolk'/'EVE', 'tilling treefolk').
card_uid('tilling treefolk'/'EVE', 'EVE:Tilling Treefolk:tilling treefolk').
card_rarity('tilling treefolk'/'EVE', 'Common').
card_artist('tilling treefolk'/'EVE', 'rk post').
card_number('tilling treefolk'/'EVE', '78').
card_flavor_text('tilling treefolk'/'EVE', 'He digs his roots into barren ground, searching for old seeds that never had the chance to sprout.').
card_multiverse_id('tilling treefolk'/'EVE', '151096').

card_in_set('trapjaw kelpie', 'EVE').
card_original_type('trapjaw kelpie'/'EVE', 'Creature — Beast').
card_original_text('trapjaw kelpie'/'EVE', 'Flash\nPersist (When this creature is put into a graveyard from play, if it had no -1/-1 counters on it, return it to play under its owner\'s control with a -1/-1 counter on it.)').
card_first_print('trapjaw kelpie', 'EVE').
card_image_name('trapjaw kelpie'/'EVE', 'trapjaw kelpie').
card_uid('trapjaw kelpie'/'EVE', 'EVE:Trapjaw Kelpie:trapjaw kelpie').
card_rarity('trapjaw kelpie'/'EVE', 'Common').
card_artist('trapjaw kelpie'/'EVE', 'Lars Grant-West').
card_number('trapjaw kelpie'/'EVE', '164').
card_flavor_text('trapjaw kelpie'/'EVE', 'Travelers in these waters leave a bit of themselves behind.').
card_multiverse_id('trapjaw kelpie'/'EVE', '152140').

card_in_set('twilight mire', 'EVE').
card_original_type('twilight mire'/'EVE', 'Land').
card_original_text('twilight mire'/'EVE', '{T}: Add {1} to your mana pool.\n{B/G}, {T}: Add {B}{B}, {B}{G}, or {G}{G} to your mana pool.').
card_first_print('twilight mire', 'EVE').
card_image_name('twilight mire'/'EVE', 'twilight mire').
card_uid('twilight mire'/'EVE', 'EVE:Twilight Mire:twilight mire').
card_rarity('twilight mire'/'EVE', 'Rare').
card_artist('twilight mire'/'EVE', 'Rob Alexander').
card_number('twilight mire'/'EVE', '180').
card_flavor_text('twilight mire'/'EVE', 'As their roots soaked in a weak tea of bogwater and decay, the trees absorbed the essence of death into their living tissue.').
card_multiverse_id('twilight mire'/'EVE', '153425').

card_in_set('twinblade slasher', 'EVE').
card_original_type('twinblade slasher'/'EVE', 'Creature — Elf Warrior').
card_original_text('twinblade slasher'/'EVE', 'Wither (This deals damage to creatures in the form of -1/-1 counters.)\n{1}{G}: Twinblade Slasher gets +2/+2 until end of turn. Play this ability only once each turn.').
card_first_print('twinblade slasher', 'EVE').
card_image_name('twinblade slasher'/'EVE', 'twinblade slasher').
card_uid('twinblade slasher'/'EVE', 'EVE:Twinblade Slasher:twinblade slasher').
card_rarity('twinblade slasher'/'EVE', 'Uncommon').
card_artist('twinblade slasher'/'EVE', 'Trevor Hairsine').
card_number('twinblade slasher'/'EVE', '79').
card_flavor_text('twinblade slasher'/'EVE', 'Righteous anger hones a humble soul.').
card_multiverse_id('twinblade slasher'/'EVE', '153436').

card_in_set('umbra stalker', 'EVE').
card_original_type('umbra stalker'/'EVE', 'Creature — Elemental').
card_original_text('umbra stalker'/'EVE', 'Chroma Umbra Stalker\'s power and toughness are each equal to the number of black mana symbols in the mana costs of cards in your graveyard.').
card_first_print('umbra stalker', 'EVE').
card_image_name('umbra stalker'/'EVE', 'umbra stalker').
card_uid('umbra stalker'/'EVE', 'EVE:Umbra Stalker:umbra stalker').
card_rarity('umbra stalker'/'EVE', 'Rare').
card_artist('umbra stalker'/'EVE', 'Daarken').
card_number('umbra stalker'/'EVE', '48').
card_flavor_text('umbra stalker'/'EVE', 'Don\'t venture into the darkness, lest it decide not to let you leave.').
card_multiverse_id('umbra stalker'/'EVE', '157399').

card_in_set('unmake', 'EVE').
card_original_type('unmake'/'EVE', 'Instant').
card_original_text('unmake'/'EVE', 'Remove target creature from the game.').
card_image_name('unmake'/'EVE', 'unmake').
card_uid('unmake'/'EVE', 'EVE:Unmake:unmake').
card_rarity('unmake'/'EVE', 'Common').
card_artist('unmake'/'EVE', 'Steven Belledin').
card_number('unmake'/'EVE', '96').
card_flavor_text('unmake'/'EVE', 'A gwyllion\'s favorite trap is the vanity mirror. A bewitched piece of glass traps the looker\'s soul and does away with the body.').
card_multiverse_id('unmake'/'EVE', '151083').

card_in_set('unnerving assault', 'EVE').
card_original_type('unnerving assault'/'EVE', 'Instant').
card_original_text('unnerving assault'/'EVE', 'Creatures your opponents control get -1/-0 until end of turn if {U} was spent to play Unnerving Assault, and creatures you control get +1/+0 until end of turn if {R} was spent to play it. (Do both if {U}{R} was spent.)').
card_first_print('unnerving assault', 'EVE').
card_image_name('unnerving assault'/'EVE', 'unnerving assault').
card_uid('unnerving assault'/'EVE', 'EVE:Unnerving Assault:unnerving assault').
card_rarity('unnerving assault'/'EVE', 'Uncommon').
card_artist('unnerving assault'/'EVE', 'Ron Spencer').
card_number('unnerving assault'/'EVE', '114').
card_flavor_text('unnerving assault'/'EVE', 'The scales of battle rarely balance.').
card_multiverse_id('unnerving assault'/'EVE', '157416').

card_in_set('unwilling recruit', 'EVE').
card_original_type('unwilling recruit'/'EVE', 'Sorcery').
card_original_text('unwilling recruit'/'EVE', 'Gain control of target creature until end of turn. Untap that creature. It gets +X/+0 and gains haste until end of turn.').
card_first_print('unwilling recruit', 'EVE').
card_image_name('unwilling recruit'/'EVE', 'unwilling recruit').
card_uid('unwilling recruit'/'EVE', 'EVE:Unwilling Recruit:unwilling recruit').
card_rarity('unwilling recruit'/'EVE', 'Uncommon').
card_artist('unwilling recruit'/'EVE', 'Dave Allsop').
card_number('unwilling recruit'/'EVE', '64').
card_flavor_text('unwilling recruit'/'EVE', 'Frorn didn\'t want to be kept, but he didn\'t want to be thrown back either.').
card_multiverse_id('unwilling recruit'/'EVE', '157202').

card_in_set('voracious hatchling', 'EVE').
card_original_type('voracious hatchling'/'EVE', 'Creature — Elemental').
card_original_text('voracious hatchling'/'EVE', 'Lifelink\nVoracious Hatchling comes into play with four -1/-1 counters on it.\nWhenever you play a white spell, remove a -1/-1 counter from Voracious Hatchling.\nWhenever you play a black spell, remove a -1/-1 counter from Voracious Hatchling.').
card_first_print('voracious hatchling', 'EVE').
card_image_name('voracious hatchling'/'EVE', 'voracious hatchling').
card_uid('voracious hatchling'/'EVE', 'EVE:Voracious Hatchling:voracious hatchling').
card_rarity('voracious hatchling'/'EVE', 'Uncommon').
card_artist('voracious hatchling'/'EVE', 'Jeremy Enecio').
card_number('voracious hatchling'/'EVE', '97').
card_multiverse_id('voracious hatchling'/'EVE', '153444').

card_in_set('wake thrasher', 'EVE').
card_original_type('wake thrasher'/'EVE', 'Creature — Merfolk Soldier').
card_original_text('wake thrasher'/'EVE', 'Whenever a permanent you control becomes untapped, Wake Thrasher gets +1/+1 until end of turn.').
card_first_print('wake thrasher', 'EVE').
card_image_name('wake thrasher'/'EVE', 'wake thrasher').
card_uid('wake thrasher'/'EVE', 'EVE:Wake Thrasher:wake thrasher').
card_rarity('wake thrasher'/'EVE', 'Rare').
card_artist('wake thrasher'/'EVE', 'Jesper Ejsing').
card_number('wake thrasher'/'EVE', '31').
card_flavor_text('wake thrasher'/'EVE', 'Anyone who thinks merrows have more brains than brawn has never been run over by one.').
card_multiverse_id('wake thrasher'/'EVE', '157405').

card_in_set('ward of bones', 'EVE').
card_original_type('ward of bones'/'EVE', 'Artifact').
card_original_text('ward of bones'/'EVE', 'Each opponent who controls more creatures than you can\'t play creature cards. The same is true for artifacts, enchantments, and lands.').
card_first_print('ward of bones', 'EVE').
card_image_name('ward of bones'/'EVE', 'ward of bones').
card_uid('ward of bones'/'EVE', 'EVE:Ward of Bones:ward of bones').
card_rarity('ward of bones'/'EVE', 'Rare').
card_artist('ward of bones'/'EVE', 'David Martin').
card_number('ward of bones'/'EVE', '174').
card_flavor_text('ward of bones'/'EVE', 'Mages dare not attempt magic in its grotesque presence.').
card_multiverse_id('ward of bones'/'EVE', '157972').

card_in_set('waves of aggression', 'EVE').
card_original_type('waves of aggression'/'EVE', 'Sorcery').
card_original_text('waves of aggression'/'EVE', 'Untap all creatures that attacked this turn. After this main phase, there is an additional combat phase followed by an additional main phase.\nRetrace (You may play this card from your graveyard by discarding a land card in addition to paying its other costs.)').
card_first_print('waves of aggression', 'EVE').
card_image_name('waves of aggression'/'EVE', 'waves of aggression').
card_uid('waves of aggression'/'EVE', 'EVE:Waves of Aggression:waves of aggression').
card_rarity('waves of aggression'/'EVE', 'Rare').
card_artist('waves of aggression'/'EVE', 'Jim Pavelec').
card_number('waves of aggression'/'EVE', '148').
card_multiverse_id('waves of aggression'/'EVE', '158107').

card_in_set('wickerbough elder', 'EVE').
card_original_type('wickerbough elder'/'EVE', 'Creature — Treefolk Shaman').
card_original_text('wickerbough elder'/'EVE', 'Wickerbough Elder comes into play with a -1/-1 counter on it.\n{G}, Remove a -1/-1 counter from Wickerbough Elder: Destroy target artifact or enchantment.').
card_first_print('wickerbough elder', 'EVE').
card_image_name('wickerbough elder'/'EVE', 'wickerbough elder').
card_uid('wickerbough elder'/'EVE', 'EVE:Wickerbough Elder:wickerbough elder').
card_rarity('wickerbough elder'/'EVE', 'Common').
card_artist('wickerbough elder'/'EVE', 'Jesper Ejsing').
card_number('wickerbough elder'/'EVE', '80').
card_flavor_text('wickerbough elder'/'EVE', '\"Living scarecrows make a mockery of the natural order. Dead ones make fine hats.\"').
card_multiverse_id('wickerbough elder'/'EVE', '151097').

card_in_set('wilderness hypnotist', 'EVE').
card_original_type('wilderness hypnotist'/'EVE', 'Creature — Merfolk Wizard').
card_original_text('wilderness hypnotist'/'EVE', '{T}: Target red or green creature gets -2/-0 until end of turn.').
card_first_print('wilderness hypnotist', 'EVE').
card_image_name('wilderness hypnotist'/'EVE', 'wilderness hypnotist').
card_uid('wilderness hypnotist'/'EVE', 'EVE:Wilderness Hypnotist:wilderness hypnotist').
card_rarity('wilderness hypnotist'/'EVE', 'Common').
card_artist('wilderness hypnotist'/'EVE', 'Rebecca Guay').
card_number('wilderness hypnotist'/'EVE', '32').
card_flavor_text('wilderness hypnotist'/'EVE', '\"I felt as if I were moving in brackish water, as if I were drowning, even though I knew I stood on dry land.\"\n—Dindun of Kulrath Mine').
card_multiverse_id('wilderness hypnotist'/'EVE', '153476').

card_in_set('wistful selkie', 'EVE').
card_original_type('wistful selkie'/'EVE', 'Creature — Merfolk Wizard').
card_original_text('wistful selkie'/'EVE', 'When Wistful Selkie comes into play, draw a card.').
card_first_print('wistful selkie', 'EVE').
card_image_name('wistful selkie'/'EVE', 'wistful selkie').
card_uid('wistful selkie'/'EVE', 'EVE:Wistful Selkie:wistful selkie').
card_rarity('wistful selkie'/'EVE', 'Uncommon').
card_artist('wistful selkie'/'EVE', 'Mark Tedin').
card_number('wistful selkie'/'EVE', '165').
card_flavor_text('wistful selkie'/'EVE', 'Selkies call to a sea they never swam, in a tongue they never spoke, with a song they never learned.').
card_multiverse_id('wistful selkie'/'EVE', '151143').

card_in_set('woodlurker mimic', 'EVE').
card_original_type('woodlurker mimic'/'EVE', 'Creature — Shapeshifter').
card_original_text('woodlurker mimic'/'EVE', 'Whenever you play a spell that\'s both black and green, Woodlurker Mimic becomes 4/5 and gains wither until end of turn. (It deals damage to creatures in the form of -1/-1 counters.)').
card_first_print('woodlurker mimic', 'EVE').
card_image_name('woodlurker mimic'/'EVE', 'woodlurker mimic').
card_uid('woodlurker mimic'/'EVE', 'EVE:Woodlurker Mimic:woodlurker mimic').
card_rarity('woodlurker mimic'/'EVE', 'Common').
card_artist('woodlurker mimic'/'EVE', 'Franz Vohwinkel').
card_number('woodlurker mimic'/'EVE', '130').
card_flavor_text('woodlurker mimic'/'EVE', 'A mimic\'s disguise fools its victims just long enough to draw them within range of its venomous tentacles.').
card_multiverse_id('woodlurker mimic'/'EVE', '158599').

card_in_set('worm harvest', 'EVE').
card_original_type('worm harvest'/'EVE', 'Sorcery').
card_original_text('worm harvest'/'EVE', 'Put a 1/1 black and green Worm creature token into play for each land card in your graveyard.\nRetrace (You may play this card from your graveyard by discarding a land card in addition to paying its other costs.)').
card_first_print('worm harvest', 'EVE').
card_image_name('worm harvest'/'EVE', 'worm harvest').
card_uid('worm harvest'/'EVE', 'EVE:Worm Harvest:worm harvest').
card_rarity('worm harvest'/'EVE', 'Rare').
card_artist('worm harvest'/'EVE', 'Chuck Lukacs').
card_number('worm harvest'/'EVE', '131').
card_multiverse_id('worm harvest'/'EVE', '151135').
