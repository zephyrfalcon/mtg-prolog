% Planeshift

set('PLS').
set_name('PLS', 'Planeshift').
set_release_date('PLS', '2001-02-05').
set_border('PLS', 'black').
set_type('PLS', 'expansion').
set_block('PLS', 'Invasion').

card_in_set('allied strategies', 'PLS').
card_original_type('allied strategies'/'PLS', 'Sorcery').
card_original_text('allied strategies'/'PLS', 'Target player draws a card for each basic land type among lands he or she controls.').
card_first_print('allied strategies', 'PLS').
card_image_name('allied strategies'/'PLS', 'allied strategies').
card_uid('allied strategies'/'PLS', 'PLS:Allied Strategies:allied strategies').
card_rarity('allied strategies'/'PLS', 'Uncommon').
card_artist('allied strategies'/'PLS', 'Paolo Parente').
card_number('allied strategies'/'PLS', '20').
card_flavor_text('allied strategies'/'PLS', 'Each commander looked at the others and wondered who would be first to break the alliance.').
card_multiverse_id('allied strategies'/'PLS', '26383').

card_in_set('alpha kavu', 'PLS').
card_original_type('alpha kavu'/'PLS', 'Creature — Kavu').
card_original_text('alpha kavu'/'PLS', '{1}{G}: Target Kavu gets -1/+1 until end of turn.').
card_first_print('alpha kavu', 'PLS').
card_image_name('alpha kavu'/'PLS', 'alpha kavu').
card_uid('alpha kavu'/'PLS', 'PLS:Alpha Kavu:alpha kavu').
card_rarity('alpha kavu'/'PLS', 'Uncommon').
card_artist('alpha kavu'/'PLS', 'Matt Cavotta').
card_number('alpha kavu'/'PLS', '77').
card_flavor_text('alpha kavu'/'PLS', 'Kavu leaders instinctively protect their herds from harm.').
card_multiverse_id('alpha kavu'/'PLS', '27613').

card_in_set('amphibious kavu', 'PLS').
card_original_type('amphibious kavu'/'PLS', 'Creature — Kavu').
card_original_text('amphibious kavu'/'PLS', 'Whenever Amphibious Kavu blocks or becomes blocked by one or more blue and/or black creatures, Amphibious Kavu gets +3/+3 until end of turn.').
card_first_print('amphibious kavu', 'PLS').
card_image_name('amphibious kavu'/'PLS', 'amphibious kavu').
card_uid('amphibious kavu'/'PLS', 'PLS:Amphibious Kavu:amphibious kavu').
card_rarity('amphibious kavu'/'PLS', 'Common').
card_artist('amphibious kavu'/'PLS', 'Wayne England').
card_number('amphibious kavu'/'PLS', '78').
card_flavor_text('amphibious kavu'/'PLS', 'Merfolk raided the shores untroubled for years . . . until the shores started fighting back.').
card_multiverse_id('amphibious kavu'/'PLS', '26409').

card_in_set('ancient spider', 'PLS').
card_original_type('ancient spider'/'PLS', 'Creature — Spider').
card_original_text('ancient spider'/'PLS', 'First strike\nAncient Spider may block as though it had flying.').
card_first_print('ancient spider', 'PLS').
card_image_name('ancient spider'/'PLS', 'ancient spider').
card_uid('ancient spider'/'PLS', 'PLS:Ancient Spider:ancient spider').
card_rarity('ancient spider'/'PLS', 'Rare').
card_artist('ancient spider'/'PLS', 'Greg Staples').
card_number('ancient spider'/'PLS', '96').
card_flavor_text('ancient spider'/'PLS', 'It outlived both king and castle.').
card_multiverse_id('ancient spider'/'PLS', '19149').

card_in_set('arctic merfolk', 'PLS').
card_original_type('arctic merfolk'/'PLS', 'Creature — Merfolk').
card_original_text('arctic merfolk'/'PLS', 'Kicker—Return a creature you control to its owner\'s hand. (You may return a creature you control to its owner\'s hand in addition to any other costs as you play this spell.)\nIf you paid the kicker cost, Arctic Merfolk comes into play with a +1/+1 counter on it.').
card_first_print('arctic merfolk', 'PLS').
card_image_name('arctic merfolk'/'PLS', 'arctic merfolk').
card_uid('arctic merfolk'/'PLS', 'PLS:Arctic Merfolk:arctic merfolk').
card_rarity('arctic merfolk'/'PLS', 'Common').
card_artist('arctic merfolk'/'PLS', 'Ron Spears').
card_number('arctic merfolk'/'PLS', '21').
card_multiverse_id('arctic merfolk'/'PLS', '26360').

card_in_set('aura blast', 'PLS').
card_original_type('aura blast'/'PLS', 'Instant').
card_original_text('aura blast'/'PLS', 'Destroy target enchantment.\nDraw a card.').
card_first_print('aura blast', 'PLS').
card_image_name('aura blast'/'PLS', 'aura blast').
card_uid('aura blast'/'PLS', 'PLS:Aura Blast:aura blast').
card_rarity('aura blast'/'PLS', 'Common').
card_artist('aura blast'/'PLS', 'Ron Walotsky').
card_number('aura blast'/'PLS', '1').
card_flavor_text('aura blast'/'PLS', '\"None of your history books mention me?\" laughed Daria. \"They will now.\"').
card_multiverse_id('aura blast'/'PLS', '26382').

card_in_set('aurora griffin', 'PLS').
card_original_type('aurora griffin'/'PLS', 'Creature — Griffin').
card_original_text('aurora griffin'/'PLS', 'Flying\n{W}: Target permanent becomes white until end of turn.').
card_first_print('aurora griffin', 'PLS').
card_image_name('aurora griffin'/'PLS', 'aurora griffin').
card_uid('aurora griffin'/'PLS', 'PLS:Aurora Griffin:aurora griffin').
card_rarity('aurora griffin'/'PLS', 'Common').
card_artist('aurora griffin'/'PLS', 'Ciruelo').
card_number('aurora griffin'/'PLS', '2').
card_flavor_text('aurora griffin'/'PLS', 'Though a fierce fighter, its true value is that it inspires beleaguered forces.').
card_multiverse_id('aurora griffin'/'PLS', '26284').

card_in_set('bog down', 'PLS').
card_original_type('bog down'/'PLS', 'Sorcery').
card_original_text('bog down'/'PLS', 'Kicker—Sacrifice two lands. (You may sacrifice two lands in addition to any other costs as you play this spell.)\nTarget player discards two cards from his or her hand. If you paid the kicker cost, that player discards three cards from his or her hand instead.').
card_first_print('bog down', 'PLS').
card_image_name('bog down'/'PLS', 'bog down').
card_uid('bog down'/'PLS', 'PLS:Bog Down:bog down').
card_rarity('bog down'/'PLS', 'Common').
card_artist('bog down'/'PLS', 'Andrew Goldhawk').
card_number('bog down'/'PLS', '39').
card_multiverse_id('bog down'/'PLS', '25943').

card_in_set('caldera kavu', 'PLS').
card_original_type('caldera kavu'/'PLS', 'Creature — Kavu').
card_original_text('caldera kavu'/'PLS', '{1}{B}: Caldera Kavu gets +1/+1 until end of turn.\n{G}: Caldera Kavu becomes the color of your choice until end of turn.').
card_first_print('caldera kavu', 'PLS').
card_image_name('caldera kavu'/'PLS', 'caldera kavu').
card_uid('caldera kavu'/'PLS', 'PLS:Caldera Kavu:caldera kavu').
card_rarity('caldera kavu'/'PLS', 'Common').
card_artist('caldera kavu'/'PLS', 'Arnie Swekel').
card_number('caldera kavu'/'PLS', '58').
card_flavor_text('caldera kavu'/'PLS', 'As powerful and malleable as the molten rock it calls home.').
card_multiverse_id('caldera kavu'/'PLS', '26437').

card_in_set('cavern harpy', 'PLS').
card_original_type('cavern harpy'/'PLS', 'Creature — Beast').
card_original_text('cavern harpy'/'PLS', 'Flying\nWhen Cavern Harpy comes into play, return a blue or black creature you control to its owner\'s hand.\nPay 1 life: Return Cavern Harpy to its owner\'s hand.').
card_first_print('cavern harpy', 'PLS').
card_image_name('cavern harpy'/'PLS', 'cavern harpy').
card_uid('cavern harpy'/'PLS', 'PLS:Cavern Harpy:cavern harpy').
card_rarity('cavern harpy'/'PLS', 'Common').
card_artist('cavern harpy'/'PLS', 'Daren Bader').
card_number('cavern harpy'/'PLS', '97').
card_multiverse_id('cavern harpy'/'PLS', '25926').

card_in_set('cloud cover', 'PLS').
card_original_type('cloud cover'/'PLS', 'Enchantment').
card_original_text('cloud cover'/'PLS', 'Whenever another permanent you control becomes the target of a spell or ability an opponent controls, you may return that permanent to its owner\'s hand.').
card_first_print('cloud cover', 'PLS').
card_image_name('cloud cover'/'PLS', 'cloud cover').
card_uid('cloud cover'/'PLS', 'PLS:Cloud Cover:cloud cover').
card_rarity('cloud cover'/'PLS', 'Rare').
card_artist('cloud cover'/'PLS', 'Marc Fishman').
card_number('cloud cover'/'PLS', '98').
card_multiverse_id('cloud cover'/'PLS', '25899').

card_in_set('confound', 'PLS').
card_original_type('confound'/'PLS', 'Instant').
card_original_text('confound'/'PLS', 'Counter target spell that targets one or more creatures.\nDraw a card.').
card_first_print('confound', 'PLS').
card_image_name('confound'/'PLS', 'confound').
card_uid('confound'/'PLS', 'PLS:Confound:confound').
card_rarity('confound'/'PLS', 'Common').
card_artist('confound'/'PLS', 'Doug Chaffee').
card_number('confound'/'PLS', '22').
card_flavor_text('confound'/'PLS', '\"Did I know Szat would betray us?\" Urza asked quizzically. \"I was counting on it.\"').
card_multiverse_id('confound'/'PLS', '27714').

card_in_set('crosis\'s catacombs', 'PLS').
card_original_type('crosis\'s catacombs'/'PLS', 'Land').
card_original_text('crosis\'s catacombs'/'PLS', 'Crosis\'s Catacombs is a Lair in addition to its land type.\nWhen Crosis\'s Catacombs comes into play, sacrifice it unless you return a non-Lair land you control to its owner\'s hand.\n{T}: Add {U}, {B}, or {R} to your mana pool.').
card_first_print('crosis\'s catacombs', 'PLS').
card_image_name('crosis\'s catacombs'/'PLS', 'crosis\'s catacombs').
card_uid('crosis\'s catacombs'/'PLS', 'PLS:Crosis\'s Catacombs:crosis\'s catacombs').
card_rarity('crosis\'s catacombs'/'PLS', 'Uncommon').
card_artist('crosis\'s catacombs'/'PLS', 'Edward P. Beard, Jr.').
card_number('crosis\'s catacombs'/'PLS', '136').
card_multiverse_id('crosis\'s catacombs'/'PLS', '25936').

card_in_set('crosis\'s charm', 'PLS').
card_original_type('crosis\'s charm'/'PLS', 'Instant').
card_original_text('crosis\'s charm'/'PLS', 'Choose one Return target permanent to its owner\'s hand; or destroy target nonblack creature, and it can\'t be regenerated; or destroy target artifact.').
card_first_print('crosis\'s charm', 'PLS').
card_image_name('crosis\'s charm'/'PLS', 'crosis\'s charm').
card_uid('crosis\'s charm'/'PLS', 'PLS:Crosis\'s Charm:crosis\'s charm').
card_rarity('crosis\'s charm'/'PLS', 'Uncommon').
card_artist('crosis\'s charm'/'PLS', 'David Martin').
card_number('crosis\'s charm'/'PLS', '99').
card_multiverse_id('crosis\'s charm'/'PLS', '25863').

card_in_set('darigaaz\'s caldera', 'PLS').
card_original_type('darigaaz\'s caldera'/'PLS', 'Land').
card_original_text('darigaaz\'s caldera'/'PLS', 'Darigaaz\'s Caldera is a Lair in addition to its land type.\nWhen Darigaaz\'s Caldera comes into play, sacrifice it unless you return a non-Lair land you control to its owner\'s hand.\n{T}: Add {B}, {R}, or {G} to your mana pool.').
card_first_print('darigaaz\'s caldera', 'PLS').
card_image_name('darigaaz\'s caldera'/'PLS', 'darigaaz\'s caldera').
card_uid('darigaaz\'s caldera'/'PLS', 'PLS:Darigaaz\'s Caldera:darigaaz\'s caldera').
card_rarity('darigaaz\'s caldera'/'PLS', 'Uncommon').
card_artist('darigaaz\'s caldera'/'PLS', 'Franz Vohwinkel').
card_number('darigaaz\'s caldera'/'PLS', '137').
card_multiverse_id('darigaaz\'s caldera'/'PLS', '25938').

card_in_set('darigaaz\'s charm', 'PLS').
card_original_type('darigaaz\'s charm'/'PLS', 'Instant').
card_original_text('darigaaz\'s charm'/'PLS', 'Choose one Return target creature card from your graveyard to your hand; or Darigaaz\'s Charm deals 3 damage to target creature or player; or target creature gets +3/+3 until end of turn.').
card_first_print('darigaaz\'s charm', 'PLS').
card_image_name('darigaaz\'s charm'/'PLS', 'darigaaz\'s charm').
card_uid('darigaaz\'s charm'/'PLS', 'PLS:Darigaaz\'s Charm:darigaaz\'s charm').
card_rarity('darigaaz\'s charm'/'PLS', 'Uncommon').
card_artist('darigaaz\'s charm'/'PLS', 'David Martin').
card_number('darigaaz\'s charm'/'PLS', '100').
card_multiverse_id('darigaaz\'s charm'/'PLS', '25864').

card_in_set('daring leap', 'PLS').
card_original_type('daring leap'/'PLS', 'Instant').
card_original_text('daring leap'/'PLS', 'Target creature gets +1/+1 and gains flying and first strike until end of turn.').
card_first_print('daring leap', 'PLS').
card_image_name('daring leap'/'PLS', 'daring leap').
card_uid('daring leap'/'PLS', 'PLS:Daring Leap:daring leap').
card_rarity('daring leap'/'PLS', 'Common').
card_artist('daring leap'/'PLS', 'Paolo Parente').
card_number('daring leap'/'PLS', '101').
card_flavor_text('daring leap'/'PLS', 'Most warriors think in only two dimensions. Agnate and his metathran brethren were trained to battle in three.').
card_multiverse_id('daring leap'/'PLS', '25822').

card_in_set('dark suspicions', 'PLS').
card_original_type('dark suspicions'/'PLS', 'Enchantment').
card_original_text('dark suspicions'/'PLS', 'At the beginning of each opponent\'s upkeep, that player loses 1 life for each card in his or her hand more than you have in your hand.').
card_first_print('dark suspicions', 'PLS').
card_image_name('dark suspicions'/'PLS', 'dark suspicions').
card_uid('dark suspicions'/'PLS', 'PLS:Dark Suspicions:dark suspicions').
card_rarity('dark suspicions'/'PLS', 'Rare').
card_artist('dark suspicions'/'PLS', 'Matt Cavotta').
card_number('dark suspicions'/'PLS', '40').
card_flavor_text('dark suspicions'/'PLS', '\"After four thousand years, you learn to plan for betrayal.\"\n—Urza').
card_multiverse_id('dark suspicions'/'PLS', '26414').

card_in_set('deadapult', 'PLS').
card_original_type('deadapult'/'PLS', 'Enchantment').
card_original_text('deadapult'/'PLS', '{R}, Sacrifice a Zombie: Deadapult deals 2 damage to target creature or player.').
card_first_print('deadapult', 'PLS').
card_image_name('deadapult'/'PLS', 'deadapult').
card_uid('deadapult'/'PLS', 'PLS:Deadapult:deadapult').
card_rarity('deadapult'/'PLS', 'Rare').
card_artist('deadapult'/'PLS', 'Mark Brill').
card_number('deadapult'/'PLS', '59').
card_flavor_text('deadapult'/'PLS', '\"Nothing ruins your day like a blazing zombie.\"\n—Tahngarth').
card_multiverse_id('deadapult'/'PLS', '26837').

card_in_set('death bomb', 'PLS').
card_original_type('death bomb'/'PLS', 'Instant').
card_original_text('death bomb'/'PLS', 'As an additional cost to play Death Bomb, sacrifice a creature.\nDestroy target nonblack creature. It can\'t be regenerated. Its controller loses 2 life.').
card_first_print('death bomb', 'PLS').
card_image_name('death bomb'/'PLS', 'death bomb').
card_uid('death bomb'/'PLS', 'PLS:Death Bomb:death bomb').
card_rarity('death bomb'/'PLS', 'Common').
card_artist('death bomb'/'PLS', 'Dan Frazier').
card_number('death bomb'/'PLS', '41').
card_flavor_text('death bomb'/'PLS', 'There\'s no higher honor in Phyrexia than dying for Yawgmoth\'s glorious vision.').
card_multiverse_id('death bomb'/'PLS', '26782').

card_in_set('destructive flow', 'PLS').
card_original_type('destructive flow'/'PLS', 'Enchantment').
card_original_text('destructive flow'/'PLS', 'At the beginning of each player\'s upkeep, that player sacrifices a nonbasic land.').
card_first_print('destructive flow', 'PLS').
card_image_name('destructive flow'/'PLS', 'destructive flow').
card_uid('destructive flow'/'PLS', 'PLS:Destructive Flow:destructive flow').
card_rarity('destructive flow'/'PLS', 'Rare').
card_artist('destructive flow'/'PLS', 'Don Hazeltine').
card_number('destructive flow'/'PLS', '102').
card_flavor_text('destructive flow'/'PLS', 'Stage two of the Phyrexian invasion included the complete alteration of Dominaria\'s landscape.').
card_multiverse_id('destructive flow'/'PLS', '9690').

card_in_set('diabolic intent', 'PLS').
card_original_type('diabolic intent'/'PLS', 'Sorcery').
card_original_text('diabolic intent'/'PLS', 'As an additional cost to play Diabolic Intent, sacrifice a creature.\nSearch your library for a card and put that card into your hand. Then shuffle your library.').
card_first_print('diabolic intent', 'PLS').
card_image_name('diabolic intent'/'PLS', 'diabolic intent').
card_uid('diabolic intent'/'PLS', 'PLS:Diabolic Intent:diabolic intent').
card_rarity('diabolic intent'/'PLS', 'Rare').
card_artist('diabolic intent'/'PLS', 'Dave Dorman').
card_number('diabolic intent'/'PLS', '42').
card_multiverse_id('diabolic intent'/'PLS', '26422').

card_in_set('disciple of kangee', 'PLS').
card_original_type('disciple of kangee'/'PLS', 'Creature — Wizard').
card_original_text('disciple of kangee'/'PLS', '{U}, {T}: Target creature gains flying and becomes blue until end of turn.').
card_first_print('disciple of kangee', 'PLS').
card_image_name('disciple of kangee'/'PLS', 'disciple of kangee').
card_uid('disciple of kangee'/'PLS', 'PLS:Disciple of Kangee:disciple of kangee').
card_rarity('disciple of kangee'/'PLS', 'Common').
card_artist('disciple of kangee'/'PLS', 'Wayne England').
card_number('disciple of kangee'/'PLS', '3').
card_flavor_text('disciple of kangee'/'PLS', 'When no birds are available, Kangee\'s disciples fill the sky with anything that‘s willing to fight.').
card_multiverse_id('disciple of kangee'/'PLS', '26285').

card_in_set('dominaria\'s judgment', 'PLS').
card_original_type('dominaria\'s judgment'/'PLS', 'Instant').
card_original_text('dominaria\'s judgment'/'PLS', 'Until end of turn, creatures you control gain protection from white if you control a plains, from blue if you control an island, from black if you control a swamp, from red if you control a mountain, and from green if you control a forest.').
card_first_print('dominaria\'s judgment', 'PLS').
card_image_name('dominaria\'s judgment'/'PLS', 'dominaria\'s judgment').
card_uid('dominaria\'s judgment'/'PLS', 'PLS:Dominaria\'s Judgment:dominaria\'s judgment').
card_rarity('dominaria\'s judgment'/'PLS', 'Rare').
card_artist('dominaria\'s judgment'/'PLS', 'John Avon').
card_number('dominaria\'s judgment'/'PLS', '4').
card_multiverse_id('dominaria\'s judgment'/'PLS', '26423').

card_in_set('doomsday specter', 'PLS').
card_original_type('doomsday specter'/'PLS', 'Creature — Specter').
card_original_text('doomsday specter'/'PLS', 'Flying\nWhen Doomsday Specter comes into play, return a blue or black creature you control to its owner\'s hand.\nWhenever Doomsday Specter deals combat damage to a player, look at that player\'s hand and choose a card from it. The player discards that card.').
card_first_print('doomsday specter', 'PLS').
card_image_name('doomsday specter'/'PLS', 'doomsday specter').
card_uid('doomsday specter'/'PLS', 'PLS:Doomsday Specter:doomsday specter').
card_rarity('doomsday specter'/'PLS', 'Rare').
card_artist('doomsday specter'/'PLS', 'Donato Giancola').
card_number('doomsday specter'/'PLS', '103').
card_multiverse_id('doomsday specter'/'PLS', '23051').

card_in_set('draco', 'PLS').
card_original_type('draco'/'PLS', 'Artifact Creature — Dragon').
card_original_text('draco'/'PLS', 'Draco costs {2} less to play for each basic land type among lands you control.\nFlying\nAt the beginning of your upkeep, sacrifice Draco unless you pay {1}0. This cost is reduced by {2} for each basic land type among lands you control.').
card_first_print('draco', 'PLS').
card_image_name('draco'/'PLS', 'draco').
card_uid('draco'/'PLS', 'PLS:Draco:draco').
card_rarity('draco'/'PLS', 'Rare').
card_artist('draco'/'PLS', 'Sam Wood').
card_number('draco'/'PLS', '131').
card_multiverse_id('draco'/'PLS', '25797').

card_in_set('dralnu\'s crusade', 'PLS').
card_original_type('dralnu\'s crusade'/'PLS', 'Enchantment').
card_original_text('dralnu\'s crusade'/'PLS', 'All Goblins get +1/+1, are black, and are Zombies in addition to their creature types.').
card_first_print('dralnu\'s crusade', 'PLS').
card_image_name('dralnu\'s crusade'/'PLS', 'dralnu\'s crusade').
card_uid('dralnu\'s crusade'/'PLS', 'PLS:Dralnu\'s Crusade:dralnu\'s crusade').
card_rarity('dralnu\'s crusade'/'PLS', 'Rare').
card_artist('dralnu\'s crusade'/'PLS', 'Arnie Swekel').
card_number('dralnu\'s crusade'/'PLS', '104').
card_flavor_text('dralnu\'s crusade'/'PLS', '\"Don\'t you agree that mogg intellects are improved by zombification?\"\n—Lord Dralnu').
card_multiverse_id('dralnu\'s crusade'/'PLS', '26840').

card_in_set('dralnu\'s pet', 'PLS').
card_original_type('dralnu\'s pet'/'PLS', 'Creature — Shapeshifter').
card_original_text('dralnu\'s pet'/'PLS', 'Kicker—{2}{B}, Discard a creature card from your hand. (You may pay {2}{B} and discard a creature card from your hand in addition to any other costs as you play this spell.)\nIf you paid the kicker cost, Dralnu\'s Pet has flying and comes into play with X +1/+1 counters on it, where X is the discarded card\'s converted mana cost.').
card_first_print('dralnu\'s pet', 'PLS').
card_image_name('dralnu\'s pet'/'PLS', 'dralnu\'s pet').
card_uid('dralnu\'s pet'/'PLS', 'PLS:Dralnu\'s Pet:dralnu\'s pet').
card_rarity('dralnu\'s pet'/'PLS', 'Rare').
card_artist('dralnu\'s pet'/'PLS', 'Glen Angus').
card_number('dralnu\'s pet'/'PLS', '23').
card_multiverse_id('dralnu\'s pet'/'PLS', '15144').

card_in_set('dromar\'s cavern', 'PLS').
card_original_type('dromar\'s cavern'/'PLS', 'Land').
card_original_text('dromar\'s cavern'/'PLS', 'Dromar\'s Cavern is a Lair in addition to its land type.\nWhen Dromar\'s Cavern comes into play, sacrifice it unless you return a non-Lair land you control to its owner\'s hand.\n{T}: Add {W}, {U}, or {B} to your mana pool.').
card_first_print('dromar\'s cavern', 'PLS').
card_image_name('dromar\'s cavern'/'PLS', 'dromar\'s cavern').
card_uid('dromar\'s cavern'/'PLS', 'PLS:Dromar\'s Cavern:dromar\'s cavern').
card_rarity('dromar\'s cavern'/'PLS', 'Uncommon').
card_artist('dromar\'s cavern'/'PLS', 'Franz Vohwinkel').
card_number('dromar\'s cavern'/'PLS', '138').
card_multiverse_id('dromar\'s cavern'/'PLS', '25932').

card_in_set('dromar\'s charm', 'PLS').
card_original_type('dromar\'s charm'/'PLS', 'Instant').
card_original_text('dromar\'s charm'/'PLS', 'Choose one You gain 5 life; or counter target spell; or target creature gets -2/-2 until end of turn.').
card_first_print('dromar\'s charm', 'PLS').
card_image_name('dromar\'s charm'/'PLS', 'dromar\'s charm').
card_uid('dromar\'s charm'/'PLS', 'PLS:Dromar\'s Charm:dromar\'s charm').
card_rarity('dromar\'s charm'/'PLS', 'Uncommon').
card_artist('dromar\'s charm'/'PLS', 'David Martin').
card_number('dromar\'s charm'/'PLS', '105').
card_multiverse_id('dromar\'s charm'/'PLS', '25862').

card_in_set('eladamri\'s call', 'PLS').
card_original_type('eladamri\'s call'/'PLS', 'Instant').
card_original_text('eladamri\'s call'/'PLS', 'Search your library for a creature card, reveal that card, and put it into your hand. Then shuffle your library.').
card_first_print('eladamri\'s call', 'PLS').
card_image_name('eladamri\'s call'/'PLS', 'eladamri\'s call').
card_uid('eladamri\'s call'/'PLS', 'PLS:Eladamri\'s Call:eladamri\'s call').
card_rarity('eladamri\'s call'/'PLS', 'Rare').
card_artist('eladamri\'s call'/'PLS', 'Kev Walker').
card_number('eladamri\'s call'/'PLS', '106').
card_flavor_text('eladamri\'s call'/'PLS', 'Tribal rivalries and petty disputes were laid aside at Eladamri\'s summons.').
card_multiverse_id('eladamri\'s call'/'PLS', '25646').

card_in_set('ertai\'s trickery', 'PLS').
card_original_type('ertai\'s trickery'/'PLS', 'Instant').
card_original_text('ertai\'s trickery'/'PLS', 'Counter target spell if a kicker cost was paid for it.').
card_first_print('ertai\'s trickery', 'PLS').
card_image_name('ertai\'s trickery'/'PLS', 'ertai\'s trickery').
card_uid('ertai\'s trickery'/'PLS', 'PLS:Ertai\'s Trickery:ertai\'s trickery').
card_rarity('ertai\'s trickery'/'PLS', 'Uncommon').
card_artist('ertai\'s trickery'/'PLS', 'Kev Walker').
card_number('ertai\'s trickery'/'PLS', '24').
card_flavor_text('ertai\'s trickery'/'PLS', '\"Don\'t worry, Gerrard,\" said Ertai. \"I\'m sure the crew will come to your rescue as quickly as they came to mine.\"').
card_multiverse_id('ertai\'s trickery'/'PLS', '26590').

card_in_set('ertai, the corrupted', 'PLS').
card_original_type('ertai, the corrupted'/'PLS', 'Creature — Wizard Legend').
card_original_text('ertai, the corrupted'/'PLS', '{U}, {T}, Sacrifice a creature or enchantment: Counter target spell.').
card_first_print('ertai, the corrupted', 'PLS').
card_image_name('ertai, the corrupted'/'PLS', 'ertai, the corrupted1').
card_uid('ertai, the corrupted'/'PLS', 'PLS:Ertai, the Corrupted:ertai, the corrupted1').
card_rarity('ertai, the corrupted'/'PLS', 'Rare').
card_artist('ertai, the corrupted'/'PLS', 'Mark Tedin').
card_number('ertai, the corrupted'/'PLS', '107').
card_flavor_text('ertai, the corrupted'/'PLS', 'Altered by Phyrexian science, corrupted by black mana, and twisted by rage, Ertai still looked in the mirror and saw only glory.').
card_multiverse_id('ertai, the corrupted'/'PLS', '25614').

card_in_set('ertai, the corrupted', 'PLS').
card_original_type('ertai, the corrupted'/'PLS', 'Creature — Wizard Legend').
card_original_text('ertai, the corrupted'/'PLS', '{U}, {T}, Sacrifice a creature or enchantment: Counter target spell.').
card_image_name('ertai, the corrupted'/'PLS', 'ertai, the corrupted2').
card_uid('ertai, the corrupted'/'PLS', 'PLS:Ertai, the Corrupted:ertai, the corrupted2').
card_rarity('ertai, the corrupted'/'PLS', 'Rare').
card_artist('ertai, the corrupted'/'PLS', 'Kev Walker').
card_number('ertai, the corrupted'/'PLS', '★107').
card_flavor_text('ertai, the corrupted'/'PLS', 'Altered by Phyrexian science, corrupted by black mana, and twisted by rage, Ertai still looked in the mirror and saw only glory.').
card_multiverse_id('ertai, the corrupted'/'PLS', '29292').

card_in_set('escape routes', 'PLS').
card_original_type('escape routes'/'PLS', 'Enchantment').
card_original_text('escape routes'/'PLS', '{2}{U}: Return target white or black creature you control to its owner\'s hand.').
card_first_print('escape routes', 'PLS').
card_image_name('escape routes'/'PLS', 'escape routes').
card_uid('escape routes'/'PLS', 'PLS:Escape Routes:escape routes').
card_rarity('escape routes'/'PLS', 'Common').
card_artist('escape routes'/'PLS', 'Marc Fishman').
card_number('escape routes'/'PLS', '25').
card_flavor_text('escape routes'/'PLS', '\"I\'ll get you to safety—or whatever passes for safety these days.\"\n—Urborg ferry pilot').
card_multiverse_id('escape routes'/'PLS', '21276').

card_in_set('exotic disease', 'PLS').
card_original_type('exotic disease'/'PLS', 'Sorcery').
card_original_text('exotic disease'/'PLS', 'Target player loses X life and you gain X life, where X is the number of basic land types among lands you control.').
card_first_print('exotic disease', 'PLS').
card_image_name('exotic disease'/'PLS', 'exotic disease').
card_uid('exotic disease'/'PLS', 'PLS:Exotic Disease:exotic disease').
card_rarity('exotic disease'/'PLS', 'Uncommon').
card_artist('exotic disease'/'PLS', 'Kev Walker').
card_number('exotic disease'/'PLS', '43').
card_flavor_text('exotic disease'/'PLS', '\"Do not fear death. I shall release you from the eternal bonds and bring you glory.\"\n—Lord Dralnu').
card_multiverse_id('exotic disease'/'PLS', '25931').

card_in_set('falling timber', 'PLS').
card_original_type('falling timber'/'PLS', 'Instant').
card_original_text('falling timber'/'PLS', 'Kicker—Sacrifice a land. (You may sacrifice a land in addition to any other costs as you play this spell.)\nPrevent all combat damage target creature would deal this turn. If you paid the kicker cost, prevent all combat damage another target creature would deal this turn.').
card_first_print('falling timber', 'PLS').
card_image_name('falling timber'/'PLS', 'falling timber').
card_uid('falling timber'/'PLS', 'PLS:Falling Timber:falling timber').
card_rarity('falling timber'/'PLS', 'Common').
card_artist('falling timber'/'PLS', 'Eric Peterson').
card_number('falling timber'/'PLS', '79').
card_multiverse_id('falling timber'/'PLS', '25945').

card_in_set('flametongue kavu', 'PLS').
card_original_type('flametongue kavu'/'PLS', 'Creature — Kavu').
card_original_text('flametongue kavu'/'PLS', 'When Flametongue Kavu comes into play, it deals 4 damage to target creature.').
card_image_name('flametongue kavu'/'PLS', 'flametongue kavu').
card_uid('flametongue kavu'/'PLS', 'PLS:Flametongue Kavu:flametongue kavu').
card_rarity('flametongue kavu'/'PLS', 'Uncommon').
card_artist('flametongue kavu'/'PLS', 'Pete Venters').
card_number('flametongue kavu'/'PLS', '60').
card_flavor_text('flametongue kavu'/'PLS', '\"For dim-witted, thick-skulled genetic mutants, they have pretty good aim.\"\n—Sisay').
card_multiverse_id('flametongue kavu'/'PLS', '26262').

card_in_set('fleetfoot panther', 'PLS').
card_original_type('fleetfoot panther'/'PLS', 'Creature — Cat').
card_original_text('fleetfoot panther'/'PLS', 'You may play Fleetfoot Panther any time you could play an instant.\nWhen Fleetfoot Panther comes into play, return a green or white creature you control to its owner\'s hand.').
card_first_print('fleetfoot panther', 'PLS').
card_image_name('fleetfoot panther'/'PLS', 'fleetfoot panther').
card_uid('fleetfoot panther'/'PLS', 'PLS:Fleetfoot Panther:fleetfoot panther').
card_rarity('fleetfoot panther'/'PLS', 'Uncommon').
card_artist('fleetfoot panther'/'PLS', 'Mark Brill').
card_number('fleetfoot panther'/'PLS', '108').
card_multiverse_id('fleetfoot panther'/'PLS', '26479').

card_in_set('forsaken city', 'PLS').
card_original_type('forsaken city'/'PLS', 'Land').
card_original_text('forsaken city'/'PLS', 'Forsaken City doesn\'t untap during your untap step.\nAt the beginning of your upkeep, you may remove a card in your hand from the game. If you do, untap Forsaken City.\n{T}: Add one mana of any color to your mana pool.').
card_first_print('forsaken city', 'PLS').
card_image_name('forsaken city'/'PLS', 'forsaken city').
card_uid('forsaken city'/'PLS', 'PLS:Forsaken City:forsaken city').
card_rarity('forsaken city'/'PLS', 'Rare').
card_artist('forsaken city'/'PLS', 'Dana Knutson').
card_number('forsaken city'/'PLS', '139').
card_multiverse_id('forsaken city'/'PLS', '25893').

card_in_set('gaea\'s herald', 'PLS').
card_original_type('gaea\'s herald'/'PLS', 'Creature — Elf').
card_original_text('gaea\'s herald'/'PLS', 'Creature spells can\'t be countered by spells or abilities.').
card_first_print('gaea\'s herald', 'PLS').
card_image_name('gaea\'s herald'/'PLS', 'gaea\'s herald').
card_uid('gaea\'s herald'/'PLS', 'PLS:Gaea\'s Herald:gaea\'s herald').
card_rarity('gaea\'s herald'/'PLS', 'Rare').
card_artist('gaea\'s herald'/'PLS', 'Dan Frazier').
card_number('gaea\'s herald'/'PLS', '80').
card_flavor_text('gaea\'s herald'/'PLS', '\"I bring word from Gaea. Fight on! She will never allow her children to die alone.\"').
card_multiverse_id('gaea\'s herald'/'PLS', '26411').

card_in_set('gaea\'s might', 'PLS').
card_original_type('gaea\'s might'/'PLS', 'Instant').
card_original_text('gaea\'s might'/'PLS', 'Target creature gets +1/+1 until end of turn for each basic land type among lands you control.').
card_first_print('gaea\'s might', 'PLS').
card_image_name('gaea\'s might'/'PLS', 'gaea\'s might').
card_uid('gaea\'s might'/'PLS', 'PLS:Gaea\'s Might:gaea\'s might').
card_rarity('gaea\'s might'/'PLS', 'Common').
card_artist('gaea\'s might'/'PLS', 'Ron Spencer').
card_number('gaea\'s might'/'PLS', '81').
card_flavor_text('gaea\'s might'/'PLS', 'With the Heart of Yavimaya aiding him, Multani didn\'t even strain at the load.').
card_multiverse_id('gaea\'s might'/'PLS', '26838').

card_in_set('gainsay', 'PLS').
card_original_type('gainsay'/'PLS', 'Instant').
card_original_text('gainsay'/'PLS', 'Counter target blue spell.').
card_first_print('gainsay', 'PLS').
card_image_name('gainsay'/'PLS', 'gainsay').
card_uid('gainsay'/'PLS', 'PLS:Gainsay:gainsay').
card_rarity('gainsay'/'PLS', 'Uncommon').
card_artist('gainsay'/'PLS', 'Roger Raupp').
card_number('gainsay'/'PLS', '26').
card_flavor_text('gainsay'/'PLS', '\"I\'d be happy to stop contradicting you, Urza, just as soon as you start being right.\"\n—Bo Levar, planeswalker').
card_multiverse_id('gainsay'/'PLS', '27189').

card_in_set('gerrard\'s command', 'PLS').
card_original_type('gerrard\'s command'/'PLS', 'Instant').
card_original_text('gerrard\'s command'/'PLS', 'Untap target creature. It gets +3/+3 until end of turn.').
card_first_print('gerrard\'s command', 'PLS').
card_image_name('gerrard\'s command'/'PLS', 'gerrard\'s command').
card_uid('gerrard\'s command'/'PLS', 'PLS:Gerrard\'s Command:gerrard\'s command').
card_rarity('gerrard\'s command'/'PLS', 'Common').
card_artist('gerrard\'s command'/'PLS', 'Roger Raupp').
card_number('gerrard\'s command'/'PLS', '109').
card_flavor_text('gerrard\'s command'/'PLS', '\"Unless you like the idea of becoming some goblin\'s lunch, get up and move!\"').
card_multiverse_id('gerrard\'s command'/'PLS', '25873').

card_in_set('goblin game', 'PLS').
card_original_type('goblin game'/'PLS', 'Sorcery').
card_original_text('goblin game'/'PLS', 'Each player hides at least one object, then all players reveal them simultaneously. Each player loses life equal to the number of objects he or she revealed. The player who revealed the fewest objects then loses half his or her life, rounded up. If two or more players are tied for fewest, each loses half his or her life, rounded up.').
card_first_print('goblin game', 'PLS').
card_image_name('goblin game'/'PLS', 'goblin game').
card_uid('goblin game'/'PLS', 'PLS:Goblin Game:goblin game').
card_rarity('goblin game'/'PLS', 'Rare').
card_artist('goblin game'/'PLS', 'DiTerlizzi').
card_number('goblin game'/'PLS', '61').
card_multiverse_id('goblin game'/'PLS', '8902').

card_in_set('guard dogs', 'PLS').
card_original_type('guard dogs'/'PLS', 'Creature — Hound').
card_original_text('guard dogs'/'PLS', '{2}{W}, {T}: Choose a permanent you control. Prevent all combat damage target creature would deal this turn if it shares a color with that permanent.').
card_first_print('guard dogs', 'PLS').
card_image_name('guard dogs'/'PLS', 'guard dogs').
card_uid('guard dogs'/'PLS', 'PLS:Guard Dogs:guard dogs').
card_rarity('guard dogs'/'PLS', 'Uncommon').
card_artist('guard dogs'/'PLS', 'Mike Raabe').
card_number('guard dogs'/'PLS', '5').
card_flavor_text('guard dogs'/'PLS', '\"A good guard is alert. A great guard is also loyal.\"\n—Benalish kennelmaster').
card_multiverse_id('guard dogs'/'PLS', '19141').

card_in_set('heroic defiance', 'PLS').
card_original_type('heroic defiance'/'PLS', 'Enchant Creature').
card_original_text('heroic defiance'/'PLS', 'Enchanted creature gets +3/+3 unless it shares a color with the most common color among all permanents or a color tied for most common.').
card_first_print('heroic defiance', 'PLS').
card_image_name('heroic defiance'/'PLS', 'heroic defiance').
card_uid('heroic defiance'/'PLS', 'PLS:Heroic Defiance:heroic defiance').
card_rarity('heroic defiance'/'PLS', 'Common').
card_artist('heroic defiance'/'PLS', 'Terese Nielsen').
card_number('heroic defiance'/'PLS', '6').
card_flavor_text('heroic defiance'/'PLS', '\"Wear courage as your armor. Wield honor as your blade.\"\n—Gerrard').
card_multiverse_id('heroic defiance'/'PLS', '26420').

card_in_set('hobble', 'PLS').
card_original_type('hobble'/'PLS', 'Enchant Creature').
card_original_text('hobble'/'PLS', 'When Hobble comes into play, draw a card.\nEnchanted creature can\'t attack.\nEnchanted creature can\'t block if it\'s black.').
card_first_print('hobble', 'PLS').
card_image_name('hobble'/'PLS', 'hobble').
card_uid('hobble'/'PLS', 'PLS:Hobble:hobble').
card_rarity('hobble'/'PLS', 'Common').
card_artist('hobble'/'PLS', 'Alan Pollack').
card_number('hobble'/'PLS', '7').
card_multiverse_id('hobble'/'PLS', '26292').

card_in_set('honorable scout', 'PLS').
card_original_type('honorable scout'/'PLS', 'Creature — Soldier').
card_original_text('honorable scout'/'PLS', 'When Honorable Scout comes into play, you gain 2 life for each black and/or red creature target opponent controls.').
card_first_print('honorable scout', 'PLS').
card_image_name('honorable scout'/'PLS', 'honorable scout').
card_uid('honorable scout'/'PLS', 'PLS:Honorable Scout:honorable scout').
card_rarity('honorable scout'/'PLS', 'Common').
card_artist('honorable scout'/'PLS', 'Mike Ploog').
card_number('honorable scout'/'PLS', '8').
card_flavor_text('honorable scout'/'PLS', '\"I\'ve faced your kind before. This time I\'m ready for you.\"').
card_multiverse_id('honorable scout'/'PLS', '26263').

card_in_set('horned kavu', 'PLS').
card_original_type('horned kavu'/'PLS', 'Creature — Kavu').
card_original_text('horned kavu'/'PLS', 'When Horned Kavu comes into play, return a red or green creature you control to its owner\'s hand.').
card_first_print('horned kavu', 'PLS').
card_image_name('horned kavu'/'PLS', 'horned kavu').
card_uid('horned kavu'/'PLS', 'PLS:Horned Kavu:horned kavu').
card_rarity('horned kavu'/'PLS', 'Common').
card_artist('horned kavu'/'PLS', 'Michael Sutfin').
card_number('horned kavu'/'PLS', '110').
card_flavor_text('horned kavu'/'PLS', '\"That didn\'t happen naturally,\" said Multani. \"Rath\'s overlay is interfering with the Æther itself.\"').
card_multiverse_id('horned kavu'/'PLS', '25928').

card_in_set('hull breach', 'PLS').
card_original_type('hull breach'/'PLS', 'Sorcery').
card_original_text('hull breach'/'PLS', 'Choose one Destroy target artifact; or destroy target enchantment; or destroy target artifact and target enchantment.').
card_first_print('hull breach', 'PLS').
card_image_name('hull breach'/'PLS', 'hull breach').
card_uid('hull breach'/'PLS', 'PLS:Hull Breach:hull breach').
card_rarity('hull breach'/'PLS', 'Common').
card_artist('hull breach'/'PLS', 'Brian Snõddy').
card_number('hull breach'/'PLS', '111').
card_flavor_text('hull breach'/'PLS', '\"Crovax knows we\'re coming now,\" said a grinning Sisay. \"I just sent the Predator crashing into his stronghold.\"').
card_multiverse_id('hull breach'/'PLS', '25872').

card_in_set('hunting drake', 'PLS').
card_original_type('hunting drake'/'PLS', 'Creature — Drake').
card_original_text('hunting drake'/'PLS', 'Flying\nWhen Hunting Drake comes into play, put target red or green creature on top of its owner\'s library.').
card_first_print('hunting drake', 'PLS').
card_image_name('hunting drake'/'PLS', 'hunting drake').
card_uid('hunting drake'/'PLS', 'PLS:Hunting Drake:hunting drake').
card_rarity('hunting drake'/'PLS', 'Common').
card_artist('hunting drake'/'PLS', 'Wayne England').
card_number('hunting drake'/'PLS', '27').
card_flavor_text('hunting drake'/'PLS', 'Even the fiercest beast will flee when faced with the possibility of becoming dinner.').
card_multiverse_id('hunting drake'/'PLS', '22980').

card_in_set('implode', 'PLS').
card_original_type('implode'/'PLS', 'Sorcery').
card_original_text('implode'/'PLS', 'Destroy target land.\nDraw a card.').
card_first_print('implode', 'PLS').
card_image_name('implode'/'PLS', 'implode').
card_uid('implode'/'PLS', 'PLS:Implode:implode').
card_rarity('implode'/'PLS', 'Uncommon').
card_artist('implode'/'PLS', 'Arnie Swekel').
card_number('implode'/'PLS', '62').
card_flavor_text('implode'/'PLS', 'To save Dominaria, nine planeswalkers sought the annihilation of Phyrexia itself.').
card_multiverse_id('implode'/'PLS', '26260').

card_in_set('insolence', 'PLS').
card_original_type('insolence'/'PLS', 'Enchant Creature').
card_original_text('insolence'/'PLS', 'Whenever enchanted creature becomes tapped, Insolence deals 2 damage to that creature\'s controller.').
card_first_print('insolence', 'PLS').
card_image_name('insolence'/'PLS', 'insolence').
card_uid('insolence'/'PLS', 'PLS:Insolence:insolence').
card_rarity('insolence'/'PLS', 'Common').
card_artist('insolence'/'PLS', 'Carl Critchlow').
card_number('insolence'/'PLS', '63').
card_flavor_text('insolence'/'PLS', '\"Sacrificing mortal dragons to satisfy your immortal hunger ends here.\"\n—Darigaaz, to Rith').
card_multiverse_id('insolence'/'PLS', '26415').

card_in_set('kavu recluse', 'PLS').
card_original_type('kavu recluse'/'PLS', 'Creature — Kavu').
card_original_text('kavu recluse'/'PLS', '{T}: Target land becomes a forest until end of turn.').
card_first_print('kavu recluse', 'PLS').
card_image_name('kavu recluse'/'PLS', 'kavu recluse').
card_uid('kavu recluse'/'PLS', 'PLS:Kavu Recluse:kavu recluse').
card_rarity('kavu recluse'/'PLS', 'Common').
card_artist('kavu recluse'/'PLS', 'Aaron Boyd').
card_number('kavu recluse'/'PLS', '64').
card_flavor_text('kavu recluse'/'PLS', 'Few ever see this particular breed of kavu, which hides in forests of its own making.').
card_multiverse_id('kavu recluse'/'PLS', '26268').

card_in_set('keldon mantle', 'PLS').
card_original_type('keldon mantle'/'PLS', 'Enchant Creature').
card_original_text('keldon mantle'/'PLS', '{B}: Regenerate enchanted creature.\n{R}: Enchanted creature gets +1/+0 until end of turn.\n{G}: Enchanted creature gains trample until end of turn.').
card_first_print('keldon mantle', 'PLS').
card_image_name('keldon mantle'/'PLS', 'keldon mantle').
card_uid('keldon mantle'/'PLS', 'PLS:Keldon Mantle:keldon mantle').
card_rarity('keldon mantle'/'PLS', 'Common').
card_artist('keldon mantle'/'PLS', 'Rebecca Guay').
card_number('keldon mantle'/'PLS', '65').
card_multiverse_id('keldon mantle'/'PLS', '19358').

card_in_set('keldon twilight', 'PLS').
card_original_type('keldon twilight'/'PLS', 'Enchantment').
card_original_text('keldon twilight'/'PLS', 'At the end of each player\'s turn, if no creatures attacked that turn, that player sacrifices a creature he or she controlled since the beginning of the turn.').
card_first_print('keldon twilight', 'PLS').
card_image_name('keldon twilight'/'PLS', 'keldon twilight').
card_uid('keldon twilight'/'PLS', 'PLS:Keldon Twilight:keldon twilight').
card_rarity('keldon twilight'/'PLS', 'Rare').
card_artist('keldon twilight'/'PLS', 'Franz Vohwinkel').
card_number('keldon twilight'/'PLS', '112').
card_multiverse_id('keldon twilight'/'PLS', '21323').

card_in_set('lashknife barrier', 'PLS').
card_original_type('lashknife barrier'/'PLS', 'Enchantment').
card_original_text('lashknife barrier'/'PLS', 'When Lashknife Barrier comes into play, draw a card.\nIf a source would deal damage to a creature you control, it deals that much damage minus 1 to that creature instead.').
card_first_print('lashknife barrier', 'PLS').
card_image_name('lashknife barrier'/'PLS', 'lashknife barrier').
card_uid('lashknife barrier'/'PLS', 'PLS:Lashknife Barrier:lashknife barrier').
card_rarity('lashknife barrier'/'PLS', 'Uncommon').
card_artist('lashknife barrier'/'PLS', 'Paolo Parente').
card_number('lashknife barrier'/'PLS', '9').
card_multiverse_id('lashknife barrier'/'PLS', '26833').

card_in_set('lava zombie', 'PLS').
card_original_type('lava zombie'/'PLS', 'Creature — Zombie').
card_original_text('lava zombie'/'PLS', 'When Lava Zombie comes into play, return a black or red creature you control to its owner\'s hand.\n{2}: Lava Zombie gets +1/+0 until end of turn.').
card_first_print('lava zombie', 'PLS').
card_image_name('lava zombie'/'PLS', 'lava zombie').
card_uid('lava zombie'/'PLS', 'PLS:Lava Zombie:lava zombie').
card_rarity('lava zombie'/'PLS', 'Common').
card_artist('lava zombie'/'PLS', 'Tom Wänerstrand').
card_number('lava zombie'/'PLS', '113').
card_multiverse_id('lava zombie'/'PLS', '25927').

card_in_set('lord of the undead', 'PLS').
card_original_type('lord of the undead'/'PLS', 'Creature — Lord').
card_original_text('lord of the undead'/'PLS', 'All Zombies get +1/+1.\n{1}{B}, {T}: Return target Zombie card from your graveyard to your hand.').
card_first_print('lord of the undead', 'PLS').
card_image_name('lord of the undead'/'PLS', 'lord of the undead').
card_uid('lord of the undead'/'PLS', 'PLS:Lord of the Undead:lord of the undead').
card_rarity('lord of the undead'/'PLS', 'Rare').
card_artist('lord of the undead'/'PLS', 'Brom').
card_number('lord of the undead'/'PLS', '44').
card_flavor_text('lord of the undead'/'PLS', '\"I confer with Death itself. What could you possibly do to injure me?\"\n—Lord Dralnu').
card_multiverse_id('lord of the undead'/'PLS', '10742').

card_in_set('maggot carrier', 'PLS').
card_original_type('maggot carrier'/'PLS', 'Creature — Zombie').
card_original_text('maggot carrier'/'PLS', 'When Maggot Carrier comes into play, each player loses 1 life.').
card_first_print('maggot carrier', 'PLS').
card_image_name('maggot carrier'/'PLS', 'maggot carrier').
card_uid('maggot carrier'/'PLS', 'PLS:Maggot Carrier:maggot carrier').
card_rarity('maggot carrier'/'PLS', 'Common').
card_artist('maggot carrier'/'PLS', 'Ron Spencer').
card_number('maggot carrier'/'PLS', '45').
card_flavor_text('maggot carrier'/'PLS', '\"The mere sight of our undead allies sickens me. What unholy bargain have you struck?\"\n—Grizzlegom, to Agnate').
card_multiverse_id('maggot carrier'/'PLS', '26266').

card_in_set('magma burst', 'PLS').
card_original_type('magma burst'/'PLS', 'Instant').
card_original_text('magma burst'/'PLS', 'Kicker—Sacrifice two lands. (You may sacrifice two lands in addition to any other costs as you play this spell.)\nMagma Burst deals 3 damage to target creature or player. If you paid the kicker cost, Magma Burst deals 3 damage to another target creature or player.').
card_first_print('magma burst', 'PLS').
card_image_name('magma burst'/'PLS', 'magma burst').
card_uid('magma burst'/'PLS', 'PLS:Magma Burst:magma burst').
card_rarity('magma burst'/'PLS', 'Common').
card_artist('magma burst'/'PLS', 'Bradley Williams').
card_number('magma burst'/'PLS', '66').
card_multiverse_id('magma burst'/'PLS', '25944').

card_in_set('magnigoth treefolk', 'PLS').
card_original_type('magnigoth treefolk'/'PLS', 'Creature — Treefolk').
card_original_text('magnigoth treefolk'/'PLS', 'For each basic land type among lands you control, Magnigoth Treefolk has landwalk of that type. (It\'s unblockable as long as defending player controls a land of that type.)').
card_first_print('magnigoth treefolk', 'PLS').
card_image_name('magnigoth treefolk'/'PLS', 'magnigoth treefolk').
card_uid('magnigoth treefolk'/'PLS', 'PLS:Magnigoth Treefolk:magnigoth treefolk').
card_rarity('magnigoth treefolk'/'PLS', 'Rare').
card_artist('magnigoth treefolk'/'PLS', 'Peter Bollinger').
card_number('magnigoth treefolk'/'PLS', '82').
card_multiverse_id('magnigoth treefolk'/'PLS', '26397').

card_in_set('malicious advice', 'PLS').
card_original_type('malicious advice'/'PLS', 'Instant').
card_original_text('malicious advice'/'PLS', 'Tap X target artifacts, creatures, and/or lands. You lose X life.').
card_first_print('malicious advice', 'PLS').
card_image_name('malicious advice'/'PLS', 'malicious advice').
card_uid('malicious advice'/'PLS', 'PLS:Malicious Advice:malicious advice').
card_rarity('malicious advice'/'PLS', 'Common').
card_artist('malicious advice'/'PLS', 'Glen Angus').
card_number('malicious advice'/'PLS', '114').
card_flavor_text('malicious advice'/'PLS', '\"Rule through fear, Darigaaz. Only if the dragons feel terror will they truly be your servants.\"\n—Tevesh Szat').
card_multiverse_id('malicious advice'/'PLS', '25870').

card_in_set('mana cylix', 'PLS').
card_original_type('mana cylix'/'PLS', 'Artifact').
card_original_text('mana cylix'/'PLS', '{1}, {T}: Add one mana of any color to your mana pool.').
card_first_print('mana cylix', 'PLS').
card_image_name('mana cylix'/'PLS', 'mana cylix').
card_uid('mana cylix'/'PLS', 'PLS:Mana Cylix:mana cylix').
card_rarity('mana cylix'/'PLS', 'Uncommon').
card_artist('mana cylix'/'PLS', 'Donato Giancola').
card_number('mana cylix'/'PLS', '132').
card_flavor_text('mana cylix'/'PLS', 'Overflowing with visions of the possible future, it lends power to those in the present.').
card_multiverse_id('mana cylix'/'PLS', '26381').

card_in_set('march of souls', 'PLS').
card_original_type('march of souls'/'PLS', 'Sorcery').
card_original_text('march of souls'/'PLS', 'Destroy all creatures. They can\'t be regenerated. For each creature destroyed this way, its controller puts a 1/1 white Spirit creature token with flying into play.').
card_first_print('march of souls', 'PLS').
card_image_name('march of souls'/'PLS', 'march of souls').
card_uid('march of souls'/'PLS', 'PLS:March of Souls:march of souls').
card_rarity('march of souls'/'PLS', 'Rare').
card_artist('march of souls'/'PLS', 'Marc Fishman').
card_number('march of souls'/'PLS', '10').
card_multiverse_id('march of souls'/'PLS', '26277').

card_in_set('marsh crocodile', 'PLS').
card_original_type('marsh crocodile'/'PLS', 'Creature — Crocodile').
card_original_text('marsh crocodile'/'PLS', 'When Marsh Crocodile comes into play, return a blue or black creature you control to its owner\'s hand.\nWhen Marsh Crocodile comes into play, each player discards a card from his or her hand.').
card_first_print('marsh crocodile', 'PLS').
card_image_name('marsh crocodile'/'PLS', 'marsh crocodile').
card_uid('marsh crocodile'/'PLS', 'PLS:Marsh Crocodile:marsh crocodile').
card_rarity('marsh crocodile'/'PLS', 'Uncommon').
card_artist('marsh crocodile'/'PLS', 'Kev Walker').
card_number('marsh crocodile'/'PLS', '115').
card_multiverse_id('marsh crocodile'/'PLS', '25934').

card_in_set('meddling mage', 'PLS').
card_original_type('meddling mage'/'PLS', 'Creature — Wizard').
card_original_text('meddling mage'/'PLS', 'As Meddling Mage comes into play, name a nonland card.\nThe named card can\'t be played.').
card_image_name('meddling mage'/'PLS', 'meddling mage').
card_uid('meddling mage'/'PLS', 'PLS:Meddling Mage:meddling mage').
card_rarity('meddling mage'/'PLS', 'Rare').
card_artist('meddling mage'/'PLS', 'Christopher Moeller').
card_number('meddling mage'/'PLS', '116').
card_flavor_text('meddling mage'/'PLS', 'Meddling mages chant so loudly that no one can get a spell in edgewise.').
card_multiverse_id('meddling mage'/'PLS', '26591').

card_in_set('meteor crater', 'PLS').
card_original_type('meteor crater'/'PLS', 'Land').
card_original_text('meteor crater'/'PLS', '{T}: Choose a color of a permanent you control. Add one mana of that color to your mana pool.').
card_first_print('meteor crater', 'PLS').
card_image_name('meteor crater'/'PLS', 'meteor crater').
card_uid('meteor crater'/'PLS', 'PLS:Meteor Crater:meteor crater').
card_rarity('meteor crater'/'PLS', 'Rare').
card_artist('meteor crater'/'PLS', 'John Avon').
card_number('meteor crater'/'PLS', '140').
card_flavor_text('meteor crater'/'PLS', 'Legend has it that meteor craters are haunted by the ghosts of those who died on impact.').
card_multiverse_id('meteor crater'/'PLS', '25979').

card_in_set('mire kavu', 'PLS').
card_original_type('mire kavu'/'PLS', 'Creature — Kavu').
card_original_text('mire kavu'/'PLS', 'Mire Kavu gets +1/+1 as long as you control a swamp.').
card_first_print('mire kavu', 'PLS').
card_image_name('mire kavu'/'PLS', 'mire kavu').
card_uid('mire kavu'/'PLS', 'PLS:Mire Kavu:mire kavu').
card_rarity('mire kavu'/'PLS', 'Common').
card_artist('mire kavu'/'PLS', 'Wayne England').
card_number('mire kavu'/'PLS', '67').
card_flavor_text('mire kavu'/'PLS', '\"At this rate,\" said Sisay, \"the next kavu we see will have wings.\"').
card_multiverse_id('mire kavu'/'PLS', '26363').

card_in_set('mirrorwood treefolk', 'PLS').
card_original_type('mirrorwood treefolk'/'PLS', 'Creature — Treefolk').
card_original_text('mirrorwood treefolk'/'PLS', '{2}{R}{W}: The next time damage would be dealt to Mirrorwood Treefolk this turn, that damage is dealt to target creature or player instead.').
card_first_print('mirrorwood treefolk', 'PLS').
card_image_name('mirrorwood treefolk'/'PLS', 'mirrorwood treefolk').
card_uid('mirrorwood treefolk'/'PLS', 'PLS:Mirrorwood Treefolk:mirrorwood treefolk').
card_rarity('mirrorwood treefolk'/'PLS', 'Uncommon').
card_artist('mirrorwood treefolk'/'PLS', 'Arnie Swekel').
card_number('mirrorwood treefolk'/'PLS', '83').
card_flavor_text('mirrorwood treefolk'/'PLS', 'It doesn\'t need a bite. The bark works just fine.').
card_multiverse_id('mirrorwood treefolk'/'PLS', '26839').

card_in_set('mogg jailer', 'PLS').
card_original_type('mogg jailer'/'PLS', 'Creature — Goblin').
card_original_text('mogg jailer'/'PLS', 'Mogg Jailer can\'t attack if defending player controls an untapped creature with power 2 or less.').
card_first_print('mogg jailer', 'PLS').
card_image_name('mogg jailer'/'PLS', 'mogg jailer').
card_uid('mogg jailer'/'PLS', 'PLS:Mogg Jailer:mogg jailer').
card_rarity('mogg jailer'/'PLS', 'Uncommon').
card_artist('mogg jailer'/'PLS', 'Mark Romanoski').
card_number('mogg jailer'/'PLS', '68').
card_flavor_text('mogg jailer'/'PLS', 'Hey, wake up\n—Mogg secret password').
card_multiverse_id('mogg jailer'/'PLS', '26275').

card_in_set('mogg sentry', 'PLS').
card_original_type('mogg sentry'/'PLS', 'Creature — Goblin').
card_original_text('mogg sentry'/'PLS', 'Whenever an opponent plays a spell, Mogg Sentry gets +2/+2 until end of turn.').
card_first_print('mogg sentry', 'PLS').
card_image_name('mogg sentry'/'PLS', 'mogg sentry').
card_uid('mogg sentry'/'PLS', 'PLS:Mogg Sentry:mogg sentry').
card_rarity('mogg sentry'/'PLS', 'Rare').
card_artist('mogg sentry'/'PLS', 'Edward P. Beard, Jr.').
card_number('mogg sentry'/'PLS', '69').
card_flavor_text('mogg sentry'/'PLS', '\"Nobody stands in one place better than a mogg.\"\n—Tahngarth').
card_multiverse_id('mogg sentry'/'PLS', '26269').

card_in_set('morgue toad', 'PLS').
card_original_type('morgue toad'/'PLS', 'Creature — Toad').
card_original_text('morgue toad'/'PLS', 'Sacrifice Morgue Toad: Add {U}{R} to your mana pool.').
card_first_print('morgue toad', 'PLS').
card_image_name('morgue toad'/'PLS', 'morgue toad').
card_uid('morgue toad'/'PLS', 'PLS:Morgue Toad:morgue toad').
card_rarity('morgue toad'/'PLS', 'Common').
card_artist('morgue toad'/'PLS', 'Franz Vohwinkel').
card_number('morgue toad'/'PLS', '46').
card_flavor_text('morgue toad'/'PLS', '\"The toads of Urborg aren\'t fast, powerful, or pleasant, but they have their uses.\"\n—Ertai').
card_multiverse_id('morgue toad'/'PLS', '27589').

card_in_set('multani\'s harmony', 'PLS').
card_original_type('multani\'s harmony'/'PLS', 'Enchant Creature').
card_original_text('multani\'s harmony'/'PLS', 'Enchanted creature has \"{T}: Add one mana of any color to your mana pool.\"').
card_first_print('multani\'s harmony', 'PLS').
card_image_name('multani\'s harmony'/'PLS', 'multani\'s harmony').
card_uid('multani\'s harmony'/'PLS', 'PLS:Multani\'s Harmony:multani\'s harmony').
card_rarity('multani\'s harmony'/'PLS', 'Uncommon').
card_artist('multani\'s harmony'/'PLS', 'Darrell Riche').
card_number('multani\'s harmony'/'PLS', '84').
card_flavor_text('multani\'s harmony'/'PLS', '\"Welcome to paradise.\"').
card_multiverse_id('multani\'s harmony'/'PLS', '22298').

card_in_set('natural emergence', 'PLS').
card_original_type('natural emergence'/'PLS', 'Enchantment').
card_original_text('natural emergence'/'PLS', 'When Natural Emergence comes into play, return a red or green enchantment you control to its owner\'s hand.\nLands you control are 2/2 creatures with first strike. They\'re still lands.').
card_first_print('natural emergence', 'PLS').
card_image_name('natural emergence'/'PLS', 'natural emergence').
card_uid('natural emergence'/'PLS', 'PLS:Natural Emergence:natural emergence').
card_rarity('natural emergence'/'PLS', 'Rare').
card_artist('natural emergence'/'PLS', 'Heather Hudson').
card_number('natural emergence'/'PLS', '117').
card_multiverse_id('natural emergence'/'PLS', '26410').

card_in_set('nemata, grove guardian', 'PLS').
card_original_type('nemata, grove guardian'/'PLS', 'Creature — Treefolk Legend').
card_original_text('nemata, grove guardian'/'PLS', '{2}{G}: Put a 1/1 green Saproling creature token into play.\nSacrifice a Saproling: All Saprolings get +1/+1 until end of turn.').
card_first_print('nemata, grove guardian', 'PLS').
card_image_name('nemata, grove guardian'/'PLS', 'nemata, grove guardian').
card_uid('nemata, grove guardian'/'PLS', 'PLS:Nemata, Grove Guardian:nemata, grove guardian').
card_rarity('nemata, grove guardian'/'PLS', 'Rare').
card_artist('nemata, grove guardian'/'PLS', 'John Avon').
card_number('nemata, grove guardian'/'PLS', '85').
card_multiverse_id('nemata, grove guardian'/'PLS', '26412').

card_in_set('nightscape battlemage', 'PLS').
card_original_type('nightscape battlemage'/'PLS', 'Creature — Wizard').
card_original_text('nightscape battlemage'/'PLS', 'Kicker {2}{U} and/or {2}{R}\nWhen Nightscape Battlemage comes into play, if you paid the {2}{U} kicker cost, return up to two target nonblack creatures to their owners\' hands.\nWhen Nightscape Battlemage comes into play, if you paid the {2}{R} kicker cost, destroy target land.').
card_first_print('nightscape battlemage', 'PLS').
card_image_name('nightscape battlemage'/'PLS', 'nightscape battlemage').
card_uid('nightscape battlemage'/'PLS', 'PLS:Nightscape Battlemage:nightscape battlemage').
card_rarity('nightscape battlemage'/'PLS', 'Uncommon').
card_artist('nightscape battlemage'/'PLS', 'Andrew Goldhawk').
card_number('nightscape battlemage'/'PLS', '47').
card_multiverse_id('nightscape battlemage'/'PLS', '25914').

card_in_set('nightscape familiar', 'PLS').
card_original_type('nightscape familiar'/'PLS', 'Creature — Zombie').
card_original_text('nightscape familiar'/'PLS', 'Blue spells and red spells you play cost {1} less to play.\n{1}{B}: Regenerate Nightscape Familiar.').
card_first_print('nightscape familiar', 'PLS').
card_image_name('nightscape familiar'/'PLS', 'nightscape familiar').
card_uid('nightscape familiar'/'PLS', 'PLS:Nightscape Familiar:nightscape familiar').
card_rarity('nightscape familiar'/'PLS', 'Common').
card_artist('nightscape familiar'/'PLS', 'Jeff Easley').
card_number('nightscape familiar'/'PLS', '48').
card_flavor_text('nightscape familiar'/'PLS', 'Nightscape masters don\'t stop at raising the spirit of a fallen battlemage. They raise the flesh along with it.').
card_multiverse_id('nightscape familiar'/'PLS', '25693').

card_in_set('noxious vapors', 'PLS').
card_original_type('noxious vapors'/'PLS', 'Sorcery').
card_original_text('noxious vapors'/'PLS', 'Each player reveals his or her hand and chooses one card of each color from it, then discards all other nonland cards from it.').
card_first_print('noxious vapors', 'PLS').
card_image_name('noxious vapors'/'PLS', 'noxious vapors').
card_uid('noxious vapors'/'PLS', 'PLS:Noxious Vapors:noxious vapors').
card_rarity('noxious vapors'/'PLS', 'Uncommon').
card_artist('noxious vapors'/'PLS', 'Ben Thompson').
card_number('noxious vapors'/'PLS', '49').
card_flavor_text('noxious vapors'/'PLS', 'Urborg would just as soon steal your memories as your life.').
card_multiverse_id('noxious vapors'/'PLS', '26478').

card_in_set('orim\'s chant', 'PLS').
card_original_type('orim\'s chant'/'PLS', 'Instant').
card_original_text('orim\'s chant'/'PLS', 'Kicker {W} (You may pay an additional {W} as you play this spell.)\nTarget player can\'t play spells this turn.\nIf you paid the kicker cost, creatures can\'t attack this turn.').
card_image_name('orim\'s chant'/'PLS', 'orim\'s chant').
card_uid('orim\'s chant'/'PLS', 'PLS:Orim\'s Chant:orim\'s chant').
card_rarity('orim\'s chant'/'PLS', 'Rare').
card_artist('orim\'s chant'/'PLS', 'Kev Walker').
card_number('orim\'s chant'/'PLS', '11').
card_multiverse_id('orim\'s chant'/'PLS', '26852').

card_in_set('phyrexian bloodstock', 'PLS').
card_original_type('phyrexian bloodstock'/'PLS', 'Creature — Zombie').
card_original_text('phyrexian bloodstock'/'PLS', 'When Phyrexian Bloodstock leaves play, destroy target white creature. It can\'t be regenerated.').
card_first_print('phyrexian bloodstock', 'PLS').
card_image_name('phyrexian bloodstock'/'PLS', 'phyrexian bloodstock').
card_uid('phyrexian bloodstock'/'PLS', 'PLS:Phyrexian Bloodstock:phyrexian bloodstock').
card_rarity('phyrexian bloodstock'/'PLS', 'Common').
card_artist('phyrexian bloodstock'/'PLS', 'Mark Tedin').
card_number('phyrexian bloodstock'/'PLS', '50').
card_flavor_text('phyrexian bloodstock'/'PLS', 'It reeks of terror and chaos.').
card_multiverse_id('phyrexian bloodstock'/'PLS', '26280').

card_in_set('phyrexian scuta', 'PLS').
card_original_type('phyrexian scuta'/'PLS', 'Creature — Zombie').
card_original_text('phyrexian scuta'/'PLS', 'Kicker—Pay 3 life. (You may pay 3 life in addition to any other costs as you play this spell.)\nIf you paid the kicker cost, Phyrexian Scuta comes into play with two +1/+1 counters on it.').
card_first_print('phyrexian scuta', 'PLS').
card_image_name('phyrexian scuta'/'PLS', 'phyrexian scuta').
card_uid('phyrexian scuta'/'PLS', 'PLS:Phyrexian Scuta:phyrexian scuta').
card_rarity('phyrexian scuta'/'PLS', 'Rare').
card_artist('phyrexian scuta'/'PLS', 'Scott M. Fischer').
card_number('phyrexian scuta'/'PLS', '51').
card_multiverse_id('phyrexian scuta'/'PLS', '26477').

card_in_set('phyrexian tyranny', 'PLS').
card_original_type('phyrexian tyranny'/'PLS', 'Enchantment').
card_original_text('phyrexian tyranny'/'PLS', 'Whenever a player draws a card, that player loses 2 life unless he or she pays {2}.').
card_first_print('phyrexian tyranny', 'PLS').
card_image_name('phyrexian tyranny'/'PLS', 'phyrexian tyranny').
card_uid('phyrexian tyranny'/'PLS', 'PLS:Phyrexian Tyranny:phyrexian tyranny').
card_rarity('phyrexian tyranny'/'PLS', 'Rare').
card_artist('phyrexian tyranny'/'PLS', 'Kev Walker').
card_number('phyrexian tyranny'/'PLS', '118').
card_flavor_text('phyrexian tyranny'/'PLS', '\"He is Yawgmoth\'s reward to me. I shall kill him a hundred times a day.\"\n—Crovax, to Ertai').
card_multiverse_id('phyrexian tyranny'/'PLS', '26281').

card_in_set('planar overlay', 'PLS').
card_original_type('planar overlay'/'PLS', 'Sorcery').
card_original_text('planar overlay'/'PLS', 'Each player chooses a land he or she controls of each basic land type. Return those lands to their owners\' hands.').
card_first_print('planar overlay', 'PLS').
card_image_name('planar overlay'/'PLS', 'planar overlay').
card_uid('planar overlay'/'PLS', 'PLS:Planar Overlay:planar overlay').
card_rarity('planar overlay'/'PLS', 'Rare').
card_artist('planar overlay'/'PLS', 'Ron Walotsky').
card_number('planar overlay'/'PLS', '28').
card_flavor_text('planar overlay'/'PLS', 'The Phyrexians don\'t want to conquer Dominaria. They want to remake it.').
card_multiverse_id('planar overlay'/'PLS', '26835').

card_in_set('planeswalker\'s favor', 'PLS').
card_original_type('planeswalker\'s favor'/'PLS', 'Enchantment').
card_original_text('planeswalker\'s favor'/'PLS', '{3}{G}: Target opponent reveals a card at random from his or her hand. Target creature gets +X/+X until end of turn, where X is the revealed card\'s converted mana cost.').
card_first_print('planeswalker\'s favor', 'PLS').
card_image_name('planeswalker\'s favor'/'PLS', 'planeswalker\'s favor').
card_uid('planeswalker\'s favor'/'PLS', 'PLS:Planeswalker\'s Favor:planeswalker\'s favor').
card_rarity('planeswalker\'s favor'/'PLS', 'Rare').
card_artist('planeswalker\'s favor'/'PLS', 'Rebecca Guay').
card_number('planeswalker\'s favor'/'PLS', '86').
card_flavor_text('planeswalker\'s favor'/'PLS', 'Freyalise strengthens her faithful.').
card_multiverse_id('planeswalker\'s favor'/'PLS', '25890').

card_in_set('planeswalker\'s fury', 'PLS').
card_original_type('planeswalker\'s fury'/'PLS', 'Enchantment').
card_original_text('planeswalker\'s fury'/'PLS', '{3}{R}: Target opponent reveals a card at random from his or her hand. Planeswalker\'s Fury deals damage equal to that card\'s converted mana cost to that player. Play this ability only any time you could play a sorcery.').
card_first_print('planeswalker\'s fury', 'PLS').
card_image_name('planeswalker\'s fury'/'PLS', 'planeswalker\'s fury').
card_uid('planeswalker\'s fury'/'PLS', 'PLS:Planeswalker\'s Fury:planeswalker\'s fury').
card_rarity('planeswalker\'s fury'/'PLS', 'Rare').
card_artist('planeswalker\'s fury'/'PLS', 'Christopher Moeller').
card_number('planeswalker\'s fury'/'PLS', '70').
card_multiverse_id('planeswalker\'s fury'/'PLS', '25889').

card_in_set('planeswalker\'s mirth', 'PLS').
card_original_type('planeswalker\'s mirth'/'PLS', 'Enchantment').
card_original_text('planeswalker\'s mirth'/'PLS', '{3}{W}: Target opponent reveals a card at random from his or her hand. You gain life equal to that card\'s converted mana cost.').
card_first_print('planeswalker\'s mirth', 'PLS').
card_image_name('planeswalker\'s mirth'/'PLS', 'planeswalker\'s mirth').
card_uid('planeswalker\'s mirth'/'PLS', 'PLS:Planeswalker\'s Mirth:planeswalker\'s mirth').
card_rarity('planeswalker\'s mirth'/'PLS', 'Rare').
card_artist('planeswalker\'s mirth'/'PLS', 'John Matson').
card_number('planeswalker\'s mirth'/'PLS', '12').
card_flavor_text('planeswalker\'s mirth'/'PLS', '\"I enjoy a good joke as much as the next guy.\"\n—Commodore Guff').
card_multiverse_id('planeswalker\'s mirth'/'PLS', '25886').

card_in_set('planeswalker\'s mischief', 'PLS').
card_original_type('planeswalker\'s mischief'/'PLS', 'Enchantment').
card_original_text('planeswalker\'s mischief'/'PLS', '{3}{U}: Target opponent reveals a card at random from his or her hand. If it\'s an instant or sorcery card, remove it from the game. As long as it remains removed from the game, you may play it as though it were in your hand without paying its mana cost. If it has X in its mana cost, X is 0. At end of turn, if you haven\'t played it, return it to its owner\'s hand. Play this ability only any time you could play a sorcery.').
card_first_print('planeswalker\'s mischief', 'PLS').
card_image_name('planeswalker\'s mischief'/'PLS', 'planeswalker\'s mischief').
card_uid('planeswalker\'s mischief'/'PLS', 'PLS:Planeswalker\'s Mischief:planeswalker\'s mischief').
card_rarity('planeswalker\'s mischief'/'PLS', 'Rare').
card_artist('planeswalker\'s mischief'/'PLS', 'Pete Venters').
card_number('planeswalker\'s mischief'/'PLS', '29').
card_multiverse_id('planeswalker\'s mischief'/'PLS', '25887').

card_in_set('planeswalker\'s scorn', 'PLS').
card_original_type('planeswalker\'s scorn'/'PLS', 'Enchantment').
card_original_text('planeswalker\'s scorn'/'PLS', '{3}{B}: Target opponent reveals a card at random from his or her hand. Target creature gets -X/-X until end of turn, where X is the revealed card\'s converted mana cost. Play this ability only any time you could play a sorcery.').
card_first_print('planeswalker\'s scorn', 'PLS').
card_image_name('planeswalker\'s scorn'/'PLS', 'planeswalker\'s scorn').
card_uid('planeswalker\'s scorn'/'PLS', 'PLS:Planeswalker\'s Scorn:planeswalker\'s scorn').
card_rarity('planeswalker\'s scorn'/'PLS', 'Rare').
card_artist('planeswalker\'s scorn'/'PLS', 'Glen Angus').
card_number('planeswalker\'s scorn'/'PLS', '52').
card_multiverse_id('planeswalker\'s scorn'/'PLS', '25888').

card_in_set('pollen remedy', 'PLS').
card_original_type('pollen remedy'/'PLS', 'Instant').
card_original_text('pollen remedy'/'PLS', 'Kicker—Sacrifice a land. (You may sacrifice a land in addition to any other costs as you play this spell.)\nPrevent the next 3 damage that would be dealt this turn to any number of target creatures and/or players, divided as you choose. If you paid the kicker cost, prevent the next 6 damage this way instead.').
card_first_print('pollen remedy', 'PLS').
card_image_name('pollen remedy'/'PLS', 'pollen remedy').
card_uid('pollen remedy'/'PLS', 'PLS:Pollen Remedy:pollen remedy').
card_rarity('pollen remedy'/'PLS', 'Common').
card_artist('pollen remedy'/'PLS', 'Ben Thompson').
card_number('pollen remedy'/'PLS', '13').
card_multiverse_id('pollen remedy'/'PLS', '25941').

card_in_set('primal growth', 'PLS').
card_original_type('primal growth'/'PLS', 'Sorcery').
card_original_text('primal growth'/'PLS', 'Kicker—Sacrifice a creature. (You may sacrifice a creature in addition to any other costs as you play this spell.) \nSearch your library for a basic land card, put that card into play, then shuffle your library. If you paid the kicker cost, instead search your library for two basic land cards, put them into play, then shuffle your library.').
card_first_print('primal growth', 'PLS').
card_image_name('primal growth'/'PLS', 'primal growth').
card_uid('primal growth'/'PLS', 'PLS:Primal Growth:primal growth').
card_rarity('primal growth'/'PLS', 'Common').
card_artist('primal growth'/'PLS', 'rk post').
card_number('primal growth'/'PLS', '87').
card_multiverse_id('primal growth'/'PLS', '26396').

card_in_set('pygmy kavu', 'PLS').
card_original_type('pygmy kavu'/'PLS', 'Creature — Kavu').
card_original_text('pygmy kavu'/'PLS', 'When Pygmy Kavu comes into play, draw a card for each black creature your opponents control.').
card_first_print('pygmy kavu', 'PLS').
card_image_name('pygmy kavu'/'PLS', 'pygmy kavu').
card_uid('pygmy kavu'/'PLS', 'PLS:Pygmy Kavu:pygmy kavu').
card_rarity('pygmy kavu'/'PLS', 'Common').
card_artist('pygmy kavu'/'PLS', 'Greg Staples').
card_number('pygmy kavu'/'PLS', '88').
card_flavor_text('pygmy kavu'/'PLS', '\"We could learn a lot by watching the little ones,\" said Agnate.\n\"Define ‘little,\'\" replied Grizzlegom.').
card_multiverse_id('pygmy kavu'/'PLS', '23113').

card_in_set('questing phelddagrif', 'PLS').
card_original_type('questing phelddagrif'/'PLS', 'Creature — Phelddagrif').
card_original_text('questing phelddagrif'/'PLS', '{G}: Questing Phelddagrif gets +1/+1 until end of turn. Target opponent puts a 1/1 green Hippo creature token into play.\n{W}: Questing Phelddagrif gains protection from black and from red until end of turn. Target opponent gains 2 life.\n{U}: Questing Phelddagrif gains flying until end of turn. Target opponent may draw a card.').
card_image_name('questing phelddagrif'/'PLS', 'questing phelddagrif').
card_uid('questing phelddagrif'/'PLS', 'PLS:Questing Phelddagrif:questing phelddagrif').
card_rarity('questing phelddagrif'/'PLS', 'Rare').
card_artist('questing phelddagrif'/'PLS', 'Matt Cavotta').
card_number('questing phelddagrif'/'PLS', '119').
card_multiverse_id('questing phelddagrif'/'PLS', '27634').

card_in_set('quirion dryad', 'PLS').
card_original_type('quirion dryad'/'PLS', 'Creature — Dryad').
card_original_text('quirion dryad'/'PLS', 'Whenever you play a white, blue, black, or red spell, put a +1/+1 counter on Quirion Dryad.').
card_first_print('quirion dryad', 'PLS').
card_image_name('quirion dryad'/'PLS', 'quirion dryad').
card_uid('quirion dryad'/'PLS', 'PLS:Quirion Dryad:quirion dryad').
card_rarity('quirion dryad'/'PLS', 'Rare').
card_artist('quirion dryad'/'PLS', 'Don Hazeltine').
card_number('quirion dryad'/'PLS', '89').
card_flavor_text('quirion dryad'/'PLS', '\"Never underestimate the ability of natural forces to adapt to unnatural influences.\"\n—Molimo, maro-sorcerer').
card_multiverse_id('quirion dryad'/'PLS', '26286').

card_in_set('quirion explorer', 'PLS').
card_original_type('quirion explorer'/'PLS', 'Creature — Elf').
card_original_text('quirion explorer'/'PLS', '{T}: Add to your mana pool one mana of any color that a land an opponent controls could produce.').
card_first_print('quirion explorer', 'PLS').
card_image_name('quirion explorer'/'PLS', 'quirion explorer').
card_uid('quirion explorer'/'PLS', 'PLS:Quirion Explorer:quirion explorer').
card_rarity('quirion explorer'/'PLS', 'Common').
card_artist('quirion explorer'/'PLS', 'Ron Spears').
card_number('quirion explorer'/'PLS', '90').
card_flavor_text('quirion explorer'/'PLS', 'Fight with a friend at your back, steel in your hands, and magic in your veins.\n—Quirion creed').
card_multiverse_id('quirion explorer'/'PLS', '26348').

card_in_set('radiant kavu', 'PLS').
card_original_type('radiant kavu'/'PLS', 'Creature — Kavu').
card_original_text('radiant kavu'/'PLS', '{R}{G}{W}: Prevent all combat damage blue creatures and black creatures would deal this turn.').
card_first_print('radiant kavu', 'PLS').
card_image_name('radiant kavu'/'PLS', 'radiant kavu').
card_uid('radiant kavu'/'PLS', 'PLS:Radiant Kavu:radiant kavu').
card_rarity('radiant kavu'/'PLS', 'Rare').
card_artist('radiant kavu'/'PLS', 'Ron Spencer').
card_number('radiant kavu'/'PLS', '120').
card_flavor_text('radiant kavu'/'PLS', 'When angered or frightened, it protects itself with a burst of blinding light.').
card_multiverse_id('radiant kavu'/'PLS', '12691').

card_in_set('razing snidd', 'PLS').
card_original_type('razing snidd'/'PLS', 'Creature — Beast').
card_original_text('razing snidd'/'PLS', 'When Razing Snidd comes into play, return a black or red creature you control to its owner\'s hand.\nWhen Razing Snidd comes into play, each player sacrifices a land.').
card_first_print('razing snidd', 'PLS').
card_image_name('razing snidd'/'PLS', 'razing snidd').
card_uid('razing snidd'/'PLS', 'PLS:Razing Snidd:razing snidd').
card_rarity('razing snidd'/'PLS', 'Uncommon').
card_artist('razing snidd'/'PLS', 'Alan Pollack').
card_number('razing snidd'/'PLS', '121').
card_multiverse_id('razing snidd'/'PLS', '25937').

card_in_set('rith\'s charm', 'PLS').
card_original_type('rith\'s charm'/'PLS', 'Instant').
card_original_text('rith\'s charm'/'PLS', 'Choose one Destroy target nonbasic land; or put three 1/1 green Saproling creature tokens into play; or prevent all damage a source of your choice would deal this turn.').
card_first_print('rith\'s charm', 'PLS').
card_image_name('rith\'s charm'/'PLS', 'rith\'s charm').
card_uid('rith\'s charm'/'PLS', 'PLS:Rith\'s Charm:rith\'s charm').
card_rarity('rith\'s charm'/'PLS', 'Uncommon').
card_artist('rith\'s charm'/'PLS', 'David Martin').
card_number('rith\'s charm'/'PLS', '122').
card_multiverse_id('rith\'s charm'/'PLS', '25865').

card_in_set('rith\'s grove', 'PLS').
card_original_type('rith\'s grove'/'PLS', 'Land').
card_original_text('rith\'s grove'/'PLS', 'Rith\'s Grove is a Lair in addition to its land type.\nWhen Rith\'s Grove comes into play, sacrifice it unless you return a non-Lair land you control to its owner\'s hand.\n{T}: Add {R}, {G}, or {W} to your mana pool.').
card_first_print('rith\'s grove', 'PLS').
card_image_name('rith\'s grove'/'PLS', 'rith\'s grove').
card_uid('rith\'s grove'/'PLS', 'PLS:Rith\'s Grove:rith\'s grove').
card_rarity('rith\'s grove'/'PLS', 'Uncommon').
card_artist('rith\'s grove'/'PLS', 'Scott Bailey').
card_number('rith\'s grove'/'PLS', '141').
card_multiverse_id('rith\'s grove'/'PLS', '25939').

card_in_set('root greevil', 'PLS').
card_original_type('root greevil'/'PLS', 'Creature — Beast').
card_original_text('root greevil'/'PLS', '{2}{G}, {T}, Sacrifice Root Greevil: Destroy all enchantments of the color of your choice.').
card_first_print('root greevil', 'PLS').
card_image_name('root greevil'/'PLS', 'root greevil').
card_uid('root greevil'/'PLS', 'PLS:Root Greevil:root greevil').
card_rarity('root greevil'/'PLS', 'Common').
card_artist('root greevil'/'PLS', 'Andrew Robinson').
card_number('root greevil'/'PLS', '91').
card_flavor_text('root greevil'/'PLS', 'The root of all greevils.').
card_multiverse_id('root greevil'/'PLS', '8827').

card_in_set('rushing river', 'PLS').
card_original_type('rushing river'/'PLS', 'Instant').
card_original_text('rushing river'/'PLS', 'Kicker—Sacrifice a land. (You may sacrifice a land in addition to any other costs as you play this spell.)\nReturn target nonland permanent to its owner\'s hand. If you paid the kicker cost, return another target nonland permanent to its owner\'s hand.').
card_first_print('rushing river', 'PLS').
card_image_name('rushing river'/'PLS', 'rushing river').
card_uid('rushing river'/'PLS', 'PLS:Rushing River:rushing river').
card_rarity('rushing river'/'PLS', 'Common').
card_artist('rushing river'/'PLS', 'Don Hazeltine').
card_number('rushing river'/'PLS', '30').
card_multiverse_id('rushing river'/'PLS', '25942').

card_in_set('samite elder', 'PLS').
card_original_type('samite elder'/'PLS', 'Creature — Cleric').
card_original_text('samite elder'/'PLS', '{T}: Creatures you control gain protection from the color(s) of target permanent you control until end of turn.').
card_first_print('samite elder', 'PLS').
card_image_name('samite elder'/'PLS', 'samite elder').
card_uid('samite elder'/'PLS', 'PLS:Samite Elder:samite elder').
card_rarity('samite elder'/'PLS', 'Rare').
card_artist('samite elder'/'PLS', 'Terese Nielsen').
card_number('samite elder'/'PLS', '14').
card_multiverse_id('samite elder'/'PLS', '22962').

card_in_set('samite pilgrim', 'PLS').
card_original_type('samite pilgrim'/'PLS', 'Creature — Cleric').
card_original_text('samite pilgrim'/'PLS', '{T}: Prevent the next X damage that would be dealt to target creature this turn, where X is the number of basic land types among lands you control.').
card_first_print('samite pilgrim', 'PLS').
card_image_name('samite pilgrim'/'PLS', 'samite pilgrim').
card_uid('samite pilgrim'/'PLS', 'PLS:Samite Pilgrim:samite pilgrim').
card_rarity('samite pilgrim'/'PLS', 'Common').
card_artist('samite pilgrim'/'PLS', 'D. J. Cleland-Hura').
card_number('samite pilgrim'/'PLS', '15').
card_multiverse_id('samite pilgrim'/'PLS', '26271').

card_in_set('sawtooth loon', 'PLS').
card_original_type('sawtooth loon'/'PLS', 'Creature — Bird').
card_original_text('sawtooth loon'/'PLS', 'Flying\nWhen Sawtooth Loon comes into play, return a white or blue creature you control to its owner\'s hand.\nWhen Sawtooth Loon comes into play, draw two cards, then put two cards from your hand on the bottom of your library.').
card_first_print('sawtooth loon', 'PLS').
card_image_name('sawtooth loon'/'PLS', 'sawtooth loon').
card_uid('sawtooth loon'/'PLS', 'PLS:Sawtooth Loon:sawtooth loon').
card_rarity('sawtooth loon'/'PLS', 'Uncommon').
card_artist('sawtooth loon'/'PLS', 'Heather Hudson').
card_number('sawtooth loon'/'PLS', '123').
card_multiverse_id('sawtooth loon'/'PLS', '25933').

card_in_set('sea snidd', 'PLS').
card_original_type('sea snidd'/'PLS', 'Creature — Beast').
card_original_text('sea snidd'/'PLS', '{T}: Target land\'s type becomes the basic land type of your choice until end of turn.').
card_first_print('sea snidd', 'PLS').
card_image_name('sea snidd'/'PLS', 'sea snidd').
card_uid('sea snidd'/'PLS', 'PLS:Sea Snidd:sea snidd').
card_rarity('sea snidd'/'PLS', 'Common').
card_artist('sea snidd'/'PLS', 'Chippy').
card_number('sea snidd'/'PLS', '31').
card_flavor_text('sea snidd'/'PLS', 'It always has the home-turf advantage.').
card_multiverse_id('sea snidd'/'PLS', '26362').

card_in_set('shifting sky', 'PLS').
card_original_type('shifting sky'/'PLS', 'Enchantment').
card_original_text('shifting sky'/'PLS', 'As Shifting Sky comes into play, choose a color.\nAll nonland permanents are the chosen color.').
card_first_print('shifting sky', 'PLS').
card_image_name('shifting sky'/'PLS', 'shifting sky').
card_uid('shifting sky'/'PLS', 'PLS:Shifting Sky:shifting sky').
card_rarity('shifting sky'/'PLS', 'Uncommon').
card_artist('shifting sky'/'PLS', 'Jerry Tiritilli').
card_number('shifting sky'/'PLS', '32').
card_flavor_text('shifting sky'/'PLS', 'Master the sky and you rule everything beneath it.\n—Metathran saying').
card_multiverse_id('shifting sky'/'PLS', '26290').

card_in_set('shivan wurm', 'PLS').
card_original_type('shivan wurm'/'PLS', 'Creature — Wurm').
card_original_text('shivan wurm'/'PLS', 'Trample\nWhen Shivan Wurm comes into play, return a red or green creature you control to its owner\'s hand.').
card_first_print('shivan wurm', 'PLS').
card_image_name('shivan wurm'/'PLS', 'shivan wurm').
card_uid('shivan wurm'/'PLS', 'PLS:Shivan Wurm:shivan wurm').
card_rarity('shivan wurm'/'PLS', 'Rare').
card_artist('shivan wurm'/'PLS', 'Scott M. Fischer').
card_number('shivan wurm'/'PLS', '124').
card_multiverse_id('shivan wurm'/'PLS', '25924').

card_in_set('shriek of dread', 'PLS').
card_original_type('shriek of dread'/'PLS', 'Instant').
card_original_text('shriek of dread'/'PLS', 'Target creature can\'t be blocked this turn except by artifact creatures and/or black creatures.').
card_first_print('shriek of dread', 'PLS').
card_image_name('shriek of dread'/'PLS', 'shriek of dread').
card_uid('shriek of dread'/'PLS', 'PLS:Shriek of Dread:shriek of dread').
card_rarity('shriek of dread'/'PLS', 'Common').
card_artist('shriek of dread'/'PLS', 'Nelson DeCastro').
card_number('shriek of dread'/'PLS', '53').
card_flavor_text('shriek of dread'/'PLS', 'When Crosis roared, the battle ended and the slaughter began.').
card_multiverse_id('shriek of dread'/'PLS', '27229').

card_in_set('silver drake', 'PLS').
card_original_type('silver drake'/'PLS', 'Creature — Drake').
card_original_text('silver drake'/'PLS', 'Flying\nWhen Silver Drake comes into play, return a white or blue creature you control to its owner\'s hand.').
card_image_name('silver drake'/'PLS', 'silver drake').
card_uid('silver drake'/'PLS', 'PLS:Silver Drake:silver drake').
card_rarity('silver drake'/'PLS', 'Common').
card_artist('silver drake'/'PLS', 'Alan Pollack').
card_number('silver drake'/'PLS', '125').
card_multiverse_id('silver drake'/'PLS', '25925').

card_in_set('singe', 'PLS').
card_original_type('singe'/'PLS', 'Instant').
card_original_text('singe'/'PLS', 'Singe deals 1 damage to target creature. That creature becomes black until end of turn.').
card_first_print('singe', 'PLS').
card_image_name('singe'/'PLS', 'singe').
card_uid('singe'/'PLS', 'PLS:Singe:singe').
card_rarity('singe'/'PLS', 'Common').
card_artist('singe'/'PLS', 'John Avon').
card_number('singe'/'PLS', '71').
card_flavor_text('singe'/'PLS', 'Burnt flesh. Blackened soul.').
card_multiverse_id('singe'/'PLS', '25900').

card_in_set('sinister strength', 'PLS').
card_original_type('sinister strength'/'PLS', 'Enchant Creature').
card_original_text('sinister strength'/'PLS', 'Enchanted creature gets +3/+1 and is black.').
card_first_print('sinister strength', 'PLS').
card_image_name('sinister strength'/'PLS', 'sinister strength').
card_uid('sinister strength'/'PLS', 'PLS:Sinister Strength:sinister strength').
card_rarity('sinister strength'/'PLS', 'Common').
card_artist('sinister strength'/'PLS', 'Terese Nielsen').
card_number('sinister strength'/'PLS', '54').
card_flavor_text('sinister strength'/'PLS', '\"I bless the living with a song of death.\"\n—Lord Dralnu').
card_multiverse_id('sinister strength'/'PLS', '25954').

card_in_set('sisay\'s ingenuity', 'PLS').
card_original_type('sisay\'s ingenuity'/'PLS', 'Enchant Creature').
card_original_text('sisay\'s ingenuity'/'PLS', 'When Sisay\'s Ingenuity comes into play, draw a card.\nEnchanted creature has \"{2}{U}: Target creature becomes the color of your choice until end of turn.\"').
card_first_print('sisay\'s ingenuity', 'PLS').
card_image_name('sisay\'s ingenuity'/'PLS', 'sisay\'s ingenuity').
card_uid('sisay\'s ingenuity'/'PLS', 'PLS:Sisay\'s Ingenuity:sisay\'s ingenuity').
card_rarity('sisay\'s ingenuity'/'PLS', 'Common').
card_artist('sisay\'s ingenuity'/'PLS', 'Paolo Parente').
card_number('sisay\'s ingenuity'/'PLS', '33').
card_multiverse_id('sisay\'s ingenuity'/'PLS', '26407').

card_in_set('skyship weatherlight', 'PLS').
card_original_type('skyship weatherlight'/'PLS', 'Legendary Artifact').
card_original_text('skyship weatherlight'/'PLS', 'When Skyship Weatherlight comes into play, search your library for any number of artifact and/or creature cards and remove them from the game. Then shuffle your library.\n{4}, {T}: Choose a card at random that was removed from the game with Skyship Weatherlight. Put that card into your hand.').
card_first_print('skyship weatherlight', 'PLS').
card_image_name('skyship weatherlight'/'PLS', 'skyship weatherlight1').
card_uid('skyship weatherlight'/'PLS', 'PLS:Skyship Weatherlight:skyship weatherlight1').
card_rarity('skyship weatherlight'/'PLS', 'Rare').
card_artist('skyship weatherlight'/'PLS', 'Mark Tedin').
card_number('skyship weatherlight'/'PLS', '133').
card_multiverse_id('skyship weatherlight'/'PLS', '26480').

card_in_set('skyship weatherlight', 'PLS').
card_original_type('skyship weatherlight'/'PLS', 'Legendary Artifact').
card_original_text('skyship weatherlight'/'PLS', 'When Skyship Weatherlight comes into play, search your library for any number of artifact and/or creature cards and remove them from the game. Then shuffle your library.\n{4}, {T}: Choose a card at random that was removed from the game with Skyship Weatherlight. Put that card into your hand.').
card_image_name('skyship weatherlight'/'PLS', 'skyship weatherlight2').
card_uid('skyship weatherlight'/'PLS', 'PLS:Skyship Weatherlight:skyship weatherlight2').
card_rarity('skyship weatherlight'/'PLS', 'Rare').
card_artist('skyship weatherlight'/'PLS', 'Kev Walker').
card_number('skyship weatherlight'/'PLS', '★133').
card_multiverse_id('skyship weatherlight'/'PLS', '29293').

card_in_set('skyshroud blessing', 'PLS').
card_original_type('skyshroud blessing'/'PLS', 'Instant').
card_original_text('skyshroud blessing'/'PLS', 'Lands can\'t be the targets of spells or abilities this turn.\nDraw a card.').
card_first_print('skyshroud blessing', 'PLS').
card_image_name('skyshroud blessing'/'PLS', 'skyshroud blessing').
card_uid('skyshroud blessing'/'PLS', 'PLS:Skyshroud Blessing:skyshroud blessing').
card_rarity('skyshroud blessing'/'PLS', 'Uncommon').
card_artist('skyshroud blessing'/'PLS', 'Jerry Tiritilli').
card_number('skyshroud blessing'/'PLS', '92').
card_flavor_text('skyshroud blessing'/'PLS', '\"Behold,\" said Freyalise to Eladamri. \"Your Skyshroud home has followed you to Dominaria.\"').
card_multiverse_id('skyshroud blessing'/'PLS', '26402').

card_in_set('slay', 'PLS').
card_original_type('slay'/'PLS', 'Instant').
card_original_text('slay'/'PLS', 'Destroy target green creature. It can\'t be regenerated.\nDraw a card.').
card_first_print('slay', 'PLS').
card_image_name('slay'/'PLS', 'slay').
card_uid('slay'/'PLS', 'PLS:Slay:slay').
card_rarity('slay'/'PLS', 'Uncommon').
card_artist('slay'/'PLS', 'Ben Thompson').
card_number('slay'/'PLS', '55').
card_flavor_text('slay'/'PLS', 'The elves had the edge in guile, skill, and valor. But in the end, only sheer numbers mattered.').
card_multiverse_id('slay'/'PLS', '15137').

card_in_set('sleeping potion', 'PLS').
card_original_type('sleeping potion'/'PLS', 'Enchant Creature').
card_original_text('sleeping potion'/'PLS', 'When Sleeping Potion comes into play, tap enchanted creature.\nEnchanted creature doesn\'t untap during its controller\'s untap step.\nWhen enchanted creature becomes the target of a spell or ability, sacrifice Sleeping Potion.').
card_first_print('sleeping potion', 'PLS').
card_image_name('sleeping potion'/'PLS', 'sleeping potion').
card_uid('sleeping potion'/'PLS', 'PLS:Sleeping Potion:sleeping potion').
card_rarity('sleeping potion'/'PLS', 'Common').
card_artist('sleeping potion'/'PLS', 'Daren Bader').
card_number('sleeping potion'/'PLS', '34').
card_multiverse_id('sleeping potion'/'PLS', '26781').

card_in_set('slingshot goblin', 'PLS').
card_original_type('slingshot goblin'/'PLS', 'Creature — Goblin').
card_original_text('slingshot goblin'/'PLS', '{R}, {T}: Slingshot Goblin deals 2 damage to target blue creature.').
card_first_print('slingshot goblin', 'PLS').
card_image_name('slingshot goblin'/'PLS', 'slingshot goblin').
card_uid('slingshot goblin'/'PLS', 'PLS:Slingshot Goblin:slingshot goblin').
card_rarity('slingshot goblin'/'PLS', 'Common').
card_artist('slingshot goblin'/'PLS', 'Jeff Easley').
card_number('slingshot goblin'/'PLS', '72').
card_flavor_text('slingshot goblin'/'PLS', 'It\'s most effective when it doesn\'t eat the ammunition.').
card_multiverse_id('slingshot goblin'/'PLS', '22025').

card_in_set('sparkcaster', 'PLS').
card_original_type('sparkcaster'/'PLS', 'Creature — Kavu').
card_original_text('sparkcaster'/'PLS', 'When Sparkcaster comes into play, return a red or green creature you control to its owner\'s hand.\nWhen Sparkcaster comes into play, it deals 1 damage to target player.').
card_first_print('sparkcaster', 'PLS').
card_image_name('sparkcaster'/'PLS', 'sparkcaster').
card_uid('sparkcaster'/'PLS', 'PLS:Sparkcaster:sparkcaster').
card_rarity('sparkcaster'/'PLS', 'Uncommon').
card_artist('sparkcaster'/'PLS', 'Adam Rex').
card_number('sparkcaster'/'PLS', '126').
card_multiverse_id('sparkcaster'/'PLS', '25935').

card_in_set('star compass', 'PLS').
card_original_type('star compass'/'PLS', 'Artifact').
card_original_text('star compass'/'PLS', 'Star Compass comes into play tapped.\n{T}: Add to your mana pool one mana of any color a basic land you control could produce.').
card_first_print('star compass', 'PLS').
card_image_name('star compass'/'PLS', 'star compass').
card_uid('star compass'/'PLS', 'PLS:Star Compass:star compass').
card_rarity('star compass'/'PLS', 'Uncommon').
card_artist('star compass'/'PLS', 'Donato Giancola').
card_number('star compass'/'PLS', '134').
card_flavor_text('star compass'/'PLS', 'It doesn\'t point north. It points home.').
card_multiverse_id('star compass'/'PLS', '25880').

card_in_set('steel leaf paladin', 'PLS').
card_original_type('steel leaf paladin'/'PLS', 'Creature — Knight').
card_original_text('steel leaf paladin'/'PLS', 'First strike\nWhen Steel Leaf Paladin comes into play, return a green or white creature you control to its owner\'s hand.').
card_first_print('steel leaf paladin', 'PLS').
card_image_name('steel leaf paladin'/'PLS', 'steel leaf paladin').
card_uid('steel leaf paladin'/'PLS', 'PLS:Steel Leaf Paladin:steel leaf paladin').
card_rarity('steel leaf paladin'/'PLS', 'Common').
card_artist('steel leaf paladin'/'PLS', 'Paolo Parente').
card_number('steel leaf paladin'/'PLS', '127').
card_multiverse_id('steel leaf paladin'/'PLS', '25929').

card_in_set('stone kavu', 'PLS').
card_original_type('stone kavu'/'PLS', 'Creature — Kavu').
card_original_text('stone kavu'/'PLS', '{R}: Stone Kavu gets +1/+0 until end of turn.\n{W}: Stone Kavu gets +0/+1 until end of turn.').
card_first_print('stone kavu', 'PLS').
card_image_name('stone kavu'/'PLS', 'stone kavu').
card_uid('stone kavu'/'PLS', 'PLS:Stone Kavu:stone kavu').
card_rarity('stone kavu'/'PLS', 'Common').
card_artist('stone kavu'/'PLS', 'Adam Rex').
card_number('stone kavu'/'PLS', '93').
card_flavor_text('stone kavu'/'PLS', 'To maintain its hide, it consumes a variety of rocks—not to mention a variety of Phyrexians.').
card_multiverse_id('stone kavu'/'PLS', '26377').

card_in_set('stormscape battlemage', 'PLS').
card_original_type('stormscape battlemage'/'PLS', 'Creature — Wizard').
card_original_text('stormscape battlemage'/'PLS', 'Kicker {W} and/or {2}{B}\nWhen Stormscape Battlemage comes into play, if you paid the {W} kicker cost, you gain 3 life.\nWhen Stormscape Battlemage comes into play, if you paid the {2}{B} kicker cost, destroy target nonblack creature. That creature can\'t be regenerated.').
card_first_print('stormscape battlemage', 'PLS').
card_image_name('stormscape battlemage'/'PLS', 'stormscape battlemage').
card_uid('stormscape battlemage'/'PLS', 'PLS:Stormscape Battlemage:stormscape battlemage').
card_rarity('stormscape battlemage'/'PLS', 'Uncommon').
card_artist('stormscape battlemage'/'PLS', 'Christopher Moeller').
card_number('stormscape battlemage'/'PLS', '35').
card_multiverse_id('stormscape battlemage'/'PLS', '25913').

card_in_set('stormscape familiar', 'PLS').
card_original_type('stormscape familiar'/'PLS', 'Creature — Bird').
card_original_text('stormscape familiar'/'PLS', 'Flying\nWhite spells and black spells you play cost {1} less to play.').
card_first_print('stormscape familiar', 'PLS').
card_image_name('stormscape familiar'/'PLS', 'stormscape familiar').
card_uid('stormscape familiar'/'PLS', 'PLS:Stormscape Familiar:stormscape familiar').
card_rarity('stormscape familiar'/'PLS', 'Common').
card_artist('stormscape familiar'/'PLS', 'Heather Hudson').
card_number('stormscape familiar'/'PLS', '36').
card_flavor_text('stormscape familiar'/'PLS', 'Each Stormscape apprentice must hand-raise and train an owl before attaining the rank of master.').
card_multiverse_id('stormscape familiar'/'PLS', '25616').

card_in_set('strafe', 'PLS').
card_original_type('strafe'/'PLS', 'Sorcery').
card_original_text('strafe'/'PLS', 'Strafe deals 3 damage to target nonred creature.').
card_first_print('strafe', 'PLS').
card_image_name('strafe'/'PLS', 'strafe').
card_uid('strafe'/'PLS', 'PLS:Strafe:strafe').
card_rarity('strafe'/'PLS', 'Uncommon').
card_artist('strafe'/'PLS', 'Jim Nelson').
card_number('strafe'/'PLS', '73').
card_flavor_text('strafe'/'PLS', '\"All right, let\'s light ‘em up\"\n—Gerrard').
card_multiverse_id('strafe'/'PLS', '26288').

card_in_set('stratadon', 'PLS').
card_original_type('stratadon'/'PLS', 'Artifact Creature').
card_original_text('stratadon'/'PLS', 'Stratadon costs {1} less to play for each basic land type among lands you control.\nTrample').
card_first_print('stratadon', 'PLS').
card_image_name('stratadon'/'PLS', 'stratadon').
card_uid('stratadon'/'PLS', 'PLS:Stratadon:stratadon').
card_rarity('stratadon'/'PLS', 'Uncommon').
card_artist('stratadon'/'PLS', 'Brian Snõddy').
card_number('stratadon'/'PLS', '135').
card_flavor_text('stratadon'/'PLS', 'Designed like the gargadons of Keld—big and brutal.').
card_multiverse_id('stratadon'/'PLS', '25869').

card_in_set('sunken hope', 'PLS').
card_original_type('sunken hope'/'PLS', 'Enchantment').
card_original_text('sunken hope'/'PLS', 'At the beginning of each player\'s upkeep, that player returns a creature he or she controls to its owner\'s hand.').
card_first_print('sunken hope', 'PLS').
card_image_name('sunken hope'/'PLS', 'sunken hope').
card_uid('sunken hope'/'PLS', 'PLS:Sunken Hope:sunken hope').
card_rarity('sunken hope'/'PLS', 'Rare').
card_artist('sunken hope'/'PLS', 'Greg Staples').
card_number('sunken hope'/'PLS', '37').
card_flavor_text('sunken hope'/'PLS', 'While Dominarians fought for their world, Rath stole it from under their feet.').
card_multiverse_id('sunken hope'/'PLS', '21305').

card_in_set('sunscape battlemage', 'PLS').
card_original_type('sunscape battlemage'/'PLS', 'Creature — Wizard').
card_original_text('sunscape battlemage'/'PLS', 'Kicker {1}{G} and/or {2}{U}\nWhen Sunscape Battlemage comes into play, if you paid the {1}{G} kicker cost, destroy target creature with flying.\nWhen Sunscape Battlemage comes into play, if you paid the {2}{U} kicker cost, draw two cards.').
card_first_print('sunscape battlemage', 'PLS').
card_image_name('sunscape battlemage'/'PLS', 'sunscape battlemage').
card_uid('sunscape battlemage'/'PLS', 'PLS:Sunscape Battlemage:sunscape battlemage').
card_rarity('sunscape battlemage'/'PLS', 'Uncommon').
card_artist('sunscape battlemage'/'PLS', 'Tony Szczudlo').
card_number('sunscape battlemage'/'PLS', '16').
card_multiverse_id('sunscape battlemage'/'PLS', '25912').

card_in_set('sunscape familiar', 'PLS').
card_original_type('sunscape familiar'/'PLS', 'Creature — Wall').
card_original_text('sunscape familiar'/'PLS', '(Walls can\'t attack.)\nGreen spells and blue spells you play cost {1} less to play.').
card_first_print('sunscape familiar', 'PLS').
card_image_name('sunscape familiar'/'PLS', 'sunscape familiar').
card_uid('sunscape familiar'/'PLS', 'PLS:Sunscape Familiar:sunscape familiar').
card_rarity('sunscape familiar'/'PLS', 'Common').
card_artist('sunscape familiar'/'PLS', 'Brian Despain').
card_number('sunscape familiar'/'PLS', '17').
card_flavor_text('sunscape familiar'/'PLS', 'The spirits of fallen battlemages can serve their guilds as familiars by joining with any physical form.').
card_multiverse_id('sunscape familiar'/'PLS', '25615').

card_in_set('surprise deployment', 'PLS').
card_original_type('surprise deployment'/'PLS', 'Instant').
card_original_text('surprise deployment'/'PLS', 'Play Surprise Deployment only during combat.\nPut a nonwhite creature card from your hand into play. At end of turn, return that creature to your hand. (Return it only if it\'s in play.)').
card_first_print('surprise deployment', 'PLS').
card_image_name('surprise deployment'/'PLS', 'surprise deployment').
card_uid('surprise deployment'/'PLS', 'PLS:Surprise Deployment:surprise deployment').
card_rarity('surprise deployment'/'PLS', 'Uncommon').
card_artist('surprise deployment'/'PLS', 'Bradley Williams').
card_number('surprise deployment'/'PLS', '18').
card_multiverse_id('surprise deployment'/'PLS', '26403').

card_in_set('tahngarth, talruum hero', 'PLS').
card_original_type('tahngarth, talruum hero'/'PLS', 'Creature — Minotaur Legend').
card_original_text('tahngarth, talruum hero'/'PLS', 'Attacking doesn\'t cause Tahngarth, Talruum Hero to tap.\n{1}{R}, {T}: Tahngarth deals damage equal to its power to target creature. That creature deals damage equal to its power to Tahngarth.').
card_first_print('tahngarth, talruum hero', 'PLS').
card_image_name('tahngarth, talruum hero'/'PLS', 'tahngarth, talruum hero1').
card_uid('tahngarth, talruum hero'/'PLS', 'PLS:Tahngarth, Talruum Hero:tahngarth, talruum hero1').
card_rarity('tahngarth, talruum hero'/'PLS', 'Rare').
card_artist('tahngarth, talruum hero'/'PLS', 'Dave Dorman').
card_number('tahngarth, talruum hero'/'PLS', '74').
card_multiverse_id('tahngarth, talruum hero'/'PLS', '26408').

card_in_set('tahngarth, talruum hero', 'PLS').
card_original_type('tahngarth, talruum hero'/'PLS', 'Creature — Minotaur Legend').
card_original_text('tahngarth, talruum hero'/'PLS', 'Attacking doesn\'t cause Tahngarth, Talruum Hero to tap.\n{1}{R}, {T}: Tahngarth deals damage equal to its power to target creature. That creature deals damage equal to its power to Tahngarth.').
card_image_name('tahngarth, talruum hero'/'PLS', 'tahngarth, talruum hero2').
card_uid('tahngarth, talruum hero'/'PLS', 'PLS:Tahngarth, Talruum Hero:tahngarth, talruum hero2').
card_rarity('tahngarth, talruum hero'/'PLS', 'Rare').
card_artist('tahngarth, talruum hero'/'PLS', 'Kev Walker').
card_number('tahngarth, talruum hero'/'PLS', '★74').
card_multiverse_id('tahngarth, talruum hero'/'PLS', '29291').

card_in_set('terminal moraine', 'PLS').
card_original_type('terminal moraine'/'PLS', 'Land').
card_original_text('terminal moraine'/'PLS', '{T}: Add one colorless mana to your mana pool.\n{2}, {T}, Sacrifice Terminal Moraine: Search your library for a basic land card and put that card into play tapped. Then shuffle your library.').
card_first_print('terminal moraine', 'PLS').
card_image_name('terminal moraine'/'PLS', 'terminal moraine').
card_uid('terminal moraine'/'PLS', 'PLS:Terminal Moraine:terminal moraine').
card_rarity('terminal moraine'/'PLS', 'Uncommon').
card_artist('terminal moraine'/'PLS', 'Scott Bailey').
card_number('terminal moraine'/'PLS', '142').
card_multiverse_id('terminal moraine'/'PLS', '25980').

card_in_set('terminate', 'PLS').
card_original_type('terminate'/'PLS', 'Instant').
card_original_text('terminate'/'PLS', 'Destroy target creature. It can\'t be regenerated.').
card_image_name('terminate'/'PLS', 'terminate').
card_uid('terminate'/'PLS', 'PLS:Terminate:terminate').
card_rarity('terminate'/'PLS', 'Common').
card_artist('terminate'/'PLS', 'DiTerlizzi').
card_number('terminate'/'PLS', '128').
card_flavor_text('terminate'/'PLS', 'Like his mother before him, Darigaaz gave his life defending Dominaria from the tyranny of self-styled \"gods.\"').
card_multiverse_id('terminate'/'PLS', '25871').

card_in_set('thornscape battlemage', 'PLS').
card_original_type('thornscape battlemage'/'PLS', 'Creature — Wizard').
card_original_text('thornscape battlemage'/'PLS', 'Kicker {R} and/or {W}\nWhen Thornscape Battlemage comes into play, if you paid the {R} kicker cost, Thornscape Battlemage deals 2 damage to target creature or player.\nWhen Thornscape Battlemage comes into play, if you paid the {W} kicker cost, destroy target artifact.').
card_first_print('thornscape battlemage', 'PLS').
card_image_name('thornscape battlemage'/'PLS', 'thornscape battlemage').
card_uid('thornscape battlemage'/'PLS', 'PLS:Thornscape Battlemage:thornscape battlemage').
card_rarity('thornscape battlemage'/'PLS', 'Uncommon').
card_artist('thornscape battlemage'/'PLS', 'Matt Cavotta').
card_number('thornscape battlemage'/'PLS', '94').
card_multiverse_id('thornscape battlemage'/'PLS', '25916').

card_in_set('thornscape familiar', 'PLS').
card_original_type('thornscape familiar'/'PLS', 'Creature — Insect').
card_original_text('thornscape familiar'/'PLS', 'Red spells and white spells you play cost {1} less to play.').
card_first_print('thornscape familiar', 'PLS').
card_image_name('thornscape familiar'/'PLS', 'thornscape familiar').
card_uid('thornscape familiar'/'PLS', 'PLS:Thornscape Familiar:thornscape familiar').
card_rarity('thornscape familiar'/'PLS', 'Common').
card_artist('thornscape familiar'/'PLS', 'Heather Hudson').
card_number('thornscape familiar'/'PLS', '95').
card_flavor_text('thornscape familiar'/'PLS', 'Blessed with many options, Thornscape masters favor scarabs as familiars.').
card_multiverse_id('thornscape familiar'/'PLS', '25618').

card_in_set('thunderscape battlemage', 'PLS').
card_original_type('thunderscape battlemage'/'PLS', 'Creature — Wizard').
card_original_text('thunderscape battlemage'/'PLS', 'Kicker {1}{B} and/or {G}\nWhen Thunderscape Battlemage comes into play, if you paid the {1}{B} kicker cost, target player discards two cards from his or her hand.\nWhen Thunderscape Battlemage comes into play, if you paid the {G} kicker cost, destroy target enchantment.').
card_first_print('thunderscape battlemage', 'PLS').
card_image_name('thunderscape battlemage'/'PLS', 'thunderscape battlemage').
card_uid('thunderscape battlemage'/'PLS', 'PLS:Thunderscape Battlemage:thunderscape battlemage').
card_rarity('thunderscape battlemage'/'PLS', 'Uncommon').
card_artist('thunderscape battlemage'/'PLS', 'Mike Ploog').
card_number('thunderscape battlemage'/'PLS', '75').
card_multiverse_id('thunderscape battlemage'/'PLS', '25915').

card_in_set('thunderscape familiar', 'PLS').
card_original_type('thunderscape familiar'/'PLS', 'Creature — Kavu').
card_original_text('thunderscape familiar'/'PLS', 'First strike\nBlack spells and green spells you play cost {1} less to play.').
card_first_print('thunderscape familiar', 'PLS').
card_image_name('thunderscape familiar'/'PLS', 'thunderscape familiar').
card_uid('thunderscape familiar'/'PLS', 'PLS:Thunderscape Familiar:thunderscape familiar').
card_rarity('thunderscape familiar'/'PLS', 'Common').
card_artist('thunderscape familiar'/'PLS', 'Daren Bader').
card_number('thunderscape familiar'/'PLS', '76').
card_flavor_text('thunderscape familiar'/'PLS', 'Thunderscape masters release familiars to help guide their apprentices down the thunderscape path.').
card_multiverse_id('thunderscape familiar'/'PLS', '25617').

card_in_set('treva\'s charm', 'PLS').
card_original_type('treva\'s charm'/'PLS', 'Instant').
card_original_text('treva\'s charm'/'PLS', 'Choose one Destroy target enchantment; or remove target attacking creature from the game; or draw a card, then discard a card from your hand.').
card_first_print('treva\'s charm', 'PLS').
card_image_name('treva\'s charm'/'PLS', 'treva\'s charm').
card_uid('treva\'s charm'/'PLS', 'PLS:Treva\'s Charm:treva\'s charm').
card_rarity('treva\'s charm'/'PLS', 'Uncommon').
card_artist('treva\'s charm'/'PLS', 'David Martin').
card_number('treva\'s charm'/'PLS', '129').
card_multiverse_id('treva\'s charm'/'PLS', '25866').

card_in_set('treva\'s ruins', 'PLS').
card_original_type('treva\'s ruins'/'PLS', 'Land').
card_original_text('treva\'s ruins'/'PLS', 'Treva\'s Ruins is a Lair in addition to its land type.\nWhen Treva\'s Ruins comes into play, sacrifice it unless you return a non-Lair land you control to its owner\'s hand.\n{T}: Add {G}, {W}, or {U} to your mana pool.').
card_first_print('treva\'s ruins', 'PLS').
card_image_name('treva\'s ruins'/'PLS', 'treva\'s ruins').
card_uid('treva\'s ruins'/'PLS', 'PLS:Treva\'s Ruins:treva\'s ruins').
card_rarity('treva\'s ruins'/'PLS', 'Uncommon').
card_artist('treva\'s ruins'/'PLS', 'Jerry Tiritilli').
card_number('treva\'s ruins'/'PLS', '143').
card_multiverse_id('treva\'s ruins'/'PLS', '25940').

card_in_set('urza\'s guilt', 'PLS').
card_original_type('urza\'s guilt'/'PLS', 'Sorcery').
card_original_text('urza\'s guilt'/'PLS', 'Each player draws two cards, then discards three cards from his or her hand, then loses 4 life.').
card_first_print('urza\'s guilt', 'PLS').
card_image_name('urza\'s guilt'/'PLS', 'urza\'s guilt').
card_uid('urza\'s guilt'/'PLS', 'PLS:Urza\'s Guilt:urza\'s guilt').
card_rarity('urza\'s guilt'/'PLS', 'Rare').
card_artist('urza\'s guilt'/'PLS', 'Paolo Parente').
card_number('urza\'s guilt'/'PLS', '130').
card_flavor_text('urza\'s guilt'/'PLS', 'Deep in Yawgmoth\'s realm, Urza stopped dead in his tracks and rubbed his soot-filled eyes. \"Mishra?\"').
card_multiverse_id('urza\'s guilt'/'PLS', '25918').

card_in_set('voice of all', 'PLS').
card_original_type('voice of all'/'PLS', 'Creature — Angel').
card_original_text('voice of all'/'PLS', 'Flying\nAs Voice of All comes into play, choose a color.\nVoice of All has protection from the chosen color.').
card_first_print('voice of all', 'PLS').
card_image_name('voice of all'/'PLS', 'voice of all').
card_uid('voice of all'/'PLS', 'PLS:Voice of All:voice of all').
card_rarity('voice of all'/'PLS', 'Uncommon').
card_artist('voice of all'/'PLS', 'rk post').
card_number('voice of all'/'PLS', '19').
card_multiverse_id('voice of all'/'PLS', '26417').

card_in_set('volcano imp', 'PLS').
card_original_type('volcano imp'/'PLS', 'Creature — Imp').
card_original_text('volcano imp'/'PLS', 'Flying\n{1}{R}: Volcano Imp gains first strike until end of turn.').
card_first_print('volcano imp', 'PLS').
card_image_name('volcano imp'/'PLS', 'volcano imp').
card_uid('volcano imp'/'PLS', 'PLS:Volcano Imp:volcano imp').
card_rarity('volcano imp'/'PLS', 'Common').
card_artist('volcano imp'/'PLS', 'Thomas M. Baxa').
card_number('volcano imp'/'PLS', '56').
card_flavor_text('volcano imp'/'PLS', 'Its claws cut faster than a flame flickers.').
card_multiverse_id('volcano imp'/'PLS', '26368').

card_in_set('warped devotion', 'PLS').
card_original_type('warped devotion'/'PLS', 'Enchantment').
card_original_text('warped devotion'/'PLS', 'Whenever a permanent is returned to a player\'s hand, that player discards a card from his or her hand.').
card_first_print('warped devotion', 'PLS').
card_image_name('warped devotion'/'PLS', 'warped devotion').
card_uid('warped devotion'/'PLS', 'PLS:Warped Devotion:warped devotion').
card_rarity('warped devotion'/'PLS', 'Uncommon').
card_artist('warped devotion'/'PLS', 'Orizio Daniele').
card_number('warped devotion'/'PLS', '57').
card_flavor_text('warped devotion'/'PLS', '\"Before the glory of Yawgmoth, yes, even this makes sense.\"\n—Urza, to Gerrard').
card_multiverse_id('warped devotion'/'PLS', '26836').

card_in_set('waterspout elemental', 'PLS').
card_original_type('waterspout elemental'/'PLS', 'Creature — Elemental').
card_original_text('waterspout elemental'/'PLS', 'Kicker {U} (You may pay an additional {U} as you play this spell.)\nFlying\nWhen Waterspout Elemental comes into play, if you paid the kicker cost, return all other creatures to their owners\' hands and you skip your next turn.').
card_first_print('waterspout elemental', 'PLS').
card_image_name('waterspout elemental'/'PLS', 'waterspout elemental').
card_uid('waterspout elemental'/'PLS', 'PLS:Waterspout Elemental:waterspout elemental').
card_rarity('waterspout elemental'/'PLS', 'Rare').
card_artist('waterspout elemental'/'PLS', 'Mark Romanoski').
card_number('waterspout elemental'/'PLS', '38').
card_multiverse_id('waterspout elemental'/'PLS', '26851').
