% Fallen Empires

set('FEM').
set_name('FEM', 'Fallen Empires').
set_release_date('FEM', '1994-11-01').
set_border('FEM', 'black').
set_type('FEM', 'expansion').

card_in_set('aeolipile', 'FEM').
card_original_type('aeolipile'/'FEM', 'Artifact').
card_original_text('aeolipile'/'FEM', '{1}, {T}: Sacrifice Aeolipile to have it deal 2 damage to any target.').
card_first_print('aeolipile', 'FEM').
card_image_name('aeolipile'/'FEM', 'aeolipile').
card_uid('aeolipile'/'FEM', 'FEM:Aeolipile:aeolipile').
card_rarity('aeolipile'/'FEM', 'Rare').
card_artist('aeolipile'/'FEM', 'Heather Hudson').
card_flavor_text('aeolipile'/'FEM', '\"Although fragile, the Aeolipile could be quite destructive.\"\n—Sarpadian Empires, vol. I').
card_multiverse_id('aeolipile'/'FEM', '1827').

card_in_set('armor thrull', 'FEM').
card_original_type('armor thrull'/'FEM', 'Summon — Thrull').
card_original_text('armor thrull'/'FEM', '{T}: Sacrifice Armor Thrull to put a +1/+2 counter on a target creature.').
card_first_print('armor thrull', 'FEM').
card_image_name('armor thrull'/'FEM', 'armor thrull1').
card_uid('armor thrull'/'FEM', 'FEM:Armor Thrull:armor thrull1').
card_rarity('armor thrull'/'FEM', 'Common').
card_artist('armor thrull'/'FEM', 'Pete Venters').
card_flavor_text('armor thrull'/'FEM', '\"The worst thing about being a mercenary for the Ebon Hand is having to wear a dead Thrull.\"\n—Ivra Jursdotter').
card_multiverse_id('armor thrull'/'FEM', '1841').

card_in_set('armor thrull', 'FEM').
card_original_type('armor thrull'/'FEM', 'Summon — Thrull').
card_original_text('armor thrull'/'FEM', '{T}: Sacrifice Armor Thrull to put a +1/+2 counter on a target creature.').
card_image_name('armor thrull'/'FEM', 'armor thrull2').
card_uid('armor thrull'/'FEM', 'FEM:Armor Thrull:armor thrull2').
card_rarity('armor thrull'/'FEM', 'Common').
card_artist('armor thrull'/'FEM', 'Ron Spencer').
card_flavor_text('armor thrull'/'FEM', '\"They gave their lives to strengthen the Order\'s armies, until they declined this honor in favor of joining the Thrull Rebellion.\"\n—Jherana Rure, Counter-Insurgency Commander').
card_multiverse_id('armor thrull'/'FEM', '1840').

card_in_set('armor thrull', 'FEM').
card_original_type('armor thrull'/'FEM', 'Summon — Thrull').
card_original_text('armor thrull'/'FEM', '{T}: Sacrifice Armor Thrull to put a +1/+2 counter on a target creature.').
card_image_name('armor thrull'/'FEM', 'armor thrull3').
card_uid('armor thrull'/'FEM', 'FEM:Armor Thrull:armor thrull3').
card_rarity('armor thrull'/'FEM', 'Common').
card_artist('armor thrull'/'FEM', 'Jeff A. Menges').
card_flavor_text('armor thrull'/'FEM', '\"Except for a bare spot at the base of their skulls, Armor Thrulls bear interlocking plates and spikes with the strength of steel. Their design is almost artistic.\"\n—Endrek Sahr, \"Thrulls\"').
card_multiverse_id('armor thrull'/'FEM', '1838').

card_in_set('armor thrull', 'FEM').
card_original_type('armor thrull'/'FEM', 'Summon — Thrull').
card_original_text('armor thrull'/'FEM', '{T}: Sacrifice Armor Thrull to put a +1/+2 counter on a target creature.').
card_image_name('armor thrull'/'FEM', 'armor thrull4').
card_uid('armor thrull'/'FEM', 'FEM:Armor Thrull:armor thrull4').
card_rarity('armor thrull'/'FEM', 'Common').
card_artist('armor thrull'/'FEM', 'Scott Kirschner').
card_flavor_text('armor thrull'/'FEM', 'Occasionally, an ally balked at wearing a dead Thrull as armor. The priests\' whips, however, were usually enough to encourage a more practical outlook.').
card_multiverse_id('armor thrull'/'FEM', '1839').

card_in_set('balm of restoration', 'FEM').
card_original_type('balm of restoration'/'FEM', 'Artifact').
card_original_text('balm of restoration'/'FEM', '{1}, {T}: Sacrifice Balm of Restoration to gain 2 life or prevent up to 2 damage to any player or creature.').
card_first_print('balm of restoration', 'FEM').
card_image_name('balm of restoration'/'FEM', 'balm of restoration').
card_uid('balm of restoration'/'FEM', 'FEM:Balm of Restoration:balm of restoration').
card_rarity('balm of restoration'/'FEM', 'Rare').
card_artist('balm of restoration'/'FEM', 'Margaret Organ-Kean').
card_flavor_text('balm of restoration'/'FEM', '\"Not all armies enjoyed the services of a medic. For them, Balm of Restoration was that much more valuable.\"\n—Sarpadian Empires, vol. I').
card_multiverse_id('balm of restoration'/'FEM', '1828').

card_in_set('basal thrull', 'FEM').
card_original_type('basal thrull'/'FEM', 'Summon — Thrull').
card_original_text('basal thrull'/'FEM', '{T}: Sacrifice Basal Thrull to add {B}{B} to your mana pool. Play this ability as an interrupt.').
card_first_print('basal thrull', 'FEM').
card_image_name('basal thrull'/'FEM', 'basal thrull1').
card_uid('basal thrull'/'FEM', 'FEM:Basal Thrull:basal thrull1').
card_rarity('basal thrull'/'FEM', 'Common').
card_artist('basal thrull'/'FEM', 'Kaja Foglio').
card_flavor_text('basal thrull'/'FEM', 'Initially bred for sacrifice, the Thrulls eventually turned on their masters, the Order of the Ebon Hand, with gruesome results.\n—Sarpadian Empires, vol. II').
card_multiverse_id('basal thrull'/'FEM', '1842').

card_in_set('basal thrull', 'FEM').
card_original_type('basal thrull'/'FEM', 'Summon — Thrull').
card_original_text('basal thrull'/'FEM', '{T}: Sacrifice Basal Thrull to add {B}{B} to your mana pool. Play this ability as an interrupt.').
card_image_name('basal thrull'/'FEM', 'basal thrull2').
card_uid('basal thrull'/'FEM', 'FEM:Basal Thrull:basal thrull2').
card_rarity('basal thrull'/'FEM', 'Common').
card_artist('basal thrull'/'FEM', 'Phil Foglio').
card_flavor_text('basal thrull'/'FEM', 'Although my later designs were much more sophisticated, Basal Thrulls were no less a success.\"\n—Endrek Sahr, \"Thrulls\"').
card_multiverse_id('basal thrull'/'FEM', '1844').

card_in_set('basal thrull', 'FEM').
card_original_type('basal thrull'/'FEM', 'Summon — Thrull').
card_original_text('basal thrull'/'FEM', '{T}: Sacrifice Basal Thrull to add {B}{B} to your mana pool. Play this ability as an interrupt.').
card_image_name('basal thrull'/'FEM', 'basal thrull3').
card_uid('basal thrull'/'FEM', 'FEM:Basal Thrull:basal thrull3').
card_rarity('basal thrull'/'FEM', 'Common').
card_artist('basal thrull'/'FEM', 'Richard Kane Ferguson').
card_flavor_text('basal thrull'/'FEM', '\"To create the first Thrulls, I only introduced alchemic elements into the Order\'s necromancy; Tourach\'s principles remained unchanged.\"\n—Endrek Sahr, Master Breeder').
card_multiverse_id('basal thrull'/'FEM', '1843').

card_in_set('basal thrull', 'FEM').
card_original_type('basal thrull'/'FEM', 'Summon — Thrull').
card_original_text('basal thrull'/'FEM', '{T}: Sacrifice Basal Thrull to add {B}{B} to your mana pool. Play this ability as an interrupt.').
card_image_name('basal thrull'/'FEM', 'basal thrull4').
card_uid('basal thrull'/'FEM', 'FEM:Basal Thrull:basal thrull4').
card_rarity('basal thrull'/'FEM', 'Common').
card_artist('basal thrull'/'FEM', 'Christopher Rush').
card_flavor_text('basal thrull'/'FEM', '\"Above all, a well crafted Thrull should be hideous—blasted of countenance and twisted of limb—to inspire fear and revulsion.\"\n—Endrek Sahr, Master Breeder').
card_multiverse_id('basal thrull'/'FEM', '1845').

card_in_set('bottomless vault', 'FEM').
card_original_type('bottomless vault'/'FEM', 'Land').
card_original_text('bottomless vault'/'FEM', 'Comes into play tapped. You may choose not to untap Bottomless Vault during your untap phase and instead put a storage counter on it.\n{T}: Remove any number of storage counters from Bottomless Vault. For each storage counter removed, add {B} to your mana pool.').
card_first_print('bottomless vault', 'FEM').
card_image_name('bottomless vault'/'FEM', 'bottomless vault').
card_uid('bottomless vault'/'FEM', 'FEM:Bottomless Vault:bottomless vault').
card_rarity('bottomless vault'/'FEM', 'Rare').
card_artist('bottomless vault'/'FEM', 'Pat Morrissey').
card_multiverse_id('bottomless vault'/'FEM', '2003').

card_in_set('brassclaw orcs', 'FEM').
card_original_type('brassclaw orcs'/'FEM', 'Summon — Orcs').
card_original_text('brassclaw orcs'/'FEM', 'Cannot be assigned to block any creature of power greater than 1.').
card_first_print('brassclaw orcs', 'FEM').
card_image_name('brassclaw orcs'/'FEM', 'brassclaw orcs1').
card_uid('brassclaw orcs'/'FEM', 'FEM:Brassclaw Orcs:brassclaw orcs1').
card_rarity('brassclaw orcs'/'FEM', 'Common').
card_artist('brassclaw orcs'/'FEM', 'Rob Alexander').
card_flavor_text('brassclaw orcs'/'FEM', '\"The Brassclaws delighted in lightning raids on Icatian and Dwarven towns; an unprepared enemy is easier to defeat.\"\n—Sarpadian Empires, vol. IV').
card_multiverse_id('brassclaw orcs'/'FEM', '1966').

card_in_set('brassclaw orcs', 'FEM').
card_original_type('brassclaw orcs'/'FEM', 'Summon — Orcs').
card_original_text('brassclaw orcs'/'FEM', 'Cannot be assigned to block any creature of power greater than 1.').
card_image_name('brassclaw orcs'/'FEM', 'brassclaw orcs2').
card_uid('brassclaw orcs'/'FEM', 'FEM:Brassclaw Orcs:brassclaw orcs2').
card_rarity('brassclaw orcs'/'FEM', 'Common').
card_artist('brassclaw orcs'/'FEM', 'Dan Frazier').
card_flavor_text('brassclaw orcs'/'FEM', '\"Brassclaws were typical Orcs—quick to laud their own prowess in battle, quick to jeer at their opponents, and quicker still to run away when things started to look slightly dangerous.\"\n—Sarpadian Empires, vol. IV').
card_multiverse_id('brassclaw orcs'/'FEM', '1938').

card_in_set('brassclaw orcs', 'FEM').
card_original_type('brassclaw orcs'/'FEM', 'Summon — Orcs').
card_original_text('brassclaw orcs'/'FEM', 'Cannot be assigned to block any creature of power greater than 1.').
card_image_name('brassclaw orcs'/'FEM', 'brassclaw orcs3').
card_uid('brassclaw orcs'/'FEM', 'FEM:Brassclaw Orcs:brassclaw orcs3').
card_rarity('brassclaw orcs'/'FEM', 'Common').
card_artist('brassclaw orcs'/'FEM', 'Rob Alexander').
card_flavor_text('brassclaw orcs'/'FEM', '\"A whole skin is worth a thousand victories.\"\n—Orcish Veteran of the Battle of Montford').
card_multiverse_id('brassclaw orcs'/'FEM', '1937').

card_in_set('brassclaw orcs', 'FEM').
card_original_type('brassclaw orcs'/'FEM', 'Summon — Orcs').
card_original_text('brassclaw orcs'/'FEM', 'Cannot be assigned to block any creature of power greater than 1.').
card_image_name('brassclaw orcs'/'FEM', 'brassclaw orcs4').
card_uid('brassclaw orcs'/'FEM', 'FEM:Brassclaw Orcs:brassclaw orcs4').
card_rarity('brassclaw orcs'/'FEM', 'Common').
card_artist('brassclaw orcs'/'FEM', 'Heather Hudson').
card_flavor_text('brassclaw orcs'/'FEM', '\"The large brass claws worn by some Sarpadian Orc tribes were among the least feared weapons ever known.\"\n—Sarpadian Empires, vol. IV').
card_multiverse_id('brassclaw orcs'/'FEM', '1940').

card_in_set('breeding pit', 'FEM').
card_original_type('breeding pit'/'FEM', 'Enchantment').
card_original_text('breeding pit'/'FEM', 'During your upkeep, pay {B}{B} or bury Breeding Pit. At the end of your turn, put a Thrull token into play. Treat this token as a 0/1 black creature.').
card_first_print('breeding pit', 'FEM').
card_image_name('breeding pit'/'FEM', 'breeding pit').
card_uid('breeding pit'/'FEM', 'FEM:Breeding Pit:breeding pit').
card_rarity('breeding pit'/'FEM', 'Uncommon').
card_artist('breeding pit'/'FEM', 'Anson Maddocks').
card_flavor_text('breeding pit'/'FEM', 'The Thrulls bred at a terrifying pace. In the end, they overwhelmed the Order of the Ebon Hand.').
card_multiverse_id('breeding pit'/'FEM', '1846').

card_in_set('combat medic', 'FEM').
card_original_type('combat medic'/'FEM', 'Summon — Soldier').
card_original_text('combat medic'/'FEM', '{1}{W} Prevent 1 damage to any player or creature.').
card_first_print('combat medic', 'FEM').
card_image_name('combat medic'/'FEM', 'combat medic1').
card_uid('combat medic'/'FEM', 'FEM:Combat Medic:combat medic1').
card_rarity('combat medic'/'FEM', 'Common').
card_artist('combat medic'/'FEM', 'Edward P. Beard, Jr.').
card_flavor_text('combat medic'/'FEM', '\"Although Icatia\'s Combat Medics borrowed much of their knowledge from other societies, their skills were their own.\"\n—Sarpadian Empires, vol. VI').
card_multiverse_id('combat medic'/'FEM', '1971').

card_in_set('combat medic', 'FEM').
card_original_type('combat medic'/'FEM', 'Summon — Soldier').
card_original_text('combat medic'/'FEM', '{1}{W} Prevent 1 damage to any player or creature.').
card_image_name('combat medic'/'FEM', 'combat medic2').
card_uid('combat medic'/'FEM', 'FEM:Combat Medic:combat medic2').
card_rarity('combat medic'/'FEM', 'Common').
card_artist('combat medic'/'FEM', 'Susan Van Camp').
card_flavor_text('combat medic'/'FEM', '\"We\'d no sooner knock \'em back on their heels than that accursed sawbones would show up and patch \'em back together again.\"\n—Ivra Jursdotter').
card_multiverse_id('combat medic'/'FEM', '1972').

card_in_set('combat medic', 'FEM').
card_original_type('combat medic'/'FEM', 'Summon — Soldier').
card_original_text('combat medic'/'FEM', '{1}{W} Prevent 1 damage to any player or creature.').
card_image_name('combat medic'/'FEM', 'combat medic3').
card_uid('combat medic'/'FEM', 'FEM:Combat Medic:combat medic3').
card_rarity('combat medic'/'FEM', 'Common').
card_artist('combat medic'/'FEM', 'Anson Maddocks').
card_flavor_text('combat medic'/'FEM', '\"Without Combat Medics, Icatia would probably not have withstood the forces of chaos as long as it did.\"\n—Sarpadian Empires, vol. VI').
card_multiverse_id('combat medic'/'FEM', '1970').

card_in_set('combat medic', 'FEM').
card_original_type('combat medic'/'FEM', 'Summon — Soldier').
card_original_text('combat medic'/'FEM', '{1}{W} Prevent 1 damage to any player or creature.').
card_image_name('combat medic'/'FEM', 'combat medic4').
card_uid('combat medic'/'FEM', 'FEM:Combat Medic:combat medic4').
card_rarity('combat medic'/'FEM', 'Common').
card_artist('combat medic'/'FEM', 'Liz Danforth').
card_flavor_text('combat medic'/'FEM', '\"Time enough to become a Medic once you\'re hurt too badly to fight. For now, Icatia needs your strength more than your compassion.\"\n—Lydia Wynforth, Mayor of Trokair').
card_multiverse_id('combat medic'/'FEM', '1973').

card_in_set('conch horn', 'FEM').
card_original_type('conch horn'/'FEM', 'Artifact').
card_original_text('conch horn'/'FEM', '{1}, {T}: Sacrifice Conch Horn. Draw two cards, then put any one card from your hand back on top of your library.').
card_first_print('conch horn', 'FEM').
card_image_name('conch horn'/'FEM', 'conch horn').
card_uid('conch horn'/'FEM', 'FEM:Conch Horn:conch horn').
card_rarity('conch horn'/'FEM', 'Rare').
card_artist('conch horn'/'FEM', 'Phil Foglio').
card_flavor_text('conch horn'/'FEM', 'Even the most skilled of modern mages only partially understand the Conch Horn\'s awesome powers.').
card_multiverse_id('conch horn'/'FEM', '1829').

card_in_set('deep spawn', 'FEM').
card_original_type('deep spawn'/'FEM', 'Summon — Homarid').
card_original_text('deep spawn'/'FEM', 'Trample\nDuring your upkeep, take two cards from the top of your library and put them in your graveyard, or destroy Deep Spawn.\n{U} Deep Spawn may not be the target of spells or effects until end of turn and does not untap as normal during your next untap phase. If Deep Spawn is untapped, tap it.').
card_first_print('deep spawn', 'FEM').
card_image_name('deep spawn'/'FEM', 'deep spawn').
card_uid('deep spawn'/'FEM', 'FEM:Deep Spawn:deep spawn').
card_rarity('deep spawn'/'FEM', 'Uncommon').
card_artist('deep spawn'/'FEM', 'Mark Tedin').
card_multiverse_id('deep spawn'/'FEM', '1871').

card_in_set('delif\'s cone', 'FEM').
card_original_type('delif\'s cone'/'FEM', 'Artifact').
card_original_text('delif\'s cone'/'FEM', '{T}: Sacrifice Delif\'s Cone. If target creature you control attacks and is not blocked, you may choose to gain its power in life. If you do so, it deals no damage to opponent this turn.').
card_first_print('delif\'s cone', 'FEM').
card_image_name('delif\'s cone'/'FEM', 'delif\'s cone').
card_uid('delif\'s cone'/'FEM', 'FEM:Delif\'s Cone:delif\'s cone').
card_rarity('delif\'s cone'/'FEM', 'Common').
card_artist('delif\'s cone'/'FEM', 'Mark Tedin').
card_flavor_text('delif\'s cone'/'FEM', '\"Where is it written that beasts must cause pain?\" —Delif, Ponderings').
card_multiverse_id('delif\'s cone'/'FEM', '1830').

card_in_set('delif\'s cube', 'FEM').
card_original_type('delif\'s cube'/'FEM', 'Artifact').
card_original_text('delif\'s cube'/'FEM', '{2}, {T}: If target creature you control attacks and is not blocked, it deals no damage to opponent. Instead, put a cube counter on Delif\'s Cube.\n{2}: Remove a cube counter to regenerate a target creature.').
card_first_print('delif\'s cube', 'FEM').
card_image_name('delif\'s cube'/'FEM', 'delif\'s cube').
card_uid('delif\'s cube'/'FEM', 'FEM:Delif\'s Cube:delif\'s cube').
card_rarity('delif\'s cube'/'FEM', 'Rare').
card_artist('delif\'s cube'/'FEM', 'Mark Tedin').
card_multiverse_id('delif\'s cube'/'FEM', '1831').

card_in_set('derelor', 'FEM').
card_original_type('derelor'/'FEM', 'Summon — Thrull').
card_original_text('derelor'/'FEM', 'Your black spells cost an additional {B} to cast.').
card_first_print('derelor', 'FEM').
card_image_name('derelor'/'FEM', 'derelor').
card_uid('derelor'/'FEM', 'FEM:Derelor:derelor').
card_rarity('derelor'/'FEM', 'Rare').
card_artist('derelor'/'FEM', 'Anson Maddocks').
card_flavor_text('derelor'/'FEM', '\"Strength it has, but at the cost of a continuous supply of energy. Such failure can bear only one result.\"\n—From the execution order for Endrek Sahr, Master Breeder').
card_multiverse_id('derelor'/'FEM', '1847').

card_in_set('draconian cylix', 'FEM').
card_original_type('draconian cylix'/'FEM', 'Artifact').
card_original_text('draconian cylix'/'FEM', '{2}, {T}: Discard a card at random from your hand to regenerate a target creature.').
card_first_print('draconian cylix', 'FEM').
card_image_name('draconian cylix'/'FEM', 'draconian cylix').
card_uid('draconian cylix'/'FEM', 'FEM:Draconian Cylix:draconian cylix').
card_rarity('draconian cylix'/'FEM', 'Rare').
card_artist('draconian cylix'/'FEM', 'Edward P. Beard, Jr.').
card_flavor_text('draconian cylix'/'FEM', '\"There is no gain without sacrifice.\"\n—Icatian proverb').
card_multiverse_id('draconian cylix'/'FEM', '1832').

card_in_set('dwarven armorer', 'FEM').
card_original_type('dwarven armorer'/'FEM', 'Summon — Dwarf').
card_original_text('dwarven armorer'/'FEM', '{R}, {T}: Discard a card from your hand to put either a +0/+1 or a +1/+0 counter on a target creature.').
card_first_print('dwarven armorer', 'FEM').
card_image_name('dwarven armorer'/'FEM', 'dwarven armorer').
card_uid('dwarven armorer'/'FEM', 'FEM:Dwarven Armorer:dwarven armorer').
card_rarity('dwarven armorer'/'FEM', 'Rare').
card_artist('dwarven armorer'/'FEM', 'Bryon Wackwitz').
card_flavor_text('dwarven armorer'/'FEM', '\"The few remaining pieces from this period suggest the Dwarves eventually made weapons and armor out of everything, even children\'s toys.\"\n—Sarpadian Empires, vol. IV').
card_multiverse_id('dwarven armorer'/'FEM', '1941').

card_in_set('dwarven catapult', 'FEM').
card_original_type('dwarven catapult'/'FEM', 'Instant').
card_original_text('dwarven catapult'/'FEM', 'Dwarven Catapult does X damage, divided evenly among all of opponent\'s creatures (round down).').
card_first_print('dwarven catapult', 'FEM').
card_image_name('dwarven catapult'/'FEM', 'dwarven catapult').
card_uid('dwarven catapult'/'FEM', 'FEM:Dwarven Catapult:dwarven catapult').
card_rarity('dwarven catapult'/'FEM', 'Uncommon').
card_artist('dwarven catapult'/'FEM', 'Jeff A. Menges').
card_flavor_text('dwarven catapult'/'FEM', '\"Often greatly outnumbered in battle, Dwarves relied on catapults as one means of damaging a large army.\"\n—Sarpadian Empires, vol. IV').
card_multiverse_id('dwarven catapult'/'FEM', '1942').

card_in_set('dwarven hold', 'FEM').
card_original_type('dwarven hold'/'FEM', 'Land').
card_original_text('dwarven hold'/'FEM', 'Comes into play tapped. You may choose not to untap Dwarven Hold during your untap phase and instead put a storage counter on it.\n{T}: Remove any number of storage counters from Dwarven Hold. For each storage counter removed, add {R} to your mana pool.').
card_first_print('dwarven hold', 'FEM').
card_image_name('dwarven hold'/'FEM', 'dwarven hold').
card_uid('dwarven hold'/'FEM', 'FEM:Dwarven Hold:dwarven hold').
card_rarity('dwarven hold'/'FEM', 'Rare').
card_artist('dwarven hold'/'FEM', 'Pat Morrissey').
card_multiverse_id('dwarven hold'/'FEM', '2004').

card_in_set('dwarven lieutenant', 'FEM').
card_original_type('dwarven lieutenant'/'FEM', 'Summon — Dwarf').
card_original_text('dwarven lieutenant'/'FEM', '{1}{R} Target Dwarf gets +1/+0 until end of turn.').
card_first_print('dwarven lieutenant', 'FEM').
card_image_name('dwarven lieutenant'/'FEM', 'dwarven lieutenant').
card_uid('dwarven lieutenant'/'FEM', 'FEM:Dwarven Lieutenant:dwarven lieutenant').
card_rarity('dwarven lieutenant'/'FEM', 'Uncommon').
card_artist('dwarven lieutenant'/'FEM', 'Jeff A. Menges').
card_flavor_text('dwarven lieutenant'/'FEM', '\"Dwarven officers were tireless in battle, moving up and down the lines to rally their troops and boost morale.\"\n—Sarpadian Empires, vol. IV').
card_multiverse_id('dwarven lieutenant'/'FEM', '1943').

card_in_set('dwarven ruins', 'FEM').
card_original_type('dwarven ruins'/'FEM', 'Land').
card_original_text('dwarven ruins'/'FEM', 'Comes into play tapped.\n{T}: Add {R} to your mana pool.\n{T}: Sacrifice Dwarven Ruins to add {R}{R} to your mana pool.').
card_first_print('dwarven ruins', 'FEM').
card_image_name('dwarven ruins'/'FEM', 'dwarven ruins').
card_uid('dwarven ruins'/'FEM', 'FEM:Dwarven Ruins:dwarven ruins').
card_rarity('dwarven ruins'/'FEM', 'Uncommon').
card_artist('dwarven ruins'/'FEM', 'Mark Poole').
card_multiverse_id('dwarven ruins'/'FEM', '2005').

card_in_set('dwarven soldier', 'FEM').
card_original_type('dwarven soldier'/'FEM', 'Summon — Dwarf').
card_original_text('dwarven soldier'/'FEM', 'If Dwarven Soldier blocks or is blocked by Orcs, it gets +0/+2 until end of turn.').
card_first_print('dwarven soldier', 'FEM').
card_image_name('dwarven soldier'/'FEM', 'dwarven soldier1').
card_uid('dwarven soldier'/'FEM', 'FEM:Dwarven Soldier:dwarven soldier1').
card_rarity('dwarven soldier'/'FEM', 'Common').
card_artist('dwarven soldier'/'FEM', 'Rob Alexander').
card_flavor_text('dwarven soldier'/'FEM', '\"Let no one say we did not fight until the last . . . .\"\n—Headstone fragment from a mass grave found in the Crimson Peaks').
card_multiverse_id('dwarven soldier'/'FEM', '1945').

card_in_set('dwarven soldier', 'FEM').
card_original_type('dwarven soldier'/'FEM', 'Summon — Dwarf').
card_original_text('dwarven soldier'/'FEM', 'If Dwarven Soldier blocks or is blocked by Orcs, it gets +0/+2 until end of turn.').
card_image_name('dwarven soldier'/'FEM', 'dwarven soldier2').
card_uid('dwarven soldier'/'FEM', 'FEM:Dwarven Soldier:dwarven soldier2').
card_rarity('dwarven soldier'/'FEM', 'Common').
card_artist('dwarven soldier'/'FEM', 'Randy Asplund-Faith').
card_flavor_text('dwarven soldier'/'FEM', 'There is a legend among present-day Dwarves that the Dwarves of Sarpadia will one day return to defend Dwarvenkind against a deadly peril.').
card_multiverse_id('dwarven soldier'/'FEM', '1944').

card_in_set('dwarven soldier', 'FEM').
card_original_type('dwarven soldier'/'FEM', 'Summon — Dwarf').
card_original_text('dwarven soldier'/'FEM', 'If Dwarven Soldier blocks or is blocked by Orcs, it gets +0/+2 until end of turn.').
card_image_name('dwarven soldier'/'FEM', 'dwarven soldier3').
card_uid('dwarven soldier'/'FEM', 'FEM:Dwarven Soldier:dwarven soldier3').
card_rarity('dwarven soldier'/'FEM', 'Common').
card_artist('dwarven soldier'/'FEM', 'Douglas Shuler').
card_flavor_text('dwarven soldier'/'FEM', '\"Although the Dwarves staunchly defended their walled city-states against the Orcs, their civilization was the first to fall, and its name was sadly lost.\"\n—Sarpadian Empires, vol. I').
card_multiverse_id('dwarven soldier'/'FEM', '1946').

card_in_set('ebon praetor', 'FEM').
card_original_type('ebon praetor'/'FEM', 'Summon — Avatar').
card_original_text('ebon praetor'/'FEM', 'Trample, first strike\nDuring your upkeep, put a -2/-2 counter on Ebon Praetor.\nYou may sacrifice a creature during your upkeep to remove a -2/-2 counter from Ebon Praetor. If the creature sacrificed was a Thrull, also put a +1/+0 counter on Ebon Praetor. Only one creature may be sacrificed in this manner each turn.').
card_first_print('ebon praetor', 'FEM').
card_image_name('ebon praetor'/'FEM', 'ebon praetor').
card_uid('ebon praetor'/'FEM', 'FEM:Ebon Praetor:ebon praetor').
card_rarity('ebon praetor'/'FEM', 'Rare').
card_artist('ebon praetor'/'FEM', 'Randy Asplund-Faith').
card_multiverse_id('ebon praetor'/'FEM', '1848').

card_in_set('ebon stronghold', 'FEM').
card_original_type('ebon stronghold'/'FEM', 'Land').
card_original_text('ebon stronghold'/'FEM', 'Comes into play tapped.\n{T}: Add {B} to your mana pool.\n{T}: Sacrifice Ebon Stronghold to add {B}{B} to your mana pool.').
card_first_print('ebon stronghold', 'FEM').
card_image_name('ebon stronghold'/'FEM', 'ebon stronghold').
card_uid('ebon stronghold'/'FEM', 'FEM:Ebon Stronghold:ebon stronghold').
card_rarity('ebon stronghold'/'FEM', 'Uncommon').
card_artist('ebon stronghold'/'FEM', 'Mark Poole').
card_multiverse_id('ebon stronghold'/'FEM', '2006').

card_in_set('elven fortress', 'FEM').
card_original_type('elven fortress'/'FEM', 'Enchantment').
card_original_text('elven fortress'/'FEM', '{1}{G} Target blocking creature gets +0/+1 until end of turn.').
card_first_print('elven fortress', 'FEM').
card_image_name('elven fortress'/'FEM', 'elven fortress1').
card_uid('elven fortress'/'FEM', 'FEM:Elven Fortress:elven fortress1').
card_rarity('elven fortress'/'FEM', 'Common').
card_artist('elven fortress'/'FEM', 'Pete Venters').
card_flavor_text('elven fortress'/'FEM', 'Many Elven Fortresses weren\'t built by masons and carpenters, but created from the living forest itself.').
card_multiverse_id('elven fortress'/'FEM', '1905').

card_in_set('elven fortress', 'FEM').
card_original_type('elven fortress'/'FEM', 'Enchantment').
card_original_text('elven fortress'/'FEM', '{1}{G} Target blocking creature gets +0/+1 until end of turn.').
card_image_name('elven fortress'/'FEM', 'elven fortress2').
card_uid('elven fortress'/'FEM', 'FEM:Elven Fortress:elven fortress2').
card_rarity('elven fortress'/'FEM', 'Common').
card_artist('elven fortress'/'FEM', 'Randy Asplund-Faith').
card_flavor_text('elven fortress'/'FEM', '\"The size of the obvious Fortress walls often misled foes. Actually, the Elves enchanted the forest itself to provide the first line of defense with tangling vines and stinging thorns.\"\n—Sarpadian Empires, vol. III').
card_multiverse_id('elven fortress'/'FEM', '1904').

card_in_set('elven fortress', 'FEM').
card_original_type('elven fortress'/'FEM', 'Enchantment').
card_original_text('elven fortress'/'FEM', '{1}{G} Target blocking creature gets +0/+1 until end of turn.').
card_image_name('elven fortress'/'FEM', 'elven fortress3').
card_uid('elven fortress'/'FEM', 'FEM:Elven Fortress:elven fortress3').
card_rarity('elven fortress'/'FEM', 'Common').
card_artist('elven fortress'/'FEM', 'Mark Poole').
card_flavor_text('elven fortress'/'FEM', '\"Thallids are not ordinary enemies of flesh and bone and reason. They attack with no thought for our strength or of their own losses. I fear our Fortresses shall be overwhelmed.\"\n—Kyliki of Havenwood').
card_multiverse_id('elven fortress'/'FEM', '1906').

card_in_set('elven fortress', 'FEM').
card_original_type('elven fortress'/'FEM', 'Enchantment').
card_original_text('elven fortress'/'FEM', '{1}{G} Target blocking creature gets +0/+1 until end of turn.').
card_image_name('elven fortress'/'FEM', 'elven fortress4').
card_uid('elven fortress'/'FEM', 'FEM:Elven Fortress:elven fortress4').
card_rarity('elven fortress'/'FEM', 'Common').
card_artist('elven fortress'/'FEM', 'Tom Wänerstrand').
card_flavor_text('elven fortress'/'FEM', '\"Burn it down, you say? Now there\'s a stupid idea. What do you suppose will become of those overhanging branches, hmmm? And then what do you suppose will become of us?\" —Ivra Jursdotter').
card_multiverse_id('elven fortress'/'FEM', '1907').

card_in_set('elven lyre', 'FEM').
card_original_type('elven lyre'/'FEM', 'Artifact').
card_original_text('elven lyre'/'FEM', '{1}, {T}: Sacrifice Elven Lyre to give a target creature +2/+2 until end of turn.').
card_first_print('elven lyre', 'FEM').
card_image_name('elven lyre'/'FEM', 'elven lyre').
card_uid('elven lyre'/'FEM', 'FEM:Elven Lyre:elven lyre').
card_rarity('elven lyre'/'FEM', 'Rare').
card_artist('elven lyre'/'FEM', 'Kaja Foglio').
card_flavor_text('elven lyre'/'FEM', 'Scholars are uncertain whether it was the actual sound or some other magical property of the Elven Lyre that transformed its player.').
card_multiverse_id('elven lyre'/'FEM', '1833').

card_in_set('elvish farmer', 'FEM').
card_original_type('elvish farmer'/'FEM', 'Summon — Elf').
card_original_text('elvish farmer'/'FEM', 'During your upkeep, put a spore counter on Elvish Farmer.\n{0}: Remove three spore counters from Elvish Farmer to put a Saproling token into play. Treat this token as a 1/1 green creature.\n{0}: Sacrifice a Saproling to gain 2 life.').
card_first_print('elvish farmer', 'FEM').
card_image_name('elvish farmer'/'FEM', 'elvish farmer').
card_uid('elvish farmer'/'FEM', 'FEM:Elvish Farmer:elvish farmer').
card_rarity('elvish farmer'/'FEM', 'Rare').
card_artist('elvish farmer'/'FEM', 'Richard Kane Ferguson').
card_multiverse_id('elvish farmer'/'FEM', '1908').

card_in_set('elvish hunter', 'FEM').
card_original_type('elvish hunter'/'FEM', 'Summon — Elf').
card_original_text('elvish hunter'/'FEM', '{1}{G}, {T}: Target creature does not untap as normal during its controller\'s next untap phase.').
card_first_print('elvish hunter', 'FEM').
card_image_name('elvish hunter'/'FEM', 'elvish hunter1').
card_uid('elvish hunter'/'FEM', 'FEM:Elvish Hunter:elvish hunter1').
card_rarity('elvish hunter'/'FEM', 'Common').
card_artist('elvish hunter'/'FEM', 'Mark Poole').
card_flavor_text('elvish hunter'/'FEM', '\"Elves often tipped their arrows with a drug that caused a deep but harmless sleep.\"\n—Sarpadian Empires, vol. III').
card_multiverse_id('elvish hunter'/'FEM', '1910').

card_in_set('elvish hunter', 'FEM').
card_original_type('elvish hunter'/'FEM', 'Summon — Elf').
card_original_text('elvish hunter'/'FEM', '{1}{G}, {T}: Target creature does not untap as normal during its controller\'s next untap phase.').
card_image_name('elvish hunter'/'FEM', 'elvish hunter2').
card_uid('elvish hunter'/'FEM', 'FEM:Elvish Hunter:elvish hunter2').
card_rarity('elvish hunter'/'FEM', 'Common').
card_artist('elvish hunter'/'FEM', 'Anson Maddocks').
card_flavor_text('elvish hunter'/'FEM', '\"As the climate cooled, many Elves turned to Thallid farming for food, while the Hunters honed their skills on what little game remained.\"\n—Sarpadian Empires, vol. III').
card_multiverse_id('elvish hunter'/'FEM', '1911').

card_in_set('elvish hunter', 'FEM').
card_original_type('elvish hunter'/'FEM', 'Summon — Elf').
card_original_text('elvish hunter'/'FEM', '{1}{G}, {T}: Target creature does not untap as normal during its controller\'s next untap phase.').
card_image_name('elvish hunter'/'FEM', 'elvish hunter3').
card_uid('elvish hunter'/'FEM', 'FEM:Elvish Hunter:elvish hunter3').
card_rarity('elvish hunter'/'FEM', 'Common').
card_artist('elvish hunter'/'FEM', 'Susan Van Camp').
card_flavor_text('elvish hunter'/'FEM', 'The Elves never had a standing army; when battle came, they pressed Hunters, Scouts, and Farmers into a surprisingly effective militia.').
card_multiverse_id('elvish hunter'/'FEM', '1909').

card_in_set('elvish scout', 'FEM').
card_original_type('elvish scout'/'FEM', 'Summon — Elf').
card_original_text('elvish scout'/'FEM', '{G}, {T}: Untap a target attacking creature you control. That creature neither receives nor deals damage during combat this turn.').
card_first_print('elvish scout', 'FEM').
card_image_name('elvish scout'/'FEM', 'elvish scout1').
card_uid('elvish scout'/'FEM', 'FEM:Elvish Scout:elvish scout1').
card_rarity('elvish scout'/'FEM', 'Common').
card_artist('elvish scout'/'FEM', 'Mark Poole').
card_flavor_text('elvish scout'/'FEM', 'Even one whose ears were closely tuned to the sounds of Havenwood could miss hearing a Scout move past.').
card_multiverse_id('elvish scout'/'FEM', '1913').

card_in_set('elvish scout', 'FEM').
card_original_type('elvish scout'/'FEM', 'Summon — Elf').
card_original_text('elvish scout'/'FEM', '{G}, {T}: Untap a target attacking creature you control. That creature neither receives nor deals damage during combat this turn.').
card_image_name('elvish scout'/'FEM', 'elvish scout2').
card_uid('elvish scout'/'FEM', 'FEM:Elvish Scout:elvish scout2').
card_rarity('elvish scout'/'FEM', 'Common').
card_artist('elvish scout'/'FEM', 'Pete Venters').
card_flavor_text('elvish scout'/'FEM', '\"Even for Elves, they were stealthy little twerps. They\'d taken our measure before we\'d even seen them.\"\n—Marshall Volnikov').
card_multiverse_id('elvish scout'/'FEM', '1912').

card_in_set('elvish scout', 'FEM').
card_original_type('elvish scout'/'FEM', 'Summon — Elf').
card_original_text('elvish scout'/'FEM', '{G}, {T}: Untap a target attacking creature you control. That creature neither receives nor deals damage during combat this turn.').
card_image_name('elvish scout'/'FEM', 'elvish scout3').
card_uid('elvish scout'/'FEM', 'FEM:Elvish Scout:elvish scout3').
card_rarity('elvish scout'/'FEM', 'Common').
card_artist('elvish scout'/'FEM', 'Christopher Rush').
card_flavor_text('elvish scout'/'FEM', 'Although the Elves of Havenwood lived in isolated villages, their swift communications allowed them to act as a single community.').
card_multiverse_id('elvish scout'/'FEM', '1914').

card_in_set('farrel\'s mantle', 'FEM').
card_original_type('farrel\'s mantle'/'FEM', 'Enchant Creature').
card_original_text('farrel\'s mantle'/'FEM', 'If target creature attacks and is not blocked, it may deal X+2 damage to any other target creature, where X is the power of the creature Farrel\'s Mantle enchants. If it does so, it deals no damage to opponent this turn.').
card_first_print('farrel\'s mantle', 'FEM').
card_image_name('farrel\'s mantle'/'FEM', 'farrel\'s mantle').
card_uid('farrel\'s mantle'/'FEM', 'FEM:Farrel\'s Mantle:farrel\'s mantle').
card_rarity('farrel\'s mantle'/'FEM', 'Uncommon').
card_artist('farrel\'s mantle'/'FEM', 'Anthony Waters').
card_multiverse_id('farrel\'s mantle'/'FEM', '1974').

card_in_set('farrel\'s zealot', 'FEM').
card_original_type('farrel\'s zealot'/'FEM', 'Summon — Townsfolk').
card_original_text('farrel\'s zealot'/'FEM', 'If Farrel\'s Zealot attacks and is not blocked, you may choose to have it deal 3 damage to a target creature. If you do so, it deals no damage to opponent this turn.').
card_first_print('farrel\'s zealot', 'FEM').
card_image_name('farrel\'s zealot'/'FEM', 'farrel\'s zealot1').
card_uid('farrel\'s zealot'/'FEM', 'FEM:Farrel\'s Zealot:farrel\'s zealot1').
card_rarity('farrel\'s zealot'/'FEM', 'Common').
card_artist('farrel\'s zealot'/'FEM', 'Melissa A. Benson').
card_flavor_text('farrel\'s zealot'/'FEM', 'After the fall of Trokair, Farrel and his followers formally broke their ties with the rest of Icatia.').
card_multiverse_id('farrel\'s zealot'/'FEM', '1977').

card_in_set('farrel\'s zealot', 'FEM').
card_original_type('farrel\'s zealot'/'FEM', 'Summon — Townsfolk').
card_original_text('farrel\'s zealot'/'FEM', 'If Farrel\'s Zealot attacks and is not blocked, you may choose to have it deal 3 damage to a target creature. If you do so, it deals no damage to opponent this turn.').
card_image_name('farrel\'s zealot'/'FEM', 'farrel\'s zealot2').
card_uid('farrel\'s zealot'/'FEM', 'FEM:Farrel\'s Zealot:farrel\'s zealot2').
card_rarity('farrel\'s zealot'/'FEM', 'Common').
card_artist('farrel\'s zealot'/'FEM', 'Richard Kane Ferguson').
card_flavor_text('farrel\'s zealot'/'FEM', 'Farrel and his followers became a formidable band of vigilantes, battling Icatians and followers of Tourach.').
card_multiverse_id('farrel\'s zealot'/'FEM', '1976').

card_in_set('farrel\'s zealot', 'FEM').
card_original_type('farrel\'s zealot'/'FEM', 'Summon — Townsfolk').
card_original_text('farrel\'s zealot'/'FEM', 'If Farrel\'s Zealot attacks and is not blocked, you may choose to have it deal 3 damage to a target creature. If you do so, it deals no damage to opponent this turn.').
card_image_name('farrel\'s zealot'/'FEM', 'farrel\'s zealot3').
card_uid('farrel\'s zealot'/'FEM', 'FEM:Farrel\'s Zealot:farrel\'s zealot3').
card_rarity('farrel\'s zealot'/'FEM', 'Common').
card_artist('farrel\'s zealot'/'FEM', 'Edward P. Beard, Jr.').
card_flavor_text('farrel\'s zealot'/'FEM', 'Farrel, a former priest, believed Icatia was far too complacent toward the Order of the Ebon Hand.').
card_multiverse_id('farrel\'s zealot'/'FEM', '1975').

card_in_set('farrelite priest', 'FEM').
card_original_type('farrelite priest'/'FEM', 'Summon — Cleric').
card_original_text('farrelite priest'/'FEM', '{1}: Add {W} to your mana pool. Play this ability as an interrupt. If more than 3 is spent in this way during one turn, bury Farrelite Priest at end of turn.').
card_first_print('farrelite priest', 'FEM').
card_image_name('farrelite priest'/'FEM', 'farrelite priest').
card_uid('farrelite priest'/'FEM', 'FEM:Farrelite Priest:farrelite priest').
card_rarity('farrelite priest'/'FEM', 'Uncommon').
card_artist('farrelite priest'/'FEM', 'Phil Foglio').
card_flavor_text('farrelite priest'/'FEM', 'Although their methods were often brutal, Farrel\'s followers believed in the preservation of justice and virtue.').
card_multiverse_id('farrelite priest'/'FEM', '1978').

card_in_set('feral thallid', 'FEM').
card_original_type('feral thallid'/'FEM', 'Summon — Fungus').
card_original_text('feral thallid'/'FEM', 'During your upkeep, put a spore counter on Feral Thallid.\n{0}: Remove three spore counters from Feral Thallid to regenerate it.').
card_first_print('feral thallid', 'FEM').
card_image_name('feral thallid'/'FEM', 'feral thallid').
card_uid('feral thallid'/'FEM', 'FEM:Feral Thallid:feral thallid').
card_rarity('feral thallid'/'FEM', 'Uncommon').
card_artist('feral thallid'/'FEM', 'Rob Alexander').
card_flavor_text('feral thallid'/'FEM', '\"Born and bred of fungus, Thallids were nearly impossible to kill.\"\n—Sarpadian Empires, vol. I').
card_multiverse_id('feral thallid'/'FEM', '1915').

card_in_set('fungal bloom', 'FEM').
card_original_type('fungal bloom'/'FEM', 'Enchantment').
card_original_text('fungal bloom'/'FEM', '{G}{G} Put a spore counter on a target Fungus.').
card_first_print('fungal bloom', 'FEM').
card_image_name('fungal bloom'/'FEM', 'fungal bloom').
card_uid('fungal bloom'/'FEM', 'FEM:Fungal Bloom:fungal bloom').
card_rarity('fungal bloom'/'FEM', 'Rare').
card_artist('fungal bloom'/'FEM', 'Daniel Gelon').
card_flavor_text('fungal bloom'/'FEM', '\"Thallids could absorb energy from the forest itself. Even Elves were at a disadvantage in fighting them.\"\n—Sarpadian Empires, vol. III').
card_multiverse_id('fungal bloom'/'FEM', '1916').

card_in_set('goblin chirurgeon', 'FEM').
card_original_type('goblin chirurgeon'/'FEM', 'Summon — Goblin').
card_original_text('goblin chirurgeon'/'FEM', '{0}: Sacrifice a Goblin to regenerate a target creature.').
card_first_print('goblin chirurgeon', 'FEM').
card_image_name('goblin chirurgeon'/'FEM', 'goblin chirurgeon1').
card_uid('goblin chirurgeon'/'FEM', 'FEM:Goblin Chirurgeon:goblin chirurgeon1').
card_rarity('goblin chirurgeon'/'FEM', 'Common').
card_artist('goblin chirurgeon'/'FEM', 'Daniel Gelon').
card_flavor_text('goblin chirurgeon'/'FEM', 'The Chirurgeons patched up their fallen comrades with a gruesome mix of twisted limbs and mangled flesh.').
card_multiverse_id('goblin chirurgeon'/'FEM', '1948').

card_in_set('goblin chirurgeon', 'FEM').
card_original_type('goblin chirurgeon'/'FEM', 'Summon — Goblin').
card_original_text('goblin chirurgeon'/'FEM', '{0}: Sacrifice a Goblin to regenerate a target creature.').
card_image_name('goblin chirurgeon'/'FEM', 'goblin chirurgeon2').
card_uid('goblin chirurgeon'/'FEM', 'FEM:Goblin Chirurgeon:goblin chirurgeon2').
card_rarity('goblin chirurgeon'/'FEM', 'Common').
card_artist('goblin chirurgeon'/'FEM', 'Phil Foglio').
card_flavor_text('goblin chirurgeon'/'FEM', '\"I asked one of my aides how they do it, but all he\'d say was, ‘Trust me, Mayor, you don\'t want to know.\'\"\n—Lydia Wynforth, Mayor of Trokair').
card_multiverse_id('goblin chirurgeon'/'FEM', '1949').

card_in_set('goblin chirurgeon', 'FEM').
card_original_type('goblin chirurgeon'/'FEM', 'Summon — Goblin').
card_original_text('goblin chirurgeon'/'FEM', '{0}: Sacrifice a Goblin to regenerate a target creature.').
card_image_name('goblin chirurgeon'/'FEM', 'goblin chirurgeon3').
card_uid('goblin chirurgeon'/'FEM', 'FEM:Goblin Chirurgeon:goblin chirurgeon3').
card_rarity('goblin chirurgeon'/'FEM', 'Common').
card_artist('goblin chirurgeon'/'FEM', 'Dan Frazier').
card_flavor_text('goblin chirurgeon'/'FEM', '\"Perhaps Goblins are good for something after all.\"\n—Attributed to General Khurzog').
card_multiverse_id('goblin chirurgeon'/'FEM', '1947').

card_in_set('goblin flotilla', 'FEM').
card_original_type('goblin flotilla'/'FEM', 'Summon — Goblins').
card_original_text('goblin flotilla'/'FEM', 'Islandwalk\nAt the beginning of the attack, pay {R} or any creatures blocking or blocked by Goblin Flotilla gain first strike until end of turn.').
card_first_print('goblin flotilla', 'FEM').
card_image_name('goblin flotilla'/'FEM', 'goblin flotilla').
card_uid('goblin flotilla'/'FEM', 'FEM:Goblin Flotilla:goblin flotilla').
card_rarity('goblin flotilla'/'FEM', 'Rare').
card_artist('goblin flotilla'/'FEM', 'Tom Wänerstrand').
card_flavor_text('goblin flotilla'/'FEM', 'Exceptionally poor sailors, Goblins usually arrived at their destination retching and in no condition to fight.').
card_multiverse_id('goblin flotilla'/'FEM', '1950').

card_in_set('goblin grenade', 'FEM').
card_original_type('goblin grenade'/'FEM', 'Sorcery').
card_original_text('goblin grenade'/'FEM', 'Sacrifice a Goblin to have Goblin Grenade deal 5 damage to one target.').
card_first_print('goblin grenade', 'FEM').
card_image_name('goblin grenade'/'FEM', 'goblin grenade1').
card_uid('goblin grenade'/'FEM', 'FEM:Goblin Grenade:goblin grenade1').
card_rarity('goblin grenade'/'FEM', 'Common').
card_artist('goblin grenade'/'FEM', 'Ron Spencer').
card_flavor_text('goblin grenade'/'FEM', '\"According to accepted theory, the Grenade held some kind of flammable mixture and was carried to its target by a hapless Goblin.\"\n—Sarpadian Empires, vol. IV').
card_multiverse_id('goblin grenade'/'FEM', '1951').

card_in_set('goblin grenade', 'FEM').
card_original_type('goblin grenade'/'FEM', 'Sorcery').
card_original_text('goblin grenade'/'FEM', 'Sacrifice a Goblin to have Goblin Grenade deal 5 damage to one target.').
card_image_name('goblin grenade'/'FEM', 'goblin grenade2').
card_uid('goblin grenade'/'FEM', 'FEM:Goblin Grenade:goblin grenade2').
card_rarity('goblin grenade'/'FEM', 'Common').
card_artist('goblin grenade'/'FEM', 'Dan Frazier').
card_flavor_text('goblin grenade'/'FEM', '\"I don\'t suppose we could teach them to throw the cursed things?\"\n—Ivra Jursdotter').
card_multiverse_id('goblin grenade'/'FEM', '1952').

card_in_set('goblin grenade', 'FEM').
card_original_type('goblin grenade'/'FEM', 'Sorcery').
card_original_text('goblin grenade'/'FEM', 'Sacrifice a Goblin to have Goblin Grenade deal 5 damage to one target.').
card_image_name('goblin grenade'/'FEM', 'goblin grenade3').
card_uid('goblin grenade'/'FEM', 'FEM:Goblin Grenade:goblin grenade3').
card_rarity('goblin grenade'/'FEM', 'Common').
card_artist('goblin grenade'/'FEM', 'Christopher Rush').
card_flavor_text('goblin grenade'/'FEM', '\"Without their massive numbers, the Goblins could never have launched such a successful offensive.\"\n—Sarpadian Empires, vol. VI').
card_multiverse_id('goblin grenade'/'FEM', '1953').

card_in_set('goblin kites', 'FEM').
card_original_type('goblin kites'/'FEM', 'Enchantment').
card_original_text('goblin kites'/'FEM', '{R} A target creature you control, which cannot have a toughness greater than 2, gains flying until end of turn. Other effects may later be used to increase the creature\'s toughness. At end of turn, flip a coin; opponent calls heads or tails while coin is in the air. If the flip ends up in opponent\'s favor, bury that creature.').
card_first_print('goblin kites', 'FEM').
card_image_name('goblin kites'/'FEM', 'goblin kites').
card_uid('goblin kites'/'FEM', 'FEM:Goblin Kites:goblin kites').
card_rarity('goblin kites'/'FEM', 'Uncommon').
card_artist('goblin kites'/'FEM', 'Anson Maddocks').
card_multiverse_id('goblin kites'/'FEM', '1954').

card_in_set('goblin war drums', 'FEM').
card_original_type('goblin war drums'/'FEM', 'Enchantment').
card_original_text('goblin war drums'/'FEM', 'Each attacking creature you control that opponent chooses to block may not be blocked with fewer than two creatures.').
card_first_print('goblin war drums', 'FEM').
card_image_name('goblin war drums'/'FEM', 'goblin war drums1').
card_uid('goblin war drums'/'FEM', 'FEM:Goblin War Drums:goblin war drums1').
card_rarity('goblin war drums'/'FEM', 'Common').
card_artist('goblin war drums'/'FEM', 'Dan Frazier').
card_flavor_text('goblin war drums'/'FEM', '\"The Goblins\' dreaded War Drums struck terror into the hearts of even their bravest foes.\"\n—Sarpadian Empires, vol. IV').
card_multiverse_id('goblin war drums'/'FEM', '1955').

card_in_set('goblin war drums', 'FEM').
card_original_type('goblin war drums'/'FEM', 'Enchantment').
card_original_text('goblin war drums'/'FEM', 'Each attacking creature you control that opponent chooses to block may not be blocked with fewer than two creatures.').
card_image_name('goblin war drums'/'FEM', 'goblin war drums2').
card_uid('goblin war drums'/'FEM', 'FEM:Goblin War Drums:goblin war drums2').
card_rarity('goblin war drums'/'FEM', 'Common').
card_artist('goblin war drums'/'FEM', 'Richard Kane Ferguson').
card_flavor_text('goblin war drums'/'FEM', '\"Defending the outer trenches, we strained our eyes in the darkness. All we could hear was the Drums\' terrible pounding. Dobbs, on my right, broke first. Soon, we were all heading for the walls.\"\n—Corporal Peter Douglas, courtmartial testimony').
card_multiverse_id('goblin war drums'/'FEM', '1957').

card_in_set('goblin war drums', 'FEM').
card_original_type('goblin war drums'/'FEM', 'Enchantment').
card_original_text('goblin war drums'/'FEM', 'Each attacking creature you control that opponent chooses to block may not be blocked with fewer than two creatures.').
card_image_name('goblin war drums'/'FEM', 'goblin war drums3').
card_uid('goblin war drums'/'FEM', 'FEM:Goblin War Drums:goblin war drums3').
card_rarity('goblin war drums'/'FEM', 'Common').
card_artist('goblin war drums'/'FEM', 'Heather Hudson').
card_flavor_text('goblin war drums'/'FEM', 'The War Drums enabled Goblin and Orcish armies to crush the sparsely defended Dwarven cities.').
card_multiverse_id('goblin war drums'/'FEM', '1956').

card_in_set('goblin war drums', 'FEM').
card_original_type('goblin war drums'/'FEM', 'Enchantment').
card_original_text('goblin war drums'/'FEM', 'Each attacking creature you control that opponent chooses to block may not be blocked with fewer than two creatures.').
card_image_name('goblin war drums'/'FEM', 'goblin war drums4').
card_uid('goblin war drums'/'FEM', 'FEM:Goblin War Drums:goblin war drums4').
card_rarity('goblin war drums'/'FEM', 'Common').
card_artist('goblin war drums'/'FEM', 'Jeff A. Menges').
card_flavor_text('goblin war drums'/'FEM', 'When creating a new War Drum, Goblin designers sought out the highest quality skulls. Serra Angel skulls were most highly prized, although most acknowledged that the skull of the exotic Sea Serpent made for a richer tone.').
card_multiverse_id('goblin war drums'/'FEM', '1958').

card_in_set('goblin warrens', 'FEM').
card_original_type('goblin warrens'/'FEM', 'Enchantment').
card_original_text('goblin warrens'/'FEM', '{2}{R} Sacrifice two Goblins to put three Goblin tokens into play. Treat these tokens as 1/1 red creatures.').
card_first_print('goblin warrens', 'FEM').
card_image_name('goblin warrens'/'FEM', 'goblin warrens').
card_uid('goblin warrens'/'FEM', 'FEM:Goblin Warrens:goblin warrens').
card_rarity('goblin warrens'/'FEM', 'Rare').
card_artist('goblin warrens'/'FEM', 'Dan Frazier').
card_flavor_text('goblin warrens'/'FEM', '\"Goblins bred underground, their numbers hidden from the enemy until it was too late.\"\n—Sarpadian Empires, vol. IV').
card_multiverse_id('goblin warrens'/'FEM', '1959').

card_in_set('hand of justice', 'FEM').
card_original_type('hand of justice'/'FEM', 'Summon — Avatar').
card_original_text('hand of justice'/'FEM', '{T}: Tap three target white creatures you control to destroy any target creature.').
card_first_print('hand of justice', 'FEM').
card_image_name('hand of justice'/'FEM', 'hand of justice').
card_uid('hand of justice'/'FEM', 'FEM:Hand of Justice:hand of justice').
card_rarity('hand of justice'/'FEM', 'Rare').
card_artist('hand of justice'/'FEM', 'Melissa A. Benson').
card_flavor_text('hand of justice'/'FEM', '\"The Hand of Justice will come to cleanse the world if we are true.\"\n—Oliver Farrel').
card_multiverse_id('hand of justice'/'FEM', '1979').

card_in_set('havenwood battleground', 'FEM').
card_original_type('havenwood battleground'/'FEM', 'Land').
card_original_text('havenwood battleground'/'FEM', 'Comes into play tapped.\n{T}: Add {G} to your mana pool.\n{T}: Sacrifice Havenwood Battleground to add {G}{G} to your mana pool.').
card_first_print('havenwood battleground', 'FEM').
card_image_name('havenwood battleground'/'FEM', 'havenwood battleground').
card_uid('havenwood battleground'/'FEM', 'FEM:Havenwood Battleground:havenwood battleground').
card_rarity('havenwood battleground'/'FEM', 'Uncommon').
card_artist('havenwood battleground'/'FEM', 'Mark Poole').
card_multiverse_id('havenwood battleground'/'FEM', '2007').

card_in_set('heroism', 'FEM').
card_original_type('heroism'/'FEM', 'Enchantment').
card_original_text('heroism'/'FEM', '{0}: Sacrifice a white creature to have attacking red creatures deal no damage during combat this turn. The attacking player may pay {2}{R} for an attacking creature to have it deal damage as normal.').
card_first_print('heroism', 'FEM').
card_image_name('heroism'/'FEM', 'heroism').
card_uid('heroism'/'FEM', 'FEM:Heroism:heroism').
card_rarity('heroism'/'FEM', 'Uncommon').
card_artist('heroism'/'FEM', 'Mark Poole').
card_multiverse_id('heroism'/'FEM', '1980').

card_in_set('high tide', 'FEM').
card_original_type('high tide'/'FEM', 'Instant').
card_original_text('high tide'/'FEM', 'Until end of turn, all islands produce an additional {U} when tapped for mana.').
card_first_print('high tide', 'FEM').
card_image_name('high tide'/'FEM', 'high tide1').
card_uid('high tide'/'FEM', 'FEM:High Tide:high tide1').
card_rarity('high tide'/'FEM', 'Common').
card_artist('high tide'/'FEM', 'Drew Tucker').
card_flavor_text('high tide'/'FEM', '\"When the very tides turn against you, it\'s time to consider retirement.\"\n—General Khurzog').
card_multiverse_id('high tide'/'FEM', '1873').

card_in_set('high tide', 'FEM').
card_original_type('high tide'/'FEM', 'Instant').
card_original_text('high tide'/'FEM', 'Until end of turn, all islands produce an additional {U} when tapped for mana.').
card_image_name('high tide'/'FEM', 'high tide2').
card_uid('high tide'/'FEM', 'FEM:High Tide:high tide2').
card_rarity('high tide'/'FEM', 'Common').
card_artist('high tide'/'FEM', 'Anson Maddocks').
card_flavor_text('high tide'/'FEM', '\"May Svyelun and her tides favor you.\"\n—Traditional Merfolk blessing').
card_multiverse_id('high tide'/'FEM', '1872').

card_in_set('high tide', 'FEM').
card_original_type('high tide'/'FEM', 'Instant').
card_original_text('high tide'/'FEM', 'Until end of turn, all islands produce an additional {U} when tapped for mana.').
card_image_name('high tide'/'FEM', 'high tide3').
card_uid('high tide'/'FEM', 'FEM:High Tide:high tide3').
card_rarity('high tide'/'FEM', 'Common').
card_artist('high tide'/'FEM', 'Amy Weber').
card_flavor_text('high tide'/'FEM', '\"By the time their enemies noticed the sea\'s changing mood, the Vodalians had often shifted formation and were ready to attack.\"\n—Sarpadian Empires, vol. V').
card_multiverse_id('high tide'/'FEM', '1874').

card_in_set('hollow trees', 'FEM').
card_original_type('hollow trees'/'FEM', 'Land').
card_original_text('hollow trees'/'FEM', 'Comes into play tapped. You may choose not to untap Hollow Trees during your untap phase and instead put a storage counter on it.\n{T}: Remove any number of storage counters from Hollow Trees. For each storage counter removed, add {G} to your mana pool.').
card_first_print('hollow trees', 'FEM').
card_image_name('hollow trees'/'FEM', 'hollow trees').
card_uid('hollow trees'/'FEM', 'FEM:Hollow Trees:hollow trees').
card_rarity('hollow trees'/'FEM', 'Rare').
card_artist('hollow trees'/'FEM', 'Pat Morrissey').
card_multiverse_id('hollow trees'/'FEM', '2008').

card_in_set('homarid', 'FEM').
card_original_type('homarid'/'FEM', 'Summon — Homarid').
card_original_text('homarid'/'FEM', 'Put a tide counter on Homarid when it is brought into play and during your upkeep. If there is one tide counter on Homarid, it gets -1/-1. If there are three tide counters on Homarid, it gets +1/+1. When there are four tide counters on Homarid, remove them all.').
card_first_print('homarid', 'FEM').
card_image_name('homarid'/'FEM', 'homarid1').
card_uid('homarid'/'FEM', 'FEM:Homarid:homarid1').
card_rarity('homarid'/'FEM', 'Common').
card_artist('homarid'/'FEM', 'Quinton Hoover').
card_multiverse_id('homarid'/'FEM', '1875').

card_in_set('homarid', 'FEM').
card_original_type('homarid'/'FEM', 'Summon — Homarid').
card_original_text('homarid'/'FEM', 'Put a tide counter on Homarid when it is brought into play and during your upkeep. If there is one tide counter on Homarid, it gets -1/-1. If there are three tide counters on Homarid, it gets +1/+1. When there are four tide counters on Homarid, remove them all.').
card_image_name('homarid'/'FEM', 'homarid2').
card_uid('homarid'/'FEM', 'FEM:Homarid:homarid2').
card_rarity('homarid'/'FEM', 'Common').
card_artist('homarid'/'FEM', 'Heather Hudson').
card_multiverse_id('homarid'/'FEM', '1876').

card_in_set('homarid', 'FEM').
card_original_type('homarid'/'FEM', 'Summon — Homarid').
card_original_text('homarid'/'FEM', 'Put a tide counter on Homarid when it is brought into play and during your upkeep. If there is one tide counter on Homarid, it gets -1/-1. If there are three tide counters on Homarid, it gets +1/+1. When there are four tide counters on Homarid, remove them all.').
card_image_name('homarid'/'FEM', 'homarid3').
card_uid('homarid'/'FEM', 'FEM:Homarid:homarid3').
card_rarity('homarid'/'FEM', 'Common').
card_artist('homarid'/'FEM', 'Mark Tedin').
card_multiverse_id('homarid'/'FEM', '1877').

card_in_set('homarid', 'FEM').
card_original_type('homarid'/'FEM', 'Summon — Homarid').
card_original_text('homarid'/'FEM', 'Put a tide counter on Homarid when it is brought into play and during your upkeep. If there is one tide counter on Homarid, it gets -1/-1. If there are three tide counters on Homarid, it gets +1/+1. When there are four tide counters on Homarid, remove them all.').
card_image_name('homarid'/'FEM', 'homarid4').
card_uid('homarid'/'FEM', 'FEM:Homarid:homarid4').
card_rarity('homarid'/'FEM', 'Common').
card_artist('homarid'/'FEM', 'Bryon Wackwitz').
card_multiverse_id('homarid'/'FEM', '1878').

card_in_set('homarid shaman', 'FEM').
card_original_type('homarid shaman'/'FEM', 'Summon — Homarid').
card_original_text('homarid shaman'/'FEM', '{U} Tap a target green creature.').
card_first_print('homarid shaman', 'FEM').
card_image_name('homarid shaman'/'FEM', 'homarid shaman').
card_uid('homarid shaman'/'FEM', 'FEM:Homarid Shaman:homarid shaman').
card_rarity('homarid shaman'/'FEM', 'Rare').
card_artist('homarid shaman'/'FEM', 'Amy Weber').
card_flavor_text('homarid shaman'/'FEM', '\"The ground grew swampy; hooves and claws sank into the marshy earth. Snarls of rage and bleats of despair echoed through the trees as the waters grew higher and higher.\"\n—Kyliki of Havenwood,\n\"Havenwood Remembered\"').
card_multiverse_id('homarid shaman'/'FEM', '1879').

card_in_set('homarid spawning bed', 'FEM').
card_original_type('homarid spawning bed'/'FEM', 'Enchantment').
card_original_text('homarid spawning bed'/'FEM', '{1}{U}{U} Sacrifice a blue creature to put X Camarid tokens into play, where X is the casting cost of the sacrificed creature. Treat these tokens as 1/1 blue creatures.').
card_first_print('homarid spawning bed', 'FEM').
card_image_name('homarid spawning bed'/'FEM', 'homarid spawning bed').
card_uid('homarid spawning bed'/'FEM', 'FEM:Homarid Spawning Bed:homarid spawning bed').
card_rarity('homarid spawning bed'/'FEM', 'Uncommon').
card_artist('homarid spawning bed'/'FEM', 'Douglas Shuler').
card_multiverse_id('homarid spawning bed'/'FEM', '1880').

card_in_set('homarid warrior', 'FEM').
card_original_type('homarid warrior'/'FEM', 'Summon — Homarid').
card_original_text('homarid warrior'/'FEM', '{U} Homarid Warrior may not be the target of spells or effects until end of turn and does not untap as normal during your next untap phase. If Homarid Warrior is untapped, tap it.').
card_first_print('homarid warrior', 'FEM').
card_image_name('homarid warrior'/'FEM', 'homarid warrior1').
card_uid('homarid warrior'/'FEM', 'FEM:Homarid Warrior:homarid warrior1').
card_rarity('homarid warrior'/'FEM', 'Common').
card_artist('homarid warrior'/'FEM', 'Daniel Gelon').
card_multiverse_id('homarid warrior'/'FEM', '1882').

card_in_set('homarid warrior', 'FEM').
card_original_type('homarid warrior'/'FEM', 'Summon — Homarid').
card_original_text('homarid warrior'/'FEM', '{U} Homarid Warrior may not be the target of spells or effects until end of turn and does not untap as normal during your next untap phase. If Homarid Warrior is untapped, tap it.').
card_image_name('homarid warrior'/'FEM', 'homarid warrior2').
card_uid('homarid warrior'/'FEM', 'FEM:Homarid Warrior:homarid warrior2').
card_rarity('homarid warrior'/'FEM', 'Common').
card_artist('homarid warrior'/'FEM', 'Randy Asplund-Faith').
card_multiverse_id('homarid warrior'/'FEM', '1881').

card_in_set('homarid warrior', 'FEM').
card_original_type('homarid warrior'/'FEM', 'Summon — Homarid').
card_original_text('homarid warrior'/'FEM', '{U} Homarid Warrior may not be the target of spells or effects until end of turn and does not untap as normal during your next untap phase. If Homarid Warrior is untapped, tap it.').
card_image_name('homarid warrior'/'FEM', 'homarid warrior3').
card_uid('homarid warrior'/'FEM', 'FEM:Homarid Warrior:homarid warrior3').
card_rarity('homarid warrior'/'FEM', 'Common').
card_artist('homarid warrior'/'FEM', 'Douglas Shuler').
card_multiverse_id('homarid warrior'/'FEM', '1883').

card_in_set('hymn to tourach', 'FEM').
card_original_type('hymn to tourach'/'FEM', 'Sorcery').
card_original_text('hymn to tourach'/'FEM', 'Target player discards two cards at random from his or her hand. If target player does not have enough cards, his or her entire hand is discarded.').
card_first_print('hymn to tourach', 'FEM').
card_image_name('hymn to tourach'/'FEM', 'hymn to tourach1').
card_uid('hymn to tourach'/'FEM', 'FEM:Hymn to Tourach:hymn to tourach1').
card_rarity('hymn to tourach'/'FEM', 'Common').
card_artist('hymn to tourach'/'FEM', 'Susan Van Camp').
card_flavor_text('hymn to tourach'/'FEM', '\"The eerie, wailing Hymn caused insanity even in hardened warriors.\"\n—Sarpadian Empires, vol. II').
card_multiverse_id('hymn to tourach'/'FEM', '1850').

card_in_set('hymn to tourach', 'FEM').
card_original_type('hymn to tourach'/'FEM', 'Sorcery').
card_original_text('hymn to tourach'/'FEM', 'Target player discards two cards at random from his or her hand. If target player does not have enough cards, his or her entire hand is discarded.').
card_image_name('hymn to tourach'/'FEM', 'hymn to tourach2').
card_uid('hymn to tourach'/'FEM', 'FEM:Hymn to Tourach:hymn to tourach2').
card_rarity('hymn to tourach'/'FEM', 'Common').
card_artist('hymn to tourach'/'FEM', 'Liz Danforth').
card_flavor_text('hymn to tourach'/'FEM', '\"Tourach\'s power was such that his followers deified him after his death.\"\n—Sarpadian Empires, vol. II').
card_multiverse_id('hymn to tourach'/'FEM', '1849').

card_in_set('hymn to tourach', 'FEM').
card_original_type('hymn to tourach'/'FEM', 'Sorcery').
card_original_text('hymn to tourach'/'FEM', 'Target player discards two cards at random from his or her hand. If target player does not have enough cards, his or her entire hand is discarded.').
card_image_name('hymn to tourach'/'FEM', 'hymn to tourach3').
card_uid('hymn to tourach'/'FEM', 'FEM:Hymn to Tourach:hymn to tourach3').
card_rarity('hymn to tourach'/'FEM', 'Common').
card_artist('hymn to tourach'/'FEM', 'Quinton Hoover').
card_flavor_text('hymn to tourach'/'FEM', 'Knowing the Hymn\'s power, the followers of Leitbur carefully guarded their pillaged transcriptions.').
card_multiverse_id('hymn to tourach'/'FEM', '1851').

card_in_set('hymn to tourach', 'FEM').
card_original_type('hymn to tourach'/'FEM', 'Sorcery').
card_original_text('hymn to tourach'/'FEM', 'Target player discards two cards at random from his or her hand. If target player does not have enough cards, his or her entire hand is discarded.').
card_image_name('hymn to tourach'/'FEM', 'hymn to tourach4').
card_uid('hymn to tourach'/'FEM', 'FEM:Hymn to Tourach:hymn to tourach4').
card_rarity('hymn to tourach'/'FEM', 'Common').
card_artist('hymn to tourach'/'FEM', 'Scott Kirschner').
card_flavor_text('hymn to tourach'/'FEM', 'Members of the Order often played the Hymn on instruments made from their victims\' bones.').
card_multiverse_id('hymn to tourach'/'FEM', '1852').

card_in_set('icatian infantry', 'FEM').
card_original_type('icatian infantry'/'FEM', 'Summon — Soldiers').
card_original_text('icatian infantry'/'FEM', '{1}: Bands until end of turn\n{1}: First strike until end of turn').
card_first_print('icatian infantry', 'FEM').
card_image_name('icatian infantry'/'FEM', 'icatian infantry1').
card_uid('icatian infantry'/'FEM', 'FEM:Icatian Infantry:icatian infantry1').
card_rarity('icatian infantry'/'FEM', 'Common').
card_artist('icatian infantry'/'FEM', 'Edward P. Beard, Jr.').
card_flavor_text('icatian infantry'/'FEM', 'The Icatian army easily repelled early surprise attacks by the Orcs on border towns like Montford.').
card_multiverse_id('icatian infantry'/'FEM', '1981').

card_in_set('icatian infantry', 'FEM').
card_original_type('icatian infantry'/'FEM', 'Summon — Soldiers').
card_original_text('icatian infantry'/'FEM', '{1}: Bands until end of turn\n{1}: First strike until end of turn').
card_image_name('icatian infantry'/'FEM', 'icatian infantry2').
card_uid('icatian infantry'/'FEM', 'FEM:Icatian Infantry:icatian infantry2').
card_rarity('icatian infantry'/'FEM', 'Common').
card_artist('icatian infantry'/'FEM', 'Christopher Rush').
card_flavor_text('icatian infantry'/'FEM', 'Although they had long been concerned about the Order of the Ebon Hand, the Icatians faced an even greater threat from Goblin and Orcish raiders.').
card_multiverse_id('icatian infantry'/'FEM', '1982').

card_in_set('icatian infantry', 'FEM').
card_original_type('icatian infantry'/'FEM', 'Summon — Soldiers').
card_original_text('icatian infantry'/'FEM', '{1}: Bands until end of turn\n{1}: First strike until end of turn').
card_image_name('icatian infantry'/'FEM', 'icatian infantry3').
card_uid('icatian infantry'/'FEM', 'FEM:Icatian Infantry:icatian infantry3').
card_rarity('icatian infantry'/'FEM', 'Common').
card_artist('icatian infantry'/'FEM', 'Douglas Shuler').
card_flavor_text('icatian infantry'/'FEM', '\"Valiant Icatia was the last of the Sarpadian empires to fall. Its faithful soldiers defended their cities to the very end.\"\n—Sarpadian Empires, vol. VI').
card_multiverse_id('icatian infantry'/'FEM', '1983').

card_in_set('icatian infantry', 'FEM').
card_original_type('icatian infantry'/'FEM', 'Summon — Soldiers').
card_original_text('icatian infantry'/'FEM', '{1}: Bands until end of turn\n{1}: First strike until end of turn').
card_image_name('icatian infantry'/'FEM', 'icatian infantry4').
card_uid('icatian infantry'/'FEM', 'FEM:Icatian Infantry:icatian infantry4').
card_rarity('icatian infantry'/'FEM', 'Common').
card_artist('icatian infantry'/'FEM', 'Drew Tucker').
card_flavor_text('icatian infantry'/'FEM', '\"Never Surrender\"\n—Motto carved into a fragment of an Icatian wall').
card_multiverse_id('icatian infantry'/'FEM', '1984').

card_in_set('icatian javelineers', 'FEM').
card_original_type('icatian javelineers'/'FEM', 'Summon — Soldiers').
card_original_text('icatian javelineers'/'FEM', 'When Icatian Javelineers is brought into play, put a javelin counter on it.\n{T}: Remove the javelin counter to have Icatian Javelineers deal 1 damage to any target.').
card_first_print('icatian javelineers', 'FEM').
card_image_name('icatian javelineers'/'FEM', 'icatian javelineers1').
card_uid('icatian javelineers'/'FEM', 'FEM:Icatian Javelineers:icatian javelineers1').
card_rarity('icatian javelineers'/'FEM', 'Common').
card_artist('icatian javelineers'/'FEM', 'Melissa A. Benson').
card_multiverse_id('icatian javelineers'/'FEM', '1986').

card_in_set('icatian javelineers', 'FEM').
card_original_type('icatian javelineers'/'FEM', 'Summon — Soldiers').
card_original_text('icatian javelineers'/'FEM', 'When Icatian Javelineers is brought into play, put a javelin counter on it.\n{T}: Remove the javelin counter to have Icatian Javelineers deal 1 damage to any target.').
card_image_name('icatian javelineers'/'FEM', 'icatian javelineers2').
card_uid('icatian javelineers'/'FEM', 'FEM:Icatian Javelineers:icatian javelineers2').
card_rarity('icatian javelineers'/'FEM', 'Common').
card_artist('icatian javelineers'/'FEM', 'Edward P. Beard, Jr.').
card_multiverse_id('icatian javelineers'/'FEM', '1985').

card_in_set('icatian javelineers', 'FEM').
card_original_type('icatian javelineers'/'FEM', 'Summon — Soldiers').
card_original_text('icatian javelineers'/'FEM', 'When Icatian Javelineers is brought into play, put a javelin counter on it.\n{T}: Remove the javelin counter to have Icatian Javelineers deal 1 damage to any target.').
card_image_name('icatian javelineers'/'FEM', 'icatian javelineers3').
card_uid('icatian javelineers'/'FEM', 'FEM:Icatian Javelineers:icatian javelineers3').
card_rarity('icatian javelineers'/'FEM', 'Common').
card_artist('icatian javelineers'/'FEM', 'Scott Kirschner').
card_multiverse_id('icatian javelineers'/'FEM', '1987').

card_in_set('icatian lieutenant', 'FEM').
card_original_type('icatian lieutenant'/'FEM', 'Summon — Soldier').
card_original_text('icatian lieutenant'/'FEM', '{1}{W} Target Soldier gets +1/+0 until end of turn.').
card_first_print('icatian lieutenant', 'FEM').
card_image_name('icatian lieutenant'/'FEM', 'icatian lieutenant').
card_uid('icatian lieutenant'/'FEM', 'FEM:Icatian Lieutenant:icatian lieutenant').
card_rarity('icatian lieutenant'/'FEM', 'Rare').
card_artist('icatian lieutenant'/'FEM', 'Pete Venters').
card_flavor_text('icatian lieutenant'/'FEM', 'To become an officer, an Icatian Soldier had to pass a series of tests. These evaluated not only fighting and leadership skills, but also integrity, honor, and moral strength.').
card_multiverse_id('icatian lieutenant'/'FEM', '1988').

card_in_set('icatian moneychanger', 'FEM').
card_original_type('icatian moneychanger'/'FEM', 'Summon — Townsfolk').
card_original_text('icatian moneychanger'/'FEM', 'Moneychanger deals 3 damage to you when summoned; put three credit counters on Moneychanger at that time. During your upkeep, put one credit counter on Moneychanger.\n{0}: Sacrifice Moneychanger to gain 1 life for each credit counter on it. Use this ability only during your upkeep.').
card_first_print('icatian moneychanger', 'FEM').
card_image_name('icatian moneychanger'/'FEM', 'icatian moneychanger1').
card_uid('icatian moneychanger'/'FEM', 'FEM:Icatian Moneychanger:icatian moneychanger1').
card_rarity('icatian moneychanger'/'FEM', 'Common').
card_artist('icatian moneychanger'/'FEM', 'Drew Tucker').
card_multiverse_id('icatian moneychanger'/'FEM', '1989').

card_in_set('icatian moneychanger', 'FEM').
card_original_type('icatian moneychanger'/'FEM', 'Summon — Townsfolk').
card_original_text('icatian moneychanger'/'FEM', 'Moneychanger deals 3 damage to you when summoned; put three credit counters on Moneychanger at that time. During your upkeep, put one credit counter on Moneychanger.\n{0}: Sacrifice Moneychanger to gain 1 life for each credit counter on it. Use this ability only during your upkeep.').
card_image_name('icatian moneychanger'/'FEM', 'icatian moneychanger2').
card_uid('icatian moneychanger'/'FEM', 'FEM:Icatian Moneychanger:icatian moneychanger2').
card_rarity('icatian moneychanger'/'FEM', 'Common').
card_artist('icatian moneychanger'/'FEM', 'Edward P. Beard, Jr.').
card_multiverse_id('icatian moneychanger'/'FEM', '1990').

card_in_set('icatian moneychanger', 'FEM').
card_original_type('icatian moneychanger'/'FEM', 'Summon — Townsfolk').
card_original_text('icatian moneychanger'/'FEM', 'Moneychanger deals 3 damage to you when summoned; put three credit counters on Moneychanger at that time. During your upkeep, put one credit counter on Moneychanger.\n{0}: Sacrifice Moneychanger to gain 1 life for each credit counter on it. Use this ability only during your upkeep.').
card_image_name('icatian moneychanger'/'FEM', 'icatian moneychanger3').
card_uid('icatian moneychanger'/'FEM', 'FEM:Icatian Moneychanger:icatian moneychanger3').
card_rarity('icatian moneychanger'/'FEM', 'Common').
card_artist('icatian moneychanger'/'FEM', 'Melissa A. Benson').
card_multiverse_id('icatian moneychanger'/'FEM', '1991').

card_in_set('icatian phalanx', 'FEM').
card_original_type('icatian phalanx'/'FEM', 'Summon — Soldiers').
card_original_text('icatian phalanx'/'FEM', 'Bands').
card_first_print('icatian phalanx', 'FEM').
card_image_name('icatian phalanx'/'FEM', 'icatian phalanx').
card_uid('icatian phalanx'/'FEM', 'FEM:Icatian Phalanx:icatian phalanx').
card_rarity('icatian phalanx'/'FEM', 'Uncommon').
card_artist('icatian phalanx'/'FEM', 'Kaja Foglio').
card_flavor_text('icatian phalanx'/'FEM', 'Even after the wall was breached in half a dozen places, the Phalanxes fought on, standing solidly against the onrushing raiders. Disciplined and dedicated, they held their ranks to the end, even in the face of tremendous losses.').
card_multiverse_id('icatian phalanx'/'FEM', '1992').

card_in_set('icatian priest', 'FEM').
card_original_type('icatian priest'/'FEM', 'Summon — Cleric').
card_original_text('icatian priest'/'FEM', '{1}{W}{W}: Target creature gets +1/+1 until end of turn.').
card_first_print('icatian priest', 'FEM').
card_image_name('icatian priest'/'FEM', 'icatian priest').
card_uid('icatian priest'/'FEM', 'FEM:Icatian Priest:icatian priest').
card_rarity('icatian priest'/'FEM', 'Uncommon').
card_artist('icatian priest'/'FEM', 'Drew Tucker').
card_flavor_text('icatian priest'/'FEM', '\"May you be strong and valiant, to defeat the enemies of the pure.\"\n—\"Leitbur\'s Prayer\"').
card_multiverse_id('icatian priest'/'FEM', '1993').

card_in_set('icatian scout', 'FEM').
card_original_type('icatian scout'/'FEM', 'Summon — Soldier').
card_original_text('icatian scout'/'FEM', '{1}, {T}: Target creature gains first strike until end of turn.').
card_first_print('icatian scout', 'FEM').
card_image_name('icatian scout'/'FEM', 'icatian scout1').
card_uid('icatian scout'/'FEM', 'FEM:Icatian Scout:icatian scout1').
card_rarity('icatian scout'/'FEM', 'Common').
card_artist('icatian scout'/'FEM', 'Richard Kane Ferguson').
card_flavor_text('icatian scout'/'FEM', '\"Because the Orc hordes attacked along the entire border, Scouts were essential to Icatia\'s defense.\"\n—Sarpadian Empires, vol. VI').
card_multiverse_id('icatian scout'/'FEM', '1994').

card_in_set('icatian scout', 'FEM').
card_original_type('icatian scout'/'FEM', 'Summon — Soldier').
card_original_text('icatian scout'/'FEM', '{1}, {T}: Target creature gains first strike until end of turn.').
card_image_name('icatian scout'/'FEM', 'icatian scout2').
card_uid('icatian scout'/'FEM', 'FEM:Icatian Scout:icatian scout2').
card_rarity('icatian scout'/'FEM', 'Common').
card_artist('icatian scout'/'FEM', 'Douglas Shuler').
card_flavor_text('icatian scout'/'FEM', '\"Let it be known that Ailis Connaut acted with honor and bravery in the defense of the town of Montford, risking her life to scout the enemy\'s position . . . .\"\n—Proclamation granting knighthood').
card_multiverse_id('icatian scout'/'FEM', '1997').

card_in_set('icatian scout', 'FEM').
card_original_type('icatian scout'/'FEM', 'Summon — Soldier').
card_original_text('icatian scout'/'FEM', '{1}, {T}: Target creature gains first strike until end of turn.').
card_image_name('icatian scout'/'FEM', 'icatian scout3').
card_uid('icatian scout'/'FEM', 'FEM:Icatian Scout:icatian scout3').
card_rarity('icatian scout'/'FEM', 'Common').
card_artist('icatian scout'/'FEM', 'Rob Alexander').
card_flavor_text('icatian scout'/'FEM', '\"Of course I\'m not a spy The enemy has spies. I am a Scout.\"\n—Ailis Connaut').
card_multiverse_id('icatian scout'/'FEM', '1996').

card_in_set('icatian scout', 'FEM').
card_original_type('icatian scout'/'FEM', 'Summon — Soldier').
card_original_text('icatian scout'/'FEM', '{1}, {T}: Target creature gains first strike until end of turn.').
card_image_name('icatian scout'/'FEM', 'icatian scout4').
card_uid('icatian scout'/'FEM', 'FEM:Icatian Scout:icatian scout4').
card_rarity('icatian scout'/'FEM', 'Common').
card_artist('icatian scout'/'FEM', 'Phil Foglio').
card_flavor_text('icatian scout'/'FEM', '\"Scouting is the art of balancing the need to go undiscovered with the need to get all the information you can. It\'s only by Leitbur\'s good grace that I\'m still alive today.\"\n—Ailis Connaut, Diary').
card_multiverse_id('icatian scout'/'FEM', '1995').

card_in_set('icatian skirmishers', 'FEM').
card_original_type('icatian skirmishers'/'FEM', 'Summon — Soldiers').
card_original_text('icatian skirmishers'/'FEM', 'Bands, first strike\nAll creatures that band with Skirmishers to attack gain first strike until end of turn.').
card_first_print('icatian skirmishers', 'FEM').
card_image_name('icatian skirmishers'/'FEM', 'icatian skirmishers').
card_uid('icatian skirmishers'/'FEM', 'FEM:Icatian Skirmishers:icatian skirmishers').
card_rarity('icatian skirmishers'/'FEM', 'Rare').
card_artist('icatian skirmishers'/'FEM', 'Heather Hudson').
card_flavor_text('icatian skirmishers'/'FEM', 'Skirmishers engaged raiders before they could reach the towns. Although these units typically suffered huge losses, they never lacked volunteers.').
card_multiverse_id('icatian skirmishers'/'FEM', '1998').

card_in_set('icatian store', 'FEM').
card_original_type('icatian store'/'FEM', 'Land').
card_original_text('icatian store'/'FEM', 'Comes into play tapped. You may choose not to untap Icatian Store during your untap phase and instead put a storage counter on it.\n{T}: Remove any number of storage counters from Icatian Store. For each storage counter removed, add {W} to your mana pool.').
card_first_print('icatian store', 'FEM').
card_image_name('icatian store'/'FEM', 'icatian store').
card_uid('icatian store'/'FEM', 'FEM:Icatian Store:icatian store').
card_rarity('icatian store'/'FEM', 'Rare').
card_artist('icatian store'/'FEM', 'Pat Morrissey').
card_multiverse_id('icatian store'/'FEM', '2009').

card_in_set('icatian town', 'FEM').
card_original_type('icatian town'/'FEM', 'Sorcery').
card_original_text('icatian town'/'FEM', 'Put 4 Citizen tokens into play. Treat these tokens as 1/1 white creatures.').
card_first_print('icatian town', 'FEM').
card_image_name('icatian town'/'FEM', 'icatian town').
card_uid('icatian town'/'FEM', 'FEM:Icatian Town:icatian town').
card_rarity('icatian town'/'FEM', 'Rare').
card_artist('icatian town'/'FEM', 'Tom Wänerstrand').
card_flavor_text('icatian town'/'FEM', 'Icatia\'s once peaceful towns faced increasing attacks from Orcs and Goblins as the climate cooled. By the time the empire fell, they were little more than armed camps.').
card_multiverse_id('icatian town'/'FEM', '1999').

card_in_set('implements of sacrifice', 'FEM').
card_original_type('implements of sacrifice'/'FEM', 'Artifact').
card_original_text('implements of sacrifice'/'FEM', '{1}, {T}: Sacrifice Implements of Sacrifice to add 2 mana of any one color to your mana pool. Play this ability as an interrupt.').
card_first_print('implements of sacrifice', 'FEM').
card_image_name('implements of sacrifice'/'FEM', 'implements of sacrifice').
card_uid('implements of sacrifice'/'FEM', 'FEM:Implements of Sacrifice:implements of sacrifice').
card_rarity('implements of sacrifice'/'FEM', 'Rare').
card_artist('implements of sacrifice'/'FEM', 'Margaret Organ-Kean').
card_flavor_text('implements of sacrifice'/'FEM', 'Relics of the Order of the Ebon Hand, the bowl and dagger bespeak the hideous cruelty of its rituals.').
card_multiverse_id('implements of sacrifice'/'FEM', '1834').

card_in_set('initiates of the ebon hand', 'FEM').
card_original_type('initiates of the ebon hand'/'FEM', 'Summon — Clerics').
card_original_text('initiates of the ebon hand'/'FEM', '{1}: Add {B} to your mana pool. Play this ability as an interrupt. If more than {3} is spent in this way during one turn, bury Initiates of the Ebon Hand at end of turn.').
card_first_print('initiates of the ebon hand', 'FEM').
card_image_name('initiates of the ebon hand'/'FEM', 'initiates of the ebon hand1').
card_uid('initiates of the ebon hand'/'FEM', 'FEM:Initiates of the Ebon Hand:initiates of the ebon hand1').
card_rarity('initiates of the ebon hand'/'FEM', 'Common').
card_artist('initiates of the ebon hand'/'FEM', 'Liz Danforth').
card_flavor_text('initiates of the ebon hand'/'FEM', 'Ebon leaders soon realized that non-human races often had special talents that could be exploited.').
card_multiverse_id('initiates of the ebon hand'/'FEM', '1855').

card_in_set('initiates of the ebon hand', 'FEM').
card_original_type('initiates of the ebon hand'/'FEM', 'Summon — Clerics').
card_original_text('initiates of the ebon hand'/'FEM', '{1}: Add {B} to your mana pool. Play this ability as an interrupt. If more than {3} is spent in this way during one turn, bury Initiates of the Ebon Hand at end of turn.').
card_image_name('initiates of the ebon hand'/'FEM', 'initiates of the ebon hand2').
card_uid('initiates of the ebon hand'/'FEM', 'FEM:Initiates of the Ebon Hand:initiates of the ebon hand2').
card_rarity('initiates of the ebon hand'/'FEM', 'Common').
card_artist('initiates of the ebon hand'/'FEM', 'Kaja Foglio').
card_flavor_text('initiates of the ebon hand'/'FEM', '\"Many Initiates sacrificed a hand to become full members of the Order.\"\n—Sarpadian Empires, vol. II').
card_multiverse_id('initiates of the ebon hand'/'FEM', '1854').

card_in_set('initiates of the ebon hand', 'FEM').
card_original_type('initiates of the ebon hand'/'FEM', 'Summon — Clerics').
card_original_text('initiates of the ebon hand'/'FEM', '{1}: Add {B} to your mana pool. Play this ability as an interrupt. If more than {3} is spent in this way during one turn, bury Initiates of the Ebon Hand at end of turn.').
card_image_name('initiates of the ebon hand'/'FEM', 'initiates of the ebon hand3').
card_uid('initiates of the ebon hand'/'FEM', 'FEM:Initiates of the Ebon Hand:initiates of the ebon hand3').
card_rarity('initiates of the ebon hand'/'FEM', 'Common').
card_artist('initiates of the ebon hand'/'FEM', 'Heather Hudson').
card_flavor_text('initiates of the ebon hand'/'FEM', '\"We are no longer Nature\'s children, but her masters . . . .\"\n—Oath of the Ebon Hand').
card_multiverse_id('initiates of the ebon hand'/'FEM', '1853').

card_in_set('merseine', 'FEM').
card_original_type('merseine'/'FEM', 'Enchant Creature').
card_original_text('merseine'/'FEM', 'Put three net counters on Merseine when it is brought into play. Target creature Merseine enchants does not untap as normal during its controller\'s untap phase as long as any net counters remain. As a fast effect, target creature\'s controller may pay creature\'s casting cost to remove a net counter.').
card_first_print('merseine', 'FEM').
card_image_name('merseine'/'FEM', 'merseine1').
card_uid('merseine'/'FEM', 'FEM:Merseine:merseine1').
card_rarity('merseine'/'FEM', 'Common').
card_artist('merseine'/'FEM', 'Heather Hudson').
card_multiverse_id('merseine'/'FEM', '1884').

card_in_set('merseine', 'FEM').
card_original_type('merseine'/'FEM', 'Enchant Creature').
card_original_text('merseine'/'FEM', 'Put three net counters on Merseine when it is brought into play. Target creature Merseine enchants does not untap as normal during its controller\'s untap phase as long as any net counters remain. As a fast effect, target creature\'s controller may pay creature\'s casting cost to remove a net counter.').
card_image_name('merseine'/'FEM', 'merseine2').
card_uid('merseine'/'FEM', 'FEM:Merseine:merseine2').
card_rarity('merseine'/'FEM', 'Common').
card_artist('merseine'/'FEM', 'Margaret Organ-Kean').
card_multiverse_id('merseine'/'FEM', '1885').

card_in_set('merseine', 'FEM').
card_original_type('merseine'/'FEM', 'Enchant Creature').
card_original_text('merseine'/'FEM', 'Put three net counters on Merseine when it is brought into play. Target creature Merseine enchants does not untap as normal during its controller\'s untap phase as long as any net counters remain. As a fast effect, target creature\'s controller may pay creature\'s casting cost to remove a net counter.').
card_image_name('merseine'/'FEM', 'merseine3').
card_uid('merseine'/'FEM', 'FEM:Merseine:merseine3').
card_rarity('merseine'/'FEM', 'Common').
card_artist('merseine'/'FEM', 'Drew Tucker').
card_multiverse_id('merseine'/'FEM', '1887').

card_in_set('merseine', 'FEM').
card_original_type('merseine'/'FEM', 'Enchant Creature').
card_original_text('merseine'/'FEM', 'Put three net counters on Merseine when it is brought into play. Target creature Merseine enchants does not untap as normal during its controller\'s untap phase as long as any net counters remain. As a fast effect, target creature\'s controller may pay creature\'s casting cost to remove a net counter.').
card_image_name('merseine'/'FEM', 'merseine4').
card_uid('merseine'/'FEM', 'FEM:Merseine:merseine4').
card_rarity('merseine'/'FEM', 'Common').
card_artist('merseine'/'FEM', 'Pete Venters').
card_multiverse_id('merseine'/'FEM', '1886').

card_in_set('mindstab thrull', 'FEM').
card_original_type('mindstab thrull'/'FEM', 'Summon — Thrull').
card_original_text('mindstab thrull'/'FEM', 'If Mindstab Thrull attacks and is not blocked, you may sacrifice it to force the player it attacked to discard three cards. If you do so, it deals no damage during combat this turn. If that player does not have enough cards, his or her entire hand is discarded.').
card_first_print('mindstab thrull', 'FEM').
card_image_name('mindstab thrull'/'FEM', 'mindstab thrull1').
card_uid('mindstab thrull'/'FEM', 'FEM:Mindstab Thrull:mindstab thrull1').
card_rarity('mindstab thrull'/'FEM', 'Common').
card_artist('mindstab thrull'/'FEM', 'Richard Kane Ferguson').
card_multiverse_id('mindstab thrull'/'FEM', '1857').

card_in_set('mindstab thrull', 'FEM').
card_original_type('mindstab thrull'/'FEM', 'Summon — Thrull').
card_original_text('mindstab thrull'/'FEM', 'If Mindstab Thrull attacks and is not blocked, you may sacrifice it to force the player it attacked to discard three cards. If you do so, it deals no damage during combat this turn. If that player does not have enough cards, his or her entire hand is discarded.').
card_image_name('mindstab thrull'/'FEM', 'mindstab thrull2').
card_uid('mindstab thrull'/'FEM', 'FEM:Mindstab Thrull:mindstab thrull2').
card_rarity('mindstab thrull'/'FEM', 'Common').
card_artist('mindstab thrull'/'FEM', 'Heather Hudson').
card_multiverse_id('mindstab thrull'/'FEM', '1856').

card_in_set('mindstab thrull', 'FEM').
card_original_type('mindstab thrull'/'FEM', 'Summon — Thrull').
card_original_text('mindstab thrull'/'FEM', 'If Mindstab Thrull attacks and is not blocked, you may sacrifice it to force the player it attacked to discard three cards. If you do so, it deals no damage during combat this turn. If that player does not have enough cards, his or her entire hand is discarded.').
card_image_name('mindstab thrull'/'FEM', 'mindstab thrull3').
card_uid('mindstab thrull'/'FEM', 'FEM:Mindstab Thrull:mindstab thrull3').
card_rarity('mindstab thrull'/'FEM', 'Common').
card_artist('mindstab thrull'/'FEM', 'Mark Tedin').
card_multiverse_id('mindstab thrull'/'FEM', '1858').

card_in_set('necrite', 'FEM').
card_original_type('necrite'/'FEM', 'Summon — Thrull').
card_original_text('necrite'/'FEM', 'If Necrite attacks and is not blocked, you may sacrifice it to bury a target creature controlled by the player Necrite attacked this turn. If you do so, Necrite deals no damage during combat this turn.').
card_first_print('necrite', 'FEM').
card_image_name('necrite'/'FEM', 'necrite1').
card_uid('necrite'/'FEM', 'FEM:Necrite:necrite1').
card_rarity('necrite'/'FEM', 'Common').
card_artist('necrite'/'FEM', 'Ron Spencer').
card_flavor_text('necrite'/'FEM', 'Necrites killed Jherana Rure, ending the counter-insurgency.').
card_multiverse_id('necrite'/'FEM', '1860').

card_in_set('necrite', 'FEM').
card_original_type('necrite'/'FEM', 'Summon — Thrull').
card_original_text('necrite'/'FEM', 'If Necrite attacks and is not blocked, you may sacrifice it to bury a target creature controlled by the player Necrite attacked this turn. If you do so, Necrite deals no damage during combat this turn.').
card_image_name('necrite'/'FEM', 'necrite2').
card_uid('necrite'/'FEM', 'FEM:Necrite:necrite2').
card_rarity('necrite'/'FEM', 'Common').
card_artist('necrite'/'FEM', 'Christopher Rush').
card_flavor_text('necrite'/'FEM', '\"Ever see a wall drop dead of fright, kid? It ain\'t pretty.\" —Cpl. Dobbs').
card_multiverse_id('necrite'/'FEM', '1859').

card_in_set('necrite', 'FEM').
card_original_type('necrite'/'FEM', 'Summon — Thrull').
card_original_text('necrite'/'FEM', 'If Necrite attacks and is not blocked, you may sacrifice it to bury a target creature controlled by the player Necrite attacked this turn. If you do so, Necrite deals no damage during combat this turn.').
card_image_name('necrite'/'FEM', 'necrite3').
card_uid('necrite'/'FEM', 'FEM:Necrite:necrite3').
card_rarity('necrite'/'FEM', 'Common').
card_artist('necrite'/'FEM', 'Drew Tucker').
card_flavor_text('necrite'/'FEM', 'Was it the sight, smell, or aura of the Necrite that killed so effectively?').
card_multiverse_id('necrite'/'FEM', '1861').

card_in_set('night soil', 'FEM').
card_original_type('night soil'/'FEM', 'Enchantment').
card_original_text('night soil'/'FEM', '{1}: Remove two creatures in any graveyard from the game to put a Saproling token into play. Treat this token as a 1/1 green creature.').
card_first_print('night soil', 'FEM').
card_image_name('night soil'/'FEM', 'night soil1').
card_uid('night soil'/'FEM', 'FEM:Night Soil:night soil1').
card_rarity('night soil'/'FEM', 'Common').
card_artist('night soil'/'FEM', 'Sandra Everingham').
card_flavor_text('night soil'/'FEM', 'Some said killing the Thallids only encouraged them.').
card_multiverse_id('night soil'/'FEM', '1918').

card_in_set('night soil', 'FEM').
card_original_type('night soil'/'FEM', 'Enchantment').
card_original_text('night soil'/'FEM', '{1}: Remove two creatures in any graveyard from the game to put a Saproling token into play. Treat this token as a 1/1 green creature.').
card_image_name('night soil'/'FEM', 'night soil2').
card_uid('night soil'/'FEM', 'FEM:Night Soil:night soil2').
card_rarity('night soil'/'FEM', 'Common').
card_artist('night soil'/'FEM', 'Heather Hudson').
card_flavor_text('night soil'/'FEM', 'The Elves gathered huge piles of rot to grow fungus. Out of imitation or forethought, the Thallids did the same.').
card_multiverse_id('night soil'/'FEM', '1917').

card_in_set('night soil', 'FEM').
card_original_type('night soil'/'FEM', 'Enchantment').
card_original_text('night soil'/'FEM', '{1}: Remove two creatures in any graveyard from the game to put a Saproling token into play. Treat this token as a 1/1 green creature.').
card_image_name('night soil'/'FEM', 'night soil3').
card_uid('night soil'/'FEM', 'FEM:Night Soil:night soil3').
card_rarity('night soil'/'FEM', 'Common').
card_artist('night soil'/'FEM', 'Drew Tucker').
card_flavor_text('night soil'/'FEM', '\"There were often more Thallids after a battle than before.\"\n—Sarpadian Empires, vol. III').
card_multiverse_id('night soil'/'FEM', '1919').

card_in_set('orcish captain', 'FEM').
card_original_type('orcish captain'/'FEM', 'Summon — Orc').
card_original_text('orcish captain'/'FEM', '{1}: Choose a target Orc. Flip a coin; opponent calls heads or tails while coin is in the air. If the flip ends up in your favor, that Orc gets +2/+0 until end of turn. Otherwise, that Orc gets -0/-2 until end of turn.').
card_first_print('orcish captain', 'FEM').
card_image_name('orcish captain'/'FEM', 'orcish captain').
card_uid('orcish captain'/'FEM', 'FEM:Orcish Captain:orcish captain').
card_rarity('orcish captain'/'FEM', 'Uncommon').
card_artist('orcish captain'/'FEM', 'Mark Tedin').
card_flavor_text('orcish captain'/'FEM', 'There\'s a chance to win every battle.').
card_multiverse_id('orcish captain'/'FEM', '1960').

card_in_set('orcish spy', 'FEM').
card_original_type('orcish spy'/'FEM', 'Summon — Orc').
card_original_text('orcish spy'/'FEM', '{T}: Look at the top three cards of target player\'s library and return them in the same order.').
card_first_print('orcish spy', 'FEM').
card_image_name('orcish spy'/'FEM', 'orcish spy1').
card_uid('orcish spy'/'FEM', 'FEM:Orcish Spy:orcish spy1').
card_rarity('orcish spy'/'FEM', 'Common').
card_artist('orcish spy'/'FEM', 'Susan Van Camp').
card_flavor_text('orcish spy'/'FEM', '\"Yeah, they\'re ugly, they desert in droves, and their personal habits are enough to make you sick. But I\'ll say this for Orcs: they make great spies.\"\n—Ivra Jursdotter').
card_multiverse_id('orcish spy'/'FEM', '1962').

card_in_set('orcish spy', 'FEM').
card_original_type('orcish spy'/'FEM', 'Summon — Orc').
card_original_text('orcish spy'/'FEM', '{T}: Look at the top three cards of target player\'s library and return them in the same order.').
card_image_name('orcish spy'/'FEM', 'orcish spy2').
card_uid('orcish spy'/'FEM', 'FEM:Orcish Spy:orcish spy2').
card_rarity('orcish spy'/'FEM', 'Common').
card_artist('orcish spy'/'FEM', 'Daniel Gelon').
card_flavor_text('orcish spy'/'FEM', '\"Orcish armies often employed the smaller, swifter, and less intelligent Goblins as spies.\"\n—Sarpadian Empires, vol. I').
card_multiverse_id('orcish spy'/'FEM', '1961').

card_in_set('orcish spy', 'FEM').
card_original_type('orcish spy'/'FEM', 'Summon — Orc').
card_original_text('orcish spy'/'FEM', '{T}: Look at the top three cards of target player\'s library and return them in the same order.').
card_image_name('orcish spy'/'FEM', 'orcish spy3').
card_uid('orcish spy'/'FEM', 'FEM:Orcish Spy:orcish spy3').
card_rarity('orcish spy'/'FEM', 'Common').
card_artist('orcish spy'/'FEM', 'Pete Venters').
card_flavor_text('orcish spy'/'FEM', '\"You idiot Never let the spies mingle with the Orcish regulars after completing a mission. Now we\'ll never get them to fight\"\n—General Khurzog').
card_multiverse_id('orcish spy'/'FEM', '1963').

card_in_set('orcish veteran', 'FEM').
card_original_type('orcish veteran'/'FEM', 'Summon — Orc').
card_original_text('orcish veteran'/'FEM', 'Cannot be assigned to block any white creature of power greater than 1.\n{R} First strike until end of turn').
card_first_print('orcish veteran', 'FEM').
card_image_name('orcish veteran'/'FEM', 'orcish veteran1').
card_uid('orcish veteran'/'FEM', 'FEM:Orcish Veteran:orcish veteran1').
card_rarity('orcish veteran'/'FEM', 'Common').
card_artist('orcish veteran'/'FEM', 'Douglas Shuler').
card_flavor_text('orcish veteran'/'FEM', 'Orcs are not exactly known for their valor—although most Orcs have seen countless battles, only a handful have actually fought in them.').
card_multiverse_id('orcish veteran'/'FEM', '1967').

card_in_set('orcish veteran', 'FEM').
card_original_type('orcish veteran'/'FEM', 'Summon — Orc').
card_original_text('orcish veteran'/'FEM', 'Cannot be assigned to block any white creature of power greater than 1.\n{R} First strike until end of turn').
card_image_name('orcish veteran'/'FEM', 'orcish veteran2').
card_uid('orcish veteran'/'FEM', 'FEM:Orcish Veteran:orcish veteran2').
card_rarity('orcish veteran'/'FEM', 'Common').
card_artist('orcish veteran'/'FEM', 'Dan Frazier').
card_flavor_text('orcish veteran'/'FEM', '\"Orcs often greeted promotions to Icatian battle with anguished wails and pleas for mercy.\"\n—Sarpadian Empires, vol. VI').
card_multiverse_id('orcish veteran'/'FEM', '1965').

card_in_set('orcish veteran', 'FEM').
card_original_type('orcish veteran'/'FEM', 'Summon — Orc').
card_original_text('orcish veteran'/'FEM', 'Cannot be assigned to block any white creature of power greater than 1.\n{R} First strike until end of turn').
card_image_name('orcish veteran'/'FEM', 'orcish veteran3').
card_uid('orcish veteran'/'FEM', 'FEM:Orcish Veteran:orcish veteran3').
card_rarity('orcish veteran'/'FEM', 'Common').
card_artist('orcish veteran'/'FEM', 'Quinton Hoover').
card_flavor_text('orcish veteran'/'FEM', 'Unsuccessful in their early battle for Montford, the Orcs quickly tempered their bloodlust with cowardice.').
card_multiverse_id('orcish veteran'/'FEM', '1939').

card_in_set('orcish veteran', 'FEM').
card_original_type('orcish veteran'/'FEM', 'Summon — Orc').
card_original_text('orcish veteran'/'FEM', 'Cannot be assigned to block any white creature of power greater than 1.\n{R} First strike until end of turn').
card_image_name('orcish veteran'/'FEM', 'orcish veteran4').
card_uid('orcish veteran'/'FEM', 'FEM:Orcish Veteran:orcish veteran4').
card_rarity('orcish veteran'/'FEM', 'Common').
card_artist('orcish veteran'/'FEM', 'Melissa A. Benson').
card_flavor_text('orcish veteran'/'FEM', 'Although models of courage for Orcs, the Veterans quickly learned to fear the swift justice of their Icatian enemies.').
card_multiverse_id('orcish veteran'/'FEM', '1964').

card_in_set('order of leitbur', 'FEM').
card_original_type('order of leitbur'/'FEM', 'Summon — Clerics').
card_original_text('order of leitbur'/'FEM', 'Protection from black\n{W}{W} +1/+0 until end of turn\n{W} First strike until end of turn').
card_first_print('order of leitbur', 'FEM').
card_image_name('order of leitbur'/'FEM', 'order of leitbur1').
card_uid('order of leitbur'/'FEM', 'FEM:Order of Leitbur:order of leitbur1').
card_rarity('order of leitbur'/'FEM', 'Common').
card_artist('order of leitbur'/'FEM', 'Bryon Wackwitz').
card_flavor_text('order of leitbur'/'FEM', 'Followers of Tourach regarded all other religions equally: with open contempt. Not so the followers of Leitbur, who made it their mission to eradicate the Order of the Ebon Hand.').
card_multiverse_id('order of leitbur'/'FEM', '2001').

card_in_set('order of leitbur', 'FEM').
card_original_type('order of leitbur'/'FEM', 'Summon — Clerics').
card_original_text('order of leitbur'/'FEM', 'Protection from black\n{W}{W} +1/+0 until end of turn\n{W} First strike until end of turn').
card_image_name('order of leitbur'/'FEM', 'order of leitbur2').
card_uid('order of leitbur'/'FEM', 'FEM:Order of Leitbur:order of leitbur2').
card_rarity('order of leitbur'/'FEM', 'Common').
card_artist('order of leitbur'/'FEM', 'Bryon Wackwitz').
card_flavor_text('order of leitbur'/'FEM', '\"The powers of the corrupt will fade before the fury of the pure.\"\n—Bethan Leitbur, \"The Way\"').
card_multiverse_id('order of leitbur'/'FEM', '2000').

card_in_set('order of leitbur', 'FEM').
card_original_type('order of leitbur'/'FEM', 'Summon — Clerics').
card_original_text('order of leitbur'/'FEM', 'Protection from black\n{W}{W} +1/+0 until end of turn\n{W} First strike until end of turn').
card_image_name('order of leitbur'/'FEM', 'order of leitbur3').
card_uid('order of leitbur'/'FEM', 'FEM:Order of Leitbur:order of leitbur3').
card_rarity('order of leitbur'/'FEM', 'Common').
card_artist('order of leitbur'/'FEM', 'Randy Asplund-Faith').
card_flavor_text('order of leitbur'/'FEM', '\"Trained to battle the followers of Tourach, the Order of Leitbur was not as successful in later conflicts with Orcish and Goblin raiders.\"\n—Sarpadian Empires, vol. I').
card_multiverse_id('order of leitbur'/'FEM', '2002').

card_in_set('order of the ebon hand', 'FEM').
card_original_type('order of the ebon hand'/'FEM', 'Summon — Clerics').
card_original_text('order of the ebon hand'/'FEM', 'Protection from white\n{B}{B} +1/+0 until end of turn\n{B} First strike until end of turn').
card_first_print('order of the ebon hand', 'FEM').
card_image_name('order of the ebon hand'/'FEM', 'order of the ebon hand1').
card_uid('order of the ebon hand'/'FEM', 'FEM:Order of the Ebon Hand:order of the ebon hand1').
card_rarity('order of the ebon hand'/'FEM', 'Common').
card_artist('order of the ebon hand'/'FEM', 'Melissa A. Benson').
card_flavor_text('order of the ebon hand'/'FEM', 'A true follower of Tourach took pride in achievement to the exclusion of other concerns.').
card_multiverse_id('order of the ebon hand'/'FEM', '1863').

card_in_set('order of the ebon hand', 'FEM').
card_original_type('order of the ebon hand'/'FEM', 'Summon — Clerics').
card_original_text('order of the ebon hand'/'FEM', 'Protection from white\n{B}{B} +1/+0 until end of turn\n{B} First strike until end of turn').
card_image_name('order of the ebon hand'/'FEM', 'order of the ebon hand2').
card_uid('order of the ebon hand'/'FEM', 'FEM:Order of the Ebon Hand:order of the ebon hand2').
card_rarity('order of the ebon hand'/'FEM', 'Common').
card_artist('order of the ebon hand'/'FEM', 'Christopher Rush').
card_flavor_text('order of the ebon hand'/'FEM', '\"There are intriguing similarities between the Order and Icatia\'s Leitbur religion, suggesting the two had a common origin.\"\n—Sarpadian Empires, vol. VI').
card_multiverse_id('order of the ebon hand'/'FEM', '1862').

card_in_set('order of the ebon hand', 'FEM').
card_original_type('order of the ebon hand'/'FEM', 'Summon — Clerics').
card_original_text('order of the ebon hand'/'FEM', 'Protection from white\n{B}{B} +1/+0 until end of turn\n{B} First strike until end of turn').
card_image_name('order of the ebon hand'/'FEM', 'order of the ebon hand3').
card_uid('order of the ebon hand'/'FEM', 'FEM:Order of the Ebon Hand:order of the ebon hand3').
card_rarity('order of the ebon hand'/'FEM', 'Common').
card_artist('order of the ebon hand'/'FEM', 'Ron Spencer').
card_flavor_text('order of the ebon hand'/'FEM', 'Dedicated to the principles of Tourach, members of the Order of the Ebon Hand demonstrated their devotion with grisly rituals.').
card_multiverse_id('order of the ebon hand'/'FEM', '1864').

card_in_set('orgg', 'FEM').
card_original_type('orgg'/'FEM', 'Summon — Orgg').
card_original_text('orgg'/'FEM', 'Trample\nOrgg may not attack if opponent controls an untapped creature of power greater than 2. Orgg cannot be assigned to block any creature of power greater than 2.').
card_first_print('orgg', 'FEM').
card_image_name('orgg'/'FEM', 'orgg').
card_uid('orgg'/'FEM', 'FEM:Orgg:orgg').
card_rarity('orgg'/'FEM', 'Rare').
card_artist('orgg'/'FEM', 'Daniel Gelon').
card_flavor_text('orgg'/'FEM', 'It\'s bigger than it thinks.').
card_multiverse_id('orgg'/'FEM', '1968').

card_in_set('raiding party', 'FEM').
card_original_type('raiding party'/'FEM', 'Enchantment').
card_original_text('raiding party'/'FEM', 'Raiding Party may not be the target of white spells or effects.\n{0}: Sacrifice an Orc to destroy all plains. A player may tap a white creature to prevent up to two plains from being destroyed. Any number of creatures may be tapped in this manner.').
card_first_print('raiding party', 'FEM').
card_image_name('raiding party'/'FEM', 'raiding party').
card_uid('raiding party'/'FEM', 'FEM:Raiding Party:raiding party').
card_rarity('raiding party'/'FEM', 'Uncommon').
card_artist('raiding party'/'FEM', 'Quinton Hoover').
card_multiverse_id('raiding party'/'FEM', '1969').

card_in_set('rainbow vale', 'FEM').
card_original_type('rainbow vale'/'FEM', 'Land').
card_original_text('rainbow vale'/'FEM', '{T}: Add 1 mana of any color to your mana pool. Control of Rainbow Vale passes to opponent at end of turn.').
card_first_print('rainbow vale', 'FEM').
card_image_name('rainbow vale'/'FEM', 'rainbow vale').
card_uid('rainbow vale'/'FEM', 'FEM:Rainbow Vale:rainbow vale').
card_rarity('rainbow vale'/'FEM', 'Rare').
card_artist('rainbow vale'/'FEM', 'Kaja Foglio').
card_flavor_text('rainbow vale'/'FEM', 'In the feudal days of Icatia, finding the Rainbow Vale was often the goal of Knights\' quests.').
card_multiverse_id('rainbow vale'/'FEM', '2010').

card_in_set('ring of renewal', 'FEM').
card_original_type('ring of renewal'/'FEM', 'Artifact').
card_original_text('ring of renewal'/'FEM', '{5}, {T}: Discard a card at random from your hand and draw two cards.').
card_first_print('ring of renewal', 'FEM').
card_image_name('ring of renewal'/'FEM', 'ring of renewal').
card_uid('ring of renewal'/'FEM', 'FEM:Ring of Renewal:ring of renewal').
card_rarity('ring of renewal'/'FEM', 'Rare').
card_artist('ring of renewal'/'FEM', 'Douglas Shuler').
card_flavor_text('ring of renewal'/'FEM', 'To the uninitiated, the Ring of Renewal is merely an oddity. For those fluent in the wielding of magic, however, it is a source of great knowledge.').
card_multiverse_id('ring of renewal'/'FEM', '1835').

card_in_set('river merfolk', 'FEM').
card_original_type('river merfolk'/'FEM', 'Summon — Merfolk').
card_original_text('river merfolk'/'FEM', '{U} Mountainwalk until end of turn').
card_first_print('river merfolk', 'FEM').
card_image_name('river merfolk'/'FEM', 'river merfolk').
card_uid('river merfolk'/'FEM', 'FEM:River Merfolk:river merfolk').
card_rarity('river merfolk'/'FEM', 'Rare').
card_artist('river merfolk'/'FEM', 'Douglas Shuler').
card_flavor_text('river merfolk'/'FEM', '\"Dwelling in icy mountain streams near Goblin and Orcish foes, the River Merfolk were known for their stoicism.\"\n—Sarpadian Empires, vol. V').
card_multiverse_id('river merfolk'/'FEM', '1888').

card_in_set('ruins of trokair', 'FEM').
card_original_type('ruins of trokair'/'FEM', 'Land').
card_original_text('ruins of trokair'/'FEM', 'Comes into play tapped.\n{T}: Add {W} to your mana pool.\n{T}: Sacrifice Ruins of Trokair to add {W}{W} to your mana pool.').
card_first_print('ruins of trokair', 'FEM').
card_image_name('ruins of trokair'/'FEM', 'ruins of trokair').
card_uid('ruins of trokair'/'FEM', 'FEM:Ruins of Trokair:ruins of trokair').
card_rarity('ruins of trokair'/'FEM', 'Uncommon').
card_artist('ruins of trokair'/'FEM', 'Mark Poole').
card_multiverse_id('ruins of trokair'/'FEM', '2011').

card_in_set('sand silos', 'FEM').
card_original_type('sand silos'/'FEM', 'Land').
card_original_text('sand silos'/'FEM', 'Comes into play tapped. You may choose not to untap Sand Silos during your untap phase and instead put a storage counter on it.\n{T}: Remove any number of storage counters from Sand Silos. For each storage counter removed, add {U} to your mana pool.').
card_first_print('sand silos', 'FEM').
card_image_name('sand silos'/'FEM', 'sand silos').
card_uid('sand silos'/'FEM', 'FEM:Sand Silos:sand silos').
card_rarity('sand silos'/'FEM', 'Rare').
card_artist('sand silos'/'FEM', 'Pat Morrissey').
card_multiverse_id('sand silos'/'FEM', '2012').

card_in_set('seasinger', 'FEM').
card_original_type('seasinger'/'FEM', 'Summon — Merfolk').
card_original_text('seasinger'/'FEM', 'Bury Seasinger if you control no islands.\n{T}: Gain control of a target creature if its controller controls at least one island. You lose control of target creature if Seasinger leaves play, if you lose control of Seasinger, or if Seasinger becomes untapped. You may choose not to untap Seasinger as normal during your untap phase.').
card_first_print('seasinger', 'FEM').
card_image_name('seasinger'/'FEM', 'seasinger').
card_uid('seasinger'/'FEM', 'FEM:Seasinger:seasinger').
card_rarity('seasinger'/'FEM', 'Uncommon').
card_artist('seasinger'/'FEM', 'Amy Weber').
card_multiverse_id('seasinger'/'FEM', '1889').

card_in_set('soul exchange', 'FEM').
card_original_type('soul exchange'/'FEM', 'Sorcery').
card_original_text('soul exchange'/'FEM', 'Sacrifice a creature, but remove it from the game instead of putting it in your graveyard. Take a creature from your graveyard and put it directly into play as though it were just summoned. Put a +2/+2 counter on this creature if the creature sacrificed was a Thrull.').
card_first_print('soul exchange', 'FEM').
card_image_name('soul exchange'/'FEM', 'soul exchange').
card_uid('soul exchange'/'FEM', 'FEM:Soul Exchange:soul exchange').
card_rarity('soul exchange'/'FEM', 'Uncommon').
card_artist('soul exchange'/'FEM', 'Anthony Waters').
card_multiverse_id('soul exchange'/'FEM', '1865').

card_in_set('spirit shield', 'FEM').
card_original_type('spirit shield'/'FEM', 'Artifact').
card_original_text('spirit shield'/'FEM', '{2}, {T}: Target creature gets +0/+2 as long as Spirit Shield remains tapped.\nYou may choose not to untap Spirit Shield as normal during your untap phase.').
card_first_print('spirit shield', 'FEM').
card_image_name('spirit shield'/'FEM', 'spirit shield').
card_uid('spirit shield'/'FEM', 'FEM:Spirit Shield:spirit shield').
card_rarity('spirit shield'/'FEM', 'Rare').
card_artist('spirit shield'/'FEM', 'Scott Kirschner').
card_flavor_text('spirit shield'/'FEM', 'At times, survival must outweigh all other considerations.').
card_multiverse_id('spirit shield'/'FEM', '1836').

card_in_set('spore cloud', 'FEM').
card_original_type('spore cloud'/'FEM', 'Instant').
card_original_text('spore cloud'/'FEM', 'Tap all blocking creatures. No creatures deal damage in combat this turn. Neither attacking nor blocking creatures untap as normal during their controllers\' next untap phase.').
card_first_print('spore cloud', 'FEM').
card_image_name('spore cloud'/'FEM', 'spore cloud1').
card_uid('spore cloud'/'FEM', 'FEM:Spore Cloud:spore cloud1').
card_rarity('spore cloud'/'FEM', 'Common').
card_artist('spore cloud'/'FEM', 'Susan Van Camp').
card_multiverse_id('spore cloud'/'FEM', '1920').

card_in_set('spore cloud', 'FEM').
card_original_type('spore cloud'/'FEM', 'Instant').
card_original_text('spore cloud'/'FEM', 'Tap all blocking creatures. No creatures deal damage in combat this turn. Neither attacking nor blocking creatures untap as normal during their controllers\' next untap phase.').
card_image_name('spore cloud'/'FEM', 'spore cloud2').
card_uid('spore cloud'/'FEM', 'FEM:Spore Cloud:spore cloud2').
card_rarity('spore cloud'/'FEM', 'Common').
card_artist('spore cloud'/'FEM', 'Amy Weber').
card_multiverse_id('spore cloud'/'FEM', '1922').

card_in_set('spore cloud', 'FEM').
card_original_type('spore cloud'/'FEM', 'Instant').
card_original_text('spore cloud'/'FEM', 'Tap all blocking creatures. No creatures deal damage in combat this turn. Neither attacking nor blocking creatures untap as normal during their controllers\' next untap phase.').
card_image_name('spore cloud'/'FEM', 'spore cloud3').
card_uid('spore cloud'/'FEM', 'FEM:Spore Cloud:spore cloud3').
card_rarity('spore cloud'/'FEM', 'Common').
card_artist('spore cloud'/'FEM', 'Jesper Myrfors').
card_multiverse_id('spore cloud'/'FEM', '1921').

card_in_set('spore flower', 'FEM').
card_original_type('spore flower'/'FEM', 'Summon — Fungus').
card_original_text('spore flower'/'FEM', 'During your upkeep, put a spore counter on Spore Flower.\n{0}: Remove three spore counters from Spore Flower. No creatures deal damage in combat this turn.').
card_first_print('spore flower', 'FEM').
card_image_name('spore flower'/'FEM', 'spore flower').
card_uid('spore flower'/'FEM', 'FEM:Spore Flower:spore flower').
card_rarity('spore flower'/'FEM', 'Uncommon').
card_artist('spore flower'/'FEM', 'Margaret Organ-Kean').
card_multiverse_id('spore flower'/'FEM', '1923').

card_in_set('svyelunite priest', 'FEM').
card_original_type('svyelunite priest'/'FEM', 'Summon — Merfolk').
card_original_text('svyelunite priest'/'FEM', '{U}{U}, {T}: Target creature may not be the target of spells or effects until end of turn. Use this ability only during your upkeep.').
card_first_print('svyelunite priest', 'FEM').
card_image_name('svyelunite priest'/'FEM', 'svyelunite priest').
card_uid('svyelunite priest'/'FEM', 'FEM:Svyelunite Priest:svyelunite priest').
card_rarity('svyelunite priest'/'FEM', 'Uncommon').
card_artist('svyelunite priest'/'FEM', 'Ron Spencer').
card_flavor_text('svyelunite priest'/'FEM', '\"Early Vodalians worshipped Svyelun, goddess of the Pearl Moon. Later she became a more abstract figure.\"\n—Sarpadian Empires, vol. V').
card_multiverse_id('svyelunite priest'/'FEM', '1890').

card_in_set('svyelunite temple', 'FEM').
card_original_type('svyelunite temple'/'FEM', 'Land').
card_original_text('svyelunite temple'/'FEM', 'Comes into play tapped.\n{T}: Add {U} to your mana pool.\n{T}: Sacrifice Svyelunite Temple to add {U}{U} to your mana pool.').
card_first_print('svyelunite temple', 'FEM').
card_image_name('svyelunite temple'/'FEM', 'svyelunite temple').
card_uid('svyelunite temple'/'FEM', 'FEM:Svyelunite Temple:svyelunite temple').
card_rarity('svyelunite temple'/'FEM', 'Uncommon').
card_artist('svyelunite temple'/'FEM', 'Mark Poole').
card_multiverse_id('svyelunite temple'/'FEM', '2013').

card_in_set('thallid', 'FEM').
card_original_type('thallid'/'FEM', 'Summon — Fungus').
card_original_text('thallid'/'FEM', 'During your upkeep, put a spore counter on Thallid.\n{0}: Remove three spore counters from Thallid to put a Saproling token into play. Treat this token as a 1/1 green creature.').
card_first_print('thallid', 'FEM').
card_image_name('thallid'/'FEM', 'thallid1').
card_uid('thallid'/'FEM', 'FEM:Thallid:thallid1').
card_rarity('thallid'/'FEM', 'Common').
card_artist('thallid'/'FEM', 'Edward P. Beard, Jr.').
card_multiverse_id('thallid'/'FEM', '1924').

card_in_set('thallid', 'FEM').
card_original_type('thallid'/'FEM', 'Summon — Fungus').
card_original_text('thallid'/'FEM', 'During your upkeep, put a spore counter on Thallid.\n{0}: Remove three spore counters from Thallid to put a Saproling token into play. Treat this token as a 1/1 green creature.').
card_image_name('thallid'/'FEM', 'thallid2').
card_uid('thallid'/'FEM', 'FEM:Thallid:thallid2').
card_rarity('thallid'/'FEM', 'Common').
card_artist('thallid'/'FEM', 'Jesper Myrfors').
card_multiverse_id('thallid'/'FEM', '1926').

card_in_set('thallid', 'FEM').
card_original_type('thallid'/'FEM', 'Summon — Fungus').
card_original_text('thallid'/'FEM', 'During your upkeep, put a spore counter on Thallid.\n{0}: Remove three spore counters from Thallid to put a Saproling token into play. Treat this token as a 1/1 green creature.').
card_image_name('thallid'/'FEM', 'thallid3').
card_uid('thallid'/'FEM', 'FEM:Thallid:thallid3').
card_rarity('thallid'/'FEM', 'Common').
card_artist('thallid'/'FEM', 'Ron Spencer').
card_multiverse_id('thallid'/'FEM', '1927').

card_in_set('thallid', 'FEM').
card_original_type('thallid'/'FEM', 'Summon — Fungus').
card_original_text('thallid'/'FEM', 'During your upkeep, put a spore counter on Thallid.\n{0}: Remove three spore counters from Thallid to put a Saproling token into play. Treat this token as a 1/1 green creature.').
card_image_name('thallid'/'FEM', 'thallid4').
card_uid('thallid'/'FEM', 'FEM:Thallid:thallid4').
card_rarity('thallid'/'FEM', 'Common').
card_artist('thallid'/'FEM', 'Daniel Gelon').
card_multiverse_id('thallid'/'FEM', '1925').

card_in_set('thallid devourer', 'FEM').
card_original_type('thallid devourer'/'FEM', 'Summon — Fungus').
card_original_text('thallid devourer'/'FEM', 'During your upkeep, put a spore counter on Thallid Devourer.\n{0}: Remove three spore counters from Thallid Devourer to put a Saproling token into play. Treat this token as a 1/1 green creature.\n{0}: Sacrifice a Saproling to give Thallid Devourer +1/+2 until end of turn.').
card_first_print('thallid devourer', 'FEM').
card_image_name('thallid devourer'/'FEM', 'thallid devourer').
card_uid('thallid devourer'/'FEM', 'FEM:Thallid Devourer:thallid devourer').
card_rarity('thallid devourer'/'FEM', 'Uncommon').
card_artist('thallid devourer'/'FEM', 'Ron Spencer').
card_multiverse_id('thallid devourer'/'FEM', '1928').

card_in_set('thelon\'s chant', 'FEM').
card_original_type('thelon\'s chant'/'FEM', 'Enchantment').
card_original_text('thelon\'s chant'/'FEM', 'During your upkeep, pay {G} or bury Thelon\'s Chant.\nWhenever a player puts a swamp into play, Thelon\'s Chant deals 3 damage to him or her unless that player puts a -1/-1 counter on a target creature he or she controls.').
card_first_print('thelon\'s chant', 'FEM').
card_image_name('thelon\'s chant'/'FEM', 'thelon\'s chant').
card_uid('thelon\'s chant'/'FEM', 'FEM:Thelon\'s Chant:thelon\'s chant').
card_rarity('thelon\'s chant'/'FEM', 'Uncommon').
card_artist('thelon\'s chant'/'FEM', 'Melissa A. Benson').
card_multiverse_id('thelon\'s chant'/'FEM', '1929').

card_in_set('thelon\'s curse', 'FEM').
card_original_type('thelon\'s curse'/'FEM', 'Enchantment').
card_original_text('thelon\'s curse'/'FEM', 'Blue creatures do not untap as normal during their controller\'s untap phase. During his or her upkeep, a blue creature\'s controller may pay an additional {U} to untap it. Each creature may be untapped in this way only once per turn.').
card_first_print('thelon\'s curse', 'FEM').
card_image_name('thelon\'s curse'/'FEM', 'thelon\'s curse').
card_uid('thelon\'s curse'/'FEM', 'FEM:Thelon\'s Curse:thelon\'s curse').
card_rarity('thelon\'s curse'/'FEM', 'Rare').
card_artist('thelon\'s curse'/'FEM', 'Pete Venters').
card_multiverse_id('thelon\'s curse'/'FEM', '1930').

card_in_set('thelonite druid', 'FEM').
card_original_type('thelonite druid'/'FEM', 'Summon — Cleric').
card_original_text('thelonite druid'/'FEM', '{1}{G}, {T}: Sacrifice a creature to turn all your forests into 2/3 creatures until end of turn. The forests still count as lands but may not be tapped for mana if they were brought into play this turn.').
card_first_print('thelonite druid', 'FEM').
card_image_name('thelonite druid'/'FEM', 'thelonite druid').
card_uid('thelonite druid'/'FEM', 'FEM:Thelonite Druid:thelonite druid').
card_rarity('thelonite druid'/'FEM', 'Uncommon').
card_artist('thelonite druid'/'FEM', 'Margaret Organ-Kean').
card_flavor_text('thelonite druid'/'FEM', '\"The magic at the heart of all living things can bear awe-inspiring fruit.\"\n—Kolevi of Havenwood, Elder Druid').
card_multiverse_id('thelonite druid'/'FEM', '1931').

card_in_set('thelonite monk', 'FEM').
card_original_type('thelonite monk'/'FEM', 'Summon — Cleric').
card_original_text('thelonite monk'/'FEM', '{T}: Sacrifice a green creature to turn a target land into a basic forest. Mark changed land with a counter.').
card_first_print('thelonite monk', 'FEM').
card_image_name('thelonite monk'/'FEM', 'thelonite monk').
card_uid('thelonite monk'/'FEM', 'FEM:Thelonite Monk:thelonite monk').
card_rarity('thelonite monk'/'FEM', 'Rare').
card_artist('thelonite monk'/'FEM', 'Bryon Wackwitz').
card_flavor_text('thelonite monk'/'FEM', '\"As the climate worsened, some Thelonites turned to fertilizing with fresh blood in an attempt to keep Havenwood alive and growing.\"\n—Sarpadian Empires, vol. III').
card_multiverse_id('thelonite monk'/'FEM', '1932').

card_in_set('thorn thallid', 'FEM').
card_original_type('thorn thallid'/'FEM', 'Summon — Fungus').
card_original_text('thorn thallid'/'FEM', 'During your upkeep, put a spore counter on Thorn Thallid.\n{0}: Remove three spore counters from Thorn Thallid to have it deal 1 damage to any target.').
card_first_print('thorn thallid', 'FEM').
card_image_name('thorn thallid'/'FEM', 'thorn thallid1').
card_uid('thorn thallid'/'FEM', 'FEM:Thorn Thallid:thorn thallid1').
card_rarity('thorn thallid'/'FEM', 'Common').
card_artist('thorn thallid'/'FEM', 'Daniel Gelon').
card_flavor_text('thorn thallid'/'FEM', '\"The danger in cultivating massive plants caught the elves by surprise.\"\n—Sarpadian Empires, vol. III').
card_multiverse_id('thorn thallid'/'FEM', '1933').

card_in_set('thorn thallid', 'FEM').
card_original_type('thorn thallid'/'FEM', 'Summon — Fungus').
card_original_text('thorn thallid'/'FEM', 'During your upkeep, put a spore counter on Thorn Thallid.\n{0}: Remove three spore counters from Thorn Thallid to have it deal 1 damage to any target.').
card_image_name('thorn thallid'/'FEM', 'thorn thallid2').
card_uid('thorn thallid'/'FEM', 'FEM:Thorn Thallid:thorn thallid2').
card_rarity('thorn thallid'/'FEM', 'Common').
card_artist('thorn thallid'/'FEM', 'Heather Hudson').
card_flavor_text('thorn thallid'/'FEM', '\"The cooling climate forced the Elves to experiment with new food sources.\"\n—Sarpadian Empires, vol. I').
card_multiverse_id('thorn thallid'/'FEM', '1934').

card_in_set('thorn thallid', 'FEM').
card_original_type('thorn thallid'/'FEM', 'Summon — Fungus').
card_original_text('thorn thallid'/'FEM', 'During your upkeep, put a spore counter on Thorn Thallid.\n{0}: Remove three spore counters from Thorn Thallid to have it deal 1 damage to any target.').
card_image_name('thorn thallid'/'FEM', 'thorn thallid3').
card_uid('thorn thallid'/'FEM', 'FEM:Thorn Thallid:thorn thallid3').
card_rarity('thorn thallid'/'FEM', 'Common').
card_artist('thorn thallid'/'FEM', 'Jesper Myrfors').
card_flavor_text('thorn thallid'/'FEM', '\"I don\'t know which is worse, getting hit with those darts or having to watch them grow back.\"\n—Orcish Soldier').
card_multiverse_id('thorn thallid'/'FEM', '1935').

card_in_set('thorn thallid', 'FEM').
card_original_type('thorn thallid'/'FEM', 'Summon — Fungus').
card_original_text('thorn thallid'/'FEM', 'During your upkeep, put a spore counter on Thorn Thallid.\n{0}: Remove three spore counters from Thorn Thallid to have it deal 1 damage to any target.').
card_image_name('thorn thallid'/'FEM', 'thorn thallid4').
card_uid('thorn thallid'/'FEM', 'FEM:Thorn Thallid:thorn thallid4').
card_rarity('thorn thallid'/'FEM', 'Common').
card_artist('thorn thallid'/'FEM', 'Mark Tedin').
card_flavor_text('thorn thallid'/'FEM', '\"Scholars still debate whether the Thallids were truly sentient.\"\n—Sarpadian Empires, vol. III').
card_multiverse_id('thorn thallid'/'FEM', '1936').

card_in_set('thrull champion', 'FEM').
card_original_type('thrull champion'/'FEM', 'Summon — Thrull').
card_original_text('thrull champion'/'FEM', 'All Thrulls get +1/+1.\n{T}: Take control of a target Thrull. You lose control of target Thrull if Thrull Champion leaves play or you lose control of Thrull Champion.').
card_first_print('thrull champion', 'FEM').
card_image_name('thrull champion'/'FEM', 'thrull champion').
card_uid('thrull champion'/'FEM', 'FEM:Thrull Champion:thrull champion').
card_rarity('thrull champion'/'FEM', 'Rare').
card_artist('thrull champion'/'FEM', 'Daniel Gelon').
card_flavor_text('thrull champion'/'FEM', '\"Those idiots should never have bred Thrulls for combat!\"\n—Jherana Rure').
card_multiverse_id('thrull champion'/'FEM', '1866').

card_in_set('thrull retainer', 'FEM').
card_original_type('thrull retainer'/'FEM', 'Enchant Creature').
card_original_text('thrull retainer'/'FEM', 'Target creature gets +1/+1.\nSacrifice Thrull Retainer to regenerate the creature it enchants.').
card_first_print('thrull retainer', 'FEM').
card_image_name('thrull retainer'/'FEM', 'thrull retainer').
card_uid('thrull retainer'/'FEM', 'FEM:Thrull Retainer:thrull retainer').
card_rarity('thrull retainer'/'FEM', 'Uncommon').
card_artist('thrull retainer'/'FEM', 'Ron Spencer').
card_flavor_text('thrull retainer'/'FEM', '\"Until the Rebellion, Thrulls served their masters faithfully—even at the cost of their own lives.\"\n—Sarpadian Empires, vol. II').
card_multiverse_id('thrull retainer'/'FEM', '1867').

card_in_set('thrull wizard', 'FEM').
card_original_type('thrull wizard'/'FEM', 'Summon — Thrull').
card_original_text('thrull wizard'/'FEM', '{1}{B} Counters a target black spell if caster of target spell does not pay an additional {B} or {3}. Play this ability as an interrupt.').
card_first_print('thrull wizard', 'FEM').
card_image_name('thrull wizard'/'FEM', 'thrull wizard').
card_uid('thrull wizard'/'FEM', 'FEM:Thrull Wizard:thrull wizard').
card_rarity('thrull wizard'/'FEM', 'Uncommon').
card_artist('thrull wizard'/'FEM', 'Anson Maddocks').
card_flavor_text('thrull wizard'/'FEM', '\"In crafting intelligent Thrulls to assist in sacrifices, Sahr inadvertantly set the stage for the Thrull Rebellion.\"\n—Sarpadian Empires, vol. II').
card_multiverse_id('thrull wizard'/'FEM', '1868').

card_in_set('tidal flats', 'FEM').
card_original_type('tidal flats'/'FEM', 'Enchantment').
card_original_text('tidal flats'/'FEM', '{U}{U} All your creatures that are blocking any non-flying creatures gain first strike until end of turn. The attacking player may pay {1} for each attacking creature to prevent Tidal Flats from giving that creature\'s blockers first strike.').
card_first_print('tidal flats', 'FEM').
card_image_name('tidal flats'/'FEM', 'tidal flats1').
card_uid('tidal flats'/'FEM', 'FEM:Tidal Flats:tidal flats1').
card_rarity('tidal flats'/'FEM', 'Common').
card_artist('tidal flats'/'FEM', 'Rob Alexander').
card_multiverse_id('tidal flats'/'FEM', '1891').

card_in_set('tidal flats', 'FEM').
card_original_type('tidal flats'/'FEM', 'Enchantment').
card_original_text('tidal flats'/'FEM', '{U}{U} All your creatures that are blocking any non-flying creatures gain first strike until end of turn. The attacking player may pay {1} for each attacking creature to prevent Tidal Flats from giving that creature\'s blockers first strike.').
card_image_name('tidal flats'/'FEM', 'tidal flats2').
card_uid('tidal flats'/'FEM', 'FEM:Tidal Flats:tidal flats2').
card_rarity('tidal flats'/'FEM', 'Common').
card_artist('tidal flats'/'FEM', 'Rob Alexander').
card_multiverse_id('tidal flats'/'FEM', '1892').

card_in_set('tidal flats', 'FEM').
card_original_type('tidal flats'/'FEM', 'Enchantment').
card_original_text('tidal flats'/'FEM', '{U}{U} All your creatures that are blocking any non-flying creatures gain first strike until end of turn. The attacking player may pay {1} for each attacking creature to prevent Tidal Flats from giving that creature\'s blockers first strike.').
card_image_name('tidal flats'/'FEM', 'tidal flats3').
card_uid('tidal flats'/'FEM', 'FEM:Tidal Flats:tidal flats3').
card_rarity('tidal flats'/'FEM', 'Common').
card_artist('tidal flats'/'FEM', 'Sandra Everingham').
card_multiverse_id('tidal flats'/'FEM', '1893').

card_in_set('tidal influence', 'FEM').
card_original_type('tidal influence'/'FEM', 'Enchantment').
card_original_text('tidal influence'/'FEM', 'Put a tide counter on Tidal Influence when it is brought into play and during your upkeep. If there is one tide counter on Tidal Influence, all blue creatures get -2/-0. If there are three tide counters on Tidal Influence, all blue creatures get +2/+0. When there are four tide counters on Tidal Influence, remove them all.\nYou may not cast Tidal Influence if there is another Tidal Influence in play.').
card_first_print('tidal influence', 'FEM').
card_image_name('tidal influence'/'FEM', 'tidal influence').
card_uid('tidal influence'/'FEM', 'FEM:Tidal Influence:tidal influence').
card_rarity('tidal influence'/'FEM', 'Uncommon').
card_artist('tidal influence'/'FEM', 'Tom Wänerstrand').
card_multiverse_id('tidal influence'/'FEM', '1894').

card_in_set('tourach\'s chant', 'FEM').
card_original_type('tourach\'s chant'/'FEM', 'Enchantment').
card_original_text('tourach\'s chant'/'FEM', 'During your upkeep, pay {B} or bury Tourach\'s Chant.\nWhenever a player puts a forest into play, Tourach\'s Chant deals 3 damage to him or her unless that player puts a -1/-1 counter on a target creature he or she controls.').
card_first_print('tourach\'s chant', 'FEM').
card_image_name('tourach\'s chant'/'FEM', 'tourach\'s chant').
card_uid('tourach\'s chant'/'FEM', 'FEM:Tourach\'s Chant:tourach\'s chant').
card_rarity('tourach\'s chant'/'FEM', 'Uncommon').
card_artist('tourach\'s chant'/'FEM', 'Richard Kane Ferguson').
card_multiverse_id('tourach\'s chant'/'FEM', '1869').

card_in_set('tourach\'s gate', 'FEM').
card_original_type('tourach\'s gate'/'FEM', 'Enchant Land').
card_original_text('tourach\'s gate'/'FEM', 'Can only be played on a target land you control. Sacrifice a Thrull to put three time counters on Tourach\'s Gate. During your upkeep, remove a time counter from Tourach\'s Gate. If there are no time counters on Tourach\'s Gate, bury it.\n{0}: Tap land Tourach\'s Gate enchants. All attacking creatures you control get +2/-1 until end of turn.').
card_first_print('tourach\'s gate', 'FEM').
card_image_name('tourach\'s gate'/'FEM', 'tourach\'s gate').
card_uid('tourach\'s gate'/'FEM', 'FEM:Tourach\'s Gate:tourach\'s gate').
card_rarity('tourach\'s gate'/'FEM', 'Rare').
card_artist('tourach\'s gate'/'FEM', 'Sandra Everingham').
card_multiverse_id('tourach\'s gate'/'FEM', '1870').

card_in_set('vodalian knights', 'FEM').
card_original_type('vodalian knights'/'FEM', 'Summon — Merfolk').
card_original_text('vodalian knights'/'FEM', 'First strike\n{U} Flying until end of turn\nVodalian Knights may not attack unless opponent controls at least one island. Bury Vodalian Knights if you control no islands.').
card_first_print('vodalian knights', 'FEM').
card_image_name('vodalian knights'/'FEM', 'vodalian knights').
card_uid('vodalian knights'/'FEM', 'FEM:Vodalian Knights:vodalian knights').
card_rarity('vodalian knights'/'FEM', 'Rare').
card_artist('vodalian knights'/'FEM', 'Susan Van Camp').
card_flavor_text('vodalian knights'/'FEM', 'Fear the Knight leaping from the water into the air, weapon ready.').
card_multiverse_id('vodalian knights'/'FEM', '1895').

card_in_set('vodalian mage', 'FEM').
card_original_type('vodalian mage'/'FEM', 'Summon — Merfolk').
card_original_text('vodalian mage'/'FEM', '{U}, {T}: Counters a target spell if caster of target spell does not pay an additional {1}. Play this ability as an interrupt.').
card_first_print('vodalian mage', 'FEM').
card_image_name('vodalian mage'/'FEM', 'vodalian mage1').
card_uid('vodalian mage'/'FEM', 'FEM:Vodalian Mage:vodalian mage1').
card_rarity('vodalian mage'/'FEM', 'Common').
card_artist('vodalian mage'/'FEM', 'Susan Van Camp').
card_flavor_text('vodalian mage'/'FEM', '\"Vodalian Mages are remarkable. Their merchants bring them arcane lore and devices from across the seas.\"\n—Lydia Wynforth, Mayor of Trokair').
card_multiverse_id('vodalian mage'/'FEM', '1896').

card_in_set('vodalian mage', 'FEM').
card_original_type('vodalian mage'/'FEM', 'Summon — Merfolk').
card_original_text('vodalian mage'/'FEM', '{U}, {T}: Counters a target spell if caster of target spell does not pay an additional {1}. Play this ability as an interrupt.').
card_image_name('vodalian mage'/'FEM', 'vodalian mage2').
card_uid('vodalian mage'/'FEM', 'FEM:Vodalian Mage:vodalian mage2').
card_rarity('vodalian mage'/'FEM', 'Common').
card_artist('vodalian mage'/'FEM', 'Mark Poole').
card_flavor_text('vodalian mage'/'FEM', '\"Come back, cowards! Everyone knows Merfolk can\'t wield magic!\"\n—Pashadar Dirf, Goblin Flotilla Commander, last words').
card_multiverse_id('vodalian mage'/'FEM', '1898').

card_in_set('vodalian mage', 'FEM').
card_original_type('vodalian mage'/'FEM', 'Summon — Merfolk').
card_original_text('vodalian mage'/'FEM', '{U}, {T}: Counters a target spell if caster of target spell does not pay an additional {1}. Play this ability as an interrupt.').
card_image_name('vodalian mage'/'FEM', 'vodalian mage3').
card_uid('vodalian mage'/'FEM', 'FEM:Vodalian Mage:vodalian mage3').
card_rarity('vodalian mage'/'FEM', 'Common').
card_artist('vodalian mage'/'FEM', 'Quinton Hoover').
card_flavor_text('vodalian mage'/'FEM', 'Vodalian Mages were invaluable in magical combat. Unfortunately, the Homarids raided with strength, numbers, and very little magic.').
card_multiverse_id('vodalian mage'/'FEM', '1897').

card_in_set('vodalian soldiers', 'FEM').
card_original_type('vodalian soldiers'/'FEM', 'Summon — Merfolk').
card_original_text('vodalian soldiers'/'FEM', '').
card_first_print('vodalian soldiers', 'FEM').
card_image_name('vodalian soldiers'/'FEM', 'vodalian soldiers1').
card_uid('vodalian soldiers'/'FEM', 'FEM:Vodalian Soldiers:vodalian soldiers1').
card_rarity('vodalian soldiers'/'FEM', 'Common').
card_artist('vodalian soldiers'/'FEM', 'Melissa A. Benson').
card_flavor_text('vodalian soldiers'/'FEM', '\"Vodalian Soldiers had some unique advantages. Often they would ride into battle on war machines rumored to have come from the far northern oceans.\"\n—Sarpadian Empires, vol. V').
card_multiverse_id('vodalian soldiers'/'FEM', '1899').

card_in_set('vodalian soldiers', 'FEM').
card_original_type('vodalian soldiers'/'FEM', 'Summon — Merfolk').
card_original_text('vodalian soldiers'/'FEM', '').
card_image_name('vodalian soldiers'/'FEM', 'vodalian soldiers2').
card_uid('vodalian soldiers'/'FEM', 'FEM:Vodalian Soldiers:vodalian soldiers2').
card_rarity('vodalian soldiers'/'FEM', 'Common').
card_artist('vodalian soldiers'/'FEM', 'Jeff A. Menges').
card_flavor_text('vodalian soldiers'/'FEM', '\"You think you know everything there is to know about battle? You know Orc droppings Underwater combat is three dimensional. Those thrice-damned Vodalians don\'t attack in ranks; they attack in schools.\"\n—Ivra Jursdotter').
card_multiverse_id('vodalian soldiers'/'FEM', '1901').

card_in_set('vodalian soldiers', 'FEM').
card_original_type('vodalian soldiers'/'FEM', 'Summon — Merfolk').
card_original_text('vodalian soldiers'/'FEM', '').
card_image_name('vodalian soldiers'/'FEM', 'vodalian soldiers3').
card_uid('vodalian soldiers'/'FEM', 'FEM:Vodalian Soldiers:vodalian soldiers3').
card_rarity('vodalian soldiers'/'FEM', 'Common').
card_artist('vodalian soldiers'/'FEM', 'Richard Kane Ferguson').
card_flavor_text('vodalian soldiers'/'FEM', '\"Stalwart Soldiers, you stand against the Homarids for Vodalia and for honor. Your homeland cries out for protection, and the moment has come. Now let your valor shine, and sound the cry to battle. Victory to Vodalia\"\n—Marshall Volnikov').
card_multiverse_id('vodalian soldiers'/'FEM', '1900').

card_in_set('vodalian soldiers', 'FEM').
card_original_type('vodalian soldiers'/'FEM', 'Summon — Merfolk').
card_original_text('vodalian soldiers'/'FEM', '').
card_image_name('vodalian soldiers'/'FEM', 'vodalian soldiers4').
card_uid('vodalian soldiers'/'FEM', 'FEM:Vodalian Soldiers:vodalian soldiers4').
card_rarity('vodalian soldiers'/'FEM', 'Common').
card_artist('vodalian soldiers'/'FEM', 'Susan Van Camp').
card_flavor_text('vodalian soldiers'/'FEM', '\"The cooling climate introduced a new threat to Vodalia: the Homarids. Once a minor nuisance, they thrived in the changing environment. Their ceaseless attacks strained Vodalia\'s defenses to their limit, eventually overwhelming the empire.\"\n—Sarpadian Empires, vol. V').
card_multiverse_id('vodalian soldiers'/'FEM', '1902').

card_in_set('vodalian war machine', 'FEM').
card_original_type('vodalian war machine'/'FEM', 'Summon — Wall').
card_original_text('vodalian war machine'/'FEM', '{0}: Tap target Merfolk you control to allow Vodalian War Machine to attack this turn or to give Vodalian War Machine +2/+1 until end of turn. If Vodalian War Machine is put in the graveyard, all Merfolk tapped in this manner this turn are destroyed.').
card_first_print('vodalian war machine', 'FEM').
card_image_name('vodalian war machine'/'FEM', 'vodalian war machine').
card_uid('vodalian war machine'/'FEM', 'FEM:Vodalian War Machine:vodalian war machine').
card_rarity('vodalian war machine'/'FEM', 'Rare').
card_artist('vodalian war machine'/'FEM', 'Amy Weber').
card_multiverse_id('vodalian war machine'/'FEM', '1903').

card_in_set('zelyon sword', 'FEM').
card_original_type('zelyon sword'/'FEM', 'Artifact').
card_original_text('zelyon sword'/'FEM', '{3}, {T}: Target creature gets +2/+0 as long as Zelyon Sword remains tapped.\nYou may choose not to untap Zelyon Sword as normal during your untap phase.').
card_first_print('zelyon sword', 'FEM').
card_image_name('zelyon sword'/'FEM', 'zelyon sword').
card_uid('zelyon sword'/'FEM', 'FEM:Zelyon Sword:zelyon sword').
card_rarity('zelyon sword'/'FEM', 'Rare').
card_artist('zelyon sword'/'FEM', 'Scott Kirschner').
card_flavor_text('zelyon sword'/'FEM', 'No sheath shall hold what finds its home in flesh.').
card_multiverse_id('zelyon sword'/'FEM', '1837').
