% Media Inserts

set('pMEI').
set_name('pMEI', 'Media Inserts').
set_release_date('pMEI', '1995-01-01').
set_border('pMEI', 'black').
set_type('pMEI', 'promo').

card_in_set('acquire', 'pMEI').
card_original_type('acquire'/'pMEI', 'Sorcery').
card_original_text('acquire'/'pMEI', '').
card_first_print('acquire', 'pMEI').
card_image_name('acquire'/'pMEI', 'acquire').
card_uid('acquire'/'pMEI', 'pMEI:Acquire:acquire').
card_rarity('acquire'/'pMEI', 'Special').
card_artist('acquire'/'pMEI', 'Anthony Francisco').
card_number('acquire'/'pMEI', '83').
card_flavor_text('acquire'/'pMEI', '\"Ownership\' is such a fray area.\"').

card_in_set('aeronaut tinkerer', 'pMEI').
card_original_type('aeronaut tinkerer'/'pMEI', 'Creature — Human Artificer').
card_original_text('aeronaut tinkerer'/'pMEI', '').
card_first_print('aeronaut tinkerer', 'pMEI').
card_image_name('aeronaut tinkerer'/'pMEI', 'aeronaut tinkerer').
card_uid('aeronaut tinkerer'/'pMEI', 'pMEI:Aeronaut Tinkerer:aeronaut tinkerer').
card_rarity('aeronaut tinkerer'/'pMEI', 'Special').
card_artist('aeronaut tinkerer'/'pMEI', 'William Murai').
card_number('aeronaut tinkerer'/'pMEI', '118').
card_flavor_text('aeronaut tinkerer'/'pMEI', '\"All tinkerers have their heads in the clouds. I don\'t intend to stop there.\"').

card_in_set('ajani steadfast', 'pMEI').
card_original_type('ajani steadfast'/'pMEI', 'Planeswalker — Ajani').
card_original_text('ajani steadfast'/'pMEI', '').
card_first_print('ajani steadfast', 'pMEI').
card_image_name('ajani steadfast'/'pMEI', 'ajani steadfast').
card_uid('ajani steadfast'/'pMEI', 'pMEI:Ajani Steadfast:ajani steadfast').
card_rarity('ajani steadfast'/'pMEI', 'Special').
card_artist('ajani steadfast'/'pMEI', 'Jae Lee').
card_number('ajani steadfast'/'pMEI', '99').

card_in_set('ajani, caller of the pride', 'pMEI').
card_original_type('ajani, caller of the pride'/'pMEI', 'Planeswalker — Ajani').
card_original_text('ajani, caller of the pride'/'pMEI', '').
card_first_print('ajani, caller of the pride', 'pMEI').
card_image_name('ajani, caller of the pride'/'pMEI', 'ajani, caller of the pride').
card_uid('ajani, caller of the pride'/'pMEI', 'pMEI:Ajani, Caller of the Pride:ajani, caller of the pride').
card_rarity('ajani, caller of the pride'/'pMEI', 'Special').
card_artist('ajani, caller of the pride'/'pMEI', 'Steve Prescott').
card_number('ajani, caller of the pride'/'pMEI', '72').

card_in_set('angel of glory\'s rise', 'pMEI').
card_original_type('angel of glory\'s rise'/'pMEI', 'Creature — Angel').
card_original_text('angel of glory\'s rise'/'pMEI', '').
card_first_print('angel of glory\'s rise', 'pMEI').
card_image_name('angel of glory\'s rise'/'pMEI', 'angel of glory\'s rise').
card_uid('angel of glory\'s rise'/'pMEI', 'pMEI:Angel of Glory\'s Rise:angel of glory\'s rise').
card_rarity('angel of glory\'s rise'/'pMEI', 'Special').
card_artist('angel of glory\'s rise'/'pMEI', 'James Ryman').
card_number('angel of glory\'s rise'/'pMEI', '59').
card_flavor_text('angel of glory\'s rise'/'pMEI', '\"Justice isn\'t done until undeath is undone.\"').

card_in_set('angelic skirmisher', 'pMEI').
card_original_type('angelic skirmisher'/'pMEI', 'Creature — Angel').
card_original_text('angelic skirmisher'/'pMEI', '').
card_first_print('angelic skirmisher', 'pMEI').
card_image_name('angelic skirmisher'/'pMEI', 'angelic skirmisher').
card_uid('angelic skirmisher'/'pMEI', 'pMEI:Angelic Skirmisher:angelic skirmisher').
card_rarity('angelic skirmisher'/'pMEI', 'Special').
card_artist('angelic skirmisher'/'pMEI', 'David Rapoza').
card_number('angelic skirmisher'/'pMEI', '90').
card_flavor_text('angelic skirmisher'/'pMEI', '\"A keen mind wields an army as a deft hand wields a sword.\"').

card_in_set('ankle shanker', 'pMEI').
card_original_type('ankle shanker'/'pMEI', 'Creature — Goblin Berserker').
card_original_text('ankle shanker'/'pMEI', '').
card_first_print('ankle shanker', 'pMEI').
card_image_name('ankle shanker'/'pMEI', 'ankle shanker').
card_uid('ankle shanker'/'pMEI', 'pMEI:Ankle Shanker:ankle shanker').
card_rarity('ankle shanker'/'pMEI', 'Special').
card_artist('ankle shanker'/'pMEI', 'Steven Belledin').
card_number('ankle shanker'/'pMEI', '93').
card_flavor_text('ankle shanker'/'pMEI', 'The stature of the fighter matters less than the depth of the cut.').

card_in_set('arashin sovereign', 'pMEI').
card_original_type('arashin sovereign'/'pMEI', 'Creature — Dragon').
card_original_text('arashin sovereign'/'pMEI', '').
card_first_print('arashin sovereign', 'pMEI').
card_image_name('arashin sovereign'/'pMEI', 'arashin sovereign').
card_uid('arashin sovereign'/'pMEI', 'pMEI:Arashin Sovereign:arashin sovereign').
card_rarity('arashin sovereign'/'pMEI', 'Special').
card_artist('arashin sovereign'/'pMEI', 'David Palumbo').
card_number('arashin sovereign'/'pMEI', '112').
card_flavor_text('arashin sovereign'/'pMEI', 'Dromoka dragons foster trust among their subjects, while the other clans must spend their time quelling rebellion.').

card_in_set('archfiend of depravity', 'pMEI').
card_original_type('archfiend of depravity'/'pMEI', 'Creature — Demon').
card_original_text('archfiend of depravity'/'pMEI', '').
card_first_print('archfiend of depravity', 'pMEI').
card_image_name('archfiend of depravity'/'pMEI', 'archfiend of depravity').
card_uid('archfiend of depravity'/'pMEI', 'pMEI:Archfiend of Depravity:archfiend of depravity').
card_rarity('archfiend of depravity'/'pMEI', 'Special').
card_artist('archfiend of depravity'/'pMEI', 'Volkan Baga').
card_number('archfiend of depravity'/'pMEI', '109').
card_flavor_text('archfiend of depravity'/'pMEI', '\"Why would I kill you all? Who then would be left to worship me?\"').

card_in_set('arena', 'pMEI').
card_original_type('arena'/'pMEI', 'Land').
card_original_text('arena'/'pMEI', '').
card_first_print('arena', 'pMEI').
card_image_name('arena'/'pMEI', 'arena').
card_uid('arena'/'pMEI', 'pMEI:Arena:arena').
card_rarity('arena'/'pMEI', 'Special').
card_artist('arena'/'pMEI', 'Rob Alexander').
card_number('arena'/'pMEI', '1').
card_multiverse_id('arena'/'pMEI', '97042').

card_in_set('arrest', 'pMEI').
card_original_type('arrest'/'pMEI', 'Enchantment — Aura').
card_original_text('arrest'/'pMEI', '').
card_first_print('arrest', 'pMEI').
card_image_name('arrest'/'pMEI', 'arrest').
card_uid('arrest'/'pMEI', 'pMEI:Arrest:arrest').
card_rarity('arrest'/'pMEI', 'Special').
card_artist('arrest'/'pMEI', 'Christopher Moeller').
card_number('arrest'/'pMEI', '53').
card_flavor_text('arrest'/'pMEI', 'Fayden didn\'t know if the giant had created the spehere\'s magic—only that he had to escape it to find Sifa.').

card_in_set('avalanche tusker', 'pMEI').
card_original_type('avalanche tusker'/'pMEI', 'Creature — Elephant Warrior').
card_original_text('avalanche tusker'/'pMEI', '').
card_first_print('avalanche tusker', 'pMEI').
card_image_name('avalanche tusker'/'pMEI', 'avalanche tusker').
card_uid('avalanche tusker'/'pMEI', 'pMEI:Avalanche Tusker:avalanche tusker').
card_rarity('avalanche tusker'/'pMEI', 'Special').
card_artist('avalanche tusker'/'pMEI', 'Eric Deschamps').
card_number('avalanche tusker'/'pMEI', '94').
card_flavor_text('avalanche tusker'/'pMEI', '\"Hold the high ground, then bring it to your enemy\"\n—Surrak, khan of the Temur').

card_in_set('birds of paradise', 'pMEI').
card_original_type('birds of paradise'/'pMEI', 'Creature — Bird').
card_original_text('birds of paradise'/'pMEI', '').
card_image_name('birds of paradise'/'pMEI', 'birds of paradise').
card_uid('birds of paradise'/'pMEI', 'pMEI:Birds of Paradise:birds of paradise').
card_rarity('birds of paradise'/'pMEI', 'Special').
card_artist('birds of paradise'/'pMEI', 'Terese Nielsen').
card_number('birds of paradise'/'pMEI', '28').
card_flavor_text('birds of paradise'/'pMEI', '\"The gods used their feathers to paint all the colors of the world.\"\n—Yare-Tiva, warden of Gramur forest').

card_in_set('bloodthrone vampire', 'pMEI').
card_original_type('bloodthrone vampire'/'pMEI', 'Creature — Vampire').
card_original_text('bloodthrone vampire'/'pMEI', '').
card_first_print('bloodthrone vampire', 'pMEI').
card_image_name('bloodthrone vampire'/'pMEI', 'bloodthrone vampire').
card_uid('bloodthrone vampire'/'pMEI', 'pMEI:Bloodthrone Vampire:bloodthrone vampire').
card_rarity('bloodthrone vampire'/'pMEI', 'Special').
card_artist('bloodthrone vampire'/'pMEI', 'Steve Argyle').
card_number('bloodthrone vampire'/'pMEI', '31').
card_flavor_text('bloodthrone vampire'/'pMEI', 'www.MagicTheGathering.com').

card_in_set('blue elemental blast', 'pMEI').
card_original_type('blue elemental blast'/'pMEI', 'Instant').
card_original_text('blue elemental blast'/'pMEI', '').
card_border('blue elemental blast'/'pMEI', 'white').
card_image_name('blue elemental blast'/'pMEI', 'blue elemental blast').
card_uid('blue elemental blast'/'pMEI', 'pMEI:Blue Elemental Blast:blue elemental blast').
card_rarity('blue elemental blast'/'pMEI', 'Special').
card_artist('blue elemental blast'/'pMEI', 'Richard Thomas').
card_number('blue elemental blast'/'pMEI', '5').

card_in_set('boltwing marauder', 'pMEI').
card_original_type('boltwing marauder'/'pMEI', 'Creature — Dragon').
card_original_text('boltwing marauder'/'pMEI', '').
card_first_print('boltwing marauder', 'pMEI').
card_image_name('boltwing marauder'/'pMEI', 'boltwing marauder').
card_uid('boltwing marauder'/'pMEI', 'pMEI:Boltwing Marauder:boltwing marauder').
card_rarity('boltwing marauder'/'pMEI', 'Special').
card_artist('boltwing marauder'/'pMEI', 'Daarken').
card_number('boltwing marauder'/'pMEI', '115').
card_flavor_text('boltwing marauder'/'pMEI', 'When battling the Kolaghan, consider yourself lucky if lightning strikes the same place only twice.').

card_in_set('bonescythe sliver', 'pMEI').
card_original_type('bonescythe sliver'/'pMEI', 'Creature — Sliver').
card_original_text('bonescythe sliver'/'pMEI', '').
card_first_print('bonescythe sliver', 'pMEI').
card_image_name('bonescythe sliver'/'pMEI', 'bonescythe sliver').
card_uid('bonescythe sliver'/'pMEI', 'pMEI:Bonescythe Sliver:bonescythe sliver').
card_rarity('bonescythe sliver'/'pMEI', 'Special').
card_artist('bonescythe sliver'/'pMEI', 'Brad Rigney').
card_number('bonescythe sliver'/'pMEI', '68').
card_flavor_text('bonescythe sliver'/'pMEI', '\"Their appendages are sharper than our swords and quicker than our bows.\"\n—Hastric, Thunian scout').

card_in_set('breath of malfegor', 'pMEI').
card_original_type('breath of malfegor'/'pMEI', 'Instant').
card_original_text('breath of malfegor'/'pMEI', '').
card_first_print('breath of malfegor', 'pMEI').
card_image_name('breath of malfegor'/'pMEI', 'breath of malfegor').
card_uid('breath of malfegor'/'pMEI', 'pMEI:Breath of Malfegor:breath of malfegor').
card_rarity('breath of malfegor'/'pMEI', 'Special').
card_artist('breath of malfegor'/'pMEI', 'Alex Horley-Orlandelli').
card_number('breath of malfegor'/'pMEI', '58').
card_flavor_text('breath of malfegor'/'pMEI', 'The draconic demon loomed over Dack, and he suddenly sensed he wasn\'t the only Planeswalker that Malfegor had encountered.').

card_in_set('brion stoutarm', 'pMEI').
card_original_type('brion stoutarm'/'pMEI', 'Legendary Creature — Giant Warrior').
card_original_text('brion stoutarm'/'pMEI', '').
card_first_print('brion stoutarm', 'pMEI').
card_image_name('brion stoutarm'/'pMEI', 'brion stoutarm').
card_uid('brion stoutarm'/'pMEI', 'pMEI:Brion Stoutarm:brion stoutarm').
card_rarity('brion stoutarm'/'pMEI', 'Special').
card_artist('brion stoutarm'/'pMEI', 'Zoltan Boros & Gabor Szikszai').
card_number('brion stoutarm'/'pMEI', '17').

card_in_set('broodmate dragon', 'pMEI').
card_original_type('broodmate dragon'/'pMEI', 'Creature — Dragon').
card_original_text('broodmate dragon'/'pMEI', '').
card_first_print('broodmate dragon', 'pMEI').
card_image_name('broodmate dragon'/'pMEI', 'broodmate dragon').
card_uid('broodmate dragon'/'pMEI', 'pMEI:Broodmate Dragon:broodmate dragon').
card_rarity('broodmate dragon'/'pMEI', 'Special').
card_artist('broodmate dragon'/'pMEI', 'Vance Kovacs').
card_number('broodmate dragon'/'pMEI', '19').
card_flavor_text('broodmate dragon'/'pMEI', 'Frozen in fear, the goblins stared upward at the circling hunter—and were promptly eaten by its diving mate.').

card_in_set('cathedral of war', 'pMEI').
card_original_type('cathedral of war'/'pMEI', 'Land').
card_original_text('cathedral of war'/'pMEI', '').
card_first_print('cathedral of war', 'pMEI').
card_image_name('cathedral of war'/'pMEI', 'cathedral of war').
card_uid('cathedral of war'/'pMEI', 'pMEI:Cathedral of War:cathedral of war').
card_rarity('cathedral of war'/'pMEI', 'Special').
card_artist('cathedral of war'/'pMEI', 'Franz Vohwinkel').
card_number('cathedral of war'/'pMEI', '51').

card_in_set('celestial colonnade', 'pMEI').
card_original_type('celestial colonnade'/'pMEI', 'Land').
card_original_text('celestial colonnade'/'pMEI', '').
card_first_print('celestial colonnade', 'pMEI').
card_image_name('celestial colonnade'/'pMEI', 'celestial colonnade').
card_uid('celestial colonnade'/'pMEI', 'pMEI:Celestial Colonnade:celestial colonnade').
card_rarity('celestial colonnade'/'pMEI', 'Special').
card_artist('celestial colonnade'/'pMEI', 'Kekai Kotaki').
card_number('celestial colonnade'/'pMEI', '23').

card_in_set('chandra\'s fury', 'pMEI').
card_original_type('chandra\'s fury'/'pMEI', 'Instant').
card_original_text('chandra\'s fury'/'pMEI', '').
card_first_print('chandra\'s fury', 'pMEI').
card_image_name('chandra\'s fury'/'pMEI', 'chandra\'s fury').
card_uid('chandra\'s fury'/'pMEI', 'pMEI:Chandra\'s Fury:chandra\'s fury').
card_rarity('chandra\'s fury'/'pMEI', 'Special').
card_artist('chandra\'s fury'/'pMEI', 'Volkan Baga').
card_number('chandra\'s fury'/'pMEI', '65').
card_flavor_text('chandra\'s fury'/'pMEI', 'www.MagicTheGathering.com').

card_in_set('chandra\'s phoenix', 'pMEI').
card_original_type('chandra\'s phoenix'/'pMEI', 'Creature — Phoenix').
card_original_text('chandra\'s phoenix'/'pMEI', '').
card_first_print('chandra\'s phoenix', 'pMEI').
card_image_name('chandra\'s phoenix'/'pMEI', 'chandra\'s phoenix').
card_uid('chandra\'s phoenix'/'pMEI', 'pMEI:Chandra\'s Phoenix:chandra\'s phoenix').
card_rarity('chandra\'s phoenix'/'pMEI', 'Special').
card_artist('chandra\'s phoenix'/'pMEI', 'Steve Argyle').
card_number('chandra\'s phoenix'/'pMEI', '37').

card_in_set('chandra, pyromaster', 'pMEI').
card_original_type('chandra, pyromaster'/'pMEI', 'Planeswalker — Chandra').
card_original_text('chandra, pyromaster'/'pMEI', '').
card_first_print('chandra, pyromaster', 'pMEI').
card_image_name('chandra, pyromaster'/'pMEI', 'chandra, pyromaster1').
card_uid('chandra, pyromaster'/'pMEI', 'pMEI:Chandra, Pyromaster:chandra, pyromaster1').
card_rarity('chandra, pyromaster'/'pMEI', 'Special').
card_artist('chandra, pyromaster'/'pMEI', 'Jae Lee').
card_number('chandra, pyromaster'/'pMEI', '102').

card_in_set('chandra, pyromaster', 'pMEI').
card_original_type('chandra, pyromaster'/'pMEI', 'Planeswalker — Chandra').
card_original_text('chandra, pyromaster'/'pMEI', '').
card_image_name('chandra, pyromaster'/'pMEI', 'chandra, pyromaster2').
card_uid('chandra, pyromaster'/'pMEI', 'pMEI:Chandra, Pyromaster:chandra, pyromaster2').
card_rarity('chandra, pyromaster'/'pMEI', 'Special').
card_artist('chandra, pyromaster'/'pMEI', 'Steve Prescott').
card_number('chandra, pyromaster'/'pMEI', '75').

card_in_set('consume spirit', 'pMEI').
card_original_type('consume spirit'/'pMEI', 'Sorcery').
card_original_text('consume spirit'/'pMEI', '').
card_first_print('consume spirit', 'pMEI').
card_image_name('consume spirit'/'pMEI', 'consume spirit').
card_uid('consume spirit'/'pMEI', 'pMEI:Consume Spirit:consume spirit').
card_rarity('consume spirit'/'pMEI', 'Special').
card_artist('consume spirit'/'pMEI', 'Dan Scott').
card_number('consume spirit'/'pMEI', '54').
card_flavor_text('consume spirit'/'pMEI', '\"You clerics brag about your strength of spirit, yet I take away the tiniest fragment, and you crumple like a rag doll.\"\n—Sorin Markov').

card_in_set('corrupt', 'pMEI').
card_original_type('corrupt'/'pMEI', 'Sorcery').
card_original_text('corrupt'/'pMEI', '').
card_first_print('corrupt', 'pMEI').
card_image_name('corrupt'/'pMEI', 'corrupt').
card_uid('corrupt'/'pMEI', 'pMEI:Corrupt:corrupt').
card_rarity('corrupt'/'pMEI', 'Special').
card_artist('corrupt'/'pMEI', 'Alex Horley-Orlandelli').
card_number('corrupt'/'pMEI', '64').

card_in_set('day of judgment', 'pMEI').
card_original_type('day of judgment'/'pMEI', 'Sorcery').
card_original_text('day of judgment'/'pMEI', '').
card_first_print('day of judgment', 'pMEI').
card_image_name('day of judgment'/'pMEI', 'day of judgment').
card_uid('day of judgment'/'pMEI', 'pMEI:Day of Judgment:day of judgment').
card_rarity('day of judgment'/'pMEI', 'Special').
card_artist('day of judgment'/'pMEI', 'Kev Walker').
card_number('day of judgment'/'pMEI', '22').
card_flavor_text('day of judgment'/'pMEI', '\"I have seen planes leveled and all life rendered to dust. It brought no pleasure, even to a heart as dark as mine.\"\n—Sorin Markov').

card_in_set('devil\'s play', 'pMEI').
card_original_type('devil\'s play'/'pMEI', 'Sorcery').
card_original_text('devil\'s play'/'pMEI', '').
card_first_print('devil\'s play', 'pMEI').
card_image_name('devil\'s play'/'pMEI', 'devil\'s play').
card_uid('devil\'s play'/'pMEI', 'pMEI:Devil\'s Play:devil\'s play').
card_rarity('devil\'s play'/'pMEI', 'Special').
card_artist('devil\'s play'/'pMEI', 'Raymond Swanland').
card_number('devil\'s play'/'pMEI', '40').
card_flavor_text('devil\'s play'/'pMEI', 'A devil\'s hands are never idle.').

card_in_set('dragon fodder', 'pMEI').
card_original_type('dragon fodder'/'pMEI', 'Sorcery').
card_original_text('dragon fodder'/'pMEI', '').
card_first_print('dragon fodder', 'pMEI').
card_image_name('dragon fodder'/'pMEI', 'dragon fodder').
card_uid('dragon fodder'/'pMEI', 'pMEI:Dragon Fodder:dragon fodder').
card_rarity('dragon fodder'/'pMEI', 'Special').
card_artist('dragon fodder'/'pMEI', 'Jesper Ejsing').
card_number('dragon fodder'/'pMEI', '119').
card_flavor_text('dragon fodder'/'pMEI', 'Atarka goblins meet their demise as readily as their Temur counterparts did, but usually under big, winged shadows.').

card_in_set('dragonlord\'s servant', 'pMEI').
card_original_type('dragonlord\'s servant'/'pMEI', 'Creature — Goblin Shaman').
card_original_text('dragonlord\'s servant'/'pMEI', '').
card_first_print('dragonlord\'s servant', 'pMEI').
card_image_name('dragonlord\'s servant'/'pMEI', 'dragonlord\'s servant').
card_uid('dragonlord\'s servant'/'pMEI', 'pMEI:Dragonlord\'s Servant:dragonlord\'s servant').
card_rarity('dragonlord\'s servant'/'pMEI', 'Special').
card_artist('dragonlord\'s servant'/'pMEI', 'Carl Frank').
card_number('dragonlord\'s servant'/'pMEI', '120').
card_flavor_text('dragonlord\'s servant'/'pMEI', 'Atarka serving-goblins coat themselves with grease imbued with noxious herbs, hoping to discourage their ravenous masters from adding them to the meal.').

card_in_set('dragonscale general', 'pMEI').
card_original_type('dragonscale general'/'pMEI', 'Creature — Human Warrior').
card_original_text('dragonscale general'/'pMEI', '').
card_first_print('dragonscale general', 'pMEI').
card_image_name('dragonscale general'/'pMEI', 'dragonscale general').
card_uid('dragonscale general'/'pMEI', 'pMEI:Dragonscale General:dragonscale general').
card_rarity('dragonscale general'/'pMEI', 'Special').
card_artist('dragonscale general'/'pMEI', 'William Murai').
card_number('dragonscale general'/'pMEI', '107').
card_flavor_text('dragonscale general'/'pMEI', '\"Dragons seek war. I bring it to them.\"').

card_in_set('dreg mangler', 'pMEI').
card_original_type('dreg mangler'/'pMEI', 'Creature — Plant Zombie').
card_original_text('dreg mangler'/'pMEI', '').
card_first_print('dreg mangler', 'pMEI').
card_image_name('dreg mangler'/'pMEI', 'dreg mangler').
card_uid('dreg mangler'/'pMEI', 'pMEI:Dreg Mangler:dreg mangler').
card_rarity('dreg mangler'/'pMEI', 'Special').
card_artist('dreg mangler'/'pMEI', 'Svetlin Velinov').
card_number('dreg mangler'/'pMEI', '55').

card_in_set('duress', 'pMEI').
card_original_type('duress'/'pMEI', 'Sorcery').
card_original_text('duress'/'pMEI', '').
card_first_print('duress', 'pMEI').
card_image_name('duress'/'pMEI', 'duress').
card_uid('duress'/'pMEI', 'pMEI:Duress:duress').
card_rarity('duress'/'pMEI', 'Special').
card_artist('duress'/'pMEI', 'Michael Komarck').
card_number('duress'/'pMEI', '84').
card_flavor_text('duress'/'pMEI', '\"Your night names inspire me, Dack!\"').

card_in_set('eidolon of blossoms', 'pMEI').
card_original_type('eidolon of blossoms'/'pMEI', 'Enchantment Creature — Spirit').
card_original_text('eidolon of blossoms'/'pMEI', '').
card_first_print('eidolon of blossoms', 'pMEI').
card_image_name('eidolon of blossoms'/'pMEI', 'eidolon of blossoms').
card_uid('eidolon of blossoms'/'pMEI', 'pMEI:Eidolon of Blossoms:eidolon of blossoms').
card_rarity('eidolon of blossoms'/'pMEI', 'Special').
card_artist('eidolon of blossoms'/'pMEI', 'Seb McKinnon').
card_number('eidolon of blossoms'/'pMEI', '85').
card_flavor_text('eidolon of blossoms'/'pMEI', 'The emotional echoes of dryad gatherings attract lost souls.').

card_in_set('electrolyze', 'pMEI').
card_original_type('electrolyze'/'pMEI', 'Instant').
card_original_text('electrolyze'/'pMEI', '').
card_first_print('electrolyze', 'pMEI').
card_image_name('electrolyze'/'pMEI', 'electrolyze').
card_uid('electrolyze'/'pMEI', 'pMEI:Electrolyze:electrolyze').
card_rarity('electrolyze'/'pMEI', 'Special').
card_artist('electrolyze'/'pMEI', 'Karl Kopinski').
card_number('electrolyze'/'pMEI', '42').

card_in_set('evolving wilds', 'pMEI').
card_original_type('evolving wilds'/'pMEI', 'Land').
card_original_text('evolving wilds'/'pMEI', '').
card_first_print('evolving wilds', 'pMEI').
card_image_name('evolving wilds'/'pMEI', 'evolving wilds').
card_uid('evolving wilds'/'pMEI', 'pMEI:Evolving Wilds:evolving wilds').
card_rarity('evolving wilds'/'pMEI', 'Special').
card_artist('evolving wilds'/'pMEI', 'Noah Bradley').
card_number('evolving wilds'/'pMEI', '121').
card_flavor_text('evolving wilds'/'pMEI', 'The land is ever resilient. Should it die, it will be reborn.').

card_in_set('faithless looting', 'pMEI').
card_original_type('faithless looting'/'pMEI', 'Sorcery').
card_original_text('faithless looting'/'pMEI', '').
card_first_print('faithless looting', 'pMEI').
card_image_name('faithless looting'/'pMEI', 'faithless looting').
card_uid('faithless looting'/'pMEI', 'pMEI:Faithless Looting:faithless looting').
card_rarity('faithless looting'/'pMEI', 'Special').
card_artist('faithless looting'/'pMEI', 'Karl Kopinski').
card_number('faithless looting'/'pMEI', '39').
card_flavor_text('faithless looting'/'pMEI', '\"Avacyn has abandoned us! We have nothing left except what we can take!\"').

card_in_set('fated conflagration', 'pMEI').
card_original_type('fated conflagration'/'pMEI', 'Instant').
card_original_text('fated conflagration'/'pMEI', '').
card_first_print('fated conflagration', 'pMEI').
card_image_name('fated conflagration'/'pMEI', 'fated conflagration').
card_uid('fated conflagration'/'pMEI', 'pMEI:Fated Conflagration:fated conflagration').
card_rarity('fated conflagration'/'pMEI', 'Special').
card_artist('fated conflagration'/'pMEI', 'Clint Cearley').
card_number('fated conflagration'/'pMEI', '79').

card_in_set('feast of blood', 'pMEI').
card_original_type('feast of blood'/'pMEI', 'Sorcery').
card_original_text('feast of blood'/'pMEI', '').
card_first_print('feast of blood', 'pMEI').
card_image_name('feast of blood'/'pMEI', 'feast of blood').
card_uid('feast of blood'/'pMEI', 'pMEI:Feast of Blood:feast of blood').
card_rarity('feast of blood'/'pMEI', 'Special').
card_artist('feast of blood'/'pMEI', 'Christopher Moeller').
card_number('feast of blood'/'pMEI', '43').
card_flavor_text('feast of blood'/'pMEI', 'Imbued in the Ancient Fang were spells far darker than any Dack Fayden would cast.').

card_in_set('fireball', 'pMEI').
card_original_type('fireball'/'pMEI', 'Sorcery').
card_original_text('fireball'/'pMEI', '').
card_border('fireball'/'pMEI', 'white').
card_image_name('fireball'/'pMEI', 'fireball').
card_uid('fireball'/'pMEI', 'pMEI:Fireball:fireball').
card_rarity('fireball'/'pMEI', 'Special').
card_artist('fireball'/'pMEI', 'Mark Tedin').
card_number('fireball'/'pMEI', '4').

card_in_set('flamerush rider', 'pMEI').
card_original_type('flamerush rider'/'pMEI', 'Creature — Human Warrior').
card_original_text('flamerush rider'/'pMEI', '').
card_first_print('flamerush rider', 'pMEI').
card_image_name('flamerush rider'/'pMEI', 'flamerush rider').
card_uid('flamerush rider'/'pMEI', 'pMEI:Flamerush Rider:flamerush rider').
card_rarity('flamerush rider'/'pMEI', 'Special').
card_artist('flamerush rider'/'pMEI', 'Viktor Titov').
card_number('flamerush rider'/'pMEI', '110').

card_in_set('foe-razer regent', 'pMEI').
card_original_type('foe-razer regent'/'pMEI', 'Creature — Dragon').
card_original_text('foe-razer regent'/'pMEI', '').
card_first_print('foe-razer regent', 'pMEI').
card_image_name('foe-razer regent'/'pMEI', 'foe-razer regent').
card_uid('foe-razer regent'/'pMEI', 'pMEI:Foe-Razer Regent:foe-razer regent').
card_rarity('foe-razer regent'/'pMEI', 'Special').
card_artist('foe-razer regent'/'pMEI', 'Svetlin Velinov').
card_number('foe-razer regent'/'pMEI', '122').

card_in_set('frost titan', 'pMEI').
card_original_type('frost titan'/'pMEI', 'Creature — Giant').
card_original_text('frost titan'/'pMEI', '').
card_first_print('frost titan', 'pMEI').
card_image_name('frost titan'/'pMEI', 'frost titan').
card_uid('frost titan'/'pMEI', 'pMEI:Frost Titan:frost titan').
card_rarity('frost titan'/'pMEI', 'Special').
card_artist('frost titan'/'pMEI', 'Mike Sass').
card_number('frost titan'/'pMEI', '34').

card_in_set('garruk wildspeaker', 'pMEI').
card_original_type('garruk wildspeaker'/'pMEI', 'Planeswalker — Garruk').
card_original_text('garruk wildspeaker'/'pMEI', '').
card_first_print('garruk wildspeaker', 'pMEI').
card_image_name('garruk wildspeaker'/'pMEI', 'garruk wildspeaker').
card_uid('garruk wildspeaker'/'pMEI', 'pMEI:Garruk Wildspeaker:garruk wildspeaker').
card_rarity('garruk wildspeaker'/'pMEI', 'Special').
card_artist('garruk wildspeaker'/'pMEI', 'Nils Hamm').
card_number('garruk wildspeaker'/'pMEI', '16').

card_in_set('garruk, apex predator', 'pMEI').
card_original_type('garruk, apex predator'/'pMEI', 'Planeswalker — Garruk').
card_original_text('garruk, apex predator'/'pMEI', '').
card_first_print('garruk, apex predator', 'pMEI').
card_image_name('garruk, apex predator'/'pMEI', 'garruk, apex predator').
card_uid('garruk, apex predator'/'pMEI', 'pMEI:Garruk, Apex Predator:garruk, apex predator').
card_rarity('garruk, apex predator'/'pMEI', 'Special').
card_artist('garruk, apex predator'/'pMEI', 'Jae Lee').
card_number('garruk, apex predator'/'pMEI', '104').

card_in_set('garruk, caller of beasts', 'pMEI').
card_original_type('garruk, caller of beasts'/'pMEI', 'Planeswalker — Garruk').
card_original_text('garruk, caller of beasts'/'pMEI', '').
card_first_print('garruk, caller of beasts', 'pMEI').
card_image_name('garruk, caller of beasts'/'pMEI', 'garruk, caller of beasts').
card_uid('garruk, caller of beasts'/'pMEI', 'pMEI:Garruk, Caller of Beasts:garruk, caller of beasts').
card_rarity('garruk, caller of beasts'/'pMEI', 'Special').
card_artist('garruk, caller of beasts'/'pMEI', 'Steve Prescott').
card_number('garruk, caller of beasts'/'pMEI', '76').

card_in_set('gaze of granite', 'pMEI').
card_original_type('gaze of granite'/'pMEI', 'Sorcery').
card_original_text('gaze of granite'/'pMEI', '').
card_first_print('gaze of granite', 'pMEI').
card_image_name('gaze of granite'/'pMEI', 'gaze of granite').
card_uid('gaze of granite'/'pMEI', 'pMEI:Gaze of Granite:gaze of granite').
card_rarity('gaze of granite'/'pMEI', 'Special').
card_artist('gaze of granite'/'pMEI', 'Dan Scott').
card_number('gaze of granite'/'pMEI', '81').
card_flavor_text('gaze of granite'/'pMEI', '\"Show yourself, thief.\"').

card_in_set('giant badger', 'pMEI').
card_original_type('giant badger'/'pMEI', 'Creature — Badger').
card_original_text('giant badger'/'pMEI', '').
card_first_print('giant badger', 'pMEI').
card_image_name('giant badger'/'pMEI', 'giant badger').
card_uid('giant badger'/'pMEI', 'pMEI:Giant Badger:giant badger').
card_rarity('giant badger'/'pMEI', 'Special').
card_artist('giant badger'/'pMEI', 'Liz Danforth').
card_number('giant badger'/'pMEI', '8').
card_flavor_text('giant badger'/'pMEI', 'The wizard Greensleeves called a Giant Badger to her aid in an battle with the desert mage Karli.').
card_multiverse_id('giant badger'/'pMEI', '97045').

card_in_set('goblin rabblemaster', 'pMEI').
card_original_type('goblin rabblemaster'/'pMEI', 'Creature — Goblin Warrior').
card_original_text('goblin rabblemaster'/'pMEI', '').
card_first_print('goblin rabblemaster', 'pMEI').
card_image_name('goblin rabblemaster'/'pMEI', 'goblin rabblemaster').
card_uid('goblin rabblemaster'/'pMEI', 'pMEI:Goblin Rabblemaster:goblin rabblemaster').
card_rarity('goblin rabblemaster'/'pMEI', 'Special').
card_artist('goblin rabblemaster'/'pMEI', 'Zoltan Boros').
card_number('goblin rabblemaster'/'pMEI', '98').

card_in_set('grave titan', 'pMEI').
card_original_type('grave titan'/'pMEI', 'Creature — Giant').
card_original_text('grave titan'/'pMEI', '').
card_first_print('grave titan', 'pMEI').
card_image_name('grave titan'/'pMEI', 'grave titan').
card_uid('grave titan'/'pMEI', 'pMEI:Grave Titan:grave titan').
card_rarity('grave titan'/'pMEI', 'Special').
card_artist('grave titan'/'pMEI', 'Lucas Graciano').
card_number('grave titan'/'pMEI', '35').
card_flavor_text('grave titan'/'pMEI', 'Death in form and function.').

card_in_set('gravecrawler', 'pMEI').
card_original_type('gravecrawler'/'pMEI', 'Creature — Zombie').
card_original_text('gravecrawler'/'pMEI', '').
card_first_print('gravecrawler', 'pMEI').
card_image_name('gravecrawler'/'pMEI', 'gravecrawler').
card_uid('gravecrawler'/'pMEI', 'pMEI:Gravecrawler:gravecrawler').
card_rarity('gravecrawler'/'pMEI', 'Special').
card_artist('gravecrawler'/'pMEI', 'Dave Kendall').
card_number('gravecrawler'/'pMEI', '41').
card_flavor_text('gravecrawler'/'pMEI', '\"Innistrad\'s ghoulcallers are talented enough, but let me show you what someone with real power can create.\"\n—Liliana Vess').

card_in_set('guul draz assassin', 'pMEI').
card_original_type('guul draz assassin'/'pMEI', 'Creature — Vampire Assassin').
card_original_text('guul draz assassin'/'pMEI', '').
card_first_print('guul draz assassin', 'pMEI').
card_image_name('guul draz assassin'/'pMEI', 'guul draz assassin').
card_uid('guul draz assassin'/'pMEI', 'pMEI:Guul Draz Assassin:guul draz assassin').
card_rarity('guul draz assassin'/'pMEI', 'Special').
card_artist('guul draz assassin'/'pMEI', 'Jason A. Engle').
card_number('guul draz assassin'/'pMEI', '26').

card_in_set('hamletback goliath', 'pMEI').
card_original_type('hamletback goliath'/'pMEI', 'Creature — Giant Warrior').
card_original_text('hamletback goliath'/'pMEI', '').
card_first_print('hamletback goliath', 'pMEI').
card_image_name('hamletback goliath'/'pMEI', 'hamletback goliath').
card_uid('hamletback goliath'/'pMEI', 'pMEI:Hamletback Goliath:hamletback goliath').
card_rarity('hamletback goliath'/'pMEI', 'Special').
card_artist('hamletback goliath'/'pMEI', 'Paolo Parente & Brian Snõddy').
card_number('hamletback goliath'/'pMEI', '71').
card_flavor_text('hamletback goliath'/'pMEI', '\"If you live on a giant\'s back, there\'s only one individual you\'ll ever need to fear.\"\n—Gaddock Teeg').

card_in_set('harbinger of the hunt', 'pMEI').
card_original_type('harbinger of the hunt'/'pMEI', 'Creature — Dragon').
card_original_text('harbinger of the hunt'/'pMEI', '').
card_first_print('harbinger of the hunt', 'pMEI').
card_image_name('harbinger of the hunt'/'pMEI', 'harbinger of the hunt').
card_uid('harbinger of the hunt'/'pMEI', 'pMEI:Harbinger of the Hunt:harbinger of the hunt').
card_rarity('harbinger of the hunt'/'pMEI', 'Special').
card_artist('harbinger of the hunt'/'pMEI', 'Chris Rahn').
card_number('harbinger of the hunt'/'pMEI', '116').
card_flavor_text('harbinger of the hunt'/'pMEI', 'An Atarka dragon\'s exhale cooks what its inhale consumes.').

card_in_set('high tide', 'pMEI').
card_original_type('high tide'/'pMEI', 'Instant').
card_original_text('high tide'/'pMEI', '').
card_image_name('high tide'/'pMEI', 'high tide').
card_uid('high tide'/'pMEI', 'pMEI:High Tide:high tide').
card_rarity('high tide'/'pMEI', 'Special').
card_artist('high tide'/'pMEI', 'Eric Deschamps').
card_number('high tide'/'pMEI', '80').
card_flavor_text('high tide'/'pMEI', '\"There\'s nothing like the high seas to clear your head or escape pursuers.\"').

card_in_set('honor of the pure', 'pMEI').
card_original_type('honor of the pure'/'pMEI', 'Enchantment').
card_original_text('honor of the pure'/'pMEI', '').
card_first_print('honor of the pure', 'pMEI').
card_image_name('honor of the pure'/'pMEI', 'honor of the pure').
card_uid('honor of the pure'/'pMEI', 'pMEI:Honor of the Pure:honor of the pure').
card_rarity('honor of the pure'/'pMEI', 'Special').
card_artist('honor of the pure'/'pMEI', 'Ralph Horsley').
card_number('honor of the pure'/'pMEI', '20').
card_flavor_text('honor of the pure'/'pMEI', 'Together the soldiers were like a golden blade, cutting down their enemies and scarring the darkness.').

card_in_set('inferno titan', 'pMEI').
card_original_type('inferno titan'/'pMEI', 'Creature — Giant').
card_original_text('inferno titan'/'pMEI', '').
card_first_print('inferno titan', 'pMEI').
card_image_name('inferno titan'/'pMEI', 'inferno titan').
card_uid('inferno titan'/'pMEI', 'pMEI:Inferno Titan:inferno titan').
card_rarity('inferno titan'/'pMEI', 'Special').
card_artist('inferno titan'/'pMEI', 'Sam Burley').
card_number('inferno titan'/'pMEI', '36').

card_in_set('ivorytusk fortress', 'pMEI').
card_original_type('ivorytusk fortress'/'pMEI', 'Creature — Elephant').
card_original_text('ivorytusk fortress'/'pMEI', '').
card_first_print('ivorytusk fortress', 'pMEI').
card_image_name('ivorytusk fortress'/'pMEI', 'ivorytusk fortress').
card_uid('ivorytusk fortress'/'pMEI', 'pMEI:Ivorytusk Fortress:ivorytusk fortress').
card_rarity('ivorytusk fortress'/'pMEI', 'Special').
card_artist('ivorytusk fortress'/'pMEI', 'Matt Stewart').
card_number('ivorytusk fortress'/'pMEI', '95').
card_flavor_text('ivorytusk fortress'/'pMEI', 'Abzan soldiers march to war confident that their Houses march with them.').

card_in_set('jace beleren', 'pMEI').
card_original_type('jace beleren'/'pMEI', 'Planeswalker — Jace').
card_original_text('jace beleren'/'pMEI', '').
card_first_print('jace beleren', 'pMEI').
card_image_name('jace beleren'/'pMEI', 'jace beleren').
card_uid('jace beleren'/'pMEI', 'pMEI:Jace Beleren:jace beleren').
card_rarity('jace beleren'/'pMEI', 'Special').
card_artist('jace beleren'/'pMEI', 'Aleksi Briclot').
card_number('jace beleren'/'pMEI', '15').

card_in_set('jace, memory adept', 'pMEI').
card_original_type('jace, memory adept'/'pMEI', 'Planeswalker — Jace').
card_original_text('jace, memory adept'/'pMEI', '').
card_first_print('jace, memory adept', 'pMEI').
card_image_name('jace, memory adept'/'pMEI', 'jace, memory adept').
card_uid('jace, memory adept'/'pMEI', 'pMEI:Jace, Memory Adept:jace, memory adept').
card_rarity('jace, memory adept'/'pMEI', 'Special').
card_artist('jace, memory adept'/'pMEI', 'Steve Prescott').
card_number('jace, memory adept'/'pMEI', '73').

card_in_set('jace, the living guildpact', 'pMEI').
card_original_type('jace, the living guildpact'/'pMEI', 'Planeswalker — Jace').
card_original_text('jace, the living guildpact'/'pMEI', '').
card_first_print('jace, the living guildpact', 'pMEI').
card_image_name('jace, the living guildpact'/'pMEI', 'jace, the living guildpact').
card_uid('jace, the living guildpact'/'pMEI', 'pMEI:Jace, the Living Guildpact:jace, the living guildpact').
card_rarity('jace, the living guildpact'/'pMEI', 'Special').
card_artist('jace, the living guildpact'/'pMEI', 'Jae Lee').
card_number('jace, the living guildpact'/'pMEI', '100').

card_in_set('jaya ballard, task mage', 'pMEI').
card_original_type('jaya ballard, task mage'/'pMEI', 'Legendary Creature — Human Spellshaper').
card_original_text('jaya ballard, task mage'/'pMEI', '').
card_first_print('jaya ballard, task mage', 'pMEI').
card_image_name('jaya ballard, task mage'/'pMEI', 'jaya ballard, task mage').
card_uid('jaya ballard, task mage'/'pMEI', 'pMEI:Jaya Ballard, Task Mage:jaya ballard, task mage').
card_rarity('jaya ballard, task mage'/'pMEI', 'Special').
card_artist('jaya ballard, task mage'/'pMEI', 'Matt Cavotta').
card_number('jaya ballard, task mage'/'pMEI', '18').

card_in_set('karametra\'s acolyte', 'pMEI').
card_original_type('karametra\'s acolyte'/'pMEI', 'Creature — Human Druid').
card_original_text('karametra\'s acolyte'/'pMEI', '').
card_first_print('karametra\'s acolyte', 'pMEI').
card_image_name('karametra\'s acolyte'/'pMEI', 'karametra\'s acolyte').
card_uid('karametra\'s acolyte'/'pMEI', 'pMEI:Karametra\'s Acolyte:karametra\'s acolyte').
card_rarity('karametra\'s acolyte'/'pMEI', 'Special').
card_artist('karametra\'s acolyte'/'pMEI', 'Dan Scott').
card_number('karametra\'s acolyte'/'pMEI', '78').
card_flavor_text('karametra\'s acolyte'/'pMEI', '\"The wilds are a garden tended by divine hands.\"').

card_in_set('knight exemplar', 'pMEI').
card_original_type('knight exemplar'/'pMEI', 'Creature — Human Knight').
card_original_text('knight exemplar'/'pMEI', '').
card_first_print('knight exemplar', 'pMEI').
card_image_name('knight exemplar'/'pMEI', 'knight exemplar').
card_uid('knight exemplar'/'pMEI', 'pMEI:Knight Exemplar:knight exemplar').
card_rarity('knight exemplar'/'pMEI', 'Special').
card_artist('knight exemplar'/'pMEI', 'Jason Chan').
card_number('knight exemplar'/'pMEI', '46').
card_flavor_text('knight exemplar'/'pMEI', '\"If you think you are brave enough to walk the path of honor, follow me into the dragon\'s den.\"').

card_in_set('kor skyfisher', 'pMEI').
card_original_type('kor skyfisher'/'pMEI', 'Creature — Kor Soldier').
card_original_text('kor skyfisher'/'pMEI', '').
card_first_print('kor skyfisher', 'pMEI').
card_image_name('kor skyfisher'/'pMEI', 'kor skyfisher').
card_uid('kor skyfisher'/'pMEI', 'pMEI:Kor Skyfisher:kor skyfisher').
card_rarity('kor skyfisher'/'pMEI', 'Special').
card_artist('kor skyfisher'/'pMEI', 'Dan Scott').
card_number('kor skyfisher'/'pMEI', '25').
card_flavor_text('kor skyfisher'/'pMEI', 'www.MagicTheGathering.com').

card_in_set('lightning hounds', 'pMEI').
card_original_type('lightning hounds'/'pMEI', 'Creature — Hound').
card_original_text('lightning hounds'/'pMEI', '').
card_first_print('lightning hounds', 'pMEI').
card_image_name('lightning hounds'/'pMEI', 'lightning hounds').
card_uid('lightning hounds'/'pMEI', 'pMEI:Lightning Hounds:lightning hounds').
card_rarity('lightning hounds'/'pMEI', 'Special').
card_artist('lightning hounds'/'pMEI', 'Andrew Robinson').
card_number('lightning hounds'/'pMEI', '10').
card_flavor_text('lightning hounds'/'pMEI', 'Quick enough to avoid jhovalls when alone and fierce enough to attack them in packs, the hounds were as at home in the mountains as the Mercadians were atop theirs.').

card_in_set('liliana of the dark realms', 'pMEI').
card_original_type('liliana of the dark realms'/'pMEI', 'Planeswalker — Liliana').
card_original_text('liliana of the dark realms'/'pMEI', '').
card_first_print('liliana of the dark realms', 'pMEI').
card_image_name('liliana of the dark realms'/'pMEI', 'liliana of the dark realms').
card_uid('liliana of the dark realms'/'pMEI', 'pMEI:Liliana of the Dark Realms:liliana of the dark realms').
card_rarity('liliana of the dark realms'/'pMEI', 'Special').
card_artist('liliana of the dark realms'/'pMEI', 'Steve Prescott').
card_number('liliana of the dark realms'/'pMEI', '74').

card_in_set('liliana vess', 'pMEI').
card_original_type('liliana vess'/'pMEI', 'Planeswalker — Liliana').
card_original_text('liliana vess'/'pMEI', '').
card_first_print('liliana vess', 'pMEI').
card_image_name('liliana vess'/'pMEI', 'liliana vess1').
card_uid('liliana vess'/'pMEI', 'pMEI:Liliana Vess:liliana vess1').
card_rarity('liliana vess'/'pMEI', 'Special').
card_artist('liliana vess'/'pMEI', 'Jae Lee').
card_number('liliana vess'/'pMEI', '101').

card_in_set('liliana vess', 'pMEI').
card_original_type('liliana vess'/'pMEI', 'Planeswalker — Liliana').
card_original_text('liliana vess'/'pMEI', '').
card_image_name('liliana vess'/'pMEI', 'liliana vess2').
card_uid('liliana vess'/'pMEI', 'pMEI:Liliana Vess:liliana vess2').
card_rarity('liliana vess'/'pMEI', 'Special').
card_artist('liliana vess'/'pMEI', 'Kekai Kotaki').
card_number('liliana vess'/'pMEI', '30').

card_in_set('magister of worth', 'pMEI').
card_original_type('magister of worth'/'pMEI', 'Creature — Angel').
card_original_text('magister of worth'/'pMEI', '').
card_first_print('magister of worth', 'pMEI').
card_image_name('magister of worth'/'pMEI', 'magister of worth').
card_uid('magister of worth'/'pMEI', 'pMEI:Magister of Worth:magister of worth').
card_rarity('magister of worth'/'pMEI', 'Special').
card_artist('magister of worth'/'pMEI', 'rk post').
card_number('magister of worth'/'pMEI', '86').

card_in_set('mana crypt', 'pMEI').
card_original_type('mana crypt'/'pMEI', 'Artifact').
card_original_text('mana crypt'/'pMEI', '').
card_first_print('mana crypt', 'pMEI').
card_image_name('mana crypt'/'pMEI', 'mana crypt').
card_uid('mana crypt'/'pMEI', 'pMEI:Mana Crypt:mana crypt').
card_rarity('mana crypt'/'pMEI', 'Special').
card_artist('mana crypt'/'pMEI', 'Mark Tedin').
card_number('mana crypt'/'pMEI', '6').
card_multiverse_id('mana crypt'/'pMEI', '97048').

card_in_set('memoricide', 'pMEI').
card_original_type('memoricide'/'pMEI', 'Sorcery').
card_original_text('memoricide'/'pMEI', '').
card_first_print('memoricide', 'pMEI').
card_image_name('memoricide'/'pMEI', 'memoricide').
card_uid('memoricide'/'pMEI', 'pMEI:Memoricide:memoricide').
card_rarity('memoricide'/'pMEI', 'Special').
card_artist('memoricide'/'pMEI', 'Erica Yang').
card_number('memoricide'/'pMEI', '29').
card_flavor_text('memoricide'/'pMEI', '\"You claim wisdom, yet I see nothing in here but tawdry wishes.\"').

card_in_set('merfolk mesmerist', 'pMEI').
card_original_type('merfolk mesmerist'/'pMEI', 'Creature — Merfolk Wizard').
card_original_text('merfolk mesmerist'/'pMEI', '').
card_first_print('merfolk mesmerist', 'pMEI').
card_image_name('merfolk mesmerist'/'pMEI', 'merfolk mesmerist').
card_uid('merfolk mesmerist'/'pMEI', 'pMEI:Merfolk Mesmerist:merfolk mesmerist').
card_rarity('merfolk mesmerist'/'pMEI', 'Special').
card_artist('merfolk mesmerist'/'pMEI', 'Jana Schirmer & Johannes Voss').
card_number('merfolk mesmerist'/'pMEI', '45').
card_flavor_text('merfolk mesmerist'/'pMEI', 'www.MagicTheGathering.com').

card_in_set('mirran crusader', 'pMEI').
card_original_type('mirran crusader'/'pMEI', 'Creature — Human Knight').
card_original_text('mirran crusader'/'pMEI', '').
card_first_print('mirran crusader', 'pMEI').
card_image_name('mirran crusader'/'pMEI', 'mirran crusader').
card_uid('mirran crusader'/'pMEI', 'pMEI:Mirran Crusader:mirran crusader').
card_rarity('mirran crusader'/'pMEI', 'Special').
card_artist('mirran crusader'/'pMEI', 'Karl Kopinski').
card_number('mirran crusader'/'pMEI', '32').
card_flavor_text('mirran crusader'/'pMEI', 'A symbol of what Mirrodin once was and hope for what it will be again.').

card_in_set('nalathni dragon', 'pMEI').
card_original_type('nalathni dragon'/'pMEI', 'Creature — Dragon').
card_original_text('nalathni dragon'/'pMEI', '').
card_image_name('nalathni dragon'/'pMEI', 'nalathni dragon').
card_uid('nalathni dragon'/'pMEI', 'pMEI:Nalathni Dragon:nalathni dragon').
card_rarity('nalathni dragon'/'pMEI', 'Special').
card_artist('nalathni dragon'/'pMEI', 'Michael Whelan').
card_number('nalathni dragon'/'pMEI', '3').
card_flavor_text('nalathni dragon'/'pMEI', 'These small but intelligent Dragons and their Olesian allies held back the tide of Pashalik Mons\'s hordes of Goblin Raiders.\nDragonCon 1994').
card_multiverse_id('nalathni dragon'/'pMEI', '97050').

card_in_set('necromaster dragon', 'pMEI').
card_original_type('necromaster dragon'/'pMEI', 'Creature — Dragon').
card_original_text('necromaster dragon'/'pMEI', '').
card_first_print('necromaster dragon', 'pMEI').
card_image_name('necromaster dragon'/'pMEI', 'necromaster dragon').
card_uid('necromaster dragon'/'pMEI', 'pMEI:Necromaster Dragon:necromaster dragon').
card_rarity('necromaster dragon'/'pMEI', 'Special').
card_artist('necromaster dragon'/'pMEI', 'Peter Mohrbacher').
card_number('necromaster dragon'/'pMEI', '114').

card_in_set('nightveil specter', 'pMEI').
card_original_type('nightveil specter'/'pMEI', 'Creature — Specter').
card_original_text('nightveil specter'/'pMEI', '').
card_first_print('nightveil specter', 'pMEI').
card_image_name('nightveil specter'/'pMEI', 'nightveil specter').
card_uid('nightveil specter'/'pMEI', 'pMEI:Nightveil Specter:nightveil specter').
card_rarity('nightveil specter'/'pMEI', 'Special').
card_artist('nightveil specter'/'pMEI', 'Steven Belledin').
card_number('nightveil specter'/'pMEI', '61').

card_in_set('nissa revane', 'pMEI').
card_original_type('nissa revane'/'pMEI', 'Planeswalker — Nissa').
card_original_text('nissa revane'/'pMEI', '').
card_first_print('nissa revane', 'pMEI').
card_image_name('nissa revane'/'pMEI', 'nissa revane').
card_uid('nissa revane'/'pMEI', 'pMEI:Nissa Revane:nissa revane').
card_rarity('nissa revane'/'pMEI', 'Special').
card_artist('nissa revane'/'pMEI', 'Jaime Jones').
card_number('nissa revane'/'pMEI', '27').

card_in_set('nissa, worldwaker', 'pMEI').
card_original_type('nissa, worldwaker'/'pMEI', 'Planeswalker — Nissa').
card_original_text('nissa, worldwaker'/'pMEI', '').
card_first_print('nissa, worldwaker', 'pMEI').
card_image_name('nissa, worldwaker'/'pMEI', 'nissa, worldwaker').
card_uid('nissa, worldwaker'/'pMEI', 'pMEI:Nissa, Worldwaker:nissa, worldwaker').
card_rarity('nissa, worldwaker'/'pMEI', 'Special').
card_artist('nissa, worldwaker'/'pMEI', 'Jae Lee').
card_number('nissa, worldwaker'/'pMEI', '103').

card_in_set('ogre arsonist', 'pMEI').
card_original_type('ogre arsonist'/'pMEI', 'Creature — Ogre').
card_original_text('ogre arsonist'/'pMEI', '').
card_first_print('ogre arsonist', 'pMEI').
card_image_name('ogre arsonist'/'pMEI', 'ogre arsonist').
card_uid('ogre arsonist'/'pMEI', 'pMEI:Ogre Arsonist:ogre arsonist').
card_rarity('ogre arsonist'/'pMEI', 'Special').
card_artist('ogre arsonist'/'pMEI', 'Chris Rahn').
card_number('ogre arsonist'/'pMEI', '63').
card_flavor_text('ogre arsonist'/'pMEI', '\"All of Ravnica\'s looking for you, Dack, from the Wojek on down.\"\n—Maytov').

card_in_set('ogre battledriver', 'pMEI').
card_original_type('ogre battledriver'/'pMEI', 'Creature — Ogre Warrior').
card_original_text('ogre battledriver'/'pMEI', '').
card_first_print('ogre battledriver', 'pMEI').
card_image_name('ogre battledriver'/'pMEI', 'ogre battledriver').
card_uid('ogre battledriver'/'pMEI', 'pMEI:Ogre Battledriver:ogre battledriver').
card_rarity('ogre battledriver'/'pMEI', 'Special').
card_artist('ogre battledriver'/'pMEI', 'Matt Stewart').
card_number('ogre battledriver'/'pMEI', '69').
card_flavor_text('ogre battledriver'/'pMEI', 'Ogres are driven by passion, rage, and another ogre standing behind them with a whip.').

card_in_set('ojutai\'s command', 'pMEI').
card_original_type('ojutai\'s command'/'pMEI', 'Instant').
card_original_text('ojutai\'s command'/'pMEI', '').
card_first_print('ojutai\'s command', 'pMEI').
card_image_name('ojutai\'s command'/'pMEI', 'ojutai\'s command').
card_uid('ojutai\'s command'/'pMEI', 'pMEI:Ojutai\'s Command:ojutai\'s command').
card_rarity('ojutai\'s command'/'pMEI', 'Special').
card_artist('ojutai\'s command'/'pMEI', 'Craig J Spearing').
card_number('ojutai\'s command'/'pMEI', '106').

card_in_set('phyrexian rager', 'pMEI').
card_original_type('phyrexian rager'/'pMEI', 'Creature — Horror').
card_original_text('phyrexian rager'/'pMEI', '').
card_first_print('phyrexian rager', 'pMEI').
card_image_name('phyrexian rager'/'pMEI', 'phyrexian rager').
card_uid('phyrexian rager'/'pMEI', 'pMEI:Phyrexian Rager:phyrexian rager').
card_rarity('phyrexian rager'/'pMEI', 'Special').
card_artist('phyrexian rager'/'pMEI', 'Mark Tedin').
card_number('phyrexian rager'/'pMEI', '14').
card_flavor_text('phyrexian rager'/'pMEI', 'It takes no prisoners, but it keeps the choicest bits for Phyrexia.').

card_in_set('primordial hydra', 'pMEI').
card_original_type('primordial hydra'/'pMEI', 'Creature — Hydra').
card_original_text('primordial hydra'/'pMEI', '').
card_first_print('primordial hydra', 'pMEI').
card_image_name('primordial hydra'/'pMEI', 'primordial hydra').
card_uid('primordial hydra'/'pMEI', 'pMEI:Primordial Hydra:primordial hydra').
card_rarity('primordial hydra'/'pMEI', 'Special').
card_artist('primordial hydra'/'pMEI', 'Chase Stone').
card_number('primordial hydra'/'pMEI', '49').

card_in_set('pristine skywise', 'pMEI').
card_original_type('pristine skywise'/'pMEI', 'Creature — Dragon').
card_original_text('pristine skywise'/'pMEI', '').
card_first_print('pristine skywise', 'pMEI').
card_image_name('pristine skywise'/'pMEI', 'pristine skywise').
card_uid('pristine skywise'/'pMEI', 'pMEI:Pristine Skywise:pristine skywise').
card_rarity('pristine skywise'/'pMEI', 'Special').
card_artist('pristine skywise'/'pMEI', 'Dan Scott').
card_number('pristine skywise'/'pMEI', '113').
card_flavor_text('pristine skywise'/'pMEI', 'The elite of Ojutai\'s brood, the skywise see their enemies as puzzles to be solved.').

card_in_set('rakshasa vizier', 'pMEI').
card_original_type('rakshasa vizier'/'pMEI', 'Creature — Cat Demon').
card_original_text('rakshasa vizier'/'pMEI', '').
card_first_print('rakshasa vizier', 'pMEI').
card_image_name('rakshasa vizier'/'pMEI', 'rakshasa vizier').
card_uid('rakshasa vizier'/'pMEI', 'pMEI:Rakshasa Vizier:rakshasa vizier').
card_rarity('rakshasa vizier'/'pMEI', 'Special').
card_artist('rakshasa vizier'/'pMEI', 'Peter Mohrbacher').
card_number('rakshasa vizier'/'pMEI', '96').
card_flavor_text('rakshasa vizier'/'pMEI', 'Rakshasa offer deals that sound advantageous to those who forget who they are dealing with.').

card_in_set('ratchet bomb', 'pMEI').
card_original_type('ratchet bomb'/'pMEI', 'Artifact').
card_original_text('ratchet bomb'/'pMEI', '').
card_first_print('ratchet bomb', 'pMEI').
card_image_name('ratchet bomb'/'pMEI', 'ratchet bomb').
card_uid('ratchet bomb'/'pMEI', 'pMEI:Ratchet Bomb:ratchet bomb').
card_rarity('ratchet bomb'/'pMEI', 'Special').
card_artist('ratchet bomb'/'pMEI', 'Dan Scott').
card_number('ratchet bomb'/'pMEI', '67').

card_in_set('rattleclaw mystic', 'pMEI').
card_original_type('rattleclaw mystic'/'pMEI', 'Creature — Human Shaman').
card_original_text('rattleclaw mystic'/'pMEI', '').
card_first_print('rattleclaw mystic', 'pMEI').
card_image_name('rattleclaw mystic'/'pMEI', 'rattleclaw mystic').
card_uid('rattleclaw mystic'/'pMEI', 'pMEI:Rattleclaw Mystic:rattleclaw mystic').
card_rarity('rattleclaw mystic'/'pMEI', 'Special').
card_artist('rattleclaw mystic'/'pMEI', 'Min Yum').
card_number('rattleclaw mystic'/'pMEI', '92').

card_in_set('render silent', 'pMEI').
card_original_type('render silent'/'pMEI', 'Instant').
card_original_text('render silent'/'pMEI', '').
card_first_print('render silent', 'pMEI').
card_image_name('render silent'/'pMEI', 'render silent').
card_uid('render silent'/'pMEI', 'pMEI:Render Silent:render silent').
card_rarity('render silent'/'pMEI', 'Special').
card_artist('render silent'/'pMEI', 'Kev Walker').
card_number('render silent'/'pMEI', '66').
card_flavor_text('render silent'/'pMEI', '\"We have confiscated your spells as evidence. Once we conclude our investigation, you may petition to have them returned.\"').

card_in_set('retaliator griffin', 'pMEI').
card_original_type('retaliator griffin'/'pMEI', 'Creature — Griffin').
card_original_text('retaliator griffin'/'pMEI', '').
card_first_print('retaliator griffin', 'pMEI').
card_image_name('retaliator griffin'/'pMEI', 'retaliator griffin').
card_uid('retaliator griffin'/'pMEI', 'pMEI:Retaliator Griffin:retaliator griffin').
card_rarity('retaliator griffin'/'pMEI', 'Special').
card_artist('retaliator griffin'/'pMEI', 'Jesper Ejsing').
card_number('retaliator griffin'/'pMEI', '24').
card_flavor_text('retaliator griffin'/'pMEI', 'For every eye, an eye. For every tooth, a tooth. Griffins are nothing if not fair.').

card_in_set('sage of the inward eye', 'pMEI').
card_original_type('sage of the inward eye'/'pMEI', 'Creature — Djinn Wizard').
card_original_text('sage of the inward eye'/'pMEI', '').
card_first_print('sage of the inward eye', 'pMEI').
card_image_name('sage of the inward eye'/'pMEI', 'sage of the inward eye').
card_uid('sage of the inward eye'/'pMEI', 'pMEI:Sage of the Inward Eye:sage of the inward eye').
card_rarity('sage of the inward eye'/'pMEI', 'Special').
card_artist('sage of the inward eye'/'pMEI', 'Anastasia Ovchinnikova').
card_number('sage of the inward eye'/'pMEI', '97').
card_flavor_text('sage of the inward eye'/'pMEI', '\"No one petal claims beauty for the lotus.\"').

card_in_set('sage-eye avengers', 'pMEI').
card_original_type('sage-eye avengers'/'pMEI', 'Creature — Djinn Monk').
card_original_text('sage-eye avengers'/'pMEI', '').
card_first_print('sage-eye avengers', 'pMEI').
card_image_name('sage-eye avengers'/'pMEI', 'sage-eye avengers').
card_uid('sage-eye avengers'/'pMEI', 'pMEI:Sage-Eye Avengers:sage-eye avengers').
card_rarity('sage-eye avengers'/'pMEI', 'Special').
card_artist('sage-eye avengers'/'pMEI', 'Viktor Titov').
card_number('sage-eye avengers'/'pMEI', '108').

card_in_set('scavenging ooze', 'pMEI').
card_original_type('scavenging ooze'/'pMEI', 'Creature — Ooze').
card_original_text('scavenging ooze'/'pMEI', '').
card_first_print('scavenging ooze', 'pMEI').
card_image_name('scavenging ooze'/'pMEI', 'scavenging ooze').
card_uid('scavenging ooze'/'pMEI', 'pMEI:Scavenging Ooze:scavenging ooze').
card_rarity('scavenging ooze'/'pMEI', 'Special').
card_artist('scavenging ooze'/'pMEI', 'Dan Scott').
card_number('scavenging ooze'/'pMEI', '70').
card_flavor_text('scavenging ooze'/'pMEI', 'In nature, not a single bone or scrap of flesh goes to waste.').

card_in_set('scent of cinder', 'pMEI').
card_original_type('scent of cinder'/'pMEI', 'Sorcery').
card_original_text('scent of cinder'/'pMEI', '').
card_first_print('scent of cinder', 'pMEI').
card_image_name('scent of cinder'/'pMEI', 'scent of cinder').
card_uid('scent of cinder'/'pMEI', 'pMEI:Scent of Cinder:scent of cinder').
card_rarity('scent of cinder'/'pMEI', 'Special').
card_artist('scent of cinder'/'pMEI', 'Carl Critchlow').
card_number('scent of cinder'/'pMEI', '9').

card_in_set('serra avatar', 'pMEI').
card_original_type('serra avatar'/'pMEI', 'Creature — Avatar').
card_original_text('serra avatar'/'pMEI', '').
card_first_print('serra avatar', 'pMEI').
card_image_name('serra avatar'/'pMEI', 'serra avatar').
card_uid('serra avatar'/'pMEI', 'pMEI:Serra Avatar:serra avatar').
card_rarity('serra avatar'/'pMEI', 'Special').
card_artist('serra avatar'/'pMEI', 'Igor Kieryluk').
card_number('serra avatar'/'pMEI', '48').

card_in_set('sewers of estark', 'pMEI').
card_original_type('sewers of estark'/'pMEI', 'Instant').
card_original_text('sewers of estark'/'pMEI', '').
card_first_print('sewers of estark', 'pMEI').
card_image_name('sewers of estark'/'pMEI', 'sewers of estark').
card_uid('sewers of estark'/'pMEI', 'pMEI:Sewers of Estark:sewers of estark').
card_rarity('sewers of estark'/'pMEI', 'Special').
card_artist('sewers of estark'/'pMEI', 'Melissa A. Benson').
card_number('sewers of estark'/'pMEI', '2').
card_multiverse_id('sewers of estark'/'pMEI', '97054').

card_in_set('shamanic revelation', 'pMEI').
card_original_type('shamanic revelation'/'pMEI', 'Sorcery').
card_original_text('shamanic revelation'/'pMEI', '').
card_first_print('shamanic revelation', 'pMEI').
card_image_name('shamanic revelation'/'pMEI', 'shamanic revelation').
card_uid('shamanic revelation'/'pMEI', 'pMEI:Shamanic Revelation:shamanic revelation').
card_rarity('shamanic revelation'/'pMEI', 'Special').
card_artist('shamanic revelation'/'pMEI', 'Matt Stewart').
card_number('shamanic revelation'/'pMEI', '105').
card_flavor_text('shamanic revelation'/'pMEI', 'First, whispers of ancient days. Then, visions of things to come.').

card_in_set('silver drake', 'pMEI').
card_original_type('silver drake'/'pMEI', 'Creature — Drake').
card_original_text('silver drake'/'pMEI', '').
card_first_print('silver drake', 'pMEI').
card_image_name('silver drake'/'pMEI', 'silver drake').
card_uid('silver drake'/'pMEI', 'pMEI:Silver Drake:silver drake').
card_rarity('silver drake'/'pMEI', 'Special').
card_artist('silver drake'/'pMEI', 'Allan Pollack').
card_number('silver drake'/'pMEI', '13').

card_in_set('silverblade paladin', 'pMEI').
card_original_type('silverblade paladin'/'pMEI', 'Creature — Human Knight').
card_original_text('silverblade paladin'/'pMEI', '').
card_first_print('silverblade paladin', 'pMEI').
card_image_name('silverblade paladin'/'pMEI', 'silverblade paladin').
card_uid('silverblade paladin'/'pMEI', 'pMEI:Silverblade Paladin:silverblade paladin').
card_rarity('silverblade paladin'/'pMEI', 'Special').
card_artist('silverblade paladin'/'pMEI', 'Todd Lockwood').
card_number('silverblade paladin'/'pMEI', '44').

card_in_set('soul of ravnica', 'pMEI').
card_original_type('soul of ravnica'/'pMEI', 'Creature — Avatar').
card_original_text('soul of ravnica'/'pMEI', '').
card_first_print('soul of ravnica', 'pMEI').
card_image_name('soul of ravnica'/'pMEI', 'soul of ravnica').
card_uid('soul of ravnica'/'pMEI', 'pMEI:Soul of Ravnica:soul of ravnica').
card_rarity('soul of ravnica'/'pMEI', 'Special').
card_artist('soul of ravnica'/'pMEI', 'Lucas Graciano').
card_number('soul of ravnica'/'pMEI', '87').

card_in_set('soul of zendikar', 'pMEI').
card_original_type('soul of zendikar'/'pMEI', 'Creature — Avatar').
card_original_text('soul of zendikar'/'pMEI', '').
card_first_print('soul of zendikar', 'pMEI').
card_image_name('soul of zendikar'/'pMEI', 'soul of zendikar').
card_uid('soul of zendikar'/'pMEI', 'pMEI:Soul of Zendikar:soul of zendikar').
card_rarity('soul of zendikar'/'pMEI', 'Special').
card_artist('soul of zendikar'/'pMEI', 'Eytan Zana').
card_number('soul of zendikar'/'pMEI', '88').

card_in_set('spined wurm', 'pMEI').
card_original_type('spined wurm'/'pMEI', 'Creature — Wurm').
card_original_text('spined wurm'/'pMEI', '').
card_first_print('spined wurm', 'pMEI').
card_border('spined wurm'/'pMEI', 'white').
card_image_name('spined wurm'/'pMEI', 'spined wurm').
card_uid('spined wurm'/'pMEI', 'pMEI:Spined Wurm:spined wurm').
card_rarity('spined wurm'/'pMEI', 'Special').
card_artist('spined wurm'/'pMEI', 'Keith Parkinson').
card_number('spined wurm'/'pMEI', '11').
card_flavor_text('spined wurm'/'pMEI', '\"As it moved, the wurm\'s spines gathered up bits of flowstone, which took the shapes of dead villagers\' heads. Each head spoke a single sound, but if taken together, they said, \'Alas for the living.\'\"\n—Dal myth of the wurm').

card_in_set('standstill', 'pMEI').
card_original_type('standstill'/'pMEI', 'Enchantment').
card_original_text('standstill'/'pMEI', '').
card_first_print('standstill', 'pMEI').
card_image_name('standstill'/'pMEI', 'standstill').
card_uid('standstill'/'pMEI', 'pMEI:Standstill:standstill').
card_rarity('standstill'/'pMEI', 'Special').
card_artist('standstill'/'pMEI', 'Matt Stewart').
card_number('standstill'/'pMEI', '57').
card_flavor_text('standstill'/'pMEI', '\"You learn a lot in no time when you\'re about to get your neck snapped.\"\n—Dack Fayden').

card_in_set('stealer of secrets', 'pMEI').
card_original_type('stealer of secrets'/'pMEI', 'Creature — Human Rogue').
card_original_text('stealer of secrets'/'pMEI', '').
card_first_print('stealer of secrets', 'pMEI').
card_image_name('stealer of secrets'/'pMEI', 'stealer of secrets').
card_uid('stealer of secrets'/'pMEI', 'pMEI:Stealer of Secrets:stealer of secrets').
card_rarity('stealer of secrets'/'pMEI', 'Special').
card_artist('stealer of secrets'/'pMEI', 'Michael C. Hayes').
card_number('stealer of secrets'/'pMEI', '89').
card_flavor_text('stealer of secrets'/'pMEI', 'www.MagicTheGathering.com').

card_in_set('steward of valeron', 'pMEI').
card_original_type('steward of valeron'/'pMEI', 'Creature — Human Druid Knight').
card_original_text('steward of valeron'/'pMEI', '').
card_first_print('steward of valeron', 'pMEI').
card_image_name('steward of valeron'/'pMEI', 'steward of valeron').
card_uid('steward of valeron'/'pMEI', 'pMEI:Steward of Valeron:steward of valeron').
card_rarity('steward of valeron'/'pMEI', 'Special').
card_artist('steward of valeron'/'pMEI', 'Greg Staples').
card_number('steward of valeron'/'pMEI', '21').
card_flavor_text('steward of valeron'/'pMEI', 'www.MagicTheGathering.com').

card_in_set('sultai charm', 'pMEI').
card_original_type('sultai charm'/'pMEI', 'Instant').
card_original_text('sultai charm'/'pMEI', '').
card_first_print('sultai charm', 'pMEI').
card_image_name('sultai charm'/'pMEI', 'sultai charm').
card_uid('sultai charm'/'pMEI', 'pMEI:Sultai Charm:sultai charm').
card_rarity('sultai charm'/'pMEI', 'Special').
card_artist('sultai charm'/'pMEI', 'Ryan Yee').
card_number('sultai charm'/'pMEI', '117').
card_flavor_text('sultai charm'/'pMEI', '\"Strike,\" the fumes hiss. \"Raise an empire with your ambition.\"').

card_in_set('sunblast angel', 'pMEI').
card_original_type('sunblast angel'/'pMEI', 'Creature — Angel').
card_original_text('sunblast angel'/'pMEI', '').
card_first_print('sunblast angel', 'pMEI').
card_image_name('sunblast angel'/'pMEI', 'sunblast angel').
card_uid('sunblast angel'/'pMEI', 'pMEI:Sunblast Angel:sunblast angel').
card_rarity('sunblast angel'/'pMEI', 'Special').
card_artist('sunblast angel'/'pMEI', 'Jason Chan').
card_number('sunblast angel'/'pMEI', '47').
card_flavor_text('sunblast angel'/'pMEI', 'There may exist powers even greater than Phyrexia.').

card_in_set('supreme verdict', 'pMEI').
card_original_type('supreme verdict'/'pMEI', 'Sorcery').
card_original_text('supreme verdict'/'pMEI', '').
card_first_print('supreme verdict', 'pMEI').
card_image_name('supreme verdict'/'pMEI', 'supreme verdict').
card_uid('supreme verdict'/'pMEI', 'pMEI:Supreme Verdict:supreme verdict').
card_rarity('supreme verdict'/'pMEI', 'Special').
card_artist('supreme verdict'/'pMEI', 'John Avon').
card_number('supreme verdict'/'pMEI', '56').
card_flavor_text('supreme verdict'/'pMEI', 'Leonos had no second thoughts about the abolishment edict. He\'d left skyrunes warning of the eviction, even though it was cloudy.').

card_in_set('surgical extraction', 'pMEI').
card_original_type('surgical extraction'/'pMEI', 'Instant').
card_original_text('surgical extraction'/'pMEI', '').
card_first_print('surgical extraction', 'pMEI').
card_image_name('surgical extraction'/'pMEI', 'surgical extraction').
card_uid('surgical extraction'/'pMEI', 'pMEI:Surgical Extraction:surgical extraction').
card_rarity('surgical extraction'/'pMEI', 'Special').
card_artist('surgical extraction'/'pMEI', 'Greg Staples').
card_number('surgical extraction'/'pMEI', '33').

card_in_set('sylvan caryatid', 'pMEI').
card_original_type('sylvan caryatid'/'pMEI', 'Creature — Plant').
card_original_text('sylvan caryatid'/'pMEI', '').
card_first_print('sylvan caryatid', 'pMEI').
card_image_name('sylvan caryatid'/'pMEI', 'sylvan caryatid').
card_uid('sylvan caryatid'/'pMEI', 'pMEI:Sylvan Caryatid:sylvan caryatid').
card_rarity('sylvan caryatid'/'pMEI', 'Special').
card_artist('sylvan caryatid'/'pMEI', 'Lars Grant-West').
card_number('sylvan caryatid'/'pMEI', '77').
card_flavor_text('sylvan caryatid'/'pMEI', 'Those who enter the copse never leave. They find peace there and take root, becoming part of the ever-growing grove.').

card_in_set('temur war shaman', 'pMEI').
card_original_type('temur war shaman'/'pMEI', 'Creature — Human Shaman').
card_original_text('temur war shaman'/'pMEI', '').
card_first_print('temur war shaman', 'pMEI').
card_image_name('temur war shaman'/'pMEI', 'temur war shaman').
card_uid('temur war shaman'/'pMEI', 'pMEI:Temur War Shaman:temur war shaman').
card_rarity('temur war shaman'/'pMEI', 'Special').
card_artist('temur war shaman'/'pMEI', 'Raymond Swanland').
card_number('temur war shaman'/'pMEI', '111').

card_in_set('terastodon', 'pMEI').
card_original_type('terastodon'/'pMEI', 'Creature — Elephant').
card_original_text('terastodon'/'pMEI', '').
card_first_print('terastodon', 'pMEI').
card_image_name('terastodon'/'pMEI', 'terastodon').
card_uid('terastodon'/'pMEI', 'pMEI:Terastodon:terastodon').
card_rarity('terastodon'/'pMEI', 'Special').
card_artist('terastodon'/'pMEI', 'Lars Grant-West').
card_number('terastodon'/'pMEI', '52').

card_in_set('treasure hunt', 'pMEI').
card_original_type('treasure hunt'/'pMEI', 'Sorcery').
card_original_text('treasure hunt'/'pMEI', '').
card_first_print('treasure hunt', 'pMEI').
card_image_name('treasure hunt'/'pMEI', 'treasure hunt').
card_uid('treasure hunt'/'pMEI', 'pMEI:Treasure Hunt:treasure hunt').
card_rarity('treasure hunt'/'pMEI', 'Special').
card_artist('treasure hunt'/'pMEI', 'Aleksi Briclot').
card_number('treasure hunt'/'pMEI', '38').
card_flavor_text('treasure hunt'/'pMEI', '\"Some call me a thief. I prefer to think of myself as a discreet borrower.\"\n—Dack Fayden, Planeswalker').

card_in_set('turnabout', 'pMEI').
card_original_type('turnabout'/'pMEI', 'Instant').
card_original_text('turnabout'/'pMEI', '').
card_first_print('turnabout', 'pMEI').
card_image_name('turnabout'/'pMEI', 'turnabout').
card_uid('turnabout'/'pMEI', 'pMEI:Turnabout:turnabout').
card_rarity('turnabout'/'pMEI', 'Special').
card_artist('turnabout'/'pMEI', 'Dan Scott').
card_number('turnabout'/'pMEI', '60').
card_flavor_text('turnabout'/'pMEI', 'The best cure for a big ego is a little failure.').

card_in_set('vampire nocturnus', 'pMEI').
card_original_type('vampire nocturnus'/'pMEI', 'Creature — Vampire').
card_original_text('vampire nocturnus'/'pMEI', '').
card_first_print('vampire nocturnus', 'pMEI').
card_image_name('vampire nocturnus'/'pMEI', 'vampire nocturnus').
card_uid('vampire nocturnus'/'pMEI', 'pMEI:Vampire Nocturnus:vampire nocturnus').
card_rarity('vampire nocturnus'/'pMEI', 'Special').
card_artist('vampire nocturnus'/'pMEI', 'Jason Felix').
card_number('vampire nocturnus'/'pMEI', '50').
card_flavor_text('vampire nocturnus'/'pMEI', '\"Your life will set with the sun.\"').

card_in_set('voidmage husher', 'pMEI').
card_original_type('voidmage husher'/'pMEI', 'Creature — Human Wizard').
card_original_text('voidmage husher'/'pMEI', '').
card_first_print('voidmage husher', 'pMEI').
card_image_name('voidmage husher'/'pMEI', 'voidmage husher').
card_uid('voidmage husher'/'pMEI', 'pMEI:Voidmage Husher:voidmage husher').
card_rarity('voidmage husher'/'pMEI', 'Special').
card_artist('voidmage husher'/'pMEI', 'Ryan Pancoast').
card_number('voidmage husher'/'pMEI', '62').

card_in_set('warmonger', 'pMEI').
card_original_type('warmonger'/'pMEI', 'Creature — Minotaur Monger').
card_original_text('warmonger'/'pMEI', '').
card_first_print('warmonger', 'pMEI').
card_image_name('warmonger'/'pMEI', 'warmonger').
card_uid('warmonger'/'pMEI', 'pMEI:Warmonger:warmonger').
card_rarity('warmonger'/'pMEI', 'Special').
card_artist('warmonger'/'pMEI', 'Heather Hudson').
card_number('warmonger'/'pMEI', '12').
card_flavor_text('warmonger'/'pMEI', 'Funny how money and war travel so well together.').

card_in_set('wash out', 'pMEI').
card_original_type('wash out'/'pMEI', 'Sorcery').
card_original_text('wash out'/'pMEI', '').
card_first_print('wash out', 'pMEI').
card_image_name('wash out'/'pMEI', 'wash out').
card_uid('wash out'/'pMEI', 'pMEI:Wash Out:wash out').
card_rarity('wash out'/'pMEI', 'Special').
card_artist('wash out'/'pMEI', 'Volkan Baga').
card_number('wash out'/'pMEI', '82').
card_flavor_text('wash out'/'pMEI', '\"Perhaps planning should become a higher priority for me.\"\n—Dack Fayden').

card_in_set('windseeker centaur', 'pMEI').
card_original_type('windseeker centaur'/'pMEI', 'Creature — Centaur').
card_original_text('windseeker centaur'/'pMEI', '').
card_first_print('windseeker centaur', 'pMEI').
card_image_name('windseeker centaur'/'pMEI', 'windseeker centaur').
card_uid('windseeker centaur'/'pMEI', 'pMEI:Windseeker Centaur:windseeker centaur').
card_rarity('windseeker centaur'/'pMEI', 'Special').
card_artist('windseeker centaur'/'pMEI', 'Anson Maddocks').
card_number('windseeker centaur'/'pMEI', '7').
card_flavor_text('windseeker centaur'/'pMEI', 'Loyal and passionate in battle, the Windseeker tribe roams the Green Lands near the Honeyed Sea.').
card_multiverse_id('windseeker centaur'/'pMEI', '97057').

card_in_set('xathrid necromancer', 'pMEI').
card_original_type('xathrid necromancer'/'pMEI', 'Creature — Human Wizard').
card_original_text('xathrid necromancer'/'pMEI', '').
card_first_print('xathrid necromancer', 'pMEI').
card_image_name('xathrid necromancer'/'pMEI', 'xathrid necromancer').
card_uid('xathrid necromancer'/'pMEI', 'pMEI:Xathrid Necromancer:xathrid necromancer').
card_rarity('xathrid necromancer'/'pMEI', 'Special').
card_artist('xathrid necromancer'/'pMEI', 'Maciej Kuciara').
card_number('xathrid necromancer'/'pMEI', '91').
card_flavor_text('xathrid necromancer'/'pMEI', '\"My commands shall echo forever in their dusty skulls.\"').
