% Commander's Arsenal

set('CM1').
set_name('CM1', 'Commander\'s Arsenal').
set_release_date('CM1', '2012-11-02').
set_border('CM1', 'black').
set_type('CM1', 'commander').

card_in_set('chaos warp', 'CM1').
card_original_type('chaos warp'/'CM1', 'Instant').
card_original_text('chaos warp'/'CM1', 'The owner of target permanent shuffles it into his or her library, then reveals the top card of his or her library. If it\'s a permanent card, he or she puts it onto the battlefield.').
card_image_name('chaos warp'/'CM1', 'chaos warp').
card_uid('chaos warp'/'CM1', 'CM1:Chaos Warp:chaos warp').
card_rarity('chaos warp'/'CM1', 'Rare').
card_artist('chaos warp'/'CM1', 'Trevor Claxton').
card_number('chaos warp'/'CM1', '1').
card_multiverse_id('chaos warp'/'CM1', '338441').

card_in_set('command tower', 'CM1').
card_original_type('command tower'/'CM1', 'Land').
card_original_text('command tower'/'CM1', '{T}: Add to your mana pool one mana of any color in your commander\'s color identity.').
card_image_name('command tower'/'CM1', 'command tower').
card_uid('command tower'/'CM1', 'CM1:Command Tower:command tower').
card_rarity('command tower'/'CM1', 'Common').
card_artist('command tower'/'CM1', 'Adam Paquette').
card_number('command tower'/'CM1', '2').
card_flavor_text('command tower'/'CM1', 'When defeat is near and guidance is scarce, all eyes look in one direction.').
card_multiverse_id('command tower'/'CM1', '338442').

card_in_set('decree of pain', 'CM1').
card_original_type('decree of pain'/'CM1', 'Sorcery').
card_original_text('decree of pain'/'CM1', 'Destroy all creatures. They can\'t be regenerated. Draw a card for each creature destroyed this way.\nCycling {3}{B}{B} ({3}{B}{B}, Discard this card: Draw a card.)\nWhen you cycle Decree of Pain, all creatures get -2/-2 until end of turn.').
card_image_name('decree of pain'/'CM1', 'decree of pain').
card_uid('decree of pain'/'CM1', 'CM1:Decree of Pain:decree of pain').
card_rarity('decree of pain'/'CM1', 'Rare').
card_artist('decree of pain'/'CM1', 'Mathias Kollros').
card_number('decree of pain'/'CM1', '3').
card_multiverse_id('decree of pain'/'CM1', '338453').

card_in_set('desertion', 'CM1').
card_original_type('desertion'/'CM1', 'Instant').
card_original_text('desertion'/'CM1', 'Counter target spell. If an artifact or creature spell is countered this way, put that card onto the battlefield under your control instead of into its owner\'s graveyard.').
card_image_name('desertion'/'CM1', 'desertion').
card_uid('desertion'/'CM1', 'CM1:Desertion:desertion').
card_rarity('desertion'/'CM1', 'Rare').
card_artist('desertion'/'CM1', 'Richard Kane Ferguson').
card_number('desertion'/'CM1', '4').
card_flavor_text('desertion'/'CM1', 'First the insult, then the injury.').
card_multiverse_id('desertion'/'CM1', '338454').

card_in_set('diaochan, artful beauty', 'CM1').
card_original_type('diaochan, artful beauty'/'CM1', 'Legendary Creature — Human Advisor').
card_original_text('diaochan, artful beauty'/'CM1', '{T}: Destroy target creature of your choice, then destroy target creature of an opponent\'s choice. Activate this ability only during your turn, before attackers are declared.').
card_image_name('diaochan, artful beauty'/'CM1', 'diaochan, artful beauty').
card_uid('diaochan, artful beauty'/'CM1', 'CM1:Diaochan, Artful Beauty:diaochan, artful beauty').
card_rarity('diaochan, artful beauty'/'CM1', 'Rare').
card_artist('diaochan, artful beauty'/'CM1', 'Miao Aili').
card_number('diaochan, artful beauty'/'CM1', '5').
card_multiverse_id('diaochan, artful beauty'/'CM1', '338449').

card_in_set('dragonlair spider', 'CM1').
card_original_type('dragonlair spider'/'CM1', 'Creature — Spider').
card_original_text('dragonlair spider'/'CM1', 'Reach\nWhenever an opponent casts a spell, put a 1/1 green Insect creature token onto the battlefield.').
card_image_name('dragonlair spider'/'CM1', 'dragonlair spider').
card_uid('dragonlair spider'/'CM1', 'CM1:Dragonlair Spider:dragonlair spider').
card_rarity('dragonlair spider'/'CM1', 'Rare').
card_artist('dragonlair spider'/'CM1', 'Carl Critchlow').
card_number('dragonlair spider'/'CM1', '6').
card_flavor_text('dragonlair spider'/'CM1', 'Swarms thrive in its nest, feeding on leathery bits of discarded wing.').
card_multiverse_id('dragonlair spider'/'CM1', '338446').

card_in_set('duplicant', 'CM1').
card_original_type('duplicant'/'CM1', 'Artifact Creature — Shapeshifter').
card_original_text('duplicant'/'CM1', 'Imprint — When Duplicant enters the battlefield, you may exile target nontoken creature.\nAs long as the exiled card is a creature card, Duplicant has that card\'s power, toughness, and creature types. It\'s still a Shapeshifter.').
card_image_name('duplicant'/'CM1', 'duplicant').
card_uid('duplicant'/'CM1', 'CM1:Duplicant:duplicant').
card_rarity('duplicant'/'CM1', 'Rare').
card_artist('duplicant'/'CM1', 'Marco Nelor').
card_number('duplicant'/'CM1', '7').
card_multiverse_id('duplicant'/'CM1', '338451').

card_in_set('edric, spymaster of trest', 'CM1').
card_original_type('edric, spymaster of trest'/'CM1', 'Legendary Creature — Elf Rogue').
card_original_text('edric, spymaster of trest'/'CM1', 'Whenever a creature deals combat damage to one of your opponents, its controller may draw a card.').
card_image_name('edric, spymaster of trest'/'CM1', 'edric, spymaster of trest').
card_uid('edric, spymaster of trest'/'CM1', 'CM1:Edric, Spymaster of Trest:edric, spymaster of trest').
card_rarity('edric, spymaster of trest'/'CM1', 'Rare').
card_artist('edric, spymaster of trest'/'CM1', 'Volkan Baga').
card_number('edric, spymaster of trest'/'CM1', '8').
card_flavor_text('edric, spymaster of trest'/'CM1', '\"I am not at liberty to reveal my sources, but I can assure you, the price on your head is high.\"').
card_multiverse_id('edric, spymaster of trest'/'CM1', '338443').

card_in_set('kaalia of the vast', 'CM1').
card_original_type('kaalia of the vast'/'CM1', 'Legendary Creature — Human Cleric').
card_original_text('kaalia of the vast'/'CM1', 'Flying\nWhenever Kaalia of the Vast attacks an opponent, you may put an Angel, Demon, or Dragon creature card from your hand onto the battlefield tapped and attacking that opponent.').
card_image_name('kaalia of the vast'/'CM1', 'kaalia of the vast').
card_uid('kaalia of the vast'/'CM1', 'CM1:Kaalia of the Vast:kaalia of the vast').
card_rarity('kaalia of the vast'/'CM1', 'Mythic Rare').
card_artist('kaalia of the vast'/'CM1', 'Michael Komarck').
card_number('kaalia of the vast'/'CM1', '9').
card_flavor_text('kaalia of the vast'/'CM1', '\"I\'ll have my revenge if I have to call on every force from above and below.\"').
card_multiverse_id('kaalia of the vast'/'CM1', '338444').

card_in_set('loyal retainers', 'CM1').
card_original_type('loyal retainers'/'CM1', 'Creature — Human Advisor').
card_original_text('loyal retainers'/'CM1', 'Sacrifice Loyal Retainers: Return target legendary creature card from your graveyard to the battlefield. Activate this ability only during your turn, before attackers are declared.').
card_image_name('loyal retainers'/'CM1', 'loyal retainers').
card_uid('loyal retainers'/'CM1', 'CM1:Loyal Retainers:loyal retainers').
card_rarity('loyal retainers'/'CM1', 'Uncommon').
card_artist('loyal retainers'/'CM1', 'Solomon Au Yeung').
card_number('loyal retainers'/'CM1', '10').
card_multiverse_id('loyal retainers'/'CM1', '338450').

card_in_set('maelstrom wanderer', 'CM1').
card_original_type('maelstrom wanderer'/'CM1', 'Legendary Creature — Elemental').
card_original_text('maelstrom wanderer'/'CM1', 'Creatures you control have haste.\nCascade, cascade (When you cast this spell, exile cards from the top of your library until you exile a nonland card that costs less. You may cast it without paying its mana cost. Put the exiled cards on the bottom in a random order. Then do it again.)').
card_image_name('maelstrom wanderer'/'CM1', 'maelstrom wanderer').
card_uid('maelstrom wanderer'/'CM1', 'CM1:Maelstrom Wanderer:maelstrom wanderer').
card_rarity('maelstrom wanderer'/'CM1', 'Mythic Rare').
card_artist('maelstrom wanderer'/'CM1', 'Thomas M. Baxa').
card_number('maelstrom wanderer'/'CM1', '11').
card_multiverse_id('maelstrom wanderer'/'CM1', '338447').

card_in_set('mind\'s eye', 'CM1').
card_original_type('mind\'s eye'/'CM1', 'Artifact').
card_original_text('mind\'s eye'/'CM1', 'Whenever an opponent draws a card, you may pay {1}. If you do, draw a card.').
card_image_name('mind\'s eye'/'CM1', 'mind\'s eye').
card_uid('mind\'s eye'/'CM1', 'CM1:Mind\'s Eye:mind\'s eye').
card_rarity('mind\'s eye'/'CM1', 'Rare').
card_artist('mind\'s eye'/'CM1', 'Karl Kopinski').
card_number('mind\'s eye'/'CM1', '13').
card_flavor_text('mind\'s eye'/'CM1', '\"Ideas drift like petals on the wind. I have only to lift my face to the breeze.\"').
card_multiverse_id('mind\'s eye'/'CM1', '338452').

card_in_set('mirari\'s wake', 'CM1').
card_original_type('mirari\'s wake'/'CM1', 'Enchantment').
card_original_text('mirari\'s wake'/'CM1', 'Creatures you control get +1/+1.\nWhenever you tap a land for mana, add one mana to your mana pool of any type that land produced.').
card_image_name('mirari\'s wake'/'CM1', 'mirari\'s wake').
card_uid('mirari\'s wake'/'CM1', 'CM1:Mirari\'s Wake:mirari\'s wake').
card_rarity('mirari\'s wake'/'CM1', 'Rare').
card_artist('mirari\'s wake'/'CM1', 'Volkan Baga').
card_number('mirari\'s wake'/'CM1', '14').
card_flavor_text('mirari\'s wake'/'CM1', 'Even after a false god tore the magic from Dominaria, power still radiated from the Mirari sword that slew her.').
card_multiverse_id('mirari\'s wake'/'CM1', '338455').

card_in_set('rhystic study', 'CM1').
card_original_type('rhystic study'/'CM1', 'Enchantment').
card_original_text('rhystic study'/'CM1', 'Whenever an opponent casts a spell, you may draw a card unless that player pays {1}.').
card_image_name('rhystic study'/'CM1', 'rhystic study').
card_uid('rhystic study'/'CM1', 'CM1:Rhystic Study:rhystic study').
card_rarity('rhystic study'/'CM1', 'Common').
card_artist('rhystic study'/'CM1', 'Terese Nielsen').
card_number('rhystic study'/'CM1', '15').
card_flavor_text('rhystic study'/'CM1', 'Friends teach what you want to know. Enemies teach what you need to know.').
card_multiverse_id('rhystic study'/'CM1', '338457').

card_in_set('scroll rack', 'CM1').
card_original_type('scroll rack'/'CM1', 'Artifact').
card_original_text('scroll rack'/'CM1', '{1}, {T}: Exile any number of cards from your hand face down. Put that many cards from the top of your library into your hand. Then look at the exiled cards and put them on top of your library in any order.').
card_image_name('scroll rack'/'CM1', 'scroll rack').
card_uid('scroll rack'/'CM1', 'CM1:Scroll Rack:scroll rack').
card_rarity('scroll rack'/'CM1', 'Rare').
card_artist('scroll rack'/'CM1', 'Heather Hudson').
card_number('scroll rack'/'CM1', '16').
card_multiverse_id('scroll rack'/'CM1', '338458').

card_in_set('sylvan library', 'CM1').
card_original_type('sylvan library'/'CM1', 'Enchantment').
card_original_text('sylvan library'/'CM1', 'At the beginning of your draw step, you may draw two additional cards. If you do, choose two cards in your hand drawn this turn. For each of those cards, pay 4 life or put the card on top of your library.').
card_image_name('sylvan library'/'CM1', 'sylvan library').
card_uid('sylvan library'/'CM1', 'CM1:Sylvan Library:sylvan library').
card_rarity('sylvan library'/'CM1', 'Rare').
card_artist('sylvan library'/'CM1', 'Yeong-Hao Han').
card_number('sylvan library'/'CM1', '17').
card_multiverse_id('sylvan library'/'CM1', '338456').

card_in_set('the mimeoplasm', 'CM1').
card_original_type('the mimeoplasm'/'CM1', 'Legendary Creature — Ooze').
card_original_text('the mimeoplasm'/'CM1', 'As The Mimeoplasm enters the battlefield, you may exile two creature cards from graveyards. If you do, it enters the battlefield as a copy of one of those cards with a number of additional +1/+1 counters on it equal to the power of the other card.').
card_image_name('the mimeoplasm'/'CM1', 'the mimeoplasm').
card_uid('the mimeoplasm'/'CM1', 'CM1:The Mimeoplasm:the mimeoplasm').
card_rarity('the mimeoplasm'/'CM1', 'Mythic Rare').
card_artist('the mimeoplasm'/'CM1', 'Svetlin Velinov').
card_number('the mimeoplasm'/'CM1', '12').
card_multiverse_id('the mimeoplasm'/'CM1', '338445').

card_in_set('vela the night-clad', 'CM1').
card_original_type('vela the night-clad'/'CM1', 'Legendary Creature — Human Wizard').
card_original_text('vela the night-clad'/'CM1', 'Intimidate\nOther creatures you control have intimidate.\nWhenever Vela the Night-Clad or another creature you control leaves the battlefield, each opponent loses 1 life.').
card_image_name('vela the night-clad'/'CM1', 'vela the night-clad').
card_uid('vela the night-clad'/'CM1', 'CM1:Vela the Night-Clad:vela the night-clad').
card_rarity('vela the night-clad'/'CM1', 'Mythic Rare').
card_artist('vela the night-clad'/'CM1', 'Allen Williams').
card_number('vela the night-clad'/'CM1', '18').
card_flavor_text('vela the night-clad'/'CM1', 'Vela snuffs out every trace of Krond\'s reign, leaving pure nightfall in her wake.').
card_multiverse_id('vela the night-clad'/'CM1', '338448').
