% Tenth Edition

set('10E').
set_name('10E', 'Tenth Edition').
set_release_date('10E', '2007-07-13').
set_border('10E', 'black').
set_type('10E', 'core').

card_in_set('abundance', '10E').
card_original_type('abundance'/'10E', 'Enchantment').
card_original_text('abundance'/'10E', 'If you would draw a card, you may instead choose land or nonland and reveal cards from the top of your library until you reveal a card of the chosen kind. Put that card into your hand and put all other cards revealed this way on the bottom of your library in any order.').
card_image_name('abundance'/'10E', 'abundance').
card_uid('abundance'/'10E', '10E:Abundance:abundance').
card_rarity('abundance'/'10E', 'Rare').
card_artist('abundance'/'10E', 'Rebecca Guay').
card_number('abundance'/'10E', '249').
card_multiverse_id('abundance'/'10E', '130483').

card_in_set('academy researchers', '10E').
card_original_type('academy researchers'/'10E', 'Creature — Human Wizard').
card_original_text('academy researchers'/'10E', 'When Academy Researchers comes into play, you may put an Aura card from your hand into play attached to Academy Researchers.').
card_image_name('academy researchers'/'10E', 'academy researchers').
card_uid('academy researchers'/'10E', '10E:Academy Researchers:academy researchers').
card_rarity('academy researchers'/'10E', 'Uncommon').
card_artist('academy researchers'/'10E', 'Stephen Daniele').
card_number('academy researchers'/'10E', '63').
card_flavor_text('academy researchers'/'10E', 'They brandish their latest theories as warriors would wield weapons.').
card_multiverse_id('academy researchers'/'10E', '132072').

card_in_set('adarkar wastes', '10E').
card_original_type('adarkar wastes'/'10E', 'Land').
card_original_text('adarkar wastes'/'10E', '{T}: Add {1} to your mana pool.\n{T}: Add {W} or {U} to your mana pool. Adarkar Wastes deals 1 damage to you.').
card_image_name('adarkar wastes'/'10E', 'adarkar wastes').
card_uid('adarkar wastes'/'10E', '10E:Adarkar Wastes:adarkar wastes').
card_rarity('adarkar wastes'/'10E', 'Rare').
card_artist('adarkar wastes'/'10E', 'John Avon').
card_number('adarkar wastes'/'10E', '347').
card_multiverse_id('adarkar wastes'/'10E', '129458').

card_in_set('afflict', '10E').
card_original_type('afflict'/'10E', 'Instant').
card_original_text('afflict'/'10E', 'Target creature gets -1/-1 until end of turn.\nDraw a card.').
card_image_name('afflict'/'10E', 'afflict').
card_uid('afflict'/'10E', '10E:Afflict:afflict').
card_rarity('afflict'/'10E', 'Common').
card_artist('afflict'/'10E', 'Roger Raupp').
card_number('afflict'/'10E', '125').
card_flavor_text('afflict'/'10E', 'One rarely notices a heartbeat, save when it is stolen.').
card_multiverse_id('afflict'/'10E', '135206').

card_in_set('aggressive urge', '10E').
card_original_type('aggressive urge'/'10E', 'Instant').
card_original_text('aggressive urge'/'10E', 'Target creature gets +1/+1 until end of turn.\nDraw a card.').
card_image_name('aggressive urge'/'10E', 'aggressive urge').
card_uid('aggressive urge'/'10E', '10E:Aggressive Urge:aggressive urge').
card_rarity('aggressive urge'/'10E', 'Common').
card_artist('aggressive urge'/'10E', 'Christopher Moeller').
card_number('aggressive urge'/'10E', '250').
card_flavor_text('aggressive urge'/'10E', 'The power of the wild, concentrated in a single charge.').
card_multiverse_id('aggressive urge'/'10E', '130525').

card_in_set('agonizing memories', '10E').
card_original_type('agonizing memories'/'10E', 'Sorcery').
card_original_text('agonizing memories'/'10E', 'Look at target player\'s hand and choose two cards from it. Put them on top of that player\'s library in any order.').
card_image_name('agonizing memories'/'10E', 'agonizing memories').
card_uid('agonizing memories'/'10E', '10E:Agonizing Memories:agonizing memories').
card_rarity('agonizing memories'/'10E', 'Uncommon').
card_artist('agonizing memories'/'10E', 'Adam Rex').
card_number('agonizing memories'/'10E', '126').
card_flavor_text('agonizing memories'/'10E', 'In the aftermath of war, when the slaying is long done, the greatest miseries come home to roost.').
card_multiverse_id('agonizing memories'/'10E', '135228').

card_in_set('air elemental', '10E').
card_original_type('air elemental'/'10E', 'Creature — Elemental').
card_original_text('air elemental'/'10E', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)').
card_image_name('air elemental'/'10E', 'air elemental').
card_uid('air elemental'/'10E', '10E:Air Elemental:air elemental').
card_rarity('air elemental'/'10E', 'Uncommon').
card_artist('air elemental'/'10E', 'Kev Walker').
card_number('air elemental'/'10E', '64').
card_flavor_text('air elemental'/'10E', '\"The East Wind, an interloper in the dominions of Westerly Weather, is an impassive-faced tyrant with a sharp poniard held behind his back for a treacherous stab.\"\n—Joseph Conrad, The Mirror of the Sea').
card_multiverse_id('air elemental'/'10E', '129459').

card_in_set('ambassador laquatus', '10E').
card_original_type('ambassador laquatus'/'10E', 'Legendary Creature — Merfolk Wizard').
card_original_text('ambassador laquatus'/'10E', '{3}: Target player puts the top three cards of his or her library into his or her graveyard.').
card_image_name('ambassador laquatus'/'10E', 'ambassador laquatus').
card_uid('ambassador laquatus'/'10E', '10E:Ambassador Laquatus:ambassador laquatus').
card_rarity('ambassador laquatus'/'10E', 'Rare').
card_artist('ambassador laquatus'/'10E', 'Jim Murray').
card_number('ambassador laquatus'/'10E', '65').
card_flavor_text('ambassador laquatus'/'10E', '\"Life is a game. The only thing that matters is whether you\'re a pawn or a player.\"').
card_multiverse_id('ambassador laquatus'/'10E', '129913').

card_in_set('anaba bodyguard', '10E').
card_original_type('anaba bodyguard'/'10E', 'Creature — Minotaur').
card_original_text('anaba bodyguard'/'10E', 'First strike (This creature deals combat damage before creatures without first strike.)').
card_image_name('anaba bodyguard'/'10E', 'anaba bodyguard').
card_uid('anaba bodyguard'/'10E', '10E:Anaba Bodyguard:anaba bodyguard').
card_rarity('anaba bodyguard'/'10E', 'Common').
card_artist('anaba bodyguard'/'10E', 'Greg Staples').
card_number('anaba bodyguard'/'10E', '187').
card_flavor_text('anaba bodyguard'/'10E', '\"They who challenge a minotaur enjoy the taste of their own blood.\"\n—Mirri of the Weatherlight').
card_multiverse_id('anaba bodyguard'/'10E', '134753').

card_in_set('ancestor\'s chosen', '10E').
card_original_type('ancestor\'s chosen'/'10E', 'Creature — Human Cleric').
card_original_text('ancestor\'s chosen'/'10E', 'First strike (This creature deals combat damage before creatures without first strike.)\nWhen Ancestor\'s Chosen comes into play, you gain 1 life for each card in your graveyard.').
card_image_name('ancestor\'s chosen'/'10E', 'ancestor\'s chosen').
card_uid('ancestor\'s chosen'/'10E', '10E:Ancestor\'s Chosen:ancestor\'s chosen').
card_rarity('ancestor\'s chosen'/'10E', 'Uncommon').
card_artist('ancestor\'s chosen'/'10E', 'Pete Venters').
card_number('ancestor\'s chosen'/'10E', '1').
card_flavor_text('ancestor\'s chosen'/'10E', '\"The will of all, by my hand done.\"').
card_multiverse_id('ancestor\'s chosen'/'10E', '130550').

card_in_set('angel of mercy', '10E').
card_original_type('angel of mercy'/'10E', 'Creature — Angel').
card_original_text('angel of mercy'/'10E', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nWhen Angel of Mercy comes into play, you gain 3 life.').
card_image_name('angel of mercy'/'10E', 'angel of mercy').
card_uid('angel of mercy'/'10E', '10E:Angel of Mercy:angel of mercy').
card_rarity('angel of mercy'/'10E', 'Uncommon').
card_artist('angel of mercy'/'10E', 'Volkan Baga').
card_number('angel of mercy'/'10E', '2').
card_flavor_text('angel of mercy'/'10E', 'Every tear shed is a drop of immortality.').
card_multiverse_id('angel of mercy'/'10E', '129465').

card_in_set('angel\'s feather', '10E').
card_original_type('angel\'s feather'/'10E', 'Artifact').
card_original_text('angel\'s feather'/'10E', 'Whenever a player plays a white spell, you may gain 1 life.').
card_image_name('angel\'s feather'/'10E', 'angel\'s feather').
card_uid('angel\'s feather'/'10E', '10E:Angel\'s Feather:angel\'s feather').
card_rarity('angel\'s feather'/'10E', 'Uncommon').
card_artist('angel\'s feather'/'10E', 'Alan Pollack').
card_number('angel\'s feather'/'10E', '311').
card_flavor_text('angel\'s feather'/'10E', 'If taken, it cuts the hand that clutches it. If given, it heals the hand that holds it.').
card_multiverse_id('angel\'s feather'/'10E', '129466').

card_in_set('angelic blessing', '10E').
card_original_type('angelic blessing'/'10E', 'Sorcery').
card_original_text('angelic blessing'/'10E', 'Target creature gets +3/+3 and gains flying until end of turn. (It can\'t be blocked except by creatures with flying or reach.)').
card_image_name('angelic blessing'/'10E', 'angelic blessing').
card_uid('angelic blessing'/'10E', '10E:Angelic Blessing:angelic blessing').
card_rarity('angelic blessing'/'10E', 'Common').
card_artist('angelic blessing'/'10E', 'Mark Zug').
card_number('angelic blessing'/'10E', '3').
card_flavor_text('angelic blessing'/'10E', 'Only the warrior who can admit mortal weakness will be bolstered by immortal strength.').
card_multiverse_id('angelic blessing'/'10E', '129711').

card_in_set('angelic chorus', '10E').
card_original_type('angelic chorus'/'10E', 'Enchantment').
card_original_text('angelic chorus'/'10E', 'Whenever a creature comes into play under your control, you gain life equal to its toughness.').
card_image_name('angelic chorus'/'10E', 'angelic chorus').
card_uid('angelic chorus'/'10E', '10E:Angelic Chorus:angelic chorus').
card_rarity('angelic chorus'/'10E', 'Rare').
card_artist('angelic chorus'/'10E', 'Jim Murray').
card_number('angelic chorus'/'10E', '4').
card_flavor_text('angelic chorus'/'10E', 'The harmony of the glorious is a dirge to the wicked.').
card_multiverse_id('angelic chorus'/'10E', '129710').

card_in_set('angelic wall', '10E').
card_original_type('angelic wall'/'10E', 'Creature — Wall').
card_original_text('angelic wall'/'10E', 'Defender, flying (This creature can\'t attack, and it can block creatures with flying.)').
card_image_name('angelic wall'/'10E', 'angelic wall').
card_uid('angelic wall'/'10E', '10E:Angelic Wall:angelic wall').
card_rarity('angelic wall'/'10E', 'Common').
card_artist('angelic wall'/'10E', 'John Avon').
card_number('angelic wall'/'10E', '5').
card_flavor_text('angelic wall'/'10E', '\"The Ancestor protects us in ways we can\'t begin to comprehend.\"\n—Mystic elder').
card_multiverse_id('angelic wall'/'10E', '129671').

card_in_set('arcane teachings', '10E').
card_original_type('arcane teachings'/'10E', 'Enchantment — Aura').
card_original_text('arcane teachings'/'10E', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nEnchanted creature gets +2/+2 and has \"{T}: This creature deals 1 damage to target creature or player.\"').
card_image_name('arcane teachings'/'10E', 'arcane teachings').
card_uid('arcane teachings'/'10E', '10E:Arcane Teachings:arcane teachings').
card_rarity('arcane teachings'/'10E', 'Uncommon').
card_artist('arcane teachings'/'10E', 'Dan Dos Santos').
card_number('arcane teachings'/'10E', '188').
card_flavor_text('arcane teachings'/'10E', 'Not all knowledge is learned from parchment.').
card_multiverse_id('arcane teachings'/'10E', '130530').

card_in_set('arcanis the omnipotent', '10E').
card_original_type('arcanis the omnipotent'/'10E', 'Legendary Creature — Wizard').
card_original_text('arcanis the omnipotent'/'10E', '{T}: Draw three cards.\n{2}{U}{U}: Return Arcanis the Omnipotent to its owner\'s hand.').
card_image_name('arcanis the omnipotent'/'10E', 'arcanis the omnipotent').
card_uid('arcanis the omnipotent'/'10E', '10E:Arcanis the Omnipotent:arcanis the omnipotent').
card_rarity('arcanis the omnipotent'/'10E', 'Rare').
card_artist('arcanis the omnipotent'/'10E', 'Justin Sweet').
card_number('arcanis the omnipotent'/'10E', '66').
card_flavor_text('arcanis the omnipotent'/'10E', '\"Do not concern yourself with my origin, my race, or my ancestry. Seek my record in the pits, and then make your wager.\"').
card_multiverse_id('arcanis the omnipotent'/'10E', '106426').

card_in_set('ascendant evincar', '10E').
card_original_type('ascendant evincar'/'10E', 'Legendary Creature — Vampire').
card_original_text('ascendant evincar'/'10E', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nOther black creatures get +1/+1. \nNonblack creatures get -1/-1.').
card_image_name('ascendant evincar'/'10E', 'ascendant evincar').
card_uid('ascendant evincar'/'10E', '10E:Ascendant Evincar:ascendant evincar').
card_rarity('ascendant evincar'/'10E', 'Rare').
card_artist('ascendant evincar'/'10E', 'Mark Zug').
card_number('ascendant evincar'/'10E', '127').
card_flavor_text('ascendant evincar'/'10E', 'His soul snared by an angel\'s curse, Crovax twisted heroism into its purest shadow.').
card_multiverse_id('ascendant evincar'/'10E', '106525').

card_in_set('assassinate', '10E').
card_original_type('assassinate'/'10E', 'Sorcery').
card_original_text('assassinate'/'10E', 'Destroy target tapped creature.').
card_image_name('assassinate'/'10E', 'assassinate').
card_uid('assassinate'/'10E', '10E:Assassinate:assassinate').
card_rarity('assassinate'/'10E', 'Common').
card_artist('assassinate'/'10E', 'Kev Walker').
card_number('assassinate'/'10E', '128').
card_flavor_text('assassinate'/'10E', '\"This is how wars are won—not with armies of soldiers but with a single knife blade, artfully placed.\"\n—Yurin, royal assassin').
card_multiverse_id('assassinate'/'10E', '135194').

card_in_set('aura graft', '10E').
card_original_type('aura graft'/'10E', 'Instant').
card_original_text('aura graft'/'10E', 'Gain control of target Aura that\'s attached to a permanent. Attach it to another permanent it can enchant.').
card_image_name('aura graft'/'10E', 'aura graft').
card_uid('aura graft'/'10E', '10E:Aura Graft:aura graft').
card_rarity('aura graft'/'10E', 'Uncommon').
card_artist('aura graft'/'10E', 'Ray Lago').
card_number('aura graft'/'10E', '67').
card_flavor_text('aura graft'/'10E', '\"It\'s not really stealing. It\'s more like extended borrowing.\"').
card_multiverse_id('aura graft'/'10E', '130976').

card_in_set('aura of silence', '10E').
card_original_type('aura of silence'/'10E', 'Enchantment').
card_original_text('aura of silence'/'10E', 'Artifact and enchantment spells your opponents play cost {2} more to play.\nSacrifice Aura of Silence: Destroy target artifact or enchantment.').
card_image_name('aura of silence'/'10E', 'aura of silence').
card_uid('aura of silence'/'10E', '10E:Aura of Silence:aura of silence').
card_rarity('aura of silence'/'10E', 'Uncommon').
card_artist('aura of silence'/'10E', 'D. Alexander Gregory').
card_number('aura of silence'/'10E', '6').
card_flavor_text('aura of silence'/'10E', 'Not all silences are easily broken.').
card_multiverse_id('aura of silence'/'10E', '132127').

card_in_set('avatar of might', '10E').
card_original_type('avatar of might'/'10E', 'Creature — Avatar').
card_original_text('avatar of might'/'10E', 'If an opponent controls at least four more creatures than you, Avatar of Might costs {6} less to play.\nTrample (If this creature would deal enough combat damage to its blockers to destroy them, you may have it deal the rest of its damage to defending player.)').
card_image_name('avatar of might'/'10E', 'avatar of might').
card_uid('avatar of might'/'10E', '10E:Avatar of Might:avatar of might').
card_rarity('avatar of might'/'10E', 'Rare').
card_artist('avatar of might'/'10E', 'rk post').
card_number('avatar of might'/'10E', '251').
card_flavor_text('avatar of might'/'10E', 'In their most desperate hour, the elves of Llanowar had one song left to sing.').
card_multiverse_id('avatar of might'/'10E', '135249').

card_in_set('aven cloudchaser', '10E').
card_original_type('aven cloudchaser'/'10E', 'Creature — Bird Soldier').
card_original_text('aven cloudchaser'/'10E', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.) \nWhen Aven Cloudchaser comes into play, destroy target enchantment.').
card_image_name('aven cloudchaser'/'10E', 'aven cloudchaser').
card_uid('aven cloudchaser'/'10E', '10E:Aven Cloudchaser:aven cloudchaser').
card_rarity('aven cloudchaser'/'10E', 'Common').
card_artist('aven cloudchaser'/'10E', 'Justin Sweet').
card_number('aven cloudchaser'/'10E', '7').
card_flavor_text('aven cloudchaser'/'10E', '\"At the Reapportionment, Eagle begged to be human. The Ancestor granted half that prayer.\"\n—Nomad myth').
card_multiverse_id('aven cloudchaser'/'10E', '129470').

card_in_set('aven fisher', '10E').
card_original_type('aven fisher'/'10E', 'Creature — Bird Soldier').
card_original_text('aven fisher'/'10E', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.) \nWhen Aven Fisher is put into a graveyard from play, you may draw a card.').
card_image_name('aven fisher'/'10E', 'aven fisher').
card_uid('aven fisher'/'10E', '10E:Aven Fisher:aven fisher').
card_rarity('aven fisher'/'10E', 'Common').
card_artist('aven fisher'/'10E', 'Christopher Moeller').
card_number('aven fisher'/'10E', '68').
card_flavor_text('aven fisher'/'10E', 'The same spears that catch their food today will defend their homes tomorrow.').
card_multiverse_id('aven fisher'/'10E', '130985').

card_in_set('aven windreader', '10E').
card_original_type('aven windreader'/'10E', 'Creature — Bird Soldier Wizard').
card_original_text('aven windreader'/'10E', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\n{1}{U}: Target player reveals the top card of his or her library.').
card_image_name('aven windreader'/'10E', 'aven windreader').
card_uid('aven windreader'/'10E', '10E:Aven Windreader:aven windreader').
card_rarity('aven windreader'/'10E', 'Common').
card_artist('aven windreader'/'10E', 'Greg Hildebrandt').
card_number('aven windreader'/'10E', '69').
card_flavor_text('aven windreader'/'10E', '\"The tiniest ripple tells a story ten fathoms deep.\"').
card_multiverse_id('aven windreader'/'10E', '129473').

card_in_set('ballista squad', '10E').
card_original_type('ballista squad'/'10E', 'Creature — Human Rebel').
card_original_text('ballista squad'/'10E', '{X}{W}, {T}: Ballista Squad deals X damage to target attacking or blocking creature.').
card_image_name('ballista squad'/'10E', 'ballista squad').
card_uid('ballista squad'/'10E', '10E:Ballista Squad:ballista squad').
card_rarity('ballista squad'/'10E', 'Uncommon').
card_artist('ballista squad'/'10E', 'Matthew D. Wilson').
card_number('ballista squad'/'10E', '8').
card_flavor_text('ballista squad'/'10E', 'The perfect antidote for a tightly packed formation.').
card_multiverse_id('ballista squad'/'10E', '129477').

card_in_set('bandage', '10E').
card_original_type('bandage'/'10E', 'Instant').
card_original_text('bandage'/'10E', 'Prevent the next 1 damage that would be dealt to target creature or player this turn.\nDraw a card.').
card_image_name('bandage'/'10E', 'bandage').
card_uid('bandage'/'10E', '10E:Bandage:bandage').
card_rarity('bandage'/'10E', 'Common').
card_artist('bandage'/'10E', 'Rebecca Guay').
card_number('bandage'/'10E', '9').
card_flavor_text('bandage'/'10E', 'Life is measured in inches. To a healer, every one of those inches is precious.').
card_multiverse_id('bandage'/'10E', '132106').

card_in_set('battlefield forge', '10E').
card_original_type('battlefield forge'/'10E', 'Land').
card_original_text('battlefield forge'/'10E', '{T}: Add {1} to your mana pool.\n{T}: Add {R} or {W} to your mana pool. Battlefield Forge deals 1 damage to you.').
card_image_name('battlefield forge'/'10E', 'battlefield forge').
card_uid('battlefield forge'/'10E', '10E:Battlefield Forge:battlefield forge').
card_rarity('battlefield forge'/'10E', 'Rare').
card_artist('battlefield forge'/'10E', 'Darrell Riche').
card_number('battlefield forge'/'10E', '348').
card_multiverse_id('battlefield forge'/'10E', '129479').

card_in_set('beacon of destruction', '10E').
card_original_type('beacon of destruction'/'10E', 'Instant').
card_original_text('beacon of destruction'/'10E', 'Beacon of Destruction deals 5 damage to target creature or player. Shuffle Beacon of Destruction into its owner\'s library.').
card_image_name('beacon of destruction'/'10E', 'beacon of destruction').
card_uid('beacon of destruction'/'10E', '10E:Beacon of Destruction:beacon of destruction').
card_rarity('beacon of destruction'/'10E', 'Rare').
card_artist('beacon of destruction'/'10E', 'Greg Hildebrandt').
card_number('beacon of destruction'/'10E', '189').
card_flavor_text('beacon of destruction'/'10E', 'The Great Furnace\'s blessing is a spectacular sight, but the best view comes at a high cost.').
card_multiverse_id('beacon of destruction'/'10E', '135262').

card_in_set('beacon of immortality', '10E').
card_original_type('beacon of immortality'/'10E', 'Instant').
card_original_text('beacon of immortality'/'10E', 'Double target player\'s life total. Shuffle Beacon of Immortality into its owner\'s library.').
card_image_name('beacon of immortality'/'10E', 'beacon of immortality').
card_uid('beacon of immortality'/'10E', '10E:Beacon of Immortality:beacon of immortality').
card_rarity('beacon of immortality'/'10E', 'Rare').
card_artist('beacon of immortality'/'10E', 'Rob Alexander').
card_number('beacon of immortality'/'10E', '10').
card_flavor_text('beacon of immortality'/'10E', 'The cave floods with light. A thousand rays shine forth and meld into one.').
card_multiverse_id('beacon of immortality'/'10E', '130553').

card_in_set('beacon of unrest', '10E').
card_original_type('beacon of unrest'/'10E', 'Sorcery').
card_original_text('beacon of unrest'/'10E', 'Put target artifact or creature card in a graveyard into play under your control. Shuffle Beacon of Unrest into its owner\'s library.').
card_image_name('beacon of unrest'/'10E', 'beacon of unrest').
card_uid('beacon of unrest'/'10E', '10E:Beacon of Unrest:beacon of unrest').
card_rarity('beacon of unrest'/'10E', 'Rare').
card_artist('beacon of unrest'/'10E', 'Alan Pollack').
card_number('beacon of unrest'/'10E', '129').
card_flavor_text('beacon of unrest'/'10E', 'A vertical scream pierces the night air and echoes doom through the clouds.').
card_multiverse_id('beacon of unrest'/'10E', '135270').

card_in_set('benalish knight', '10E').
card_original_type('benalish knight'/'10E', 'Creature — Human Knight').
card_original_text('benalish knight'/'10E', 'Flash (You may play this spell any time you could play an instant.)\nFirst strike (This creature deals combat damage before creatures without first strike.)').
card_image_name('benalish knight'/'10E', 'benalish knight').
card_uid('benalish knight'/'10E', '10E:Benalish Knight:benalish knight').
card_rarity('benalish knight'/'10E', 'Common').
card_artist('benalish knight'/'10E', 'Zoltan Boros & Gabor Szikszai').
card_number('benalish knight'/'10E', '11').
card_flavor_text('benalish knight'/'10E', '\"We called them ‘armored lightning.\'\"\n—Gerrard of the Weatherlight').
card_multiverse_id('benalish knight'/'10E', '136279').

card_in_set('birds of paradise', '10E').
card_original_type('birds of paradise'/'10E', 'Creature — Bird').
card_original_text('birds of paradise'/'10E', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\n{T}: Add one mana of any color to your mana pool.').
card_image_name('birds of paradise'/'10E', 'birds of paradise').
card_uid('birds of paradise'/'10E', '10E:Birds of Paradise:birds of paradise').
card_rarity('birds of paradise'/'10E', 'Rare').
card_artist('birds of paradise'/'10E', 'Marcelo Vignali').
card_number('birds of paradise'/'10E', '252').
card_multiverse_id('birds of paradise'/'10E', '129906').

card_in_set('blanchwood armor', '10E').
card_original_type('blanchwood armor'/'10E', 'Enchantment — Aura').
card_original_text('blanchwood armor'/'10E', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nEnchanted creature gets +1/+1 for each Forest you control.').
card_image_name('blanchwood armor'/'10E', 'blanchwood armor').
card_uid('blanchwood armor'/'10E', '10E:Blanchwood Armor:blanchwood armor').
card_rarity('blanchwood armor'/'10E', 'Uncommon').
card_artist('blanchwood armor'/'10E', 'Paolo Parente').
card_number('blanchwood armor'/'10E', '253').
card_flavor_text('blanchwood armor'/'10E', '\"Before armor, there was bark. Before blades, there were thorns.\"\n—Molimo, maro-sorcerer').
card_multiverse_id('blanchwood armor'/'10E', '135267').

card_in_set('blaze', '10E').
card_original_type('blaze'/'10E', 'Sorcery').
card_original_text('blaze'/'10E', 'Blaze deals X damage to target creature or player.').
card_image_name('blaze'/'10E', 'blaze').
card_uid('blaze'/'10E', '10E:Blaze:blaze').
card_rarity('blaze'/'10E', 'Uncommon').
card_artist('blaze'/'10E', 'Alex Horley-Orlandelli').
card_number('blaze'/'10E', '190').
card_multiverse_id('blaze'/'10E', '129484').

card_in_set('bloodfire colossus', '10E').
card_original_type('bloodfire colossus'/'10E', 'Creature — Giant').
card_original_text('bloodfire colossus'/'10E', '{R}, Sacrifice Bloodfire Colossus: Bloodfire Colossus deals 6 damage to each creature and each player.').
card_image_name('bloodfire colossus'/'10E', 'bloodfire colossus').
card_uid('bloodfire colossus'/'10E', '10E:Bloodfire Colossus:bloodfire colossus').
card_rarity('bloodfire colossus'/'10E', 'Rare').
card_artist('bloodfire colossus'/'10E', 'Greg Staples').
card_number('bloodfire colossus'/'10E', '191').
card_flavor_text('bloodfire colossus'/'10E', 'It took all its strength to contain the fire within.').
card_multiverse_id('bloodfire colossus'/'10E', '129709').

card_in_set('bloodrock cyclops', '10E').
card_original_type('bloodrock cyclops'/'10E', 'Creature — Cyclops').
card_original_text('bloodrock cyclops'/'10E', 'Bloodrock Cyclops attacks each turn if able.').
card_image_name('bloodrock cyclops'/'10E', 'bloodrock cyclops').
card_uid('bloodrock cyclops'/'10E', '10E:Bloodrock Cyclops:bloodrock cyclops').
card_rarity('bloodrock cyclops'/'10E', 'Common').
card_artist('bloodrock cyclops'/'10E', 'Alex Horley-Orlandelli').
card_number('bloodrock cyclops'/'10E', '192').
card_flavor_text('bloodrock cyclops'/'10E', '\"There are only fifty words in the cyclops language, and ten of them mean ‘kill.\'\"\n—Ertai').
card_multiverse_id('bloodrock cyclops'/'10E', '130384').

card_in_set('bog wraith', '10E').
card_original_type('bog wraith'/'10E', 'Creature — Wraith').
card_original_text('bog wraith'/'10E', 'Swampwalk (This creature is unblockable as long as defending player controls a Swamp.)').
card_image_name('bog wraith'/'10E', 'bog wraith').
card_uid('bog wraith'/'10E', '10E:Bog Wraith:bog wraith').
card_rarity('bog wraith'/'10E', 'Uncommon').
card_artist('bog wraith'/'10E', 'Daarken').
card_number('bog wraith'/'10E', '130').
card_flavor_text('bog wraith'/'10E', 'Knowing Takenuma Swamp to be dangerous, Hisata set wards to warn him of predators. He never imagined that his murderer would pass through them unhindered.').
card_multiverse_id('bog wraith'/'10E', '129491').

card_in_set('bogardan firefiend', '10E').
card_original_type('bogardan firefiend'/'10E', 'Creature — Elemental Spirit').
card_original_text('bogardan firefiend'/'10E', 'When Bogardan Firefiend is put into a graveyard from play, it deals 2 damage to target creature.').
card_image_name('bogardan firefiend'/'10E', 'bogardan firefiend').
card_uid('bogardan firefiend'/'10E', '10E:Bogardan Firefiend:bogardan firefiend').
card_rarity('bogardan firefiend'/'10E', 'Common').
card_artist('bogardan firefiend'/'10E', 'Terese Nielsen').
card_number('bogardan firefiend'/'10E', '193').
card_flavor_text('bogardan firefiend'/'10E', '\"The next one who tells me to relax and curl up by a fire is dead.\"\n—Mirri of the Weatherlight').
card_multiverse_id('bogardan firefiend'/'10E', '130534').

card_in_set('boomerang', '10E').
card_original_type('boomerang'/'10E', 'Instant').
card_original_text('boomerang'/'10E', 'Return target permanent to its owner\'s hand.').
card_image_name('boomerang'/'10E', 'boomerang').
card_uid('boomerang'/'10E', '10E:Boomerang:boomerang').
card_rarity('boomerang'/'10E', 'Common').
card_artist('boomerang'/'10E', 'Arnie Swekel').
card_number('boomerang'/'10E', '70').
card_flavor_text('boomerang'/'10E', 'Early Jamuraan hunters devised a weapon that would return to its source. Tolarian Æthermancers developed a spell that skipped the weapon entirely.').
card_multiverse_id('boomerang'/'10E', '129494').

card_in_set('bottle gnomes', '10E').
card_original_type('bottle gnomes'/'10E', 'Artifact Creature — Gnome').
card_original_text('bottle gnomes'/'10E', 'Sacrifice Bottle Gnomes: You gain 3 life.').
card_image_name('bottle gnomes'/'10E', 'bottle gnomes').
card_uid('bottle gnomes'/'10E', '10E:Bottle Gnomes:bottle gnomes').
card_rarity('bottle gnomes'/'10E', 'Uncommon').
card_artist('bottle gnomes'/'10E', 'Ben Thompson').
card_number('bottle gnomes'/'10E', '312').
card_flavor_text('bottle gnomes'/'10E', 'Reinforcements . . . or refreshments?').
card_multiverse_id('bottle gnomes'/'10E', '129495').

card_in_set('brushland', '10E').
card_original_type('brushland'/'10E', 'Land').
card_original_text('brushland'/'10E', '{T}: Add {1} to your mana pool.\n{T}: Add {G} or {W} to your mana pool. Brushland deals 1 damage to you.').
card_image_name('brushland'/'10E', 'brushland').
card_uid('brushland'/'10E', '10E:Brushland:brushland').
card_rarity('brushland'/'10E', 'Rare').
card_artist('brushland'/'10E', 'Scott Bailey').
card_number('brushland'/'10E', '349').
card_multiverse_id('brushland'/'10E', '129496').

card_in_set('cancel', '10E').
card_original_type('cancel'/'10E', 'Instant').
card_original_text('cancel'/'10E', 'Counter target spell.').
card_image_name('cancel'/'10E', 'cancel').
card_uid('cancel'/'10E', '10E:Cancel:cancel').
card_rarity('cancel'/'10E', 'Common').
card_artist('cancel'/'10E', 'Mark Poole').
card_number('cancel'/'10E', '71').
card_multiverse_id('cancel'/'10E', '129882').

card_in_set('canopy spider', '10E').
card_original_type('canopy spider'/'10E', 'Creature — Spider').
card_original_text('canopy spider'/'10E', 'Reach (This creature can block creatures with flying.)').
card_image_name('canopy spider'/'10E', 'canopy spider').
card_uid('canopy spider'/'10E', '10E:Canopy Spider:canopy spider').
card_rarity('canopy spider'/'10E', 'Common').
card_artist('canopy spider'/'10E', 'Christopher Rush').
card_number('canopy spider'/'10E', '254').
card_flavor_text('canopy spider'/'10E', 'It keeps the upper reaches of the forest free of every menace . . . except for the spider itself.').
card_multiverse_id('canopy spider'/'10E', '135185').

card_in_set('caves of koilos', '10E').
card_original_type('caves of koilos'/'10E', 'Land').
card_original_text('caves of koilos'/'10E', '{T}: Add {1} to your mana pool.\n{T}: Add {W} or {B} to your mana pool. Caves of Koilos deals 1 damage to you.').
card_image_name('caves of koilos'/'10E', 'caves of koilos').
card_uid('caves of koilos'/'10E', '10E:Caves of Koilos:caves of koilos').
card_rarity('caves of koilos'/'10E', 'Rare').
card_artist('caves of koilos'/'10E', 'Jim Nelson').
card_number('caves of koilos'/'10E', '350').
card_multiverse_id('caves of koilos'/'10E', '129497').

card_in_set('cephalid constable', '10E').
card_original_type('cephalid constable'/'10E', 'Creature — Cephalid Wizard').
card_original_text('cephalid constable'/'10E', 'Whenever Cephalid Constable deals combat damage to a player, return up to that many target permanents that player controls to their owners\' hands.').
card_image_name('cephalid constable'/'10E', 'cephalid constable').
card_uid('cephalid constable'/'10E', '10E:Cephalid Constable:cephalid constable').
card_rarity('cephalid constable'/'10E', 'Rare').
card_artist('cephalid constable'/'10E', 'Alan Pollack').
card_number('cephalid constable'/'10E', '72').
card_flavor_text('cephalid constable'/'10E', 'Cephalids don\'t police people. They police loyalties.').
card_multiverse_id('cephalid constable'/'10E', '135261').

card_in_set('chimeric staff', '10E').
card_original_type('chimeric staff'/'10E', 'Artifact').
card_original_text('chimeric staff'/'10E', '{X}: Chimeric Staff becomes an X/X Construct artifact creature until end of turn.').
card_image_name('chimeric staff'/'10E', 'chimeric staff').
card_uid('chimeric staff'/'10E', '10E:Chimeric Staff:chimeric staff').
card_rarity('chimeric staff'/'10E', 'Rare').
card_artist('chimeric staff'/'10E', 'Michael Sutfin').
card_number('chimeric staff'/'10E', '313').
card_flavor_text('chimeric staff'/'10E', 'The staff unraveled in a chaotic chorus of clanging, becoming an improbable beast of hissing blades.').
card_multiverse_id('chimeric staff'/'10E', '135254').

card_in_set('cho-manno, revolutionary', '10E').
card_original_type('cho-manno, revolutionary'/'10E', 'Legendary Creature — Human Rebel').
card_original_text('cho-manno, revolutionary'/'10E', 'Prevent all damage that would be dealt to Cho-Manno, Revolutionary.').
card_image_name('cho-manno, revolutionary'/'10E', 'cho-manno, revolutionary').
card_uid('cho-manno, revolutionary'/'10E', '10E:Cho-Manno, Revolutionary:cho-manno, revolutionary').
card_rarity('cho-manno, revolutionary'/'10E', 'Rare').
card_artist('cho-manno, revolutionary'/'10E', 'Steven Belledin').
card_number('cho-manno, revolutionary'/'10E', '12').
card_flavor_text('cho-manno, revolutionary'/'10E', '\"Mercadia\'s masks can no longer hide the truth. Our day has come at last.\"').
card_multiverse_id('cho-manno, revolutionary'/'10E', '130554').

card_in_set('chromatic star', '10E').
card_original_type('chromatic star'/'10E', 'Artifact').
card_original_text('chromatic star'/'10E', '{1}, {T}, Sacrifice Chromatic Star: Add one mana of any color to your mana pool.\nWhen Chromatic Star is put into a graveyard from play, draw a card.').
card_image_name('chromatic star'/'10E', 'chromatic star').
card_uid('chromatic star'/'10E', '10E:Chromatic Star:chromatic star').
card_rarity('chromatic star'/'10E', 'Uncommon').
card_artist('chromatic star'/'10E', 'Alex Horley-Orlandelli').
card_number('chromatic star'/'10E', '314').
card_multiverse_id('chromatic star'/'10E', '135279').

card_in_set('citanul flute', '10E').
card_original_type('citanul flute'/'10E', 'Artifact').
card_original_text('citanul flute'/'10E', '{X}, {T}: Search your library for a creature card with converted mana cost X or less, reveal it, and put it into your hand. Then shuffle your library.').
card_image_name('citanul flute'/'10E', 'citanul flute').
card_uid('citanul flute'/'10E', '10E:Citanul Flute:citanul flute').
card_rarity('citanul flute'/'10E', 'Rare').
card_artist('citanul flute'/'10E', 'Greg Hildebrandt').
card_number('citanul flute'/'10E', '315').
card_flavor_text('citanul flute'/'10E', 'Each note of the flute mimics the call of a different beast.').
card_multiverse_id('citanul flute'/'10E', '135244').

card_in_set('civic wayfinder', '10E').
card_original_type('civic wayfinder'/'10E', 'Creature — Elf Warrior Druid').
card_original_text('civic wayfinder'/'10E', 'When Civic Wayfinder comes into play, you may search your library for a basic land card, reveal it, and put it into your hand. If you do, shuffle your library.').
card_image_name('civic wayfinder'/'10E', 'civic wayfinder').
card_uid('civic wayfinder'/'10E', '10E:Civic Wayfinder:civic wayfinder').
card_rarity('civic wayfinder'/'10E', 'Common').
card_artist('civic wayfinder'/'10E', 'Cyril Van Der Haegen').
card_number('civic wayfinder'/'10E', '255').
card_flavor_text('civic wayfinder'/'10E', '\"These alleys are not safe. Come, I can guide you back to the market square.\"').
card_multiverse_id('civic wayfinder'/'10E', '130522').

card_in_set('clone', '10E').
card_original_type('clone'/'10E', 'Creature — Shapeshifter').
card_original_text('clone'/'10E', 'As Clone comes into play, you may choose a creature in play. If you do, Clone comes into play as a copy of that creature.').
card_image_name('clone'/'10E', 'clone').
card_uid('clone'/'10E', '10E:Clone:clone').
card_rarity('clone'/'10E', 'Rare').
card_artist('clone'/'10E', 'Kev Walker').
card_number('clone'/'10E', '73').
card_multiverse_id('clone'/'10E', '129501').

card_in_set('cloud elemental', '10E').
card_original_type('cloud elemental'/'10E', 'Creature — Elemental').
card_original_text('cloud elemental'/'10E', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nCloud Elemental can block only creatures with flying.').
card_image_name('cloud elemental'/'10E', 'cloud elemental').
card_uid('cloud elemental'/'10E', '10E:Cloud Elemental:cloud elemental').
card_rarity('cloud elemental'/'10E', 'Common').
card_artist('cloud elemental'/'10E', 'Michael Sutfin').
card_number('cloud elemental'/'10E', '74').
card_flavor_text('cloud elemental'/'10E', 'The soratami surround their sky-castles with living clouds that serve as both sword and shield.').
card_multiverse_id('cloud elemental'/'10E', '129804').

card_in_set('cloud sprite', '10E').
card_original_type('cloud sprite'/'10E', 'Creature — Faerie').
card_original_text('cloud sprite'/'10E', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nCloud Sprite can block only creatures with flying.').
card_image_name('cloud sprite'/'10E', 'cloud sprite').
card_uid('cloud sprite'/'10E', '10E:Cloud Sprite:cloud sprite').
card_rarity('cloud sprite'/'10E', 'Common').
card_artist('cloud sprite'/'10E', 'Mark Zug').
card_number('cloud sprite'/'10E', '75').
card_flavor_text('cloud sprite'/'10E', 'The delicate sprites carry messages for Saprazzans, but they refuse to land in Mercadia City\'s filthy markets.').
card_multiverse_id('cloud sprite'/'10E', '132069').

card_in_set('coat of arms', '10E').
card_original_type('coat of arms'/'10E', 'Artifact').
card_original_text('coat of arms'/'10E', 'Each creature gets +1/+1 for each other creature in play that shares a creature type with it. (For example, if a Goblin Warrior, a Goblin Scout, and a Zombie Goblin are in play, each gets +2/+2.)').
card_image_name('coat of arms'/'10E', 'coat of arms').
card_uid('coat of arms'/'10E', '10E:Coat of Arms:coat of arms').
card_rarity('coat of arms'/'10E', 'Rare').
card_artist('coat of arms'/'10E', 'Scott M. Fischer').
card_number('coat of arms'/'10E', '316').
card_flavor_text('coat of arms'/'10E', '\"Hup, two, three, four,\nDunno how to count no more.\"').
card_multiverse_id('coat of arms'/'10E', '129502').

card_in_set('colossus of sardia', '10E').
card_original_type('colossus of sardia'/'10E', 'Artifact Creature — Golem').
card_original_text('colossus of sardia'/'10E', 'Trample (If this creature would deal enough combat damage to its blockers to destroy them, you may have it deal the rest of its damage to defending player.)\nColossus of Sardia doesn\'t untap during your untap step.\n{9}: Untap Colossus of Sardia. Play this ability only during your upkeep.').
card_image_name('colossus of sardia'/'10E', 'colossus of sardia').
card_uid('colossus of sardia'/'10E', '10E:Colossus of Sardia:colossus of sardia').
card_rarity('colossus of sardia'/'10E', 'Rare').
card_artist('colossus of sardia'/'10E', 'Greg Staples').
card_number('colossus of sardia'/'10E', '317').
card_flavor_text('colossus of sardia'/'10E', 'Buried under a thin layer of dirt, it was known for centuries as Mount Sardia.').
card_multiverse_id('colossus of sardia'/'10E', '135268').

card_in_set('commune with nature', '10E').
card_original_type('commune with nature'/'10E', 'Sorcery').
card_original_text('commune with nature'/'10E', 'Look at the top five cards of your library. You may reveal a creature card from among them and put it into your hand. Put the rest on the bottom of your library in any order.').
card_image_name('commune with nature'/'10E', 'commune with nature').
card_uid('commune with nature'/'10E', '10E:Commune with Nature:commune with nature').
card_rarity('commune with nature'/'10E', 'Common').
card_artist('commune with nature'/'10E', 'Lars Grant-West').
card_number('commune with nature'/'10E', '256').
card_multiverse_id('commune with nature'/'10E', '130521').

card_in_set('composite golem', '10E').
card_original_type('composite golem'/'10E', 'Artifact Creature — Golem').
card_original_text('composite golem'/'10E', 'Sacrifice Composite Golem: Add {W}{U}{B}{R}{G} to your mana pool.').
card_image_name('composite golem'/'10E', 'composite golem').
card_uid('composite golem'/'10E', '10E:Composite Golem:composite golem').
card_rarity('composite golem'/'10E', 'Uncommon').
card_artist('composite golem'/'10E', 'Mark Tedin').
card_number('composite golem'/'10E', '318').
card_flavor_text('composite golem'/'10E', 'An artificer\'s experiment in cross-material engineering found its own purpose as an interpreter between cultures.').
card_multiverse_id('composite golem'/'10E', '135275').

card_in_set('condemn', '10E').
card_original_type('condemn'/'10E', 'Instant').
card_original_text('condemn'/'10E', 'Put target attacking creature on the bottom of its owner\'s library. Its controller gains life equal to its toughness.').
card_image_name('condemn'/'10E', 'condemn').
card_uid('condemn'/'10E', '10E:Condemn:condemn').
card_rarity('condemn'/'10E', 'Uncommon').
card_artist('condemn'/'10E', 'Daren Bader').
card_number('condemn'/'10E', '13').
card_flavor_text('condemn'/'10E', '\"No doubt the arbiters would put you away, after all the documents are signed. But I will have justice now!\"\n—Alovnek, Boros guildmage').
card_multiverse_id('condemn'/'10E', '130528').

card_in_set('cone of flame', '10E').
card_original_type('cone of flame'/'10E', 'Sorcery').
card_original_text('cone of flame'/'10E', 'Cone of Flame deals 1 damage to target creature or player, 2 damage to another target creature or player, and 3 damage to a third target creature or player.').
card_image_name('cone of flame'/'10E', 'cone of flame').
card_uid('cone of flame'/'10E', '10E:Cone of Flame:cone of flame').
card_rarity('cone of flame'/'10E', 'Uncommon').
card_artist('cone of flame'/'10E', 'Chippy').
card_number('cone of flame'/'10E', '194').
card_multiverse_id('cone of flame'/'10E', '130535').

card_in_set('consume spirit', '10E').
card_original_type('consume spirit'/'10E', 'Sorcery').
card_original_text('consume spirit'/'10E', 'Spend only black mana on X.\nConsume Spirit deals X damage to target creature or player and you gain X life.').
card_image_name('consume spirit'/'10E', 'consume spirit').
card_uid('consume spirit'/'10E', '10E:Consume Spirit:consume spirit').
card_rarity('consume spirit'/'10E', 'Uncommon').
card_artist('consume spirit'/'10E', 'Matt Thompson').
card_number('consume spirit'/'10E', '131').
card_flavor_text('consume spirit'/'10E', '\"Your blood, your marrow, your spirit—all are mine.\"\n—Mayvar, minion of Geth').
card_multiverse_id('consume spirit'/'10E', '129505').

card_in_set('contaminated bond', '10E').
card_original_type('contaminated bond'/'10E', 'Enchantment — Aura').
card_original_text('contaminated bond'/'10E', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nWhenever enchanted creature attacks or blocks, its controller loses 3 life.').
card_image_name('contaminated bond'/'10E', 'contaminated bond').
card_uid('contaminated bond'/'10E', '10E:Contaminated Bond:contaminated bond').
card_rarity('contaminated bond'/'10E', 'Common').
card_artist('contaminated bond'/'10E', 'Thomas M. Baxa').
card_number('contaminated bond'/'10E', '132').
card_flavor_text('contaminated bond'/'10E', '\"My favorite curses are those that manifest slowly, blackening the soul in ways the victim cannot hope to understand.\"\n—Erissa, bog witch').
card_multiverse_id('contaminated bond'/'10E', '129590').

card_in_set('counsel of the soratami', '10E').
card_original_type('counsel of the soratami'/'10E', 'Sorcery').
card_original_text('counsel of the soratami'/'10E', 'Draw two cards.').
card_image_name('counsel of the soratami'/'10E', 'counsel of the soratami').
card_uid('counsel of the soratami'/'10E', '10E:Counsel of the Soratami:counsel of the soratami').
card_rarity('counsel of the soratami'/'10E', 'Common').
card_artist('counsel of the soratami'/'10E', 'Randy Gallegos').
card_number('counsel of the soratami'/'10E', '76').
card_flavor_text('counsel of the soratami'/'10E', '\"Wisdom is not the counting of all the drops in a waterfall. Wisdom is learning why the water seeks the earth.\"').
card_multiverse_id('counsel of the soratami'/'10E', '134757').

card_in_set('crafty pathmage', '10E').
card_original_type('crafty pathmage'/'10E', 'Creature — Human Wizard').
card_original_text('crafty pathmage'/'10E', '{T}: Target creature with power 2 or less is unblockable this turn.').
card_image_name('crafty pathmage'/'10E', 'crafty pathmage').
card_uid('crafty pathmage'/'10E', '10E:Crafty Pathmage:crafty pathmage').
card_rarity('crafty pathmage'/'10E', 'Common').
card_artist('crafty pathmage'/'10E', 'Wayne England').
card_number('crafty pathmage'/'10E', '77').
card_flavor_text('crafty pathmage'/'10E', 'Follow the pathmage\n—Otarian expression meaning\n\"escape quickly\"').
card_multiverse_id('crafty pathmage'/'10E', '134758').

card_in_set('craw wurm', '10E').
card_original_type('craw wurm'/'10E', 'Creature — Wurm').
card_original_text('craw wurm'/'10E', '').
card_image_name('craw wurm'/'10E', 'craw wurm').
card_uid('craw wurm'/'10E', '10E:Craw Wurm:craw wurm').
card_rarity('craw wurm'/'10E', 'Common').
card_artist('craw wurm'/'10E', 'Richard Sardinha').
card_number('craw wurm'/'10E', '257').
card_flavor_text('craw wurm'/'10E', 'The most terrifying thing about the craw wurm is probably the horrible crashing sound it makes as it speeds through the forest. This noise is so loud it echoes through the trees and seems to come from all directions at once.').
card_multiverse_id('craw wurm'/'10E', '130527').

card_in_set('creeping mold', '10E').
card_original_type('creeping mold'/'10E', 'Sorcery').
card_original_text('creeping mold'/'10E', 'Destroy target artifact, enchantment, or land.').
card_image_name('creeping mold'/'10E', 'creeping mold').
card_uid('creeping mold'/'10E', '10E:Creeping Mold:creeping mold').
card_rarity('creeping mold'/'10E', 'Uncommon').
card_artist('creeping mold'/'10E', 'Gary Ruddell').
card_number('creeping mold'/'10E', '258').
card_flavor_text('creeping mold'/'10E', 'Mold crept over the walls and into every crevice until the gleaming white stone strained and burst.').
card_multiverse_id('creeping mold'/'10E', '129512').

card_in_set('crucible of worlds', '10E').
card_original_type('crucible of worlds'/'10E', 'Artifact').
card_original_text('crucible of worlds'/'10E', 'You may play land cards from your graveyard.').
card_image_name('crucible of worlds'/'10E', 'crucible of worlds').
card_uid('crucible of worlds'/'10E', '10E:Crucible of Worlds:crucible of worlds').
card_rarity('crucible of worlds'/'10E', 'Rare').
card_artist('crucible of worlds'/'10E', 'Ron Spencer').
card_number('crucible of worlds'/'10E', '319').
card_flavor_text('crucible of worlds'/'10E', 'Amidst the darkest ashes grow the strongest seeds.').
card_multiverse_id('crucible of worlds'/'10E', '129480').

card_in_set('cruel edict', '10E').
card_original_type('cruel edict'/'10E', 'Sorcery').
card_original_text('cruel edict'/'10E', 'Target opponent sacrifices a creature.').
card_image_name('cruel edict'/'10E', 'cruel edict').
card_uid('cruel edict'/'10E', '10E:Cruel Edict:cruel edict').
card_rarity('cruel edict'/'10E', 'Uncommon').
card_artist('cruel edict'/'10E', 'Michael Sutfin').
card_number('cruel edict'/'10E', '133').
card_flavor_text('cruel edict'/'10E', '\"Choose your next words carefully. They will be your last.\"\n—Phage the Untouchable').
card_multiverse_id('cruel edict'/'10E', '129514').

card_in_set('cryoclasm', '10E').
card_original_type('cryoclasm'/'10E', 'Sorcery').
card_original_text('cryoclasm'/'10E', 'Destroy target Plains or Island. Cryoclasm deals 3 damage to that land\'s controller.').
card_image_name('cryoclasm'/'10E', 'cryoclasm').
card_uid('cryoclasm'/'10E', '10E:Cryoclasm:cryoclasm').
card_rarity('cryoclasm'/'10E', 'Uncommon').
card_artist('cryoclasm'/'10E', 'Zoltan Boros & Gabor Szikszai').
card_number('cryoclasm'/'10E', '195').
card_flavor_text('cryoclasm'/'10E', 'The people of Terisiare had come to live on frozen fields as though on solid ground. Nothing reminded them of the difference more clearly than the rifts brought on by the Thaw.').
card_multiverse_id('cryoclasm'/'10E', '129909').

card_in_set('deathmark', '10E').
card_original_type('deathmark'/'10E', 'Sorcery').
card_original_text('deathmark'/'10E', 'Destroy target green or white creature.').
card_image_name('deathmark'/'10E', 'deathmark').
card_uid('deathmark'/'10E', '10E:Deathmark:deathmark').
card_rarity('deathmark'/'10E', 'Uncommon').
card_artist('deathmark'/'10E', 'Jeremy Jarvis').
card_number('deathmark'/'10E', '134').
card_flavor_text('deathmark'/'10E', '\"I hope it\'s true that every snowflake is unique, because I never want to see one like this again. Now clean up that body.\"\n—Thangbrand Gyrdsson, Kjeldoran patrol').
card_multiverse_id('deathmark'/'10E', '129910').

card_in_set('dehydration', '10E').
card_original_type('dehydration'/'10E', 'Enchantment — Aura').
card_original_text('dehydration'/'10E', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nEnchanted creature doesn\'t untap during its controller\'s untap step.').
card_image_name('dehydration'/'10E', 'dehydration').
card_uid('dehydration'/'10E', '10E:Dehydration:dehydration').
card_rarity('dehydration'/'10E', 'Common').
card_artist('dehydration'/'10E', 'Arnie Swekel').
card_number('dehydration'/'10E', '78').
card_flavor_text('dehydration'/'10E', 'The viashino used the spell to dry foods for crossing the Great Desert. The Suq\'Ata later discovered its use in war.').
card_multiverse_id('dehydration'/'10E', '129521').

card_in_set('deluge', '10E').
card_original_type('deluge'/'10E', 'Instant').
card_original_text('deluge'/'10E', 'Tap all creatures without flying.').
card_image_name('deluge'/'10E', 'deluge').
card_uid('deluge'/'10E', '10E:Deluge:deluge').
card_rarity('deluge'/'10E', 'Uncommon').
card_artist('deluge'/'10E', 'Wayne England').
card_number('deluge'/'10E', '79').
card_flavor_text('deluge'/'10E', '\"From the sea came all life, and to the sea it will return. The sooner the better.\"\n—Emperor Aboshan').
card_multiverse_id('deluge'/'10E', '135223').

card_in_set('demolish', '10E').
card_original_type('demolish'/'10E', 'Sorcery').
card_original_text('demolish'/'10E', 'Destroy target artifact or land.').
card_image_name('demolish'/'10E', 'demolish').
card_uid('demolish'/'10E', '10E:Demolish:demolish').
card_rarity('demolish'/'10E', 'Common').
card_artist('demolish'/'10E', 'Gary Ruddell').
card_number('demolish'/'10E', '196').
card_flavor_text('demolish'/'10E', '\"Pound the steel until it fits.\nDoesn\'t work? Bash to bits.\"\n—Dwarven forging song').
card_multiverse_id('demolish'/'10E', '129522').

card_in_set('demon\'s horn', '10E').
card_original_type('demon\'s horn'/'10E', 'Artifact').
card_original_text('demon\'s horn'/'10E', 'Whenever a player plays a black spell, you may gain 1 life.').
card_image_name('demon\'s horn'/'10E', 'demon\'s horn').
card_uid('demon\'s horn'/'10E', '10E:Demon\'s Horn:demon\'s horn').
card_rarity('demon\'s horn'/'10E', 'Uncommon').
card_artist('demon\'s horn'/'10E', 'Alan Pollack').
card_number('demon\'s horn'/'10E', '320').
card_flavor_text('demon\'s horn'/'10E', 'Its curve mimics the twists of life and death.').
card_multiverse_id('demon\'s horn'/'10E', '129523').

card_in_set('demystify', '10E').
card_original_type('demystify'/'10E', 'Instant').
card_original_text('demystify'/'10E', 'Destroy target enchantment.').
card_image_name('demystify'/'10E', 'demystify').
card_uid('demystify'/'10E', '10E:Demystify:demystify').
card_rarity('demystify'/'10E', 'Common').
card_artist('demystify'/'10E', 'Christopher Rush').
card_number('demystify'/'10E', '14').
card_flavor_text('demystify'/'10E', '\"Illusion is a crutch for those with no grounding in reality.\"\n—Cho-Manno').
card_multiverse_id('demystify'/'10E', '129524').

card_in_set('denizen of the deep', '10E').
card_original_type('denizen of the deep'/'10E', 'Creature — Serpent').
card_original_text('denizen of the deep'/'10E', 'When Denizen of the Deep comes into play, return each other creature you control to its owner\'s hand.').
card_image_name('denizen of the deep'/'10E', 'denizen of the deep').
card_uid('denizen of the deep'/'10E', '10E:Denizen of the Deep:denizen of the deep').
card_rarity('denizen of the deep'/'10E', 'Rare').
card_artist('denizen of the deep'/'10E', 'Jim Pavelec').
card_number('denizen of the deep'/'10E', '80').
card_flavor_text('denizen of the deep'/'10E', 'According to merfolk legend, a denizen of the deep swallows the horizon at the end of each day, bringing on the cold blanket of night.').
card_multiverse_id('denizen of the deep'/'10E', '135250').

card_in_set('diabolic tutor', '10E').
card_original_type('diabolic tutor'/'10E', 'Sorcery').
card_original_text('diabolic tutor'/'10E', 'Search your library for a card and put that card into your hand. Then shuffle your library.').
card_image_name('diabolic tutor'/'10E', 'diabolic tutor').
card_uid('diabolic tutor'/'10E', '10E:Diabolic Tutor:diabolic tutor').
card_rarity('diabolic tutor'/'10E', 'Uncommon').
card_artist('diabolic tutor'/'10E', 'Greg Staples').
card_number('diabolic tutor'/'10E', '135').
card_flavor_text('diabolic tutor'/'10E', 'The best ideas often come from the worst minds.').
card_multiverse_id('diabolic tutor'/'10E', '129525').

card_in_set('discombobulate', '10E').
card_original_type('discombobulate'/'10E', 'Instant').
card_original_text('discombobulate'/'10E', 'Counter target spell. Look at the top four cards of your library, then put them back in any order.').
card_image_name('discombobulate'/'10E', 'discombobulate').
card_uid('discombobulate'/'10E', '10E:Discombobulate:discombobulate').
card_rarity('discombobulate'/'10E', 'Uncommon').
card_artist('discombobulate'/'10E', 'Alex Horley-Orlandelli').
card_number('discombobulate'/'10E', '81').
card_flavor_text('discombobulate'/'10E', '\"I said ‘pick his brain,\' not ‘tear off his head.\'\"\n—Riptide Project researcher').
card_multiverse_id('discombobulate'/'10E', '134749').

card_in_set('distress', '10E').
card_original_type('distress'/'10E', 'Sorcery').
card_original_text('distress'/'10E', 'Target player reveals his or her hand. You choose a nonland card from it. That player discards that card.').
card_image_name('distress'/'10E', 'distress').
card_uid('distress'/'10E', '10E:Distress:distress').
card_rarity('distress'/'10E', 'Common').
card_artist('distress'/'10E', 'Michael Sutfin').
card_number('distress'/'10E', '136').
card_flavor_text('distress'/'10E', '\"They say the eyes are the windows to the soul. I like to break windows and take what\'s inside.\"\n—Braids, dementia summoner').
card_multiverse_id('distress'/'10E', '135219').

card_in_set('doomed necromancer', '10E').
card_original_type('doomed necromancer'/'10E', 'Creature — Human Cleric Mercenary').
card_original_text('doomed necromancer'/'10E', '{B}, {T}, Sacrifice Doomed Necromancer: Return target creature card from your graveyard to play.').
card_image_name('doomed necromancer'/'10E', 'doomed necromancer').
card_uid('doomed necromancer'/'10E', '10E:Doomed Necromancer:doomed necromancer').
card_rarity('doomed necromancer'/'10E', 'Rare').
card_artist('doomed necromancer'/'10E', 'Volkan Baga').
card_number('doomed necromancer'/'10E', '137').
card_flavor_text('doomed necromancer'/'10E', 'Many necromancers share the same last words—the ones that conclude the undeath ritual.').
card_multiverse_id('doomed necromancer'/'10E', '129880').

card_in_set('doubling cube', '10E').
card_original_type('doubling cube'/'10E', 'Artifact').
card_original_text('doubling cube'/'10E', '{3}, {T}: Double the amount of each type of mana in your mana pool.').
card_image_name('doubling cube'/'10E', 'doubling cube').
card_uid('doubling cube'/'10E', '10E:Doubling Cube:doubling cube').
card_rarity('doubling cube'/'10E', 'Rare').
card_artist('doubling cube'/'10E', 'Mark Tedin').
card_number('doubling cube'/'10E', '321').
card_flavor_text('doubling cube'/'10E', 'The cube\'s surface is pockmarked with jagged runes that seem to shift when unobserved.').
card_multiverse_id('doubling cube'/'10E', '135243').

card_in_set('dragon roost', '10E').
card_original_type('dragon roost'/'10E', 'Enchantment').
card_original_text('dragon roost'/'10E', '{5}{R}{R}: Put a 5/5 red Dragon creature token with flying into play. (It can\'t be blocked except by creatures with flying or reach.)').
card_image_name('dragon roost'/'10E', 'dragon roost').
card_uid('dragon roost'/'10E', '10E:Dragon Roost:dragon roost').
card_rarity('dragon roost'/'10E', 'Rare').
card_artist('dragon roost'/'10E', 'Jim Pavelec').
card_number('dragon roost'/'10E', '197').
card_flavor_text('dragon roost'/'10E', 'Dragons erupt from peaks of chaos and flow from rivers of molten rage.').
card_multiverse_id('dragon roost'/'10E', '143024').

card_in_set('dragon\'s claw', '10E').
card_original_type('dragon\'s claw'/'10E', 'Artifact').
card_original_text('dragon\'s claw'/'10E', 'Whenever a player plays a red spell, you may gain 1 life.').
card_image_name('dragon\'s claw'/'10E', 'dragon\'s claw').
card_uid('dragon\'s claw'/'10E', '10E:Dragon\'s Claw:dragon\'s claw').
card_rarity('dragon\'s claw'/'10E', 'Uncommon').
card_artist('dragon\'s claw'/'10E', 'Alan Pollack').
card_number('dragon\'s claw'/'10E', '322').
card_flavor_text('dragon\'s claw'/'10E', 'Though no longer attached to the hand, it still holds its adversary in its grasp.').
card_multiverse_id('dragon\'s claw'/'10E', '129527').

card_in_set('dreamborn muse', '10E').
card_original_type('dreamborn muse'/'10E', 'Creature — Spirit').
card_original_text('dreamborn muse'/'10E', 'At the beginning of each player\'s upkeep, that player puts the top X cards of his or her library into his or her graveyard, where X is the number of cards in his or her hand.').
card_image_name('dreamborn muse'/'10E', 'dreamborn muse').
card_uid('dreamborn muse'/'10E', '10E:Dreamborn Muse:dreamborn muse').
card_rarity('dreamborn muse'/'10E', 'Rare').
card_artist('dreamborn muse'/'10E', 'Kev Walker').
card_number('dreamborn muse'/'10E', '82').
card_flavor_text('dreamborn muse'/'10E', '\"Her voice is insight, piercing and true.\"\n—Ixidor, reality sculptor').
card_multiverse_id('dreamborn muse'/'10E', '135246').

card_in_set('dross crocodile', '10E').
card_original_type('dross crocodile'/'10E', 'Creature — Zombie Crocodile').
card_original_text('dross crocodile'/'10E', '').
card_image_name('dross crocodile'/'10E', 'dross crocodile').
card_uid('dross crocodile'/'10E', '10E:Dross Crocodile:dross crocodile').
card_rarity('dross crocodile'/'10E', 'Common').
card_artist('dross crocodile'/'10E', 'Carl Critchlow').
card_number('dross crocodile'/'10E', '138').
card_flavor_text('dross crocodile'/'10E', '\"As soon as it surfaced, we could all smell it. Its rancid breath reeked of half-digested carrion and its own rotting innards.\"\n—Dafri, Auriok champion').
card_multiverse_id('dross crocodile'/'10E', '135216').

card_in_set('drudge skeletons', '10E').
card_original_type('drudge skeletons'/'10E', 'Creature — Skeleton').
card_original_text('drudge skeletons'/'10E', '{B}: Regenerate Drudge Skeletons. (The next time this creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_image_name('drudge skeletons'/'10E', 'drudge skeletons').
card_uid('drudge skeletons'/'10E', '10E:Drudge Skeletons:drudge skeletons').
card_rarity('drudge skeletons'/'10E', 'Uncommon').
card_artist('drudge skeletons'/'10E', 'Jim Nelson').
card_number('drudge skeletons'/'10E', '139').
card_flavor_text('drudge skeletons'/'10E', '\"The dead make good soldiers. They can\'t disobey orders, never surrender, and don\'t stop fighting when a random body part falls off.\"\n—Nevinyrral, Necromancer\'s Handbook').
card_multiverse_id('drudge skeletons'/'10E', '129529').

card_in_set('duct crawler', '10E').
card_original_type('duct crawler'/'10E', 'Creature — Insect').
card_original_text('duct crawler'/'10E', '{1}{R}: Target creature can\'t block Duct Crawler this turn.').
card_image_name('duct crawler'/'10E', 'duct crawler').
card_uid('duct crawler'/'10E', '10E:Duct Crawler:duct crawler').
card_rarity('duct crawler'/'10E', 'Common').
card_artist('duct crawler'/'10E', 'Stephen Daniele').
card_number('duct crawler'/'10E', '198').
card_flavor_text('duct crawler'/'10E', '\"Boss told us to try and train \'em. Trained it to attack—it ate Flugg. Trained it to run fast—it got away. Success!\"\n—Dlig, goblin spelunker').
card_multiverse_id('duct crawler'/'10E', '129579').

card_in_set('dusk imp', '10E').
card_original_type('dusk imp'/'10E', 'Creature — Imp').
card_original_text('dusk imp'/'10E', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)').
card_image_name('dusk imp'/'10E', 'dusk imp').
card_uid('dusk imp'/'10E', '10E:Dusk Imp:dusk imp').
card_rarity('dusk imp'/'10E', 'Common').
card_artist('dusk imp'/'10E', 'Pete Venters').
card_number('dusk imp'/'10E', '140').
card_flavor_text('dusk imp'/'10E', 'Imps are just intelligent enough to have an understanding of cruelty.').
card_multiverse_id('dusk imp'/'10E', '129490').

card_in_set('earth elemental', '10E').
card_original_type('earth elemental'/'10E', 'Creature — Elemental').
card_original_text('earth elemental'/'10E', '').
card_image_name('earth elemental'/'10E', 'earth elemental').
card_uid('earth elemental'/'10E', '10E:Earth Elemental:earth elemental').
card_rarity('earth elemental'/'10E', 'Uncommon').
card_artist('earth elemental'/'10E', 'Anthony S. Waters').
card_number('earth elemental'/'10E', '199').
card_flavor_text('earth elemental'/'10E', 'Earth elementals have the eternal strength of stone and the endurance of mountains. Primordially connected to the land they inhabit, they take a long-term view of things, scorning the impetuous haste of short-lived mortal creatures.').
card_multiverse_id('earth elemental'/'10E', '129554').

card_in_set('elven riders', '10E').
card_original_type('elven riders'/'10E', 'Creature — Elf').
card_original_text('elven riders'/'10E', 'Elven Riders can\'t be blocked except by Walls and/or creatures with flying.').
card_image_name('elven riders'/'10E', 'elven riders').
card_uid('elven riders'/'10E', '10E:Elven Riders:elven riders').
card_rarity('elven riders'/'10E', 'Uncommon').
card_artist('elven riders'/'10E', 'Darrell Riche').
card_number('elven riders'/'10E', '259').
card_flavor_text('elven riders'/'10E', '\"Wirewood cannot hide great size. Only with speed and skill can we survive here.\"').
card_multiverse_id('elven riders'/'10E', '135266').

card_in_set('elvish berserker', '10E').
card_original_type('elvish berserker'/'10E', 'Creature — Elf Berserker').
card_original_text('elvish berserker'/'10E', 'Whenever Elvish Berserker becomes blocked, it gets +1/+1 until end of turn for each creature blocking it.').
card_image_name('elvish berserker'/'10E', 'elvish berserker').
card_uid('elvish berserker'/'10E', '10E:Elvish Berserker:elvish berserker').
card_rarity('elvish berserker'/'10E', 'Common').
card_artist('elvish berserker'/'10E', 'Paolo Parente').
card_number('elvish berserker'/'10E', '260').
card_flavor_text('elvish berserker'/'10E', 'Their fury scatters enemies like a pile of dry leaves.').
card_multiverse_id('elvish berserker'/'10E', '129533').

card_in_set('elvish champion', '10E').
card_original_type('elvish champion'/'10E', 'Creature — Elf').
card_original_text('elvish champion'/'10E', 'Other Elf creatures get +1/+1 and have forestwalk. (They\'re unblockable as long as defending player controls a Forest.)').
card_image_name('elvish champion'/'10E', 'elvish champion').
card_uid('elvish champion'/'10E', '10E:Elvish Champion:elvish champion').
card_rarity('elvish champion'/'10E', 'Rare').
card_artist('elvish champion'/'10E', 'D. Alexander Gregory').
card_number('elvish champion'/'10E', '261').
card_flavor_text('elvish champion'/'10E', '\"For what are leaves but countless blades\nTo fight a countless foe on high.\"\n—Elvish hymn').
card_multiverse_id('elvish champion'/'10E', '129534').

card_in_set('elvish piper', '10E').
card_original_type('elvish piper'/'10E', 'Creature — Elf Shaman').
card_original_text('elvish piper'/'10E', '{G}, {T}: You may put a creature card from your hand into play.').
card_image_name('elvish piper'/'10E', 'elvish piper').
card_uid('elvish piper'/'10E', '10E:Elvish Piper:elvish piper').
card_rarity('elvish piper'/'10E', 'Rare').
card_artist('elvish piper'/'10E', 'Rebecca Guay').
card_number('elvish piper'/'10E', '262').
card_flavor_text('elvish piper'/'10E', 'From Gaea grew the world, and the world was silent. From Gaea grew the world\'s elves, and the world was silent no more.\n—Elvish teaching').
card_multiverse_id('elvish piper'/'10E', '129535').

card_in_set('enormous baloth', '10E').
card_original_type('enormous baloth'/'10E', 'Creature — Beast').
card_original_text('enormous baloth'/'10E', '').
card_image_name('enormous baloth'/'10E', 'enormous baloth').
card_uid('enormous baloth'/'10E', '10E:Enormous Baloth:enormous baloth').
card_rarity('enormous baloth'/'10E', 'Uncommon').
card_artist('enormous baloth'/'10E', 'Mark Tedin').
card_number('enormous baloth'/'10E', '263').
card_flavor_text('enormous baloth'/'10E', 'Its diet consists of fruits, plants, small woodland animals, large woodland animals, woodlands, fruit groves, fruit farmers, and small cities.').
card_multiverse_id('enormous baloth'/'10E', '135263').

card_in_set('essence drain', '10E').
card_original_type('essence drain'/'10E', 'Sorcery').
card_original_text('essence drain'/'10E', 'Essence Drain deals 3 damage to target creature or player and you gain 3 life.').
card_image_name('essence drain'/'10E', 'essence drain').
card_uid('essence drain'/'10E', '10E:Essence Drain:essence drain').
card_rarity('essence drain'/'10E', 'Common').
card_artist('essence drain'/'10E', 'Jim Nelson').
card_number('essence drain'/'10E', '141').
card_flavor_text('essence drain'/'10E', '\"The elves are right: Death inevitably leads to life. But the truly powerful don\'t just experience this cycle. They control it.\"\n—Crovax, ascendant evincar').
card_multiverse_id('essence drain'/'10E', '135191').

card_in_set('evacuation', '10E').
card_original_type('evacuation'/'10E', 'Instant').
card_original_text('evacuation'/'10E', 'Return all creatures to their owners\' hands.').
card_image_name('evacuation'/'10E', 'evacuation').
card_uid('evacuation'/'10E', '10E:Evacuation:evacuation').
card_rarity('evacuation'/'10E', 'Rare').
card_artist('evacuation'/'10E', 'Franz Vohwinkel').
card_number('evacuation'/'10E', '83').
card_flavor_text('evacuation'/'10E', '\"Once I supply the breeze, you\'ll see your warriors for the smoke they truly are.\"\n—Alexi, zephyr mage').
card_multiverse_id('evacuation'/'10E', '129541').

card_in_set('faerie conclave', '10E').
card_original_type('faerie conclave'/'10E', 'Land').
card_original_text('faerie conclave'/'10E', 'Faerie Conclave comes into play tapped.\n{T}: Add {U} to your mana pool.\n{1}{U}: Faerie Conclave becomes a 2/1 blue Faerie creature with flying until end of turn. It\'s still a land. (It can\'t be blocked except by creatures with flying or reach.)').
card_image_name('faerie conclave'/'10E', 'faerie conclave').
card_uid('faerie conclave'/'10E', '10E:Faerie Conclave:faerie conclave').
card_rarity('faerie conclave'/'10E', 'Uncommon').
card_artist('faerie conclave'/'10E', 'Stephan Martiniere').
card_number('faerie conclave'/'10E', '351').
card_multiverse_id('faerie conclave'/'10E', '106531').

card_in_set('fear', '10E').
card_original_type('fear'/'10E', 'Enchantment — Aura').
card_original_text('fear'/'10E', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nEnchanted creature has fear. (It can\'t be blocked except by artifact creatures and/or black creatures.)').
card_image_name('fear'/'10E', 'fear').
card_uid('fear'/'10E', '10E:Fear:fear').
card_rarity('fear'/'10E', 'Common').
card_artist('fear'/'10E', 'Adam Rex').
card_number('fear'/'10E', '142').
card_flavor_text('fear'/'10E', '\"Even the bravest of warriors yet knows the dark clutch of fright upon his stalwart heart.\"\n—Lim-Dûl the Necromancer').
card_multiverse_id('fear'/'10E', '129544').

card_in_set('femeref archers', '10E').
card_original_type('femeref archers'/'10E', 'Creature — Human Archer').
card_original_text('femeref archers'/'10E', '{T}: Femeref Archers deals 4 damage to target attacking creature with flying.').
card_image_name('femeref archers'/'10E', 'femeref archers').
card_uid('femeref archers'/'10E', '10E:Femeref Archers:femeref archers').
card_rarity('femeref archers'/'10E', 'Uncommon').
card_artist('femeref archers'/'10E', 'Zoltan Boros & Gabor Szikszai').
card_number('femeref archers'/'10E', '264').
card_flavor_text('femeref archers'/'10E', '\"Bet you can\'t put it through the eye.\"\n\"Left or right?\"').
card_multiverse_id('femeref archers'/'10E', '135186').

card_in_set('festering goblin', '10E').
card_original_type('festering goblin'/'10E', 'Creature — Zombie Goblin').
card_original_text('festering goblin'/'10E', 'When Festering Goblin is put into a graveyard from play, target creature gets -1/-1 until end of turn.').
card_image_name('festering goblin'/'10E', 'festering goblin').
card_uid('festering goblin'/'10E', '10E:Festering Goblin:festering goblin').
card_rarity('festering goblin'/'10E', 'Common').
card_artist('festering goblin'/'10E', 'Thomas M. Baxa').
card_number('festering goblin'/'10E', '143').
card_flavor_text('festering goblin'/'10E', 'In life, it was a fetid, disease-ridden thing. In death, not much changed.').
card_multiverse_id('festering goblin'/'10E', '129546').

card_in_set('field marshal', '10E').
card_original_type('field marshal'/'10E', 'Creature — Human Soldier').
card_original_text('field marshal'/'10E', 'Other Soldier creatures get +1/+1 and have first strike. (They deal combat damage before creatures without first strike.)').
card_image_name('field marshal'/'10E', 'field marshal').
card_uid('field marshal'/'10E', '10E:Field Marshal:field marshal').
card_rarity('field marshal'/'10E', 'Rare').
card_artist('field marshal'/'10E', 'Stephen Tappin').
card_number('field marshal'/'10E', '15').
card_flavor_text('field marshal'/'10E', 'He is the only one who sees the patterns in the overlapping maps and conflicting reports.').
card_multiverse_id('field marshal'/'10E', '135258').

card_in_set('firebreathing', '10E').
card_original_type('firebreathing'/'10E', 'Enchantment — Aura').
card_original_text('firebreathing'/'10E', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\n{R}: Enchanted creature gets +1/+0 until end of turn.').
card_image_name('firebreathing'/'10E', 'firebreathing').
card_uid('firebreathing'/'10E', '10E:Firebreathing:firebreathing').
card_rarity('firebreathing'/'10E', 'Common').
card_artist('firebreathing'/'10E', 'Aleksi Briclot').
card_number('firebreathing'/'10E', '200').
card_flavor_text('firebreathing'/'10E', '\"Cradle your rage close to your heart and nurture your grudge with hatred that you may unleash upon your foes a dragon\'s fury.\"\n—Tomor, dragonspeaker shaman').
card_multiverse_id('firebreathing'/'10E', '129548').

card_in_set('fists of the anvil', '10E').
card_original_type('fists of the anvil'/'10E', 'Instant').
card_original_text('fists of the anvil'/'10E', 'Target creature gets +4/+0 until end of turn.').
card_image_name('fists of the anvil'/'10E', 'fists of the anvil').
card_uid('fists of the anvil'/'10E', '10E:Fists of the Anvil:fists of the anvil').
card_rarity('fists of the anvil'/'10E', 'Common').
card_artist('fists of the anvil'/'10E', 'Pete Venters').
card_number('fists of the anvil'/'10E', '201').
card_flavor_text('fists of the anvil'/'10E', 'Gron\'s mind reeled with possibilities, most of which are best left unmentioned.').
card_multiverse_id('fists of the anvil'/'10E', '130379').

card_in_set('flamewave invoker', '10E').
card_original_type('flamewave invoker'/'10E', 'Creature — Goblin Mutant').
card_original_text('flamewave invoker'/'10E', '{7}{R}: Flamewave Invoker deals 5 damage to target player.').
card_image_name('flamewave invoker'/'10E', 'flamewave invoker').
card_uid('flamewave invoker'/'10E', '10E:Flamewave Invoker:flamewave invoker').
card_rarity('flamewave invoker'/'10E', 'Uncommon').
card_artist('flamewave invoker'/'10E', 'Dave Dorman').
card_number('flamewave invoker'/'10E', '202').
card_flavor_text('flamewave invoker'/'10E', 'Inside even the humblest goblin lurks the potential for far greater things—and far worse.').
card_multiverse_id('flamewave invoker'/'10E', '130378').

card_in_set('flashfreeze', '10E').
card_original_type('flashfreeze'/'10E', 'Instant').
card_original_text('flashfreeze'/'10E', 'Counter target red or green spell.').
card_image_name('flashfreeze'/'10E', 'flashfreeze').
card_uid('flashfreeze'/'10E', '10E:Flashfreeze:flashfreeze').
card_rarity('flashfreeze'/'10E', 'Uncommon').
card_artist('flashfreeze'/'10E', 'Brian Despain').
card_number('flashfreeze'/'10E', '84').
card_flavor_text('flashfreeze'/'10E', '\"Nature? Fire? Bah! Both are chaotic and difficult to control. Ice is structured, latticed, light as a feather, massive as a glacier. In ice, there is power!\"\n—Heidar, Rimewind master').
card_multiverse_id('flashfreeze'/'10E', '129908').

card_in_set('flowstone slide', '10E').
card_original_type('flowstone slide'/'10E', 'Sorcery').
card_original_text('flowstone slide'/'10E', 'All creatures get +X/-X until end of turn.').
card_image_name('flowstone slide'/'10E', 'flowstone slide').
card_uid('flowstone slide'/'10E', '10E:Flowstone Slide:flowstone slide').
card_rarity('flowstone slide'/'10E', 'Rare').
card_artist('flowstone slide'/'10E', 'Chippy').
card_number('flowstone slide'/'10E', '203').
card_flavor_text('flowstone slide'/'10E', '\"Like an avalanche. With teeth. That will chase you. Uphill.\"\n—Flint Foot, viashino runner').
card_multiverse_id('flowstone slide'/'10E', '130542').

card_in_set('fog elemental', '10E').
card_original_type('fog elemental'/'10E', 'Creature — Elemental').
card_original_text('fog elemental'/'10E', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nWhen Fog Elemental attacks or blocks, sacrifice it at end of combat.').
card_image_name('fog elemental'/'10E', 'fog elemental').
card_uid('fog elemental'/'10E', '10E:Fog Elemental:fog elemental').
card_rarity('fog elemental'/'10E', 'Uncommon').
card_artist('fog elemental'/'10E', 'Jon J. Muth').
card_number('fog elemental'/'10E', '85').
card_flavor_text('fog elemental'/'10E', '\"It\'s the perfect sentry. Those who should pass can go right through it.\"\n—Teferi').
card_multiverse_id('fog elemental'/'10E', '132073').

card_in_set('forbidding watchtower', '10E').
card_original_type('forbidding watchtower'/'10E', 'Land').
card_original_text('forbidding watchtower'/'10E', 'Forbidding Watchtower comes into play tapped.\n{T}: Add {W} to your mana pool.\n{1}{W}: Forbidding Watchtower becomes a 1/5 white Soldier creature until end of turn. It\'s still a land.').
card_image_name('forbidding watchtower'/'10E', 'forbidding watchtower').
card_uid('forbidding watchtower'/'10E', '10E:Forbidding Watchtower:forbidding watchtower').
card_rarity('forbidding watchtower'/'10E', 'Uncommon').
card_artist('forbidding watchtower'/'10E', 'Aleksi Briclot').
card_number('forbidding watchtower'/'10E', '352').
card_multiverse_id('forbidding watchtower'/'10E', '106428').

card_in_set('forest', '10E').
card_original_type('forest'/'10E', 'Basic Land — Forest').
card_original_text('forest'/'10E', 'G').
card_image_name('forest'/'10E', 'forest1').
card_uid('forest'/'10E', '10E:Forest:forest1').
card_rarity('forest'/'10E', 'Basic Land').
card_artist('forest'/'10E', 'Anthony S. Waters').
card_number('forest'/'10E', '380').
card_multiverse_id('forest'/'10E', '129559').

card_in_set('forest', '10E').
card_original_type('forest'/'10E', 'Basic Land — Forest').
card_original_text('forest'/'10E', 'G').
card_image_name('forest'/'10E', 'forest2').
card_uid('forest'/'10E', '10E:Forest:forest2').
card_rarity('forest'/'10E', 'Basic Land').
card_artist('forest'/'10E', 'John Avon').
card_number('forest'/'10E', '381').
card_multiverse_id('forest'/'10E', '129560').

card_in_set('forest', '10E').
card_original_type('forest'/'10E', 'Basic Land — Forest').
card_original_text('forest'/'10E', 'G').
card_image_name('forest'/'10E', 'forest3').
card_uid('forest'/'10E', '10E:Forest:forest3').
card_rarity('forest'/'10E', 'Basic Land').
card_artist('forest'/'10E', 'Rob Alexander').
card_number('forest'/'10E', '382').
card_multiverse_id('forest'/'10E', '129561').

card_in_set('forest', '10E').
card_original_type('forest'/'10E', 'Basic Land — Forest').
card_original_text('forest'/'10E', 'G').
card_image_name('forest'/'10E', 'forest4').
card_uid('forest'/'10E', '10E:Forest:forest4').
card_rarity('forest'/'10E', 'Basic Land').
card_artist('forest'/'10E', 'Stephan Martiniere').
card_number('forest'/'10E', '383').
card_multiverse_id('forest'/'10E', '129562').

card_in_set('fountain of youth', '10E').
card_original_type('fountain of youth'/'10E', 'Artifact').
card_original_text('fountain of youth'/'10E', '{2}, {T}: You gain 1 life.').
card_image_name('fountain of youth'/'10E', 'fountain of youth').
card_uid('fountain of youth'/'10E', '10E:Fountain of Youth:fountain of youth').
card_rarity('fountain of youth'/'10E', 'Uncommon').
card_artist('fountain of youth'/'10E', 'Dan Scott').
card_number('fountain of youth'/'10E', '323').
card_flavor_text('fountain of youth'/'10E', 'Some say the fountain\'s waters are the first rains that fell over Dominaria.').
card_multiverse_id('fountain of youth'/'10E', '135273').

card_in_set('fugitive wizard', '10E').
card_original_type('fugitive wizard'/'10E', 'Creature — Human Wizard').
card_original_text('fugitive wizard'/'10E', '').
card_image_name('fugitive wizard'/'10E', 'fugitive wizard').
card_uid('fugitive wizard'/'10E', '10E:Fugitive Wizard:fugitive wizard').
card_rarity('fugitive wizard'/'10E', 'Common').
card_artist('fugitive wizard'/'10E', 'Mark Zug').
card_number('fugitive wizard'/'10E', '86').
card_flavor_text('fugitive wizard'/'10E', '\"The law has its place—as a footnote in my spellbook.\"\n—Siyani, fugitive mage').
card_multiverse_id('fugitive wizard'/'10E', '134762').

card_in_set('furnace of rath', '10E').
card_original_type('furnace of rath'/'10E', 'Enchantment').
card_original_text('furnace of rath'/'10E', 'If a source would deal damage to a creature or player, it deals double that damage to that creature or player instead.').
card_image_name('furnace of rath'/'10E', 'furnace of rath').
card_uid('furnace of rath'/'10E', '10E:Furnace of Rath:furnace of rath').
card_rarity('furnace of rath'/'10E', 'Rare').
card_artist('furnace of rath'/'10E', 'John Matson').
card_number('furnace of rath'/'10E', '204').
card_flavor_text('furnace of rath'/'10E', 'The furnace awaits the next master who would stoke the fires of apocalypse.').
card_multiverse_id('furnace of rath'/'10E', '129566').

card_in_set('furnace whelp', '10E').
card_original_type('furnace whelp'/'10E', 'Creature — Dragon').
card_original_text('furnace whelp'/'10E', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\n{R}: Furnace Whelp gets +1/+0 until end of turn.').
card_image_name('furnace whelp'/'10E', 'furnace whelp').
card_uid('furnace whelp'/'10E', '10E:Furnace Whelp:furnace whelp').
card_rarity('furnace whelp'/'10E', 'Uncommon').
card_artist('furnace whelp'/'10E', 'Matt Cavotta').
card_number('furnace whelp'/'10E', '205').
card_flavor_text('furnace whelp'/'10E', 'Baby dragons can\'t figure out humans—if they didn\'t want to be killed, why were they made of meat and treasure?').
card_multiverse_id('furnace whelp'/'10E', '130386').

card_in_set('gaea\'s herald', '10E').
card_original_type('gaea\'s herald'/'10E', 'Creature — Elf').
card_original_text('gaea\'s herald'/'10E', 'Creature spells can\'t be countered.').
card_image_name('gaea\'s herald'/'10E', 'gaea\'s herald').
card_uid('gaea\'s herald'/'10E', '10E:Gaea\'s Herald:gaea\'s herald').
card_rarity('gaea\'s herald'/'10E', 'Rare').
card_artist('gaea\'s herald'/'10E', 'Jim Murray').
card_number('gaea\'s herald'/'10E', '265').
card_flavor_text('gaea\'s herald'/'10E', '\"Gaea sings with the voice of nature rampant: a thousand howls, chitters, and cries, and none of them can be ignored.\"').
card_multiverse_id('gaea\'s herald'/'10E', '129789').

card_in_set('ghitu encampment', '10E').
card_original_type('ghitu encampment'/'10E', 'Land').
card_original_text('ghitu encampment'/'10E', 'Ghitu Encampment comes into play tapped.\n{T}: Add {R} to your mana pool.\n{1}{R}: Ghitu Encampment becomes a 2/1 red Warrior creature with first strike until end of turn. It\'s still a land. (It deals combat damage before creatures without first strike.)').
card_image_name('ghitu encampment'/'10E', 'ghitu encampment').
card_uid('ghitu encampment'/'10E', '10E:Ghitu Encampment:ghitu encampment').
card_rarity('ghitu encampment'/'10E', 'Uncommon').
card_artist('ghitu encampment'/'10E', 'John Avon').
card_number('ghitu encampment'/'10E', '353').
card_multiverse_id('ghitu encampment'/'10E', '106564').

card_in_set('ghost warden', '10E').
card_original_type('ghost warden'/'10E', 'Creature — Spirit').
card_original_text('ghost warden'/'10E', '{T}: Target creature gets +1/+1 until end of turn.').
card_image_name('ghost warden'/'10E', 'ghost warden').
card_uid('ghost warden'/'10E', '10E:Ghost Warden:ghost warden').
card_rarity('ghost warden'/'10E', 'Common').
card_artist('ghost warden'/'10E', 'Ittoku').
card_number('ghost warden'/'10E', '16').
card_flavor_text('ghost warden'/'10E', '\"I thought of fate as an iron lattice, intricate but rigidly unchangeable. That was until some force bent fate\'s bars to spare my life.\"\n—Ilromov, traveling storyteller').
card_multiverse_id('ghost warden'/'10E', '132105').

card_in_set('giant growth', '10E').
card_original_type('giant growth'/'10E', 'Instant').
card_original_text('giant growth'/'10E', 'Target creature gets +3/+3 until end of turn.').
card_image_name('giant growth'/'10E', 'giant growth').
card_uid('giant growth'/'10E', '10E:Giant Growth:giant growth').
card_rarity('giant growth'/'10E', 'Common').
card_artist('giant growth'/'10E', 'Matt Cavotta').
card_number('giant growth'/'10E', '266').
card_multiverse_id('giant growth'/'10E', '129568').

card_in_set('giant spider', '10E').
card_original_type('giant spider'/'10E', 'Creature — Spider').
card_original_text('giant spider'/'10E', 'Reach (This creature can block creatures with flying.)').
card_image_name('giant spider'/'10E', 'giant spider').
card_uid('giant spider'/'10E', '10E:Giant Spider:giant spider').
card_rarity('giant spider'/'10E', 'Common').
card_artist('giant spider'/'10E', 'Randy Gallegos').
card_number('giant spider'/'10E', '267').
card_flavor_text('giant spider'/'10E', '\"When I trained under Multani, he bade me sit for hours watching Yavimaya\'s spiders. From them I learned stealth, patience, and the best way to snare a wind drake.\"\n—Mirri of the Weatherlight').
card_multiverse_id('giant spider'/'10E', '129570').

card_in_set('glorious anthem', '10E').
card_original_type('glorious anthem'/'10E', 'Enchantment').
card_original_text('glorious anthem'/'10E', 'Creatures you control get +1/+1.').
card_image_name('glorious anthem'/'10E', 'glorious anthem').
card_uid('glorious anthem'/'10E', '10E:Glorious Anthem:glorious anthem').
card_rarity('glorious anthem'/'10E', 'Rare').
card_artist('glorious anthem'/'10E', 'Kev Walker').
card_number('glorious anthem'/'10E', '17').
card_flavor_text('glorious anthem'/'10E', 'Once heard, the battle song of an angel becomes part of the listener forever.').
card_multiverse_id('glorious anthem'/'10E', '129572').

card_in_set('goblin elite infantry', '10E').
card_original_type('goblin elite infantry'/'10E', 'Creature — Goblin Warrior').
card_original_text('goblin elite infantry'/'10E', 'Whenever Goblin Elite Infantry blocks or becomes blocked, it gets -1/-1 until end of turn.').
card_image_name('goblin elite infantry'/'10E', 'goblin elite infantry').
card_uid('goblin elite infantry'/'10E', '10E:Goblin Elite Infantry:goblin elite infantry').
card_rarity('goblin elite infantry'/'10E', 'Common').
card_artist('goblin elite infantry'/'10E', 'Robert Bliss').
card_number('goblin elite infantry'/'10E', '206').
card_flavor_text('goblin elite infantry'/'10E', 'They talk a good fight.').
card_multiverse_id('goblin elite infantry'/'10E', '130380').

card_in_set('goblin king', '10E').
card_original_type('goblin king'/'10E', 'Creature — Goblin').
card_original_text('goblin king'/'10E', 'Other Goblin creatures get +1/+1 and have mountainwalk. (They\'re unblockable as long as defending player controls a Mountain.)').
card_image_name('goblin king'/'10E', 'goblin king').
card_uid('goblin king'/'10E', '10E:Goblin King:goblin king').
card_rarity('goblin king'/'10E', 'Rare').
card_artist('goblin king'/'10E', 'Ron Spears').
card_number('goblin king'/'10E', '207').
card_flavor_text('goblin king'/'10E', 'To be king, Numsgil did in Blog, who did in Unkful, who did in Viddle, who did in Loll, who did in Alrok. . . .').
card_multiverse_id('goblin king'/'10E', '129578').

card_in_set('goblin lore', '10E').
card_original_type('goblin lore'/'10E', 'Sorcery').
card_original_text('goblin lore'/'10E', 'Draw four cards, then discard three cards at random.').
card_image_name('goblin lore'/'10E', 'goblin lore').
card_uid('goblin lore'/'10E', '10E:Goblin Lore:goblin lore').
card_rarity('goblin lore'/'10E', 'Uncommon').
card_artist('goblin lore'/'10E', 'D. Alexander Gregory').
card_number('goblin lore'/'10E', '208').
card_flavor_text('goblin lore'/'10E', '\"Now listen closely, my young gob. There are many ways to eat pimple bugs, but I say the best way is all smashed together between two slug chips.\"').
card_multiverse_id('goblin lore'/'10E', '135221').

card_in_set('goblin piker', '10E').
card_original_type('goblin piker'/'10E', 'Creature — Goblin Warrior').
card_original_text('goblin piker'/'10E', '').
card_image_name('goblin piker'/'10E', 'goblin piker').
card_uid('goblin piker'/'10E', '10E:Goblin Piker:goblin piker').
card_rarity('goblin piker'/'10E', 'Common').
card_artist('goblin piker'/'10E', 'DiTerlizzi').
card_number('goblin piker'/'10E', '209').
card_flavor_text('goblin piker'/'10E', 'Once he\'d worked out which end of the thing was sharp, he was promoted to guard duty.').
card_multiverse_id('goblin piker'/'10E', '129580').

card_in_set('goblin sky raider', '10E').
card_original_type('goblin sky raider'/'10E', 'Creature — Goblin Warrior').
card_original_text('goblin sky raider'/'10E', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)').
card_image_name('goblin sky raider'/'10E', 'goblin sky raider').
card_uid('goblin sky raider'/'10E', '10E:Goblin Sky Raider:goblin sky raider').
card_rarity('goblin sky raider'/'10E', 'Common').
card_artist('goblin sky raider'/'10E', 'Daren Bader').
card_number('goblin sky raider'/'10E', '210').
card_flavor_text('goblin sky raider'/'10E', 'The goblin word for \"flying\" is more accurately translated as \"falling slowly.\"').
card_multiverse_id('goblin sky raider'/'10E', '129582').

card_in_set('grave pact', '10E').
card_original_type('grave pact'/'10E', 'Enchantment').
card_original_text('grave pact'/'10E', 'Whenever a creature you control is put into a graveyard from play, each other player sacrifices a creature.').
card_image_name('grave pact'/'10E', 'grave pact').
card_uid('grave pact'/'10E', '10E:Grave Pact:grave pact').
card_rarity('grave pact'/'10E', 'Rare').
card_artist('grave pact'/'10E', 'Puddnhead').
card_number('grave pact'/'10E', '144').
card_flavor_text('grave pact'/'10E', '\"The bonds of loyalty can tie one to the grave.\"\n—Crovax, ascendant evincar').
card_multiverse_id('grave pact'/'10E', '129583').

card_in_set('graveborn muse', '10E').
card_original_type('graveborn muse'/'10E', 'Creature — Zombie Spirit').
card_original_text('graveborn muse'/'10E', 'At the beginning of your upkeep, you draw X cards and you lose X life, where X is the number of Zombies you control.').
card_image_name('graveborn muse'/'10E', 'graveborn muse').
card_uid('graveborn muse'/'10E', '10E:Graveborn Muse:graveborn muse').
card_rarity('graveborn muse'/'10E', 'Rare').
card_artist('graveborn muse'/'10E', 'Kev Walker').
card_number('graveborn muse'/'10E', '145').
card_flavor_text('graveborn muse'/'10E', '\"Her voice is damnation, unyielding and certain.\"\n—Phage the Untouchable').
card_multiverse_id('graveborn muse'/'10E', '135256').

card_in_set('gravedigger', '10E').
card_original_type('gravedigger'/'10E', 'Creature — Zombie').
card_original_text('gravedigger'/'10E', 'When Gravedigger comes into play, you may return target creature card from your graveyard to your hand.').
card_image_name('gravedigger'/'10E', 'gravedigger').
card_uid('gravedigger'/'10E', '10E:Gravedigger:gravedigger').
card_rarity('gravedigger'/'10E', 'Common').
card_artist('gravedigger'/'10E', 'Dermot Power').
card_number('gravedigger'/'10E', '146').
card_flavor_text('gravedigger'/'10E', 'A full coffin is like a full coffer—both are attractive to thieves.').
card_multiverse_id('gravedigger'/'10E', '129584').

card_in_set('grizzly bears', '10E').
card_original_type('grizzly bears'/'10E', 'Creature — Bear').
card_original_text('grizzly bears'/'10E', '').
card_image_name('grizzly bears'/'10E', 'grizzly bears').
card_uid('grizzly bears'/'10E', '10E:Grizzly Bears:grizzly bears').
card_rarity('grizzly bears'/'10E', 'Common').
card_artist('grizzly bears'/'10E', 'D. J. Cleland-Hura').
card_number('grizzly bears'/'10E', '268').
card_flavor_text('grizzly bears'/'10E', '\"We cannot forget that among all of Dominaria\'s wonders, a system of life exists, with prey and predators that will never fight wars nor vie for ancient power.\"\n—Jolrael, empress of beasts').
card_multiverse_id('grizzly bears'/'10E', '129586').

card_in_set('guerrilla tactics', '10E').
card_original_type('guerrilla tactics'/'10E', 'Instant').
card_original_text('guerrilla tactics'/'10E', 'Guerrilla Tactics deals 2 damage to target creature or player.\nWhen a spell or ability an opponent controls causes you to discard Guerrilla Tactics, Guerrilla Tactics deals 4 damage to target creature or player.').
card_image_name('guerrilla tactics'/'10E', 'guerrilla tactics').
card_uid('guerrilla tactics'/'10E', '10E:Guerrilla Tactics:guerrilla tactics').
card_rarity('guerrilla tactics'/'10E', 'Uncommon').
card_artist('guerrilla tactics'/'10E', 'Dave Dorman').
card_number('guerrilla tactics'/'10E', '211').
card_multiverse_id('guerrilla tactics'/'10E', '129588').

card_in_set('hail of arrows', '10E').
card_original_type('hail of arrows'/'10E', 'Instant').
card_original_text('hail of arrows'/'10E', 'Hail of Arrows deals X damage divided as you choose among any number of target attacking creatures.').
card_image_name('hail of arrows'/'10E', 'hail of arrows').
card_uid('hail of arrows'/'10E', '10E:Hail of Arrows:hail of arrows').
card_rarity('hail of arrows'/'10E', 'Uncommon').
card_artist('hail of arrows'/'10E', 'Anthony S. Waters').
card_number('hail of arrows'/'10E', '18').
card_flavor_text('hail of arrows'/'10E', '\"Do not let a single shaft loose until my word. And when I give that word, do not leave a single shaft in Eiganjo.\"\n—General Takeno').
card_multiverse_id('hail of arrows'/'10E', '132107').

card_in_set('hate weaver', '10E').
card_original_type('hate weaver'/'10E', 'Creature — Zombie Wizard').
card_original_text('hate weaver'/'10E', '{2}: Target blue or red creature gets +1/+0 until end of turn.').
card_image_name('hate weaver'/'10E', 'hate weaver').
card_uid('hate weaver'/'10E', '10E:Hate Weaver:hate weaver').
card_rarity('hate weaver'/'10E', 'Uncommon').
card_artist('hate weaver'/'10E', 'Roger Raupp').
card_number('hate weaver'/'10E', '147').
card_flavor_text('hate weaver'/'10E', '\"Let my hate fuel your fury.\"').
card_multiverse_id('hate weaver'/'10E', '130995').

card_in_set('head games', '10E').
card_original_type('head games'/'10E', 'Sorcery').
card_original_text('head games'/'10E', 'Target opponent puts the cards from his or her hand on top of his or her library. Search that player\'s library for that many cards. The player puts those cards into his or her hand, then shuffles his or her library.').
card_image_name('head games'/'10E', 'head games').
card_uid('head games'/'10E', '10E:Head Games:head games').
card_rarity('head games'/'10E', 'Rare').
card_artist('head games'/'10E', 'Terese Nielsen').
card_number('head games'/'10E', '148').
card_multiverse_id('head games'/'10E', '135255').

card_in_set('heart of light', '10E').
card_original_type('heart of light'/'10E', 'Enchantment — Aura').
card_original_text('heart of light'/'10E', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nPrevent all damage that would be dealt to and dealt by enchanted creature.').
card_image_name('heart of light'/'10E', 'heart of light').
card_uid('heart of light'/'10E', '10E:Heart of Light:heart of light').
card_rarity('heart of light'/'10E', 'Common').
card_artist('heart of light'/'10E', 'Luca Zontini').
card_number('heart of light'/'10E', '19').
card_flavor_text('heart of light'/'10E', 'For those who reach enlightenment, violence is an unnecessary distraction.').
card_multiverse_id('heart of light'/'10E', '132090').

card_in_set('hidden horror', '10E').
card_original_type('hidden horror'/'10E', 'Creature — Horror').
card_original_text('hidden horror'/'10E', 'When Hidden Horror comes into play, sacrifice it unless you discard a creature card.').
card_image_name('hidden horror'/'10E', 'hidden horror').
card_uid('hidden horror'/'10E', '10E:Hidden Horror:hidden horror').
card_rarity('hidden horror'/'10E', 'Uncommon').
card_artist('hidden horror'/'10E', 'Brom').
card_number('hidden horror'/'10E', '149').
card_flavor_text('hidden horror'/'10E', 'If the presence of evil were obvious, it wouldn\'t be nearly as dangerous.').
card_multiverse_id('hidden horror'/'10E', '135234').

card_in_set('high ground', '10E').
card_original_type('high ground'/'10E', 'Enchantment').
card_original_text('high ground'/'10E', 'Each creature you control can block an additional creature.').
card_image_name('high ground'/'10E', 'high ground').
card_uid('high ground'/'10E', '10E:High Ground:high ground').
card_rarity('high ground'/'10E', 'Uncommon').
card_artist('high ground'/'10E', 'rk post').
card_number('high ground'/'10E', '20').
card_flavor_text('high ground'/'10E', 'In war, as in society, position is everything.').
card_multiverse_id('high ground'/'10E', '132145').

card_in_set('highway robber', '10E').
card_original_type('highway robber'/'10E', 'Creature — Human Mercenary').
card_original_text('highway robber'/'10E', 'When Highway Robber comes into play, you gain 2 life and target opponent loses 2 life.').
card_image_name('highway robber'/'10E', 'highway robber').
card_uid('highway robber'/'10E', '10E:Highway Robber:highway robber').
card_rarity('highway robber'/'10E', 'Common').
card_artist('highway robber'/'10E', 'Kev Walker').
card_number('highway robber'/'10E', '150').
card_flavor_text('highway robber'/'10E', '\"Tonight, madam, it\'s your money and your life.\"').
card_multiverse_id('highway robber'/'10E', '129564').

card_in_set('hill giant', '10E').
card_original_type('hill giant'/'10E', 'Creature — Giant').
card_original_text('hill giant'/'10E', '').
card_image_name('hill giant'/'10E', 'hill giant').
card_uid('hill giant'/'10E', '10E:Hill Giant:hill giant').
card_rarity('hill giant'/'10E', 'Common').
card_artist('hill giant'/'10E', 'Kev Walker').
card_number('hill giant'/'10E', '212').
card_flavor_text('hill giant'/'10E', 'Fortunately, hill giants have large blind spots in which a human can easily hide. Unfortunately, these blind spots are beneath the bottoms of their feet.').
card_multiverse_id('hill giant'/'10E', '129591').

card_in_set('holy day', '10E').
card_original_type('holy day'/'10E', 'Instant').
card_original_text('holy day'/'10E', 'Prevent all combat damage that would be dealt this turn.').
card_image_name('holy day'/'10E', 'holy day').
card_uid('holy day'/'10E', '10E:Holy Day:holy day').
card_rarity('holy day'/'10E', 'Common').
card_artist('holy day'/'10E', 'Volkan Baga').
card_number('holy day'/'10E', '21').
card_flavor_text('holy day'/'10E', '\"Today there is feasting and peace across our land, but the war has not ended. Tuck away your bloodlust. You\'ll need it tomorrow.\"\n—Karrim, Samite healer').
card_multiverse_id('holy day'/'10E', '129593').

card_in_set('holy strength', '10E').
card_original_type('holy strength'/'10E', 'Enchantment — Aura').
card_original_text('holy strength'/'10E', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nEnchanted creature gets +1/+2.').
card_image_name('holy strength'/'10E', 'holy strength').
card_uid('holy strength'/'10E', '10E:Holy Strength:holy strength').
card_rarity('holy strength'/'10E', 'Common').
card_artist('holy strength'/'10E', 'Terese Nielsen').
card_number('holy strength'/'10E', '22').
card_flavor_text('holy strength'/'10E', '\"May angels fly at your back. May your blade cleave the darkness.\"\n—War blessing of Serra').
card_multiverse_id('holy strength'/'10E', '129594').

card_in_set('honor guard', '10E').
card_original_type('honor guard'/'10E', 'Creature — Human Soldier').
card_original_text('honor guard'/'10E', '{W}: Honor Guard gets +0/+1 until end of turn.').
card_image_name('honor guard'/'10E', 'honor guard').
card_uid('honor guard'/'10E', '10E:Honor Guard:honor guard').
card_rarity('honor guard'/'10E', 'Common').
card_artist('honor guard'/'10E', 'Dan Dos Santos').
card_number('honor guard'/'10E', '23').
card_flavor_text('honor guard'/'10E', 'The strength of one. The courage of ten.').
card_multiverse_id('honor guard'/'10E', '129595').

card_in_set('horseshoe crab', '10E').
card_original_type('horseshoe crab'/'10E', 'Creature — Crab').
card_original_text('horseshoe crab'/'10E', '{U}: Untap Horseshoe Crab.').
card_image_name('horseshoe crab'/'10E', 'horseshoe crab').
card_uid('horseshoe crab'/'10E', '10E:Horseshoe Crab:horseshoe crab').
card_rarity('horseshoe crab'/'10E', 'Common').
card_artist('horseshoe crab'/'10E', 'Scott Kirschner').
card_number('horseshoe crab'/'10E', '87').
card_flavor_text('horseshoe crab'/'10E', '\"Hmm . . . It looks kinda like a bug. Let\'s crack it open an\' see if it tastes like one!\"\n—Squee, goblin cabin hand').
card_multiverse_id('horseshoe crab'/'10E', '129596').

card_in_set('howling mine', '10E').
card_original_type('howling mine'/'10E', 'Artifact').
card_original_text('howling mine'/'10E', 'At the beginning of each player\'s draw step, if Howling Mine is untapped, that player draws a card.').
card_image_name('howling mine'/'10E', 'howling mine').
card_uid('howling mine'/'10E', '10E:Howling Mine:howling mine').
card_rarity('howling mine'/'10E', 'Rare').
card_artist('howling mine'/'10E', 'Ralph Horsley').
card_number('howling mine'/'10E', '325').
card_flavor_text('howling mine'/'10E', 'Legend has it that the mine howls out the last words of those who died inside.').
card_multiverse_id('howling mine'/'10E', '129598').

card_in_set('hunted wumpus', '10E').
card_original_type('hunted wumpus'/'10E', 'Creature — Beast').
card_original_text('hunted wumpus'/'10E', 'When Hunted Wumpus comes into play, each other player may put a creature card from his or her hand into play.').
card_image_name('hunted wumpus'/'10E', 'hunted wumpus').
card_uid('hunted wumpus'/'10E', '10E:Hunted Wumpus:hunted wumpus').
card_rarity('hunted wumpus'/'10E', 'Uncommon').
card_artist('hunted wumpus'/'10E', 'Thomas M. Baxa').
card_number('hunted wumpus'/'10E', '269').
card_flavor_text('hunted wumpus'/'10E', 'Just one can feed a dozen people for a month.').
card_multiverse_id('hunted wumpus'/'10E', '129599').

card_in_set('hurkyl\'s recall', '10E').
card_original_type('hurkyl\'s recall'/'10E', 'Instant').
card_original_text('hurkyl\'s recall'/'10E', 'Return all artifacts target player owns to his or her hand.').
card_image_name('hurkyl\'s recall'/'10E', 'hurkyl\'s recall').
card_uid('hurkyl\'s recall'/'10E', '10E:Hurkyl\'s Recall:hurkyl\'s recall').
card_rarity('hurkyl\'s recall'/'10E', 'Rare').
card_artist('hurkyl\'s recall'/'10E', 'Ralph Horsley').
card_number('hurkyl\'s recall'/'10E', '88').
card_flavor_text('hurkyl\'s recall'/'10E', 'Hurkyl\'s research at the College of Lat-Nam wasn\'t enough to stop the two brothers, but for centuries thereafter her spellcraft taught artificers restraint.').
card_multiverse_id('hurkyl\'s recall'/'10E', '135260').

card_in_set('hurricane', '10E').
card_original_type('hurricane'/'10E', 'Sorcery').
card_original_text('hurricane'/'10E', 'Hurricane deals X damage to each creature with flying and each player.').
card_image_name('hurricane'/'10E', 'hurricane').
card_uid('hurricane'/'10E', '10E:Hurricane:hurricane').
card_rarity('hurricane'/'10E', 'Rare').
card_artist('hurricane'/'10E', 'John Howe').
card_number('hurricane'/'10E', '270').
card_flavor_text('hurricane'/'10E', '\"Don\'t envy the grandeur of drakes. Their impression of grace fades when the first gust sends them crashing into one another.\"\n—Molimo, maro-sorcerer').
card_multiverse_id('hurricane'/'10E', '129885').

card_in_set('hypnotic specter', '10E').
card_original_type('hypnotic specter'/'10E', 'Creature — Specter').
card_original_text('hypnotic specter'/'10E', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nWhenever Hypnotic Specter deals damage to an opponent, that player discards a card at random.').
card_image_name('hypnotic specter'/'10E', 'hypnotic specter').
card_uid('hypnotic specter'/'10E', '10E:Hypnotic Specter:hypnotic specter').
card_rarity('hypnotic specter'/'10E', 'Rare').
card_artist('hypnotic specter'/'10E', 'Greg Staples').
card_number('hypnotic specter'/'10E', '151').
card_flavor_text('hypnotic specter'/'10E', 'Its victims are known by their eyes: shattered vessels leaking broken dreams.').
card_multiverse_id('hypnotic specter'/'10E', '129600').

card_in_set('icatian priest', '10E').
card_original_type('icatian priest'/'10E', 'Creature — Human Cleric').
card_original_text('icatian priest'/'10E', '{1}{W}{W}: Target creature gets +1/+1 until end of turn.').
card_image_name('icatian priest'/'10E', 'icatian priest').
card_uid('icatian priest'/'10E', '10E:Icatian Priest:icatian priest').
card_rarity('icatian priest'/'10E', 'Uncommon').
card_artist('icatian priest'/'10E', 'Stephen Tappin').
card_number('icatian priest'/'10E', '24').
card_flavor_text('icatian priest'/'10E', 'Grelden knelt and felt the cool, dry hand of the priest on his brow. Hours later, when his wits returned, he was covered in his enemies\' blood on the field of victory.').
card_multiverse_id('icatian priest'/'10E', '132123').

card_in_set('icy manipulator', '10E').
card_original_type('icy manipulator'/'10E', 'Artifact').
card_original_text('icy manipulator'/'10E', '{1}, {T}: Tap target artifact, creature, or land.').
card_image_name('icy manipulator'/'10E', 'icy manipulator').
card_uid('icy manipulator'/'10E', '10E:Icy Manipulator:icy manipulator').
card_rarity('icy manipulator'/'10E', 'Uncommon').
card_artist('icy manipulator'/'10E', 'Matt Cavotta').
card_number('icy manipulator'/'10E', '326').
card_flavor_text('icy manipulator'/'10E', 'In fire there is the spark of chaos and destruction, the seed of life. In ice there is perfect tranquility, perfect order, and the silence of death.').
card_multiverse_id('icy manipulator'/'10E', '129601').

card_in_set('incinerate', '10E').
card_original_type('incinerate'/'10E', 'Instant').
card_original_text('incinerate'/'10E', 'Incinerate deals 3 damage to target creature or player. A creature dealt damage this way can\'t be regenerated this turn.').
card_image_name('incinerate'/'10E', 'incinerate').
card_uid('incinerate'/'10E', '10E:Incinerate:incinerate').
card_rarity('incinerate'/'10E', 'Common').
card_artist('incinerate'/'10E', 'Zoltan Boros & Gabor Szikszai').
card_number('incinerate'/'10E', '213').
card_flavor_text('incinerate'/'10E', '\"Yes, I think ‘toast\' is an appropriate description.\"\n—Jaya Ballard, task mage').
card_multiverse_id('incinerate'/'10E', '134751').

card_in_set('island', '10E').
card_original_type('island'/'10E', 'Basic Land — Island').
card_original_text('island'/'10E', 'U').
card_image_name('island'/'10E', 'island1').
card_uid('island'/'10E', '10E:Island:island1').
card_rarity('island'/'10E', 'Basic Land').
card_artist('island'/'10E', 'Donato Giancola').
card_number('island'/'10E', '368').
card_multiverse_id('island'/'10E', '129606').

card_in_set('island', '10E').
card_original_type('island'/'10E', 'Basic Land — Island').
card_original_text('island'/'10E', 'U').
card_image_name('island'/'10E', 'island2').
card_uid('island'/'10E', '10E:Island:island2').
card_rarity('island'/'10E', 'Basic Land').
card_artist('island'/'10E', 'John Avon').
card_number('island'/'10E', '369').
card_multiverse_id('island'/'10E', '129607').

card_in_set('island', '10E').
card_original_type('island'/'10E', 'Basic Land — Island').
card_original_text('island'/'10E', 'U').
card_image_name('island'/'10E', 'island3').
card_uid('island'/'10E', '10E:Island:island3').
card_rarity('island'/'10E', 'Basic Land').
card_artist('island'/'10E', 'Martina Pilcerova').
card_number('island'/'10E', '370').
card_multiverse_id('island'/'10E', '129608').

card_in_set('island', '10E').
card_original_type('island'/'10E', 'Basic Land — Island').
card_original_text('island'/'10E', 'U').
card_image_name('island'/'10E', 'island4').
card_uid('island'/'10E', '10E:Island:island4').
card_rarity('island'/'10E', 'Basic Land').
card_artist('island'/'10E', 'Stephan Martiniere').
card_number('island'/'10E', '371').
card_multiverse_id('island'/'10E', '129609').

card_in_set('jayemdae tome', '10E').
card_original_type('jayemdae tome'/'10E', 'Artifact').
card_original_text('jayemdae tome'/'10E', '{4}, {T}: Draw a card.').
card_image_name('jayemdae tome'/'10E', 'jayemdae tome').
card_uid('jayemdae tome'/'10E', '10E:Jayemdae Tome:jayemdae tome').
card_rarity('jayemdae tome'/'10E', 'Rare').
card_artist('jayemdae tome'/'10E', 'Donato Giancola').
card_number('jayemdae tome'/'10E', '327').
card_multiverse_id('jayemdae tome'/'10E', '106478').

card_in_set('joiner adept', '10E').
card_original_type('joiner adept'/'10E', 'Creature — Elf Druid').
card_original_text('joiner adept'/'10E', 'Lands you control have \"{T}: Add one mana of any color to your mana pool.\"').
card_image_name('joiner adept'/'10E', 'joiner adept').
card_uid('joiner adept'/'10E', '10E:Joiner Adept:joiner adept').
card_rarity('joiner adept'/'10E', 'Rare').
card_artist('joiner adept'/'10E', 'Heather Hudson').
card_number('joiner adept'/'10E', '271').
card_flavor_text('joiner adept'/'10E', 'A talented apprentice makes all things. The master makes all things possible.').
card_multiverse_id('joiner adept'/'10E', '130500').

card_in_set('juggernaut', '10E').
card_original_type('juggernaut'/'10E', 'Artifact Creature — Juggernaut').
card_original_text('juggernaut'/'10E', 'Juggernaut attacks each turn if able.\nJuggernaut can\'t be blocked by Walls.').
card_image_name('juggernaut'/'10E', 'juggernaut').
card_uid('juggernaut'/'10E', '10E:Juggernaut:juggernaut').
card_rarity('juggernaut'/'10E', 'Uncommon').
card_artist('juggernaut'/'10E', 'Arnie Swekel').
card_number('juggernaut'/'10E', '328').
card_flavor_text('juggernaut'/'10E', 'Built with neither a way to steer nor a way to stop, the juggernauts were simply aimed at an enemy\'s best defenses and told to charge.').
card_multiverse_id('juggernaut'/'10E', '135240').

card_in_set('kamahl, pit fighter', '10E').
card_original_type('kamahl, pit fighter'/'10E', 'Legendary Creature — Human Barbarian').
card_original_text('kamahl, pit fighter'/'10E', 'Haste (This creature can attack and {T} as soon as it comes under your control.)\n{T}: Kamahl, Pit Fighter deals 3 damage to target creature or player.').
card_image_name('kamahl, pit fighter'/'10E', 'kamahl, pit fighter').
card_uid('kamahl, pit fighter'/'10E', '10E:Kamahl, Pit Fighter:kamahl, pit fighter').
card_rarity('kamahl, pit fighter'/'10E', 'Rare').
card_artist('kamahl, pit fighter'/'10E', 'Kev Walker').
card_number('kamahl, pit fighter'/'10E', '214').
card_flavor_text('kamahl, pit fighter'/'10E', 'In times when freedom seems lost, great souls arise to reclaim it.').
card_multiverse_id('kamahl, pit fighter'/'10E', '106398').

card_in_set('karplusan forest', '10E').
card_original_type('karplusan forest'/'10E', 'Land').
card_original_text('karplusan forest'/'10E', '{T}: Add {1} to your mana pool.\n{T}: Add {R} or {G} to your mana pool. Karplusan Forest deals 1 damage to you.').
card_image_name('karplusan forest'/'10E', 'karplusan forest').
card_uid('karplusan forest'/'10E', '10E:Karplusan Forest:karplusan forest').
card_rarity('karplusan forest'/'10E', 'Rare').
card_artist('karplusan forest'/'10E', 'John Avon').
card_number('karplusan forest'/'10E', '354').
card_multiverse_id('karplusan forest'/'10E', '129614').

card_in_set('karplusan strider', '10E').
card_original_type('karplusan strider'/'10E', 'Creature — Yeti').
card_original_text('karplusan strider'/'10E', 'Karplusan Strider can\'t be the target of blue or black spells.').
card_image_name('karplusan strider'/'10E', 'karplusan strider').
card_uid('karplusan strider'/'10E', '10E:Karplusan Strider:karplusan strider').
card_rarity('karplusan strider'/'10E', 'Uncommon').
card_artist('karplusan strider'/'10E', 'Dan Scott').
card_number('karplusan strider'/'10E', '272').
card_flavor_text('karplusan strider'/'10E', 'The strider\'s long, loping gait is an adaptation that allows it to move quickly in deep snow.').
card_multiverse_id('karplusan strider'/'10E', '129911').

card_in_set('kavu climber', '10E').
card_original_type('kavu climber'/'10E', 'Creature — Kavu').
card_original_text('kavu climber'/'10E', 'When Kavu Climber comes into play, draw a card.').
card_image_name('kavu climber'/'10E', 'kavu climber').
card_uid('kavu climber'/'10E', '10E:Kavu Climber:kavu climber').
card_rarity('kavu climber'/'10E', 'Common').
card_artist('kavu climber'/'10E', 'Rob Alexander').
card_number('kavu climber'/'10E', '273').
card_flavor_text('kavu climber'/'10E', '\"There was a crack of branches, a rustle of leaves, then a tremendous roar. Our party had no chance as death descended from above.\"\n—Taseen, elvish bard').
card_multiverse_id('kavu climber'/'10E', '129511').

card_in_set('kjeldoran royal guard', '10E').
card_original_type('kjeldoran royal guard'/'10E', 'Creature — Human Soldier').
card_original_text('kjeldoran royal guard'/'10E', '{T}: All combat damage that would be dealt to you by unblocked creatures this turn is dealt to Kjeldoran Royal Guard instead.').
card_image_name('kjeldoran royal guard'/'10E', 'kjeldoran royal guard').
card_uid('kjeldoran royal guard'/'10E', '10E:Kjeldoran Royal Guard:kjeldoran royal guard').
card_rarity('kjeldoran royal guard'/'10E', 'Rare').
card_artist('kjeldoran royal guard'/'10E', 'Carl Critchlow').
card_number('kjeldoran royal guard'/'10E', '25').
card_flavor_text('kjeldoran royal guard'/'10E', 'Upon the frozen tundra stand the Kjeldoran Royal Guard, pikes raised, with the king\'s oath upon their lips.').
card_multiverse_id('kjeldoran royal guard'/'10E', '130551').

card_in_set('knight of dusk', '10E').
card_original_type('knight of dusk'/'10E', 'Creature — Human Knight').
card_original_text('knight of dusk'/'10E', '{B}{B}: Destroy target creature blocking Knight of Dusk.').
card_image_name('knight of dusk'/'10E', 'knight of dusk').
card_uid('knight of dusk'/'10E', '10E:Knight of Dusk:knight of dusk').
card_rarity('knight of dusk'/'10E', 'Uncommon').
card_artist('knight of dusk'/'10E', 'rk post').
card_number('knight of dusk'/'10E', '152').
card_flavor_text('knight of dusk'/'10E', '\"Challenge me with your strongest. I will paint my sword with their blood.\"').
card_multiverse_id('knight of dusk'/'10E', '129574').

card_in_set('kraken\'s eye', '10E').
card_original_type('kraken\'s eye'/'10E', 'Artifact').
card_original_text('kraken\'s eye'/'10E', 'Whenever a player plays a blue spell, you may gain 1 life.').
card_image_name('kraken\'s eye'/'10E', 'kraken\'s eye').
card_uid('kraken\'s eye'/'10E', '10E:Kraken\'s Eye:kraken\'s eye').
card_rarity('kraken\'s eye'/'10E', 'Uncommon').
card_artist('kraken\'s eye'/'10E', 'Alan Pollack').
card_number('kraken\'s eye'/'10E', '329').
card_flavor_text('kraken\'s eye'/'10E', 'Bright as a mirror, dark as the sea.').
card_multiverse_id('kraken\'s eye'/'10E', '129619').

card_in_set('lava axe', '10E').
card_original_type('lava axe'/'10E', 'Sorcery').
card_original_text('lava axe'/'10E', 'Lava Axe deals 5 damage to target player.').
card_image_name('lava axe'/'10E', 'lava axe').
card_uid('lava axe'/'10E', '10E:Lava Axe:lava axe').
card_rarity('lava axe'/'10E', 'Common').
card_artist('lava axe'/'10E', 'Brian Snõddy').
card_number('lava axe'/'10E', '215').
card_flavor_text('lava axe'/'10E', '\"Catch!\"').
card_multiverse_id('lava axe'/'10E', '129620').

card_in_set('lavaborn muse', '10E').
card_original_type('lavaborn muse'/'10E', 'Creature — Spirit').
card_original_text('lavaborn muse'/'10E', 'At the beginning of each opponent\'s upkeep, if that player has two or fewer cards in hand, Lavaborn Muse deals 3 damage to him or her.').
card_image_name('lavaborn muse'/'10E', 'lavaborn muse').
card_uid('lavaborn muse'/'10E', '10E:Lavaborn Muse:lavaborn muse').
card_rarity('lavaborn muse'/'10E', 'Rare').
card_artist('lavaborn muse'/'10E', 'Brian Snõddy').
card_number('lavaborn muse'/'10E', '216').
card_flavor_text('lavaborn muse'/'10E', '\"Her voice is disaster, painful and final.\"\n—Matoc, lavamancer').
card_multiverse_id('lavaborn muse'/'10E', '130373').

card_in_set('legacy weapon', '10E').
card_original_type('legacy weapon'/'10E', 'Legendary Artifact').
card_original_text('legacy weapon'/'10E', '{W}{U}{B}{R}{G}: Remove target permanent from the game.\nIf Legacy Weapon would be put into a graveyard from anywhere, reveal Legacy Weapon and shuffle it into its owner\'s library instead.').
card_image_name('legacy weapon'/'10E', 'legacy weapon').
card_uid('legacy weapon'/'10E', '10E:Legacy Weapon:legacy weapon').
card_rarity('legacy weapon'/'10E', 'Rare').
card_artist('legacy weapon'/'10E', 'Terese Nielsen').
card_number('legacy weapon'/'10E', '330').
card_multiverse_id('legacy weapon'/'10E', '135242').

card_in_set('leonin scimitar', '10E').
card_original_type('leonin scimitar'/'10E', 'Artifact — Equipment').
card_original_text('leonin scimitar'/'10E', 'Equipped creature gets +1/+1.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_image_name('leonin scimitar'/'10E', 'leonin scimitar').
card_uid('leonin scimitar'/'10E', '10E:Leonin Scimitar:leonin scimitar').
card_rarity('leonin scimitar'/'10E', 'Uncommon').
card_artist('leonin scimitar'/'10E', 'Doug Chaffee').
card_number('leonin scimitar'/'10E', '331').
card_flavor_text('leonin scimitar'/'10E', '\"Every scratch tells a story, and every notch has a name. Four generations have held this blade. You shall not disappoint them.\"\n—Initiation of Laena, skyhunter').
card_multiverse_id('leonin scimitar'/'10E', '135277').

card_in_set('lightning elemental', '10E').
card_original_type('lightning elemental'/'10E', 'Creature — Elemental').
card_original_text('lightning elemental'/'10E', 'Haste (This creature can attack and {T} as soon as it comes under your control.)').
card_image_name('lightning elemental'/'10E', 'lightning elemental').
card_uid('lightning elemental'/'10E', '10E:Lightning Elemental:lightning elemental').
card_rarity('lightning elemental'/'10E', 'Common').
card_artist('lightning elemental'/'10E', 'Kev Walker').
card_number('lightning elemental'/'10E', '217').
card_flavor_text('lightning elemental'/'10E', '\"A flash of the lightning, a break of the wave,\nHe passes from life to his rest in the grave.\"\n—William Knox, \"Mortality\"').
card_multiverse_id('lightning elemental'/'10E', '129624').

card_in_set('llanowar elves', '10E').
card_original_type('llanowar elves'/'10E', 'Creature — Elf Druid').
card_original_text('llanowar elves'/'10E', '{T}: Add {G} to your mana pool.').
card_image_name('llanowar elves'/'10E', 'llanowar elves').
card_uid('llanowar elves'/'10E', '10E:Llanowar Elves:llanowar elves').
card_rarity('llanowar elves'/'10E', 'Common').
card_artist('llanowar elves'/'10E', 'Kev Walker').
card_number('llanowar elves'/'10E', '274').
card_flavor_text('llanowar elves'/'10E', 'One bone broken for every twig snapped underfoot.\n—Llanowar penalty for trespassing').
card_multiverse_id('llanowar elves'/'10E', '129626').

card_in_set('llanowar sentinel', '10E').
card_original_type('llanowar sentinel'/'10E', 'Creature — Elf').
card_original_text('llanowar sentinel'/'10E', 'When Llanowar Sentinel comes into play, you may pay {1}{G}. If you do, search your library for a card named Llanowar Sentinel and put that card into play. Then shuffle your library.').
card_image_name('llanowar sentinel'/'10E', 'llanowar sentinel').
card_uid('llanowar sentinel'/'10E', '10E:Llanowar Sentinel:llanowar sentinel').
card_rarity('llanowar sentinel'/'10E', 'Common').
card_artist('llanowar sentinel'/'10E', 'Randy Gallegos').
card_number('llanowar sentinel'/'10E', '275').
card_flavor_text('llanowar sentinel'/'10E', '\"The forest has as many eyes as leaves.\"\n—Mirri of the Weatherlight').
card_multiverse_id('llanowar sentinel'/'10E', '129536').

card_in_set('llanowar wastes', '10E').
card_original_type('llanowar wastes'/'10E', 'Land').
card_original_text('llanowar wastes'/'10E', '{T}: Add {1} to your mana pool.\n{T}: Add {B} or {G} to your mana pool. Llanowar Wastes deals 1 damage to you.').
card_image_name('llanowar wastes'/'10E', 'llanowar wastes').
card_uid('llanowar wastes'/'10E', '10E:Llanowar Wastes:llanowar wastes').
card_rarity('llanowar wastes'/'10E', 'Rare').
card_artist('llanowar wastes'/'10E', 'Rob Alexander').
card_number('llanowar wastes'/'10E', '355').
card_multiverse_id('llanowar wastes'/'10E', '129627').

card_in_set('looming shade', '10E').
card_original_type('looming shade'/'10E', 'Creature — Shade').
card_original_text('looming shade'/'10E', '{B}: Looming Shade gets +1/+1 until end of turn.').
card_image_name('looming shade'/'10E', 'looming shade').
card_uid('looming shade'/'10E', '10E:Looming Shade:looming shade').
card_rarity('looming shade'/'10E', 'Common').
card_artist('looming shade'/'10E', 'Kev Walker').
card_number('looming shade'/'10E', '153').
card_flavor_text('looming shade'/'10E', 'Its form never rests, propelled by some undetectable gale from beyond the gloom.').
card_multiverse_id('looming shade'/'10E', '129628').

card_in_set('lord of the pit', '10E').
card_original_type('lord of the pit'/'10E', 'Creature — Demon').
card_original_text('lord of the pit'/'10E', 'Flying, trample (This creature can\'t be blocked except by creatures with flying or reach. If this creature would deal enough combat damage to its blockers to destroy them, you may have it deal the rest of its damage to defending player.)\nAt the beginning of your upkeep, sacrifice a creature other than Lord of the Pit. If you can\'t, Lord of the Pit deals 7 damage to you.').
card_image_name('lord of the pit'/'10E', 'lord of the pit').
card_uid('lord of the pit'/'10E', '10E:Lord of the Pit:lord of the pit').
card_rarity('lord of the pit'/'10E', 'Rare').
card_artist('lord of the pit'/'10E', 'Michael Sutfin').
card_number('lord of the pit'/'10E', '154').
card_multiverse_id('lord of the pit'/'10E', '135271').

card_in_set('lord of the undead', '10E').
card_original_type('lord of the undead'/'10E', 'Creature — Zombie').
card_original_text('lord of the undead'/'10E', 'Other Zombie creatures get +1/+1.\n{1}{B}, {T}: Return target Zombie card from your graveyard to your hand.').
card_image_name('lord of the undead'/'10E', 'lord of the undead').
card_uid('lord of the undead'/'10E', '10E:Lord of the Undead:lord of the undead').
card_rarity('lord of the undead'/'10E', 'Rare').
card_artist('lord of the undead'/'10E', 'Brom').
card_number('lord of the undead'/'10E', '155').
card_flavor_text('lord of the undead'/'10E', '\"Do not weep at your defeat, my dear. In death, you will serve a greater purpose—mine.\"').
card_multiverse_id('lord of the undead'/'10E', '129629').

card_in_set('loxodon mystic', '10E').
card_original_type('loxodon mystic'/'10E', 'Creature — Elephant Cleric').
card_original_text('loxodon mystic'/'10E', '{W}, {T}: Tap target creature.').
card_image_name('loxodon mystic'/'10E', 'loxodon mystic').
card_uid('loxodon mystic'/'10E', '10E:Loxodon Mystic:loxodon mystic').
card_rarity('loxodon mystic'/'10E', 'Common').
card_artist('loxodon mystic'/'10E', 'Randy Gallegos').
card_number('loxodon mystic'/'10E', '26').
card_flavor_text('loxodon mystic'/'10E', 'Elder mystics take their vow of silence so seriously that they impose it on any who enter their presence.').
card_multiverse_id('loxodon mystic'/'10E', '129638').

card_in_set('loxodon warhammer', '10E').
card_original_type('loxodon warhammer'/'10E', 'Artifact — Equipment').
card_original_text('loxodon warhammer'/'10E', 'Equipped creature gets +3/+0 and has lifelink and trample. (When it deals damage, you gain that much life. If it would deal enough combat damage to its blockers to destroy them, you may have it deal the rest of its damage to defending player.)\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery.)').
card_image_name('loxodon warhammer'/'10E', 'loxodon warhammer').
card_uid('loxodon warhammer'/'10E', '10E:Loxodon Warhammer:loxodon warhammer').
card_rarity('loxodon warhammer'/'10E', 'Rare').
card_artist('loxodon warhammer'/'10E', 'Jeremy Jarvis').
card_number('loxodon warhammer'/'10E', '332').
card_multiverse_id('loxodon warhammer'/'10E', '129630').

card_in_set('loyal sentry', '10E').
card_original_type('loyal sentry'/'10E', 'Creature — Human Soldier').
card_original_text('loyal sentry'/'10E', 'When Loyal Sentry blocks a creature, destroy that creature and Loyal Sentry.').
card_image_name('loyal sentry'/'10E', 'loyal sentry').
card_uid('loyal sentry'/'10E', '10E:Loyal Sentry:loyal sentry').
card_rarity('loyal sentry'/'10E', 'Rare').
card_artist('loyal sentry'/'10E', 'Michael Sutfin').
card_number('loyal sentry'/'10E', '27').
card_flavor_text('loyal sentry'/'10E', '\"My cause is simple: To stop you, at any cost, from ever seeing the inside of this keep.\"').
card_multiverse_id('loyal sentry'/'10E', '129798').

card_in_set('lumengrid warden', '10E').
card_original_type('lumengrid warden'/'10E', 'Creature — Human Wizard').
card_original_text('lumengrid warden'/'10E', '').
card_image_name('lumengrid warden'/'10E', 'lumengrid warden').
card_uid('lumengrid warden'/'10E', '10E:Lumengrid Warden:lumengrid warden').
card_rarity('lumengrid warden'/'10E', 'Common').
card_artist('lumengrid warden'/'10E', 'Francis Tsai').
card_number('lumengrid warden'/'10E', '89').
card_flavor_text('lumengrid warden'/'10E', 'The wardens keep watch over an endless sea of progress.').
card_multiverse_id('lumengrid warden'/'10E', '129631').

card_in_set('luminesce', '10E').
card_original_type('luminesce'/'10E', 'Instant').
card_original_text('luminesce'/'10E', 'Prevent all damage that black sources and red sources would deal this turn.').
card_image_name('luminesce'/'10E', 'luminesce').
card_uid('luminesce'/'10E', '10E:Luminesce:luminesce').
card_rarity('luminesce'/'10E', 'Uncommon').
card_artist('luminesce'/'10E', 'Daren Bader').
card_number('luminesce'/'10E', '28').
card_flavor_text('luminesce'/'10E', '\"The White Shield is not the burnished metal you lash to your forearm but the conviction that burns in your chest.\"\n—Lucilde Fiksdotter,\nleader of the Order of the White Shield').
card_multiverse_id('luminesce'/'10E', '129912').

card_in_set('lure', '10E').
card_original_type('lure'/'10E', 'Enchantment — Aura').
card_original_text('lure'/'10E', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nAll creatures able to block enchanted creature do so.').
card_image_name('lure'/'10E', 'lure').
card_uid('lure'/'10E', '10E:Lure:lure').
card_rarity('lure'/'10E', 'Uncommon').
card_artist('lure'/'10E', 'D. Alexander Gregory').
card_number('lure'/'10E', '276').
card_flavor_text('lure'/'10E', 'Her true theft was to steal the soldiers away from their posts.').
card_multiverse_id('lure'/'10E', '135226').

card_in_set('mahamoti djinn', '10E').
card_original_type('mahamoti djinn'/'10E', 'Creature — Djinn').
card_original_text('mahamoti djinn'/'10E', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)').
card_image_name('mahamoti djinn'/'10E', 'mahamoti djinn').
card_uid('mahamoti djinn'/'10E', '10E:Mahamoti Djinn:mahamoti djinn').
card_rarity('mahamoti djinn'/'10E', 'Rare').
card_artist('mahamoti djinn'/'10E', 'Greg Staples').
card_number('mahamoti djinn'/'10E', '90').
card_flavor_text('mahamoti djinn'/'10E', 'Of royal blood among the spirits of the air, the Mahamoti djinn rides on the wings of the winds. As dangerous in the gambling hall as he is in battle, he is a master of trickery and misdirection.').
card_multiverse_id('mahamoti djinn'/'10E', '129633').

card_in_set('manabarbs', '10E').
card_original_type('manabarbs'/'10E', 'Enchantment').
card_original_text('manabarbs'/'10E', 'Whenever a player taps a land for mana, Manabarbs deals 1 damage to that player.').
card_image_name('manabarbs'/'10E', 'manabarbs').
card_uid('manabarbs'/'10E', '10E:Manabarbs:manabarbs').
card_rarity('manabarbs'/'10E', 'Rare').
card_artist('manabarbs'/'10E', 'Jeff Miracola').
card_number('manabarbs'/'10E', '218').
card_flavor_text('manabarbs'/'10E', '\"I don\'t know why people say a double-edged sword is bad. It\'s a sword. With two edges.\"\n—Kamahl, pit fighter').
card_multiverse_id('manabarbs'/'10E', '130367').

card_in_set('mantis engine', '10E').
card_original_type('mantis engine'/'10E', 'Artifact Creature — Insect').
card_original_text('mantis engine'/'10E', '{2}: Mantis Engine gains flying until end of turn. (It can\'t be blocked except by creatures with flying or reach.)\n{2}: Mantis Engine gains first strike until end of turn. (It deals combat damage before creatures without first strike.)').
card_image_name('mantis engine'/'10E', 'mantis engine').
card_uid('mantis engine'/'10E', '10E:Mantis Engine:mantis engine').
card_rarity('mantis engine'/'10E', 'Uncommon').
card_artist('mantis engine'/'10E', 'John Zeleznik').
card_number('mantis engine'/'10E', '333').
card_flavor_text('mantis engine'/'10E', '\"It\'s a clever design. You should look closely at its gearing after it swallows your head.\"\n—Jhoira, master artificer').
card_multiverse_id('mantis engine'/'10E', '136289').

card_in_set('march of the machines', '10E').
card_original_type('march of the machines'/'10E', 'Enchantment').
card_original_text('march of the machines'/'10E', 'Each noncreature artifact is an artifact creature with power and toughness each equal to its converted mana cost. (Equipment that\'s a creature can\'t equip a creature.)').
card_image_name('march of the machines'/'10E', 'march of the machines').
card_uid('march of the machines'/'10E', '10E:March of the Machines:march of the machines').
card_rarity('march of the machines'/'10E', 'Rare').
card_artist('march of the machines'/'10E', 'Ben Thompson').
card_number('march of the machines'/'10E', '91').
card_multiverse_id('march of the machines'/'10E', '106555').

card_in_set('mass of ghouls', '10E').
card_original_type('mass of ghouls'/'10E', 'Creature — Zombie Warrior').
card_original_text('mass of ghouls'/'10E', '').
card_image_name('mass of ghouls'/'10E', 'mass of ghouls').
card_uid('mass of ghouls'/'10E', '10E:Mass of Ghouls:mass of ghouls').
card_rarity('mass of ghouls'/'10E', 'Common').
card_artist('mass of ghouls'/'10E', 'Lucio Parrillo').
card_number('mass of ghouls'/'10E', '156').
card_flavor_text('mass of ghouls'/'10E', '\"An army has filled the valley, but it\'s not like any army I\'ve ever seen. There are no tents, no fires, no horses . . . just a sea of bodies, writhing and moaning, as if a pestilent village were sent to invade us.\"\n—Onean scout').
card_multiverse_id('mass of ghouls'/'10E', '135218').

card_in_set('megrim', '10E').
card_original_type('megrim'/'10E', 'Enchantment').
card_original_text('megrim'/'10E', 'Whenever an opponent discards a card, Megrim deals 2 damage to that player.').
card_image_name('megrim'/'10E', 'megrim').
card_uid('megrim'/'10E', '10E:Megrim:megrim').
card_rarity('megrim'/'10E', 'Uncommon').
card_artist('megrim'/'10E', 'Nick Percival').
card_number('megrim'/'10E', '157').
card_flavor_text('megrim'/'10E', '\"Pain is both a tool and a craft. Like any tool, it must be sharpened. Like any craft, it must be practiced.\"\n—Hooks, Cabal torturer').
card_multiverse_id('megrim'/'10E', '129640').

card_in_set('merfolk looter', '10E').
card_original_type('merfolk looter'/'10E', 'Creature — Merfolk Rogue').
card_original_text('merfolk looter'/'10E', '{T}: Draw a card, then discard a card.').
card_image_name('merfolk looter'/'10E', 'merfolk looter').
card_uid('merfolk looter'/'10E', '10E:Merfolk Looter:merfolk looter').
card_rarity('merfolk looter'/'10E', 'Common').
card_artist('merfolk looter'/'10E', 'Tristan Elwell').
card_number('merfolk looter'/'10E', '92').
card_flavor_text('merfolk looter'/'10E', 'It belongs to the merfolk now.\n—Shipwreck prayer').
card_multiverse_id('merfolk looter'/'10E', '130900').

card_in_set('midnight ritual', '10E').
card_original_type('midnight ritual'/'10E', 'Sorcery').
card_original_text('midnight ritual'/'10E', 'Remove X target creature cards in your graveyard from the game. For each creature card removed this way, put a 2/2 black Zombie creature token into play.').
card_image_name('midnight ritual'/'10E', 'midnight ritual').
card_uid('midnight ritual'/'10E', '10E:Midnight Ritual:midnight ritual').
card_rarity('midnight ritual'/'10E', 'Rare').
card_artist('midnight ritual'/'10E', 'Jeff Easley').
card_number('midnight ritual'/'10E', '158').
card_flavor_text('midnight ritual'/'10E', 'Bury old friends deeply, lest they return bearing worms.').
card_multiverse_id('midnight ritual'/'10E', '135272').

card_in_set('might of oaks', '10E').
card_original_type('might of oaks'/'10E', 'Instant').
card_original_text('might of oaks'/'10E', 'Target creature gets +7/+7 until end of turn.').
card_image_name('might of oaks'/'10E', 'might of oaks').
card_uid('might of oaks'/'10E', '10E:Might of Oaks:might of oaks').
card_rarity('might of oaks'/'10E', 'Rare').
card_artist('might of oaks'/'10E', 'Jeremy Jarvis').
card_number('might of oaks'/'10E', '277').
card_flavor_text('might of oaks'/'10E', '\"Guess where I\'m gonna plant this!\"').
card_multiverse_id('might of oaks'/'10E', '129642').

card_in_set('might weaver', '10E').
card_original_type('might weaver'/'10E', 'Creature — Human Wizard').
card_original_text('might weaver'/'10E', '{2}: Target red or white creature gains trample until end of turn. (If it would deal enough combat damage to its blockers to destroy them, you may have it deal the rest of its damage to defending player.)').
card_image_name('might weaver'/'10E', 'might weaver').
card_uid('might weaver'/'10E', '10E:Might Weaver:might weaver').
card_rarity('might weaver'/'10E', 'Uncommon').
card_artist('might weaver'/'10E', 'Larry Elmore').
card_number('might weaver'/'10E', '278').
card_flavor_text('might weaver'/'10E', '\"Let my strength harden your resolve.\"').
card_multiverse_id('might weaver'/'10E', '130996').

card_in_set('millstone', '10E').
card_original_type('millstone'/'10E', 'Artifact').
card_original_text('millstone'/'10E', '{2}, {T}: Target player puts the top two cards of his or her library into his or her graveyard.').
card_image_name('millstone'/'10E', 'millstone').
card_uid('millstone'/'10E', '10E:Millstone:millstone').
card_rarity('millstone'/'10E', 'Rare').
card_artist('millstone'/'10E', 'John Avon').
card_number('millstone'/'10E', '334').
card_flavor_text('millstone'/'10E', 'More than one mage has been driven insane by the sound of the millstone relentlessly grinding away.').
card_multiverse_id('millstone'/'10E', '129643').

card_in_set('mind bend', '10E').
card_original_type('mind bend'/'10E', 'Instant').
card_original_text('mind bend'/'10E', 'Change the text of target permanent by replacing all instances of one color word with another or one basic land type with another. (For example, you may change \"nonblack creature\" to \"nongreen creature\" or \"forestwalk\" to \"islandwalk.\" This effect doesn\'t end at end of turn.)').
card_image_name('mind bend'/'10E', 'mind bend').
card_uid('mind bend'/'10E', '10E:Mind Bend:mind bend').
card_rarity('mind bend'/'10E', 'Rare').
card_artist('mind bend'/'10E', 'Mike Dringenberg').
card_number('mind bend'/'10E', '93').
card_flavor_text('mind bend'/'10E', '\"The world, I\'ve come to realize, is a blank canvas on which to paint.\"\n—Ixidor, reality sculptor').
card_multiverse_id('mind bend'/'10E', '129644').

card_in_set('mind rot', '10E').
card_original_type('mind rot'/'10E', 'Sorcery').
card_original_text('mind rot'/'10E', 'Target player discards two cards.').
card_image_name('mind rot'/'10E', 'mind rot').
card_uid('mind rot'/'10E', '10E:Mind Rot:mind rot').
card_rarity('mind rot'/'10E', 'Common').
card_artist('mind rot'/'10E', 'Steve Luke').
card_number('mind rot'/'10E', '159').
card_flavor_text('mind rot'/'10E', '\"The beauty of mental attacks is that your victims never remember them.\"\n—Volrath').
card_multiverse_id('mind rot'/'10E', '129645').

card_in_set('mind stone', '10E').
card_original_type('mind stone'/'10E', 'Artifact').
card_original_text('mind stone'/'10E', '{T}: Add {1} to your mana pool.\n{1}, {T}, Sacrifice Mind Stone: Draw a card.').
card_image_name('mind stone'/'10E', 'mind stone').
card_uid('mind stone'/'10E', '10E:Mind Stone:mind stone').
card_rarity('mind stone'/'10E', 'Uncommon').
card_artist('mind stone'/'10E', 'Adam Rex').
card_number('mind stone'/'10E', '335').
card_flavor_text('mind stone'/'10E', '\"Not by age but by capacity is wisdom gained.\"\n—Titus Maccius Plautus, Trinummus').
card_multiverse_id('mind stone'/'10E', '135280').

card_in_set('mirri, cat warrior', '10E').
card_original_type('mirri, cat warrior'/'10E', 'Legendary Creature — Cat Warrior').
card_original_text('mirri, cat warrior'/'10E', 'First strike, forestwalk, vigilance (This creature deals combat damage before creatures without first strike, it\'s unblockable as long as defending player controls a Forest, and attacking doesn\'t cause this creature to tap.)').
card_image_name('mirri, cat warrior'/'10E', 'mirri, cat warrior').
card_uid('mirri, cat warrior'/'10E', '10E:Mirri, Cat Warrior:mirri, cat warrior').
card_rarity('mirri, cat warrior'/'10E', 'Rare').
card_artist('mirri, cat warrior'/'10E', 'Daren Bader').
card_number('mirri, cat warrior'/'10E', '279').
card_flavor_text('mirri, cat warrior'/'10E', '\"Full of beauty and grace, with a predator\'s instincts . . . Although she wanders, I have always thought Mirri belongs in Llanowar most of all.\"\n—Rofellos, Llanowar emissary').
card_multiverse_id('mirri, cat warrior'/'10E', '106405').

card_in_set('mobilization', '10E').
card_original_type('mobilization'/'10E', 'Enchantment').
card_original_text('mobilization'/'10E', 'Soldier creatures have vigilance. (Attacking doesn\'t cause them to tap.)\n{2}{W}: Put a 1/1 white Soldier creature token into play.').
card_image_name('mobilization'/'10E', 'mobilization').
card_uid('mobilization'/'10E', '10E:Mobilization:mobilization').
card_rarity('mobilization'/'10E', 'Rare').
card_artist('mobilization'/'10E', 'Carl Critchlow').
card_number('mobilization'/'10E', '29').
card_flavor_text('mobilization'/'10E', 'Wars are won with strength, valor, and numbers—especially numbers.').
card_multiverse_id('mobilization'/'10E', '129716').

card_in_set('mogg fanatic', '10E').
card_original_type('mogg fanatic'/'10E', 'Creature — Goblin').
card_original_text('mogg fanatic'/'10E', 'Sacrifice Mogg Fanatic: Mogg Fanatic deals 1 damage to target creature or player.').
card_image_name('mogg fanatic'/'10E', 'mogg fanatic').
card_uid('mogg fanatic'/'10E', '10E:Mogg Fanatic:mogg fanatic').
card_rarity('mogg fanatic'/'10E', 'Uncommon').
card_artist('mogg fanatic'/'10E', 'Brom').
card_number('mogg fanatic'/'10E', '219').
card_flavor_text('mogg fanatic'/'10E', '\"I got it! I got it! I—\"').
card_multiverse_id('mogg fanatic'/'10E', '134748').

card_in_set('molimo, maro-sorcerer', '10E').
card_original_type('molimo, maro-sorcerer'/'10E', 'Legendary Creature — Elemental').
card_original_text('molimo, maro-sorcerer'/'10E', 'Trample (If this creature would deal enough combat damage to its blockers to destroy them, you may have it deal the rest of its damage to defending player.)\nMolimo, Maro-Sorcerer\'s power and toughness are each equal to the number of lands you control.').
card_image_name('molimo, maro-sorcerer'/'10E', 'molimo, maro-sorcerer').
card_uid('molimo, maro-sorcerer'/'10E', '10E:Molimo, Maro-Sorcerer:molimo, maro-sorcerer').
card_rarity('molimo, maro-sorcerer'/'10E', 'Rare').
card_artist('molimo, maro-sorcerer'/'10E', 'Mark Zug').
card_number('molimo, maro-sorcerer'/'10E', '280').
card_flavor_text('molimo, maro-sorcerer'/'10E', '\"My mind is the spread of the canopy. My heart is the embrace of the roots. I am deathless Llanowar, its fury and its peace.\"').
card_multiverse_id('molimo, maro-sorcerer'/'10E', '106368').

card_in_set('mortal combat', '10E').
card_original_type('mortal combat'/'10E', 'Enchantment').
card_original_text('mortal combat'/'10E', 'At the beginning of your upkeep, if twenty or more creature cards are in your graveyard, you win the game.').
card_image_name('mortal combat'/'10E', 'mortal combat').
card_uid('mortal combat'/'10E', '10E:Mortal Combat:mortal combat').
card_rarity('mortal combat'/'10E', 'Rare').
card_artist('mortal combat'/'10E', 'Mike Ploog').
card_number('mortal combat'/'10E', '160').
card_flavor_text('mortal combat'/'10E', 'The crowd roared, the fighters bled, and the dead piled high in the pits. Only the Cabal could win.').
card_multiverse_id('mortal combat'/'10E', '135259').

card_in_set('mortivore', '10E').
card_original_type('mortivore'/'10E', 'Creature — Lhurgoyf').
card_original_text('mortivore'/'10E', 'Mortivore\'s power and toughness are each equal to the number of creature cards in all graveyards.\n{B}: Regenerate Mortivore. (The next time this creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_image_name('mortivore'/'10E', 'mortivore').
card_uid('mortivore'/'10E', '10E:Mortivore:mortivore').
card_rarity('mortivore'/'10E', 'Rare').
card_artist('mortivore'/'10E', 'Anthony S. Waters').
card_number('mortivore'/'10E', '161').
card_flavor_text('mortivore'/'10E', 'The light sigh of its breath whistles through its bones.').
card_multiverse_id('mortivore'/'10E', '129648').

card_in_set('mountain', '10E').
card_original_type('mountain'/'10E', 'Basic Land — Mountain').
card_original_text('mountain'/'10E', 'R').
card_image_name('mountain'/'10E', 'mountain1').
card_uid('mountain'/'10E', '10E:Mountain:mountain1').
card_rarity('mountain'/'10E', 'Basic Land').
card_artist('mountain'/'10E', 'John Avon').
card_number('mountain'/'10E', '376').
card_multiverse_id('mountain'/'10E', '129649').

card_in_set('mountain', '10E').
card_original_type('mountain'/'10E', 'Basic Land — Mountain').
card_original_text('mountain'/'10E', 'R').
card_image_name('mountain'/'10E', 'mountain2').
card_uid('mountain'/'10E', '10E:Mountain:mountain2').
card_rarity('mountain'/'10E', 'Basic Land').
card_artist('mountain'/'10E', 'Glen Angus').
card_number('mountain'/'10E', '377').
card_multiverse_id('mountain'/'10E', '129650').

card_in_set('mountain', '10E').
card_original_type('mountain'/'10E', 'Basic Land — Mountain').
card_original_text('mountain'/'10E', 'R').
card_image_name('mountain'/'10E', 'mountain3').
card_uid('mountain'/'10E', '10E:Mountain:mountain3').
card_rarity('mountain'/'10E', 'Basic Land').
card_artist('mountain'/'10E', 'John Avon').
card_number('mountain'/'10E', '378').
card_multiverse_id('mountain'/'10E', '129651').

card_in_set('mountain', '10E').
card_original_type('mountain'/'10E', 'Basic Land — Mountain').
card_original_text('mountain'/'10E', 'R').
card_image_name('mountain'/'10E', 'mountain4').
card_uid('mountain'/'10E', '10E:Mountain:mountain4').
card_rarity('mountain'/'10E', 'Basic Land').
card_artist('mountain'/'10E', 'Stephan Martiniere').
card_number('mountain'/'10E', '379').
card_multiverse_id('mountain'/'10E', '129652').

card_in_set('nantuko husk', '10E').
card_original_type('nantuko husk'/'10E', 'Creature — Zombie Insect').
card_original_text('nantuko husk'/'10E', 'Sacrifice a creature: Nantuko Husk gets +2/+2 until end of turn.').
card_image_name('nantuko husk'/'10E', 'nantuko husk').
card_uid('nantuko husk'/'10E', '10E:Nantuko Husk:nantuko husk').
card_rarity('nantuko husk'/'10E', 'Uncommon').
card_artist('nantuko husk'/'10E', 'Carl Critchlow').
card_number('nantuko husk'/'10E', '162').
card_flavor_text('nantuko husk'/'10E', 'The soul sheds light, and death is its shadow. When the light dims, life and death embrace.\n—Nantuko teaching').
card_multiverse_id('nantuko husk'/'10E', '129653').

card_in_set('natural spring', '10E').
card_original_type('natural spring'/'10E', 'Sorcery').
card_original_text('natural spring'/'10E', 'Target player gains 8 life.').
card_image_name('natural spring'/'10E', 'natural spring').
card_uid('natural spring'/'10E', '10E:Natural Spring:natural spring').
card_rarity('natural spring'/'10E', 'Common').
card_artist('natural spring'/'10E', 'Jeffrey R. Busch').
card_number('natural spring'/'10E', '281').
card_flavor_text('natural spring'/'10E', '\"Jewels cannot be eaten nor gold drunk. What civilization trades, nature simply provides.\"\n—Molimo, maro-sorcerer').
card_multiverse_id('natural spring'/'10E', '129655').

card_in_set('naturalize', '10E').
card_original_type('naturalize'/'10E', 'Instant').
card_original_text('naturalize'/'10E', 'Destroy target artifact or enchantment.').
card_image_name('naturalize'/'10E', 'naturalize').
card_uid('naturalize'/'10E', '10E:Naturalize:naturalize').
card_rarity('naturalize'/'10E', 'Common').
card_artist('naturalize'/'10E', 'Tim Hildebrandt').
card_number('naturalize'/'10E', '282').
card_flavor_text('naturalize'/'10E', '\"Well, it also makes a very nice flower pot. . . .\"\n—Arcum Dagsson, Soldevi machinist').
card_multiverse_id('naturalize'/'10E', '129656').

card_in_set('nekrataal', '10E').
card_original_type('nekrataal'/'10E', 'Creature — Human Assassin').
card_original_text('nekrataal'/'10E', 'First strike (This creature deals combat damage before creatures without first strike.)\nWhen Nekrataal comes into play, destroy target nonartifact, nonblack creature. That creature can\'t be regenerated.').
card_image_name('nekrataal'/'10E', 'nekrataal').
card_uid('nekrataal'/'10E', '10E:Nekrataal:nekrataal').
card_rarity('nekrataal'/'10E', 'Uncommon').
card_artist('nekrataal'/'10E', 'Christopher Moeller').
card_number('nekrataal'/'10E', '163').
card_flavor_text('nekrataal'/'10E', 'His victims don\'t have time to feel despair.').
card_multiverse_id('nekrataal'/'10E', '129658').

card_in_set('nightmare', '10E').
card_original_type('nightmare'/'10E', 'Creature — Nightmare').
card_original_text('nightmare'/'10E', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nNightmare\'s power and toughness are each equal to the number of Swamps you control.').
card_image_name('nightmare'/'10E', 'nightmare').
card_uid('nightmare'/'10E', '10E:Nightmare:nightmare').
card_rarity('nightmare'/'10E', 'Rare').
card_artist('nightmare'/'10E', 'Carl Critchlow').
card_number('nightmare'/'10E', '164').
card_flavor_text('nightmare'/'10E', 'The thunder of its hooves beats dreams into despair.').
card_multiverse_id('nightmare'/'10E', '129659').

card_in_set('no rest for the wicked', '10E').
card_original_type('no rest for the wicked'/'10E', 'Enchantment').
card_original_text('no rest for the wicked'/'10E', 'Sacrifice No Rest for the Wicked: Return to your hand all creature cards that were put into your graveyard from play this turn.').
card_image_name('no rest for the wicked'/'10E', 'no rest for the wicked').
card_uid('no rest for the wicked'/'10E', '10E:No Rest for the Wicked:no rest for the wicked').
card_rarity('no rest for the wicked'/'10E', 'Uncommon').
card_artist('no rest for the wicked'/'10E', 'Carl Critchlow').
card_number('no rest for the wicked'/'10E', '165').
card_flavor_text('no rest for the wicked'/'10E', '\"The soul? Here, we have no use for such frivolities.\"\n—Sitrik, birth priest').
card_multiverse_id('no rest for the wicked'/'10E', '136283').

card_in_set('nomad mythmaker', '10E').
card_original_type('nomad mythmaker'/'10E', 'Creature — Human Nomad Cleric').
card_original_text('nomad mythmaker'/'10E', '{W}, {T}: Put target Aura card in a graveyard into play attached to a creature you control. (You control that Aura.)').
card_image_name('nomad mythmaker'/'10E', 'nomad mythmaker').
card_uid('nomad mythmaker'/'10E', '10E:Nomad Mythmaker:nomad mythmaker').
card_rarity('nomad mythmaker'/'10E', 'Rare').
card_artist('nomad mythmaker'/'10E', 'Darrell Riche').
card_number('nomad mythmaker'/'10E', '30').
card_flavor_text('nomad mythmaker'/'10E', 'On the wild steppes, history vanishes in the dust. Only the mythmakers remain to say what was, and is, and will be.').
card_multiverse_id('nomad mythmaker'/'10E', '130547').

card_in_set('orcish artillery', '10E').
card_original_type('orcish artillery'/'10E', 'Creature — Orc Warrior').
card_original_text('orcish artillery'/'10E', '{T}: Orcish Artillery deals 2 damage to target creature or player and 3 damage to you.').
card_image_name('orcish artillery'/'10E', 'orcish artillery').
card_uid('orcish artillery'/'10E', '10E:Orcish Artillery:orcish artillery').
card_rarity('orcish artillery'/'10E', 'Uncommon').
card_artist('orcish artillery'/'10E', 'Jeff Miracola').
card_number('orcish artillery'/'10E', '220').
card_flavor_text('orcish artillery'/'10E', '\"So they want to kill my men? Well two can play at that game.\"\n—General Khurzog').
card_multiverse_id('orcish artillery'/'10E', '129663').

card_in_set('ornithopter', '10E').
card_original_type('ornithopter'/'10E', 'Artifact Creature — Thopter').
card_original_text('ornithopter'/'10E', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)').
card_image_name('ornithopter'/'10E', 'ornithopter').
card_uid('ornithopter'/'10E', '10E:Ornithopter:ornithopter').
card_rarity('ornithopter'/'10E', 'Uncommon').
card_artist('ornithopter'/'10E', 'Dana Knutson').
card_number('ornithopter'/'10E', '336').
card_flavor_text('ornithopter'/'10E', '\"It has been my honor to improve on the Thran\'s original design. Perhaps history will remember me in some small part for my work.\"\n—Urza, in his apprenticeship').
card_multiverse_id('ornithopter'/'10E', '129665').

card_in_set('overgrowth', '10E').
card_original_type('overgrowth'/'10E', 'Enchantment — Aura').
card_original_text('overgrowth'/'10E', 'Enchant land (Target a land as you play this. This card comes into play attached to that land.)\nWhenever enchanted land is tapped for mana, its controller adds {G}{G} to his or her mana pool.').
card_image_name('overgrowth'/'10E', 'overgrowth').
card_uid('overgrowth'/'10E', '10E:Overgrowth:overgrowth').
card_rarity('overgrowth'/'10E', 'Common').
card_artist('overgrowth'/'10E', 'Ron Spears').
card_number('overgrowth'/'10E', '283').
card_flavor_text('overgrowth'/'10E', '\"Let the forest spread! From salt, stone, and fen, let the new trees rise.\"\n—Molimo, maro-sorcerer').
card_multiverse_id('overgrowth'/'10E', '135282').

card_in_set('overrun', '10E').
card_original_type('overrun'/'10E', 'Sorcery').
card_original_text('overrun'/'10E', 'Creatures you control get +3/+3 and gain trample until end of turn. (If a creature you control would deal enough combat damage to its blockers to destroy them, you may have it deal the rest of its damage to defending player.)').
card_image_name('overrun'/'10E', 'overrun').
card_uid('overrun'/'10E', '10E:Overrun:overrun').
card_rarity('overrun'/'10E', 'Uncommon').
card_artist('overrun'/'10E', 'Carl Critchlow').
card_number('overrun'/'10E', '284').
card_flavor_text('overrun'/'10E', 'Nature doesn\'t walk.').
card_multiverse_id('overrun'/'10E', '130506').

card_in_set('pacifism', '10E').
card_original_type('pacifism'/'10E', 'Enchantment — Aura').
card_original_text('pacifism'/'10E', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nEnchanted creature can\'t attack or block.').
card_image_name('pacifism'/'10E', 'pacifism').
card_uid('pacifism'/'10E', '10E:Pacifism:pacifism').
card_rarity('pacifism'/'10E', 'Common').
card_artist('pacifism'/'10E', 'Robert Bliss').
card_number('pacifism'/'10E', '31').
card_flavor_text('pacifism'/'10E', 'For the first time in his life, Grakk felt a little warm and fuzzy inside.').
card_multiverse_id('pacifism'/'10E', '129667').

card_in_set('paladin en-vec', '10E').
card_original_type('paladin en-vec'/'10E', 'Creature — Human Knight').
card_original_text('paladin en-vec'/'10E', 'First strike, protection from black, protection from red (This creature deals combat damage before creatures without first strike. It can\'t be blocked, targeted, dealt damage, or enchanted by anything black or red.)').
card_image_name('paladin en-vec'/'10E', 'paladin en-vec').
card_uid('paladin en-vec'/'10E', '10E:Paladin en-Vec:paladin en-vec').
card_rarity('paladin en-vec'/'10E', 'Rare').
card_artist('paladin en-vec'/'10E', 'Dave Kendall').
card_number('paladin en-vec'/'10E', '32').
card_flavor_text('paladin en-vec'/'10E', '\"I do not consider myself a hero. I know only what the Vec teach: justice must always be served and corruption must always be opposed.\"').
card_multiverse_id('paladin en-vec'/'10E', '129668').

card_in_set('pariah', '10E').
card_original_type('pariah'/'10E', 'Enchantment — Aura').
card_original_text('pariah'/'10E', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nAll damage that would be dealt to you is dealt to enchanted creature instead.').
card_image_name('pariah'/'10E', 'pariah').
card_uid('pariah'/'10E', '10E:Pariah:pariah').
card_rarity('pariah'/'10E', 'Rare').
card_artist('pariah'/'10E', 'Jon J. Muth').
card_number('pariah'/'10E', '33').
card_flavor_text('pariah'/'10E', '\"Why would I consider penance for my actions when I have so many subjects willing to do it for me?\"\n—Lord Konda').
card_multiverse_id('pariah'/'10E', '135248').

card_in_set('peek', '10E').
card_original_type('peek'/'10E', 'Instant').
card_original_text('peek'/'10E', 'Look at target player\'s hand.\nDraw a card.').
card_image_name('peek'/'10E', 'peek').
card_uid('peek'/'10E', '10E:Peek:peek').
card_rarity('peek'/'10E', 'Common').
card_artist('peek'/'10E', 'Adam Rex').
card_number('peek'/'10E', '94').
card_flavor_text('peek'/'10E', 'Sometimes you have to read between the minds.').
card_multiverse_id('peek'/'10E', '130903').

card_in_set('persuasion', '10E').
card_original_type('persuasion'/'10E', 'Enchantment — Aura').
card_original_text('persuasion'/'10E', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nYou control enchanted creature.').
card_image_name('persuasion'/'10E', 'persuasion').
card_uid('persuasion'/'10E', '10E:Persuasion:persuasion').
card_rarity('persuasion'/'10E', 'Uncommon').
card_artist('persuasion'/'10E', 'William O\'Connor').
card_number('persuasion'/'10E', '95').
card_flavor_text('persuasion'/'10E', 'There\'s no telling what an accomplished mage might collect.').
card_multiverse_id('persuasion'/'10E', '129900').

card_in_set('phage the untouchable', '10E').
card_original_type('phage the untouchable'/'10E', 'Legendary Creature — Minion').
card_original_text('phage the untouchable'/'10E', 'When Phage the Untouchable comes into play, if you didn\'t play it from your hand, you lose the game. \nWhenever Phage deals combat damage to a creature, destroy that creature. It can\'t be regenerated. \nWhenever Phage deals combat damage to a player, that player loses the game.').
card_image_name('phage the untouchable'/'10E', 'phage the untouchable').
card_uid('phage the untouchable'/'10E', '10E:Phage the Untouchable:phage the untouchable').
card_rarity('phage the untouchable'/'10E', 'Rare').
card_artist('phage the untouchable'/'10E', 'Ron Spears').
card_number('phage the untouchable'/'10E', '166').
card_multiverse_id('phage the untouchable'/'10E', '106427').

card_in_set('phantom warrior', '10E').
card_original_type('phantom warrior'/'10E', 'Creature — Illusion Warrior').
card_original_text('phantom warrior'/'10E', 'Phantom Warrior is unblockable.').
card_image_name('phantom warrior'/'10E', 'phantom warrior').
card_uid('phantom warrior'/'10E', '10E:Phantom Warrior:phantom warrior').
card_rarity('phantom warrior'/'10E', 'Uncommon').
card_artist('phantom warrior'/'10E', 'Greg Staples').
card_number('phantom warrior'/'10E', '96').
card_flavor_text('phantom warrior'/'10E', '\"There are as many pillows of illusion as flakes in a snow-storm. We wake from one dream into another dream.\"\n—Ralph Waldo Emerson, \"Illusions\"').
card_multiverse_id('phantom warrior'/'10E', '132064').

card_in_set('phyrexian rager', '10E').
card_original_type('phyrexian rager'/'10E', 'Creature — Horror').
card_original_text('phyrexian rager'/'10E', 'When Phyrexian Rager comes into play, you draw a card and you lose 1 life.').
card_image_name('phyrexian rager'/'10E', 'phyrexian rager').
card_uid('phyrexian rager'/'10E', '10E:Phyrexian Rager:phyrexian rager').
card_rarity('phyrexian rager'/'10E', 'Common').
card_artist('phyrexian rager'/'10E', 'Mark Tedin').
card_number('phyrexian rager'/'10E', '167').
card_flavor_text('phyrexian rager'/'10E', 'It takes no prisoners, but it keeps the choicest bits for Phyrexia.').
card_multiverse_id('phyrexian rager'/'10E', '135189').

card_in_set('phyrexian vault', '10E').
card_original_type('phyrexian vault'/'10E', 'Artifact').
card_original_text('phyrexian vault'/'10E', '{2}, {T}, Sacrifice a creature: Draw a card.').
card_image_name('phyrexian vault'/'10E', 'phyrexian vault').
card_uid('phyrexian vault'/'10E', '10E:Phyrexian Vault:phyrexian vault').
card_rarity('phyrexian vault'/'10E', 'Uncommon').
card_artist('phyrexian vault'/'10E', 'Hannibal King').
card_number('phyrexian vault'/'10E', '337').
card_flavor_text('phyrexian vault'/'10E', '\"The secrets of Phyrexia are expensive. You will pay in brass and bone, steel and sinew.\"\n—Kaervek').
card_multiverse_id('phyrexian vault'/'10E', '135281').

card_in_set('pincher beetles', '10E').
card_original_type('pincher beetles'/'10E', 'Creature — Insect').
card_original_text('pincher beetles'/'10E', 'Shroud (This creature can\'t be the target of spells or abilities.)').
card_image_name('pincher beetles'/'10E', 'pincher beetles').
card_uid('pincher beetles'/'10E', '10E:Pincher Beetles:pincher beetles').
card_rarity('pincher beetles'/'10E', 'Common').
card_artist('pincher beetles'/'10E', 'Stephen Daniele').
card_number('pincher beetles'/'10E', '285').
card_flavor_text('pincher beetles'/'10E', '\"No fair Since when does a bug get ta munch on me?\"\n—Squee, goblin cabin hand').
card_multiverse_id('pincher beetles'/'10E', '130526').

card_in_set('pithing needle', '10E').
card_original_type('pithing needle'/'10E', 'Artifact').
card_original_text('pithing needle'/'10E', 'As Pithing Needle comes into play, name a card.\nActivated abilities of sources with the chosen name can\'t be played unless they\'re mana abilities.').
card_image_name('pithing needle'/'10E', 'pithing needle').
card_uid('pithing needle'/'10E', '10E:Pithing Needle:pithing needle').
card_rarity('pithing needle'/'10E', 'Rare').
card_artist('pithing needle'/'10E', 'Pete Venters').
card_number('pithing needle'/'10E', '338').
card_multiverse_id('pithing needle'/'10E', '129526').

card_in_set('plagiarize', '10E').
card_original_type('plagiarize'/'10E', 'Instant').
card_original_text('plagiarize'/'10E', 'Until end of turn, if target player would draw a card, instead that player skips that draw and you draw a card.').
card_image_name('plagiarize'/'10E', 'plagiarize').
card_uid('plagiarize'/'10E', '10E:Plagiarize:plagiarize').
card_rarity('plagiarize'/'10E', 'Rare').
card_artist('plagiarize'/'10E', 'Jeremy Jarvis').
card_number('plagiarize'/'10E', '97').
card_flavor_text('plagiarize'/'10E', '\"Are you thinking what I\'m thinking?\"').
card_multiverse_id('plagiarize'/'10E', '129883').

card_in_set('plague beetle', '10E').
card_original_type('plague beetle'/'10E', 'Creature — Insect').
card_original_text('plague beetle'/'10E', 'Swampwalk (This creature is unblockable as long as defending player controls a Swamp.)').
card_image_name('plague beetle'/'10E', 'plague beetle').
card_uid('plague beetle'/'10E', '10E:Plague Beetle:plague beetle').
card_rarity('plague beetle'/'10E', 'Common').
card_artist('plague beetle'/'10E', 'Tom Fleming').
card_number('plague beetle'/'10E', '168').
card_flavor_text('plague beetle'/'10E', 'No one knows whether they were named for the disease they carry or for the speed at which they multiply.').
card_multiverse_id('plague beetle'/'10E', '129678').

card_in_set('plague wind', '10E').
card_original_type('plague wind'/'10E', 'Sorcery').
card_original_text('plague wind'/'10E', 'Destroy all creatures you don\'t control. They can\'t be regenerated.').
card_image_name('plague wind'/'10E', 'plague wind').
card_uid('plague wind'/'10E', '10E:Plague Wind:plague wind').
card_rarity('plague wind'/'10E', 'Rare').
card_artist('plague wind'/'10E', 'Alan Pollack').
card_number('plague wind'/'10E', '169').
card_multiverse_id('plague wind'/'10E', '129679').

card_in_set('plains', '10E').
card_original_type('plains'/'10E', 'Basic Land — Plains').
card_original_text('plains'/'10E', 'W').
card_image_name('plains'/'10E', 'plains1').
card_uid('plains'/'10E', '10E:Plains:plains1').
card_rarity('plains'/'10E', 'Basic Land').
card_artist('plains'/'10E', 'Rob Alexander').
card_number('plains'/'10E', '364').
card_multiverse_id('plains'/'10E', '129680').

card_in_set('plains', '10E').
card_original_type('plains'/'10E', 'Basic Land — Plains').
card_original_text('plains'/'10E', 'W').
card_image_name('plains'/'10E', 'plains2').
card_uid('plains'/'10E', '10E:Plains:plains2').
card_rarity('plains'/'10E', 'Basic Land').
card_artist('plains'/'10E', 'John Avon').
card_number('plains'/'10E', '365').
card_multiverse_id('plains'/'10E', '129681').

card_in_set('plains', '10E').
card_original_type('plains'/'10E', 'Basic Land — Plains').
card_original_text('plains'/'10E', 'W').
card_image_name('plains'/'10E', 'plains3').
card_uid('plains'/'10E', '10E:Plains:plains3').
card_rarity('plains'/'10E', 'Basic Land').
card_artist('plains'/'10E', 'Greg Staples').
card_number('plains'/'10E', '366').
card_multiverse_id('plains'/'10E', '129682').

card_in_set('plains', '10E').
card_original_type('plains'/'10E', 'Basic Land — Plains').
card_original_text('plains'/'10E', 'W').
card_image_name('plains'/'10E', 'plains4').
card_uid('plains'/'10E', '10E:Plains:plains4').
card_rarity('plains'/'10E', 'Basic Land').
card_artist('plains'/'10E', 'Richard Wright').
card_number('plains'/'10E', '367').
card_multiverse_id('plains'/'10E', '129683').

card_in_set('platinum angel', '10E').
card_original_type('platinum angel'/'10E', 'Artifact Creature — Angel').
card_original_text('platinum angel'/'10E', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nYou can\'t lose the game and your opponents can\'t win the game.').
card_image_name('platinum angel'/'10E', 'platinum angel').
card_uid('platinum angel'/'10E', '10E:Platinum Angel:platinum angel').
card_rarity('platinum angel'/'10E', 'Rare').
card_artist('platinum angel'/'10E', 'Brom').
card_number('platinum angel'/'10E', '339').
card_flavor_text('platinum angel'/'10E', 'In its heart lies the secret of immortality.').
card_multiverse_id('platinum angel'/'10E', '106537').

card_in_set('primal rage', '10E').
card_original_type('primal rage'/'10E', 'Enchantment').
card_original_text('primal rage'/'10E', 'Creatures you control have trample. (If a creature you control would deal enough combat damage to its blockers to destroy them, you may have it deal the rest of its damage to defending player.)').
card_image_name('primal rage'/'10E', 'primal rage').
card_uid('primal rage'/'10E', '10E:Primal Rage:primal rage').
card_rarity('primal rage'/'10E', 'Uncommon').
card_artist('primal rage'/'10E', 'Brian Snõddy').
card_number('primal rage'/'10E', '286').
card_flavor_text('primal rage'/'10E', 'Ferocity and cunning accounted for half the victory. Gravity accounted for the rest.').
card_multiverse_id('primal rage'/'10E', '136288').

card_in_set('prodigal pyromancer', '10E').
card_original_type('prodigal pyromancer'/'10E', 'Creature — Human Wizard').
card_original_text('prodigal pyromancer'/'10E', '{T}: Prodigal Pyromancer deals 1 damage to target creature or player.').
card_image_name('prodigal pyromancer'/'10E', 'prodigal pyromancer').
card_uid('prodigal pyromancer'/'10E', '10E:Prodigal Pyromancer:prodigal pyromancer').
card_rarity('prodigal pyromancer'/'10E', 'Common').
card_artist('prodigal pyromancer'/'10E', 'Jeremy Jarvis').
card_number('prodigal pyromancer'/'10E', '221').
card_flavor_text('prodigal pyromancer'/'10E', '\"What am I looking at? Ashes, dead man.\"').
card_multiverse_id('prodigal pyromancer'/'10E', '134752').

card_in_set('puppeteer', '10E').
card_original_type('puppeteer'/'10E', 'Creature — Human Wizard').
card_original_text('puppeteer'/'10E', '{U}, {T}: You may tap or untap target creature.').
card_image_name('puppeteer'/'10E', 'puppeteer').
card_uid('puppeteer'/'10E', '10E:Puppeteer:puppeteer').
card_rarity('puppeteer'/'10E', 'Uncommon').
card_artist('puppeteer'/'10E', 'Kev Walker').
card_number('puppeteer'/'10E', '98').
card_flavor_text('puppeteer'/'10E', '\"That\'s the funny thing about free will. You don\'t feel any different without it.\"\n—Ambassador Laquatus').
card_multiverse_id('puppeteer'/'10E', '129685').

card_in_set('pyroclasm', '10E').
card_original_type('pyroclasm'/'10E', 'Sorcery').
card_original_text('pyroclasm'/'10E', 'Pyroclasm deals 2 damage to each creature.').
card_image_name('pyroclasm'/'10E', 'pyroclasm').
card_uid('pyroclasm'/'10E', '10E:Pyroclasm:pyroclasm').
card_rarity('pyroclasm'/'10E', 'Uncommon').
card_artist('pyroclasm'/'10E', 'John Avon').
card_number('pyroclasm'/'10E', '222').
card_flavor_text('pyroclasm'/'10E', '\"When the air burns, only death breathes deep.\"\n—Bogardan mage').
card_multiverse_id('pyroclasm'/'10E', '129801').

card_in_set('quicksand', '10E').
card_original_type('quicksand'/'10E', 'Land').
card_original_text('quicksand'/'10E', '{T}: Add {1} to your mana pool.\n{T}, Sacrifice Quicksand: Target attacking creature without flying gets -1/-2 until end of turn.').
card_image_name('quicksand'/'10E', 'quicksand').
card_uid('quicksand'/'10E', '10E:Quicksand:quicksand').
card_rarity('quicksand'/'10E', 'Uncommon').
card_artist('quicksand'/'10E', 'Roger Raupp').
card_number('quicksand'/'10E', '356').
card_multiverse_id('quicksand'/'10E', '129687').

card_in_set('quirion dryad', '10E').
card_original_type('quirion dryad'/'10E', 'Creature — Dryad').
card_original_text('quirion dryad'/'10E', 'Whenever you play a white, blue, black, or red spell, put a +1/+1 counter on Quirion Dryad.').
card_image_name('quirion dryad'/'10E', 'quirion dryad').
card_uid('quirion dryad'/'10E', '10E:Quirion Dryad:quirion dryad').
card_rarity('quirion dryad'/'10E', 'Rare').
card_artist('quirion dryad'/'10E', 'Todd Lockwood').
card_number('quirion dryad'/'10E', '287').
card_flavor_text('quirion dryad'/'10E', '\"Never underestimate the ability of natural forces to adapt to unnatural influences.\"\n—Molimo, maro-sorcerer').
card_multiverse_id('quirion dryad'/'10E', '130489').

card_in_set('rage weaver', '10E').
card_original_type('rage weaver'/'10E', 'Creature — Human Wizard').
card_original_text('rage weaver'/'10E', '{2}: Target black or green creature gains haste until end of turn. (It can attack and {T} this turn.)').
card_image_name('rage weaver'/'10E', 'rage weaver').
card_uid('rage weaver'/'10E', '10E:Rage Weaver:rage weaver').
card_rarity('rage weaver'/'10E', 'Uncommon').
card_artist('rage weaver'/'10E', 'John Matson').
card_number('rage weaver'/'10E', '223').
card_flavor_text('rage weaver'/'10E', '\"Let my passion spur your victory.\"').
card_multiverse_id('rage weaver'/'10E', '130997').

card_in_set('raging goblin', '10E').
card_original_type('raging goblin'/'10E', 'Creature — Goblin Berserker').
card_original_text('raging goblin'/'10E', 'Haste (This creature can attack and {T} as soon as it comes under your control.)').
card_image_name('raging goblin'/'10E', 'raging goblin').
card_uid('raging goblin'/'10E', '10E:Raging Goblin:raging goblin').
card_rarity('raging goblin'/'10E', 'Common').
card_artist('raging goblin'/'10E', 'Jeff Miracola').
card_number('raging goblin'/'10E', '224').
card_flavor_text('raging goblin'/'10E', 'He raged at the world, at his family, at his life. But mostly he just raged.').
card_multiverse_id('raging goblin'/'10E', '129688').

card_in_set('rain of tears', '10E').
card_original_type('rain of tears'/'10E', 'Sorcery').
card_original_text('rain of tears'/'10E', 'Destroy target land.').
card_image_name('rain of tears'/'10E', 'rain of tears').
card_uid('rain of tears'/'10E', '10E:Rain of Tears:rain of tears').
card_rarity('rain of tears'/'10E', 'Uncommon').
card_artist('rain of tears'/'10E', 'Eric Peterson').
card_number('rain of tears'/'10E', '170').
card_flavor_text('rain of tears'/'10E', '\"When mortals die, they have family, friends, compatriots to mourn them. When the land dies, all else dies with it, and there is no one left to weep.\"\n—Jacinth, Skyshroud ranger').
card_multiverse_id('rain of tears'/'10E', '135220').

card_in_set('rampant growth', '10E').
card_original_type('rampant growth'/'10E', 'Sorcery').
card_original_text('rampant growth'/'10E', 'Search your library for a basic land card and put that card into play tapped. Then shuffle your library.').
card_image_name('rampant growth'/'10E', 'rampant growth').
card_uid('rampant growth'/'10E', '10E:Rampant Growth:rampant growth').
card_rarity('rampant growth'/'10E', 'Common').
card_artist('rampant growth'/'10E', 'Steven Belledin').
card_number('rampant growth'/'10E', '288').
card_flavor_text('rampant growth'/'10E', 'Nature grows solutions to her problems.').
card_multiverse_id('rampant growth'/'10E', '129690').

card_in_set('ravenous rats', '10E').
card_original_type('ravenous rats'/'10E', 'Creature — Rat').
card_original_text('ravenous rats'/'10E', 'When Ravenous Rats comes into play, target opponent discards a card.').
card_image_name('ravenous rats'/'10E', 'ravenous rats').
card_uid('ravenous rats'/'10E', '10E:Ravenous Rats:ravenous rats').
card_rarity('ravenous rats'/'10E', 'Common').
card_artist('ravenous rats'/'10E', 'Carl Critchlow').
card_number('ravenous rats'/'10E', '171').
card_flavor_text('ravenous rats'/'10E', 'Nothing is sacred to rats. Everything is simply another meal.').
card_multiverse_id('ravenous rats'/'10E', '129692').

card_in_set('razormane masticore', '10E').
card_original_type('razormane masticore'/'10E', 'Artifact Creature — Masticore').
card_original_text('razormane masticore'/'10E', 'First strike (This creature deals combat damage before creatures without first strike.)\nAt the beginning of your upkeep, sacrifice Razormane Masticore unless you discard a card.\nAt the beginning of your draw step, you may have Razormane Masticore deal 3 damage to target creature.').
card_image_name('razormane masticore'/'10E', 'razormane masticore').
card_uid('razormane masticore'/'10E', '10E:Razormane Masticore:razormane masticore').
card_rarity('razormane masticore'/'10E', 'Rare').
card_artist('razormane masticore'/'10E', 'Jim Murray').
card_number('razormane masticore'/'10E', '340').
card_multiverse_id('razormane masticore'/'10E', '106552').

card_in_set('recollect', '10E').
card_original_type('recollect'/'10E', 'Sorcery').
card_original_text('recollect'/'10E', 'Return target card from your graveyard to your hand.').
card_image_name('recollect'/'10E', 'recollect').
card_uid('recollect'/'10E', '10E:Recollect:recollect').
card_rarity('recollect'/'10E', 'Uncommon').
card_artist('recollect'/'10E', 'Pete Venters').
card_number('recollect'/'10E', '289').
card_flavor_text('recollect'/'10E', '\"The bones of the past will tell their tales—if you know how to speak their language.\"\n—Savra').
card_multiverse_id('recollect'/'10E', '130507').

card_in_set('recover', '10E').
card_original_type('recover'/'10E', 'Sorcery').
card_original_text('recover'/'10E', 'Return target creature card from your graveyard to your hand.\nDraw a card.').
card_image_name('recover'/'10E', 'recover').
card_uid('recover'/'10E', '10E:Recover:recover').
card_rarity('recover'/'10E', 'Common').
card_artist('recover'/'10E', 'Nelson DeCastro').
card_number('recover'/'10E', '172').
card_flavor_text('recover'/'10E', 'As Barrin exhumed his daughter\'s body, he finally realized the full price of his faith in Urza.').
card_multiverse_id('recover'/'10E', '135188').

card_in_set('regeneration', '10E').
card_original_type('regeneration'/'10E', 'Enchantment — Aura').
card_original_text('regeneration'/'10E', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\n{G}: Regenerate enchanted creature. (The next time that creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_image_name('regeneration'/'10E', 'regeneration').
card_uid('regeneration'/'10E', '10E:Regeneration:regeneration').
card_rarity('regeneration'/'10E', 'Uncommon').
card_artist('regeneration'/'10E', 'Jeremy Jarvis').
card_number('regeneration'/'10E', '290').
card_flavor_text('regeneration'/'10E', '\"A touch of vine sap in the blood, and the flesh knits as quickly as new roots twining.\"\n—Freyalise').
card_multiverse_id('regeneration'/'10E', '129696').

card_in_set('relentless assault', '10E').
card_original_type('relentless assault'/'10E', 'Sorcery').
card_original_text('relentless assault'/'10E', 'Untap all creatures that attacked this turn. After this main phase, there is an additional combat phase followed by an additional main phase.').
card_image_name('relentless assault'/'10E', 'relentless assault').
card_uid('relentless assault'/'10E', '10E:Relentless Assault:relentless assault').
card_rarity('relentless assault'/'10E', 'Rare').
card_artist('relentless assault'/'10E', 'Greg Hildebrandt').
card_number('relentless assault'/'10E', '225').
card_flavor_text('relentless assault'/'10E', '\"Mercy? Mercy is for the playground, not the battleground.\"').
card_multiverse_id('relentless assault'/'10E', '129697').

card_in_set('relentless rats', '10E').
card_original_type('relentless rats'/'10E', 'Creature — Rat').
card_original_text('relentless rats'/'10E', 'Relentless Rats gets +1/+1 for each other creature in play named Relentless Rats.\nA deck can have any number of cards named Relentless Rats.').
card_image_name('relentless rats'/'10E', 'relentless rats').
card_uid('relentless rats'/'10E', '10E:Relentless Rats:relentless rats').
card_rarity('relentless rats'/'10E', 'Uncommon').
card_artist('relentless rats'/'10E', 'Thomas M. Baxa').
card_number('relentless rats'/'10E', '173').
card_multiverse_id('relentless rats'/'10E', '135236').

card_in_set('reminisce', '10E').
card_original_type('reminisce'/'10E', 'Sorcery').
card_original_text('reminisce'/'10E', 'Target player shuffles his or her graveyard into his or her library.').
card_image_name('reminisce'/'10E', 'reminisce').
card_uid('reminisce'/'10E', '10E:Reminisce:reminisce').
card_rarity('reminisce'/'10E', 'Uncommon').
card_artist('reminisce'/'10E', 'Ralph Horsley').
card_number('reminisce'/'10E', '99').
card_flavor_text('reminisce'/'10E', 'Leave the door to the past even slightly ajar and it could be blown off its hinges.').
card_multiverse_id('reminisce'/'10E', '129698').

card_in_set('remove soul', '10E').
card_original_type('remove soul'/'10E', 'Instant').
card_original_text('remove soul'/'10E', 'Counter target creature spell.').
card_image_name('remove soul'/'10E', 'remove soul').
card_uid('remove soul'/'10E', '10E:Remove Soul:remove soul').
card_rarity('remove soul'/'10E', 'Common').
card_artist('remove soul'/'10E', 'Adam Rex').
card_number('remove soul'/'10E', '100').
card_flavor_text('remove soul'/'10E', 'When your enemies are denied soldiers, they are denied victory.').
card_multiverse_id('remove soul'/'10E', '129699').

card_in_set('reviving dose', '10E').
card_original_type('reviving dose'/'10E', 'Instant').
card_original_text('reviving dose'/'10E', 'You gain 3 life.\nDraw a card.').
card_image_name('reviving dose'/'10E', 'reviving dose').
card_uid('reviving dose'/'10E', '10E:Reviving Dose:reviving dose').
card_rarity('reviving dose'/'10E', 'Common').
card_artist('reviving dose'/'10E', 'D. Alexander Gregory').
card_number('reviving dose'/'10E', '34').
card_flavor_text('reviving dose'/'10E', 'Samite healers never mix their pungent elixir with sweetener or tea. The threat of a second dose is enough to get most warriors back on their feet.').
card_multiverse_id('reviving dose'/'10E', '132089').

card_in_set('reya dawnbringer', '10E').
card_original_type('reya dawnbringer'/'10E', 'Legendary Creature — Angel').
card_original_text('reya dawnbringer'/'10E', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nAt the beginning of your upkeep, you may return target creature card from your graveyard to play.').
card_image_name('reya dawnbringer'/'10E', 'reya dawnbringer').
card_uid('reya dawnbringer'/'10E', '10E:Reya Dawnbringer:reya dawnbringer').
card_rarity('reya dawnbringer'/'10E', 'Rare').
card_artist('reya dawnbringer'/'10E', 'Matthew D. Wilson').
card_number('reya dawnbringer'/'10E', '35').
card_flavor_text('reya dawnbringer'/'10E', '\"You have not died until I consent.\"').
card_multiverse_id('reya dawnbringer'/'10E', '106384').

card_in_set('rhox', '10E').
card_original_type('rhox'/'10E', 'Creature — Beast').
card_original_text('rhox'/'10E', 'You may have Rhox deal its combat damage to defending player as though it weren\'t blocked.\n{2}{G}: Regenerate Rhox. (The next time this creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_image_name('rhox'/'10E', 'rhox').
card_uid('rhox'/'10E', '10E:Rhox:rhox').
card_rarity('rhox'/'10E', 'Rare').
card_artist('rhox'/'10E', 'Mark Zug').
card_number('rhox'/'10E', '291').
card_flavor_text('rhox'/'10E', 'It\'s as solid as the club in its hand—and about as subtle.').
card_multiverse_id('rhox'/'10E', '106504').

card_in_set('righteousness', '10E').
card_original_type('righteousness'/'10E', 'Instant').
card_original_text('righteousness'/'10E', 'Target blocking creature gets +7/+7 until end of turn.').
card_image_name('righteousness'/'10E', 'righteousness').
card_uid('righteousness'/'10E', '10E:Righteousness:righteousness').
card_rarity('righteousness'/'10E', 'Rare').
card_artist('righteousness'/'10E', 'Wayne England').
card_number('righteousness'/'10E', '36').
card_flavor_text('righteousness'/'10E', 'Sometimes the greatest strength is the strength of conviction.').
card_multiverse_id('righteousness'/'10E', '130552').

card_in_set('robe of mirrors', '10E').
card_original_type('robe of mirrors'/'10E', 'Enchantment — Aura').
card_original_text('robe of mirrors'/'10E', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nEnchanted creature has shroud. (It can\'t be the target of spells or abilities.)').
card_image_name('robe of mirrors'/'10E', 'robe of mirrors').
card_uid('robe of mirrors'/'10E', '10E:Robe of Mirrors:robe of mirrors').
card_rarity('robe of mirrors'/'10E', 'Common').
card_artist('robe of mirrors'/'10E', 'Christopher Moeller').
card_number('robe of mirrors'/'10E', '101').
card_flavor_text('robe of mirrors'/'10E', '\"Some say you cannot hit what you cannot see. I prefer that you hit what I want you to see.\"\n—Arcanis the Omnipotent').
card_multiverse_id('robe of mirrors'/'10E', '134764').

card_in_set('rock badger', '10E').
card_original_type('rock badger'/'10E', 'Creature — Beast').
card_original_text('rock badger'/'10E', 'Mountainwalk (This creature is unblockable as long as defending player controls a Mountain.)').
card_image_name('rock badger'/'10E', 'rock badger').
card_uid('rock badger'/'10E', '10E:Rock Badger:rock badger').
card_rarity('rock badger'/'10E', 'Common').
card_artist('rock badger'/'10E', 'Heather Hudson').
card_number('rock badger'/'10E', '226').
card_flavor_text('rock badger'/'10E', '\"As it charged our keep, I mistook it for a bear. After it tore through our ramparts and half the guard, I wished it had been a bear after all.\"\n—Ballista guard').
card_multiverse_id('rock badger'/'10E', '129715').

card_in_set('rod of ruin', '10E').
card_original_type('rod of ruin'/'10E', 'Artifact').
card_original_text('rod of ruin'/'10E', '{3}, {T}: Rod of Ruin deals 1 damage to target creature or player.').
card_image_name('rod of ruin'/'10E', 'rod of ruin').
card_uid('rod of ruin'/'10E', '10E:Rod of Ruin:rod of ruin').
card_rarity('rod of ruin'/'10E', 'Uncommon').
card_artist('rod of ruin'/'10E', 'Mark Zug').
card_number('rod of ruin'/'10E', '341').
card_flavor_text('rod of ruin'/'10E', 'The rod is a relic from ancient times . . . cruel, vicious, mean-spirited times.').
card_multiverse_id('rod of ruin'/'10E', '129704').

card_in_set('root maze', '10E').
card_original_type('root maze'/'10E', 'Enchantment').
card_original_text('root maze'/'10E', 'Artifacts and lands come into play tapped.').
card_image_name('root maze'/'10E', 'root maze').
card_uid('root maze'/'10E', '10E:Root Maze:root maze').
card_rarity('root maze'/'10E', 'Rare').
card_artist('root maze'/'10E', 'Rebecca Guay').
card_number('root maze'/'10E', '292').
card_flavor_text('root maze'/'10E', 'Pause for the briefest of moments to consider the whorl of brambles, the tangled twist of bark, and the maze has already won.').
card_multiverse_id('root maze'/'10E', '135251').

card_in_set('rootwalla', '10E').
card_original_type('rootwalla'/'10E', 'Creature — Lizard').
card_original_text('rootwalla'/'10E', '{1}{G}: Rootwalla gets +2/+2 until end of turn. Play this ability only once each turn.').
card_image_name('rootwalla'/'10E', 'rootwalla').
card_uid('rootwalla'/'10E', '10E:Rootwalla:rootwalla').
card_rarity('rootwalla'/'10E', 'Common').
card_artist('rootwalla'/'10E', 'Roger Raupp').
card_number('rootwalla'/'10E', '293').
card_flavor_text('rootwalla'/'10E', 'If you try to sneak up on a rootwalla, you\'ll suddenly find yourself dealing with twice the lizard.').
card_multiverse_id('rootwalla'/'10E', '129707').

card_in_set('rootwater commando', '10E').
card_original_type('rootwater commando'/'10E', 'Creature — Merfolk').
card_original_text('rootwater commando'/'10E', 'Islandwalk (This creature is unblockable as long as defending player controls an Island.)').
card_image_name('rootwater commando'/'10E', 'rootwater commando').
card_uid('rootwater commando'/'10E', '10E:Rootwater Commando:rootwater commando').
card_rarity('rootwater commando'/'10E', 'Common').
card_artist('rootwater commando'/'10E', 'Mark Tedin').
card_number('rootwater commando'/'10E', '102').
card_flavor_text('rootwater commando'/'10E', 'Rootwater merfolk are seldom seen these days, but elf corpses are as numerous as ever.').
card_multiverse_id('rootwater commando'/'10E', '129733').

card_in_set('rootwater matriarch', '10E').
card_original_type('rootwater matriarch'/'10E', 'Creature — Merfolk').
card_original_text('rootwater matriarch'/'10E', '{T}: Gain control of target creature as long as that creature is enchanted.').
card_image_name('rootwater matriarch'/'10E', 'rootwater matriarch').
card_uid('rootwater matriarch'/'10E', '10E:Rootwater Matriarch:rootwater matriarch').
card_rarity('rootwater matriarch'/'10E', 'Rare').
card_artist('rootwater matriarch'/'10E', 'Daren Bader').
card_number('rootwater matriarch'/'10E', '103').
card_flavor_text('rootwater matriarch'/'10E', 'In the harsh depths of Rootwater, the charms of surface dwellers are quite useless . . . except as lures.').
card_multiverse_id('rootwater matriarch'/'10E', '135252').

card_in_set('royal assassin', '10E').
card_original_type('royal assassin'/'10E', 'Creature — Human Assassin').
card_original_text('royal assassin'/'10E', '{T}: Destroy target tapped creature.').
card_image_name('royal assassin'/'10E', 'royal assassin').
card_uid('royal assassin'/'10E', '10E:Royal Assassin:royal assassin').
card_rarity('royal assassin'/'10E', 'Rare').
card_artist('royal assassin'/'10E', 'Mark Zug').
card_number('royal assassin'/'10E', '174').
card_flavor_text('royal assassin'/'10E', 'Trained in the arts of stealth, royal assassins choose their victims carefully, relying on timing and precision rather than brute force.').
card_multiverse_id('royal assassin'/'10E', '129708').

card_in_set('rule of law', '10E').
card_original_type('rule of law'/'10E', 'Enchantment').
card_original_text('rule of law'/'10E', 'Each player can\'t play more than one spell each turn.').
card_image_name('rule of law'/'10E', 'rule of law').
card_uid('rule of law'/'10E', '10E:Rule of Law:rule of law').
card_rarity('rule of law'/'10E', 'Uncommon').
card_artist('rule of law'/'10E', 'Scott M. Fischer').
card_number('rule of law'/'10E', '37').
card_flavor_text('rule of law'/'10E', 'Appointed by the kha himself, members of the tribunal ensure all disputes are settled with the utmost fairness.').
card_multiverse_id('rule of law'/'10E', '136291').

card_in_set('rushwood dryad', '10E').
card_original_type('rushwood dryad'/'10E', 'Creature — Dryad').
card_original_text('rushwood dryad'/'10E', 'Forestwalk (This creature is unblockable as long as defending player controls a Forest.)').
card_image_name('rushwood dryad'/'10E', 'rushwood dryad').
card_uid('rushwood dryad'/'10E', '10E:Rushwood Dryad:rushwood dryad').
card_rarity('rushwood dryad'/'10E', 'Common').
card_artist('rushwood dryad'/'10E', 'Todd Lockwood').
card_number('rushwood dryad'/'10E', '294').
card_flavor_text('rushwood dryad'/'10E', 'To a dryad, every tree in the forest is both a doorway and a window.').
card_multiverse_id('rushwood dryad'/'10E', '136285').

card_in_set('sage owl', '10E').
card_original_type('sage owl'/'10E', 'Creature — Bird').
card_original_text('sage owl'/'10E', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nWhen Sage Owl comes into play, look at the top four cards of your library, then put them back in any order.').
card_image_name('sage owl'/'10E', 'sage owl').
card_uid('sage owl'/'10E', '10E:Sage Owl:sage owl').
card_rarity('sage owl'/'10E', 'Common').
card_artist('sage owl'/'10E', 'Mark Brill').
card_number('sage owl'/'10E', '104').
card_multiverse_id('sage owl'/'10E', '135217').

card_in_set('samite healer', '10E').
card_original_type('samite healer'/'10E', 'Creature — Human Cleric').
card_original_text('samite healer'/'10E', '{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.').
card_image_name('samite healer'/'10E', 'samite healer').
card_uid('samite healer'/'10E', '10E:Samite Healer:samite healer').
card_rarity('samite healer'/'10E', 'Common').
card_artist('samite healer'/'10E', 'Anson Maddocks').
card_number('samite healer'/'10E', '38').
card_flavor_text('samite healer'/'10E', 'Healers ultimately acquire the divine gifts of spiritual and physical wholeness. The most devout are also granted the ability to pass physical wholeness on to others.').
card_multiverse_id('samite healer'/'10E', '132101').

card_in_set('scalpelexis', '10E').
card_original_type('scalpelexis'/'10E', 'Creature — Beast').
card_original_text('scalpelexis'/'10E', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nWhenever Scalpelexis deals combat damage to a player, that player removes the top four cards of his or her library from the game. If two or more of those cards have the same name, repeat this process.').
card_image_name('scalpelexis'/'10E', 'scalpelexis').
card_uid('scalpelexis'/'10E', '10E:Scalpelexis:scalpelexis').
card_rarity('scalpelexis'/'10E', 'Rare').
card_artist('scalpelexis'/'10E', 'Mark Tedin').
card_number('scalpelexis'/'10E', '105').
card_multiverse_id('scalpelexis'/'10E', '130818').

card_in_set('scathe zombies', '10E').
card_original_type('scathe zombies'/'10E', 'Creature — Zombie').
card_original_text('scathe zombies'/'10E', '').
card_image_name('scathe zombies'/'10E', 'scathe zombies').
card_uid('scathe zombies'/'10E', '10E:Scathe Zombies:scathe zombies').
card_rarity('scathe zombies'/'10E', 'Common').
card_artist('scathe zombies'/'10E', 'Kev Walker').
card_number('scathe zombies'/'10E', '175').
card_flavor_text('scathe zombies'/'10E', '\"They groaned, they stirred, they all uprose,\nNor spake, nor moved their eyes;\nIt had been strange, even in a dream,\nTo have seen those dead men rise.\"\n—Samuel Taylor Coleridge,\n\"The Rime of the Ancient Mariner\"').
card_multiverse_id('scathe zombies'/'10E', '129718').

card_in_set('scion of the wild', '10E').
card_original_type('scion of the wild'/'10E', 'Creature — Avatar').
card_original_text('scion of the wild'/'10E', 'Scion of the Wild\'s power and toughness are each equal to the number of creatures you control.').
card_image_name('scion of the wild'/'10E', 'scion of the wild').
card_uid('scion of the wild'/'10E', '10E:Scion of the Wild:scion of the wild').
card_rarity('scion of the wild'/'10E', 'Rare').
card_artist('scion of the wild'/'10E', 'Kev Walker').
card_number('scion of the wild'/'10E', '295').
card_flavor_text('scion of the wild'/'10E', 'It has a hundred thousand extinctions to avenge.').
card_multiverse_id('scion of the wild'/'10E', '130504').

card_in_set('scoria wurm', '10E').
card_original_type('scoria wurm'/'10E', 'Creature — Wurm').
card_original_text('scoria wurm'/'10E', 'At the beginning of your upkeep, flip a coin. If you lose the flip, return Scoria Wurm to its owner\'s hand.').
card_image_name('scoria wurm'/'10E', 'scoria wurm').
card_uid('scoria wurm'/'10E', '10E:Scoria Wurm:scoria wurm').
card_rarity('scoria wurm'/'10E', 'Rare').
card_artist('scoria wurm'/'10E', 'Steve Firchow').
card_number('scoria wurm'/'10E', '227').
card_flavor_text('scoria wurm'/'10E', 'Late at night, ululations echo from deep under Shiv, as the wurms sing of times older than humanity.').
card_multiverse_id('scoria wurm'/'10E', '130374').

card_in_set('sculpting steel', '10E').
card_original_type('sculpting steel'/'10E', 'Artifact').
card_original_text('sculpting steel'/'10E', 'As Sculpting Steel comes into play, you may choose an artifact in play. If you do, Sculpting Steel comes into play as a copy of that artifact.').
card_image_name('sculpting steel'/'10E', 'sculpting steel').
card_uid('sculpting steel'/'10E', '10E:Sculpting Steel:sculpting steel').
card_rarity('sculpting steel'/'10E', 'Rare').
card_artist('sculpting steel'/'10E', 'Heather Hudson').
card_number('sculpting steel'/'10E', '342').
card_flavor_text('sculpting steel'/'10E', 'An artificer once dropped one in a vault full of coins. She has yet to find it.').
card_multiverse_id('sculpting steel'/'10E', '135241').

card_in_set('sea monster', '10E').
card_original_type('sea monster'/'10E', 'Creature — Serpent').
card_original_text('sea monster'/'10E', 'Sea Monster can\'t attack unless defending player controls an Island.').
card_image_name('sea monster'/'10E', 'sea monster').
card_uid('sea monster'/'10E', '10E:Sea Monster:sea monster').
card_rarity('sea monster'/'10E', 'Common').
card_artist('sea monster'/'10E', 'Brian Despain').
card_number('sea monster'/'10E', '106').
card_flavor_text('sea monster'/'10E', 'It\'s easy to believe the monster is a myth—until you feel three hundred thousand pounds of myth crashing down on your ship.').
card_multiverse_id('sea monster'/'10E', '129719').

card_in_set('seedborn muse', '10E').
card_original_type('seedborn muse'/'10E', 'Creature — Spirit').
card_original_text('seedborn muse'/'10E', 'Untap all permanents you control during each other player\'s untap step.').
card_image_name('seedborn muse'/'10E', 'seedborn muse').
card_uid('seedborn muse'/'10E', '10E:Seedborn Muse:seedborn muse').
card_rarity('seedborn muse'/'10E', 'Rare').
card_artist('seedborn muse'/'10E', 'Adam Rex').
card_number('seedborn muse'/'10E', '296').
card_flavor_text('seedborn muse'/'10E', '\"Her voice is the wilderness, savage and pure.\"\n—Kamahl, druid acolyte').
card_multiverse_id('seedborn muse'/'10E', '129722').

card_in_set('seismic assault', '10E').
card_original_type('seismic assault'/'10E', 'Enchantment').
card_original_text('seismic assault'/'10E', 'Discard a land card: Seismic Assault deals 2 damage to target creature or player.').
card_image_name('seismic assault'/'10E', 'seismic assault').
card_uid('seismic assault'/'10E', '10E:Seismic Assault:seismic assault').
card_rarity('seismic assault'/'10E', 'Rare').
card_artist('seismic assault'/'10E', 'Greg Staples').
card_number('seismic assault'/'10E', '228').
card_flavor_text('seismic assault'/'10E', 'Earth arms itself with fire.').
card_multiverse_id('seismic assault'/'10E', '129884').

card_in_set('sengir vampire', '10E').
card_original_type('sengir vampire'/'10E', 'Creature — Vampire').
card_original_text('sengir vampire'/'10E', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nWhenever a creature dealt damage by Sengir Vampire this turn is put into a graveyard, put a +1/+1 counter on Sengir Vampire.').
card_image_name('sengir vampire'/'10E', 'sengir vampire').
card_uid('sengir vampire'/'10E', '10E:Sengir Vampire:sengir vampire').
card_rarity('sengir vampire'/'10E', 'Rare').
card_artist('sengir vampire'/'10E', 'Kev Walker').
card_number('sengir vampire'/'10E', '176').
card_flavor_text('sengir vampire'/'10E', 'Empires rise and fall, but evil is eternal.').
card_multiverse_id('sengir vampire'/'10E', '129724').

card_in_set('serra angel', '10E').
card_original_type('serra angel'/'10E', 'Creature — Angel').
card_original_text('serra angel'/'10E', 'Flying, vigilance (This creature can\'t be blocked except by creatures with flying or reach, and attacking doesn\'t cause this creature to tap.)').
card_image_name('serra angel'/'10E', 'serra angel').
card_uid('serra angel'/'10E', '10E:Serra Angel:serra angel').
card_rarity('serra angel'/'10E', 'Rare').
card_artist('serra angel'/'10E', 'Greg Staples').
card_number('serra angel'/'10E', '39').
card_flavor_text('serra angel'/'10E', 'Her sword sings more beautifully than any choir.').
card_multiverse_id('serra angel'/'10E', '129726').

card_in_set('serra\'s embrace', '10E').
card_original_type('serra\'s embrace'/'10E', 'Enchantment — Aura').
card_original_text('serra\'s embrace'/'10E', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nEnchanted creature gets +2/+2 and has flying and vigilance. (It can\'t be blocked except by creatures with flying or reach, and attacking doesn\'t cause it to tap.)').
card_image_name('serra\'s embrace'/'10E', 'serra\'s embrace').
card_uid('serra\'s embrace'/'10E', '10E:Serra\'s Embrace:serra\'s embrace').
card_rarity('serra\'s embrace'/'10E', 'Uncommon').
card_artist('serra\'s embrace'/'10E', 'Zoltan Boros & Gabor Szikszai').
card_number('serra\'s embrace'/'10E', '40').
card_flavor_text('serra\'s embrace'/'10E', 'The touch of Serra\'s angels bears hopes aloft and empowers noble causes.').
card_multiverse_id('serra\'s embrace'/'10E', '135214').

card_in_set('severed legion', '10E').
card_original_type('severed legion'/'10E', 'Creature — Zombie').
card_original_text('severed legion'/'10E', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)').
card_image_name('severed legion'/'10E', 'severed legion').
card_uid('severed legion'/'10E', '10E:Severed Legion:severed legion').
card_rarity('severed legion'/'10E', 'Common').
card_artist('severed legion'/'10E', 'Dany Orizio').
card_number('severed legion'/'10E', '177').
card_flavor_text('severed legion'/'10E', 'No one in Aphetto answers a knock at the door after sundown.').
card_multiverse_id('severed legion'/'10E', '129693').

card_in_set('shatterstorm', '10E').
card_original_type('shatterstorm'/'10E', 'Sorcery').
card_original_text('shatterstorm'/'10E', 'Destroy all artifacts. They can\'t be regenerated.').
card_image_name('shatterstorm'/'10E', 'shatterstorm').
card_uid('shatterstorm'/'10E', '10E:Shatterstorm:shatterstorm').
card_rarity('shatterstorm'/'10E', 'Uncommon').
card_artist('shatterstorm'/'10E', 'Paolo Parente').
card_number('shatterstorm'/'10E', '229').
card_flavor_text('shatterstorm'/'10E', 'From a marvel of magical mechanization to a smoking stack of scrap in one easy step.').
card_multiverse_id('shatterstorm'/'10E', '130370').

card_in_set('shimmering wings', '10E').
card_original_type('shimmering wings'/'10E', 'Enchantment — Aura').
card_original_text('shimmering wings'/'10E', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nEnchanted creature has flying. (It can\'t be blocked except by creatures with flying or reach.)\n{U}: Return Shimmering Wings to its owner\'s hand.').
card_image_name('shimmering wings'/'10E', 'shimmering wings').
card_uid('shimmering wings'/'10E', '10E:Shimmering Wings:shimmering wings').
card_rarity('shimmering wings'/'10E', 'Common').
card_artist('shimmering wings'/'10E', 'Carl Critchlow').
card_number('shimmering wings'/'10E', '107').
card_flavor_text('shimmering wings'/'10E', '\"I leave words like ‘impossible\' to the rabble. Whatever I imagine, I can create.\"\n—Ixidor, reality sculptor').
card_multiverse_id('shimmering wings'/'10E', '129553').

card_in_set('shivan dragon', '10E').
card_original_type('shivan dragon'/'10E', 'Creature — Dragon').
card_original_text('shivan dragon'/'10E', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\n{R}: Shivan Dragon gets +1/+0 until end of turn.').
card_image_name('shivan dragon'/'10E', 'shivan dragon').
card_uid('shivan dragon'/'10E', '10E:Shivan Dragon:shivan dragon').
card_rarity('shivan dragon'/'10E', 'Rare').
card_artist('shivan dragon'/'10E', 'Donato Giancola').
card_number('shivan dragon'/'10E', '230').
card_flavor_text('shivan dragon'/'10E', 'The undisputed master of the mountains of Shiv.').
card_multiverse_id('shivan dragon'/'10E', '129730').

card_in_set('shivan hellkite', '10E').
card_original_type('shivan hellkite'/'10E', 'Creature — Dragon').
card_original_text('shivan hellkite'/'10E', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\n{1}{R}: Shivan Hellkite deals 1 damage to target creature or player.').
card_image_name('shivan hellkite'/'10E', 'shivan hellkite').
card_uid('shivan hellkite'/'10E', '10E:Shivan Hellkite:shivan hellkite').
card_rarity('shivan hellkite'/'10E', 'Rare').
card_artist('shivan hellkite'/'10E', 'Kev Walker').
card_number('shivan hellkite'/'10E', '231').
card_flavor_text('shivan hellkite'/'10E', 'A dragon\'s scale can be carved into a mighty shield, provided you can procure a dragontooth to cut it.').
card_multiverse_id('shivan hellkite'/'10E', '130538').

card_in_set('shivan reef', '10E').
card_original_type('shivan reef'/'10E', 'Land').
card_original_text('shivan reef'/'10E', '{T}: Add {1} to your mana pool.\n{T}: Add {U} or {R} to your mana pool. Shivan Reef deals 1 damage to you.').
card_image_name('shivan reef'/'10E', 'shivan reef').
card_uid('shivan reef'/'10E', '10E:Shivan Reef:shivan reef').
card_rarity('shivan reef'/'10E', 'Rare').
card_artist('shivan reef'/'10E', 'Rob Alexander').
card_number('shivan reef'/'10E', '357').
card_multiverse_id('shivan reef'/'10E', '129731').

card_in_set('shock', '10E').
card_original_type('shock'/'10E', 'Instant').
card_original_text('shock'/'10E', 'Shock deals 2 damage to target creature or player.').
card_image_name('shock'/'10E', 'shock').
card_uid('shock'/'10E', '10E:Shock:shock').
card_rarity('shock'/'10E', 'Common').
card_artist('shock'/'10E', 'Jon Foster').
card_number('shock'/'10E', '232').
card_multiverse_id('shock'/'10E', '129732').

card_in_set('shunt', '10E').
card_original_type('shunt'/'10E', 'Instant').
card_original_text('shunt'/'10E', 'Change the target of target spell with a single target.').
card_image_name('shunt'/'10E', 'shunt').
card_uid('shunt'/'10E', '10E:Shunt:shunt').
card_rarity('shunt'/'10E', 'Rare').
card_artist('shunt'/'10E', 'Greg Hildebrandt').
card_number('shunt'/'10E', '233').
card_flavor_text('shunt'/'10E', 'Aranghil held her fury in her heart, hard and bright as a ruby. With its razor edge, she rewrote the laws of battle.').
card_multiverse_id('shunt'/'10E', '130362').

card_in_set('siege-gang commander', '10E').
card_original_type('siege-gang commander'/'10E', 'Creature — Goblin').
card_original_text('siege-gang commander'/'10E', 'When Siege-Gang Commander comes into play, put three 1/1 red Goblin creature tokens into play.\n{1}{R}, Sacrifice a Goblin: Siege-Gang Commander deals 2 damage to target creature or player.').
card_image_name('siege-gang commander'/'10E', 'siege-gang commander').
card_uid('siege-gang commander'/'10E', '10E:Siege-Gang Commander:siege-gang commander').
card_rarity('siege-gang commander'/'10E', 'Rare').
card_artist('siege-gang commander'/'10E', 'Christopher Moeller').
card_number('siege-gang commander'/'10E', '234').
card_multiverse_id('siege-gang commander'/'10E', '130539').

card_in_set('sift', '10E').
card_original_type('sift'/'10E', 'Sorcery').
card_original_text('sift'/'10E', 'Draw three cards, then discard a card.').
card_image_name('sift'/'10E', 'sift').
card_uid('sift'/'10E', '10E:Sift:sift').
card_rarity('sift'/'10E', 'Common').
card_artist('sift'/'10E', 'Jeremy Jarvis').
card_number('sift'/'10E', '108').
card_flavor_text('sift'/'10E', 'Dwell longest on the thoughts that shine brightest.').
card_multiverse_id('sift'/'10E', '136217').

card_in_set('sky weaver', '10E').
card_original_type('sky weaver'/'10E', 'Creature — Metathran Wizard').
card_original_text('sky weaver'/'10E', '{2}: Target white or black creature gains flying until end of turn. (It can\'t be blocked except by creatures with flying or reach.)').
card_image_name('sky weaver'/'10E', 'sky weaver').
card_uid('sky weaver'/'10E', '10E:Sky Weaver:sky weaver').
card_rarity('sky weaver'/'10E', 'Uncommon').
card_artist('sky weaver'/'10E', 'Christopher Moeller').
card_number('sky weaver'/'10E', '109').
card_flavor_text('sky weaver'/'10E', '\"Let my wisdom give you wings.\"').
card_multiverse_id('sky weaver'/'10E', '130998').

card_in_set('skyhunter patrol', '10E').
card_original_type('skyhunter patrol'/'10E', 'Creature — Cat Knight').
card_original_text('skyhunter patrol'/'10E', 'Flying, first strike (This creature can\'t be blocked except by creatures with flying or reach, and it deals combat damage before creatures without first strike.)').
card_image_name('skyhunter patrol'/'10E', 'skyhunter patrol').
card_uid('skyhunter patrol'/'10E', '10E:Skyhunter Patrol:skyhunter patrol').
card_rarity('skyhunter patrol'/'10E', 'Common').
card_artist('skyhunter patrol'/'10E', 'Matt Cavotta').
card_number('skyhunter patrol'/'10E', '41').
card_flavor_text('skyhunter patrol'/'10E', '\"We leonin have come to rule the plains by taking to the skies.\"\n—Raksha Golden Cub').
card_multiverse_id('skyhunter patrol'/'10E', '129735').

card_in_set('skyhunter prowler', '10E').
card_original_type('skyhunter prowler'/'10E', 'Creature — Cat Knight').
card_original_text('skyhunter prowler'/'10E', 'Flying, vigilance (This creature can\'t be blocked except by creatures with flying or reach, and attacking doesn\'t cause this creature to tap.)').
card_image_name('skyhunter prowler'/'10E', 'skyhunter prowler').
card_uid('skyhunter prowler'/'10E', '10E:Skyhunter Prowler:skyhunter prowler').
card_rarity('skyhunter prowler'/'10E', 'Common').
card_artist('skyhunter prowler'/'10E', 'Vance Kovacs').
card_number('skyhunter prowler'/'10E', '42').
card_flavor_text('skyhunter prowler'/'10E', 'As tireless as her mount, a skyhunter\'s vigil is measured in days.').
card_multiverse_id('skyhunter prowler'/'10E', '132102').

card_in_set('skyhunter skirmisher', '10E').
card_original_type('skyhunter skirmisher'/'10E', 'Creature — Cat Knight').
card_original_text('skyhunter skirmisher'/'10E', 'Flying, double strike (This creature can\'t be blocked except by creatures with flying or reach, and it deals both first-strike and regular combat damage.)').
card_image_name('skyhunter skirmisher'/'10E', 'skyhunter skirmisher').
card_uid('skyhunter skirmisher'/'10E', '10E:Skyhunter Skirmisher:skyhunter skirmisher').
card_rarity('skyhunter skirmisher'/'10E', 'Uncommon').
card_artist('skyhunter skirmisher'/'10E', 'Greg Staples').
card_number('skyhunter skirmisher'/'10E', '43').
card_flavor_text('skyhunter skirmisher'/'10E', '\"Take the deadly grace and strength of the leonin, combined with a pteron mount capable of rending a foe in two and armed with notched steel. That is a skyhunter—and why Taj-Nar has never fallen.\"\n—Raksha Golden Cub').
card_multiverse_id('skyhunter skirmisher'/'10E', '129513').

card_in_set('skyshroud ranger', '10E').
card_original_type('skyshroud ranger'/'10E', 'Creature — Elf').
card_original_text('skyshroud ranger'/'10E', '{T}: You may put a land card from your hand into play. Play this ability only any time you could play a sorcery.').
card_image_name('skyshroud ranger'/'10E', 'skyshroud ranger').
card_uid('skyshroud ranger'/'10E', '10E:Skyshroud Ranger:skyshroud ranger').
card_rarity('skyshroud ranger'/'10E', 'Common').
card_artist('skyshroud ranger'/'10E', 'Mark Tedin').
card_number('skyshroud ranger'/'10E', '297').
card_flavor_text('skyshroud ranger'/'10E', 'Not even the elves truly know the Skyshroud Forest. The rangers are merely the best equipped to handle its uncertainty.').
card_multiverse_id('skyshroud ranger'/'10E', '135182').

card_in_set('sleeper agent', '10E').
card_original_type('sleeper agent'/'10E', 'Creature — Minion').
card_original_text('sleeper agent'/'10E', 'When Sleeper Agent comes into play, target opponent gains control of it. \nAt the beginning of your upkeep, Sleeper Agent deals 2 damage to you.').
card_image_name('sleeper agent'/'10E', 'sleeper agent').
card_uid('sleeper agent'/'10E', '10E:Sleeper Agent:sleeper agent').
card_rarity('sleeper agent'/'10E', 'Rare').
card_artist('sleeper agent'/'10E', 'Randy Gallegos').
card_number('sleeper agent'/'10E', '178').
card_multiverse_id('sleeper agent'/'10E', '129803').

card_in_set('smash', '10E').
card_original_type('smash'/'10E', 'Instant').
card_original_text('smash'/'10E', 'Destroy target artifact.\nDraw a card.').
card_image_name('smash'/'10E', 'smash').
card_uid('smash'/'10E', '10E:Smash:smash').
card_rarity('smash'/'10E', 'Common').
card_artist('smash'/'10E', 'Paolo Parente').
card_number('smash'/'10E', '235').
card_flavor_text('smash'/'10E', 'Ravnica\'s laws protect not its citizens but its industry. Aging equipment is destroyed rather than restored, to bring more money into the factories\' coffers.').
card_multiverse_id('smash'/'10E', '130532').

card_in_set('snapping drake', '10E').
card_original_type('snapping drake'/'10E', 'Creature — Drake').
card_original_text('snapping drake'/'10E', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)').
card_image_name('snapping drake'/'10E', 'snapping drake').
card_uid('snapping drake'/'10E', '10E:Snapping Drake:snapping drake').
card_rarity('snapping drake'/'10E', 'Common').
card_artist('snapping drake'/'10E', 'Dave Dorman').
card_number('snapping drake'/'10E', '110').
card_flavor_text('snapping drake'/'10E', 'The irritable drakes are solitary nesters. Even brooding females barely contain the urge to attack their own young.').
card_multiverse_id('snapping drake'/'10E', '129471').

card_in_set('soul feast', '10E').
card_original_type('soul feast'/'10E', 'Sorcery').
card_original_text('soul feast'/'10E', 'Target player loses 4 life and you gain 4 life.').
card_image_name('soul feast'/'10E', 'soul feast').
card_uid('soul feast'/'10E', '10E:Soul Feast:soul feast').
card_rarity('soul feast'/'10E', 'Uncommon').
card_artist('soul feast'/'10E', 'Adam Rex').
card_number('soul feast'/'10E', '179').
card_flavor_text('soul feast'/'10E', '\"I smell the tang of a mortal\'s fresh blood. I hear it sliding wetly beneath its skin. My hunger cannot refuse the invitation.\"\n—Yuri, Sengir vampire').
card_multiverse_id('soul feast'/'10E', '129739').

card_in_set('soul warden', '10E').
card_original_type('soul warden'/'10E', 'Creature — Human Cleric').
card_original_text('soul warden'/'10E', 'Whenever another creature comes into play, you gain 1 life.').
card_image_name('soul warden'/'10E', 'soul warden').
card_uid('soul warden'/'10E', '10E:Soul Warden:soul warden').
card_rarity('soul warden'/'10E', 'Uncommon').
card_artist('soul warden'/'10E', 'Randy Gallegos').
card_number('soul warden'/'10E', '44').
card_flavor_text('soul warden'/'10E', 'Count carefully the souls and see that none are lost.\n—Vec teaching').
card_multiverse_id('soul warden'/'10E', '129740').

card_in_set('soulblast', '10E').
card_original_type('soulblast'/'10E', 'Instant').
card_original_text('soulblast'/'10E', 'As an additional cost to play Soulblast, sacrifice all creatures you control. \nSoulblast deals damage to target creature or player equal to the total power of the sacrificed creatures.').
card_image_name('soulblast'/'10E', 'soulblast').
card_uid('soulblast'/'10E', '10E:Soulblast:soulblast').
card_rarity('soulblast'/'10E', 'Rare').
card_artist('soulblast'/'10E', 'Jim Nelson').
card_number('soulblast'/'10E', '236').
card_multiverse_id('soulblast'/'10E', '130369').

card_in_set('spark elemental', '10E').
card_original_type('spark elemental'/'10E', 'Creature — Elemental').
card_original_text('spark elemental'/'10E', 'Trample, haste (If this creature would deal enough combat damage to its blockers to destroy them, you may have it deal the rest of its damage to defending player. This creature can attack and {T} as soon as it comes under your control.)\nAt end of turn, sacrifice Spark Elemental.').
card_image_name('spark elemental'/'10E', 'spark elemental').
card_uid('spark elemental'/'10E', '10E:Spark Elemental:spark elemental').
card_rarity('spark elemental'/'10E', 'Uncommon').
card_artist('spark elemental'/'10E', 'John Avon').
card_number('spark elemental'/'10E', '237').
card_flavor_text('spark elemental'/'10E', 'Vulshok shamans could never keep them alive for more than a few seconds, yet those few seconds seemed to be enough.').
card_multiverse_id('spark elemental'/'10E', '129577').

card_in_set('spawning pool', '10E').
card_original_type('spawning pool'/'10E', 'Land').
card_original_text('spawning pool'/'10E', 'Spawning Pool comes into play tapped.\n{T}: Add {B} to your mana pool.\n{1}{B}: Spawning Pool becomes a 1/1 black Skeleton creature with \"{B}: Regenerate this creature\" until end of turn. It\'s still a land. (If it regenerates, the next time it would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_image_name('spawning pool'/'10E', 'spawning pool').
card_uid('spawning pool'/'10E', '10E:Spawning Pool:spawning pool').
card_rarity('spawning pool'/'10E', 'Uncommon').
card_artist('spawning pool'/'10E', 'Nils Hamm').
card_number('spawning pool'/'10E', '358').
card_multiverse_id('spawning pool'/'10E', '106556').

card_in_set('spellbook', '10E').
card_original_type('spellbook'/'10E', 'Artifact').
card_original_text('spellbook'/'10E', 'You have no maximum hand size.').
card_image_name('spellbook'/'10E', 'spellbook').
card_uid('spellbook'/'10E', '10E:Spellbook:spellbook').
card_rarity('spellbook'/'10E', 'Uncommon').
card_artist('spellbook'/'10E', 'Andrew Goldhawk').
card_number('spellbook'/'10E', '343').
card_flavor_text('spellbook'/'10E', '\"Everything the wise woman learned she wrote in a book, and when the pages were black with ink, she took white ink and began again.\"\n—Karn, silver golem').
card_multiverse_id('spellbook'/'10E', '129741').

card_in_set('spiketail hatchling', '10E').
card_original_type('spiketail hatchling'/'10E', 'Creature — Drake').
card_original_text('spiketail hatchling'/'10E', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nSacrifice Spiketail Hatchling: Counter target spell unless its controller pays {1}.').
card_image_name('spiketail hatchling'/'10E', 'spiketail hatchling').
card_uid('spiketail hatchling'/'10E', '10E:Spiketail Hatchling:spiketail hatchling').
card_rarity('spiketail hatchling'/'10E', 'Uncommon').
card_artist('spiketail hatchling'/'10E', 'Greg Staples').
card_number('spiketail hatchling'/'10E', '111').
card_flavor_text('spiketail hatchling'/'10E', 'It dodges waves of water to prepare for waves of magic.').
card_multiverse_id('spiketail hatchling'/'10E', '129901').

card_in_set('spined wurm', '10E').
card_original_type('spined wurm'/'10E', 'Creature — Wurm').
card_original_text('spined wurm'/'10E', '').
card_image_name('spined wurm'/'10E', 'spined wurm').
card_uid('spined wurm'/'10E', '10E:Spined Wurm:spined wurm').
card_rarity('spined wurm'/'10E', 'Common').
card_artist('spined wurm'/'10E', 'Carl Critchlow').
card_number('spined wurm'/'10E', '298').
card_flavor_text('spined wurm'/'10E', 'Its spines act like the teeth of a saw, scoring distinctive marks in the vegetation of its territory.').
card_multiverse_id('spined wurm'/'10E', '129742').

card_in_set('spineless thug', '10E').
card_original_type('spineless thug'/'10E', 'Creature — Zombie Mercenary').
card_original_text('spineless thug'/'10E', 'Spineless Thug can\'t block.').
card_image_name('spineless thug'/'10E', 'spineless thug').
card_uid('spineless thug'/'10E', '10E:Spineless Thug:spineless thug').
card_rarity('spineless thug'/'10E', 'Common').
card_artist('spineless thug'/'10E', 'Matthew D. Wilson').
card_number('spineless thug'/'10E', '180').
card_flavor_text('spineless thug'/'10E', 'What it lacks in backbone, it makes up for in cruelty.').
card_multiverse_id('spineless thug'/'10E', '129743').

card_in_set('spirit link', '10E').
card_original_type('spirit link'/'10E', 'Enchantment — Aura').
card_original_text('spirit link'/'10E', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nWhenever enchanted creature deals damage, you gain that much life.').
card_image_name('spirit link'/'10E', 'spirit link').
card_uid('spirit link'/'10E', '10E:Spirit Link:spirit link').
card_rarity('spirit link'/'10E', 'Uncommon').
card_artist('spirit link'/'10E', 'Kev Walker').
card_number('spirit link'/'10E', '45').
card_flavor_text('spirit link'/'10E', '\"We are all inextricably linked, souls woven in tapestry.\"').
card_multiverse_id('spirit link'/'10E', '129744').

card_in_set('spirit weaver', '10E').
card_original_type('spirit weaver'/'10E', 'Creature — Human Wizard').
card_original_text('spirit weaver'/'10E', '{2}: Target green or blue creature gets +0/+1 until end of turn.').
card_image_name('spirit weaver'/'10E', 'spirit weaver').
card_uid('spirit weaver'/'10E', '10E:Spirit Weaver:spirit weaver').
card_rarity('spirit weaver'/'10E', 'Uncommon').
card_artist('spirit weaver'/'10E', 'Matthew D. Wilson').
card_number('spirit weaver'/'10E', '46').
card_flavor_text('spirit weaver'/'10E', '\"Let my hope be your shield.\"').
card_multiverse_id('spirit weaver'/'10E', '130999').

card_in_set('spitting earth', '10E').
card_original_type('spitting earth'/'10E', 'Sorcery').
card_original_text('spitting earth'/'10E', 'Spitting Earth deals damage equal to the number of Mountains you control to target creature.').
card_image_name('spitting earth'/'10E', 'spitting earth').
card_uid('spitting earth'/'10E', '10E:Spitting Earth:spitting earth').
card_rarity('spitting earth'/'10E', 'Common').
card_artist('spitting earth'/'10E', 'Michael Koelsch').
card_number('spitting earth'/'10E', '238').
card_flavor_text('spitting earth'/'10E', '\"Within each of you is the strength of a landslide.\"\n—Kumano, to his pupils').
card_multiverse_id('spitting earth'/'10E', '136509').

card_in_set('squee, goblin nabob', '10E').
card_original_type('squee, goblin nabob'/'10E', 'Legendary Creature — Goblin').
card_original_text('squee, goblin nabob'/'10E', 'At the beginning of your upkeep, you may return Squee, Goblin Nabob from your graveyard to your hand.').
card_image_name('squee, goblin nabob'/'10E', 'squee, goblin nabob').
card_uid('squee, goblin nabob'/'10E', '10E:Squee, Goblin Nabob:squee, goblin nabob').
card_rarity('squee, goblin nabob'/'10E', 'Rare').
card_artist('squee, goblin nabob'/'10E', 'Greg Staples').
card_number('squee, goblin nabob'/'10E', '239').
card_flavor_text('squee, goblin nabob'/'10E', '\"Some goblins are expendable. Some are impossible to get rid of. But he\'s both—at the same time!\"\n—Starke').
card_multiverse_id('squee, goblin nabob'/'10E', '106473').

card_in_set('stalking tiger', '10E').
card_original_type('stalking tiger'/'10E', 'Creature — Cat').
card_original_text('stalking tiger'/'10E', 'Stalking Tiger can\'t be blocked by more than one creature.').
card_image_name('stalking tiger'/'10E', 'stalking tiger').
card_uid('stalking tiger'/'10E', '10E:Stalking Tiger:stalking tiger').
card_rarity('stalking tiger'/'10E', 'Common').
card_artist('stalking tiger'/'10E', 'Terese Nielsen').
card_number('stalking tiger'/'10E', '299').
card_flavor_text('stalking tiger'/'10E', 'In the Jamuraan jungles, there is often no separating beauty from danger.').
card_multiverse_id('stalking tiger'/'10E', '135184').

card_in_set('stampeding wildebeests', '10E').
card_original_type('stampeding wildebeests'/'10E', 'Creature — Beast').
card_original_text('stampeding wildebeests'/'10E', 'Trample (If this creature would deal enough combat damage to its blockers to destroy them, you may have it deal the rest of its damage to defending player.)\nAt the beginning of your upkeep, return a green creature you control to its owner\'s hand.').
card_image_name('stampeding wildebeests'/'10E', 'stampeding wildebeests').
card_uid('stampeding wildebeests'/'10E', '10E:Stampeding Wildebeests:stampeding wildebeests').
card_rarity('stampeding wildebeests'/'10E', 'Uncommon').
card_artist('stampeding wildebeests'/'10E', 'Randy Gallegos').
card_number('stampeding wildebeests'/'10E', '300').
card_flavor_text('stampeding wildebeests'/'10E', 'The most violent and destructive storms in Femeref occur on cloudless days.').
card_multiverse_id('stampeding wildebeests'/'10E', '135264').

card_in_set('starlight invoker', '10E').
card_original_type('starlight invoker'/'10E', 'Creature — Human Cleric Mutant').
card_original_text('starlight invoker'/'10E', '{7}{W}: You gain 5 life.').
card_image_name('starlight invoker'/'10E', 'starlight invoker').
card_uid('starlight invoker'/'10E', '10E:Starlight Invoker:starlight invoker').
card_rarity('starlight invoker'/'10E', 'Uncommon').
card_artist('starlight invoker'/'10E', 'Glen Angus').
card_number('starlight invoker'/'10E', '47').
card_flavor_text('starlight invoker'/'10E', '\"The constellations form a tapestry of light that traces my people\'s broken history. Day and night, I feel their glittering presence calling me to weave the pattern whole.\"').
card_multiverse_id('starlight invoker'/'10E', '130385').

card_in_set('steadfast guard', '10E').
card_original_type('steadfast guard'/'10E', 'Creature — Human Rebel').
card_original_text('steadfast guard'/'10E', 'Vigilance (Attacking doesn\'t cause this creature to tap.)').
card_image_name('steadfast guard'/'10E', 'steadfast guard').
card_uid('steadfast guard'/'10E', '10E:Steadfast Guard:steadfast guard').
card_rarity('steadfast guard'/'10E', 'Common').
card_artist('steadfast guard'/'10E', 'Michael Komarck').
card_number('steadfast guard'/'10E', '48').
card_flavor_text('steadfast guard'/'10E', '\"Best leave your tongue in its yap-hole, Mercadian scum, for your silvered words and golden bribes do not sparkle so brightly outside your city.\"').
card_multiverse_id('steadfast guard'/'10E', '132111').

card_in_set('steel golem', '10E').
card_original_type('steel golem'/'10E', 'Artifact Creature — Golem').
card_original_text('steel golem'/'10E', 'You can\'t play creature spells.').
card_image_name('steel golem'/'10E', 'steel golem').
card_uid('steel golem'/'10E', '10E:Steel Golem:steel golem').
card_rarity('steel golem'/'10E', 'Uncommon').
card_artist('steel golem'/'10E', 'Donato Giancola').
card_number('steel golem'/'10E', '344').
card_flavor_text('steel golem'/'10E', '\"Once you create perfection, what else is there to make?\"\n—Arcum Dagsson, Soldevi machinist').
card_multiverse_id('steel golem'/'10E', '135235').

card_in_set('story circle', '10E').
card_original_type('story circle'/'10E', 'Enchantment').
card_original_text('story circle'/'10E', 'As Story Circle comes into play, choose a color.\n{W}: The next time a source of your choice of the chosen color would deal damage to you this turn, prevent that damage.').
card_image_name('story circle'/'10E', 'story circle').
card_uid('story circle'/'10E', '10E:Story Circle:story circle').
card_rarity('story circle'/'10E', 'Rare').
card_artist('story circle'/'10E', 'Aleksi Briclot').
card_number('story circle'/'10E', '49').
card_multiverse_id('story circle'/'10E', '129748').

card_in_set('stronghold discipline', '10E').
card_original_type('stronghold discipline'/'10E', 'Sorcery').
card_original_text('stronghold discipline'/'10E', 'Each player loses 1 life for each creature he or she controls.').
card_image_name('stronghold discipline'/'10E', 'stronghold discipline').
card_uid('stronghold discipline'/'10E', '10E:Stronghold Discipline:stronghold discipline').
card_rarity('stronghold discipline'/'10E', 'Uncommon').
card_artist('stronghold discipline'/'10E', 'Daarken').
card_number('stronghold discipline'/'10E', '181').
card_flavor_text('stronghold discipline'/'10E', 'In Urborg, the instigators of each new rebellion are soaked in the blood of the last.').
card_multiverse_id('stronghold discipline'/'10E', '135197').

card_in_set('stun', '10E').
card_original_type('stun'/'10E', 'Instant').
card_original_text('stun'/'10E', 'Target creature can\'t block this turn.\nDraw a card.').
card_image_name('stun'/'10E', 'stun').
card_uid('stun'/'10E', '10E:Stun:stun').
card_rarity('stun'/'10E', 'Common').
card_artist('stun'/'10E', 'Terese Nielsen').
card_number('stun'/'10E', '240').
card_flavor_text('stun'/'10E', '\"Fortunately, merfolk aren\'t used to opponents with feet.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('stun'/'10E', '134755').

card_in_set('sudden impact', '10E').
card_original_type('sudden impact'/'10E', 'Instant').
card_original_text('sudden impact'/'10E', 'Sudden Impact deals damage equal to the number of cards in target player\'s hand to that player.').
card_image_name('sudden impact'/'10E', 'sudden impact').
card_uid('sudden impact'/'10E', '10E:Sudden Impact:sudden impact').
card_rarity('sudden impact'/'10E', 'Uncommon').
card_artist('sudden impact'/'10E', 'Wayne Reynolds').
card_number('sudden impact'/'10E', '241').
card_flavor_text('sudden impact'/'10E', '\"Some say it\'s better to think before you act. While those people are considering all the options, that\'s usually when I kill them.\"\n—Dravus, lava mage').
card_multiverse_id('sudden impact'/'10E', '129750').

card_in_set('sulfurous springs', '10E').
card_original_type('sulfurous springs'/'10E', 'Land').
card_original_text('sulfurous springs'/'10E', '{T}: Add {1} to your mana pool.\n{T}: Add {B} or {R} to your mana pool. Sulfurous Springs deals 1 damage to you.').
card_image_name('sulfurous springs'/'10E', 'sulfurous springs').
card_uid('sulfurous springs'/'10E', '10E:Sulfurous Springs:sulfurous springs').
card_rarity('sulfurous springs'/'10E', 'Rare').
card_artist('sulfurous springs'/'10E', 'Rob Alexander').
card_number('sulfurous springs'/'10E', '359').
card_multiverse_id('sulfurous springs'/'10E', '129751').

card_in_set('sunken hope', '10E').
card_original_type('sunken hope'/'10E', 'Enchantment').
card_original_text('sunken hope'/'10E', 'At the beginning of each player\'s upkeep, that player returns a creature he or she controls to its owner\'s hand.').
card_image_name('sunken hope'/'10E', 'sunken hope').
card_uid('sunken hope'/'10E', '10E:Sunken Hope:sunken hope').
card_rarity('sunken hope'/'10E', 'Rare').
card_artist('sunken hope'/'10E', 'Volkan Baga').
card_number('sunken hope'/'10E', '112').
card_flavor_text('sunken hope'/'10E', '\"I don\'t concern myself with my enemy\'s hopes. Hopes follow the tides. They will retreat soon enough.\"\n—Ambassador Laquatus').
card_multiverse_id('sunken hope'/'10E', '129509').

card_in_set('suntail hawk', '10E').
card_original_type('suntail hawk'/'10E', 'Creature — Bird').
card_original_text('suntail hawk'/'10E', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)').
card_image_name('suntail hawk'/'10E', 'suntail hawk').
card_uid('suntail hawk'/'10E', '10E:Suntail Hawk:suntail hawk').
card_rarity('suntail hawk'/'10E', 'Common').
card_artist('suntail hawk'/'10E', 'Heather Hudson').
card_number('suntail hawk'/'10E', '50').
card_flavor_text('suntail hawk'/'10E', 'Its eye the glaring sun, its cry the keening wind.').
card_multiverse_id('suntail hawk'/'10E', '129753').

card_in_set('swamp', '10E').
card_original_type('swamp'/'10E', 'Basic Land — Swamp').
card_original_text('swamp'/'10E', 'B').
card_image_name('swamp'/'10E', 'swamp1').
card_uid('swamp'/'10E', '10E:Swamp:swamp1').
card_rarity('swamp'/'10E', 'Basic Land').
card_artist('swamp'/'10E', 'John Avon').
card_number('swamp'/'10E', '372').
card_multiverse_id('swamp'/'10E', '129754').

card_in_set('swamp', '10E').
card_original_type('swamp'/'10E', 'Basic Land — Swamp').
card_original_text('swamp'/'10E', 'B').
card_image_name('swamp'/'10E', 'swamp2').
card_uid('swamp'/'10E', '10E:Swamp:swamp2').
card_rarity('swamp'/'10E', 'Basic Land').
card_artist('swamp'/'10E', 'Ron Spencer').
card_number('swamp'/'10E', '373').
card_multiverse_id('swamp'/'10E', '129755').

card_in_set('swamp', '10E').
card_original_type('swamp'/'10E', 'Basic Land — Swamp').
card_original_text('swamp'/'10E', 'B').
card_image_name('swamp'/'10E', 'swamp3').
card_uid('swamp'/'10E', '10E:Swamp:swamp3').
card_rarity('swamp'/'10E', 'Basic Land').
card_artist('swamp'/'10E', 'Jim Nelson').
card_number('swamp'/'10E', '374').
card_multiverse_id('swamp'/'10E', '129756').

card_in_set('swamp', '10E').
card_original_type('swamp'/'10E', 'Basic Land — Swamp').
card_original_text('swamp'/'10E', 'B').
card_image_name('swamp'/'10E', 'swamp4').
card_uid('swamp'/'10E', '10E:Swamp:swamp4').
card_rarity('swamp'/'10E', 'Basic Land').
card_artist('swamp'/'10E', 'Richard Wright').
card_number('swamp'/'10E', '375').
card_multiverse_id('swamp'/'10E', '129757').

card_in_set('sylvan basilisk', '10E').
card_original_type('sylvan basilisk'/'10E', 'Creature — Basilisk').
card_original_text('sylvan basilisk'/'10E', 'Whenever Sylvan Basilisk becomes blocked by a creature, destroy that creature.').
card_image_name('sylvan basilisk'/'10E', 'sylvan basilisk').
card_uid('sylvan basilisk'/'10E', '10E:Sylvan Basilisk:sylvan basilisk').
card_rarity('sylvan basilisk'/'10E', 'Uncommon').
card_artist('sylvan basilisk'/'10E', 'Steven Belledin').
card_number('sylvan basilisk'/'10E', '301').
card_flavor_text('sylvan basilisk'/'10E', '\"We\'re not sure what one looks like, so to be safe, nobody look at anything until we\'re out of here.\"\n—Parbold Drix, veteran explorer').
card_multiverse_id('sylvan basilisk'/'10E', '135215').

card_in_set('sylvan scrying', '10E').
card_original_type('sylvan scrying'/'10E', 'Sorcery').
card_original_text('sylvan scrying'/'10E', 'Search your library for a land card, reveal it, and put it into your hand. Then shuffle your library.').
card_image_name('sylvan scrying'/'10E', 'sylvan scrying').
card_uid('sylvan scrying'/'10E', '10E:Sylvan Scrying:sylvan scrying').
card_rarity('sylvan scrying'/'10E', 'Uncommon').
card_artist('sylvan scrying'/'10E', 'Scott M. Fischer').
card_number('sylvan scrying'/'10E', '302').
card_flavor_text('sylvan scrying'/'10E', 'One glimpse of an elf\'s home lasts her weeks away in the wild.').
card_multiverse_id('sylvan scrying'/'10E', '130513').

card_in_set('tangle spider', '10E').
card_original_type('tangle spider'/'10E', 'Creature — Spider').
card_original_text('tangle spider'/'10E', 'Flash (You may play this spell any time you could play an instant.)\nReach (This creature can block creatures with flying.)').
card_image_name('tangle spider'/'10E', 'tangle spider').
card_uid('tangle spider'/'10E', '10E:Tangle Spider:tangle spider').
card_rarity('tangle spider'/'10E', 'Uncommon').
card_artist('tangle spider'/'10E', 'Terese Nielsen').
card_number('tangle spider'/'10E', '303').
card_flavor_text('tangle spider'/'10E', 'Only rumors have ever escaped its web.').
card_multiverse_id('tangle spider'/'10E', '135231').

card_in_set('telepathy', '10E').
card_original_type('telepathy'/'10E', 'Enchantment').
card_original_text('telepathy'/'10E', 'Your opponents play with their hands revealed.').
card_image_name('telepathy'/'10E', 'telepathy').
card_uid('telepathy'/'10E', '10E:Telepathy:telepathy').
card_rarity('telepathy'/'10E', 'Uncommon').
card_artist('telepathy'/'10E', 'Matthew D. Wilson').
card_number('telepathy'/'10E', '113').
card_flavor_text('telepathy'/'10E', '\"The question isn\'t whether I can read minds. It\'s whether I have yet to find a mind worth reading.\"\n—Ambassador Laquatus').
card_multiverse_id('telepathy'/'10E', '129761').

card_in_set('telling time', '10E').
card_original_type('telling time'/'10E', 'Instant').
card_original_text('telling time'/'10E', 'Look at the top three cards of your library. Put one of those cards into your hand, one on top of your library, and one on the bottom of your library.').
card_image_name('telling time'/'10E', 'telling time').
card_uid('telling time'/'10E', '10E:Telling Time:telling time').
card_rarity('telling time'/'10E', 'Uncommon').
card_artist('telling time'/'10E', 'Scott M. Fischer').
card_number('telling time'/'10E', '114').
card_flavor_text('telling time'/'10E', 'Mastery is achieved when \"telling time\" becomes \"telling time what to do.\"').
card_multiverse_id('telling time'/'10E', '132071').

card_in_set('tempest of light', '10E').
card_original_type('tempest of light'/'10E', 'Instant').
card_original_text('tempest of light'/'10E', 'Destroy all enchantments.').
card_image_name('tempest of light'/'10E', 'tempest of light').
card_uid('tempest of light'/'10E', '10E:Tempest of Light:tempest of light').
card_rarity('tempest of light'/'10E', 'Uncommon').
card_artist('tempest of light'/'10E', 'Wayne England').
card_number('tempest of light'/'10E', '51').
card_flavor_text('tempest of light'/'10E', '\"Let everything return to its true nature, so that destiny may takes its course.\"').
card_multiverse_id('tempest of light'/'10E', '132131').

card_in_set('terramorphic expanse', '10E').
card_original_type('terramorphic expanse'/'10E', 'Land').
card_original_text('terramorphic expanse'/'10E', '{T}, Sacrifice Terramorphic Expanse: Search your library for a basic land card and put it into play tapped. Then shuffle your library.').
card_image_name('terramorphic expanse'/'10E', 'terramorphic expanse').
card_uid('terramorphic expanse'/'10E', '10E:Terramorphic Expanse:terramorphic expanse').
card_rarity('terramorphic expanse'/'10E', 'Common').
card_artist('terramorphic expanse'/'10E', 'Dan Scott').
card_number('terramorphic expanse'/'10E', '360').
card_flavor_text('terramorphic expanse'/'10E', 'Take two steps north into the unsettled future, south into the unquiet past, east into the present day, or west into the great unknown.').
card_multiverse_id('terramorphic expanse'/'10E', '129881').

card_in_set('terror', '10E').
card_original_type('terror'/'10E', 'Instant').
card_original_text('terror'/'10E', 'Destroy target nonartifact, nonblack creature. It can\'t be regenerated.').
card_image_name('terror'/'10E', 'terror').
card_uid('terror'/'10E', '10E:Terror:terror').
card_rarity('terror'/'10E', 'Common').
card_artist('terror'/'10E', 'Adam Rex').
card_number('terror'/'10E', '182').
card_multiverse_id('terror'/'10E', '135199').

card_in_set('the hive', '10E').
card_original_type('the hive'/'10E', 'Artifact').
card_original_text('the hive'/'10E', '{5}, {T}: Put a 1/1 Insect artifact creature token with flying named Wasp into play. (It can\'t be blocked except by creatures with flying or reach.)').
card_image_name('the hive'/'10E', 'the hive').
card_uid('the hive'/'10E', '10E:The Hive:the hive').
card_rarity('the hive'/'10E', 'Rare').
card_artist('the hive'/'10E', 'Ron Spencer').
card_number('the hive'/'10E', '324').
card_flavor_text('the hive'/'10E', '\"Their buzzing makes my bones hum from a mile away.\"\n—Galenti, treasure hunter').
card_multiverse_id('the hive'/'10E', '135253').

card_in_set('thieving magpie', '10E').
card_original_type('thieving magpie'/'10E', 'Creature — Bird').
card_original_text('thieving magpie'/'10E', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.) \nWhenever Thieving Magpie deals damage to an opponent, you draw a card.').
card_image_name('thieving magpie'/'10E', 'thieving magpie').
card_uid('thieving magpie'/'10E', '10E:Thieving Magpie:thieving magpie').
card_rarity('thieving magpie'/'10E', 'Uncommon').
card_artist('thieving magpie'/'10E', 'Una Fricker').
card_number('thieving magpie'/'10E', '115').
card_flavor_text('thieving magpie'/'10E', 'Other birds collect twigs for their nests. Magpies collect jewels for theirs.').
card_multiverse_id('thieving magpie'/'10E', '129764').

card_in_set('threaten', '10E').
card_original_type('threaten'/'10E', 'Sorcery').
card_original_text('threaten'/'10E', 'Untap target creature and gain control of it until end of turn. That creature gains haste until end of turn. (It can attack and {T} this turn.)').
card_image_name('threaten'/'10E', 'threaten').
card_uid('threaten'/'10E', '10E:Threaten:threaten').
card_rarity('threaten'/'10E', 'Uncommon').
card_artist('threaten'/'10E', 'Pete Venters').
card_number('threaten'/'10E', '242').
card_flavor_text('threaten'/'10E', 'Goblins\' motivational techniques are crude, but effective.').
card_multiverse_id('threaten'/'10E', '129767').

card_in_set('thrull surgeon', '10E').
card_original_type('thrull surgeon'/'10E', 'Creature — Thrull').
card_original_text('thrull surgeon'/'10E', '{1}{B}, Sacrifice Thrull Surgeon: Look at target player\'s hand and choose a card from it. That player discards that card. Play this ability only any time you could play a sorcery.').
card_image_name('thrull surgeon'/'10E', 'thrull surgeon').
card_uid('thrull surgeon'/'10E', '10E:Thrull Surgeon:thrull surgeon').
card_rarity('thrull surgeon'/'10E', 'Uncommon').
card_artist('thrull surgeon'/'10E', 'rk post').
card_number('thrull surgeon'/'10E', '183').
card_flavor_text('thrull surgeon'/'10E', '\"Just take a little off the top.\"').
card_multiverse_id('thrull surgeon'/'10E', '136282').

card_in_set('thundering giant', '10E').
card_original_type('thundering giant'/'10E', 'Creature — Giant').
card_original_text('thundering giant'/'10E', 'Haste (This creature can attack and {T} as soon as it comes under your control.)').
card_image_name('thundering giant'/'10E', 'thundering giant').
card_uid('thundering giant'/'10E', '10E:Thundering Giant:thundering giant').
card_rarity('thundering giant'/'10E', 'Uncommon').
card_artist('thundering giant'/'10E', 'Mark Zug').
card_number('thundering giant'/'10E', '243').
card_flavor_text('thundering giant'/'10E', 'The giant was felt a few seconds before he was seen.').
card_multiverse_id('thundering giant'/'10E', '130381').

card_in_set('tidings', '10E').
card_original_type('tidings'/'10E', 'Sorcery').
card_original_text('tidings'/'10E', 'Draw four cards.').
card_image_name('tidings'/'10E', 'tidings').
card_uid('tidings'/'10E', '10E:Tidings:tidings').
card_rarity('tidings'/'10E', 'Uncommon').
card_artist('tidings'/'10E', 'Pete Venters').
card_number('tidings'/'10E', '116').
card_flavor_text('tidings'/'10E', '\"Though the letter was many pages long, I could tell all I needed to know from one look at the messenger\'s face.\"\n—Barrin, master wizard').
card_multiverse_id('tidings'/'10E', '129770').

card_in_set('time stop', '10E').
card_original_type('time stop'/'10E', 'Instant').
card_original_text('time stop'/'10E', 'End the turn. (Remove all spells and abilities on the stack from the game, including this card. The player whose turn it is discards down to his or her maximum hand size. Damage wears off, and \"this turn\" and \"until end of turn\" effects end.)').
card_image_name('time stop'/'10E', 'time stop').
card_uid('time stop'/'10E', '10E:Time Stop:time stop').
card_rarity('time stop'/'10E', 'Rare').
card_artist('time stop'/'10E', 'Scott M. Fischer').
card_number('time stop'/'10E', '117').
card_multiverse_id('time stop'/'10E', '129898').

card_in_set('time stretch', '10E').
card_original_type('time stretch'/'10E', 'Sorcery').
card_original_text('time stretch'/'10E', 'Target player takes two extra turns after this one.').
card_image_name('time stretch'/'10E', 'time stretch').
card_uid('time stretch'/'10E', '10E:Time Stretch:time stretch').
card_rarity('time stretch'/'10E', 'Rare').
card_artist('time stretch'/'10E', 'Matt Cavotta').
card_number('time stretch'/'10E', '118').
card_flavor_text('time stretch'/'10E', 'There\'s lots of time like the present.').
card_multiverse_id('time stretch'/'10E', '129677').

card_in_set('traumatize', '10E').
card_original_type('traumatize'/'10E', 'Sorcery').
card_original_text('traumatize'/'10E', 'Target player puts the top half of his or her library, rounded down, into his or her graveyard.').
card_image_name('traumatize'/'10E', 'traumatize').
card_uid('traumatize'/'10E', '10E:Traumatize:traumatize').
card_rarity('traumatize'/'10E', 'Rare').
card_artist('traumatize'/'10E', 'Greg Staples').
card_number('traumatize'/'10E', '119').
card_flavor_text('traumatize'/'10E', '\"The educated mind is heavy with lore and knowledge. It\'s also the most likely to collapse under its own weight.\"\n—Ambassador Laquatus').
card_multiverse_id('traumatize'/'10E', '129774').

card_in_set('treasure hunter', '10E').
card_original_type('treasure hunter'/'10E', 'Creature — Human').
card_original_text('treasure hunter'/'10E', 'When Treasure Hunter comes into play, you may return target artifact card from your graveyard to your hand.').
card_image_name('treasure hunter'/'10E', 'treasure hunter').
card_uid('treasure hunter'/'10E', '10E:Treasure Hunter:treasure hunter').
card_rarity('treasure hunter'/'10E', 'Uncommon').
card_artist('treasure hunter'/'10E', 'Adam Rex').
card_number('treasure hunter'/'10E', '52').
card_flavor_text('treasure hunter'/'10E', '\"The treasures of the ancients belong in museums, not in the grubby hands of grave robbers.\"').
card_multiverse_id('treasure hunter'/'10E', '135232').

card_in_set('treetop bracers', '10E').
card_original_type('treetop bracers'/'10E', 'Enchantment — Aura').
card_original_text('treetop bracers'/'10E', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nEnchanted creature gets +1/+1 and can\'t be blocked except by creatures with flying.').
card_image_name('treetop bracers'/'10E', 'treetop bracers').
card_uid('treetop bracers'/'10E', '10E:Treetop Bracers:treetop bracers').
card_rarity('treetop bracers'/'10E', 'Common').
card_artist('treetop bracers'/'10E', 'Heather Hudson').
card_number('treetop bracers'/'10E', '304').
card_flavor_text('treetop bracers'/'10E', 'Gravity is truly what you make of it.').
card_multiverse_id('treetop bracers'/'10E', '129777').

card_in_set('treetop village', '10E').
card_original_type('treetop village'/'10E', 'Land').
card_original_text('treetop village'/'10E', 'Treetop Village comes into play tapped.\n{T}: Add {G} to your mana pool.\n{1}{G}: Treetop Village becomes a 3/3 green Ape creature with trample until end of turn. It\'s still a land. (If it would deal enough combat damage to its blockers to destroy them, you may have it deal the rest of its damage to defending player.)').
card_image_name('treetop village'/'10E', 'treetop village').
card_uid('treetop village'/'10E', '10E:Treetop Village:treetop village').
card_rarity('treetop village'/'10E', 'Uncommon').
card_artist('treetop village'/'10E', 'Rob Alexander').
card_number('treetop village'/'10E', '361').
card_multiverse_id('treetop village'/'10E', '106455').

card_in_set('troll ascetic', '10E').
card_original_type('troll ascetic'/'10E', 'Creature — Troll Shaman').
card_original_text('troll ascetic'/'10E', 'Troll Ascetic can\'t be the target of spells or abilities your opponents control.\n{1}{G}: Regenerate Troll Ascetic. (The next time this creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_image_name('troll ascetic'/'10E', 'troll ascetic').
card_uid('troll ascetic'/'10E', '10E:Troll Ascetic:troll ascetic').
card_rarity('troll ascetic'/'10E', 'Rare').
card_artist('troll ascetic'/'10E', 'Puddnhead').
card_number('troll ascetic'/'10E', '305').
card_flavor_text('troll ascetic'/'10E', 'It\'s no coincidence that the oldest trolls are also the angriest.').
card_multiverse_id('troll ascetic'/'10E', '130498').

card_in_set('true believer', '10E').
card_original_type('true believer'/'10E', 'Creature — Human Cleric').
card_original_text('true believer'/'10E', 'You have shroud. (You can\'t be the target of spells or abilities.)').
card_image_name('true believer'/'10E', 'true believer').
card_uid('true believer'/'10E', '10E:True Believer:true believer').
card_rarity('true believer'/'10E', 'Rare').
card_artist('true believer'/'10E', 'Alex Horley-Orlandelli').
card_number('true believer'/'10E', '53').
card_flavor_text('true believer'/'10E', 'So great is his certainty that mere facts cannot shake it.').
card_multiverse_id('true believer'/'10E', '129610').

card_in_set('tundra wolves', '10E').
card_original_type('tundra wolves'/'10E', 'Creature — Wolf').
card_original_text('tundra wolves'/'10E', 'First strike (This creature deals combat damage before creatures without first strike.)').
card_image_name('tundra wolves'/'10E', 'tundra wolves').
card_uid('tundra wolves'/'10E', '10E:Tundra Wolves:tundra wolves').
card_rarity('tundra wolves'/'10E', 'Common').
card_artist('tundra wolves'/'10E', 'Richard Sardinha').
card_number('tundra wolves'/'10E', '54').
card_flavor_text('tundra wolves'/'10E', '\"I heard their eerie howling, the wolves calling their kindred across the frozen plains.\"\n—Onean scout').
card_multiverse_id('tundra wolves'/'10E', '129604').

card_in_set('twincast', '10E').
card_original_type('twincast'/'10E', 'Instant').
card_original_text('twincast'/'10E', 'Copy target instant or sorcery spell. You may choose new targets for the copy.').
card_image_name('twincast'/'10E', 'twincast').
card_uid('twincast'/'10E', '10E:Twincast:twincast').
card_rarity('twincast'/'10E', 'Rare').
card_artist('twincast'/'10E', 'Christopher Moeller').
card_number('twincast'/'10E', '120').
card_flavor_text('twincast'/'10E', 'Imitation is the most dangerous form of flattery.').
card_multiverse_id('twincast'/'10E', '129887').

card_in_set('twitch', '10E').
card_original_type('twitch'/'10E', 'Instant').
card_original_text('twitch'/'10E', 'You may tap or untap target artifact, creature, or land.\nDraw a card.').
card_image_name('twitch'/'10E', 'twitch').
card_uid('twitch'/'10E', '10E:Twitch:twitch').
card_rarity('twitch'/'10E', 'Common').
card_artist('twitch'/'10E', 'DiTerlizzi').
card_number('twitch'/'10E', '121').
card_flavor_text('twitch'/'10E', 'Battles are won in nuance.').
card_multiverse_id('twitch'/'10E', '136286').

card_in_set('uncontrollable anger', '10E').
card_original_type('uncontrollable anger'/'10E', 'Enchantment — Aura').
card_original_text('uncontrollable anger'/'10E', 'Flash (You may play this spell any time you could play an instant.)\nEnchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nEnchanted creature gets +2/+2 and attacks each turn if able.').
card_image_name('uncontrollable anger'/'10E', 'uncontrollable anger').
card_uid('uncontrollable anger'/'10E', '10E:Uncontrollable Anger:uncontrollable anger').
card_rarity('uncontrollable anger'/'10E', 'Common').
card_artist('uncontrollable anger'/'10E', 'Kev Walker').
card_number('uncontrollable anger'/'10E', '244').
card_flavor_text('uncontrollable anger'/'10E', 'A barbarian\'s heart pounds with the fury of a prisoner, threatening at any moment to break the bars of its cage.').
card_multiverse_id('uncontrollable anger'/'10E', '134756').

card_in_set('underground river', '10E').
card_original_type('underground river'/'10E', 'Land').
card_original_text('underground river'/'10E', '{T}: Add {1} to your mana pool.\n{T}: Add {U} or {B} to your mana pool. Underground River deals 1 damage to you.').
card_image_name('underground river'/'10E', 'underground river').
card_uid('underground river'/'10E', '10E:Underground River:underground river').
card_rarity('underground river'/'10E', 'Rare').
card_artist('underground river'/'10E', 'Andrew Goldhawk').
card_number('underground river'/'10E', '362').
card_multiverse_id('underground river'/'10E', '129778').

card_in_set('underworld dreams', '10E').
card_original_type('underworld dreams'/'10E', 'Enchantment').
card_original_text('underworld dreams'/'10E', 'Whenever an opponent draws a card, Underworld Dreams deals 1 damage to him or her.').
card_image_name('underworld dreams'/'10E', 'underworld dreams').
card_uid('underworld dreams'/'10E', '10E:Underworld Dreams:underworld dreams').
card_rarity('underworld dreams'/'10E', 'Rare').
card_artist('underworld dreams'/'10E', 'Carl Critchlow').
card_number('underworld dreams'/'10E', '184').
card_flavor_text('underworld dreams'/'10E', '\"In the drowsy dark cave of the mind, dreams build their nest with fragments dropped from day\'s caravan.\"\n—Rabindranath Tagore').
card_multiverse_id('underworld dreams'/'10E', '129779').

card_in_set('unholy strength', '10E').
card_original_type('unholy strength'/'10E', 'Enchantment — Aura').
card_original_text('unholy strength'/'10E', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nEnchanted creature gets +2/+1.').
card_image_name('unholy strength'/'10E', 'unholy strength').
card_uid('unholy strength'/'10E', '10E:Unholy Strength:unholy strength').
card_rarity('unholy strength'/'10E', 'Common').
card_artist('unholy strength'/'10E', 'Terese Nielsen').
card_number('unholy strength'/'10E', '185').
card_flavor_text('unholy strength'/'10E', 'Such power grows the body as it shrinks the soul.').
card_multiverse_id('unholy strength'/'10E', '129780').

card_in_set('unsummon', '10E').
card_original_type('unsummon'/'10E', 'Instant').
card_original_text('unsummon'/'10E', 'Return target creature to its owner\'s hand.').
card_image_name('unsummon'/'10E', 'unsummon').
card_uid('unsummon'/'10E', '10E:Unsummon:unsummon').
card_rarity('unsummon'/'10E', 'Common').
card_artist('unsummon'/'10E', 'Ron Spencer').
card_number('unsummon'/'10E', '122').
card_flavor_text('unsummon'/'10E', 'Tolarian apprentices learn to liberate Ætheric energy long before they are taught to bind it.').
card_multiverse_id('unsummon'/'10E', '136218').

card_in_set('upwelling', '10E').
card_original_type('upwelling'/'10E', 'Enchantment').
card_original_text('upwelling'/'10E', 'Mana pools don\'t empty at the end of phases or turns. (This effect stops mana burn.)').
card_image_name('upwelling'/'10E', 'upwelling').
card_uid('upwelling'/'10E', '10E:Upwelling:upwelling').
card_rarity('upwelling'/'10E', 'Rare').
card_artist('upwelling'/'10E', 'Chippy').
card_number('upwelling'/'10E', '306').
card_flavor_text('upwelling'/'10E', 'Freyalise promised a dawn that would never end, a day of limitless life. As many dread her promise as revere it.').
card_multiverse_id('upwelling'/'10E', '130494').

card_in_set('vampire bats', '10E').
card_original_type('vampire bats'/'10E', 'Creature — Bat').
card_original_text('vampire bats'/'10E', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\n{B}: Vampire Bats gets +1/+0 until end of turn. Play this ability no more than twice each turn.').
card_image_name('vampire bats'/'10E', 'vampire bats').
card_uid('vampire bats'/'10E', '10E:Vampire Bats:vampire bats').
card_rarity('vampire bats'/'10E', 'Common').
card_artist('vampire bats'/'10E', 'Chippy').
card_number('vampire bats'/'10E', '186').
card_multiverse_id('vampire bats'/'10E', '135195').

card_in_set('vedalken mastermind', '10E').
card_original_type('vedalken mastermind'/'10E', 'Creature — Vedalken Wizard').
card_original_text('vedalken mastermind'/'10E', '{U}, {T}: Return target permanent you control to its owner\'s hand.').
card_image_name('vedalken mastermind'/'10E', 'vedalken mastermind').
card_uid('vedalken mastermind'/'10E', '10E:Vedalken Mastermind:vedalken mastermind').
card_rarity('vedalken mastermind'/'10E', 'Uncommon').
card_artist('vedalken mastermind'/'10E', 'Darrell Riche').
card_number('vedalken mastermind'/'10E', '123').
card_flavor_text('vedalken mastermind'/'10E', 'The vedalken see other beings from a cold distance, as either experimental subjects or objects of study.').
card_multiverse_id('vedalken mastermind'/'10E', '130834').

card_in_set('venerable monk', '10E').
card_original_type('venerable monk'/'10E', 'Creature — Human Monk Cleric').
card_original_text('venerable monk'/'10E', 'When Venerable Monk comes into play, you gain 2 life.').
card_image_name('venerable monk'/'10E', 'venerable monk').
card_uid('venerable monk'/'10E', '10E:Venerable Monk:venerable monk').
card_rarity('venerable monk'/'10E', 'Common').
card_artist('venerable monk'/'10E', 'D. Alexander Gregory').
card_number('venerable monk'/'10E', '55').
card_flavor_text('venerable monk'/'10E', 'Age wears the flesh but galvanizes the soul.').
card_multiverse_id('venerable monk'/'10E', '129786').

card_in_set('verdant force', '10E').
card_original_type('verdant force'/'10E', 'Creature — Elemental').
card_original_text('verdant force'/'10E', 'At the beginning of each upkeep, put a 1/1 green Saproling creature token into play under your control.').
card_image_name('verdant force'/'10E', 'verdant force').
card_uid('verdant force'/'10E', '10E:Verdant Force:verdant force').
card_rarity('verdant force'/'10E', 'Rare').
card_artist('verdant force'/'10E', 'DiTerlizzi').
card_number('verdant force'/'10E', '307').
card_flavor_text('verdant force'/'10E', 'Left to itself, nature overflows any container, overthrows any restriction, and overreaches any boundary.').
card_multiverse_id('verdant force'/'10E', '129788').

card_in_set('viashino runner', '10E').
card_original_type('viashino runner'/'10E', 'Creature — Viashino').
card_original_text('viashino runner'/'10E', 'Viashino Runner can\'t be blocked except by two or more creatures.').
card_image_name('viashino runner'/'10E', 'viashino runner').
card_uid('viashino runner'/'10E', '10E:Viashino Runner:viashino runner').
card_rarity('viashino runner'/'10E', 'Common').
card_artist('viashino runner'/'10E', 'Steve White').
card_number('viashino runner'/'10E', '245').
card_flavor_text('viashino runner'/'10E', '\"It moved this way, an\' that way, an\' then before I could stick it, it jumped over my head an\' was gone.\"\n—Jula, goblin raider').
card_multiverse_id('viashino runner'/'10E', '130531').

card_in_set('viashino sandscout', '10E').
card_original_type('viashino sandscout'/'10E', 'Creature — Viashino Scout').
card_original_text('viashino sandscout'/'10E', 'Haste (This creature can attack and {T} as soon as it comes under your control.)\nAt end of turn, return Viashino Sandscout to its owner\'s hand. (Return it only if it\'s in play.)').
card_image_name('viashino sandscout'/'10E', 'viashino sandscout').
card_uid('viashino sandscout'/'10E', '10E:Viashino Sandscout:viashino sandscout').
card_rarity('viashino sandscout'/'10E', 'Common').
card_artist('viashino sandscout'/'10E', 'Scott M. Fischer').
card_number('viashino sandscout'/'10E', '246').
card_flavor_text('viashino sandscout'/'10E', 'A viashino\'s overlapping scales form a buffer against Shiv\'s ash-flecked winds and a slippery target for goblin pikes.').
card_multiverse_id('viashino sandscout'/'10E', '130387').

card_in_set('viridian shaman', '10E').
card_original_type('viridian shaman'/'10E', 'Creature — Elf Shaman').
card_original_text('viridian shaman'/'10E', 'When Viridian Shaman comes into play, destroy target artifact.').
card_image_name('viridian shaman'/'10E', 'viridian shaman').
card_uid('viridian shaman'/'10E', '10E:Viridian Shaman:viridian shaman').
card_rarity('viridian shaman'/'10E', 'Uncommon').
card_artist('viridian shaman'/'10E', 'Scott M. Fischer').
card_number('viridian shaman'/'10E', '308').
card_flavor_text('viridian shaman'/'10E', 'She stands as a living symbol of the natural world and an enemy of the forces that threaten it.').
card_multiverse_id('viridian shaman'/'10E', '129792').

card_in_set('voice of all', '10E').
card_original_type('voice of all'/'10E', 'Creature — Angel').
card_original_text('voice of all'/'10E', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nAs Voice of All comes into play, choose a color. \nVoice of All has protection from the chosen color. (It can\'t be blocked, targeted, dealt damage, or enchanted by anything of the chosen color.)').
card_image_name('voice of all'/'10E', 'voice of all').
card_uid('voice of all'/'10E', '10E:Voice of All:voice of all').
card_rarity('voice of all'/'10E', 'Rare').
card_artist('voice of all'/'10E', 'rk post').
card_number('voice of all'/'10E', '56').
card_multiverse_id('voice of all'/'10E', '136290').

card_in_set('wall of air', '10E').
card_original_type('wall of air'/'10E', 'Creature — Wall').
card_original_text('wall of air'/'10E', 'Defender, flying (This creature can\'t attack, and it can block creatures with flying.)').
card_image_name('wall of air'/'10E', 'wall of air').
card_uid('wall of air'/'10E', '10E:Wall of Air:wall of air').
card_rarity('wall of air'/'10E', 'Uncommon').
card_artist('wall of air'/'10E', 'John Avon').
card_number('wall of air'/'10E', '124').
card_flavor_text('wall of air'/'10E', 'When no falcons fly, beware the sky.\n—Femeref aphorism').
card_multiverse_id('wall of air'/'10E', '135274').

card_in_set('wall of fire', '10E').
card_original_type('wall of fire'/'10E', 'Creature — Wall').
card_original_text('wall of fire'/'10E', 'Defender (This creature can\'t attack.)\n{R}: Wall of Fire gets +1/+0 until end of turn.').
card_image_name('wall of fire'/'10E', 'wall of fire').
card_uid('wall of fire'/'10E', '10E:Wall of Fire:wall of fire').
card_rarity('wall of fire'/'10E', 'Uncommon').
card_artist('wall of fire'/'10E', 'Dan Dos Santos').
card_number('wall of fire'/'10E', '247').
card_flavor_text('wall of fire'/'10E', 'The erupting flames work as both barrier and weapon.').
card_multiverse_id('wall of fire'/'10E', '136284').

card_in_set('wall of swords', '10E').
card_original_type('wall of swords'/'10E', 'Creature — Wall').
card_original_text('wall of swords'/'10E', 'Defender, flying (This creature can\'t attack, and it can block creatures with flying.)').
card_image_name('wall of swords'/'10E', 'wall of swords').
card_uid('wall of swords'/'10E', '10E:Wall of Swords:wall of swords').
card_rarity('wall of swords'/'10E', 'Uncommon').
card_artist('wall of swords'/'10E', 'Zoltan Boros & Gabor Szikszai').
card_number('wall of swords'/'10E', '57').
card_flavor_text('wall of swords'/'10E', 'The air hummed with the scissoring sound of uncounted blades that hovered in front of the invaders as though wielded by a phalanx of unseen hands.').
card_multiverse_id('wall of swords'/'10E', '132120').

card_in_set('wall of wood', '10E').
card_original_type('wall of wood'/'10E', 'Creature — Wall').
card_original_text('wall of wood'/'10E', 'Defender (This creature can\'t attack.)').
card_image_name('wall of wood'/'10E', 'wall of wood').
card_uid('wall of wood'/'10E', '10E:Wall of Wood:wall of wood').
card_rarity('wall of wood'/'10E', 'Common').
card_artist('wall of wood'/'10E', 'Rebecca Guay').
card_number('wall of wood'/'10E', '309').
card_flavor_text('wall of wood'/'10E', 'Orcish lumberjacks spent the night sharpening their blades and resting for a day of labor. They awoke to find that the forest had been making its own preparations.').
card_multiverse_id('wall of wood'/'10E', '129773').

card_in_set('warp world', '10E').
card_original_type('warp world'/'10E', 'Sorcery').
card_original_text('warp world'/'10E', 'Each player shuffles all permanents he or she owns into his or her library, then reveals that many cards from the top of his or her library. Each player puts all artifact, creature, and land cards revealed this way into play, then puts all enchantment cards revealed this way into play, then puts all cards revealed this way that weren\'t put into play on the bottom of his or her library in any order.').
card_image_name('warp world'/'10E', 'warp world').
card_uid('warp world'/'10E', '10E:Warp World:warp world').
card_rarity('warp world'/'10E', 'Rare').
card_artist('warp world'/'10E', 'Ron Spencer').
card_number('warp world'/'10E', '248').
card_multiverse_id('warp world'/'10E', '130364').

card_in_set('warrior\'s honor', '10E').
card_original_type('warrior\'s honor'/'10E', 'Instant').
card_original_text('warrior\'s honor'/'10E', 'Creatures you control get +1/+1 until end of turn.').
card_image_name('warrior\'s honor'/'10E', 'warrior\'s honor').
card_uid('warrior\'s honor'/'10E', '10E:Warrior\'s Honor:warrior\'s honor').
card_rarity('warrior\'s honor'/'10E', 'Common').
card_artist('warrior\'s honor'/'10E', 'D. Alexander Gregory').
card_number('warrior\'s honor'/'10E', '58').
card_flavor_text('warrior\'s honor'/'10E', '\"The day will come when the righteous warrior faces a battle she cannot win. She will greet that day as she has any other.\"\n—Asmira, holy avenger').
card_multiverse_id('warrior\'s honor'/'10E', '129797').

card_in_set('whispersilk cloak', '10E').
card_original_type('whispersilk cloak'/'10E', 'Artifact — Equipment').
card_original_text('whispersilk cloak'/'10E', 'Equipped creature is unblockable and has shroud. (It can\'t be the target of spells or abilities.)\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_image_name('whispersilk cloak'/'10E', 'whispersilk cloak').
card_uid('whispersilk cloak'/'10E', '10E:Whispersilk Cloak:whispersilk cloak').
card_rarity('whispersilk cloak'/'10E', 'Uncommon').
card_artist('whispersilk cloak'/'10E', 'Luca Zontini').
card_number('whispersilk cloak'/'10E', '345').
card_flavor_text('whispersilk cloak'/'10E', 'Such cloaks are in high demand both by assassins and by those who fear them.').
card_multiverse_id('whispersilk cloak'/'10E', '135278').

card_in_set('wild griffin', '10E').
card_original_type('wild griffin'/'10E', 'Creature — Griffin').
card_original_text('wild griffin'/'10E', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)').
card_image_name('wild griffin'/'10E', 'wild griffin').
card_uid('wild griffin'/'10E', '10E:Wild Griffin:wild griffin').
card_rarity('wild griffin'/'10E', 'Common').
card_artist('wild griffin'/'10E', 'Matt Cavotta').
card_number('wild griffin'/'10E', '59').
card_flavor_text('wild griffin'/'10E', '\"I abandoned my dream of a squadron of griffin-riders when the cost proved too high. Three trainers were eaten for every griffin broken to the bridle.\"\n—King Darien of Kjeldor').
card_multiverse_id('wild griffin'/'10E', '129557').

card_in_set('windborn muse', '10E').
card_original_type('windborn muse'/'10E', 'Creature — Spirit').
card_original_text('windborn muse'/'10E', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nCreatures can\'t attack you unless their controller pays {2} for each creature he or she controls that\'s attacking you.').
card_image_name('windborn muse'/'10E', 'windborn muse').
card_uid('windborn muse'/'10E', '10E:Windborn Muse:windborn muse').
card_rarity('windborn muse'/'10E', 'Rare').
card_artist('windborn muse'/'10E', 'Adam Rex').
card_number('windborn muse'/'10E', '60').
card_flavor_text('windborn muse'/'10E', '\"Her voice is justice, clear and relentless.\"\n—Akroma, angelic avenger').
card_multiverse_id('windborn muse'/'10E', '130549').

card_in_set('wrath of god', '10E').
card_original_type('wrath of god'/'10E', 'Sorcery').
card_original_text('wrath of god'/'10E', 'Destroy all creatures. They can\'t be regenerated.').
card_image_name('wrath of god'/'10E', 'wrath of god').
card_uid('wrath of god'/'10E', '10E:Wrath of God:wrath of god').
card_rarity('wrath of god'/'10E', 'Rare').
card_artist('wrath of god'/'10E', 'Kev Walker').
card_number('wrath of god'/'10E', '61').
card_multiverse_id('wrath of god'/'10E', '129808').

card_in_set('wurm\'s tooth', '10E').
card_original_type('wurm\'s tooth'/'10E', 'Artifact').
card_original_text('wurm\'s tooth'/'10E', 'Whenever a player plays a green spell, you may gain 1 life.').
card_image_name('wurm\'s tooth'/'10E', 'wurm\'s tooth').
card_uid('wurm\'s tooth'/'10E', '10E:Wurm\'s Tooth:wurm\'s tooth').
card_rarity('wurm\'s tooth'/'10E', 'Uncommon').
card_artist('wurm\'s tooth'/'10E', 'Alan Pollack').
card_number('wurm\'s tooth'/'10E', '346').
card_flavor_text('wurm\'s tooth'/'10E', 'A wurm knows nothing of deception. If it opens its mouth, it plans to eat you.').
card_multiverse_id('wurm\'s tooth'/'10E', '129809').

card_in_set('yavimaya coast', '10E').
card_original_type('yavimaya coast'/'10E', 'Land').
card_original_text('yavimaya coast'/'10E', '{T}: Add {1} to your mana pool.\n{T}: Add {G} or {U} to your mana pool. Yavimaya Coast deals 1 damage to you.').
card_image_name('yavimaya coast'/'10E', 'yavimaya coast').
card_uid('yavimaya coast'/'10E', '10E:Yavimaya Coast:yavimaya coast').
card_rarity('yavimaya coast'/'10E', 'Rare').
card_artist('yavimaya coast'/'10E', 'Anthony S. Waters').
card_number('yavimaya coast'/'10E', '363').
card_multiverse_id('yavimaya coast'/'10E', '129810').

card_in_set('yavimaya enchantress', '10E').
card_original_type('yavimaya enchantress'/'10E', 'Creature — Human Druid').
card_original_text('yavimaya enchantress'/'10E', 'Yavimaya Enchantress gets +1/+1 for each enchantment in play.').
card_image_name('yavimaya enchantress'/'10E', 'yavimaya enchantress').
card_uid('yavimaya enchantress'/'10E', '10E:Yavimaya Enchantress:yavimaya enchantress').
card_rarity('yavimaya enchantress'/'10E', 'Uncommon').
card_artist('yavimaya enchantress'/'10E', 'Terese Nielsen').
card_number('yavimaya enchantress'/'10E', '310').
card_flavor_text('yavimaya enchantress'/'10E', 'Her roots connect her to the forest\'s wishes.').
card_multiverse_id('yavimaya enchantress'/'10E', '130515').

card_in_set('youthful knight', '10E').
card_original_type('youthful knight'/'10E', 'Creature — Human Knight').
card_original_text('youthful knight'/'10E', 'First strike (This creature deals combat damage before creatures without first strike.)').
card_image_name('youthful knight'/'10E', 'youthful knight').
card_uid('youthful knight'/'10E', '10E:Youthful Knight:youthful knight').
card_rarity('youthful knight'/'10E', 'Common').
card_artist('youthful knight'/'10E', 'Rebecca Guay').
card_number('youthful knight'/'10E', '62').
card_flavor_text('youthful knight'/'10E', 'Idealism fits him better than his armor.').
card_multiverse_id('youthful knight'/'10E', '129790').
