% Scars of Mirrodin

set('SOM').
set_name('SOM', 'Scars of Mirrodin').
set_release_date('SOM', '2010-10-01').
set_border('SOM', 'black').
set_type('SOM', 'expansion').
set_block('SOM', 'Scars of Mirrodin').

card_in_set('abuna acolyte', 'SOM').
card_original_type('abuna acolyte'/'SOM', 'Creature — Cat Cleric').
card_original_text('abuna acolyte'/'SOM', '{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.\n{T}: Prevent the next 2 damage that would be dealt to target artifact creature this turn.').
card_first_print('abuna acolyte', 'SOM').
card_image_name('abuna acolyte'/'SOM', 'abuna acolyte').
card_uid('abuna acolyte'/'SOM', 'SOM:Abuna Acolyte:abuna acolyte').
card_rarity('abuna acolyte'/'SOM', 'Uncommon').
card_artist('abuna acolyte'/'SOM', 'Igor Kieryluk').
card_number('abuna acolyte'/'SOM', '1').
card_flavor_text('abuna acolyte'/'SOM', '\"You can break nothing I cannot mend.\"').
card_multiverse_id('abuna acolyte'/'SOM', '194371').
card_watermark('abuna acolyte'/'SOM', 'Mirran').

card_in_set('accorder\'s shield', 'SOM').
card_original_type('accorder\'s shield'/'SOM', 'Artifact — Equipment').
card_original_text('accorder\'s shield'/'SOM', 'Equipped creature gets +0/+3 and has vigilance.\nEquip {3}').
card_first_print('accorder\'s shield', 'SOM').
card_image_name('accorder\'s shield'/'SOM', 'accorder\'s shield').
card_uid('accorder\'s shield'/'SOM', 'SOM:Accorder\'s Shield:accorder\'s shield').
card_rarity('accorder\'s shield'/'SOM', 'Common').
card_artist('accorder\'s shield'/'SOM', 'Alan Pollack').
card_number('accorder\'s shield'/'SOM', '136').
card_flavor_text('accorder\'s shield'/'SOM', 'An Auriok shield is polished to a mirror finish even on the inside, enabling its bearer to watch foes ahead and behind.').
card_multiverse_id('accorder\'s shield'/'SOM', '209046').
card_watermark('accorder\'s shield'/'SOM', 'Mirran').

card_in_set('acid web spider', 'SOM').
card_original_type('acid web spider'/'SOM', 'Creature — Spider').
card_original_text('acid web spider'/'SOM', 'Reach\nWhen Acid Web Spider enters the battlefield, you may destroy target Equipment.').
card_first_print('acid web spider', 'SOM').
card_image_name('acid web spider'/'SOM', 'acid web spider').
card_uid('acid web spider'/'SOM', 'SOM:Acid Web Spider:acid web spider').
card_rarity('acid web spider'/'SOM', 'Uncommon').
card_artist('acid web spider'/'SOM', 'Austin Hsu').
card_number('acid web spider'/'SOM', '108').
card_flavor_text('acid web spider'/'SOM', 'Its web is an excellent source of found copper for the daring or the stupid.').
card_multiverse_id('acid web spider'/'SOM', '209290').
card_watermark('acid web spider'/'SOM', 'Mirran').

card_in_set('alpha tyrranax', 'SOM').
card_original_type('alpha tyrranax'/'SOM', 'Creature — Beast').
card_original_text('alpha tyrranax'/'SOM', '').
card_first_print('alpha tyrranax', 'SOM').
card_image_name('alpha tyrranax'/'SOM', 'alpha tyrranax').
card_uid('alpha tyrranax'/'SOM', 'SOM:Alpha Tyrranax:alpha tyrranax').
card_rarity('alpha tyrranax'/'SOM', 'Common').
card_artist('alpha tyrranax'/'SOM', 'Dave Kendall').
card_number('alpha tyrranax'/'SOM', '109').
card_flavor_text('alpha tyrranax'/'SOM', 'Hunger seized the tyrranax, and the Sylvok\'s vision quest ended in disaster.').
card_multiverse_id('alpha tyrranax'/'SOM', '202651').
card_watermark('alpha tyrranax'/'SOM', 'Mirran').

card_in_set('arc trail', 'SOM').
card_original_type('arc trail'/'SOM', 'Sorcery').
card_original_text('arc trail'/'SOM', 'Arc Trail deals 2 damage to target creature or player and 1 damage to another target creature or player.').
card_first_print('arc trail', 'SOM').
card_image_name('arc trail'/'SOM', 'arc trail').
card_uid('arc trail'/'SOM', 'SOM:Arc Trail:arc trail').
card_rarity('arc trail'/'SOM', 'Uncommon').
card_artist('arc trail'/'SOM', 'Marc Simonetti').
card_number('arc trail'/'SOM', '81').
card_flavor_text('arc trail'/'SOM', '\"Don\'t try to hit your enemies. Concentrate on the space between them, and fill the air with doom.\"\n—Spear-Tribe teaching').
card_multiverse_id('arc trail'/'SOM', '206343').
card_watermark('arc trail'/'SOM', 'Mirran').

card_in_set('argent sphinx', 'SOM').
card_original_type('argent sphinx'/'SOM', 'Creature — Sphinx').
card_original_text('argent sphinx'/'SOM', 'Flying\nMetalcraft — {U}: Exile Argent Sphinx. Return it to the battlefield under your control at the beginning of the next end step. Activate this ability only if you control three or more artifacts.').
card_first_print('argent sphinx', 'SOM').
card_image_name('argent sphinx'/'SOM', 'argent sphinx').
card_uid('argent sphinx'/'SOM', 'SOM:Argent Sphinx:argent sphinx').
card_rarity('argent sphinx'/'SOM', 'Rare').
card_artist('argent sphinx'/'SOM', 'Chris Rahn').
card_number('argent sphinx'/'SOM', '28').
card_flavor_text('argent sphinx'/'SOM', 'A great mirage, a dream of wings and silver.').
card_multiverse_id('argent sphinx'/'SOM', '215085').
card_watermark('argent sphinx'/'SOM', 'Mirran').

card_in_set('argentum armor', 'SOM').
card_original_type('argentum armor'/'SOM', 'Artifact — Equipment').
card_original_text('argentum armor'/'SOM', 'Equipped creature gets +6/+6.\nWhenever equipped creature attacks, destroy target permanent.\nEquip {6}').
card_first_print('argentum armor', 'SOM').
card_image_name('argentum armor'/'SOM', 'argentum armor').
card_uid('argentum armor'/'SOM', 'SOM:Argentum Armor:argentum armor').
card_rarity('argentum armor'/'SOM', 'Rare').
card_artist('argentum armor'/'SOM', 'Matt Cavotta').
card_number('argentum armor'/'SOM', '137').
card_flavor_text('argentum armor'/'SOM', 'Mirrodin\'s creator still lives, still shapes metal, and still commands world-shaking power.').
card_multiverse_id('argentum armor'/'SOM', '217967').
card_watermark('argentum armor'/'SOM', 'Mirran').

card_in_set('arrest', 'SOM').
card_original_type('arrest'/'SOM', 'Enchantment — Aura').
card_original_text('arrest'/'SOM', 'Enchant creature\nEnchanted creature can\'t attack or block, and its activated abilities can\'t be activated.').
card_image_name('arrest'/'SOM', 'arrest').
card_uid('arrest'/'SOM', 'SOM:Arrest:arrest').
card_rarity('arrest'/'SOM', 'Common').
card_artist('arrest'/'SOM', 'Daarken').
card_number('arrest'/'SOM', '2').
card_flavor_text('arrest'/'SOM', 'Bladehold houses a rogues\' gallery filled with the living \"statues\" of the worst enemies of the Auriok.').
card_multiverse_id('arrest'/'SOM', '209007').
card_watermark('arrest'/'SOM', 'Mirran').

card_in_set('asceticism', 'SOM').
card_original_type('asceticism'/'SOM', 'Enchantment').
card_original_text('asceticism'/'SOM', 'Creatures you control can\'t be the targets of spells or abilities your opponents control.\n{1}{G}: Regenerate target creature.').
card_first_print('asceticism', 'SOM').
card_image_name('asceticism'/'SOM', 'asceticism').
card_uid('asceticism'/'SOM', 'SOM:Asceticism:asceticism').
card_rarity('asceticism'/'SOM', 'Rare').
card_artist('asceticism'/'SOM', 'Daarken').
card_number('asceticism'/'SOM', '110').
card_flavor_text('asceticism'/'SOM', '\"Let my ignominy build walls thicker than iron and stronger than darksteel.\"\n—Thrun, the last troll').
card_multiverse_id('asceticism'/'SOM', '215082').
card_watermark('asceticism'/'SOM', 'Mirran').

card_in_set('assault strobe', 'SOM').
card_original_type('assault strobe'/'SOM', 'Sorcery').
card_original_text('assault strobe'/'SOM', 'Target creature gains double strike until end of turn. (It deals both first-strike and regular combat damage.)').
card_first_print('assault strobe', 'SOM').
card_image_name('assault strobe'/'SOM', 'assault strobe').
card_uid('assault strobe'/'SOM', 'SOM:Assault Strobe:assault strobe').
card_rarity('assault strobe'/'SOM', 'Common').
card_artist('assault strobe'/'SOM', 'Kev Walker').
card_number('assault strobe'/'SOM', '82').
card_flavor_text('assault strobe'/'SOM', 'When breaking someone\'s face once just isn\'t enough.').
card_multiverse_id('assault strobe'/'SOM', '194119').
card_watermark('assault strobe'/'SOM', 'Mirran').

card_in_set('auriok edgewright', 'SOM').
card_original_type('auriok edgewright'/'SOM', 'Creature — Human Soldier').
card_original_text('auriok edgewright'/'SOM', 'Metalcraft — Auriok Edgewright has double strike as long as you control three or more artifacts.').
card_first_print('auriok edgewright', 'SOM').
card_image_name('auriok edgewright'/'SOM', 'auriok edgewright').
card_uid('auriok edgewright'/'SOM', 'SOM:Auriok Edgewright:auriok edgewright').
card_rarity('auriok edgewright'/'SOM', 'Uncommon').
card_artist('auriok edgewright'/'SOM', 'Mike Bierek').
card_number('auriok edgewright'/'SOM', '3').
card_flavor_text('auriok edgewright'/'SOM', 'Auriok soldiers craft their own weapons, forging a connection to the steel with each blow of the hammer.').
card_multiverse_id('auriok edgewright'/'SOM', '208260').
card_watermark('auriok edgewright'/'SOM', 'Mirran').

card_in_set('auriok replica', 'SOM').
card_original_type('auriok replica'/'SOM', 'Artifact Creature — Cleric').
card_original_text('auriok replica'/'SOM', '{W}, Sacrifice Auriok Replica: Prevent all damage a source of your choice would deal to you this turn.').
card_first_print('auriok replica', 'SOM').
card_image_name('auriok replica'/'SOM', 'auriok replica').
card_uid('auriok replica'/'SOM', 'SOM:Auriok Replica:auriok replica').
card_rarity('auriok replica'/'SOM', 'Common').
card_artist('auriok replica'/'SOM', 'Zoltan Boros & Gabor Szikszai').
card_number('auriok replica'/'SOM', '138').
card_flavor_text('auriok replica'/'SOM', 'All the loyalty of the Auriok with only a trace of their self-righteousness.').
card_multiverse_id('auriok replica'/'SOM', '209051').
card_watermark('auriok replica'/'SOM', 'Mirran').

card_in_set('auriok sunchaser', 'SOM').
card_original_type('auriok sunchaser'/'SOM', 'Creature — Human Soldier').
card_original_text('auriok sunchaser'/'SOM', 'Metalcraft — As long as you control three or more artifacts, Auriok Sunchaser gets +2/+2 and has flying.').
card_first_print('auriok sunchaser', 'SOM').
card_image_name('auriok sunchaser'/'SOM', 'auriok sunchaser').
card_uid('auriok sunchaser'/'SOM', 'SOM:Auriok Sunchaser:auriok sunchaser').
card_rarity('auriok sunchaser'/'SOM', 'Common').
card_artist('auriok sunchaser'/'SOM', 'James Ryman').
card_number('auriok sunchaser'/'SOM', '4').
card_flavor_text('auriok sunchaser'/'SOM', '\"Grant me loft. Grant me light. Grant me the accuracy I need to kill all who threaten Bladehold.\"\n—Prayer to the Whitesun').
card_multiverse_id('auriok sunchaser'/'SOM', '208249').
card_watermark('auriok sunchaser'/'SOM', 'Mirran').

card_in_set('barbed battlegear', 'SOM').
card_original_type('barbed battlegear'/'SOM', 'Artifact — Equipment').
card_original_text('barbed battlegear'/'SOM', 'Equipped creature gets +4/-1.\nEquip {2}').
card_first_print('barbed battlegear', 'SOM').
card_image_name('barbed battlegear'/'SOM', 'barbed battlegear').
card_uid('barbed battlegear'/'SOM', 'SOM:Barbed Battlegear:barbed battlegear').
card_rarity('barbed battlegear'/'SOM', 'Uncommon').
card_artist('barbed battlegear'/'SOM', 'Steve Argyle').
card_number('barbed battlegear'/'SOM', '139').
card_flavor_text('barbed battlegear'/'SOM', '\"One need only look at the inhabitants of this world to see that they are forged halfway to perfection. All they need is a whisper of the Glorious Word.\"\n—Urabrask the Hidden').
card_multiverse_id('barbed battlegear'/'SOM', '208255').
card_watermark('barbed battlegear'/'SOM', 'Mirran').

card_in_set('barrage ogre', 'SOM').
card_original_type('barrage ogre'/'SOM', 'Creature — Ogre Warrior').
card_original_text('barrage ogre'/'SOM', '{T}, Sacrifice an artifact: Barrage Ogre deals 2 damage to target creature or player.').
card_first_print('barrage ogre', 'SOM').
card_image_name('barrage ogre'/'SOM', 'barrage ogre').
card_uid('barrage ogre'/'SOM', 'SOM:Barrage Ogre:barrage ogre').
card_rarity('barrage ogre'/'SOM', 'Uncommon').
card_artist('barrage ogre'/'SOM', 'David Rapoza').
card_number('barrage ogre'/'SOM', '83').
card_flavor_text('barrage ogre'/'SOM', 'The elves had devised countless strategies to combat Memnarch\'s war machines, but they had no idea what to do when one was thrown at them.').
card_multiverse_id('barrage ogre'/'SOM', '194316').
card_watermark('barrage ogre'/'SOM', 'Mirran').

card_in_set('bellowing tanglewurm', 'SOM').
card_original_type('bellowing tanglewurm'/'SOM', 'Creature — Wurm').
card_original_text('bellowing tanglewurm'/'SOM', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)\nOther green creatures you control have intimidate.').
card_first_print('bellowing tanglewurm', 'SOM').
card_image_name('bellowing tanglewurm'/'SOM', 'bellowing tanglewurm').
card_uid('bellowing tanglewurm'/'SOM', 'SOM:Bellowing Tanglewurm:bellowing tanglewurm').
card_rarity('bellowing tanglewurm'/'SOM', 'Uncommon').
card_artist('bellowing tanglewurm'/'SOM', 'jD').
card_number('bellowing tanglewurm'/'SOM', '111').
card_multiverse_id('bellowing tanglewurm'/'SOM', '209048').
card_watermark('bellowing tanglewurm'/'SOM', 'Mirran').

card_in_set('blackcleave cliffs', 'SOM').
card_original_type('blackcleave cliffs'/'SOM', 'Land').
card_original_text('blackcleave cliffs'/'SOM', 'Blackcleave Cliffs enters the battlefield tapped unless you control two or fewer other lands.\n{T}: Add {B} or {R} to your mana pool.').
card_first_print('blackcleave cliffs', 'SOM').
card_image_name('blackcleave cliffs'/'SOM', 'blackcleave cliffs').
card_uid('blackcleave cliffs'/'SOM', 'SOM:Blackcleave Cliffs:blackcleave cliffs').
card_rarity('blackcleave cliffs'/'SOM', 'Rare').
card_artist('blackcleave cliffs'/'SOM', 'Dave Kendall').
card_number('blackcleave cliffs'/'SOM', '224').
card_flavor_text('blackcleave cliffs'/'SOM', 'Where the Oxidda Chain mingles with the Mephidross, oil-suffused metal crumbles away, leaving walls of blackened bones.').
card_multiverse_id('blackcleave cliffs'/'SOM', '209401').
card_watermark('blackcleave cliffs'/'SOM', 'Mirran').

card_in_set('blackcleave goblin', 'SOM').
card_original_type('blackcleave goblin'/'SOM', 'Creature — Goblin Zombie').
card_original_text('blackcleave goblin'/'SOM', 'Haste\nInfect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_first_print('blackcleave goblin', 'SOM').
card_image_name('blackcleave goblin'/'SOM', 'blackcleave goblin').
card_uid('blackcleave goblin'/'SOM', 'SOM:Blackcleave Goblin:blackcleave goblin').
card_rarity('blackcleave goblin'/'SOM', 'Common').
card_artist('blackcleave goblin'/'SOM', 'Nils Hamm').
card_number('blackcleave goblin'/'SOM', '54').
card_flavor_text('blackcleave goblin'/'SOM', '\"Bitten? Save time, amputate now.\"\n—Lephar, loxodon surgeon').
card_multiverse_id('blackcleave goblin'/'SOM', '194297').
card_watermark('blackcleave goblin'/'SOM', 'Phyrexian').

card_in_set('blade-tribe berserkers', 'SOM').
card_original_type('blade-tribe berserkers'/'SOM', 'Creature — Human Berserker').
card_original_text('blade-tribe berserkers'/'SOM', 'Metalcraft — When Blade-Tribe Berserkers enters the battlefield, if you control three or more artifacts, Blade-Tribe Berserkers gets +3/+3 and gains haste until end of turn.').
card_first_print('blade-tribe berserkers', 'SOM').
card_image_name('blade-tribe berserkers'/'SOM', 'blade-tribe berserkers').
card_uid('blade-tribe berserkers'/'SOM', 'SOM:Blade-Tribe Berserkers:blade-tribe berserkers').
card_rarity('blade-tribe berserkers'/'SOM', 'Common').
card_artist('blade-tribe berserkers'/'SOM', 'Kev Walker').
card_number('blade-tribe berserkers'/'SOM', '84').
card_multiverse_id('blade-tribe berserkers'/'SOM', '194165').
card_watermark('blade-tribe berserkers'/'SOM', 'Mirran').

card_in_set('bladed pinions', 'SOM').
card_original_type('bladed pinions'/'SOM', 'Artifact — Equipment').
card_original_text('bladed pinions'/'SOM', 'Equipped creature has flying and first strike.\nEquip {2}').
card_first_print('bladed pinions', 'SOM').
card_image_name('bladed pinions'/'SOM', 'bladed pinions').
card_uid('bladed pinions'/'SOM', 'SOM:Bladed Pinions:bladed pinions').
card_rarity('bladed pinions'/'SOM', 'Common').
card_artist('bladed pinions'/'SOM', 'Steve Argyle').
card_number('bladed pinions'/'SOM', '140').
card_flavor_text('bladed pinions'/'SOM', 'Lacking trained pterons, the Auriok had to rely on other measures to gain the upper hand in the skies.').
card_multiverse_id('bladed pinions'/'SOM', '206354').
card_watermark('bladed pinions'/'SOM', 'Mirran').

card_in_set('bleak coven vampires', 'SOM').
card_original_type('bleak coven vampires'/'SOM', 'Creature — Vampire Warrior').
card_original_text('bleak coven vampires'/'SOM', 'Metalcraft — When Bleak Coven Vampires enters the battlefield, if you control three or more artifacts, target player loses 4 life and you gain 4 life.').
card_first_print('bleak coven vampires', 'SOM').
card_image_name('bleak coven vampires'/'SOM', 'bleak coven vampires').
card_uid('bleak coven vampires'/'SOM', 'SOM:Bleak Coven Vampires:bleak coven vampires').
card_rarity('bleak coven vampires'/'SOM', 'Common').
card_artist('bleak coven vampires'/'SOM', 'Randis Albion').
card_number('bleak coven vampires'/'SOM', '55').
card_flavor_text('bleak coven vampires'/'SOM', 'As the shadow of the Mephidross spread over the Tangle, the vampires\' territory expanded.').
card_multiverse_id('bleak coven vampires'/'SOM', '209006').
card_watermark('bleak coven vampires'/'SOM', 'Mirran').

card_in_set('blight mamba', 'SOM').
card_original_type('blight mamba'/'SOM', 'Creature — Snake').
card_original_text('blight mamba'/'SOM', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\n{1}{G}: Regenerate Blight Mamba.').
card_first_print('blight mamba', 'SOM').
card_image_name('blight mamba'/'SOM', 'blight mamba').
card_uid('blight mamba'/'SOM', 'SOM:Blight Mamba:blight mamba').
card_rarity('blight mamba'/'SOM', 'Common').
card_artist('blight mamba'/'SOM', 'Drew Baker').
card_number('blight mamba'/'SOM', '112').
card_flavor_text('blight mamba'/'SOM', 'Its venom drips with Phyrexia\'s hate.').
card_multiverse_id('blight mamba'/'SOM', '194158').
card_watermark('blight mamba'/'SOM', 'Phyrexian').

card_in_set('blistergrub', 'SOM').
card_original_type('blistergrub'/'SOM', 'Creature — Horror').
card_original_text('blistergrub'/'SOM', 'Swampwalk\nWhen Blistergrub is put into a graveyard from the battlefield, each opponent loses 2 life.').
card_first_print('blistergrub', 'SOM').
card_image_name('blistergrub'/'SOM', 'blistergrub').
card_uid('blistergrub'/'SOM', 'SOM:Blistergrub:blistergrub').
card_rarity('blistergrub'/'SOM', 'Common').
card_artist('blistergrub'/'SOM', 'Daarken').
card_number('blistergrub'/'SOM', '56').
card_flavor_text('blistergrub'/'SOM', '\"The sooner you join Phyrexia, the sooner you\'ll forget your painful rebirth.\"\n—Sheoldred, Whispering One').
card_multiverse_id('blistergrub'/'SOM', '202687').
card_watermark('blistergrub'/'SOM', 'Phyrexian').

card_in_set('bloodshot trainee', 'SOM').
card_original_type('bloodshot trainee'/'SOM', 'Creature — Goblin Warrior').
card_original_text('bloodshot trainee'/'SOM', '{T}: Bloodshot Trainee deals 4 damage to target creature. Activate this ability only if Bloodshot Trainee\'s power is 4 or greater.').
card_image_name('bloodshot trainee'/'SOM', 'bloodshot trainee').
card_uid('bloodshot trainee'/'SOM', 'SOM:Bloodshot Trainee:bloodshot trainee').
card_rarity('bloodshot trainee'/'SOM', 'Uncommon').
card_artist('bloodshot trainee'/'SOM', 'Matt Stewart').
card_number('bloodshot trainee'/'SOM', '85').
card_flavor_text('bloodshot trainee'/'SOM', '\"Just think how strong I\'ll be if my arms don\'t tear off!\"').
card_multiverse_id('bloodshot trainee'/'SOM', '194255').
card_watermark('bloodshot trainee'/'SOM', 'Mirran').

card_in_set('blunt the assault', 'SOM').
card_original_type('blunt the assault'/'SOM', 'Instant').
card_original_text('blunt the assault'/'SOM', 'You gain 1 life for each creature on the battlefield. Prevent all combat damage that would be dealt this turn.').
card_first_print('blunt the assault', 'SOM').
card_image_name('blunt the assault'/'SOM', 'blunt the assault').
card_uid('blunt the assault'/'SOM', 'SOM:Blunt the Assault:blunt the assault').
card_rarity('blunt the assault'/'SOM', 'Common').
card_artist('blunt the assault'/'SOM', 'Matt Stewart').
card_number('blunt the assault'/'SOM', '113').
card_flavor_text('blunt the assault'/'SOM', '\"Much can be gained from the appearance of vulnerability.\"\n—Ezuri, renegade leader').
card_multiverse_id('blunt the assault'/'SOM', '205486').
card_watermark('blunt the assault'/'SOM', 'Mirran').

card_in_set('bonds of quicksilver', 'SOM').
card_original_type('bonds of quicksilver'/'SOM', 'Enchantment — Aura').
card_original_text('bonds of quicksilver'/'SOM', 'Flash (You may cast this spell any time you could cast an instant.)\nEnchant creature\nEnchanted creature doesn\'t untap during its controller\'s untap step.').
card_first_print('bonds of quicksilver', 'SOM').
card_image_name('bonds of quicksilver'/'SOM', 'bonds of quicksilver').
card_uid('bonds of quicksilver'/'SOM', 'SOM:Bonds of Quicksilver:bonds of quicksilver').
card_rarity('bonds of quicksilver'/'SOM', 'Common').
card_artist('bonds of quicksilver'/'SOM', 'Steven Belledin').
card_number('bonds of quicksilver'/'SOM', '29').
card_flavor_text('bonds of quicksilver'/'SOM', 'The sea\'s reach extends beyond its shores.').
card_multiverse_id('bonds of quicksilver'/'SOM', '202638').
card_watermark('bonds of quicksilver'/'SOM', 'Mirran').

card_in_set('carapace forger', 'SOM').
card_original_type('carapace forger'/'SOM', 'Creature — Elf Artificer').
card_original_text('carapace forger'/'SOM', 'Metalcraft — Carapace Forger gets +2/+2 as long as you control three or more artifacts.').
card_first_print('carapace forger', 'SOM').
card_image_name('carapace forger'/'SOM', 'carapace forger').
card_uid('carapace forger'/'SOM', 'SOM:Carapace Forger:carapace forger').
card_rarity('carapace forger'/'SOM', 'Common').
card_artist('carapace forger'/'SOM', 'Matt Cavotta').
card_number('carapace forger'/'SOM', '114').
card_flavor_text('carapace forger'/'SOM', '\"Bows and whips cannot save us from these new horrors of the Mephidross.\"').
card_multiverse_id('carapace forger'/'SOM', '208250').
card_watermark('carapace forger'/'SOM', 'Mirran').

card_in_set('carnifex demon', 'SOM').
card_original_type('carnifex demon'/'SOM', 'Creature — Demon').
card_original_text('carnifex demon'/'SOM', 'Flying\nCarnifex Demon enters the battlefield with two -1/-1 counters on it.\n{B}, Remove a -1/-1 counter from Carnifex Demon: Put a -1/-1 counter on each other creature.').
card_first_print('carnifex demon', 'SOM').
card_image_name('carnifex demon'/'SOM', 'carnifex demon').
card_uid('carnifex demon'/'SOM', 'SOM:Carnifex Demon:carnifex demon').
card_rarity('carnifex demon'/'SOM', 'Rare').
card_artist('carnifex demon'/'SOM', 'Aleksi Briclot').
card_number('carnifex demon'/'SOM', '57').
card_flavor_text('carnifex demon'/'SOM', 'Every contagion has a host.').
card_multiverse_id('carnifex demon'/'SOM', '206357').
card_watermark('carnifex demon'/'SOM', 'Phyrexian').

card_in_set('carrion call', 'SOM').
card_original_type('carrion call'/'SOM', 'Instant').
card_original_text('carrion call'/'SOM', 'Put two 1/1 green Insect creature tokens with infect onto the battlefield. (They deal damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_first_print('carrion call', 'SOM').
card_image_name('carrion call'/'SOM', 'carrion call').
card_uid('carrion call'/'SOM', 'SOM:Carrion Call:carrion call').
card_rarity('carrion call'/'SOM', 'Uncommon').
card_artist('carrion call'/'SOM', 'Adrian Smith').
card_number('carrion call'/'SOM', '115').
card_flavor_text('carrion call'/'SOM', 'The natural order is out of order.').
card_multiverse_id('carrion call'/'SOM', '194340').
card_watermark('carrion call'/'SOM', 'Phyrexian').

card_in_set('cerebral eruption', 'SOM').
card_original_type('cerebral eruption'/'SOM', 'Sorcery').
card_original_text('cerebral eruption'/'SOM', 'Target opponent reveals the top card of his or her library. Cerebral Eruption deals damage equal to the revealed card\'s converted mana cost to that player and each creature he or she controls. If a land card is revealed this way, return Cerebral Eruption to its owner\'s hand.').
card_first_print('cerebral eruption', 'SOM').
card_image_name('cerebral eruption'/'SOM', 'cerebral eruption').
card_uid('cerebral eruption'/'SOM', 'SOM:Cerebral Eruption:cerebral eruption').
card_rarity('cerebral eruption'/'SOM', 'Rare').
card_artist('cerebral eruption'/'SOM', 'Kev Walker').
card_number('cerebral eruption'/'SOM', '86').
card_multiverse_id('cerebral eruption'/'SOM', '215073').
card_watermark('cerebral eruption'/'SOM', 'Mirran').

card_in_set('chimeric mass', 'SOM').
card_original_type('chimeric mass'/'SOM', 'Artifact').
card_original_text('chimeric mass'/'SOM', 'Chimeric Mass enters the battlefield with X charge counters on it.\n{1}: Until end of turn, Chimeric Mass becomes a Construct artifact creature with \"This creature\'s power and toughness are each equal to the number of charge counters on it.\"').
card_first_print('chimeric mass', 'SOM').
card_image_name('chimeric mass'/'SOM', 'chimeric mass').
card_uid('chimeric mass'/'SOM', 'SOM:Chimeric Mass:chimeric mass').
card_rarity('chimeric mass'/'SOM', 'Rare').
card_artist('chimeric mass'/'SOM', 'David Palumbo').
card_number('chimeric mass'/'SOM', '141').
card_multiverse_id('chimeric mass'/'SOM', '209714').
card_watermark('chimeric mass'/'SOM', 'Mirran').

card_in_set('chrome steed', 'SOM').
card_original_type('chrome steed'/'SOM', 'Artifact Creature — Horse').
card_original_text('chrome steed'/'SOM', 'Metalcraft — Chrome Steed gets +2/+2 as long as you control three or more artifacts.').
card_first_print('chrome steed', 'SOM').
card_image_name('chrome steed'/'SOM', 'chrome steed').
card_uid('chrome steed'/'SOM', 'SOM:Chrome Steed:chrome steed').
card_rarity('chrome steed'/'SOM', 'Common').
card_artist('chrome steed'/'SOM', 'Jana Schirmer & Johannes Voss').
card_number('chrome steed'/'SOM', '142').
card_flavor_text('chrome steed'/'SOM', 'According to Auriok myth, it collects scrap in order to reassemble its lost rider.').
card_multiverse_id('chrome steed'/'SOM', '212247').
card_watermark('chrome steed'/'SOM', 'Mirran').

card_in_set('clone shell', 'SOM').
card_original_type('clone shell'/'SOM', 'Artifact Creature — Shapeshifter').
card_original_text('clone shell'/'SOM', 'Imprint — When Clone Shell enters the battlefield, look at the top four cards of your library, exile one face down, then put the rest on the bottom of your library in any order.\nWhen Clone Shell is put into a graveyard from the battlefield, turn the exiled card face up. If it\'s a creature card, put it onto the battlefield under your control.').
card_first_print('clone shell', 'SOM').
card_image_name('clone shell'/'SOM', 'clone shell').
card_uid('clone shell'/'SOM', 'SOM:Clone Shell:clone shell').
card_rarity('clone shell'/'SOM', 'Uncommon').
card_artist('clone shell'/'SOM', 'Volkan Baga').
card_number('clone shell'/'SOM', '143').
card_multiverse_id('clone shell'/'SOM', '209278').
card_watermark('clone shell'/'SOM', 'Mirran').

card_in_set('contagion clasp', 'SOM').
card_original_type('contagion clasp'/'SOM', 'Artifact').
card_original_text('contagion clasp'/'SOM', 'When Contagion Clasp enters the battlefield, put a -1/-1 counter on target creature.\n{4}, {T}: Proliferate. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there.)').
card_image_name('contagion clasp'/'SOM', 'contagion clasp').
card_uid('contagion clasp'/'SOM', 'SOM:Contagion Clasp:contagion clasp').
card_rarity('contagion clasp'/'SOM', 'Uncommon').
card_artist('contagion clasp'/'SOM', 'Anthony Palumbo').
card_number('contagion clasp'/'SOM', '144').
card_multiverse_id('contagion clasp'/'SOM', '194341').
card_watermark('contagion clasp'/'SOM', 'Phyrexian').

card_in_set('contagion engine', 'SOM').
card_original_type('contagion engine'/'SOM', 'Artifact').
card_original_text('contagion engine'/'SOM', 'When Contagion Engine enters the battlefield, put a -1/-1 counter on each creature target player controls.\n{4}, {T}: Proliferate, then proliferate again. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there. Then do it again.)').
card_first_print('contagion engine', 'SOM').
card_image_name('contagion engine'/'SOM', 'contagion engine').
card_uid('contagion engine'/'SOM', 'SOM:Contagion Engine:contagion engine').
card_rarity('contagion engine'/'SOM', 'Rare').
card_artist('contagion engine'/'SOM', 'Daarken').
card_number('contagion engine'/'SOM', '145').
card_multiverse_id('contagion engine'/'SOM', '212252').
card_watermark('contagion engine'/'SOM', 'Phyrexian').

card_in_set('contagious nim', 'SOM').
card_original_type('contagious nim'/'SOM', 'Creature — Zombie').
card_original_text('contagious nim'/'SOM', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_first_print('contagious nim', 'SOM').
card_image_name('contagious nim'/'SOM', 'contagious nim').
card_uid('contagious nim'/'SOM', 'SOM:Contagious Nim:contagious nim').
card_rarity('contagious nim'/'SOM', 'Common').
card_artist('contagious nim'/'SOM', 'Efrem Palacios').
card_number('contagious nim'/'SOM', '58').
card_flavor_text('contagious nim'/'SOM', '\"This species has grown to be my preferred receptacle for our divine message.\"\n—Sheoldred, Whispering One').
card_multiverse_id('contagious nim'/'SOM', '194150').
card_watermark('contagious nim'/'SOM', 'Phyrexian').

card_in_set('copper myr', 'SOM').
card_original_type('copper myr'/'SOM', 'Artifact Creature — Myr').
card_original_text('copper myr'/'SOM', '{T}: Add {G} to your mana pool.').
card_image_name('copper myr'/'SOM', 'copper myr').
card_uid('copper myr'/'SOM', 'SOM:Copper Myr:copper myr').
card_rarity('copper myr'/'SOM', 'Common').
card_artist('copper myr'/'SOM', 'Alan Pollack').
card_number('copper myr'/'SOM', '146').
card_flavor_text('copper myr'/'SOM', 'The myr are like verdigris: an ever-present patina on the surface of a metal world.').
card_multiverse_id('copper myr'/'SOM', '194063').
card_watermark('copper myr'/'SOM', 'Mirran').

card_in_set('copperhorn scout', 'SOM').
card_original_type('copperhorn scout'/'SOM', 'Creature — Elf Scout').
card_original_text('copperhorn scout'/'SOM', 'Whenever Copperhorn Scout attacks, untap each other creature you control.').
card_first_print('copperhorn scout', 'SOM').
card_image_name('copperhorn scout'/'SOM', 'copperhorn scout').
card_uid('copperhorn scout'/'SOM', 'SOM:Copperhorn Scout:copperhorn scout').
card_rarity('copperhorn scout'/'SOM', 'Common').
card_artist('copperhorn scout'/'SOM', 'Shelly Wan').
card_number('copperhorn scout'/'SOM', '116').
card_flavor_text('copperhorn scout'/'SOM', 'Fangren spur may sound louder, but Viridian resurgents claim that the horn of the vorrac creates a truer, more inspiring note.').
card_multiverse_id('copperhorn scout'/'SOM', '194270').
card_watermark('copperhorn scout'/'SOM', 'Mirran').

card_in_set('copperline gorge', 'SOM').
card_original_type('copperline gorge'/'SOM', 'Land').
card_original_text('copperline gorge'/'SOM', 'Copperline Gorge enters the battlefield tapped unless you control two or fewer other lands.\n{T}: Add {R} or {G} to your mana pool.').
card_first_print('copperline gorge', 'SOM').
card_image_name('copperline gorge'/'SOM', 'copperline gorge').
card_uid('copperline gorge'/'SOM', 'SOM:Copperline Gorge:copperline gorge').
card_rarity('copperline gorge'/'SOM', 'Rare').
card_artist('copperline gorge'/'SOM', 'Zoltan Boros & Gabor Szikszai').
card_number('copperline gorge'/'SOM', '225').
card_flavor_text('copperline gorge'/'SOM', 'Where the Tangle overruns the Oxidda mountains, metallic beasts scratch their territories in the verdigris.').
card_multiverse_id('copperline gorge'/'SOM', '209408').
card_watermark('copperline gorge'/'SOM', 'Mirran').

card_in_set('corpse cur', 'SOM').
card_original_type('corpse cur'/'SOM', 'Artifact Creature — Hound').
card_original_text('corpse cur'/'SOM', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nWhen Corpse Cur enters the battlefield, you may return target creature card with infect from your graveyard to your hand.').
card_first_print('corpse cur', 'SOM').
card_image_name('corpse cur'/'SOM', 'corpse cur').
card_uid('corpse cur'/'SOM', 'SOM:Corpse Cur:corpse cur').
card_rarity('corpse cur'/'SOM', 'Common').
card_artist('corpse cur'/'SOM', 'Pete Venters').
card_number('corpse cur'/'SOM', '147').
card_multiverse_id('corpse cur'/'SOM', '194337').
card_watermark('corpse cur'/'SOM', 'Phyrexian').

card_in_set('corrupted harvester', 'SOM').
card_original_type('corrupted harvester'/'SOM', 'Creature — Horror').
card_original_text('corrupted harvester'/'SOM', '{B}, Sacrifice a creature: Regenerate Corrupted Harvester.').
card_first_print('corrupted harvester', 'SOM').
card_image_name('corrupted harvester'/'SOM', 'corrupted harvester').
card_uid('corrupted harvester'/'SOM', 'SOM:Corrupted Harvester:corrupted harvester').
card_rarity('corrupted harvester'/'SOM', 'Uncommon').
card_artist('corrupted harvester'/'SOM', 'Nils Hamm').
card_number('corrupted harvester'/'SOM', '59').
card_flavor_text('corrupted harvester'/'SOM', '\"Before the blessed assault begins, we must seek specimens that are well-adapted to our way of . . . life.\"\n—Sheoldred, Whispering One').
card_multiverse_id('corrupted harvester'/'SOM', '202640').
card_watermark('corrupted harvester'/'SOM', 'Phyrexian').

card_in_set('culling dais', 'SOM').
card_original_type('culling dais'/'SOM', 'Artifact').
card_original_text('culling dais'/'SOM', '{T}, Sacrifice a creature: Put a charge counter on Culling Dais.\n{1}, Sacrifice Culling Dais: Draw a card for each charge counter on Culling Dais.').
card_first_print('culling dais', 'SOM').
card_image_name('culling dais'/'SOM', 'culling dais').
card_uid('culling dais'/'SOM', 'SOM:Culling Dais:culling dais').
card_rarity('culling dais'/'SOM', 'Uncommon').
card_artist('culling dais'/'SOM', 'Anthony Palumbo').
card_number('culling dais'/'SOM', '148').
card_flavor_text('culling dais'/'SOM', '\"Forswear the flesh and you will truly see.\"\n—Jin-Gitaxias, Core Augur').
card_multiverse_id('culling dais'/'SOM', '194127').
card_watermark('culling dais'/'SOM', 'Phyrexian').

card_in_set('cystbearer', 'SOM').
card_original_type('cystbearer'/'SOM', 'Creature — Beast').
card_original_text('cystbearer'/'SOM', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_first_print('cystbearer', 'SOM').
card_image_name('cystbearer'/'SOM', 'cystbearer').
card_uid('cystbearer'/'SOM', 'SOM:Cystbearer:cystbearer').
card_rarity('cystbearer'/'SOM', 'Common').
card_artist('cystbearer'/'SOM', 'Kev Walker').
card_number('cystbearer'/'SOM', '117').
card_flavor_text('cystbearer'/'SOM', 'Phyrexia strives to simulate natural life, but it can\'t resist improving on the design.').
card_multiverse_id('cystbearer'/'SOM', '205477').
card_watermark('cystbearer'/'SOM', 'Phyrexian').

card_in_set('darkslick drake', 'SOM').
card_original_type('darkslick drake'/'SOM', 'Creature — Drake').
card_original_text('darkslick drake'/'SOM', 'Flying\nWhen Darkslick Drake is put into a graveyard from the battlefield, draw a card.').
card_first_print('darkslick drake', 'SOM').
card_image_name('darkslick drake'/'SOM', 'darkslick drake').
card_uid('darkslick drake'/'SOM', 'SOM:Darkslick Drake:darkslick drake').
card_rarity('darkslick drake'/'SOM', 'Uncommon').
card_artist('darkslick drake'/'SOM', 'Chippy').
card_number('darkslick drake'/'SOM', '30').
card_flavor_text('darkslick drake'/'SOM', 'At the edge of the Mephidross, Phyrexia\'s influence seeps into life and land.').
card_multiverse_id('darkslick drake'/'SOM', '194217').
card_watermark('darkslick drake'/'SOM', 'Phyrexian').

card_in_set('darkslick shores', 'SOM').
card_original_type('darkslick shores'/'SOM', 'Land').
card_original_text('darkslick shores'/'SOM', 'Darkslick Shores enters the battlefield tapped unless you control two or fewer other lands.\n{T}: Add {U} or {B} to your mana pool.').
card_first_print('darkslick shores', 'SOM').
card_image_name('darkslick shores'/'SOM', 'darkslick shores').
card_uid('darkslick shores'/'SOM', 'SOM:Darkslick Shores:darkslick shores').
card_rarity('darkslick shores'/'SOM', 'Rare').
card_artist('darkslick shores'/'SOM', 'Charles Urbach').
card_number('darkslick shores'/'SOM', '226').
card_flavor_text('darkslick shores'/'SOM', 'Where the Mephidross leaches into the Quicksilver Sea, the waves writhe with necrogen.').
card_multiverse_id('darkslick shores'/'SOM', '209400').
card_watermark('darkslick shores'/'SOM', 'Mirran').

card_in_set('darksteel axe', 'SOM').
card_original_type('darksteel axe'/'SOM', 'Artifact — Equipment').
card_original_text('darksteel axe'/'SOM', 'Darksteel Axe is indestructible. (Effects that say \"destroy\" don\'t destroy it.)\nEquipped creature gets +2/+0.\nEquip {2}').
card_first_print('darksteel axe', 'SOM').
card_image_name('darksteel axe'/'SOM', 'darksteel axe').
card_uid('darksteel axe'/'SOM', 'SOM:Darksteel Axe:darksteel axe').
card_rarity('darksteel axe'/'SOM', 'Uncommon').
card_artist('darksteel axe'/'SOM', 'Daniel Ljunggren').
card_number('darksteel axe'/'SOM', '149').
card_flavor_text('darksteel axe'/'SOM', 'Heavier than it looks, tricky to wield, guaranteed to last.').
card_multiverse_id('darksteel axe'/'SOM', '209033').
card_watermark('darksteel axe'/'SOM', 'Mirran').

card_in_set('darksteel juggernaut', 'SOM').
card_original_type('darksteel juggernaut'/'SOM', 'Artifact Creature — Juggernaut').
card_original_text('darksteel juggernaut'/'SOM', 'Darksteel Juggernaut\'s power and toughness are each equal to the number of artifacts you control.\nDarksteel Juggernaut is indestructible and attacks each turn if able.').
card_first_print('darksteel juggernaut', 'SOM').
card_image_name('darksteel juggernaut'/'SOM', 'darksteel juggernaut').
card_uid('darksteel juggernaut'/'SOM', 'SOM:Darksteel Juggernaut:darksteel juggernaut').
card_rarity('darksteel juggernaut'/'SOM', 'Rare').
card_artist('darksteel juggernaut'/'SOM', 'Randis Albion').
card_number('darksteel juggernaut'/'SOM', '150').
card_flavor_text('darksteel juggernaut'/'SOM', 'One part unstoppable force, one part immovable object.').
card_multiverse_id('darksteel juggernaut'/'SOM', '209011').
card_watermark('darksteel juggernaut'/'SOM', 'Mirran').

card_in_set('darksteel myr', 'SOM').
card_original_type('darksteel myr'/'SOM', 'Artifact Creature — Myr').
card_original_text('darksteel myr'/'SOM', 'Darksteel Myr is indestructible. (Lethal damage and effects that say \"destroy\" don\'t destroy it. If its toughness is 0 or less, it\'s still put into its owner\'s graveyard.)').
card_first_print('darksteel myr', 'SOM').
card_image_name('darksteel myr'/'SOM', 'darksteel myr').
card_uid('darksteel myr'/'SOM', 'SOM:Darksteel Myr:darksteel myr').
card_rarity('darksteel myr'/'SOM', 'Uncommon').
card_artist('darksteel myr'/'SOM', 'Randis Albion').
card_number('darksteel myr'/'SOM', '151').
card_multiverse_id('darksteel myr'/'SOM', '209000').
card_watermark('darksteel myr'/'SOM', 'Mirran').

card_in_set('darksteel sentinel', 'SOM').
card_original_type('darksteel sentinel'/'SOM', 'Artifact Creature — Golem').
card_original_text('darksteel sentinel'/'SOM', 'Flash (You may cast this spell any time you could cast an instant.)\nVigilance\nDarksteel Sentinel is indestructible. (Lethal damage and effects that say \"destroy\" don\'t destroy it. If its toughness is 0 or less, it\'s still put into its owner\'s graveyard.)').
card_first_print('darksteel sentinel', 'SOM').
card_image_name('darksteel sentinel'/'SOM', 'darksteel sentinel').
card_uid('darksteel sentinel'/'SOM', 'SOM:Darksteel Sentinel:darksteel sentinel').
card_rarity('darksteel sentinel'/'SOM', 'Uncommon').
card_artist('darksteel sentinel'/'SOM', 'Erica Yang').
card_number('darksteel sentinel'/'SOM', '152').
card_multiverse_id('darksteel sentinel'/'SOM', '207865').
card_watermark('darksteel sentinel'/'SOM', 'Mirran').

card_in_set('dispense justice', 'SOM').
card_original_type('dispense justice'/'SOM', 'Instant').
card_original_text('dispense justice'/'SOM', 'Target player sacrifices an attacking creature.\nMetalcraft — That player sacrifices two attacking creatures instead if you control three or more artifacts.').
card_first_print('dispense justice', 'SOM').
card_image_name('dispense justice'/'SOM', 'dispense justice').
card_uid('dispense justice'/'SOM', 'SOM:Dispense Justice:dispense justice').
card_rarity('dispense justice'/'SOM', 'Uncommon').
card_artist('dispense justice'/'SOM', 'Austin Hsu').
card_number('dispense justice'/'SOM', '5').
card_flavor_text('dispense justice'/'SOM', 'The Accorders never strike first, but they always strike back.').
card_multiverse_id('dispense justice'/'SOM', '209015').
card_watermark('dispense justice'/'SOM', 'Mirran').

card_in_set('disperse', 'SOM').
card_original_type('disperse'/'SOM', 'Instant').
card_original_text('disperse'/'SOM', 'Return target nonland permanent to its owner\'s hand.').
card_image_name('disperse'/'SOM', 'disperse').
card_uid('disperse'/'SOM', 'SOM:Disperse:disperse').
card_rarity('disperse'/'SOM', 'Common').
card_artist('disperse'/'SOM', 'Adrian Smith').
card_number('disperse'/'SOM', '31').
card_flavor_text('disperse'/'SOM', 'Most casualties of the power struggle between vedalken and Neurok didn\'t die on the field of battle; they merely disappeared.').
card_multiverse_id('disperse'/'SOM', '212242').
card_watermark('disperse'/'SOM', 'Mirran').

card_in_set('dissipation field', 'SOM').
card_original_type('dissipation field'/'SOM', 'Enchantment').
card_original_text('dissipation field'/'SOM', 'Whenever a permanent deals damage to you, return it to its owner\'s hand.').
card_first_print('dissipation field', 'SOM').
card_image_name('dissipation field'/'SOM', 'dissipation field').
card_uid('dissipation field'/'SOM', 'SOM:Dissipation Field:dissipation field').
card_rarity('dissipation field'/'SOM', 'Rare').
card_artist('dissipation field'/'SOM', 'Matt Cavotta').
card_number('dissipation field'/'SOM', '32').
card_flavor_text('dissipation field'/'SOM', '\"Strike me once, shame on you. Strike me twice . . . Well, let\'s just see if you can.\"\n—Kara Vrist, Neurok agent').
card_multiverse_id('dissipation field'/'SOM', '202635').
card_watermark('dissipation field'/'SOM', 'Mirran').

card_in_set('dross hopper', 'SOM').
card_original_type('dross hopper'/'SOM', 'Creature — Insect Horror').
card_original_text('dross hopper'/'SOM', 'Sacrifice a creature: Dross Hopper gains flying until end of turn.').
card_first_print('dross hopper', 'SOM').
card_image_name('dross hopper'/'SOM', 'dross hopper').
card_uid('dross hopper'/'SOM', 'SOM:Dross Hopper:dross hopper').
card_rarity('dross hopper'/'SOM', 'Common').
card_artist('dross hopper'/'SOM', 'Dave Allsop').
card_number('dross hopper'/'SOM', '60').
card_flavor_text('dross hopper'/'SOM', 'Bred in the vicious Mephidross, dross hoppers learned to eat quickly and escape faster.').
card_multiverse_id('dross hopper'/'SOM', '194273').
card_watermark('dross hopper'/'SOM', 'Phyrexian').

card_in_set('echo circlet', 'SOM').
card_original_type('echo circlet'/'SOM', 'Artifact — Equipment').
card_original_text('echo circlet'/'SOM', 'Equipped creature can block an additional creature.\nEquip {1}').
card_first_print('echo circlet', 'SOM').
card_image_name('echo circlet'/'SOM', 'echo circlet').
card_uid('echo circlet'/'SOM', 'SOM:Echo Circlet:echo circlet').
card_rarity('echo circlet'/'SOM', 'Common').
card_artist('echo circlet'/'SOM', 'Daarken').
card_number('echo circlet'/'SOM', '153').
card_flavor_text('echo circlet'/'SOM', 'After the vanishing of the elder generations, the Anvil Tribe developed creative solutions to their personnel shortages.').
card_multiverse_id('echo circlet'/'SOM', '209042').
card_watermark('echo circlet'/'SOM', 'Mirran').

card_in_set('elspeth tirel', 'SOM').
card_original_type('elspeth tirel'/'SOM', 'Planeswalker — Elspeth').
card_original_text('elspeth tirel'/'SOM', '+2: You gain 1 life for each creature you control.\n-2: Put three 1/1 white Soldier creature tokens onto the battlefield.\n-5: Destroy all other permanents except\nfor lands and tokens.').
card_first_print('elspeth tirel', 'SOM').
card_image_name('elspeth tirel'/'SOM', 'elspeth tirel').
card_uid('elspeth tirel'/'SOM', 'SOM:Elspeth Tirel:elspeth tirel').
card_rarity('elspeth tirel'/'SOM', 'Mythic Rare').
card_artist('elspeth tirel'/'SOM', 'Michael Komarck').
card_number('elspeth tirel'/'SOM', '6').
card_multiverse_id('elspeth tirel'/'SOM', '212241').

card_in_set('embersmith', 'SOM').
card_original_type('embersmith'/'SOM', 'Creature — Human Artificer').
card_original_text('embersmith'/'SOM', 'Whenever you cast an artifact spell, you may pay {1}. If you do, Embersmith deals 1 damage to target creature or player.').
card_first_print('embersmith', 'SOM').
card_image_name('embersmith'/'SOM', 'embersmith').
card_uid('embersmith'/'SOM', 'SOM:Embersmith:embersmith').
card_rarity('embersmith'/'SOM', 'Uncommon').
card_artist('embersmith'/'SOM', 'Eric Deschamps').
card_number('embersmith'/'SOM', '87').
card_flavor_text('embersmith'/'SOM', 'The Vulshok see the artificer as a catalyst, bringing the spark of creation that ignites change.').
card_multiverse_id('embersmith'/'SOM', '194202').
card_watermark('embersmith'/'SOM', 'Mirran').

card_in_set('engulfing slagwurm', 'SOM').
card_original_type('engulfing slagwurm'/'SOM', 'Creature — Wurm').
card_original_text('engulfing slagwurm'/'SOM', 'Whenever Engulfing Slagwurm blocks or becomes blocked by a creature, destroy that creature. You gain life equal to that creature\'s toughness.').
card_first_print('engulfing slagwurm', 'SOM').
card_image_name('engulfing slagwurm'/'SOM', 'engulfing slagwurm').
card_uid('engulfing slagwurm'/'SOM', 'SOM:Engulfing Slagwurm:engulfing slagwurm').
card_rarity('engulfing slagwurm'/'SOM', 'Rare').
card_artist('engulfing slagwurm'/'SOM', 'Jaime Jones').
card_number('engulfing slagwurm'/'SOM', '118').
card_flavor_text('engulfing slagwurm'/'SOM', 'Its teeth exist only for decoration.').
card_multiverse_id('engulfing slagwurm'/'SOM', '215080').
card_watermark('engulfing slagwurm'/'SOM', 'Mirran').

card_in_set('etched champion', 'SOM').
card_original_type('etched champion'/'SOM', 'Artifact Creature — Soldier').
card_original_text('etched champion'/'SOM', 'Metalcraft — Etched Champion has protection from all colors as long as you control three or more artifacts.').
card_first_print('etched champion', 'SOM').
card_image_name('etched champion'/'SOM', 'etched champion').
card_uid('etched champion'/'SOM', 'SOM:Etched Champion:etched champion').
card_rarity('etched champion'/'SOM', 'Rare').
card_artist('etched champion'/'SOM', 'Matt Cavotta').
card_number('etched champion'/'SOM', '154').
card_flavor_text('etched champion'/'SOM', 'Its predecessors were etched with the wisdom of ancients; its own etchings bear warnings of a future fraught with war.').
card_multiverse_id('etched champion'/'SOM', '215099').
card_watermark('etched champion'/'SOM', 'Mirran').

card_in_set('exsanguinate', 'SOM').
card_original_type('exsanguinate'/'SOM', 'Sorcery').
card_original_text('exsanguinate'/'SOM', 'Each opponent loses X life. You gain life equal to the life lost this way.').
card_first_print('exsanguinate', 'SOM').
card_image_name('exsanguinate'/'SOM', 'exsanguinate').
card_uid('exsanguinate'/'SOM', 'SOM:Exsanguinate:exsanguinate').
card_rarity('exsanguinate'/'SOM', 'Uncommon').
card_artist('exsanguinate'/'SOM', 'Carl Critchlow').
card_number('exsanguinate'/'SOM', '61').
card_flavor_text('exsanguinate'/'SOM', 'Vampires don\'t consider patience a virtue nor gluttony a sin.').
card_multiverse_id('exsanguinate'/'SOM', '210235').
card_watermark('exsanguinate'/'SOM', 'Mirran').

card_in_set('ezuri\'s archers', 'SOM').
card_original_type('ezuri\'s archers'/'SOM', 'Creature — Elf Archer').
card_original_text('ezuri\'s archers'/'SOM', 'Reach (This creature can block creatures with flying.)\nWhenever Ezuri\'s Archers blocks a creature with flying, Ezuri\'s Archers gets +3/+0 until end of turn.').
card_first_print('ezuri\'s archers', 'SOM').
card_image_name('ezuri\'s archers'/'SOM', 'ezuri\'s archers').
card_uid('ezuri\'s archers'/'SOM', 'SOM:Ezuri\'s Archers:ezuri\'s archers').
card_rarity('ezuri\'s archers'/'SOM', 'Common').
card_artist('ezuri\'s archers'/'SOM', 'Shelly Wan').
card_number('ezuri\'s archers'/'SOM', '120').
card_multiverse_id('ezuri\'s archers'/'SOM', '194092').
card_watermark('ezuri\'s archers'/'SOM', 'Mirran').

card_in_set('ezuri\'s brigade', 'SOM').
card_original_type('ezuri\'s brigade'/'SOM', 'Creature — Elf Warrior').
card_original_text('ezuri\'s brigade'/'SOM', 'Metalcraft — As long as you control three or more artifacts, Ezuri\'s Brigade gets +4/+4 and has trample.').
card_first_print('ezuri\'s brigade', 'SOM').
card_image_name('ezuri\'s brigade'/'SOM', 'ezuri\'s brigade').
card_uid('ezuri\'s brigade'/'SOM', 'SOM:Ezuri\'s Brigade:ezuri\'s brigade').
card_rarity('ezuri\'s brigade'/'SOM', 'Rare').
card_artist('ezuri\'s brigade'/'SOM', 'Nic Klein').
card_number('ezuri\'s brigade'/'SOM', '121').
card_flavor_text('ezuri\'s brigade'/'SOM', 'Riding ravenous, ever-growing vorracs is almost as dangerous as fitting them with saddles.').
card_multiverse_id('ezuri\'s brigade'/'SOM', '212707').
card_watermark('ezuri\'s brigade'/'SOM', 'Mirran').

card_in_set('ezuri, renegade leader', 'SOM').
card_original_type('ezuri, renegade leader'/'SOM', 'Legendary Creature — Elf Warrior').
card_original_text('ezuri, renegade leader'/'SOM', '{G}: Regenerate another target Elf.\n{2}{G}{G}{G}: Elf creatures you control get +3/+3 and gain trample until end of turn.').
card_first_print('ezuri, renegade leader', 'SOM').
card_image_name('ezuri, renegade leader'/'SOM', 'ezuri, renegade leader').
card_uid('ezuri, renegade leader'/'SOM', 'SOM:Ezuri, Renegade Leader:ezuri, renegade leader').
card_rarity('ezuri, renegade leader'/'SOM', 'Rare').
card_artist('ezuri, renegade leader'/'SOM', 'Karl Kopinski').
card_number('ezuri, renegade leader'/'SOM', '119').
card_flavor_text('ezuri, renegade leader'/'SOM', 'The infamous Ezuri commands the highest bounty the vedalken have ever placed upon an outlaw.').
card_multiverse_id('ezuri, renegade leader'/'SOM', '194243').
card_watermark('ezuri, renegade leader'/'SOM', 'Mirran').

card_in_set('ferrovore', 'SOM').
card_original_type('ferrovore'/'SOM', 'Creature — Beast').
card_original_text('ferrovore'/'SOM', '{R}, Sacrifice an artifact: Ferrovore gets +3/+0 until end of turn.').
card_first_print('ferrovore', 'SOM').
card_image_name('ferrovore'/'SOM', 'ferrovore').
card_uid('ferrovore'/'SOM', 'SOM:Ferrovore:ferrovore').
card_rarity('ferrovore'/'SOM', 'Common').
card_artist('ferrovore'/'SOM', 'Austin Hsu').
card_number('ferrovore'/'SOM', '88').
card_flavor_text('ferrovore'/'SOM', 'The Vulshok use its digestion to break down the most obstinate metals, from darksteel myr to seastrider plates.').
card_multiverse_id('ferrovore'/'SOM', '194317').
card_watermark('ferrovore'/'SOM', 'Mirran').

card_in_set('flameborn hellion', 'SOM').
card_original_type('flameborn hellion'/'SOM', 'Creature — Hellion').
card_original_text('flameborn hellion'/'SOM', 'Haste\nFlameborn Hellion attacks each turn if able.').
card_first_print('flameborn hellion', 'SOM').
card_image_name('flameborn hellion'/'SOM', 'flameborn hellion').
card_uid('flameborn hellion'/'SOM', 'SOM:Flameborn Hellion:flameborn hellion').
card_rarity('flameborn hellion'/'SOM', 'Common').
card_artist('flameborn hellion'/'SOM', 'Aleksi Briclot').
card_number('flameborn hellion'/'SOM', '89').
card_flavor_text('flameborn hellion'/'SOM', '\"The most reliable omen of a future hellion attack is a past hellion attack.\"\n—Spear-Tribe teaching').
card_multiverse_id('flameborn hellion'/'SOM', '208256').
card_watermark('flameborn hellion'/'SOM', 'Mirran').

card_in_set('flesh allergy', 'SOM').
card_original_type('flesh allergy'/'SOM', 'Sorcery').
card_original_text('flesh allergy'/'SOM', 'As an additional cost to cast Flesh Allergy, sacrifice a creature.\nDestroy target creature. Its controller loses life equal to the number of creatures put into all graveyards from the battlefield this turn.').
card_first_print('flesh allergy', 'SOM').
card_image_name('flesh allergy'/'SOM', 'flesh allergy').
card_uid('flesh allergy'/'SOM', 'SOM:Flesh Allergy:flesh allergy').
card_rarity('flesh allergy'/'SOM', 'Uncommon').
card_artist('flesh allergy'/'SOM', 'Vance Kovacs').
card_number('flesh allergy'/'SOM', '62').
card_multiverse_id('flesh allergy'/'SOM', '208999').
card_watermark('flesh allergy'/'SOM', 'Phyrexian').

card_in_set('flight spellbomb', 'SOM').
card_original_type('flight spellbomb'/'SOM', 'Artifact').
card_original_text('flight spellbomb'/'SOM', '{T}, Sacrifice Flight Spellbomb: Target creature gains flying until end of turn.\nWhen Flight Spellbomb is put into a graveyard from the battlefield, you may pay {U}. If you do, draw a card.').
card_first_print('flight spellbomb', 'SOM').
card_image_name('flight spellbomb'/'SOM', 'flight spellbomb').
card_uid('flight spellbomb'/'SOM', 'SOM:Flight Spellbomb:flight spellbomb').
card_rarity('flight spellbomb'/'SOM', 'Common').
card_artist('flight spellbomb'/'SOM', 'Franz Vohwinkel').
card_number('flight spellbomb'/'SOM', '155').
card_multiverse_id('flight spellbomb'/'SOM', '210233').
card_watermark('flight spellbomb'/'SOM', 'Mirran').

card_in_set('forest', 'SOM').
card_original_type('forest'/'SOM', 'Basic Land — Forest').
card_original_text('forest'/'SOM', 'G').
card_image_name('forest'/'SOM', 'forest1').
card_uid('forest'/'SOM', 'SOM:Forest:forest1').
card_rarity('forest'/'SOM', 'Basic Land').
card_artist('forest'/'SOM', 'Mark Tedin').
card_number('forest'/'SOM', '246').
card_multiverse_id('forest'/'SOM', '214665').

card_in_set('forest', 'SOM').
card_original_type('forest'/'SOM', 'Basic Land — Forest').
card_original_text('forest'/'SOM', 'G').
card_image_name('forest'/'SOM', 'forest2').
card_uid('forest'/'SOM', 'SOM:Forest:forest2').
card_rarity('forest'/'SOM', 'Basic Land').
card_artist('forest'/'SOM', 'Mark Tedin').
card_number('forest'/'SOM', '247').
card_multiverse_id('forest'/'SOM', '214673').

card_in_set('forest', 'SOM').
card_original_type('forest'/'SOM', 'Basic Land — Forest').
card_original_text('forest'/'SOM', 'G').
card_image_name('forest'/'SOM', 'forest3').
card_uid('forest'/'SOM', 'SOM:Forest:forest3').
card_rarity('forest'/'SOM', 'Basic Land').
card_artist('forest'/'SOM', 'Mark Tedin').
card_number('forest'/'SOM', '248').
card_multiverse_id('forest'/'SOM', '214680').

card_in_set('forest', 'SOM').
card_original_type('forest'/'SOM', 'Basic Land — Forest').
card_original_text('forest'/'SOM', 'G').
card_image_name('forest'/'SOM', 'forest4').
card_uid('forest'/'SOM', 'SOM:Forest:forest4').
card_rarity('forest'/'SOM', 'Basic Land').
card_artist('forest'/'SOM', 'Mark Tedin').
card_number('forest'/'SOM', '249').
card_multiverse_id('forest'/'SOM', '214670').

card_in_set('fulgent distraction', 'SOM').
card_original_type('fulgent distraction'/'SOM', 'Instant').
card_original_text('fulgent distraction'/'SOM', 'Choose two target creatures. Tap those creatures, then unattach all Equipment from them.').
card_first_print('fulgent distraction', 'SOM').
card_image_name('fulgent distraction'/'SOM', 'fulgent distraction').
card_uid('fulgent distraction'/'SOM', 'SOM:Fulgent Distraction:fulgent distraction').
card_rarity('fulgent distraction'/'SOM', 'Common').
card_artist('fulgent distraction'/'SOM', 'Nic Klein').
card_number('fulgent distraction'/'SOM', '7').
card_flavor_text('fulgent distraction'/'SOM', '\"Channeling the white-hot power of all five suns creates a pretty good diversion.\"\n—Yavash of the Accorders').
card_multiverse_id('fulgent distraction'/'SOM', '205488').
card_watermark('fulgent distraction'/'SOM', 'Mirran').

card_in_set('fume spitter', 'SOM').
card_original_type('fume spitter'/'SOM', 'Creature — Horror').
card_original_text('fume spitter'/'SOM', 'Sacrifice Fume Spitter: Put a -1/-1 counter on target creature.').
card_first_print('fume spitter', 'SOM').
card_image_name('fume spitter'/'SOM', 'fume spitter').
card_uid('fume spitter'/'SOM', 'SOM:Fume Spitter:fume spitter').
card_rarity('fume spitter'/'SOM', 'Common').
card_artist('fume spitter'/'SOM', 'Nils Hamm').
card_number('fume spitter'/'SOM', '63').
card_flavor_text('fume spitter'/'SOM', '\"Our archers made sport of it as it fumbled its way up the slag ridge. As it collapsed we thought ourselves safe, but the foul thing carried more than necrogen.\"\n—Adaran, Tangle hunter').
card_multiverse_id('fume spitter'/'SOM', '209005').
card_watermark('fume spitter'/'SOM', 'Phyrexian').

card_in_set('furnace celebration', 'SOM').
card_original_type('furnace celebration'/'SOM', 'Enchantment').
card_original_text('furnace celebration'/'SOM', 'Whenever you sacrifice another permanent, you may pay {2}. If you do, Furnace Celebration deals 2 damage to target creature or player.').
card_first_print('furnace celebration', 'SOM').
card_image_name('furnace celebration'/'SOM', 'furnace celebration').
card_uid('furnace celebration'/'SOM', 'SOM:Furnace Celebration:furnace celebration').
card_rarity('furnace celebration'/'SOM', 'Uncommon').
card_artist('furnace celebration'/'SOM', 'Svetlin Velinov').
card_number('furnace celebration'/'SOM', '90').
card_flavor_text('furnace celebration'/'SOM', 'Reduce. Reuse. Wreak havoc.').
card_multiverse_id('furnace celebration'/'SOM', '194374').
card_watermark('furnace celebration'/'SOM', 'Mirran').

card_in_set('galvanic blast', 'SOM').
card_original_type('galvanic blast'/'SOM', 'Instant').
card_original_text('galvanic blast'/'SOM', 'Galvanic Blast deals 2 damage to target creature or player.\nMetalcraft — Galvanic Blast deals 4 damage to that creature or player instead if you control three or more artifacts.').
card_first_print('galvanic blast', 'SOM').
card_image_name('galvanic blast'/'SOM', 'galvanic blast').
card_uid('galvanic blast'/'SOM', 'SOM:Galvanic Blast:galvanic blast').
card_rarity('galvanic blast'/'SOM', 'Common').
card_artist('galvanic blast'/'SOM', 'Marc Simonetti').
card_number('galvanic blast'/'SOM', '91').
card_flavor_text('galvanic blast'/'SOM', 'Mirrodin has little weather, but it certainly has lightning.').
card_multiverse_id('galvanic blast'/'SOM', '208251').
card_watermark('galvanic blast'/'SOM', 'Mirran').

card_in_set('genesis wave', 'SOM').
card_original_type('genesis wave'/'SOM', 'Sorcery').
card_original_text('genesis wave'/'SOM', 'Reveal the top X cards of your library. You may put any number of permanent cards with converted mana cost X or less from among them onto the battlefield. Then put all cards revealed this way that weren\'t put onto the battlefield into your graveyard.').
card_first_print('genesis wave', 'SOM').
card_image_name('genesis wave'/'SOM', 'genesis wave').
card_uid('genesis wave'/'SOM', 'SOM:Genesis Wave:genesis wave').
card_rarity('genesis wave'/'SOM', 'Rare').
card_artist('genesis wave'/'SOM', 'James Paick').
card_number('genesis wave'/'SOM', '122').
card_multiverse_id('genesis wave'/'SOM', '207882').
card_watermark('genesis wave'/'SOM', 'Mirran').

card_in_set('geth, lord of the vault', 'SOM').
card_original_type('geth, lord of the vault'/'SOM', 'Legendary Creature — Zombie').
card_original_text('geth, lord of the vault'/'SOM', 'Intimidate\n{X}{B}: Put target artifact or creature card with converted mana cost X from an opponent\'s graveyard onto the battlefield under your control tapped. Then that player puts the top X cards of his or her library into his or her graveyard.').
card_first_print('geth, lord of the vault', 'SOM').
card_image_name('geth, lord of the vault'/'SOM', 'geth, lord of the vault').
card_uid('geth, lord of the vault'/'SOM', 'SOM:Geth, Lord of the Vault:geth, lord of the vault').
card_rarity('geth, lord of the vault'/'SOM', 'Mythic Rare').
card_artist('geth, lord of the vault'/'SOM', 'Whit Brachna').
card_number('geth, lord of the vault'/'SOM', '64').
card_multiverse_id('geth, lord of the vault'/'SOM', '215076').
card_watermark('geth, lord of the vault'/'SOM', 'Phyrexian').

card_in_set('ghalma\'s warden', 'SOM').
card_original_type('ghalma\'s warden'/'SOM', 'Creature — Elephant Soldier').
card_original_text('ghalma\'s warden'/'SOM', 'Metalcraft — Ghalma\'s Warden gets +2/+2 as long as you control three or more artifacts.').
card_first_print('ghalma\'s warden', 'SOM').
card_image_name('ghalma\'s warden'/'SOM', 'ghalma\'s warden').
card_uid('ghalma\'s warden'/'SOM', 'SOM:Ghalma\'s Warden:ghalma\'s warden').
card_rarity('ghalma\'s warden'/'SOM', 'Common').
card_artist('ghalma\'s warden'/'SOM', 'Mike Bierek').
card_number('ghalma\'s warden'/'SOM', '8').
card_flavor_text('ghalma\'s warden'/'SOM', 'A special unit guards the loxodon artificer known as Ghalma the Shaper. They are armed and armored in her finest works of silver and steel.').
card_multiverse_id('ghalma\'s warden'/'SOM', '194124').
card_watermark('ghalma\'s warden'/'SOM', 'Mirran').

card_in_set('glimmerpoint stag', 'SOM').
card_original_type('glimmerpoint stag'/'SOM', 'Creature — Elk').
card_original_text('glimmerpoint stag'/'SOM', 'Vigilance\nWhen Glimmerpoint Stag enters the battlefield, exile another target permanent. Return that card to the battlefield under its owner\'s control at the beginning of the next end step.').
card_first_print('glimmerpoint stag', 'SOM').
card_image_name('glimmerpoint stag'/'SOM', 'glimmerpoint stag').
card_uid('glimmerpoint stag'/'SOM', 'SOM:Glimmerpoint Stag:glimmerpoint stag').
card_rarity('glimmerpoint stag'/'SOM', 'Uncommon').
card_artist('glimmerpoint stag'/'SOM', 'Ryan Pancoast').
card_number('glimmerpoint stag'/'SOM', '9').
card_multiverse_id('glimmerpoint stag'/'SOM', '194046').
card_watermark('glimmerpoint stag'/'SOM', 'Mirran').

card_in_set('glimmerpost', 'SOM').
card_original_type('glimmerpost'/'SOM', 'Land — Locus').
card_original_text('glimmerpost'/'SOM', 'When Glimmerpost enters the battlefield, you gain 1 life for each Locus on the battlefield.\n{T}: Add {1} to your mana pool.').
card_first_print('glimmerpost', 'SOM').
card_image_name('glimmerpost'/'SOM', 'glimmerpost').
card_uid('glimmerpost'/'SOM', 'SOM:Glimmerpost:glimmerpost').
card_rarity('glimmerpost'/'SOM', 'Common').
card_artist('glimmerpost'/'SOM', 'Matt Cavotta').
card_number('glimmerpost'/'SOM', '227').
card_flavor_text('glimmerpost'/'SOM', 'What once spied upon Mirrodin from above now waits below for another to assume control.').
card_multiverse_id('glimmerpost'/'SOM', '209043').
card_watermark('glimmerpost'/'SOM', 'Mirran').

card_in_set('glint hawk', 'SOM').
card_original_type('glint hawk'/'SOM', 'Creature — Bird').
card_original_text('glint hawk'/'SOM', 'Flying\nWhen Glint Hawk enters the battlefield, sacrifice it unless you return an artifact you control to its owner\'s hand.').
card_first_print('glint hawk', 'SOM').
card_image_name('glint hawk'/'SOM', 'glint hawk').
card_uid('glint hawk'/'SOM', 'SOM:Glint Hawk:glint hawk').
card_rarity('glint hawk'/'SOM', 'Common').
card_artist('glint hawk'/'SOM', 'Dave Allsop').
card_number('glint hawk'/'SOM', '10').
card_flavor_text('glint hawk'/'SOM', 'Its eyes burned nearly blind by the Whitesun, it hunts by metallic gleam.').
card_multiverse_id('glint hawk'/'SOM', '206358').
card_watermark('glint hawk'/'SOM', 'Mirran').

card_in_set('glint hawk idol', 'SOM').
card_original_type('glint hawk idol'/'SOM', 'Artifact').
card_original_text('glint hawk idol'/'SOM', 'Whenever another artifact enters the battlefield under your control, you may have Glint Hawk Idol become a 2/2 artifact creature with flying until end of turn.\n{W}: Glint Hawk Idol becomes a 2/2 artifact creature with flying until end of turn.').
card_first_print('glint hawk idol', 'SOM').
card_image_name('glint hawk idol'/'SOM', 'glint hawk idol').
card_uid('glint hawk idol'/'SOM', 'SOM:Glint Hawk Idol:glint hawk idol').
card_rarity('glint hawk idol'/'SOM', 'Common').
card_artist('glint hawk idol'/'SOM', 'Dave Allsop').
card_number('glint hawk idol'/'SOM', '156').
card_multiverse_id('glint hawk idol'/'SOM', '194049').
card_watermark('glint hawk idol'/'SOM', 'Mirran').

card_in_set('goblin gaveleer', 'SOM').
card_original_type('goblin gaveleer'/'SOM', 'Creature — Goblin Warrior').
card_original_text('goblin gaveleer'/'SOM', 'Trample\nGoblin Gaveleer gets +2/+0 for each Equipment attached to it.').
card_first_print('goblin gaveleer', 'SOM').
card_image_name('goblin gaveleer'/'SOM', 'goblin gaveleer').
card_uid('goblin gaveleer'/'SOM', 'SOM:Goblin Gaveleer:goblin gaveleer').
card_rarity('goblin gaveleer'/'SOM', 'Common').
card_artist('goblin gaveleer'/'SOM', 'Svetlin Velinov').
card_number('goblin gaveleer'/'SOM', '92').
card_flavor_text('goblin gaveleer'/'SOM', '\"When everything looks like a nail, you really need to get yourself a hammer.\"\n—Krol, goblin furnace-priest').
card_multiverse_id('goblin gaveleer'/'SOM', '194373').
card_watermark('goblin gaveleer'/'SOM', 'Mirran').

card_in_set('gold myr', 'SOM').
card_original_type('gold myr'/'SOM', 'Artifact Creature — Myr').
card_original_text('gold myr'/'SOM', '{T}: Add {W} to your mana pool.').
card_image_name('gold myr'/'SOM', 'gold myr').
card_uid('gold myr'/'SOM', 'SOM:Gold Myr:gold myr').
card_rarity('gold myr'/'SOM', 'Common').
card_artist('gold myr'/'SOM', 'Alan Pollack').
card_number('gold myr'/'SOM', '157').
card_flavor_text('gold myr'/'SOM', 'The myr are like razorgrass: numberless metal figures, reflecting each other\'s light.').
card_multiverse_id('gold myr'/'SOM', '194384').
card_watermark('gold myr'/'SOM', 'Mirran').

card_in_set('golden urn', 'SOM').
card_original_type('golden urn'/'SOM', 'Artifact').
card_original_text('golden urn'/'SOM', 'At the beginning of your upkeep, you may put a charge counter on Golden Urn.\n{T}, Sacrifice Golden Urn: You gain life equal to the number of charge counters on Golden Urn.').
card_first_print('golden urn', 'SOM').
card_image_name('golden urn'/'SOM', 'golden urn').
card_uid('golden urn'/'SOM', 'SOM:Golden Urn:golden urn').
card_rarity('golden urn'/'SOM', 'Common').
card_artist('golden urn'/'SOM', 'Charles Urbach').
card_number('golden urn'/'SOM', '158').
card_multiverse_id('golden urn'/'SOM', '213926').
card_watermark('golden urn'/'SOM', 'Mirran').

card_in_set('golem artisan', 'SOM').
card_original_type('golem artisan'/'SOM', 'Artifact Creature — Golem').
card_original_text('golem artisan'/'SOM', '{2}: Target artifact creature gets +1/+1 until end of turn.\n{2}: Target artifact creature gains your choice of flying, trample, or haste until end of turn.').
card_first_print('golem artisan', 'SOM').
card_image_name('golem artisan'/'SOM', 'golem artisan').
card_uid('golem artisan'/'SOM', 'SOM:Golem Artisan:golem artisan').
card_rarity('golem artisan'/'SOM', 'Uncommon').
card_artist('golem artisan'/'SOM', 'Nic Klein').
card_number('golem artisan'/'SOM', '159').
card_flavor_text('golem artisan'/'SOM', 'Better living through metallurgy.').
card_multiverse_id('golem artisan'/'SOM', '206360').
card_watermark('golem artisan'/'SOM', 'Mirran').

card_in_set('golem foundry', 'SOM').
card_original_type('golem foundry'/'SOM', 'Artifact').
card_original_text('golem foundry'/'SOM', 'Whenever you cast an artifact spell, you may put a charge counter on Golem Foundry.\nRemove three charge counters from Golem Foundry: Put a 3/3 colorless Golem artifact creature token onto the battlefield.').
card_first_print('golem foundry', 'SOM').
card_image_name('golem foundry'/'SOM', 'golem foundry').
card_uid('golem foundry'/'SOM', 'SOM:Golem Foundry:golem foundry').
card_rarity('golem foundry'/'SOM', 'Common').
card_artist('golem foundry'/'SOM', 'Nic Klein').
card_number('golem foundry'/'SOM', '160').
card_multiverse_id('golem foundry'/'SOM', '194314').
card_watermark('golem foundry'/'SOM', 'Mirran').

card_in_set('golem\'s heart', 'SOM').
card_original_type('golem\'s heart'/'SOM', 'Artifact').
card_original_text('golem\'s heart'/'SOM', 'Whenever a player casts an artifact spell, you may gain 1 life.').
card_image_name('golem\'s heart'/'SOM', 'golem\'s heart').
card_uid('golem\'s heart'/'SOM', 'SOM:Golem\'s Heart:golem\'s heart').
card_rarity('golem\'s heart'/'SOM', 'Uncommon').
card_artist('golem\'s heart'/'SOM', 'Matt Cavotta').
card_number('golem\'s heart'/'SOM', '161').
card_flavor_text('golem\'s heart'/'SOM', 'The heart of a golem gives life to more than just the iron husk around it.').
card_multiverse_id('golem\'s heart'/'SOM', '205472').
card_watermark('golem\'s heart'/'SOM', 'Mirran').

card_in_set('grafted exoskeleton', 'SOM').
card_original_type('grafted exoskeleton'/'SOM', 'Artifact — Equipment').
card_original_text('grafted exoskeleton'/'SOM', 'Equipped creature gets +2/+2 and has infect. (It deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nWhenever Grafted Exoskeleton becomes unattached from a permanent, sacrifice that permanent.\nEquip {2}').
card_first_print('grafted exoskeleton', 'SOM').
card_image_name('grafted exoskeleton'/'SOM', 'grafted exoskeleton').
card_uid('grafted exoskeleton'/'SOM', 'SOM:Grafted Exoskeleton:grafted exoskeleton').
card_rarity('grafted exoskeleton'/'SOM', 'Uncommon').
card_artist('grafted exoskeleton'/'SOM', 'Allen Williams').
card_number('grafted exoskeleton'/'SOM', '162').
card_multiverse_id('grafted exoskeleton'/'SOM', '209045').
card_watermark('grafted exoskeleton'/'SOM', 'Phyrexian').

card_in_set('grand architect', 'SOM').
card_original_type('grand architect'/'SOM', 'Creature — Vedalken Artificer').
card_original_text('grand architect'/'SOM', 'Other blue creatures you control get +1/+1.\n{U}: Target artifact creature becomes blue until end of turn.\nTap an untapped blue creature you control: Add {2} to your mana pool. Spend this mana only to cast artifact spells or activate abilities of artifacts.').
card_first_print('grand architect', 'SOM').
card_image_name('grand architect'/'SOM', 'grand architect').
card_uid('grand architect'/'SOM', 'SOM:Grand Architect:grand architect').
card_rarity('grand architect'/'SOM', 'Rare').
card_artist('grand architect'/'SOM', 'Steven Belledin').
card_number('grand architect'/'SOM', '33').
card_multiverse_id('grand architect'/'SOM', '207884').
card_watermark('grand architect'/'SOM', 'Mirran').

card_in_set('grasp of darkness', 'SOM').
card_original_type('grasp of darkness'/'SOM', 'Instant').
card_original_text('grasp of darkness'/'SOM', 'Target creature gets -4/-4 until end of turn.').
card_first_print('grasp of darkness', 'SOM').
card_image_name('grasp of darkness'/'SOM', 'grasp of darkness').
card_uid('grasp of darkness'/'SOM', 'SOM:Grasp of Darkness:grasp of darkness').
card_rarity('grasp of darkness'/'SOM', 'Common').
card_artist('grasp of darkness'/'SOM', 'Johann Bodin').
card_number('grasp of darkness'/'SOM', '65').
card_flavor_text('grasp of darkness'/'SOM', 'On a world with five suns, night is compelled to become an aggressive force.').
card_multiverse_id('grasp of darkness'/'SOM', '194105').
card_watermark('grasp of darkness'/'SOM', 'Mirran').

card_in_set('grindclock', 'SOM').
card_original_type('grindclock'/'SOM', 'Artifact').
card_original_text('grindclock'/'SOM', '{T}: Put a charge counter on Grindclock.\n{T}: Target player puts the top X cards of his or her library into his or her graveyard, where X is the number of charge counters on Grindclock.').
card_first_print('grindclock', 'SOM').
card_image_name('grindclock'/'SOM', 'grindclock').
card_uid('grindclock'/'SOM', 'SOM:Grindclock:grindclock').
card_rarity('grindclock'/'SOM', 'Rare').
card_artist('grindclock'/'SOM', 'Nils Hamm').
card_number('grindclock'/'SOM', '163').
card_flavor_text('grindclock'/'SOM', 'Pray you never hear it chime.').
card_multiverse_id('grindclock'/'SOM', '194134').
card_watermark('grindclock'/'SOM', 'Mirran').

card_in_set('halt order', 'SOM').
card_original_type('halt order'/'SOM', 'Instant').
card_original_text('halt order'/'SOM', 'Counter target artifact spell.\nDraw a card.').
card_first_print('halt order', 'SOM').
card_image_name('halt order'/'SOM', 'halt order').
card_uid('halt order'/'SOM', 'SOM:Halt Order:halt order').
card_rarity('halt order'/'SOM', 'Uncommon').
card_artist('halt order'/'SOM', 'Izzy').
card_number('halt order'/'SOM', '34').
card_flavor_text('halt order'/'SOM', 'An artificer\'s least favorite mind-missive may be \"I regret to inform you that the funding for your project has been cut.\"').
card_multiverse_id('halt order'/'SOM', '194110').
card_watermark('halt order'/'SOM', 'Mirran').

card_in_set('hand of the praetors', 'SOM').
card_original_type('hand of the praetors'/'SOM', 'Creature — Zombie').
card_original_text('hand of the praetors'/'SOM', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nOther creatures you control with infect get +1/+1.\nWhenever you cast a creature spell with infect, target player gets a poison counter.').
card_first_print('hand of the praetors', 'SOM').
card_image_name('hand of the praetors'/'SOM', 'hand of the praetors').
card_uid('hand of the praetors'/'SOM', 'SOM:Hand of the Praetors:hand of the praetors').
card_rarity('hand of the praetors'/'SOM', 'Rare').
card_artist('hand of the praetors'/'SOM', 'Izzy').
card_number('hand of the praetors'/'SOM', '66').
card_multiverse_id('hand of the praetors'/'SOM', '215086').
card_watermark('hand of the praetors'/'SOM', 'Phyrexian').

card_in_set('heavy arbalest', 'SOM').
card_original_type('heavy arbalest'/'SOM', 'Artifact — Equipment').
card_original_text('heavy arbalest'/'SOM', 'Equipped creature doesn\'t untap during its controller\'s untap step.\nEquipped creature has \"{T}: This creature deals 2 damage to target creature or player.\"\nEquip {4}').
card_first_print('heavy arbalest', 'SOM').
card_image_name('heavy arbalest'/'SOM', 'heavy arbalest').
card_uid('heavy arbalest'/'SOM', 'SOM:Heavy Arbalest:heavy arbalest').
card_rarity('heavy arbalest'/'SOM', 'Uncommon').
card_artist('heavy arbalest'/'SOM', 'David Rapoza').
card_number('heavy arbalest'/'SOM', '164').
card_multiverse_id('heavy arbalest'/'SOM', '209050').
card_watermark('heavy arbalest'/'SOM', 'Mirran').

card_in_set('hoard-smelter dragon', 'SOM').
card_original_type('hoard-smelter dragon'/'SOM', 'Creature — Dragon').
card_original_text('hoard-smelter dragon'/'SOM', 'Flying\n{3}{R}: Destroy target artifact. Hoard-Smelter Dragon gets +X/+0 until end of turn, where X is that artifact\'s converted mana cost.').
card_first_print('hoard-smelter dragon', 'SOM').
card_image_name('hoard-smelter dragon'/'SOM', 'hoard-smelter dragon').
card_uid('hoard-smelter dragon'/'SOM', 'SOM:Hoard-Smelter Dragon:hoard-smelter dragon').
card_rarity('hoard-smelter dragon'/'SOM', 'Rare').
card_artist('hoard-smelter dragon'/'SOM', 'Eric Deschamps').
card_number('hoard-smelter dragon'/'SOM', '93').
card_flavor_text('hoard-smelter dragon'/'SOM', 'He collects the precious metals, and then converts the rest to propellant.').
card_multiverse_id('hoard-smelter dragon'/'SOM', '215074').
card_watermark('hoard-smelter dragon'/'SOM', 'Mirran').

card_in_set('horizon spellbomb', 'SOM').
card_original_type('horizon spellbomb'/'SOM', 'Artifact').
card_original_text('horizon spellbomb'/'SOM', '{2}, {T}, Sacrifice Horizon Spellbomb: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.\nWhen Horizon Spellbomb is put into a graveyard from the battlefield, you may pay {G}. If you do, draw a card.').
card_first_print('horizon spellbomb', 'SOM').
card_image_name('horizon spellbomb'/'SOM', 'horizon spellbomb').
card_uid('horizon spellbomb'/'SOM', 'SOM:Horizon Spellbomb:horizon spellbomb').
card_rarity('horizon spellbomb'/'SOM', 'Common').
card_artist('horizon spellbomb'/'SOM', 'Franz Vohwinkel').
card_number('horizon spellbomb'/'SOM', '165').
card_multiverse_id('horizon spellbomb'/'SOM', '210229').
card_watermark('horizon spellbomb'/'SOM', 'Mirran').

card_in_set('ichor rats', 'SOM').
card_original_type('ichor rats'/'SOM', 'Creature — Rat').
card_original_text('ichor rats'/'SOM', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nWhen Ichor Rats enters the battlefield, each player gets a poison counter.').
card_first_print('ichor rats', 'SOM').
card_image_name('ichor rats'/'SOM', 'ichor rats').
card_uid('ichor rats'/'SOM', 'SOM:Ichor Rats:ichor rats').
card_rarity('ichor rats'/'SOM', 'Uncommon').
card_artist('ichor rats'/'SOM', 'Matt Stewart').
card_number('ichor rats'/'SOM', '67').
card_multiverse_id('ichor rats'/'SOM', '194177').
card_watermark('ichor rats'/'SOM', 'Phyrexian').

card_in_set('ichorclaw myr', 'SOM').
card_original_type('ichorclaw myr'/'SOM', 'Artifact Creature — Myr').
card_original_text('ichorclaw myr'/'SOM', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nWhenever Ichorclaw Myr becomes blocked, it gets +2/+2 until end of turn.').
card_first_print('ichorclaw myr', 'SOM').
card_image_name('ichorclaw myr'/'SOM', 'ichorclaw myr').
card_uid('ichorclaw myr'/'SOM', 'SOM:Ichorclaw Myr:ichorclaw myr').
card_rarity('ichorclaw myr'/'SOM', 'Common').
card_artist('ichorclaw myr'/'SOM', 'Eric Deschamps').
card_number('ichorclaw myr'/'SOM', '166').
card_multiverse_id('ichorclaw myr'/'SOM', '194256').
card_watermark('ichorclaw myr'/'SOM', 'Phyrexian').

card_in_set('indomitable archangel', 'SOM').
card_original_type('indomitable archangel'/'SOM', 'Creature — Angel').
card_original_text('indomitable archangel'/'SOM', 'Flying\nMetalcraft — Artifacts you control have shroud as long as you control three or more artifacts.').
card_first_print('indomitable archangel', 'SOM').
card_image_name('indomitable archangel'/'SOM', 'indomitable archangel').
card_uid('indomitable archangel'/'SOM', 'SOM:Indomitable Archangel:indomitable archangel').
card_rarity('indomitable archangel'/'SOM', 'Mythic Rare').
card_artist('indomitable archangel'/'SOM', 'Allen Williams').
card_number('indomitable archangel'/'SOM', '11').
card_flavor_text('indomitable archangel'/'SOM', '\"Every sword drawn to defend is an angelic blade, guided to justice by an angelic hand.\"\n—Auriok proverb').
card_multiverse_id('indomitable archangel'/'SOM', '212248').
card_watermark('indomitable archangel'/'SOM', 'Mirran').

card_in_set('inexorable tide', 'SOM').
card_original_type('inexorable tide'/'SOM', 'Enchantment').
card_original_text('inexorable tide'/'SOM', 'Whenever you cast a spell, proliferate. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there.)').
card_first_print('inexorable tide', 'SOM').
card_image_name('inexorable tide'/'SOM', 'inexorable tide').
card_uid('inexorable tide'/'SOM', 'SOM:Inexorable Tide:inexorable tide').
card_rarity('inexorable tide'/'SOM', 'Rare').
card_artist('inexorable tide'/'SOM', 'Dave Kendall').
card_number('inexorable tide'/'SOM', '35').
card_flavor_text('inexorable tide'/'SOM', '\"See how gratefully this world accepts our blessings.\"\n—Jin-Gitaxias, Core Augur').
card_multiverse_id('inexorable tide'/'SOM', '194363').
card_watermark('inexorable tide'/'SOM', 'Phyrexian').

card_in_set('infiltration lens', 'SOM').
card_original_type('infiltration lens'/'SOM', 'Artifact — Equipment').
card_original_text('infiltration lens'/'SOM', 'Whenever equipped creature becomes blocked by a creature, you may draw two cards.\nEquip {1}').
card_first_print('infiltration lens', 'SOM').
card_image_name('infiltration lens'/'SOM', 'infiltration lens').
card_uid('infiltration lens'/'SOM', 'SOM:Infiltration Lens:infiltration lens').
card_rarity('infiltration lens'/'SOM', 'Uncommon').
card_artist('infiltration lens'/'SOM', 'Izzy').
card_number('infiltration lens'/'SOM', '167').
card_flavor_text('infiltration lens'/'SOM', 'Neurok spies carry devices that let them look a few moments into the future, giving them an almost insurmountable edge.').
card_multiverse_id('infiltration lens'/'SOM', '205480').
card_watermark('infiltration lens'/'SOM', 'Mirran').

card_in_set('instill infection', 'SOM').
card_original_type('instill infection'/'SOM', 'Instant').
card_original_text('instill infection'/'SOM', 'Put a -1/-1 counter on target creature.\nDraw a card.').
card_first_print('instill infection', 'SOM').
card_image_name('instill infection'/'SOM', 'instill infection').
card_uid('instill infection'/'SOM', 'SOM:Instill Infection:instill infection').
card_rarity('instill infection'/'SOM', 'Common').
card_artist('instill infection'/'SOM', 'Chris Rahn').
card_number('instill infection'/'SOM', '68').
card_flavor_text('instill infection'/'SOM', '\"The results of our ninth inoculation were the same. The specimen was, of course, euthanized before the contagion could spread.\"\n—Vedalken research notes').
card_multiverse_id('instill infection'/'SOM', '194349').
card_watermark('instill infection'/'SOM', 'Phyrexian').

card_in_set('iron myr', 'SOM').
card_original_type('iron myr'/'SOM', 'Artifact Creature — Myr').
card_original_text('iron myr'/'SOM', '{T}: Add {R} to your mana pool.').
card_image_name('iron myr'/'SOM', 'iron myr').
card_uid('iron myr'/'SOM', 'SOM:Iron Myr:iron myr').
card_rarity('iron myr'/'SOM', 'Common').
card_artist('iron myr'/'SOM', 'Alan Pollack').
card_number('iron myr'/'SOM', '168').
card_flavor_text('iron myr'/'SOM', 'The myr are like rusted metal: gleaming purpose hidden by a thin disguise of debris.').
card_multiverse_id('iron myr'/'SOM', '194168').
card_watermark('iron myr'/'SOM', 'Mirran').

card_in_set('island', 'SOM').
card_original_type('island'/'SOM', 'Basic Land — Island').
card_original_text('island'/'SOM', 'U').
card_image_name('island'/'SOM', 'island1').
card_uid('island'/'SOM', 'SOM:Island:island1').
card_rarity('island'/'SOM', 'Basic Land').
card_artist('island'/'SOM', 'Jung Park').
card_number('island'/'SOM', '234').
card_multiverse_id('island'/'SOM', '214672').

card_in_set('island', 'SOM').
card_original_type('island'/'SOM', 'Basic Land — Island').
card_original_text('island'/'SOM', 'U').
card_image_name('island'/'SOM', 'island2').
card_uid('island'/'SOM', 'SOM:Island:island2').
card_rarity('island'/'SOM', 'Basic Land').
card_artist('island'/'SOM', 'Jung Park').
card_number('island'/'SOM', '235').
card_multiverse_id('island'/'SOM', '214666').

card_in_set('island', 'SOM').
card_original_type('island'/'SOM', 'Basic Land — Island').
card_original_text('island'/'SOM', 'U').
card_image_name('island'/'SOM', 'island3').
card_uid('island'/'SOM', 'SOM:Island:island3').
card_rarity('island'/'SOM', 'Basic Land').
card_artist('island'/'SOM', 'Jung Park').
card_number('island'/'SOM', '236').
card_multiverse_id('island'/'SOM', '214678').

card_in_set('island', 'SOM').
card_original_type('island'/'SOM', 'Basic Land — Island').
card_original_text('island'/'SOM', 'U').
card_image_name('island'/'SOM', 'island4').
card_uid('island'/'SOM', 'SOM:Island:island4').
card_rarity('island'/'SOM', 'Basic Land').
card_artist('island'/'SOM', 'Jung Park').
card_number('island'/'SOM', '237').
card_multiverse_id('island'/'SOM', '214682').

card_in_set('kemba\'s skyguard', 'SOM').
card_original_type('kemba\'s skyguard'/'SOM', 'Creature — Cat Knight').
card_original_text('kemba\'s skyguard'/'SOM', 'Flying\nWhen Kemba\'s Skyguard enters the battlefield, you gain 2 life.').
card_image_name('kemba\'s skyguard'/'SOM', 'kemba\'s skyguard').
card_uid('kemba\'s skyguard'/'SOM', 'SOM:Kemba\'s Skyguard:kemba\'s skyguard').
card_rarity('kemba\'s skyguard'/'SOM', 'Common').
card_artist('kemba\'s skyguard'/'SOM', 'Whit Brachna').
card_number('kemba\'s skyguard'/'SOM', '13').
card_flavor_text('kemba\'s skyguard'/'SOM', '\"We\'re now to dispense aid to any Mirran we see battling anything . . . ‘strange.\' Regent\'s orders.\"\n—Ranya, skyhunter captain').
card_multiverse_id('kemba\'s skyguard'/'SOM', '194083').
card_watermark('kemba\'s skyguard'/'SOM', 'Mirran').

card_in_set('kemba, kha regent', 'SOM').
card_original_type('kemba, kha regent'/'SOM', 'Legendary Creature — Cat Cleric').
card_original_text('kemba, kha regent'/'SOM', 'At the beginning of your upkeep, put a 2/2 white Cat creature token onto the battlefield for each Equipment attached to Kemba, Kha Regent.').
card_first_print('kemba, kha regent', 'SOM').
card_image_name('kemba, kha regent'/'SOM', 'kemba, kha regent').
card_uid('kemba, kha regent'/'SOM', 'SOM:Kemba, Kha Regent:kemba, kha regent').
card_rarity('kemba, kha regent'/'SOM', 'Rare').
card_artist('kemba, kha regent'/'SOM', 'Todd Lockwood').
card_number('kemba, kha regent'/'SOM', '12').
card_flavor_text('kemba, kha regent'/'SOM', '\"I am not Raksha. I never will be. But I refuse to be the kha who watched her pride be torn asunder.\"').
card_multiverse_id('kemba, kha regent'/'SOM', '215091').
card_watermark('kemba, kha regent'/'SOM', 'Mirran').

card_in_set('koth of the hammer', 'SOM').
card_original_type('koth of the hammer'/'SOM', 'Planeswalker — Koth').
card_original_text('koth of the hammer'/'SOM', '+1: Untap target Mountain. It becomes a 4/4 red Elemental creature until end of turn. It\'s still a land.\n-2: Add {R} to your mana pool for each Mountain you control.\n-5: You get an emblem with \"Mountains you control have ‘{T}: This land deals 1 damage to target creature or player.\'\"').
card_first_print('koth of the hammer', 'SOM').
card_image_name('koth of the hammer'/'SOM', 'koth of the hammer').
card_uid('koth of the hammer'/'SOM', 'SOM:Koth of the Hammer:koth of the hammer').
card_rarity('koth of the hammer'/'SOM', 'Mythic Rare').
card_artist('koth of the hammer'/'SOM', 'Jason Chan').
card_number('koth of the hammer'/'SOM', '94').
card_multiverse_id('koth of the hammer'/'SOM', '212238').

card_in_set('kuldotha forgemaster', 'SOM').
card_original_type('kuldotha forgemaster'/'SOM', 'Artifact Creature — Construct').
card_original_text('kuldotha forgemaster'/'SOM', '{T}, Sacrifice three artifacts: Search your library for an artifact card and put it onto the battlefield. Then shuffle your library.').
card_first_print('kuldotha forgemaster', 'SOM').
card_image_name('kuldotha forgemaster'/'SOM', 'kuldotha forgemaster').
card_uid('kuldotha forgemaster'/'SOM', 'SOM:Kuldotha Forgemaster:kuldotha forgemaster').
card_rarity('kuldotha forgemaster'/'SOM', 'Rare').
card_artist('kuldotha forgemaster'/'SOM', 'jD').
card_number('kuldotha forgemaster'/'SOM', '169').
card_flavor_text('kuldotha forgemaster'/'SOM', 'The goblins say it used to be larger, before it began to stoke the Great Furnace with pieces of itself.').
card_multiverse_id('kuldotha forgemaster'/'SOM', '215098').
card_watermark('kuldotha forgemaster'/'SOM', 'Mirran').

card_in_set('kuldotha phoenix', 'SOM').
card_original_type('kuldotha phoenix'/'SOM', 'Creature — Phoenix').
card_original_text('kuldotha phoenix'/'SOM', 'Flying, haste\nMetalcraft — {4}: Return Kuldotha Phoenix from your graveyard to the battlefield. Activate this ability only during your upkeep and only if you control three or more artifacts.').
card_first_print('kuldotha phoenix', 'SOM').
card_image_name('kuldotha phoenix'/'SOM', 'kuldotha phoenix').
card_uid('kuldotha phoenix'/'SOM', 'SOM:Kuldotha Phoenix:kuldotha phoenix').
card_rarity('kuldotha phoenix'/'SOM', 'Rare').
card_artist('kuldotha phoenix'/'SOM', 'Mike Bierek').
card_number('kuldotha phoenix'/'SOM', '95').
card_multiverse_id('kuldotha phoenix'/'SOM', '194326').
card_watermark('kuldotha phoenix'/'SOM', 'Mirran').

card_in_set('kuldotha rebirth', 'SOM').
card_original_type('kuldotha rebirth'/'SOM', 'Sorcery').
card_original_text('kuldotha rebirth'/'SOM', 'As an additional cost to cast Kuldotha Rebirth, sacrifice an artifact.\nPut three 1/1 red Goblin creature tokens onto the battlefield.').
card_first_print('kuldotha rebirth', 'SOM').
card_image_name('kuldotha rebirth'/'SOM', 'kuldotha rebirth').
card_uid('kuldotha rebirth'/'SOM', 'SOM:Kuldotha Rebirth:kuldotha rebirth').
card_rarity('kuldotha rebirth'/'SOM', 'Common').
card_artist('kuldotha rebirth'/'SOM', 'Goran Josic').
card_number('kuldotha rebirth'/'SOM', '96').
card_flavor_text('kuldotha rebirth'/'SOM', 'All goblin rituals serve a dual purpose as fertility rites, even the destructive ones. Especially the destructive ones.').
card_multiverse_id('kuldotha rebirth'/'SOM', '194239').
card_watermark('kuldotha rebirth'/'SOM', 'Mirran').

card_in_set('leaden myr', 'SOM').
card_original_type('leaden myr'/'SOM', 'Artifact Creature — Myr').
card_original_text('leaden myr'/'SOM', '{T}: Add {B} to your mana pool.').
card_image_name('leaden myr'/'SOM', 'leaden myr').
card_uid('leaden myr'/'SOM', 'SOM:Leaden Myr:leaden myr').
card_rarity('leaden myr'/'SOM', 'Common').
card_artist('leaden myr'/'SOM', 'Alan Pollack').
card_number('leaden myr'/'SOM', '170').
card_flavor_text('leaden myr'/'SOM', 'The myr are like necrogen: a transformative force unconcerned with the changes they wreak.').
card_multiverse_id('leaden myr'/'SOM', '194204').
card_watermark('leaden myr'/'SOM', 'Mirran').

card_in_set('leonin arbiter', 'SOM').
card_original_type('leonin arbiter'/'SOM', 'Creature — Cat Cleric').
card_original_text('leonin arbiter'/'SOM', 'Players can\'t search libraries. Any player may pay {2} for that player to ignore this effect until end of turn.').
card_first_print('leonin arbiter', 'SOM').
card_image_name('leonin arbiter'/'SOM', 'leonin arbiter').
card_uid('leonin arbiter'/'SOM', 'SOM:Leonin Arbiter:leonin arbiter').
card_rarity('leonin arbiter'/'SOM', 'Rare').
card_artist('leonin arbiter'/'SOM', 'Shelly Wan').
card_number('leonin arbiter'/'SOM', '14').
card_flavor_text('leonin arbiter'/'SOM', '\"Our people are torn by infighting. Until the two sides reconcile, our laws can carry no meaning.\"').
card_multiverse_id('leonin arbiter'/'SOM', '209287').
card_watermark('leonin arbiter'/'SOM', 'Mirran').

card_in_set('liege of the tangle', 'SOM').
card_original_type('liege of the tangle'/'SOM', 'Creature — Elemental').
card_original_text('liege of the tangle'/'SOM', 'Trample\nWhenever Liege of the Tangle deals combat damage to a player, you may choose any number of target lands you control and put an awakening counter on each of them. Each of those lands is an 8/8 green Elemental creature for as long as it has an awakening counter on it. They\'re still lands.').
card_first_print('liege of the tangle', 'SOM').
card_image_name('liege of the tangle'/'SOM', 'liege of the tangle').
card_uid('liege of the tangle'/'SOM', 'SOM:Liege of the Tangle:liege of the tangle').
card_rarity('liege of the tangle'/'SOM', 'Mythic Rare').
card_artist('liege of the tangle'/'SOM', 'Jason Chan').
card_number('liege of the tangle'/'SOM', '123').
card_multiverse_id('liege of the tangle'/'SOM', '194130').
card_watermark('liege of the tangle'/'SOM', 'Mirran').

card_in_set('lifesmith', 'SOM').
card_original_type('lifesmith'/'SOM', 'Creature — Human Artificer').
card_original_text('lifesmith'/'SOM', 'Whenever you cast an artifact spell, you may pay {1}. If you do, you gain 3 life.').
card_first_print('lifesmith', 'SOM').
card_image_name('lifesmith'/'SOM', 'lifesmith').
card_uid('lifesmith'/'SOM', 'SOM:Lifesmith:lifesmith').
card_rarity('lifesmith'/'SOM', 'Uncommon').
card_artist('lifesmith'/'SOM', 'Eric Deschamps').
card_number('lifesmith'/'SOM', '124').
card_flavor_text('lifesmith'/'SOM', 'The Sylvok see the artificer as a gardener, preparing the world for hardy growth.').
card_multiverse_id('lifesmith'/'SOM', '208252').
card_watermark('lifesmith'/'SOM', 'Mirran').

card_in_set('liquimetal coating', 'SOM').
card_original_type('liquimetal coating'/'SOM', 'Artifact').
card_original_text('liquimetal coating'/'SOM', '{T}: Target permanent becomes an artifact in addition to its other types until end of turn.').
card_first_print('liquimetal coating', 'SOM').
card_image_name('liquimetal coating'/'SOM', 'liquimetal coating').
card_uid('liquimetal coating'/'SOM', 'SOM:Liquimetal Coating:liquimetal coating').
card_rarity('liquimetal coating'/'SOM', 'Uncommon').
card_artist('liquimetal coating'/'SOM', 'Johann Bodin').
card_number('liquimetal coating'/'SOM', '171').
card_flavor_text('liquimetal coating'/'SOM', '\"They\'ll soon become accustomed to wearing skin that is not their own.\"\n—Elesh Norn, Grand Cenobite').
card_multiverse_id('liquimetal coating'/'SOM', '212709').
card_watermark('liquimetal coating'/'SOM', 'Mirran').

card_in_set('livewire lash', 'SOM').
card_original_type('livewire lash'/'SOM', 'Artifact — Equipment').
card_original_text('livewire lash'/'SOM', 'Equipped creature gets +2/+0 and has \"Whenever this creature becomes the target of a spell, this creature deals 2 damage to target creature or player.\"\nEquip {2}').
card_first_print('livewire lash', 'SOM').
card_image_name('livewire lash'/'SOM', 'livewire lash').
card_uid('livewire lash'/'SOM', 'SOM:Livewire Lash:livewire lash').
card_rarity('livewire lash'/'SOM', 'Rare').
card_artist('livewire lash'/'SOM', 'Daniel Ljunggren').
card_number('livewire lash'/'SOM', '172').
card_multiverse_id('livewire lash'/'SOM', '212239').
card_watermark('livewire lash'/'SOM', 'Mirran').

card_in_set('loxodon wayfarer', 'SOM').
card_original_type('loxodon wayfarer'/'SOM', 'Creature — Elephant Monk').
card_original_text('loxodon wayfarer'/'SOM', '').
card_first_print('loxodon wayfarer', 'SOM').
card_image_name('loxodon wayfarer'/'SOM', 'loxodon wayfarer').
card_uid('loxodon wayfarer'/'SOM', 'SOM:Loxodon Wayfarer:loxodon wayfarer').
card_rarity('loxodon wayfarer'/'SOM', 'Common').
card_artist('loxodon wayfarer'/'SOM', 'Steven Belledin').
card_number('loxodon wayfarer'/'SOM', '15').
card_flavor_text('loxodon wayfarer'/'SOM', 'The Mirran elders vanished with Memnarch, leaving behind a generation of wayward orphans.').
card_multiverse_id('loxodon wayfarer'/'SOM', '207885').
card_watermark('loxodon wayfarer'/'SOM', 'Mirran').

card_in_set('lumengrid drake', 'SOM').
card_original_type('lumengrid drake'/'SOM', 'Creature — Drake').
card_original_text('lumengrid drake'/'SOM', 'Flying\nMetalcraft — When Lumengrid Drake enters the battlefield, if you control three or more artifacts, return target creature to its owner\'s hand.').
card_first_print('lumengrid drake', 'SOM').
card_image_name('lumengrid drake'/'SOM', 'lumengrid drake').
card_uid('lumengrid drake'/'SOM', 'SOM:Lumengrid Drake:lumengrid drake').
card_rarity('lumengrid drake'/'SOM', 'Common').
card_artist('lumengrid drake'/'SOM', 'Johann Bodin').
card_number('lumengrid drake'/'SOM', '36').
card_flavor_text('lumengrid drake'/'SOM', 'Properly outfitted, drakes made perfect sentries for vedalken strongholds.').
card_multiverse_id('lumengrid drake'/'SOM', '194178').
card_watermark('lumengrid drake'/'SOM', 'Mirran').

card_in_set('lux cannon', 'SOM').
card_original_type('lux cannon'/'SOM', 'Artifact').
card_original_text('lux cannon'/'SOM', '{T}: Put a charge counter on Lux Cannon.\n{T}, Remove three charge counters from Lux Cannon: Destroy target permanent.').
card_first_print('lux cannon', 'SOM').
card_image_name('lux cannon'/'SOM', 'lux cannon').
card_uid('lux cannon'/'SOM', 'SOM:Lux Cannon:lux cannon').
card_rarity('lux cannon'/'SOM', 'Mythic Rare').
card_artist('lux cannon'/'SOM', 'Martina Pilcerova').
card_number('lux cannon'/'SOM', '173').
card_flavor_text('lux cannon'/'SOM', 'There are few problems that can\'t be solved by putting a hole in the world.').
card_multiverse_id('lux cannon'/'SOM', '210236').
card_watermark('lux cannon'/'SOM', 'Mirran').

card_in_set('melt terrain', 'SOM').
card_original_type('melt terrain'/'SOM', 'Sorcery').
card_original_text('melt terrain'/'SOM', 'Destroy target land. Melt Terrain deals 2 damage to that land\'s controller.').
card_first_print('melt terrain', 'SOM').
card_image_name('melt terrain'/'SOM', 'melt terrain').
card_uid('melt terrain'/'SOM', 'SOM:Melt Terrain:melt terrain').
card_rarity('melt terrain'/'SOM', 'Common').
card_artist('melt terrain'/'SOM', 'John Avon').
card_number('melt terrain'/'SOM', '97').
card_flavor_text('melt terrain'/'SOM', 'Just as the most finely wrought metal can warp and rust, so too does Mirrodin buckle under pressures from without and within.').
card_multiverse_id('melt terrain'/'SOM', '194106').
card_watermark('melt terrain'/'SOM', 'Mirran').

card_in_set('memnite', 'SOM').
card_original_type('memnite'/'SOM', 'Artifact Creature — Construct').
card_original_text('memnite'/'SOM', '').
card_image_name('memnite'/'SOM', 'memnite').
card_uid('memnite'/'SOM', 'SOM:Memnite:memnite').
card_rarity('memnite'/'SOM', 'Uncommon').
card_artist('memnite'/'SOM', 'Svetlin Velinov').
card_number('memnite'/'SOM', '174').
card_flavor_text('memnite'/'SOM', 'Reminders of Memnarch\'s reign still skirr across Mirrodin, reminiscent of his form if not his power.').
card_multiverse_id('memnite'/'SOM', '194078').
card_watermark('memnite'/'SOM', 'Mirran').

card_in_set('memoricide', 'SOM').
card_original_type('memoricide'/'SOM', 'Sorcery').
card_original_text('memoricide'/'SOM', 'Name a nonland card. Search target player\'s graveyard, hand, and library for any number of cards with that name and exile them. Then that player shuffles his or her library.').
card_image_name('memoricide'/'SOM', 'memoricide').
card_uid('memoricide'/'SOM', 'SOM:Memoricide:memoricide').
card_rarity('memoricide'/'SOM', 'Rare').
card_artist('memoricide'/'SOM', 'James Ryman').
card_number('memoricide'/'SOM', '69').
card_flavor_text('memoricide'/'SOM', '\"You claim wisdom, yet I see nothing in here but tawdry wishes.\"').
card_multiverse_id('memoricide'/'SOM', '215103').
card_watermark('memoricide'/'SOM', 'Mirran').

card_in_set('mimic vat', 'SOM').
card_original_type('mimic vat'/'SOM', 'Artifact').
card_original_text('mimic vat'/'SOM', 'Imprint — Whenever a nontoken creature is put into a graveyard from the battlefield, you may exile that card. If you do, return each other card exiled with Mimic Vat to its owner\'s graveyard.\n{3}, {T}: Put a token onto the battlefield that\'s a copy of the exiled card. It gains haste. Exile it at the beginning of the next end step.').
card_first_print('mimic vat', 'SOM').
card_image_name('mimic vat'/'SOM', 'mimic vat').
card_uid('mimic vat'/'SOM', 'SOM:Mimic Vat:mimic vat').
card_rarity('mimic vat'/'SOM', 'Rare').
card_artist('mimic vat'/'SOM', 'Matt Cavotta').
card_number('mimic vat'/'SOM', '175').
card_multiverse_id('mimic vat'/'SOM', '207883').
card_watermark('mimic vat'/'SOM', 'Mirran').

card_in_set('mindslaver', 'SOM').
card_original_type('mindslaver'/'SOM', 'Legendary Artifact').
card_original_text('mindslaver'/'SOM', '{4}, {T}, Sacrifice Mindslaver: You control target player during that player\'s next turn. (You see all cards that player could see and make all decisions for the player.)').
card_image_name('mindslaver'/'SOM', 'mindslaver').
card_uid('mindslaver'/'SOM', 'SOM:Mindslaver:mindslaver').
card_rarity('mindslaver'/'SOM', 'Mythic Rare').
card_artist('mindslaver'/'SOM', 'Volkan Baga').
card_number('mindslaver'/'SOM', '176').
card_flavor_text('mindslaver'/'SOM', 'It\'s a helm that leaves the head at its most vulnerable.').
card_multiverse_id('mindslaver'/'SOM', '209044').
card_watermark('mindslaver'/'SOM', 'Mirran').

card_in_set('molder beast', 'SOM').
card_original_type('molder beast'/'SOM', 'Creature — Beast').
card_original_text('molder beast'/'SOM', 'Trample\nWhenever an artifact is put into a graveyard from the battlefield, Molder Beast gets +2/+0 until end of turn.').
card_first_print('molder beast', 'SOM').
card_image_name('molder beast'/'SOM', 'molder beast').
card_uid('molder beast'/'SOM', 'SOM:Molder Beast:molder beast').
card_rarity('molder beast'/'SOM', 'Common').
card_artist('molder beast'/'SOM', 'Randis Albion').
card_number('molder beast'/'SOM', '125').
card_flavor_text('molder beast'/'SOM', '\"A trail of scrap metal can lead it into an ambush. But take care not to overfeed it.\"\n—Adaran, Tangle hunter').
card_multiverse_id('molder beast'/'SOM', '209403').
card_watermark('molder beast'/'SOM', 'Mirran').

card_in_set('molten psyche', 'SOM').
card_original_type('molten psyche'/'SOM', 'Sorcery').
card_original_text('molten psyche'/'SOM', 'Each player shuffles the cards from his or her hand into his or her library, then draws that many cards.\nMetalcraft — If you control three or more artifacts, Molten Psyche deals damage to each opponent equal to the number of cards that player has drawn this turn.').
card_first_print('molten psyche', 'SOM').
card_image_name('molten psyche'/'SOM', 'molten psyche').
card_uid('molten psyche'/'SOM', 'SOM:Molten Psyche:molten psyche').
card_rarity('molten psyche'/'SOM', 'Rare').
card_artist('molten psyche'/'SOM', 'Ryan Yee').
card_number('molten psyche'/'SOM', '98').
card_multiverse_id('molten psyche'/'SOM', '212716').
card_watermark('molten psyche'/'SOM', 'Mirran').

card_in_set('molten-tail masticore', 'SOM').
card_original_type('molten-tail masticore'/'SOM', 'Artifact Creature — Masticore').
card_original_text('molten-tail masticore'/'SOM', 'At the beginning of your upkeep, sacrifice Molten-Tail Masticore unless you discard a card.\n{4}, Exile a creature card from your graveyard: Molten-Tail Masticore deals 4 damage to target creature or player.\n{2}: Regenerate Molten-Tail Masticore.').
card_first_print('molten-tail masticore', 'SOM').
card_image_name('molten-tail masticore'/'SOM', 'molten-tail masticore').
card_uid('molten-tail masticore'/'SOM', 'SOM:Molten-Tail Masticore:molten-tail masticore').
card_rarity('molten-tail masticore'/'SOM', 'Mythic Rare').
card_artist('molten-tail masticore'/'SOM', 'Whit Brachna').
card_number('molten-tail masticore'/'SOM', '177').
card_multiverse_id('molten-tail masticore'/'SOM', '215089').
card_watermark('molten-tail masticore'/'SOM', 'Mirran').

card_in_set('moriok reaver', 'SOM').
card_original_type('moriok reaver'/'SOM', 'Creature — Human Warrior').
card_original_text('moriok reaver'/'SOM', '').
card_first_print('moriok reaver', 'SOM').
card_image_name('moriok reaver'/'SOM', 'moriok reaver').
card_uid('moriok reaver'/'SOM', 'SOM:Moriok Reaver:moriok reaver').
card_rarity('moriok reaver'/'SOM', 'Common').
card_artist('moriok reaver'/'SOM', 'Marc Simonetti').
card_number('moriok reaver'/'SOM', '70').
card_flavor_text('moriok reaver'/'SOM', '\"The Moriok are fools. They try so hard to gain my favor. All I want is for them to die quickly, to join the ranks of the nim.\"\n—Geth, Lord of the Vault').
card_multiverse_id('moriok reaver'/'SOM', '194309').
card_watermark('moriok reaver'/'SOM', 'Mirran').

card_in_set('moriok replica', 'SOM').
card_original_type('moriok replica'/'SOM', 'Artifact Creature — Warrior').
card_original_text('moriok replica'/'SOM', '{1}{B}, Sacrifice Moriok Replica: You draw two cards and lose 2 life.').
card_first_print('moriok replica', 'SOM').
card_image_name('moriok replica'/'SOM', 'moriok replica').
card_uid('moriok replica'/'SOM', 'SOM:Moriok Replica:moriok replica').
card_rarity('moriok replica'/'SOM', 'Common').
card_artist('moriok replica'/'SOM', 'Zoltan Boros & Gabor Szikszai').
card_number('moriok replica'/'SOM', '178').
card_flavor_text('moriok replica'/'SOM', 'All the secrets of the Moriok with only a trace of their corruption.').
card_multiverse_id('moriok replica'/'SOM', '209037').
card_watermark('moriok replica'/'SOM', 'Mirran').

card_in_set('mountain', 'SOM').
card_original_type('mountain'/'SOM', 'Basic Land — Mountain').
card_original_text('mountain'/'SOM', 'R').
card_image_name('mountain'/'SOM', 'mountain1').
card_uid('mountain'/'SOM', 'SOM:Mountain:mountain1').
card_rarity('mountain'/'SOM', 'Basic Land').
card_artist('mountain'/'SOM', 'Tomasz Jedruszek').
card_number('mountain'/'SOM', '242').
card_multiverse_id('mountain'/'SOM', '214667').

card_in_set('mountain', 'SOM').
card_original_type('mountain'/'SOM', 'Basic Land — Mountain').
card_original_text('mountain'/'SOM', 'R').
card_image_name('mountain'/'SOM', 'mountain2').
card_uid('mountain'/'SOM', 'SOM:Mountain:mountain2').
card_rarity('mountain'/'SOM', 'Basic Land').
card_artist('mountain'/'SOM', 'Tomasz Jedruszek').
card_number('mountain'/'SOM', '243').
card_multiverse_id('mountain'/'SOM', '214668').

card_in_set('mountain', 'SOM').
card_original_type('mountain'/'SOM', 'Basic Land — Mountain').
card_original_text('mountain'/'SOM', 'R').
card_image_name('mountain'/'SOM', 'mountain3').
card_uid('mountain'/'SOM', 'SOM:Mountain:mountain3').
card_rarity('mountain'/'SOM', 'Basic Land').
card_artist('mountain'/'SOM', 'Tomasz Jedruszek').
card_number('mountain'/'SOM', '244').
card_multiverse_id('mountain'/'SOM', '214675').

card_in_set('mountain', 'SOM').
card_original_type('mountain'/'SOM', 'Basic Land — Mountain').
card_original_text('mountain'/'SOM', 'R').
card_image_name('mountain'/'SOM', 'mountain4').
card_uid('mountain'/'SOM', 'SOM:Mountain:mountain4').
card_rarity('mountain'/'SOM', 'Basic Land').
card_artist('mountain'/'SOM', 'Tomasz Jedruszek').
card_number('mountain'/'SOM', '245').
card_multiverse_id('mountain'/'SOM', '214681').

card_in_set('mox opal', 'SOM').
card_original_type('mox opal'/'SOM', 'Legendary Artifact').
card_original_text('mox opal'/'SOM', 'Metalcraft — {T}: Add one mana of any color to your mana pool. Activate this ability only if you control three or more artifacts.').
card_first_print('mox opal', 'SOM').
card_image_name('mox opal'/'SOM', 'mox opal').
card_uid('mox opal'/'SOM', 'SOM:Mox Opal:mox opal').
card_rarity('mox opal'/'SOM', 'Mythic Rare').
card_artist('mox opal'/'SOM', 'Volkan Baga').
card_number('mox opal'/'SOM', '179').
card_flavor_text('mox opal'/'SOM', 'The suns of Mirrodin have shone upon perfection only once.').
card_multiverse_id('mox opal'/'SOM', '208248').
card_watermark('mox opal'/'SOM', 'Mirran').

card_in_set('myr battlesphere', 'SOM').
card_original_type('myr battlesphere'/'SOM', 'Artifact Creature — Myr Construct').
card_original_text('myr battlesphere'/'SOM', 'When Myr Battlesphere enters the battlefield, put four 1/1 colorless Myr artifact creature tokens onto the battlefield.\nWhenever Myr Battlesphere attacks, you may tap X untapped Myr you control. If you do, Myr Battlesphere gets +X/+0 until end of turn and deals X damage to defending player.').
card_first_print('myr battlesphere', 'SOM').
card_image_name('myr battlesphere'/'SOM', 'myr battlesphere').
card_uid('myr battlesphere'/'SOM', 'SOM:Myr Battlesphere:myr battlesphere').
card_rarity('myr battlesphere'/'SOM', 'Rare').
card_artist('myr battlesphere'/'SOM', 'Franz Vohwinkel').
card_number('myr battlesphere'/'SOM', '180').
card_multiverse_id('myr battlesphere'/'SOM', '209717').
card_watermark('myr battlesphere'/'SOM', 'Mirran').

card_in_set('myr galvanizer', 'SOM').
card_original_type('myr galvanizer'/'SOM', 'Artifact Creature — Myr').
card_original_text('myr galvanizer'/'SOM', 'Other Myr creatures you control get +1/+1.\n{1}, {T}: Untap each other Myr you control.').
card_first_print('myr galvanizer', 'SOM').
card_image_name('myr galvanizer'/'SOM', 'myr galvanizer').
card_uid('myr galvanizer'/'SOM', 'SOM:Myr Galvanizer:myr galvanizer').
card_rarity('myr galvanizer'/'SOM', 'Uncommon').
card_artist('myr galvanizer'/'SOM', 'Greg Staples').
card_number('myr galvanizer'/'SOM', '181').
card_flavor_text('myr galvanizer'/'SOM', '\"We can\'t quite determine what they\'re doing, but they seem to be doing it quite well.\"\n—Vedalken research notes').
card_multiverse_id('myr galvanizer'/'SOM', '220364').
card_watermark('myr galvanizer'/'SOM', 'Mirran').

card_in_set('myr propagator', 'SOM').
card_original_type('myr propagator'/'SOM', 'Artifact Creature — Myr').
card_original_text('myr propagator'/'SOM', '{3}, {T}: Put a token that\'s a copy of Myr Propagator onto the battlefield.').
card_first_print('myr propagator', 'SOM').
card_image_name('myr propagator'/'SOM', 'myr propagator').
card_uid('myr propagator'/'SOM', 'SOM:Myr Propagator:myr propagator').
card_rarity('myr propagator'/'SOM', 'Rare').
card_artist('myr propagator'/'SOM', 'Ryan Pancoast').
card_number('myr propagator'/'SOM', '182').
card_flavor_text('myr propagator'/'SOM', '\"We will ingest and remake this world in our image, not unlike some admirable designs I have seen here.\"\n—Vorinclex, Voice of Hunger').
card_multiverse_id('myr propagator'/'SOM', '215069').
card_watermark('myr propagator'/'SOM', 'Mirran').

card_in_set('myr reservoir', 'SOM').
card_original_type('myr reservoir'/'SOM', 'Artifact').
card_original_text('myr reservoir'/'SOM', '{T}: Add {2} to your mana pool. Spend this mana only to cast Myr spells or activate abilities of Myr.\n{3}, {T}: Return target Myr card from your graveyard to your hand.').
card_first_print('myr reservoir', 'SOM').
card_image_name('myr reservoir'/'SOM', 'myr reservoir').
card_uid('myr reservoir'/'SOM', 'SOM:Myr Reservoir:myr reservoir').
card_rarity('myr reservoir'/'SOM', 'Rare').
card_artist('myr reservoir'/'SOM', 'Jung Park').
card_number('myr reservoir'/'SOM', '183').
card_flavor_text('myr reservoir'/'SOM', 'A mana well as bottomless as the myr are tireless.').
card_multiverse_id('myr reservoir'/'SOM', '215114').
card_watermark('myr reservoir'/'SOM', 'Mirran').

card_in_set('myrsmith', 'SOM').
card_original_type('myrsmith'/'SOM', 'Creature — Human Artificer').
card_original_text('myrsmith'/'SOM', 'Whenever you cast an artifact spell, you may pay {1}. If you do, put a 1/1 colorless Myr artifact creature token onto the battlefield.').
card_first_print('myrsmith', 'SOM').
card_image_name('myrsmith'/'SOM', 'myrsmith').
card_uid('myrsmith'/'SOM', 'SOM:Myrsmith:myrsmith').
card_rarity('myrsmith'/'SOM', 'Uncommon').
card_artist('myrsmith'/'SOM', 'Eric Deschamps').
card_number('myrsmith'/'SOM', '16').
card_flavor_text('myrsmith'/'SOM', 'The Auriok see the artificer as a conduit, beckoning new creations into the world.').
card_multiverse_id('myrsmith'/'SOM', '209712').
card_watermark('myrsmith'/'SOM', 'Mirran').

card_in_set('necrogen censer', 'SOM').
card_original_type('necrogen censer'/'SOM', 'Artifact').
card_original_text('necrogen censer'/'SOM', 'Necrogen Censer enters the battlefield with two charge counters on it.\n{T}, Remove a charge counter from Necrogen Censer: Target player loses 2 life.').
card_first_print('necrogen censer', 'SOM').
card_image_name('necrogen censer'/'SOM', 'necrogen censer').
card_uid('necrogen censer'/'SOM', 'SOM:Necrogen Censer:necrogen censer').
card_rarity('necrogen censer'/'SOM', 'Common').
card_artist('necrogen censer'/'SOM', 'Pete Venters').
card_number('necrogen censer'/'SOM', '184').
card_flavor_text('necrogen censer'/'SOM', 'Moriok necromancers actively spread necrogen, encouraging the transformation from Mirran to nim.').
card_multiverse_id('necrogen censer'/'SOM', '194223').
card_watermark('necrogen censer'/'SOM', 'Mirran').

card_in_set('necrogen scudder', 'SOM').
card_original_type('necrogen scudder'/'SOM', 'Creature — Horror').
card_original_text('necrogen scudder'/'SOM', 'Flying\nWhen Necrogen Scudder enters the battlefield, you lose 3 life.').
card_first_print('necrogen scudder', 'SOM').
card_image_name('necrogen scudder'/'SOM', 'necrogen scudder').
card_uid('necrogen scudder'/'SOM', 'SOM:Necrogen Scudder:necrogen scudder').
card_rarity('necrogen scudder'/'SOM', 'Uncommon').
card_artist('necrogen scudder'/'SOM', 'Raymond Swanland').
card_number('necrogen scudder'/'SOM', '71').
card_flavor_text('necrogen scudder'/'SOM', 'Contrary to popular belief, it\'s kept aloft by necrogen gas, not the screaming agony of a thousand murdered souls.').
card_multiverse_id('necrogen scudder'/'SOM', '202653').
card_watermark('necrogen scudder'/'SOM', 'Phyrexian').

card_in_set('necropede', 'SOM').
card_original_type('necropede'/'SOM', 'Artifact Creature — Insect').
card_original_text('necropede'/'SOM', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nWhen Necropede is put into a graveyard from the battlefield, you may put a -1/-1 counter on target creature.').
card_first_print('necropede', 'SOM').
card_image_name('necropede'/'SOM', 'necropede').
card_uid('necropede'/'SOM', 'SOM:Necropede:necropede').
card_rarity('necropede'/'SOM', 'Uncommon').
card_artist('necropede'/'SOM', 'Nic Klein').
card_number('necropede'/'SOM', '185').
card_multiverse_id('necropede'/'SOM', '194052').
card_watermark('necropede'/'SOM', 'Phyrexian').

card_in_set('necrotic ooze', 'SOM').
card_original_type('necrotic ooze'/'SOM', 'Creature — Ooze').
card_original_text('necrotic ooze'/'SOM', 'As long as Necrotic Ooze is on the battlefield, it has all activated abilities of all creature cards in all graveyards.').
card_first_print('necrotic ooze', 'SOM').
card_image_name('necrotic ooze'/'SOM', 'necrotic ooze').
card_uid('necrotic ooze'/'SOM', 'SOM:Necrotic Ooze:necrotic ooze').
card_rarity('necrotic ooze'/'SOM', 'Rare').
card_artist('necrotic ooze'/'SOM', 'James Ryman').
card_number('necrotic ooze'/'SOM', '72').
card_flavor_text('necrotic ooze'/'SOM', '\"To death all must go, and so in death lies ultimate power.\"\n—Geth, Lord of the Vault').
card_multiverse_id('necrotic ooze'/'SOM', '207876').
card_watermark('necrotic ooze'/'SOM', 'Mirran').

card_in_set('neurok invisimancer', 'SOM').
card_original_type('neurok invisimancer'/'SOM', 'Creature — Human Wizard').
card_original_text('neurok invisimancer'/'SOM', 'Neurok Invisimancer is unblockable.\nWhen Neurok Invisimancer enters the battlefield, target creature is unblockable this turn.').
card_first_print('neurok invisimancer', 'SOM').
card_image_name('neurok invisimancer'/'SOM', 'neurok invisimancer').
card_uid('neurok invisimancer'/'SOM', 'SOM:Neurok Invisimancer:neurok invisimancer').
card_rarity('neurok invisimancer'/'SOM', 'Common').
card_artist('neurok invisimancer'/'SOM', 'Izzy').
card_number('neurok invisimancer'/'SOM', '37').
card_flavor_text('neurok invisimancer'/'SOM', '\"They won\'t see your shadow or hear your breath, but they will feel your blade.\"').
card_multiverse_id('neurok invisimancer'/'SOM', '194109').
card_watermark('neurok invisimancer'/'SOM', 'Mirran').

card_in_set('neurok replica', 'SOM').
card_original_type('neurok replica'/'SOM', 'Artifact Creature — Wizard').
card_original_text('neurok replica'/'SOM', '{1}{U}, Sacrifice Neurok Replica: Return target creature to its owner\'s hand.').
card_first_print('neurok replica', 'SOM').
card_image_name('neurok replica'/'SOM', 'neurok replica').
card_uid('neurok replica'/'SOM', 'SOM:Neurok Replica:neurok replica').
card_rarity('neurok replica'/'SOM', 'Common').
card_artist('neurok replica'/'SOM', 'Zoltan Boros & Gabor Szikszai').
card_number('neurok replica'/'SOM', '186').
card_flavor_text('neurok replica'/'SOM', 'All the curiosity of the Neurok with only a trace of their duplicity.').
card_multiverse_id('neurok replica'/'SOM', '209039').
card_watermark('neurok replica'/'SOM', 'Mirran').

card_in_set('nihil spellbomb', 'SOM').
card_original_type('nihil spellbomb'/'SOM', 'Artifact').
card_original_text('nihil spellbomb'/'SOM', '{T}, Sacrifice Nihil Spellbomb: Exile all cards from target player\'s graveyard.\nWhen Nihil Spellbomb is put into a graveyard from the battlefield, you may pay {B}. If you do, draw a card.').
card_first_print('nihil spellbomb', 'SOM').
card_image_name('nihil spellbomb'/'SOM', 'nihil spellbomb').
card_uid('nihil spellbomb'/'SOM', 'SOM:Nihil Spellbomb:nihil spellbomb').
card_rarity('nihil spellbomb'/'SOM', 'Common').
card_artist('nihil spellbomb'/'SOM', 'Franz Vohwinkel').
card_number('nihil spellbomb'/'SOM', '187').
card_multiverse_id('nihil spellbomb'/'SOM', '210226').
card_watermark('nihil spellbomb'/'SOM', 'Mirran').

card_in_set('nim deathmantle', 'SOM').
card_original_type('nim deathmantle'/'SOM', 'Artifact — Equipment').
card_original_text('nim deathmantle'/'SOM', 'Equipped creature gets +2/+2, has intimidate, and is a black Zombie.\nWhenever a nontoken creature is put into your graveyard from the battlefield, you may pay {4}. If you do, return that card to the battlefield and attach Nim Deathmantle to it.\nEquip {4}').
card_first_print('nim deathmantle', 'SOM').
card_image_name('nim deathmantle'/'SOM', 'nim deathmantle').
card_uid('nim deathmantle'/'SOM', 'SOM:Nim Deathmantle:nim deathmantle').
card_rarity('nim deathmantle'/'SOM', 'Rare').
card_artist('nim deathmantle'/'SOM', 'Karl Kopinski').
card_number('nim deathmantle'/'SOM', '188').
card_multiverse_id('nim deathmantle'/'SOM', '209289').
card_watermark('nim deathmantle'/'SOM', 'Mirran').

card_in_set('ogre geargrabber', 'SOM').
card_original_type('ogre geargrabber'/'SOM', 'Creature — Ogre Warrior').
card_original_text('ogre geargrabber'/'SOM', 'Whenever Ogre Geargrabber attacks, gain control of target Equipment an opponent controls until end of turn. Attach it to Ogre Geargrabber. When you lose control of that Equipment, unattach it.').
card_first_print('ogre geargrabber', 'SOM').
card_image_name('ogre geargrabber'/'SOM', 'ogre geargrabber').
card_uid('ogre geargrabber'/'SOM', 'SOM:Ogre Geargrabber:ogre geargrabber').
card_rarity('ogre geargrabber'/'SOM', 'Uncommon').
card_artist('ogre geargrabber'/'SOM', 'David Rapoza').
card_number('ogre geargrabber'/'SOM', '99').
card_flavor_text('ogre geargrabber'/'SOM', 'Long reach, short attention span.').
card_multiverse_id('ogre geargrabber'/'SOM', '215101').
card_watermark('ogre geargrabber'/'SOM', 'Mirran').

card_in_set('origin spellbomb', 'SOM').
card_original_type('origin spellbomb'/'SOM', 'Artifact').
card_original_text('origin spellbomb'/'SOM', '{1}, {T}, Sacrifice Origin Spellbomb: Put a 1/1 colorless Myr artifact creature token onto the battlefield.\nWhen Origin Spellbomb is put into a graveyard from the battlefield, you may pay {W}. If you do, draw a card.').
card_first_print('origin spellbomb', 'SOM').
card_image_name('origin spellbomb'/'SOM', 'origin spellbomb').
card_uid('origin spellbomb'/'SOM', 'SOM:Origin Spellbomb:origin spellbomb').
card_rarity('origin spellbomb'/'SOM', 'Common').
card_artist('origin spellbomb'/'SOM', 'Franz Vohwinkel').
card_number('origin spellbomb'/'SOM', '189').
card_multiverse_id('origin spellbomb'/'SOM', '210239').
card_watermark('origin spellbomb'/'SOM', 'Mirran').

card_in_set('oxidda daredevil', 'SOM').
card_original_type('oxidda daredevil'/'SOM', 'Creature — Goblin Artificer').
card_original_text('oxidda daredevil'/'SOM', 'Sacrifice an artifact: Oxidda Daredevil gains haste until end of turn.').
card_first_print('oxidda daredevil', 'SOM').
card_image_name('oxidda daredevil'/'SOM', 'oxidda daredevil').
card_uid('oxidda daredevil'/'SOM', 'SOM:Oxidda Daredevil:oxidda daredevil').
card_rarity('oxidda daredevil'/'SOM', 'Common').
card_artist('oxidda daredevil'/'SOM', 'Pete Venters').
card_number('oxidda daredevil'/'SOM', '100').
card_flavor_text('oxidda daredevil'/'SOM', 'His goggles spattered with grime and his mouth full of bugs, he tossed the engines another priceless relic.').
card_multiverse_id('oxidda daredevil'/'SOM', '194213').
card_watermark('oxidda daredevil'/'SOM', 'Mirran').

card_in_set('oxidda scrapmelter', 'SOM').
card_original_type('oxidda scrapmelter'/'SOM', 'Creature — Beast').
card_original_text('oxidda scrapmelter'/'SOM', 'When Oxidda Scrapmelter enters the battlefield, destroy target artifact.').
card_first_print('oxidda scrapmelter', 'SOM').
card_image_name('oxidda scrapmelter'/'SOM', 'oxidda scrapmelter').
card_uid('oxidda scrapmelter'/'SOM', 'SOM:Oxidda Scrapmelter:oxidda scrapmelter').
card_rarity('oxidda scrapmelter'/'SOM', 'Uncommon').
card_artist('oxidda scrapmelter'/'SOM', 'Igor Kieryluk').
card_number('oxidda scrapmelter'/'SOM', '101').
card_flavor_text('oxidda scrapmelter'/'SOM', 'It subsists on a diet rich in screaming metal and molten blood.').
card_multiverse_id('oxidda scrapmelter'/'SOM', '194281').
card_watermark('oxidda scrapmelter'/'SOM', 'Mirran').

card_in_set('painful quandary', 'SOM').
card_original_type('painful quandary'/'SOM', 'Enchantment').
card_original_text('painful quandary'/'SOM', 'Whenever an opponent casts a spell, that player loses 5 life unless he or she discards a card.').
card_first_print('painful quandary', 'SOM').
card_image_name('painful quandary'/'SOM', 'painful quandary').
card_uid('painful quandary'/'SOM', 'SOM:Painful Quandary:painful quandary').
card_rarity('painful quandary'/'SOM', 'Rare').
card_artist('painful quandary'/'SOM', 'Whit Brachna').
card_number('painful quandary'/'SOM', '73').
card_flavor_text('painful quandary'/'SOM', '\"For each word spoken, one forgotten. For each thought, a memory rotten.\"\n—Moriok incantation').
card_multiverse_id('painful quandary'/'SOM', '212243').
card_watermark('painful quandary'/'SOM', 'Mirran').

card_in_set('painsmith', 'SOM').
card_original_type('painsmith'/'SOM', 'Creature — Human Artificer').
card_original_text('painsmith'/'SOM', 'Whenever you cast an artifact spell, you may have target creature get +2/+0 and gain deathtouch until end of turn.').
card_first_print('painsmith', 'SOM').
card_image_name('painsmith'/'SOM', 'painsmith').
card_uid('painsmith'/'SOM', 'SOM:Painsmith:painsmith').
card_rarity('painsmith'/'SOM', 'Uncommon').
card_artist('painsmith'/'SOM', 'Eric Deschamps').
card_number('painsmith'/'SOM', '74').
card_flavor_text('painsmith'/'SOM', 'The Moriok see the artificer as a taskmaster, forcing lazy metal into more potent shapes.').
card_multiverse_id('painsmith'/'SOM', '194210').
card_watermark('painsmith'/'SOM', 'Mirran').

card_in_set('palladium myr', 'SOM').
card_original_type('palladium myr'/'SOM', 'Artifact Creature — Myr').
card_original_text('palladium myr'/'SOM', '{T}: Add {2} to your mana pool.').
card_first_print('palladium myr', 'SOM').
card_image_name('palladium myr'/'SOM', 'palladium myr').
card_uid('palladium myr'/'SOM', 'SOM:Palladium Myr:palladium myr').
card_rarity('palladium myr'/'SOM', 'Uncommon').
card_artist('palladium myr'/'SOM', 'Alan Pollack').
card_number('palladium myr'/'SOM', '190').
card_flavor_text('palladium myr'/'SOM', 'The myr are like the Glimmervoid: blank canvases on which to build grand creations.').
card_multiverse_id('palladium myr'/'SOM', '212251').
card_watermark('palladium myr'/'SOM', 'Mirran').

card_in_set('panic spellbomb', 'SOM').
card_original_type('panic spellbomb'/'SOM', 'Artifact').
card_original_text('panic spellbomb'/'SOM', '{T}, Sacrifice Panic Spellbomb: Target creature can\'t block this turn.\nWhen Panic Spellbomb is put into a graveyard from the battlefield, you may pay {R}. If you do, draw a card.').
card_first_print('panic spellbomb', 'SOM').
card_image_name('panic spellbomb'/'SOM', 'panic spellbomb').
card_uid('panic spellbomb'/'SOM', 'SOM:Panic Spellbomb:panic spellbomb').
card_rarity('panic spellbomb'/'SOM', 'Common').
card_artist('panic spellbomb'/'SOM', 'Franz Vohwinkel').
card_number('panic spellbomb'/'SOM', '191').
card_multiverse_id('panic spellbomb'/'SOM', '210234').
card_watermark('panic spellbomb'/'SOM', 'Mirran').

card_in_set('perilous myr', 'SOM').
card_original_type('perilous myr'/'SOM', 'Artifact Creature — Myr').
card_original_text('perilous myr'/'SOM', 'When Perilous Myr is put into a graveyard from the battlefield, it deals 2 damage to target creature or player.').
card_first_print('perilous myr', 'SOM').
card_image_name('perilous myr'/'SOM', 'perilous myr').
card_uid('perilous myr'/'SOM', 'SOM:Perilous Myr:perilous myr').
card_rarity('perilous myr'/'SOM', 'Common').
card_artist('perilous myr'/'SOM', 'Jason Felix').
card_number('perilous myr'/'SOM', '192').
card_flavor_text('perilous myr'/'SOM', 'To Mirrodin, an explosive. To Phyrexia, a missionary.').
card_multiverse_id('perilous myr'/'SOM', '194056').
card_watermark('perilous myr'/'SOM', 'Phyrexian').

card_in_set('plague stinger', 'SOM').
card_original_type('plague stinger'/'SOM', 'Creature — Insect Horror').
card_original_text('plague stinger'/'SOM', 'Flying\nInfect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_image_name('plague stinger'/'SOM', 'plague stinger').
card_uid('plague stinger'/'SOM', 'SOM:Plague Stinger:plague stinger').
card_rarity('plague stinger'/'SOM', 'Common').
card_artist('plague stinger'/'SOM', 'Ryan Pancoast').
card_number('plague stinger'/'SOM', '75').
card_flavor_text('plague stinger'/'SOM', 'It leaves its victims one sting closer to phyresis.').
card_multiverse_id('plague stinger'/'SOM', '208261').
card_watermark('plague stinger'/'SOM', 'Phyrexian').

card_in_set('plains', 'SOM').
card_original_type('plains'/'SOM', 'Basic Land — Plains').
card_original_text('plains'/'SOM', 'W').
card_image_name('plains'/'SOM', 'plains1').
card_uid('plains'/'SOM', 'SOM:Plains:plains1').
card_rarity('plains'/'SOM', 'Basic Land').
card_artist('plains'/'SOM', 'James Paick').
card_number('plains'/'SOM', '230').
card_multiverse_id('plains'/'SOM', '214677').

card_in_set('plains', 'SOM').
card_original_type('plains'/'SOM', 'Basic Land — Plains').
card_original_text('plains'/'SOM', 'W').
card_image_name('plains'/'SOM', 'plains2').
card_uid('plains'/'SOM', 'SOM:Plains:plains2').
card_rarity('plains'/'SOM', 'Basic Land').
card_artist('plains'/'SOM', 'James Paick').
card_number('plains'/'SOM', '231').
card_multiverse_id('plains'/'SOM', '214679').

card_in_set('plains', 'SOM').
card_original_type('plains'/'SOM', 'Basic Land — Plains').
card_original_text('plains'/'SOM', 'W').
card_image_name('plains'/'SOM', 'plains3').
card_uid('plains'/'SOM', 'SOM:Plains:plains3').
card_rarity('plains'/'SOM', 'Basic Land').
card_artist('plains'/'SOM', 'James Paick').
card_number('plains'/'SOM', '232').
card_multiverse_id('plains'/'SOM', '214671').

card_in_set('plains', 'SOM').
card_original_type('plains'/'SOM', 'Basic Land — Plains').
card_original_text('plains'/'SOM', 'W').
card_image_name('plains'/'SOM', 'plains4').
card_uid('plains'/'SOM', 'SOM:Plains:plains4').
card_rarity('plains'/'SOM', 'Basic Land').
card_artist('plains'/'SOM', 'James Paick').
card_number('plains'/'SOM', '233').
card_multiverse_id('plains'/'SOM', '214683').

card_in_set('plated seastrider', 'SOM').
card_original_type('plated seastrider'/'SOM', 'Creature — Beast').
card_original_text('plated seastrider'/'SOM', '').
card_first_print('plated seastrider', 'SOM').
card_image_name('plated seastrider'/'SOM', 'plated seastrider').
card_uid('plated seastrider'/'SOM', 'SOM:Plated Seastrider:plated seastrider').
card_rarity('plated seastrider'/'SOM', 'Common').
card_artist('plated seastrider'/'SOM', 'Izzy').
card_number('plated seastrider'/'SOM', '38').
card_flavor_text('plated seastrider'/'SOM', 'The Neurok buried entangling cables just under the Quicksilver Sea. One seastrider harvest can provide an army\'s worth of armor and shields.').
card_multiverse_id('plated seastrider'/'SOM', '194061').
card_watermark('plated seastrider'/'SOM', 'Mirran').

card_in_set('platinum emperion', 'SOM').
card_original_type('platinum emperion'/'SOM', 'Artifact Creature — Golem').
card_original_text('platinum emperion'/'SOM', 'Your life total can\'t change. (You can\'t gain or lose life. You can\'t pay any amount of life except 0.)').
card_first_print('platinum emperion', 'SOM').
card_image_name('platinum emperion'/'SOM', 'platinum emperion').
card_uid('platinum emperion'/'SOM', 'SOM:Platinum Emperion:platinum emperion').
card_rarity('platinum emperion'/'SOM', 'Mythic Rare').
card_artist('platinum emperion'/'SOM', 'Chris Rahn').
card_number('platinum emperion'/'SOM', '193').
card_flavor_text('platinum emperion'/'SOM', 'It ignores such insignificant forces as time, entropy, and death.').
card_multiverse_id('platinum emperion'/'SOM', '215092').
card_watermark('platinum emperion'/'SOM', 'Mirran').

card_in_set('precursor golem', 'SOM').
card_original_type('precursor golem'/'SOM', 'Artifact Creature — Golem').
card_original_text('precursor golem'/'SOM', 'When Precursor Golem enters the battlefield, put two 3/3 colorless Golem artifact creature tokens onto the battlefield.\nWhenever a player casts an instant or sorcery spell that targets only a single Golem, that player copies that spell for each other Golem that spell could target. Each copy targets a different one of those Golems.').
card_first_print('precursor golem', 'SOM').
card_image_name('precursor golem'/'SOM', 'precursor golem').
card_uid('precursor golem'/'SOM', 'SOM:Precursor Golem:precursor golem').
card_rarity('precursor golem'/'SOM', 'Rare').
card_artist('precursor golem'/'SOM', 'Chippy').
card_number('precursor golem'/'SOM', '194').
card_multiverse_id('precursor golem'/'SOM', '206348').
card_watermark('precursor golem'/'SOM', 'Mirran').

card_in_set('prototype portal', 'SOM').
card_original_type('prototype portal'/'SOM', 'Artifact').
card_original_text('prototype portal'/'SOM', 'Imprint — When Prototype Portal enters the battlefield, you may exile an artifact card from your hand.\n{X}, {T}: Put a token that\'s a copy of the exiled card onto the battlefield. X is the converted mana cost of that card.').
card_first_print('prototype portal', 'SOM').
card_image_name('prototype portal'/'SOM', 'prototype portal').
card_uid('prototype portal'/'SOM', 'SOM:Prototype Portal:prototype portal').
card_rarity('prototype portal'/'SOM', 'Rare').
card_artist('prototype portal'/'SOM', 'Drew Baker').
card_number('prototype portal'/'SOM', '195').
card_multiverse_id('prototype portal'/'SOM', '209009').
card_watermark('prototype portal'/'SOM', 'Mirran').

card_in_set('psychic miasma', 'SOM').
card_original_type('psychic miasma'/'SOM', 'Sorcery').
card_original_text('psychic miasma'/'SOM', 'Target player discards a card. If a land card is discarded this way, return Psychic Miasma to its owner\'s hand.').
card_first_print('psychic miasma', 'SOM').
card_image_name('psychic miasma'/'SOM', 'psychic miasma').
card_uid('psychic miasma'/'SOM', 'SOM:Psychic Miasma:psychic miasma').
card_rarity('psychic miasma'/'SOM', 'Common').
card_artist('psychic miasma'/'SOM', 'Svetlin Velinov').
card_number('psychic miasma'/'SOM', '76').
card_flavor_text('psychic miasma'/'SOM', '\"A mind may be steeled against madness, but all battlements fall in time.\"\n—Jin-Gitaxias, Core Augur').
card_multiverse_id('psychic miasma'/'SOM', '206345').
card_watermark('psychic miasma'/'SOM', 'Mirran').

card_in_set('putrefax', 'SOM').
card_original_type('putrefax'/'SOM', 'Creature — Horror').
card_original_text('putrefax'/'SOM', 'Trample, haste\nInfect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nAt the beginning of the end step, sacrifice Putrefax.').
card_first_print('putrefax', 'SOM').
card_image_name('putrefax'/'SOM', 'putrefax').
card_uid('putrefax'/'SOM', 'SOM:Putrefax:putrefax').
card_rarity('putrefax'/'SOM', 'Rare').
card_artist('putrefax'/'SOM', 'Steven Belledin').
card_number('putrefax'/'SOM', '126').
card_multiverse_id('putrefax'/'SOM', '207871').
card_watermark('putrefax'/'SOM', 'Phyrexian').

card_in_set('quicksilver gargantuan', 'SOM').
card_original_type('quicksilver gargantuan'/'SOM', 'Creature — Shapeshifter').
card_original_text('quicksilver gargantuan'/'SOM', 'You may have Quicksilver Gargantuan enter the battlefield as a copy of any creature on the battlefield, except it\'s still 7/7.').
card_first_print('quicksilver gargantuan', 'SOM').
card_image_name('quicksilver gargantuan'/'SOM', 'quicksilver gargantuan').
card_uid('quicksilver gargantuan'/'SOM', 'SOM:Quicksilver Gargantuan:quicksilver gargantuan').
card_rarity('quicksilver gargantuan'/'SOM', 'Mythic Rare').
card_artist('quicksilver gargantuan'/'SOM', 'Steven Belledin').
card_number('quicksilver gargantuan'/'SOM', '39').
card_flavor_text('quicksilver gargantuan'/'SOM', '\"You know you have achieved true power if the sea itself seeks to emulate your greatness.\"\n—Vy Covalt, Neurok agent').
card_multiverse_id('quicksilver gargantuan'/'SOM', '207874').
card_watermark('quicksilver gargantuan'/'SOM', 'Mirran').

card_in_set('ratchet bomb', 'SOM').
card_original_type('ratchet bomb'/'SOM', 'Artifact').
card_original_text('ratchet bomb'/'SOM', '{T}: Put a charge counter on Ratchet Bomb.\n{T}, Sacrifice Ratchet Bomb: Destroy each nonland permanent with converted mana cost equal to the number of charge counters on Ratchet Bomb.').
card_image_name('ratchet bomb'/'SOM', 'ratchet bomb').
card_uid('ratchet bomb'/'SOM', 'SOM:Ratchet Bomb:ratchet bomb').
card_rarity('ratchet bomb'/'SOM', 'Rare').
card_artist('ratchet bomb'/'SOM', 'Austin Hsu').
card_number('ratchet bomb'/'SOM', '196').
card_multiverse_id('ratchet bomb'/'SOM', '205482').
card_watermark('ratchet bomb'/'SOM', 'Mirran').

card_in_set('razor hippogriff', 'SOM').
card_original_type('razor hippogriff'/'SOM', 'Creature — Hippogriff').
card_original_text('razor hippogriff'/'SOM', 'Flying\nWhen Razor Hippogriff enters the battlefield, return target artifact card from your graveyard to your hand. You gain life equal to that card\'s converted mana cost.').
card_first_print('razor hippogriff', 'SOM').
card_image_name('razor hippogriff'/'SOM', 'razor hippogriff').
card_uid('razor hippogriff'/'SOM', 'SOM:Razor Hippogriff:razor hippogriff').
card_rarity('razor hippogriff'/'SOM', 'Uncommon').
card_artist('razor hippogriff'/'SOM', 'David Rapoza').
card_number('razor hippogriff'/'SOM', '17').
card_flavor_text('razor hippogriff'/'SOM', 'She incubates her eggs in gold and mana.').
card_multiverse_id('razor hippogriff'/'SOM', '194323').
card_watermark('razor hippogriff'/'SOM', 'Mirran').

card_in_set('razorfield thresher', 'SOM').
card_original_type('razorfield thresher'/'SOM', 'Artifact Creature — Construct').
card_original_text('razorfield thresher'/'SOM', '').
card_first_print('razorfield thresher', 'SOM').
card_image_name('razorfield thresher'/'SOM', 'razorfield thresher').
card_uid('razorfield thresher'/'SOM', 'SOM:Razorfield Thresher:razorfield thresher').
card_rarity('razorfield thresher'/'SOM', 'Common').
card_artist('razorfield thresher'/'SOM', 'Karl Kopinski').
card_number('razorfield thresher'/'SOM', '197').
card_flavor_text('razorfield thresher'/'SOM', 'DANGER! Keep appendages clear of front of machine. And rear of machine. And side of machine. And top of machine.').
card_multiverse_id('razorfield thresher'/'SOM', '209725').
card_watermark('razorfield thresher'/'SOM', 'Mirran').

card_in_set('razorverge thicket', 'SOM').
card_original_type('razorverge thicket'/'SOM', 'Land').
card_original_text('razorverge thicket'/'SOM', 'Razorverge Thicket enters the battlefield tapped unless you control two or fewer other lands.\n{T}: Add {G} or {W} to your mana pool.').
card_first_print('razorverge thicket', 'SOM').
card_image_name('razorverge thicket'/'SOM', 'razorverge thicket').
card_uid('razorverge thicket'/'SOM', 'SOM:Razorverge Thicket:razorverge thicket').
card_rarity('razorverge thicket'/'SOM', 'Rare').
card_artist('razorverge thicket'/'SOM', 'James Paick').
card_number('razorverge thicket'/'SOM', '228').
card_flavor_text('razorverge thicket'/'SOM', 'Where the Razor Fields beat back the Tangle, the crowded thicket yields to bright scimitars of grass.').
card_multiverse_id('razorverge thicket'/'SOM', '209407').
card_watermark('razorverge thicket'/'SOM', 'Mirran').

card_in_set('relic putrescence', 'SOM').
card_original_type('relic putrescence'/'SOM', 'Enchantment — Aura').
card_original_text('relic putrescence'/'SOM', 'Enchant artifact\nWhenever enchanted artifact becomes tapped, its controller gets a poison counter.').
card_first_print('relic putrescence', 'SOM').
card_image_name('relic putrescence'/'SOM', 'relic putrescence').
card_uid('relic putrescence'/'SOM', 'SOM:Relic Putrescence:relic putrescence').
card_rarity('relic putrescence'/'SOM', 'Common').
card_artist('relic putrescence'/'SOM', 'Allen Williams').
card_number('relic putrescence'/'SOM', '77').
card_flavor_text('relic putrescence'/'SOM', '\"Until we shed the curse of sentience, we must endure its grotesque handiwork.\"\n—Vorinclex, Voice of Hunger').
card_multiverse_id('relic putrescence'/'SOM', '209720').
card_watermark('relic putrescence'/'SOM', 'Phyrexian').

card_in_set('revoke existence', 'SOM').
card_original_type('revoke existence'/'SOM', 'Sorcery').
card_original_text('revoke existence'/'SOM', 'Exile target artifact or enchantment.').
card_first_print('revoke existence', 'SOM').
card_image_name('revoke existence'/'SOM', 'revoke existence').
card_uid('revoke existence'/'SOM', 'SOM:Revoke Existence:revoke existence').
card_rarity('revoke existence'/'SOM', 'Common').
card_artist('revoke existence'/'SOM', 'Allen Williams').
card_number('revoke existence'/'SOM', '18').
card_flavor_text('revoke existence'/'SOM', '\"No half measures, no regrets. We\'ll tell no stories of this day. It will be as if it never existed at all.\"\n—Ganedor, loxodon mystic').
card_multiverse_id('revoke existence'/'SOM', '194123').
card_watermark('revoke existence'/'SOM', 'Mirran').

card_in_set('riddlesmith', 'SOM').
card_original_type('riddlesmith'/'SOM', 'Creature — Human Artificer').
card_original_text('riddlesmith'/'SOM', 'Whenever you cast an artifact spell, you may draw a card. If you do, discard a card.').
card_first_print('riddlesmith', 'SOM').
card_image_name('riddlesmith'/'SOM', 'riddlesmith').
card_uid('riddlesmith'/'SOM', 'SOM:Riddlesmith:riddlesmith').
card_rarity('riddlesmith'/'SOM', 'Uncommon').
card_artist('riddlesmith'/'SOM', 'Eric Deschamps').
card_number('riddlesmith'/'SOM', '40').
card_flavor_text('riddlesmith'/'SOM', 'The Neurok see the artificer as an archaeologist, clearing away falsehoods to see the truth hidden beneath.').
card_multiverse_id('riddlesmith'/'SOM', '202643').
card_watermark('riddlesmith'/'SOM', 'Mirran').

card_in_set('rust tick', 'SOM').
card_original_type('rust tick'/'SOM', 'Artifact Creature — Insect').
card_original_text('rust tick'/'SOM', 'You may choose not to untap Rust Tick during your untap step.\n{1}, {T}: Tap target artifact. It doesn\'t untap during its controller\'s untap step for as long as Rust Tick remains tapped.').
card_first_print('rust tick', 'SOM').
card_image_name('rust tick'/'SOM', 'rust tick').
card_uid('rust tick'/'SOM', 'SOM:Rust Tick:rust tick').
card_rarity('rust tick'/'SOM', 'Uncommon').
card_artist('rust tick'/'SOM', 'Carl Critchlow').
card_number('rust tick'/'SOM', '198').
card_multiverse_id('rust tick'/'SOM', '209282').
card_watermark('rust tick'/'SOM', 'Mirran').

card_in_set('rusted relic', 'SOM').
card_original_type('rusted relic'/'SOM', 'Artifact').
card_original_text('rusted relic'/'SOM', 'Metalcraft — Rusted Relic is a 5/5 Golem artifact creature as long as you control three or more artifacts.').
card_first_print('rusted relic', 'SOM').
card_image_name('rusted relic'/'SOM', 'rusted relic').
card_uid('rusted relic'/'SOM', 'SOM:Rusted Relic:rusted relic').
card_rarity('rusted relic'/'SOM', 'Uncommon').
card_artist('rusted relic'/'SOM', 'Igor Kieryluk').
card_number('rusted relic'/'SOM', '199').
card_flavor_text('rusted relic'/'SOM', '\"We consider rust a curable disease.\"\n—Tarrin, Hammer-Tribe shaman').
card_multiverse_id('rusted relic'/'SOM', '194069').
card_watermark('rusted relic'/'SOM', 'Mirran').

card_in_set('saberclaw golem', 'SOM').
card_original_type('saberclaw golem'/'SOM', 'Artifact Creature — Golem').
card_original_text('saberclaw golem'/'SOM', '{R}: Saberclaw Golem gains first strike until end of turn.').
card_first_print('saberclaw golem', 'SOM').
card_image_name('saberclaw golem'/'SOM', 'saberclaw golem').
card_uid('saberclaw golem'/'SOM', 'SOM:Saberclaw Golem:saberclaw golem').
card_rarity('saberclaw golem'/'SOM', 'Common').
card_artist('saberclaw golem'/'SOM', 'Mike Bierek').
card_number('saberclaw golem'/'SOM', '200').
card_flavor_text('saberclaw golem'/'SOM', 'The warriors of the Blade Tribe charged the golem, twenty strong. They returned numbering ten . . . and a half.').
card_multiverse_id('saberclaw golem'/'SOM', '212717').
card_watermark('saberclaw golem'/'SOM', 'Mirran').

card_in_set('salvage scout', 'SOM').
card_original_type('salvage scout'/'SOM', 'Creature — Human Scout').
card_original_text('salvage scout'/'SOM', '{W}, Sacrifice Salvage Scout: Return target artifact card from your graveyard to your hand.').
card_first_print('salvage scout', 'SOM').
card_image_name('salvage scout'/'SOM', 'salvage scout').
card_uid('salvage scout'/'SOM', 'SOM:Salvage Scout:salvage scout').
card_rarity('salvage scout'/'SOM', 'Common').
card_artist('salvage scout'/'SOM', 'Randis Albion').
card_number('salvage scout'/'SOM', '19').
card_flavor_text('salvage scout'/'SOM', '\"I\'m not saying it\'s dangerous work. I\'m just saying don\'t sign up if you have plans for your seventieth birthday.\"').
card_multiverse_id('salvage scout'/'SOM', '205491').
card_watermark('salvage scout'/'SOM', 'Mirran').

card_in_set('scoria elemental', 'SOM').
card_original_type('scoria elemental'/'SOM', 'Creature — Elemental').
card_original_text('scoria elemental'/'SOM', '').
card_first_print('scoria elemental', 'SOM').
card_image_name('scoria elemental'/'SOM', 'scoria elemental').
card_uid('scoria elemental'/'SOM', 'SOM:Scoria Elemental:scoria elemental').
card_rarity('scoria elemental'/'SOM', 'Common').
card_artist('scoria elemental'/'SOM', 'Karl Kopinski').
card_number('scoria elemental'/'SOM', '102').
card_flavor_text('scoria elemental'/'SOM', 'A single molten cord links it to the subterranean furnaces, drawing heat and metal from beneath Kuldotha. Until that bond is cut, it carves a swath of raw destruction.').
card_multiverse_id('scoria elemental'/'SOM', '212710').
card_watermark('scoria elemental'/'SOM', 'Mirran').

card_in_set('scrapdiver serpent', 'SOM').
card_original_type('scrapdiver serpent'/'SOM', 'Creature — Serpent').
card_original_text('scrapdiver serpent'/'SOM', 'Scrapdiver Serpent is unblockable as long as defending player controls an artifact.').
card_first_print('scrapdiver serpent', 'SOM').
card_image_name('scrapdiver serpent'/'SOM', 'scrapdiver serpent').
card_uid('scrapdiver serpent'/'SOM', 'SOM:Scrapdiver Serpent:scrapdiver serpent').
card_rarity('scrapdiver serpent'/'SOM', 'Common').
card_artist('scrapdiver serpent'/'SOM', 'Adrian Smith').
card_number('scrapdiver serpent'/'SOM', '41').
card_flavor_text('scrapdiver serpent'/'SOM', 'Smart artificers clean up after themselves.').
card_multiverse_id('scrapdiver serpent'/'SOM', '207863').
card_watermark('scrapdiver serpent'/'SOM', 'Mirran').

card_in_set('screeching silcaw', 'SOM').
card_original_type('screeching silcaw'/'SOM', 'Creature — Bird').
card_original_text('screeching silcaw'/'SOM', 'Flying\nMetalcraft — Whenever Screeching Silcaw deals combat damage to a player, if you control three or more artifacts, that player puts the top four cards of his or her library into his or her graveyard.').
card_first_print('screeching silcaw', 'SOM').
card_image_name('screeching silcaw'/'SOM', 'screeching silcaw').
card_uid('screeching silcaw'/'SOM', 'SOM:Screeching Silcaw:screeching silcaw').
card_rarity('screeching silcaw'/'SOM', 'Common').
card_artist('screeching silcaw'/'SOM', 'Mike Bierek').
card_number('screeching silcaw'/'SOM', '42').
card_flavor_text('screeching silcaw'/'SOM', 'It\'s adaptively deaf to its own cries.').
card_multiverse_id('screeching silcaw'/'SOM', '209281').
card_watermark('screeching silcaw'/'SOM', 'Mirran').

card_in_set('seachrome coast', 'SOM').
card_original_type('seachrome coast'/'SOM', 'Land').
card_original_text('seachrome coast'/'SOM', 'Seachrome Coast enters the battlefield tapped unless you control two or fewer other lands.\n{T}: Add {W} or {U} to your mana pool.').
card_first_print('seachrome coast', 'SOM').
card_image_name('seachrome coast'/'SOM', 'seachrome coast').
card_uid('seachrome coast'/'SOM', 'SOM:Seachrome Coast:seachrome coast').
card_rarity('seachrome coast'/'SOM', 'Rare').
card_artist('seachrome coast'/'SOM', 'Lars Grant-West').
card_number('seachrome coast'/'SOM', '229').
card_flavor_text('seachrome coast'/'SOM', 'Where the Quicksilver Sea laps over the Razor Fields, the landscape reflects the suns\' fading hope.').
card_multiverse_id('seachrome coast'/'SOM', '209399').
card_watermark('seachrome coast'/'SOM', 'Mirran').

card_in_set('seize the initiative', 'SOM').
card_original_type('seize the initiative'/'SOM', 'Instant').
card_original_text('seize the initiative'/'SOM', 'Target creature gets +1/+1 and gains first strike until end of turn.').
card_first_print('seize the initiative', 'SOM').
card_image_name('seize the initiative'/'SOM', 'seize the initiative').
card_uid('seize the initiative'/'SOM', 'SOM:Seize the Initiative:seize the initiative').
card_rarity('seize the initiative'/'SOM', 'Common').
card_artist('seize the initiative'/'SOM', 'Steve Argyle').
card_number('seize the initiative'/'SOM', '20').
card_flavor_text('seize the initiative'/'SOM', 'The time between spotting a leonin shikari and feeling its claws is just time enough to draw your last breath.').
card_multiverse_id('seize the initiative'/'SOM', '194206').
card_watermark('seize the initiative'/'SOM', 'Mirran').

card_in_set('semblance anvil', 'SOM').
card_original_type('semblance anvil'/'SOM', 'Artifact').
card_original_text('semblance anvil'/'SOM', 'Imprint — When Semblance Anvil enters the battlefield, you may exile a nonland card from your hand.\nSpells you cast that share a card type with the exiled card cost {2} less to cast.').
card_first_print('semblance anvil', 'SOM').
card_image_name('semblance anvil'/'SOM', 'semblance anvil').
card_uid('semblance anvil'/'SOM', 'SOM:Semblance Anvil:semblance anvil').
card_rarity('semblance anvil'/'SOM', 'Rare').
card_artist('semblance anvil'/'SOM', 'Dan Scott').
card_number('semblance anvil'/'SOM', '201').
card_flavor_text('semblance anvil'/'SOM', 'Forged from what it forges.').
card_multiverse_id('semblance anvil'/'SOM', '209002').
card_watermark('semblance anvil'/'SOM', 'Mirran').

card_in_set('shape anew', 'SOM').
card_original_type('shape anew'/'SOM', 'Sorcery').
card_original_text('shape anew'/'SOM', 'The controller of target artifact sacrifices it, then reveals cards from the top of his or her library until he or she reveals an artifact card. That player puts that card onto the battlefield, then shuffles all other cards revealed this way into his or her library.').
card_first_print('shape anew', 'SOM').
card_image_name('shape anew'/'SOM', 'shape anew').
card_uid('shape anew'/'SOM', 'SOM:Shape Anew:shape anew').
card_rarity('shape anew'/'SOM', 'Rare').
card_artist('shape anew'/'SOM', 'Zoltan Boros & Gabor Szikszai').
card_number('shape anew'/'SOM', '43').
card_multiverse_id('shape anew'/'SOM', '194201').
card_watermark('shape anew'/'SOM', 'Mirran').

card_in_set('shatter', 'SOM').
card_original_type('shatter'/'SOM', 'Instant').
card_original_text('shatter'/'SOM', 'Destroy target artifact.').
card_image_name('shatter'/'SOM', 'shatter').
card_uid('shatter'/'SOM', 'SOM:Shatter:shatter').
card_rarity('shatter'/'SOM', 'Common').
card_artist('shatter'/'SOM', 'jD').
card_number('shatter'/'SOM', '103').
card_flavor_text('shatter'/'SOM', '\"I am an artist, a builder, a creator . . . I create the biggest explosions around.\"\n—Krol, goblin furnace-priest').
card_multiverse_id('shatter'/'SOM', '194147').
card_watermark('shatter'/'SOM', 'Mirran').

card_in_set('silver myr', 'SOM').
card_original_type('silver myr'/'SOM', 'Artifact Creature — Myr').
card_original_text('silver myr'/'SOM', '{T}: Add {U} to your mana pool.').
card_image_name('silver myr'/'SOM', 'silver myr').
card_uid('silver myr'/'SOM', 'SOM:Silver Myr:silver myr').
card_rarity('silver myr'/'SOM', 'Common').
card_artist('silver myr'/'SOM', 'Alan Pollack').
card_number('silver myr'/'SOM', '202').
card_flavor_text('silver myr'/'SOM', 'The myr are like quicksilver: malleable in function and adaptable in form.').
card_multiverse_id('silver myr'/'SOM', '194378').
card_watermark('silver myr'/'SOM', 'Mirran').

card_in_set('skinrender', 'SOM').
card_original_type('skinrender'/'SOM', 'Creature — Zombie').
card_original_text('skinrender'/'SOM', 'When Skinrender enters the battlefield, put three -1/-1 counters on target creature.').
card_image_name('skinrender'/'SOM', 'skinrender').
card_uid('skinrender'/'SOM', 'SOM:Skinrender:skinrender').
card_rarity('skinrender'/'SOM', 'Uncommon').
card_artist('skinrender'/'SOM', 'David Rapoza').
card_number('skinrender'/'SOM', '78').
card_flavor_text('skinrender'/'SOM', '\"Your creations are effective, Sheoldred, but we must unite the flesh, not merely flay it.\"\n—Elesh Norn, Grand Cenobite').
card_multiverse_id('skinrender'/'SOM', '204958').
card_watermark('skinrender'/'SOM', 'Phyrexian').

card_in_set('skithiryx, the blight dragon', 'SOM').
card_original_type('skithiryx, the blight dragon'/'SOM', 'Legendary Creature — Dragon Skeleton').
card_original_text('skithiryx, the blight dragon'/'SOM', 'Flying\nInfect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\n{B}: Skithiryx, the Blight Dragon gains haste until end of turn.\n{B}{B}: Regenerate Skithiryx.').
card_first_print('skithiryx, the blight dragon', 'SOM').
card_image_name('skithiryx, the blight dragon'/'SOM', 'skithiryx, the blight dragon').
card_uid('skithiryx, the blight dragon'/'SOM', 'SOM:Skithiryx, the Blight Dragon:skithiryx, the blight dragon').
card_rarity('skithiryx, the blight dragon'/'SOM', 'Mythic Rare').
card_artist('skithiryx, the blight dragon'/'SOM', 'Chippy').
card_number('skithiryx, the blight dragon'/'SOM', '79').
card_multiverse_id('skithiryx, the blight dragon'/'SOM', '212249').
card_watermark('skithiryx, the blight dragon'/'SOM', 'Phyrexian').

card_in_set('sky-eel school', 'SOM').
card_original_type('sky-eel school'/'SOM', 'Creature — Fish').
card_original_text('sky-eel school'/'SOM', 'Flying\nWhen Sky-Eel School enters the battlefield, draw a card, then discard a card.').
card_first_print('sky-eel school', 'SOM').
card_image_name('sky-eel school'/'SOM', 'sky-eel school').
card_uid('sky-eel school'/'SOM', 'SOM:Sky-Eel School:sky-eel school').
card_rarity('sky-eel school'/'SOM', 'Common').
card_artist('sky-eel school'/'SOM', 'Daniel Ljunggren').
card_number('sky-eel school'/'SOM', '44').
card_flavor_text('sky-eel school'/'SOM', 'They swim on tides few can see, away from a threat few yet understand.').
card_multiverse_id('sky-eel school'/'SOM', '194103').
card_watermark('sky-eel school'/'SOM', 'Mirran').

card_in_set('slice in twain', 'SOM').
card_original_type('slice in twain'/'SOM', 'Instant').
card_original_text('slice in twain'/'SOM', 'Destroy target artifact or enchantment.\nDraw a card.').
card_first_print('slice in twain', 'SOM').
card_image_name('slice in twain'/'SOM', 'slice in twain').
card_uid('slice in twain'/'SOM', 'SOM:Slice in Twain:slice in twain').
card_rarity('slice in twain'/'SOM', 'Uncommon').
card_artist('slice in twain'/'SOM', 'Efrem Palacios').
card_number('slice in twain'/'SOM', '127').
card_flavor_text('slice in twain'/'SOM', '\"The hypocrisy of these elves is thicker than steel—destroying ‘unnatural metal\' with their own enchanted swords.\"\n—Kara Vrist, Neurok agent').
card_multiverse_id('slice in twain'/'SOM', '208254').
card_watermark('slice in twain'/'SOM', 'Mirran').

card_in_set('snapsail glider', 'SOM').
card_original_type('snapsail glider'/'SOM', 'Artifact Creature — Construct').
card_original_text('snapsail glider'/'SOM', 'Metalcraft — Snapsail Glider has flying as long as you control three or more artifacts.').
card_first_print('snapsail glider', 'SOM').
card_image_name('snapsail glider'/'SOM', 'snapsail glider').
card_uid('snapsail glider'/'SOM', 'SOM:Snapsail Glider:snapsail glider').
card_rarity('snapsail glider'/'SOM', 'Common').
card_artist('snapsail glider'/'SOM', 'Efrem Palacios').
card_number('snapsail glider'/'SOM', '203').
card_flavor_text('snapsail glider'/'SOM', 'Built from a reconfigured thresher, it charges with light reflected off the golden plain, ready to take to the air in case of danger.').
card_multiverse_id('snapsail glider'/'SOM', '209010').
card_watermark('snapsail glider'/'SOM', 'Mirran').

card_in_set('soliton', 'SOM').
card_original_type('soliton'/'SOM', 'Artifact Creature — Construct').
card_original_text('soliton'/'SOM', '{U}: Untap Soliton.').
card_first_print('soliton', 'SOM').
card_image_name('soliton'/'SOM', 'soliton').
card_uid('soliton'/'SOM', 'SOM:Soliton:soliton').
card_rarity('soliton'/'SOM', 'Common').
card_artist('soliton'/'SOM', 'Jason Felix').
card_number('soliton'/'SOM', '204').
card_flavor_text('soliton'/'SOM', 'The gemini engines had lost connection with each other and wandered apart, developing an independent awareness of their surroundings.').
card_multiverse_id('soliton'/'SOM', '205484').
card_watermark('soliton'/'SOM', 'Mirran').

card_in_set('soul parry', 'SOM').
card_original_type('soul parry'/'SOM', 'Instant').
card_original_text('soul parry'/'SOM', 'Prevent all damage one or two target creatures would deal this turn.').
card_first_print('soul parry', 'SOM').
card_image_name('soul parry'/'SOM', 'soul parry').
card_uid('soul parry'/'SOM', 'SOM:Soul Parry:soul parry').
card_rarity('soul parry'/'SOM', 'Common').
card_artist('soul parry'/'SOM', 'Igor Kieryluk').
card_number('soul parry'/'SOM', '21').
card_flavor_text('soul parry'/'SOM', '\"I was called to this world of steel, and it will be my steel that answers.\"\n—Elspeth Tirel').
card_multiverse_id('soul parry'/'SOM', '212705').
card_watermark('soul parry'/'SOM', 'Mirran').

card_in_set('spikeshot elder', 'SOM').
card_original_type('spikeshot elder'/'SOM', 'Creature — Goblin Shaman').
card_original_text('spikeshot elder'/'SOM', '{1}{R}{R}: Spikeshot Elder deals damage equal to its power to target creature or player.').
card_first_print('spikeshot elder', 'SOM').
card_image_name('spikeshot elder'/'SOM', 'spikeshot elder').
card_uid('spikeshot elder'/'SOM', 'SOM:Spikeshot Elder:spikeshot elder').
card_rarity('spikeshot elder'/'SOM', 'Rare').
card_artist('spikeshot elder'/'SOM', 'Izzy').
card_number('spikeshot elder'/'SOM', '104').
card_flavor_text('spikeshot elder'/'SOM', 'Now that he knows a thousand ways of hurting people, his next shamanic quest is to discover a thousand more.').
card_multiverse_id('spikeshot elder'/'SOM', '209404').
card_watermark('spikeshot elder'/'SOM', 'Mirran').

card_in_set('steady progress', 'SOM').
card_original_type('steady progress'/'SOM', 'Instant').
card_original_text('steady progress'/'SOM', 'Proliferate. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there.)\nDraw a card.').
card_first_print('steady progress', 'SOM').
card_image_name('steady progress'/'SOM', 'steady progress').
card_uid('steady progress'/'SOM', 'SOM:Steady Progress:steady progress').
card_rarity('steady progress'/'SOM', 'Common').
card_artist('steady progress'/'SOM', 'Efrem Palacios').
card_number('steady progress'/'SOM', '45').
card_flavor_text('steady progress'/'SOM', '\"More of that strange oil . . . It\'s probably nothing.\"').
card_multiverse_id('steady progress'/'SOM', '194361').
card_watermark('steady progress'/'SOM', 'Phyrexian').

card_in_set('steel hellkite', 'SOM').
card_original_type('steel hellkite'/'SOM', 'Artifact Creature — Dragon').
card_original_text('steel hellkite'/'SOM', 'Flying\n{2}: Steel Hellkite gets +1/+0 until end of turn.\n{X}: Destroy each nonland permanent with converted mana cost X whose controller was dealt combat damage by Steel Hellkite this turn. Activate this ability only once each turn.').
card_image_name('steel hellkite'/'SOM', 'steel hellkite').
card_uid('steel hellkite'/'SOM', 'SOM:Steel Hellkite:steel hellkite').
card_rarity('steel hellkite'/'SOM', 'Rare').
card_artist('steel hellkite'/'SOM', 'James Paick').
card_number('steel hellkite'/'SOM', '205').
card_multiverse_id('steel hellkite'/'SOM', '206351').
card_watermark('steel hellkite'/'SOM', 'Mirran').

card_in_set('stoic rebuttal', 'SOM').
card_original_type('stoic rebuttal'/'SOM', 'Instant').
card_original_text('stoic rebuttal'/'SOM', 'Metalcraft — Stoic Rebuttal costs {1} less to cast if you control three or more artifacts.\nCounter target spell.').
card_first_print('stoic rebuttal', 'SOM').
card_image_name('stoic rebuttal'/'SOM', 'stoic rebuttal').
card_uid('stoic rebuttal'/'SOM', 'SOM:Stoic Rebuttal:stoic rebuttal').
card_rarity('stoic rebuttal'/'SOM', 'Common').
card_artist('stoic rebuttal'/'SOM', 'Chris Rahn').
card_number('stoic rebuttal'/'SOM', '46').
card_flavor_text('stoic rebuttal'/'SOM', 'Obsessed with the pursuit of knowledge above all else, vedalken can appear to be cold and emotionless.').
card_multiverse_id('stoic rebuttal'/'SOM', '212703').
card_watermark('stoic rebuttal'/'SOM', 'Mirran').

card_in_set('strata scythe', 'SOM').
card_original_type('strata scythe'/'SOM', 'Artifact — Equipment').
card_original_text('strata scythe'/'SOM', 'Imprint — When Strata Scythe enters the battlefield, search your library for a land card, exile it, then shuffle your library.\nEquipped creature gets +1/+1 for each land on the battlefield with the same name as the exiled card.\nEquip {3}').
card_first_print('strata scythe', 'SOM').
card_image_name('strata scythe'/'SOM', 'strata scythe').
card_uid('strata scythe'/'SOM', 'SOM:Strata Scythe:strata scythe').
card_rarity('strata scythe'/'SOM', 'Rare').
card_artist('strata scythe'/'SOM', 'Scott Chou').
card_number('strata scythe'/'SOM', '206').
card_multiverse_id('strata scythe'/'SOM', '207870').
card_watermark('strata scythe'/'SOM', 'Mirran').

card_in_set('strider harness', 'SOM').
card_original_type('strider harness'/'SOM', 'Artifact — Equipment').
card_original_text('strider harness'/'SOM', 'Equipped creature gets +1/+1 and has haste.\nEquip {1}').
card_first_print('strider harness', 'SOM').
card_image_name('strider harness'/'SOM', 'strider harness').
card_uid('strider harness'/'SOM', 'SOM:Strider Harness:strider harness').
card_rarity('strider harness'/'SOM', 'Common').
card_artist('strider harness'/'SOM', 'Matt Stewart').
card_number('strider harness'/'SOM', '207').
card_flavor_text('strider harness'/'SOM', 'Each journey begins with a single step—and sometimes ends with that single step as well.').
card_multiverse_id('strider harness'/'SOM', '206344').
card_watermark('strider harness'/'SOM', 'Mirran').

card_in_set('sunblast angel', 'SOM').
card_original_type('sunblast angel'/'SOM', 'Creature — Angel').
card_original_text('sunblast angel'/'SOM', 'Flying\nWhen Sunblast Angel enters the battlefield, destroy all tapped creatures.').
card_image_name('sunblast angel'/'SOM', 'sunblast angel').
card_uid('sunblast angel'/'SOM', 'SOM:Sunblast Angel:sunblast angel').
card_rarity('sunblast angel'/'SOM', 'Rare').
card_artist('sunblast angel'/'SOM', 'Jason Chan').
card_number('sunblast angel'/'SOM', '22').
card_flavor_text('sunblast angel'/'SOM', 'There may exist powers even greater than Phyrexia.').
card_multiverse_id('sunblast angel'/'SOM', '215111').
card_watermark('sunblast angel'/'SOM', 'Mirran').

card_in_set('sunspear shikari', 'SOM').
card_original_type('sunspear shikari'/'SOM', 'Creature — Cat Soldier').
card_original_text('sunspear shikari'/'SOM', 'As long as Sunspear Shikari is equipped, it has first strike and lifelink.').
card_first_print('sunspear shikari', 'SOM').
card_image_name('sunspear shikari'/'SOM', 'sunspear shikari').
card_uid('sunspear shikari'/'SOM', 'SOM:Sunspear Shikari:sunspear shikari').
card_rarity('sunspear shikari'/'SOM', 'Common').
card_artist('sunspear shikari'/'SOM', 'Allen Williams').
card_number('sunspear shikari'/'SOM', '23').
card_flavor_text('sunspear shikari'/'SOM', 'Left without their leader Raksha, the leonin split into two prides: one side supported the regent kha, while the other rebelled in fury.').
card_multiverse_id('sunspear shikari'/'SOM', '209008').
card_watermark('sunspear shikari'/'SOM', 'Mirran').

card_in_set('swamp', 'SOM').
card_original_type('swamp'/'SOM', 'Basic Land — Swamp').
card_original_text('swamp'/'SOM', 'B').
card_image_name('swamp'/'SOM', 'swamp1').
card_uid('swamp'/'SOM', 'SOM:Swamp:swamp1').
card_rarity('swamp'/'SOM', 'Basic Land').
card_artist('swamp'/'SOM', 'Lars Grant-West').
card_number('swamp'/'SOM', '238').
card_multiverse_id('swamp'/'SOM', '214684').

card_in_set('swamp', 'SOM').
card_original_type('swamp'/'SOM', 'Basic Land — Swamp').
card_original_text('swamp'/'SOM', 'B').
card_image_name('swamp'/'SOM', 'swamp2').
card_uid('swamp'/'SOM', 'SOM:Swamp:swamp2').
card_rarity('swamp'/'SOM', 'Basic Land').
card_artist('swamp'/'SOM', 'Lars Grant-West').
card_number('swamp'/'SOM', '239').
card_multiverse_id('swamp'/'SOM', '214669').

card_in_set('swamp', 'SOM').
card_original_type('swamp'/'SOM', 'Basic Land — Swamp').
card_original_text('swamp'/'SOM', 'B').
card_image_name('swamp'/'SOM', 'swamp3').
card_uid('swamp'/'SOM', 'SOM:Swamp:swamp3').
card_rarity('swamp'/'SOM', 'Basic Land').
card_artist('swamp'/'SOM', 'Lars Grant-West').
card_number('swamp'/'SOM', '240').
card_multiverse_id('swamp'/'SOM', '214676').

card_in_set('swamp', 'SOM').
card_original_type('swamp'/'SOM', 'Basic Land — Swamp').
card_original_text('swamp'/'SOM', 'B').
card_image_name('swamp'/'SOM', 'swamp4').
card_uid('swamp'/'SOM', 'SOM:Swamp:swamp4').
card_rarity('swamp'/'SOM', 'Basic Land').
card_artist('swamp'/'SOM', 'Lars Grant-West').
card_number('swamp'/'SOM', '241').
card_multiverse_id('swamp'/'SOM', '214674').

card_in_set('sword of body and mind', 'SOM').
card_original_type('sword of body and mind'/'SOM', 'Artifact — Equipment').
card_original_text('sword of body and mind'/'SOM', 'Equipped creature gets +2/+2 and has protection from green and from blue.\nWhenever equipped creature deals combat damage to a player, you put a 2/2 green Wolf creature token onto the battlefield and that player puts the top ten cards of his or her library into his or her graveyard.\nEquip {2}').
card_image_name('sword of body and mind'/'SOM', 'sword of body and mind').
card_uid('sword of body and mind'/'SOM', 'SOM:Sword of Body and Mind:sword of body and mind').
card_rarity('sword of body and mind'/'SOM', 'Mythic Rare').
card_artist('sword of body and mind'/'SOM', 'Chris Rahn').
card_number('sword of body and mind'/'SOM', '208').
card_multiverse_id('sword of body and mind'/'SOM', '209280').
card_watermark('sword of body and mind'/'SOM', 'Mirran').

card_in_set('sylvok lifestaff', 'SOM').
card_original_type('sylvok lifestaff'/'SOM', 'Artifact — Equipment').
card_original_text('sylvok lifestaff'/'SOM', 'Equipped creature gets +1/+0.\nWhenever equipped creature is put into a graveyard, you gain 3 life.\nEquip {1}').
card_first_print('sylvok lifestaff', 'SOM').
card_image_name('sylvok lifestaff'/'SOM', 'sylvok lifestaff').
card_uid('sylvok lifestaff'/'SOM', 'SOM:Sylvok Lifestaff:sylvok lifestaff').
card_rarity('sylvok lifestaff'/'SOM', 'Common').
card_artist('sylvok lifestaff'/'SOM', 'Martina Pilcerova').
card_number('sylvok lifestaff'/'SOM', '209').
card_flavor_text('sylvok lifestaff'/'SOM', 'The druid\'s lifestaff connects her to generations of bygone Sylvok.').
card_multiverse_id('sylvok lifestaff'/'SOM', '207857').
card_watermark('sylvok lifestaff'/'SOM', 'Mirran').

card_in_set('sylvok replica', 'SOM').
card_original_type('sylvok replica'/'SOM', 'Artifact Creature — Shaman').
card_original_text('sylvok replica'/'SOM', '{G}, Sacrifice Sylvok Replica: Destroy target artifact or enchantment.').
card_first_print('sylvok replica', 'SOM').
card_image_name('sylvok replica'/'SOM', 'sylvok replica').
card_uid('sylvok replica'/'SOM', 'SOM:Sylvok Replica:sylvok replica').
card_rarity('sylvok replica'/'SOM', 'Common').
card_artist('sylvok replica'/'SOM', 'Zoltan Boros & Gabor Szikszai').
card_number('sylvok replica'/'SOM', '210').
card_flavor_text('sylvok replica'/'SOM', 'All the zeal of the Sylvok with only a trace of their conservancy.').
card_multiverse_id('sylvok replica'/'SOM', '209052').
card_watermark('sylvok replica'/'SOM', 'Mirran').

card_in_set('tainted strike', 'SOM').
card_original_type('tainted strike'/'SOM', 'Instant').
card_original_text('tainted strike'/'SOM', 'Target creature gets +1/+0 and gains infect until end of turn. (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_first_print('tainted strike', 'SOM').
card_image_name('tainted strike'/'SOM', 'tainted strike').
card_uid('tainted strike'/'SOM', 'SOM:Tainted Strike:tainted strike').
card_rarity('tainted strike'/'SOM', 'Common').
card_artist('tainted strike'/'SOM', 'James Ryman').
card_number('tainted strike'/'SOM', '80').
card_flavor_text('tainted strike'/'SOM', '\"Welcome to perfection.\"\n—Sheoldred, Whispering One').
card_multiverse_id('tainted strike'/'SOM', '209049').
card_watermark('tainted strike'/'SOM', 'Phyrexian').

card_in_set('tangle angler', 'SOM').
card_original_type('tangle angler'/'SOM', 'Creature — Horror').
card_original_text('tangle angler'/'SOM', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\n{G}: Target creature blocks Tangle Angler this turn if able.').
card_first_print('tangle angler', 'SOM').
card_image_name('tangle angler'/'SOM', 'tangle angler').
card_uid('tangle angler'/'SOM', 'SOM:Tangle Angler:tangle angler').
card_rarity('tangle angler'/'SOM', 'Uncommon').
card_artist('tangle angler'/'SOM', 'Igor Kieryluk').
card_number('tangle angler'/'SOM', '128').
card_flavor_text('tangle angler'/'SOM', 'A plague on the curious.').
card_multiverse_id('tangle angler'/'SOM', '205485').
card_watermark('tangle angler'/'SOM', 'Phyrexian').

card_in_set('tel-jilad defiance', 'SOM').
card_original_type('tel-jilad defiance'/'SOM', 'Instant').
card_original_text('tel-jilad defiance'/'SOM', 'Target creature gains protection from artifacts until end of turn.\nDraw a card.').
card_first_print('tel-jilad defiance', 'SOM').
card_image_name('tel-jilad defiance'/'SOM', 'tel-jilad defiance').
card_uid('tel-jilad defiance'/'SOM', 'SOM:Tel-Jilad Defiance:tel-jilad defiance').
card_rarity('tel-jilad defiance'/'SOM', 'Common').
card_artist('tel-jilad defiance'/'SOM', 'Goran Josic').
card_number('tel-jilad defiance'/'SOM', '129').
card_flavor_text('tel-jilad defiance'/'SOM', 'The Viridian elves had purged their culture of the trolls\' traitorous influence. In times of strife, however, the ancestral memories returned.').
card_multiverse_id('tel-jilad defiance'/'SOM', '202636').
card_watermark('tel-jilad defiance'/'SOM', 'Mirran').

card_in_set('tel-jilad fallen', 'SOM').
card_original_type('tel-jilad fallen'/'SOM', 'Creature — Elf Warrior').
card_original_text('tel-jilad fallen'/'SOM', 'Protection from artifacts\nInfect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_first_print('tel-jilad fallen', 'SOM').
card_image_name('tel-jilad fallen'/'SOM', 'tel-jilad fallen').
card_uid('tel-jilad fallen'/'SOM', 'SOM:Tel-Jilad Fallen:tel-jilad fallen').
card_rarity('tel-jilad fallen'/'SOM', 'Common').
card_artist('tel-jilad fallen'/'SOM', 'James Ryman').
card_number('tel-jilad fallen'/'SOM', '130').
card_flavor_text('tel-jilad fallen'/'SOM', '\"No longer am I shackled to antiquity. My new masters will etch a glorious future.\"').
card_multiverse_id('tel-jilad fallen'/'SOM', '205483').
card_watermark('tel-jilad fallen'/'SOM', 'Phyrexian').

card_in_set('tempered steel', 'SOM').
card_original_type('tempered steel'/'SOM', 'Enchantment').
card_original_text('tempered steel'/'SOM', 'Artifact creatures you control get +2/+2.').
card_image_name('tempered steel'/'SOM', 'tempered steel').
card_uid('tempered steel'/'SOM', 'SOM:Tempered Steel:tempered steel').
card_rarity('tempered steel'/'SOM', 'Rare').
card_artist('tempered steel'/'SOM', 'Wayne Reynolds').
card_number('tempered steel'/'SOM', '24').
card_flavor_text('tempered steel'/'SOM', '\"Death shall prevail as long as our will falls to rust. May necessity anneal our resolve.\"\n—Ghalma the Shaper').
card_multiverse_id('tempered steel'/'SOM', '194391').
card_watermark('tempered steel'/'SOM', 'Mirran').

card_in_set('throne of geth', 'SOM').
card_original_type('throne of geth'/'SOM', 'Artifact').
card_original_text('throne of geth'/'SOM', '{T}, Sacrifice an artifact: Proliferate. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there.)').
card_first_print('throne of geth', 'SOM').
card_image_name('throne of geth'/'SOM', 'throne of geth').
card_uid('throne of geth'/'SOM', 'SOM:Throne of Geth:throne of geth').
card_rarity('throne of geth'/'SOM', 'Uncommon').
card_artist('throne of geth'/'SOM', 'Jana Schirmer & Johannes Voss').
card_number('throne of geth'/'SOM', '211').
card_flavor_text('throne of geth'/'SOM', 'At the heart of Ish Sah, Geth stokes the dark fires of Phyrexia.').
card_multiverse_id('throne of geth'/'SOM', '202675').
card_watermark('throne of geth'/'SOM', 'Phyrexian').

card_in_set('thrummingbird', 'SOM').
card_original_type('thrummingbird'/'SOM', 'Creature — Bird Horror').
card_original_text('thrummingbird'/'SOM', 'Flying\nWhenever Thrummingbird deals combat damage to a player, proliferate. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there.)').
card_first_print('thrummingbird', 'SOM').
card_image_name('thrummingbird'/'SOM', 'thrummingbird').
card_uid('thrummingbird'/'SOM', 'SOM:Thrummingbird:thrummingbird').
card_rarity('thrummingbird'/'SOM', 'Uncommon').
card_artist('thrummingbird'/'SOM', 'Efrem Palacios').
card_number('thrummingbird'/'SOM', '47').
card_multiverse_id('thrummingbird'/'SOM', '194332').
card_watermark('thrummingbird'/'SOM', 'Phyrexian').

card_in_set('tower of calamities', 'SOM').
card_original_type('tower of calamities'/'SOM', 'Artifact').
card_original_text('tower of calamities'/'SOM', '{8}, {T}: Tower of Calamities deals 12 damage to target creature.').
card_first_print('tower of calamities', 'SOM').
card_image_name('tower of calamities'/'SOM', 'tower of calamities').
card_uid('tower of calamities'/'SOM', 'SOM:Tower of Calamities:tower of calamities').
card_rarity('tower of calamities'/'SOM', 'Rare').
card_artist('tower of calamities'/'SOM', 'Aleksi Briclot').
card_number('tower of calamities'/'SOM', '212').
card_flavor_text('tower of calamities'/'SOM', 'The ur-golems concealed one of their towers out of fear that its power would be abused, and in anticipation of a time when its power would be sorely needed.').
card_multiverse_id('tower of calamities'/'SOM', '207864').
card_watermark('tower of calamities'/'SOM', 'Mirran').

card_in_set('trigon of corruption', 'SOM').
card_original_type('trigon of corruption'/'SOM', 'Artifact').
card_original_text('trigon of corruption'/'SOM', 'Trigon of Corruption enters the battlefield with three charge counters on it.\n{B}{B}, {T}: Put a charge counter on Trigon of Corruption.\n{2}, {T}, Remove a charge counter from Trigon of Corruption: Put a -1/-1 counter on target creature.').
card_first_print('trigon of corruption', 'SOM').
card_image_name('trigon of corruption'/'SOM', 'trigon of corruption').
card_uid('trigon of corruption'/'SOM', 'SOM:Trigon of Corruption:trigon of corruption').
card_rarity('trigon of corruption'/'SOM', 'Uncommon').
card_artist('trigon of corruption'/'SOM', 'Nils Hamm').
card_number('trigon of corruption'/'SOM', '213').
card_multiverse_id('trigon of corruption'/'SOM', '202686').
card_watermark('trigon of corruption'/'SOM', 'Phyrexian').

card_in_set('trigon of infestation', 'SOM').
card_original_type('trigon of infestation'/'SOM', 'Artifact').
card_original_text('trigon of infestation'/'SOM', 'Trigon of Infestation enters the battlefield with three charge counters on it.\n{G}{G}, {T}: Put a charge counter on Trigon of Infestation.\n{2}, {T}, Remove a charge counter from Trigon of Infestation: Put a 1/1 green Insect creature token with infect onto the battlefield.').
card_first_print('trigon of infestation', 'SOM').
card_image_name('trigon of infestation'/'SOM', 'trigon of infestation').
card_uid('trigon of infestation'/'SOM', 'SOM:Trigon of Infestation:trigon of infestation').
card_rarity('trigon of infestation'/'SOM', 'Uncommon').
card_artist('trigon of infestation'/'SOM', 'Dave Allsop').
card_number('trigon of infestation'/'SOM', '214').
card_multiverse_id('trigon of infestation'/'SOM', '194232').
card_watermark('trigon of infestation'/'SOM', 'Phyrexian').

card_in_set('trigon of mending', 'SOM').
card_original_type('trigon of mending'/'SOM', 'Artifact').
card_original_text('trigon of mending'/'SOM', 'Trigon of Mending enters the battlefield with three charge counters on it.\n{W}{W}, {T}: Put a charge counter on Trigon of Mending.\n{2}, {T}, Remove a charge counter from Trigon of Mending: Target player gains 3 life.').
card_first_print('trigon of mending', 'SOM').
card_image_name('trigon of mending'/'SOM', 'trigon of mending').
card_uid('trigon of mending'/'SOM', 'SOM:Trigon of Mending:trigon of mending').
card_rarity('trigon of mending'/'SOM', 'Uncommon').
card_artist('trigon of mending'/'SOM', 'Igor Kieryluk').
card_number('trigon of mending'/'SOM', '215').
card_multiverse_id('trigon of mending'/'SOM', '212714').
card_watermark('trigon of mending'/'SOM', 'Mirran').

card_in_set('trigon of rage', 'SOM').
card_original_type('trigon of rage'/'SOM', 'Artifact').
card_original_text('trigon of rage'/'SOM', 'Trigon of Rage enters the battlefield with three charge counters on it.\n{R}{R}, {T}: Put a charge counter on Trigon of Rage.\n{2}, {T}, Remove a charge counter from Trigon of Rage: Target creature gets +3/+0 until end of turn.').
card_first_print('trigon of rage', 'SOM').
card_image_name('trigon of rage'/'SOM', 'trigon of rage').
card_uid('trigon of rage'/'SOM', 'SOM:Trigon of Rage:trigon of rage').
card_rarity('trigon of rage'/'SOM', 'Uncommon').
card_artist('trigon of rage'/'SOM', 'Marc Simonetti').
card_number('trigon of rage'/'SOM', '216').
card_multiverse_id('trigon of rage'/'SOM', '194393').
card_watermark('trigon of rage'/'SOM', 'Mirran').

card_in_set('trigon of thought', 'SOM').
card_original_type('trigon of thought'/'SOM', 'Artifact').
card_original_text('trigon of thought'/'SOM', 'Trigon of Thought enters the battlefield with three charge counters on it.\n{U}{U}, {T}: Put a charge counter on Trigon of Thought.\n{2}, {T}, Remove a charge counter from Trigon of Thought: Draw a card.').
card_first_print('trigon of thought', 'SOM').
card_image_name('trigon of thought'/'SOM', 'trigon of thought').
card_uid('trigon of thought'/'SOM', 'SOM:Trigon of Thought:trigon of thought').
card_rarity('trigon of thought'/'SOM', 'Uncommon').
card_artist('trigon of thought'/'SOM', 'Mike Bierek').
card_number('trigon of thought'/'SOM', '217').
card_multiverse_id('trigon of thought'/'SOM', '205476').
card_watermark('trigon of thought'/'SOM', 'Mirran').

card_in_set('trinket mage', 'SOM').
card_original_type('trinket mage'/'SOM', 'Creature — Human Wizard').
card_original_text('trinket mage'/'SOM', 'When Trinket Mage enters the battlefield, you may search your library for an artifact card with converted mana cost 1 or less, reveal that card, and put it into your hand. If you do, shuffle your library.').
card_image_name('trinket mage'/'SOM', 'trinket mage').
card_uid('trinket mage'/'SOM', 'SOM:Trinket Mage:trinket mage').
card_rarity('trinket mage'/'SOM', 'Uncommon').
card_artist('trinket mage'/'SOM', 'Scott Chou').
card_number('trinket mage'/'SOM', '48').
card_multiverse_id('trinket mage'/'SOM', '209040').
card_watermark('trinket mage'/'SOM', 'Mirran').

card_in_set('true conviction', 'SOM').
card_original_type('true conviction'/'SOM', 'Enchantment').
card_original_text('true conviction'/'SOM', 'Creatures you control have double strike and lifelink.').
card_first_print('true conviction', 'SOM').
card_image_name('true conviction'/'SOM', 'true conviction').
card_uid('true conviction'/'SOM', 'SOM:True Conviction:true conviction').
card_rarity('true conviction'/'SOM', 'Rare').
card_artist('true conviction'/'SOM', 'Svetlin Velinov').
card_number('true conviction'/'SOM', '25').
card_flavor_text('true conviction'/'SOM', 'Dozens of spells known only to the Auriok elders were lost, but the next generation, galvanized by war, devised even more potent magic.').
card_multiverse_id('true conviction'/'SOM', '208262').
card_watermark('true conviction'/'SOM', 'Mirran').

card_in_set('tumble magnet', 'SOM').
card_original_type('tumble magnet'/'SOM', 'Artifact').
card_original_text('tumble magnet'/'SOM', 'Tumble Magnet enters the battlefield with three charge counters on it.\n{T}, Remove a charge counter from Tumble Magnet: Tap target artifact or creature.').
card_first_print('tumble magnet', 'SOM').
card_image_name('tumble magnet'/'SOM', 'tumble magnet').
card_uid('tumble magnet'/'SOM', 'SOM:Tumble Magnet:tumble magnet').
card_rarity('tumble magnet'/'SOM', 'Common').
card_artist('tumble magnet'/'SOM', 'Drew Baker').
card_number('tumble magnet'/'SOM', '218').
card_flavor_text('tumble magnet'/'SOM', 'Magnetic devices that keep massive golems and structures standing can also be used for the opposite purpose.').
card_multiverse_id('tumble magnet'/'SOM', '210232').
card_watermark('tumble magnet'/'SOM', 'Mirran').

card_in_set('tunnel ignus', 'SOM').
card_original_type('tunnel ignus'/'SOM', 'Creature — Elemental').
card_original_text('tunnel ignus'/'SOM', 'Whenever a land enters the battlefield under an opponent\'s control, if that player had another land enter the battlefield under his or her control this turn, Tunnel Ignus deals 3 damage to that player.').
card_first_print('tunnel ignus', 'SOM').
card_image_name('tunnel ignus'/'SOM', 'tunnel ignus').
card_uid('tunnel ignus'/'SOM', 'SOM:Tunnel Ignus:tunnel ignus').
card_rarity('tunnel ignus'/'SOM', 'Rare').
card_artist('tunnel ignus'/'SOM', 'Scott Chou').
card_number('tunnel ignus'/'SOM', '105').
card_flavor_text('tunnel ignus'/'SOM', 'Fight mischief with mischief.').
card_multiverse_id('tunnel ignus'/'SOM', '206361').
card_watermark('tunnel ignus'/'SOM', 'Mirran').

card_in_set('turn aside', 'SOM').
card_original_type('turn aside'/'SOM', 'Instant').
card_original_text('turn aside'/'SOM', 'Counter target spell that targets a permanent you control.').
card_first_print('turn aside', 'SOM').
card_image_name('turn aside'/'SOM', 'turn aside').
card_uid('turn aside'/'SOM', 'SOM:Turn Aside:turn aside').
card_rarity('turn aside'/'SOM', 'Common').
card_artist('turn aside'/'SOM', 'Shelly Wan').
card_number('turn aside'/'SOM', '49').
card_flavor_text('turn aside'/'SOM', '\"It\'s not my job to ask our leaders about their experiments at the Knowledge Pool. It\'s my job to make sure they can continue performing them.\"').
card_multiverse_id('turn aside'/'SOM', '194303').
card_watermark('turn aside'/'SOM', 'Mirran').

card_in_set('turn to slag', 'SOM').
card_original_type('turn to slag'/'SOM', 'Sorcery').
card_original_text('turn to slag'/'SOM', 'Turn to Slag deals 5 damage to target creature. Destroy all Equipment attached to that creature.').
card_first_print('turn to slag', 'SOM').
card_image_name('turn to slag'/'SOM', 'turn to slag').
card_uid('turn to slag'/'SOM', 'SOM:Turn to Slag:turn to slag').
card_rarity('turn to slag'/'SOM', 'Common').
card_artist('turn to slag'/'SOM', 'Zoltan Boros & Gabor Szikszai').
card_number('turn to slag'/'SOM', '106').
card_flavor_text('turn to slag'/'SOM', '\"If it wasn\'t a blackened, stinking, melted abomination before, it certainly is now.\"\n—Koth of the Hammer').
card_multiverse_id('turn to slag'/'SOM', '209719').
card_watermark('turn to slag'/'SOM', 'Mirran').

card_in_set('twisted image', 'SOM').
card_original_type('twisted image'/'SOM', 'Instant').
card_original_text('twisted image'/'SOM', 'Switch target creature\'s power and toughness until end of turn.\nDraw a card.').
card_first_print('twisted image', 'SOM').
card_image_name('twisted image'/'SOM', 'twisted image').
card_uid('twisted image'/'SOM', 'SOM:Twisted Image:twisted image').
card_rarity('twisted image'/'SOM', 'Uncommon').
card_artist('twisted image'/'SOM', 'Izzy').
card_number('twisted image'/'SOM', '50').
card_flavor_text('twisted image'/'SOM', '\"True strength is in the mind, for the body is too malleable a form.\"\n—Politus, vedalken emisar').
card_multiverse_id('twisted image'/'SOM', '208253').
card_watermark('twisted image'/'SOM', 'Mirran').

card_in_set('untamed might', 'SOM').
card_original_type('untamed might'/'SOM', 'Instant').
card_original_text('untamed might'/'SOM', 'Target creature gets +X/+X until end of turn.').
card_first_print('untamed might', 'SOM').
card_image_name('untamed might'/'SOM', 'untamed might').
card_uid('untamed might'/'SOM', 'SOM:Untamed Might:untamed might').
card_rarity('untamed might'/'SOM', 'Common').
card_artist('untamed might'/'SOM', 'Erica Yang').
card_number('untamed might'/'SOM', '131').
card_flavor_text('untamed might'/'SOM', '\"Beasts kill for their very survival. If you would have their strength, you must know their desperation.\"\n—Konnos, Sylvok sage').
card_multiverse_id('untamed might'/'SOM', '194160').
card_watermark('untamed might'/'SOM', 'Mirran').

card_in_set('vault skyward', 'SOM').
card_original_type('vault skyward'/'SOM', 'Instant').
card_original_text('vault skyward'/'SOM', 'Target creature gains flying until end of turn. Untap it.').
card_first_print('vault skyward', 'SOM').
card_image_name('vault skyward'/'SOM', 'vault skyward').
card_uid('vault skyward'/'SOM', 'SOM:Vault Skyward:vault skyward').
card_rarity('vault skyward'/'SOM', 'Common').
card_artist('vault skyward'/'SOM', 'Dan Scott').
card_number('vault skyward'/'SOM', '51').
card_flavor_text('vault skyward'/'SOM', '\"The edges of the Quicksilver Sea run dark with the spread of the Mephidross. Two more aerial passes before I can be sure what I\'m seeing . . .\"\n—Lafarius, Neurok sky agent').
card_multiverse_id('vault skyward'/'SOM', '194265').
card_watermark('vault skyward'/'SOM', 'Mirran').

card_in_set('vector asp', 'SOM').
card_original_type('vector asp'/'SOM', 'Artifact Creature — Snake').
card_original_text('vector asp'/'SOM', '{B}: Vector Asp gains infect until end of turn. (It deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_first_print('vector asp', 'SOM').
card_image_name('vector asp'/'SOM', 'vector asp').
card_uid('vector asp'/'SOM', 'SOM:Vector Asp:vector asp').
card_rarity('vector asp'/'SOM', 'Common').
card_artist('vector asp'/'SOM', 'Erica Yang').
card_number('vector asp'/'SOM', '219').
card_flavor_text('vector asp'/'SOM', 'Phyrexia\'s machines take inspiration from nature. Twisted, disturbing inspiration.').
card_multiverse_id('vector asp'/'SOM', '194074').
card_watermark('vector asp'/'SOM', 'Phyrexian').

card_in_set('vedalken certarch', 'SOM').
card_original_type('vedalken certarch'/'SOM', 'Creature — Vedalken Wizard').
card_original_text('vedalken certarch'/'SOM', 'Metalcraft — {T}: Tap target artifact, creature, or land. Activate this ability only if you control three or more artifacts.').
card_first_print('vedalken certarch', 'SOM').
card_image_name('vedalken certarch'/'SOM', 'vedalken certarch').
card_uid('vedalken certarch'/'SOM', 'SOM:Vedalken Certarch:vedalken certarch').
card_rarity('vedalken certarch'/'SOM', 'Common').
card_artist('vedalken certarch'/'SOM', 'Karl Kopinski').
card_number('vedalken certarch'/'SOM', '52').
card_flavor_text('vedalken certarch'/'SOM', '\"An appropriately detailed model is indistinguishable from reality, and much easier to control.\"').
card_multiverse_id('vedalken certarch'/'SOM', '208257').
card_watermark('vedalken certarch'/'SOM', 'Mirran').

card_in_set('venser\'s journal', 'SOM').
card_original_type('venser\'s journal'/'SOM', 'Artifact').
card_original_text('venser\'s journal'/'SOM', 'You have no maximum hand size.\nAt the beginning of your upkeep, you gain 1 life for each card in your hand.').
card_first_print('venser\'s journal', 'SOM').
card_image_name('venser\'s journal'/'SOM', 'venser\'s journal').
card_uid('venser\'s journal'/'SOM', 'SOM:Venser\'s Journal:venser\'s journal').
card_rarity('venser\'s journal'/'SOM', 'Rare').
card_artist('venser\'s journal'/'SOM', 'Christopher Moeller').
card_number('venser\'s journal'/'SOM', '220').
card_flavor_text('venser\'s journal'/'SOM', 'A Planeswalker\'s chronicle spans worlds and civilizations, each page a lifetime.').
card_multiverse_id('venser\'s journal'/'SOM', '215079').
card_watermark('venser\'s journal'/'SOM', 'Mirran').

card_in_set('venser, the sojourner', 'SOM').
card_original_type('venser, the sojourner'/'SOM', 'Planeswalker — Venser').
card_original_text('venser, the sojourner'/'SOM', '+2: Exile target permanent you own. Return it to the battlefield under your control at the beginning of the next end step.\n-1: Creatures are unblockable this turn.\n-8: You get an emblem with \"Whenever you cast a spell, exile target permanent.\"').
card_first_print('venser, the sojourner', 'SOM').
card_image_name('venser, the sojourner'/'SOM', 'venser, the sojourner').
card_uid('venser, the sojourner'/'SOM', 'SOM:Venser, the Sojourner:venser, the sojourner').
card_rarity('venser, the sojourner'/'SOM', 'Mythic Rare').
card_artist('venser, the sojourner'/'SOM', 'Eric Deschamps').
card_number('venser, the sojourner'/'SOM', '135').
card_multiverse_id('venser, the sojourner'/'SOM', '212240').

card_in_set('vigil for the lost', 'SOM').
card_original_type('vigil for the lost'/'SOM', 'Enchantment').
card_original_text('vigil for the lost'/'SOM', 'Whenever a creature you control is put into a graveyard from the battlefield, you may pay {X}. If you do, you gain X life.').
card_first_print('vigil for the lost', 'SOM').
card_image_name('vigil for the lost'/'SOM', 'vigil for the lost').
card_uid('vigil for the lost'/'SOM', 'SOM:Vigil for the Lost:vigil for the lost').
card_rarity('vigil for the lost'/'SOM', 'Uncommon').
card_artist('vigil for the lost'/'SOM', 'Igor Kieryluk').
card_number('vigil for the lost'/'SOM', '26').
card_flavor_text('vigil for the lost'/'SOM', '\"As she grew cold in my arms, I swore an oath that her funeral pyre would be dwarfed by a bonfire of our enemies.\"').
card_multiverse_id('vigil for the lost'/'SOM', '205489').
card_watermark('vigil for the lost'/'SOM', 'Mirran').

card_in_set('viridian revel', 'SOM').
card_original_type('viridian revel'/'SOM', 'Enchantment').
card_original_text('viridian revel'/'SOM', 'Whenever an artifact is put into an opponent\'s graveyard from the battlefield, you may draw a card.').
card_first_print('viridian revel', 'SOM').
card_image_name('viridian revel'/'SOM', 'viridian revel').
card_uid('viridian revel'/'SOM', 'SOM:Viridian Revel:viridian revel').
card_rarity('viridian revel'/'SOM', 'Uncommon').
card_artist('viridian revel'/'SOM', 'rk post').
card_number('viridian revel'/'SOM', '132').
card_flavor_text('viridian revel'/'SOM', '\"Let them have their revel. Soon those who won\'t submit will be turned. Those who won\'t be turned will die.\"\n—Vorinclex, Voice of Hunger').
card_multiverse_id('viridian revel'/'SOM', '194266').
card_watermark('viridian revel'/'SOM', 'Mirran').

card_in_set('volition reins', 'SOM').
card_original_type('volition reins'/'SOM', 'Enchantment — Aura').
card_original_text('volition reins'/'SOM', 'Enchant permanent\nWhen Volition Reins enters the battlefield, if enchanted permanent is tapped, untap it.\nYou control enchanted permanent.').
card_first_print('volition reins', 'SOM').
card_image_name('volition reins'/'SOM', 'volition reins').
card_uid('volition reins'/'SOM', 'SOM:Volition Reins:volition reins').
card_rarity('volition reins'/'SOM', 'Uncommon').
card_artist('volition reins'/'SOM', 'Svetlin Velinov').
card_number('volition reins'/'SOM', '53').
card_flavor_text('volition reins'/'SOM', '\"Glorifying the will is a mistake. It is simply one more force to be mastered.\"').
card_multiverse_id('volition reins'/'SOM', '209001').
card_watermark('volition reins'/'SOM', 'Mirran').

card_in_set('vulshok heartstoker', 'SOM').
card_original_type('vulshok heartstoker'/'SOM', 'Creature — Human Shaman').
card_original_text('vulshok heartstoker'/'SOM', 'When Vulshok Heartstoker enters the battlefield, target creature gets +2/+0 until end of turn.').
card_first_print('vulshok heartstoker', 'SOM').
card_image_name('vulshok heartstoker'/'SOM', 'vulshok heartstoker').
card_uid('vulshok heartstoker'/'SOM', 'SOM:Vulshok Heartstoker:vulshok heartstoker').
card_rarity('vulshok heartstoker'/'SOM', 'Common').
card_artist('vulshok heartstoker'/'SOM', 'Shelly Wan').
card_number('vulshok heartstoker'/'SOM', '107').
card_flavor_text('vulshok heartstoker'/'SOM', 'He fashions stirring words with as much passion as a smith fashioning a warhammer.').
card_multiverse_id('vulshok heartstoker'/'SOM', '194320').
card_watermark('vulshok heartstoker'/'SOM', 'Mirran').

card_in_set('vulshok replica', 'SOM').
card_original_type('vulshok replica'/'SOM', 'Artifact Creature — Berserker').
card_original_text('vulshok replica'/'SOM', '{1}{R}, Sacrifice Vulshok Replica: Vulshok Replica deals 3 damage to target player.').
card_first_print('vulshok replica', 'SOM').
card_image_name('vulshok replica'/'SOM', 'vulshok replica').
card_uid('vulshok replica'/'SOM', 'SOM:Vulshok Replica:vulshok replica').
card_rarity('vulshok replica'/'SOM', 'Common').
card_artist('vulshok replica'/'SOM', 'Zoltan Boros & Gabor Szikszai').
card_number('vulshok replica'/'SOM', '221').
card_flavor_text('vulshok replica'/'SOM', 'All the fury of the Vulshok with only a trace of their recklessness.').
card_multiverse_id('vulshok replica'/'SOM', '209034').
card_watermark('vulshok replica'/'SOM', 'Mirran').

card_in_set('wall of tanglecord', 'SOM').
card_original_type('wall of tanglecord'/'SOM', 'Artifact Creature — Wall').
card_original_text('wall of tanglecord'/'SOM', 'Defender\n{G}: Wall of Tanglecord gains reach until end of turn. (It can block creatures with flying.)').
card_first_print('wall of tanglecord', 'SOM').
card_image_name('wall of tanglecord'/'SOM', 'wall of tanglecord').
card_uid('wall of tanglecord'/'SOM', 'SOM:Wall of Tanglecord:wall of tanglecord').
card_rarity('wall of tanglecord'/'SOM', 'Common').
card_artist('wall of tanglecord'/'SOM', 'Vance Kovacs').
card_number('wall of tanglecord'/'SOM', '222').
card_flavor_text('wall of tanglecord'/'SOM', 'Rootlike fibers travel far from Mirrodin\'s metallic forests, emerging from the crust to drink in the mana-infused sunlight.').
card_multiverse_id('wall of tanglecord'/'SOM', '202652').
card_watermark('wall of tanglecord'/'SOM', 'Mirran').

card_in_set('whitesun\'s passage', 'SOM').
card_original_type('whitesun\'s passage'/'SOM', 'Instant').
card_original_text('whitesun\'s passage'/'SOM', 'You gain 5 life.').
card_first_print('whitesun\'s passage', 'SOM').
card_image_name('whitesun\'s passage'/'SOM', 'whitesun\'s passage').
card_uid('whitesun\'s passage'/'SOM', 'SOM:Whitesun\'s Passage:whitesun\'s passage').
card_rarity('whitesun\'s passage'/'SOM', 'Common').
card_artist('whitesun\'s passage'/'SOM', 'John Avon').
card_number('whitesun\'s passage'/'SOM', '27').
card_flavor_text('whitesun\'s passage'/'SOM', 'All over the Razor Fields, Whitesun is celebrated. Even the followers of the rebel Juryan, far from the Cave of Light, bow their heads in reverence.').
card_multiverse_id('whitesun\'s passage'/'SOM', '206349').
card_watermark('whitesun\'s passage'/'SOM', 'Mirran').

card_in_set('wing puncture', 'SOM').
card_original_type('wing puncture'/'SOM', 'Instant').
card_original_text('wing puncture'/'SOM', 'Target creature you control deals damage equal to its power to target creature with flying.').
card_first_print('wing puncture', 'SOM').
card_image_name('wing puncture'/'SOM', 'wing puncture').
card_uid('wing puncture'/'SOM', 'SOM:Wing Puncture:wing puncture').
card_rarity('wing puncture'/'SOM', 'Common').
card_artist('wing puncture'/'SOM', 'jD').
card_number('wing puncture'/'SOM', '133').
card_flavor_text('wing puncture'/'SOM', '\"If I were the air, I wouldn\'t taunt the ground. It has the weight advantage.\"\n—Konnos, Sylvok sage').
card_multiverse_id('wing puncture'/'SOM', '194097').
card_watermark('wing puncture'/'SOM', 'Mirran').

card_in_set('withstand death', 'SOM').
card_original_type('withstand death'/'SOM', 'Instant').
card_original_text('withstand death'/'SOM', 'Target creature is indestructible this turn. (Lethal damage and effects that say \"destroy\" don\'t destroy it. If its toughness is 0 or less, it\'s still put into its owner\'s graveyard.)').
card_first_print('withstand death', 'SOM').
card_image_name('withstand death'/'SOM', 'withstand death').
card_uid('withstand death'/'SOM', 'SOM:Withstand Death:withstand death').
card_rarity('withstand death'/'SOM', 'Common').
card_artist('withstand death'/'SOM', 'Tomasz Jedruszek').
card_number('withstand death'/'SOM', '134').
card_flavor_text('withstand death'/'SOM', 'On Mirrodin, every conflict ends in either death or darksteel.').
card_multiverse_id('withstand death'/'SOM', '209035').
card_watermark('withstand death'/'SOM', 'Mirran').

card_in_set('wurmcoil engine', 'SOM').
card_original_type('wurmcoil engine'/'SOM', 'Artifact Creature — Wurm').
card_original_text('wurmcoil engine'/'SOM', 'Deathtouch, lifelink\nWhen Wurmcoil Engine is put into a graveyard from the battlefield, put a 3/3 colorless Wurm artifact creature token with deathtouch and a 3/3 colorless Wurm artifact creature token with lifelink onto the battlefield.').
card_image_name('wurmcoil engine'/'SOM', 'wurmcoil engine').
card_uid('wurmcoil engine'/'SOM', 'SOM:Wurmcoil Engine:wurmcoil engine').
card_rarity('wurmcoil engine'/'SOM', 'Mythic Rare').
card_artist('wurmcoil engine'/'SOM', 'Raymond Swanland').
card_number('wurmcoil engine'/'SOM', '223').
card_multiverse_id('wurmcoil engine'/'SOM', '207875').
card_watermark('wurmcoil engine'/'SOM', 'Phyrexian').
