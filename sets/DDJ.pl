% Duel Decks: Izzet vs. Golgari

set('DDJ').
set_name('DDJ', 'Duel Decks: Izzet vs. Golgari').
set_release_date('DDJ', '2012-09-07').
set_border('DDJ', 'black').
set_type('DDJ', 'duel deck').

card_in_set('barren moor', 'DDJ').
card_original_type('barren moor'/'DDJ', 'Land').
card_original_text('barren moor'/'DDJ', 'Barren Moor enters the battlefield tapped.\n{T}: Add {B} to your mana pool.\nCycling {B} ({B}, Discard this card: Draw a card.)').
card_image_name('barren moor'/'DDJ', 'barren moor').
card_uid('barren moor'/'DDJ', 'DDJ:Barren Moor:barren moor').
card_rarity('barren moor'/'DDJ', 'Common').
card_artist('barren moor'/'DDJ', 'Heather Hudson').
card_number('barren moor'/'DDJ', '78').
card_multiverse_id('barren moor'/'DDJ', '292964').

card_in_set('boneyard wurm', 'DDJ').
card_original_type('boneyard wurm'/'DDJ', 'Creature — Wurm').
card_original_text('boneyard wurm'/'DDJ', 'Boneyard Wurm\'s power and toughness are each equal to the number of creature cards in your graveyard.').
card_image_name('boneyard wurm'/'DDJ', 'boneyard wurm').
card_uid('boneyard wurm'/'DDJ', 'DDJ:Boneyard Wurm:boneyard wurm').
card_rarity('boneyard wurm'/'DDJ', 'Uncommon').
card_artist('boneyard wurm'/'DDJ', 'Jaime Jones').
card_number('boneyard wurm'/'DDJ', '51').
card_flavor_text('boneyard wurm'/'DDJ', 'The only thing it likes more than discovering a pit of bones is adding to it.').
card_multiverse_id('boneyard wurm'/'DDJ', '338400').

card_in_set('brain weevil', 'DDJ').
card_original_type('brain weevil'/'DDJ', 'Creature — Insect').
card_original_text('brain weevil'/'DDJ', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)\nSacrifice Brain Weevil: Target player discards two cards. Activate this ability only any time you could cast a sorcery.').
card_image_name('brain weevil'/'DDJ', 'brain weevil').
card_uid('brain weevil'/'DDJ', 'DDJ:Brain Weevil:brain weevil').
card_rarity('brain weevil'/'DDJ', 'Common').
card_artist('brain weevil'/'DDJ', 'Anthony Jones').
card_number('brain weevil'/'DDJ', '58').
card_multiverse_id('brain weevil'/'DDJ', '338401').

card_in_set('brainstorm', 'DDJ').
card_original_type('brainstorm'/'DDJ', 'Instant').
card_original_text('brainstorm'/'DDJ', 'Draw three cards, then put two cards from your hand on top of your library in any order.').
card_image_name('brainstorm'/'DDJ', 'brainstorm').
card_uid('brainstorm'/'DDJ', 'DDJ:Brainstorm:brainstorm').
card_rarity('brainstorm'/'DDJ', 'Common').
card_artist('brainstorm'/'DDJ', 'Willian Murai').
card_number('brainstorm'/'DDJ', '13').
card_flavor_text('brainstorm'/'DDJ', 'The mizzium-sphere array drove her mind deep into the thought field, where only the rarest motes of genius may be plucked.').
card_multiverse_id('brainstorm'/'DDJ', '292751').

card_in_set('call to heel', 'DDJ').
card_original_type('call to heel'/'DDJ', 'Instant').
card_original_text('call to heel'/'DDJ', 'Return target creature to its owner\'s hand. Its controller draws a card.').
card_image_name('call to heel'/'DDJ', 'call to heel').
card_uid('call to heel'/'DDJ', 'DDJ:Call to Heel:call to heel').
card_rarity('call to heel'/'DDJ', 'Common').
card_artist('call to heel'/'DDJ', 'Randy Gallegos').
card_number('call to heel'/'DDJ', '18').
card_flavor_text('call to heel'/'DDJ', 'On Bant, a sigil is both a prize of honor and a bond of duty. The one who bears it may be called to fulfill that charge at any moment.').
card_multiverse_id('call to heel'/'DDJ', '292748').

card_in_set('dakmor salvage', 'DDJ').
card_original_type('dakmor salvage'/'DDJ', 'Land').
card_original_text('dakmor salvage'/'DDJ', 'Dakmor Salvage enters the battlefield tapped.\n{T}: Add {B} to your mana pool.\nDredge 2 (If you would draw a card, instead you may put exactly two cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_image_name('dakmor salvage'/'DDJ', 'dakmor salvage').
card_uid('dakmor salvage'/'DDJ', 'DDJ:Dakmor Salvage:dakmor salvage').
card_rarity('dakmor salvage'/'DDJ', 'Uncommon').
card_artist('dakmor salvage'/'DDJ', 'John Avon').
card_number('dakmor salvage'/'DDJ', '79').
card_multiverse_id('dakmor salvage'/'DDJ', '292984').

card_in_set('death', 'DDJ').
card_original_type('death'/'DDJ', 'Sorcery').
card_original_text('death'/'DDJ', 'All lands you control become 1/1 creatures until end of turn. They\'re still lands.\n//\nDeath\n{1}{B}\nSorcery\nReturn target creature card from your graveyard to the battlefield. You lose life equal to its converted mana cost.\nRyan Barger').
card_image_name('death'/'DDJ', 'lifedeath').
card_uid('death'/'DDJ', 'DDJ:Death:lifedeath').
card_rarity('death'/'DDJ', 'Uncommon').
card_artist('death'/'DDJ', 'Ryan Barger').
card_number('death'/'DDJ', '77b').
card_multiverse_id('death'/'DDJ', '292976').

card_in_set('dissipate', 'DDJ').
card_original_type('dissipate'/'DDJ', 'Instant').
card_original_text('dissipate'/'DDJ', 'Counter target spell. If that spell is countered this way, exile it instead of putting it into its owner\'s graveyard.').
card_image_name('dissipate'/'DDJ', 'dissipate').
card_uid('dissipate'/'DDJ', 'DDJ:Dissipate:dissipate').
card_rarity('dissipate'/'DDJ', 'Uncommon').
card_artist('dissipate'/'DDJ', 'Tomasz Jedruszek').
card_number('dissipate'/'DDJ', '25').
card_flavor_text('dissipate'/'DDJ', '\"This abomination never belonged in our world. I\'m merely setting it free.\"\n—Dierk, geistmage').
card_multiverse_id('dissipate'/'DDJ', '292758').

card_in_set('djinn illuminatus', 'DDJ').
card_original_type('djinn illuminatus'/'DDJ', 'Creature — Djinn').
card_original_text('djinn illuminatus'/'DDJ', 'Flying\nEach instant and sorcery spell you cast has replicate. The replicate cost is equal to its mana cost. (When you cast it, copy it for each time you paid its replicate cost. You may choose new targets for the copies.)').
card_image_name('djinn illuminatus'/'DDJ', 'djinn illuminatus').
card_uid('djinn illuminatus'/'DDJ', 'DDJ:Djinn Illuminatus:djinn illuminatus').
card_rarity('djinn illuminatus'/'DDJ', 'Rare').
card_artist('djinn illuminatus'/'DDJ', 'Carl Critchlow').
card_number('djinn illuminatus'/'DDJ', '12').
card_multiverse_id('djinn illuminatus'/'DDJ', '292730').

card_in_set('doomgape', 'DDJ').
card_original_type('doomgape'/'DDJ', 'Creature — Elemental').
card_original_text('doomgape'/'DDJ', 'Trample\nAt the beginning of your upkeep, sacrifice a creature. You gain life equal to that creature\'s toughness.').
card_image_name('doomgape'/'DDJ', 'doomgape').
card_uid('doomgape'/'DDJ', 'DDJ:Doomgape:doomgape').
card_rarity('doomgape'/'DDJ', 'Rare').
card_artist('doomgape'/'DDJ', 'Dave Allsop').
card_number('doomgape'/'DDJ', '65').
card_flavor_text('doomgape'/'DDJ', 'It eats and eats until the juiciest morsel it can find is itself.').
card_multiverse_id('doomgape'/'DDJ', '292979').

card_in_set('dreg mangler', 'DDJ').
card_original_type('dreg mangler'/'DDJ', 'Creature — Plant Zombie').
card_original_text('dreg mangler'/'DDJ', 'Haste\nScavenge {3}{B}{G} ({3}{B}{G}, Exile this card from your graveyard: Put a number of +1/+1 counters equal to this card\'s power on target creature. Scavenge only as a sorcery.)').
card_image_name('dreg mangler'/'DDJ', 'dreg mangler').
card_uid('dreg mangler'/'DDJ', 'DDJ:Dreg Mangler:dreg mangler').
card_rarity('dreg mangler'/'DDJ', 'Uncommon').
card_artist('dreg mangler'/'DDJ', 'Peter Mohrbacher').
card_number('dreg mangler'/'DDJ', '56').
card_multiverse_id('dreg mangler'/'DDJ', '338412').

card_in_set('elves of deep shadow', 'DDJ').
card_original_type('elves of deep shadow'/'DDJ', 'Creature — Elf Druid').
card_original_text('elves of deep shadow'/'DDJ', '{T}: Add {B} to your mana pool. Elves of Deep Shadow deals 1 damage to you.').
card_image_name('elves of deep shadow'/'DDJ', 'elves of deep shadow').
card_uid('elves of deep shadow'/'DDJ', 'DDJ:Elves of Deep Shadow:elves of deep shadow').
card_rarity('elves of deep shadow'/'DDJ', 'Common').
card_artist('elves of deep shadow'/'DDJ', 'Justin Sweet').
card_number('elves of deep shadow'/'DDJ', '47').
card_flavor_text('elves of deep shadow'/'DDJ', 'Cast out of the Conclave generations ago, these elves found a home in the corrupted districts of the Golgari.').
card_multiverse_id('elves of deep shadow'/'DDJ', '292942').

card_in_set('eternal witness', 'DDJ').
card_original_type('eternal witness'/'DDJ', 'Creature — Human Shaman').
card_original_text('eternal witness'/'DDJ', 'When Eternal Witness enters the battlefield, you may return target card from your graveyard to your hand.').
card_image_name('eternal witness'/'DDJ', 'eternal witness').
card_uid('eternal witness'/'DDJ', 'DDJ:Eternal Witness:eternal witness').
card_rarity('eternal witness'/'DDJ', 'Uncommon').
card_artist('eternal witness'/'DDJ', 'Terese Nielsen').
card_number('eternal witness'/'DDJ', '55').
card_flavor_text('eternal witness'/'DDJ', 'She remembers every word spoken, from the hero\'s oath to the baby\'s cry.').
card_multiverse_id('eternal witness'/'DDJ', '292966').

card_in_set('feast or famine', 'DDJ').
card_original_type('feast or famine'/'DDJ', 'Instant').
card_original_text('feast or famine'/'DDJ', 'Choose one — Put a 2/2 black Zombie creature token onto the battlefield; or destroy target nonartifact, nonblack creature and it can\'t be regenerated.').
card_image_name('feast or famine'/'DDJ', 'feast or famine').
card_uid('feast or famine'/'DDJ', 'DDJ:Feast or Famine:feast or famine').
card_rarity('feast or famine'/'DDJ', 'Common').
card_artist('feast or famine'/'DDJ', 'Chase Stone').
card_number('feast or famine'/'DDJ', '72').
card_flavor_text('feast or famine'/'DDJ', '\"The living cannot understand the benefits of death.\"\n—Chaeska, Keeper of Tresserhorn').
card_multiverse_id('feast or famine'/'DDJ', '338405').

card_in_set('fire', 'DDJ').
card_original_type('fire'/'DDJ', 'Instant').
card_original_text('fire'/'DDJ', 'Fire deals 2 damage divided as you choose among one or two target creatures and/or players.\n//\nIce\n{1}{U}\nTap target permanent.\nDraw a card.\nDan Scott').
card_image_name('fire'/'DDJ', 'fireice').
card_uid('fire'/'DDJ', 'DDJ:Fire:fireice').
card_rarity('fire'/'DDJ', 'Uncommon').
card_artist('fire'/'DDJ', 'Dan Scott').
card_number('fire'/'DDJ', '32a').
card_multiverse_id('fire'/'DDJ', '292753').

card_in_set('force spike', 'DDJ').
card_original_type('force spike'/'DDJ', 'Instant').
card_original_text('force spike'/'DDJ', 'Counter target spell unless its controller pays {1}.').
card_image_name('force spike'/'DDJ', 'force spike').
card_uid('force spike'/'DDJ', 'DDJ:Force Spike:force spike').
card_rarity('force spike'/'DDJ', 'Common').
card_artist('force spike'/'DDJ', 'Nelson DeCastro').
card_number('force spike'/'DDJ', '14').
card_flavor_text('force spike'/'DDJ', '\"I don\'t think so.\"').
card_multiverse_id('force spike'/'DDJ', '292750').

card_in_set('forest', 'DDJ').
card_original_type('forest'/'DDJ', 'Basic Land — Forest').
card_original_text('forest'/'DDJ', 'G').
card_image_name('forest'/'DDJ', 'forest1').
card_uid('forest'/'DDJ', 'DDJ:Forest:forest1').
card_rarity('forest'/'DDJ', 'Basic Land').
card_artist('forest'/'DDJ', 'Stephan Martiniere').
card_number('forest'/'DDJ', '87').
card_multiverse_id('forest'/'DDJ', '292943').

card_in_set('forest', 'DDJ').
card_original_type('forest'/'DDJ', 'Basic Land — Forest').
card_original_text('forest'/'DDJ', 'G').
card_image_name('forest'/'DDJ', 'forest2').
card_uid('forest'/'DDJ', 'DDJ:Forest:forest2').
card_rarity('forest'/'DDJ', 'Basic Land').
card_artist('forest'/'DDJ', 'Christopher Moeller').
card_number('forest'/'DDJ', '88').
card_multiverse_id('forest'/'DDJ', '292945').

card_in_set('forest', 'DDJ').
card_original_type('forest'/'DDJ', 'Basic Land — Forest').
card_original_text('forest'/'DDJ', 'G').
card_image_name('forest'/'DDJ', 'forest3').
card_uid('forest'/'DDJ', 'DDJ:Forest:forest3').
card_rarity('forest'/'DDJ', 'Basic Land').
card_artist('forest'/'DDJ', 'Anthony S. Waters').
card_number('forest'/'DDJ', '89').
card_multiverse_id('forest'/'DDJ', '292944').

card_in_set('forest', 'DDJ').
card_original_type('forest'/'DDJ', 'Basic Land — Forest').
card_original_text('forest'/'DDJ', 'G').
card_image_name('forest'/'DDJ', 'forest4').
card_uid('forest'/'DDJ', 'DDJ:Forest:forest4').
card_rarity('forest'/'DDJ', 'Basic Land').
card_artist('forest'/'DDJ', 'Richard Wright').
card_number('forest'/'DDJ', '90').
card_multiverse_id('forest'/'DDJ', '292946').

card_in_set('forgotten cave', 'DDJ').
card_original_type('forgotten cave'/'DDJ', 'Land').
card_original_text('forgotten cave'/'DDJ', 'Forgotten Cave enters the battlefield tapped.\n{T}: Add {R} to your mana pool.\nCycling {R} ({R}, Discard this card: Draw a card.)').
card_image_name('forgotten cave'/'DDJ', 'forgotten cave').
card_uid('forgotten cave'/'DDJ', 'DDJ:Forgotten Cave:forgotten cave').
card_rarity('forgotten cave'/'DDJ', 'Common').
card_artist('forgotten cave'/'DDJ', 'Tony Szczudlo').
card_number('forgotten cave'/'DDJ', '33').
card_multiverse_id('forgotten cave'/'DDJ', '292761').

card_in_set('galvanoth', 'DDJ').
card_original_type('galvanoth'/'DDJ', 'Creature — Beast').
card_original_text('galvanoth'/'DDJ', 'At the beginning of your upkeep, you may look at the top card of your library. If it\'s an instant or sorcery card, you may cast it without paying its mana cost.').
card_image_name('galvanoth'/'DDJ', 'galvanoth').
card_uid('galvanoth'/'DDJ', 'DDJ:Galvanoth:galvanoth').
card_rarity('galvanoth'/'DDJ', 'Rare').
card_artist('galvanoth'/'DDJ', 'Kev Walker').
card_number('galvanoth'/'DDJ', '10').
card_flavor_text('galvanoth'/'DDJ', 'It chews open Mirrodin\'s husk and feeds on the outpouring of energy.').
card_multiverse_id('galvanoth'/'DDJ', '338469').

card_in_set('gelectrode', 'DDJ').
card_original_type('gelectrode'/'DDJ', 'Creature — Weird').
card_original_text('gelectrode'/'DDJ', '{T}: Gelectrode deals 1 damage to target creature or player.\nWhenever you cast an instant or sorcery spell, you may untap Gelectrode.').
card_image_name('gelectrode'/'DDJ', 'gelectrode').
card_uid('gelectrode'/'DDJ', 'DDJ:Gelectrode:gelectrode').
card_rarity('gelectrode'/'DDJ', 'Uncommon').
card_artist('gelectrode'/'DDJ', 'Dan Scott').
card_number('gelectrode'/'DDJ', '5').
card_flavor_text('gelectrode'/'DDJ', '\"Diametrically opposing energies in self-sealed plasmodermic bubbles make great pets!\"\n—Trivaz, Izzet mage').
card_multiverse_id('gelectrode'/'DDJ', '292731').

card_in_set('ghoul\'s feast', 'DDJ').
card_original_type('ghoul\'s feast'/'DDJ', 'Instant').
card_original_text('ghoul\'s feast'/'DDJ', 'Target creature gets +X/+0 until end of turn, where X is the number of creature cards in your graveyard.').
card_image_name('ghoul\'s feast'/'DDJ', 'ghoul\'s feast').
card_uid('ghoul\'s feast'/'DDJ', 'DDJ:Ghoul\'s Feast:ghoul\'s feast').
card_rarity('ghoul\'s feast'/'DDJ', 'Uncommon').
card_artist('ghoul\'s feast'/'DDJ', 'Alan Pollack').
card_number('ghoul\'s feast'/'DDJ', '67').
card_flavor_text('ghoul\'s feast'/'DDJ', 'Mercadians not wealthy enough to buy a tomb are thrown into a bog called \"the Ghoul\'s Larder.\"').
card_multiverse_id('ghoul\'s feast'/'DDJ', '338407').

card_in_set('gleancrawler', 'DDJ').
card_original_type('gleancrawler'/'DDJ', 'Creature — Insect Horror').
card_original_text('gleancrawler'/'DDJ', 'Trample\nAt the beginning of your end step, return to your hand all creature cards in your graveyard that were put there from the battlefield this turn.').
card_image_name('gleancrawler'/'DDJ', 'gleancrawler').
card_uid('gleancrawler'/'DDJ', 'DDJ:Gleancrawler:gleancrawler').
card_rarity('gleancrawler'/'DDJ', 'Rare').
card_artist('gleancrawler'/'DDJ', 'Jeremy Jarvis').
card_number('gleancrawler'/'DDJ', '64').
card_multiverse_id('gleancrawler'/'DDJ', '338391').

card_in_set('goblin electromancer', 'DDJ').
card_original_type('goblin electromancer'/'DDJ', 'Creature — Goblin Wizard').
card_original_text('goblin electromancer'/'DDJ', 'Instant and sorcery spells you cast cost {1} less to cast.').
card_first_print('goblin electromancer', 'DDJ').
card_image_name('goblin electromancer'/'DDJ', 'goblin electromancer').
card_uid('goblin electromancer'/'DDJ', 'DDJ:Goblin Electromancer:goblin electromancer').
card_rarity('goblin electromancer'/'DDJ', 'Common').
card_artist('goblin electromancer'/'DDJ', 'Svetlin Velinov').
card_number('goblin electromancer'/'DDJ', '3').
card_flavor_text('goblin electromancer'/'DDJ', 'When asked how much power is required, Izzet mages always answer \"more.\"').
card_multiverse_id('goblin electromancer'/'DDJ', '338414').

card_in_set('golgari germination', 'DDJ').
card_original_type('golgari germination'/'DDJ', 'Enchantment').
card_original_text('golgari germination'/'DDJ', 'Whenever a nontoken creature you control dies, put a 1/1 green Saproling creature token onto the battlefield.').
card_image_name('golgari germination'/'DDJ', 'golgari germination').
card_uid('golgari germination'/'DDJ', 'DDJ:Golgari Germination:golgari germination').
card_rarity('golgari germination'/'DDJ', 'Uncommon').
card_artist('golgari germination'/'DDJ', 'Thomas M. Baxa').
card_number('golgari germination'/'DDJ', '70').
card_flavor_text('golgari germination'/'DDJ', 'The Golgari don\'t bury their dead. They plant them.').
card_multiverse_id('golgari germination'/'DDJ', '292948').

card_in_set('golgari grave-troll', 'DDJ').
card_original_type('golgari grave-troll'/'DDJ', 'Creature — Skeleton Troll').
card_original_text('golgari grave-troll'/'DDJ', 'Golgari Grave-Troll enters the battlefield with a +1/+1 counter on it for each creature card in your graveyard.\n{1}, Remove a +1/+1 counter from Golgari Grave-Troll: Regenerate Golgari Grave-Troll.\nDredge 6').
card_image_name('golgari grave-troll'/'DDJ', 'golgari grave-troll').
card_uid('golgari grave-troll'/'DDJ', 'DDJ:Golgari Grave-Troll:golgari grave-troll').
card_rarity('golgari grave-troll'/'DDJ', 'Rare').
card_artist('golgari grave-troll'/'DDJ', 'Greg Hildebrandt').
card_number('golgari grave-troll'/'DDJ', '60').
card_multiverse_id('golgari grave-troll'/'DDJ', '338406').

card_in_set('golgari rot farm', 'DDJ').
card_original_type('golgari rot farm'/'DDJ', 'Land').
card_original_text('golgari rot farm'/'DDJ', 'Golgari Rot Farm enters the battlefield tapped.\nWhen Golgari Rot Farm enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {B}{G} to your mana pool.').
card_image_name('golgari rot farm'/'DDJ', 'golgari rot farm').
card_uid('golgari rot farm'/'DDJ', 'DDJ:Golgari Rot Farm:golgari rot farm').
card_rarity('golgari rot farm'/'DDJ', 'Common').
card_artist('golgari rot farm'/'DDJ', 'John Avon').
card_number('golgari rot farm'/'DDJ', '80').
card_multiverse_id('golgari rot farm'/'DDJ', '292950').

card_in_set('golgari rotwurm', 'DDJ').
card_original_type('golgari rotwurm'/'DDJ', 'Creature — Zombie Wurm').
card_original_text('golgari rotwurm'/'DDJ', '{B}, Sacrifice a creature: Target player loses 1 life.').
card_image_name('golgari rotwurm'/'DDJ', 'golgari rotwurm').
card_uid('golgari rotwurm'/'DDJ', 'DDJ:Golgari Rotwurm:golgari rotwurm').
card_rarity('golgari rotwurm'/'DDJ', 'Common').
card_artist('golgari rotwurm'/'DDJ', 'Wayne England').
card_number('golgari rotwurm'/'DDJ', '63').
card_flavor_text('golgari rotwurm'/'DDJ', 'Like corpse-worms through a carapace, rotwurms slide through the hollowed bones of the undercity.').
card_multiverse_id('golgari rotwurm'/'DDJ', '292951').

card_in_set('golgari signet', 'DDJ').
card_original_type('golgari signet'/'DDJ', 'Artifact').
card_original_text('golgari signet'/'DDJ', '{1}, {T}: Add {B}{G} to your mana pool.').
card_image_name('golgari signet'/'DDJ', 'golgari signet').
card_uid('golgari signet'/'DDJ', 'DDJ:Golgari Signet:golgari signet').
card_rarity('golgari signet'/'DDJ', 'Common').
card_artist('golgari signet'/'DDJ', 'Raoul Vitale').
card_number('golgari signet'/'DDJ', '66').
card_flavor_text('golgari signet'/'DDJ', 'Depending on your point of view, the seal represents a proud guardian of the natural cycle or one who has sold her soul to darkness for eternal life.').
card_multiverse_id('golgari signet'/'DDJ', '292952').

card_in_set('golgari thug', 'DDJ').
card_original_type('golgari thug'/'DDJ', 'Creature — Human Warrior').
card_original_text('golgari thug'/'DDJ', 'When Golgari Thug dies, put target creature card from your graveyard on top of your library.\nDredge 4 (If you would draw a card, instead you may put exactly four cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_image_name('golgari thug'/'DDJ', 'golgari thug').
card_uid('golgari thug'/'DDJ', 'DDJ:Golgari Thug:golgari thug').
card_rarity('golgari thug'/'DDJ', 'Uncommon').
card_artist('golgari thug'/'DDJ', 'Johann Bodin').
card_number('golgari thug'/'DDJ', '48').
card_multiverse_id('golgari thug'/'DDJ', '292953').

card_in_set('greater mossdog', 'DDJ').
card_original_type('greater mossdog'/'DDJ', 'Creature — Plant Hound').
card_original_text('greater mossdog'/'DDJ', 'Dredge 3 (If you would draw a card, instead you may put exactly three cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_image_name('greater mossdog'/'DDJ', 'greater mossdog').
card_uid('greater mossdog'/'DDJ', 'DDJ:Greater Mossdog:greater mossdog').
card_rarity('greater mossdog'/'DDJ', 'Common').
card_artist('greater mossdog'/'DDJ', 'Chippy').
card_number('greater mossdog'/'DDJ', '59').
card_flavor_text('greater mossdog'/'DDJ', 'Man\'s best fungus.').
card_multiverse_id('greater mossdog'/'DDJ', '338472').

card_in_set('grim flowering', 'DDJ').
card_original_type('grim flowering'/'DDJ', 'Sorcery').
card_original_text('grim flowering'/'DDJ', 'Draw a card for each creature card in your graveyard.').
card_image_name('grim flowering'/'DDJ', 'grim flowering').
card_uid('grim flowering'/'DDJ', 'DDJ:Grim Flowering:grim flowering').
card_rarity('grim flowering'/'DDJ', 'Uncommon').
card_artist('grim flowering'/'DDJ', 'Adam Paquette').
card_number('grim flowering'/'DDJ', '75').
card_flavor_text('grim flowering'/'DDJ', '\"Nothing in nature goes to waste, not even the rotting corpse of a good-for-nothing, blood-sucking vampire.\"\n—Halana of Ulvenwald').
card_multiverse_id('grim flowering'/'DDJ', '338390').

card_in_set('ice', 'DDJ').
card_original_type('ice'/'DDJ', 'Instant').
card_original_text('ice'/'DDJ', 'Fire deals 2 damage divided as you choose among one or two target creatures and/or players.\n//\nIce\n{1}{U}\nTap target permanent.\nDraw a card.\nDan Scott').
card_image_name('ice'/'DDJ', 'fireice').
card_uid('ice'/'DDJ', 'DDJ:Ice:fireice').
card_rarity('ice'/'DDJ', 'Uncommon').
card_artist('ice'/'DDJ', 'Dan Scott').
card_number('ice'/'DDJ', '32b').
card_multiverse_id('ice'/'DDJ', '292753').

card_in_set('invoke the firemind', 'DDJ').
card_original_type('invoke the firemind'/'DDJ', 'Sorcery').
card_original_text('invoke the firemind'/'DDJ', 'Choose one — Draw X cards; or Invoke the Firemind deals X damage to target creature or player.').
card_image_name('invoke the firemind'/'DDJ', 'invoke the firemind').
card_uid('invoke the firemind'/'DDJ', 'DDJ:Invoke the Firemind:invoke the firemind').
card_rarity('invoke the firemind'/'DDJ', 'Rare').
card_artist('invoke the firemind'/'DDJ', 'Zoltan Boros & Gabor Szikszai').
card_number('invoke the firemind'/'DDJ', '31').
card_flavor_text('invoke the firemind'/'DDJ', 'To those in tune with the Firemind, there is no difference between knowledge and flame.').
card_multiverse_id('invoke the firemind'/'DDJ', '292732').

card_in_set('island', 'DDJ').
card_original_type('island'/'DDJ', 'Basic Land — Island').
card_original_text('island'/'DDJ', 'U').
card_image_name('island'/'DDJ', 'island1').
card_uid('island'/'DDJ', 'DDJ:Island:island1').
card_rarity('island'/'DDJ', 'Basic Land').
card_artist('island'/'DDJ', 'Stephan Martiniere').
card_number('island'/'DDJ', '37').
card_multiverse_id('island'/'DDJ', '292764').

card_in_set('island', 'DDJ').
card_original_type('island'/'DDJ', 'Basic Land — Island').
card_original_text('island'/'DDJ', 'U').
card_image_name('island'/'DDJ', 'island2').
card_uid('island'/'DDJ', 'DDJ:Island:island2').
card_rarity('island'/'DDJ', 'Basic Land').
card_artist('island'/'DDJ', 'Christopher Moeller').
card_number('island'/'DDJ', '38').
card_multiverse_id('island'/'DDJ', '292765').

card_in_set('island', 'DDJ').
card_original_type('island'/'DDJ', 'Basic Land — Island').
card_original_text('island'/'DDJ', 'U').
card_image_name('island'/'DDJ', 'island3').
card_uid('island'/'DDJ', 'DDJ:Island:island3').
card_rarity('island'/'DDJ', 'Basic Land').
card_artist('island'/'DDJ', 'Anthony S. Waters').
card_number('island'/'DDJ', '39').
card_multiverse_id('island'/'DDJ', '292763').

card_in_set('island', 'DDJ').
card_original_type('island'/'DDJ', 'Basic Land — Island').
card_original_text('island'/'DDJ', 'U').
card_image_name('island'/'DDJ', 'island4').
card_uid('island'/'DDJ', 'DDJ:Island:island4').
card_rarity('island'/'DDJ', 'Basic Land').
card_artist('island'/'DDJ', 'Richard Wright').
card_number('island'/'DDJ', '40').
card_multiverse_id('island'/'DDJ', '292766').

card_in_set('isochron scepter', 'DDJ').
card_original_type('isochron scepter'/'DDJ', 'Artifact').
card_original_text('isochron scepter'/'DDJ', 'Imprint — When Isochron Scepter enters the battlefield, you may exile an instant card with converted mana cost 2 or less from your hand.\n{2}, {T}: You may copy the exiled card. If you do, you may cast the copy without paying its mana cost.').
card_image_name('isochron scepter'/'DDJ', 'isochron scepter').
card_uid('isochron scepter'/'DDJ', 'DDJ:Isochron Scepter:isochron scepter').
card_rarity('isochron scepter'/'DDJ', 'Uncommon').
card_artist('isochron scepter'/'DDJ', 'Mark Harrison').
card_number('isochron scepter'/'DDJ', '16').
card_multiverse_id('isochron scepter'/'DDJ', '292752').

card_in_set('izzet boilerworks', 'DDJ').
card_original_type('izzet boilerworks'/'DDJ', 'Land').
card_original_text('izzet boilerworks'/'DDJ', 'Izzet Boilerworks enters the battlefield tapped.\nWhen Izzet Boilerworks enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {U}{R} to your mana pool.').
card_image_name('izzet boilerworks'/'DDJ', 'izzet boilerworks').
card_uid('izzet boilerworks'/'DDJ', 'DDJ:Izzet Boilerworks:izzet boilerworks').
card_rarity('izzet boilerworks'/'DDJ', 'Common').
card_artist('izzet boilerworks'/'DDJ', 'John Avon').
card_number('izzet boilerworks'/'DDJ', '34').
card_multiverse_id('izzet boilerworks'/'DDJ', '292733').

card_in_set('izzet charm', 'DDJ').
card_original_type('izzet charm'/'DDJ', 'Instant').
card_original_text('izzet charm'/'DDJ', 'Choose one — Counter target noncreature spell unless its controller pays {2}; or Izzet Charm deals 2 damage to target creature; or draw two cards, then discard two cards.').
card_image_name('izzet charm'/'DDJ', 'izzet charm').
card_uid('izzet charm'/'DDJ', 'DDJ:Izzet Charm:izzet charm').
card_rarity('izzet charm'/'DDJ', 'Uncommon').
card_artist('izzet charm'/'DDJ', 'Zoltan Boros').
card_number('izzet charm'/'DDJ', '21').
card_multiverse_id('izzet charm'/'DDJ', '338413').

card_in_set('izzet chronarch', 'DDJ').
card_original_type('izzet chronarch'/'DDJ', 'Creature — Human Wizard').
card_original_text('izzet chronarch'/'DDJ', 'When Izzet Chronarch enters the battlefield, return target instant or sorcery card from your graveyard to your hand.').
card_image_name('izzet chronarch'/'DDJ', 'izzet chronarch').
card_uid('izzet chronarch'/'DDJ', 'DDJ:Izzet Chronarch:izzet chronarch').
card_rarity('izzet chronarch'/'DDJ', 'Common').
card_artist('izzet chronarch'/'DDJ', 'Steven Belledin').
card_number('izzet chronarch'/'DDJ', '11').
card_flavor_text('izzet chronarch'/'DDJ', 'He ensures not only whether but also when and where the lightning strikes twice.').
card_multiverse_id('izzet chronarch'/'DDJ', '292734').

card_in_set('izzet guildmage', 'DDJ').
card_original_type('izzet guildmage'/'DDJ', 'Creature — Human Wizard').
card_original_text('izzet guildmage'/'DDJ', '{2}{U}: Copy target instant spell you control with converted mana cost 2 or less. You may choose new targets for the copy.\n{2}{R}: Copy target sorcery spell you control with converted mana cost 2 or less. You may choose new targets for the copy.').
card_image_name('izzet guildmage'/'DDJ', 'izzet guildmage').
card_uid('izzet guildmage'/'DDJ', 'DDJ:Izzet Guildmage:izzet guildmage').
card_rarity('izzet guildmage'/'DDJ', 'Uncommon').
card_artist('izzet guildmage'/'DDJ', 'Jim Murray').
card_number('izzet guildmage'/'DDJ', '4').
card_multiverse_id('izzet guildmage'/'DDJ', '292735').

card_in_set('izzet signet', 'DDJ').
card_original_type('izzet signet'/'DDJ', 'Artifact').
card_original_text('izzet signet'/'DDJ', '{1}, {T}: Add {U}{R} to your mana pool.').
card_image_name('izzet signet'/'DDJ', 'izzet signet').
card_uid('izzet signet'/'DDJ', 'DDJ:Izzet Signet:izzet signet').
card_rarity('izzet signet'/'DDJ', 'Common').
card_artist('izzet signet'/'DDJ', 'Raoul Vitale').
card_number('izzet signet'/'DDJ', '17').
card_flavor_text('izzet signet'/'DDJ', 'The Izzet signet is redesigned often, each time becoming closer to a vanity portrait of Niv-Mizzet.').
card_multiverse_id('izzet signet'/'DDJ', '292736').

card_in_set('jarad, golgari lich lord', 'DDJ').
card_original_type('jarad, golgari lich lord'/'DDJ', 'Legendary Creature — Zombie Elf').
card_original_text('jarad, golgari lich lord'/'DDJ', 'Jarad, Golgari Lich Lord gets +1/+1 for each creature card in your graveyard.\n{1}{B}{G}, Sacrifice another creature: Each opponent loses life equal to the sacrificed creature\'s power.\nSacrifice a Swamp and a Forest: Return Jarad from your graveyard to your hand.').
card_first_print('jarad, golgari lich lord', 'DDJ').
card_image_name('jarad, golgari lich lord'/'DDJ', 'jarad, golgari lich lord').
card_uid('jarad, golgari lich lord'/'DDJ', 'DDJ:Jarad, Golgari Lich Lord:jarad, golgari lich lord').
card_rarity('jarad, golgari lich lord'/'DDJ', 'Mythic Rare').
card_artist('jarad, golgari lich lord'/'DDJ', 'Svetlin Velinov').
card_number('jarad, golgari lich lord'/'DDJ', '45').
card_multiverse_id('jarad, golgari lich lord'/'DDJ', '292771').

card_in_set('kiln fiend', 'DDJ').
card_original_type('kiln fiend'/'DDJ', 'Creature — Elemental Beast').
card_original_text('kiln fiend'/'DDJ', 'Whenever you cast an instant or sorcery spell, Kiln Fiend gets +3/+0 until end of turn.').
card_image_name('kiln fiend'/'DDJ', 'kiln fiend').
card_uid('kiln fiend'/'DDJ', 'DDJ:Kiln Fiend:kiln fiend').
card_rarity('kiln fiend'/'DDJ', 'Common').
card_artist('kiln fiend'/'DDJ', 'Adi Granov').
card_number('kiln fiend'/'DDJ', '2').
card_flavor_text('kiln fiend'/'DDJ', 'It traps an explosion within its stony skin.').
card_multiverse_id('kiln fiend'/'DDJ', '292742').

card_in_set('korozda guildmage', 'DDJ').
card_original_type('korozda guildmage'/'DDJ', 'Creature — Elf Shaman').
card_original_text('korozda guildmage'/'DDJ', '{1}{B}{G}: Target creature gets +1/+1 and gains intimidate until end of turn.\n{2}{B}{G}, Sacrifice a nontoken creature: Put X 1/1 green Saproling creature tokens onto the battlefield, where X is the sacrificed creature\'s toughness.').
card_first_print('korozda guildmage', 'DDJ').
card_image_name('korozda guildmage'/'DDJ', 'korozda guildmage').
card_uid('korozda guildmage'/'DDJ', 'DDJ:Korozda Guildmage:korozda guildmage').
card_rarity('korozda guildmage'/'DDJ', 'Uncommon').
card_artist('korozda guildmage'/'DDJ', 'Ryan Pancoast').
card_number('korozda guildmage'/'DDJ', '52').
card_multiverse_id('korozda guildmage'/'DDJ', '338415').

card_in_set('life', 'DDJ').
card_original_type('life'/'DDJ', 'Sorcery').
card_original_text('life'/'DDJ', 'All lands you control become 1/1 creatures until end of turn. They\'re still lands.\n//\nDeath\n{1}{B}\nSorcery\nReturn target creature card from your graveyard to the battlefield. You lose life equal to its converted mana cost.\nRyan Barger').
card_image_name('life'/'DDJ', 'lifedeath').
card_uid('life'/'DDJ', 'DDJ:Life:lifedeath').
card_rarity('life'/'DDJ', 'Uncommon').
card_artist('life'/'DDJ', 'Ryan Barger').
card_number('life'/'DDJ', '77a').
card_multiverse_id('life'/'DDJ', '292976').

card_in_set('life from the loam', 'DDJ').
card_original_type('life from the loam'/'DDJ', 'Sorcery').
card_original_text('life from the loam'/'DDJ', 'Return up to three target land cards from your graveyard to your hand.\nDredge 3 (If you would draw a card, instead you may put exactly three cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_image_name('life from the loam'/'DDJ', 'life from the loam').
card_uid('life from the loam'/'DDJ', 'DDJ:Life from the Loam:life from the loam').
card_rarity('life from the loam'/'DDJ', 'Rare').
card_artist('life from the loam'/'DDJ', 'Terese Nielsen').
card_number('life from the loam'/'DDJ', '69').
card_multiverse_id('life from the loam'/'DDJ', '338409').

card_in_set('lonely sandbar', 'DDJ').
card_original_type('lonely sandbar'/'DDJ', 'Land').
card_original_text('lonely sandbar'/'DDJ', 'Lonely Sandbar enters the battlefield tapped.\n{T}: Add {U} to your mana pool.\nCycling {U} ({U}, Discard this card: Draw a card.)').
card_image_name('lonely sandbar'/'DDJ', 'lonely sandbar').
card_uid('lonely sandbar'/'DDJ', 'DDJ:Lonely Sandbar:lonely sandbar').
card_rarity('lonely sandbar'/'DDJ', 'Common').
card_artist('lonely sandbar'/'DDJ', 'Heather Hudson').
card_number('lonely sandbar'/'DDJ', '35').
card_multiverse_id('lonely sandbar'/'DDJ', '292762').

card_in_set('magma spray', 'DDJ').
card_original_type('magma spray'/'DDJ', 'Instant').
card_original_text('magma spray'/'DDJ', 'Magma Spray deals 2 damage to target creature. If that creature would die this turn, exile it instead.').
card_image_name('magma spray'/'DDJ', 'magma spray').
card_uid('magma spray'/'DDJ', 'DDJ:Magma Spray:magma spray').
card_rarity('magma spray'/'DDJ', 'Common').
card_artist('magma spray'/'DDJ', 'Jarreau Wimberly').
card_number('magma spray'/'DDJ', '15').
card_flavor_text('magma spray'/'DDJ', '\"Jund is a wounded world. Beware its searing blood spilling out onto the surface.\"\n—Rakka Mar').
card_multiverse_id('magma spray'/'DDJ', '338470').

card_in_set('mountain', 'DDJ').
card_original_type('mountain'/'DDJ', 'Basic Land — Mountain').
card_original_text('mountain'/'DDJ', 'R').
card_image_name('mountain'/'DDJ', 'mountain1').
card_uid('mountain'/'DDJ', 'DDJ:Mountain:mountain1').
card_rarity('mountain'/'DDJ', 'Basic Land').
card_artist('mountain'/'DDJ', 'Stephan Martiniere').
card_number('mountain'/'DDJ', '41').
card_multiverse_id('mountain'/'DDJ', '292767').

card_in_set('mountain', 'DDJ').
card_original_type('mountain'/'DDJ', 'Basic Land — Mountain').
card_original_text('mountain'/'DDJ', 'R').
card_image_name('mountain'/'DDJ', 'mountain2').
card_uid('mountain'/'DDJ', 'DDJ:Mountain:mountain2').
card_rarity('mountain'/'DDJ', 'Basic Land').
card_artist('mountain'/'DDJ', 'Christopher Moeller').
card_number('mountain'/'DDJ', '42').
card_multiverse_id('mountain'/'DDJ', '292769').

card_in_set('mountain', 'DDJ').
card_original_type('mountain'/'DDJ', 'Basic Land — Mountain').
card_original_text('mountain'/'DDJ', 'R').
card_image_name('mountain'/'DDJ', 'mountain3').
card_uid('mountain'/'DDJ', 'DDJ:Mountain:mountain3').
card_rarity('mountain'/'DDJ', 'Basic Land').
card_artist('mountain'/'DDJ', 'Anthony S. Waters').
card_number('mountain'/'DDJ', '43').
card_multiverse_id('mountain'/'DDJ', '292768').

card_in_set('mountain', 'DDJ').
card_original_type('mountain'/'DDJ', 'Basic Land — Mountain').
card_original_text('mountain'/'DDJ', 'R').
card_image_name('mountain'/'DDJ', 'mountain4').
card_uid('mountain'/'DDJ', 'DDJ:Mountain:mountain4').
card_rarity('mountain'/'DDJ', 'Basic Land').
card_artist('mountain'/'DDJ', 'Richard Wright').
card_number('mountain'/'DDJ', '44').
card_multiverse_id('mountain'/'DDJ', '292770').

card_in_set('nightmare void', 'DDJ').
card_original_type('nightmare void'/'DDJ', 'Sorcery').
card_original_text('nightmare void'/'DDJ', 'Target player reveals his or her hand. You choose a card from it. That player discards that card.\nDredge 2 (If you would draw a card, instead you may put exactly two cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_image_name('nightmare void'/'DDJ', 'nightmare void').
card_uid('nightmare void'/'DDJ', 'DDJ:Nightmare Void:nightmare void').
card_rarity('nightmare void'/'DDJ', 'Uncommon').
card_artist('nightmare void'/'DDJ', 'Chippy').
card_number('nightmare void'/'DDJ', '73').
card_multiverse_id('nightmare void'/'DDJ', '338410').

card_in_set('niv-mizzet, the firemind', 'DDJ').
card_original_type('niv-mizzet, the firemind'/'DDJ', 'Legendary Creature — Dragon Wizard').
card_original_text('niv-mizzet, the firemind'/'DDJ', 'Flying\nWhenever you draw a card, Niv-Mizzet, the Firemind deals 1 damage to target creature or player.\n{T}: Draw a card.').
card_image_name('niv-mizzet, the firemind'/'DDJ', 'niv-mizzet, the firemind').
card_uid('niv-mizzet, the firemind'/'DDJ', 'DDJ:Niv-Mizzet, the Firemind:niv-mizzet, the firemind').
card_rarity('niv-mizzet, the firemind'/'DDJ', 'Mythic Rare').
card_artist('niv-mizzet, the firemind'/'DDJ', 'Svetlin Velinov').
card_number('niv-mizzet, the firemind'/'DDJ', '1').
card_multiverse_id('niv-mizzet, the firemind'/'DDJ', '292738').

card_in_set('nivix, aerie of the firemind', 'DDJ').
card_original_type('nivix, aerie of the firemind'/'DDJ', 'Land').
card_original_text('nivix, aerie of the firemind'/'DDJ', '{T}: Add {1} to your mana pool.\n{2}{U}{R}, {T}: Exile the top card of your library. Until your next turn, you may cast that card if it\'s an instant or sorcery.').
card_image_name('nivix, aerie of the firemind'/'DDJ', 'nivix, aerie of the firemind').
card_uid('nivix, aerie of the firemind'/'DDJ', 'DDJ:Nivix, Aerie of the Firemind:nivix, aerie of the firemind').
card_rarity('nivix, aerie of the firemind'/'DDJ', 'Uncommon').
card_artist('nivix, aerie of the firemind'/'DDJ', 'Martina Pilcerova').
card_number('nivix, aerie of the firemind'/'DDJ', '36').
card_flavor_text('nivix, aerie of the firemind'/'DDJ', 'Niv-Mizzet\'s genius and vanity reverberate throughout the mirrored halls of Nivix.').
card_multiverse_id('nivix, aerie of the firemind'/'DDJ', '292737').

card_in_set('ogre savant', 'DDJ').
card_original_type('ogre savant'/'DDJ', 'Creature — Ogre Wizard').
card_original_text('ogre savant'/'DDJ', 'When Ogre Savant enters the battlefield, if {U} was spent to cast Ogre Savant, return target creature to its owner\'s hand.').
card_image_name('ogre savant'/'DDJ', 'ogre savant').
card_uid('ogre savant'/'DDJ', 'DDJ:Ogre Savant:ogre savant').
card_rarity('ogre savant'/'DDJ', 'Common').
card_artist('ogre savant'/'DDJ', 'Paolo Parente').
card_number('ogre savant'/'DDJ', '9').
card_flavor_text('ogre savant'/'DDJ', 'He\'s an oxymoron.').
card_multiverse_id('ogre savant'/'DDJ', '338389').

card_in_set('overwhelming intellect', 'DDJ').
card_original_type('overwhelming intellect'/'DDJ', 'Instant').
card_original_text('overwhelming intellect'/'DDJ', 'Counter target creature spell. Draw cards equal to that spell\'s converted mana cost.').
card_image_name('overwhelming intellect'/'DDJ', 'overwhelming intellect').
card_uid('overwhelming intellect'/'DDJ', 'DDJ:Overwhelming Intellect:overwhelming intellect').
card_rarity('overwhelming intellect'/'DDJ', 'Uncommon').
card_artist('overwhelming intellect'/'DDJ', 'Alex Horley-Orlandelli').
card_number('overwhelming intellect'/'DDJ', '28').
card_flavor_text('overwhelming intellect'/'DDJ', '\"My brain hurts.\"').
card_multiverse_id('overwhelming intellect'/'DDJ', '292759').

card_in_set('plagued rusalka', 'DDJ').
card_original_type('plagued rusalka'/'DDJ', 'Creature — Spirit').
card_original_text('plagued rusalka'/'DDJ', '{B}, Sacrifice a creature: Target creature gets -1/-1 until end of turn.').
card_image_name('plagued rusalka'/'DDJ', 'plagued rusalka').
card_uid('plagued rusalka'/'DDJ', 'DDJ:Plagued Rusalka:plagued rusalka').
card_rarity('plagued rusalka'/'DDJ', 'Uncommon').
card_artist('plagued rusalka'/'DDJ', 'Alex Horley-Orlandelli').
card_number('plagued rusalka'/'DDJ', '46').
card_flavor_text('plagued rusalka'/'DDJ', '\"Look at her, once filled with innocence. Death has a way of wringing away such . . . deficiencies.\"\n—Savra').
card_multiverse_id('plagued rusalka'/'DDJ', '338396').

card_in_set('prophetic bolt', 'DDJ').
card_original_type('prophetic bolt'/'DDJ', 'Instant').
card_original_text('prophetic bolt'/'DDJ', 'Prophetic Bolt deals 4 damage to target creature or player. Look at the top four cards of your library. Put one of those cards into your hand and the rest on the bottom of your library in any order.').
card_image_name('prophetic bolt'/'DDJ', 'prophetic bolt').
card_uid('prophetic bolt'/'DDJ', 'DDJ:Prophetic Bolt:prophetic bolt').
card_rarity('prophetic bolt'/'DDJ', 'Rare').
card_artist('prophetic bolt'/'DDJ', 'Slawomir Maniak').
card_number('prophetic bolt'/'DDJ', '27').
card_multiverse_id('prophetic bolt'/'DDJ', '338392').

card_in_set('putrefy', 'DDJ').
card_original_type('putrefy'/'DDJ', 'Instant').
card_original_text('putrefy'/'DDJ', 'Destroy target artifact or creature. It can\'t be regenerated.').
card_image_name('putrefy'/'DDJ', 'putrefy').
card_uid('putrefy'/'DDJ', 'DDJ:Putrefy:putrefy').
card_rarity('putrefy'/'DDJ', 'Uncommon').
card_artist('putrefy'/'DDJ', 'Clint Cearley').
card_number('putrefy'/'DDJ', '71').
card_flavor_text('putrefy'/'DDJ', '\"All matter, animate or not, rots when exposed to time. We merely speed up the process.\"\n—Ezoc, Golgari rot farmer').
card_multiverse_id('putrefy'/'DDJ', '292956').

card_in_set('putrid leech', 'DDJ').
card_original_type('putrid leech'/'DDJ', 'Creature — Zombie Leech').
card_original_text('putrid leech'/'DDJ', 'Pay 2 life: Putrid Leech gets +2/+2 until end of turn. Activate this ability only once each turn.').
card_image_name('putrid leech'/'DDJ', 'putrid leech').
card_uid('putrid leech'/'DDJ', 'DDJ:Putrid Leech:putrid leech').
card_rarity('putrid leech'/'DDJ', 'Common').
card_artist('putrid leech'/'DDJ', 'Dave Allsop').
card_number('putrid leech'/'DDJ', '53').
card_flavor_text('putrid leech'/'DDJ', 'It reacted to Naya\'s overabundant food supply by growing additional mouths.').
card_multiverse_id('putrid leech'/'DDJ', '338388').

card_in_set('pyromatics', 'DDJ').
card_original_type('pyromatics'/'DDJ', 'Instant').
card_original_text('pyromatics'/'DDJ', 'Replicate {1}{R} (When you cast this spell, copy it for each time you paid its replicate cost. You may choose new targets for the copies.)\nPyromatics deals 1 damage to target creature or player.').
card_image_name('pyromatics'/'DDJ', 'pyromatics').
card_uid('pyromatics'/'DDJ', 'DDJ:Pyromatics:pyromatics').
card_rarity('pyromatics'/'DDJ', 'Common').
card_artist('pyromatics'/'DDJ', 'Glen Angus').
card_number('pyromatics'/'DDJ', '20').
card_multiverse_id('pyromatics'/'DDJ', '292739').

card_in_set('quicksilver dagger', 'DDJ').
card_original_type('quicksilver dagger'/'DDJ', 'Enchantment — Aura').
card_original_text('quicksilver dagger'/'DDJ', 'Enchant creature\nEnchanted creature has \"{T}: This creature deals 1 damage to target player. You draw a card.\"').
card_image_name('quicksilver dagger'/'DDJ', 'quicksilver dagger').
card_uid('quicksilver dagger'/'DDJ', 'DDJ:Quicksilver Dagger:quicksilver dagger').
card_rarity('quicksilver dagger'/'DDJ', 'Common').
card_artist('quicksilver dagger'/'DDJ', 'Alex Horley-Orlandelli').
card_number('quicksilver dagger'/'DDJ', '26').
card_flavor_text('quicksilver dagger'/'DDJ', 'The dwarves call their quicksilver techniques \"improvisational weaponsmithing.\"').
card_multiverse_id('quicksilver dagger'/'DDJ', '292757').

card_in_set('ravenous rats', 'DDJ').
card_original_type('ravenous rats'/'DDJ', 'Creature — Rat').
card_original_text('ravenous rats'/'DDJ', 'When Ravenous Rats enters the battlefield, target opponent discards a card.').
card_image_name('ravenous rats'/'DDJ', 'ravenous rats').
card_uid('ravenous rats'/'DDJ', 'DDJ:Ravenous Rats:ravenous rats').
card_rarity('ravenous rats'/'DDJ', 'Common').
card_artist('ravenous rats'/'DDJ', 'Carl Critchlow').
card_number('ravenous rats'/'DDJ', '49').
card_flavor_text('ravenous rats'/'DDJ', 'Devouring books is only the beginning. The librarian\'s next.').
card_multiverse_id('ravenous rats'/'DDJ', '338404').

card_in_set('reassembling skeleton', 'DDJ').
card_original_type('reassembling skeleton'/'DDJ', 'Creature — Skeleton Warrior').
card_original_text('reassembling skeleton'/'DDJ', '{1}{B}: Return Reassembling Skeleton from your graveyard to the battlefield tapped.').
card_image_name('reassembling skeleton'/'DDJ', 'reassembling skeleton').
card_uid('reassembling skeleton'/'DDJ', 'DDJ:Reassembling Skeleton:reassembling skeleton').
card_rarity('reassembling skeleton'/'DDJ', 'Uncommon').
card_artist('reassembling skeleton'/'DDJ', 'Austin Hsu').
card_number('reassembling skeleton'/'DDJ', '50').
card_flavor_text('reassembling skeleton'/'DDJ', '\"They may show up with the wrong thigh bone or mandible, but they always show up.\"\n—Zul Ashur, lich lord').
card_multiverse_id('reassembling skeleton'/'DDJ', '338402').

card_in_set('reminisce', 'DDJ').
card_original_type('reminisce'/'DDJ', 'Sorcery').
card_original_text('reminisce'/'DDJ', 'Target player shuffles his or her graveyard into his or her library.').
card_image_name('reminisce'/'DDJ', 'reminisce').
card_uid('reminisce'/'DDJ', 'DDJ:Reminisce:reminisce').
card_rarity('reminisce'/'DDJ', 'Uncommon').
card_artist('reminisce'/'DDJ', 'Ralph Horsley').
card_number('reminisce'/'DDJ', '22').
card_flavor_text('reminisce'/'DDJ', 'Leave the door to the past even slightly ajar and it could be blown off its hinges.').
card_multiverse_id('reminisce'/'DDJ', '338471').

card_in_set('sadistic hypnotist', 'DDJ').
card_original_type('sadistic hypnotist'/'DDJ', 'Creature — Human Minion').
card_original_text('sadistic hypnotist'/'DDJ', 'Sacrifice a creature: Target player discards two cards. Activate this ability only any time you could cast a sorcery.').
card_image_name('sadistic hypnotist'/'DDJ', 'sadistic hypnotist').
card_uid('sadistic hypnotist'/'DDJ', 'DDJ:Sadistic Hypnotist:sadistic hypnotist').
card_rarity('sadistic hypnotist'/'DDJ', 'Uncommon').
card_artist('sadistic hypnotist'/'DDJ', 'Paolo Parente').
card_number('sadistic hypnotist'/'DDJ', '62').
card_flavor_text('sadistic hypnotist'/'DDJ', '\"Victory in the pits is almost as rewarding as the methods I use to achieve it.\"').
card_multiverse_id('sadistic hypnotist'/'DDJ', '338408').

card_in_set('shambling shell', 'DDJ').
card_original_type('shambling shell'/'DDJ', 'Creature — Plant Zombie').
card_original_text('shambling shell'/'DDJ', 'Sacrifice Shambling Shell: Put a +1/+1 counter on target creature.\nDredge 3 (If you would draw a card, instead you may put exactly three cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_image_name('shambling shell'/'DDJ', 'shambling shell').
card_uid('shambling shell'/'DDJ', 'DDJ:Shambling Shell:shambling shell').
card_rarity('shambling shell'/'DDJ', 'Common').
card_artist('shambling shell'/'DDJ', 'Joel Thomas').
card_number('shambling shell'/'DDJ', '57').
card_multiverse_id('shambling shell'/'DDJ', '292957').

card_in_set('shrewd hatchling', 'DDJ').
card_original_type('shrewd hatchling'/'DDJ', 'Creature — Elemental').
card_original_text('shrewd hatchling'/'DDJ', 'Shrewd Hatchling enters the battlefield with four -1/-1 counters on it.\n{U/R}: Target creature can\'t block Shrewd Hatchling this turn.\nWhenever you cast a blue spell, remove a -1/-1 counter from Shrewd Hatchling.\nWhenever you cast a red spell, remove a -1/-1 counter from Shrewd Hatchling.').
card_image_name('shrewd hatchling'/'DDJ', 'shrewd hatchling').
card_uid('shrewd hatchling'/'DDJ', 'DDJ:Shrewd Hatchling:shrewd hatchling').
card_rarity('shrewd hatchling'/'DDJ', 'Uncommon').
card_artist('shrewd hatchling'/'DDJ', 'Carl Frank').
card_number('shrewd hatchling'/'DDJ', '8').
card_multiverse_id('shrewd hatchling'/'DDJ', '338395').

card_in_set('sphinx-bone wand', 'DDJ').
card_original_type('sphinx-bone wand'/'DDJ', 'Artifact').
card_original_text('sphinx-bone wand'/'DDJ', 'Whenever you cast an instant or sorcery spell, you may put a charge counter on Sphinx-Bone Wand. If you do, Sphinx-Bone Wand deals damage equal to the number of charge counters on it to target creature or player.').
card_image_name('sphinx-bone wand'/'DDJ', 'sphinx-bone wand').
card_uid('sphinx-bone wand'/'DDJ', 'DDJ:Sphinx-Bone Wand:sphinx-bone wand').
card_rarity('sphinx-bone wand'/'DDJ', 'Rare').
card_artist('sphinx-bone wand'/'DDJ', 'Franz Vohwinkel').
card_number('sphinx-bone wand'/'DDJ', '29').
card_flavor_text('sphinx-bone wand'/'DDJ', 'Its host\'s final curse lies captured within.').
card_multiverse_id('sphinx-bone wand'/'DDJ', '292760').

card_in_set('steamcore weird', 'DDJ').
card_original_type('steamcore weird'/'DDJ', 'Creature — Weird').
card_original_text('steamcore weird'/'DDJ', 'When Steamcore Weird enters the battlefield, if {R} was spent to cast Steamcore Weird, it deals 2 damage to target creature or player.').
card_image_name('steamcore weird'/'DDJ', 'steamcore weird').
card_uid('steamcore weird'/'DDJ', 'DDJ:Steamcore Weird:steamcore weird').
card_rarity('steamcore weird'/'DDJ', 'Common').
card_artist('steamcore weird'/'DDJ', 'Justin Norman').
card_number('steamcore weird'/'DDJ', '7').
card_flavor_text('steamcore weird'/'DDJ', 'Like many Izzet creations, weirds are based on wild contradictions yet somehow manage to work.').
card_multiverse_id('steamcore weird'/'DDJ', '338393').

card_in_set('stingerfling spider', 'DDJ').
card_original_type('stingerfling spider'/'DDJ', 'Creature — Spider').
card_original_text('stingerfling spider'/'DDJ', 'Reach (This creature can block creatures with flying.)\nWhen Stingerfling Spider enters the battlefield, you may destroy target creature with flying.').
card_image_name('stingerfling spider'/'DDJ', 'stingerfling spider').
card_uid('stingerfling spider'/'DDJ', 'DDJ:Stingerfling Spider:stingerfling spider').
card_rarity('stingerfling spider'/'DDJ', 'Uncommon').
card_artist('stingerfling spider'/'DDJ', 'Dave Allsop').
card_number('stingerfling spider'/'DDJ', '61').
card_flavor_text('stingerfling spider'/'DDJ', 'The juiciest prey is that which grows lazy, thinking itself beyond the reach of danger.').
card_multiverse_id('stingerfling spider'/'DDJ', '338403').

card_in_set('stinkweed imp', 'DDJ').
card_original_type('stinkweed imp'/'DDJ', 'Creature — Imp').
card_original_text('stinkweed imp'/'DDJ', 'Flying\nWhenever Stinkweed Imp deals combat damage to a creature, destroy that creature.\nDredge 5 (If you would draw a card, instead you may put exactly five cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_image_name('stinkweed imp'/'DDJ', 'stinkweed imp').
card_uid('stinkweed imp'/'DDJ', 'DDJ:Stinkweed Imp:stinkweed imp').
card_rarity('stinkweed imp'/'DDJ', 'Common').
card_artist('stinkweed imp'/'DDJ', 'Nils Hamm').
card_number('stinkweed imp'/'DDJ', '54').
card_multiverse_id('stinkweed imp'/'DDJ', '292958').

card_in_set('street spasm', 'DDJ').
card_original_type('street spasm'/'DDJ', 'Instant').
card_original_text('street spasm'/'DDJ', 'Street Spasm deals X damage to target creature without flying you don\'t control.\nOverload {X}{X}{R}{R} (You may cast this spell for its overload cost. If you do, change its text by replacing all instances of \"target\" with \"each.\")').
card_first_print('street spasm', 'DDJ').
card_image_name('street spasm'/'DDJ', 'street spasm').
card_uid('street spasm'/'DDJ', 'DDJ:Street Spasm:street spasm').
card_rarity('street spasm'/'DDJ', 'Uncommon').
card_artist('street spasm'/'DDJ', 'Raymond Swanland').
card_number('street spasm'/'DDJ', '30').
card_multiverse_id('street spasm'/'DDJ', '338411').

card_in_set('svogthos, the restless tomb', 'DDJ').
card_original_type('svogthos, the restless tomb'/'DDJ', 'Land').
card_original_text('svogthos, the restless tomb'/'DDJ', '{T}: Add {1} to your mana pool.\n{3}{B}{G}: Until end of turn, Svogthos, the Restless Tomb becomes a black and green Plant Zombie creature with \"This creature\'s power and toughness are each equal to the number of creature cards in your graveyard.\" It\'s still a land.').
card_image_name('svogthos, the restless tomb'/'DDJ', 'svogthos, the restless tomb').
card_uid('svogthos, the restless tomb'/'DDJ', 'DDJ:Svogthos, the Restless Tomb:svogthos, the restless tomb').
card_rarity('svogthos, the restless tomb'/'DDJ', 'Uncommon').
card_artist('svogthos, the restless tomb'/'DDJ', 'Martina Pilcerova').
card_number('svogthos, the restless tomb'/'DDJ', '81').
card_multiverse_id('svogthos, the restless tomb'/'DDJ', '292959').

card_in_set('swamp', 'DDJ').
card_original_type('swamp'/'DDJ', 'Basic Land — Swamp').
card_original_text('swamp'/'DDJ', 'B').
card_image_name('swamp'/'DDJ', 'swamp1').
card_uid('swamp'/'DDJ', 'DDJ:Swamp:swamp1').
card_rarity('swamp'/'DDJ', 'Basic Land').
card_artist('swamp'/'DDJ', 'Stephan Martiniere').
card_number('swamp'/'DDJ', '83').
card_multiverse_id('swamp'/'DDJ', '292962').

card_in_set('swamp', 'DDJ').
card_original_type('swamp'/'DDJ', 'Basic Land — Swamp').
card_original_text('swamp'/'DDJ', 'B').
card_image_name('swamp'/'DDJ', 'swamp2').
card_uid('swamp'/'DDJ', 'DDJ:Swamp:swamp2').
card_rarity('swamp'/'DDJ', 'Basic Land').
card_artist('swamp'/'DDJ', 'Christopher Moeller').
card_number('swamp'/'DDJ', '84').
card_multiverse_id('swamp'/'DDJ', '292961').

card_in_set('swamp', 'DDJ').
card_original_type('swamp'/'DDJ', 'Basic Land — Swamp').
card_original_text('swamp'/'DDJ', 'B').
card_image_name('swamp'/'DDJ', 'swamp3').
card_uid('swamp'/'DDJ', 'DDJ:Swamp:swamp3').
card_rarity('swamp'/'DDJ', 'Basic Land').
card_artist('swamp'/'DDJ', 'Anthony S. Waters').
card_number('swamp'/'DDJ', '85').
card_multiverse_id('swamp'/'DDJ', '292960').

card_in_set('swamp', 'DDJ').
card_original_type('swamp'/'DDJ', 'Basic Land — Swamp').
card_original_text('swamp'/'DDJ', 'B').
card_image_name('swamp'/'DDJ', 'swamp4').
card_uid('swamp'/'DDJ', 'DDJ:Swamp:swamp4').
card_rarity('swamp'/'DDJ', 'Basic Land').
card_artist('swamp'/'DDJ', 'Richard Wright').
card_number('swamp'/'DDJ', '86').
card_multiverse_id('swamp'/'DDJ', '292963').

card_in_set('thunderheads', 'DDJ').
card_original_type('thunderheads'/'DDJ', 'Instant').
card_original_text('thunderheads'/'DDJ', 'Replicate {2}{U} (When you cast this spell, copy it for each time you paid its replicate cost.)\nPut a 3/3 blue Weird creature token with defender and flying onto the battlefield. Exile it at the beginning of the next end step.').
card_image_name('thunderheads'/'DDJ', 'thunderheads').
card_uid('thunderheads'/'DDJ', 'DDJ:Thunderheads:thunderheads').
card_rarity('thunderheads'/'DDJ', 'Uncommon').
card_artist('thunderheads'/'DDJ', 'Hideaki Takamura').
card_number('thunderheads'/'DDJ', '23').
card_flavor_text('thunderheads'/'DDJ', 'The clouds grew thick, and then they grew teeth.').
card_multiverse_id('thunderheads'/'DDJ', '338397').

card_in_set('train of thought', 'DDJ').
card_original_type('train of thought'/'DDJ', 'Sorcery').
card_original_text('train of thought'/'DDJ', 'Replicate {1}{U} (When you cast this spell, copy it for each time you paid its replicate cost.)\nDraw a card.').
card_image_name('train of thought'/'DDJ', 'train of thought').
card_uid('train of thought'/'DDJ', 'DDJ:Train of Thought:train of thought').
card_rarity('train of thought'/'DDJ', 'Common').
card_artist('train of thought'/'DDJ', 'Matt Thompson').
card_number('train of thought'/'DDJ', '19').
card_flavor_text('train of thought'/'DDJ', '\"But then . . . oh, but . . . which means . . . which would lead to . . . exactly!\"').
card_multiverse_id('train of thought'/'DDJ', '292740').

card_in_set('tranquil thicket', 'DDJ').
card_original_type('tranquil thicket'/'DDJ', 'Land').
card_original_text('tranquil thicket'/'DDJ', 'Tranquil Thicket enters the battlefield tapped.\n{T}: Add {G} to your mana pool.\nCycling {G} ({G}, Discard this card: Draw a card.)').
card_image_name('tranquil thicket'/'DDJ', 'tranquil thicket').
card_uid('tranquil thicket'/'DDJ', 'DDJ:Tranquil Thicket:tranquil thicket').
card_rarity('tranquil thicket'/'DDJ', 'Common').
card_artist('tranquil thicket'/'DDJ', 'Heather Hudson').
card_number('tranquil thicket'/'DDJ', '82').
card_multiverse_id('tranquil thicket'/'DDJ', '292969').

card_in_set('twilight\'s call', 'DDJ').
card_original_type('twilight\'s call'/'DDJ', 'Sorcery').
card_original_text('twilight\'s call'/'DDJ', 'You may cast Twilight\'s Call any time you could cast an instant if you pay {2} more to cast it.\nEach player returns all creature cards from his or her graveyard to the battlefield.').
card_image_name('twilight\'s call'/'DDJ', 'twilight\'s call').
card_uid('twilight\'s call'/'DDJ', 'DDJ:Twilight\'s Call:twilight\'s call').
card_rarity('twilight\'s call'/'DDJ', 'Rare').
card_artist('twilight\'s call'/'DDJ', 'Mark Romanoski').
card_number('twilight\'s call'/'DDJ', '76').
card_flavor_text('twilight\'s call'/'DDJ', 'Twilight falls. We rise.\n—Necropolis inscription').
card_multiverse_id('twilight\'s call'/'DDJ', '338399').

card_in_set('vacuumelt', 'DDJ').
card_original_type('vacuumelt'/'DDJ', 'Sorcery').
card_original_text('vacuumelt'/'DDJ', 'Replicate {2}{U} (When you cast this spell, copy it for each time you paid its replicate cost. You may choose new targets for the copies.)\nReturn target creature to its owner\'s hand.').
card_image_name('vacuumelt'/'DDJ', 'vacuumelt').
card_uid('vacuumelt'/'DDJ', 'DDJ:Vacuumelt:vacuumelt').
card_rarity('vacuumelt'/'DDJ', 'Uncommon').
card_artist('vacuumelt'/'DDJ', 'Nottsuo').
card_number('vacuumelt'/'DDJ', '24').
card_multiverse_id('vacuumelt'/'DDJ', '338398').

card_in_set('vigor mortis', 'DDJ').
card_original_type('vigor mortis'/'DDJ', 'Sorcery').
card_original_text('vigor mortis'/'DDJ', 'Return target creature card from your graveyard to the battlefield. If {G} was spent to cast Vigor Mortis, that creature enters the battlefield with an additional +1/+1 counter on it.').
card_image_name('vigor mortis'/'DDJ', 'vigor mortis').
card_uid('vigor mortis'/'DDJ', 'DDJ:Vigor Mortis:vigor mortis').
card_rarity('vigor mortis'/'DDJ', 'Uncommon').
card_artist('vigor mortis'/'DDJ', 'Dan Scott').
card_number('vigor mortis'/'DDJ', '74').
card_flavor_text('vigor mortis'/'DDJ', 'After fear and weakness rot away, that which remains is stronger than its living form.').
card_multiverse_id('vigor mortis'/'DDJ', '338473').

card_in_set('wee dragonauts', 'DDJ').
card_original_type('wee dragonauts'/'DDJ', 'Creature — Faerie Wizard').
card_original_text('wee dragonauts'/'DDJ', 'Flying\nWhenever you cast an instant or sorcery spell, Wee Dragonauts gets +2/+0 until end of turn.').
card_image_name('wee dragonauts'/'DDJ', 'wee dragonauts').
card_uid('wee dragonauts'/'DDJ', 'DDJ:Wee Dragonauts:wee dragonauts').
card_rarity('wee dragonauts'/'DDJ', 'Common').
card_artist('wee dragonauts'/'DDJ', 'Greg Staples').
card_number('wee dragonauts'/'DDJ', '6').
card_flavor_text('wee dragonauts'/'DDJ', '\"The blazekite is a simple concept, really—just a vehicular application of dragscoop ionics and electropropulsion magnetronics.\"\n—Juzba, Izzet tinker').
card_multiverse_id('wee dragonauts'/'DDJ', '292741').

card_in_set('yoke of the damned', 'DDJ').
card_original_type('yoke of the damned'/'DDJ', 'Enchantment — Aura').
card_original_text('yoke of the damned'/'DDJ', 'Enchant creature\nWhen a creature dies, destroy enchanted creature.').
card_image_name('yoke of the damned'/'DDJ', 'yoke of the damned').
card_uid('yoke of the damned'/'DDJ', 'DDJ:Yoke of the Damned:yoke of the damned').
card_rarity('yoke of the damned'/'DDJ', 'Common').
card_artist('yoke of the damned'/'DDJ', 'Paul Bonner').
card_number('yoke of the damned'/'DDJ', '68').
card_flavor_text('yoke of the damned'/'DDJ', 'The demon\'s yoke is part leash, part noose.').
card_multiverse_id('yoke of the damned'/'DDJ', '338474').
