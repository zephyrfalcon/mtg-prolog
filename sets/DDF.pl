% Duel Decks: Elspeth vs. Tezzeret

set('DDF').
set_name('DDF', 'Duel Decks: Elspeth vs. Tezzeret').
set_release_date('DDF', '2010-09-03').
set_border('DDF', 'black').
set_type('DDF', 'duel deck').

card_in_set('abolish', 'DDF').
card_original_type('abolish'/'DDF', 'Instant').
card_original_text('abolish'/'DDF', 'You may discard a Plains card rather than pay Abolish\'s mana cost.\nDestroy target artifact or enchantment.').
card_image_name('abolish'/'DDF', 'abolish').
card_uid('abolish'/'DDF', 'DDF:Abolish:abolish').
card_rarity('abolish'/'DDF', 'Uncommon').
card_artist('abolish'/'DDF', 'Kev Walker').
card_number('abolish'/'DDF', '29').
card_flavor_text('abolish'/'DDF', 'As war raged on, the young mages became more direct in their tactics, often at great cost.').
card_multiverse_id('abolish'/'DDF', '222765').

card_in_set('æther spellbomb', 'DDF').
card_original_type('æther spellbomb'/'DDF', 'Artifact').
card_original_text('æther spellbomb'/'DDF', '{U}, Sacrifice Æther Spellbomb: Return target creature to its owner\'s hand.\n{1}, Sacrifice Æther Spellbomb: Draw a card.').
card_image_name('æther spellbomb'/'DDF', 'aether spellbomb').
card_uid('æther spellbomb'/'DDF', 'DDF:Æther Spellbomb:aether spellbomb').
card_rarity('æther spellbomb'/'DDF', 'Common').
card_artist('æther spellbomb'/'DDF', 'Jim Nelson').
card_number('æther spellbomb'/'DDF', '61').
card_flavor_text('æther spellbomb'/'DDF', '\"Release that which was never caged.\"\n—Spellbomb inscription').
card_multiverse_id('æther spellbomb'/'DDF', '222720').

card_in_set('angel of salvation', 'DDF').
card_original_type('angel of salvation'/'DDF', 'Creature — Angel').
card_original_text('angel of salvation'/'DDF', 'Flash; convoke (Each creature you tap while casting this spell reduces its cost by {1} or by one mana of that creature\'s color.)\nFlying\nWhen Angel of Salvation enters the battlefield, prevent the next 5 damage that would be dealt this turn to any number of target creatures and/or players, divided as you choose.').
card_image_name('angel of salvation'/'DDF', 'angel of salvation').
card_uid('angel of salvation'/'DDF', 'DDF:Angel of Salvation:angel of salvation').
card_rarity('angel of salvation'/'DDF', 'Rare').
card_artist('angel of salvation'/'DDF', 'D. Alexander Gregory').
card_number('angel of salvation'/'DDF', '20').
card_multiverse_id('angel of salvation'/'DDF', '222774').

card_in_set('arcbound worker', 'DDF').
card_original_type('arcbound worker'/'DDF', 'Artifact Creature — Construct').
card_original_text('arcbound worker'/'DDF', 'Modular 1 (This enters the battlefield with a +1/+1 counter on it. When it\'s put into a graveyard, you may put its +1/+1 counters on target artifact creature.)').
card_image_name('arcbound worker'/'DDF', 'arcbound worker').
card_uid('arcbound worker'/'DDF', 'DDF:Arcbound Worker:arcbound worker').
card_rarity('arcbound worker'/'DDF', 'Common').
card_artist('arcbound worker'/'DDF', 'Darrell Riche').
card_number('arcbound worker'/'DDF', '40').
card_flavor_text('arcbound worker'/'DDF', 'The parts are as strong as the whole.').
card_multiverse_id('arcbound worker'/'DDF', '222733').

card_in_set('argivian restoration', 'DDF').
card_original_type('argivian restoration'/'DDF', 'Sorcery').
card_original_text('argivian restoration'/'DDF', 'Return target artifact card from your graveyard to the battlefield.').
card_image_name('argivian restoration'/'DDF', 'argivian restoration').
card_uid('argivian restoration'/'DDF', 'DDF:Argivian Restoration:argivian restoration').
card_rarity('argivian restoration'/'DDF', 'Uncommon').
card_artist('argivian restoration'/'DDF', 'Roger Raupp').
card_number('argivian restoration'/'DDF', '69').
card_flavor_text('argivian restoration'/'DDF', '\"The Argivian University taught me two things: always look to the past, and never dismiss what appears useless.\"\n—Hanna, Weatherlight navigator').
card_multiverse_id('argivian restoration'/'DDF', '222857').

card_in_set('assembly-worker', 'DDF').
card_original_type('assembly-worker'/'DDF', 'Artifact Creature — Assembly-Worker').
card_original_text('assembly-worker'/'DDF', '{T}: Target Assembly-Worker creature gets +1/+1 until end of turn.').
card_image_name('assembly-worker'/'DDF', 'assembly-worker').
card_uid('assembly-worker'/'DDF', 'DDF:Assembly-Worker:assembly-worker').
card_rarity('assembly-worker'/'DDF', 'Uncommon').
card_artist('assembly-worker'/'DDF', 'Chippy').
card_number('assembly-worker'/'DDF', '45').
card_flavor_text('assembly-worker'/'DDF', 'With their factories long destroyed, some of Mishra\'s creations still toil in remote areas, endlessly performing and reperforming their last orders.').
card_multiverse_id('assembly-worker'/'DDF', '226984').

card_in_set('blinding beam', 'DDF').
card_original_type('blinding beam'/'DDF', 'Instant').
card_original_text('blinding beam'/'DDF', 'Choose one — Tap two target creatures; or creatures don\'t untap during target player\'s next untap step.\nEntwine {1} (Choose both if you pay the entwine cost.)').
card_image_name('blinding beam'/'DDF', 'blinding beam').
card_uid('blinding beam'/'DDF', 'DDF:Blinding Beam:blinding beam').
card_rarity('blinding beam'/'DDF', 'Common').
card_artist('blinding beam'/'DDF', 'Doug Chaffee').
card_number('blinding beam'/'DDF', '28').
card_multiverse_id('blinding beam'/'DDF', '222721').

card_in_set('burrenton bombardier', 'DDF').
card_original_type('burrenton bombardier'/'DDF', 'Creature — Kithkin Soldier').
card_original_text('burrenton bombardier'/'DDF', 'Flying\nReinforce 2—{2}{W} ({2}{W}, Discard this card: Put two +1/+1 counters on target creature.)').
card_image_name('burrenton bombardier'/'DDF', 'burrenton bombardier').
card_uid('burrenton bombardier'/'DDF', 'DDF:Burrenton Bombardier:burrenton bombardier').
card_rarity('burrenton bombardier'/'DDF', 'Common').
card_artist('burrenton bombardier'/'DDF', 'Ron Spencer').
card_number('burrenton bombardier'/'DDF', '11').
card_flavor_text('burrenton bombardier'/'DDF', 'The flasks provide ballast, a means of steering, and—with a little luck—a way to deliver ward-spells to cohorts below.').
card_multiverse_id('burrenton bombardier'/'DDF', '222770').

card_in_set('catapult master', 'DDF').
card_original_type('catapult master'/'DDF', 'Creature — Human Soldier').
card_original_text('catapult master'/'DDF', 'Tap five untapped Soldiers you control: Exile target creature.').
card_image_name('catapult master'/'DDF', 'catapult master').
card_uid('catapult master'/'DDF', 'DDF:Catapult Master:catapult master').
card_rarity('catapult master'/'DDF', 'Rare').
card_artist('catapult master'/'DDF', 'Terese Nielsen').
card_number('catapult master'/'DDF', '18').
card_flavor_text('catapult master'/'DDF', '\"There\'s no ‘I\' in ‘team,\' but there\'s a ‘we\' in ‘weapon.\'\"').
card_multiverse_id('catapult master'/'DDF', '222772').

card_in_set('celestial crusader', 'DDF').
card_original_type('celestial crusader'/'DDF', 'Creature — Spirit').
card_original_text('celestial crusader'/'DDF', 'Flash (You may cast this spell any time you could cast an instant.)\nSplit second (As long as this spell is on the stack, players can\'t cast spells or activate abilities that aren\'t mana abilities.)\nFlying\nOther white creatures get +1/+1.').
card_image_name('celestial crusader'/'DDF', 'celestial crusader').
card_uid('celestial crusader'/'DDF', 'DDF:Celestial Crusader:celestial crusader').
card_rarity('celestial crusader'/'DDF', 'Uncommon').
card_artist('celestial crusader'/'DDF', 'Jim Murray').
card_number('celestial crusader'/'DDF', '14').
card_multiverse_id('celestial crusader'/'DDF', '222748').

card_in_set('clockwork condor', 'DDF').
card_original_type('clockwork condor'/'DDF', 'Artifact Creature — Bird').
card_original_text('clockwork condor'/'DDF', 'Flying\nClockwork Condor enters the battlefield with three +1/+1 counters on it.\nWhenever Clockwork Condor attacks or blocks, remove a +1/+1 counter from it at end of combat.').
card_image_name('clockwork condor'/'DDF', 'clockwork condor').
card_uid('clockwork condor'/'DDF', 'DDF:Clockwork Condor:clockwork condor').
card_rarity('clockwork condor'/'DDF', 'Common').
card_artist('clockwork condor'/'DDF', 'Arnie Swekel').
card_number('clockwork condor'/'DDF', '50').
card_multiverse_id('clockwork condor'/'DDF', '222723').

card_in_set('clockwork hydra', 'DDF').
card_original_type('clockwork hydra'/'DDF', 'Artifact Creature — Hydra').
card_original_text('clockwork hydra'/'DDF', 'Clockwork Hydra enters the battlefield with four +1/+1 counters on it.\nWhenever Clockwork Hydra attacks or blocks, remove a +1/+1 counter from it. If you do, Clockwork Hydra deals 1 damage to target creature or player.\n{T}: Put a +1/+1 counter on Clockwork Hydra.').
card_image_name('clockwork hydra'/'DDF', 'clockwork hydra').
card_uid('clockwork hydra'/'DDF', 'DDF:Clockwork Hydra:clockwork hydra').
card_rarity('clockwork hydra'/'DDF', 'Uncommon').
card_artist('clockwork hydra'/'DDF', 'Daren Bader').
card_number('clockwork hydra'/'DDF', '55').
card_multiverse_id('clockwork hydra'/'DDF', '222749').

card_in_set('conclave equenaut', 'DDF').
card_original_type('conclave equenaut'/'DDF', 'Creature — Human Soldier').
card_original_text('conclave equenaut'/'DDF', 'Convoke (Each creature you tap while casting this spell reduces its cost by {1} or by one mana of that creature\'s color.)\nFlying').
card_image_name('conclave equenaut'/'DDF', 'conclave equenaut').
card_uid('conclave equenaut'/'DDF', 'DDF:Conclave Equenaut:conclave equenaut').
card_rarity('conclave equenaut'/'DDF', 'Common').
card_artist('conclave equenaut'/'DDF', 'Terese Nielsen').
card_number('conclave equenaut'/'DDF', '19').
card_flavor_text('conclave equenaut'/'DDF', 'Equenauts are the seeds of the Conclave, scattered on the four winds, searching for new places to take root.').
card_multiverse_id('conclave equenaut'/'DDF', '222767').

card_in_set('conclave phalanx', 'DDF').
card_original_type('conclave phalanx'/'DDF', 'Creature — Human Soldier').
card_original_text('conclave phalanx'/'DDF', 'Convoke (Each creature you tap while casting this spell reduces its cost by {1} or by one mana of that creature\'s color.)\nWhen Conclave Phalanx enters the battlefield, you gain 1 life for each creature you control.').
card_image_name('conclave phalanx'/'DDF', 'conclave phalanx').
card_uid('conclave phalanx'/'DDF', 'DDF:Conclave Phalanx:conclave phalanx').
card_rarity('conclave phalanx'/'DDF', 'Uncommon').
card_artist('conclave phalanx'/'DDF', 'Wayne Reynolds').
card_number('conclave phalanx'/'DDF', '16').
card_multiverse_id('conclave phalanx'/'DDF', '222768').

card_in_set('contagion clasp', 'DDF').
card_original_type('contagion clasp'/'DDF', 'Artifact').
card_original_text('contagion clasp'/'DDF', 'When Contagion Clasp enters the battlefield, put a -1/-1 counter on target creature.\n{4}, {T}: Proliferate. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there.)').
card_image_name('contagion clasp'/'DDF', 'contagion clasp').
card_uid('contagion clasp'/'DDF', 'DDF:Contagion Clasp:contagion clasp').
card_rarity('contagion clasp'/'DDF', 'Uncommon').
card_artist('contagion clasp'/'DDF', 'Anthony Palumbo').
card_number('contagion clasp'/'DDF', '63').
card_multiverse_id('contagion clasp'/'DDF', '227149').

card_in_set('crusade', 'DDF').
card_original_type('crusade'/'DDF', 'Enchantment').
card_original_text('crusade'/'DDF', 'White creatures get +1/+1.').
card_image_name('crusade'/'DDF', 'crusade').
card_uid('crusade'/'DDF', 'DDF:Crusade:crusade').
card_rarity('crusade'/'DDF', 'Rare').
card_artist('crusade'/'DDF', 'Howard Lyon').
card_number('crusade'/'DDF', '27').
card_flavor_text('crusade'/'DDF', '\"Finally, I understand. Home isn\'t where you rest. It\'s what you fight for.\"\n—Elspeth Tirel').
card_multiverse_id('crusade'/'DDF', '218582').

card_in_set('darksteel citadel', 'DDF').
card_original_type('darksteel citadel'/'DDF', 'Artifact Land').
card_original_text('darksteel citadel'/'DDF', 'Darksteel Citadel is indestructible. (\"Destroy\" effects and lethal damage don\'t destroy it.)\n{T}: Add {1} to your mana pool.').
card_image_name('darksteel citadel'/'DDF', 'darksteel citadel').
card_uid('darksteel citadel'/'DDF', 'DDF:Darksteel Citadel:darksteel citadel').
card_rarity('darksteel citadel'/'DDF', 'Common').
card_artist('darksteel citadel'/'DDF', 'John Avon').
card_number('darksteel citadel'/'DDF', '72').
card_flavor_text('darksteel citadel'/'DDF', 'Panopticon, forge of the Darksteel Eye, home of Mirrodin\'s keeper.').
card_multiverse_id('darksteel citadel'/'DDF', '222734').

card_in_set('daru encampment', 'DDF').
card_original_type('daru encampment'/'DDF', 'Land').
card_original_text('daru encampment'/'DDF', '{T}: Add {1} to your mana pool.\n{W}, {T}: Target Soldier creature gets +1/+1 until end of turn.').
card_image_name('daru encampment'/'DDF', 'daru encampment').
card_uid('daru encampment'/'DDF', 'DDF:Daru Encampment:daru encampment').
card_rarity('daru encampment'/'DDF', 'Uncommon').
card_artist('daru encampment'/'DDF', 'Tony Szczudlo').
card_number('daru encampment'/'DDF', '32').
card_multiverse_id('daru encampment'/'DDF', '222773').

card_in_set('echoing truth', 'DDF').
card_original_type('echoing truth'/'DDF', 'Instant').
card_original_text('echoing truth'/'DDF', 'Return target nonland permanent and all other permanents with the same name as that permanent to their owners\' hands.').
card_image_name('echoing truth'/'DDF', 'echoing truth').
card_uid('echoing truth'/'DDF', 'DDF:Echoing Truth:echoing truth').
card_rarity('echoing truth'/'DDF', 'Common').
card_artist('echoing truth'/'DDF', 'Greg Staples').
card_number('echoing truth'/'DDF', '66').
card_flavor_text('echoing truth'/'DDF', 'A single lie unleashes a tide of disbelief.').
card_multiverse_id('echoing truth'/'DDF', '222735').

card_in_set('elite vanguard', 'DDF').
card_original_type('elite vanguard'/'DDF', 'Creature — Human Soldier').
card_original_text('elite vanguard'/'DDF', '').
card_image_name('elite vanguard'/'DDF', 'elite vanguard').
card_uid('elite vanguard'/'DDF', 'DDF:Elite Vanguard:elite vanguard').
card_rarity('elite vanguard'/'DDF', 'Uncommon').
card_artist('elite vanguard'/'DDF', 'Mark Tedin').
card_number('elite vanguard'/'DDF', '2').
card_flavor_text('elite vanguard'/'DDF', 'The vanguard is skilled at waging war alone. The enemy is often defeated before its reinforcements reach the front.').
card_multiverse_id('elite vanguard'/'DDF', '222710').

card_in_set('elixir of immortality', 'DDF').
card_original_type('elixir of immortality'/'DDF', 'Artifact').
card_original_text('elixir of immortality'/'DDF', '{2}, {T}: You gain 5 life. Shuffle Elixir of Immortality and your graveyard into your library.').
card_image_name('elixir of immortality'/'DDF', 'elixir of immortality').
card_uid('elixir of immortality'/'DDF', 'DDF:Elixir of Immortality:elixir of immortality').
card_rarity('elixir of immortality'/'DDF', 'Uncommon').
card_artist('elixir of immortality'/'DDF', 'Zoltan Boros & Gabor Szikszai').
card_number('elixir of immortality'/'DDF', '62').
card_flavor_text('elixir of immortality'/'DDF', '\"Bottled life. Not as tasty as I\'m used to, rather stale, but it has the same effect.\"\n—Baron Sengir').
card_multiverse_id('elixir of immortality'/'DDF', '222711').

card_in_set('elspeth, knight-errant', 'DDF').
card_original_type('elspeth, knight-errant'/'DDF', 'Planeswalker — Elspeth').
card_original_text('elspeth, knight-errant'/'DDF', '+1: Put a 1/1 white Soldier creature token onto the battlefield.\n+1: Target creature gets +3/+3 and gains flying until end of turn.\n-8: You get an emblem with \"Artifacts, creatures, enchantments, and lands you control are indestructible.\"').
card_image_name('elspeth, knight-errant'/'DDF', 'elspeth, knight-errant').
card_uid('elspeth, knight-errant'/'DDF', 'DDF:Elspeth, Knight-Errant:elspeth, knight-errant').
card_rarity('elspeth, knight-errant'/'DDF', 'Mythic Rare').
card_artist('elspeth, knight-errant'/'DDF', 'Igor Kieryluk').
card_number('elspeth, knight-errant'/'DDF', '1').
card_multiverse_id('elspeth, knight-errant'/'DDF', '217825').

card_in_set('energy chamber', 'DDF').
card_original_type('energy chamber'/'DDF', 'Artifact').
card_original_text('energy chamber'/'DDF', 'At the beginning of your upkeep, choose one — Put a +1/+1 counter on target artifact creature; or put a charge counter on target noncreature artifact.').
card_image_name('energy chamber'/'DDF', 'energy chamber').
card_uid('energy chamber'/'DDF', 'DDF:Energy Chamber:energy chamber').
card_rarity('energy chamber'/'DDF', 'Uncommon').
card_artist('energy chamber'/'DDF', 'Kev Walker').
card_number('energy chamber'/'DDF', '64').
card_multiverse_id('energy chamber'/'DDF', '226711').

card_in_set('esperzoa', 'DDF').
card_original_type('esperzoa'/'DDF', 'Artifact Creature — Jellyfish').
card_original_text('esperzoa'/'DDF', 'Flying\nAt the beginning of your upkeep, return an artifact you control to its owner\'s hand.').
card_image_name('esperzoa'/'DDF', 'esperzoa').
card_uid('esperzoa'/'DDF', 'DDF:Esperzoa:esperzoa').
card_rarity('esperzoa'/'DDF', 'Uncommon').
card_artist('esperzoa'/'DDF', 'Warren Mahy').
card_number('esperzoa'/'DDF', '47').
card_flavor_text('esperzoa'/'DDF', 'The more metal it digests, the more its jelly will fetch on the alchemists\' market.').
card_multiverse_id('esperzoa'/'DDF', '226715').

card_in_set('everflowing chalice', 'DDF').
card_original_type('everflowing chalice'/'DDF', 'Artifact').
card_original_text('everflowing chalice'/'DDF', 'Multikicker {2} (You may pay an additional {2} any number of times as you cast this spell.)\nEverflowing Chalice enters the battlefield with a charge counter on it for each time it was kicked.\n{T}: Add {1} to your mana pool for each charge counter on Everflowing Chalice.').
card_image_name('everflowing chalice'/'DDF', 'everflowing chalice').
card_uid('everflowing chalice'/'DDF', 'DDF:Everflowing Chalice:everflowing chalice').
card_rarity('everflowing chalice'/'DDF', 'Uncommon').
card_artist('everflowing chalice'/'DDF', 'Steve Argyle').
card_number('everflowing chalice'/'DDF', '60').
card_multiverse_id('everflowing chalice'/'DDF', '222750').

card_in_set('faerie mechanist', 'DDF').
card_original_type('faerie mechanist'/'DDF', 'Artifact Creature — Faerie Artificer').
card_original_text('faerie mechanist'/'DDF', 'Flying\nWhen Faerie Mechanist enters the battlefield, look at the top three cards of your library. You may reveal an artifact card from among them and put it into your hand. Put the rest on the bottom of your library in any order.').
card_image_name('faerie mechanist'/'DDF', 'faerie mechanist').
card_uid('faerie mechanist'/'DDF', 'DDF:Faerie Mechanist:faerie mechanist').
card_rarity('faerie mechanist'/'DDF', 'Common').
card_artist('faerie mechanist'/'DDF', 'Matt Cavotta').
card_number('faerie mechanist'/'DDF', '54').
card_multiverse_id('faerie mechanist'/'DDF', '222747').

card_in_set('foil', 'DDF').
card_original_type('foil'/'DDF', 'Instant').
card_original_text('foil'/'DDF', 'You may discard an Island card and another card rather than pay Foil\'s mana cost.\nCounter target spell.').
card_image_name('foil'/'DDF', 'foil').
card_uid('foil'/'DDF', 'DDF:Foil:foil').
card_rarity('foil'/'DDF', 'Uncommon').
card_artist('foil'/'DDF', 'Bradley Williams').
card_number('foil'/'DDF', '70').
card_multiverse_id('foil'/'DDF', '222766').

card_in_set('frogmite', 'DDF').
card_original_type('frogmite'/'DDF', 'Artifact Creature — Frog').
card_original_text('frogmite'/'DDF', 'Affinity for artifacts (This spell costs {1} less to cast for each artifact you control.)').
card_image_name('frogmite'/'DDF', 'frogmite').
card_uid('frogmite'/'DDF', 'DDF:Frogmite:frogmite').
card_rarity('frogmite'/'DDF', 'Common').
card_artist('frogmite'/'DDF', 'Terese Nielsen').
card_number('frogmite'/'DDF', '51').
card_flavor_text('frogmite'/'DDF', 'At first, vedalken observers thought blinkmoths naturally avoided certain places. Then they realized those places were frogmite feeding grounds.').
card_multiverse_id('frogmite'/'DDF', '222856').

card_in_set('glory seeker', 'DDF').
card_original_type('glory seeker'/'DDF', 'Creature — Human Soldier').
card_original_text('glory seeker'/'DDF', '').
card_image_name('glory seeker'/'DDF', 'glory seeker').
card_uid('glory seeker'/'DDF', 'DDF:Glory Seeker:glory seeker').
card_rarity('glory seeker'/'DDF', 'Common').
card_artist('glory seeker'/'DDF', 'Matt Cavotta').
card_number('glory seeker'/'DDF', '7').
card_flavor_text('glory seeker'/'DDF', '\"There\'s no contract to sign, no oath to swear. The enlistment procedure is to unsheathe your sword and point it at the enemy.\"').
card_multiverse_id('glory seeker'/'DDF', '222736').

card_in_set('goldmeadow harrier', 'DDF').
card_original_type('goldmeadow harrier'/'DDF', 'Creature — Kithkin Soldier').
card_original_text('goldmeadow harrier'/'DDF', '{W}, {T}: Tap target creature.').
card_image_name('goldmeadow harrier'/'DDF', 'goldmeadow harrier').
card_uid('goldmeadow harrier'/'DDF', 'DDF:Goldmeadow Harrier:goldmeadow harrier').
card_rarity('goldmeadow harrier'/'DDF', 'Common').
card_artist('goldmeadow harrier'/'DDF', 'Steve Prescott').
card_number('goldmeadow harrier'/'DDF', '3').
card_flavor_text('goldmeadow harrier'/'DDF', '\"It\'s a proven fact that sling-stones from the dawn side of the riverbank sail the farthest and truest.\"\n—Deagan, cenn of Burrenton').
card_multiverse_id('goldmeadow harrier'/'DDF', '222708').

card_in_set('infantry veteran', 'DDF').
card_original_type('infantry veteran'/'DDF', 'Creature — Human Soldier').
card_original_text('infantry veteran'/'DDF', '{T}: Target attacking creature gets +1/+1 until end of turn.').
card_image_name('infantry veteran'/'DDF', 'infantry veteran').
card_uid('infantry veteran'/'DDF', 'DDF:Infantry Veteran:infantry veteran').
card_rarity('infantry veteran'/'DDF', 'Common').
card_artist('infantry veteran'/'DDF', 'Christopher Rush').
card_number('infantry veteran'/'DDF', '4').
card_flavor_text('infantry veteran'/'DDF', '\"Any fool can pick up a sword and charge into battle. To emerge alive at the end, that is a gift that comes from leadership and experience.\"').
card_multiverse_id('infantry veteran'/'DDF', '222712').

card_in_set('island', 'DDF').
card_original_type('island'/'DDF', 'Basic Land — Island').
card_original_text('island'/'DDF', 'U').
card_image_name('island'/'DDF', 'island1').
card_uid('island'/'DDF', 'DDF:Island:island1').
card_rarity('island'/'DDF', 'Basic Land').
card_artist('island'/'DDF', 'Rob Alexander').
card_number('island'/'DDF', '76').
card_multiverse_id('island'/'DDF', '222725').

card_in_set('island', 'DDF').
card_original_type('island'/'DDF', 'Basic Land — Island').
card_original_text('island'/'DDF', 'U').
card_image_name('island'/'DDF', 'island2').
card_uid('island'/'DDF', 'DDF:Island:island2').
card_rarity('island'/'DDF', 'Basic Land').
card_artist('island'/'DDF', 'John Avon').
card_number('island'/'DDF', '77').
card_multiverse_id('island'/'DDF', '222727').

card_in_set('island', 'DDF').
card_original_type('island'/'DDF', 'Basic Land — Island').
card_original_text('island'/'DDF', 'U').
card_image_name('island'/'DDF', 'island3').
card_uid('island'/'DDF', 'DDF:Island:island3').
card_rarity('island'/'DDF', 'Basic Land').
card_artist('island'/'DDF', 'Chippy').
card_number('island'/'DDF', '78').
card_multiverse_id('island'/'DDF', '222726').

card_in_set('island', 'DDF').
card_original_type('island'/'DDF', 'Basic Land — Island').
card_original_text('island'/'DDF', 'U').
card_image_name('island'/'DDF', 'island4').
card_uid('island'/'DDF', 'DDF:Island:island4').
card_rarity('island'/'DDF', 'Basic Land').
card_artist('island'/'DDF', 'Chippy').
card_number('island'/'DDF', '79').
card_multiverse_id('island'/'DDF', '222724').

card_in_set('journey to nowhere', 'DDF').
card_original_type('journey to nowhere'/'DDF', 'Enchantment').
card_original_text('journey to nowhere'/'DDF', 'When Journey to Nowhere enters the battlefield, exile target creature.\nWhen Journey to Nowhere leaves the battlefield, return the exiled card to the battlefield under its owner\'s control.').
card_image_name('journey to nowhere'/'DDF', 'journey to nowhere').
card_uid('journey to nowhere'/'DDF', 'DDF:Journey to Nowhere:journey to nowhere').
card_rarity('journey to nowhere'/'DDF', 'Common').
card_artist('journey to nowhere'/'DDF', 'Warren Mahy').
card_number('journey to nowhere'/'DDF', '23').
card_multiverse_id('journey to nowhere'/'DDF', '222761').

card_in_set('juggernaut', 'DDF').
card_original_type('juggernaut'/'DDF', 'Artifact Creature — Juggernaut').
card_original_text('juggernaut'/'DDF', 'Juggernaut attacks each turn if able.\nJuggernaut can\'t be blocked by Walls.').
card_image_name('juggernaut'/'DDF', 'juggernaut').
card_uid('juggernaut'/'DDF', 'DDF:Juggernaut:juggernaut').
card_rarity('juggernaut'/'DDF', 'Uncommon').
card_artist('juggernaut'/'DDF', 'Mark Hyzer').
card_number('juggernaut'/'DDF', '52').
card_flavor_text('juggernaut'/'DDF', 'The mighty city of An Karras, with its rich history, built over thousands of years, fell in three hours.').
card_multiverse_id('juggernaut'/'DDF', '222713').

card_in_set('kabira crossroads', 'DDF').
card_original_type('kabira crossroads'/'DDF', 'Land').
card_original_text('kabira crossroads'/'DDF', 'Kabira Crossroads enters the battlefield tapped.\nWhen Kabira Crossroads enters the battlefield, you gain 2 life.\n{T}: Add {W} to your mana pool.').
card_image_name('kabira crossroads'/'DDF', 'kabira crossroads').
card_uid('kabira crossroads'/'DDF', 'DDF:Kabira Crossroads:kabira crossroads').
card_rarity('kabira crossroads'/'DDF', 'Common').
card_artist('kabira crossroads'/'DDF', 'James Paick').
card_number('kabira crossroads'/'DDF', '33').
card_multiverse_id('kabira crossroads'/'DDF', '222762').

card_in_set('kemba\'s skyguard', 'DDF').
card_original_type('kemba\'s skyguard'/'DDF', 'Creature — Cat Knight').
card_original_text('kemba\'s skyguard'/'DDF', 'Flying\nWhen Kemba\'s Skyguard enters the battlefield, you gain 2 life.').
card_first_print('kemba\'s skyguard', 'DDF').
card_image_name('kemba\'s skyguard'/'DDF', 'kemba\'s skyguard').
card_uid('kemba\'s skyguard'/'DDF', 'DDF:Kemba\'s Skyguard:kemba\'s skyguard').
card_rarity('kemba\'s skyguard'/'DDF', 'Common').
card_artist('kemba\'s skyguard'/'DDF', 'Whit Brachna').
card_number('kemba\'s skyguard'/'DDF', '13').
card_flavor_text('kemba\'s skyguard'/'DDF', '\"We\'re now to dispense aid to any Mirran we see battling anything . . . ‘strange.\' Regent\'s orders.\"\n—Ranya, skyhunter captain').
card_multiverse_id('kemba\'s skyguard'/'DDF', '227150').

card_in_set('kor aeronaut', 'DDF').
card_original_type('kor aeronaut'/'DDF', 'Creature — Kor Soldier').
card_original_text('kor aeronaut'/'DDF', 'Kicker {1}{W} (You may pay an additional {1}{W} as you cast this spell.)\nFlying\nWhen Kor Aeronaut enters the battlefield, if it was kicked, target creature gains flying until end of turn.').
card_image_name('kor aeronaut'/'DDF', 'kor aeronaut').
card_uid('kor aeronaut'/'DDF', 'DDF:Kor Aeronaut:kor aeronaut').
card_rarity('kor aeronaut'/'DDF', 'Uncommon').
card_artist('kor aeronaut'/'DDF', 'Karl Kopinski').
card_number('kor aeronaut'/'DDF', '10').
card_multiverse_id('kor aeronaut'/'DDF', '226714').

card_in_set('kor hookmaster', 'DDF').
card_original_type('kor hookmaster'/'DDF', 'Creature — Kor Soldier').
card_original_text('kor hookmaster'/'DDF', 'When Kor Hookmaster enters the battlefield, tap target creature an opponent controls. That creature doesn\'t untap during its controller\'s next untap step.').
card_image_name('kor hookmaster'/'DDF', 'kor hookmaster').
card_uid('kor hookmaster'/'DDF', 'DDF:Kor Hookmaster:kor hookmaster').
card_rarity('kor hookmaster'/'DDF', 'Common').
card_artist('kor hookmaster'/'DDF', 'Wayne Reynolds').
card_number('kor hookmaster'/'DDF', '12').
card_flavor_text('kor hookmaster'/'DDF', '\"For us, a rope represents the ties that bind the kor. For you, it\'s more literal.\"').
card_multiverse_id('kor hookmaster'/'DDF', '222763').

card_in_set('kor skyfisher', 'DDF').
card_original_type('kor skyfisher'/'DDF', 'Creature — Kor Soldier').
card_original_text('kor skyfisher'/'DDF', 'Flying\nWhen Kor Skyfisher enters the battlefield, return a permanent you control to its owner\'s hand.').
card_image_name('kor skyfisher'/'DDF', 'kor skyfisher').
card_uid('kor skyfisher'/'DDF', 'DDF:Kor Skyfisher:kor skyfisher').
card_rarity('kor skyfisher'/'DDF', 'Common').
card_artist('kor skyfisher'/'DDF', 'Dan Scott').
card_number('kor skyfisher'/'DDF', '8').
card_flavor_text('kor skyfisher'/'DDF', '\"Sometimes I snare the unexpected, but I know its purpose will be revealed in time.\"').
card_multiverse_id('kor skyfisher'/'DDF', '222764').

card_in_set('loyal sentry', 'DDF').
card_original_type('loyal sentry'/'DDF', 'Creature — Human Soldier').
card_original_text('loyal sentry'/'DDF', 'When Loyal Sentry blocks a creature, destroy that creature and Loyal Sentry.').
card_image_name('loyal sentry'/'DDF', 'loyal sentry').
card_uid('loyal sentry'/'DDF', 'DDF:Loyal Sentry:loyal sentry').
card_rarity('loyal sentry'/'DDF', 'Rare').
card_artist('loyal sentry'/'DDF', 'Michael Sutfin').
card_number('loyal sentry'/'DDF', '5').
card_flavor_text('loyal sentry'/'DDF', '\"My cause is simple: To stop you, at any cost, from ever seeing the inside of this keep.\"').
card_multiverse_id('loyal sentry'/'DDF', '222706').

card_in_set('master of etherium', 'DDF').
card_original_type('master of etherium'/'DDF', 'Artifact Creature — Vedalken Wizard').
card_original_text('master of etherium'/'DDF', 'Master of Etherium\'s power and toughness are each equal to the number of artifacts you control.\nOther artifact creatures you control get +1/+1.').
card_image_name('master of etherium'/'DDF', 'master of etherium').
card_uid('master of etherium'/'DDF', 'DDF:Master of Etherium:master of etherium').
card_rarity('master of etherium'/'DDF', 'Rare').
card_artist('master of etherium'/'DDF', 'Matt Cavotta').
card_number('master of etherium'/'DDF', '48').
card_flavor_text('master of etherium'/'DDF', '\"Only a mind unfettered with the concerns of the flesh can see the world as it truly is.\"').
card_multiverse_id('master of etherium'/'DDF', '222744').

card_in_set('mighty leap', 'DDF').
card_original_type('mighty leap'/'DDF', 'Instant').
card_original_text('mighty leap'/'DDF', 'Target creature gets +2/+2 and gains flying until end of turn.').
card_image_name('mighty leap'/'DDF', 'mighty leap').
card_uid('mighty leap'/'DDF', 'DDF:Mighty Leap:mighty leap').
card_rarity('mighty leap'/'DDF', 'Common').
card_artist('mighty leap'/'DDF', 'rk post').
card_number('mighty leap'/'DDF', '24').
card_flavor_text('mighty leap'/'DDF', '\"The southern fortress taken by invaders? Heh, sure . . . when elephants fly.\"\n—Brezard Skeinbow, captain of the guard').
card_multiverse_id('mighty leap'/'DDF', '226713').

card_in_set('mishra\'s factory', 'DDF').
card_original_type('mishra\'s factory'/'DDF', 'Land').
card_original_text('mishra\'s factory'/'DDF', '{T}: Add {1} to your mana pool.\n{1}: Mishra\'s Factory becomes a 2/2 Assembly-Worker artifact creature until end of turn. It\'s still a land.\n{T}: Target Assembly-Worker creature gets +1/+1 until end of turn.').
card_image_name('mishra\'s factory'/'DDF', 'mishra\'s factory').
card_uid('mishra\'s factory'/'DDF', 'DDF:Mishra\'s Factory:mishra\'s factory').
card_rarity('mishra\'s factory'/'DDF', 'Uncommon').
card_artist('mishra\'s factory'/'DDF', 'Scott Chou').
card_number('mishra\'s factory'/'DDF', '73').
card_multiverse_id('mishra\'s factory'/'DDF', '218340').

card_in_set('moonglove extract', 'DDF').
card_original_type('moonglove extract'/'DDF', 'Artifact').
card_original_text('moonglove extract'/'DDF', 'Sacrifice Moonglove Extract: Moonglove Extract deals 2 damage to target creature or player.').
card_image_name('moonglove extract'/'DDF', 'moonglove extract').
card_uid('moonglove extract'/'DDF', 'DDF:Moonglove Extract:moonglove extract').
card_rarity('moonglove extract'/'DDF', 'Common').
card_artist('moonglove extract'/'DDF', 'Terese Nielsen').
card_number('moonglove extract'/'DDF', '67').
card_flavor_text('moonglove extract'/'DDF', 'Diluted, moonglove can etch living tissue. Concentrated, a drop will kill a giant.').
card_multiverse_id('moonglove extract'/'DDF', '222709').

card_in_set('mosquito guard', 'DDF').
card_original_type('mosquito guard'/'DDF', 'Creature — Kithkin Soldier').
card_original_text('mosquito guard'/'DDF', 'First strike\nReinforce 1—{1}{W} ({1}{W}, Discard this card: Put a +1/+1 counter on target creature.)').
card_image_name('mosquito guard'/'DDF', 'mosquito guard').
card_uid('mosquito guard'/'DDF', 'DDF:Mosquito Guard:mosquito guard').
card_rarity('mosquito guard'/'DDF', 'Common').
card_artist('mosquito guard'/'DDF', 'Randy Gallegos').
card_number('mosquito guard'/'DDF', '6').
card_flavor_text('mosquito guard'/'DDF', 'His aim is as sure as a mosquito\'s sting, but with none of the warning.').
card_multiverse_id('mosquito guard'/'DDF', '222716').

card_in_set('pentavus', 'DDF').
card_original_type('pentavus'/'DDF', 'Artifact Creature — Construct').
card_original_text('pentavus'/'DDF', 'Pentavus enters the battlefield with five +1/+1 counters on it.\n{1}, Remove a +1/+1 counter from Pentavus: Put a 1/1 colorless Pentavite artifact creature token with flying onto the battlefield.\n{1}, Sacrifice a Pentavite: Put a +1/+1 counter on Pentavus.').
card_image_name('pentavus'/'DDF', 'pentavus').
card_uid('pentavus'/'DDF', 'DDF:Pentavus:pentavus').
card_rarity('pentavus'/'DDF', 'Rare').
card_artist('pentavus'/'DDF', 'Greg Staples').
card_number('pentavus'/'DDF', '58').
card_multiverse_id('pentavus'/'DDF', '227151').

card_in_set('plains', 'DDF').
card_original_type('plains'/'DDF', 'Basic Land — Plains').
card_original_text('plains'/'DDF', 'W').
card_image_name('plains'/'DDF', 'plains1').
card_uid('plains'/'DDF', 'DDF:Plains:plains1').
card_rarity('plains'/'DDF', 'Basic Land').
card_artist('plains'/'DDF', 'Rob Alexander').
card_number('plains'/'DDF', '35').
card_multiverse_id('plains'/'DDF', '222754').

card_in_set('plains', 'DDF').
card_original_type('plains'/'DDF', 'Basic Land — Plains').
card_original_text('plains'/'DDF', 'W').
card_image_name('plains'/'DDF', 'plains2').
card_uid('plains'/'DDF', 'DDF:Plains:plains2').
card_rarity('plains'/'DDF', 'Basic Land').
card_artist('plains'/'DDF', 'Michael Komarck').
card_number('plains'/'DDF', '36').
card_multiverse_id('plains'/'DDF', '222752').

card_in_set('plains', 'DDF').
card_original_type('plains'/'DDF', 'Basic Land — Plains').
card_original_text('plains'/'DDF', 'W').
card_image_name('plains'/'DDF', 'plains3').
card_uid('plains'/'DDF', 'DDF:Plains:plains3').
card_rarity('plains'/'DDF', 'Basic Land').
card_artist('plains'/'DDF', 'Michael Komarck').
card_number('plains'/'DDF', '37').
card_multiverse_id('plains'/'DDF', '222753').

card_in_set('plains', 'DDF').
card_original_type('plains'/'DDF', 'Basic Land — Plains').
card_original_text('plains'/'DDF', 'W').
card_image_name('plains'/'DDF', 'plains4').
card_uid('plains'/'DDF', 'DDF:Plains:plains4').
card_rarity('plains'/'DDF', 'Basic Land').
card_artist('plains'/'DDF', 'Martina Pilcerova').
card_number('plains'/'DDF', '38').
card_multiverse_id('plains'/'DDF', '222751').

card_in_set('qumulox', 'DDF').
card_original_type('qumulox'/'DDF', 'Creature — Beast').
card_original_text('qumulox'/'DDF', 'Affinity for artifacts (This spell costs {1} less to cast for each artifact you control.)\nFlying').
card_image_name('qumulox'/'DDF', 'qumulox').
card_uid('qumulox'/'DDF', 'DDF:Qumulox:qumulox').
card_rarity('qumulox'/'DDF', 'Uncommon').
card_artist('qumulox'/'DDF', 'Carl Critchlow').
card_number('qumulox'/'DDF', '59').
card_flavor_text('qumulox'/'DDF', 'Even the clouds bend themselves to Memnarch\'s will, eager to swallow those who oppose him.').
card_multiverse_id('qumulox'/'DDF', '222739').

card_in_set('raise the alarm', 'DDF').
card_original_type('raise the alarm'/'DDF', 'Instant').
card_original_text('raise the alarm'/'DDF', 'Put two 1/1 white Soldier creature tokens onto the battlefield.').
card_image_name('raise the alarm'/'DDF', 'raise the alarm').
card_uid('raise the alarm'/'DDF', 'DDF:Raise the Alarm:raise the alarm').
card_rarity('raise the alarm'/'DDF', 'Common').
card_artist('raise the alarm'/'DDF', 'John Matson').
card_number('raise the alarm'/'DDF', '25').
card_flavor_text('raise the alarm'/'DDF', '\"The nim raid our homes without warning. We must defend our homes without hesitation.\"').
card_multiverse_id('raise the alarm'/'DDF', '222728').

card_in_set('razor barrier', 'DDF').
card_original_type('razor barrier'/'DDF', 'Instant').
card_original_text('razor barrier'/'DDF', 'Target permanent you control gains protection from artifacts or from the color of your choice until end of turn.').
card_image_name('razor barrier'/'DDF', 'razor barrier').
card_uid('razor barrier'/'DDF', 'DDF:Razor Barrier:razor barrier').
card_rarity('razor barrier'/'DDF', 'Common').
card_artist('razor barrier'/'DDF', 'Ron Spencer').
card_number('razor barrier'/'DDF', '26').
card_flavor_text('razor barrier'/'DDF', '\"We protect our homelands. Why should they not protect us?\"').
card_multiverse_id('razor barrier'/'DDF', '222729').

card_in_set('razormane masticore', 'DDF').
card_original_type('razormane masticore'/'DDF', 'Artifact Creature — Masticore').
card_original_text('razormane masticore'/'DDF', 'First strike\nAt the beginning of your upkeep, sacrifice Razormane Masticore unless you discard a card.\nAt the beginning of your draw step, you may have Razormane Masticore deal 3 damage to target creature.').
card_image_name('razormane masticore'/'DDF', 'razormane masticore').
card_uid('razormane masticore'/'DDF', 'DDF:Razormane Masticore:razormane masticore').
card_rarity('razormane masticore'/'DDF', 'Rare').
card_artist('razormane masticore'/'DDF', 'Jim Murray').
card_number('razormane masticore'/'DDF', '56').
card_multiverse_id('razormane masticore'/'DDF', '222707').

card_in_set('runed servitor', 'DDF').
card_original_type('runed servitor'/'DDF', 'Artifact Creature — Construct').
card_original_text('runed servitor'/'DDF', 'When Runed Servitor is put into a graveyard from the battlefield, each player draws a card.').
card_image_name('runed servitor'/'DDF', 'runed servitor').
card_uid('runed servitor'/'DDF', 'DDF:Runed Servitor:runed servitor').
card_rarity('runed servitor'/'DDF', 'Uncommon').
card_artist('runed servitor'/'DDF', 'Mike Bierek').
card_number('runed servitor'/'DDF', '42').
card_flavor_text('runed servitor'/'DDF', 'Scholars had puzzled for centuries over the ruins at Tal Terig. Its secrets had always lived within one rune-carved head.').
card_multiverse_id('runed servitor'/'DDF', '222737').

card_in_set('rustic clachan', 'DDF').
card_original_type('rustic clachan'/'DDF', 'Land').
card_original_text('rustic clachan'/'DDF', 'As Rustic Clachan enters the battlefield, you may reveal a Kithkin card from your hand. If you don\'t, Rustic Clachan enters the battlefield tapped.\n{T}: Add {W} to your mana pool.\nReinforce 1—{1}{W} ({1}{W}, Discard this card: Put a +1/+1 counter on target creature.)').
card_image_name('rustic clachan'/'DDF', 'rustic clachan').
card_uid('rustic clachan'/'DDF', 'DDF:Rustic Clachan:rustic clachan').
card_rarity('rustic clachan'/'DDF', 'Rare').
card_artist('rustic clachan'/'DDF', 'Fred Fields').
card_number('rustic clachan'/'DDF', '34').
card_multiverse_id('rustic clachan'/'DDF', '222717').

card_in_set('saltblast', 'DDF').
card_original_type('saltblast'/'DDF', 'Sorcery').
card_original_text('saltblast'/'DDF', 'Destroy target nonwhite permanent.').
card_image_name('saltblast'/'DDF', 'saltblast').
card_uid('saltblast'/'DDF', 'DDF:Saltblast:saltblast').
card_rarity('saltblast'/'DDF', 'Uncommon').
card_artist('saltblast'/'DDF', 'Paolo Parente').
card_number('saltblast'/'DDF', '30').
card_flavor_text('saltblast'/'DDF', 'Dominaria erodes with each passing gust.').
card_multiverse_id('saltblast'/'DDF', '222775').

card_in_set('seasoned marshal', 'DDF').
card_original_type('seasoned marshal'/'DDF', 'Creature — Human Soldier').
card_original_text('seasoned marshal'/'DDF', 'Whenever Seasoned Marshal attacks, you may tap target creature.').
card_image_name('seasoned marshal'/'DDF', 'seasoned marshal').
card_uid('seasoned marshal'/'DDF', 'DDF:Seasoned Marshal:seasoned marshal').
card_rarity('seasoned marshal'/'DDF', 'Uncommon').
card_artist('seasoned marshal'/'DDF', 'Matthew D. Wilson').
card_number('seasoned marshal'/'DDF', '15').
card_flavor_text('seasoned marshal'/'DDF', 'There are only two rules of tactics: never be without a plan, and never rely on it.').
card_multiverse_id('seasoned marshal'/'DDF', '222771').

card_in_set('seat of the synod', 'DDF').
card_original_type('seat of the synod'/'DDF', 'Artifact Land').
card_original_text('seat of the synod'/'DDF', '{T}: Add {U} to your mana pool.').
card_image_name('seat of the synod'/'DDF', 'seat of the synod').
card_uid('seat of the synod'/'DDF', 'DDF:Seat of the Synod:seat of the synod').
card_rarity('seat of the synod'/'DDF', 'Common').
card_artist('seat of the synod'/'DDF', 'John Avon').
card_number('seat of the synod'/'DDF', '74').
card_flavor_text('seat of the synod'/'DDF', 'Lumengrid, site of the Knowledge Pool, source of vedalken arcana.').
card_multiverse_id('seat of the synod'/'DDF', '222740').

card_in_set('serrated biskelion', 'DDF').
card_original_type('serrated biskelion'/'DDF', 'Artifact Creature — Construct').
card_original_text('serrated biskelion'/'DDF', '{T}: Put a -1/-1 counter on Serrated Biskelion and a -1/-1 counter on target creature.').
card_image_name('serrated biskelion'/'DDF', 'serrated biskelion').
card_uid('serrated biskelion'/'DDF', 'DDF:Serrated Biskelion:serrated biskelion').
card_rarity('serrated biskelion'/'DDF', 'Uncommon').
card_artist('serrated biskelion'/'DDF', 'Ron Spencer').
card_number('serrated biskelion'/'DDF', '46').
card_flavor_text('serrated biskelion'/'DDF', '\"Whereas I was created to protect, the biskelion was created to destroy.\"\n—Karn, silver golem').
card_multiverse_id('serrated biskelion'/'DDF', '222743').

card_in_set('silver myr', 'DDF').
card_original_type('silver myr'/'DDF', 'Artifact Creature — Myr').
card_original_text('silver myr'/'DDF', '{T}: Add {U} to your mana pool.').
card_image_name('silver myr'/'DDF', 'silver myr').
card_uid('silver myr'/'DDF', 'DDF:Silver Myr:silver myr').
card_rarity('silver myr'/'DDF', 'Common').
card_artist('silver myr'/'DDF', 'Kev Walker').
card_number('silver myr'/'DDF', '43').
card_flavor_text('silver myr'/'DDF', 'The vedalken saw the myr as toys, unaware of the intelligence lurking behind their empty eyes.').
card_multiverse_id('silver myr'/'DDF', '222741').

card_in_set('stalking stones', 'DDF').
card_original_type('stalking stones'/'DDF', 'Land').
card_original_text('stalking stones'/'DDF', '{T}: Add {1} to your mana pool.\n{6}: Stalking Stones becomes a 3/3 Elemental artifact creature that\'s still a land.').
card_image_name('stalking stones'/'DDF', 'stalking stones').
card_uid('stalking stones'/'DDF', 'DDF:Stalking Stones:stalking stones').
card_rarity('stalking stones'/'DDF', 'Uncommon').
card_artist('stalking stones'/'DDF', 'David Day').
card_number('stalking stones'/'DDF', '75').
card_multiverse_id('stalking stones'/'DDF', '222730').

card_in_set('steel overseer', 'DDF').
card_original_type('steel overseer'/'DDF', 'Artifact Creature — Construct').
card_original_text('steel overseer'/'DDF', '{T}: Put a +1/+1 counter on each artifact creature you control.').
card_image_name('steel overseer'/'DDF', 'steel overseer').
card_uid('steel overseer'/'DDF', 'DDF:Steel Overseer:steel overseer').
card_rarity('steel overseer'/'DDF', 'Rare').
card_artist('steel overseer'/'DDF', 'Chris Rahn').
card_number('steel overseer'/'DDF', '44').
card_flavor_text('steel overseer'/'DDF', '\"The world is already run by all manner of machines. One day, they\'ll remind us of that fact.\"\n—Sargis Haz, artificer').
card_multiverse_id('steel overseer'/'DDF', '222714').

card_in_set('steel wall', 'DDF').
card_original_type('steel wall'/'DDF', 'Artifact Creature — Wall').
card_original_text('steel wall'/'DDF', 'Defender').
card_image_name('steel wall'/'DDF', 'steel wall').
card_uid('steel wall'/'DDF', 'DDF:Steel Wall:steel wall').
card_rarity('steel wall'/'DDF', 'Common').
card_artist('steel wall'/'DDF', 'David Day').
card_number('steel wall'/'DDF', '41').
card_flavor_text('steel wall'/'DDF', '\"We sculpt the land into what we need—homes, armament, fortresses of war. Our strength comes not only from knowing, but from commanding the terrain.\"\n—Raksha Golden Cub, leonin kha').
card_multiverse_id('steel wall'/'DDF', '222731').

card_in_set('stormfront riders', 'DDF').
card_original_type('stormfront riders'/'DDF', 'Creature — Human Soldier').
card_original_text('stormfront riders'/'DDF', 'Flying\nWhen Stormfront Riders enters the battlefield, return two creatures you control to their owner\'s hand.\nWhenever Stormfront Riders or another creature is returned to your hand from the battlefield, put a 1/1 white Soldier creature token onto the battlefield.').
card_image_name('stormfront riders'/'DDF', 'stormfront riders').
card_uid('stormfront riders'/'DDF', 'DDF:Stormfront Riders:stormfront riders').
card_rarity('stormfront riders'/'DDF', 'Uncommon').
card_artist('stormfront riders'/'DDF', 'Wayne Reynolds').
card_number('stormfront riders'/'DDF', '17').
card_multiverse_id('stormfront riders'/'DDF', '226716').

card_in_set('sunlance', 'DDF').
card_original_type('sunlance'/'DDF', 'Sorcery').
card_original_text('sunlance'/'DDF', 'Sunlance deals 3 damage to target nonwhite creature.').
card_image_name('sunlance'/'DDF', 'sunlance').
card_uid('sunlance'/'DDF', 'DDF:Sunlance:sunlance').
card_rarity('sunlance'/'DDF', 'Common').
card_artist('sunlance'/'DDF', 'Volkan Baga').
card_number('sunlance'/'DDF', '21').
card_flavor_text('sunlance'/'DDF', '\"It\'s easy for the innocent to speak of justice. They seldom feel its terrible power.\"\n—Orim, Samite inquisitor').
card_multiverse_id('sunlance'/'DDF', '222776').

card_in_set('swell of courage', 'DDF').
card_original_type('swell of courage'/'DDF', 'Instant').
card_original_text('swell of courage'/'DDF', 'Creatures you control get +2/+2 until end of turn.\nReinforce X—{X}{W}{W} ({X}{W}{W}, Discard this card: Put X +1/+1 counters on target creature.)').
card_image_name('swell of courage'/'DDF', 'swell of courage').
card_uid('swell of courage'/'DDF', 'DDF:Swell of Courage:swell of courage').
card_rarity('swell of courage'/'DDF', 'Uncommon').
card_artist('swell of courage'/'DDF', 'Jim Nelson').
card_number('swell of courage'/'DDF', '31').
card_flavor_text('swell of courage'/'DDF', '\"Tideshaping is more than creating a few\nnew puddles.\"').
card_multiverse_id('swell of courage'/'DDF', '222718').

card_in_set('swords to plowshares', 'DDF').
card_original_type('swords to plowshares'/'DDF', 'Instant').
card_original_text('swords to plowshares'/'DDF', 'Exile target creature. Its controller gains life equal to its power.').
card_image_name('swords to plowshares'/'DDF', 'swords to plowshares').
card_uid('swords to plowshares'/'DDF', 'DDF:Swords to Plowshares:swords to plowshares').
card_rarity('swords to plowshares'/'DDF', 'Uncommon').
card_artist('swords to plowshares'/'DDF', 'Terese Nielsen').
card_number('swords to plowshares'/'DDF', '22').
card_flavor_text('swords to plowshares'/'DDF', 'The smallest seed of regret can bloom into redemption.').
card_multiverse_id('swords to plowshares'/'DDF', '218581').

card_in_set('synod centurion', 'DDF').
card_original_type('synod centurion'/'DDF', 'Artifact Creature — Construct').
card_original_text('synod centurion'/'DDF', 'When you control no other artifacts, sacrifice Synod Centurion.').
card_image_name('synod centurion'/'DDF', 'synod centurion').
card_uid('synod centurion'/'DDF', 'DDF:Synod Centurion:synod centurion').
card_rarity('synod centurion'/'DDF', 'Uncommon').
card_artist('synod centurion'/'DDF', 'Kev Walker').
card_number('synod centurion'/'DDF', '53').
card_flavor_text('synod centurion'/'DDF', '\"We order them to stand, they stand. We order them to wait, they wait. We order them to die, they die.\"\n—Pontifex, elder researcher').
card_multiverse_id('synod centurion'/'DDF', '226712').

card_in_set('temple acolyte', 'DDF').
card_original_type('temple acolyte'/'DDF', 'Creature — Human Cleric').
card_original_text('temple acolyte'/'DDF', 'When Temple Acolyte enters the battlefield, you gain 3 life.').
card_image_name('temple acolyte'/'DDF', 'temple acolyte').
card_uid('temple acolyte'/'DDF', 'DDF:Temple Acolyte:temple acolyte').
card_rarity('temple acolyte'/'DDF', 'Common').
card_artist('temple acolyte'/'DDF', 'Lubov').
card_number('temple acolyte'/'DDF', '9').
card_flavor_text('temple acolyte'/'DDF', 'Young, yes. Inexperienced, yes. Weak? Don\'t count on it.').
card_multiverse_id('temple acolyte'/'DDF', '222760').

card_in_set('tezzeret the seeker', 'DDF').
card_original_type('tezzeret the seeker'/'DDF', 'Planeswalker — Tezzeret').
card_original_text('tezzeret the seeker'/'DDF', '+1: Untap up to two target artifacts.\n-X: Search your library for an artifact card with converted mana cost X or less and put it onto the battlefield. Then shuffle your library.\n-5: Artifacts you control become 5/5 artifact creatures until end of turn.').
card_image_name('tezzeret the seeker'/'DDF', 'tezzeret the seeker').
card_uid('tezzeret the seeker'/'DDF', 'DDF:Tezzeret the Seeker:tezzeret the seeker').
card_rarity('tezzeret the seeker'/'DDF', 'Mythic Rare').
card_artist('tezzeret the seeker'/'DDF', 'Igor Kieryluk').
card_number('tezzeret the seeker'/'DDF', '39').
card_multiverse_id('tezzeret the seeker'/'DDF', '217826').

card_in_set('thirst for knowledge', 'DDF').
card_original_type('thirst for knowledge'/'DDF', 'Instant').
card_original_text('thirst for knowledge'/'DDF', 'Draw three cards. Then discard two cards unless you discard an artifact card.').
card_image_name('thirst for knowledge'/'DDF', 'thirst for knowledge').
card_uid('thirst for knowledge'/'DDF', 'DDF:Thirst for Knowledge:thirst for knowledge').
card_rarity('thirst for knowledge'/'DDF', 'Uncommon').
card_artist('thirst for knowledge'/'DDF', 'Anthony Francisco').
card_number('thirst for knowledge'/'DDF', '68').
card_flavor_text('thirst for knowledge'/'DDF', '\"To truly know something, you must become it. The trick is to not lose yourself in the process.\"').
card_multiverse_id('thirst for knowledge'/'DDF', '218580').

card_in_set('thoughtcast', 'DDF').
card_original_type('thoughtcast'/'DDF', 'Sorcery').
card_original_text('thoughtcast'/'DDF', 'Affinity for artifacts (This spell costs {1} less to cast for each artifact you control.)\nDraw two cards.').
card_image_name('thoughtcast'/'DDF', 'thoughtcast').
card_uid('thoughtcast'/'DDF', 'DDF:Thoughtcast:thoughtcast').
card_rarity('thoughtcast'/'DDF', 'Common').
card_artist('thoughtcast'/'DDF', 'Greg Hildebrandt').
card_number('thoughtcast'/'DDF', '71').
card_flavor_text('thoughtcast'/'DDF', 'Vedalken eyes don\'t see the beauty in things. They see only what those things can teach.').
card_multiverse_id('thoughtcast'/'DDF', '222732').

card_in_set('trinket mage', 'DDF').
card_original_type('trinket mage'/'DDF', 'Creature — Human Wizard').
card_original_text('trinket mage'/'DDF', 'When Trinket Mage enters the battlefield, you may search your library for an artifact card with converted mana cost 1 or less, reveal that card, and put it into your hand. If you do, shuffle your library.').
card_image_name('trinket mage'/'DDF', 'trinket mage').
card_uid('trinket mage'/'DDF', 'DDF:Trinket Mage:trinket mage').
card_rarity('trinket mage'/'DDF', 'Common').
card_artist('trinket mage'/'DDF', 'Mark A. Nelson').
card_number('trinket mage'/'DDF', '49').
card_multiverse_id('trinket mage'/'DDF', '222746').

card_in_set('trip noose', 'DDF').
card_original_type('trip noose'/'DDF', 'Artifact').
card_original_text('trip noose'/'DDF', '{2}, {T}: Tap target creature.').
card_image_name('trip noose'/'DDF', 'trip noose').
card_uid('trip noose'/'DDF', 'DDF:Trip Noose:trip noose').
card_rarity('trip noose'/'DDF', 'Uncommon').
card_artist('trip noose'/'DDF', 'Randy Gallegos').
card_number('trip noose'/'DDF', '65').
card_flavor_text('trip noose'/'DDF', 'A taut slipknot trigger is the only thing standing between you and standing.').
card_multiverse_id('trip noose'/'DDF', '222759').

card_in_set('triskelion', 'DDF').
card_original_type('triskelion'/'DDF', 'Artifact Creature — Construct').
card_original_text('triskelion'/'DDF', 'Triskelion enters the battlefield with three +1/+1 counters on it.\nRemove a +1/+1 counter from Triskelion: Triskelion deals 1 damage to target creature or player.').
card_image_name('triskelion'/'DDF', 'triskelion').
card_uid('triskelion'/'DDF', 'DDF:Triskelion:triskelion').
card_rarity('triskelion'/'DDF', 'Rare').
card_artist('triskelion'/'DDF', 'Christopher Moeller').
card_number('triskelion'/'DDF', '57').
card_flavor_text('triskelion'/'DDF', '\"Why do bad things always come in threes?\"\n—Gisulf, expedition survivor').
card_multiverse_id('triskelion'/'DDF', '222715').
