% Dragon Con

set('pDRC').
set_name('pDRC', 'Dragon Con').
set_release_date('pDRC', '1994-01-01').
set_border('pDRC', 'black').
set_type('pDRC', 'promo').

card_in_set('nalathni dragon', 'pDRC').
card_original_type('nalathni dragon'/'pDRC', 'Creature — Dragon').
card_original_text('nalathni dragon'/'pDRC', '').
card_first_print('nalathni dragon', 'pDRC').
card_image_name('nalathni dragon'/'pDRC', 'nalathni dragon').
card_uid('nalathni dragon'/'pDRC', 'pDRC:Nalathni Dragon:nalathni dragon').
card_rarity('nalathni dragon'/'pDRC', 'Special').
card_artist('nalathni dragon'/'pDRC', 'Michael Whelan').
card_number('nalathni dragon'/'pDRC', '1').
card_flavor_text('nalathni dragon'/'pDRC', 'These small but intelligent Dragons and their Olesian allies held back the tide of Pashalik Mons\'s hordes of Goblin Raiders.\nDragonCon 1994').
card_multiverse_id('nalathni dragon'/'pDRC', '97050').
