% Grand Prix

set('pGPX').
set_name('pGPX', 'Grand Prix').
set_release_date('pGPX', '2007-02-24').
set_border('pGPX', 'black').
set_type('pGPX', 'promo').

card_in_set('all is dust', 'pGPX').
card_original_type('all is dust'/'pGPX', 'Tribal Sorcery — Eldrazi').
card_original_text('all is dust'/'pGPX', '').
card_first_print('all is dust', 'pGPX').
card_image_name('all is dust'/'pGPX', 'all is dust').
card_uid('all is dust'/'pGPX', 'pGPX:All Is Dust:all is dust').
card_rarity('all is dust'/'pGPX', 'Special').
card_artist('all is dust'/'pGPX', 'Vincent Proce').
card_number('all is dust'/'pGPX', '9').
card_flavor_text('all is dust'/'pGPX', '\"The emergence of the Eldrazi isn\'t necessarily a bad thing, as long as you\'ve already lived a fulfilling and complete life without regrets.\"\n—Javad Nasrin, Ondu relic hunter').

card_in_set('batterskull', 'pGPX').
card_original_type('batterskull'/'pGPX', 'Artifact — Equipment').
card_original_text('batterskull'/'pGPX', '').
card_first_print('batterskull', 'pGPX').
card_image_name('batterskull'/'pGPX', 'batterskull').
card_uid('batterskull'/'pGPX', 'pGPX:Batterskull:batterskull').
card_rarity('batterskull'/'pGPX', 'Special').
card_artist('batterskull'/'pGPX', 'Igor Kieryluk').
card_number('batterskull'/'pGPX', '10').

card_in_set('call of the herd', 'pGPX').
card_original_type('call of the herd'/'pGPX', 'Sorcery').
card_original_text('call of the herd'/'pGPX', '').
card_image_name('call of the herd'/'pGPX', 'call of the herd').
card_uid('call of the herd'/'pGPX', 'pGPX:Call of the Herd:call of the herd').
card_rarity('call of the herd'/'pGPX', 'Special').
card_artist('call of the herd'/'pGPX', 'Lars Grant-West').
card_number('call of the herd'/'pGPX', '2').

card_in_set('chrome mox', 'pGPX').
card_original_type('chrome mox'/'pGPX', 'Artifact').
card_original_text('chrome mox'/'pGPX', '').
card_image_name('chrome mox'/'pGPX', 'chrome mox').
card_uid('chrome mox'/'pGPX', 'pGPX:Chrome Mox:chrome mox').
card_rarity('chrome mox'/'pGPX', 'Special').
card_artist('chrome mox'/'pGPX', 'Alan Pollack').
card_number('chrome mox'/'pGPX', '3').

card_in_set('goblin guide', 'pGPX').
card_original_type('goblin guide'/'pGPX', 'Creature — Goblin Scout').
card_original_text('goblin guide'/'pGPX', '').
card_first_print('goblin guide', 'pGPX').
card_image_name('goblin guide'/'pGPX', 'goblin guide').
card_uid('goblin guide'/'pGPX', 'pGPX:Goblin Guide:goblin guide').
card_rarity('goblin guide'/'pGPX', 'Special').
card_artist('goblin guide'/'pGPX', 'Steve Prescott').
card_number('goblin guide'/'pGPX', '6').
card_flavor_text('goblin guide'/'pGPX', '\"I\'ve been all over this world. I even remember some of those places.\"').

card_in_set('griselbrand', 'pGPX').
card_original_type('griselbrand'/'pGPX', 'Legendary Creature — Demon').
card_original_text('griselbrand'/'pGPX', '').
card_first_print('griselbrand', 'pGPX').
card_image_name('griselbrand'/'pGPX', 'griselbrand').
card_uid('griselbrand'/'pGPX', 'pGPX:Griselbrand:griselbrand').
card_rarity('griselbrand'/'pGPX', 'Special').
card_artist('griselbrand'/'pGPX', 'Jaime Jones').
card_number('griselbrand'/'pGPX', '11').
card_flavor_text('griselbrand'/'pGPX', '\"Avacyn emerged from the broken Helvault, but her freedom came at a price—him.\"\n—Thalia, Knight-Cathar').

card_in_set('lotus cobra', 'pGPX').
card_original_type('lotus cobra'/'pGPX', 'Creature — Snake').
card_original_text('lotus cobra'/'pGPX', '').
card_first_print('lotus cobra', 'pGPX').
card_image_name('lotus cobra'/'pGPX', 'lotus cobra').
card_uid('lotus cobra'/'pGPX', 'pGPX:Lotus Cobra:lotus cobra').
card_rarity('lotus cobra'/'pGPX', 'Special').
card_artist('lotus cobra'/'pGPX', 'Terese Nielsen').
card_number('lotus cobra'/'pGPX', '7').
card_flavor_text('lotus cobra'/'pGPX', 'Its scales contain the essence of thousands of lotus blooms.').

card_in_set('maelstrom pulse', 'pGPX').
card_original_type('maelstrom pulse'/'pGPX', 'Sorcery').
card_original_text('maelstrom pulse'/'pGPX', '').
card_first_print('maelstrom pulse', 'pGPX').
card_image_name('maelstrom pulse'/'pGPX', 'maelstrom pulse').
card_uid('maelstrom pulse'/'pGPX', 'pGPX:Maelstrom Pulse:maelstrom pulse').
card_rarity('maelstrom pulse'/'pGPX', 'Special').
card_artist('maelstrom pulse'/'pGPX', 'John Avon').
card_number('maelstrom pulse'/'pGPX', '5').
card_flavor_text('maelstrom pulse'/'pGPX', 'During the collision of the shards, entire ways of life disappeared without a trace.').

card_in_set('primeval titan', 'pGPX').
card_original_type('primeval titan'/'pGPX', 'Creature — Giant').
card_original_text('primeval titan'/'pGPX', '').
card_first_print('primeval titan', 'pGPX').
card_image_name('primeval titan'/'pGPX', 'primeval titan').
card_uid('primeval titan'/'pGPX', 'pGPX:Primeval Titan:primeval titan').
card_rarity('primeval titan'/'pGPX', 'Special').
card_artist('primeval titan'/'pGPX', 'Peter Mohrbacher').
card_number('primeval titan'/'pGPX', '8').
card_flavor_text('primeval titan'/'pGPX', 'When nature calls, run.').

card_in_set('spiritmonger', 'pGPX').
card_original_type('spiritmonger'/'pGPX', 'Creature — Beast').
card_original_text('spiritmonger'/'pGPX', '').
card_image_name('spiritmonger'/'pGPX', 'spiritmonger').
card_uid('spiritmonger'/'pGPX', 'pGPX:Spiritmonger:spiritmonger').
card_rarity('spiritmonger'/'pGPX', 'Special').
card_artist('spiritmonger'/'pGPX', 'Kev Walker').
card_number('spiritmonger'/'pGPX', '1').

card_in_set('umezawa\'s jitte', 'pGPX').
card_original_type('umezawa\'s jitte'/'pGPX', 'Legendary Artifact — Equipment').
card_original_text('umezawa\'s jitte'/'pGPX', '').
card_image_name('umezawa\'s jitte'/'pGPX', 'umezawa\'s jitte').
card_uid('umezawa\'s jitte'/'pGPX', 'pGPX:Umezawa\'s Jitte:umezawa\'s jitte').
card_rarity('umezawa\'s jitte'/'pGPX', 'Special').
card_artist('umezawa\'s jitte'/'pGPX', 'Svetlin Velinov').
card_number('umezawa\'s jitte'/'pGPX', '4').
