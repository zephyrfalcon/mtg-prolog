% Duel Decks: Jace vs. Vraska

set('DDM').
set_name('DDM', 'Duel Decks: Jace vs. Vraska').
set_release_date('DDM', '2014-03-14').
set_border('DDM', 'black').
set_type('DDM', 'duel deck').

card_in_set('acidic slime', 'DDM').
card_original_type('acidic slime'/'DDM', 'Creature — Ooze').
card_original_text('acidic slime'/'DDM', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)\nWhen Acidic Slime enters the battlefield, destroy target artifact, enchantment, or land.').
card_image_name('acidic slime'/'DDM', 'acidic slime').
card_uid('acidic slime'/'DDM', 'DDM:Acidic Slime:acidic slime').
card_rarity('acidic slime'/'DDM', 'Uncommon').
card_artist('acidic slime'/'DDM', 'Karl Kopinski').
card_number('acidic slime'/'DDM', '64').
card_multiverse_id('acidic slime'/'DDM', '380236').

card_in_set('aeon chronicler', 'DDM').
card_original_type('aeon chronicler'/'DDM', 'Creature — Avatar').
card_original_text('aeon chronicler'/'DDM', 'Aeon Chronicler\'s power and toughness are each equal to the number of cards in your hand.\nSuspend X—{X}{3}{U}. X can\'t be 0.\nWhenever a time counter is removed from Aeon Chronicler while it\'s exiled, draw a card.').
card_image_name('aeon chronicler'/'DDM', 'aeon chronicler').
card_uid('aeon chronicler'/'DDM', 'DDM:Aeon Chronicler:aeon chronicler').
card_rarity('aeon chronicler'/'DDM', 'Rare').
card_artist('aeon chronicler'/'DDM', 'Dan Dos Santos').
card_number('aeon chronicler'/'DDM', '17').
card_multiverse_id('aeon chronicler'/'DDM', '380272').

card_in_set('æther adept', 'DDM').
card_original_type('æther adept'/'DDM', 'Creature — Human Wizard').
card_original_text('æther adept'/'DDM', 'When Æther Adept enters the battlefield, return target creature to its owner\'s hand.').
card_image_name('æther adept'/'DDM', 'aether adept').
card_uid('æther adept'/'DDM', 'DDM:Æther Adept:aether adept').
card_rarity('æther adept'/'DDM', 'Common').
card_artist('æther adept'/'DDM', 'Eric Deschamps').
card_number('æther adept'/'DDM', '12').
card_flavor_text('æther adept'/'DDM', '\"The universe is my instrument, and the song I play upon it is one you are forbidden to hear.\"').
card_multiverse_id('æther adept'/'DDM', '380241').

card_in_set('æther figment', 'DDM').
card_original_type('æther figment'/'DDM', 'Creature — Illusion').
card_original_text('æther figment'/'DDM', 'Kicker {3} (You may pay an additional {3} as you cast this spell.)\nÆther Figment can\'t be blocked.\nIf Æther Figment was kicked, it enters the battlefield with two +1/+1 counters on it.').
card_image_name('æther figment'/'DDM', 'aether figment').
card_uid('æther figment'/'DDM', 'DDM:Æther Figment:aether figment').
card_rarity('æther figment'/'DDM', 'Uncommon').
card_artist('æther figment'/'DDM', 'Thomas M. Baxa').
card_number('æther figment'/'DDM', '5').
card_multiverse_id('æther figment'/'DDM', '380263').

card_in_set('agoraphobia', 'DDM').
card_original_type('agoraphobia'/'DDM', 'Enchantment — Aura').
card_original_text('agoraphobia'/'DDM', 'Enchant creature\nEnchanted creature gets -5/-0.\n{2}{U}: Return Agoraphobia to its owner\'s hand.').
card_image_name('agoraphobia'/'DDM', 'agoraphobia').
card_uid('agoraphobia'/'DDM', 'DDM:Agoraphobia:agoraphobia').
card_rarity('agoraphobia'/'DDM', 'Uncommon').
card_artist('agoraphobia'/'DDM', 'Jim Murray').
card_number('agoraphobia'/'DDM', '22').
card_flavor_text('agoraphobia'/'DDM', '\"Everyone in this city is choosing sides. I choose inside.\"').
card_multiverse_id('agoraphobia'/'DDM', '380253').

card_in_set('archaeomancer', 'DDM').
card_original_type('archaeomancer'/'DDM', 'Creature — Human Wizard').
card_original_text('archaeomancer'/'DDM', 'When Archaeomancer enters the battlefield, return target instant or sorcery card from your graveyard to your hand.').
card_image_name('archaeomancer'/'DDM', 'archaeomancer').
card_uid('archaeomancer'/'DDM', 'DDM:Archaeomancer:archaeomancer').
card_rarity('archaeomancer'/'DDM', 'Common').
card_artist('archaeomancer'/'DDM', 'Zoltan Boros').
card_number('archaeomancer'/'DDM', '13').
card_flavor_text('archaeomancer'/'DDM', '\"Words of power never disappear. They sleep, awaiting those with the will to rouse them.\"').
card_multiverse_id('archaeomancer'/'DDM', '380252').

card_in_set('body double', 'DDM').
card_original_type('body double'/'DDM', 'Creature — Shapeshifter').
card_original_text('body double'/'DDM', 'You may have Body Double enter the battlefield as a copy of any creature card in a graveyard.').
card_image_name('body double'/'DDM', 'body double').
card_uid('body double'/'DDM', 'DDM:Body Double:body double').
card_rarity('body double'/'DDM', 'Rare').
card_artist('body double'/'DDM', 'Winona Nelson').
card_number('body double'/'DDM', '15').
card_flavor_text('body double'/'DDM', '\"I am who I pretend to be.\"').
card_multiverse_id('body double'/'DDM', '380228').

card_in_set('chronomaton', 'DDM').
card_original_type('chronomaton'/'DDM', 'Artifact Creature — Golem').
card_original_text('chronomaton'/'DDM', '{1}, {T}: Put a +1/+1 counter on Chronomaton.').
card_image_name('chronomaton'/'DDM', 'chronomaton').
card_uid('chronomaton'/'DDM', 'DDM:Chronomaton:chronomaton').
card_rarity('chronomaton'/'DDM', 'Uncommon').
card_artist('chronomaton'/'DDM', 'Vincent Proce').
card_number('chronomaton'/'DDM', '2').
card_flavor_text('chronomaton'/'DDM', 'On the third night, the villagers destroyed their clocks. The sounds of whirring gears and chiming metal held only dread for them.').
card_multiverse_id('chronomaton'/'DDM', '380269').

card_in_set('claustrophobia', 'DDM').
card_original_type('claustrophobia'/'DDM', 'Enchantment — Aura').
card_original_text('claustrophobia'/'DDM', 'Enchant creature\nWhen Claustrophobia enters the battlefield, tap enchanted creature.\nEnchanted creature doesn\'t untap during its controller\'s untap step.').
card_image_name('claustrophobia'/'DDM', 'claustrophobia').
card_uid('claustrophobia'/'DDM', 'DDM:Claustrophobia:claustrophobia').
card_rarity('claustrophobia'/'DDM', 'Common').
card_artist('claustrophobia'/'DDM', 'Ryan Pancoast').
card_number('claustrophobia'/'DDM', '27').
card_flavor_text('claustrophobia'/'DDM', 'Six feet of earth muffled his cries.').
card_multiverse_id('claustrophobia'/'DDM', '380259').

card_in_set('consume strength', 'DDM').
card_original_type('consume strength'/'DDM', 'Instant').
card_original_text('consume strength'/'DDM', 'Target creature gets +2/+2 until end of turn. Another target creature gets -2/-2 until end of turn.').
card_image_name('consume strength'/'DDM', 'consume strength').
card_uid('consume strength'/'DDM', 'DDM:Consume Strength:consume strength').
card_rarity('consume strength'/'DDM', 'Common').
card_artist('consume strength'/'DDM', 'Adam Rex').
card_number('consume strength'/'DDM', '74').
card_flavor_text('consume strength'/'DDM', '\"You are correct. This is going to hurt.\"\n—Vraska').
card_multiverse_id('consume strength'/'DDM', '380246').

card_in_set('control magic', 'DDM').
card_original_type('control magic'/'DDM', 'Enchantment — Aura').
card_original_text('control magic'/'DDM', 'Enchant creature\nYou control enchanted creature.').
card_image_name('control magic'/'DDM', 'control magic').
card_uid('control magic'/'DDM', 'DDM:Control Magic:control magic').
card_rarity('control magic'/'DDM', 'Uncommon').
card_artist('control magic'/'DDM', 'Clint Cearley').
card_number('control magic'/'DDM', '30').
card_flavor_text('control magic'/'DDM', '\"Do as I think, not as I do.\"\n—Jace Beleren').
card_multiverse_id('control magic'/'DDM', '380201').

card_in_set('corpse traders', 'DDM').
card_original_type('corpse traders'/'DDM', 'Creature — Human Rogue').
card_original_text('corpse traders'/'DDM', '{2}{B}, Sacrifice a creature: Target opponent reveals his or her hand. You choose a card from it. That player discards that card. Activate this ability only any time you could cast a sorcery.').
card_image_name('corpse traders'/'DDM', 'corpse traders').
card_uid('corpse traders'/'DDM', 'DDM:Corpse Traders:corpse traders').
card_rarity('corpse traders'/'DDM', 'Uncommon').
card_artist('corpse traders'/'DDM', 'Kev Walker').
card_number('corpse traders'/'DDM', '58').
card_flavor_text('corpse traders'/'DDM', 'Those without breath can\'t complain.').
card_multiverse_id('corpse traders'/'DDM', '380254').

card_in_set('crosstown courier', 'DDM').
card_original_type('crosstown courier'/'DDM', 'Creature — Vedalken').
card_original_text('crosstown courier'/'DDM', 'Whenever Crosstown Courier deals combat damage to a player, that player puts that many cards from the top of his or her library into his or her graveyard.').
card_image_name('crosstown courier'/'DDM', 'crosstown courier').
card_uid('crosstown courier'/'DDM', 'DDM:Crosstown Courier:crosstown courier').
card_rarity('crosstown courier'/'DDM', 'Common').
card_artist('crosstown courier'/'DDM', 'Chase Stone').
card_number('crosstown courier'/'DDM', '6').
card_flavor_text('crosstown courier'/'DDM', 'Information travels quickly through Ravnica\'s network of messengers and thought agents.').
card_multiverse_id('crosstown courier'/'DDM', '380256').

card_in_set('death-hood cobra', 'DDM').
card_original_type('death-hood cobra'/'DDM', 'Creature — Snake').
card_original_text('death-hood cobra'/'DDM', '{1}{G}: Death-Hood Cobra gains reach until end of turn. (It can block creatures with flying.)\n{1}{G}: Death-Hood Cobra gains deathtouch until end of turn. (Any amount of damage it deals to a creature is enough to destroy it.)').
card_image_name('death-hood cobra'/'DDM', 'death-hood cobra').
card_uid('death-hood cobra'/'DDM', 'DDM:Death-Hood Cobra:death-hood cobra').
card_rarity('death-hood cobra'/'DDM', 'Common').
card_artist('death-hood cobra'/'DDM', 'Jason Felix').
card_number('death-hood cobra'/'DDM', '47').
card_multiverse_id('death-hood cobra'/'DDM', '380262').

card_in_set('dread statuary', 'DDM').
card_original_type('dread statuary'/'DDM', 'Land').
card_original_text('dread statuary'/'DDM', '{T}: Add {1} to your mana pool.\n{4}: Dread Statuary becomes a 4/2 Golem artifact creature until end of turn. It\'s still a land.').
card_image_name('dread statuary'/'DDM', 'dread statuary').
card_uid('dread statuary'/'DDM', 'DDM:Dread Statuary:dread statuary').
card_rarity('dread statuary'/'DDM', 'Uncommon').
card_artist('dread statuary'/'DDM', 'Jason A. Engle').
card_number('dread statuary'/'DDM', '35').
card_flavor_text('dread statuary'/'DDM', 'The last reliable landmark in Tazeem just walked away.').
card_multiverse_id('dread statuary'/'DDM', '380232').

card_in_set('dream stalker', 'DDM').
card_original_type('dream stalker'/'DDM', 'Creature — Illusion').
card_original_text('dream stalker'/'DDM', 'When Dream Stalker enters the battlefield, return a permanent you control to its owner\'s hand.').
card_image_name('dream stalker'/'DDM', 'dream stalker').
card_uid('dream stalker'/'DDM', 'DDM:Dream Stalker:dream stalker').
card_rarity('dream stalker'/'DDM', 'Common').
card_artist('dream stalker'/'DDM', 'Brian Despain').
card_number('dream stalker'/'DDM', '7').
card_flavor_text('dream stalker'/'DDM', 'What happens when it is the dream that wakes and the sleeper that fades into memory?').
card_multiverse_id('dream stalker'/'DDM', '380210').

card_in_set('drooling groodion', 'DDM').
card_original_type('drooling groodion'/'DDM', 'Creature — Beast').
card_original_text('drooling groodion'/'DDM', '{2}{B}{G}, Sacrifice a creature: Target creature gets +2/+2 until end of turn. Another target creature gets -2/-2 until end of turn.').
card_image_name('drooling groodion'/'DDM', 'drooling groodion').
card_uid('drooling groodion'/'DDM', 'DDM:Drooling Groodion:drooling groodion').
card_rarity('drooling groodion'/'DDM', 'Uncommon').
card_artist('drooling groodion'/'DDM', 'Kev Walker').
card_number('drooling groodion'/'DDM', '65').
card_flavor_text('drooling groodion'/'DDM', '\"The Golgari expand, yes, but I refuse to call their tainted creations ‘growth.\'\"\n—Niszka, Selesnya evangel').
card_multiverse_id('drooling groodion'/'DDM', '380205').

card_in_set('errant ephemeron', 'DDM').
card_original_type('errant ephemeron'/'DDM', 'Creature — Illusion').
card_original_text('errant ephemeron'/'DDM', 'Flying\nSuspend 4—{1}{U} (Rather than cast this card from your hand, you may pay {1}{U} and exile it with four time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)').
card_image_name('errant ephemeron'/'DDM', 'errant ephemeron').
card_uid('errant ephemeron'/'DDM', 'DDM:Errant Ephemeron:errant ephemeron').
card_rarity('errant ephemeron'/'DDM', 'Common').
card_artist('errant ephemeron'/'DDM', 'Luca Zontini').
card_number('errant ephemeron'/'DDM', '20').
card_multiverse_id('errant ephemeron'/'DDM', '380243').

card_in_set('festerhide boar', 'DDM').
card_original_type('festerhide boar'/'DDM', 'Creature — Boar').
card_original_text('festerhide boar'/'DDM', 'Trample\nMorbid — Festerhide Boar enters the battlefield with two +1/+1 counters on it if a creature died this turn.').
card_image_name('festerhide boar'/'DDM', 'festerhide boar').
card_uid('festerhide boar'/'DDM', 'DDM:Festerhide Boar:festerhide boar').
card_rarity('festerhide boar'/'DDM', 'Common').
card_artist('festerhide boar'/'DDM', 'Nils Hamm').
card_number('festerhide boar'/'DDM', '59').
card_flavor_text('festerhide boar'/'DDM', '\"Bury your dead deep. The boars are hungriest while the corpse is still warm.\"\n—Paulin, trapper of Somberwald').
card_multiverse_id('festerhide boar'/'DDM', '380200').

card_in_set('forest', 'DDM').
card_original_type('forest'/'DDM', 'Basic Land — Forest').
card_original_text('forest'/'DDM', 'G').
card_image_name('forest'/'DDM', 'forest1').
card_uid('forest'/'DDM', 'DDM:Forest:forest1').
card_rarity('forest'/'DDM', 'Basic Land').
card_artist('forest'/'DDM', 'John Avon').
card_number('forest'/'DDM', '84').
card_multiverse_id('forest'/'DDM', '380208').

card_in_set('forest', 'DDM').
card_original_type('forest'/'DDM', 'Basic Land — Forest').
card_original_text('forest'/'DDM', 'G').
card_image_name('forest'/'DDM', 'forest2').
card_uid('forest'/'DDM', 'DDM:Forest:forest2').
card_rarity('forest'/'DDM', 'Basic Land').
card_artist('forest'/'DDM', 'Yeong-Hao Han').
card_number('forest'/'DDM', '85').
card_multiverse_id('forest'/'DDM', '380209').

card_in_set('forest', 'DDM').
card_original_type('forest'/'DDM', 'Basic Land — Forest').
card_original_text('forest'/'DDM', 'G').
card_image_name('forest'/'DDM', 'forest3').
card_uid('forest'/'DDM', 'DDM:Forest:forest3').
card_rarity('forest'/'DDM', 'Basic Land').
card_artist('forest'/'DDM', 'Adam Paquette').
card_number('forest'/'DDM', '86').
card_multiverse_id('forest'/'DDM', '380198').

card_in_set('forest', 'DDM').
card_original_type('forest'/'DDM', 'Basic Land — Forest').
card_original_text('forest'/'DDM', 'G').
card_image_name('forest'/'DDM', 'forest4').
card_uid('forest'/'DDM', 'DDM:Forest:forest4').
card_rarity('forest'/'DDM', 'Basic Land').
card_artist('forest'/'DDM', 'Richard Wright').
card_number('forest'/'DDM', '87').
card_multiverse_id('forest'/'DDM', '380229').

card_in_set('forest', 'DDM').
card_original_type('forest'/'DDM', 'Basic Land — Forest').
card_original_text('forest'/'DDM', 'G').
card_image_name('forest'/'DDM', 'forest5').
card_uid('forest'/'DDM', 'DDM:Forest:forest5').
card_rarity('forest'/'DDM', 'Basic Land').
card_artist('forest'/'DDM', 'Richard Wright').
card_number('forest'/'DDM', '88').
card_multiverse_id('forest'/'DDM', '380192').

card_in_set('future sight', 'DDM').
card_original_type('future sight'/'DDM', 'Enchantment').
card_original_text('future sight'/'DDM', 'Play with the top card of your library revealed.\nYou may play the top card of your library.').
card_image_name('future sight'/'DDM', 'future sight').
card_uid('future sight'/'DDM', 'DDM:Future Sight:future sight').
card_rarity('future sight'/'DDM', 'Rare').
card_artist('future sight'/'DDM', 'Dan Scott').
card_number('future sight'/'DDM', '33').
card_flavor_text('future sight'/'DDM', '\"Is the inevitable any less miraculous?\"\n—Veldka, wandering sage').
card_multiverse_id('future sight'/'DDM', '380251').

card_in_set('gatecreeper vine', 'DDM').
card_original_type('gatecreeper vine'/'DDM', 'Creature — Plant').
card_original_text('gatecreeper vine'/'DDM', 'Defender\nWhen Gatecreeper Vine enters the battlefield, you may search your library for a basic land card or a Gate card, reveal it, put it into your hand, then shuffle your library.').
card_image_name('gatecreeper vine'/'DDM', 'gatecreeper vine').
card_uid('gatecreeper vine'/'DDM', 'DDM:Gatecreeper Vine:gatecreeper vine').
card_rarity('gatecreeper vine'/'DDM', 'Common').
card_artist('gatecreeper vine'/'DDM', 'Trevor Claxton').
card_number('gatecreeper vine'/'DDM', '48').
card_flavor_text('gatecreeper vine'/'DDM', 'Every inch of Ravnica is home to something.').
card_multiverse_id('gatecreeper vine'/'DDM', '380268').

card_in_set('golgari guildgate', 'DDM').
card_original_type('golgari guildgate'/'DDM', 'Land — Gate').
card_original_text('golgari guildgate'/'DDM', 'Golgari Guildgate enters the battlefield tapped.\n{T}: Add {B} or {G} to your mana pool.').
card_image_name('golgari guildgate'/'DDM', 'golgari guildgate').
card_uid('golgari guildgate'/'DDM', 'DDM:Golgari Guildgate:golgari guildgate').
card_rarity('golgari guildgate'/'DDM', 'Common').
card_artist('golgari guildgate'/'DDM', 'Eytan Zana').
card_number('golgari guildgate'/'DDM', '76').
card_flavor_text('golgari guildgate'/'DDM', 'Enter those who are starving and sick. You are welcome among the Swarm when the rest of Ravnica rejects you.').
card_multiverse_id('golgari guildgate'/'DDM', '380220').

card_in_set('griptide', 'DDM').
card_original_type('griptide'/'DDM', 'Instant').
card_original_text('griptide'/'DDM', 'Put target creature on top of its owner\'s library.').
card_image_name('griptide'/'DDM', 'griptide').
card_uid('griptide'/'DDM', 'DDM:Griptide:griptide').
card_rarity('griptide'/'DDM', 'Common').
card_artist('griptide'/'DDM', 'Igor Kieryluk').
card_number('griptide'/'DDM', '28').
card_flavor_text('griptide'/'DDM', '\"Beware the seagrafs just off the shore. These waters are filled with hungry geists looking for an easy meal.\"\n—Captain Eberhart').
card_multiverse_id('griptide'/'DDM', '380267').

card_in_set('grisly spectacle', 'DDM').
card_original_type('grisly spectacle'/'DDM', 'Instant').
card_original_text('grisly spectacle'/'DDM', 'Destroy target nonartifact creature. Its controller puts a number of cards equal to that creature\'s power from the top of his or her library into his or her graveyard.').
card_image_name('grisly spectacle'/'DDM', 'grisly spectacle').
card_uid('grisly spectacle'/'DDM', 'DDM:Grisly Spectacle:grisly spectacle').
card_rarity('grisly spectacle'/'DDM', 'Common').
card_artist('grisly spectacle'/'DDM', 'Zoltan Boros').
card_number('grisly spectacle'/'DDM', '75').
card_flavor_text('grisly spectacle'/'DDM', '\"Watch people flock to a murder scene. Then tell me we\'re not all a little sick in the head.\"\n—Juri, proprietor of the Juri Revue').
card_multiverse_id('grisly spectacle'/'DDM', '380274').

card_in_set('halimar depths', 'DDM').
card_original_type('halimar depths'/'DDM', 'Land').
card_original_text('halimar depths'/'DDM', 'Halimar Depths enters the battlefield tapped.\nWhen Halimar Depths enters the battlefield, look at the top three cards of your library, then put them back in any order.\n{T}: Add {U} to your mana pool.').
card_image_name('halimar depths'/'DDM', 'halimar depths').
card_uid('halimar depths'/'DDM', 'DDM:Halimar Depths:halimar depths').
card_rarity('halimar depths'/'DDM', 'Common').
card_artist('halimar depths'/'DDM', 'Volkan Baga').
card_number('halimar depths'/'DDM', '36').
card_multiverse_id('halimar depths'/'DDM', '380231').

card_in_set('highway robber', 'DDM').
card_original_type('highway robber'/'DDM', 'Creature — Human Mercenary').
card_original_text('highway robber'/'DDM', 'When Highway Robber enters the battlefield, target opponent loses 2 life and you gain 2 life.').
card_image_name('highway robber'/'DDM', 'highway robber').
card_uid('highway robber'/'DDM', 'DDM:Highway Robber:highway robber').
card_rarity('highway robber'/'DDM', 'Common').
card_artist('highway robber'/'DDM', 'Kev Walker').
card_number('highway robber'/'DDM', '61').
card_flavor_text('highway robber'/'DDM', '\"Tonight, madam, it\'s your money and your life.\"').
card_multiverse_id('highway robber'/'DDM', '380193').

card_in_set('hypnotic cloud', 'DDM').
card_original_type('hypnotic cloud'/'DDM', 'Sorcery').
card_original_text('hypnotic cloud'/'DDM', 'Kicker {4} (You may pay an additional {4} as you cast this spell.)\nTarget player discards a card. If Hypnotic Cloud was kicked, that player discards three cards instead.').
card_image_name('hypnotic cloud'/'DDM', 'hypnotic cloud').
card_uid('hypnotic cloud'/'DDM', 'DDM:Hypnotic Cloud:hypnotic cloud').
card_rarity('hypnotic cloud'/'DDM', 'Common').
card_artist('hypnotic cloud'/'DDM', 'Randy Gallegos').
card_number('hypnotic cloud'/'DDM', '67').
card_multiverse_id('hypnotic cloud'/'DDM', '380225').

card_in_set('into the roil', 'DDM').
card_original_type('into the roil'/'DDM', 'Instant').
card_original_text('into the roil'/'DDM', 'Kicker {1}{U} (You may pay an additional {1}{U} as you cast this spell.)\nReturn target nonland permanent to its owner\'s hand. If Into the Roil was kicked, draw a card.').
card_image_name('into the roil'/'DDM', 'into the roil').
card_uid('into the roil'/'DDM', 'DDM:Into the Roil:into the roil').
card_rarity('into the roil'/'DDM', 'Common').
card_artist('into the roil'/'DDM', 'Kieran Yanner').
card_number('into the roil'/'DDM', '23').
card_flavor_text('into the roil'/'DDM', '\"Roil tide! Roil tide! Tie yourselves down!\"').
card_multiverse_id('into the roil'/'DDM', '380188').

card_in_set('island', 'DDM').
card_original_type('island'/'DDM', 'Basic Land — Island').
card_original_text('island'/'DDM', 'U').
card_image_name('island'/'DDM', 'island1').
card_uid('island'/'DDM', 'DDM:Island:island1').
card_rarity('island'/'DDM', 'Basic Land').
card_artist('island'/'DDM', 'John Avon').
card_number('island'/'DDM', '37').
card_multiverse_id('island'/'DDM', '380261').

card_in_set('island', 'DDM').
card_original_type('island'/'DDM', 'Basic Land — Island').
card_original_text('island'/'DDM', 'U').
card_image_name('island'/'DDM', 'island2').
card_uid('island'/'DDM', 'DDM:Island:island2').
card_rarity('island'/'DDM', 'Basic Land').
card_artist('island'/'DDM', 'Yeong-Hao Han').
card_number('island'/'DDM', '38').
card_multiverse_id('island'/'DDM', '380217').

card_in_set('island', 'DDM').
card_original_type('island'/'DDM', 'Basic Land — Island').
card_original_text('island'/'DDM', 'U').
card_image_name('island'/'DDM', 'island3').
card_uid('island'/'DDM', 'DDM:Island:island3').
card_rarity('island'/'DDM', 'Basic Land').
card_artist('island'/'DDM', 'Adam Paquette').
card_number('island'/'DDM', '39').
card_multiverse_id('island'/'DDM', '380223').

card_in_set('island', 'DDM').
card_original_type('island'/'DDM', 'Basic Land — Island').
card_original_text('island'/'DDM', 'U').
card_image_name('island'/'DDM', 'island4').
card_uid('island'/'DDM', 'DDM:Island:island4').
card_rarity('island'/'DDM', 'Basic Land').
card_artist('island'/'DDM', 'Richard Wright').
card_number('island'/'DDM', '40').
card_multiverse_id('island'/'DDM', '380222').

card_in_set('island', 'DDM').
card_original_type('island'/'DDM', 'Basic Land — Island').
card_original_text('island'/'DDM', 'U').
card_image_name('island'/'DDM', 'island5').
card_uid('island'/'DDM', 'DDM:Island:island5').
card_rarity('island'/'DDM', 'Basic Land').
card_artist('island'/'DDM', 'Richard Wright').
card_number('island'/'DDM', '41').
card_multiverse_id('island'/'DDM', '380194').

card_in_set('jace\'s ingenuity', 'DDM').
card_original_type('jace\'s ingenuity'/'DDM', 'Instant').
card_original_text('jace\'s ingenuity'/'DDM', 'Draw three cards.').
card_image_name('jace\'s ingenuity'/'DDM', 'jace\'s ingenuity').
card_uid('jace\'s ingenuity'/'DDM', 'DDM:Jace\'s Ingenuity:jace\'s ingenuity').
card_rarity('jace\'s ingenuity'/'DDM', 'Uncommon').
card_artist('jace\'s ingenuity'/'DDM', 'Igor Kieryluk').
card_number('jace\'s ingenuity'/'DDM', '32').
card_flavor_text('jace\'s ingenuity'/'DDM', '\"Brute force can sometimes kick down a locked door, but knowledge is a skeleton key.\"').
card_multiverse_id('jace\'s ingenuity'/'DDM', '380211').

card_in_set('jace\'s mindseeker', 'DDM').
card_original_type('jace\'s mindseeker'/'DDM', 'Creature — Fish Illusion').
card_original_text('jace\'s mindseeker'/'DDM', 'Flying\nWhen Jace\'s Mindseeker enters the battlefield, target opponent puts the top five cards of his or her library into his or her graveyard. You may cast an instant or sorcery card from among them without paying its mana cost.').
card_image_name('jace\'s mindseeker'/'DDM', 'jace\'s mindseeker').
card_uid('jace\'s mindseeker'/'DDM', 'DDM:Jace\'s Mindseeker:jace\'s mindseeker').
card_rarity('jace\'s mindseeker'/'DDM', 'Rare').
card_artist('jace\'s mindseeker'/'DDM', 'Greg Staples').
card_number('jace\'s mindseeker'/'DDM', '19').
card_multiverse_id('jace\'s mindseeker'/'DDM', '380206').

card_in_set('jace\'s phantasm', 'DDM').
card_original_type('jace\'s phantasm'/'DDM', 'Creature — Illusion').
card_original_text('jace\'s phantasm'/'DDM', 'Flying\nJace\'s Phantasm gets +4/+4 as long as an opponent has ten or more cards in his or her graveyard.').
card_image_name('jace\'s phantasm'/'DDM', 'jace\'s phantasm').
card_uid('jace\'s phantasm'/'DDM', 'DDM:Jace\'s Phantasm:jace\'s phantasm').
card_rarity('jace\'s phantasm'/'DDM', 'Uncommon').
card_artist('jace\'s phantasm'/'DDM', 'Johann Bodin').
card_number('jace\'s phantasm'/'DDM', '3').
card_flavor_text('jace\'s phantasm'/'DDM', 'In the abstract memories of the Iquati, Jace found interesting ideas to improve upon.').
card_multiverse_id('jace\'s phantasm'/'DDM', '380245').

card_in_set('jace, architect of thought', 'DDM').
card_original_type('jace, architect of thought'/'DDM', 'Planeswalker — Jace').
card_original_text('jace, architect of thought'/'DDM', '+1: Until your next turn, whenever a creature an opponent controls attacks, it gets -1/-0 until end of turn.\n-2: Reveal the top three cards of your library. An opponent separates those cards into two piles. Put one pile into your hand and the other on the bottom of your library in any order.\n-8: For each player, search that player\'s library for a nonland card and exile it, then that player shuffles his or her library. You may cast those cards without paying their mana costs.').
card_image_name('jace, architect of thought'/'DDM', 'jace, architect of thought').
card_uid('jace, architect of thought'/'DDM', 'DDM:Jace, Architect of Thought:jace, architect of thought').
card_rarity('jace, architect of thought'/'DDM', 'Mythic Rare').
card_artist('jace, architect of thought'/'DDM', 'Igor Kieryluk').
card_number('jace, architect of thought'/'DDM', '1').
card_multiverse_id('jace, architect of thought'/'DDM', '380190').

card_in_set('krovikan mist', 'DDM').
card_original_type('krovikan mist'/'DDM', 'Creature — Illusion').
card_original_text('krovikan mist'/'DDM', 'Flying\nKrovikan Mist\'s power and toughness are each equal to the number of Illusions on the battlefield.').
card_image_name('krovikan mist'/'DDM', 'krovikan mist').
card_uid('krovikan mist'/'DDM', 'DDM:Krovikan Mist:krovikan mist').
card_rarity('krovikan mist'/'DDM', 'Common').
card_artist('krovikan mist'/'DDM', 'Jeremy Jarvis').
card_number('krovikan mist'/'DDM', '8').
card_flavor_text('krovikan mist'/'DDM', '\"It\'s as strong as you believe it is, and it\'s very convincing.\"\n—Jace Beleren').
card_multiverse_id('krovikan mist'/'DDM', '380275').

card_in_set('last kiss', 'DDM').
card_original_type('last kiss'/'DDM', 'Instant').
card_original_text('last kiss'/'DDM', 'Last Kiss deals 2 damage to target creature and you gain 2 life.').
card_image_name('last kiss'/'DDM', 'last kiss').
card_uid('last kiss'/'DDM', 'DDM:Last Kiss:last kiss').
card_rarity('last kiss'/'DDM', 'Common').
card_artist('last kiss'/'DDM', 'Vance Kovacs').
card_number('last kiss'/'DDM', '71').
card_flavor_text('last kiss'/'DDM', '\"Romanticize it, glamorize it, call it what you will. To me, it will always be carnal, bloody murder.\"\n—Ayli, Kamsa cleric').
card_multiverse_id('last kiss'/'DDM', '380218').

card_in_set('leyline phantom', 'DDM').
card_original_type('leyline phantom'/'DDM', 'Creature — Illusion').
card_original_text('leyline phantom'/'DDM', 'When Leyline Phantom deals combat damage, return it to its owner\'s hand. (Return it only if it survived combat.)').
card_image_name('leyline phantom'/'DDM', 'leyline phantom').
card_uid('leyline phantom'/'DDM', 'DDM:Leyline Phantom:leyline phantom').
card_rarity('leyline phantom'/'DDM', 'Common').
card_artist('leyline phantom'/'DDM', 'Ryan Yee').
card_number('leyline phantom'/'DDM', '16').
card_flavor_text('leyline phantom'/'DDM', '\"Is the maze itself a phantom? Or is one as real as the other? Perhaps I am as mad as the dragon.\"\n—Jace Beleren, journal').
card_multiverse_id('leyline phantom'/'DDM', '380257').

card_in_set('marsh casualties', 'DDM').
card_original_type('marsh casualties'/'DDM', 'Sorcery').
card_original_text('marsh casualties'/'DDM', 'Kicker {3} (You may pay an additional {3} as you cast this spell.)\nCreatures target player controls get -1/-1 until end of turn. If Marsh Casualties was kicked, those creatures get -2/-2 until end of turn instead.').
card_image_name('marsh casualties'/'DDM', 'marsh casualties').
card_uid('marsh casualties'/'DDM', 'DDM:Marsh Casualties:marsh casualties').
card_rarity('marsh casualties'/'DDM', 'Uncommon').
card_artist('marsh casualties'/'DDM', 'Scott Chou').
card_number('marsh casualties'/'DDM', '69').
card_multiverse_id('marsh casualties'/'DDM', '380238').

card_in_set('memory lapse', 'DDM').
card_original_type('memory lapse'/'DDM', 'Instant').
card_original_text('memory lapse'/'DDM', 'Counter target spell. If that spell is countered this way, put it on top of its owner\'s library instead of into that player\'s graveyard.').
card_image_name('memory lapse'/'DDM', 'memory lapse').
card_uid('memory lapse'/'DDM', 'DDM:Memory Lapse:memory lapse').
card_rarity('memory lapse'/'DDM', 'Common').
card_artist('memory lapse'/'DDM', 'Rebecca Guay').
card_number('memory lapse'/'DDM', '24').
card_flavor_text('memory lapse'/'DDM', '\"To sculpt a mind is to see every memory and thought as an individual piece that can be rearranged—or removed.\"\n—Jace Beleren').
card_multiverse_id('memory lapse'/'DDM', '380199').

card_in_set('merfolk wayfinder', 'DDM').
card_original_type('merfolk wayfinder'/'DDM', 'Creature — Merfolk Scout').
card_original_text('merfolk wayfinder'/'DDM', 'Flying\nWhen Merfolk Wayfinder enters the battlefield, reveal the top three cards of your library. Put all Island cards revealed this way into your hand and the rest on the bottom of your library in any order.').
card_image_name('merfolk wayfinder'/'DDM', 'merfolk wayfinder').
card_uid('merfolk wayfinder'/'DDM', 'DDM:Merfolk Wayfinder:merfolk wayfinder').
card_rarity('merfolk wayfinder'/'DDM', 'Uncommon').
card_artist('merfolk wayfinder'/'DDM', 'Christopher Moeller').
card_number('merfolk wayfinder'/'DDM', '9').
card_multiverse_id('merfolk wayfinder'/'DDM', '380233').

card_in_set('mold shambler', 'DDM').
card_original_type('mold shambler'/'DDM', 'Creature — Fungus Beast').
card_original_text('mold shambler'/'DDM', 'Kicker {1}{G} (You may pay an additional {1}{G} as you cast this spell.)\nWhen Mold Shambler enters the battlefield, if it was kicked, destroy target noncreature permanent.').
card_image_name('mold shambler'/'DDM', 'mold shambler').
card_uid('mold shambler'/'DDM', 'DDM:Mold Shambler:mold shambler').
card_rarity('mold shambler'/'DDM', 'Common').
card_artist('mold shambler'/'DDM', 'Karl Kopinski').
card_number('mold shambler'/'DDM', '60').
card_flavor_text('mold shambler'/'DDM', 'When civilization encroaches on nature, Zendikar encroaches back.').
card_multiverse_id('mold shambler'/'DDM', '380249').

card_in_set('nekrataal', 'DDM').
card_original_type('nekrataal'/'DDM', 'Creature — Human Assassin').
card_original_text('nekrataal'/'DDM', 'First strike\nWhen Nekrataal enters the battlefield, destroy target nonartifact, nonblack creature. That creature can\'t be regenerated.').
card_image_name('nekrataal'/'DDM', 'nekrataal').
card_uid('nekrataal'/'DDM', 'DDM:Nekrataal:nekrataal').
card_rarity('nekrataal'/'DDM', 'Uncommon').
card_artist('nekrataal'/'DDM', 'Christopher Moeller').
card_number('nekrataal'/'DDM', '62').
card_flavor_text('nekrataal'/'DDM', '\"You can\'t protect them all, Jace.\"\n—Vraska').
card_multiverse_id('nekrataal'/'DDM', '380273').

card_in_set('night\'s whisper', 'DDM').
card_original_type('night\'s whisper'/'DDM', 'Sorcery').
card_original_text('night\'s whisper'/'DDM', 'You draw two cards and you lose 2 life.').
card_image_name('night\'s whisper'/'DDM', 'night\'s whisper').
card_uid('night\'s whisper'/'DDM', 'DDM:Night\'s Whisper:night\'s whisper').
card_rarity('night\'s whisper'/'DDM', 'Uncommon').
card_artist('night\'s whisper'/'DDM', 'John Severin Brassell').
card_number('night\'s whisper'/'DDM', '68').
card_flavor_text('night\'s whisper'/'DDM', '\"I have seen things that would reduce a weaker person to blubbering and raving. Want to hear about them?\"\n—Vraska').
card_multiverse_id('night\'s whisper'/'DDM', '380191').

card_in_set('ohran viper', 'DDM').
card_original_type('ohran viper'/'DDM', 'Snow Creature — Snake').
card_original_text('ohran viper'/'DDM', 'Whenever Ohran Viper deals combat damage to a creature, destroy that creature at end of combat.\nWhenever Ohran Viper deals combat damage to a player, you may draw a card.').
card_image_name('ohran viper'/'DDM', 'ohran viper').
card_uid('ohran viper'/'DDM', 'DDM:Ohran Viper:ohran viper').
card_rarity('ohran viper'/'DDM', 'Rare').
card_artist('ohran viper'/'DDM', 'Kev Walker').
card_number('ohran viper'/'DDM', '57').
card_flavor_text('ohran viper'/'DDM', 'The ohran viper is not cold-blooded. Its veins course with the same antigelid venom used to kill its prey.').
card_multiverse_id('ohran viper'/'DDM', '380265').

card_in_set('oran-rief recluse', 'DDM').
card_original_type('oran-rief recluse'/'DDM', 'Creature — Spider').
card_original_text('oran-rief recluse'/'DDM', 'Kicker {2}{G} (You may pay an additional {2}{G} as you cast this spell.)\nReach (This creature can block creatures with flying.)\nWhen Oran-Rief Recluse enters the battlefield, if it was kicked, destroy target creature with flying.').
card_image_name('oran-rief recluse'/'DDM', 'oran-rief recluse').
card_uid('oran-rief recluse'/'DDM', 'DDM:Oran-Rief Recluse:oran-rief recluse').
card_rarity('oran-rief recluse'/'DDM', 'Common').
card_artist('oran-rief recluse'/'DDM', 'Lars Grant-West').
card_number('oran-rief recluse'/'DDM', '54').
card_multiverse_id('oran-rief recluse'/'DDM', '380248').

card_in_set('phantasmal bear', 'DDM').
card_original_type('phantasmal bear'/'DDM', 'Creature — Bear Illusion').
card_original_text('phantasmal bear'/'DDM', 'When Phantasmal Bear becomes the target of a spell or ability, sacrifice it.').
card_image_name('phantasmal bear'/'DDM', 'phantasmal bear').
card_uid('phantasmal bear'/'DDM', 'DDM:Phantasmal Bear:phantasmal bear').
card_rarity('phantasmal bear'/'DDM', 'Common').
card_artist('phantasmal bear'/'DDM', 'Ryan Yee').
card_number('phantasmal bear'/'DDM', '4').
card_flavor_text('phantasmal bear'/'DDM', '\"You know it to be false, but when its claws find you, what you know won\'t matter much at all.\"\n—Pol Jamaar, illusionist').
card_multiverse_id('phantasmal bear'/'DDM', '380219').

card_in_set('phantasmal dragon', 'DDM').
card_original_type('phantasmal dragon'/'DDM', 'Creature — Dragon Illusion').
card_original_text('phantasmal dragon'/'DDM', 'Flying\nWhen Phantasmal Dragon becomes the target of a spell or ability, sacrifice it.').
card_image_name('phantasmal dragon'/'DDM', 'phantasmal dragon').
card_uid('phantasmal dragon'/'DDM', 'DDM:Phantasmal Dragon:phantasmal dragon').
card_rarity('phantasmal dragon'/'DDM', 'Uncommon').
card_artist('phantasmal dragon'/'DDM', 'Wayne Reynolds').
card_number('phantasmal dragon'/'DDM', '14').
card_flavor_text('phantasmal dragon'/'DDM', 'Its hunger and ire are no less for being wrought of lies and mist.').
card_multiverse_id('phantasmal dragon'/'DDM', '380215').

card_in_set('prohibit', 'DDM').
card_original_type('prohibit'/'DDM', 'Instant').
card_original_text('prohibit'/'DDM', 'Kicker {2} (You may pay an additional {2} as you cast this spell.)\nCounter target spell if its converted mana cost is 2 or less. If Prohibit was kicked, counter that spell if its converted mana cost is 4 or less instead.').
card_image_name('prohibit'/'DDM', 'prohibit').
card_uid('prohibit'/'DDM', 'DDM:Prohibit:prohibit').
card_rarity('prohibit'/'DDM', 'Common').
card_artist('prohibit'/'DDM', 'Adam Rex').
card_number('prohibit'/'DDM', '25').
card_multiverse_id('prohibit'/'DDM', '380270').

card_in_set('pulse tracker', 'DDM').
card_original_type('pulse tracker'/'DDM', 'Creature — Vampire Rogue').
card_original_text('pulse tracker'/'DDM', 'Whenever Pulse Tracker attacks, each opponent loses 1 life.').
card_image_name('pulse tracker'/'DDM', 'pulse tracker').
card_uid('pulse tracker'/'DDM', 'DDM:Pulse Tracker:pulse tracker').
card_rarity('pulse tracker'/'DDM', 'Common').
card_artist('pulse tracker'/'DDM', 'Andrew Robinson').
card_number('pulse tracker'/'DDM', '43').
card_flavor_text('pulse tracker'/'DDM', '\"You can\'t think your way out of this, Jace. Thought implies life, and life requires a pulse—music to a hunter\'s ears.\"\n—Vraska').
card_multiverse_id('pulse tracker'/'DDM', '380250').

card_in_set('putrid leech', 'DDM').
card_original_type('putrid leech'/'DDM', 'Creature — Zombie Leech').
card_original_text('putrid leech'/'DDM', 'Pay 2 life: Putrid Leech gets +2/+2 until end of turn. Activate this ability only once each turn.').
card_image_name('putrid leech'/'DDM', 'putrid leech').
card_uid('putrid leech'/'DDM', 'DDM:Putrid Leech:putrid leech').
card_rarity('putrid leech'/'DDM', 'Common').
card_artist('putrid leech'/'DDM', 'Craig J Spearing').
card_number('putrid leech'/'DDM', '51').
card_flavor_text('putrid leech'/'DDM', '\"The Golgari would say that life comes from death and death comes from life. But sometimes, death comes from the dead.\"\n—Vraska').
card_multiverse_id('putrid leech'/'DDM', '380242').

card_in_set('ray of command', 'DDM').
card_original_type('ray of command'/'DDM', 'Instant').
card_original_text('ray of command'/'DDM', 'Untap target creature an opponent controls and gain control of it until end of turn. That creature gains haste until end of turn. When you lose control of the creature, tap it.').
card_image_name('ray of command'/'DDM', 'ray of command').
card_uid('ray of command'/'DDM', 'DDM:Ray of Command:ray of command').
card_rarity('ray of command'/'DDM', 'Common').
card_artist('ray of command'/'DDM', 'Andrew Robinson').
card_number('ray of command'/'DDM', '29').
card_flavor_text('ray of command'/'DDM', '\"Heel.\"').
card_multiverse_id('ray of command'/'DDM', '380216').

card_in_set('reaper of the wilds', 'DDM').
card_original_type('reaper of the wilds'/'DDM', 'Creature — Gorgon').
card_original_text('reaper of the wilds'/'DDM', 'Whenever another creature dies, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)\n{B}: Reaper of the Wilds gains deathtouch until end of turn.\n{1}{G}: Reaper of the Wilds gains hexproof until end of turn.').
card_image_name('reaper of the wilds'/'DDM', 'reaper of the wilds').
card_uid('reaper of the wilds'/'DDM', 'DDM:Reaper of the Wilds:reaper of the wilds').
card_rarity('reaper of the wilds'/'DDM', 'Rare').
card_artist('reaper of the wilds'/'DDM', 'Karl Kopinski').
card_number('reaper of the wilds'/'DDM', '63').
card_multiverse_id('reaper of the wilds'/'DDM', '380224').

card_in_set('remand', 'DDM').
card_original_type('remand'/'DDM', 'Instant').
card_original_text('remand'/'DDM', 'Counter target spell. If that spell is countered this way, put it into its owner\'s hand instead of into that player\'s graveyard.\nDraw a card.').
card_image_name('remand'/'DDM', 'remand').
card_uid('remand'/'DDM', 'DDM:Remand:remand').
card_rarity('remand'/'DDM', 'Uncommon').
card_artist('remand'/'DDM', 'Zoltan Boros').
card_number('remand'/'DDM', '26').
card_flavor_text('remand'/'DDM', 'For the Azorius, the law can be a physical shield against chaos and anarchy.').
card_multiverse_id('remand'/'DDM', '380255').

card_in_set('riftwing cloudskate', 'DDM').
card_original_type('riftwing cloudskate'/'DDM', 'Creature — Illusion').
card_original_text('riftwing cloudskate'/'DDM', 'Flying\nWhen Riftwing Cloudskate enters the battlefield, return target permanent to its owner\'s hand.\nSuspend 3—{1}{U} (Rather than cast this card from your hand, you may pay {1}{U} and exile it with three time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)').
card_image_name('riftwing cloudskate'/'DDM', 'riftwing cloudskate').
card_uid('riftwing cloudskate'/'DDM', 'DDM:Riftwing Cloudskate:riftwing cloudskate').
card_rarity('riftwing cloudskate'/'DDM', 'Uncommon').
card_artist('riftwing cloudskate'/'DDM', 'Carl Critchlow').
card_number('riftwing cloudskate'/'DDM', '18').
card_multiverse_id('riftwing cloudskate'/'DDM', '380197').

card_in_set('river boa', 'DDM').
card_original_type('river boa'/'DDM', 'Creature — Snake').
card_original_text('river boa'/'DDM', 'Islandwalk (This creature can\'t be blocked as long as defending player controls an Island.)\n{G}: Regenerate River Boa.').
card_image_name('river boa'/'DDM', 'river boa').
card_uid('river boa'/'DDM', 'DDM:River Boa:river boa').
card_rarity('river boa'/'DDM', 'Uncommon').
card_artist('river boa'/'DDM', 'Paul Bonner').
card_number('river boa'/'DDM', '49').
card_flavor_text('river boa'/'DDM', '\"Sheath your swords! Cudgels only. I see a new pair of waterproof trek boots slithering away.\"\n—Nablus, North Hada trapper').
card_multiverse_id('river boa'/'DDM', '380260').

card_in_set('rogue\'s passage', 'DDM').
card_original_type('rogue\'s passage'/'DDM', 'Land').
card_original_text('rogue\'s passage'/'DDM', '{T}: Add {1} to your mana pool.\n{4}, {T}: Target creature can\'t be blocked this turn.').
card_image_name('rogue\'s passage'/'DDM', 'rogue\'s passage').
card_uid('rogue\'s passage'/'DDM', 'DDM:Rogue\'s Passage:rogue\'s passage').
card_rarity('rogue\'s passage'/'DDM', 'Uncommon').
card_artist('rogue\'s passage'/'DDM', 'Christine Choi').
card_number('rogue\'s passage'/'DDM', '77').
card_flavor_text('rogue\'s passage'/'DDM', 'Rumors quickly spread among thieves about a labyrinth without walls and a prize beyond all measures of worth.').
card_multiverse_id('rogue\'s passage'/'DDM', '380234').

card_in_set('sadistic augermage', 'DDM').
card_original_type('sadistic augermage'/'DDM', 'Creature — Human Wizard').
card_original_text('sadistic augermage'/'DDM', 'When Sadistic Augermage dies, each player puts a card from his or her hand on top of his or her library.').
card_image_name('sadistic augermage'/'DDM', 'sadistic augermage').
card_uid('sadistic augermage'/'DDM', 'DDM:Sadistic Augermage:sadistic augermage').
card_rarity('sadistic augermage'/'DDM', 'Common').
card_artist('sadistic augermage'/'DDM', 'Nick Percival').
card_number('sadistic augermage'/'DDM', '52').
card_flavor_text('sadistic augermage'/'DDM', '\"Don\'t worry. I know the drill.\"').
card_multiverse_id('sadistic augermage'/'DDM', '380189').

card_in_set('sea gate oracle', 'DDM').
card_original_type('sea gate oracle'/'DDM', 'Creature — Human Wizard').
card_original_text('sea gate oracle'/'DDM', 'When Sea Gate Oracle enters the battlefield, look at the top two cards of your library. Put one of them into your hand and the other on the bottom of your library.').
card_image_name('sea gate oracle'/'DDM', 'sea gate oracle').
card_uid('sea gate oracle'/'DDM', 'DDM:Sea Gate Oracle:sea gate oracle').
card_rarity('sea gate oracle'/'DDM', 'Common').
card_artist('sea gate oracle'/'DDM', 'Daniel Ljunggren').
card_number('sea gate oracle'/'DDM', '10').
card_flavor_text('sea gate oracle'/'DDM', '\"The secret entrance should be near.\"').
card_multiverse_id('sea gate oracle'/'DDM', '380247').

card_in_set('shadow alley denizen', 'DDM').
card_original_type('shadow alley denizen'/'DDM', 'Creature — Vampire Rogue').
card_original_text('shadow alley denizen'/'DDM', 'Whenever another black creature enters the battlefield under your control, target creature gains intimidate until end of turn. (It can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)').
card_image_name('shadow alley denizen'/'DDM', 'shadow alley denizen').
card_uid('shadow alley denizen'/'DDM', 'DDM:Shadow Alley Denizen:shadow alley denizen').
card_rarity('shadow alley denizen'/'DDM', 'Common').
card_artist('shadow alley denizen'/'DDM', 'Cynthia Sheppard').
card_number('shadow alley denizen'/'DDM', '44').
card_multiverse_id('shadow alley denizen'/'DDM', '380212').

card_in_set('slate street ruffian', 'DDM').
card_original_type('slate street ruffian'/'DDM', 'Creature — Human Warrior').
card_original_text('slate street ruffian'/'DDM', 'Whenever Slate Street Ruffian becomes blocked, defending player discards a card.').
card_image_name('slate street ruffian'/'DDM', 'slate street ruffian').
card_uid('slate street ruffian'/'DDM', 'DDM:Slate Street Ruffian:slate street ruffian').
card_rarity('slate street ruffian'/'DDM', 'Common').
card_artist('slate street ruffian'/'DDM', 'Jim Murray').
card_number('slate street ruffian'/'DDM', '53').
card_flavor_text('slate street ruffian'/'DDM', '\"Merciless to the point of psychosis. Let\'s give him a job.\"\n—Zelinas, Orzhov recruiter').
card_multiverse_id('slate street ruffian'/'DDM', '380226').

card_in_set('spawnwrithe', 'DDM').
card_original_type('spawnwrithe'/'DDM', 'Creature — Elemental').
card_original_text('spawnwrithe'/'DDM', 'Trample\nWhenever Spawnwrithe deals combat damage to a player, put a token that\'s a copy of Spawnwrithe onto the battlefield.').
card_image_name('spawnwrithe'/'DDM', 'spawnwrithe').
card_uid('spawnwrithe'/'DDM', 'DDM:Spawnwrithe:spawnwrithe').
card_rarity('spawnwrithe'/'DDM', 'Rare').
card_artist('spawnwrithe'/'DDM', 'Daarken').
card_number('spawnwrithe'/'DDM', '55').
card_flavor_text('spawnwrithe'/'DDM', 'Its victims feel only an itchy, wriggling feeling just under their skin. By then, it\'s far too late.').
card_multiverse_id('spawnwrithe'/'DDM', '380196').

card_in_set('spelltwine', 'DDM').
card_original_type('spelltwine'/'DDM', 'Sorcery').
card_original_text('spelltwine'/'DDM', 'Exile target instant or sorcery card from your graveyard and target instant or sorcery card from an opponent\'s graveyard. Copy those cards. Cast the copies if able without paying their mana costs. Exile Spelltwine.').
card_image_name('spelltwine'/'DDM', 'spelltwine').
card_uid('spelltwine'/'DDM', 'DDM:Spelltwine:spelltwine').
card_rarity('spelltwine'/'DDM', 'Rare').
card_artist('spelltwine'/'DDM', 'Noah Bradley').
card_number('spelltwine'/'DDM', '34').
card_multiverse_id('spelltwine'/'DDM', '380258').

card_in_set('stab wound', 'DDM').
card_original_type('stab wound'/'DDM', 'Enchantment — Aura').
card_original_text('stab wound'/'DDM', 'Enchant creature\nEnchanted creature gets -2/-2.\nAt the beginning of the upkeep of enchanted creature\'s controller, that player loses 2 life.').
card_image_name('stab wound'/'DDM', 'stab wound').
card_uid('stab wound'/'DDM', 'DDM:Stab Wound:stab wound').
card_rarity('stab wound'/'DDM', 'Common').
card_artist('stab wound'/'DDM', 'Scott Chou').
card_number('stab wound'/'DDM', '72').
card_multiverse_id('stab wound'/'DDM', '380204').

card_in_set('stealer of secrets', 'DDM').
card_original_type('stealer of secrets'/'DDM', 'Creature — Human Rogue').
card_original_text('stealer of secrets'/'DDM', 'Whenever Stealer of Secrets deals combat damage to a player, draw a card.').
card_image_name('stealer of secrets'/'DDM', 'stealer of secrets').
card_uid('stealer of secrets'/'DDM', 'DDM:Stealer of Secrets:stealer of secrets').
card_rarity('stealer of secrets'/'DDM', 'Common').
card_artist('stealer of secrets'/'DDM', 'Michael C. Hayes').
card_number('stealer of secrets'/'DDM', '11').
card_flavor_text('stealer of secrets'/'DDM', 'The Dimir would hire her, if only they knew where she lived. The Azorius would condemn her, if only they knew her name.').
card_multiverse_id('stealer of secrets'/'DDM', '380202').

card_in_set('stonefare crocodile', 'DDM').
card_original_type('stonefare crocodile'/'DDM', 'Creature — Crocodile').
card_original_text('stonefare crocodile'/'DDM', '{2}{B}: Stonefare Crocodile gains lifelink until end of turn. (Damage dealt by this creature also causes you to gain that much life.)').
card_image_name('stonefare crocodile'/'DDM', 'stonefare crocodile').
card_uid('stonefare crocodile'/'DDM', 'DDM:Stonefare Crocodile:stonefare crocodile').
card_rarity('stonefare crocodile'/'DDM', 'Common').
card_artist('stonefare crocodile'/'DDM', 'Tomasz Jedruszek').
card_number('stonefare crocodile'/'DDM', '56').
card_flavor_text('stonefare crocodile'/'DDM', 'The Izzet\'s plans to exploit the undercity ran into a few stubborn obstacles.').
card_multiverse_id('stonefare crocodile'/'DDM', '380230').

card_in_set('summoner\'s bane', 'DDM').
card_original_type('summoner\'s bane'/'DDM', 'Instant').
card_original_text('summoner\'s bane'/'DDM', 'Counter target creature spell. Put a 2/2 blue Illusion creature token onto the battlefield.').
card_image_name('summoner\'s bane'/'DDM', 'summoner\'s bane').
card_uid('summoner\'s bane'/'DDM', 'DDM:Summoner\'s Bane:summoner\'s bane').
card_rarity('summoner\'s bane'/'DDM', 'Uncommon').
card_artist('summoner\'s bane'/'DDM', 'Cyril Van Der Haegen').
card_number('summoner\'s bane'/'DDM', '31').
card_flavor_text('summoner\'s bane'/'DDM', '\"I don\'t need to have the perfect plan. My foe just has to have an imperfect one.\"\n—Jace Beleren').
card_multiverse_id('summoner\'s bane'/'DDM', '380266').

card_in_set('swamp', 'DDM').
card_original_type('swamp'/'DDM', 'Basic Land — Swamp').
card_original_text('swamp'/'DDM', 'B').
card_image_name('swamp'/'DDM', 'swamp1').
card_uid('swamp'/'DDM', 'DDM:Swamp:swamp1').
card_rarity('swamp'/'DDM', 'Basic Land').
card_artist('swamp'/'DDM', 'John Avon').
card_number('swamp'/'DDM', '79').
card_multiverse_id('swamp'/'DDM', '380271').

card_in_set('swamp', 'DDM').
card_original_type('swamp'/'DDM', 'Basic Land — Swamp').
card_original_text('swamp'/'DDM', 'B').
card_image_name('swamp'/'DDM', 'swamp2').
card_uid('swamp'/'DDM', 'DDM:Swamp:swamp2').
card_rarity('swamp'/'DDM', 'Basic Land').
card_artist('swamp'/'DDM', 'Yeong-Hao Han').
card_number('swamp'/'DDM', '80').
card_multiverse_id('swamp'/'DDM', '380239').

card_in_set('swamp', 'DDM').
card_original_type('swamp'/'DDM', 'Basic Land — Swamp').
card_original_text('swamp'/'DDM', 'B').
card_image_name('swamp'/'DDM', 'swamp3').
card_uid('swamp'/'DDM', 'DDM:Swamp:swamp3').
card_rarity('swamp'/'DDM', 'Basic Land').
card_artist('swamp'/'DDM', 'Adam Paquette').
card_number('swamp'/'DDM', '81').
card_multiverse_id('swamp'/'DDM', '380237').

card_in_set('swamp', 'DDM').
card_original_type('swamp'/'DDM', 'Basic Land — Swamp').
card_original_text('swamp'/'DDM', 'B').
card_image_name('swamp'/'DDM', 'swamp4').
card_uid('swamp'/'DDM', 'DDM:Swamp:swamp4').
card_rarity('swamp'/'DDM', 'Basic Land').
card_artist('swamp'/'DDM', 'Richard Wright').
card_number('swamp'/'DDM', '82').
card_multiverse_id('swamp'/'DDM', '380240').

card_in_set('swamp', 'DDM').
card_original_type('swamp'/'DDM', 'Basic Land — Swamp').
card_original_text('swamp'/'DDM', 'B').
card_image_name('swamp'/'DDM', 'swamp5').
card_uid('swamp'/'DDM', 'DDM:Swamp:swamp5').
card_rarity('swamp'/'DDM', 'Basic Land').
card_artist('swamp'/'DDM', 'Richard Wright').
card_number('swamp'/'DDM', '83').
card_multiverse_id('swamp'/'DDM', '380264').

card_in_set('tainted wood', 'DDM').
card_original_type('tainted wood'/'DDM', 'Land').
card_original_text('tainted wood'/'DDM', '{T}: Add {1} to your mana pool.\n{T}: Add {B} or {G} to your mana pool. Activate this ability only if you control a Swamp.').
card_image_name('tainted wood'/'DDM', 'tainted wood').
card_uid('tainted wood'/'DDM', 'DDM:Tainted Wood:tainted wood').
card_rarity('tainted wood'/'DDM', 'Uncommon').
card_artist('tainted wood'/'DDM', 'Rob Alexander').
card_number('tainted wood'/'DDM', '78').
card_multiverse_id('tainted wood'/'DDM', '380213').

card_in_set('tavern swindler', 'DDM').
card_original_type('tavern swindler'/'DDM', 'Creature — Human Rogue').
card_original_text('tavern swindler'/'DDM', '{T}, Pay 3 life: Flip a coin. If you win the flip, you gain 6 life.').
card_image_name('tavern swindler'/'DDM', 'tavern swindler').
card_uid('tavern swindler'/'DDM', 'DDM:Tavern Swindler:tavern swindler').
card_rarity('tavern swindler'/'DDM', 'Uncommon').
card_artist('tavern swindler'/'DDM', 'Cynthia Sheppard').
card_number('tavern swindler'/'DDM', '45').
card_flavor_text('tavern swindler'/'DDM', 'Rakdos cultists are her best customers. They never flinch at pain and are seldom good at math.').
card_multiverse_id('tavern swindler'/'DDM', '380195').

card_in_set('thought scour', 'DDM').
card_original_type('thought scour'/'DDM', 'Instant').
card_original_text('thought scour'/'DDM', 'Target player puts the top two cards of his or her library into his or her graveyard.\nDraw a card.').
card_image_name('thought scour'/'DDM', 'thought scour').
card_uid('thought scour'/'DDM', 'DDM:Thought Scour:thought scour').
card_rarity('thought scour'/'DDM', 'Common').
card_artist('thought scour'/'DDM', 'David Rapoza').
card_number('thought scour'/'DDM', '21').
card_flavor_text('thought scour'/'DDM', '\"As you inject the viscus vitae into the brain stem, don\'t let the spastic moaning bother you. It will soon become music to your ears.\"\n—Stitcher Geralf').
card_multiverse_id('thought scour'/'DDM', '380203').

card_in_set('tragic slip', 'DDM').
card_original_type('tragic slip'/'DDM', 'Instant').
card_original_text('tragic slip'/'DDM', 'Target creature gets -1/-1 until end of turn.\nMorbid — That creature gets -13/-13 until end of turn instead if a creature died this turn.').
card_image_name('tragic slip'/'DDM', 'tragic slip').
card_uid('tragic slip'/'DDM', 'DDM:Tragic Slip:tragic slip').
card_rarity('tragic slip'/'DDM', 'Common').
card_artist('tragic slip'/'DDM', 'Christopher Moeller').
card_number('tragic slip'/'DDM', '66').
card_flavor_text('tragic slip'/'DDM', 'Linger on death\'s door and risk being invited in.').
card_multiverse_id('tragic slip'/'DDM', '380221').

card_in_set('treasured find', 'DDM').
card_original_type('treasured find'/'DDM', 'Sorcery').
card_original_text('treasured find'/'DDM', 'Return target card from your graveyard to your hand. Exile Treasured Find.').
card_image_name('treasured find'/'DDM', 'treasured find').
card_uid('treasured find'/'DDM', 'DDM:Treasured Find:treasured find').
card_rarity('treasured find'/'DDM', 'Uncommon').
card_artist('treasured find'/'DDM', 'Jason Chan').
card_number('treasured find'/'DDM', '70').
card_flavor_text('treasured find'/'DDM', 'Gorgons crave beautiful things: gems, exquisite amulets, the alabaster corpses of the petrified dead . . .').
card_multiverse_id('treasured find'/'DDM', '380235').

card_in_set('underworld connections', 'DDM').
card_original_type('underworld connections'/'DDM', 'Enchantment — Aura').
card_original_text('underworld connections'/'DDM', 'Enchant land\nEnchanted land has \"{T}, Pay 1 life: Draw a card.\"').
card_image_name('underworld connections'/'DDM', 'underworld connections').
card_uid('underworld connections'/'DDM', 'DDM:Underworld Connections:underworld connections').
card_rarity('underworld connections'/'DDM', 'Rare').
card_artist('underworld connections'/'DDM', 'Yeong-Hao Han').
card_number('underworld connections'/'DDM', '73').
card_flavor_text('underworld connections'/'DDM', 'If you\'re looking for it, it\'s available. The question is how much you\'re willing to pay.').
card_multiverse_id('underworld connections'/'DDM', '380227').

card_in_set('vinelasher kudzu', 'DDM').
card_original_type('vinelasher kudzu'/'DDM', 'Creature — Plant').
card_original_text('vinelasher kudzu'/'DDM', 'Whenever a land enters the battlefield under your control, put a +1/+1 counter on Vinelasher Kudzu.').
card_image_name('vinelasher kudzu'/'DDM', 'vinelasher kudzu').
card_uid('vinelasher kudzu'/'DDM', 'DDM:Vinelasher Kudzu:vinelasher kudzu').
card_rarity('vinelasher kudzu'/'DDM', 'Rare').
card_artist('vinelasher kudzu'/'DDM', 'Mark Tedin').
card_number('vinelasher kudzu'/'DDM', '50').
card_flavor_text('vinelasher kudzu'/'DDM', 'It grows to hate you.').
card_multiverse_id('vinelasher kudzu'/'DDM', '380244').

card_in_set('vraska the unseen', 'DDM').
card_original_type('vraska the unseen'/'DDM', 'Planeswalker — Vraska').
card_original_text('vraska the unseen'/'DDM', '+1: Until your next turn, whenever a creature deals combat damage to Vraska the Unseen, destroy that creature.\n-3: Destroy target nonland permanent.\n-7: Put three 1/1 black Assassin creature tokens onto the battlefield with \"Whenever this creature deals combat damage to a player, that player loses the game.\"').
card_image_name('vraska the unseen'/'DDM', 'vraska the unseen').
card_uid('vraska the unseen'/'DDM', 'DDM:Vraska the Unseen:vraska the unseen').
card_rarity('vraska the unseen'/'DDM', 'Mythic Rare').
card_artist('vraska the unseen'/'DDM', 'Igor Kieryluk').
card_number('vraska the unseen'/'DDM', '42').
card_multiverse_id('vraska the unseen'/'DDM', '380214').

card_in_set('wight of precinct six', 'DDM').
card_original_type('wight of precinct six'/'DDM', 'Creature — Zombie').
card_original_text('wight of precinct six'/'DDM', 'Wight of Precinct Six gets +1/+1 for each creature card in your opponents\' graveyards.').
card_image_name('wight of precinct six'/'DDM', 'wight of precinct six').
card_uid('wight of precinct six'/'DDM', 'DDM:Wight of Precinct Six:wight of precinct six').
card_rarity('wight of precinct six'/'DDM', 'Uncommon').
card_artist('wight of precinct six'/'DDM', 'Ryan Barger').
card_number('wight of precinct six'/'DDM', '46').
card_flavor_text('wight of precinct six'/'DDM', 'Even the lost and undead need a protector.').
card_multiverse_id('wight of precinct six'/'DDM', '380207').
