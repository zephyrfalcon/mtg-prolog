% Portal Demo Game

set('pPOD').
set_name('pPOD', 'Portal Demo Game').
set_release_date('pPOD', '1997-05-01').
set_border('pPOD', 'black').
set_type('pPOD', 'promo').

card_in_set('armored pegasus', 'pPOD').
card_original_type('armored pegasus'/'pPOD', 'Creature — Pegasus').
card_original_text('armored pegasus'/'pPOD', '').
card_image_name('armored pegasus'/'pPOD', 'armored pegasus').
card_uid('armored pegasus'/'pPOD', 'pPOD:Armored Pegasus:armored pegasus').
card_rarity('armored pegasus'/'pPOD', 'Special').
card_artist('armored pegasus'/'pPOD', 'Andrew Robinson').
card_number('armored pegasus'/'pPOD', '1').
card_flavor_text('armored pegasus'/'pPOD', 'Asked how it survived a run-in with a bog imp, the pegasus just shook its mane and burped.').

card_in_set('bull hippo', 'pPOD').
card_original_type('bull hippo'/'pPOD', 'Creature — Hippo').
card_original_text('bull hippo'/'pPOD', '').
card_image_name('bull hippo'/'pPOD', 'bull hippo').
card_uid('bull hippo'/'pPOD', 'pPOD:Bull Hippo:bull hippo').
card_rarity('bull hippo'/'pPOD', 'Special').
card_artist('bull hippo'/'pPOD', 'Roger Raupp').
card_number('bull hippo'/'pPOD', '2').

card_in_set('cloud pirates', 'pPOD').
card_original_type('cloud pirates'/'pPOD', 'Creature — Human Pirate').
card_original_text('cloud pirates'/'pPOD', '').
card_image_name('cloud pirates'/'pPOD', 'cloud pirates').
card_uid('cloud pirates'/'pPOD', 'pPOD:Cloud Pirates:cloud pirates').
card_rarity('cloud pirates'/'pPOD', 'Special').
card_artist('cloud pirates'/'pPOD', 'Phil Foglio').
card_number('cloud pirates'/'pPOD', '3').

card_in_set('feral shadow', 'pPOD').
card_original_type('feral shadow'/'pPOD', 'Creature — Nightstalker').
card_original_text('feral shadow'/'pPOD', '').
card_image_name('feral shadow'/'pPOD', 'feral shadow').
card_uid('feral shadow'/'pPOD', 'pPOD:Feral Shadow:feral shadow').
card_rarity('feral shadow'/'pPOD', 'Special').
card_artist('feral shadow'/'pPOD', 'Colin MacNeil').
card_number('feral shadow'/'pPOD', '4').
card_flavor_text('feral shadow'/'pPOD', 'Not all shadows are cast by light—some are cast by darkness.').

card_in_set('snapping drake', 'pPOD').
card_original_type('snapping drake'/'pPOD', 'Creature — Drake').
card_original_text('snapping drake'/'pPOD', '').
card_image_name('snapping drake'/'pPOD', 'snapping drake').
card_uid('snapping drake'/'pPOD', 'pPOD:Snapping Drake:snapping drake').
card_rarity('snapping drake'/'pPOD', 'Special').
card_artist('snapping drake'/'pPOD', 'Christopher Rush').
card_number('snapping drake'/'pPOD', '5').
card_flavor_text('snapping drake'/'pPOD', 'Drakes claim to be dragons—until the dragons show up.').

card_in_set('storm crow', 'pPOD').
card_original_type('storm crow'/'pPOD', 'Creature — Bird').
card_original_text('storm crow'/'pPOD', '').
card_image_name('storm crow'/'pPOD', 'storm crow').
card_uid('storm crow'/'pPOD', 'pPOD:Storm Crow:storm crow').
card_rarity('storm crow'/'pPOD', 'Special').
card_artist('storm crow'/'pPOD', 'Una Fricker').
card_number('storm crow'/'pPOD', '6').
card_flavor_text('storm crow'/'pPOD', 'Storm crow descending, winter unending. Storm crow departing, summer is starting.').
