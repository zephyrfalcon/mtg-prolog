% Nemesis

set('NMS').
set_name('NMS', 'Nemesis').
set_release_date('NMS', '2000-02-14').
set_border('NMS', 'black').
set_type('NMS', 'expansion').
set_block('NMS', 'Masques').

card_in_set('accumulated knowledge', 'NMS').
card_original_type('accumulated knowledge'/'NMS', 'Instant').
card_original_text('accumulated knowledge'/'NMS', 'Draw a card, then draw cards equal to the number of Accumulated Knowledge cards in all graveyards.').
card_image_name('accumulated knowledge'/'NMS', 'accumulated knowledge').
card_uid('accumulated knowledge'/'NMS', 'NMS:Accumulated Knowledge:accumulated knowledge').
card_rarity('accumulated knowledge'/'NMS', 'Common').
card_artist('accumulated knowledge'/'NMS', 'Randy Gallegos').
card_number('accumulated knowledge'/'NMS', '26').
card_flavor_text('accumulated knowledge'/'NMS', '\"I have seen and heard much here. There are secrets within secrets. Let me show you.\"\n—Takara, to Eladamri').
card_multiverse_id('accumulated knowledge'/'NMS', '21285').

card_in_set('æther barrier', 'NMS').
card_original_type('æther barrier'/'NMS', 'Enchantment').
card_original_text('æther barrier'/'NMS', 'Whenever a player plays a creature spell, that player sacrifices a permanent unless he or she pays {1}.').
card_first_print('æther barrier', 'NMS').
card_image_name('æther barrier'/'NMS', 'aether barrier').
card_uid('æther barrier'/'NMS', 'NMS:Æther Barrier:aether barrier').
card_rarity('æther barrier'/'NMS', 'Rare').
card_artist('æther barrier'/'NMS', 'David Martin').
card_number('æther barrier'/'NMS', '27').
card_flavor_text('æther barrier'/'NMS', '\"Creatures fade in and out of existence. It\'s an unfortunate side effect. The losses are tolerable.\"\n—Belbe').
card_multiverse_id('æther barrier'/'NMS', '22289').

card_in_set('air bladder', 'NMS').
card_original_type('air bladder'/'NMS', 'Enchant Creature').
card_original_text('air bladder'/'NMS', 'Enchanted creature has flying.\nEnchanted creature can block only creatures with flying.').
card_first_print('air bladder', 'NMS').
card_image_name('air bladder'/'NMS', 'air bladder').
card_uid('air bladder'/'NMS', 'NMS:Air Bladder:air bladder').
card_rarity('air bladder'/'NMS', 'Common').
card_artist('air bladder'/'NMS', 'Donato Giancola').
card_number('air bladder'/'NMS', '28').
card_flavor_text('air bladder'/'NMS', 'Random mutations among Rootwater merfolk were common—and disturbing.').
card_multiverse_id('air bladder'/'NMS', '22377').

card_in_set('ancient hydra', 'NMS').
card_original_type('ancient hydra'/'NMS', 'Creature — Hydra').
card_original_text('ancient hydra'/'NMS', 'Fading 5 (This creature comes into play with five fade counters on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)\n{1}, Remove a fade counter from Ancient Hydra: Ancient Hydra deals 1 damage to target creature or player.').
card_first_print('ancient hydra', 'NMS').
card_image_name('ancient hydra'/'NMS', 'ancient hydra').
card_uid('ancient hydra'/'NMS', 'NMS:Ancient Hydra:ancient hydra').
card_rarity('ancient hydra'/'NMS', 'Uncommon').
card_artist('ancient hydra'/'NMS', 'Scott M. Fischer').
card_number('ancient hydra'/'NMS', '76').
card_multiverse_id('ancient hydra'/'NMS', '22296').

card_in_set('angelic favor', 'NMS').
card_original_type('angelic favor'/'NMS', 'Instant').
card_original_text('angelic favor'/'NMS', 'If you control a plains, you may tap an untapped creature you control instead of paying Angelic Favor\'s mana cost.\nPlay Angelic Favor only during combat.\nPut a 4/4 white Angel creature token with flying into play. Remove it from the game at end of turn.').
card_first_print('angelic favor', 'NMS').
card_image_name('angelic favor'/'NMS', 'angelic favor').
card_uid('angelic favor'/'NMS', 'NMS:Angelic Favor:angelic favor').
card_rarity('angelic favor'/'NMS', 'Uncommon').
card_artist('angelic favor'/'NMS', 'Paolo Parente').
card_number('angelic favor'/'NMS', '1').
card_multiverse_id('angelic favor'/'NMS', '21258').

card_in_set('animate land', 'NMS').
card_original_type('animate land'/'NMS', 'Instant').
card_original_text('animate land'/'NMS', 'Until end of turn, target land is a 3/3 creature that\'s still a land.').
card_first_print('animate land', 'NMS').
card_image_name('animate land'/'NMS', 'animate land').
card_uid('animate land'/'NMS', 'NMS:Animate Land:animate land').
card_rarity('animate land'/'NMS', 'Uncommon').
card_artist('animate land'/'NMS', 'Rebecca Guay').
card_number('animate land'/'NMS', '101').
card_flavor_text('animate land'/'NMS', 'Irony is getting walked on by the earth instead of the other way around.').
card_multiverse_id('animate land'/'NMS', '21377').

card_in_set('arc mage', 'NMS').
card_original_type('arc mage'/'NMS', 'Creature — Spellshaper').
card_original_text('arc mage'/'NMS', '{2}{R}, {T}, Discard a card from your hand: Arc Mage deals 2 damage divided as you choose among any number of target creatures and/or players.').
card_first_print('arc mage', 'NMS').
card_image_name('arc mage'/'NMS', 'arc mage').
card_uid('arc mage'/'NMS', 'NMS:Arc Mage:arc mage').
card_rarity('arc mage'/'NMS', 'Uncommon').
card_artist('arc mage'/'NMS', 'Terese Nielsen').
card_number('arc mage'/'NMS', '77').
card_multiverse_id('arc mage'/'NMS', '21343').

card_in_set('ascendant evincar', 'NMS').
card_original_type('ascendant evincar'/'NMS', 'Creature — Legend').
card_original_text('ascendant evincar'/'NMS', 'Flying\nOther black creatures get +1/+1.\nNonblack creatures get -1/-1.').
card_first_print('ascendant evincar', 'NMS').
card_image_name('ascendant evincar'/'NMS', 'ascendant evincar').
card_uid('ascendant evincar'/'NMS', 'NMS:Ascendant Evincar:ascendant evincar').
card_rarity('ascendant evincar'/'NMS', 'Rare').
card_artist('ascendant evincar'/'NMS', 'Mark Zug').
card_number('ascendant evincar'/'NMS', '51').
card_flavor_text('ascendant evincar'/'NMS', '\"Things are going to change around here.\"\n—Crovax').
card_multiverse_id('ascendant evincar'/'NMS', '22897').

card_in_set('avenger en-dal', 'NMS').
card_original_type('avenger en-dal'/'NMS', 'Creature — Spellshaper').
card_original_text('avenger en-dal'/'NMS', '{2}{W}, {T}, Discard a card from your hand: Remove target attacking creature from the game. Its controller gains life equal to its toughness.').
card_first_print('avenger en-dal', 'NMS').
card_image_name('avenger en-dal'/'NMS', 'avenger en-dal').
card_uid('avenger en-dal'/'NMS', 'NMS:Avenger en-Dal:avenger en-dal').
card_rarity('avenger en-dal'/'NMS', 'Rare').
card_artist('avenger en-dal'/'NMS', 'Ron Spencer').
card_number('avenger en-dal'/'NMS', '2').
card_multiverse_id('avenger en-dal'/'NMS', '21263').

card_in_set('battlefield percher', 'NMS').
card_original_type('battlefield percher'/'NMS', 'Creature — Bird').
card_original_text('battlefield percher'/'NMS', 'Flying \nBattlefield Percher can block only creatures with flying. \n{1}{B} Battlefield Percher gets +1/+1 until end of turn.').
card_first_print('battlefield percher', 'NMS').
card_image_name('battlefield percher'/'NMS', 'battlefield percher').
card_uid('battlefield percher'/'NMS', 'NMS:Battlefield Percher:battlefield percher').
card_rarity('battlefield percher'/'NMS', 'Uncommon').
card_artist('battlefield percher'/'NMS', 'Edward P. Beard, Jr.').
card_number('battlefield percher'/'NMS', '52').
card_multiverse_id('battlefield percher'/'NMS', '21320').

card_in_set('belbe\'s armor', 'NMS').
card_original_type('belbe\'s armor'/'NMS', 'Artifact').
card_original_text('belbe\'s armor'/'NMS', '{X}, {T}: Target creature gets -X/+X until end of turn.').
card_first_print('belbe\'s armor', 'NMS').
card_image_name('belbe\'s armor'/'NMS', 'belbe\'s armor').
card_uid('belbe\'s armor'/'NMS', 'NMS:Belbe\'s Armor:belbe\'s armor').
card_rarity('belbe\'s armor'/'NMS', 'Uncommon').
card_artist('belbe\'s armor'/'NMS', 'D. Alexander Gregory').
card_number('belbe\'s armor'/'NMS', '126').
card_flavor_text('belbe\'s armor'/'NMS', 'It protects her from the ravages of Phyrexia and the savages of Rath.').
card_multiverse_id('belbe\'s armor'/'NMS', '21390').

card_in_set('belbe\'s percher', 'NMS').
card_original_type('belbe\'s percher'/'NMS', 'Creature — Bird').
card_original_text('belbe\'s percher'/'NMS', 'Flying \nBelbe\'s Percher can block only creatures with flying.').
card_first_print('belbe\'s percher', 'NMS').
card_image_name('belbe\'s percher'/'NMS', 'belbe\'s percher').
card_uid('belbe\'s percher'/'NMS', 'NMS:Belbe\'s Percher:belbe\'s percher').
card_rarity('belbe\'s percher'/'NMS', 'Common').
card_artist('belbe\'s percher'/'NMS', 'Edward P. Beard, Jr.').
card_number('belbe\'s percher'/'NMS', '53').
card_flavor_text('belbe\'s percher'/'NMS', 'Long and powerful, these birds were used to carry messages to and from the evincar.').
card_multiverse_id('belbe\'s percher'/'NMS', '21308').

card_in_set('belbe\'s portal', 'NMS').
card_original_type('belbe\'s portal'/'NMS', 'Artifact').
card_original_text('belbe\'s portal'/'NMS', 'As Belbe\'s Portal comes into play, choose a creature type.\n{3}, {T}: Put a creature card of the chosen type from your hand into play.').
card_first_print('belbe\'s portal', 'NMS').
card_image_name('belbe\'s portal'/'NMS', 'belbe\'s portal').
card_uid('belbe\'s portal'/'NMS', 'NMS:Belbe\'s Portal:belbe\'s portal').
card_rarity('belbe\'s portal'/'NMS', 'Rare').
card_artist('belbe\'s portal'/'NMS', 'Mark Tedin').
card_number('belbe\'s portal'/'NMS', '127').
card_flavor_text('belbe\'s portal'/'NMS', 'The fight will continue on a new battlefield.').
card_multiverse_id('belbe\'s portal'/'NMS', '21401').

card_in_set('blastoderm', 'NMS').
card_original_type('blastoderm'/'NMS', 'Creature — Beast').
card_original_text('blastoderm'/'NMS', 'Fading 3 (This creature comes into play with three fade counters on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)\nBlastoderm can\'t be the target of spells or abilities.').
card_image_name('blastoderm'/'NMS', 'blastoderm').
card_uid('blastoderm'/'NMS', 'NMS:Blastoderm:blastoderm').
card_rarity('blastoderm'/'NMS', 'Common').
card_artist('blastoderm'/'NMS', 'Eric Peterson').
card_number('blastoderm'/'NMS', '102').
card_multiverse_id('blastoderm'/'NMS', '21363').

card_in_set('blinding angel', 'NMS').
card_original_type('blinding angel'/'NMS', 'Creature — Angel').
card_original_text('blinding angel'/'NMS', 'Flying \nWhenever Blinding Angel deals combat damage to a player, that player skips his or her next combat phase.').
card_first_print('blinding angel', 'NMS').
card_image_name('blinding angel'/'NMS', 'blinding angel').
card_uid('blinding angel'/'NMS', 'NMS:Blinding Angel:blinding angel').
card_rarity('blinding angel'/'NMS', 'Rare').
card_artist('blinding angel'/'NMS', 'Todd Lockwood').
card_number('blinding angel'/'NMS', '3').
card_flavor_text('blinding angel'/'NMS', '\"Their eyes will shrivel and blacken before faith\'s true light.\"').
card_multiverse_id('blinding angel'/'NMS', '21273').

card_in_set('bola warrior', 'NMS').
card_original_type('bola warrior'/'NMS', 'Creature — Spellshaper').
card_original_text('bola warrior'/'NMS', '{R}, {T}, Discard a card from your hand: Target creature can\'t block this turn.').
card_first_print('bola warrior', 'NMS').
card_image_name('bola warrior'/'NMS', 'bola warrior').
card_uid('bola warrior'/'NMS', 'NMS:Bola Warrior:bola warrior').
card_rarity('bola warrior'/'NMS', 'Common').
card_artist('bola warrior'/'NMS', 'Adam Rex').
card_number('bola warrior'/'NMS', '78').
card_flavor_text('bola warrior'/'NMS', 'The ingredients for panic include equal parts danger, uncertainty, and helplessness.').
card_multiverse_id('bola warrior'/'NMS', '22060').

card_in_set('carrion wall', 'NMS').
card_original_type('carrion wall'/'NMS', 'Creature — Wall').
card_original_text('carrion wall'/'NMS', '(Walls can\'t attack.)\n{1}{B} Regenerate Carrion Wall.').
card_first_print('carrion wall', 'NMS').
card_image_name('carrion wall'/'NMS', 'carrion wall').
card_uid('carrion wall'/'NMS', 'NMS:Carrion Wall:carrion wall').
card_rarity('carrion wall'/'NMS', 'Uncommon').
card_artist('carrion wall'/'NMS', 'Tony Szczudlo').
card_number('carrion wall'/'NMS', '54').
card_flavor_text('carrion wall'/'NMS', 'A wall built from the bodies of the enemy will never lack building material in times of war.').
card_multiverse_id('carrion wall'/'NMS', '21319').

card_in_set('chieftain en-dal', 'NMS').
card_original_type('chieftain en-dal'/'NMS', 'Creature — Knight').
card_original_text('chieftain en-dal'/'NMS', 'Whenever Chieftain en-Dal attacks, attacking creatures gain first strike until end of turn.').
card_first_print('chieftain en-dal', 'NMS').
card_image_name('chieftain en-dal'/'NMS', 'chieftain en-dal').
card_uid('chieftain en-dal'/'NMS', 'NMS:Chieftain en-Dal:chieftain en-dal').
card_rarity('chieftain en-dal'/'NMS', 'Uncommon').
card_artist('chieftain en-dal'/'NMS', 'Orizio Daniele').
card_number('chieftain en-dal'/'NMS', '4').
card_flavor_text('chieftain en-dal'/'NMS', 'The clearest commands are practical examples.').
card_multiverse_id('chieftain en-dal'/'NMS', '21264').

card_in_set('cloudskate', 'NMS').
card_original_type('cloudskate'/'NMS', 'Creature — Illusion').
card_original_text('cloudskate'/'NMS', 'Flying \nFading 3 (This creature comes into play with three fade counters on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)').
card_first_print('cloudskate', 'NMS').
card_image_name('cloudskate'/'NMS', 'cloudskate').
card_uid('cloudskate'/'NMS', 'NMS:Cloudskate:cloudskate').
card_rarity('cloudskate'/'NMS', 'Common').
card_artist('cloudskate'/'NMS', 'Carl Critchlow').
card_number('cloudskate'/'NMS', '29').
card_multiverse_id('cloudskate'/'NMS', '21280').

card_in_set('coiling woodworm', 'NMS').
card_original_type('coiling woodworm'/'NMS', 'Creature — Insect').
card_original_text('coiling woodworm'/'NMS', 'Coiling Woodworm\'s power is equal to the number of forests in play.').
card_first_print('coiling woodworm', 'NMS').
card_image_name('coiling woodworm'/'NMS', 'coiling woodworm').
card_uid('coiling woodworm'/'NMS', 'NMS:Coiling Woodworm:coiling woodworm').
card_rarity('coiling woodworm'/'NMS', 'Uncommon').
card_artist('coiling woodworm'/'NMS', 'David Martin').
card_number('coiling woodworm'/'NMS', '103').
card_flavor_text('coiling woodworm'/'NMS', 'Only the elves know where the woods end and the woodworm begins.').
card_multiverse_id('coiling woodworm'/'NMS', '21364').

card_in_set('complex automaton', 'NMS').
card_original_type('complex automaton'/'NMS', 'Artifact Creature — Golem').
card_original_text('complex automaton'/'NMS', 'At the beginning of your upkeep, if you control seven or more permanents, return Complex Automaton to its owner\'s hand.').
card_first_print('complex automaton', 'NMS').
card_image_name('complex automaton'/'NMS', 'complex automaton').
card_uid('complex automaton'/'NMS', 'NMS:Complex Automaton:complex automaton').
card_rarity('complex automaton'/'NMS', 'Rare').
card_artist('complex automaton'/'NMS', 'Dana Knutson').
card_number('complex automaton'/'NMS', '128').
card_flavor_text('complex automaton'/'NMS', '\"This wasteful design cannot possibly be Phyrexian.\"\n—Belbe').
card_multiverse_id('complex automaton'/'NMS', '21395').

card_in_set('dark triumph', 'NMS').
card_original_type('dark triumph'/'NMS', 'Instant').
card_original_text('dark triumph'/'NMS', 'If you control a swamp, you may sacrifice a creature instead of paying Dark Triumph\'s mana cost. \nCreatures you control get +2/+0 until end of turn.').
card_first_print('dark triumph', 'NMS').
card_image_name('dark triumph'/'NMS', 'dark triumph').
card_uid('dark triumph'/'NMS', 'NMS:Dark Triumph:dark triumph').
card_rarity('dark triumph'/'NMS', 'Uncommon').
card_artist('dark triumph'/'NMS', 'Adam Rex').
card_number('dark triumph'/'NMS', '55').
card_flavor_text('dark triumph'/'NMS', 'All that\'s left is the coronation.').
card_multiverse_id('dark triumph'/'NMS', '21322').

card_in_set('daze', 'NMS').
card_original_type('daze'/'NMS', 'Instant').
card_original_text('daze'/'NMS', 'You may return an island you control to its owner\'s hand instead of paying Daze\'s mana cost.\nCounter target spell unless its controller pays {1}.').
card_first_print('daze', 'NMS').
card_image_name('daze'/'NMS', 'daze').
card_uid('daze'/'NMS', 'NMS:Daze:daze').
card_rarity('daze'/'NMS', 'Common').
card_artist('daze'/'NMS', 'Matthew D. Wilson').
card_number('daze'/'NMS', '30').
card_multiverse_id('daze'/'NMS', '21284').

card_in_set('death pit offering', 'NMS').
card_original_type('death pit offering'/'NMS', 'Enchantment').
card_original_text('death pit offering'/'NMS', 'As Death Pit Offering comes into play, sacrifice all creatures you control.\nCreatures you control get +2/+2.').
card_first_print('death pit offering', 'NMS').
card_image_name('death pit offering'/'NMS', 'death pit offering').
card_uid('death pit offering'/'NMS', 'NMS:Death Pit Offering:death pit offering').
card_rarity('death pit offering'/'NMS', 'Rare').
card_artist('death pit offering'/'NMS', 'Pete Venters').
card_number('death pit offering'/'NMS', '56').
card_flavor_text('death pit offering'/'NMS', '\"Kill them all and feed them to the new recruits.\"\n—Crovax').
card_multiverse_id('death pit offering'/'NMS', '21330').

card_in_set('defender en-vec', 'NMS').
card_original_type('defender en-vec'/'NMS', 'Creature — Cleric').
card_original_text('defender en-vec'/'NMS', 'Fading 4 (This creature comes into play with four fade counters on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)\nRemove a fade counter from Defender en-Vec: Prevent the next 2 damage that would be dealt to target creature or player this turn.').
card_first_print('defender en-vec', 'NMS').
card_image_name('defender en-vec'/'NMS', 'defender en-vec').
card_uid('defender en-vec'/'NMS', 'NMS:Defender en-Vec:defender en-vec').
card_rarity('defender en-vec'/'NMS', 'Common').
card_artist('defender en-vec'/'NMS', 'Bradley Williams').
card_number('defender en-vec'/'NMS', '5').
card_multiverse_id('defender en-vec'/'NMS', '22026').

card_in_set('defiant falcon', 'NMS').
card_original_type('defiant falcon'/'NMS', 'Creature — Rebel Bird').
card_original_text('defiant falcon'/'NMS', 'Flying\n{4}, {T}: Search your library for a Rebel card with converted mana cost 3 or less and put that card into play. Then shuffle your library.').
card_first_print('defiant falcon', 'NMS').
card_image_name('defiant falcon'/'NMS', 'defiant falcon').
card_uid('defiant falcon'/'NMS', 'NMS:Defiant Falcon:defiant falcon').
card_rarity('defiant falcon'/'NMS', 'Common').
card_artist('defiant falcon'/'NMS', 'Heather Hudson').
card_number('defiant falcon'/'NMS', '6').
card_multiverse_id('defiant falcon'/'NMS', '21251').

card_in_set('defiant vanguard', 'NMS').
card_original_type('defiant vanguard'/'NMS', 'Creature — Rebel').
card_original_text('defiant vanguard'/'NMS', 'When Defiant Vanguard blocks, at end of combat, destroy it and all creatures it blocked this turn.\n{5}, {T}: Search your library for a Rebel card with converted mana cost 4 or less and put that card into play. Then shuffle your library.').
card_first_print('defiant vanguard', 'NMS').
card_image_name('defiant vanguard'/'NMS', 'defiant vanguard').
card_uid('defiant vanguard'/'NMS', 'NMS:Defiant Vanguard:defiant vanguard').
card_rarity('defiant vanguard'/'NMS', 'Uncommon').
card_artist('defiant vanguard'/'NMS', 'Pete Venters').
card_number('defiant vanguard'/'NMS', '7').
card_multiverse_id('defiant vanguard'/'NMS', '21266').

card_in_set('divining witch', 'NMS').
card_original_type('divining witch'/'NMS', 'Creature — Spellshaper').
card_original_text('divining witch'/'NMS', '{1}{B}, {T}, Discard a card from your hand: Name a card. Remove the top six cards of your library from the game. Reveal cards from the top of your library until you reveal the named card, then put that card into your hand. Remove all other cards revealed this way from the game.').
card_first_print('divining witch', 'NMS').
card_image_name('divining witch'/'NMS', 'divining witch').
card_uid('divining witch'/'NMS', 'NMS:Divining Witch:divining witch').
card_rarity('divining witch'/'NMS', 'Rare').
card_artist('divining witch'/'NMS', 'Donato Giancola').
card_number('divining witch'/'NMS', '57').
card_multiverse_id('divining witch'/'NMS', '21318').

card_in_set('dominate', 'NMS').
card_original_type('dominate'/'NMS', 'Instant').
card_original_text('dominate'/'NMS', 'Gain control of target creature with converted mana cost X or less. (This spell\'s effect doesn\'t end at end of turn.)').
card_first_print('dominate', 'NMS').
card_image_name('dominate'/'NMS', 'dominate').
card_uid('dominate'/'NMS', 'NMS:Dominate:dominate').
card_rarity('dominate'/'NMS', 'Uncommon').
card_artist('dominate'/'NMS', 'Scott Hampton').
card_number('dominate'/'NMS', '31').
card_flavor_text('dominate'/'NMS', '\"I\'ve come to reclaim what is mine. And you, Greven, are mine.\"\n—Volrath').
card_multiverse_id('dominate'/'NMS', '21303').

card_in_set('downhill charge', 'NMS').
card_original_type('downhill charge'/'NMS', 'Instant').
card_original_text('downhill charge'/'NMS', 'You may sacrifice a mountain instead of paying Downhill Charge\'s mana cost.\nTarget creature gets +X/+0 until end of turn, where X is the number of mountains you control.').
card_first_print('downhill charge', 'NMS').
card_image_name('downhill charge'/'NMS', 'downhill charge').
card_uid('downhill charge'/'NMS', 'NMS:Downhill Charge:downhill charge').
card_rarity('downhill charge'/'NMS', 'Common').
card_artist('downhill charge'/'NMS', 'Greg & Tim Hildebrandt').
card_number('downhill charge'/'NMS', '79').
card_multiverse_id('downhill charge'/'NMS', '21338').

card_in_set('ensnare', 'NMS').
card_original_type('ensnare'/'NMS', 'Instant').
card_original_text('ensnare'/'NMS', 'You may return two islands you control to their owner\'s hand instead of paying Ensnare\'s mana cost.\nTap all creatures.').
card_first_print('ensnare', 'NMS').
card_image_name('ensnare'/'NMS', 'ensnare').
card_uid('ensnare'/'NMS', 'NMS:Ensnare:ensnare').
card_rarity('ensnare'/'NMS', 'Uncommon').
card_artist('ensnare'/'NMS', 'Gao Yan').
card_number('ensnare'/'NMS', '32').
card_flavor_text('ensnare'/'NMS', 'Rootwater has become as dangerous to its denizens as to its enemies.').
card_multiverse_id('ensnare'/'NMS', '22881').

card_in_set('eye of yawgmoth', 'NMS').
card_original_type('eye of yawgmoth'/'NMS', 'Artifact').
card_original_text('eye of yawgmoth'/'NMS', '{3}, {T}, Sacrifice a creature: Reveal cards from the top of your library equal to the sacrificed creature\'s power. Put one into your hand and remove the rest from the game.').
card_first_print('eye of yawgmoth', 'NMS').
card_image_name('eye of yawgmoth'/'NMS', 'eye of yawgmoth').
card_uid('eye of yawgmoth'/'NMS', 'NMS:Eye of Yawgmoth:eye of yawgmoth').
card_rarity('eye of yawgmoth'/'NMS', 'Rare').
card_artist('eye of yawgmoth'/'NMS', 'DiTerlizzi').
card_number('eye of yawgmoth'/'NMS', '129').
card_multiverse_id('eye of yawgmoth'/'NMS', '21392').

card_in_set('fanatical devotion', 'NMS').
card_original_type('fanatical devotion'/'NMS', 'Enchantment').
card_original_text('fanatical devotion'/'NMS', 'Sacrifice a creature: Regenerate target creature.').
card_first_print('fanatical devotion', 'NMS').
card_image_name('fanatical devotion'/'NMS', 'fanatical devotion').
card_uid('fanatical devotion'/'NMS', 'NMS:Fanatical Devotion:fanatical devotion').
card_rarity('fanatical devotion'/'NMS', 'Common').
card_artist('fanatical devotion'/'NMS', 'Massimilano Frezzato').
card_number('fanatical devotion'/'NMS', '8').
card_flavor_text('fanatical devotion'/'NMS', 'The Oracle told Eladamri he would save his people by abandoning them. His followers adopted that ideal.').
card_multiverse_id('fanatical devotion'/'NMS', '21260').

card_in_set('flame rift', 'NMS').
card_original_type('flame rift'/'NMS', 'Sorcery').
card_original_text('flame rift'/'NMS', 'Flame Rift deals 4 damage to each player.').
card_first_print('flame rift', 'NMS').
card_image_name('flame rift'/'NMS', 'flame rift').
card_uid('flame rift'/'NMS', 'NMS:Flame Rift:flame rift').
card_rarity('flame rift'/'NMS', 'Common').
card_artist('flame rift'/'NMS', 'Ben Thompson').
card_number('flame rift'/'NMS', '80').
card_flavor_text('flame rift'/'NMS', 'Crovax hungered for power, and the stronghold devoured the sky.').
card_multiverse_id('flame rift'/'NMS', '22290').

card_in_set('flint golem', 'NMS').
card_original_type('flint golem'/'NMS', 'Artifact Creature — Golem').
card_original_text('flint golem'/'NMS', 'Whenever Flint Golem becomes blocked, defending player puts the top three cards of his or her library into his or her graveyard.').
card_first_print('flint golem', 'NMS').
card_image_name('flint golem'/'NMS', 'flint golem').
card_uid('flint golem'/'NMS', 'NMS:Flint Golem:flint golem').
card_rarity('flint golem'/'NMS', 'Uncommon').
card_artist('flint golem'/'NMS', 'Lou Harrison').
card_number('flint golem'/'NMS', '130').
card_multiverse_id('flint golem'/'NMS', '22284').

card_in_set('flowstone armor', 'NMS').
card_original_type('flowstone armor'/'NMS', 'Artifact').
card_original_text('flowstone armor'/'NMS', 'You may choose not to untap Flowstone Armor during your untap step. \n{3}, {T}: Target creature gets +1/-1 as long as Flowstone Armor remains tapped.').
card_first_print('flowstone armor', 'NMS').
card_image_name('flowstone armor'/'NMS', 'flowstone armor').
card_uid('flowstone armor'/'NMS', 'NMS:Flowstone Armor:flowstone armor').
card_rarity('flowstone armor'/'NMS', 'Uncommon').
card_artist('flowstone armor'/'NMS', 'Paolo Parente').
card_number('flowstone armor'/'NMS', '131').
card_multiverse_id('flowstone armor'/'NMS', '21391').

card_in_set('flowstone crusher', 'NMS').
card_original_type('flowstone crusher'/'NMS', 'Creature — Beast').
card_original_text('flowstone crusher'/'NMS', '{R} Flowstone Crusher gets +1/-1 until end of turn.').
card_first_print('flowstone crusher', 'NMS').
card_image_name('flowstone crusher'/'NMS', 'flowstone crusher').
card_uid('flowstone crusher'/'NMS', 'NMS:Flowstone Crusher:flowstone crusher').
card_rarity('flowstone crusher'/'NMS', 'Common').
card_artist('flowstone crusher'/'NMS', 'Ben Thompson').
card_number('flowstone crusher'/'NMS', '81').
card_flavor_text('flowstone crusher'/'NMS', '\"Someone must have done something very bad to make a rock that angry.\"\n—Rebel scout').
card_multiverse_id('flowstone crusher'/'NMS', '21334').

card_in_set('flowstone overseer', 'NMS').
card_original_type('flowstone overseer'/'NMS', 'Creature — Beast').
card_original_text('flowstone overseer'/'NMS', '{R}{R} Target creature gets +1/-1 until end of turn.').
card_first_print('flowstone overseer', 'NMS').
card_image_name('flowstone overseer'/'NMS', 'flowstone overseer').
card_uid('flowstone overseer'/'NMS', 'NMS:Flowstone Overseer:flowstone overseer').
card_rarity('flowstone overseer'/'NMS', 'Rare').
card_artist('flowstone overseer'/'NMS', 'Andrew Goldhawk').
card_number('flowstone overseer'/'NMS', '82').
card_flavor_text('flowstone overseer'/'NMS', 'The rebels couldn\'t see where the roar was coming from. Then they saw it was coming from everywhere.').
card_multiverse_id('flowstone overseer'/'NMS', '21351').

card_in_set('flowstone slide', 'NMS').
card_original_type('flowstone slide'/'NMS', 'Sorcery').
card_original_text('flowstone slide'/'NMS', 'All creatures get +X/-X until end of turn.').
card_first_print('flowstone slide', 'NMS').
card_image_name('flowstone slide'/'NMS', 'flowstone slide').
card_uid('flowstone slide'/'NMS', 'NMS:Flowstone Slide:flowstone slide').
card_rarity('flowstone slide'/'NMS', 'Rare').
card_artist('flowstone slide'/'NMS', 'Chippy').
card_number('flowstone slide'/'NMS', '83').
card_flavor_text('flowstone slide'/'NMS', 'It may look like soil, but its nature is malice.').
card_multiverse_id('flowstone slide'/'NMS', '21355').

card_in_set('flowstone strike', 'NMS').
card_original_type('flowstone strike'/'NMS', 'Instant').
card_original_text('flowstone strike'/'NMS', 'Target creature gets +1/-1 and gains haste until end of turn. (It may attack and {T} the turn it comes under your control.)').
card_first_print('flowstone strike', 'NMS').
card_image_name('flowstone strike'/'NMS', 'flowstone strike').
card_uid('flowstone strike'/'NMS', 'NMS:Flowstone Strike:flowstone strike').
card_rarity('flowstone strike'/'NMS', 'Common').
card_artist('flowstone strike'/'NMS', 'Mike Ploog').
card_number('flowstone strike'/'NMS', '84').
card_flavor_text('flowstone strike'/'NMS', 'Aggression exacts its toll.').
card_multiverse_id('flowstone strike'/'NMS', '21341').

card_in_set('flowstone surge', 'NMS').
card_original_type('flowstone surge'/'NMS', 'Enchantment').
card_original_text('flowstone surge'/'NMS', 'Creatures you control get +1/-1.').
card_first_print('flowstone surge', 'NMS').
card_image_name('flowstone surge'/'NMS', 'flowstone surge').
card_uid('flowstone surge'/'NMS', 'NMS:Flowstone Surge:flowstone surge').
card_rarity('flowstone surge'/'NMS', 'Uncommon').
card_artist('flowstone surge'/'NMS', 'Scott Hampton').
card_number('flowstone surge'/'NMS', '85').
card_flavor_text('flowstone surge'/'NMS', 'The army was never at a loss for weapons; Crovax was their forge.').
card_multiverse_id('flowstone surge'/'NMS', '21348').

card_in_set('flowstone thopter', 'NMS').
card_original_type('flowstone thopter'/'NMS', 'Artifact Creature').
card_original_text('flowstone thopter'/'NMS', '{1}: Flowstone Thopter gets +1/-1 and gains flying until end of turn.').
card_first_print('flowstone thopter', 'NMS').
card_image_name('flowstone thopter'/'NMS', 'flowstone thopter').
card_uid('flowstone thopter'/'NMS', 'NMS:Flowstone Thopter:flowstone thopter').
card_rarity('flowstone thopter'/'NMS', 'Uncommon').
card_artist('flowstone thopter'/'NMS', 'Mike Ploog').
card_number('flowstone thopter'/'NMS', '132').
card_flavor_text('flowstone thopter'/'NMS', 'Airborne flowstone isn\'t less dangerous, only less common.').
card_multiverse_id('flowstone thopter'/'NMS', '21398').

card_in_set('flowstone wall', 'NMS').
card_original_type('flowstone wall'/'NMS', 'Creature — Wall').
card_original_text('flowstone wall'/'NMS', '(Walls can\'t attack.)\n{R} Flowstone Wall gets +1/-1 until end of turn.').
card_first_print('flowstone wall', 'NMS').
card_image_name('flowstone wall'/'NMS', 'flowstone wall').
card_uid('flowstone wall'/'NMS', 'NMS:Flowstone Wall:flowstone wall').
card_rarity('flowstone wall'/'NMS', 'Common').
card_artist('flowstone wall'/'NMS', 'Jeff Miracola').
card_number('flowstone wall'/'NMS', '86').
card_flavor_text('flowstone wall'/'NMS', 'The rebels assaulted the wall, never suspecting the wall would assault back.').
card_multiverse_id('flowstone wall'/'NMS', '21336').

card_in_set('fog patch', 'NMS').
card_original_type('fog patch'/'NMS', 'Instant').
card_original_text('fog patch'/'NMS', 'Play Fog Patch only during the declare blockers step.\nAttacking creatures become blocked. (This spell works on unblockable creatures.)').
card_first_print('fog patch', 'NMS').
card_image_name('fog patch'/'NMS', 'fog patch').
card_uid('fog patch'/'NMS', 'NMS:Fog Patch:fog patch').
card_rarity('fog patch'/'NMS', 'Common').
card_artist('fog patch'/'NMS', 'Rebecca Guay').
card_number('fog patch'/'NMS', '104').
card_flavor_text('fog patch'/'NMS', 'Realizing they weren\'t getting past the fog, the elves did the only thing they could do: wait.').
card_multiverse_id('fog patch'/'NMS', '21368').

card_in_set('harvest mage', 'NMS').
card_original_type('harvest mage'/'NMS', 'Creature — Spellshaper').
card_original_text('harvest mage'/'NMS', '{G}, {T}, Discard a card from your hand: Until end of turn, if you tap a land for mana, it produces one mana of any color instead of its normal type and amount.').
card_first_print('harvest mage', 'NMS').
card_image_name('harvest mage'/'NMS', 'harvest mage').
card_uid('harvest mage'/'NMS', 'NMS:Harvest Mage:harvest mage').
card_rarity('harvest mage'/'NMS', 'Common').
card_artist('harvest mage'/'NMS', 'Dan Frazier').
card_number('harvest mage'/'NMS', '105').
card_flavor_text('harvest mage'/'NMS', '\"Why should one limit oneself to the resources at hand?\"').
card_multiverse_id('harvest mage'/'NMS', '21370').

card_in_set('infiltrate', 'NMS').
card_original_type('infiltrate'/'NMS', 'Instant').
card_original_text('infiltrate'/'NMS', 'Target creature is unblockable this turn.').
card_first_print('infiltrate', 'NMS').
card_image_name('infiltrate'/'NMS', 'infiltrate').
card_uid('infiltrate'/'NMS', 'NMS:Infiltrate:infiltrate').
card_rarity('infiltrate'/'NMS', 'Common').
card_artist('infiltrate'/'NMS', 'Nelson DeCastro').
card_number('infiltrate'/'NMS', '33').
card_flavor_text('infiltrate'/'NMS', 'Getting in was the easy part. Getting out proved to be more difficult.').
card_multiverse_id('infiltrate'/'NMS', '21286').

card_in_set('jolting merfolk', 'NMS').
card_original_type('jolting merfolk'/'NMS', 'Creature — Merfolk').
card_original_text('jolting merfolk'/'NMS', 'Fading 4 (This creature comes into play with four fade counters on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)\nRemove a fade counter from Jolting Merfolk: Tap target creature.').
card_first_print('jolting merfolk', 'NMS').
card_image_name('jolting merfolk'/'NMS', 'jolting merfolk').
card_uid('jolting merfolk'/'NMS', 'NMS:Jolting Merfolk:jolting merfolk').
card_rarity('jolting merfolk'/'NMS', 'Uncommon').
card_artist('jolting merfolk'/'NMS', 'Glen Angus').
card_number('jolting merfolk'/'NMS', '34').
card_multiverse_id('jolting merfolk'/'NMS', '21281').

card_in_set('kill switch', 'NMS').
card_original_type('kill switch'/'NMS', 'Artifact').
card_original_text('kill switch'/'NMS', '{2}, {T}: Tap all other artifacts. They don\'t untap during their controllers\' untap steps as long as Kill Switch remains tapped.').
card_first_print('kill switch', 'NMS').
card_image_name('kill switch'/'NMS', 'kill switch').
card_uid('kill switch'/'NMS', 'NMS:Kill Switch:kill switch').
card_rarity('kill switch'/'NMS', 'Rare').
card_artist('kill switch'/'NMS', 'Brian Snõddy').
card_number('kill switch'/'NMS', '133').
card_flavor_text('kill switch'/'NMS', 'Success is largely a matter of knowing which lever to pull.').
card_multiverse_id('kill switch'/'NMS', '22318').

card_in_set('kor haven', 'NMS').
card_original_type('kor haven'/'NMS', 'Legendary Land').
card_original_text('kor haven'/'NMS', '{T}: Add one colorless mana to your mana pool.\n{1}{W}, {T}: Prevent all combat damage that would be dealt by target attacking creature this turn.').
card_first_print('kor haven', 'NMS').
card_image_name('kor haven'/'NMS', 'kor haven').
card_uid('kor haven'/'NMS', 'NMS:Kor Haven:kor haven').
card_rarity('kor haven'/'NMS', 'Rare').
card_artist('kor haven'/'NMS', 'Darrell Riche').
card_number('kor haven'/'NMS', '141').
card_multiverse_id('kor haven'/'NMS', '21403').

card_in_set('laccolith grunt', 'NMS').
card_original_type('laccolith grunt'/'NMS', 'Creature — Beast').
card_original_text('laccolith grunt'/'NMS', 'Whenever Laccolith Grunt becomes blocked, you may have it deal damage equal to its power to target creature. If you do, Laccolith Grunt deals no combat damage this turn.').
card_first_print('laccolith grunt', 'NMS').
card_image_name('laccolith grunt'/'NMS', 'laccolith grunt').
card_uid('laccolith grunt'/'NMS', 'NMS:Laccolith Grunt:laccolith grunt').
card_rarity('laccolith grunt'/'NMS', 'Common').
card_artist('laccolith grunt'/'NMS', 'Arnie Swekel').
card_number('laccolith grunt'/'NMS', '87').
card_flavor_text('laccolith grunt'/'NMS', 'They leave a trail of ash and sorrow.').
card_multiverse_id('laccolith grunt'/'NMS', '21333').

card_in_set('laccolith rig', 'NMS').
card_original_type('laccolith rig'/'NMS', 'Enchant Creature').
card_original_text('laccolith rig'/'NMS', 'Whenever enchanted creature becomes blocked, you may have it deal damage equal to its power to target creature. If you do, enchanted creature deals no combat damage this turn.').
card_first_print('laccolith rig', 'NMS').
card_image_name('laccolith rig'/'NMS', 'laccolith rig').
card_uid('laccolith rig'/'NMS', 'NMS:Laccolith Rig:laccolith rig').
card_rarity('laccolith rig'/'NMS', 'Common').
card_artist('laccolith rig'/'NMS', 'Massimilano Frezzato').
card_number('laccolith rig'/'NMS', '88').
card_multiverse_id('laccolith rig'/'NMS', '21339').

card_in_set('laccolith titan', 'NMS').
card_original_type('laccolith titan'/'NMS', 'Creature — Beast').
card_original_text('laccolith titan'/'NMS', 'Whenever Laccolith Titan becomes blocked, you may have it deal damage equal to its power to target creature. If you do, Laccolith Titan deals no combat damage this turn.').
card_first_print('laccolith titan', 'NMS').
card_image_name('laccolith titan'/'NMS', 'laccolith titan').
card_uid('laccolith titan'/'NMS', 'NMS:Laccolith Titan:laccolith titan').
card_rarity('laccolith titan'/'NMS', 'Rare').
card_artist('laccolith titan'/'NMS', 'Tony Szczudlo').
card_number('laccolith titan'/'NMS', '89').
card_flavor_text('laccolith titan'/'NMS', 'Proximity guarantees injury or death.').
card_multiverse_id('laccolith titan'/'NMS', '22297').

card_in_set('laccolith warrior', 'NMS').
card_original_type('laccolith warrior'/'NMS', 'Creature — Beast').
card_original_text('laccolith warrior'/'NMS', 'Whenever Laccolith Warrior becomes blocked, you may have it deal damage equal to its power to target creature. If you do, Laccolith Warrior deals no combat damage this turn.').
card_first_print('laccolith warrior', 'NMS').
card_image_name('laccolith warrior'/'NMS', 'laccolith warrior').
card_uid('laccolith warrior'/'NMS', 'NMS:Laccolith Warrior:laccolith warrior').
card_rarity('laccolith warrior'/'NMS', 'Uncommon').
card_artist('laccolith warrior'/'NMS', 'Mark Zug').
card_number('laccolith warrior'/'NMS', '90').
card_flavor_text('laccolith warrior'/'NMS', 'The screams are brief. The silence lingers.').
card_multiverse_id('laccolith warrior'/'NMS', '21344').

card_in_set('laccolith whelp', 'NMS').
card_original_type('laccolith whelp'/'NMS', 'Creature — Beast').
card_original_text('laccolith whelp'/'NMS', 'Whenever Laccolith Whelp becomes blocked, you may have it deal damage equal to its power to target creature. If you do, Laccolith Whelp deals no combat damage this turn.').
card_first_print('laccolith whelp', 'NMS').
card_image_name('laccolith whelp'/'NMS', 'laccolith whelp').
card_uid('laccolith whelp'/'NMS', 'NMS:Laccolith Whelp:laccolith whelp').
card_rarity('laccolith whelp'/'NMS', 'Common').
card_artist('laccolith whelp'/'NMS', 'Dave Dorman').
card_number('laccolith whelp'/'NMS', '91').
card_flavor_text('laccolith whelp'/'NMS', 'They don\'t feed; they stoke.').
card_multiverse_id('laccolith whelp'/'NMS', '21335').

card_in_set('lashknife', 'NMS').
card_original_type('lashknife'/'NMS', 'Enchant Creature').
card_original_text('lashknife'/'NMS', 'If you control a plains, you may tap an untapped creature you control instead of paying Lashknife\'s mana cost.\nEnchanted creature has first strike.').
card_first_print('lashknife', 'NMS').
card_image_name('lashknife'/'NMS', 'lashknife').
card_uid('lashknife'/'NMS', 'NMS:Lashknife:lashknife').
card_rarity('lashknife'/'NMS', 'Common').
card_artist('lashknife'/'NMS', 'Hannibal King').
card_number('lashknife'/'NMS', '9').
card_flavor_text('lashknife'/'NMS', 'Those it won\'t scare, it scars.').
card_multiverse_id('lashknife'/'NMS', '21257').

card_in_set('lawbringer', 'NMS').
card_original_type('lawbringer'/'NMS', 'Creature — Rebel').
card_original_text('lawbringer'/'NMS', '{T}, Sacrifice Lawbringer: Remove target red creature from the game.').
card_first_print('lawbringer', 'NMS').
card_image_name('lawbringer'/'NMS', 'lawbringer').
card_uid('lawbringer'/'NMS', 'NMS:Lawbringer:lawbringer').
card_rarity('lawbringer'/'NMS', 'Common').
card_artist('lawbringer'/'NMS', 'Matt Cavotta').
card_number('lawbringer'/'NMS', '10').
card_flavor_text('lawbringer'/'NMS', 'One injustice starts a thousand riots. One law stops a thousand crimes.\n—Lawbringer creed').
card_multiverse_id('lawbringer'/'NMS', '21255').

card_in_set('lightbringer', 'NMS').
card_original_type('lightbringer'/'NMS', 'Creature — Rebel').
card_original_text('lightbringer'/'NMS', '{T}, Sacrifice Lightbringer: Remove target black creature from the game.').
card_first_print('lightbringer', 'NMS').
card_image_name('lightbringer'/'NMS', 'lightbringer').
card_uid('lightbringer'/'NMS', 'NMS:Lightbringer:lightbringer').
card_rarity('lightbringer'/'NMS', 'Common').
card_artist('lightbringer'/'NMS', 'Paolo Parente').
card_number('lightbringer'/'NMS', '11').
card_flavor_text('lightbringer'/'NMS', 'One thought opens a thousand eyes. One sun brings a thousand dawns.\n—Lightbringer creed').
card_multiverse_id('lightbringer'/'NMS', '21256').

card_in_set('lin sivvi, defiant hero', 'NMS').
card_original_type('lin sivvi, defiant hero'/'NMS', 'Creature — Rebel Legend').
card_original_text('lin sivvi, defiant hero'/'NMS', '{X}, {T}: Search your library for a Rebel card with converted mana cost X or less and put that card into play. Then shuffle your library.\n{3}: Put target Rebel card from your graveyard on the bottom of your library.').
card_first_print('lin sivvi, defiant hero', 'NMS').
card_image_name('lin sivvi, defiant hero'/'NMS', 'lin sivvi, defiant hero').
card_uid('lin sivvi, defiant hero'/'NMS', 'NMS:Lin Sivvi, Defiant Hero:lin sivvi, defiant hero').
card_rarity('lin sivvi, defiant hero'/'NMS', 'Rare').
card_artist('lin sivvi, defiant hero'/'NMS', 'rk post').
card_number('lin sivvi, defiant hero'/'NMS', '12').
card_multiverse_id('lin sivvi, defiant hero'/'NMS', '21271').

card_in_set('mana cache', 'NMS').
card_original_type('mana cache'/'NMS', 'Enchantment').
card_original_text('mana cache'/'NMS', 'At the end of each player\'s turn, put a charge counter on Mana Cache for each untapped land that player controls.\nRemove a charge counter from Mana Cache: Add one colorless mana to your mana pool. Any player may play this ability but only during his or her turn before the end phase.').
card_first_print('mana cache', 'NMS').
card_image_name('mana cache'/'NMS', 'mana cache').
card_uid('mana cache'/'NMS', 'NMS:Mana Cache:mana cache').
card_rarity('mana cache'/'NMS', 'Rare').
card_artist('mana cache'/'NMS', 'rk post').
card_number('mana cache'/'NMS', '92').
card_multiverse_id('mana cache'/'NMS', '21356').

card_in_set('massacre', 'NMS').
card_original_type('massacre'/'NMS', 'Sorcery').
card_original_text('massacre'/'NMS', 'If an opponent controls a plains and you control a swamp, you may play Massacre without paying its mana cost.\nAll creatures get -2/-2 until end of turn.').
card_first_print('massacre', 'NMS').
card_image_name('massacre'/'NMS', 'massacre').
card_uid('massacre'/'NMS', 'NMS:Massacre:massacre').
card_rarity('massacre'/'NMS', 'Uncommon').
card_artist('massacre'/'NMS', 'Pete Venters').
card_number('massacre'/'NMS', '58').
card_multiverse_id('massacre'/'NMS', '21324').

card_in_set('mind slash', 'NMS').
card_original_type('mind slash'/'NMS', 'Enchantment').
card_original_text('mind slash'/'NMS', '{B}, Sacrifice a creature: Look at target opponent\'s hand and choose a card from it. That player discards that card. Play this ability only if you could play a sorcery.').
card_first_print('mind slash', 'NMS').
card_image_name('mind slash'/'NMS', 'mind slash').
card_uid('mind slash'/'NMS', 'NMS:Mind Slash:mind slash').
card_rarity('mind slash'/'NMS', 'Uncommon').
card_artist('mind slash'/'NMS', 'Adam Rex').
card_number('mind slash'/'NMS', '59').
card_flavor_text('mind slash'/'NMS', '\"I can\'t think with all that screaming.\"').
card_multiverse_id('mind slash'/'NMS', '21331').

card_in_set('mind swords', 'NMS').
card_original_type('mind swords'/'NMS', 'Sorcery').
card_original_text('mind swords'/'NMS', 'If you control a swamp, you may sacrifice a creature instead of paying Mind Swords\'s mana cost.\nEach player removes two cards in his or her hand from the game.').
card_first_print('mind swords', 'NMS').
card_image_name('mind swords'/'NMS', 'mind swords').
card_uid('mind swords'/'NMS', 'NMS:Mind Swords:mind swords').
card_rarity('mind swords'/'NMS', 'Common').
card_artist('mind swords'/'NMS', 'Daren Bader').
card_number('mind swords'/'NMS', '60').
card_multiverse_id('mind swords'/'NMS', '21313').

card_in_set('mogg alarm', 'NMS').
card_original_type('mogg alarm'/'NMS', 'Sorcery').
card_original_text('mogg alarm'/'NMS', 'You may sacrifice two mountains instead of paying Mogg Alarm\'s mana cost. \nPut two 1/1 red Goblin creature tokens into play.').
card_first_print('mogg alarm', 'NMS').
card_image_name('mogg alarm'/'NMS', 'mogg alarm').
card_uid('mogg alarm'/'NMS', 'NMS:Mogg Alarm:mogg alarm').
card_rarity('mogg alarm'/'NMS', 'Uncommon').
card_artist('mogg alarm'/'NMS', 'Dave Dorman').
card_number('mogg alarm'/'NMS', '93').
card_flavor_text('mogg alarm'/'NMS', 'They make mountains into mogg holes.').
card_multiverse_id('mogg alarm'/'NMS', '21345').

card_in_set('mogg salvage', 'NMS').
card_original_type('mogg salvage'/'NMS', 'Instant').
card_original_text('mogg salvage'/'NMS', 'If an opponent controls an island and you control a mountain, you may play Mogg Salvage without paying its mana cost.\nDestroy target artifact.').
card_first_print('mogg salvage', 'NMS').
card_image_name('mogg salvage'/'NMS', 'mogg salvage').
card_uid('mogg salvage'/'NMS', 'NMS:Mogg Salvage:mogg salvage').
card_rarity('mogg salvage'/'NMS', 'Uncommon').
card_artist('mogg salvage'/'NMS', 'Paolo Parente').
card_number('mogg salvage'/'NMS', '94').
card_flavor_text('mogg salvage'/'NMS', 'Three moggs, one treasure, too bad.').
card_multiverse_id('mogg salvage'/'NMS', '21349').

card_in_set('mogg toady', 'NMS').
card_original_type('mogg toady'/'NMS', 'Creature — Goblin').
card_original_text('mogg toady'/'NMS', 'Mogg Toady can\'t attack unless you control more creatures than defending player.\nMogg Toady can\'t block unless you control more creatures than attacking player.').
card_first_print('mogg toady', 'NMS').
card_image_name('mogg toady'/'NMS', 'mogg toady').
card_uid('mogg toady'/'NMS', 'NMS:Mogg Toady:mogg toady').
card_rarity('mogg toady'/'NMS', 'Common').
card_artist('mogg toady'/'NMS', 'Mike Ploog').
card_number('mogg toady'/'NMS', '95').
card_multiverse_id('mogg toady'/'NMS', '22299').

card_in_set('moggcatcher', 'NMS').
card_original_type('moggcatcher'/'NMS', 'Creature — Mercenary').
card_original_text('moggcatcher'/'NMS', '{3}, {T}: Search your library for a Goblin card and put that card into play. Then shuffle your library.').
card_first_print('moggcatcher', 'NMS').
card_image_name('moggcatcher'/'NMS', 'moggcatcher').
card_uid('moggcatcher'/'NMS', 'NMS:Moggcatcher:moggcatcher').
card_rarity('moggcatcher'/'NMS', 'Rare').
card_artist('moggcatcher'/'NMS', 'Pete Venters').
card_number('moggcatcher'/'NMS', '96').
card_flavor_text('moggcatcher'/'NMS', '\"They\'re not worth much, but they\'re easy to catch.\"').
card_multiverse_id('moggcatcher'/'NMS', '21350').

card_in_set('mossdog', 'NMS').
card_original_type('mossdog'/'NMS', 'Creature — Hound').
card_original_text('mossdog'/'NMS', 'Whenever Mossdog becomes the target of a spell or ability an opponent controls, put a +1/+1 counter on Mossdog.').
card_first_print('mossdog', 'NMS').
card_image_name('mossdog'/'NMS', 'mossdog').
card_uid('mossdog'/'NMS', 'NMS:Mossdog:mossdog').
card_rarity('mossdog'/'NMS', 'Common').
card_artist('mossdog'/'NMS', 'Matt Cavotta').
card_number('mossdog'/'NMS', '106').
card_flavor_text('mossdog'/'NMS', 'The more you look at it, the more dangerous it becomes.').
card_multiverse_id('mossdog'/'NMS', '21290').

card_in_set('murderous betrayal', 'NMS').
card_original_type('murderous betrayal'/'NMS', 'Enchantment').
card_original_text('murderous betrayal'/'NMS', '{B}{B}, Pay half your life rounded up: Destroy target nonblack creature. It can\'t be regenerated.').
card_first_print('murderous betrayal', 'NMS').
card_image_name('murderous betrayal'/'NMS', 'murderous betrayal').
card_uid('murderous betrayal'/'NMS', 'NMS:Murderous Betrayal:murderous betrayal').
card_rarity('murderous betrayal'/'NMS', 'Rare').
card_artist('murderous betrayal'/'NMS', 'Randy Gallegos').
card_number('murderous betrayal'/'NMS', '61').
card_flavor_text('murderous betrayal'/'NMS', 'In darkness, the assassin sneaked into the village and destroyed what Eladamri loved most: his daughter.').
card_multiverse_id('murderous betrayal'/'NMS', '21329').

card_in_set('nesting wurm', 'NMS').
card_original_type('nesting wurm'/'NMS', 'Creature — Wurm').
card_original_text('nesting wurm'/'NMS', 'Trample\nWhen Nesting Wurm comes into play, you may search your library for up to three Nesting Wurm cards, reveal them, and put them into your hand. If you do, shuffle your library.').
card_first_print('nesting wurm', 'NMS').
card_image_name('nesting wurm'/'NMS', 'nesting wurm').
card_uid('nesting wurm'/'NMS', 'NMS:Nesting Wurm:nesting wurm').
card_rarity('nesting wurm'/'NMS', 'Uncommon').
card_artist('nesting wurm'/'NMS', 'rk post').
card_number('nesting wurm'/'NMS', '107').
card_multiverse_id('nesting wurm'/'NMS', '21373').

card_in_set('netter en-dal', 'NMS').
card_original_type('netter en-dal'/'NMS', 'Creature — Spellshaper').
card_original_text('netter en-dal'/'NMS', '{W}, {T}, Discard a card from your hand: Target creature can\'t attack this turn.').
card_first_print('netter en-dal', 'NMS').
card_image_name('netter en-dal'/'NMS', 'netter en-dal').
card_uid('netter en-dal'/'NMS', 'NMS:Netter en-Dal:netter en-dal').
card_rarity('netter en-dal'/'NMS', 'Common').
card_artist('netter en-dal'/'NMS', 'Matt Cavotta').
card_number('netter en-dal'/'NMS', '13').
card_flavor_text('netter en-dal'/'NMS', 'The wizard\'s vocabulary of mogg profanities increased with every net she cast.').
card_multiverse_id('netter en-dal'/'NMS', '22027').

card_in_set('noble stand', 'NMS').
card_original_type('noble stand'/'NMS', 'Enchantment').
card_original_text('noble stand'/'NMS', 'Whenever a creature you control blocks, you gain 2 life.').
card_first_print('noble stand', 'NMS').
card_image_name('noble stand'/'NMS', 'noble stand').
card_uid('noble stand'/'NMS', 'NMS:Noble Stand:noble stand').
card_rarity('noble stand'/'NMS', 'Uncommon').
card_artist('noble stand'/'NMS', 'Greg & Tim Hildebrandt').
card_number('noble stand'/'NMS', '14').
card_flavor_text('noble stand'/'NMS', 'Soldiers will always endanger their lives to defend many other lives.').
card_multiverse_id('noble stand'/'NMS', '21262').

card_in_set('off balance', 'NMS').
card_original_type('off balance'/'NMS', 'Instant').
card_original_text('off balance'/'NMS', 'Target creature can\'t attack or block this turn.').
card_first_print('off balance', 'NMS').
card_image_name('off balance'/'NMS', 'off balance').
card_uid('off balance'/'NMS', 'NMS:Off Balance:off balance').
card_rarity('off balance'/'NMS', 'Common').
card_artist('off balance'/'NMS', 'Jeff Miracola').
card_number('off balance'/'NMS', '15').
card_flavor_text('off balance'/'NMS', 'A heavy weapon deals plenty of damage—but it does have to hit.').
card_multiverse_id('off balance'/'NMS', '22032').

card_in_set('oracle\'s attendants', 'NMS').
card_original_type('oracle\'s attendants'/'NMS', 'Creature — Soldier').
card_original_text('oracle\'s attendants'/'NMS', '{T}: All damage that would be dealt to target creature this turn by a source of your choice is dealt to Oracle\'s Attendants instead.').
card_first_print('oracle\'s attendants', 'NMS').
card_image_name('oracle\'s attendants'/'NMS', 'oracle\'s attendants').
card_uid('oracle\'s attendants'/'NMS', 'NMS:Oracle\'s Attendants:oracle\'s attendants').
card_rarity('oracle\'s attendants'/'NMS', 'Rare').
card_artist('oracle\'s attendants'/'NMS', 'Orizio Daniele').
card_number('oracle\'s attendants'/'NMS', '16').
card_flavor_text('oracle\'s attendants'/'NMS', 'The future isn\'t sacred, but its speaker is.').
card_multiverse_id('oracle\'s attendants'/'NMS', '22030').

card_in_set('oraxid', 'NMS').
card_original_type('oraxid'/'NMS', 'Creature — Beast').
card_original_text('oraxid'/'NMS', 'Protection from red').
card_first_print('oraxid', 'NMS').
card_image_name('oraxid'/'NMS', 'oraxid').
card_uid('oraxid'/'NMS', 'NMS:Oraxid:oraxid').
card_rarity('oraxid'/'NMS', 'Common').
card_artist('oraxid'/'NMS', 'Dave Dorman').
card_number('oraxid'/'NMS', '35').
card_flavor_text('oraxid'/'NMS', '\"I\'m constantly amazed by the tenacity of living creatures on this plane. Even in the stronghold\'s white-hot core, life persists.\"\n—Ertai').
card_multiverse_id('oraxid'/'NMS', '22293').

card_in_set('overlaid terrain', 'NMS').
card_original_type('overlaid terrain'/'NMS', 'Enchantment').
card_original_text('overlaid terrain'/'NMS', 'As Overlaid Terrain comes into play, sacrifice all lands you control.\nLands you control have \"{T}: Add two mana of any one color to your mana pool.\"').
card_first_print('overlaid terrain', 'NMS').
card_image_name('overlaid terrain'/'NMS', 'overlaid terrain').
card_uid('overlaid terrain'/'NMS', 'NMS:Overlaid Terrain:overlaid terrain').
card_rarity('overlaid terrain'/'NMS', 'Rare').
card_artist('overlaid terrain'/'NMS', 'DiTerlizzi').
card_number('overlaid terrain'/'NMS', '108').
card_flavor_text('overlaid terrain'/'NMS', 'One must reap before one can sow.').
card_multiverse_id('overlaid terrain'/'NMS', '22061').

card_in_set('pack hunt', 'NMS').
card_original_type('pack hunt'/'NMS', 'Sorcery').
card_original_text('pack hunt'/'NMS', 'Search your library for up to three copies of target creature, reveal them, and put them into your hand. Then shuffle your library.').
card_first_print('pack hunt', 'NMS').
card_image_name('pack hunt'/'NMS', 'pack hunt').
card_uid('pack hunt'/'NMS', 'NMS:Pack Hunt:pack hunt').
card_rarity('pack hunt'/'NMS', 'Rare').
card_artist('pack hunt'/'NMS', 'Sam Wood').
card_number('pack hunt'/'NMS', '109').
card_flavor_text('pack hunt'/'NMS', 'The one you see is just a diversion.').
card_multiverse_id('pack hunt'/'NMS', '21375').

card_in_set('pale moon', 'NMS').
card_original_type('pale moon'/'NMS', 'Instant').
card_original_text('pale moon'/'NMS', 'Until end of turn, if a player taps a nonbasic land for mana, it produces colorless mana instead of its normal type.').
card_first_print('pale moon', 'NMS').
card_image_name('pale moon'/'NMS', 'pale moon').
card_uid('pale moon'/'NMS', 'NMS:Pale Moon:pale moon').
card_rarity('pale moon'/'NMS', 'Rare').
card_artist('pale moon'/'NMS', 'Pete Venters').
card_number('pale moon'/'NMS', '36').
card_flavor_text('pale moon'/'NMS', 'Denizens of Rath had never seen the moon before. It was a symbol of change, not constancy.').
card_multiverse_id('pale moon'/'NMS', '21302').

card_in_set('parallax dementia', 'NMS').
card_original_type('parallax dementia'/'NMS', 'Enchant Creature').
card_original_text('parallax dementia'/'NMS', 'Fading 1 (This enchantment comes into play with one fade counter on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)\nEnchanted creature gets +3/+2.\nWhen Parallax Dementia leaves play, destroy enchanted creature. That creature can\'t be regenerated.').
card_first_print('parallax dementia', 'NMS').
card_image_name('parallax dementia'/'NMS', 'parallax dementia').
card_uid('parallax dementia'/'NMS', 'NMS:Parallax Dementia:parallax dementia').
card_rarity('parallax dementia'/'NMS', 'Common').
card_artist('parallax dementia'/'NMS', 'Eric Peterson').
card_number('parallax dementia'/'NMS', '62').
card_multiverse_id('parallax dementia'/'NMS', '21315').

card_in_set('parallax inhibitor', 'NMS').
card_original_type('parallax inhibitor'/'NMS', 'Artifact').
card_original_text('parallax inhibitor'/'NMS', '{1}, {T}, Sacrifice Parallax Inhibitor: Put a fade counter on each permanent with fading you control.').
card_first_print('parallax inhibitor', 'NMS').
card_image_name('parallax inhibitor'/'NMS', 'parallax inhibitor').
card_uid('parallax inhibitor'/'NMS', 'NMS:Parallax Inhibitor:parallax inhibitor').
card_rarity('parallax inhibitor'/'NMS', 'Rare').
card_artist('parallax inhibitor'/'NMS', 'Greg Staples').
card_number('parallax inhibitor'/'NMS', '134').
card_flavor_text('parallax inhibitor'/'NMS', '\"The best I can offer is a temporary reprieve.\"\n—Belbe').
card_multiverse_id('parallax inhibitor'/'NMS', '22301').

card_in_set('parallax nexus', 'NMS').
card_original_type('parallax nexus'/'NMS', 'Enchantment').
card_original_text('parallax nexus'/'NMS', 'Fading 5\nRemove a fade counter from Parallax Nexus: Target opponent removes a card in his or her hand from the game. Play this ability only if you could play a sorcery.\nWhen Parallax Nexus leaves play, each player returns to his or her hand all cards he or she owns removed from the game with Parallax Nexus.').
card_first_print('parallax nexus', 'NMS').
card_image_name('parallax nexus'/'NMS', 'parallax nexus').
card_uid('parallax nexus'/'NMS', 'NMS:Parallax Nexus:parallax nexus').
card_rarity('parallax nexus'/'NMS', 'Rare').
card_artist('parallax nexus'/'NMS', 'Greg Staples').
card_number('parallax nexus'/'NMS', '63').
card_multiverse_id('parallax nexus'/'NMS', '22031').

card_in_set('parallax tide', 'NMS').
card_original_type('parallax tide'/'NMS', 'Enchantment').
card_original_text('parallax tide'/'NMS', 'Fading 5\nRemove a fade counter from Parallax Tide: Remove target land from the game.\nWhen Parallax Tide leaves play, each player returns to play all cards he or she owns removed from the game with Parallax Tide.').
card_first_print('parallax tide', 'NMS').
card_image_name('parallax tide'/'NMS', 'parallax tide').
card_uid('parallax tide'/'NMS', 'NMS:Parallax Tide:parallax tide').
card_rarity('parallax tide'/'NMS', 'Rare').
card_artist('parallax tide'/'NMS', 'Carl Critchlow').
card_number('parallax tide'/'NMS', '37').
card_multiverse_id('parallax tide'/'NMS', '22035').

card_in_set('parallax wave', 'NMS').
card_original_type('parallax wave'/'NMS', 'Enchantment').
card_original_text('parallax wave'/'NMS', 'Fading 5\nRemove a fade counter from Parallax Wave: Remove target creature from the game.\nWhen Parallax Wave leaves play, each player returns to play all cards he or she owns removed from the game with Parallax Wave.').
card_first_print('parallax wave', 'NMS').
card_image_name('parallax wave'/'NMS', 'parallax wave').
card_uid('parallax wave'/'NMS', 'NMS:Parallax Wave:parallax wave').
card_rarity('parallax wave'/'NMS', 'Rare').
card_artist('parallax wave'/'NMS', 'Greg Staples').
card_number('parallax wave'/'NMS', '17').
card_multiverse_id('parallax wave'/'NMS', '22028').

card_in_set('phyrexian driver', 'NMS').
card_original_type('phyrexian driver'/'NMS', 'Creature — Mercenary').
card_original_text('phyrexian driver'/'NMS', 'When Phyrexian Driver comes into play, all other Mercenaries get +1/+1 until end of turn.').
card_first_print('phyrexian driver', 'NMS').
card_image_name('phyrexian driver'/'NMS', 'phyrexian driver').
card_uid('phyrexian driver'/'NMS', 'NMS:Phyrexian Driver:phyrexian driver').
card_rarity('phyrexian driver'/'NMS', 'Common').
card_artist('phyrexian driver'/'NMS', 'Chippy').
card_number('phyrexian driver'/'NMS', '64').
card_flavor_text('phyrexian driver'/'NMS', 'Although the abuse was initially effective, the troops soon became numb to it.').
card_multiverse_id('phyrexian driver'/'NMS', '22379').

card_in_set('phyrexian prowler', 'NMS').
card_original_type('phyrexian prowler'/'NMS', 'Creature — Mercenary').
card_original_text('phyrexian prowler'/'NMS', 'Fading 3 (This creature comes into play with three fade counters on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)\nRemove a fade counter from Phyrexian Prowler: Phyrexian Prowler gets +1/+1 until end of turn.').
card_first_print('phyrexian prowler', 'NMS').
card_image_name('phyrexian prowler'/'NMS', 'phyrexian prowler').
card_uid('phyrexian prowler'/'NMS', 'NMS:Phyrexian Prowler:phyrexian prowler').
card_rarity('phyrexian prowler'/'NMS', 'Uncommon').
card_artist('phyrexian prowler'/'NMS', 'Mark Zug').
card_number('phyrexian prowler'/'NMS', '65').
card_multiverse_id('phyrexian prowler'/'NMS', '22029').

card_in_set('plague witch', 'NMS').
card_original_type('plague witch'/'NMS', 'Creature — Spellshaper').
card_original_text('plague witch'/'NMS', '{B}, {T}, Discard a card from your hand: Target creature gets -1/-1 until end of turn.').
card_first_print('plague witch', 'NMS').
card_image_name('plague witch'/'NMS', 'plague witch').
card_uid('plague witch'/'NMS', 'NMS:Plague Witch:plague witch').
card_rarity('plague witch'/'NMS', 'Common').
card_artist('plague witch'/'NMS', 'Nelson DeCastro').
card_number('plague witch'/'NMS', '66').
card_flavor_text('plague witch'/'NMS', '\"I can ail what cures you.\"').
card_multiverse_id('plague witch'/'NMS', '19579').

card_in_set('predator, flagship', 'NMS').
card_original_type('predator, flagship'/'NMS', 'Legendary Artifact').
card_original_text('predator, flagship'/'NMS', '{2}: Target creature gains flying until end of turn.\n{5}, {T}: Destroy target creature with flying.').
card_first_print('predator, flagship', 'NMS').
card_image_name('predator, flagship'/'NMS', 'predator, flagship').
card_uid('predator, flagship'/'NMS', 'NMS:Predator, Flagship:predator, flagship').
card_rarity('predator, flagship'/'NMS', 'Rare').
card_artist('predator, flagship'/'NMS', 'Mark Tedin').
card_number('predator, flagship'/'NMS', '135').
card_flavor_text('predator, flagship'/'NMS', '\"The scourge of Skyshroud is airborne once more.\"\n—Oracle en-Vec').
card_multiverse_id('predator, flagship'/'NMS', '21400').

card_in_set('rackling', 'NMS').
card_original_type('rackling'/'NMS', 'Artifact Creature').
card_original_text('rackling'/'NMS', 'At the beginning of each opponent\'s upkeep, Rackling deals X damage to that player, where X is the number of cards in his or her hand fewer than three.').
card_first_print('rackling', 'NMS').
card_image_name('rackling'/'NMS', 'rackling').
card_uid('rackling'/'NMS', 'NMS:Rackling:rackling').
card_rarity('rackling'/'NMS', 'Uncommon').
card_artist('rackling'/'NMS', 'D. Alexander Gregory').
card_number('rackling'/'NMS', '136').
card_flavor_text('rackling'/'NMS', '\"This may hurt a little.\"').
card_multiverse_id('rackling'/'NMS', '21396').

card_in_set('rath\'s edge', 'NMS').
card_original_type('rath\'s edge'/'NMS', 'Legendary Land').
card_original_text('rath\'s edge'/'NMS', '{T}: Add one colorless mana to your mana pool.\n{4}, {T}, Sacrifice a land: Rath\'s Edge deals 1 damage to target creature or player.').
card_first_print('rath\'s edge', 'NMS').
card_image_name('rath\'s edge'/'NMS', 'rath\'s edge').
card_uid('rath\'s edge'/'NMS', 'NMS:Rath\'s Edge:rath\'s edge').
card_rarity('rath\'s edge'/'NMS', 'Rare').
card_artist('rath\'s edge'/'NMS', 'Ron Spencer').
card_number('rath\'s edge'/'NMS', '142').
card_multiverse_id('rath\'s edge'/'NMS', '21402').

card_in_set('rathi assassin', 'NMS').
card_original_type('rathi assassin'/'NMS', 'Creature — Mercenary').
card_original_text('rathi assassin'/'NMS', '{1}{B}{B}, {T}: Destroy target tapped nonblack creature.\n{3}, {T}: Search your library for a Mercenary card with converted mana cost 3 or less and put that card into play. Then shuffle your library.').
card_image_name('rathi assassin'/'NMS', 'rathi assassin').
card_uid('rathi assassin'/'NMS', 'NMS:Rathi Assassin:rathi assassin').
card_rarity('rathi assassin'/'NMS', 'Rare').
card_artist('rathi assassin'/'NMS', 'Dana Knutson').
card_number('rathi assassin'/'NMS', '67').
card_multiverse_id('rathi assassin'/'NMS', '22890').

card_in_set('rathi fiend', 'NMS').
card_original_type('rathi fiend'/'NMS', 'Creature — Mercenary').
card_original_text('rathi fiend'/'NMS', 'When Rathi Fiend comes into play, each player loses 3 life.\n{3}, {T}: Search your library for a Mercenary card with converted mana cost 3 or less and put that card into play. Then shuffle your library.').
card_first_print('rathi fiend', 'NMS').
card_image_name('rathi fiend'/'NMS', 'rathi fiend').
card_uid('rathi fiend'/'NMS', 'NMS:Rathi Fiend:rathi fiend').
card_rarity('rathi fiend'/'NMS', 'Uncommon').
card_artist('rathi fiend'/'NMS', 'Mark Tedin').
card_number('rathi fiend'/'NMS', '68').
card_multiverse_id('rathi fiend'/'NMS', '21311').

card_in_set('rathi intimidator', 'NMS').
card_original_type('rathi intimidator'/'NMS', 'Creature — Mercenary').
card_original_text('rathi intimidator'/'NMS', 'Rathi Intimidator can\'t be blocked except by artifact creatures and black creatures.\n{2}, {T}: Search your library for a Mercenary card with converted mana cost 2 or less and put that card into play. Then shuffle your library.').
card_first_print('rathi intimidator', 'NMS').
card_image_name('rathi intimidator'/'NMS', 'rathi intimidator').
card_uid('rathi intimidator'/'NMS', 'NMS:Rathi Intimidator:rathi intimidator').
card_rarity('rathi intimidator'/'NMS', 'Common').
card_artist('rathi intimidator'/'NMS', 'Mike Ploog').
card_number('rathi intimidator'/'NMS', '69').
card_multiverse_id('rathi intimidator'/'NMS', '21307').

card_in_set('refreshing rain', 'NMS').
card_original_type('refreshing rain'/'NMS', 'Instant').
card_original_text('refreshing rain'/'NMS', 'If an opponent controls a swamp and you control a forest, you may play Refreshing Rain without paying its mana cost.\nTarget player gains 6 life.').
card_first_print('refreshing rain', 'NMS').
card_image_name('refreshing rain'/'NMS', 'refreshing rain').
card_uid('refreshing rain'/'NMS', 'NMS:Refreshing Rain:refreshing rain').
card_rarity('refreshing rain'/'NMS', 'Uncommon').
card_artist('refreshing rain'/'NMS', 'Don Hazeltine').
card_number('refreshing rain'/'NMS', '110').
card_multiverse_id('refreshing rain'/'NMS', '21378').

card_in_set('rejuvenation chamber', 'NMS').
card_original_type('rejuvenation chamber'/'NMS', 'Artifact').
card_original_text('rejuvenation chamber'/'NMS', 'Fading 2 (This artifact comes into play with two fade counters on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)\n{T}: You gain 2 life.').
card_first_print('rejuvenation chamber', 'NMS').
card_image_name('rejuvenation chamber'/'NMS', 'rejuvenation chamber').
card_uid('rejuvenation chamber'/'NMS', 'NMS:Rejuvenation Chamber:rejuvenation chamber').
card_rarity('rejuvenation chamber'/'NMS', 'Uncommon').
card_artist('rejuvenation chamber'/'NMS', 'Alan Pollack').
card_number('rejuvenation chamber'/'NMS', '137').
card_multiverse_id('rejuvenation chamber'/'NMS', '21386').

card_in_set('reverent silence', 'NMS').
card_original_type('reverent silence'/'NMS', 'Sorcery').
card_original_text('reverent silence'/'NMS', 'If you control a forest, you may have each other player gain 6 life instead of paying Reverent Silence\'s mana cost.\nDestroy all enchantments.').
card_first_print('reverent silence', 'NMS').
card_image_name('reverent silence'/'NMS', 'reverent silence').
card_uid('reverent silence'/'NMS', 'NMS:Reverent Silence:reverent silence').
card_rarity('reverent silence'/'NMS', 'Common').
card_artist('reverent silence'/'NMS', 'Don Hazeltine').
card_number('reverent silence'/'NMS', '111').
card_multiverse_id('reverent silence'/'NMS', '22316').

card_in_set('rhox', 'NMS').
card_original_type('rhox'/'NMS', 'Creature — Beast').
card_original_text('rhox'/'NMS', 'You may have Rhox deal combat damage to defending player as though it weren\'t blocked.\n{2}{G}: Regenerate Rhox.').
card_first_print('rhox', 'NMS').
card_image_name('rhox'/'NMS', 'rhox').
card_uid('rhox'/'NMS', 'NMS:Rhox:rhox').
card_rarity('rhox'/'NMS', 'Rare').
card_artist('rhox'/'NMS', 'Carl Critchlow').
card_number('rhox'/'NMS', '112').
card_multiverse_id('rhox'/'NMS', '24687').

card_in_set('rising waters', 'NMS').
card_original_type('rising waters'/'NMS', 'Enchantment').
card_original_text('rising waters'/'NMS', 'Lands don\'t untap during their controllers\' untap steps.\nAt the beginning of each player\'s upkeep, that player untaps a land he or she controls.').
card_first_print('rising waters', 'NMS').
card_image_name('rising waters'/'NMS', 'rising waters').
card_uid('rising waters'/'NMS', 'NMS:Rising Waters:rising waters').
card_rarity('rising waters'/'NMS', 'Rare').
card_artist('rising waters'/'NMS', 'Scott M. Fischer').
card_number('rising waters'/'NMS', '38').
card_flavor_text('rising waters'/'NMS', 'Rising waters, sinking hope.').
card_multiverse_id('rising waters'/'NMS', '21304').

card_in_set('rootwater commando', 'NMS').
card_original_type('rootwater commando'/'NMS', 'Creature — Merfolk').
card_original_text('rootwater commando'/'NMS', 'Islandwalk (This creature is unblockable as long as defending player controls an island.)').
card_first_print('rootwater commando', 'NMS').
card_image_name('rootwater commando'/'NMS', 'rootwater commando').
card_uid('rootwater commando'/'NMS', 'NMS:Rootwater Commando:rootwater commando').
card_rarity('rootwater commando'/'NMS', 'Common').
card_artist('rootwater commando'/'NMS', 'Mark Tedin').
card_number('rootwater commando'/'NMS', '39').
card_flavor_text('rootwater commando'/'NMS', 'Rootwater merfolk are seldom seen these days, but elf corpses are as numerous as ever.').
card_multiverse_id('rootwater commando'/'NMS', '22292').

card_in_set('rootwater thief', 'NMS').
card_original_type('rootwater thief'/'NMS', 'Creature — Merfolk').
card_original_text('rootwater thief'/'NMS', '{U} Rootwater Thief gains flying until end of turn.\nWhenever Rootwater Thief deals combat damage to a player, you may pay {2}. If you do, search that player\'s library for a card and remove that card from the game, then the player shuffles his or her library.').
card_first_print('rootwater thief', 'NMS').
card_image_name('rootwater thief'/'NMS', 'rootwater thief').
card_uid('rootwater thief'/'NMS', 'NMS:Rootwater Thief:rootwater thief').
card_rarity('rootwater thief'/'NMS', 'Rare').
card_artist('rootwater thief'/'NMS', 'Ron Spears').
card_number('rootwater thief'/'NMS', '40').
card_multiverse_id('rootwater thief'/'NMS', '22889').

card_in_set('rupture', 'NMS').
card_original_type('rupture'/'NMS', 'Sorcery').
card_original_text('rupture'/'NMS', 'Sacrifice a creature. Rupture deals damage equal to that creature\'s power to each creature without flying and each player.').
card_first_print('rupture', 'NMS').
card_image_name('rupture'/'NMS', 'rupture').
card_uid('rupture'/'NMS', 'NMS:Rupture:rupture').
card_rarity('rupture'/'NMS', 'Uncommon').
card_artist('rupture'/'NMS', 'Gao Yan').
card_number('rupture'/'NMS', '97').
card_flavor_text('rupture'/'NMS', 'All creatures have an inner fire. Releasing it can be dangerous.').
card_multiverse_id('rupture'/'NMS', '21347').

card_in_set('rusting golem', 'NMS').
card_original_type('rusting golem'/'NMS', 'Artifact Creature — Golem').
card_original_text('rusting golem'/'NMS', 'Fading 5 (This creature comes into play with five fade counters on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)\nRusting Golem\'s power and toughness are each equal to the number of fade counters on it.').
card_first_print('rusting golem', 'NMS').
card_image_name('rusting golem'/'NMS', 'rusting golem').
card_uid('rusting golem'/'NMS', 'NMS:Rusting Golem:rusting golem').
card_rarity('rusting golem'/'NMS', 'Uncommon').
card_artist('rusting golem'/'NMS', 'Greg Staples').
card_number('rusting golem'/'NMS', '138').
card_multiverse_id('rusting golem'/'NMS', '21388').

card_in_set('saproling burst', 'NMS').
card_original_type('saproling burst'/'NMS', 'Enchantment').
card_original_text('saproling burst'/'NMS', 'Fading 7\nRemove a fade counter from Saproling Burst: Put a green Saproling creature token into play. It has \"This creature\'s power and toughness are each equal to the number of fade counters on Saproling Burst.\"\nWhen Saproling Burst leaves play, destroy all tokens put into play with Saproling Burst. They can\'t be regenerated.').
card_first_print('saproling burst', 'NMS').
card_image_name('saproling burst'/'NMS', 'saproling burst').
card_uid('saproling burst'/'NMS', 'NMS:Saproling Burst:saproling burst').
card_rarity('saproling burst'/'NMS', 'Rare').
card_artist('saproling burst'/'NMS', 'Carl Critchlow').
card_number('saproling burst'/'NMS', '113').
card_multiverse_id('saproling burst'/'NMS', '21381').

card_in_set('saproling cluster', 'NMS').
card_original_type('saproling cluster'/'NMS', 'Enchantment').
card_original_text('saproling cluster'/'NMS', '{1}, Discard a card from your hand: Put a 1/1 green Saproling creature token into play. Any player may play this ability.').
card_first_print('saproling cluster', 'NMS').
card_image_name('saproling cluster'/'NMS', 'saproling cluster').
card_uid('saproling cluster'/'NMS', 'NMS:Saproling Cluster:saproling cluster').
card_rarity('saproling cluster'/'NMS', 'Rare').
card_artist('saproling cluster'/'NMS', 'Matt Cavotta').
card_number('saproling cluster'/'NMS', '114').
card_multiverse_id('saproling cluster'/'NMS', '21385').

card_in_set('seahunter', 'NMS').
card_original_type('seahunter'/'NMS', 'Creature — Mercenary').
card_original_text('seahunter'/'NMS', '{3}, {T}: Search your library for a Merfolk card and put that card into play. Then shuffle your library.').
card_first_print('seahunter', 'NMS').
card_image_name('seahunter'/'NMS', 'seahunter').
card_uid('seahunter'/'NMS', 'NMS:Seahunter:seahunter').
card_rarity('seahunter'/'NMS', 'Rare').
card_artist('seahunter'/'NMS', 'Heather Hudson').
card_number('seahunter'/'NMS', '41').
card_flavor_text('seahunter'/'NMS', '\"They can\'t feel pain. They just wiggle \'cause they\'re scared.\"').
card_multiverse_id('seahunter'/'NMS', '21297').

card_in_set('seal of cleansing', 'NMS').
card_original_type('seal of cleansing'/'NMS', 'Enchantment').
card_original_text('seal of cleansing'/'NMS', 'Sacrifice Seal of Cleansing: Destroy target artifact or enchantment.').
card_image_name('seal of cleansing'/'NMS', 'seal of cleansing').
card_uid('seal of cleansing'/'NMS', 'NMS:Seal of Cleansing:seal of cleansing').
card_rarity('seal of cleansing'/'NMS', 'Common').
card_artist('seal of cleansing'/'NMS', 'Christopher Moeller').
card_number('seal of cleansing'/'NMS', '18').
card_flavor_text('seal of cleansing'/'NMS', '\"I am the purifier, the light that clears all shadows.\"\n—Seal inscription').
card_multiverse_id('seal of cleansing'/'NMS', '21259').

card_in_set('seal of doom', 'NMS').
card_original_type('seal of doom'/'NMS', 'Enchantment').
card_original_text('seal of doom'/'NMS', 'Sacrifice Seal of Doom: Destroy target nonblack creature. It can\'t be regenerated.').
card_first_print('seal of doom', 'NMS').
card_image_name('seal of doom'/'NMS', 'seal of doom').
card_uid('seal of doom'/'NMS', 'NMS:Seal of Doom:seal of doom').
card_rarity('seal of doom'/'NMS', 'Common').
card_artist('seal of doom'/'NMS', 'Christopher Moeller').
card_number('seal of doom'/'NMS', '70').
card_flavor_text('seal of doom'/'NMS', '\"I am the banisher, the ill will that snuffs the final candle.\"\n—Seal inscription').
card_multiverse_id('seal of doom'/'NMS', '21316').

card_in_set('seal of fire', 'NMS').
card_original_type('seal of fire'/'NMS', 'Enchantment').
card_original_text('seal of fire'/'NMS', 'Sacrifice Seal of Fire: Seal of Fire deals 2 damage to target creature or player.').
card_first_print('seal of fire', 'NMS').
card_image_name('seal of fire'/'NMS', 'seal of fire').
card_uid('seal of fire'/'NMS', 'NMS:Seal of Fire:seal of fire').
card_rarity('seal of fire'/'NMS', 'Common').
card_artist('seal of fire'/'NMS', 'Christopher Moeller').
card_number('seal of fire'/'NMS', '98').
card_flavor_text('seal of fire'/'NMS', '\"I am the romancer, the passion that consumes the flesh.\"\n—Seal inscription').
card_multiverse_id('seal of fire'/'NMS', '21340').

card_in_set('seal of removal', 'NMS').
card_original_type('seal of removal'/'NMS', 'Enchantment').
card_original_text('seal of removal'/'NMS', 'Sacrifice Seal of Removal: Return target creature to its owner\'s hand.').
card_first_print('seal of removal', 'NMS').
card_image_name('seal of removal'/'NMS', 'seal of removal').
card_uid('seal of removal'/'NMS', 'NMS:Seal of Removal:seal of removal').
card_rarity('seal of removal'/'NMS', 'Common').
card_artist('seal of removal'/'NMS', 'Christopher Moeller').
card_number('seal of removal'/'NMS', '42').
card_flavor_text('seal of removal'/'NMS', '\"I am the unraveler, the loosened thread that will not hold.\"\n—Seal inscription').
card_multiverse_id('seal of removal'/'NMS', '21287').

card_in_set('seal of strength', 'NMS').
card_original_type('seal of strength'/'NMS', 'Enchantment').
card_original_text('seal of strength'/'NMS', 'Sacrifice Seal of Strength: Target creature gets +3/+3 until end of turn.').
card_first_print('seal of strength', 'NMS').
card_image_name('seal of strength'/'NMS', 'seal of strength').
card_uid('seal of strength'/'NMS', 'NMS:Seal of Strength:seal of strength').
card_rarity('seal of strength'/'NMS', 'Common').
card_artist('seal of strength'/'NMS', 'Christopher Moeller').
card_number('seal of strength'/'NMS', '115').
card_flavor_text('seal of strength'/'NMS', '\"I am the feeder, the morsel that revives the starving.\"\n—Seal inscription').
card_multiverse_id('seal of strength'/'NMS', '21366').

card_in_set('shrieking mogg', 'NMS').
card_original_type('shrieking mogg'/'NMS', 'Creature — Goblin').
card_original_text('shrieking mogg'/'NMS', 'Haste (This creature may attack and {T} the turn it comes under your control.)\nWhen Shrieking Mogg comes into play, tap all other creatures.').
card_first_print('shrieking mogg', 'NMS').
card_image_name('shrieking mogg'/'NMS', 'shrieking mogg').
card_uid('shrieking mogg'/'NMS', 'NMS:Shrieking Mogg:shrieking mogg').
card_rarity('shrieking mogg'/'NMS', 'Rare').
card_artist('shrieking mogg'/'NMS', 'Dan Frazier').
card_number('shrieking mogg'/'NMS', '99').
card_flavor_text('shrieking mogg'/'NMS', 'You have to admire its enthusiasm.').
card_multiverse_id('shrieking mogg'/'NMS', '21353').

card_in_set('silkenfist fighter', 'NMS').
card_original_type('silkenfist fighter'/'NMS', 'Creature — Soldier').
card_original_text('silkenfist fighter'/'NMS', 'Whenever Silkenfist Fighter becomes blocked, untap it.').
card_first_print('silkenfist fighter', 'NMS').
card_image_name('silkenfist fighter'/'NMS', 'silkenfist fighter').
card_uid('silkenfist fighter'/'NMS', 'NMS:Silkenfist Fighter:silkenfist fighter').
card_rarity('silkenfist fighter'/'NMS', 'Common').
card_artist('silkenfist fighter'/'NMS', 'Mark Brill').
card_number('silkenfist fighter'/'NMS', '19').
card_flavor_text('silkenfist fighter'/'NMS', 'Simple wooden staff\nMade from life, protecting life,\nStronger than cold steel.').
card_multiverse_id('silkenfist fighter'/'NMS', '22320').

card_in_set('silkenfist order', 'NMS').
card_original_type('silkenfist order'/'NMS', 'Creature — Soldier').
card_original_text('silkenfist order'/'NMS', 'Whenever Silkenfist Order becomes blocked, untap it.').
card_first_print('silkenfist order', 'NMS').
card_image_name('silkenfist order'/'NMS', 'silkenfist order').
card_uid('silkenfist order'/'NMS', 'NMS:Silkenfist Order:silkenfist order').
card_rarity('silkenfist order'/'NMS', 'Uncommon').
card_artist('silkenfist order'/'NMS', 'Greg & Tim Hildebrandt').
card_number('silkenfist order'/'NMS', '20').
card_flavor_text('silkenfist order'/'NMS', 'Hands weave life and death;\nIntertwining spirits knit\nTapestries and shrouds.').
card_multiverse_id('silkenfist order'/'NMS', '22338').

card_in_set('sivvi\'s ruse', 'NMS').
card_original_type('sivvi\'s ruse'/'NMS', 'Instant').
card_original_text('sivvi\'s ruse'/'NMS', 'If an opponent controls a mountain and you control a plains, you may play Sivvi\'s Ruse without paying its mana cost.\nPrevent all damage that would be dealt this turn to creatures you control.').
card_first_print('sivvi\'s ruse', 'NMS').
card_image_name('sivvi\'s ruse'/'NMS', 'sivvi\'s ruse').
card_uid('sivvi\'s ruse'/'NMS', 'NMS:Sivvi\'s Ruse:sivvi\'s ruse').
card_rarity('sivvi\'s ruse'/'NMS', 'Uncommon').
card_artist('sivvi\'s ruse'/'NMS', 'Kev Walker').
card_number('sivvi\'s ruse'/'NMS', '21').
card_multiverse_id('sivvi\'s ruse'/'NMS', '21270').

card_in_set('sivvi\'s valor', 'NMS').
card_original_type('sivvi\'s valor'/'NMS', 'Instant').
card_original_text('sivvi\'s valor'/'NMS', 'If you control a plains, you may tap an untapped creature you control instead of paying the mana cost of Sivvi\'s Valor.\nAll damage that would be dealt to target creature this turn is dealt to you instead.').
card_first_print('sivvi\'s valor', 'NMS').
card_image_name('sivvi\'s valor'/'NMS', 'sivvi\'s valor').
card_uid('sivvi\'s valor'/'NMS', 'NMS:Sivvi\'s Valor:sivvi\'s valor').
card_rarity('sivvi\'s valor'/'NMS', 'Rare').
card_artist('sivvi\'s valor'/'NMS', 'Jeff Miracola').
card_number('sivvi\'s valor'/'NMS', '22').
card_multiverse_id('sivvi\'s valor'/'NMS', '22302').

card_in_set('skyshroud behemoth', 'NMS').
card_original_type('skyshroud behemoth'/'NMS', 'Creature — Beast').
card_original_text('skyshroud behemoth'/'NMS', 'Fading 2 (This creature comes into play with two fade counters on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)\nSkyshroud Behemoth comes into play tapped.').
card_first_print('skyshroud behemoth', 'NMS').
card_image_name('skyshroud behemoth'/'NMS', 'skyshroud behemoth').
card_uid('skyshroud behemoth'/'NMS', 'NMS:Skyshroud Behemoth:skyshroud behemoth').
card_rarity('skyshroud behemoth'/'NMS', 'Rare').
card_artist('skyshroud behemoth'/'NMS', 'Eric Peterson').
card_number('skyshroud behemoth'/'NMS', '116').
card_multiverse_id('skyshroud behemoth'/'NMS', '22892').

card_in_set('skyshroud claim', 'NMS').
card_original_type('skyshroud claim'/'NMS', 'Sorcery').
card_original_text('skyshroud claim'/'NMS', 'Search your library for up to two forest cards and put them into play. Then shuffle your library.').
card_first_print('skyshroud claim', 'NMS').
card_image_name('skyshroud claim'/'NMS', 'skyshroud claim').
card_uid('skyshroud claim'/'NMS', 'NMS:Skyshroud Claim:skyshroud claim').
card_rarity('skyshroud claim'/'NMS', 'Common').
card_artist('skyshroud claim'/'NMS', 'Mark Romanoski').
card_number('skyshroud claim'/'NMS', '117').
card_flavor_text('skyshroud claim'/'NMS', 'The forest\'s constant struggle is to keep the spreading flowstone at bay.').
card_multiverse_id('skyshroud claim'/'NMS', '22891').

card_in_set('skyshroud cutter', 'NMS').
card_original_type('skyshroud cutter'/'NMS', 'Creature — Beast').
card_original_text('skyshroud cutter'/'NMS', 'If you control a forest, you may have each other player gain 5 life instead of paying Skyshroud Cutter\'s mana cost.').
card_first_print('skyshroud cutter', 'NMS').
card_image_name('skyshroud cutter'/'NMS', 'skyshroud cutter').
card_uid('skyshroud cutter'/'NMS', 'NMS:Skyshroud Cutter:skyshroud cutter').
card_rarity('skyshroud cutter'/'NMS', 'Common').
card_artist('skyshroud cutter'/'NMS', 'Tony Szczudlo').
card_number('skyshroud cutter'/'NMS', '118').
card_multiverse_id('skyshroud cutter'/'NMS', '21372').

card_in_set('skyshroud poacher', 'NMS').
card_original_type('skyshroud poacher'/'NMS', 'Creature — Rebel').
card_original_text('skyshroud poacher'/'NMS', '{3}, {T}: Search your library for an Elf card and put that card into play. Then shuffle your library.').
card_first_print('skyshroud poacher', 'NMS').
card_image_name('skyshroud poacher'/'NMS', 'skyshroud poacher').
card_uid('skyshroud poacher'/'NMS', 'NMS:Skyshroud Poacher:skyshroud poacher').
card_rarity('skyshroud poacher'/'NMS', 'Rare').
card_artist('skyshroud poacher'/'NMS', 'Ron Spencer').
card_number('skyshroud poacher'/'NMS', '119').
card_flavor_text('skyshroud poacher'/'NMS', '\"It\'s OK. They\'re just elves.\"').
card_multiverse_id('skyshroud poacher'/'NMS', '21379').

card_in_set('skyshroud ridgeback', 'NMS').
card_original_type('skyshroud ridgeback'/'NMS', 'Creature — Beast').
card_original_text('skyshroud ridgeback'/'NMS', 'Fading 2 (This creature comes into play with two fade counters on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)').
card_first_print('skyshroud ridgeback', 'NMS').
card_image_name('skyshroud ridgeback'/'NMS', 'skyshroud ridgeback').
card_uid('skyshroud ridgeback'/'NMS', 'NMS:Skyshroud Ridgeback:skyshroud ridgeback').
card_rarity('skyshroud ridgeback'/'NMS', 'Common').
card_artist('skyshroud ridgeback'/'NMS', 'Carl Critchlow').
card_number('skyshroud ridgeback'/'NMS', '120').
card_multiverse_id('skyshroud ridgeback'/'NMS', '21360').

card_in_set('skyshroud sentinel', 'NMS').
card_original_type('skyshroud sentinel'/'NMS', 'Creature — Elf').
card_original_text('skyshroud sentinel'/'NMS', 'When Skyshroud Sentinel comes into play, you may search your library for up to three Skyshroud Sentinel cards, reveal them, and put them into your hand. If you do, shuffle your library.').
card_first_print('skyshroud sentinel', 'NMS').
card_image_name('skyshroud sentinel'/'NMS', 'skyshroud sentinel').
card_uid('skyshroud sentinel'/'NMS', 'NMS:Skyshroud Sentinel:skyshroud sentinel').
card_rarity('skyshroud sentinel'/'NMS', 'Common').
card_artist('skyshroud sentinel'/'NMS', 'Randy Gallegos').
card_number('skyshroud sentinel'/'NMS', '121').
card_multiverse_id('skyshroud sentinel'/'NMS', '21362').

card_in_set('sliptide serpent', 'NMS').
card_original_type('sliptide serpent'/'NMS', 'Creature — Serpent').
card_original_text('sliptide serpent'/'NMS', '{3}{U} Return Sliptide Serpent to its owner\'s hand.').
card_first_print('sliptide serpent', 'NMS').
card_image_name('sliptide serpent'/'NMS', 'sliptide serpent').
card_uid('sliptide serpent'/'NMS', 'NMS:Sliptide Serpent:sliptide serpent').
card_rarity('sliptide serpent'/'NMS', 'Rare').
card_artist('sliptide serpent'/'NMS', 'Daren Bader').
card_number('sliptide serpent'/'NMS', '43').
card_flavor_text('sliptide serpent'/'NMS', 'It\'s futile to fight something you can see only in the corner of your eye.').
card_multiverse_id('sliptide serpent'/'NMS', '21283').

card_in_set('sneaky homunculus', 'NMS').
card_original_type('sneaky homunculus'/'NMS', 'Creature — Illusion').
card_original_text('sneaky homunculus'/'NMS', 'Sneaky Homunculus can\'t block or be blocked by creatures with power 2 or greater.').
card_first_print('sneaky homunculus', 'NMS').
card_image_name('sneaky homunculus'/'NMS', 'sneaky homunculus').
card_uid('sneaky homunculus'/'NMS', 'NMS:Sneaky Homunculus:sneaky homunculus').
card_rarity('sneaky homunculus'/'NMS', 'Common').
card_artist('sneaky homunculus'/'NMS', 'Scott M. Fischer').
card_number('sneaky homunculus'/'NMS', '44').
card_flavor_text('sneaky homunculus'/'NMS', 'Keep watch only for the giants and you\'ll be eaten by the ants.').
card_multiverse_id('sneaky homunculus'/'NMS', '21282').

card_in_set('spineless thug', 'NMS').
card_original_type('spineless thug'/'NMS', 'Creature — Mercenary').
card_original_text('spineless thug'/'NMS', 'Spineless Thug can\'t block.').
card_first_print('spineless thug', 'NMS').
card_image_name('spineless thug'/'NMS', 'spineless thug').
card_uid('spineless thug'/'NMS', 'NMS:Spineless Thug:spineless thug').
card_rarity('spineless thug'/'NMS', 'Common').
card_artist('spineless thug'/'NMS', 'Matthew D. Wilson').
card_number('spineless thug'/'NMS', '71').
card_flavor_text('spineless thug'/'NMS', 'These troops are only as strong as the evincar\'s control.').
card_multiverse_id('spineless thug'/'NMS', '21310').

card_in_set('spiritual asylum', 'NMS').
card_original_type('spiritual asylum'/'NMS', 'Enchantment').
card_original_text('spiritual asylum'/'NMS', 'Creatures and lands you control can\'t be the target of spells or abilities.  \nWhen a creature you control attacks, sacrifice Spiritual Asylum.').
card_first_print('spiritual asylum', 'NMS').
card_image_name('spiritual asylum'/'NMS', 'spiritual asylum').
card_uid('spiritual asylum'/'NMS', 'NMS:Spiritual Asylum:spiritual asylum').
card_rarity('spiritual asylum'/'NMS', 'Rare').
card_artist('spiritual asylum'/'NMS', 'Matt Cavotta').
card_number('spiritual asylum'/'NMS', '23').
card_flavor_text('spiritual asylum'/'NMS', 'Trapped in safety is still trapped.').
card_multiverse_id('spiritual asylum'/'NMS', '21277').

card_in_set('spiteful bully', 'NMS').
card_original_type('spiteful bully'/'NMS', 'Creature — Mercenary').
card_original_text('spiteful bully'/'NMS', 'At the beginning of your upkeep, Spiteful Bully deals 3 damage to target creature you control.').
card_first_print('spiteful bully', 'NMS').
card_image_name('spiteful bully'/'NMS', 'spiteful bully').
card_uid('spiteful bully'/'NMS', 'NMS:Spiteful Bully:spiteful bully').
card_rarity('spiteful bully'/'NMS', 'Common').
card_artist('spiteful bully'/'NMS', 'Chippy').
card_number('spiteful bully'/'NMS', '72').
card_flavor_text('spiteful bully'/'NMS', 'Like most creatures bent on total destruction, they rarely bother to double-check their targets.').
card_multiverse_id('spiteful bully'/'NMS', '21309').

card_in_set('stampede driver', 'NMS').
card_original_type('stampede driver'/'NMS', 'Creature — Spellshaper').
card_original_text('stampede driver'/'NMS', '{1}{G}, {T}, Discard a card from your hand: Creatures you control get +1/+1 and gain trample until end of turn.').
card_first_print('stampede driver', 'NMS').
card_image_name('stampede driver'/'NMS', 'stampede driver').
card_uid('stampede driver'/'NMS', 'NMS:Stampede Driver:stampede driver').
card_rarity('stampede driver'/'NMS', 'Uncommon').
card_artist('stampede driver'/'NMS', 'Ron Spears').
card_number('stampede driver'/'NMS', '122').
card_flavor_text('stampede driver'/'NMS', '\"The thunder of hooves is music to my ears.\"').
card_multiverse_id('stampede driver'/'NMS', '21359').

card_in_set('stronghold biologist', 'NMS').
card_original_type('stronghold biologist'/'NMS', 'Creature — Spellshaper').
card_original_text('stronghold biologist'/'NMS', '{U}{U}, {T}, Discard a card from your hand: Counter target creature spell.').
card_first_print('stronghold biologist', 'NMS').
card_image_name('stronghold biologist'/'NMS', 'stronghold biologist').
card_uid('stronghold biologist'/'NMS', 'NMS:Stronghold Biologist:stronghold biologist').
card_rarity('stronghold biologist'/'NMS', 'Uncommon').
card_artist('stronghold biologist'/'NMS', 'Terese Nielsen').
card_number('stronghold biologist'/'NMS', '45').
card_flavor_text('stronghold biologist'/'NMS', 'He twists the laws of nature until they scream.').
card_multiverse_id('stronghold biologist'/'NMS', '21298').

card_in_set('stronghold discipline', 'NMS').
card_original_type('stronghold discipline'/'NMS', 'Sorcery').
card_original_text('stronghold discipline'/'NMS', 'Each player loses 1 life for each creature he or she controls.').
card_first_print('stronghold discipline', 'NMS').
card_image_name('stronghold discipline'/'NMS', 'stronghold discipline').
card_uid('stronghold discipline'/'NMS', 'NMS:Stronghold Discipline:stronghold discipline').
card_rarity('stronghold discipline'/'NMS', 'Common').
card_artist('stronghold discipline'/'NMS', 'Li Tie').
card_number('stronghold discipline'/'NMS', '73').
card_flavor_text('stronghold discipline'/'NMS', '\"Crovax never passes up an opportunity to cause widespread misery.\"\n—Belbe').
card_multiverse_id('stronghold discipline'/'NMS', '22374').

card_in_set('stronghold gambit', 'NMS').
card_original_type('stronghold gambit'/'NMS', 'Sorcery').
card_original_text('stronghold gambit'/'NMS', 'Each player chooses a card in his or her hand. Then each player reveals his or her chosen card. The owner of the creature card revealed this way with the lowest converted mana cost puts that card into play. If two or more creature cards are tied for lowest cost, those cards are put into play.').
card_first_print('stronghold gambit', 'NMS').
card_image_name('stronghold gambit'/'NMS', 'stronghold gambit').
card_uid('stronghold gambit'/'NMS', 'NMS:Stronghold Gambit:stronghold gambit').
card_rarity('stronghold gambit'/'NMS', 'Rare').
card_artist('stronghold gambit'/'NMS', 'Greg & Tim Hildebrandt').
card_number('stronghold gambit'/'NMS', '100').
card_multiverse_id('stronghold gambit'/'NMS', '21357').

card_in_set('stronghold machinist', 'NMS').
card_original_type('stronghold machinist'/'NMS', 'Creature — Spellshaper').
card_original_text('stronghold machinist'/'NMS', '{U}{U}, {T}, Discard a card from your hand: Counter target noncreature spell.').
card_first_print('stronghold machinist', 'NMS').
card_image_name('stronghold machinist'/'NMS', 'stronghold machinist').
card_uid('stronghold machinist'/'NMS', 'NMS:Stronghold Machinist:stronghold machinist').
card_rarity('stronghold machinist'/'NMS', 'Uncommon').
card_artist('stronghold machinist'/'NMS', 'Terese Nielsen').
card_number('stronghold machinist'/'NMS', '46').
card_flavor_text('stronghold machinist'/'NMS', 'She stretches the laws of physics until they snap.').
card_multiverse_id('stronghold machinist'/'NMS', '22033').

card_in_set('stronghold zeppelin', 'NMS').
card_original_type('stronghold zeppelin'/'NMS', 'Creature — Ship').
card_original_text('stronghold zeppelin'/'NMS', 'Flying\nStronghold Zeppelin can block only creatures with flying.').
card_first_print('stronghold zeppelin', 'NMS').
card_image_name('stronghold zeppelin'/'NMS', 'stronghold zeppelin').
card_uid('stronghold zeppelin'/'NMS', 'NMS:Stronghold Zeppelin:stronghold zeppelin').
card_rarity('stronghold zeppelin'/'NMS', 'Uncommon').
card_artist('stronghold zeppelin'/'NMS', 'Arnie Swekel').
card_number('stronghold zeppelin'/'NMS', '47').
card_flavor_text('stronghold zeppelin'/'NMS', 'As long as the stronghold controlled the sky, the rebels were doomed.').
card_multiverse_id('stronghold zeppelin'/'NMS', '22880').

card_in_set('submerge', 'NMS').
card_original_type('submerge'/'NMS', 'Instant').
card_original_text('submerge'/'NMS', 'If an opponent controls a forest and you control an island, you may play Submerge without paying its mana cost. \nPut target creature on top of its owner\'s library.').
card_first_print('submerge', 'NMS').
card_image_name('submerge'/'NMS', 'submerge').
card_uid('submerge'/'NMS', 'NMS:Submerge:submerge').
card_rarity('submerge'/'NMS', 'Uncommon').
card_artist('submerge'/'NMS', 'Mark Romanoski').
card_number('submerge'/'NMS', '48').
card_multiverse_id('submerge'/'NMS', '21296').

card_in_set('tangle wire', 'NMS').
card_original_type('tangle wire'/'NMS', 'Artifact').
card_original_text('tangle wire'/'NMS', 'Fading 4 (This artifact comes into play with four fade counters on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)\nAt the beginning of each player\'s upkeep, that player taps an untapped artifact, creature, or land he or she controls for each fade counter on Tangle Wire.').
card_first_print('tangle wire', 'NMS').
card_image_name('tangle wire'/'NMS', 'tangle wire').
card_uid('tangle wire'/'NMS', 'NMS:Tangle Wire:tangle wire').
card_rarity('tangle wire'/'NMS', 'Rare').
card_artist('tangle wire'/'NMS', 'Glen Angus').
card_number('tangle wire'/'NMS', '139').
card_multiverse_id('tangle wire'/'NMS', '21399').

card_in_set('terrain generator', 'NMS').
card_original_type('terrain generator'/'NMS', 'Land').
card_original_text('terrain generator'/'NMS', '{T}: Add one colorless mana to your mana pool.\n{2}, {T}: Put a basic land card from your hand into play tapped.').
card_first_print('terrain generator', 'NMS').
card_image_name('terrain generator'/'NMS', 'terrain generator').
card_uid('terrain generator'/'NMS', 'NMS:Terrain Generator:terrain generator').
card_rarity('terrain generator'/'NMS', 'Uncommon').
card_artist('terrain generator'/'NMS', 'Alan Pollack').
card_number('terrain generator'/'NMS', '143').
card_multiverse_id('terrain generator'/'NMS', '21394').

card_in_set('topple', 'NMS').
card_original_type('topple'/'NMS', 'Sorcery').
card_original_text('topple'/'NMS', 'Remove target creature with the greatest power from the game. (If two or more creatures are tied for greatest power, target only one of them.)').
card_first_print('topple', 'NMS').
card_image_name('topple'/'NMS', 'topple').
card_uid('topple'/'NMS', 'NMS:Topple:topple').
card_rarity('topple'/'NMS', 'Common').
card_artist('topple'/'NMS', 'Daren Bader').
card_number('topple'/'NMS', '24').
card_flavor_text('topple'/'NMS', '\"Let all witness the fate of those who defy me.\"\n—Crovax').
card_multiverse_id('topple'/'NMS', '21268').

card_in_set('treetop bracers', 'NMS').
card_original_type('treetop bracers'/'NMS', 'Enchant Creature').
card_original_text('treetop bracers'/'NMS', 'Enchanted creature gets +1/+1 and can be blocked only by creatures with flying.').
card_first_print('treetop bracers', 'NMS').
card_image_name('treetop bracers'/'NMS', 'treetop bracers').
card_uid('treetop bracers'/'NMS', 'NMS:Treetop Bracers:treetop bracers').
card_rarity('treetop bracers'/'NMS', 'Common').
card_artist('treetop bracers'/'NMS', 'Heather Hudson').
card_number('treetop bracers'/'NMS', '123').
card_flavor_text('treetop bracers'/'NMS', 'Gravity is truly what you make of it.').
card_multiverse_id('treetop bracers'/'NMS', '21365').

card_in_set('trickster mage', 'NMS').
card_original_type('trickster mage'/'NMS', 'Creature — Spellshaper').
card_original_text('trickster mage'/'NMS', '{U}, {T}, Discard a card from your hand: Tap or untap target artifact, creature, or land.').
card_first_print('trickster mage', 'NMS').
card_image_name('trickster mage'/'NMS', 'trickster mage').
card_uid('trickster mage'/'NMS', 'NMS:Trickster Mage:trickster mage').
card_rarity('trickster mage'/'NMS', 'Common').
card_artist('trickster mage'/'NMS', 'Alan Rabinowitz').
card_number('trickster mage'/'NMS', '49').
card_flavor_text('trickster mage'/'NMS', '\"If it ain\'t broke, I\'ll break it. If it is broke, I\'ll fix it.\"').
card_multiverse_id('trickster mage'/'NMS', '21278').

card_in_set('vicious hunger', 'NMS').
card_original_type('vicious hunger'/'NMS', 'Sorcery').
card_original_text('vicious hunger'/'NMS', 'Vicious Hunger deals 2 damage to target creature. You gain 2 life.').
card_first_print('vicious hunger', 'NMS').
card_image_name('vicious hunger'/'NMS', 'vicious hunger').
card_uid('vicious hunger'/'NMS', 'NMS:Vicious Hunger:vicious hunger').
card_rarity('vicious hunger'/'NMS', 'Common').
card_artist('vicious hunger'/'NMS', 'Massimilano Frezzato').
card_number('vicious hunger'/'NMS', '74').
card_flavor_text('vicious hunger'/'NMS', '\"Blood is foul, the flesh dross. My ascendance thirsts for life itself.\"\n—Crovax').
card_multiverse_id('vicious hunger'/'NMS', '21317').

card_in_set('viseling', 'NMS').
card_original_type('viseling'/'NMS', 'Artifact Creature').
card_original_text('viseling'/'NMS', 'At the beginning of each opponent\'s upkeep, Viseling deals X damage to that player, where X is the number of cards in his or her hand minus four.').
card_first_print('viseling', 'NMS').
card_image_name('viseling'/'NMS', 'viseling').
card_uid('viseling'/'NMS', 'NMS:Viseling:viseling').
card_rarity('viseling'/'NMS', 'Uncommon').
card_artist('viseling'/'NMS', 'Kev Walker').
card_number('viseling'/'NMS', '140').
card_flavor_text('viseling'/'NMS', '\"This may hurt a lot.\"').
card_multiverse_id('viseling'/'NMS', '21397').

card_in_set('voice of truth', 'NMS').
card_original_type('voice of truth'/'NMS', 'Creature — Angel').
card_original_text('voice of truth'/'NMS', 'Flying, protection from white').
card_first_print('voice of truth', 'NMS').
card_image_name('voice of truth'/'NMS', 'voice of truth').
card_uid('voice of truth'/'NMS', 'NMS:Voice of Truth:voice of truth').
card_rarity('voice of truth'/'NMS', 'Uncommon').
card_artist('voice of truth'/'NMS', 'rk post').
card_number('voice of truth'/'NMS', '25').
card_flavor_text('voice of truth'/'NMS', '\"Above them all is Truth, and Truth must be set free. If the wings of Truth are clipped, the voices will fall silent.\"\n—Song of All, canto 167').
card_multiverse_id('voice of truth'/'NMS', '21265').

card_in_set('volrath the fallen', 'NMS').
card_original_type('volrath the fallen'/'NMS', 'Creature — Legend').
card_original_text('volrath the fallen'/'NMS', '{1}{B}, Discard a creature card from your hand: Volrath the Fallen gets +X/+X until end of turn, where X is the discarded card\'s converted mana cost.').
card_first_print('volrath the fallen', 'NMS').
card_image_name('volrath the fallen'/'NMS', 'volrath the fallen').
card_uid('volrath the fallen'/'NMS', 'NMS:Volrath the Fallen:volrath the fallen').
card_rarity('volrath the fallen'/'NMS', 'Rare').
card_artist('volrath the fallen'/'NMS', 'Kev Walker').
card_number('volrath the fallen'/'NMS', '75').
card_flavor_text('volrath the fallen'/'NMS', '\"I stepped out. I did not step down.\"').
card_multiverse_id('volrath the fallen'/'NMS', '21328').

card_in_set('wandering eye', 'NMS').
card_original_type('wandering eye'/'NMS', 'Creature — Illusion').
card_original_text('wandering eye'/'NMS', 'Flying \nAll players play with their hands revealed.').
card_first_print('wandering eye', 'NMS').
card_image_name('wandering eye'/'NMS', 'wandering eye').
card_uid('wandering eye'/'NMS', 'NMS:Wandering Eye:wandering eye').
card_rarity('wandering eye'/'NMS', 'Common').
card_artist('wandering eye'/'NMS', 'Sam Wood').
card_number('wandering eye'/'NMS', '50').
card_flavor_text('wandering eye'/'NMS', 'The evincar has many ways of keeping track of his subjects.').
card_multiverse_id('wandering eye'/'NMS', '21279').

card_in_set('wild mammoth', 'NMS').
card_original_type('wild mammoth'/'NMS', 'Creature — Elephant').
card_original_text('wild mammoth'/'NMS', 'At the beginning of your upkeep, if a player controls more creatures than any other, that player gains control of Wild Mammoth.').
card_first_print('wild mammoth', 'NMS').
card_image_name('wild mammoth'/'NMS', 'wild mammoth').
card_uid('wild mammoth'/'NMS', 'NMS:Wild Mammoth:wild mammoth').
card_rarity('wild mammoth'/'NMS', 'Uncommon').
card_artist('wild mammoth'/'NMS', 'Bradley Williams').
card_number('wild mammoth'/'NMS', '124').
card_flavor_text('wild mammoth'/'NMS', '\"Sit. Heel! Down! HELP!\"').
card_multiverse_id('wild mammoth'/'NMS', '21376').

card_in_set('woodripper', 'NMS').
card_original_type('woodripper'/'NMS', 'Creature — Beast').
card_original_text('woodripper'/'NMS', 'Fading 3 (This creature comes into play with three fade counters on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)\n{1}, Remove a fade counter from Woodripper: Destroy target artifact.').
card_first_print('woodripper', 'NMS').
card_image_name('woodripper'/'NMS', 'woodripper').
card_uid('woodripper'/'NMS', 'NMS:Woodripper:woodripper').
card_rarity('woodripper'/'NMS', 'Uncommon').
card_artist('woodripper'/'NMS', 'Alan Pollack').
card_number('woodripper'/'NMS', '125').
card_multiverse_id('woodripper'/'NMS', '21374').
