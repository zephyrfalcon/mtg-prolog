% Prerelease Events

set('pPRE').
set_name('pPRE', 'Prerelease Events').
set_release_date('pPRE', '1997-10-04').
set_border('pPRE', 'black').
set_type('pPRE', 'promo').

card_in_set('abhorrent overlord', 'pPRE').
card_original_type('abhorrent overlord'/'pPRE', 'Creature — Demon').
card_original_text('abhorrent overlord'/'pPRE', '').
card_first_print('abhorrent overlord', 'pPRE').
card_image_name('abhorrent overlord'/'pPRE', 'abhorrent overlord').
card_uid('abhorrent overlord'/'pPRE', 'pPRE:Abhorrent Overlord:abhorrent overlord').
card_rarity('abhorrent overlord'/'pPRE', 'Special').
card_artist('abhorrent overlord'/'pPRE', 'Dave Kendall').
card_number('abhorrent overlord'/'pPRE', '70').

card_in_set('abzan ascendancy', 'pPRE').
card_original_type('abzan ascendancy'/'pPRE', 'Enchantment').
card_original_text('abzan ascendancy'/'pPRE', '').
card_first_print('abzan ascendancy', 'pPRE').
card_image_name('abzan ascendancy'/'pPRE', 'abzan ascendancy').
card_uid('abzan ascendancy'/'pPRE', 'pPRE:Abzan Ascendancy:abzan ascendancy').
card_rarity('abzan ascendancy'/'pPRE', 'Special').
card_artist('abzan ascendancy'/'pPRE', 'Mark Winters').
card_number('abzan ascendancy'/'pPRE', '88').

card_in_set('ajani vengeant', 'pPRE').
card_original_type('ajani vengeant'/'pPRE', 'Planeswalker — Ajani').
card_original_text('ajani vengeant'/'pPRE', '').
card_first_print('ajani vengeant', 'pPRE').
card_image_name('ajani vengeant'/'pPRE', 'ajani vengeant').
card_uid('ajani vengeant'/'pPRE', 'pPRE:Ajani Vengeant:ajani vengeant').
card_rarity('ajani vengeant'/'pPRE', 'Special').
card_artist('ajani vengeant'/'pPRE', 'Jason Chan').
card_number('ajani vengeant'/'pPRE', '38').

card_in_set('allosaurus rider', 'pPRE').
card_original_type('allosaurus rider'/'pPRE', 'Creature — Elf Warrior').
card_original_text('allosaurus rider'/'pPRE', '').
card_first_print('allosaurus rider', 'pPRE').
card_image_name('allosaurus rider'/'pPRE', 'allosaurus rider').
card_uid('allosaurus rider'/'pPRE', 'pPRE:Allosaurus Rider:allosaurus rider').
card_rarity('allosaurus rider'/'pPRE', 'Special').
card_artist('allosaurus rider'/'pPRE', 'Daren Bader').
card_number('allosaurus rider'/'pPRE', '30').

card_in_set('anafenza, the foremost', 'pPRE').
card_original_type('anafenza, the foremost'/'pPRE', 'Legendary Creature — Human Soldier').
card_original_text('anafenza, the foremost'/'pPRE', '').
card_first_print('anafenza, the foremost', 'pPRE').
card_image_name('anafenza, the foremost'/'pPRE', 'anafenza, the foremost').
card_uid('anafenza, the foremost'/'pPRE', 'pPRE:Anafenza, the Foremost:anafenza, the foremost').
card_rarity('anafenza, the foremost'/'pPRE', 'Special').
card_artist('anafenza, the foremost'/'pPRE', 'James Ryman').
card_number('anafenza, the foremost'/'pPRE', '89').
card_flavor_text('anafenza, the foremost'/'pPRE', 'Rarely at rest on the Amber Throne, Anafenza always leads the Abzan Houses to battle.').

card_in_set('ankle shanker', 'pPRE').
card_original_type('ankle shanker'/'pPRE', 'Creature — Goblin Berserker').
card_original_text('ankle shanker'/'pPRE', '').
card_image_name('ankle shanker'/'pPRE', 'ankle shanker').
card_uid('ankle shanker'/'pPRE', 'pPRE:Ankle Shanker:ankle shanker').
card_rarity('ankle shanker'/'pPRE', 'Special').
card_artist('ankle shanker'/'pPRE', 'Zoltan Boros').
card_number('ankle shanker'/'pPRE', '90').
card_flavor_text('ankle shanker'/'pPRE', 'The stature of the fighter matters less than the depth of the cut.').

card_in_set('anthousa, setessan hero', 'pPRE').
card_original_type('anthousa, setessan hero'/'pPRE', 'Legendary Creature — Human Warrior').
card_original_text('anthousa, setessan hero'/'pPRE', '').
card_first_print('anthousa, setessan hero', 'pPRE').
card_image_name('anthousa, setessan hero'/'pPRE', 'anthousa, setessan hero').
card_uid('anthousa, setessan hero'/'pPRE', 'pPRE:Anthousa, Setessan Hero:anthousa, setessan hero').
card_rarity('anthousa, setessan hero'/'pPRE', 'Special').
card_artist('anthousa, setessan hero'/'pPRE', 'Howard Lyon').
card_number('anthousa, setessan hero'/'pPRE', '72').
card_flavor_text('anthousa, setessan hero'/'pPRE', 'The warriors she leads can rarely keep pace with her, and neither can the tales.').

card_in_set('arbiter of the ideal', 'pPRE').
card_original_type('arbiter of the ideal'/'pPRE', 'Creature — Sphinx').
card_original_text('arbiter of the ideal'/'pPRE', '').
card_first_print('arbiter of the ideal', 'pPRE').
card_image_name('arbiter of the ideal'/'pPRE', 'arbiter of the ideal').
card_uid('arbiter of the ideal'/'pPRE', 'pPRE:Arbiter of the Ideal:arbiter of the ideal').
card_rarity('arbiter of the ideal'/'pPRE', 'Special').
card_artist('arbiter of the ideal'/'pPRE', 'Johann Bodin').
card_number('arbiter of the ideal'/'pPRE', '74').

card_in_set('archdemon of greed', 'pPRE').
card_original_type('archdemon of greed'/'pPRE', 'Creature — Demon').
card_original_text('archdemon of greed'/'pPRE', '').
card_first_print('archdemon of greed', 'pPRE').
card_image_name('archdemon of greed'/'pPRE', 'archdemon of greed').
card_uid('archdemon of greed'/'pPRE', 'pPRE:Archdemon of Greed:archdemon of greed').
card_rarity('archdemon of greed'/'pPRE', 'Special').
card_artist('archdemon of greed'/'pPRE', 'Kev Walker').
card_number('archdemon of greed'/'pPRE', '52b').
card_flavor_text('archdemon of greed'/'pPRE', '\"The price is paid. The power is mine!\"').

card_in_set('archon of the triumvirate', 'pPRE').
card_original_type('archon of the triumvirate'/'pPRE', 'Creature — Archon').
card_original_text('archon of the triumvirate'/'pPRE', '').
card_first_print('archon of the triumvirate', 'pPRE').
card_image_name('archon of the triumvirate'/'pPRE', 'archon of the triumvirate').
card_uid('archon of the triumvirate'/'pPRE', 'pPRE:Archon of the Triumvirate:archon of the triumvirate').
card_rarity('archon of the triumvirate'/'pPRE', 'Special').
card_artist('archon of the triumvirate'/'pPRE', 'Svetlin Velinov').
card_number('archon of the triumvirate'/'pPRE', '55').

card_in_set('avalanche tusker', 'pPRE').
card_original_type('avalanche tusker'/'pPRE', 'Creature — Elephant Warrior').
card_original_text('avalanche tusker'/'pPRE', '').
card_image_name('avalanche tusker'/'pPRE', 'avalanche tusker').
card_uid('avalanche tusker'/'pPRE', 'pPRE:Avalanche Tusker:avalanche tusker').
card_rarity('avalanche tusker'/'pPRE', 'Special').
card_artist('avalanche tusker'/'pPRE', 'Matt Stewart').
card_number('avalanche tusker'/'pPRE', '91').
card_flavor_text('avalanche tusker'/'pPRE', '\"Hold the high ground, then bring it to your enemy\"\n—Surrak, khan of the Temur').

card_in_set('avatar of discord', 'pPRE').
card_original_type('avatar of discord'/'pPRE', 'Creature — Avatar').
card_original_text('avatar of discord'/'pPRE', '').
card_first_print('avatar of discord', 'pPRE').
card_image_name('avatar of discord'/'pPRE', 'avatar of discord').
card_uid('avatar of discord'/'pPRE', 'pPRE:Avatar of Discord:avatar of discord').
card_rarity('avatar of discord'/'pPRE', 'Special').
card_artist('avatar of discord'/'pPRE', 'rk post').
card_number('avatar of discord'/'pPRE', '29').
card_flavor_text('avatar of discord'/'pPRE', 'Such is the power of Rakdos that even his shadow takes on a cruel life of its own.').

card_in_set('avatar of hope', 'pPRE').
card_original_type('avatar of hope'/'pPRE', 'Creature — Avatar').
card_original_text('avatar of hope'/'pPRE', '').
card_first_print('avatar of hope', 'pPRE').
card_image_name('avatar of hope'/'pPRE', 'avatar of hope').
card_uid('avatar of hope'/'pPRE', 'pPRE:Avatar of Hope:avatar of hope').
card_rarity('avatar of hope'/'pPRE', 'Special').
card_artist('avatar of hope'/'pPRE', 'rk post').
card_number('avatar of hope'/'pPRE', '11').

card_in_set('beast of burden', 'pPRE').
card_original_type('beast of burden'/'pPRE', 'Artifact Creature — Golem').
card_original_text('beast of burden'/'pPRE', '').
card_first_print('beast of burden', 'pPRE').
card_image_name('beast of burden'/'pPRE', 'beast of burden').
card_uid('beast of burden'/'pPRE', 'pPRE:Beast of Burden:beast of burden').
card_rarity('beast of burden'/'pPRE', 'Special').
card_artist('beast of burden'/'pPRE', 'Ron Spears').
card_number('beast of burden'/'pPRE', '5').
card_flavor_text('beast of burden'/'pPRE', '\"If it is meant to be nothing but a machine,\" Karn finally asked Jhoira, \"why did Urza build it to be like me?\"').

card_in_set('bloodlord of vaasgoth', 'pPRE').
card_original_type('bloodlord of vaasgoth'/'pPRE', 'Creature — Vampire Warrior').
card_original_text('bloodlord of vaasgoth'/'pPRE', '').
card_first_print('bloodlord of vaasgoth', 'pPRE').
card_image_name('bloodlord of vaasgoth'/'pPRE', 'bloodlord of vaasgoth').
card_uid('bloodlord of vaasgoth'/'pPRE', 'pPRE:Bloodlord of Vaasgoth:bloodlord of vaasgoth').
card_rarity('bloodlord of vaasgoth'/'pPRE', 'Special').
card_artist('bloodlord of vaasgoth'/'pPRE', 'Min Yum').
card_number('bloodlord of vaasgoth'/'pPRE', '50').

card_in_set('bloodsoaked champion', 'pPRE').
card_original_type('bloodsoaked champion'/'pPRE', 'Creature — Human Warrior').
card_original_text('bloodsoaked champion'/'pPRE', '').
card_first_print('bloodsoaked champion', 'pPRE').
card_image_name('bloodsoaked champion'/'pPRE', 'bloodsoaked champion').
card_uid('bloodsoaked champion'/'pPRE', 'pPRE:Bloodsoaked Champion:bloodsoaked champion').
card_rarity('bloodsoaked champion'/'pPRE', 'Special').
card_artist('bloodsoaked champion'/'pPRE', 'Aaron Miller').
card_number('bloodsoaked champion'/'pPRE', '92').
card_flavor_text('bloodsoaked champion'/'pPRE', '\"Death is merely another foe the Mardu will overcome.\"').

card_in_set('butcher of the horde', 'pPRE').
card_original_type('butcher of the horde'/'pPRE', 'Creature — Demon').
card_original_text('butcher of the horde'/'pPRE', '').
card_first_print('butcher of the horde', 'pPRE').
card_image_name('butcher of the horde'/'pPRE', 'butcher of the horde').
card_uid('butcher of the horde'/'pPRE', 'pPRE:Butcher of the Horde:butcher of the horde').
card_rarity('butcher of the horde'/'pPRE', 'Special').
card_artist('butcher of the horde'/'pPRE', 'Karl Kopinski').
card_number('butcher of the horde'/'pPRE', '93').

card_in_set('carnival hellsteed', 'pPRE').
card_original_type('carnival hellsteed'/'pPRE', 'Creature — Nightmare Horse').
card_original_text('carnival hellsteed'/'pPRE', '').
card_first_print('carnival hellsteed', 'pPRE').
card_image_name('carnival hellsteed'/'pPRE', 'carnival hellsteed').
card_uid('carnival hellsteed'/'pPRE', 'pPRE:Carnival Hellsteed:carnival hellsteed').
card_rarity('carnival hellsteed'/'pPRE', 'Special').
card_artist('carnival hellsteed'/'pPRE', 'Min Yum').
card_number('carnival hellsteed'/'pPRE', '57').
card_flavor_text('carnival hellsteed'/'pPRE', 'Its favorite treats are candied hands and sweet hearts.').

card_in_set('celestial archon', 'pPRE').
card_original_type('celestial archon'/'pPRE', 'Enchantment Creature — Archon').
card_original_text('celestial archon'/'pPRE', '').
card_first_print('celestial archon', 'pPRE').
card_image_name('celestial archon'/'pPRE', 'celestial archon').
card_uid('celestial archon'/'pPRE', 'pPRE:Celestial Archon:celestial archon').
card_rarity('celestial archon'/'pPRE', 'Special').
card_artist('celestial archon'/'pPRE', 'Wesley Burt').
card_number('celestial archon'/'pPRE', '68').

card_in_set('comet storm', 'pPRE').
card_original_type('comet storm'/'pPRE', 'Instant').
card_original_text('comet storm'/'pPRE', '').
card_first_print('comet storm', 'pPRE').
card_image_name('comet storm'/'pPRE', 'comet storm').
card_uid('comet storm'/'pPRE', 'pPRE:Comet Storm:comet storm').
card_rarity('comet storm'/'pPRE', 'Special').
card_artist('comet storm'/'pPRE', 'Tomasz Jedruszek').
card_number('comet storm'/'pPRE', '43').

card_in_set('consuming aberration', 'pPRE').
card_original_type('consuming aberration'/'pPRE', 'Creature — Horror').
card_original_text('consuming aberration'/'pPRE', '').
card_first_print('consuming aberration', 'pPRE').
card_image_name('consuming aberration'/'pPRE', 'consuming aberration').
card_uid('consuming aberration'/'pPRE', 'pPRE:Consuming Aberration:consuming aberration').
card_rarity('consuming aberration'/'pPRE', 'Special').
card_artist('consuming aberration'/'pPRE', 'Volkan Baga').
card_number('consuming aberration'/'pPRE', '60').

card_in_set('corpsejack menace', 'pPRE').
card_original_type('corpsejack menace'/'pPRE', 'Creature — Fungus').
card_original_text('corpsejack menace'/'pPRE', '').
card_first_print('corpsejack menace', 'pPRE').
card_image_name('corpsejack menace'/'pPRE', 'corpsejack menace').
card_uid('corpsejack menace'/'pPRE', 'pPRE:Corpsejack Menace:corpsejack menace').
card_rarity('corpsejack menace'/'pPRE', 'Special').
card_artist('corpsejack menace'/'pPRE', 'Vincent Proce').
card_number('corpsejack menace'/'pPRE', '58').
card_flavor_text('corpsejack menace'/'pPRE', 'Weakness is not in the nature of the Swarm.').

card_in_set('crackling doom', 'pPRE').
card_original_type('crackling doom'/'pPRE', 'Instant').
card_original_text('crackling doom'/'pPRE', '').
card_first_print('crackling doom', 'pPRE').
card_image_name('crackling doom'/'pPRE', 'crackling doom').
card_uid('crackling doom'/'pPRE', 'pPRE:Crackling Doom:crackling doom').
card_rarity('crackling doom'/'pPRE', 'Special').
card_artist('crackling doom'/'pPRE', 'Yohann Schepacz').
card_number('crackling doom'/'pPRE', '94').
card_flavor_text('crackling doom'/'pPRE', 'Do not fear the lightning. Fear the one it obeys.').

card_in_set('crater\'s claws', 'pPRE').
card_original_type('crater\'s claws'/'pPRE', 'Sorcery').
card_original_text('crater\'s claws'/'pPRE', '').
card_first_print('crater\'s claws', 'pPRE').
card_image_name('crater\'s claws'/'pPRE', 'crater\'s claws').
card_uid('crater\'s claws'/'pPRE', 'pPRE:Crater\'s Claws:crater\'s claws').
card_rarity('crater\'s claws'/'pPRE', 'Special').
card_artist('crater\'s claws'/'pPRE', 'Noah Bradley').
card_number('crater\'s claws'/'pPRE', '95').

card_in_set('dawnbringer charioteers', 'pPRE').
card_original_type('dawnbringer charioteers'/'pPRE', 'Creature — Human Soldier').
card_original_text('dawnbringer charioteers'/'pPRE', '').
card_first_print('dawnbringer charioteers', 'pPRE').
card_image_name('dawnbringer charioteers'/'pPRE', 'dawnbringer charioteers').
card_uid('dawnbringer charioteers'/'pPRE', 'pPRE:Dawnbringer Charioteers:dawnbringer charioteers').
card_rarity('dawnbringer charioteers'/'pPRE', 'Special').
card_artist('dawnbringer charioteers'/'pPRE', 'Volkan Baga').
card_number('dawnbringer charioteers'/'pPRE', '78').
card_flavor_text('dawnbringer charioteers'/'pPRE', '\"Nyx may belong to the gods, but the skies of Theros are ours.\"').

card_in_set('deflecting palm', 'pPRE').
card_original_type('deflecting palm'/'pPRE', 'Instant').
card_original_text('deflecting palm'/'pPRE', '').
card_first_print('deflecting palm', 'pPRE').
card_image_name('deflecting palm'/'pPRE', 'deflecting palm').
card_uid('deflecting palm'/'pPRE', 'pPRE:Deflecting Palm:deflecting palm').
card_rarity('deflecting palm'/'pPRE', 'Special').
card_artist('deflecting palm'/'pPRE', 'Eric Deschamps').
card_number('deflecting palm'/'pPRE', '96').

card_in_set('demigod of revenge', 'pPRE').
card_original_type('demigod of revenge'/'pPRE', 'Creature — Spirit Avatar').
card_original_text('demigod of revenge'/'pPRE', '').
card_first_print('demigod of revenge', 'pPRE').
card_image_name('demigod of revenge'/'pPRE', 'demigod of revenge').
card_uid('demigod of revenge'/'pPRE', 'pPRE:Demigod of Revenge:demigod of revenge').
card_rarity('demigod of revenge'/'pPRE', 'Special').
card_artist('demigod of revenge'/'pPRE', 'Izzy').
card_number('demigod of revenge'/'pPRE', '36').
card_flavor_text('demigod of revenge'/'pPRE', '\"His laugh, a bellowing, deathly din, slices through the heavens, making them bleed.\"\n—The Seer\'s Parables').

card_in_set('dig through time', 'pPRE').
card_original_type('dig through time'/'pPRE', 'Instant').
card_original_text('dig through time'/'pPRE', '').
card_first_print('dig through time', 'pPRE').
card_image_name('dig through time'/'pPRE', 'dig through time').
card_uid('dig through time'/'pPRE', 'pPRE:Dig Through Time:dig through time').
card_rarity('dig through time'/'pPRE', 'Special').
card_artist('dig through time'/'pPRE', 'Ryan Yee').
card_number('dig through time'/'pPRE', '97').

card_in_set('dirtcowl wurm', 'pPRE').
card_original_type('dirtcowl wurm'/'pPRE', 'Creature — Wurm').
card_original_text('dirtcowl wurm'/'pPRE', '').
card_first_print('dirtcowl wurm', 'pPRE').
card_image_name('dirtcowl wurm'/'pPRE', 'dirtcowl wurm').
card_uid('dirtcowl wurm'/'pPRE', 'pPRE:Dirtcowl Wurm:dirtcowl wurm').
card_rarity('dirtcowl wurm'/'pPRE', 'Special').
card_artist('dirtcowl wurm'/'pPRE', 'Dan Frazier').
card_number('dirtcowl wurm'/'pPRE', '1').
card_flavor_text('dirtcowl wurm'/'pPRE', 'Few of Rath\'s rivers follow a steady course, so many new channels are carved for them.').

card_in_set('djinn illuminatus', 'pPRE').
card_original_type('djinn illuminatus'/'pPRE', 'Creature — Djinn').
card_original_text('djinn illuminatus'/'pPRE', '').
card_first_print('djinn illuminatus', 'pPRE').
card_image_name('djinn illuminatus'/'pPRE', 'djinn illuminatus').
card_uid('djinn illuminatus'/'pPRE', 'pPRE:Djinn Illuminatus:djinn illuminatus').
card_rarity('djinn illuminatus'/'pPRE', 'Special').
card_artist('djinn illuminatus'/'pPRE', 'Michael Sutfin').
card_number('djinn illuminatus'/'pPRE', '28').

card_in_set('doomwake giant', 'pPRE').
card_original_type('doomwake giant'/'pPRE', 'Enchantment Creature — Giant').
card_original_text('doomwake giant'/'pPRE', '').
card_first_print('doomwake giant', 'pPRE').
card_image_name('doomwake giant'/'pPRE', 'doomwake giant').
card_uid('doomwake giant'/'pPRE', 'pPRE:Doomwake Giant:doomwake giant').
card_rarity('doomwake giant'/'pPRE', 'Special').
card_artist('doomwake giant'/'pPRE', 'Nils Hamm').
card_number('doomwake giant'/'pPRE', '80').
card_flavor_text('doomwake giant'/'pPRE', 'Crippling fear precedes it. A foul stench accompanies it. Death follows in its wake.').

card_in_set('door of destinies', 'pPRE').
card_original_type('door of destinies'/'pPRE', 'Artifact').
card_original_text('door of destinies'/'pPRE', '').
card_first_print('door of destinies', 'pPRE').
card_image_name('door of destinies'/'pPRE', 'door of destinies').
card_uid('door of destinies'/'pPRE', 'pPRE:Door of Destinies:door of destinies').
card_rarity('door of destinies'/'pPRE', 'Special').
card_artist('door of destinies'/'pPRE', 'Rob Alexander').
card_number('door of destinies'/'pPRE', '35').

card_in_set('dragon broodmother', 'pPRE').
card_original_type('dragon broodmother'/'pPRE', 'Creature — Dragon').
card_original_text('dragon broodmother'/'pPRE', '').
card_first_print('dragon broodmother', 'pPRE').
card_image_name('dragon broodmother'/'pPRE', 'dragon broodmother').
card_uid('dragon broodmother'/'pPRE', 'pPRE:Dragon Broodmother:dragon broodmother').
card_rarity('dragon broodmother'/'pPRE', 'Special').
card_artist('dragon broodmother'/'pPRE', 'Jean-Sébastien Rossbach').
card_number('dragon broodmother'/'pPRE', '40').

card_in_set('dragon-style twins', 'pPRE').
card_original_type('dragon-style twins'/'pPRE', 'Creature — Human Monk').
card_original_text('dragon-style twins'/'pPRE', '').
card_first_print('dragon-style twins', 'pPRE').
card_image_name('dragon-style twins'/'pPRE', 'dragon-style twins').
card_uid('dragon-style twins'/'pPRE', 'pPRE:Dragon-Style Twins:dragon-style twins').
card_rarity('dragon-style twins'/'pPRE', 'Special').
card_artist('dragon-style twins'/'pPRE', 'Wesley Burt').
card_number('dragon-style twins'/'pPRE', '98').
card_flavor_text('dragon-style twins'/'pPRE', '\"We are the flicker of the flame and its heat, the two sides of a single blade.\"').

card_in_set('duneblast', 'pPRE').
card_original_type('duneblast'/'pPRE', 'Sorcery').
card_original_text('duneblast'/'pPRE', '').
card_first_print('duneblast', 'pPRE').
card_image_name('duneblast'/'pPRE', 'duneblast').
card_uid('duneblast'/'pPRE', 'pPRE:Duneblast:duneblast').
card_rarity('duneblast'/'pPRE', 'Special').
card_artist('duneblast'/'pPRE', 'Ryan Alexander Lee').
card_number('duneblast'/'pPRE', '99').
card_flavor_text('duneblast'/'pPRE', 'The Abzan turn to this spell only as a last resort, for its inevitable result is what they most dread: to be alone.').

card_in_set('eater of hope', 'pPRE').
card_original_type('eater of hope'/'pPRE', 'Creature — Demon').
card_original_text('eater of hope'/'pPRE', '').
card_first_print('eater of hope', 'pPRE').
card_image_name('eater of hope'/'pPRE', 'eater of hope').
card_uid('eater of hope'/'pPRE', 'pPRE:Eater of Hope:eater of hope').
card_rarity('eater of hope'/'pPRE', 'Special').
card_artist('eater of hope'/'pPRE', 'Mathias Kollros').
card_number('eater of hope'/'pPRE', '75').
card_flavor_text('eater of hope'/'pPRE', 'Gods can be appeased. Demons, however . . .').

card_in_set('ember swallower', 'pPRE').
card_original_type('ember swallower'/'pPRE', 'Creature — Elemental').
card_original_text('ember swallower'/'pPRE', '').
card_first_print('ember swallower', 'pPRE').
card_image_name('ember swallower'/'pPRE', 'ember swallower').
card_uid('ember swallower'/'pPRE', 'pPRE:Ember Swallower:ember swallower').
card_rarity('ember swallower'/'pPRE', 'Special').
card_artist('ember swallower'/'pPRE', 'Vincent Proce').
card_number('ember swallower'/'pPRE', '71').

card_in_set('emrakul, the aeons torn', 'pPRE').
card_original_type('emrakul, the aeons torn'/'pPRE', 'Legendary Creature — Eldrazi').
card_original_text('emrakul, the aeons torn'/'pPRE', '').
card_first_print('emrakul, the aeons torn', 'pPRE').
card_image_name('emrakul, the aeons torn'/'pPRE', 'emrakul, the aeons torn').
card_uid('emrakul, the aeons torn'/'pPRE', 'pPRE:Emrakul, the Aeons Torn:emrakul, the aeons torn').
card_rarity('emrakul, the aeons torn'/'pPRE', 'Special').
card_artist('emrakul, the aeons torn'/'pPRE', 'Justin Sweet').
card_number('emrakul, the aeons torn'/'pPRE', '44').

card_in_set('false prophet', 'pPRE').
card_original_type('false prophet'/'pPRE', 'Creature — Human Cleric').
card_original_text('false prophet'/'pPRE', '').
card_first_print('false prophet', 'pPRE').
card_image_name('false prophet'/'pPRE', 'false prophet').
card_uid('false prophet'/'pPRE', 'pPRE:False Prophet:false prophet').
card_rarity('false prophet'/'pPRE', 'Special').
card_artist('false prophet'/'pPRE', 'Eric Peterson').
card_number('false prophet'/'pPRE', '7').
card_flavor_text('false prophet'/'pPRE', '\"You lived for Serra\'s love. Will you not die for it?\"').

card_in_set('fathom mage', 'pPRE').
card_original_type('fathom mage'/'pPRE', 'Creature — Human Wizard').
card_original_text('fathom mage'/'pPRE', '').
card_first_print('fathom mage', 'pPRE').
card_image_name('fathom mage'/'pPRE', 'fathom mage').
card_uid('fathom mage'/'pPRE', 'pPRE:Fathom Mage:fathom mage').
card_rarity('fathom mage'/'pPRE', 'Special').
card_artist('fathom mage'/'pPRE', 'Dan Scott').
card_number('fathom mage'/'pPRE', '61').

card_in_set('feral throwback', 'pPRE').
card_original_type('feral throwback'/'pPRE', 'Creature — Beast').
card_original_text('feral throwback'/'pPRE', '').
card_first_print('feral throwback', 'pPRE').
card_image_name('feral throwback'/'pPRE', 'feral throwback').
card_uid('feral throwback'/'pPRE', 'pPRE:Feral Throwback:feral throwback').
card_rarity('feral throwback'/'pPRE', 'Special').
card_artist('feral throwback'/'pPRE', 'Carl Critchlow').
card_number('feral throwback'/'pPRE', '19').

card_in_set('flying crane technique', 'pPRE').
card_original_type('flying crane technique'/'pPRE', 'Instant').
card_original_text('flying crane technique'/'pPRE', '').
card_first_print('flying crane technique', 'pPRE').
card_image_name('flying crane technique'/'pPRE', 'flying crane technique').
card_uid('flying crane technique'/'pPRE', 'pPRE:Flying Crane Technique:flying crane technique').
card_rarity('flying crane technique'/'pPRE', 'Special').
card_artist('flying crane technique'/'pPRE', 'Jack Wang').
card_number('flying crane technique'/'pPRE', '100').
card_flavor_text('flying crane technique'/'pPRE', 'There are many Jeskai styles: Riverwalk imitates flowing water, Dragonfist the ancient hellkites, and Flying Crane the wild aven of the high peaks.').

card_in_set('forgestoker dragon', 'pPRE').
card_original_type('forgestoker dragon'/'pPRE', 'Creature — Dragon').
card_original_text('forgestoker dragon'/'pPRE', '').
card_first_print('forgestoker dragon', 'pPRE').
card_image_name('forgestoker dragon'/'pPRE', 'forgestoker dragon').
card_uid('forgestoker dragon'/'pPRE', 'pPRE:Forgestoker Dragon:forgestoker dragon').
card_rarity('forgestoker dragon'/'pPRE', 'Special').
card_artist('forgestoker dragon'/'pPRE', 'Lucas Graciano').
card_number('forgestoker dragon'/'pPRE', '76').
card_flavor_text('forgestoker dragon'/'pPRE', 'The Akroans fashion their helms to honor the dragons, not to protect against them, as that would be of little help.').

card_in_set('foundry champion', 'pPRE').
card_original_type('foundry champion'/'pPRE', 'Creature — Elemental Soldier').
card_original_text('foundry champion'/'pPRE', '').
card_first_print('foundry champion', 'pPRE').
card_image_name('foundry champion'/'pPRE', 'foundry champion').
card_uid('foundry champion'/'pPRE', 'pPRE:Foundry Champion:foundry champion').
card_rarity('foundry champion'/'pPRE', 'Special').
card_artist('foundry champion'/'pPRE', 'James Ryman').
card_number('foundry champion'/'pPRE', '62').

card_in_set('fungal shambler', 'pPRE').
card_original_type('fungal shambler'/'pPRE', 'Creature — Fungus Beast').
card_original_text('fungal shambler'/'pPRE', '').
card_first_print('fungal shambler', 'pPRE').
card_image_name('fungal shambler'/'pPRE', 'fungal shambler').
card_uid('fungal shambler'/'pPRE', 'pPRE:Fungal Shambler:fungal shambler').
card_rarity('fungal shambler'/'pPRE', 'Special').
card_artist('fungal shambler'/'pPRE', 'Jim Nelson').
card_number('fungal shambler'/'pPRE', '14').

card_in_set('gleancrawler', 'pPRE').
card_original_type('gleancrawler'/'pPRE', 'Creature — Insect Horror').
card_original_text('gleancrawler'/'pPRE', '').
card_first_print('gleancrawler', 'pPRE').
card_image_name('gleancrawler'/'pPRE', 'gleancrawler').
card_uid('gleancrawler'/'pPRE', 'pPRE:Gleancrawler:gleancrawler').
card_rarity('gleancrawler'/'pPRE', 'Special').
card_artist('gleancrawler'/'pPRE', 'Jeremy Jarvis').
card_number('gleancrawler'/'pPRE', '27').

card_in_set('glissa, the traitor', 'pPRE').
card_original_type('glissa, the traitor'/'pPRE', 'Legendary Creature — Zombie Elf').
card_original_text('glissa, the traitor'/'pPRE', '').
card_first_print('glissa, the traitor', 'pPRE').
card_image_name('glissa, the traitor'/'pPRE', 'glissa, the traitor').
card_uid('glissa, the traitor'/'pPRE', 'pPRE:Glissa, the Traitor:glissa, the traitor').
card_rarity('glissa, the traitor'/'pPRE', 'Special').
card_artist('glissa, the traitor'/'pPRE', 'Steve Argyle').
card_number('glissa, the traitor'/'pPRE', '48').

card_in_set('glory', 'pPRE').
card_original_type('glory'/'pPRE', 'Creature — Incarnation').
card_original_text('glory'/'pPRE', '').
card_first_print('glory', 'pPRE').
card_image_name('glory'/'pPRE', 'glory').
card_uid('glory'/'pPRE', 'pPRE:Glory:glory').
card_rarity('glory'/'pPRE', 'Special').
card_artist('glory'/'pPRE', 'Donato Giancola').
card_number('glory'/'pPRE', '17').
card_flavor_text('glory'/'pPRE', '\"Glory was gone; Glory was everywhere.\"\n—Scroll of Beginnings').

card_in_set('grim haruspex', 'pPRE').
card_original_type('grim haruspex'/'pPRE', 'Creature — Human Wizard').
card_original_text('grim haruspex'/'pPRE', '').
card_first_print('grim haruspex', 'pPRE').
card_image_name('grim haruspex'/'pPRE', 'grim haruspex').
card_uid('grim haruspex'/'pPRE', 'pPRE:Grim Haruspex:grim haruspex').
card_rarity('grim haruspex'/'pPRE', 'Special').
card_artist('grim haruspex'/'pPRE', 'Seb McKinnon').
card_number('grim haruspex'/'pPRE', '101').
card_flavor_text('grim haruspex'/'pPRE', '\"We all want to know what\'s going on in someone else\'s head. I simply open it up and look.\"').

card_in_set('grove of the guardian', 'pPRE').
card_original_type('grove of the guardian'/'pPRE', 'Land').
card_original_text('grove of the guardian'/'pPRE', '').
card_first_print('grove of the guardian', 'pPRE').
card_image_name('grove of the guardian'/'pPRE', 'grove of the guardian').
card_uid('grove of the guardian'/'pPRE', 'pPRE:Grove of the Guardian:grove of the guardian').
card_rarity('grove of the guardian'/'pPRE', 'Special').
card_artist('grove of the guardian'/'pPRE', 'Yeong-Hao Han').
card_number('grove of the guardian'/'pPRE', '59').

card_in_set('hardened scales', 'pPRE').
card_original_type('hardened scales'/'pPRE', 'Enchantment').
card_original_text('hardened scales'/'pPRE', '').
card_first_print('hardened scales', 'pPRE').
card_image_name('hardened scales'/'pPRE', 'hardened scales').
card_uid('hardened scales'/'pPRE', 'pPRE:Hardened Scales:hardened scales').
card_rarity('hardened scales'/'pPRE', 'Special').
card_artist('hardened scales'/'pPRE', 'Mark Winters').
card_number('hardened scales'/'pPRE', '102').
card_flavor_text('hardened scales'/'pPRE', '\"Naga shed their scales. We wear ours with pride.\"\n—Golran, dragonscale captain').

card_in_set('helm of kaldra', 'pPRE').
card_original_type('helm of kaldra'/'pPRE', 'Legendary Artifact — Equipment').
card_original_text('helm of kaldra'/'pPRE', '').
card_first_print('helm of kaldra', 'pPRE').
card_image_name('helm of kaldra'/'pPRE', 'helm of kaldra').
card_uid('helm of kaldra'/'pPRE', 'pPRE:Helm of Kaldra:helm of kaldra').
card_rarity('helm of kaldra'/'pPRE', 'Special').
card_artist('helm of kaldra'/'pPRE', 'Donato Giancola').
card_number('helm of kaldra'/'pPRE', '23').

card_in_set('herald of anafenza', 'pPRE').
card_original_type('herald of anafenza'/'pPRE', 'Creature — Human Soldier').
card_original_text('herald of anafenza'/'pPRE', '').
card_first_print('herald of anafenza', 'pPRE').
card_image_name('herald of anafenza'/'pPRE', 'herald of anafenza').
card_uid('herald of anafenza'/'pPRE', 'pPRE:Herald of Anafenza:herald of anafenza').
card_rarity('herald of anafenza'/'pPRE', 'Special').
card_artist('herald of anafenza'/'pPRE', 'Aaron Miller').
card_number('herald of anafenza'/'pPRE', '103').

card_in_set('hero of bladehold', 'pPRE').
card_original_type('hero of bladehold'/'pPRE', 'Creature — Human Knight').
card_original_text('hero of bladehold'/'pPRE', '').
card_first_print('hero of bladehold', 'pPRE').
card_image_name('hero of bladehold'/'pPRE', 'hero of bladehold').
card_uid('hero of bladehold'/'pPRE', 'pPRE:Hero of Bladehold:hero of bladehold').
card_rarity('hero of bladehold'/'pPRE', 'Special').
card_artist('hero of bladehold'/'pPRE', 'Scott Chou').
card_number('hero of bladehold'/'pPRE', '47').

card_in_set('heroes\' bane', 'pPRE').
card_original_type('heroes\' bane'/'pPRE', 'Creature — Hydra').
card_original_text('heroes\' bane'/'pPRE', '').
card_first_print('heroes\' bane', 'pPRE').
card_image_name('heroes\' bane'/'pPRE', 'heroes\' bane').
card_uid('heroes\' bane'/'pPRE', 'pPRE:Heroes\' Bane:heroes\' bane').
card_rarity('heroes\' bane'/'pPRE', 'Special').
card_artist('heroes\' bane'/'pPRE', 'Volkan Baga').
card_number('heroes\' bane'/'pPRE', '82').
card_flavor_text('heroes\' bane'/'pPRE', 'Travelers\' tales claim that hydras bite off their own heads to make themselves deadlier.').

card_in_set('high sentinels of arashin', 'pPRE').
card_original_type('high sentinels of arashin'/'pPRE', 'Creature — Bird Soldier').
card_original_text('high sentinels of arashin'/'pPRE', '').
card_first_print('high sentinels of arashin', 'pPRE').
card_image_name('high sentinels of arashin'/'pPRE', 'high sentinels of arashin').
card_uid('high sentinels of arashin'/'pPRE', 'pPRE:High Sentinels of Arashin:high sentinels of arashin').
card_rarity('high sentinels of arashin'/'pPRE', 'Special').
card_artist('high sentinels of arashin'/'pPRE', 'James Ryman').
card_number('high sentinels of arashin'/'pPRE', '104').

card_in_set('howlpack alpha', 'pPRE').
card_original_type('howlpack alpha'/'pPRE', 'Creature — Werewolf').
card_original_text('howlpack alpha'/'pPRE', '').
card_first_print('howlpack alpha', 'pPRE').
card_image_name('howlpack alpha'/'pPRE', 'howlpack alpha').
card_uid('howlpack alpha'/'pPRE', 'pPRE:Howlpack Alpha:howlpack alpha').
card_rarity('howlpack alpha'/'pPRE', 'Special').
card_artist('howlpack alpha'/'pPRE', 'Ryan Pancoast').
card_number('howlpack alpha'/'pPRE', '51b').

card_in_set('hypersonic dragon', 'pPRE').
card_original_type('hypersonic dragon'/'pPRE', 'Creature — Dragon').
card_original_text('hypersonic dragon'/'pPRE', '').
card_first_print('hypersonic dragon', 'pPRE').
card_image_name('hypersonic dragon'/'pPRE', 'hypersonic dragon').
card_uid('hypersonic dragon'/'pPRE', 'pPRE:Hypersonic Dragon:hypersonic dragon').
card_rarity('hypersonic dragon'/'pPRE', 'Special').
card_artist('hypersonic dragon'/'pPRE', 'Volkan Baga').
card_number('hypersonic dragon'/'pPRE', '56').
card_flavor_text('hypersonic dragon'/'pPRE', '\"Even this primitive specimen of my kind is impressive.\"\n—Niv-Mizzet').

card_in_set('icy blast', 'pPRE').
card_original_type('icy blast'/'pPRE', 'Instant').
card_original_text('icy blast'/'pPRE', '').
card_first_print('icy blast', 'pPRE').
card_image_name('icy blast'/'pPRE', 'icy blast').
card_uid('icy blast'/'pPRE', 'pPRE:Icy Blast:icy blast').
card_rarity('icy blast'/'pPRE', 'Special').
card_artist('icy blast'/'pPRE', 'Eric Deschamps').
card_number('icy blast'/'pPRE', '105').
card_flavor_text('icy blast'/'pPRE', '\"Do not think the sand or the sun will hold back the breath of winter.\"').

card_in_set('indulgent tormentor', 'pPRE').
card_original_type('indulgent tormentor'/'pPRE', 'Creature — Demon').
card_original_text('indulgent tormentor'/'pPRE', '').
card_first_print('indulgent tormentor', 'pPRE').
card_image_name('indulgent tormentor'/'pPRE', 'indulgent tormentor').
card_uid('indulgent tormentor'/'pPRE', 'pPRE:Indulgent Tormentor:indulgent tormentor').
card_rarity('indulgent tormentor'/'pPRE', 'Special').
card_artist('indulgent tormentor'/'pPRE', 'Karl Kopinski').
card_number('indulgent tormentor'/'pPRE', '85').
card_flavor_text('indulgent tormentor'/'pPRE', 'The promise of anguish is payment enough for services rendered.').

card_in_set('ink-eyes, servant of oni', 'pPRE').
card_original_type('ink-eyes, servant of oni'/'pPRE', 'Legendary Creature — Rat Ninja').
card_original_text('ink-eyes, servant of oni'/'pPRE', '').
card_first_print('ink-eyes, servant of oni', 'pPRE').
card_image_name('ink-eyes, servant of oni'/'pPRE', 'ink-eyes, servant of oni').
card_uid('ink-eyes, servant of oni'/'pPRE', 'pPRE:Ink-Eyes, Servant of Oni:ink-eyes, servant of oni').
card_rarity('ink-eyes, servant of oni'/'pPRE', 'Special').
card_artist('ink-eyes, servant of oni'/'pPRE', 'Ron Spears').
card_number('ink-eyes, servant of oni'/'pPRE', '25').

card_in_set('ivorytusk fortress', 'pPRE').
card_original_type('ivorytusk fortress'/'pPRE', 'Creature — Elephant').
card_original_text('ivorytusk fortress'/'pPRE', '').
card_image_name('ivorytusk fortress'/'pPRE', 'ivorytusk fortress').
card_uid('ivorytusk fortress'/'pPRE', 'pPRE:Ivorytusk Fortress:ivorytusk fortress').
card_rarity('ivorytusk fortress'/'pPRE', 'Special').
card_artist('ivorytusk fortress'/'pPRE', 'Jasper Sandner').
card_number('ivorytusk fortress'/'pPRE', '106').
card_flavor_text('ivorytusk fortress'/'pPRE', 'Abzan soldiers march to war confident that their Houses march with them.').

card_in_set('jeering instigator', 'pPRE').
card_original_type('jeering instigator'/'pPRE', 'Creature — Goblin Rogue').
card_original_text('jeering instigator'/'pPRE', '').
card_first_print('jeering instigator', 'pPRE').
card_image_name('jeering instigator'/'pPRE', 'jeering instigator').
card_uid('jeering instigator'/'pPRE', 'pPRE:Jeering Instigator:jeering instigator').
card_rarity('jeering instigator'/'pPRE', 'Special').
card_artist('jeering instigator'/'pPRE', 'William Murai').
card_number('jeering instigator'/'pPRE', '107').

card_in_set('jeskai ascendancy', 'pPRE').
card_original_type('jeskai ascendancy'/'pPRE', 'Enchantment').
card_original_text('jeskai ascendancy'/'pPRE', '').
card_first_print('jeskai ascendancy', 'pPRE').
card_image_name('jeskai ascendancy'/'pPRE', 'jeskai ascendancy').
card_uid('jeskai ascendancy'/'pPRE', 'pPRE:Jeskai Ascendancy:jeskai ascendancy').
card_rarity('jeskai ascendancy'/'pPRE', 'Special').
card_artist('jeskai ascendancy'/'pPRE', 'Dan Scott').
card_number('jeskai ascendancy'/'pPRE', '108').

card_in_set('kheru lich lord', 'pPRE').
card_original_type('kheru lich lord'/'pPRE', 'Creature — Zombie Wizard').
card_original_text('kheru lich lord'/'pPRE', '').
card_first_print('kheru lich lord', 'pPRE').
card_image_name('kheru lich lord'/'pPRE', 'kheru lich lord').
card_uid('kheru lich lord'/'pPRE', 'pPRE:Kheru Lich Lord:kheru lich lord').
card_rarity('kheru lich lord'/'pPRE', 'Special').
card_artist('kheru lich lord'/'pPRE', 'Karl Kopinski').
card_number('kheru lich lord'/'pPRE', '109').

card_in_set('kiyomaro, first to stand', 'pPRE').
card_original_type('kiyomaro, first to stand'/'pPRE', 'Legendary Creature — Spirit').
card_original_text('kiyomaro, first to stand'/'pPRE', '').
card_first_print('kiyomaro, first to stand', 'pPRE').
card_image_name('kiyomaro, first to stand'/'pPRE', 'kiyomaro, first to stand').
card_uid('kiyomaro, first to stand'/'pPRE', 'pPRE:Kiyomaro, First to Stand:kiyomaro, first to stand').
card_rarity('kiyomaro, first to stand'/'pPRE', 'Special').
card_artist('kiyomaro, first to stand'/'pPRE', 'Ittoku').
card_number('kiyomaro, first to stand'/'pPRE', '26').

card_in_set('korlash, heir to blackblade', 'pPRE').
card_original_type('korlash, heir to blackblade'/'pPRE', 'Legendary Creature — Zombie Warrior').
card_original_text('korlash, heir to blackblade'/'pPRE', '').
card_first_print('korlash, heir to blackblade', 'pPRE').
card_image_name('korlash, heir to blackblade'/'pPRE', 'korlash, heir to blackblade').
card_uid('korlash, heir to blackblade'/'pPRE', 'pPRE:Korlash, Heir to Blackblade:korlash, heir to blackblade').
card_rarity('korlash, heir to blackblade'/'pPRE', 'Special').
card_artist('korlash, heir to blackblade'/'pPRE', 'Richard Kane Ferguson').
card_number('korlash, heir to blackblade'/'pPRE', '33').

card_in_set('laquatus\'s champion', 'pPRE').
card_original_type('laquatus\'s champion'/'pPRE', 'Creature — Nightmare Horror').
card_original_text('laquatus\'s champion'/'pPRE', '').
card_first_print('laquatus\'s champion', 'pPRE').
card_image_name('laquatus\'s champion'/'pPRE', 'laquatus\'s champion').
card_uid('laquatus\'s champion'/'pPRE', 'pPRE:Laquatus\'s Champion:laquatus\'s champion').
card_rarity('laquatus\'s champion'/'pPRE', 'Special').
card_artist('laquatus\'s champion'/'pPRE', 'Greg Staples').
card_number('laquatus\'s champion'/'pPRE', '16').
card_flavor_text('laquatus\'s champion'/'pPRE', 'Chainer\'s dark gift to a darker soul.').

card_in_set('lightning dragon', 'pPRE').
card_original_type('lightning dragon'/'pPRE', 'Creature — Dragon').
card_original_text('lightning dragon'/'pPRE', '').
card_first_print('lightning dragon', 'pPRE').
card_image_name('lightning dragon'/'pPRE', 'lightning dragon').
card_uid('lightning dragon'/'pPRE', 'pPRE:Lightning Dragon:lightning dragon').
card_rarity('lightning dragon'/'pPRE', 'Special').
card_artist('lightning dragon'/'pPRE', 'Ron Spencer').
card_number('lightning dragon'/'pPRE', '4').

card_in_set('lotus bloom', 'pPRE').
card_original_type('lotus bloom'/'pPRE', 'Artifact').
card_original_text('lotus bloom'/'pPRE', '').
card_first_print('lotus bloom', 'pPRE').
card_image_name('lotus bloom'/'pPRE', 'lotus bloom').
card_uid('lotus bloom'/'pPRE', 'pPRE:Lotus Bloom:lotus bloom').
card_rarity('lotus bloom'/'pPRE', 'Special').
card_artist('lotus bloom'/'pPRE', 'Christopher Rush').
card_number('lotus bloom'/'pPRE', '31').

card_in_set('lu bu, master-at-arms', 'pPRE').
card_original_type('lu bu, master-at-arms'/'pPRE', 'Legendary Creature — Human Soldier Warrior').
card_original_text('lu bu, master-at-arms'/'pPRE', '').
card_first_print('lu bu, master-at-arms', 'pPRE').
card_image_name('lu bu, master-at-arms'/'pPRE', 'lu bu, master-at-arms1').
card_uid('lu bu, master-at-arms'/'pPRE', 'pPRE:Lu Bu, Master-at-Arms:lu bu, master-at-arms1').
card_rarity('lu bu, master-at-arms'/'pPRE', 'Special').
card_artist('lu bu, master-at-arms'/'pPRE', 'Gao Jianzhang').
card_number('lu bu, master-at-arms'/'pPRE', '6').
card_flavor_text('lu bu, master-at-arms'/'pPRE', '\"Dong Zhuo\'s man, Lu Bu, warrior without peer, / Far surpassed the champions of his sphere.\"').

card_in_set('lu bu, master-at-arms', 'pPRE').
card_original_type('lu bu, master-at-arms'/'pPRE', 'Legendary Creature — Human Soldier Warrior').
card_original_text('lu bu, master-at-arms'/'pPRE', '').
card_image_name('lu bu, master-at-arms'/'pPRE', 'lu bu, master-at-arms2').
card_uid('lu bu, master-at-arms'/'pPRE', 'pPRE:Lu Bu, Master-at-Arms:lu bu, master-at-arms2').
card_rarity('lu bu, master-at-arms'/'pPRE', 'Special').
card_artist('lu bu, master-at-arms'/'pPRE', 'Gao Jianzhang').
card_number('lu bu, master-at-arms'/'pPRE', '8').
card_flavor_text('lu bu, master-at-arms'/'pPRE', '\"Dong Zhuo\'s man, Lu Bu, warrior without peer, / Far surpassed the champions of his sphere.\"').

card_in_set('malfegor', 'pPRE').
card_original_type('malfegor'/'pPRE', 'Legendary Creature — Demon Dragon').
card_original_text('malfegor'/'pPRE', '').
card_first_print('malfegor', 'pPRE').
card_image_name('malfegor'/'pPRE', 'malfegor').
card_uid('malfegor'/'pPRE', 'pPRE:Malfegor:malfegor').
card_rarity('malfegor'/'pPRE', 'Special').
card_artist('malfegor'/'pPRE', 'Karl Kopinski').
card_number('malfegor'/'pPRE', '39').
card_flavor_text('malfegor'/'pPRE', 'A demon cannot be trusted, and a dragon will not be ruled.').

card_in_set('mardu ascendancy', 'pPRE').
card_original_type('mardu ascendancy'/'pPRE', 'Enchantment').
card_original_text('mardu ascendancy'/'pPRE', '').
card_first_print('mardu ascendancy', 'pPRE').
card_image_name('mardu ascendancy'/'pPRE', 'mardu ascendancy').
card_uid('mardu ascendancy'/'pPRE', 'pPRE:Mardu Ascendancy:mardu ascendancy').
card_rarity('mardu ascendancy'/'pPRE', 'Special').
card_artist('mardu ascendancy'/'pPRE', 'Jason Chan').
card_number('mardu ascendancy'/'pPRE', '110').

card_in_set('master of pearls', 'pPRE').
card_original_type('master of pearls'/'pPRE', 'Creature — Human Monk').
card_original_text('master of pearls'/'pPRE', '').
card_first_print('master of pearls', 'pPRE').
card_image_name('master of pearls'/'pPRE', 'master of pearls').
card_uid('master of pearls'/'pPRE', 'pPRE:Master of Pearls:master of pearls').
card_rarity('master of pearls'/'pPRE', 'Special').
card_artist('master of pearls'/'pPRE', 'David Gaillet').
card_number('master of pearls'/'pPRE', '111').

card_in_set('mayor of avabruck', 'pPRE').
card_original_type('mayor of avabruck'/'pPRE', 'Creature — Human Advisor Werewolf').
card_original_text('mayor of avabruck'/'pPRE', '').
card_first_print('mayor of avabruck', 'pPRE').
card_image_name('mayor of avabruck'/'pPRE', 'mayor of avabruck').
card_uid('mayor of avabruck'/'pPRE', 'pPRE:Mayor of Avabruck:mayor of avabruck').
card_rarity('mayor of avabruck'/'pPRE', 'Special').
card_artist('mayor of avabruck'/'pPRE', 'Ryan Pancoast').
card_number('mayor of avabruck'/'pPRE', '51a').
card_flavor_text('mayor of avabruck'/'pPRE', 'He can deny his true nature for only so long.').

card_in_set('maze\'s end', 'pPRE').
card_original_type('maze\'s end'/'pPRE', 'Land').
card_original_text('maze\'s end'/'pPRE', '').
card_first_print('maze\'s end', 'pPRE').
card_image_name('maze\'s end'/'pPRE', 'maze\'s end').
card_uid('maze\'s end'/'pPRE', 'pPRE:Maze\'s End:maze\'s end').
card_rarity('maze\'s end'/'pPRE', 'Special').
card_artist('maze\'s end'/'pPRE', 'Cliff Childs').
card_number('maze\'s end'/'pPRE', '65').

card_in_set('megantic sliver', 'pPRE').
card_original_type('megantic sliver'/'pPRE', 'Creature — Sliver').
card_original_text('megantic sliver'/'pPRE', '').
card_first_print('megantic sliver', 'pPRE').
card_image_name('megantic sliver'/'pPRE', 'megantic sliver').
card_uid('megantic sliver'/'pPRE', 'pPRE:Megantic Sliver:megantic sliver').
card_rarity('megantic sliver'/'pPRE', 'Special').
card_artist('megantic sliver'/'pPRE', 'Lucas Graciano').
card_number('megantic sliver'/'pPRE', '67').
card_flavor_text('megantic sliver'/'pPRE', 'Even the thrums, the lowliest of slivers, become deadly in its presence.').

card_in_set('mercurial pretender', 'pPRE').
card_original_type('mercurial pretender'/'pPRE', 'Creature — Shapeshifter').
card_original_text('mercurial pretender'/'pPRE', '').
card_first_print('mercurial pretender', 'pPRE').
card_image_name('mercurial pretender'/'pPRE', 'mercurial pretender').
card_uid('mercurial pretender'/'pPRE', 'pPRE:Mercurial Pretender:mercurial pretender').
card_rarity('mercurial pretender'/'pPRE', 'Special').
card_artist('mercurial pretender'/'pPRE', 'Karla Ortiz').
card_number('mercurial pretender'/'pPRE', '84').
card_flavor_text('mercurial pretender'/'pPRE', 'The king went off to find himself. Imagine his terror when he succeeded.').

card_in_set('monstrous hound', 'pPRE').
card_original_type('monstrous hound'/'pPRE', 'Creature — Hound').
card_original_text('monstrous hound'/'pPRE', '').
card_first_print('monstrous hound', 'pPRE').
card_image_name('monstrous hound'/'pPRE', 'monstrous hound').
card_uid('monstrous hound'/'pPRE', 'pPRE:Monstrous Hound:monstrous hound').
card_rarity('monstrous hound'/'pPRE', 'Special').
card_artist('monstrous hound'/'pPRE', 'Dermot Power').
card_number('monstrous hound'/'pPRE', '3').
card_flavor_text('monstrous hound'/'pPRE', '\"Stay.\"').

card_in_set('moonsilver spear', 'pPRE').
card_original_type('moonsilver spear'/'pPRE', 'Artifact — Equipment').
card_original_text('moonsilver spear'/'pPRE', '').
card_first_print('moonsilver spear', 'pPRE').
card_image_name('moonsilver spear'/'pPRE', 'moonsilver spear').
card_uid('moonsilver spear'/'pPRE', 'pPRE:Moonsilver Spear:moonsilver spear').
card_rarity('moonsilver spear'/'pPRE', 'Special').
card_artist('moonsilver spear'/'pPRE', 'Nic Klein').
card_number('moonsilver spear'/'pPRE', '53').

card_in_set('narset, enlightened master', 'pPRE').
card_original_type('narset, enlightened master'/'pPRE', 'Legendary Creature — Human Monk').
card_original_text('narset, enlightened master'/'pPRE', '').
card_first_print('narset, enlightened master', 'pPRE').
card_image_name('narset, enlightened master'/'pPRE', 'narset, enlightened master').
card_uid('narset, enlightened master'/'pPRE', 'pPRE:Narset, Enlightened Master:narset, enlightened master').
card_rarity('narset, enlightened master'/'pPRE', 'Special').
card_artist('narset, enlightened master'/'pPRE', 'Magali Villeneuve').
card_number('narset, enlightened master'/'pPRE', '112').

card_in_set('necropolis fiend', 'pPRE').
card_original_type('necropolis fiend'/'pPRE', 'Creature — Demon').
card_original_text('necropolis fiend'/'pPRE', '').
card_first_print('necropolis fiend', 'pPRE').
card_image_name('necropolis fiend'/'pPRE', 'necropolis fiend').
card_uid('necropolis fiend'/'pPRE', 'pPRE:Necropolis Fiend:necropolis fiend').
card_rarity('necropolis fiend'/'pPRE', 'Special').
card_artist('necropolis fiend'/'pPRE', 'Seb McKinnon').
card_number('necropolis fiend'/'pPRE', '113').

card_in_set('nessian wilds ravager', 'pPRE').
card_original_type('nessian wilds ravager'/'pPRE', 'Creature — Hydra').
card_original_text('nessian wilds ravager'/'pPRE', '').
card_first_print('nessian wilds ravager', 'pPRE').
card_image_name('nessian wilds ravager'/'pPRE', 'nessian wilds ravager').
card_uid('nessian wilds ravager'/'pPRE', 'pPRE:Nessian Wilds Ravager:nessian wilds ravager').
card_rarity('nessian wilds ravager'/'pPRE', 'Special').
card_artist('nessian wilds ravager'/'pPRE', 'Kev Walker').
card_number('nessian wilds ravager'/'pPRE', '77').

card_in_set('oros, the avenger', 'pPRE').
card_original_type('oros, the avenger'/'pPRE', 'Legendary Creature — Dragon').
card_original_text('oros, the avenger'/'pPRE', '').
card_first_print('oros, the avenger', 'pPRE').
card_image_name('oros, the avenger'/'pPRE', 'oros, the avenger').
card_uid('oros, the avenger'/'pPRE', 'pPRE:Oros, the Avenger:oros, the avenger').
card_rarity('oros, the avenger'/'pPRE', 'Special').
card_artist('oros, the avenger'/'pPRE', 'Daren Bader').
card_number('oros, the avenger'/'pPRE', '32').

card_in_set('overbeing of myth', 'pPRE').
card_original_type('overbeing of myth'/'pPRE', 'Creature — Spirit Avatar').
card_original_text('overbeing of myth'/'pPRE', '').
card_first_print('overbeing of myth', 'pPRE').
card_image_name('overbeing of myth'/'pPRE', 'overbeing of myth').
card_uid('overbeing of myth'/'pPRE', 'pPRE:Overbeing of Myth:overbeing of myth').
card_rarity('overbeing of myth'/'pPRE', 'Special').
card_artist('overbeing of myth'/'pPRE', 'Zoltan Boros & Gabor Szikszai').
card_number('overbeing of myth'/'pPRE', '37').
card_flavor_text('overbeing of myth'/'pPRE', '\"She walks among us unseen, learning from our imperfections.\"\n—The Seer\'s Parables').

card_in_set('overtaker', 'pPRE').
card_original_type('overtaker'/'pPRE', 'Creature — Merfolk Spellshaper').
card_original_text('overtaker'/'pPRE', '').
card_first_print('overtaker', 'pPRE').
card_image_name('overtaker'/'pPRE', 'overtaker').
card_uid('overtaker'/'pPRE', 'pPRE:Overtaker:overtaker').
card_rarity('overtaker'/'pPRE', 'Special').
card_artist('overtaker'/'pPRE', 'Clyde Caldwell').
card_number('overtaker'/'pPRE', '9').

card_in_set('phytotitan', 'pPRE').
card_original_type('phytotitan'/'pPRE', 'Creature — Plant Elemental').
card_original_text('phytotitan'/'pPRE', '').
card_first_print('phytotitan', 'pPRE').
card_image_name('phytotitan'/'pPRE', 'phytotitan').
card_uid('phytotitan'/'pPRE', 'pPRE:Phytotitan:phytotitan').
card_rarity('phytotitan'/'pPRE', 'Special').
card_artist('phytotitan'/'pPRE', 'Marco Nelor').
card_number('phytotitan'/'pPRE', '87').
card_flavor_text('phytotitan'/'pPRE', 'Its root system spans the entire floor of the jungle, making eradication impossible.').

card_in_set('plains', 'pPRE').
card_original_type('plains'/'pPRE', 'Basic Land — Plains').
card_original_text('plains'/'pPRE', '').
card_image_name('plains'/'pPRE', 'plains').
card_uid('plains'/'pPRE', 'pPRE:Plains:plains').
card_rarity('plains'/'pPRE', 'Basic Land').
card_artist('plains'/'pPRE', 'Eric Deschamps').
card_number('plains'/'pPRE', '66').

card_in_set('questing phelddagrif', 'pPRE').
card_original_type('questing phelddagrif'/'pPRE', 'Creature — Phelddagrif').
card_original_text('questing phelddagrif'/'pPRE', '').
card_first_print('questing phelddagrif', 'pPRE').
card_image_name('questing phelddagrif'/'pPRE', 'questing phelddagrif').
card_uid('questing phelddagrif'/'pPRE', 'pPRE:Questing Phelddagrif:questing phelddagrif').
card_rarity('questing phelddagrif'/'pPRE', 'Special').
card_artist('questing phelddagrif'/'pPRE', 'Matt Cavotta').
card_number('questing phelddagrif'/'pPRE', '13').

card_in_set('raging kavu', 'pPRE').
card_original_type('raging kavu'/'pPRE', 'Creature — Kavu').
card_original_text('raging kavu'/'pPRE', '').
card_first_print('raging kavu', 'pPRE').
card_image_name('raging kavu'/'pPRE', 'raging kavu').
card_uid('raging kavu'/'pPRE', 'pPRE:Raging Kavu:raging kavu').
card_rarity('raging kavu'/'pPRE', 'Special').
card_artist('raging kavu'/'pPRE', 'Arnie Swekel').
card_number('raging kavu'/'pPRE', '12').
card_flavor_text('raging kavu'/'pPRE', 'It took Yavimaya a thousand years to breed them, but it took only seconds for them to prove their worth.').

card_in_set('rakshasa vizier', 'pPRE').
card_original_type('rakshasa vizier'/'pPRE', 'Creature — Cat Demon').
card_original_text('rakshasa vizier'/'pPRE', '').
card_image_name('rakshasa vizier'/'pPRE', 'rakshasa vizier').
card_uid('rakshasa vizier'/'pPRE', 'pPRE:Rakshasa Vizier:rakshasa vizier').
card_rarity('rakshasa vizier'/'pPRE', 'Special').
card_artist('rakshasa vizier'/'pPRE', 'Nils Hamm').
card_number('rakshasa vizier'/'pPRE', '114').
card_flavor_text('rakshasa vizier'/'pPRE', 'Rakshasa offer deals that sound advantageous to those who forget who they are dealing with.').

card_in_set('rampaging baloths', 'pPRE').
card_original_type('rampaging baloths'/'pPRE', 'Creature — Beast').
card_original_text('rampaging baloths'/'pPRE', '').
card_first_print('rampaging baloths', 'pPRE').
card_image_name('rampaging baloths'/'pPRE', 'rampaging baloths').
card_uid('rampaging baloths'/'pPRE', 'pPRE:Rampaging Baloths:rampaging baloths').
card_rarity('rampaging baloths'/'pPRE', 'Special').
card_artist('rampaging baloths'/'pPRE', 'Eric Deschamps').
card_number('rampaging baloths'/'pPRE', '42').
card_flavor_text('rampaging baloths'/'pPRE', '\"When the land is angry, so are they.\"\n—Nissa Revane').

card_in_set('rathi assassin', 'pPRE').
card_original_type('rathi assassin'/'pPRE', 'Creature — Zombie Mercenary Assassin').
card_original_text('rathi assassin'/'pPRE', '').
card_first_print('rathi assassin', 'pPRE').
card_image_name('rathi assassin'/'pPRE', 'rathi assassin').
card_uid('rathi assassin'/'pPRE', 'pPRE:Rathi Assassin:rathi assassin').
card_rarity('rathi assassin'/'pPRE', 'Special').
card_artist('rathi assassin'/'pPRE', 'Dana Knutson').
card_number('rathi assassin'/'pPRE', '10').

card_in_set('rattleclaw mystic', 'pPRE').
card_original_type('rattleclaw mystic'/'pPRE', 'Creature — Human Shaman').
card_original_text('rattleclaw mystic'/'pPRE', '').
card_image_name('rattleclaw mystic'/'pPRE', 'rattleclaw mystic').
card_uid('rattleclaw mystic'/'pPRE', 'pPRE:Rattleclaw Mystic:rattleclaw mystic').
card_rarity('rattleclaw mystic'/'pPRE', 'Special').
card_artist('rattleclaw mystic'/'pPRE', 'Tyler Jacobson').
card_number('rattleclaw mystic'/'pPRE', '115').

card_in_set('ravenous demon', 'pPRE').
card_original_type('ravenous demon'/'pPRE', 'Creature — Demon').
card_original_text('ravenous demon'/'pPRE', '').
card_first_print('ravenous demon', 'pPRE').
card_image_name('ravenous demon'/'pPRE', 'ravenous demon').
card_uid('ravenous demon'/'pPRE', 'pPRE:Ravenous Demon:ravenous demon').
card_rarity('ravenous demon'/'pPRE', 'Special').
card_artist('ravenous demon'/'pPRE', 'Kev Walker').
card_number('ravenous demon'/'pPRE', '52a').
card_flavor_text('ravenous demon'/'pPRE', 'His twisted words entice the weak.').

card_in_set('resolute archangel', 'pPRE').
card_original_type('resolute archangel'/'pPRE', 'Creature — Angel').
card_original_text('resolute archangel'/'pPRE', '').
card_first_print('resolute archangel', 'pPRE').
card_image_name('resolute archangel'/'pPRE', 'resolute archangel').
card_uid('resolute archangel'/'pPRE', 'pPRE:Resolute Archangel:resolute archangel').
card_rarity('resolute archangel'/'pPRE', 'Special').
card_artist('resolute archangel'/'pPRE', 'Winona Nelson').
card_number('resolute archangel'/'pPRE', '83').
card_flavor_text('resolute archangel'/'pPRE', 'Cut it down, bury it in snow, put it to the torch. The rose will still bloom again.').

card_in_set('revenant', 'pPRE').
card_original_type('revenant'/'pPRE', 'Creature — Spirit').
card_original_text('revenant'/'pPRE', '').
card_first_print('revenant', 'pPRE').
card_image_name('revenant'/'pPRE', 'revenant').
card_uid('revenant'/'pPRE', 'pPRE:Revenant:revenant').
card_rarity('revenant'/'pPRE', 'Special').
card_artist('revenant'/'pPRE', 'Terese Nielsen').
card_number('revenant'/'pPRE', '2').
card_flavor_text('revenant'/'pPRE', '\"Not again.\"\n—Hans').

card_in_set('rubblehulk', 'pPRE').
card_original_type('rubblehulk'/'pPRE', 'Creature — Elemental').
card_original_text('rubblehulk'/'pPRE', '').
card_first_print('rubblehulk', 'pPRE').
card_image_name('rubblehulk'/'pPRE', 'rubblehulk').
card_uid('rubblehulk'/'pPRE', 'pPRE:Rubblehulk:rubblehulk').
card_rarity('rubblehulk'/'pPRE', 'Special').
card_artist('rubblehulk'/'pPRE', 'Cliff Childs').
card_number('rubblehulk'/'pPRE', '63').

card_in_set('ryusei, the falling star', 'pPRE').
card_original_type('ryusei, the falling star'/'pPRE', 'Legendary Creature — Dragon Spirit').
card_original_text('ryusei, the falling star'/'pPRE', '').
card_first_print('ryusei, the falling star', 'pPRE').
card_image_name('ryusei, the falling star'/'pPRE', 'ryusei, the falling star').
card_uid('ryusei, the falling star'/'pPRE', 'pPRE:Ryusei, the Falling Star:ryusei, the falling star').
card_rarity('ryusei, the falling star'/'pPRE', 'Special').
card_artist('ryusei, the falling star'/'pPRE', 'Nottsuo').
card_number('ryusei, the falling star'/'pPRE', '24').

card_in_set('sage of the inward eye', 'pPRE').
card_original_type('sage of the inward eye'/'pPRE', 'Creature — Djinn Wizard').
card_original_text('sage of the inward eye'/'pPRE', '').
card_image_name('sage of the inward eye'/'pPRE', 'sage of the inward eye').
card_uid('sage of the inward eye'/'pPRE', 'pPRE:Sage of the Inward Eye:sage of the inward eye').
card_rarity('sage of the inward eye'/'pPRE', 'Special').
card_artist('sage of the inward eye'/'pPRE', 'Chase Stone').
card_number('sage of the inward eye'/'pPRE', '116').
card_flavor_text('sage of the inward eye'/'pPRE', '\"No one petal claims beauty for the lotus.\"').

card_in_set('scourge of fleets', 'pPRE').
card_original_type('scourge of fleets'/'pPRE', 'Creature — Kraken').
card_original_text('scourge of fleets'/'pPRE', '').
card_first_print('scourge of fleets', 'pPRE').
card_image_name('scourge of fleets'/'pPRE', 'scourge of fleets').
card_uid('scourge of fleets'/'pPRE', 'pPRE:Scourge of Fleets:scourge of fleets').
card_rarity('scourge of fleets'/'pPRE', 'Special').
card_artist('scourge of fleets'/'pPRE', 'Adam Paquette').
card_number('scourge of fleets'/'pPRE', '79').

card_in_set('sheoldred, whispering one', 'pPRE').
card_original_type('sheoldred, whispering one'/'pPRE', 'Legendary Creature — Praetor').
card_original_text('sheoldred, whispering one'/'pPRE', '').
card_first_print('sheoldred, whispering one', 'pPRE').
card_image_name('sheoldred, whispering one'/'pPRE', 'sheoldred, whispering one').
card_uid('sheoldred, whispering one'/'pPRE', 'pPRE:Sheoldred, Whispering One:sheoldred, whispering one').
card_rarity('sheoldred, whispering one'/'pPRE', 'Special').
card_artist('sheoldred, whispering one'/'pPRE', 'Jason Felix').
card_number('sheoldred, whispering one'/'pPRE', '49').

card_in_set('shield of kaldra', 'pPRE').
card_original_type('shield of kaldra'/'pPRE', 'Legendary Artifact — Equipment').
card_original_text('shield of kaldra'/'pPRE', '').
card_first_print('shield of kaldra', 'pPRE').
card_image_name('shield of kaldra'/'pPRE', 'shield of kaldra').
card_uid('shield of kaldra'/'pPRE', 'pPRE:Shield of Kaldra:shield of kaldra').
card_rarity('shield of kaldra'/'pPRE', 'Special').
card_artist('shield of kaldra'/'pPRE', 'Donato Giancola').
card_number('shield of kaldra'/'pPRE', '22').

card_in_set('shipbreaker kraken', 'pPRE').
card_original_type('shipbreaker kraken'/'pPRE', 'Creature — Kraken').
card_original_text('shipbreaker kraken'/'pPRE', '').
card_first_print('shipbreaker kraken', 'pPRE').
card_image_name('shipbreaker kraken'/'pPRE', 'shipbreaker kraken').
card_uid('shipbreaker kraken'/'pPRE', 'pPRE:Shipbreaker Kraken:shipbreaker kraken').
card_rarity('shipbreaker kraken'/'pPRE', 'Special').
card_artist('shipbreaker kraken'/'pPRE', 'Svetlin Velinov').
card_number('shipbreaker kraken'/'pPRE', '69').

card_in_set('sidisi, brood tyrant', 'pPRE').
card_original_type('sidisi, brood tyrant'/'pPRE', 'Legendary Creature — Naga Shaman').
card_original_text('sidisi, brood tyrant'/'pPRE', '').
card_first_print('sidisi, brood tyrant', 'pPRE').
card_image_name('sidisi, brood tyrant'/'pPRE', 'sidisi, brood tyrant').
card_uid('sidisi, brood tyrant'/'pPRE', 'pPRE:Sidisi, Brood Tyrant:sidisi, brood tyrant').
card_rarity('sidisi, brood tyrant'/'pPRE', 'Special').
card_artist('sidisi, brood tyrant'/'pPRE', 'Karl Kopinski').
card_number('sidisi, brood tyrant'/'pPRE', '117').

card_in_set('siege dragon', 'pPRE').
card_original_type('siege dragon'/'pPRE', 'Creature — Dragon').
card_original_text('siege dragon'/'pPRE', '').
card_first_print('siege dragon', 'pPRE').
card_image_name('siege dragon'/'pPRE', 'siege dragon').
card_uid('siege dragon'/'pPRE', 'pPRE:Siege Dragon:siege dragon').
card_rarity('siege dragon'/'pPRE', 'Special').
card_artist('siege dragon'/'pPRE', 'Steve Prescott').
card_number('siege dragon'/'pPRE', '86').

card_in_set('siege rhino', 'pPRE').
card_original_type('siege rhino'/'pPRE', 'Creature — Rhino').
card_original_text('siege rhino'/'pPRE', '').
card_first_print('siege rhino', 'pPRE').
card_image_name('siege rhino'/'pPRE', 'siege rhino').
card_uid('siege rhino'/'pPRE', 'pPRE:Siege Rhino:siege rhino').
card_rarity('siege rhino'/'pPRE', 'Special').
card_artist('siege rhino'/'pPRE', 'Volkan Baga').
card_number('siege rhino'/'pPRE', '118').
card_flavor_text('siege rhino'/'pPRE', 'The mere approach of an Abzan war beast is enough to send enemies fleeing in panic.').

card_in_set('silent sentinel', 'pPRE').
card_original_type('silent sentinel'/'pPRE', 'Creature — Archon').
card_original_text('silent sentinel'/'pPRE', '').
card_first_print('silent sentinel', 'pPRE').
card_image_name('silent sentinel'/'pPRE', 'silent sentinel').
card_uid('silent sentinel'/'pPRE', 'pPRE:Silent Sentinel:silent sentinel').
card_rarity('silent sentinel'/'pPRE', 'Special').
card_artist('silent sentinel'/'pPRE', 'Kev Walker').
card_number('silent sentinel'/'pPRE', '73').
card_flavor_text('silent sentinel'/'pPRE', 'It serves a justice higher than the whims of the gods.').

card_in_set('silent specter', 'pPRE').
card_original_type('silent specter'/'pPRE', 'Creature — Specter').
card_original_text('silent specter'/'pPRE', '').
card_first_print('silent specter', 'pPRE').
card_image_name('silent specter'/'pPRE', 'silent specter').
card_uid('silent specter'/'pPRE', 'pPRE:Silent Specter:silent specter').
card_rarity('silent specter'/'pPRE', 'Special').
card_artist('silent specter'/'pPRE', 'Daren Bader').
card_number('silent specter'/'pPRE', '18').

card_in_set('soul collector', 'pPRE').
card_original_type('soul collector'/'pPRE', 'Creature — Vampire').
card_original_text('soul collector'/'pPRE', '').
card_first_print('soul collector', 'pPRE').
card_image_name('soul collector'/'pPRE', 'soul collector').
card_uid('soul collector'/'pPRE', 'pPRE:Soul Collector:soul collector').
card_rarity('soul collector'/'pPRE', 'Special').
card_artist('soul collector'/'pPRE', 'Matthew D. Wilson').
card_number('soul collector'/'pPRE', '20').

card_in_set('spawn of thraxes', 'pPRE').
card_original_type('spawn of thraxes'/'pPRE', 'Creature — Dragon').
card_original_text('spawn of thraxes'/'pPRE', '').
card_first_print('spawn of thraxes', 'pPRE').
card_image_name('spawn of thraxes'/'pPRE', 'spawn of thraxes').
card_uid('spawn of thraxes'/'pPRE', 'pPRE:Spawn of Thraxes:spawn of thraxes').
card_rarity('spawn of thraxes'/'pPRE', 'Special').
card_artist('spawn of thraxes'/'pPRE', 'Jason Rainville').
card_number('spawn of thraxes'/'pPRE', '81').
card_flavor_text('spawn of thraxes'/'pPRE', 'Sparks from Purphoros\'s forge fill the belly of every dragon.').

card_in_set('stone-tongue basilisk', 'pPRE').
card_original_type('stone-tongue basilisk'/'pPRE', 'Creature — Basilisk').
card_original_text('stone-tongue basilisk'/'pPRE', '').
card_first_print('stone-tongue basilisk', 'pPRE').
card_image_name('stone-tongue basilisk'/'pPRE', 'stone-tongue basilisk').
card_uid('stone-tongue basilisk'/'pPRE', 'pPRE:Stone-Tongue Basilisk:stone-tongue basilisk').
card_rarity('stone-tongue basilisk'/'pPRE', 'Special').
card_artist('stone-tongue basilisk'/'pPRE', 'Wayne England').
card_number('stone-tongue basilisk'/'pPRE', '15').

card_in_set('sultai ascendancy', 'pPRE').
card_original_type('sultai ascendancy'/'pPRE', 'Enchantment').
card_original_text('sultai ascendancy'/'pPRE', '').
card_first_print('sultai ascendancy', 'pPRE').
card_image_name('sultai ascendancy'/'pPRE', 'sultai ascendancy').
card_uid('sultai ascendancy'/'pPRE', 'pPRE:Sultai Ascendancy:sultai ascendancy').
card_rarity('sultai ascendancy'/'pPRE', 'Special').
card_artist('sultai ascendancy'/'pPRE', 'Karl Kopinski').
card_number('sultai ascendancy'/'pPRE', '119').

card_in_set('sun titan', 'pPRE').
card_original_type('sun titan'/'pPRE', 'Creature — Giant').
card_original_text('sun titan'/'pPRE', '').
card_first_print('sun titan', 'pPRE').
card_image_name('sun titan'/'pPRE', 'sun titan').
card_uid('sun titan'/'pPRE', 'pPRE:Sun Titan:sun titan').
card_rarity('sun titan'/'pPRE', 'Special').
card_artist('sun titan'/'pPRE', 'Chris Rahn').
card_number('sun titan'/'pPRE', '45').
card_flavor_text('sun titan'/'pPRE', 'A blazing sun that never sets.').

card_in_set('surrak dragonclaw', 'pPRE').
card_original_type('surrak dragonclaw'/'pPRE', 'Legendary Creature — Human Warrior').
card_original_text('surrak dragonclaw'/'pPRE', '').
card_first_print('surrak dragonclaw', 'pPRE').
card_image_name('surrak dragonclaw'/'pPRE', 'surrak dragonclaw').
card_uid('surrak dragonclaw'/'pPRE', 'pPRE:Surrak Dragonclaw:surrak dragonclaw').
card_rarity('surrak dragonclaw'/'pPRE', 'Special').
card_artist('surrak dragonclaw'/'pPRE', 'Jaime Jones').
card_number('surrak dragonclaw'/'pPRE', '120').
card_flavor_text('surrak dragonclaw'/'pPRE', 'Both his rank and his scars were earned in single combat against a cave bear.').

card_in_set('sword of kaldra', 'pPRE').
card_original_type('sword of kaldra'/'pPRE', 'Legendary Artifact — Equipment').
card_original_text('sword of kaldra'/'pPRE', '').
card_first_print('sword of kaldra', 'pPRE').
card_image_name('sword of kaldra'/'pPRE', 'sword of kaldra').
card_uid('sword of kaldra'/'pPRE', 'pPRE:Sword of Kaldra:sword of kaldra').
card_rarity('sword of kaldra'/'pPRE', 'Special').
card_artist('sword of kaldra'/'pPRE', 'Donato Giancola').
card_number('sword of kaldra'/'pPRE', '21').

card_in_set('temur ascendancy', 'pPRE').
card_original_type('temur ascendancy'/'pPRE', 'Enchantment').
card_original_text('temur ascendancy'/'pPRE', '').
card_first_print('temur ascendancy', 'pPRE').
card_image_name('temur ascendancy'/'pPRE', 'temur ascendancy').
card_uid('temur ascendancy'/'pPRE', 'pPRE:Temur Ascendancy:temur ascendancy').
card_rarity('temur ascendancy'/'pPRE', 'Special').
card_artist('temur ascendancy'/'pPRE', 'Jaime Jones').
card_number('temur ascendancy'/'pPRE', '121').

card_in_set('thousand winds', 'pPRE').
card_original_type('thousand winds'/'pPRE', 'Creature — Elemental').
card_original_text('thousand winds'/'pPRE', '').
card_first_print('thousand winds', 'pPRE').
card_image_name('thousand winds'/'pPRE', 'thousand winds').
card_uid('thousand winds'/'pPRE', 'pPRE:Thousand Winds:thousand winds').
card_rarity('thousand winds'/'pPRE', 'Special').
card_artist('thousand winds'/'pPRE', 'Raymond Swanland').
card_number('thousand winds'/'pPRE', '122').

card_in_set('trail of mystery', 'pPRE').
card_original_type('trail of mystery'/'pPRE', 'Enchantment').
card_original_text('trail of mystery'/'pPRE', '').
card_first_print('trail of mystery', 'pPRE').
card_image_name('trail of mystery'/'pPRE', 'trail of mystery').
card_uid('trail of mystery'/'pPRE', 'pPRE:Trail of Mystery:trail of mystery').
card_rarity('trail of mystery'/'pPRE', 'Special').
card_artist('trail of mystery'/'pPRE', 'Raymond Swanland').
card_number('trail of mystery'/'pPRE', '123').

card_in_set('trap essence', 'pPRE').
card_original_type('trap essence'/'pPRE', 'Instant').
card_original_text('trap essence'/'pPRE', '').
card_first_print('trap essence', 'pPRE').
card_image_name('trap essence'/'pPRE', 'trap essence').
card_uid('trap essence'/'pPRE', 'pPRE:Trap Essence:trap essence').
card_rarity('trap essence'/'pPRE', 'Special').
card_artist('trap essence'/'pPRE', 'Raymond Swanland').
card_number('trap essence'/'pPRE', '124').
card_flavor_text('trap essence'/'pPRE', '\"Meat sustains the body. The spirit requires different sustenance.\"\n—Arel the Whisperer').

card_in_set('treasury thrull', 'pPRE').
card_original_type('treasury thrull'/'pPRE', 'Creature — Thrull').
card_original_text('treasury thrull'/'pPRE', '').
card_first_print('treasury thrull', 'pPRE').
card_image_name('treasury thrull'/'pPRE', 'treasury thrull').
card_uid('treasury thrull'/'pPRE', 'pPRE:Treasury Thrull:treasury thrull').
card_rarity('treasury thrull'/'pPRE', 'Special').
card_artist('treasury thrull'/'pPRE', 'Lucas Graciano').
card_number('treasury thrull'/'pPRE', '64').

card_in_set('utter end', 'pPRE').
card_original_type('utter end'/'pPRE', 'Instant').
card_original_text('utter end'/'pPRE', '').
card_first_print('utter end', 'pPRE').
card_image_name('utter end'/'pPRE', 'utter end').
card_uid('utter end'/'pPRE', 'pPRE:Utter End:utter end').
card_rarity('utter end'/'pPRE', 'Special').
card_artist('utter end'/'pPRE', 'Mark Winters').
card_number('utter end'/'pPRE', '125').
card_flavor_text('utter end'/'pPRE', '\"I came seeking a challenge. All I found was you.\"\n—Zurgo, khan of the Mardu').

card_in_set('vampire nocturnus', 'pPRE').
card_original_type('vampire nocturnus'/'pPRE', 'Creature — Vampire').
card_original_text('vampire nocturnus'/'pPRE', '').
card_image_name('vampire nocturnus'/'pPRE', 'vampire nocturnus').
card_uid('vampire nocturnus'/'pPRE', 'pPRE:Vampire Nocturnus:vampire nocturnus').
card_rarity('vampire nocturnus'/'pPRE', 'Special').
card_artist('vampire nocturnus'/'pPRE', 'Andrew Robinson').
card_number('vampire nocturnus'/'pPRE', '41').
card_flavor_text('vampire nocturnus'/'pPRE', '\"Your life will set with the sun.\"').

card_in_set('villainous wealth', 'pPRE').
card_original_type('villainous wealth'/'pPRE', 'Sorcery').
card_original_text('villainous wealth'/'pPRE', '').
card_first_print('villainous wealth', 'pPRE').
card_image_name('villainous wealth'/'pPRE', 'villainous wealth').
card_uid('villainous wealth'/'pPRE', 'pPRE:Villainous Wealth:villainous wealth').
card_rarity('villainous wealth'/'pPRE', 'Special').
card_artist('villainous wealth'/'pPRE', 'Erica Yang').
card_number('villainous wealth'/'pPRE', '126').
card_flavor_text('villainous wealth'/'pPRE', 'Gold buys death. Death earns gold.').

card_in_set('wren\'s run packmaster', 'pPRE').
card_original_type('wren\'s run packmaster'/'pPRE', 'Creature — Elf Warrior').
card_original_text('wren\'s run packmaster'/'pPRE', '').
card_first_print('wren\'s run packmaster', 'pPRE').
card_image_name('wren\'s run packmaster'/'pPRE', 'wren\'s run packmaster').
card_uid('wren\'s run packmaster'/'pPRE', 'pPRE:Wren\'s Run Packmaster:wren\'s run packmaster').
card_rarity('wren\'s run packmaster'/'pPRE', 'Special').
card_artist('wren\'s run packmaster'/'pPRE', 'Steve Prescott').
card_number('wren\'s run packmaster'/'pPRE', '34').

card_in_set('wurmcoil engine', 'pPRE').
card_original_type('wurmcoil engine'/'pPRE', 'Artifact Creature — Wurm').
card_original_text('wurmcoil engine'/'pPRE', '').
card_first_print('wurmcoil engine', 'pPRE').
card_image_name('wurmcoil engine'/'pPRE', 'wurmcoil engine').
card_uid('wurmcoil engine'/'pPRE', 'pPRE:Wurmcoil Engine:wurmcoil engine').
card_rarity('wurmcoil engine'/'pPRE', 'Special').
card_artist('wurmcoil engine'/'pPRE', 'Raymond Swanland').
card_number('wurmcoil engine'/'pPRE', '46').

card_in_set('xathrid gorgon', 'pPRE').
card_original_type('xathrid gorgon'/'pPRE', 'Creature — Gorgon').
card_original_text('xathrid gorgon'/'pPRE', '').
card_first_print('xathrid gorgon', 'pPRE').
card_image_name('xathrid gorgon'/'pPRE', 'xathrid gorgon').
card_uid('xathrid gorgon'/'pPRE', 'pPRE:Xathrid Gorgon:xathrid gorgon').
card_rarity('xathrid gorgon'/'pPRE', 'Special').
card_artist('xathrid gorgon'/'pPRE', 'Wesley Burt').
card_number('xathrid gorgon'/'pPRE', '54').

card_in_set('zurgo helmsmasher', 'pPRE').
card_original_type('zurgo helmsmasher'/'pPRE', 'Legendary Creature — Orc Warrior').
card_original_text('zurgo helmsmasher'/'pPRE', '').
card_first_print('zurgo helmsmasher', 'pPRE').
card_image_name('zurgo helmsmasher'/'pPRE', 'zurgo helmsmasher').
card_uid('zurgo helmsmasher'/'pPRE', 'pPRE:Zurgo Helmsmasher:zurgo helmsmasher').
card_rarity('zurgo helmsmasher'/'pPRE', 'Special').
card_artist('zurgo helmsmasher'/'pPRE', 'Aleksi Briclot').
card_number('zurgo helmsmasher'/'pPRE', '127').
