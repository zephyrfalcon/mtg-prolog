% Eighth Edition

set('8ED').
set_name('8ED', 'Eighth Edition').
set_release_date('8ED', '2003-07-28').
set_border('8ED', 'white').
set_type('8ED', 'core').

card_in_set('abyssal specter', '8ED').
card_original_type('abyssal specter'/'8ED', 'Creature — Specter').
card_original_text('abyssal specter'/'8ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\nWhenever Abyssal Specter deals damage to a player, that player discards a card from his or her hand.').
card_image_name('abyssal specter'/'8ED', 'abyssal specter').
card_uid('abyssal specter'/'8ED', '8ED:Abyssal Specter:abyssal specter').
card_rarity('abyssal specter'/'8ED', 'Uncommon').
card_artist('abyssal specter'/'8ED', 'Michael Sutfin').
card_number('abyssal specter'/'8ED', '117').
card_flavor_text('abyssal specter'/'8ED', 'To gaze under its hood is to invite death.').
card_multiverse_id('abyssal specter'/'8ED', '45307').

card_in_set('air elemental', '8ED').
card_original_type('air elemental'/'8ED', 'Creature — Elemental').
card_original_text('air elemental'/'8ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)').
card_image_name('air elemental'/'8ED', 'air elemental').
card_uid('air elemental'/'8ED', '8ED:Air Elemental:air elemental').
card_rarity('air elemental'/'8ED', 'Uncommon').
card_artist('air elemental'/'8ED', 'Wayne England').
card_number('air elemental'/'8ED', '59').
card_flavor_text('air elemental'/'8ED', 'Pray that it doesn\'t seek the safety of your lungs.').
card_multiverse_id('air elemental'/'8ED', '45255').

card_in_set('aladdin\'s ring', '8ED').
card_original_type('aladdin\'s ring'/'8ED', 'Artifact').
card_original_text('aladdin\'s ring'/'8ED', '{8}, {T}: Aladdin\'s Ring deals 4 damage to target creature or player.').
card_image_name('aladdin\'s ring'/'8ED', 'aladdin\'s ring').
card_uid('aladdin\'s ring'/'8ED', '8ED:Aladdin\'s Ring:aladdin\'s ring').
card_rarity('aladdin\'s ring'/'8ED', 'Rare').
card_artist('aladdin\'s ring'/'8ED', 'Dave Dorman').
card_number('aladdin\'s ring'/'8ED', '291').
card_flavor_text('aladdin\'s ring'/'8ED', '\"A good lamp will light your way. A good ring will clear it.\"\n—Nervan, royal jeweler').
card_multiverse_id('aladdin\'s ring'/'8ED', '45487').

card_in_set('ambition\'s cost', '8ED').
card_original_type('ambition\'s cost'/'8ED', 'Sorcery').
card_original_text('ambition\'s cost'/'8ED', 'You draw three cards and you lose 3 life.').
card_image_name('ambition\'s cost'/'8ED', 'ambition\'s cost').
card_uid('ambition\'s cost'/'8ED', '8ED:Ambition\'s Cost:ambition\'s cost').
card_rarity('ambition\'s cost'/'8ED', 'Uncommon').
card_artist('ambition\'s cost'/'8ED', 'Junko Taguchi').
card_number('ambition\'s cost'/'8ED', '118').
card_flavor_text('ambition\'s cost'/'8ED', '\"Knowledge demands sacrifice.\"').
card_multiverse_id('ambition\'s cost'/'8ED', '45313').

card_in_set('anaba shaman', '8ED').
card_original_type('anaba shaman'/'8ED', 'Creature — Minotaur').
card_original_text('anaba shaman'/'8ED', '{R}, {T}: Anaba Shaman deals 1 damage to target creature or player.').
card_image_name('anaba shaman'/'8ED', 'anaba shaman').
card_uid('anaba shaman'/'8ED', '8ED:Anaba Shaman:anaba shaman').
card_rarity('anaba shaman'/'8ED', 'Common').
card_artist('anaba shaman'/'8ED', 'Alex Horley-Orlandelli').
card_number('anaba shaman'/'8ED', '175').
card_flavor_text('anaba shaman'/'8ED', 'Just try taking this bull by the horns.').
card_multiverse_id('anaba shaman'/'8ED', '45364').

card_in_set('angel of mercy', '8ED').
card_original_type('angel of mercy'/'8ED', 'Creature — Angel').
card_original_text('angel of mercy'/'8ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\nWhen Angel of Mercy comes into play, you gain 3 life.').
card_image_name('angel of mercy'/'8ED', 'angel of mercy').
card_uid('angel of mercy'/'8ED', '8ED:Angel of Mercy:angel of mercy').
card_rarity('angel of mercy'/'8ED', 'Uncommon').
card_artist('angel of mercy'/'8ED', 'Melissa A. Benson').
card_number('angel of mercy'/'8ED', '1').
card_flavor_text('angel of mercy'/'8ED', 'A song of life soars over fields of blood.').
card_multiverse_id('angel of mercy'/'8ED', '45195').

card_in_set('angelic page', '8ED').
card_original_type('angelic page'/'8ED', 'Creature — Spirit').
card_original_text('angelic page'/'8ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\n{T}: Target attacking or blocking creature gets +1/+1 until end of turn.').
card_image_name('angelic page'/'8ED', 'angelic page').
card_uid('angelic page'/'8ED', '8ED:Angelic Page:angelic page').
card_rarity('angelic page'/'8ED', 'Common').
card_artist('angelic page'/'8ED', 'Marc Fishman').
card_number('angelic page'/'8ED', '2').
card_flavor_text('angelic page'/'8ED', 'If only every message were as perfect as its bearer.').
card_multiverse_id('angelic page'/'8ED', '45172').

card_in_set('archivist', '8ED').
card_original_type('archivist'/'8ED', 'Creature — Wizard').
card_original_text('archivist'/'8ED', '{T}: Draw a card.').
card_image_name('archivist'/'8ED', 'archivist').
card_uid('archivist'/'8ED', '8ED:Archivist:archivist').
card_rarity('archivist'/'8ED', 'Rare').
card_artist('archivist'/'8ED', 'Donato Giancola').
card_number('archivist'/'8ED', '60').
card_flavor_text('archivist'/'8ED', '\"Words—so innocent and powerless are they, as standing in a dictionary; how potent for good and evil they become to one who knows how to combine them\"\n—Nathaniel Hawthorne').
card_multiverse_id('archivist'/'8ED', '45268').

card_in_set('ardent militia', '8ED').
card_original_type('ardent militia'/'8ED', 'Creature — Soldier').
card_original_text('ardent militia'/'8ED', 'Attacking doesn\'t cause Ardent Militia to tap.').
card_image_name('ardent militia'/'8ED', 'ardent militia').
card_uid('ardent militia'/'8ED', '8ED:Ardent Militia:ardent militia').
card_rarity('ardent militia'/'8ED', 'Uncommon').
card_artist('ardent militia'/'8ED', 'Paolo Parente').
card_number('ardent militia'/'8ED', '3').
card_flavor_text('ardent militia'/'8ED', 'Knights fight for honor and mercenaries fight for gold. The militia fights for hearth and home.').
card_multiverse_id('ardent militia'/'8ED', '45194').

card_in_set('avatar of hope', '8ED').
card_original_type('avatar of hope'/'8ED', 'Creature — Avatar').
card_original_text('avatar of hope'/'8ED', 'If you have 3 life or less, Avatar of Hope costs {6} less to play.\nFlying (This creature can\'t be blocked except by creatures with flying.)\nAvatar of Hope may block any number of creatures.').
card_image_name('avatar of hope'/'8ED', 'avatar of hope').
card_uid('avatar of hope'/'8ED', '8ED:Avatar of Hope:avatar of hope').
card_rarity('avatar of hope'/'8ED', 'Rare').
card_artist('avatar of hope'/'8ED', 'Mark Zug').
card_number('avatar of hope'/'8ED', '4').
card_multiverse_id('avatar of hope'/'8ED', '46617').

card_in_set('aven cloudchaser', '8ED').
card_original_type('aven cloudchaser'/'8ED', 'Creature — Bird Soldier').
card_original_text('aven cloudchaser'/'8ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\nWhen Aven Cloudchaser comes into play, destroy target enchantment.').
card_image_name('aven cloudchaser'/'8ED', 'aven cloudchaser').
card_uid('aven cloudchaser'/'8ED', '8ED:Aven Cloudchaser:aven cloudchaser').
card_rarity('aven cloudchaser'/'8ED', 'Common').
card_artist('aven cloudchaser'/'8ED', 'Justin Sweet').
card_number('aven cloudchaser'/'8ED', '5').
card_multiverse_id('aven cloudchaser'/'8ED', '45177').

card_in_set('aven fisher', '8ED').
card_original_type('aven fisher'/'8ED', 'Creature — Bird Soldier').
card_original_text('aven fisher'/'8ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\nWhen Aven Fisher is put into a graveyard from play, you may draw a card.').
card_image_name('aven fisher'/'8ED', 'aven fisher').
card_uid('aven fisher'/'8ED', '8ED:Aven Fisher:aven fisher').
card_rarity('aven fisher'/'8ED', 'Common').
card_artist('aven fisher'/'8ED', 'Christopher Moeller').
card_number('aven fisher'/'8ED', '61').
card_flavor_text('aven fisher'/'8ED', 'The same spears that catch their food today will defend their homes tomorrow.').
card_multiverse_id('aven fisher'/'8ED', '45261').

card_in_set('aven flock', '8ED').
card_original_type('aven flock'/'8ED', 'Creature — Bird Soldier').
card_original_text('aven flock'/'8ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\n{W}: Aven Flock gets +0/+1 until end of turn.').
card_image_name('aven flock'/'8ED', 'aven flock').
card_uid('aven flock'/'8ED', '8ED:Aven Flock:aven flock').
card_rarity('aven flock'/'8ED', 'Common').
card_artist('aven flock'/'8ED', 'Greg & Tim Hildebrandt').
card_number('aven flock'/'8ED', '6').
card_flavor_text('aven flock'/'8ED', 'Just as each added feather steadies the wing, so does the flock grow stronger with each new member.').
card_multiverse_id('aven flock'/'8ED', '45179').

card_in_set('balance of power', '8ED').
card_original_type('balance of power'/'8ED', 'Sorcery').
card_original_text('balance of power'/'8ED', 'If target opponent has more cards in hand than you, draw cards equal to the difference.').
card_image_name('balance of power'/'8ED', 'balance of power').
card_uid('balance of power'/'8ED', '8ED:Balance of Power:balance of power').
card_rarity('balance of power'/'8ED', 'Rare').
card_artist('balance of power'/'8ED', 'Adam Rex').
card_number('balance of power'/'8ED', '62').
card_flavor_text('balance of power'/'8ED', '\"To empty your mind is to fill your grave.\"\n—Lat-Nam teaching').
card_multiverse_id('balance of power'/'8ED', '45276').

card_in_set('balduvian barbarians', '8ED').
card_original_type('balduvian barbarians'/'8ED', 'Creature — Barbarian').
card_original_text('balduvian barbarians'/'8ED', '').
card_image_name('balduvian barbarians'/'8ED', 'balduvian barbarians').
card_uid('balduvian barbarians'/'8ED', '8ED:Balduvian Barbarians:balduvian barbarians').
card_rarity('balduvian barbarians'/'8ED', 'Common').
card_artist('balduvian barbarians'/'8ED', 'Jim Nelson').
card_number('balduvian barbarians'/'8ED', '176').
card_flavor_text('balduvian barbarians'/'8ED', '\"From the snowy slopes of Kaelor,\nTo the canyons of Bandu,\nWe drink and fight and feast and die\nAs we were born to do.\"\n—Balduvian tavern song').
card_multiverse_id('balduvian barbarians'/'8ED', '45345').

card_in_set('beast of burden', '8ED').
card_original_type('beast of burden'/'8ED', 'Artifact Creature').
card_original_text('beast of burden'/'8ED', 'Beast of Burden\'s power and toughness are each equal to the number of creatures in play.').
card_image_name('beast of burden'/'8ED', 'beast of burden').
card_uid('beast of burden'/'8ED', '8ED:Beast of Burden:beast of burden').
card_rarity('beast of burden'/'8ED', 'Rare').
card_artist('beast of burden'/'8ED', 'Chippy').
card_number('beast of burden'/'8ED', '292').
card_flavor_text('beast of burden'/'8ED', 'The heavier the load, the harder it works.').
card_multiverse_id('beast of burden'/'8ED', '45488').

card_in_set('birds of paradise', '8ED').
card_original_type('birds of paradise'/'8ED', 'Creature — Bird').
card_original_text('birds of paradise'/'8ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\n{T}: Add one mana of any color to your mana pool.').
card_image_name('birds of paradise'/'8ED', 'birds of paradise').
card_uid('birds of paradise'/'8ED', '8ED:Birds of Paradise:birds of paradise').
card_rarity('birds of paradise'/'8ED', 'Rare').
card_artist('birds of paradise'/'8ED', 'Edward P. Beard, Jr.').
card_number('birds of paradise'/'8ED', '233').
card_multiverse_id('birds of paradise'/'8ED', '45439').

card_in_set('blanchwood armor', '8ED').
card_original_type('blanchwood armor'/'8ED', 'Enchant Creature').
card_original_text('blanchwood armor'/'8ED', 'Enchanted creature gets +1/+1 for each Forest you control.').
card_image_name('blanchwood armor'/'8ED', 'blanchwood armor').
card_uid('blanchwood armor'/'8ED', '8ED:Blanchwood Armor:blanchwood armor').
card_rarity('blanchwood armor'/'8ED', 'Uncommon').
card_artist('blanchwood armor'/'8ED', 'Paolo Parente').
card_number('blanchwood armor'/'8ED', '234').
card_flavor_text('blanchwood armor'/'8ED', '\"There\'s never been a dwarven smith who could match the marvels that Gaea herself forges.\"\n—Elvish champion').
card_multiverse_id('blanchwood armor'/'8ED', '45434').

card_in_set('blaze', '8ED').
card_original_type('blaze'/'8ED', 'Sorcery').
card_original_text('blaze'/'8ED', 'Blaze deals X damage to target creature or player.').
card_image_name('blaze'/'8ED', 'blaze').
card_uid('blaze'/'8ED', '8ED:Blaze:blaze').
card_rarity('blaze'/'8ED', 'Uncommon').
card_artist('blaze'/'8ED', 'Alex Horley-Orlandelli').
card_number('blaze'/'8ED', '177').
card_flavor_text('blaze'/'8ED', 'Fire never dies alone.').
card_multiverse_id('blaze'/'8ED', '45379').

card_in_set('blessed reversal', '8ED').
card_original_type('blessed reversal'/'8ED', 'Instant').
card_original_text('blessed reversal'/'8ED', 'You gain 3 life for each creature attacking you.').
card_image_name('blessed reversal'/'8ED', 'blessed reversal').
card_uid('blessed reversal'/'8ED', '8ED:Blessed Reversal:blessed reversal').
card_rarity('blessed reversal'/'8ED', 'Rare').
card_artist('blessed reversal'/'8ED', 'Christopher Moeller').
card_number('blessed reversal'/'8ED', '7').
card_flavor_text('blessed reversal'/'8ED', 'Even an enemy is a valuable resource.').
card_multiverse_id('blessed reversal'/'8ED', '45216').

card_in_set('blinding angel', '8ED').
card_original_type('blinding angel'/'8ED', 'Creature — Angel').
card_original_text('blinding angel'/'8ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\nWhenever Blinding Angel deals combat damage to a player, that player skips his or her next combat phase.').
card_image_name('blinding angel'/'8ED', 'blinding angel').
card_uid('blinding angel'/'8ED', '8ED:Blinding Angel:blinding angel').
card_rarity('blinding angel'/'8ED', 'Rare').
card_artist('blinding angel'/'8ED', 'Todd Lockwood').
card_number('blinding angel'/'8ED', '8').
card_flavor_text('blinding angel'/'8ED', '\"Their eyes will shrivel and blacken before faith\'s true light.\"').
card_multiverse_id('blinding angel'/'8ED', '45211').

card_in_set('blood moon', '8ED').
card_original_type('blood moon'/'8ED', 'Enchantment').
card_original_text('blood moon'/'8ED', 'Nonbasic lands are Mountains.').
card_image_name('blood moon'/'8ED', 'blood moon').
card_uid('blood moon'/'8ED', '8ED:Blood Moon:blood moon').
card_rarity('blood moon'/'8ED', 'Rare').
card_artist('blood moon'/'8ED', 'Franz Vohwinkel').
card_number('blood moon'/'8ED', '178').
card_flavor_text('blood moon'/'8ED', 'Heavy light flooded across the landscape, cloaking everything in deep crimson.').
card_multiverse_id('blood moon'/'8ED', '45386').

card_in_set('bloodshot cyclops', '8ED').
card_original_type('bloodshot cyclops'/'8ED', 'Creature — Giant').
card_original_text('bloodshot cyclops'/'8ED', '{T}, Sacrifice a creature: Bloodshot Cyclops deals damage equal to the sacrificed creature\'s power to target creature or player.').
card_image_name('bloodshot cyclops'/'8ED', 'bloodshot cyclops').
card_uid('bloodshot cyclops'/'8ED', '8ED:Bloodshot Cyclops:bloodshot cyclops').
card_rarity('bloodshot cyclops'/'8ED', 'Rare').
card_artist('bloodshot cyclops'/'8ED', 'Daren Bader').
card_number('bloodshot cyclops'/'8ED', '179').
card_flavor_text('bloodshot cyclops'/'8ED', 'After their first encounter, the goblins named him Chuck.').
card_multiverse_id('bloodshot cyclops'/'8ED', '45387').

card_in_set('bog imp', '8ED').
card_original_type('bog imp'/'8ED', 'Creature — Imp').
card_original_text('bog imp'/'8ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)').
card_image_name('bog imp'/'8ED', 'bog imp').
card_uid('bog imp'/'8ED', '8ED:Bog Imp:bog imp').
card_rarity('bog imp'/'8ED', 'Common').
card_artist('bog imp'/'8ED', 'Carl Critchlow').
card_number('bog imp'/'8ED', '119').
card_flavor_text('bog imp'/'8ED', 'Think of it as a butcher knife with wings.').
card_multiverse_id('bog imp'/'8ED', '45284').

card_in_set('bog wraith', '8ED').
card_original_type('bog wraith'/'8ED', 'Creature — Wraith').
card_original_text('bog wraith'/'8ED', 'Swampwalk (This creature is unblockable as long as defending player controls a Swamp.)').
card_image_name('bog wraith'/'8ED', 'bog wraith').
card_uid('bog wraith'/'8ED', '8ED:Bog Wraith:bog wraith').
card_rarity('bog wraith'/'8ED', 'Uncommon').
card_artist('bog wraith'/'8ED', 'Dave Dorman').
card_number('bog wraith'/'8ED', '120').
card_flavor_text('bog wraith'/'8ED', 'The bodies of its victims are the only tracks it leaves.').
card_multiverse_id('bog wraith'/'8ED', '45308').

card_in_set('boil', '8ED').
card_original_type('boil'/'8ED', 'Instant').
card_original_text('boil'/'8ED', 'Destroy all Islands.').
card_image_name('boil'/'8ED', 'boil').
card_uid('boil'/'8ED', '8ED:Boil:boil').
card_rarity('boil'/'8ED', 'Uncommon').
card_artist('boil'/'8ED', 'Jason Alexander Behnke').
card_number('boil'/'8ED', '180').
card_flavor_text('boil'/'8ED', '\"When every reef is bared and every wave is dust—only then shall I rest.\"').
card_multiverse_id('boil'/'8ED', '45373').

card_in_set('boomerang', '8ED').
card_original_type('boomerang'/'8ED', 'Instant').
card_original_text('boomerang'/'8ED', 'Return target permanent to its owner\'s hand.').
card_image_name('boomerang'/'8ED', 'boomerang').
card_uid('boomerang'/'8ED', '8ED:Boomerang:boomerang').
card_rarity('boomerang'/'8ED', 'Common').
card_artist('boomerang'/'8ED', 'Alan Rabinowitz').
card_number('boomerang'/'8ED', '63').
card_flavor_text('boomerang'/'8ED', '\"Returne from whence ye came. . . .\"\n—Edmund Spenser, The Faerie Queene').
card_multiverse_id('boomerang'/'8ED', '45243').

card_in_set('brass herald', '8ED').
card_original_type('brass herald'/'8ED', 'Artifact Creature — Golem').
card_original_text('brass herald'/'8ED', 'As Brass Herald comes into play, choose a creature type.\nWhen Brass Herald comes into play, reveal the top four cards of your library. Put all creature cards of the chosen type revealed this way into your hand and the rest on the bottom of your library in any order.\nCreatures of the chosen type get +1/+1.').
card_image_name('brass herald'/'8ED', 'brass herald').
card_uid('brass herald'/'8ED', '8ED:Brass Herald:brass herald').
card_rarity('brass herald'/'8ED', 'Rare').
card_artist('brass herald'/'8ED', 'Daren Bader').
card_number('brass herald'/'8ED', '293').
card_multiverse_id('brass herald'/'8ED', '45472').

card_in_set('bribery', '8ED').
card_original_type('bribery'/'8ED', 'Sorcery').
card_original_text('bribery'/'8ED', 'Search target opponent\'s library for a creature card and put that card into play under your control. Then that player shuffles his or her library.').
card_image_name('bribery'/'8ED', 'bribery').
card_uid('bribery'/'8ED', '8ED:Bribery:bribery').
card_rarity('bribery'/'8ED', 'Rare').
card_artist('bribery'/'8ED', 'Andrew Robinson').
card_number('bribery'/'8ED', '64').
card_flavor_text('bribery'/'8ED', 'In Mercadia City, loyalties extend only as far as your purse can stretch.').
card_multiverse_id('bribery'/'8ED', '45279').

card_in_set('call of the wild', '8ED').
card_original_type('call of the wild'/'8ED', 'Enchantment').
card_original_text('call of the wild'/'8ED', '{2}{G}{G}: Reveal the top card of your library. If it\'s a creature card, put it into play. Otherwise, put it into your graveyard.').
card_image_name('call of the wild'/'8ED', 'call of the wild').
card_uid('call of the wild'/'8ED', '8ED:Call of the Wild:call of the wild').
card_rarity('call of the wild'/'8ED', 'Rare').
card_artist('call of the wild'/'8ED', 'Paolo Parente').
card_number('call of the wild'/'8ED', '235').
card_flavor_text('call of the wild'/'8ED', 'From a whisper to a roar, the call is heard by all and refused by none.').
card_multiverse_id('call of the wild'/'8ED', '46620').

card_in_set('canopy spider', '8ED').
card_original_type('canopy spider'/'8ED', 'Creature — Spider').
card_original_text('canopy spider'/'8ED', 'Canopy Spider may block as though it had flying.').
card_image_name('canopy spider'/'8ED', 'canopy spider').
card_uid('canopy spider'/'8ED', '8ED:Canopy Spider:canopy spider').
card_rarity('canopy spider'/'8ED', 'Common').
card_artist('canopy spider'/'8ED', 'Christopher Rush').
card_number('canopy spider'/'8ED', '236').
card_flavor_text('canopy spider'/'8ED', 'It keeps the upper reaches of the forest free of every menace . . . except for the spider itself.').
card_multiverse_id('canopy spider'/'8ED', '45401').

card_in_set('canyon wildcat', '8ED').
card_original_type('canyon wildcat'/'8ED', 'Creature — Cat').
card_original_text('canyon wildcat'/'8ED', 'Mountainwalk (This creature is unblockable as long as defending player controls a Mountain.)').
card_image_name('canyon wildcat'/'8ED', 'canyon wildcat').
card_uid('canyon wildcat'/'8ED', '8ED:Canyon Wildcat:canyon wildcat').
card_rarity('canyon wildcat'/'8ED', 'Common').
card_artist('canyon wildcat'/'8ED', 'Gary Leach').
card_number('canyon wildcat'/'8ED', '181').
card_flavor_text('canyon wildcat'/'8ED', 'In the warrior kingdom of Keld, the cats used in the hunt are prized for their tracking sense and ability to climb walls.').
card_multiverse_id('canyon wildcat'/'8ED', '45353').

card_in_set('carrion wall', '8ED').
card_original_type('carrion wall'/'8ED', 'Creature — Wall').
card_original_text('carrion wall'/'8ED', '(Walls can\'t attack.)\n{1}{B}: Regenerate Carrion Wall.').
card_image_name('carrion wall'/'8ED', 'carrion wall').
card_uid('carrion wall'/'8ED', '8ED:Carrion Wall:carrion wall').
card_rarity('carrion wall'/'8ED', 'Uncommon').
card_artist('carrion wall'/'8ED', 'Tony Szczudlo').
card_number('carrion wall'/'8ED', '121').
card_flavor_text('carrion wall'/'8ED', 'A wall built from the bodies of the enemy will never lack building material in times of war.').
card_multiverse_id('carrion wall'/'8ED', '45306').

card_in_set('catalog', '8ED').
card_original_type('catalog'/'8ED', 'Instant').
card_original_text('catalog'/'8ED', 'Draw two cards, then discard a card from your hand.').
card_image_name('catalog'/'8ED', 'catalog').
card_uid('catalog'/'8ED', '8ED:Catalog:catalog').
card_rarity('catalog'/'8ED', 'Common').
card_artist('catalog'/'8ED', 'Berry').
card_number('catalog'/'8ED', '65').
card_flavor_text('catalog'/'8ED', 'Though those who study are as many as the hairs on an ox, those who succeed are as rare as unicorns\' horns.\n—Chinese proverb').
card_multiverse_id('catalog'/'8ED', '45245').

card_in_set('chastise', '8ED').
card_original_type('chastise'/'8ED', 'Instant').
card_original_text('chastise'/'8ED', 'Destroy target attacking creature. You gain life equal to its power.').
card_image_name('chastise'/'8ED', 'chastise').
card_uid('chastise'/'8ED', '8ED:Chastise:chastise').
card_rarity('chastise'/'8ED', 'Uncommon').
card_artist('chastise'/'8ED', 'Carl Critchlow').
card_number('chastise'/'8ED', '9').
card_flavor_text('chastise'/'8ED', '\"Why do we pray to the Ancestor? Because She listens.\"\n—Mystic elder').
card_multiverse_id('chastise'/'8ED', '45200').

card_in_set('choke', '8ED').
card_original_type('choke'/'8ED', 'Enchantment').
card_original_text('choke'/'8ED', 'Islands don\'t untap during their controllers\' untap steps.').
card_image_name('choke'/'8ED', 'choke').
card_uid('choke'/'8ED', '8ED:Choke:choke').
card_rarity('choke'/'8ED', 'Uncommon').
card_artist('choke'/'8ED', 'Terese Nielsen').
card_number('choke'/'8ED', '237').
card_flavor_text('choke'/'8ED', '\"One day we shall walk where once was water.\"\n—Eladamri, Lord of Leaves').
card_multiverse_id('choke'/'8ED', '45431').

card_in_set('cinder wall', '8ED').
card_original_type('cinder wall'/'8ED', 'Creature — Wall').
card_original_text('cinder wall'/'8ED', '(Walls can\'t attack.)\nWhen Cinder Wall blocks, destroy it at end of combat.').
card_image_name('cinder wall'/'8ED', 'cinder wall').
card_uid('cinder wall'/'8ED', '8ED:Cinder Wall:cinder wall').
card_rarity('cinder wall'/'8ED', 'Common').
card_artist('cinder wall'/'8ED', 'Anthony S. Waters').
card_number('cinder wall'/'8ED', '182').
card_flavor_text('cinder wall'/'8ED', '\"The smell of roasted flesh hung over the valley for hours.\"\n—Onean sergeant').
card_multiverse_id('cinder wall'/'8ED', '45341').

card_in_set('circle of protection: black', '8ED').
card_original_type('circle of protection: black'/'8ED', 'Enchantment').
card_original_text('circle of protection: black'/'8ED', '{1}: The next time a black source of your choice would deal damage to you this turn, prevent that damage.').
card_image_name('circle of protection: black'/'8ED', 'circle of protection black').
card_uid('circle of protection: black'/'8ED', '8ED:Circle of Protection: Black:circle of protection black').
card_rarity('circle of protection: black'/'8ED', 'Uncommon').
card_artist('circle of protection: black'/'8ED', 'Mark Romanoski').
card_number('circle of protection: black'/'8ED', '10').
card_multiverse_id('circle of protection: black'/'8ED', '45204').

card_in_set('circle of protection: blue', '8ED').
card_original_type('circle of protection: blue'/'8ED', 'Enchantment').
card_original_text('circle of protection: blue'/'8ED', '{1}: The next time a blue source of your choice would deal damage to you this turn, prevent that damage.').
card_image_name('circle of protection: blue'/'8ED', 'circle of protection blue').
card_uid('circle of protection: blue'/'8ED', '8ED:Circle of Protection: Blue:circle of protection blue').
card_rarity('circle of protection: blue'/'8ED', 'Uncommon').
card_artist('circle of protection: blue'/'8ED', 'Greg & Tim Hildebrandt').
card_number('circle of protection: blue'/'8ED', '11').
card_multiverse_id('circle of protection: blue'/'8ED', '45203').

card_in_set('circle of protection: green', '8ED').
card_original_type('circle of protection: green'/'8ED', 'Enchantment').
card_original_text('circle of protection: green'/'8ED', '{1}: The next time a green source of your choice would deal damage to you this turn, prevent that damage.').
card_image_name('circle of protection: green'/'8ED', 'circle of protection green').
card_uid('circle of protection: green'/'8ED', '8ED:Circle of Protection: Green:circle of protection green').
card_rarity('circle of protection: green'/'8ED', 'Uncommon').
card_artist('circle of protection: green'/'8ED', 'Alan Pollack').
card_number('circle of protection: green'/'8ED', '12').
card_multiverse_id('circle of protection: green'/'8ED', '45206').

card_in_set('circle of protection: red', '8ED').
card_original_type('circle of protection: red'/'8ED', 'Enchantment').
card_original_text('circle of protection: red'/'8ED', '{1}: The next time a red source of your choice would deal damage to you this turn, prevent that damage.').
card_image_name('circle of protection: red'/'8ED', 'circle of protection red').
card_uid('circle of protection: red'/'8ED', '8ED:Circle of Protection: Red:circle of protection red').
card_rarity('circle of protection: red'/'8ED', 'Uncommon').
card_artist('circle of protection: red'/'8ED', 'Gary Ruddell').
card_number('circle of protection: red'/'8ED', '13').
card_multiverse_id('circle of protection: red'/'8ED', '45205').

card_in_set('circle of protection: white', '8ED').
card_original_type('circle of protection: white'/'8ED', 'Enchantment').
card_original_text('circle of protection: white'/'8ED', '{1}: The next time a white source of your choice would deal damage to you this turn, prevent that damage.').
card_image_name('circle of protection: white'/'8ED', 'circle of protection white').
card_uid('circle of protection: white'/'8ED', '8ED:Circle of Protection: White:circle of protection white').
card_rarity('circle of protection: white'/'8ED', 'Uncommon').
card_artist('circle of protection: white'/'8ED', 'Darrell Riche').
card_number('circle of protection: white'/'8ED', '14').
card_multiverse_id('circle of protection: white'/'8ED', '45202').

card_in_set('city of brass', '8ED').
card_original_type('city of brass'/'8ED', 'Land').
card_original_text('city of brass'/'8ED', 'Whenever City of Brass becomes tapped, it deals 1 damage to you.\n{T}: Add one mana of any color to your mana pool.').
card_image_name('city of brass'/'8ED', 'city of brass').
card_uid('city of brass'/'8ED', '8ED:City of Brass:city of brass').
card_rarity('city of brass'/'8ED', 'Rare').
card_artist('city of brass'/'8ED', 'Ron Walotsky').
card_number('city of brass'/'8ED', '322').
card_multiverse_id('city of brass'/'8ED', '45496').

card_in_set('coastal hornclaw', '8ED').
card_original_type('coastal hornclaw'/'8ED', 'Creature — Bird').
card_original_text('coastal hornclaw'/'8ED', 'Sacrifice a land: Coastal Hornclaw gains flying until end of turn. (It can\'t be blocked except by creatures with flying.)').
card_image_name('coastal hornclaw'/'8ED', 'coastal hornclaw').
card_uid('coastal hornclaw'/'8ED', '8ED:Coastal Hornclaw:coastal hornclaw').
card_rarity('coastal hornclaw'/'8ED', 'Common').
card_artist('coastal hornclaw'/'8ED', 'DiTerlizzi').
card_number('coastal hornclaw'/'8ED', '66').
card_flavor_text('coastal hornclaw'/'8ED', 'Jamuraan farmers fear three things above all else: droughts, floods, and hornclaws.').
card_multiverse_id('coastal hornclaw'/'8ED', '45233').

card_in_set('coastal piracy', '8ED').
card_original_type('coastal piracy'/'8ED', 'Enchantment').
card_original_text('coastal piracy'/'8ED', 'Whenever a creature you control deals combat damage to an opponent, you may draw a card.').
card_image_name('coastal piracy'/'8ED', 'coastal piracy').
card_uid('coastal piracy'/'8ED', '8ED:Coastal Piracy:coastal piracy').
card_rarity('coastal piracy'/'8ED', 'Rare').
card_artist('coastal piracy'/'8ED', 'Matthew D. Wilson').
card_number('coastal piracy'/'8ED', '67').
card_flavor_text('coastal piracy'/'8ED', '\"I don\'t like to think of myself as a pirate. I\'m more like a stimulator of the local economy.\"').
card_multiverse_id('coastal piracy'/'8ED', '46618').

card_in_set('coastal tower', '8ED').
card_original_type('coastal tower'/'8ED', 'Land').
card_original_text('coastal tower'/'8ED', 'Coastal Tower comes into play tapped.\n{T}: Add {W} or {U} to your mana pool.').
card_image_name('coastal tower'/'8ED', 'coastal tower').
card_uid('coastal tower'/'8ED', '8ED:Coastal Tower:coastal tower').
card_rarity('coastal tower'/'8ED', 'Uncommon').
card_artist('coastal tower'/'8ED', 'Don Hazeltine').
card_number('coastal tower'/'8ED', '323').
card_flavor_text('coastal tower'/'8ED', 'A beacon in the night, a dream in the fog.').
card_multiverse_id('coastal tower'/'8ED', '45491').

card_in_set('coat of arms', '8ED').
card_original_type('coat of arms'/'8ED', 'Artifact').
card_original_text('coat of arms'/'8ED', 'Each creature gets +1/+1 for each other creature in play that shares a creature type with it. (For example, if there are three Goblins in play, each gets +2/+2.)').
card_image_name('coat of arms'/'8ED', 'coat of arms').
card_uid('coat of arms'/'8ED', '8ED:Coat of Arms:coat of arms').
card_rarity('coat of arms'/'8ED', 'Rare').
card_artist('coat of arms'/'8ED', 'Scott M. Fischer').
card_number('coat of arms'/'8ED', '294').
card_flavor_text('coat of arms'/'8ED', '\"Hup, two, three, four,\nDunno how to count no more.\"\n—Mogg march').
card_multiverse_id('coat of arms'/'8ED', '45485').

card_in_set('coercion', '8ED').
card_original_type('coercion'/'8ED', 'Sorcery').
card_original_text('coercion'/'8ED', 'Target opponent reveals his or her hand. Choose a card from it. That player discards that card.').
card_image_name('coercion'/'8ED', 'coercion').
card_uid('coercion'/'8ED', '8ED:Coercion:coercion').
card_rarity('coercion'/'8ED', 'Common').
card_artist('coercion'/'8ED', 'DiTerlizzi').
card_number('coercion'/'8ED', '122').
card_flavor_text('coercion'/'8ED', '\"Human tenderness is simply weakness in a pretty package.\"').
card_multiverse_id('coercion'/'8ED', '45297').

card_in_set('collective unconscious', '8ED').
card_original_type('collective unconscious'/'8ED', 'Sorcery').
card_original_text('collective unconscious'/'8ED', 'Draw a card for each creature you control.').
card_image_name('collective unconscious'/'8ED', 'collective unconscious').
card_uid('collective unconscious'/'8ED', '8ED:Collective Unconscious:collective unconscious').
card_rarity('collective unconscious'/'8ED', 'Rare').
card_artist('collective unconscious'/'8ED', 'Andrew Goldhawk').
card_number('collective unconscious'/'8ED', '238').
card_flavor_text('collective unconscious'/'8ED', '\"The beasts of the wild are my senses. It is through their eyes that all knowledge shall come to me.\"').
card_multiverse_id('collective unconscious'/'8ED', '45456').

card_in_set('concentrate', '8ED').
card_original_type('concentrate'/'8ED', 'Sorcery').
card_original_text('concentrate'/'8ED', 'Draw three cards.').
card_image_name('concentrate'/'8ED', 'concentrate').
card_uid('concentrate'/'8ED', '8ED:Concentrate:concentrate').
card_rarity('concentrate'/'8ED', 'Uncommon').
card_artist('concentrate'/'8ED', 'Glen Angus & Arnie Swekel').
card_number('concentrate'/'8ED', '68').
card_flavor_text('concentrate'/'8ED', '\"The treasure in my mind is greater than any worldly glory.\"').
card_multiverse_id('concentrate'/'8ED', '45262').

card_in_set('confiscate', '8ED').
card_original_type('confiscate'/'8ED', 'Enchant Permanent').
card_original_text('confiscate'/'8ED', 'You control enchanted permanent.').
card_image_name('confiscate'/'8ED', 'confiscate').
card_uid('confiscate'/'8ED', '8ED:Confiscate:confiscate').
card_rarity('confiscate'/'8ED', 'Uncommon').
card_artist('confiscate'/'8ED', 'Adam Rex').
card_number('confiscate'/'8ED', '69').
card_flavor_text('confiscate'/'8ED', '\"It is better to take what does not belong to you than to let it lie around neglected.\"\n—Mark Twain, More Maxims of Mark').
card_multiverse_id('confiscate'/'8ED', '45273').

card_in_set('coral eel', '8ED').
card_original_type('coral eel'/'8ED', 'Creature — Eel').
card_original_text('coral eel'/'8ED', '').
card_image_name('coral eel'/'8ED', 'coral eel').
card_uid('coral eel'/'8ED', '8ED:Coral Eel:coral eel').
card_rarity('coral eel'/'8ED', 'Common').
card_artist('coral eel'/'8ED', 'Una Fricker').
card_number('coral eel'/'8ED', '70').
card_flavor_text('coral eel'/'8ED', 'Some fishers like to eat eels, and some eels like to eat fishers.').
card_multiverse_id('coral eel'/'8ED', '45247').

card_in_set('cowardice', '8ED').
card_original_type('cowardice'/'8ED', 'Enchantment').
card_original_text('cowardice'/'8ED', 'Whenever a creature becomes the target of a spell or ability, return that creature to its owner\'s hand. (It won\'t be affected by the spell or ability.)').
card_image_name('cowardice'/'8ED', 'cowardice').
card_uid('cowardice'/'8ED', '8ED:Cowardice:cowardice').
card_rarity('cowardice'/'8ED', 'Rare').
card_artist('cowardice'/'8ED', 'Scott M. Fischer').
card_number('cowardice'/'8ED', '71').
card_flavor_text('cowardice'/'8ED', '\"Cowards die many times before their deaths;\nThe valiant never taste of death but once.\"\n—William Shakespeare, Julius Caesar').
card_multiverse_id('cowardice'/'8ED', '45280').

card_in_set('craw wurm', '8ED').
card_original_type('craw wurm'/'8ED', 'Creature — Wurm').
card_original_text('craw wurm'/'8ED', '').
card_image_name('craw wurm'/'8ED', 'craw wurm').
card_uid('craw wurm'/'8ED', '8ED:Craw Wurm:craw wurm').
card_rarity('craw wurm'/'8ED', 'Common').
card_artist('craw wurm'/'8ED', 'Heather Hudson').
card_number('craw wurm'/'8ED', '239').
card_flavor_text('craw wurm'/'8ED', 'The most terrifying thing about the craw wurm is probably the horrible crashing sound it makes as it speeds through the forest. This noise is so loud it echoes through the trees and seems to come from all directions at once.').
card_multiverse_id('craw wurm'/'8ED', '47280').

card_in_set('creeping mold', '8ED').
card_original_type('creeping mold'/'8ED', 'Sorcery').
card_original_text('creeping mold'/'8ED', 'Destroy target artifact, enchantment, or land.').
card_image_name('creeping mold'/'8ED', 'creeping mold').
card_uid('creeping mold'/'8ED', '8ED:Creeping Mold:creeping mold').
card_rarity('creeping mold'/'8ED', 'Uncommon').
card_artist('creeping mold'/'8ED', 'Gary Ruddell').
card_number('creeping mold'/'8ED', '240').
card_flavor_text('creeping mold'/'8ED', 'Mold crept over the walls and into every crevice until the gleaming white stone strained and burst.').
card_multiverse_id('creeping mold'/'8ED', '45438').

card_in_set('crossbow infantry', '8ED').
card_original_type('crossbow infantry'/'8ED', 'Creature — Soldier').
card_original_text('crossbow infantry'/'8ED', '{T}: Crossbow Infantry deals 1 damage to target attacking or blocking creature.').
card_image_name('crossbow infantry'/'8ED', 'crossbow infantry').
card_uid('crossbow infantry'/'8ED', '8ED:Crossbow Infantry:crossbow infantry').
card_rarity('crossbow infantry'/'8ED', 'Common').
card_artist('crossbow infantry'/'8ED', 'James Bernardin').
card_number('crossbow infantry'/'8ED', '15').
card_flavor_text('crossbow infantry'/'8ED', '\"He can split a marshfly in two from halfway across the range.\"\n—Onean sergeant').
card_multiverse_id('crossbow infantry'/'8ED', '45171').

card_in_set('crystal rod', '8ED').
card_original_type('crystal rod'/'8ED', 'Artifact').
card_original_text('crystal rod'/'8ED', 'Whenever a player plays a blue spell, you may pay {1}. If you do, you gain 1 life.').
card_image_name('crystal rod'/'8ED', 'crystal rod').
card_uid('crystal rod'/'8ED', '8ED:Crystal Rod:crystal rod').
card_rarity('crystal rod'/'8ED', 'Uncommon').
card_artist('crystal rod'/'8ED', 'Donato Giancola').
card_number('crystal rod'/'8ED', '295').
card_flavor_text('crystal rod'/'8ED', 'Polished by a thousand waves.').
card_multiverse_id('crystal rod'/'8ED', '45459').

card_in_set('curiosity', '8ED').
card_original_type('curiosity'/'8ED', 'Enchant Creature').
card_original_text('curiosity'/'8ED', 'Whenever enchanted creature deals damage to an opponent, you may draw a card.').
card_image_name('curiosity'/'8ED', 'curiosity').
card_uid('curiosity'/'8ED', '8ED:Curiosity:curiosity').
card_rarity('curiosity'/'8ED', 'Uncommon').
card_artist('curiosity'/'8ED', 'Jim Nelson').
card_number('curiosity'/'8ED', '72').
card_flavor_text('curiosity'/'8ED', '\"You know that secret? The one that wakes you up at night in a cold sweat? The one you think no one else in the world knows? Well, guess what.\"').
card_multiverse_id('curiosity'/'8ED', '45240').

card_in_set('daring apprentice', '8ED').
card_original_type('daring apprentice'/'8ED', 'Creature — Wizard').
card_original_text('daring apprentice'/'8ED', '{T}, Sacrifice Daring Apprentice: Counter target spell.').
card_image_name('daring apprentice'/'8ED', 'daring apprentice').
card_uid('daring apprentice'/'8ED', '8ED:Daring Apprentice:daring apprentice').
card_rarity('daring apprentice'/'8ED', 'Rare').
card_artist('daring apprentice'/'8ED', 'Dany Orizio').
card_number('daring apprentice'/'8ED', '73').
card_flavor_text('daring apprentice'/'8ED', '\"I\'ve seen my master do this a hundred times. How hard can it be?\"').
card_multiverse_id('daring apprentice'/'8ED', '45267').

card_in_set('dark banishing', '8ED').
card_original_type('dark banishing'/'8ED', 'Instant').
card_original_text('dark banishing'/'8ED', 'Destroy target nonblack creature. It can\'t be regenerated.').
card_image_name('dark banishing'/'8ED', 'dark banishing').
card_uid('dark banishing'/'8ED', '8ED:Dark Banishing:dark banishing').
card_rarity('dark banishing'/'8ED', 'Common').
card_artist('dark banishing'/'8ED', 'Dermot Power').
card_number('dark banishing'/'8ED', '123').
card_flavor_text('dark banishing'/'8ED', '\"Ha, banishment? Be merciful, say ‘death,\'\nFor exile hath more terror in his look,\nMuch more than death.\"\n—William Shakespeare, Romeo and Juliet').
card_multiverse_id('dark banishing'/'8ED', '45301').

card_in_set('death pit offering', '8ED').
card_original_type('death pit offering'/'8ED', 'Enchantment').
card_original_text('death pit offering'/'8ED', 'When Death Pit Offering comes into play, sacrifice all creatures you control.\nCreatures you control get +2/+2.').
card_image_name('death pit offering'/'8ED', 'death pit offering').
card_uid('death pit offering'/'8ED', '8ED:Death Pit Offering:death pit offering').
card_rarity('death pit offering'/'8ED', 'Rare').
card_artist('death pit offering'/'8ED', 'Pete Venters').
card_number('death pit offering'/'8ED', '124').
card_flavor_text('death pit offering'/'8ED', '\"Kill them all and feed them to the new recruits.\"\n—Crovax, ascendant evincar').
card_multiverse_id('death pit offering'/'8ED', '45338').

card_in_set('death pits of rath', '8ED').
card_original_type('death pits of rath'/'8ED', 'Enchantment').
card_original_text('death pits of rath'/'8ED', 'Whenever a creature is dealt damage, destroy it. It can\'t be regenerated.').
card_image_name('death pits of rath'/'8ED', 'death pits of rath').
card_uid('death pits of rath'/'8ED', '8ED:Death Pits of Rath:death pits of rath').
card_rarity('death pits of rath'/'8ED', 'Rare').
card_artist('death pits of rath'/'8ED', 'Joel Biske').
card_number('death pits of rath'/'8ED', '125').
card_flavor_text('death pits of rath'/'8ED', '\"Neither could I forget what I had read of these pits—that the sudden extinction of life formed no part of their most horrible plan.\"\n—Edgar Allan Poe,\n\"The Pit and the Pendulum\"').
card_multiverse_id('death pits of rath'/'8ED', '45329').

card_in_set('deathgazer', '8ED').
card_original_type('deathgazer'/'8ED', 'Creature — Lizard').
card_original_text('deathgazer'/'8ED', 'Whenever Deathgazer blocks or becomes blocked by a nonblack creature, destroy that creature at end of combat.').
card_image_name('deathgazer'/'8ED', 'deathgazer').
card_uid('deathgazer'/'8ED', '8ED:Deathgazer:deathgazer').
card_rarity('deathgazer'/'8ED', 'Uncommon').
card_artist('deathgazer'/'8ED', 'Donato Giancola').
card_number('deathgazer'/'8ED', '126').
card_flavor_text('deathgazer'/'8ED', 'Your entire life passes before its eyes.').
card_multiverse_id('deathgazer'/'8ED', '45309').

card_in_set('deepwood ghoul', '8ED').
card_original_type('deepwood ghoul'/'8ED', 'Creature — Zombie').
card_original_text('deepwood ghoul'/'8ED', 'Pay 2 life: Regenerate Deepwood Ghoul.').
card_image_name('deepwood ghoul'/'8ED', 'deepwood ghoul').
card_uid('deepwood ghoul'/'8ED', '8ED:Deepwood Ghoul:deepwood ghoul').
card_rarity('deepwood ghoul'/'8ED', 'Common').
card_artist('deepwood ghoul'/'8ED', 'Alan Pollack').
card_number('deepwood ghoul'/'8ED', '127').
card_flavor_text('deepwood ghoul'/'8ED', 'They feed on the living to stay undead.').
card_multiverse_id('deepwood ghoul'/'8ED', '45285').

card_in_set('defense grid', '8ED').
card_original_type('defense grid'/'8ED', 'Artifact').
card_original_text('defense grid'/'8ED', 'During each player\'s turn, each other player\'s spells cost {3} more to play.').
card_image_name('defense grid'/'8ED', 'defense grid').
card_uid('defense grid'/'8ED', '8ED:Defense Grid:defense grid').
card_rarity('defense grid'/'8ED', 'Rare').
card_artist('defense grid'/'8ED', 'Mark Tedin').
card_number('defense grid'/'8ED', '296').
card_flavor_text('defense grid'/'8ED', '\"A good deal of tyranny goes by the name of protection.\"\n—Crystal Eastman,\n\"Equality or Protection\"').
card_multiverse_id('defense grid'/'8ED', '45481').

card_in_set('deflection', '8ED').
card_original_type('deflection'/'8ED', 'Instant').
card_original_text('deflection'/'8ED', 'Change the target of target spell with a single target.').
card_image_name('deflection'/'8ED', 'deflection').
card_uid('deflection'/'8ED', '8ED:Deflection:deflection').
card_rarity('deflection'/'8ED', 'Rare').
card_artist('deflection'/'8ED', 'Jeremy Jarvis').
card_number('deflection'/'8ED', '74').
card_flavor_text('deflection'/'8ED', 'The golden rule of revenge: Do unto others what they tried to do unto you.').
card_multiverse_id('deflection'/'8ED', '45277').

card_in_set('dehydration', '8ED').
card_original_type('dehydration'/'8ED', 'Enchant Creature').
card_original_text('dehydration'/'8ED', 'Enchanted creature doesn\'t untap during its controller\'s untap step.').
card_image_name('dehydration'/'8ED', 'dehydration').
card_uid('dehydration'/'8ED', '8ED:Dehydration:dehydration').
card_rarity('dehydration'/'8ED', 'Common').
card_artist('dehydration'/'8ED', 'Arnie Swekel').
card_number('dehydration'/'8ED', '75').
card_flavor_text('dehydration'/'8ED', '\"Cry to the sun and watch as even your tears forsake you.\"\n—Acolyte of Marit Lage').
card_multiverse_id('dehydration'/'8ED', '45244').

card_in_set('demolish', '8ED').
card_original_type('demolish'/'8ED', 'Sorcery').
card_original_text('demolish'/'8ED', 'Destroy target artifact or land.').
card_image_name('demolish'/'8ED', 'demolish').
card_uid('demolish'/'8ED', '8ED:Demolish:demolish').
card_rarity('demolish'/'8ED', 'Uncommon').
card_artist('demolish'/'8ED', 'Gary Ruddell').
card_number('demolish'/'8ED', '183').
card_flavor_text('demolish'/'8ED', '\"Pound the steel until it fits.\nDoesn\'t work? Bash to bits.\"\n—Dwarven forging song').
card_multiverse_id('demolish'/'8ED', '45375').

card_in_set('demystify', '8ED').
card_original_type('demystify'/'8ED', 'Instant').
card_original_text('demystify'/'8ED', 'Destroy target enchantment.').
card_image_name('demystify'/'8ED', 'demystify').
card_uid('demystify'/'8ED', '8ED:Demystify:demystify').
card_rarity('demystify'/'8ED', 'Common').
card_artist('demystify'/'8ED', 'Christopher Rush').
card_number('demystify'/'8ED', '16').
card_multiverse_id('demystify'/'8ED', '45185').

card_in_set('diabolic tutor', '8ED').
card_original_type('diabolic tutor'/'8ED', 'Sorcery').
card_original_text('diabolic tutor'/'8ED', 'Search your library for a card and put that card into your hand. Then shuffle your library.').
card_image_name('diabolic tutor'/'8ED', 'diabolic tutor').
card_uid('diabolic tutor'/'8ED', '8ED:Diabolic Tutor:diabolic tutor').
card_rarity('diabolic tutor'/'8ED', 'Uncommon').
card_artist('diabolic tutor'/'8ED', 'Rick Farrell').
card_number('diabolic tutor'/'8ED', '128').
card_flavor_text('diabolic tutor'/'8ED', 'The best ideas often come from the worst minds.').
card_multiverse_id('diabolic tutor'/'8ED', '45319').

card_in_set('dingus egg', '8ED').
card_original_type('dingus egg'/'8ED', 'Artifact').
card_original_text('dingus egg'/'8ED', 'Whenever a land is put into a graveyard from play, Dingus Egg deals 2 damage to that land\'s controller.').
card_image_name('dingus egg'/'8ED', 'dingus egg').
card_uid('dingus egg'/'8ED', '8ED:Dingus Egg:dingus egg').
card_rarity('dingus egg'/'8ED', 'Rare').
card_artist('dingus egg'/'8ED', 'Randy Gallegos').
card_number('dingus egg'/'8ED', '297').
card_flavor_text('dingus egg'/'8ED', 'Legend has it that the world was hatched from a dingus egg.').
card_multiverse_id('dingus egg'/'8ED', '45480').

card_in_set('disrupting scepter', '8ED').
card_original_type('disrupting scepter'/'8ED', 'Artifact').
card_original_text('disrupting scepter'/'8ED', '{3}, {T}: Target player discards a card from his or her hand. Play this ability only during your turn.').
card_image_name('disrupting scepter'/'8ED', 'disrupting scepter').
card_uid('disrupting scepter'/'8ED', '8ED:Disrupting Scepter:disrupting scepter').
card_rarity('disrupting scepter'/'8ED', 'Rare').
card_artist('disrupting scepter'/'8ED', 'Stuart Griffin').
card_number('disrupting scepter'/'8ED', '298').
card_flavor_text('disrupting scepter'/'8ED', '\"True power is controlling not only the hearts of your subjects, but their minds as well.\"').
card_multiverse_id('disrupting scepter'/'8ED', '45476').

card_in_set('distorting lens', '8ED').
card_original_type('distorting lens'/'8ED', 'Artifact').
card_original_text('distorting lens'/'8ED', '{T}: Target permanent becomes the color of your choice until end of turn.').
card_image_name('distorting lens'/'8ED', 'distorting lens').
card_uid('distorting lens'/'8ED', '8ED:Distorting Lens:distorting lens').
card_rarity('distorting lens'/'8ED', 'Rare').
card_artist('distorting lens'/'8ED', 'Glen Angus').
card_number('distorting lens'/'8ED', '299').
card_flavor_text('distorting lens'/'8ED', '\"The Eye altering alters all.\"\n—William Blake, \"The Mental Traveller\"').
card_multiverse_id('distorting lens'/'8ED', '45473').

card_in_set('diving griffin', '8ED').
card_original_type('diving griffin'/'8ED', 'Creature — Griffin').
card_original_text('diving griffin'/'8ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\nAttacking doesn\'t cause Diving Griffin to tap.').
card_image_name('diving griffin'/'8ED', 'diving griffin').
card_uid('diving griffin'/'8ED', '8ED:Diving Griffin:diving griffin').
card_rarity('diving griffin'/'8ED', 'Common').
card_artist('diving griffin'/'8ED', 'John Howe').
card_number('diving griffin'/'8ED', '17').
card_flavor_text('diving griffin'/'8ED', 'To outrace the griffin\n—Kipamu expression meaning\n\"to do the impossible\"').
card_multiverse_id('diving griffin'/'8ED', '45176').

card_in_set('drudge skeletons', '8ED').
card_original_type('drudge skeletons'/'8ED', 'Creature — Skeleton').
card_original_text('drudge skeletons'/'8ED', '{B}: Regenerate Drudge Skeletons.').
card_image_name('drudge skeletons'/'8ED', 'drudge skeletons').
card_uid('drudge skeletons'/'8ED', '8ED:Drudge Skeletons:drudge skeletons').
card_rarity('drudge skeletons'/'8ED', 'Common').
card_artist('drudge skeletons'/'8ED', 'Jim Nelson').
card_number('drudge skeletons'/'8ED', '129').
card_flavor_text('drudge skeletons'/'8ED', '\"The dead make good soldiers. They can\'t disobey orders, they never surrender, and they don\'t stop fighting when a random body part falls off.\"\n—Nevinyrral, Necromancer\'s Handbook').
card_multiverse_id('drudge skeletons'/'8ED', '45311').

card_in_set('dusk imp', '8ED').
card_original_type('dusk imp'/'8ED', 'Creature — Imp').
card_original_text('dusk imp'/'8ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)').
card_image_name('dusk imp'/'8ED', 'dusk imp').
card_uid('dusk imp'/'8ED', '8ED:Dusk Imp:dusk imp').
card_rarity('dusk imp'/'8ED', 'Common').
card_artist('dusk imp'/'8ED', 'Edward P. Beard, Jr.').
card_number('dusk imp'/'8ED', '130').
card_flavor_text('dusk imp'/'8ED', 'It despises humans and squirrels and beasts and dwarves and cephalids . . . well, it despises just about everything.').
card_multiverse_id('dusk imp'/'8ED', '45288').

card_in_set('dwarven demolition team', '8ED').
card_original_type('dwarven demolition team'/'8ED', 'Creature — Dwarf').
card_original_text('dwarven demolition team'/'8ED', '{T}: Destroy target Wall.').
card_image_name('dwarven demolition team'/'8ED', 'dwarven demolition team').
card_uid('dwarven demolition team'/'8ED', '8ED:Dwarven Demolition Team:dwarven demolition team').
card_rarity('dwarven demolition team'/'8ED', 'Uncommon').
card_artist('dwarven demolition team'/'8ED', 'Kev Walker').
card_number('dwarven demolition team'/'8ED', '184').
card_flavor_text('dwarven demolition team'/'8ED', 'Foolishly, Najib retreated to his castle at El-Abar; the next morning he was dead. In just one night, the dwarven forces had reduced the mighty walls to mere rubble.').
card_multiverse_id('dwarven demolition team'/'8ED', '45367').

card_in_set('eager cadet', '8ED').
card_original_type('eager cadet'/'8ED', 'Creature — Soldier').
card_original_text('eager cadet'/'8ED', '').
card_image_name('eager cadet'/'8ED', 'eager cadet').
card_uid('eager cadet'/'8ED', '8ED:Eager Cadet:eager cadet').
card_rarity('eager cadet'/'8ED', 'Common').
card_artist('eager cadet'/'8ED', 'Greg & Tim Hildebrandt').
card_number('eager cadet'/'8ED', 'S1').
card_flavor_text('eager cadet'/'8ED', '\"Training? Seeing my crops burnt to cinders was all the ‘training\' I needed.\"').
card_multiverse_id('eager cadet'/'8ED', '47784').

card_in_set('eastern paladin', '8ED').
card_original_type('eastern paladin'/'8ED', 'Creature — Knight').
card_original_text('eastern paladin'/'8ED', '{B}{B}, {T}: Destroy target green creature.').
card_image_name('eastern paladin'/'8ED', 'eastern paladin').
card_uid('eastern paladin'/'8ED', '8ED:Eastern Paladin:eastern paladin').
card_rarity('eastern paladin'/'8ED', 'Rare').
card_artist('eastern paladin'/'8ED', 'Carl Critchlow').
card_number('eastern paladin'/'8ED', '131').
card_flavor_text('eastern paladin'/'8ED', '\"There is no worse enemy. His passion for death doesn\'t just border on insanity, it wallows in it.\"\n—Southern paladin').
card_multiverse_id('eastern paladin'/'8ED', '45325').

card_in_set('elfhame palace', '8ED').
card_original_type('elfhame palace'/'8ED', 'Land').
card_original_text('elfhame palace'/'8ED', 'Elfhame Palace comes into play tapped.\n{T}: Add {G} or {W} to your mana pool.').
card_image_name('elfhame palace'/'8ED', 'elfhame palace').
card_uid('elfhame palace'/'8ED', '8ED:Elfhame Palace:elfhame palace').
card_rarity('elfhame palace'/'8ED', 'Uncommon').
card_artist('elfhame palace'/'8ED', 'Anthony S. Waters').
card_number('elfhame palace'/'8ED', '324').
card_flavor_text('elfhame palace'/'8ED', 'The elfhames are the closest thing to \"civilization\" the elves allow themselves.').
card_multiverse_id('elfhame palace'/'8ED', '45495').

card_in_set('elite archers', '8ED').
card_original_type('elite archers'/'8ED', 'Creature — Soldier').
card_original_text('elite archers'/'8ED', '{T}: Elite Archers deals 3 damage to target attacking or blocking creature.').
card_image_name('elite archers'/'8ED', 'elite archers').
card_uid('elite archers'/'8ED', '8ED:Elite Archers:elite archers').
card_rarity('elite archers'/'8ED', 'Rare').
card_artist('elite archers'/'8ED', 'Greg Staples').
card_number('elite archers'/'8ED', '18').
card_flavor_text('elite archers'/'8ED', 'They fletch their arrows with the feathers of angels.').
card_multiverse_id('elite archers'/'8ED', '45214').

card_in_set('elite javelineer', '8ED').
card_original_type('elite javelineer'/'8ED', 'Creature — Soldier').
card_original_text('elite javelineer'/'8ED', 'Whenever Elite Javelineer blocks, it deals 1 damage to target attacking creature.').
card_image_name('elite javelineer'/'8ED', 'elite javelineer').
card_uid('elite javelineer'/'8ED', '8ED:Elite Javelineer:elite javelineer').
card_rarity('elite javelineer'/'8ED', 'Uncommon').
card_artist('elite javelineer'/'8ED', 'Mark Poole').
card_number('elite javelineer'/'8ED', '19').
card_flavor_text('elite javelineer'/'8ED', '\"Precision is frequently more valuable than force.\"').
card_multiverse_id('elite javelineer'/'8ED', '45199').

card_in_set('elvish champion', '8ED').
card_original_type('elvish champion'/'8ED', 'Creature — Lord').
card_original_text('elvish champion'/'8ED', 'All Elves get +1/+1 and have forestwalk. (They\'re unblockable as long as defending player controls a Forest.)').
card_image_name('elvish champion'/'8ED', 'elvish champion').
card_uid('elvish champion'/'8ED', '8ED:Elvish Champion:elvish champion').
card_rarity('elvish champion'/'8ED', 'Rare').
card_artist('elvish champion'/'8ED', 'Mark Zug').
card_number('elvish champion'/'8ED', '241').
card_flavor_text('elvish champion'/'8ED', '\"For what are leaves but countless blades\nTo fight a countless foe on high.\"\n—Elvish hymn').
card_multiverse_id('elvish champion'/'8ED', '45442').

card_in_set('elvish lyrist', '8ED').
card_original_type('elvish lyrist'/'8ED', 'Creature — Elf').
card_original_text('elvish lyrist'/'8ED', '{G}, {T}, Sacrifice Elvish Lyrist: Destroy target enchantment.').
card_image_name('elvish lyrist'/'8ED', 'elvish lyrist').
card_uid('elvish lyrist'/'8ED', '8ED:Elvish Lyrist:elvish lyrist').
card_rarity('elvish lyrist'/'8ED', 'Uncommon').
card_artist('elvish lyrist'/'8ED', 'Michael Koelsch').
card_number('elvish lyrist'/'8ED', '242').
card_flavor_text('elvish lyrist'/'8ED', 'Each pluck of the strings breaks the hold of the unnatural on the world.').
card_multiverse_id('elvish lyrist'/'8ED', '45421').

card_in_set('elvish pioneer', '8ED').
card_original_type('elvish pioneer'/'8ED', 'Creature — Elf Druid').
card_original_text('elvish pioneer'/'8ED', 'When Elvish Pioneer comes into play, you may put a basic land card from your hand into play tapped.').
card_image_name('elvish pioneer'/'8ED', 'elvish pioneer').
card_uid('elvish pioneer'/'8ED', '8ED:Elvish Pioneer:elvish pioneer').
card_rarity('elvish pioneer'/'8ED', 'Common').
card_artist('elvish pioneer'/'8ED', 'Thomas M. Baxa').
card_number('elvish pioneer'/'8ED', '243').
card_flavor_text('elvish pioneer'/'8ED', '\"Destruction is the work of an afternoon. Creation is the work of a lifetime.\"').
card_multiverse_id('elvish pioneer'/'8ED', '45413').

card_in_set('elvish piper', '8ED').
card_original_type('elvish piper'/'8ED', 'Creature — Elf').
card_original_text('elvish piper'/'8ED', '{G}, {T}: Put a creature card from your hand into play.').
card_image_name('elvish piper'/'8ED', 'elvish piper').
card_uid('elvish piper'/'8ED', '8ED:Elvish Piper:elvish piper').
card_rarity('elvish piper'/'8ED', 'Rare').
card_artist('elvish piper'/'8ED', 'Tristan Elwell').
card_number('elvish piper'/'8ED', '244').
card_flavor_text('elvish piper'/'8ED', 'From Gaea grew the world, and the world was silent. From Gaea grew the world\'s elves, and the world was silent no more.\n—Elvish teaching').
card_multiverse_id('elvish piper'/'8ED', '45444').

card_in_set('elvish scrapper', '8ED').
card_original_type('elvish scrapper'/'8ED', 'Creature — Elf').
card_original_text('elvish scrapper'/'8ED', '{G}, {T}, Sacrifice Elvish Scrapper: Destroy target artifact.').
card_image_name('elvish scrapper'/'8ED', 'elvish scrapper').
card_uid('elvish scrapper'/'8ED', '8ED:Elvish Scrapper:elvish scrapper').
card_rarity('elvish scrapper'/'8ED', 'Uncommon').
card_artist('elvish scrapper'/'8ED', 'Edward P. Beard, Jr.').
card_number('elvish scrapper'/'8ED', '245').
card_flavor_text('elvish scrapper'/'8ED', '\"The stories tell of a distant time when machines overran the forests, destroying everything that lived. That time will not come again.\"').
card_multiverse_id('elvish scrapper'/'8ED', '45422').

card_in_set('emperor crocodile', '8ED').
card_original_type('emperor crocodile'/'8ED', 'Creature — Crocodile').
card_original_text('emperor crocodile'/'8ED', 'When you control no other creatures, sacrifice Emperor Crocodile.').
card_image_name('emperor crocodile'/'8ED', 'emperor crocodile').
card_uid('emperor crocodile'/'8ED', '8ED:Emperor Crocodile:emperor crocodile').
card_rarity('emperor crocodile'/'8ED', 'Rare').
card_artist('emperor crocodile'/'8ED', 'Kev Walker').
card_number('emperor crocodile'/'8ED', '246').
card_flavor_text('emperor crocodile'/'8ED', 'The king of Yavimaya\'s waters pays constant attention to his subjects . . . and thrives on their adulation.').
card_multiverse_id('emperor crocodile'/'8ED', '45446').

card_in_set('enormous baloth', '8ED').
card_original_type('enormous baloth'/'8ED', 'Creature — Beast').
card_original_text('enormous baloth'/'8ED', '').
card_image_name('enormous baloth'/'8ED', 'enormous baloth').
card_uid('enormous baloth'/'8ED', '8ED:Enormous Baloth:enormous baloth').
card_rarity('enormous baloth'/'8ED', 'Uncommon').
card_artist('enormous baloth'/'8ED', 'Mark Tedin').
card_number('enormous baloth'/'8ED', 'S6').
card_flavor_text('enormous baloth'/'8ED', 'Its diet consists of fruits, plants, small woodland animals, large woodland animals, woodlands, fruit groves, fruit farmers, and small cities.').
card_multiverse_id('enormous baloth'/'8ED', '47787').

card_in_set('enrage', '8ED').
card_original_type('enrage'/'8ED', 'Instant').
card_original_text('enrage'/'8ED', 'Target creature gets +X/+0 until end of turn.').
card_image_name('enrage'/'8ED', 'enrage').
card_uid('enrage'/'8ED', '8ED:Enrage:enrage').
card_rarity('enrage'/'8ED', 'Uncommon').
card_artist('enrage'/'8ED', 'Justin Sweet').
card_number('enrage'/'8ED', '185').
card_flavor_text('enrage'/'8ED', '\"You wouldn\'t like me when I\'m angry.\"').
card_multiverse_id('enrage'/'8ED', '45357').

card_in_set('ensnaring bridge', '8ED').
card_original_type('ensnaring bridge'/'8ED', 'Artifact').
card_original_text('ensnaring bridge'/'8ED', 'Creatures with power greater than the number of cards in your hand can\'t attack.').
card_image_name('ensnaring bridge'/'8ED', 'ensnaring bridge').
card_uid('ensnaring bridge'/'8ED', '8ED:Ensnaring Bridge:ensnaring bridge').
card_rarity('ensnaring bridge'/'8ED', 'Rare').
card_artist('ensnaring bridge'/'8ED', 'Ron Spencer').
card_number('ensnaring bridge'/'8ED', '300').
card_flavor_text('ensnaring bridge'/'8ED', '\"Why build a bridge that needs to be guarded?\"\n—Elvish champion').
card_multiverse_id('ensnaring bridge'/'8ED', '45478').

card_in_set('evacuation', '8ED').
card_original_type('evacuation'/'8ED', 'Instant').
card_original_text('evacuation'/'8ED', 'Return all creatures to their owners\' hands.').
card_image_name('evacuation'/'8ED', 'evacuation').
card_uid('evacuation'/'8ED', '8ED:Evacuation:evacuation').
card_rarity('evacuation'/'8ED', 'Rare').
card_artist('evacuation'/'8ED', 'Rob Alexander').
card_number('evacuation'/'8ED', '76').
card_flavor_text('evacuation'/'8ED', 'Which is more unsettling: the roar of battle or the silence that follows?').
card_multiverse_id('evacuation'/'8ED', '45281').

card_in_set('execute', '8ED').
card_original_type('execute'/'8ED', 'Instant').
card_original_text('execute'/'8ED', 'Destroy target white creature. It can\'t be regenerated.\nDraw a card.').
card_image_name('execute'/'8ED', 'execute').
card_uid('execute'/'8ED', '8ED:Execute:execute').
card_rarity('execute'/'8ED', 'Uncommon').
card_artist('execute'/'8ED', 'Gary Ruddell').
card_number('execute'/'8ED', '132').
card_flavor_text('execute'/'8ED', '\"Any fool who would die for honor is better off dead.\"\n—Cabal Patriarch').
card_multiverse_id('execute'/'8ED', '45315').

card_in_set('fallen angel', '8ED').
card_original_type('fallen angel'/'8ED', 'Creature — Angel').
card_original_text('fallen angel'/'8ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\nSacrifice a creature: Fallen Angel gets +2/+1 until end of turn.').
card_image_name('fallen angel'/'8ED', 'fallen angel').
card_uid('fallen angel'/'8ED', '8ED:Fallen Angel:fallen angel').
card_rarity('fallen angel'/'8ED', 'Rare').
card_artist('fallen angel'/'8ED', 'Matthew D. Wilson').
card_number('fallen angel'/'8ED', '133').
card_flavor_text('fallen angel'/'8ED', '\"Why do you weep for the dead? I rejoice, for they have died for me.\"').
card_multiverse_id('fallen angel'/'8ED', '45330').

card_in_set('fear', '8ED').
card_original_type('fear'/'8ED', 'Enchant Creature').
card_original_text('fear'/'8ED', 'Enchanted creature has fear. (It can\'t be blocked except by artifact creatures and/or black creatures.)').
card_image_name('fear'/'8ED', 'fear').
card_uid('fear'/'8ED', '8ED:Fear:fear').
card_rarity('fear'/'8ED', 'Common').
card_artist('fear'/'8ED', 'Adam Rex').
card_number('fear'/'8ED', '134').
card_flavor_text('fear'/'8ED', '\"The horror The horror\"\n—Joseph Conrad, Heart of Darkness').
card_multiverse_id('fear'/'8ED', '45299').

card_in_set('fecundity', '8ED').
card_original_type('fecundity'/'8ED', 'Enchantment').
card_original_text('fecundity'/'8ED', 'Whenever a creature is put into a graveyard from play, that creature\'s controller may draw a card.').
card_image_name('fecundity'/'8ED', 'fecundity').
card_uid('fecundity'/'8ED', '8ED:Fecundity:fecundity').
card_rarity('fecundity'/'8ED', 'Uncommon').
card_artist('fecundity'/'8ED', 'Rebecca Guay').
card_number('fecundity'/'8ED', '247').
card_flavor_text('fecundity'/'8ED', 'Life is eternal. A lifetime is ephemeral.').
card_multiverse_id('fecundity'/'8ED', '45451').

card_in_set('fertile ground', '8ED').
card_original_type('fertile ground'/'8ED', 'Enchant Land').
card_original_text('fertile ground'/'8ED', 'Whenever enchanted land is tapped for mana, its controller adds one mana of any color to his or her mana pool.').
card_image_name('fertile ground'/'8ED', 'fertile ground').
card_uid('fertile ground'/'8ED', '8ED:Fertile Ground:fertile ground').
card_rarity('fertile ground'/'8ED', 'Common').
card_artist('fertile ground'/'8ED', 'Heather Hudson').
card_number('fertile ground'/'8ED', '248').
card_flavor_text('fertile ground'/'8ED', '\"The love of nature . . . is a furious, burning, physical greed. . . .\"\n—Mary Webb, The House in Dormer Forest').
card_multiverse_id('fertile ground'/'8ED', '45416').

card_in_set('fighting drake', '8ED').
card_original_type('fighting drake'/'8ED', 'Creature — Drake').
card_original_text('fighting drake'/'8ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)').
card_image_name('fighting drake'/'8ED', 'fighting drake').
card_uid('fighting drake'/'8ED', '8ED:Fighting Drake:fighting drake').
card_rarity('fighting drake'/'8ED', 'Uncommon').
card_artist('fighting drake'/'8ED', 'Matt Cavotta').
card_number('fighting drake'/'8ED', '77').
card_flavor_text('fighting drake'/'8ED', 'Scholars in their ivory towers call them \"sharks of the sky.\" Scholars on the road don\'t call them at all.').
card_multiverse_id('fighting drake'/'8ED', '45253').

card_in_set('flash counter', '8ED').
card_original_type('flash counter'/'8ED', 'Instant').
card_original_text('flash counter'/'8ED', 'Counter target instant spell.').
card_image_name('flash counter'/'8ED', 'flash counter').
card_uid('flash counter'/'8ED', '8ED:Flash Counter:flash counter').
card_rarity('flash counter'/'8ED', 'Common').
card_artist('flash counter'/'8ED', 'Matt Cavotta').
card_number('flash counter'/'8ED', '78').
card_flavor_text('flash counter'/'8ED', '\"So much for that bright idea!\"').
card_multiverse_id('flash counter'/'8ED', '45238').

card_in_set('flashfires', '8ED').
card_original_type('flashfires'/'8ED', 'Sorcery').
card_original_text('flashfires'/'8ED', 'Destroy all Plains.').
card_image_name('flashfires'/'8ED', 'flashfires').
card_uid('flashfires'/'8ED', '8ED:Flashfires:flashfires').
card_rarity('flashfires'/'8ED', 'Uncommon').
card_artist('flashfires'/'8ED', 'Randy Gallegos').
card_number('flashfires'/'8ED', '186').
card_flavor_text('flashfires'/'8ED', 'Dry grass is tinder before the spark.').
card_multiverse_id('flashfires'/'8ED', '45372').

card_in_set('fleeting image', '8ED').
card_original_type('fleeting image'/'8ED', 'Creature — Illusion').
card_original_text('fleeting image'/'8ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\n{1}{U}: Return Fleeting Image to its owner\'s hand.').
card_image_name('fleeting image'/'8ED', 'fleeting image').
card_uid('fleeting image'/'8ED', '8ED:Fleeting Image:fleeting image').
card_rarity('fleeting image'/'8ED', 'Rare').
card_artist('fleeting image'/'8ED', 'Dave Dorman').
card_number('fleeting image'/'8ED', '79').
card_flavor_text('fleeting image'/'8ED', '\"Beware lest you lose the substance by grasping at the shadow.\"\n—Aesop, Fables, trans. Jacobs').
card_multiverse_id('fleeting image'/'8ED', '45265').

card_in_set('flight', '8ED').
card_original_type('flight'/'8ED', 'Enchant Creature').
card_original_text('flight'/'8ED', 'Enchanted creature has flying. (It can\'t be blocked except by creatures with flying.)').
card_image_name('flight'/'8ED', 'flight').
card_uid('flight'/'8ED', '8ED:Flight:flight').
card_rarity('flight'/'8ED', 'Common').
card_artist('flight'/'8ED', 'Arnie Swekel').
card_number('flight'/'8ED', '80').
card_multiverse_id('flight'/'8ED', '45236').

card_in_set('flying carpet', '8ED').
card_original_type('flying carpet'/'8ED', 'Artifact').
card_original_text('flying carpet'/'8ED', '{2}, {T}: Target creature gains flying until end of turn. (It can\'t be blocked except by creatures with flying.)').
card_image_name('flying carpet'/'8ED', 'flying carpet').
card_uid('flying carpet'/'8ED', '8ED:Flying Carpet:flying carpet').
card_rarity('flying carpet'/'8ED', 'Rare').
card_artist('flying carpet'/'8ED', 'Dany Orizio').
card_number('flying carpet'/'8ED', '301').
card_flavor_text('flying carpet'/'8ED', 'Woven from strands of griffin mane and pegasus tail.').
card_multiverse_id('flying carpet'/'8ED', '45489').

card_in_set('fodder cannon', '8ED').
card_original_type('fodder cannon'/'8ED', 'Artifact').
card_original_text('fodder cannon'/'8ED', '{4}, {T}, Sacrifice a creature: Fodder Cannon deals 4 damage to target creature.').
card_image_name('fodder cannon'/'8ED', 'fodder cannon').
card_uid('fodder cannon'/'8ED', '8ED:Fodder Cannon:fodder cannon').
card_rarity('fodder cannon'/'8ED', 'Uncommon').
card_artist('fodder cannon'/'8ED', 'Christopher Moeller').
card_number('fodder cannon'/'8ED', '302').
card_flavor_text('fodder cannon'/'8ED', 'Step 1: Find your cousin.\nStep 2: Get your cousin in the cannon.\nStep 3: Find another cousin.').
card_multiverse_id('fodder cannon'/'8ED', '45467').

card_in_set('foratog', '8ED').
card_original_type('foratog'/'8ED', 'Creature — Atog').
card_original_text('foratog'/'8ED', '{G}, Sacrifice a Forest: Foratog gets +2/+2 until end of turn.').
card_image_name('foratog'/'8ED', 'foratog').
card_uid('foratog'/'8ED', '8ED:Foratog:foratog').
card_rarity('foratog'/'8ED', 'Uncommon').
card_artist('foratog'/'8ED', 'Mark Poole').
card_number('foratog'/'8ED', '249').
card_flavor_text('foratog'/'8ED', 'Five hundred years to grow—barely a minute to eat.').
card_multiverse_id('foratog'/'8ED', '45424').

card_in_set('forest', '8ED').
card_original_type('forest'/'8ED', 'Basic Land — Forest').
card_original_text('forest'/'8ED', 'G').
card_image_name('forest'/'8ED', 'forest1').
card_uid('forest'/'8ED', '8ED:Forest:forest1').
card_rarity('forest'/'8ED', 'Basic Land').
card_artist('forest'/'8ED', 'John Avon').
card_number('forest'/'8ED', '347').
card_multiverse_id('forest'/'8ED', '46453').

card_in_set('forest', '8ED').
card_original_type('forest'/'8ED', 'Basic Land — Forest').
card_original_text('forest'/'8ED', 'G').
card_image_name('forest'/'8ED', 'forest2').
card_uid('forest'/'8ED', '8ED:Forest:forest2').
card_rarity('forest'/'8ED', 'Basic Land').
card_artist('forest'/'8ED', 'John Avon').
card_number('forest'/'8ED', '348').
card_multiverse_id('forest'/'8ED', '46454').

card_in_set('forest', '8ED').
card_original_type('forest'/'8ED', 'Basic Land — Forest').
card_original_text('forest'/'8ED', 'G').
card_image_name('forest'/'8ED', 'forest3').
card_uid('forest'/'8ED', '8ED:Forest:forest3').
card_rarity('forest'/'8ED', 'Basic Land').
card_artist('forest'/'8ED', 'John Avon').
card_number('forest'/'8ED', '349').
card_multiverse_id('forest'/'8ED', '46455').

card_in_set('forest', '8ED').
card_original_type('forest'/'8ED', 'Basic Land — Forest').
card_original_text('forest'/'8ED', 'G').
card_image_name('forest'/'8ED', 'forest4').
card_uid('forest'/'8ED', '8ED:Forest:forest4').
card_rarity('forest'/'8ED', 'Basic Land').
card_artist('forest'/'8ED', 'John Avon').
card_number('forest'/'8ED', '350').
card_multiverse_id('forest'/'8ED', '46456').

card_in_set('fugitive wizard', '8ED').
card_original_type('fugitive wizard'/'8ED', 'Creature — Wizard').
card_original_text('fugitive wizard'/'8ED', '').
card_image_name('fugitive wizard'/'8ED', 'fugitive wizard').
card_uid('fugitive wizard'/'8ED', '8ED:Fugitive Wizard:fugitive wizard').
card_rarity('fugitive wizard'/'8ED', 'Common').
card_artist('fugitive wizard'/'8ED', 'Jim Nelson').
card_number('fugitive wizard'/'8ED', '81').
card_flavor_text('fugitive wizard'/'8ED', 'The College of Lat-Nam is often forced to expel students whose experiments grow too risky or too cruel.').
card_multiverse_id('fugitive wizard'/'8ED', '45225').

card_in_set('fungusaur', '8ED').
card_original_type('fungusaur'/'8ED', 'Creature — Fungusaur').
card_original_text('fungusaur'/'8ED', 'Whenever Fungusaur is dealt damage, put a +1/+1 counter on it. (The damage is dealt before the counter is put on.)').
card_image_name('fungusaur'/'8ED', 'fungusaur').
card_uid('fungusaur'/'8ED', '8ED:Fungusaur:fungusaur').
card_rarity('fungusaur'/'8ED', 'Rare').
card_artist('fungusaur'/'8ED', 'Heather Hudson').
card_number('fungusaur'/'8ED', '250').
card_flavor_text('fungusaur'/'8ED', 'Rather than sheltering her young, the female fungusaur often injures her own offspring, thereby ensuring their rapid growth.').
card_multiverse_id('fungusaur'/'8ED', '45441').

card_in_set('furnace of rath', '8ED').
card_original_type('furnace of rath'/'8ED', 'Enchantment').
card_original_text('furnace of rath'/'8ED', 'If a source would deal damage to a creature or player, it deals double that damage to that creature or player instead.').
card_image_name('furnace of rath'/'8ED', 'furnace of rath').
card_uid('furnace of rath'/'8ED', '8ED:Furnace of Rath:furnace of rath').
card_rarity('furnace of rath'/'8ED', 'Rare').
card_artist('furnace of rath'/'8ED', 'John Matson').
card_number('furnace of rath'/'8ED', '187').
card_flavor_text('furnace of rath'/'8ED', 'The furnace awaits the next master who would stoke the fires of apocalypse.').
card_multiverse_id('furnace of rath'/'8ED', '45398').

card_in_set('fyndhorn elder', '8ED').
card_original_type('fyndhorn elder'/'8ED', 'Creature — Elf').
card_original_text('fyndhorn elder'/'8ED', '{T}: Add {G}{G} to your mana pool.').
card_image_name('fyndhorn elder'/'8ED', 'fyndhorn elder').
card_uid('fyndhorn elder'/'8ED', '8ED:Fyndhorn Elder:fyndhorn elder').
card_rarity('fyndhorn elder'/'8ED', 'Uncommon').
card_artist('fyndhorn elder'/'8ED', 'Donato Giancola').
card_number('fyndhorn elder'/'8ED', '251').
card_flavor_text('fyndhorn elder'/'8ED', '\"It is useful to be able to speak to the trees. But it is truly wondrous to be able to listen to them.\"').
card_multiverse_id('fyndhorn elder'/'8ED', '45423').

card_in_set('gaea\'s herald', '8ED').
card_original_type('gaea\'s herald'/'8ED', 'Creature — Elf').
card_original_text('gaea\'s herald'/'8ED', 'Creature spells can\'t be countered.').
card_image_name('gaea\'s herald'/'8ED', 'gaea\'s herald').
card_uid('gaea\'s herald'/'8ED', '8ED:Gaea\'s Herald:gaea\'s herald').
card_rarity('gaea\'s herald'/'8ED', 'Rare').
card_artist('gaea\'s herald'/'8ED', 'Dan Frazier').
card_number('gaea\'s herald'/'8ED', '252').
card_flavor_text('gaea\'s herald'/'8ED', 'The elves\' battle cries cut paths through the Æther, guiding those who would stand in Gaea\'s defense.').
card_multiverse_id('gaea\'s herald'/'8ED', '45440').

card_in_set('giant badger', '8ED').
card_original_type('giant badger'/'8ED', 'Creature — Badger').
card_original_text('giant badger'/'8ED', 'Whenever Giant Badger blocks, it gets +2/+2 until end of turn.').
card_image_name('giant badger'/'8ED', 'giant badger').
card_uid('giant badger'/'8ED', '8ED:Giant Badger:giant badger').
card_rarity('giant badger'/'8ED', 'Common').
card_artist('giant badger'/'8ED', 'Lars Grant-West').
card_number('giant badger'/'8ED', '253').
card_flavor_text('giant badger'/'8ED', 'Disturb a sleeping badger and the snapping of your bones will awaken the countryside.').
card_multiverse_id('giant badger'/'8ED', '45425').

card_in_set('giant cockroach', '8ED').
card_original_type('giant cockroach'/'8ED', 'Creature — Insect').
card_original_text('giant cockroach'/'8ED', '').
card_image_name('giant cockroach'/'8ED', 'giant cockroach').
card_uid('giant cockroach'/'8ED', '8ED:Giant Cockroach:giant cockroach').
card_rarity('giant cockroach'/'8ED', 'Common').
card_artist('giant cockroach'/'8ED', 'Heather Hudson').
card_number('giant cockroach'/'8ED', '135').
card_flavor_text('giant cockroach'/'8ED', 'Toren had stepped on a lot of bugs during his life, so he couldn\'t help feeling embarrassed when a bug stepped on him.').
card_multiverse_id('giant cockroach'/'8ED', '45292').

card_in_set('giant growth', '8ED').
card_original_type('giant growth'/'8ED', 'Instant').
card_original_text('giant growth'/'8ED', 'Target creature gets +3/+3 until end of turn.').
card_image_name('giant growth'/'8ED', 'giant growth').
card_uid('giant growth'/'8ED', '8ED:Giant Growth:giant growth').
card_rarity('giant growth'/'8ED', 'Common').
card_artist('giant growth'/'8ED', 'DiTerlizzi').
card_number('giant growth'/'8ED', '254').
card_multiverse_id('giant growth'/'8ED', '45414').

card_in_set('giant octopus', '8ED').
card_original_type('giant octopus'/'8ED', 'Creature — Octopus').
card_original_text('giant octopus'/'8ED', '').
card_image_name('giant octopus'/'8ED', 'giant octopus').
card_uid('giant octopus'/'8ED', '8ED:Giant Octopus:giant octopus').
card_rarity('giant octopus'/'8ED', 'Common').
card_artist('giant octopus'/'8ED', 'Heather Hudson').
card_number('giant octopus'/'8ED', 'S3').
card_flavor_text('giant octopus'/'8ED', '\"Before my eyes was a horrible monster, worthy to figure in the legends of the marvellous. . . . Its eight arms, or rather feet, fixed to its head . . . were twice as long as its body, and were twisted like the furies\' hair.\"\n—Jules Verne, Twenty Thousand Leagues under the Sea, trans. Lewis').
card_multiverse_id('giant octopus'/'8ED', '47785').

card_in_set('giant spider', '8ED').
card_original_type('giant spider'/'8ED', 'Creature — Spider').
card_original_text('giant spider'/'8ED', 'Giant Spider may block as though it had flying.').
card_image_name('giant spider'/'8ED', 'giant spider').
card_uid('giant spider'/'8ED', '8ED:Giant Spider:giant spider').
card_rarity('giant spider'/'8ED', 'Common').
card_artist('giant spider'/'8ED', 'Randy Gallegos').
card_number('giant spider'/'8ED', '255').
card_flavor_text('giant spider'/'8ED', 'Watching the spider\'s web\n—Llanowar expression meaning\n\"focusing on the wrong thing\"').
card_multiverse_id('giant spider'/'8ED', '45408').

card_in_set('glorious anthem', '8ED').
card_original_type('glorious anthem'/'8ED', 'Enchantment').
card_original_text('glorious anthem'/'8ED', 'Creatures you control get +1/+1.').
card_image_name('glorious anthem'/'8ED', 'glorious anthem').
card_uid('glorious anthem'/'8ED', '8ED:Glorious Anthem:glorious anthem').
card_rarity('glorious anthem'/'8ED', 'Rare').
card_artist('glorious anthem'/'8ED', 'Kev Walker').
card_number('glorious anthem'/'8ED', '20').
card_flavor_text('glorious anthem'/'8ED', 'Each victory adds a new verse.').
card_multiverse_id('glorious anthem'/'8ED', '45218').

card_in_set('glory seeker', '8ED').
card_original_type('glory seeker'/'8ED', 'Creature — Soldier').
card_original_text('glory seeker'/'8ED', '').
card_image_name('glory seeker'/'8ED', 'glory seeker').
card_uid('glory seeker'/'8ED', '8ED:Glory Seeker:glory seeker').
card_rarity('glory seeker'/'8ED', 'Common').
card_artist('glory seeker'/'8ED', 'Dave Dorman').
card_number('glory seeker'/'8ED', '21').
card_flavor_text('glory seeker'/'8ED', 'The turning of the tide always begins with one soldier\'s decision to head back into the fray.').
card_multiverse_id('glory seeker'/'8ED', '45173').

card_in_set('gluttonous zombie', '8ED').
card_original_type('gluttonous zombie'/'8ED', 'Creature — Zombie').
card_original_text('gluttonous zombie'/'8ED', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)').
card_image_name('gluttonous zombie'/'8ED', 'gluttonous zombie').
card_uid('gluttonous zombie'/'8ED', '8ED:Gluttonous Zombie:gluttonous zombie').
card_rarity('gluttonous zombie'/'8ED', 'Uncommon').
card_artist('gluttonous zombie'/'8ED', 'Thomas M. Baxa').
card_number('gluttonous zombie'/'8ED', '136').
card_flavor_text('gluttonous zombie'/'8ED', 'Greed and gluttony led him to death. Now they are his greatest assets.').
card_multiverse_id('gluttonous zombie'/'8ED', '47282').

card_in_set('goblin chariot', '8ED').
card_original_type('goblin chariot'/'8ED', 'Creature — Goblin').
card_original_text('goblin chariot'/'8ED', 'Haste (This creature may attack the turn it comes under your control.)').
card_image_name('goblin chariot'/'8ED', 'goblin chariot').
card_uid('goblin chariot'/'8ED', '8ED:Goblin Chariot:goblin chariot').
card_rarity('goblin chariot'/'8ED', 'Common').
card_artist('goblin chariot'/'8ED', 'John Howe').
card_number('goblin chariot'/'8ED', '188').
card_flavor_text('goblin chariot'/'8ED', 'The hardest part of his training is learning not to eat the boar.').
card_multiverse_id('goblin chariot'/'8ED', '45344').

card_in_set('goblin glider', '8ED').
card_original_type('goblin glider'/'8ED', 'Creature — Goblin').
card_original_text('goblin glider'/'8ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\nGoblin Glider can\'t block.').
card_image_name('goblin glider'/'8ED', 'goblin glider').
card_uid('goblin glider'/'8ED', '8ED:Goblin Glider:goblin glider').
card_rarity('goblin glider'/'8ED', 'Uncommon').
card_artist('goblin glider'/'8ED', 'Patrick Faricy').
card_number('goblin glider'/'8ED', '189').
card_flavor_text('goblin glider'/'8ED', '\"What\'s got two arms, one wing, and no brains?\"\n—Ghitu riddle').
card_multiverse_id('goblin glider'/'8ED', '45363').

card_in_set('goblin king', '8ED').
card_original_type('goblin king'/'8ED', 'Creature — Lord').
card_original_text('goblin king'/'8ED', 'All Goblins get +1/+1 and have mountainwalk. (They\'re unblockable as long as defending player controls a Mountain.)').
card_image_name('goblin king'/'8ED', 'goblin king').
card_uid('goblin king'/'8ED', '8ED:Goblin King:goblin king').
card_rarity('goblin king'/'8ED', 'Rare').
card_artist('goblin king'/'8ED', 'Ron Spears').
card_number('goblin king'/'8ED', '190').
card_flavor_text('goblin king'/'8ED', 'To be king, Numsgil did in Blog, who did in Unkful, who did in Viddle, who did in Loll, who did in Alrok. . . .').
card_multiverse_id('goblin king'/'8ED', '45384').

card_in_set('goblin raider', '8ED').
card_original_type('goblin raider'/'8ED', 'Creature — Goblin').
card_original_text('goblin raider'/'8ED', 'Goblin Raider can\'t block.').
card_image_name('goblin raider'/'8ED', 'goblin raider').
card_uid('goblin raider'/'8ED', '8ED:Goblin Raider:goblin raider').
card_rarity('goblin raider'/'8ED', 'Common').
card_artist('goblin raider'/'8ED', 'Greg Staples').
card_number('goblin raider'/'8ED', '191').
card_flavor_text('goblin raider'/'8ED', 'He\'s smart for a goblin. He can do two things: hit and run.').
card_multiverse_id('goblin raider'/'8ED', '45343').

card_in_set('grave pact', '8ED').
card_original_type('grave pact'/'8ED', 'Enchantment').
card_original_text('grave pact'/'8ED', 'Whenever a creature you control is put into a graveyard from play, each other player sacrifices a creature.').
card_image_name('grave pact'/'8ED', 'grave pact').
card_uid('grave pact'/'8ED', '8ED:Grave Pact:grave pact').
card_rarity('grave pact'/'8ED', 'Rare').
card_artist('grave pact'/'8ED', 'Tony Szczudlo').
card_number('grave pact'/'8ED', '137').
card_flavor_text('grave pact'/'8ED', '\"The bonds of loyalty can tie one to the grave.\"\n—Crovax, ascendant evincar').
card_multiverse_id('grave pact'/'8ED', '45336').

card_in_set('gravedigger', '8ED').
card_original_type('gravedigger'/'8ED', 'Creature — Zombie').
card_original_text('gravedigger'/'8ED', 'When Gravedigger comes into play, you may return target creature card from your graveyard to your hand.').
card_image_name('gravedigger'/'8ED', 'gravedigger').
card_uid('gravedigger'/'8ED', '8ED:Gravedigger:gravedigger').
card_rarity('gravedigger'/'8ED', 'Common').
card_artist('gravedigger'/'8ED', 'Dermot Power').
card_number('gravedigger'/'8ED', '138').
card_flavor_text('gravedigger'/'8ED', 'A full coffin is like a full coffer—both are attractive to thieves.').
card_multiverse_id('gravedigger'/'8ED', '45293').

card_in_set('grizzly bears', '8ED').
card_original_type('grizzly bears'/'8ED', 'Creature — Bear').
card_original_text('grizzly bears'/'8ED', '').
card_image_name('grizzly bears'/'8ED', 'grizzly bears').
card_uid('grizzly bears'/'8ED', '8ED:Grizzly Bears:grizzly bears').
card_rarity('grizzly bears'/'8ED', 'Common').
card_artist('grizzly bears'/'8ED', 'D. J. Cleland-Hura').
card_number('grizzly bears'/'8ED', '256').
card_flavor_text('grizzly bears'/'8ED', 'Don\'t try to outrun one of Dominaria\'s grizzlies; it\'ll catch you, knock you down, and eat you. Of course, you could run up a tree. In that case you\'ll get a nice view before it knocks the tree down and eats you.').
card_multiverse_id('grizzly bears'/'8ED', '45402').

card_in_set('guerrilla tactics', '8ED').
card_original_type('guerrilla tactics'/'8ED', 'Instant').
card_original_text('guerrilla tactics'/'8ED', 'Guerrilla Tactics deals 2 damage to target creature or player.\nWhen a spell or ability an opponent controls causes you to discard Guerrilla Tactics from your hand, Guerrilla Tactics deals 4 damage to target creature or player.').
card_image_name('guerrilla tactics'/'8ED', 'guerrilla tactics').
card_uid('guerrilla tactics'/'8ED', '8ED:Guerrilla Tactics:guerrilla tactics').
card_rarity('guerrilla tactics'/'8ED', 'Uncommon').
card_artist('guerrilla tactics'/'8ED', 'Ron Spencer').
card_number('guerrilla tactics'/'8ED', '192').
card_multiverse_id('guerrilla tactics'/'8ED', '45376').

card_in_set('hammer of bogardan', '8ED').
card_original_type('hammer of bogardan'/'8ED', 'Sorcery').
card_original_text('hammer of bogardan'/'8ED', 'Hammer of Bogardan deals 3 damage to target creature or player.\n{2}{R}{R}{R}: Return Hammer of Bogardan from your graveyard to your hand. Play this ability only during your upkeep. (Your upkeep step is after you untap and before you draw.)').
card_image_name('hammer of bogardan'/'8ED', 'hammer of bogardan').
card_uid('hammer of bogardan'/'8ED', '8ED:Hammer of Bogardan:hammer of bogardan').
card_rarity('hammer of bogardan'/'8ED', 'Rare').
card_artist('hammer of bogardan'/'8ED', 'Ron Spencer').
card_number('hammer of bogardan'/'8ED', '193').
card_multiverse_id('hammer of bogardan'/'8ED', '49039').

card_in_set('healing salve', '8ED').
card_original_type('healing salve'/'8ED', 'Instant').
card_original_text('healing salve'/'8ED', 'Choose one Target player gains 3 life; or prevent the next 3 damage that would be dealt to target creature or player this turn.').
card_image_name('healing salve'/'8ED', 'healing salve').
card_uid('healing salve'/'8ED', '8ED:Healing Salve:healing salve').
card_rarity('healing salve'/'8ED', 'Common').
card_artist('healing salve'/'8ED', 'Greg & Tim Hildebrandt').
card_number('healing salve'/'8ED', '22').
card_multiverse_id('healing salve'/'8ED', '45180').

card_in_set('hibernation', '8ED').
card_original_type('hibernation'/'8ED', 'Instant').
card_original_text('hibernation'/'8ED', 'Return all green permanents to their owners\' hands.').
card_image_name('hibernation'/'8ED', 'hibernation').
card_uid('hibernation'/'8ED', '8ED:Hibernation:hibernation').
card_rarity('hibernation'/'8ED', 'Uncommon').
card_artist('hibernation'/'8ED', 'Matt Cavotta').
card_number('hibernation'/'8ED', '82').
card_flavor_text('hibernation'/'8ED', 'The deep sleep of winter is not always restful.').
card_multiverse_id('hibernation'/'8ED', '47600').

card_in_set('hill giant', '8ED').
card_original_type('hill giant'/'8ED', 'Creature — Giant').
card_original_text('hill giant'/'8ED', '').
card_image_name('hill giant'/'8ED', 'hill giant').
card_uid('hill giant'/'8ED', '8ED:Hill Giant:hill giant').
card_rarity('hill giant'/'8ED', 'Common').
card_artist('hill giant'/'8ED', 'Dany Orizio').
card_number('hill giant'/'8ED', '194').
card_flavor_text('hill giant'/'8ED', 'Bent on a quest for blood, a giant can storm through a fort\'s wall as if it were nothing more than a picket fence.').
card_multiverse_id('hill giant'/'8ED', '45348').

card_in_set('holy day', '8ED').
card_original_type('holy day'/'8ED', 'Instant').
card_original_text('holy day'/'8ED', 'Prevent all combat damage that would be dealt this turn.').
card_image_name('holy day'/'8ED', 'holy day').
card_uid('holy day'/'8ED', '8ED:Holy Day:holy day').
card_rarity('holy day'/'8ED', 'Common').
card_artist('holy day'/'8ED', 'Pete Venters').
card_number('holy day'/'8ED', '23').
card_flavor_text('holy day'/'8ED', '\"The day of Spirits; my soul\'s calm retreat / Which none disturb\"\n—Henry Vaughan, \"The Night\"').
card_multiverse_id('holy day'/'8ED', '45181').

card_in_set('holy strength', '8ED').
card_original_type('holy strength'/'8ED', 'Enchant Creature').
card_original_text('holy strength'/'8ED', 'Enchanted creature gets +1/+2.').
card_image_name('holy strength'/'8ED', 'holy strength').
card_uid('holy strength'/'8ED', '8ED:Holy Strength:holy strength').
card_rarity('holy strength'/'8ED', 'Common').
card_artist('holy strength'/'8ED', 'Scott M. Fischer').
card_number('holy strength'/'8ED', '24').
card_multiverse_id('holy strength'/'8ED', '45182').

card_in_set('honor guard', '8ED').
card_original_type('honor guard'/'8ED', 'Creature — Soldier').
card_original_text('honor guard'/'8ED', '{W}: Honor Guard gets +0/+1 until end of turn.').
card_image_name('honor guard'/'8ED', 'honor guard').
card_uid('honor guard'/'8ED', '8ED:Honor Guard:honor guard').
card_rarity('honor guard'/'8ED', 'Common').
card_artist('honor guard'/'8ED', 'Mark Zug').
card_number('honor guard'/'8ED', '25').
card_flavor_text('honor guard'/'8ED', '\"It is not a choice I make, to have this guard. It is the choice of my people, and my duty to them.\"\n—Oracle en-Vec').
card_multiverse_id('honor guard'/'8ED', '45168').

card_in_set('horned troll', '8ED').
card_original_type('horned troll'/'8ED', 'Creature — Troll').
card_original_text('horned troll'/'8ED', '{G}: Regenerate Horned Troll.').
card_image_name('horned troll'/'8ED', 'horned troll').
card_uid('horned troll'/'8ED', '8ED:Horned Troll:horned troll').
card_rarity('horned troll'/'8ED', 'Common').
card_artist('horned troll'/'8ED', 'Christopher Moeller').
card_number('horned troll'/'8ED', '257').
card_flavor_text('horned troll'/'8ED', 'Sword hilts jut from some trolls\' bodies where wounds healed so fast that the weapons were trapped inside.').
card_multiverse_id('horned troll'/'8ED', '45404').

card_in_set('horned turtle', '8ED').
card_original_type('horned turtle'/'8ED', 'Creature — Turtle').
card_original_text('horned turtle'/'8ED', '').
card_image_name('horned turtle'/'8ED', 'horned turtle').
card_uid('horned turtle'/'8ED', '8ED:Horned Turtle:horned turtle').
card_rarity('horned turtle'/'8ED', 'Common').
card_artist('horned turtle'/'8ED', 'DiTerlizzi').
card_number('horned turtle'/'8ED', '83').
card_flavor_text('horned turtle'/'8ED', 'It doesn\'t like having visitors . . . unless it\'s having them for lunch.').
card_multiverse_id('horned turtle'/'8ED', '45229').

card_in_set('howling mine', '8ED').
card_original_type('howling mine'/'8ED', 'Artifact').
card_original_text('howling mine'/'8ED', 'At the beginning of each player\'s draw step, if Howling Mine is untapped, that player draws a card.').
card_image_name('howling mine'/'8ED', 'howling mine').
card_uid('howling mine'/'8ED', '8ED:Howling Mine:howling mine').
card_rarity('howling mine'/'8ED', 'Rare').
card_artist('howling mine'/'8ED', 'Dana Knutson').
card_number('howling mine'/'8ED', '303').
card_flavor_text('howling mine'/'8ED', 'Legend has it that the mine howls out the last words of those who died inside.').
card_multiverse_id('howling mine'/'8ED', '45474').

card_in_set('hulking cyclops', '8ED').
card_original_type('hulking cyclops'/'8ED', 'Creature — Giant').
card_original_text('hulking cyclops'/'8ED', 'Hulking Cyclops can\'t block.').
card_image_name('hulking cyclops'/'8ED', 'hulking cyclops').
card_uid('hulking cyclops'/'8ED', '8ED:Hulking Cyclops:hulking cyclops').
card_rarity('hulking cyclops'/'8ED', 'Uncommon').
card_artist('hulking cyclops'/'8ED', 'Paolo Parente').
card_number('hulking cyclops'/'8ED', '195').
card_flavor_text('hulking cyclops'/'8ED', 'Anyone can get around a cyclops, but few can stand in its way.').
card_multiverse_id('hulking cyclops'/'8ED', '45371').

card_in_set('hunted wumpus', '8ED').
card_original_type('hunted wumpus'/'8ED', 'Creature — Beast').
card_original_text('hunted wumpus'/'8ED', 'When Hunted Wumpus comes into play, each other player may put a creature card from his or her hand into play.').
card_image_name('hunted wumpus'/'8ED', 'hunted wumpus').
card_uid('hunted wumpus'/'8ED', '8ED:Hunted Wumpus:hunted wumpus').
card_rarity('hunted wumpus'/'8ED', 'Uncommon').
card_artist('hunted wumpus'/'8ED', 'Thomas M. Baxa').
card_number('hunted wumpus'/'8ED', '258').
card_flavor_text('hunted wumpus'/'8ED', 'Just one can feed a dozen people for a month.').
card_multiverse_id('hunted wumpus'/'8ED', '45426').

card_in_set('index', '8ED').
card_original_type('index'/'8ED', 'Sorcery').
card_original_text('index'/'8ED', 'Look at the top five cards of your library, then put them back in any order.').
card_image_name('index'/'8ED', 'index').
card_uid('index'/'8ED', '8ED:Index:index').
card_rarity('index'/'8ED', 'Common').
card_artist('index'/'8ED', 'Kev Walker').
card_number('index'/'8ED', '84').
card_flavor_text('index'/'8ED', '\"Let\'s see . . . Mercadia, mercenary, merfolk . . . you know, I really need a better filing system.\"').
card_multiverse_id('index'/'8ED', '45259').

card_in_set('inferno', '8ED').
card_original_type('inferno'/'8ED', 'Instant').
card_original_text('inferno'/'8ED', 'Inferno deals 6 damage to each creature and each player.').
card_image_name('inferno'/'8ED', 'inferno').
card_uid('inferno'/'8ED', '8ED:Inferno:inferno').
card_rarity('inferno'/'8ED', 'Rare').
card_artist('inferno'/'8ED', 'Don Hazeltine').
card_number('inferno'/'8ED', '196').
card_flavor_text('inferno'/'8ED', '\"Some have said there is no subtlety to destruction. You know what? They\'re dead.\"\n—Jaya Ballard, task mage').
card_multiverse_id('inferno'/'8ED', '45394').

card_in_set('inspiration', '8ED').
card_original_type('inspiration'/'8ED', 'Instant').
card_original_text('inspiration'/'8ED', 'Target player draws two cards.').
card_image_name('inspiration'/'8ED', 'inspiration').
card_uid('inspiration'/'8ED', '8ED:Inspiration:inspiration').
card_rarity('inspiration'/'8ED', 'Common').
card_artist('inspiration'/'8ED', 'Matt Cavotta').
card_number('inspiration'/'8ED', '85').
card_flavor_text('inspiration'/'8ED', 'Madness and genius are separated only by degrees of success.').
card_multiverse_id('inspiration'/'8ED', '45246').

card_in_set('intrepid hero', '8ED').
card_original_type('intrepid hero'/'8ED', 'Creature — Soldier').
card_original_text('intrepid hero'/'8ED', '{T}: Destroy target creature with power 4 or greater.').
card_image_name('intrepid hero'/'8ED', 'intrepid hero').
card_uid('intrepid hero'/'8ED', '8ED:Intrepid Hero:intrepid hero').
card_rarity('intrepid hero'/'8ED', 'Rare').
card_artist('intrepid hero'/'8ED', 'Greg Hildebrandt').
card_number('intrepid hero'/'8ED', '26').
card_flavor_text('intrepid hero'/'8ED', 'A fool knows no fear. A hero shows no fear.').
card_multiverse_id('intrepid hero'/'8ED', '45208').

card_in_set('intruder alarm', '8ED').
card_original_type('intruder alarm'/'8ED', 'Enchantment').
card_original_text('intruder alarm'/'8ED', 'Creatures don\'t untap during their controllers\' untap steps.\nWhenever a creature comes into play, untap all creatures.').
card_image_name('intruder alarm'/'8ED', 'intruder alarm').
card_uid('intruder alarm'/'8ED', '8ED:Intruder Alarm:intruder alarm').
card_rarity('intruder alarm'/'8ED', 'Rare').
card_artist('intruder alarm'/'8ED', 'Luca Zontini').
card_number('intruder alarm'/'8ED', '86').
card_flavor_text('intruder alarm'/'8ED', 'One footstep among many is silent. One footstep alone is deafening.').
card_multiverse_id('intruder alarm'/'8ED', '45258').

card_in_set('invisibility', '8ED').
card_original_type('invisibility'/'8ED', 'Enchant Creature').
card_original_text('invisibility'/'8ED', 'Enchanted creature can\'t be blocked except by Walls.').
card_image_name('invisibility'/'8ED', 'invisibility').
card_uid('invisibility'/'8ED', '8ED:Invisibility:invisibility').
card_rarity('invisibility'/'8ED', 'Uncommon').
card_artist('invisibility'/'8ED', 'Pete Venters').
card_number('invisibility'/'8ED', '87').
card_flavor_text('invisibility'/'8ED', 'Verick held his breath. Breathing wouldn\'t reveal his position, but it would force him to smell the goblins.').
card_multiverse_id('invisibility'/'8ED', '45237').

card_in_set('iron star', '8ED').
card_original_type('iron star'/'8ED', 'Artifact').
card_original_text('iron star'/'8ED', 'Whenever a player plays a red spell, you may pay {1}. If you do, you gain 1 life.').
card_image_name('iron star'/'8ED', 'iron star').
card_uid('iron star'/'8ED', '8ED:Iron Star:iron star').
card_rarity('iron star'/'8ED', 'Uncommon').
card_artist('iron star'/'8ED', 'Donato Giancola').
card_number('iron star'/'8ED', '304').
card_flavor_text('iron star'/'8ED', 'Forged by a thousand flames.').
card_multiverse_id('iron star'/'8ED', '45461').

card_in_set('island', '8ED').
card_original_type('island'/'8ED', 'Basic Land — Island').
card_original_text('island'/'8ED', 'U').
card_image_name('island'/'8ED', 'island1').
card_uid('island'/'8ED', '8ED:Island:island1').
card_rarity('island'/'8ED', 'Basic Land').
card_artist('island'/'8ED', 'John Avon').
card_number('island'/'8ED', '335').
card_multiverse_id('island'/'8ED', '46441').

card_in_set('island', '8ED').
card_original_type('island'/'8ED', 'Basic Land — Island').
card_original_text('island'/'8ED', 'U').
card_image_name('island'/'8ED', 'island2').
card_uid('island'/'8ED', '8ED:Island:island2').
card_rarity('island'/'8ED', 'Basic Land').
card_artist('island'/'8ED', 'John Avon').
card_number('island'/'8ED', '336').
card_multiverse_id('island'/'8ED', '46442').

card_in_set('island', '8ED').
card_original_type('island'/'8ED', 'Basic Land — Island').
card_original_text('island'/'8ED', 'U').
card_image_name('island'/'8ED', 'island3').
card_uid('island'/'8ED', '8ED:Island:island3').
card_rarity('island'/'8ED', 'Basic Land').
card_artist('island'/'8ED', 'Tony Szczudlo').
card_number('island'/'8ED', '337').
card_multiverse_id('island'/'8ED', '46443').

card_in_set('island', '8ED').
card_original_type('island'/'8ED', 'Basic Land — Island').
card_original_text('island'/'8ED', 'U').
card_image_name('island'/'8ED', 'island4').
card_uid('island'/'8ED', '8ED:Island:island4').
card_rarity('island'/'8ED', 'Basic Land').
card_artist('island'/'8ED', 'Scott Bailey').
card_number('island'/'8ED', '338').
card_multiverse_id('island'/'8ED', '46444').

card_in_set('ivory cup', '8ED').
card_original_type('ivory cup'/'8ED', 'Artifact').
card_original_text('ivory cup'/'8ED', 'Whenever a player plays a white spell, you may pay {1}. If you do, you gain 1 life.').
card_image_name('ivory cup'/'8ED', 'ivory cup').
card_uid('ivory cup'/'8ED', '8ED:Ivory Cup:ivory cup').
card_rarity('ivory cup'/'8ED', 'Uncommon').
card_artist('ivory cup'/'8ED', 'Donato Giancola').
card_number('ivory cup'/'8ED', '305').
card_flavor_text('ivory cup'/'8ED', 'Blessed by a thousand prayers.').
card_multiverse_id('ivory cup'/'8ED', '45458').

card_in_set('ivory mask', '8ED').
card_original_type('ivory mask'/'8ED', 'Enchantment').
card_original_text('ivory mask'/'8ED', 'You can\'t be the target of spells or abilities.').
card_image_name('ivory mask'/'8ED', 'ivory mask').
card_uid('ivory mask'/'8ED', '8ED:Ivory Mask:ivory mask').
card_rarity('ivory mask'/'8ED', 'Rare').
card_artist('ivory mask'/'8ED', 'Glen Angus').
card_number('ivory mask'/'8ED', '27').
card_flavor_text('ivory mask'/'8ED', 'Made from its wearer\'s hopes, the mask ensures that those hopes will be fulfilled.').
card_multiverse_id('ivory mask'/'8ED', '45223').

card_in_set('jayemdae tome', '8ED').
card_original_type('jayemdae tome'/'8ED', 'Artifact').
card_original_text('jayemdae tome'/'8ED', '{4}, {T}: Draw a card.').
card_image_name('jayemdae tome'/'8ED', 'jayemdae tome').
card_uid('jayemdae tome'/'8ED', '8ED:Jayemdae Tome:jayemdae tome').
card_rarity('jayemdae tome'/'8ED', 'Rare').
card_artist('jayemdae tome'/'8ED', 'Donato Giancola').
card_number('jayemdae tome'/'8ED', '306').
card_flavor_text('jayemdae tome'/'8ED', '\"Knowledge is power.\"\n—Sir Francis Bacon, Meditationes Sacrae').
card_multiverse_id('jayemdae tome'/'8ED', '45482').

card_in_set('karma', '8ED').
card_original_type('karma'/'8ED', 'Enchantment').
card_original_text('karma'/'8ED', 'At the beginning of each player\'s upkeep, Karma deals damage to that player equal to the number of Swamps he or she controls. (Your upkeep step is after you untap and before you draw.)').
card_image_name('karma'/'8ED', 'karma').
card_uid('karma'/'8ED', '8ED:Karma:karma').
card_rarity('karma'/'8ED', 'Uncommon').
card_artist('karma'/'8ED', 'Bob Eggleton').
card_number('karma'/'8ED', '28').
card_multiverse_id('karma'/'8ED', '49011').

card_in_set('larceny', '8ED').
card_original_type('larceny'/'8ED', 'Enchantment').
card_original_text('larceny'/'8ED', 'Whenever a creature you control deals combat damage to a player, that player discards a card from his or her hand.').
card_image_name('larceny'/'8ED', 'larceny').
card_uid('larceny'/'8ED', '8ED:Larceny:larceny').
card_rarity('larceny'/'8ED', 'Rare').
card_artist('larceny'/'8ED', 'Dave Dorman').
card_number('larceny'/'8ED', '139').
card_flavor_text('larceny'/'8ED', 'Opportunity makes the thief.\n—Roman proverb').
card_multiverse_id('larceny'/'8ED', '45332').

card_in_set('lava axe', '8ED').
card_original_type('lava axe'/'8ED', 'Sorcery').
card_original_text('lava axe'/'8ED', 'Lava Axe deals 5 damage to target player.').
card_image_name('lava axe'/'8ED', 'lava axe').
card_uid('lava axe'/'8ED', '8ED:Lava Axe:lava axe').
card_rarity('lava axe'/'8ED', 'Common').
card_artist('lava axe'/'8ED', 'Brian Snõddy').
card_number('lava axe'/'8ED', '197').
card_flavor_text('lava axe'/'8ED', '\"Catch!\"').
card_multiverse_id('lava axe'/'8ED', '45362').

card_in_set('lava hounds', '8ED').
card_original_type('lava hounds'/'8ED', 'Creature — Hound').
card_original_text('lava hounds'/'8ED', 'Haste (This creature may attack the turn it comes under your control.)\nWhen Lava Hounds comes into play, it deals 4 damage to you.').
card_image_name('lava hounds'/'8ED', 'lava hounds').
card_uid('lava hounds'/'8ED', '8ED:Lava Hounds:lava hounds').
card_rarity('lava hounds'/'8ED', 'Rare').
card_artist('lava hounds'/'8ED', 'Steve White').
card_number('lava hounds'/'8ED', '198').
card_multiverse_id('lava hounds'/'8ED', '45383').

card_in_set('lesser gargadon', '8ED').
card_original_type('lesser gargadon'/'8ED', 'Creature — Beast').
card_original_text('lesser gargadon'/'8ED', 'Whenever Lesser Gargadon attacks or blocks, sacrifice a land.').
card_image_name('lesser gargadon'/'8ED', 'lesser gargadon').
card_uid('lesser gargadon'/'8ED', '8ED:Lesser Gargadon:lesser gargadon').
card_rarity('lesser gargadon'/'8ED', 'Uncommon').
card_artist('lesser gargadon'/'8ED', 'Rob Alexander').
card_number('lesser gargadon'/'8ED', '199').
card_flavor_text('lesser gargadon'/'8ED', '\"In some cultures, elephants are ridden by those in power. I don\'t think a mere elephant would even be noticed in Keld.\"\n—Onean sergeant').
card_multiverse_id('lesser gargadon'/'8ED', '45370').

card_in_set('lhurgoyf', '8ED').
card_original_type('lhurgoyf'/'8ED', 'Creature — Lhurgoyf').
card_original_text('lhurgoyf'/'8ED', 'Lhurgoyf\'s power is equal to the number of creature cards in all graveyards and its toughness is equal to that number plus 1.').
card_image_name('lhurgoyf'/'8ED', 'lhurgoyf').
card_uid('lhurgoyf'/'8ED', '8ED:Lhurgoyf:lhurgoyf').
card_rarity('lhurgoyf'/'8ED', 'Rare').
card_artist('lhurgoyf'/'8ED', 'Pete Venters').
card_number('lhurgoyf'/'8ED', '259').
card_flavor_text('lhurgoyf'/'8ED', '\"Ach! Hans, run! It\'s the Lhurgoyf!\"\n—Saffi Eriksdotter, last words').
card_multiverse_id('lhurgoyf'/'8ED', '46622').

card_in_set('lightning blast', '8ED').
card_original_type('lightning blast'/'8ED', 'Instant').
card_original_text('lightning blast'/'8ED', 'Lightning Blast deals 4 damage to target creature or player.').
card_image_name('lightning blast'/'8ED', 'lightning blast').
card_uid('lightning blast'/'8ED', '8ED:Lightning Blast:lightning blast').
card_rarity('lightning blast'/'8ED', 'Uncommon').
card_artist('lightning blast'/'8ED', 'Richard Thomas').
card_number('lightning blast'/'8ED', '200').
card_flavor_text('lightning blast'/'8ED', '\"Those who fear the darkness have never seen what the light can do.\"\n—Selenia, dark angel').
card_multiverse_id('lightning blast'/'8ED', '45361').

card_in_set('lightning elemental', '8ED').
card_original_type('lightning elemental'/'8ED', 'Creature — Elemental').
card_original_text('lightning elemental'/'8ED', 'Haste (This creature may attack the turn it comes under your control.)').
card_image_name('lightning elemental'/'8ED', 'lightning elemental').
card_uid('lightning elemental'/'8ED', '8ED:Lightning Elemental:lightning elemental').
card_rarity('lightning elemental'/'8ED', 'Common').
card_artist('lightning elemental'/'8ED', 'Kev Walker').
card_number('lightning elemental'/'8ED', '201').
card_flavor_text('lightning elemental'/'8ED', '\"A flash of the lightning, a break of the wave,\nHe passes from life to his rest in the grave.\"\n—William Knox, \"Mortality\"').
card_multiverse_id('lightning elemental'/'8ED', '45349').

card_in_set('living terrain', '8ED').
card_original_type('living terrain'/'8ED', 'Enchant Land').
card_original_text('living terrain'/'8ED', 'Enchanted land is a 5/6 green Treefolk creature that\'s still a land.').
card_image_name('living terrain'/'8ED', 'living terrain').
card_uid('living terrain'/'8ED', '8ED:Living Terrain:living terrain').
card_rarity('living terrain'/'8ED', 'Uncommon').
card_artist('living terrain'/'8ED', 'Andrew Goldhawk').
card_number('living terrain'/'8ED', '260').
card_flavor_text('living terrain'/'8ED', 'Some warriors walk the earth. Others are born of it.').
card_multiverse_id('living terrain'/'8ED', '45430').

card_in_set('llanowar behemoth', '8ED').
card_original_type('llanowar behemoth'/'8ED', 'Creature — Behemoth').
card_original_text('llanowar behemoth'/'8ED', 'Tap an untapped creature you control: Llanowar Behemoth gets +1/+1 until end of turn.').
card_image_name('llanowar behemoth'/'8ED', 'llanowar behemoth').
card_uid('llanowar behemoth'/'8ED', '8ED:Llanowar Behemoth:llanowar behemoth').
card_rarity('llanowar behemoth'/'8ED', 'Uncommon').
card_artist('llanowar behemoth'/'8ED', 'Hannibal King').
card_number('llanowar behemoth'/'8ED', '261').
card_flavor_text('llanowar behemoth'/'8ED', '\"Most people can\'t build decent weapons out of stone or steel. Trust the elves to do it with only mud and vines.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('llanowar behemoth'/'8ED', '45429').

card_in_set('lone wolf', '8ED').
card_original_type('lone wolf'/'8ED', 'Creature — Wolf').
card_original_text('lone wolf'/'8ED', 'You may have Lone Wolf deal its combat damage to defending player as though it weren\'t blocked.').
card_image_name('lone wolf'/'8ED', 'lone wolf').
card_uid('lone wolf'/'8ED', '8ED:Lone Wolf:lone wolf').
card_rarity('lone wolf'/'8ED', 'Common').
card_artist('lone wolf'/'8ED', 'Una Fricker').
card_number('lone wolf'/'8ED', '262').
card_flavor_text('lone wolf'/'8ED', 'A wolf without a pack is either a survivor or a brute.').
card_multiverse_id('lone wolf'/'8ED', '45405').

card_in_set('looming shade', '8ED').
card_original_type('looming shade'/'8ED', 'Creature — Shade').
card_original_text('looming shade'/'8ED', '{B}: Looming Shade gets +1/+1 until end of turn.').
card_image_name('looming shade'/'8ED', 'looming shade').
card_uid('looming shade'/'8ED', '8ED:Looming Shade:looming shade').
card_rarity('looming shade'/'8ED', 'Common').
card_artist('looming shade'/'8ED', 'Kev Walker').
card_number('looming shade'/'8ED', '140').
card_flavor_text('looming shade'/'8ED', 'On darkness feeding, in nightmares breeding.').
card_multiverse_id('looming shade'/'8ED', '45289').

card_in_set('lord of the undead', '8ED').
card_original_type('lord of the undead'/'8ED', 'Creature — Lord').
card_original_text('lord of the undead'/'8ED', 'All Zombies get +1/+1.\n{1}{B}, {T}: Return target Zombie card from your graveyard to your hand.').
card_image_name('lord of the undead'/'8ED', 'lord of the undead').
card_uid('lord of the undead'/'8ED', '8ED:Lord of the Undead:lord of the undead').
card_rarity('lord of the undead'/'8ED', 'Rare').
card_artist('lord of the undead'/'8ED', 'Brom').
card_number('lord of the undead'/'8ED', '141').
card_flavor_text('lord of the undead'/'8ED', '\"I confer with Death itself. What could you possibly do to injure me?\"').
card_multiverse_id('lord of the undead'/'8ED', '45324').

card_in_set('lure', '8ED').
card_original_type('lure'/'8ED', 'Enchant Creature').
card_original_text('lure'/'8ED', 'All creatures able to block enchanted creature do so.').
card_image_name('lure'/'8ED', 'lure').
card_uid('lure'/'8ED', '8ED:Lure:lure').
card_rarity('lure'/'8ED', 'Uncommon').
card_artist('lure'/'8ED', 'DiTerlizzi').
card_number('lure'/'8ED', '263').
card_flavor_text('lure'/'8ED', '\"When the forest wants something, no mortal mind can resist her.\"\n—Maro').
card_multiverse_id('lure'/'8ED', '45437').

card_in_set('maggot carrier', '8ED').
card_original_type('maggot carrier'/'8ED', 'Creature — Zombie').
card_original_text('maggot carrier'/'8ED', 'When Maggot Carrier comes into play, each player loses 1 life.').
card_image_name('maggot carrier'/'8ED', 'maggot carrier').
card_uid('maggot carrier'/'8ED', '8ED:Maggot Carrier:maggot carrier').
card_rarity('maggot carrier'/'8ED', 'Common').
card_artist('maggot carrier'/'8ED', 'Ron Spencer').
card_number('maggot carrier'/'8ED', '142').
card_flavor_text('maggot carrier'/'8ED', '\"We do not suddenly fall on death, but advance towards it by slight degrees; we die every day.\"\n—Seneca, Epistles, trans. Gummere').
card_multiverse_id('maggot carrier'/'8ED', '45283').

card_in_set('mahamoti djinn', '8ED').
card_original_type('mahamoti djinn'/'8ED', 'Creature — Djinn').
card_original_text('mahamoti djinn'/'8ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)').
card_image_name('mahamoti djinn'/'8ED', 'mahamoti djinn').
card_uid('mahamoti djinn'/'8ED', '8ED:Mahamoti Djinn:mahamoti djinn').
card_rarity('mahamoti djinn'/'8ED', 'Rare').
card_artist('mahamoti djinn'/'8ED', 'Eric Peterson').
card_number('mahamoti djinn'/'8ED', '88').
card_flavor_text('mahamoti djinn'/'8ED', 'Of royal blood among the spirits of the air, the Mahamoti djinn rides on the wings of the winds. As dangerous in the gambling hall as he is in battle, he is a master of trickery and misdirection.').
card_multiverse_id('mahamoti djinn'/'8ED', '45270').

card_in_set('mana clash', '8ED').
card_original_type('mana clash'/'8ED', 'Sorcery').
card_original_text('mana clash'/'8ED', 'You and target opponent each flip a coin. Mana Clash deals 1 damage to each player whose coin comes up tails. Repeat this process until both players\' coins come up heads on the same flip.').
card_image_name('mana clash'/'8ED', 'mana clash').
card_uid('mana clash'/'8ED', '8ED:Mana Clash:mana clash').
card_rarity('mana clash'/'8ED', 'Rare').
card_artist('mana clash'/'8ED', 'Ron Spencer').
card_number('mana clash'/'8ED', '202').
card_multiverse_id('mana clash'/'8ED', '45391').

card_in_set('mana leak', '8ED').
card_original_type('mana leak'/'8ED', 'Instant').
card_original_text('mana leak'/'8ED', 'Counter target spell unless its controller pays {3}.').
card_image_name('mana leak'/'8ED', 'mana leak').
card_uid('mana leak'/'8ED', '8ED:Mana Leak:mana leak').
card_rarity('mana leak'/'8ED', 'Common').
card_artist('mana leak'/'8ED', 'Christopher Rush').
card_number('mana leak'/'8ED', '89').
card_flavor_text('mana leak'/'8ED', 'The fatal flaw in every plan is the assumption that you know more than your enemy.').
card_multiverse_id('mana leak'/'8ED', '45242').

card_in_set('maro', '8ED').
card_original_type('maro'/'8ED', 'Creature — Elemental').
card_original_text('maro'/'8ED', 'Maro\'s power and toughness are each equal to the number of cards in your hand.').
card_image_name('maro'/'8ED', 'maro').
card_uid('maro'/'8ED', '8ED:Maro:maro').
card_rarity('maro'/'8ED', 'Rare').
card_artist('maro'/'8ED', 'Stuart Griffin').
card_number('maro'/'8ED', '264').
card_flavor_text('maro'/'8ED', 'No two see the same Maro.').
card_multiverse_id('maro'/'8ED', '45445').

card_in_set('master decoy', '8ED').
card_original_type('master decoy'/'8ED', 'Creature — Soldier').
card_original_text('master decoy'/'8ED', '{W}, {T}: Tap target creature.').
card_image_name('master decoy'/'8ED', 'master decoy').
card_uid('master decoy'/'8ED', '8ED:Master Decoy:master decoy').
card_rarity('master decoy'/'8ED', 'Common').
card_artist('master decoy'/'8ED', 'Ben Thompson').
card_number('master decoy'/'8ED', '29').
card_flavor_text('master decoy'/'8ED', 'A skilled decoy can throw your enemies off your trail. A master decoy can survive to do it again.').
card_multiverse_id('master decoy'/'8ED', '45169').

card_in_set('master healer', '8ED').
card_original_type('master healer'/'8ED', 'Creature — Cleric').
card_original_text('master healer'/'8ED', '{T}: Prevent the next 4 damage that would be dealt to target creature or player this turn.').
card_image_name('master healer'/'8ED', 'master healer').
card_uid('master healer'/'8ED', '8ED:Master Healer:master healer').
card_rarity('master healer'/'8ED', 'Rare').
card_artist('master healer'/'8ED', 'Greg & Tim Hildebrandt').
card_number('master healer'/'8ED', '30').
card_flavor_text('master healer'/'8ED', 'Her heart holds the pain of every soldier she\'s ever healed.').
card_multiverse_id('master healer'/'8ED', '45213').

card_in_set('megrim', '8ED').
card_original_type('megrim'/'8ED', 'Enchantment').
card_original_text('megrim'/'8ED', 'Whenever an opponent discards a card from his or her hand, Megrim deals 2 damage to that player.').
card_image_name('megrim'/'8ED', 'megrim').
card_uid('megrim'/'8ED', '8ED:Megrim:megrim').
card_rarity('megrim'/'8ED', 'Uncommon').
card_artist('megrim'/'8ED', 'Peter Bollinger').
card_number('megrim'/'8ED', '143').
card_flavor_text('megrim'/'8ED', 'Memories cut from the mind can cut right back.').
card_multiverse_id('megrim'/'8ED', '45316').

card_in_set('merchant of secrets', '8ED').
card_original_type('merchant of secrets'/'8ED', 'Creature — Wizard').
card_original_text('merchant of secrets'/'8ED', 'When Merchant of Secrets comes into play, draw a card.').
card_image_name('merchant of secrets'/'8ED', 'merchant of secrets').
card_uid('merchant of secrets'/'8ED', '8ED:Merchant of Secrets:merchant of secrets').
card_rarity('merchant of secrets'/'8ED', 'Common').
card_artist('merchant of secrets'/'8ED', 'Greg Hildebrandt').
card_number('merchant of secrets'/'8ED', '90').
card_flavor_text('merchant of secrets'/'8ED', 'To scrape out a living in Aphetto, wizards are reduced to selling rumors, lies, forgeries, or—if they get desperate enough—the truth.').
card_multiverse_id('merchant of secrets'/'8ED', '45231').

card_in_set('merchant scroll', '8ED').
card_original_type('merchant scroll'/'8ED', 'Sorcery').
card_original_text('merchant scroll'/'8ED', 'Search your library for a blue instant card, reveal that card, and put it into your hand. Then shuffle your library.').
card_image_name('merchant scroll'/'8ED', 'merchant scroll').
card_uid('merchant scroll'/'8ED', '8ED:Merchant Scroll:merchant scroll').
card_rarity('merchant scroll'/'8ED', 'Uncommon').
card_artist('merchant scroll'/'8ED', 'David Martin').
card_number('merchant scroll'/'8ED', '91').
card_flavor_text('merchant scroll'/'8ED', 'Lat-Nam wizards trade knowledge on scrolls made of magic so they can better guard against thieves and spies.').
card_multiverse_id('merchant scroll'/'8ED', '45275').

card_in_set('might of oaks', '8ED').
card_original_type('might of oaks'/'8ED', 'Instant').
card_original_text('might of oaks'/'8ED', 'Target creature gets +7/+7 until end of turn.').
card_image_name('might of oaks'/'8ED', 'might of oaks').
card_uid('might of oaks'/'8ED', '8ED:Might of Oaks:might of oaks').
card_rarity('might of oaks'/'8ED', 'Rare').
card_artist('might of oaks'/'8ED', 'Greg Staples').
card_number('might of oaks'/'8ED', '265').
card_flavor_text('might of oaks'/'8ED', '\"Guess where I\'m gonna plant this!\"').
card_multiverse_id('might of oaks'/'8ED', '45452').

card_in_set('millstone', '8ED').
card_original_type('millstone'/'8ED', 'Artifact').
card_original_text('millstone'/'8ED', '{2}, {T}: Target player puts the top two cards of his or her library into his or her graveyard.').
card_image_name('millstone'/'8ED', 'millstone').
card_uid('millstone'/'8ED', '8ED:Millstone:millstone').
card_rarity('millstone'/'8ED', 'Rare').
card_artist('millstone'/'8ED', 'John Avon').
card_number('millstone'/'8ED', '307').
card_flavor_text('millstone'/'8ED', 'More than one mage has been driven insane by the sound of the millstone relentlessly grinding away.').
card_multiverse_id('millstone'/'8ED', '45475').

card_in_set('mind bend', '8ED').
card_original_type('mind bend'/'8ED', 'Instant').
card_original_text('mind bend'/'8ED', 'Change the text of target permanent by replacing all instances of one color word with another or one basic land type with another. (For example, you may change \"nonblack creature\" to \"nongreen creature\" or \"forestwalk\" to \"plainswalk.\" This effect doesn\'t end at end of turn.)').
card_image_name('mind bend'/'8ED', 'mind bend').
card_uid('mind bend'/'8ED', '8ED:Mind Bend:mind bend').
card_rarity('mind bend'/'8ED', 'Rare').
card_artist('mind bend'/'8ED', 'Tony Szczudlo').
card_number('mind bend'/'8ED', '92').
card_multiverse_id('mind bend'/'8ED', '45274').

card_in_set('mind rot', '8ED').
card_original_type('mind rot'/'8ED', 'Sorcery').
card_original_text('mind rot'/'8ED', 'Target player discards two cards from his or her hand.').
card_image_name('mind rot'/'8ED', 'mind rot').
card_uid('mind rot'/'8ED', '8ED:Mind Rot:mind rot').
card_rarity('mind rot'/'8ED', 'Common').
card_artist('mind rot'/'8ED', 'Steve Luke').
card_number('mind rot'/'8ED', '144').
card_flavor_text('mind rot'/'8ED', 'What wizards fear most is losing their minds.').
card_multiverse_id('mind rot'/'8ED', '45302').

card_in_set('mind slash', '8ED').
card_original_type('mind slash'/'8ED', 'Enchantment').
card_original_text('mind slash'/'8ED', '{B}, Sacrifice a creature: Target opponent reveals his or her hand. Choose a card from it. That player discards that card. Play this ability only any time you could play a sorcery.').
card_image_name('mind slash'/'8ED', 'mind slash').
card_uid('mind slash'/'8ED', '8ED:Mind Slash:mind slash').
card_rarity('mind slash'/'8ED', 'Uncommon').
card_artist('mind slash'/'8ED', 'Adam Rex').
card_number('mind slash'/'8ED', '145').
card_flavor_text('mind slash'/'8ED', '\"I can\'t think with all that screaming.\"').
card_multiverse_id('mind slash'/'8ED', '45304').

card_in_set('mind sludge', '8ED').
card_original_type('mind sludge'/'8ED', 'Sorcery').
card_original_text('mind sludge'/'8ED', 'Target player discards a card from his or her hand for each Swamp you control.').
card_image_name('mind sludge'/'8ED', 'mind sludge').
card_uid('mind sludge'/'8ED', '8ED:Mind Sludge:mind sludge').
card_rarity('mind sludge'/'8ED', 'Uncommon').
card_artist('mind sludge'/'8ED', 'Eric Peterson').
card_number('mind sludge'/'8ED', '146').
card_flavor_text('mind sludge'/'8ED', 'When you get into the swamp, the swamp gets into you.').
card_multiverse_id('mind sludge'/'8ED', '45320').

card_in_set('mogg sentry', '8ED').
card_original_type('mogg sentry'/'8ED', 'Creature — Goblin').
card_original_text('mogg sentry'/'8ED', 'Whenever an opponent plays a spell, Mogg Sentry gets +2/+2 until end of turn.').
card_image_name('mogg sentry'/'8ED', 'mogg sentry').
card_uid('mogg sentry'/'8ED', '8ED:Mogg Sentry:mogg sentry').
card_rarity('mogg sentry'/'8ED', 'Rare').
card_artist('mogg sentry'/'8ED', 'Greg Staples').
card_number('mogg sentry'/'8ED', '203').
card_flavor_text('mogg sentry'/'8ED', 'Tortured with magic by their masters, the moggs are enflamed by the smell of sorcery.').
card_multiverse_id('mogg sentry'/'8ED', '45381').

card_in_set('monstrous growth', '8ED').
card_original_type('monstrous growth'/'8ED', 'Sorcery').
card_original_text('monstrous growth'/'8ED', 'Target creature gets +4/+4 until end of turn.').
card_image_name('monstrous growth'/'8ED', 'monstrous growth').
card_uid('monstrous growth'/'8ED', '8ED:Monstrous Growth:monstrous growth').
card_rarity('monstrous growth'/'8ED', 'Common').
card_artist('monstrous growth'/'8ED', 'Ron Spencer').
card_number('monstrous growth'/'8ED', '266').
card_flavor_text('monstrous growth'/'8ED', 'Get in touch with your inner monster.').
card_multiverse_id('monstrous growth'/'8ED', '45412').

card_in_set('moss monster', '8ED').
card_original_type('moss monster'/'8ED', 'Creature — Monster').
card_original_text('moss monster'/'8ED', '').
card_image_name('moss monster'/'8ED', 'moss monster').
card_uid('moss monster'/'8ED', '8ED:Moss Monster:moss monster').
card_rarity('moss monster'/'8ED', 'Common').
card_artist('moss monster'/'8ED', 'Glen Angus').
card_number('moss monster'/'8ED', '267').
card_flavor_text('moss monster'/'8ED', 'After the battle, an eerie silence gripped the forest. The losers\' remains were lightly dusted with green.').
card_multiverse_id('moss monster'/'8ED', '45411').

card_in_set('mountain', '8ED').
card_original_type('mountain'/'8ED', 'Basic Land — Mountain').
card_original_text('mountain'/'8ED', 'R').
card_image_name('mountain'/'8ED', 'mountain1').
card_uid('mountain'/'8ED', '8ED:Mountain:mountain1').
card_rarity('mountain'/'8ED', 'Basic Land').
card_artist('mountain'/'8ED', 'John Avon').
card_number('mountain'/'8ED', '343').
card_multiverse_id('mountain'/'8ED', '46449').

card_in_set('mountain', '8ED').
card_original_type('mountain'/'8ED', 'Basic Land — Mountain').
card_original_text('mountain'/'8ED', 'R').
card_image_name('mountain'/'8ED', 'mountain2').
card_uid('mountain'/'8ED', '8ED:Mountain:mountain2').
card_rarity('mountain'/'8ED', 'Basic Land').
card_artist('mountain'/'8ED', 'John Avon').
card_number('mountain'/'8ED', '344').
card_multiverse_id('mountain'/'8ED', '46450').

card_in_set('mountain', '8ED').
card_original_type('mountain'/'8ED', 'Basic Land — Mountain').
card_original_text('mountain'/'8ED', 'R').
card_image_name('mountain'/'8ED', 'mountain3').
card_uid('mountain'/'8ED', '8ED:Mountain:mountain3').
card_rarity('mountain'/'8ED', 'Basic Land').
card_artist('mountain'/'8ED', 'John Avon').
card_number('mountain'/'8ED', '345').
card_multiverse_id('mountain'/'8ED', '46451').

card_in_set('mountain', '8ED').
card_original_type('mountain'/'8ED', 'Basic Land — Mountain').
card_original_text('mountain'/'8ED', 'R').
card_image_name('mountain'/'8ED', 'mountain4').
card_uid('mountain'/'8ED', '8ED:Mountain:mountain4').
card_rarity('mountain'/'8ED', 'Basic Land').
card_artist('mountain'/'8ED', 'Rob Alexander').
card_number('mountain'/'8ED', '346').
card_multiverse_id('mountain'/'8ED', '46452').

card_in_set('murderous betrayal', '8ED').
card_original_type('murderous betrayal'/'8ED', 'Enchantment').
card_original_text('murderous betrayal'/'8ED', '{B}{B}, Pay half your life rounded up: Destroy target nonblack creature. It can\'t be regenerated.').
card_image_name('murderous betrayal'/'8ED', 'murderous betrayal').
card_uid('murderous betrayal'/'8ED', '8ED:Murderous Betrayal:murderous betrayal').
card_rarity('murderous betrayal'/'8ED', 'Rare').
card_artist('murderous betrayal'/'8ED', 'Randy Gallegos').
card_number('murderous betrayal'/'8ED', '147').
card_flavor_text('murderous betrayal'/'8ED', 'Which hurts more: a sword in the gut or a dagger in the back?').
card_multiverse_id('murderous betrayal'/'8ED', '45335').

card_in_set('nantuko disciple', '8ED').
card_original_type('nantuko disciple'/'8ED', 'Creature — Insect Druid').
card_original_text('nantuko disciple'/'8ED', '{G}, {T}: Target creature gets +2/+2 until end of turn.').
card_image_name('nantuko disciple'/'8ED', 'nantuko disciple').
card_uid('nantuko disciple'/'8ED', '8ED:Nantuko Disciple:nantuko disciple').
card_rarity('nantuko disciple'/'8ED', 'Common').
card_artist('nantuko disciple'/'8ED', 'Justin Sweet').
card_number('nantuko disciple'/'8ED', '268').
card_flavor_text('nantuko disciple'/'8ED', 'Unanswered prayers are themselves answers.\n—Nantuko teaching').
card_multiverse_id('nantuko disciple'/'8ED', '45407').

card_in_set('natural affinity', '8ED').
card_original_type('natural affinity'/'8ED', 'Instant').
card_original_text('natural affinity'/'8ED', 'Until end of turn, all lands become 2/2 creatures that are still lands.').
card_image_name('natural affinity'/'8ED', 'natural affinity').
card_uid('natural affinity'/'8ED', '8ED:Natural Affinity:natural affinity').
card_rarity('natural affinity'/'8ED', 'Rare').
card_artist('natural affinity'/'8ED', 'Pete Venters').
card_number('natural affinity'/'8ED', '269').
card_flavor_text('natural affinity'/'8ED', 'As long as the dryads are a part of the forest, the forest is a part of the army.').
card_multiverse_id('natural affinity'/'8ED', '45449').

card_in_set('naturalize', '8ED').
card_original_type('naturalize'/'8ED', 'Instant').
card_original_text('naturalize'/'8ED', 'Destroy target artifact or enchantment.').
card_image_name('naturalize'/'8ED', 'naturalize').
card_uid('naturalize'/'8ED', '8ED:Naturalize:naturalize').
card_rarity('naturalize'/'8ED', 'Common').
card_artist('naturalize'/'8ED', 'Tim Hildebrandt').
card_number('naturalize'/'8ED', '270').
card_flavor_text('naturalize'/'8ED', '\"One touch of nature makes the whole world kin.\"\n—William Shakespeare,\nTroilus and Cressida').
card_multiverse_id('naturalize'/'8ED', '45420').

card_in_set('nausea', '8ED').
card_original_type('nausea'/'8ED', 'Sorcery').
card_original_text('nausea'/'8ED', 'All creatures get -1/-1 until end of turn.').
card_image_name('nausea'/'8ED', 'nausea').
card_uid('nausea'/'8ED', '8ED:Nausea:nausea').
card_rarity('nausea'/'8ED', 'Common').
card_artist('nausea'/'8ED', 'James Bernardin').
card_number('nausea'/'8ED', '148').
card_flavor_text('nausea'/'8ED', 'No fear. No sense. And soon, no lunch.').
card_multiverse_id('nausea'/'8ED', '45298').

card_in_set('nekrataal', '8ED').
card_original_type('nekrataal'/'8ED', 'Creature — Nekrataal').
card_original_text('nekrataal'/'8ED', 'First strike (This creature deals combat damage before creatures without first strike.)\nWhen Nekrataal comes into play, destroy target nonartifact, nonblack creature. That creature can\'t be regenerated.').
card_image_name('nekrataal'/'8ED', 'nekrataal').
card_uid('nekrataal'/'8ED', '8ED:Nekrataal:nekrataal').
card_rarity('nekrataal'/'8ED', 'Uncommon').
card_artist('nekrataal'/'8ED', 'Adrian Smith').
card_number('nekrataal'/'8ED', '149').
card_multiverse_id('nekrataal'/'8ED', '45310').

card_in_set('nightmare', '8ED').
card_original_type('nightmare'/'8ED', 'Creature — Nightmare').
card_original_text('nightmare'/'8ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\nNightmare\'s power and toughness are each equal to the number of Swamps you control.').
card_image_name('nightmare'/'8ED', 'nightmare').
card_uid('nightmare'/'8ED', '8ED:Nightmare:nightmare').
card_rarity('nightmare'/'8ED', 'Rare').
card_artist('nightmare'/'8ED', 'Carl Critchlow').
card_number('nightmare'/'8ED', '150').
card_flavor_text('nightmare'/'8ED', 'The thunder of its hooves beats dreams into despair.').
card_multiverse_id('nightmare'/'8ED', '45331').

card_in_set('noble purpose', '8ED').
card_original_type('noble purpose'/'8ED', 'Enchantment').
card_original_text('noble purpose'/'8ED', 'Whenever a creature you control deals combat damage, you gain that much life.').
card_image_name('noble purpose'/'8ED', 'noble purpose').
card_uid('noble purpose'/'8ED', '8ED:Noble Purpose:noble purpose').
card_rarity('noble purpose'/'8ED', 'Rare').
card_artist('noble purpose'/'8ED', 'Kev Walker').
card_number('noble purpose'/'8ED', '31').
card_flavor_text('noble purpose'/'8ED', 'Fighting for the right reason is itself a source of strength.').
card_multiverse_id('noble purpose'/'8ED', '45224').

card_in_set('norwood ranger', '8ED').
card_original_type('norwood ranger'/'8ED', 'Creature — Elf').
card_original_text('norwood ranger'/'8ED', '').
card_image_name('norwood ranger'/'8ED', 'norwood ranger').
card_uid('norwood ranger'/'8ED', '8ED:Norwood Ranger:norwood ranger').
card_rarity('norwood ranger'/'8ED', 'Common').
card_artist('norwood ranger'/'8ED', 'Ron Spencer').
card_number('norwood ranger'/'8ED', '271').
card_flavor_text('norwood ranger'/'8ED', 'The song of the forest is in perfect harmony. If a single note is out of place, the elves will find its source.').
card_multiverse_id('norwood ranger'/'8ED', '45409').

card_in_set('obliterate', '8ED').
card_original_type('obliterate'/'8ED', 'Sorcery').
card_original_text('obliterate'/'8ED', 'Obliterate can\'t be countered.\nDestroy all artifacts, creatures, and lands. They can\'t be regenerated.').
card_image_name('obliterate'/'8ED', 'obliterate').
card_uid('obliterate'/'8ED', '8ED:Obliterate:obliterate').
card_rarity('obliterate'/'8ED', 'Rare').
card_artist('obliterate'/'8ED', 'Kev Walker').
card_number('obliterate'/'8ED', '204').
card_flavor_text('obliterate'/'8ED', '\"The enemy has been destroyed, sir. So have the forest, the city, your palace, your dog . . .\"\n—Keldon soldier').
card_multiverse_id('obliterate'/'8ED', '45396').

card_in_set('ogre taskmaster', '8ED').
card_original_type('ogre taskmaster'/'8ED', 'Creature — Ogre').
card_original_text('ogre taskmaster'/'8ED', 'Ogre Taskmaster can\'t block.').
card_image_name('ogre taskmaster'/'8ED', 'ogre taskmaster').
card_uid('ogre taskmaster'/'8ED', '8ED:Ogre Taskmaster:ogre taskmaster').
card_rarity('ogre taskmaster'/'8ED', 'Uncommon').
card_artist('ogre taskmaster'/'8ED', 'Dany Orizio').
card_number('ogre taskmaster'/'8ED', '205').
card_flavor_text('ogre taskmaster'/'8ED', 'Ogres learn how to smash stuff even before they learn how to walk.').
card_multiverse_id('ogre taskmaster'/'8ED', '45369').

card_in_set('okk', '8ED').
card_original_type('okk'/'8ED', 'Creature — Goblin').
card_original_text('okk'/'8ED', 'Okk can\'t attack unless a creature with greater power also attacks.\nOkk can\'t block unless a creature with greater power also blocks.').
card_image_name('okk'/'8ED', 'okk').
card_uid('okk'/'8ED', '8ED:Okk:okk').
card_rarity('okk'/'8ED', 'Rare').
card_artist('okk'/'8ED', 'Peter Bollinger').
card_number('okk'/'8ED', '206').
card_multiverse_id('okk'/'8ED', '45382').

card_in_set('oracle\'s attendants', '8ED').
card_original_type('oracle\'s attendants'/'8ED', 'Creature — Soldier').
card_original_text('oracle\'s attendants'/'8ED', '{T}: All damage that would be dealt to target creature this turn by a source of your choice is dealt to Oracle\'s Attendants instead.').
card_image_name('oracle\'s attendants'/'8ED', 'oracle\'s attendants').
card_uid('oracle\'s attendants'/'8ED', '8ED:Oracle\'s Attendants:oracle\'s attendants').
card_rarity('oracle\'s attendants'/'8ED', 'Rare').
card_artist('oracle\'s attendants'/'8ED', 'Dany Orizio').
card_number('oracle\'s attendants'/'8ED', '32').
card_flavor_text('oracle\'s attendants'/'8ED', 'The future isn\'t sacred, but its speaker is.').
card_multiverse_id('oracle\'s attendants'/'8ED', '45210').

card_in_set('orcish artillery', '8ED').
card_original_type('orcish artillery'/'8ED', 'Creature — Orc').
card_original_text('orcish artillery'/'8ED', '{T}: Orcish Artillery deals 2 damage to target creature or player and 3 damage to you.').
card_image_name('orcish artillery'/'8ED', 'orcish artillery').
card_uid('orcish artillery'/'8ED', '8ED:Orcish Artillery:orcish artillery').
card_rarity('orcish artillery'/'8ED', 'Uncommon').
card_artist('orcish artillery'/'8ED', 'Dan Frazier').
card_number('orcish artillery'/'8ED', '207').
card_flavor_text('orcish artillery'/'8ED', '\"So they want to kill my men? Well two can play at that game.\"\n—General Khurzog').
card_multiverse_id('orcish artillery'/'8ED', '45366').

card_in_set('orcish spy', '8ED').
card_original_type('orcish spy'/'8ED', 'Creature — Orc').
card_original_text('orcish spy'/'8ED', '{T}: Look at the top three cards of target player\'s library. (Put them back in the same order.)').
card_image_name('orcish spy'/'8ED', 'orcish spy').
card_uid('orcish spy'/'8ED', '8ED:Orcish Spy:orcish spy').
card_rarity('orcish spy'/'8ED', 'Common').
card_artist('orcish spy'/'8ED', 'Greg Staples').
card_number('orcish spy'/'8ED', '208').
card_flavor_text('orcish spy'/'8ED', '\"You idiot Never let the spies mingle with the orcish regulars after completing a mission. Now we\'ll never get them to fight\"\n—General Khurzog').
card_multiverse_id('orcish spy'/'8ED', '45351').

card_in_set('pacifism', '8ED').
card_original_type('pacifism'/'8ED', 'Enchant Creature').
card_original_text('pacifism'/'8ED', 'Enchanted creature can\'t attack or block.').
card_image_name('pacifism'/'8ED', 'pacifism').
card_uid('pacifism'/'8ED', '8ED:Pacifism:pacifism').
card_rarity('pacifism'/'8ED', 'Common').
card_artist('pacifism'/'8ED', 'Robert Bliss').
card_number('pacifism'/'8ED', '33').
card_flavor_text('pacifism'/'8ED', 'For the first time in his life, Grakk felt a little warm and fuzzy inside.').
card_multiverse_id('pacifism'/'8ED', '45186').

card_in_set('panic attack', '8ED').
card_original_type('panic attack'/'8ED', 'Sorcery').
card_original_text('panic attack'/'8ED', 'Up to three target creatures can\'t block this turn.').
card_image_name('panic attack'/'8ED', 'panic attack').
card_uid('panic attack'/'8ED', '8ED:Panic Attack:panic attack').
card_rarity('panic attack'/'8ED', 'Common').
card_artist('panic attack'/'8ED', 'Mike Ploog').
card_number('panic attack'/'8ED', '209').
card_flavor_text('panic attack'/'8ED', 'Sometimes everyone decides to go for help at the same time.').
card_multiverse_id('panic attack'/'8ED', '45359').

card_in_set('patagia golem', '8ED').
card_original_type('patagia golem'/'8ED', 'Artifact Creature — Golem').
card_original_text('patagia golem'/'8ED', '{3}: Patagia Golem gains flying until end of turn. (It can\'t be blocked except by creatures with flying.)').
card_image_name('patagia golem'/'8ED', 'patagia golem').
card_uid('patagia golem'/'8ED', '8ED:Patagia Golem:patagia golem').
card_rarity('patagia golem'/'8ED', 'Uncommon').
card_artist('patagia golem'/'8ED', 'Kev Walker').
card_number('patagia golem'/'8ED', '308').
card_flavor_text('patagia golem'/'8ED', 'Its wings were only designed to be ornamental, but it learned to use them on its own.').
card_multiverse_id('patagia golem'/'8ED', '45465').

card_in_set('peach garden oath', '8ED').
card_original_type('peach garden oath'/'8ED', 'Sorcery').
card_original_text('peach garden oath'/'8ED', 'You gain 2 life for each creature you control.').
card_image_name('peach garden oath'/'8ED', 'peach garden oath').
card_uid('peach garden oath'/'8ED', '8ED:Peach Garden Oath:peach garden oath').
card_rarity('peach garden oath'/'8ED', 'Uncommon').
card_artist('peach garden oath'/'8ED', 'Qiao Dafu').
card_number('peach garden oath'/'8ED', '34').
card_flavor_text('peach garden oath'/'8ED', '\"We three, though of separate ancestry, join in brotherhood. . . . We dare not hope to be together always but hereby vow to die the selfsame day.\"\n—Peach Garden Oath').
card_multiverse_id('peach garden oath'/'8ED', '45201').

card_in_set('persecute', '8ED').
card_original_type('persecute'/'8ED', 'Sorcery').
card_original_text('persecute'/'8ED', 'Choose a color. Target player reveals his or her hand and discards all cards of that color from it.').
card_image_name('persecute'/'8ED', 'persecute').
card_uid('persecute'/'8ED', '8ED:Persecute:persecute').
card_rarity('persecute'/'8ED', 'Rare').
card_artist('persecute'/'8ED', 'D. Alexander Gregory').
card_number('persecute'/'8ED', '151').
card_flavor_text('persecute'/'8ED', '\"Of all the tyrannies on humane kind\nThe worst is that which persecutes the mind.\"\n—John Dryden,\n\"The Hind and the Panther\"').
card_multiverse_id('persecute'/'8ED', '45337').

card_in_set('phantom warrior', '8ED').
card_original_type('phantom warrior'/'8ED', 'Creature — Illusion').
card_original_text('phantom warrior'/'8ED', 'Phantom Warrior is unblockable.').
card_image_name('phantom warrior'/'8ED', 'phantom warrior').
card_uid('phantom warrior'/'8ED', '8ED:Phantom Warrior:phantom warrior').
card_rarity('phantom warrior'/'8ED', 'Uncommon').
card_artist('phantom warrior'/'8ED', 'Greg Staples').
card_number('phantom warrior'/'8ED', '93').
card_flavor_text('phantom warrior'/'8ED', 'It can pass through solid matter—but that doesn\'t mean it\'s harmless.').
card_multiverse_id('phantom warrior'/'8ED', '45250').

card_in_set('phyrexian arena', '8ED').
card_original_type('phyrexian arena'/'8ED', 'Enchantment').
card_original_text('phyrexian arena'/'8ED', 'At the beginning of your upkeep, you draw a card and you lose 1 life. (Your upkeep step is after you untap and before you draw.)').
card_image_name('phyrexian arena'/'8ED', 'phyrexian arena').
card_uid('phyrexian arena'/'8ED', '8ED:Phyrexian Arena:phyrexian arena').
card_rarity('phyrexian arena'/'8ED', 'Rare').
card_artist('phyrexian arena'/'8ED', 'Carl Critchlow').
card_number('phyrexian arena'/'8ED', '152').
card_flavor_text('phyrexian arena'/'8ED', 'A drop of humanity for a sea of power.').
card_multiverse_id('phyrexian arena'/'8ED', '45339').

card_in_set('phyrexian colossus', '8ED').
card_original_type('phyrexian colossus'/'8ED', 'Artifact Creature').
card_original_text('phyrexian colossus'/'8ED', 'Phyrexian Colossus doesn\'t untap during your untap step.\nPay 8 life: Untap Phyrexian Colossus.\nPhyrexian Colossus can\'t be blocked except by three or more creatures.').
card_image_name('phyrexian colossus'/'8ED', 'phyrexian colossus').
card_uid('phyrexian colossus'/'8ED', '8ED:Phyrexian Colossus:phyrexian colossus').
card_rarity('phyrexian colossus'/'8ED', 'Rare').
card_artist('phyrexian colossus'/'8ED', 'Mark Tedin').
card_number('phyrexian colossus'/'8ED', '309').
card_multiverse_id('phyrexian colossus'/'8ED', '45490').

card_in_set('phyrexian hulk', '8ED').
card_original_type('phyrexian hulk'/'8ED', 'Artifact Creature').
card_original_text('phyrexian hulk'/'8ED', '').
card_image_name('phyrexian hulk'/'8ED', 'phyrexian hulk').
card_uid('phyrexian hulk'/'8ED', '8ED:Phyrexian Hulk:phyrexian hulk').
card_rarity('phyrexian hulk'/'8ED', 'Uncommon').
card_artist('phyrexian hulk'/'8ED', 'Matthew D. Wilson').
card_number('phyrexian hulk'/'8ED', '310').
card_flavor_text('phyrexian hulk'/'8ED', 'It doesn\'t think. It doesn\'t feel.\nIt doesn\'t laugh or cry.\nAll it does from dusk till dawn\nIs make the soldiers die.\n—Onean children\'s rhyme').
card_multiverse_id('phyrexian hulk'/'8ED', '45471').

card_in_set('phyrexian plaguelord', '8ED').
card_original_type('phyrexian plaguelord'/'8ED', 'Creature — Carrier').
card_original_text('phyrexian plaguelord'/'8ED', '{T}, Sacrifice Phyrexian Plaguelord: Target creature gets -4/-4 until end of turn.\nSacrifice a creature: Target creature gets -1/-1 until end of turn.').
card_image_name('phyrexian plaguelord'/'8ED', 'phyrexian plaguelord').
card_uid('phyrexian plaguelord'/'8ED', '8ED:Phyrexian Plaguelord:phyrexian plaguelord').
card_rarity('phyrexian plaguelord'/'8ED', 'Rare').
card_artist('phyrexian plaguelord'/'8ED', 'Kev Walker').
card_number('phyrexian plaguelord'/'8ED', '153').
card_flavor_text('phyrexian plaguelord'/'8ED', '\"The final stage of the illness: delirium, convulsions, and death.\"\n—Phyrexian progress notes').
card_multiverse_id('phyrexian plaguelord'/'8ED', '45327').

card_in_set('plague beetle', '8ED').
card_original_type('plague beetle'/'8ED', 'Creature — Insect').
card_original_text('plague beetle'/'8ED', 'Swampwalk (This creature is unblockable as long as defending player controls a Swamp.)').
card_image_name('plague beetle'/'8ED', 'plague beetle').
card_uid('plague beetle'/'8ED', '8ED:Plague Beetle:plague beetle').
card_rarity('plague beetle'/'8ED', 'Common').
card_artist('plague beetle'/'8ED', 'Matt Cavotta').
card_number('plague beetle'/'8ED', '154').
card_flavor_text('plague beetle'/'8ED', 'No one knows whether they were named for the disease they carry or for the speed at which they multiply.').
card_multiverse_id('plague beetle'/'8ED', '45291').

card_in_set('plague wind', '8ED').
card_original_type('plague wind'/'8ED', 'Sorcery').
card_original_text('plague wind'/'8ED', 'Destroy all creatures you don\'t control. They can\'t be regenerated.').
card_image_name('plague wind'/'8ED', 'plague wind').
card_uid('plague wind'/'8ED', '8ED:Plague Wind:plague wind').
card_rarity('plague wind'/'8ED', 'Rare').
card_artist('plague wind'/'8ED', 'Alan Pollack').
card_number('plague wind'/'8ED', '155').
card_flavor_text('plague wind'/'8ED', '\"The second wind of ascension is Reaver, slaying the unworthy.\"\n—Keld Triumphant').
card_multiverse_id('plague wind'/'8ED', '45340').

card_in_set('plains', '8ED').
card_original_type('plains'/'8ED', 'Basic Land — Plains').
card_original_text('plains'/'8ED', 'W').
card_image_name('plains'/'8ED', 'plains1').
card_uid('plains'/'8ED', '8ED:Plains:plains1').
card_rarity('plains'/'8ED', 'Basic Land').
card_artist('plains'/'8ED', 'John Avon').
card_number('plains'/'8ED', '331').
card_multiverse_id('plains'/'8ED', '46437').

card_in_set('plains', '8ED').
card_original_type('plains'/'8ED', 'Basic Land — Plains').
card_original_text('plains'/'8ED', 'W').
card_image_name('plains'/'8ED', 'plains2').
card_uid('plains'/'8ED', '8ED:Plains:plains2').
card_rarity('plains'/'8ED', 'Basic Land').
card_artist('plains'/'8ED', 'Matthew Mitchell').
card_number('plains'/'8ED', '332').
card_multiverse_id('plains'/'8ED', '46438').

card_in_set('plains', '8ED').
card_original_type('plains'/'8ED', 'Basic Land — Plains').
card_original_text('plains'/'8ED', 'W').
card_image_name('plains'/'8ED', 'plains3').
card_uid('plains'/'8ED', '8ED:Plains:plains3').
card_rarity('plains'/'8ED', 'Basic Land').
card_artist('plains'/'8ED', 'John Avon').
card_number('plains'/'8ED', '333').
card_multiverse_id('plains'/'8ED', '46439').

card_in_set('plains', '8ED').
card_original_type('plains'/'8ED', 'Basic Land — Plains').
card_original_text('plains'/'8ED', 'W').
card_image_name('plains'/'8ED', 'plains4').
card_uid('plains'/'8ED', '8ED:Plains:plains4').
card_rarity('plains'/'8ED', 'Basic Land').
card_artist('plains'/'8ED', 'Fred Fields').
card_number('plains'/'8ED', '334').
card_multiverse_id('plains'/'8ED', '46440').

card_in_set('planar portal', '8ED').
card_original_type('planar portal'/'8ED', 'Artifact').
card_original_text('planar portal'/'8ED', '{6}, {T}: Search your library for a card and put that card into your hand. Then shuffle your library.').
card_image_name('planar portal'/'8ED', 'planar portal').
card_uid('planar portal'/'8ED', '8ED:Planar Portal:planar portal').
card_rarity('planar portal'/'8ED', 'Rare').
card_artist('planar portal'/'8ED', 'Mark Tedin').
card_number('planar portal'/'8ED', '311').
card_flavor_text('planar portal'/'8ED', 'These devices have been known to pull entire worlds through the Æther.').
card_multiverse_id('planar portal'/'8ED', '45486').

card_in_set('plow under', '8ED').
card_original_type('plow under'/'8ED', 'Sorcery').
card_original_text('plow under'/'8ED', 'Put two target lands on top of their owner\'s library.').
card_image_name('plow under'/'8ED', 'plow under').
card_uid('plow under'/'8ED', '8ED:Plow Under:plow under').
card_rarity('plow under'/'8ED', 'Rare').
card_artist('plow under'/'8ED', 'Rob Alexander').
card_number('plow under'/'8ED', '272').
card_flavor_text('plow under'/'8ED', 'To renew the land, plow the land.\nTo destroy the land, do nothing.\n—Druids\' saying').
card_multiverse_id('plow under'/'8ED', '45450').

card_in_set('primeval force', '8ED').
card_original_type('primeval force'/'8ED', 'Creature — Elemental').
card_original_text('primeval force'/'8ED', 'When Primeval Force comes into play, sacrifice it unless you sacrifice three Forests.').
card_image_name('primeval force'/'8ED', 'primeval force').
card_uid('primeval force'/'8ED', '8ED:Primeval Force:primeval force').
card_rarity('primeval force'/'8ED', 'Rare').
card_artist('primeval force'/'8ED', 'Randy Gallegos').
card_number('primeval force'/'8ED', '273').
card_flavor_text('primeval force'/'8ED', 'It has germinated in the earth for thousands of years. Finally it wakes to the battle\'s call.').
card_multiverse_id('primeval force'/'8ED', '45453').

card_in_set('primeval shambler', '8ED').
card_original_type('primeval shambler'/'8ED', 'Creature — Mercenary').
card_original_text('primeval shambler'/'8ED', '{B}: Primeval Shambler gets +1/+1 until end of turn.').
card_image_name('primeval shambler'/'8ED', 'primeval shambler').
card_uid('primeval shambler'/'8ED', '8ED:Primeval Shambler:primeval shambler').
card_rarity('primeval shambler'/'8ED', 'Uncommon').
card_artist('primeval shambler'/'8ED', 'Chippy').
card_number('primeval shambler'/'8ED', '156').
card_flavor_text('primeval shambler'/'8ED', 'His mind, like his body, is made up of the swamp\'s flotsam.').
card_multiverse_id('primeval shambler'/'8ED', '45312').

card_in_set('puppeteer', '8ED').
card_original_type('puppeteer'/'8ED', 'Creature — Wizard').
card_original_text('puppeteer'/'8ED', '{U}, {T}: Tap or untap target creature.').
card_image_name('puppeteer'/'8ED', 'puppeteer').
card_uid('puppeteer'/'8ED', '8ED:Puppeteer:puppeteer').
card_rarity('puppeteer'/'8ED', 'Uncommon').
card_artist('puppeteer'/'8ED', 'Scott M. Fischer').
card_number('puppeteer'/'8ED', '94').
card_flavor_text('puppeteer'/'8ED', '\"Getting people to do what you want is merely a matter of telling them what they want to hear.\"').
card_multiverse_id('puppeteer'/'8ED', '45249').

card_in_set('pyroclasm', '8ED').
card_original_type('pyroclasm'/'8ED', 'Sorcery').
card_original_text('pyroclasm'/'8ED', 'Pyroclasm deals 2 damage to each creature.').
card_image_name('pyroclasm'/'8ED', 'pyroclasm').
card_uid('pyroclasm'/'8ED', '8ED:Pyroclasm:pyroclasm').
card_rarity('pyroclasm'/'8ED', 'Uncommon').
card_artist('pyroclasm'/'8ED', 'John Avon').
card_number('pyroclasm'/'8ED', '210').
card_flavor_text('pyroclasm'/'8ED', '\"When the air burns, only death breathes deep.\"\n—Bogardan mage').
card_multiverse_id('pyroclasm'/'8ED', '45374').

card_in_set('pyrotechnics', '8ED').
card_original_type('pyrotechnics'/'8ED', 'Sorcery').
card_original_text('pyrotechnics'/'8ED', 'Pyrotechnics deals 4 damage divided as you choose among any number of target creatures and/or players.').
card_image_name('pyrotechnics'/'8ED', 'pyrotechnics').
card_uid('pyrotechnics'/'8ED', '8ED:Pyrotechnics:pyrotechnics').
card_rarity('pyrotechnics'/'8ED', 'Uncommon').
card_artist('pyrotechnics'/'8ED', 'John Avon').
card_number('pyrotechnics'/'8ED', '211').
card_flavor_text('pyrotechnics'/'8ED', '\"Who says lightning never strikes twice?\"\n—Storm shaman').
card_multiverse_id('pyrotechnics'/'8ED', '45378').

card_in_set('raging goblin', '8ED').
card_original_type('raging goblin'/'8ED', 'Creature — Goblin').
card_original_text('raging goblin'/'8ED', 'Haste (This creature may attack the turn it comes under your control.)').
card_image_name('raging goblin'/'8ED', 'raging goblin').
card_uid('raging goblin'/'8ED', '8ED:Raging Goblin:raging goblin').
card_rarity('raging goblin'/'8ED', 'Common').
card_artist('raging goblin'/'8ED', 'Jeff Miracola').
card_number('raging goblin'/'8ED', '212').
card_flavor_text('raging goblin'/'8ED', 'He raged at the world, at his family, at his life. But mostly he just raged.').
card_multiverse_id('raging goblin'/'8ED', '45342').

card_in_set('rain of blades', '8ED').
card_original_type('rain of blades'/'8ED', 'Instant').
card_original_text('rain of blades'/'8ED', 'Rain of Blades deals 1 damage to each attacking creature.').
card_image_name('rain of blades'/'8ED', 'rain of blades').
card_uid('rain of blades'/'8ED', '8ED:Rain of Blades:rain of blades').
card_rarity('rain of blades'/'8ED', 'Uncommon').
card_artist('rain of blades'/'8ED', 'Rob Alexander').
card_number('rain of blades'/'8ED', '35').
card_flavor_text('rain of blades'/'8ED', 'Some say they are the weapons of heroes fallen in battle, eager for one last chance at glory.').
card_multiverse_id('rain of blades'/'8ED', '45198').

card_in_set('raise dead', '8ED').
card_original_type('raise dead'/'8ED', 'Sorcery').
card_original_text('raise dead'/'8ED', 'Return target creature card from your graveyard to your hand.').
card_image_name('raise dead'/'8ED', 'raise dead').
card_uid('raise dead'/'8ED', '8ED:Raise Dead:raise dead').
card_rarity('raise dead'/'8ED', 'Common').
card_artist('raise dead'/'8ED', 'Carl Critchlow').
card_number('raise dead'/'8ED', '157').
card_flavor_text('raise dead'/'8ED', 'The earth cannot hold that which magic commands.').
card_multiverse_id('raise dead'/'8ED', '45295').

card_in_set('rampant growth', '8ED').
card_original_type('rampant growth'/'8ED', 'Sorcery').
card_original_text('rampant growth'/'8ED', 'Search your library for a basic land card and put that card into play tapped. Then shuffle your library.').
card_image_name('rampant growth'/'8ED', 'rampant growth').
card_uid('rampant growth'/'8ED', '8ED:Rampant Growth:rampant growth').
card_rarity('rampant growth'/'8ED', 'Common').
card_artist('rampant growth'/'8ED', 'Tom Kyffin').
card_number('rampant growth'/'8ED', '274').
card_flavor_text('rampant growth'/'8ED', 'Nature grows solutions to her problems.').
card_multiverse_id('rampant growth'/'8ED', '45417').

card_in_set('ravenous rats', '8ED').
card_original_type('ravenous rats'/'8ED', 'Creature — Rat').
card_original_text('ravenous rats'/'8ED', 'When Ravenous Rats comes into play, target opponent discards a card from his or her hand.').
card_image_name('ravenous rats'/'8ED', 'ravenous rats').
card_uid('ravenous rats'/'8ED', '8ED:Ravenous Rats:ravenous rats').
card_rarity('ravenous rats'/'8ED', 'Common').
card_artist('ravenous rats'/'8ED', 'Tom Wänerstrand').
card_number('ravenous rats'/'8ED', '158').
card_flavor_text('ravenous rats'/'8ED', 'Nothing is sacred to the rats. Everything is simply another meal.').
card_multiverse_id('ravenous rats'/'8ED', '45286').

card_in_set('razorfoot griffin', '8ED').
card_original_type('razorfoot griffin'/'8ED', 'Creature — Griffin').
card_original_text('razorfoot griffin'/'8ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\nFirst strike (This creature deals combat damage before creatures without first strike.)').
card_image_name('razorfoot griffin'/'8ED', 'razorfoot griffin').
card_uid('razorfoot griffin'/'8ED', '8ED:Razorfoot Griffin:razorfoot griffin').
card_rarity('razorfoot griffin'/'8ED', 'Common').
card_artist('razorfoot griffin'/'8ED', 'Ben Thompson').
card_number('razorfoot griffin'/'8ED', '36').
card_multiverse_id('razorfoot griffin'/'8ED', '45178').

card_in_set('redeem', '8ED').
card_original_type('redeem'/'8ED', 'Instant').
card_original_text('redeem'/'8ED', 'Prevent all damage that would be dealt this turn to up to two target creatures.').
card_image_name('redeem'/'8ED', 'redeem').
card_uid('redeem'/'8ED', '8ED:Redeem:redeem').
card_rarity('redeem'/'8ED', 'Common').
card_artist('redeem'/'8ED', 'D. Alexander Gregory').
card_number('redeem'/'8ED', '37').
card_flavor_text('redeem'/'8ED', '\"Let me redeem my brothers both from death.\"\n—William Shakespeare, Titus Andronicus').
card_multiverse_id('redeem'/'8ED', '45184').

card_in_set('reflexes', '8ED').
card_original_type('reflexes'/'8ED', 'Enchant Creature').
card_original_text('reflexes'/'8ED', 'Enchanted creature has first strike. (It deals combat damage before creatures without first strike.)').
card_image_name('reflexes'/'8ED', 'reflexes').
card_uid('reflexes'/'8ED', '8ED:Reflexes:reflexes').
card_rarity('reflexes'/'8ED', 'Common').
card_artist('reflexes'/'8ED', 'Steve White').
card_number('reflexes'/'8ED', '213').
card_flavor_text('reflexes'/'8ED', '\"Be as fast as lightning, and you will be just as deadly.\"\n—Combat trainer').
card_multiverse_id('reflexes'/'8ED', '45354').

card_in_set('regeneration', '8ED').
card_original_type('regeneration'/'8ED', 'Enchant Creature').
card_original_text('regeneration'/'8ED', '{G}: Regenerate enchanted creature.').
card_image_name('regeneration'/'8ED', 'regeneration').
card_uid('regeneration'/'8ED', '8ED:Regeneration:regeneration').
card_rarity('regeneration'/'8ED', 'Common').
card_artist('regeneration'/'8ED', 'Adam Rex').
card_number('regeneration'/'8ED', '275').
card_flavor_text('regeneration'/'8ED', '\"Why should death always have the last word?\"\n—Llanowar druid').
card_multiverse_id('regeneration'/'8ED', '45418').

card_in_set('relentless assault', '8ED').
card_original_type('relentless assault'/'8ED', 'Sorcery').
card_original_text('relentless assault'/'8ED', 'Untap all creatures that attacked this turn. After this phase, there is an additional combat phase followed by an additional main phase.').
card_image_name('relentless assault'/'8ED', 'relentless assault').
card_uid('relentless assault'/'8ED', '8ED:Relentless Assault:relentless assault').
card_rarity('relentless assault'/'8ED', 'Rare').
card_artist('relentless assault'/'8ED', 'Greg & Tim Hildebrandt').
card_number('relentless assault'/'8ED', '214').
card_flavor_text('relentless assault'/'8ED', '\"Mercy? Mercy is for the playground, not the battleground.\"').
card_multiverse_id('relentless assault'/'8ED', '45393').

card_in_set('remove soul', '8ED').
card_original_type('remove soul'/'8ED', 'Instant').
card_original_text('remove soul'/'8ED', 'Counter target creature spell.').
card_image_name('remove soul'/'8ED', 'remove soul').
card_uid('remove soul'/'8ED', '8ED:Remove Soul:remove soul').
card_rarity('remove soul'/'8ED', 'Common').
card_artist('remove soul'/'8ED', 'Adam Rex').
card_number('remove soul'/'8ED', '95').
card_multiverse_id('remove soul'/'8ED', '45241').

card_in_set('revive', '8ED').
card_original_type('revive'/'8ED', 'Sorcery').
card_original_text('revive'/'8ED', 'Return target green card from your graveyard to your hand.').
card_image_name('revive'/'8ED', 'revive').
card_uid('revive'/'8ED', '8ED:Revive:revive').
card_rarity('revive'/'8ED', 'Uncommon').
card_artist('revive'/'8ED', 'Matthew D. Wilson').
card_number('revive'/'8ED', '276').
card_flavor_text('revive'/'8ED', 'Even what is lost beyond recall returns when coaxed by a dryad\'s hand.').
card_multiverse_id('revive'/'8ED', '45433').

card_in_set('rewind', '8ED').
card_original_type('rewind'/'8ED', 'Instant').
card_original_text('rewind'/'8ED', 'Counter target spell, then untap up to four lands.').
card_image_name('rewind'/'8ED', 'rewind').
card_uid('rewind'/'8ED', '8ED:Rewind:rewind').
card_rarity('rewind'/'8ED', 'Uncommon').
card_artist('rewind'/'8ED', 'Dermot Power').
card_number('rewind'/'8ED', '96').
card_flavor_text('rewind'/'8ED', '\"Let\'s go over this one more time. . . .\"').
card_multiverse_id('rewind'/'8ED', '45264').

card_in_set('rhox', '8ED').
card_original_type('rhox'/'8ED', 'Creature — Beast').
card_original_text('rhox'/'8ED', 'You may have Rhox deal its combat damage to defending player as though it weren\'t blocked.\n{2}{G}: Regenerate Rhox.').
card_image_name('rhox'/'8ED', 'rhox').
card_uid('rhox'/'8ED', '8ED:Rhox:rhox').
card_rarity('rhox'/'8ED', 'Rare').
card_artist('rhox'/'8ED', 'Carl Critchlow').
card_number('rhox'/'8ED', '277').
card_multiverse_id('rhox'/'8ED', '49038').

card_in_set('ridgeline rager', '8ED').
card_original_type('ridgeline rager'/'8ED', 'Creature — Beast').
card_original_text('ridgeline rager'/'8ED', '{R}: Ridgeline Rager gets +1/+0 until end of turn.').
card_image_name('ridgeline rager'/'8ED', 'ridgeline rager').
card_uid('ridgeline rager'/'8ED', '8ED:Ridgeline Rager:ridgeline rager').
card_rarity('ridgeline rager'/'8ED', 'Common').
card_artist('ridgeline rager'/'8ED', 'Chippy').
card_number('ridgeline rager'/'8ED', '215').
card_flavor_text('ridgeline rager'/'8ED', 'While armies battled across Jamuraa, beasts stalked the high places looking for victims.').
card_multiverse_id('ridgeline rager'/'8ED', '45346').

card_in_set('rod of ruin', '8ED').
card_original_type('rod of ruin'/'8ED', 'Artifact').
card_original_text('rod of ruin'/'8ED', '{3}, {T}: Rod of Ruin deals 1 damage to target creature or player.').
card_image_name('rod of ruin'/'8ED', 'rod of ruin').
card_uid('rod of ruin'/'8ED', '8ED:Rod of Ruin:rod of ruin').
card_rarity('rod of ruin'/'8ED', 'Uncommon').
card_artist('rod of ruin'/'8ED', 'David Martin').
card_number('rod of ruin'/'8ED', '312').
card_flavor_text('rod of ruin'/'8ED', 'The rod is a relic from ancient times . . . cruel, vicious, mean-spirited times.').
card_multiverse_id('rod of ruin'/'8ED', '45466').

card_in_set('rolling stones', '8ED').
card_original_type('rolling stones'/'8ED', 'Enchantment').
card_original_text('rolling stones'/'8ED', 'Walls may attack as though they weren\'t Walls.').
card_image_name('rolling stones'/'8ED', 'rolling stones').
card_uid('rolling stones'/'8ED', '8ED:Rolling Stones:rolling stones').
card_rarity('rolling stones'/'8ED', 'Rare').
card_artist('rolling stones'/'8ED', 'Don Hazeltine').
card_number('rolling stones'/'8ED', '38').
card_flavor_text('rolling stones'/'8ED', '\"Trust me—the walls have more than just ears.\"\n—Orcish spy').
card_multiverse_id('rolling stones'/'8ED', '45217').

card_in_set('royal assassin', '8ED').
card_original_type('royal assassin'/'8ED', 'Creature — Assassin').
card_original_text('royal assassin'/'8ED', '{T}: Destroy target tapped creature.').
card_image_name('royal assassin'/'8ED', 'royal assassin').
card_uid('royal assassin'/'8ED', '8ED:Royal Assassin:royal assassin').
card_rarity('royal assassin'/'8ED', 'Rare').
card_artist('royal assassin'/'8ED', 'Mark Zug').
card_number('royal assassin'/'8ED', '159').
card_flavor_text('royal assassin'/'8ED', 'Trained in the arts of stealth, royal assassins choose their victims carefully, relying on timing and precision rather than brute force.').
card_multiverse_id('royal assassin'/'8ED', '45323').

card_in_set('rukh egg', '8ED').
card_original_type('rukh egg'/'8ED', 'Creature — Egg').
card_original_text('rukh egg'/'8ED', 'When Rukh Egg is put into a graveyard from play, put a 4/4 red Rukh creature token with flying into play at end of turn.').
card_image_name('rukh egg'/'8ED', 'rukh egg').
card_uid('rukh egg'/'8ED', '8ED:Rukh Egg:rukh egg').
card_rarity('rukh egg'/'8ED', 'Rare').
card_artist('rukh egg'/'8ED', 'Mark Zug').
card_number('rukh egg'/'8ED', '216').
card_flavor_text('rukh egg'/'8ED', 'Called \"stonefeathers\" by many lowlanders, rukhs are thought to descend from a phoenix that sacrificed its flame for a body of stone.').
card_multiverse_id('rukh egg'/'8ED', '45390').

card_in_set('rushwood dryad', '8ED').
card_original_type('rushwood dryad'/'8ED', 'Creature — Dryad').
card_original_text('rushwood dryad'/'8ED', 'Forestwalk (This creature is unblockable as long as defending player controls a Forest.)').
card_image_name('rushwood dryad'/'8ED', 'rushwood dryad').
card_uid('rushwood dryad'/'8ED', '8ED:Rushwood Dryad:rushwood dryad').
card_rarity('rushwood dryad'/'8ED', 'Common').
card_artist('rushwood dryad'/'8ED', 'Todd Lockwood').
card_number('rushwood dryad'/'8ED', '278').
card_flavor_text('rushwood dryad'/'8ED', 'To a dryad, every tree in the forest is both a doorway and a window.').
card_multiverse_id('rushwood dryad'/'8ED', '45403').

card_in_set('sabretooth tiger', '8ED').
card_original_type('sabretooth tiger'/'8ED', 'Creature — Cat').
card_original_text('sabretooth tiger'/'8ED', 'First strike (This creature deals combat damage before creatures without first strike.)').
card_image_name('sabretooth tiger'/'8ED', 'sabretooth tiger').
card_uid('sabretooth tiger'/'8ED', '8ED:Sabretooth Tiger:sabretooth tiger').
card_rarity('sabretooth tiger'/'8ED', 'Common').
card_artist('sabretooth tiger'/'8ED', 'Monte Michael Moore').
card_number('sabretooth tiger'/'8ED', '217').
card_flavor_text('sabretooth tiger'/'8ED', 'To a sabretooth, all men are mice.').
card_multiverse_id('sabretooth tiger'/'8ED', '45347').

card_in_set('sacred ground', '8ED').
card_original_type('sacred ground'/'8ED', 'Enchantment').
card_original_text('sacred ground'/'8ED', 'Whenever a spell or ability an opponent controls causes a land to be put into your graveyard from play, return that land to play.').
card_image_name('sacred ground'/'8ED', 'sacred ground').
card_uid('sacred ground'/'8ED', '8ED:Sacred Ground:sacred ground').
card_rarity('sacred ground'/'8ED', 'Rare').
card_artist('sacred ground'/'8ED', 'Gary Ruddell').
card_number('sacred ground'/'8ED', '39').
card_flavor_text('sacred ground'/'8ED', 'The land will never betray those who gave their lives for it.').
card_multiverse_id('sacred ground'/'8ED', '45215').

card_in_set('sacred nectar', '8ED').
card_original_type('sacred nectar'/'8ED', 'Sorcery').
card_original_text('sacred nectar'/'8ED', 'You gain 4 life.').
card_image_name('sacred nectar'/'8ED', 'sacred nectar').
card_uid('sacred nectar'/'8ED', '8ED:Sacred Nectar:sacred nectar').
card_rarity('sacred nectar'/'8ED', 'Common').
card_artist('sacred nectar'/'8ED', 'Janine Johnston').
card_number('sacred nectar'/'8ED', '40').
card_flavor_text('sacred nectar'/'8ED', '\"Over the silver mountains,\nWhere spring the nectar fountains,\n     There will I kiss\n     The bowl of bliss;\nAnd drink my everlasting fill. . . .\"\n—Sir Walter Raleigh, \"The Pilgrimage\"').
card_multiverse_id('sacred nectar'/'8ED', '45187').

card_in_set('sage of lat-nam', '8ED').
card_original_type('sage of lat-nam'/'8ED', 'Creature — Sage').
card_original_text('sage of lat-nam'/'8ED', '{T}, Sacrifice an artifact: Draw a card.').
card_image_name('sage of lat-nam'/'8ED', 'sage of lat-nam').
card_uid('sage of lat-nam'/'8ED', '8ED:Sage of Lat-Nam:sage of lat-nam').
card_rarity('sage of lat-nam'/'8ED', 'Rare').
card_artist('sage of lat-nam'/'8ED', 'Alan Pollack').
card_number('sage of lat-nam'/'8ED', '97').
card_flavor_text('sage of lat-nam'/'8ED', 'Within each device is a spark of its creator\'s soul. Within each sage is a lamp that awaits that spark.').
card_multiverse_id('sage of lat-nam'/'8ED', '45269').

card_in_set('sage owl', '8ED').
card_original_type('sage owl'/'8ED', 'Creature — Bird').
card_original_text('sage owl'/'8ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\nWhen Sage Owl comes into play, look at the top four cards of your library, then put them back in any order.').
card_image_name('sage owl'/'8ED', 'sage owl').
card_uid('sage owl'/'8ED', '8ED:Sage Owl:sage owl').
card_rarity('sage owl'/'8ED', 'Common').
card_artist('sage owl'/'8ED', 'Mark Brill').
card_number('sage owl'/'8ED', '98').
card_multiverse_id('sage owl'/'8ED', '45226').

card_in_set('salt marsh', '8ED').
card_original_type('salt marsh'/'8ED', 'Land').
card_original_text('salt marsh'/'8ED', 'Salt Marsh comes into play tapped.\n{T}: Add {U} or {B} to your mana pool.').
card_image_name('salt marsh'/'8ED', 'salt marsh').
card_uid('salt marsh'/'8ED', '8ED:Salt Marsh:salt marsh').
card_rarity('salt marsh'/'8ED', 'Uncommon').
card_artist('salt marsh'/'8ED', 'Jerry Tiritilli').
card_number('salt marsh'/'8ED', '325').
card_flavor_text('salt marsh'/'8ED', 'Only death breeds in stagnant water.\n—Urborg saying').
card_multiverse_id('salt marsh'/'8ED', '45492').

card_in_set('samite healer', '8ED').
card_original_type('samite healer'/'8ED', 'Creature — Cleric').
card_original_text('samite healer'/'8ED', '{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.').
card_image_name('samite healer'/'8ED', 'samite healer').
card_uid('samite healer'/'8ED', '8ED:Samite Healer:samite healer').
card_rarity('samite healer'/'8ED', 'Common').
card_artist('samite healer'/'8ED', 'Anson Maddocks').
card_number('samite healer'/'8ED', '41').
card_flavor_text('samite healer'/'8ED', 'Healers ultimately acquire the divine gifts of spiritual and physical wholeness. The most devout are also granted the ability to pass physical wholeness on to others.').
card_multiverse_id('samite healer'/'8ED', '45170').

card_in_set('sanctimony', '8ED').
card_original_type('sanctimony'/'8ED', 'Enchantment').
card_original_text('sanctimony'/'8ED', 'Whenever an opponent taps a Mountain for mana, you may gain 1 life.').
card_image_name('sanctimony'/'8ED', 'sanctimony').
card_uid('sanctimony'/'8ED', '8ED:Sanctimony:sanctimony').
card_rarity('sanctimony'/'8ED', 'Uncommon').
card_artist('sanctimony'/'8ED', 'Patrick Faricy').
card_number('sanctimony'/'8ED', '42').
card_flavor_text('sanctimony'/'8ED', '\"The peasants labor for my profit. I approve.\"').
card_multiverse_id('sanctimony'/'8ED', '45197').

card_in_set('savannah lions', '8ED').
card_original_type('savannah lions'/'8ED', 'Creature — Lion').
card_original_text('savannah lions'/'8ED', '').
card_image_name('savannah lions'/'8ED', 'savannah lions').
card_uid('savannah lions'/'8ED', '8ED:Savannah Lions:savannah lions').
card_rarity('savannah lions'/'8ED', 'Rare').
card_artist('savannah lions'/'8ED', 'Carl Critchlow').
card_number('savannah lions'/'8ED', '43').
card_flavor_text('savannah lions'/'8ED', 'Even the brave are scared by a lion three times: first by its tracks, again by its roar, and one last time face to face.\n—Somali proverb').
card_multiverse_id('savannah lions'/'8ED', '45207').

card_in_set('scathe zombies', '8ED').
card_original_type('scathe zombies'/'8ED', 'Creature — Zombie').
card_original_text('scathe zombies'/'8ED', '').
card_image_name('scathe zombies'/'8ED', 'scathe zombies').
card_uid('scathe zombies'/'8ED', '8ED:Scathe Zombies:scathe zombies').
card_rarity('scathe zombies'/'8ED', 'Common').
card_artist('scathe zombies'/'8ED', 'Kev Walker').
card_number('scathe zombies'/'8ED', '160').
card_flavor_text('scathe zombies'/'8ED', '\"They groaned, they stirred, they all uprose,\nNor spake, nor moved their eyes;\nIt had been strange, even in a dream,\nTo have seen those dead men rise.\"\n—Samuel Taylor Coleridge,\n\"The Rime of the Ancient Mariner\"').
card_multiverse_id('scathe zombies'/'8ED', '45294').

card_in_set('sea eagle', '8ED').
card_original_type('sea eagle'/'8ED', 'Creature — Bird').
card_original_text('sea eagle'/'8ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)').
card_image_name('sea eagle'/'8ED', 'sea eagle').
card_uid('sea eagle'/'8ED', '8ED:Sea Eagle:sea eagle').
card_rarity('sea eagle'/'8ED', 'Common').
card_artist('sea eagle'/'8ED', 'Anthony S. Waters').
card_number('sea eagle'/'8ED', 'S4').
card_flavor_text('sea eagle'/'8ED', 'Where air meets water, fish meets talon.').
card_multiverse_id('sea eagle'/'8ED', '47786').

card_in_set('sea monster', '8ED').
card_original_type('sea monster'/'8ED', 'Creature — Serpent').
card_original_text('sea monster'/'8ED', 'Sea Monster can\'t attack unless defending player controls an Island.').
card_image_name('sea monster'/'8ED', 'sea monster').
card_uid('sea monster'/'8ED', '8ED:Sea Monster:sea monster').
card_rarity('sea monster'/'8ED', 'Common').
card_artist('sea monster'/'8ED', 'Daniel Gelon').
card_number('sea monster'/'8ED', '99').
card_flavor_text('sea monster'/'8ED', 'It\'s easy to believe the monster is a myth—until you feel three hundred thousand pounds of myth crashing down on your ship.').
card_multiverse_id('sea monster'/'8ED', '45234').

card_in_set('searing wind', '8ED').
card_original_type('searing wind'/'8ED', 'Instant').
card_original_text('searing wind'/'8ED', 'Searing Wind deals 10 damage to target creature or player.').
card_image_name('searing wind'/'8ED', 'searing wind').
card_uid('searing wind'/'8ED', '8ED:Searing Wind:searing wind').
card_rarity('searing wind'/'8ED', 'Rare').
card_artist('searing wind'/'8ED', 'John Matson').
card_number('searing wind'/'8ED', '218').
card_flavor_text('searing wind'/'8ED', '\"The first wind of ascension is Forger, burning away impurity.\"\n—Keld Triumphant').
card_multiverse_id('searing wind'/'8ED', '45397').

card_in_set('seasoned marshal', '8ED').
card_original_type('seasoned marshal'/'8ED', 'Creature — Soldier').
card_original_text('seasoned marshal'/'8ED', 'Whenever Seasoned Marshal attacks, you may tap target creature.').
card_image_name('seasoned marshal'/'8ED', 'seasoned marshal').
card_uid('seasoned marshal'/'8ED', '8ED:Seasoned Marshal:seasoned marshal').
card_rarity('seasoned marshal'/'8ED', 'Uncommon').
card_artist('seasoned marshal'/'8ED', 'Matthew D. Wilson').
card_number('seasoned marshal'/'8ED', '44').
card_flavor_text('seasoned marshal'/'8ED', 'She reads the battlefield like a map, always seeking the shortest route to victory.').
card_multiverse_id('seasoned marshal'/'8ED', '45193').

card_in_set('seismic assault', '8ED').
card_original_type('seismic assault'/'8ED', 'Enchantment').
card_original_text('seismic assault'/'8ED', 'Discard a land card from your hand: Seismic Assault deals 2 damage to target creature or player.').
card_image_name('seismic assault'/'8ED', 'seismic assault').
card_uid('seismic assault'/'8ED', '8ED:Seismic Assault:seismic assault').
card_rarity('seismic assault'/'8ED', 'Rare').
card_artist('seismic assault'/'8ED', 'Greg Staples').
card_number('seismic assault'/'8ED', '219').
card_flavor_text('seismic assault'/'8ED', 'Earth arms itself with fire.').
card_multiverse_id('seismic assault'/'8ED', '45392').

card_in_set('serpent warrior', '8ED').
card_original_type('serpent warrior'/'8ED', 'Creature — Soldier').
card_original_text('serpent warrior'/'8ED', 'When Serpent Warrior comes into play, you lose 3 life.').
card_image_name('serpent warrior'/'8ED', 'serpent warrior').
card_uid('serpent warrior'/'8ED', '8ED:Serpent Warrior:serpent warrior').
card_rarity('serpent warrior'/'8ED', 'Common').
card_artist('serpent warrior'/'8ED', 'Ron Spencer').
card_number('serpent warrior'/'8ED', '161').
card_flavor_text('serpent warrior'/'8ED', 'A hiss before dying.').
card_multiverse_id('serpent warrior'/'8ED', '45303').

card_in_set('serra angel', '8ED').
card_original_type('serra angel'/'8ED', 'Creature — Angel').
card_original_text('serra angel'/'8ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\nAttacking doesn\'t cause Serra Angel to tap.').
card_image_name('serra angel'/'8ED', 'serra angel').
card_uid('serra angel'/'8ED', '8ED:Serra Angel:serra angel').
card_rarity('serra angel'/'8ED', 'Rare').
card_artist('serra angel'/'8ED', 'Mark Zug').
card_number('serra angel'/'8ED', '45').
card_flavor_text('serra angel'/'8ED', 'When she flies above the good, they consider themselves blessed. When she flies above the wicked, they consider themselves dead.').
card_multiverse_id('serra angel'/'8ED', '45212').

card_in_set('sever soul', '8ED').
card_original_type('sever soul'/'8ED', 'Sorcery').
card_original_text('sever soul'/'8ED', 'Destroy target nonblack creature. It can\'t be regenerated. You gain life equal to its toughness.').
card_image_name('sever soul'/'8ED', 'sever soul').
card_uid('sever soul'/'8ED', '8ED:Sever Soul:sever soul').
card_rarity('sever soul'/'8ED', 'Uncommon').
card_artist('sever soul'/'8ED', 'Glen Angus').
card_number('sever soul'/'8ED', '162').
card_flavor_text('sever soul'/'8ED', '\"The mortal world is a grove of sweet fruit, and I am ready to harvest it.\"\n—Fallen angel').
card_multiverse_id('sever soul'/'8ED', '45317').

card_in_set('severed legion', '8ED').
card_original_type('severed legion'/'8ED', 'Creature — Zombie').
card_original_text('severed legion'/'8ED', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)').
card_image_name('severed legion'/'8ED', 'severed legion').
card_uid('severed legion'/'8ED', '8ED:Severed Legion:severed legion').
card_rarity('severed legion'/'8ED', 'Common').
card_artist('severed legion'/'8ED', 'Dany Orizio').
card_number('severed legion'/'8ED', '163').
card_flavor_text('severed legion'/'8ED', 'No one in Aphetto answers a knock at the door after sundown.').
card_multiverse_id('severed legion'/'8ED', '45290').

card_in_set('shatter', '8ED').
card_original_type('shatter'/'8ED', 'Instant').
card_original_text('shatter'/'8ED', 'Destroy target artifact.').
card_image_name('shatter'/'8ED', 'shatter').
card_uid('shatter'/'8ED', '8ED:Shatter:shatter').
card_rarity('shatter'/'8ED', 'Common').
card_artist('shatter'/'8ED', 'Jason Alexander Behnke').
card_number('shatter'/'8ED', '220').
card_multiverse_id('shatter'/'8ED', '45355').

card_in_set('shifting sky', '8ED').
card_original_type('shifting sky'/'8ED', 'Enchantment').
card_original_text('shifting sky'/'8ED', 'As Shifting Sky comes into play, choose a color.\nAll nonland permanents are the chosen color.').
card_image_name('shifting sky'/'8ED', 'shifting sky').
card_uid('shifting sky'/'8ED', '8ED:Shifting Sky:shifting sky').
card_rarity('shifting sky'/'8ED', 'Rare').
card_artist('shifting sky'/'8ED', 'Jerry Tiritilli').
card_number('shifting sky'/'8ED', '100').
card_flavor_text('shifting sky'/'8ED', 'Master the sky and you rule everything beneath it.\n—Metathran saying').
card_multiverse_id('shifting sky'/'8ED', '45257').

card_in_set('shivan dragon', '8ED').
card_original_type('shivan dragon'/'8ED', 'Creature — Dragon').
card_original_text('shivan dragon'/'8ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\n{R}: Shivan Dragon gets +1/+0 until end of turn.').
card_image_name('shivan dragon'/'8ED', 'shivan dragon').
card_uid('shivan dragon'/'8ED', '8ED:Shivan Dragon:shivan dragon').
card_rarity('shivan dragon'/'8ED', 'Rare').
card_artist('shivan dragon'/'8ED', 'Donato Giancola').
card_number('shivan dragon'/'8ED', '221').
card_flavor_text('shivan dragon'/'8ED', 'The undisputed master of the mountains of Shiv.').
card_multiverse_id('shivan dragon'/'8ED', '45388').

card_in_set('shivan oasis', '8ED').
card_original_type('shivan oasis'/'8ED', 'Land').
card_original_text('shivan oasis'/'8ED', 'Shivan Oasis comes into play tapped.\n{T}: Add {R} or {G} to your mana pool.').
card_image_name('shivan oasis'/'8ED', 'shivan oasis').
card_uid('shivan oasis'/'8ED', '8ED:Shivan Oasis:shivan oasis').
card_rarity('shivan oasis'/'8ED', 'Uncommon').
card_artist('shivan oasis'/'8ED', 'Rob Alexander').
card_number('shivan oasis'/'8ED', '326').
card_flavor_text('shivan oasis'/'8ED', 'Only the hardiest explorers survive to eat the fruit.').
card_multiverse_id('shivan oasis'/'8ED', '45494').

card_in_set('shock', '8ED').
card_original_type('shock'/'8ED', 'Instant').
card_original_text('shock'/'8ED', 'Shock deals 2 damage to target creature or player.').
card_image_name('shock'/'8ED', 'shock').
card_uid('shock'/'8ED', '8ED:Shock:shock').
card_rarity('shock'/'8ED', 'Common').
card_artist('shock'/'8ED', 'Randy Gallegos').
card_number('shock'/'8ED', '222').
card_flavor_text('shock'/'8ED', '\"I love lightning It\'s my best invention since the rock.\"\n—Toggo, goblin weaponsmith').
card_multiverse_id('shock'/'8ED', '45352').

card_in_set('shock troops', '8ED').
card_original_type('shock troops'/'8ED', 'Creature — Soldier').
card_original_text('shock troops'/'8ED', 'Sacrifice Shock Troops: Shock Troops deals 2 damage to target creature or player.').
card_image_name('shock troops'/'8ED', 'shock troops').
card_uid('shock troops'/'8ED', '8ED:Shock Troops:shock troops').
card_rarity('shock troops'/'8ED', 'Common').
card_artist('shock troops'/'8ED', 'Jeff Miracola').
card_number('shock troops'/'8ED', '223').
card_flavor_text('shock troops'/'8ED', 'War is a conflict that determines not who is right—but who is left.').
card_multiverse_id('shock troops'/'8ED', '45350').

card_in_set('silverback ape', '8ED').
card_original_type('silverback ape'/'8ED', 'Creature — Ape').
card_original_text('silverback ape'/'8ED', '').
card_image_name('silverback ape'/'8ED', 'silverback ape').
card_uid('silverback ape'/'8ED', '8ED:Silverback Ape:silverback ape').
card_rarity('silverback ape'/'8ED', 'Uncommon').
card_artist('silverback ape'/'8ED', 'Ron Spears').
card_number('silverback ape'/'8ED', 'S7').
card_flavor_text('silverback ape'/'8ED', 'It does not wear a crown or sit on a throne of gold, but those who have witnessed its power hail it nonetheless.').
card_multiverse_id('silverback ape'/'8ED', '49056').

card_in_set('sizzle', '8ED').
card_original_type('sizzle'/'8ED', 'Sorcery').
card_original_text('sizzle'/'8ED', 'Sizzle deals 3 damage to each opponent.').
card_image_name('sizzle'/'8ED', 'sizzle').
card_uid('sizzle'/'8ED', '8ED:Sizzle:sizzle').
card_rarity('sizzle'/'8ED', 'Common').
card_artist('sizzle'/'8ED', 'Christopher Moeller').
card_number('sizzle'/'8ED', '224').
card_flavor_text('sizzle'/'8ED', '\"Of course you should fight fire with fire. You should fight everything with fire.\"\n—Jaya Ballard, task mage').
card_multiverse_id('sizzle'/'8ED', '45358').

card_in_set('skull of orm', '8ED').
card_original_type('skull of orm'/'8ED', 'Artifact').
card_original_text('skull of orm'/'8ED', '{5}, {T}: Return target enchantment card from your graveyard to your hand.').
card_image_name('skull of orm'/'8ED', 'skull of orm').
card_uid('skull of orm'/'8ED', '8ED:Skull of Orm:skull of orm').
card_rarity('skull of orm'/'8ED', 'Rare').
card_artist('skull of orm'/'8ED', 'Daren Bader').
card_number('skull of orm'/'8ED', '313').
card_flavor_text('skull of orm'/'8ED', 'Though lifeless, the skull still possessed a strange power over the flow of magic.').
card_multiverse_id('skull of orm'/'8ED', '45477').

card_in_set('slay', '8ED').
card_original_type('slay'/'8ED', 'Instant').
card_original_text('slay'/'8ED', 'Destroy target green creature. It can\'t be regenerated.\nDraw a card.').
card_image_name('slay'/'8ED', 'slay').
card_uid('slay'/'8ED', '8ED:Slay:slay').
card_rarity('slay'/'8ED', 'Uncommon').
card_artist('slay'/'8ED', 'Ben Thompson').
card_number('slay'/'8ED', '164').
card_flavor_text('slay'/'8ED', 'The elves had the edge in guile, skill, and valor. But in the end, only sheer numbers mattered.').
card_multiverse_id('slay'/'8ED', '45314').

card_in_set('sneaky homunculus', '8ED').
card_original_type('sneaky homunculus'/'8ED', 'Creature — Illusion').
card_original_text('sneaky homunculus'/'8ED', 'Sneaky Homunculus can\'t block or be blocked by creatures with power 2 or greater.').
card_image_name('sneaky homunculus'/'8ED', 'sneaky homunculus').
card_uid('sneaky homunculus'/'8ED', '8ED:Sneaky Homunculus:sneaky homunculus').
card_rarity('sneaky homunculus'/'8ED', 'Common').
card_artist('sneaky homunculus'/'8ED', 'Scott M. Fischer').
card_number('sneaky homunculus'/'8ED', '101').
card_flavor_text('sneaky homunculus'/'8ED', 'Keep watch only for the giants and you\'ll be eaten by the ants.').
card_multiverse_id('sneaky homunculus'/'8ED', '45228').

card_in_set('solidarity', '8ED').
card_original_type('solidarity'/'8ED', 'Instant').
card_original_text('solidarity'/'8ED', 'Creatures you control get +0/+5 until end of turn.').
card_image_name('solidarity'/'8ED', 'solidarity').
card_uid('solidarity'/'8ED', '8ED:Solidarity:solidarity').
card_rarity('solidarity'/'8ED', 'Common').
card_artist('solidarity'/'8ED', 'John Zeleznik').
card_number('solidarity'/'8ED', '46').
card_flavor_text('solidarity'/'8ED', '\"We must all hang together, or assuredly we shall all hang separately.\"\n—Benjamin Franklin').
card_multiverse_id('solidarity'/'8ED', '45188').

card_in_set('soul feast', '8ED').
card_original_type('soul feast'/'8ED', 'Sorcery').
card_original_text('soul feast'/'8ED', 'Target player loses 4 life and you gain 4 life.').
card_image_name('soul feast'/'8ED', 'soul feast').
card_uid('soul feast'/'8ED', '8ED:Soul Feast:soul feast').
card_rarity('soul feast'/'8ED', 'Uncommon').
card_artist('soul feast'/'8ED', 'Adam Rex').
card_number('soul feast'/'8ED', '165').
card_flavor_text('soul feast'/'8ED', 'You are who you eat.').
card_multiverse_id('soul feast'/'8ED', '45321').

card_in_set('spellbook', '8ED').
card_original_type('spellbook'/'8ED', 'Artifact').
card_original_text('spellbook'/'8ED', 'You have no maximum hand size.').
card_image_name('spellbook'/'8ED', 'spellbook').
card_uid('spellbook'/'8ED', '8ED:Spellbook:spellbook').
card_rarity('spellbook'/'8ED', 'Uncommon').
card_artist('spellbook'/'8ED', 'Andrew Goldhawk').
card_number('spellbook'/'8ED', '314').
card_flavor_text('spellbook'/'8ED', 'Knowledge knows no bounds.').
card_multiverse_id('spellbook'/'8ED', '45457').

card_in_set('spiketail hatchling', '8ED').
card_original_type('spiketail hatchling'/'8ED', 'Creature — Drake').
card_original_text('spiketail hatchling'/'8ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\nSacrifice Spiketail Hatchling: Counter target spell unless its controller pays {1}.').
card_image_name('spiketail hatchling'/'8ED', 'spiketail hatchling').
card_uid('spiketail hatchling'/'8ED', '8ED:Spiketail Hatchling:spiketail hatchling').
card_rarity('spiketail hatchling'/'8ED', 'Uncommon').
card_artist('spiketail hatchling'/'8ED', 'Greg Staples').
card_number('spiketail hatchling'/'8ED', '102').
card_flavor_text('spiketail hatchling'/'8ED', 'It dodges waves of water to prepare for waves of magic.').
card_multiverse_id('spiketail hatchling'/'8ED', '45232').

card_in_set('spined wurm', '8ED').
card_original_type('spined wurm'/'8ED', 'Creature — Wurm').
card_original_text('spined wurm'/'8ED', '').
card_image_name('spined wurm'/'8ED', 'spined wurm').
card_uid('spined wurm'/'8ED', '8ED:Spined Wurm:spined wurm').
card_rarity('spined wurm'/'8ED', 'Common').
card_artist('spined wurm'/'8ED', 'Keith Parkinson').
card_number('spined wurm'/'8ED', '279').
card_flavor_text('spined wurm'/'8ED', '\"I wouldn\'t stand in front of that wurm, son. \'Course, I wouldn\'t stand behind it neither. In fact, standing anywhere near that wurm\'s not much of a plan. Running, now that\'s a plan\"\n—Wandering mage').
card_multiverse_id('spined wurm'/'8ED', '45410').

card_in_set('spineless thug', '8ED').
card_original_type('spineless thug'/'8ED', 'Creature — Mercenary').
card_original_text('spineless thug'/'8ED', 'Spineless Thug can\'t block.').
card_image_name('spineless thug'/'8ED', 'spineless thug').
card_uid('spineless thug'/'8ED', '8ED:Spineless Thug:spineless thug').
card_rarity('spineless thug'/'8ED', 'Common').
card_artist('spineless thug'/'8ED', 'Matthew D. Wilson').
card_number('spineless thug'/'8ED', '166').
card_flavor_text('spineless thug'/'8ED', 'What it lacks in backbone, it makes up for in cruelty.').
card_multiverse_id('spineless thug'/'8ED', '45287').

card_in_set('spirit link', '8ED').
card_original_type('spirit link'/'8ED', 'Enchant Creature').
card_original_text('spirit link'/'8ED', 'Whenever enchanted creature deals damage, you gain that much life.').
card_image_name('spirit link'/'8ED', 'spirit link').
card_uid('spirit link'/'8ED', '8ED:Spirit Link:spirit link').
card_rarity('spirit link'/'8ED', 'Uncommon').
card_artist('spirit link'/'8ED', 'Kev Walker').
card_number('spirit link'/'8ED', '47').
card_flavor_text('spirit link'/'8ED', '\"I am bound to all my children. I share in their joys, I rage at their injuries.\"').
card_multiverse_id('spirit link'/'8ED', '45183').

card_in_set('spitting spider', '8ED').
card_original_type('spitting spider'/'8ED', 'Creature — Spider').
card_original_text('spitting spider'/'8ED', 'Spitting Spider may block as though it had flying.\nSacrifice a land: Spitting Spider deals 1 damage to each creature with flying.').
card_image_name('spitting spider'/'8ED', 'spitting spider').
card_uid('spitting spider'/'8ED', '8ED:Spitting Spider:spitting spider').
card_rarity('spitting spider'/'8ED', 'Uncommon').
card_artist('spitting spider'/'8ED', 'Edward P. Beard, Jr.').
card_number('spitting spider'/'8ED', '280').
card_multiverse_id('spitting spider'/'8ED', '45427').

card_in_set('spreading algae', '8ED').
card_original_type('spreading algae'/'8ED', 'Enchant Land').
card_original_text('spreading algae'/'8ED', 'Spreading Algae can enchant only a Swamp.\nWhen enchanted land becomes tapped, destroy that land.\nWhen Spreading Algae is put into a graveyard from play, return Spreading Algae to its owner\'s hand.').
card_image_name('spreading algae'/'8ED', 'spreading algae').
card_uid('spreading algae'/'8ED', '8ED:Spreading Algae:spreading algae').
card_rarity('spreading algae'/'8ED', 'Uncommon').
card_artist('spreading algae'/'8ED', 'Stephen Daniele').
card_number('spreading algae'/'8ED', '281').
card_multiverse_id('spreading algae'/'8ED', '45432').

card_in_set('standing troops', '8ED').
card_original_type('standing troops'/'8ED', 'Creature — Soldier').
card_original_text('standing troops'/'8ED', 'Attacking doesn\'t cause Standing Troops to tap.').
card_image_name('standing troops'/'8ED', 'standing troops').
card_uid('standing troops'/'8ED', '8ED:Standing Troops:standing troops').
card_rarity('standing troops'/'8ED', 'Common').
card_artist('standing troops'/'8ED', 'Daren Bader').
card_number('standing troops'/'8ED', '48').
card_flavor_text('standing troops'/'8ED', 'The less you have, the harder you fight for it.').
card_multiverse_id('standing troops'/'8ED', '45174').

card_in_set('star compass', '8ED').
card_original_type('star compass'/'8ED', 'Artifact').
card_original_text('star compass'/'8ED', 'Star Compass comes into play tapped.\n{T}: Add to your mana pool one mana of any color a basic land you control could produce.').
card_image_name('star compass'/'8ED', 'star compass').
card_uid('star compass'/'8ED', '8ED:Star Compass:star compass').
card_rarity('star compass'/'8ED', 'Uncommon').
card_artist('star compass'/'8ED', 'Donato Giancola').
card_number('star compass'/'8ED', '315').
card_flavor_text('star compass'/'8ED', 'It doesn\'t point north. It points home.').
card_multiverse_id('star compass'/'8ED', '45463').

card_in_set('staunch defenders', '8ED').
card_original_type('staunch defenders'/'8ED', 'Creature — Soldier').
card_original_text('staunch defenders'/'8ED', 'When Staunch Defenders comes into play, you gain 4 life.').
card_image_name('staunch defenders'/'8ED', 'staunch defenders').
card_uid('staunch defenders'/'8ED', '8ED:Staunch Defenders:staunch defenders').
card_rarity('staunch defenders'/'8ED', 'Uncommon').
card_artist('staunch defenders'/'8ED', 'Tristan Elwell').
card_number('staunch defenders'/'8ED', '49').
card_flavor_text('staunch defenders'/'8ED', 'The key to winning any fight is simply staying alive.').
card_multiverse_id('staunch defenders'/'8ED', '45196').

card_in_set('steal artifact', '8ED').
card_original_type('steal artifact'/'8ED', 'Enchant Artifact').
card_original_text('steal artifact'/'8ED', 'You control enchanted artifact.').
card_image_name('steal artifact'/'8ED', 'steal artifact').
card_uid('steal artifact'/'8ED', '8ED:Steal Artifact:steal artifact').
card_rarity('steal artifact'/'8ED', 'Uncommon').
card_artist('steal artifact'/'8ED', 'Peter Bollinger').
card_number('steal artifact'/'8ED', '103').
card_multiverse_id('steal artifact'/'8ED', '45263').

card_in_set('stone rain', '8ED').
card_original_type('stone rain'/'8ED', 'Sorcery').
card_original_text('stone rain'/'8ED', 'Destroy target land.').
card_image_name('stone rain'/'8ED', 'stone rain').
card_uid('stone rain'/'8ED', '8ED:Stone Rain:stone rain').
card_rarity('stone rain'/'8ED', 'Common').
card_artist('stone rain'/'8ED', 'John Matson').
card_number('stone rain'/'8ED', '225').
card_multiverse_id('stone rain'/'8ED', '45360').

card_in_set('storm crow', '8ED').
card_original_type('storm crow'/'8ED', 'Creature — Bird').
card_original_text('storm crow'/'8ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)').
card_image_name('storm crow'/'8ED', 'storm crow').
card_uid('storm crow'/'8ED', '8ED:Storm Crow:storm crow').
card_rarity('storm crow'/'8ED', 'Common').
card_artist('storm crow'/'8ED', 'John Matson').
card_number('storm crow'/'8ED', '104').
card_flavor_text('storm crow'/'8ED', 'Storm crow descending, winter unending. Storm crow departing, summer is starting.').
card_multiverse_id('storm crow'/'8ED', '45227').

card_in_set('story circle', '8ED').
card_original_type('story circle'/'8ED', 'Enchantment').
card_original_text('story circle'/'8ED', 'As Story Circle comes into play, choose a color.\n{W}: The next time a source of your choice of the chosen color would deal damage to you this turn, prevent that damage.').
card_image_name('story circle'/'8ED', 'story circle').
card_uid('story circle'/'8ED', '8ED:Story Circle:story circle').
card_rarity('story circle'/'8ED', 'Rare').
card_artist('story circle'/'8ED', 'Alan Pollack').
card_number('story circle'/'8ED', '50').
card_multiverse_id('story circle'/'8ED', '45220').

card_in_set('stream of life', '8ED').
card_original_type('stream of life'/'8ED', 'Sorcery').
card_original_text('stream of life'/'8ED', 'Target player gains X life.').
card_image_name('stream of life'/'8ED', 'stream of life').
card_uid('stream of life'/'8ED', '8ED:Stream of Life:stream of life').
card_rarity('stream of life'/'8ED', 'Uncommon').
card_artist('stream of life'/'8ED', 'Andrew Goldhawk').
card_number('stream of life'/'8ED', '282').
card_multiverse_id('stream of life'/'8ED', '45419').

card_in_set('sudden impact', '8ED').
card_original_type('sudden impact'/'8ED', 'Instant').
card_original_text('sudden impact'/'8ED', 'Sudden Impact deals damage equal to the number of cards in target player\'s hand to that player.').
card_image_name('sudden impact'/'8ED', 'sudden impact').
card_uid('sudden impact'/'8ED', '8ED:Sudden Impact:sudden impact').
card_rarity('sudden impact'/'8ED', 'Uncommon').
card_artist('sudden impact'/'8ED', 'Greg & Tim Hildebrandt').
card_number('sudden impact'/'8ED', '226').
card_flavor_text('sudden impact'/'8ED', 'Most people know better. Goblins only know bigger.').
card_multiverse_id('sudden impact'/'8ED', '45380').

card_in_set('suntail hawk', '8ED').
card_original_type('suntail hawk'/'8ED', 'Creature — Bird').
card_original_text('suntail hawk'/'8ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)').
card_image_name('suntail hawk'/'8ED', 'suntail hawk').
card_uid('suntail hawk'/'8ED', '8ED:Suntail Hawk:suntail hawk').
card_rarity('suntail hawk'/'8ED', 'Common').
card_artist('suntail hawk'/'8ED', 'Heather Hudson').
card_number('suntail hawk'/'8ED', '51').
card_flavor_text('suntail hawk'/'8ED', 'Its eye the glaring sun, its cry the keening wind.').
card_multiverse_id('suntail hawk'/'8ED', '45189').

card_in_set('sunweb', '8ED').
card_original_type('sunweb'/'8ED', 'Creature — Wall').
card_original_text('sunweb'/'8ED', '(Walls can\'t attack.)\nFlying (This creature may block creatures with flying.)\nSunweb can\'t block creatures with power 2 or less.').
card_image_name('sunweb'/'8ED', 'sunweb').
card_uid('sunweb'/'8ED', '8ED:Sunweb:sunweb').
card_rarity('sunweb'/'8ED', 'Rare').
card_artist('sunweb'/'8ED', 'Greg Staples').
card_number('sunweb'/'8ED', '52').
card_flavor_text('sunweb'/'8ED', 'Sometimes the wings of fallen dragons refuse to give up the sky.').
card_multiverse_id('sunweb'/'8ED', '45209').

card_in_set('swamp', '8ED').
card_original_type('swamp'/'8ED', 'Basic Land — Swamp').
card_original_text('swamp'/'8ED', 'B').
card_image_name('swamp'/'8ED', 'swamp1').
card_uid('swamp'/'8ED', '8ED:Swamp:swamp1').
card_rarity('swamp'/'8ED', 'Basic Land').
card_artist('swamp'/'8ED', 'Bob Eggleton').
card_number('swamp'/'8ED', '339').
card_multiverse_id('swamp'/'8ED', '46445').

card_in_set('swamp', '8ED').
card_original_type('swamp'/'8ED', 'Basic Land — Swamp').
card_original_text('swamp'/'8ED', 'B').
card_image_name('swamp'/'8ED', 'swamp2').
card_uid('swamp'/'8ED', '8ED:Swamp:swamp2').
card_rarity('swamp'/'8ED', 'Basic Land').
card_artist('swamp'/'8ED', 'John Avon').
card_number('swamp'/'8ED', '340').
card_multiverse_id('swamp'/'8ED', '46446').

card_in_set('swamp', '8ED').
card_original_type('swamp'/'8ED', 'Basic Land — Swamp').
card_original_text('swamp'/'8ED', 'B').
card_image_name('swamp'/'8ED', 'swamp3').
card_uid('swamp'/'8ED', '8ED:Swamp:swamp3').
card_rarity('swamp'/'8ED', 'Basic Land').
card_artist('swamp'/'8ED', 'Dan Frazier').
card_number('swamp'/'8ED', '341').
card_multiverse_id('swamp'/'8ED', '46447').

card_in_set('swamp', '8ED').
card_original_type('swamp'/'8ED', 'Basic Land — Swamp').
card_original_text('swamp'/'8ED', 'B').
card_image_name('swamp'/'8ED', 'swamp4').
card_uid('swamp'/'8ED', '8ED:Swamp:swamp4').
card_rarity('swamp'/'8ED', 'Basic Land').
card_artist('swamp'/'8ED', 'Larry Elmore').
card_number('swamp'/'8ED', '342').
card_multiverse_id('swamp'/'8ED', '46448').

card_in_set('swarm of rats', '8ED').
card_original_type('swarm of rats'/'8ED', 'Creature — Rat').
card_original_text('swarm of rats'/'8ED', 'Swarm of Rats\'s power is equal to the number of Rats you control.').
card_image_name('swarm of rats'/'8ED', 'swarm of rats').
card_uid('swarm of rats'/'8ED', '8ED:Swarm of Rats:swarm of rats').
card_rarity('swarm of rats'/'8ED', 'Uncommon').
card_artist('swarm of rats'/'8ED', 'Kev Walker').
card_number('swarm of rats'/'8ED', '167').
card_flavor_text('swarm of rats'/'8ED', '\"Rats, rats, rats Hundreds, thousands, millions of them, and every one a life.\"\n—Bram Stoker, Dracula').
card_multiverse_id('swarm of rats'/'8ED', '45305').

card_in_set('sword dancer', '8ED').
card_original_type('sword dancer'/'8ED', 'Creature — Rebel').
card_original_text('sword dancer'/'8ED', '{W}{W}: Target attacking creature gets -1/-0 until end of turn.').
card_image_name('sword dancer'/'8ED', 'sword dancer').
card_uid('sword dancer'/'8ED', '8ED:Sword Dancer:sword dancer').
card_rarity('sword dancer'/'8ED', 'Uncommon').
card_artist('sword dancer'/'8ED', 'Roger Raupp').
card_number('sword dancer'/'8ED', '53').
card_flavor_text('sword dancer'/'8ED', 'Most soldiers think of the sword as a weapon. In the hands of Zho monks, swords are also the strongest of shields.').
card_multiverse_id('sword dancer'/'8ED', '45190').

card_in_set('teferi\'s puzzle box', '8ED').
card_original_type('teferi\'s puzzle box'/'8ED', 'Artifact').
card_original_text('teferi\'s puzzle box'/'8ED', 'At the beginning of each player\'s draw step, that player puts the cards in his or her hand on the bottom of his or her library in any order, then draws that many cards. (That player draws his or her card for the turn first.)').
card_image_name('teferi\'s puzzle box'/'8ED', 'teferi\'s puzzle box').
card_uid('teferi\'s puzzle box'/'8ED', '8ED:Teferi\'s Puzzle Box:teferi\'s puzzle box').
card_rarity('teferi\'s puzzle box'/'8ED', 'Rare').
card_artist('teferi\'s puzzle box'/'8ED', 'Donato Giancola').
card_number('teferi\'s puzzle box'/'8ED', '316').
card_multiverse_id('teferi\'s puzzle box'/'8ED', '45483').

card_in_set('telepathy', '8ED').
card_original_type('telepathy'/'8ED', 'Enchantment').
card_original_text('telepathy'/'8ED', 'Your opponents play with their hands revealed.').
card_image_name('telepathy'/'8ED', 'telepathy').
card_uid('telepathy'/'8ED', '8ED:Telepathy:telepathy').
card_rarity('telepathy'/'8ED', 'Uncommon').
card_artist('telepathy'/'8ED', 'Matthew D. Wilson').
card_number('telepathy'/'8ED', '105').
card_flavor_text('telepathy'/'8ED', '\"Secrets? What secrets?\"').
card_multiverse_id('telepathy'/'8ED', '45256').

card_in_set('temporal adept', '8ED').
card_original_type('temporal adept'/'8ED', 'Creature — Wizard').
card_original_text('temporal adept'/'8ED', '{U}{U}{U}, {T}: Return target permanent to its owner\'s hand.').
card_image_name('temporal adept'/'8ED', 'temporal adept').
card_uid('temporal adept'/'8ED', '8ED:Temporal Adept:temporal adept').
card_rarity('temporal adept'/'8ED', 'Rare').
card_artist('temporal adept'/'8ED', 'Roger Raupp').
card_number('temporal adept'/'8ED', '106').
card_flavor_text('temporal adept'/'8ED', '\"Of course she\'s at the head of her class. All of her classmates have disappeared.\"\n—Tolarian renegade').
card_multiverse_id('temporal adept'/'8ED', '45266').

card_in_set('thieves\' auction', '8ED').
card_original_type('thieves\' auction'/'8ED', 'Sorcery').
card_original_text('thieves\' auction'/'8ED', 'Set aside all cards in play. Starting with you, each player chooses one of the cards set aside and puts it into play tapped under his or her control. Repeat this process until all those cards have been chosen.').
card_image_name('thieves\' auction'/'8ED', 'thieves\' auction').
card_uid('thieves\' auction'/'8ED', '8ED:Thieves\' Auction:thieves\' auction').
card_rarity('thieves\' auction'/'8ED', 'Rare').
card_artist('thieves\' auction'/'8ED', 'Kevin Murphy').
card_number('thieves\' auction'/'8ED', '227').
card_multiverse_id('thieves\' auction'/'8ED', '45395').

card_in_set('thieving magpie', '8ED').
card_original_type('thieving magpie'/'8ED', 'Creature — Bird').
card_original_text('thieving magpie'/'8ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\nWhenever Thieving Magpie deals damage to an opponent, you draw a card.').
card_image_name('thieving magpie'/'8ED', 'thieving magpie').
card_uid('thieving magpie'/'8ED', '8ED:Thieving Magpie:thieving magpie').
card_rarity('thieving magpie'/'8ED', 'Uncommon').
card_artist('thieving magpie'/'8ED', 'Christopher Moeller').
card_number('thieving magpie'/'8ED', '107').
card_flavor_text('thieving magpie'/'8ED', 'Other birds collect twigs for their nests. Magpies steal jewels for theirs.').
card_multiverse_id('thieving magpie'/'8ED', '45252').

card_in_set('thorn elemental', '8ED').
card_original_type('thorn elemental'/'8ED', 'Creature — Elemental').
card_original_text('thorn elemental'/'8ED', 'You may have Thorn Elemental deal its combat damage to defending player as though it weren\'t blocked.').
card_image_name('thorn elemental'/'8ED', 'thorn elemental').
card_uid('thorn elemental'/'8ED', '8ED:Thorn Elemental:thorn elemental').
card_rarity('thorn elemental'/'8ED', 'Rare').
card_artist('thorn elemental'/'8ED', 'rk post').
card_number('thorn elemental'/'8ED', '283').
card_flavor_text('thorn elemental'/'8ED', 'Rain from this storm leaves you pinned to the ground like an insect.').
card_multiverse_id('thorn elemental'/'8ED', '45448').

card_in_set('throne of bone', '8ED').
card_original_type('throne of bone'/'8ED', 'Artifact').
card_original_text('throne of bone'/'8ED', 'Whenever a player plays a black spell, you may pay {1}. If you do, you gain 1 life.').
card_image_name('throne of bone'/'8ED', 'throne of bone').
card_uid('throne of bone'/'8ED', '8ED:Throne of Bone:throne of bone').
card_rarity('throne of bone'/'8ED', 'Uncommon').
card_artist('throne of bone'/'8ED', 'Donato Giancola').
card_number('throne of bone'/'8ED', '317').
card_flavor_text('throne of bone'/'8ED', 'Tainted by a thousand fears.').
card_multiverse_id('throne of bone'/'8ED', '45460').

card_in_set('tidal kraken', '8ED').
card_original_type('tidal kraken'/'8ED', 'Creature — Monster').
card_original_text('tidal kraken'/'8ED', 'Tidal Kraken is unblockable.').
card_image_name('tidal kraken'/'8ED', 'tidal kraken').
card_uid('tidal kraken'/'8ED', '8ED:Tidal Kraken:tidal kraken').
card_rarity('tidal kraken'/'8ED', 'Rare').
card_artist('tidal kraken'/'8ED', 'Christopher Moeller').
card_number('tidal kraken'/'8ED', '108').
card_flavor_text('tidal kraken'/'8ED', 'To merfolk, pirates are a nuisance. To pirates, merfolk are a threat. To the kraken, they\'re both appetizers.').
card_multiverse_id('tidal kraken'/'8ED', '45271').

card_in_set('trade routes', '8ED').
card_original_type('trade routes'/'8ED', 'Enchantment').
card_original_text('trade routes'/'8ED', '{1}: Return target land you control to its owner\'s hand.\n{1}, Discard a land card from your hand: Draw a card.').
card_image_name('trade routes'/'8ED', 'trade routes').
card_uid('trade routes'/'8ED', '8ED:Trade Routes:trade routes').
card_rarity('trade routes'/'8ED', 'Rare').
card_artist('trade routes'/'8ED', 'Matt Cavotta').
card_number('trade routes'/'8ED', '109').
card_flavor_text('trade routes'/'8ED', 'The wise, the righteous, the mighty—the merchant feeds them all.').
card_multiverse_id('trade routes'/'8ED', '45272').

card_in_set('trained armodon', '8ED').
card_original_type('trained armodon'/'8ED', 'Creature — Elephant').
card_original_text('trained armodon'/'8ED', '').
card_image_name('trained armodon'/'8ED', 'trained armodon').
card_uid('trained armodon'/'8ED', '8ED:Trained Armodon:trained armodon').
card_rarity('trained armodon'/'8ED', 'Common').
card_artist('trained armodon'/'8ED', 'Gary Leach').
card_number('trained armodon'/'8ED', '284').
card_flavor_text('trained armodon'/'8ED', 'Armodons are trained to step on things. Enemy things.').
card_multiverse_id('trained armodon'/'8ED', '45406').

card_in_set('treasure trove', '8ED').
card_original_type('treasure trove'/'8ED', 'Enchantment').
card_original_text('treasure trove'/'8ED', '{2}{U}{U}: Draw a card.').
card_image_name('treasure trove'/'8ED', 'treasure trove').
card_uid('treasure trove'/'8ED', '8ED:Treasure Trove:treasure trove').
card_rarity('treasure trove'/'8ED', 'Uncommon').
card_artist('treasure trove'/'8ED', 'Brian Despain').
card_number('treasure trove'/'8ED', '110').
card_flavor_text('treasure trove'/'8ED', '\"Wealth means power; the power to subdue, to crush, to exploit, the power to enslave, to outrage, to degrade.\"\n—Emma Goldman, \"Anarchism\"').
card_multiverse_id('treasure trove'/'8ED', '45260').

card_in_set('tremor', '8ED').
card_original_type('tremor'/'8ED', 'Sorcery').
card_original_text('tremor'/'8ED', 'Tremor deals 1 damage to each creature without flying.').
card_image_name('tremor'/'8ED', 'tremor').
card_uid('tremor'/'8ED', '8ED:Tremor:tremor').
card_rarity('tremor'/'8ED', 'Common').
card_artist('tremor'/'8ED', 'Pete Venters').
card_number('tremor'/'8ED', '228').
card_flavor_text('tremor'/'8ED', 'Where do you run when the earth becomes your enemy?').
card_multiverse_id('tremor'/'8ED', '45377').

card_in_set('tundra wolves', '8ED').
card_original_type('tundra wolves'/'8ED', 'Creature — Wolf').
card_original_text('tundra wolves'/'8ED', 'First strike (This creature deals combat damage before creatures without first strike.)').
card_image_name('tundra wolves'/'8ED', 'tundra wolves').
card_uid('tundra wolves'/'8ED', '8ED:Tundra Wolves:tundra wolves').
card_rarity('tundra wolves'/'8ED', 'Common').
card_artist('tundra wolves'/'8ED', 'Richard Sardinha').
card_number('tundra wolves'/'8ED', '54').
card_flavor_text('tundra wolves'/'8ED', '\"I heard their eerie howling, the wolves calling their kindred across the frozen plains.\"\n—Onean scout').
card_multiverse_id('tundra wolves'/'8ED', '45167').

card_in_set('twiddle', '8ED').
card_original_type('twiddle'/'8ED', 'Instant').
card_original_text('twiddle'/'8ED', 'Tap or untap target artifact, creature, or land.').
card_image_name('twiddle'/'8ED', 'twiddle').
card_uid('twiddle'/'8ED', '8ED:Twiddle:twiddle').
card_rarity('twiddle'/'8ED', 'Common').
card_artist('twiddle'/'8ED', 'Matt Cavotta').
card_number('twiddle'/'8ED', '111').
card_multiverse_id('twiddle'/'8ED', '45235').

card_in_set('two-headed dragon', '8ED').
card_original_type('two-headed dragon'/'8ED', 'Creature — Dragon').
card_original_text('two-headed dragon'/'8ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\n{1}{R}: Two-Headed Dragon gets +2/+0 until end of turn.\nTwo-Headed Dragon can\'t be blocked except by two or more creatures.\nTwo-Headed Dragon may block an additional creature.').
card_image_name('two-headed dragon'/'8ED', 'two-headed dragon').
card_uid('two-headed dragon'/'8ED', '8ED:Two-Headed Dragon:two-headed dragon').
card_rarity('two-headed dragon'/'8ED', 'Rare').
card_artist('two-headed dragon'/'8ED', 'Sam Wood').
card_number('two-headed dragon'/'8ED', '229').
card_multiverse_id('two-headed dragon'/'8ED', '45389').

card_in_set('underworld dreams', '8ED').
card_original_type('underworld dreams'/'8ED', 'Enchantment').
card_original_text('underworld dreams'/'8ED', 'Whenever an opponent draws a card, Underworld Dreams deals 1 damage to him or her.').
card_image_name('underworld dreams'/'8ED', 'underworld dreams').
card_uid('underworld dreams'/'8ED', '8ED:Underworld Dreams:underworld dreams').
card_rarity('underworld dreams'/'8ED', 'Rare').
card_artist('underworld dreams'/'8ED', 'Carl Critchlow').
card_number('underworld dreams'/'8ED', '168').
card_flavor_text('underworld dreams'/'8ED', '\"In the drowsy dark cave of the mind, dreams build their nest with fragments dropped from day\'s caravan.\"\n—Rabindranath Tagore').
card_multiverse_id('underworld dreams'/'8ED', '45334').

card_in_set('unholy strength', '8ED').
card_original_type('unholy strength'/'8ED', 'Enchant Creature').
card_original_text('unholy strength'/'8ED', 'Enchanted creature gets +2/+1.').
card_image_name('unholy strength'/'8ED', 'unholy strength').
card_uid('unholy strength'/'8ED', '8ED:Unholy Strength:unholy strength').
card_rarity('unholy strength'/'8ED', 'Common').
card_artist('unholy strength'/'8ED', 'Puddnhead').
card_number('unholy strength'/'8ED', '169').
card_flavor_text('unholy strength'/'8ED', 'Such power grows the body as it shrinks the soul.').
card_multiverse_id('unholy strength'/'8ED', '45296').

card_in_set('unsummon', '8ED').
card_original_type('unsummon'/'8ED', 'Instant').
card_original_text('unsummon'/'8ED', 'Return target creature to its owner\'s hand.').
card_image_name('unsummon'/'8ED', 'unsummon').
card_uid('unsummon'/'8ED', '8ED:Unsummon:unsummon').
card_rarity('unsummon'/'8ED', 'Common').
card_artist('unsummon'/'8ED', 'Ron Spencer').
card_number('unsummon'/'8ED', '112').
card_multiverse_id('unsummon'/'8ED', '45239').

card_in_set('urborg volcano', '8ED').
card_original_type('urborg volcano'/'8ED', 'Land').
card_original_text('urborg volcano'/'8ED', 'Urborg Volcano comes into play tapped.\n{T}: Add {B} or {R} to your mana pool.').
card_image_name('urborg volcano'/'8ED', 'urborg volcano').
card_uid('urborg volcano'/'8ED', '8ED:Urborg Volcano:urborg volcano').
card_rarity('urborg volcano'/'8ED', 'Uncommon').
card_artist('urborg volcano'/'8ED', 'Tony Szczudlo').
card_number('urborg volcano'/'8ED', '327').
card_flavor_text('urborg volcano'/'8ED', 'Deep in the heart of Urborg lie massive volcanoes whose thick black smoke covers the land with perpetual darkness.').
card_multiverse_id('urborg volcano'/'8ED', '45493').

card_in_set('urza\'s armor', '8ED').
card_original_type('urza\'s armor'/'8ED', 'Artifact').
card_original_text('urza\'s armor'/'8ED', 'If a source would deal damage to you, prevent 1 of that damage.').
card_image_name('urza\'s armor'/'8ED', 'urza\'s armor').
card_uid('urza\'s armor'/'8ED', '8ED:Urza\'s Armor:urza\'s armor').
card_rarity('urza\'s armor'/'8ED', 'Rare').
card_artist('urza\'s armor'/'8ED', 'rk post').
card_number('urza\'s armor'/'8ED', '318').
card_flavor_text('urza\'s armor'/'8ED', 'Urza protected his body well, but neglected his soul.').
card_multiverse_id('urza\'s armor'/'8ED', '45484').

card_in_set('urza\'s mine', '8ED').
card_original_type('urza\'s mine'/'8ED', 'Land — Urza\'s Mine').
card_original_text('urza\'s mine'/'8ED', '{T}: Add {1} to your mana pool. If you control an Urza\'s Power-Plant and an Urza\'s Tower, add {2} to your mana pool instead.').
card_image_name('urza\'s mine'/'8ED', 'urza\'s mine').
card_uid('urza\'s mine'/'8ED', '8ED:Urza\'s Mine:urza\'s mine').
card_rarity('urza\'s mine'/'8ED', 'Uncommon').
card_artist('urza\'s mine'/'8ED', 'Brian Snõddy').
card_number('urza\'s mine'/'8ED', '328').
card_multiverse_id('urza\'s mine'/'8ED', '45464').

card_in_set('urza\'s power plant', '8ED').
card_original_type('urza\'s power plant'/'8ED', 'Land — Urza\'s Power-Plant').
card_original_text('urza\'s power plant'/'8ED', '{T}: Add {1} to your mana pool. If you control an Urza\'s Mine and an Urza\'s Tower, add {2} to your mana pool instead.').
card_image_name('urza\'s power plant'/'8ED', 'urza\'s power plant').
card_uid('urza\'s power plant'/'8ED', '8ED:Urza\'s Power Plant:urza\'s power plant').
card_rarity('urza\'s power plant'/'8ED', 'Uncommon').
card_artist('urza\'s power plant'/'8ED', 'Brian Snõddy').
card_number('urza\'s power plant'/'8ED', '329').
card_multiverse_id('urza\'s power plant'/'8ED', '45470').

card_in_set('urza\'s tower', '8ED').
card_original_type('urza\'s tower'/'8ED', 'Land — Urza\'s Tower').
card_original_text('urza\'s tower'/'8ED', '{T}: Add {1} to your mana pool. If you control an Urza\'s Mine and an Urza\'s Power-Plant, add {3} to your mana pool instead.').
card_image_name('urza\'s tower'/'8ED', 'urza\'s tower').
card_uid('urza\'s tower'/'8ED', '8ED:Urza\'s Tower:urza\'s tower').
card_rarity('urza\'s tower'/'8ED', 'Uncommon').
card_artist('urza\'s tower'/'8ED', 'Brian Snõddy').
card_number('urza\'s tower'/'8ED', '330').
card_multiverse_id('urza\'s tower'/'8ED', '45468').

card_in_set('vampiric spirit', '8ED').
card_original_type('vampiric spirit'/'8ED', 'Creature — Spirit').
card_original_text('vampiric spirit'/'8ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\nWhen Vampiric Spirit comes into play, you lose 4 life.').
card_image_name('vampiric spirit'/'8ED', 'vampiric spirit').
card_uid('vampiric spirit'/'8ED', '8ED:Vampiric Spirit:vampiric spirit').
card_rarity('vampiric spirit'/'8ED', 'Rare').
card_artist('vampiric spirit'/'8ED', 'Anson Maddocks').
card_number('vampiric spirit'/'8ED', '170').
card_multiverse_id('vampiric spirit'/'8ED', '45328').

card_in_set('venerable monk', '8ED').
card_original_type('venerable monk'/'8ED', 'Creature — Cleric').
card_original_text('venerable monk'/'8ED', 'When Venerable Monk comes into play, you gain 2 life.').
card_image_name('venerable monk'/'8ED', 'venerable monk').
card_uid('venerable monk'/'8ED', '8ED:Venerable Monk:venerable monk').
card_rarity('venerable monk'/'8ED', 'Common').
card_artist('venerable monk'/'8ED', 'D. Alexander Gregory').
card_number('venerable monk'/'8ED', '55').
card_flavor_text('venerable monk'/'8ED', 'Age wears the flesh but galvanizes the soul.').
card_multiverse_id('venerable monk'/'8ED', '45175').

card_in_set('vengeance', '8ED').
card_original_type('vengeance'/'8ED', 'Sorcery').
card_original_text('vengeance'/'8ED', 'Destroy target tapped creature.').
card_image_name('vengeance'/'8ED', 'vengeance').
card_uid('vengeance'/'8ED', '8ED:Vengeance:vengeance').
card_rarity('vengeance'/'8ED', 'Uncommon').
card_artist('vengeance'/'8ED', 'Paolo Parente').
card_number('vengeance'/'8ED', 'S2').
card_flavor_text('vengeance'/'8ED', 'Bitter as wormwood, sweet as mulled wine.').
card_multiverse_id('vengeance'/'8ED', '47788').

card_in_set('verduran enchantress', '8ED').
card_original_type('verduran enchantress'/'8ED', 'Creature — Druid').
card_original_text('verduran enchantress'/'8ED', 'Whenever you play an enchantment spell, you may draw a card.').
card_image_name('verduran enchantress'/'8ED', 'verduran enchantress').
card_uid('verduran enchantress'/'8ED', '8ED:Verduran Enchantress:verduran enchantress').
card_rarity('verduran enchantress'/'8ED', 'Rare').
card_artist('verduran enchantress'/'8ED', 'Rob Alexander').
card_number('verduran enchantress'/'8ED', '285').
card_flavor_text('verduran enchantress'/'8ED', '\"Graceful? Yes. Beautiful? Absolutely. Harmless? Definitely not.\"\n—Fyndhorn elder').
card_multiverse_id('verduran enchantress'/'8ED', '45443').

card_in_set('vernal bloom', '8ED').
card_original_type('vernal bloom'/'8ED', 'Enchantment').
card_original_text('vernal bloom'/'8ED', 'Whenever a Forest is tapped for mana, its controller adds {G} to his or her mana pool.').
card_image_name('vernal bloom'/'8ED', 'vernal bloom').
card_uid('vernal bloom'/'8ED', '8ED:Vernal Bloom:vernal bloom').
card_rarity('vernal bloom'/'8ED', 'Rare').
card_artist('vernal bloom'/'8ED', 'Bob Eggleton').
card_number('vernal bloom'/'8ED', '286').
card_flavor_text('vernal bloom'/'8ED', 'The sun breaks, the beasts stir, and the forest awakens to stand against its enemies.').
card_multiverse_id('vernal bloom'/'8ED', '45454').

card_in_set('vexing arcanix', '8ED').
card_original_type('vexing arcanix'/'8ED', 'Artifact').
card_original_text('vexing arcanix'/'8ED', '{3}, {T}: Target player names a card, then reveals the top card of his or her library. If it\'s the named card, the player puts it into his or her hand. Otherwise, the player puts it into his or her graveyard and Vexing Arcanix deals 2 damage to him or her.').
card_image_name('vexing arcanix'/'8ED', 'vexing arcanix').
card_uid('vexing arcanix'/'8ED', '8ED:Vexing Arcanix:vexing arcanix').
card_rarity('vexing arcanix'/'8ED', 'Rare').
card_artist('vexing arcanix'/'8ED', 'Jim Nelson').
card_number('vexing arcanix'/'8ED', '319').
card_multiverse_id('vexing arcanix'/'8ED', '45479').

card_in_set('viashino sandstalker', '8ED').
card_original_type('viashino sandstalker'/'8ED', 'Creature — Viashino').
card_original_text('viashino sandstalker'/'8ED', 'Haste (This creature may attack the turn it comes under your control.)\nAt end of turn, return Viashino Sandstalker to its owner\'s hand. (Return it only if it\'s in play.)').
card_image_name('viashino sandstalker'/'8ED', 'viashino sandstalker').
card_uid('viashino sandstalker'/'8ED', '8ED:Viashino Sandstalker:viashino sandstalker').
card_rarity('viashino sandstalker'/'8ED', 'Uncommon').
card_artist('viashino sandstalker'/'8ED', 'Andrew Robinson').
card_number('viashino sandstalker'/'8ED', '230').
card_flavor_text('viashino sandstalker'/'8ED', '\"Some believe sandstalkers to be illusions; those with scars know better.\"\n—Zhalfirin Guide to the Desert').
card_multiverse_id('viashino sandstalker'/'8ED', '45365').

card_in_set('vicious hunger', '8ED').
card_original_type('vicious hunger'/'8ED', 'Sorcery').
card_original_text('vicious hunger'/'8ED', 'Vicious Hunger deals 2 damage to target creature and you gain 2 life.').
card_image_name('vicious hunger'/'8ED', 'vicious hunger').
card_uid('vicious hunger'/'8ED', '8ED:Vicious Hunger:vicious hunger').
card_rarity('vicious hunger'/'8ED', 'Common').
card_artist('vicious hunger'/'8ED', 'Massimilano Frezzato').
card_number('vicious hunger'/'8ED', '171').
card_flavor_text('vicious hunger'/'8ED', 'Only the most ravenous soul feeds on the health of the weak.').
card_multiverse_id('vicious hunger'/'8ED', '45300').

card_in_set('vine trellis', '8ED').
card_original_type('vine trellis'/'8ED', 'Creature — Wall').
card_original_text('vine trellis'/'8ED', '(Walls can\'t attack.)\n{T}: Add {G} to your mana pool.').
card_image_name('vine trellis'/'8ED', 'vine trellis').
card_uid('vine trellis'/'8ED', '8ED:Vine Trellis:vine trellis').
card_rarity('vine trellis'/'8ED', 'Common').
card_artist('vine trellis'/'8ED', 'DiTerlizzi').
card_number('vine trellis'/'8ED', '287').
card_flavor_text('vine trellis'/'8ED', 'Nature twists knots stronger than any rope.').
card_multiverse_id('vine trellis'/'8ED', '45399').

card_in_set('vizzerdrix', '8ED').
card_original_type('vizzerdrix'/'8ED', 'Creature — Beast').
card_original_text('vizzerdrix'/'8ED', '').
card_image_name('vizzerdrix'/'8ED', 'vizzerdrix').
card_uid('vizzerdrix'/'8ED', '8ED:Vizzerdrix:vizzerdrix').
card_rarity('vizzerdrix'/'8ED', 'Rare').
card_artist('vizzerdrix'/'8ED', 'Dave Dorman').
card_number('vizzerdrix'/'8ED', 'S5').
card_flavor_text('vizzerdrix'/'8ED', 'A bored wizard once created a vizzerdrix out of a bunny and a piranha. He never made that mistake again.').
card_multiverse_id('vizzerdrix'/'8ED', '47789').

card_in_set('volcanic hammer', '8ED').
card_original_type('volcanic hammer'/'8ED', 'Sorcery').
card_original_text('volcanic hammer'/'8ED', 'Volcanic Hammer deals 3 damage to target creature or player.').
card_image_name('volcanic hammer'/'8ED', 'volcanic hammer').
card_uid('volcanic hammer'/'8ED', '8ED:Volcanic Hammer:volcanic hammer').
card_rarity('volcanic hammer'/'8ED', 'Common').
card_artist('volcanic hammer'/'8ED', 'Ben Thompson').
card_number('volcanic hammer'/'8ED', '231').
card_flavor_text('volcanic hammer'/'8ED', 'Fire finds its form in the heat of the forge.').
card_multiverse_id('volcanic hammer'/'8ED', '45356').

card_in_set('wall of air', '8ED').
card_original_type('wall of air'/'8ED', 'Creature — Wall').
card_original_text('wall of air'/'8ED', '(Walls can\'t attack.)\nFlying (This creature may block creatures with flying.)').
card_image_name('wall of air'/'8ED', 'wall of air').
card_uid('wall of air'/'8ED', '8ED:Wall of Air:wall of air').
card_rarity('wall of air'/'8ED', 'Uncommon').
card_artist('wall of air'/'8ED', 'John Avon').
card_number('wall of air'/'8ED', '113').
card_flavor_text('wall of air'/'8ED', 'When no falcons fly, beware the sky.\n—Femeref aphorism').
card_multiverse_id('wall of air'/'8ED', '45251').

card_in_set('wall of spears', '8ED').
card_original_type('wall of spears'/'8ED', 'Artifact Creature — Wall').
card_original_text('wall of spears'/'8ED', '(Walls can\'t attack.)\nFirst strike (This creature deals combat damage before creatures without first strike.)').
card_image_name('wall of spears'/'8ED', 'wall of spears').
card_uid('wall of spears'/'8ED', '8ED:Wall of Spears:wall of spears').
card_rarity('wall of spears'/'8ED', 'Uncommon').
card_artist('wall of spears'/'8ED', 'Christopher Moeller').
card_number('wall of spears'/'8ED', '320').
card_flavor_text('wall of spears'/'8ED', '\"A stick is your friend. A pointed stick is your good friend. An army of pointed sticks is your best friend.\"\n—Onean sergeant').
card_multiverse_id('wall of spears'/'8ED', '46616').

card_in_set('wall of stone', '8ED').
card_original_type('wall of stone'/'8ED', 'Creature — Wall').
card_original_text('wall of stone'/'8ED', '(Walls can\'t attack.)').
card_image_name('wall of stone'/'8ED', 'wall of stone').
card_uid('wall of stone'/'8ED', '8ED:Wall of Stone:wall of stone').
card_rarity('wall of stone'/'8ED', 'Uncommon').
card_artist('wall of stone'/'8ED', 'Rob Alexander').
card_number('wall of stone'/'8ED', '232').
card_flavor_text('wall of stone'/'8ED', 'The ground itself lends its strength to these walls of living stone, which possess the stability of ancient mountains. These mighty bulwarks thwart ground-based troops, providing welcome relief for weary warriors who defend the land.').
card_multiverse_id('wall of stone'/'8ED', '45368').

card_in_set('wall of swords', '8ED').
card_original_type('wall of swords'/'8ED', 'Creature — Wall').
card_original_text('wall of swords'/'8ED', '(Walls can\'t attack.)\nFlying (This creature may block creatures with flying.)').
card_image_name('wall of swords'/'8ED', 'wall of swords').
card_uid('wall of swords'/'8ED', '8ED:Wall of Swords:wall of swords').
card_rarity('wall of swords'/'8ED', 'Uncommon').
card_artist('wall of swords'/'8ED', 'Hannibal King').
card_number('wall of swords'/'8ED', '56').
card_flavor_text('wall of swords'/'8ED', 'Sharper than wind, lighter than air.').
card_multiverse_id('wall of swords'/'8ED', '45192').

card_in_set('warped devotion', '8ED').
card_original_type('warped devotion'/'8ED', 'Enchantment').
card_original_text('warped devotion'/'8ED', 'Whenever a permanent is returned to a player\'s hand, that player discards a card from his or her hand.').
card_image_name('warped devotion'/'8ED', 'warped devotion').
card_uid('warped devotion'/'8ED', '8ED:Warped Devotion:warped devotion').
card_rarity('warped devotion'/'8ED', 'Rare').
card_artist('warped devotion'/'8ED', 'Eric Peterson').
card_number('warped devotion'/'8ED', '172').
card_flavor_text('warped devotion'/'8ED', '\"The spirit of the devout is easily crushed by the loss of hope.\"\n—Fallen angel').
card_multiverse_id('warped devotion'/'8ED', '45333').

card_in_set('western paladin', '8ED').
card_original_type('western paladin'/'8ED', 'Creature — Knight').
card_original_text('western paladin'/'8ED', '{B}{B}, {T}: Destroy target white creature.').
card_image_name('western paladin'/'8ED', 'western paladin').
card_uid('western paladin'/'8ED', '8ED:Western Paladin:western paladin').
card_rarity('western paladin'/'8ED', 'Rare').
card_artist('western paladin'/'8ED', 'Carl Critchlow').
card_number('western paladin'/'8ED', '173').
card_flavor_text('western paladin'/'8ED', '\"Expect nothing but scorn, flattery, and lies. And never turn your back on him.\"\n—Northern paladin').
card_multiverse_id('western paladin'/'8ED', '45326').

card_in_set('wind drake', '8ED').
card_original_type('wind drake'/'8ED', 'Creature — Drake').
card_original_text('wind drake'/'8ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)').
card_image_name('wind drake'/'8ED', 'wind drake').
card_uid('wind drake'/'8ED', '8ED:Wind Drake:wind drake').
card_rarity('wind drake'/'8ED', 'Common').
card_artist('wind drake'/'8ED', 'Tom Wänerstrand').
card_number('wind drake'/'8ED', '114').
card_flavor_text('wind drake'/'8ED', '\"But high she shoots through air and light,\nAbove all low delay,\nWhere nothing earthly bounds her flight,\nNor shadow dims her way.\"\n—Thomas Moore, \"Oh that I had Wings\"').
card_multiverse_id('wind drake'/'8ED', '45230').

card_in_set('wing snare', '8ED').
card_original_type('wing snare'/'8ED', 'Sorcery').
card_original_text('wing snare'/'8ED', 'Destroy target creature with flying.').
card_image_name('wing snare'/'8ED', 'wing snare').
card_uid('wing snare'/'8ED', '8ED:Wing Snare:wing snare').
card_rarity('wing snare'/'8ED', 'Uncommon').
card_artist('wing snare'/'8ED', 'Daren Bader').
card_number('wing snare'/'8ED', '288').
card_flavor_text('wing snare'/'8ED', '\"We are the hands of the great trees, reaching out to ensnare what threatens their branches.\"\n—Elvish hunter').
card_multiverse_id('wing snare'/'8ED', '45436').

card_in_set('wood elves', '8ED').
card_original_type('wood elves'/'8ED', 'Creature — Elf').
card_original_text('wood elves'/'8ED', 'When Wood Elves comes into play, search your library for a Forest card and put that card into play. Then shuffle your library.').
card_image_name('wood elves'/'8ED', 'wood elves').
card_uid('wood elves'/'8ED', '8ED:Wood Elves:wood elves').
card_rarity('wood elves'/'8ED', 'Common').
card_artist('wood elves'/'8ED', 'Christopher Moeller').
card_number('wood elves'/'8ED', '289').
card_flavor_text('wood elves'/'8ED', 'Every branch a crossroads, every vine a swift steed.').
card_multiverse_id('wood elves'/'8ED', '45400').

card_in_set('wooden sphere', '8ED').
card_original_type('wooden sphere'/'8ED', 'Artifact').
card_original_text('wooden sphere'/'8ED', 'Whenever a player plays a green spell, you may pay {1}. If you do, you gain 1 life.').
card_image_name('wooden sphere'/'8ED', 'wooden sphere').
card_uid('wooden sphere'/'8ED', '8ED:Wooden Sphere:wooden sphere').
card_rarity('wooden sphere'/'8ED', 'Uncommon').
card_artist('wooden sphere'/'8ED', 'Donato Giancola').
card_number('wooden sphere'/'8ED', '321').
card_flavor_text('wooden sphere'/'8ED', 'Strengthened by a thousand limbs.').
card_multiverse_id('wooden sphere'/'8ED', '45462').

card_in_set('worship', '8ED').
card_original_type('worship'/'8ED', 'Enchantment').
card_original_text('worship'/'8ED', 'If you control a creature, damage that would reduce your life total to less than 1 reduces it to 1 instead.').
card_image_name('worship'/'8ED', 'worship').
card_uid('worship'/'8ED', '8ED:Worship:worship').
card_rarity('worship'/'8ED', 'Rare').
card_artist('worship'/'8ED', 'Mark Zug').
card_number('worship'/'8ED', '57').
card_flavor_text('worship'/'8ED', '\"Believe in the ideal, not the idol.\"\n—Serra').
card_multiverse_id('worship'/'8ED', '45219').

card_in_set('wrath of god', '8ED').
card_original_type('wrath of god'/'8ED', 'Sorcery').
card_original_text('wrath of god'/'8ED', 'Destroy all creatures. They can\'t be regenerated.').
card_image_name('wrath of god'/'8ED', 'wrath of god').
card_uid('wrath of god'/'8ED', '8ED:Wrath of God:wrath of god').
card_rarity('wrath of god'/'8ED', 'Rare').
card_artist('wrath of god'/'8ED', 'Kev Walker').
card_number('wrath of god'/'8ED', '58').
card_multiverse_id('wrath of god'/'8ED', '45222').

card_in_set('wrath of marit lage', '8ED').
card_original_type('wrath of marit lage'/'8ED', 'Enchantment').
card_original_text('wrath of marit lage'/'8ED', 'When Wrath of Marit Lage comes into play, tap all red creatures.\nRed creatures don\'t untap during their controllers\' untap steps.').
card_image_name('wrath of marit lage'/'8ED', 'wrath of marit lage').
card_uid('wrath of marit lage'/'8ED', '8ED:Wrath of Marit Lage:wrath of marit lage').
card_rarity('wrath of marit lage'/'8ED', 'Uncommon').
card_artist('wrath of marit lage'/'8ED', 'Matt Thompson').
card_number('wrath of marit lage'/'8ED', '115').
card_flavor_text('wrath of marit lage'/'8ED', '\"Marit Lage lies frozen in a glacier\'s heart. Still her dreams take form in our world, stealing the heat from our souls.\"\n—Halvor Arenson, Kjeldoran priest').
card_multiverse_id('wrath of marit lage'/'8ED', '45254').

card_in_set('yavimaya enchantress', '8ED').
card_original_type('yavimaya enchantress'/'8ED', 'Creature — Druid').
card_original_text('yavimaya enchantress'/'8ED', 'Yavimaya Enchantress gets +1/+1 for each enchantment in play.').
card_image_name('yavimaya enchantress'/'8ED', 'yavimaya enchantress').
card_uid('yavimaya enchantress'/'8ED', '8ED:Yavimaya Enchantress:yavimaya enchantress').
card_rarity('yavimaya enchantress'/'8ED', 'Uncommon').
card_artist('yavimaya enchantress'/'8ED', 'Matthew D. Wilson').
card_number('yavimaya enchantress'/'8ED', '290').
card_flavor_text('yavimaya enchantress'/'8ED', 'Her roots connect her to the forest\'s wishes.').
card_multiverse_id('yavimaya enchantress'/'8ED', '47601').

card_in_set('zombify', '8ED').
card_original_type('zombify'/'8ED', 'Sorcery').
card_original_text('zombify'/'8ED', 'Return target creature card from your graveyard to play.').
card_image_name('zombify'/'8ED', 'zombify').
card_uid('zombify'/'8ED', '8ED:Zombify:zombify').
card_rarity('zombify'/'8ED', 'Uncommon').
card_artist('zombify'/'8ED', 'Mark Romanoski').
card_number('zombify'/'8ED', '174').
card_flavor_text('zombify'/'8ED', '\"The first birth celebrates life. The second birth mocks it.\"\n—Mystic elder').
card_multiverse_id('zombify'/'8ED', '45318').

card_in_set('zur\'s weirding', '8ED').
card_original_type('zur\'s weirding'/'8ED', 'Enchantment').
card_original_text('zur\'s weirding'/'8ED', 'Players play with their hands revealed.\nIf a player would draw a card, he or she reveals it instead. Then any other player may pay 2 life. If a player does, put that card into its owner\'s graveyard. Otherwise, that player draws the card.').
card_image_name('zur\'s weirding'/'8ED', 'zur\'s weirding').
card_uid('zur\'s weirding'/'8ED', '8ED:Zur\'s Weirding:zur\'s weirding').
card_rarity('zur\'s weirding'/'8ED', 'Rare').
card_artist('zur\'s weirding'/'8ED', 'Scott M. Fischer').
card_number('zur\'s weirding'/'8ED', '116').
card_multiverse_id('zur\'s weirding'/'8ED', '45278').
