% Magic Player Rewards

set('pMPR').
set_name('pMPR', 'Magic Player Rewards').
set_release_date('pMPR', '2001-05-01').
set_border('pMPR', 'black').
set_type('pMPR', 'promo').

card_in_set('bituminous blast', 'pMPR').
card_original_type('bituminous blast'/'pMPR', 'Instant').
card_original_text('bituminous blast'/'pMPR', '').
card_first_print('bituminous blast', 'pMPR').
card_image_name('bituminous blast'/'pMPR', 'bituminous blast').
card_uid('bituminous blast'/'pMPR', 'pMPR:Bituminous Blast:bituminous blast').
card_rarity('bituminous blast'/'pMPR', 'Special').
card_artist('bituminous blast'/'pMPR', 'Aleksi Briclot').
card_number('bituminous blast'/'pMPR', '46').

card_in_set('blightning', 'pMPR').
card_original_type('blightning'/'pMPR', 'Sorcery').
card_original_text('blightning'/'pMPR', '').
card_first_print('blightning', 'pMPR').
card_image_name('blightning'/'pMPR', 'blightning').
card_uid('blightning'/'pMPR', 'pMPR:Blightning:blightning').
card_rarity('blightning'/'pMPR', 'Special').
card_artist('blightning'/'pMPR', 'Jim Pavelec').
card_number('blightning'/'pMPR', '36').

card_in_set('brave the elements', 'pMPR').
card_original_type('brave the elements'/'pMPR', 'Instant').
card_original_text('brave the elements'/'pMPR', '').
card_first_print('brave the elements', 'pMPR').
card_image_name('brave the elements'/'pMPR', 'brave the elements').
card_uid('brave the elements'/'pMPR', 'pMPR:Brave the Elements:brave the elements').
card_rarity('brave the elements'/'pMPR', 'Special').
card_artist('brave the elements'/'pMPR', 'Kekai Kotaki').
card_number('brave the elements'/'pMPR', '50').

card_in_set('burst lightning', 'pMPR').
card_original_type('burst lightning'/'pMPR', 'Instant').
card_original_text('burst lightning'/'pMPR', '').
card_first_print('burst lightning', 'pMPR').
card_image_name('burst lightning'/'pMPR', 'burst lightning').
card_uid('burst lightning'/'pMPR', 'pMPR:Burst Lightning:burst lightning').
card_rarity('burst lightning'/'pMPR', 'Special').
card_artist('burst lightning'/'pMPR', 'Jung Park').
card_number('burst lightning'/'pMPR', '47').

card_in_set('cancel', 'pMPR').
card_original_type('cancel'/'pMPR', 'Instant').
card_original_text('cancel'/'pMPR', '').
card_first_print('cancel', 'pMPR').
card_image_name('cancel'/'pMPR', 'cancel').
card_uid('cancel'/'pMPR', 'pMPR:Cancel:cancel').
card_rarity('cancel'/'pMPR', 'Special').
card_artist('cancel'/'pMPR', 'Izzy').
card_number('cancel'/'pMPR', '41').

card_in_set('celestial purge', 'pMPR').
card_original_type('celestial purge'/'pMPR', 'Instant').
card_original_text('celestial purge'/'pMPR', '').
card_first_print('celestial purge', 'pMPR').
card_image_name('celestial purge'/'pMPR', 'celestial purge').
card_uid('celestial purge'/'pMPR', 'pMPR:Celestial Purge:celestial purge').
card_rarity('celestial purge'/'pMPR', 'Special').
card_artist('celestial purge'/'pMPR', 'Nils Hamm').
card_number('celestial purge'/'pMPR', '45').

card_in_set('condemn', 'pMPR').
card_original_type('condemn'/'pMPR', 'Instant').
card_original_text('condemn'/'pMPR', '').
card_first_print('condemn', 'pMPR').
card_image_name('condemn'/'pMPR', 'condemn').
card_uid('condemn'/'pMPR', 'pMPR:Condemn:condemn').
card_rarity('condemn'/'pMPR', 'Special').
card_artist('condemn'/'pMPR', 'Kev Walker').
card_number('condemn'/'pMPR', '18').

card_in_set('corrupt', 'pMPR').
card_original_type('corrupt'/'pMPR', 'Sorcery').
card_original_text('corrupt'/'pMPR', '').
card_image_name('corrupt'/'pMPR', 'corrupt').
card_uid('corrupt'/'pMPR', 'pMPR:Corrupt:corrupt').
card_rarity('corrupt'/'pMPR', 'Special').
card_artist('corrupt'/'pMPR', 'Philip Straub').
card_number('corrupt'/'pMPR', '30').

card_in_set('cruel edict', 'pMPR').
card_original_type('cruel edict'/'pMPR', 'Sorcery').
card_original_text('cruel edict'/'pMPR', '').
card_image_name('cruel edict'/'pMPR', 'cruel edict').
card_uid('cruel edict'/'pMPR', 'pMPR:Cruel Edict:cruel edict').
card_rarity('cruel edict'/'pMPR', 'Special').
card_artist('cruel edict'/'pMPR', 'Steve Ellis').
card_number('cruel edict'/'pMPR', '21').

card_in_set('cryptic command', 'pMPR').
card_original_type('cryptic command'/'pMPR', 'Instant').
card_original_text('cryptic command'/'pMPR', '').
card_first_print('cryptic command', 'pMPR').
card_image_name('cryptic command'/'pMPR', 'cryptic command').
card_uid('cryptic command'/'pMPR', 'pMPR:Cryptic Command:cryptic command').
card_rarity('cryptic command'/'pMPR', 'Special').
card_artist('cryptic command'/'pMPR', 'Wayne England').
card_number('cryptic command'/'pMPR', '31').

card_in_set('damnation', 'pMPR').
card_original_type('damnation'/'pMPR', 'Sorcery').
card_original_text('damnation'/'pMPR', '').
card_first_print('damnation', 'pMPR').
card_image_name('damnation'/'pMPR', 'damnation').
card_uid('damnation'/'pMPR', 'pMPR:Damnation:damnation').
card_rarity('damnation'/'pMPR', 'Special').
card_artist('damnation'/'pMPR', 'Ron Spencer').
card_number('damnation'/'pMPR', '24').

card_in_set('day of judgment', 'pMPR').
card_original_type('day of judgment'/'pMPR', 'Sorcery').
card_original_text('day of judgment'/'pMPR', '').
card_image_name('day of judgment'/'pMPR', 'day of judgment').
card_uid('day of judgment'/'pMPR', 'pMPR:Day of Judgment:day of judgment').
card_rarity('day of judgment'/'pMPR', 'Special').
card_artist('day of judgment'/'pMPR', 'Svetlin Velinov').
card_number('day of judgment'/'pMPR', '49').

card_in_set('disenchant', 'pMPR').
card_original_type('disenchant'/'pMPR', 'Instant').
card_original_text('disenchant'/'pMPR', '').
card_image_name('disenchant'/'pMPR', 'disenchant').
card_uid('disenchant'/'pMPR', 'pMPR:Disenchant:disenchant').
card_rarity('disenchant'/'pMPR', 'Special').
card_artist('disenchant'/'pMPR', 'Heather Hudson').
card_number('disenchant'/'pMPR', '22').

card_in_set('doom blade', 'pMPR').
card_original_type('doom blade'/'pMPR', 'Instant').
card_original_text('doom blade'/'pMPR', '').
card_first_print('doom blade', 'pMPR').
card_image_name('doom blade'/'pMPR', 'doom blade').
card_uid('doom blade'/'pMPR', 'pMPR:Doom Blade:doom blade').
card_rarity('doom blade'/'pMPR', 'Special').
card_artist('doom blade'/'pMPR', 'Carl Critchlow').
card_number('doom blade'/'pMPR', '51').

card_in_set('fireball', 'pMPR').
card_original_type('fireball'/'pMPR', 'Sorcery').
card_original_text('fireball'/'pMPR', '').
card_image_name('fireball'/'pMPR', 'fireball').
card_uid('fireball'/'pMPR', 'pMPR:Fireball:fireball').
card_rarity('fireball'/'pMPR', 'Special').
card_artist('fireball'/'pMPR', 'Mark Tedin').
card_number('fireball'/'pMPR', '6').

card_in_set('flame javelin', 'pMPR').
card_original_type('flame javelin'/'pMPR', 'Instant').
card_original_text('flame javelin'/'pMPR', '').
card_first_print('flame javelin', 'pMPR').
card_image_name('flame javelin'/'pMPR', 'flame javelin').
card_uid('flame javelin'/'pMPR', 'pMPR:Flame Javelin:flame javelin').
card_rarity('flame javelin'/'pMPR', 'Special').
card_artist('flame javelin'/'pMPR', 'Zoltan Boros & Gabor Szikszai').
card_number('flame javelin'/'pMPR', '32').

card_in_set('giant growth', 'pMPR').
card_original_type('giant growth'/'pMPR', 'Instant').
card_original_text('giant growth'/'pMPR', '').
card_image_name('giant growth'/'pMPR', 'giant growth').
card_uid('giant growth'/'pMPR', 'pMPR:Giant Growth:giant growth').
card_rarity('giant growth'/'pMPR', 'Special').
card_artist('giant growth'/'pMPR', 'Joel Thomas').
card_number('giant growth'/'pMPR', '13').

card_in_set('harmonize', 'pMPR').
card_original_type('harmonize'/'pMPR', 'Sorcery').
card_original_text('harmonize'/'pMPR', '').
card_first_print('harmonize', 'pMPR').
card_image_name('harmonize'/'pMPR', 'harmonize').
card_uid('harmonize'/'pMPR', 'pMPR:Harmonize:harmonize').
card_rarity('harmonize'/'pMPR', 'Special').
card_artist('harmonize'/'pMPR', 'Vance Kovacs').
card_number('harmonize'/'pMPR', '28').

card_in_set('harrow', 'pMPR').
card_original_type('harrow'/'pMPR', 'Instant').
card_original_text('harrow'/'pMPR', '').
card_image_name('harrow'/'pMPR', 'harrow').
card_uid('harrow'/'pMPR', 'pMPR:Harrow:harrow').
card_rarity('harrow'/'pMPR', 'Special').
card_artist('harrow'/'pMPR', 'Lars Grant-West').
card_number('harrow'/'pMPR', '48').

card_in_set('hinder', 'pMPR').
card_original_type('hinder'/'pMPR', 'Instant').
card_original_text('hinder'/'pMPR', '').
card_first_print('hinder', 'pMPR').
card_image_name('hinder'/'pMPR', 'hinder').
card_uid('hinder'/'pMPR', 'pMPR:Hinder:hinder').
card_rarity('hinder'/'pMPR', 'Special').
card_artist('hinder'/'pMPR', 'Anthony S. Waters').
card_number('hinder'/'pMPR', '11').

card_in_set('hypnotic specter', 'pMPR').
card_original_type('hypnotic specter'/'pMPR', 'Creature — Specter').
card_original_text('hypnotic specter'/'pMPR', '').
card_image_name('hypnotic specter'/'pMPR', 'hypnotic specter').
card_uid('hypnotic specter'/'pMPR', 'pMPR:Hypnotic Specter:hypnotic specter').
card_rarity('hypnotic specter'/'pMPR', 'Special').
card_artist('hypnotic specter'/'pMPR', 'Douglas Shuler').
card_number('hypnotic specter'/'pMPR', '10').
card_flavor_text('hypnotic specter'/'pMPR', '\"There was no trace\nOf aught on that illumined face...\" -Samuel Taylor Coleridge, \"Phantom\"').

card_in_set('incinerate', 'pMPR').
card_original_type('incinerate'/'pMPR', 'Instant').
card_original_text('incinerate'/'pMPR', '').
card_image_name('incinerate'/'pMPR', 'incinerate').
card_uid('incinerate'/'pMPR', 'pMPR:Incinerate:incinerate').
card_rarity('incinerate'/'pMPR', 'Special').
card_artist('incinerate'/'pMPR', 'Zoltan Boros & Gabor Szikszai').
card_number('incinerate'/'pMPR', '26').

card_in_set('infest', 'pMPR').
card_original_type('infest'/'pMPR', 'Sorcery').
card_original_text('infest'/'pMPR', '').
card_first_print('infest', 'pMPR').
card_image_name('infest'/'pMPR', 'infest').
card_uid('infest'/'pMPR', 'pMPR:Infest:infest').
card_rarity('infest'/'pMPR', 'Special').
card_artist('infest'/'pMPR', 'Pete Venters').
card_number('infest'/'pMPR', '43').

card_in_set('lightning bolt', 'pMPR').
card_original_type('lightning bolt'/'pMPR', 'Instant').
card_original_text('lightning bolt'/'pMPR', '').
card_image_name('lightning bolt'/'pMPR', 'lightning bolt').
card_uid('lightning bolt'/'pMPR', 'pMPR:Lightning Bolt:lightning bolt').
card_rarity('lightning bolt'/'pMPR', 'Special').
card_artist('lightning bolt'/'pMPR', 'Véronique Meignaud').
card_number('lightning bolt'/'pMPR', '40').

card_in_set('lightning helix', 'pMPR').
card_original_type('lightning helix'/'pMPR', 'Instant').
card_original_text('lightning helix'/'pMPR', '').
card_first_print('lightning helix', 'pMPR').
card_image_name('lightning helix'/'pMPR', 'lightning helix').
card_uid('lightning helix'/'pMPR', 'pMPR:Lightning Helix:lightning helix').
card_rarity('lightning helix'/'pMPR', 'Special').
card_artist('lightning helix'/'pMPR', 'Kev Walker').
card_number('lightning helix'/'pMPR', '16').

card_in_set('mana leak', 'pMPR').
card_original_type('mana leak'/'pMPR', 'Instant').
card_original_text('mana leak'/'pMPR', '').
card_image_name('mana leak'/'pMPR', 'mana leak').
card_uid('mana leak'/'pMPR', 'pMPR:Mana Leak:mana leak').
card_rarity('mana leak'/'pMPR', 'Special').
card_artist('mana leak'/'pMPR', 'Christopher Rush').
card_number('mana leak'/'pMPR', '8').

card_in_set('mana tithe', 'pMPR').
card_original_type('mana tithe'/'pMPR', 'Instant').
card_original_text('mana tithe'/'pMPR', '').
card_first_print('mana tithe', 'pMPR').
card_image_name('mana tithe'/'pMPR', 'mana tithe').
card_uid('mana tithe'/'pMPR', 'pMPR:Mana Tithe:mana tithe').
card_rarity('mana tithe'/'pMPR', 'Special').
card_artist('mana tithe'/'pMPR', 'Donato Giancola').
card_number('mana tithe'/'pMPR', '27').

card_in_set('mortify', 'pMPR').
card_original_type('mortify'/'pMPR', 'Instant').
card_original_text('mortify'/'pMPR', '').
card_first_print('mortify', 'pMPR').
card_image_name('mortify'/'pMPR', 'mortify').
card_uid('mortify'/'pMPR', 'pMPR:Mortify:mortify').
card_rarity('mortify'/'pMPR', 'Special').
card_artist('mortify'/'pMPR', 'Brian Snõddy').
card_number('mortify'/'pMPR', '19').

card_in_set('nameless inversion', 'pMPR').
card_original_type('nameless inversion'/'pMPR', 'Tribal Instant — Shapeshifter').
card_original_text('nameless inversion'/'pMPR', '').
card_first_print('nameless inversion', 'pMPR').
card_image_name('nameless inversion'/'pMPR', 'nameless inversion').
card_uid('nameless inversion'/'pMPR', 'pMPR:Nameless Inversion:nameless inversion').
card_rarity('nameless inversion'/'pMPR', 'Special').
card_artist('nameless inversion'/'pMPR', 'Jesper Ejsing').
card_number('nameless inversion'/'pMPR', '34').

card_in_set('negate', 'pMPR').
card_original_type('negate'/'pMPR', 'Instant').
card_original_text('negate'/'pMPR', '').
card_first_print('negate', 'pMPR').
card_image_name('negate'/'pMPR', 'negate').
card_uid('negate'/'pMPR', 'pMPR:Negate:negate').
card_rarity('negate'/'pMPR', 'Special').
card_artist('negate'/'pMPR', 'Ralph Horsley').
card_number('negate'/'pMPR', '38').

card_in_set('oxidize', 'pMPR').
card_original_type('oxidize'/'pMPR', 'Instant').
card_original_text('oxidize'/'pMPR', '').
card_first_print('oxidize', 'pMPR').
card_image_name('oxidize'/'pMPR', 'oxidize').
card_uid('oxidize'/'pMPR', 'pMPR:Oxidize:oxidize').
card_rarity('oxidize'/'pMPR', 'Special').
card_artist('oxidize'/'pMPR', 'Scott M. Fischer').
card_number('oxidize'/'pMPR', '7').

card_in_set('ponder', 'pMPR').
card_original_type('ponder'/'pMPR', 'Sorcery').
card_original_text('ponder'/'pMPR', '').
card_first_print('ponder', 'pMPR').
card_image_name('ponder'/'pMPR', 'ponder').
card_uid('ponder'/'pMPR', 'pMPR:Ponder:ponder').
card_rarity('ponder'/'pMPR', 'Special').
card_artist('ponder'/'pMPR', 'Steve Argyle').
card_number('ponder'/'pMPR', '29').

card_in_set('powder keg', 'pMPR').
card_original_type('powder keg'/'pMPR', 'Artifact').
card_original_text('powder keg'/'pMPR', '').
card_image_name('powder keg'/'pMPR', 'powder keg').
card_uid('powder keg'/'pMPR', 'pMPR:Powder Keg:powder keg').
card_rarity('powder keg'/'pMPR', 'Special').
card_artist('powder keg'/'pMPR', 'Dan Frazier').
card_number('powder keg'/'pMPR', '3').

card_in_set('psionic blast', 'pMPR').
card_original_type('psionic blast'/'pMPR', 'Instant').
card_original_text('psionic blast'/'pMPR', '').
card_image_name('psionic blast'/'pMPR', 'psionic blast').
card_uid('psionic blast'/'pMPR', 'pMPR:Psionic Blast:psionic blast').
card_rarity('psionic blast'/'pMPR', 'Special').
card_artist('psionic blast'/'pMPR', 'Aleksi Briclot').
card_number('psionic blast'/'pMPR', '20').

card_in_set('psychatog', 'pMPR').
card_original_type('psychatog'/'pMPR', 'Creature — Atog').
card_original_text('psychatog'/'pMPR', '').
card_first_print('psychatog', 'pMPR').
card_image_name('psychatog'/'pMPR', 'psychatog').
card_uid('psychatog'/'pMPR', 'pMPR:Psychatog:psychatog').
card_rarity('psychatog'/'pMPR', 'Special').
card_artist('psychatog'/'pMPR', 'Edward P. Beard, Jr.').
card_number('psychatog'/'pMPR', '4').

card_in_set('putrefy', 'pMPR').
card_original_type('putrefy'/'pMPR', 'Instant').
card_original_text('putrefy'/'pMPR', '').
card_first_print('putrefy', 'pMPR').
card_image_name('putrefy'/'pMPR', 'putrefy').
card_uid('putrefy'/'pMPR', 'pMPR:Putrefy:putrefy').
card_rarity('putrefy'/'pMPR', 'Special').
card_artist('putrefy'/'pMPR', 'Richard Sardinha').
card_number('putrefy'/'pMPR', '14').

card_in_set('pyroclasm', 'pMPR').
card_original_type('pyroclasm'/'pMPR', 'Sorcery').
card_original_text('pyroclasm'/'pMPR', '').
card_image_name('pyroclasm'/'pMPR', 'pyroclasm').
card_uid('pyroclasm'/'pMPR', 'pMPR:Pyroclasm:pyroclasm').
card_rarity('pyroclasm'/'pMPR', 'Special').
card_artist('pyroclasm'/'pMPR', 'Dave Dorman').
card_number('pyroclasm'/'pMPR', '12').

card_in_set('rampant growth', 'pMPR').
card_original_type('rampant growth'/'pMPR', 'Sorcery').
card_original_text('rampant growth'/'pMPR', '').
card_image_name('rampant growth'/'pMPR', 'rampant growth').
card_uid('rampant growth'/'pMPR', 'pMPR:Rampant Growth:rampant growth').
card_rarity('rampant growth'/'pMPR', 'Special').
card_artist('rampant growth'/'pMPR', 'Terese Nielsen').
card_number('rampant growth'/'pMPR', '37').

card_in_set('reciprocate', 'pMPR').
card_original_type('reciprocate'/'pMPR', 'Instant').
card_original_text('reciprocate'/'pMPR', '').
card_first_print('reciprocate', 'pMPR').
card_image_name('reciprocate'/'pMPR', 'reciprocate').
card_uid('reciprocate'/'pMPR', 'pMPR:Reciprocate:reciprocate').
card_rarity('reciprocate'/'pMPR', 'Special').
card_artist('reciprocate'/'pMPR', 'Jim Murray').
card_number('reciprocate'/'pMPR', '9').

card_in_set('recollect', 'pMPR').
card_original_type('recollect'/'pMPR', 'Sorcery').
card_original_text('recollect'/'pMPR', '').
card_first_print('recollect', 'pMPR').
card_image_name('recollect'/'pMPR', 'recollect').
card_uid('recollect'/'pMPR', 'pMPR:Recollect:recollect').
card_rarity('recollect'/'pMPR', 'Special').
card_artist('recollect'/'pMPR', 'Michael Sutfin').
card_number('recollect'/'pMPR', '23').

card_in_set('remove soul', 'pMPR').
card_original_type('remove soul'/'pMPR', 'Instant').
card_original_text('remove soul'/'pMPR', '').
card_image_name('remove soul'/'pMPR', 'remove soul').
card_uid('remove soul'/'pMPR', 'pMPR:Remove Soul:remove soul').
card_rarity('remove soul'/'pMPR', 'Special').
card_artist('remove soul'/'pMPR', 'Mike Dringenberg').
card_number('remove soul'/'pMPR', '35').

card_in_set('searing blaze', 'pMPR').
card_original_type('searing blaze'/'pMPR', 'Instant').
card_original_text('searing blaze'/'pMPR', '').
card_first_print('searing blaze', 'pMPR').
card_image_name('searing blaze'/'pMPR', 'searing blaze').
card_uid('searing blaze'/'pMPR', 'pMPR:Searing Blaze:searing blaze').
card_rarity('searing blaze'/'pMPR', 'Special').
card_artist('searing blaze'/'pMPR', 'Jaime Jones').
card_number('searing blaze'/'pMPR', '53').

card_in_set('sign in blood', 'pMPR').
card_original_type('sign in blood'/'pMPR', 'Sorcery').
card_original_text('sign in blood'/'pMPR', '').
card_first_print('sign in blood', 'pMPR').
card_image_name('sign in blood'/'pMPR', 'sign in blood').
card_uid('sign in blood'/'pMPR', 'pMPR:Sign in Blood:sign in blood').
card_rarity('sign in blood'/'pMPR', 'Special').
card_artist('sign in blood'/'pMPR', 'Alan Pollack').
card_number('sign in blood'/'pMPR', '42').

card_in_set('terminate', 'pMPR').
card_original_type('terminate'/'pMPR', 'Instant').
card_original_text('terminate'/'pMPR', '').
card_image_name('terminate'/'pMPR', 'terminate').
card_uid('terminate'/'pMPR', 'pMPR:Terminate:terminate').
card_rarity('terminate'/'pMPR', 'Special').
card_artist('terminate'/'pMPR', 'Thomas M. Baxa').
card_number('terminate'/'pMPR', '39').

card_in_set('terror', 'pMPR').
card_original_type('terror'/'pMPR', 'Instant').
card_original_text('terror'/'pMPR', '').
card_image_name('terror'/'pMPR', 'terror').
card_uid('terror'/'pMPR', 'pMPR:Terror:terror').
card_rarity('terror'/'pMPR', 'Special').
card_artist('terror'/'pMPR', 'Ron Spencer').
card_number('terror'/'pMPR', '5').

card_in_set('tidings', 'pMPR').
card_original_type('tidings'/'pMPR', 'Sorcery').
card_original_text('tidings'/'pMPR', '').
card_image_name('tidings'/'pMPR', 'tidings').
card_uid('tidings'/'pMPR', 'pMPR:Tidings:tidings').
card_rarity('tidings'/'pMPR', 'Special').
card_artist('tidings'/'pMPR', 'Michael Komarck').
card_number('tidings'/'pMPR', '25').

card_in_set('treasure hunt', 'pMPR').
card_original_type('treasure hunt'/'pMPR', 'Sorcery').
card_original_text('treasure hunt'/'pMPR', '').
card_image_name('treasure hunt'/'pMPR', 'treasure hunt').
card_uid('treasure hunt'/'pMPR', 'pMPR:Treasure Hunt:treasure hunt').
card_rarity('treasure hunt'/'pMPR', 'Special').
card_artist('treasure hunt'/'pMPR', 'Steve Prescott').
card_number('treasure hunt'/'pMPR', '52').

card_in_set('unmake', 'pMPR').
card_original_type('unmake'/'pMPR', 'Instant').
card_original_text('unmake'/'pMPR', '').
card_first_print('unmake', 'pMPR').
card_image_name('unmake'/'pMPR', 'unmake').
card_uid('unmake'/'pMPR', 'pMPR:Unmake:unmake').
card_rarity('unmake'/'pMPR', 'Special').
card_artist('unmake'/'pMPR', 'Chris Rahn').
card_number('unmake'/'pMPR', '33').

card_in_set('voidmage prodigy', 'pMPR').
card_original_type('voidmage prodigy'/'pMPR', 'Creature — Human Wizard').
card_original_text('voidmage prodigy'/'pMPR', '').
card_first_print('voidmage prodigy', 'pMPR').
card_image_name('voidmage prodigy'/'pMPR', 'voidmage prodigy').
card_uid('voidmage prodigy'/'pMPR', 'pMPR:Voidmage Prodigy:voidmage prodigy').
card_rarity('voidmage prodigy'/'pMPR', 'Special').
card_artist('voidmage prodigy'/'pMPR', 'Christopher Moeller').
card_number('voidmage prodigy'/'pMPR', '2').

card_in_set('volcanic fallout', 'pMPR').
card_original_type('volcanic fallout'/'pMPR', 'Instant').
card_original_text('volcanic fallout'/'pMPR', '').
card_first_print('volcanic fallout', 'pMPR').
card_image_name('volcanic fallout'/'pMPR', 'volcanic fallout').
card_uid('volcanic fallout'/'pMPR', 'pMPR:Volcanic Fallout:volcanic fallout').
card_rarity('volcanic fallout'/'pMPR', 'Special').
card_artist('volcanic fallout'/'pMPR', 'Jim Pavelec').
card_number('volcanic fallout'/'pMPR', '44').

card_in_set('wasteland', 'pMPR').
card_original_type('wasteland'/'pMPR', 'Land').
card_original_text('wasteland'/'pMPR', '').
card_image_name('wasteland'/'pMPR', 'wasteland').
card_uid('wasteland'/'pMPR', 'pMPR:Wasteland:wasteland').
card_rarity('wasteland'/'pMPR', 'Special').
card_artist('wasteland'/'pMPR', 'Una Fricker').
card_number('wasteland'/'pMPR', '1').
card_flavor_text('wasteland'/'pMPR', '\"I will show you fear in a handful of dust.\" - T. S. Eliot, \"The Waste Land\"').

card_in_set('wrath of god', 'pMPR').
card_original_type('wrath of god'/'pMPR', 'Sorcery').
card_original_text('wrath of god'/'pMPR', '').
card_image_name('wrath of god'/'pMPR', 'wrath of god').
card_uid('wrath of god'/'pMPR', 'pMPR:Wrath of God:wrath of god').
card_rarity('wrath of god'/'pMPR', 'Special').
card_artist('wrath of god'/'pMPR', 'Ron Spencer').
card_number('wrath of god'/'pMPR', '17').

card_in_set('zombify', 'pMPR').
card_original_type('zombify'/'pMPR', 'Sorcery').
card_original_text('zombify'/'pMPR', '').
card_first_print('zombify', 'pMPR').
card_image_name('zombify'/'pMPR', 'zombify').
card_uid('zombify'/'pMPR', 'pMPR:Zombify:zombify').
card_rarity('zombify'/'pMPR', 'Special').
card_artist('zombify'/'pMPR', 'Allan Pollack').
card_number('zombify'/'pMPR', '15').
