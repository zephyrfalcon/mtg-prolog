% New Phyrexia

set('NPH').
set_name('NPH', 'New Phyrexia').
set_release_date('NPH', '2011-05-13').
set_border('NPH', 'black').
set_type('NPH', 'expansion').
set_block('NPH', 'Scars of Mirrodin').

card_in_set('act of aggression', 'NPH').
card_original_type('act of aggression'/'NPH', 'Instant').
card_original_text('act of aggression'/'NPH', '({R/P} can be paid with either {R} or 2 life.)\nGain control of target creature an opponent controls until end of turn. Untap that creature. It gains haste until end of turn.').
card_first_print('act of aggression', 'NPH').
card_image_name('act of aggression'/'NPH', 'act of aggression').
card_uid('act of aggression'/'NPH', 'NPH:Act of Aggression:act of aggression').
card_rarity('act of aggression'/'NPH', 'Uncommon').
card_artist('act of aggression'/'NPH', 'Whit Brachna').
card_number('act of aggression'/'NPH', '78').
card_multiverse_id('act of aggression'/'NPH', '230076').
card_watermark('act of aggression'/'NPH', 'Phyrexian').

card_in_set('alloy myr', 'NPH').
card_original_type('alloy myr'/'NPH', 'Artifact Creature — Myr').
card_original_text('alloy myr'/'NPH', '{T}: Add one mana of any color to your mana pool.').
card_first_print('alloy myr', 'NPH').
card_image_name('alloy myr'/'NPH', 'alloy myr').
card_uid('alloy myr'/'NPH', 'NPH:Alloy Myr:alloy myr').
card_rarity('alloy myr'/'NPH', 'Uncommon').
card_artist('alloy myr'/'NPH', 'Matt Cavotta').
card_number('alloy myr'/'NPH', '129').
card_flavor_text('alloy myr'/'NPH', 'With or without witnesses, the suns continued their prismatic dance.').
card_multiverse_id('alloy myr'/'NPH', '214344').
card_watermark('alloy myr'/'NPH', 'Mirran').

card_in_set('apostle\'s blessing', 'NPH').
card_original_type('apostle\'s blessing'/'NPH', 'Instant').
card_original_text('apostle\'s blessing'/'NPH', '({W/P} can be paid with either {W} or 2 life.)\nTarget artifact or creature you control gains protection from artifacts or from the color of your choice until end of turn.').
card_first_print('apostle\'s blessing', 'NPH').
card_image_name('apostle\'s blessing'/'NPH', 'apostle\'s blessing').
card_uid('apostle\'s blessing'/'NPH', 'NPH:Apostle\'s Blessing:apostle\'s blessing').
card_rarity('apostle\'s blessing'/'NPH', 'Common').
card_artist('apostle\'s blessing'/'NPH', 'Brad Rigney').
card_number('apostle\'s blessing'/'NPH', '2').
card_multiverse_id('apostle\'s blessing'/'NPH', '194208').
card_watermark('apostle\'s blessing'/'NPH', 'Phyrexian').

card_in_set('argent mutation', 'NPH').
card_original_type('argent mutation'/'NPH', 'Instant').
card_original_text('argent mutation'/'NPH', 'Target permanent becomes an artifact in addition to its other types until end of turn.\nDraw a card.').
card_first_print('argent mutation', 'NPH').
card_image_name('argent mutation'/'NPH', 'argent mutation').
card_uid('argent mutation'/'NPH', 'NPH:Argent Mutation:argent mutation').
card_rarity('argent mutation'/'NPH', 'Uncommon').
card_artist('argent mutation'/'NPH', 'Jana Schirmer & Johannes Voss').
card_number('argent mutation'/'NPH', '27').
card_flavor_text('argent mutation'/'NPH', '\"My world is corrupted beyond cleansing. I must prevent this from happening to other worlds.\"\n—Karn').
card_multiverse_id('argent mutation'/'NPH', '218001').
card_watermark('argent mutation'/'NPH', 'Mirran').

card_in_set('arm with æther', 'NPH').
card_original_type('arm with æther'/'NPH', 'Sorcery').
card_original_text('arm with æther'/'NPH', 'Until end of turn, creatures you control gain \"Whenever this creature deals damage to an opponent, you may return target creature that player controls to its owner\'s hand.\"').
card_first_print('arm with æther', 'NPH').
card_image_name('arm with æther'/'NPH', 'arm with aether').
card_uid('arm with æther'/'NPH', 'NPH:Arm with Æther:arm with aether').
card_rarity('arm with æther'/'NPH', 'Uncommon').
card_artist('arm with æther'/'NPH', 'Austin Hsu').
card_number('arm with æther'/'NPH', '28').
card_flavor_text('arm with æther'/'NPH', '\"Anything we can do to hurt the Phyrexian invaders is worth dying for.\"').
card_multiverse_id('arm with æther'/'NPH', '202641').
card_watermark('arm with æther'/'NPH', 'Mirran').

card_in_set('artillerize', 'NPH').
card_original_type('artillerize'/'NPH', 'Instant').
card_original_text('artillerize'/'NPH', 'As an additional cost to cast Artillerize, sacrifice an artifact or creature.\nArtillerize deals 5 damage to target creature or player.').
card_first_print('artillerize', 'NPH').
card_image_name('artillerize'/'NPH', 'artillerize').
card_uid('artillerize'/'NPH', 'NPH:Artillerize:artillerize').
card_rarity('artillerize'/'NPH', 'Common').
card_artist('artillerize'/'NPH', 'Johann Bodin').
card_number('artillerize'/'NPH', '79').
card_flavor_text('artillerize'/'NPH', 'Phyrexian or Mirran, most goblins stick to the job they know best.').
card_multiverse_id('artillerize'/'NPH', '218081').
card_watermark('artillerize'/'NPH', 'Phyrexian').

card_in_set('auriok survivors', 'NPH').
card_original_type('auriok survivors'/'NPH', 'Creature — Human Soldier').
card_original_text('auriok survivors'/'NPH', 'When Auriok Survivors enters the battlefield, you may return target Equipment card from your graveyard to the battlefield. If you do, you may attach it to Auriok Survivors.').
card_first_print('auriok survivors', 'NPH').
card_image_name('auriok survivors'/'NPH', 'auriok survivors').
card_uid('auriok survivors'/'NPH', 'NPH:Auriok Survivors:auriok survivors').
card_rarity('auriok survivors'/'NPH', 'Uncommon').
card_artist('auriok survivors'/'NPH', 'James Ryman').
card_number('auriok survivors'/'NPH', '3').
card_flavor_text('auriok survivors'/'NPH', '\"We are Mirrodin\'s children. We will reclaim her—one scrap at a time, if we have to.\"').
card_multiverse_id('auriok survivors'/'NPH', '214353').
card_watermark('auriok survivors'/'NPH', 'Mirran').

card_in_set('batterskull', 'NPH').
card_original_type('batterskull'/'NPH', 'Artifact — Equipment').
card_original_text('batterskull'/'NPH', 'Living weapon (When this Equipment enters the battlefield, put a 0/0 black Germ creature token onto the battlefield, then attach this to it.)\nEquipped creature gets +4/+4 and has vigilance and lifelink.\n{3}: Return Batterskull to its owner\'s hand.\nEquip {5}').
card_image_name('batterskull'/'NPH', 'batterskull').
card_uid('batterskull'/'NPH', 'NPH:Batterskull:batterskull').
card_rarity('batterskull'/'NPH', 'Mythic Rare').
card_artist('batterskull'/'NPH', 'Mark Zug').
card_number('batterskull'/'NPH', '130').
card_multiverse_id('batterskull'/'NPH', '233055').
card_watermark('batterskull'/'NPH', 'Phyrexian').

card_in_set('beast within', 'NPH').
card_original_type('beast within'/'NPH', 'Instant').
card_original_text('beast within'/'NPH', 'Destroy target permanent. Its controller puts a 3/3 green Beast creature token onto the battlefield.').
card_first_print('beast within', 'NPH').
card_image_name('beast within'/'NPH', 'beast within').
card_uid('beast within'/'NPH', 'NPH:Beast Within:beast within').
card_rarity('beast within'/'NPH', 'Uncommon').
card_artist('beast within'/'NPH', 'Dave Allsop').
card_number('beast within'/'NPH', '103').
card_flavor_text('beast within'/'NPH', '\"Kill the weak so they can\'t drag the strong down to their level. This is true compassion.\"\n—Benzir, archdruid of Temple Might').
card_multiverse_id('beast within'/'NPH', '221533').
card_watermark('beast within'/'NPH', 'Phyrexian').

card_in_set('birthing pod', 'NPH').
card_original_type('birthing pod'/'NPH', 'Artifact').
card_original_text('birthing pod'/'NPH', '({G/P} can be paid with either {G} or 2 life.)\n{1}{G/P}, {T}, Sacrifice a creature: Search your library for a creature card with converted mana cost equal to 1 plus the sacrificed creature\'s converted mana cost, put that card onto the battlefield, then shuffle your library. Activate this ability only any time you could cast a sorcery.').
card_first_print('birthing pod', 'NPH').
card_image_name('birthing pod'/'NPH', 'birthing pod').
card_uid('birthing pod'/'NPH', 'NPH:Birthing Pod:birthing pod').
card_rarity('birthing pod'/'NPH', 'Rare').
card_artist('birthing pod'/'NPH', 'Daarken').
card_number('birthing pod'/'NPH', '104').
card_multiverse_id('birthing pod'/'NPH', '218006').
card_watermark('birthing pod'/'NPH', 'Phyrexian').

card_in_set('blade splicer', 'NPH').
card_original_type('blade splicer'/'NPH', 'Creature — Human Artificer').
card_original_text('blade splicer'/'NPH', 'When Blade Splicer enters the battlefield, put a 3/3 colorless Golem artifact creature token onto the battlefield.\nGolem creatures you control have first strike.').
card_first_print('blade splicer', 'NPH').
card_image_name('blade splicer'/'NPH', 'blade splicer').
card_uid('blade splicer'/'NPH', 'NPH:Blade Splicer:blade splicer').
card_rarity('blade splicer'/'NPH', 'Rare').
card_artist('blade splicer'/'NPH', 'Greg Staples').
card_number('blade splicer'/'NPH', '4').
card_multiverse_id('blade splicer'/'NPH', '233068').
card_watermark('blade splicer'/'NPH', 'Phyrexian').

card_in_set('blighted agent', 'NPH').
card_original_type('blighted agent'/'NPH', 'Creature — Human Rogue').
card_original_text('blighted agent'/'NPH', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nBlighted Agent is unblockable.').
card_first_print('blighted agent', 'NPH').
card_image_name('blighted agent'/'NPH', 'blighted agent').
card_uid('blighted agent'/'NPH', 'NPH:Blighted Agent:blighted agent').
card_rarity('blighted agent'/'NPH', 'Common').
card_artist('blighted agent'/'NPH', 'Anthony Francisco').
card_number('blighted agent'/'NPH', '29').
card_flavor_text('blighted agent'/'NPH', '\"Urabrask may suspect our surveillance, but he cannot stop it.\"\n—Avaricta, Gitaxian sective').
card_multiverse_id('blighted agent'/'NPH', '214383').
card_watermark('blighted agent'/'NPH', 'Phyrexian').

card_in_set('blind zealot', 'NPH').
card_original_type('blind zealot'/'NPH', 'Creature — Human Cleric').
card_original_text('blind zealot'/'NPH', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)\nWhenever Blind Zealot deals combat damage to a player, you may sacrifice it. If you do, destroy target creature that player controls.').
card_first_print('blind zealot', 'NPH').
card_image_name('blind zealot'/'NPH', 'blind zealot').
card_uid('blind zealot'/'NPH', 'NPH:Blind Zealot:blind zealot').
card_rarity('blind zealot'/'NPH', 'Common').
card_artist('blind zealot'/'NPH', 'Jana Schirmer & Johannes Voss').
card_number('blind zealot'/'NPH', '52').
card_multiverse_id('blind zealot'/'NPH', '217999').
card_watermark('blind zealot'/'NPH', 'Phyrexian').

card_in_set('blinding souleater', 'NPH').
card_original_type('blinding souleater'/'NPH', 'Artifact Creature — Cleric').
card_original_text('blinding souleater'/'NPH', '{W/P}, {T}: Tap target creature. ({W/P} can be paid with either {W} or 2 life.)').
card_first_print('blinding souleater', 'NPH').
card_image_name('blinding souleater'/'NPH', 'blinding souleater').
card_uid('blinding souleater'/'NPH', 'NPH:Blinding Souleater:blinding souleater').
card_rarity('blinding souleater'/'NPH', 'Common').
card_artist('blinding souleater'/'NPH', 'Igor Kieryluk').
card_number('blinding souleater'/'NPH', '131').
card_flavor_text('blinding souleater'/'NPH', '\"We thank the souleaters for inscribing our souls with subservience, to reinforce the sacred order.\"\n—Drones\' hymn of gratitude').
card_multiverse_id('blinding souleater'/'NPH', '233045').
card_watermark('blinding souleater'/'NPH', 'Phyrexian').

card_in_set('bludgeon brawl', 'NPH').
card_original_type('bludgeon brawl'/'NPH', 'Enchantment').
card_original_text('bludgeon brawl'/'NPH', 'Each noncreature, non-Equipment artifact is an Equipment with equip {X} and \"Equipped creature gets +X/+0,\" where X is that artifact\'s converted mana cost.').
card_first_print('bludgeon brawl', 'NPH').
card_image_name('bludgeon brawl'/'NPH', 'bludgeon brawl').
card_uid('bludgeon brawl'/'NPH', 'NPH:Bludgeon Brawl:bludgeon brawl').
card_rarity('bludgeon brawl'/'NPH', 'Rare').
card_artist('bludgeon brawl'/'NPH', 'Kev Walker').
card_number('bludgeon brawl'/'NPH', '80').
card_multiverse_id('bludgeon brawl'/'NPH', '218055').
card_watermark('bludgeon brawl'/'NPH', 'Mirran').

card_in_set('brutalizer exarch', 'NPH').
card_original_type('brutalizer exarch'/'NPH', 'Creature — Cleric').
card_original_text('brutalizer exarch'/'NPH', 'When Brutalizer Exarch enters the battlefield, choose one — Search your library for a creature card, reveal it, then shuffle your library and put that card on top of it; or put target noncreature permanent on the bottom of its owner\'s library.').
card_first_print('brutalizer exarch', 'NPH').
card_image_name('brutalizer exarch'/'NPH', 'brutalizer exarch').
card_uid('brutalizer exarch'/'NPH', 'NPH:Brutalizer Exarch:brutalizer exarch').
card_rarity('brutalizer exarch'/'NPH', 'Uncommon').
card_artist('brutalizer exarch'/'NPH', 'Mark Zug').
card_number('brutalizer exarch'/'NPH', '105').
card_multiverse_id('brutalizer exarch'/'NPH', '217998').
card_watermark('brutalizer exarch'/'NPH', 'Phyrexian').

card_in_set('caged sun', 'NPH').
card_original_type('caged sun'/'NPH', 'Artifact').
card_original_text('caged sun'/'NPH', 'As Caged Sun enters the battlefield, choose a color.\nCreatures you control of the chosen color get +1/+1.\nWhenever a land\'s ability adds one or more mana of the chosen color to your mana pool, add one additional mana of that color to your mana pool.').
card_first_print('caged sun', 'NPH').
card_image_name('caged sun'/'NPH', 'caged sun').
card_uid('caged sun'/'NPH', 'NPH:Caged Sun:caged sun').
card_rarity('caged sun'/'NPH', 'Rare').
card_artist('caged sun'/'NPH', 'Scott Chou').
card_number('caged sun'/'NPH', '132').
card_multiverse_id('caged sun'/'NPH', '214345').
card_watermark('caged sun'/'NPH', 'Mirran').

card_in_set('caress of phyrexia', 'NPH').
card_original_type('caress of phyrexia'/'NPH', 'Sorcery').
card_original_text('caress of phyrexia'/'NPH', 'Target player draws three cards, loses 3 life, and gets three poison counters.').
card_first_print('caress of phyrexia', 'NPH').
card_image_name('caress of phyrexia'/'NPH', 'caress of phyrexia').
card_uid('caress of phyrexia'/'NPH', 'NPH:Caress of Phyrexia:caress of phyrexia').
card_rarity('caress of phyrexia'/'NPH', 'Uncommon').
card_artist('caress of phyrexia'/'NPH', 'Karl Kopinski').
card_number('caress of phyrexia'/'NPH', '53').
card_flavor_text('caress of phyrexia'/'NPH', '\"I am weak. Make . . . me . . . ideal.\"').
card_multiverse_id('caress of phyrexia'/'NPH', '218061').
card_watermark('caress of phyrexia'/'NPH', 'Phyrexian').

card_in_set('cathedral membrane', 'NPH').
card_original_type('cathedral membrane'/'NPH', 'Artifact Creature — Wall').
card_original_text('cathedral membrane'/'NPH', '({W/P} can be paid with either {W} or 2 life.)\nDefender\nWhen Cathedral Membrane is put into a graveyard from the battlefield during combat, it deals 6 damage to each creature it blocked this combat.').
card_first_print('cathedral membrane', 'NPH').
card_image_name('cathedral membrane'/'NPH', 'cathedral membrane').
card_uid('cathedral membrane'/'NPH', 'NPH:Cathedral Membrane:cathedral membrane').
card_rarity('cathedral membrane'/'NPH', 'Uncommon').
card_artist('cathedral membrane'/'NPH', 'Richard Whitters').
card_number('cathedral membrane'/'NPH', '5').
card_multiverse_id('cathedral membrane'/'NPH', '218024').
card_watermark('cathedral membrane'/'NPH', 'Phyrexian').

card_in_set('chained throatseeker', 'NPH').
card_original_type('chained throatseeker'/'NPH', 'Creature — Horror').
card_original_text('chained throatseeker'/'NPH', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nChained Throatseeker can\'t attack unless defending player is poisoned.').
card_first_print('chained throatseeker', 'NPH').
card_image_name('chained throatseeker'/'NPH', 'chained throatseeker').
card_uid('chained throatseeker'/'NPH', 'NPH:Chained Throatseeker:chained throatseeker').
card_rarity('chained throatseeker'/'NPH', 'Common').
card_artist('chained throatseeker'/'NPH', 'Stephan Martiniere').
card_number('chained throatseeker'/'NPH', '30').
card_multiverse_id('chained throatseeker'/'NPH', '218075').
card_watermark('chained throatseeker'/'NPH', 'Phyrexian').

card_in_set('chancellor of the annex', 'NPH').
card_original_type('chancellor of the annex'/'NPH', 'Creature — Angel').
card_original_text('chancellor of the annex'/'NPH', 'You may reveal this card from your opening hand. If you do, when each opponent casts his or her first spell of the game, counter that spell unless that player pays {1}.\nFlying\nWhenever an opponent casts a spell, counter it unless that player pays {1}.').
card_first_print('chancellor of the annex', 'NPH').
card_image_name('chancellor of the annex'/'NPH', 'chancellor of the annex').
card_uid('chancellor of the annex'/'NPH', 'NPH:Chancellor of the Annex:chancellor of the annex').
card_rarity('chancellor of the annex'/'NPH', 'Rare').
card_artist('chancellor of the annex'/'NPH', 'Min Yum').
card_number('chancellor of the annex'/'NPH', '6').
card_multiverse_id('chancellor of the annex'/'NPH', '218083').
card_watermark('chancellor of the annex'/'NPH', 'Phyrexian').

card_in_set('chancellor of the dross', 'NPH').
card_original_type('chancellor of the dross'/'NPH', 'Creature — Vampire').
card_original_text('chancellor of the dross'/'NPH', 'You may reveal this card from your opening hand. If you do, at the beginning of the first upkeep, each opponent loses 3 life, then you gain life equal to the life lost this way.\nFlying, lifelink').
card_first_print('chancellor of the dross', 'NPH').
card_image_name('chancellor of the dross'/'NPH', 'chancellor of the dross').
card_uid('chancellor of the dross'/'NPH', 'NPH:Chancellor of the Dross:chancellor of the dross').
card_rarity('chancellor of the dross'/'NPH', 'Rare').
card_artist('chancellor of the dross'/'NPH', 'Stephan Martiniere').
card_number('chancellor of the dross'/'NPH', '54').
card_multiverse_id('chancellor of the dross'/'NPH', '218020').
card_watermark('chancellor of the dross'/'NPH', 'Phyrexian').

card_in_set('chancellor of the forge', 'NPH').
card_original_type('chancellor of the forge'/'NPH', 'Creature — Giant').
card_original_text('chancellor of the forge'/'NPH', 'You may reveal this card from your opening hand. If you do, at the beginning of the first upkeep, put a 1/1 red Goblin creature token with haste onto the battlefield.\nWhen Chancellor of the Forge enters the battlefield, put X 1/1 red Goblin creature tokens with haste onto the battlefield, where X is the number of creatures you control.').
card_first_print('chancellor of the forge', 'NPH').
card_image_name('chancellor of the forge'/'NPH', 'chancellor of the forge').
card_uid('chancellor of the forge'/'NPH', 'NPH:Chancellor of the Forge:chancellor of the forge').
card_rarity('chancellor of the forge'/'NPH', 'Rare').
card_artist('chancellor of the forge'/'NPH', 'Chippy').
card_number('chancellor of the forge'/'NPH', '81').
card_multiverse_id('chancellor of the forge'/'NPH', '218021').
card_watermark('chancellor of the forge'/'NPH', 'Phyrexian').

card_in_set('chancellor of the spires', 'NPH').
card_original_type('chancellor of the spires'/'NPH', 'Creature — Sphinx').
card_original_text('chancellor of the spires'/'NPH', 'You may reveal this card from your opening hand. If you do, at the beginning of the first upkeep, each opponent puts the top seven cards of his or her library into his or her graveyard.\nFlying\nWhen Chancellor of the Spires enters the battlefield, you may cast target instant or sorcery card from an opponent\'s graveyard without paying its mana cost.').
card_first_print('chancellor of the spires', 'NPH').
card_image_name('chancellor of the spires'/'NPH', 'chancellor of the spires').
card_uid('chancellor of the spires'/'NPH', 'NPH:Chancellor of the Spires:chancellor of the spires').
card_rarity('chancellor of the spires'/'NPH', 'Rare').
card_artist('chancellor of the spires'/'NPH', 'Nils Hamm').
card_number('chancellor of the spires'/'NPH', '31').
card_multiverse_id('chancellor of the spires'/'NPH', '214360').
card_watermark('chancellor of the spires'/'NPH', 'Phyrexian').

card_in_set('chancellor of the tangle', 'NPH').
card_original_type('chancellor of the tangle'/'NPH', 'Creature — Beast').
card_original_text('chancellor of the tangle'/'NPH', 'You may reveal this card from your opening hand. If you do, at the beginning of your first main phase, add {G} to your mana pool.\nVigilance, reach').
card_first_print('chancellor of the tangle', 'NPH').
card_image_name('chancellor of the tangle'/'NPH', 'chancellor of the tangle').
card_uid('chancellor of the tangle'/'NPH', 'NPH:Chancellor of the Tangle:chancellor of the tangle').
card_rarity('chancellor of the tangle'/'NPH', 'Rare').
card_artist('chancellor of the tangle'/'NPH', 'Steve Prescott').
card_number('chancellor of the tangle'/'NPH', '106').
card_multiverse_id('chancellor of the tangle'/'NPH', '218062').
card_watermark('chancellor of the tangle'/'NPH', 'Phyrexian').

card_in_set('conversion chamber', 'NPH').
card_original_type('conversion chamber'/'NPH', 'Artifact').
card_original_text('conversion chamber'/'NPH', '{2}, {T}: Exile target artifact card from a graveyard. Put a charge counter on Conversion Chamber.\n{2}, {T}, Remove a charge counter from Conversion Chamber: Put a 3/3 colorless Golem artifact creature token onto the battlefield.').
card_first_print('conversion chamber', 'NPH').
card_image_name('conversion chamber'/'NPH', 'conversion chamber').
card_uid('conversion chamber'/'NPH', 'NPH:Conversion Chamber:conversion chamber').
card_rarity('conversion chamber'/'NPH', 'Uncommon').
card_artist('conversion chamber'/'NPH', 'Anthony Francisco').
card_number('conversion chamber'/'NPH', '133').
card_multiverse_id('conversion chamber'/'NPH', '230070').
card_watermark('conversion chamber'/'NPH', 'Phyrexian').

card_in_set('corrosive gale', 'NPH').
card_original_type('corrosive gale'/'NPH', 'Sorcery').
card_original_text('corrosive gale'/'NPH', '({G/P} can be paid with either {G} or 2 life.)\nCorrosive Gale deals X damage to each creature with flying.').
card_first_print('corrosive gale', 'NPH').
card_image_name('corrosive gale'/'NPH', 'corrosive gale').
card_uid('corrosive gale'/'NPH', 'NPH:Corrosive Gale:corrosive gale').
card_rarity('corrosive gale'/'NPH', 'Uncommon').
card_artist('corrosive gale'/'NPH', 'Dan Scott').
card_number('corrosive gale'/'NPH', '107').
card_flavor_text('corrosive gale'/'NPH', '\"Wipe Sheoldred\'s spies from the sky. She\'ll see the result of our vision soon enough.\"\n—Glissa').
card_multiverse_id('corrosive gale'/'NPH', '230075').
card_watermark('corrosive gale'/'NPH', 'Phyrexian').

card_in_set('corrupted resolve', 'NPH').
card_original_type('corrupted resolve'/'NPH', 'Instant').
card_original_text('corrupted resolve'/'NPH', 'Counter target spell if its controller is poisoned.').
card_first_print('corrupted resolve', 'NPH').
card_image_name('corrupted resolve'/'NPH', 'corrupted resolve').
card_uid('corrupted resolve'/'NPH', 'NPH:Corrupted Resolve:corrupted resolve').
card_rarity('corrupted resolve'/'NPH', 'Uncommon').
card_artist('corrupted resolve'/'NPH', 'Greg Staples').
card_number('corrupted resolve'/'NPH', '32').
card_flavor_text('corrupted resolve'/'NPH', '\"Flesh is a sickness that infects Phyrexia.\"\n—Avaricta, Gitaxian sective').
card_multiverse_id('corrupted resolve'/'NPH', '233080').
card_watermark('corrupted resolve'/'NPH', 'Phyrexian').

card_in_set('darksteel relic', 'NPH').
card_original_type('darksteel relic'/'NPH', 'Artifact').
card_original_text('darksteel relic'/'NPH', 'Darksteel Relic is indestructible. (Effects that say \"destroy\" don\'t destroy it.)').
card_first_print('darksteel relic', 'NPH').
card_image_name('darksteel relic'/'NPH', 'darksteel relic').
card_uid('darksteel relic'/'NPH', 'NPH:Darksteel Relic:darksteel relic').
card_rarity('darksteel relic'/'NPH', 'Uncommon').
card_artist('darksteel relic'/'NPH', 'Daniel Ljunggren').
card_number('darksteel relic'/'NPH', '134').
card_flavor_text('darksteel relic'/'NPH', '\"It\'s the last thing we can call our own.\"\n—Minhu, Mirran resistance').
card_multiverse_id('darksteel relic'/'NPH', '218067').
card_watermark('darksteel relic'/'NPH', 'Mirran').

card_in_set('death-hood cobra', 'NPH').
card_original_type('death-hood cobra'/'NPH', 'Creature — Snake').
card_original_text('death-hood cobra'/'NPH', '{1}{G}: Death-Hood Cobra gains reach until end of turn. (It can block creatures with flying.)\n{1}{G}: Death-Hood Cobra gains deathtouch until end of turn. (Any amount of damage it deals to a creature is enough to destroy it.)').
card_first_print('death-hood cobra', 'NPH').
card_image_name('death-hood cobra'/'NPH', 'death-hood cobra').
card_uid('death-hood cobra'/'NPH', 'NPH:Death-Hood Cobra:death-hood cobra').
card_rarity('death-hood cobra'/'NPH', 'Common').
card_artist('death-hood cobra'/'NPH', 'Jason Felix').
card_number('death-hood cobra'/'NPH', '108').
card_multiverse_id('death-hood cobra'/'NPH', '233065').
card_watermark('death-hood cobra'/'NPH', 'Phyrexian').

card_in_set('deceiver exarch', 'NPH').
card_original_type('deceiver exarch'/'NPH', 'Creature — Cleric').
card_original_text('deceiver exarch'/'NPH', 'Flash (You may cast this spell any time you could cast an instant.)\nWhen Deceiver Exarch enters the battlefield, choose one — Untap target permanent you control; or tap target permanent an opponent controls.').
card_first_print('deceiver exarch', 'NPH').
card_image_name('deceiver exarch'/'NPH', 'deceiver exarch').
card_uid('deceiver exarch'/'NPH', 'NPH:Deceiver Exarch:deceiver exarch').
card_rarity('deceiver exarch'/'NPH', 'Uncommon').
card_artist('deceiver exarch'/'NPH', 'Izzy').
card_number('deceiver exarch'/'NPH', '33').
card_multiverse_id('deceiver exarch'/'NPH', '214365').
card_watermark('deceiver exarch'/'NPH', 'Phyrexian').

card_in_set('defensive stance', 'NPH').
card_original_type('defensive stance'/'NPH', 'Enchantment — Aura').
card_original_text('defensive stance'/'NPH', 'Enchant creature\nEnchanted creature gets -1/+1.').
card_first_print('defensive stance', 'NPH').
card_image_name('defensive stance'/'NPH', 'defensive stance').
card_uid('defensive stance'/'NPH', 'NPH:Defensive Stance:defensive stance').
card_rarity('defensive stance'/'NPH', 'Common').
card_artist('defensive stance'/'NPH', 'Dan Scott').
card_number('defensive stance'/'NPH', '34').
card_flavor_text('defensive stance'/'NPH', '\"It isn\'t for fighting. It\'s for when the fighting\'s gone bad.\"\n—Kara Vrist, Mirran resistance').
card_multiverse_id('defensive stance'/'NPH', '217987').
card_watermark('defensive stance'/'NPH', 'Mirran').

card_in_set('dementia bat', 'NPH').
card_original_type('dementia bat'/'NPH', 'Creature — Bat').
card_original_text('dementia bat'/'NPH', 'Flying\n{4}{B}, Sacrifice Dementia Bat: Target player discards two cards.').
card_first_print('dementia bat', 'NPH').
card_image_name('dementia bat'/'NPH', 'dementia bat').
card_uid('dementia bat'/'NPH', 'NPH:Dementia Bat:dementia bat').
card_rarity('dementia bat'/'NPH', 'Common').
card_artist('dementia bat'/'NPH', 'Daarken').
card_number('dementia bat'/'NPH', '55').
card_flavor_text('dementia bat'/'NPH', '\"When terror outweighs all other thoughts, they will understand that I am the true Father of Machines.\"\n—Azax-Azog, the Demon Thane').
card_multiverse_id('dementia bat'/'NPH', '233084').
card_watermark('dementia bat'/'NPH', 'Phyrexian').

card_in_set('despise', 'NPH').
card_original_type('despise'/'NPH', 'Sorcery').
card_original_text('despise'/'NPH', 'Target opponent reveals his or her hand. You choose a creature or planeswalker card from it. That player discards that card.').
card_image_name('despise'/'NPH', 'despise').
card_uid('despise'/'NPH', 'NPH:Despise:despise').
card_rarity('despise'/'NPH', 'Uncommon').
card_artist('despise'/'NPH', 'Terese Nielsen').
card_number('despise'/'NPH', '56').
card_flavor_text('despise'/'NPH', '\"Truth is always a weapon in your enemies\' hands.\"\n—Geth, Lord of the Vault').
card_multiverse_id('despise'/'NPH', '233043').
card_watermark('despise'/'NPH', 'Phyrexian').

card_in_set('dismember', 'NPH').
card_original_type('dismember'/'NPH', 'Instant').
card_original_text('dismember'/'NPH', '({B/P} can be paid with either {B} or 2 life.)\nTarget creature gets -5/-5 until end of turn.').
card_image_name('dismember'/'NPH', 'dismember').
card_uid('dismember'/'NPH', 'NPH:Dismember:dismember').
card_rarity('dismember'/'NPH', 'Uncommon').
card_artist('dismember'/'NPH', 'Terese Nielsen').
card_number('dismember'/'NPH', '57').
card_flavor_text('dismember'/'NPH', '\"You serve Phyrexia. Your pieces would better serve Phyrexia elsewhere.\"\n—Azax-Azog, the Demon Thane').
card_multiverse_id('dismember'/'NPH', '230082').
card_watermark('dismember'/'NPH', 'Phyrexian').

card_in_set('dispatch', 'NPH').
card_original_type('dispatch'/'NPH', 'Instant').
card_original_text('dispatch'/'NPH', 'Tap target creature.\nMetalcraft — If you control three or more artifacts, exile that creature.').
card_first_print('dispatch', 'NPH').
card_image_name('dispatch'/'NPH', 'dispatch').
card_uid('dispatch'/'NPH', 'NPH:Dispatch:dispatch').
card_rarity('dispatch'/'NPH', 'Uncommon').
card_artist('dispatch'/'NPH', 'Erica Yang').
card_number('dispatch'/'NPH', '7').
card_flavor_text('dispatch'/'NPH', 'Venser wondered if it could still be called a teleportation spell if the destination is oblivion.').
card_multiverse_id('dispatch'/'NPH', '218072').
card_watermark('dispatch'/'NPH', 'Mirran').

card_in_set('due respect', 'NPH').
card_original_type('due respect'/'NPH', 'Instant').
card_original_text('due respect'/'NPH', 'Permanents enter the battlefield tapped this turn.\nDraw a card.').
card_first_print('due respect', 'NPH').
card_image_name('due respect'/'NPH', 'due respect').
card_uid('due respect'/'NPH', 'NPH:Due Respect:due respect').
card_rarity('due respect'/'NPH', 'Uncommon').
card_artist('due respect'/'NPH', 'James Ryman').
card_number('due respect'/'NPH', '8').
card_flavor_text('due respect'/'NPH', '\"A display of humility is required of those who still possess such frailties.\"\n—Elesh Norn, Grand Cenobite').
card_multiverse_id('due respect'/'NPH', '233077').
card_watermark('due respect'/'NPH', 'Phyrexian').

card_in_set('elesh norn, grand cenobite', 'NPH').
card_original_type('elesh norn, grand cenobite'/'NPH', 'Legendary Creature — Praetor').
card_original_text('elesh norn, grand cenobite'/'NPH', 'Vigilance\nOther creatures you control get +2/+2.\nCreatures your opponents control get -2/-2.').
card_image_name('elesh norn, grand cenobite'/'NPH', 'elesh norn, grand cenobite').
card_uid('elesh norn, grand cenobite'/'NPH', 'NPH:Elesh Norn, Grand Cenobite:elesh norn, grand cenobite').
card_rarity('elesh norn, grand cenobite'/'NPH', 'Mythic Rare').
card_artist('elesh norn, grand cenobite'/'NPH', 'Igor Kieryluk').
card_number('elesh norn, grand cenobite'/'NPH', '9').
card_flavor_text('elesh norn, grand cenobite'/'NPH', '\"The Gitaxians whisper among themselves of other worlds. If they exist, we must bring Phyrexia\'s magnificence to them.\"').
card_multiverse_id('elesh norn, grand cenobite'/'NPH', '214352').
card_watermark('elesh norn, grand cenobite'/'NPH', 'Phyrexian').

card_in_set('enslave', 'NPH').
card_original_type('enslave'/'NPH', 'Enchantment — Aura').
card_original_text('enslave'/'NPH', 'Enchant creature\nYou control enchanted creature.\nAt the beginning of your upkeep, enchanted creature deals 1 damage to its owner.').
card_image_name('enslave'/'NPH', 'enslave').
card_uid('enslave'/'NPH', 'NPH:Enslave:enslave').
card_rarity('enslave'/'NPH', 'Uncommon').
card_artist('enslave'/'NPH', 'Chris Rahn').
card_number('enslave'/'NPH', '58').
card_flavor_text('enslave'/'NPH', 'Phyrexia extended its command until there was nothing left to take.').
card_multiverse_id('enslave'/'NPH', '233060').
card_watermark('enslave'/'NPH', 'Phyrexian').

card_in_set('entomber exarch', 'NPH').
card_original_type('entomber exarch'/'NPH', 'Creature — Cleric').
card_original_text('entomber exarch'/'NPH', 'When Entomber Exarch enters the battlefield, choose one — Return target creature card from your graveyard to your hand; or target opponent reveals his or her hand, you choose a noncreature card from it, then that player discards that card.').
card_first_print('entomber exarch', 'NPH').
card_image_name('entomber exarch'/'NPH', 'entomber exarch').
card_uid('entomber exarch'/'NPH', 'NPH:Entomber Exarch:entomber exarch').
card_rarity('entomber exarch'/'NPH', 'Uncommon').
card_artist('entomber exarch'/'NPH', 'Svetlin Velinov').
card_number('entomber exarch'/'NPH', '59').
card_multiverse_id('entomber exarch'/'NPH', '218023').
card_watermark('entomber exarch'/'NPH', 'Phyrexian').

card_in_set('etched monstrosity', 'NPH').
card_original_type('etched monstrosity'/'NPH', 'Artifact Creature — Golem').
card_original_text('etched monstrosity'/'NPH', 'Etched Monstrosity enters the battlefield with five -1/-1 counters on it.\n{W}{U}{B}{R}{G}, Remove five -1/-1 counters from Etched Monstrosity: Target player draws three cards.').
card_first_print('etched monstrosity', 'NPH').
card_image_name('etched monstrosity'/'NPH', 'etched monstrosity').
card_uid('etched monstrosity'/'NPH', 'NPH:Etched Monstrosity:etched monstrosity').
card_rarity('etched monstrosity'/'NPH', 'Mythic Rare').
card_artist('etched monstrosity'/'NPH', 'Steven Belledin').
card_number('etched monstrosity'/'NPH', '135').
card_flavor_text('etched monstrosity'/'NPH', 'Now etched only with the scars of phyresis.').
card_multiverse_id('etched monstrosity'/'NPH', '217996').
card_watermark('etched monstrosity'/'NPH', 'Phyrexian').

card_in_set('evil presence', 'NPH').
card_original_type('evil presence'/'NPH', 'Enchantment — Aura').
card_original_text('evil presence'/'NPH', 'Enchant land\nEnchanted land is a Swamp.').
card_image_name('evil presence'/'NPH', 'evil presence').
card_uid('evil presence'/'NPH', 'NPH:Evil Presence:evil presence').
card_rarity('evil presence'/'NPH', 'Common').
card_artist('evil presence'/'NPH', 'Scott Chou').
card_number('evil presence'/'NPH', '60').
card_flavor_text('evil presence'/'NPH', '\"The Accorders would lament the transformation of their cherished lands, had they lived to see it.\"\n—Sheoldred, Whispering One').
card_multiverse_id('evil presence'/'NPH', '218073').
card_watermark('evil presence'/'NPH', 'Phyrexian').

card_in_set('exclusion ritual', 'NPH').
card_original_type('exclusion ritual'/'NPH', 'Enchantment').
card_original_text('exclusion ritual'/'NPH', 'Imprint — When Exclusion Ritual enters the battlefield, exile target nonland permanent.\nPlayers can\'t cast spells with the same name as the exiled card.').
card_first_print('exclusion ritual', 'NPH').
card_image_name('exclusion ritual'/'NPH', 'exclusion ritual').
card_uid('exclusion ritual'/'NPH', 'NPH:Exclusion Ritual:exclusion ritual').
card_rarity('exclusion ritual'/'NPH', 'Uncommon').
card_artist('exclusion ritual'/'NPH', 'Daniel Ljunggren').
card_number('exclusion ritual'/'NPH', '10').
card_flavor_text('exclusion ritual'/'NPH', '\"Your desires have become our discretion.\"\n—Izathel, priest of the Annex').
card_multiverse_id('exclusion ritual'/'NPH', '214364').
card_watermark('exclusion ritual'/'NPH', 'Phyrexian').

card_in_set('fallen ferromancer', 'NPH').
card_original_type('fallen ferromancer'/'NPH', 'Creature — Human Shaman').
card_original_text('fallen ferromancer'/'NPH', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\n{1}{R}, {T}: Fallen Ferromancer deals 1 damage to target creature or player.').
card_first_print('fallen ferromancer', 'NPH').
card_image_name('fallen ferromancer'/'NPH', 'fallen ferromancer').
card_uid('fallen ferromancer'/'NPH', 'NPH:Fallen Ferromancer:fallen ferromancer').
card_rarity('fallen ferromancer'/'NPH', 'Uncommon').
card_artist('fallen ferromancer'/'NPH', 'David Rapoza').
card_number('fallen ferromancer'/'NPH', '82').
card_multiverse_id('fallen ferromancer'/'NPH', '227537').
card_watermark('fallen ferromancer'/'NPH', 'Phyrexian').

card_in_set('flameborn viron', 'NPH').
card_original_type('flameborn viron'/'NPH', 'Creature — Insect').
card_original_text('flameborn viron'/'NPH', '').
card_first_print('flameborn viron', 'NPH').
card_image_name('flameborn viron'/'NPH', 'flameborn viron').
card_uid('flameborn viron'/'NPH', 'NPH:Flameborn Viron:flameborn viron').
card_rarity('flameborn viron'/'NPH', 'Common').
card_artist('flameborn viron'/'NPH', 'Svetlin Velinov').
card_number('flameborn viron'/'NPH', '83').
card_flavor_text('flameborn viron'/'NPH', '\"Large or small, all will toil for the Great Work.\"\n—Decree of Urabrask').
card_multiverse_id('flameborn viron'/'NPH', '233086').
card_watermark('flameborn viron'/'NPH', 'Phyrexian').

card_in_set('forced worship', 'NPH').
card_original_type('forced worship'/'NPH', 'Enchantment — Aura').
card_original_text('forced worship'/'NPH', 'Enchant creature\nEnchanted creature can\'t attack.\n{2}{W}: Return Forced Worship to its owner\'s hand.').
card_first_print('forced worship', 'NPH').
card_image_name('forced worship'/'NPH', 'forced worship').
card_uid('forced worship'/'NPH', 'NPH:Forced Worship:forced worship').
card_rarity('forced worship'/'NPH', 'Common').
card_artist('forced worship'/'NPH', 'Karl Kopinski').
card_number('forced worship'/'NPH', '11').
card_flavor_text('forced worship'/'NPH', 'Imprisonment teaches revenge. Hobbling teaches resignation.').
card_multiverse_id('forced worship'/'NPH', '230083').
card_watermark('forced worship'/'NPH', 'Phyrexian').

card_in_set('forest', 'NPH').
card_original_type('forest'/'NPH', 'Basic Land — Forest').
card_original_text('forest'/'NPH', 'G').
card_image_name('forest'/'NPH', 'forest1').
card_uid('forest'/'NPH', 'NPH:Forest:forest1').
card_rarity('forest'/'NPH', 'Basic Land').
card_artist('forest'/'NPH', 'Mark Tedin').
card_number('forest'/'NPH', '174').
card_multiverse_id('forest'/'NPH', '230081').

card_in_set('forest', 'NPH').
card_original_type('forest'/'NPH', 'Basic Land — Forest').
card_original_text('forest'/'NPH', 'G').
card_image_name('forest'/'NPH', 'forest2').
card_uid('forest'/'NPH', 'NPH:Forest:forest2').
card_rarity('forest'/'NPH', 'Basic Land').
card_artist('forest'/'NPH', 'Mark Tedin').
card_number('forest'/'NPH', '175').
card_multiverse_id('forest'/'NPH', '230071').

card_in_set('fresh meat', 'NPH').
card_original_type('fresh meat'/'NPH', 'Instant').
card_original_text('fresh meat'/'NPH', 'Put a 3/3 green Beast creature token onto the battlefield for each creature put into your graveyard from the battlefield this turn.').
card_first_print('fresh meat', 'NPH').
card_image_name('fresh meat'/'NPH', 'fresh meat').
card_uid('fresh meat'/'NPH', 'NPH:Fresh Meat:fresh meat').
card_rarity('fresh meat'/'NPH', 'Rare').
card_artist('fresh meat'/'NPH', 'Dave Allsop').
card_number('fresh meat'/'NPH', '109').
card_flavor_text('fresh meat'/'NPH', 'A scavenger\'s favorite appetizer is death.').
card_multiverse_id('fresh meat'/'NPH', '218051').
card_watermark('fresh meat'/'NPH', 'Phyrexian').

card_in_set('furnace scamp', 'NPH').
card_original_type('furnace scamp'/'NPH', 'Creature — Beast').
card_original_text('furnace scamp'/'NPH', 'Whenever Furnace Scamp deals combat damage to a player, you may sacrifice it. If you do, Furnace Scamp deals 3 damage to that player.').
card_first_print('furnace scamp', 'NPH').
card_image_name('furnace scamp'/'NPH', 'furnace scamp').
card_uid('furnace scamp'/'NPH', 'NPH:Furnace Scamp:furnace scamp').
card_rarity('furnace scamp'/'NPH', 'Common').
card_artist('furnace scamp'/'NPH', 'Karl Kopinski').
card_number('furnace scamp'/'NPH', '84').
card_flavor_text('furnace scamp'/'NPH', 'Born of the core, and dying to stoke it.').
card_multiverse_id('furnace scamp'/'NPH', '217979').
card_watermark('furnace scamp'/'NPH', 'Phyrexian').

card_in_set('geosurge', 'NPH').
card_original_type('geosurge'/'NPH', 'Sorcery').
card_original_text('geosurge'/'NPH', 'Add {R}{R}{R}{R}{R}{R}{R} to your mana pool. Spend this mana only to cast artifact or creature spells.').
card_first_print('geosurge', 'NPH').
card_image_name('geosurge'/'NPH', 'geosurge').
card_uid('geosurge'/'NPH', 'NPH:Geosurge:geosurge').
card_rarity('geosurge'/'NPH', 'Uncommon').
card_artist('geosurge'/'NPH', 'Igor Kieryluk').
card_number('geosurge'/'NPH', '85').
card_flavor_text('geosurge'/'NPH', '\"The mountains have been corrupted. Otherwise, they would not lend me their fire so easily.\"\n—Koth of the Hammer').
card_multiverse_id('geosurge'/'NPH', '218004').
card_watermark('geosurge'/'NPH', 'Mirran').

card_in_set('geth\'s verdict', 'NPH').
card_original_type('geth\'s verdict'/'NPH', 'Instant').
card_original_text('geth\'s verdict'/'NPH', 'Target player sacrifices a creature and loses 1 life.').
card_first_print('geth\'s verdict', 'NPH').
card_image_name('geth\'s verdict'/'NPH', 'geth\'s verdict').
card_uid('geth\'s verdict'/'NPH', 'NPH:Geth\'s Verdict:geth\'s verdict').
card_rarity('geth\'s verdict'/'NPH', 'Common').
card_artist('geth\'s verdict'/'NPH', 'Whit Brachna').
card_number('geth\'s verdict'/'NPH', '61').
card_flavor_text('geth\'s verdict'/'NPH', '\"Everyone should owe you something.\"\n—Geth, Lord of the Vault').
card_multiverse_id('geth\'s verdict'/'NPH', '214376').
card_watermark('geth\'s verdict'/'NPH', 'Phyrexian').

card_in_set('gitaxian probe', 'NPH').
card_original_type('gitaxian probe'/'NPH', 'Sorcery').
card_original_text('gitaxian probe'/'NPH', '({U/P} can be paid with either {U} or 2 life.)\nLook at target player\'s hand.\nDraw a card.').
card_image_name('gitaxian probe'/'NPH', 'gitaxian probe').
card_uid('gitaxian probe'/'NPH', 'NPH:Gitaxian Probe:gitaxian probe').
card_rarity('gitaxian probe'/'NPH', 'Common').
card_artist('gitaxian probe'/'NPH', 'Chippy').
card_number('gitaxian probe'/'NPH', '35').
card_flavor_text('gitaxian probe'/'NPH', '\"My flesh holds no secrets, monster. The spirit of Mirrodin will fight on.\"\n—Vy Covalt, Mirran resistance').
card_multiverse_id('gitaxian probe'/'NPH', '233056').
card_watermark('gitaxian probe'/'NPH', 'Phyrexian').

card_in_set('glissa\'s scorn', 'NPH').
card_original_type('glissa\'s scorn'/'NPH', 'Instant').
card_original_text('glissa\'s scorn'/'NPH', 'Destroy target artifact. Its controller loses 1 life.').
card_first_print('glissa\'s scorn', 'NPH').
card_image_name('glissa\'s scorn'/'NPH', 'glissa\'s scorn').
card_uid('glissa\'s scorn'/'NPH', 'NPH:Glissa\'s Scorn:glissa\'s scorn').
card_rarity('glissa\'s scorn'/'NPH', 'Common').
card_artist('glissa\'s scorn'/'NPH', 'Nils Hamm').
card_number('glissa\'s scorn'/'NPH', '110').
card_flavor_text('glissa\'s scorn'/'NPH', '\"If it were fit to survive, it wouldn\'t have been so easily put down.\"\n—Glissa').
card_multiverse_id('glissa\'s scorn'/'NPH', '194171').
card_watermark('glissa\'s scorn'/'NPH', 'Phyrexian').

card_in_set('glistener elf', 'NPH').
card_original_type('glistener elf'/'NPH', 'Creature — Elf Warrior').
card_original_text('glistener elf'/'NPH', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_image_name('glistener elf'/'NPH', 'glistener elf').
card_uid('glistener elf'/'NPH', 'NPH:Glistener Elf:glistener elf').
card_rarity('glistener elf'/'NPH', 'Common').
card_artist('glistener elf'/'NPH', 'Steve Argyle').
card_number('glistener elf'/'NPH', '111').
card_flavor_text('glistener elf'/'NPH', '\"Beg me for life, and I will fill you with the glory of Phyrexian perfection.\"').
card_multiverse_id('glistener elf'/'NPH', '233052').
card_watermark('glistener elf'/'NPH', 'Phyrexian').

card_in_set('glistening oil', 'NPH').
card_original_type('glistening oil'/'NPH', 'Enchantment — Aura').
card_original_text('glistening oil'/'NPH', 'Enchant creature\nEnchanted creature has infect.\nAt the beginning of your upkeep, put a -1/-1 counter on enchanted creature.\nWhen Glistening Oil is put into a graveyard from the battlefield, return Glistening Oil to its owner\'s hand.').
card_first_print('glistening oil', 'NPH').
card_image_name('glistening oil'/'NPH', 'glistening oil').
card_uid('glistening oil'/'NPH', 'NPH:Glistening Oil:glistening oil').
card_rarity('glistening oil'/'NPH', 'Rare').
card_artist('glistening oil'/'NPH', 'Steven Belledin').
card_number('glistening oil'/'NPH', '62').
card_multiverse_id('glistening oil'/'NPH', '214346').
card_watermark('glistening oil'/'NPH', 'Phyrexian').

card_in_set('greenhilt trainee', 'NPH').
card_original_type('greenhilt trainee'/'NPH', 'Creature — Elf Warrior').
card_original_text('greenhilt trainee'/'NPH', '{T}: Target creature gets +4/+4 until end of turn. Activate this ability only if Greenhilt Trainee\'s power is 4 or greater.').
card_first_print('greenhilt trainee', 'NPH').
card_image_name('greenhilt trainee'/'NPH', 'greenhilt trainee').
card_uid('greenhilt trainee'/'NPH', 'NPH:Greenhilt Trainee:greenhilt trainee').
card_rarity('greenhilt trainee'/'NPH', 'Uncommon').
card_artist('greenhilt trainee'/'NPH', 'Chris Rahn').
card_number('greenhilt trainee'/'NPH', '112').
card_flavor_text('greenhilt trainee'/'NPH', '\"Our ancestors adapted to this world of metal. Just as they did, we will adapt to this world of evil.\"').
card_multiverse_id('greenhilt trainee'/'NPH', '233063').
card_watermark('greenhilt trainee'/'NPH', 'Mirran').

card_in_set('gremlin mine', 'NPH').
card_original_type('gremlin mine'/'NPH', 'Artifact').
card_original_text('gremlin mine'/'NPH', '{1}, {T}, Sacrifice Gremlin Mine: Gremlin Mine deals 4 damage to target artifact creature.\n{1}, {T}, Sacrifice Gremlin Mine: Remove up to four charge counters from target noncreature artifact.').
card_first_print('gremlin mine', 'NPH').
card_image_name('gremlin mine'/'NPH', 'gremlin mine').
card_uid('gremlin mine'/'NPH', 'NPH:Gremlin Mine:gremlin mine').
card_rarity('gremlin mine'/'NPH', 'Common').
card_artist('gremlin mine'/'NPH', 'Matt Stewart').
card_number('gremlin mine'/'NPH', '136').
card_multiverse_id('gremlin mine'/'NPH', '218068').
card_watermark('gremlin mine'/'NPH', 'Phyrexian').

card_in_set('grim affliction', 'NPH').
card_original_type('grim affliction'/'NPH', 'Instant').
card_original_text('grim affliction'/'NPH', 'Put a -1/-1 counter on target creature, then proliferate. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there.)').
card_first_print('grim affliction', 'NPH').
card_image_name('grim affliction'/'NPH', 'grim affliction').
card_uid('grim affliction'/'NPH', 'NPH:Grim Affliction:grim affliction').
card_rarity('grim affliction'/'NPH', 'Common').
card_artist('grim affliction'/'NPH', 'Erica Yang').
card_number('grim affliction'/'NPH', '63').
card_flavor_text('grim affliction'/'NPH', 'Even the small wounds let hope bleed out.').
card_multiverse_id('grim affliction'/'NPH', '214389').
card_watermark('grim affliction'/'NPH', 'Phyrexian').

card_in_set('gut shot', 'NPH').
card_original_type('gut shot'/'NPH', 'Instant').
card_original_text('gut shot'/'NPH', '({R/P} can be paid with either {R} or 2 life.)\nGut Shot deals 1 damage to target creature or player.').
card_first_print('gut shot', 'NPH').
card_image_name('gut shot'/'NPH', 'gut shot').
card_uid('gut shot'/'NPH', 'NPH:Gut Shot:gut shot').
card_rarity('gut shot'/'NPH', 'Uncommon').
card_artist('gut shot'/'NPH', 'Greg Staples').
card_number('gut shot'/'NPH', '86').
card_flavor_text('gut shot'/'NPH', '\"Down here, we have a more pointed version of the scriptures.\"\n—Urabrask\'s enforcer').
card_multiverse_id('gut shot'/'NPH', '230074').
card_watermark('gut shot'/'NPH', 'Phyrexian').

card_in_set('hex parasite', 'NPH').
card_original_type('hex parasite'/'NPH', 'Artifact Creature — Insect').
card_original_text('hex parasite'/'NPH', '{X}{B/P}: Remove up to X counters from target permanent. For each counter removed this way, Hex Parasite gets +1/+0 until end of turn. ({B/P} can be paid with either {B} or 2 life.)').
card_first_print('hex parasite', 'NPH').
card_image_name('hex parasite'/'NPH', 'hex parasite').
card_uid('hex parasite'/'NPH', 'NPH:Hex Parasite:hex parasite').
card_rarity('hex parasite'/'NPH', 'Rare').
card_artist('hex parasite'/'NPH', 'Raymond Swanland').
card_number('hex parasite'/'NPH', '137').
card_flavor_text('hex parasite'/'NPH', 'Drawn by power, fueled by conquest.').
card_multiverse_id('hex parasite'/'NPH', '218008').
card_watermark('hex parasite'/'NPH', 'Phyrexian').

card_in_set('hovermyr', 'NPH').
card_original_type('hovermyr'/'NPH', 'Artifact Creature — Myr').
card_original_text('hovermyr'/'NPH', 'Flying, vigilance').
card_first_print('hovermyr', 'NPH').
card_image_name('hovermyr'/'NPH', 'hovermyr').
card_uid('hovermyr'/'NPH', 'NPH:Hovermyr:hovermyr').
card_rarity('hovermyr'/'NPH', 'Common').
card_artist('hovermyr'/'NPH', 'Dan Scott').
card_number('hovermyr'/'NPH', '138').
card_flavor_text('hovermyr'/'NPH', 'Originally created to harvest blinkmoths, the hovermyr are now the silent observers of a dying world.').
card_multiverse_id('hovermyr'/'NPH', '227513').
card_watermark('hovermyr'/'NPH', 'Mirran').

card_in_set('ichor explosion', 'NPH').
card_original_type('ichor explosion'/'NPH', 'Sorcery').
card_original_text('ichor explosion'/'NPH', 'As an additional cost to cast Ichor Explosion, sacrifice a creature.\nAll creatures get -X/-X until end of turn, where X is the sacrificed creature\'s power.').
card_first_print('ichor explosion', 'NPH').
card_image_name('ichor explosion'/'NPH', 'ichor explosion').
card_uid('ichor explosion'/'NPH', 'NPH:Ichor Explosion:ichor explosion').
card_rarity('ichor explosion'/'NPH', 'Uncommon').
card_artist('ichor explosion'/'NPH', 'James Ryman').
card_number('ichor explosion'/'NPH', '64').
card_flavor_text('ichor explosion'/'NPH', 'The first explosion that didn\'t excite goblins.').
card_multiverse_id('ichor explosion'/'NPH', '218066').
card_watermark('ichor explosion'/'NPH', 'Phyrexian').

card_in_set('immolating souleater', 'NPH').
card_original_type('immolating souleater'/'NPH', 'Artifact Creature — Hound').
card_original_text('immolating souleater'/'NPH', '{R/P}: Immolating Souleater gets +1/+0 until end of turn. ({R/P} can be paid with either {R} or 2 life.)').
card_first_print('immolating souleater', 'NPH').
card_image_name('immolating souleater'/'NPH', 'immolating souleater').
card_uid('immolating souleater'/'NPH', 'NPH:Immolating Souleater:immolating souleater').
card_rarity('immolating souleater'/'NPH', 'Common').
card_artist('immolating souleater'/'NPH', 'Austin Hsu').
card_number('immolating souleater'/'NPH', '139').
card_flavor_text('immolating souleater'/'NPH', '\"We thank the souleaters for melting the sinful that we may be reminded of our own insignificance.\"\n—Drones\' hymn of gratitude').
card_multiverse_id('immolating souleater'/'NPH', '233058').
card_watermark('immolating souleater'/'NPH', 'Phyrexian').

card_in_set('impaler shrike', 'NPH').
card_original_type('impaler shrike'/'NPH', 'Creature — Bird').
card_original_text('impaler shrike'/'NPH', 'Flying\nWhen Impaler Shrike deals combat damage to a player, you may sacrifice it. If you do, draw three cards.').
card_first_print('impaler shrike', 'NPH').
card_image_name('impaler shrike'/'NPH', 'impaler shrike').
card_uid('impaler shrike'/'NPH', 'NPH:Impaler Shrike:impaler shrike').
card_rarity('impaler shrike'/'NPH', 'Common').
card_artist('impaler shrike'/'NPH', 'Nils Hamm').
card_number('impaler shrike'/'NPH', '36').
card_flavor_text('impaler shrike'/'NPH', '\"Interrogation is an unnecessary step for acquiring information.\"\n—Politus, Gitaxian morphologist').
card_multiverse_id('impaler shrike'/'NPH', '217988').
card_watermark('impaler shrike'/'NPH', 'Phyrexian').

card_in_set('inquisitor exarch', 'NPH').
card_original_type('inquisitor exarch'/'NPH', 'Creature — Cleric').
card_original_text('inquisitor exarch'/'NPH', 'When Inquisitor Exarch enters the battlefield, choose one — You gain 2 life; or target opponent loses 2 life.').
card_first_print('inquisitor exarch', 'NPH').
card_image_name('inquisitor exarch'/'NPH', 'inquisitor exarch').
card_uid('inquisitor exarch'/'NPH', 'NPH:Inquisitor Exarch:inquisitor exarch').
card_rarity('inquisitor exarch'/'NPH', 'Uncommon').
card_artist('inquisitor exarch'/'NPH', 'Igor Kieryluk').
card_number('inquisitor exarch'/'NPH', '12').
card_flavor_text('inquisitor exarch'/'NPH', '\"Skin is the prison of the blessed and the stronghold of the heretic.\"\n—Argent Etchings, plate 64, passage 17').
card_multiverse_id('inquisitor exarch'/'NPH', '218070').
card_watermark('inquisitor exarch'/'NPH', 'Phyrexian').

card_in_set('insatiable souleater', 'NPH').
card_original_type('insatiable souleater'/'NPH', 'Artifact Creature — Beast').
card_original_text('insatiable souleater'/'NPH', '{G/P}: Insatiable Souleater gains trample until end of turn. ({G/P} can be paid with either {G} or 2 life.)').
card_first_print('insatiable souleater', 'NPH').
card_image_name('insatiable souleater'/'NPH', 'insatiable souleater').
card_uid('insatiable souleater'/'NPH', 'NPH:Insatiable Souleater:insatiable souleater').
card_rarity('insatiable souleater'/'NPH', 'Common').
card_artist('insatiable souleater'/'NPH', 'Dave Kendall').
card_number('insatiable souleater'/'NPH', '140').
card_flavor_text('insatiable souleater'/'NPH', '\"We thank the souleaters for culling our sick so that the strong may earn their triumphs.\"\n—Drones\' hymn of gratitude').
card_multiverse_id('insatiable souleater'/'NPH', '233066').
card_watermark('insatiable souleater'/'NPH', 'Phyrexian').

card_in_set('invader parasite', 'NPH').
card_original_type('invader parasite'/'NPH', 'Creature — Insect').
card_original_text('invader parasite'/'NPH', 'Imprint — When Invader Parasite enters the battlefield, exile target land.\nWhenever a land with the same name as the exiled card enters the battlefield under an opponent\'s control, Invader Parasite deals 2 damage to that player.').
card_first_print('invader parasite', 'NPH').
card_image_name('invader parasite'/'NPH', 'invader parasite').
card_uid('invader parasite'/'NPH', 'NPH:Invader Parasite:invader parasite').
card_rarity('invader parasite'/'NPH', 'Rare').
card_artist('invader parasite'/'NPH', 'Volkan Baga').
card_number('invader parasite'/'NPH', '87').
card_multiverse_id('invader parasite'/'NPH', '233067').
card_watermark('invader parasite'/'NPH', 'Phyrexian').

card_in_set('island', 'NPH').
card_original_type('island'/'NPH', 'Basic Land — Island').
card_original_text('island'/'NPH', 'U').
card_image_name('island'/'NPH', 'island1').
card_uid('island'/'NPH', 'NPH:Island:island1').
card_rarity('island'/'NPH', 'Basic Land').
card_artist('island'/'NPH', 'Jung Park').
card_number('island'/'NPH', '168').
card_multiverse_id('island'/'NPH', '227530').

card_in_set('island', 'NPH').
card_original_type('island'/'NPH', 'Basic Land — Island').
card_original_text('island'/'NPH', 'U').
card_image_name('island'/'NPH', 'island2').
card_uid('island'/'NPH', 'NPH:Island:island2').
card_rarity('island'/'NPH', 'Basic Land').
card_artist('island'/'NPH', 'Jung Park').
card_number('island'/'NPH', '169').
card_multiverse_id('island'/'NPH', '227505').

card_in_set('isolation cell', 'NPH').
card_original_type('isolation cell'/'NPH', 'Artifact').
card_original_text('isolation cell'/'NPH', 'Whenever an opponent casts a creature spell, that player loses 2 life unless he or she pays {2}.').
card_first_print('isolation cell', 'NPH').
card_image_name('isolation cell'/'NPH', 'isolation cell').
card_uid('isolation cell'/'NPH', 'NPH:Isolation Cell:isolation cell').
card_rarity('isolation cell'/'NPH', 'Uncommon').
card_artist('isolation cell'/'NPH', 'Adrian Smith').
card_number('isolation cell'/'NPH', '141').
card_flavor_text('isolation cell'/'NPH', '\"Attrition is the answer. Starve them, and let their despair be their undoing.\"\n—Sheoldred, Whispering One').
card_multiverse_id('isolation cell'/'NPH', '218057').
card_watermark('isolation cell'/'NPH', 'Phyrexian').

card_in_set('jin-gitaxias, core augur', 'NPH').
card_original_type('jin-gitaxias, core augur'/'NPH', 'Legendary Creature — Praetor').
card_original_text('jin-gitaxias, core augur'/'NPH', 'Flash\nAt the beginning of your end step, draw seven cards.\nEach opponent\'s maximum hand size is reduced by seven.').
card_first_print('jin-gitaxias, core augur', 'NPH').
card_image_name('jin-gitaxias, core augur'/'NPH', 'jin-gitaxias, core augur').
card_uid('jin-gitaxias, core augur'/'NPH', 'NPH:Jin-Gitaxias, Core Augur:jin-gitaxias, core augur').
card_rarity('jin-gitaxias, core augur'/'NPH', 'Mythic Rare').
card_artist('jin-gitaxias, core augur'/'NPH', 'Eric Deschamps').
card_number('jin-gitaxias, core augur'/'NPH', '37').
card_flavor_text('jin-gitaxias, core augur'/'NPH', '\"It is not a goal, but a process—the process of creating the perfect Phyrexia.\"').
card_multiverse_id('jin-gitaxias, core augur'/'NPH', '214349').
card_watermark('jin-gitaxias, core augur'/'NPH', 'Phyrexian').

card_in_set('jor kadeen, the prevailer', 'NPH').
card_original_type('jor kadeen, the prevailer'/'NPH', 'Legendary Creature — Human Warrior').
card_original_text('jor kadeen, the prevailer'/'NPH', 'First strike\nMetalcraft — Creatures you control get +3/+0 as long as you control three or more artifacts.').
card_first_print('jor kadeen, the prevailer', 'NPH').
card_image_name('jor kadeen, the prevailer'/'NPH', 'jor kadeen, the prevailer').
card_uid('jor kadeen, the prevailer'/'NPH', 'NPH:Jor Kadeen, the Prevailer:jor kadeen, the prevailer').
card_rarity('jor kadeen, the prevailer'/'NPH', 'Rare').
card_artist('jor kadeen, the prevailer'/'NPH', 'Austin Hsu').
card_number('jor kadeen, the prevailer'/'NPH', '128').
card_flavor_text('jor kadeen, the prevailer'/'NPH', '\"As long as my hand holds a blade, there\'s hope for a pure Mirrodin once again.\"').
card_multiverse_id('jor kadeen, the prevailer'/'NPH', '227508').
card_watermark('jor kadeen, the prevailer'/'NPH', 'Mirran').

card_in_set('karn liberated', 'NPH').
card_original_type('karn liberated'/'NPH', 'Planeswalker — Karn').
card_original_text('karn liberated'/'NPH', '+4: Target player exiles a card from his or her hand.\n-3: Exile target permanent.\n-14: Restart the game, leaving in exile all non-Aura permanent cards exiled with Karn Liberated. Then put those cards onto the battlefield under your control.').
card_first_print('karn liberated', 'NPH').
card_image_name('karn liberated'/'NPH', 'karn liberated').
card_uid('karn liberated'/'NPH', 'NPH:Karn Liberated:karn liberated').
card_rarity('karn liberated'/'NPH', 'Mythic Rare').
card_artist('karn liberated'/'NPH', 'Jason Chan').
card_number('karn liberated'/'NPH', '1').
card_multiverse_id('karn liberated'/'NPH', '214350').

card_in_set('kiln walker', 'NPH').
card_original_type('kiln walker'/'NPH', 'Artifact Creature — Construct').
card_original_text('kiln walker'/'NPH', 'Whenever Kiln Walker attacks, it gets +3/+0 until end of turn.').
card_first_print('kiln walker', 'NPH').
card_image_name('kiln walker'/'NPH', 'kiln walker').
card_uid('kiln walker'/'NPH', 'NPH:Kiln Walker:kiln walker').
card_rarity('kiln walker'/'NPH', 'Uncommon').
card_artist('kiln walker'/'NPH', 'Volkan Baga').
card_number('kiln walker'/'NPH', '142').
card_flavor_text('kiln walker'/'NPH', 'If it possesses an ability to understand and appreciate life, it has never shown it.').
card_multiverse_id('kiln walker'/'NPH', '214380').
card_watermark('kiln walker'/'NPH', 'Phyrexian').

card_in_set('lashwrithe', 'NPH').
card_original_type('lashwrithe'/'NPH', 'Artifact — Equipment').
card_original_text('lashwrithe'/'NPH', 'Living weapon (When this Equipment enters the battlefield, put a 0/0 black Germ creature token onto the battlefield, then attach this to it.)\nEquipped creature gets +1/+1 for each Swamp you control.\nEquip {B/P}{B/P} ({B/P} can be paid with either {B} or 2 life.)').
card_first_print('lashwrithe', 'NPH').
card_image_name('lashwrithe'/'NPH', 'lashwrithe').
card_uid('lashwrithe'/'NPH', 'NPH:Lashwrithe:lashwrithe').
card_rarity('lashwrithe'/'NPH', 'Rare').
card_artist('lashwrithe'/'NPH', 'Jason Felix').
card_number('lashwrithe'/'NPH', '143').
card_multiverse_id('lashwrithe'/'NPH', '214359').
card_watermark('lashwrithe'/'NPH', 'Phyrexian').

card_in_set('leeching bite', 'NPH').
card_original_type('leeching bite'/'NPH', 'Instant').
card_original_text('leeching bite'/'NPH', 'Target creature gets +1/+1 until end of turn. Another target creature gets -1/-1 until end of turn.').
card_first_print('leeching bite', 'NPH').
card_image_name('leeching bite'/'NPH', 'leeching bite').
card_uid('leeching bite'/'NPH', 'NPH:Leeching Bite:leeching bite').
card_rarity('leeching bite'/'NPH', 'Common').
card_artist('leeching bite'/'NPH', 'Cos Koniotis').
card_number('leeching bite'/'NPH', '113').
card_flavor_text('leeching bite'/'NPH', 'A substantial portion of Vorinclex\'s forces became fuel for the rest.').
card_multiverse_id('leeching bite'/'NPH', '218028').
card_watermark('leeching bite'/'NPH', 'Phyrexian').

card_in_set('life\'s finale', 'NPH').
card_original_type('life\'s finale'/'NPH', 'Sorcery').
card_original_text('life\'s finale'/'NPH', 'Destroy all creatures, then search target opponent\'s library for up to three creature cards and put them into his or her graveyard. Then that player shuffles his or her library.').
card_first_print('life\'s finale', 'NPH').
card_image_name('life\'s finale'/'NPH', 'life\'s finale').
card_uid('life\'s finale'/'NPH', 'NPH:Life\'s Finale:life\'s finale').
card_rarity('life\'s finale'/'NPH', 'Rare').
card_artist('life\'s finale'/'NPH', 'Svetlin Velinov').
card_number('life\'s finale'/'NPH', '65').
card_flavor_text('life\'s finale'/'NPH', 'The feeble resistance of the flesh is over. Phyrexia spreads its shadow over all.').
card_multiverse_id('life\'s finale'/'NPH', '214362').
card_watermark('life\'s finale'/'NPH', 'Phyrexian').

card_in_set('lost leonin', 'NPH').
card_original_type('lost leonin'/'NPH', 'Creature — Cat Soldier').
card_original_text('lost leonin'/'NPH', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_first_print('lost leonin', 'NPH').
card_image_name('lost leonin'/'NPH', 'lost leonin').
card_uid('lost leonin'/'NPH', 'NPH:Lost Leonin:lost leonin').
card_rarity('lost leonin'/'NPH', 'Common').
card_artist('lost leonin'/'NPH', 'Min Yum').
card_number('lost leonin'/'NPH', '13').
card_flavor_text('lost leonin'/'NPH', 'Mirrans were broken down and rebuilt, made to be loyal servants of the force they had fought.').
card_multiverse_id('lost leonin'/'NPH', '218053').
card_watermark('lost leonin'/'NPH', 'Phyrexian').

card_in_set('loxodon convert', 'NPH').
card_original_type('loxodon convert'/'NPH', 'Creature — Elephant Soldier').
card_original_text('loxodon convert'/'NPH', '').
card_first_print('loxodon convert', 'NPH').
card_image_name('loxodon convert'/'NPH', 'loxodon convert').
card_uid('loxodon convert'/'NPH', 'NPH:Loxodon Convert:loxodon convert').
card_rarity('loxodon convert'/'NPH', 'Common').
card_artist('loxodon convert'/'NPH', 'Adrian Smith').
card_number('loxodon convert'/'NPH', '14').
card_flavor_text('loxodon convert'/'NPH', 'Just one drop of the glistening oil can eventually stain even a soul as stalwart as a loxodon\'s beyond redemption.').
card_multiverse_id('loxodon convert'/'NPH', '218013').
card_watermark('loxodon convert'/'NPH', 'Phyrexian').

card_in_set('marrow shards', 'NPH').
card_original_type('marrow shards'/'NPH', 'Instant').
card_original_text('marrow shards'/'NPH', '({W/P} can be paid with either {W} or 2 life.)\nMarrow Shards deals 1 damage to each attacking creature.').
card_first_print('marrow shards', 'NPH').
card_image_name('marrow shards'/'NPH', 'marrow shards').
card_uid('marrow shards'/'NPH', 'NPH:Marrow Shards:marrow shards').
card_rarity('marrow shards'/'NPH', 'Uncommon').
card_artist('marrow shards'/'NPH', 'Raymond Swanland').
card_number('marrow shards'/'NPH', '15').
card_flavor_text('marrow shards'/'NPH', '\"We are a single entity. Dissenters must be sutured into the Orthodoxy.\"\n—Elesh Norn, Grand Cenobite').
card_multiverse_id('marrow shards'/'NPH', '230072').
card_watermark('marrow shards'/'NPH', 'Phyrexian').

card_in_set('master splicer', 'NPH').
card_original_type('master splicer'/'NPH', 'Creature — Human Artificer').
card_original_text('master splicer'/'NPH', 'When Master Splicer enters the battlefield, put a 3/3 colorless Golem artifact creature token onto the battlefield.\nGolem creatures you control get +1/+1.').
card_first_print('master splicer', 'NPH').
card_image_name('master splicer'/'NPH', 'master splicer').
card_uid('master splicer'/'NPH', 'NPH:Master Splicer:master splicer').
card_rarity('master splicer'/'NPH', 'Uncommon').
card_artist('master splicer'/'NPH', 'Chippy').
card_number('master splicer'/'NPH', '16').
card_flavor_text('master splicer'/'NPH', 'With Phyrexia\'s victory at hand, each sect began perfecting its vision.').
card_multiverse_id('master splicer'/'NPH', '233062').
card_watermark('master splicer'/'NPH', 'Phyrexian').

card_in_set('maul splicer', 'NPH').
card_original_type('maul splicer'/'NPH', 'Creature — Human Artificer').
card_original_text('maul splicer'/'NPH', 'When Maul Splicer enters the battlefield, put two 3/3 colorless Golem artifact creature tokens onto the battlefield.\nGolem creatures you control have trample.').
card_image_name('maul splicer'/'NPH', 'maul splicer').
card_uid('maul splicer'/'NPH', 'NPH:Maul Splicer:maul splicer').
card_rarity('maul splicer'/'NPH', 'Common').
card_artist('maul splicer'/'NPH', 'Jason Chan').
card_number('maul splicer'/'NPH', '114').
card_multiverse_id('maul splicer'/'NPH', '233040').
card_watermark('maul splicer'/'NPH', 'Phyrexian').

card_in_set('melira, sylvok outcast', 'NPH').
card_original_type('melira, sylvok outcast'/'NPH', 'Legendary Creature — Human Scout').
card_original_text('melira, sylvok outcast'/'NPH', 'You can\'t get poison counters.\nCreatures you control can\'t have -1/-1 counters placed on them.\nCreatures your opponents control lose infect.').
card_first_print('melira, sylvok outcast', 'NPH').
card_image_name('melira, sylvok outcast'/'NPH', 'melira, sylvok outcast').
card_uid('melira, sylvok outcast'/'NPH', 'NPH:Melira, Sylvok Outcast:melira, sylvok outcast').
card_rarity('melira, sylvok outcast'/'NPH', 'Rare').
card_artist('melira, sylvok outcast'/'NPH', 'Min Yum').
card_number('melira, sylvok outcast'/'NPH', '115').
card_flavor_text('melira, sylvok outcast'/'NPH', 'Once a pariah, now Mirrodin\'s greatest hope.').
card_multiverse_id('melira, sylvok outcast'/'NPH', '194274').
card_watermark('melira, sylvok outcast'/'NPH', 'Mirran').

card_in_set('mental misstep', 'NPH').
card_original_type('mental misstep'/'NPH', 'Instant').
card_original_text('mental misstep'/'NPH', '({U/P} can be paid with either {U} or 2 life.)\nCounter target spell with converted mana cost 1.').
card_first_print('mental misstep', 'NPH').
card_image_name('mental misstep'/'NPH', 'mental misstep').
card_uid('mental misstep'/'NPH', 'NPH:Mental Misstep:mental misstep').
card_rarity('mental misstep'/'NPH', 'Uncommon').
card_artist('mental misstep'/'NPH', 'Erica Yang').
card_number('mental misstep'/'NPH', '38').
card_flavor_text('mental misstep'/'NPH', '\"Your first mistake was thinking I would let you live long enough to make a second.\"\n—Sarnvax, Gitaxian sective').
card_multiverse_id('mental misstep'/'NPH', '230066').
card_watermark('mental misstep'/'NPH', 'Phyrexian').

card_in_set('mindcrank', 'NPH').
card_original_type('mindcrank'/'NPH', 'Artifact').
card_original_text('mindcrank'/'NPH', 'Whenever an opponent loses life, that player puts that many cards from the top of his or her library into his or her graveyard. (Damage dealt by sources without infect causes loss of life.)').
card_first_print('mindcrank', 'NPH').
card_image_name('mindcrank'/'NPH', 'mindcrank').
card_uid('mindcrank'/'NPH', 'NPH:Mindcrank:mindcrank').
card_rarity('mindcrank'/'NPH', 'Uncommon').
card_artist('mindcrank'/'NPH', 'Chris Rahn').
card_number('mindcrank'/'NPH', '144').
card_flavor_text('mindcrank'/'NPH', '\"Let go of your memories and be reborn.\"\n—Argent Etchings, plate 106, passage 27').
card_multiverse_id('mindcrank'/'NPH', '233078').
card_watermark('mindcrank'/'NPH', 'Phyrexian').

card_in_set('mindculling', 'NPH').
card_original_type('mindculling'/'NPH', 'Sorcery').
card_original_text('mindculling'/'NPH', 'You draw two cards and target opponent discards two cards.').
card_first_print('mindculling', 'NPH').
card_image_name('mindculling'/'NPH', 'mindculling').
card_uid('mindculling'/'NPH', 'NPH:Mindculling:mindculling').
card_rarity('mindculling'/'NPH', 'Uncommon').
card_artist('mindculling'/'NPH', 'Cos Koniotis').
card_number('mindculling'/'NPH', '39').
card_flavor_text('mindculling'/'NPH', '\"Why infiltrate their strongholds when what we seek resides in such a poorly guarded safe?\"\n—Malcator, Executor of Synthesis').
card_multiverse_id('mindculling'/'NPH', '218085').
card_watermark('mindculling'/'NPH', 'Phyrexian').

card_in_set('moltensteel dragon', 'NPH').
card_original_type('moltensteel dragon'/'NPH', 'Artifact Creature — Dragon').
card_original_text('moltensteel dragon'/'NPH', '({R/P} can be paid with either {R} or 2 life.)\nFlying\n{R/P}: Moltensteel Dragon gets +1/+0 until end of turn.').
card_first_print('moltensteel dragon', 'NPH').
card_image_name('moltensteel dragon'/'NPH', 'moltensteel dragon').
card_uid('moltensteel dragon'/'NPH', 'NPH:Moltensteel Dragon:moltensteel dragon').
card_rarity('moltensteel dragon'/'NPH', 'Rare').
card_artist('moltensteel dragon'/'NPH', 'James Ryman').
card_number('moltensteel dragon'/'NPH', '88').
card_flavor_text('moltensteel dragon'/'NPH', 'An apocalypse in dragon form.').
card_multiverse_id('moltensteel dragon'/'NPH', '233051').
card_watermark('moltensteel dragon'/'NPH', 'Phyrexian').

card_in_set('mortis dogs', 'NPH').
card_original_type('mortis dogs'/'NPH', 'Creature — Hound').
card_original_text('mortis dogs'/'NPH', 'Whenever Mortis Dogs attacks, it gets +2/+0 until end of turn.\nWhen Mortis Dogs is put into a graveyard from the battlefield, target player loses life equal to its power.').
card_first_print('mortis dogs', 'NPH').
card_image_name('mortis dogs'/'NPH', 'mortis dogs').
card_uid('mortis dogs'/'NPH', 'NPH:Mortis Dogs:mortis dogs').
card_rarity('mortis dogs'/'NPH', 'Common').
card_artist('mortis dogs'/'NPH', 'Chippy').
card_number('mortis dogs'/'NPH', '66').
card_multiverse_id('mortis dogs'/'NPH', '233072').
card_watermark('mortis dogs'/'NPH', 'Phyrexian').

card_in_set('mountain', 'NPH').
card_original_type('mountain'/'NPH', 'Basic Land — Mountain').
card_original_text('mountain'/'NPH', 'R').
card_image_name('mountain'/'NPH', 'mountain1').
card_uid('mountain'/'NPH', 'NPH:Mountain:mountain1').
card_rarity('mountain'/'NPH', 'Basic Land').
card_artist('mountain'/'NPH', 'Tomasz Jedruszek').
card_number('mountain'/'NPH', '172').
card_multiverse_id('mountain'/'NPH', '227507').

card_in_set('mountain', 'NPH').
card_original_type('mountain'/'NPH', 'Basic Land — Mountain').
card_original_text('mountain'/'NPH', 'R').
card_image_name('mountain'/'NPH', 'mountain2').
card_uid('mountain'/'NPH', 'NPH:Mountain:mountain2').
card_rarity('mountain'/'NPH', 'Basic Land').
card_artist('mountain'/'NPH', 'Tomasz Jedruszek').
card_number('mountain'/'NPH', '173').
card_multiverse_id('mountain'/'NPH', '230064').

card_in_set('mutagenic growth', 'NPH').
card_original_type('mutagenic growth'/'NPH', 'Instant').
card_original_text('mutagenic growth'/'NPH', '({G/P} can be paid with either {G} or 2 life.)\nTarget creature gets +2/+2 until end of turn.').
card_first_print('mutagenic growth', 'NPH').
card_image_name('mutagenic growth'/'NPH', 'mutagenic growth').
card_uid('mutagenic growth'/'NPH', 'NPH:Mutagenic Growth:mutagenic growth').
card_rarity('mutagenic growth'/'NPH', 'Common').
card_artist('mutagenic growth'/'NPH', 'Dave Kendall').
card_number('mutagenic growth'/'NPH', '116').
card_flavor_text('mutagenic growth'/'NPH', '\"Sympathy is for weaklings. Whoever survives, wins.\"\n—Benzir, archdruid of Temple Might').
card_multiverse_id('mutagenic growth'/'NPH', '233070').
card_watermark('mutagenic growth'/'NPH', 'Phyrexian').

card_in_set('mycosynth fiend', 'NPH').
card_original_type('mycosynth fiend'/'NPH', 'Creature — Horror').
card_original_text('mycosynth fiend'/'NPH', 'Mycosynth Fiend gets +1/+1 for each poison counter your opponents have.').
card_first_print('mycosynth fiend', 'NPH').
card_image_name('mycosynth fiend'/'NPH', 'mycosynth fiend').
card_uid('mycosynth fiend'/'NPH', 'NPH:Mycosynth Fiend:mycosynth fiend').
card_rarity('mycosynth fiend'/'NPH', 'Uncommon').
card_artist('mycosynth fiend'/'NPH', 'Kev Walker').
card_number('mycosynth fiend'/'NPH', '117').
card_flavor_text('mycosynth fiend'/'NPH', '\"I wish I could take credit for it, but nature progresses on its own, just the way it should.\"\n—Vorinclex, Voice of Hunger').
card_multiverse_id('mycosynth fiend'/'NPH', '217995').
card_watermark('mycosynth fiend'/'NPH', 'Phyrexian').

card_in_set('mycosynth wellspring', 'NPH').
card_original_type('mycosynth wellspring'/'NPH', 'Artifact').
card_original_text('mycosynth wellspring'/'NPH', 'When Mycosynth Wellspring enters the battlefield or is put into a graveyard from the battlefield, you may search your library for a basic land card, reveal it, put it into your hand, then shuffle your library.').
card_first_print('mycosynth wellspring', 'NPH').
card_image_name('mycosynth wellspring'/'NPH', 'mycosynth wellspring').
card_uid('mycosynth wellspring'/'NPH', 'NPH:Mycosynth Wellspring:mycosynth wellspring').
card_rarity('mycosynth wellspring'/'NPH', 'Common').
card_artist('mycosynth wellspring'/'NPH', 'David Rapoza').
card_number('mycosynth wellspring'/'NPH', '145').
card_flavor_text('mycosynth wellspring'/'NPH', 'The oil created the mycosynth. The mycosynth created New Phyrexia.').
card_multiverse_id('mycosynth wellspring'/'NPH', '218039').
card_watermark('mycosynth wellspring'/'NPH', 'Phyrexian').

card_in_set('myr superion', 'NPH').
card_original_type('myr superion'/'NPH', 'Artifact Creature — Myr').
card_original_text('myr superion'/'NPH', 'Spend only mana produced by creatures to cast Myr Superion.').
card_image_name('myr superion'/'NPH', 'myr superion').
card_uid('myr superion'/'NPH', 'NPH:Myr Superion:myr superion').
card_rarity('myr superion'/'NPH', 'Rare').
card_artist('myr superion'/'NPH', 'Jana Schirmer & Johannes Voss').
card_number('myr superion'/'NPH', '146').
card_flavor_text('myr superion'/'NPH', 'Two kinds of myr survive: the powerful and those that stay close by them.').
card_multiverse_id('myr superion'/'NPH', '194275').
card_watermark('myr superion'/'NPH', 'Mirran').

card_in_set('necropouncer', 'NPH').
card_original_type('necropouncer'/'NPH', 'Artifact — Equipment').
card_original_text('necropouncer'/'NPH', 'Living weapon (When this Equipment enters the battlefield, put a 0/0 black Germ creature token onto the battlefield, then attach this to it.)\nEquipped creature gets +3/+1 and has haste.\nEquip {2}').
card_first_print('necropouncer', 'NPH').
card_image_name('necropouncer'/'NPH', 'necropouncer').
card_uid('necropouncer'/'NPH', 'NPH:Necropouncer:necropouncer').
card_rarity('necropouncer'/'NPH', 'Uncommon').
card_artist('necropouncer'/'NPH', 'Cos Koniotis').
card_number('necropouncer'/'NPH', '147').
card_multiverse_id('necropouncer'/'NPH', '214367').
card_watermark('necropouncer'/'NPH', 'Phyrexian').

card_in_set('norn\'s annex', 'NPH').
card_original_type('norn\'s annex'/'NPH', 'Artifact').
card_original_text('norn\'s annex'/'NPH', '({W/P} can be paid with either {W} or 2 life.)\nCreatures can\'t attack you or a planeswalker you control unless their controller pays {W/P} for each of those creatures.').
card_first_print('norn\'s annex', 'NPH').
card_image_name('norn\'s annex'/'NPH', 'norn\'s annex').
card_uid('norn\'s annex'/'NPH', 'NPH:Norn\'s Annex:norn\'s annex').
card_rarity('norn\'s annex'/'NPH', 'Rare').
card_artist('norn\'s annex'/'NPH', 'James Paick').
card_number('norn\'s annex'/'NPH', '17').
card_multiverse_id('norn\'s annex'/'NPH', '233050').
card_watermark('norn\'s annex'/'NPH', 'Phyrexian').

card_in_set('noxious revival', 'NPH').
card_original_type('noxious revival'/'NPH', 'Instant').
card_original_text('noxious revival'/'NPH', '({G/P} can be paid with either {G} or 2 life.)\nPut target card from a graveyard on top of its owner\'s library.').
card_first_print('noxious revival', 'NPH').
card_image_name('noxious revival'/'NPH', 'noxious revival').
card_uid('noxious revival'/'NPH', 'NPH:Noxious Revival:noxious revival').
card_rarity('noxious revival'/'NPH', 'Uncommon').
card_artist('noxious revival'/'NPH', 'Matt Stewart').
card_number('noxious revival'/'NPH', '118').
card_flavor_text('noxious revival'/'NPH', '\"Dead or alive, my creations are stronger than Jin-Gitaxias\'s septic minions.\"\n—Vorinclex, Voice of Hunger').
card_multiverse_id('noxious revival'/'NPH', '230067').
card_watermark('noxious revival'/'NPH', 'Phyrexian').

card_in_set('numbing dose', 'NPH').
card_original_type('numbing dose'/'NPH', 'Enchantment — Aura').
card_original_text('numbing dose'/'NPH', 'Enchant artifact or creature\nEnchanted permanent doesn\'t untap during its controller\'s untap step.\nAt the beginning of the upkeep of enchanted permanent\'s controller, that player loses 1 life.').
card_first_print('numbing dose', 'NPH').
card_image_name('numbing dose'/'NPH', 'numbing dose').
card_uid('numbing dose'/'NPH', 'NPH:Numbing Dose:numbing dose').
card_rarity('numbing dose'/'NPH', 'Common').
card_artist('numbing dose'/'NPH', 'Brad Rigney').
card_number('numbing dose'/'NPH', '40').
card_multiverse_id('numbing dose'/'NPH', '218040').
card_watermark('numbing dose'/'NPH', 'Phyrexian').

card_in_set('ogre menial', 'NPH').
card_original_type('ogre menial'/'NPH', 'Creature — Ogre').
card_original_text('ogre menial'/'NPH', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\n{R}: Ogre Menial gets +1/+0 until end of turn.').
card_first_print('ogre menial', 'NPH').
card_image_name('ogre menial'/'NPH', 'ogre menial').
card_uid('ogre menial'/'NPH', 'NPH:Ogre Menial:ogre menial').
card_rarity('ogre menial'/'NPH', 'Common').
card_artist('ogre menial'/'NPH', 'David Rapoza').
card_number('ogre menial'/'NPH', '89').
card_multiverse_id('ogre menial'/'NPH', '233082').
card_watermark('ogre menial'/'NPH', 'Phyrexian').

card_in_set('omen machine', 'NPH').
card_original_type('omen machine'/'NPH', 'Artifact').
card_original_text('omen machine'/'NPH', 'Players can\'t draw cards.\nAt the beginning of each player\'s draw step, that player exiles the top card of his or her library. If it\'s a land card, the player puts it onto the battlefield. Otherwise, the player casts it without paying its mana cost if able.').
card_first_print('omen machine', 'NPH').
card_image_name('omen machine'/'NPH', 'omen machine').
card_uid('omen machine'/'NPH', 'NPH:Omen Machine:omen machine').
card_rarity('omen machine'/'NPH', 'Rare').
card_artist('omen machine'/'NPH', 'David Rapoza').
card_number('omen machine'/'NPH', '148').
card_multiverse_id('omen machine'/'NPH', '233042').
card_watermark('omen machine'/'NPH', 'Phyrexian').

card_in_set('parasitic implant', 'NPH').
card_original_type('parasitic implant'/'NPH', 'Enchantment — Aura').
card_original_text('parasitic implant'/'NPH', 'Enchant creature\nAt the beginning of your upkeep, enchanted creature\'s controller sacrifices it and you put a 1/1 colorless Myr artifact creature token onto the battlefield.').
card_first_print('parasitic implant', 'NPH').
card_image_name('parasitic implant'/'NPH', 'parasitic implant').
card_uid('parasitic implant'/'NPH', 'NPH:Parasitic Implant:parasitic implant').
card_rarity('parasitic implant'/'NPH', 'Common').
card_artist('parasitic implant'/'NPH', 'Jason Felix').
card_number('parasitic implant'/'NPH', '67').
card_multiverse_id('parasitic implant'/'NPH', '194216').
card_watermark('parasitic implant'/'NPH', 'Phyrexian').

card_in_set('pestilent souleater', 'NPH').
card_original_type('pestilent souleater'/'NPH', 'Artifact Creature — Insect').
card_original_text('pestilent souleater'/'NPH', '{B/P}: Pestilent Souleater gains infect until end of turn. ({B/P} can be paid with either {B} or 2 life. A creature with infect deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_first_print('pestilent souleater', 'NPH').
card_image_name('pestilent souleater'/'NPH', 'pestilent souleater').
card_uid('pestilent souleater'/'NPH', 'NPH:Pestilent Souleater:pestilent souleater').
card_rarity('pestilent souleater'/'NPH', 'Common').
card_artist('pestilent souleater'/'NPH', 'Matt Stewart').
card_number('pestilent souleater'/'NPH', '149').
card_multiverse_id('pestilent souleater'/'NPH', '233085').
card_watermark('pestilent souleater'/'NPH', 'Phyrexian').

card_in_set('phyrexia\'s core', 'NPH').
card_original_type('phyrexia\'s core'/'NPH', 'Land').
card_original_text('phyrexia\'s core'/'NPH', '{T}: Add {1} to your mana pool.\n{1}, {T}, Sacrifice an artifact: You gain 1 life.').
card_first_print('phyrexia\'s core', 'NPH').
card_image_name('phyrexia\'s core'/'NPH', 'phyrexia\'s core').
card_uid('phyrexia\'s core'/'NPH', 'NPH:Phyrexia\'s Core:phyrexia\'s core').
card_rarity('phyrexia\'s core'/'NPH', 'Uncommon').
card_artist('phyrexia\'s core'/'NPH', 'Franz Vohwinkel').
card_number('phyrexia\'s core'/'NPH', '165').
card_flavor_text('phyrexia\'s core'/'NPH', '\"So even the heart of our world has succumbed.\"\n—Koth of the Hammer').
card_multiverse_id('phyrexia\'s core'/'NPH', '218015').
card_watermark('phyrexia\'s core'/'NPH', 'Phyrexian').

card_in_set('phyrexian hulk', 'NPH').
card_original_type('phyrexian hulk'/'NPH', 'Artifact Creature — Golem').
card_original_text('phyrexian hulk'/'NPH', '').
card_image_name('phyrexian hulk'/'NPH', 'phyrexian hulk').
card_uid('phyrexian hulk'/'NPH', 'NPH:Phyrexian Hulk:phyrexian hulk').
card_rarity('phyrexian hulk'/'NPH', 'Common').
card_artist('phyrexian hulk'/'NPH', 'Steven Belledin').
card_number('phyrexian hulk'/'NPH', '150').
card_flavor_text('phyrexian hulk'/'NPH', 'An invasion weapon of ages past, the glistening oil contained the blueprints of countless atrocities.').
card_multiverse_id('phyrexian hulk'/'NPH', '214379').
card_watermark('phyrexian hulk'/'NPH', 'Phyrexian').

card_in_set('phyrexian ingester', 'NPH').
card_original_type('phyrexian ingester'/'NPH', 'Creature — Beast').
card_original_text('phyrexian ingester'/'NPH', 'Imprint — When Phyrexian Ingester enters the battlefield, you may exile target nontoken creature.\nPhyrexian Ingester gets +X/+Y, where X is the exiled creature card\'s power and Y is its toughness.').
card_first_print('phyrexian ingester', 'NPH').
card_image_name('phyrexian ingester'/'NPH', 'phyrexian ingester').
card_uid('phyrexian ingester'/'NPH', 'NPH:Phyrexian Ingester:phyrexian ingester').
card_rarity('phyrexian ingester'/'NPH', 'Rare').
card_artist('phyrexian ingester'/'NPH', 'Chris Rahn').
card_number('phyrexian ingester'/'NPH', '41').
card_multiverse_id('phyrexian ingester'/'NPH', '214384').
card_watermark('phyrexian ingester'/'NPH', 'Phyrexian').

card_in_set('phyrexian metamorph', 'NPH').
card_original_type('phyrexian metamorph'/'NPH', 'Artifact Creature — Shapeshifter').
card_original_text('phyrexian metamorph'/'NPH', '({U/P} can be paid with either {U} or 2 life.)\nYou may have Phyrexian Metamorph enter the battlefield as a copy of any artifact or creature on the battlefield, except it\'s an artifact in addition to its other types.').
card_image_name('phyrexian metamorph'/'NPH', 'phyrexian metamorph').
card_uid('phyrexian metamorph'/'NPH', 'NPH:Phyrexian Metamorph:phyrexian metamorph').
card_rarity('phyrexian metamorph'/'NPH', 'Rare').
card_artist('phyrexian metamorph'/'NPH', 'Jana Schirmer & Johannes Voss').
card_number('phyrexian metamorph'/'NPH', '42').
card_multiverse_id('phyrexian metamorph'/'NPH', '214375').
card_watermark('phyrexian metamorph'/'NPH', 'Phyrexian').

card_in_set('phyrexian obliterator', 'NPH').
card_original_type('phyrexian obliterator'/'NPH', 'Creature — Horror').
card_original_text('phyrexian obliterator'/'NPH', 'Trample\nWhenever a source deals damage to Phyrexian Obliterator, that source\'s controller sacrifices that many permanents.').
card_first_print('phyrexian obliterator', 'NPH').
card_image_name('phyrexian obliterator'/'NPH', 'phyrexian obliterator').
card_uid('phyrexian obliterator'/'NPH', 'NPH:Phyrexian Obliterator:phyrexian obliterator').
card_rarity('phyrexian obliterator'/'NPH', 'Mythic Rare').
card_artist('phyrexian obliterator'/'NPH', 'Todd Lockwood').
card_number('phyrexian obliterator'/'NPH', '68').
card_flavor_text('phyrexian obliterator'/'NPH', '\"Behold blessed perfection.\"\n—Sheoldred, Whispering One').
card_multiverse_id('phyrexian obliterator'/'NPH', '214386').
card_watermark('phyrexian obliterator'/'NPH', 'Phyrexian').

card_in_set('phyrexian swarmlord', 'NPH').
card_original_type('phyrexian swarmlord'/'NPH', 'Creature — Insect Horror').
card_original_text('phyrexian swarmlord'/'NPH', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nAt the beginning of your upkeep, put a 1/1 green Insect creature token with infect onto the battlefield for each poison counter your opponents have.').
card_first_print('phyrexian swarmlord', 'NPH').
card_image_name('phyrexian swarmlord'/'NPH', 'phyrexian swarmlord').
card_uid('phyrexian swarmlord'/'NPH', 'NPH:Phyrexian Swarmlord:phyrexian swarmlord').
card_rarity('phyrexian swarmlord'/'NPH', 'Rare').
card_artist('phyrexian swarmlord'/'NPH', 'Svetlin Velinov').
card_number('phyrexian swarmlord'/'NPH', '119').
card_multiverse_id('phyrexian swarmlord'/'NPH', '218086').
card_watermark('phyrexian swarmlord'/'NPH', 'Phyrexian').

card_in_set('phyrexian unlife', 'NPH').
card_original_type('phyrexian unlife'/'NPH', 'Enchantment').
card_original_text('phyrexian unlife'/'NPH', 'You don\'t lose the game for having 0 or less life.\nAs long as you have 0 or less life, all damage is dealt to you as though its source had infect. (Damage is dealt to you in the form of poison counters.)').
card_first_print('phyrexian unlife', 'NPH').
card_image_name('phyrexian unlife'/'NPH', 'phyrexian unlife').
card_uid('phyrexian unlife'/'NPH', 'NPH:Phyrexian Unlife:phyrexian unlife').
card_rarity('phyrexian unlife'/'NPH', 'Rare').
card_artist('phyrexian unlife'/'NPH', 'Jason Chan').
card_number('phyrexian unlife'/'NPH', '18').
card_multiverse_id('phyrexian unlife'/'NPH', '218058').
card_watermark('phyrexian unlife'/'NPH', 'Phyrexian').

card_in_set('pith driller', 'NPH').
card_original_type('pith driller'/'NPH', 'Artifact Creature — Horror').
card_original_text('pith driller'/'NPH', '({B/P} can be paid with either {B} or 2 life.)\nWhen Pith Driller enters the battlefield, put a -1/-1 counter on target creature.').
card_first_print('pith driller', 'NPH').
card_image_name('pith driller'/'NPH', 'pith driller').
card_uid('pith driller'/'NPH', 'NPH:Pith Driller:pith driller').
card_rarity('pith driller'/'NPH', 'Common').
card_artist('pith driller'/'NPH', 'Nils Hamm').
card_number('pith driller'/'NPH', '69').
card_flavor_text('pith driller'/'NPH', 'After boring up through the crust of Mirrodin, it turned its expertise upon the surface dwellers.').
card_multiverse_id('pith driller'/'NPH', '230079').
card_watermark('pith driller'/'NPH', 'Phyrexian').

card_in_set('plains', 'NPH').
card_original_type('plains'/'NPH', 'Basic Land — Plains').
card_original_text('plains'/'NPH', 'W').
card_image_name('plains'/'NPH', 'plains1').
card_uid('plains'/'NPH', 'NPH:Plains:plains1').
card_rarity('plains'/'NPH', 'Basic Land').
card_artist('plains'/'NPH', 'James Paick').
card_number('plains'/'NPH', '166').
card_multiverse_id('plains'/'NPH', '227552').

card_in_set('plains', 'NPH').
card_original_type('plains'/'NPH', 'Basic Land — Plains').
card_original_text('plains'/'NPH', 'W').
card_image_name('plains'/'NPH', 'plains2').
card_uid('plains'/'NPH', 'NPH:Plains:plains2').
card_rarity('plains'/'NPH', 'Basic Land').
card_artist('plains'/'NPH', 'James Paick').
card_number('plains'/'NPH', '167').
card_multiverse_id('plains'/'NPH', '227518').

card_in_set('porcelain legionnaire', 'NPH').
card_original_type('porcelain legionnaire'/'NPH', 'Artifact Creature — Soldier').
card_original_text('porcelain legionnaire'/'NPH', '({W/P} can be paid with either {W} or 2 life.)\nFirst strike').
card_first_print('porcelain legionnaire', 'NPH').
card_image_name('porcelain legionnaire'/'NPH', 'porcelain legionnaire').
card_uid('porcelain legionnaire'/'NPH', 'NPH:Porcelain Legionnaire:porcelain legionnaire').
card_rarity('porcelain legionnaire'/'NPH', 'Common').
card_artist('porcelain legionnaire'/'NPH', 'Eric Deschamps').
card_number('porcelain legionnaire'/'NPH', '19').
card_flavor_text('porcelain legionnaire'/'NPH', '\"My new design has lightweight plating specially cultivated from heretics\' spare tissues.\"\n—Izathel, priest of the Annex').
card_multiverse_id('porcelain legionnaire'/'NPH', '218043').
card_watermark('porcelain legionnaire'/'NPH', 'Phyrexian').

card_in_set('postmortem lunge', 'NPH').
card_original_type('postmortem lunge'/'NPH', 'Sorcery').
card_original_text('postmortem lunge'/'NPH', '({B/P} can be paid with either {B} or 2 life.)\nReturn target creature card with converted mana cost X from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step.').
card_first_print('postmortem lunge', 'NPH').
card_image_name('postmortem lunge'/'NPH', 'postmortem lunge').
card_uid('postmortem lunge'/'NPH', 'NPH:Postmortem Lunge:postmortem lunge').
card_rarity('postmortem lunge'/'NPH', 'Uncommon').
card_artist('postmortem lunge'/'NPH', 'Daarken').
card_number('postmortem lunge'/'NPH', '70').
card_multiverse_id('postmortem lunge'/'NPH', '233054').
card_watermark('postmortem lunge'/'NPH', 'Phyrexian').

card_in_set('praetor\'s grasp', 'NPH').
card_original_type('praetor\'s grasp'/'NPH', 'Sorcery').
card_original_text('praetor\'s grasp'/'NPH', 'Search target opponent\'s library for a card and exile it face down. Then that player shuffles his or her library. You may look at and play that card for as long as it remains exiled.').
card_first_print('praetor\'s grasp', 'NPH').
card_image_name('praetor\'s grasp'/'NPH', 'praetor\'s grasp').
card_uid('praetor\'s grasp'/'NPH', 'NPH:Praetor\'s Grasp:praetor\'s grasp').
card_rarity('praetor\'s grasp'/'NPH', 'Rare').
card_artist('praetor\'s grasp'/'NPH', 'Steve Argyle').
card_number('praetor\'s grasp'/'NPH', '71').
card_flavor_text('praetor\'s grasp'/'NPH', 'Sheoldred weaves every thread of information into a noose to hang her enemies.').
card_multiverse_id('praetor\'s grasp'/'NPH', '218050').
card_watermark('praetor\'s grasp'/'NPH', 'Phyrexian').

card_in_set('priest of urabrask', 'NPH').
card_original_type('priest of urabrask'/'NPH', 'Creature — Human Cleric').
card_original_text('priest of urabrask'/'NPH', 'When Priest of Urabrask enters the battlefield, add {R}{R}{R} to your mana pool.').
card_image_name('priest of urabrask'/'NPH', 'priest of urabrask').
card_uid('priest of urabrask'/'NPH', 'NPH:Priest of Urabrask:priest of urabrask').
card_rarity('priest of urabrask'/'NPH', 'Uncommon').
card_artist('priest of urabrask'/'NPH', 'Kev Walker').
card_number('priest of urabrask'/'NPH', '90').
card_flavor_text('priest of urabrask'/'NPH', 'Even in New Phyrexia, red mana sparks glimmers of individualism, passion, and freedom.').
card_multiverse_id('priest of urabrask'/'NPH', '218037').
card_watermark('priest of urabrask'/'NPH', 'Phyrexian').

card_in_set('pristine talisman', 'NPH').
card_original_type('pristine talisman'/'NPH', 'Artifact').
card_original_text('pristine talisman'/'NPH', '{T}: Add {1} to your mana pool. You gain 1 life.').
card_image_name('pristine talisman'/'NPH', 'pristine talisman').
card_uid('pristine talisman'/'NPH', 'NPH:Pristine Talisman:pristine talisman').
card_rarity('pristine talisman'/'NPH', 'Common').
card_artist('pristine talisman'/'NPH', 'Matt Cavotta').
card_number('pristine talisman'/'NPH', '151').
card_flavor_text('pristine talisman'/'NPH', '\"Tools and artisans can be destroyed, but the act of creation is inviolate.\"\n—Elspeth Tirel').
card_multiverse_id('pristine talisman'/'NPH', '233074').
card_watermark('pristine talisman'/'NPH', 'Mirran').

card_in_set('psychic barrier', 'NPH').
card_original_type('psychic barrier'/'NPH', 'Instant').
card_original_text('psychic barrier'/'NPH', 'Counter target creature spell. Its controller loses 1 life.').
card_first_print('psychic barrier', 'NPH').
card_image_name('psychic barrier'/'NPH', 'psychic barrier').
card_uid('psychic barrier'/'NPH', 'NPH:Psychic Barrier:psychic barrier').
card_rarity('psychic barrier'/'NPH', 'Common').
card_artist('psychic barrier'/'NPH', 'Dan Scott').
card_number('psychic barrier'/'NPH', '43').
card_flavor_text('psychic barrier'/'NPH', '\"I tolerate Phyrexians because the dragon requires it. But I have no patience for uninvited guests.\"\n—Tezzeret').
card_multiverse_id('psychic barrier'/'NPH', '218016').
card_watermark('psychic barrier'/'NPH', 'Phyrexian').

card_in_set('psychic surgery', 'NPH').
card_original_type('psychic surgery'/'NPH', 'Enchantment').
card_original_text('psychic surgery'/'NPH', 'Whenever an opponent shuffles his or her library, you may look at the top two cards of that library. You may exile one of those cards. Then put the rest on top of that library in any order.').
card_first_print('psychic surgery', 'NPH').
card_image_name('psychic surgery'/'NPH', 'psychic surgery').
card_uid('psychic surgery'/'NPH', 'NPH:Psychic Surgery:psychic surgery').
card_rarity('psychic surgery'/'NPH', 'Rare').
card_artist('psychic surgery'/'NPH', 'Anthony Francisco').
card_number('psychic surgery'/'NPH', '44').
card_multiverse_id('psychic surgery'/'NPH', '214347').
card_watermark('psychic surgery'/'NPH', 'Phyrexian').

card_in_set('puresteel paladin', 'NPH').
card_original_type('puresteel paladin'/'NPH', 'Creature — Human Knight').
card_original_text('puresteel paladin'/'NPH', 'Whenever an Equipment enters the battlefield under your control, you may draw a card.\nMetalcraft — Equipment you control have equip {0} as long as you control three or more artifacts.').
card_first_print('puresteel paladin', 'NPH').
card_image_name('puresteel paladin'/'NPH', 'puresteel paladin').
card_uid('puresteel paladin'/'NPH', 'NPH:Puresteel Paladin:puresteel paladin').
card_rarity('puresteel paladin'/'NPH', 'Rare').
card_artist('puresteel paladin'/'NPH', 'Jason Chan').
card_number('puresteel paladin'/'NPH', '20').
card_multiverse_id('puresteel paladin'/'NPH', '227504').
card_watermark('puresteel paladin'/'NPH', 'Mirran').

card_in_set('rage extractor', 'NPH').
card_original_type('rage extractor'/'NPH', 'Artifact').
card_original_text('rage extractor'/'NPH', '({R/P} can be paid with either {R} or 2 life.)\nWhenever you cast a spell with p in its mana cost, Rage Extractor deals damage equal to that spell\'s converted mana cost to target creature or player.').
card_first_print('rage extractor', 'NPH').
card_image_name('rage extractor'/'NPH', 'rage extractor').
card_uid('rage extractor'/'NPH', 'NPH:Rage Extractor:rage extractor').
card_rarity('rage extractor'/'NPH', 'Uncommon').
card_artist('rage extractor'/'NPH', 'Raymond Swanland').
card_number('rage extractor'/'NPH', '91').
card_flavor_text('rage extractor'/'NPH', 'New Phyrexia is an engine that both consumes and creates malice.').
card_multiverse_id('rage extractor'/'NPH', '214385').
card_watermark('rage extractor'/'NPH', 'Phyrexian').

card_in_set('razor swine', 'NPH').
card_original_type('razor swine'/'NPH', 'Creature — Boar').
card_original_text('razor swine'/'NPH', 'First strike\nInfect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_first_print('razor swine', 'NPH').
card_image_name('razor swine'/'NPH', 'razor swine').
card_uid('razor swine'/'NPH', 'NPH:Razor Swine:razor swine').
card_rarity('razor swine'/'NPH', 'Common').
card_artist('razor swine'/'NPH', 'Dave Allsop').
card_number('razor swine'/'NPH', '92').
card_flavor_text('razor swine'/'NPH', '\"They aren\'t helping the forges. Send them to the surface.\"\n—Furnace boss, sector 12').
card_multiverse_id('razor swine'/'NPH', '218022').
card_watermark('razor swine'/'NPH', 'Phyrexian').

card_in_set('reaper of sheoldred', 'NPH').
card_original_type('reaper of sheoldred'/'NPH', 'Creature — Horror').
card_original_text('reaper of sheoldred'/'NPH', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nWhenever a source deals damage to Reaper of Sheoldred, that source\'s controller gets a poison counter.').
card_first_print('reaper of sheoldred', 'NPH').
card_image_name('reaper of sheoldred'/'NPH', 'reaper of sheoldred').
card_uid('reaper of sheoldred'/'NPH', 'NPH:Reaper of Sheoldred:reaper of sheoldred').
card_rarity('reaper of sheoldred'/'NPH', 'Uncommon').
card_artist('reaper of sheoldred'/'NPH', 'Stephan Martiniere').
card_number('reaper of sheoldred'/'NPH', '72').
card_multiverse_id('reaper of sheoldred'/'NPH', '214374').
card_watermark('reaper of sheoldred'/'NPH', 'Phyrexian').

card_in_set('remember the fallen', 'NPH').
card_original_type('remember the fallen'/'NPH', 'Sorcery').
card_original_text('remember the fallen'/'NPH', 'Choose one or both — Return target creature card from your graveyard to your hand; and/or return target artifact card from your graveyard to your hand.').
card_first_print('remember the fallen', 'NPH').
card_image_name('remember the fallen'/'NPH', 'remember the fallen').
card_uid('remember the fallen'/'NPH', 'NPH:Remember the Fallen:remember the fallen').
card_rarity('remember the fallen'/'NPH', 'Common').
card_artist('remember the fallen'/'NPH', 'Eric Deschamps').
card_number('remember the fallen'/'NPH', '21').
card_flavor_text('remember the fallen'/'NPH', 'When the Mirrans had fallen, Planeswalkers carried the burden of remembrance.').
card_multiverse_id('remember the fallen'/'NPH', '218078').
card_watermark('remember the fallen'/'NPH', 'Mirran').

card_in_set('rotted hystrix', 'NPH').
card_original_type('rotted hystrix'/'NPH', 'Creature — Beast').
card_original_text('rotted hystrix'/'NPH', '').
card_first_print('rotted hystrix', 'NPH').
card_image_name('rotted hystrix'/'NPH', 'rotted hystrix').
card_uid('rotted hystrix'/'NPH', 'NPH:Rotted Hystrix:rotted hystrix').
card_rarity('rotted hystrix'/'NPH', 'Common').
card_artist('rotted hystrix'/'NPH', 'Dave Allsop').
card_number('rotted hystrix'/'NPH', '120').
card_flavor_text('rotted hystrix'/'NPH', 'Vorinclex had no grand plan. The oil did its own work, evolving creatures into worthy predators.').
card_multiverse_id('rotted hystrix'/'NPH', '233064').
card_watermark('rotted hystrix'/'NPH', 'Phyrexian').

card_in_set('ruthless invasion', 'NPH').
card_original_type('ruthless invasion'/'NPH', 'Sorcery').
card_original_text('ruthless invasion'/'NPH', '({R/P} can be paid with either {R} or 2 life.)\nNonartifact creatures can\'t block this turn.').
card_first_print('ruthless invasion', 'NPH').
card_image_name('ruthless invasion'/'NPH', 'ruthless invasion').
card_uid('ruthless invasion'/'NPH', 'NPH:Ruthless Invasion:ruthless invasion').
card_rarity('ruthless invasion'/'NPH', 'Common').
card_artist('ruthless invasion'/'NPH', 'Svetlin Velinov').
card_number('ruthless invasion'/'NPH', '93').
card_flavor_text('ruthless invasion'/'NPH', '\"For the survival of our people, every conflict must now end in retreat.\"\n—Pythor, Mirran resistance').
card_multiverse_id('ruthless invasion'/'NPH', '227528').
card_watermark('ruthless invasion'/'NPH', 'Phyrexian').

card_in_set('scrapyard salvo', 'NPH').
card_original_type('scrapyard salvo'/'NPH', 'Sorcery').
card_original_text('scrapyard salvo'/'NPH', 'Scrapyard Salvo deals damage to target player equal to the number of artifact cards in your graveyard.').
card_first_print('scrapyard salvo', 'NPH').
card_image_name('scrapyard salvo'/'NPH', 'scrapyard salvo').
card_uid('scrapyard salvo'/'NPH', 'NPH:Scrapyard Salvo:scrapyard salvo').
card_rarity('scrapyard salvo'/'NPH', 'Common').
card_artist('scrapyard salvo'/'NPH', 'Austin Hsu').
card_number('scrapyard salvo'/'NPH', '94').
card_flavor_text('scrapyard salvo'/'NPH', '\"Squealstokers! Build me a glorious pile of Mirran metal. Then add yourselves to the pile.\"\n—Furnace boss, sector 11').
card_multiverse_id('scrapyard salvo'/'NPH', '218019').
card_watermark('scrapyard salvo'/'NPH', 'Phyrexian').

card_in_set('sensor splicer', 'NPH').
card_original_type('sensor splicer'/'NPH', 'Creature — Artificer').
card_original_text('sensor splicer'/'NPH', 'When Sensor Splicer enters the battlefield, put a 3/3 colorless Golem artifact creature token onto the battlefield.\nGolem creatures you control have vigilance.').
card_first_print('sensor splicer', 'NPH').
card_image_name('sensor splicer'/'NPH', 'sensor splicer').
card_uid('sensor splicer'/'NPH', 'NPH:Sensor Splicer:sensor splicer').
card_rarity('sensor splicer'/'NPH', 'Common').
card_artist('sensor splicer'/'NPH', 'Izzy').
card_number('sensor splicer'/'NPH', '22').
card_multiverse_id('sensor splicer'/'NPH', '233038').
card_watermark('sensor splicer'/'NPH', 'Phyrexian').

card_in_set('shattered angel', 'NPH').
card_original_type('shattered angel'/'NPH', 'Creature — Angel').
card_original_text('shattered angel'/'NPH', 'Flying\nWhenever a land enters the battlefield under an opponent\'s control, you may gain 3 life.').
card_first_print('shattered angel', 'NPH').
card_image_name('shattered angel'/'NPH', 'shattered angel').
card_uid('shattered angel'/'NPH', 'NPH:Shattered Angel:shattered angel').
card_rarity('shattered angel'/'NPH', 'Uncommon').
card_artist('shattered angel'/'NPH', 'Kev Walker').
card_number('shattered angel'/'NPH', '23').
card_flavor_text('shattered angel'/'NPH', 'She preaches the blessings of the Machine Orthodoxy.').
card_multiverse_id('shattered angel'/'NPH', '233059').
card_watermark('shattered angel'/'NPH', 'Phyrexian').

card_in_set('sheoldred, whispering one', 'NPH').
card_original_type('sheoldred, whispering one'/'NPH', 'Legendary Creature — Praetor').
card_original_text('sheoldred, whispering one'/'NPH', 'Swampwalk\nAt the beginning of your upkeep, return target creature card from your graveyard to the battlefield.\nAt the beginning of each opponent\'s upkeep, that player sacrifices a creature.').
card_image_name('sheoldred, whispering one'/'NPH', 'sheoldred, whispering one').
card_uid('sheoldred, whispering one'/'NPH', 'NPH:Sheoldred, Whispering One:sheoldred, whispering one').
card_rarity('sheoldred, whispering one'/'NPH', 'Mythic Rare').
card_artist('sheoldred, whispering one'/'NPH', 'Jana Schirmer & Johannes Voss').
card_number('sheoldred, whispering one'/'NPH', '73').
card_multiverse_id('sheoldred, whispering one'/'NPH', '214382').
card_watermark('sheoldred, whispering one'/'NPH', 'Phyrexian').

card_in_set('shriek raptor', 'NPH').
card_original_type('shriek raptor'/'NPH', 'Creature — Bird').
card_original_text('shriek raptor'/'NPH', 'Flying\nInfect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_first_print('shriek raptor', 'NPH').
card_image_name('shriek raptor'/'NPH', 'shriek raptor').
card_uid('shriek raptor'/'NPH', 'NPH:Shriek Raptor:shriek raptor').
card_rarity('shriek raptor'/'NPH', 'Common').
card_artist('shriek raptor'/'NPH', 'Efrem Palacios').
card_number('shriek raptor'/'NPH', '24').
card_flavor_text('shriek raptor'/'NPH', '\"That\'s no glint hawk. The skies have changed for the worse.\"\n—Ria Ivor, White Sun partisan').
card_multiverse_id('shriek raptor'/'NPH', '217994').
card_watermark('shriek raptor'/'NPH', 'Phyrexian').

card_in_set('shrine of boundless growth', 'NPH').
card_original_type('shrine of boundless growth'/'NPH', 'Artifact').
card_original_text('shrine of boundless growth'/'NPH', 'At the beginning of your upkeep or whenever you cast a green spell, put a charge counter on Shrine of Boundless Growth.\n{T}, Sacrifice Shrine of Boundless Growth: Add {1} to your mana pool for each charge counter on Shrine of Boundless Growth.').
card_first_print('shrine of boundless growth', 'NPH').
card_image_name('shrine of boundless growth'/'NPH', 'shrine of boundless growth').
card_uid('shrine of boundless growth'/'NPH', 'NPH:Shrine of Boundless Growth:shrine of boundless growth').
card_rarity('shrine of boundless growth'/'NPH', 'Uncommon').
card_artist('shrine of boundless growth'/'NPH', 'Karl Kopinski').
card_number('shrine of boundless growth'/'NPH', '152').
card_multiverse_id('shrine of boundless growth'/'NPH', '217982').
card_watermark('shrine of boundless growth'/'NPH', 'Phyrexian').

card_in_set('shrine of burning rage', 'NPH').
card_original_type('shrine of burning rage'/'NPH', 'Artifact').
card_original_text('shrine of burning rage'/'NPH', 'At the beginning of your upkeep or whenever you cast a red spell, put a charge counter on Shrine of Burning Rage.\n{3}, {T}, Sacrifice Shrine of Burning Rage: Shrine of Burning Rage deals damage equal to the number of charge counters on it to target creature or player.').
card_image_name('shrine of burning rage'/'NPH', 'shrine of burning rage').
card_uid('shrine of burning rage'/'NPH', 'NPH:Shrine of Burning Rage:shrine of burning rage').
card_rarity('shrine of burning rage'/'NPH', 'Uncommon').
card_artist('shrine of burning rage'/'NPH', 'Dave Kendall').
card_number('shrine of burning rage'/'NPH', '153').
card_multiverse_id('shrine of burning rage'/'NPH', '218018').
card_watermark('shrine of burning rage'/'NPH', 'Phyrexian').

card_in_set('shrine of limitless power', 'NPH').
card_original_type('shrine of limitless power'/'NPH', 'Artifact').
card_original_text('shrine of limitless power'/'NPH', 'At the beginning of your upkeep or whenever you cast a black spell, put a charge counter on Shrine of Limitless Power.\n{4}, {T}, Sacrifice Shrine of Limitless Power: Target player discards a card for each charge counter on Shrine of Limitless Power.').
card_first_print('shrine of limitless power', 'NPH').
card_image_name('shrine of limitless power'/'NPH', 'shrine of limitless power').
card_uid('shrine of limitless power'/'NPH', 'NPH:Shrine of Limitless Power:shrine of limitless power').
card_rarity('shrine of limitless power'/'NPH', 'Uncommon').
card_artist('shrine of limitless power'/'NPH', 'Min Yum').
card_number('shrine of limitless power'/'NPH', '154').
card_multiverse_id('shrine of limitless power'/'NPH', '217980').
card_watermark('shrine of limitless power'/'NPH', 'Phyrexian').

card_in_set('shrine of loyal legions', 'NPH').
card_original_type('shrine of loyal legions'/'NPH', 'Artifact').
card_original_text('shrine of loyal legions'/'NPH', 'At the beginning of your upkeep or whenever you cast a white spell, put a charge counter on Shrine of Loyal Legions.\n{3}, {T}, Sacrifice Shrine of Loyal Legions: Put a 1/1 colorless Myr artifact creature token onto the battlefield for each charge counter on Shrine of Loyal Legions.').
card_first_print('shrine of loyal legions', 'NPH').
card_image_name('shrine of loyal legions'/'NPH', 'shrine of loyal legions').
card_uid('shrine of loyal legions'/'NPH', 'NPH:Shrine of Loyal Legions:shrine of loyal legions').
card_rarity('shrine of loyal legions'/'NPH', 'Uncommon').
card_artist('shrine of loyal legions'/'NPH', 'Igor Kieryluk').
card_number('shrine of loyal legions'/'NPH', '155').
card_multiverse_id('shrine of loyal legions'/'NPH', '206347').
card_watermark('shrine of loyal legions'/'NPH', 'Phyrexian').

card_in_set('shrine of piercing vision', 'NPH').
card_original_type('shrine of piercing vision'/'NPH', 'Artifact').
card_original_text('shrine of piercing vision'/'NPH', 'At the beginning of your upkeep or whenever you cast a blue spell, put a charge counter on Shrine of Piercing Vision.\n{T}, Sacrifice Shrine of Piercing Vision: Look at the top X cards of your library, where X is the number of charge counters on Shrine of Piercing Vision. Put one of those cards into your hand and the rest on the bottom of your library in any order.').
card_first_print('shrine of piercing vision', 'NPH').
card_image_name('shrine of piercing vision'/'NPH', 'shrine of piercing vision').
card_uid('shrine of piercing vision'/'NPH', 'NPH:Shrine of Piercing Vision:shrine of piercing vision').
card_rarity('shrine of piercing vision'/'NPH', 'Uncommon').
card_artist('shrine of piercing vision'/'NPH', 'Jana Schirmer & Johannes Voss').
card_number('shrine of piercing vision'/'NPH', '156').
card_multiverse_id('shrine of piercing vision'/'NPH', '194318').
card_watermark('shrine of piercing vision'/'NPH', 'Phyrexian').

card_in_set('sickleslicer', 'NPH').
card_original_type('sickleslicer'/'NPH', 'Artifact — Equipment').
card_original_text('sickleslicer'/'NPH', 'Living weapon (When this Equipment enters the battlefield, put a 0/0 black Germ creature token onto the battlefield, then attach this to it.)\nEquipped creature gets +2/+2.\nEquip {4}').
card_first_print('sickleslicer', 'NPH').
card_image_name('sickleslicer'/'NPH', 'sickleslicer').
card_uid('sickleslicer'/'NPH', 'NPH:Sickleslicer:sickleslicer').
card_rarity('sickleslicer'/'NPH', 'Uncommon').
card_artist('sickleslicer'/'NPH', 'Jason Felix').
card_number('sickleslicer'/'NPH', '157').
card_multiverse_id('sickleslicer'/'NPH', '214357').
card_watermark('sickleslicer'/'NPH', 'Phyrexian').

card_in_set('slag fiend', 'NPH').
card_original_type('slag fiend'/'NPH', 'Creature — Construct').
card_original_text('slag fiend'/'NPH', 'Slag Fiend\'s power and toughness are each equal to the number of artifact cards in all graveyards.').
card_first_print('slag fiend', 'NPH').
card_image_name('slag fiend'/'NPH', 'slag fiend').
card_uid('slag fiend'/'NPH', 'NPH:Slag Fiend:slag fiend').
card_rarity('slag fiend'/'NPH', 'Rare').
card_artist('slag fiend'/'NPH', 'Mike Bierek').
card_number('slag fiend'/'NPH', '95').
card_flavor_text('slag fiend'/'NPH', '\"Seal the furnace vents. Admit no others. We\'ll tend our forges without their tainted ways.\"\n—Decree of Urabrask').
card_multiverse_id('slag fiend'/'NPH', '218010').
card_watermark('slag fiend'/'NPH', 'Phyrexian').

card_in_set('slash panther', 'NPH').
card_original_type('slash panther'/'NPH', 'Artifact Creature — Cat').
card_original_text('slash panther'/'NPH', '({R/P} can be paid with either {R} or 2 life.)\nHaste').
card_first_print('slash panther', 'NPH').
card_image_name('slash panther'/'NPH', 'slash panther').
card_uid('slash panther'/'NPH', 'NPH:Slash Panther:slash panther').
card_rarity('slash panther'/'NPH', 'Common').
card_artist('slash panther'/'NPH', 'Matt Stewart').
card_number('slash panther'/'NPH', '96').
card_flavor_text('slash panther'/'NPH', 'It runs an endless circuit along the ruined roads of the soon-to-be-forgotten Mirran civilization.').
card_multiverse_id('slash panther'/'NPH', '230065').
card_watermark('slash panther'/'NPH', 'Phyrexian').

card_in_set('soul conduit', 'NPH').
card_original_type('soul conduit'/'NPH', 'Artifact').
card_original_text('soul conduit'/'NPH', '{6}, {T}: Two target players exchange life totals.').
card_first_print('soul conduit', 'NPH').
card_image_name('soul conduit'/'NPH', 'soul conduit').
card_uid('soul conduit'/'NPH', 'NPH:Soul Conduit:soul conduit').
card_rarity('soul conduit'/'NPH', 'Rare').
card_artist('soul conduit'/'NPH', 'Brad Rigney').
card_number('soul conduit'/'NPH', '158').
card_flavor_text('soul conduit'/'NPH', '\"You have an unhealthy attachment to your selfhood. I can help you with that.\"\n—Malcator, Executor of Synthesis').
card_multiverse_id('soul conduit'/'NPH', '233048').
card_watermark('soul conduit'/'NPH', 'Phyrexian').

card_in_set('spellskite', 'NPH').
card_original_type('spellskite'/'NPH', 'Artifact Creature — Horror').
card_original_text('spellskite'/'NPH', '{U/P}: Change a target of target spell or ability to Spellskite. ({U/P} can be paid with either {U} or 2 life.)').
card_first_print('spellskite', 'NPH').
card_image_name('spellskite'/'NPH', 'spellskite').
card_uid('spellskite'/'NPH', 'NPH:Spellskite:spellskite').
card_rarity('spellskite'/'NPH', 'Rare').
card_artist('spellskite'/'NPH', 'Chippy').
card_number('spellskite'/'NPH', '159').
card_flavor_text('spellskite'/'NPH', '\"Let\'s show Vorinclex that progress doesn\'t always need teeth or claws.\"\n—Malcator, Executor of Synthesis').
card_multiverse_id('spellskite'/'NPH', '217992').
card_watermark('spellskite'/'NPH', 'Phyrexian').

card_in_set('spinebiter', 'NPH').
card_original_type('spinebiter'/'NPH', 'Creature — Beast').
card_original_text('spinebiter'/'NPH', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nYou may have Spinebiter assign its combat damage as though it weren\'t blocked.').
card_first_print('spinebiter', 'NPH').
card_image_name('spinebiter'/'NPH', 'spinebiter').
card_uid('spinebiter'/'NPH', 'NPH:Spinebiter:spinebiter').
card_rarity('spinebiter'/'NPH', 'Uncommon').
card_artist('spinebiter'/'NPH', 'Jaime Jones').
card_number('spinebiter'/'NPH', '121').
card_multiverse_id('spinebiter'/'NPH', '230068').
card_watermark('spinebiter'/'NPH', 'Phyrexian').

card_in_set('spined thopter', 'NPH').
card_original_type('spined thopter'/'NPH', 'Artifact Creature — Thopter').
card_original_text('spined thopter'/'NPH', '({U/P} can be paid with either {U} or 2 life.)\nFlying').
card_first_print('spined thopter', 'NPH').
card_image_name('spined thopter'/'NPH', 'spined thopter').
card_uid('spined thopter'/'NPH', 'NPH:Spined Thopter:spined thopter').
card_rarity('spined thopter'/'NPH', 'Common').
card_artist('spined thopter'/'NPH', 'Pete Venters').
card_number('spined thopter'/'NPH', '45').
card_flavor_text('spined thopter'/'NPH', '\"I appreciate the depravity of noble designs turned to devious ends.\"\n—Tezzeret').
card_multiverse_id('spined thopter'/'NPH', '217990').
card_watermark('spined thopter'/'NPH', 'Phyrexian').

card_in_set('spire monitor', 'NPH').
card_original_type('spire monitor'/'NPH', 'Creature — Drake').
card_original_text('spire monitor'/'NPH', 'Flash (You may cast this spell any time you could cast an instant.)\nFlying').
card_first_print('spire monitor', 'NPH').
card_image_name('spire monitor'/'NPH', 'spire monitor').
card_uid('spire monitor'/'NPH', 'NPH:Spire Monitor:spire monitor').
card_rarity('spire monitor'/'NPH', 'Common').
card_artist('spire monitor'/'NPH', 'Daniel Ljunggren').
card_number('spire monitor'/'NPH', '46').
card_flavor_text('spire monitor'/'NPH', 'Each monitor is given an eye for each spire it is to guard.').
card_multiverse_id('spire monitor'/'NPH', '233053').
card_watermark('spire monitor'/'NPH', 'Phyrexian').

card_in_set('surge node', 'NPH').
card_original_type('surge node'/'NPH', 'Artifact').
card_original_text('surge node'/'NPH', 'Surge Node enters the battlefield with six charge counters on it.\n{1}, {T}, Remove a charge counter from Surge Node: Put a charge counter on target artifact.').
card_first_print('surge node', 'NPH').
card_image_name('surge node'/'NPH', 'surge node').
card_uid('surge node'/'NPH', 'NPH:Surge Node:surge node').
card_rarity('surge node'/'NPH', 'Uncommon').
card_artist('surge node'/'NPH', 'Lars Grant-West').
card_number('surge node'/'NPH', '160').
card_flavor_text('surge node'/'NPH', 'Without a foe to focus on, the machine priests had time to indulge in new sciences.').
card_multiverse_id('surge node'/'NPH', '194070').
card_watermark('surge node'/'NPH', 'Phyrexian').

card_in_set('surgical extraction', 'NPH').
card_original_type('surgical extraction'/'NPH', 'Instant').
card_original_text('surgical extraction'/'NPH', '({B/P} can be paid with either {B} or 2 life.)\nChoose target card in a graveyard other than a basic land card. Search its owner\'s graveyard, hand, and library for any number of cards with the same name as that card and exile them. Then that player shuffles his or her library.').
card_image_name('surgical extraction'/'NPH', 'surgical extraction').
card_uid('surgical extraction'/'NPH', 'NPH:Surgical Extraction:surgical extraction').
card_rarity('surgical extraction'/'NPH', 'Rare').
card_artist('surgical extraction'/'NPH', 'Steven Belledin').
card_number('surgical extraction'/'NPH', '74').
card_multiverse_id('surgical extraction'/'NPH', '233041').
card_watermark('surgical extraction'/'NPH', 'Phyrexian').

card_in_set('suture priest', 'NPH').
card_original_type('suture priest'/'NPH', 'Creature — Cleric').
card_original_text('suture priest'/'NPH', 'Whenever another creature enters the battlefield under your control, you may gain 1 life.\nWhenever a creature enters the battlefield under an opponent\'s control, you may have that player lose 1 life.').
card_image_name('suture priest'/'NPH', 'suture priest').
card_uid('suture priest'/'NPH', 'NPH:Suture Priest:suture priest').
card_rarity('suture priest'/'NPH', 'Common').
card_artist('suture priest'/'NPH', 'Igor Kieryluk').
card_number('suture priest'/'NPH', '25').
card_multiverse_id('suture priest'/'NPH', '217981').
card_watermark('suture priest'/'NPH', 'Phyrexian').

card_in_set('swamp', 'NPH').
card_original_type('swamp'/'NPH', 'Basic Land — Swamp').
card_original_text('swamp'/'NPH', 'B').
card_image_name('swamp'/'NPH', 'swamp1').
card_uid('swamp'/'NPH', 'NPH:Swamp:swamp1').
card_rarity('swamp'/'NPH', 'Basic Land').
card_artist('swamp'/'NPH', 'Lars Grant-West').
card_number('swamp'/'NPH', '170').
card_multiverse_id('swamp'/'NPH', '227519').

card_in_set('swamp', 'NPH').
card_original_type('swamp'/'NPH', 'Basic Land — Swamp').
card_original_text('swamp'/'NPH', 'B').
card_image_name('swamp'/'NPH', 'swamp2').
card_uid('swamp'/'NPH', 'NPH:Swamp:swamp2').
card_rarity('swamp'/'NPH', 'Basic Land').
card_artist('swamp'/'NPH', 'Lars Grant-West').
card_number('swamp'/'NPH', '171').
card_multiverse_id('swamp'/'NPH', '227521').

card_in_set('sword of war and peace', 'NPH').
card_original_type('sword of war and peace'/'NPH', 'Artifact — Equipment').
card_original_text('sword of war and peace'/'NPH', 'Equipped creature gets +2/+2 and has protection from red and from white.\nWhenever equipped creature deals combat damage to a player, Sword of War and Peace deals damage to that player equal to the number of cards in his or her hand and you gain 1 life for each card in your hand.\nEquip {2}').
card_first_print('sword of war and peace', 'NPH').
card_image_name('sword of war and peace'/'NPH', 'sword of war and peace').
card_uid('sword of war and peace'/'NPH', 'NPH:Sword of War and Peace:sword of war and peace').
card_rarity('sword of war and peace'/'NPH', 'Mythic Rare').
card_artist('sword of war and peace'/'NPH', 'Chris Rahn').
card_number('sword of war and peace'/'NPH', '161').
card_multiverse_id('sword of war and peace'/'NPH', '214368').
card_watermark('sword of war and peace'/'NPH', 'Mirran').

card_in_set('tezzeret\'s gambit', 'NPH').
card_original_type('tezzeret\'s gambit'/'NPH', 'Sorcery').
card_original_text('tezzeret\'s gambit'/'NPH', '({U/P} can be paid with either {U} or 2 life.)\nDraw two cards, then proliferate. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there.)').
card_first_print('tezzeret\'s gambit', 'NPH').
card_image_name('tezzeret\'s gambit'/'NPH', 'tezzeret\'s gambit').
card_uid('tezzeret\'s gambit'/'NPH', 'NPH:Tezzeret\'s Gambit:tezzeret\'s gambit').
card_rarity('tezzeret\'s gambit'/'NPH', 'Uncommon').
card_artist('tezzeret\'s gambit'/'NPH', 'Karl Kopinski').
card_number('tezzeret\'s gambit'/'NPH', '47').
card_multiverse_id('tezzeret\'s gambit'/'NPH', '227547').
card_watermark('tezzeret\'s gambit'/'NPH', 'Phyrexian').

card_in_set('thundering tanadon', 'NPH').
card_original_type('thundering tanadon'/'NPH', 'Artifact Creature — Beast').
card_original_text('thundering tanadon'/'NPH', '({G/P} can be paid with either {G} or 2 life.)\nTrample').
card_first_print('thundering tanadon', 'NPH').
card_image_name('thundering tanadon'/'NPH', 'thundering tanadon').
card_uid('thundering tanadon'/'NPH', 'NPH:Thundering Tanadon:thundering tanadon').
card_rarity('thundering tanadon'/'NPH', 'Common').
card_artist('thundering tanadon'/'NPH', 'Dan Scott').
card_number('thundering tanadon'/'NPH', '122').
card_flavor_text('thundering tanadon'/'NPH', '\"We do not need beakers and vials to test our predators.\"\n—Vorinclex, Voice of Hunger').
card_multiverse_id('thundering tanadon'/'NPH', '218077').
card_watermark('thundering tanadon'/'NPH', 'Phyrexian').

card_in_set('tormentor exarch', 'NPH').
card_original_type('tormentor exarch'/'NPH', 'Creature — Cleric').
card_original_text('tormentor exarch'/'NPH', 'When Tormentor Exarch enters the battlefield, choose one — Target creature gets +2/+0 until end of turn; or target creature gets -0/-2 until end of turn.').
card_first_print('tormentor exarch', 'NPH').
card_image_name('tormentor exarch'/'NPH', 'tormentor exarch').
card_uid('tormentor exarch'/'NPH', 'NPH:Tormentor Exarch:tormentor exarch').
card_rarity('tormentor exarch'/'NPH', 'Uncommon').
card_artist('tormentor exarch'/'NPH', 'Brad Rigney').
card_number('tormentor exarch'/'NPH', '97').
card_flavor_text('tormentor exarch'/'NPH', '\"Tend the molten slag, or be the molten slag.\"\n—Urabrask\'s enforcer').
card_multiverse_id('tormentor exarch'/'NPH', '218082').
card_watermark('tormentor exarch'/'NPH', 'Phyrexian').

card_in_set('torpor orb', 'NPH').
card_original_type('torpor orb'/'NPH', 'Artifact').
card_original_text('torpor orb'/'NPH', 'Creatures entering the battlefield don\'t cause abilities to trigger.').
card_first_print('torpor orb', 'NPH').
card_image_name('torpor orb'/'NPH', 'torpor orb').
card_uid('torpor orb'/'NPH', 'NPH:Torpor Orb:torpor orb').
card_rarity('torpor orb'/'NPH', 'Rare').
card_artist('torpor orb'/'NPH', 'Svetlin Velinov').
card_number('torpor orb'/'NPH', '162').
card_flavor_text('torpor orb'/'NPH', '\"Phyrexia is certainly dangerous, but I have to admire some of its innovations.\"\n—Tezzeret').
card_multiverse_id('torpor orb'/'NPH', '233069').
card_watermark('torpor orb'/'NPH', 'Phyrexian').

card_in_set('toxic nim', 'NPH').
card_original_type('toxic nim'/'NPH', 'Creature — Zombie').
card_original_text('toxic nim'/'NPH', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\n{B}: Regenerate Toxic Nim.').
card_first_print('toxic nim', 'NPH').
card_image_name('toxic nim'/'NPH', 'toxic nim').
card_uid('toxic nim'/'NPH', 'NPH:Toxic Nim:toxic nim').
card_rarity('toxic nim'/'NPH', 'Common').
card_artist('toxic nim'/'NPH', 'Karl Kopinski').
card_number('toxic nim'/'NPH', '75').
card_flavor_text('toxic nim'/'NPH', 'The nim have become roving killers marshaled by whichever thane holds tenuous leadership.').
card_multiverse_id('toxic nim'/'NPH', '233081').
card_watermark('toxic nim'/'NPH', 'Phyrexian').

card_in_set('trespassing souleater', 'NPH').
card_original_type('trespassing souleater'/'NPH', 'Artifact Creature — Construct').
card_original_text('trespassing souleater'/'NPH', '{U/P}: Trespassing Souleater is unblockable this turn. ({U/P} can be paid with either {U} or 2 life.)').
card_first_print('trespassing souleater', 'NPH').
card_image_name('trespassing souleater'/'NPH', 'trespassing souleater').
card_uid('trespassing souleater'/'NPH', 'NPH:Trespassing Souleater:trespassing souleater').
card_rarity('trespassing souleater'/'NPH', 'Common').
card_artist('trespassing souleater'/'NPH', 'Scott Chou').
card_number('trespassing souleater'/'NPH', '163').
card_flavor_text('trespassing souleater'/'NPH', '\"We thank the souleaters for surveilling our lives that we may be preserved from our own ignorance.\"\n—Drones\' hymn of gratitude').
card_multiverse_id('trespassing souleater'/'NPH', '233073').
card_watermark('trespassing souleater'/'NPH', 'Phyrexian').

card_in_set('triumph of the hordes', 'NPH').
card_original_type('triumph of the hordes'/'NPH', 'Sorcery').
card_original_text('triumph of the hordes'/'NPH', 'Until end of turn, creatures you control get +1/+1 and gain trample and infect. (Creatures with infect deal damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_first_print('triumph of the hordes', 'NPH').
card_image_name('triumph of the hordes'/'NPH', 'triumph of the hordes').
card_uid('triumph of the hordes'/'NPH', 'NPH:Triumph of the Hordes:triumph of the hordes').
card_rarity('triumph of the hordes'/'NPH', 'Uncommon').
card_artist('triumph of the hordes'/'NPH', 'Izzy').
card_number('triumph of the hordes'/'NPH', '123').
card_multiverse_id('triumph of the hordes'/'NPH', '218044').
card_watermark('triumph of the hordes'/'NPH', 'Phyrexian').

card_in_set('unwinding clock', 'NPH').
card_original_type('unwinding clock'/'NPH', 'Artifact').
card_original_text('unwinding clock'/'NPH', 'Untap all artifacts you control during each other player\'s untap step.').
card_first_print('unwinding clock', 'NPH').
card_image_name('unwinding clock'/'NPH', 'unwinding clock').
card_uid('unwinding clock'/'NPH', 'NPH:Unwinding Clock:unwinding clock').
card_rarity('unwinding clock'/'NPH', 'Rare').
card_artist('unwinding clock'/'NPH', 'Mike Bierek').
card_number('unwinding clock'/'NPH', '164').
card_flavor_text('unwinding clock'/'NPH', '\"The partisans have unearthed a strange artifact. None know its origin, but it gives them hope of a life beyond.\"\n—Elspeth\'s journal').
card_multiverse_id('unwinding clock'/'NPH', '218079').
card_watermark('unwinding clock'/'NPH', 'Mirran').

card_in_set('urabrask the hidden', 'NPH').
card_original_type('urabrask the hidden'/'NPH', 'Legendary Creature — Praetor').
card_original_text('urabrask the hidden'/'NPH', 'Creatures you control have haste.\nCreatures your opponents control enter the battlefield tapped.').
card_first_print('urabrask the hidden', 'NPH').
card_image_name('urabrask the hidden'/'NPH', 'urabrask the hidden').
card_uid('urabrask the hidden'/'NPH', 'NPH:Urabrask the Hidden:urabrask the hidden').
card_rarity('urabrask the hidden'/'NPH', 'Mythic Rare').
card_artist('urabrask the hidden'/'NPH', 'Brad Rigney').
card_number('urabrask the hidden'/'NPH', '98').
card_flavor_text('urabrask the hidden'/'NPH', 'When the Mirran resistance arrived, the furnace dwellers looked to Urabrask for guidance. His decree stunned the others: \"Let them be.\"').
card_multiverse_id('urabrask the hidden'/'NPH', '214378').
card_watermark('urabrask the hidden'/'NPH', 'Phyrexian').

card_in_set('vapor snag', 'NPH').
card_original_type('vapor snag'/'NPH', 'Instant').
card_original_text('vapor snag'/'NPH', 'Return target creature to its owner\'s hand. Its controller loses 1 life.').
card_first_print('vapor snag', 'NPH').
card_image_name('vapor snag'/'NPH', 'vapor snag').
card_uid('vapor snag'/'NPH', 'NPH:Vapor Snag:vapor snag').
card_rarity('vapor snag'/'NPH', 'Common').
card_artist('vapor snag'/'NPH', 'Raymond Swanland').
card_number('vapor snag'/'NPH', '48').
card_flavor_text('vapor snag'/'NPH', '\"This creature is inadequate. Send it to the splicers for innovation.\"\n—Malcator, Executor of Synthesis').
card_multiverse_id('vapor snag'/'NPH', '218017').
card_watermark('vapor snag'/'NPH', 'Phyrexian').

card_in_set('vault skirge', 'NPH').
card_original_type('vault skirge'/'NPH', 'Artifact Creature — Imp').
card_original_text('vault skirge'/'NPH', '({B/P} can be paid with either {B} or 2 life.)\nFlying\nLifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_image_name('vault skirge'/'NPH', 'vault skirge').
card_uid('vault skirge'/'NPH', 'NPH:Vault Skirge:vault skirge').
card_rarity('vault skirge'/'NPH', 'Common').
card_artist('vault skirge'/'NPH', 'Brad Rigney').
card_number('vault skirge'/'NPH', '76').
card_flavor_text('vault skirge'/'NPH', 'From the remnants of the dead, Geth forged a swarm to safeguard his throne.').
card_multiverse_id('vault skirge'/'NPH', '217984').
card_watermark('vault skirge'/'NPH', 'Phyrexian').

card_in_set('victorious destruction', 'NPH').
card_original_type('victorious destruction'/'NPH', 'Sorcery').
card_original_text('victorious destruction'/'NPH', 'Destroy target artifact or land. Its controller loses 1 life.').
card_first_print('victorious destruction', 'NPH').
card_image_name('victorious destruction'/'NPH', 'victorious destruction').
card_uid('victorious destruction'/'NPH', 'NPH:Victorious Destruction:victorious destruction').
card_rarity('victorious destruction'/'NPH', 'Common').
card_artist('victorious destruction'/'NPH', 'Jung Park').
card_number('victorious destruction'/'NPH', '99').
card_flavor_text('victorious destruction'/'NPH', '\"The refugees look at these structures with hope. We must relieve them of this burden.\"\n—Juex, tormentor exarch').
card_multiverse_id('victorious destruction'/'NPH', '227549').
card_watermark('victorious destruction'/'NPH', 'Phyrexian').

card_in_set('viral drake', 'NPH').
card_original_type('viral drake'/'NPH', 'Creature — Drake').
card_original_text('viral drake'/'NPH', 'Flying\nInfect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\n{3}{U}: Proliferate. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there.)').
card_first_print('viral drake', 'NPH').
card_image_name('viral drake'/'NPH', 'viral drake').
card_uid('viral drake'/'NPH', 'NPH:Viral Drake:viral drake').
card_rarity('viral drake'/'NPH', 'Uncommon').
card_artist('viral drake'/'NPH', 'Lars Grant-West').
card_number('viral drake'/'NPH', '49').
card_multiverse_id('viral drake'/'NPH', '233047').
card_watermark('viral drake'/'NPH', 'Phyrexian').

card_in_set('viridian betrayers', 'NPH').
card_original_type('viridian betrayers'/'NPH', 'Creature — Elf Warrior').
card_original_text('viridian betrayers'/'NPH', 'Viridian Betrayers has infect as long as an opponent is poisoned. (It deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_first_print('viridian betrayers', 'NPH').
card_image_name('viridian betrayers'/'NPH', 'viridian betrayers').
card_uid('viridian betrayers'/'NPH', 'NPH:Viridian Betrayers:viridian betrayers').
card_rarity('viridian betrayers'/'NPH', 'Common').
card_artist('viridian betrayers'/'NPH', 'Karl Kopinski').
card_number('viridian betrayers'/'NPH', '124').
card_flavor_text('viridian betrayers'/'NPH', 'To secure a place in Phyrexia\'s ranks, they hunted their surviving kin.').
card_multiverse_id('viridian betrayers'/'NPH', '214390').
card_watermark('viridian betrayers'/'NPH', 'Phyrexian').

card_in_set('viridian harvest', 'NPH').
card_original_type('viridian harvest'/'NPH', 'Enchantment — Aura').
card_original_text('viridian harvest'/'NPH', 'Enchant artifact\nWhen enchanted artifact is put into a graveyard, you gain 6 life.').
card_first_print('viridian harvest', 'NPH').
card_image_name('viridian harvest'/'NPH', 'viridian harvest').
card_uid('viridian harvest'/'NPH', 'NPH:Viridian Harvest:viridian harvest').
card_rarity('viridian harvest'/'NPH', 'Common').
card_artist('viridian harvest'/'NPH', 'Johann Bodin').
card_number('viridian harvest'/'NPH', '125').
card_flavor_text('viridian harvest'/'NPH', '\"In the midst of horror beyond imagining, I still see things that lift my heart.\"\n—Ezuri').
card_multiverse_id('viridian harvest'/'NPH', '214388').
card_watermark('viridian harvest'/'NPH', 'Mirran').

card_in_set('vital splicer', 'NPH').
card_original_type('vital splicer'/'NPH', 'Creature — Human Artificer').
card_original_text('vital splicer'/'NPH', 'When Vital Splicer enters the battlefield, put a 3/3 colorless Golem artifact creature token onto the battlefield.\n{1}: Regenerate target Golem you control.').
card_first_print('vital splicer', 'NPH').
card_image_name('vital splicer'/'NPH', 'vital splicer').
card_uid('vital splicer'/'NPH', 'NPH:Vital Splicer:vital splicer').
card_rarity('vital splicer'/'NPH', 'Uncommon').
card_artist('vital splicer'/'NPH', 'Daarken').
card_number('vital splicer'/'NPH', '126').
card_flavor_text('vital splicer'/'NPH', '\"For every one that falls, ten shall rise. The revelation of New Phyrexia is at hand.\"\n—Vorinclex, Voice of Hunger').
card_multiverse_id('vital splicer'/'NPH', '233079').
card_watermark('vital splicer'/'NPH', 'Phyrexian').

card_in_set('volt charge', 'NPH').
card_original_type('volt charge'/'NPH', 'Instant').
card_original_text('volt charge'/'NPH', 'Volt Charge deals 3 damage to target creature or player. Proliferate. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there.)').
card_first_print('volt charge', 'NPH').
card_image_name('volt charge'/'NPH', 'volt charge').
card_uid('volt charge'/'NPH', 'NPH:Volt Charge:volt charge').
card_rarity('volt charge'/'NPH', 'Common').
card_artist('volt charge'/'NPH', 'Jana Schirmer & Johannes Voss').
card_number('volt charge'/'NPH', '100').
card_multiverse_id('volt charge'/'NPH', '214381').
card_watermark('volt charge'/'NPH', 'Phyrexian').

card_in_set('vorinclex, voice of hunger', 'NPH').
card_original_type('vorinclex, voice of hunger'/'NPH', 'Legendary Creature — Praetor').
card_original_text('vorinclex, voice of hunger'/'NPH', 'Trample\nWhenever you tap a land for mana, add one mana to your mana pool of any type that land produced.\nWhenever an opponent taps a land for mana, that land doesn\'t untap during its controller\'s next untap step.').
card_first_print('vorinclex, voice of hunger', 'NPH').
card_image_name('vorinclex, voice of hunger'/'NPH', 'vorinclex, voice of hunger').
card_uid('vorinclex, voice of hunger'/'NPH', 'NPH:Vorinclex, Voice of Hunger:vorinclex, voice of hunger').
card_rarity('vorinclex, voice of hunger'/'NPH', 'Mythic Rare').
card_artist('vorinclex, voice of hunger'/'NPH', 'Karl Kopinski').
card_number('vorinclex, voice of hunger'/'NPH', '127').
card_multiverse_id('vorinclex, voice of hunger'/'NPH', '218002').
card_watermark('vorinclex, voice of hunger'/'NPH', 'Phyrexian').

card_in_set('vulshok refugee', 'NPH').
card_original_type('vulshok refugee'/'NPH', 'Creature — Human Warrior').
card_original_text('vulshok refugee'/'NPH', 'Protection from red').
card_first_print('vulshok refugee', 'NPH').
card_image_name('vulshok refugee'/'NPH', 'vulshok refugee').
card_uid('vulshok refugee'/'NPH', 'NPH:Vulshok Refugee:vulshok refugee').
card_rarity('vulshok refugee'/'NPH', 'Uncommon').
card_artist('vulshok refugee'/'NPH', 'Wayne Reynolds').
card_number('vulshok refugee'/'NPH', '101').
card_flavor_text('vulshok refugee'/'NPH', '\"I will carve my people\'s vengeance on the face of Phyrexia.\"').
card_multiverse_id('vulshok refugee'/'NPH', '218038').
card_watermark('vulshok refugee'/'NPH', 'Mirran').

card_in_set('war report', 'NPH').
card_original_type('war report'/'NPH', 'Instant').
card_original_text('war report'/'NPH', 'You gain life equal to the number of creatures on the battlefield plus the number of artifacts on the battlefield.').
card_first_print('war report', 'NPH').
card_image_name('war report'/'NPH', 'war report').
card_uid('war report'/'NPH', 'NPH:War Report:war report').
card_rarity('war report'/'NPH', 'Common').
card_artist('war report'/'NPH', 'Mike Bierek').
card_number('war report'/'NPH', '26').
card_flavor_text('war report'/'NPH', 'Underling Ethu\'s 263rd report read simply \"Yes, my lord. Overwhelmingly, my lord.\" This marked the end of the Mirran-Phyrexian War.').
card_multiverse_id('war report'/'NPH', '230078').
card_watermark('war report'/'NPH', 'Phyrexian').

card_in_set('whipflare', 'NPH').
card_original_type('whipflare'/'NPH', 'Sorcery').
card_original_text('whipflare'/'NPH', 'Whipflare deals 2 damage to each nonartifact creature.').
card_first_print('whipflare', 'NPH').
card_image_name('whipflare'/'NPH', 'whipflare').
card_uid('whipflare'/'NPH', 'NPH:Whipflare:whipflare').
card_rarity('whipflare'/'NPH', 'Uncommon').
card_artist('whipflare'/'NPH', 'Johann Bodin').
card_number('whipflare'/'NPH', '102').
card_flavor_text('whipflare'/'NPH', 'The slag-workers wasted no time in creating their own flare pulses to cleanse the area of the incompleat.').
card_multiverse_id('whipflare'/'NPH', '194383').
card_watermark('whipflare'/'NPH', 'Mirran').

card_in_set('whispering specter', 'NPH').
card_original_type('whispering specter'/'NPH', 'Creature — Specter').
card_original_text('whispering specter'/'NPH', 'Flying\nInfect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nWhenever Whispering Specter deals combat damage to a player, you may sacrifice it. If you do, that player discards a card for each poison counter he or she has.').
card_first_print('whispering specter', 'NPH').
card_image_name('whispering specter'/'NPH', 'whispering specter').
card_uid('whispering specter'/'NPH', 'NPH:Whispering Specter:whispering specter').
card_rarity('whispering specter'/'NPH', 'Uncommon').
card_artist('whispering specter'/'NPH', 'Jason Felix').
card_number('whispering specter'/'NPH', '77').
card_multiverse_id('whispering specter'/'NPH', '218045').
card_watermark('whispering specter'/'NPH', 'Phyrexian').

card_in_set('wing splicer', 'NPH').
card_original_type('wing splicer'/'NPH', 'Creature — Human Artificer').
card_original_text('wing splicer'/'NPH', 'When Wing Splicer enters the battlefield, put a 3/3 colorless Golem artifact creature token onto the battlefield.\nGolem creatures you control have flying.').
card_first_print('wing splicer', 'NPH').
card_image_name('wing splicer'/'NPH', 'wing splicer').
card_uid('wing splicer'/'NPH', 'NPH:Wing Splicer:wing splicer').
card_rarity('wing splicer'/'NPH', 'Uncommon').
card_artist('wing splicer'/'NPH', 'Kev Walker').
card_number('wing splicer'/'NPH', '50').
card_multiverse_id('wing splicer'/'NPH', '233037').
card_watermark('wing splicer'/'NPH', 'Phyrexian').

card_in_set('xenograft', 'NPH').
card_original_type('xenograft'/'NPH', 'Enchantment').
card_original_text('xenograft'/'NPH', 'As Xenograft enters the battlefield, choose a creature type.\nEach creature you control is the chosen type in addition to its other types.').
card_first_print('xenograft', 'NPH').
card_image_name('xenograft'/'NPH', 'xenograft').
card_uid('xenograft'/'NPH', 'NPH:Xenograft:xenograft').
card_rarity('xenograft'/'NPH', 'Rare').
card_artist('xenograft'/'NPH', 'Daniel Ljunggren').
card_number('xenograft'/'NPH', '51').
card_flavor_text('xenograft'/'NPH', '\"I despise Vorinclex and his slobberings about ‘evolution.\' Only I know true progress.\"\n—Jin-Gitaxias, Core Augur').
card_multiverse_id('xenograft'/'NPH', '227544').
card_watermark('xenograft'/'NPH', 'Phyrexian').
