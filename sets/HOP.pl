% Planechase

set('HOP').
set_name('HOP', 'Planechase').
set_release_date('HOP', '2009-09-04').
set_border('HOP', 'black').
set_type('HOP', 'planechase').

card_in_set('academy at tolaria west', 'HOP').
card_original_type('academy at tolaria west'/'HOP', 'Plane — Dominaria').
card_original_text('academy at tolaria west'/'HOP', 'At the beginning of your end step, if you have no cards in hand, draw seven cards.\nWhenever you roll {C}, discard your hand.').
card_first_print('academy at tolaria west', 'HOP').
card_image_name('academy at tolaria west'/'HOP', 'academy at tolaria west').
card_uid('academy at tolaria west'/'HOP', 'HOP:Academy at Tolaria West:academy at tolaria west').
card_rarity('academy at tolaria west'/'HOP', 'Common').
card_artist('academy at tolaria west'/'HOP', 'James Paick').
card_number('academy at tolaria west'/'HOP', '1').
card_multiverse_id('academy at tolaria west'/'HOP', '198073').

card_in_set('agyrem', 'HOP').
card_original_type('agyrem'/'HOP', 'Plane — Ravnica').
card_original_text('agyrem'/'HOP', 'Whenever a white creature is put into a graveyard from the battlefield, return it to the battlefield under its owner\'s control at the beginning of the next end step.\nWhenever a nonwhite creature is put into a graveyard from the battlefield, return it to its owner\'s hand at the beginning of the next end step.\nWhenever you roll {C}, creatures can\'t attack you until a player planeswalks.').
card_first_print('agyrem', 'HOP').
card_image_name('agyrem'/'HOP', 'agyrem').
card_uid('agyrem'/'HOP', 'HOP:Agyrem:agyrem').
card_rarity('agyrem'/'HOP', 'Common').
card_artist('agyrem'/'HOP', 'Todd Lockwood').
card_number('agyrem'/'HOP', '3').
card_multiverse_id('agyrem'/'HOP', '198090').

card_in_set('akroma\'s vengeance', 'HOP').
card_original_type('akroma\'s vengeance'/'HOP', 'Sorcery').
card_original_text('akroma\'s vengeance'/'HOP', 'Destroy all artifacts, creatures, and enchantments.\nCycling {3} ({3}, Discard this card: Draw a card.)').
card_image_name('akroma\'s vengeance'/'HOP', 'akroma\'s vengeance').
card_uid('akroma\'s vengeance'/'HOP', 'HOP:Akroma\'s Vengeance:akroma\'s vengeance').
card_rarity('akroma\'s vengeance'/'HOP', 'Rare').
card_artist('akroma\'s vengeance'/'HOP', 'Greg & Tim Hildebrandt').
card_number('akroma\'s vengeance'/'HOP', '1').
card_flavor_text('akroma\'s vengeance'/'HOP', 'Ixidor had only to imagine their ruin and Akroma made it so.').
card_multiverse_id('akroma\'s vengeance'/'HOP', '205366').

card_in_set('ancient den', 'HOP').
card_original_type('ancient den'/'HOP', 'Artifact Land').
card_original_text('ancient den'/'HOP', '(Ancient Den isn\'t a spell.)\n{T}: Add {W} to your mana pool.').
card_image_name('ancient den'/'HOP', 'ancient den').
card_uid('ancient den'/'HOP', 'HOP:Ancient Den:ancient den').
card_rarity('ancient den'/'HOP', 'Common').
card_artist('ancient den'/'HOP', 'Rob Alexander').
card_number('ancient den'/'HOP', '130').
card_flavor_text('ancient den'/'HOP', 'Taj-Nar, throne of Raksha Golden Cub, destined leader of the leonin prides.').
card_multiverse_id('ancient den'/'HOP', '205275').

card_in_set('arc lightning', 'HOP').
card_original_type('arc lightning'/'HOP', 'Sorcery').
card_original_text('arc lightning'/'HOP', 'Arc Lightning deals 3 damage divided as you choose among any number of target creatures and/or players.').
card_image_name('arc lightning'/'HOP', 'arc lightning').
card_uid('arc lightning'/'HOP', 'HOP:Arc Lightning:arc lightning').
card_rarity('arc lightning'/'HOP', 'Common').
card_artist('arc lightning'/'HOP', 'Andrew Goldhawk').
card_number('arc lightning'/'HOP', '46').
card_flavor_text('arc lightning'/'HOP', 'Rainclouds don\'t last long in Shiv, but that doesn\'t stop the lightning.').
card_multiverse_id('arc lightning'/'HOP', '205386').

card_in_set('arcbound crusher', 'HOP').
card_original_type('arcbound crusher'/'HOP', 'Artifact Creature — Juggernaut').
card_original_text('arcbound crusher'/'HOP', 'Trample\nWhenever another artifact enters the battlefield, put a +1/+1 counter on Arcbound Crusher.\nModular 1 (This enters the battlefield with a +1/+1 counter on it. When it\'s put into a graveyard, you may put its +1/+1 counters on target artifact creature.)').
card_image_name('arcbound crusher'/'HOP', 'arcbound crusher').
card_uid('arcbound crusher'/'HOP', 'HOP:Arcbound Crusher:arcbound crusher').
card_rarity('arcbound crusher'/'HOP', 'Uncommon').
card_artist('arcbound crusher'/'HOP', 'Michael Sutfin').
card_number('arcbound crusher'/'HOP', '105').
card_multiverse_id('arcbound crusher'/'HOP', '205335').

card_in_set('arcbound slith', 'HOP').
card_original_type('arcbound slith'/'HOP', 'Artifact Creature — Slith').
card_original_text('arcbound slith'/'HOP', 'Whenever Arcbound Slith deals combat damage to a player, put a +1/+1 counter on it.\nModular 1 (This enters the battlefield with a +1/+1 counter on it. When it\'s put into a graveyard, you may put its +1/+1 counters on target artifact creature.)').
card_image_name('arcbound slith'/'HOP', 'arcbound slith').
card_uid('arcbound slith'/'HOP', 'HOP:Arcbound Slith:arcbound slith').
card_rarity('arcbound slith'/'HOP', 'Uncommon').
card_artist('arcbound slith'/'HOP', 'Vance Kovacs').
card_number('arcbound slith'/'HOP', '106').
card_multiverse_id('arcbound slith'/'HOP', '205336').

card_in_set('arsenal thresher', 'HOP').
card_original_type('arsenal thresher'/'HOP', 'Artifact Creature — Construct').
card_original_text('arsenal thresher'/'HOP', 'As Arsenal Thresher enters the battlefield, you may reveal any number of other artifact cards from your hand. Arsenal Thresher enters the battlefield with a +1/+1 counter on it for each card revealed this way.').
card_image_name('arsenal thresher'/'HOP', 'arsenal thresher').
card_uid('arsenal thresher'/'HOP', 'HOP:Arsenal Thresher:arsenal thresher').
card_rarity('arsenal thresher'/'HOP', 'Common').
card_artist('arsenal thresher'/'HOP', 'Ralph Horsley').
card_number('arsenal thresher'/'HOP', '96').
card_multiverse_id('arsenal thresher'/'HOP', '205317').

card_in_set('ascendant evincar', 'HOP').
card_original_type('ascendant evincar'/'HOP', 'Legendary Creature — Vampire').
card_original_text('ascendant evincar'/'HOP', 'Flying\nOther black creatures get +1/+1. \nNonblack creatures get -1/-1.').
card_image_name('ascendant evincar'/'HOP', 'ascendant evincar').
card_uid('ascendant evincar'/'HOP', 'HOP:Ascendant Evincar:ascendant evincar').
card_rarity('ascendant evincar'/'HOP', 'Rare').
card_artist('ascendant evincar'/'HOP', 'Mark Zug').
card_number('ascendant evincar'/'HOP', '17').
card_flavor_text('ascendant evincar'/'HOP', 'His soul snared by an angel\'s curse, Crovax twisted heroism into its purest shadow.').
card_multiverse_id('ascendant evincar'/'HOP', '205263').

card_in_set('assault', 'HOP').
card_original_type('assault'/'HOP', 'Sorcery').
card_original_text('assault'/'HOP', 'Assault deals 2 damage to target creature or player.\n//\nBattery\n{3}{G}\nSorcery\nPut a 3/3 green Elephant creature token onto the battlefield.').
card_image_name('assault'/'HOP', 'assaultbattery').
card_uid('assault'/'HOP', 'HOP:Assault:assaultbattery').
card_rarity('assault'/'HOP', 'Uncommon').
card_artist('assault'/'HOP', 'Ben Thompson').
card_number('assault'/'HOP', '103a').
card_multiverse_id('assault'/'HOP', '205409').

card_in_set('balefire liege', 'HOP').
card_original_type('balefire liege'/'HOP', 'Creature — Spirit Horror').
card_original_text('balefire liege'/'HOP', 'Other red creatures you control get +1/+1.\nOther white creatures you control get +1/+1.\nWhenever you cast a red spell, Balefire Liege deals 3 damage to target player.\nWhenever you cast a white spell, you gain 3 life.').
card_image_name('balefire liege'/'HOP', 'balefire liege').
card_uid('balefire liege'/'HOP', 'HOP:Balefire Liege:balefire liege').
card_rarity('balefire liege'/'HOP', 'Rare').
card_artist('balefire liege'/'HOP', 'Ralph Horsley').
card_number('balefire liege'/'HOP', '97').
card_multiverse_id('balefire liege'/'HOP', '205377').

card_in_set('bant', 'HOP').
card_original_type('bant'/'HOP', 'Plane — Alara').
card_original_text('bant'/'HOP', 'All creatures have exalted. (Whenever a creature attacks alone, it gets +1/+1 until end of turn for each instance of exalted among creatures its controller controls.)\nWhenever you roll {C}, put a divinity counter on target green, white, or blue creature. That creature is indestructible as long as it has a divinity counter on it.').
card_first_print('bant', 'HOP').
card_image_name('bant'/'HOP', 'bant').
card_uid('bant'/'HOP', 'HOP:Bant:bant').
card_rarity('bant'/'HOP', 'Common').
card_artist('bant'/'HOP', 'Michael Komarck').
card_number('bant'/'HOP', '4').
card_multiverse_id('bant'/'HOP', '198099').

card_in_set('battery', 'HOP').
card_original_type('battery'/'HOP', 'Sorcery').
card_original_text('battery'/'HOP', 'Assault deals 2 damage to target creature or player.\n//\nBattery\n{3}{G}\nSorcery\nPut a 3/3 green Elephant creature token onto the battlefield.').
card_image_name('battery'/'HOP', 'assaultbattery').
card_uid('battery'/'HOP', 'HOP:Battery:assaultbattery').
card_rarity('battery'/'HOP', 'Uncommon').
card_artist('battery'/'HOP', 'Ben Thompson').
card_number('battery'/'HOP', '103b').
card_multiverse_id('battery'/'HOP', '205409').

card_in_set('battlegate mimic', 'HOP').
card_original_type('battlegate mimic'/'HOP', 'Creature — Shapeshifter').
card_original_text('battlegate mimic'/'HOP', 'Whenever you cast a spell that\'s both red and white, Battlegate Mimic becomes 4/2 and gains first strike until end of turn.').
card_image_name('battlegate mimic'/'HOP', 'battlegate mimic').
card_uid('battlegate mimic'/'HOP', 'HOP:Battlegate Mimic:battlegate mimic').
card_rarity('battlegate mimic'/'HOP', 'Common').
card_artist('battlegate mimic'/'HOP', 'Franz Vohwinkel').
card_number('battlegate mimic'/'HOP', '98').
card_flavor_text('battlegate mimic'/'HOP', 'Mimics don\'t need perfect disguises. They need only the perfect victims: the naive, the young, or the poor of sight.').
card_multiverse_id('battlegate mimic'/'HOP', '205378').

card_in_set('beacon of unrest', 'HOP').
card_original_type('beacon of unrest'/'HOP', 'Sorcery').
card_original_text('beacon of unrest'/'HOP', 'Put target artifact or creature card in a graveyard onto the battlefield under your control. Shuffle Beacon of Unrest into its owner\'s library.').
card_image_name('beacon of unrest'/'HOP', 'beacon of unrest').
card_uid('beacon of unrest'/'HOP', 'HOP:Beacon of Unrest:beacon of unrest').
card_rarity('beacon of unrest'/'HOP', 'Rare').
card_artist('beacon of unrest'/'HOP', 'Alan Pollack').
card_number('beacon of unrest'/'HOP', '18').
card_flavor_text('beacon of unrest'/'HOP', 'A vertical scream pierces the night air and echoes doom through the clouds.').
card_multiverse_id('beacon of unrest'/'HOP', '205418').

card_in_set('beast hunt', 'HOP').
card_original_type('beast hunt'/'HOP', 'Sorcery').
card_original_text('beast hunt'/'HOP', 'Reveal the top three cards of your library. Put all creature cards revealed this way into your hand and the rest into your graveyard.').
card_first_print('beast hunt', 'HOP').
card_image_name('beast hunt'/'HOP', 'beast hunt').
card_uid('beast hunt'/'HOP', 'HOP:Beast Hunt:beast hunt').
card_rarity('beast hunt'/'HOP', 'Common').
card_artist('beast hunt'/'HOP', 'Kieran Yanner').
card_number('beast hunt'/'HOP', '68').
card_flavor_text('beast hunt'/'HOP', '\"Surely we could tame something besides hurdas and pillarfield oxen!\"\n—Sheyda, Ondu gamekeeper').
card_multiverse_id('beast hunt'/'HOP', '205340').

card_in_set('beseech the queen', 'HOP').
card_original_type('beseech the queen'/'HOP', 'Sorcery').
card_original_text('beseech the queen'/'HOP', '({2/B} can be paid with any two mana or with {B}. This card\'s converted mana cost is 6.)\nSearch your library for a card with converted mana cost less than or equal to the number of lands you control, reveal it, and put it into your hand. Then shuffle your library.').
card_image_name('beseech the queen'/'HOP', 'beseech the queen').
card_uid('beseech the queen'/'HOP', 'HOP:Beseech the Queen:beseech the queen').
card_rarity('beseech the queen'/'HOP', 'Uncommon').
card_artist('beseech the queen'/'HOP', 'Jason Chan').
card_number('beseech the queen'/'HOP', '19').
card_flavor_text('beseech the queen'/'HOP', 'Those who hear her go mad with inspiration.').
card_multiverse_id('beseech the queen'/'HOP', '205399').

card_in_set('blaze', 'HOP').
card_original_type('blaze'/'HOP', 'Sorcery').
card_original_text('blaze'/'HOP', 'Blaze deals X damage to target creature or player.').
card_image_name('blaze'/'HOP', 'blaze').
card_uid('blaze'/'HOP', 'HOP:Blaze:blaze').
card_rarity('blaze'/'HOP', 'Uncommon').
card_artist('blaze'/'HOP', 'Alex Horley-Orlandelli').
card_number('blaze'/'HOP', '47').
card_flavor_text('blaze'/'HOP', 'Fire never dies alone.').
card_multiverse_id('blaze'/'HOP', '205264').

card_in_set('bogardan firefiend', 'HOP').
card_original_type('bogardan firefiend'/'HOP', 'Creature — Elemental Spirit').
card_original_text('bogardan firefiend'/'HOP', 'When Bogardan Firefiend is put into a graveyard from the battlefield, it deals 2 damage to target creature.').
card_image_name('bogardan firefiend'/'HOP', 'bogardan firefiend').
card_uid('bogardan firefiend'/'HOP', 'HOP:Bogardan Firefiend:bogardan firefiend').
card_rarity('bogardan firefiend'/'HOP', 'Common').
card_artist('bogardan firefiend'/'HOP', 'Terese Nielsen').
card_number('bogardan firefiend'/'HOP', '48').
card_flavor_text('bogardan firefiend'/'HOP', '\"The next one who tells me to relax and curl up by a fire is dead.\"\n—Mirri of the Weatherlight').
card_multiverse_id('bogardan firefiend'/'HOP', '205265').

card_in_set('bogardan rager', 'HOP').
card_original_type('bogardan rager'/'HOP', 'Creature — Elemental').
card_original_text('bogardan rager'/'HOP', 'Flash (You may cast this spell any time you could cast an instant.)\nWhen Bogardan Rager enters the battlefield, target creature gets +4/+0 until end of turn.').
card_image_name('bogardan rager'/'HOP', 'bogardan rager').
card_uid('bogardan rager'/'HOP', 'HOP:Bogardan Rager:bogardan rager').
card_rarity('bogardan rager'/'HOP', 'Common').
card_artist('bogardan rager'/'HOP', 'Clint Langley').
card_number('bogardan rager'/'HOP', '49').
card_flavor_text('bogardan rager'/'HOP', 'In the erupting heart of Bogardan, it\'s hard to tell hurtling volcanic rocks from pouncing volcanic beasts.').
card_multiverse_id('bogardan rager'/'HOP', '205407').

card_in_set('boros garrison', 'HOP').
card_original_type('boros garrison'/'HOP', 'Land').
card_original_text('boros garrison'/'HOP', 'Boros Garrison enters the battlefield tapped.\nWhen Boros Garrison enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {R}{W} to your mana pool.').
card_image_name('boros garrison'/'HOP', 'boros garrison').
card_uid('boros garrison'/'HOP', 'HOP:Boros Garrison:boros garrison').
card_rarity('boros garrison'/'HOP', 'Common').
card_artist('boros garrison'/'HOP', 'John Avon').
card_number('boros garrison'/'HOP', '131').
card_multiverse_id('boros garrison'/'HOP', '205356').

card_in_set('boros guildmage', 'HOP').
card_original_type('boros guildmage'/'HOP', 'Creature — Human Wizard').
card_original_text('boros guildmage'/'HOP', '{1}{R}: Target creature gains haste until end of turn.\n{1}{W}: Target creature gains first strike until end of turn.').
card_image_name('boros guildmage'/'HOP', 'boros guildmage').
card_uid('boros guildmage'/'HOP', 'HOP:Boros Guildmage:boros guildmage').
card_rarity('boros guildmage'/'HOP', 'Uncommon').
card_artist('boros guildmage'/'HOP', 'Paolo Parente').
card_number('boros guildmage'/'HOP', '99').
card_multiverse_id('boros guildmage'/'HOP', '205357').

card_in_set('boros signet', 'HOP').
card_original_type('boros signet'/'HOP', 'Artifact').
card_original_text('boros signet'/'HOP', '{1}, {T}: Add {R}{W} to your mana pool.').
card_image_name('boros signet'/'HOP', 'boros signet').
card_uid('boros signet'/'HOP', 'HOP:Boros Signet:boros signet').
card_rarity('boros signet'/'HOP', 'Common').
card_artist('boros signet'/'HOP', 'Greg Hildebrandt').
card_number('boros signet'/'HOP', '107').
card_flavor_text('boros signet'/'HOP', '\"Have you ever held a Boros signet? There\'s a weight to it that belies its size—a weight of strength and of pride.\"\n—Agrus Kos').
card_multiverse_id('boros signet'/'HOP', '205358').

card_in_set('boros swiftblade', 'HOP').
card_original_type('boros swiftblade'/'HOP', 'Creature — Human Soldier').
card_original_text('boros swiftblade'/'HOP', 'Double strike (This creature deals both first-strike and regular combat damage.)').
card_image_name('boros swiftblade'/'HOP', 'boros swiftblade').
card_uid('boros swiftblade'/'HOP', 'HOP:Boros Swiftblade:boros swiftblade').
card_rarity('boros swiftblade'/'HOP', 'Uncommon').
card_artist('boros swiftblade'/'HOP', 'Doug Chaffee').
card_number('boros swiftblade'/'HOP', '82').
card_flavor_text('boros swiftblade'/'HOP', 'When the Boros Legion attacks, swiftblades enter the fray first. They pick off the archers and mages, softening the enemy front before the flame-kin and giants go in.').
card_multiverse_id('boros swiftblade'/'HOP', '205359').

card_in_set('bosh, iron golem', 'HOP').
card_original_type('bosh, iron golem'/'HOP', 'Legendary Artifact Creature — Golem').
card_original_text('bosh, iron golem'/'HOP', 'Trample\n{3}{R}, Sacrifice an artifact: Bosh, Iron Golem deals damage equal to the sacrificed artifact\'s converted mana cost to target creature or player.').
card_image_name('bosh, iron golem'/'HOP', 'bosh, iron golem').
card_uid('bosh, iron golem'/'HOP', 'HOP:Bosh, Iron Golem:bosh, iron golem').
card_rarity('bosh, iron golem'/'HOP', 'Rare').
card_artist('bosh, iron golem'/'HOP', 'Brom').
card_number('bosh, iron golem'/'HOP', '108').
card_flavor_text('bosh, iron golem'/'HOP', 'As Glissa searches for the truth about Memnarch, Bosh searches to unearth the secrets of his past.').
card_multiverse_id('bosh, iron golem'/'HOP', '205276').

card_in_set('branching bolt', 'HOP').
card_original_type('branching bolt'/'HOP', 'Instant').
card_original_text('branching bolt'/'HOP', 'Choose one or both — Branching Bolt deals 3 damage to target creature with flying; and/or Branching Bolt deals 3 damage to target creature without flying.').
card_image_name('branching bolt'/'HOP', 'branching bolt').
card_uid('branching bolt'/'HOP', 'HOP:Branching Bolt:branching bolt').
card_rarity('branching bolt'/'HOP', 'Common').
card_artist('branching bolt'/'HOP', 'Vance Kovacs').
card_number('branching bolt'/'HOP', '83').
card_flavor_text('branching bolt'/'HOP', '\"Lightning lives in everything, in living flesh and growing things. It must be set free.\"\n—Rakka Mar').
card_multiverse_id('branching bolt'/'HOP', '205320').

card_in_set('briarhorn', 'HOP').
card_original_type('briarhorn'/'HOP', 'Creature — Elemental').
card_original_text('briarhorn'/'HOP', 'Flash (You may cast this spell any time you could cast an instant.)\nWhen Briarhorn enters the battlefield, target creature gets +3/+3 until end of turn.\nEvoke {1}{G} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_image_name('briarhorn'/'HOP', 'briarhorn').
card_uid('briarhorn'/'HOP', 'HOP:Briarhorn:briarhorn').
card_rarity('briarhorn'/'HOP', 'Uncommon').
card_artist('briarhorn'/'HOP', 'Nils Hamm').
card_number('briarhorn'/'HOP', '69').
card_multiverse_id('briarhorn'/'HOP', '205393').

card_in_set('broodstar', 'HOP').
card_original_type('broodstar'/'HOP', 'Creature — Beast').
card_original_text('broodstar'/'HOP', 'Affinity for artifacts (This spell costs {1} less to cast for each artifact you control.)\nFlying\nBroodstar\'s power and toughness are each equal to the number of artifacts you control.').
card_image_name('broodstar'/'HOP', 'broodstar').
card_uid('broodstar'/'HOP', 'HOP:Broodstar:broodstar').
card_rarity('broodstar'/'HOP', 'Rare').
card_artist('broodstar'/'HOP', 'Glen Angus').
card_number('broodstar'/'HOP', '8').
card_multiverse_id('broodstar'/'HOP', '205277').

card_in_set('browbeat', 'HOP').
card_original_type('browbeat'/'HOP', 'Sorcery').
card_original_text('browbeat'/'HOP', 'Any player may have Browbeat deal 5 damage to him or her. If no one does, target player draws three cards.').
card_image_name('browbeat'/'HOP', 'browbeat').
card_uid('browbeat'/'HOP', 'HOP:Browbeat:browbeat').
card_rarity('browbeat'/'HOP', 'Uncommon').
card_artist('browbeat'/'HOP', 'Mark Tedin').
card_number('browbeat'/'HOP', '50').
card_flavor_text('browbeat'/'HOP', '\"Even the threat of power has power.\"\n—Jeska, warrior adept').
card_multiverse_id('browbeat'/'HOP', '205416').

card_in_set('bull cerodon', 'HOP').
card_original_type('bull cerodon'/'HOP', 'Creature — Beast').
card_original_text('bull cerodon'/'HOP', 'Vigilance, haste').
card_image_name('bull cerodon'/'HOP', 'bull cerodon').
card_uid('bull cerodon'/'HOP', 'HOP:Bull Cerodon:bull cerodon').
card_rarity('bull cerodon'/'HOP', 'Uncommon').
card_artist('bull cerodon'/'HOP', 'Jesper Ejsing').
card_number('bull cerodon'/'HOP', '84').
card_flavor_text('bull cerodon'/'HOP', 'It holds motionless vigil, watching Naya in silence through the screen of the whitecover. When it senses anything amiss, it launches forward with the uncanny sound of torn fog.').
card_multiverse_id('bull cerodon'/'HOP', '205321').

card_in_set('cabal coffers', 'HOP').
card_original_type('cabal coffers'/'HOP', 'Land').
card_original_text('cabal coffers'/'HOP', '{2}, {T}: Add {B} to your mana pool for each Swamp you control.').
card_image_name('cabal coffers'/'HOP', 'cabal coffers').
card_uid('cabal coffers'/'HOP', 'HOP:Cabal Coffers:cabal coffers').
card_rarity('cabal coffers'/'HOP', 'Uncommon').
card_artist('cabal coffers'/'HOP', 'Don Hazeltine').
card_number('cabal coffers'/'HOP', '132').
card_flavor_text('cabal coffers'/'HOP', 'Deep within the Cabal\'s vault, the Mirari pulsed like a dead sun—and its darkness radiated across Otaria.').
card_multiverse_id('cabal coffers'/'HOP', '205421').

card_in_set('cadaverous knight', 'HOP').
card_original_type('cadaverous knight'/'HOP', 'Creature — Zombie Knight').
card_original_text('cadaverous knight'/'HOP', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)\n{1}{B}{B}: Regenerate Cadaverous Knight.').
card_image_name('cadaverous knight'/'HOP', 'cadaverous knight').
card_uid('cadaverous knight'/'HOP', 'HOP:Cadaverous Knight:cadaverous knight').
card_rarity('cadaverous knight'/'HOP', 'Common').
card_artist('cadaverous knight'/'HOP', 'Dermot Power').
card_number('cadaverous knight'/'HOP', '20').
card_flavor_text('cadaverous knight'/'HOP', 'Grieve for the soul in death dishonored.\n—Shadow Guild saying').
card_multiverse_id('cadaverous knight'/'HOP', '205423').

card_in_set('captain\'s maneuver', 'HOP').
card_original_type('captain\'s maneuver'/'HOP', 'Instant').
card_original_text('captain\'s maneuver'/'HOP', 'The next X damage that would be dealt to target creature or player this turn is dealt to another target creature or player instead.').
card_image_name('captain\'s maneuver'/'HOP', 'captain\'s maneuver').
card_uid('captain\'s maneuver'/'HOP', 'HOP:Captain\'s Maneuver:captain\'s maneuver').
card_rarity('captain\'s maneuver'/'HOP', 'Uncommon').
card_artist('captain\'s maneuver'/'HOP', 'Ben Thompson').
card_number('captain\'s maneuver'/'HOP', '85').
card_flavor_text('captain\'s maneuver'/'HOP', 'Sisay discovered that the mirrored hull of the Weatherlight could be used as a defensive weapon.').
card_multiverse_id('captain\'s maneuver'/'HOP', '205382').

card_in_set('cerodon yearling', 'HOP').
card_original_type('cerodon yearling'/'HOP', 'Creature — Beast').
card_original_text('cerodon yearling'/'HOP', 'Vigilance, haste').
card_image_name('cerodon yearling'/'HOP', 'cerodon yearling').
card_uid('cerodon yearling'/'HOP', 'HOP:Cerodon Yearling:cerodon yearling').
card_rarity('cerodon yearling'/'HOP', 'Common').
card_artist('cerodon yearling'/'HOP', 'Christopher Moeller').
card_number('cerodon yearling'/'HOP', '86').
card_flavor_text('cerodon yearling'/'HOP', 'The unit of measurement used to chart a cerodon\'s growth is \"persons consumed.\"').
card_multiverse_id('cerodon yearling'/'HOP', '205802').

card_in_set('chaos', 'HOP').
card_original_type('chaos'/'HOP', 'Instant').
card_original_text('chaos'/'HOP', 'Exile target attacking creature.\n//\nChaos\n{2}{R}\nInstant\nCreatures can\'t block this turn.').
card_image_name('chaos'/'HOP', 'orderchaos').
card_uid('chaos'/'HOP', 'HOP:Chaos:orderchaos').
card_rarity('chaos'/'HOP', 'Uncommon').
card_artist('chaos'/'HOP', 'Tim Hildebrandt').
card_number('chaos'/'HOP', '104b').
card_multiverse_id('chaos'/'HOP', '205384').

card_in_set('cinder elemental', 'HOP').
card_original_type('cinder elemental'/'HOP', 'Creature — Elemental').
card_original_text('cinder elemental'/'HOP', '{X}{R}, {T}, Sacrifice Cinder Elemental: Cinder Elemental deals X damage to target creature or player.').
card_image_name('cinder elemental'/'HOP', 'cinder elemental').
card_uid('cinder elemental'/'HOP', 'HOP:Cinder Elemental:cinder elemental').
card_rarity('cinder elemental'/'HOP', 'Uncommon').
card_artist('cinder elemental'/'HOP', 'Greg Staples').
card_number('cinder elemental'/'HOP', '51').
card_flavor_text('cinder elemental'/'HOP', 'Their rage can grow to such proportions that they explode in a cloud of fire.').
card_multiverse_id('cinder elemental'/'HOP', '205414').

card_in_set('cliffside market', 'HOP').
card_original_type('cliffside market'/'HOP', 'Plane — Mercadia').
card_original_text('cliffside market'/'HOP', 'When you planeswalk to Cliffside Market or at the beginning of your upkeep, you may exchange life totals with target player.\nWhenever you roll {C}, exchange control of two target permanents that share a type.').
card_first_print('cliffside market', 'HOP').
card_image_name('cliffside market'/'HOP', 'cliffside market').
card_uid('cliffside market'/'HOP', 'HOP:Cliffside Market:cliffside market').
card_rarity('cliffside market'/'HOP', 'Common').
card_artist('cliffside market'/'HOP', 'Matt Stewart').
card_number('cliffside market'/'HOP', '5').
card_multiverse_id('cliffside market'/'HOP', '198077').

card_in_set('cone of flame', 'HOP').
card_original_type('cone of flame'/'HOP', 'Sorcery').
card_original_text('cone of flame'/'HOP', 'Cone of Flame deals 1 damage to target creature or player, 2 damage to another target creature or player, and 3 damage to a third target creature or player.').
card_image_name('cone of flame'/'HOP', 'cone of flame').
card_uid('cone of flame'/'HOP', 'HOP:Cone of Flame:cone of flame').
card_rarity('cone of flame'/'HOP', 'Uncommon').
card_artist('cone of flame'/'HOP', 'Chippy').
card_number('cone of flame'/'HOP', '52').
card_multiverse_id('cone of flame'/'HOP', '205266').

card_in_set('congregate', 'HOP').
card_original_type('congregate'/'HOP', 'Instant').
card_original_text('congregate'/'HOP', 'Target player gains 2 life for each creature on the battlefield.').
card_image_name('congregate'/'HOP', 'congregate').
card_uid('congregate'/'HOP', 'HOP:Congregate:congregate').
card_rarity('congregate'/'HOP', 'Common').
card_artist('congregate'/'HOP', 'Mark Zug').
card_number('congregate'/'HOP', '2').
card_flavor_text('congregate'/'HOP', '\"In the gathering there is strength for all who founder, renewal for all who languish, love for all who sing.\"\n—Song of All, canto 642').
card_multiverse_id('congregate'/'HOP', '205387').

card_in_set('consume spirit', 'HOP').
card_original_type('consume spirit'/'HOP', 'Sorcery').
card_original_text('consume spirit'/'HOP', 'Spend only black mana on X. \nConsume Spirit deals X damage to target creature or player and you gain X life.').
card_image_name('consume spirit'/'HOP', 'consume spirit').
card_uid('consume spirit'/'HOP', 'HOP:Consume Spirit:consume spirit').
card_rarity('consume spirit'/'HOP', 'Uncommon').
card_artist('consume spirit'/'HOP', 'Justin Sweet').
card_number('consume spirit'/'HOP', '21').
card_multiverse_id('consume spirit'/'HOP', '205347').

card_in_set('copper myr', 'HOP').
card_original_type('copper myr'/'HOP', 'Artifact Creature — Myr').
card_original_text('copper myr'/'HOP', '{T}: Add {G} to your mana pool.').
card_image_name('copper myr'/'HOP', 'copper myr').
card_uid('copper myr'/'HOP', 'HOP:Copper Myr:copper myr').
card_rarity('copper myr'/'HOP', 'Common').
card_artist('copper myr'/'HOP', 'Kev Walker').
card_number('copper myr'/'HOP', '109').
card_flavor_text('copper myr'/'HOP', 'The elves thought of the myr as minor threats, just as the myr thought of the elves.').
card_multiverse_id('copper myr'/'HOP', '205278').

card_in_set('corpse harvester', 'HOP').
card_original_type('corpse harvester'/'HOP', 'Creature — Zombie Wizard').
card_original_text('corpse harvester'/'HOP', '{1}{B}, {T}, Sacrifice a creature: Search your library for a Zombie card and a Swamp card, reveal them, and put them into your hand. Then shuffle your library.').
card_image_name('corpse harvester'/'HOP', 'corpse harvester').
card_uid('corpse harvester'/'HOP', 'HOP:Corpse Harvester:corpse harvester').
card_rarity('corpse harvester'/'HOP', 'Uncommon').
card_artist('corpse harvester'/'HOP', 'Mark Tedin').
card_number('corpse harvester'/'HOP', '22').
card_multiverse_id('corpse harvester'/'HOP', '205419').

card_in_set('cranial plating', 'HOP').
card_original_type('cranial plating'/'HOP', 'Artifact — Equipment').
card_original_text('cranial plating'/'HOP', 'Equipped creature gets +1/+0 for each artifact you control.\n{B}{B}: Attach Cranial Plating to target creature you control.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_image_name('cranial plating'/'HOP', 'cranial plating').
card_uid('cranial plating'/'HOP', 'HOP:Cranial Plating:cranial plating').
card_rarity('cranial plating'/'HOP', 'Common').
card_artist('cranial plating'/'HOP', 'Adam Rex').
card_number('cranial plating'/'HOP', '110').
card_multiverse_id('cranial plating'/'HOP', '205328').

card_in_set('cruel revival', 'HOP').
card_original_type('cruel revival'/'HOP', 'Instant').
card_original_text('cruel revival'/'HOP', 'Destroy target non-Zombie creature. It can\'t be regenerated. Return up to one target Zombie card from your graveyard to your hand.').
card_image_name('cruel revival'/'HOP', 'cruel revival').
card_uid('cruel revival'/'HOP', 'HOP:Cruel Revival:cruel revival').
card_rarity('cruel revival'/'HOP', 'Common').
card_artist('cruel revival'/'HOP', 'Greg Staples').
card_number('cruel revival'/'HOP', '23').
card_flavor_text('cruel revival'/'HOP', 'Few Cabal fighters fear death. They fear what follows it.').
card_multiverse_id('cruel revival'/'HOP', '205367').

card_in_set('dark ritual', 'HOP').
card_original_type('dark ritual'/'HOP', 'Instant').
card_original_text('dark ritual'/'HOP', 'Add {B}{B}{B} to your mana pool.').
card_image_name('dark ritual'/'HOP', 'dark ritual').
card_uid('dark ritual'/'HOP', 'HOP:Dark Ritual:dark ritual').
card_rarity('dark ritual'/'HOP', 'Common').
card_artist('dark ritual'/'HOP', 'Clint Langley').
card_number('dark ritual'/'HOP', '24').
card_flavor_text('dark ritual'/'HOP', '\"If there is such a thing as too much power, I have not discovered it.\"\n—Volrath').
card_multiverse_id('dark ritual'/'HOP', '205422').

card_in_set('darksteel forge', 'HOP').
card_original_type('darksteel forge'/'HOP', 'Artifact').
card_original_text('darksteel forge'/'HOP', 'Artifacts you control are indestructible. (Effects that say \"destroy\" don\'t destroy them. Indestructible creatures can\'t be destroyed by damage.)').
card_image_name('darksteel forge'/'HOP', 'darksteel forge').
card_uid('darksteel forge'/'HOP', 'HOP:Darksteel Forge:darksteel forge').
card_rarity('darksteel forge'/'HOP', 'Rare').
card_artist('darksteel forge'/'HOP', 'Martina Pilcerova').
card_number('darksteel forge'/'HOP', '111').
card_flavor_text('darksteel forge'/'HOP', '\"Did it have this shape upon Mirrodin\'s creation, or did some inconceivable force shape the unshapable?\"\n—Pontifex, elder researcher').
card_multiverse_id('darksteel forge'/'HOP', '205337').

card_in_set('death baron', 'HOP').
card_original_type('death baron'/'HOP', 'Creature — Zombie Wizard').
card_original_text('death baron'/'HOP', 'Skeleton creatures you control and other Zombie creatures you control get +1/+1 and have deathtouch.').
card_image_name('death baron'/'HOP', 'death baron').
card_uid('death baron'/'HOP', 'HOP:Death Baron:death baron').
card_rarity('death baron'/'HOP', 'Rare').
card_artist('death baron'/'HOP', 'Nils Hamm').
card_number('death baron'/'HOP', '25').
card_flavor_text('death baron'/'HOP', 'For the necromancer barons, killing and recruitment are one and the same.').
card_multiverse_id('death baron'/'HOP', '205322').

card_in_set('door to nothingness', 'HOP').
card_original_type('door to nothingness'/'HOP', 'Artifact').
card_original_text('door to nothingness'/'HOP', 'Door to Nothingness enters the battlefield tapped.\n{W}{W}{U}{U}{B}{B}{R}{R}{G}{G}, {T}, Sacrifice Door to Nothingness: Target player loses the game.').
card_image_name('door to nothingness'/'HOP', 'door to nothingness').
card_uid('door to nothingness'/'HOP', 'HOP:Door to Nothingness:door to nothingness').
card_rarity('door to nothingness'/'HOP', 'Rare').
card_artist('door to nothingness'/'HOP', 'Puddnhead').
card_number('door to nothingness'/'HOP', '112').
card_flavor_text('door to nothingness'/'HOP', '\"All memory of your existence will be wiped from reality. You will die, and no one will mourn.\"\n—Memnarch').
card_multiverse_id('door to nothingness'/'HOP', '205329').

card_in_set('double cleave', 'HOP').
card_original_type('double cleave'/'HOP', 'Instant').
card_original_text('double cleave'/'HOP', 'Target creature gains double strike until end of turn. (It deals both first-strike and regular combat damage.)').
card_image_name('double cleave'/'HOP', 'double cleave').
card_uid('double cleave'/'HOP', 'HOP:Double Cleave:double cleave').
card_rarity('double cleave'/'HOP', 'Common').
card_artist('double cleave'/'HOP', 'rk post').
card_number('double cleave'/'HOP', '100').
card_flavor_text('double cleave'/'HOP', '\"When in doubt, kill \'em twice.\"').
card_multiverse_id('double cleave'/'HOP', '205379').

card_in_set('dregscape zombie', 'HOP').
card_original_type('dregscape zombie'/'HOP', 'Creature — Zombie').
card_original_text('dregscape zombie'/'HOP', 'Unearth {B} ({B}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave play. Unearth only as a sorcery.)').
card_image_name('dregscape zombie'/'HOP', 'dregscape zombie').
card_uid('dregscape zombie'/'HOP', 'HOP:Dregscape Zombie:dregscape zombie').
card_rarity('dregscape zombie'/'HOP', 'Common').
card_artist('dregscape zombie'/'HOP', 'Lars Grant-West').
card_number('dregscape zombie'/'HOP', '26').
card_flavor_text('dregscape zombie'/'HOP', 'The undead of Grixis are fueled by their hatred of the living.').
card_multiverse_id('dregscape zombie'/'HOP', '205323').

card_in_set('duergar hedge-mage', 'HOP').
card_original_type('duergar hedge-mage'/'HOP', 'Creature — Dwarf Shaman').
card_original_text('duergar hedge-mage'/'HOP', 'When Duergar Hedge-Mage enters the battlefield, if you control two or more Mountains, you may destroy target artifact.\nWhen Duergar Hedge-Mage enters the battlefield, if you control two or more Plains, you may destroy target enchantment.').
card_image_name('duergar hedge-mage'/'HOP', 'duergar hedge-mage').
card_uid('duergar hedge-mage'/'HOP', 'HOP:Duergar Hedge-Mage:duergar hedge-mage').
card_rarity('duergar hedge-mage'/'HOP', 'Uncommon').
card_artist('duergar hedge-mage'/'HOP', 'Dave Allsop').
card_number('duergar hedge-mage'/'HOP', '101').
card_multiverse_id('duergar hedge-mage'/'HOP', '205380').

card_in_set('eloren wilds', 'HOP').
card_original_type('eloren wilds'/'HOP', 'Plane — Shandalar').
card_original_text('eloren wilds'/'HOP', 'Whenever a player taps a permanent for mana, that player adds one mana to his or her mana pool of any type that permanent produced.\nWhenever you roll {C}, target player can\'t cast spells until a player planeswalks.').
card_first_print('eloren wilds', 'HOP').
card_image_name('eloren wilds'/'HOP', 'eloren wilds').
card_uid('eloren wilds'/'HOP', 'HOP:Eloren Wilds:eloren wilds').
card_rarity('eloren wilds'/'HOP', 'Common').
card_artist('eloren wilds'/'HOP', 'Darrell Riche').
card_number('eloren wilds'/'HOP', '7').
card_multiverse_id('eloren wilds'/'HOP', '198109').

card_in_set('etched oracle', 'HOP').
card_original_type('etched oracle'/'HOP', 'Artifact Creature — Wizard').
card_original_text('etched oracle'/'HOP', 'Sunburst (This enters the battlefield with a +1/+1 counter on it for each color of mana spent to cast it.)\n{1}, Remove four +1/+1 counters from Etched Oracle: Target player draws three cards.').
card_image_name('etched oracle'/'HOP', 'etched oracle').
card_uid('etched oracle'/'HOP', 'HOP:Etched Oracle:etched oracle').
card_rarity('etched oracle'/'HOP', 'Uncommon').
card_artist('etched oracle'/'HOP', 'Matt Cavotta').
card_number('etched oracle'/'HOP', '113').
card_multiverse_id('etched oracle'/'HOP', '205330').

card_in_set('explosive vegetation', 'HOP').
card_original_type('explosive vegetation'/'HOP', 'Sorcery').
card_original_text('explosive vegetation'/'HOP', 'Search your library for up to two basic land cards and put them onto the battlefield tapped. Then shuffle your library.').
card_image_name('explosive vegetation'/'HOP', 'explosive vegetation').
card_uid('explosive vegetation'/'HOP', 'HOP:Explosive Vegetation:explosive vegetation').
card_rarity('explosive vegetation'/'HOP', 'Uncommon').
card_artist('explosive vegetation'/'HOP', 'John Avon').
card_number('explosive vegetation'/'HOP', '70').
card_flavor_text('explosive vegetation'/'HOP', 'Torching Krosa would be pointless. It grows faster than it burns.').
card_multiverse_id('explosive vegetation'/'HOP', '205368').

card_in_set('fabricate', 'HOP').
card_original_type('fabricate'/'HOP', 'Sorcery').
card_original_text('fabricate'/'HOP', 'Search your library for an artifact card, reveal it, and put it into your hand. Then shuffle your library.').
card_image_name('fabricate'/'HOP', 'fabricate').
card_uid('fabricate'/'HOP', 'HOP:Fabricate:fabricate').
card_rarity('fabricate'/'HOP', 'Uncommon').
card_artist('fabricate'/'HOP', 'Glen Angus').
card_number('fabricate'/'HOP', '9').
card_flavor_text('fabricate'/'HOP', '\"The secret to invention is to see something in your mind, then find where it hides in the world.\"').
card_multiverse_id('fabricate'/'HOP', '205348').

card_in_set('feeding grounds', 'HOP').
card_original_type('feeding grounds'/'HOP', 'Plane — Muraganda').
card_original_text('feeding grounds'/'HOP', 'Red spells cost {1} less to cast.\nGreen spells cost {1} less to cast.\nWhenever you roll {C}, put X +1/+1 counters on target creature, where X is that creature\'s converted mana cost.').
card_first_print('feeding grounds', 'HOP').
card_image_name('feeding grounds'/'HOP', 'feeding grounds').
card_uid('feeding grounds'/'HOP', 'HOP:Feeding Grounds:feeding grounds').
card_rarity('feeding grounds'/'HOP', 'Common').
card_artist('feeding grounds'/'HOP', 'Matt Stewart').
card_number('feeding grounds'/'HOP', '9').
card_multiverse_id('feeding grounds'/'HOP', '198066').

card_in_set('fertile ground', 'HOP').
card_original_type('fertile ground'/'HOP', 'Enchantment — Aura').
card_original_text('fertile ground'/'HOP', 'Enchant land\nWhenever enchanted land is tapped for mana, its controller adds one mana of any color to his or her mana pool (in addition to the mana the land produces).').
card_image_name('fertile ground'/'HOP', 'fertile ground').
card_uid('fertile ground'/'HOP', 'HOP:Fertile Ground:fertile ground').
card_rarity('fertile ground'/'HOP', 'Common').
card_artist('fertile ground'/'HOP', 'Mark Tedin').
card_number('fertile ground'/'HOP', '71').
card_multiverse_id('fertile ground'/'HOP', '205394').

card_in_set('fertilid', 'HOP').
card_original_type('fertilid'/'HOP', 'Creature — Elemental').
card_original_text('fertilid'/'HOP', 'Fertilid enters the battlefield with two +1/+1 counters on it.\n{1}{G}, Remove a +1/+1 counter from Fertilid: Target player searches his or her library for a basic land card and puts it onto the battlefield tapped. Then that player shuffles his or her library.').
card_image_name('fertilid'/'HOP', 'fertilid').
card_uid('fertilid'/'HOP', 'HOP:Fertilid:fertilid').
card_rarity('fertilid'/'HOP', 'Common').
card_artist('fertilid'/'HOP', 'Wayne Reynolds').
card_number('fertilid'/'HOP', '72').
card_multiverse_id('fertilid'/'HOP', '205403').

card_in_set('festering goblin', 'HOP').
card_original_type('festering goblin'/'HOP', 'Creature — Zombie Goblin').
card_original_text('festering goblin'/'HOP', 'When Festering Goblin is put into a graveyard from the battlefield, target creature gets -1/-1 until end of turn.').
card_image_name('festering goblin'/'HOP', 'festering goblin').
card_uid('festering goblin'/'HOP', 'HOP:Festering Goblin:festering goblin').
card_rarity('festering goblin'/'HOP', 'Common').
card_artist('festering goblin'/'HOP', 'Thomas M. Baxa').
card_number('festering goblin'/'HOP', '27').
card_flavor_text('festering goblin'/'HOP', 'In life, it was a fetid, disease-ridden thing. In death, not much changed.').
card_multiverse_id('festering goblin'/'HOP', '205267').

card_in_set('fields of summer', 'HOP').
card_original_type('fields of summer'/'HOP', 'Plane — Moag').
card_original_text('fields of summer'/'HOP', 'Whenever a player casts a spell, that player may gain 2 life.\nWhenever you roll {C}, you may gain 10 life.').
card_first_print('fields of summer', 'HOP').
card_image_name('fields of summer'/'HOP', 'fields of summer').
card_uid('fields of summer'/'HOP', 'HOP:Fields of Summer:fields of summer').
card_rarity('fields of summer'/'HOP', 'Common').
card_artist('fields of summer'/'HOP', 'Daniel Ljunggren').
card_number('fields of summer'/'HOP', '10').
card_multiverse_id('fields of summer'/'HOP', '198097').

card_in_set('fires of yavimaya', 'HOP').
card_original_type('fires of yavimaya'/'HOP', 'Enchantment').
card_original_text('fires of yavimaya'/'HOP', 'Creatures you control have haste.\nSacrifice Fires of Yavimaya: Target creature gets +2/+2 until end of turn.').
card_image_name('fires of yavimaya'/'HOP', 'fires of yavimaya').
card_uid('fires of yavimaya'/'HOP', 'HOP:Fires of Yavimaya:fires of yavimaya').
card_rarity('fires of yavimaya'/'HOP', 'Uncommon').
card_artist('fires of yavimaya'/'HOP', 'Val Mayerik').
card_number('fires of yavimaya'/'HOP', '87').
card_multiverse_id('fires of yavimaya'/'HOP', '205412').

card_in_set('flamekin harbinger', 'HOP').
card_original_type('flamekin harbinger'/'HOP', 'Creature — Elemental Shaman').
card_original_text('flamekin harbinger'/'HOP', 'When Flamekin Harbinger enters the battlefield, you may search your library for an Elemental card, reveal it, then shuffle your library and put that card on top of it.').
card_image_name('flamekin harbinger'/'HOP', 'flamekin harbinger').
card_uid('flamekin harbinger'/'HOP', 'HOP:Flamekin Harbinger:flamekin harbinger').
card_rarity('flamekin harbinger'/'HOP', 'Uncommon').
card_artist('flamekin harbinger'/'HOP', 'Steve Prescott').
card_number('flamekin harbinger'/'HOP', '53').
card_multiverse_id('flamekin harbinger'/'HOP', '205395').

card_in_set('flametongue kavu', 'HOP').
card_original_type('flametongue kavu'/'HOP', 'Creature — Kavu').
card_original_text('flametongue kavu'/'HOP', 'When Flametongue Kavu enters the battlefield, it deals 4 damage to target creature.').
card_image_name('flametongue kavu'/'HOP', 'flametongue kavu').
card_uid('flametongue kavu'/'HOP', 'HOP:Flametongue Kavu:flametongue kavu').
card_rarity('flametongue kavu'/'HOP', 'Uncommon').
card_artist('flametongue kavu'/'HOP', 'Pete Venters').
card_number('flametongue kavu'/'HOP', '54').
card_flavor_text('flametongue kavu'/'HOP', '\"For dim-witted, thick-skulled genetic mutants, they have pretty good aim.\"\n—Sisay, captain of the Weatherlight').
card_multiverse_id('flametongue kavu'/'HOP', '205392').

card_in_set('forest', 'HOP').
card_original_type('forest'/'HOP', 'Basic Land — Forest').
card_original_text('forest'/'HOP', 'G').
card_image_name('forest'/'HOP', 'forest1').
card_uid('forest'/'HOP', 'HOP:Forest:forest1').
card_rarity('forest'/'HOP', 'Basic Land').
card_artist('forest'/'HOP', 'John Avon').
card_number('forest'/'HOP', '165').
card_multiverse_id('forest'/'HOP', '205464').

card_in_set('forest', 'HOP').
card_original_type('forest'/'HOP', 'Basic Land — Forest').
card_original_text('forest'/'HOP', 'G').
card_image_name('forest'/'HOP', 'forest2').
card_uid('forest'/'HOP', 'HOP:Forest:forest2').
card_rarity('forest'/'HOP', 'Basic Land').
card_artist('forest'/'HOP', 'Craig Mullins').
card_number('forest'/'HOP', '166').
card_multiverse_id('forest'/'HOP', '205466').

card_in_set('forest', 'HOP').
card_original_type('forest'/'HOP', 'Basic Land — Forest').
card_original_text('forest'/'HOP', 'G').
card_image_name('forest'/'HOP', 'forest3').
card_uid('forest'/'HOP', 'HOP:Forest:forest3').
card_rarity('forest'/'HOP', 'Basic Land').
card_artist('forest'/'HOP', 'Chippy').
card_number('forest'/'HOP', '167').
card_multiverse_id('forest'/'HOP', '205467').

card_in_set('forest', 'HOP').
card_original_type('forest'/'HOP', 'Basic Land — Forest').
card_original_text('forest'/'HOP', 'G').
card_image_name('forest'/'HOP', 'forest4').
card_uid('forest'/'HOP', 'HOP:Forest:forest4').
card_rarity('forest'/'HOP', 'Basic Land').
card_artist('forest'/'HOP', 'Glen Angus').
card_number('forest'/'HOP', '168').
card_multiverse_id('forest'/'HOP', '205465').

card_in_set('forest', 'HOP').
card_original_type('forest'/'HOP', 'Basic Land — Forest').
card_original_text('forest'/'HOP', 'G').
card_image_name('forest'/'HOP', 'forest5').
card_uid('forest'/'HOP', 'HOP:Forest:forest5').
card_rarity('forest'/'HOP', 'Basic Land').
card_artist('forest'/'HOP', 'Mark Tedin').
card_number('forest'/'HOP', '169').
card_multiverse_id('forest'/'HOP', '205279').

card_in_set('forgotten ancient', 'HOP').
card_original_type('forgotten ancient'/'HOP', 'Creature — Elemental').
card_original_text('forgotten ancient'/'HOP', 'Whenever a player casts a spell, you may put a +1/+1 counter on Forgotten Ancient.\nAt the beginning of your upkeep, you may move any number of +1/+1 counters from Forgotten Ancient onto other creatures.').
card_image_name('forgotten ancient'/'HOP', 'forgotten ancient').
card_uid('forgotten ancient'/'HOP', 'HOP:Forgotten Ancient:forgotten ancient').
card_rarity('forgotten ancient'/'HOP', 'Rare').
card_artist('forgotten ancient'/'HOP', 'Mark Tedin').
card_number('forgotten ancient'/'HOP', '73').
card_flavor_text('forgotten ancient'/'HOP', 'Its blood is life. Its body is growth.').
card_multiverse_id('forgotten ancient'/'HOP', '205273').

card_in_set('furnace of rath', 'HOP').
card_original_type('furnace of rath'/'HOP', 'Enchantment').
card_original_text('furnace of rath'/'HOP', 'If a source would deal damage to a creature or player, it deals double that damage to that creature or player instead.').
card_image_name('furnace of rath'/'HOP', 'furnace of rath').
card_uid('furnace of rath'/'HOP', 'HOP:Furnace of Rath:furnace of rath').
card_rarity('furnace of rath'/'HOP', 'Rare').
card_artist('furnace of rath'/'HOP', 'John Matson').
card_number('furnace of rath'/'HOP', '55').
card_flavor_text('furnace of rath'/'HOP', 'The furnace awaits the next master who would stoke the fires of apocalypse.').
card_multiverse_id('furnace of rath'/'HOP', '205268').

card_in_set('glimmervoid basin', 'HOP').
card_original_type('glimmervoid basin'/'HOP', 'Plane — Mirrodin').
card_original_text('glimmervoid basin'/'HOP', 'Whenever a player casts an instant or sorcery spell with a single target, he or she copies that spell for each other spell, permanent, card not on the battlefield, and/or player the spell could target. Each copy targets a different one of them.\nWhenever you roll {C}, choose target creature. Each player except that creature\'s controller puts a token onto the battlefield that\'s a copy of that creature.').
card_first_print('glimmervoid basin', 'HOP').
card_image_name('glimmervoid basin'/'HOP', 'glimmervoid basin').
card_uid('glimmervoid basin'/'HOP', 'HOP:Glimmervoid Basin:glimmervoid basin').
card_rarity('glimmervoid basin'/'HOP', 'Common').
card_artist('glimmervoid basin'/'HOP', 'Lars Grant-West').
card_number('glimmervoid basin'/'HOP', '12').
card_multiverse_id('glimmervoid basin'/'HOP', '198068').

card_in_set('glory of warfare', 'HOP').
card_original_type('glory of warfare'/'HOP', 'Enchantment').
card_original_text('glory of warfare'/'HOP', 'As long as it\'s your turn, creatures you control get +2/+0.\nAs long as it\'s not your turn, creatures you control get +0/+2.').
card_image_name('glory of warfare'/'HOP', 'glory of warfare').
card_uid('glory of warfare'/'HOP', 'HOP:Glory of Warfare:glory of warfare').
card_rarity('glory of warfare'/'HOP', 'Rare').
card_artist('glory of warfare'/'HOP', 'Paolo Parente').
card_number('glory of warfare'/'HOP', '88').
card_flavor_text('glory of warfare'/'HOP', '\"Their hopes dashed on our shields! Their victory slashed by our swords!\"').
card_multiverse_id('glory of warfare'/'HOP', '205318').

card_in_set('goblin offensive', 'HOP').
card_original_type('goblin offensive'/'HOP', 'Sorcery').
card_original_text('goblin offensive'/'HOP', 'Put X 1/1 red Goblin creature tokens onto the battlefield.').
card_image_name('goblin offensive'/'HOP', 'goblin offensive').
card_uid('goblin offensive'/'HOP', 'HOP:Goblin Offensive:goblin offensive').
card_rarity('goblin offensive'/'HOP', 'Uncommon').
card_artist('goblin offensive'/'HOP', 'Carl Critchlow').
card_number('goblin offensive'/'HOP', '56').
card_flavor_text('goblin offensive'/'HOP', 'They certainly are.').
card_multiverse_id('goblin offensive'/'HOP', '205388').

card_in_set('gold myr', 'HOP').
card_original_type('gold myr'/'HOP', 'Artifact Creature — Myr').
card_original_text('gold myr'/'HOP', '{T}: Add {W} to your mana pool.').
card_image_name('gold myr'/'HOP', 'gold myr').
card_uid('gold myr'/'HOP', 'HOP:Gold Myr:gold myr').
card_rarity('gold myr'/'HOP', 'Common').
card_artist('gold myr'/'HOP', 'Kev Walker').
card_number('gold myr'/'HOP', '114').
card_flavor_text('gold myr'/'HOP', 'The leonin thought of the myr as omens, never imagining the sinister fate they foretold.').
card_multiverse_id('gold myr'/'HOP', '205283').

card_in_set('goldmeadow', 'HOP').
card_original_type('goldmeadow'/'HOP', 'Plane — Lorwyn').
card_original_text('goldmeadow'/'HOP', 'Whenever a land enters the battlefield, that land\'s controller puts three 0/1 white Goat creature tokens onto the battlefield.\nWhenever you roll {C}, put a 0/1 white Goat creature token onto the battlefield.').
card_first_print('goldmeadow', 'HOP').
card_image_name('goldmeadow'/'HOP', 'goldmeadow').
card_uid('goldmeadow'/'HOP', 'HOP:Goldmeadow:goldmeadow').
card_rarity('goldmeadow'/'HOP', 'Common').
card_artist('goldmeadow'/'HOP', 'Warren Mahy').
card_number('goldmeadow'/'HOP', '13').
card_multiverse_id('goldmeadow'/'HOP', '198087').

card_in_set('grave pact', 'HOP').
card_original_type('grave pact'/'HOP', 'Enchantment').
card_original_text('grave pact'/'HOP', 'Whenever a creature you control is put into a graveyard from the battlefield, each other player sacrifices a creature.').
card_image_name('grave pact'/'HOP', 'grave pact').
card_uid('grave pact'/'HOP', 'HOP:Grave Pact:grave pact').
card_rarity('grave pact'/'HOP', 'Rare').
card_artist('grave pact'/'HOP', 'Puddnhead').
card_number('grave pact'/'HOP', '28').
card_flavor_text('grave pact'/'HOP', '\"The bonds of loyalty can tie one to the grave.\"\n—Crovax, ascendant evincar').
card_multiverse_id('grave pact'/'HOP', '205269').

card_in_set('gravedigger', 'HOP').
card_original_type('gravedigger'/'HOP', 'Creature — Zombie').
card_original_text('gravedigger'/'HOP', 'When Gravedigger enters the battlefield, you may return target creature card from your graveyard to your hand.').
card_image_name('gravedigger'/'HOP', 'gravedigger').
card_uid('gravedigger'/'HOP', 'HOP:Gravedigger:gravedigger').
card_rarity('gravedigger'/'HOP', 'Common').
card_artist('gravedigger'/'HOP', 'Dermot Power').
card_number('gravedigger'/'HOP', '29').
card_flavor_text('gravedigger'/'HOP', 'Oddly, no one questioned the Cabal\'s wisdom in hiring a zombie to work at the cemetery.').
card_multiverse_id('gravedigger'/'HOP', '205349').

card_in_set('great furnace', 'HOP').
card_original_type('great furnace'/'HOP', 'Artifact Land').
card_original_text('great furnace'/'HOP', '(Great Furnace isn\'t a spell.)\n{T}: Add {R} to your mana pool.').
card_image_name('great furnace'/'HOP', 'great furnace').
card_uid('great furnace'/'HOP', 'HOP:Great Furnace:great furnace').
card_rarity('great furnace'/'HOP', 'Common').
card_artist('great furnace'/'HOP', 'Rob Alexander').
card_number('great furnace'/'HOP', '133').
card_flavor_text('great furnace'/'HOP', 'Kuldotha, wellspring of molten metal, temple of the goblin horde.').
card_multiverse_id('great furnace'/'HOP', '205284').

card_in_set('grixis', 'HOP').
card_original_type('grixis'/'HOP', 'Plane — Alara').
card_original_text('grixis'/'HOP', 'Blue, black, and/or red creature cards in your graveyard have unearth. The unearth cost is equal to the card\'s mana cost. (Pay the card\'s mana cost: Return it to the battlefield. The creature gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)\nWhenever you roll {C}, put target creature card in a graveyard onto the battlefield under your control.').
card_first_print('grixis', 'HOP').
card_image_name('grixis'/'HOP', 'grixis').
card_uid('grixis'/'HOP', 'HOP:Grixis:grixis').
card_rarity('grixis'/'HOP', 'Common').
card_artist('grixis'/'HOP', 'Nils Hamm').
card_number('grixis'/'HOP', '15').
card_multiverse_id('grixis'/'HOP', '198071').

card_in_set('gruul turf', 'HOP').
card_original_type('gruul turf'/'HOP', 'Land').
card_original_text('gruul turf'/'HOP', 'Gruul Turf enters the battlefield tapped.\nWhen Gruul Turf enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {R}{G} to your mana pool.').
card_image_name('gruul turf'/'HOP', 'gruul turf').
card_uid('gruul turf'/'HOP', 'HOP:Gruul Turf:gruul turf').
card_rarity('gruul turf'/'HOP', 'Common').
card_artist('gruul turf'/'HOP', 'John Avon').
card_number('gruul turf'/'HOP', '134').
card_multiverse_id('gruul turf'/'HOP', '205353').

card_in_set('hearthfire hobgoblin', 'HOP').
card_original_type('hearthfire hobgoblin'/'HOP', 'Creature — Goblin Soldier').
card_original_text('hearthfire hobgoblin'/'HOP', 'Double strike (This creature deals both first-strike and regular combat damage.)').
card_image_name('hearthfire hobgoblin'/'HOP', 'hearthfire hobgoblin').
card_uid('hearthfire hobgoblin'/'HOP', 'HOP:Hearthfire Hobgoblin:hearthfire hobgoblin').
card_rarity('hearthfire hobgoblin'/'HOP', 'Uncommon').
card_artist('hearthfire hobgoblin'/'HOP', 'Steven Belledin').
card_number('hearthfire hobgoblin'/'HOP', '102').
card_flavor_text('hearthfire hobgoblin'/'HOP', 'Hobgoblins are best left alone. They sharpen their farm implements far more than is necessary for their work in the fields.').
card_multiverse_id('hearthfire hobgoblin'/'HOP', '205381').

card_in_set('helldozer', 'HOP').
card_original_type('helldozer'/'HOP', 'Creature — Zombie Giant').
card_original_text('helldozer'/'HOP', '{B}{B}{B}, {T}: Destroy target land. If that land was nonbasic, untap Helldozer.').
card_image_name('helldozer'/'HOP', 'helldozer').
card_uid('helldozer'/'HOP', 'HOP:Helldozer:helldozer').
card_rarity('helldozer'/'HOP', 'Rare').
card_artist('helldozer'/'HOP', 'Zoltan Boros & Gabor Szikszai').
card_number('helldozer'/'HOP', '30').
card_flavor_text('helldozer'/'HOP', 'Sometimes you go to hell, and sometimes hell comes to you.').
card_multiverse_id('helldozer'/'HOP', '205360').

card_in_set('hideous end', 'HOP').
card_original_type('hideous end'/'HOP', 'Instant').
card_original_text('hideous end'/'HOP', 'Destroy target nonblack creature. Its controller loses 2 life.').
card_first_print('hideous end', 'HOP').
card_image_name('hideous end'/'HOP', 'hideous end').
card_uid('hideous end'/'HOP', 'HOP:Hideous End:hideous end').
card_rarity('hideous end'/'HOP', 'Common').
card_artist('hideous end'/'HOP', 'Zoltan Boros & Gabor Szikszai').
card_number('hideous end'/'HOP', '31').
card_flavor_text('hideous end'/'HOP', '\"A little dark magic won\'t stop me. The worse the curse, the better the prize.\"\n—Radavi, Joraga relic hunter, last words').
card_multiverse_id('hideous end'/'HOP', '205341').

card_in_set('hull breach', 'HOP').
card_original_type('hull breach'/'HOP', 'Sorcery').
card_original_text('hull breach'/'HOP', 'Choose one — Destroy target artifact; or destroy target enchantment; or destroy target artifact and target enchantment.').
card_image_name('hull breach'/'HOP', 'hull breach').
card_uid('hull breach'/'HOP', 'HOP:Hull Breach:hull breach').
card_rarity('hull breach'/'HOP', 'Common').
card_artist('hull breach'/'HOP', 'Brian Snõddy').
card_number('hull breach'/'HOP', '89').
card_flavor_text('hull breach'/'HOP', '\"Crovax knows we\'re coming now,\" said a grinning Sisay. \"I just sent the Predator crashing into his stronghold.\"').
card_multiverse_id('hull breach'/'HOP', '205413').

card_in_set('immersturm', 'HOP').
card_original_type('immersturm'/'HOP', 'Plane — Valla').
card_original_text('immersturm'/'HOP', 'Whenever a creature enters the battlefield, that creature\'s controller may have it deal damage equal to its power to target creature or player of his or her choice.\nWhenever you roll {C}, exile target creature, then return it to the battlefield under its owner\'s control.').
card_first_print('immersturm', 'HOP').
card_image_name('immersturm'/'HOP', 'immersturm').
card_uid('immersturm'/'HOP', 'HOP:Immersturm:immersturm').
card_rarity('immersturm'/'HOP', 'Common').
card_artist('immersturm'/'HOP', 'Raymond Swanland').
card_number('immersturm'/'HOP', '17').
card_multiverse_id('immersturm'/'HOP', '198084').

card_in_set('incremental blight', 'HOP').
card_original_type('incremental blight'/'HOP', 'Sorcery').
card_original_text('incremental blight'/'HOP', 'Put a -1/-1 counter on target creature, two -1/-1 counters on another target creature, and three -1/-1 counters on a third target creature.').
card_image_name('incremental blight'/'HOP', 'incremental blight').
card_uid('incremental blight'/'HOP', 'HOP:Incremental Blight:incremental blight').
card_rarity('incremental blight'/'HOP', 'Uncommon').
card_artist('incremental blight'/'HOP', 'Chuck Lukacs').
card_number('incremental blight'/'HOP', '32').
card_flavor_text('incremental blight'/'HOP', 'Shadowmoor\'s main crops are rot, slime, and despair.').
card_multiverse_id('incremental blight'/'HOP', '205400').

card_in_set('innocent blood', 'HOP').
card_original_type('innocent blood'/'HOP', 'Sorcery').
card_original_text('innocent blood'/'HOP', 'Each player sacrifices a creature.').
card_image_name('innocent blood'/'HOP', 'innocent blood').
card_uid('innocent blood'/'HOP', 'HOP:Innocent Blood:innocent blood').
card_rarity('innocent blood'/'HOP', 'Common').
card_artist('innocent blood'/'HOP', 'Carl Critchlow').
card_number('innocent blood'/'HOP', '33').
card_flavor_text('innocent blood'/'HOP', 'Zombies mourn for the living and celebrate those who will soon be given the gift of death.').
card_multiverse_id('innocent blood'/'HOP', '205364').

card_in_set('insurrection', 'HOP').
card_original_type('insurrection'/'HOP', 'Sorcery').
card_original_text('insurrection'/'HOP', 'Untap all creatures and gain control of them until end of turn. They gain haste until end of turn.').
card_image_name('insurrection'/'HOP', 'insurrection').
card_uid('insurrection'/'HOP', 'HOP:Insurrection:insurrection').
card_rarity('insurrection'/'HOP', 'Rare').
card_artist('insurrection'/'HOP', 'Mark Zug').
card_number('insurrection'/'HOP', '57').
card_flavor_text('insurrection'/'HOP', '\"Maybe they wanted to be on the winning side for once.\"\n—Matoc, lavamancer').
card_multiverse_id('insurrection'/'HOP', '205369').

card_in_set('iron myr', 'HOP').
card_original_type('iron myr'/'HOP', 'Artifact Creature — Myr').
card_original_text('iron myr'/'HOP', '{T}: Add {R} to your mana pool.').
card_image_name('iron myr'/'HOP', 'iron myr').
card_uid('iron myr'/'HOP', 'HOP:Iron Myr:iron myr').
card_rarity('iron myr'/'HOP', 'Common').
card_artist('iron myr'/'HOP', 'Kev Walker').
card_number('iron myr'/'HOP', '115').
card_flavor_text('iron myr'/'HOP', 'The goblins didn\'t think of the myr at all, which allowed the myr to observe everywhere unhindered.').
card_multiverse_id('iron myr'/'HOP', '205285').

card_in_set('island', 'HOP').
card_original_type('island'/'HOP', 'Basic Land — Island').
card_original_text('island'/'HOP', 'U').
card_image_name('island'/'HOP', 'island1').
card_uid('island'/'HOP', 'HOP:Island:island1').
card_rarity('island'/'HOP', 'Basic Land').
card_artist('island'/'HOP', 'Mark Tedin').
card_number('island'/'HOP', '147').
card_multiverse_id('island'/'HOP', '205286').

card_in_set('island', 'HOP').
card_original_type('island'/'HOP', 'Basic Land — Island').
card_original_text('island'/'HOP', 'U').
card_image_name('island'/'HOP', 'island2').
card_uid('island'/'HOP', 'HOP:Island:island2').
card_rarity('island'/'HOP', 'Basic Land').
card_artist('island'/'HOP', 'Rob Alexander').
card_number('island'/'HOP', '148').
card_multiverse_id('island'/'HOP', '205287').

card_in_set('island', 'HOP').
card_original_type('island'/'HOP', 'Basic Land — Island').
card_original_text('island'/'HOP', 'U').
card_image_name('island'/'HOP', 'island3').
card_uid('island'/'HOP', 'HOP:Island:island3').
card_rarity('island'/'HOP', 'Basic Land').
card_artist('island'/'HOP', 'Martina Pilcerova').
card_number('island'/'HOP', '149').
card_multiverse_id('island'/'HOP', '205288').

card_in_set('island', 'HOP').
card_original_type('island'/'HOP', 'Basic Land — Island').
card_original_text('island'/'HOP', 'U').
card_image_name('island'/'HOP', 'island4').
card_uid('island'/'HOP', 'HOP:Island:island4').
card_rarity('island'/'HOP', 'Basic Land').
card_artist('island'/'HOP', 'John Avon').
card_number('island'/'HOP', '150').
card_multiverse_id('island'/'HOP', '205289').

card_in_set('isle of vesuva', 'HOP').
card_original_type('isle of vesuva'/'HOP', 'Plane — Dominaria').
card_original_text('isle of vesuva'/'HOP', 'Whenever a nontoken creature enters the battlefield, its controller puts a token onto the battlefield that\'s a copy of that creature.\nWhenever you roll {C}, destroy target creature and all other creatures with the same name as that creature.').
card_first_print('isle of vesuva', 'HOP').
card_image_name('isle of vesuva'/'HOP', 'isle of vesuva').
card_uid('isle of vesuva'/'HOP', 'HOP:Isle of Vesuva:isle of vesuva').
card_rarity('isle of vesuva'/'HOP', 'Common').
card_artist('isle of vesuva'/'HOP', 'Zoltan Boros & Gabor Szikszai').
card_number('isle of vesuva'/'HOP', '18').
card_multiverse_id('isle of vesuva'/'HOP', '198096').

card_in_set('ivy elemental', 'HOP').
card_original_type('ivy elemental'/'HOP', 'Creature — Elemental').
card_original_text('ivy elemental'/'HOP', 'Ivy Elemental enters the battlefield with X +1/+1 counters on it.').
card_image_name('ivy elemental'/'HOP', 'ivy elemental').
card_uid('ivy elemental'/'HOP', 'HOP:Ivy Elemental:ivy elemental').
card_rarity('ivy elemental'/'HOP', 'Rare').
card_artist('ivy elemental'/'HOP', 'Ron Spencer').
card_number('ivy elemental'/'HOP', '74').
card_flavor_text('ivy elemental'/'HOP', 'In the gardens of the centaurs, many travelers have mysteriously vanished while admiring the elaborate topiaries.').
card_multiverse_id('ivy elemental'/'HOP', '206024').

card_in_set('izzet steam maze', 'HOP').
card_original_type('izzet steam maze'/'HOP', 'Plane — Ravnica').
card_original_text('izzet steam maze'/'HOP', 'Whenever a player casts an instant or sorcery spell, that player copies it. The player may choose new targets for the copy.\nWhenever you roll {C}, instant and sorcery spells you cast this turn cost {3} less to cast.').
card_first_print('izzet steam maze', 'HOP').
card_image_name('izzet steam maze'/'HOP', 'izzet steam maze').
card_uid('izzet steam maze'/'HOP', 'HOP:Izzet Steam Maze:izzet steam maze').
card_rarity('izzet steam maze'/'HOP', 'Common').
card_artist('izzet steam maze'/'HOP', 'Franz Vohwinkel').
card_number('izzet steam maze'/'HOP', '19').
card_multiverse_id('izzet steam maze'/'HOP', '198065').

card_in_set('keep watch', 'HOP').
card_original_type('keep watch'/'HOP', 'Instant').
card_original_text('keep watch'/'HOP', 'Draw a card for each attacking creature.').
card_image_name('keep watch'/'HOP', 'keep watch').
card_uid('keep watch'/'HOP', 'HOP:Keep Watch:keep watch').
card_rarity('keep watch'/'HOP', 'Common').
card_artist('keep watch'/'HOP', 'Fred Rahmqvist').
card_number('keep watch'/'HOP', '10').
card_flavor_text('keep watch'/'HOP', '\"I see their moral dilemmas. I see their raw courage. I see their self-sacrifice. I see our victory.\"').
card_multiverse_id('keep watch'/'HOP', '205346').

card_in_set('keldon champion', 'HOP').
card_original_type('keldon champion'/'HOP', 'Creature — Human Barbarian').
card_original_text('keldon champion'/'HOP', 'Haste\nEcho {2}{R}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Keldon Champion enters the battlefield, it deals 3 damage to target player.').
card_image_name('keldon champion'/'HOP', 'keldon champion').
card_uid('keldon champion'/'HOP', 'HOP:Keldon Champion:keldon champion').
card_rarity('keldon champion'/'HOP', 'Uncommon').
card_artist('keldon champion'/'HOP', 'Mark Tedin').
card_number('keldon champion'/'HOP', '58').
card_multiverse_id('keldon champion'/'HOP', '205801').

card_in_set('kor sanctifiers', 'HOP').
card_original_type('kor sanctifiers'/'HOP', 'Creature — Kor Cleric').
card_original_text('kor sanctifiers'/'HOP', 'Kicker {W} (You may pay an additional {W} as you cast this spell.)\nWhen Kor Sanctifiers enters the battlefield, if it was kicked, destroy target artifact or enchantment.').
card_first_print('kor sanctifiers', 'HOP').
card_image_name('kor sanctifiers'/'HOP', 'kor sanctifiers').
card_uid('kor sanctifiers'/'HOP', 'HOP:Kor Sanctifiers:kor sanctifiers').
card_rarity('kor sanctifiers'/'HOP', 'Common').
card_artist('kor sanctifiers'/'HOP', 'Dan Scott').
card_number('kor sanctifiers'/'HOP', '3').
card_flavor_text('kor sanctifiers'/'HOP', '\"Why keep such trinkets? They only add weight to your travels.\"').
card_multiverse_id('kor sanctifiers'/'HOP', '205342').

card_in_set('krosa', 'HOP').
card_original_type('krosa'/'HOP', 'Plane — Dominaria').
card_original_text('krosa'/'HOP', 'All creatures get +2/+2.\nWhenever you roll {C}, you may add {W}{U}{B}{R}{G} to your mana pool.').
card_first_print('krosa', 'HOP').
card_image_name('krosa'/'HOP', 'krosa').
card_uid('krosa'/'HOP', 'HOP:Krosa:krosa').
card_rarity('krosa'/'HOP', 'Common').
card_artist('krosa'/'HOP', 'Steven Belledin').
card_number('krosa'/'HOP', '20').
card_multiverse_id('krosa'/'HOP', '198100').

card_in_set('leaden myr', 'HOP').
card_original_type('leaden myr'/'HOP', 'Artifact Creature — Myr').
card_original_text('leaden myr'/'HOP', '{T}: Add {B} to your mana pool.').
card_image_name('leaden myr'/'HOP', 'leaden myr').
card_uid('leaden myr'/'HOP', 'HOP:Leaden Myr:leaden myr').
card_rarity('leaden myr'/'HOP', 'Common').
card_artist('leaden myr'/'HOP', 'Kev Walker').
card_number('leaden myr'/'HOP', '116').
card_flavor_text('leaden myr'/'HOP', 'The Moriok saw the myr as fellow scavengers, never knowing just who the myr were scavenging for.').
card_multiverse_id('leaden myr'/'HOP', '205290').

card_in_set('leechridden swamp', 'HOP').
card_original_type('leechridden swamp'/'HOP', 'Land — Swamp').
card_original_text('leechridden swamp'/'HOP', '({T}: Add {B} to your mana pool.)\nLeechridden Swamp enters the battlefield tapped.\n{B}, {T}: Each opponent loses 1 life. Activate this ability only if you control two or more black permanents.').
card_image_name('leechridden swamp'/'HOP', 'leechridden swamp').
card_uid('leechridden swamp'/'HOP', 'HOP:Leechridden Swamp:leechridden swamp').
card_rarity('leechridden swamp'/'HOP', 'Uncommon').
card_artist('leechridden swamp'/'HOP', 'Lars Grant-West').
card_number('leechridden swamp'/'HOP', '135').
card_multiverse_id('leechridden swamp'/'HOP', '205401').

card_in_set('lethe lake', 'HOP').
card_original_type('lethe lake'/'HOP', 'Plane — Arkhos').
card_original_text('lethe lake'/'HOP', 'At the beginning of your upkeep, put the top ten cards of your library into your graveyard.\nWhenever you roll {C}, target player puts the top ten cards of his or her library into his or her graveyard.').
card_first_print('lethe lake', 'HOP').
card_image_name('lethe lake'/'HOP', 'lethe lake').
card_uid('lethe lake'/'HOP', 'HOP:Lethe Lake:lethe lake').
card_rarity('lethe lake'/'HOP', 'Common').
card_artist('lethe lake'/'HOP', 'Chris J. Anderson').
card_number('lethe lake'/'HOP', '21').
card_multiverse_id('lethe lake'/'HOP', '198105').

card_in_set('lightning helix', 'HOP').
card_original_type('lightning helix'/'HOP', 'Instant').
card_original_text('lightning helix'/'HOP', 'Lightning Helix deals 3 damage to target creature or player and you gain 3 life.').
card_image_name('lightning helix'/'HOP', 'lightning helix').
card_uid('lightning helix'/'HOP', 'HOP:Lightning Helix:lightning helix').
card_rarity('lightning helix'/'HOP', 'Uncommon').
card_artist('lightning helix'/'HOP', 'Kev Walker').
card_number('lightning helix'/'HOP', '90').
card_flavor_text('lightning helix'/'HOP', 'Though less well-known than its army of soldiers, the Boros Legion\'s mage-priests are as respected by the innocent as they are hated by the ghosts of the guilty.').
card_multiverse_id('lightning helix'/'HOP', '205361').

card_in_set('living hive', 'HOP').
card_original_type('living hive'/'HOP', 'Creature — Elemental Insect').
card_original_text('living hive'/'HOP', 'Trample\nWhenever Living Hive deals combat damage to a player, put that many 1/1 green Insect creature tokens onto the battlefield.').
card_image_name('living hive'/'HOP', 'living hive').
card_uid('living hive'/'HOP', 'HOP:Living Hive:living hive').
card_rarity('living hive'/'HOP', 'Rare').
card_artist('living hive'/'HOP', 'Anthony S. Waters').
card_number('living hive'/'HOP', '75').
card_flavor_text('living hive'/'HOP', 'In its center is a single red ant, a queen that regulates the hive\'s movements.').
card_multiverse_id('living hive'/'HOP', '205315').

card_in_set('llanowar', 'HOP').
card_original_type('llanowar'/'HOP', 'Plane — Dominaria').
card_original_text('llanowar'/'HOP', 'All creatures have \"{T}: Add {G}{G} to your mana pool.\"\nWhenever you roll {C}, untap all creatures you control.').
card_first_print('llanowar', 'HOP').
card_image_name('llanowar'/'HOP', 'llanowar').
card_uid('llanowar'/'HOP', 'HOP:Llanowar:llanowar').
card_rarity('llanowar'/'HOP', 'Common').
card_artist('llanowar'/'HOP', 'Kev Walker').
card_number('llanowar'/'HOP', '22').
card_multiverse_id('llanowar'/'HOP', '198107').

card_in_set('lodestone myr', 'HOP').
card_original_type('lodestone myr'/'HOP', 'Artifact Creature — Myr').
card_original_text('lodestone myr'/'HOP', 'Trample\nTap an untapped artifact you control: Lodestone Myr gets +1/+1 until end of turn.').
card_image_name('lodestone myr'/'HOP', 'lodestone myr').
card_uid('lodestone myr'/'HOP', 'HOP:Lodestone Myr:lodestone myr').
card_rarity('lodestone myr'/'HOP', 'Rare').
card_artist('lodestone myr'/'HOP', 'Greg Staples').
card_number('lodestone myr'/'HOP', '117').
card_flavor_text('lodestone myr'/'HOP', 'When necessary, myr can override and control any artificial object, as can their creator.').
card_multiverse_id('lodestone myr'/'HOP', '205291').

card_in_set('loxodon warhammer', 'HOP').
card_original_type('loxodon warhammer'/'HOP', 'Artifact — Equipment').
card_original_text('loxodon warhammer'/'HOP', 'Equipped creature gets +3/+0 and has lifelink and trample.\nEquip {3}').
card_image_name('loxodon warhammer'/'HOP', 'loxodon warhammer').
card_uid('loxodon warhammer'/'HOP', 'HOP:Loxodon Warhammer:loxodon warhammer').
card_rarity('loxodon warhammer'/'HOP', 'Rare').
card_artist('loxodon warhammer'/'HOP', 'Jeremy Jarvis').
card_number('loxodon warhammer'/'HOP', '118').
card_multiverse_id('loxodon warhammer'/'HOP', '205270').

card_in_set('mage slayer', 'HOP').
card_original_type('mage slayer'/'HOP', 'Artifact — Equipment').
card_original_text('mage slayer'/'HOP', 'Whenever equipped creature attacks, it deals damage equal to its power to defending player.\nEquip {3}').
card_image_name('mage slayer'/'HOP', 'mage slayer').
card_uid('mage slayer'/'HOP', 'HOP:Mage Slayer:mage slayer').
card_rarity('mage slayer'/'HOP', 'Uncommon').
card_artist('mage slayer'/'HOP', 'Lars Grant-West').
card_number('mage slayer'/'HOP', '91').
card_flavor_text('mage slayer'/'HOP', '\"When the wielder is strong enough, any sword will do.\"\n—Kresh the Bloodbraided').
card_multiverse_id('mage slayer'/'HOP', '205319').

card_in_set('mask of memory', 'HOP').
card_original_type('mask of memory'/'HOP', 'Artifact — Equipment').
card_original_text('mask of memory'/'HOP', 'Whenever equipped creature deals combat damage to a player, you may draw two cards. If you do, discard a card.\nEquip {1}').
card_image_name('mask of memory'/'HOP', 'mask of memory').
card_uid('mask of memory'/'HOP', 'HOP:Mask of Memory:mask of memory').
card_rarity('mask of memory'/'HOP', 'Uncommon').
card_artist('mask of memory'/'HOP', 'Alan Pollack').
card_number('mask of memory'/'HOP', '119').
card_multiverse_id('mask of memory'/'HOP', '205316').

card_in_set('master of etherium', 'HOP').
card_original_type('master of etherium'/'HOP', 'Artifact Creature — Vedalken Wizard').
card_original_text('master of etherium'/'HOP', 'Master of Etherium\'s power and toughness are each equal to the number of artifacts you control.\nOther artifact creatures you control get +1/+1.').
card_image_name('master of etherium'/'HOP', 'master of etherium').
card_uid('master of etherium'/'HOP', 'HOP:Master of Etherium:master of etherium').
card_rarity('master of etherium'/'HOP', 'Rare').
card_artist('master of etherium'/'HOP', 'Matt Cavotta').
card_number('master of etherium'/'HOP', '11').
card_flavor_text('master of etherium'/'HOP', '\"Only a mind unfettered with the concerns of the flesh can see the world as it truly is.\"').
card_multiverse_id('master of etherium'/'HOP', '205325').

card_in_set('menacing ogre', 'HOP').
card_original_type('menacing ogre'/'HOP', 'Creature — Ogre').
card_original_text('menacing ogre'/'HOP', 'Trample, haste\nWhen Menacing Ogre enters the battlefield, each player secretly chooses a number. Then those numbers are revealed. Each player with the highest number loses that much life. If you are one of those players, put two +1/+1 counters on Menacing Ogre.').
card_image_name('menacing ogre'/'HOP', 'menacing ogre').
card_uid('menacing ogre'/'HOP', 'HOP:Menacing Ogre:menacing ogre').
card_rarity('menacing ogre'/'HOP', 'Rare').
card_artist('menacing ogre'/'HOP', 'Ron Spencer').
card_number('menacing ogre'/'HOP', '59').
card_multiverse_id('menacing ogre'/'HOP', '205370').

card_in_set('minamo', 'HOP').
card_original_type('minamo'/'HOP', 'Plane — Kamigawa').
card_original_text('minamo'/'HOP', 'Whenever a player casts a spell, that player may draw a card.\nWhenever you roll {C}, each player may return a blue card from his or her graveyard to his or her hand.').
card_first_print('minamo', 'HOP').
card_image_name('minamo'/'HOP', 'minamo').
card_uid('minamo'/'HOP', 'HOP:Minamo:minamo').
card_rarity('minamo'/'HOP', 'Common').
card_artist('minamo'/'HOP', 'Charles Urbach').
card_number('minamo'/'HOP', '24').
card_multiverse_id('minamo'/'HOP', '198078').

card_in_set('mountain', 'HOP').
card_original_type('mountain'/'HOP', 'Basic Land — Mountain').
card_original_text('mountain'/'HOP', 'R').
card_image_name('mountain'/'HOP', 'mountain1').
card_uid('mountain'/'HOP', 'HOP:Mountain:mountain1').
card_rarity('mountain'/'HOP', 'Basic Land').
card_artist('mountain'/'HOP', 'Craig Mullins').
card_number('mountain'/'HOP', '156').
card_multiverse_id('mountain'/'HOP', '205468').

card_in_set('mountain', 'HOP').
card_original_type('mountain'/'HOP', 'Basic Land — Mountain').
card_original_text('mountain'/'HOP', 'R').
card_image_name('mountain'/'HOP', 'mountain2').
card_uid('mountain'/'HOP', 'HOP:Mountain:mountain2').
card_rarity('mountain'/'HOP', 'Basic Land').
card_artist('mountain'/'HOP', 'Aleksi Briclot').
card_number('mountain'/'HOP', '157').
card_multiverse_id('mountain'/'HOP', '205471').

card_in_set('mountain', 'HOP').
card_original_type('mountain'/'HOP', 'Basic Land — Mountain').
card_original_text('mountain'/'HOP', 'R').
card_image_name('mountain'/'HOP', 'mountain3').
card_uid('mountain'/'HOP', 'HOP:Mountain:mountain3').
card_rarity('mountain'/'HOP', 'Basic Land').
card_artist('mountain'/'HOP', 'Aleksi Briclot').
card_number('mountain'/'HOP', '158').
card_multiverse_id('mountain'/'HOP', '205470').

card_in_set('mountain', 'HOP').
card_original_type('mountain'/'HOP', 'Basic Land — Mountain').
card_original_text('mountain'/'HOP', 'R').
card_image_name('mountain'/'HOP', 'mountain4').
card_uid('mountain'/'HOP', 'HOP:Mountain:mountain4').
card_rarity('mountain'/'HOP', 'Basic Land').
card_artist('mountain'/'HOP', 'Nils Hamm').
card_number('mountain'/'HOP', '159').
card_multiverse_id('mountain'/'HOP', '205469').

card_in_set('mountain', 'HOP').
card_original_type('mountain'/'HOP', 'Basic Land — Mountain').
card_original_text('mountain'/'HOP', 'R').
card_image_name('mountain'/'HOP', 'mountain5').
card_uid('mountain'/'HOP', 'HOP:Mountain:mountain5').
card_rarity('mountain'/'HOP', 'Basic Land').
card_artist('mountain'/'HOP', 'Mark Tedin').
card_number('mountain'/'HOP', '160').
card_multiverse_id('mountain'/'HOP', '205292').

card_in_set('mountain', 'HOP').
card_original_type('mountain'/'HOP', 'Basic Land — Mountain').
card_original_text('mountain'/'HOP', 'R').
card_image_name('mountain'/'HOP', 'mountain6').
card_uid('mountain'/'HOP', 'HOP:Mountain:mountain6').
card_rarity('mountain'/'HOP', 'Basic Land').
card_artist('mountain'/'HOP', 'Stephan Martiniere').
card_number('mountain'/'HOP', '161').
card_multiverse_id('mountain'/'HOP', '205447').

card_in_set('mountain', 'HOP').
card_original_type('mountain'/'HOP', 'Basic Land — Mountain').
card_original_text('mountain'/'HOP', 'R').
card_image_name('mountain'/'HOP', 'mountain7').
card_uid('mountain'/'HOP', 'HOP:Mountain:mountain7').
card_rarity('mountain'/'HOP', 'Basic Land').
card_artist('mountain'/'HOP', 'Richard Wright').
card_number('mountain'/'HOP', '162').
card_multiverse_id('mountain'/'HOP', '205432').

card_in_set('mountain', 'HOP').
card_original_type('mountain'/'HOP', 'Basic Land — Mountain').
card_original_text('mountain'/'HOP', 'R').
card_image_name('mountain'/'HOP', 'mountain8').
card_uid('mountain'/'HOP', 'HOP:Mountain:mountain8').
card_rarity('mountain'/'HOP', 'Basic Land').
card_artist('mountain'/'HOP', 'Dave Kendall').
card_number('mountain'/'HOP', '163').
card_multiverse_id('mountain'/'HOP', '205441').

card_in_set('mountain', 'HOP').
card_original_type('mountain'/'HOP', 'Basic Land — Mountain').
card_original_text('mountain'/'HOP', 'R').
card_image_name('mountain'/'HOP', 'mountain9').
card_uid('mountain'/'HOP', 'HOP:Mountain:mountain9').
card_rarity('mountain'/'HOP', 'Basic Land').
card_artist('mountain'/'HOP', 'Brandon Kitkouski').
card_number('mountain'/'HOP', '164').
card_multiverse_id('mountain'/'HOP', '205435').

card_in_set('murasa', 'HOP').
card_original_type('murasa'/'HOP', 'Plane — Zendikar').
card_original_text('murasa'/'HOP', 'Whenever a nontoken creature enters the battlefield, its controller may search his or her library for a basic land card, put it onto the battlefield tapped, then shuffle his or her library.\nWhenever you roll {C}, target land becomes a 4/4 creature that\'s still a land.').
card_first_print('murasa', 'HOP').
card_image_name('murasa'/'HOP', 'murasa').
card_uid('murasa'/'HOP', 'HOP:Murasa:murasa').
card_rarity('murasa'/'HOP', 'Common').
card_artist('murasa'/'HOP', 'Jung Park').
card_number('murasa'/'HOP', '25').
card_multiverse_id('murasa'/'HOP', '198074').

card_in_set('myr enforcer', 'HOP').
card_original_type('myr enforcer'/'HOP', 'Artifact Creature — Myr').
card_original_text('myr enforcer'/'HOP', 'Affinity for artifacts (This spell costs {1} less to cast for each artifact you control.)').
card_image_name('myr enforcer'/'HOP', 'myr enforcer').
card_uid('myr enforcer'/'HOP', 'HOP:Myr Enforcer:myr enforcer').
card_rarity('myr enforcer'/'HOP', 'Common').
card_artist('myr enforcer'/'HOP', 'Greg Staples').
card_number('myr enforcer'/'HOP', '120').
card_flavor_text('myr enforcer'/'HOP', 'Most myr monitor other species. Some myr monitor other myr.').
card_multiverse_id('myr enforcer'/'HOP', '205296').

card_in_set('naar isle', 'HOP').
card_original_type('naar isle'/'HOP', 'Plane — Wildfire').
card_original_text('naar isle'/'HOP', 'At the beginning of your upkeep, put a flame counter on Naar Isle, then Naar Isle deals damage to you equal to the number of flame counters on it.\nWhenever you roll {C}, Naar Isle deals 3 damage to target player.').
card_first_print('naar isle', 'HOP').
card_image_name('naar isle'/'HOP', 'naar isle').
card_uid('naar isle'/'HOP', 'HOP:Naar Isle:naar isle').
card_rarity('naar isle'/'HOP', 'Common').
card_artist('naar isle'/'HOP', 'Mark Zug').
card_number('naar isle'/'HOP', '26').
card_multiverse_id('naar isle'/'HOP', '198070').

card_in_set('naya', 'HOP').
card_original_type('naya'/'HOP', 'Plane — Alara').
card_original_text('naya'/'HOP', 'You may play any number of additional lands on each of your turns.\nWhenever you roll {C}, target red, green, or white creature you control gets +1/+1 until end of turn for each land you control.').
card_first_print('naya', 'HOP').
card_image_name('naya'/'HOP', 'naya').
card_uid('naya'/'HOP', 'HOP:Naya:naya').
card_rarity('naya'/'HOP', 'Common').
card_artist('naya'/'HOP', 'Zoltan Boros & Gabor Szikszai').
card_number('naya'/'HOP', '27').
card_multiverse_id('naya'/'HOP', '198103').

card_in_set('nefashu', 'HOP').
card_original_type('nefashu'/'HOP', 'Creature — Zombie Mutant').
card_original_text('nefashu'/'HOP', 'Whenever Nefashu attacks, up to five target creatures each get -1/-1 until end of turn.').
card_image_name('nefashu'/'HOP', 'nefashu').
card_uid('nefashu'/'HOP', 'HOP:Nefashu:nefashu').
card_rarity('nefashu'/'HOP', 'Rare').
card_artist('nefashu'/'HOP', 'rk post').
card_number('nefashu'/'HOP', '34').
card_flavor_text('nefashu'/'HOP', 'Where the nefashu pass, blood rolls like a silk carpet.').
card_multiverse_id('nefashu'/'HOP', '205274').

card_in_set('noxious ghoul', 'HOP').
card_original_type('noxious ghoul'/'HOP', 'Creature — Zombie').
card_original_text('noxious ghoul'/'HOP', 'Whenever Noxious Ghoul or another Zombie enters the battlefield, all non-Zombie creatures get -1/-1 until end of turn.').
card_image_name('noxious ghoul'/'HOP', 'noxious ghoul').
card_uid('noxious ghoul'/'HOP', 'HOP:Noxious Ghoul:noxious ghoul').
card_rarity('noxious ghoul'/'HOP', 'Uncommon').
card_artist('noxious ghoul'/'HOP', 'Luca Zontini').
card_number('noxious ghoul'/'HOP', '35').
card_flavor_text('noxious ghoul'/'HOP', 'Plague and death wrapped in one convenient package.').
card_multiverse_id('noxious ghoul'/'HOP', '205420').

card_in_set('nuisance engine', 'HOP').
card_original_type('nuisance engine'/'HOP', 'Artifact').
card_original_text('nuisance engine'/'HOP', '{2}, {T}: Put a 0/1 colorless Pest artifact creature token onto the battlefield.').
card_image_name('nuisance engine'/'HOP', 'nuisance engine').
card_uid('nuisance engine'/'HOP', 'HOP:Nuisance Engine:nuisance engine').
card_rarity('nuisance engine'/'HOP', 'Uncommon').
card_artist('nuisance engine'/'HOP', 'Stephen Tappin').
card_number('nuisance engine'/'HOP', '121').
card_flavor_text('nuisance engine'/'HOP', 'All Auriok children know the tale, \"Bolgri and the Long Day of Squashing.\"').
card_multiverse_id('nuisance engine'/'HOP', '205297').

card_in_set('oblivion ring', 'HOP').
card_original_type('oblivion ring'/'HOP', 'Enchantment').
card_original_text('oblivion ring'/'HOP', 'When Oblivion Ring enters the battlefield, exile another target nonland permanent. \nWhen Oblivion Ring leaves the battlefield, return the exiled card to the battlefield under its owner\'s control.').
card_image_name('oblivion ring'/'HOP', 'oblivion ring').
card_uid('oblivion ring'/'HOP', 'HOP:Oblivion Ring:oblivion ring').
card_rarity('oblivion ring'/'HOP', 'Common').
card_artist('oblivion ring'/'HOP', 'Franz Vohwinkel').
card_number('oblivion ring'/'HOP', '4').
card_multiverse_id('oblivion ring'/'HOP', '205396').

card_in_set('order', 'HOP').
card_original_type('order'/'HOP', 'Instant').
card_original_text('order'/'HOP', 'Exile target attacking creature.\n//\nChaos\n{2}{R}\nInstant\nCreatures can\'t block this turn.').
card_image_name('order'/'HOP', 'orderchaos').
card_uid('order'/'HOP', 'HOP:Order:orderchaos').
card_rarity('order'/'HOP', 'Uncommon').
card_artist('order'/'HOP', 'Tim Hildebrandt').
card_number('order'/'HOP', '104a').
card_multiverse_id('order'/'HOP', '205384').

card_in_set('orim\'s thunder', 'HOP').
card_original_type('orim\'s thunder'/'HOP', 'Instant').
card_original_text('orim\'s thunder'/'HOP', 'Kicker {R} (You may pay an additional {R} as you cast this spell.)\nDestroy target artifact or enchantment. If Orim\'s Thunder was kicked, it deals damage equal to that permanent\'s converted mana cost to target creature.').
card_image_name('orim\'s thunder'/'HOP', 'orim\'s thunder').
card_uid('orim\'s thunder'/'HOP', 'HOP:Orim\'s Thunder:orim\'s thunder').
card_rarity('orim\'s thunder'/'HOP', 'Common').
card_artist('orim\'s thunder'/'HOP', 'Carl Critchlow').
card_number('orim\'s thunder'/'HOP', '5').
card_multiverse_id('orim\'s thunder'/'HOP', '205385').

card_in_set('otaria', 'HOP').
card_original_type('otaria'/'HOP', 'Plane — Dominaria').
card_original_text('otaria'/'HOP', 'Instant and sorcery cards in graveyards have flashback. The flashback cost is equal to the card\'s mana cost. (Its owner may cast the card from his or her graveyard for its mana cost. Then he or she exiles it.)\nWhenever you roll {C}, take an extra turn after this one.').
card_first_print('otaria', 'HOP').
card_image_name('otaria'/'HOP', 'otaria').
card_uid('otaria'/'HOP', 'HOP:Otaria:otaria').
card_rarity('otaria'/'HOP', 'Common').
card_artist('otaria'/'HOP', 'Charles Urbach').
card_number('otaria'/'HOP', '28').
card_multiverse_id('otaria'/'HOP', '198095').

card_in_set('panopticon', 'HOP').
card_original_type('panopticon'/'HOP', 'Plane — Mirrodin').
card_original_text('panopticon'/'HOP', 'When you planeswalk to Panopticon, draw a card.\nAt the beginning of your draw step, draw an additional card.\nWhenever you roll {C}, draw a card.').
card_first_print('panopticon', 'HOP').
card_image_name('panopticon'/'HOP', 'panopticon').
card_uid('panopticon'/'HOP', 'HOP:Panopticon:panopticon').
card_rarity('panopticon'/'HOP', 'Common').
card_artist('panopticon'/'HOP', 'John Avon').
card_number('panopticon'/'HOP', '29').
card_multiverse_id('panopticon'/'HOP', '198064').

card_in_set('pentad prism', 'HOP').
card_original_type('pentad prism'/'HOP', 'Artifact').
card_original_text('pentad prism'/'HOP', 'Sunburst (This enters the battlefield with a charge counter on it for each color of mana spent to cast it.)\nRemove a charge counter from Pentad Prism: Add one mana of any color to your mana pool.').
card_image_name('pentad prism'/'HOP', 'pentad prism').
card_uid('pentad prism'/'HOP', 'HOP:Pentad Prism:pentad prism').
card_rarity('pentad prism'/'HOP', 'Common').
card_artist('pentad prism'/'HOP', 'David Martin').
card_number('pentad prism'/'HOP', '122').
card_multiverse_id('pentad prism'/'HOP', '205331').

card_in_set('pentavus', 'HOP').
card_original_type('pentavus'/'HOP', 'Artifact Creature — Construct').
card_original_text('pentavus'/'HOP', 'Pentavus enters the battlefield with five +1/+1 counters on it.\n{1}, Remove a +1/+1 counter from Pentavus: Put a 1/1 colorless Pentavite artifact creature token with flying onto the battlefield.\n{1}, Sacrifice a Pentavite: Put a +1/+1 counter on Pentavus.').
card_image_name('pentavus'/'HOP', 'pentavus').
card_uid('pentavus'/'HOP', 'HOP:Pentavus:pentavus').
card_rarity('pentavus'/'HOP', 'Rare').
card_artist('pentavus'/'HOP', 'Greg Staples').
card_number('pentavus'/'HOP', '123').
card_multiverse_id('pentavus'/'HOP', '205298').

card_in_set('phyrexian arena', 'HOP').
card_original_type('phyrexian arena'/'HOP', 'Enchantment').
card_original_text('phyrexian arena'/'HOP', 'At the beginning of your upkeep, you draw a card and you lose 1 life.').
card_image_name('phyrexian arena'/'HOP', 'phyrexian arena').
card_uid('phyrexian arena'/'HOP', 'HOP:Phyrexian Arena:phyrexian arena').
card_rarity('phyrexian arena'/'HOP', 'Rare').
card_artist('phyrexian arena'/'HOP', 'Carl Critchlow').
card_number('phyrexian arena'/'HOP', '36').
card_flavor_text('phyrexian arena'/'HOP', 'A drop of humanity for a sea of power.').
card_multiverse_id('phyrexian arena'/'HOP', '205417').

card_in_set('phyrexian ghoul', 'HOP').
card_original_type('phyrexian ghoul'/'HOP', 'Creature — Zombie').
card_original_text('phyrexian ghoul'/'HOP', 'Sacrifice a creature: Phyrexian Ghoul gets +2/+2 until end of turn.').
card_image_name('phyrexian ghoul'/'HOP', 'phyrexian ghoul').
card_uid('phyrexian ghoul'/'HOP', 'HOP:Phyrexian Ghoul:phyrexian ghoul').
card_rarity('phyrexian ghoul'/'HOP', 'Common').
card_artist('phyrexian ghoul'/'HOP', 'Pete Venters').
card_number('phyrexian ghoul'/'HOP', '37').
card_flavor_text('phyrexian ghoul'/'HOP', 'Phyrexia wastes nothing. Its food chain is a spiraling cycle.').
card_multiverse_id('phyrexian ghoul'/'HOP', '205389').

card_in_set('plains', 'HOP').
card_original_type('plains'/'HOP', 'Basic Land — Plains').
card_original_text('plains'/'HOP', 'W').
card_image_name('plains'/'HOP', 'plains1').
card_uid('plains'/'HOP', 'HOP:Plains:plains1').
card_rarity('plains'/'HOP', 'Basic Land').
card_artist('plains'/'HOP', 'Martina Pilcerova').
card_number('plains'/'HOP', '142').
card_multiverse_id('plains'/'HOP', '205301').

card_in_set('plains', 'HOP').
card_original_type('plains'/'HOP', 'Basic Land — Plains').
card_original_text('plains'/'HOP', 'W').
card_image_name('plains'/'HOP', 'plains2').
card_uid('plains'/'HOP', 'HOP:Plains:plains2').
card_rarity('plains'/'HOP', 'Basic Land').
card_artist('plains'/'HOP', 'Larry MacDougall').
card_number('plains'/'HOP', '143').
card_multiverse_id('plains'/'HOP', '205430').

card_in_set('plains', 'HOP').
card_original_type('plains'/'HOP', 'Basic Land — Plains').
card_original_text('plains'/'HOP', 'W').
card_image_name('plains'/'HOP', 'plains3').
card_uid('plains'/'HOP', 'HOP:Plains:plains3').
card_rarity('plains'/'HOP', 'Basic Land').
card_artist('plains'/'HOP', 'Richard Wright').
card_number('plains'/'HOP', '144').
card_multiverse_id('plains'/'HOP', '205428').

card_in_set('plains', 'HOP').
card_original_type('plains'/'HOP', 'Basic Land — Plains').
card_original_text('plains'/'HOP', 'W').
card_image_name('plains'/'HOP', 'plains4').
card_uid('plains'/'HOP', 'HOP:Plains:plains4').
card_rarity('plains'/'HOP', 'Basic Land').
card_artist('plains'/'HOP', 'Dave Kendall').
card_number('plains'/'HOP', '145').
card_multiverse_id('plains'/'HOP', '205436').

card_in_set('plains', 'HOP').
card_original_type('plains'/'HOP', 'Basic Land — Plains').
card_original_text('plains'/'HOP', 'W').
card_image_name('plains'/'HOP', 'plains5').
card_uid('plains'/'HOP', 'HOP:Plains:plains5').
card_rarity('plains'/'HOP', 'Basic Land').
card_artist('plains'/'HOP', 'Stephan Martiniere').
card_number('plains'/'HOP', '146').
card_multiverse_id('plains'/'HOP', '205440').

card_in_set('pools of becoming', 'HOP').
card_original_type('pools of becoming'/'HOP', 'Plane — Bolas\'s Meditation Realm').
card_original_text('pools of becoming'/'HOP', 'At the beginning of your end step, put the cards in your hand on the bottom of your library in any order, then draw that many cards.\nWhenever you roll {C}, reveal the top three cards of your planar deck. Each of the revealed cards\' chaos abilities triggers. Then put the revealed cards on the bottom of your planar deck in any order.').
card_first_print('pools of becoming', 'HOP').
card_image_name('pools of becoming'/'HOP', 'pools of becoming').
card_uid('pools of becoming'/'HOP', 'HOP:Pools of Becoming:pools of becoming').
card_rarity('pools of becoming'/'HOP', 'Common').
card_artist('pools of becoming'/'HOP', 'Jason Chan').
card_number('pools of becoming'/'HOP', '30').
card_multiverse_id('pools of becoming'/'HOP', '198080').

card_in_set('prison term', 'HOP').
card_original_type('prison term'/'HOP', 'Enchantment — Aura').
card_original_text('prison term'/'HOP', 'Enchant creature\nEnchanted creature can\'t attack or block and its activated abilities can\'t be activated.\nWhenever a creature enters the battlefield under an opponent\'s control, you may attach Prison Term to that creature.').
card_image_name('prison term'/'HOP', 'prison term').
card_uid('prison term'/'HOP', 'HOP:Prison Term:prison term').
card_rarity('prison term'/'HOP', 'Uncommon').
card_artist('prison term'/'HOP', 'Zoltan Boros & Gabor Szikszai').
card_number('prison term'/'HOP', '6').
card_multiverse_id('prison term'/'HOP', '205402').

card_in_set('profane command', 'HOP').
card_original_type('profane command'/'HOP', 'Sorcery').
card_original_text('profane command'/'HOP', 'Choose two — Target player loses X life; or return target creature card with converted mana cost X or less from your graveyard to the battlefield; or target creature gets -X/-X until end of turn; or up to X target creatures gain fear until end of turn.').
card_image_name('profane command'/'HOP', 'profane command').
card_uid('profane command'/'HOP', 'HOP:Profane Command:profane command').
card_rarity('profane command'/'HOP', 'Rare').
card_artist('profane command'/'HOP', 'Wayne England').
card_number('profane command'/'HOP', '38').
card_multiverse_id('profane command'/'HOP', '205397').

card_in_set('pyrotechnics', 'HOP').
card_original_type('pyrotechnics'/'HOP', 'Sorcery').
card_original_text('pyrotechnics'/'HOP', 'Pyrotechnics deals 4 damage divided as you choose among any number of target creatures and/or players.').
card_image_name('pyrotechnics'/'HOP', 'pyrotechnics').
card_uid('pyrotechnics'/'HOP', 'HOP:Pyrotechnics:pyrotechnics').
card_rarity('pyrotechnics'/'HOP', 'Uncommon').
card_artist('pyrotechnics'/'HOP', 'John Avon').
card_number('pyrotechnics'/'HOP', '60').
card_flavor_text('pyrotechnics'/'HOP', '\"Who says lightning never strikes twice?\"\n—Storm shaman').
card_multiverse_id('pyrotechnics'/'HOP', '205405').

card_in_set('qumulox', 'HOP').
card_original_type('qumulox'/'HOP', 'Creature — Beast').
card_original_text('qumulox'/'HOP', 'Affinity for artifacts (This spell costs {1} less to cast for each artifact you control.)\nFlying').
card_image_name('qumulox'/'HOP', 'qumulox').
card_uid('qumulox'/'HOP', 'HOP:Qumulox:qumulox').
card_rarity('qumulox'/'HOP', 'Uncommon').
card_artist('qumulox'/'HOP', 'Carl Critchlow').
card_number('qumulox'/'HOP', '12').
card_flavor_text('qumulox'/'HOP', 'Even the clouds bend themselves to Memnarch\'s will, eager to swallow those who oppose him.').
card_multiverse_id('qumulox'/'HOP', '205332').

card_in_set('rampant growth', 'HOP').
card_original_type('rampant growth'/'HOP', 'Sorcery').
card_original_text('rampant growth'/'HOP', 'Search your library for a basic land card and put that card onto the battlefield tapped. Then shuffle your library.').
card_image_name('rampant growth'/'HOP', 'rampant growth').
card_uid('rampant growth'/'HOP', 'HOP:Rampant Growth:rampant growth').
card_rarity('rampant growth'/'HOP', 'Common').
card_artist('rampant growth'/'HOP', 'Steven Belledin').
card_number('rampant growth'/'HOP', '76').
card_flavor_text('rampant growth'/'HOP', 'Nature grows solutions to its problems.').
card_multiverse_id('rampant growth'/'HOP', '205350').

card_in_set('raven\'s run', 'HOP').
card_original_type('raven\'s run'/'HOP', 'Plane — Shadowmoor').
card_original_text('raven\'s run'/'HOP', 'All creatures have wither. (They deal damage to creatures in the form of -1/-1 counters.)\nWhenever you roll {C}, put a -1/-1 counter on target creature, two -1/-1 counters on another target creature, and three -1/-1 counters on a third target creature.').
card_first_print('raven\'s run', 'HOP').
card_image_name('raven\'s run'/'HOP', 'raven\'s run').
card_uid('raven\'s run'/'HOP', 'HOP:Raven\'s Run:raven\'s run').
card_rarity('raven\'s run'/'HOP', 'Common').
card_artist('raven\'s run'/'HOP', 'Omar Rayyan').
card_number('raven\'s run'/'HOP', '31').
card_multiverse_id('raven\'s run'/'HOP', '198076').

card_in_set('razia, boros archangel', 'HOP').
card_original_type('razia, boros archangel'/'HOP', 'Legendary Creature — Angel').
card_original_text('razia, boros archangel'/'HOP', 'Flying, vigilance, haste\n{T}: The next 3 damage that would be dealt to target creature you control this turn is dealt to another target creature instead.').
card_image_name('razia, boros archangel'/'HOP', 'razia, boros archangel').
card_uid('razia, boros archangel'/'HOP', 'HOP:Razia, Boros Archangel:razia, boros archangel').
card_rarity('razia, boros archangel'/'HOP', 'Rare').
card_artist('razia, boros archangel'/'HOP', 'Donato Giancola').
card_number('razia, boros archangel'/'HOP', '92').
card_flavor_text('razia, boros archangel'/'HOP', 'Her sword burns with such brightness that foes avert their eyes and arrows divert their paths.').
card_multiverse_id('razia, boros archangel'/'HOP', '205362').

card_in_set('reckless charge', 'HOP').
card_original_type('reckless charge'/'HOP', 'Sorcery').
card_original_text('reckless charge'/'HOP', 'Target creature gets +3/+0 and gains haste until end of turn.\nFlashback {2}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('reckless charge'/'HOP', 'reckless charge').
card_uid('reckless charge'/'HOP', 'HOP:Reckless Charge:reckless charge').
card_rarity('reckless charge'/'HOP', 'Common').
card_artist('reckless charge'/'HOP', 'Scott M. Fischer').
card_number('reckless charge'/'HOP', '61').
card_multiverse_id('reckless charge'/'HOP', '205365').

card_in_set('relentless assault', 'HOP').
card_original_type('relentless assault'/'HOP', 'Sorcery').
card_original_text('relentless assault'/'HOP', 'Untap all creatures that attacked this turn. After this main phase, there is an additional combat phase followed by an additional main phase.').
card_image_name('relentless assault'/'HOP', 'relentless assault').
card_uid('relentless assault'/'HOP', 'HOP:Relentless Assault:relentless assault').
card_rarity('relentless assault'/'HOP', 'Rare').
card_artist('relentless assault'/'HOP', 'Greg Hildebrandt').
card_number('relentless assault'/'HOP', '62').
card_flavor_text('relentless assault'/'HOP', '\"Mercy? Mercy is for the playground, not the battleground.\"').
card_multiverse_id('relentless assault'/'HOP', '205271').

card_in_set('relic of progenitus', 'HOP').
card_original_type('relic of progenitus'/'HOP', 'Artifact').
card_original_text('relic of progenitus'/'HOP', '{T}: Target player exiles a card from his or her graveyard.\n{1}, Exile Relic of Progenitus: Exile all cards from all graveyards. Draw a card.').
card_image_name('relic of progenitus'/'HOP', 'relic of progenitus').
card_uid('relic of progenitus'/'HOP', 'HOP:Relic of Progenitus:relic of progenitus').
card_rarity('relic of progenitus'/'HOP', 'Common').
card_artist('relic of progenitus'/'HOP', 'Jean-Sébastien Rossbach').
card_number('relic of progenitus'/'HOP', '124').
card_flavor_text('relic of progenitus'/'HOP', 'Elves believe the hydra-god Progenitus sleeps beneath Naya, feeding on forgotten magics.').
card_multiverse_id('relic of progenitus'/'HOP', '205326').

card_in_set('rockslide elemental', 'HOP').
card_original_type('rockslide elemental'/'HOP', 'Creature — Elemental').
card_original_text('rockslide elemental'/'HOP', 'First strike\nWhenever another creature is put into a graveyard from the battlefield, you may put a +1/+1 counter on Rockslide Elemental.').
card_image_name('rockslide elemental'/'HOP', 'rockslide elemental').
card_uid('rockslide elemental'/'HOP', 'HOP:Rockslide Elemental:rockslide elemental').
card_rarity('rockslide elemental'/'HOP', 'Uncommon').
card_artist('rockslide elemental'/'HOP', 'Joshua Hagler').
card_number('rockslide elemental'/'HOP', '63').
card_flavor_text('rockslide elemental'/'HOP', 'On Jund, even the landscape is a deadly hunter. Volcanoes, tar pits, and rock slides seem to prey upon the living.').
card_multiverse_id('rockslide elemental'/'HOP', '205327').

card_in_set('rolling thunder', 'HOP').
card_original_type('rolling thunder'/'HOP', 'Sorcery').
card_original_text('rolling thunder'/'HOP', 'Rolling Thunder deals X damage divided as you choose among any number of target creatures and/or players.').
card_image_name('rolling thunder'/'HOP', 'rolling thunder').
card_uid('rolling thunder'/'HOP', 'HOP:Rolling Thunder:rolling thunder').
card_rarity('rolling thunder'/'HOP', 'Common').
card_artist('rolling thunder'/'HOP', 'Richard Thomas').
card_number('rolling thunder'/'HOP', '64').
card_flavor_text('rolling thunder'/'HOP', '\"Such rage,\" thought Vhati, gazing up at the thunderhead from the Predator. \"It is Greven\'s mind manifest.\"').
card_multiverse_id('rolling thunder'/'HOP', '205391').

card_in_set('rorix bladewing', 'HOP').
card_original_type('rorix bladewing'/'HOP', 'Legendary Creature — Dragon').
card_original_text('rorix bladewing'/'HOP', 'Flying, haste').
card_image_name('rorix bladewing'/'HOP', 'rorix bladewing').
card_uid('rorix bladewing'/'HOP', 'HOP:Rorix Bladewing:rorix bladewing').
card_rarity('rorix bladewing'/'HOP', 'Rare').
card_artist('rorix bladewing'/'HOP', 'Darrell Riche').
card_number('rorix bladewing'/'HOP', '65').
card_flavor_text('rorix bladewing'/'HOP', 'In the smoldering ashes of Shiv, a few dragons strive to rebuild their native land. The rest seek any opportunity to restore the broken pride of their race.').
card_multiverse_id('rorix bladewing'/'HOP', '205371').

card_in_set('rotting rats', 'HOP').
card_original_type('rotting rats'/'HOP', 'Creature — Zombie Rat').
card_original_text('rotting rats'/'HOP', 'When Rotting Rats enters the battlefield, each player discards a card.\nUnearth {1}{B} ({1}{B}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave play. Unearth only as a sorcery.)').
card_image_name('rotting rats'/'HOP', 'rotting rats').
card_uid('rotting rats'/'HOP', 'HOP:Rotting Rats:rotting rats').
card_rarity('rotting rats'/'HOP', 'Common').
card_artist('rotting rats'/'HOP', 'Dave Allsop').
card_number('rotting rats'/'HOP', '39').
card_multiverse_id('rotting rats'/'HOP', '205344').

card_in_set('rumbling slum', 'HOP').
card_original_type('rumbling slum'/'HOP', 'Creature — Elemental').
card_original_text('rumbling slum'/'HOP', 'At the beginning of your upkeep, Rumbling Slum deals 1 damage to each player.').
card_image_name('rumbling slum'/'HOP', 'rumbling slum').
card_uid('rumbling slum'/'HOP', 'HOP:Rumbling Slum:rumbling slum').
card_rarity('rumbling slum'/'HOP', 'Rare').
card_artist('rumbling slum'/'HOP', 'Carl Critchlow').
card_number('rumbling slum'/'HOP', '93').
card_flavor_text('rumbling slum'/'HOP', 'The Orzhov contract the Izzet to animate slum districts and banish them to the wastes. The Gruul adopt them and send them back to the city for vengeance.').
card_multiverse_id('rumbling slum'/'HOP', '205354').

card_in_set('sanctum of serra', 'HOP').
card_original_type('sanctum of serra'/'HOP', 'Plane — Serra\'s Realm').
card_original_text('sanctum of serra'/'HOP', 'When you planeswalk away from Sanctum of Serra, destroy all nonland permanents.\nWhenever you roll {C}, you may have your life total become 20.').
card_first_print('sanctum of serra', 'HOP').
card_image_name('sanctum of serra'/'HOP', 'sanctum of serra').
card_uid('sanctum of serra'/'HOP', 'HOP:Sanctum of Serra:sanctum of serra').
card_rarity('sanctum of serra'/'HOP', 'Common').
card_artist('sanctum of serra'/'HOP', 'Rob Alexander').
card_number('sanctum of serra'/'HOP', '32').
card_multiverse_id('sanctum of serra'/'HOP', '198079').

card_in_set('sarcomite myr', 'HOP').
card_original_type('sarcomite myr'/'HOP', 'Artifact Creature — Myr').
card_original_text('sarcomite myr'/'HOP', '{2}: Sarcomite Myr gains flying until end of turn.\n{2}, Sacrifice Sarcomite Myr: Draw a card.').
card_image_name('sarcomite myr'/'HOP', 'sarcomite myr').
card_uid('sarcomite myr'/'HOP', 'HOP:Sarcomite Myr:sarcomite myr').
card_rarity('sarcomite myr'/'HOP', 'Common').
card_artist('sarcomite myr'/'HOP', 'Michael Bruinsma').
card_number('sarcomite myr'/'HOP', '13').
card_flavor_text('sarcomite myr'/'HOP', '\"A horrible sight, yes, but the sounds . . . Its twanging tendons and grinding gears are almost musical.\"\n—Brudiclad, Telchor engineer').
card_multiverse_id('sarcomite myr'/'HOP', '205339').

card_in_set('savage twister', 'HOP').
card_original_type('savage twister'/'HOP', 'Sorcery').
card_original_text('savage twister'/'HOP', 'Savage Twister deals X damage to each creature.').
card_image_name('savage twister'/'HOP', 'savage twister').
card_uid('savage twister'/'HOP', 'HOP:Savage Twister:savage twister').
card_rarity('savage twister'/'HOP', 'Uncommon').
card_artist('savage twister'/'HOP', 'Bob Eggleton').
card_number('savage twister'/'HOP', '94').
card_flavor_text('savage twister'/'HOP', '\"Frozen, we watched the funnel pluck up three of the goats—pook! pook! pook!—before we ran for the wadi.\"\n—Travelogue of Najat').
card_multiverse_id('savage twister'/'HOP', '205355').

card_in_set('sea of sand', 'HOP').
card_original_type('sea of sand'/'HOP', 'Plane — Rabiah').
card_original_text('sea of sand'/'HOP', 'Players reveal each card they draw.\nWhenever a player draws a land card, that player gains 3 life.\nWhenever a player draws a nonland card, that player loses 3 life.\nWhenever you roll {C}, put target permanent on top of its owner\'s library.').
card_first_print('sea of sand', 'HOP').
card_image_name('sea of sand'/'HOP', 'sea of sand').
card_uid('sea of sand'/'HOP', 'HOP:Sea of Sand:sea of sand').
card_rarity('sea of sand'/'HOP', 'Common').
card_artist('sea of sand'/'HOP', 'Jim Nelson').
card_number('sea of sand'/'HOP', '33').
card_multiverse_id('sea of sand'/'HOP', '198111').

card_in_set('search for tomorrow', 'HOP').
card_original_type('search for tomorrow'/'HOP', 'Sorcery').
card_original_text('search for tomorrow'/'HOP', 'Search your library for a basic land card and put it onto the battlefield. Then shuffle your library.\nSuspend 2—{G} (Rather than cast this card from your hand, you may pay {G} and exile it with two time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost.)').
card_image_name('search for tomorrow'/'HOP', 'search for tomorrow').
card_uid('search for tomorrow'/'HOP', 'HOP:Search for Tomorrow:search for tomorrow').
card_rarity('search for tomorrow'/'HOP', 'Common').
card_artist('search for tomorrow'/'HOP', 'Randy Gallegos').
card_number('search for tomorrow'/'HOP', '77').
card_multiverse_id('search for tomorrow'/'HOP', '205408').

card_in_set('seat of the synod', 'HOP').
card_original_type('seat of the synod'/'HOP', 'Artifact Land').
card_original_text('seat of the synod'/'HOP', '(Seat of the Synod isn\'t a spell.)\n{T}: Add {U} to your mana pool.').
card_image_name('seat of the synod'/'HOP', 'seat of the synod').
card_uid('seat of the synod'/'HOP', 'HOP:Seat of the Synod:seat of the synod').
card_rarity('seat of the synod'/'HOP', 'Common').
card_artist('seat of the synod'/'HOP', 'John Avon').
card_number('seat of the synod'/'HOP', '136').
card_flavor_text('seat of the synod'/'HOP', 'Lumengrid, site of the Knowledge Pool, source of vedalken arcana.').
card_multiverse_id('seat of the synod'/'HOP', '205303').

card_in_set('serum tank', 'HOP').
card_original_type('serum tank'/'HOP', 'Artifact').
card_original_text('serum tank'/'HOP', 'Whenever Serum Tank or another artifact enters the battlefield, put a charge counter on Serum Tank.\n{3}, {T}, Remove a charge counter from Serum Tank: Draw a card.').
card_image_name('serum tank'/'HOP', 'serum tank').
card_uid('serum tank'/'HOP', 'HOP:Serum Tank:serum tank').
card_rarity('serum tank'/'HOP', 'Uncommon').
card_artist('serum tank'/'HOP', 'Corey D. Macourek').
card_number('serum tank'/'HOP', '125').
card_multiverse_id('serum tank'/'HOP', '205304').

card_in_set('shepherd of rot', 'HOP').
card_original_type('shepherd of rot'/'HOP', 'Creature — Zombie Cleric').
card_original_text('shepherd of rot'/'HOP', '{T}: Each player loses 1 life for each Zombie on the battlefield.').
card_image_name('shepherd of rot'/'HOP', 'shepherd of rot').
card_uid('shepherd of rot'/'HOP', 'HOP:Shepherd of Rot:shepherd of rot').
card_rarity('shepherd of rot'/'HOP', 'Common').
card_artist('shepherd of rot'/'HOP', 'Greg Staples').
card_number('shepherd of rot'/'HOP', '40').
card_flavor_text('shepherd of rot'/'HOP', '\"My priests swore loyalty to the Cabal not until death, but beyond it.\"\n—Cabal Patriarch').
card_multiverse_id('shepherd of rot'/'HOP', '205372').

card_in_set('shiv', 'HOP').
card_original_type('shiv'/'HOP', 'Plane — Dominaria').
card_original_text('shiv'/'HOP', 'All creatures have \"{R}: This creature gets +1/+0 until end of turn.\"\nWhenever you roll {C}, put a 5/5 red Dragon creature token with flying onto the battlefield.').
card_first_print('shiv', 'HOP').
card_image_name('shiv'/'HOP', 'shiv').
card_uid('shiv'/'HOP', 'HOP:Shiv:shiv').
card_rarity('shiv'/'HOP', 'Common').
card_artist('shiv'/'HOP', 'rk post').
card_number('shiv'/'HOP', '34').
card_multiverse_id('shiv'/'HOP', '198086').

card_in_set('shivan oasis', 'HOP').
card_original_type('shivan oasis'/'HOP', 'Land').
card_original_text('shivan oasis'/'HOP', 'Shivan Oasis enters the battlefield tapped.\n{T}: Add {R} or {G} to your mana pool.').
card_image_name('shivan oasis'/'HOP', 'shivan oasis').
card_uid('shivan oasis'/'HOP', 'HOP:Shivan Oasis:shivan oasis').
card_rarity('shivan oasis'/'HOP', 'Uncommon').
card_artist('shivan oasis'/'HOP', 'Rob Alexander').
card_number('shivan oasis'/'HOP', '137').
card_flavor_text('shivan oasis'/'HOP', 'Only the hardiest explorers survive to eat the fruit.').
card_multiverse_id('shivan oasis'/'HOP', '205406').

card_in_set('silver myr', 'HOP').
card_original_type('silver myr'/'HOP', 'Artifact Creature — Myr').
card_original_text('silver myr'/'HOP', '{T}: Add {U} to your mana pool.').
card_image_name('silver myr'/'HOP', 'silver myr').
card_uid('silver myr'/'HOP', 'HOP:Silver Myr:silver myr').
card_rarity('silver myr'/'HOP', 'Common').
card_artist('silver myr'/'HOP', 'Kev Walker').
card_number('silver myr'/'HOP', '126').
card_flavor_text('silver myr'/'HOP', 'The vedalken saw the myr as toys, unaware of the intelligence lurking behind their empty eyes.').
card_multiverse_id('silver myr'/'HOP', '205305').

card_in_set('silverglade elemental', 'HOP').
card_original_type('silverglade elemental'/'HOP', 'Creature — Elemental').
card_original_text('silverglade elemental'/'HOP', 'When Silverglade Elemental enters the battlefield, you may search your library for a Forest card and put that card onto the battlefield. If you do, shuffle your library.').
card_image_name('silverglade elemental'/'HOP', 'silverglade elemental').
card_uid('silverglade elemental'/'HOP', 'HOP:Silverglade Elemental:silverglade elemental').
card_rarity('silverglade elemental'/'HOP', 'Common').
card_artist('silverglade elemental'/'HOP', 'Chippy').
card_number('silverglade elemental'/'HOP', '78').
card_multiverse_id('silverglade elemental'/'HOP', '205415').

card_in_set('skeleton shard', 'HOP').
card_original_type('skeleton shard'/'HOP', 'Artifact').
card_original_text('skeleton shard'/'HOP', '{3}, {T} or {B}, {T}: Return target artifact creature card from your graveyard to your hand.').
card_image_name('skeleton shard'/'HOP', 'skeleton shard').
card_uid('skeleton shard'/'HOP', 'HOP:Skeleton Shard:skeleton shard').
card_rarity('skeleton shard'/'HOP', 'Uncommon').
card_artist('skeleton shard'/'HOP', 'Doug Chaffee').
card_number('skeleton shard'/'HOP', '127').
card_flavor_text('skeleton shard'/'HOP', 'Metal permeates the marrow of every bone in Mephidross—except one.').
card_multiverse_id('skeleton shard'/'HOP', '205306').

card_in_set('skybreen', 'HOP').
card_original_type('skybreen'/'HOP', 'Plane — Kaldheim').
card_original_text('skybreen'/'HOP', 'Players play with the top card of their libraries revealed.\nSpells that share a card type with the top card of a library can\'t be cast.\nWhenever you roll {C}, target player loses life equal to the number of cards in his or her hand.').
card_first_print('skybreen', 'HOP').
card_image_name('skybreen'/'HOP', 'skybreen').
card_uid('skybreen'/'HOP', 'HOP:Skybreen:skybreen').
card_rarity('skybreen'/'HOP', 'Common').
card_artist('skybreen'/'HOP', 'Wayne England').
card_number('skybreen'/'HOP', '35').
card_multiverse_id('skybreen'/'HOP', '198101').

card_in_set('sludge strider', 'HOP').
card_original_type('sludge strider'/'HOP', 'Artifact Creature — Insect').
card_original_text('sludge strider'/'HOP', 'Whenever another artifact enters the battlefield under your control or another artifact you control leaves the battlefield, you may pay {1}. If you do, target player loses 1 life and you gain 1 life.').
card_image_name('sludge strider'/'HOP', 'sludge strider').
card_uid('sludge strider'/'HOP', 'HOP:Sludge Strider:sludge strider').
card_rarity('sludge strider'/'HOP', 'Uncommon').
card_artist('sludge strider'/'HOP', 'Franz Vohwinkel').
card_number('sludge strider'/'HOP', '95').
card_flavor_text('sludge strider'/'HOP', 'Underneath the cities of Esper are cycles of life unseen by those who feed them.').
card_multiverse_id('sludge strider'/'HOP', '205345').

card_in_set('smokebraider', 'HOP').
card_original_type('smokebraider'/'HOP', 'Creature — Elemental Shaman').
card_original_text('smokebraider'/'HOP', '{T}: Add two mana in any combination of colors to your mana pool. Spend this mana only to cast Elemental spells or activate abilities of Elementals.').
card_image_name('smokebraider'/'HOP', 'smokebraider').
card_uid('smokebraider'/'HOP', 'HOP:Smokebraider:smokebraider').
card_rarity('smokebraider'/'HOP', 'Common').
card_artist('smokebraider'/'HOP', 'Anthony S. Waters').
card_number('smokebraider'/'HOP', '66').
card_flavor_text('smokebraider'/'HOP', '\"Be silent and listen to your inner fire. Only then can you walk the Path of Flame.\"').
card_multiverse_id('smokebraider'/'HOP', '205398').

card_in_set('sokenzan', 'HOP').
card_original_type('sokenzan'/'HOP', 'Plane — Kamigawa').
card_original_text('sokenzan'/'HOP', 'All creatures get +1/+1 and have haste.\nWhenever you roll {C}, untap all creatures that attacked this turn. After this main phase, there is an additional combat phase followed by an additional main phase.').
card_first_print('sokenzan', 'HOP').
card_image_name('sokenzan'/'HOP', 'sokenzan').
card_uid('sokenzan'/'HOP', 'HOP:Sokenzan:sokenzan').
card_rarity('sokenzan'/'HOP', 'Common').
card_artist('sokenzan'/'HOP', 'Brian Snõddy').
card_number('sokenzan'/'HOP', '36').
card_multiverse_id('sokenzan'/'HOP', '198075').

card_in_set('soul warden', 'HOP').
card_original_type('soul warden'/'HOP', 'Creature — Human Cleric').
card_original_text('soul warden'/'HOP', 'Whenever another creature enters the battlefield, you gain 1 life.').
card_image_name('soul warden'/'HOP', 'soul warden').
card_uid('soul warden'/'HOP', 'HOP:Soul Warden:soul warden').
card_rarity('soul warden'/'HOP', 'Common').
card_artist('soul warden'/'HOP', 'Randy Gallegos').
card_number('soul warden'/'HOP', '7').
card_flavor_text('soul warden'/'HOP', '\"One does not question the size or shape of the grains of sand in an hourglass. Nor do I question the temperament of the souls under my guidance.\"').
card_multiverse_id('soul warden'/'HOP', '205351').

card_in_set('soulless one', 'HOP').
card_original_type('soulless one'/'HOP', 'Creature — Zombie Avatar').
card_original_text('soulless one'/'HOP', 'Soulless One\'s power and toughness are each equal to the number of Zombies on the battlefield plus the number of Zombie cards in all graveyards.').
card_image_name('soulless one'/'HOP', 'soulless one').
card_uid('soulless one'/'HOP', 'HOP:Soulless One:soulless one').
card_rarity('soulless one'/'HOP', 'Uncommon').
card_artist('soulless one'/'HOP', 'Thomas M. Baxa').
card_number('soulless one'/'HOP', '41').
card_flavor_text('soulless one'/'HOP', '\"Surrender your soul to me.\"').
card_multiverse_id('soulless one'/'HOP', '205373').

card_in_set('stronghold furnace', 'HOP').
card_original_type('stronghold furnace'/'HOP', 'Plane — Rath').
card_original_text('stronghold furnace'/'HOP', 'If a source would deal damage to a creature or player, it deals double that damage to that creature or player instead.\nWhenever you roll {C}, Stronghold Furnace deals 1 damage to target creature or player.').
card_first_print('stronghold furnace', 'HOP').
card_image_name('stronghold furnace'/'HOP', 'stronghold furnace').
card_uid('stronghold furnace'/'HOP', 'HOP:Stronghold Furnace:stronghold furnace').
card_rarity('stronghold furnace'/'HOP', 'Common').
card_artist('stronghold furnace'/'HOP', 'Jim Pavelec').
card_number('stronghold furnace'/'HOP', '37').
card_multiverse_id('stronghold furnace'/'HOP', '198063').

card_in_set('sunhome, fortress of the legion', 'HOP').
card_original_type('sunhome, fortress of the legion'/'HOP', 'Land').
card_original_text('sunhome, fortress of the legion'/'HOP', '{T}: Add {1} to your mana pool.\n{2}{R}{W}, {T}: Target creature gains double strike until end of turn. (It deals both first-strike and regular combat damage.)').
card_image_name('sunhome, fortress of the legion'/'HOP', 'sunhome, fortress of the legion').
card_uid('sunhome, fortress of the legion'/'HOP', 'HOP:Sunhome, Fortress of the Legion:sunhome, fortress of the legion').
card_rarity('sunhome, fortress of the legion'/'HOP', 'Uncommon').
card_artist('sunhome, fortress of the legion'/'HOP', 'Martina Pilcerova').
card_number('sunhome, fortress of the legion'/'HOP', '138').
card_flavor_text('sunhome, fortress of the legion'/'HOP', 'Sunhome—the stalwart shield, the towering sentinel, the seat of justice.').
card_multiverse_id('sunhome, fortress of the legion'/'HOP', '205363').

card_in_set('suntouched myr', 'HOP').
card_original_type('suntouched myr'/'HOP', 'Artifact Creature — Myr').
card_original_text('suntouched myr'/'HOP', 'Sunburst (This enters the battlefield with a +1/+1 counter on it for each color of mana spent to cast it.)').
card_image_name('suntouched myr'/'HOP', 'suntouched myr').
card_uid('suntouched myr'/'HOP', 'HOP:Suntouched Myr:suntouched myr').
card_rarity('suntouched myr'/'HOP', 'Common').
card_artist('suntouched myr'/'HOP', 'Greg Hildebrandt').
card_number('suntouched myr'/'HOP', '128').
card_flavor_text('suntouched myr'/'HOP', 'As the fifth sun joins the other four, new abilities awaken in the creatures of Mirrodin.').
card_multiverse_id('suntouched myr'/'HOP', '205334').

card_in_set('swamp', 'HOP').
card_original_type('swamp'/'HOP', 'Basic Land — Swamp').
card_original_text('swamp'/'HOP', 'B').
card_image_name('swamp'/'HOP', 'swamp1').
card_uid('swamp'/'HOP', 'HOP:Swamp:swamp1').
card_rarity('swamp'/'HOP', 'Basic Land').
card_artist('swamp'/'HOP', 'Martina Pilcerova').
card_number('swamp'/'HOP', '151').
card_multiverse_id('swamp'/'HOP', '205309').

card_in_set('swamp', 'HOP').
card_original_type('swamp'/'HOP', 'Basic Land — Swamp').
card_original_text('swamp'/'HOP', 'B').
card_image_name('swamp'/'HOP', 'swamp2').
card_uid('swamp'/'HOP', 'HOP:Swamp:swamp2').
card_rarity('swamp'/'HOP', 'Basic Land').
card_artist('swamp'/'HOP', 'Vance Kovacs').
card_number('swamp'/'HOP', '152').
card_multiverse_id('swamp'/'HOP', '205443').

card_in_set('swamp', 'HOP').
card_original_type('swamp'/'HOP', 'Basic Land — Swamp').
card_original_text('swamp'/'HOP', 'B').
card_image_name('swamp'/'HOP', 'swamp3').
card_uid('swamp'/'HOP', 'HOP:Swamp:swamp3').
card_rarity('swamp'/'HOP', 'Basic Land').
card_artist('swamp'/'HOP', 'Craig Mullins').
card_number('swamp'/'HOP', '153').
card_multiverse_id('swamp'/'HOP', '205434').

card_in_set('swamp', 'HOP').
card_original_type('swamp'/'HOP', 'Basic Land — Swamp').
card_original_text('swamp'/'HOP', 'B').
card_image_name('swamp'/'HOP', 'swamp4').
card_uid('swamp'/'HOP', 'HOP:Swamp:swamp4').
card_rarity('swamp'/'HOP', 'Basic Land').
card_artist('swamp'/'HOP', 'Mark Tedin').
card_number('swamp'/'HOP', '154').
card_multiverse_id('swamp'/'HOP', '205442').

card_in_set('swamp', 'HOP').
card_original_type('swamp'/'HOP', 'Basic Land — Swamp').
card_original_text('swamp'/'HOP', 'B').
card_image_name('swamp'/'HOP', 'swamp5').
card_uid('swamp'/'HOP', 'HOP:Swamp:swamp5').
card_rarity('swamp'/'HOP', 'Basic Land').
card_artist('swamp'/'HOP', 'Mark Tedin').
card_number('swamp'/'HOP', '155').
card_multiverse_id('swamp'/'HOP', '205446').

card_in_set('syphon mind', 'HOP').
card_original_type('syphon mind'/'HOP', 'Sorcery').
card_original_text('syphon mind'/'HOP', 'Each other player discards a card. You draw a card for each card discarded this way.').
card_image_name('syphon mind'/'HOP', 'syphon mind').
card_uid('syphon mind'/'HOP', 'HOP:Syphon Mind:syphon mind').
card_rarity('syphon mind'/'HOP', 'Common').
card_artist('syphon mind'/'HOP', 'Jeff Easley').
card_number('syphon mind'/'HOP', '42').
card_flavor_text('syphon mind'/'HOP', 'When tempers run high, it\'s easy to lose your head.').
card_multiverse_id('syphon mind'/'HOP', '205374').

card_in_set('syphon soul', 'HOP').
card_original_type('syphon soul'/'HOP', 'Sorcery').
card_original_text('syphon soul'/'HOP', 'Syphon Soul deals 2 damage to each other player. You gain life equal to the damage dealt this way.').
card_image_name('syphon soul'/'HOP', 'syphon soul').
card_uid('syphon soul'/'HOP', 'HOP:Syphon Soul:syphon soul').
card_rarity('syphon soul'/'HOP', 'Common').
card_artist('syphon soul'/'HOP', 'Ron Spears').
card_number('syphon soul'/'HOP', '43').
card_flavor_text('syphon soul'/'HOP', 'As Phage drank their energy, a vague memory of Jeska stirred. Then she lost herself again in the joy of her victims\' suffering.').
card_multiverse_id('syphon soul'/'HOP', '205375').

card_in_set('taurean mauler', 'HOP').
card_original_type('taurean mauler'/'HOP', 'Creature — Shapeshifter').
card_original_text('taurean mauler'/'HOP', 'Changeling (This card is every creature type at all times.)\nWhenever an opponent casts a spell, you may put a +1/+1 counter on Taurean Mauler.').
card_image_name('taurean mauler'/'HOP', 'taurean mauler').
card_uid('taurean mauler'/'HOP', 'HOP:Taurean Mauler:taurean mauler').
card_rarity('taurean mauler'/'HOP', 'Rare').
card_artist('taurean mauler'/'HOP', 'Dominick Domingo').
card_number('taurean mauler'/'HOP', '67').
card_flavor_text('taurean mauler'/'HOP', 'The power of a waterfall. The fury of an avalanche. The intellect of a gale-force wind.').
card_multiverse_id('taurean mauler'/'HOP', '205404').

card_in_set('tazeem', 'HOP').
card_original_type('tazeem'/'HOP', 'Plane — Zendikar').
card_original_text('tazeem'/'HOP', 'Creatures can\'t block.\nWhenever you roll {C}, draw a card for each land you control.').
card_first_print('tazeem', 'HOP').
card_image_name('tazeem'/'HOP', 'tazeem').
card_uid('tazeem'/'HOP', 'HOP:Tazeem:tazeem').
card_rarity('tazeem'/'HOP', 'Special').
card_artist('tazeem'/'HOP', 'Vincent Proce').
card_number('tazeem'/'HOP', '41').
card_multiverse_id('tazeem'/'HOP', '198069').

card_in_set('terramorphic expanse', 'HOP').
card_original_type('terramorphic expanse'/'HOP', 'Land').
card_original_text('terramorphic expanse'/'HOP', '{T}, Sacrifice Terramorphic Expanse: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('terramorphic expanse'/'HOP', 'terramorphic expanse').
card_uid('terramorphic expanse'/'HOP', 'HOP:Terramorphic Expanse:terramorphic expanse').
card_rarity('terramorphic expanse'/'HOP', 'Common').
card_artist('terramorphic expanse'/'HOP', 'Dan Scott').
card_number('terramorphic expanse'/'HOP', '139').
card_multiverse_id('terramorphic expanse'/'HOP', '205352').

card_in_set('the æther flues', 'HOP').
card_original_type('the æther flues'/'HOP', 'Plane — Iquatana').
card_original_text('the æther flues'/'HOP', 'When you planeswalk to The Æther Flues or at the beginning of your upkeep, you may sacrifice a creature. If you do, reveal cards from the top of your library until you reveal a creature card, put that card onto the battlefield, then shuffle all other cards revealed this way into your library.\nWhenever you roll {C}, you may put a creature card from your hand onto the battlefield.').
card_first_print('the æther flues', 'HOP').
card_image_name('the æther flues'/'HOP', 'the aether flues').
card_uid('the æther flues'/'HOP', 'HOP:The Æther Flues:the aether flues').
card_rarity('the æther flues'/'HOP', 'Common').
card_artist('the æther flues'/'HOP', 'Jason A. Engle').
card_number('the æther flues'/'HOP', '2').
card_multiverse_id('the æther flues'/'HOP', '198098').

card_in_set('the dark barony', 'HOP').
card_original_type('the dark barony'/'HOP', 'Plane — Ulgrotha').
card_original_text('the dark barony'/'HOP', 'Whenever a nonblack card is put into a player\'s graveyard from anywhere, that player loses 1 life.\nWhenever you roll {C}, each opponent discards a card.').
card_first_print('the dark barony', 'HOP').
card_image_name('the dark barony'/'HOP', 'the dark barony').
card_uid('the dark barony'/'HOP', 'HOP:The Dark Barony:the dark barony').
card_rarity('the dark barony'/'HOP', 'Common').
card_artist('the dark barony'/'HOP', 'Pete Venters').
card_number('the dark barony'/'HOP', '6').
card_multiverse_id('the dark barony'/'HOP', '198083').

card_in_set('the eon fog', 'HOP').
card_original_type('the eon fog'/'HOP', 'Plane — Equilor').
card_original_text('the eon fog'/'HOP', 'Players skip their untap steps.\nWhenever you roll {C}, untap all permanents you control.').
card_first_print('the eon fog', 'HOP').
card_image_name('the eon fog'/'HOP', 'the eon fog').
card_uid('the eon fog'/'HOP', 'HOP:The Eon Fog:the eon fog').
card_rarity('the eon fog'/'HOP', 'Common').
card_artist('the eon fog'/'HOP', 'Jaime Jones').
card_number('the eon fog'/'HOP', '8').
card_multiverse_id('the eon fog'/'HOP', '198092').

card_in_set('the fourth sphere', 'HOP').
card_original_type('the fourth sphere'/'HOP', 'Plane — Phyrexia').
card_original_text('the fourth sphere'/'HOP', 'At the beginning of your upkeep, sacrifice a nonblack creature.\nWhenever you roll {C}, put a 2/2 black Zombie creature token onto the battlefield.').
card_first_print('the fourth sphere', 'HOP').
card_image_name('the fourth sphere'/'HOP', 'the fourth sphere').
card_uid('the fourth sphere'/'HOP', 'HOP:The Fourth Sphere:the fourth sphere').
card_rarity('the fourth sphere'/'HOP', 'Common').
card_artist('the fourth sphere'/'HOP', 'Dave Kendall').
card_number('the fourth sphere'/'HOP', '11').
card_multiverse_id('the fourth sphere'/'HOP', '198102').

card_in_set('the great forest', 'HOP').
card_original_type('the great forest'/'HOP', 'Plane — Lorwyn').
card_original_text('the great forest'/'HOP', 'Each creature assigns combat damage equal to its toughness rather than its power.\nWhenever you roll {C}, creatures you control get +0/+2 and gain trample until end of turn.').
card_first_print('the great forest', 'HOP').
card_image_name('the great forest'/'HOP', 'the great forest').
card_uid('the great forest'/'HOP', 'HOP:The Great Forest:the great forest').
card_rarity('the great forest'/'HOP', 'Common').
card_artist('the great forest'/'HOP', 'Howard Lyon').
card_number('the great forest'/'HOP', '14').
card_multiverse_id('the great forest'/'HOP', '198088').

card_in_set('the hippodrome', 'HOP').
card_original_type('the hippodrome'/'HOP', 'Plane — Segovia').
card_original_text('the hippodrome'/'HOP', 'All creatures get -5/-0.\nWhenever you roll {C}, you may destroy target creature if its power is 0 or less.').
card_first_print('the hippodrome', 'HOP').
card_image_name('the hippodrome'/'HOP', 'the hippodrome').
card_uid('the hippodrome'/'HOP', 'HOP:The Hippodrome:the hippodrome').
card_rarity('the hippodrome'/'HOP', 'Common').
card_artist('the hippodrome'/'HOP', 'Steve Argyle').
card_number('the hippodrome'/'HOP', '16').
card_multiverse_id('the hippodrome'/'HOP', '198067').

card_in_set('the maelstrom', 'HOP').
card_original_type('the maelstrom'/'HOP', 'Plane — Alara').
card_original_text('the maelstrom'/'HOP', 'When you planeswalk to The Maelstrom or at the beginning of your upkeep, you may reveal the top card of your library. If it\'s a permanent card, you may put it onto the battlefield. Otherwise, put that card on the bottom of your library.\nWhenever you roll {C}, return target permanent card from your graveyard to the battlefield.').
card_first_print('the maelstrom', 'HOP').
card_image_name('the maelstrom'/'HOP', 'the maelstrom').
card_uid('the maelstrom'/'HOP', 'HOP:The Maelstrom:the maelstrom').
card_rarity('the maelstrom'/'HOP', 'Common').
card_artist('the maelstrom'/'HOP', 'James Paick').
card_number('the maelstrom'/'HOP', '23').
card_multiverse_id('the maelstrom'/'HOP', '198062').

card_in_set('thirst for knowledge', 'HOP').
card_original_type('thirst for knowledge'/'HOP', 'Instant').
card_original_text('thirst for knowledge'/'HOP', 'Draw three cards. Then discard two cards unless you discard an artifact card.').
card_image_name('thirst for knowledge'/'HOP', 'thirst for knowledge').
card_uid('thirst for knowledge'/'HOP', 'HOP:Thirst for Knowledge:thirst for knowledge').
card_rarity('thirst for knowledge'/'HOP', 'Uncommon').
card_artist('thirst for knowledge'/'HOP', 'Ben Thompson').
card_number('thirst for knowledge'/'HOP', '14').
card_flavor_text('thirst for knowledge'/'HOP', 'Lymph, the fluid essence of blinkmoths, is prized by wizards for the rush of intellect it provides.').
card_multiverse_id('thirst for knowledge'/'HOP', '205311').

card_in_set('tornado elemental', 'HOP').
card_original_type('tornado elemental'/'HOP', 'Creature — Elemental').
card_original_text('tornado elemental'/'HOP', 'When Tornado Elemental enters the battlefield, it deals 6 damage to each creature with flying.\nYou may have Tornado Elemental assign its combat damage as though it weren\'t blocked.').
card_image_name('tornado elemental'/'HOP', 'tornado elemental').
card_uid('tornado elemental'/'HOP', 'HOP:Tornado Elemental:tornado elemental').
card_rarity('tornado elemental'/'HOP', 'Rare').
card_artist('tornado elemental'/'HOP', 'Alex Horley-Orlandelli').
card_number('tornado elemental'/'HOP', '79').
card_multiverse_id('tornado elemental'/'HOP', '205333').

card_in_set('tree of tales', 'HOP').
card_original_type('tree of tales'/'HOP', 'Artifact Land').
card_original_text('tree of tales'/'HOP', '(Tree of Tales isn\'t a spell.)\n{T}: Add {G} to your mana pool.').
card_image_name('tree of tales'/'HOP', 'tree of tales').
card_uid('tree of tales'/'HOP', 'HOP:Tree of Tales:tree of tales').
card_rarity('tree of tales'/'HOP', 'Common').
card_artist('tree of tales'/'HOP', 'John Avon').
card_number('tree of tales'/'HOP', '140').
card_flavor_text('tree of tales'/'HOP', 'Tel-Jilad, sanctum of the ancient trolls, keepers of the secret of Mirrodin\'s origin.').
card_multiverse_id('tree of tales'/'HOP', '205312').

card_in_set('tribal unity', 'HOP').
card_original_type('tribal unity'/'HOP', 'Instant').
card_original_text('tribal unity'/'HOP', 'Creatures of the creature type of your choice get +X/+X until end of turn.').
card_image_name('tribal unity'/'HOP', 'tribal unity').
card_uid('tribal unity'/'HOP', 'HOP:Tribal Unity:tribal unity').
card_rarity('tribal unity'/'HOP', 'Uncommon').
card_artist('tribal unity'/'HOP', 'Ron Spears').
card_number('tribal unity'/'HOP', '80').
card_flavor_text('tribal unity'/'HOP', 'Kamahl left the violence of his former life behind him, but he still believes in the power of muscle.').
card_multiverse_id('tribal unity'/'HOP', '205376').

card_in_set('turri island', 'HOP').
card_original_type('turri island'/'HOP', 'Plane — Ir').
card_original_text('turri island'/'HOP', 'Creature spells cost {2} less to cast.\nWhenever you roll {C}, reveal the top three cards of your library. Put all creature cards revealed this way into your hand and the rest into your graveyard.').
card_first_print('turri island', 'HOP').
card_image_name('turri island'/'HOP', 'turri island').
card_uid('turri island'/'HOP', 'HOP:Turri Island:turri island').
card_rarity('turri island'/'HOP', 'Common').
card_artist('turri island'/'HOP', 'Raymond Swanland').
card_number('turri island'/'HOP', '38').
card_multiverse_id('turri island'/'HOP', '198085').

card_in_set('undead warchief', 'HOP').
card_original_type('undead warchief'/'HOP', 'Creature — Zombie').
card_original_text('undead warchief'/'HOP', 'Zombie spells you cast cost {1} less to cast.\nZombie creatures you control get +2/+1.').
card_image_name('undead warchief'/'HOP', 'undead warchief').
card_uid('undead warchief'/'HOP', 'HOP:Undead Warchief:undead warchief').
card_rarity('undead warchief'/'HOP', 'Uncommon').
card_artist('undead warchief'/'HOP', 'Greg Hildebrandt').
card_number('undead warchief'/'HOP', '44').
card_flavor_text('undead warchief'/'HOP', 'It has the strength of seven men. In fact, it used to be seven men.').
card_multiverse_id('undead warchief'/'HOP', '205410').

card_in_set('undercity reaches', 'HOP').
card_original_type('undercity reaches'/'HOP', 'Plane — Ravnica').
card_original_text('undercity reaches'/'HOP', 'Whenever a creature deals combat damage to a player, its controller may draw a card.\nWhenever you roll {C}, you have no maximum hand size for the rest of the game.').
card_first_print('undercity reaches', 'HOP').
card_image_name('undercity reaches'/'HOP', 'undercity reaches').
card_uid('undercity reaches'/'HOP', 'HOP:Undercity Reaches:undercity reaches').
card_rarity('undercity reaches'/'HOP', 'Common').
card_artist('undercity reaches'/'HOP', 'Stephan Martiniere').
card_number('undercity reaches'/'HOP', '39').
card_multiverse_id('undercity reaches'/'HOP', '198110').

card_in_set('vault of whispers', 'HOP').
card_original_type('vault of whispers'/'HOP', 'Artifact Land').
card_original_text('vault of whispers'/'HOP', '(Vault of Whispers isn\'t a spell.)\n{T}: Add {B} to your mana pool.').
card_image_name('vault of whispers'/'HOP', 'vault of whispers').
card_uid('vault of whispers'/'HOP', 'HOP:Vault of Whispers:vault of whispers').
card_rarity('vault of whispers'/'HOP', 'Common').
card_artist('vault of whispers'/'HOP', 'Rob Alexander').
card_number('vault of whispers'/'HOP', '141').
card_flavor_text('vault of whispers'/'HOP', 'Ish Sah, den of the warlord Geth, commander of countless nim.').
card_multiverse_id('vault of whispers'/'HOP', '205313').

card_in_set('vedalken engineer', 'HOP').
card_original_type('vedalken engineer'/'HOP', 'Creature — Vedalken Artificer').
card_original_text('vedalken engineer'/'HOP', '{T}: Add two mana of any one color to your mana pool. Spend this mana only to cast artifact spells or activate abilities of artifacts.').
card_image_name('vedalken engineer'/'HOP', 'vedalken engineer').
card_uid('vedalken engineer'/'HOP', 'HOP:Vedalken Engineer:vedalken engineer').
card_rarity('vedalken engineer'/'HOP', 'Common').
card_artist('vedalken engineer'/'HOP', 'Lars Grant-West').
card_number('vedalken engineer'/'HOP', '15').
card_flavor_text('vedalken engineer'/'HOP', 'Art is unknown to the vedalken—for them, all creations must serve a purpose.').
card_multiverse_id('vedalken engineer'/'HOP', '205338').

card_in_set('velis vel', 'HOP').
card_original_type('velis vel'/'HOP', 'Plane — Lorwyn').
card_original_text('velis vel'/'HOP', 'Each creature gets +1/+1 for each other creature on the battlefield that shares at least one creature type with it. (For example, if two Elemental Shamans and an Elemental Spirit are on the battlefield, each gets +2/+2.)\nWhenever you roll {C}, target creature gains all creature types until end of turn.').
card_first_print('velis vel', 'HOP').
card_image_name('velis vel'/'HOP', 'velis vel').
card_uid('velis vel'/'HOP', 'HOP:Velis Vel:velis vel').
card_rarity('velis vel'/'HOP', 'Common').
card_artist('velis vel'/'HOP', 'Terese Nielsen').
card_number('velis vel'/'HOP', '40').
card_multiverse_id('velis vel'/'HOP', '198081').

card_in_set('verdant force', 'HOP').
card_original_type('verdant force'/'HOP', 'Creature — Elemental').
card_original_text('verdant force'/'HOP', 'At the beginning of each upkeep, put a 1/1 green Saproling creature token onto the battlefield.').
card_image_name('verdant force'/'HOP', 'verdant force').
card_uid('verdant force'/'HOP', 'HOP:Verdant Force:verdant force').
card_rarity('verdant force'/'HOP', 'Rare').
card_artist('verdant force'/'HOP', 'DiTerlizzi').
card_number('verdant force'/'HOP', '81').
card_flavor_text('verdant force'/'HOP', 'Left to itself, nature overflows any container, overthrows any restriction, and overreaches any boundary.').
card_multiverse_id('verdant force'/'HOP', '205272').

card_in_set('whiplash trap', 'HOP').
card_original_type('whiplash trap'/'HOP', 'Instant — Trap').
card_original_text('whiplash trap'/'HOP', 'If an opponent had two or more creatures enter the battlefield under his or her control this turn, you may pay {U} rather than pay Whiplash Trap\'s mana cost.\nReturn two target creatures to their owners\' hands.').
card_first_print('whiplash trap', 'HOP').
card_image_name('whiplash trap'/'HOP', 'whiplash trap').
card_uid('whiplash trap'/'HOP', 'HOP:Whiplash Trap:whiplash trap').
card_rarity('whiplash trap'/'HOP', 'Common').
card_artist('whiplash trap'/'HOP', 'Zoltan Boros & Gabor Szikszai').
card_number('whiplash trap'/'HOP', '16').
card_multiverse_id('whiplash trap'/'HOP', '205343').

card_in_set('withered wretch', 'HOP').
card_original_type('withered wretch'/'HOP', 'Creature — Zombie Cleric').
card_original_text('withered wretch'/'HOP', '{1}: Exile target card from a graveyard.').
card_image_name('withered wretch'/'HOP', 'withered wretch').
card_uid('withered wretch'/'HOP', 'HOP:Withered Wretch:withered wretch').
card_rarity('withered wretch'/'HOP', 'Uncommon').
card_artist('withered wretch'/'HOP', 'Tim Hildebrandt').
card_number('withered wretch'/'HOP', '45').
card_flavor_text('withered wretch'/'HOP', 'Once it consecrated the dead. Now it desecrates them.').
card_multiverse_id('withered wretch'/'HOP', '205411').

card_in_set('wizard replica', 'HOP').
card_original_type('wizard replica'/'HOP', 'Artifact Creature — Wizard').
card_original_text('wizard replica'/'HOP', 'Flying\n{U}, Sacrifice Wizard Replica: Counter target spell unless its controller pays {2}.').
card_image_name('wizard replica'/'HOP', 'wizard replica').
card_uid('wizard replica'/'HOP', 'HOP:Wizard Replica:wizard replica').
card_rarity('wizard replica'/'HOP', 'Common').
card_artist('wizard replica'/'HOP', 'Carl Critchlow').
card_number('wizard replica'/'HOP', '129').
card_flavor_text('wizard replica'/'HOP', 'It responds with unnatural precision.').
card_multiverse_id('wizard replica'/'HOP', '205314').
