% Arabian Nights

set('ARN').
set_name('ARN', 'Arabian Nights').
set_release_date('ARN', '1993-12-01').
set_border('ARN', 'black').
set_type('ARN', 'expansion').

card_in_set('abu ja\'far', 'ARN').
card_original_type('abu ja\'far'/'ARN', 'Summon — Leper').
card_original_text('abu ja\'far'/'ARN', 'If Abu dies without regenerating while participating in an attack or defense,all creatures Abu is blocking or being blocked by are also killed and may not regenerate.').
card_first_print('abu ja\'far', 'ARN').
card_image_name('abu ja\'far'/'ARN', 'abu ja\'far').
card_uid('abu ja\'far'/'ARN', 'ARN:Abu Ja\'far:abu ja\'far').
card_rarity('abu ja\'far'/'ARN', 'Uncommon').
card_artist('abu ja\'far'/'ARN', 'Ken Meyer, Jr.').
card_multiverse_id('abu ja\'far'/'ARN', '968').

card_in_set('aladdin', 'ARN').
card_original_type('aladdin'/'ARN', 'Summon — Aladdin').
card_original_text('aladdin'/'ARN', '{1}{R}{R} and tap to take control of an artifact from opponent. Artifact is returned when Aladdin is removed from play or when game ends.').
card_first_print('aladdin', 'ARN').
card_image_name('aladdin'/'ARN', 'aladdin').
card_uid('aladdin'/'ARN', 'ARN:Aladdin:aladdin').
card_rarity('aladdin'/'ARN', 'Rare').
card_artist('aladdin'/'ARN', 'Julie Baroh').
card_multiverse_id('aladdin'/'ARN', '955').

card_in_set('aladdin\'s lamp', 'ARN').
card_original_type('aladdin\'s lamp'/'ARN', 'Mono Artifact').
card_original_text('aladdin\'s lamp'/'ARN', '{X} Instead of drawing a card from the top of your library, draw X cards but choose only one to put in your hand. You must shuffle the leftover cards and put them at the bottom of your library.').
card_first_print('aladdin\'s lamp', 'ARN').
card_image_name('aladdin\'s lamp'/'ARN', 'aladdin\'s lamp').
card_uid('aladdin\'s lamp'/'ARN', 'ARN:Aladdin\'s Lamp:aladdin\'s lamp').
card_rarity('aladdin\'s lamp'/'ARN', 'Rare').
card_artist('aladdin\'s lamp'/'ARN', 'Mark Tedin').
card_multiverse_id('aladdin\'s lamp'/'ARN', '900').

card_in_set('aladdin\'s ring', 'ARN').
card_original_type('aladdin\'s ring'/'ARN', 'Mono Artifact').
card_original_text('aladdin\'s ring'/'ARN', '{8}: Do 4 damage to any target.').
card_first_print('aladdin\'s ring', 'ARN').
card_image_name('aladdin\'s ring'/'ARN', 'aladdin\'s ring').
card_uid('aladdin\'s ring'/'ARN', 'ARN:Aladdin\'s Ring:aladdin\'s ring').
card_rarity('aladdin\'s ring'/'ARN', 'Rare').
card_artist('aladdin\'s ring'/'ARN', 'Dan Frazier').
card_flavor_text('aladdin\'s ring'/'ARN', '\"After these words the magician drew a ring off his finger, and put it on one of Aladdin\'s, saying: \'It is a talisman against all evil, so long as you obey me.\'\" —The Arabian Nights, Junior Classics trans.').
card_multiverse_id('aladdin\'s ring'/'ARN', '901').

card_in_set('ali baba', 'ARN').
card_original_type('ali baba'/'ARN', 'Summon — Ali Baba').
card_original_text('ali baba'/'ARN', '{R} Tap a wall.').
card_first_print('ali baba', 'ARN').
card_image_name('ali baba'/'ARN', 'ali baba').
card_uid('ali baba'/'ARN', 'ARN:Ali Baba:ali baba').
card_rarity('ali baba'/'ARN', 'Uncommon').
card_artist('ali baba'/'ARN', 'Julie Baroh').
card_flavor_text('ali baba'/'ARN', '\"When he reached the entrance of the cavern, he pronounced the words, ‘Open, Sesame!\'\" —The Arabian Nights, Junior Classics trans.').
card_multiverse_id('ali baba'/'ARN', '956').

card_in_set('ali from cairo', 'ARN').
card_original_type('ali from cairo'/'ARN', 'Summon — Ali from Cairo').
card_original_text('ali from cairo'/'ARN', 'While Ali is in play, damage that would reduce you to less than 1 life lowers you to 1 life. All further damage is prevented.').
card_first_print('ali from cairo', 'ARN').
card_image_name('ali from cairo'/'ARN', 'ali from cairo').
card_uid('ali from cairo'/'ARN', 'ARN:Ali from Cairo:ali from cairo').
card_rarity('ali from cairo'/'ARN', 'Rare').
card_artist('ali from cairo'/'ARN', 'Mark Poole').
card_multiverse_id('ali from cairo'/'ARN', '957').

card_in_set('army of allah', 'ARN').
card_original_type('army of allah'/'ARN', 'Instant').
card_original_text('army of allah'/'ARN', 'All attacking creatures gain +2/+0 until end of turn.').
card_first_print('army of allah', 'ARN').
card_image_name('army of allah'/'ARN', 'army of allah1').
card_uid('army of allah'/'ARN', 'ARN:Army of Allah:army of allah1').
card_rarity('army of allah'/'ARN', 'Common').
card_artist('army of allah'/'ARN', 'Brian Snõddy').
card_flavor_text('army of allah'/'ARN', 'On the day of victory no one is tired. —Arab proverb').
card_multiverse_id('army of allah'/'ARN', '970').

card_in_set('army of allah', 'ARN').
card_original_type('army of allah'/'ARN', 'Instant').
card_original_text('army of allah'/'ARN', 'All attacking creatures gain +2/+0 until end of turn.').
card_image_name('army of allah'/'ARN', 'army of allah2').
card_uid('army of allah'/'ARN', 'ARN:Army of Allah:army of allah2').
card_rarity('army of allah'/'ARN', 'Common').
card_artist('army of allah'/'ARN', 'Brian Snõddy').
card_flavor_text('army of allah'/'ARN', 'On the day of victory no one is tired. —Arab proverb').
card_multiverse_id('army of allah'/'ARN', '969').

card_in_set('bazaar of baghdad', 'ARN').
card_original_type('bazaar of baghdad'/'ARN', 'Land').
card_original_text('bazaar of baghdad'/'ARN', 'Tap to take two cards from your library, after which you must immediately discard three from your hand to your graveyard. If you don\'t have three or more cards in your hand, discard your whole hand. No spells may be cast between drawing and discarding cards.').
card_first_print('bazaar of baghdad', 'ARN').
card_image_name('bazaar of baghdad'/'ARN', 'bazaar of baghdad').
card_uid('bazaar of baghdad'/'ARN', 'ARN:Bazaar of Baghdad:bazaar of baghdad').
card_rarity('bazaar of baghdad'/'ARN', 'Uncommon').
card_artist('bazaar of baghdad'/'ARN', 'Jeff A. Menges').
card_multiverse_id('bazaar of baghdad'/'ARN', '984').

card_in_set('bird maiden', 'ARN').
card_original_type('bird maiden'/'ARN', 'Summon — Bird Maiden').
card_original_text('bird maiden'/'ARN', 'Flying').
card_first_print('bird maiden', 'ARN').
card_image_name('bird maiden'/'ARN', 'bird maiden1').
card_uid('bird maiden'/'ARN', 'ARN:Bird Maiden:bird maiden1').
card_rarity('bird maiden'/'ARN', 'Common').
card_artist('bird maiden'/'ARN', 'Kaja Foglio').
card_flavor_text('bird maiden'/'ARN', '\"Four things that never meet do here unite To shed my blood and to ravage my heart, A radiant brow and tresses that beguile And rosy cheeks and a glittering smile.\" —The Arabian Nights, trans. Haddawy').
card_multiverse_id('bird maiden'/'ARN', '959').

card_in_set('bird maiden', 'ARN').
card_original_type('bird maiden'/'ARN', 'Summon — Bird Maiden').
card_original_text('bird maiden'/'ARN', 'Flying').
card_image_name('bird maiden'/'ARN', 'bird maiden2').
card_uid('bird maiden'/'ARN', 'ARN:Bird Maiden:bird maiden2').
card_rarity('bird maiden'/'ARN', 'Common').
card_artist('bird maiden'/'ARN', 'Kaja Foglio').
card_flavor_text('bird maiden'/'ARN', '\"Four things that never meet do here unite To shed my blood and to ravage my heart, A radiant brow and tresses that beguile And rosy cheeks and a glittering smile.\" —The Arabian Nights, trans. Haddawy').
card_multiverse_id('bird maiden'/'ARN', '958').

card_in_set('bottle of suleiman', 'ARN').
card_original_type('bottle of suleiman'/'ARN', 'Mono Artifact').
card_original_text('bottle of suleiman'/'ARN', '{1}: Flip a coin, with opponent calling heads or tails while coin is in the air. If the flip ends up in opponent\'s favor, Bottle of Suleiman does 5 damage to you. Otherwise, a 5/5 flying Djinn immediately comes into play on your side. Use a counter to represent Djinn. Djinn is treated exactly like a normal artifact creature except that if it leaves play it is removed from the game entirely. No matter how the flip turns out, Bottle of Suleiman is discarded after use.').
card_first_print('bottle of suleiman', 'ARN').
card_image_name('bottle of suleiman'/'ARN', 'bottle of suleiman').
card_uid('bottle of suleiman'/'ARN', 'ARN:Bottle of Suleiman:bottle of suleiman').
card_rarity('bottle of suleiman'/'ARN', 'Rare').
card_artist('bottle of suleiman'/'ARN', 'Jesper Myrfors').
card_multiverse_id('bottle of suleiman'/'ARN', '902').

card_in_set('brass man', 'ARN').
card_original_type('brass man'/'ARN', 'Artifact Creature').
card_original_text('brass man'/'ARN', 'Brass Man does not untap as normal; you must pay {1} during your untap phase to untap it.').
card_first_print('brass man', 'ARN').
card_image_name('brass man'/'ARN', 'brass man').
card_uid('brass man'/'ARN', 'ARN:Brass Man:brass man').
card_rarity('brass man'/'ARN', 'Uncommon').
card_artist('brass man'/'ARN', 'Christopher Rush').
card_multiverse_id('brass man'/'ARN', '903').

card_in_set('camel', 'ARN').
card_original_type('camel'/'ARN', 'Summon — Camel').
card_original_text('camel'/'ARN', 'Bands\nAll creatures attacking in a band with Camel are immune to damage done by Deserts.').
card_first_print('camel', 'ARN').
card_image_name('camel'/'ARN', 'camel').
card_uid('camel'/'ARN', 'ARN:Camel:camel').
card_rarity('camel'/'ARN', 'Common').
card_artist('camel'/'ARN', 'Sandra Everingham').
card_flavor_text('camel'/'ARN', 'Everyone knew Walid was a pious man, for he had been blessed with many sons, many jewels, and a great many Camels.').
card_multiverse_id('camel'/'ARN', '971').

card_in_set('city in a bottle', 'ARN').
card_original_type('city in a bottle'/'ARN', 'Continuous Artifact').
card_original_text('city in a bottle'/'ARN', 'All cards from Arabian Nights must be discarded from play, except for City in a Bottle. While City in a Bottle is in play, no further cards from Arabian Nights can be played.').
card_first_print('city in a bottle', 'ARN').
card_image_name('city in a bottle'/'ARN', 'city in a bottle').
card_uid('city in a bottle'/'ARN', 'ARN:City in a Bottle:city in a bottle').
card_rarity('city in a bottle'/'ARN', 'Rare').
card_artist('city in a bottle'/'ARN', 'Drew Tucker').
card_multiverse_id('city in a bottle'/'ARN', '904').

card_in_set('city of brass', 'ARN').
card_original_type('city of brass'/'ARN', 'Land').
card_original_text('city of brass'/'ARN', 'Tap to add 1 mana of any color to your mana pool. You suffer 1 damage whenever City of Brass becomes tapped.').
card_first_print('city of brass', 'ARN').
card_image_name('city of brass'/'ARN', 'city of brass').
card_uid('city of brass'/'ARN', 'ARN:City of Brass:city of brass').
card_rarity('city of brass'/'ARN', 'Uncommon').
card_artist('city of brass'/'ARN', 'Mark Tedin').
card_multiverse_id('city of brass'/'ARN', '985').

card_in_set('cuombajj witches', 'ARN').
card_original_type('cuombajj witches'/'ARN', 'Summon — Witches').
card_original_text('cuombajj witches'/'ARN', 'Tap to do 1 damage to any target; opponent may also do 1 damage to any target. You choose your target before opponent does, but damage is inflicted simultaneously.').
card_first_print('cuombajj witches', 'ARN').
card_image_name('cuombajj witches'/'ARN', 'cuombajj witches').
card_uid('cuombajj witches'/'ARN', 'ARN:Cuombajj Witches:cuombajj witches').
card_rarity('cuombajj witches'/'ARN', 'Common').
card_artist('cuombajj witches'/'ARN', 'Kaja Foglio').
card_multiverse_id('cuombajj witches'/'ARN', '914').

card_in_set('cyclone', 'ARN').
card_original_type('cyclone'/'ARN', 'Enchantment').
card_original_text('cyclone'/'ARN', 'Put one chip on Cyclone each round during your upkeep, then pay {G} for each chip or discard Cyclone. If not discarded, Cyclone immediately does 1 damage per chip to each player and each creature in play.').
card_first_print('cyclone', 'ARN').
card_image_name('cyclone'/'ARN', 'cyclone').
card_uid('cyclone'/'ARN', 'ARN:Cyclone:cyclone').
card_rarity('cyclone'/'ARN', 'Uncommon').
card_artist('cyclone'/'ARN', 'Mark Tedin').
card_multiverse_id('cyclone'/'ARN', '942').

card_in_set('dancing scimitar', 'ARN').
card_original_type('dancing scimitar'/'ARN', 'Artifact Creature').
card_original_text('dancing scimitar'/'ARN', 'Flying').
card_first_print('dancing scimitar', 'ARN').
card_image_name('dancing scimitar'/'ARN', 'dancing scimitar').
card_uid('dancing scimitar'/'ARN', 'ARN:Dancing Scimitar:dancing scimitar').
card_rarity('dancing scimitar'/'ARN', 'Rare').
card_artist('dancing scimitar'/'ARN', 'Anson Maddocks').
card_flavor_text('dancing scimitar'/'ARN', 'Bobbing merrily from opponent to opponent, the scimitar began adding playful little flourishes to its strokes; it even turned a couple of somersaults.').
card_multiverse_id('dancing scimitar'/'ARN', '905').

card_in_set('dandân', 'ARN').
card_original_type('dandân'/'ARN', 'Summon — Dandân').
card_original_text('dandân'/'ARN', 'Dandân cannot attack unless opponent has islands in play. Dandân is destroyed immediately if at any time you have no islands in play.').
card_first_print('dandân', 'ARN').
card_image_name('dandân'/'ARN', 'dandan').
card_uid('dandân'/'ARN', 'ARN:Dandân:dandan').
card_rarity('dandân'/'ARN', 'Common').
card_artist('dandân'/'ARN', 'Drew Tucker').
card_multiverse_id('dandân'/'ARN', '929').

card_in_set('desert', 'ARN').
card_original_type('desert'/'ARN', 'Land').
card_original_text('desert'/'ARN', 'Tap to add 1 colorless mana to your mana pool or do 1 damage to an attacking creature after it deals its damage.').
card_first_print('desert', 'ARN').
card_image_name('desert'/'ARN', 'desert').
card_uid('desert'/'ARN', 'ARN:Desert:desert').
card_rarity('desert'/'ARN', 'Common').
card_artist('desert'/'ARN', 'Jesper Myrfors').
card_multiverse_id('desert'/'ARN', '986').

card_in_set('desert nomads', 'ARN').
card_original_type('desert nomads'/'ARN', 'Summon — Nomads').
card_original_text('desert nomads'/'ARN', 'Desertwalk\nDesert Nomads are immune to damage done by Deserts.').
card_first_print('desert nomads', 'ARN').
card_image_name('desert nomads'/'ARN', 'desert nomads').
card_uid('desert nomads'/'ARN', 'ARN:Desert Nomads:desert nomads').
card_rarity('desert nomads'/'ARN', 'Common').
card_artist('desert nomads'/'ARN', 'Christopher Rush').
card_multiverse_id('desert nomads'/'ARN', '960').

card_in_set('desert twister', 'ARN').
card_original_type('desert twister'/'ARN', 'Sorcery').
card_original_text('desert twister'/'ARN', 'Destroy any card in play.').
card_first_print('desert twister', 'ARN').
card_image_name('desert twister'/'ARN', 'desert twister').
card_uid('desert twister'/'ARN', 'ARN:Desert Twister:desert twister').
card_rarity('desert twister'/'ARN', 'Uncommon').
card_artist('desert twister'/'ARN', 'Susan Van Camp').
card_multiverse_id('desert twister'/'ARN', '943').

card_in_set('diamond valley', 'ARN').
card_original_type('diamond valley'/'ARN', 'Land').
card_original_text('diamond valley'/'ARN', 'Tap to sacrifice one of your creatures in exchange for a number of life points equal to its toughness. Note that this ability may be used after blocking has been declared.').
card_first_print('diamond valley', 'ARN').
card_image_name('diamond valley'/'ARN', 'diamond valley').
card_uid('diamond valley'/'ARN', 'ARN:Diamond Valley:diamond valley').
card_rarity('diamond valley'/'ARN', 'Uncommon').
card_artist('diamond valley'/'ARN', 'Brian Snõddy').
card_multiverse_id('diamond valley'/'ARN', '987').

card_in_set('drop of honey', 'ARN').
card_original_type('drop of honey'/'ARN', 'Enchantment').
card_original_text('drop of honey'/'ARN', 'During your upkeep, the creature in play with the lowest power is destroyed and cannot be regenerated. If there is a tie you choose which to destroy. Drop of Honey must be discarded if there are no creatures in play.').
card_first_print('drop of honey', 'ARN').
card_image_name('drop of honey'/'ARN', 'drop of honey').
card_uid('drop of honey'/'ARN', 'ARN:Drop of Honey:drop of honey').
card_rarity('drop of honey'/'ARN', 'Rare').
card_artist('drop of honey'/'ARN', 'Anson Maddocks').
card_multiverse_id('drop of honey'/'ARN', '944').

card_in_set('ebony horse', 'ARN').
card_original_type('ebony horse'/'ARN', 'Mono Artifact').
card_original_text('ebony horse'/'ARN', '{2}: Remove one of your attacking creatures from combat. Treat this as if the creature never attacked, except that defenders assigned to block it cannot choose to block another creature.').
card_first_print('ebony horse', 'ARN').
card_image_name('ebony horse'/'ARN', 'ebony horse').
card_uid('ebony horse'/'ARN', 'ARN:Ebony Horse:ebony horse').
card_rarity('ebony horse'/'ARN', 'Rare').
card_artist('ebony horse'/'ARN', 'Dameon Willich').
card_multiverse_id('ebony horse'/'ARN', '906').

card_in_set('el-hajjâj', 'ARN').
card_original_type('el-hajjâj'/'ARN', 'Summon — El-Hajjâj').
card_original_text('el-hajjâj'/'ARN', 'You gain 1 life for every point of damage El-Hajjâj inflicts.').
card_first_print('el-hajjâj', 'ARN').
card_image_name('el-hajjâj'/'ARN', 'el-hajjaj').
card_uid('el-hajjâj'/'ARN', 'ARN:El-Hajjâj:el-hajjaj').
card_rarity('el-hajjâj'/'ARN', 'Rare').
card_artist('el-hajjâj'/'ARN', 'Dameon Willich').
card_multiverse_id('el-hajjâj'/'ARN', '915').

card_in_set('elephant graveyard', 'ARN').
card_original_type('elephant graveyard'/'ARN', 'Land').
card_original_text('elephant graveyard'/'ARN', 'Tap to add 1 colorless mana to your mana pool or regenerate an Elephant or Mammoth.').
card_first_print('elephant graveyard', 'ARN').
card_image_name('elephant graveyard'/'ARN', 'elephant graveyard').
card_uid('elephant graveyard'/'ARN', 'ARN:Elephant Graveyard:elephant graveyard').
card_rarity('elephant graveyard'/'ARN', 'Rare').
card_artist('elephant graveyard'/'ARN', 'Rob Alexander').
card_multiverse_id('elephant graveyard'/'ARN', '988').

card_in_set('erg raiders', 'ARN').
card_original_type('erg raiders'/'ARN', 'Summon — Raiders').
card_original_text('erg raiders'/'ARN', 'If you do not attack with Raiders, they do 2 damage to you at end of turn. Raiders do no damage to you during the turn in which they are summoned.').
card_first_print('erg raiders', 'ARN').
card_image_name('erg raiders'/'ARN', 'erg raiders1').
card_uid('erg raiders'/'ARN', 'ARN:Erg Raiders:erg raiders1').
card_rarity('erg raiders'/'ARN', 'Common').
card_artist('erg raiders'/'ARN', 'Dameon Willich').
card_multiverse_id('erg raiders'/'ARN', '916').

card_in_set('erg raiders', 'ARN').
card_original_type('erg raiders'/'ARN', 'Summon — Raiders').
card_original_text('erg raiders'/'ARN', 'If you do not attack with Raiders, they do 2 damage to you at end of turn. Raiders do no damage to you during the turn in which they are summoned.').
card_image_name('erg raiders'/'ARN', 'erg raiders2').
card_uid('erg raiders'/'ARN', 'ARN:Erg Raiders:erg raiders2').
card_rarity('erg raiders'/'ARN', 'Common').
card_artist('erg raiders'/'ARN', 'Dameon Willich').
card_multiverse_id('erg raiders'/'ARN', '917').

card_in_set('erhnam djinn', 'ARN').
card_original_type('erhnam djinn'/'ARN', 'Summon — Djinn').
card_original_text('erhnam djinn'/'ARN', 'During your upkeep, you must choose one of opponent\'s non-wall creatures in play. Until your next upkeep, that creature gains the forestwalk ability. If opponent has no creatures, ignore this effect.').
card_first_print('erhnam djinn', 'ARN').
card_image_name('erhnam djinn'/'ARN', 'erhnam djinn').
card_uid('erhnam djinn'/'ARN', 'ARN:Erhnam Djinn:erhnam djinn').
card_rarity('erhnam djinn'/'ARN', 'Rare').
card_artist('erhnam djinn'/'ARN', 'Ken Meyer, Jr.').
card_multiverse_id('erhnam djinn'/'ARN', '945').

card_in_set('eye for an eye', 'ARN').
card_original_type('eye for an eye'/'ARN', 'Instant').
card_original_text('eye for an eye'/'ARN', 'Can be cast only when a creature or spell does damage to you. Eye for an Eye does an equal amount of damage to the controller of that creature or spell. If some spell or effect reduces the amount of damage you receive, it does not reduce the damage dealt by Eye for an Eye.').
card_first_print('eye for an eye', 'ARN').
card_image_name('eye for an eye'/'ARN', 'eye for an eye').
card_uid('eye for an eye'/'ARN', 'ARN:Eye for an Eye:eye for an eye').
card_rarity('eye for an eye'/'ARN', 'Uncommon').
card_artist('eye for an eye'/'ARN', 'Mark Poole').
card_multiverse_id('eye for an eye'/'ARN', '972').

card_in_set('fishliver oil', 'ARN').
card_original_type('fishliver oil'/'ARN', 'Enchant Creature').
card_original_text('fishliver oil'/'ARN', 'Target creature gains islandwalk ability.').
card_first_print('fishliver oil', 'ARN').
card_image_name('fishliver oil'/'ARN', 'fishliver oil1').
card_uid('fishliver oil'/'ARN', 'ARN:Fishliver Oil:fishliver oil1').
card_rarity('fishliver oil'/'ARN', 'Common').
card_artist('fishliver oil'/'ARN', 'Anson Maddocks').
card_flavor_text('fishliver oil'/'ARN', 'Then the maiden bade him cast off his robes and cover his body with fishliver oil, that he might safely follow her into the sea.').
card_multiverse_id('fishliver oil'/'ARN', '930').

card_in_set('fishliver oil', 'ARN').
card_original_type('fishliver oil'/'ARN', 'Enchant Creature').
card_original_text('fishliver oil'/'ARN', 'Target creature gains islandwalk ability.').
card_image_name('fishliver oil'/'ARN', 'fishliver oil2').
card_uid('fishliver oil'/'ARN', 'ARN:Fishliver Oil:fishliver oil2').
card_rarity('fishliver oil'/'ARN', 'Common').
card_artist('fishliver oil'/'ARN', 'Anson Maddocks').
card_flavor_text('fishliver oil'/'ARN', 'Then the maiden bade him cast off his robes and cover his body with fishliver oil, that he might safely follow her into the sea.').
card_multiverse_id('fishliver oil'/'ARN', '931').

card_in_set('flying carpet', 'ARN').
card_original_type('flying carpet'/'ARN', 'Mono Artifact').
card_original_text('flying carpet'/'ARN', '{2}: Gives one creature flying ability until end of turn. If that creature is destroyed before end of turn, so is Flying Carpet.').
card_first_print('flying carpet', 'ARN').
card_image_name('flying carpet'/'ARN', 'flying carpet').
card_uid('flying carpet'/'ARN', 'ARN:Flying Carpet:flying carpet').
card_rarity('flying carpet'/'ARN', 'Uncommon').
card_artist('flying carpet'/'ARN', 'Mark Tedin').
card_multiverse_id('flying carpet'/'ARN', '907').

card_in_set('flying men', 'ARN').
card_original_type('flying men'/'ARN', 'Summon — Flying Men').
card_original_text('flying men'/'ARN', 'Flying').
card_first_print('flying men', 'ARN').
card_image_name('flying men'/'ARN', 'flying men').
card_uid('flying men'/'ARN', 'ARN:Flying Men:flying men').
card_rarity('flying men'/'ARN', 'Common').
card_artist('flying men'/'ARN', 'Christopher Rush').
card_flavor_text('flying men'/'ARN', 'Saffiyah clapped her hands and twenty flying men appeared at her side, each well trained in the art of combat.').
card_multiverse_id('flying men'/'ARN', '932').

card_in_set('ghazbán ogre', 'ARN').
card_original_type('ghazbán ogre'/'ARN', 'Summon — Ogre').
card_original_text('ghazbán ogre'/'ARN', 'During its current controller\'s upkeep, the player with the highest life total takes control of Ghazbán Ogre.').
card_first_print('ghazbán ogre', 'ARN').
card_image_name('ghazbán ogre'/'ARN', 'ghazban ogre').
card_uid('ghazbán ogre'/'ARN', 'ARN:Ghazbán Ogre:ghazban ogre').
card_rarity('ghazbán ogre'/'ARN', 'Common').
card_artist('ghazbán ogre'/'ARN', 'Jesper Myrfors').
card_multiverse_id('ghazbán ogre'/'ARN', '946').

card_in_set('giant tortoise', 'ARN').
card_original_type('giant tortoise'/'ARN', 'Summon — Tortoise').
card_original_text('giant tortoise'/'ARN', 'Giant Tortoise gains +0/+3 while untapped.').
card_first_print('giant tortoise', 'ARN').
card_image_name('giant tortoise'/'ARN', 'giant tortoise1').
card_uid('giant tortoise'/'ARN', 'ARN:Giant Tortoise:giant tortoise1').
card_rarity('giant tortoise'/'ARN', 'Common').
card_artist('giant tortoise'/'ARN', 'Kaja Foglio').
card_multiverse_id('giant tortoise'/'ARN', '934').

card_in_set('giant tortoise', 'ARN').
card_original_type('giant tortoise'/'ARN', 'Summon — Tortoise').
card_original_text('giant tortoise'/'ARN', 'Giant Tortoise gains +0/+3 while untapped.').
card_image_name('giant tortoise'/'ARN', 'giant tortoise2').
card_uid('giant tortoise'/'ARN', 'ARN:Giant Tortoise:giant tortoise2').
card_rarity('giant tortoise'/'ARN', 'Common').
card_artist('giant tortoise'/'ARN', 'Kaja Foglio').
card_multiverse_id('giant tortoise'/'ARN', '933').

card_in_set('guardian beast', 'ARN').
card_original_type('guardian beast'/'ARN', 'Summon — Guardian').
card_original_text('guardian beast'/'ARN', 'As long as Guardian Beast is untapped, your non-creature artifacts cannot be further enchanted, destroyed, or taken under someone else\'s control. If something occurs that would destroy the Guardian Beast and artifacts simultaneously, the Guardian Beast is destroyed but your artifacts are not. If an artifact is enchanted or stolen while Guardian Beast is tapped, it remains so when Guardian Beast becomes untapped.').
card_first_print('guardian beast', 'ARN').
card_image_name('guardian beast'/'ARN', 'guardian beast').
card_uid('guardian beast'/'ARN', 'ARN:Guardian Beast:guardian beast').
card_rarity('guardian beast'/'ARN', 'Rare').
card_artist('guardian beast'/'ARN', 'Ken Meyer, Jr.').
card_multiverse_id('guardian beast'/'ARN', '918').

card_in_set('hasran ogress', 'ARN').
card_original_type('hasran ogress'/'ARN', 'Summon — Ogre').
card_original_text('hasran ogress'/'ARN', 'Unless you pay {2} each time Hasran Ogress\nattacks, Hasran Ogress does 3 damage to you.').
card_first_print('hasran ogress', 'ARN').
card_image_name('hasran ogress'/'ARN', 'hasran ogress1').
card_uid('hasran ogress'/'ARN', 'ARN:Hasran Ogress:hasran ogress1').
card_rarity('hasran ogress'/'ARN', 'Common').
card_artist('hasran ogress'/'ARN', 'Dan Frazier').
card_multiverse_id('hasran ogress'/'ARN', '919').

card_in_set('hasran ogress', 'ARN').
card_original_type('hasran ogress'/'ARN', 'Summon — Ogre').
card_original_text('hasran ogress'/'ARN', 'Unless you pay {2} each time Hasran Ogress\nattacks, Hasran Ogress does 3 damage to you.').
card_image_name('hasran ogress'/'ARN', 'hasran ogress2').
card_uid('hasran ogress'/'ARN', 'ARN:Hasran Ogress:hasran ogress2').
card_rarity('hasran ogress'/'ARN', 'Common').
card_artist('hasran ogress'/'ARN', 'Dan Frazier').
card_multiverse_id('hasran ogress'/'ARN', '920').

card_in_set('hurr jackal', 'ARN').
card_original_type('hurr jackal'/'ARN', 'Summon — Jackal').
card_original_text('hurr jackal'/'ARN', 'Tap to prevent a target creature from regenerating for the remainder of the turn.').
card_first_print('hurr jackal', 'ARN').
card_image_name('hurr jackal'/'ARN', 'hurr jackal').
card_uid('hurr jackal'/'ARN', 'ARN:Hurr Jackal:hurr jackal').
card_rarity('hurr jackal'/'ARN', 'Common').
card_artist('hurr jackal'/'ARN', 'Drew Tucker').
card_multiverse_id('hurr jackal'/'ARN', '961').

card_in_set('ifh-bíff efreet', 'ARN').
card_original_type('ifh-bíff efreet'/'ARN', 'Summon — Efreet').
card_original_text('ifh-bíff efreet'/'ARN', 'Flying\nWhile Ifh-Bíff Efreet is in play, any player can pay {G} to have Ifh-Bíff Efreet do 1 damage to each player and each flying creature in play. This ability does not tap the Ifh-Bíff Efreet, and can be used as soon as it is successfully summoned.').
card_first_print('ifh-bíff efreet', 'ARN').
card_image_name('ifh-bíff efreet'/'ARN', 'ifh-biff efreet').
card_uid('ifh-bíff efreet'/'ARN', 'ARN:Ifh-Bíff Efreet:ifh-biff efreet').
card_rarity('ifh-bíff efreet'/'ARN', 'Rare').
card_artist('ifh-bíff efreet'/'ARN', 'Jesper Myrfors').
card_multiverse_id('ifh-bíff efreet'/'ARN', '947').

card_in_set('island fish jasconius', 'ARN').
card_original_type('island fish jasconius'/'ARN', 'Summon — Island Fish').
card_original_text('island fish jasconius'/'ARN', 'You must pay {U}{U}{U} during your untap phase to untap Island Fish. Island Fish cannot attack unless opponent has islands in play. Island Fish is destroyed immediately if at any time you have no islands in play.').
card_first_print('island fish jasconius', 'ARN').
card_image_name('island fish jasconius'/'ARN', 'island fish jasconius').
card_uid('island fish jasconius'/'ARN', 'ARN:Island Fish Jasconius:island fish jasconius').
card_rarity('island fish jasconius'/'ARN', 'Rare').
card_artist('island fish jasconius'/'ARN', 'Jesper Myrfors').
card_multiverse_id('island fish jasconius'/'ARN', '935').

card_in_set('island of wak-wak', 'ARN').
card_original_type('island of wak-wak'/'ARN', 'Land').
card_original_text('island of wak-wak'/'ARN', 'Tap to reduce target flying creature\'s power to 0.').
card_first_print('island of wak-wak', 'ARN').
card_image_name('island of wak-wak'/'ARN', 'island of wak-wak').
card_uid('island of wak-wak'/'ARN', 'ARN:Island of Wak-Wak:island of wak-wak').
card_rarity('island of wak-wak'/'ARN', 'Rare').
card_artist('island of wak-wak'/'ARN', 'Douglas Shuler').
card_flavor_text('island of wak-wak'/'ARN', 'The Isle of Wak-Wak, home to a tribe of winged folk, is named for a peculiar fruit that grows there. The fruit looks like a woman\'s head, and when ripe speaks the word \"Wak.\"').
card_multiverse_id('island of wak-wak'/'ARN', '989').

card_in_set('jandor\'s ring', 'ARN').
card_original_type('jandor\'s ring'/'ARN', 'Mono Artifact').
card_original_text('jandor\'s ring'/'ARN', '{2}: Discard a card you just drew from your library, and draw another card to replace it.').
card_first_print('jandor\'s ring', 'ARN').
card_image_name('jandor\'s ring'/'ARN', 'jandor\'s ring').
card_uid('jandor\'s ring'/'ARN', 'ARN:Jandor\'s Ring:jandor\'s ring').
card_rarity('jandor\'s ring'/'ARN', 'Rare').
card_artist('jandor\'s ring'/'ARN', 'Dan Frazier').
card_multiverse_id('jandor\'s ring'/'ARN', '908').

card_in_set('jandor\'s saddlebags', 'ARN').
card_original_type('jandor\'s saddlebags'/'ARN', 'Mono Artifact').
card_original_text('jandor\'s saddlebags'/'ARN', '{3}: Untap a creature.').
card_first_print('jandor\'s saddlebags', 'ARN').
card_image_name('jandor\'s saddlebags'/'ARN', 'jandor\'s saddlebags').
card_uid('jandor\'s saddlebags'/'ARN', 'ARN:Jandor\'s Saddlebags:jandor\'s saddlebags').
card_rarity('jandor\'s saddlebags'/'ARN', 'Rare').
card_artist('jandor\'s saddlebags'/'ARN', 'Dameon Willich').
card_flavor_text('jandor\'s saddlebags'/'ARN', 'Each day of their journey, Jandor opened the saddlebags and found them full of mutton, quinces, cheese, date rolls, wine, and all manner of delicious and satisfying foods.').
card_multiverse_id('jandor\'s saddlebags'/'ARN', '909').

card_in_set('jeweled bird', 'ARN').
card_original_type('jeweled bird'/'ARN', 'Mono Artifact').
card_original_text('jeweled bird'/'ARN', 'Draw a card, and exchange Jeweled Bird for your contribution to the ante. Your former contribution goes to your graveyard. Remove this card from your deck before playing if you are not playing for ante.').
card_first_print('jeweled bird', 'ARN').
card_image_name('jeweled bird'/'ARN', 'jeweled bird').
card_uid('jeweled bird'/'ARN', 'ARN:Jeweled Bird:jeweled bird').
card_rarity('jeweled bird'/'ARN', 'Uncommon').
card_artist('jeweled bird'/'ARN', 'Amy Weber').
card_multiverse_id('jeweled bird'/'ARN', '910').

card_in_set('jihad', 'ARN').
card_original_type('jihad'/'ARN', 'Enchantment').
card_original_text('jihad'/'ARN', 'Choose a color. As long as opponent has cards of this color in play, all white creatures gain +2/+1. Jihad must be discarded immediately if at any time opponent has no cards of this color in play.').
card_first_print('jihad', 'ARN').
card_image_name('jihad'/'ARN', 'jihad').
card_uid('jihad'/'ARN', 'ARN:Jihad:jihad').
card_rarity('jihad'/'ARN', 'Rare').
card_artist('jihad'/'ARN', 'Brian Snõddy').
card_multiverse_id('jihad'/'ARN', '973').

card_in_set('junún efreet', 'ARN').
card_original_type('junún efreet'/'ARN', 'Summon — Efreet').
card_original_text('junún efreet'/'ARN', 'Flying\nYou must pay {B}{B} during your upkeep or Junún Efreet is destroyed and may not regenerate.').
card_first_print('junún efreet', 'ARN').
card_image_name('junún efreet'/'ARN', 'junun efreet').
card_uid('junún efreet'/'ARN', 'ARN:Junún Efreet:junun efreet').
card_rarity('junún efreet'/'ARN', 'Rare').
card_artist('junún efreet'/'ARN', 'Christopher Rush').
card_multiverse_id('junún efreet'/'ARN', '921').

card_in_set('juzám djinn', 'ARN').
card_original_type('juzám djinn'/'ARN', 'Summon — Djinn').
card_original_text('juzám djinn'/'ARN', 'Juzám Djinn does 1 damage to you during your upkeep.').
card_first_print('juzám djinn', 'ARN').
card_image_name('juzám djinn'/'ARN', 'juzam djinn').
card_uid('juzám djinn'/'ARN', 'ARN:Juzám Djinn:juzam djinn').
card_rarity('juzám djinn'/'ARN', 'Rare').
card_artist('juzám djinn'/'ARN', 'Mark Tedin').
card_flavor_text('juzám djinn'/'ARN', '\"Expect my visit when the darkness comes. The night I think is best for hiding all.\" —Ouallada').
card_multiverse_id('juzám djinn'/'ARN', '922').

card_in_set('khabál ghoul', 'ARN').
card_original_type('khabál ghoul'/'ARN', 'Summon — Ghoul').
card_original_text('khabál ghoul'/'ARN', 'At the end of each turn, put a +1/+1 counter on Khabál Ghoul for each other creature that died during the turn and was not regenerated.').
card_first_print('khabál ghoul', 'ARN').
card_image_name('khabál ghoul'/'ARN', 'khabal ghoul').
card_uid('khabál ghoul'/'ARN', 'ARN:Khabál Ghoul:khabal ghoul').
card_rarity('khabál ghoul'/'ARN', 'Uncommon').
card_artist('khabál ghoul'/'ARN', 'Douglas Shuler').
card_multiverse_id('khabál ghoul'/'ARN', '923').

card_in_set('king suleiman', 'ARN').
card_original_type('king suleiman'/'ARN', 'Summon — King').
card_original_text('king suleiman'/'ARN', 'Tap to destroy an Efreet or Djinn.').
card_first_print('king suleiman', 'ARN').
card_image_name('king suleiman'/'ARN', 'king suleiman').
card_uid('king suleiman'/'ARN', 'ARN:King Suleiman:king suleiman').
card_rarity('king suleiman'/'ARN', 'Rare').
card_artist('king suleiman'/'ARN', 'Mark Poole').
card_flavor_text('king suleiman'/'ARN', '\"We made tempestuous winds obedient to Solomon . . . And many of the devils We also made obedient to him.\" —The Qur\'an, 21:81').
card_multiverse_id('king suleiman'/'ARN', '974').

card_in_set('kird ape', 'ARN').
card_original_type('kird ape'/'ARN', 'Summon — Ape').
card_original_text('kird ape'/'ARN', 'Kird Ape gains +1/+2 if you have any forests in play.').
card_first_print('kird ape', 'ARN').
card_image_name('kird ape'/'ARN', 'kird ape').
card_uid('kird ape'/'ARN', 'ARN:Kird Ape:kird ape').
card_rarity('kird ape'/'ARN', 'Common').
card_artist('kird ape'/'ARN', 'Ken Meyer, Jr.').
card_multiverse_id('kird ape'/'ARN', '962').

card_in_set('library of alexandria', 'ARN').
card_original_type('library of alexandria'/'ARN', 'Land').
card_original_text('library of alexandria'/'ARN', 'Tap to add 1 colorless mana to your mana pool or draw a card from your library; you may use the card-drawing ability only if you have exactly seven cards in your hand.').
card_first_print('library of alexandria', 'ARN').
card_image_name('library of alexandria'/'ARN', 'library of alexandria').
card_uid('library of alexandria'/'ARN', 'ARN:Library of Alexandria:library of alexandria').
card_rarity('library of alexandria'/'ARN', 'Uncommon').
card_artist('library of alexandria'/'ARN', 'Mark Poole').
card_multiverse_id('library of alexandria'/'ARN', '990').

card_in_set('magnetic mountain', 'ARN').
card_original_type('magnetic mountain'/'ARN', 'Enchantment').
card_original_text('magnetic mountain'/'ARN', 'Blue creatures do not untap as normal. During their untap phases, players must spend {4} for each blue creature they wish to untap. This cost must be paid in addition to any other untap cost a given blue creature may already require.').
card_first_print('magnetic mountain', 'ARN').
card_image_name('magnetic mountain'/'ARN', 'magnetic mountain').
card_uid('magnetic mountain'/'ARN', 'ARN:Magnetic Mountain:magnetic mountain').
card_rarity('magnetic mountain'/'ARN', 'Uncommon').
card_artist('magnetic mountain'/'ARN', 'Susan Van Camp').
card_multiverse_id('magnetic mountain'/'ARN', '963').

card_in_set('merchant ship', 'ARN').
card_original_type('merchant ship'/'ARN', 'Summon — Ship').
card_original_text('merchant ship'/'ARN', 'If Merchant Ship attacks and is not blocked, you gain 2 life. Merchant Ship cannot attack unless opponent has islands in play. Merchant Ship is destroyed immediately if at any time you have no islands in play.').
card_first_print('merchant ship', 'ARN').
card_image_name('merchant ship'/'ARN', 'merchant ship').
card_uid('merchant ship'/'ARN', 'ARN:Merchant Ship:merchant ship').
card_rarity('merchant ship'/'ARN', 'Uncommon').
card_artist('merchant ship'/'ARN', 'Tom Wänerstrand').
card_multiverse_id('merchant ship'/'ARN', '936').

card_in_set('metamorphosis', 'ARN').
card_original_type('metamorphosis'/'ARN', 'Sorcery').
card_original_text('metamorphosis'/'ARN', 'Sacrifice a creature of yours in play for an amount of mana equal to its casting cost plus 1. This mana can be of any one color, and can only be used to summon creatures.').
card_first_print('metamorphosis', 'ARN').
card_image_name('metamorphosis'/'ARN', 'metamorphosis').
card_uid('metamorphosis'/'ARN', 'ARN:Metamorphosis:metamorphosis').
card_rarity('metamorphosis'/'ARN', 'Common').
card_artist('metamorphosis'/'ARN', 'Christopher Rush').
card_multiverse_id('metamorphosis'/'ARN', '948').

card_in_set('mijae djinn', 'ARN').
card_original_type('mijae djinn'/'ARN', 'Summon — Djinn').
card_original_text('mijae djinn'/'ARN', 'If you choose to attack with Mijae Djinn, flip a coin immediately after attack is announced; opponent calls heads or tails while coin is in the air. If the flip ends up in opponent\'s favor, Mijae Djinn is tapped but does not attack.').
card_first_print('mijae djinn', 'ARN').
card_image_name('mijae djinn'/'ARN', 'mijae djinn').
card_uid('mijae djinn'/'ARN', 'ARN:Mijae Djinn:mijae djinn').
card_rarity('mijae djinn'/'ARN', 'Rare').
card_artist('mijae djinn'/'ARN', 'Susan Van Camp').
card_multiverse_id('mijae djinn'/'ARN', '964').

card_in_set('moorish cavalry', 'ARN').
card_original_type('moorish cavalry'/'ARN', 'Summon — Cavalry').
card_original_text('moorish cavalry'/'ARN', 'Trample').
card_first_print('moorish cavalry', 'ARN').
card_image_name('moorish cavalry'/'ARN', 'moorish cavalry1').
card_uid('moorish cavalry'/'ARN', 'ARN:Moorish Cavalry:moorish cavalry1').
card_rarity('moorish cavalry'/'ARN', 'Common').
card_artist('moorish cavalry'/'ARN', 'Dameon Willich').
card_flavor_text('moorish cavalry'/'ARN', 'Members of the elite Moorish Cavalry are very particular about their mounts, choosing only those whose bloodlines have been pure for generations.').
card_multiverse_id('moorish cavalry'/'ARN', '975').

card_in_set('moorish cavalry', 'ARN').
card_original_type('moorish cavalry'/'ARN', 'Summon — Cavalry').
card_original_text('moorish cavalry'/'ARN', 'Trample').
card_image_name('moorish cavalry'/'ARN', 'moorish cavalry2').
card_uid('moorish cavalry'/'ARN', 'ARN:Moorish Cavalry:moorish cavalry2').
card_rarity('moorish cavalry'/'ARN', 'Common').
card_artist('moorish cavalry'/'ARN', 'Dameon Willich').
card_flavor_text('moorish cavalry'/'ARN', 'Members of the elite Moorish Cavalry are very particular about their mounts, choosing only those whose bloodlines have been pure for generations.').
card_multiverse_id('moorish cavalry'/'ARN', '976').

card_in_set('mountain', 'ARN').
card_original_type('mountain'/'ARN', 'Land').
card_original_text('mountain'/'ARN', 'Tap to add {R} to your mana pool.').
card_image_name('mountain'/'ARN', 'mountain').
card_uid('mountain'/'ARN', 'ARN:Mountain:mountain').
card_rarity('mountain'/'ARN', 'Basic Land').
card_artist('mountain'/'ARN', 'Douglas Shuler').
card_multiverse_id('mountain'/'ARN', '983').

card_in_set('nafs asp', 'ARN').
card_original_type('nafs asp'/'ARN', 'Summon — Asp').
card_original_text('nafs asp'/'ARN', 'If Asp inflicts any damage on your opponent, your opponent must spend {1} before the draw phase of his or her next turn or lose an additional 1 life.').
card_first_print('nafs asp', 'ARN').
card_image_name('nafs asp'/'ARN', 'nafs asp1').
card_uid('nafs asp'/'ARN', 'ARN:Nafs Asp:nafs asp1').
card_rarity('nafs asp'/'ARN', 'Common').
card_artist('nafs asp'/'ARN', 'Christopher Rush').
card_multiverse_id('nafs asp'/'ARN', '949').

card_in_set('nafs asp', 'ARN').
card_original_type('nafs asp'/'ARN', 'Summon — Asp').
card_original_text('nafs asp'/'ARN', 'If Asp inflicts any damage on your opponent, your opponent must spend {1} before the draw phase of his or her next turn or lose an additional 1 life.').
card_image_name('nafs asp'/'ARN', 'nafs asp2').
card_uid('nafs asp'/'ARN', 'ARN:Nafs Asp:nafs asp2').
card_rarity('nafs asp'/'ARN', 'Common').
card_artist('nafs asp'/'ARN', 'Christopher Rush').
card_multiverse_id('nafs asp'/'ARN', '950').

card_in_set('oasis', 'ARN').
card_original_type('oasis'/'ARN', 'Land').
card_original_text('oasis'/'ARN', 'Tap to prevent 1 damage to any creature.').
card_first_print('oasis', 'ARN').
card_image_name('oasis'/'ARN', 'oasis').
card_uid('oasis'/'ARN', 'ARN:Oasis:oasis').
card_rarity('oasis'/'ARN', 'Uncommon').
card_artist('oasis'/'ARN', 'Brian Snõddy').
card_multiverse_id('oasis'/'ARN', '991').

card_in_set('old man of the sea', 'ARN').
card_original_type('old man of the sea'/'ARN', 'Summon — Marid').
card_original_text('old man of the sea'/'ARN', 'Tap to gain control of a creature with power no greater than Old Man\'s power. If Old Man becomes untapped, you lose control of this creature; you may choose not to untap Old Man as normal. You also lose control of the creature if Old Man dies or if the creature\'s power becomes greater than Old Man\'s.').
card_first_print('old man of the sea', 'ARN').
card_image_name('old man of the sea'/'ARN', 'old man of the sea').
card_uid('old man of the sea'/'ARN', 'ARN:Old Man of the Sea:old man of the sea').
card_rarity('old man of the sea'/'ARN', 'Rare').
card_artist('old man of the sea'/'ARN', 'Susan Van Camp').
card_multiverse_id('old man of the sea'/'ARN', '937').

card_in_set('oubliette', 'ARN').
card_original_type('oubliette'/'ARN', 'Enchantment').
card_original_text('oubliette'/'ARN', 'Select a creature in play when Oubliette is cast. That creature is considered out of play as long as Oubliette is in play. Hence the creature cannot be the target of spells and cannot receive damage, use special powers, attack, or defend. All counters and enchantments on the creature remain but are also out of play. If Oubliette is removed, creature returns to play tapped.').
card_first_print('oubliette', 'ARN').
card_image_name('oubliette'/'ARN', 'oubliette1').
card_uid('oubliette'/'ARN', 'ARN:Oubliette:oubliette1').
card_rarity('oubliette'/'ARN', 'Common').
card_artist('oubliette'/'ARN', 'Douglas Shuler').
card_multiverse_id('oubliette'/'ARN', '924').

card_in_set('oubliette', 'ARN').
card_original_type('oubliette'/'ARN', 'Enchantment').
card_original_text('oubliette'/'ARN', 'Select a creature in play when Oubliette is cast. That creature is considered out of play as long as Oubliette is in play. Hence the creature cannot be the target of spells and cannot receive damage, use special powers, attack, or defend. All counters and enchantments on the creature remain but are also out of play. If Oubliette is removed, creature returns to play tapped.').
card_image_name('oubliette'/'ARN', 'oubliette2').
card_uid('oubliette'/'ARN', 'ARN:Oubliette:oubliette2').
card_rarity('oubliette'/'ARN', 'Common').
card_artist('oubliette'/'ARN', 'Douglas Shuler').
card_multiverse_id('oubliette'/'ARN', '925').

card_in_set('piety', 'ARN').
card_original_type('piety'/'ARN', 'Instant').
card_original_text('piety'/'ARN', 'All defending creatures gain +0/+3 until end of turn.').
card_first_print('piety', 'ARN').
card_image_name('piety'/'ARN', 'piety1').
card_uid('piety'/'ARN', 'ARN:Piety:piety1').
card_rarity('piety'/'ARN', 'Common').
card_artist('piety'/'ARN', 'Mark Poole').
card_flavor_text('piety'/'ARN', '\"Whoever obeys God and His Prophet, fears God and does his duty to Him, will surely find success.\" —The Qur\'an, 24:52').
card_multiverse_id('piety'/'ARN', '978').

card_in_set('piety', 'ARN').
card_original_type('piety'/'ARN', 'Instant').
card_original_text('piety'/'ARN', 'All defending creatures gain +0/+3 until end of turn.').
card_image_name('piety'/'ARN', 'piety2').
card_uid('piety'/'ARN', 'ARN:Piety:piety2').
card_rarity('piety'/'ARN', 'Common').
card_artist('piety'/'ARN', 'Mark Poole').
card_flavor_text('piety'/'ARN', '\"Whoever obeys God and His Prophet, fears God and does his duty to Him, will surely find success.\" —The Qur\'an, 24:52').
card_multiverse_id('piety'/'ARN', '977').

card_in_set('pyramids', 'ARN').
card_original_type('pyramids'/'ARN', 'Poly Artifact').
card_original_text('pyramids'/'ARN', '{2}: Prevents a land from being destroyed, or removes an enchantment from any land.').
card_first_print('pyramids', 'ARN').
card_image_name('pyramids'/'ARN', 'pyramids').
card_uid('pyramids'/'ARN', 'ARN:Pyramids:pyramids').
card_rarity('pyramids'/'ARN', 'Rare').
card_artist('pyramids'/'ARN', 'Amy Weber').
card_multiverse_id('pyramids'/'ARN', '911').

card_in_set('repentant blacksmith', 'ARN').
card_original_type('repentant blacksmith'/'ARN', 'Summon — Smith').
card_original_text('repentant blacksmith'/'ARN', 'Protection from red').
card_first_print('repentant blacksmith', 'ARN').
card_image_name('repentant blacksmith'/'ARN', 'repentant blacksmith').
card_uid('repentant blacksmith'/'ARN', 'ARN:Repentant Blacksmith:repentant blacksmith').
card_rarity('repentant blacksmith'/'ARN', 'Rare').
card_artist('repentant blacksmith'/'ARN', 'Drew Tucker').
card_flavor_text('repentant blacksmith'/'ARN', '\"For my confession they burned me with fire And found that I was for endurance made.\" —The Arabian Nights, trans. Haddawy').
card_multiverse_id('repentant blacksmith'/'ARN', '979').

card_in_set('ring of ma\'rûf', 'ARN').
card_original_type('ring of ma\'rûf'/'ARN', 'Mono Artifact').
card_original_text('ring of ma\'rûf'/'ARN', '{5}: Instead of drawing a card from the top of your library, select one of your cards from outside the game. This card can be any card you have that you\'re not using in your deck or that for some reason has left the game. Ring of Ma\'rûf is removed from the game entirely after use.').
card_first_print('ring of ma\'rûf', 'ARN').
card_image_name('ring of ma\'rûf'/'ARN', 'ring of ma\'ruf').
card_uid('ring of ma\'rûf'/'ARN', 'ARN:Ring of Ma\'rûf:ring of ma\'ruf').
card_rarity('ring of ma\'rûf'/'ARN', 'Rare').
card_artist('ring of ma\'rûf'/'ARN', 'Dan Frazier').
card_multiverse_id('ring of ma\'rûf'/'ARN', '912').

card_in_set('rukh egg', 'ARN').
card_original_type('rukh egg'/'ARN', 'Summon — Egg').
card_original_text('rukh egg'/'ARN', 'If Rukh Egg goes to the graveyard, a Rukh—a 4/4 red flying creature—comes into play on your side at the end of that turn. Use a counter to represent Rukh. Rukh is treated exactly like a normal creature except that if it leaves play it is removed from the game entirely.').
card_first_print('rukh egg', 'ARN').
card_image_name('rukh egg'/'ARN', 'rukh egg1').
card_uid('rukh egg'/'ARN', 'ARN:Rukh Egg:rukh egg1').
card_rarity('rukh egg'/'ARN', 'Common').
card_artist('rukh egg'/'ARN', 'Christopher Rush').
card_multiverse_id('rukh egg'/'ARN', '966').

card_in_set('rukh egg', 'ARN').
card_original_type('rukh egg'/'ARN', 'Summon — Egg').
card_original_text('rukh egg'/'ARN', 'If Rukh Egg goes to the graveyard, a Rukh—a 4/4 red flying creature—comes into play on your side at the end of that turn. Use a counter to represent Rukh. Rukh is treated exactly like a normal creature except that if it leaves play it is removed from the game entirely.').
card_image_name('rukh egg'/'ARN', 'rukh egg2').
card_uid('rukh egg'/'ARN', 'ARN:Rukh Egg:rukh egg2').
card_rarity('rukh egg'/'ARN', 'Common').
card_artist('rukh egg'/'ARN', 'Christopher Rush').
card_multiverse_id('rukh egg'/'ARN', '965').

card_in_set('sandals of abdallah', 'ARN').
card_original_type('sandals of abdallah'/'ARN', 'Mono Artifact').
card_original_text('sandals of abdallah'/'ARN', '{2}: Gives one creature islandwalk ability until end of turn. If that creature is destroyed before end of turn, so are Sandals.').
card_first_print('sandals of abdallah', 'ARN').
card_image_name('sandals of abdallah'/'ARN', 'sandals of abdallah').
card_uid('sandals of abdallah'/'ARN', 'ARN:Sandals of Abdallah:sandals of abdallah').
card_rarity('sandals of abdallah'/'ARN', 'Uncommon').
card_artist('sandals of abdallah'/'ARN', 'Dan Frazier').
card_multiverse_id('sandals of abdallah'/'ARN', '913').

card_in_set('sandstorm', 'ARN').
card_original_type('sandstorm'/'ARN', 'Instant').
card_original_text('sandstorm'/'ARN', 'All attacking creatures suffer 1 damage.').
card_first_print('sandstorm', 'ARN').
card_image_name('sandstorm'/'ARN', 'sandstorm').
card_uid('sandstorm'/'ARN', 'ARN:Sandstorm:sandstorm').
card_rarity('sandstorm'/'ARN', 'Common').
card_artist('sandstorm'/'ARN', 'Brian Snõddy').
card_flavor_text('sandstorm'/'ARN', 'Even the landscape turned against Sarsour, first rising up and pelting him, then rearranging itself so he could no longer find his way.').
card_multiverse_id('sandstorm'/'ARN', '951').

card_in_set('serendib djinn', 'ARN').
card_original_type('serendib djinn'/'ARN', 'Summon — Djinn').
card_original_text('serendib djinn'/'ARN', 'Flying\nDuring your upkeep, you must choose one of your own lands and destroy it. If you destroy an island in this manner, Serendib Djinn does 3 damage to you. Serendib Djinn is destroyed immediately if at any time you have no land in play.').
card_first_print('serendib djinn', 'ARN').
card_image_name('serendib djinn'/'ARN', 'serendib djinn').
card_uid('serendib djinn'/'ARN', 'ARN:Serendib Djinn:serendib djinn').
card_rarity('serendib djinn'/'ARN', 'Rare').
card_artist('serendib djinn'/'ARN', 'Anson Maddocks').
card_multiverse_id('serendib djinn'/'ARN', '938').

card_in_set('serendib efreet', 'ARN').
card_original_type('serendib efreet'/'ARN', 'Summon — Efreet').
card_original_text('serendib efreet'/'ARN', 'Flying\nSerendib Efreet does 1 damage to you during your upkeep.').
card_first_print('serendib efreet', 'ARN').
card_image_name('serendib efreet'/'ARN', 'serendib efreet').
card_uid('serendib efreet'/'ARN', 'ARN:Serendib Efreet:serendib efreet').
card_rarity('serendib efreet'/'ARN', 'Rare').
card_artist('serendib efreet'/'ARN', 'Anson Maddocks').
card_multiverse_id('serendib efreet'/'ARN', '939').

card_in_set('shahrazad', 'ARN').
card_original_type('shahrazad'/'ARN', 'Sorcery').
card_original_text('shahrazad'/'ARN', 'Players must leave game in progress as it is and use the cards left in their libraries as decks with which to play a subgame of Magic. When subgame is over, players shuffle these cards, return them to libraries, and resume game in progress, with any loser of subgame halving his or her remaining life points, rounding down. Effects that prevent damage may not be used to counter this loss of life. The subgame has no ante; using less than forty cards may be necessary.').
card_first_print('shahrazad', 'ARN').
card_image_name('shahrazad'/'ARN', 'shahrazad').
card_uid('shahrazad'/'ARN', 'ARN:Shahrazad:shahrazad').
card_rarity('shahrazad'/'ARN', 'Rare').
card_artist('shahrazad'/'ARN', 'Kaja Foglio').
card_multiverse_id('shahrazad'/'ARN', '980').

card_in_set('sindbad', 'ARN').
card_original_type('sindbad'/'ARN', 'Summon — Sindbad').
card_original_text('sindbad'/'ARN', 'Tap to draw a card from your library, but discard that card if it is not a land.').
card_first_print('sindbad', 'ARN').
card_image_name('sindbad'/'ARN', 'sindbad').
card_uid('sindbad'/'ARN', 'ARN:Sindbad:sindbad').
card_rarity('sindbad'/'ARN', 'Uncommon').
card_artist('sindbad'/'ARN', 'Julie Baroh').
card_multiverse_id('sindbad'/'ARN', '940').

card_in_set('singing tree', 'ARN').
card_original_type('singing tree'/'ARN', 'Summon — Singing Tree').
card_original_text('singing tree'/'ARN', 'Tap to reduce an attacking creature\'s power to 0.').
card_first_print('singing tree', 'ARN').
card_image_name('singing tree'/'ARN', 'singing tree').
card_uid('singing tree'/'ARN', 'ARN:Singing Tree:singing tree').
card_rarity('singing tree'/'ARN', 'Rare').
card_artist('singing tree'/'ARN', 'Rob Alexander').
card_multiverse_id('singing tree'/'ARN', '952').

card_in_set('sorceress queen', 'ARN').
card_original_type('sorceress queen'/'ARN', 'Summon — Sorceress').
card_original_text('sorceress queen'/'ARN', 'Tap to make another creature 0/2 until end of turn. Treat this exactly as if the numbers in the lower right of the target card were 0/2. All special characteristics and enchantments on the creature are unaffected.').
card_first_print('sorceress queen', 'ARN').
card_image_name('sorceress queen'/'ARN', 'sorceress queen').
card_uid('sorceress queen'/'ARN', 'ARN:Sorceress Queen:sorceress queen').
card_rarity('sorceress queen'/'ARN', 'Uncommon').
card_artist('sorceress queen'/'ARN', 'Kaja Foglio').
card_multiverse_id('sorceress queen'/'ARN', '926').

card_in_set('stone-throwing devils', 'ARN').
card_original_type('stone-throwing devils'/'ARN', 'Summon — Devils').
card_original_text('stone-throwing devils'/'ARN', 'First strike').
card_first_print('stone-throwing devils', 'ARN').
card_image_name('stone-throwing devils'/'ARN', 'stone-throwing devils1').
card_uid('stone-throwing devils'/'ARN', 'ARN:Stone-Throwing Devils:stone-throwing devils1').
card_rarity('stone-throwing devils'/'ARN', 'Common').
card_artist('stone-throwing devils'/'ARN', 'Ken Meyer, Jr.').
card_flavor_text('stone-throwing devils'/'ARN', 'Sometimes those with the most sin cast the first stones.').
card_multiverse_id('stone-throwing devils'/'ARN', '927').

card_in_set('stone-throwing devils', 'ARN').
card_original_type('stone-throwing devils'/'ARN', 'Summon — Devils').
card_original_text('stone-throwing devils'/'ARN', 'First strike').
card_image_name('stone-throwing devils'/'ARN', 'stone-throwing devils2').
card_uid('stone-throwing devils'/'ARN', 'ARN:Stone-Throwing Devils:stone-throwing devils2').
card_rarity('stone-throwing devils'/'ARN', 'Common').
card_artist('stone-throwing devils'/'ARN', 'Ken Meyer, Jr.').
card_flavor_text('stone-throwing devils'/'ARN', 'Sometimes those with the most sin cast the first stones.').
card_multiverse_id('stone-throwing devils'/'ARN', '928').

card_in_set('unstable mutation', 'ARN').
card_original_type('unstable mutation'/'ARN', 'Enchant Creature').
card_original_text('unstable mutation'/'ARN', 'Target creature gains +3/+3. Each round, put a -1/-1 counter on the creature during its controller\'s upkeep. These counters remain even if this enchantment is removed before the creature dies.').
card_first_print('unstable mutation', 'ARN').
card_image_name('unstable mutation'/'ARN', 'unstable mutation').
card_uid('unstable mutation'/'ARN', 'ARN:Unstable Mutation:unstable mutation').
card_rarity('unstable mutation'/'ARN', 'Common').
card_artist('unstable mutation'/'ARN', 'Douglas Shuler').
card_multiverse_id('unstable mutation'/'ARN', '941').

card_in_set('war elephant', 'ARN').
card_original_type('war elephant'/'ARN', 'Summon — Elephant').
card_original_text('war elephant'/'ARN', 'Trample, bands').
card_first_print('war elephant', 'ARN').
card_image_name('war elephant'/'ARN', 'war elephant1').
card_uid('war elephant'/'ARN', 'ARN:War Elephant:war elephant1').
card_rarity('war elephant'/'ARN', 'Common').
card_artist('war elephant'/'ARN', 'Kristen Bishop').
card_flavor_text('war elephant'/'ARN', '\"When elephants fight it is the grass that suffers.\" —Kikuyu Proverb').
card_multiverse_id('war elephant'/'ARN', '981').

card_in_set('war elephant', 'ARN').
card_original_type('war elephant'/'ARN', 'Summon — Elephant').
card_original_text('war elephant'/'ARN', 'Trample, bands').
card_image_name('war elephant'/'ARN', 'war elephant2').
card_uid('war elephant'/'ARN', 'ARN:War Elephant:war elephant2').
card_rarity('war elephant'/'ARN', 'Common').
card_artist('war elephant'/'ARN', 'Kristen Bishop').
card_flavor_text('war elephant'/'ARN', '\"When elephants fight it is the grass that suffers.\" —Kikuyu Proverb').
card_multiverse_id('war elephant'/'ARN', '982').

card_in_set('wyluli wolf', 'ARN').
card_original_type('wyluli wolf'/'ARN', 'Summon — Wolf').
card_original_text('wyluli wolf'/'ARN', 'Tap to give any creature in play +1/+1 until end of turn.').
card_first_print('wyluli wolf', 'ARN').
card_image_name('wyluli wolf'/'ARN', 'wyluli wolf1').
card_uid('wyluli wolf'/'ARN', 'ARN:Wyluli Wolf:wyluli wolf1').
card_rarity('wyluli wolf'/'ARN', 'Common').
card_artist('wyluli wolf'/'ARN', 'Susan Van Camp').
card_flavor_text('wyluli wolf'/'ARN', '\"When one wolf calls, others follow. Who wants to fight creatures that eat scorpions?\" —Maimun al-Wyluli, Diary').
card_multiverse_id('wyluli wolf'/'ARN', '953').

card_in_set('wyluli wolf', 'ARN').
card_original_type('wyluli wolf'/'ARN', 'Summon — Wolf').
card_original_text('wyluli wolf'/'ARN', 'Tap to give any creature in play +1/+1 until end of turn.').
card_image_name('wyluli wolf'/'ARN', 'wyluli wolf2').
card_uid('wyluli wolf'/'ARN', 'ARN:Wyluli Wolf:wyluli wolf2').
card_rarity('wyluli wolf'/'ARN', 'Common').
card_artist('wyluli wolf'/'ARN', 'Susan Van Camp').
card_flavor_text('wyluli wolf'/'ARN', '\"When one wolf calls, others follow. Who wants to fight creatures that eat scorpions?\" —Maimun al-Wyluli, Diary').
card_multiverse_id('wyluli wolf'/'ARN', '954').

card_in_set('ydwen efreet', 'ARN').
card_original_type('ydwen efreet'/'ARN', 'Summon — Efreet').
card_original_text('ydwen efreet'/'ARN', 'If you choose to block with Ydwen Efreet, flip a coin immediately after defense is announced; opponent calls heads or tails while coin is in the air. If the flip ends up in opponent\'s favor, Ydwen Efreet cannot block this turn.').
card_first_print('ydwen efreet', 'ARN').
card_image_name('ydwen efreet'/'ARN', 'ydwen efreet').
card_uid('ydwen efreet'/'ARN', 'ARN:Ydwen Efreet:ydwen efreet').
card_rarity('ydwen efreet'/'ARN', 'Rare').
card_artist('ydwen efreet'/'ARN', 'Drew Tucker').
card_multiverse_id('ydwen efreet'/'ARN', '967').
