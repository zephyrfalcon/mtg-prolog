% Saviors of Kamigawa

set('SOK').
set_name('SOK', 'Saviors of Kamigawa').
set_release_date('SOK', '2005-06-03').
set_border('SOK', 'black').
set_type('SOK', 'expansion').
set_block('SOK', 'Kamigawa').

card_in_set('adamaro, first to desire', 'SOK').
card_original_type('adamaro, first to desire'/'SOK', 'Legendary Creature — Spirit').
card_original_text('adamaro, first to desire'/'SOK', 'Adamaro, First to Desire\'s power and toughness are each equal to the number of cards in the hand of the opponent with the most cards in hand.').
card_first_print('adamaro, first to desire', 'SOK').
card_image_name('adamaro, first to desire'/'SOK', 'adamaro, first to desire').
card_uid('adamaro, first to desire'/'SOK', 'SOK:Adamaro, First to Desire:adamaro, first to desire').
card_rarity('adamaro, first to desire'/'SOK', 'Rare').
card_artist('adamaro, first to desire'/'SOK', 'Paolo Parente').
card_number('adamaro, first to desire'/'SOK', '91').
card_flavor_text('adamaro, first to desire'/'SOK', '\"Beware Adamaro! In him all pain and anger is perfected.\"\n—Isao, Enlightened Bushi').
card_multiverse_id('adamaro, first to desire'/'SOK', '74187').

card_in_set('æther shockwave', 'SOK').
card_original_type('æther shockwave'/'SOK', 'Instant').
card_original_text('æther shockwave'/'SOK', 'Choose one Tap all Spirits; or tap all non-Spirit creatures.').
card_first_print('æther shockwave', 'SOK').
card_image_name('æther shockwave'/'SOK', 'aether shockwave').
card_uid('æther shockwave'/'SOK', 'SOK:Æther Shockwave:aether shockwave').
card_rarity('æther shockwave'/'SOK', 'Uncommon').
card_artist('æther shockwave'/'SOK', 'Stephen Tappin').
card_number('æther shockwave'/'SOK', '1').
card_flavor_text('æther shockwave'/'SOK', '\"The strain upon the veil between worlds began to show near the end of the Kami War. Strange happenings that neither side could control lashed out across Kamigawa.\"\n—Observations of the Kami War').
card_multiverse_id('æther shockwave'/'SOK', '74208').

card_in_set('akki drillmaster', 'SOK').
card_original_type('akki drillmaster'/'SOK', 'Creature — Goblin Shaman').
card_original_text('akki drillmaster'/'SOK', '{T}: Target creature gains haste until end of turn.').
card_first_print('akki drillmaster', 'SOK').
card_image_name('akki drillmaster'/'SOK', 'akki drillmaster').
card_uid('akki drillmaster'/'SOK', 'SOK:Akki Drillmaster:akki drillmaster').
card_rarity('akki drillmaster'/'SOK', 'Common').
card_artist('akki drillmaster'/'SOK', 'Alan Pollack').
card_number('akki drillmaster'/'SOK', '92').
card_flavor_text('akki drillmaster'/'SOK', '\"What part of ‘hayaku ikee!\' did you not understand?\"').
card_multiverse_id('akki drillmaster'/'SOK', '74393').

card_in_set('akki underling', 'SOK').
card_original_type('akki underling'/'SOK', 'Creature — Goblin Warrior').
card_original_text('akki underling'/'SOK', 'As long as you have seven or more cards in hand, Akki Underling gets +2/+1 and has first strike.').
card_first_print('akki underling', 'SOK').
card_image_name('akki underling'/'SOK', 'akki underling').
card_uid('akki underling'/'SOK', 'SOK:Akki Underling:akki underling').
card_rarity('akki underling'/'SOK', 'Common').
card_artist('akki underling'/'SOK', 'Franz Vohwinkel').
card_number('akki underling'/'SOK', '93').
card_flavor_text('akki underling'/'SOK', '\"I lost my hounds in the war and thus found myself employing akki as servants. I found their training to be remarkably similar.\"\n—Kensuke, houndmaster').
card_multiverse_id('akki underling'/'SOK', '79164').

card_in_set('akuta, born of ash', 'SOK').
card_original_type('akuta, born of ash'/'SOK', 'Legendary Creature — Spirit').
card_original_text('akuta, born of ash'/'SOK', 'Haste\nAt the beginning of your upkeep, if you have more cards in hand than each opponent, you may sacrifice a Swamp. If you do, return Akuta, Born of Ash from your graveyard to play.').
card_first_print('akuta, born of ash', 'SOK').
card_image_name('akuta, born of ash'/'SOK', 'akuta, born of ash').
card_uid('akuta, born of ash'/'SOK', 'SOK:Akuta, Born of Ash:akuta, born of ash').
card_rarity('akuta, born of ash'/'SOK', 'Rare').
card_artist('akuta, born of ash'/'SOK', 'Ben Thompson').
card_number('akuta, born of ash'/'SOK', '61').
card_multiverse_id('akuta, born of ash'/'SOK', '74148').

card_in_set('araba mothrider', 'SOK').
card_original_type('araba mothrider'/'SOK', 'Creature — Human Samurai').
card_original_text('araba mothrider'/'SOK', 'Flying\nBushido 1 (When this blocks or becomes blocked, it gets +1/+1 until end of turn.)').
card_first_print('araba mothrider', 'SOK').
card_image_name('araba mothrider'/'SOK', 'araba mothrider').
card_uid('araba mothrider'/'SOK', 'SOK:Araba Mothrider:araba mothrider').
card_rarity('araba mothrider'/'SOK', 'Common').
card_artist('araba mothrider'/'SOK', 'Anthony S. Waters').
card_number('araba mothrider'/'SOK', '2').
card_flavor_text('araba mothrider'/'SOK', '\"My finest creations are fashioned after the moths of Eiganjo. They fly higher, faster, and more gracefully than any of my own designs.\"\n—Noboru, master kitemaker').
card_multiverse_id('araba mothrider'/'SOK', '74050').

card_in_set('arashi, the sky asunder', 'SOK').
card_original_type('arashi, the sky asunder'/'SOK', 'Legendary Creature — Spirit').
card_original_text('arashi, the sky asunder'/'SOK', '{X}{G}, {T}: Arashi, the Sky Asunder deals X damage to target creature with flying.\nChannel {X}{G}{G}, Discard Arashi: Arashi deals X damage to each creature with flying.').
card_first_print('arashi, the sky asunder', 'SOK').
card_image_name('arashi, the sky asunder'/'SOK', 'arashi, the sky asunder').
card_uid('arashi, the sky asunder'/'SOK', 'SOK:Arashi, the Sky Asunder:arashi, the sky asunder').
card_rarity('arashi, the sky asunder'/'SOK', 'Rare').
card_artist('arashi, the sky asunder'/'SOK', 'Kev Walker').
card_number('arashi, the sky asunder'/'SOK', '121').
card_multiverse_id('arashi, the sky asunder'/'SOK', '87333').

card_in_set('ashes of the fallen', 'SOK').
card_original_type('ashes of the fallen'/'SOK', 'Artifact').
card_original_text('ashes of the fallen'/'SOK', 'As Ashes of the Fallen comes into play, choose a creature type.\nEach creature card in your graveyard has the chosen creature type in addition to its other types.').
card_first_print('ashes of the fallen', 'SOK').
card_image_name('ashes of the fallen'/'SOK', 'ashes of the fallen').
card_uid('ashes of the fallen'/'SOK', 'SOK:Ashes of the Fallen:ashes of the fallen').
card_rarity('ashes of the fallen'/'SOK', 'Rare').
card_artist('ashes of the fallen'/'SOK', 'Dan Frazier').
card_number('ashes of the fallen'/'SOK', '152').
card_flavor_text('ashes of the fallen'/'SOK', 'The ashes of the dead mingle with the soil, and both become as one.').
card_multiverse_id('ashes of the fallen'/'SOK', '87334').

card_in_set('ayumi, the last visitor', 'SOK').
card_original_type('ayumi, the last visitor'/'SOK', 'Legendary Creature — Spirit').
card_original_text('ayumi, the last visitor'/'SOK', 'Legendary landwalk').
card_first_print('ayumi, the last visitor', 'SOK').
card_image_name('ayumi, the last visitor'/'SOK', 'ayumi, the last visitor').
card_uid('ayumi, the last visitor'/'SOK', 'SOK:Ayumi, the Last Visitor:ayumi, the last visitor').
card_rarity('ayumi, the last visitor'/'SOK', 'Rare').
card_artist('ayumi, the last visitor'/'SOK', 'rk post').
card_number('ayumi, the last visitor'/'SOK', '122').
card_flavor_text('ayumi, the last visitor'/'SOK', 'Before Shinka\'s walls were built, before the torii at Okina stood, she was there, tall and proud against the sky, and she will be there long after they have fallen into dust.').
card_multiverse_id('ayumi, the last visitor'/'SOK', '84370').

card_in_set('barrel down sokenzan', 'SOK').
card_original_type('barrel down sokenzan'/'SOK', 'Instant — Arcane').
card_original_text('barrel down sokenzan'/'SOK', 'Sweep Return any number of Mountains you control to their owner\'s hand. Barrel Down Sokenzan deals damage to target creature equal to twice the number of Mountains returned this way.').
card_first_print('barrel down sokenzan', 'SOK').
card_image_name('barrel down sokenzan'/'SOK', 'barrel down sokenzan').
card_uid('barrel down sokenzan'/'SOK', 'SOK:Barrel Down Sokenzan:barrel down sokenzan').
card_rarity('barrel down sokenzan'/'SOK', 'Common').
card_artist('barrel down sokenzan'/'SOK', 'Greg Staples').
card_number('barrel down sokenzan'/'SOK', '94').
card_multiverse_id('barrel down sokenzan'/'SOK', '74201').

card_in_set('blood clock', 'SOK').
card_original_type('blood clock'/'SOK', 'Artifact').
card_original_text('blood clock'/'SOK', 'At the beginning of each player\'s upkeep, that player returns a permanent he or she controls to its owner\'s hand unless he or she pays 2 life.').
card_first_print('blood clock', 'SOK').
card_image_name('blood clock'/'SOK', 'blood clock').
card_uid('blood clock'/'SOK', 'SOK:Blood Clock:blood clock').
card_rarity('blood clock'/'SOK', 'Rare').
card_artist('blood clock'/'SOK', 'Keith Garletts').
card_number('blood clock'/'SOK', '153').
card_flavor_text('blood clock'/'SOK', 'In an age of war, time is measured not by sand but by blood.').
card_multiverse_id('blood clock'/'SOK', '84709').

card_in_set('bounteous kirin', 'SOK').
card_original_type('bounteous kirin'/'SOK', 'Legendary Creature — Kirin Spirit').
card_original_text('bounteous kirin'/'SOK', 'Flying\nWhenever you play a Spirit or Arcane spell, you may gain life equal to that spell\'s converted mana cost.').
card_first_print('bounteous kirin', 'SOK').
card_image_name('bounteous kirin'/'SOK', 'bounteous kirin').
card_uid('bounteous kirin'/'SOK', 'SOK:Bounteous Kirin:bounteous kirin').
card_rarity('bounteous kirin'/'SOK', 'Rare').
card_artist('bounteous kirin'/'SOK', 'Shishizaru').
card_number('bounteous kirin'/'SOK', '123').
card_flavor_text('bounteous kirin'/'SOK', 'Even the most benevolent and life-giving of kami joined the war against the mortal world, but only with heavy and sorrowful hearts.').
card_multiverse_id('bounteous kirin'/'SOK', '78585').

card_in_set('briarknit kami', 'SOK').
card_original_type('briarknit kami'/'SOK', 'Creature — Spirit').
card_original_text('briarknit kami'/'SOK', 'Whenever you play a Spirit or Arcane spell, put a +1/+1 counter on target creature.').
card_first_print('briarknit kami', 'SOK').
card_image_name('briarknit kami'/'SOK', 'briarknit kami').
card_uid('briarknit kami'/'SOK', 'SOK:Briarknit Kami:briarknit kami').
card_rarity('briarknit kami'/'SOK', 'Uncommon').
card_artist('briarknit kami'/'SOK', 'Brian Despain').
card_number('briarknit kami'/'SOK', '124').
card_flavor_text('briarknit kami'/'SOK', 'One thorn to warn, ten thorns to defend, a hundred thorns to destroy.').
card_multiverse_id('briarknit kami'/'SOK', '74163').

card_in_set('burning-eye zubera', 'SOK').
card_original_type('burning-eye zubera'/'SOK', 'Creature — Zubera Spirit').
card_original_text('burning-eye zubera'/'SOK', 'When Burning-Eye Zubera is put into a graveyard from play, if 4 or more damage was dealt to it this turn, Burning-Eye Zubera deals 3 damage to target creature or player.').
card_first_print('burning-eye zubera', 'SOK').
card_image_name('burning-eye zubera'/'SOK', 'burning-eye zubera').
card_uid('burning-eye zubera'/'SOK', 'SOK:Burning-Eye Zubera:burning-eye zubera').
card_rarity('burning-eye zubera'/'SOK', 'Uncommon').
card_artist('burning-eye zubera'/'SOK', 'Anthony S. Waters').
card_number('burning-eye zubera'/'SOK', '95').
card_multiverse_id('burning-eye zubera'/'SOK', '50234').

card_in_set('captive flame', 'SOK').
card_original_type('captive flame'/'SOK', 'Enchantment').
card_original_text('captive flame'/'SOK', '{R}: Target creature gets +1/+0 until end of turn.').
card_first_print('captive flame', 'SOK').
card_image_name('captive flame'/'SOK', 'captive flame').
card_uid('captive flame'/'SOK', 'SOK:Captive Flame:captive flame').
card_rarity('captive flame'/'SOK', 'Uncommon').
card_artist('captive flame'/'SOK', 'Keith Garletts').
card_number('captive flame'/'SOK', '96').
card_flavor_text('captive flame'/'SOK', '\"Kills and cooks your food in one easy stroke.\"').
card_multiverse_id('captive flame'/'SOK', '88810').

card_in_set('celestial kirin', 'SOK').
card_original_type('celestial kirin'/'SOK', 'Legendary Creature — Kirin Spirit').
card_original_text('celestial kirin'/'SOK', 'Flying\nWhenever you play a Spirit or Arcane spell, destroy all permanents with that spell\'s converted mana cost.').
card_first_print('celestial kirin', 'SOK').
card_image_name('celestial kirin'/'SOK', 'celestial kirin').
card_uid('celestial kirin'/'SOK', 'SOK:Celestial Kirin:celestial kirin').
card_rarity('celestial kirin'/'SOK', 'Rare').
card_artist('celestial kirin'/'SOK', 'Adam Rex').
card_number('celestial kirin'/'SOK', '3').
card_flavor_text('celestial kirin'/'SOK', 'All bow at its visitation—some in awe, some in honor, some in fear.').
card_multiverse_id('celestial kirin'/'SOK', '84380').

card_in_set('charge across the araba', 'SOK').
card_original_type('charge across the araba'/'SOK', 'Instant — Arcane').
card_original_text('charge across the araba'/'SOK', 'Sweep Return any number of Plains you control to their owner\'s hand. Creatures you control get +1/+1 until end of turn for each Plains returned this way.').
card_first_print('charge across the araba', 'SOK').
card_image_name('charge across the araba'/'SOK', 'charge across the araba').
card_uid('charge across the araba'/'SOK', 'SOK:Charge Across the Araba:charge across the araba').
card_rarity('charge across the araba'/'SOK', 'Uncommon').
card_artist('charge across the araba'/'SOK', 'Dany Orizio').
card_number('charge across the araba'/'SOK', '4').
card_multiverse_id('charge across the araba'/'SOK', '74186').

card_in_set('choice of damnations', 'SOK').
card_original_type('choice of damnations'/'SOK', 'Sorcery — Arcane').
card_original_text('choice of damnations'/'SOK', 'Target opponent chooses a number. You may have that player lose that much life. If you don\'t, that player sacrifices all but that many permanents.').
card_first_print('choice of damnations', 'SOK').
card_image_name('choice of damnations'/'SOK', 'choice of damnations').
card_uid('choice of damnations'/'SOK', 'SOK:Choice of Damnations:choice of damnations').
card_rarity('choice of damnations'/'SOK', 'Rare').
card_artist('choice of damnations'/'SOK', 'Tim Hildebrandt').
card_number('choice of damnations'/'SOK', '62').
card_flavor_text('choice of damnations'/'SOK', '\"Life is a series of choices between bad and worse.\"\n—Toshiro Umezawa').
card_multiverse_id('choice of damnations'/'SOK', '88803').

card_in_set('cloudhoof kirin', 'SOK').
card_original_type('cloudhoof kirin'/'SOK', 'Legendary Creature — Kirin Spirit').
card_original_text('cloudhoof kirin'/'SOK', 'Flying\nWhenever you play a Spirit or Arcane spell, you may put the top X cards of target player\'s library into his or her graveyard, where X is that spell\'s converted mana cost.').
card_first_print('cloudhoof kirin', 'SOK').
card_image_name('cloudhoof kirin'/'SOK', 'cloudhoof kirin').
card_uid('cloudhoof kirin'/'SOK', 'SOK:Cloudhoof Kirin:cloudhoof kirin').
card_rarity('cloudhoof kirin'/'SOK', 'Rare').
card_artist('cloudhoof kirin'/'SOK', 'Randy Gallegos').
card_number('cloudhoof kirin'/'SOK', '31').
card_multiverse_id('cloudhoof kirin'/'SOK', '89399').

card_in_set('cowed by wisdom', 'SOK').
card_original_type('cowed by wisdom'/'SOK', 'Enchant Creature').
card_original_text('cowed by wisdom'/'SOK', 'Enchanted creature can\'t attack or block unless its controller pays {1} for each card in your hand. (This cost is paid as attackers or blockers are declared.)').
card_first_print('cowed by wisdom', 'SOK').
card_image_name('cowed by wisdom'/'SOK', 'cowed by wisdom').
card_uid('cowed by wisdom'/'SOK', 'SOK:Cowed by Wisdom:cowed by wisdom').
card_rarity('cowed by wisdom'/'SOK', 'Common').
card_artist('cowed by wisdom'/'SOK', 'Daren Bader').
card_number('cowed by wisdom'/'SOK', '5').
card_multiverse_id('cowed by wisdom'/'SOK', '74132').

card_in_set('curtain of light', 'SOK').
card_original_type('curtain of light'/'SOK', 'Instant').
card_original_text('curtain of light'/'SOK', 'Target attacking unblocked creature becomes blocked.\nDraw a card.').
card_first_print('curtain of light', 'SOK').
card_image_name('curtain of light'/'SOK', 'curtain of light').
card_uid('curtain of light'/'SOK', 'SOK:Curtain of Light:curtain of light').
card_rarity('curtain of light'/'SOK', 'Common').
card_artist('curtain of light'/'SOK', 'Chippy').
card_number('curtain of light'/'SOK', '6').
card_flavor_text('curtain of light'/'SOK', '\"Paint gold upon the wind,\nShape the wind into a shield,\nAnd vanish behind the brightness.\"\n—Snow-Fur, kitsune poet').
card_multiverse_id('curtain of light'/'SOK', '87329').

card_in_set('cut the earthly bond', 'SOK').
card_original_type('cut the earthly bond'/'SOK', 'Instant — Arcane').
card_original_text('cut the earthly bond'/'SOK', 'Return target enchanted permanent to its owner\'s hand.').
card_first_print('cut the earthly bond', 'SOK').
card_image_name('cut the earthly bond'/'SOK', 'cut the earthly bond').
card_uid('cut the earthly bond'/'SOK', 'SOK:Cut the Earthly Bond:cut the earthly bond').
card_rarity('cut the earthly bond'/'SOK', 'Common').
card_artist('cut the earthly bond'/'SOK', 'Jeff Miracola').
card_number('cut the earthly bond'/'SOK', '32').
card_flavor_text('cut the earthly bond'/'SOK', '\"The more kami that passed through the veil that separated them from our world, the weaker that veil became, threatening to tear into tatters at the slightest pressure.\"\n—The History of Kamigawa').
card_multiverse_id('cut the earthly bond'/'SOK', '88786').

card_in_set('death denied', 'SOK').
card_original_type('death denied'/'SOK', 'Instant — Arcane').
card_original_text('death denied'/'SOK', 'Return X target creature cards from your graveyard to your hand.').
card_first_print('death denied', 'SOK').
card_image_name('death denied'/'SOK', 'death denied').
card_uid('death denied'/'SOK', 'SOK:Death Denied:death denied').
card_rarity('death denied'/'SOK', 'Common').
card_artist('death denied'/'SOK', 'Greg Hildebrandt').
card_number('death denied'/'SOK', '63').
card_flavor_text('death denied'/'SOK', 'Takenuma was filled with a chorus of moans, shrieks, and wails. Some came from the living, some from the dying, and some, most horribly, from the dead.').
card_multiverse_id('death denied'/'SOK', '87337').

card_in_set('death of a thousand stings', 'SOK').
card_original_type('death of a thousand stings'/'SOK', 'Instant — Arcane').
card_original_text('death of a thousand stings'/'SOK', 'Target player loses 1 life and you gain 1 life.\nAt the beginning of your upkeep, if you have more cards in hand than each opponent, you may return Death of a Thousand Stings from your graveyard to your hand.').
card_first_print('death of a thousand stings', 'SOK').
card_image_name('death of a thousand stings'/'SOK', 'death of a thousand stings').
card_uid('death of a thousand stings'/'SOK', 'SOK:Death of a Thousand Stings:death of a thousand stings').
card_rarity('death of a thousand stings'/'SOK', 'Common').
card_artist('death of a thousand stings'/'SOK', 'Scott M. Fischer').
card_number('death of a thousand stings'/'SOK', '64').
card_multiverse_id('death of a thousand stings'/'SOK', '74202').

card_in_set('deathknell kami', 'SOK').
card_original_type('deathknell kami'/'SOK', 'Creature — Spirit').
card_original_text('deathknell kami'/'SOK', 'Flying\n{2}: Deathknell Kami gets +1/+1 until end of turn. Sacrifice it at end of turn.\nSoulshift 1 (When this is put into a graveyard from play, you may return target Spirit card with converted mana cost 1 or less from your graveyard to your hand.)').
card_first_print('deathknell kami', 'SOK').
card_image_name('deathknell kami'/'SOK', 'deathknell kami').
card_uid('deathknell kami'/'SOK', 'SOK:Deathknell Kami:deathknell kami').
card_rarity('deathknell kami'/'SOK', 'Common').
card_artist('deathknell kami'/'SOK', 'Puddnhead').
card_number('deathknell kami'/'SOK', '65').
card_multiverse_id('deathknell kami'/'SOK', '74003').

card_in_set('deathmask nezumi', 'SOK').
card_original_type('deathmask nezumi'/'SOK', 'Creature — Rat Shaman').
card_original_text('deathmask nezumi'/'SOK', 'As long as you have seven or more cards in hand, Deathmask Nezumi gets +2/+1 and has fear.').
card_first_print('deathmask nezumi', 'SOK').
card_image_name('deathmask nezumi'/'SOK', 'deathmask nezumi').
card_uid('deathmask nezumi'/'SOK', 'SOK:Deathmask Nezumi:deathmask nezumi').
card_rarity('deathmask nezumi'/'SOK', 'Common').
card_artist('deathmask nezumi'/'SOK', 'Daren Bader').
card_number('deathmask nezumi'/'SOK', '66').
card_flavor_text('deathmask nezumi'/'SOK', '\"We had him cornered, we did . . . and, well, you know what they say about rats.\"\n—Araba patrol survivor').
card_multiverse_id('deathmask nezumi'/'SOK', '84703').

card_in_set('dense canopy', 'SOK').
card_original_type('dense canopy'/'SOK', 'Enchantment').
card_original_text('dense canopy'/'SOK', 'Creatures with flying can\'t block creatures without flying.').
card_first_print('dense canopy', 'SOK').
card_image_name('dense canopy'/'SOK', 'dense canopy').
card_uid('dense canopy'/'SOK', 'SOK:Dense Canopy:dense canopy').
card_rarity('dense canopy'/'SOK', 'Uncommon').
card_artist('dense canopy'/'SOK', 'Luca Zontini').
card_number('dense canopy'/'SOK', '125').
card_flavor_text('dense canopy'/'SOK', 'The orochi learned how to move swiftly and surely along the forest floor, like fish darting through gull-watched waters.').
card_multiverse_id('dense canopy'/'SOK', '88816').

card_in_set('descendant of kiyomaro', 'SOK').
card_original_type('descendant of kiyomaro'/'SOK', 'Creature — Human Soldier').
card_original_text('descendant of kiyomaro'/'SOK', 'As long as you have more cards in hand than each opponent, Descendant of Kiyomaro gets +1/+2 and has \"Whenever this creature deals combat damage, you gain 3 life.\"').
card_first_print('descendant of kiyomaro', 'SOK').
card_image_name('descendant of kiyomaro'/'SOK', 'descendant of kiyomaro').
card_uid('descendant of kiyomaro'/'SOK', 'SOK:Descendant of Kiyomaro:descendant of kiyomaro').
card_rarity('descendant of kiyomaro'/'SOK', 'Uncommon').
card_artist('descendant of kiyomaro'/'SOK', 'Christopher Rush').
card_number('descendant of kiyomaro'/'SOK', '7').
card_flavor_text('descendant of kiyomaro'/'SOK', '\"The blood of Kiyomaro flows in my veins. He did not let Eiganjo fall and neither shall I.\"').
card_multiverse_id('descendant of kiyomaro'/'SOK', '89400').

card_in_set('descendant of masumaro', 'SOK').
card_original_type('descendant of masumaro'/'SOK', 'Creature — Human Monk').
card_original_text('descendant of masumaro'/'SOK', 'At the beginning of your upkeep, put a +1/+1 counter on Descendant of Masumaro for each card in your hand, then remove a +1/+1 counter from Descendant of Masumaro for each card in target opponent\'s hand.').
card_first_print('descendant of masumaro', 'SOK').
card_image_name('descendant of masumaro'/'SOK', 'descendant of masumaro').
card_uid('descendant of masumaro'/'SOK', 'SOK:Descendant of Masumaro:descendant of masumaro').
card_rarity('descendant of masumaro'/'SOK', 'Uncommon').
card_artist('descendant of masumaro'/'SOK', 'Rob Alexander').
card_number('descendant of masumaro'/'SOK', '126').
card_multiverse_id('descendant of masumaro'/'SOK', '74119').

card_in_set('descendant of soramaro', 'SOK').
card_original_type('descendant of soramaro'/'SOK', 'Creature — Human Wizard').
card_original_text('descendant of soramaro'/'SOK', '{1}{U}: Look at the top X cards of your library, where X is the number of cards in your hand, then put them back in any order.').
card_first_print('descendant of soramaro', 'SOK').
card_image_name('descendant of soramaro'/'SOK', 'descendant of soramaro').
card_uid('descendant of soramaro'/'SOK', 'SOK:Descendant of Soramaro:descendant of soramaro').
card_rarity('descendant of soramaro'/'SOK', 'Common').
card_artist('descendant of soramaro'/'SOK', 'Brian Despain').
card_number('descendant of soramaro'/'SOK', '33').
card_flavor_text('descendant of soramaro'/'SOK', '\"Soramaro speaks through me. Listen! And I will tell the wisdom of the ancestors.\"').
card_multiverse_id('descendant of soramaro'/'SOK', '84648').

card_in_set('dosan\'s oldest chant', 'SOK').
card_original_type('dosan\'s oldest chant'/'SOK', 'Sorcery').
card_original_text('dosan\'s oldest chant'/'SOK', 'You gain 6 life.\nDraw a card.').
card_first_print('dosan\'s oldest chant', 'SOK').
card_image_name('dosan\'s oldest chant'/'SOK', 'dosan\'s oldest chant').
card_uid('dosan\'s oldest chant'/'SOK', 'SOK:Dosan\'s Oldest Chant:dosan\'s oldest chant').
card_rarity('dosan\'s oldest chant'/'SOK', 'Common').
card_artist('dosan\'s oldest chant'/'SOK', 'Tim Hildebrandt').
card_number('dosan\'s oldest chant'/'SOK', '127').
card_flavor_text('dosan\'s oldest chant'/'SOK', 'As Dosan\'s chant grew in volume, a second, deeper voice rose up in harmony behind it, strong enough to shake the earth and yet vibrant enough to fill the spirit.').
card_multiverse_id('dosan\'s oldest chant'/'SOK', '87332').

card_in_set('dreamcatcher', 'SOK').
card_original_type('dreamcatcher'/'SOK', 'Creature — Spirit').
card_original_text('dreamcatcher'/'SOK', 'Whenever you play a Spirit or Arcane spell, you may sacrifice Dreamcatcher. If you do, draw a card.').
card_first_print('dreamcatcher', 'SOK').
card_image_name('dreamcatcher'/'SOK', 'dreamcatcher').
card_uid('dreamcatcher'/'SOK', 'SOK:Dreamcatcher:dreamcatcher').
card_rarity('dreamcatcher'/'SOK', 'Common').
card_artist('dreamcatcher'/'SOK', 'Mark Tedin').
card_number('dreamcatcher'/'SOK', '34').
card_flavor_text('dreamcatcher'/'SOK', '\"Be careful where your mind wanders. Even in the quietest moments, the kami are listening, and your thoughts give them shape.\"\n—Lady Azami').
card_multiverse_id('dreamcatcher'/'SOK', '73988').

card_in_set('ebony owl netsuke', 'SOK').
card_original_type('ebony owl netsuke'/'SOK', 'Artifact').
card_original_text('ebony owl netsuke'/'SOK', 'At the beginning of each opponent\'s upkeep, if that player has seven or more cards in hand, Ebony Owl Netsuke deals 4 damage to him or her.').
card_first_print('ebony owl netsuke', 'SOK').
card_image_name('ebony owl netsuke'/'SOK', 'ebony owl netsuke').
card_uid('ebony owl netsuke'/'SOK', 'SOK:Ebony Owl Netsuke:ebony owl netsuke').
card_rarity('ebony owl netsuke'/'SOK', 'Uncommon').
card_artist('ebony owl netsuke'/'SOK', 'Tim Hildebrandt').
card_number('ebony owl netsuke'/'SOK', '154').
card_flavor_text('ebony owl netsuke'/'SOK', '\"Owls filled the skies during the Kami War, harbingers of darker times to come.\"\n—The History of Kamigawa').
card_multiverse_id('ebony owl netsuke'/'SOK', '88799').

card_in_set('eiganjo free-riders', 'SOK').
card_original_type('eiganjo free-riders'/'SOK', 'Creature — Human Soldier').
card_original_text('eiganjo free-riders'/'SOK', 'Flying\nAt the beginning of your upkeep, return a white creature you control to its owner\'s hand.').
card_first_print('eiganjo free-riders', 'SOK').
card_image_name('eiganjo free-riders'/'SOK', 'eiganjo free-riders').
card_uid('eiganjo free-riders'/'SOK', 'SOK:Eiganjo Free-Riders:eiganjo free-riders').
card_rarity('eiganjo free-riders'/'SOK', 'Uncommon').
card_artist('eiganjo free-riders'/'SOK', 'Kev Walker').
card_number('eiganjo free-riders'/'SOK', '8').
card_flavor_text('eiganjo free-riders'/'SOK', '\"The air filled with dust and the sound of wingbeats. The mothriders had joined the fray.\"\n—Great Battles of Kamigawa').
card_multiverse_id('eiganjo free-riders'/'SOK', '74373').

card_in_set('elder pine of jukai', 'SOK').
card_original_type('elder pine of jukai'/'SOK', 'Creature — Spirit').
card_original_text('elder pine of jukai'/'SOK', 'Whenever you play a Spirit or Arcane spell, reveal the top three cards of your library. Put all land cards revealed this way into your hand and the rest on the bottom of your library in any order.\nSoulshift 2').
card_first_print('elder pine of jukai', 'SOK').
card_image_name('elder pine of jukai'/'SOK', 'elder pine of jukai').
card_uid('elder pine of jukai'/'SOK', 'SOK:Elder Pine of Jukai:elder pine of jukai').
card_rarity('elder pine of jukai'/'SOK', 'Common').
card_artist('elder pine of jukai'/'SOK', 'Alan Pollack').
card_number('elder pine of jukai'/'SOK', '128').
card_multiverse_id('elder pine of jukai'/'SOK', '74204').

card_in_set('endless swarm', 'SOK').
card_original_type('endless swarm'/'SOK', 'Sorcery').
card_original_text('endless swarm'/'SOK', 'Put a 1/1 green Snake creature token into play for each card in your hand.\nEpic (For the rest of the game, you can\'t play spells. At the beginning of each of your upkeeps, copy this spell except for its epic ability.)').
card_first_print('endless swarm', 'SOK').
card_image_name('endless swarm'/'SOK', 'endless swarm').
card_uid('endless swarm'/'SOK', 'SOK:Endless Swarm:endless swarm').
card_rarity('endless swarm'/'SOK', 'Rare').
card_artist('endless swarm'/'SOK', 'Jeremy Jarvis').
card_number('endless swarm'/'SOK', '129').
card_multiverse_id('endless swarm'/'SOK', '87594').

card_in_set('enduring ideal', 'SOK').
card_original_type('enduring ideal'/'SOK', 'Sorcery').
card_original_text('enduring ideal'/'SOK', 'Search your library for an enchantment card and put it into play. Then shuffle your library.\nEpic (For the rest of the game, you can\'t play spells. At the beginning of each of your upkeeps, copy this spell except for its epic ability.)').
card_first_print('enduring ideal', 'SOK').
card_image_name('enduring ideal'/'SOK', 'enduring ideal').
card_uid('enduring ideal'/'SOK', 'SOK:Enduring Ideal:enduring ideal').
card_rarity('enduring ideal'/'SOK', 'Rare').
card_artist('enduring ideal'/'SOK', 'Daren Bader').
card_number('enduring ideal'/'SOK', '9').
card_multiverse_id('enduring ideal'/'SOK', '87598').

card_in_set('erayo\'s essence', 'SOK').
card_original_type('erayo\'s essence'/'SOK', 'Legendary Creature — Moonfolk Monk').
card_original_text('erayo\'s essence'/'SOK', 'Flying\nWhenever the fourth spell of a turn is played, flip Erayo, Soratami Ascendant.\n-----\nErayo\'s Essence\nLegendary Enchantment\nCounter the first spell played by each opponent each turn.').
card_first_print('erayo\'s essence', 'SOK').
card_image_name('erayo\'s essence'/'SOK', 'erayo\'s essence').
card_uid('erayo\'s essence'/'SOK', 'SOK:Erayo\'s Essence:erayo\'s essence').
card_rarity('erayo\'s essence'/'SOK', 'Rare').
card_artist('erayo\'s essence'/'SOK', 'Matt Cavotta').
card_number('erayo\'s essence'/'SOK', '35b').
card_multiverse_id('erayo\'s essence'/'SOK', '87599').

card_in_set('erayo, soratami ascendant', 'SOK').
card_original_type('erayo, soratami ascendant'/'SOK', 'Legendary Creature — Moonfolk Monk').
card_original_text('erayo, soratami ascendant'/'SOK', 'Flying\nWhenever the fourth spell of a turn is played, flip Erayo, Soratami Ascendant.\n-----\nErayo\'s Essence\nLegendary Enchantment\nCounter the first spell played by each opponent each turn.').
card_first_print('erayo, soratami ascendant', 'SOK').
card_image_name('erayo, soratami ascendant'/'SOK', 'erayo, soratami ascendant').
card_uid('erayo, soratami ascendant'/'SOK', 'SOK:Erayo, Soratami Ascendant:erayo, soratami ascendant').
card_rarity('erayo, soratami ascendant'/'SOK', 'Rare').
card_artist('erayo, soratami ascendant'/'SOK', 'Matt Cavotta').
card_number('erayo, soratami ascendant'/'SOK', '35a').
card_multiverse_id('erayo, soratami ascendant'/'SOK', '87599').

card_in_set('eternal dominion', 'SOK').
card_original_type('eternal dominion'/'SOK', 'Sorcery').
card_original_text('eternal dominion'/'SOK', 'Search target opponent\'s library for an artifact, creature, enchantment, or land card. Put that card into play under your control. Then that player shuffles his or her library.\nEpic (For the rest of the game, you can\'t play spells. At the beginning of each of your upkeeps, copy this spell except for its epic ability. You may choose a new target for the copy.)').
card_first_print('eternal dominion', 'SOK').
card_image_name('eternal dominion'/'SOK', 'eternal dominion').
card_uid('eternal dominion'/'SOK', 'SOK:Eternal Dominion:eternal dominion').
card_rarity('eternal dominion'/'SOK', 'Rare').
card_artist('eternal dominion'/'SOK', 'Shishizaru').
card_number('eternal dominion'/'SOK', '36').
card_multiverse_id('eternal dominion'/'SOK', '87593').

card_in_set('evermind', 'SOK').
card_original_type('evermind'/'SOK', 'Instant — Arcane').
card_original_text('evermind'/'SOK', '(Spells without mana costs can\'t be played.)\nDraw a card.\nEvermind is blue.\nSplice onto Arcane {1}{U} (As you play an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_first_print('evermind', 'SOK').
card_image_name('evermind'/'SOK', 'evermind').
card_uid('evermind'/'SOK', 'SOK:Evermind:evermind').
card_rarity('evermind'/'SOK', 'Uncommon').
card_artist('evermind'/'SOK', 'Matt Thompson').
card_number('evermind'/'SOK', '37').
card_multiverse_id('evermind'/'SOK', '74144').

card_in_set('exile into darkness', 'SOK').
card_original_type('exile into darkness'/'SOK', 'Sorcery').
card_original_text('exile into darkness'/'SOK', 'Target player sacrifices a creature with converted mana cost 3 or less.\nAt the beginning of your upkeep, if you have more cards in hand than each opponent, you may return Exile into Darkness from your graveyard to your hand.').
card_first_print('exile into darkness', 'SOK').
card_image_name('exile into darkness'/'SOK', 'exile into darkness').
card_uid('exile into darkness'/'SOK', 'SOK:Exile into Darkness:exile into darkness').
card_rarity('exile into darkness'/'SOK', 'Uncommon').
card_artist('exile into darkness'/'SOK', 'Pete Venters').
card_number('exile into darkness'/'SOK', '67').
card_multiverse_id('exile into darkness'/'SOK', '74399').

card_in_set('feral lightning', 'SOK').
card_original_type('feral lightning'/'SOK', 'Sorcery').
card_original_text('feral lightning'/'SOK', 'Put three 3/1 red Elemental creature tokens with haste into play. Remove them from the game at end of turn.').
card_first_print('feral lightning', 'SOK').
card_image_name('feral lightning'/'SOK', 'feral lightning').
card_uid('feral lightning'/'SOK', 'SOK:Feral Lightning:feral lightning').
card_rarity('feral lightning'/'SOK', 'Uncommon').
card_artist('feral lightning'/'SOK', 'Chippy').
card_number('feral lightning'/'SOK', '97').
card_flavor_text('feral lightning'/'SOK', 'They moved like apes, but a thousand times swifter, prying into every corner, leaving pawprints of ash.').
card_multiverse_id('feral lightning'/'SOK', '84373').

card_in_set('fiddlehead kami', 'SOK').
card_original_type('fiddlehead kami'/'SOK', 'Creature — Spirit').
card_original_text('fiddlehead kami'/'SOK', 'Whenever you play a Spirit or Arcane spell, regenerate Fiddlehead Kami.').
card_first_print('fiddlehead kami', 'SOK').
card_image_name('fiddlehead kami'/'SOK', 'fiddlehead kami').
card_uid('fiddlehead kami'/'SOK', 'SOK:Fiddlehead Kami:fiddlehead kami').
card_rarity('fiddlehead kami'/'SOK', 'Common').
card_artist('fiddlehead kami'/'SOK', 'Rebecca Guay').
card_number('fiddlehead kami'/'SOK', '130').
card_flavor_text('fiddlehead kami'/'SOK', '\"Like a riddle, it unwinds.\nLike a green wind, it reaches out.\nLike a sudden guilt, it seizes.\"\n—Snow Fur, kitsune poet').
card_multiverse_id('fiddlehead kami'/'SOK', '74166').

card_in_set('footsteps of the goryo', 'SOK').
card_original_type('footsteps of the goryo'/'SOK', 'Sorcery — Arcane').
card_original_text('footsteps of the goryo'/'SOK', 'Return target creature card from your graveyard to play. Sacrifice that creature at end of turn.').
card_first_print('footsteps of the goryo', 'SOK').
card_image_name('footsteps of the goryo'/'SOK', 'footsteps of the goryo').
card_uid('footsteps of the goryo'/'SOK', 'SOK:Footsteps of the Goryo:footsteps of the goryo').
card_rarity('footsteps of the goryo'/'SOK', 'Uncommon').
card_artist('footsteps of the goryo'/'SOK', 'Chippy').
card_number('footsteps of the goryo'/'SOK', '68').
card_flavor_text('footsteps of the goryo'/'SOK', 'In the case of violent deaths, kami offered a chance for revenge if the deceased offered up its corpse as a host to the invading kami.').
card_multiverse_id('footsteps of the goryo'/'SOK', '74400').

card_in_set('freed from the real', 'SOK').
card_original_type('freed from the real'/'SOK', 'Enchant Creature').
card_original_text('freed from the real'/'SOK', '{U}: Tap enchanted creature.\n{U}: Untap enchanted creature.').
card_first_print('freed from the real', 'SOK').
card_image_name('freed from the real'/'SOK', 'freed from the real').
card_uid('freed from the real'/'SOK', 'SOK:Freed from the Real:freed from the real').
card_rarity('freed from the real'/'SOK', 'Common').
card_artist('freed from the real'/'SOK', 'Scott M. Fischer').
card_number('freed from the real'/'SOK', '38').
card_flavor_text('freed from the real'/'SOK', 'When a strong mind moves, form and energy shift to heed it.').
card_multiverse_id('freed from the real'/'SOK', '87336').

card_in_set('gaze of adamaro', 'SOK').
card_original_type('gaze of adamaro'/'SOK', 'Instant — Arcane').
card_original_text('gaze of adamaro'/'SOK', 'Gaze of Adamaro deals damage equal to the number of cards in target player\'s hand to that player.').
card_first_print('gaze of adamaro', 'SOK').
card_image_name('gaze of adamaro'/'SOK', 'gaze of adamaro').
card_uid('gaze of adamaro'/'SOK', 'SOK:Gaze of Adamaro:gaze of adamaro').
card_rarity('gaze of adamaro'/'SOK', 'Uncommon').
card_artist('gaze of adamaro'/'SOK', 'Paolo Parente').
card_number('gaze of adamaro'/'SOK', '98').
card_flavor_text('gaze of adamaro'/'SOK', 'Wherever it looked, it saw destruction—the wake of its own gaze.').
card_multiverse_id('gaze of adamaro'/'SOK', '89401').

card_in_set('ghost-lit nourisher', 'SOK').
card_original_type('ghost-lit nourisher'/'SOK', 'Creature — Spirit').
card_original_text('ghost-lit nourisher'/'SOK', '{2}{G}, {T}: Target creature gets +2/+2 until end of turn.\nChannel {3}{G}, Discard Ghost-Lit Nourisher: Target creature gets +4/+4 until end of turn.').
card_first_print('ghost-lit nourisher', 'SOK').
card_image_name('ghost-lit nourisher'/'SOK', 'ghost-lit nourisher').
card_uid('ghost-lit nourisher'/'SOK', 'SOK:Ghost-Lit Nourisher:ghost-lit nourisher').
card_rarity('ghost-lit nourisher'/'SOK', 'Uncommon').
card_artist('ghost-lit nourisher'/'SOK', 'Tsutomu Kawade').
card_number('ghost-lit nourisher'/'SOK', '131').
card_multiverse_id('ghost-lit nourisher'/'SOK', '74068').

card_in_set('ghost-lit raider', 'SOK').
card_original_type('ghost-lit raider'/'SOK', 'Creature — Spirit').
card_original_text('ghost-lit raider'/'SOK', '{2}{R}, {T}: Ghost-Lit Raider deals 2 damage to target creature.\nChannel {3}{R}, Discard Ghost-Lit Raider: Ghost-Lit Raider deals 4 damage to target creature.').
card_image_name('ghost-lit raider'/'SOK', 'ghost-lit raider').
card_uid('ghost-lit raider'/'SOK', 'SOK:Ghost-Lit Raider:ghost-lit raider').
card_rarity('ghost-lit raider'/'SOK', 'Uncommon').
card_artist('ghost-lit raider'/'SOK', 'Ittoku').
card_number('ghost-lit raider'/'SOK', '99').
card_multiverse_id('ghost-lit raider'/'SOK', '87341').

card_in_set('ghost-lit redeemer', 'SOK').
card_original_type('ghost-lit redeemer'/'SOK', 'Creature — Spirit').
card_original_text('ghost-lit redeemer'/'SOK', '{W}, {T}: You gain 2 life.\nChannel {1}{W}, Discard Ghost-Lit Redeemer: You gain 4 life.').
card_first_print('ghost-lit redeemer', 'SOK').
card_image_name('ghost-lit redeemer'/'SOK', 'ghost-lit redeemer').
card_uid('ghost-lit redeemer'/'SOK', 'SOK:Ghost-Lit Redeemer:ghost-lit redeemer').
card_rarity('ghost-lit redeemer'/'SOK', 'Uncommon').
card_artist('ghost-lit redeemer'/'SOK', 'Shishizaru').
card_number('ghost-lit redeemer'/'SOK', '10').
card_multiverse_id('ghost-lit redeemer'/'SOK', '74082').

card_in_set('ghost-lit stalker', 'SOK').
card_original_type('ghost-lit stalker'/'SOK', 'Creature — Spirit').
card_original_text('ghost-lit stalker'/'SOK', '{4}{B}, {T}: Target player discards two cards. Play this ability only any time you could play a sorcery.\nChannel {5}{B}{B}, Discard Ghost-Lit Stalker: Target player discards four cards. Play this ability only any time you could play a sorcery.').
card_first_print('ghost-lit stalker', 'SOK').
card_image_name('ghost-lit stalker'/'SOK', 'ghost-lit stalker').
card_uid('ghost-lit stalker'/'SOK', 'SOK:Ghost-Lit Stalker:ghost-lit stalker').
card_rarity('ghost-lit stalker'/'SOK', 'Uncommon').
card_artist('ghost-lit stalker'/'SOK', 'Hideaki Takamura').
card_number('ghost-lit stalker'/'SOK', '69').
card_multiverse_id('ghost-lit stalker'/'SOK', '74401').

card_in_set('ghost-lit warder', 'SOK').
card_original_type('ghost-lit warder'/'SOK', 'Creature — Spirit').
card_original_text('ghost-lit warder'/'SOK', '{3}{U}, {T}: Counter target spell unless its controller pays {2}.\nChannel {3}{U}, Discard Ghost-Lit Warder: Counter target spell unless its controller pays {4}.').
card_first_print('ghost-lit warder', 'SOK').
card_image_name('ghost-lit warder'/'SOK', 'ghost-lit warder').
card_uid('ghost-lit warder'/'SOK', 'SOK:Ghost-Lit Warder:ghost-lit warder').
card_rarity('ghost-lit warder'/'SOK', 'Uncommon').
card_artist('ghost-lit warder'/'SOK', 'Kensuke Okabayashi').
card_number('ghost-lit warder'/'SOK', '39').
card_multiverse_id('ghost-lit warder'/'SOK', '73996').

card_in_set('glitterfang', 'SOK').
card_original_type('glitterfang'/'SOK', 'Creature — Spirit').
card_original_text('glitterfang'/'SOK', 'Haste\nAt end of turn, return Glitterfang to its owner\'s hand.').
card_first_print('glitterfang', 'SOK').
card_image_name('glitterfang'/'SOK', 'glitterfang').
card_uid('glitterfang'/'SOK', 'SOK:Glitterfang:glitterfang').
card_rarity('glitterfang'/'SOK', 'Common').
card_artist('glitterfang'/'SOK', 'Ron Spencer').
card_number('glitterfang'/'SOK', '100').
card_flavor_text('glitterfang'/'SOK', '\"Through the veil\'s rip spilled hundreds of minor kami, rushing over the battlefield like embers from a kicked campfire. Then they were gone.\"\n—Great Battles of Kamigawa').
card_multiverse_id('glitterfang'/'SOK', '89402').

card_in_set('gnat miser', 'SOK').
card_original_type('gnat miser'/'SOK', 'Creature — Rat Shaman').
card_original_text('gnat miser'/'SOK', 'Each opponent\'s maximum hand size is reduced by one.').
card_first_print('gnat miser', 'SOK').
card_image_name('gnat miser'/'SOK', 'gnat miser').
card_uid('gnat miser'/'SOK', 'SOK:Gnat Miser:gnat miser').
card_rarity('gnat miser'/'SOK', 'Common').
card_artist('gnat miser'/'SOK', 'Thomas M. Baxa').
card_number('gnat miser'/'SOK', '70').
card_flavor_text('gnat miser'/'SOK', 'Nezumi insect-shamans enjoyed solitary lives, which was fortunate since no other nezumi could stand to live within half a mile of their foul stench.').
card_multiverse_id('gnat miser'/'SOK', '88794').

card_in_set('godo\'s irregulars', 'SOK').
card_original_type('godo\'s irregulars'/'SOK', 'Creature — Human Warrior').
card_original_text('godo\'s irregulars'/'SOK', '{R}: Godo\'s Irregulars deals 1 damage to target creature blocking it.').
card_first_print('godo\'s irregulars', 'SOK').
card_image_name('godo\'s irregulars'/'SOK', 'godo\'s irregulars').
card_uid('godo\'s irregulars'/'SOK', 'SOK:Godo\'s Irregulars:godo\'s irregulars').
card_rarity('godo\'s irregulars'/'SOK', 'Uncommon').
card_artist('godo\'s irregulars'/'SOK', 'Tim Hildebrandt').
card_number('godo\'s irregulars'/'SOK', '101').
card_flavor_text('godo\'s irregulars'/'SOK', 'Godo trained their muscles and honed their skills, but he left their hearts wild and empty.').
card_multiverse_id('godo\'s irregulars'/'SOK', '74015').

card_in_set('hail of arrows', 'SOK').
card_original_type('hail of arrows'/'SOK', 'Instant').
card_original_text('hail of arrows'/'SOK', 'Hail of Arrows deals X damage divided as you choose among any number of target attacking creatures.').
card_first_print('hail of arrows', 'SOK').
card_image_name('hail of arrows'/'SOK', 'hail of arrows').
card_uid('hail of arrows'/'SOK', 'SOK:Hail of Arrows:hail of arrows').
card_rarity('hail of arrows'/'SOK', 'Uncommon').
card_artist('hail of arrows'/'SOK', 'Anthony S. Waters').
card_number('hail of arrows'/'SOK', '11').
card_flavor_text('hail of arrows'/'SOK', '\"Do not let a single shaft loose until my word. And when I give that word, do not leave a single shaft in Eiganjo.\"\n—General Takeno').
card_multiverse_id('hail of arrows'/'SOK', '74137').

card_in_set('hand of cruelty', 'SOK').
card_original_type('hand of cruelty'/'SOK', 'Creature — Human Samurai').
card_original_text('hand of cruelty'/'SOK', 'Protection from white\nBushido 1 (When this blocks or becomes blocked, it gets +1/+1 until end of turn.)').
card_first_print('hand of cruelty', 'SOK').
card_image_name('hand of cruelty'/'SOK', 'hand of cruelty').
card_uid('hand of cruelty'/'SOK', 'SOK:Hand of Cruelty:hand of cruelty').
card_rarity('hand of cruelty'/'SOK', 'Uncommon').
card_artist('hand of cruelty'/'SOK', 'Kev Walker').
card_number('hand of cruelty'/'SOK', '71').
card_flavor_text('hand of cruelty'/'SOK', '\"The sword is just a tool. It is the samurai\'s hand that delivers death.\"').
card_multiverse_id('hand of cruelty'/'SOK', '74063').

card_in_set('hand of honor', 'SOK').
card_original_type('hand of honor'/'SOK', 'Creature — Human Samurai').
card_original_text('hand of honor'/'SOK', 'Protection from black\nBushido 1 (When this blocks or becomes blocked, it gets +1/+1 until end of turn.)').
card_first_print('hand of honor', 'SOK').
card_image_name('hand of honor'/'SOK', 'hand of honor').
card_uid('hand of honor'/'SOK', 'SOK:Hand of Honor:hand of honor').
card_rarity('hand of honor'/'SOK', 'Uncommon').
card_artist('hand of honor'/'SOK', 'Kev Walker').
card_number('hand of honor'/'SOK', '12').
card_flavor_text('hand of honor'/'SOK', '\"The sword is just a tool. It is the samurai\'s hand that brings it to life.\"').
card_multiverse_id('hand of honor'/'SOK', '74173').

card_in_set('haru-onna', 'SOK').
card_original_type('haru-onna'/'SOK', 'Creature — Spirit').
card_original_text('haru-onna'/'SOK', 'When Haru-Onna comes into play, draw a card.\nWhenever you play a Spirit or Arcane spell, you may return Haru-Onna to its owner\'s hand.').
card_first_print('haru-onna', 'SOK').
card_image_name('haru-onna'/'SOK', 'haru-onna').
card_uid('haru-onna'/'SOK', 'SOK:Haru-Onna:haru-onna').
card_rarity('haru-onna'/'SOK', 'Uncommon').
card_artist('haru-onna'/'SOK', 'Rebecca Guay').
card_number('haru-onna'/'SOK', '132').
card_multiverse_id('haru-onna'/'SOK', '74062').

card_in_set('hidetsugu\'s second rite', 'SOK').
card_original_type('hidetsugu\'s second rite'/'SOK', 'Instant').
card_original_text('hidetsugu\'s second rite'/'SOK', 'If target player has exactly 10 life, Hidetsugu\'s Second Rite deals 10 damage to that player.').
card_first_print('hidetsugu\'s second rite', 'SOK').
card_image_name('hidetsugu\'s second rite'/'SOK', 'hidetsugu\'s second rite').
card_uid('hidetsugu\'s second rite'/'SOK', 'SOK:Hidetsugu\'s Second Rite:hidetsugu\'s second rite').
card_rarity('hidetsugu\'s second rite'/'SOK', 'Rare').
card_artist('hidetsugu\'s second rite'/'SOK', 'Jeff Miracola').
card_number('hidetsugu\'s second rite'/'SOK', '102').
card_flavor_text('hidetsugu\'s second rite'/'SOK', 'Hidetsugu never relinquished a grudge. He let it burn within him, gathering ever greater intensity until the final moment of vengeance.').
card_multiverse_id('hidetsugu\'s second rite'/'SOK', '88818').

card_in_set('homura\'s essence', 'SOK').
card_original_type('homura\'s essence'/'SOK', 'Legendary Creature — Human Monk').
card_original_text('homura\'s essence'/'SOK', 'Homura, Human Ascendant can\'t block.\nWhen Homura is put into a graveyard from play, return it to play flipped.\n-----\nHomura\'s Essence\nLegendary Enchantment\nCreatures you control get +2/+2 and have flying and \"{R}: This creature gets +1/+0 until end of turn.\"').
card_first_print('homura\'s essence', 'SOK').
card_image_name('homura\'s essence'/'SOK', 'homura\'s essence').
card_uid('homura\'s essence'/'SOK', 'SOK:Homura\'s Essence:homura\'s essence').
card_rarity('homura\'s essence'/'SOK', 'Rare').
card_artist('homura\'s essence'/'SOK', 'Kev Walker').
card_number('homura\'s essence'/'SOK', '103b').
card_multiverse_id('homura\'s essence'/'SOK', '84716').

card_in_set('homura, human ascendant', 'SOK').
card_original_type('homura, human ascendant'/'SOK', 'Legendary Creature — Human Monk').
card_original_text('homura, human ascendant'/'SOK', 'Homura, Human Ascendant can\'t block.\nWhen Homura is put into a graveyard from play, return it to play flipped.\n-----\nHomura\'s Essence\nLegendary Enchantment\nCreatures you control get +2/+2 and have flying and \"{R}: This creature gets +1/+0 until end of turn.\"').
card_first_print('homura, human ascendant', 'SOK').
card_image_name('homura, human ascendant'/'SOK', 'homura, human ascendant').
card_uid('homura, human ascendant'/'SOK', 'SOK:Homura, Human Ascendant:homura, human ascendant').
card_rarity('homura, human ascendant'/'SOK', 'Rare').
card_artist('homura, human ascendant'/'SOK', 'Kev Walker').
card_number('homura, human ascendant'/'SOK', '103a').
card_multiverse_id('homura, human ascendant'/'SOK', '84716').

card_in_set('ideas unbound', 'SOK').
card_original_type('ideas unbound'/'SOK', 'Sorcery — Arcane').
card_original_text('ideas unbound'/'SOK', 'Draw three cards. Discard three cards at end of turn.').
card_first_print('ideas unbound', 'SOK').
card_image_name('ideas unbound'/'SOK', 'ideas unbound').
card_uid('ideas unbound'/'SOK', 'SOK:Ideas Unbound:ideas unbound').
card_rarity('ideas unbound'/'SOK', 'Common').
card_artist('ideas unbound'/'SOK', 'Mark Tedin').
card_number('ideas unbound'/'SOK', '40').
card_flavor_text('ideas unbound'/'SOK', 'The apprentice stared in puzzlement. \"But Master, you finished writing those spells just yesterday. Don\'t you remember?\" The jushi\'s heart froze.').
card_multiverse_id('ideas unbound'/'SOK', '88789').

card_in_set('iizuka the ruthless', 'SOK').
card_original_type('iizuka the ruthless'/'SOK', 'Legendary Creature — Human Samurai').
card_original_text('iizuka the ruthless'/'SOK', 'Bushido 2 (When this blocks or becomes blocked, it gets +2/+2 until end of turn.)\n{2}{R}, Sacrifice a Samurai: Samurai you control gain double strike until end of turn.').
card_first_print('iizuka the ruthless', 'SOK').
card_image_name('iizuka the ruthless'/'SOK', 'iizuka the ruthless').
card_uid('iizuka the ruthless'/'SOK', 'SOK:Iizuka the Ruthless:iizuka the ruthless').
card_rarity('iizuka the ruthless'/'SOK', 'Rare').
card_artist('iizuka the ruthless'/'SOK', 'Darrell Riche').
card_number('iizuka the ruthless'/'SOK', '104').
card_multiverse_id('iizuka the ruthless'/'SOK', '79860').

card_in_set('iname as one', 'SOK').
card_original_type('iname as one'/'SOK', 'Legendary Creature — Spirit').
card_original_text('iname as one'/'SOK', 'When Iname as One comes into play, if you played it from your hand, you may search your library for a Spirit card, put it into play, then shuffle your library.\nWhen Iname as One is put into a graveyard from play, you may remove it from the game. If you do, return target Spirit card from your graveyard to play.').
card_first_print('iname as one', 'SOK').
card_image_name('iname as one'/'SOK', 'iname as one').
card_uid('iname as one'/'SOK', 'SOK:Iname as One:iname as one').
card_rarity('iname as one'/'SOK', 'Rare').
card_artist('iname as one'/'SOK', 'Stephen Tappin').
card_number('iname as one'/'SOK', '151').
card_multiverse_id('iname as one'/'SOK', '74618').

card_in_set('infernal kirin', 'SOK').
card_original_type('infernal kirin'/'SOK', 'Legendary Creature — Kirin Spirit').
card_original_text('infernal kirin'/'SOK', 'Flying\nWhenever you play a Spirit or Arcane spell, target player reveals his or her hand and discards all cards with that spell\'s converted mana cost.').
card_first_print('infernal kirin', 'SOK').
card_image_name('infernal kirin'/'SOK', 'infernal kirin').
card_uid('infernal kirin'/'SOK', 'SOK:Infernal Kirin:infernal kirin').
card_rarity('infernal kirin'/'SOK', 'Rare').
card_artist('infernal kirin'/'SOK', 'Carl Critchlow').
card_number('infernal kirin'/'SOK', '72').
card_flavor_text('infernal kirin'/'SOK', 'The kirin bellowed—an echoing, twisted parody of its once-inspirational call.').
card_multiverse_id('infernal kirin'/'SOK', '74377').

card_in_set('inner calm, outer strength', 'SOK').
card_original_type('inner calm, outer strength'/'SOK', 'Instant — Arcane').
card_original_text('inner calm, outer strength'/'SOK', 'Target creature gets +X/+X until end of turn, where X is the number of cards in your hand.').
card_first_print('inner calm, outer strength', 'SOK').
card_image_name('inner calm, outer strength'/'SOK', 'inner calm, outer strength').
card_uid('inner calm, outer strength'/'SOK', 'SOK:Inner Calm, Outer Strength:inner calm, outer strength').
card_rarity('inner calm, outer strength'/'SOK', 'Common').
card_artist('inner calm, outer strength'/'SOK', 'Stephen Tappin').
card_number('inner calm, outer strength'/'SOK', '133').
card_flavor_text('inner calm, outer strength'/'SOK', 'The ghostly objects around the kami circled faster and faster, creating a dizzying cyclone of motion. When it opened its eyes, it struck.').
card_multiverse_id('inner calm, outer strength'/'SOK', '74049').

card_in_set('inner fire', 'SOK').
card_original_type('inner fire'/'SOK', 'Sorcery').
card_original_text('inner fire'/'SOK', 'Add {R} to your mana pool for each card in your hand.').
card_first_print('inner fire', 'SOK').
card_image_name('inner fire'/'SOK', 'inner fire').
card_uid('inner fire'/'SOK', 'SOK:Inner Fire:inner fire').
card_rarity('inner fire'/'SOK', 'Common').
card_artist('inner fire'/'SOK', 'Christopher Moeller').
card_number('inner fire'/'SOK', '105').
card_flavor_text('inner fire'/'SOK', '\"Heart of Sokenzan, I call to you! Lend me your fire.\"').
card_multiverse_id('inner fire'/'SOK', '84711').

card_in_set('inner-chamber guard', 'SOK').
card_original_type('inner-chamber guard'/'SOK', 'Creature — Human Samurai').
card_original_text('inner-chamber guard'/'SOK', 'Bushido 2 (When this blocks or becomes blocked, it gets +2/+2 until end of turn.)').
card_first_print('inner-chamber guard', 'SOK').
card_image_name('inner-chamber guard'/'SOK', 'inner-chamber guard').
card_uid('inner-chamber guard'/'SOK', 'SOK:Inner-Chamber Guard:inner-chamber guard').
card_rarity('inner-chamber guard'/'SOK', 'Uncommon').
card_artist('inner-chamber guard'/'SOK', 'Brian Snõddy').
card_number('inner-chamber guard'/'SOK', '13').
card_flavor_text('inner-chamber guard'/'SOK', '\"It seems that each time I visit Lord Konda his guards are more on edge, perhaps even more eager for the war to reach them.\"\n—Masako the Humorless').
card_multiverse_id('inner-chamber guard'/'SOK', '74002').

card_in_set('into the fray', 'SOK').
card_original_type('into the fray'/'SOK', 'Instant — Arcane').
card_original_text('into the fray'/'SOK', 'Target creature attacks this turn if able.\nSplice onto Arcane {R} (As you play an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_first_print('into the fray', 'SOK').
card_image_name('into the fray'/'SOK', 'into the fray').
card_uid('into the fray'/'SOK', 'SOK:Into the Fray:into the fray').
card_rarity('into the fray'/'SOK', 'Common').
card_artist('into the fray'/'SOK', 'Paolo Parente').
card_number('into the fray'/'SOK', '106').
card_multiverse_id('into the fray'/'SOK', '88813').

card_in_set('ivory crane netsuke', 'SOK').
card_original_type('ivory crane netsuke'/'SOK', 'Artifact').
card_original_text('ivory crane netsuke'/'SOK', 'At the beginning of your upkeep, if you have seven or more cards in hand, you gain 4 life.').
card_first_print('ivory crane netsuke', 'SOK').
card_image_name('ivory crane netsuke'/'SOK', 'ivory crane netsuke').
card_uid('ivory crane netsuke'/'SOK', 'SOK:Ivory Crane Netsuke:ivory crane netsuke').
card_rarity('ivory crane netsuke'/'SOK', 'Uncommon').
card_artist('ivory crane netsuke'/'SOK', 'Greg Hildebrandt').
card_number('ivory crane netsuke'/'SOK', '155').
card_flavor_text('ivory crane netsuke'/'SOK', 'It is said that the feathers of a crane are clouds, plucked and sewn together by the hands of the kami.').
card_multiverse_id('ivory crane netsuke'/'SOK', '88796').

card_in_set('jiwari, the earth aflame', 'SOK').
card_original_type('jiwari, the earth aflame'/'SOK', 'Legendary Creature — Spirit').
card_original_text('jiwari, the earth aflame'/'SOK', '{X}{R}, {T}: Jiwari, the Earth Aflame deals X damage to target creature without flying.\nChannel {X}{R}{R}{R}, Discard Jiwari: Jiwari deals X damage to each creature without flying.').
card_first_print('jiwari, the earth aflame', 'SOK').
card_image_name('jiwari, the earth aflame'/'SOK', 'jiwari, the earth aflame').
card_uid('jiwari, the earth aflame'/'SOK', 'SOK:Jiwari, the Earth Aflame:jiwari, the earth aflame').
card_rarity('jiwari, the earth aflame'/'SOK', 'Rare').
card_artist('jiwari, the earth aflame'/'SOK', 'Adam Rex').
card_number('jiwari, the earth aflame'/'SOK', '107').
card_multiverse_id('jiwari, the earth aflame'/'SOK', '74047').

card_in_set('kagemaro\'s clutch', 'SOK').
card_original_type('kagemaro\'s clutch'/'SOK', 'Enchant Creature').
card_original_text('kagemaro\'s clutch'/'SOK', 'Enchanted creature gets -X/-X, where X is the number of cards in your hand.').
card_first_print('kagemaro\'s clutch', 'SOK').
card_image_name('kagemaro\'s clutch'/'SOK', 'kagemaro\'s clutch').
card_uid('kagemaro\'s clutch'/'SOK', 'SOK:Kagemaro\'s Clutch:kagemaro\'s clutch').
card_rarity('kagemaro\'s clutch'/'SOK', 'Common').
card_artist('kagemaro\'s clutch'/'SOK', 'Pat Lee').
card_number('kagemaro\'s clutch'/'SOK', '74').
card_flavor_text('kagemaro\'s clutch'/'SOK', 'The mist was thick, cloying, and constricting. Shiro fought his way to fresh air only to look back and see his comrades fallen on the battlefield behind him.').
card_multiverse_id('kagemaro\'s clutch'/'SOK', '74560').

card_in_set('kagemaro, first to suffer', 'SOK').
card_original_type('kagemaro, first to suffer'/'SOK', 'Legendary Creature — Demon Spirit').
card_original_text('kagemaro, first to suffer'/'SOK', 'Kagemaro, First to Suffer\'s power and toughness are each equal to the number of cards in your hand.\n{B}, Sacrifice Kagemaro: All creatures get -X/-X until end of turn, where X is the number of cards in your hand.').
card_first_print('kagemaro, first to suffer', 'SOK').
card_image_name('kagemaro, first to suffer'/'SOK', 'kagemaro, first to suffer').
card_uid('kagemaro, first to suffer'/'SOK', 'SOK:Kagemaro, First to Suffer:kagemaro, first to suffer').
card_rarity('kagemaro, first to suffer'/'SOK', 'Rare').
card_artist('kagemaro, first to suffer'/'SOK', 'Adam Rex').
card_number('kagemaro, first to suffer'/'SOK', '73').
card_multiverse_id('kagemaro, first to suffer'/'SOK', '74016').

card_in_set('kaho, minamo historian', 'SOK').
card_original_type('kaho, minamo historian'/'SOK', 'Legendary Creature — Human Wizard').
card_original_text('kaho, minamo historian'/'SOK', 'When Kaho, Minamo Historian comes into play, search your library for up to three instant cards and remove them from the game. Then shuffle your library.\n{X}, {T}: You may play a card with converted mana cost X removed from the game with Kaho without paying its mana cost.').
card_first_print('kaho, minamo historian', 'SOK').
card_image_name('kaho, minamo historian'/'SOK', 'kaho, minamo historian').
card_uid('kaho, minamo historian'/'SOK', 'SOK:Kaho, Minamo Historian:kaho, minamo historian').
card_rarity('kaho, minamo historian'/'SOK', 'Rare').
card_artist('kaho, minamo historian'/'SOK', 'Greg Staples').
card_number('kaho, minamo historian'/'SOK', '41').
card_multiverse_id('kaho, minamo historian'/'SOK', '74055').

card_in_set('kami of empty graves', 'SOK').
card_original_type('kami of empty graves'/'SOK', 'Creature — Spirit').
card_original_text('kami of empty graves'/'SOK', 'Soulshift 3 (When this is put into a graveyard from play, you may return target Spirit card with converted mana cost 3 or less from your graveyard to your hand.)').
card_first_print('kami of empty graves', 'SOK').
card_image_name('kami of empty graves'/'SOK', 'kami of empty graves').
card_uid('kami of empty graves'/'SOK', 'SOK:Kami of Empty Graves:kami of empty graves').
card_rarity('kami of empty graves'/'SOK', 'Common').
card_artist('kami of empty graves'/'SOK', 'Greg Hildebrandt').
card_number('kami of empty graves'/'SOK', '75').
card_multiverse_id('kami of empty graves'/'SOK', '87335').

card_in_set('kami of the crescent moon', 'SOK').
card_original_type('kami of the crescent moon'/'SOK', 'Legendary Creature — Spirit').
card_original_text('kami of the crescent moon'/'SOK', 'At the beginning of each player\'s draw step, that player draws a card.').
card_first_print('kami of the crescent moon', 'SOK').
card_image_name('kami of the crescent moon'/'SOK', 'kami of the crescent moon').
card_uid('kami of the crescent moon'/'SOK', 'SOK:Kami of the Crescent Moon:kami of the crescent moon').
card_rarity('kami of the crescent moon'/'SOK', 'Rare').
card_artist('kami of the crescent moon'/'SOK', 'Darrell Riche').
card_number('kami of the crescent moon'/'SOK', '42').
card_flavor_text('kami of the crescent moon'/'SOK', '\"He\'s a lot like me, that masterless little kami . . . unimpressed by grandeur and never at a loss for a trick.\"\n—Toshiro Umezawa').
card_multiverse_id('kami of the crescent moon'/'SOK', '84364').

card_in_set('kami of the tended garden', 'SOK').
card_original_type('kami of the tended garden'/'SOK', 'Creature — Spirit').
card_original_text('kami of the tended garden'/'SOK', 'At the beginning of your upkeep, sacrifice Kami of the Tended Garden unless you pay {G}.\nSoulshift 3 (When this is put into a graveyard from play, you may return target Spirit card with converted mana cost 3 or less from your graveyard to your hand.)').
card_first_print('kami of the tended garden', 'SOK').
card_image_name('kami of the tended garden'/'SOK', 'kami of the tended garden').
card_uid('kami of the tended garden'/'SOK', 'SOK:Kami of the Tended Garden:kami of the tended garden').
card_rarity('kami of the tended garden'/'SOK', 'Uncommon').
card_artist('kami of the tended garden'/'SOK', 'Thomas M. Baxa').
card_number('kami of the tended garden'/'SOK', '134').
card_multiverse_id('kami of the tended garden'/'SOK', '74217').

card_in_set('kashi-tribe elite', 'SOK').
card_original_type('kashi-tribe elite'/'SOK', 'Creature — Snake Warrior').
card_original_text('kashi-tribe elite'/'SOK', 'Legendary Snakes you control can\'t be the targets of spells or abilities.\nWhenever Kashi-Tribe Elite deals combat damage to a creature, tap that creature and it doesn\'t untap during its controller\'s next untap step.').
card_first_print('kashi-tribe elite', 'SOK').
card_image_name('kashi-tribe elite'/'SOK', 'kashi-tribe elite').
card_uid('kashi-tribe elite'/'SOK', 'SOK:Kashi-Tribe Elite:kashi-tribe elite').
card_rarity('kashi-tribe elite'/'SOK', 'Uncommon').
card_artist('kashi-tribe elite'/'SOK', 'Ben Thompson').
card_number('kashi-tribe elite'/'SOK', '135').
card_multiverse_id('kashi-tribe elite'/'SOK', '74195').

card_in_set('kataki, war\'s wage', 'SOK').
card_original_type('kataki, war\'s wage'/'SOK', 'Legendary Creature — Spirit').
card_original_text('kataki, war\'s wage'/'SOK', 'All artifacts have \"At the beginning of your upkeep, sacrifice this artifact unless you pay {1}.\"').
card_first_print('kataki, war\'s wage', 'SOK').
card_image_name('kataki, war\'s wage'/'SOK', 'kataki, war\'s wage').
card_uid('kataki, war\'s wage'/'SOK', 'SOK:Kataki, War\'s Wage:kataki, war\'s wage').
card_rarity('kataki, war\'s wage'/'SOK', 'Rare').
card_artist('kataki, war\'s wage'/'SOK', 'Matt Thompson').
card_number('kataki, war\'s wage'/'SOK', '14').
card_flavor_text('kataki, war\'s wage'/'SOK', '\"Before the war, we prayed to Kataki to sharpen our swords and harden our armor. Without his blessing our weapons are all but useless against the kami hordes.\"\n—Kenzo the Hardhearted').
card_multiverse_id('kataki, war\'s wage'/'SOK', '74190').

card_in_set('kemuri-onna', 'SOK').
card_original_type('kemuri-onna'/'SOK', 'Creature — Spirit').
card_original_text('kemuri-onna'/'SOK', 'When Kemuri-Onna comes into play, target player discards a card.\nWhenever you play a Spirit or Arcane spell, you may return Kemuri-Onna to its owner\'s hand.').
card_first_print('kemuri-onna', 'SOK').
card_image_name('kemuri-onna'/'SOK', 'kemuri-onna').
card_uid('kemuri-onna'/'SOK', 'SOK:Kemuri-Onna:kemuri-onna').
card_rarity('kemuri-onna'/'SOK', 'Uncommon').
card_artist('kemuri-onna'/'SOK', 'Ittoku').
card_number('kemuri-onna'/'SOK', '76').
card_multiverse_id('kemuri-onna'/'SOK', '84705').

card_in_set('kiku\'s shadow', 'SOK').
card_original_type('kiku\'s shadow'/'SOK', 'Sorcery').
card_original_text('kiku\'s shadow'/'SOK', 'Target creature deals damage to itself equal to its power.').
card_first_print('kiku\'s shadow', 'SOK').
card_image_name('kiku\'s shadow'/'SOK', 'kiku\'s shadow').
card_uid('kiku\'s shadow'/'SOK', 'SOK:Kiku\'s Shadow:kiku\'s shadow').
card_rarity('kiku\'s shadow'/'SOK', 'Uncommon').
card_artist('kiku\'s shadow'/'SOK', 'Pete Venters').
card_number('kiku\'s shadow'/'SOK', '77').
card_flavor_text('kiku\'s shadow'/'SOK', '\"Me? No, I\'m not going to kill you. I won\'t even lay a finger on you. I promise.\"\n—Kiku, Night\'s Flower').
card_multiverse_id('kiku\'s shadow'/'SOK', '74157').

card_in_set('kiri-onna', 'SOK').
card_original_type('kiri-onna'/'SOK', 'Creature — Spirit').
card_original_text('kiri-onna'/'SOK', 'When Kiri-Onna comes into play, return target creature to its owner\'s hand.\nWhenever you play a Spirit or Arcane spell, you may return Kiri-Onna to its owner\'s hand.').
card_first_print('kiri-onna', 'SOK').
card_image_name('kiri-onna'/'SOK', 'kiri-onna').
card_uid('kiri-onna'/'SOK', 'SOK:Kiri-Onna:kiri-onna').
card_rarity('kiri-onna'/'SOK', 'Uncommon').
card_artist('kiri-onna'/'SOK', 'Kensuke Okabayashi').
card_number('kiri-onna'/'SOK', '43').
card_multiverse_id('kiri-onna'/'SOK', '84704').

card_in_set('kitsune bonesetter', 'SOK').
card_original_type('kitsune bonesetter'/'SOK', 'Creature — Fox Cleric').
card_original_text('kitsune bonesetter'/'SOK', '{T}: Prevent the next 3 damage that would be dealt to target creature this turn. Play this ability only if you have more cards in hand than each opponent.').
card_first_print('kitsune bonesetter', 'SOK').
card_image_name('kitsune bonesetter'/'SOK', 'kitsune bonesetter').
card_uid('kitsune bonesetter'/'SOK', 'SOK:Kitsune Bonesetter:kitsune bonesetter').
card_rarity('kitsune bonesetter'/'SOK', 'Common').
card_artist('kitsune bonesetter'/'SOK', 'Heather Hudson').
card_number('kitsune bonesetter'/'SOK', '15').
card_flavor_text('kitsune bonesetter'/'SOK', '\"He kept generations whole before the Kami War, and now he turns his century of wisdom to healing the latest casualties.\"\n—Eight-and-a-Half-Tails').
card_multiverse_id('kitsune bonesetter'/'SOK', '74046').

card_in_set('kitsune dawnblade', 'SOK').
card_original_type('kitsune dawnblade'/'SOK', 'Creature — Fox Samurai').
card_original_text('kitsune dawnblade'/'SOK', 'Bushido 1 (When this blocks or becomes blocked, it gets +1/+1 until end of turn.)\nWhen Kitsune Dawnblade comes into play, you may tap target creature.').
card_first_print('kitsune dawnblade', 'SOK').
card_image_name('kitsune dawnblade'/'SOK', 'kitsune dawnblade').
card_uid('kitsune dawnblade'/'SOK', 'SOK:Kitsune Dawnblade:kitsune dawnblade').
card_rarity('kitsune dawnblade'/'SOK', 'Common').
card_artist('kitsune dawnblade'/'SOK', 'Carl Critchlow').
card_number('kitsune dawnblade'/'SOK', '16').
card_multiverse_id('kitsune dawnblade'/'SOK', '80530').

card_in_set('kitsune loreweaver', 'SOK').
card_original_type('kitsune loreweaver'/'SOK', 'Creature — Fox Cleric').
card_original_text('kitsune loreweaver'/'SOK', '{1}{W}: Kitsune Loreweaver gets +0/+X until end of turn, where X is the number of cards in your hand.').
card_first_print('kitsune loreweaver', 'SOK').
card_image_name('kitsune loreweaver'/'SOK', 'kitsune loreweaver').
card_uid('kitsune loreweaver'/'SOK', 'SOK:Kitsune Loreweaver:kitsune loreweaver').
card_rarity('kitsune loreweaver'/'SOK', 'Common').
card_artist('kitsune loreweaver'/'SOK', 'Eric Polak').
card_number('kitsune loreweaver'/'SOK', '17').
card_flavor_text('kitsune loreweaver'/'SOK', '\"Know your history. The lessons of the past will shield you in times of doubt.\"').
card_multiverse_id('kitsune loreweaver'/'SOK', '74213').

card_in_set('kiyomaro, first to stand', 'SOK').
card_original_type('kiyomaro, first to stand'/'SOK', 'Legendary Creature — Spirit').
card_original_text('kiyomaro, first to stand'/'SOK', 'Kiyomaro, First to Stand\'s power and toughness are each equal to the number of cards in your hand.\nAs long as you have four or more cards in hand, Kiyomaro has vigilance.\nWhenever Kiyomaro deals damage, if you have seven or more cards in hand, you gain 7 life.').
card_image_name('kiyomaro, first to stand'/'SOK', 'kiyomaro, first to stand').
card_uid('kiyomaro, first to stand'/'SOK', 'SOK:Kiyomaro, First to Stand:kiyomaro, first to stand').
card_rarity('kiyomaro, first to stand'/'SOK', 'Rare').
card_artist('kiyomaro, first to stand'/'SOK', 'Kev Walker').
card_number('kiyomaro, first to stand'/'SOK', '18').
card_multiverse_id('kiyomaro, first to stand'/'SOK', '74054').

card_in_set('kuon\'s essence', 'SOK').
card_original_type('kuon\'s essence'/'SOK', 'Legendary Creature — Ogre Monk').
card_original_text('kuon\'s essence'/'SOK', 'At end of turn, if three or more creatures were put into graveyards from play this turn, flip Kuon, Ogre Ascendant.\n-----\nKuon\'s Essence\nLegendary Enchantment\nAt the beginning of each player\'s upkeep, that player sacrifices a creature.').
card_first_print('kuon\'s essence', 'SOK').
card_image_name('kuon\'s essence'/'SOK', 'kuon\'s essence').
card_uid('kuon\'s essence'/'SOK', 'SOK:Kuon\'s Essence:kuon\'s essence').
card_rarity('kuon\'s essence'/'SOK', 'Rare').
card_artist('kuon\'s essence'/'SOK', 'Hideaki Takamura').
card_number('kuon\'s essence'/'SOK', '78b').
card_multiverse_id('kuon\'s essence'/'SOK', '87596').

card_in_set('kuon, ogre ascendant', 'SOK').
card_original_type('kuon, ogre ascendant'/'SOK', 'Legendary Creature — Ogre Monk').
card_original_text('kuon, ogre ascendant'/'SOK', 'At end of turn, if three or more creatures were put into graveyards from play this turn, flip Kuon, Ogre Ascendant.\n-----\nKuon\'s Essence\nLegendary Enchantment\nAt the beginning of each player\'s upkeep, that player sacrifices a creature.').
card_first_print('kuon, ogre ascendant', 'SOK').
card_image_name('kuon, ogre ascendant'/'SOK', 'kuon, ogre ascendant').
card_uid('kuon, ogre ascendant'/'SOK', 'SOK:Kuon, Ogre Ascendant:kuon, ogre ascendant').
card_rarity('kuon, ogre ascendant'/'SOK', 'Rare').
card_artist('kuon, ogre ascendant'/'SOK', 'Hideaki Takamura').
card_number('kuon, ogre ascendant'/'SOK', '78a').
card_multiverse_id('kuon, ogre ascendant'/'SOK', '87596').

card_in_set('kuro\'s taken', 'SOK').
card_original_type('kuro\'s taken'/'SOK', 'Creature — Rat Samurai').
card_original_text('kuro\'s taken'/'SOK', 'Bushido 1 (When this blocks or becomes blocked, it gets +1/+1 until end of turn.)\n{1}{B}: Regenerate Kuro\'s Taken.').
card_first_print('kuro\'s taken', 'SOK').
card_image_name('kuro\'s taken'/'SOK', 'kuro\'s taken').
card_uid('kuro\'s taken'/'SOK', 'SOK:Kuro\'s Taken:kuro\'s taken').
card_rarity('kuro\'s taken'/'SOK', 'Common').
card_artist('kuro\'s taken'/'SOK', 'Puddnhead').
card_number('kuro\'s taken'/'SOK', '79').
card_flavor_text('kuro\'s taken'/'SOK', 'Simple bargains are the most tempting, and oni bargains are the simplest of all: eternal life for eternal service.').
card_multiverse_id('kuro\'s taken'/'SOK', '74065').

card_in_set('locust miser', 'SOK').
card_original_type('locust miser'/'SOK', 'Creature — Rat Shaman').
card_original_text('locust miser'/'SOK', 'Each opponent\'s maximum hand size is reduced by two.').
card_first_print('locust miser', 'SOK').
card_image_name('locust miser'/'SOK', 'locust miser').
card_uid('locust miser'/'SOK', 'SOK:Locust Miser:locust miser').
card_rarity('locust miser'/'SOK', 'Uncommon').
card_artist('locust miser'/'SOK', 'Alan Pollack').
card_number('locust miser'/'SOK', '80').
card_flavor_text('locust miser'/'SOK', '\"They\'re numerous, attack in swarms, and are always hungry. No wonder we rats hold such a kinship with them.\"').
card_multiverse_id('locust miser'/'SOK', '74394').

card_in_set('maga, traitor to mortals', 'SOK').
card_original_type('maga, traitor to mortals'/'SOK', 'Legendary Creature — Human Wizard').
card_original_text('maga, traitor to mortals'/'SOK', 'Maga, Traitor to Mortals comes into play with X +1/+1 counters on it.\nWhen Maga comes into play, target player loses life equal to the number of +1/+1 counters on it.').
card_first_print('maga, traitor to mortals', 'SOK').
card_image_name('maga, traitor to mortals'/'SOK', 'maga, traitor to mortals').
card_uid('maga, traitor to mortals'/'SOK', 'SOK:Maga, Traitor to Mortals:maga, traitor to mortals').
card_rarity('maga, traitor to mortals'/'SOK', 'Rare').
card_artist('maga, traitor to mortals'/'SOK', 'Jim Murray').
card_number('maga, traitor to mortals'/'SOK', '81').
card_multiverse_id('maga, traitor to mortals'/'SOK', '88802').

card_in_set('manriki-gusari', 'SOK').
card_original_type('manriki-gusari'/'SOK', 'Artifact — Equipment').
card_original_text('manriki-gusari'/'SOK', 'Equipped creature gets +1/+2 and has \"{T}: Destroy target Equipment.\"\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('manriki-gusari', 'SOK').
card_image_name('manriki-gusari'/'SOK', 'manriki-gusari').
card_uid('manriki-gusari'/'SOK', 'SOK:Manriki-Gusari:manriki-gusari').
card_rarity('manriki-gusari'/'SOK', 'Uncommon').
card_artist('manriki-gusari'/'SOK', 'Thomas Gianni').
card_number('manriki-gusari'/'SOK', '156').
card_multiverse_id('manriki-gusari'/'SOK', '74158').

card_in_set('masumaro, first to live', 'SOK').
card_original_type('masumaro, first to live'/'SOK', 'Legendary Creature — Spirit').
card_original_text('masumaro, first to live'/'SOK', 'Masumaro, First to Live\'s power and toughness are each equal to twice the number of cards in your hand.').
card_first_print('masumaro, first to live', 'SOK').
card_image_name('masumaro, first to live'/'SOK', 'masumaro, first to live').
card_uid('masumaro, first to live'/'SOK', 'SOK:Masumaro, First to Live:masumaro, first to live').
card_rarity('masumaro, first to live'/'SOK', 'Rare').
card_artist('masumaro, first to live'/'SOK', 'Adam Rex').
card_number('masumaro, first to live'/'SOK', '136').
card_flavor_text('masumaro, first to live'/'SOK', '\"In the waning moments of war, humanity\'s most ancient ancestors return. But whose side shall they make their own?\"\n—Isao, Enlightened Bushi').
card_multiverse_id('masumaro, first to live'/'SOK', '74184').

card_in_set('matsu-tribe birdstalker', 'SOK').
card_original_type('matsu-tribe birdstalker'/'SOK', 'Creature — Snake Warrior Archer').
card_original_text('matsu-tribe birdstalker'/'SOK', 'Whenever Matsu-Tribe Birdstalker deals combat damage to a creature, tap that creature and it doesn\'t untap during its controller\'s next untap step.\n{G}: Matsu-Tribe Birdstalker may block as though it had flying until end of turn.').
card_first_print('matsu-tribe birdstalker', 'SOK').
card_image_name('matsu-tribe birdstalker'/'SOK', 'matsu-tribe birdstalker').
card_uid('matsu-tribe birdstalker'/'SOK', 'SOK:Matsu-Tribe Birdstalker:matsu-tribe birdstalker').
card_rarity('matsu-tribe birdstalker'/'SOK', 'Common').
card_artist('matsu-tribe birdstalker'/'SOK', 'Heather Hudson').
card_number('matsu-tribe birdstalker'/'SOK', '137').
card_multiverse_id('matsu-tribe birdstalker'/'SOK', '73991').

card_in_set('measure of wickedness', 'SOK').
card_original_type('measure of wickedness'/'SOK', 'Enchantment').
card_original_text('measure of wickedness'/'SOK', 'At the end of your turn, sacrifice Measure of Wickedness and you lose 8 life.\nWhenever another card is put into your graveyard from anywhere, target opponent gains control of Measure of Wickedness.').
card_first_print('measure of wickedness', 'SOK').
card_image_name('measure of wickedness'/'SOK', 'measure of wickedness').
card_uid('measure of wickedness'/'SOK', 'SOK:Measure of Wickedness:measure of wickedness').
card_rarity('measure of wickedness'/'SOK', 'Uncommon').
card_artist('measure of wickedness'/'SOK', 'Daren Bader').
card_number('measure of wickedness'/'SOK', '82').
card_multiverse_id('measure of wickedness'/'SOK', '88821').

card_in_set('meishin, the mind cage', 'SOK').
card_original_type('meishin, the mind cage'/'SOK', 'Legendary Enchantment').
card_original_text('meishin, the mind cage'/'SOK', 'All creatures get -X/-0, where X is the number of cards in your hand.').
card_first_print('meishin, the mind cage', 'SOK').
card_image_name('meishin, the mind cage'/'SOK', 'meishin, the mind cage').
card_uid('meishin, the mind cage'/'SOK', 'SOK:Meishin, the Mind Cage:meishin, the mind cage').
card_rarity('meishin, the mind cage'/'SOK', 'Rare').
card_artist('meishin, the mind cage'/'SOK', 'Thomas Gianni').
card_number('meishin, the mind cage'/'SOK', '44').
card_flavor_text('meishin, the mind cage'/'SOK', 'It\'s dangerous indeed to be lost in someone else\'s thoughts.').
card_multiverse_id('meishin, the mind cage'/'SOK', '89406').

card_in_set('michiko konda, truth seeker', 'SOK').
card_original_type('michiko konda, truth seeker'/'SOK', 'Legendary Creature — Human Advisor').
card_original_text('michiko konda, truth seeker'/'SOK', 'Whenever a source an opponent controls deals damage to you, that player sacrifices a permanent.').
card_first_print('michiko konda, truth seeker', 'SOK').
card_image_name('michiko konda, truth seeker'/'SOK', 'michiko konda, truth seeker').
card_uid('michiko konda, truth seeker'/'SOK', 'SOK:Michiko Konda, Truth Seeker:michiko konda, truth seeker').
card_rarity('michiko konda, truth seeker'/'SOK', 'Rare').
card_artist('michiko konda, truth seeker'/'SOK', 'Christopher Moeller').
card_number('michiko konda, truth seeker'/'SOK', '19').
card_flavor_text('michiko konda, truth seeker'/'SOK', '\"Watch over my father. Tell him I\'m safe, but I won\'t come home until I find out how to bring him back to his senses, and Kamigawa is again at peace.\"\n—Michiko Konda, last letter to General Takeno').
card_multiverse_id('michiko konda, truth seeker'/'SOK', '84359').

card_in_set('mikokoro, center of the sea', 'SOK').
card_original_type('mikokoro, center of the sea'/'SOK', 'Legendary Land').
card_original_text('mikokoro, center of the sea'/'SOK', '{T}: Add {1} to your mana pool.\n{2}, {T}: Each player draws a card.').
card_first_print('mikokoro, center of the sea', 'SOK').
card_image_name('mikokoro, center of the sea'/'SOK', 'mikokoro, center of the sea').
card_uid('mikokoro, center of the sea'/'SOK', 'SOK:Mikokoro, Center of the Sea:mikokoro, center of the sea').
card_rarity('mikokoro, center of the sea'/'SOK', 'Rare').
card_artist('mikokoro, center of the sea'/'SOK', 'John Avon').
card_number('mikokoro, center of the sea'/'SOK', '162').
card_flavor_text('mikokoro, center of the sea'/'SOK', '\"Center of the Sea, Eye of the World, Shrine of Enlightenment. Seek it for answers. Seek it for healing. Seek it and return transformed.\"\n—Scroll fragment from the ruins of Minamo').
card_multiverse_id('mikokoro, center of the sea'/'SOK', '74388').

card_in_set('minamo scrollkeeper', 'SOK').
card_original_type('minamo scrollkeeper'/'SOK', 'Creature — Human Wizard').
card_original_text('minamo scrollkeeper'/'SOK', 'Defender (This creature can\'t attack.)\nYour maximum hand size is increased by one.').
card_first_print('minamo scrollkeeper', 'SOK').
card_image_name('minamo scrollkeeper'/'SOK', 'minamo scrollkeeper').
card_uid('minamo scrollkeeper'/'SOK', 'SOK:Minamo Scrollkeeper:minamo scrollkeeper').
card_rarity('minamo scrollkeeper'/'SOK', 'Common').
card_artist('minamo scrollkeeper'/'SOK', 'Paolo Parente').
card_number('minamo scrollkeeper'/'SOK', '45').
card_flavor_text('minamo scrollkeeper'/'SOK', 'The scrollkeepers never stepped an inch away from the doors of the Great Library, not even when the ogres and their oni masters swept through Minamo.').
card_multiverse_id('minamo scrollkeeper'/'SOK', '88791').

card_in_set('miren, the moaning well', 'SOK').
card_original_type('miren, the moaning well'/'SOK', 'Legendary Land').
card_original_text('miren, the moaning well'/'SOK', '{T}: Add {1} to your mana pool.\n{3}, {T}, Sacrifice a creature: You gain life equal to the sacrificed creature\'s toughness.').
card_first_print('miren, the moaning well', 'SOK').
card_image_name('miren, the moaning well'/'SOK', 'miren, the moaning well').
card_uid('miren, the moaning well'/'SOK', 'SOK:Miren, the Moaning Well:miren, the moaning well').
card_rarity('miren, the moaning well'/'SOK', 'Rare').
card_artist('miren, the moaning well'/'SOK', 'Rob Alexander').
card_number('miren, the moaning well'/'SOK', '163').
card_multiverse_id('miren, the moaning well'/'SOK', '88807').

card_in_set('molting skin', 'SOK').
card_original_type('molting skin'/'SOK', 'Enchantment').
card_original_text('molting skin'/'SOK', 'Return Molting Skin to its owner\'s hand: Regenerate target creature.').
card_first_print('molting skin', 'SOK').
card_image_name('molting skin'/'SOK', 'molting skin').
card_uid('molting skin'/'SOK', 'SOK:Molting Skin:molting skin').
card_rarity('molting skin'/'SOK', 'Uncommon').
card_artist('molting skin'/'SOK', 'Jeremy Jarvis').
card_number('molting skin'/'SOK', '138').
card_flavor_text('molting skin'/'SOK', '\"We have learned much from the orochi.\"').
card_multiverse_id('molting skin'/'SOK', '87331').

card_in_set('moonbow illusionist', 'SOK').
card_original_type('moonbow illusionist'/'SOK', 'Creature — Moonfolk Wizard').
card_original_text('moonbow illusionist'/'SOK', 'Flying\n{2}, Return a land you control to its owner\'s hand: Target land\'s type becomes the basic land type of your choice until end of turn.').
card_first_print('moonbow illusionist', 'SOK').
card_image_name('moonbow illusionist'/'SOK', 'moonbow illusionist').
card_uid('moonbow illusionist'/'SOK', 'SOK:Moonbow Illusionist:moonbow illusionist').
card_rarity('moonbow illusionist'/'SOK', 'Common').
card_artist('moonbow illusionist'/'SOK', 'Dan Frazier').
card_number('moonbow illusionist'/'SOK', '46').
card_multiverse_id('moonbow illusionist'/'SOK', '74198').

card_in_set('moonwing moth', 'SOK').
card_original_type('moonwing moth'/'SOK', 'Creature — Insect').
card_original_text('moonwing moth'/'SOK', 'Flying\n{W}: Moonwing Moth gets +0/+1 until end of turn.').
card_first_print('moonwing moth', 'SOK').
card_image_name('moonwing moth'/'SOK', 'moonwing moth').
card_uid('moonwing moth'/'SOK', 'SOK:Moonwing Moth:moonwing moth').
card_rarity('moonwing moth'/'SOK', 'Common').
card_artist('moonwing moth'/'SOK', 'Franz Vohwinkel').
card_number('moonwing moth'/'SOK', '20').
card_flavor_text('moonwing moth'/'SOK', '\"Night wings sprinkled with dust\nOf a thousand soldiers dead\nAnd a thousand cities fallen.\"\n—Snow-Fur, kitsune poet').
card_multiverse_id('moonwing moth'/'SOK', '84643').

card_in_set('murmurs from beyond', 'SOK').
card_original_type('murmurs from beyond'/'SOK', 'Instant — Arcane').
card_original_text('murmurs from beyond'/'SOK', 'Reveal the top three cards of your library. An opponent chooses one. Put that card into your graveyard and the rest into your hand.').
card_first_print('murmurs from beyond', 'SOK').
card_image_name('murmurs from beyond'/'SOK', 'murmurs from beyond').
card_uid('murmurs from beyond'/'SOK', 'SOK:Murmurs from Beyond:murmurs from beyond').
card_rarity('murmurs from beyond'/'SOK', 'Common').
card_artist('murmurs from beyond'/'SOK', 'Paolo Parente').
card_number('murmurs from beyond'/'SOK', '47').
card_flavor_text('murmurs from beyond'/'SOK', 'Like pools of fog that hid treacherous rocks from sailors\' eyes, so too were the hands of the kami.').
card_multiverse_id('murmurs from beyond'/'SOK', '87330').

card_in_set('neverending torment', 'SOK').
card_original_type('neverending torment'/'SOK', 'Sorcery').
card_original_text('neverending torment'/'SOK', 'Search target player\'s library for X cards, where X is the number of cards in your hand, and remove them from the game. Then that player shuffles his or her library.\nEpic (For the rest of the game, you can\'t play spells. At the beginning of each of your upkeeps, copy this spell except for its epic ability. You may choose a new target for the copy.)').
card_first_print('neverending torment', 'SOK').
card_image_name('neverending torment'/'SOK', 'neverending torment').
card_uid('neverending torment'/'SOK', 'SOK:Neverending Torment:neverending torment').
card_rarity('neverending torment'/'SOK', 'Rare').
card_artist('neverending torment'/'SOK', 'Thomas Gianni').
card_number('neverending torment'/'SOK', '83').
card_multiverse_id('neverending torment'/'SOK', '87601').

card_in_set('nightsoil kami', 'SOK').
card_original_type('nightsoil kami'/'SOK', 'Creature — Spirit').
card_original_text('nightsoil kami'/'SOK', 'Soulshift 5 (When this is put into a graveyard from play, you may return target Spirit card with converted mana cost 5 or less from your graveyard to your hand.)').
card_first_print('nightsoil kami', 'SOK').
card_image_name('nightsoil kami'/'SOK', 'nightsoil kami').
card_uid('nightsoil kami'/'SOK', 'SOK:Nightsoil Kami:nightsoil kami').
card_rarity('nightsoil kami'/'SOK', 'Common').
card_artist('nightsoil kami'/'SOK', 'Jim Nelson').
card_number('nightsoil kami'/'SOK', '139').
card_multiverse_id('nightsoil kami'/'SOK', '88815').

card_in_set('nikko-onna', 'SOK').
card_original_type('nikko-onna'/'SOK', 'Creature — Spirit').
card_original_text('nikko-onna'/'SOK', 'When Nikko-Onna comes into play, destroy target enchantment.\nWhenever you play a Spirit or Arcane spell, you may return Nikko-Onna to its owner\'s hand.').
card_first_print('nikko-onna', 'SOK').
card_image_name('nikko-onna'/'SOK', 'nikko-onna').
card_uid('nikko-onna'/'SOK', 'SOK:Nikko-Onna:nikko-onna').
card_rarity('nikko-onna'/'SOK', 'Uncommon').
card_artist('nikko-onna'/'SOK', 'Shishizaru').
card_number('nikko-onna'/'SOK', '21').
card_multiverse_id('nikko-onna'/'SOK', '84639').

card_in_set('o-naginata', 'SOK').
card_original_type('o-naginata'/'SOK', 'Artifact — Equipment').
card_original_text('o-naginata'/'SOK', 'O-Naginata can be attached only to a creature with 3 or more power.\nEquipped creature gets +3/+0 and has trample.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('o-naginata', 'SOK').
card_image_name('o-naginata'/'SOK', 'o-naginata').
card_uid('o-naginata'/'SOK', 'SOK:O-Naginata:o-naginata').
card_rarity('o-naginata'/'SOK', 'Uncommon').
card_artist('o-naginata'/'SOK', 'Brian Snõddy').
card_number('o-naginata'/'SOK', '157').
card_multiverse_id('o-naginata'/'SOK', '74146').

card_in_set('oboro breezecaller', 'SOK').
card_original_type('oboro breezecaller'/'SOK', 'Creature — Moonfolk Wizard').
card_original_text('oboro breezecaller'/'SOK', 'Flying\n{2}, Return a land you control to its owner\'s hand: Untap target land.').
card_first_print('oboro breezecaller', 'SOK').
card_image_name('oboro breezecaller'/'SOK', 'oboro breezecaller').
card_uid('oboro breezecaller'/'SOK', 'SOK:Oboro Breezecaller:oboro breezecaller').
card_rarity('oboro breezecaller'/'SOK', 'Common').
card_artist('oboro breezecaller'/'SOK', 'Rebecca Guay').
card_number('oboro breezecaller'/'SOK', '48').
card_flavor_text('oboro breezecaller'/'SOK', '\"I feel their unnatural influence in the weary, weeping land. Do they care nothing for the creatures who walk below their clouds?\"\n—Eight-and-a-Half-Tails').
card_multiverse_id('oboro breezecaller'/'SOK', '88784').

card_in_set('oboro envoy', 'SOK').
card_original_type('oboro envoy'/'SOK', 'Creature — Moonfolk Wizard').
card_original_text('oboro envoy'/'SOK', 'Flying\n{2}, Return a land you control to its owner\'s hand: Target creature gets -X/-0, where X is the number of cards in your hand.').
card_first_print('oboro envoy', 'SOK').
card_image_name('oboro envoy'/'SOK', 'oboro envoy').
card_uid('oboro envoy'/'SOK', 'SOK:Oboro Envoy:oboro envoy').
card_rarity('oboro envoy'/'SOK', 'Uncommon').
card_artist('oboro envoy'/'SOK', 'Rob Alexander').
card_number('oboro envoy'/'SOK', '49').
card_multiverse_id('oboro envoy'/'SOK', '74211').

card_in_set('oboro, palace in the clouds', 'SOK').
card_original_type('oboro, palace in the clouds'/'SOK', 'Legendary Land').
card_original_text('oboro, palace in the clouds'/'SOK', '{T}: Add {U} to your mana pool.\n{1}: Return Oboro, Palace in the Clouds to its owner\'s hand.').
card_first_print('oboro, palace in the clouds', 'SOK').
card_image_name('oboro, palace in the clouds'/'SOK', 'oboro, palace in the clouds').
card_uid('oboro, palace in the clouds'/'SOK', 'SOK:Oboro, Palace in the Clouds:oboro, palace in the clouds').
card_rarity('oboro, palace in the clouds'/'SOK', 'Rare').
card_artist('oboro, palace in the clouds'/'SOK', 'Rob Alexander').
card_number('oboro, palace in the clouds'/'SOK', '164').
card_flavor_text('oboro, palace in the clouds'/'SOK', 'Here the soratami wove their endless plots—graceful, artful, and deceitful, while the unwitting world moved below.').
card_multiverse_id('oboro, palace in the clouds'/'SOK', '74206').

card_in_set('okina nightwatch', 'SOK').
card_original_type('okina nightwatch'/'SOK', 'Creature — Human Monk').
card_original_text('okina nightwatch'/'SOK', 'As long as you have more cards in hand than each opponent, Okina Nightwatch gets +3/+3.').
card_image_name('okina nightwatch'/'SOK', 'okina nightwatch').
card_uid('okina nightwatch'/'SOK', 'SOK:Okina Nightwatch:okina nightwatch').
card_rarity('okina nightwatch'/'SOK', 'Common').
card_artist('okina nightwatch'/'SOK', 'Heather Hudson').
card_number('okina nightwatch'/'SOK', '140').
card_flavor_text('okina nightwatch'/'SOK', 'Only the most skilled budoka could protect the Okina Shrine. Only the best of these protectors took the night watch.').
card_multiverse_id('okina nightwatch'/'SOK', '74196').

card_in_set('one with nothing', 'SOK').
card_original_type('one with nothing'/'SOK', 'Instant').
card_original_text('one with nothing'/'SOK', 'Discard your hand.').
card_first_print('one with nothing', 'SOK').
card_image_name('one with nothing'/'SOK', 'one with nothing').
card_uid('one with nothing'/'SOK', 'SOK:One with Nothing:one with nothing').
card_rarity('one with nothing'/'SOK', 'Rare').
card_artist('one with nothing'/'SOK', 'Jim Nelson').
card_number('one with nothing'/'SOK', '84').
card_flavor_text('one with nothing'/'SOK', 'When nothing remains, everything is equally possible.').
card_multiverse_id('one with nothing'/'SOK', '88817').

card_in_set('oni of wild places', 'SOK').
card_original_type('oni of wild places'/'SOK', 'Creature — Demon Spirit').
card_original_text('oni of wild places'/'SOK', 'Haste\nAt the beginning of your upkeep, return a red creature you control to its owner\'s hand.').
card_first_print('oni of wild places', 'SOK').
card_image_name('oni of wild places'/'SOK', 'oni of wild places').
card_uid('oni of wild places'/'SOK', 'SOK:Oni of Wild Places:oni of wild places').
card_rarity('oni of wild places'/'SOK', 'Uncommon').
card_artist('oni of wild places'/'SOK', 'Mark Tedin').
card_number('oni of wild places'/'SOK', '108').
card_flavor_text('oni of wild places'/'SOK', 'The oni leapt easily from peak to peak, toying with its victims, its voice a purr from the rumbling depths of nightmare.').
card_multiverse_id('oni of wild places'/'SOK', '74048').

card_in_set('oppressive will', 'SOK').
card_original_type('oppressive will'/'SOK', 'Instant').
card_original_text('oppressive will'/'SOK', 'Counter target spell unless its controller pays {1} for each card in your hand.').
card_first_print('oppressive will', 'SOK').
card_image_name('oppressive will'/'SOK', 'oppressive will').
card_uid('oppressive will'/'SOK', 'SOK:Oppressive Will:oppressive will').
card_rarity('oppressive will'/'SOK', 'Common').
card_artist('oppressive will'/'SOK', 'Pat Lee').
card_number('oppressive will'/'SOK', '50').
card_flavor_text('oppressive will'/'SOK', 'When Minamo was abandoned, its younger students found themselves thrust into a war beyond their skill with talents beyond their control.').
card_multiverse_id('oppressive will'/'SOK', '74423').

card_in_set('overwhelming intellect', 'SOK').
card_original_type('overwhelming intellect'/'SOK', 'Instant').
card_original_text('overwhelming intellect'/'SOK', 'Counter target creature spell. Draw cards equal to that spell\'s converted mana cost.').
card_first_print('overwhelming intellect', 'SOK').
card_image_name('overwhelming intellect'/'SOK', 'overwhelming intellect').
card_uid('overwhelming intellect'/'SOK', 'SOK:Overwhelming Intellect:overwhelming intellect').
card_rarity('overwhelming intellect'/'SOK', 'Uncommon').
card_artist('overwhelming intellect'/'SOK', 'Alex Horley-Orlandelli').
card_number('overwhelming intellect'/'SOK', '51').
card_flavor_text('overwhelming intellect'/'SOK', '\"My brain hurts.\"').
card_multiverse_id('overwhelming intellect'/'SOK', '89403').

card_in_set('pain\'s reward', 'SOK').
card_original_type('pain\'s reward'/'SOK', 'Sorcery').
card_original_text('pain\'s reward'/'SOK', 'You bid any amount of life. In turn order, each player may top the high bid. The bidding ends if the high bid stands. The high bidder loses life equal to the high bid and draws four cards.').
card_first_print('pain\'s reward', 'SOK').
card_image_name('pain\'s reward'/'SOK', 'pain\'s reward').
card_uid('pain\'s reward'/'SOK', 'SOK:Pain\'s Reward:pain\'s reward').
card_rarity('pain\'s reward'/'SOK', 'Rare').
card_artist('pain\'s reward'/'SOK', 'Matt Cavotta').
card_number('pain\'s reward'/'SOK', '85').
card_multiverse_id('pain\'s reward'/'SOK', '75340').

card_in_set('path of anger\'s flame', 'SOK').
card_original_type('path of anger\'s flame'/'SOK', 'Instant — Arcane').
card_original_text('path of anger\'s flame'/'SOK', 'Creatures you control get +2/+0 until end of turn.').
card_first_print('path of anger\'s flame', 'SOK').
card_image_name('path of anger\'s flame'/'SOK', 'path of anger\'s flame').
card_uid('path of anger\'s flame'/'SOK', 'SOK:Path of Anger\'s Flame:path of anger\'s flame').
card_rarity('path of anger\'s flame'/'SOK', 'Common').
card_artist('path of anger\'s flame'/'SOK', 'Pat Lee').
card_number('path of anger\'s flame'/'SOK', '109').
card_flavor_text('path of anger\'s flame'/'SOK', '\"Walking the path of the kami only leaves you with singed feet.\"\n—Soratami saying').
card_multiverse_id('path of anger\'s flame'/'SOK', '74179').

card_in_set('pithing needle', 'SOK').
card_original_type('pithing needle'/'SOK', 'Artifact').
card_original_text('pithing needle'/'SOK', 'As Pithing Needle comes into play, name a card.\nActivated abilities of the named card can\'t be played unless they\'re mana abilities.').
card_first_print('pithing needle', 'SOK').
card_image_name('pithing needle'/'SOK', 'pithing needle').
card_uid('pithing needle'/'SOK', 'SOK:Pithing Needle:pithing needle').
card_rarity('pithing needle'/'SOK', 'Rare').
card_artist('pithing needle'/'SOK', 'Pete Venters').
card_number('pithing needle'/'SOK', '158').
card_multiverse_id('pithing needle'/'SOK', '74207').

card_in_set('plow through reito', 'SOK').
card_original_type('plow through reito'/'SOK', 'Instant — Arcane').
card_original_text('plow through reito'/'SOK', 'Sweep Return any number of Plains you control to their owner\'s hand. Target creature gets +1/+1 until end of turn for each Plains returned this way.').
card_first_print('plow through reito', 'SOK').
card_image_name('plow through reito'/'SOK', 'plow through reito').
card_uid('plow through reito'/'SOK', 'SOK:Plow Through Reito:plow through reito').
card_rarity('plow through reito'/'SOK', 'Common').
card_artist('plow through reito'/'SOK', 'Ron Spencer').
card_number('plow through reito'/'SOK', '22').
card_flavor_text('plow through reito'/'SOK', 'The paths of the kami crisscrossed Kamigawa like scars left by a maddened surgeon.').
card_multiverse_id('plow through reito'/'SOK', '89397').

card_in_set('presence of the wise', 'SOK').
card_original_type('presence of the wise'/'SOK', 'Sorcery').
card_original_text('presence of the wise'/'SOK', 'You gain 2 life for each card in your hand.').
card_first_print('presence of the wise', 'SOK').
card_image_name('presence of the wise'/'SOK', 'presence of the wise').
card_uid('presence of the wise'/'SOK', 'SOK:Presence of the Wise:presence of the wise').
card_rarity('presence of the wise'/'SOK', 'Uncommon').
card_artist('presence of the wise'/'SOK', 'Christopher Moeller').
card_number('presence of the wise'/'SOK', '23').
card_flavor_text('presence of the wise'/'SOK', '\"Humbly I bow. Silently I wait. Deeply I listen.\"').
card_multiverse_id('presence of the wise'/'SOK', '74216').

card_in_set('promise of bunrei', 'SOK').
card_original_type('promise of bunrei'/'SOK', 'Enchantment').
card_original_text('promise of bunrei'/'SOK', 'Whenever a creature you control is put into a graveyard from play, sacrifice Promise of Bunrei. If you do, put four 1/1 colorless Spirit creature tokens into play.').
card_first_print('promise of bunrei', 'SOK').
card_image_name('promise of bunrei'/'SOK', 'promise of bunrei').
card_uid('promise of bunrei'/'SOK', 'SOK:Promise of Bunrei:promise of bunrei').
card_rarity('promise of bunrei'/'SOK', 'Rare').
card_artist('promise of bunrei'/'SOK', 'Stephen Tappin').
card_number('promise of bunrei'/'SOK', '24').
card_flavor_text('promise of bunrei'/'SOK', '\"I am not afraid to die today nor afraid of what death will bring.\"').
card_multiverse_id('promise of bunrei'/'SOK', '84376').

card_in_set('promised kannushi', 'SOK').
card_original_type('promised kannushi'/'SOK', 'Creature — Human Druid').
card_original_text('promised kannushi'/'SOK', 'Soulshift 7 (When this is put into a graveyard from play, you may return target Spirit card with converted mana cost 7 or less from your graveyard to your hand.)').
card_first_print('promised kannushi', 'SOK').
card_image_name('promised kannushi'/'SOK', 'promised kannushi').
card_uid('promised kannushi'/'SOK', 'SOK:Promised Kannushi:promised kannushi').
card_rarity('promised kannushi'/'SOK', 'Common').
card_artist('promised kannushi'/'SOK', 'Matt Thompson').
card_number('promised kannushi'/'SOK', '141').
card_multiverse_id('promised kannushi'/'SOK', '74018').

card_in_set('pure intentions', 'SOK').
card_original_type('pure intentions'/'SOK', 'Instant — Arcane').
card_original_text('pure intentions'/'SOK', 'Whenever a spell or ability an opponent controls causes you to discard cards this turn, return those cards from your graveyard to your hand.\nWhenever a spell or ability an opponent controls causes you to discard Pure Intentions, return Pure Intentions from your graveyard to your hand at end of turn.').
card_first_print('pure intentions', 'SOK').
card_image_name('pure intentions'/'SOK', 'pure intentions').
card_uid('pure intentions'/'SOK', 'SOK:Pure Intentions:pure intentions').
card_rarity('pure intentions'/'SOK', 'Rare').
card_artist('pure intentions'/'SOK', 'Randy Gallegos').
card_number('pure intentions'/'SOK', '25').
card_multiverse_id('pure intentions'/'SOK', '88785').

card_in_set('rally the horde', 'SOK').
card_original_type('rally the horde'/'SOK', 'Sorcery').
card_original_text('rally the horde'/'SOK', 'Remove the top three cards of your library from the game. If the last card removed isn\'t a land, repeat this process until the last card removed is a land. Put a 1/1 red Warrior creature token into play for each nonland card removed from the game this way.').
card_first_print('rally the horde', 'SOK').
card_image_name('rally the horde'/'SOK', 'rally the horde').
card_uid('rally the horde'/'SOK', 'SOK:Rally the Horde:rally the horde').
card_rarity('rally the horde'/'SOK', 'Rare').
card_artist('rally the horde'/'SOK', 'Paolo Parente').
card_number('rally the horde'/'SOK', '110').
card_multiverse_id('rally the horde'/'SOK', '74411').

card_in_set('raving oni-slave', 'SOK').
card_original_type('raving oni-slave'/'SOK', 'Creature — Ogre Warrior').
card_original_text('raving oni-slave'/'SOK', 'When Raving Oni-Slave comes into play, you lose 3 life if you don\'t control a Demon.\nWhen Raving Oni-Slave leaves play, you lose 3 life if you don\'t control a Demon.').
card_first_print('raving oni-slave', 'SOK').
card_image_name('raving oni-slave'/'SOK', 'raving oni-slave').
card_uid('raving oni-slave'/'SOK', 'SOK:Raving Oni-Slave:raving oni-slave').
card_rarity('raving oni-slave'/'SOK', 'Common').
card_artist('raving oni-slave'/'SOK', 'Eric Polak').
card_number('raving oni-slave'/'SOK', '86').
card_multiverse_id('raving oni-slave'/'SOK', '88787').

card_in_set('razorjaw oni', 'SOK').
card_original_type('razorjaw oni'/'SOK', 'Creature — Demon Spirit').
card_original_text('razorjaw oni'/'SOK', 'Black creatures can\'t block.').
card_first_print('razorjaw oni', 'SOK').
card_image_name('razorjaw oni'/'SOK', 'razorjaw oni').
card_uid('razorjaw oni'/'SOK', 'SOK:Razorjaw Oni:razorjaw oni').
card_rarity('razorjaw oni'/'SOK', 'Uncommon').
card_artist('razorjaw oni'/'SOK', 'Carl Critchlow').
card_number('razorjaw oni'/'SOK', '87').
card_flavor_text('razorjaw oni'/'SOK', 'It gnashed its teeth, and the battlefield filled with the screech of blade striking blade.').
card_multiverse_id('razorjaw oni'/'SOK', '74396').

card_in_set('reki, the history of kamigawa', 'SOK').
card_original_type('reki, the history of kamigawa'/'SOK', 'Legendary Creature — Human Shaman').
card_original_text('reki, the history of kamigawa'/'SOK', 'Whenever you play a legendary spell, draw a card.').
card_first_print('reki, the history of kamigawa', 'SOK').
card_image_name('reki, the history of kamigawa'/'SOK', 'reki, the history of kamigawa').
card_uid('reki, the history of kamigawa'/'SOK', 'SOK:Reki, the History of Kamigawa:reki, the history of kamigawa').
card_rarity('reki, the history of kamigawa'/'SOK', 'Rare').
card_artist('reki, the history of kamigawa'/'SOK', 'Edward P. Beard, Jr.').
card_number('reki, the history of kamigawa'/'SOK', '142').
card_flavor_text('reki, the history of kamigawa'/'SOK', '\"After his death, monks spent ten years transcribing the tattoos from Reki\'s body and gathering stories from those who spoke with him. Thus the volume you hold was written.\"\n—The History of Kamigawa').
card_multiverse_id('reki, the history of kamigawa'/'SOK', '74604').

card_in_set('rending vines', 'SOK').
card_original_type('rending vines'/'SOK', 'Instant — Arcane').
card_original_text('rending vines'/'SOK', 'Destroy target artifact or enchantment if its converted mana cost is less than or equal to the number of cards in your hand.\nDraw a card.').
card_first_print('rending vines', 'SOK').
card_image_name('rending vines'/'SOK', 'rending vines').
card_uid('rending vines'/'SOK', 'SOK:Rending Vines:rending vines').
card_rarity('rending vines'/'SOK', 'Common').
card_artist('rending vines'/'SOK', 'Dan Frazier').
card_number('rending vines'/'SOK', '143').
card_multiverse_id('rending vines'/'SOK', '74189').

card_in_set('reverence', 'SOK').
card_original_type('reverence'/'SOK', 'Enchantment').
card_original_text('reverence'/'SOK', 'Creatures with power 2 or less can\'t attack you.').
card_first_print('reverence', 'SOK').
card_image_name('reverence'/'SOK', 'reverence').
card_uid('reverence'/'SOK', 'SOK:Reverence:reverence').
card_rarity('reverence'/'SOK', 'Rare').
card_artist('reverence'/'SOK', 'Ittoku').
card_number('reverence'/'SOK', '26').
card_flavor_text('reverence'/'SOK', '\"Despite the massive forces that allied against him, Konda never once believed he would lose the Kami War.\"\n—Masako the Humorless').
card_multiverse_id('reverence'/'SOK', '84371').

card_in_set('ronin cavekeeper', 'SOK').
card_original_type('ronin cavekeeper'/'SOK', 'Creature — Human Samurai').
card_original_text('ronin cavekeeper'/'SOK', 'Bushido 2 (When this blocks or becomes blocked, it gets +2/+2 until end of turn.)').
card_first_print('ronin cavekeeper', 'SOK').
card_image_name('ronin cavekeeper'/'SOK', 'ronin cavekeeper').
card_uid('ronin cavekeeper'/'SOK', 'SOK:Ronin Cavekeeper:ronin cavekeeper').
card_rarity('ronin cavekeeper'/'SOK', 'Common').
card_artist('ronin cavekeeper'/'SOK', 'Paolo Parente').
card_number('ronin cavekeeper'/'SOK', '111').
card_flavor_text('ronin cavekeeper'/'SOK', 'The akki were surprised to encounter a human who could fight in the caves as well as they.').
card_multiverse_id('ronin cavekeeper'/'SOK', '74199').

card_in_set('rune-tail\'s essence', 'SOK').
card_original_type('rune-tail\'s essence'/'SOK', 'Legendary Creature — Fox Monk').
card_original_text('rune-tail\'s essence'/'SOK', 'When you have 30 or more life, flip Rune-Tail, Kitsune Ascendant.\n-----\nRune-Tail\'s Essence\nLegendary Enchantment\nPrevent all damage that would be dealt to creatures you control.').
card_first_print('rune-tail\'s essence', 'SOK').
card_image_name('rune-tail\'s essence'/'SOK', 'rune-tail\'s essence').
card_uid('rune-tail\'s essence'/'SOK', 'SOK:Rune-Tail\'s Essence:rune-tail\'s essence').
card_rarity('rune-tail\'s essence'/'SOK', 'Rare').
card_artist('rune-tail\'s essence'/'SOK', 'Randy Gallegos').
card_number('rune-tail\'s essence'/'SOK', '27b').
card_multiverse_id('rune-tail\'s essence'/'SOK', '87600').

card_in_set('rune-tail, kitsune ascendant', 'SOK').
card_original_type('rune-tail, kitsune ascendant'/'SOK', 'Legendary Creature — Fox Monk').
card_original_text('rune-tail, kitsune ascendant'/'SOK', 'When you have 30 or more life, flip Rune-Tail, Kitsune Ascendant.\n-----\nRune-Tail\'s Essence\nLegendary Enchantment\nPrevent all damage that would be dealt to creatures you control.').
card_first_print('rune-tail, kitsune ascendant', 'SOK').
card_image_name('rune-tail, kitsune ascendant'/'SOK', 'rune-tail, kitsune ascendant').
card_uid('rune-tail, kitsune ascendant'/'SOK', 'SOK:Rune-Tail, Kitsune Ascendant:rune-tail, kitsune ascendant').
card_rarity('rune-tail, kitsune ascendant'/'SOK', 'Rare').
card_artist('rune-tail, kitsune ascendant'/'SOK', 'Randy Gallegos').
card_number('rune-tail, kitsune ascendant'/'SOK', '27a').
card_multiverse_id('rune-tail, kitsune ascendant'/'SOK', '87600').

card_in_set('rushing-tide zubera', 'SOK').
card_original_type('rushing-tide zubera'/'SOK', 'Creature — Zubera Spirit').
card_original_text('rushing-tide zubera'/'SOK', 'When Rushing-Tide Zubera is put into a graveyard from play, if 4 or more damage was dealt to it this turn, draw three cards.').
card_first_print('rushing-tide zubera', 'SOK').
card_image_name('rushing-tide zubera'/'SOK', 'rushing-tide zubera').
card_uid('rushing-tide zubera'/'SOK', 'SOK:Rushing-Tide Zubera:rushing-tide zubera').
card_rarity('rushing-tide zubera'/'SOK', 'Uncommon').
card_artist('rushing-tide zubera'/'SOK', 'Mark Brill').
card_number('rushing-tide zubera'/'SOK', '52').
card_flavor_text('rushing-tide zubera'/'SOK', 'Cut off from the kakuriyo by the destruction of their honden, the zubera sought new ways to bring their secrets home.').
card_multiverse_id('rushing-tide zubera'/'SOK', '50430').

card_in_set('sakashima the impostor', 'SOK').
card_original_type('sakashima the impostor'/'SOK', 'Legendary Creature — Human Rogue').
card_original_text('sakashima the impostor'/'SOK', 'As Sakashima the Impostor comes into play, you may choose a creature in play. If you do, Sakashima comes into play as a copy of that creature, except its name is still Sakashima the Impostor, it\'s still legendary, and it gains \"{2}{U}{U}: Return Sakashima the Impostor to its owner\'s hand at end of turn.\"').
card_first_print('sakashima the impostor', 'SOK').
card_image_name('sakashima the impostor'/'SOK', 'sakashima the impostor').
card_uid('sakashima the impostor'/'SOK', 'SOK:Sakashima the Impostor:sakashima the impostor').
card_rarity('sakashima the impostor'/'SOK', 'Rare').
card_artist('sakashima the impostor'/'SOK', 'rk post').
card_number('sakashima the impostor'/'SOK', '53').
card_multiverse_id('sakashima the impostor'/'SOK', '74509').

card_in_set('sakura-tribe scout', 'SOK').
card_original_type('sakura-tribe scout'/'SOK', 'Creature — Snake Shaman Scout').
card_original_text('sakura-tribe scout'/'SOK', '{T}: You may put a land card from your hand into play.').
card_first_print('sakura-tribe scout', 'SOK').
card_image_name('sakura-tribe scout'/'SOK', 'sakura-tribe scout').
card_uid('sakura-tribe scout'/'SOK', 'SOK:Sakura-Tribe Scout:sakura-tribe scout').
card_rarity('sakura-tribe scout'/'SOK', 'Common').
card_artist('sakura-tribe scout'/'SOK', 'Darrell Riche').
card_number('sakura-tribe scout'/'SOK', '144').
card_flavor_text('sakura-tribe scout'/'SOK', '\"Scouts of the Sakura Tribe spent two years wandering the forest to learn every leaf and tree. That knowledge was called upon during the Kami War to ensure the orochi\'s survival.\"\n—The History of Kamigawa').
card_multiverse_id('sakura-tribe scout'/'SOK', '74210').

card_in_set('sasaya\'s essence', 'SOK').
card_original_type('sasaya\'s essence'/'SOK', 'Legendary Creature — Snake Monk').
card_original_text('sasaya\'s essence'/'SOK', 'Reveal your hand: If you have seven or more land cards in your hand, flip Sasaya, Orochi Ascendant.\n-----\nSasaya\'s Essence\nLegendary Enchantment\nWhenever a land you control is tapped for mana, add one mana of that type to your mana pool for each other land you control with the same name.').
card_first_print('sasaya\'s essence', 'SOK').
card_image_name('sasaya\'s essence'/'SOK', 'sasaya\'s essence').
card_uid('sasaya\'s essence'/'SOK', 'SOK:Sasaya\'s Essence:sasaya\'s essence').
card_rarity('sasaya\'s essence'/'SOK', 'Rare').
card_artist('sasaya\'s essence'/'SOK', 'Christopher Moeller').
card_number('sasaya\'s essence'/'SOK', '145b').
card_multiverse_id('sasaya\'s essence'/'SOK', '87595').

card_in_set('sasaya, orochi ascendant', 'SOK').
card_original_type('sasaya, orochi ascendant'/'SOK', 'Legendary Creature — Snake Monk').
card_original_text('sasaya, orochi ascendant'/'SOK', 'Reveal your hand: If you have seven or more land cards in your hand, flip Sasaya, Orochi Ascendant.\n-----\nSasaya\'s Essence\nLegendary Enchantment\nWhenever a land you control is tapped for mana, add one mana of that type to your mana pool for each other land you control with the same name.').
card_first_print('sasaya, orochi ascendant', 'SOK').
card_image_name('sasaya, orochi ascendant'/'SOK', 'sasaya, orochi ascendant').
card_uid('sasaya, orochi ascendant'/'SOK', 'SOK:Sasaya, Orochi Ascendant:sasaya, orochi ascendant').
card_rarity('sasaya, orochi ascendant'/'SOK', 'Rare').
card_artist('sasaya, orochi ascendant'/'SOK', 'Christopher Moeller').
card_number('sasaya, orochi ascendant'/'SOK', '145a').
card_multiverse_id('sasaya, orochi ascendant'/'SOK', '87595').

card_in_set('scroll of origins', 'SOK').
card_original_type('scroll of origins'/'SOK', 'Artifact').
card_original_text('scroll of origins'/'SOK', '{2}, {T}: Draw a card if you have seven or more cards in hand.').
card_first_print('scroll of origins', 'SOK').
card_image_name('scroll of origins'/'SOK', 'scroll of origins').
card_uid('scroll of origins'/'SOK', 'SOK:Scroll of Origins:scroll of origins').
card_rarity('scroll of origins'/'SOK', 'Rare').
card_artist('scroll of origins'/'SOK', 'Dany Orizio').
card_number('scroll of origins'/'SOK', '159').
card_flavor_text('scroll of origins'/'SOK', 'The words from which language sprang, the knowledge from which thought was born, the story from which history was woven.').
card_multiverse_id('scroll of origins'/'SOK', '88811').

card_in_set('secretkeeper', 'SOK').
card_original_type('secretkeeper'/'SOK', 'Creature — Spirit').
card_original_text('secretkeeper'/'SOK', 'As long as you have more cards in hand than each opponent, Secretkeeper gets +2/+2 and has flying.').
card_first_print('secretkeeper', 'SOK').
card_image_name('secretkeeper'/'SOK', 'secretkeeper').
card_uid('secretkeeper'/'SOK', 'SOK:Secretkeeper:secretkeeper').
card_rarity('secretkeeper'/'SOK', 'Uncommon').
card_artist('secretkeeper'/'SOK', 'Ron Spencer').
card_number('secretkeeper'/'SOK', '54').
card_flavor_text('secretkeeper'/'SOK', 'It is all the things that humanity was not meant to know.').
card_multiverse_id('secretkeeper'/'SOK', '89398').

card_in_set('seed the land', 'SOK').
card_original_type('seed the land'/'SOK', 'Enchantment').
card_original_text('seed the land'/'SOK', 'Whenever a land comes into play, its controller puts a 1/1 green Snake creature token into play.').
card_first_print('seed the land', 'SOK').
card_image_name('seed the land'/'SOK', 'seed the land').
card_uid('seed the land'/'SOK', 'SOK:Seed the Land:seed the land').
card_rarity('seed the land'/'SOK', 'Rare').
card_artist('seed the land'/'SOK', 'Anthony S. Waters').
card_number('seed the land'/'SOK', '146').
card_flavor_text('seed the land'/'SOK', 'With a sadness and a sense of hope, Senka left the colony of her birth and, with her attendants, set out to serve as broodmistress, founder of a new colony.').
card_multiverse_id('seed the land'/'SOK', '74168').

card_in_set('seek the horizon', 'SOK').
card_original_type('seek the horizon'/'SOK', 'Sorcery').
card_original_text('seek the horizon'/'SOK', 'Search your library for up to three basic land cards, reveal them, and put them into your hand. Then shuffle your library.').
card_first_print('seek the horizon', 'SOK').
card_image_name('seek the horizon'/'SOK', 'seek the horizon').
card_uid('seek the horizon'/'SOK', 'SOK:Seek the Horizon:seek the horizon').
card_rarity('seek the horizon'/'SOK', 'Uncommon').
card_artist('seek the horizon'/'SOK', 'Eric Polak').
card_number('seek the horizon'/'SOK', '147').
card_flavor_text('seek the horizon'/'SOK', '\"I\'ve seen this great land from every angle. I know its every tree, stone, and river bend. Yet I have come to realize that knowing a thing is not the same as healing it.\"\n—Diary of Azusa').
card_multiverse_id('seek the horizon'/'SOK', '74056').

card_in_set('sekki, seasons\' guide', 'SOK').
card_original_type('sekki, seasons\' guide'/'SOK', 'Legendary Creature — Spirit').
card_original_text('sekki, seasons\' guide'/'SOK', 'Sekki, Seasons\' Guide comes into play with eight +1/+1 counters on it.\nIf damage would be dealt to Sekki, prevent that damage, remove that many +1/+1 counters from Sekki, and put that many 1/1 colorless Spirit creature tokens into play.\nSacrifice eight Spirits: Return Sekki from your graveyard to play.').
card_first_print('sekki, seasons\' guide', 'SOK').
card_image_name('sekki, seasons\' guide'/'SOK', 'sekki, seasons\' guide').
card_uid('sekki, seasons\' guide'/'SOK', 'SOK:Sekki, Seasons\' Guide:sekki, seasons\' guide').
card_rarity('sekki, seasons\' guide'/'SOK', 'Rare').
card_artist('sekki, seasons\' guide'/'SOK', 'Kev Walker').
card_number('sekki, seasons\' guide'/'SOK', '148').
card_multiverse_id('sekki, seasons\' guide'/'SOK', '74471').

card_in_set('shape stealer', 'SOK').
card_original_type('shape stealer'/'SOK', 'Creature — Shapeshifter Spirit').
card_original_text('shape stealer'/'SOK', 'Whenever Shape Stealer blocks or becomes blocked by a creature, change Shape Stealer\'s power and toughness to that creature\'s power and toughness until end of turn.').
card_first_print('shape stealer', 'SOK').
card_image_name('shape stealer'/'SOK', 'shape stealer').
card_uid('shape stealer'/'SOK', 'SOK:Shape Stealer:shape stealer').
card_rarity('shape stealer'/'SOK', 'Uncommon').
card_artist('shape stealer'/'SOK', 'Jim Nelson').
card_number('shape stealer'/'SOK', '55').
card_flavor_text('shape stealer'/'SOK', '\"Stand back. I know just what to do.\"\n—Kiki-Jiki, Mirror Breaker').
card_multiverse_id('shape stealer'/'SOK', '88823').

card_in_set('shifting borders', 'SOK').
card_original_type('shifting borders'/'SOK', 'Instant — Arcane').
card_original_text('shifting borders'/'SOK', 'Exchange control of two target lands.\nSplice onto Arcane {3}{U} (As you play an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_first_print('shifting borders', 'SOK').
card_image_name('shifting borders'/'SOK', 'shifting borders').
card_uid('shifting borders'/'SOK', 'SOK:Shifting Borders:shifting borders').
card_rarity('shifting borders'/'SOK', 'Uncommon').
card_artist('shifting borders'/'SOK', 'Alex Horley-Orlandelli').
card_number('shifting borders'/'SOK', '56').
card_multiverse_id('shifting borders'/'SOK', '74191').

card_in_set('shinen of fear\'s chill', 'SOK').
card_original_type('shinen of fear\'s chill'/'SOK', 'Creature — Spirit').
card_original_text('shinen of fear\'s chill'/'SOK', 'Shinen of Fear\'s Chill can\'t block.\nChannel {1}{B}, Discard Shinen of Fear\'s Chill: Target creature can\'t block this turn.').
card_first_print('shinen of fear\'s chill', 'SOK').
card_image_name('shinen of fear\'s chill'/'SOK', 'shinen of fear\'s chill').
card_uid('shinen of fear\'s chill'/'SOK', 'SOK:Shinen of Fear\'s Chill:shinen of fear\'s chill').
card_rarity('shinen of fear\'s chill'/'SOK', 'Common').
card_artist('shinen of fear\'s chill'/'SOK', 'Darrell Riche').
card_number('shinen of fear\'s chill'/'SOK', '88').
card_multiverse_id('shinen of fear\'s chill'/'SOK', '88812').

card_in_set('shinen of flight\'s wings', 'SOK').
card_original_type('shinen of flight\'s wings'/'SOK', 'Creature — Spirit').
card_original_text('shinen of flight\'s wings'/'SOK', 'Flying\nChannel {U}, Discard Shinen of Flight\'s Wings: Target creature gains flying until end of turn.').
card_first_print('shinen of flight\'s wings', 'SOK').
card_image_name('shinen of flight\'s wings'/'SOK', 'shinen of flight\'s wings').
card_uid('shinen of flight\'s wings'/'SOK', 'SOK:Shinen of Flight\'s Wings:shinen of flight\'s wings').
card_rarity('shinen of flight\'s wings'/'SOK', 'Common').
card_artist('shinen of flight\'s wings'/'SOK', 'Scott M. Fischer').
card_number('shinen of flight\'s wings'/'SOK', '57').
card_multiverse_id('shinen of flight\'s wings'/'SOK', '74088').

card_in_set('shinen of fury\'s fire', 'SOK').
card_original_type('shinen of fury\'s fire'/'SOK', 'Creature — Spirit').
card_original_text('shinen of fury\'s fire'/'SOK', 'Haste\nChannel {R}, Discard Shinen of Fury\'s Fire: Target creature gains haste until end of turn.').
card_first_print('shinen of fury\'s fire', 'SOK').
card_image_name('shinen of fury\'s fire'/'SOK', 'shinen of fury\'s fire').
card_uid('shinen of fury\'s fire'/'SOK', 'SOK:Shinen of Fury\'s Fire:shinen of fury\'s fire').
card_rarity('shinen of fury\'s fire'/'SOK', 'Common').
card_artist('shinen of fury\'s fire'/'SOK', 'Dan Frazier').
card_number('shinen of fury\'s fire'/'SOK', '112').
card_multiverse_id('shinen of fury\'s fire'/'SOK', '87339').

card_in_set('shinen of life\'s roar', 'SOK').
card_original_type('shinen of life\'s roar'/'SOK', 'Creature — Spirit').
card_original_text('shinen of life\'s roar'/'SOK', 'All creatures able to block Shinen of Life\'s Roar do so.\nChannel {2}{G}{G}, Discard Shinen of Life\'s Roar: All creatures able to block target creature this turn do so.').
card_first_print('shinen of life\'s roar', 'SOK').
card_image_name('shinen of life\'s roar'/'SOK', 'shinen of life\'s roar').
card_uid('shinen of life\'s roar'/'SOK', 'SOK:Shinen of Life\'s Roar:shinen of life\'s roar').
card_rarity('shinen of life\'s roar'/'SOK', 'Common').
card_artist('shinen of life\'s roar'/'SOK', 'Matt Cavotta').
card_number('shinen of life\'s roar'/'SOK', '149').
card_multiverse_id('shinen of life\'s roar'/'SOK', '74017').

card_in_set('shinen of stars\' light', 'SOK').
card_original_type('shinen of stars\' light'/'SOK', 'Creature — Spirit').
card_original_text('shinen of stars\' light'/'SOK', 'First strike\nChannel {1}{W}, Discard Shinen of Stars\' Light: Target creature gains first strike until end of turn.').
card_first_print('shinen of stars\' light', 'SOK').
card_image_name('shinen of stars\' light'/'SOK', 'shinen of stars\' light').
card_uid('shinen of stars\' light'/'SOK', 'SOK:Shinen of Stars\' Light:shinen of stars\' light').
card_rarity('shinen of stars\' light'/'SOK', 'Common').
card_artist('shinen of stars\' light'/'SOK', 'Jeremy Jarvis').
card_number('shinen of stars\' light'/'SOK', '28').
card_multiverse_id('shinen of stars\' light'/'SOK', '74361').

card_in_set('sink into takenuma', 'SOK').
card_original_type('sink into takenuma'/'SOK', 'Sorcery — Arcane').
card_original_text('sink into takenuma'/'SOK', 'Sweep Return any number of Swamps you control to their owner\'s hand. Target player discards a card for each Swamp returned this way.').
card_first_print('sink into takenuma', 'SOK').
card_image_name('sink into takenuma'/'SOK', 'sink into takenuma').
card_uid('sink into takenuma'/'SOK', 'SOK:Sink into Takenuma:sink into takenuma').
card_rarity('sink into takenuma'/'SOK', 'Common').
card_artist('sink into takenuma'/'SOK', 'Pat Lee').
card_number('sink into takenuma'/'SOK', '89').
card_multiverse_id('sink into takenuma'/'SOK', '84717').

card_in_set('skull collector', 'SOK').
card_original_type('skull collector'/'SOK', 'Creature — Ogre Warrior').
card_original_text('skull collector'/'SOK', 'At the beginning of your upkeep, return a black creature you control to its owner\'s hand.\n{1}{B}: Regenerate Skull Collector.').
card_first_print('skull collector', 'SOK').
card_image_name('skull collector'/'SOK', 'skull collector').
card_uid('skull collector'/'SOK', 'SOK:Skull Collector:skull collector').
card_rarity('skull collector'/'SOK', 'Uncommon').
card_artist('skull collector'/'SOK', 'Thomas M. Baxa').
card_number('skull collector'/'SOK', '90').
card_flavor_text('skull collector'/'SOK', '\"Your blood I\'ll use. Your skull I\'ll keep.\"').
card_multiverse_id('skull collector'/'SOK', '88797').

card_in_set('skyfire kirin', 'SOK').
card_original_type('skyfire kirin'/'SOK', 'Legendary Creature — Kirin Spirit').
card_original_text('skyfire kirin'/'SOK', 'Flying\nWhenever you play a Spirit or Arcane spell, you may gain control of target creature with that spell\'s converted mana cost until end of turn.').
card_first_print('skyfire kirin', 'SOK').
card_image_name('skyfire kirin'/'SOK', 'skyfire kirin').
card_uid('skyfire kirin'/'SOK', 'SOK:Skyfire Kirin:skyfire kirin').
card_rarity('skyfire kirin'/'SOK', 'Rare').
card_artist('skyfire kirin'/'SOK', 'Tsutomu Kawade').
card_number('skyfire kirin'/'SOK', '113').
card_flavor_text('skyfire kirin'/'SOK', 'Its fiery visitation burns away the fog of war and dissipates the ties of loyalty.').
card_multiverse_id('skyfire kirin'/'SOK', '88793').

card_in_set('sokenzan renegade', 'SOK').
card_original_type('sokenzan renegade'/'SOK', 'Creature — Ogre Samurai Mercenary').
card_original_text('sokenzan renegade'/'SOK', 'Bushido 1 (When this blocks or becomes blocked, it gets +1/+1 until end of turn.)\nAt the beginning of your upkeep, if a player has more cards in hand than any other, the player with the most cards in hand gains control of Sokenzan Renegade.').
card_first_print('sokenzan renegade', 'SOK').
card_image_name('sokenzan renegade'/'SOK', 'sokenzan renegade').
card_uid('sokenzan renegade'/'SOK', 'SOK:Sokenzan Renegade:sokenzan renegade').
card_rarity('sokenzan renegade'/'SOK', 'Uncommon').
card_artist('sokenzan renegade'/'SOK', 'Alan Pollack').
card_number('sokenzan renegade'/'SOK', '114').
card_multiverse_id('sokenzan renegade'/'SOK', '74133').

card_in_set('sokenzan spellblade', 'SOK').
card_original_type('sokenzan spellblade'/'SOK', 'Creature — Ogre Samurai Shaman').
card_original_text('sokenzan spellblade'/'SOK', 'Bushido 1 (When this blocks or becomes blocked, it gets +1/+1 until end of turn.)\n{1}{R}: Sokenzan Spellblade gets +X/+0 until end of turn, where X is the number of cards in your hand.').
card_first_print('sokenzan spellblade', 'SOK').
card_image_name('sokenzan spellblade'/'SOK', 'sokenzan spellblade').
card_uid('sokenzan spellblade'/'SOK', 'SOK:Sokenzan Spellblade:sokenzan spellblade').
card_rarity('sokenzan spellblade'/'SOK', 'Common').
card_artist('sokenzan spellblade'/'SOK', 'Carl Critchlow').
card_number('sokenzan spellblade'/'SOK', '115').
card_multiverse_id('sokenzan spellblade'/'SOK', '74398').

card_in_set('soramaro, first to dream', 'SOK').
card_original_type('soramaro, first to dream'/'SOK', 'Legendary Creature — Spirit').
card_original_text('soramaro, first to dream'/'SOK', 'Flying\nSoramaro, First to Dream\'s power and toughness are each equal to the number of cards in your hand.\n{4}, Return a land you control to its owner\'s hand: Draw a card.').
card_first_print('soramaro, first to dream', 'SOK').
card_image_name('soramaro, first to dream'/'SOK', 'soramaro, first to dream').
card_uid('soramaro, first to dream'/'SOK', 'SOK:Soramaro, First to Dream:soramaro, first to dream').
card_rarity('soramaro, first to dream'/'SOK', 'Rare').
card_artist('soramaro, first to dream'/'SOK', 'Brian Despain').
card_number('soramaro, first to dream'/'SOK', '58').
card_multiverse_id('soramaro, first to dream'/'SOK', '74170').

card_in_set('soratami cloud chariot', 'SOK').
card_original_type('soratami cloud chariot'/'SOK', 'Artifact').
card_original_text('soratami cloud chariot'/'SOK', '{2}: Target creature you control gains flying until end of turn.\n{2}: Prevent all combat damage that would be dealt to and dealt by target creature you control this turn.').
card_first_print('soratami cloud chariot', 'SOK').
card_image_name('soratami cloud chariot'/'SOK', 'soratami cloud chariot').
card_uid('soratami cloud chariot'/'SOK', 'SOK:Soratami Cloud Chariot:soratami cloud chariot').
card_rarity('soratami cloud chariot'/'SOK', 'Uncommon').
card_artist('soratami cloud chariot'/'SOK', 'Franz Vohwinkel').
card_number('soratami cloud chariot'/'SOK', '160').
card_multiverse_id('soratami cloud chariot'/'SOK', '74174').

card_in_set('spiraling embers', 'SOK').
card_original_type('spiraling embers'/'SOK', 'Sorcery — Arcane').
card_original_text('spiraling embers'/'SOK', 'Spiraling Embers deals damage to target creature or player equal to the number of cards in your hand.').
card_first_print('spiraling embers', 'SOK').
card_image_name('spiraling embers'/'SOK', 'spiraling embers').
card_uid('spiraling embers'/'SOK', 'SOK:Spiraling Embers:spiraling embers').
card_rarity('spiraling embers'/'SOK', 'Common').
card_artist('spiraling embers'/'SOK', 'Chippy').
card_number('spiraling embers'/'SOK', '116').
card_flavor_text('spiraling embers'/'SOK', 'The akki knew to stay indoors on the days the oni flew their kites.').
card_multiverse_id('spiraling embers'/'SOK', '74503').

card_in_set('spiritual visit', 'SOK').
card_original_type('spiritual visit'/'SOK', 'Instant — Arcane').
card_original_text('spiritual visit'/'SOK', 'Put a 1/1 colorless Spirit creature token into play.\nSplice onto Arcane {W} (As you play an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_first_print('spiritual visit', 'SOK').
card_image_name('spiritual visit'/'SOK', 'spiritual visit').
card_uid('spiritual visit'/'SOK', 'SOK:Spiritual Visit:spiritual visit').
card_rarity('spiritual visit'/'SOK', 'Common').
card_artist('spiritual visit'/'SOK', 'Pete Venters').
card_number('spiritual visit'/'SOK', '29').
card_multiverse_id('spiritual visit'/'SOK', '74372').

card_in_set('stampeding serow', 'SOK').
card_original_type('stampeding serow'/'SOK', 'Creature — Beast').
card_original_text('stampeding serow'/'SOK', 'Trample\nAt the beginning of your upkeep, return a green creature you control to its owner\'s hand.').
card_first_print('stampeding serow', 'SOK').
card_image_name('stampeding serow'/'SOK', 'stampeding serow').
card_uid('stampeding serow'/'SOK', 'SOK:Stampeding Serow:stampeding serow').
card_rarity('stampeding serow'/'SOK', 'Uncommon').
card_artist('stampeding serow'/'SOK', 'Edward P. Beard, Jr.').
card_number('stampeding serow'/'SOK', '150').
card_multiverse_id('stampeding serow'/'SOK', '74407').

card_in_set('sunder from within', 'SOK').
card_original_type('sunder from within'/'SOK', 'Sorcery — Arcane').
card_original_text('sunder from within'/'SOK', 'Destroy target artifact or land.').
card_first_print('sunder from within', 'SOK').
card_image_name('sunder from within'/'SOK', 'sunder from within').
card_uid('sunder from within'/'SOK', 'SOK:Sunder from Within:sunder from within').
card_rarity('sunder from within'/'SOK', 'Uncommon').
card_artist('sunder from within'/'SOK', 'Jeff Miracola').
card_number('sunder from within'/'SOK', '117').
card_flavor_text('sunder from within'/'SOK', 'Trapped for eons within the sacred gong, the kami boomed and clanged to a deafening tempo as they destroyed their erstwhile prison.').
card_multiverse_id('sunder from within'/'SOK', '88808').

card_in_set('thoughts of ruin', 'SOK').
card_original_type('thoughts of ruin'/'SOK', 'Sorcery').
card_original_text('thoughts of ruin'/'SOK', 'Each player sacrifices a land for each card in your hand.').
card_first_print('thoughts of ruin', 'SOK').
card_image_name('thoughts of ruin'/'SOK', 'thoughts of ruin').
card_uid('thoughts of ruin'/'SOK', 'SOK:Thoughts of Ruin:thoughts of ruin').
card_rarity('thoughts of ruin'/'SOK', 'Rare').
card_artist('thoughts of ruin'/'SOK', 'John Avon').
card_number('thoughts of ruin'/'SOK', '118').
card_flavor_text('thoughts of ruin'/'SOK', '\"In our war with the kami, we annihilate what is our own. Do they too suffer ruination in their hidden world?\"\n—Diary of Azusa').
card_multiverse_id('thoughts of ruin'/'SOK', '74374').

card_in_set('tomb of urami', 'SOK').
card_original_type('tomb of urami'/'SOK', 'Legendary Land').
card_original_text('tomb of urami'/'SOK', '{T}: Add {B} to your mana pool. Tomb of Urami deals 1 damage to you if you don\'t control an Ogre.\n{2}{B}{B}, {T}, Sacrifice all lands you control: Put a legendary 5/5 black Demon Spirit creature token with flying named Urami into play.').
card_first_print('tomb of urami', 'SOK').
card_image_name('tomb of urami'/'SOK', 'tomb of urami').
card_uid('tomb of urami'/'SOK', 'SOK:Tomb of Urami:tomb of urami').
card_rarity('tomb of urami'/'SOK', 'Rare').
card_artist('tomb of urami'/'SOK', 'Alex Horley-Orlandelli').
card_number('tomb of urami'/'SOK', '165').
card_multiverse_id('tomb of urami'/'SOK', '74492').

card_in_set('torii watchward', 'SOK').
card_original_type('torii watchward'/'SOK', 'Creature — Spirit').
card_original_text('torii watchward'/'SOK', 'Vigilance (Attacking doesn\'t cause this creature to tap.)\nSoulshift 4 (When this is put into a graveyard from play, you may return target Spirit card with converted mana cost 4 or less from your graveyard to your hand.)').
card_first_print('torii watchward', 'SOK').
card_image_name('torii watchward'/'SOK', 'torii watchward').
card_uid('torii watchward'/'SOK', 'SOK:Torii Watchward:torii watchward').
card_rarity('torii watchward'/'SOK', 'Common').
card_artist('torii watchward'/'SOK', 'Jeff Miracola').
card_number('torii watchward'/'SOK', '30').
card_multiverse_id('torii watchward'/'SOK', '87342').

card_in_set('trusted advisor', 'SOK').
card_original_type('trusted advisor'/'SOK', 'Creature — Human Advisor').
card_original_text('trusted advisor'/'SOK', 'Your maximum hand size is increased by two.\nAt the beginning of your upkeep, return a blue creature you control to its owner\'s hand.').
card_first_print('trusted advisor', 'SOK').
card_image_name('trusted advisor'/'SOK', 'trusted advisor').
card_uid('trusted advisor'/'SOK', 'SOK:Trusted Advisor:trusted advisor').
card_rarity('trusted advisor'/'SOK', 'Uncommon').
card_artist('trusted advisor'/'SOK', 'Jim Nelson').
card_number('trusted advisor'/'SOK', '59').
card_flavor_text('trusted advisor'/'SOK', 'Some of Minamo\'s students claimed he was as old as the knowledge he dispensed.').
card_multiverse_id('trusted advisor'/'SOK', '88804').

card_in_set('twincast', 'SOK').
card_original_type('twincast'/'SOK', 'Instant').
card_original_text('twincast'/'SOK', 'Copy target instant or sorcery spell. You may choose new targets for the copy.').
card_first_print('twincast', 'SOK').
card_image_name('twincast'/'SOK', 'twincast').
card_uid('twincast'/'SOK', 'SOK:Twincast:twincast').
card_rarity('twincast'/'SOK', 'Rare').
card_artist('twincast'/'SOK', 'Christopher Moeller').
card_number('twincast'/'SOK', '60').
card_flavor_text('twincast'/'SOK', 'Sometimes you have to search for inspiration, and sometimes it\'s right there in front of you.').
card_multiverse_id('twincast'/'SOK', '80412').

card_in_set('undying flames', 'SOK').
card_original_type('undying flames'/'SOK', 'Sorcery').
card_original_text('undying flames'/'SOK', 'Remove cards from the top of your library from the game until you remove a nonland card. Undying Flames deals damage to target creature or player equal to that card\'s converted mana cost.\nEpic (For the rest of the game, you can\'t play spells. At the beginning of each of your upkeeps, copy this spell except for its epic ability. You may choose a new target for the copy.)').
card_first_print('undying flames', 'SOK').
card_image_name('undying flames'/'SOK', 'undying flames').
card_uid('undying flames'/'SOK', 'SOK:Undying Flames:undying flames').
card_rarity('undying flames'/'SOK', 'Rare').
card_artist('undying flames'/'SOK', 'Tsutomu Kawade').
card_number('undying flames'/'SOK', '119').
card_multiverse_id('undying flames'/'SOK', '87602').

card_in_set('wine of blood and iron', 'SOK').
card_original_type('wine of blood and iron'/'SOK', 'Artifact').
card_original_text('wine of blood and iron'/'SOK', '{4}: Target creature gets +X/+0 until end of turn, where X is its power. Sacrifice Wine of Blood and Iron at end of turn.').
card_first_print('wine of blood and iron', 'SOK').
card_image_name('wine of blood and iron'/'SOK', 'wine of blood and iron').
card_uid('wine of blood and iron'/'SOK', 'SOK:Wine of Blood and Iron:wine of blood and iron').
card_rarity('wine of blood and iron'/'SOK', 'Rare').
card_artist('wine of blood and iron'/'SOK', 'Luca Zontini').
card_number('wine of blood and iron'/'SOK', '161').
card_flavor_text('wine of blood and iron'/'SOK', 'Godo never revealed what went into his special brew, but he always had more of it after great victories.').
card_multiverse_id('wine of blood and iron'/'SOK', '74130').

card_in_set('yuki-onna', 'SOK').
card_original_type('yuki-onna'/'SOK', 'Creature — Spirit').
card_original_text('yuki-onna'/'SOK', 'When Yuki-Onna comes into play, destroy target artifact.\nWhenever you play a Spirit or Arcane spell, you may return Yuki-Onna to its owner\'s hand.').
card_first_print('yuki-onna', 'SOK').
card_image_name('yuki-onna'/'SOK', 'yuki-onna').
card_uid('yuki-onna'/'SOK', 'SOK:Yuki-Onna:yuki-onna').
card_rarity('yuki-onna'/'SOK', 'Uncommon').
card_artist('yuki-onna'/'SOK', 'Hideaki Takamura').
card_number('yuki-onna'/'SOK', '120').
card_multiverse_id('yuki-onna'/'SOK', '84712').
