% Odyssey

set('ODY').
set_name('ODY', 'Odyssey').
set_release_date('ODY', '2001-10-01').
set_border('ODY', 'black').
set_type('ODY', 'expansion').
set_block('ODY', 'Odyssey').

card_in_set('abandoned outpost', 'ODY').
card_original_type('abandoned outpost'/'ODY', 'Land').
card_original_text('abandoned outpost'/'ODY', 'Abandoned Outpost comes into play tapped.\n{T}: Add {W} to your mana pool.\n{T}, Sacrifice Abandoned Outpost: Add one mana of any color to your mana pool.').
card_first_print('abandoned outpost', 'ODY').
card_image_name('abandoned outpost'/'ODY', 'abandoned outpost').
card_uid('abandoned outpost'/'ODY', 'ODY:Abandoned Outpost:abandoned outpost').
card_rarity('abandoned outpost'/'ODY', 'Common').
card_artist('abandoned outpost'/'ODY', 'Edward P. Beard, Jr.').
card_number('abandoned outpost'/'ODY', '312').
card_multiverse_id('abandoned outpost'/'ODY', '31761').

card_in_set('aboshan\'s desire', 'ODY').
card_original_type('aboshan\'s desire'/'ODY', 'Enchant Creature').
card_original_text('aboshan\'s desire'/'ODY', 'Enchanted creature has flying.\nThreshold Enchanted creature can\'t be the target of spells or abilities. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('aboshan\'s desire', 'ODY').
card_image_name('aboshan\'s desire'/'ODY', 'aboshan\'s desire').
card_uid('aboshan\'s desire'/'ODY', 'ODY:Aboshan\'s Desire:aboshan\'s desire').
card_rarity('aboshan\'s desire'/'ODY', 'Common').
card_artist('aboshan\'s desire'/'ODY', 'Ciruelo').
card_number('aboshan\'s desire'/'ODY', '59').
card_multiverse_id('aboshan\'s desire'/'ODY', '29728').

card_in_set('aboshan, cephalid emperor', 'ODY').
card_original_type('aboshan, cephalid emperor'/'ODY', 'Creature — Cephalid Legend').
card_original_text('aboshan, cephalid emperor'/'ODY', 'Tap an untapped Cephalid you control: Tap target permanent.\n{U}{U}{U}: Tap all creatures without flying.').
card_first_print('aboshan, cephalid emperor', 'ODY').
card_image_name('aboshan, cephalid emperor'/'ODY', 'aboshan, cephalid emperor').
card_uid('aboshan, cephalid emperor'/'ODY', 'ODY:Aboshan, Cephalid Emperor:aboshan, cephalid emperor').
card_rarity('aboshan, cephalid emperor'/'ODY', 'Rare').
card_artist('aboshan, cephalid emperor'/'ODY', 'Christopher Moeller').
card_number('aboshan, cephalid emperor'/'ODY', '58').
card_flavor_text('aboshan, cephalid emperor'/'ODY', '\"No one can fight the tide forever.\"').
card_multiverse_id('aboshan, cephalid emperor'/'ODY', '31821').

card_in_set('acceptable losses', 'ODY').
card_original_type('acceptable losses'/'ODY', 'Sorcery').
card_original_text('acceptable losses'/'ODY', 'As an additional cost to play Acceptable Losses, discard a card at random from your hand.\nAcceptable Losses deals 5 damage to target creature.').
card_first_print('acceptable losses', 'ODY').
card_image_name('acceptable losses'/'ODY', 'acceptable losses').
card_uid('acceptable losses'/'ODY', 'ODY:Acceptable Losses:acceptable losses').
card_rarity('acceptable losses'/'ODY', 'Common').
card_artist('acceptable losses'/'ODY', 'Mike Ploog').
card_number('acceptable losses'/'ODY', '172').
card_multiverse_id('acceptable losses'/'ODY', '30692').

card_in_set('aegis of honor', 'ODY').
card_original_type('aegis of honor'/'ODY', 'Enchantment').
card_original_text('aegis of honor'/'ODY', '{1}: The next time an instant or sorcery spell would deal damage to you this turn, that spell deals that damage to its controller instead.').
card_first_print('aegis of honor', 'ODY').
card_image_name('aegis of honor'/'ODY', 'aegis of honor').
card_uid('aegis of honor'/'ODY', 'ODY:Aegis of Honor:aegis of honor').
card_rarity('aegis of honor'/'ODY', 'Rare').
card_artist('aegis of honor'/'ODY', 'Ron Spears').
card_number('aegis of honor'/'ODY', '1').
card_multiverse_id('aegis of honor'/'ODY', '29922').

card_in_set('æther burst', 'ODY').
card_original_type('æther burst'/'ODY', 'Instant').
card_original_text('æther burst'/'ODY', 'Return up to X target creatures to their owners\' hands, where X is one plus the number of Æther Burst cards in all graveyards as you play Æther Burst.').
card_first_print('æther burst', 'ODY').
card_image_name('æther burst'/'ODY', 'aether burst').
card_uid('æther burst'/'ODY', 'ODY:Æther Burst:aether burst').
card_rarity('æther burst'/'ODY', 'Common').
card_artist('æther burst'/'ODY', 'Adam Rex').
card_number('æther burst'/'ODY', '60').
card_flavor_text('æther burst'/'ODY', 'As fleeting as a cephalid\'s promise.').
card_multiverse_id('æther burst'/'ODY', '29725').

card_in_set('afflict', 'ODY').
card_original_type('afflict'/'ODY', 'Instant').
card_original_text('afflict'/'ODY', 'Target creature gets -1/-1 until end of turn.\nDraw a card.').
card_first_print('afflict', 'ODY').
card_image_name('afflict'/'ODY', 'afflict').
card_uid('afflict'/'ODY', 'ODY:Afflict:afflict').
card_rarity('afflict'/'ODY', 'Common').
card_artist('afflict'/'ODY', 'Roger Raupp').
card_number('afflict'/'ODY', '115').
card_flavor_text('afflict'/'ODY', 'When one of the Patriarch\'s champions displeased him, he would only shake his finger. That was enough.').
card_multiverse_id('afflict'/'ODY', '29740').

card_in_set('amugaba', 'ODY').
card_original_type('amugaba'/'ODY', 'Creature — Illusion').
card_original_text('amugaba'/'ODY', 'Flying\n{2}{U}, Discard a card from your hand: Return Amugaba to its owner\'s hand.').
card_first_print('amugaba', 'ODY').
card_image_name('amugaba'/'ODY', 'amugaba').
card_uid('amugaba'/'ODY', 'ODY:Amugaba:amugaba').
card_rarity('amugaba'/'ODY', 'Rare').
card_artist('amugaba'/'ODY', 'Heather Hudson').
card_number('amugaba'/'ODY', '61').
card_flavor_text('amugaba'/'ODY', 'The aven claim it feeds on thunder and rainbows.').
card_multiverse_id('amugaba'/'ODY', '29929').

card_in_set('anarchist', 'ODY').
card_original_type('anarchist'/'ODY', 'Creature — Townsfolk').
card_original_text('anarchist'/'ODY', 'When Anarchist comes into play, you may return target sorcery card from your graveyard to your hand.').
card_image_name('anarchist'/'ODY', 'anarchist').
card_uid('anarchist'/'ODY', 'ODY:Anarchist:anarchist').
card_rarity('anarchist'/'ODY', 'Common').
card_artist('anarchist'/'ODY', 'Greg & Tim Hildebrandt').
card_number('anarchist'/'ODY', '173').
card_flavor_text('anarchist'/'ODY', 'Those who will not follow are doomed to lead.').
card_multiverse_id('anarchist'/'ODY', '29758').

card_in_set('ancestral tribute', 'ODY').
card_original_type('ancestral tribute'/'ODY', 'Sorcery').
card_original_text('ancestral tribute'/'ODY', 'You gain 2 life for each card in your graveyard.\nFlashback {9}{W}{W}{W} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('ancestral tribute', 'ODY').
card_image_name('ancestral tribute'/'ODY', 'ancestral tribute').
card_uid('ancestral tribute'/'ODY', 'ODY:Ancestral Tribute:ancestral tribute').
card_rarity('ancestral tribute'/'ODY', 'Rare').
card_artist('ancestral tribute'/'ODY', 'Edward P. Beard, Jr.').
card_number('ancestral tribute'/'ODY', '2').
card_multiverse_id('ancestral tribute'/'ODY', '30555').

card_in_set('angelic wall', 'ODY').
card_original_type('angelic wall'/'ODY', 'Creature — Wall').
card_original_text('angelic wall'/'ODY', '(Walls can\'t attack.)\nFlying').
card_image_name('angelic wall'/'ODY', 'angelic wall').
card_uid('angelic wall'/'ODY', 'ODY:Angelic Wall:angelic wall').
card_rarity('angelic wall'/'ODY', 'Common').
card_artist('angelic wall'/'ODY', 'John Avon').
card_number('angelic wall'/'ODY', '3').
card_flavor_text('angelic wall'/'ODY', '\"The Ancestor protects us in ways we can\'t begin to comprehend.\"\n—Mystic elder').
card_multiverse_id('angelic wall'/'ODY', '29699').

card_in_set('animal boneyard', 'ODY').
card_original_type('animal boneyard'/'ODY', 'Enchant Land').
card_original_text('animal boneyard'/'ODY', 'Enchanted land has \"{T}, Sacrifice a creature: You gain life equal to that creature\'s toughness.\"').
card_first_print('animal boneyard', 'ODY').
card_image_name('animal boneyard'/'ODY', 'animal boneyard').
card_uid('animal boneyard'/'ODY', 'ODY:Animal Boneyard:animal boneyard').
card_rarity('animal boneyard'/'ODY', 'Uncommon').
card_artist('animal boneyard'/'ODY', 'Edward P. Beard, Jr.').
card_number('animal boneyard'/'ODY', '4').
card_flavor_text('animal boneyard'/'ODY', '\"The animal spirits are the Ancestor\'s grandchildren. Remember them.\"\n—Mystic elder').
card_multiverse_id('animal boneyard'/'ODY', '31839').

card_in_set('ashen firebeast', 'ODY').
card_original_type('ashen firebeast'/'ODY', 'Creature — Beast').
card_original_text('ashen firebeast'/'ODY', '{1}{R}: Ashen Firebeast deals 1 damage to each creature without flying.').
card_first_print('ashen firebeast', 'ODY').
card_image_name('ashen firebeast'/'ODY', 'ashen firebeast').
card_uid('ashen firebeast'/'ODY', 'ODY:Ashen Firebeast:ashen firebeast').
card_rarity('ashen firebeast'/'ODY', 'Rare').
card_artist('ashen firebeast'/'ODY', 'Mark Tedin').
card_number('ashen firebeast'/'ODY', '174').
card_flavor_text('ashen firebeast'/'ODY', '\"I\'m not sure which impresses me most, its thoroughness or its intensity.\"\n—Matoc, lavamancer').
card_multiverse_id('ashen firebeast'/'ODY', '29965').

card_in_set('atogatog', 'ODY').
card_original_type('atogatog'/'ODY', 'Creature — Atog Legend').
card_original_text('atogatog'/'ODY', 'Sacrifice an Atog: Atogatog gets +X/+X until end of turn, where X is the sacrificed Atog\'s power.').
card_first_print('atogatog', 'ODY').
card_image_name('atogatog'/'ODY', 'atogatog').
card_uid('atogatog'/'ODY', 'ODY:Atogatog:atogatog').
card_rarity('atogatog'/'ODY', 'Rare').
card_artist('atogatog'/'ODY', 'Ron Spears').
card_number('atogatog'/'ODY', '286').
card_flavor_text('atogatog'/'ODY', 'It relishes old-fashioned family meals.').
card_multiverse_id('atogatog'/'ODY', '31834').

card_in_set('aura graft', 'ODY').
card_original_type('aura graft'/'ODY', 'Instant').
card_original_text('aura graft'/'ODY', 'Move target enchantment that\'s enchanting a permanent to another permanent it can enchant. Gain control of that enchantment. (This effect doesn\'t end at end of turn.)').
card_first_print('aura graft', 'ODY').
card_image_name('aura graft'/'ODY', 'aura graft').
card_uid('aura graft'/'ODY', 'ODY:Aura Graft:aura graft').
card_rarity('aura graft'/'ODY', 'Uncommon').
card_artist('aura graft'/'ODY', 'Ray Lago').
card_number('aura graft'/'ODY', '62').
card_flavor_text('aura graft'/'ODY', '\"It\'s not really stealing. It\'s more like extended borrowing.\"').
card_multiverse_id('aura graft'/'ODY', '29938').

card_in_set('auramancer', 'ODY').
card_original_type('auramancer'/'ODY', 'Creature — Wizard').
card_original_text('auramancer'/'ODY', 'When Auramancer comes into play, you may return target enchantment card from your graveyard to your hand.').
card_first_print('auramancer', 'ODY').
card_image_name('auramancer'/'ODY', 'auramancer').
card_uid('auramancer'/'ODY', 'ODY:Auramancer:auramancer').
card_rarity('auramancer'/'ODY', 'Common').
card_artist('auramancer'/'ODY', 'Rebecca Guay').
card_number('auramancer'/'ODY', '5').
card_flavor_text('auramancer'/'ODY', 'Beauty stirs the memory like a sweet perfume excites the air.').
card_multiverse_id('auramancer'/'ODY', '29700').

card_in_set('aven archer', 'ODY').
card_original_type('aven archer'/'ODY', 'Creature — Bird Soldier').
card_original_text('aven archer'/'ODY', 'Flying\n{2}{W}, {T}: Aven Archer deals 2 damage to target attacking or blocking creature.').
card_first_print('aven archer', 'ODY').
card_image_name('aven archer'/'ODY', 'aven archer').
card_uid('aven archer'/'ODY', 'ODY:Aven Archer:aven archer').
card_rarity('aven archer'/'ODY', 'Uncommon').
card_artist('aven archer'/'ODY', 'Mark Zug').
card_number('aven archer'/'ODY', '6').
card_flavor_text('aven archer'/'ODY', 'Aimed by the eyes of eagles, their arrows never miss the mark.').
card_multiverse_id('aven archer'/'ODY', '29691').

card_in_set('aven cloudchaser', 'ODY').
card_original_type('aven cloudchaser'/'ODY', 'Creature — Bird Soldier').
card_original_text('aven cloudchaser'/'ODY', 'Flying\nWhen Aven Cloudchaser comes into play, destroy target enchantment.').
card_first_print('aven cloudchaser', 'ODY').
card_image_name('aven cloudchaser'/'ODY', 'aven cloudchaser').
card_uid('aven cloudchaser'/'ODY', 'ODY:Aven Cloudchaser:aven cloudchaser').
card_rarity('aven cloudchaser'/'ODY', 'Common').
card_artist('aven cloudchaser'/'ODY', 'Justin Sweet').
card_number('aven cloudchaser'/'ODY', '7').
card_flavor_text('aven cloudchaser'/'ODY', '\"At the Reapportionment, Eagle begged to be human. The Ancestor granted half that prayer.\"\n—Nomad myth').
card_multiverse_id('aven cloudchaser'/'ODY', '29694').

card_in_set('aven fisher', 'ODY').
card_original_type('aven fisher'/'ODY', 'Creature — Bird Soldier').
card_original_text('aven fisher'/'ODY', 'Flying\nWhen Aven Fisher is put into a graveyard from play, you may draw a card.').
card_first_print('aven fisher', 'ODY').
card_image_name('aven fisher'/'ODY', 'aven fisher').
card_uid('aven fisher'/'ODY', 'ODY:Aven Fisher:aven fisher').
card_rarity('aven fisher'/'ODY', 'Common').
card_artist('aven fisher'/'ODY', 'Christopher Moeller').
card_number('aven fisher'/'ODY', '63').
card_flavor_text('aven fisher'/'ODY', 'The same spears that catch their food today will defend their homes tomorrow.').
card_multiverse_id('aven fisher'/'ODY', '31815').

card_in_set('aven flock', 'ODY').
card_original_type('aven flock'/'ODY', 'Creature — Bird Soldier').
card_original_text('aven flock'/'ODY', 'Flying\n{W}: Aven Flock gets +0/+1 until end of turn.').
card_first_print('aven flock', 'ODY').
card_image_name('aven flock'/'ODY', 'aven flock').
card_uid('aven flock'/'ODY', 'ODY:Aven Flock:aven flock').
card_rarity('aven flock'/'ODY', 'Common').
card_artist('aven flock'/'ODY', 'Greg & Tim Hildebrandt').
card_number('aven flock'/'ODY', '8').
card_flavor_text('aven flock'/'ODY', 'Just as each added feather steadies the wing, so does the flock grow stronger with each new member.').
card_multiverse_id('aven flock'/'ODY', '31767').

card_in_set('aven shrine', 'ODY').
card_original_type('aven shrine'/'ODY', 'Enchantment').
card_original_text('aven shrine'/'ODY', 'Whenever a player plays a spell, that player gains X life, where X is the number of cards in all graveyards with the same name as that spell.').
card_first_print('aven shrine', 'ODY').
card_image_name('aven shrine'/'ODY', 'aven shrine').
card_uid('aven shrine'/'ODY', 'ODY:Aven Shrine:aven shrine').
card_rarity('aven shrine'/'ODY', 'Rare').
card_artist('aven shrine'/'ODY', 'Wayne England').
card_number('aven shrine'/'ODY', '9').
card_multiverse_id('aven shrine'/'ODY', '29923').

card_in_set('aven smokeweaver', 'ODY').
card_original_type('aven smokeweaver'/'ODY', 'Creature — Bird Soldier').
card_original_text('aven smokeweaver'/'ODY', 'Flying, protection from red').
card_first_print('aven smokeweaver', 'ODY').
card_image_name('aven smokeweaver'/'ODY', 'aven smokeweaver').
card_uid('aven smokeweaver'/'ODY', 'ODY:Aven Smokeweaver:aven smokeweaver').
card_rarity('aven smokeweaver'/'ODY', 'Uncommon').
card_artist('aven smokeweaver'/'ODY', 'Kev Walker').
card_number('aven smokeweaver'/'ODY', '64').
card_flavor_text('aven smokeweaver'/'ODY', 'In a world still pained from burns, some are born already scarred.').
card_multiverse_id('aven smokeweaver'/'ODY', '29709').

card_in_set('aven windreader', 'ODY').
card_original_type('aven windreader'/'ODY', 'Creature — Bird Soldier Wizard').
card_original_text('aven windreader'/'ODY', 'Flying\n{1}{U}: Target player reveals the top card of his or her library.').
card_first_print('aven windreader', 'ODY').
card_image_name('aven windreader'/'ODY', 'aven windreader').
card_uid('aven windreader'/'ODY', 'ODY:Aven Windreader:aven windreader').
card_rarity('aven windreader'/'ODY', 'Common').
card_artist('aven windreader'/'ODY', 'Greg & Tim Hildebrandt').
card_number('aven windreader'/'ODY', '65').
card_flavor_text('aven windreader'/'ODY', '\"The tiniest ripple tells a story ten fathoms deep.\"').
card_multiverse_id('aven windreader'/'ODY', '29716').

card_in_set('balancing act', 'ODY').
card_original_type('balancing act'/'ODY', 'Sorcery').
card_original_text('balancing act'/'ODY', 'Each player chooses a number of permanents he or she controls equal to the number of permanents controlled by the player who controls the fewest, then sacrifices the rest. Each player discards cards from his or her hand the same way.').
card_first_print('balancing act', 'ODY').
card_image_name('balancing act'/'ODY', 'balancing act').
card_uid('balancing act'/'ODY', 'ODY:Balancing Act:balancing act').
card_rarity('balancing act'/'ODY', 'Rare').
card_artist('balancing act'/'ODY', 'Scott M. Fischer').
card_number('balancing act'/'ODY', '10').
card_multiverse_id('balancing act'/'ODY', '31819').

card_in_set('balshan beguiler', 'ODY').
card_original_type('balshan beguiler'/'ODY', 'Creature — Wizard').
card_original_text('balshan beguiler'/'ODY', 'Whenever Balshan Beguiler deals combat damage to a player, that player reveals the top two cards of his or her library. You choose one of those cards and put it into his or her graveyard.').
card_first_print('balshan beguiler', 'ODY').
card_image_name('balshan beguiler'/'ODY', 'balshan beguiler').
card_uid('balshan beguiler'/'ODY', 'ODY:Balshan Beguiler:balshan beguiler').
card_rarity('balshan beguiler'/'ODY', 'Uncommon').
card_artist('balshan beguiler'/'ODY', 'Ray Lago').
card_number('balshan beguiler'/'ODY', '66').
card_multiverse_id('balshan beguiler'/'ODY', '29738').

card_in_set('balshan griffin', 'ODY').
card_original_type('balshan griffin'/'ODY', 'Creature — Griffin').
card_original_text('balshan griffin'/'ODY', 'Flying\n{1}{U}, Discard a card from your hand: Return Balshan Griffin to its owner\'s hand.').
card_first_print('balshan griffin', 'ODY').
card_image_name('balshan griffin'/'ODY', 'balshan griffin').
card_uid('balshan griffin'/'ODY', 'ODY:Balshan Griffin:balshan griffin').
card_rarity('balshan griffin'/'ODY', 'Uncommon').
card_artist('balshan griffin'/'ODY', 'Wayne England').
card_number('balshan griffin'/'ODY', '67').
card_flavor_text('balshan griffin'/'ODY', 'It helps to have an extra direction in which to run.').
card_multiverse_id('balshan griffin'/'ODY', '29820').

card_in_set('bamboozle', 'ODY').
card_original_type('bamboozle'/'ODY', 'Sorcery').
card_original_text('bamboozle'/'ODY', 'Target player reveals the top four cards of his or her library. You choose two of those cards and put them into his or her graveyard. Put the rest on top of his or her library in any order.').
card_first_print('bamboozle', 'ODY').
card_image_name('bamboozle'/'ODY', 'bamboozle').
card_uid('bamboozle'/'ODY', 'ODY:Bamboozle:bamboozle').
card_rarity('bamboozle'/'ODY', 'Uncommon').
card_artist('bamboozle'/'ODY', 'Alan Pollack').
card_number('bamboozle'/'ODY', '68').
card_multiverse_id('bamboozle'/'ODY', '29747').

card_in_set('barbarian lunatic', 'ODY').
card_original_type('barbarian lunatic'/'ODY', 'Creature — Barbarian').
card_original_text('barbarian lunatic'/'ODY', '{2}{R}, Sacrifice Barbarian Lunatic: Barbarian Lunatic deals 2 damage to target creature.').
card_first_print('barbarian lunatic', 'ODY').
card_image_name('barbarian lunatic'/'ODY', 'barbarian lunatic').
card_uid('barbarian lunatic'/'ODY', 'ODY:Barbarian Lunatic:barbarian lunatic').
card_rarity('barbarian lunatic'/'ODY', 'Common').
card_artist('barbarian lunatic'/'ODY', 'Ron Spears').
card_number('barbarian lunatic'/'ODY', '175').
card_flavor_text('barbarian lunatic'/'ODY', '\"I once saw a single barbarian charge a band of twenty nomads. I\'ve seen some crazy ones, too.\"\n—Nomad sentry').
card_multiverse_id('barbarian lunatic'/'ODY', '29757').

card_in_set('barbarian ring', 'ODY').
card_original_type('barbarian ring'/'ODY', 'Land').
card_original_text('barbarian ring'/'ODY', '{T}: Add {R} to your mana pool. Barbarian Ring deals 1 damage to you.\nThreshold {R}, {T}, Sacrifice Barbarian Ring: Barbarian Ring deals 2 damage to target creature or player. (Play this ability only if seven or more cards are in your graveyard.)').
card_first_print('barbarian ring', 'ODY').
card_image_name('barbarian ring'/'ODY', 'barbarian ring').
card_uid('barbarian ring'/'ODY', 'ODY:Barbarian Ring:barbarian ring').
card_rarity('barbarian ring'/'ODY', 'Uncommon').
card_artist('barbarian ring'/'ODY', 'John Avon').
card_number('barbarian ring'/'ODY', '313').
card_multiverse_id('barbarian ring'/'ODY', '29906').

card_in_set('bash to bits', 'ODY').
card_original_type('bash to bits'/'ODY', 'Instant').
card_original_text('bash to bits'/'ODY', 'Destroy target artifact.\nFlashback {4}{R}{R} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('bash to bits', 'ODY').
card_image_name('bash to bits'/'ODY', 'bash to bits').
card_uid('bash to bits'/'ODY', 'ODY:Bash to Bits:bash to bits').
card_rarity('bash to bits'/'ODY', 'Uncommon').
card_artist('bash to bits'/'ODY', 'Gary Ruddell').
card_number('bash to bits'/'ODY', '176').
card_multiverse_id('bash to bits'/'ODY', '30569').

card_in_set('battle of wits', 'ODY').
card_original_type('battle of wits'/'ODY', 'Enchantment').
card_original_text('battle of wits'/'ODY', 'At the beginning of your upkeep, if you have 200 or more cards in your library, you win the game.').
card_first_print('battle of wits', 'ODY').
card_image_name('battle of wits'/'ODY', 'battle of wits').
card_uid('battle of wits'/'ODY', 'ODY:Battle of Wits:battle of wits').
card_rarity('battle of wits'/'ODY', 'Rare').
card_artist('battle of wits'/'ODY', 'Mark Brill').
card_number('battle of wits'/'ODY', '69').
card_flavor_text('battle of wits'/'ODY', 'The wizard who reads a thousand books is powerful. The wizard who memorizes a thousand books is insane.').
card_multiverse_id('battle of wits'/'ODY', '29942').

card_in_set('battle strain', 'ODY').
card_original_type('battle strain'/'ODY', 'Enchantment').
card_original_text('battle strain'/'ODY', 'Whenever a creature blocks, Battle Strain deals 1 damage to that creature\'s controller.').
card_first_print('battle strain', 'ODY').
card_image_name('battle strain'/'ODY', 'battle strain').
card_uid('battle strain'/'ODY', 'ODY:Battle Strain:battle strain').
card_rarity('battle strain'/'ODY', 'Uncommon').
card_artist('battle strain'/'ODY', 'Dave Dorman').
card_number('battle strain'/'ODY', '177').
card_flavor_text('battle strain'/'ODY', 'The pits swallow the weak as predators swallow prey.').
card_multiverse_id('battle strain'/'ODY', '29861').

card_in_set('bearscape', 'ODY').
card_original_type('bearscape'/'ODY', 'Enchantment').
card_original_text('bearscape'/'ODY', '{1}{G}, Remove two cards in your graveyard from the game: Put a 2/2 green Bear creature token into play.').
card_first_print('bearscape', 'ODY').
card_image_name('bearscape'/'ODY', 'bearscape').
card_uid('bearscape'/'ODY', 'ODY:Bearscape:bearscape').
card_rarity('bearscape'/'ODY', 'Rare').
card_artist('bearscape'/'ODY', 'Heather Hudson').
card_number('bearscape'/'ODY', '229').
card_flavor_text('bearscape'/'ODY', '\"Nature is the endless dance between life and death.\"\n—Seton, centaur druid').
card_multiverse_id('bearscape'/'ODY', '30747').

card_in_set('beast attack', 'ODY').
card_original_type('beast attack'/'ODY', 'Instant').
card_original_text('beast attack'/'ODY', 'Put a 4/4 green Beast creature token into play.\nFlashback {2}{G}{G}{G} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('beast attack', 'ODY').
card_image_name('beast attack'/'ODY', 'beast attack').
card_uid('beast attack'/'ODY', 'ODY:Beast Attack:beast attack').
card_rarity('beast attack'/'ODY', 'Uncommon').
card_artist('beast attack'/'ODY', 'Ciruelo').
card_number('beast attack'/'ODY', '230').
card_multiverse_id('beast attack'/'ODY', '29880').

card_in_set('beloved chaplain', 'ODY').
card_original_type('beloved chaplain'/'ODY', 'Creature — Cleric').
card_original_text('beloved chaplain'/'ODY', 'Protection from creatures').
card_first_print('beloved chaplain', 'ODY').
card_image_name('beloved chaplain'/'ODY', 'beloved chaplain').
card_uid('beloved chaplain'/'ODY', 'ODY:Beloved Chaplain:beloved chaplain').
card_rarity('beloved chaplain'/'ODY', 'Uncommon').
card_artist('beloved chaplain'/'ODY', 'Darrell Riche').
card_number('beloved chaplain'/'ODY', '11').
card_flavor_text('beloved chaplain'/'ODY', 'Nomad and Nantuko, eagle and elephant; all the birds and beasts are charmed by his quiet dignity.').
card_multiverse_id('beloved chaplain'/'ODY', '29798').

card_in_set('blazing salvo', 'ODY').
card_original_type('blazing salvo'/'ODY', 'Instant').
card_original_text('blazing salvo'/'ODY', 'Blazing Salvo deals 3 damage to target creature unless that creature\'s controller has Blazing Salvo deal 5 damage to him or her.').
card_first_print('blazing salvo', 'ODY').
card_image_name('blazing salvo'/'ODY', 'blazing salvo').
card_uid('blazing salvo'/'ODY', 'ODY:Blazing Salvo:blazing salvo').
card_rarity('blazing salvo'/'ODY', 'Common').
card_artist('blazing salvo'/'ODY', 'rk post').
card_number('blazing salvo'/'ODY', '178').
card_flavor_text('blazing salvo'/'ODY', 'Barbarians equate courage with the ability to endure pain.').
card_multiverse_id('blazing salvo'/'ODY', '29858').

card_in_set('blessed orator', 'ODY').
card_original_type('blessed orator'/'ODY', 'Creature — Cleric').
card_original_text('blessed orator'/'ODY', 'Other creatures you control get +0/+1.').
card_first_print('blessed orator', 'ODY').
card_image_name('blessed orator'/'ODY', 'blessed orator').
card_uid('blessed orator'/'ODY', 'ODY:Blessed Orator:blessed orator').
card_rarity('blessed orator'/'ODY', 'Uncommon').
card_artist('blessed orator'/'ODY', 'Terese Nielsen').
card_number('blessed orator'/'ODY', '12').
card_flavor_text('blessed orator'/'ODY', 'His quiet words fade into the night\'s chill, yet they burn into his listeners\' minds with all the power of a thousand suns.').
card_multiverse_id('blessed orator'/'ODY', '29688').

card_in_set('bloodcurdler', 'ODY').
card_original_type('bloodcurdler'/'ODY', 'Creature — Horror').
card_original_text('bloodcurdler'/'ODY', 'Flying\nAt the beginning of your upkeep, put the top card of your library into your graveyard.\nThreshold Bloodcurdler gets +1/+1 and has \"At the end of your turn, remove two cards in your graveyard from the game.\" (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('bloodcurdler', 'ODY').
card_image_name('bloodcurdler'/'ODY', 'bloodcurdler').
card_uid('bloodcurdler'/'ODY', 'ODY:Bloodcurdler:bloodcurdler').
card_rarity('bloodcurdler'/'ODY', 'Rare').
card_artist('bloodcurdler'/'ODY', 'Adam Rex').
card_number('bloodcurdler'/'ODY', '116').
card_multiverse_id('bloodcurdler'/'ODY', '29950').

card_in_set('bog wreckage', 'ODY').
card_original_type('bog wreckage'/'ODY', 'Land').
card_original_text('bog wreckage'/'ODY', 'Bog Wreckage comes into play tapped.\n{T}: Add {B} to your mana pool.\n{T}, Sacrifice Bog Wreckage: Add one mana of any color to your mana pool.').
card_first_print('bog wreckage', 'ODY').
card_image_name('bog wreckage'/'ODY', 'bog wreckage').
card_uid('bog wreckage'/'ODY', 'ODY:Bog Wreckage:bog wreckage').
card_rarity('bog wreckage'/'ODY', 'Common').
card_artist('bog wreckage'/'ODY', 'Brian Snõddy').
card_number('bog wreckage'/'ODY', '314').
card_multiverse_id('bog wreckage'/'ODY', '31763').

card_in_set('bomb squad', 'ODY').
card_original_type('bomb squad'/'ODY', 'Creature — Dwarf').
card_original_text('bomb squad'/'ODY', '{T}: Put a fuse counter on target creature.\nAt the beginning of your upkeep, put a fuse counter on each creature with a fuse counter on it.\nWhenever a creature has four or more fuse counters on it, remove all fuse counters from it and destroy it. That creature deals 4 damage to its controller.').
card_first_print('bomb squad', 'ODY').
card_image_name('bomb squad'/'ODY', 'bomb squad').
card_uid('bomb squad'/'ODY', 'ODY:Bomb Squad:bomb squad').
card_rarity('bomb squad'/'ODY', 'Rare').
card_artist('bomb squad'/'ODY', 'Greg & Tim Hildebrandt').
card_number('bomb squad'/'ODY', '179').
card_multiverse_id('bomb squad'/'ODY', '26812').

card_in_set('braids, cabal minion', 'ODY').
card_original_type('braids, cabal minion'/'ODY', 'Creature — Minion Legend').
card_original_text('braids, cabal minion'/'ODY', 'At the beginning of each player\'s upkeep, that player sacrifices an artifact, creature, or land.').
card_first_print('braids, cabal minion', 'ODY').
card_image_name('braids, cabal minion'/'ODY', 'braids, cabal minion').
card_uid('braids, cabal minion'/'ODY', 'ODY:Braids, Cabal Minion:braids, cabal minion').
card_rarity('braids, cabal minion'/'ODY', 'Rare').
card_artist('braids, cabal minion'/'ODY', 'Eric Peterson').
card_number('braids, cabal minion'/'ODY', '117').
card_flavor_text('braids, cabal minion'/'ODY', '\"Home is where you can find a decent graveyard and strangers can disappear without awkward questions.\"').
card_multiverse_id('braids, cabal minion'/'ODY', '29947').

card_in_set('buried alive', 'ODY').
card_original_type('buried alive'/'ODY', 'Sorcery').
card_original_text('buried alive'/'ODY', 'Search your library for up to three creature cards and put them into your graveyard. Then shuffle your library.').
card_image_name('buried alive'/'ODY', 'buried alive').
card_uid('buried alive'/'ODY', 'ODY:Buried Alive:buried alive').
card_rarity('buried alive'/'ODY', 'Uncommon').
card_artist('buried alive'/'ODY', 'Greg Staples').
card_number('buried alive'/'ODY', '118').
card_flavor_text('buried alive'/'ODY', 'The scrape of shovels and the tumble of cold dirt soon muffled their pleas.').
card_multiverse_id('buried alive'/'ODY', '29846').

card_in_set('burning sands', 'ODY').
card_original_type('burning sands'/'ODY', 'Enchantment').
card_original_text('burning sands'/'ODY', 'Whenever a creature is put into a graveyard from play, that creature\'s controller sacrifices a land.').
card_first_print('burning sands', 'ODY').
card_image_name('burning sands'/'ODY', 'burning sands').
card_uid('burning sands'/'ODY', 'ODY:Burning Sands:burning sands').
card_rarity('burning sands'/'ODY', 'Rare').
card_artist('burning sands'/'ODY', 'Ron Spencer').
card_number('burning sands'/'ODY', '180').
card_flavor_text('burning sands'/'ODY', '\"Pain teaches lessons no scholar can.\"\n—Kamahl, pit fighter').
card_multiverse_id('burning sands'/'ODY', '31797').

card_in_set('cabal inquisitor', 'ODY').
card_original_type('cabal inquisitor'/'ODY', 'Creature — Minion').
card_original_text('cabal inquisitor'/'ODY', 'Threshold {1}{B}, {T}, Remove two cards in your graveyard from the game: Target player discards a card from his or her hand. Play this ability only any time you could play a sorcery. (Play this ability only if seven or more cards are in your graveyard.)').
card_first_print('cabal inquisitor', 'ODY').
card_image_name('cabal inquisitor'/'ODY', 'cabal inquisitor').
card_uid('cabal inquisitor'/'ODY', 'ODY:Cabal Inquisitor:cabal inquisitor').
card_rarity('cabal inquisitor'/'ODY', 'Common').
card_artist('cabal inquisitor'/'ODY', 'Alex Horley-Orlandelli').
card_number('cabal inquisitor'/'ODY', '119').
card_multiverse_id('cabal inquisitor'/'ODY', '29730').

card_in_set('cabal patriarch', 'ODY').
card_original_type('cabal patriarch'/'ODY', 'Creature — Wizard Legend').
card_original_text('cabal patriarch'/'ODY', '{2}{B}, Sacrifice a creature: Target creature gets -2/-2 until end of turn.\n{2}{B}, Remove a creature card in your graveyard from the game: Target creature gets -2/-2 until end of turn.').
card_first_print('cabal patriarch', 'ODY').
card_image_name('cabal patriarch'/'ODY', 'cabal patriarch').
card_uid('cabal patriarch'/'ODY', 'ODY:Cabal Patriarch:cabal patriarch').
card_rarity('cabal patriarch'/'ODY', 'Rare').
card_artist('cabal patriarch'/'ODY', 'Mark Zug').
card_number('cabal patriarch'/'ODY', '120').
card_multiverse_id('cabal patriarch'/'ODY', '29943').

card_in_set('cabal pit', 'ODY').
card_original_type('cabal pit'/'ODY', 'Land').
card_original_text('cabal pit'/'ODY', '{T}: Add {B} to your mana pool. Cabal Pit deals 1 damage to you.\nThreshold {B}, {T}, Sacrifice Cabal Pit: Target creature gets -2/-2 until end of turn. (Play this ability only if seven or more cards are in your graveyard.)').
card_first_print('cabal pit', 'ODY').
card_image_name('cabal pit'/'ODY', 'cabal pit').
card_uid('cabal pit'/'ODY', 'ODY:Cabal Pit:cabal pit').
card_rarity('cabal pit'/'ODY', 'Uncommon').
card_artist('cabal pit'/'ODY', 'John Avon').
card_number('cabal pit'/'ODY', '315').
card_multiverse_id('cabal pit'/'ODY', '29904').

card_in_set('cabal shrine', 'ODY').
card_original_type('cabal shrine'/'ODY', 'Enchantment').
card_original_text('cabal shrine'/'ODY', 'Whenever a player plays a spell, that player discards X cards from his or her hand, where X is the number of cards in all graveyards with the same name as that spell.').
card_first_print('cabal shrine', 'ODY').
card_image_name('cabal shrine'/'ODY', 'cabal shrine').
card_uid('cabal shrine'/'ODY', 'ODY:Cabal Shrine:cabal shrine').
card_rarity('cabal shrine'/'ODY', 'Rare').
card_artist('cabal shrine'/'ODY', 'Ben Thompson').
card_number('cabal shrine'/'ODY', '121').
card_multiverse_id('cabal shrine'/'ODY', '29959').

card_in_set('call of the herd', 'ODY').
card_original_type('call of the herd'/'ODY', 'Sorcery').
card_original_text('call of the herd'/'ODY', 'Put a 3/3 green Elephant creature token into play.\nFlashback {3}{G} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('call of the herd', 'ODY').
card_image_name('call of the herd'/'ODY', 'call of the herd').
card_uid('call of the herd'/'ODY', 'ODY:Call of the Herd:call of the herd').
card_rarity('call of the herd'/'ODY', 'Rare').
card_artist('call of the herd'/'ODY', 'Carl Critchlow').
card_number('call of the herd'/'ODY', '231').
card_multiverse_id('call of the herd'/'ODY', '29786').

card_in_set('cantivore', 'ODY').
card_original_type('cantivore'/'ODY', 'Creature — Lhurgoyf').
card_original_text('cantivore'/'ODY', 'Attacking doesn\'t cause Cantivore to tap.\nCantivore\'s power and toughness are each equal to the number of enchantment cards in all graveyards.').
card_first_print('cantivore', 'ODY').
card_image_name('cantivore'/'ODY', 'cantivore').
card_uid('cantivore'/'ODY', 'ODY:Cantivore:cantivore').
card_rarity('cantivore'/'ODY', 'Rare').
card_artist('cantivore'/'ODY', 'Daren Bader').
card_number('cantivore'/'ODY', '13').
card_multiverse_id('cantivore'/'ODY', '29914').

card_in_set('careful study', 'ODY').
card_original_type('careful study'/'ODY', 'Sorcery').
card_original_text('careful study'/'ODY', 'Draw two cards, then discard two cards from your hand.').
card_first_print('careful study', 'ODY').
card_image_name('careful study'/'ODY', 'careful study').
card_uid('careful study'/'ODY', 'ODY:Careful Study:careful study').
card_rarity('careful study'/'ODY', 'Common').
card_artist('careful study'/'ODY', 'Scott M. Fischer').
card_number('careful study'/'ODY', '70').
card_flavor_text('careful study'/'ODY', 'Books are like crops—they must be thinned for best yield.').
card_multiverse_id('careful study'/'ODY', '29727').

card_in_set('cartographer', 'ODY').
card_original_type('cartographer'/'ODY', 'Creature — Townsfolk').
card_original_text('cartographer'/'ODY', 'When Cartographer comes into play, you may return target land card from your graveyard to your hand.').
card_image_name('cartographer'/'ODY', 'cartographer').
card_uid('cartographer'/'ODY', 'ODY:Cartographer:cartographer').
card_rarity('cartographer'/'ODY', 'Common').
card_artist('cartographer'/'ODY', 'Donato Giancola').
card_number('cartographer'/'ODY', '232').
card_flavor_text('cartographer'/'ODY', '\"Finding the way once is luck. Finding the way twice takes a good map.\"').
card_multiverse_id('cartographer'/'ODY', '29783').

card_in_set('catalyst stone', 'ODY').
card_original_type('catalyst stone'/'ODY', 'Artifact').
card_original_text('catalyst stone'/'ODY', 'Flashback costs you pay cost up to {2} less.\nFlashback costs your opponents pay cost {2} more.').
card_first_print('catalyst stone', 'ODY').
card_image_name('catalyst stone'/'ODY', 'catalyst stone').
card_uid('catalyst stone'/'ODY', 'ODY:Catalyst Stone:catalyst stone').
card_rarity('catalyst stone'/'ODY', 'Rare').
card_artist('catalyst stone'/'ODY', 'Tony Szczudlo').
card_number('catalyst stone'/'ODY', '297').
card_flavor_text('catalyst stone'/'ODY', 'The gems\' incandescence invades the dreams of all who see them.').
card_multiverse_id('catalyst stone'/'ODY', '30005').

card_in_set('caustic tar', 'ODY').
card_original_type('caustic tar'/'ODY', 'Enchant Land').
card_original_text('caustic tar'/'ODY', 'Enchanted land has \"{T}: Target player loses 3 life.\"').
card_first_print('caustic tar', 'ODY').
card_image_name('caustic tar'/'ODY', 'caustic tar').
card_uid('caustic tar'/'ODY', 'ODY:Caustic Tar:caustic tar').
card_rarity('caustic tar'/'ODY', 'Uncommon').
card_artist('caustic tar'/'ODY', 'John Avon').
card_number('caustic tar'/'ODY', '122').
card_flavor_text('caustic tar'/'ODY', '\"Twisted magic, twisted fate,\nForce the land to ooze with hate.\"\n—Cabal chant').
card_multiverse_id('caustic tar'/'ODY', '31837').

card_in_set('cease-fire', 'ODY').
card_original_type('cease-fire'/'ODY', 'Instant').
card_original_text('cease-fire'/'ODY', 'Target player can\'t play creature spells this turn.\nDraw a card.').
card_first_print('cease-fire', 'ODY').
card_image_name('cease-fire'/'ODY', 'cease-fire').
card_uid('cease-fire'/'ODY', 'ODY:Cease-Fire:cease-fire').
card_rarity('cease-fire'/'ODY', 'Common').
card_artist('cease-fire'/'ODY', 'Darrell Riche').
card_number('cease-fire'/'ODY', '14').
card_flavor_text('cease-fire'/'ODY', 'When the crimson fades from the magical embers, a final wisp of smoke will rise. Soon after, hostilities will begin anew.').
card_multiverse_id('cease-fire'/'ODY', '30544').

card_in_set('centaur garden', 'ODY').
card_original_type('centaur garden'/'ODY', 'Land').
card_original_text('centaur garden'/'ODY', '{T}: Add {G} to your mana pool. Centaur Garden deals 1 damage to you.\nThreshold {G}, {T}, Sacrifice Centaur Garden: Target creature gets +3/+3 until end of turn. (Play this ability only if seven or more cards are in your graveyard.)').
card_first_print('centaur garden', 'ODY').
card_image_name('centaur garden'/'ODY', 'centaur garden').
card_uid('centaur garden'/'ODY', 'ODY:Centaur Garden:centaur garden').
card_rarity('centaur garden'/'ODY', 'Uncommon').
card_artist('centaur garden'/'ODY', 'John Avon').
card_number('centaur garden'/'ODY', '316').
card_multiverse_id('centaur garden'/'ODY', '29905').

card_in_set('cephalid broker', 'ODY').
card_original_type('cephalid broker'/'ODY', 'Creature — Cephalid').
card_original_text('cephalid broker'/'ODY', '{T}: Target player draws two cards, then discards two cards from his or her hand.').
card_first_print('cephalid broker', 'ODY').
card_image_name('cephalid broker'/'ODY', 'cephalid broker').
card_uid('cephalid broker'/'ODY', 'ODY:Cephalid Broker:cephalid broker').
card_rarity('cephalid broker'/'ODY', 'Uncommon').
card_artist('cephalid broker'/'ODY', 'Dave Dorman').
card_number('cephalid broker'/'ODY', '71').
card_flavor_text('cephalid broker'/'ODY', 'Cephalids value the free exchange of ideas—except for the \"free\" part.').
card_multiverse_id('cephalid broker'/'ODY', '29711').

card_in_set('cephalid coliseum', 'ODY').
card_original_type('cephalid coliseum'/'ODY', 'Land').
card_original_text('cephalid coliseum'/'ODY', '{T}: Add {U} to your mana pool. Cephalid Coliseum deals 1 damage to you.\nThreshold {U}, {T}, Sacrifice Cephalid Coliseum: Target player draws three cards, then discards three cards from his or her hand. (Play this ability only if seven or more cards are in your graveyard.)').
card_first_print('cephalid coliseum', 'ODY').
card_image_name('cephalid coliseum'/'ODY', 'cephalid coliseum').
card_uid('cephalid coliseum'/'ODY', 'ODY:Cephalid Coliseum:cephalid coliseum').
card_rarity('cephalid coliseum'/'ODY', 'Uncommon').
card_artist('cephalid coliseum'/'ODY', 'John Avon').
card_number('cephalid coliseum'/'ODY', '317').
card_multiverse_id('cephalid coliseum'/'ODY', '29903').

card_in_set('cephalid looter', 'ODY').
card_original_type('cephalid looter'/'ODY', 'Creature — Cephalid').
card_original_text('cephalid looter'/'ODY', '{T}: Target player draws a card, then discards a card from his or her hand.').
card_first_print('cephalid looter', 'ODY').
card_image_name('cephalid looter'/'ODY', 'cephalid looter').
card_uid('cephalid looter'/'ODY', 'ODY:Cephalid Looter:cephalid looter').
card_rarity('cephalid looter'/'ODY', 'Common').
card_artist('cephalid looter'/'ODY', 'Keith Garletts').
card_number('cephalid looter'/'ODY', '72').
card_flavor_text('cephalid looter'/'ODY', '\"Amazing, isn\'t it, what humans will throw away when they\'re drowning?\"').
card_multiverse_id('cephalid looter'/'ODY', '29821').

card_in_set('cephalid retainer', 'ODY').
card_original_type('cephalid retainer'/'ODY', 'Creature — Cephalid').
card_original_text('cephalid retainer'/'ODY', '{U}{U}: Tap target creature without flying.').
card_first_print('cephalid retainer', 'ODY').
card_image_name('cephalid retainer'/'ODY', 'cephalid retainer').
card_uid('cephalid retainer'/'ODY', 'ODY:Cephalid Retainer:cephalid retainer').
card_rarity('cephalid retainer'/'ODY', 'Rare').
card_artist('cephalid retainer'/'ODY', 'Tony Szczudlo').
card_number('cephalid retainer'/'ODY', '73').
card_flavor_text('cephalid retainer'/'ODY', 'Cephalids whisper flattery in your ear, put jewels in your hand, spin dreams in your head, and stick knives in your back.').
card_multiverse_id('cephalid retainer'/'ODY', '31870').

card_in_set('cephalid scout', 'ODY').
card_original_type('cephalid scout'/'ODY', 'Creature — Cephalid Wizard').
card_original_text('cephalid scout'/'ODY', 'Flying\n{2}{U}, Sacrifice a land: Draw a card.').
card_first_print('cephalid scout', 'ODY').
card_image_name('cephalid scout'/'ODY', 'cephalid scout').
card_uid('cephalid scout'/'ODY', 'ODY:Cephalid Scout:cephalid scout').
card_rarity('cephalid scout'/'ODY', 'Common').
card_artist('cephalid scout'/'ODY', 'Alan Pollack').
card_number('cephalid scout'/'ODY', '74').
card_flavor_text('cephalid scout'/'ODY', 'The more they survey the dreadful dry expanses, the more the cephalids thirst to tame them.').
card_multiverse_id('cephalid scout'/'ODY', '29710').

card_in_set('cephalid shrine', 'ODY').
card_original_type('cephalid shrine'/'ODY', 'Enchantment').
card_original_text('cephalid shrine'/'ODY', 'Whenever a player plays a spell, counter that spell unless that player pays {X}, where X is the number of cards in all graveyards with the same name as the spell.').
card_first_print('cephalid shrine', 'ODY').
card_image_name('cephalid shrine'/'ODY', 'cephalid shrine').
card_uid('cephalid shrine'/'ODY', 'ODY:Cephalid Shrine:cephalid shrine').
card_rarity('cephalid shrine'/'ODY', 'Rare').
card_artist('cephalid shrine'/'ODY', 'Wayne England').
card_number('cephalid shrine'/'ODY', '75').
card_multiverse_id('cephalid shrine'/'ODY', '29941').

card_in_set('chainflinger', 'ODY').
card_original_type('chainflinger'/'ODY', 'Creature — Beast').
card_original_text('chainflinger'/'ODY', '{1}{R}, {T}: Chainflinger deals 1 damage to target creature or player.\nThreshold {2}{R}, {T}: Chainflinger deals 2 damage to target creature or player. (Play this ability only if seven or more cards are in your graveyard.)').
card_first_print('chainflinger', 'ODY').
card_image_name('chainflinger'/'ODY', 'chainflinger').
card_uid('chainflinger'/'ODY', 'ODY:Chainflinger:chainflinger').
card_rarity('chainflinger'/'ODY', 'Common').
card_artist('chainflinger'/'ODY', 'Scott M. Fischer').
card_number('chainflinger'/'ODY', '181').
card_multiverse_id('chainflinger'/'ODY', '29755').

card_in_set('chamber of manipulation', 'ODY').
card_original_type('chamber of manipulation'/'ODY', 'Enchant Land').
card_original_text('chamber of manipulation'/'ODY', 'Enchanted land has \"{T}, Discard a card from your hand: Gain control of target creature until end of turn.\"').
card_first_print('chamber of manipulation', 'ODY').
card_image_name('chamber of manipulation'/'ODY', 'chamber of manipulation').
card_uid('chamber of manipulation'/'ODY', 'ODY:Chamber of Manipulation:chamber of manipulation').
card_rarity('chamber of manipulation'/'ODY', 'Uncommon').
card_artist('chamber of manipulation'/'ODY', 'Wayne England').
card_number('chamber of manipulation'/'ODY', '76').
card_flavor_text('chamber of manipulation'/'ODY', 'Politics is just a subtle form of mind control—and cephalids are master politicians.').
card_multiverse_id('chamber of manipulation'/'ODY', '31868').

card_in_set('chance encounter', 'ODY').
card_original_type('chance encounter'/'ODY', 'Enchantment').
card_original_text('chance encounter'/'ODY', 'Whenever you win a coin flip, put a luck counter on Chance Encounter.\nAt the beginning of your upkeep, if Chance Encounter has ten or more luck counters on it, you win the game.').
card_first_print('chance encounter', 'ODY').
card_image_name('chance encounter'/'ODY', 'chance encounter').
card_uid('chance encounter'/'ODY', 'ODY:Chance Encounter:chance encounter').
card_rarity('chance encounter'/'ODY', 'Rare').
card_artist('chance encounter'/'ODY', 'Mark Brill').
card_number('chance encounter'/'ODY', '182').
card_flavor_text('chance encounter'/'ODY', 'The more unlikely the victory, the more memorable the success.').
card_multiverse_id('chance encounter'/'ODY', '29978').

card_in_set('charmed pendant', 'ODY').
card_original_type('charmed pendant'/'ODY', 'Artifact').
card_original_text('charmed pendant'/'ODY', '{T}, Put the top card of your library into your graveyard: For each colored mana symbol in that card\'s mana cost, add one mana of that color to your mana pool. Play this ability only any time you could play an instant. (For example, if the card\'s mana cost is {3}{U}{U}{B}, you add {U}{U}{B} to your mana pool.)').
card_first_print('charmed pendant', 'ODY').
card_image_name('charmed pendant'/'ODY', 'charmed pendant').
card_uid('charmed pendant'/'ODY', 'ODY:Charmed Pendant:charmed pendant').
card_rarity('charmed pendant'/'ODY', 'Rare').
card_artist('charmed pendant'/'ODY', 'Anthony S. Waters').
card_number('charmed pendant'/'ODY', '298').
card_multiverse_id('charmed pendant'/'ODY', '29899').

card_in_set('chatter of the squirrel', 'ODY').
card_original_type('chatter of the squirrel'/'ODY', 'Sorcery').
card_original_text('chatter of the squirrel'/'ODY', 'Put a 1/1 green Squirrel creature token into play.\nFlashback {1}{G} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('chatter of the squirrel', 'ODY').
card_image_name('chatter of the squirrel'/'ODY', 'chatter of the squirrel').
card_uid('chatter of the squirrel'/'ODY', 'ODY:Chatter of the Squirrel:chatter of the squirrel').
card_rarity('chatter of the squirrel'/'ODY', 'Common').
card_artist('chatter of the squirrel'/'ODY', 'Jim Nelson').
card_number('chatter of the squirrel'/'ODY', '233').
card_multiverse_id('chatter of the squirrel'/'ODY', '31444').

card_in_set('childhood horror', 'ODY').
card_original_type('childhood horror'/'ODY', 'Creature — Horror').
card_original_text('childhood horror'/'ODY', 'Flying\nThreshold Childhood Horror gets +2/+2 and can\'t block. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('childhood horror', 'ODY').
card_image_name('childhood horror'/'ODY', 'childhood horror').
card_uid('childhood horror'/'ODY', 'ODY:Childhood Horror:childhood horror').
card_rarity('childhood horror'/'ODY', 'Uncommon').
card_artist('childhood horror'/'ODY', 'Larry Elmore').
card_number('childhood horror'/'ODY', '123').
card_multiverse_id('childhood horror'/'ODY', '29836').

card_in_set('chlorophant', 'ODY').
card_original_type('chlorophant'/'ODY', 'Creature — Elemental').
card_original_text('chlorophant'/'ODY', 'At the beginning of your upkeep, you may put a +1/+1 counter on Chlorophant.\nThreshold At the beginning of your upkeep, you may put another +1/+1 counter on Chlorophant. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('chlorophant', 'ODY').
card_image_name('chlorophant'/'ODY', 'chlorophant').
card_uid('chlorophant'/'ODY', 'ODY:Chlorophant:chlorophant').
card_rarity('chlorophant'/'ODY', 'Rare').
card_artist('chlorophant'/'ODY', 'John Avon').
card_number('chlorophant'/'ODY', '234').
card_multiverse_id('chlorophant'/'ODY', '29986').

card_in_set('coffin purge', 'ODY').
card_original_type('coffin purge'/'ODY', 'Instant').
card_original_text('coffin purge'/'ODY', 'Remove target card in a graveyard from the game.\nFlashback {B} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('coffin purge', 'ODY').
card_image_name('coffin purge'/'ODY', 'coffin purge').
card_uid('coffin purge'/'ODY', 'ODY:Coffin Purge:coffin purge').
card_rarity('coffin purge'/'ODY', 'Common').
card_artist('coffin purge'/'ODY', 'Pete Venters').
card_number('coffin purge'/'ODY', '124').
card_multiverse_id('coffin purge'/'ODY', '30762').

card_in_set('cognivore', 'ODY').
card_original_type('cognivore'/'ODY', 'Creature — Lhurgoyf').
card_original_text('cognivore'/'ODY', 'Flying\nCognivore\'s power and toughness are each equal to the number of instant cards in all graveyards.').
card_first_print('cognivore', 'ODY').
card_image_name('cognivore'/'ODY', 'cognivore').
card_uid('cognivore'/'ODY', 'ODY:Cognivore:cognivore').
card_rarity('cognivore'/'ODY', 'Rare').
card_artist('cognivore'/'ODY', 'Adam Rex').
card_number('cognivore'/'ODY', '77').
card_multiverse_id('cognivore'/'ODY', '29931').

card_in_set('concentrate', 'ODY').
card_original_type('concentrate'/'ODY', 'Sorcery').
card_original_text('concentrate'/'ODY', 'Draw three cards.').
card_first_print('concentrate', 'ODY').
card_image_name('concentrate'/'ODY', 'concentrate').
card_uid('concentrate'/'ODY', 'ODY:Concentrate:concentrate').
card_rarity('concentrate'/'ODY', 'Uncommon').
card_artist('concentrate'/'ODY', 'Glen Angus & Arnie Swekel').
card_number('concentrate'/'ODY', '78').
card_flavor_text('concentrate'/'ODY', '\"The treasure in my mind is greater than any worldly glory.\"').
card_multiverse_id('concentrate'/'ODY', '30655').

card_in_set('confessor', 'ODY').
card_original_type('confessor'/'ODY', 'Creature — Cleric').
card_original_text('confessor'/'ODY', 'Whenever a player discards a card from his or her hand, you may gain 1 life.').
card_first_print('confessor', 'ODY').
card_image_name('confessor'/'ODY', 'confessor').
card_uid('confessor'/'ODY', 'ODY:Confessor:confessor').
card_rarity('confessor'/'ODY', 'Common').
card_artist('confessor'/'ODY', 'John Avon').
card_number('confessor'/'ODY', '15').
card_flavor_text('confessor'/'ODY', '\"There will be gains for our losses. There will be rights for our wrongs.\"').
card_multiverse_id('confessor'/'ODY', '31769').

card_in_set('crashing centaur', 'ODY').
card_original_type('crashing centaur'/'ODY', 'Creature — Centaur').
card_original_text('crashing centaur'/'ODY', '{G}, Discard a card from your hand: Crashing Centaur gains trample until end of turn.\nThreshold Crashing Centaur gets +2/+2 and can\'t be the target of spells or abilities. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('crashing centaur', 'ODY').
card_image_name('crashing centaur'/'ODY', 'crashing centaur').
card_uid('crashing centaur'/'ODY', 'ODY:Crashing Centaur:crashing centaur').
card_rarity('crashing centaur'/'ODY', 'Uncommon').
card_artist('crashing centaur'/'ODY', 'Eric Peterson').
card_number('crashing centaur'/'ODY', '235').
card_multiverse_id('crashing centaur'/'ODY', '29878').

card_in_set('crypt creeper', 'ODY').
card_original_type('crypt creeper'/'ODY', 'Creature — Zombie').
card_original_text('crypt creeper'/'ODY', 'Sacrifice Crypt Creeper: Remove target card in a graveyard from the game.').
card_first_print('crypt creeper', 'ODY').
card_image_name('crypt creeper'/'ODY', 'crypt creeper').
card_uid('crypt creeper'/'ODY', 'ODY:Crypt Creeper:crypt creeper').
card_rarity('crypt creeper'/'ODY', 'Common').
card_artist('crypt creeper'/'ODY', 'John Matson').
card_number('crypt creeper'/'ODY', '125').
card_flavor_text('crypt creeper'/'ODY', 'To unearth its dinner, it inters itself.').
card_multiverse_id('crypt creeper'/'ODY', '29840').

card_in_set('crystal quarry', 'ODY').
card_original_type('crystal quarry'/'ODY', 'Land').
card_original_text('crystal quarry'/'ODY', '{T}: Add one colorless mana to your mana pool.\n{5}, {T}: Add {W}{U}{B}{R}{G} to your mana pool.').
card_first_print('crystal quarry', 'ODY').
card_image_name('crystal quarry'/'ODY', 'crystal quarry').
card_uid('crystal quarry'/'ODY', 'ODY:Crystal Quarry:crystal quarry').
card_rarity('crystal quarry'/'ODY', 'Rare').
card_artist('crystal quarry'/'ODY', 'Alan Pollack').
card_number('crystal quarry'/'ODY', '318').
card_flavor_text('crystal quarry'/'ODY', '\"How tragic that greed eclipses beauty.\"\n—Seton, centaur druid').
card_multiverse_id('crystal quarry'/'ODY', '30012').

card_in_set('cultural exchange', 'ODY').
card_original_type('cultural exchange'/'ODY', 'Sorcery').
card_original_text('cultural exchange'/'ODY', 'Choose any number of creatures target player controls. Choose the same number of creatures another target player controls. Those players exchange control of those creatures. (This effect doesn\'t end at end of turn.)').
card_first_print('cultural exchange', 'ODY').
card_image_name('cultural exchange'/'ODY', 'cultural exchange').
card_uid('cultural exchange'/'ODY', 'ODY:Cultural Exchange:cultural exchange').
card_rarity('cultural exchange'/'ODY', 'Rare').
card_artist('cultural exchange'/'ODY', 'Daren Bader').
card_number('cultural exchange'/'ODY', '79').
card_multiverse_id('cultural exchange'/'ODY', '30763').

card_in_set('cursed monstrosity', 'ODY').
card_original_type('cursed monstrosity'/'ODY', 'Creature — Horror').
card_original_text('cursed monstrosity'/'ODY', 'Flying\nWhenever Cursed Monstrosity becomes the target of a spell or ability, sacrifice it unless you discard a land card from your hand.').
card_first_print('cursed monstrosity', 'ODY').
card_image_name('cursed monstrosity'/'ODY', 'cursed monstrosity').
card_uid('cursed monstrosity'/'ODY', 'ODY:Cursed Monstrosity:cursed monstrosity').
card_rarity('cursed monstrosity'/'ODY', 'Rare').
card_artist('cursed monstrosity'/'ODY', 'Jeff Remmer').
card_number('cursed monstrosity'/'ODY', '126').
card_flavor_text('cursed monstrosity'/'ODY', '\"Run away! It\'s an . . . um . . . run away!\"\n—Nomad sentry').
card_multiverse_id('cursed monstrosity'/'ODY', '29850').

card_in_set('darkwater catacombs', 'ODY').
card_original_type('darkwater catacombs'/'ODY', 'Land').
card_original_text('darkwater catacombs'/'ODY', '{1}, {T}: Add {U}{B} to your mana pool.').
card_first_print('darkwater catacombs', 'ODY').
card_image_name('darkwater catacombs'/'ODY', 'darkwater catacombs').
card_uid('darkwater catacombs'/'ODY', 'ODY:Darkwater Catacombs:darkwater catacombs').
card_rarity('darkwater catacombs'/'ODY', 'Rare').
card_artist('darkwater catacombs'/'ODY', 'Monte Michael Moore').
card_number('darkwater catacombs'/'ODY', '319').
card_flavor_text('darkwater catacombs'/'ODY', 'Murky Otarian tides hide the forgotten treacheries of the Phyrexian war.').
card_multiverse_id('darkwater catacombs'/'ODY', '29793').

card_in_set('darkwater egg', 'ODY').
card_original_type('darkwater egg'/'ODY', 'Artifact').
card_original_text('darkwater egg'/'ODY', '{2}, {T}, Sacrifice Darkwater Egg: Add {U}{B} to your mana pool. Draw a card.').
card_first_print('darkwater egg', 'ODY').
card_image_name('darkwater egg'/'ODY', 'darkwater egg').
card_uid('darkwater egg'/'ODY', 'ODY:Darkwater Egg:darkwater egg').
card_rarity('darkwater egg'/'ODY', 'Uncommon').
card_artist('darkwater egg'/'ODY', 'David Martin').
card_number('darkwater egg'/'ODY', '299').
card_flavor_text('darkwater egg'/'ODY', '\"Do you thirst for knowledge no matter the cost?\"\n—Watnik, master glassblower').
card_multiverse_id('darkwater egg'/'ODY', '30749').

card_in_set('decaying soil', 'ODY').
card_original_type('decaying soil'/'ODY', 'Enchantment').
card_original_text('decaying soil'/'ODY', 'At the beginning of your upkeep, remove a card in your graveyard from the game.\nThreshold Whenever a nontoken creature is put into your graveyard from play, you may pay {1}. If you do, return that card to your hand. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('decaying soil', 'ODY').
card_image_name('decaying soil'/'ODY', 'decaying soil').
card_uid('decaying soil'/'ODY', 'ODY:Decaying Soil:decaying soil').
card_rarity('decaying soil'/'ODY', 'Rare').
card_artist('decaying soil'/'ODY', 'Don Hazeltine').
card_number('decaying soil'/'ODY', '127').
card_multiverse_id('decaying soil'/'ODY', '29955').

card_in_set('decimate', 'ODY').
card_original_type('decimate'/'ODY', 'Sorcery').
card_original_text('decimate'/'ODY', 'Destroy target artifact, target creature, target enchantment, and target land.').
card_first_print('decimate', 'ODY').
card_image_name('decimate'/'ODY', 'decimate').
card_uid('decimate'/'ODY', 'ODY:Decimate:decimate').
card_rarity('decimate'/'ODY', 'Rare').
card_artist('decimate'/'ODY', 'Alex Horley-Orlandelli').
card_number('decimate'/'ODY', '287').
card_flavor_text('decimate'/'ODY', '\"Anyone can admire creation. Only a barbarian sees the beauty in demolition.\"\n—Kamahl, pit fighter').
card_multiverse_id('decimate'/'ODY', '31798').

card_in_set('decompose', 'ODY').
card_original_type('decompose'/'ODY', 'Sorcery').
card_original_text('decompose'/'ODY', 'Remove up to three target cards in a single graveyard from the game.').
card_first_print('decompose', 'ODY').
card_image_name('decompose'/'ODY', 'decompose').
card_uid('decompose'/'ODY', 'ODY:Decompose:decompose').
card_rarity('decompose'/'ODY', 'Uncommon').
card_artist('decompose'/'ODY', 'Tony Szczudlo').
card_number('decompose'/'ODY', '128').
card_flavor_text('decompose'/'ODY', '\"Sheesh! How am I supposed to make a decent living around here?\"\n—Cabal grave robber').
card_multiverse_id('decompose'/'ODY', '31794').

card_in_set('dedicated martyr', 'ODY').
card_original_type('dedicated martyr'/'ODY', 'Creature — Cleric').
card_original_text('dedicated martyr'/'ODY', '{W}, Sacrifice Dedicated Martyr: You gain 3 life.').
card_first_print('dedicated martyr', 'ODY').
card_image_name('dedicated martyr'/'ODY', 'dedicated martyr').
card_uid('dedicated martyr'/'ODY', 'ODY:Dedicated Martyr:dedicated martyr').
card_rarity('dedicated martyr'/'ODY', 'Common').
card_artist('dedicated martyr'/'ODY', 'Dave Dorman').
card_number('dedicated martyr'/'ODY', '16').
card_flavor_text('dedicated martyr'/'ODY', '\"If he\'s so willing to suffer for his cause, then by all means, let us oblige him.\"\n—Cabal Patriarch').
card_multiverse_id('dedicated martyr'/'ODY', '31771').

card_in_set('deep reconnaissance', 'ODY').
card_original_type('deep reconnaissance'/'ODY', 'Sorcery').
card_original_text('deep reconnaissance'/'ODY', 'Search your library for a basic land card and put that card into play tapped. Then shuffle your library.\nFlashback {4}{G} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('deep reconnaissance', 'ODY').
card_image_name('deep reconnaissance'/'ODY', 'deep reconnaissance').
card_uid('deep reconnaissance'/'ODY', 'ODY:Deep Reconnaissance:deep reconnaissance').
card_rarity('deep reconnaissance'/'ODY', 'Uncommon').
card_artist('deep reconnaissance'/'ODY', 'Jeff Remmer').
card_number('deep reconnaissance'/'ODY', '236').
card_multiverse_id('deep reconnaissance'/'ODY', '29884').

card_in_set('delaying shield', 'ODY').
card_original_type('delaying shield'/'ODY', 'Enchantment').
card_original_text('delaying shield'/'ODY', 'If you would be dealt damage, put that many delay counters on Delaying Shield instead.\nAt the beginning of your upkeep, remove all delay counters from Delaying Shield. For each delay counter removed this way, you lose 1 life unless you pay {1}{W}.').
card_first_print('delaying shield', 'ODY').
card_image_name('delaying shield'/'ODY', 'delaying shield').
card_uid('delaying shield'/'ODY', 'ODY:Delaying Shield:delaying shield').
card_rarity('delaying shield'/'ODY', 'Rare').
card_artist('delaying shield'/'ODY', 'Luca Zontini').
card_number('delaying shield'/'ODY', '17').
card_multiverse_id('delaying shield'/'ODY', '29916').

card_in_set('deluge', 'ODY').
card_original_type('deluge'/'ODY', 'Instant').
card_original_text('deluge'/'ODY', 'Tap all creatures without flying.').
card_first_print('deluge', 'ODY').
card_image_name('deluge'/'ODY', 'deluge').
card_uid('deluge'/'ODY', 'ODY:Deluge:deluge').
card_rarity('deluge'/'ODY', 'Uncommon').
card_artist('deluge'/'ODY', 'Wayne England').
card_number('deluge'/'ODY', '80').
card_flavor_text('deluge'/'ODY', '\"From the sea came all life, and to the sea it will return. The sooner the better.\"\n—Emperor Aboshan').
card_multiverse_id('deluge'/'ODY', '31816').

card_in_set('dematerialize', 'ODY').
card_original_type('dematerialize'/'ODY', 'Sorcery').
card_original_text('dematerialize'/'ODY', 'Return target permanent to its owner\'s hand.\nFlashback {5}{U}{U} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('dematerialize', 'ODY').
card_image_name('dematerialize'/'ODY', 'dematerialize').
card_uid('dematerialize'/'ODY', 'ODY:Dematerialize:dematerialize').
card_rarity('dematerialize'/'ODY', 'Common').
card_artist('dematerialize'/'ODY', 'Mike Ploog').
card_number('dematerialize'/'ODY', '81').
card_multiverse_id('dematerialize'/'ODY', '29826').

card_in_set('demolish', 'ODY').
card_original_type('demolish'/'ODY', 'Sorcery').
card_original_text('demolish'/'ODY', 'Destroy target artifact or land.').
card_first_print('demolish', 'ODY').
card_image_name('demolish'/'ODY', 'demolish').
card_uid('demolish'/'ODY', 'ODY:Demolish:demolish').
card_rarity('demolish'/'ODY', 'Uncommon').
card_artist('demolish'/'ODY', 'Gary Ruddell').
card_number('demolish'/'ODY', '183').
card_flavor_text('demolish'/'ODY', '\"Pound the steel until it fits.\nDoesn\'t work? Bash to bits.\"\n—Dwarven forging song').
card_multiverse_id('demolish'/'ODY', '29867').

card_in_set('demoralize', 'ODY').
card_original_type('demoralize'/'ODY', 'Instant').
card_original_text('demoralize'/'ODY', 'Each creature can\'t be blocked this turn except by two or more creatures. \nThreshold Creatures can\'t block this turn. (You have threshold if seven or more cards are in your graveyard.)').
card_first_print('demoralize', 'ODY').
card_image_name('demoralize'/'ODY', 'demoralize').
card_uid('demoralize'/'ODY', 'ODY:Demoralize:demoralize').
card_rarity('demoralize'/'ODY', 'Common').
card_artist('demoralize'/'ODY', 'Greg & Tim Hildebrandt').
card_number('demoralize'/'ODY', '184').
card_multiverse_id('demoralize'/'ODY', '30545').

card_in_set('deserted temple', 'ODY').
card_original_type('deserted temple'/'ODY', 'Land').
card_original_text('deserted temple'/'ODY', '{T}: Add one colorless mana to your mana pool.\n{1}, {T}: Untap target land.').
card_first_print('deserted temple', 'ODY').
card_image_name('deserted temple'/'ODY', 'deserted temple').
card_uid('deserted temple'/'ODY', 'ODY:Deserted Temple:deserted temple').
card_rarity('deserted temple'/'ODY', 'Rare').
card_artist('deserted temple'/'ODY', 'Rob Alexander').
card_number('deserted temple'/'ODY', '320').
card_flavor_text('deserted temple'/'ODY', 'A monument to long-forgotten gods, it stands as a tribute to the fleeting nature of human ways.').
card_multiverse_id('deserted temple'/'ODY', '30013').

card_in_set('devoted caretaker', 'ODY').
card_original_type('devoted caretaker'/'ODY', 'Creature — Cleric').
card_original_text('devoted caretaker'/'ODY', '{W}, {T}: Target permanent you control gains protection from instant spells and from sorcery spells until end of turn.').
card_first_print('devoted caretaker', 'ODY').
card_image_name('devoted caretaker'/'ODY', 'devoted caretaker').
card_uid('devoted caretaker'/'ODY', 'ODY:Devoted Caretaker:devoted caretaker').
card_rarity('devoted caretaker'/'ODY', 'Rare').
card_artist('devoted caretaker'/'ODY', 'Clyde Caldwell').
card_number('devoted caretaker'/'ODY', '18').
card_flavor_text('devoted caretaker'/'ODY', 'Her eyes blaze with the strength of twenty shields. Every glance is a salvation.').
card_multiverse_id('devoted caretaker'/'ODY', '29909').

card_in_set('diabolic tutor', 'ODY').
card_original_type('diabolic tutor'/'ODY', 'Sorcery').
card_original_text('diabolic tutor'/'ODY', 'Search your library for a card and put that card into your hand. Then shuffle your library.').
card_first_print('diabolic tutor', 'ODY').
card_image_name('diabolic tutor'/'ODY', 'diabolic tutor').
card_uid('diabolic tutor'/'ODY', 'ODY:Diabolic Tutor:diabolic tutor').
card_rarity('diabolic tutor'/'ODY', 'Uncommon').
card_artist('diabolic tutor'/'ODY', 'Rick Farrell').
card_number('diabolic tutor'/'ODY', '129').
card_flavor_text('diabolic tutor'/'ODY', 'The best ideas often come from the worst minds.').
card_multiverse_id('diabolic tutor'/'ODY', '29954').

card_in_set('diligent farmhand', 'ODY').
card_original_type('diligent farmhand'/'ODY', 'Creature — Druid').
card_original_text('diligent farmhand'/'ODY', '{1}{G}, Sacrifice Diligent Farmhand: Search your library for a basic land card and put that card into play tapped. Then shuffle your library.\nIf Diligent Farmhand is in a graveyard, Muscle Burst\'s effect counts it as a Muscle Burst.').
card_first_print('diligent farmhand', 'ODY').
card_image_name('diligent farmhand'/'ODY', 'diligent farmhand').
card_uid('diligent farmhand'/'ODY', 'ODY:Diligent Farmhand:diligent farmhand').
card_rarity('diligent farmhand'/'ODY', 'Common').
card_artist('diligent farmhand'/'ODY', 'Gary Ruddell').
card_number('diligent farmhand'/'ODY', '237').
card_multiverse_id('diligent farmhand'/'ODY', '29773').

card_in_set('dirty wererat', 'ODY').
card_original_type('dirty wererat'/'ODY', 'Creature — Minion Rat').
card_original_text('dirty wererat'/'ODY', '{B}, Discard a card from your hand: Regenerate Dirty Wererat.\nThreshold Dirty Wererat gets +2/+2 and can\'t block. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('dirty wererat', 'ODY').
card_image_name('dirty wererat'/'ODY', 'dirty wererat').
card_uid('dirty wererat'/'ODY', 'ODY:Dirty Wererat:dirty wererat').
card_rarity('dirty wererat'/'ODY', 'Common').
card_artist('dirty wererat'/'ODY', 'Daren Bader').
card_number('dirty wererat'/'ODY', '130').
card_multiverse_id('dirty wererat'/'ODY', '29729').

card_in_set('divert', 'ODY').
card_original_type('divert'/'ODY', 'Instant').
card_original_text('divert'/'ODY', 'Change the target of target spell with a single target unless that spell\'s controller pays {2}.').
card_first_print('divert', 'ODY').
card_image_name('divert'/'ODY', 'divert').
card_uid('divert'/'ODY', 'ODY:Divert:divert').
card_rarity('divert'/'ODY', 'Rare').
card_artist('divert'/'ODY', 'Christopher Moeller').
card_number('divert'/'ODY', '82').
card_flavor_text('divert'/'ODY', '\"What\'s the fun of being a wizard if you can\'t mess with people\'s heads?\"').
card_multiverse_id('divert'/'ODY', '31824').

card_in_set('divine sacrament', 'ODY').
card_original_type('divine sacrament'/'ODY', 'Enchantment').
card_original_text('divine sacrament'/'ODY', 'White creatures get +1/+1.\nThreshold White creatures get an additional +1/+1. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('divine sacrament', 'ODY').
card_image_name('divine sacrament'/'ODY', 'divine sacrament').
card_uid('divine sacrament'/'ODY', 'ODY:Divine Sacrament:divine sacrament').
card_rarity('divine sacrament'/'ODY', 'Rare').
card_artist('divine sacrament'/'ODY', 'Ray Lago').
card_number('divine sacrament'/'ODY', '19').
card_flavor_text('divine sacrament'/'ODY', 'Their whispered prayers rally the nomads better than any war cry could.').
card_multiverse_id('divine sacrament'/'ODY', '29917').

card_in_set('dogged hunter', 'ODY').
card_original_type('dogged hunter'/'ODY', 'Creature — Nomad').
card_original_text('dogged hunter'/'ODY', '{T}: Destroy target creature token.').
card_first_print('dogged hunter', 'ODY').
card_image_name('dogged hunter'/'ODY', 'dogged hunter').
card_uid('dogged hunter'/'ODY', 'ODY:Dogged Hunter:dogged hunter').
card_rarity('dogged hunter'/'ODY', 'Rare').
card_artist('dogged hunter'/'ODY', 'rk post').
card_number('dogged hunter'/'ODY', '20').
card_flavor_text('dogged hunter'/'ODY', 'The relationship between hunter and hunted knows nothing of alliances.').
card_multiverse_id('dogged hunter'/'ODY', '31789').

card_in_set('dreamwinder', 'ODY').
card_original_type('dreamwinder'/'ODY', 'Creature — Serpent').
card_original_text('dreamwinder'/'ODY', 'Dreamwinder can\'t attack unless defending player controls an island.\n{U}, Sacrifice an island: Target land becomes an island until end of turn.').
card_first_print('dreamwinder', 'ODY').
card_image_name('dreamwinder'/'ODY', 'dreamwinder').
card_uid('dreamwinder'/'ODY', 'ODY:Dreamwinder:dreamwinder').
card_rarity('dreamwinder'/'ODY', 'Common').
card_artist('dreamwinder'/'ODY', 'Wayne England').
card_number('dreamwinder'/'ODY', '83').
card_multiverse_id('dreamwinder'/'ODY', '31802').

card_in_set('druid lyrist', 'ODY').
card_original_type('druid lyrist'/'ODY', 'Creature — Druid').
card_original_text('druid lyrist'/'ODY', '{G}, {T}, Sacrifice Druid Lyrist: Destroy target enchantment.').
card_first_print('druid lyrist', 'ODY').
card_image_name('druid lyrist'/'ODY', 'druid lyrist').
card_uid('druid lyrist'/'ODY', 'ODY:Druid Lyrist:druid lyrist').
card_rarity('druid lyrist'/'ODY', 'Common').
card_artist('druid lyrist'/'ODY', 'Mark Zug').
card_number('druid lyrist'/'ODY', '238').
card_flavor_text('druid lyrist'/'ODY', 'The druids and the forest are in perfect harmony. They sing the same songs.').
card_multiverse_id('druid lyrist'/'ODY', '29778').

card_in_set('druid\'s call', 'ODY').
card_original_type('druid\'s call'/'ODY', 'Enchant Creature').
card_original_text('druid\'s call'/'ODY', 'Whenever enchanted creature is dealt damage, its controller puts that many 1/1 green Squirrel creature tokens into play.').
card_first_print('druid\'s call', 'ODY').
card_image_name('druid\'s call'/'ODY', 'druid\'s call').
card_uid('druid\'s call'/'ODY', 'ODY:Druid\'s Call:druid\'s call').
card_rarity('druid\'s call'/'ODY', 'Uncommon').
card_artist('druid\'s call'/'ODY', 'Greg & Tim Hildebrandt').
card_number('druid\'s call'/'ODY', '239').
card_flavor_text('druid\'s call'/'ODY', 'Suffer the little creatures, for they may yet rise up and beat you senseless.').
card_multiverse_id('druid\'s call'/'ODY', '31826').

card_in_set('dusk imp', 'ODY').
card_original_type('dusk imp'/'ODY', 'Creature — Imp').
card_original_text('dusk imp'/'ODY', 'Flying').
card_first_print('dusk imp', 'ODY').
card_image_name('dusk imp'/'ODY', 'dusk imp').
card_uid('dusk imp'/'ODY', 'ODY:Dusk Imp:dusk imp').
card_rarity('dusk imp'/'ODY', 'Common').
card_artist('dusk imp'/'ODY', 'Edward P. Beard, Jr.').
card_number('dusk imp'/'ODY', '131').
card_flavor_text('dusk imp'/'ODY', 'It despises humans and squirrels and beasts and dwarves and cephalids . . . well, it despises just about everything.').
card_multiverse_id('dusk imp'/'ODY', '31807').

card_in_set('dwarven grunt', 'ODY').
card_original_type('dwarven grunt'/'ODY', 'Creature — Dwarf').
card_original_text('dwarven grunt'/'ODY', 'Mountainwalk').
card_first_print('dwarven grunt', 'ODY').
card_image_name('dwarven grunt'/'ODY', 'dwarven grunt').
card_uid('dwarven grunt'/'ODY', 'ODY:Dwarven Grunt:dwarven grunt').
card_rarity('dwarven grunt'/'ODY', 'Common').
card_artist('dwarven grunt'/'ODY', 'Mike Ploog').
card_number('dwarven grunt'/'ODY', '185').
card_flavor_text('dwarven grunt'/'ODY', 'Hard as stone and twice as stubborn.\n—Dwarven compliment').
card_multiverse_id('dwarven grunt'/'ODY', '29760').

card_in_set('dwarven recruiter', 'ODY').
card_original_type('dwarven recruiter'/'ODY', 'Creature — Dwarf').
card_original_text('dwarven recruiter'/'ODY', 'When Dwarven Recruiter comes into play, search your library for any number of Dwarf cards and reveal those cards. Shuffle your library, then put them on top of it in any order.').
card_first_print('dwarven recruiter', 'ODY').
card_image_name('dwarven recruiter'/'ODY', 'dwarven recruiter').
card_uid('dwarven recruiter'/'ODY', 'ODY:Dwarven Recruiter:dwarven recruiter').
card_rarity('dwarven recruiter'/'ODY', 'Uncommon').
card_artist('dwarven recruiter'/'ODY', 'Ciruelo').
card_number('dwarven recruiter'/'ODY', '186').
card_multiverse_id('dwarven recruiter'/'ODY', '29853').

card_in_set('dwarven shrine', 'ODY').
card_original_type('dwarven shrine'/'ODY', 'Enchantment').
card_original_text('dwarven shrine'/'ODY', 'Whenever a player plays a spell, Dwarven Shrine deals X damage to that player, where X is twice the number of cards in all graveyards with the same name as that spell.').
card_first_print('dwarven shrine', 'ODY').
card_image_name('dwarven shrine'/'ODY', 'dwarven shrine').
card_uid('dwarven shrine'/'ODY', 'ODY:Dwarven Shrine:dwarven shrine').
card_rarity('dwarven shrine'/'ODY', 'Rare').
card_artist('dwarven shrine'/'ODY', 'Matt Cavotta').
card_number('dwarven shrine'/'ODY', '187').
card_multiverse_id('dwarven shrine'/'ODY', '29977').

card_in_set('dwarven strike force', 'ODY').
card_original_type('dwarven strike force'/'ODY', 'Creature — Dwarf').
card_original_text('dwarven strike force'/'ODY', 'Discard a card at random from your hand: Dwarven Strike Force gains first strike and haste until end of turn.').
card_first_print('dwarven strike force', 'ODY').
card_image_name('dwarven strike force'/'ODY', 'dwarven strike force').
card_uid('dwarven strike force'/'ODY', 'ODY:Dwarven Strike Force:dwarven strike force').
card_rarity('dwarven strike force'/'ODY', 'Uncommon').
card_artist('dwarven strike force'/'ODY', 'Mike Ploog').
card_number('dwarven strike force'/'ODY', '188').
card_flavor_text('dwarven strike force'/'ODY', 'Any given foothill may hide dozens of dwarves waiting to spill from their tunnels like fire ants.').
card_multiverse_id('dwarven strike force'/'ODY', '31796').

card_in_set('earnest fellowship', 'ODY').
card_original_type('earnest fellowship'/'ODY', 'Enchantment').
card_original_text('earnest fellowship'/'ODY', 'Each creature has protection from its colors.').
card_first_print('earnest fellowship', 'ODY').
card_image_name('earnest fellowship'/'ODY', 'earnest fellowship').
card_uid('earnest fellowship'/'ODY', 'ODY:Earnest Fellowship:earnest fellowship').
card_rarity('earnest fellowship'/'ODY', 'Rare').
card_artist('earnest fellowship'/'ODY', 'Heather Hudson').
card_number('earnest fellowship'/'ODY', '21').
card_flavor_text('earnest fellowship'/'ODY', '\"The world has enough strife as it is. Where would we be if we turned against each other?\"\n—Pianna, nomad captain').
card_multiverse_id('earnest fellowship'/'ODY', '29918').

card_in_set('earth rift', 'ODY').
card_original_type('earth rift'/'ODY', 'Sorcery').
card_original_text('earth rift'/'ODY', 'Destroy target land.\nFlashback {5}{R}{R} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('earth rift', 'ODY').
card_image_name('earth rift'/'ODY', 'earth rift').
card_uid('earth rift'/'ODY', 'ODY:Earth Rift:earth rift').
card_rarity('earth rift'/'ODY', 'Common').
card_artist('earth rift'/'ODY', 'Wayne England').
card_number('earth rift'/'ODY', '189').
card_multiverse_id('earth rift'/'ODY', '29769').

card_in_set('elephant ambush', 'ODY').
card_original_type('elephant ambush'/'ODY', 'Instant').
card_original_text('elephant ambush'/'ODY', 'Put a 3/3 green Elephant creature token into play.\nFlashback {6}{G}{G} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('elephant ambush', 'ODY').
card_image_name('elephant ambush'/'ODY', 'elephant ambush').
card_uid('elephant ambush'/'ODY', 'ODY:Elephant Ambush:elephant ambush').
card_rarity('elephant ambush'/'ODY', 'Common').
card_artist('elephant ambush'/'ODY', 'Anthony S. Waters').
card_number('elephant ambush'/'ODY', '240').
card_multiverse_id('elephant ambush'/'ODY', '32919').

card_in_set('ember beast', 'ODY').
card_original_type('ember beast'/'ODY', 'Creature — Beast').
card_original_text('ember beast'/'ODY', 'Ember Beast can\'t attack or block alone.').
card_first_print('ember beast', 'ODY').
card_image_name('ember beast'/'ODY', 'ember beast').
card_uid('ember beast'/'ODY', 'ODY:Ember Beast:ember beast').
card_rarity('ember beast'/'ODY', 'Common').
card_artist('ember beast'/'ODY', 'Wayne England').
card_number('ember beast'/'ODY', '190').
card_flavor_text('ember beast'/'ODY', 'Spot one ember beast, attack.\nSpot a second, best fall back.\n—Dwarven saying').
card_multiverse_id('ember beast'/'ODY', '29752').

card_in_set('embolden', 'ODY').
card_original_type('embolden'/'ODY', 'Instant').
card_original_text('embolden'/'ODY', 'Prevent the next 4 damage that would be dealt this turn to any number of target creatures and/or players, divided as you choose.\nFlashback {1}{W} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('embolden', 'ODY').
card_image_name('embolden'/'ODY', 'embolden').
card_uid('embolden'/'ODY', 'ODY:Embolden:embolden').
card_rarity('embolden'/'ODY', 'Common').
card_artist('embolden'/'ODY', 'Rebecca Guay').
card_number('embolden'/'ODY', '22').
card_multiverse_id('embolden'/'ODY', '29703').

card_in_set('engulfing flames', 'ODY').
card_original_type('engulfing flames'/'ODY', 'Instant').
card_original_text('engulfing flames'/'ODY', 'Engulfing Flames deals 1 damage to target creature. It can\'t be regenerated this turn.\nFlashback {3}{R} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('engulfing flames', 'ODY').
card_image_name('engulfing flames'/'ODY', 'engulfing flames').
card_uid('engulfing flames'/'ODY', 'ODY:Engulfing Flames:engulfing flames').
card_rarity('engulfing flames'/'ODY', 'Uncommon').
card_artist('engulfing flames'/'ODY', 'Marc Fishman').
card_number('engulfing flames'/'ODY', '191').
card_multiverse_id('engulfing flames'/'ODY', '31810').

card_in_set('entomb', 'ODY').
card_original_type('entomb'/'ODY', 'Instant').
card_original_text('entomb'/'ODY', 'Search your library for a card and put that card into your graveyard. Then shuffle your library.').
card_image_name('entomb'/'ODY', 'entomb').
card_uid('entomb'/'ODY', 'ODY:Entomb:entomb').
card_rarity('entomb'/'ODY', 'Rare').
card_artist('entomb'/'ODY', 'Ron Spears').
card_number('entomb'/'ODY', '132').
card_flavor_text('entomb'/'ODY', 'A grave is the safest place to store ill-gotten treasures.').
card_multiverse_id('entomb'/'ODY', '30552').

card_in_set('epicenter', 'ODY').
card_original_type('epicenter'/'ODY', 'Sorcery').
card_original_text('epicenter'/'ODY', 'Target player sacrifices a land.\nThreshold All players sacrifice all lands instead. (You have threshold if seven or more cards are in your graveyard.)').
card_first_print('epicenter', 'ODY').
card_image_name('epicenter'/'ODY', 'epicenter').
card_uid('epicenter'/'ODY', 'ODY:Epicenter:epicenter').
card_rarity('epicenter'/'ODY', 'Rare').
card_artist('epicenter'/'ODY', 'Anthony S. Waters').
card_number('epicenter'/'ODY', '192').
card_flavor_text('epicenter'/'ODY', 'Sometimes destruction is Dominaria\'s way of rebuilding. Sometimes it\'s not.').
card_multiverse_id('epicenter'/'ODY', '30547').

card_in_set('escape artist', 'ODY').
card_original_type('escape artist'/'ODY', 'Creature — Wizard').
card_original_text('escape artist'/'ODY', 'Escape Artist is unblockable.\n{U}, Discard a card from your hand: Return Escape Artist to its owner\'s hand.').
card_first_print('escape artist', 'ODY').
card_image_name('escape artist'/'ODY', 'escape artist').
card_uid('escape artist'/'ODY', 'ODY:Escape Artist:escape artist').
card_rarity('escape artist'/'ODY', 'Common').
card_artist('escape artist'/'ODY', 'Scott M. Fischer').
card_number('escape artist'/'ODY', '84').
card_flavor_text('escape artist'/'ODY', '\"Not even my shadow knows where I am.\"').
card_multiverse_id('escape artist'/'ODY', '31813').

card_in_set('execute', 'ODY').
card_original_type('execute'/'ODY', 'Instant').
card_original_text('execute'/'ODY', 'Destroy target white creature. It can\'t be regenerated.\nDraw a card.').
card_first_print('execute', 'ODY').
card_image_name('execute'/'ODY', 'execute').
card_uid('execute'/'ODY', 'ODY:Execute:execute').
card_rarity('execute'/'ODY', 'Uncommon').
card_artist('execute'/'ODY', 'Gary Ruddell').
card_number('execute'/'ODY', '133').
card_flavor_text('execute'/'ODY', '\"Any fool who would die for honor is better off dead.\"\n—Cabal Patriarch').
card_multiverse_id('execute'/'ODY', '30558').

card_in_set('extract', 'ODY').
card_original_type('extract'/'ODY', 'Sorcery').
card_original_text('extract'/'ODY', 'Search target player\'s library for a card and remove that card from the game. Then that player shuffles his or her library.').
card_first_print('extract', 'ODY').
card_image_name('extract'/'ODY', 'extract').
card_uid('extract'/'ODY', 'ODY:Extract:extract').
card_rarity('extract'/'ODY', 'Rare').
card_artist('extract'/'ODY', 'Matt Cavotta').
card_number('extract'/'ODY', '85').
card_flavor_text('extract'/'ODY', 'To say Nixar had a splitting headache would be a bit of an understatement.').
card_multiverse_id('extract'/'ODY', '29849').

card_in_set('face of fear', 'ODY').
card_original_type('face of fear'/'ODY', 'Creature — Horror').
card_original_text('face of fear'/'ODY', '{2}{B}, Discard a card from your hand: Face of Fear can\'t be blocked this turn except by artifact creatures and/or black creatures.').
card_first_print('face of fear', 'ODY').
card_image_name('face of fear'/'ODY', 'face of fear').
card_uid('face of fear'/'ODY', 'ODY:Face of Fear:face of fear').
card_rarity('face of fear'/'ODY', 'Uncommon').
card_artist('face of fear'/'ODY', 'Thomas M. Baxa').
card_number('face of fear'/'ODY', '134').
card_flavor_text('face of fear'/'ODY', '\"It\'s only frightened five people to death. Not my best work.\"\n—Braids, dementia summoner').
card_multiverse_id('face of fear'/'ODY', '31738').

card_in_set('famished ghoul', 'ODY').
card_original_type('famished ghoul'/'ODY', 'Creature — Zombie').
card_original_text('famished ghoul'/'ODY', '{1}{B}, Sacrifice Famished Ghoul: Remove up to two target cards in a single graveyard from the game.').
card_first_print('famished ghoul', 'ODY').
card_image_name('famished ghoul'/'ODY', 'famished ghoul').
card_uid('famished ghoul'/'ODY', 'ODY:Famished Ghoul:famished ghoul').
card_rarity('famished ghoul'/'ODY', 'Uncommon').
card_artist('famished ghoul'/'ODY', 'Adam Rex').
card_number('famished ghoul'/'ODY', '135').
card_flavor_text('famished ghoul'/'ODY', 'Memories have distinct tastes. Its favorites are the sickly sweetness of lost love and the bitter smokiness of cruel murder.').
card_multiverse_id('famished ghoul'/'ODY', '29736').

card_in_set('fervent denial', 'ODY').
card_original_type('fervent denial'/'ODY', 'Instant').
card_original_text('fervent denial'/'ODY', 'Counter target spell.\nFlashback {5}{U}{U} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('fervent denial', 'ODY').
card_image_name('fervent denial'/'ODY', 'fervent denial').
card_uid('fervent denial'/'ODY', 'ODY:Fervent Denial:fervent denial').
card_rarity('fervent denial'/'ODY', 'Uncommon').
card_artist('fervent denial'/'ODY', 'Scott M. Fischer').
card_number('fervent denial'/'ODY', '86').
card_flavor_text('fervent denial'/'ODY', '\"Your tenacity is admirable. And futile.\"\n—Emperor Aboshan').
card_multiverse_id('fervent denial'/'ODY', '29724').

card_in_set('filthy cur', 'ODY').
card_original_type('filthy cur'/'ODY', 'Creature — Hound').
card_original_text('filthy cur'/'ODY', 'Whenever Filthy Cur is dealt damage, you lose that much life.').
card_first_print('filthy cur', 'ODY').
card_image_name('filthy cur'/'ODY', 'filthy cur').
card_uid('filthy cur'/'ODY', 'ODY:Filthy Cur:filthy cur').
card_rarity('filthy cur'/'ODY', 'Common').
card_artist('filthy cur'/'ODY', 'Adam Rex').
card_number('filthy cur'/'ODY', '136').
card_flavor_text('filthy cur'/'ODY', 'Some things even fleas won\'t bite.').
card_multiverse_id('filthy cur'/'ODY', '31804').

card_in_set('firebolt', 'ODY').
card_original_type('firebolt'/'ODY', 'Sorcery').
card_original_text('firebolt'/'ODY', 'Firebolt deals 2 damage to target creature or player.\nFlashback {4}{R} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_image_name('firebolt'/'ODY', 'firebolt').
card_uid('firebolt'/'ODY', 'ODY:Firebolt:firebolt').
card_rarity('firebolt'/'ODY', 'Common').
card_artist('firebolt'/'ODY', 'Ron Spencer').
card_number('firebolt'/'ODY', '193').
card_flavor_text('firebolt'/'ODY', 'Reach out and torch someone.').
card_multiverse_id('firebolt'/'ODY', '30570').

card_in_set('flame burst', 'ODY').
card_original_type('flame burst'/'ODY', 'Instant').
card_original_text('flame burst'/'ODY', 'Flame Burst deals X damage to target creature or player, where X is 2 plus the number of Flame Burst cards in all graveyards.').
card_first_print('flame burst', 'ODY').
card_image_name('flame burst'/'ODY', 'flame burst').
card_uid('flame burst'/'ODY', 'ODY:Flame Burst:flame burst').
card_rarity('flame burst'/'ODY', 'Common').
card_artist('flame burst'/'ODY', 'Ron Spencer').
card_number('flame burst'/'ODY', '194').
card_flavor_text('flame burst'/'ODY', 'As hot as a dwarf\'s rage.').
card_multiverse_id('flame burst'/'ODY', '29767').

card_in_set('fledgling imp', 'ODY').
card_original_type('fledgling imp'/'ODY', 'Creature — Imp').
card_original_text('fledgling imp'/'ODY', '{B}, Discard a card from your hand: Fledgling Imp gains flying until end of turn.').
card_first_print('fledgling imp', 'ODY').
card_image_name('fledgling imp'/'ODY', 'fledgling imp').
card_uid('fledgling imp'/'ODY', 'ODY:Fledgling Imp:fledgling imp').
card_rarity('fledgling imp'/'ODY', 'Common').
card_artist('fledgling imp'/'ODY', 'John Matson').
card_number('fledgling imp'/'ODY', '137').
card_flavor_text('fledgling imp'/'ODY', 'Imps aren\'t born knowing how to fly—just how to annoy.').
card_multiverse_id('fledgling imp'/'ODY', '31777').

card_in_set('forest', 'ODY').
card_original_type('forest'/'ODY', 'Land').
card_original_text('forest'/'ODY', 'G').
card_image_name('forest'/'ODY', 'forest1').
card_uid('forest'/'ODY', 'ODY:Forest:forest1').
card_rarity('forest'/'ODY', 'Basic Land').
card_artist('forest'/'ODY', 'Larry Elmore').
card_number('forest'/'ODY', '347').
card_multiverse_id('forest'/'ODY', '31625').

card_in_set('forest', 'ODY').
card_original_type('forest'/'ODY', 'Land').
card_original_text('forest'/'ODY', 'G').
card_image_name('forest'/'ODY', 'forest2').
card_uid('forest'/'ODY', 'ODY:Forest:forest2').
card_rarity('forest'/'ODY', 'Basic Land').
card_artist('forest'/'ODY', 'Rob Alexander').
card_number('forest'/'ODY', '348').
card_multiverse_id('forest'/'ODY', '31626').

card_in_set('forest', 'ODY').
card_original_type('forest'/'ODY', 'Land').
card_original_text('forest'/'ODY', 'G').
card_image_name('forest'/'ODY', 'forest3').
card_uid('forest'/'ODY', 'ODY:Forest:forest3').
card_rarity('forest'/'ODY', 'Basic Land').
card_artist('forest'/'ODY', 'Jerry Tiritilli').
card_number('forest'/'ODY', '349').
card_multiverse_id('forest'/'ODY', '31627').

card_in_set('forest', 'ODY').
card_original_type('forest'/'ODY', 'Land').
card_original_text('forest'/'ODY', 'G').
card_image_name('forest'/'ODY', 'forest4').
card_uid('forest'/'ODY', 'ODY:Forest:forest4').
card_rarity('forest'/'ODY', 'Basic Land').
card_artist('forest'/'ODY', 'Tony Szczudlo').
card_number('forest'/'ODY', '350').
card_multiverse_id('forest'/'ODY', '31628').

card_in_set('frenetic ogre', 'ODY').
card_original_type('frenetic ogre'/'ODY', 'Creature — Ogre').
card_original_text('frenetic ogre'/'ODY', '{R}, Discard a card at random from your hand: Frenetic Ogre gets +3/+0 until end of turn.').
card_first_print('frenetic ogre', 'ODY').
card_image_name('frenetic ogre'/'ODY', 'frenetic ogre').
card_uid('frenetic ogre'/'ODY', 'ODY:Frenetic Ogre:frenetic ogre').
card_rarity('frenetic ogre'/'ODY', 'Uncommon').
card_artist('frenetic ogre'/'ODY', 'Ron Spears').
card_number('frenetic ogre'/'ODY', '195').
card_flavor_text('frenetic ogre'/'ODY', 'The best thing about ogres is that they never try to sneak up on you.').
card_multiverse_id('frenetic ogre'/'ODY', '29855').

card_in_set('frightcrawler', 'ODY').
card_original_type('frightcrawler'/'ODY', 'Creature — Horror').
card_original_text('frightcrawler'/'ODY', 'Frightcrawler can\'t be blocked except by artifact creatures and/or black creatures.\nThreshold Frightcrawler gets +2/+2 and can\'t block. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('frightcrawler', 'ODY').
card_image_name('frightcrawler'/'ODY', 'frightcrawler').
card_uid('frightcrawler'/'ODY', 'ODY:Frightcrawler:frightcrawler').
card_rarity('frightcrawler'/'ODY', 'Common').
card_artist('frightcrawler'/'ODY', 'Matt Cavotta').
card_number('frightcrawler'/'ODY', '138').
card_multiverse_id('frightcrawler'/'ODY', '29733').

card_in_set('gallantry', 'ODY').
card_original_type('gallantry'/'ODY', 'Instant').
card_original_text('gallantry'/'ODY', 'Target blocking creature gets +4/+4 until end of turn.\nDraw a card.').
card_image_name('gallantry'/'ODY', 'gallantry').
card_uid('gallantry'/'ODY', 'ODY:Gallantry:gallantry').
card_rarity('gallantry'/'ODY', 'Uncommon').
card_artist('gallantry'/'ODY', 'Mark Tedin').
card_number('gallantry'/'ODY', '23').
card_flavor_text('gallantry'/'ODY', 'The courageous accomplish more in a single moment than the cowardly do in a lifetime.').
card_multiverse_id('gallantry'/'ODY', '28755').

card_in_set('ghastly demise', 'ODY').
card_original_type('ghastly demise'/'ODY', 'Instant').
card_original_text('ghastly demise'/'ODY', 'Destroy target nonblack creature if its toughness is less than or equal to the number of cards in your graveyard.').
card_first_print('ghastly demise', 'ODY').
card_image_name('ghastly demise'/'ODY', 'ghastly demise').
card_uid('ghastly demise'/'ODY', 'ODY:Ghastly Demise:ghastly demise').
card_rarity('ghastly demise'/'ODY', 'Common').
card_artist('ghastly demise'/'ODY', 'Gary Ruddell').
card_number('ghastly demise'/'ODY', '139').
card_flavor_text('ghastly demise'/'ODY', '\"Anyone can snap her fingers and yank a soul. I prefer to kill creatively.\"\n—Braids, dementia summoner').
card_multiverse_id('ghastly demise'/'ODY', '29744').

card_in_set('gorilla titan', 'ODY').
card_original_type('gorilla titan'/'ODY', 'Creature — Ape').
card_original_text('gorilla titan'/'ODY', 'Trample  \nGorilla Titan gets +4/+4 as long as there are no cards in your graveyard.').
card_first_print('gorilla titan', 'ODY').
card_image_name('gorilla titan'/'ODY', 'gorilla titan').
card_uid('gorilla titan'/'ODY', 'ODY:Gorilla Titan:gorilla titan').
card_rarity('gorilla titan'/'ODY', 'Uncommon').
card_artist('gorilla titan'/'ODY', 'Heather Hudson').
card_number('gorilla titan'/'ODY', '241').
card_flavor_text('gorilla titan'/'ODY', '\"I want a banana this big!\"').
card_multiverse_id('gorilla titan'/'ODY', '29980').

card_in_set('graceful antelope', 'ODY').
card_original_type('graceful antelope'/'ODY', 'Creature — Antelope').
card_original_text('graceful antelope'/'ODY', 'Plainswalk\nWhenever Graceful Antelope deals combat damage to a player, you may have target land become a plains until Graceful Antelope leaves play.').
card_first_print('graceful antelope', 'ODY').
card_image_name('graceful antelope'/'ODY', 'graceful antelope').
card_uid('graceful antelope'/'ODY', 'ODY:Graceful Antelope:graceful antelope').
card_rarity('graceful antelope'/'ODY', 'Rare').
card_artist('graceful antelope'/'ODY', 'Heather Hudson').
card_number('graceful antelope'/'ODY', '24').
card_multiverse_id('graceful antelope'/'ODY', '29913').

card_in_set('gravedigger', 'ODY').
card_original_type('gravedigger'/'ODY', 'Creature — Zombie').
card_original_text('gravedigger'/'ODY', 'When Gravedigger comes into play, you may return target creature card from your graveyard to your hand.').
card_image_name('gravedigger'/'ODY', 'gravedigger').
card_uid('gravedigger'/'ODY', 'ODY:Gravedigger:gravedigger').
card_rarity('gravedigger'/'ODY', 'Common').
card_artist('gravedigger'/'ODY', 'Tony Szczudlo').
card_number('gravedigger'/'ODY', '140').
card_flavor_text('gravedigger'/'ODY', 'Oddly, no one questioned the Cabal\'s wisdom in hiring a zombie to work at the cemetery.').
card_multiverse_id('gravedigger'/'ODY', '29741').

card_in_set('gravestorm', 'ODY').
card_original_type('gravestorm'/'ODY', 'Enchantment').
card_original_text('gravestorm'/'ODY', 'At the beginning of your upkeep, target opponent may remove a card in his or her graveyard from the game. If that player doesn\'t, you may draw a card.').
card_first_print('gravestorm', 'ODY').
card_image_name('gravestorm'/'ODY', 'gravestorm').
card_uid('gravestorm'/'ODY', 'ODY:Gravestorm:gravestorm').
card_rarity('gravestorm'/'ODY', 'Rare').
card_artist('gravestorm'/'ODY', 'Alex Horley-Orlandelli').
card_number('gravestorm'/'ODY', '141').
card_multiverse_id('gravestorm'/'ODY', '29935').

card_in_set('ground seal', 'ODY').
card_original_type('ground seal'/'ODY', 'Enchantment').
card_original_text('ground seal'/'ODY', 'When Ground Seal comes into play, draw a card.\nCards in graveyards can\'t be the targets of spells or abilities.').
card_first_print('ground seal', 'ODY').
card_image_name('ground seal'/'ODY', 'ground seal').
card_uid('ground seal'/'ODY', 'ODY:Ground Seal:ground seal').
card_rarity('ground seal'/'ODY', 'Rare').
card_artist('ground seal'/'ODY', 'Edward P. Beard, Jr.').
card_number('ground seal'/'ODY', '242').
card_flavor_text('ground seal'/'ODY', '\"Let all we have buried be buried forever.\"\n—Seton, centaur druid').
card_multiverse_id('ground seal'/'ODY', '29991').

card_in_set('halberdier', 'ODY').
card_original_type('halberdier'/'ODY', 'Creature — Barbarian').
card_original_text('halberdier'/'ODY', 'First strike').
card_first_print('halberdier', 'ODY').
card_image_name('halberdier'/'ODY', 'halberdier').
card_uid('halberdier'/'ODY', 'ODY:Halberdier:halberdier').
card_rarity('halberdier'/'ODY', 'Common').
card_artist('halberdier'/'ODY', 'Ben Thompson').
card_number('halberdier'/'ODY', '196').
card_flavor_text('halberdier'/'ODY', 'His reputation precedes him—and so does his weapon.').
card_multiverse_id('halberdier'/'ODY', '29751').

card_in_set('hallowed healer', 'ODY').
card_original_type('hallowed healer'/'ODY', 'Creature — Cleric').
card_original_text('hallowed healer'/'ODY', '{T}: Prevent the next 2 damage that would be dealt to target creature or player this turn.\nThreshold {T}: Prevent the next 4 damage that would be dealt to target creature or player this turn. (Play this ability only if seven or more cards are in your graveyard.)').
card_first_print('hallowed healer', 'ODY').
card_image_name('hallowed healer'/'ODY', 'hallowed healer').
card_uid('hallowed healer'/'ODY', 'ODY:Hallowed Healer:hallowed healer').
card_rarity('hallowed healer'/'ODY', 'Common').
card_artist('hallowed healer'/'ODY', 'Ben Thompson').
card_number('hallowed healer'/'ODY', '25').
card_multiverse_id('hallowed healer'/'ODY', '29693').

card_in_set('haunting echoes', 'ODY').
card_original_type('haunting echoes'/'ODY', 'Sorcery').
card_original_text('haunting echoes'/'ODY', 'Remove all cards in target player\'s graveyard other than basic land cards from the game. Search that player\'s library for all cards with the same name as cards removed this way and remove them from the game. Then that player shuffles his or her library.').
card_first_print('haunting echoes', 'ODY').
card_image_name('haunting echoes'/'ODY', 'haunting echoes').
card_uid('haunting echoes'/'ODY', 'ODY:Haunting Echoes:haunting echoes').
card_rarity('haunting echoes'/'ODY', 'Rare').
card_artist('haunting echoes'/'ODY', 'Arnie Swekel').
card_number('haunting echoes'/'ODY', '142').
card_multiverse_id('haunting echoes'/'ODY', '29956').

card_in_set('hint of insanity', 'ODY').
card_original_type('hint of insanity'/'ODY', 'Sorcery').
card_original_text('hint of insanity'/'ODY', 'Target player reveals his or her hand. That player discards from it all nonland cards with the same name as another card in his or her hand.').
card_first_print('hint of insanity', 'ODY').
card_image_name('hint of insanity'/'ODY', 'hint of insanity').
card_uid('hint of insanity'/'ODY', 'ODY:Hint of Insanity:hint of insanity').
card_rarity('hint of insanity'/'ODY', 'Rare').
card_artist('hint of insanity'/'ODY', 'Luca Zontini').
card_number('hint of insanity'/'ODY', '143').
card_flavor_text('hint of insanity'/'ODY', '\"Practice makes perfect, but obsession makes better.\"').
card_multiverse_id('hint of insanity'/'ODY', '31867').

card_in_set('holistic wisdom', 'ODY').
card_original_type('holistic wisdom'/'ODY', 'Enchantment').
card_original_text('holistic wisdom'/'ODY', '{2}, Remove a card in your hand from the game: Return target card from your graveyard to your hand if it shares a type with the card removed this way. (The card types are artifact, creature, enchantment, instant, land, and sorcery.)').
card_first_print('holistic wisdom', 'ODY').
card_image_name('holistic wisdom'/'ODY', 'holistic wisdom').
card_uid('holistic wisdom'/'ODY', 'ODY:Holistic Wisdom:holistic wisdom').
card_rarity('holistic wisdom'/'ODY', 'Rare').
card_artist('holistic wisdom'/'ODY', 'Rebecca Guay').
card_number('holistic wisdom'/'ODY', '243').
card_multiverse_id('holistic wisdom'/'ODY', '29994').

card_in_set('howling gale', 'ODY').
card_original_type('howling gale'/'ODY', 'Instant').
card_original_text('howling gale'/'ODY', 'Howling Gale deals 1 damage to each creature with flying and each player.\nFlashback {1}{G} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('howling gale', 'ODY').
card_image_name('howling gale'/'ODY', 'howling gale').
card_uid('howling gale'/'ODY', 'ODY:Howling Gale:howling gale').
card_rarity('howling gale'/'ODY', 'Uncommon').
card_artist('howling gale'/'ODY', 'Greg Staples').
card_number('howling gale'/'ODY', '244').
card_multiverse_id('howling gale'/'ODY', '29776').

card_in_set('immobilizing ink', 'ODY').
card_original_type('immobilizing ink'/'ODY', 'Enchant Creature').
card_original_text('immobilizing ink'/'ODY', 'Enchanted creature doesn\'t untap during its controller\'s untap step.\nEnchanted creature has \"{1}, Discard a card from your hand: Untap this creature.\"').
card_first_print('immobilizing ink', 'ODY').
card_image_name('immobilizing ink'/'ODY', 'immobilizing ink').
card_uid('immobilizing ink'/'ODY', 'ODY:Immobilizing Ink:immobilizing ink').
card_rarity('immobilizing ink'/'ODY', 'Common').
card_artist('immobilizing ink'/'ODY', 'Darrell Riche').
card_number('immobilizing ink'/'ODY', '87').
card_multiverse_id('immobilizing ink'/'ODY', '29828').

card_in_set('impulsive maneuvers', 'ODY').
card_original_type('impulsive maneuvers'/'ODY', 'Enchantment').
card_original_text('impulsive maneuvers'/'ODY', 'Whenever a creature attacks, flip a coin. If you win the flip, the next time that creature would deal combat damage this turn, it deals double that damage instead. If you lose the flip, the next time that creature would deal combat damage this turn, prevent that damage.').
card_first_print('impulsive maneuvers', 'ODY').
card_image_name('impulsive maneuvers'/'ODY', 'impulsive maneuvers').
card_uid('impulsive maneuvers'/'ODY', 'ODY:Impulsive Maneuvers:impulsive maneuvers').
card_rarity('impulsive maneuvers'/'ODY', 'Rare').
card_artist('impulsive maneuvers'/'ODY', 'Dave Dorman').
card_number('impulsive maneuvers'/'ODY', '197').
card_multiverse_id('impulsive maneuvers'/'ODY', '31787').

card_in_set('infected vermin', 'ODY').
card_original_type('infected vermin'/'ODY', 'Creature — Rat').
card_original_text('infected vermin'/'ODY', '{2}{B}: Infected Vermin deals 1 damage to each creature and each player. \nThreshold {3}{B}: Infected Vermin deals 3 damage to each creature and each player. (Play this ability only if seven or more cards are in your graveyard.)').
card_first_print('infected vermin', 'ODY').
card_image_name('infected vermin'/'ODY', 'infected vermin').
card_uid('infected vermin'/'ODY', 'ODY:Infected Vermin:infected vermin').
card_rarity('infected vermin'/'ODY', 'Uncommon').
card_artist('infected vermin'/'ODY', 'Anthony S. Waters').
card_number('infected vermin'/'ODY', '144').
card_multiverse_id('infected vermin'/'ODY', '31808').

card_in_set('innocent blood', 'ODY').
card_original_type('innocent blood'/'ODY', 'Sorcery').
card_original_text('innocent blood'/'ODY', 'Each player sacrifices a creature.').
card_first_print('innocent blood', 'ODY').
card_image_name('innocent blood'/'ODY', 'innocent blood').
card_uid('innocent blood'/'ODY', 'ODY:Innocent Blood:innocent blood').
card_rarity('innocent blood'/'ODY', 'Common').
card_artist('innocent blood'/'ODY', 'Carl Critchlow').
card_number('innocent blood'/'ODY', '145').
card_flavor_text('innocent blood'/'ODY', 'Zombies mourn for the living and celebrate those who will soon be given the gift of death.').
card_multiverse_id('innocent blood'/'ODY', '29818').

card_in_set('iridescent angel', 'ODY').
card_original_type('iridescent angel'/'ODY', 'Creature — Angel').
card_original_text('iridescent angel'/'ODY', 'Flying, protection from all colors').
card_first_print('iridescent angel', 'ODY').
card_image_name('iridescent angel'/'ODY', 'iridescent angel').
card_uid('iridescent angel'/'ODY', 'ODY:Iridescent Angel:iridescent angel').
card_rarity('iridescent angel'/'ODY', 'Rare').
card_artist('iridescent angel'/'ODY', 'Matt Cavotta').
card_number('iridescent angel'/'ODY', '288').
card_flavor_text('iridescent angel'/'ODY', 'She enraptures all, encompasses all, endures all.').
card_multiverse_id('iridescent angel'/'ODY', '31799').

card_in_set('island', 'ODY').
card_original_type('island'/'ODY', 'Land').
card_original_text('island'/'ODY', 'U').
card_image_name('island'/'ODY', 'island1').
card_uid('island'/'ODY', 'ODY:Island:island1').
card_rarity('island'/'ODY', 'Basic Land').
card_artist('island'/'ODY', 'Alan Pollack').
card_number('island'/'ODY', '335').
card_multiverse_id('island'/'ODY', '31613').

card_in_set('island', 'ODY').
card_original_type('island'/'ODY', 'Land').
card_original_text('island'/'ODY', 'U').
card_image_name('island'/'ODY', 'island2').
card_uid('island'/'ODY', 'ODY:Island:island2').
card_rarity('island'/'ODY', 'Basic Land').
card_artist('island'/'ODY', 'Ben Thompson').
card_number('island'/'ODY', '336').
card_multiverse_id('island'/'ODY', '31614').

card_in_set('island', 'ODY').
card_original_type('island'/'ODY', 'Land').
card_original_text('island'/'ODY', 'U').
card_image_name('island'/'ODY', 'island3').
card_uid('island'/'ODY', 'ODY:Island:island3').
card_rarity('island'/'ODY', 'Basic Land').
card_artist('island'/'ODY', 'Larry Elmore').
card_number('island'/'ODY', '337').
card_multiverse_id('island'/'ODY', '31615').

card_in_set('island', 'ODY').
card_original_type('island'/'ODY', 'Land').
card_original_text('island'/'ODY', 'U').
card_image_name('island'/'ODY', 'island4').
card_uid('island'/'ODY', 'ODY:Island:island4').
card_rarity('island'/'ODY', 'Basic Land').
card_artist('island'/'ODY', 'Rob Alexander').
card_number('island'/'ODY', '338').
card_multiverse_id('island'/'ODY', '31616').

card_in_set('ivy elemental', 'ODY').
card_original_type('ivy elemental'/'ODY', 'Creature — Elemental').
card_original_text('ivy elemental'/'ODY', 'Ivy Elemental comes into play with X +1/+1 counters on it.').
card_first_print('ivy elemental', 'ODY').
card_image_name('ivy elemental'/'ODY', 'ivy elemental').
card_uid('ivy elemental'/'ODY', 'ODY:Ivy Elemental:ivy elemental').
card_rarity('ivy elemental'/'ODY', 'Rare').
card_artist('ivy elemental'/'ODY', 'Ron Spencer').
card_number('ivy elemental'/'ODY', '245').
card_flavor_text('ivy elemental'/'ODY', 'In the gardens of the centaurs, many travelers have mysteriously vanished while admiring the elaborate topiaries.').
card_multiverse_id('ivy elemental'/'ODY', '29871').

card_in_set('junk golem', 'ODY').
card_original_type('junk golem'/'ODY', 'Artifact Creature — Golem').
card_original_text('junk golem'/'ODY', 'Junk Golem comes into play with three +1/+1 counters on it.\nAt the beginning of your upkeep, sacrifice Junk Golem unless you remove a +1/+1 counter from it.\n{1}, Discard a card from your hand: Put a +1/+1 counter on Junk Golem.').
card_first_print('junk golem', 'ODY').
card_image_name('junk golem'/'ODY', 'junk golem').
card_uid('junk golem'/'ODY', 'ODY:Junk Golem:junk golem').
card_rarity('junk golem'/'ODY', 'Rare').
card_artist('junk golem'/'ODY', 'Brian Snõddy').
card_number('junk golem'/'ODY', '300').
card_multiverse_id('junk golem'/'ODY', '29997').

card_in_set('kamahl\'s desire', 'ODY').
card_original_type('kamahl\'s desire'/'ODY', 'Enchant Creature').
card_original_text('kamahl\'s desire'/'ODY', 'Enchanted creature has first strike. \nThreshold Enchanted creature gets +3/+0. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('kamahl\'s desire', 'ODY').
card_image_name('kamahl\'s desire'/'ODY', 'kamahl\'s desire').
card_uid('kamahl\'s desire'/'ODY', 'ODY:Kamahl\'s Desire:kamahl\'s desire').
card_rarity('kamahl\'s desire'/'ODY', 'Common').
card_artist('kamahl\'s desire'/'ODY', 'Dave Dorman').
card_number('kamahl\'s desire'/'ODY', '199').
card_multiverse_id('kamahl\'s desire'/'ODY', '29761').

card_in_set('kamahl, pit fighter', 'ODY').
card_original_type('kamahl, pit fighter'/'ODY', 'Creature — Barbarian Legend').
card_original_text('kamahl, pit fighter'/'ODY', 'Haste\n{T}: Kamahl, Pit Fighter deals 3 damage to target creature or player.').
card_first_print('kamahl, pit fighter', 'ODY').
card_image_name('kamahl, pit fighter'/'ODY', 'kamahl, pit fighter').
card_uid('kamahl, pit fighter'/'ODY', 'ODY:Kamahl, Pit Fighter:kamahl, pit fighter').
card_rarity('kamahl, pit fighter'/'ODY', 'Rare').
card_artist('kamahl, pit fighter'/'ODY', 'Kev Walker').
card_number('kamahl, pit fighter'/'ODY', '198').
card_flavor_text('kamahl, pit fighter'/'ODY', '\"I didn\'t come to play. I came to win.\"').
card_multiverse_id('kamahl, pit fighter'/'ODY', '29963').

card_in_set('karmic justice', 'ODY').
card_original_type('karmic justice'/'ODY', 'Enchantment').
card_original_text('karmic justice'/'ODY', 'Whenever a spell or ability an opponent controls destroys a noncreature permanent you control, you may destroy target permanent that opponent controls.').
card_first_print('karmic justice', 'ODY').
card_image_name('karmic justice'/'ODY', 'karmic justice').
card_uid('karmic justice'/'ODY', 'ODY:Karmic Justice:karmic justice').
card_rarity('karmic justice'/'ODY', 'Rare').
card_artist('karmic justice'/'ODY', 'Ray Lago').
card_number('karmic justice'/'ODY', '26').
card_multiverse_id('karmic justice'/'ODY', '31872').

card_in_set('kirtar\'s desire', 'ODY').
card_original_type('kirtar\'s desire'/'ODY', 'Enchant Creature').
card_original_text('kirtar\'s desire'/'ODY', 'Enchanted creature can\'t attack.\nThreshold Enchanted creature can\'t block. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('kirtar\'s desire', 'ODY').
card_image_name('kirtar\'s desire'/'ODY', 'kirtar\'s desire').
card_uid('kirtar\'s desire'/'ODY', 'ODY:Kirtar\'s Desire:kirtar\'s desire').
card_rarity('kirtar\'s desire'/'ODY', 'Common').
card_artist('kirtar\'s desire'/'ODY', 'Pete Venters').
card_number('kirtar\'s desire'/'ODY', '27').
card_multiverse_id('kirtar\'s desire'/'ODY', '29707').

card_in_set('kirtar\'s wrath', 'ODY').
card_original_type('kirtar\'s wrath'/'ODY', 'Sorcery').
card_original_text('kirtar\'s wrath'/'ODY', 'Destroy all creatures. They can\'t be regenerated.\nThreshold Instead destroy all creatures, then put two 1/1 white Spirit creature tokens with flying into play. Creatures destroyed this way can\'t be regenerated. (You have threshold if seven or more cards are in your graveyard.)').
card_first_print('kirtar\'s wrath', 'ODY').
card_image_name('kirtar\'s wrath'/'ODY', 'kirtar\'s wrath').
card_uid('kirtar\'s wrath'/'ODY', 'ODY:Kirtar\'s Wrath:kirtar\'s wrath').
card_rarity('kirtar\'s wrath'/'ODY', 'Rare').
card_artist('kirtar\'s wrath'/'ODY', 'Kev Walker').
card_number('kirtar\'s wrath'/'ODY', '28').
card_multiverse_id('kirtar\'s wrath'/'ODY', '29921').

card_in_set('krosan archer', 'ODY').
card_original_type('krosan archer'/'ODY', 'Creature — Centaur').
card_original_text('krosan archer'/'ODY', 'Krosan Archer may block as though it had flying.\n{G}, Discard a card from your hand: Krosan Archer gets +0/+2 until end of turn.').
card_first_print('krosan archer', 'ODY').
card_image_name('krosan archer'/'ODY', 'krosan archer').
card_uid('krosan archer'/'ODY', 'ODY:Krosan Archer:krosan archer').
card_rarity('krosan archer'/'ODY', 'Common').
card_artist('krosan archer'/'ODY', 'Ron Spears').
card_number('krosan archer'/'ODY', '246').
card_multiverse_id('krosan archer'/'ODY', '29780').

card_in_set('krosan avenger', 'ODY').
card_original_type('krosan avenger'/'ODY', 'Creature — Druid').
card_original_text('krosan avenger'/'ODY', 'Trample\nThreshold {1}{G}: Regenerate Krosan Avenger. (Play this ability only if seven or more cards are in your graveyard.)').
card_first_print('krosan avenger', 'ODY').
card_image_name('krosan avenger'/'ODY', 'krosan avenger').
card_uid('krosan avenger'/'ODY', 'ODY:Krosan Avenger:krosan avenger').
card_rarity('krosan avenger'/'ODY', 'Common').
card_artist('krosan avenger'/'ODY', 'D. Alexander Gregory').
card_number('krosan avenger'/'ODY', '247').
card_flavor_text('krosan avenger'/'ODY', 'She\'d have a fearsome reputation if she left any survivors.').
card_multiverse_id('krosan avenger'/'ODY', '29775').

card_in_set('krosan beast', 'ODY').
card_original_type('krosan beast'/'ODY', 'Creature — Squirrel Beast').
card_original_text('krosan beast'/'ODY', 'Threshold Krosan Beast gets +7/+7. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('krosan beast', 'ODY').
card_image_name('krosan beast'/'ODY', 'krosan beast').
card_uid('krosan beast'/'ODY', 'ODY:Krosan Beast:krosan beast').
card_rarity('krosan beast'/'ODY', 'Rare').
card_artist('krosan beast'/'ODY', 'Kev Walker').
card_number('krosan beast'/'ODY', '248').
card_flavor_text('krosan beast'/'ODY', 'Those who see the squirrels go underground in the winter aren\'t ready for what emerges in the spring.').
card_multiverse_id('krosan beast'/'ODY', '30549').

card_in_set('laquatus\'s creativity', 'ODY').
card_original_type('laquatus\'s creativity'/'ODY', 'Sorcery').
card_original_text('laquatus\'s creativity'/'ODY', 'Target player draws cards equal to the number of cards in his or her hand, then discards that many cards.').
card_first_print('laquatus\'s creativity', 'ODY').
card_image_name('laquatus\'s creativity'/'ODY', 'laquatus\'s creativity').
card_uid('laquatus\'s creativity'/'ODY', 'ODY:Laquatus\'s Creativity:laquatus\'s creativity').
card_rarity('laquatus\'s creativity'/'ODY', 'Uncommon').
card_artist('laquatus\'s creativity'/'ODY', 'Don Hazeltine').
card_number('laquatus\'s creativity'/'ODY', '88').
card_flavor_text('laquatus\'s creativity'/'ODY', 'One never knows when inspiration will bubble up.').
card_multiverse_id('laquatus\'s creativity'/'ODY', '29937').

card_in_set('last rites', 'ODY').
card_original_type('last rites'/'ODY', 'Sorcery').
card_original_text('last rites'/'ODY', 'Discard any number of cards from your hand. Target player reveals his or her hand, then you choose a nonland card from it for each card discarded this way. That player discards those cards.').
card_first_print('last rites', 'ODY').
card_image_name('last rites'/'ODY', 'last rites').
card_uid('last rites'/'ODY', 'ODY:Last Rites:last rites').
card_rarity('last rites'/'ODY', 'Common').
card_artist('last rites'/'ODY', 'Bradley Williams').
card_number('last rites'/'ODY', '146').
card_multiverse_id('last rites'/'ODY', '31778').

card_in_set('lava blister', 'ODY').
card_original_type('lava blister'/'ODY', 'Sorcery').
card_original_text('lava blister'/'ODY', 'Destroy target nonbasic land unless its controller has Lava Blister deal 6 damage to him or her.').
card_first_print('lava blister', 'ODY').
card_image_name('lava blister'/'ODY', 'lava blister').
card_uid('lava blister'/'ODY', 'ODY:Lava Blister:lava blister').
card_rarity('lava blister'/'ODY', 'Uncommon').
card_artist('lava blister'/'ODY', 'Rob Alexander').
card_number('lava blister'/'ODY', '200').
card_flavor_text('lava blister'/'ODY', 'To treat flesh wounds, use earth. To treat earth wounds, use flesh.\n—Barbarian saying').
card_multiverse_id('lava blister'/'ODY', '29763').

card_in_set('leaf dancer', 'ODY').
card_original_type('leaf dancer'/'ODY', 'Creature — Centaur').
card_original_text('leaf dancer'/'ODY', 'Forestwalk').
card_first_print('leaf dancer', 'ODY').
card_image_name('leaf dancer'/'ODY', 'leaf dancer').
card_uid('leaf dancer'/'ODY', 'ODY:Leaf Dancer:leaf dancer').
card_rarity('leaf dancer'/'ODY', 'Common').
card_artist('leaf dancer'/'ODY', 'Greg & Tim Hildebrandt').
card_number('leaf dancer'/'ODY', '249').
card_flavor_text('leaf dancer'/'ODY', 'A leaf dancer sweeps through the forest like a spring breeze, evading even the sharpest eyes and ears.').
card_multiverse_id('leaf dancer'/'ODY', '29782').

card_in_set('lieutenant kirtar', 'ODY').
card_original_type('lieutenant kirtar'/'ODY', 'Creature — Bird Soldier Legend').
card_original_text('lieutenant kirtar'/'ODY', 'Flying\n{1}{W}, Sacrifice Lieutenant Kirtar: Remove target attacking creature from the game.').
card_first_print('lieutenant kirtar', 'ODY').
card_image_name('lieutenant kirtar'/'ODY', 'lieutenant kirtar').
card_uid('lieutenant kirtar'/'ODY', 'ODY:Lieutenant Kirtar:lieutenant kirtar').
card_rarity('lieutenant kirtar'/'ODY', 'Rare').
card_artist('lieutenant kirtar'/'ODY', 'Paolo Parente').
card_number('lieutenant kirtar'/'ODY', '29').
card_flavor_text('lieutenant kirtar'/'ODY', '\"His strengths are pride, devotion, and ambition. His weaknesses are the same.\"\n—Pianna, nomad captain').
card_multiverse_id('lieutenant kirtar'/'ODY', '31820').

card_in_set('life burst', 'ODY').
card_original_type('life burst'/'ODY', 'Instant').
card_original_text('life burst'/'ODY', 'Target player gains 4 life, then gains 4 life for each Life Burst card in each graveyard.').
card_first_print('life burst', 'ODY').
card_image_name('life burst'/'ODY', 'life burst').
card_uid('life burst'/'ODY', 'ODY:Life Burst:life burst').
card_rarity('life burst'/'ODY', 'Common').
card_artist('life burst'/'ODY', 'John Avon').
card_number('life burst'/'ODY', '30').
card_flavor_text('life burst'/'ODY', 'As uplifting as a mystic\'s dreams.').
card_multiverse_id('life burst'/'ODY', '29704').

card_in_set('limestone golem', 'ODY').
card_original_type('limestone golem'/'ODY', 'Artifact Creature — Golem').
card_original_text('limestone golem'/'ODY', '{2}, Sacrifice Limestone Golem: Target player draws a card.').
card_first_print('limestone golem', 'ODY').
card_image_name('limestone golem'/'ODY', 'limestone golem').
card_uid('limestone golem'/'ODY', 'ODY:Limestone Golem:limestone golem').
card_rarity('limestone golem'/'ODY', 'Uncommon').
card_artist('limestone golem'/'ODY', 'Mark Tedin').
card_number('limestone golem'/'ODY', '301').
card_flavor_text('limestone golem'/'ODY', 'Dwarves have been known to take pickaxes to their own creations, hoping to discover treasures within.').
card_multiverse_id('limestone golem'/'ODY', '29887').

card_in_set('liquid fire', 'ODY').
card_original_type('liquid fire'/'ODY', 'Sorcery').
card_original_text('liquid fire'/'ODY', 'Liquid Fire deals 5 damage divided as you choose between target creature and that creature\'s controller.').
card_first_print('liquid fire', 'ODY').
card_image_name('liquid fire'/'ODY', 'liquid fire').
card_uid('liquid fire'/'ODY', 'ODY:Liquid Fire:liquid fire').
card_rarity('liquid fire'/'ODY', 'Uncommon').
card_artist('liquid fire'/'ODY', 'Greg Staples').
card_number('liquid fire'/'ODY', '201').
card_flavor_text('liquid fire'/'ODY', 'There\'s no such thing as a little bit of lava.').
card_multiverse_id('liquid fire'/'ODY', '29866').

card_in_set('lithatog', 'ODY').
card_original_type('lithatog'/'ODY', 'Creature — Atog').
card_original_text('lithatog'/'ODY', 'Sacrifice an artifact: Lithatog gets +1/+1 until end of turn.\nSacrifice a land: Lithatog gets +1/+1 until end of turn.').
card_first_print('lithatog', 'ODY').
card_image_name('lithatog'/'ODY', 'lithatog').
card_uid('lithatog'/'ODY', 'ODY:Lithatog:lithatog').
card_rarity('lithatog'/'ODY', 'Uncommon').
card_artist('lithatog'/'ODY', 'Franz Vohwinkel').
card_number('lithatog'/'ODY', '289').
card_multiverse_id('lithatog'/'ODY', '31770').

card_in_set('luminous guardian', 'ODY').
card_original_type('luminous guardian'/'ODY', 'Creature — Guardian').
card_original_text('luminous guardian'/'ODY', '{W}: Luminous Guardian gets +0/+1 until end of turn.\n{2}: Luminous Guardian may block an additional creature this turn.').
card_first_print('luminous guardian', 'ODY').
card_image_name('luminous guardian'/'ODY', 'luminous guardian').
card_uid('luminous guardian'/'ODY', 'ODY:Luminous Guardian:luminous guardian').
card_rarity('luminous guardian'/'ODY', 'Uncommon').
card_artist('luminous guardian'/'ODY', 'Terese Nielsen').
card_number('luminous guardian'/'ODY', '31').
card_flavor_text('luminous guardian'/'ODY', '\"There is no victory without virtue.\"').
card_multiverse_id('luminous guardian'/'ODY', '29806').

card_in_set('mad dog', 'ODY').
card_original_type('mad dog'/'ODY', 'Creature — Hound').
card_original_text('mad dog'/'ODY', 'At the end of your turn, if Mad Dog didn\'t attack or come under your control this turn, sacrifice it.').
card_first_print('mad dog', 'ODY').
card_image_name('mad dog'/'ODY', 'mad dog').
card_uid('mad dog'/'ODY', 'ODY:Mad Dog:mad dog').
card_rarity('mad dog'/'ODY', 'Common').
card_artist('mad dog'/'ODY', 'Ron Spencer').
card_number('mad dog'/'ODY', '202').
card_flavor_text('mad dog'/'ODY', 'The bones it buries may be yours.').
card_multiverse_id('mad dog'/'ODY', '31818').

card_in_set('magma vein', 'ODY').
card_original_type('magma vein'/'ODY', 'Enchantment').
card_original_text('magma vein'/'ODY', '{R}, Sacrifice a land: Magma Vein deals 1 damage to each creature without flying.').
card_first_print('magma vein', 'ODY').
card_image_name('magma vein'/'ODY', 'magma vein').
card_uid('magma vein'/'ODY', 'ODY:Magma Vein:magma vein').
card_rarity('magma vein'/'ODY', 'Uncommon').
card_artist('magma vein'/'ODY', 'Glen Angus').
card_number('magma vein'/'ODY', '203').
card_flavor_text('magma vein'/'ODY', 'Standing near a volcano\n—Barbarian expression meaning\n\"asking for it\"').
card_multiverse_id('magma vein'/'ODY', '29859').

card_in_set('magnivore', 'ODY').
card_original_type('magnivore'/'ODY', 'Creature — Lhurgoyf').
card_original_text('magnivore'/'ODY', 'Haste\nMagnivore\'s power and toughness are each equal to the number of sorcery cards in all graveyards.').
card_first_print('magnivore', 'ODY').
card_image_name('magnivore'/'ODY', 'magnivore').
card_uid('magnivore'/'ODY', 'ODY:Magnivore:magnivore').
card_rarity('magnivore'/'ODY', 'Rare').
card_artist('magnivore'/'ODY', 'Carl Critchlow').
card_number('magnivore'/'ODY', '204').
card_multiverse_id('magnivore'/'ODY', '30550').

card_in_set('malevolent awakening', 'ODY').
card_original_type('malevolent awakening'/'ODY', 'Enchantment').
card_original_text('malevolent awakening'/'ODY', '{1}{B}{B}, Sacrifice a creature: Return target creature card from your graveyard to your hand.').
card_first_print('malevolent awakening', 'ODY').
card_image_name('malevolent awakening'/'ODY', 'malevolent awakening').
card_uid('malevolent awakening'/'ODY', 'ODY:Malevolent Awakening:malevolent awakening').
card_rarity('malevolent awakening'/'ODY', 'Uncommon').
card_artist('malevolent awakening'/'ODY', 'Alex Horley-Orlandelli').
card_number('malevolent awakening'/'ODY', '147').
card_flavor_text('malevolent awakening'/'ODY', 'The most popular pit fighters are brought back for encore performances.').
card_multiverse_id('malevolent awakening'/'ODY', '30560').

card_in_set('master apothecary', 'ODY').
card_original_type('master apothecary'/'ODY', 'Creature — Cleric').
card_original_text('master apothecary'/'ODY', 'Tap an untapped Cleric you control: Prevent the next 2 damage that would be dealt to target creature or player this turn.').
card_first_print('master apothecary', 'ODY').
card_image_name('master apothecary'/'ODY', 'master apothecary').
card_uid('master apothecary'/'ODY', 'ODY:Master Apothecary:master apothecary').
card_rarity('master apothecary'/'ODY', 'Rare').
card_artist('master apothecary'/'ODY', 'Terese Nielsen').
card_number('master apothecary'/'ODY', '32').
card_flavor_text('master apothecary'/'ODY', 'It takes a remarkable cleric to turn a group of healers into a hospital.').
card_multiverse_id('master apothecary'/'ODY', '29911').

card_in_set('metamorphic wurm', 'ODY').
card_original_type('metamorphic wurm'/'ODY', 'Creature — Elephant Wurm').
card_original_text('metamorphic wurm'/'ODY', 'Threshold Metamorphic Wurm gets +4/+4. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('metamorphic wurm', 'ODY').
card_image_name('metamorphic wurm'/'ODY', 'metamorphic wurm').
card_uid('metamorphic wurm'/'ODY', 'ODY:Metamorphic Wurm:metamorphic wurm').
card_rarity('metamorphic wurm'/'ODY', 'Uncommon').
card_artist('metamorphic wurm'/'ODY', 'Thomas M. Baxa').
card_number('metamorphic wurm'/'ODY', '250').
card_flavor_text('metamorphic wurm'/'ODY', 'Only a handful have seen it transform. Or, from the wurm\'s perspective, only a mouthful.').
card_multiverse_id('metamorphic wurm'/'ODY', '29774').

card_in_set('millikin', 'ODY').
card_original_type('millikin'/'ODY', 'Artifact Creature').
card_original_text('millikin'/'ODY', '{T}, Put the top card of your library into your graveyard: Add one colorless mana to your mana pool.').
card_first_print('millikin', 'ODY').
card_image_name('millikin'/'ODY', 'millikin').
card_uid('millikin'/'ODY', 'ODY:Millikin:millikin').
card_rarity('millikin'/'ODY', 'Uncommon').
card_artist('millikin'/'ODY', 'Alex Horley-Orlandelli').
card_number('millikin'/'ODY', '302').
card_flavor_text('millikin'/'ODY', 'The toymaker frowned in bewilderment. None of his creations had ever sneezed before.').
card_multiverse_id('millikin'/'ODY', '31830').

card_in_set('mind burst', 'ODY').
card_original_type('mind burst'/'ODY', 'Sorcery').
card_original_text('mind burst'/'ODY', 'Target player discards X cards from his or her hand, where X is one plus the number of Mind Burst cards in all graveyards.').
card_first_print('mind burst', 'ODY').
card_image_name('mind burst'/'ODY', 'mind burst').
card_uid('mind burst'/'ODY', 'ODY:Mind Burst:mind burst').
card_rarity('mind burst'/'ODY', 'Common').
card_artist('mind burst'/'ODY', 'Marc Fishman').
card_number('mind burst'/'ODY', '148').
card_flavor_text('mind burst'/'ODY', 'As haunting as a zombie\'s curse.').
card_multiverse_id('mind burst'/'ODY', '29746').

card_in_set('mindslicer', 'ODY').
card_original_type('mindslicer'/'ODY', 'Creature — Horror').
card_original_text('mindslicer'/'ODY', 'When Mindslicer is put into a graveyard from play, each player discards his or her hand.').
card_first_print('mindslicer', 'ODY').
card_image_name('mindslicer'/'ODY', 'mindslicer').
card_uid('mindslicer'/'ODY', 'ODY:Mindslicer:mindslicer').
card_rarity('mindslicer'/'ODY', 'Rare').
card_artist('mindslicer'/'ODY', 'Kev Walker').
card_number('mindslicer'/'ODY', '149').
card_flavor_text('mindslicer'/'ODY', 'It is the thing that goes bump in the night.').
card_multiverse_id('mindslicer'/'ODY', '29949').

card_in_set('mine layer', 'ODY').
card_original_type('mine layer'/'ODY', 'Creature — Dwarf').
card_original_text('mine layer'/'ODY', '{1}{R}, {T}: Put a mine counter on target land.\nWhenever a land with a mine counter on it becomes tapped, destroy it.\nWhen Mine Layer leaves play, remove all mine counters from all lands.').
card_first_print('mine layer', 'ODY').
card_image_name('mine layer'/'ODY', 'mine layer').
card_uid('mine layer'/'ODY', 'ODY:Mine Layer:mine layer').
card_rarity('mine layer'/'ODY', 'Rare').
card_artist('mine layer'/'ODY', 'Mark Brill').
card_number('mine layer'/'ODY', '205').
card_multiverse_id('mine layer'/'ODY', '29964').

card_in_set('minotaur explorer', 'ODY').
card_original_type('minotaur explorer'/'ODY', 'Creature — Minotaur').
card_original_text('minotaur explorer'/'ODY', 'When Minotaur Explorer comes into play, sacrifice it unless you discard a card at random from your hand.').
card_first_print('minotaur explorer', 'ODY').
card_image_name('minotaur explorer'/'ODY', 'minotaur explorer').
card_uid('minotaur explorer'/'ODY', 'ODY:Minotaur Explorer:minotaur explorer').
card_rarity('minotaur explorer'/'ODY', 'Uncommon').
card_artist('minotaur explorer'/'ODY', 'Dave Dorman').
card_number('minotaur explorer'/'ODY', '206').
card_flavor_text('minotaur explorer'/'ODY', 'After the invasion devastated the Talruum and Hurloon tribes, the survivors began to search for other minotaurs.').
card_multiverse_id('minotaur explorer'/'ODY', '31786').

card_in_set('mirari', 'ODY').
card_original_type('mirari'/'ODY', 'Legendary Artifact').
card_original_text('mirari'/'ODY', 'Whenever you play an instant or sorcery spell, you may pay {3}. If you do, put a copy of that spell onto the stack. You may choose new targets for that copy.').
card_first_print('mirari', 'ODY').
card_image_name('mirari'/'ODY', 'mirari').
card_uid('mirari'/'ODY', 'ODY:Mirari:mirari').
card_rarity('mirari'/'ODY', 'Rare').
card_artist('mirari'/'ODY', 'Donato Giancola').
card_number('mirari'/'ODY', '303').
card_flavor_text('mirari'/'ODY', '\"It offers you what you want, not what you need.\"\n—Braids, dementia summoner').
card_multiverse_id('mirari'/'ODY', '31801').

card_in_set('molten influence', 'ODY').
card_original_type('molten influence'/'ODY', 'Instant').
card_original_text('molten influence'/'ODY', 'Counter target instant or sorcery spell unless its controller has Molten Influence deal 4 damage to him or her.').
card_first_print('molten influence', 'ODY').
card_image_name('molten influence'/'ODY', 'molten influence').
card_uid('molten influence'/'ODY', 'ODY:Molten Influence:molten influence').
card_rarity('molten influence'/'ODY', 'Rare').
card_artist('molten influence'/'ODY', 'Franz Vohwinkel').
card_number('molten influence'/'ODY', '207').
card_flavor_text('molten influence'/'ODY', 'Lavamancers make simple requests: obey or die.').
card_multiverse_id('molten influence'/'ODY', '29972').

card_in_set('moment\'s peace', 'ODY').
card_original_type('moment\'s peace'/'ODY', 'Instant').
card_original_text('moment\'s peace'/'ODY', 'Prevent all combat damage that would be dealt this turn.\nFlashback {2}{G} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('moment\'s peace', 'ODY').
card_image_name('moment\'s peace'/'ODY', 'moment\'s peace').
card_uid('moment\'s peace'/'ODY', 'ODY:Moment\'s Peace:moment\'s peace').
card_rarity('moment\'s peace'/'ODY', 'Common').
card_artist('moment\'s peace'/'ODY', 'Rebecca Guay').
card_number('moment\'s peace'/'ODY', '251').
card_multiverse_id('moment\'s peace'/'ODY', '31811').

card_in_set('morbid hunger', 'ODY').
card_original_type('morbid hunger'/'ODY', 'Sorcery').
card_original_text('morbid hunger'/'ODY', 'Morbid Hunger deals 3 damage to target creature or player. You gain 3 life.\nFlashback {7}{B}{B} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('morbid hunger', 'ODY').
card_image_name('morbid hunger'/'ODY', 'morbid hunger').
card_uid('morbid hunger'/'ODY', 'ODY:Morbid Hunger:morbid hunger').
card_rarity('morbid hunger'/'ODY', 'Common').
card_artist('morbid hunger'/'ODY', 'Eric Peterson').
card_number('morbid hunger'/'ODY', '150').
card_multiverse_id('morbid hunger'/'ODY', '29745').

card_in_set('morgue theft', 'ODY').
card_original_type('morgue theft'/'ODY', 'Sorcery').
card_original_text('morgue theft'/'ODY', 'Return target creature card from your graveyard to your hand.\nFlashback {4}{B} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('morgue theft', 'ODY').
card_image_name('morgue theft'/'ODY', 'morgue theft').
card_uid('morgue theft'/'ODY', 'ODY:Morgue Theft:morgue theft').
card_rarity('morgue theft'/'ODY', 'Common').
card_artist('morgue theft'/'ODY', 'Justin Sweet').
card_number('morgue theft'/'ODY', '151').
card_multiverse_id('morgue theft'/'ODY', '29743').

card_in_set('mortivore', 'ODY').
card_original_type('mortivore'/'ODY', 'Creature — Lhurgoyf').
card_original_text('mortivore'/'ODY', 'Mortivore\'s power and toughness are each equal to the number of creature cards in all graveyards.\n{B}: Regenerate Mortivore.').
card_first_print('mortivore', 'ODY').
card_image_name('mortivore'/'ODY', 'mortivore').
card_uid('mortivore'/'ODY', 'ODY:Mortivore:mortivore').
card_rarity('mortivore'/'ODY', 'Rare').
card_artist('mortivore'/'ODY', 'Anthony S. Waters').
card_number('mortivore'/'ODY', '152').
card_multiverse_id('mortivore'/'ODY', '29946').

card_in_set('mossfire egg', 'ODY').
card_original_type('mossfire egg'/'ODY', 'Artifact').
card_original_text('mossfire egg'/'ODY', '{2}, {T}, Sacrifice Mossfire Egg: Add {R}{G} to your mana pool. Draw a card.').
card_first_print('mossfire egg', 'ODY').
card_image_name('mossfire egg'/'ODY', 'mossfire egg').
card_uid('mossfire egg'/'ODY', 'ODY:Mossfire Egg:mossfire egg').
card_rarity('mossfire egg'/'ODY', 'Uncommon').
card_artist('mossfire egg'/'ODY', 'David Martin').
card_number('mossfire egg'/'ODY', '304').
card_flavor_text('mossfire egg'/'ODY', '\"Can you feel your instincts overwhelm your reason?\"\n—Watnik, master glassblower').
card_multiverse_id('mossfire egg'/'ODY', '30010').

card_in_set('mossfire valley', 'ODY').
card_original_type('mossfire valley'/'ODY', 'Land').
card_original_text('mossfire valley'/'ODY', '{1}, {T}: Add {R}{G} to your mana pool.').
card_first_print('mossfire valley', 'ODY').
card_image_name('mossfire valley'/'ODY', 'mossfire valley').
card_uid('mossfire valley'/'ODY', 'ODY:Mossfire Valley:mossfire valley').
card_rarity('mossfire valley'/'ODY', 'Rare').
card_artist('mossfire valley'/'ODY', 'John Avon').
card_number('mossfire valley'/'ODY', '321').
card_flavor_text('mossfire valley'/'ODY', 'Lush growth spreads like a ferocious blaze across the Otarian hillsides.').
card_multiverse_id('mossfire valley'/'ODY', '29795').

card_in_set('mountain', 'ODY').
card_original_type('mountain'/'ODY', 'Land').
card_original_text('mountain'/'ODY', 'R').
card_image_name('mountain'/'ODY', 'mountain1').
card_uid('mountain'/'ODY', 'ODY:Mountain:mountain1').
card_rarity('mountain'/'ODY', 'Basic Land').
card_artist('mountain'/'ODY', 'Anthony S. Waters').
card_number('mountain'/'ODY', '343').
card_multiverse_id('mountain'/'ODY', '31621').

card_in_set('mountain', 'ODY').
card_original_type('mountain'/'ODY', 'Land').
card_original_text('mountain'/'ODY', 'R').
card_image_name('mountain'/'ODY', 'mountain2').
card_uid('mountain'/'ODY', 'ODY:Mountain:mountain2').
card_rarity('mountain'/'ODY', 'Basic Land').
card_artist('mountain'/'ODY', 'Franz Vohwinkel').
card_number('mountain'/'ODY', '344').
card_multiverse_id('mountain'/'ODY', '31622').

card_in_set('mountain', 'ODY').
card_original_type('mountain'/'ODY', 'Land').
card_original_text('mountain'/'ODY', 'R').
card_image_name('mountain'/'ODY', 'mountain3').
card_uid('mountain'/'ODY', 'ODY:Mountain:mountain3').
card_rarity('mountain'/'ODY', 'Basic Land').
card_artist('mountain'/'ODY', 'Rob Alexander').
card_number('mountain'/'ODY', '345').
card_multiverse_id('mountain'/'ODY', '31623').

card_in_set('mountain', 'ODY').
card_original_type('mountain'/'ODY', 'Land').
card_original_text('mountain'/'ODY', 'R').
card_image_name('mountain'/'ODY', 'mountain4').
card_uid('mountain'/'ODY', 'ODY:Mountain:mountain4').
card_rarity('mountain'/'ODY', 'Basic Land').
card_artist('mountain'/'ODY', 'Tony Szczudlo').
card_number('mountain'/'ODY', '346').
card_multiverse_id('mountain'/'ODY', '31624').

card_in_set('mudhole', 'ODY').
card_original_type('mudhole'/'ODY', 'Instant').
card_original_text('mudhole'/'ODY', 'Target player removes all land cards in his or her graveyard from the game.').
card_first_print('mudhole', 'ODY').
card_image_name('mudhole'/'ODY', 'mudhole').
card_uid('mudhole'/'ODY', 'ODY:Mudhole:mudhole').
card_rarity('mudhole'/'ODY', 'Rare').
card_artist('mudhole'/'ODY', 'Gary Ruddell').
card_number('mudhole'/'ODY', '208').
card_flavor_text('mudhole'/'ODY', '\"Doing okay back there, Tarv? Tarv?\"').
card_multiverse_id('mudhole'/'ODY', '31740').

card_in_set('muscle burst', 'ODY').
card_original_type('muscle burst'/'ODY', 'Instant').
card_original_text('muscle burst'/'ODY', 'Target creature gets +X/+X until end of turn, where X is 3 plus the number of Muscle Burst cards in all graveyards.').
card_first_print('muscle burst', 'ODY').
card_image_name('muscle burst'/'ODY', 'muscle burst').
card_uid('muscle burst'/'ODY', 'ODY:Muscle Burst:muscle burst').
card_rarity('muscle burst'/'ODY', 'Common').
card_artist('muscle burst'/'ODY', 'Gary Ruddell').
card_number('muscle burst'/'ODY', '252').
card_flavor_text('muscle burst'/'ODY', 'As strong as a centaur\'s will.').
card_multiverse_id('muscle burst'/'ODY', '29788').

card_in_set('mystic crusader', 'ODY').
card_original_type('mystic crusader'/'ODY', 'Creature — Nomad Mystic').
card_original_text('mystic crusader'/'ODY', 'Protection from black and from red\nThreshold Mystic Crusader gets +1/+1 and has flying. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('mystic crusader', 'ODY').
card_image_name('mystic crusader'/'ODY', 'mystic crusader').
card_uid('mystic crusader'/'ODY', 'ODY:Mystic Crusader:mystic crusader').
card_rarity('mystic crusader'/'ODY', 'Rare').
card_artist('mystic crusader'/'ODY', 'Kev Walker').
card_number('mystic crusader'/'ODY', '33').
card_multiverse_id('mystic crusader'/'ODY', '29802').

card_in_set('mystic enforcer', 'ODY').
card_original_type('mystic enforcer'/'ODY', 'Creature — Nomad Mystic').
card_original_text('mystic enforcer'/'ODY', 'Protection from black\nThreshold Mystic Enforcer gets +3/+3 and has flying. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('mystic enforcer', 'ODY').
card_image_name('mystic enforcer'/'ODY', 'mystic enforcer').
card_uid('mystic enforcer'/'ODY', 'ODY:Mystic Enforcer:mystic enforcer').
card_rarity('mystic enforcer'/'ODY', 'Rare').
card_artist('mystic enforcer'/'ODY', 'Gary Ruddell').
card_number('mystic enforcer'/'ODY', '290').
card_multiverse_id('mystic enforcer'/'ODY', '31772').

card_in_set('mystic penitent', 'ODY').
card_original_type('mystic penitent'/'ODY', 'Creature — Nomad Mystic').
card_original_text('mystic penitent'/'ODY', 'Attacking doesn\'t cause Mystic Penitent to tap.\nThreshold Mystic Penitent gets +1/+1 and has flying. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('mystic penitent', 'ODY').
card_image_name('mystic penitent'/'ODY', 'mystic penitent').
card_uid('mystic penitent'/'ODY', 'ODY:Mystic Penitent:mystic penitent').
card_rarity('mystic penitent'/'ODY', 'Uncommon').
card_artist('mystic penitent'/'ODY', 'Larry Elmore').
card_number('mystic penitent'/'ODY', '34').
card_multiverse_id('mystic penitent'/'ODY', '31766').

card_in_set('mystic visionary', 'ODY').
card_original_type('mystic visionary'/'ODY', 'Creature — Nomad Mystic').
card_original_text('mystic visionary'/'ODY', 'Threshold Mystic Visionary has flying. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('mystic visionary', 'ODY').
card_image_name('mystic visionary'/'ODY', 'mystic visionary').
card_uid('mystic visionary'/'ODY', 'ODY:Mystic Visionary:mystic visionary').
card_rarity('mystic visionary'/'ODY', 'Common').
card_artist('mystic visionary'/'ODY', 'Matt Cavotta').
card_number('mystic visionary'/'ODY', '35').
card_flavor_text('mystic visionary'/'ODY', '\"The aven are heralds of divinity. The greatest glory is to join them in the sky.\"').
card_multiverse_id('mystic visionary'/'ODY', '29697').

card_in_set('mystic zealot', 'ODY').
card_original_type('mystic zealot'/'ODY', 'Creature — Nomad Mystic').
card_original_text('mystic zealot'/'ODY', 'Threshold Mystic Zealot gets +1/+1 and has flying. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('mystic zealot', 'ODY').
card_image_name('mystic zealot'/'ODY', 'mystic zealot').
card_uid('mystic zealot'/'ODY', 'ODY:Mystic Zealot:mystic zealot').
card_rarity('mystic zealot'/'ODY', 'Common').
card_artist('mystic zealot'/'ODY', 'Paolo Parente').
card_number('mystic zealot'/'ODY', '36').
card_flavor_text('mystic zealot'/'ODY', 'Nomad youths aspire to one of two roles in the tribe: priest or warrior. Their secret dream is to become both.').
card_multiverse_id('mystic zealot'/'ODY', '29801').

card_in_set('nantuko disciple', 'ODY').
card_original_type('nantuko disciple'/'ODY', 'Creature — Insect Druid').
card_original_text('nantuko disciple'/'ODY', '{G}, {T}: Target creature gets +2/+2 until end of turn.').
card_first_print('nantuko disciple', 'ODY').
card_image_name('nantuko disciple'/'ODY', 'nantuko disciple').
card_uid('nantuko disciple'/'ODY', 'ODY:Nantuko Disciple:nantuko disciple').
card_rarity('nantuko disciple'/'ODY', 'Common').
card_artist('nantuko disciple'/'ODY', 'Justin Sweet').
card_number('nantuko disciple'/'ODY', '253').
card_flavor_text('nantuko disciple'/'ODY', 'Unanswered prayers are themselves answers.\n—Nantuko teaching').
card_multiverse_id('nantuko disciple'/'ODY', '29771').

card_in_set('nantuko elder', 'ODY').
card_original_type('nantuko elder'/'ODY', 'Creature — Insect Druid').
card_original_text('nantuko elder'/'ODY', '{T}: Add {1}{G} to your mana pool.').
card_first_print('nantuko elder', 'ODY').
card_image_name('nantuko elder'/'ODY', 'nantuko elder').
card_uid('nantuko elder'/'ODY', 'ODY:Nantuko Elder:nantuko elder').
card_rarity('nantuko elder'/'ODY', 'Uncommon').
card_artist('nantuko elder'/'ODY', 'Daren Bader').
card_number('nantuko elder'/'ODY', '254').
card_flavor_text('nantuko elder'/'ODY', 'Plant a field with wishes, and you will grow more wishes.\n—Nantuko teaching').
card_multiverse_id('nantuko elder'/'ODY', '29870').

card_in_set('nantuko mentor', 'ODY').
card_original_type('nantuko mentor'/'ODY', 'Creature — Insect Druid').
card_original_text('nantuko mentor'/'ODY', '{2}{G}, {T}: Target creature gets +X/+X until end of turn, where X is that creature\'s power.').
card_first_print('nantuko mentor', 'ODY').
card_image_name('nantuko mentor'/'ODY', 'nantuko mentor').
card_uid('nantuko mentor'/'ODY', 'ODY:Nantuko Mentor:nantuko mentor').
card_rarity('nantuko mentor'/'ODY', 'Rare').
card_artist('nantuko mentor'/'ODY', 'John Matson').
card_number('nantuko mentor'/'ODY', '255').
card_flavor_text('nantuko mentor'/'ODY', 'That which grows without roots cannot be uprooted.\n—Nantuko teaching').
card_multiverse_id('nantuko mentor'/'ODY', '29981').

card_in_set('nantuko shrine', 'ODY').
card_original_type('nantuko shrine'/'ODY', 'Enchantment').
card_original_text('nantuko shrine'/'ODY', 'Whenever a player plays a spell, that player puts X 1/1 green Squirrel creature tokens into play, where X is the number of cards in all graveyards with the same name as that spell.').
card_first_print('nantuko shrine', 'ODY').
card_image_name('nantuko shrine'/'ODY', 'nantuko shrine').
card_uid('nantuko shrine'/'ODY', 'ODY:Nantuko Shrine:nantuko shrine').
card_rarity('nantuko shrine'/'ODY', 'Rare').
card_artist('nantuko shrine'/'ODY', 'Rebecca Guay').
card_number('nantuko shrine'/'ODY', '256').
card_multiverse_id('nantuko shrine'/'ODY', '29995').

card_in_set('need for speed', 'ODY').
card_original_type('need for speed'/'ODY', 'Enchantment').
card_original_text('need for speed'/'ODY', 'Sacrifice a land: Target creature gains haste until end of turn.').
card_first_print('need for speed', 'ODY').
card_image_name('need for speed'/'ODY', 'need for speed').
card_uid('need for speed'/'ODY', 'ODY:Need for Speed:need for speed').
card_rarity('need for speed'/'ODY', 'Rare').
card_artist('need for speed'/'ODY', 'Christopher Moeller').
card_number('need for speed'/'ODY', '209').
card_flavor_text('need for speed'/'ODY', 'His feet buckled the ground, his hands cut the wind to ribbons, and he was gone.').
card_multiverse_id('need for speed'/'ODY', '29971').

card_in_set('nefarious lich', 'ODY').
card_original_type('nefarious lich'/'ODY', 'Enchantment').
card_original_text('nefarious lich'/'ODY', 'If you would be dealt damage, remove that many cards in your graveyard from the game instead. If you can\'t, you lose the game.\nIf you would gain life, draw that many cards instead.\nWhen Nefarious Lich leaves play, you lose the game.').
card_first_print('nefarious lich', 'ODY').
card_image_name('nefarious lich'/'ODY', 'nefarious lich').
card_uid('nefarious lich'/'ODY', 'ODY:Nefarious Lich:nefarious lich').
card_rarity('nefarious lich'/'ODY', 'Rare').
card_artist('nefarious lich'/'ODY', 'Jerry Tiritilli').
card_number('nefarious lich'/'ODY', '153').
card_multiverse_id('nefarious lich'/'ODY', '29952').

card_in_set('new frontiers', 'ODY').
card_original_type('new frontiers'/'ODY', 'Sorcery').
card_original_text('new frontiers'/'ODY', 'Each player may search his or her library for up to X basic land cards and put them into play tapped. Then each player who searched his or her library this way shuffles it.').
card_first_print('new frontiers', 'ODY').
card_image_name('new frontiers'/'ODY', 'new frontiers').
card_uid('new frontiers'/'ODY', 'ODY:New Frontiers:new frontiers').
card_rarity('new frontiers'/'ODY', 'Rare').
card_artist('new frontiers'/'ODY', 'Ron Spencer').
card_number('new frontiers'/'ODY', '257').
card_flavor_text('new frontiers'/'ODY', 'Spreading a blanket of growth to warm a weary world.').
card_multiverse_id('new frontiers'/'ODY', '31854').

card_in_set('nimble mongoose', 'ODY').
card_original_type('nimble mongoose'/'ODY', 'Creature — Mongoose').
card_original_text('nimble mongoose'/'ODY', 'Nimble Mongoose can\'t be the target of spells or abilities.\nThreshold Nimble Mongoose gets +2/+2. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('nimble mongoose', 'ODY').
card_image_name('nimble mongoose'/'ODY', 'nimble mongoose').
card_uid('nimble mongoose'/'ODY', 'ODY:Nimble Mongoose:nimble mongoose').
card_rarity('nimble mongoose'/'ODY', 'Uncommon').
card_artist('nimble mongoose'/'ODY', 'Terese Nielsen').
card_number('nimble mongoose'/'ODY', '258').
card_flavor_text('nimble mongoose'/'ODY', 'Faster than a cobra\'s bite.').
card_multiverse_id('nimble mongoose'/'ODY', '29784').

card_in_set('nomad decoy', 'ODY').
card_original_type('nomad decoy'/'ODY', 'Creature — Nomad').
card_original_text('nomad decoy'/'ODY', '{W}, {T}: Tap target creature.\nThreshold {W}{W}, {T}: Tap two target creatures. (Play this ability only if seven or more cards are in your graveyard.)').
card_first_print('nomad decoy', 'ODY').
card_image_name('nomad decoy'/'ODY', 'nomad decoy').
card_uid('nomad decoy'/'ODY', 'ODY:Nomad Decoy:nomad decoy').
card_rarity('nomad decoy'/'ODY', 'Uncommon').
card_artist('nomad decoy'/'ODY', 'Pete Venters').
card_number('nomad decoy'/'ODY', '37').
card_flavor_text('nomad decoy'/'ODY', 'If you can take something from a nomad, it\'s probably bait.').
card_multiverse_id('nomad decoy'/'ODY', '29805').

card_in_set('nomad stadium', 'ODY').
card_original_type('nomad stadium'/'ODY', 'Land').
card_original_text('nomad stadium'/'ODY', '{T}: Add {W} to your mana pool. Nomad Stadium deals 1 damage to you.\nThreshold {W}, {T}, Sacrifice Nomad Stadium: You gain 4 life. (Play this ability only if seven or more cards are in your graveyard.)').
card_first_print('nomad stadium', 'ODY').
card_image_name('nomad stadium'/'ODY', 'nomad stadium').
card_uid('nomad stadium'/'ODY', 'ODY:Nomad Stadium:nomad stadium').
card_rarity('nomad stadium'/'ODY', 'Uncommon').
card_artist('nomad stadium'/'ODY', 'David Martin').
card_number('nomad stadium'/'ODY', '322').
card_multiverse_id('nomad stadium'/'ODY', '29902').

card_in_set('nut collector', 'ODY').
card_original_type('nut collector'/'ODY', 'Creature — Druid').
card_original_text('nut collector'/'ODY', 'At the beginning of your upkeep, you may put a 1/1 green Squirrel creature token into play.\nThreshold All Squirrels get +2/+2. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('nut collector', 'ODY').
card_image_name('nut collector'/'ODY', 'nut collector').
card_uid('nut collector'/'ODY', 'ODY:Nut Collector:nut collector').
card_rarity('nut collector'/'ODY', 'Rare').
card_artist('nut collector'/'ODY', 'Christopher Moeller').
card_number('nut collector'/'ODY', '259').
card_multiverse_id('nut collector'/'ODY', '29987').

card_in_set('obstinate familiar', 'ODY').
card_original_type('obstinate familiar'/'ODY', 'Creature — Lizard').
card_original_text('obstinate familiar'/'ODY', 'If you would draw a card, you may skip that draw instead.').
card_first_print('obstinate familiar', 'ODY').
card_image_name('obstinate familiar'/'ODY', 'obstinate familiar').
card_uid('obstinate familiar'/'ODY', 'ODY:Obstinate Familiar:obstinate familiar').
card_rarity('obstinate familiar'/'ODY', 'Rare').
card_artist('obstinate familiar'/'ODY', 'Terese Nielsen').
card_number('obstinate familiar'/'ODY', '210').
card_flavor_text('obstinate familiar'/'ODY', '\"Lizards make excellent familiars. They never know when they\'ve lost.\"\n—Matoc, lavamancer').
card_multiverse_id('obstinate familiar'/'ODY', '29968').

card_in_set('otarian juggernaut', 'ODY').
card_original_type('otarian juggernaut'/'ODY', 'Artifact Creature').
card_original_text('otarian juggernaut'/'ODY', 'Otarian Juggernaut can\'t be blocked by Walls.\nThreshold Otarian Juggernaut gets +3/+0 and attacks each turn if able. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('otarian juggernaut', 'ODY').
card_image_name('otarian juggernaut'/'ODY', 'otarian juggernaut').
card_uid('otarian juggernaut'/'ODY', 'ODY:Otarian Juggernaut:otarian juggernaut').
card_rarity('otarian juggernaut'/'ODY', 'Rare').
card_artist('otarian juggernaut'/'ODY', 'Brian Snõddy').
card_number('otarian juggernaut'/'ODY', '305').
card_multiverse_id('otarian juggernaut'/'ODY', '29889').

card_in_set('overeager apprentice', 'ODY').
card_original_type('overeager apprentice'/'ODY', 'Creature — Minion').
card_original_text('overeager apprentice'/'ODY', 'Discard a card from your hand, Sacrifice Overeager Apprentice: Add {B}{B}{B} to your mana pool.').
card_first_print('overeager apprentice', 'ODY').
card_image_name('overeager apprentice'/'ODY', 'overeager apprentice').
card_uid('overeager apprentice'/'ODY', 'ODY:Overeager Apprentice:overeager apprentice').
card_rarity('overeager apprentice'/'ODY', 'Common').
card_artist('overeager apprentice'/'ODY', 'Ray Lago').
card_number('overeager apprentice'/'ODY', '154').
card_flavor_text('overeager apprentice'/'ODY', 'Slow and steady may not always win the race, but at least it doesn\'t end up splattered on the walls.').
card_multiverse_id('overeager apprentice'/'ODY', '29737').

card_in_set('overrun', 'ODY').
card_original_type('overrun'/'ODY', 'Sorcery').
card_original_text('overrun'/'ODY', 'Creatures you control get +3/+3 and gain trample until end of turn.').
card_image_name('overrun'/'ODY', 'overrun').
card_uid('overrun'/'ODY', 'ODY:Overrun:overrun').
card_rarity('overrun'/'ODY', 'Uncommon').
card_artist('overrun'/'ODY', 'Carl Critchlow').
card_number('overrun'/'ODY', '260').
card_flavor_text('overrun'/'ODY', 'The dance of angry feet\n—Nantuko expression meaning\n\"stampede\"').
card_multiverse_id('overrun'/'ODY', '29996').

card_in_set('painbringer', 'ODY').
card_original_type('painbringer'/'ODY', 'Creature — Minion').
card_original_text('painbringer'/'ODY', '{T}, Remove any number of cards in your graveyard from the game: Target creature gets -X/-X until end of turn, where X is the number of cards removed this way.').
card_first_print('painbringer', 'ODY').
card_image_name('painbringer'/'ODY', 'painbringer').
card_uid('painbringer'/'ODY', 'ODY:Painbringer:painbringer').
card_rarity('painbringer'/'ODY', 'Uncommon').
card_artist('painbringer'/'ODY', 'Pete Venters').
card_number('painbringer'/'ODY', '155').
card_multiverse_id('painbringer'/'ODY', '29835').

card_in_set('pardic firecat', 'ODY').
card_original_type('pardic firecat'/'ODY', 'Creature — Cat').
card_original_text('pardic firecat'/'ODY', 'Haste\nIf Pardic Firecat is in a graveyard, Flame Burst\'s effect counts it as a Flame Burst.').
card_first_print('pardic firecat', 'ODY').
card_image_name('pardic firecat'/'ODY', 'pardic firecat').
card_uid('pardic firecat'/'ODY', 'ODY:Pardic Firecat:pardic firecat').
card_rarity('pardic firecat'/'ODY', 'Common').
card_artist('pardic firecat'/'ODY', 'Glen Angus').
card_number('pardic firecat'/'ODY', '211').
card_flavor_text('pardic firecat'/'ODY', '\"I\'ve heard of putting the cat out, but this isn\'t what I had in mind.\"\n—Nomad sentry').
card_multiverse_id('pardic firecat'/'ODY', '29750').

card_in_set('pardic miner', 'ODY').
card_original_type('pardic miner'/'ODY', 'Creature — Dwarf').
card_original_text('pardic miner'/'ODY', 'Sacrifice Pardic Miner: Target player can\'t play lands this turn.').
card_first_print('pardic miner', 'ODY').
card_image_name('pardic miner'/'ODY', 'pardic miner').
card_uid('pardic miner'/'ODY', 'ODY:Pardic Miner:pardic miner').
card_rarity('pardic miner'/'ODY', 'Rare').
card_artist('pardic miner'/'ODY', 'Tony Szczudlo').
card_number('pardic miner'/'ODY', '212').
card_flavor_text('pardic miner'/'ODY', '\"Punch and drag,\nToss the slag,\nSift the soil,\nLeave the spoil.\"').
card_multiverse_id('pardic miner'/'ODY', '29962').

card_in_set('pardic swordsmith', 'ODY').
card_original_type('pardic swordsmith'/'ODY', 'Creature — Dwarf').
card_original_text('pardic swordsmith'/'ODY', '{R}, Discard a card at random from your hand: Pardic Swordsmith gets +2/+0 until end of turn.').
card_first_print('pardic swordsmith', 'ODY').
card_image_name('pardic swordsmith'/'ODY', 'pardic swordsmith').
card_uid('pardic swordsmith'/'ODY', 'ODY:Pardic Swordsmith:pardic swordsmith').
card_rarity('pardic swordsmith'/'ODY', 'Common').
card_artist('pardic swordsmith'/'ODY', 'Bob Petillo').
card_number('pardic swordsmith'/'ODY', '213').
card_flavor_text('pardic swordsmith'/'ODY', 'A finely crafted blade will never meet as many blows on the battlefield as it did on the anvil.').
card_multiverse_id('pardic swordsmith'/'ODY', '29753').

card_in_set('patchwork gnomes', 'ODY').
card_original_type('patchwork gnomes'/'ODY', 'Artifact Creature — Gnome').
card_original_text('patchwork gnomes'/'ODY', 'Discard a card from your hand: Regenerate Patchwork Gnomes.').
card_image_name('patchwork gnomes'/'ODY', 'patchwork gnomes').
card_uid('patchwork gnomes'/'ODY', 'ODY:Patchwork Gnomes:patchwork gnomes').
card_rarity('patchwork gnomes'/'ODY', 'Uncommon').
card_artist('patchwork gnomes'/'ODY', 'Jerry Tiritilli').
card_number('patchwork gnomes'/'ODY', '306').
card_flavor_text('patchwork gnomes'/'ODY', 'When building gnomes, dwarves adhere to the highest standards. The gnomes themselves aren\'t as particular.').
card_multiverse_id('patchwork gnomes'/'ODY', '31832').

card_in_set('patriarch\'s desire', 'ODY').
card_original_type('patriarch\'s desire'/'ODY', 'Enchant Creature').
card_original_text('patriarch\'s desire'/'ODY', 'Enchanted creature gets +2/-2.\nThreshold Enchanted creature gets an additional +2/-2. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('patriarch\'s desire', 'ODY').
card_image_name('patriarch\'s desire'/'ODY', 'patriarch\'s desire').
card_uid('patriarch\'s desire'/'ODY', 'ODY:Patriarch\'s Desire:patriarch\'s desire').
card_rarity('patriarch\'s desire'/'ODY', 'Common').
card_artist('patriarch\'s desire'/'ODY', 'Eric Peterson').
card_number('patriarch\'s desire'/'ODY', '156').
card_multiverse_id('patriarch\'s desire'/'ODY', '29749').

card_in_set('patrol hound', 'ODY').
card_original_type('patrol hound'/'ODY', 'Creature — Hound').
card_original_text('patrol hound'/'ODY', 'Discard a card from your hand: Patrol Hound gains first strike until end of turn.').
card_first_print('patrol hound', 'ODY').
card_image_name('patrol hound'/'ODY', 'patrol hound').
card_uid('patrol hound'/'ODY', 'ODY:Patrol Hound:patrol hound').
card_rarity('patrol hound'/'ODY', 'Common').
card_artist('patrol hound'/'ODY', 'Daren Bader').
card_number('patrol hound'/'ODY', '38').
card_flavor_text('patrol hound'/'ODY', 'To the camp, it was a fierce and loyal protector. To the sentry\'s youngest daughter, it would always be her \"Wuv Muffin.\"').
card_multiverse_id('patrol hound'/'ODY', '30654').

card_in_set('patron wizard', 'ODY').
card_original_type('patron wizard'/'ODY', 'Creature — Wizard').
card_original_text('patron wizard'/'ODY', 'Tap an untapped Wizard you control: Counter target spell unless its controller pays {1}.').
card_first_print('patron wizard', 'ODY').
card_image_name('patron wizard'/'ODY', 'patron wizard').
card_uid('patron wizard'/'ODY', 'ODY:Patron Wizard:patron wizard').
card_rarity('patron wizard'/'ODY', 'Rare').
card_artist('patron wizard'/'ODY', 'Donato Giancola').
card_number('patron wizard'/'ODY', '89').
card_flavor_text('patron wizard'/'ODY', '\"The best way to teach is by example.\"').
card_multiverse_id('patron wizard'/'ODY', '29925').

card_in_set('pedantic learning', 'ODY').
card_original_type('pedantic learning'/'ODY', 'Enchantment').
card_original_text('pedantic learning'/'ODY', 'Whenever a land card is put into your graveyard from your library, you may pay {1}. If you do, draw a card.').
card_first_print('pedantic learning', 'ODY').
card_image_name('pedantic learning'/'ODY', 'pedantic learning').
card_uid('pedantic learning'/'ODY', 'ODY:Pedantic Learning:pedantic learning').
card_rarity('pedantic learning'/'ODY', 'Rare').
card_artist('pedantic learning'/'ODY', 'Heather Hudson').
card_number('pedantic learning'/'ODY', '90').
card_flavor_text('pedantic learning'/'ODY', 'Even the trivial can inspire.').
card_multiverse_id('pedantic learning'/'ODY', '29940').

card_in_set('peek', 'ODY').
card_original_type('peek'/'ODY', 'Instant').
card_original_text('peek'/'ODY', 'Look at target player\'s hand.\nDraw a card.').
card_first_print('peek', 'ODY').
card_image_name('peek'/'ODY', 'peek').
card_uid('peek'/'ODY', 'ODY:Peek:peek').
card_rarity('peek'/'ODY', 'Common').
card_artist('peek'/'ODY', 'Adam Rex').
card_number('peek'/'ODY', '91').
card_flavor_text('peek'/'ODY', 'Sometimes you have to read between the minds.').
card_multiverse_id('peek'/'ODY', '30686').

card_in_set('persuasion', 'ODY').
card_original_type('persuasion'/'ODY', 'Enchant Creature').
card_original_text('persuasion'/'ODY', 'You control enchanted creature.').
card_first_print('persuasion', 'ODY').
card_image_name('persuasion'/'ODY', 'persuasion').
card_uid('persuasion'/'ODY', 'ODY:Persuasion:persuasion').
card_rarity('persuasion'/'ODY', 'Rare').
card_artist('persuasion'/'ODY', 'Adam Rex').
card_number('persuasion'/'ODY', '92').
card_flavor_text('persuasion'/'ODY', '\"Protocol demands that you bow down before me. I always stand on protocol.\"').
card_multiverse_id('persuasion'/'ODY', '29829').

card_in_set('petrified field', 'ODY').
card_original_type('petrified field'/'ODY', 'Land').
card_original_text('petrified field'/'ODY', '{T}: Add one colorless mana to your mana pool.\n{T}, Sacrifice Petrified Field: Return target land card from your graveyard to your hand.').
card_first_print('petrified field', 'ODY').
card_image_name('petrified field'/'ODY', 'petrified field').
card_uid('petrified field'/'ODY', 'ODY:Petrified Field:petrified field').
card_rarity('petrified field'/'ODY', 'Rare').
card_artist('petrified field'/'ODY', 'Glen Angus').
card_number('petrified field'/'ODY', '323').
card_multiverse_id('petrified field'/'ODY', '30014').

card_in_set('phantatog', 'ODY').
card_original_type('phantatog'/'ODY', 'Creature — Atog').
card_original_text('phantatog'/'ODY', 'Sacrifice an enchantment: Phantatog gets +1/+1 until end of turn.\nDiscard a card from your hand: Phantatog gets +1/+1 until end of turn.').
card_first_print('phantatog', 'ODY').
card_image_name('phantatog'/'ODY', 'phantatog').
card_uid('phantatog'/'ODY', 'ODY:Phantatog:phantatog').
card_rarity('phantatog'/'ODY', 'Uncommon').
card_artist('phantatog'/'ODY', 'Glen Angus').
card_number('phantatog'/'ODY', '291').
card_multiverse_id('phantatog'/'ODY', '31792').

card_in_set('phantom whelp', 'ODY').
card_original_type('phantom whelp'/'ODY', 'Creature — Hound').
card_original_text('phantom whelp'/'ODY', 'When Phantom Whelp attacks or blocks, return it to its owner\'s hand at end of combat.').
card_first_print('phantom whelp', 'ODY').
card_image_name('phantom whelp'/'ODY', 'phantom whelp').
card_uid('phantom whelp'/'ODY', 'ODY:Phantom Whelp:phantom whelp').
card_rarity('phantom whelp'/'ODY', 'Common').
card_artist('phantom whelp'/'ODY', 'Wayne England').
card_number('phantom whelp'/'ODY', '93').
card_flavor_text('phantom whelp'/'ODY', 'It lurks in the mist, waiting for stragglers to fall behind. When the fog clears, nothing remains but footprints.').
card_multiverse_id('phantom whelp'/'ODY', '31814').

card_in_set('pianna, nomad captain', 'ODY').
card_original_type('pianna, nomad captain'/'ODY', 'Creature — Nomad Legend').
card_original_text('pianna, nomad captain'/'ODY', 'Whenever Pianna, Nomad Captain attacks, attacking creatures get +1/+1 until end of turn.').
card_first_print('pianna, nomad captain', 'ODY').
card_image_name('pianna, nomad captain'/'ODY', 'pianna, nomad captain').
card_uid('pianna, nomad captain'/'ODY', 'ODY:Pianna, Nomad Captain:pianna, nomad captain').
card_rarity('pianna, nomad captain'/'ODY', 'Rare').
card_artist('pianna, nomad captain'/'ODY', 'D. Alexander Gregory').
card_number('pianna, nomad captain'/'ODY', '39').
card_flavor_text('pianna, nomad captain'/'ODY', 'Some find inspiration in their swords. Others find it in their leaders.').
card_multiverse_id('pianna, nomad captain'/'ODY', '31790').

card_in_set('pilgrim of justice', 'ODY').
card_original_type('pilgrim of justice'/'ODY', 'Creature — Cleric').
card_original_text('pilgrim of justice'/'ODY', 'Protection from red\n{W}, Sacrifice Pilgrim of Justice: The next time a red source of your choice would deal damage this turn, prevent that damage.').
card_first_print('pilgrim of justice', 'ODY').
card_image_name('pilgrim of justice'/'ODY', 'pilgrim of justice').
card_uid('pilgrim of justice'/'ODY', 'ODY:Pilgrim of Justice:pilgrim of justice').
card_rarity('pilgrim of justice'/'ODY', 'Common').
card_artist('pilgrim of justice'/'ODY', 'Orizio Daniele').
card_number('pilgrim of justice'/'ODY', '40').
card_flavor_text('pilgrim of justice'/'ODY', '\"I would die a thousand deaths before I let you harm another.\"').
card_multiverse_id('pilgrim of justice'/'ODY', '29690').

card_in_set('pilgrim of virtue', 'ODY').
card_original_type('pilgrim of virtue'/'ODY', 'Creature — Cleric').
card_original_text('pilgrim of virtue'/'ODY', 'Protection from black\n{W}, Sacrifice Pilgrim of Virtue: The next time a black source of your choice would deal damage this turn, prevent that damage.').
card_first_print('pilgrim of virtue', 'ODY').
card_image_name('pilgrim of virtue'/'ODY', 'pilgrim of virtue').
card_uid('pilgrim of virtue'/'ODY', 'ODY:Pilgrim of Virtue:pilgrim of virtue').
card_rarity('pilgrim of virtue'/'ODY', 'Common').
card_artist('pilgrim of virtue'/'ODY', 'Massimilano Frezzato').
card_number('pilgrim of virtue'/'ODY', '41').
card_flavor_text('pilgrim of virtue'/'ODY', '\"A life is measured by the lives it saves.\"').
card_multiverse_id('pilgrim of virtue'/'ODY', '29689').

card_in_set('piper\'s melody', 'ODY').
card_original_type('piper\'s melody'/'ODY', 'Sorcery').
card_original_text('piper\'s melody'/'ODY', 'Shuffle any number of target creature cards from your graveyard into your library.').
card_first_print('piper\'s melody', 'ODY').
card_image_name('piper\'s melody'/'ODY', 'piper\'s melody').
card_uid('piper\'s melody'/'ODY', 'ODY:Piper\'s Melody:piper\'s melody').
card_rarity('piper\'s melody'/'ODY', 'Uncommon').
card_artist('piper\'s melody'/'ODY', 'Greg & Tim Hildebrandt').
card_number('piper\'s melody'/'ODY', '261').
card_flavor_text('piper\'s melody'/'ODY', 'Those who follow nature\'s path eventually walk in a circle.').
card_multiverse_id('piper\'s melody'/'ODY', '29882').

card_in_set('plains', 'ODY').
card_original_type('plains'/'ODY', 'Land').
card_original_text('plains'/'ODY', 'W').
card_image_name('plains'/'ODY', 'plains1').
card_uid('plains'/'ODY', 'ODY:Plains:plains1').
card_rarity('plains'/'ODY', 'Basic Land').
card_artist('plains'/'ODY', 'Alan Pollack').
card_number('plains'/'ODY', '331').
card_multiverse_id('plains'/'ODY', '31612').

card_in_set('plains', 'ODY').
card_original_type('plains'/'ODY', 'Land').
card_original_text('plains'/'ODY', 'W').
card_image_name('plains'/'ODY', 'plains2').
card_uid('plains'/'ODY', 'ODY:Plains:plains2').
card_rarity('plains'/'ODY', 'Basic Land').
card_artist('plains'/'ODY', 'Don Hazeltine').
card_number('plains'/'ODY', '332').
card_multiverse_id('plains'/'ODY', '31629').

card_in_set('plains', 'ODY').
card_original_type('plains'/'ODY', 'Land').
card_original_text('plains'/'ODY', 'W').
card_image_name('plains'/'ODY', 'plains3').
card_uid('plains'/'ODY', 'ODY:Plains:plains3').
card_rarity('plains'/'ODY', 'Basic Land').
card_artist('plains'/'ODY', 'Eric Peterson').
card_number('plains'/'ODY', '333').
card_multiverse_id('plains'/'ODY', '31630').

card_in_set('plains', 'ODY').
card_original_type('plains'/'ODY', 'Land').
card_original_text('plains'/'ODY', 'W').
card_image_name('plains'/'ODY', 'plains4').
card_uid('plains'/'ODY', 'ODY:Plains:plains4').
card_rarity('plains'/'ODY', 'Basic Land').
card_artist('plains'/'ODY', 'Rob Alexander').
card_number('plains'/'ODY', '334').
card_multiverse_id('plains'/'ODY', '31631').

card_in_set('predict', 'ODY').
card_original_type('predict'/'ODY', 'Instant').
card_original_text('predict'/'ODY', 'Name a card, then put the top card of target player\'s library into his or her graveyard. If that card is the named card, you draw two cards. Otherwise, you draw a card.').
card_first_print('predict', 'ODY').
card_image_name('predict'/'ODY', 'predict').
card_uid('predict'/'ODY', 'ODY:Predict:predict').
card_rarity('predict'/'ODY', 'Uncommon').
card_artist('predict'/'ODY', 'Rebecca Guay').
card_number('predict'/'ODY', '94').
card_multiverse_id('predict'/'ODY', '29823').

card_in_set('price of glory', 'ODY').
card_original_type('price of glory'/'ODY', 'Enchantment').
card_original_text('price of glory'/'ODY', 'Whenever a player taps a land for mana during another player\'s turn, destroy that land.').
card_first_print('price of glory', 'ODY').
card_image_name('price of glory'/'ODY', 'price of glory').
card_uid('price of glory'/'ODY', 'ODY:Price of Glory:price of glory').
card_rarity('price of glory'/'ODY', 'Uncommon').
card_artist('price of glory'/'ODY', 'Darrell Riche').
card_number('price of glory'/'ODY', '214').
card_flavor_text('price of glory'/'ODY', '\"True strength lies in action. Let the weak react to me.\"\n—Kamahl, pit fighter').
card_multiverse_id('price of glory'/'ODY', '29973').

card_in_set('primal frenzy', 'ODY').
card_original_type('primal frenzy'/'ODY', 'Enchant Creature').
card_original_text('primal frenzy'/'ODY', 'Enchanted creature has trample.').
card_first_print('primal frenzy', 'ODY').
card_image_name('primal frenzy'/'ODY', 'primal frenzy').
card_uid('primal frenzy'/'ODY', 'ODY:Primal Frenzy:primal frenzy').
card_rarity('primal frenzy'/'ODY', 'Common').
card_artist('primal frenzy'/'ODY', 'Alex Horley-Orlandelli').
card_number('primal frenzy'/'ODY', '262').
card_flavor_text('primal frenzy'/'ODY', '\"Don\'t dismiss its anger as mindless fury. I assure you, it has purpose.\"\n—Seton, centaur druid').
card_multiverse_id('primal frenzy'/'ODY', '31812').

card_in_set('psionic gift', 'ODY').
card_original_type('psionic gift'/'ODY', 'Enchant Creature').
card_original_text('psionic gift'/'ODY', 'Enchanted creature has \"{T}: This creature deals 1 damage to target creature or player.\"').
card_first_print('psionic gift', 'ODY').
card_image_name('psionic gift'/'ODY', 'psionic gift').
card_uid('psionic gift'/'ODY', 'ODY:Psionic Gift:psionic gift').
card_rarity('psionic gift'/'ODY', 'Common').
card_artist('psionic gift'/'ODY', 'Orizio Daniele').
card_number('psionic gift'/'ODY', '95').
card_flavor_text('psionic gift'/'ODY', 'For sale: Complete wizard\'s library. Rare scrolls, grimoires, and works of ancient spellcrafting! Slightly abridged.').
card_multiverse_id('psionic gift'/'ODY', '31791').

card_in_set('psychatog', 'ODY').
card_original_type('psychatog'/'ODY', 'Creature — Atog').
card_original_text('psychatog'/'ODY', 'Discard a card from your hand: Psychatog gets +1/+1 until end of turn.\nRemove two cards in your graveyard from the game: Psychatog gets +1/+1 until end of turn.').
card_image_name('psychatog'/'ODY', 'psychatog').
card_uid('psychatog'/'ODY', 'ODY:Psychatog:psychatog').
card_rarity('psychatog'/'ODY', 'Uncommon').
card_artist('psychatog'/'ODY', 'Edward P. Beard, Jr.').
card_number('psychatog'/'ODY', '292').
card_multiverse_id('psychatog'/'ODY', '31825').

card_in_set('pulsating illusion', 'ODY').
card_original_type('pulsating illusion'/'ODY', 'Creature — Illusion').
card_original_text('pulsating illusion'/'ODY', 'Flying\nDiscard a card from your hand: Pulsating Illusion gets +4/+4 until end of turn. Play this ability only once each turn.').
card_first_print('pulsating illusion', 'ODY').
card_image_name('pulsating illusion'/'ODY', 'pulsating illusion').
card_uid('pulsating illusion'/'ODY', 'ODY:Pulsating Illusion:pulsating illusion').
card_rarity('pulsating illusion'/'ODY', 'Uncommon').
card_artist('pulsating illusion'/'ODY', 'Arnie Swekel').
card_number('pulsating illusion'/'ODY', '96').
card_multiverse_id('pulsating illusion'/'ODY', '31611').

card_in_set('puppeteer', 'ODY').
card_original_type('puppeteer'/'ODY', 'Creature — Wizard').
card_original_text('puppeteer'/'ODY', '{U}, {T}: Tap or untap target creature.').
card_first_print('puppeteer', 'ODY').
card_image_name('puppeteer'/'ODY', 'puppeteer').
card_uid('puppeteer'/'ODY', 'ODY:Puppeteer:puppeteer').
card_rarity('puppeteer'/'ODY', 'Uncommon').
card_artist('puppeteer'/'ODY', 'Scott M. Fischer').
card_number('puppeteer'/'ODY', '97').
card_flavor_text('puppeteer'/'ODY', '\"Getting people to do what you want is merely a matter of telling them what they want to hear.\"').
card_multiverse_id('puppeteer'/'ODY', '29819').

card_in_set('rabid elephant', 'ODY').
card_original_type('rabid elephant'/'ODY', 'Creature — Elephant').
card_original_text('rabid elephant'/'ODY', 'Whenever Rabid Elephant becomes blocked, it gets +2/+2 until end of turn for each creature blocking it.').
card_first_print('rabid elephant', 'ODY').
card_image_name('rabid elephant'/'ODY', 'rabid elephant').
card_uid('rabid elephant'/'ODY', 'ODY:Rabid Elephant:rabid elephant').
card_rarity('rabid elephant'/'ODY', 'Common').
card_artist('rabid elephant'/'ODY', 'Dave Dorman').
card_number('rabid elephant'/'ODY', '263').
card_flavor_text('rabid elephant'/'ODY', 'When in doubt, stomp.').
card_multiverse_id('rabid elephant'/'ODY', '29781').

card_in_set('ravaged highlands', 'ODY').
card_original_type('ravaged highlands'/'ODY', 'Land').
card_original_text('ravaged highlands'/'ODY', 'Ravaged Highlands comes into play tapped.\n{T}: Add {R} to your mana pool.\n{T}, Sacrifice Ravaged Highlands: Add one mana of any color to your mana pool.').
card_first_print('ravaged highlands', 'ODY').
card_image_name('ravaged highlands'/'ODY', 'ravaged highlands').
card_uid('ravaged highlands'/'ODY', 'ODY:Ravaged Highlands:ravaged highlands').
card_rarity('ravaged highlands'/'ODY', 'Common').
card_artist('ravaged highlands'/'ODY', 'David Martin').
card_number('ravaged highlands'/'ODY', '324').
card_multiverse_id('ravaged highlands'/'ODY', '31764').

card_in_set('ray of distortion', 'ODY').
card_original_type('ray of distortion'/'ODY', 'Instant').
card_original_text('ray of distortion'/'ODY', 'Destroy target artifact or enchantment.\nFlashback {4}{W}{W} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('ray of distortion', 'ODY').
card_image_name('ray of distortion'/'ODY', 'ray of distortion').
card_uid('ray of distortion'/'ODY', 'ODY:Ray of Distortion:ray of distortion').
card_rarity('ray of distortion'/'ODY', 'Common').
card_artist('ray of distortion'/'ODY', 'Carl Critchlow').
card_number('ray of distortion'/'ODY', '42').
card_multiverse_id('ray of distortion'/'ODY', '29808').

card_in_set('reckless charge', 'ODY').
card_original_type('reckless charge'/'ODY', 'Sorcery').
card_original_text('reckless charge'/'ODY', 'Target creature gets +3/+0 and gains haste until end of turn.\nFlashback {2}{R} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('reckless charge', 'ODY').
card_image_name('reckless charge'/'ODY', 'reckless charge').
card_uid('reckless charge'/'ODY', 'ODY:Reckless Charge:reckless charge').
card_rarity('reckless charge'/'ODY', 'Common').
card_artist('reckless charge'/'ODY', 'Scott M. Fischer').
card_number('reckless charge'/'ODY', '215').
card_multiverse_id('reckless charge'/'ODY', '31450').

card_in_set('recoup', 'ODY').
card_original_type('recoup'/'ODY', 'Sorcery').
card_original_text('recoup'/'ODY', 'Target sorcery card in your graveyard gains flashback until end of turn. Its flashback cost is equal to its mana cost. (Mana cost includes color.)\nFlashback {3}{R} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('recoup', 'ODY').
card_image_name('recoup'/'ODY', 'recoup').
card_uid('recoup'/'ODY', 'ODY:Recoup:recoup').
card_rarity('recoup'/'ODY', 'Uncommon').
card_artist('recoup'/'ODY', 'Dave Dorman').
card_number('recoup'/'ODY', '216').
card_multiverse_id('recoup'/'ODY', '29864').

card_in_set('refresh', 'ODY').
card_original_type('refresh'/'ODY', 'Instant').
card_original_text('refresh'/'ODY', 'Regenerate target creature.\nDraw a card.').
card_first_print('refresh', 'ODY').
card_image_name('refresh'/'ODY', 'refresh').
card_uid('refresh'/'ODY', 'ODY:Refresh:refresh').
card_rarity('refresh'/'ODY', 'Common').
card_artist('refresh'/'ODY', 'Keith Garletts').
card_number('refresh'/'ODY', '264').
card_flavor_text('refresh'/'ODY', 'Death is a moth that dances toward our inner flame. The brightest fire can singe its wings.\n—Nantuko teaching').
card_multiverse_id('refresh'/'ODY', '31741').

card_in_set('repel', 'ODY').
card_original_type('repel'/'ODY', 'Instant').
card_original_text('repel'/'ODY', 'Put target creature on top of its owner\'s library.').
card_first_print('repel', 'ODY').
card_image_name('repel'/'ODY', 'repel').
card_uid('repel'/'ODY', 'ODY:Repel:repel').
card_rarity('repel'/'ODY', 'Common').
card_artist('repel'/'ODY', 'Terese Nielsen').
card_number('repel'/'ODY', '98').
card_flavor_text('repel'/'ODY', '\"I have a hunch we\'ll be seeing that one again soon.\"\n—Ambassador Laquatus').
card_multiverse_id('repel'/'ODY', '29723').

card_in_set('repentant vampire', 'ODY').
card_original_type('repentant vampire'/'ODY', 'Creature — Vampire').
card_original_text('repentant vampire'/'ODY', 'Flying\nWhenever a creature dealt damage by Repentant Vampire this turn is put into a graveyard, put a +1/+1 counter on Repentant Vampire.\nThreshold Repentant Vampire is white and has \"{T}: Destroy target black creature.\"').
card_first_print('repentant vampire', 'ODY').
card_image_name('repentant vampire'/'ODY', 'repentant vampire').
card_uid('repentant vampire'/'ODY', 'ODY:Repentant Vampire:repentant vampire').
card_rarity('repentant vampire'/'ODY', 'Rare').
card_artist('repentant vampire'/'ODY', 'Mark Tedin').
card_number('repentant vampire'/'ODY', '157').
card_multiverse_id('repentant vampire'/'ODY', '29945').

card_in_set('resilient wanderer', 'ODY').
card_original_type('resilient wanderer'/'ODY', 'Creature — Nomad').
card_original_text('resilient wanderer'/'ODY', 'First strike\nDiscard a card from your hand: Resilient Wanderer gains protection from the color of your choice until end of turn.').
card_first_print('resilient wanderer', 'ODY').
card_image_name('resilient wanderer'/'ODY', 'resilient wanderer').
card_uid('resilient wanderer'/'ODY', 'ODY:Resilient Wanderer:resilient wanderer').
card_rarity('resilient wanderer'/'ODY', 'Uncommon').
card_artist('resilient wanderer'/'ODY', 'Clyde Caldwell').
card_number('resilient wanderer'/'ODY', '43').
card_flavor_text('resilient wanderer'/'ODY', 'Nomads understand many cultures and therefore travel without fear.').
card_multiverse_id('resilient wanderer'/'ODY', '29804').

card_in_set('rites of initiation', 'ODY').
card_original_type('rites of initiation'/'ODY', 'Instant').
card_original_text('rites of initiation'/'ODY', 'Discard any number of cards at random from your hand. Creatures you control get +1/+0 until end of turn for each card discarded this way.').
card_first_print('rites of initiation', 'ODY').
card_image_name('rites of initiation'/'ODY', 'rites of initiation').
card_uid('rites of initiation'/'ODY', 'ODY:Rites of Initiation:rites of initiation').
card_rarity('rites of initiation'/'ODY', 'Common').
card_artist('rites of initiation'/'ODY', 'Bradley Williams').
card_number('rites of initiation'/'ODY', '217').
card_multiverse_id('rites of initiation'/'ODY', '29765').

card_in_set('rites of refusal', 'ODY').
card_original_type('rites of refusal'/'ODY', 'Instant').
card_original_text('rites of refusal'/'ODY', 'Discard any number of cards from your hand. Counter target spell unless its controller pays {3} for each card discarded this way.').
card_first_print('rites of refusal', 'ODY').
card_image_name('rites of refusal'/'ODY', 'rites of refusal').
card_uid('rites of refusal'/'ODY', 'ODY:Rites of Refusal:rites of refusal').
card_rarity('rites of refusal'/'ODY', 'Common').
card_artist('rites of refusal'/'ODY', 'Bradley Williams').
card_number('rites of refusal'/'ODY', '99').
card_multiverse_id('rites of refusal'/'ODY', '29722').

card_in_set('rites of spring', 'ODY').
card_original_type('rites of spring'/'ODY', 'Sorcery').
card_original_text('rites of spring'/'ODY', 'Discard any number of cards from your hand. Search your library for that many basic land cards, reveal those cards, and put them into your hand. Then shuffle your library.').
card_first_print('rites of spring', 'ODY').
card_image_name('rites of spring'/'ODY', 'rites of spring').
card_uid('rites of spring'/'ODY', 'ODY:Rites of Spring:rites of spring').
card_rarity('rites of spring'/'ODY', 'Common').
card_artist('rites of spring'/'ODY', 'Bradley Williams').
card_number('rites of spring'/'ODY', '265').
card_multiverse_id('rites of spring'/'ODY', '29789').

card_in_set('roar of the wurm', 'ODY').
card_original_type('roar of the wurm'/'ODY', 'Sorcery').
card_original_text('roar of the wurm'/'ODY', 'Put a 6/6 green Wurm creature token into play.\nFlashback {3}{G} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_image_name('roar of the wurm'/'ODY', 'roar of the wurm').
card_uid('roar of the wurm'/'ODY', 'ODY:Roar of the Wurm:roar of the wurm').
card_rarity('roar of the wurm'/'ODY', 'Uncommon').
card_artist('roar of the wurm'/'ODY', 'Kev Walker').
card_number('roar of the wurm'/'ODY', '266').
card_multiverse_id('roar of the wurm'/'ODY', '29881').

card_in_set('rotting giant', 'ODY').
card_original_type('rotting giant'/'ODY', 'Creature — Zombie Giant').
card_original_text('rotting giant'/'ODY', 'Whenever Rotting Giant attacks or blocks, sacrifice it unless you remove a card in your graveyard from the game.').
card_first_print('rotting giant', 'ODY').
card_image_name('rotting giant'/'ODY', 'rotting giant').
card_uid('rotting giant'/'ODY', 'ODY:Rotting Giant:rotting giant').
card_rarity('rotting giant'/'ODY', 'Uncommon').
card_artist('rotting giant'/'ODY', 'Kev Walker').
card_number('rotting giant'/'ODY', '158').
card_flavor_text('rotting giant'/'ODY', 'Wracked with rot, it wrecks and ruins.').
card_multiverse_id('rotting giant'/'ODY', '29842').

card_in_set('sacred rites', 'ODY').
card_original_type('sacred rites'/'ODY', 'Instant').
card_original_text('sacred rites'/'ODY', 'Discard any number of cards from your hand. Creatures you control get +0/+1 until end of turn for each card discarded this way.').
card_first_print('sacred rites', 'ODY').
card_image_name('sacred rites'/'ODY', 'sacred rites').
card_uid('sacred rites'/'ODY', 'ODY:Sacred Rites:sacred rites').
card_rarity('sacred rites'/'ODY', 'Common').
card_artist('sacred rites'/'ODY', 'Bradley Williams').
card_number('sacred rites'/'ODY', '44').
card_multiverse_id('sacred rites'/'ODY', '31768').

card_in_set('sadistic hypnotist', 'ODY').
card_original_type('sadistic hypnotist'/'ODY', 'Creature — Minion').
card_original_text('sadistic hypnotist'/'ODY', 'Sacrifice a creature: Target player discards two cards from his or her hand. Play this ability only any time you could play a sorcery.').
card_first_print('sadistic hypnotist', 'ODY').
card_image_name('sadistic hypnotist'/'ODY', 'sadistic hypnotist').
card_uid('sadistic hypnotist'/'ODY', 'ODY:Sadistic Hypnotist:sadistic hypnotist').
card_rarity('sadistic hypnotist'/'ODY', 'Uncommon').
card_artist('sadistic hypnotist'/'ODY', 'Paolo Parente').
card_number('sadistic hypnotist'/'ODY', '159').
card_flavor_text('sadistic hypnotist'/'ODY', '\"Victory in the pits is almost as rewarding as the methods I use to achieve it.\"').
card_multiverse_id('sadistic hypnotist'/'ODY', '31809').

card_in_set('sandstone deadfall', 'ODY').
card_original_type('sandstone deadfall'/'ODY', 'Artifact').
card_original_text('sandstone deadfall'/'ODY', '{T}, Sacrifice two lands and Sandstone Deadfall: Destroy target attacking creature.').
card_first_print('sandstone deadfall', 'ODY').
card_image_name('sandstone deadfall'/'ODY', 'sandstone deadfall').
card_uid('sandstone deadfall'/'ODY', 'ODY:Sandstone Deadfall:sandstone deadfall').
card_rarity('sandstone deadfall'/'ODY', 'Uncommon').
card_artist('sandstone deadfall'/'ODY', 'Jim Nelson').
card_number('sandstone deadfall'/'ODY', '307').
card_flavor_text('sandstone deadfall'/'ODY', '\"Two or three tons of rock can really get you down—and keep you there.\"\n—Kamahl, pit fighter').
card_multiverse_id('sandstone deadfall'/'ODY', '29893').

card_in_set('sarcatog', 'ODY').
card_original_type('sarcatog'/'ODY', 'Creature — Atog').
card_original_text('sarcatog'/'ODY', 'Remove two cards in your graveyard from the game: Sarcatog gets +1/+1 until end of turn.\nSacrifice an artifact: Sarcatog gets +1/+1 until end of turn.').
card_first_print('sarcatog', 'ODY').
card_image_name('sarcatog'/'ODY', 'sarcatog').
card_uid('sarcatog'/'ODY', 'ODY:Sarcatog:sarcatog').
card_rarity('sarcatog'/'ODY', 'Uncommon').
card_artist('sarcatog'/'ODY', 'Mark Brill').
card_number('sarcatog'/'ODY', '293').
card_multiverse_id('sarcatog'/'ODY', '31793').

card_in_set('savage firecat', 'ODY').
card_original_type('savage firecat'/'ODY', 'Creature — Cat').
card_original_text('savage firecat'/'ODY', 'Trample\nSavage Firecat comes into play with seven +1/+1 counters on it.\nWhenever you tap a land for mana, remove a +1/+1 counter from Savage Firecat.').
card_first_print('savage firecat', 'ODY').
card_image_name('savage firecat'/'ODY', 'savage firecat').
card_uid('savage firecat'/'ODY', 'ODY:Savage Firecat:savage firecat').
card_rarity('savage firecat'/'ODY', 'Rare').
card_artist('savage firecat'/'ODY', 'Dave Dorman').
card_number('savage firecat'/'ODY', '218').
card_multiverse_id('savage firecat'/'ODY', '29966').

card_in_set('scorching missile', 'ODY').
card_original_type('scorching missile'/'ODY', 'Sorcery').
card_original_text('scorching missile'/'ODY', 'Scorching Missile deals 4 damage to target player.\nFlashback {9}{R} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('scorching missile', 'ODY').
card_image_name('scorching missile'/'ODY', 'scorching missile').
card_uid('scorching missile'/'ODY', 'ODY:Scorching Missile:scorching missile').
card_rarity('scorching missile'/'ODY', 'Common').
card_artist('scorching missile'/'ODY', 'Don Hazeltine').
card_number('scorching missile'/'ODY', '219').
card_multiverse_id('scorching missile'/'ODY', '30572').

card_in_set('screams of the damned', 'ODY').
card_original_type('screams of the damned'/'ODY', 'Enchantment').
card_original_text('screams of the damned'/'ODY', '{1}{B}, Remove a card in your graveyard from the game: Screams of the Damned deals 1 damage to each creature and each player.').
card_first_print('screams of the damned', 'ODY').
card_image_name('screams of the damned'/'ODY', 'screams of the damned').
card_uid('screams of the damned'/'ODY', 'ODY:Screams of the Damned:screams of the damned').
card_rarity('screams of the damned'/'ODY', 'Uncommon').
card_artist('screams of the damned'/'ODY', 'Jerry Tiritilli').
card_number('screams of the damned'/'ODY', '160').
card_flavor_text('screams of the damned'/'ODY', 'It\'s important to face your inner demons . . . unless they\'re the kind of demons that can kill you.').
card_multiverse_id('screams of the damned'/'ODY', '29845').

card_in_set('scrivener', 'ODY').
card_original_type('scrivener'/'ODY', 'Creature — Townsfolk').
card_original_text('scrivener'/'ODY', 'When Scrivener comes into play, you may return target instant card from your graveyard to your hand.').
card_image_name('scrivener'/'ODY', 'scrivener').
card_uid('scrivener'/'ODY', 'ODY:Scrivener:scrivener').
card_rarity('scrivener'/'ODY', 'Common').
card_artist('scrivener'/'ODY', 'Kev Walker').
card_number('scrivener'/'ODY', '100').
card_flavor_text('scrivener'/'ODY', 'A good memory is no match for a good scribe.').
card_multiverse_id('scrivener'/'ODY', '29712').

card_in_set('seafloor debris', 'ODY').
card_original_type('seafloor debris'/'ODY', 'Land').
card_original_text('seafloor debris'/'ODY', 'Seafloor Debris comes into play tapped.\n{T}: Add {U} to your mana pool.\n{T}, Sacrifice Seafloor Debris: Add one mana of any color to your mana pool.').
card_first_print('seafloor debris', 'ODY').
card_image_name('seafloor debris'/'ODY', 'seafloor debris').
card_uid('seafloor debris'/'ODY', 'ODY:Seafloor Debris:seafloor debris').
card_rarity('seafloor debris'/'ODY', 'Common').
card_artist('seafloor debris'/'ODY', 'Larry Elmore').
card_number('seafloor debris'/'ODY', '325').
card_multiverse_id('seafloor debris'/'ODY', '31762').

card_in_set('second thoughts', 'ODY').
card_original_type('second thoughts'/'ODY', 'Instant').
card_original_text('second thoughts'/'ODY', 'Remove target attacking creature from the game.\nDraw a card.').
card_first_print('second thoughts', 'ODY').
card_image_name('second thoughts'/'ODY', 'second thoughts').
card_uid('second thoughts'/'ODY', 'ODY:Second Thoughts:second thoughts').
card_rarity('second thoughts'/'ODY', 'Common').
card_artist('second thoughts'/'ODY', 'Ray Lago').
card_number('second thoughts'/'ODY', '45').
card_flavor_text('second thoughts'/'ODY', 'It suddenly dawned on Jorin that farming was quite a noble profession.').
card_multiverse_id('second thoughts'/'ODY', '29706').

card_in_set('seize the day', 'ODY').
card_original_type('seize the day'/'ODY', 'Sorcery').
card_original_text('seize the day'/'ODY', 'Untap target creature. After this phase, there is an additional combat phase followed by an additional main phase.\nFlashback {2}{R} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('seize the day', 'ODY').
card_image_name('seize the day'/'ODY', 'seize the day').
card_uid('seize the day'/'ODY', 'ODY:Seize the Day:seize the day').
card_rarity('seize the day'/'ODY', 'Rare').
card_artist('seize the day'/'ODY', 'Greg Staples').
card_number('seize the day'/'ODY', '220').
card_multiverse_id('seize the day'/'ODY', '30573').

card_in_set('seton\'s desire', 'ODY').
card_original_type('seton\'s desire'/'ODY', 'Enchant Creature').
card_original_text('seton\'s desire'/'ODY', 'Enchanted creature gets +2/+2.\nThreshold All creatures able to block enchanted creature do so. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('seton\'s desire', 'ODY').
card_image_name('seton\'s desire'/'ODY', 'seton\'s desire').
card_uid('seton\'s desire'/'ODY', 'ODY:Seton\'s Desire:seton\'s desire').
card_rarity('seton\'s desire'/'ODY', 'Common').
card_artist('seton\'s desire'/'ODY', 'Ciruelo').
card_number('seton\'s desire'/'ODY', '268').
card_multiverse_id('seton\'s desire'/'ODY', '29791').

card_in_set('seton, krosan protector', 'ODY').
card_original_type('seton, krosan protector'/'ODY', 'Creature — Centaur Druid Legend').
card_original_text('seton, krosan protector'/'ODY', 'Tap an untapped Druid you control: Add {G} to your mana pool.').
card_first_print('seton, krosan protector', 'ODY').
card_image_name('seton, krosan protector'/'ODY', 'seton, krosan protector').
card_uid('seton, krosan protector'/'ODY', 'ODY:Seton, Krosan Protector:seton, krosan protector').
card_rarity('seton, krosan protector'/'ODY', 'Rare').
card_artist('seton, krosan protector'/'ODY', 'Greg Staples').
card_number('seton, krosan protector'/'ODY', '267').
card_flavor_text('seton, krosan protector'/'ODY', '\"Survival of the fittest is not only the law of the pits, it is the law of nature. That is why I fight.\"').
card_multiverse_id('seton, krosan protector'/'ODY', '29985').

card_in_set('shadowblood egg', 'ODY').
card_original_type('shadowblood egg'/'ODY', 'Artifact').
card_original_text('shadowblood egg'/'ODY', '{2}, {T}, Sacrifice Shadowblood Egg: Add {B}{R} to your mana pool. Draw a card.').
card_first_print('shadowblood egg', 'ODY').
card_image_name('shadowblood egg'/'ODY', 'shadowblood egg').
card_uid('shadowblood egg'/'ODY', 'ODY:Shadowblood Egg:shadowblood egg').
card_rarity('shadowblood egg'/'ODY', 'Uncommon').
card_artist('shadowblood egg'/'ODY', 'David Martin').
card_number('shadowblood egg'/'ODY', '308').
card_flavor_text('shadowblood egg'/'ODY', '\"Does your passion for power consume your soul?\"\n—Watnik, master glassblower').
card_multiverse_id('shadowblood egg'/'ODY', '30553').

card_in_set('shadowblood ridge', 'ODY').
card_original_type('shadowblood ridge'/'ODY', 'Land').
card_original_text('shadowblood ridge'/'ODY', '{1}, {T}: Add {B}{R} to your mana pool.').
card_first_print('shadowblood ridge', 'ODY').
card_image_name('shadowblood ridge'/'ODY', 'shadowblood ridge').
card_uid('shadowblood ridge'/'ODY', 'ODY:Shadowblood Ridge:shadowblood ridge').
card_rarity('shadowblood ridge'/'ODY', 'Rare').
card_artist('shadowblood ridge'/'ODY', 'Glen Angus').
card_number('shadowblood ridge'/'ODY', '326').
card_flavor_text('shadowblood ridge'/'ODY', 'The carnage of conflicts long past still stains the Otarian soil.').
card_multiverse_id('shadowblood ridge'/'ODY', '29794').

card_in_set('shadowmage infiltrator', 'ODY').
card_original_type('shadowmage infiltrator'/'ODY', 'Creature — Wizard').
card_original_text('shadowmage infiltrator'/'ODY', 'Shadowmage Infiltrator can\'t be blocked except by artifact creatures and/or black creatures.\nWhenever Shadowmage Infiltrator deals combat damage to a player, you may draw a card.').
card_first_print('shadowmage infiltrator', 'ODY').
card_image_name('shadowmage infiltrator'/'ODY', 'shadowmage infiltrator').
card_uid('shadowmage infiltrator'/'ODY', 'ODY:Shadowmage Infiltrator:shadowmage infiltrator').
card_rarity('shadowmage infiltrator'/'ODY', 'Rare').
card_artist('shadowmage infiltrator'/'ODY', 'Rick Farrell').
card_number('shadowmage infiltrator'/'ODY', '294').
card_multiverse_id('shadowmage infiltrator'/'ODY', '33604').

card_in_set('shelter', 'ODY').
card_original_type('shelter'/'ODY', 'Instant').
card_original_text('shelter'/'ODY', 'Target creature you control gains protection from the color of your choice until end of turn.\nDraw a card.').
card_first_print('shelter', 'ODY').
card_image_name('shelter'/'ODY', 'shelter').
card_uid('shelter'/'ODY', 'ODY:Shelter:shelter').
card_rarity('shelter'/'ODY', 'Common').
card_artist('shelter'/'ODY', 'Christopher Moeller').
card_number('shelter'/'ODY', '46').
card_flavor_text('shelter'/'ODY', 'Good strategists seize opportunities. Great strategists make their own.').
card_multiverse_id('shelter'/'ODY', '30761').

card_in_set('shifty doppelganger', 'ODY').
card_original_type('shifty doppelganger'/'ODY', 'Creature — Shapeshifter').
card_original_text('shifty doppelganger'/'ODY', '{3}{U}, Remove Shifty Doppelganger from the game: Put a creature card from your hand into play. That creature gains haste until end of turn. At end of turn, sacrifice that creature. If you do, return Shifty Doppelganger to play.').
card_first_print('shifty doppelganger', 'ODY').
card_image_name('shifty doppelganger'/'ODY', 'shifty doppelganger').
card_uid('shifty doppelganger'/'ODY', 'ODY:Shifty Doppelganger:shifty doppelganger').
card_rarity('shifty doppelganger'/'ODY', 'Rare').
card_artist('shifty doppelganger'/'ODY', 'Greg Staples').
card_number('shifty doppelganger'/'ODY', '101').
card_multiverse_id('shifty doppelganger'/'ODY', '31823').

card_in_set('shower of coals', 'ODY').
card_original_type('shower of coals'/'ODY', 'Sorcery').
card_original_text('shower of coals'/'ODY', 'Shower of Coals deals 2 damage to each of up to three target creatures and/or players.\nThreshold Shower of Coals deals 4 damage to each of those creatures and/or players instead. (You have threshold if seven or more cards are in your graveyard.)').
card_first_print('shower of coals', 'ODY').
card_image_name('shower of coals'/'ODY', 'shower of coals').
card_uid('shower of coals'/'ODY', 'ODY:Shower of Coals:shower of coals').
card_rarity('shower of coals'/'ODY', 'Uncommon').
card_artist('shower of coals'/'ODY', 'Matt Cavotta').
card_number('shower of coals'/'ODY', '221').
card_multiverse_id('shower of coals'/'ODY', '29860').

card_in_set('simplify', 'ODY').
card_original_type('simplify'/'ODY', 'Sorcery').
card_original_text('simplify'/'ODY', 'Each player sacrifices an enchantment.').
card_first_print('simplify', 'ODY').
card_image_name('simplify'/'ODY', 'simplify').
card_uid('simplify'/'ODY', 'ODY:Simplify:simplify').
card_rarity('simplify'/'ODY', 'Common').
card_artist('simplify'/'ODY', 'Greg & Tim Hildebrandt').
card_number('simplify'/'ODY', '269').
card_flavor_text('simplify'/'ODY', 'A blossom\'s petals close inward to shut out the frost.\n—Nantuko teaching').
card_multiverse_id('simplify'/'ODY', '29779').

card_in_set('skeletal scrying', 'ODY').
card_original_type('skeletal scrying'/'ODY', 'Instant').
card_original_text('skeletal scrying'/'ODY', 'As an additional cost to play Skeletal Scrying, remove X cards in your graveyard from the game.\nYou draw X cards and you lose X life.').
card_first_print('skeletal scrying', 'ODY').
card_image_name('skeletal scrying'/'ODY', 'skeletal scrying').
card_uid('skeletal scrying'/'ODY', 'ODY:Skeletal Scrying:skeletal scrying').
card_rarity('skeletal scrying'/'ODY', 'Uncommon').
card_artist('skeletal scrying'/'ODY', 'Bob Petillo').
card_number('skeletal scrying'/'ODY', '161').
card_multiverse_id('skeletal scrying'/'ODY', '29958').

card_in_set('skull fracture', 'ODY').
card_original_type('skull fracture'/'ODY', 'Sorcery').
card_original_text('skull fracture'/'ODY', 'Target player discards a card from his or her hand.\nFlashback {3}{B} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('skull fracture', 'ODY').
card_image_name('skull fracture'/'ODY', 'skull fracture').
card_uid('skull fracture'/'ODY', 'ODY:Skull Fracture:skull fracture').
card_rarity('skull fracture'/'ODY', 'Uncommon').
card_artist('skull fracture'/'ODY', 'Paolo Parente').
card_number('skull fracture'/'ODY', '162').
card_multiverse_id('skull fracture'/'ODY', '29848').

card_in_set('skycloud egg', 'ODY').
card_original_type('skycloud egg'/'ODY', 'Artifact').
card_original_text('skycloud egg'/'ODY', '{2}, {T}, Sacrifice Skycloud Egg: Add {W}{U} to your mana pool. Draw a card.').
card_first_print('skycloud egg', 'ODY').
card_image_name('skycloud egg'/'ODY', 'skycloud egg').
card_uid('skycloud egg'/'ODY', 'ODY:Skycloud Egg:skycloud egg').
card_rarity('skycloud egg'/'ODY', 'Uncommon').
card_artist('skycloud egg'/'ODY', 'David Martin').
card_number('skycloud egg'/'ODY', '309').
card_flavor_text('skycloud egg'/'ODY', '\"Have your dreams of a better society become an obsession?\"\n—Watnik, master glassblower').
card_multiverse_id('skycloud egg'/'ODY', '29892').

card_in_set('skycloud expanse', 'ODY').
card_original_type('skycloud expanse'/'ODY', 'Land').
card_original_text('skycloud expanse'/'ODY', '{1}, {T}: Add {W}{U} to your mana pool.').
card_first_print('skycloud expanse', 'ODY').
card_image_name('skycloud expanse'/'ODY', 'skycloud expanse').
card_uid('skycloud expanse'/'ODY', 'ODY:Skycloud Expanse:skycloud expanse').
card_rarity('skycloud expanse'/'ODY', 'Rare').
card_artist('skycloud expanse'/'ODY', 'Rob Alexander').
card_number('skycloud expanse'/'ODY', '327').
card_flavor_text('skycloud expanse'/'ODY', 'From Otaria, the sun above meets the sea below at a glittering horizon.').
card_multiverse_id('skycloud expanse'/'ODY', '29792').

card_in_set('skyshooter', 'ODY').
card_original_type('skyshooter'/'ODY', 'Creature — Centaur').
card_original_text('skyshooter'/'ODY', 'Skyshooter may block as though it had flying.\n{T}, Sacrifice Skyshooter: Destroy target attacking or blocking creature with flying.').
card_first_print('skyshooter', 'ODY').
card_image_name('skyshooter'/'ODY', 'skyshooter').
card_uid('skyshooter'/'ODY', 'ODY:Skyshooter:skyshooter').
card_rarity('skyshooter'/'ODY', 'Uncommon').
card_artist('skyshooter'/'ODY', 'Jerry Tiritilli').
card_number('skyshooter'/'ODY', '270').
card_multiverse_id('skyshooter'/'ODY', '30563').

card_in_set('soulcatcher', 'ODY').
card_original_type('soulcatcher'/'ODY', 'Creature — Bird Soldier').
card_original_text('soulcatcher'/'ODY', 'Flying\nWhenever a creature with flying is put into a graveyard from play, put a +1/+1 counter on Soulcatcher.').
card_first_print('soulcatcher', 'ODY').
card_image_name('soulcatcher'/'ODY', 'soulcatcher').
card_uid('soulcatcher'/'ODY', 'ODY:Soulcatcher:soulcatcher').
card_rarity('soulcatcher'/'ODY', 'Uncommon').
card_artist('soulcatcher'/'ODY', 'Ron Spencer').
card_number('soulcatcher'/'ODY', '47').
card_flavor_text('soulcatcher'/'ODY', '\"Holy couriers guide aven spirits up to the home of the Ancestor.\"\n—Mystic elder').
card_multiverse_id('soulcatcher'/'ODY', '31865').

card_in_set('spark mage', 'ODY').
card_original_type('spark mage'/'ODY', 'Creature — Dwarf Wizard').
card_original_text('spark mage'/'ODY', 'Whenever Spark Mage deals combat damage to a player, you may have Spark Mage deal 1 damage to target creature that player controls.').
card_first_print('spark mage', 'ODY').
card_image_name('spark mage'/'ODY', 'spark mage').
card_uid('spark mage'/'ODY', 'ODY:Spark Mage:spark mage').
card_rarity('spark mage'/'ODY', 'Uncommon').
card_artist('spark mage'/'ODY', 'Paolo Parente').
card_number('spark mage'/'ODY', '222').
card_flavor_text('spark mage'/'ODY', 'The Pardic Mountains are known for their spicy cuisine.').
card_multiverse_id('spark mage'/'ODY', '29854').

card_in_set('spellbane centaur', 'ODY').
card_original_type('spellbane centaur'/'ODY', 'Creature — Centaur').
card_original_text('spellbane centaur'/'ODY', 'Creatures you control can\'t be the targets of blue spells or abilities from blue sources.').
card_first_print('spellbane centaur', 'ODY').
card_image_name('spellbane centaur'/'ODY', 'spellbane centaur').
card_uid('spellbane centaur'/'ODY', 'ODY:Spellbane Centaur:spellbane centaur').
card_rarity('spellbane centaur'/'ODY', 'Rare').
card_artist('spellbane centaur'/'ODY', 'Rick Farrell').
card_number('spellbane centaur'/'ODY', '271').
card_flavor_text('spellbane centaur'/'ODY', '\"No mage\'s deception can harm those who keep nature balanced.\"').
card_multiverse_id('spellbane centaur'/'ODY', '31800').

card_in_set('sphere of duty', 'ODY').
card_original_type('sphere of duty'/'ODY', 'Enchantment').
card_original_text('sphere of duty'/'ODY', 'If a green source would deal damage to you, prevent 2 of that damage.').
card_first_print('sphere of duty', 'ODY').
card_image_name('sphere of duty'/'ODY', 'sphere of duty').
card_uid('sphere of duty'/'ODY', 'ODY:Sphere of Duty:sphere of duty').
card_rarity('sphere of duty'/'ODY', 'Uncommon').
card_artist('sphere of duty'/'ODY', 'Christopher Moeller').
card_number('sphere of duty'/'ODY', '48').
card_flavor_text('sphere of duty'/'ODY', 'Duty subdues instinct.').
card_multiverse_id('sphere of duty'/'ODY', '29814').

card_in_set('sphere of grace', 'ODY').
card_original_type('sphere of grace'/'ODY', 'Enchantment').
card_original_text('sphere of grace'/'ODY', 'If a black source would deal damage to you, prevent 2 of that damage.').
card_first_print('sphere of grace', 'ODY').
card_image_name('sphere of grace'/'ODY', 'sphere of grace').
card_uid('sphere of grace'/'ODY', 'ODY:Sphere of Grace:sphere of grace').
card_rarity('sphere of grace'/'ODY', 'Uncommon').
card_artist('sphere of grace'/'ODY', 'Christopher Moeller').
card_number('sphere of grace'/'ODY', '49').
card_flavor_text('sphere of grace'/'ODY', 'Grace repels darkness.').
card_multiverse_id('sphere of grace'/'ODY', '29812').

card_in_set('sphere of law', 'ODY').
card_original_type('sphere of law'/'ODY', 'Enchantment').
card_original_text('sphere of law'/'ODY', 'If a red source would deal damage to you, prevent 2 of that damage.').
card_first_print('sphere of law', 'ODY').
card_image_name('sphere of law'/'ODY', 'sphere of law').
card_uid('sphere of law'/'ODY', 'ODY:Sphere of Law:sphere of law').
card_rarity('sphere of law'/'ODY', 'Uncommon').
card_artist('sphere of law'/'ODY', 'Christopher Moeller').
card_number('sphere of law'/'ODY', '50').
card_flavor_text('sphere of law'/'ODY', 'Law smothers anarchy.').
card_multiverse_id('sphere of law'/'ODY', '29813').

card_in_set('sphere of reason', 'ODY').
card_original_type('sphere of reason'/'ODY', 'Enchantment').
card_original_text('sphere of reason'/'ODY', 'If a blue source would deal damage to you, prevent 2 of that damage.').
card_first_print('sphere of reason', 'ODY').
card_image_name('sphere of reason'/'ODY', 'sphere of reason').
card_uid('sphere of reason'/'ODY', 'ODY:Sphere of Reason:sphere of reason').
card_rarity('sphere of reason'/'ODY', 'Uncommon').
card_artist('sphere of reason'/'ODY', 'Christopher Moeller').
card_number('sphere of reason'/'ODY', '51').
card_flavor_text('sphere of reason'/'ODY', 'Reason exposes deception.').
card_multiverse_id('sphere of reason'/'ODY', '29811').

card_in_set('sphere of truth', 'ODY').
card_original_type('sphere of truth'/'ODY', 'Enchantment').
card_original_text('sphere of truth'/'ODY', 'If a white source would deal damage to you, prevent 2 of that damage.').
card_first_print('sphere of truth', 'ODY').
card_image_name('sphere of truth'/'ODY', 'sphere of truth').
card_uid('sphere of truth'/'ODY', 'ODY:Sphere of Truth:sphere of truth').
card_rarity('sphere of truth'/'ODY', 'Uncommon').
card_artist('sphere of truth'/'ODY', 'Christopher Moeller').
card_number('sphere of truth'/'ODY', '52').
card_flavor_text('sphere of truth'/'ODY', 'Truth tempers fanaticism.').
card_multiverse_id('sphere of truth'/'ODY', '29810').

card_in_set('spiritualize', 'ODY').
card_original_type('spiritualize'/'ODY', 'Instant').
card_original_text('spiritualize'/'ODY', 'Until end of turn, whenever target creature deals damage, you gain that much life. \nDraw a card.').
card_first_print('spiritualize', 'ODY').
card_image_name('spiritualize'/'ODY', 'spiritualize').
card_uid('spiritualize'/'ODY', 'ODY:Spiritualize:spiritualize').
card_rarity('spiritualize'/'ODY', 'Uncommon').
card_artist('spiritualize'/'ODY', 'Christopher Moeller').
card_number('spiritualize'/'ODY', '53').
card_flavor_text('spiritualize'/'ODY', '\"There is purity in all things. Even hatred.\"\n—Lieutenant Kirtar').
card_multiverse_id('spiritualize'/'ODY', '29807').

card_in_set('springing tiger', 'ODY').
card_original_type('springing tiger'/'ODY', 'Creature — Cat').
card_original_text('springing tiger'/'ODY', 'Threshold Springing Tiger gets +2/+2. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('springing tiger', 'ODY').
card_image_name('springing tiger'/'ODY', 'springing tiger').
card_uid('springing tiger'/'ODY', 'ODY:Springing Tiger:springing tiger').
card_rarity('springing tiger'/'ODY', 'Common').
card_artist('springing tiger'/'ODY', 'Arnie Swekel').
card_number('springing tiger'/'ODY', '272').
card_flavor_text('springing tiger'/'ODY', 'The ground trembled in a violent purr.').
card_multiverse_id('springing tiger'/'ODY', '29877').

card_in_set('squirrel mob', 'ODY').
card_original_type('squirrel mob'/'ODY', 'Creature — Squirrel').
card_original_text('squirrel mob'/'ODY', 'Squirrel Mob gets +1/+1 for each other Squirrel in play.').
card_first_print('squirrel mob', 'ODY').
card_image_name('squirrel mob'/'ODY', 'squirrel mob').
card_uid('squirrel mob'/'ODY', 'ODY:Squirrel Mob:squirrel mob').
card_rarity('squirrel mob'/'ODY', 'Rare').
card_artist('squirrel mob'/'ODY', 'Carl Critchlow').
card_number('squirrel mob'/'ODY', '273').
card_flavor_text('squirrel mob'/'ODY', 'An army of squirrels is still an army.').
card_multiverse_id('squirrel mob'/'ODY', '29979').

card_in_set('squirrel nest', 'ODY').
card_original_type('squirrel nest'/'ODY', 'Enchant Land').
card_original_text('squirrel nest'/'ODY', 'Enchanted land has \"{T}: Put a 1/1 green Squirrel creature token into play.\"').
card_first_print('squirrel nest', 'ODY').
card_image_name('squirrel nest'/'ODY', 'squirrel nest').
card_uid('squirrel nest'/'ODY', 'ODY:Squirrel Nest:squirrel nest').
card_rarity('squirrel nest'/'ODY', 'Uncommon').
card_artist('squirrel nest'/'ODY', 'Anthony S. Waters').
card_number('squirrel nest'/'ODY', '274').
card_flavor_text('squirrel nest'/'ODY', 'In the Krosan Forest, you\'ve got to keep an eye out for the squirrels.').
card_multiverse_id('squirrel nest'/'ODY', '31836').

card_in_set('stalking bloodsucker', 'ODY').
card_original_type('stalking bloodsucker'/'ODY', 'Creature — Vampire').
card_original_text('stalking bloodsucker'/'ODY', 'Flying\n{1}{B}, Discard a card from your hand: Stalking Bloodsucker gets +2/+2 until end of turn.').
card_first_print('stalking bloodsucker', 'ODY').
card_image_name('stalking bloodsucker'/'ODY', 'stalking bloodsucker').
card_uid('stalking bloodsucker'/'ODY', 'ODY:Stalking Bloodsucker:stalking bloodsucker').
card_rarity('stalking bloodsucker'/'ODY', 'Rare').
card_artist('stalking bloodsucker'/'ODY', 'Greg Staples').
card_number('stalking bloodsucker'/'ODY', '163').
card_flavor_text('stalking bloodsucker'/'ODY', 'The pits feed both its thirst for combat and its thirst for blood.').
card_multiverse_id('stalking bloodsucker'/'ODY', '29839').

card_in_set('standstill', 'ODY').
card_original_type('standstill'/'ODY', 'Enchantment').
card_original_text('standstill'/'ODY', 'When a player plays a spell, sacrifice Standstill. If you do, each of that player\'s opponents draws three cards.').
card_image_name('standstill'/'ODY', 'standstill').
card_uid('standstill'/'ODY', 'ODY:Standstill:standstill').
card_rarity('standstill'/'ODY', 'Uncommon').
card_artist('standstill'/'ODY', 'Heather Hudson').
card_number('standstill'/'ODY', '102').
card_flavor_text('standstill'/'ODY', '\"Take your time.\"').
card_multiverse_id('standstill'/'ODY', '29936').

card_in_set('steam vines', 'ODY').
card_original_type('steam vines'/'ODY', 'Enchant Land').
card_original_text('steam vines'/'ODY', 'When enchanted land becomes tapped, destroy it and Steam Vines deals 1 damage to that land\'s controller. That player moves Steam Vines to a land of his or her choice.').
card_first_print('steam vines', 'ODY').
card_image_name('steam vines'/'ODY', 'steam vines').
card_uid('steam vines'/'ODY', 'ODY:Steam Vines:steam vines').
card_rarity('steam vines'/'ODY', 'Uncommon').
card_artist('steam vines'/'ODY', 'Anthony S. Waters').
card_number('steam vines'/'ODY', '223').
card_multiverse_id('steam vines'/'ODY', '31838').

card_in_set('steamclaw', 'ODY').
card_original_type('steamclaw'/'ODY', 'Artifact').
card_original_text('steamclaw'/'ODY', '{3}, {T}: Remove target card in a graveyard from the game.  \n{1}, Sacrifice Steamclaw: Remove target card in a graveyard from the game.').
card_first_print('steamclaw', 'ODY').
card_image_name('steamclaw'/'ODY', 'steamclaw').
card_uid('steamclaw'/'ODY', 'ODY:Steamclaw:steamclaw').
card_rarity('steamclaw'/'ODY', 'Uncommon').
card_artist('steamclaw'/'ODY', 'Jim Nelson').
card_number('steamclaw'/'ODY', '310').
card_flavor_text('steamclaw'/'ODY', '\"There\'s dead, and then there\'s dead dead.\"\n—Steamclaw driver').
card_multiverse_id('steamclaw'/'ODY', '29897').

card_in_set('still life', 'ODY').
card_original_type('still life'/'ODY', 'Enchantment').
card_original_text('still life'/'ODY', '{G}{G}: Still Life becomes a 4/3 Centaur creature until end of turn. It\'s still an enchantment.').
card_first_print('still life', 'ODY').
card_image_name('still life'/'ODY', 'still life').
card_uid('still life'/'ODY', 'ODY:Still Life:still life').
card_rarity('still life'/'ODY', 'Uncommon').
card_artist('still life'/'ODY', 'Matt Cavotta').
card_number('still life'/'ODY', '275').
card_flavor_text('still life'/'ODY', 'The centaurs\' simple magic can undo a basilisk\'s gaze, but only for a short time.').
card_multiverse_id('still life'/'ODY', '29879').

card_in_set('stone-tongue basilisk', 'ODY').
card_original_type('stone-tongue basilisk'/'ODY', 'Creature — Basilisk').
card_original_text('stone-tongue basilisk'/'ODY', 'Whenever Stone-Tongue Basilisk deals combat damage to a creature, destroy that creature at end of combat.\nThreshold All creatures able to block Stone-Tongue Basilisk do so. (You have threshold as long as seven or more cards are in your graveyard.)').
card_image_name('stone-tongue basilisk'/'ODY', 'stone-tongue basilisk').
card_uid('stone-tongue basilisk'/'ODY', 'ODY:Stone-Tongue Basilisk:stone-tongue basilisk').
card_rarity('stone-tongue basilisk'/'ODY', 'Rare').
card_artist('stone-tongue basilisk'/'ODY', 'Wayne England').
card_number('stone-tongue basilisk'/'ODY', '276').
card_multiverse_id('stone-tongue basilisk'/'ODY', '29983').

card_in_set('sungrass egg', 'ODY').
card_original_type('sungrass egg'/'ODY', 'Artifact').
card_original_text('sungrass egg'/'ODY', '{2}, {T}, Sacrifice Sungrass Egg: Add {G}{W} to your mana pool. Draw a card.').
card_first_print('sungrass egg', 'ODY').
card_image_name('sungrass egg'/'ODY', 'sungrass egg').
card_uid('sungrass egg'/'ODY', 'ODY:Sungrass Egg:sungrass egg').
card_rarity('sungrass egg'/'ODY', 'Uncommon').
card_artist('sungrass egg'/'ODY', 'David Martin').
card_number('sungrass egg'/'ODY', '311').
card_flavor_text('sungrass egg'/'ODY', '\"Has your peaceful nature led you on a crusade?\"\n—Watnik, master glassblower').
card_multiverse_id('sungrass egg'/'ODY', '30750').

card_in_set('sungrass prairie', 'ODY').
card_original_type('sungrass prairie'/'ODY', 'Land').
card_original_text('sungrass prairie'/'ODY', '{1}, {T}: Add {G}{W} to your mana pool.').
card_first_print('sungrass prairie', 'ODY').
card_image_name('sungrass prairie'/'ODY', 'sungrass prairie').
card_uid('sungrass prairie'/'ODY', 'ODY:Sungrass Prairie:sungrass prairie').
card_rarity('sungrass prairie'/'ODY', 'Rare').
card_artist('sungrass prairie'/'ODY', 'Ron Spencer').
card_number('sungrass prairie'/'ODY', '328').
card_flavor_text('sungrass prairie'/'ODY', 'On Otaria, peace and harmony are rare. Places that provide both are cherished.').
card_multiverse_id('sungrass prairie'/'ODY', '29796').

card_in_set('swamp', 'ODY').
card_original_type('swamp'/'ODY', 'Land').
card_original_text('swamp'/'ODY', 'B').
card_image_name('swamp'/'ODY', 'swamp1').
card_uid('swamp'/'ODY', 'ODY:Swamp:swamp1').
card_rarity('swamp'/'ODY', 'Basic Land').
card_artist('swamp'/'ODY', 'Jerry Tiritilli').
card_number('swamp'/'ODY', '339').
card_multiverse_id('swamp'/'ODY', '31617').

card_in_set('swamp', 'ODY').
card_original_type('swamp'/'ODY', 'Land').
card_original_text('swamp'/'ODY', 'B').
card_image_name('swamp'/'ODY', 'swamp2').
card_uid('swamp'/'ODY', 'ODY:Swamp:swamp2').
card_rarity('swamp'/'ODY', 'Basic Land').
card_artist('swamp'/'ODY', 'Arnie Swekel').
card_number('swamp'/'ODY', '340').
card_multiverse_id('swamp'/'ODY', '31618').

card_in_set('swamp', 'ODY').
card_original_type('swamp'/'ODY', 'Land').
card_original_text('swamp'/'ODY', 'B').
card_image_name('swamp'/'ODY', 'swamp3').
card_uid('swamp'/'ODY', 'ODY:Swamp:swamp3').
card_rarity('swamp'/'ODY', 'Basic Land').
card_artist('swamp'/'ODY', 'Rob Alexander').
card_number('swamp'/'ODY', '341').
card_multiverse_id('swamp'/'ODY', '31619').

card_in_set('swamp', 'ODY').
card_original_type('swamp'/'ODY', 'Land').
card_original_text('swamp'/'ODY', 'B').
card_image_name('swamp'/'ODY', 'swamp4').
card_uid('swamp'/'ODY', 'ODY:Swamp:swamp4').
card_rarity('swamp'/'ODY', 'Basic Land').
card_artist('swamp'/'ODY', 'Alan Pollack').
card_number('swamp'/'ODY', '342').
card_multiverse_id('swamp'/'ODY', '31620').

card_in_set('sylvan might', 'ODY').
card_original_type('sylvan might'/'ODY', 'Instant').
card_original_text('sylvan might'/'ODY', 'Target creature gets +2/+2 and gains trample until end of turn.\nFlashback {2}{G}{G} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('sylvan might', 'ODY').
card_image_name('sylvan might'/'ODY', 'sylvan might').
card_uid('sylvan might'/'ODY', 'ODY:Sylvan Might:sylvan might').
card_rarity('sylvan might'/'ODY', 'Uncommon').
card_artist('sylvan might'/'ODY', 'Arnie Swekel').
card_number('sylvan might'/'ODY', '277').
card_multiverse_id('sylvan might'/'ODY', '29790').

card_in_set('syncopate', 'ODY').
card_original_type('syncopate'/'ODY', 'Instant').
card_original_text('syncopate'/'ODY', 'Counter target spell unless its controller pays {X}. If that spell is countered this way, remove it from the game instead of putting it into its owner\'s graveyard.').
card_first_print('syncopate', 'ODY').
card_image_name('syncopate'/'ODY', 'syncopate').
card_uid('syncopate'/'ODY', 'ODY:Syncopate:syncopate').
card_rarity('syncopate'/'ODY', 'Common').
card_artist('syncopate'/'ODY', 'Pete Venters').
card_number('syncopate'/'ODY', '103').
card_flavor_text('syncopate'/'ODY', '\"Memories can\'t simply be washed away. The mind must be scrubbed clean.\"\n—Ambassador Laquatus').
card_multiverse_id('syncopate'/'ODY', '29832').

card_in_set('tainted pact', 'ODY').
card_original_type('tainted pact'/'ODY', 'Instant').
card_original_text('tainted pact'/'ODY', 'Remove the top card of your library from the game. You may put that card into your hand unless it has the same name as another card removed this way. Repeat this process until you put a card into your hand or you remove two cards with the same name, whichever comes first.').
card_first_print('tainted pact', 'ODY').
card_image_name('tainted pact'/'ODY', 'tainted pact').
card_uid('tainted pact'/'ODY', 'ODY:Tainted Pact:tainted pact').
card_rarity('tainted pact'/'ODY', 'Rare').
card_artist('tainted pact'/'ODY', 'Adam Rex').
card_number('tainted pact'/'ODY', '164').
card_multiverse_id('tainted pact'/'ODY', '29953').

card_in_set('tarnished citadel', 'ODY').
card_original_type('tarnished citadel'/'ODY', 'Land').
card_original_text('tarnished citadel'/'ODY', '{T}: Add one colorless mana to your mana pool.\n{T}: Add one mana of any color to your mana pool. Tarnished Citadel deals 3 damage to you.').
card_first_print('tarnished citadel', 'ODY').
card_image_name('tarnished citadel'/'ODY', 'tarnished citadel').
card_uid('tarnished citadel'/'ODY', 'ODY:Tarnished Citadel:tarnished citadel').
card_rarity('tarnished citadel'/'ODY', 'Rare').
card_artist('tarnished citadel'/'ODY', 'David Martin').
card_number('tarnished citadel'/'ODY', '329').
card_multiverse_id('tarnished citadel'/'ODY', '30015').

card_in_set('tattoo ward', 'ODY').
card_original_type('tattoo ward'/'ODY', 'Enchant Creature').
card_original_text('tattoo ward'/'ODY', 'Enchanted creature gets +1/+1 and has protection from enchantments. This effect doesn\'t remove Tattoo Ward.\nSacrifice Tattoo Ward: Destroy target enchantment.').
card_first_print('tattoo ward', 'ODY').
card_image_name('tattoo ward'/'ODY', 'tattoo ward').
card_uid('tattoo ward'/'ODY', 'ODY:Tattoo Ward:tattoo ward').
card_rarity('tattoo ward'/'ODY', 'Uncommon').
card_artist('tattoo ward'/'ODY', 'Ben Thompson').
card_number('tattoo ward'/'ODY', '54').
card_multiverse_id('tattoo ward'/'ODY', '31873').

card_in_set('terravore', 'ODY').
card_original_type('terravore'/'ODY', 'Creature — Lhurgoyf').
card_original_text('terravore'/'ODY', 'Trample\nTerravore\'s power and toughness are each equal to the number of land cards in all graveyards.').
card_first_print('terravore', 'ODY').
card_image_name('terravore'/'ODY', 'terravore').
card_uid('terravore'/'ODY', 'ODY:Terravore:terravore').
card_rarity('terravore'/'ODY', 'Rare').
card_artist('terravore'/'ODY', 'Jim Nelson').
card_number('terravore'/'ODY', '278').
card_multiverse_id('terravore'/'ODY', '29982').

card_in_set('testament of faith', 'ODY').
card_original_type('testament of faith'/'ODY', 'Enchantment').
card_original_text('testament of faith'/'ODY', '{X}: Testament of Faith becomes an X/X Wall creature until end of turn. It\'s still an enchantment. (Walls can\'t attack.)').
card_first_print('testament of faith', 'ODY').
card_image_name('testament of faith'/'ODY', 'testament of faith').
card_uid('testament of faith'/'ODY', 'ODY:Testament of Faith:testament of faith').
card_rarity('testament of faith'/'ODY', 'Uncommon').
card_artist('testament of faith'/'ODY', 'Roger Raupp').
card_number('testament of faith'/'ODY', '55').
card_flavor_text('testament of faith'/'ODY', '\"Belief in the Ancestor is admirable, but our enemies often demand something more . . . tangible.\"\n—Pianna, nomad captain').
card_multiverse_id('testament of faith'/'ODY', '30765').

card_in_set('thaumatog', 'ODY').
card_original_type('thaumatog'/'ODY', 'Creature — Atog').
card_original_text('thaumatog'/'ODY', 'Sacrifice a land: Thaumatog gets +1/+1 until end of turn.\nSacrifice an enchantment: Thaumatog gets +1/+1 until end of turn.').
card_first_print('thaumatog', 'ODY').
card_image_name('thaumatog'/'ODY', 'thaumatog').
card_uid('thaumatog'/'ODY', 'ODY:Thaumatog:thaumatog').
card_rarity('thaumatog'/'ODY', 'Uncommon').
card_artist('thaumatog'/'ODY', 'Monte Michael Moore').
card_number('thaumatog'/'ODY', '295').
card_multiverse_id('thaumatog'/'ODY', '31835').

card_in_set('thermal blast', 'ODY').
card_original_type('thermal blast'/'ODY', 'Instant').
card_original_text('thermal blast'/'ODY', 'Thermal Blast deals 3 damage to target creature.\nThreshold Thermal Blast deals 5 damage to that creature instead. (You have threshold if seven or more cards are in your graveyard.)').
card_first_print('thermal blast', 'ODY').
card_image_name('thermal blast'/'ODY', 'thermal blast').
card_uid('thermal blast'/'ODY', 'ODY:Thermal Blast:thermal blast').
card_rarity('thermal blast'/'ODY', 'Common').
card_artist('thermal blast'/'ODY', 'Franz Vohwinkel').
card_number('thermal blast'/'ODY', '224').
card_multiverse_id('thermal blast'/'ODY', '29759').

card_in_set('think tank', 'ODY').
card_original_type('think tank'/'ODY', 'Enchantment').
card_original_text('think tank'/'ODY', 'At the beginning of your upkeep, look at the top card of your library. You may put that card into your graveyard.').
card_first_print('think tank', 'ODY').
card_image_name('think tank'/'ODY', 'think tank').
card_uid('think tank'/'ODY', 'ODY:Think Tank:think tank').
card_rarity('think tank'/'ODY', 'Uncommon').
card_artist('think tank'/'ODY', 'Jim Nelson').
card_number('think tank'/'ODY', '104').
card_flavor_text('think tank'/'ODY', 'Cephalids seek knowledge not to better themselves, but to best others.').
card_multiverse_id('think tank'/'ODY', '29830').

card_in_set('thought devourer', 'ODY').
card_original_type('thought devourer'/'ODY', 'Creature — Beast').
card_original_text('thought devourer'/'ODY', 'Flying\nYour maximum hand size is reduced by four.').
card_first_print('thought devourer', 'ODY').
card_image_name('thought devourer'/'ODY', 'thought devourer').
card_uid('thought devourer'/'ODY', 'ODY:Thought Devourer:thought devourer').
card_rarity('thought devourer'/'ODY', 'Rare').
card_artist('thought devourer'/'ODY', 'Jim Nelson').
card_number('thought devourer'/'ODY', '105').
card_flavor_text('thought devourer'/'ODY', 'Thinking is hard.').
card_multiverse_id('thought devourer'/'ODY', '31817').

card_in_set('thought eater', 'ODY').
card_original_type('thought eater'/'ODY', 'Creature — Beast').
card_original_text('thought eater'/'ODY', 'Flying\nYour maximum hand size is reduced by three.').
card_first_print('thought eater', 'ODY').
card_image_name('thought eater'/'ODY', 'thought eater').
card_uid('thought eater'/'ODY', 'ODY:Thought Eater:thought eater').
card_rarity('thought eater'/'ODY', 'Uncommon').
card_artist('thought eater'/'ODY', 'Luca Zontini').
card_number('thought eater'/'ODY', '106').
card_flavor_text('thought eater'/'ODY', 'The first memory it sucks out of its host is how to get rid of it.').
card_multiverse_id('thought eater'/'ODY', '31803').

card_in_set('thought nibbler', 'ODY').
card_original_type('thought nibbler'/'ODY', 'Creature — Beast').
card_original_text('thought nibbler'/'ODY', 'Flying\nYour maximum hand size is reduced by two.').
card_first_print('thought nibbler', 'ODY').
card_image_name('thought nibbler'/'ODY', 'thought nibbler').
card_uid('thought nibbler'/'ODY', 'ODY:Thought Nibbler:thought nibbler').
card_rarity('thought nibbler'/'ODY', 'Common').
card_artist('thought nibbler'/'ODY', 'Arnie Swekel').
card_number('thought nibbler'/'ODY', '107').
card_flavor_text('thought nibbler'/'ODY', 'Balshan wizards have long used them to gnaw away the memories they wish to forget.').
card_multiverse_id('thought nibbler'/'ODY', '31774').

card_in_set('timberland ruins', 'ODY').
card_original_type('timberland ruins'/'ODY', 'Land').
card_original_text('timberland ruins'/'ODY', 'Timberland Ruins comes into play tapped.\n{T}: Add {G} to your mana pool.\n{T}, Sacrifice Timberland Ruins: Add one mana of any color to your mana pool.').
card_first_print('timberland ruins', 'ODY').
card_image_name('timberland ruins'/'ODY', 'timberland ruins').
card_uid('timberland ruins'/'ODY', 'ODY:Timberland Ruins:timberland ruins').
card_rarity('timberland ruins'/'ODY', 'Common').
card_artist('timberland ruins'/'ODY', 'Alan Pollack').
card_number('timberland ruins'/'ODY', '330').
card_multiverse_id('timberland ruins'/'ODY', '31765').

card_in_set('time stretch', 'ODY').
card_original_type('time stretch'/'ODY', 'Sorcery').
card_original_text('time stretch'/'ODY', 'Target player takes two extra turns after this one.').
card_first_print('time stretch', 'ODY').
card_image_name('time stretch'/'ODY', 'time stretch').
card_uid('time stretch'/'ODY', 'ODY:Time Stretch:time stretch').
card_rarity('time stretch'/'ODY', 'Rare').
card_artist('time stretch'/'ODY', 'Paolo Parente').
card_number('time stretch'/'ODY', '108').
card_flavor_text('time stretch'/'ODY', 'There\'s lots of time like the present.').
card_multiverse_id('time stretch'/'ODY', '29933').

card_in_set('tireless tribe', 'ODY').
card_original_type('tireless tribe'/'ODY', 'Creature — Nomad').
card_original_text('tireless tribe'/'ODY', 'Discard a card from your hand: Tireless Tribe gets +0/+4 until end of turn.').
card_first_print('tireless tribe', 'ODY').
card_image_name('tireless tribe'/'ODY', 'tireless tribe').
card_uid('tireless tribe'/'ODY', 'ODY:Tireless Tribe:tireless tribe').
card_rarity('tireless tribe'/'ODY', 'Common').
card_artist('tireless tribe'/'ODY', 'Carl Critchlow').
card_number('tireless tribe'/'ODY', '56').
card_flavor_text('tireless tribe'/'ODY', '\"We\'ve survived starvation, dwarf attacks, and heatstroke. Why should a little sandstorm bother us?\"').
card_multiverse_id('tireless tribe'/'ODY', '29695').

card_in_set('tombfire', 'ODY').
card_original_type('tombfire'/'ODY', 'Sorcery').
card_original_text('tombfire'/'ODY', 'Target player removes all cards with flashback in his or her graveyard from the game.').
card_first_print('tombfire', 'ODY').
card_image_name('tombfire'/'ODY', 'tombfire').
card_uid('tombfire'/'ODY', 'ODY:Tombfire:tombfire').
card_rarity('tombfire'/'ODY', 'Rare').
card_artist('tombfire'/'ODY', 'Arnie Swekel').
card_number('tombfire'/'ODY', '165').
card_flavor_text('tombfire'/'ODY', 'The fondest memories burn brightest. As they\'re lost forever, the flames grow hotter still.').
card_multiverse_id('tombfire'/'ODY', '30690').

card_in_set('touch of invisibility', 'ODY').
card_original_type('touch of invisibility'/'ODY', 'Sorcery').
card_original_text('touch of invisibility'/'ODY', 'Target creature is unblockable this turn.\nDraw a card.').
card_first_print('touch of invisibility', 'ODY').
card_image_name('touch of invisibility'/'ODY', 'touch of invisibility').
card_uid('touch of invisibility'/'ODY', 'ODY:Touch of Invisibility:touch of invisibility').
card_rarity('touch of invisibility'/'ODY', 'Common').
card_artist('touch of invisibility'/'ODY', 'Eric Peterson').
card_number('touch of invisibility'/'ODY', '109').
card_flavor_text('touch of invisibility'/'ODY', 'Cephalids specialize in things unseen, be they politics, betrayal, or themselves.').
card_multiverse_id('touch of invisibility'/'ODY', '30691').

card_in_set('traumatize', 'ODY').
card_original_type('traumatize'/'ODY', 'Sorcery').
card_original_text('traumatize'/'ODY', 'Target player puts the top half of his or her library, rounded down, into his or her graveyard.').
card_first_print('traumatize', 'ODY').
card_image_name('traumatize'/'ODY', 'traumatize').
card_uid('traumatize'/'ODY', 'ODY:Traumatize:traumatize').
card_rarity('traumatize'/'ODY', 'Rare').
card_artist('traumatize'/'ODY', 'Greg Staples').
card_number('traumatize'/'ODY', '110').
card_flavor_text('traumatize'/'ODY', 'Dreads had half a mind to leave.').
card_multiverse_id('traumatize'/'ODY', '30556').

card_in_set('traveling plague', 'ODY').
card_original_type('traveling plague'/'ODY', 'Enchant Creature').
card_original_text('traveling plague'/'ODY', 'At the beginning of each player\'s upkeep, put a plague counter on Traveling Plague.\nEnchanted creature gets -1/-1 for each plague counter on Traveling Plague.\nWhen enchanted creature leaves play, that creature\'s controller returns Traveling Plague from its owner\'s graveyard to play.').
card_first_print('traveling plague', 'ODY').
card_image_name('traveling plague'/'ODY', 'traveling plague').
card_uid('traveling plague'/'ODY', 'ODY:Traveling Plague:traveling plague').
card_rarity('traveling plague'/'ODY', 'Rare').
card_artist('traveling plague'/'ODY', 'Dave Dorman').
card_number('traveling plague'/'ODY', '166').
card_multiverse_id('traveling plague'/'ODY', '31853').

card_in_set('treetop sentinel', 'ODY').
card_original_type('treetop sentinel'/'ODY', 'Creature — Bird Soldier').
card_original_text('treetop sentinel'/'ODY', 'Flying, protection from green').
card_first_print('treetop sentinel', 'ODY').
card_image_name('treetop sentinel'/'ODY', 'treetop sentinel').
card_uid('treetop sentinel'/'ODY', 'ODY:Treetop Sentinel:treetop sentinel').
card_rarity('treetop sentinel'/'ODY', 'Uncommon').
card_artist('treetop sentinel'/'ODY', 'Carl Critchlow').
card_number('treetop sentinel'/'ODY', '111').
card_flavor_text('treetop sentinel'/'ODY', 'Crian\'s patrols afford him plenty of opportunity to take potshots at the \"dirtwalkers\" far below.').
card_multiverse_id('treetop sentinel'/'ODY', '31736').

card_in_set('tremble', 'ODY').
card_original_type('tremble'/'ODY', 'Sorcery').
card_original_text('tremble'/'ODY', 'Each player sacrifices a land.').
card_first_print('tremble', 'ODY').
card_image_name('tremble'/'ODY', 'tremble').
card_uid('tremble'/'ODY', 'ODY:Tremble:tremble').
card_rarity('tremble'/'ODY', 'Common').
card_artist('tremble'/'ODY', 'Ciruelo').
card_number('tremble'/'ODY', '225').
card_flavor_text('tremble'/'ODY', 'Otaria\'s frequent tremors are the last stages of Dominaria\'s recovery from the Phyrexian war.').
card_multiverse_id('tremble'/'ODY', '31795').

card_in_set('twigwalker', 'ODY').
card_original_type('twigwalker'/'ODY', 'Creature — Insect').
card_original_text('twigwalker'/'ODY', '{1}{G}, Sacrifice Twigwalker: Two target creatures each get +2/+2 until end of turn.').
card_first_print('twigwalker', 'ODY').
card_image_name('twigwalker'/'ODY', 'twigwalker').
card_uid('twigwalker'/'ODY', 'ODY:Twigwalker:twigwalker').
card_rarity('twigwalker'/'ODY', 'Uncommon').
card_artist('twigwalker'/'ODY', 'Carl Critchlow').
card_number('twigwalker'/'ODY', '279').
card_flavor_text('twigwalker'/'ODY', '\"Break one in half and you\'ll get a decent—if gooey—pair of weapons.\"\n—Centaur warrior').
card_multiverse_id('twigwalker'/'ODY', '30548').

card_in_set('unifying theory', 'ODY').
card_original_type('unifying theory'/'ODY', 'Enchantment').
card_original_text('unifying theory'/'ODY', 'Whenever a player plays a spell, that player may pay {2}. If the player does, he or she draws a card.').
card_first_print('unifying theory', 'ODY').
card_image_name('unifying theory'/'ODY', 'unifying theory').
card_uid('unifying theory'/'ODY', 'ODY:Unifying Theory:unifying theory').
card_rarity('unifying theory'/'ODY', 'Rare').
card_artist('unifying theory'/'ODY', 'Ron Spears').
card_number('unifying theory'/'ODY', '112').
card_flavor_text('unifying theory'/'ODY', 'Scribbles of a lunatic and formulas of a genius are often indistinguishable.').
card_multiverse_id('unifying theory'/'ODY', '29934').

card_in_set('upheaval', 'ODY').
card_original_type('upheaval'/'ODY', 'Sorcery').
card_original_text('upheaval'/'ODY', 'Return all permanents to their owners\' hands.').
card_first_print('upheaval', 'ODY').
card_image_name('upheaval'/'ODY', 'upheaval').
card_uid('upheaval'/'ODY', 'ODY:Upheaval:upheaval').
card_rarity('upheaval'/'ODY', 'Rare').
card_artist('upheaval'/'ODY', 'Kev Walker').
card_number('upheaval'/'ODY', '113').
card_flavor_text('upheaval'/'ODY', 'The calm comes after the storm.').
card_multiverse_id('upheaval'/'ODY', '31852').

card_in_set('vampiric dragon', 'ODY').
card_original_type('vampiric dragon'/'ODY', 'Creature — Vampire Dragon').
card_original_text('vampiric dragon'/'ODY', 'Flying\nWhenever a creature dealt damage by Vampiric Dragon this turn is put into a graveyard, put a +1/+1 counter on Vampiric Dragon.\n{1}{R}: Vampiric Dragon deals 1 damage to target creature.').
card_first_print('vampiric dragon', 'ODY').
card_image_name('vampiric dragon'/'ODY', 'vampiric dragon').
card_uid('vampiric dragon'/'ODY', 'ODY:Vampiric Dragon:vampiric dragon').
card_rarity('vampiric dragon'/'ODY', 'Rare').
card_artist('vampiric dragon'/'ODY', 'Gary Ruddell').
card_number('vampiric dragon'/'ODY', '296').
card_multiverse_id('vampiric dragon'/'ODY', '29969').

card_in_set('verdant succession', 'ODY').
card_original_type('verdant succession'/'ODY', 'Enchantment').
card_original_text('verdant succession'/'ODY', 'Whenever a green nontoken creature is put into a graveyard from play, that creature\'s controller may search his or her library for a card with the same name as that creature and put it into play. If that player does, he or she then shuffles his or her library.').
card_first_print('verdant succession', 'ODY').
card_image_name('verdant succession'/'ODY', 'verdant succession').
card_uid('verdant succession'/'ODY', 'ODY:Verdant Succession:verdant succession').
card_rarity('verdant succession'/'ODY', 'Rare').
card_artist('verdant succession'/'ODY', 'Edward P. Beard, Jr.').
card_number('verdant succession'/'ODY', '280').
card_multiverse_id('verdant succession'/'ODY', '29989').

card_in_set('vivify', 'ODY').
card_original_type('vivify'/'ODY', 'Instant').
card_original_text('vivify'/'ODY', 'Target land becomes a 3/3 creature until end of turn. It\'s still a land.\nDraw a card.').
card_first_print('vivify', 'ODY').
card_image_name('vivify'/'ODY', 'vivify').
card_uid('vivify'/'ODY', 'ODY:Vivify:vivify').
card_rarity('vivify'/'ODY', 'Uncommon').
card_artist('vivify'/'ODY', 'Greg Staples').
card_number('vivify'/'ODY', '281').
card_flavor_text('vivify'/'ODY', 'Near the Krosan border, farmers keep watch over their fields lest the sod get up and wander away.').
card_multiverse_id('vivify'/'ODY', '30546').

card_in_set('volcanic spray', 'ODY').
card_original_type('volcanic spray'/'ODY', 'Sorcery').
card_original_text('volcanic spray'/'ODY', 'Volcanic Spray deals 1 damage to each creature without flying and each player.\nFlashback {1}{R} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('volcanic spray', 'ODY').
card_image_name('volcanic spray'/'ODY', 'volcanic spray').
card_uid('volcanic spray'/'ODY', 'ODY:Volcanic Spray:volcanic spray').
card_rarity('volcanic spray'/'ODY', 'Uncommon').
card_artist('volcanic spray'/'ODY', 'Matt Cavotta').
card_number('volcanic spray'/'ODY', '226').
card_multiverse_id('volcanic spray'/'ODY', '29768').

card_in_set('volley of boulders', 'ODY').
card_original_type('volley of boulders'/'ODY', 'Sorcery').
card_original_text('volley of boulders'/'ODY', 'Volley of Boulders deals 6 damage divided as you choose among any number of target creatures and/or players.\nFlashback {R}{R}{R}{R}{R}{R} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('volley of boulders', 'ODY').
card_image_name('volley of boulders'/'ODY', 'volley of boulders').
card_uid('volley of boulders'/'ODY', 'ODY:Volley of Boulders:volley of boulders').
card_rarity('volley of boulders'/'ODY', 'Rare').
card_artist('volley of boulders'/'ODY', 'Tony Szczudlo').
card_number('volley of boulders'/'ODY', '227').
card_multiverse_id('volley of boulders'/'ODY', '29970').

card_in_set('wayward angel', 'ODY').
card_original_type('wayward angel'/'ODY', 'Creature — Angel Horror').
card_original_text('wayward angel'/'ODY', 'Flying\nAttacking doesn\'t cause Wayward Angel to tap.\nThreshold Wayward Angel gets +3/+3, is black, has trample, and has \"At the beginning of your upkeep, sacrifice a creature.\" (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('wayward angel', 'ODY').
card_image_name('wayward angel'/'ODY', 'wayward angel').
card_uid('wayward angel'/'ODY', 'ODY:Wayward Angel:wayward angel').
card_rarity('wayward angel'/'ODY', 'Rare').
card_artist('wayward angel'/'ODY', 'Mark Tedin').
card_number('wayward angel'/'ODY', '57').
card_multiverse_id('wayward angel'/'ODY', '29910').

card_in_set('werebear', 'ODY').
card_original_type('werebear'/'ODY', 'Creature — Druid Bear').
card_original_text('werebear'/'ODY', '{T}: Add {G} to your mana pool.\nThreshold Werebear gets +3/+3. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('werebear', 'ODY').
card_image_name('werebear'/'ODY', 'werebear').
card_uid('werebear'/'ODY', 'ODY:Werebear:werebear').
card_rarity('werebear'/'ODY', 'Common').
card_artist('werebear'/'ODY', 'Carl Critchlow').
card_number('werebear'/'ODY', '282').
card_flavor_text('werebear'/'ODY', 'He exercises his right to bear arms.').
card_multiverse_id('werebear'/'ODY', '29785').

card_in_set('whipkeeper', 'ODY').
card_original_type('whipkeeper'/'ODY', 'Creature — Dwarf').
card_original_text('whipkeeper'/'ODY', '{T}: Whipkeeper deals damage to target creature equal to the damage already dealt to it this turn.').
card_first_print('whipkeeper', 'ODY').
card_image_name('whipkeeper'/'ODY', 'whipkeeper').
card_uid('whipkeeper'/'ODY', 'ODY:Whipkeeper:whipkeeper').
card_rarity('whipkeeper'/'ODY', 'Uncommon').
card_artist('whipkeeper'/'ODY', 'Ron Spencer').
card_number('whipkeeper'/'ODY', '228').
card_flavor_text('whipkeeper'/'ODY', '\"If you don\'t hit your adversaries while they\'re down, they might get up again.\"').
card_multiverse_id('whipkeeper'/'ODY', '29961').

card_in_set('whispering shade', 'ODY').
card_original_type('whispering shade'/'ODY', 'Creature — Shade').
card_original_text('whispering shade'/'ODY', 'Swampwalk\n{B}: Whispering Shade gets +1/+1 until end of turn.').
card_first_print('whispering shade', 'ODY').
card_image_name('whispering shade'/'ODY', 'whispering shade').
card_uid('whispering shade'/'ODY', 'ODY:Whispering Shade:whispering shade').
card_rarity('whispering shade'/'ODY', 'Common').
card_artist('whispering shade'/'ODY', 'Daren Bader').
card_number('whispering shade'/'ODY', '167').
card_flavor_text('whispering shade'/'ODY', 'It glides through the shadows until it reaches yours.').
card_multiverse_id('whispering shade'/'ODY', '29734').

card_in_set('wild mongrel', 'ODY').
card_original_type('wild mongrel'/'ODY', 'Creature — Hound').
card_original_text('wild mongrel'/'ODY', 'Discard a card from your hand: Wild Mongrel gets +1/+1 and becomes the color of your choice until end of turn.').
card_image_name('wild mongrel'/'ODY', 'wild mongrel').
card_uid('wild mongrel'/'ODY', 'ODY:Wild Mongrel:wild mongrel').
card_rarity('wild mongrel'/'ODY', 'Common').
card_artist('wild mongrel'/'ODY', 'Anthony S. Waters').
card_number('wild mongrel'/'ODY', '283').
card_flavor_text('wild mongrel'/'ODY', 'It teaches you to play dead.').
card_multiverse_id('wild mongrel'/'ODY', '29777').

card_in_set('woodland druid', 'ODY').
card_original_type('woodland druid'/'ODY', 'Creature — Druid').
card_original_text('woodland druid'/'ODY', '').
card_first_print('woodland druid', 'ODY').
card_image_name('woodland druid'/'ODY', 'woodland druid').
card_uid('woodland druid'/'ODY', 'ODY:Woodland Druid:woodland druid').
card_rarity('woodland druid'/'ODY', 'Common').
card_artist('woodland druid'/'ODY', 'Rick Farrell').
card_number('woodland druid'/'ODY', '284').
card_flavor_text('woodland druid'/'ODY', 'What is a tree?\nSlow hard thought.\nWhat is a flower?\nSwift soft flesh.\nWhat is a druid?\nBoth.').
card_multiverse_id('woodland druid'/'ODY', '29772').

card_in_set('words of wisdom', 'ODY').
card_original_type('words of wisdom'/'ODY', 'Instant').
card_original_text('words of wisdom'/'ODY', 'You draw two cards, then each other player draws a card.').
card_first_print('words of wisdom', 'ODY').
card_image_name('words of wisdom'/'ODY', 'words of wisdom').
card_uid('words of wisdom'/'ODY', 'ODY:Words of Wisdom:words of wisdom').
card_rarity('words of wisdom'/'ODY', 'Common').
card_artist('words of wisdom'/'ODY', 'Eric Peterson').
card_number('words of wisdom'/'ODY', '114').
card_flavor_text('words of wisdom'/'ODY', 'Knowledge can\'t be kept in a bottle or sealed in a box. It yearns to be spread and shared.').
card_multiverse_id('words of wisdom'/'ODY', '31779').

card_in_set('zombie assassin', 'ODY').
card_original_type('zombie assassin'/'ODY', 'Creature — Zombie Assassin').
card_original_text('zombie assassin'/'ODY', '{T}, Remove two cards in your graveyard and Zombie Assassin from the game: Destroy target nonblack creature. It can\'t be regenerated.').
card_first_print('zombie assassin', 'ODY').
card_image_name('zombie assassin'/'ODY', 'zombie assassin').
card_uid('zombie assassin'/'ODY', 'ODY:Zombie Assassin:zombie assassin').
card_rarity('zombie assassin'/'ODY', 'Common').
card_artist('zombie assassin'/'ODY', 'Pete Venters').
card_number('zombie assassin'/'ODY', '168').
card_flavor_text('zombie assassin'/'ODY', '\"Zombies don\'t kill. They recruit.\"\n—Braids, dementia summoner').
card_multiverse_id('zombie assassin'/'ODY', '29732').

card_in_set('zombie cannibal', 'ODY').
card_original_type('zombie cannibal'/'ODY', 'Creature — Zombie').
card_original_text('zombie cannibal'/'ODY', 'Whenever Zombie Cannibal deals combat damage to a player, you may remove target card in that player\'s graveyard from the game.').
card_first_print('zombie cannibal', 'ODY').
card_image_name('zombie cannibal'/'ODY', 'zombie cannibal').
card_uid('zombie cannibal'/'ODY', 'ODY:Zombie Cannibal:zombie cannibal').
card_rarity('zombie cannibal'/'ODY', 'Common').
card_artist('zombie cannibal'/'ODY', 'Adam Rex').
card_number('zombie cannibal'/'ODY', '169').
card_flavor_text('zombie cannibal'/'ODY', 'Nothing is sacred to the undead—not even their own kind.').
card_multiverse_id('zombie cannibal'/'ODY', '29735').

card_in_set('zombie infestation', 'ODY').
card_original_type('zombie infestation'/'ODY', 'Enchantment').
card_original_text('zombie infestation'/'ODY', 'Discard two cards from your hand: Put a 2/2 black Zombie creature token into play.').
card_first_print('zombie infestation', 'ODY').
card_image_name('zombie infestation'/'ODY', 'zombie infestation').
card_uid('zombie infestation'/'ODY', 'ODY:Zombie Infestation:zombie infestation').
card_rarity('zombie infestation'/'ODY', 'Uncommon').
card_artist('zombie infestation'/'ODY', 'Thomas M. Baxa').
card_number('zombie infestation'/'ODY', '170').
card_flavor_text('zombie infestation'/'ODY', 'The nomads\' funeral pyres are more practical than ceremonial.').
card_multiverse_id('zombie infestation'/'ODY', '29957').

card_in_set('zombify', 'ODY').
card_original_type('zombify'/'ODY', 'Sorcery').
card_original_text('zombify'/'ODY', 'Return target creature card from your graveyard to play.').
card_image_name('zombify'/'ODY', 'zombify').
card_uid('zombify'/'ODY', 'ODY:Zombify:zombify').
card_rarity('zombify'/'ODY', 'Uncommon').
card_artist('zombify'/'ODY', 'Mark Romanoski').
card_number('zombify'/'ODY', '171').
card_flavor_text('zombify'/'ODY', '\"The first birth celebrates life. The second birth mocks it.\"\n—Mystic elder').
card_multiverse_id('zombify'/'ODY', '29847').

card_in_set('zoologist', 'ODY').
card_original_type('zoologist'/'ODY', 'Creature — Druid').
card_original_text('zoologist'/'ODY', '{3}{G}, {T}: Reveal the top card of your library. If it\'s a creature card, put it into play. Otherwise, put it into your graveyard.').
card_first_print('zoologist', 'ODY').
card_image_name('zoologist'/'ODY', 'zoologist').
card_uid('zoologist'/'ODY', 'ODY:Zoologist:zoologist').
card_rarity('zoologist'/'ODY', 'Rare').
card_artist('zoologist'/'ODY', 'Alex Horley-Orlandelli').
card_number('zoologist'/'ODY', '285').
card_flavor_text('zoologist'/'ODY', '\"With friends like these, who needs people?\"').
card_multiverse_id('zoologist'/'ODY', '29984').
