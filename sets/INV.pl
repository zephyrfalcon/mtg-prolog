% Invasion

set('INV').
set_name('INV', 'Invasion').
set_release_date('INV', '2000-10-02').
set_border('INV', 'black').
set_type('INV', 'expansion').
set_block('INV', 'Invasion').

card_in_set('absorb', 'INV').
card_original_type('absorb'/'INV', 'Instant').
card_original_text('absorb'/'INV', 'Counter target spell. You gain 3 life.').
card_first_print('absorb', 'INV').
card_image_name('absorb'/'INV', 'absorb').
card_uid('absorb'/'INV', 'INV:Absorb:absorb').
card_rarity('absorb'/'INV', 'Rare').
card_artist('absorb'/'INV', 'Andrew Goldhawk').
card_number('absorb'/'INV', '226').
card_flavor_text('absorb'/'INV', '\"Your presumption is your downfall.\"').
card_multiverse_id('absorb'/'INV', '23155').

card_in_set('addle', 'INV').
card_original_type('addle'/'INV', 'Sorcery').
card_original_text('addle'/'INV', 'Choose a color. Look at target player\'s hand and choose a card of that color from it. That player discards that card.').
card_first_print('addle', 'INV').
card_image_name('addle'/'INV', 'addle').
card_uid('addle'/'INV', 'INV:Addle:addle').
card_rarity('addle'/'INV', 'Uncommon').
card_artist('addle'/'INV', 'Ron Spears').
card_number('addle'/'INV', '91').
card_flavor_text('addle'/'INV', '\"I\'ll wring out your tiny mind like a sponge.\"\n—Urborg witch').
card_multiverse_id('addle'/'INV', '23030').

card_in_set('æther rift', 'INV').
card_original_type('æther rift'/'INV', 'Enchantment').
card_original_text('æther rift'/'INV', 'At the beginning of your upkeep, discard a card at random from your hand. If you discard a creature card this way, put that card into play unless any player pays 5 life.').
card_first_print('æther rift', 'INV').
card_image_name('æther rift'/'INV', 'aether rift').
card_uid('æther rift'/'INV', 'INV:Æther Rift:aether rift').
card_rarity('æther rift'/'INV', 'Rare').
card_artist('æther rift'/'INV', 'Heather Hudson').
card_number('æther rift'/'INV', '227').
card_multiverse_id('æther rift'/'INV', '23194').

card_in_set('aggressive urge', 'INV').
card_original_type('aggressive urge'/'INV', 'Instant').
card_original_text('aggressive urge'/'INV', 'Target creature gets +1/+1 until end of turn.\nDraw a card.').
card_first_print('aggressive urge', 'INV').
card_image_name('aggressive urge'/'INV', 'aggressive urge').
card_uid('aggressive urge'/'INV', 'INV:Aggressive Urge:aggressive urge').
card_rarity('aggressive urge'/'INV', 'Common').
card_artist('aggressive urge'/'INV', 'Christopher Moeller').
card_number('aggressive urge'/'INV', '181').
card_flavor_text('aggressive urge'/'INV', 'The power of the wild, concentrated in a single charge.').
card_multiverse_id('aggressive urge'/'INV', '24127').

card_in_set('agonizing demise', 'INV').
card_original_type('agonizing demise'/'INV', 'Instant').
card_original_text('agonizing demise'/'INV', 'Kicker {1}{R} (You may pay an additional {1}{R} as you play this spell.)\nDestroy target nonblack creature. It can\'t be regenerated. If you paid the kicker cost, Agonizing Demise deals damage equal to that creature\'s power to the creature\'s controller.').
card_first_print('agonizing demise', 'INV').
card_image_name('agonizing demise'/'INV', 'agonizing demise').
card_uid('agonizing demise'/'INV', 'INV:Agonizing Demise:agonizing demise').
card_rarity('agonizing demise'/'INV', 'Common').
card_artist('agonizing demise'/'INV', 'Mark Brill').
card_number('agonizing demise'/'INV', '92').
card_multiverse_id('agonizing demise'/'INV', '23034').

card_in_set('alabaster leech', 'INV').
card_original_type('alabaster leech'/'INV', 'Creature — Leech').
card_original_text('alabaster leech'/'INV', 'White spells you play cost {W} more to play.').
card_first_print('alabaster leech', 'INV').
card_image_name('alabaster leech'/'INV', 'alabaster leech').
card_uid('alabaster leech'/'INV', 'INV:Alabaster Leech:alabaster leech').
card_rarity('alabaster leech'/'INV', 'Rare').
card_artist('alabaster leech'/'INV', 'Edward P. Beard, Jr.').
card_number('alabaster leech'/'INV', '1').
card_flavor_text('alabaster leech'/'INV', '\"Its stones seem to serve a healing function, but removing them intact is an exhausting process.\"\n—Tolarian research notes').
card_multiverse_id('alabaster leech'/'INV', '22960').

card_in_set('alloy golem', 'INV').
card_original_type('alloy golem'/'INV', 'Artifact Creature — Golem').
card_original_text('alloy golem'/'INV', 'As Alloy Golem comes into play, choose a color.\nAlloy Golem is the chosen color. (It\'s still an artifact.)').
card_first_print('alloy golem', 'INV').
card_image_name('alloy golem'/'INV', 'alloy golem').
card_uid('alloy golem'/'INV', 'INV:Alloy Golem:alloy golem').
card_rarity('alloy golem'/'INV', 'Uncommon').
card_artist('alloy golem'/'INV', 'Greg Staples').
card_number('alloy golem'/'INV', '297').
card_flavor_text('alloy golem'/'INV', '\"We turned old weapons into new warriors.\"\n—Jhoira, master artificer').
card_multiverse_id('alloy golem'/'INV', '23224').

card_in_set('ancient kavu', 'INV').
card_original_type('ancient kavu'/'INV', 'Creature — Kavu').
card_original_text('ancient kavu'/'INV', '{2}: Ancient Kavu becomes colorless until end of turn.').
card_first_print('ancient kavu', 'INV').
card_image_name('ancient kavu'/'INV', 'ancient kavu').
card_uid('ancient kavu'/'INV', 'INV:Ancient Kavu:ancient kavu').
card_rarity('ancient kavu'/'INV', 'Common').
card_artist('ancient kavu'/'INV', 'Glen Angus').
card_number('ancient kavu'/'INV', '136').
card_flavor_text('ancient kavu'/'INV', 'Those with the ability to change their nature survived Phyrexia\'s biological attacks. Everything else died.').
card_multiverse_id('ancient kavu'/'INV', '23069').

card_in_set('ancient spring', 'INV').
card_original_type('ancient spring'/'INV', 'Land').
card_original_text('ancient spring'/'INV', 'Ancient Spring comes into play tapped.\n{T}: Add {U} to your mana pool.\n{T}, Sacrifice Ancient Spring: Add {W}{B} to your mana pool.').
card_first_print('ancient spring', 'INV').
card_image_name('ancient spring'/'INV', 'ancient spring').
card_uid('ancient spring'/'INV', 'INV:Ancient Spring:ancient spring').
card_rarity('ancient spring'/'INV', 'Common').
card_artist('ancient spring'/'INV', 'Don Hazeltine').
card_number('ancient spring'/'INV', '319').
card_multiverse_id('ancient spring'/'INV', '23236').

card_in_set('andradite leech', 'INV').
card_original_type('andradite leech'/'INV', 'Creature — Leech').
card_original_text('andradite leech'/'INV', 'Black spells you play cost {B} more to play.\n{B} Andradite Leech gets +1/+1 until end of turn.').
card_first_print('andradite leech', 'INV').
card_image_name('andradite leech'/'INV', 'andradite leech').
card_uid('andradite leech'/'INV', 'INV:Andradite Leech:andradite leech').
card_rarity('andradite leech'/'INV', 'Rare').
card_artist('andradite leech'/'INV', 'Wayne England').
card_number('andradite leech'/'INV', '93').
card_flavor_text('andradite leech'/'INV', '\"Older specimens are completely encrusted with gems, which serve as both armor and weapons.\"\n—Tolarian research notes').
card_multiverse_id('andradite leech'/'INV', '23052').

card_in_set('angel of mercy', 'INV').
card_original_type('angel of mercy'/'INV', 'Creature — Angel').
card_original_text('angel of mercy'/'INV', 'Flying\nWhen Angel of Mercy comes into play, you gain 3 life.').
card_image_name('angel of mercy'/'INV', 'angel of mercy').
card_uid('angel of mercy'/'INV', 'INV:Angel of Mercy:angel of mercy').
card_rarity('angel of mercy'/'INV', 'Uncommon').
card_artist('angel of mercy'/'INV', 'Mark Tedin').
card_number('angel of mercy'/'INV', '2').
card_flavor_text('angel of mercy'/'INV', '\"In times like these, people need to be reminded of compassion.\"\n—Sisay').
card_multiverse_id('angel of mercy'/'INV', '22952').

card_in_set('angelic shield', 'INV').
card_original_type('angelic shield'/'INV', 'Enchantment').
card_original_text('angelic shield'/'INV', 'Creatures you control get +0/+1.\nSacrifice Angelic Shield: Return target creature to its owner\'s hand.').
card_first_print('angelic shield', 'INV').
card_image_name('angelic shield'/'INV', 'angelic shield').
card_uid('angelic shield'/'INV', 'INV:Angelic Shield:angelic shield').
card_rarity('angelic shield'/'INV', 'Uncommon').
card_artist('angelic shield'/'INV', 'Adam Rex').
card_number('angelic shield'/'INV', '228').
card_flavor_text('angelic shield'/'INV', '\"If only an angel\'s wings could shelter us all.\"\n—Barrin').
card_multiverse_id('angelic shield'/'INV', '23177').

card_in_set('annihilate', 'INV').
card_original_type('annihilate'/'INV', 'Instant').
card_original_text('annihilate'/'INV', 'Destroy target nonblack creature. It can\'t be regenerated.\nDraw a card.').
card_first_print('annihilate', 'INV').
card_image_name('annihilate'/'INV', 'annihilate').
card_uid('annihilate'/'INV', 'INV:Annihilate:annihilate').
card_rarity('annihilate'/'INV', 'Uncommon').
card_artist('annihilate'/'INV', 'Kev Walker').
card_number('annihilate'/'INV', '94').
card_flavor_text('annihilate'/'INV', '\"Whatever Yawgmoth marks, dies. There is no escape.\"\n—Tsabo Tavoc, Phyrexian general').
card_multiverse_id('annihilate'/'INV', '23042').

card_in_set('archaeological dig', 'INV').
card_original_type('archaeological dig'/'INV', 'Land').
card_original_text('archaeological dig'/'INV', '{T}: Add one colorless mana to your mana pool.\n{T}, Sacrifice Archaeological Dig: Add one mana of any color to your mana pool.').
card_first_print('archaeological dig', 'INV').
card_image_name('archaeological dig'/'INV', 'archaeological dig').
card_uid('archaeological dig'/'INV', 'INV:Archaeological Dig:archaeological dig').
card_rarity('archaeological dig'/'INV', 'Uncommon').
card_artist('archaeological dig'/'INV', 'Don Hazeltine').
card_number('archaeological dig'/'INV', '320').
card_multiverse_id('archaeological dig'/'INV', '23245').

card_in_set('ardent soldier', 'INV').
card_original_type('ardent soldier'/'INV', 'Creature — Soldier').
card_original_text('ardent soldier'/'INV', 'Kicker {2} (You may pay an additional {2} as you play this spell.)\nAttacking doesn\'t cause Ardent Soldier to tap.\nIf you paid the kicker cost, Ardent Soldier comes into play with a +1/+1 counter on it.').
card_first_print('ardent soldier', 'INV').
card_image_name('ardent soldier'/'INV', 'ardent soldier').
card_uid('ardent soldier'/'INV', 'INV:Ardent Soldier:ardent soldier').
card_rarity('ardent soldier'/'INV', 'Common').
card_artist('ardent soldier'/'INV', 'Paolo Parente').
card_number('ardent soldier'/'INV', '3').
card_multiverse_id('ardent soldier'/'INV', '22931').

card_in_set('armadillo cloak', 'INV').
card_original_type('armadillo cloak'/'INV', 'Enchant Creature').
card_original_text('armadillo cloak'/'INV', 'Enchanted creature gets +2/+2 and has trample.\nWhenever enchanted creature deals damage, you gain that much life.').
card_image_name('armadillo cloak'/'INV', 'armadillo cloak').
card_uid('armadillo cloak'/'INV', 'INV:Armadillo Cloak:armadillo cloak').
card_rarity('armadillo cloak'/'INV', 'Common').
card_artist('armadillo cloak'/'INV', 'Paolo Parente').
card_number('armadillo cloak'/'INV', '229').
card_flavor_text('armadillo cloak'/'INV', '\"Don\'t laugh. It works.\"\n—Yavimaya ranger').
card_multiverse_id('armadillo cloak'/'INV', '23164').

card_in_set('armored guardian', 'INV').
card_original_type('armored guardian'/'INV', 'Creature — Guardian').
card_original_text('armored guardian'/'INV', '{1}{W}{W} Target creature you control gains protection from the color of your choice until end of turn.\n{1}{U}{U} Armored Guardian can\'t be the target of spells or abilities this turn.').
card_first_print('armored guardian', 'INV').
card_image_name('armored guardian'/'INV', 'armored guardian').
card_uid('armored guardian'/'INV', 'INV:Armored Guardian:armored guardian').
card_rarity('armored guardian'/'INV', 'Rare').
card_artist('armored guardian'/'INV', 'Arnie Swekel').
card_number('armored guardian'/'INV', '230').
card_multiverse_id('armored guardian'/'INV', '23197').

card_in_set('artifact mutation', 'INV').
card_original_type('artifact mutation'/'INV', 'Instant').
card_original_text('artifact mutation'/'INV', 'Destroy target artifact. It can\'t be regenerated. Put X 1/1 green Saproling creature tokens into play, where X is its converted mana cost.').
card_first_print('artifact mutation', 'INV').
card_image_name('artifact mutation'/'INV', 'artifact mutation').
card_uid('artifact mutation'/'INV', 'INV:Artifact Mutation:artifact mutation').
card_rarity('artifact mutation'/'INV', 'Rare').
card_artist('artifact mutation'/'INV', 'Greg Staples').
card_number('artifact mutation'/'INV', '231').
card_flavor_text('artifact mutation'/'INV', '\"From shards and splinters I call forth my living horde.\"\n—Molimo, maro-sorcerer').
card_multiverse_id('artifact mutation'/'INV', '23195').

card_in_set('assault', 'INV').
card_original_type('assault'/'INV', 'Sorcery').
card_original_text('assault'/'INV', 'Assault deals 2 damage to target creature or player.').
card_first_print('assault', 'INV').
card_image_name('assault'/'INV', 'assaultbattery').
card_uid('assault'/'INV', 'INV:Assault:assaultbattery').
card_rarity('assault'/'INV', 'Uncommon').
card_artist('assault'/'INV', 'Ben Thompson').
card_number('assault'/'INV', '295a').
card_multiverse_id('assault'/'INV', '20580').

card_in_set('atalya, samite master', 'INV').
card_original_type('atalya, samite master'/'INV', 'Creature — Cleric Legend').
card_original_text('atalya, samite master'/'INV', '{X}, {T}: Choose one Prevent the next X damage that would be dealt to target creature this turn; or you gain X life. Spend only white mana this way.').
card_first_print('atalya, samite master', 'INV').
card_image_name('atalya, samite master'/'INV', 'atalya, samite master').
card_uid('atalya, samite master'/'INV', 'INV:Atalya, Samite Master:atalya, samite master').
card_rarity('atalya, samite master'/'INV', 'Rare').
card_artist('atalya, samite master'/'INV', 'Rebecca Guay').
card_number('atalya, samite master'/'INV', '4').
card_flavor_text('atalya, samite master'/'INV', '\"Healing is a gift the divine ones have shared with us. It is a sacred trust.\"').
card_multiverse_id('atalya, samite master'/'INV', '22964').

card_in_set('aura mutation', 'INV').
card_original_type('aura mutation'/'INV', 'Instant').
card_original_text('aura mutation'/'INV', 'Destroy target enchantment. Put X 1/1 green Saproling creature tokens into play, where X is its converted mana cost.').
card_first_print('aura mutation', 'INV').
card_image_name('aura mutation'/'INV', 'aura mutation').
card_uid('aura mutation'/'INV', 'INV:Aura Mutation:aura mutation').
card_rarity('aura mutation'/'INV', 'Rare').
card_artist('aura mutation'/'INV', 'Pete Venters').
card_number('aura mutation'/'INV', '232').
card_flavor_text('aura mutation'/'INV', '\"Life can be found in all things, even things unnatural.\"\n—Multani, maro-sorcerer').
card_multiverse_id('aura mutation'/'INV', '23184').

card_in_set('aura shards', 'INV').
card_original_type('aura shards'/'INV', 'Enchantment').
card_original_text('aura shards'/'INV', 'Whenever a creature comes into play under your control, you may destroy target artifact or enchantment.').
card_first_print('aura shards', 'INV').
card_image_name('aura shards'/'INV', 'aura shards').
card_uid('aura shards'/'INV', 'INV:Aura Shards:aura shards').
card_rarity('aura shards'/'INV', 'Uncommon').
card_artist('aura shards'/'INV', 'Ron Spencer').
card_number('aura shards'/'INV', '233').
card_flavor_text('aura shards'/'INV', 'Gaea forged her soldiers into self-wielding weapons that struck down all impurities.').
card_multiverse_id('aura shards'/'INV', '23152').

card_in_set('backlash', 'INV').
card_original_type('backlash'/'INV', 'Instant').
card_original_text('backlash'/'INV', 'Tap target untapped creature. That creature deals damage equal to its power to its controller.').
card_first_print('backlash', 'INV').
card_image_name('backlash'/'INV', 'backlash').
card_uid('backlash'/'INV', 'INV:Backlash:backlash').
card_rarity('backlash'/'INV', 'Uncommon').
card_artist('backlash'/'INV', 'Chippy').
card_number('backlash'/'INV', '234').
card_flavor_text('backlash'/'INV', 'Darigaaz decided his foe would be more useful as a weapon.').
card_multiverse_id('backlash'/'INV', '23171').

card_in_set('barrin\'s spite', 'INV').
card_original_type('barrin\'s spite'/'INV', 'Sorcery').
card_original_text('barrin\'s spite'/'INV', 'Choose two target creatures controlled by one player. That player chooses and sacrifices one of them. Return the other to its owner\'s hand.').
card_first_print('barrin\'s spite', 'INV').
card_image_name('barrin\'s spite'/'INV', 'barrin\'s spite').
card_uid('barrin\'s spite'/'INV', 'INV:Barrin\'s Spite:barrin\'s spite').
card_rarity('barrin\'s spite'/'INV', 'Rare').
card_artist('barrin\'s spite'/'INV', 'Terese Nielsen').
card_number('barrin\'s spite'/'INV', '235').
card_flavor_text('barrin\'s spite'/'INV', '\"Only vengeance matters now.\"\n—Barrin').
card_multiverse_id('barrin\'s spite'/'INV', '25751').

card_in_set('barrin\'s unmaking', 'INV').
card_original_type('barrin\'s unmaking'/'INV', 'Instant').
card_original_text('barrin\'s unmaking'/'INV', 'Return target permanent to its owner\'s hand if that permanent shares a color with the most common color among all permanents or the color tied for most common.').
card_first_print('barrin\'s unmaking', 'INV').
card_image_name('barrin\'s unmaking'/'INV', 'barrin\'s unmaking').
card_uid('barrin\'s unmaking'/'INV', 'INV:Barrin\'s Unmaking:barrin\'s unmaking').
card_rarity('barrin\'s unmaking'/'INV', 'Common').
card_artist('barrin\'s unmaking'/'INV', 'Luca Zontini').
card_number('barrin\'s unmaking'/'INV', '46').
card_multiverse_id('barrin\'s unmaking'/'INV', '22984').

card_in_set('battery', 'INV').
card_original_type('battery'/'INV', 'Sorcery').
card_original_text('battery'/'INV', 'Put a 3/3 green Elephant creature token into play.').
card_first_print('battery', 'INV').
card_image_name('battery'/'INV', 'assaultbattery').
card_uid('battery'/'INV', 'INV:Battery:assaultbattery').
card_rarity('battery'/'INV', 'Uncommon').
card_artist('battery'/'INV', 'Ben Thompson').
card_number('battery'/'INV', '295b').
card_multiverse_id('battery'/'INV', '20580').

card_in_set('benalish emissary', 'INV').
card_original_type('benalish emissary'/'INV', 'Creature — Wizard').
card_original_text('benalish emissary'/'INV', 'Kicker {1}{G} (You may pay an additional {1}{G} as you play this spell.)\nWhen Benalish Emissary comes into play, if you paid the kicker cost, destroy target land.').
card_first_print('benalish emissary', 'INV').
card_image_name('benalish emissary'/'INV', 'benalish emissary').
card_uid('benalish emissary'/'INV', 'INV:Benalish Emissary:benalish emissary').
card_rarity('benalish emissary'/'INV', 'Uncommon').
card_artist('benalish emissary'/'INV', 'Randy Gallegos').
card_number('benalish emissary'/'INV', '5').
card_multiverse_id('benalish emissary'/'INV', '22948').

card_in_set('benalish heralds', 'INV').
card_original_type('benalish heralds'/'INV', 'Creature — Soldier').
card_original_text('benalish heralds'/'INV', '{3}{U}, {T}: Draw a card.').
card_first_print('benalish heralds', 'INV').
card_image_name('benalish heralds'/'INV', 'benalish heralds').
card_uid('benalish heralds'/'INV', 'INV:Benalish Heralds:benalish heralds').
card_rarity('benalish heralds'/'INV', 'Uncommon').
card_artist('benalish heralds'/'INV', 'Don Hazeltine').
card_number('benalish heralds'/'INV', '6').
card_flavor_text('benalish heralds'/'INV', 'The detailed dispatch could be summarized in four words: \"Time is running out.\"').
card_multiverse_id('benalish heralds'/'INV', '22947').

card_in_set('benalish lancer', 'INV').
card_original_type('benalish lancer'/'INV', 'Creature — Knight').
card_original_text('benalish lancer'/'INV', 'Kicker {2}{W} (You may pay an additional {2}{W} as you play this spell.)\nIf you paid the kicker cost, Benalish Lancer comes into play with two +1/+1 counters on it and has first strike.').
card_first_print('benalish lancer', 'INV').
card_image_name('benalish lancer'/'INV', 'benalish lancer').
card_uid('benalish lancer'/'INV', 'INV:Benalish Lancer:benalish lancer').
card_rarity('benalish lancer'/'INV', 'Common').
card_artist('benalish lancer'/'INV', 'Paolo Parente').
card_number('benalish lancer'/'INV', '7').
card_multiverse_id('benalish lancer'/'INV', '22932').

card_in_set('benalish trapper', 'INV').
card_original_type('benalish trapper'/'INV', 'Creature — Soldier').
card_original_text('benalish trapper'/'INV', '{W}, {T}: Tap target creature.').
card_first_print('benalish trapper', 'INV').
card_image_name('benalish trapper'/'INV', 'benalish trapper').
card_uid('benalish trapper'/'INV', 'INV:Benalish Trapper:benalish trapper').
card_rarity('benalish trapper'/'INV', 'Common').
card_artist('benalish trapper'/'INV', 'Ken Meyer, Jr.').
card_number('benalish trapper'/'INV', '8').
card_flavor_text('benalish trapper'/'INV', '\"I\'m up here. You\'re down there. Now who\'s the lower life form?\"').
card_multiverse_id('benalish trapper'/'INV', '22937').

card_in_set('bend or break', 'INV').
card_original_type('bend or break'/'INV', 'Sorcery').
card_original_text('bend or break'/'INV', 'Each player separates all land cards he or she controls into two face-up piles. For each player, an opponent chooses a pile. Destroy all lands in that pile. Tap all lands in the other pile.').
card_first_print('bend or break', 'INV').
card_image_name('bend or break'/'INV', 'bend or break').
card_uid('bend or break'/'INV', 'INV:Bend or Break:bend or break').
card_rarity('bend or break'/'INV', 'Rare').
card_artist('bend or break'/'INV', 'Arnie Swekel').
card_number('bend or break'/'INV', '137').
card_multiverse_id('bend or break'/'INV', '23099').

card_in_set('bind', 'INV').
card_original_type('bind'/'INV', 'Instant').
card_original_text('bind'/'INV', 'Counter target activated ability. (Mana abilities can\'t be countered.)\nDraw a card.').
card_first_print('bind', 'INV').
card_image_name('bind'/'INV', 'bind').
card_uid('bind'/'INV', 'INV:Bind:bind').
card_rarity('bind'/'INV', 'Rare').
card_artist('bind'/'INV', 'Mark Zug').
card_number('bind'/'INV', '182').
card_flavor_text('bind'/'INV', '\"The battlefield is cluttered enough. Be still.\"\n—Multani, maro-sorcerer').
card_multiverse_id('bind'/'INV', '23146').

card_in_set('blazing specter', 'INV').
card_original_type('blazing specter'/'INV', 'Creature — Specter').
card_original_text('blazing specter'/'INV', 'Flying; haste (This creature may attack and {T} the turn it comes under your control.)\nWhenever Blazing Specter deals combat damage to a player, that player discards a card from his or her hand.').
card_first_print('blazing specter', 'INV').
card_image_name('blazing specter'/'INV', 'blazing specter').
card_uid('blazing specter'/'INV', 'INV:Blazing Specter:blazing specter').
card_rarity('blazing specter'/'INV', 'Rare').
card_artist('blazing specter'/'INV', 'Marc Fishman').
card_number('blazing specter'/'INV', '236').
card_multiverse_id('blazing specter'/'INV', '23192').

card_in_set('blind seer', 'INV').
card_original_type('blind seer'/'INV', 'Creature — Legend').
card_original_text('blind seer'/'INV', '{1}{U} Target spell or permanent becomes the color of your choice until end of turn.').
card_first_print('blind seer', 'INV').
card_image_name('blind seer'/'INV', 'blind seer').
card_uid('blind seer'/'INV', 'INV:Blind Seer:blind seer').
card_rarity('blind seer'/'INV', 'Rare').
card_artist('blind seer'/'INV', 'Dave Dorman').
card_number('blind seer'/'INV', '47').
card_flavor_text('blind seer'/'INV', '\"I think he sees more than he lets on.\"\n—Gerrard').
card_multiverse_id('blind seer'/'INV', '23006').

card_in_set('blinding light', 'INV').
card_original_type('blinding light'/'INV', 'Sorcery').
card_original_text('blinding light'/'INV', 'Tap all nonwhite creatures.').
card_image_name('blinding light'/'INV', 'blinding light').
card_uid('blinding light'/'INV', 'INV:Blinding Light:blinding light').
card_rarity('blinding light'/'INV', 'Uncommon').
card_artist('blinding light'/'INV', 'Marc Fishman').
card_number('blinding light'/'INV', '9').
card_flavor_text('blinding light'/'INV', 'An angel\'s sword impales only the body; her righteousness penetrates the soul.').
card_multiverse_id('blinding light'/'INV', '22955').

card_in_set('bloodstone cameo', 'INV').
card_original_type('bloodstone cameo'/'INV', 'Artifact').
card_original_text('bloodstone cameo'/'INV', '{T}: Add {B} or {R} to your mana pool.').
card_first_print('bloodstone cameo', 'INV').
card_image_name('bloodstone cameo'/'INV', 'bloodstone cameo').
card_uid('bloodstone cameo'/'INV', 'INV:Bloodstone Cameo:bloodstone cameo').
card_rarity('bloodstone cameo'/'INV', 'Uncommon').
card_artist('bloodstone cameo'/'INV', 'Tony Szczudlo').
card_number('bloodstone cameo'/'INV', '298').
card_flavor_text('bloodstone cameo'/'INV', '\"The stone whispers to me of dragon\'s fire and darkness. I wish I\'d never pried it from the figurehead of that sunken Keldon longship.\"\n—Isel, master carver').
card_multiverse_id('bloodstone cameo'/'INV', '23219').

card_in_set('blurred mongoose', 'INV').
card_original_type('blurred mongoose'/'INV', 'Creature — Mongoose').
card_original_text('blurred mongoose'/'INV', 'Blurred Mongoose can\'t be countered.\nBlurred Mongoose can\'t be the target of spells or abilities.').
card_first_print('blurred mongoose', 'INV').
card_image_name('blurred mongoose'/'INV', 'blurred mongoose').
card_uid('blurred mongoose'/'INV', 'INV:Blurred Mongoose:blurred mongoose').
card_rarity('blurred mongoose'/'INV', 'Rare').
card_artist('blurred mongoose'/'INV', 'Heather Hudson').
card_number('blurred mongoose'/'INV', '183').
card_flavor_text('blurred mongoose'/'INV', '\"The mongoose blew out its candle and was asleep in bed before the room went dark.\"\n—Llanowar fable').
card_multiverse_id('blurred mongoose'/'INV', '23139').

card_in_set('bog initiate', 'INV').
card_original_type('bog initiate'/'INV', 'Creature — Wizard').
card_original_text('bog initiate'/'INV', '{1}: Add {B} to your mana pool.').
card_first_print('bog initiate', 'INV').
card_image_name('bog initiate'/'INV', 'bog initiate').
card_uid('bog initiate'/'INV', 'INV:Bog Initiate:bog initiate').
card_rarity('bog initiate'/'INV', 'Common').
card_artist('bog initiate'/'INV', 'rk post').
card_number('bog initiate'/'INV', '95').
card_flavor_text('bog initiate'/'INV', '\"Urborg\'s magic is strong. Did Urza send you to protect us or to protect against us?\"\n—Urborg witch, to Barrin').
card_multiverse_id('bog initiate'/'INV', '23024').

card_in_set('breaking wave', 'INV').
card_original_type('breaking wave'/'INV', 'Sorcery').
card_original_text('breaking wave'/'INV', 'You may play Breaking Wave any time you could play an instant if you pay {2} more to play it.\nSimultaneously untap all tapped creatures and tap all untapped creatures.').
card_first_print('breaking wave', 'INV').
card_image_name('breaking wave'/'INV', 'breaking wave').
card_uid('breaking wave'/'INV', 'INV:Breaking Wave:breaking wave').
card_rarity('breaking wave'/'INV', 'Rare').
card_artist('breaking wave'/'INV', 'Carl Critchlow').
card_number('breaking wave'/'INV', '48').
card_multiverse_id('breaking wave'/'INV', '23012').

card_in_set('breath of darigaaz', 'INV').
card_original_type('breath of darigaaz'/'INV', 'Sorcery').
card_original_text('breath of darigaaz'/'INV', 'Kicker {2} (You may pay an additional {2} as you play this spell.)\nBreath of Darigaaz deals 1 damage to each creature without flying and each player. If you paid the kicker cost, Breath of Darigaaz deals 4 damage to each creature without flying and each player instead.').
card_first_print('breath of darigaaz', 'INV').
card_image_name('breath of darigaaz'/'INV', 'breath of darigaaz').
card_uid('breath of darigaaz'/'INV', 'INV:Breath of Darigaaz:breath of darigaaz').
card_rarity('breath of darigaaz'/'INV', 'Uncommon').
card_artist('breath of darigaaz'/'INV', 'Greg & Tim Hildebrandt').
card_number('breath of darigaaz'/'INV', '138').
card_multiverse_id('breath of darigaaz'/'INV', '23090').

card_in_set('callous giant', 'INV').
card_original_type('callous giant'/'INV', 'Creature — Giant').
card_original_text('callous giant'/'INV', 'If a source would deal 3 damage or less to Callous Giant, prevent that damage.').
card_first_print('callous giant', 'INV').
card_image_name('callous giant'/'INV', 'callous giant').
card_uid('callous giant'/'INV', 'INV:Callous Giant:callous giant').
card_rarity('callous giant'/'INV', 'Rare').
card_artist('callous giant'/'INV', 'Mark Brill').
card_number('callous giant'/'INV', '139').
card_flavor_text('callous giant'/'INV', '\"The invasion has awoken slumbering mountains,\" noted Urza. \"Good. We can use all the help we can get.\"').
card_multiverse_id('callous giant'/'INV', '23094').

card_in_set('canopy surge', 'INV').
card_original_type('canopy surge'/'INV', 'Sorcery').
card_original_text('canopy surge'/'INV', 'Kicker {2} (You may pay an additional {2} as you play this spell.)\nCanopy Surge deals 1 damage to each creature with flying and each player. If you paid the kicker cost, Canopy Surge deals 4 damage to each creature with flying and each player instead.').
card_first_print('canopy surge', 'INV').
card_image_name('canopy surge'/'INV', 'canopy surge').
card_uid('canopy surge'/'INV', 'INV:Canopy Surge:canopy surge').
card_rarity('canopy surge'/'INV', 'Uncommon').
card_artist('canopy surge'/'INV', 'Matt Cavotta').
card_number('canopy surge'/'INV', '184').
card_multiverse_id('canopy surge'/'INV', '23134').

card_in_set('capashen unicorn', 'INV').
card_original_type('capashen unicorn'/'INV', 'Creature — Unicorn').
card_original_text('capashen unicorn'/'INV', '{1}{W}, {T}, Sacrifice Capashen Unicorn: Destroy target artifact or enchantment.').
card_first_print('capashen unicorn', 'INV').
card_image_name('capashen unicorn'/'INV', 'capashen unicorn').
card_uid('capashen unicorn'/'INV', 'INV:Capashen Unicorn:capashen unicorn').
card_rarity('capashen unicorn'/'INV', 'Common').
card_artist('capashen unicorn'/'INV', 'Jerry Tiritilli').
card_number('capashen unicorn'/'INV', '10').
card_flavor_text('capashen unicorn'/'INV', 'Capashen riders were stern and humorless even before their ancestral home was reduced to rubble.').
card_multiverse_id('capashen unicorn'/'INV', '22936').

card_in_set('captain sisay', 'INV').
card_original_type('captain sisay'/'INV', 'Creature — Legend').
card_original_text('captain sisay'/'INV', '{T}: Search your library for a Legend or legendary card, reveal that card, and put it into your hand. Then shuffle your library.').
card_first_print('captain sisay', 'INV').
card_image_name('captain sisay'/'INV', 'captain sisay').
card_uid('captain sisay'/'INV', 'INV:Captain Sisay:captain sisay').
card_rarity('captain sisay'/'INV', 'Rare').
card_artist('captain sisay'/'INV', 'Ray Lago').
card_number('captain sisay'/'INV', '237').
card_flavor_text('captain sisay'/'INV', 'Her leadership forged the Weatherlight\'s finest crew.').
card_multiverse_id('captain sisay'/'INV', '25976').

card_in_set('cauldron dance', 'INV').
card_original_type('cauldron dance'/'INV', 'Instant').
card_original_text('cauldron dance'/'INV', 'Play Cauldron Dance only during combat.\nReturn target creature card from your graveyard to play. That creature gains haste. Return it to your hand at end of turn. \nPut a creature card from your hand into play. That creature gains haste. Put it into your graveyard at end of turn.').
card_first_print('cauldron dance', 'INV').
card_image_name('cauldron dance'/'INV', 'cauldron dance').
card_uid('cauldron dance'/'INV', 'INV:Cauldron Dance:cauldron dance').
card_rarity('cauldron dance'/'INV', 'Uncommon').
card_artist('cauldron dance'/'INV', 'Donato Giancola').
card_number('cauldron dance'/'INV', '238').
card_multiverse_id('cauldron dance'/'INV', '23173').

card_in_set('chaotic strike', 'INV').
card_original_type('chaotic strike'/'INV', 'Instant').
card_original_text('chaotic strike'/'INV', 'Play Chaotic Strike only during combat after blockers are declared.\nChoose target creature and flip a coin. If you win the flip, that creature gets +1/+1 until end of turn.\nDraw a card.').
card_first_print('chaotic strike', 'INV').
card_image_name('chaotic strike'/'INV', 'chaotic strike').
card_uid('chaotic strike'/'INV', 'INV:Chaotic Strike:chaotic strike').
card_rarity('chaotic strike'/'INV', 'Uncommon').
card_artist('chaotic strike'/'INV', 'Massimilano Frezzato').
card_number('chaotic strike'/'INV', '140').
card_multiverse_id('chaotic strike'/'INV', '23085').

card_in_set('charging troll', 'INV').
card_original_type('charging troll'/'INV', 'Creature — Troll').
card_original_text('charging troll'/'INV', 'Attacking doesn\'t cause Charging Troll to tap.\n{G} Regenerate Charging Troll.').
card_first_print('charging troll', 'INV').
card_image_name('charging troll'/'INV', 'charging troll').
card_uid('charging troll'/'INV', 'INV:Charging Troll:charging troll').
card_rarity('charging troll'/'INV', 'Uncommon').
card_artist('charging troll'/'INV', 'Dave Dorman').
card_number('charging troll'/'INV', '239').
card_flavor_text('charging troll'/'INV', 'They stop for nothing, not even the end of a battle.').
card_multiverse_id('charging troll'/'INV', '23162').

card_in_set('chromatic sphere', 'INV').
card_original_type('chromatic sphere'/'INV', 'Artifact').
card_original_text('chromatic sphere'/'INV', '{1}, {T}, Sacrifice Chromatic Sphere: Add one mana of any color to your mana pool. Draw a card.').
card_first_print('chromatic sphere', 'INV').
card_image_name('chromatic sphere'/'INV', 'chromatic sphere').
card_uid('chromatic sphere'/'INV', 'INV:Chromatic Sphere:chromatic sphere').
card_rarity('chromatic sphere'/'INV', 'Uncommon').
card_artist('chromatic sphere'/'INV', 'Luca Zontini').
card_number('chromatic sphere'/'INV', '299').
card_flavor_text('chromatic sphere'/'INV', '\"Let insight and energy be your guides.\"\n—The Blind Seer, to Gerrard').
card_multiverse_id('chromatic sphere'/'INV', '23230').

card_in_set('cinder shade', 'INV').
card_original_type('cinder shade'/'INV', 'Creature — Shade').
card_original_text('cinder shade'/'INV', '{B} Cinder Shade gets +1/+1 until end of turn.\n{R}, Sacrifice Cinder Shade: Cinder Shade deals damage equal to its power to target creature.').
card_first_print('cinder shade', 'INV').
card_image_name('cinder shade'/'INV', 'cinder shade').
card_uid('cinder shade'/'INV', 'INV:Cinder Shade:cinder shade').
card_rarity('cinder shade'/'INV', 'Uncommon').
card_artist('cinder shade'/'INV', 'Nelson DeCastro').
card_number('cinder shade'/'INV', '240').
card_multiverse_id('cinder shade'/'INV', '23172').

card_in_set('coalition victory', 'INV').
card_original_type('coalition victory'/'INV', 'Sorcery').
card_original_text('coalition victory'/'INV', 'You win the game if you control a land of each basic land type and a creature of each color.').
card_first_print('coalition victory', 'INV').
card_image_name('coalition victory'/'INV', 'coalition victory').
card_uid('coalition victory'/'INV', 'INV:Coalition Victory:coalition victory').
card_rarity('coalition victory'/'INV', 'Rare').
card_artist('coalition victory'/'INV', 'Eric Peterson').
card_number('coalition victory'/'INV', '241').
card_flavor_text('coalition victory'/'INV', '\"You can build a perfect machine out of imperfect parts.\"\n—Urza').
card_multiverse_id('coalition victory'/'INV', '23216').

card_in_set('coastal tower', 'INV').
card_original_type('coastal tower'/'INV', 'Land').
card_original_text('coastal tower'/'INV', 'Coastal Tower comes into play tapped.\n{T}: Add {W} or {U} to your mana pool.').
card_first_print('coastal tower', 'INV').
card_image_name('coastal tower'/'INV', 'coastal tower').
card_uid('coastal tower'/'INV', 'INV:Coastal Tower:coastal tower').
card_rarity('coastal tower'/'INV', 'Uncommon').
card_artist('coastal tower'/'INV', 'Don Hazeltine').
card_number('coastal tower'/'INV', '321').
card_flavor_text('coastal tower'/'INV', 'The Capashen built the highest towers in Benalia to afford themselves the best view.').
card_multiverse_id('coastal tower'/'INV', '23240').

card_in_set('collapsing borders', 'INV').
card_original_type('collapsing borders'/'INV', 'Enchantment').
card_original_text('collapsing borders'/'INV', 'At the beginning of each player\'s upkeep, that player gains 1 life for each basic land type among lands he or she controls. Then Collapsing Borders deals 3 damage to him or her.').
card_first_print('collapsing borders', 'INV').
card_image_name('collapsing borders'/'INV', 'collapsing borders').
card_uid('collapsing borders'/'INV', 'INV:Collapsing Borders:collapsing borders').
card_rarity('collapsing borders'/'INV', 'Rare').
card_artist('collapsing borders'/'INV', 'Glen Angus').
card_number('collapsing borders'/'INV', '141').
card_multiverse_id('collapsing borders'/'INV', '23089').

card_in_set('collective restraint', 'INV').
card_original_type('collective restraint'/'INV', 'Enchantment').
card_original_text('collective restraint'/'INV', 'Creatures can\'t attack you unless their controller pays {X} for each creature attacking you, where X is the number of basic land types among lands you control. (This cost is paid as attackers are declared.)').
card_first_print('collective restraint', 'INV').
card_image_name('collective restraint'/'INV', 'collective restraint').
card_uid('collective restraint'/'INV', 'INV:Collective Restraint:collective restraint').
card_rarity('collective restraint'/'INV', 'Rare').
card_artist('collective restraint'/'INV', 'Alan Rabinowitz').
card_number('collective restraint'/'INV', '49').
card_multiverse_id('collective restraint'/'INV', '23001').

card_in_set('cremate', 'INV').
card_original_type('cremate'/'INV', 'Instant').
card_original_text('cremate'/'INV', 'Remove target card in a graveyard from the game.\nDraw a card.').
card_first_print('cremate', 'INV').
card_image_name('cremate'/'INV', 'cremate').
card_uid('cremate'/'INV', 'INV:Cremate:cremate').
card_rarity('cremate'/'INV', 'Uncommon').
card_artist('cremate'/'INV', 'Andrew Goldhawk').
card_number('cremate'/'INV', '96').
card_flavor_text('cremate'/'INV', 'Death\'s embrace need not be cold.').
card_multiverse_id('cremate'/'INV', '24119').

card_in_set('crimson acolyte', 'INV').
card_original_type('crimson acolyte'/'INV', 'Creature — Cleric').
card_original_text('crimson acolyte'/'INV', 'Protection from red\n{W} Target creature gains protection from red until end of turn.').
card_first_print('crimson acolyte', 'INV').
card_image_name('crimson acolyte'/'INV', 'crimson acolyte').
card_uid('crimson acolyte'/'INV', 'INV:Crimson Acolyte:crimson acolyte').
card_rarity('crimson acolyte'/'INV', 'Common').
card_artist('crimson acolyte'/'INV', 'Orizio Daniele').
card_number('crimson acolyte'/'INV', '11').
card_flavor_text('crimson acolyte'/'INV', 'The faithful will walk through streams of fire and emerge unscathed.\n—Crimson acolyte creed').
card_multiverse_id('crimson acolyte'/'INV', '22934').

card_in_set('crosis\'s attendant', 'INV').
card_original_type('crosis\'s attendant'/'INV', 'Artifact Creature — Golem').
card_original_text('crosis\'s attendant'/'INV', '{1}, Sacrifice Crosis\'s Attendant: Add {U}{B}{R} to your mana pool.').
card_first_print('crosis\'s attendant', 'INV').
card_image_name('crosis\'s attendant'/'INV', 'crosis\'s attendant').
card_uid('crosis\'s attendant'/'INV', 'INV:Crosis\'s Attendant:crosis\'s attendant').
card_rarity('crosis\'s attendant'/'INV', 'Uncommon').
card_artist('crosis\'s attendant'/'INV', 'Arnie Swekel').
card_number('crosis\'s attendant'/'INV', '300').
card_flavor_text('crosis\'s attendant'/'INV', '\"Crosis is the eye of the ur-dragon, piercing illusion and darkness.\"').
card_multiverse_id('crosis\'s attendant'/'INV', '25837').

card_in_set('crosis, the purger', 'INV').
card_original_type('crosis, the purger'/'INV', 'Creature — Dragon Legend').
card_original_text('crosis, the purger'/'INV', 'Flying\nWhenever Crosis, the Purger deals combat damage to a player, you may pay {2}{B}. If you do, choose a color. That player reveals his or her hand and discards all cards of that color from it.').
card_first_print('crosis, the purger', 'INV').
card_image_name('crosis, the purger'/'INV', 'crosis, the purger').
card_uid('crosis, the purger'/'INV', 'INV:Crosis, the Purger:crosis, the purger').
card_rarity('crosis, the purger'/'INV', 'Rare').
card_artist('crosis, the purger'/'INV', 'Pete Venters').
card_number('crosis, the purger'/'INV', '242').
card_multiverse_id('crosis, the purger'/'INV', '23207').

card_in_set('crown of flames', 'INV').
card_original_type('crown of flames'/'INV', 'Enchant Creature').
card_original_text('crown of flames'/'INV', '{R} Enchanted creature gets +1/+0 until end of turn.\n{R} Return Crown of Flames to its owner\'s hand.').
card_image_name('crown of flames'/'INV', 'crown of flames').
card_uid('crown of flames'/'INV', 'INV:Crown of Flames:crown of flames').
card_rarity('crown of flames'/'INV', 'Common').
card_artist('crown of flames'/'INV', 'Christopher Moeller').
card_number('crown of flames'/'INV', '142').
card_flavor_text('crown of flames'/'INV', '\"All my life I\'ve fought these monsters. Today I finish that fight.\"\n—Barrin').
card_multiverse_id('crown of flames'/'INV', '23074').

card_in_set('crusading knight', 'INV').
card_original_type('crusading knight'/'INV', 'Creature — Knight').
card_original_text('crusading knight'/'INV', 'Protection from black\nCrusading Knight gets +1/+1 for each swamp your opponents control.').
card_first_print('crusading knight', 'INV').
card_image_name('crusading knight'/'INV', 'crusading knight').
card_uid('crusading knight'/'INV', 'INV:Crusading Knight:crusading knight').
card_rarity('crusading knight'/'INV', 'Rare').
card_artist('crusading knight'/'INV', 'Edward P. Beard, Jr.').
card_number('crusading knight'/'INV', '12').
card_flavor_text('crusading knight'/'INV', '\"My only dream is to destroy the nightmares of others.\"').
card_multiverse_id('crusading knight'/'INV', '22963').

card_in_set('crypt angel', 'INV').
card_original_type('crypt angel'/'INV', 'Creature — Angel').
card_original_text('crypt angel'/'INV', 'Flying, protection from white\nWhen Crypt Angel comes into play, return target blue or red creature card from your graveyard to your hand.').
card_first_print('crypt angel', 'INV').
card_image_name('crypt angel'/'INV', 'crypt angel').
card_uid('crypt angel'/'INV', 'INV:Crypt Angel:crypt angel').
card_rarity('crypt angel'/'INV', 'Rare').
card_artist('crypt angel'/'INV', 'Todd Lockwood').
card_number('crypt angel'/'INV', '97').
card_flavor_text('crypt angel'/'INV', 'Once an angel, now an abomination.').
card_multiverse_id('crypt angel'/'INV', '23320').

card_in_set('crystal spray', 'INV').
card_original_type('crystal spray'/'INV', 'Instant').
card_original_text('crystal spray'/'INV', 'Change the text of target spell or permanent by replacing all instances of one color word or basic land type with another until end of turn.\nDraw a card.').
card_first_print('crystal spray', 'INV').
card_image_name('crystal spray'/'INV', 'crystal spray').
card_uid('crystal spray'/'INV', 'INV:Crystal Spray:crystal spray').
card_rarity('crystal spray'/'INV', 'Rare').
card_artist('crystal spray'/'INV', 'Jeff Miracola').
card_number('crystal spray'/'INV', '50').
card_multiverse_id('crystal spray'/'INV', '23008').

card_in_set('cursed flesh', 'INV').
card_original_type('cursed flesh'/'INV', 'Enchant Creature').
card_original_text('cursed flesh'/'INV', 'Enchanted creature gets -1/-1 and can\'t be blocked except by artifact creatures and/or black creatures.').
card_image_name('cursed flesh'/'INV', 'cursed flesh').
card_uid('cursed flesh'/'INV', 'INV:Cursed Flesh:cursed flesh').
card_rarity('cursed flesh'/'INV', 'Common').
card_artist('cursed flesh'/'INV', 'Chippy').
card_number('cursed flesh'/'INV', '98').
card_flavor_text('cursed flesh'/'INV', 'Misery, filth, disease—all things that bring suffering to the world—only make Phyrexians more dangerous.').
card_multiverse_id('cursed flesh'/'INV', '23044').

card_in_set('darigaaz\'s attendant', 'INV').
card_original_type('darigaaz\'s attendant'/'INV', 'Artifact Creature — Golem').
card_original_text('darigaaz\'s attendant'/'INV', '{1}, Sacrifice Darigaaz\'s Attendant: Add {B}{R}{G} to your mana pool.').
card_first_print('darigaaz\'s attendant', 'INV').
card_image_name('darigaaz\'s attendant'/'INV', 'darigaaz\'s attendant').
card_uid('darigaaz\'s attendant'/'INV', 'INV:Darigaaz\'s Attendant:darigaaz\'s attendant').
card_rarity('darigaaz\'s attendant'/'INV', 'Uncommon').
card_artist('darigaaz\'s attendant'/'INV', 'Brom').
card_number('darigaaz\'s attendant'/'INV', '301').
card_flavor_text('darigaaz\'s attendant'/'INV', '\"Darigaaz is the breath of the ur-dragon, burning away the burdens of mortality.\"').
card_multiverse_id('darigaaz\'s attendant'/'INV', '25838').

card_in_set('darigaaz, the igniter', 'INV').
card_original_type('darigaaz, the igniter'/'INV', 'Creature — Dragon Legend').
card_original_text('darigaaz, the igniter'/'INV', 'Flying\nWhenever Darigaaz, the Igniter deals combat damage to a player, you may pay {2}{R}. If you do, choose a color. That player reveals his or her hand and Darigaaz deals X damage to him or her, where X is the number of cards revealed of that color.').
card_first_print('darigaaz, the igniter', 'INV').
card_image_name('darigaaz, the igniter'/'INV', 'darigaaz, the igniter').
card_uid('darigaaz, the igniter'/'INV', 'INV:Darigaaz, the Igniter:darigaaz, the igniter').
card_rarity('darigaaz, the igniter'/'INV', 'Rare').
card_artist('darigaaz, the igniter'/'INV', 'Mark Zug').
card_number('darigaaz, the igniter'/'INV', '243').
card_multiverse_id('darigaaz, the igniter'/'INV', '23208').

card_in_set('death or glory', 'INV').
card_original_type('death or glory'/'INV', 'Sorcery').
card_original_text('death or glory'/'INV', 'Separate all creature cards in your graveyard into two face-up piles. Remove the pile of an opponent\'s choice from the game and return the other to play.').
card_first_print('death or glory', 'INV').
card_image_name('death or glory'/'INV', 'death or glory').
card_uid('death or glory'/'INV', 'INV:Death or Glory:death or glory').
card_rarity('death or glory'/'INV', 'Rare').
card_artist('death or glory'/'INV', 'Jeff Easley').
card_number('death or glory'/'INV', '13').
card_multiverse_id('death or glory'/'INV', '22956').

card_in_set('defiling tears', 'INV').
card_original_type('defiling tears'/'INV', 'Instant').
card_original_text('defiling tears'/'INV', 'Until end of turn, target creature becomes black, gets +1/-1, and gains \"{B} Regenerate this creature.\"').
card_first_print('defiling tears', 'INV').
card_image_name('defiling tears'/'INV', 'defiling tears').
card_uid('defiling tears'/'INV', 'INV:Defiling Tears:defiling tears').
card_rarity('defiling tears'/'INV', 'Uncommon').
card_artist('defiling tears'/'INV', 'rk post').
card_number('defiling tears'/'INV', '99').
card_flavor_text('defiling tears'/'INV', 'Serra\'s living warriors looked on in horror. \"Where is our Lady now?\" they cried.').
card_multiverse_id('defiling tears'/'INV', '23046').

card_in_set('deliver', 'INV').
card_original_type('deliver'/'INV', 'Instant').
card_original_text('deliver'/'INV', 'Return target permanent to its owner\'s hand.').
card_first_print('deliver', 'INV').
card_image_name('deliver'/'INV', 'standdeliver').
card_uid('deliver'/'INV', 'INV:Deliver:standdeliver').
card_rarity('deliver'/'INV', 'Uncommon').
card_artist('deliver'/'INV', 'David Martin').
card_number('deliver'/'INV', '292b').
card_multiverse_id('deliver'/'INV', '20574').

card_in_set('desperate research', 'INV').
card_original_type('desperate research'/'INV', 'Sorcery').
card_original_text('desperate research'/'INV', 'Name a card other than a basic land. Then reveal the top seven cards of your library and put all of them with that name into your hand. Remove the rest from the game.').
card_first_print('desperate research', 'INV').
card_image_name('desperate research'/'INV', 'desperate research').
card_uid('desperate research'/'INV', 'INV:Desperate Research:desperate research').
card_rarity('desperate research'/'INV', 'Rare').
card_artist('desperate research'/'INV', 'Ron Spencer').
card_number('desperate research'/'INV', '100').
card_multiverse_id('desperate research'/'INV', '23055').

card_in_set('devouring strossus', 'INV').
card_original_type('devouring strossus'/'INV', 'Creature — Horror').
card_original_text('devouring strossus'/'INV', 'Flying, trample\nAt the beginning of your upkeep, sacrifice a creature.\nSacrifice a creature: Regenerate Devouring Strossus.').
card_first_print('devouring strossus', 'INV').
card_image_name('devouring strossus'/'INV', 'devouring strossus').
card_uid('devouring strossus'/'INV', 'INV:Devouring Strossus:devouring strossus').
card_rarity('devouring strossus'/'INV', 'Rare').
card_artist('devouring strossus'/'INV', 'D. Alexander Gregory').
card_number('devouring strossus'/'INV', '101').
card_multiverse_id('devouring strossus'/'INV', '23050').

card_in_set('dismantling blow', 'INV').
card_original_type('dismantling blow'/'INV', 'Instant').
card_original_text('dismantling blow'/'INV', 'Kicker {2}{U} (You may pay an additional {2}{U} as you play this spell.)\nDestroy target artifact or enchantment.\nIf you paid the kicker cost, draw two cards.').
card_first_print('dismantling blow', 'INV').
card_image_name('dismantling blow'/'INV', 'dismantling blow').
card_uid('dismantling blow'/'INV', 'INV:Dismantling Blow:dismantling blow').
card_rarity('dismantling blow'/'INV', 'Common').
card_artist('dismantling blow'/'INV', 'Mark Tedin').
card_number('dismantling blow'/'INV', '14').
card_multiverse_id('dismantling blow'/'INV', '22946').

card_in_set('disrupt', 'INV').
card_original_type('disrupt'/'INV', 'Instant').
card_original_text('disrupt'/'INV', 'Counter target instant or sorcery spell unless its controller pays {1}.\nDraw a card.').
card_image_name('disrupt'/'INV', 'disrupt').
card_uid('disrupt'/'INV', 'INV:Disrupt:disrupt').
card_rarity('disrupt'/'INV', 'Uncommon').
card_artist('disrupt'/'INV', 'Paolo Parente').
card_number('disrupt'/'INV', '51').
card_flavor_text('disrupt'/'INV', 'Teferi wanted no part of Urza\'s battle plans, but both agreed the Phyrexian portal must be closed.').
card_multiverse_id('disrupt'/'INV', '23000').

card_in_set('distorting wake', 'INV').
card_original_type('distorting wake'/'INV', 'Sorcery').
card_original_text('distorting wake'/'INV', 'Return X target nonland permanents to their owners\' hands.').
card_first_print('distorting wake', 'INV').
card_image_name('distorting wake'/'INV', 'distorting wake').
card_uid('distorting wake'/'INV', 'INV:Distorting Wake:distorting wake').
card_rarity('distorting wake'/'INV', 'Rare').
card_artist('distorting wake'/'INV', 'Arnie Swekel').
card_number('distorting wake'/'INV', '52').
card_flavor_text('distorting wake'/'INV', 'Gerrard savored a grim smile as the Phyrexian portals disappeared behind the Weatherlight.').
card_multiverse_id('distorting wake'/'INV', '23011').

card_in_set('divine presence', 'INV').
card_original_type('divine presence'/'INV', 'Enchantment').
card_original_text('divine presence'/'INV', 'If a source would deal 4 damage or more to a creature or player, that source deals 3 damage to that creature or player instead.').
card_first_print('divine presence', 'INV').
card_image_name('divine presence'/'INV', 'divine presence').
card_uid('divine presence'/'INV', 'INV:Divine Presence:divine presence').
card_rarity('divine presence'/'INV', 'Rare').
card_artist('divine presence'/'INV', 'Ron Spears').
card_number('divine presence'/'INV', '15').
card_flavor_text('divine presence'/'INV', '\"Serra\'s light isn\'t easily extinguished.\"\n—Reya Dawnbringer').
card_multiverse_id('divine presence'/'INV', '22969').

card_in_set('do or die', 'INV').
card_original_type('do or die'/'INV', 'Sorcery').
card_original_text('do or die'/'INV', 'Separate all creatures target player controls into two face-up piles. Destroy all creatures in the pile of that player\'s choice. They can\'t be regenerated.').
card_first_print('do or die', 'INV').
card_image_name('do or die'/'INV', 'do or die').
card_uid('do or die'/'INV', 'INV:Do or Die:do or die').
card_rarity('do or die'/'INV', 'Rare').
card_artist('do or die'/'INV', 'Christopher Moeller').
card_number('do or die'/'INV', '102').
card_multiverse_id('do or die'/'INV', '23059').

card_in_set('drake-skull cameo', 'INV').
card_original_type('drake-skull cameo'/'INV', 'Artifact').
card_original_text('drake-skull cameo'/'INV', '{T}: Add {U} or {B} to your mana pool.').
card_first_print('drake-skull cameo', 'INV').
card_image_name('drake-skull cameo'/'INV', 'drake-skull cameo').
card_uid('drake-skull cameo'/'INV', 'INV:Drake-Skull Cameo:drake-skull cameo').
card_rarity('drake-skull cameo'/'INV', 'Uncommon').
card_artist('drake-skull cameo'/'INV', 'Dan Frazier').
card_number('drake-skull cameo'/'INV', '302').
card_flavor_text('drake-skull cameo'/'INV', '\"A strange skull was turned up by an Ephran farmer\'s plow. I traded a copper ring for the ‘ox skull.\' It resonates of the sea and danger.\"\n—Isel, master carver').
card_multiverse_id('drake-skull cameo'/'INV', '23218').

card_in_set('dream thrush', 'INV').
card_original_type('dream thrush'/'INV', 'Creature — Bird').
card_original_text('dream thrush'/'INV', 'Flying\n{T}: Target land becomes a land of the basic land type of your choice until end of turn.').
card_first_print('dream thrush', 'INV').
card_image_name('dream thrush'/'INV', 'dream thrush').
card_uid('dream thrush'/'INV', 'INV:Dream Thrush:dream thrush').
card_rarity('dream thrush'/'INV', 'Common').
card_artist('dream thrush'/'INV', 'D. J. Cleland-Hura').
card_number('dream thrush'/'INV', '53').
card_flavor_text('dream thrush'/'INV', 'Whether for good or ill, their arrival always means change.').
card_multiverse_id('dream thrush'/'INV', '22979').

card_in_set('dredge', 'INV').
card_original_type('dredge'/'INV', 'Instant').
card_original_text('dredge'/'INV', 'Sacrifice a creature or land.\nDraw a card.').
card_first_print('dredge', 'INV').
card_image_name('dredge'/'INV', 'dredge').
card_uid('dredge'/'INV', 'INV:Dredge:dredge').
card_rarity('dredge'/'INV', 'Uncommon').
card_artist('dredge'/'INV', 'Donato Giancola').
card_number('dredge'/'INV', '103').
card_flavor_text('dredge'/'INV', '\"I\'d strip away the world to see what\'s under it, but I\'d never just leave it to these Phyrexian parasites and their false god.\"\n—Urborg witch').
card_multiverse_id('dredge'/'INV', '24124').

card_in_set('dromar\'s attendant', 'INV').
card_original_type('dromar\'s attendant'/'INV', 'Artifact Creature — Golem').
card_original_text('dromar\'s attendant'/'INV', '{1}, Sacrifice Dromar\'s Attendant: Add {W}{U}{B} to your mana pool.').
card_first_print('dromar\'s attendant', 'INV').
card_image_name('dromar\'s attendant'/'INV', 'dromar\'s attendant').
card_uid('dromar\'s attendant'/'INV', 'INV:Dromar\'s Attendant:dromar\'s attendant').
card_rarity('dromar\'s attendant'/'INV', 'Uncommon').
card_artist('dromar\'s attendant'/'INV', 'Carl Critchlow').
card_number('dromar\'s attendant'/'INV', '303').
card_flavor_text('dromar\'s attendant'/'INV', '\"Dromar is the wings of the ur-dragon, sweeping away all opposition.\"').
card_multiverse_id('dromar\'s attendant'/'INV', '25836').

card_in_set('dromar, the banisher', 'INV').
card_original_type('dromar, the banisher'/'INV', 'Creature — Dragon Legend').
card_original_text('dromar, the banisher'/'INV', 'Flying\nWhenever Dromar, the Banisher deals combat damage to a player, you may pay {2}{U}. If you do, choose a color. Return all creatures of that color to their owners\' hands.').
card_first_print('dromar, the banisher', 'INV').
card_image_name('dromar, the banisher'/'INV', 'dromar, the banisher').
card_uid('dromar, the banisher'/'INV', 'INV:Dromar, the Banisher:dromar, the banisher').
card_rarity('dromar, the banisher'/'INV', 'Rare').
card_artist('dromar, the banisher'/'INV', 'Dave Dorman').
card_number('dromar, the banisher'/'INV', '244').
card_multiverse_id('dromar, the banisher'/'INV', '23206').

card_in_set('dueling grounds', 'INV').
card_original_type('dueling grounds'/'INV', 'Enchantment').
card_original_text('dueling grounds'/'INV', 'No more than one creature may attack each turn.\nNo more than one creature may block each turn.').
card_first_print('dueling grounds', 'INV').
card_image_name('dueling grounds'/'INV', 'dueling grounds').
card_uid('dueling grounds'/'INV', 'INV:Dueling Grounds:dueling grounds').
card_rarity('dueling grounds'/'INV', 'Rare').
card_artist('dueling grounds'/'INV', 'Pete Venters').
card_number('dueling grounds'/'INV', '245').
card_flavor_text('dueling grounds'/'INV', '\"You think you can stop me?\" hissed Tsabo.\n\"I think I can kill you,\" replied Gerrard.').
card_multiverse_id('dueling grounds'/'INV', '25438').

card_in_set('duskwalker', 'INV').
card_original_type('duskwalker'/'INV', 'Creature — Minion').
card_original_text('duskwalker'/'INV', 'Kicker {3}{B} (You may pay an additional {3}{B} as you play this spell.)\nIf you paid the kicker cost, Duskwalker comes into play with two +1/+1 counters on it and has \"Duskwalker can\'t be blocked except by artifact creatures and/or black creatures.\"').
card_first_print('duskwalker', 'INV').
card_image_name('duskwalker'/'INV', 'duskwalker').
card_uid('duskwalker'/'INV', 'INV:Duskwalker:duskwalker').
card_rarity('duskwalker'/'INV', 'Common').
card_artist('duskwalker'/'INV', 'David Martin').
card_number('duskwalker'/'INV', '104').
card_multiverse_id('duskwalker'/'INV', '23019').

card_in_set('elfhame palace', 'INV').
card_original_type('elfhame palace'/'INV', 'Land').
card_original_text('elfhame palace'/'INV', 'Elfhame Palace comes into play tapped.\n{T}: Add {G} or {W} to your mana pool.').
card_first_print('elfhame palace', 'INV').
card_image_name('elfhame palace'/'INV', 'elfhame palace').
card_uid('elfhame palace'/'INV', 'INV:Elfhame Palace:elfhame palace').
card_rarity('elfhame palace'/'INV', 'Uncommon').
card_artist('elfhame palace'/'INV', 'Jerry Tiritilli').
card_number('elfhame palace'/'INV', '322').
card_flavor_text('elfhame palace'/'INV', 'Llanowar has seven elfhames, or kingdoms, each with its own ruler. Their palaces are objects of awe, wonder, and envy.').
card_multiverse_id('elfhame palace'/'INV', '23244').

card_in_set('elfhame sanctuary', 'INV').
card_original_type('elfhame sanctuary'/'INV', 'Enchantment').
card_original_text('elfhame sanctuary'/'INV', 'At the beginning of your upkeep, you may search your library for a basic land card, reveal that card, and put it into your hand. If you do, skip your draw step this turn and shuffle your library.').
card_first_print('elfhame sanctuary', 'INV').
card_image_name('elfhame sanctuary'/'INV', 'elfhame sanctuary').
card_uid('elfhame sanctuary'/'INV', 'INV:Elfhame Sanctuary:elfhame sanctuary').
card_rarity('elfhame sanctuary'/'INV', 'Uncommon').
card_artist('elfhame sanctuary'/'INV', 'Alan Rabinowitz').
card_number('elfhame sanctuary'/'INV', '185').
card_multiverse_id('elfhame sanctuary'/'INV', '23142').

card_in_set('elvish champion', 'INV').
card_original_type('elvish champion'/'INV', 'Creature — Lord').
card_original_text('elvish champion'/'INV', 'All Elves get +1/+1 and have forestwalk. (They\'re unblockable as long as defending player controls a forest.)').
card_image_name('elvish champion'/'INV', 'elvish champion').
card_uid('elvish champion'/'INV', 'INV:Elvish Champion:elvish champion').
card_rarity('elvish champion'/'INV', 'Rare').
card_artist('elvish champion'/'INV', 'D. Alexander Gregory').
card_number('elvish champion'/'INV', '186').
card_flavor_text('elvish champion'/'INV', '\"For what are leaves but countless blades\nTo fight a countless foe on high.\"\n—Skyshroud hymn').
card_multiverse_id('elvish champion'/'INV', '26445').

card_in_set('empress galina', 'INV').
card_original_type('empress galina'/'INV', 'Creature — Legend').
card_original_text('empress galina'/'INV', '{U}{U}, {T}: Gain control of target Legend or legendary permanent. (This effect doesn\'t end at end of turn.)').
card_first_print('empress galina', 'INV').
card_image_name('empress galina'/'INV', 'empress galina').
card_uid('empress galina'/'INV', 'INV:Empress Galina:empress galina').
card_rarity('empress galina'/'INV', 'Rare').
card_artist('empress galina'/'INV', 'Matt Cavotta').
card_number('empress galina'/'INV', '54').
card_flavor_text('empress galina'/'INV', '\"Above the waves you may be mighty indeed, but down here you belong to me.\"').
card_multiverse_id('empress galina'/'INV', '23007').

card_in_set('essence leak', 'INV').
card_original_type('essence leak'/'INV', 'Enchant Permanent').
card_original_text('essence leak'/'INV', 'If enchanted permanent is red or green, it has \"At the beginning of your upkeep, sacrifice this permanent unless you pay its mana cost.\"').
card_first_print('essence leak', 'INV').
card_image_name('essence leak'/'INV', 'essence leak').
card_uid('essence leak'/'INV', 'INV:Essence Leak:essence leak').
card_rarity('essence leak'/'INV', 'Uncommon').
card_artist('essence leak'/'INV', 'Adam Rex').
card_number('essence leak'/'INV', '55').
card_flavor_text('essence leak'/'INV', '\"We define the boundaries of reality; they don\'t define us.\"\n—Teferi').
card_multiverse_id('essence leak'/'INV', '22997').

card_in_set('exclude', 'INV').
card_original_type('exclude'/'INV', 'Instant').
card_original_text('exclude'/'INV', 'Counter target creature spell.\nDraw a card.').
card_first_print('exclude', 'INV').
card_image_name('exclude'/'INV', 'exclude').
card_uid('exclude'/'INV', 'INV:Exclude:exclude').
card_rarity('exclude'/'INV', 'Common').
card_artist('exclude'/'INV', 'Mark Romanoski').
card_number('exclude'/'INV', '56').
card_flavor_text('exclude'/'INV', '\"I don\'t have time for you right now.\"\n—Teferi').
card_multiverse_id('exclude'/'INV', '22986').

card_in_set('exotic curse', 'INV').
card_original_type('exotic curse'/'INV', 'Enchant Creature').
card_original_text('exotic curse'/'INV', 'Enchanted creature gets -1/-1 for each basic land type among lands you control.').
card_first_print('exotic curse', 'INV').
card_image_name('exotic curse'/'INV', 'exotic curse').
card_uid('exotic curse'/'INV', 'INV:Exotic Curse:exotic curse').
card_rarity('exotic curse'/'INV', 'Common').
card_artist('exotic curse'/'INV', 'Orizio Daniele').
card_number('exotic curse'/'INV', '105').
card_flavor_text('exotic curse'/'INV', 'Fouler than a necromancer\'s kiss.\n—Jamuraan expression').
card_multiverse_id('exotic curse'/'INV', '23032').

card_in_set('explosive growth', 'INV').
card_original_type('explosive growth'/'INV', 'Instant').
card_original_text('explosive growth'/'INV', 'Kicker {5} (You may pay an additional {5} as you play this spell.)\nTarget creature gets +2/+2 until end of turn. If you paid the kicker cost, that creature gets +5/+5 until end of turn instead.').
card_first_print('explosive growth', 'INV').
card_image_name('explosive growth'/'INV', 'explosive growth').
card_uid('explosive growth'/'INV', 'INV:Explosive Growth:explosive growth').
card_rarity('explosive growth'/'INV', 'Common').
card_artist('explosive growth'/'INV', 'Arnie Swekel').
card_number('explosive growth'/'INV', '187').
card_multiverse_id('explosive growth'/'INV', '23120').

card_in_set('fact or fiction', 'INV').
card_original_type('fact or fiction'/'INV', 'Instant').
card_original_text('fact or fiction'/'INV', 'Reveal the top five cards of your library. An opponent separates those cards into two face-up piles. Put one pile into your hand and the other into your graveyard.').
card_image_name('fact or fiction'/'INV', 'fact or fiction').
card_uid('fact or fiction'/'INV', 'INV:Fact or Fiction:fact or fiction').
card_rarity('fact or fiction'/'INV', 'Uncommon').
card_artist('fact or fiction'/'INV', 'Terese Nielsen').
card_number('fact or fiction'/'INV', '57').
card_multiverse_id('fact or fiction'/'INV', '22998').

card_in_set('faerie squadron', 'INV').
card_original_type('faerie squadron'/'INV', 'Creature — Faerie').
card_original_text('faerie squadron'/'INV', 'Kicker {3}{U} (You may pay an additional {3}{U} as you play this spell.)\nIf you paid the kicker cost, Faerie Squadron comes into play with two +1/+1 counters on it and has flying.').
card_first_print('faerie squadron', 'INV').
card_image_name('faerie squadron'/'INV', 'faerie squadron').
card_uid('faerie squadron'/'INV', 'INV:Faerie Squadron:faerie squadron').
card_rarity('faerie squadron'/'INV', 'Common').
card_artist('faerie squadron'/'INV', 'rk post').
card_number('faerie squadron'/'INV', '58').
card_multiverse_id('faerie squadron'/'INV', '22975').

card_in_set('fertile ground', 'INV').
card_original_type('fertile ground'/'INV', 'Enchant Land').
card_original_text('fertile ground'/'INV', 'Whenever enchanted land is tapped for mana, its controller adds one mana of any color to his or her mana pool.').
card_image_name('fertile ground'/'INV', 'fertile ground').
card_uid('fertile ground'/'INV', 'INV:Fertile Ground:fertile ground').
card_rarity('fertile ground'/'INV', 'Common').
card_artist('fertile ground'/'INV', 'Carl Critchlow').
card_number('fertile ground'/'INV', '188').
card_flavor_text('fertile ground'/'INV', 'As Phyrexians descended, Multani paused to reflect on the beauty that might never be seen again.').
card_multiverse_id('fertile ground'/'INV', '23114').

card_in_set('fight or flight', 'INV').
card_original_type('fight or flight'/'INV', 'Enchantment').
card_original_text('fight or flight'/'INV', 'At the beginning of each opponent\'s combat phase, separate all creatures that player controls into two face-up piles. Only creatures in the pile of his or her choice may attack this turn.').
card_first_print('fight or flight', 'INV').
card_image_name('fight or flight'/'INV', 'fight or flight').
card_uid('fight or flight'/'INV', 'INV:Fight or Flight:fight or flight').
card_rarity('fight or flight'/'INV', 'Rare').
card_artist('fight or flight'/'INV', 'Randy Gallegos').
card_number('fight or flight'/'INV', '16').
card_multiverse_id('fight or flight'/'INV', '22970').

card_in_set('firebrand ranger', 'INV').
card_original_type('firebrand ranger'/'INV', 'Creature — Soldier').
card_original_text('firebrand ranger'/'INV', '{G}, {T}: Put a basic land card from your hand into play.').
card_first_print('firebrand ranger', 'INV').
card_image_name('firebrand ranger'/'INV', 'firebrand ranger').
card_uid('firebrand ranger'/'INV', 'INV:Firebrand Ranger:firebrand ranger').
card_rarity('firebrand ranger'/'INV', 'Uncommon').
card_artist('firebrand ranger'/'INV', 'Quinton Hoover').
card_number('firebrand ranger'/'INV', '143').
card_flavor_text('firebrand ranger'/'INV', 'A skilled ranger can glance at the mud on your boots and tell where you last camped.').
card_multiverse_id('firebrand ranger'/'INV', '23083').

card_in_set('fires of yavimaya', 'INV').
card_original_type('fires of yavimaya'/'INV', 'Enchantment').
card_original_text('fires of yavimaya'/'INV', 'Creatures you control have haste. (They may attack and {T} the turn they come under your control.)\nSacrifice Fires of Yavimaya: Target creature gets +2/+2 until end of turn.').
card_first_print('fires of yavimaya', 'INV').
card_image_name('fires of yavimaya'/'INV', 'fires of yavimaya').
card_uid('fires of yavimaya'/'INV', 'INV:Fires of Yavimaya:fires of yavimaya').
card_rarity('fires of yavimaya'/'INV', 'Uncommon').
card_artist('fires of yavimaya'/'INV', 'Val Mayerik').
card_number('fires of yavimaya'/'INV', '246').
card_multiverse_id('fires of yavimaya'/'INV', '23180').

card_in_set('firescreamer', 'INV').
card_original_type('firescreamer'/'INV', 'Creature — Kavu').
card_original_text('firescreamer'/'INV', '{R} Firescreamer gets +1/+0 until end of turn.').
card_first_print('firescreamer', 'INV').
card_image_name('firescreamer'/'INV', 'firescreamer').
card_uid('firescreamer'/'INV', 'INV:Firescreamer:firescreamer').
card_rarity('firescreamer'/'INV', 'Common').
card_artist('firescreamer'/'INV', 'Alan Pollack').
card_number('firescreamer'/'INV', '106').
card_flavor_text('firescreamer'/'INV', 'In the dark, it\'s nearly invisible—until it exhales.').
card_multiverse_id('firescreamer'/'INV', '23017').

card_in_set('forest', 'INV').
card_original_type('forest'/'INV', 'Land').
card_original_text('forest'/'INV', 'G').
card_image_name('forest'/'INV', 'forest1').
card_uid('forest'/'INV', 'INV:Forest:forest1').
card_rarity('forest'/'INV', 'Basic Land').
card_artist('forest'/'INV', 'John Avon').
card_number('forest'/'INV', '347').
card_multiverse_id('forest'/'INV', '25967').

card_in_set('forest', 'INV').
card_original_type('forest'/'INV', 'Land').
card_original_text('forest'/'INV', 'G').
card_image_name('forest'/'INV', 'forest2').
card_uid('forest'/'INV', 'INV:Forest:forest2').
card_rarity('forest'/'INV', 'Basic Land').
card_artist('forest'/'INV', 'Alan Pollack').
card_number('forest'/'INV', '348').
card_multiverse_id('forest'/'INV', '26310').

card_in_set('forest', 'INV').
card_original_type('forest'/'INV', 'Land').
card_original_text('forest'/'INV', 'G').
card_image_name('forest'/'INV', 'forest3').
card_uid('forest'/'INV', 'INV:Forest:forest3').
card_rarity('forest'/'INV', 'Basic Land').
card_artist('forest'/'INV', 'Alan Pollack').
card_number('forest'/'INV', '349').
card_multiverse_id('forest'/'INV', '26311').

card_in_set('forest', 'INV').
card_original_type('forest'/'INV', 'Land').
card_original_text('forest'/'INV', 'G').
card_image_name('forest'/'INV', 'forest4').
card_uid('forest'/'INV', 'INV:Forest:forest4').
card_rarity('forest'/'INV', 'Basic Land').
card_artist('forest'/'INV', 'Glen Angus').
card_number('forest'/'INV', '350').
card_multiverse_id('forest'/'INV', '26312').

card_in_set('frenzied tilling', 'INV').
card_original_type('frenzied tilling'/'INV', 'Sorcery').
card_original_text('frenzied tilling'/'INV', 'Destroy target land. Search your library for a basic land card and put that card into play tapped. Then shuffle your library.').
card_first_print('frenzied tilling', 'INV').
card_image_name('frenzied tilling'/'INV', 'frenzied tilling').
card_uid('frenzied tilling'/'INV', 'INV:Frenzied Tilling:frenzied tilling').
card_rarity('frenzied tilling'/'INV', 'Common').
card_artist('frenzied tilling'/'INV', 'Mike Raabe').
card_number('frenzied tilling'/'INV', '247').
card_flavor_text('frenzied tilling'/'INV', '\"Beneath her scars, Dominaria\'s beauty yet shines.\"\n—Multani, maro-sorcerer').
card_multiverse_id('frenzied tilling'/'INV', '23156').

card_in_set('galina\'s knight', 'INV').
card_original_type('galina\'s knight'/'INV', 'Creature — Merfolk Knight').
card_original_text('galina\'s knight'/'INV', 'Protection from red').
card_first_print('galina\'s knight', 'INV').
card_image_name('galina\'s knight'/'INV', 'galina\'s knight').
card_uid('galina\'s knight'/'INV', 'INV:Galina\'s Knight:galina\'s knight').
card_rarity('galina\'s knight'/'INV', 'Common').
card_artist('galina\'s knight'/'INV', 'David Martin').
card_number('galina\'s knight'/'INV', '248').
card_flavor_text('galina\'s knight'/'INV', '\"Are they on our side?\" asked Sisay.\n\"If they kill Phyrexians they are,\" replied Gerrard.').
card_multiverse_id('galina\'s knight'/'INV', '23158').

card_in_set('geothermal crevice', 'INV').
card_original_type('geothermal crevice'/'INV', 'Land').
card_original_text('geothermal crevice'/'INV', 'Geothermal Crevice comes into play tapped.\n{T}: Add {R} to your mana pool.\n{T}, Sacrifice Geothermal Crevice: Add {B}{G} to your mana pool.').
card_first_print('geothermal crevice', 'INV').
card_image_name('geothermal crevice'/'INV', 'geothermal crevice').
card_uid('geothermal crevice'/'INV', 'INV:Geothermal Crevice:geothermal crevice').
card_rarity('geothermal crevice'/'INV', 'Common').
card_artist('geothermal crevice'/'INV', 'John Avon').
card_number('geothermal crevice'/'INV', '323').
card_multiverse_id('geothermal crevice'/'INV', '23238').

card_in_set('ghitu fire', 'INV').
card_original_type('ghitu fire'/'INV', 'Sorcery').
card_original_text('ghitu fire'/'INV', 'You may play Ghitu Fire any time you could play an instant if you pay {2} more to play it.\nGhitu Fire deals X damage to target creature or player.').
card_first_print('ghitu fire', 'INV').
card_image_name('ghitu fire'/'INV', 'ghitu fire').
card_uid('ghitu fire'/'INV', 'INV:Ghitu Fire:ghitu fire').
card_rarity('ghitu fire'/'INV', 'Rare').
card_artist('ghitu fire'/'INV', 'Glen Angus').
card_number('ghitu fire'/'INV', '144').
card_multiverse_id('ghitu fire'/'INV', '23086').

card_in_set('glimmering angel', 'INV').
card_original_type('glimmering angel'/'INV', 'Creature — Angel').
card_original_text('glimmering angel'/'INV', 'Flying\n{U} Glimmering Angel can\'t be the target of spells or abilities this turn.').
card_first_print('glimmering angel', 'INV').
card_image_name('glimmering angel'/'INV', 'glimmering angel').
card_uid('glimmering angel'/'INV', 'INV:Glimmering Angel:glimmering angel').
card_rarity('glimmering angel'/'INV', 'Common').
card_artist('glimmering angel'/'INV', 'Ciruelo').
card_number('glimmering angel'/'INV', '17').
card_flavor_text('glimmering angel'/'INV', '\"We turned to see where the blow came from and saw only a distant light.\"\n—Capashen lord').
card_multiverse_id('glimmering angel'/'INV', '22929').

card_in_set('global ruin', 'INV').
card_original_type('global ruin'/'INV', 'Sorcery').
card_original_text('global ruin'/'INV', 'Each player chooses from the lands he or she controls a land of each basic land type, then sacrifices the rest.').
card_first_print('global ruin', 'INV').
card_image_name('global ruin'/'INV', 'global ruin').
card_uid('global ruin'/'INV', 'INV:Global Ruin:global ruin').
card_rarity('global ruin'/'INV', 'Rare').
card_artist('global ruin'/'INV', 'Greg Staples').
card_number('global ruin'/'INV', '18').
card_flavor_text('global ruin'/'INV', '\"The earth shook, the sky rained fire, and it seemed the world was ending.\"\n—Benalish refugee').
card_multiverse_id('global ruin'/'INV', '22967').

card_in_set('goblin spy', 'INV').
card_original_type('goblin spy'/'INV', 'Creature — Goblin').
card_original_text('goblin spy'/'INV', 'Play with the top card of your library revealed.').
card_first_print('goblin spy', 'INV').
card_image_name('goblin spy'/'INV', 'goblin spy').
card_uid('goblin spy'/'INV', 'INV:Goblin Spy:goblin spy').
card_rarity('goblin spy'/'INV', 'Uncommon').
card_artist('goblin spy'/'INV', 'Scott M. Fischer').
card_number('goblin spy'/'INV', '145').
card_flavor_text('goblin spy'/'INV', '\"Isn\'t he on our side?\"\n\"Yep.\"\n\"Why\'s he spyin\' on us?\"\n\"Don\'t ask.\"').
card_multiverse_id('goblin spy'/'INV', '5680').

card_in_set('goham djinn', 'INV').
card_original_type('goham djinn'/'INV', 'Creature — Djinn').
card_original_text('goham djinn'/'INV', '{1}{B} Regenerate Goham Djinn.\nGoham Djinn gets -2/-2 as long as black is the most common color among all permanents or is tied for most common.').
card_first_print('goham djinn', 'INV').
card_image_name('goham djinn'/'INV', 'goham djinn').
card_uid('goham djinn'/'INV', 'INV:Goham Djinn:goham djinn').
card_rarity('goham djinn'/'INV', 'Uncommon').
card_artist('goham djinn'/'INV', 'Ron Spencer').
card_number('goham djinn'/'INV', '107').
card_multiverse_id('goham djinn'/'INV', '23037').

card_in_set('halam djinn', 'INV').
card_original_type('halam djinn'/'INV', 'Creature — Djinn').
card_original_text('halam djinn'/'INV', 'Haste (This creature may attack and {T} the turn it comes under your control.)\nHalam Djinn gets -2/-2 as long as red is the most common color among all permanents or is tied for most common.').
card_first_print('halam djinn', 'INV').
card_image_name('halam djinn'/'INV', 'halam djinn').
card_uid('halam djinn'/'INV', 'INV:Halam Djinn:halam djinn').
card_rarity('halam djinn'/'INV', 'Uncommon').
card_artist('halam djinn'/'INV', 'Adam Rex').
card_number('halam djinn'/'INV', '146').
card_multiverse_id('halam djinn'/'INV', '23082').

card_in_set('hanna, ship\'s navigator', 'INV').
card_original_type('hanna, ship\'s navigator'/'INV', 'Creature — Legend').
card_original_text('hanna, ship\'s navigator'/'INV', '{1}{W}{U}, {T}: Return target artifact or enchantment card from your graveyard to your hand.').
card_image_name('hanna, ship\'s navigator'/'INV', 'hanna, ship\'s navigator').
card_uid('hanna, ship\'s navigator'/'INV', 'INV:Hanna, Ship\'s Navigator:hanna, ship\'s navigator').
card_rarity('hanna, ship\'s navigator'/'INV', 'Rare').
card_artist('hanna, ship\'s navigator'/'INV', 'Dave Dorman').
card_number('hanna, ship\'s navigator'/'INV', '249').
card_flavor_text('hanna, ship\'s navigator'/'INV', '\"I never thought I\'d spend my life fighting. I\'m a maker, not a destroyer.\"').
card_multiverse_id('hanna, ship\'s navigator'/'INV', '25977').

card_in_set('harrow', 'INV').
card_original_type('harrow'/'INV', 'Instant').
card_original_text('harrow'/'INV', 'As an additional cost to play Harrow, sacrifice a land.\nSearch your library for up to two basic land cards and put them into play. Then shuffle your library.').
card_image_name('harrow'/'INV', 'harrow').
card_uid('harrow'/'INV', 'INV:Harrow:harrow').
card_rarity('harrow'/'INV', 'Common').
card_artist('harrow'/'INV', 'Rob Alexander').
card_number('harrow'/'INV', '189').
card_flavor_text('harrow'/'INV', 'No spot in nature is truly barren.').
card_multiverse_id('harrow'/'INV', '23131').

card_in_set('harsh judgment', 'INV').
card_original_type('harsh judgment'/'INV', 'Enchantment').
card_original_text('harsh judgment'/'INV', 'As Harsh Judgment comes into play, choose a color.\nIf an instant or sorcery of the chosen color would deal damage to you, it deals that damage to its controller instead.').
card_first_print('harsh judgment', 'INV').
card_image_name('harsh judgment'/'INV', 'harsh judgment').
card_uid('harsh judgment'/'INV', 'INV:Harsh Judgment:harsh judgment').
card_rarity('harsh judgment'/'INV', 'Rare').
card_artist('harsh judgment'/'INV', 'Carl Critchlow').
card_number('harsh judgment'/'INV', '19').
card_flavor_text('harsh judgment'/'INV', '\"At least they remember me.\"\n—Gerrard').
card_multiverse_id('harsh judgment'/'INV', '26282').

card_in_set('hate weaver', 'INV').
card_original_type('hate weaver'/'INV', 'Creature — Wizard').
card_original_text('hate weaver'/'INV', '{2}: Target blue or red creature gets +1/+0 until end of turn.').
card_first_print('hate weaver', 'INV').
card_image_name('hate weaver'/'INV', 'hate weaver').
card_uid('hate weaver'/'INV', 'INV:Hate Weaver:hate weaver').
card_rarity('hate weaver'/'INV', 'Uncommon').
card_artist('hate weaver'/'INV', 'Roger Raupp').
card_number('hate weaver'/'INV', '108').
card_flavor_text('hate weaver'/'INV', '\"Let my hate fuel your fury.\"').
card_multiverse_id('hate weaver'/'INV', '23041').

card_in_set('heroes\' reunion', 'INV').
card_original_type('heroes\' reunion'/'INV', 'Instant').
card_original_text('heroes\' reunion'/'INV', 'Target player gains 7 life.').
card_first_print('heroes\' reunion', 'INV').
card_image_name('heroes\' reunion'/'INV', 'heroes\' reunion').
card_uid('heroes\' reunion'/'INV', 'INV:Heroes\' Reunion:heroes\' reunion').
card_rarity('heroes\' reunion'/'INV', 'Uncommon').
card_artist('heroes\' reunion'/'INV', 'Terese Nielsen').
card_number('heroes\' reunion'/'INV', '250').
card_flavor_text('heroes\' reunion'/'INV', '\"You helped save my people from a Phyrexian fate. Did you think I wouldn\'t return the favor?\"\n—Eladamri, to Gerrard').
card_multiverse_id('heroes\' reunion'/'INV', '23183').

card_in_set('holy day', 'INV').
card_original_type('holy day'/'INV', 'Instant').
card_original_text('holy day'/'INV', 'Prevent all combat damage that would be dealt this turn.').
card_image_name('holy day'/'INV', 'holy day').
card_uid('holy day'/'INV', 'INV:Holy Day:holy day').
card_rarity('holy day'/'INV', 'Common').
card_artist('holy day'/'INV', 'Pete Venters').
card_number('holy day'/'INV', '20').
card_flavor_text('holy day'/'INV', '\"Will history remember this as the day demons arrived or the day saviors arrived?\"\n—Barrin').
card_multiverse_id('holy day'/'INV', '22940').

card_in_set('hooded kavu', 'INV').
card_original_type('hooded kavu'/'INV', 'Creature — Kavu').
card_original_text('hooded kavu'/'INV', '{B} Hooded Kavu can\'t be blocked this turn except by artifact creatures and/or black creatures.').
card_first_print('hooded kavu', 'INV').
card_image_name('hooded kavu'/'INV', 'hooded kavu').
card_uid('hooded kavu'/'INV', 'INV:Hooded Kavu:hooded kavu').
card_rarity('hooded kavu'/'INV', 'Common').
card_artist('hooded kavu'/'INV', 'John Howe').
card_number('hooded kavu'/'INV', '147').
card_flavor_text('hooded kavu'/'INV', 'The spread of its hood eclipses all hope.').
card_multiverse_id('hooded kavu'/'INV', '23061').

card_in_set('horned cheetah', 'INV').
card_original_type('horned cheetah'/'INV', 'Creature — Cat').
card_original_text('horned cheetah'/'INV', 'Whenever Horned Cheetah deals damage, you gain that much life.').
card_first_print('horned cheetah', 'INV').
card_image_name('horned cheetah'/'INV', 'horned cheetah').
card_uid('horned cheetah'/'INV', 'INV:Horned Cheetah:horned cheetah').
card_rarity('horned cheetah'/'INV', 'Uncommon').
card_artist('horned cheetah'/'INV', 'John Matson').
card_number('horned cheetah'/'INV', '251').
card_flavor_text('horned cheetah'/'INV', '\"I think she wants us to eat it,\" said Squee, staring at the oily carcass the cheetah had dragged to their fire.').
card_multiverse_id('horned cheetah'/'INV', '23163').

card_in_set('hunting kavu', 'INV').
card_original_type('hunting kavu'/'INV', 'Creature — Kavu').
card_original_text('hunting kavu'/'INV', '{1}{R}{G}, {T}: Remove from the game Hunting Kavu and target creature without flying that\'s attacking you.').
card_first_print('hunting kavu', 'INV').
card_image_name('hunting kavu'/'INV', 'hunting kavu').
card_uid('hunting kavu'/'INV', 'INV:Hunting Kavu:hunting kavu').
card_rarity('hunting kavu'/'INV', 'Uncommon').
card_artist('hunting kavu'/'INV', 'Scott M. Fischer').
card_number('hunting kavu'/'INV', '252').
card_flavor_text('hunting kavu'/'INV', 'Dominarians gladly fought alongside kavu . . . until the kavu figured out some Dominarians were quite tasty.').
card_multiverse_id('hunting kavu'/'INV', '23176').

card_in_set('hypnotic cloud', 'INV').
card_original_type('hypnotic cloud'/'INV', 'Sorcery').
card_original_text('hypnotic cloud'/'INV', 'Kicker {4} (You may pay an additional {4} as you play this spell.)\nTarget player discards a card from his or her hand. If you paid the kicker cost, that player discards three cards from his or her hand instead.').
card_first_print('hypnotic cloud', 'INV').
card_image_name('hypnotic cloud'/'INV', 'hypnotic cloud').
card_uid('hypnotic cloud'/'INV', 'INV:Hypnotic Cloud:hypnotic cloud').
card_rarity('hypnotic cloud'/'INV', 'Common').
card_artist('hypnotic cloud'/'INV', 'Randy Gallegos').
card_number('hypnotic cloud'/'INV', '109').
card_multiverse_id('hypnotic cloud'/'INV', '23033').

card_in_set('irrigation ditch', 'INV').
card_original_type('irrigation ditch'/'INV', 'Land').
card_original_text('irrigation ditch'/'INV', 'Irrigation Ditch comes into play tapped.\n{T}: Add {W} to your mana pool.\n{T}, Sacrifice Irrigation Ditch: Add {G}{U} to your mana pool.').
card_first_print('irrigation ditch', 'INV').
card_image_name('irrigation ditch'/'INV', 'irrigation ditch').
card_uid('irrigation ditch'/'INV', 'INV:Irrigation Ditch:irrigation ditch').
card_rarity('irrigation ditch'/'INV', 'Common').
card_artist('irrigation ditch'/'INV', 'Rob Alexander').
card_number('irrigation ditch'/'INV', '324').
card_multiverse_id('irrigation ditch'/'INV', '23235').

card_in_set('island', 'INV').
card_original_type('island'/'INV', 'Land').
card_original_text('island'/'INV', 'U').
card_image_name('island'/'INV', 'island1').
card_uid('island'/'INV', 'INV:Island:island1').
card_rarity('island'/'INV', 'Basic Land').
card_artist('island'/'INV', 'Tony Szczudlo').
card_number('island'/'INV', '335').
card_multiverse_id('island'/'INV', '25964').

card_in_set('island', 'INV').
card_original_type('island'/'INV', 'Land').
card_original_text('island'/'INV', 'U').
card_image_name('island'/'INV', 'island2').
card_uid('island'/'INV', 'INV:Island:island2').
card_rarity('island'/'INV', 'Basic Land').
card_artist('island'/'INV', 'John Avon').
card_number('island'/'INV', '336').
card_multiverse_id('island'/'INV', '26301').

card_in_set('island', 'INV').
card_original_type('island'/'INV', 'Land').
card_original_text('island'/'INV', 'U').
card_image_name('island'/'INV', 'island3').
card_uid('island'/'INV', 'INV:Island:island3').
card_rarity('island'/'INV', 'Basic Land').
card_artist('island'/'INV', 'Terese Nielsen').
card_number('island'/'INV', '337').
card_multiverse_id('island'/'INV', '26302').

card_in_set('island', 'INV').
card_original_type('island'/'INV', 'Land').
card_original_text('island'/'INV', 'U').
card_image_name('island'/'INV', 'island4').
card_uid('island'/'INV', 'INV:Island:island4').
card_rarity('island'/'INV', 'Basic Land').
card_artist('island'/'INV', 'Darrell Riche').
card_number('island'/'INV', '338').
card_multiverse_id('island'/'INV', '26303').

card_in_set('jade leech', 'INV').
card_original_type('jade leech'/'INV', 'Creature — Leech').
card_original_text('jade leech'/'INV', 'Green spells you play cost {G} more to play.').
card_first_print('jade leech', 'INV').
card_image_name('jade leech'/'INV', 'jade leech').
card_uid('jade leech'/'INV', 'INV:Jade Leech:jade leech').
card_rarity('jade leech'/'INV', 'Rare').
card_artist('jade leech'/'INV', 'John Howe').
card_number('jade leech'/'INV', '190').
card_flavor_text('jade leech'/'INV', '\"It took magic to extract one of the stones and five people to carry it.\"\n—Tolarian research notes').
card_multiverse_id('jade leech'/'INV', '23140').

card_in_set('juntu stakes', 'INV').
card_original_type('juntu stakes'/'INV', 'Artifact').
card_original_text('juntu stakes'/'INV', 'Creatures with power 1 or less don\'t untap during their controllers\' untap steps.').
card_first_print('juntu stakes', 'INV').
card_image_name('juntu stakes'/'INV', 'juntu stakes').
card_uid('juntu stakes'/'INV', 'INV:Juntu Stakes:juntu stakes').
card_rarity('juntu stakes'/'INV', 'Rare').
card_artist('juntu stakes'/'INV', 'Mark Brill').
card_number('juntu stakes'/'INV', '304').
card_flavor_text('juntu stakes'/'INV', '\"Those who can\'t survive are of no consequence.\"\n—Tsabo Tavoc').
card_multiverse_id('juntu stakes'/'INV', '23223').

card_in_set('kangee, aerie keeper', 'INV').
card_original_type('kangee, aerie keeper'/'INV', 'Creature — Legend').
card_original_text('kangee, aerie keeper'/'INV', 'Kicker {2}{X} (You may pay an additional {2}{X} as you play this spell.)\nFlying\nWhen Kangee, Aerie Keeper comes into play, if you paid the kicker cost, put X feather counters on it.\nAll Birds get +1/+1 for each feather counter on Kangee, Aerie Keeper.').
card_first_print('kangee, aerie keeper', 'INV').
card_image_name('kangee, aerie keeper'/'INV', 'kangee, aerie keeper').
card_uid('kangee, aerie keeper'/'INV', 'INV:Kangee, Aerie Keeper:kangee, aerie keeper').
card_rarity('kangee, aerie keeper'/'INV', 'Rare').
card_artist('kangee, aerie keeper'/'INV', 'Mark Romanoski').
card_number('kangee, aerie keeper'/'INV', '253').
card_multiverse_id('kangee, aerie keeper'/'INV', '23186').

card_in_set('kavu aggressor', 'INV').
card_original_type('kavu aggressor'/'INV', 'Creature — Kavu').
card_original_text('kavu aggressor'/'INV', 'Kicker {4} (You may pay an additional {4} as you play this spell.)\nKavu Aggressor can\'t block.\nIf you paid the kicker cost, Kavu Aggressor comes into play with a +1/+1 counter on it.').
card_first_print('kavu aggressor', 'INV').
card_image_name('kavu aggressor'/'INV', 'kavu aggressor').
card_uid('kavu aggressor'/'INV', 'INV:Kavu Aggressor:kavu aggressor').
card_rarity('kavu aggressor'/'INV', 'Common').
card_artist('kavu aggressor'/'INV', 'Christopher Moeller').
card_number('kavu aggressor'/'INV', '148').
card_multiverse_id('kavu aggressor'/'INV', '23063').

card_in_set('kavu chameleon', 'INV').
card_original_type('kavu chameleon'/'INV', 'Creature — Kavu').
card_original_text('kavu chameleon'/'INV', 'Kavu Chameleon can\'t be countered.\n{G} Kavu Chameleon becomes the color of your choice until end of turn.').
card_first_print('kavu chameleon', 'INV').
card_image_name('kavu chameleon'/'INV', 'kavu chameleon').
card_uid('kavu chameleon'/'INV', 'INV:Kavu Chameleon:kavu chameleon').
card_rarity('kavu chameleon'/'INV', 'Uncommon').
card_artist('kavu chameleon'/'INV', 'John Howe').
card_number('kavu chameleon'/'INV', '191').
card_flavor_text('kavu chameleon'/'INV', '\"They weren\'t part of my plan, but Gaea insisted.\"\n—Urza').
card_multiverse_id('kavu chameleon'/'INV', '23125').

card_in_set('kavu climber', 'INV').
card_original_type('kavu climber'/'INV', 'Creature — Kavu').
card_original_text('kavu climber'/'INV', 'When Kavu Climber comes into play, draw a card.').
card_first_print('kavu climber', 'INV').
card_image_name('kavu climber'/'INV', 'kavu climber').
card_uid('kavu climber'/'INV', 'INV:Kavu Climber:kavu climber').
card_rarity('kavu climber'/'INV', 'Common').
card_artist('kavu climber'/'INV', 'Rob Alexander').
card_number('kavu climber'/'INV', '192').
card_flavor_text('kavu climber'/'INV', 'The appearance of the first kavu surprised Multani. As they continued to emerge, he no longer had any doubts about Yavimaya\'s ability to defend herself.').
card_multiverse_id('kavu climber'/'INV', '23109').

card_in_set('kavu lair', 'INV').
card_original_type('kavu lair'/'INV', 'Enchantment').
card_original_text('kavu lair'/'INV', 'Whenever a creature with power 4 or greater comes into play, its controller draws a card.').
card_first_print('kavu lair', 'INV').
card_image_name('kavu lair'/'INV', 'kavu lair').
card_uid('kavu lair'/'INV', 'INV:Kavu Lair:kavu lair').
card_rarity('kavu lair'/'INV', 'Rare').
card_artist('kavu lair'/'INV', 'Chippy').
card_number('kavu lair'/'INV', '193').
card_flavor_text('kavu lair'/'INV', 'As a dark rain of Phyrexians fell from the sky, fountains of kavu erupted from the ground.').
card_multiverse_id('kavu lair'/'INV', '23144').

card_in_set('kavu monarch', 'INV').
card_original_type('kavu monarch'/'INV', 'Creature — Kavu').
card_original_text('kavu monarch'/'INV', 'All Kavu have trample.\nWhenever another Kavu comes into play, put a +1/+1 counter on Kavu Monarch.').
card_first_print('kavu monarch', 'INV').
card_image_name('kavu monarch'/'INV', 'kavu monarch').
card_uid('kavu monarch'/'INV', 'INV:Kavu Monarch:kavu monarch').
card_rarity('kavu monarch'/'INV', 'Rare').
card_artist('kavu monarch'/'INV', 'Terese Nielsen').
card_number('kavu monarch'/'INV', '149').
card_flavor_text('kavu monarch'/'INV', 'Kavu emerged all across Dominaria, but the first and the strongest came from Yavimaya.').
card_multiverse_id('kavu monarch'/'INV', '23097').

card_in_set('kavu runner', 'INV').
card_original_type('kavu runner'/'INV', 'Creature — Kavu').
card_original_text('kavu runner'/'INV', 'Kavu Runner has haste as long as no opponent controls a white or blue creature. (It may attack and {T} the turn it comes under your control.)').
card_first_print('kavu runner', 'INV').
card_image_name('kavu runner'/'INV', 'kavu runner').
card_uid('kavu runner'/'INV', 'INV:Kavu Runner:kavu runner').
card_rarity('kavu runner'/'INV', 'Uncommon').
card_artist('kavu runner'/'INV', 'Douglas Shuler').
card_number('kavu runner'/'INV', '150').
card_multiverse_id('kavu runner'/'INV', '23079').

card_in_set('kavu scout', 'INV').
card_original_type('kavu scout'/'INV', 'Creature — Kavu').
card_original_text('kavu scout'/'INV', 'Kavu Scout gets +1/+0 for each basic land type among lands you control.').
card_first_print('kavu scout', 'INV').
card_image_name('kavu scout'/'INV', 'kavu scout').
card_uid('kavu scout'/'INV', 'INV:Kavu Scout:kavu scout').
card_rarity('kavu scout'/'INV', 'Common').
card_artist('kavu scout'/'INV', 'DiTerlizzi').
card_number('kavu scout'/'INV', '151').
card_flavor_text('kavu scout'/'INV', 'The kavu evolved for centuries deep inside Dominaria, and some can draw from its strength.').
card_multiverse_id('kavu scout'/'INV', '23323').

card_in_set('kavu titan', 'INV').
card_original_type('kavu titan'/'INV', 'Creature — Kavu').
card_original_text('kavu titan'/'INV', 'Kicker {2}{G} (You may pay an additional {2}{G} as you play this spell.)\nIf you paid the kicker cost, Kavu Titan comes into play with three +1/+1 counters on it and has trample.').
card_first_print('kavu titan', 'INV').
card_image_name('kavu titan'/'INV', 'kavu titan').
card_uid('kavu titan'/'INV', 'INV:Kavu Titan:kavu titan').
card_rarity('kavu titan'/'INV', 'Rare').
card_artist('kavu titan'/'INV', 'Todd Lockwood').
card_number('kavu titan'/'INV', '194').
card_multiverse_id('kavu titan'/'INV', '23106').

card_in_set('keldon necropolis', 'INV').
card_original_type('keldon necropolis'/'INV', 'Legendary Land').
card_original_text('keldon necropolis'/'INV', '{T}: Add one colorless mana to your mana pool.\n{4}{R}, {T}, Sacrifice a creature: Keldon Necropolis deals 2 damage to target creature or player.').
card_first_print('keldon necropolis', 'INV').
card_image_name('keldon necropolis'/'INV', 'keldon necropolis').
card_uid('keldon necropolis'/'INV', 'INV:Keldon Necropolis:keldon necropolis').
card_rarity('keldon necropolis'/'INV', 'Rare').
card_artist('keldon necropolis'/'INV', 'Franz Vohwinkel').
card_number('keldon necropolis'/'INV', '325').
card_multiverse_id('keldon necropolis'/'INV', '25930').

card_in_set('liberate', 'INV').
card_original_type('liberate'/'INV', 'Instant').
card_original_text('liberate'/'INV', 'Remove target creature you control from the game. At end of turn, return that card to play under its owner\'s control.').
card_first_print('liberate', 'INV').
card_image_name('liberate'/'INV', 'liberate').
card_uid('liberate'/'INV', 'INV:Liberate:liberate').
card_rarity('liberate'/'INV', 'Uncommon').
card_artist('liberate'/'INV', 'Alan Pollack').
card_number('liberate'/'INV', '21').
card_flavor_text('liberate'/'INV', '\"Not everyone gets a second chance.\"\n—Hanna').
card_multiverse_id('liberate'/'INV', '22953').

card_in_set('lightning dart', 'INV').
card_original_type('lightning dart'/'INV', 'Instant').
card_original_text('lightning dart'/'INV', 'Lightning Dart deals 1 damage to target creature. If that creature is white or blue, Lightning Dart deals 4 damage to it instead.').
card_first_print('lightning dart', 'INV').
card_image_name('lightning dart'/'INV', 'lightning dart').
card_uid('lightning dart'/'INV', 'INV:Lightning Dart:lightning dart').
card_rarity('lightning dart'/'INV', 'Uncommon').
card_artist('lightning dart'/'INV', 'Arnie Swekel').
card_number('lightning dart'/'INV', '152').
card_flavor_text('lightning dart'/'INV', 'Broken and punctured by dozens of Phyrexian portals, the angry sky began to spit lightning back at the world.').
card_multiverse_id('lightning dart'/'INV', '23088').

card_in_set('llanowar cavalry', 'INV').
card_original_type('llanowar cavalry'/'INV', 'Creature — Soldier').
card_original_text('llanowar cavalry'/'INV', '{W} Attacking doesn\'t cause Llanowar Cavalry to tap this turn.').
card_first_print('llanowar cavalry', 'INV').
card_image_name('llanowar cavalry'/'INV', 'llanowar cavalry').
card_uid('llanowar cavalry'/'INV', 'INV:Llanowar Cavalry:llanowar cavalry').
card_rarity('llanowar cavalry'/'INV', 'Common').
card_artist('llanowar cavalry'/'INV', 'Eric Peterson').
card_number('llanowar cavalry'/'INV', '195').
card_flavor_text('llanowar cavalry'/'INV', 'For the first time, elves welcomed Benalish soldiers into the forest with something other than arrows.').
card_multiverse_id('llanowar cavalry'/'INV', '23322').

card_in_set('llanowar elite', 'INV').
card_original_type('llanowar elite'/'INV', 'Creature — Elf').
card_original_text('llanowar elite'/'INV', 'Kicker {8} (You may pay an additional {8} as you play this spell.)\nTrample\nIf you paid the kicker cost, Llanowar Elite comes into play with five +1/+1 counters on it.').
card_first_print('llanowar elite', 'INV').
card_image_name('llanowar elite'/'INV', 'llanowar elite').
card_uid('llanowar elite'/'INV', 'INV:Llanowar Elite:llanowar elite').
card_rarity('llanowar elite'/'INV', 'Common').
card_artist('llanowar elite'/'INV', 'Kev Walker').
card_number('llanowar elite'/'INV', '196').
card_multiverse_id('llanowar elite'/'INV', '25363').

card_in_set('llanowar knight', 'INV').
card_original_type('llanowar knight'/'INV', 'Creature — Elf Knight').
card_original_text('llanowar knight'/'INV', 'Protection from black').
card_first_print('llanowar knight', 'INV').
card_image_name('llanowar knight'/'INV', 'llanowar knight').
card_uid('llanowar knight'/'INV', 'INV:Llanowar Knight:llanowar knight').
card_rarity('llanowar knight'/'INV', 'Common').
card_artist('llanowar knight'/'INV', 'Heather Hudson').
card_number('llanowar knight'/'INV', '254').
card_flavor_text('llanowar knight'/'INV', 'Her armor and steed were borrowed, but her courage was hers alone.').
card_multiverse_id('llanowar knight'/'INV', '23157').

card_in_set('llanowar vanguard', 'INV').
card_original_type('llanowar vanguard'/'INV', 'Creature — Dryad').
card_original_text('llanowar vanguard'/'INV', '{T}: Llanowar Vanguard gets +0/+4 until end of turn.').
card_first_print('llanowar vanguard', 'INV').
card_image_name('llanowar vanguard'/'INV', 'llanowar vanguard').
card_uid('llanowar vanguard'/'INV', 'INV:Llanowar Vanguard:llanowar vanguard').
card_rarity('llanowar vanguard'/'INV', 'Common').
card_artist('llanowar vanguard'/'INV', 'Greg & Tim Hildebrandt').
card_number('llanowar vanguard'/'INV', '197').
card_flavor_text('llanowar vanguard'/'INV', 'Llanowar rallied around Eladamri\'s banner and united in his name.').
card_multiverse_id('llanowar vanguard'/'INV', '23123').

card_in_set('loafing giant', 'INV').
card_original_type('loafing giant'/'INV', 'Creature — Giant').
card_original_text('loafing giant'/'INV', 'Whenever Loafing Giant attacks or blocks, put the top card of your library into your graveyard. If that card is a land card, prevent all combat damage that Loafing Giant would deal this turn.').
card_first_print('loafing giant', 'INV').
card_image_name('loafing giant'/'INV', 'loafing giant').
card_uid('loafing giant'/'INV', 'INV:Loafing Giant:loafing giant').
card_rarity('loafing giant'/'INV', 'Rare').
card_artist('loafing giant'/'INV', 'Greg & Tim Hildebrandt').
card_number('loafing giant'/'INV', '153').
card_flavor_text('loafing giant'/'INV', '\"Forget it. I\'m not getting up today.\"').
card_multiverse_id('loafing giant'/'INV', '23092').

card_in_set('lobotomy', 'INV').
card_original_type('lobotomy'/'INV', 'Sorcery').
card_original_text('lobotomy'/'INV', 'Look at target player\'s hand and choose a card other than a basic land card from it. Search that player\'s graveyard, hand, and library for all cards with the same name as the chosen card and remove them from the game. Then that player shuffles his or her library.').
card_image_name('lobotomy'/'INV', 'lobotomy').
card_uid('lobotomy'/'INV', 'INV:Lobotomy:lobotomy').
card_rarity('lobotomy'/'INV', 'Uncommon').
card_artist('lobotomy'/'INV', 'D. Alexander Gregory').
card_number('lobotomy'/'INV', '255').
card_multiverse_id('lobotomy'/'INV', '23170').

card_in_set('lotus guardian', 'INV').
card_original_type('lotus guardian'/'INV', 'Artifact Creature').
card_original_text('lotus guardian'/'INV', 'Flying\n{T}: Add one mana of any color to your mana pool.').
card_first_print('lotus guardian', 'INV').
card_image_name('lotus guardian'/'INV', 'lotus guardian').
card_uid('lotus guardian'/'INV', 'INV:Lotus Guardian:lotus guardian').
card_rarity('lotus guardian'/'INV', 'Rare').
card_artist('lotus guardian'/'INV', 'Dana Knutson').
card_number('lotus guardian'/'INV', '305').
card_flavor_text('lotus guardian'/'INV', 'Lotus fields are too valuable to leave undefended.').
card_multiverse_id('lotus guardian'/'INV', '23233').

card_in_set('mages\' contest', 'INV').
card_original_type('mages\' contest'/'INV', 'Instant').
card_original_text('mages\' contest'/'INV', 'You and target spell\'s controller bid life. You start the bidding with a high bid of 1. In turn order, each player may top the high bid. The bidding ends when the high bid stands. The highest bidder loses life equal to the high bid. If you win the bidding, counter that spell.').
card_first_print('mages\' contest', 'INV').
card_image_name('mages\' contest'/'INV', 'mages\' contest').
card_uid('mages\' contest'/'INV', 'INV:Mages\' Contest:mages\' contest').
card_rarity('mages\' contest'/'INV', 'Rare').
card_artist('mages\' contest'/'INV', 'Bradley Williams').
card_number('mages\' contest'/'INV', '154').
card_multiverse_id('mages\' contest'/'INV', '23102').

card_in_set('malice', 'INV').
card_original_type('malice'/'INV', 'Instant').
card_original_text('malice'/'INV', 'Destroy target nonblack creature. It can\'t be regenerated.').
card_first_print('malice', 'INV').
card_image_name('malice'/'INV', 'spitemalice').
card_uid('malice'/'INV', 'INV:Malice:spitemalice').
card_rarity('malice'/'INV', 'Uncommon').
card_artist('malice'/'INV', 'David Martin').
card_number('malice'/'INV', '293b').
card_multiverse_id('malice'/'INV', '20576').

card_in_set('mana maze', 'INV').
card_original_type('mana maze'/'INV', 'Enchantment').
card_original_text('mana maze'/'INV', 'Players can\'t play spells that share a color with the spell last played this turn.').
card_first_print('mana maze', 'INV').
card_image_name('mana maze'/'INV', 'mana maze').
card_uid('mana maze'/'INV', 'INV:Mana Maze:mana maze').
card_rarity('mana maze'/'INV', 'Rare').
card_artist('mana maze'/'INV', 'Rebecca Guay').
card_number('mana maze'/'INV', '59').
card_flavor_text('mana maze'/'INV', '\"Those who know only one path to victory can never hope to triumph.\"\n—The Blind Seer').
card_multiverse_id('mana maze'/'INV', '23009').

card_in_set('maniacal rage', 'INV').
card_original_type('maniacal rage'/'INV', 'Enchant Creature').
card_original_text('maniacal rage'/'INV', 'Enchanted creature gets +2/+2 and can\'t block.').
card_image_name('maniacal rage'/'INV', 'maniacal rage').
card_uid('maniacal rage'/'INV', 'INV:Maniacal Rage:maniacal rage').
card_rarity('maniacal rage'/'INV', 'Common').
card_artist('maniacal rage'/'INV', 'Matt Cavotta').
card_number('maniacal rage'/'INV', '155').
card_flavor_text('maniacal rage'/'INV', '\"Remember me?\" Tahngarth roared. \"Now I\'ll repay you for my stay on Rath\"').
card_multiverse_id('maniacal rage'/'INV', '23072').

card_in_set('manipulate fate', 'INV').
card_original_type('manipulate fate'/'INV', 'Sorcery').
card_original_text('manipulate fate'/'INV', 'Search your library for three cards, remove them from the game, then shuffle your library.\nDraw a card.').
card_first_print('manipulate fate', 'INV').
card_image_name('manipulate fate'/'INV', 'manipulate fate').
card_uid('manipulate fate'/'INV', 'INV:Manipulate Fate:manipulate fate').
card_rarity('manipulate fate'/'INV', 'Uncommon').
card_artist('manipulate fate'/'INV', 'John Matson').
card_number('manipulate fate'/'INV', '60').
card_multiverse_id('manipulate fate'/'INV', '23003').

card_in_set('marauding knight', 'INV').
card_original_type('marauding knight'/'INV', 'Creature — Knight').
card_original_text('marauding knight'/'INV', 'Protection from white\nMarauding Knight gets +1/+1 for each plains your opponents control.').
card_first_print('marauding knight', 'INV').
card_image_name('marauding knight'/'INV', 'marauding knight').
card_uid('marauding knight'/'INV', 'INV:Marauding Knight:marauding knight').
card_rarity('marauding knight'/'INV', 'Rare').
card_artist('marauding knight'/'INV', 'Daren Bader').
card_number('marauding knight'/'INV', '110').
card_flavor_text('marauding knight'/'INV', '\"They ride our trails as if they were designed to,\" said the guard.\n\"They were,\" replied Gerrard.').
card_multiverse_id('marauding knight'/'INV', '23053').

card_in_set('metathran aerostat', 'INV').
card_original_type('metathran aerostat'/'INV', 'Creature — Ship').
card_original_text('metathran aerostat'/'INV', 'Flying\n{X}{U} You may put a creature card with converted mana cost X from your hand into play. If you do, return Metathran Aerostat to its owner\'s hand.').
card_first_print('metathran aerostat', 'INV').
card_image_name('metathran aerostat'/'INV', 'metathran aerostat').
card_uid('metathran aerostat'/'INV', 'INV:Metathran Aerostat:metathran aerostat').
card_rarity('metathran aerostat'/'INV', 'Rare').
card_artist('metathran aerostat'/'INV', 'Greg Staples').
card_number('metathran aerostat'/'INV', '61').
card_multiverse_id('metathran aerostat'/'INV', '23004').

card_in_set('metathran transport', 'INV').
card_original_type('metathran transport'/'INV', 'Creature — Ship').
card_original_text('metathran transport'/'INV', 'Flying\nMetathran Transport can\'t be blocked by blue creatures.\n{U} Target creature becomes blue until end of turn.').
card_first_print('metathran transport', 'INV').
card_image_name('metathran transport'/'INV', 'metathran transport').
card_uid('metathran transport'/'INV', 'INV:Metathran Transport:metathran transport').
card_rarity('metathran transport'/'INV', 'Uncommon').
card_artist('metathran transport'/'INV', 'Glen Angus').
card_number('metathran transport'/'INV', '62').
card_multiverse_id('metathran transport'/'INV', '26379').

card_in_set('metathran zombie', 'INV').
card_original_type('metathran zombie'/'INV', 'Creature — Zombie').
card_original_text('metathran zombie'/'INV', '{B} Regenerate Metathran Zombie.').
card_first_print('metathran zombie', 'INV').
card_image_name('metathran zombie'/'INV', 'metathran zombie').
card_uid('metathran zombie'/'INV', 'INV:Metathran Zombie:metathran zombie').
card_rarity('metathran zombie'/'INV', 'Common').
card_artist('metathran zombie'/'INV', 'Arnie Swekel').
card_number('metathran zombie'/'INV', '63').
card_flavor_text('metathran zombie'/'INV', '\"Rise, metathran. You shall serve a nobler cause in death.\"\n—Tsabo Tavoc, Phyrexian general').
card_multiverse_id('metathran zombie'/'INV', '22973').

card_in_set('meteor storm', 'INV').
card_original_type('meteor storm'/'INV', 'Enchantment').
card_original_text('meteor storm'/'INV', '{2}{R}{G}, Discard two cards at random from your hand: Meteor Storm deals 4 damage to target creature or player.').
card_first_print('meteor storm', 'INV').
card_image_name('meteor storm'/'INV', 'meteor storm').
card_uid('meteor storm'/'INV', 'INV:Meteor Storm:meteor storm').
card_rarity('meteor storm'/'INV', 'Rare').
card_artist('meteor storm'/'INV', 'John Avon').
card_number('meteor storm'/'INV', '256').
card_flavor_text('meteor storm'/'INV', 'The storm wasn\'t an omen. It was the fulfillment of one.').
card_multiverse_id('meteor storm'/'INV', '23193').

card_in_set('might weaver', 'INV').
card_original_type('might weaver'/'INV', 'Creature — Wizard').
card_original_text('might weaver'/'INV', '{2}: Target red or white creature gains trample until end of turn.').
card_first_print('might weaver', 'INV').
card_image_name('might weaver'/'INV', 'might weaver').
card_uid('might weaver'/'INV', 'INV:Might Weaver:might weaver').
card_rarity('might weaver'/'INV', 'Uncommon').
card_artist('might weaver'/'INV', 'Larry Elmore').
card_number('might weaver'/'INV', '198').
card_flavor_text('might weaver'/'INV', '\"Let my strength harden your resolve.\"').
card_multiverse_id('might weaver'/'INV', '23124').

card_in_set('molimo, maro-sorcerer', 'INV').
card_original_type('molimo, maro-sorcerer'/'INV', 'Creature — Legend').
card_original_text('molimo, maro-sorcerer'/'INV', 'Trample\nMolimo, Maro-Sorcerer\'s power and toughness are each equal to the number of lands you control.').
card_first_print('molimo, maro-sorcerer', 'INV').
card_image_name('molimo, maro-sorcerer'/'INV', 'molimo, maro-sorcerer').
card_uid('molimo, maro-sorcerer'/'INV', 'INV:Molimo, Maro-Sorcerer:molimo, maro-sorcerer').
card_rarity('molimo, maro-sorcerer'/'INV', 'Rare').
card_artist('molimo, maro-sorcerer'/'INV', 'Mark Zug').
card_number('molimo, maro-sorcerer'/'INV', '199').
card_flavor_text('molimo, maro-sorcerer'/'INV', '\"The world calls and I answer.\"').
card_multiverse_id('molimo, maro-sorcerer'/'INV', '23138').

card_in_set('mountain', 'INV').
card_original_type('mountain'/'INV', 'Land').
card_original_text('mountain'/'INV', 'R').
card_image_name('mountain'/'INV', 'mountain1').
card_uid('mountain'/'INV', 'INV:Mountain:mountain1').
card_rarity('mountain'/'INV', 'Basic Land').
card_artist('mountain'/'INV', 'Matt Cavotta').
card_number('mountain'/'INV', '343').
card_multiverse_id('mountain'/'INV', '25966').

card_in_set('mountain', 'INV').
card_original_type('mountain'/'INV', 'Land').
card_original_text('mountain'/'INV', 'R').
card_image_name('mountain'/'INV', 'mountain2').
card_uid('mountain'/'INV', 'INV:Mountain:mountain2').
card_rarity('mountain'/'INV', 'Basic Land').
card_artist('mountain'/'INV', 'Jeff Miracola').
card_number('mountain'/'INV', '344').
card_multiverse_id('mountain'/'INV', '26307').

card_in_set('mountain', 'INV').
card_original_type('mountain'/'INV', 'Land').
card_original_text('mountain'/'INV', 'R').
card_image_name('mountain'/'INV', 'mountain3').
card_uid('mountain'/'INV', 'INV:Mountain:mountain3').
card_rarity('mountain'/'INV', 'Basic Land').
card_artist('mountain'/'INV', 'Glen Angus').
card_number('mountain'/'INV', '345').
card_multiverse_id('mountain'/'INV', '26308').

card_in_set('mountain', 'INV').
card_original_type('mountain'/'INV', 'Land').
card_original_text('mountain'/'INV', 'R').
card_image_name('mountain'/'INV', 'mountain4').
card_uid('mountain'/'INV', 'INV:Mountain:mountain4').
card_rarity('mountain'/'INV', 'Basic Land').
card_artist('mountain'/'INV', 'Scott Bailey').
card_number('mountain'/'INV', '346').
card_multiverse_id('mountain'/'INV', '26309').

card_in_set('mourning', 'INV').
card_original_type('mourning'/'INV', 'Enchant Creature').
card_original_text('mourning'/'INV', 'Enchanted creature gets -2/-0.\n{B} Return Mourning to its owner\'s hand.').
card_first_print('mourning', 'INV').
card_image_name('mourning'/'INV', 'mourning').
card_uid('mourning'/'INV', 'INV:Mourning:mourning').
card_rarity('mourning'/'INV', 'Common').
card_artist('mourning'/'INV', 'Terese Nielsen').
card_number('mourning'/'INV', '111').
card_flavor_text('mourning'/'INV', 'Barrin waited for the nausea to pass or for Urza to say something else. He waited in vain.').
card_multiverse_id('mourning'/'INV', '23031').

card_in_set('nightscape apprentice', 'INV').
card_original_type('nightscape apprentice'/'INV', 'Creature — Wizard').
card_original_text('nightscape apprentice'/'INV', '{U}, {T}: Put target creature you control on top of its owner\'s library.\n{R}, {T}: Target creature gains first strike until end of turn.').
card_first_print('nightscape apprentice', 'INV').
card_image_name('nightscape apprentice'/'INV', 'nightscape apprentice').
card_uid('nightscape apprentice'/'INV', 'INV:Nightscape Apprentice:nightscape apprentice').
card_rarity('nightscape apprentice'/'INV', 'Common').
card_artist('nightscape apprentice'/'INV', 'Andrew Goldhawk').
card_number('nightscape apprentice'/'INV', '112').
card_multiverse_id('nightscape apprentice'/'INV', '23016').

card_in_set('nightscape master', 'INV').
card_original_type('nightscape master'/'INV', 'Creature — Wizard').
card_original_text('nightscape master'/'INV', '{U}{U}, {T}: Return target creature to its owner\'s hand.\n{R}{R}, {T}: Nightscape Master deals 2 damage to target creature.').
card_first_print('nightscape master', 'INV').
card_image_name('nightscape master'/'INV', 'nightscape master').
card_uid('nightscape master'/'INV', 'INV:Nightscape Master:nightscape master').
card_rarity('nightscape master'/'INV', 'Rare').
card_artist('nightscape master'/'INV', 'Andrew Goldhawk').
card_number('nightscape master'/'INV', '113').
card_multiverse_id('nightscape master'/'INV', '23049').

card_in_set('noble panther', 'INV').
card_original_type('noble panther'/'INV', 'Creature — Cat').
card_original_text('noble panther'/'INV', '{1}: Noble Panther gains first strike until end of turn.').
card_first_print('noble panther', 'INV').
card_image_name('noble panther'/'INV', 'noble panther').
card_uid('noble panther'/'INV', 'INV:Noble Panther:noble panther').
card_rarity('noble panther'/'INV', 'Rare').
card_artist('noble panther'/'INV', 'Matt Cavotta').
card_number('noble panther'/'INV', '257').
card_flavor_text('noble panther'/'INV', 'Unlike many hunters, these panthers have no need for camouflage. They\'re fast enough to catch any prey.').
card_multiverse_id('noble panther'/'INV', '23147').

card_in_set('nomadic elf', 'INV').
card_original_type('nomadic elf'/'INV', 'Creature — Elf').
card_original_text('nomadic elf'/'INV', '{1}{G} Add one mana of any color to your mana pool.').
card_first_print('nomadic elf', 'INV').
card_image_name('nomadic elf'/'INV', 'nomadic elf').
card_uid('nomadic elf'/'INV', 'INV:Nomadic Elf:nomadic elf').
card_rarity('nomadic elf'/'INV', 'Common').
card_artist('nomadic elf'/'INV', 'D. J. Cleland-Hura').
card_number('nomadic elf'/'INV', '200').
card_flavor_text('nomadic elf'/'INV', '\"I\'ve journeyed across Dominaria. Phyrexians are everywhere. Plague is everywhere. There\'s no place left to hide.\"').
card_multiverse_id('nomadic elf'/'INV', '23111').

card_in_set('obliterate', 'INV').
card_original_type('obliterate'/'INV', 'Sorcery').
card_original_text('obliterate'/'INV', 'Obliterate can\'t be countered.\nDestroy all artifacts, creatures, and lands. They can\'t be regenerated.').
card_first_print('obliterate', 'INV').
card_image_name('obliterate'/'INV', 'obliterate').
card_uid('obliterate'/'INV', 'INV:Obliterate:obliterate').
card_rarity('obliterate'/'INV', 'Rare').
card_artist('obliterate'/'INV', 'Kev Walker').
card_number('obliterate'/'INV', '156').
card_flavor_text('obliterate'/'INV', 'For his family, Barrin made a funeral pyre of Tolaria.').
card_multiverse_id('obliterate'/'INV', '23098').

card_in_set('obsidian acolyte', 'INV').
card_original_type('obsidian acolyte'/'INV', 'Creature — Cleric').
card_original_text('obsidian acolyte'/'INV', 'Protection from black\n{W} Target creature gains protection from black until end of turn.').
card_first_print('obsidian acolyte', 'INV').
card_image_name('obsidian acolyte'/'INV', 'obsidian acolyte').
card_uid('obsidian acolyte'/'INV', 'INV:Obsidian Acolyte:obsidian acolyte').
card_rarity('obsidian acolyte'/'INV', 'Common').
card_artist('obsidian acolyte'/'INV', 'Matthew D. Wilson').
card_number('obsidian acolyte'/'INV', '22').
card_flavor_text('obsidian acolyte'/'INV', 'The truthful will stand in the gaze of death and escape unharmed.\n—Obsidian acolyte creed').
card_multiverse_id('obsidian acolyte'/'INV', '22935').

card_in_set('opt', 'INV').
card_original_type('opt'/'INV', 'Instant').
card_original_text('opt'/'INV', 'Look at the top card of your library. You may put that card on the bottom of your library.\nDraw a card.').
card_first_print('opt', 'INV').
card_image_name('opt'/'INV', 'opt').
card_uid('opt'/'INV', 'INV:Opt:opt').
card_rarity('opt'/'INV', 'Common').
card_artist('opt'/'INV', 'John Howe').
card_number('opt'/'INV', '64').
card_flavor_text('opt'/'INV', '\"We need alternatives, Hanna,\" yelled Gerrard. \"Now\"').
card_multiverse_id('opt'/'INV', '22988').

card_in_set('ordered migration', 'INV').
card_original_type('ordered migration'/'INV', 'Sorcery').
card_original_text('ordered migration'/'INV', 'Put a 1/1 blue Bird creature token with flying into play for each basic land type among lands you control.').
card_first_print('ordered migration', 'INV').
card_image_name('ordered migration'/'INV', 'ordered migration').
card_uid('ordered migration'/'INV', 'INV:Ordered Migration:ordered migration').
card_rarity('ordered migration'/'INV', 'Uncommon').
card_artist('ordered migration'/'INV', 'Heather Hudson').
card_number('ordered migration'/'INV', '258').
card_flavor_text('ordered migration'/'INV', '\"Birds reach all parts of the world,\" said Barrin. \"They will make excellent scouts.\"').
card_multiverse_id('ordered migration'/'INV', '23187').

card_in_set('orim\'s touch', 'INV').
card_original_type('orim\'s touch'/'INV', 'Instant').
card_original_text('orim\'s touch'/'INV', 'Kicker {1} (You may pay an additional {1} as you play this spell.)\nPrevent the next 2 damage that would be dealt to target creature or player this turn. If you paid the kicker cost, prevent the next 4 damage that would be dealt to that creature or player this turn instead.').
card_first_print('orim\'s touch', 'INV').
card_image_name('orim\'s touch'/'INV', 'orim\'s touch').
card_uid('orim\'s touch'/'INV', 'INV:Orim\'s Touch:orim\'s touch').
card_rarity('orim\'s touch'/'INV', 'Common').
card_artist('orim\'s touch'/'INV', 'Roger Raupp').
card_number('orim\'s touch'/'INV', '23').
card_multiverse_id('orim\'s touch'/'INV', '22945').

card_in_set('overabundance', 'INV').
card_original_type('overabundance'/'INV', 'Enchantment').
card_original_text('overabundance'/'INV', 'Whenever a player taps a land for mana, that player adds one additional mana to his or her mana pool of the same type, and Overabundance deals 1 damage to him or her.').
card_first_print('overabundance', 'INV').
card_image_name('overabundance'/'INV', 'overabundance').
card_uid('overabundance'/'INV', 'INV:Overabundance:overabundance').
card_rarity('overabundance'/'INV', 'Rare').
card_artist('overabundance'/'INV', 'Ben Thompson').
card_number('overabundance'/'INV', '259').
card_multiverse_id('overabundance'/'INV', '23202').

card_in_set('overload', 'INV').
card_original_type('overload'/'INV', 'Instant').
card_original_text('overload'/'INV', 'Kicker {2} (You may pay an additional {2} as you play this spell.)\nDestroy target artifact if its converted mana cost is 2 or less. If you paid the kicker cost, destroy that artifact if its converted mana cost is 5 or less instead.').
card_first_print('overload', 'INV').
card_image_name('overload'/'INV', 'overload').
card_uid('overload'/'INV', 'INV:Overload:overload').
card_rarity('overload'/'INV', 'Common').
card_artist('overload'/'INV', 'Gary Ruddell').
card_number('overload'/'INV', '157').
card_multiverse_id('overload'/'INV', '23070').

card_in_set('pain', 'INV').
card_original_type('pain'/'INV', 'Sorcery').
card_original_text('pain'/'INV', 'Target player discards a card from his or her hand.').
card_first_print('pain', 'INV').
card_image_name('pain'/'INV', 'painsuffering').
card_uid('pain'/'INV', 'INV:Pain:painsuffering').
card_rarity('pain'/'INV', 'Uncommon').
card_artist('pain'/'INV', 'David Martin').
card_number('pain'/'INV', '294a').
card_multiverse_id('pain'/'INV', '20578').

card_in_set('phantasmal terrain', 'INV').
card_original_type('phantasmal terrain'/'INV', 'Enchant Land').
card_original_text('phantasmal terrain'/'INV', 'As Phantasmal Terrain comes into play, choose a basic land type.\nEnchanted land is a land of the chosen type.').
card_image_name('phantasmal terrain'/'INV', 'phantasmal terrain').
card_uid('phantasmal terrain'/'INV', 'INV:Phantasmal Terrain:phantasmal terrain').
card_rarity('phantasmal terrain'/'INV', 'Common').
card_artist('phantasmal terrain'/'INV', 'Dana Knutson').
card_number('phantasmal terrain'/'INV', '65').
card_multiverse_id('phantasmal terrain'/'INV', '22983').

card_in_set('phyrexian altar', 'INV').
card_original_type('phyrexian altar'/'INV', 'Artifact').
card_original_text('phyrexian altar'/'INV', 'Sacrifice a creature: Add one mana of any color to your mana pool.').
card_first_print('phyrexian altar', 'INV').
card_image_name('phyrexian altar'/'INV', 'phyrexian altar').
card_uid('phyrexian altar'/'INV', 'INV:Phyrexian Altar:phyrexian altar').
card_rarity('phyrexian altar'/'INV', 'Rare').
card_artist('phyrexian altar'/'INV', 'Ron Spears').
card_number('phyrexian altar'/'INV', '306').
card_flavor_text('phyrexian altar'/'INV', '\"Your life was meaningless, but your death will glorify Yawgmoth.\"\n—Tsabo Tavoc').
card_multiverse_id('phyrexian altar'/'INV', '23226').

card_in_set('phyrexian battleflies', 'INV').
card_original_type('phyrexian battleflies'/'INV', 'Creature — Insect').
card_original_text('phyrexian battleflies'/'INV', 'Flying\n{B} Phyrexian Battleflies gets +1/+0 until end of turn. This ability may be played no more than twice each turn.').
card_first_print('phyrexian battleflies', 'INV').
card_image_name('phyrexian battleflies'/'INV', 'phyrexian battleflies').
card_uid('phyrexian battleflies'/'INV', 'INV:Phyrexian Battleflies:phyrexian battleflies').
card_rarity('phyrexian battleflies'/'INV', 'Common').
card_artist('phyrexian battleflies'/'INV', 'Dan Frazier').
card_number('phyrexian battleflies'/'INV', '114').
card_flavor_text('phyrexian battleflies'/'INV', 'After encountering them, Squee finally lost his appetite for bugs.').
card_multiverse_id('phyrexian battleflies'/'INV', '23023').

card_in_set('phyrexian delver', 'INV').
card_original_type('phyrexian delver'/'INV', 'Creature — Zombie').
card_original_text('phyrexian delver'/'INV', 'When Phyrexian Delver comes into play, return target creature card from your graveyard to play. You lose life equal to that card\'s converted mana cost.').
card_first_print('phyrexian delver', 'INV').
card_image_name('phyrexian delver'/'INV', 'phyrexian delver').
card_uid('phyrexian delver'/'INV', 'INV:Phyrexian Delver:phyrexian delver').
card_rarity('phyrexian delver'/'INV', 'Rare').
card_artist('phyrexian delver'/'INV', 'Dana Knutson').
card_number('phyrexian delver'/'INV', '115').
card_multiverse_id('phyrexian delver'/'INV', '23022').

card_in_set('phyrexian infiltrator', 'INV').
card_original_type('phyrexian infiltrator'/'INV', 'Creature — Minion').
card_original_text('phyrexian infiltrator'/'INV', '{2}{U}{U} Exchange control of Phyrexian Infiltrator and target creature.').
card_first_print('phyrexian infiltrator', 'INV').
card_image_name('phyrexian infiltrator'/'INV', 'phyrexian infiltrator').
card_uid('phyrexian infiltrator'/'INV', 'INV:Phyrexian Infiltrator:phyrexian infiltrator').
card_rarity('phyrexian infiltrator'/'INV', 'Rare').
card_artist('phyrexian infiltrator'/'INV', 'Darrell Riche').
card_number('phyrexian infiltrator'/'INV', '116').
card_flavor_text('phyrexian infiltrator'/'INV', '\"It is Yawgmoth\'s will incarnate: an efficient machine that replaces inefficient flesh.\"\n—Tsabo Tavoc').
card_multiverse_id('phyrexian infiltrator'/'INV', '26372').

card_in_set('phyrexian lens', 'INV').
card_original_type('phyrexian lens'/'INV', 'Artifact').
card_original_text('phyrexian lens'/'INV', '{T}, Pay 1 life: Add one mana of any color to your mana pool.').
card_first_print('phyrexian lens', 'INV').
card_image_name('phyrexian lens'/'INV', 'phyrexian lens').
card_uid('phyrexian lens'/'INV', 'INV:Phyrexian Lens:phyrexian lens').
card_rarity('phyrexian lens'/'INV', 'Rare').
card_artist('phyrexian lens'/'INV', 'Matt Cavotta').
card_number('phyrexian lens'/'INV', '307').
card_flavor_text('phyrexian lens'/'INV', '\"There\'s nothing I wouldn\'t give to achieve victory. Can you say the same?\"\n—Tsabo Tavoc, to Gerrard').
card_multiverse_id('phyrexian lens'/'INV', '23231').

card_in_set('phyrexian reaper', 'INV').
card_original_type('phyrexian reaper'/'INV', 'Creature — Zombie').
card_original_text('phyrexian reaper'/'INV', 'Whenever Phyrexian Reaper becomes blocked by a green creature, destroy that creature. It can\'t be regenerated.').
card_first_print('phyrexian reaper', 'INV').
card_image_name('phyrexian reaper'/'INV', 'phyrexian reaper').
card_uid('phyrexian reaper'/'INV', 'INV:Phyrexian Reaper:phyrexian reaper').
card_rarity('phyrexian reaper'/'INV', 'Common').
card_artist('phyrexian reaper'/'INV', 'Sam Wood').
card_number('phyrexian reaper'/'INV', '117').
card_flavor_text('phyrexian reaper'/'INV', 'It desires only to help others shed the itchy wet skin of life.').
card_multiverse_id('phyrexian reaper'/'INV', '23036').

card_in_set('phyrexian slayer', 'INV').
card_original_type('phyrexian slayer'/'INV', 'Creature — Minion').
card_original_text('phyrexian slayer'/'INV', 'Flying\nWhenever Phyrexian Slayer becomes blocked by a white creature, destroy that creature. It can\'t be regenerated.').
card_first_print('phyrexian slayer', 'INV').
card_image_name('phyrexian slayer'/'INV', 'phyrexian slayer').
card_uid('phyrexian slayer'/'INV', 'INV:Phyrexian Slayer:phyrexian slayer').
card_rarity('phyrexian slayer'/'INV', 'Common').
card_artist('phyrexian slayer'/'INV', 'Sam Wood').
card_number('phyrexian slayer'/'INV', '118').
card_flavor_text('phyrexian slayer'/'INV', 'Benalia was only the first nation victimized by Tsabo Tavoc\'s specialized killers.').
card_multiverse_id('phyrexian slayer'/'INV', '23040').

card_in_set('pincer spider', 'INV').
card_original_type('pincer spider'/'INV', 'Creature — Spider').
card_original_text('pincer spider'/'INV', 'Kicker {3} (You may pay an additional {3} as you play this spell.)\nPincer Spider may block as though it had flying.\nIf you paid the kicker cost, Pincer Spider comes into play with a +1/+1 counter on it.').
card_first_print('pincer spider', 'INV').
card_image_name('pincer spider'/'INV', 'pincer spider').
card_uid('pincer spider'/'INV', 'INV:Pincer Spider:pincer spider').
card_rarity('pincer spider'/'INV', 'Common').
card_artist('pincer spider'/'INV', 'Dan Frazier').
card_number('pincer spider'/'INV', '201').
card_multiverse_id('pincer spider'/'INV', '23107').

card_in_set('plague spitter', 'INV').
card_original_type('plague spitter'/'INV', 'Creature — Horror').
card_original_text('plague spitter'/'INV', 'At the beginning of your upkeep, Plague Spitter deals 1 damage to each creature and each player.\nWhen Plague Spitter is put into a graveyard from play, Plague Spitter deals 1 damage to each creature and each player.').
card_first_print('plague spitter', 'INV').
card_image_name('plague spitter'/'INV', 'plague spitter').
card_uid('plague spitter'/'INV', 'INV:Plague Spitter:plague spitter').
card_rarity('plague spitter'/'INV', 'Uncommon').
card_artist('plague spitter'/'INV', 'Chippy').
card_number('plague spitter'/'INV', '119').
card_multiverse_id('plague spitter'/'INV', '23021').

card_in_set('plague spores', 'INV').
card_original_type('plague spores'/'INV', 'Sorcery').
card_original_text('plague spores'/'INV', 'Destroy target nonblack creature and target land. They can\'t be regenerated.').
card_first_print('plague spores', 'INV').
card_image_name('plague spores'/'INV', 'plague spores').
card_uid('plague spores'/'INV', 'INV:Plague Spores:plague spores').
card_rarity('plague spores'/'INV', 'Common').
card_artist('plague spores'/'INV', 'Randy Gallegos').
card_number('plague spores'/'INV', '260').
card_flavor_text('plague spores'/'INV', '\"Breathe deep, Dominaria. Breathe deep and die.\"\n—Tsabo Tavoc, Phyrexian general').
card_multiverse_id('plague spores'/'INV', '23153').

card_in_set('plains', 'INV').
card_original_type('plains'/'INV', 'Land').
card_original_text('plains'/'INV', 'W').
card_image_name('plains'/'INV', 'plains1').
card_uid('plains'/'INV', 'INV:Plains:plains1').
card_rarity('plains'/'INV', 'Basic Land').
card_artist('plains'/'INV', 'John Avon').
card_number('plains'/'INV', '331').
card_multiverse_id('plains'/'INV', '25963').

card_in_set('plains', 'INV').
card_original_type('plains'/'INV', 'Land').
card_original_text('plains'/'INV', 'W').
card_image_name('plains'/'INV', 'plains2').
card_uid('plains'/'INV', 'INV:Plains:plains2').
card_rarity('plains'/'INV', 'Basic Land').
card_artist('plains'/'INV', 'Ben Thompson').
card_number('plains'/'INV', '332').
card_multiverse_id('plains'/'INV', '26298').

card_in_set('plains', 'INV').
card_original_type('plains'/'INV', 'Land').
card_original_text('plains'/'INV', 'W').
card_image_name('plains'/'INV', 'plains3').
card_uid('plains'/'INV', 'INV:Plains:plains3').
card_rarity('plains'/'INV', 'Basic Land').
card_artist('plains'/'INV', 'D. J. Cleland-Hura').
card_number('plains'/'INV', '333').
card_multiverse_id('plains'/'INV', '26299').

card_in_set('plains', 'INV').
card_original_type('plains'/'INV', 'Land').
card_original_text('plains'/'INV', 'W').
card_image_name('plains'/'INV', 'plains4').
card_uid('plains'/'INV', 'INV:Plains:plains4').
card_rarity('plains'/'INV', 'Basic Land').
card_artist('plains'/'INV', 'Scott Bailey').
card_number('plains'/'INV', '334').
card_multiverse_id('plains'/'INV', '26300').

card_in_set('planar portal', 'INV').
card_original_type('planar portal'/'INV', 'Artifact').
card_original_text('planar portal'/'INV', '{6}, {T}: Search your library for a card and put that card into your hand. Then shuffle your library.').
card_first_print('planar portal', 'INV').
card_image_name('planar portal'/'INV', 'planar portal').
card_uid('planar portal'/'INV', 'INV:Planar Portal:planar portal').
card_rarity('planar portal'/'INV', 'Rare').
card_artist('planar portal'/'INV', 'Mark Tedin').
card_number('planar portal'/'INV', '308').
card_flavor_text('planar portal'/'INV', 'The sky split, and the air crackled and roiled. The Phyrexian invasion of Dominaria had finally begun.').
card_multiverse_id('planar portal'/'INV', '23234').

card_in_set('pledge of loyalty', 'INV').
card_original_type('pledge of loyalty'/'INV', 'Enchant Creature').
card_original_text('pledge of loyalty'/'INV', 'Enchanted creature has protection from the colors of permanents you control. This effect doesn\'t remove Pledge of Loyalty.').
card_first_print('pledge of loyalty', 'INV').
card_image_name('pledge of loyalty'/'INV', 'pledge of loyalty').
card_uid('pledge of loyalty'/'INV', 'INV:Pledge of Loyalty:pledge of loyalty').
card_rarity('pledge of loyalty'/'INV', 'Uncommon').
card_artist('pledge of loyalty'/'INV', 'Franz Vohwinkel').
card_number('pledge of loyalty'/'INV', '24').
card_flavor_text('pledge of loyalty'/'INV', 'Urza convinced many Dominarians not only to set aside their differences, but to embrace them.').
card_multiverse_id('pledge of loyalty'/'INV', '22958').

card_in_set('pouncing kavu', 'INV').
card_original_type('pouncing kavu'/'INV', 'Creature — Kavu').
card_original_text('pouncing kavu'/'INV', 'Kicker {2}{R} (You may pay an additional {2}{R} as you play this spell.)\nFirst strike\nIf you paid the kicker cost, Pouncing Kavu comes into play with two +1/+1 counters on it and has haste. (It may attack and {T} the turn it comes under your control.)').
card_first_print('pouncing kavu', 'INV').
card_image_name('pouncing kavu'/'INV', 'pouncing kavu').
card_uid('pouncing kavu'/'INV', 'INV:Pouncing Kavu:pouncing kavu').
card_rarity('pouncing kavu'/'INV', 'Common').
card_artist('pouncing kavu'/'INV', 'Adam Rex').
card_number('pouncing kavu'/'INV', '158').
card_multiverse_id('pouncing kavu'/'INV', '23064').

card_in_set('power armor', 'INV').
card_original_type('power armor'/'INV', 'Artifact').
card_original_text('power armor'/'INV', '{3}, {T}: Target creature gets +1/+1 until end of turn for each basic land type among lands you control.').
card_first_print('power armor', 'INV').
card_image_name('power armor'/'INV', 'power armor').
card_uid('power armor'/'INV', 'INV:Power Armor:power armor').
card_rarity('power armor'/'INV', 'Uncommon').
card_artist('power armor'/'INV', 'Doug Chaffee').
card_number('power armor'/'INV', '309').
card_flavor_text('power armor'/'INV', '\"Great peril demands formidable weaponry.\"\n—Urza').
card_multiverse_id('power armor'/'INV', '23222').

card_in_set('prison barricade', 'INV').
card_original_type('prison barricade'/'INV', 'Creature — Wall').
card_original_text('prison barricade'/'INV', '(Walls can\'t attack.)\nKicker {1}{W} (You may pay an additional {1}{W} as you play this spell.)\nIf you paid the kicker cost, Prison Barricade comes into play with a +1/+1 counter on it and may attack as though it weren\'t a Wall.').
card_first_print('prison barricade', 'INV').
card_image_name('prison barricade'/'INV', 'prison barricade').
card_uid('prison barricade'/'INV', 'INV:Prison Barricade:prison barricade').
card_rarity('prison barricade'/'INV', 'Common').
card_artist('prison barricade'/'INV', 'Thomas Gianni').
card_number('prison barricade'/'INV', '25').
card_multiverse_id('prison barricade'/'INV', '22944').

card_in_set('probe', 'INV').
card_original_type('probe'/'INV', 'Sorcery').
card_original_text('probe'/'INV', 'Kicker {1}{B} (You may pay an additional {1}{B} as you play this spell.)\nDraw three cards, then discard two cards from your hand.\nIf you paid the kicker cost, target player discards two cards from his or her hand.').
card_first_print('probe', 'INV').
card_image_name('probe'/'INV', 'probe').
card_uid('probe'/'INV', 'INV:Probe:probe').
card_rarity('probe'/'INV', 'Common').
card_artist('probe'/'INV', 'Eric Peterson').
card_number('probe'/'INV', '66').
card_multiverse_id('probe'/'INV', '22990').

card_in_set('prohibit', 'INV').
card_original_type('prohibit'/'INV', 'Instant').
card_original_text('prohibit'/'INV', 'Kicker {2} (You may pay an additional {2} as you play this spell.)\nCounter target spell if its converted mana cost is 2 or less. If you paid the kicker cost, counter that spell if its converted mana cost is 4 or less instead.').
card_first_print('prohibit', 'INV').
card_image_name('prohibit'/'INV', 'prohibit').
card_uid('prohibit'/'INV', 'INV:Prohibit:prohibit').
card_rarity('prohibit'/'INV', 'Common').
card_artist('prohibit'/'INV', 'Adam Rex').
card_number('prohibit'/'INV', '67').
card_multiverse_id('prohibit'/'INV', '22989').

card_in_set('protective sphere', 'INV').
card_original_type('protective sphere'/'INV', 'Enchantment').
card_original_text('protective sphere'/'INV', '{1}, Pay 1 life: Prevent all damage that would be dealt to you this turn by a source of your choice that shares a color with the mana spent on this activation cost. (Colorless mana prevents no damage.)').
card_first_print('protective sphere', 'INV').
card_image_name('protective sphere'/'INV', 'protective sphere').
card_uid('protective sphere'/'INV', 'INV:Protective Sphere:protective sphere').
card_rarity('protective sphere'/'INV', 'Common').
card_artist('protective sphere'/'INV', 'Rebecca Guay').
card_number('protective sphere'/'INV', '26').
card_multiverse_id('protective sphere'/'INV', '26257').

card_in_set('psychic battle', 'INV').
card_original_type('psychic battle'/'INV', 'Enchantment').
card_original_text('psychic battle'/'INV', 'Whenever a player chooses one or more targets, each player reveals the top card of his or her library. The player who reveals the card with the highest converted mana cost may change the target or targets. If two or more cards are tied for highest cost, the target or targets remain unchanged.').
card_first_print('psychic battle', 'INV').
card_image_name('psychic battle'/'INV', 'psychic battle').
card_uid('psychic battle'/'INV', 'INV:Psychic Battle:psychic battle').
card_rarity('psychic battle'/'INV', 'Rare').
card_artist('psychic battle'/'INV', 'Ray Lago').
card_number('psychic battle'/'INV', '68').
card_multiverse_id('psychic battle'/'INV', '26287').

card_in_set('pulse of llanowar', 'INV').
card_original_type('pulse of llanowar'/'INV', 'Enchantment').
card_original_text('pulse of llanowar'/'INV', 'If a basic land you control is tapped for mana, it produces mana of any one color instead of its normal type.').
card_first_print('pulse of llanowar', 'INV').
card_image_name('pulse of llanowar'/'INV', 'pulse of llanowar').
card_uid('pulse of llanowar'/'INV', 'INV:Pulse of Llanowar:pulse of llanowar').
card_rarity('pulse of llanowar'/'INV', 'Uncommon').
card_artist('pulse of llanowar'/'INV', 'Rebecca Guay').
card_number('pulse of llanowar'/'INV', '202').
card_flavor_text('pulse of llanowar'/'INV', '\"Gaea\'s memory is eternal, and she exists in all things.\"\n—Molimo, maro-sorcerer').
card_multiverse_id('pulse of llanowar'/'INV', '23143').

card_in_set('pure reflection', 'INV').
card_original_type('pure reflection'/'INV', 'Enchantment').
card_original_text('pure reflection'/'INV', 'Whenever a player plays a creature spell, destroy all Reflections. Then that player puts a white Reflection creature token into play with power and toughness each equal to the converted mana cost of that spell.').
card_first_print('pure reflection', 'INV').
card_image_name('pure reflection'/'INV', 'pure reflection').
card_uid('pure reflection'/'INV', 'INV:Pure Reflection:pure reflection').
card_rarity('pure reflection'/'INV', 'Rare').
card_artist('pure reflection'/'INV', 'Scott M. Fischer').
card_number('pure reflection'/'INV', '27').
card_multiverse_id('pure reflection'/'INV', '23319').

card_in_set('pyre zombie', 'INV').
card_original_type('pyre zombie'/'INV', 'Creature — Zombie').
card_original_text('pyre zombie'/'INV', 'At the beginning of your upkeep, if Pyre Zombie is in your graveyard, you may pay {1}{B}{B}. If you do, return Pyre Zombie from your graveyard to your hand.\n{1}{R}{R}, Sacrifice Pyre Zombie: Pyre Zombie deals 2 damage to target creature or player.').
card_first_print('pyre zombie', 'INV').
card_image_name('pyre zombie'/'INV', 'pyre zombie').
card_uid('pyre zombie'/'INV', 'INV:Pyre Zombie:pyre zombie').
card_rarity('pyre zombie'/'INV', 'Rare').
card_artist('pyre zombie'/'INV', 'Nelson DeCastro').
card_number('pyre zombie'/'INV', '261').
card_multiverse_id('pyre zombie'/'INV', '23201').

card_in_set('quirion elves', 'INV').
card_original_type('quirion elves'/'INV', 'Creature — Elf').
card_original_text('quirion elves'/'INV', 'As Quirion Elves comes into play, choose a color.\n{T}: Add {G} to your mana pool.\n{T}: Add one mana of the chosen color to your mana pool.').
card_image_name('quirion elves'/'INV', 'quirion elves').
card_uid('quirion elves'/'INV', 'INV:Quirion Elves:quirion elves').
card_rarity('quirion elves'/'INV', 'Common').
card_artist('quirion elves'/'INV', 'Douglas Shuler').
card_number('quirion elves'/'INV', '203').
card_multiverse_id('quirion elves'/'INV', '23112').

card_in_set('quirion sentinel', 'INV').
card_original_type('quirion sentinel'/'INV', 'Creature — Elf').
card_original_text('quirion sentinel'/'INV', 'When Quirion Sentinel comes into play, add one mana of any color to your mana pool.').
card_first_print('quirion sentinel', 'INV').
card_image_name('quirion sentinel'/'INV', 'quirion sentinel').
card_uid('quirion sentinel'/'INV', 'INV:Quirion Sentinel:quirion sentinel').
card_rarity('quirion sentinel'/'INV', 'Common').
card_artist('quirion sentinel'/'INV', 'Heather Hudson').
card_number('quirion sentinel'/'INV', '204').
card_flavor_text('quirion sentinel'/'INV', 'All elvenkind stood against Phyrexia. The Quirion nation deployed its most spiritual adepts, who wielded the power of their native soil.').
card_multiverse_id('quirion sentinel'/'INV', '26416').

card_in_set('quirion trailblazer', 'INV').
card_original_type('quirion trailblazer'/'INV', 'Creature — Elf').
card_original_text('quirion trailblazer'/'INV', 'When Quirion Trailblazer comes into play, you may search your library for a basic land card and put that card into play tapped. If you do, shuffle your library.').
card_first_print('quirion trailblazer', 'INV').
card_image_name('quirion trailblazer'/'INV', 'quirion trailblazer').
card_uid('quirion trailblazer'/'INV', 'INV:Quirion Trailblazer:quirion trailblazer').
card_rarity('quirion trailblazer'/'INV', 'Common').
card_artist('quirion trailblazer'/'INV', 'Rebecca Guay').
card_number('quirion trailblazer'/'INV', '205').
card_flavor_text('quirion trailblazer'/'INV', '\"All that matters is the path ahead.\"').
card_multiverse_id('quirion trailblazer'/'INV', '23110').

card_in_set('rage weaver', 'INV').
card_original_type('rage weaver'/'INV', 'Creature — Wizard').
card_original_text('rage weaver'/'INV', '{2}: Target black or green creature gains haste until end of turn. (It may attack and {T} the turn it comes under your control.)').
card_first_print('rage weaver', 'INV').
card_image_name('rage weaver'/'INV', 'rage weaver').
card_uid('rage weaver'/'INV', 'INV:Rage Weaver:rage weaver').
card_rarity('rage weaver'/'INV', 'Uncommon').
card_artist('rage weaver'/'INV', 'John Matson').
card_number('rage weaver'/'INV', '159').
card_flavor_text('rage weaver'/'INV', '\"Let my passion spur your victory.\"').
card_multiverse_id('rage weaver'/'INV', '23081').

card_in_set('raging kavu', 'INV').
card_original_type('raging kavu'/'INV', 'Creature — Kavu').
card_original_text('raging kavu'/'INV', 'Haste (This creature may attack and {T} the turn it comes under your control.)\nYou may play Raging Kavu any time you could play an instant.').
card_image_name('raging kavu'/'INV', 'raging kavu').
card_uid('raging kavu'/'INV', 'INV:Raging Kavu:raging kavu').
card_rarity('raging kavu'/'INV', 'Rare').
card_artist('raging kavu'/'INV', 'Arnie Swekel').
card_number('raging kavu'/'INV', '262').
card_flavor_text('raging kavu'/'INV', 'It took Yavimaya a thousand years to breed them, but it took only seconds for them to prove their worth.').
card_multiverse_id('raging kavu'/'INV', '23148').

card_in_set('rainbow crow', 'INV').
card_original_type('rainbow crow'/'INV', 'Creature — Bird').
card_original_text('rainbow crow'/'INV', 'Flying\n{1}: Rainbow Crow becomes the color of your choice until end of turn.').
card_first_print('rainbow crow', 'INV').
card_image_name('rainbow crow'/'INV', 'rainbow crow').
card_uid('rainbow crow'/'INV', 'INV:Rainbow Crow:rainbow crow').
card_rarity('rainbow crow'/'INV', 'Uncommon').
card_artist('rainbow crow'/'INV', 'Edward P. Beard, Jr.').
card_number('rainbow crow'/'INV', '69').
card_flavor_text('rainbow crow'/'INV', 'Children claim no two feathers are exactly the same color, then eagerly gather them for proof.').
card_multiverse_id('rainbow crow'/'INV', '22994').

card_in_set('rampant elephant', 'INV').
card_original_type('rampant elephant'/'INV', 'Creature — Elephant').
card_original_text('rampant elephant'/'INV', '{G} Target creature blocks Rampant Elephant this turn if able.').
card_first_print('rampant elephant', 'INV').
card_image_name('rampant elephant'/'INV', 'rampant elephant').
card_uid('rampant elephant'/'INV', 'INV:Rampant Elephant:rampant elephant').
card_rarity('rampant elephant'/'INV', 'Common').
card_artist('rampant elephant'/'INV', 'Alan Pollack').
card_number('rampant elephant'/'INV', '28').
card_flavor_text('rampant elephant'/'INV', 'No matter how righteous the cause, it helps to bring along some muscle.').
card_multiverse_id('rampant elephant'/'INV', '23062').

card_in_set('ravenous rats', 'INV').
card_original_type('ravenous rats'/'INV', 'Creature — Rat').
card_original_text('ravenous rats'/'INV', 'When Ravenous Rats comes into play, target opponent discards a card from his or her hand.').
card_image_name('ravenous rats'/'INV', 'ravenous rats').
card_uid('ravenous rats'/'INV', 'INV:Ravenous Rats:ravenous rats').
card_rarity('ravenous rats'/'INV', 'Common').
card_artist('ravenous rats'/'INV', 'Tom Wänerstrand').
card_number('ravenous rats'/'INV', '120').
card_flavor_text('ravenous rats'/'INV', 'Nothing is sacred to the rats. Everything is simply another meal.').
card_multiverse_id('ravenous rats'/'INV', '26297').

card_in_set('razorfoot griffin', 'INV').
card_original_type('razorfoot griffin'/'INV', 'Creature — Griffin').
card_original_text('razorfoot griffin'/'INV', 'Flying, first strike').
card_first_print('razorfoot griffin', 'INV').
card_image_name('razorfoot griffin'/'INV', 'razorfoot griffin').
card_uid('razorfoot griffin'/'INV', 'INV:Razorfoot Griffin:razorfoot griffin').
card_rarity('razorfoot griffin'/'INV', 'Common').
card_artist('razorfoot griffin'/'INV', 'Ben Thompson').
card_number('razorfoot griffin'/'INV', '29').
card_flavor_text('razorfoot griffin'/'INV', '\"Do griffins fight to defend their homes or purely for sport?\"\n—Sisay').
card_multiverse_id('razorfoot griffin'/'INV', '22933').

card_in_set('reckless assault', 'INV').
card_original_type('reckless assault'/'INV', 'Enchantment').
card_original_text('reckless assault'/'INV', '{1}, Pay 2 life: Reckless Assault deals 1 damage to target creature or player.').
card_first_print('reckless assault', 'INV').
card_image_name('reckless assault'/'INV', 'reckless assault').
card_uid('reckless assault'/'INV', 'INV:Reckless Assault:reckless assault').
card_rarity('reckless assault'/'INV', 'Rare').
card_artist('reckless assault'/'INV', 'Jeff Easley').
card_number('reckless assault'/'INV', '263').
card_flavor_text('reckless assault'/'INV', '\"How will you fight an enemy that cares nothing for itself?\"\n—The Blind Seer').
card_multiverse_id('reckless assault'/'INV', '23191').

card_in_set('reckless spite', 'INV').
card_original_type('reckless spite'/'INV', 'Instant').
card_original_text('reckless spite'/'INV', 'Destroy two target nonblack creatures. You lose 5 life.').
card_image_name('reckless spite'/'INV', 'reckless spite').
card_uid('reckless spite'/'INV', 'INV:Reckless Spite:reckless spite').
card_rarity('reckless spite'/'INV', 'Uncommon').
card_artist('reckless spite'/'INV', 'Chippy').
card_number('reckless spite'/'INV', '121').
card_flavor_text('reckless spite'/'INV', '\"Death is such an exquisite sensation.\"\n—Tsabo Tavoc, Phyrexian general').
card_multiverse_id('reckless spite'/'INV', '23045').

card_in_set('recoil', 'INV').
card_original_type('recoil'/'INV', 'Instant').
card_original_text('recoil'/'INV', 'Return target permanent to its owner\'s hand. Then that player discards a card from his or her hand.').
card_first_print('recoil', 'INV').
card_image_name('recoil'/'INV', 'recoil').
card_uid('recoil'/'INV', 'INV:Recoil:recoil').
card_rarity('recoil'/'INV', 'Common').
card_artist('recoil'/'INV', 'Alan Pollack').
card_number('recoil'/'INV', '264').
card_flavor_text('recoil'/'INV', 'Anything sent into a plagued world is bound to come back infected.').
card_multiverse_id('recoil'/'INV', '23154').

card_in_set('recover', 'INV').
card_original_type('recover'/'INV', 'Sorcery').
card_original_text('recover'/'INV', 'Return target creature card from your graveyard to your hand.\nDraw a card.').
card_first_print('recover', 'INV').
card_image_name('recover'/'INV', 'recover').
card_uid('recover'/'INV', 'INV:Recover:recover').
card_rarity('recover'/'INV', 'Common').
card_artist('recover'/'INV', 'Nelson DeCastro').
card_number('recover'/'INV', '122').
card_flavor_text('recover'/'INV', 'As Barrin exhumed his daughter\'s body, he finally realized the full price of his faith in Urza.').
card_multiverse_id('recover'/'INV', '23029').

card_in_set('repulse', 'INV').
card_original_type('repulse'/'INV', 'Instant').
card_original_text('repulse'/'INV', 'Return target creature to its owner\'s hand.\nDraw a card.').
card_first_print('repulse', 'INV').
card_image_name('repulse'/'INV', 'repulse').
card_uid('repulse'/'INV', 'INV:Repulse:repulse').
card_rarity('repulse'/'INV', 'Common').
card_artist('repulse'/'INV', 'Aaron Boyd').
card_number('repulse'/'INV', '70').
card_flavor_text('repulse'/'INV', '\"You aren\'t invited.\"').
card_multiverse_id('repulse'/'INV', '22985').

card_in_set('restock', 'INV').
card_original_type('restock'/'INV', 'Sorcery').
card_original_text('restock'/'INV', 'Return two target cards from your graveyard to your hand. Remove Restock from the game.').
card_first_print('restock', 'INV').
card_image_name('restock'/'INV', 'restock').
card_uid('restock'/'INV', 'INV:Restock:restock').
card_rarity('restock'/'INV', 'Rare').
card_artist('restock'/'INV', 'Daren Bader').
card_number('restock'/'INV', '206').
card_flavor_text('restock'/'INV', '\"We hid such stockpiles all over Rath. We should continue that practice here.\"\n—Lin Sivvi').
card_multiverse_id('restock'/'INV', '23145').

card_in_set('restrain', 'INV').
card_original_type('restrain'/'INV', 'Instant').
card_original_text('restrain'/'INV', 'Prevent all combat damage that would be dealt by target attacking creature this turn.\nDraw a card.').
card_first_print('restrain', 'INV').
card_image_name('restrain'/'INV', 'restrain').
card_uid('restrain'/'INV', 'INV:Restrain:restrain').
card_rarity('restrain'/'INV', 'Common').
card_artist('restrain'/'INV', 'Dave Dorman').
card_number('restrain'/'INV', '30').
card_flavor_text('restrain'/'INV', '\"Hanna would give up her own life before she\'d abandon the Weatherlight.\"\n—Sisay').
card_multiverse_id('restrain'/'INV', '22943').

card_in_set('reviving dose', 'INV').
card_original_type('reviving dose'/'INV', 'Instant').
card_original_text('reviving dose'/'INV', 'You gain 3 life.\nDraw a card.').
card_first_print('reviving dose', 'INV').
card_image_name('reviving dose'/'INV', 'reviving dose').
card_uid('reviving dose'/'INV', 'INV:Reviving Dose:reviving dose').
card_rarity('reviving dose'/'INV', 'Common').
card_artist('reviving dose'/'INV', 'D. Alexander Gregory').
card_number('reviving dose'/'INV', '31').
card_flavor_text('reviving dose'/'INV', 'As healers battled each plague, they learned more about the next.').
card_multiverse_id('reviving dose'/'INV', '22941').

card_in_set('reviving vapors', 'INV').
card_original_type('reviving vapors'/'INV', 'Instant').
card_original_text('reviving vapors'/'INV', 'Reveal the top three cards of your library and put one of them into your hand. You gain life equal to that card\'s converted mana cost. Put the other cards revealed this way into your graveyard.').
card_first_print('reviving vapors', 'INV').
card_image_name('reviving vapors'/'INV', 'reviving vapors').
card_uid('reviving vapors'/'INV', 'INV:Reviving Vapors:reviving vapors').
card_rarity('reviving vapors'/'INV', 'Uncommon').
card_artist('reviving vapors'/'INV', 'Pete Venters').
card_number('reviving vapors'/'INV', '265').
card_multiverse_id('reviving vapors'/'INV', '23196').

card_in_set('rewards of diversity', 'INV').
card_original_type('rewards of diversity'/'INV', 'Enchantment').
card_original_text('rewards of diversity'/'INV', 'Whenever an opponent plays a multicolored spell, you gain 4 life.').
card_first_print('rewards of diversity', 'INV').
card_image_name('rewards of diversity'/'INV', 'rewards of diversity').
card_uid('rewards of diversity'/'INV', 'INV:Rewards of Diversity:rewards of diversity').
card_rarity('rewards of diversity'/'INV', 'Uncommon').
card_artist('rewards of diversity'/'INV', 'Darrell Riche').
card_number('rewards of diversity'/'INV', '32').
card_flavor_text('rewards of diversity'/'INV', '\"Everything is in place. Nothing can happen that isn\'t part of my plan.\"\n—Urza').
card_multiverse_id('rewards of diversity'/'INV', '25323').

card_in_set('reya dawnbringer', 'INV').
card_original_type('reya dawnbringer'/'INV', 'Creature — Angel Legend').
card_original_text('reya dawnbringer'/'INV', 'Flying\nAt the beginning of your upkeep, you may return target creature card from your graveyard to play.').
card_first_print('reya dawnbringer', 'INV').
card_image_name('reya dawnbringer'/'INV', 'reya dawnbringer').
card_uid('reya dawnbringer'/'INV', 'INV:Reya Dawnbringer:reya dawnbringer').
card_rarity('reya dawnbringer'/'INV', 'Rare').
card_artist('reya dawnbringer'/'INV', 'Matthew D. Wilson').
card_number('reya dawnbringer'/'INV', '33').
card_flavor_text('reya dawnbringer'/'INV', 'A beacon of hope for a battered army.').
card_multiverse_id('reya dawnbringer'/'INV', '22965').

card_in_set('riptide crab', 'INV').
card_original_type('riptide crab'/'INV', 'Creature — Crab').
card_original_text('riptide crab'/'INV', 'Attacking doesn\'t cause Riptide Crab to tap.\nWhen Riptide Crab is put into a graveyard from play, draw a card.').
card_first_print('riptide crab', 'INV').
card_image_name('riptide crab'/'INV', 'riptide crab').
card_uid('riptide crab'/'INV', 'INV:Riptide Crab:riptide crab').
card_rarity('riptide crab'/'INV', 'Uncommon').
card_artist('riptide crab'/'INV', 'David Martin').
card_number('riptide crab'/'INV', '266').
card_flavor_text('riptide crab'/'INV', 'It sleeps with its claws open.').
card_multiverse_id('riptide crab'/'INV', '23150').

card_in_set('rith\'s attendant', 'INV').
card_original_type('rith\'s attendant'/'INV', 'Artifact Creature — Golem').
card_original_text('rith\'s attendant'/'INV', '{1}, Sacrifice Rith\'s Attendant: Add {R}{G}{W} to your mana pool.').
card_first_print('rith\'s attendant', 'INV').
card_image_name('rith\'s attendant'/'INV', 'rith\'s attendant').
card_uid('rith\'s attendant'/'INV', 'INV:Rith\'s Attendant:rith\'s attendant').
card_rarity('rith\'s attendant'/'INV', 'Uncommon').
card_artist('rith\'s attendant'/'INV', 'Adam Rex').
card_number('rith\'s attendant'/'INV', '310').
card_flavor_text('rith\'s attendant'/'INV', '\"Rith is the claw of the ur-dragon, scattering seeds of devastation.\"').
card_multiverse_id('rith\'s attendant'/'INV', '25839').

card_in_set('rith, the awakener', 'INV').
card_original_type('rith, the awakener'/'INV', 'Creature — Dragon Legend').
card_original_text('rith, the awakener'/'INV', 'Flying\nWhenever Rith, the Awakener deals combat damage to a player, you may pay {2}{G}. If you do, choose a color. Put a 1/1 green Saproling creature token into play for each permanent of that color.').
card_first_print('rith, the awakener', 'INV').
card_image_name('rith, the awakener'/'INV', 'rith, the awakener').
card_uid('rith, the awakener'/'INV', 'INV:Rith, the Awakener:rith, the awakener').
card_rarity('rith, the awakener'/'INV', 'Rare').
card_artist('rith, the awakener'/'INV', 'Carl Critchlow').
card_number('rith, the awakener'/'INV', '267').
card_multiverse_id('rith, the awakener'/'INV', '23209').

card_in_set('rogue kavu', 'INV').
card_original_type('rogue kavu'/'INV', 'Creature — Kavu').
card_original_text('rogue kavu'/'INV', 'Whenever Rogue Kavu attacks alone, it gets +2/+0 until end of turn.').
card_first_print('rogue kavu', 'INV').
card_image_name('rogue kavu'/'INV', 'rogue kavu').
card_uid('rogue kavu'/'INV', 'INV:Rogue Kavu:rogue kavu').
card_rarity('rogue kavu'/'INV', 'Common').
card_artist('rogue kavu'/'INV', 'Darrell Riche').
card_number('rogue kavu'/'INV', '160').
card_flavor_text('rogue kavu'/'INV', '\"I know how this one feels,\" said Urza. \"It\'s not like others of its kind and has been shunned for its differences.\"').
card_multiverse_id('rogue kavu'/'INV', '23065').

card_in_set('rooting kavu', 'INV').
card_original_type('rooting kavu'/'INV', 'Creature — Kavu').
card_original_text('rooting kavu'/'INV', 'When Rooting Kavu is put into a graveyard from play, you may remove Rooting Kavu from the game. If you do, shuffle all creature cards from your graveyard into your library.').
card_first_print('rooting kavu', 'INV').
card_image_name('rooting kavu'/'INV', 'rooting kavu').
card_uid('rooting kavu'/'INV', 'INV:Rooting Kavu:rooting kavu').
card_rarity('rooting kavu'/'INV', 'Uncommon').
card_artist('rooting kavu'/'INV', 'Heather Hudson').
card_number('rooting kavu'/'INV', '207').
card_multiverse_id('rooting kavu'/'INV', '23122').

card_in_set('rout', 'INV').
card_original_type('rout'/'INV', 'Sorcery').
card_original_text('rout'/'INV', 'You may play Rout any time you could play an instant if you pay {2} more to play it.\nDestroy all creatures. They can\'t be regenerated.').
card_first_print('rout', 'INV').
card_image_name('rout'/'INV', 'rout').
card_uid('rout'/'INV', 'INV:Rout:rout').
card_rarity('rout'/'INV', 'Rare').
card_artist('rout'/'INV', 'Ron Spencer').
card_number('rout'/'INV', '34').
card_multiverse_id('rout'/'INV', '22971').

card_in_set('ruby leech', 'INV').
card_original_type('ruby leech'/'INV', 'Creature — Leech').
card_original_text('ruby leech'/'INV', 'First strike\nRed spells you play cost {R} more to play.').
card_first_print('ruby leech', 'INV').
card_image_name('ruby leech'/'INV', 'ruby leech').
card_uid('ruby leech'/'INV', 'INV:Ruby Leech:ruby leech').
card_rarity('ruby leech'/'INV', 'Rare').
card_artist('ruby leech'/'INV', 'Jacques Bredy').
card_number('ruby leech'/'INV', '161').
card_flavor_text('ruby leech'/'INV', '\"Its gems didn\'t stop pulsating until they were completely removed.\"\n—Tolarian research notes').
card_multiverse_id('ruby leech'/'INV', '23095').

card_in_set('ruham djinn', 'INV').
card_original_type('ruham djinn'/'INV', 'Creature — Djinn').
card_original_text('ruham djinn'/'INV', 'First strike\nRuham Djinn gets -2/-2 as long as white is the most common color among all permanents or is tied for most common.').
card_first_print('ruham djinn', 'INV').
card_image_name('ruham djinn'/'INV', 'ruham djinn').
card_uid('ruham djinn'/'INV', 'INV:Ruham Djinn:ruham djinn').
card_rarity('ruham djinn'/'INV', 'Uncommon').
card_artist('ruham djinn'/'INV', 'Jeff Easley').
card_number('ruham djinn'/'INV', '35').
card_multiverse_id('ruham djinn'/'INV', '22949').

card_in_set('sabertooth nishoba', 'INV').
card_original_type('sabertooth nishoba'/'INV', 'Creature — Beast').
card_original_text('sabertooth nishoba'/'INV', 'Trample, protection from blue, protection from red').
card_first_print('sabertooth nishoba', 'INV').
card_image_name('sabertooth nishoba'/'INV', 'sabertooth nishoba').
card_uid('sabertooth nishoba'/'INV', 'INV:Sabertooth Nishoba:sabertooth nishoba').
card_rarity('sabertooth nishoba'/'INV', 'Rare').
card_artist('sabertooth nishoba'/'INV', 'Gary Ruddell').
card_number('sabertooth nishoba'/'INV', '268').
card_flavor_text('sabertooth nishoba'/'INV', 'They sneer at the terrestrial dangers found on peaks and shores, eager to prove themselves against new and even mightier foes.').
card_multiverse_id('sabertooth nishoba'/'INV', '23205').

card_in_set('salt marsh', 'INV').
card_original_type('salt marsh'/'INV', 'Land').
card_original_text('salt marsh'/'INV', 'Salt Marsh comes into play tapped.\n{T}: Add {U} or {B} to your mana pool.').
card_first_print('salt marsh', 'INV').
card_image_name('salt marsh'/'INV', 'salt marsh').
card_uid('salt marsh'/'INV', 'INV:Salt Marsh:salt marsh').
card_rarity('salt marsh'/'INV', 'Uncommon').
card_artist('salt marsh'/'INV', 'Jerry Tiritilli').
card_number('salt marsh'/'INV', '326').
card_flavor_text('salt marsh'/'INV', 'Only death breeds in stagnant water.\n—Urborg saying').
card_multiverse_id('salt marsh'/'INV', '23241').

card_in_set('samite archer', 'INV').
card_original_type('samite archer'/'INV', 'Creature — Cleric').
card_original_text('samite archer'/'INV', '{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.\n{T}: Samite Archer deals 1 damage to target creature or player.').
card_first_print('samite archer', 'INV').
card_image_name('samite archer'/'INV', 'samite archer').
card_uid('samite archer'/'INV', 'INV:Samite Archer:samite archer').
card_rarity('samite archer'/'INV', 'Uncommon').
card_artist('samite archer'/'INV', 'Scott M. Fischer').
card_number('samite archer'/'INV', '269').
card_flavor_text('samite archer'/'INV', '\"I can preserve ten lives by taking one.\"').
card_multiverse_id('samite archer'/'INV', '23165').

card_in_set('samite ministration', 'INV').
card_original_type('samite ministration'/'INV', 'Instant').
card_original_text('samite ministration'/'INV', 'Prevent all damage that would be dealt by a source of your choice to you this turn. Whenever damage from a black or red source is prevented this way, you gain life equal to that damage.').
card_first_print('samite ministration', 'INV').
card_image_name('samite ministration'/'INV', 'samite ministration').
card_uid('samite ministration'/'INV', 'INV:Samite Ministration:samite ministration').
card_rarity('samite ministration'/'INV', 'Uncommon').
card_artist('samite ministration'/'INV', 'Darrell Riche').
card_number('samite ministration'/'INV', '36').
card_multiverse_id('samite ministration'/'INV', '22954').

card_in_set('sapphire leech', 'INV').
card_original_type('sapphire leech'/'INV', 'Creature — Leech').
card_original_text('sapphire leech'/'INV', 'Flying\nBlue spells you play cost {U} more to play.').
card_first_print('sapphire leech', 'INV').
card_image_name('sapphire leech'/'INV', 'sapphire leech').
card_uid('sapphire leech'/'INV', 'INV:Sapphire Leech:sapphire leech').
card_rarity('sapphire leech'/'INV', 'Rare').
card_artist('sapphire leech'/'INV', 'Ron Spencer').
card_number('sapphire leech'/'INV', '71').
card_flavor_text('sapphire leech'/'INV', '\"The subject\'s wings are clearly vestigial. We suspect the gems somehow keep it aloft.\"\n—Tolarian research notes').
card_multiverse_id('sapphire leech'/'INV', '23010').

card_in_set('saproling infestation', 'INV').
card_original_type('saproling infestation'/'INV', 'Enchantment').
card_original_text('saproling infestation'/'INV', 'Whenever a player pays a kicker cost, you put a 1/1 green Saproling creature token into play.').
card_first_print('saproling infestation', 'INV').
card_image_name('saproling infestation'/'INV', 'saproling infestation').
card_uid('saproling infestation'/'INV', 'INV:Saproling Infestation:saproling infestation').
card_rarity('saproling infestation'/'INV', 'Rare').
card_artist('saproling infestation'/'INV', 'Heather Hudson').
card_number('saproling infestation'/'INV', '208').
card_flavor_text('saproling infestation'/'INV', '\"My army took centuries to gather,\" remarked Urza. \"Yavimaya seems to conjure hers out of thin air.\"').
card_multiverse_id('saproling infestation'/'INV', '25750').

card_in_set('saproling symbiosis', 'INV').
card_original_type('saproling symbiosis'/'INV', 'Sorcery').
card_original_text('saproling symbiosis'/'INV', 'You may play Saproling Symbiosis any time you could play an instant if you pay {2} more to play it.\nPut a 1/1 green Saproling creature token into play for each creature you control.').
card_first_print('saproling symbiosis', 'INV').
card_image_name('saproling symbiosis'/'INV', 'saproling symbiosis').
card_uid('saproling symbiosis'/'INV', 'INV:Saproling Symbiosis:saproling symbiosis').
card_rarity('saproling symbiosis'/'INV', 'Rare').
card_artist('saproling symbiosis'/'INV', 'Ciruelo').
card_number('saproling symbiosis'/'INV', '209').
card_multiverse_id('saproling symbiosis'/'INV', '24670').

card_in_set('savage offensive', 'INV').
card_original_type('savage offensive'/'INV', 'Sorcery').
card_original_text('savage offensive'/'INV', 'Kicker {G} (You may pay an additional {G} as you play this spell.)\nCreatures you control gain first strike until end of turn. If you paid the kicker cost, they get +1/+1 until end of turn.').
card_first_print('savage offensive', 'INV').
card_image_name('savage offensive'/'INV', 'savage offensive').
card_uid('savage offensive'/'INV', 'INV:Savage Offensive:savage offensive').
card_rarity('savage offensive'/'INV', 'Common').
card_artist('savage offensive'/'INV', 'Greg & Tim Hildebrandt').
card_number('savage offensive'/'INV', '162').
card_multiverse_id('savage offensive'/'INV', '23078').

card_in_set('scarred puma', 'INV').
card_original_type('scarred puma'/'INV', 'Creature — Cat').
card_original_text('scarred puma'/'INV', 'Scarred Puma can\'t attack unless a black or green creature also attacks.').
card_first_print('scarred puma', 'INV').
card_image_name('scarred puma'/'INV', 'scarred puma').
card_uid('scarred puma'/'INV', 'INV:Scarred Puma:scarred puma').
card_rarity('scarred puma'/'INV', 'Common').
card_artist('scarred puma'/'INV', 'Aaron Boyd').
card_number('scarred puma'/'INV', '163').
card_flavor_text('scarred puma'/'INV', 'It\'s not eager to lose the other eye.').
card_multiverse_id('scarred puma'/'INV', '23067').

card_in_set('scavenged weaponry', 'INV').
card_original_type('scavenged weaponry'/'INV', 'Enchant Creature').
card_original_text('scavenged weaponry'/'INV', 'When Scavenged Weaponry comes into play, draw a card.\nEnchanted creature gets +1/+1.').
card_first_print('scavenged weaponry', 'INV').
card_image_name('scavenged weaponry'/'INV', 'scavenged weaponry').
card_uid('scavenged weaponry'/'INV', 'INV:Scavenged Weaponry:scavenged weaponry').
card_rarity('scavenged weaponry'/'INV', 'Common').
card_artist('scavenged weaponry'/'INV', 'Alan Pollack').
card_number('scavenged weaponry'/'INV', '123').
card_flavor_text('scavenged weaponry'/'INV', '\"The Phyrexians have progressed,\" admired Urza. \"Their parts are interchangeable.\"').
card_multiverse_id('scavenged weaponry'/'INV', '23028').

card_in_set('scorching lava', 'INV').
card_original_type('scorching lava'/'INV', 'Instant').
card_original_text('scorching lava'/'INV', 'Kicker {R} (You may pay an additional {R} as you play this spell.)\nScorching Lava deals 2 damage to target creature or player. If you paid the kicker cost, that creature can\'t be regenerated this turn and if it would be put into a graveyard this turn, remove it from the game instead.').
card_first_print('scorching lava', 'INV').
card_image_name('scorching lava'/'INV', 'scorching lava').
card_uid('scorching lava'/'INV', 'INV:Scorching Lava:scorching lava').
card_rarity('scorching lava'/'INV', 'Common').
card_artist('scorching lava'/'INV', 'Mark Tedin').
card_number('scorching lava'/'INV', '164').
card_multiverse_id('scorching lava'/'INV', '23077').

card_in_set('scouting trek', 'INV').
card_original_type('scouting trek'/'INV', 'Sorcery').
card_original_text('scouting trek'/'INV', 'Search your library for any number of basic land cards, reveal them, and set them aside. Shuffle your library, then put those cards on top of it in any order.').
card_first_print('scouting trek', 'INV').
card_image_name('scouting trek'/'INV', 'scouting trek').
card_uid('scouting trek'/'INV', 'INV:Scouting Trek:scouting trek').
card_rarity('scouting trek'/'INV', 'Uncommon').
card_artist('scouting trek'/'INV', 'Stephanie Law').
card_number('scouting trek'/'INV', '210').
card_flavor_text('scouting trek'/'INV', '\"I have chosen my path. Who will walk it with me?\"\n—Eladamri').
card_multiverse_id('scouting trek'/'INV', '22317').

card_in_set('searing rays', 'INV').
card_original_type('searing rays'/'INV', 'Sorcery').
card_original_text('searing rays'/'INV', 'Choose a color. Searing Rays deals damage to each player equal to the number of creatures of that color that player controls.').
card_first_print('searing rays', 'INV').
card_image_name('searing rays'/'INV', 'searing rays').
card_uid('searing rays'/'INV', 'INV:Searing Rays:searing rays').
card_rarity('searing rays'/'INV', 'Uncommon').
card_artist('searing rays'/'INV', 'Doug Chaffee').
card_number('searing rays'/'INV', '165').
card_flavor_text('searing rays'/'INV', '\"This is a battle of pawns, for the highest stakes imaginable.\"\n—Urza').
card_multiverse_id('searing rays'/'INV', '25749').

card_in_set('seashell cameo', 'INV').
card_original_type('seashell cameo'/'INV', 'Artifact').
card_original_text('seashell cameo'/'INV', '{T}: Add {W} or {U} to your mana pool.').
card_first_print('seashell cameo', 'INV').
card_image_name('seashell cameo'/'INV', 'seashell cameo').
card_uid('seashell cameo'/'INV', 'INV:Seashell Cameo:seashell cameo').
card_rarity('seashell cameo'/'INV', 'Uncommon').
card_artist('seashell cameo'/'INV', 'Tony Szczudlo').
card_number('seashell cameo'/'INV', '311').
card_flavor_text('seashell cameo'/'INV', '\"Today a seashell fell from the empty sky here in Kinymu, a hundred leagues from the sea. I\'m torn—shall I carve a woman or a bird?\"\n—Isel, master carver').
card_multiverse_id('seashell cameo'/'INV', '23217').

card_in_set('seer\'s vision', 'INV').
card_original_type('seer\'s vision'/'INV', 'Enchantment').
card_original_text('seer\'s vision'/'INV', 'All opponents play with their hands revealed.\nSacrifice Seer\'s Vision: Look at target player\'s hand and choose a card from it. That player discards that card. Play this ability only any time you could play a sorcery.').
card_first_print('seer\'s vision', 'INV').
card_image_name('seer\'s vision'/'INV', 'seer\'s vision').
card_uid('seer\'s vision'/'INV', 'INV:Seer\'s Vision:seer\'s vision').
card_rarity('seer\'s vision'/'INV', 'Uncommon').
card_artist('seer\'s vision'/'INV', 'Rebecca Guay').
card_number('seer\'s vision'/'INV', '270').
card_multiverse_id('seer\'s vision'/'INV', '23178').

card_in_set('serpentine kavu', 'INV').
card_original_type('serpentine kavu'/'INV', 'Creature — Kavu').
card_original_text('serpentine kavu'/'INV', '{R} Serpentine Kavu gains haste until end of turn. (It may attack and {T} the turn it comes under your control.)').
card_first_print('serpentine kavu', 'INV').
card_image_name('serpentine kavu'/'INV', 'serpentine kavu').
card_uid('serpentine kavu'/'INV', 'INV:Serpentine Kavu:serpentine kavu').
card_rarity('serpentine kavu'/'INV', 'Common').
card_artist('serpentine kavu'/'INV', 'Heather Hudson').
card_number('serpentine kavu'/'INV', '211').
card_flavor_text('serpentine kavu'/'INV', 'Under Yavimaya\'s peaceful facade beat the hearts of many savage beasts.').
card_multiverse_id('serpentine kavu'/'INV', '23105').

card_in_set('shackles', 'INV').
card_original_type('shackles'/'INV', 'Enchant Creature').
card_original_text('shackles'/'INV', 'Enchanted creature doesn\'t untap during its controller\'s untap step.\n{W} Return Shackles to its owner\'s hand.').
card_image_name('shackles'/'INV', 'shackles').
card_uid('shackles'/'INV', 'INV:Shackles:shackles').
card_rarity('shackles'/'INV', 'Common').
card_artist('shackles'/'INV', 'Greg Staples').
card_number('shackles'/'INV', '37').
card_flavor_text('shackles'/'INV', '\"It could be worse,\" said Gerrard, looking around. \"Well, maybe not.\"').
card_multiverse_id('shackles'/'INV', '22942').

card_in_set('shimmering wings', 'INV').
card_original_type('shimmering wings'/'INV', 'Enchant Creature').
card_original_text('shimmering wings'/'INV', 'Enchanted creature has flying.\n{U} Return Shimmering Wings to its owner\'s hand.').
card_image_name('shimmering wings'/'INV', 'shimmering wings').
card_uid('shimmering wings'/'INV', 'INV:Shimmering Wings:shimmering wings').
card_rarity('shimmering wings'/'INV', 'Common').
card_artist('shimmering wings'/'INV', 'Carl Critchlow').
card_number('shimmering wings'/'INV', '72').
card_flavor_text('shimmering wings'/'INV', '\"Wings are but fins for swimming the sky.\"\n—Empress Galina').
card_multiverse_id('shimmering wings'/'INV', '22981').

card_in_set('shivan emissary', 'INV').
card_original_type('shivan emissary'/'INV', 'Creature — Wizard').
card_original_text('shivan emissary'/'INV', 'Kicker {1}{B} (You may pay an additional {1}{B} as you play this spell.)\nWhen Shivan Emissary comes into play, if you paid the kicker cost, destroy target nonblack creature. It can\'t be regenerated.').
card_first_print('shivan emissary', 'INV').
card_image_name('shivan emissary'/'INV', 'shivan emissary').
card_uid('shivan emissary'/'INV', 'INV:Shivan Emissary:shivan emissary').
card_rarity('shivan emissary'/'INV', 'Uncommon').
card_artist('shivan emissary'/'INV', 'Paolo Parente').
card_number('shivan emissary'/'INV', '166').
card_multiverse_id('shivan emissary'/'INV', '23084').

card_in_set('shivan harvest', 'INV').
card_original_type('shivan harvest'/'INV', 'Enchantment').
card_original_text('shivan harvest'/'INV', '{1}{R}, Sacrifice a creature: Destroy target nonbasic land.').
card_first_print('shivan harvest', 'INV').
card_image_name('shivan harvest'/'INV', 'shivan harvest').
card_uid('shivan harvest'/'INV', 'INV:Shivan Harvest:shivan harvest').
card_rarity('shivan harvest'/'INV', 'Uncommon').
card_artist('shivan harvest'/'INV', 'Daren Bader').
card_number('shivan harvest'/'INV', '167').
card_flavor_text('shivan harvest'/'INV', '\"The blood of your ancestors ran heavy on this soil. Now it\'s your turn to sacrifice for the glory of Shiv.\"\n—Viashino heretic').
card_multiverse_id('shivan harvest'/'INV', '23091').

card_in_set('shivan oasis', 'INV').
card_original_type('shivan oasis'/'INV', 'Land').
card_original_text('shivan oasis'/'INV', 'Shivan Oasis comes into play tapped.\n{T}: Add {R} or {G} to your mana pool.').
card_first_print('shivan oasis', 'INV').
card_image_name('shivan oasis'/'INV', 'shivan oasis').
card_uid('shivan oasis'/'INV', 'INV:Shivan Oasis:shivan oasis').
card_rarity('shivan oasis'/'INV', 'Uncommon').
card_artist('shivan oasis'/'INV', 'Rob Alexander').
card_number('shivan oasis'/'INV', '327').
card_flavor_text('shivan oasis'/'INV', 'Only the hardiest explorers survive to eat the fruit.').
card_multiverse_id('shivan oasis'/'INV', '23243').

card_in_set('shivan zombie', 'INV').
card_original_type('shivan zombie'/'INV', 'Creature — Barbarian Zombie').
card_original_text('shivan zombie'/'INV', 'Protection from white').
card_first_print('shivan zombie', 'INV').
card_image_name('shivan zombie'/'INV', 'shivan zombie').
card_uid('shivan zombie'/'INV', 'INV:Shivan Zombie:shivan zombie').
card_rarity('shivan zombie'/'INV', 'Common').
card_artist('shivan zombie'/'INV', 'Tony Szczudlo').
card_number('shivan zombie'/'INV', '271').
card_flavor_text('shivan zombie'/'INV', 'Barbarians long for a glorious death in battle. Phyrexia was eager to grant that wish.').
card_multiverse_id('shivan zombie'/'INV', '23160').

card_in_set('shoreline raider', 'INV').
card_original_type('shoreline raider'/'INV', 'Creature — Merfolk').
card_original_text('shoreline raider'/'INV', 'Protection from Kavu').
card_first_print('shoreline raider', 'INV').
card_image_name('shoreline raider'/'INV', 'shoreline raider').
card_uid('shoreline raider'/'INV', 'INV:Shoreline Raider:shoreline raider').
card_rarity('shoreline raider'/'INV', 'Common').
card_artist('shoreline raider'/'INV', 'Nelson DeCastro').
card_number('shoreline raider'/'INV', '73').
card_flavor_text('shoreline raider'/'INV', '\"This strange new beast makes for an excellent meal. Get me more.\"\n—Empress Galina').
card_multiverse_id('shoreline raider'/'INV', '22978').

card_in_set('simoon', 'INV').
card_original_type('simoon'/'INV', 'Instant').
card_original_text('simoon'/'INV', 'Simoon deals 1 damage to each creature target opponent controls.').
card_image_name('simoon'/'INV', 'simoon').
card_uid('simoon'/'INV', 'INV:Simoon:simoon').
card_rarity('simoon'/'INV', 'Uncommon').
card_artist('simoon'/'INV', 'Tony Szczudlo').
card_number('simoon'/'INV', '272').
card_flavor_text('simoon'/'INV', 'Hot wind whipped up as if the weather itself wanted to fight Phyrexians.').
card_multiverse_id('simoon'/'INV', '23175').

card_in_set('skittish kavu', 'INV').
card_original_type('skittish kavu'/'INV', 'Creature — Kavu').
card_original_text('skittish kavu'/'INV', 'Skittish Kavu gets +1/+1 as long as no opponent controls a white or blue creature.').
card_first_print('skittish kavu', 'INV').
card_image_name('skittish kavu'/'INV', 'skittish kavu').
card_uid('skittish kavu'/'INV', 'INV:Skittish Kavu:skittish kavu').
card_rarity('skittish kavu'/'INV', 'Uncommon').
card_artist('skittish kavu'/'INV', 'Pete Venters').
card_number('skittish kavu'/'INV', '168').
card_flavor_text('skittish kavu'/'INV', 'Fierce when roused, this breed of kavu is easily distracted by a strong voice or melodic tune.').
card_multiverse_id('skittish kavu'/'INV', '23080').

card_in_set('skizzik', 'INV').
card_original_type('skizzik'/'INV', 'Creature — Elemental').
card_original_text('skizzik'/'INV', 'Kicker {R} (You may pay an additional {R} as you play this spell.)\nTrample; haste (This creature may attack and {T} the turn it comes under your control.)\nAt end of turn, sacrifice Skizzik unless the kicker cost was paid.').
card_first_print('skizzik', 'INV').
card_image_name('skizzik'/'INV', 'skizzik').
card_uid('skizzik'/'INV', 'INV:Skizzik:skizzik').
card_rarity('skizzik'/'INV', 'Rare').
card_artist('skizzik'/'INV', 'Ron Spencer').
card_number('skizzik'/'INV', '169').
card_multiverse_id('skizzik'/'INV', '23096').

card_in_set('sky weaver', 'INV').
card_original_type('sky weaver'/'INV', 'Creature — Wizard').
card_original_text('sky weaver'/'INV', '{2}: Target white or black creature gains flying until end of turn.').
card_first_print('sky weaver', 'INV').
card_image_name('sky weaver'/'INV', 'sky weaver').
card_uid('sky weaver'/'INV', 'INV:Sky Weaver:sky weaver').
card_rarity('sky weaver'/'INV', 'Uncommon').
card_artist('sky weaver'/'INV', 'Christopher Moeller').
card_number('sky weaver'/'INV', '74').
card_flavor_text('sky weaver'/'INV', '\"Let my wisdom give you wings.\"').
card_multiverse_id('sky weaver'/'INV', '22996').

card_in_set('sleeper\'s robe', 'INV').
card_original_type('sleeper\'s robe'/'INV', 'Enchant Creature').
card_original_text('sleeper\'s robe'/'INV', 'Enchanted creature can\'t be blocked except by artifact creatures and/or black creatures.\nWhenever enchanted creature deals combat damage to an opponent, you may draw a card.').
card_first_print('sleeper\'s robe', 'INV').
card_image_name('sleeper\'s robe'/'INV', 'sleeper\'s robe').
card_uid('sleeper\'s robe'/'INV', 'INV:Sleeper\'s Robe:sleeper\'s robe').
card_rarity('sleeper\'s robe'/'INV', 'Uncommon').
card_artist('sleeper\'s robe'/'INV', 'Alan Pollack').
card_number('sleeper\'s robe'/'INV', '273').
card_multiverse_id('sleeper\'s robe'/'INV', '23168').

card_in_set('slimy kavu', 'INV').
card_original_type('slimy kavu'/'INV', 'Creature — Kavu').
card_original_text('slimy kavu'/'INV', '{T}: Target land becomes a swamp until end of turn.').
card_first_print('slimy kavu', 'INV').
card_image_name('slimy kavu'/'INV', 'slimy kavu').
card_uid('slimy kavu'/'INV', 'INV:Slimy Kavu:slimy kavu').
card_rarity('slimy kavu'/'INV', 'Common').
card_artist('slimy kavu'/'INV', 'Randy Gallegos').
card_number('slimy kavu'/'INV', '170').
card_flavor_text('slimy kavu'/'INV', 'Its slime liquefies the ground as efficiently as its fangs shred prey.').
card_multiverse_id('slimy kavu'/'INV', '23068').

card_in_set('slinking serpent', 'INV').
card_original_type('slinking serpent'/'INV', 'Creature — Serpent').
card_original_text('slinking serpent'/'INV', 'Forestwalk (This creature is unblockable as long as defending player controls a forest.)').
card_first_print('slinking serpent', 'INV').
card_image_name('slinking serpent'/'INV', 'slinking serpent').
card_uid('slinking serpent'/'INV', 'INV:Slinking Serpent:slinking serpent').
card_rarity('slinking serpent'/'INV', 'Uncommon').
card_artist('slinking serpent'/'INV', 'Wayne England').
card_number('slinking serpent'/'INV', '274').
card_flavor_text('slinking serpent'/'INV', 'It winds its way through undergrowth as easily as it swims through shallows.').
card_multiverse_id('slinking serpent'/'INV', '23149').

card_in_set('smoldering tar', 'INV').
card_original_type('smoldering tar'/'INV', 'Enchantment').
card_original_text('smoldering tar'/'INV', 'At the beginning of your upkeep, target player loses 1 life.\nSacrifice Smoldering Tar: Smoldering Tar deals 4 damage to target creature. Play this ability only any time you could play a sorcery.').
card_first_print('smoldering tar', 'INV').
card_image_name('smoldering tar'/'INV', 'smoldering tar').
card_uid('smoldering tar'/'INV', 'INV:Smoldering Tar:smoldering tar').
card_rarity('smoldering tar'/'INV', 'Uncommon').
card_artist('smoldering tar'/'INV', 'David Day').
card_number('smoldering tar'/'INV', '275').
card_multiverse_id('smoldering tar'/'INV', '23179').

card_in_set('soul burn', 'INV').
card_original_type('soul burn'/'INV', 'Sorcery').
card_original_text('soul burn'/'INV', 'Spend only black and/or red mana on X.\nSoul Burn deals X damage to target creature or player. You gain life equal to the damage dealt, but not more than the amount of {B} spent on X, the player\'s life total before Soul Burn dealt damage, or the creature\'s toughness.').
card_image_name('soul burn'/'INV', 'soul burn').
card_uid('soul burn'/'INV', 'INV:Soul Burn:soul burn').
card_rarity('soul burn'/'INV', 'Common').
card_artist('soul burn'/'INV', 'Andrew Goldhawk').
card_number('soul burn'/'INV', '124').
card_multiverse_id('soul burn'/'INV', '23026').

card_in_set('sparring golem', 'INV').
card_original_type('sparring golem'/'INV', 'Artifact Creature — Golem').
card_original_text('sparring golem'/'INV', 'Whenever Sparring Golem becomes blocked, it gets +1/+1 until end of turn for each creature blocking it.').
card_first_print('sparring golem', 'INV').
card_image_name('sparring golem'/'INV', 'sparring golem').
card_uid('sparring golem'/'INV', 'INV:Sparring Golem:sparring golem').
card_rarity('sparring golem'/'INV', 'Uncommon').
card_artist('sparring golem'/'INV', 'Adam Rex').
card_number('sparring golem'/'INV', '312').
card_flavor_text('sparring golem'/'INV', '\"Part drill sergeant, part training dummy,\" thought Gerrard. \"I hope it can stand up to a real war.\"').
card_multiverse_id('sparring golem'/'INV', '23225').

card_in_set('spinal embrace', 'INV').
card_original_type('spinal embrace'/'INV', 'Instant').
card_original_text('spinal embrace'/'INV', 'Play Spinal Embrace only during combat.\nUntap target creature you don\'t control and gain control of it. It gains haste until end of turn. At end of turn, sacrifice it. If you do, you gain life equal to its toughness. (The creature may attack and {T} the turn it comes under your control.)').
card_first_print('spinal embrace', 'INV').
card_image_name('spinal embrace'/'INV', 'spinal embrace').
card_uid('spinal embrace'/'INV', 'INV:Spinal Embrace:spinal embrace').
card_rarity('spinal embrace'/'INV', 'Rare').
card_artist('spinal embrace'/'INV', 'Donato Giancola').
card_number('spinal embrace'/'INV', '276').
card_multiverse_id('spinal embrace'/'INV', '23189').

card_in_set('spirit of resistance', 'INV').
card_original_type('spirit of resistance'/'INV', 'Enchantment').
card_original_text('spirit of resistance'/'INV', 'If you control a permanent of each color, prevent all damage that would be dealt to you.').
card_first_print('spirit of resistance', 'INV').
card_image_name('spirit of resistance'/'INV', 'spirit of resistance').
card_uid('spirit of resistance'/'INV', 'INV:Spirit of Resistance:spirit of resistance').
card_rarity('spirit of resistance'/'INV', 'Rare').
card_artist('spirit of resistance'/'INV', 'John Avon').
card_number('spirit of resistance'/'INV', '38').
card_flavor_text('spirit of resistance'/'INV', '\"Our victory must come from all of Dominaria, or it will not come.\"\n—Urza').
card_multiverse_id('spirit of resistance'/'INV', '22968').

card_in_set('spirit weaver', 'INV').
card_original_type('spirit weaver'/'INV', 'Creature — Wizard').
card_original_text('spirit weaver'/'INV', '{2}: Target green or blue creature gets +0/+1 until end of turn.').
card_first_print('spirit weaver', 'INV').
card_image_name('spirit weaver'/'INV', 'spirit weaver').
card_uid('spirit weaver'/'INV', 'INV:Spirit Weaver:spirit weaver').
card_rarity('spirit weaver'/'INV', 'Uncommon').
card_artist('spirit weaver'/'INV', 'Matthew D. Wilson').
card_number('spirit weaver'/'INV', '39').
card_flavor_text('spirit weaver'/'INV', '\"Let my hope be your shield.\"').
card_multiverse_id('spirit weaver'/'INV', '22950').

card_in_set('spite', 'INV').
card_original_type('spite'/'INV', 'Instant').
card_original_text('spite'/'INV', 'Counter target noncreature spell.').
card_first_print('spite', 'INV').
card_image_name('spite'/'INV', 'spitemalice').
card_uid('spite'/'INV', 'INV:Spite:spitemalice').
card_rarity('spite'/'INV', 'Uncommon').
card_artist('spite'/'INV', 'David Martin').
card_number('spite'/'INV', '293a').
card_multiverse_id('spite'/'INV', '20576').

card_in_set('spreading plague', 'INV').
card_original_type('spreading plague'/'INV', 'Enchantment').
card_original_text('spreading plague'/'INV', 'Whenever a creature comes into play, destroy all other creatures that share a color with it. They can\'t be regenerated.').
card_first_print('spreading plague', 'INV').
card_image_name('spreading plague'/'INV', 'spreading plague').
card_uid('spreading plague'/'INV', 'INV:Spreading Plague:spreading plague').
card_rarity('spreading plague'/'INV', 'Rare').
card_artist('spreading plague'/'INV', 'Scott Bailey').
card_number('spreading plague'/'INV', '125').
card_flavor_text('spreading plague'/'INV', 'The cruelest strain of plague keeps its hosts alive long enough to return home and infect their families.').
card_multiverse_id('spreading plague'/'INV', '23057').

card_in_set('stalking assassin', 'INV').
card_original_type('stalking assassin'/'INV', 'Creature — Assassin').
card_original_text('stalking assassin'/'INV', '{3}{U}, {T}: Tap target creature.\n{3}{B}, {T}: Destroy target tapped creature.').
card_first_print('stalking assassin', 'INV').
card_image_name('stalking assassin'/'INV', 'stalking assassin').
card_uid('stalking assassin'/'INV', 'INV:Stalking Assassin:stalking assassin').
card_rarity('stalking assassin'/'INV', 'Rare').
card_artist('stalking assassin'/'INV', 'Dana Knutson').
card_number('stalking assassin'/'INV', '277').
card_multiverse_id('stalking assassin'/'INV', '25752').

card_in_set('stand', 'INV').
card_original_type('stand'/'INV', 'Instant').
card_original_text('stand'/'INV', 'Prevent the next 2 damage that would be dealt to target creature this turn.').
card_first_print('stand', 'INV').
card_image_name('stand'/'INV', 'standdeliver').
card_uid('stand'/'INV', 'INV:Stand:standdeliver').
card_rarity('stand'/'INV', 'Uncommon').
card_artist('stand'/'INV', 'David Martin').
card_number('stand'/'INV', '292a').
card_multiverse_id('stand'/'INV', '20574').

card_in_set('stand or fall', 'INV').
card_original_type('stand or fall'/'INV', 'Enchantment').
card_original_text('stand or fall'/'INV', 'At the beginning of your combat phase, separate all creatures defending player controls into two face-up piles. Only creatures in the pile of that player\'s choice may block this turn.').
card_first_print('stand or fall', 'INV').
card_image_name('stand or fall'/'INV', 'stand or fall').
card_uid('stand or fall'/'INV', 'INV:Stand or Fall:stand or fall').
card_rarity('stand or fall'/'INV', 'Rare').
card_artist('stand or fall'/'INV', 'Matt Cavotta').
card_number('stand or fall'/'INV', '171').
card_multiverse_id('stand or fall'/'INV', '26389').

card_in_set('sterling grove', 'INV').
card_original_type('sterling grove'/'INV', 'Enchantment').
card_original_text('sterling grove'/'INV', 'All other enchantments you control can\'t be the targets of spells or abilities.\n{1}, Sacrifice Sterling Grove: Search your library for an enchantment card and reveal that card. Shuffle your library, then put the card on top of it.').
card_first_print('sterling grove', 'INV').
card_image_name('sterling grove'/'INV', 'sterling grove').
card_uid('sterling grove'/'INV', 'INV:Sterling Grove:sterling grove').
card_rarity('sterling grove'/'INV', 'Uncommon').
card_artist('sterling grove'/'INV', 'Jeff Miracola').
card_number('sterling grove'/'INV', '278').
card_multiverse_id('sterling grove'/'INV', '23181').

card_in_set('stormscape apprentice', 'INV').
card_original_type('stormscape apprentice'/'INV', 'Creature — Wizard').
card_original_text('stormscape apprentice'/'INV', '{W}, {T}: Tap target creature.\n{B}, {T}: Target player loses 1 life.').
card_first_print('stormscape apprentice', 'INV').
card_image_name('stormscape apprentice'/'INV', 'stormscape apprentice').
card_uid('stormscape apprentice'/'INV', 'INV:Stormscape Apprentice:stormscape apprentice').
card_rarity('stormscape apprentice'/'INV', 'Common').
card_artist('stormscape apprentice'/'INV', 'D. Alexander Gregory').
card_number('stormscape apprentice'/'INV', '75').
card_multiverse_id('stormscape apprentice'/'INV', '22972').

card_in_set('stormscape master', 'INV').
card_original_type('stormscape master'/'INV', 'Creature — Wizard').
card_original_text('stormscape master'/'INV', '{W}{W}, {T}: Target creature gains protection from the color of your choice until end of turn.\n{B}{B}, {T}: Target player loses 2 life and you gain 2 life.').
card_first_print('stormscape master', 'INV').
card_image_name('stormscape master'/'INV', 'stormscape master').
card_uid('stormscape master'/'INV', 'INV:Stormscape Master:stormscape master').
card_rarity('stormscape master'/'INV', 'Rare').
card_artist('stormscape master'/'INV', 'Hannibal King').
card_number('stormscape master'/'INV', '76').
card_multiverse_id('stormscape master'/'INV', '23005').

card_in_set('strength of unity', 'INV').
card_original_type('strength of unity'/'INV', 'Enchant Creature').
card_original_text('strength of unity'/'INV', 'Enchanted creature gets +1/+1 for each basic land type among lands you control.').
card_first_print('strength of unity', 'INV').
card_image_name('strength of unity'/'INV', 'strength of unity').
card_uid('strength of unity'/'INV', 'INV:Strength of Unity:strength of unity').
card_rarity('strength of unity'/'INV', 'Common').
card_artist('strength of unity'/'INV', 'Andrew Goldhawk').
card_number('strength of unity'/'INV', '40').
card_flavor_text('strength of unity'/'INV', 'All Dominarians agreed on one thing: they would not go down without a fight.').
card_multiverse_id('strength of unity'/'INV', '22939').

card_in_set('stun', 'INV').
card_original_type('stun'/'INV', 'Instant').
card_original_text('stun'/'INV', 'Target creature can\'t block this turn.\nDraw a card.').
card_image_name('stun'/'INV', 'stun').
card_uid('stun'/'INV', 'INV:Stun:stun').
card_rarity('stun'/'INV', 'Common').
card_artist('stun'/'INV', 'Mike Ploog').
card_number('stun'/'INV', '172').
card_flavor_text('stun'/'INV', '\"They certainly are well protected. Too bad for them they\'re not well balanced.\"\n—Sisay').
card_multiverse_id('stun'/'INV', '23073').

card_in_set('suffering', 'INV').
card_original_type('suffering'/'INV', 'Sorcery').
card_original_text('suffering'/'INV', 'Destroy target land.').
card_first_print('suffering', 'INV').
card_image_name('suffering'/'INV', 'painsuffering').
card_uid('suffering'/'INV', 'INV:Suffering:painsuffering').
card_rarity('suffering'/'INV', 'Uncommon').
card_artist('suffering'/'INV', 'David Martin').
card_number('suffering'/'INV', '294b').
card_multiverse_id('suffering'/'INV', '20578').

card_in_set('sulam djinn', 'INV').
card_original_type('sulam djinn'/'INV', 'Creature — Djinn').
card_original_text('sulam djinn'/'INV', 'Trample\nSulam Djinn gets -2/-2 as long as green is the most common color among all permanents or is tied for most common.').
card_first_print('sulam djinn', 'INV').
card_image_name('sulam djinn'/'INV', 'sulam djinn').
card_uid('sulam djinn'/'INV', 'INV:Sulam Djinn:sulam djinn').
card_rarity('sulam djinn'/'INV', 'Uncommon').
card_artist('sulam djinn'/'INV', 'Greg & Tim Hildebrandt').
card_number('sulam djinn'/'INV', '212').
card_multiverse_id('sulam djinn'/'INV', '23126').

card_in_set('sulfur vent', 'INV').
card_original_type('sulfur vent'/'INV', 'Land').
card_original_text('sulfur vent'/'INV', 'Sulfur Vent comes into play tapped.\n{T}: Add {B} to your mana pool.\n{T}, Sacrifice Sulfur Vent: Add {U}{R} to your mana pool.').
card_first_print('sulfur vent', 'INV').
card_image_name('sulfur vent'/'INV', 'sulfur vent').
card_uid('sulfur vent'/'INV', 'INV:Sulfur Vent:sulfur vent').
card_rarity('sulfur vent'/'INV', 'Common').
card_artist('sulfur vent'/'INV', 'Edward P. Beard, Jr.').
card_number('sulfur vent'/'INV', '328').
card_multiverse_id('sulfur vent'/'INV', '23237').

card_in_set('sunscape apprentice', 'INV').
card_original_type('sunscape apprentice'/'INV', 'Creature — Wizard').
card_original_text('sunscape apprentice'/'INV', '{G}, {T}: Target creature gets +1/+1 until end of turn.\n{U}, {T}: Put target creature you control on top of its owner\'s library.').
card_first_print('sunscape apprentice', 'INV').
card_image_name('sunscape apprentice'/'INV', 'sunscape apprentice').
card_uid('sunscape apprentice'/'INV', 'INV:Sunscape Apprentice:sunscape apprentice').
card_rarity('sunscape apprentice'/'INV', 'Common').
card_artist('sunscape apprentice'/'INV', 'Stephanie Law').
card_number('sunscape apprentice'/'INV', '41').
card_multiverse_id('sunscape apprentice'/'INV', '22928').

card_in_set('sunscape master', 'INV').
card_original_type('sunscape master'/'INV', 'Creature — Wizard').
card_original_text('sunscape master'/'INV', '{G}{G}, {T}: Creatures you control get +2/+2 until end of turn.\n{U}{U}, {T}: Return target creature to its owner\'s hand.').
card_first_print('sunscape master', 'INV').
card_image_name('sunscape master'/'INV', 'sunscape master').
card_uid('sunscape master'/'INV', 'INV:Sunscape Master:sunscape master').
card_rarity('sunscape master'/'INV', 'Rare').
card_artist('sunscape master'/'INV', 'Alan Rabinowitz').
card_number('sunscape master'/'INV', '42').
card_multiverse_id('sunscape master'/'INV', '22961').

card_in_set('swamp', 'INV').
card_original_type('swamp'/'INV', 'Land').
card_original_text('swamp'/'INV', 'B').
card_image_name('swamp'/'INV', 'swamp1').
card_uid('swamp'/'INV', 'INV:Swamp:swamp1').
card_rarity('swamp'/'INV', 'Basic Land').
card_artist('swamp'/'INV', 'Ron Spencer').
card_number('swamp'/'INV', '339').
card_multiverse_id('swamp'/'INV', '25965').

card_in_set('swamp', 'INV').
card_original_type('swamp'/'INV', 'Land').
card_original_text('swamp'/'INV', 'B').
card_image_name('swamp'/'INV', 'swamp2').
card_uid('swamp'/'INV', 'INV:Swamp:swamp2').
card_rarity('swamp'/'INV', 'Basic Land').
card_artist('swamp'/'INV', 'Rob Alexander').
card_number('swamp'/'INV', '340').
card_multiverse_id('swamp'/'INV', '26304').

card_in_set('swamp', 'INV').
card_original_type('swamp'/'INV', 'Land').
card_original_text('swamp'/'INV', 'B').
card_image_name('swamp'/'INV', 'swamp3').
card_uid('swamp'/'INV', 'INV:Swamp:swamp3').
card_rarity('swamp'/'INV', 'Basic Land').
card_artist('swamp'/'INV', 'Rob Alexander').
card_number('swamp'/'INV', '341').
card_multiverse_id('swamp'/'INV', '26305').

card_in_set('swamp', 'INV').
card_original_type('swamp'/'INV', 'Land').
card_original_text('swamp'/'INV', 'B').
card_image_name('swamp'/'INV', 'swamp4').
card_uid('swamp'/'INV', 'INV:Swamp:swamp4').
card_rarity('swamp'/'INV', 'Basic Land').
card_artist('swamp'/'INV', 'Ron Spencer').
card_number('swamp'/'INV', '342').
card_multiverse_id('swamp'/'INV', '26306').

card_in_set('sway of illusion', 'INV').
card_original_type('sway of illusion'/'INV', 'Instant').
card_original_text('sway of illusion'/'INV', 'Any number of target creatures become the color of your choice until end of turn.\nDraw a card.').
card_first_print('sway of illusion', 'INV').
card_image_name('sway of illusion'/'INV', 'sway of illusion').
card_uid('sway of illusion'/'INV', 'INV:Sway of Illusion:sway of illusion').
card_rarity('sway of illusion'/'INV', 'Uncommon').
card_artist('sway of illusion'/'INV', 'Greg & Tim Hildebrandt').
card_number('sway of illusion'/'INV', '77').
card_flavor_text('sway of illusion'/'INV', '\"I suggest you take a closer look.\"\n—Tidal visionary').
card_multiverse_id('sway of illusion'/'INV', '22999').

card_in_set('tainted well', 'INV').
card_original_type('tainted well'/'INV', 'Enchant Land').
card_original_text('tainted well'/'INV', 'When Tainted Well comes into play, draw a card.\nEnchanted land is a swamp.').
card_first_print('tainted well', 'INV').
card_image_name('tainted well'/'INV', 'tainted well').
card_uid('tainted well'/'INV', 'INV:Tainted Well:tainted well').
card_rarity('tainted well'/'INV', 'Common').
card_artist('tainted well'/'INV', 'Val Mayerik').
card_number('tainted well'/'INV', '126').
card_flavor_text('tainted well'/'INV', '\"No, you get the water. I ain\'t thirsty.\"\n—Squee').
card_multiverse_id('tainted well'/'INV', '25748').

card_in_set('tangle', 'INV').
card_original_type('tangle'/'INV', 'Instant').
card_original_text('tangle'/'INV', 'Prevent all combat damage that would be dealt this turn.\nAttacking creatures don\'t untap during their controllers\' next untap steps.').
card_first_print('tangle', 'INV').
card_image_name('tangle'/'INV', 'tangle').
card_uid('tangle'/'INV', 'INV:Tangle:tangle').
card_rarity('tangle'/'INV', 'Uncommon').
card_artist('tangle'/'INV', 'John Avon').
card_number('tangle'/'INV', '213').
card_flavor_text('tangle'/'INV', 'Gaea battles rampant death with abundant life.').
card_multiverse_id('tangle'/'INV', '23115').

card_in_set('tectonic instability', 'INV').
card_original_type('tectonic instability'/'INV', 'Enchantment').
card_original_text('tectonic instability'/'INV', 'Whenever a land comes into play, tap all lands its controller controls.').
card_first_print('tectonic instability', 'INV').
card_image_name('tectonic instability'/'INV', 'tectonic instability').
card_uid('tectonic instability'/'INV', 'INV:Tectonic Instability:tectonic instability').
card_rarity('tectonic instability'/'INV', 'Rare').
card_artist('tectonic instability'/'INV', 'Rob Alexander').
card_number('tectonic instability'/'INV', '173').
card_flavor_text('tectonic instability'/'INV', '\"Think of the problems we\'ll face if you uproot entire continents,\" said Urza.\n\"I have,\" replied Teferi, \"but the alternative is worse.\"').
card_multiverse_id('tectonic instability'/'INV', '23100').

card_in_set('teferi\'s care', 'INV').
card_original_type('teferi\'s care'/'INV', 'Enchantment').
card_original_text('teferi\'s care'/'INV', '{W}, Sacrifice an enchantment: Destroy target enchantment.\n{3}{U}{U} Counter target enchantment spell.').
card_first_print('teferi\'s care', 'INV').
card_image_name('teferi\'s care'/'INV', 'teferi\'s care').
card_uid('teferi\'s care'/'INV', 'INV:Teferi\'s Care:teferi\'s care').
card_rarity('teferi\'s care'/'INV', 'Uncommon').
card_artist('teferi\'s care'/'INV', 'Scott Bailey').
card_number('teferi\'s care'/'INV', '43').
card_flavor_text('teferi\'s care'/'INV', '\"If I do nothing else, I will protect my people.\"\n—Teferi').
card_multiverse_id('teferi\'s care'/'INV', '22959').

card_in_set('teferi\'s moat', 'INV').
card_original_type('teferi\'s moat'/'INV', 'Enchantment').
card_original_text('teferi\'s moat'/'INV', 'As Teferi\'s Moat comes into play, choose a color.\nCreatures of the chosen color without flying can\'t attack you.').
card_first_print('teferi\'s moat', 'INV').
card_image_name('teferi\'s moat'/'INV', 'teferi\'s moat').
card_uid('teferi\'s moat'/'INV', 'INV:Teferi\'s Moat:teferi\'s moat').
card_rarity('teferi\'s moat'/'INV', 'Rare').
card_artist('teferi\'s moat'/'INV', 'rk post').
card_number('teferi\'s moat'/'INV', '279').
card_flavor_text('teferi\'s moat'/'INV', '\"Isolation is not the answer.\"\n—Urza, to Teferi.').
card_multiverse_id('teferi\'s moat'/'INV', '23167').

card_in_set('teferi\'s response', 'INV').
card_original_type('teferi\'s response'/'INV', 'Instant').
card_original_text('teferi\'s response'/'INV', 'Counter target spell or ability an opponent controls that targets a land you control. If a permanent\'s ability is countered this way, destroy that permanent.\nDraw two cards.').
card_first_print('teferi\'s response', 'INV').
card_image_name('teferi\'s response'/'INV', 'teferi\'s response').
card_uid('teferi\'s response'/'INV', 'INV:Teferi\'s Response:teferi\'s response').
card_rarity('teferi\'s response'/'INV', 'Rare').
card_artist('teferi\'s response'/'INV', 'Scott Bailey').
card_number('teferi\'s response'/'INV', '78').
card_multiverse_id('teferi\'s response'/'INV', '23015').

card_in_set('tek', 'INV').
card_original_type('tek'/'INV', 'Artifact Creature — Dragon').
card_original_text('tek'/'INV', 'Tek gets +0/+2 as long as you control a plains, has flying as long as you control an island, gets +2/+0 as long as you control a swamp, has first strike as long as you control a mountain, and has trample as long as you control a forest.').
card_first_print('tek', 'INV').
card_image_name('tek'/'INV', 'tek').
card_uid('tek'/'INV', 'INV:Tek:tek').
card_rarity('tek'/'INV', 'Rare').
card_artist('tek'/'INV', 'Chippy').
card_number('tek'/'INV', '313').
card_multiverse_id('tek'/'INV', '23229').

card_in_set('temporal distortion', 'INV').
card_original_type('temporal distortion'/'INV', 'Enchantment').
card_original_text('temporal distortion'/'INV', 'Whenever a creature or land becomes tapped, put an hourglass counter on it.\nPermanents with an hourglass counter on them don\'t untap during their controllers\' untap steps.\nAt the beginning of each player\'s upkeep, remove all hourglass counters from permanents that player controls.').
card_first_print('temporal distortion', 'INV').
card_image_name('temporal distortion'/'INV', 'temporal distortion').
card_uid('temporal distortion'/'INV', 'INV:Temporal Distortion:temporal distortion').
card_rarity('temporal distortion'/'INV', 'Rare').
card_artist('temporal distortion'/'INV', 'Stephanie Law').
card_number('temporal distortion'/'INV', '79').
card_multiverse_id('temporal distortion'/'INV', '23212').

card_in_set('thicket elemental', 'INV').
card_original_type('thicket elemental'/'INV', 'Creature — Elemental').
card_original_text('thicket elemental'/'INV', 'Kicker {1}{G} (You may pay an additional {1}{G} as you play this spell.)\nWhen Thicket Elemental comes into play, if you paid the kicker cost, you may reveal cards from the top of your library until you reveal a creature card. If you do, put that card into play and shuffle all other cards revealed this way into your library.').
card_first_print('thicket elemental', 'INV').
card_image_name('thicket elemental'/'INV', 'thicket elemental').
card_uid('thicket elemental'/'INV', 'INV:Thicket Elemental:thicket elemental').
card_rarity('thicket elemental'/'INV', 'Rare').
card_artist('thicket elemental'/'INV', 'Ron Spencer').
card_number('thicket elemental'/'INV', '214').
card_multiverse_id('thicket elemental'/'INV', '23135').

card_in_set('thornscape apprentice', 'INV').
card_original_type('thornscape apprentice'/'INV', 'Creature — Wizard').
card_original_text('thornscape apprentice'/'INV', '{W}, {T}: Tap target creature.\n{R}, {T}: Target creature gains first strike until end of turn.').
card_first_print('thornscape apprentice', 'INV').
card_image_name('thornscape apprentice'/'INV', 'thornscape apprentice').
card_uid('thornscape apprentice'/'INV', 'INV:Thornscape Apprentice:thornscape apprentice').
card_rarity('thornscape apprentice'/'INV', 'Common').
card_artist('thornscape apprentice'/'INV', 'Randy Gallegos').
card_number('thornscape apprentice'/'INV', '215').
card_multiverse_id('thornscape apprentice'/'INV', '23104').

card_in_set('thornscape master', 'INV').
card_original_type('thornscape master'/'INV', 'Creature — Wizard').
card_original_text('thornscape master'/'INV', '{R}{R}, {T}: Thornscape Master deals 2 damage to target creature.\n{W}{W}, {T}: Target creature gains protection from the color of your choice until end of turn.').
card_first_print('thornscape master', 'INV').
card_image_name('thornscape master'/'INV', 'thornscape master').
card_uid('thornscape master'/'INV', 'INV:Thornscape Master:thornscape master').
card_rarity('thornscape master'/'INV', 'Rare').
card_artist('thornscape master'/'INV', 'Larry Elmore').
card_number('thornscape master'/'INV', '216').
card_multiverse_id('thornscape master'/'INV', '23136').

card_in_set('thunderscape apprentice', 'INV').
card_original_type('thunderscape apprentice'/'INV', 'Creature — Wizard').
card_original_text('thunderscape apprentice'/'INV', '{B}, {T}: Target player loses 1 life.\n{G}, {T}: Target creature gets +1/+1 until end of turn.').
card_first_print('thunderscape apprentice', 'INV').
card_image_name('thunderscape apprentice'/'INV', 'thunderscape apprentice').
card_uid('thunderscape apprentice'/'INV', 'INV:Thunderscape Apprentice:thunderscape apprentice').
card_rarity('thunderscape apprentice'/'INV', 'Common').
card_artist('thunderscape apprentice'/'INV', 'D. Alexander Gregory').
card_number('thunderscape apprentice'/'INV', '174').
card_multiverse_id('thunderscape apprentice'/'INV', '23060').

card_in_set('thunderscape master', 'INV').
card_original_type('thunderscape master'/'INV', 'Creature — Wizard').
card_original_text('thunderscape master'/'INV', '{B}{B}, {T}: Target player loses 2 life and you gain 2 life.\n{G}{G}, {T}: Creatures you control get +2/+2 until end of turn.').
card_first_print('thunderscape master', 'INV').
card_image_name('thunderscape master'/'INV', 'thunderscape master').
card_uid('thunderscape master'/'INV', 'INV:Thunderscape Master:thunderscape master').
card_rarity('thunderscape master'/'INV', 'Rare').
card_artist('thunderscape master'/'INV', 'Scott M. Fischer').
card_number('thunderscape master'/'INV', '175').
card_multiverse_id('thunderscape master'/'INV', '23093').

card_in_set('tidal visionary', 'INV').
card_original_type('tidal visionary'/'INV', 'Creature — Wizard').
card_original_text('tidal visionary'/'INV', '{T}: Target creature becomes the color of your choice until end of turn.').
card_first_print('tidal visionary', 'INV').
card_image_name('tidal visionary'/'INV', 'tidal visionary').
card_uid('tidal visionary'/'INV', 'INV:Tidal Visionary:tidal visionary').
card_rarity('tidal visionary'/'INV', 'Common').
card_artist('tidal visionary'/'INV', 'Glen Angus').
card_number('tidal visionary'/'INV', '80').
card_flavor_text('tidal visionary'/'INV', 'Beneath the waves, things appear the way merfolk want them to.').
card_multiverse_id('tidal visionary'/'INV', '22977').

card_in_set('tigereye cameo', 'INV').
card_original_type('tigereye cameo'/'INV', 'Artifact').
card_original_text('tigereye cameo'/'INV', '{T}: Add {G} or {W} to your mana pool.').
card_first_print('tigereye cameo', 'INV').
card_image_name('tigereye cameo'/'INV', 'tigereye cameo').
card_uid('tigereye cameo'/'INV', 'INV:Tigereye Cameo:tigereye cameo').
card_rarity('tigereye cameo'/'INV', 'Uncommon').
card_artist('tigereye cameo'/'INV', 'Donato Giancola').
card_number('tigereye cameo'/'INV', '314').
card_flavor_text('tigereye cameo'/'INV', '\"An elvish adventurer unearthed this stone in Jolrael\'s jungle. Now it\'s truly fit for display in one of her palaces.\"\n—Isel, master carver').
card_multiverse_id('tigereye cameo'/'INV', '23221').

card_in_set('tinder farm', 'INV').
card_original_type('tinder farm'/'INV', 'Land').
card_original_text('tinder farm'/'INV', 'Tinder Farm comes into play tapped.\n{T}: Add {G} to your mana pool.\n{T}, Sacrifice Tinder Farm: Add {R}{W} to your mana pool.').
card_first_print('tinder farm', 'INV').
card_image_name('tinder farm'/'INV', 'tinder farm').
card_uid('tinder farm'/'INV', 'INV:Tinder Farm:tinder farm').
card_rarity('tinder farm'/'INV', 'Common').
card_artist('tinder farm'/'INV', 'Rob Alexander').
card_number('tinder farm'/'INV', '329').
card_multiverse_id('tinder farm'/'INV', '23239').

card_in_set('tolarian emissary', 'INV').
card_original_type('tolarian emissary'/'INV', 'Creature — Wizard').
card_original_text('tolarian emissary'/'INV', 'Kicker {1}{W} (You may pay an additional {1}{W} as you play this spell.)\nFlying\nWhen Tolarian Emissary comes into play, if you paid the kicker cost, destroy target enchantment.').
card_first_print('tolarian emissary', 'INV').
card_image_name('tolarian emissary'/'INV', 'tolarian emissary').
card_uid('tolarian emissary'/'INV', 'INV:Tolarian Emissary:tolarian emissary').
card_rarity('tolarian emissary'/'INV', 'Uncommon').
card_artist('tolarian emissary'/'INV', 'Ron Spencer').
card_number('tolarian emissary'/'INV', '81').
card_multiverse_id('tolarian emissary'/'INV', '22991').

card_in_set('tower drake', 'INV').
card_original_type('tower drake'/'INV', 'Creature — Drake').
card_original_text('tower drake'/'INV', 'Flying\n{W} Tower Drake gets +0/+1 until end of turn.').
card_first_print('tower drake', 'INV').
card_image_name('tower drake'/'INV', 'tower drake').
card_uid('tower drake'/'INV', 'INV:Tower Drake:tower drake').
card_rarity('tower drake'/'INV', 'Common').
card_artist('tower drake'/'INV', 'Carl Critchlow').
card_number('tower drake'/'INV', '82').
card_flavor_text('tower drake'/'INV', 'Young tower drakes quickly learn to maneuver among Benalia\'s many spires.').
card_multiverse_id('tower drake'/'INV', '22974').

card_in_set('tranquility', 'INV').
card_original_type('tranquility'/'INV', 'Sorcery').
card_original_text('tranquility'/'INV', 'Destroy all enchantments.').
card_image_name('tranquility'/'INV', 'tranquility').
card_uid('tranquility'/'INV', 'INV:Tranquility:tranquility').
card_rarity('tranquility'/'INV', 'Common').
card_artist('tranquility'/'INV', 'Rob Alexander').
card_number('tranquility'/'INV', '217').
card_flavor_text('tranquility'/'INV', 'The plagues robbed Dominaria of all but its dreams. Eladamri hoped dreams were enough.').
card_multiverse_id('tranquility'/'INV', '23116').

card_in_set('traveler\'s cloak', 'INV').
card_original_type('traveler\'s cloak'/'INV', 'Enchant Creature').
card_original_text('traveler\'s cloak'/'INV', 'As Traveler\'s Cloak comes into play, choose a land type.\nEnchanted creature has landwalk of the chosen type. (It\'s unblockable as long as defending player controls a land of that type.)\nWhen Traveler\'s Cloak comes into play, draw a card.').
card_first_print('traveler\'s cloak', 'INV').
card_image_name('traveler\'s cloak'/'INV', 'traveler\'s cloak').
card_uid('traveler\'s cloak'/'INV', 'INV:Traveler\'s Cloak:traveler\'s cloak').
card_rarity('traveler\'s cloak'/'INV', 'Common').
card_artist('traveler\'s cloak'/'INV', 'Rebecca Guay').
card_number('traveler\'s cloak'/'INV', '83').
card_multiverse_id('traveler\'s cloak'/'INV', '22987').

card_in_set('treefolk healer', 'INV').
card_original_type('treefolk healer'/'INV', 'Creature — Treefolk').
card_original_text('treefolk healer'/'INV', '{2}{W}, {T}: Prevent the next 2 damage that would be dealt to target creature or player this turn.').
card_first_print('treefolk healer', 'INV').
card_image_name('treefolk healer'/'INV', 'treefolk healer').
card_uid('treefolk healer'/'INV', 'INV:Treefolk Healer:treefolk healer').
card_rarity('treefolk healer'/'INV', 'Uncommon').
card_artist('treefolk healer'/'INV', 'Matt Cavotta').
card_number('treefolk healer'/'INV', '218').
card_flavor_text('treefolk healer'/'INV', 'Protected by druids for untold centuries, the forest began to return the favor.').
card_multiverse_id('treefolk healer'/'INV', '23128').

card_in_set('trench wurm', 'INV').
card_original_type('trench wurm'/'INV', 'Creature — Wurm').
card_original_text('trench wurm'/'INV', '{2}{R}, {T}: Destroy target nonbasic land.').
card_first_print('trench wurm', 'INV').
card_image_name('trench wurm'/'INV', 'trench wurm').
card_uid('trench wurm'/'INV', 'INV:Trench Wurm:trench wurm').
card_rarity('trench wurm'/'INV', 'Uncommon').
card_artist('trench wurm'/'INV', 'Wayne England').
card_number('trench wurm'/'INV', '127').
card_flavor_text('trench wurm'/'INV', '\"Their arrival marks the end of Yawgmoth\'s subtlety.\"\n—Urza').
card_multiverse_id('trench wurm'/'INV', '23039').

card_in_set('treva\'s attendant', 'INV').
card_original_type('treva\'s attendant'/'INV', 'Artifact Creature — Golem').
card_original_text('treva\'s attendant'/'INV', '{1}, Sacrifice Treva\'s Attendant: Add {G}{W}{U} to your mana pool.').
card_first_print('treva\'s attendant', 'INV').
card_image_name('treva\'s attendant'/'INV', 'treva\'s attendant').
card_uid('treva\'s attendant'/'INV', 'INV:Treva\'s Attendant:treva\'s attendant').
card_rarity('treva\'s attendant'/'INV', 'Uncommon').
card_artist('treva\'s attendant'/'INV', 'Christopher Moeller').
card_number('treva\'s attendant'/'INV', '315').
card_flavor_text('treva\'s attendant'/'INV', '\"Treva is the voice of the ur-dragon, demanding cries of worship.\"').
card_multiverse_id('treva\'s attendant'/'INV', '25840').

card_in_set('treva, the renewer', 'INV').
card_original_type('treva, the renewer'/'INV', 'Creature — Dragon Legend').
card_original_text('treva, the renewer'/'INV', 'Flying\nWhenever Treva, the Renewer deals combat damage to a player, you may pay {2}{W}. If you do, choose a color. You gain 1 life for each permanent of that color.').
card_first_print('treva, the renewer', 'INV').
card_image_name('treva, the renewer'/'INV', 'treva, the renewer').
card_uid('treva, the renewer'/'INV', 'INV:Treva, the Renewer:treva, the renewer').
card_rarity('treva, the renewer'/'INV', 'Rare').
card_artist('treva, the renewer'/'INV', 'Ciruelo').
card_number('treva, the renewer'/'INV', '280').
card_multiverse_id('treva, the renewer'/'INV', '23210').

card_in_set('tribal flames', 'INV').
card_original_type('tribal flames'/'INV', 'Sorcery').
card_original_text('tribal flames'/'INV', 'Tribal Flames deals X damage to target creature or player, where X is the number of basic land types among lands you control.').
card_first_print('tribal flames', 'INV').
card_image_name('tribal flames'/'INV', 'tribal flames').
card_uid('tribal flames'/'INV', 'INV:Tribal Flames:tribal flames').
card_rarity('tribal flames'/'INV', 'Common').
card_artist('tribal flames'/'INV', 'Tony Szczudlo').
card_number('tribal flames'/'INV', '176').
card_flavor_text('tribal flames'/'INV', '\"Fire is the universal language.\"\n—Jhoira, master artificer').
card_multiverse_id('tribal flames'/'INV', '23076').

card_in_set('troll-horn cameo', 'INV').
card_original_type('troll-horn cameo'/'INV', 'Artifact').
card_original_text('troll-horn cameo'/'INV', '{T}: Add {R} or {G} to your mana pool.').
card_first_print('troll-horn cameo', 'INV').
card_image_name('troll-horn cameo'/'INV', 'troll-horn cameo').
card_uid('troll-horn cameo'/'INV', 'INV:Troll-Horn Cameo:troll-horn cameo').
card_rarity('troll-horn cameo'/'INV', 'Uncommon').
card_artist('troll-horn cameo'/'INV', 'Donato Giancola').
card_number('troll-horn cameo'/'INV', '316').
card_flavor_text('troll-horn cameo'/'INV', '\"I found a troll-horn fragment in the wooded foothills of Hurloon, and it keeps growing larger. I wonder, is the horn recreating itself or the troll?\"\n—Isel, master carver').
card_multiverse_id('troll-horn cameo'/'INV', '23220').

card_in_set('tsabo tavoc', 'INV').
card_original_type('tsabo tavoc'/'INV', 'Creature — Legend').
card_original_text('tsabo tavoc'/'INV', 'First strike, protection from Legends\n{B}{B}, {T}: Destroy target Legend. It can\'t be regenerated.').
card_first_print('tsabo tavoc', 'INV').
card_image_name('tsabo tavoc'/'INV', 'tsabo tavoc').
card_uid('tsabo tavoc'/'INV', 'INV:Tsabo Tavoc:tsabo tavoc').
card_rarity('tsabo tavoc'/'INV', 'Rare').
card_artist('tsabo tavoc'/'INV', 'Michael Sutfin').
card_number('tsabo tavoc'/'INV', '281').
card_flavor_text('tsabo tavoc'/'INV', '\"I might almost pity my enemies—if it wasn\'t so amusing to watch them die.\"').
card_multiverse_id('tsabo tavoc'/'INV', '23321').

card_in_set('tsabo\'s assassin', 'INV').
card_original_type('tsabo\'s assassin'/'INV', 'Creature — Assassin').
card_original_text('tsabo\'s assassin'/'INV', '{T}: Destroy target creature if it shares a color with the most common color among all permanents or the color tied for most common. A creature destroyed this way can\'t be regenerated.').
card_first_print('tsabo\'s assassin', 'INV').
card_image_name('tsabo\'s assassin'/'INV', 'tsabo\'s assassin').
card_uid('tsabo\'s assassin'/'INV', 'INV:Tsabo\'s Assassin:tsabo\'s assassin').
card_rarity('tsabo\'s assassin'/'INV', 'Rare').
card_artist('tsabo\'s assassin'/'INV', 'Glen Angus').
card_number('tsabo\'s assassin'/'INV', '128').
card_multiverse_id('tsabo\'s assassin'/'INV', '23048').

card_in_set('tsabo\'s decree', 'INV').
card_original_type('tsabo\'s decree'/'INV', 'Instant').
card_original_text('tsabo\'s decree'/'INV', 'Choose a creature type. Target player reveals his or her hand and discards all creature cards of that type from it. Then destroy all creatures of that type that player controls. They can\'t be regenerated.').
card_first_print('tsabo\'s decree', 'INV').
card_image_name('tsabo\'s decree'/'INV', 'tsabo\'s decree').
card_uid('tsabo\'s decree'/'INV', 'INV:Tsabo\'s Decree:tsabo\'s decree').
card_rarity('tsabo\'s decree'/'INV', 'Rare').
card_artist('tsabo\'s decree'/'INV', 'Thomas M. Baxa').
card_number('tsabo\'s decree'/'INV', '129').
card_multiverse_id('tsabo\'s decree'/'INV', '26380').

card_in_set('tsabo\'s web', 'INV').
card_original_type('tsabo\'s web'/'INV', 'Artifact').
card_original_text('tsabo\'s web'/'INV', 'When Tsabo\'s Web comes into play, draw a card.\nLands with an activated ability that doesn\'t produce mana don\'t untap during their controllers\' untap steps.').
card_first_print('tsabo\'s web', 'INV').
card_image_name('tsabo\'s web'/'INV', 'tsabo\'s web').
card_uid('tsabo\'s web'/'INV', 'INV:Tsabo\'s Web:tsabo\'s web').
card_rarity('tsabo\'s web'/'INV', 'Rare').
card_artist('tsabo\'s web'/'INV', 'Carl Critchlow').
card_number('tsabo\'s web'/'INV', '317').
card_multiverse_id('tsabo\'s web'/'INV', '23228').

card_in_set('turf wound', 'INV').
card_original_type('turf wound'/'INV', 'Instant').
card_original_text('turf wound'/'INV', 'Target player can\'t play land cards this turn.\nDraw a card.').
card_first_print('turf wound', 'INV').
card_image_name('turf wound'/'INV', 'turf wound').
card_uid('turf wound'/'INV', 'INV:Turf Wound:turf wound').
card_rarity('turf wound'/'INV', 'Common').
card_artist('turf wound'/'INV', 'Thomas Gianni').
card_number('turf wound'/'INV', '177').
card_flavor_text('turf wound'/'INV', '\"I can\'t imagine how anyone lives in Shiv, let alone why they choose to stay.\"\n—Sisay').
card_multiverse_id('turf wound'/'INV', '23071').

card_in_set('twilight\'s call', 'INV').
card_original_type('twilight\'s call'/'INV', 'Sorcery').
card_original_text('twilight\'s call'/'INV', 'You may play Twilight\'s Call any time you could play an instant if you pay {2} more to play it.\nEach player returns all creature cards from his or her graveyard to play.').
card_first_print('twilight\'s call', 'INV').
card_image_name('twilight\'s call'/'INV', 'twilight\'s call').
card_uid('twilight\'s call'/'INV', 'INV:Twilight\'s Call:twilight\'s call').
card_rarity('twilight\'s call'/'INV', 'Rare').
card_artist('twilight\'s call'/'INV', 'Mark Romanoski').
card_number('twilight\'s call'/'INV', '130').
card_flavor_text('twilight\'s call'/'INV', 'Twilight falls. We rise.\n—Necropolis inscription').
card_multiverse_id('twilight\'s call'/'INV', '23058').

card_in_set('undermine', 'INV').
card_original_type('undermine'/'INV', 'Instant').
card_original_text('undermine'/'INV', 'Counter target spell. Its controller loses 3 life.').
card_first_print('undermine', 'INV').
card_image_name('undermine'/'INV', 'undermine').
card_uid('undermine'/'INV', 'INV:Undermine:undermine').
card_rarity('undermine'/'INV', 'Rare').
card_artist('undermine'/'INV', 'Massimilano Frezzato').
card_number('undermine'/'INV', '282').
card_flavor_text('undermine'/'INV', '\"Which would you like first, the insult or the injury?\"').
card_multiverse_id('undermine'/'INV', '23190').

card_in_set('urborg drake', 'INV').
card_original_type('urborg drake'/'INV', 'Creature — Drake').
card_original_text('urborg drake'/'INV', 'Flying\nUrborg Drake attacks each turn if able.').
card_first_print('urborg drake', 'INV').
card_image_name('urborg drake'/'INV', 'urborg drake').
card_uid('urborg drake'/'INV', 'INV:Urborg Drake:urborg drake').
card_rarity('urborg drake'/'INV', 'Uncommon').
card_artist('urborg drake'/'INV', 'Sam Wood').
card_number('urborg drake'/'INV', '283').
card_flavor_text('urborg drake'/'INV', 'Relentless as the sea, remorseless as death.').
card_multiverse_id('urborg drake'/'INV', '25975').

card_in_set('urborg emissary', 'INV').
card_original_type('urborg emissary'/'INV', 'Creature — Wizard').
card_original_text('urborg emissary'/'INV', 'Kicker {1}{U} (You may pay an additional {1}{U} as you play this spell.)\nWhen Urborg Emissary comes into play, if you paid the kicker cost, return target permanent to its owner\'s hand.').
card_first_print('urborg emissary', 'INV').
card_image_name('urborg emissary'/'INV', 'urborg emissary').
card_uid('urborg emissary'/'INV', 'INV:Urborg Emissary:urborg emissary').
card_rarity('urborg emissary'/'INV', 'Uncommon').
card_artist('urborg emissary'/'INV', 'Eric Peterson').
card_number('urborg emissary'/'INV', '131').
card_multiverse_id('urborg emissary'/'INV', '23038').

card_in_set('urborg phantom', 'INV').
card_original_type('urborg phantom'/'INV', 'Creature — Minion').
card_original_text('urborg phantom'/'INV', 'Urborg Phantom can\'t block.\n{U} Prevent all combat damage that would be dealt to and dealt by Urborg Phantom this turn.').
card_first_print('urborg phantom', 'INV').
card_image_name('urborg phantom'/'INV', 'urborg phantom').
card_uid('urborg phantom'/'INV', 'INV:Urborg Phantom:urborg phantom').
card_rarity('urborg phantom'/'INV', 'Common').
card_artist('urborg phantom'/'INV', 'Daren Bader').
card_number('urborg phantom'/'INV', '132').
card_flavor_text('urborg phantom'/'INV', 'A chilling fog with teeth of ice.').
card_multiverse_id('urborg phantom'/'INV', '23018').

card_in_set('urborg shambler', 'INV').
card_original_type('urborg shambler'/'INV', 'Creature — Horror').
card_original_text('urborg shambler'/'INV', 'All other black creatures get -1/-1.').
card_first_print('urborg shambler', 'INV').
card_image_name('urborg shambler'/'INV', 'urborg shambler').
card_uid('urborg shambler'/'INV', 'INV:Urborg Shambler:urborg shambler').
card_rarity('urborg shambler'/'INV', 'Uncommon').
card_artist('urborg shambler'/'INV', 'Pete Venters').
card_number('urborg shambler'/'INV', '133').
card_flavor_text('urborg shambler'/'INV', 'A writhing mass of rot, with sharp claws and a vicious spirit.').
card_multiverse_id('urborg shambler'/'INV', '23035').

card_in_set('urborg skeleton', 'INV').
card_original_type('urborg skeleton'/'INV', 'Creature — Skeleton').
card_original_text('urborg skeleton'/'INV', 'Kicker {3} (You may pay an additional {3} as you play this spell.)\n{B} Regenerate Urborg Skeleton.\nIf you paid the kicker cost, Urborg Skeleton comes into play with a +1/+1 counter on it.').
card_first_print('urborg skeleton', 'INV').
card_image_name('urborg skeleton'/'INV', 'urborg skeleton').
card_uid('urborg skeleton'/'INV', 'INV:Urborg Skeleton:urborg skeleton').
card_rarity('urborg skeleton'/'INV', 'Common').
card_artist('urborg skeleton'/'INV', 'Alan Pollack').
card_number('urborg skeleton'/'INV', '134').
card_multiverse_id('urborg skeleton'/'INV', '23020').

card_in_set('urborg volcano', 'INV').
card_original_type('urborg volcano'/'INV', 'Land').
card_original_text('urborg volcano'/'INV', 'Urborg Volcano comes into play tapped.\n{T}: Add {B} or {R} to your mana pool.').
card_first_print('urborg volcano', 'INV').
card_image_name('urborg volcano'/'INV', 'urborg volcano').
card_uid('urborg volcano'/'INV', 'INV:Urborg Volcano:urborg volcano').
card_rarity('urborg volcano'/'INV', 'Uncommon').
card_artist('urborg volcano'/'INV', 'Tony Szczudlo').
card_number('urborg volcano'/'INV', '330').
card_flavor_text('urborg volcano'/'INV', 'Deep in the heart of Urborg lie massive volcanoes whose thick black smoke covers the land with perpetual darkness.').
card_multiverse_id('urborg volcano'/'INV', '23242').

card_in_set('urza\'s filter', 'INV').
card_original_type('urza\'s filter'/'INV', 'Artifact').
card_original_text('urza\'s filter'/'INV', 'Multicolored spells cost up to {2} less to play.').
card_first_print('urza\'s filter', 'INV').
card_image_name('urza\'s filter'/'INV', 'urza\'s filter').
card_uid('urza\'s filter'/'INV', 'INV:Urza\'s Filter:urza\'s filter').
card_rarity('urza\'s filter'/'INV', 'Rare').
card_artist('urza\'s filter'/'INV', 'Dave Dorman').
card_number('urza\'s filter'/'INV', '318').
card_flavor_text('urza\'s filter'/'INV', '\"It\'s a tool for aiding my other tools,\" explained Urza. \"Now they\'ll work in harmony.\"').
card_multiverse_id('urza\'s filter'/'INV', '23227').

card_in_set('urza\'s rage', 'INV').
card_original_type('urza\'s rage'/'INV', 'Instant').
card_original_text('urza\'s rage'/'INV', 'Kicker {8}{R} (You may pay an additional {8}{R} as you play this spell.)\nUrza\'s Rage can\'t be countered by spells or abilities.\nUrza\'s Rage deals 3 damage to target creature or player. If you paid the kicker cost, instead Urza\'s Rage deals 10 damage to that creature or player and the damage can\'t be prevented.').
card_first_print('urza\'s rage', 'INV').
card_image_name('urza\'s rage'/'INV', 'urza\'s rage').
card_uid('urza\'s rage'/'INV', 'INV:Urza\'s Rage:urza\'s rage').
card_rarity('urza\'s rage'/'INV', 'Rare').
card_artist('urza\'s rage'/'INV', 'Matthew D. Wilson').
card_number('urza\'s rage'/'INV', '178').
card_multiverse_id('urza\'s rage'/'INV', '23087').

card_in_set('utopia tree', 'INV').
card_original_type('utopia tree'/'INV', 'Creature — Plant').
card_original_text('utopia tree'/'INV', '{T}: Add one mana of any color to your mana pool.').
card_first_print('utopia tree', 'INV').
card_image_name('utopia tree'/'INV', 'utopia tree').
card_uid('utopia tree'/'INV', 'INV:Utopia Tree:utopia tree').
card_rarity('utopia tree'/'INV', 'Rare').
card_artist('utopia tree'/'INV', 'Gary Ruddell').
card_number('utopia tree'/'INV', '219').
card_flavor_text('utopia tree'/'INV', 'The fruit of this fabled tree takes on the flavor of whatever food you love most.').
card_multiverse_id('utopia tree'/'INV', '12605').

card_in_set('verdeloth the ancient', 'INV').
card_original_type('verdeloth the ancient'/'INV', 'Creature — Treefolk Legend').
card_original_text('verdeloth the ancient'/'INV', 'Kicker {X} (You may pay an additional {X} as you play this spell.)\nAll other Treefolk and all Saprolings get +1/+1.\nWhen Verdeloth the Ancient comes into play, if you paid the kicker cost, put X 1/1 green Saproling creature tokens into play.').
card_first_print('verdeloth the ancient', 'INV').
card_image_name('verdeloth the ancient'/'INV', 'verdeloth the ancient').
card_uid('verdeloth the ancient'/'INV', 'INV:Verdeloth the Ancient:verdeloth the ancient').
card_rarity('verdeloth the ancient'/'INV', 'Rare').
card_artist('verdeloth the ancient'/'INV', 'Daren Bader').
card_number('verdeloth the ancient'/'INV', '220').
card_multiverse_id('verdeloth the ancient'/'INV', '23137').

card_in_set('verduran emissary', 'INV').
card_original_type('verduran emissary'/'INV', 'Creature — Wizard').
card_original_text('verduran emissary'/'INV', 'Kicker {1}{R} (You may pay an additional {1}{R} as you play this spell.)\nWhen Verduran Emissary comes into play, if you paid the kicker cost, destroy target artifact. It can\'t be regenerated.').
card_first_print('verduran emissary', 'INV').
card_image_name('verduran emissary'/'INV', 'verduran emissary').
card_uid('verduran emissary'/'INV', 'INV:Verduran Emissary:verduran emissary').
card_rarity('verduran emissary'/'INV', 'Uncommon').
card_artist('verduran emissary'/'INV', 'Alton Lawson').
card_number('verduran emissary'/'INV', '221').
card_multiverse_id('verduran emissary'/'INV', '23127').

card_in_set('viashino grappler', 'INV').
card_original_type('viashino grappler'/'INV', 'Creature — Viashino').
card_original_text('viashino grappler'/'INV', '{G} Viashino Grappler gains trample until end of turn.').
card_first_print('viashino grappler', 'INV').
card_image_name('viashino grappler'/'INV', 'viashino grappler').
card_uid('viashino grappler'/'INV', 'INV:Viashino Grappler:viashino grappler').
card_rarity('viashino grappler'/'INV', 'Common').
card_artist('viashino grappler'/'INV', 'Mark Romanoski').
card_number('viashino grappler'/'INV', '179').
card_flavor_text('viashino grappler'/'INV', '\"They\'ve returned for the mana rig, but it no longer belongs to them.\"\n—Viashino bey').
card_multiverse_id('viashino grappler'/'INV', '22930').

card_in_set('vicious kavu', 'INV').
card_original_type('vicious kavu'/'INV', 'Creature — Kavu').
card_original_text('vicious kavu'/'INV', 'Whenever Vicious Kavu attacks, it gets +2/+0 until end of turn.').
card_first_print('vicious kavu', 'INV').
card_image_name('vicious kavu'/'INV', 'vicious kavu').
card_uid('vicious kavu'/'INV', 'INV:Vicious Kavu:vicious kavu').
card_rarity('vicious kavu'/'INV', 'Uncommon').
card_artist('vicious kavu'/'INV', 'Kev Walker').
card_number('vicious kavu'/'INV', '284').
card_flavor_text('vicious kavu'/'INV', 'As battle raged in Shiv, a strange new ally appeared from below the ravaged ground.').
card_multiverse_id('vicious kavu'/'INV', '23151').

card_in_set('vigorous charge', 'INV').
card_original_type('vigorous charge'/'INV', 'Instant').
card_original_text('vigorous charge'/'INV', 'Kicker {W} (You may pay an additional {W} as you play this spell.)\nTarget creature gains trample until end of turn. Whenever that creature deals combat damage this turn, if you paid the kicker cost, you gain life equal to that damage.').
card_first_print('vigorous charge', 'INV').
card_image_name('vigorous charge'/'INV', 'vigorous charge').
card_uid('vigorous charge'/'INV', 'INV:Vigorous Charge:vigorous charge').
card_rarity('vigorous charge'/'INV', 'Common').
card_artist('vigorous charge'/'INV', 'Scott M. Fischer').
card_number('vigorous charge'/'INV', '222').
card_multiverse_id('vigorous charge'/'INV', '23121').

card_in_set('vile consumption', 'INV').
card_original_type('vile consumption'/'INV', 'Enchantment').
card_original_text('vile consumption'/'INV', 'All creatures have \"At the beginning of your upkeep, sacrifice this creature unless you pay 1 life.\"').
card_first_print('vile consumption', 'INV').
card_image_name('vile consumption'/'INV', 'vile consumption').
card_uid('vile consumption'/'INV', 'INV:Vile Consumption:vile consumption').
card_rarity('vile consumption'/'INV', 'Rare').
card_artist('vile consumption'/'INV', 'Heather Hudson').
card_number('vile consumption'/'INV', '285').
card_flavor_text('vile consumption'/'INV', 'The plague moved faster than an army and was far more deadly.').
card_multiverse_id('vile consumption'/'INV', '23188').

card_in_set('vodalian hypnotist', 'INV').
card_original_type('vodalian hypnotist'/'INV', 'Creature — Wizard').
card_original_text('vodalian hypnotist'/'INV', '{2}{B}, {T}: Target player discards a card from his or her hand. Play this ability only any time you could play a sorcery.').
card_first_print('vodalian hypnotist', 'INV').
card_image_name('vodalian hypnotist'/'INV', 'vodalian hypnotist').
card_uid('vodalian hypnotist'/'INV', 'INV:Vodalian Hypnotist:vodalian hypnotist').
card_rarity('vodalian hypnotist'/'INV', 'Uncommon').
card_artist('vodalian hypnotist'/'INV', 'Rebecca Guay').
card_number('vodalian hypnotist'/'INV', '84').
card_flavor_text('vodalian hypnotist'/'INV', '\"Deceit is the heart of war.\"').
card_multiverse_id('vodalian hypnotist'/'INV', '22993').

card_in_set('vodalian merchant', 'INV').
card_original_type('vodalian merchant'/'INV', 'Creature — Merfolk').
card_original_text('vodalian merchant'/'INV', 'When Vodalian Merchant comes into play, draw a card, then discard a card from your hand.').
card_first_print('vodalian merchant', 'INV').
card_image_name('vodalian merchant'/'INV', 'vodalian merchant').
card_uid('vodalian merchant'/'INV', 'INV:Vodalian Merchant:vodalian merchant').
card_rarity('vodalian merchant'/'INV', 'Common').
card_artist('vodalian merchant'/'INV', 'Scott M. Fischer').
card_number('vodalian merchant'/'INV', '85').
card_flavor_text('vodalian merchant'/'INV', 'Not choosing sides in a war can be dangerous—but lucrative.').
card_multiverse_id('vodalian merchant'/'INV', '26296').

card_in_set('vodalian serpent', 'INV').
card_original_type('vodalian serpent'/'INV', 'Creature — Serpent').
card_original_text('vodalian serpent'/'INV', 'Kicker {2} (You may pay an additional {2} as you play this spell.)\nVodalian Serpent can\'t attack unless defending player controls an island.\nIf you paid the kicker cost, Vodalian Serpent comes into play with four +1/+1 counters on it.').
card_first_print('vodalian serpent', 'INV').
card_image_name('vodalian serpent'/'INV', 'vodalian serpent').
card_uid('vodalian serpent'/'INV', 'INV:Vodalian Serpent:vodalian serpent').
card_rarity('vodalian serpent'/'INV', 'Common').
card_artist('vodalian serpent'/'INV', 'Christopher Moeller').
card_number('vodalian serpent'/'INV', '86').
card_multiverse_id('vodalian serpent'/'INV', '22976').

card_in_set('vodalian zombie', 'INV').
card_original_type('vodalian zombie'/'INV', 'Creature — Merfolk Zombie').
card_original_text('vodalian zombie'/'INV', 'Protection from green').
card_first_print('vodalian zombie', 'INV').
card_image_name('vodalian zombie'/'INV', 'vodalian zombie').
card_uid('vodalian zombie'/'INV', 'INV:Vodalian Zombie:vodalian zombie').
card_rarity('vodalian zombie'/'INV', 'Common').
card_artist('vodalian zombie'/'INV', 'Greg & Tim Hildebrandt').
card_number('vodalian zombie'/'INV', '286').
card_flavor_text('vodalian zombie'/'INV', '\"Every last one of you will become my servant. It\'s a shame you won\'t live to see the irony.\"\n—Tsabo Tavoc, Phyrexian general').
card_multiverse_id('vodalian zombie'/'INV', '23159').

card_in_set('void', 'INV').
card_original_type('void'/'INV', 'Sorcery').
card_original_text('void'/'INV', 'Choose a number. Destroy all artifacts and creatures with converted mana cost equal to that number. Then target player reveals his or her hand and discards from it all nonland cards with converted mana cost equal to the number.').
card_first_print('void', 'INV').
card_image_name('void'/'INV', 'void').
card_uid('void'/'INV', 'INV:Void:void').
card_rarity('void'/'INV', 'Rare').
card_artist('void'/'INV', 'Kev Walker').
card_number('void'/'INV', '287').
card_multiverse_id('void'/'INV', '23200').

card_in_set('voracious cobra', 'INV').
card_original_type('voracious cobra'/'INV', 'Creature — Snake').
card_original_text('voracious cobra'/'INV', 'First strike\nWhenever Voracious Cobra deals combat damage to a creature, destroy that creature.').
card_first_print('voracious cobra', 'INV').
card_image_name('voracious cobra'/'INV', 'voracious cobra').
card_uid('voracious cobra'/'INV', 'INV:Voracious Cobra:voracious cobra').
card_rarity('voracious cobra'/'INV', 'Uncommon').
card_artist('voracious cobra'/'INV', 'Terese Nielsen').
card_number('voracious cobra'/'INV', '288').
card_flavor_text('voracious cobra'/'INV', 'There\'s no known antidote for the cobra\'s venom . . . or its appetite.').
card_multiverse_id('voracious cobra'/'INV', '23203').

card_in_set('wallop', 'INV').
card_original_type('wallop'/'INV', 'Sorcery').
card_original_text('wallop'/'INV', 'Destroy target blue or black creature with flying.').
card_first_print('wallop', 'INV').
card_image_name('wallop'/'INV', 'wallop').
card_uid('wallop'/'INV', 'INV:Wallop:wallop').
card_rarity('wallop'/'INV', 'Uncommon').
card_artist('wallop'/'INV', 'Mike Ploog').
card_number('wallop'/'INV', '223').
card_flavor_text('wallop'/'INV', 'In Yavimaya, flying low to join a battle can be a costly mistake.').
card_multiverse_id('wallop'/'INV', '23130').

card_in_set('wandering stream', 'INV').
card_original_type('wandering stream'/'INV', 'Sorcery').
card_original_text('wandering stream'/'INV', 'You gain 2 life for each basic land type among lands you control.').
card_first_print('wandering stream', 'INV').
card_image_name('wandering stream'/'INV', 'wandering stream').
card_uid('wandering stream'/'INV', 'INV:Wandering Stream:wandering stream').
card_rarity('wandering stream'/'INV', 'Common').
card_artist('wandering stream'/'INV', 'Quinton Hoover').
card_number('wandering stream'/'INV', '224').
card_flavor_text('wandering stream'/'INV', '\"Dominaria touches us all.\"\n—Molimo, maro-sorcerer').
card_multiverse_id('wandering stream'/'INV', '23119').

card_in_set('wane', 'INV').
card_original_type('wane'/'INV', 'Instant').
card_original_text('wane'/'INV', 'Destroy target enchantment.').
card_first_print('wane', 'INV').
card_image_name('wane'/'INV', 'waxwane').
card_uid('wane'/'INV', 'INV:Wane:waxwane').
card_rarity('wane'/'INV', 'Uncommon').
card_artist('wane'/'INV', 'Ben Thompson').
card_number('wane'/'INV', '296b').
card_multiverse_id('wane'/'INV', '20582').

card_in_set('wash out', 'INV').
card_original_type('wash out'/'INV', 'Sorcery').
card_original_text('wash out'/'INV', 'Return all permanents of the color of your choice to their owners\' hands.').
card_image_name('wash out'/'INV', 'wash out').
card_uid('wash out'/'INV', 'INV:Wash Out:wash out').
card_rarity('wash out'/'INV', 'Uncommon').
card_artist('wash out'/'INV', 'Matthew D. Wilson').
card_number('wash out'/'INV', '87').
card_flavor_text('wash out'/'INV', '\"Rest now. You\'ve neither won nor lost, but this battle is over for you just the same.\"\n—Teferi').
card_multiverse_id('wash out'/'INV', '25320').

card_in_set('wax', 'INV').
card_original_type('wax'/'INV', 'Instant').
card_original_text('wax'/'INV', 'Target creature gets +2/+2 until end of turn.').
card_first_print('wax', 'INV').
card_image_name('wax'/'INV', 'waxwane').
card_uid('wax'/'INV', 'INV:Wax:waxwane').
card_rarity('wax'/'INV', 'Uncommon').
card_artist('wax'/'INV', 'Ben Thompson').
card_number('wax'/'INV', '296a').
card_multiverse_id('wax'/'INV', '20582').

card_in_set('wayfaring giant', 'INV').
card_original_type('wayfaring giant'/'INV', 'Creature — Giant').
card_original_text('wayfaring giant'/'INV', 'Wayfaring Giant gets +1/+1 for each basic land type among lands you control.').
card_first_print('wayfaring giant', 'INV').
card_image_name('wayfaring giant'/'INV', 'wayfaring giant').
card_uid('wayfaring giant'/'INV', 'INV:Wayfaring Giant:wayfaring giant').
card_rarity('wayfaring giant'/'INV', 'Uncommon').
card_artist('wayfaring giant'/'INV', 'Christopher Moeller').
card_number('wayfaring giant'/'INV', '44').
card_flavor_text('wayfaring giant'/'INV', 'Its stature and stride increase with each step it takes.').
card_multiverse_id('wayfaring giant'/'INV', '23211').

card_in_set('well-laid plans', 'INV').
card_original_type('well-laid plans'/'INV', 'Enchantment').
card_original_text('well-laid plans'/'INV', 'Prevent all damage that would be dealt to a creature by another creature if they share a color.').
card_first_print('well-laid plans', 'INV').
card_image_name('well-laid plans'/'INV', 'well-laid plans').
card_uid('well-laid plans'/'INV', 'INV:Well-Laid Plans:well-laid plans').
card_rarity('well-laid plans'/'INV', 'Rare').
card_artist('well-laid plans'/'INV', 'Kev Walker').
card_number('well-laid plans'/'INV', '88').
card_flavor_text('well-laid plans'/'INV', '\"I knew this day would come,\" said Urza. Looking at the destruction, Barrin sighed, \"You don\'t have to revel in it.\"').
card_multiverse_id('well-laid plans'/'INV', '24117').

card_in_set('whip silk', 'INV').
card_original_type('whip silk'/'INV', 'Enchant Creature').
card_original_text('whip silk'/'INV', 'Enchanted creature may block as though it had flying.\n{G} Return Whip Silk to its owner\'s hand.').
card_first_print('whip silk', 'INV').
card_image_name('whip silk'/'INV', 'whip silk').
card_uid('whip silk'/'INV', 'INV:Whip Silk:whip silk').
card_rarity('whip silk'/'INV', 'Common').
card_artist('whip silk'/'INV', 'Dave Dorman').
card_number('whip silk'/'INV', '225').
card_flavor_text('whip silk'/'INV', 'Llanowar forges weapons of steel. Yavimaya grows her weapons within.').
card_multiverse_id('whip silk'/'INV', '23118').

card_in_set('wings of hope', 'INV').
card_original_type('wings of hope'/'INV', 'Enchant Creature').
card_original_text('wings of hope'/'INV', 'Enchanted creature gets +1/+3 and has flying.').
card_first_print('wings of hope', 'INV').
card_image_name('wings of hope'/'INV', 'wings of hope').
card_uid('wings of hope'/'INV', 'INV:Wings of Hope:wings of hope').
card_rarity('wings of hope'/'INV', 'Common').
card_artist('wings of hope'/'INV', 'Wayne England').
card_number('wings of hope'/'INV', '289').
card_flavor_text('wings of hope'/'INV', 'Urza knew Phyrexians would come through the air and sent his soldiers to greet them.').
card_multiverse_id('wings of hope'/'INV', '24118').

card_in_set('winnow', 'INV').
card_original_type('winnow'/'INV', 'Instant').
card_original_text('winnow'/'INV', 'Destroy target nonland permanent if another permanent with the same name is in play.\nDraw a card.').
card_first_print('winnow', 'INV').
card_image_name('winnow'/'INV', 'winnow').
card_uid('winnow'/'INV', 'INV:Winnow:winnow').
card_rarity('winnow'/'INV', 'Rare').
card_artist('winnow'/'INV', 'Roger Raupp').
card_number('winnow'/'INV', '45').
card_flavor_text('winnow'/'INV', '\"Strength in numbers? I think not.\"\n—Gerrard').
card_multiverse_id('winnow'/'INV', '22966').

card_in_set('worldly counsel', 'INV').
card_original_type('worldly counsel'/'INV', 'Instant').
card_original_text('worldly counsel'/'INV', 'Look at the top X cards of your library, where X is the number of basic land types among lands you control. Put one of those cards into your hand and the rest on the bottom of your library.').
card_first_print('worldly counsel', 'INV').
card_image_name('worldly counsel'/'INV', 'worldly counsel').
card_uid('worldly counsel'/'INV', 'INV:Worldly Counsel:worldly counsel').
card_rarity('worldly counsel'/'INV', 'Common').
card_artist('worldly counsel'/'INV', 'Gary Ruddell').
card_number('worldly counsel'/'INV', '89').
card_multiverse_id('worldly counsel'/'INV', '22982').

card_in_set('yavimaya barbarian', 'INV').
card_original_type('yavimaya barbarian'/'INV', 'Creature — Barbarian Elf').
card_original_text('yavimaya barbarian'/'INV', 'Protection from blue').
card_first_print('yavimaya barbarian', 'INV').
card_image_name('yavimaya barbarian'/'INV', 'yavimaya barbarian').
card_uid('yavimaya barbarian'/'INV', 'INV:Yavimaya Barbarian:yavimaya barbarian').
card_rarity('yavimaya barbarian'/'INV', 'Common').
card_artist('yavimaya barbarian'/'INV', 'Don Hazeltine').
card_number('yavimaya barbarian'/'INV', '290').
card_flavor_text('yavimaya barbarian'/'INV', 'Not all elves embrace the pastoral life. Some still roam the forest\'s edge, forever making war against their hated enemies.').
card_multiverse_id('yavimaya barbarian'/'INV', '23161').

card_in_set('yavimaya kavu', 'INV').
card_original_type('yavimaya kavu'/'INV', 'Creature — Kavu').
card_original_text('yavimaya kavu'/'INV', 'Yavimaya Kavu\'s power is equal to the number of red creatures in play.\nYavimaya Kavu\'s toughness is equal to the number of green creatures in play.').
card_first_print('yavimaya kavu', 'INV').
card_image_name('yavimaya kavu'/'INV', 'yavimaya kavu').
card_uid('yavimaya kavu'/'INV', 'INV:Yavimaya Kavu:yavimaya kavu').
card_rarity('yavimaya kavu'/'INV', 'Uncommon').
card_artist('yavimaya kavu'/'INV', 'Greg Staples').
card_number('yavimaya kavu'/'INV', '291').
card_flavor_text('yavimaya kavu'/'INV', 'Kavu hunt prey in packs—one kavu hunts a pack of prey.').
card_multiverse_id('yavimaya kavu'/'INV', '23174').

card_in_set('yawgmoth\'s agenda', 'INV').
card_original_type('yawgmoth\'s agenda'/'INV', 'Enchantment').
card_original_text('yawgmoth\'s agenda'/'INV', 'Play no more than one spell each turn.\nYou may play cards in your graveyard as though they were in your hand.\nIf a card would be put into your graveyard from anywhere, remove it from the game instead.').
card_first_print('yawgmoth\'s agenda', 'INV').
card_image_name('yawgmoth\'s agenda'/'INV', 'yawgmoth\'s agenda').
card_uid('yawgmoth\'s agenda'/'INV', 'INV:Yawgmoth\'s Agenda:yawgmoth\'s agenda').
card_rarity('yawgmoth\'s agenda'/'INV', 'Rare').
card_artist('yawgmoth\'s agenda'/'INV', 'Arnie Swekel').
card_number('yawgmoth\'s agenda'/'INV', '135').
card_multiverse_id('yawgmoth\'s agenda'/'INV', '23054').

card_in_set('zanam djinn', 'INV').
card_original_type('zanam djinn'/'INV', 'Creature — Djinn').
card_original_text('zanam djinn'/'INV', 'Flying\nZanam Djinn gets -2/-2 as long as blue is the most common color among all permanents or is tied for most common.').
card_first_print('zanam djinn', 'INV').
card_image_name('zanam djinn'/'INV', 'zanam djinn').
card_uid('zanam djinn'/'INV', 'INV:Zanam Djinn:zanam djinn').
card_rarity('zanam djinn'/'INV', 'Uncommon').
card_artist('zanam djinn'/'INV', 'Eric Peterson').
card_number('zanam djinn'/'INV', '90').
card_multiverse_id('zanam djinn'/'INV', '22992').

card_in_set('zap', 'INV').
card_original_type('zap'/'INV', 'Instant').
card_original_text('zap'/'INV', 'Zap deals 1 damage to target creature or player.\nDraw a card.').
card_first_print('zap', 'INV').
card_image_name('zap'/'INV', 'zap').
card_uid('zap'/'INV', 'INV:Zap:zap').
card_rarity('zap'/'INV', 'Common').
card_artist('zap'/'INV', 'John Matson').
card_number('zap'/'INV', '180').
card_flavor_text('zap'/'INV', '\"All this time I thought Squee was useless,\" chuckled Sisay. \"Who knew he\'d be such a good shot?\"').
card_multiverse_id('zap'/'INV', '23075').
