% European Land Program

set('pELP').
set_name('pELP', 'European Land Program').
set_release_date('pELP', '2000-02-05').
set_border('pELP', 'black').
set_type('pELP', 'promo').

card_in_set('forest', 'pELP').
card_original_type('forest'/'pELP', 'Basic Land — Forest').
card_original_text('forest'/'pELP', '').
card_image_name('forest'/'pELP', 'forest1').
card_uid('forest'/'pELP', 'pELP:Forest:forest1').
card_rarity('forest'/'pELP', 'Basic Land').
card_artist('forest'/'pELP', 'Scott Bailey').
card_number('forest'/'pELP', '1').

card_in_set('forest', 'pELP').
card_original_type('forest'/'pELP', 'Basic Land — Forest').
card_original_text('forest'/'pELP', '').
card_image_name('forest'/'pELP', 'forest2').
card_uid('forest'/'pELP', 'pELP:Forest:forest2').
card_rarity('forest'/'pELP', 'Basic Land').
card_artist('forest'/'pELP', 'Mike Ploog').
card_number('forest'/'pELP', '11').

card_in_set('forest', 'pELP').
card_original_type('forest'/'pELP', 'Basic Land — Forest').
card_original_text('forest'/'pELP', '').
card_image_name('forest'/'pELP', 'forest3').
card_uid('forest'/'pELP', 'pELP:Forest:forest3').
card_rarity('forest'/'pELP', 'Basic Land').
card_artist('forest'/'pELP', 'Kev Walker').
card_number('forest'/'pELP', '6').

card_in_set('island', 'pELP').
card_original_type('island'/'pELP', 'Basic Land — Island').
card_original_text('island'/'pELP', '').
card_image_name('island'/'pELP', 'island1').
card_uid('island'/'pELP', 'pELP:Island:island1').
card_rarity('island'/'pELP', 'Basic Land').
card_artist('island'/'pELP', 'Ben Thompson').
card_number('island'/'pELP', '12').

card_in_set('island', 'pELP').
card_original_type('island'/'pELP', 'Basic Land — Island').
card_original_text('island'/'pELP', '').
card_image_name('island'/'pELP', 'island2').
card_uid('island'/'pELP', 'pELP:Island:island2').
card_rarity('island'/'pELP', 'Basic Land').
card_artist('island'/'pELP', 'Ben Thompson').
card_number('island'/'pELP', '2').

card_in_set('island', 'pELP').
card_original_type('island'/'pELP', 'Basic Land — Island').
card_original_text('island'/'pELP', '').
card_image_name('island'/'pELP', 'island3').
card_uid('island'/'pELP', 'pELP:Island:island3').
card_rarity('island'/'pELP', 'Basic Land').
card_artist('island'/'pELP', 'Eric Peterson').
card_number('island'/'pELP', '7').

card_in_set('mountain', 'pELP').
card_original_type('mountain'/'pELP', 'Basic Land — Mountain').
card_original_text('mountain'/'pELP', '').
card_image_name('mountain'/'pELP', 'mountain1').
card_uid('mountain'/'pELP', 'pELP:Mountain:mountain1').
card_rarity('mountain'/'pELP', 'Basic Land').
card_artist('mountain'/'pELP', 'Eric Peterson').
card_number('mountain'/'pELP', '13').

card_in_set('mountain', 'pELP').
card_original_type('mountain'/'pELP', 'Basic Land — Mountain').
card_original_text('mountain'/'pELP', '').
card_image_name('mountain'/'pELP', 'mountain2').
card_uid('mountain'/'pELP', 'pELP:Mountain:mountain2').
card_rarity('mountain'/'pELP', 'Basic Land').
card_artist('mountain'/'pELP', 'Kev Walker').
card_number('mountain'/'pELP', '3').

card_in_set('mountain', 'pELP').
card_original_type('mountain'/'pELP', 'Basic Land — Mountain').
card_original_text('mountain'/'pELP', '').
card_image_name('mountain'/'pELP', 'mountain3').
card_uid('mountain'/'pELP', 'pELP:Mountain:mountain3').
card_rarity('mountain'/'pELP', 'Basic Land').
card_artist('mountain'/'pELP', 'Scott Bailey').
card_number('mountain'/'pELP', '8').

card_in_set('plains', 'pELP').
card_original_type('plains'/'pELP', 'Basic Land — Plains').
card_original_text('plains'/'pELP', '').
card_image_name('plains'/'pELP', 'plains1').
card_uid('plains'/'pELP', 'pELP:Plains:plains1').
card_rarity('plains'/'pELP', 'Basic Land').
card_artist('plains'/'pELP', 'Ben Thompson').
card_number('plains'/'pELP', '14').

card_in_set('plains', 'pELP').
card_original_type('plains'/'pELP', 'Basic Land — Plains').
card_original_text('plains'/'pELP', '').
card_image_name('plains'/'pELP', 'plains2').
card_uid('plains'/'pELP', 'pELP:Plains:plains2').
card_rarity('plains'/'pELP', 'Basic Land').
card_artist('plains'/'pELP', 'Mike Ploog').
card_number('plains'/'pELP', '4').

card_in_set('plains', 'pELP').
card_original_type('plains'/'pELP', 'Basic Land — Plains').
card_original_text('plains'/'pELP', '').
card_image_name('plains'/'pELP', 'plains3').
card_uid('plains'/'pELP', 'pELP:Plains:plains3').
card_rarity('plains'/'pELP', 'Basic Land').
card_artist('plains'/'pELP', 'Eric Peterson').
card_number('plains'/'pELP', '9').

card_in_set('swamp', 'pELP').
card_original_type('swamp'/'pELP', 'Basic Land — Swamp').
card_original_text('swamp'/'pELP', '').
card_image_name('swamp'/'pELP', 'swamp1').
card_uid('swamp'/'pELP', 'pELP:Swamp:swamp1').
card_rarity('swamp'/'pELP', 'Basic Land').
card_artist('swamp'/'pELP', 'Kev Walker').
card_number('swamp'/'pELP', '10').

card_in_set('swamp', 'pELP').
card_original_type('swamp'/'pELP', 'Basic Land — Swamp').
card_original_text('swamp'/'pELP', '').
card_image_name('swamp'/'pELP', 'swamp2').
card_uid('swamp'/'pELP', 'pELP:Swamp:swamp2').
card_rarity('swamp'/'pELP', 'Basic Land').
card_artist('swamp'/'pELP', 'Mike Ploog').
card_number('swamp'/'pELP', '15').

card_in_set('swamp', 'pELP').
card_original_type('swamp'/'pELP', 'Basic Land — Swamp').
card_original_text('swamp'/'pELP', '').
card_image_name('swamp'/'pELP', 'swamp3').
card_uid('swamp'/'pELP', 'pELP:Swamp:swamp3').
card_rarity('swamp'/'pELP', 'Basic Land').
card_artist('swamp'/'pELP', 'Scott Bailey').
card_number('swamp'/'pELP', '5').
