% Fifth Dawn

set('5DN').
set_name('5DN', 'Fifth Dawn').
set_release_date('5DN', '2004-06-04').
set_border('5DN', 'black').
set_type('5DN', 'expansion').
set_block('5DN', 'Mirrodin').

card_in_set('abuna\'s chant', '5DN').
card_original_type('abuna\'s chant'/'5DN', 'Instant').
card_original_text('abuna\'s chant'/'5DN', 'Choose one You gain 5 life; or prevent the next 5 damage that would be dealt to target creature this turn.\nEntwine {2} (Choose both if you pay the entwine cost.)').
card_first_print('abuna\'s chant', '5DN').
card_image_name('abuna\'s chant'/'5DN', 'abuna\'s chant').
card_uid('abuna\'s chant'/'5DN', '5DN:Abuna\'s Chant:abuna\'s chant').
card_rarity('abuna\'s chant'/'5DN', 'Common').
card_artist('abuna\'s chant'/'5DN', 'John Matson').
card_number('abuna\'s chant'/'5DN', '1').
card_multiverse_id('abuna\'s chant'/'5DN', '50164').

card_in_set('acquire', '5DN').
card_original_type('acquire'/'5DN', 'Sorcery').
card_original_text('acquire'/'5DN', 'Search target opponent\'s library for an artifact card and put that card into play under your control. Then that player shuffles his or her library.').
card_image_name('acquire'/'5DN', 'acquire').
card_uid('acquire'/'5DN', '5DN:Acquire:acquire').
card_rarity('acquire'/'5DN', 'Rare').
card_artist('acquire'/'5DN', 'Daren Bader').
card_number('acquire'/'5DN', '21').
card_flavor_text('acquire'/'5DN', 'Sometimes \"innovation\" means \"stealing another\'s secret before anyone else can.\"').
card_multiverse_id('acquire'/'5DN', '51088').

card_in_set('advanced hoverguard', '5DN').
card_original_type('advanced hoverguard'/'5DN', 'Creature — Drone').
card_original_text('advanced hoverguard'/'5DN', 'Flying\n{U}: Advanced Hoverguard can\'t be the target of spells or abilities this turn.').
card_first_print('advanced hoverguard', '5DN').
card_image_name('advanced hoverguard'/'5DN', 'advanced hoverguard').
card_uid('advanced hoverguard'/'5DN', '5DN:Advanced Hoverguard:advanced hoverguard').
card_rarity('advanced hoverguard'/'5DN', 'Common').
card_artist('advanced hoverguard'/'5DN', 'Scott M. Fischer').
card_number('advanced hoverguard'/'5DN', '22').
card_flavor_text('advanced hoverguard'/'5DN', '\"They are like their vedalken masters: untouchable, aloof, and omnipresent.\"\n—Bruenna, Neurok leader').
card_multiverse_id('advanced hoverguard'/'5DN', '50144').

card_in_set('all suns\' dawn', '5DN').
card_original_type('all suns\' dawn'/'5DN', 'Sorcery').
card_original_text('all suns\' dawn'/'5DN', 'For each color, return up to one target card of that color from your graveyard to your hand. Then remove All Suns\' Dawn from the game.').
card_first_print('all suns\' dawn', '5DN').
card_image_name('all suns\' dawn'/'5DN', 'all suns\' dawn').
card_uid('all suns\' dawn'/'5DN', '5DN:All Suns\' Dawn:all suns\' dawn').
card_rarity('all suns\' dawn'/'5DN', 'Rare').
card_artist('all suns\' dawn'/'5DN', 'Glen Angus').
card_number('all suns\' dawn'/'5DN', '81').
card_multiverse_id('all suns\' dawn'/'5DN', '51221').

card_in_set('anodet lurker', '5DN').
card_original_type('anodet lurker'/'5DN', 'Artifact Creature').
card_original_text('anodet lurker'/'5DN', 'When Anodet Lurker is put into a graveyard from play, you gain 3 life.').
card_first_print('anodet lurker', '5DN').
card_image_name('anodet lurker'/'5DN', 'anodet lurker').
card_uid('anodet lurker'/'5DN', '5DN:Anodet Lurker:anodet lurker').
card_rarity('anodet lurker'/'5DN', 'Common').
card_artist('anodet lurker'/'5DN', 'Jeff Easley').
card_number('anodet lurker'/'5DN', '101').
card_flavor_text('anodet lurker'/'5DN', 'These machines build fearsome visages out of scrap metal to scare off predators who would otherwise be eager to consume them.').
card_multiverse_id('anodet lurker'/'5DN', '51119').

card_in_set('arachnoid', '5DN').
card_original_type('arachnoid'/'5DN', 'Artifact Creature — Spider').
card_original_text('arachnoid'/'5DN', 'Arachnoid may block as though it had flying.').
card_first_print('arachnoid', '5DN').
card_image_name('arachnoid'/'5DN', 'arachnoid').
card_uid('arachnoid'/'5DN', '5DN:Arachnoid:arachnoid').
card_rarity('arachnoid'/'5DN', 'Uncommon').
card_artist('arachnoid'/'5DN', 'Greg Staples').
card_number('arachnoid'/'5DN', '102').
card_flavor_text('arachnoid'/'5DN', 'It follows the green sun over Mirrodin\'s surface, spinning massive webs of whispersilk as it goes.').
card_multiverse_id('arachnoid'/'5DN', '51230').

card_in_set('arcbound wanderer', '5DN').
card_original_type('arcbound wanderer'/'5DN', 'Artifact Creature').
card_original_text('arcbound wanderer'/'5DN', 'Modular—Sunburst (This comes into play with a +1/+1 counter on it for each color of mana used to pay its cost. When it\'s put into a graveyard, you may put its +1/+1 counters on target artifact creature.)').
card_first_print('arcbound wanderer', '5DN').
card_image_name('arcbound wanderer'/'5DN', 'arcbound wanderer').
card_uid('arcbound wanderer'/'5DN', '5DN:Arcbound Wanderer:arcbound wanderer').
card_rarity('arcbound wanderer'/'5DN', 'Uncommon').
card_artist('arcbound wanderer'/'5DN', 'Christopher Moeller').
card_number('arcbound wanderer'/'5DN', '103').
card_multiverse_id('arcbound wanderer'/'5DN', '72720').

card_in_set('armed response', '5DN').
card_original_type('armed response'/'5DN', 'Instant').
card_original_text('armed response'/'5DN', 'Armed Response deals damage to target attacking creature equal to the number of Equipment you control.').
card_first_print('armed response', '5DN').
card_image_name('armed response'/'5DN', 'armed response').
card_uid('armed response'/'5DN', '5DN:Armed Response:armed response').
card_rarity('armed response'/'5DN', 'Common').
card_artist('armed response'/'5DN', 'Doug Chaffee').
card_number('armed response'/'5DN', '2').
card_flavor_text('armed response'/'5DN', 'Raksha watched as the goblins continued to pour onto the Razor Fields. \"They just don\'t know when to stop, do they?\"').
card_multiverse_id('armed response'/'5DN', '73566').

card_in_set('artificer\'s intuition', '5DN').
card_original_type('artificer\'s intuition'/'5DN', 'Enchantment').
card_original_text('artificer\'s intuition'/'5DN', '{U}, Discard an artifact card from your hand: Search your library for an artifact card with converted mana cost 1 or less, reveal that card, and put it into your hand. Then shuffle your library.').
card_first_print('artificer\'s intuition', '5DN').
card_image_name('artificer\'s intuition'/'5DN', 'artificer\'s intuition').
card_uid('artificer\'s intuition'/'5DN', '5DN:Artificer\'s Intuition:artificer\'s intuition').
card_rarity('artificer\'s intuition'/'5DN', 'Rare').
card_artist('artificer\'s intuition'/'5DN', 'Wayne England').
card_number('artificer\'s intuition'/'5DN', '23').
card_multiverse_id('artificer\'s intuition'/'5DN', '50172').

card_in_set('auriok champion', '5DN').
card_original_type('auriok champion'/'5DN', 'Creature — Human Cleric').
card_original_text('auriok champion'/'5DN', 'Protection from black and from red\nWhenever another creature comes into play, you may gain 1 life.').
card_first_print('auriok champion', '5DN').
card_image_name('auriok champion'/'5DN', 'auriok champion').
card_uid('auriok champion'/'5DN', '5DN:Auriok Champion:auriok champion').
card_rarity('auriok champion'/'5DN', 'Rare').
card_artist('auriok champion'/'5DN', 'Michael Sutfin').
card_number('auriok champion'/'5DN', '3').
card_flavor_text('auriok champion'/'5DN', '\"Her will shall banish the shadows. Her might shall punish the lawless. Her beauty shall restore the light.\"\n—Ushanti, leonin seer').
card_multiverse_id('auriok champion'/'5DN', '72921').

card_in_set('auriok salvagers', '5DN').
card_original_type('auriok salvagers'/'5DN', 'Creature — Human Soldier').
card_original_text('auriok salvagers'/'5DN', '{1}{W}: Return target artifact card with converted mana cost 1 or less from your graveyard to your hand.').
card_first_print('auriok salvagers', '5DN').
card_image_name('auriok salvagers'/'5DN', 'auriok salvagers').
card_uid('auriok salvagers'/'5DN', '5DN:Auriok Salvagers:auriok salvagers').
card_rarity('auriok salvagers'/'5DN', 'Uncommon').
card_artist('auriok salvagers'/'5DN', 'Randy Gallegos').
card_number('auriok salvagers'/'5DN', '4').
card_flavor_text('auriok salvagers'/'5DN', '\"Memnarch or the vedalken salvage most of the large machines, leaving us only scraps. Scraps are enough.\"').
card_multiverse_id('auriok salvagers'/'5DN', '51166').

card_in_set('auriok windwalker', '5DN').
card_original_type('auriok windwalker'/'5DN', 'Creature — Human Wizard').
card_original_text('auriok windwalker'/'5DN', 'Flying\n{T}: Attach target Equipment you control to target creature you control.').
card_first_print('auriok windwalker', '5DN').
card_image_name('auriok windwalker'/'5DN', 'auriok windwalker').
card_uid('auriok windwalker'/'5DN', '5DN:Auriok Windwalker:auriok windwalker').
card_rarity('auriok windwalker'/'5DN', 'Rare').
card_artist('auriok windwalker'/'5DN', 'Jeremy Jarvis').
card_number('auriok windwalker'/'5DN', '5').
card_flavor_text('auriok windwalker'/'5DN', 'Each generation, a handful of Auriok girls are allowed to visit the Cave of Light. They return riding the wind and brandishing steel.').
card_multiverse_id('auriok windwalker'/'5DN', '46175').

card_in_set('avarice totem', '5DN').
card_original_type('avarice totem'/'5DN', 'Artifact').
card_original_text('avarice totem'/'5DN', '{5}: Exchange control of Avarice Totem and target nonland permanent.').
card_first_print('avarice totem', '5DN').
card_image_name('avarice totem'/'5DN', 'avarice totem').
card_uid('avarice totem'/'5DN', '5DN:Avarice Totem:avarice totem').
card_rarity('avarice totem'/'5DN', 'Uncommon').
card_artist('avarice totem'/'5DN', 'Ben Thompson').
card_number('avarice totem'/'5DN', '104').
card_flavor_text('avarice totem'/'5DN', 'Souls and steel, the essence of Mirrodin. Memnarch lacks one but has an abundance of the other.').
card_multiverse_id('avarice totem'/'5DN', '39667').

card_in_set('baton of courage', '5DN').
card_original_type('baton of courage'/'5DN', 'Artifact').
card_original_text('baton of courage'/'5DN', 'You may play Baton of Courage any time you could play an instant.\nSunburst (This comes into play with a charge counter on it for each color of mana used to pay its cost.)\nRemove a charge counter from Baton of Courage: Target creature gets +1/+1 until end of turn.').
card_first_print('baton of courage', '5DN').
card_image_name('baton of courage'/'5DN', 'baton of courage').
card_uid('baton of courage'/'5DN', '5DN:Baton of Courage:baton of courage').
card_rarity('baton of courage'/'5DN', 'Common').
card_artist('baton of courage'/'5DN', 'Heather Hudson').
card_number('baton of courage'/'5DN', '105').
card_multiverse_id('baton of courage'/'5DN', '51375').

card_in_set('battered golem', '5DN').
card_original_type('battered golem'/'5DN', 'Artifact Creature — Golem').
card_original_text('battered golem'/'5DN', 'Battered Golem doesn\'t untap during your untap step.\nWhenever an artifact comes into play, you may untap Battered Golem.').
card_first_print('battered golem', '5DN').
card_image_name('battered golem'/'5DN', 'battered golem').
card_uid('battered golem'/'5DN', '5DN:Battered Golem:battered golem').
card_rarity('battered golem'/'5DN', 'Common').
card_artist('battered golem'/'5DN', 'Carl Critchlow').
card_number('battered golem'/'5DN', '106').
card_flavor_text('battered golem'/'5DN', 'A dent marks every year of service, and it has as much rust as glory.').
card_multiverse_id('battered golem'/'5DN', '73932').

card_in_set('beacon of creation', '5DN').
card_original_type('beacon of creation'/'5DN', 'Sorcery').
card_original_text('beacon of creation'/'5DN', 'Put a 1/1 green Insect creature token into play for each Forest you control. Shuffle Beacon of Creation into its owner\'s library.').
card_first_print('beacon of creation', '5DN').
card_image_name('beacon of creation'/'5DN', 'beacon of creation').
card_uid('beacon of creation'/'5DN', '5DN:Beacon of Creation:beacon of creation').
card_rarity('beacon of creation'/'5DN', 'Rare').
card_artist('beacon of creation'/'5DN', 'Mark Tedin').
card_number('beacon of creation'/'5DN', '82').
card_flavor_text('beacon of creation'/'5DN', 'A chorus of life heralds the arrival of Mirrodin\'s final sun.').
card_multiverse_id('beacon of creation'/'5DN', '51613').

card_in_set('beacon of destruction', '5DN').
card_original_type('beacon of destruction'/'5DN', 'Instant').
card_original_text('beacon of destruction'/'5DN', 'Beacon of Destruction deals 5 damage to target creature or player. Shuffle Beacon of Destruction into its owner\'s library.').
card_first_print('beacon of destruction', '5DN').
card_image_name('beacon of destruction'/'5DN', 'beacon of destruction').
card_uid('beacon of destruction'/'5DN', '5DN:Beacon of Destruction:beacon of destruction').
card_rarity('beacon of destruction'/'5DN', 'Rare').
card_artist('beacon of destruction'/'5DN', 'Greg Hildebrandt').
card_number('beacon of destruction'/'5DN', '61').
card_flavor_text('beacon of destruction'/'5DN', 'The Great Furnace\'s blessing is a spectacular sight, but the best view comes at a high cost.').
card_multiverse_id('beacon of destruction'/'5DN', '51612').

card_in_set('beacon of immortality', '5DN').
card_original_type('beacon of immortality'/'5DN', 'Instant').
card_original_text('beacon of immortality'/'5DN', 'Double target player\'s life total. Shuffle Beacon of Immortality into its owner\'s library.').
card_first_print('beacon of immortality', '5DN').
card_image_name('beacon of immortality'/'5DN', 'beacon of immortality').
card_uid('beacon of immortality'/'5DN', '5DN:Beacon of Immortality:beacon of immortality').
card_rarity('beacon of immortality'/'5DN', 'Rare').
card_artist('beacon of immortality'/'5DN', 'Rob Alexander').
card_number('beacon of immortality'/'5DN', '6').
card_flavor_text('beacon of immortality'/'5DN', 'The cave floods with light. A thousand rays shine forth and meld into one.').
card_multiverse_id('beacon of immortality'/'5DN', '50141').

card_in_set('beacon of tomorrows', '5DN').
card_original_type('beacon of tomorrows'/'5DN', 'Sorcery').
card_original_text('beacon of tomorrows'/'5DN', 'Target player takes an extra turn after this one. Shuffle Beacon of Tomorrows into its owner\'s library.').
card_first_print('beacon of tomorrows', '5DN').
card_image_name('beacon of tomorrows'/'5DN', 'beacon of tomorrows').
card_uid('beacon of tomorrows'/'5DN', '5DN:Beacon of Tomorrows:beacon of tomorrows').
card_rarity('beacon of tomorrows'/'5DN', 'Rare').
card_artist('beacon of tomorrows'/'5DN', 'Jeremy Jarvis').
card_number('beacon of tomorrows'/'5DN', '24').
card_flavor_text('beacon of tomorrows'/'5DN', 'To look into its light is to meet the gaze of history.').
card_multiverse_id('beacon of tomorrows'/'5DN', '51610').

card_in_set('beacon of unrest', '5DN').
card_original_type('beacon of unrest'/'5DN', 'Sorcery').
card_original_text('beacon of unrest'/'5DN', 'Put target artifact or creature card from a graveyard into play under your control. Shuffle Beacon of Unrest into its owner\'s library.').
card_first_print('beacon of unrest', '5DN').
card_image_name('beacon of unrest'/'5DN', 'beacon of unrest').
card_uid('beacon of unrest'/'5DN', '5DN:Beacon of Unrest:beacon of unrest').
card_rarity('beacon of unrest'/'5DN', 'Rare').
card_artist('beacon of unrest'/'5DN', 'Alan Pollack').
card_number('beacon of unrest'/'5DN', '41').
card_flavor_text('beacon of unrest'/'5DN', 'A vertical scream pierces the night air and echoes doom through the clouds.').
card_multiverse_id('beacon of unrest'/'5DN', '51611').

card_in_set('blasting station', '5DN').
card_original_type('blasting station'/'5DN', 'Artifact').
card_original_text('blasting station'/'5DN', '{T}, Sacrifice a creature: Blasting Station deals 1 damage to target creature or player.\nWhenever a creature comes into play, you may untap Blasting Station.').
card_first_print('blasting station', '5DN').
card_image_name('blasting station'/'5DN', 'blasting station').
card_uid('blasting station'/'5DN', '5DN:Blasting Station:blasting station').
card_rarity('blasting station'/'5DN', 'Uncommon').
card_artist('blasting station'/'5DN', 'Stephen Tappin').
card_number('blasting station'/'5DN', '107').
card_multiverse_id('blasting station'/'5DN', '51131').

card_in_set('blind creeper', '5DN').
card_original_type('blind creeper'/'5DN', 'Creature — Zombie Beast').
card_original_text('blind creeper'/'5DN', 'Whenever a player plays a spell, Blind Creeper gets -1/-1 until end of turn.').
card_first_print('blind creeper', '5DN').
card_image_name('blind creeper'/'5DN', 'blind creeper').
card_uid('blind creeper'/'5DN', '5DN:Blind Creeper:blind creeper').
card_rarity('blind creeper'/'5DN', 'Common').
card_artist('blind creeper'/'5DN', 'Carl Critchlow').
card_number('blind creeper'/'5DN', '42').
card_flavor_text('blind creeper'/'5DN', 'It shuns everything except its next meal.').
card_multiverse_id('blind creeper'/'5DN', '50212').

card_in_set('blinkmoth infusion', '5DN').
card_original_type('blinkmoth infusion'/'5DN', 'Instant').
card_original_text('blinkmoth infusion'/'5DN', 'Affinity for artifacts (This spell costs {1} less to play for each artifact you control.)\nUntap all artifacts.').
card_first_print('blinkmoth infusion', '5DN').
card_image_name('blinkmoth infusion'/'5DN', 'blinkmoth infusion').
card_uid('blinkmoth infusion'/'5DN', '5DN:Blinkmoth Infusion:blinkmoth infusion').
card_rarity('blinkmoth infusion'/'5DN', 'Rare').
card_artist('blinkmoth infusion'/'5DN', 'Alan Pollack').
card_number('blinkmoth infusion'/'5DN', '25').
card_flavor_text('blinkmoth infusion'/'5DN', 'Power begets power.').
card_multiverse_id('blinkmoth infusion'/'5DN', '51634').

card_in_set('bringer of the black dawn', '5DN').
card_original_type('bringer of the black dawn'/'5DN', 'Creature — Bringer').
card_original_text('bringer of the black dawn'/'5DN', 'You may pay {W}{U}{B}{R}{G} rather than pay Bringer of the Black Dawn\'s mana cost.\nTrample\nAt the beginning of your upkeep, you may pay 2 life. If you do, search your library for a card, then shuffle your library and put that card on top of it.').
card_first_print('bringer of the black dawn', '5DN').
card_image_name('bringer of the black dawn'/'5DN', 'bringer of the black dawn').
card_uid('bringer of the black dawn'/'5DN', '5DN:Bringer of the Black Dawn:bringer of the black dawn').
card_rarity('bringer of the black dawn'/'5DN', 'Rare').
card_artist('bringer of the black dawn'/'5DN', 'Carl Critchlow').
card_number('bringer of the black dawn'/'5DN', '43').
card_multiverse_id('bringer of the black dawn'/'5DN', '73555').

card_in_set('bringer of the blue dawn', '5DN').
card_original_type('bringer of the blue dawn'/'5DN', 'Creature — Bringer').
card_original_text('bringer of the blue dawn'/'5DN', 'You may pay {W}{U}{B}{R}{G} rather than pay Bringer of the Blue Dawn\'s mana cost.\nTrample\nAt the beginning of your upkeep, you may draw two cards.').
card_first_print('bringer of the blue dawn', '5DN').
card_image_name('bringer of the blue dawn'/'5DN', 'bringer of the blue dawn').
card_uid('bringer of the blue dawn'/'5DN', '5DN:Bringer of the Blue Dawn:bringer of the blue dawn').
card_rarity('bringer of the blue dawn'/'5DN', 'Rare').
card_artist('bringer of the blue dawn'/'5DN', 'Greg Staples').
card_number('bringer of the blue dawn'/'5DN', '26').
card_multiverse_id('bringer of the blue dawn'/'5DN', '73554').

card_in_set('bringer of the green dawn', '5DN').
card_original_type('bringer of the green dawn'/'5DN', 'Creature — Bringer').
card_original_text('bringer of the green dawn'/'5DN', 'You may pay {W}{U}{B}{R}{G} rather than pay Bringer of the Green Dawn\'s mana cost.\nTrample\nAt the beginning of your upkeep, you may put a 3/3 green Beast creature token into play.').
card_first_print('bringer of the green dawn', '5DN').
card_image_name('bringer of the green dawn'/'5DN', 'bringer of the green dawn').
card_uid('bringer of the green dawn'/'5DN', '5DN:Bringer of the Green Dawn:bringer of the green dawn').
card_rarity('bringer of the green dawn'/'5DN', 'Rare').
card_artist('bringer of the green dawn'/'5DN', 'Jim Murray').
card_number('bringer of the green dawn'/'5DN', '83').
card_multiverse_id('bringer of the green dawn'/'5DN', '73552').

card_in_set('bringer of the red dawn', '5DN').
card_original_type('bringer of the red dawn'/'5DN', 'Creature — Bringer').
card_original_text('bringer of the red dawn'/'5DN', 'You may pay {W}{U}{B}{R}{G} rather than pay Bringer of the Red Dawn\'s mana cost.\nTrample\nAt the beginning of your upkeep, you may untap target creature and gain control of it until end of turn. That creature gains haste until end of turn.').
card_first_print('bringer of the red dawn', '5DN').
card_image_name('bringer of the red dawn'/'5DN', 'bringer of the red dawn').
card_uid('bringer of the red dawn'/'5DN', '5DN:Bringer of the Red Dawn:bringer of the red dawn').
card_rarity('bringer of the red dawn'/'5DN', 'Rare').
card_artist('bringer of the red dawn'/'5DN', 'Christopher Moeller').
card_number('bringer of the red dawn'/'5DN', '62').
card_multiverse_id('bringer of the red dawn'/'5DN', '73553').

card_in_set('bringer of the white dawn', '5DN').
card_original_type('bringer of the white dawn'/'5DN', 'Creature — Bringer').
card_original_text('bringer of the white dawn'/'5DN', 'You may pay {W}{U}{B}{R}{G} rather than pay Bringer of the White Dawn\'s mana cost.\nTrample\nAt the beginning of your upkeep, you may return target artifact card from your graveyard to play.').
card_first_print('bringer of the white dawn', '5DN').
card_image_name('bringer of the white dawn'/'5DN', 'bringer of the white dawn').
card_uid('bringer of the white dawn'/'5DN', '5DN:Bringer of the White Dawn:bringer of the white dawn').
card_rarity('bringer of the white dawn'/'5DN', 'Rare').
card_artist('bringer of the white dawn'/'5DN', 'Kev Walker').
card_number('bringer of the white dawn'/'5DN', '7').
card_multiverse_id('bringer of the white dawn'/'5DN', '73556').

card_in_set('cackling imp', '5DN').
card_original_type('cackling imp'/'5DN', 'Creature — Imp').
card_original_text('cackling imp'/'5DN', 'Flying\n{T}: Target player loses 1 life.').
card_first_print('cackling imp', '5DN').
card_image_name('cackling imp'/'5DN', 'cackling imp').
card_uid('cackling imp'/'5DN', '5DN:Cackling Imp:cackling imp').
card_rarity('cackling imp'/'5DN', 'Common').
card_artist('cackling imp'/'5DN', 'Matt Thompson').
card_number('cackling imp'/'5DN', '44').
card_flavor_text('cackling imp'/'5DN', 'Its laughter drives needles through your mind.').
card_multiverse_id('cackling imp'/'5DN', '73597').

card_in_set('channel the suns', '5DN').
card_original_type('channel the suns'/'5DN', 'Sorcery').
card_original_text('channel the suns'/'5DN', 'Add {W}{U}{B}{R}{G} to your mana pool.').
card_first_print('channel the suns', '5DN').
card_image_name('channel the suns'/'5DN', 'channel the suns').
card_uid('channel the suns'/'5DN', '5DN:Channel the Suns:channel the suns').
card_rarity('channel the suns'/'5DN', 'Uncommon').
card_artist('channel the suns'/'5DN', 'Rob Alexander').
card_number('channel the suns'/'5DN', '84').
card_flavor_text('channel the suns'/'5DN', 'To some the birth of the fifth sun was a sign of doom. To others it was a sign of salvation. But to all it was a sign.').
card_multiverse_id('channel the suns'/'5DN', '73565').

card_in_set('chimeric coils', '5DN').
card_original_type('chimeric coils'/'5DN', 'Artifact').
card_original_text('chimeric coils'/'5DN', '{X}{1}: Chimeric Coils becomes an X/X artifact creature. Sacrifice it at end of turn.').
card_first_print('chimeric coils', '5DN').
card_image_name('chimeric coils'/'5DN', 'chimeric coils').
card_uid('chimeric coils'/'5DN', '5DN:Chimeric Coils:chimeric coils').
card_rarity('chimeric coils'/'5DN', 'Uncommon').
card_artist('chimeric coils'/'5DN', 'Michael Sutfin').
card_number('chimeric coils'/'5DN', '108').
card_flavor_text('chimeric coils'/'5DN', 'The energy that infuses the rings with life also melts them away, leaving only useless puddles of slag.').
card_multiverse_id('chimeric coils'/'5DN', '50156').

card_in_set('circle of protection: artifacts', '5DN').
card_original_type('circle of protection: artifacts'/'5DN', 'Enchantment').
card_original_text('circle of protection: artifacts'/'5DN', '{2}: The next time an artifact source of your choice would deal damage to you this turn, prevent that damage.').
card_image_name('circle of protection: artifacts'/'5DN', 'circle of protection artifacts').
card_uid('circle of protection: artifacts'/'5DN', '5DN:Circle of Protection: Artifacts:circle of protection artifacts').
card_rarity('circle of protection: artifacts'/'5DN', 'Uncommon').
card_artist('circle of protection: artifacts'/'5DN', 'Terese Nielsen').
card_number('circle of protection: artifacts'/'5DN', '8').
card_flavor_text('circle of protection: artifacts'/'5DN', '\"Memnarch shall rage against us. We shall endure.\"').
card_multiverse_id('circle of protection: artifacts'/'5DN', '73561').

card_in_set('clearwater goblet', '5DN').
card_original_type('clearwater goblet'/'5DN', 'Artifact').
card_original_text('clearwater goblet'/'5DN', 'Sunburst (This comes into play with a charge counter on it for each color of mana used to pay its cost.)\nAt the beginning of your upkeep, you may gain 1 life for each charge counter on Clearwater Goblet.').
card_first_print('clearwater goblet', '5DN').
card_image_name('clearwater goblet'/'5DN', 'clearwater goblet').
card_uid('clearwater goblet'/'5DN', '5DN:Clearwater Goblet:clearwater goblet').
card_rarity('clearwater goblet'/'5DN', 'Rare').
card_artist('clearwater goblet'/'5DN', 'Dany Orizio').
card_number('clearwater goblet'/'5DN', '109').
card_multiverse_id('clearwater goblet'/'5DN', '50198').

card_in_set('clock of omens', '5DN').
card_original_type('clock of omens'/'5DN', 'Artifact').
card_original_text('clock of omens'/'5DN', 'Tap two untapped artifacts you control: Untap target artifact.').
card_first_print('clock of omens', '5DN').
card_image_name('clock of omens'/'5DN', 'clock of omens').
card_uid('clock of omens'/'5DN', '5DN:Clock of Omens:clock of omens').
card_rarity('clock of omens'/'5DN', 'Uncommon').
card_artist('clock of omens'/'5DN', 'Alex Horley-Orlandelli').
card_number('clock of omens'/'5DN', '110').
card_flavor_text('clock of omens'/'5DN', 'Those near the clock don\'t hear it anymore, but their every movement is in time with its ticking.').
card_multiverse_id('clock of omens'/'5DN', '51129').

card_in_set('composite golem', '5DN').
card_original_type('composite golem'/'5DN', 'Artifact Creature — Golem').
card_original_text('composite golem'/'5DN', 'Sacrifice Composite Golem: Add {W}{U}{B}{R}{G} to your mana pool.').
card_first_print('composite golem', '5DN').
card_image_name('composite golem'/'5DN', 'composite golem').
card_uid('composite golem'/'5DN', '5DN:Composite Golem:composite golem').
card_rarity('composite golem'/'5DN', 'Uncommon').
card_artist('composite golem'/'5DN', 'Mark Tedin').
card_number('composite golem'/'5DN', '111').
card_flavor_text('composite golem'/'5DN', 'It binds the five suns in human form.').
card_multiverse_id('composite golem'/'5DN', '73557').

card_in_set('condescend', '5DN').
card_original_type('condescend'/'5DN', 'Instant').
card_original_text('condescend'/'5DN', 'Counter target spell unless its controller pays {X}.\nScry 2 (Look at the top two cards of your library. Put any number of them on the bottom of your library and the rest on top in any order.)').
card_first_print('condescend', '5DN').
card_image_name('condescend'/'5DN', 'condescend').
card_uid('condescend'/'5DN', '5DN:Condescend:condescend').
card_rarity('condescend'/'5DN', 'Common').
card_artist('condescend'/'5DN', 'Ron Spears').
card_number('condescend'/'5DN', '27').
card_multiverse_id('condescend'/'5DN', '51223').

card_in_set('conjurer\'s bauble', '5DN').
card_original_type('conjurer\'s bauble'/'5DN', 'Artifact').
card_original_text('conjurer\'s bauble'/'5DN', '{T}, Sacrifice Conjurer\'s Bauble: Put up to one target card from your graveyard on the bottom of your library. Draw a card.').
card_first_print('conjurer\'s bauble', '5DN').
card_image_name('conjurer\'s bauble'/'5DN', 'conjurer\'s bauble').
card_uid('conjurer\'s bauble'/'5DN', '5DN:Conjurer\'s Bauble:conjurer\'s bauble').
card_rarity('conjurer\'s bauble'/'5DN', 'Common').
card_artist('conjurer\'s bauble'/'5DN', 'Darrell Riche').
card_number('conjurer\'s bauble'/'5DN', '112').
card_flavor_text('conjurer\'s bauble'/'5DN', 'It is the moment of discovery, the triumph of the mind, the reimagining of past glory.').
card_multiverse_id('conjurer\'s bauble'/'5DN', '50159').

card_in_set('cosmic larva', '5DN').
card_original_type('cosmic larva'/'5DN', 'Creature — Beast').
card_original_text('cosmic larva'/'5DN', 'Trample\nAt the beginning of your upkeep, sacrifice Cosmic Larva unless you sacrifice two lands.').
card_first_print('cosmic larva', '5DN').
card_image_name('cosmic larva'/'5DN', 'cosmic larva').
card_uid('cosmic larva'/'5DN', '5DN:Cosmic Larva:cosmic larva').
card_rarity('cosmic larva'/'5DN', 'Rare').
card_artist('cosmic larva'/'5DN', 'Jeff Easley').
card_number('cosmic larva'/'5DN', '63').
card_flavor_text('cosmic larva'/'5DN', 'For millennia they wandered from plane to plane, devouring all metal. Finally, their wanderings are over.').
card_multiverse_id('cosmic larva'/'5DN', '43529').

card_in_set('cranial plating', '5DN').
card_original_type('cranial plating'/'5DN', 'Artifact — Equipment').
card_original_text('cranial plating'/'5DN', 'Equipped creature gets +1/+0 for each artifact you control.\n{B}{B}: Attach Cranial Plating to target creature you control.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('cranial plating', '5DN').
card_image_name('cranial plating'/'5DN', 'cranial plating').
card_uid('cranial plating'/'5DN', '5DN:Cranial Plating:cranial plating').
card_rarity('cranial plating'/'5DN', 'Common').
card_artist('cranial plating'/'5DN', 'Adam Rex').
card_number('cranial plating'/'5DN', '113').
card_multiverse_id('cranial plating'/'5DN', '51184').

card_in_set('crucible of worlds', '5DN').
card_original_type('crucible of worlds'/'5DN', 'Artifact').
card_original_text('crucible of worlds'/'5DN', 'You may play land cards from your graveyard as though they were in your hand.').
card_image_name('crucible of worlds'/'5DN', 'crucible of worlds').
card_uid('crucible of worlds'/'5DN', '5DN:Crucible of Worlds:crucible of worlds').
card_rarity('crucible of worlds'/'5DN', 'Rare').
card_artist('crucible of worlds'/'5DN', 'Ron Spencer').
card_number('crucible of worlds'/'5DN', '114').
card_flavor_text('crucible of worlds'/'5DN', 'Amidst the darkest ashes grow the strongest seeds.').
card_multiverse_id('crucible of worlds'/'5DN', '51133').

card_in_set('dawn\'s reflection', '5DN').
card_original_type('dawn\'s reflection'/'5DN', 'Enchant Land').
card_original_text('dawn\'s reflection'/'5DN', 'Whenever enchanted land is tapped for mana, its controller adds two mana of any combination of colored mana to his or her mana pool.').
card_first_print('dawn\'s reflection', '5DN').
card_image_name('dawn\'s reflection'/'5DN', 'dawn\'s reflection').
card_uid('dawn\'s reflection'/'5DN', '5DN:Dawn\'s Reflection:dawn\'s reflection').
card_rarity('dawn\'s reflection'/'5DN', 'Common').
card_artist('dawn\'s reflection'/'5DN', 'John Avon').
card_number('dawn\'s reflection'/'5DN', '85').
card_flavor_text('dawn\'s reflection'/'5DN', 'Only Sylvok magic could truly capture the perfection of a Mirrodin sunrise.').
card_multiverse_id('dawn\'s reflection'/'5DN', '51222').

card_in_set('desecration elemental', '5DN').
card_original_type('desecration elemental'/'5DN', 'Creature — Elemental').
card_original_text('desecration elemental'/'5DN', 'Fear\nWhenever a player plays a spell, sacrifice a creature.').
card_first_print('desecration elemental', '5DN').
card_image_name('desecration elemental'/'5DN', 'desecration elemental').
card_uid('desecration elemental'/'5DN', '5DN:Desecration Elemental:desecration elemental').
card_rarity('desecration elemental'/'5DN', 'Rare').
card_artist('desecration elemental'/'5DN', 'Pete Venters').
card_number('desecration elemental'/'5DN', '45').
card_flavor_text('desecration elemental'/'5DN', 'Its precious corpses dance like puppets to attract mindless nim prey.').
card_multiverse_id('desecration elemental'/'5DN', '50204').

card_in_set('devour in shadow', '5DN').
card_original_type('devour in shadow'/'5DN', 'Instant').
card_original_text('devour in shadow'/'5DN', 'Destroy target creature. It can\'t be regenerated. You lose life equal to that creature\'s toughness.').
card_first_print('devour in shadow', '5DN').
card_image_name('devour in shadow'/'5DN', 'devour in shadow').
card_uid('devour in shadow'/'5DN', '5DN:Devour in Shadow:devour in shadow').
card_rarity('devour in shadow'/'5DN', 'Uncommon').
card_artist('devour in shadow'/'5DN', 'Thomas M. Baxa').
card_number('devour in shadow'/'5DN', '46').
card_flavor_text('devour in shadow'/'5DN', '\"Feel the burn of the mists. Master them. They will devour your enemies.\"\n—Geth, keeper of the Vault').
card_multiverse_id('devour in shadow'/'5DN', '50210').

card_in_set('disruption aura', '5DN').
card_original_type('disruption aura'/'5DN', 'Enchant Artifact').
card_original_text('disruption aura'/'5DN', 'Enchanted artifact has \"At the beginning of your upkeep, sacrifice this artifact unless you pay its mana cost.\"').
card_first_print('disruption aura', '5DN').
card_image_name('disruption aura'/'5DN', 'disruption aura').
card_uid('disruption aura'/'5DN', '5DN:Disruption Aura:disruption aura').
card_rarity('disruption aura'/'5DN', 'Uncommon').
card_artist('disruption aura'/'5DN', 'Ron Spears').
card_number('disruption aura'/'5DN', '28').
card_flavor_text('disruption aura'/'5DN', 'Vedalken weapons sold to outsiders are as dependable as the vedalken themselves.').
card_multiverse_id('disruption aura'/'5DN', '76971').

card_in_set('door to nothingness', '5DN').
card_original_type('door to nothingness'/'5DN', 'Artifact').
card_original_text('door to nothingness'/'5DN', 'Door to Nothingness comes into play tapped.\n{W}{W}{U}{U}{B}{B}{R}{R}{G}{G}, {T}, Sacrifice Door to Nothingness: Target player loses the game.').
card_first_print('door to nothingness', '5DN').
card_image_name('door to nothingness'/'5DN', 'door to nothingness').
card_uid('door to nothingness'/'5DN', '5DN:Door to Nothingness:door to nothingness').
card_rarity('door to nothingness'/'5DN', 'Rare').
card_artist('door to nothingness'/'5DN', 'Puddnhead').
card_number('door to nothingness'/'5DN', '115').
card_flavor_text('door to nothingness'/'5DN', '\"All memory of your existence will be wiped from reality. You will die, and no one will mourn.\"\n—Memnarch').
card_multiverse_id('door to nothingness'/'5DN', '73559').

card_in_set('doubling cube', '5DN').
card_original_type('doubling cube'/'5DN', 'Artifact').
card_original_text('doubling cube'/'5DN', '{3}, {T}: Double the amount of each type of mana in your mana pool.').
card_first_print('doubling cube', '5DN').
card_image_name('doubling cube'/'5DN', 'doubling cube').
card_uid('doubling cube'/'5DN', '5DN:Doubling Cube:doubling cube').
card_rarity('doubling cube'/'5DN', 'Rare').
card_artist('doubling cube'/'5DN', 'Mark Tedin').
card_number('doubling cube'/'5DN', '116').
card_flavor_text('doubling cube'/'5DN', 'The cube\'s surface is pockmarked with jagged runes that seem to shift when unobserved.').
card_multiverse_id('doubling cube'/'5DN', '51135').

card_in_set('dross crocodile', '5DN').
card_original_type('dross crocodile'/'5DN', 'Creature — Zombie Crocodile').
card_original_text('dross crocodile'/'5DN', '').
card_first_print('dross crocodile', '5DN').
card_image_name('dross crocodile'/'5DN', 'dross crocodile').
card_uid('dross crocodile'/'5DN', '5DN:Dross Crocodile:dross crocodile').
card_rarity('dross crocodile'/'5DN', 'Common').
card_artist('dross crocodile'/'5DN', 'Edward P. Beard, Jr.').
card_number('dross crocodile'/'5DN', '47').
card_flavor_text('dross crocodile'/'5DN', 'The necrogen may have taken its sight, but it can still taste the fear of its prey.').
card_multiverse_id('dross crocodile'/'5DN', '51189').

card_in_set('early frost', '5DN').
card_original_type('early frost'/'5DN', 'Instant').
card_original_text('early frost'/'5DN', 'Tap up to three target lands.').
card_first_print('early frost', '5DN').
card_image_name('early frost'/'5DN', 'early frost').
card_uid('early frost'/'5DN', '5DN:Early Frost:early frost').
card_rarity('early frost'/'5DN', 'Common').
card_artist('early frost'/'5DN', 'Ben Thompson').
card_number('early frost'/'5DN', '29').
card_flavor_text('early frost'/'5DN', 'Under its mists, the earth sleeps.').
card_multiverse_id('early frost'/'5DN', '73571').

card_in_set('ebon drake', '5DN').
card_original_type('ebon drake'/'5DN', 'Creature — Drake').
card_original_text('ebon drake'/'5DN', 'Flying\nWhenever a player plays a spell, you lose 1 life.').
card_first_print('ebon drake', '5DN').
card_image_name('ebon drake'/'5DN', 'ebon drake').
card_uid('ebon drake'/'5DN', '5DN:Ebon Drake:ebon drake').
card_rarity('ebon drake'/'5DN', 'Uncommon').
card_artist('ebon drake'/'5DN', 'Pete Venters').
card_number('ebon drake'/'5DN', '48').
card_flavor_text('ebon drake'/'5DN', 'The deadliest killers serve only those who are not afraid to die.').
card_multiverse_id('ebon drake'/'5DN', '51095').

card_in_set('endless whispers', '5DN').
card_original_type('endless whispers'/'5DN', 'Enchantment').
card_original_text('endless whispers'/'5DN', 'Each creature has \"When this creature is put into a graveyard from play, choose target opponent. That player puts this creature card from that graveyard into play under his or her control at end of turn.\"').
card_first_print('endless whispers', '5DN').
card_image_name('endless whispers'/'5DN', 'endless whispers').
card_uid('endless whispers'/'5DN', '5DN:Endless Whispers:endless whispers').
card_rarity('endless whispers'/'5DN', 'Rare').
card_artist('endless whispers'/'5DN', 'Carl Critchlow').
card_number('endless whispers'/'5DN', '49').
card_flavor_text('endless whispers'/'5DN', 'Those who find death under the black sun\'s shadow cannot escape its pull.').
card_multiverse_id('endless whispers'/'5DN', '50129').

card_in_set('energy chamber', '5DN').
card_original_type('energy chamber'/'5DN', 'Artifact').
card_original_text('energy chamber'/'5DN', 'At the beginning of your upkeep, choose one Put a +1/+1 counter on target artifact creature; or put a charge counter on target noncreature artifact.').
card_first_print('energy chamber', '5DN').
card_image_name('energy chamber'/'5DN', 'energy chamber').
card_uid('energy chamber'/'5DN', '5DN:Energy Chamber:energy chamber').
card_rarity('energy chamber'/'5DN', 'Uncommon').
card_artist('energy chamber'/'5DN', 'Kev Walker').
card_number('energy chamber'/'5DN', '117').
card_multiverse_id('energy chamber'/'5DN', '47438').

card_in_set('engineered explosives', '5DN').
card_original_type('engineered explosives'/'5DN', 'Artifact').
card_original_text('engineered explosives'/'5DN', 'Sunburst (This comes into play with a charge counter on it for each color of mana used to pay its cost.)\n{2}, Sacrifice Engineered Explosives: Destroy each nonland permanent with converted mana cost equal to the number of charge counters on Engineered Explosives.').
card_first_print('engineered explosives', '5DN').
card_image_name('engineered explosives'/'5DN', 'engineered explosives').
card_uid('engineered explosives'/'5DN', '5DN:Engineered Explosives:engineered explosives').
card_rarity('engineered explosives'/'5DN', 'Rare').
card_artist('engineered explosives'/'5DN', 'Ron Spears').
card_number('engineered explosives'/'5DN', '118').
card_multiverse_id('engineered explosives'/'5DN', '50139').

card_in_set('ensouled scimitar', '5DN').
card_original_type('ensouled scimitar'/'5DN', 'Artifact — Equipment').
card_original_text('ensouled scimitar'/'5DN', '{3}: Ensouled Scimitar becomes a 1/5 artifact creature with flying until end of turn. (Equipment that\'s a creature can\'t equip a creature.)\nEquipped creature gets +1/+5.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('ensouled scimitar', '5DN').
card_image_name('ensouled scimitar'/'5DN', 'ensouled scimitar').
card_uid('ensouled scimitar'/'5DN', '5DN:Ensouled Scimitar:ensouled scimitar').
card_rarity('ensouled scimitar'/'5DN', 'Uncommon').
card_artist('ensouled scimitar'/'5DN', 'Edward P. Beard, Jr.').
card_number('ensouled scimitar'/'5DN', '119').
card_multiverse_id('ensouled scimitar'/'5DN', '51115').

card_in_set('eon hub', '5DN').
card_original_type('eon hub'/'5DN', 'Artifact').
card_original_text('eon hub'/'5DN', 'Players skip their upkeep steps.').
card_first_print('eon hub', '5DN').
card_image_name('eon hub'/'5DN', 'eon hub').
card_uid('eon hub'/'5DN', '5DN:Eon Hub:eon hub').
card_rarity('eon hub'/'5DN', 'Rare').
card_artist('eon hub'/'5DN', 'Daren Bader').
card_number('eon hub'/'5DN', '120').
card_flavor_text('eon hub'/'5DN', 'Its power source is time itself. It absorbs unused minutes and hours, bringing the end of existence always closer.').
card_multiverse_id('eon hub'/'5DN', '51160').

card_in_set('etched oracle', '5DN').
card_original_type('etched oracle'/'5DN', 'Artifact Creature').
card_original_text('etched oracle'/'5DN', 'Sunburst (This comes into play with a +1/+1 counter on it for each color of mana used to pay its cost.)\n{1}, Remove four +1/+1 counters from Etched Oracle: Target player draws three cards.').
card_first_print('etched oracle', '5DN').
card_image_name('etched oracle'/'5DN', 'etched oracle').
card_uid('etched oracle'/'5DN', '5DN:Etched Oracle:etched oracle').
card_rarity('etched oracle'/'5DN', 'Uncommon').
card_artist('etched oracle'/'5DN', 'Matt Cavotta').
card_number('etched oracle'/'5DN', '121').
card_multiverse_id('etched oracle'/'5DN', '72725').

card_in_set('eternal witness', '5DN').
card_original_type('eternal witness'/'5DN', 'Creature — Human Shaman').
card_original_text('eternal witness'/'5DN', 'When Eternal Witness comes into play, you may return target card from your graveyard to your hand.').
card_image_name('eternal witness'/'5DN', 'eternal witness').
card_uid('eternal witness'/'5DN', '5DN:Eternal Witness:eternal witness').
card_rarity('eternal witness'/'5DN', 'Uncommon').
card_artist('eternal witness'/'5DN', 'Terese Nielsen').
card_number('eternal witness'/'5DN', '86').
card_flavor_text('eternal witness'/'5DN', 'She remembers every word spoken, from the hero\'s oath to the baby\'s cry.').
card_multiverse_id('eternal witness'/'5DN', '51628').

card_in_set('eyes of the watcher', '5DN').
card_original_type('eyes of the watcher'/'5DN', 'Enchantment').
card_original_text('eyes of the watcher'/'5DN', 'Whenever you play an instant or sorcery spell, you may pay {1}. If you do, scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_first_print('eyes of the watcher', '5DN').
card_image_name('eyes of the watcher'/'5DN', 'eyes of the watcher').
card_uid('eyes of the watcher'/'5DN', '5DN:Eyes of the Watcher:eyes of the watcher').
card_rarity('eyes of the watcher'/'5DN', 'Uncommon').
card_artist('eyes of the watcher'/'5DN', 'Ron Spears').
card_number('eyes of the watcher'/'5DN', '30').
card_multiverse_id('eyes of the watcher'/'5DN', '73925').

card_in_set('fangren pathcutter', '5DN').
card_original_type('fangren pathcutter'/'5DN', 'Creature — Beast').
card_original_text('fangren pathcutter'/'5DN', 'Whenever Fangren Pathcutter attacks, attacking creatures gain trample until end of turn.').
card_first_print('fangren pathcutter', '5DN').
card_image_name('fangren pathcutter'/'5DN', 'fangren pathcutter').
card_uid('fangren pathcutter'/'5DN', '5DN:Fangren Pathcutter:fangren pathcutter').
card_rarity('fangren pathcutter'/'5DN', 'Uncommon').
card_artist('fangren pathcutter'/'5DN', 'Rob Alexander').
card_number('fangren pathcutter'/'5DN', '87').
card_flavor_text('fangren pathcutter'/'5DN', '\"Tracking a fangren pack is easy. Catching up to one is fatal.\"\n—Viridian scout').
card_multiverse_id('fangren pathcutter'/'5DN', '50171').

card_in_set('feedback bolt', '5DN').
card_original_type('feedback bolt'/'5DN', 'Instant').
card_original_text('feedback bolt'/'5DN', 'Feedback Bolt deals damage to target player equal to the number of artifacts you control.').
card_first_print('feedback bolt', '5DN').
card_image_name('feedback bolt'/'5DN', 'feedback bolt').
card_uid('feedback bolt'/'5DN', '5DN:Feedback Bolt:feedback bolt').
card_rarity('feedback bolt'/'5DN', 'Uncommon').
card_artist('feedback bolt'/'5DN', 'Ron Spencer').
card_number('feedback bolt'/'5DN', '64').
card_flavor_text('feedback bolt'/'5DN', 'The path of least resistance is often through your enemies.').
card_multiverse_id('feedback bolt'/'5DN', '46108').

card_in_set('ferocious charge', '5DN').
card_original_type('ferocious charge'/'5DN', 'Instant').
card_original_text('ferocious charge'/'5DN', 'Target creature gets +4/+4 until end of turn.\nScry 2 (Look at the top two cards of your library. Put any number of them on the bottom of your library and the rest on top in any order.)').
card_first_print('ferocious charge', '5DN').
card_image_name('ferocious charge'/'5DN', 'ferocious charge').
card_uid('ferocious charge'/'5DN', '5DN:Ferocious Charge:ferocious charge').
card_rarity('ferocious charge'/'5DN', 'Common').
card_artist('ferocious charge'/'5DN', 'Anthony S. Waters').
card_number('ferocious charge'/'5DN', '88').
card_multiverse_id('ferocious charge'/'5DN', '51617').

card_in_set('ferropede', '5DN').
card_original_type('ferropede'/'5DN', 'Artifact Creature — Insect').
card_original_text('ferropede'/'5DN', 'Ferropede is unblockable.\nWhenever Ferropede deals combat damage to a player, you may remove a counter from target permanent.').
card_first_print('ferropede', '5DN').
card_image_name('ferropede'/'5DN', 'ferropede').
card_uid('ferropede'/'5DN', '5DN:Ferropede:ferropede').
card_rarity('ferropede'/'5DN', 'Uncommon').
card_artist('ferropede'/'5DN', 'Jeff Easley').
card_number('ferropede'/'5DN', '122').
card_multiverse_id('ferropede'/'5DN', '51113').

card_in_set('fill with fright', '5DN').
card_original_type('fill with fright'/'5DN', 'Sorcery').
card_original_text('fill with fright'/'5DN', 'Target player discards two cards from his or her hand.\nScry 2 (Look at the top two cards of your library. Put any number of them on the bottom of your library and the rest on top in any order.)').
card_first_print('fill with fright', '5DN').
card_image_name('fill with fright'/'5DN', 'fill with fright').
card_uid('fill with fright'/'5DN', '5DN:Fill with Fright:fill with fright').
card_rarity('fill with fright'/'5DN', 'Common').
card_artist('fill with fright'/'5DN', 'Luca Zontini').
card_number('fill with fright'/'5DN', '50').
card_multiverse_id('fill with fright'/'5DN', '46159').

card_in_set('fist of suns', '5DN').
card_original_type('fist of suns'/'5DN', 'Artifact').
card_original_text('fist of suns'/'5DN', 'You may pay {W}{U}{B}{R}{G} rather than pay the mana cost for spells that you play.').
card_first_print('fist of suns', '5DN').
card_image_name('fist of suns'/'5DN', 'fist of suns').
card_uid('fist of suns'/'5DN', '5DN:Fist of Suns:fist of suns').
card_rarity('fist of suns'/'5DN', 'Rare').
card_artist('fist of suns'/'5DN', 'Arnie Swekel').
card_number('fist of suns'/'5DN', '123').
card_flavor_text('fist of suns'/'5DN', '\"It is all in my grasp. Every petty dream, every grand scheme, every soul, every fear. It all flows through me and from me.\"\n—Memnarch').
card_multiverse_id('fist of suns'/'5DN', '50117').

card_in_set('fleshgrafter', '5DN').
card_original_type('fleshgrafter'/'5DN', 'Creature — Human Warrior').
card_original_text('fleshgrafter'/'5DN', 'Discard an artifact card from your hand: Fleshgrafter gets +2/+2 until end of turn.').
card_first_print('fleshgrafter', '5DN').
card_image_name('fleshgrafter'/'5DN', 'fleshgrafter').
card_uid('fleshgrafter'/'5DN', '5DN:Fleshgrafter:fleshgrafter').
card_rarity('fleshgrafter'/'5DN', 'Common').
card_artist('fleshgrafter'/'5DN', 'Jim Murray').
card_number('fleshgrafter'/'5DN', '51').
card_flavor_text('fleshgrafter'/'5DN', 'Necrogen enflames certain emotional centers in the Morioks\' brains: hate, aggression, and especially greed.').
card_multiverse_id('fleshgrafter'/'5DN', '50209').

card_in_set('fold into æther', '5DN').
card_original_type('fold into æther'/'5DN', 'Instant').
card_original_text('fold into æther'/'5DN', 'Counter target spell. If you do, that spell\'s controller may put a creature card from his or her hand into play.').
card_first_print('fold into æther', '5DN').
card_image_name('fold into æther'/'5DN', 'fold into aether').
card_uid('fold into æther'/'5DN', '5DN:Fold into Æther:fold into aether').
card_rarity('fold into æther'/'5DN', 'Uncommon').
card_artist('fold into æther'/'5DN', 'Anthony S. Waters').
card_number('fold into æther'/'5DN', '31').
card_flavor_text('fold into æther'/'5DN', 'Æther portals drift across Mirrodin\'s surface, dragging creatures across time and space.').
card_multiverse_id('fold into æther'/'5DN', '51631').

card_in_set('furnace whelp', '5DN').
card_original_type('furnace whelp'/'5DN', 'Creature — Dragon').
card_original_text('furnace whelp'/'5DN', 'Flying\n{R}: Furnace Whelp gets +1/+0 until end of turn.').
card_first_print('furnace whelp', '5DN').
card_image_name('furnace whelp'/'5DN', 'furnace whelp').
card_uid('furnace whelp'/'5DN', '5DN:Furnace Whelp:furnace whelp').
card_rarity('furnace whelp'/'5DN', 'Uncommon').
card_artist('furnace whelp'/'5DN', 'Matt Cavotta').
card_number('furnace whelp'/'5DN', '65').
card_flavor_text('furnace whelp'/'5DN', 'Baby dragons can\'t figure out humans—if they didn\'t want to be killed, why were they made of meat and treasure?').
card_multiverse_id('furnace whelp'/'5DN', '73578').

card_in_set('gemstone array', '5DN').
card_original_type('gemstone array'/'5DN', 'Artifact').
card_original_text('gemstone array'/'5DN', '{2}: Put a charge counter on Gemstone Array.\nRemove a charge counter from Gemstone Array: Add one mana of any color to your mana pool.').
card_first_print('gemstone array', '5DN').
card_image_name('gemstone array'/'5DN', 'gemstone array').
card_uid('gemstone array'/'5DN', '5DN:Gemstone Array:gemstone array').
card_rarity('gemstone array'/'5DN', 'Uncommon').
card_artist('gemstone array'/'5DN', 'Ittoku').
card_number('gemstone array'/'5DN', '124').
card_flavor_text('gemstone array'/'5DN', 'Outside, it reflects the suns\' light. Inside, it harvests the suns\' power.').
card_multiverse_id('gemstone array'/'5DN', '50150').

card_in_set('goblin brawler', '5DN').
card_original_type('goblin brawler'/'5DN', 'Creature — Goblin Warrior').
card_original_text('goblin brawler'/'5DN', 'First strike\nGoblin Brawler can\'t be equipped.').
card_first_print('goblin brawler', '5DN').
card_image_name('goblin brawler'/'5DN', 'goblin brawler').
card_uid('goblin brawler'/'5DN', '5DN:Goblin Brawler:goblin brawler').
card_rarity('goblin brawler'/'5DN', 'Common').
card_artist('goblin brawler'/'5DN', 'Heather Hudson').
card_number('goblin brawler'/'5DN', '66').
card_flavor_text('goblin brawler'/'5DN', 'Perhaps he shuns technology. Perhaps he appreciates nature. Perhaps he doesn\'t know the difference.').
card_multiverse_id('goblin brawler'/'5DN', '51170').

card_in_set('goblin cannon', '5DN').
card_original_type('goblin cannon'/'5DN', 'Artifact').
card_original_text('goblin cannon'/'5DN', '{2}: Goblin Cannon deals 1 damage to target creature or player. Sacrifice Goblin Cannon.').
card_first_print('goblin cannon', '5DN').
card_image_name('goblin cannon'/'5DN', 'goblin cannon').
card_uid('goblin cannon'/'5DN', '5DN:Goblin Cannon:goblin cannon').
card_rarity('goblin cannon'/'5DN', 'Uncommon').
card_artist('goblin cannon'/'5DN', 'Doug Chaffee').
card_number('goblin cannon'/'5DN', '125').
card_flavor_text('goblin cannon'/'5DN', 'Goblin cannon crews are known not for their aim but for their ability to dive for cover.').
card_multiverse_id('goblin cannon'/'5DN', '48215').

card_in_set('grafted wargear', '5DN').
card_original_type('grafted wargear'/'5DN', 'Artifact — Equipment').
card_original_text('grafted wargear'/'5DN', 'Equipped creature gets +3/+2.\nWhenever Grafted Wargear becomes unattached from a creature, sacrifice that creature.\nEquip {0} ({0}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('grafted wargear', '5DN').
card_image_name('grafted wargear'/'5DN', 'grafted wargear').
card_uid('grafted wargear'/'5DN', '5DN:Grafted Wargear:grafted wargear').
card_rarity('grafted wargear'/'5DN', 'Uncommon').
card_artist('grafted wargear'/'5DN', 'Alex Horley-Orlandelli').
card_number('grafted wargear'/'5DN', '126').
card_multiverse_id('grafted wargear'/'5DN', '50927').

card_in_set('granulate', '5DN').
card_original_type('granulate'/'5DN', 'Sorcery').
card_original_text('granulate'/'5DN', 'Destroy each nonland artifact with converted mana cost 4 or less.').
card_first_print('granulate', '5DN').
card_image_name('granulate'/'5DN', 'granulate').
card_uid('granulate'/'5DN', '5DN:Granulate:granulate').
card_rarity('granulate'/'5DN', 'Rare').
card_artist('granulate'/'5DN', 'Brian Snõddy').
card_number('granulate'/'5DN', '67').
card_flavor_text('granulate'/'5DN', 'There aren\'t that many ways to destroy a solid steel weapon, but somehow the goblins keep finding new ones.').
card_multiverse_id('granulate'/'5DN', '72721').

card_in_set('grinding station', '5DN').
card_original_type('grinding station'/'5DN', 'Artifact').
card_original_text('grinding station'/'5DN', '{T}, Sacrifice an artifact: Target player puts the top three cards of his or her library into his or her graveyard.\nWhenever an artifact comes into play, you may untap Grinding Station.').
card_first_print('grinding station', '5DN').
card_image_name('grinding station'/'5DN', 'grinding station').
card_uid('grinding station'/'5DN', '5DN:Grinding Station:grinding station').
card_rarity('grinding station'/'5DN', 'Uncommon').
card_artist('grinding station'/'5DN', 'Greg Staples').
card_number('grinding station'/'5DN', '127').
card_multiverse_id('grinding station'/'5DN', '51229').

card_in_set('guardian idol', '5DN').
card_original_type('guardian idol'/'5DN', 'Artifact').
card_original_text('guardian idol'/'5DN', 'Guardian Idol comes into play tapped.\n{T}: Add {1} to your mana pool.\n{2}: Guardian Idol becomes a 2/2 artifact creature until end of turn.').
card_first_print('guardian idol', '5DN').
card_image_name('guardian idol'/'5DN', 'guardian idol').
card_uid('guardian idol'/'5DN', '5DN:Guardian Idol:guardian idol').
card_rarity('guardian idol'/'5DN', 'Uncommon').
card_artist('guardian idol'/'5DN', 'Edward P. Beard, Jr.').
card_number('guardian idol'/'5DN', '128').
card_multiverse_id('guardian idol'/'5DN', '51138').

card_in_set('healer\'s headdress', '5DN').
card_original_type('healer\'s headdress'/'5DN', 'Artifact — Equipment').
card_original_text('healer\'s headdress'/'5DN', 'Equipped creature gets +0/+2 and has \"{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.\"\n{W}{W}: Attach Healer\'s Headdress to target creature you control.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('healer\'s headdress', '5DN').
card_image_name('healer\'s headdress'/'5DN', 'healer\'s headdress').
card_uid('healer\'s headdress'/'5DN', '5DN:Healer\'s Headdress:healer\'s headdress').
card_rarity('healer\'s headdress'/'5DN', 'Common').
card_artist('healer\'s headdress'/'5DN', 'Carl Critchlow').
card_number('healer\'s headdress'/'5DN', '129').
card_multiverse_id('healer\'s headdress'/'5DN', '51182').

card_in_set('heliophial', '5DN').
card_original_type('heliophial'/'5DN', 'Artifact').
card_original_text('heliophial'/'5DN', 'Sunburst (This comes into play with a charge counter on it for each color of mana used to pay its cost.)\n{2}, Sacrifice Heliophial: Heliophial deals damage to target creature or player equal to the number of charge counters on Heliophial.').
card_first_print('heliophial', '5DN').
card_image_name('heliophial'/'5DN', 'heliophial').
card_uid('heliophial'/'5DN', '5DN:Heliophial:heliophial').
card_rarity('heliophial'/'5DN', 'Common').
card_artist('heliophial'/'5DN', 'Wayne England').
card_number('heliophial'/'5DN', '130').
card_multiverse_id('heliophial'/'5DN', '50192').

card_in_set('helm of kaldra', '5DN').
card_original_type('helm of kaldra'/'5DN', 'Legendary Artifact — Equipment').
card_original_text('helm of kaldra'/'5DN', 'Equipped creature has first strike, trample, and haste.\n{1}: If you control Equipment named Helm of Kaldra, Sword of Kaldra, and Shield of Kaldra, put a 4/4 colorless Avatar Legend creature token named Kaldra into play and attach those Equipment to it.\nEquip {2}').
card_image_name('helm of kaldra'/'5DN', 'helm of kaldra').
card_uid('helm of kaldra'/'5DN', '5DN:Helm of Kaldra:helm of kaldra').
card_rarity('helm of kaldra'/'5DN', 'Rare').
card_artist('helm of kaldra'/'5DN', 'Donato Giancola').
card_number('helm of kaldra'/'5DN', '131').
card_multiverse_id('helm of kaldra'/'5DN', '47449').

card_in_set('horned helm', '5DN').
card_original_type('horned helm'/'5DN', 'Artifact — Equipment').
card_original_text('horned helm'/'5DN', 'Equipped creature gets +1/+1 and has trample.\n{G}{G}: Attach Horned Helm to target creature you control.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('horned helm', '5DN').
card_image_name('horned helm'/'5DN', 'horned helm').
card_uid('horned helm'/'5DN', '5DN:Horned Helm:horned helm').
card_rarity('horned helm'/'5DN', 'Common').
card_artist('horned helm'/'5DN', 'Martina Pilcerova').
card_number('horned helm'/'5DN', '132').
card_multiverse_id('horned helm'/'5DN', '51186').

card_in_set('hoverguard sweepers', '5DN').
card_original_type('hoverguard sweepers'/'5DN', 'Creature — Drone').
card_original_text('hoverguard sweepers'/'5DN', 'Flying\nWhen Hoverguard Sweepers comes into play, you may return up to two target creatures to their owners\' hands.').
card_first_print('hoverguard sweepers', '5DN').
card_image_name('hoverguard sweepers'/'5DN', 'hoverguard sweepers').
card_uid('hoverguard sweepers'/'5DN', '5DN:Hoverguard Sweepers:hoverguard sweepers').
card_rarity('hoverguard sweepers'/'5DN', 'Rare').
card_artist('hoverguard sweepers'/'5DN', 'Mark A. Nelson').
card_number('hoverguard sweepers'/'5DN', '32').
card_flavor_text('hoverguard sweepers'/'5DN', 'They only appear in emergencies, and emergencies never last long.').
card_multiverse_id('hoverguard sweepers'/'5DN', '51086').

card_in_set('infused arrows', '5DN').
card_original_type('infused arrows'/'5DN', 'Artifact').
card_original_text('infused arrows'/'5DN', 'Sunburst (This comes into play with a charge counter on it for each color of mana used to pay its cost.)\n{T}, Remove X charge counters from Infused Arrows: Target creature gets -X/-X until end of turn.').
card_first_print('infused arrows', '5DN').
card_image_name('infused arrows'/'5DN', 'infused arrows').
card_uid('infused arrows'/'5DN', '5DN:Infused Arrows:infused arrows').
card_rarity('infused arrows'/'5DN', 'Uncommon').
card_artist('infused arrows'/'5DN', 'Stephen Tappin').
card_number('infused arrows'/'5DN', '133').
card_multiverse_id('infused arrows'/'5DN', '51146').

card_in_set('into thin air', '5DN').
card_original_type('into thin air'/'5DN', 'Instant').
card_original_text('into thin air'/'5DN', 'Affinity for artifacts (This spell costs {1} less to play for each artifact you control.)\nReturn target artifact to its owner\'s hand.').
card_first_print('into thin air', '5DN').
card_image_name('into thin air'/'5DN', 'into thin air').
card_uid('into thin air'/'5DN', '5DN:Into Thin Air:into thin air').
card_rarity('into thin air'/'5DN', 'Common').
card_artist('into thin air'/'5DN', 'Ittoku').
card_number('into thin air'/'5DN', '33').
card_flavor_text('into thin air'/'5DN', '\"Perhaps I should take Mirrodin apart and begin again.\"\n—Memnarch').
card_multiverse_id('into thin air'/'5DN', '50147').

card_in_set('ion storm', '5DN').
card_original_type('ion storm'/'5DN', 'Enchantment').
card_original_text('ion storm'/'5DN', '{1}{R}, Remove a +1/+1 counter or a charge counter from a permanent you control: Ion Storm deals 2 damage to target creature or player.').
card_first_print('ion storm', '5DN').
card_image_name('ion storm'/'5DN', 'ion storm').
card_uid('ion storm'/'5DN', '5DN:Ion Storm:ion storm').
card_rarity('ion storm'/'5DN', 'Rare').
card_artist('ion storm'/'5DN', 'Michael Sutfin').
card_number('ion storm'/'5DN', '68').
card_flavor_text('ion storm'/'5DN', 'When enough artifacts lie too long in the same place, their magical radiation weakens reality, opening rifts of unpredictable destructive power.').
card_multiverse_id('ion storm'/'5DN', '72925').

card_in_set('iron-barb hellion', '5DN').
card_original_type('iron-barb hellion'/'5DN', 'Creature — Beast').
card_original_text('iron-barb hellion'/'5DN', 'Haste\nIron-Barb Hellion can\'t block.').
card_first_print('iron-barb hellion', '5DN').
card_image_name('iron-barb hellion'/'5DN', 'iron-barb hellion').
card_uid('iron-barb hellion'/'5DN', '5DN:Iron-Barb Hellion:iron-barb hellion').
card_rarity('iron-barb hellion'/'5DN', 'Uncommon').
card_artist('iron-barb hellion'/'5DN', 'Doug Chaffee').
card_number('iron-barb hellion'/'5DN', '69').
card_flavor_text('iron-barb hellion'/'5DN', 'When you feel a rumble in the ground, hold perfectly still. One sound will bring the hellion.').
card_multiverse_id('iron-barb hellion'/'5DN', '44608').

card_in_set('joiner adept', '5DN').
card_original_type('joiner adept'/'5DN', 'Creature — Elf Druid').
card_original_text('joiner adept'/'5DN', 'Lands you control have \"{T}: Add one mana of any color to your mana pool.\"').
card_first_print('joiner adept', '5DN').
card_image_name('joiner adept'/'5DN', 'joiner adept').
card_uid('joiner adept'/'5DN', '5DN:Joiner Adept:joiner adept').
card_rarity('joiner adept'/'5DN', 'Rare').
card_artist('joiner adept'/'5DN', 'Christopher Rush').
card_number('joiner adept'/'5DN', '89').
card_flavor_text('joiner adept'/'5DN', 'A talented apprentice makes all things. The master makes all things possible.').
card_multiverse_id('joiner adept'/'5DN', '72928').

card_in_set('krark-clan engineers', '5DN').
card_original_type('krark-clan engineers'/'5DN', 'Creature — Goblin Artificer').
card_original_text('krark-clan engineers'/'5DN', '{R}, Sacrifice two artifacts: Destroy target artifact.').
card_first_print('krark-clan engineers', '5DN').
card_image_name('krark-clan engineers'/'5DN', 'krark-clan engineers').
card_uid('krark-clan engineers'/'5DN', '5DN:Krark-Clan Engineers:krark-clan engineers').
card_rarity('krark-clan engineers'/'5DN', 'Uncommon').
card_artist('krark-clan engineers'/'5DN', 'Pete Venters').
card_number('krark-clan engineers'/'5DN', '70').
card_flavor_text('krark-clan engineers'/'5DN', '\"Well, I jammed the whatsit into the whackdoodle, but I think I broke the thingamajigger.\"').
card_multiverse_id('krark-clan engineers'/'5DN', '50201').

card_in_set('krark-clan ironworks', '5DN').
card_original_type('krark-clan ironworks'/'5DN', 'Artifact').
card_original_text('krark-clan ironworks'/'5DN', 'Sacrifice an artifact: Add {2} to your mana pool.').
card_first_print('krark-clan ironworks', '5DN').
card_image_name('krark-clan ironworks'/'5DN', 'krark-clan ironworks').
card_uid('krark-clan ironworks'/'5DN', '5DN:Krark-Clan Ironworks:krark-clan ironworks').
card_rarity('krark-clan ironworks'/'5DN', 'Uncommon').
card_artist('krark-clan ironworks'/'5DN', 'Tim Hildebrandt').
card_number('krark-clan ironworks'/'5DN', '134').
card_flavor_text('krark-clan ironworks'/'5DN', 'All the junk, refuse, and offal of the Krark Clan is diligently collected, melted down, and made into more junk, refuse, and offal.').
card_multiverse_id('krark-clan ironworks'/'5DN', '51633').

card_in_set('krark-clan ogre', '5DN').
card_original_type('krark-clan ogre'/'5DN', 'Creature — Ogre').
card_original_text('krark-clan ogre'/'5DN', '{R}, Sacrifice an artifact: Target creature can\'t block this turn.').
card_first_print('krark-clan ogre', '5DN').
card_image_name('krark-clan ogre'/'5DN', 'krark-clan ogre').
card_uid('krark-clan ogre'/'5DN', '5DN:Krark-Clan Ogre:krark-clan ogre').
card_rarity('krark-clan ogre'/'5DN', 'Common').
card_artist('krark-clan ogre'/'5DN', 'Paolo Parente').
card_number('krark-clan ogre'/'5DN', '71').
card_flavor_text('krark-clan ogre'/'5DN', 'The Krark Clan believes it\'s caught an ogre. The ogre believes he\'s caught seven goblins.').
card_multiverse_id('krark-clan ogre'/'5DN', '51097').

card_in_set('lantern of insight', '5DN').
card_original_type('lantern of insight'/'5DN', 'Artifact').
card_original_text('lantern of insight'/'5DN', 'Each player plays with the top card of his or her library revealed.\n{T}, Sacrifice Lantern of Insight: Target player shuffles his or her library.').
card_first_print('lantern of insight', '5DN').
card_image_name('lantern of insight'/'5DN', 'lantern of insight').
card_uid('lantern of insight'/'5DN', '5DN:Lantern of Insight:lantern of insight').
card_rarity('lantern of insight'/'5DN', 'Uncommon').
card_artist('lantern of insight'/'5DN', 'Tim Hildebrandt').
card_number('lantern of insight'/'5DN', '135').
card_flavor_text('lantern of insight'/'5DN', 'Each eye sees a different possibility for tomorrow.').
card_multiverse_id('lantern of insight'/'5DN', '73927').

card_in_set('leonin squire', '5DN').
card_original_type('leonin squire'/'5DN', 'Creature — Cat Soldier').
card_original_text('leonin squire'/'5DN', 'When Leonin Squire comes into play, return target artifact card with converted mana cost 1 or less from your graveyard to your hand.').
card_first_print('leonin squire', '5DN').
card_image_name('leonin squire'/'5DN', 'leonin squire').
card_uid('leonin squire'/'5DN', '5DN:Leonin Squire:leonin squire').
card_rarity('leonin squire'/'5DN', 'Common').
card_artist('leonin squire'/'5DN', 'Pete Venters').
card_number('leonin squire'/'5DN', '9').
card_flavor_text('leonin squire'/'5DN', '\"I may be kha, but without my soldiers, my people, I am nothing.\"\n—Raksha Golden Cub').
card_multiverse_id('leonin squire'/'5DN', '72722').

card_in_set('lose hope', '5DN').
card_original_type('lose hope'/'5DN', 'Instant').
card_original_text('lose hope'/'5DN', 'Target creature gets -1/-1 until end of turn.\nScry 2 (Look at the top two cards of your library. Put any number of them on the bottom of your library and the rest on top in any order.)').
card_first_print('lose hope', '5DN').
card_image_name('lose hope'/'5DN', 'lose hope').
card_uid('lose hope'/'5DN', '5DN:Lose Hope:lose hope').
card_rarity('lose hope'/'5DN', 'Common').
card_artist('lose hope'/'5DN', 'Matt Cavotta').
card_number('lose hope'/'5DN', '52').
card_multiverse_id('lose hope'/'5DN', '50207').

card_in_set('loxodon anchorite', '5DN').
card_original_type('loxodon anchorite'/'5DN', 'Creature — Elephant Cleric').
card_original_text('loxodon anchorite'/'5DN', '{T}: Prevent the next 2 damage that would be dealt to target creature or player this turn.').
card_first_print('loxodon anchorite', '5DN').
card_image_name('loxodon anchorite'/'5DN', 'loxodon anchorite').
card_uid('loxodon anchorite'/'5DN', '5DN:Loxodon Anchorite:loxodon anchorite').
card_rarity('loxodon anchorite'/'5DN', 'Common').
card_artist('loxodon anchorite'/'5DN', 'Jim Nelson').
card_number('loxodon anchorite'/'5DN', '10').
card_flavor_text('loxodon anchorite'/'5DN', 'The loxodon ascribe religious superiority to those born albino, believing them to be spiritually pure.').
card_multiverse_id('loxodon anchorite'/'5DN', '50137').

card_in_set('loxodon stalwart', '5DN').
card_original_type('loxodon stalwart'/'5DN', 'Creature — Elephant Soldier').
card_original_text('loxodon stalwart'/'5DN', 'Attacking doesn\'t cause Loxodon Stalwart to tap.\n{W}: Loxodon Stalwart gets +0/+1 until end of turn.').
card_first_print('loxodon stalwart', '5DN').
card_image_name('loxodon stalwart'/'5DN', 'loxodon stalwart').
card_uid('loxodon stalwart'/'5DN', '5DN:Loxodon Stalwart:loxodon stalwart').
card_rarity('loxodon stalwart'/'5DN', 'Uncommon').
card_artist('loxodon stalwart'/'5DN', 'Paolo Parente').
card_number('loxodon stalwart'/'5DN', '11').
card_flavor_text('loxodon stalwart'/'5DN', 'Long ago, the Auriok attempted peace with the loxodons. The leonin attempted war. Neither succeeded.').
card_multiverse_id('loxodon stalwart'/'5DN', '73567').

card_in_set('lunar avenger', '5DN').
card_original_type('lunar avenger'/'5DN', 'Artifact Creature — Golem').
card_original_text('lunar avenger'/'5DN', 'Sunburst (This comes into play with a +1/+1 counter on it for each color of mana used to pay its cost.)\nRemove a +1/+1 counter from Lunar Avenger: Lunar Avenger gains your choice of flying, first strike, or haste until end of turn.').
card_first_print('lunar avenger', '5DN').
card_image_name('lunar avenger'/'5DN', 'lunar avenger').
card_uid('lunar avenger'/'5DN', '5DN:Lunar Avenger:lunar avenger').
card_rarity('lunar avenger'/'5DN', 'Uncommon').
card_artist('lunar avenger'/'5DN', 'Paolo Parente').
card_number('lunar avenger'/'5DN', '136').
card_multiverse_id('lunar avenger'/'5DN', '72862').

card_in_set('magma giant', '5DN').
card_original_type('magma giant'/'5DN', 'Creature — Giant').
card_original_text('magma giant'/'5DN', 'When Magma Giant comes into play, it deals 2 damage to each creature and each player.').
card_image_name('magma giant'/'5DN', 'magma giant').
card_uid('magma giant'/'5DN', '5DN:Magma Giant:magma giant').
card_rarity('magma giant'/'5DN', 'Rare').
card_artist('magma giant'/'5DN', 'Nottsuo').
card_number('magma giant'/'5DN', '72').
card_flavor_text('magma giant'/'5DN', 'Its open mouth is a furnace blast that blisters the skin. Its smoking fists are meteors that split the earth.').
card_multiverse_id('magma giant'/'5DN', '51102').

card_in_set('magma jet', '5DN').
card_original_type('magma jet'/'5DN', 'Instant').
card_original_text('magma jet'/'5DN', 'Magma Jet deals 2 damage to target creature or player.\nScry 2 (Look at the top two cards of your library. Put any number of them on the bottom of your library and the rest on top in any order.)').
card_image_name('magma jet'/'5DN', 'magma jet').
card_uid('magma jet'/'5DN', '5DN:Magma Jet:magma jet').
card_rarity('magma jet'/'5DN', 'Uncommon').
card_artist('magma jet'/'5DN', 'Justin Sweet').
card_number('magma jet'/'5DN', '73').
card_multiverse_id('magma jet'/'5DN', '51180').

card_in_set('magnetic theft', '5DN').
card_original_type('magnetic theft'/'5DN', 'Instant').
card_original_text('magnetic theft'/'5DN', 'Attach target Equipment to target creature. (Control of the Equipment doesn\'t change.)').
card_first_print('magnetic theft', '5DN').
card_image_name('magnetic theft'/'5DN', 'magnetic theft').
card_uid('magnetic theft'/'5DN', '5DN:Magnetic Theft:magnetic theft').
card_rarity('magnetic theft'/'5DN', 'Uncommon').
card_artist('magnetic theft'/'5DN', 'Dave Dorman').
card_number('magnetic theft'/'5DN', '74').
card_flavor_text('magnetic theft'/'5DN', 'Among the Vulshok, the most prized blades are those that killed their original owners.').
card_multiverse_id('magnetic theft'/'5DN', '51101').

card_in_set('mana geyser', '5DN').
card_original_type('mana geyser'/'5DN', 'Sorcery').
card_original_text('mana geyser'/'5DN', 'Add {R} to your mana pool for each tapped land your opponents control.').
card_first_print('mana geyser', '5DN').
card_image_name('mana geyser'/'5DN', 'mana geyser').
card_uid('mana geyser'/'5DN', '5DN:Mana Geyser:mana geyser').
card_rarity('mana geyser'/'5DN', 'Common').
card_artist('mana geyser'/'5DN', 'Martina Pilcerova').
card_number('mana geyser'/'5DN', '75').
card_flavor_text('mana geyser'/'5DN', 'The Quicksilver Sea hissed and bubbled at the indignity. The Vulshok shaman just smiled.').
card_multiverse_id('mana geyser'/'5DN', '72727').

card_in_set('mephidross vampire', '5DN').
card_original_type('mephidross vampire'/'5DN', 'Creature — Vampire').
card_original_text('mephidross vampire'/'5DN', 'Flying\nEach creature you control is a Vampire in addition to its other creature types and has \"Whenever this creature deals damage to a creature, put a +1/+1 counter on this creature.\"').
card_first_print('mephidross vampire', '5DN').
card_image_name('mephidross vampire'/'5DN', 'mephidross vampire').
card_uid('mephidross vampire'/'5DN', '5DN:Mephidross Vampire:mephidross vampire').
card_rarity('mephidross vampire'/'5DN', 'Rare').
card_artist('mephidross vampire'/'5DN', 'Matthew D. Wilson').
card_number('mephidross vampire'/'5DN', '53').
card_multiverse_id('mephidross vampire'/'5DN', '50211').

card_in_set('moriok rigger', '5DN').
card_original_type('moriok rigger'/'5DN', 'Creature — Human Rogue').
card_original_text('moriok rigger'/'5DN', 'Whenever an artifact is put into a graveyard from play, you may put a +1/+1 counter on Moriok Rigger.').
card_first_print('moriok rigger', '5DN').
card_image_name('moriok rigger'/'5DN', 'moriok rigger').
card_uid('moriok rigger'/'5DN', '5DN:Moriok Rigger:moriok rigger').
card_rarity('moriok rigger'/'5DN', 'Rare').
card_artist('moriok rigger'/'5DN', 'Wayne England').
card_number('moriok rigger'/'5DN', '54').
card_flavor_text('moriok rigger'/'5DN', 'The Moriok scavenge for weapons, for armor, for food—and ultimately for their souls.').
card_multiverse_id('moriok rigger'/'5DN', '51091').

card_in_set('mycosynth golem', '5DN').
card_original_type('mycosynth golem'/'5DN', 'Artifact Creature — Golem').
card_original_text('mycosynth golem'/'5DN', 'Affinity for artifacts (This spell costs {1} less to play for each artifact you control.)\nArtifact creature spells you play have affinity for artifacts. (They cost {1} less to play for each artifact you control.)').
card_first_print('mycosynth golem', '5DN').
card_image_name('mycosynth golem'/'5DN', 'mycosynth golem').
card_uid('mycosynth golem'/'5DN', '5DN:Mycosynth Golem:mycosynth golem').
card_rarity('mycosynth golem'/'5DN', 'Rare').
card_artist('mycosynth golem'/'5DN', 'Paolo Parente').
card_number('mycosynth golem'/'5DN', '137').
card_multiverse_id('mycosynth golem'/'5DN', '51136').

card_in_set('myr quadropod', '5DN').
card_original_type('myr quadropod'/'5DN', 'Artifact Creature — Myr').
card_original_text('myr quadropod'/'5DN', '{3}: Switch Myr Quadropod\'s power and toughness until end of turn.').
card_first_print('myr quadropod', '5DN').
card_image_name('myr quadropod'/'5DN', 'myr quadropod').
card_uid('myr quadropod'/'5DN', '5DN:Myr Quadropod:myr quadropod').
card_rarity('myr quadropod'/'5DN', 'Common').
card_artist('myr quadropod'/'5DN', 'Christopher Rush').
card_number('myr quadropod'/'5DN', '138').
card_flavor_text('myr quadropod'/'5DN', 'Each myr was designed to fill a specific role. This one, for example, was designed to stomp on other myr.').
card_multiverse_id('myr quadropod'/'5DN', '50176').

card_in_set('myr servitor', '5DN').
card_original_type('myr servitor'/'5DN', 'Artifact Creature — Myr').
card_original_text('myr servitor'/'5DN', 'At the beginning of your upkeep, if Myr Servitor is in play, each player returns all cards named Myr Servitor from his or her graveyard to play.').
card_first_print('myr servitor', '5DN').
card_image_name('myr servitor'/'5DN', 'myr servitor').
card_uid('myr servitor'/'5DN', '5DN:Myr Servitor:myr servitor').
card_rarity('myr servitor'/'5DN', 'Common').
card_artist('myr servitor'/'5DN', 'John Matson').
card_number('myr servitor'/'5DN', '139').
card_flavor_text('myr servitor'/'5DN', 'The Krark Clan enjoys pulling them apart just to watch them reassemble one another.').
card_multiverse_id('myr servitor'/'5DN', '50179').

card_in_set('neurok stealthsuit', '5DN').
card_original_type('neurok stealthsuit'/'5DN', 'Artifact — Equipment').
card_original_text('neurok stealthsuit'/'5DN', 'Equipped creature can\'t be the target of spells or abilities.\n{U}{U}: Attach Neurok Stealthsuit to target creature you control.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('neurok stealthsuit', '5DN').
card_image_name('neurok stealthsuit'/'5DN', 'neurok stealthsuit').
card_uid('neurok stealthsuit'/'5DN', '5DN:Neurok Stealthsuit:neurok stealthsuit').
card_rarity('neurok stealthsuit'/'5DN', 'Common').
card_artist('neurok stealthsuit'/'5DN', 'Francis Tsai').
card_number('neurok stealthsuit'/'5DN', '140').
card_multiverse_id('neurok stealthsuit'/'5DN', '51183').

card_in_set('night\'s whisper', '5DN').
card_original_type('night\'s whisper'/'5DN', 'Sorcery').
card_original_text('night\'s whisper'/'5DN', 'You draw two cards and you lose 2 life.').
card_first_print('night\'s whisper', '5DN').
card_image_name('night\'s whisper'/'5DN', 'night\'s whisper').
card_uid('night\'s whisper'/'5DN', '5DN:Night\'s Whisper:night\'s whisper').
card_rarity('night\'s whisper'/'5DN', 'Uncommon').
card_artist('night\'s whisper'/'5DN', 'David Martin').
card_number('night\'s whisper'/'5DN', '55').
card_flavor_text('night\'s whisper'/'5DN', 'The Moriok call the black moon the Whisperer. At her apex, she whispers of power.').
card_multiverse_id('night\'s whisper'/'5DN', '51178').

card_in_set('nim grotesque', '5DN').
card_original_type('nim grotesque'/'5DN', 'Creature — Zombie').
card_original_text('nim grotesque'/'5DN', 'Nim Grotesque gets +1/+0 for each artifact you control.').
card_first_print('nim grotesque', '5DN').
card_image_name('nim grotesque'/'5DN', 'nim grotesque').
card_uid('nim grotesque'/'5DN', '5DN:Nim Grotesque:nim grotesque').
card_rarity('nim grotesque'/'5DN', 'Uncommon').
card_artist('nim grotesque'/'5DN', 'Anthony S. Waters').
card_number('nim grotesque'/'5DN', '56').
card_flavor_text('nim grotesque'/'5DN', 'In the swirling mists, you could walk right up to one and not know until its teeth are in your throat.').
card_multiverse_id('nim grotesque'/'5DN', '73574').

card_in_set('opaline bracers', '5DN').
card_original_type('opaline bracers'/'5DN', 'Artifact — Equipment').
card_original_text('opaline bracers'/'5DN', 'Sunburst (This comes into play with a charge counter on it for each color of mana used to pay its cost.)\nEquipped creature gets +X/+X, where X is the number of charge counters on Opaline Bracers.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('opaline bracers', '5DN').
card_image_name('opaline bracers'/'5DN', 'opaline bracers').
card_uid('opaline bracers'/'5DN', '5DN:Opaline Bracers:opaline bracers').
card_rarity('opaline bracers'/'5DN', 'Common').
card_artist('opaline bracers'/'5DN', 'Anthony S. Waters').
card_number('opaline bracers'/'5DN', '141').
card_multiverse_id('opaline bracers'/'5DN', '50158').

card_in_set('ouphe vandals', '5DN').
card_original_type('ouphe vandals'/'5DN', 'Creature — Ouphe').
card_original_text('ouphe vandals'/'5DN', '{G}, Sacrifice Ouphe Vandals: Counter target activated ability from an artifact source and destroy that artifact if it\'s in play. (Mana abilities can\'t be targeted.)').
card_first_print('ouphe vandals', '5DN').
card_image_name('ouphe vandals'/'5DN', 'ouphe vandals').
card_uid('ouphe vandals'/'5DN', '5DN:Ouphe Vandals:ouphe vandals').
card_rarity('ouphe vandals'/'5DN', 'Uncommon').
card_artist('ouphe vandals'/'5DN', 'Thomas M. Baxa').
card_number('ouphe vandals'/'5DN', '90').
card_flavor_text('ouphe vandals'/'5DN', 'The ouphes finally managed to still the clockwork monstrosity by catching themselves in its cogs.').
card_multiverse_id('ouphe vandals'/'5DN', '50189').

card_in_set('paradise mantle', '5DN').
card_original_type('paradise mantle'/'5DN', 'Artifact — Equipment').
card_original_text('paradise mantle'/'5DN', 'Equipped creature has \"{T}: Add one mana of any color to your mana pool.\"\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('paradise mantle', '5DN').
card_image_name('paradise mantle'/'5DN', 'paradise mantle').
card_uid('paradise mantle'/'5DN', '5DN:Paradise Mantle:paradise mantle').
card_rarity('paradise mantle'/'5DN', 'Uncommon').
card_artist('paradise mantle'/'5DN', 'Greg Hildebrandt').
card_number('paradise mantle'/'5DN', '142').
card_multiverse_id('paradise mantle'/'5DN', '73558').

card_in_set('pentad prism', '5DN').
card_original_type('pentad prism'/'5DN', 'Artifact').
card_original_text('pentad prism'/'5DN', 'Sunburst (This comes into play with a charge counter on it for each color of mana used to pay its cost.)\nRemove a charge counter from Pentad Prism: Add one mana of any color to your mana pool.').
card_first_print('pentad prism', '5DN').
card_image_name('pentad prism'/'5DN', 'pentad prism').
card_uid('pentad prism'/'5DN', '5DN:Pentad Prism:pentad prism').
card_rarity('pentad prism'/'5DN', 'Common').
card_artist('pentad prism'/'5DN', 'David Martin').
card_number('pentad prism'/'5DN', '143').
card_multiverse_id('pentad prism'/'5DN', '72860').

card_in_set('plasma elemental', '5DN').
card_original_type('plasma elemental'/'5DN', 'Creature — Elemental').
card_original_text('plasma elemental'/'5DN', 'Plasma Elemental is unblockable.').
card_first_print('plasma elemental', '5DN').
card_image_name('plasma elemental'/'5DN', 'plasma elemental').
card_uid('plasma elemental'/'5DN', '5DN:Plasma Elemental:plasma elemental').
card_rarity('plasma elemental'/'5DN', 'Uncommon').
card_artist('plasma elemental'/'5DN', 'Kev Walker').
card_number('plasma elemental'/'5DN', '34').
card_flavor_text('plasma elemental'/'5DN', '\"I watched a ship sail right through it. I thought it was harmless . . . until I saw what it had done to the crew.\"\n—Bruenna, Neurok leader').
card_multiverse_id('plasma elemental'/'5DN', '73562').

card_in_set('plunge into darkness', '5DN').
card_original_type('plunge into darkness'/'5DN', 'Instant').
card_original_text('plunge into darkness'/'5DN', 'Choose one Sacrifice any number of creatures, then you gain 3 life for each sacrificed creature; or pay X life, then look at the top X cards of your library, put one of those cards into your hand, and remove the rest from the game.\nEntwine {B} (Choose both if you pay the entwine cost.)').
card_first_print('plunge into darkness', '5DN').
card_image_name('plunge into darkness'/'5DN', 'plunge into darkness').
card_uid('plunge into darkness'/'5DN', '5DN:Plunge into Darkness:plunge into darkness').
card_rarity('plunge into darkness'/'5DN', 'Rare').
card_artist('plunge into darkness'/'5DN', 'Justin Sweet').
card_number('plunge into darkness'/'5DN', '57').
card_multiverse_id('plunge into darkness'/'5DN', '72681').

card_in_set('possessed portal', '5DN').
card_original_type('possessed portal'/'5DN', 'Artifact').
card_original_text('possessed portal'/'5DN', 'If a player would draw a card, that player skips that draw instead.\nAt the end of each turn, each player sacrifices a permanent unless he or she discards a card from his or her hand.').
card_first_print('possessed portal', '5DN').
card_image_name('possessed portal'/'5DN', 'possessed portal').
card_uid('possessed portal'/'5DN', '5DN:Possessed Portal:possessed portal').
card_rarity('possessed portal'/'5DN', 'Rare').
card_artist('possessed portal'/'5DN', 'Tony Szczudlo').
card_number('possessed portal'/'5DN', '144').
card_multiverse_id('possessed portal'/'5DN', '50119').

card_in_set('qumulox', '5DN').
card_original_type('qumulox'/'5DN', 'Creature — Beast').
card_original_text('qumulox'/'5DN', 'Affinity for artifacts (This spell costs {1} less to play for each artifact you control.)\nFlying').
card_first_print('qumulox', '5DN').
card_image_name('qumulox'/'5DN', 'qumulox').
card_uid('qumulox'/'5DN', '5DN:Qumulox:qumulox').
card_rarity('qumulox'/'5DN', 'Uncommon').
card_artist('qumulox'/'5DN', 'Carl Critchlow').
card_number('qumulox'/'5DN', '35').
card_flavor_text('qumulox'/'5DN', 'Even the clouds bend themselves to Memnarch\'s will, eager to swallow those who oppose him.').
card_multiverse_id('qumulox'/'5DN', '47800').

card_in_set('rain of rust', '5DN').
card_original_type('rain of rust'/'5DN', 'Instant').
card_original_text('rain of rust'/'5DN', 'Choose one Destroy target artifact; or destroy target land.\nEntwine {3}{R} (Choose both if you pay the entwine cost.)').
card_first_print('rain of rust', '5DN').
card_image_name('rain of rust'/'5DN', 'rain of rust').
card_uid('rain of rust'/'5DN', '5DN:Rain of Rust:rain of rust').
card_rarity('rain of rust'/'5DN', 'Common').
card_artist('rain of rust'/'5DN', 'Tim Hildebrandt').
card_number('rain of rust'/'5DN', '76').
card_flavor_text('rain of rust'/'5DN', 'On Mirrodin, mages who call upon the power of rust are shunned by their people and often turn mercenary to survive.').
card_multiverse_id('rain of rust'/'5DN', '51190').

card_in_set('raksha golden cub', '5DN').
card_original_type('raksha golden cub'/'5DN', 'Creature — Cat Soldier Legend').
card_original_text('raksha golden cub'/'5DN', 'Attacking doesn\'t cause Raksha Golden Cub to tap.\nAs long as Raksha is equipped, Cats you control get +2/+2 and have double strike.').
card_first_print('raksha golden cub', '5DN').
card_image_name('raksha golden cub'/'5DN', 'raksha golden cub').
card_uid('raksha golden cub'/'5DN', '5DN:Raksha Golden Cub:raksha golden cub').
card_rarity('raksha golden cub'/'5DN', 'Rare').
card_artist('raksha golden cub'/'5DN', 'Pete Venters').
card_number('raksha golden cub'/'5DN', '12').
card_flavor_text('raksha golden cub'/'5DN', 'Some believe that Raksha, youngest of the kha, is the reincarnation of Dakan, the first and mightiest of leonin leaders.').
card_multiverse_id('raksha golden cub'/'5DN', '73569').

card_in_set('razorgrass screen', '5DN').
card_original_type('razorgrass screen'/'5DN', 'Artifact Creature — Wall').
card_original_text('razorgrass screen'/'5DN', '(Walls can\'t attack.)\nRazorgrass Screen blocks each turn if able.').
card_first_print('razorgrass screen', '5DN').
card_image_name('razorgrass screen'/'5DN', 'razorgrass screen').
card_uid('razorgrass screen'/'5DN', '5DN:Razorgrass Screen:razorgrass screen').
card_rarity('razorgrass screen'/'5DN', 'Common').
card_artist('razorgrass screen'/'5DN', 'Tony Szczudlo').
card_number('razorgrass screen'/'5DN', '145').
card_flavor_text('razorgrass screen'/'5DN', 'Hand-stitched, and marked by the blood of the Auriok.').
card_multiverse_id('razorgrass screen'/'5DN', '72999').

card_in_set('razormane masticore', '5DN').
card_original_type('razormane masticore'/'5DN', 'Artifact Creature').
card_original_text('razormane masticore'/'5DN', 'First strike\nAt the beginning of your upkeep, sacrifice Razormane Masticore unless you discard a card from your hand.\nAt the beginning of your draw step, you may have Razormane Masticore deal 3 damage to target creature.').
card_first_print('razormane masticore', '5DN').
card_image_name('razormane masticore'/'5DN', 'razormane masticore').
card_uid('razormane masticore'/'5DN', '5DN:Razormane Masticore:razormane masticore').
card_rarity('razormane masticore'/'5DN', 'Rare').
card_artist('razormane masticore'/'5DN', 'Jim Murray').
card_number('razormane masticore'/'5DN', '146').
card_multiverse_id('razormane masticore'/'5DN', '50155').

card_in_set('relentless rats', '5DN').
card_original_type('relentless rats'/'5DN', 'Creature — Rat').
card_original_text('relentless rats'/'5DN', 'Relentless Rats gets +1/+1 for each other creature in play named Relentless Rats.\nA deck can have any number of cards named Relentless Rats.').
card_first_print('relentless rats', '5DN').
card_image_name('relentless rats'/'5DN', 'relentless rats').
card_uid('relentless rats'/'5DN', '5DN:Relentless Rats:relentless rats').
card_rarity('relentless rats'/'5DN', 'Uncommon').
card_artist('relentless rats'/'5DN', 'Thomas M. Baxa').
card_number('relentless rats'/'5DN', '58').
card_multiverse_id('relentless rats'/'5DN', '73573').

card_in_set('relic barrier', '5DN').
card_original_type('relic barrier'/'5DN', 'Artifact').
card_original_text('relic barrier'/'5DN', '{T}: Tap target artifact.').
card_image_name('relic barrier'/'5DN', 'relic barrier').
card_uid('relic barrier'/'5DN', '5DN:Relic Barrier:relic barrier').
card_rarity('relic barrier'/'5DN', 'Uncommon').
card_artist('relic barrier'/'5DN', 'Nottsuo').
card_number('relic barrier'/'5DN', '147').
card_flavor_text('relic barrier'/'5DN', 'Its transmissions are the word of Memnarch. It speaks, and metal bows.').
card_multiverse_id('relic barrier'/'5DN', '43574').

card_in_set('retaliate', '5DN').
card_original_type('retaliate'/'5DN', 'Instant').
card_original_text('retaliate'/'5DN', 'Destroy all creatures that dealt damage to you this turn.').
card_first_print('retaliate', '5DN').
card_image_name('retaliate'/'5DN', 'retaliate').
card_uid('retaliate'/'5DN', '5DN:Retaliate:retaliate').
card_rarity('retaliate'/'5DN', 'Rare').
card_artist('retaliate'/'5DN', 'Vance Kovacs').
card_number('retaliate'/'5DN', '13').
card_flavor_text('retaliate'/'5DN', 'The Auriok learned perseverance from their forebears. They learned justice from the leonin.').
card_multiverse_id('retaliate'/'5DN', '50213').

card_in_set('reversal of fortune', '5DN').
card_original_type('reversal of fortune'/'5DN', 'Sorcery').
card_original_text('reversal of fortune'/'5DN', 'Target opponent reveals his or her hand. You may copy an instant or sorcery card in it and play the copy without paying its mana cost.').
card_first_print('reversal of fortune', '5DN').
card_image_name('reversal of fortune'/'5DN', 'reversal of fortune').
card_uid('reversal of fortune'/'5DN', '5DN:Reversal of Fortune:reversal of fortune').
card_rarity('reversal of fortune'/'5DN', 'Rare').
card_artist('reversal of fortune'/'5DN', 'Greg Hildebrandt').
card_number('reversal of fortune'/'5DN', '77').
card_flavor_text('reversal of fortune'/'5DN', '\"Why learn hundreds of spells when you can learn only one and use it to steal hundreds?\"').
card_multiverse_id('reversal of fortune'/'5DN', '73564').

card_in_set('rite of passage', '5DN').
card_original_type('rite of passage'/'5DN', 'Enchantment').
card_original_text('rite of passage'/'5DN', 'Whenever a creature you control is dealt damage, put a +1/+1 counter on it. (The damage is dealt before the counter is put on.)').
card_first_print('rite of passage', '5DN').
card_image_name('rite of passage'/'5DN', 'rite of passage').
card_uid('rite of passage'/'5DN', '5DN:Rite of Passage:rite of passage').
card_rarity('rite of passage'/'5DN', 'Rare').
card_artist('rite of passage'/'5DN', 'Kev Walker').
card_number('rite of passage'/'5DN', '91').
card_flavor_text('rite of passage'/'5DN', 'Every scar is a lesson, every battle a test of what you have learned.').
card_multiverse_id('rite of passage'/'5DN', '51163').

card_in_set('roar of reclamation', '5DN').
card_original_type('roar of reclamation'/'5DN', 'Sorcery').
card_original_text('roar of reclamation'/'5DN', 'Each player returns all artifact cards from his or her graveyard to play.').
card_first_print('roar of reclamation', '5DN').
card_image_name('roar of reclamation'/'5DN', 'roar of reclamation').
card_uid('roar of reclamation'/'5DN', '5DN:Roar of Reclamation:roar of reclamation').
card_rarity('roar of reclamation'/'5DN', 'Rare').
card_artist('roar of reclamation'/'5DN', 'Justin Sweet').
card_number('roar of reclamation'/'5DN', '14').
card_flavor_text('roar of reclamation'/'5DN', '\"Yesterday\'s relics will determine the outcome of tomorrow\'s war.\"\n—Ushanti, leonin seer').
card_multiverse_id('roar of reclamation'/'5DN', '51080').

card_in_set('rude awakening', '5DN').
card_original_type('rude awakening'/'5DN', 'Sorcery').
card_original_text('rude awakening'/'5DN', 'Choose one Untap all lands you control; or until end of turn, lands you control become 2/2 creatures that are still lands.\nEntwine {2}{G} (Choose both if you pay the entwine cost.)').
card_first_print('rude awakening', '5DN').
card_image_name('rude awakening'/'5DN', 'rude awakening').
card_uid('rude awakening'/'5DN', '5DN:Rude Awakening:rude awakening').
card_rarity('rude awakening'/'5DN', 'Rare').
card_artist('rude awakening'/'5DN', 'Ittoku').
card_number('rude awakening'/'5DN', '92').
card_multiverse_id('rude awakening'/'5DN', '73587').

card_in_set('salvaging station', '5DN').
card_original_type('salvaging station'/'5DN', 'Artifact').
card_original_text('salvaging station'/'5DN', '{T}: Return target noncreature artifact card with converted mana cost 1 or less from your graveyard to play.\nWhenever a creature is put into a graveyard from play, you may untap Salvaging Station.').
card_first_print('salvaging station', '5DN').
card_image_name('salvaging station'/'5DN', 'salvaging station').
card_uid('salvaging station'/'5DN', '5DN:Salvaging Station:salvaging station').
card_rarity('salvaging station'/'5DN', 'Rare').
card_artist('salvaging station'/'5DN', 'Greg Staples').
card_number('salvaging station'/'5DN', '148').
card_multiverse_id('salvaging station'/'5DN', '51132').

card_in_set('sawtooth thresher', '5DN').
card_original_type('sawtooth thresher'/'5DN', 'Artifact Creature').
card_original_text('sawtooth thresher'/'5DN', 'Sunburst (This comes into play with a +1/+1 counter on it for each color of mana used to pay its cost.)\nRemove two +1/+1 counters from Sawtooth Thresher: Sawtooth Thresher gets +4/+4 until end of turn.').
card_first_print('sawtooth thresher', '5DN').
card_image_name('sawtooth thresher'/'5DN', 'sawtooth thresher').
card_uid('sawtooth thresher'/'5DN', '5DN:Sawtooth Thresher:sawtooth thresher').
card_rarity('sawtooth thresher'/'5DN', 'Common').
card_artist('sawtooth thresher'/'5DN', 'Alan Pollack').
card_number('sawtooth thresher'/'5DN', '149').
card_multiverse_id('sawtooth thresher'/'5DN', '51141').

card_in_set('screaming fury', '5DN').
card_original_type('screaming fury'/'5DN', 'Sorcery').
card_original_text('screaming fury'/'5DN', 'Target creature gets +5/+0 and gains haste until end of turn.').
card_first_print('screaming fury', '5DN').
card_image_name('screaming fury'/'5DN', 'screaming fury').
card_uid('screaming fury'/'5DN', '5DN:Screaming Fury:screaming fury').
card_rarity('screaming fury'/'5DN', 'Common').
card_artist('screaming fury'/'5DN', 'Edward P. Beard, Jr.').
card_number('screaming fury'/'5DN', '78').
card_flavor_text('screaming fury'/'5DN', 'He\'s got an axe to grind.').
card_multiverse_id('screaming fury'/'5DN', '51148').

card_in_set('serum visions', '5DN').
card_original_type('serum visions'/'5DN', 'Sorcery').
card_original_text('serum visions'/'5DN', 'Draw a card.\nScry 2 (Look at the top two cards of your library. Put any number of them on the bottom of your library and the rest on top in any order.)').
card_image_name('serum visions'/'5DN', 'serum visions').
card_uid('serum visions'/'5DN', '5DN:Serum Visions:serum visions').
card_rarity('serum visions'/'5DN', 'Common').
card_artist('serum visions'/'5DN', 'Ben Thompson').
card_number('serum visions'/'5DN', '36').
card_multiverse_id('serum visions'/'5DN', '50145').

card_in_set('shattered dreams', '5DN').
card_original_type('shattered dreams'/'5DN', 'Sorcery').
card_original_text('shattered dreams'/'5DN', 'Target opponent reveals his or her hand. Choose an artifact card from it. That player discards that card.').
card_first_print('shattered dreams', '5DN').
card_image_name('shattered dreams'/'5DN', 'shattered dreams').
card_uid('shattered dreams'/'5DN', '5DN:Shattered Dreams:shattered dreams').
card_rarity('shattered dreams'/'5DN', 'Uncommon').
card_artist('shattered dreams'/'5DN', 'Greg Staples').
card_number('shattered dreams'/'5DN', '59').
card_flavor_text('shattered dreams'/'5DN', 'Victims are lucky if they remember ever having had what was lost.').
card_multiverse_id('shattered dreams'/'5DN', '51092').

card_in_set('silent arbiter', '5DN').
card_original_type('silent arbiter'/'5DN', 'Artifact Creature').
card_original_text('silent arbiter'/'5DN', 'No more than one creature may attack each combat.\nNo more than one creature may block each combat.').
card_first_print('silent arbiter', '5DN').
card_image_name('silent arbiter'/'5DN', 'silent arbiter').
card_uid('silent arbiter'/'5DN', '5DN:Silent Arbiter:silent arbiter').
card_rarity('silent arbiter'/'5DN', 'Rare').
card_artist('silent arbiter'/'5DN', 'Mark Zug').
card_number('silent arbiter'/'5DN', '150').
card_flavor_text('silent arbiter'/'5DN', 'Scholars say that arbiters exist on every world, created by an unknown hand to enforce justice.').
card_multiverse_id('silent arbiter'/'5DN', '73598').

card_in_set('skullcage', '5DN').
card_original_type('skullcage'/'5DN', 'Artifact').
card_original_text('skullcage'/'5DN', 'At the beginning of each opponent\'s upkeep, Skullcage deals 2 damage to that player unless he or she has exactly three or exactly four cards in hand.').
card_first_print('skullcage', '5DN').
card_image_name('skullcage'/'5DN', 'skullcage').
card_uid('skullcage'/'5DN', '5DN:Skullcage:skullcage').
card_rarity('skullcage'/'5DN', 'Uncommon').
card_artist('skullcage'/'5DN', 'Ron Spencer').
card_number('skullcage'/'5DN', '151').
card_flavor_text('skullcage'/'5DN', 'Only a focused mind can survive it.').
card_multiverse_id('skullcage'/'5DN', '73926').

card_in_set('skyhunter prowler', '5DN').
card_original_type('skyhunter prowler'/'5DN', 'Creature — Cat Knight').
card_original_text('skyhunter prowler'/'5DN', 'Flying\nAttacking doesn\'t cause Skyhunter Prowler to tap.').
card_first_print('skyhunter prowler', '5DN').
card_image_name('skyhunter prowler'/'5DN', 'skyhunter prowler').
card_uid('skyhunter prowler'/'5DN', '5DN:Skyhunter Prowler:skyhunter prowler').
card_rarity('skyhunter prowler'/'5DN', 'Common').
card_artist('skyhunter prowler'/'5DN', 'Vance Kovacs').
card_number('skyhunter prowler'/'5DN', '15').
card_flavor_text('skyhunter prowler'/'5DN', '\"If what I have seen comes to pass, we will need our guardians more than ever.\"\n—Ushanti, leonin seer').
card_multiverse_id('skyhunter prowler'/'5DN', '50136').

card_in_set('skyhunter skirmisher', '5DN').
card_original_type('skyhunter skirmisher'/'5DN', 'Creature — Cat Knight').
card_original_text('skyhunter skirmisher'/'5DN', 'Flying, double strike').
card_first_print('skyhunter skirmisher', '5DN').
card_image_name('skyhunter skirmisher'/'5DN', 'skyhunter skirmisher').
card_uid('skyhunter skirmisher'/'5DN', '5DN:Skyhunter Skirmisher:skyhunter skirmisher').
card_rarity('skyhunter skirmisher'/'5DN', 'Uncommon').
card_artist('skyhunter skirmisher'/'5DN', 'Greg Staples').
card_number('skyhunter skirmisher'/'5DN', '16').
card_flavor_text('skyhunter skirmisher'/'5DN', '\"Like dawn\'s first light, blind the unprepared and banish the shadows.\"\n—Skyhunter creed').
card_multiverse_id('skyhunter skirmisher'/'5DN', '51220').

card_in_set('skyreach manta', '5DN').
card_original_type('skyreach manta'/'5DN', 'Artifact Creature').
card_original_text('skyreach manta'/'5DN', 'Sunburst (This comes into play with a +1/+1 counter on it for each color of mana used to pay its cost.)\nFlying').
card_first_print('skyreach manta', '5DN').
card_image_name('skyreach manta'/'5DN', 'skyreach manta').
card_uid('skyreach manta'/'5DN', '5DN:Skyreach Manta:skyreach manta').
card_rarity('skyreach manta'/'5DN', 'Common').
card_artist('skyreach manta'/'5DN', 'Christopher Moeller').
card_number('skyreach manta'/'5DN', '152').
card_flavor_text('skyreach manta'/'5DN', 'As the dawns break, the manta soars.').
card_multiverse_id('skyreach manta'/'5DN', '51374').

card_in_set('solarion', '5DN').
card_original_type('solarion'/'5DN', 'Artifact Creature').
card_original_text('solarion'/'5DN', 'Sunburst (This comes into play with a +1/+1 counter on it for each color of mana used to pay its cost.)\n{T}: Double the number of +1/+1 counters on Solarion.').
card_first_print('solarion', '5DN').
card_image_name('solarion'/'5DN', 'solarion').
card_uid('solarion'/'5DN', '5DN:Solarion:solarion').
card_rarity('solarion'/'5DN', 'Rare').
card_artist('solarion'/'5DN', 'Jim Murray').
card_number('solarion'/'5DN', '153').
card_multiverse_id('solarion'/'5DN', '51629').

card_in_set('spark elemental', '5DN').
card_original_type('spark elemental'/'5DN', 'Creature — Elemental').
card_original_text('spark elemental'/'5DN', 'Trample, haste\nAt end of turn, sacrifice Spark Elemental.').
card_first_print('spark elemental', '5DN').
card_image_name('spark elemental'/'5DN', 'spark elemental').
card_uid('spark elemental'/'5DN', '5DN:Spark Elemental:spark elemental').
card_rarity('spark elemental'/'5DN', 'Common').
card_artist('spark elemental'/'5DN', 'John Avon').
card_number('spark elemental'/'5DN', '79').
card_flavor_text('spark elemental'/'5DN', 'Vulshok shamans summon spark elementals to burn paths through thick underbrush—or through goblin salvage squads.').
card_multiverse_id('spark elemental'/'5DN', '73579').

card_in_set('sparring collar', '5DN').
card_original_type('sparring collar'/'5DN', 'Artifact — Equipment').
card_original_text('sparring collar'/'5DN', 'Equipped creature has first strike.\n{R}{R}: Attach Sparring Collar to target creature you control.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('sparring collar', '5DN').
card_image_name('sparring collar'/'5DN', 'sparring collar').
card_uid('sparring collar'/'5DN', '5DN:Sparring Collar:sparring collar').
card_rarity('sparring collar'/'5DN', 'Common').
card_artist('sparring collar'/'5DN', 'Christopher Moeller').
card_number('sparring collar'/'5DN', '154').
card_multiverse_id('sparring collar'/'5DN', '51185').

card_in_set('spectral shift', '5DN').
card_original_type('spectral shift'/'5DN', 'Instant').
card_original_text('spectral shift'/'5DN', 'Choose one Change the text of target spell or permanent by replacing all instances of one basic land type with another; or change the text of target spell or permanent by replacing all instances of one color word with another. (These effects don\'t end at end of turn.)\nEntwine {2}').
card_first_print('spectral shift', '5DN').
card_image_name('spectral shift'/'5DN', 'spectral shift').
card_uid('spectral shift'/'5DN', '5DN:Spectral Shift:spectral shift').
card_rarity('spectral shift'/'5DN', 'Rare').
card_artist('spectral shift'/'5DN', 'John Avon').
card_number('spectral shift'/'5DN', '37').
card_multiverse_id('spectral shift'/'5DN', '73572').

card_in_set('spinal parasite', '5DN').
card_original_type('spinal parasite'/'5DN', 'Artifact Creature — Insect').
card_original_text('spinal parasite'/'5DN', 'Sunburst (This comes into play with a +1/+1 counter on it for each color of mana used to pay its cost.)\nRemove two +1/+1 counters from Spinal Parasite: Remove a counter from target permanent.').
card_first_print('spinal parasite', '5DN').
card_image_name('spinal parasite'/'5DN', 'spinal parasite').
card_uid('spinal parasite'/'5DN', '5DN:Spinal Parasite:spinal parasite').
card_rarity('spinal parasite'/'5DN', 'Uncommon').
card_artist('spinal parasite'/'5DN', 'Cyril Van Der Haegen').
card_number('spinal parasite'/'5DN', '155').
card_multiverse_id('spinal parasite'/'5DN', '73928').

card_in_set('staff of domination', '5DN').
card_original_type('staff of domination'/'5DN', 'Artifact').
card_original_text('staff of domination'/'5DN', '{1}: Untap Staff of Domination.\n{2}, {T}: You gain 1 life.\n{3}, {T}: Untap target creature.\n{4}, {T}: Tap target creature.\n{5}, {T}: Draw a card.').
card_first_print('staff of domination', '5DN').
card_image_name('staff of domination'/'5DN', 'staff of domination').
card_uid('staff of domination'/'5DN', '5DN:Staff of Domination:staff of domination').
card_rarity('staff of domination'/'5DN', 'Rare').
card_artist('staff of domination'/'5DN', 'Ben Thompson').
card_number('staff of domination'/'5DN', '156').
card_multiverse_id('staff of domination'/'5DN', '50162').

card_in_set('stand firm', '5DN').
card_original_type('stand firm'/'5DN', 'Instant').
card_original_text('stand firm'/'5DN', 'Target creature gets +1/+1 until end of turn.\nScry 2 (Look at the top two cards of your library. Put any number of them on the bottom of your library and the rest on top in any order.)').
card_first_print('stand firm', '5DN').
card_image_name('stand firm'/'5DN', 'stand firm').
card_uid('stand firm'/'5DN', '5DN:Stand Firm:stand firm').
card_rarity('stand firm'/'5DN', 'Common').
card_artist('stand firm'/'5DN', 'Matt Cavotta').
card_number('stand firm'/'5DN', '17').
card_multiverse_id('stand firm'/'5DN', '50138').

card_in_set('stasis cocoon', '5DN').
card_original_type('stasis cocoon'/'5DN', 'Enchant Artifact').
card_original_text('stasis cocoon'/'5DN', 'Enchanted artifact\'s activated abilities can\'t be played.\nIf enchanted artifact is a creature, it can\'t attack or block.').
card_first_print('stasis cocoon', '5DN').
card_image_name('stasis cocoon'/'5DN', 'stasis cocoon').
card_uid('stasis cocoon'/'5DN', '5DN:Stasis Cocoon:stasis cocoon').
card_rarity('stasis cocoon'/'5DN', 'Common').
card_artist('stasis cocoon'/'5DN', 'Kev Walker').
card_number('stasis cocoon'/'5DN', '18').
card_flavor_text('stasis cocoon'/'5DN', 'Though the grip of a golem is strong, the grip of Auriok magic is far stronger.').
card_multiverse_id('stasis cocoon'/'5DN', '51607').

card_in_set('steelshaper\'s gift', '5DN').
card_original_type('steelshaper\'s gift'/'5DN', 'Sorcery').
card_original_text('steelshaper\'s gift'/'5DN', 'Search your library for an Equipment card, reveal that card, and put it into your hand. Then shuffle your library.').
card_first_print('steelshaper\'s gift', '5DN').
card_image_name('steelshaper\'s gift'/'5DN', 'steelshaper\'s gift').
card_uid('steelshaper\'s gift'/'5DN', '5DN:Steelshaper\'s Gift:steelshaper\'s gift').
card_rarity('steelshaper\'s gift'/'5DN', 'Uncommon').
card_artist('steelshaper\'s gift'/'5DN', 'Tim Hildebrandt').
card_number('steelshaper\'s gift'/'5DN', '19').
card_flavor_text('steelshaper\'s gift'/'5DN', 'Some blades seek their own wielders.').
card_multiverse_id('steelshaper\'s gift'/'5DN', '51078').

card_in_set('summoner\'s egg', '5DN').
card_original_type('summoner\'s egg'/'5DN', 'Artifact Creature').
card_original_text('summoner\'s egg'/'5DN', 'Imprint When Summoner\'s Egg comes into play, you may remove a card in your hand from the game face down.\nWhen Summoner\'s Egg is put into a graveyard from play, turn the imprinted face-down card face up. If that card is a creature card, put it into play under your control.').
card_first_print('summoner\'s egg', '5DN').
card_image_name('summoner\'s egg'/'5DN', 'summoner\'s egg').
card_uid('summoner\'s egg'/'5DN', '5DN:Summoner\'s Egg:summoner\'s egg').
card_rarity('summoner\'s egg'/'5DN', 'Rare').
card_artist('summoner\'s egg'/'5DN', 'Jim Nelson').
card_number('summoner\'s egg'/'5DN', '157').
card_multiverse_id('summoner\'s egg'/'5DN', '51075').

card_in_set('summoning station', '5DN').
card_original_type('summoning station'/'5DN', 'Artifact').
card_original_text('summoning station'/'5DN', '{T}: Put a 2/2 colorless Pincher creature token into play.\nWhenever an artifact is put into a graveyard from play, you may untap Summoning Station.').
card_first_print('summoning station', '5DN').
card_image_name('summoning station'/'5DN', 'summoning station').
card_uid('summoning station'/'5DN', '5DN:Summoning Station:summoning station').
card_rarity('summoning station'/'5DN', 'Rare').
card_artist('summoning station'/'5DN', 'Stephen Tappin').
card_number('summoning station'/'5DN', '158').
card_multiverse_id('summoning station'/'5DN', '72858').

card_in_set('suncrusher', '5DN').
card_original_type('suncrusher'/'5DN', 'Artifact Creature').
card_original_text('suncrusher'/'5DN', 'Sunburst (This comes into play with a +1/+1 counter on it for each color of mana used to pay its cost.)\n{4}, {T}, Remove a +1/+1 counter from Suncrusher: Destroy target creature.\n{2}, Remove a +1/+1 counter from Suncrusher: Return Suncrusher to its owner\'s hand.').
card_first_print('suncrusher', '5DN').
card_image_name('suncrusher'/'5DN', 'suncrusher').
card_uid('suncrusher'/'5DN', '5DN:Suncrusher:suncrusher').
card_rarity('suncrusher'/'5DN', 'Rare').
card_artist('suncrusher'/'5DN', 'Dave Dorman').
card_number('suncrusher'/'5DN', '159').
card_multiverse_id('suncrusher'/'5DN', '48177').

card_in_set('suntouched myr', '5DN').
card_original_type('suntouched myr'/'5DN', 'Artifact Creature — Myr').
card_original_text('suntouched myr'/'5DN', 'Sunburst (This comes into play with a +1/+1 counter on it for each color of mana used to pay its cost.)').
card_first_print('suntouched myr', '5DN').
card_image_name('suntouched myr'/'5DN', 'suntouched myr').
card_uid('suntouched myr'/'5DN', '5DN:Suntouched Myr:suntouched myr').
card_rarity('suntouched myr'/'5DN', 'Common').
card_artist('suntouched myr'/'5DN', 'Greg Hildebrandt').
card_number('suntouched myr'/'5DN', '160').
card_flavor_text('suntouched myr'/'5DN', 'As the fifth sun joins the other four, new abilities awaken in the creatures of Mirrodin.').
card_multiverse_id('suntouched myr'/'5DN', '51105').

card_in_set('sylvok explorer', '5DN').
card_original_type('sylvok explorer'/'5DN', 'Creature — Human Druid').
card_original_text('sylvok explorer'/'5DN', '{T}: Add to your mana pool one mana of any color that a land an opponent controls could produce.').
card_first_print('sylvok explorer', '5DN').
card_image_name('sylvok explorer'/'5DN', 'sylvok explorer').
card_uid('sylvok explorer'/'5DN', '5DN:Sylvok Explorer:sylvok explorer').
card_rarity('sylvok explorer'/'5DN', 'Common').
card_artist('sylvok explorer'/'5DN', 'Wayne England').
card_number('sylvok explorer'/'5DN', '93').
card_flavor_text('sylvok explorer'/'5DN', 'Any creature on a druid\'s shoulder is his family, any land under his feet his home.').
card_multiverse_id('sylvok explorer'/'5DN', '51125').

card_in_set('synod centurion', '5DN').
card_original_type('synod centurion'/'5DN', 'Artifact Creature').
card_original_text('synod centurion'/'5DN', 'When you control no other artifacts, sacrifice Synod Centurion.').
card_first_print('synod centurion', '5DN').
card_image_name('synod centurion'/'5DN', 'synod centurion').
card_uid('synod centurion'/'5DN', '5DN:Synod Centurion:synod centurion').
card_rarity('synod centurion'/'5DN', 'Uncommon').
card_artist('synod centurion'/'5DN', 'Kev Walker').
card_number('synod centurion'/'5DN', '161').
card_flavor_text('synod centurion'/'5DN', '\"We order them to stand, they stand. We order them to wait, they wait. We order them to die, they die.\"\n—Pontifex, elder researcher').
card_multiverse_id('synod centurion'/'5DN', '73929').

card_in_set('tangle asp', '5DN').
card_original_type('tangle asp'/'5DN', 'Creature — Snake').
card_original_text('tangle asp'/'5DN', 'Whenever Tangle Asp blocks or becomes blocked by a creature, destroy that creature at end of combat.').
card_first_print('tangle asp', '5DN').
card_image_name('tangle asp'/'5DN', 'tangle asp').
card_uid('tangle asp'/'5DN', '5DN:Tangle Asp:tangle asp').
card_rarity('tangle asp'/'5DN', 'Common').
card_artist('tangle asp'/'5DN', 'Daren Bader').
card_number('tangle asp'/'5DN', '94').
card_flavor_text('tangle asp'/'5DN', '\"I have no fangs, yet I constantly bite. I have no scales, yet I slither through you. What am I?\"\n—Riddle of the viper\'s venom').
card_multiverse_id('tangle asp'/'5DN', '50203').

card_in_set('tel-jilad justice', '5DN').
card_original_type('tel-jilad justice'/'5DN', 'Instant').
card_original_text('tel-jilad justice'/'5DN', 'Destroy target artifact.\nScry 2 (Look at the top two cards of your library. Put any number of them on the bottom of your library and the rest on top in any order.)').
card_first_print('tel-jilad justice', '5DN').
card_image_name('tel-jilad justice'/'5DN', 'tel-jilad justice').
card_uid('tel-jilad justice'/'5DN', '5DN:Tel-Jilad Justice:tel-jilad justice').
card_rarity('tel-jilad justice'/'5DN', 'Uncommon').
card_artist('tel-jilad justice'/'5DN', 'Alex Horley-Orlandelli').
card_number('tel-jilad justice'/'5DN', '95').
card_multiverse_id('tel-jilad justice'/'5DN', '50190').

card_in_set('tel-jilad lifebreather', '5DN').
card_original_type('tel-jilad lifebreather'/'5DN', 'Creature — Troll Shaman').
card_original_text('tel-jilad lifebreather'/'5DN', '{G}, {T}, Sacrifice a Forest: Regenerate target creature.').
card_first_print('tel-jilad lifebreather', '5DN').
card_image_name('tel-jilad lifebreather'/'5DN', 'tel-jilad lifebreather').
card_uid('tel-jilad lifebreather'/'5DN', '5DN:Tel-Jilad Lifebreather:tel-jilad lifebreather').
card_rarity('tel-jilad lifebreather'/'5DN', 'Common').
card_artist('tel-jilad lifebreather'/'5DN', 'Jim Nelson').
card_number('tel-jilad lifebreather'/'5DN', '96').
card_flavor_text('tel-jilad lifebreather'/'5DN', '\"The Tangle loans you life. Spend it well.\"').
card_multiverse_id('tel-jilad lifebreather'/'5DN', '73582').

card_in_set('thermal navigator', '5DN').
card_original_type('thermal navigator'/'5DN', 'Artifact Creature').
card_original_text('thermal navigator'/'5DN', 'Sacrifice an artifact: Thermal Navigator gains flying until end of turn.').
card_first_print('thermal navigator', '5DN').
card_image_name('thermal navigator'/'5DN', 'thermal navigator').
card_uid('thermal navigator'/'5DN', '5DN:Thermal Navigator:thermal navigator').
card_rarity('thermal navigator'/'5DN', 'Common').
card_artist('thermal navigator'/'5DN', 'Jim Murray').
card_number('thermal navigator'/'5DN', '162').
card_flavor_text('thermal navigator'/'5DN', 'It takes off with a roar of superheated wind, startling wild pterons as they survey the ground below.').
card_multiverse_id('thermal navigator'/'5DN', '50177').

card_in_set('thought courier', '5DN').
card_original_type('thought courier'/'5DN', 'Creature — Human Wizard').
card_original_text('thought courier'/'5DN', '{T}: Draw a card, then discard a card from your hand.').
card_first_print('thought courier', '5DN').
card_image_name('thought courier'/'5DN', 'thought courier').
card_uid('thought courier'/'5DN', '5DN:Thought Courier:thought courier').
card_rarity('thought courier'/'5DN', 'Common').
card_artist('thought courier'/'5DN', 'Stephen Tappin').
card_number('thought courier'/'5DN', '38').
card_flavor_text('thought courier'/'5DN', 'It is telling of the vedalkens\' culture that they trust human couriers more than their own comrades.').
card_multiverse_id('thought courier'/'5DN', '73570').

card_in_set('tornado elemental', '5DN').
card_original_type('tornado elemental'/'5DN', 'Creature — Elemental').
card_original_text('tornado elemental'/'5DN', 'When Tornado Elemental comes into play, it deals 6 damage to each creature with flying.\nYou may have Tornado Elemental deal its combat damage to defending player as though it weren\'t blocked.').
card_first_print('tornado elemental', '5DN').
card_image_name('tornado elemental'/'5DN', 'tornado elemental').
card_uid('tornado elemental'/'5DN', '5DN:Tornado Elemental:tornado elemental').
card_rarity('tornado elemental'/'5DN', 'Rare').
card_artist('tornado elemental'/'5DN', 'Alex Horley-Orlandelli').
card_number('tornado elemental'/'5DN', '97').
card_multiverse_id('tornado elemental'/'5DN', '51161').

card_in_set('trinket mage', '5DN').
card_original_type('trinket mage'/'5DN', 'Creature — Human Wizard').
card_original_text('trinket mage'/'5DN', 'When Trinket Mage comes into play, you may search your library for an artifact card with converted mana cost 1 or less, reveal that card, and put it into your hand. If you do, shuffle your library.').
card_first_print('trinket mage', '5DN').
card_image_name('trinket mage'/'5DN', 'trinket mage').
card_uid('trinket mage'/'5DN', '5DN:Trinket Mage:trinket mage').
card_rarity('trinket mage'/'5DN', 'Common').
card_artist('trinket mage'/'5DN', 'Mark A. Nelson').
card_number('trinket mage'/'5DN', '39').
card_multiverse_id('trinket mage'/'5DN', '50163').

card_in_set('tyrranax', '5DN').
card_original_type('tyrranax'/'5DN', 'Creature — Beast').
card_original_text('tyrranax'/'5DN', '{1}{G}: Tyrranax gets -1/+1 until end of turn.').
card_first_print('tyrranax', '5DN').
card_image_name('tyrranax'/'5DN', 'tyrranax').
card_uid('tyrranax'/'5DN', '5DN:Tyrranax:tyrranax').
card_rarity('tyrranax'/'5DN', 'Common').
card_artist('tyrranax'/'5DN', 'Carl Critchlow').
card_number('tyrranax'/'5DN', '98').
card_flavor_text('tyrranax'/'5DN', 'It outnumbers you one to many.').
card_multiverse_id('tyrranax'/'5DN', '50191').

card_in_set('vanquish', '5DN').
card_original_type('vanquish'/'5DN', 'Instant').
card_original_text('vanquish'/'5DN', 'Destroy target blocking creature.').
card_first_print('vanquish', '5DN').
card_image_name('vanquish'/'5DN', 'vanquish').
card_uid('vanquish'/'5DN', '5DN:Vanquish:vanquish').
card_rarity('vanquish'/'5DN', 'Uncommon').
card_artist('vanquish'/'5DN', 'Carl Critchlow').
card_number('vanquish'/'5DN', '20').
card_flavor_text('vanquish'/'5DN', '\"All beings have an inner light. Let me show you yours.\"\n—Ushanti, leonin seer').
card_multiverse_id('vanquish'/'5DN', '51147').

card_in_set('vedalken mastermind', '5DN').
card_original_type('vedalken mastermind'/'5DN', 'Creature — Vedalken Wizard').
card_original_text('vedalken mastermind'/'5DN', '{U}, {T}: Return target permanent you control to its owner\'s hand.').
card_first_print('vedalken mastermind', '5DN').
card_image_name('vedalken mastermind'/'5DN', 'vedalken mastermind').
card_uid('vedalken mastermind'/'5DN', '5DN:Vedalken Mastermind:vedalken mastermind').
card_rarity('vedalken mastermind'/'5DN', 'Uncommon').
card_artist('vedalken mastermind'/'5DN', 'Darrell Riche').
card_number('vedalken mastermind'/'5DN', '40').
card_flavor_text('vedalken mastermind'/'5DN', 'The vedalken see other beings from a cold distance, as either experimental subjects or objects of study.').
card_multiverse_id('vedalken mastermind'/'5DN', '50834').

card_in_set('vedalken orrery', '5DN').
card_original_type('vedalken orrery'/'5DN', 'Artifact').
card_original_text('vedalken orrery'/'5DN', 'You may play nonland cards any time you could play an instant.').
card_first_print('vedalken orrery', '5DN').
card_image_name('vedalken orrery'/'5DN', 'vedalken orrery').
card_uid('vedalken orrery'/'5DN', '5DN:Vedalken Orrery:vedalken orrery').
card_rarity('vedalken orrery'/'5DN', 'Rare').
card_artist('vedalken orrery'/'5DN', 'John Avon').
card_number('vedalken orrery'/'5DN', '163').
card_flavor_text('vedalken orrery'/'5DN', 'The model incorporated the fifth sun effortlessly, as if it had always known.').
card_multiverse_id('vedalken orrery'/'5DN', '50134').

card_in_set('vedalken shackles', '5DN').
card_original_type('vedalken shackles'/'5DN', 'Artifact').
card_original_text('vedalken shackles'/'5DN', 'You may choose not to untap Vedalken Shackles during your untap step.\n{2}, {T}: Gain control of target creature with power less than or equal to the number of Islands you control as long as Vedalken Shackles remains tapped.').
card_first_print('vedalken shackles', '5DN').
card_image_name('vedalken shackles'/'5DN', 'vedalken shackles').
card_uid('vedalken shackles'/'5DN', '5DN:Vedalken Shackles:vedalken shackles').
card_rarity('vedalken shackles'/'5DN', 'Rare').
card_artist('vedalken shackles'/'5DN', 'Mark Zug').
card_number('vedalken shackles'/'5DN', '164').
card_multiverse_id('vedalken shackles'/'5DN', '50120').

card_in_set('vicious betrayal', '5DN').
card_original_type('vicious betrayal'/'5DN', 'Sorcery').
card_original_text('vicious betrayal'/'5DN', 'As an additional cost to play Vicious Betrayal, sacrifice any number of creatures.\nTarget creature gets +2/+2 until end of turn for each creature sacrificed this way.').
card_first_print('vicious betrayal', '5DN').
card_image_name('vicious betrayal'/'5DN', 'vicious betrayal').
card_uid('vicious betrayal'/'5DN', '5DN:Vicious Betrayal:vicious betrayal').
card_rarity('vicious betrayal'/'5DN', 'Common').
card_artist('vicious betrayal'/'5DN', 'Tomas Giorello').
card_number('vicious betrayal'/'5DN', '60').
card_multiverse_id('vicious betrayal'/'5DN', '73575').

card_in_set('viridian lorebearers', '5DN').
card_original_type('viridian lorebearers'/'5DN', 'Creature — Elf Shaman').
card_original_text('viridian lorebearers'/'5DN', '{3}{G}, {T}: Target creature gets +X/+X until end of turn, where X is the number of artifacts your opponents control.').
card_first_print('viridian lorebearers', '5DN').
card_image_name('viridian lorebearers'/'5DN', 'viridian lorebearers').
card_uid('viridian lorebearers'/'5DN', '5DN:Viridian Lorebearers:viridian lorebearers').
card_rarity('viridian lorebearers'/'5DN', 'Uncommon').
card_artist('viridian lorebearers'/'5DN', 'Darrell Riche').
card_number('viridian lorebearers'/'5DN', '99').
card_flavor_text('viridian lorebearers'/'5DN', 'A small band of elves pledged many years ago to avoid the Edges of Forgetting. They have remained hidden until now.').
card_multiverse_id('viridian lorebearers'/'5DN', '50188').

card_in_set('viridian scout', '5DN').
card_original_type('viridian scout'/'5DN', 'Creature — Elf Warrior').
card_original_text('viridian scout'/'5DN', '{2}{G}, Sacrifice Viridian Scout: Viridian Scout deals 2 damage to target creature with flying.').
card_first_print('viridian scout', '5DN').
card_image_name('viridian scout'/'5DN', 'viridian scout').
card_uid('viridian scout'/'5DN', '5DN:Viridian Scout:viridian scout').
card_rarity('viridian scout'/'5DN', 'Common').
card_artist('viridian scout'/'5DN', 'Daren Bader').
card_number('viridian scout'/'5DN', '100').
card_flavor_text('viridian scout'/'5DN', 'They know a hoverguard\'s only blind spot is directly above it.').
card_multiverse_id('viridian scout'/'5DN', '73583').

card_in_set('vulshok sorcerer', '5DN').
card_original_type('vulshok sorcerer'/'5DN', 'Creature — Human Shaman').
card_original_text('vulshok sorcerer'/'5DN', 'Haste\n{T}: Vulshok Sorcerer deals 1 damage to target creature or player.').
card_first_print('vulshok sorcerer', '5DN').
card_image_name('vulshok sorcerer'/'5DN', 'vulshok sorcerer').
card_uid('vulshok sorcerer'/'5DN', '5DN:Vulshok Sorcerer:vulshok sorcerer').
card_rarity('vulshok sorcerer'/'5DN', 'Common').
card_artist('vulshok sorcerer'/'5DN', 'rk post').
card_number('vulshok sorcerer'/'5DN', '80').
card_flavor_text('vulshok sorcerer'/'5DN', 'Vulshok sorcerers train by leaping into electrified storm clouds. Dead or alive, they come back down with smiles on their faces.').
card_multiverse_id('vulshok sorcerer'/'5DN', '51177').

card_in_set('wayfarer\'s bauble', '5DN').
card_original_type('wayfarer\'s bauble'/'5DN', 'Artifact').
card_original_text('wayfarer\'s bauble'/'5DN', '{2}, {T}, Sacrifice Wayfarer\'s Bauble: Search your library for a basic land card and put that card into play tapped. Then shuffle your library.').
card_first_print('wayfarer\'s bauble', '5DN').
card_image_name('wayfarer\'s bauble'/'5DN', 'wayfarer\'s bauble').
card_uid('wayfarer\'s bauble'/'5DN', '5DN:Wayfarer\'s Bauble:wayfarer\'s bauble').
card_rarity('wayfarer\'s bauble'/'5DN', 'Common').
card_artist('wayfarer\'s bauble'/'5DN', 'Darrell Riche').
card_number('wayfarer\'s bauble'/'5DN', '165').
card_flavor_text('wayfarer\'s bauble'/'5DN', 'It is the forest beyond the horizon, the mountain waiting to be climbed, the new land across the endless sea.').
card_multiverse_id('wayfarer\'s bauble'/'5DN', '51110').
