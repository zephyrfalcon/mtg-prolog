% Coldsnap Theme Decks

set('CST').
set_name('CST', 'Coldsnap Theme Decks').
set_release_date('CST', '2006-07-21').
set_border('CST', 'black').
set_type('CST', 'box').

card_in_set('arcum\'s weathervane', 'CST').
card_original_type('arcum\'s weathervane'/'CST', 'Artifact').
card_original_text('arcum\'s weathervane'/'CST', '').
card_image_name('arcum\'s weathervane'/'CST', 'arcum\'s weathervane').
card_uid('arcum\'s weathervane'/'CST', 'CST:Arcum\'s Weathervane:arcum\'s weathervane').
card_rarity('arcum\'s weathervane'/'CST', 'Special').
card_artist('arcum\'s weathervane'/'CST', 'Tom Wänerstrand').
card_number('arcum\'s weathervane'/'CST', '310').

card_in_set('ashen ghoul', 'CST').
card_original_type('ashen ghoul'/'CST', 'Creature — Zombie').
card_original_text('ashen ghoul'/'CST', '').
card_image_name('ashen ghoul'/'CST', 'ashen ghoul').
card_uid('ashen ghoul'/'CST', 'CST:Ashen Ghoul:ashen ghoul').
card_rarity('ashen ghoul'/'CST', 'Special').
card_artist('ashen ghoul'/'CST', 'Ron Spencer').
card_number('ashen ghoul'/'CST', '114').

card_in_set('aurochs', 'CST').
card_original_type('aurochs'/'CST', 'Creature — Aurochs').
card_original_text('aurochs'/'CST', '').
card_image_name('aurochs'/'CST', 'aurochs').
card_uid('aurochs'/'CST', 'CST:Aurochs:aurochs').
card_rarity('aurochs'/'CST', 'Special').
card_artist('aurochs'/'CST', 'Ken Meyer, Jr.').
card_number('aurochs'/'CST', '225').
card_flavor_text('aurochs'/'CST', 'One Auroch may feed a village, but a herd will flatten it.').

card_in_set('balduvian dead', 'CST').
card_original_type('balduvian dead'/'CST', 'Creature — Zombie').
card_original_text('balduvian dead'/'CST', '').
card_image_name('balduvian dead'/'CST', 'balduvian dead').
card_uid('balduvian dead'/'CST', 'CST:Balduvian Dead:balduvian dead').
card_rarity('balduvian dead'/'CST', 'Special').
card_artist('balduvian dead'/'CST', 'Mike Kimble').
card_number('balduvian dead'/'CST', '43').

card_in_set('barbed sextant', 'CST').
card_original_type('barbed sextant'/'CST', 'Artifact').
card_original_text('barbed sextant'/'CST', '').
card_image_name('barbed sextant'/'CST', 'barbed sextant').
card_uid('barbed sextant'/'CST', 'CST:Barbed Sextant:barbed sextant').
card_rarity('barbed sextant'/'CST', 'Special').
card_artist('barbed sextant'/'CST', 'Amy Weber').
card_number('barbed sextant'/'CST', '312').

card_in_set('binding grasp', 'CST').
card_original_type('binding grasp'/'CST', 'Enchantment — Aura').
card_original_text('binding grasp'/'CST', '').
card_image_name('binding grasp'/'CST', 'binding grasp').
card_uid('binding grasp'/'CST', 'CST:Binding Grasp:binding grasp').
card_rarity('binding grasp'/'CST', 'Special').
card_artist('binding grasp'/'CST', 'Ruth Thompson').
card_number('binding grasp'/'CST', '60').
card_flavor_text('binding grasp'/'CST', '\"What I want, I take.\"\n—Gustha Ebbasdotter,\nKjeldoran Royal Mage').

card_in_set('bounty of the hunt', 'CST').
card_original_type('bounty of the hunt'/'CST', 'Instant').
card_original_text('bounty of the hunt'/'CST', '').
card_image_name('bounty of the hunt'/'CST', 'bounty of the hunt').
card_uid('bounty of the hunt'/'CST', 'CST:Bounty of the Hunt:bounty of the hunt').
card_rarity('bounty of the hunt'/'CST', 'Special').
card_artist('bounty of the hunt'/'CST', 'Jeff A. Menges').
card_number('bounty of the hunt'/'CST', '85').

card_in_set('brainstorm', 'CST').
card_original_type('brainstorm'/'CST', 'Instant').
card_original_text('brainstorm'/'CST', '').
card_image_name('brainstorm'/'CST', 'brainstorm').
card_uid('brainstorm'/'CST', 'CST:Brainstorm:brainstorm').
card_rarity('brainstorm'/'CST', 'Special').
card_artist('brainstorm'/'CST', 'Christopher Rush').
card_number('brainstorm'/'CST', '61').
card_flavor_text('brainstorm'/'CST', '\"I reeled from the blow, and then suddenly, I knew exactly what to do. Within moments, victory was mine.\"\n—Gustha Ebbasdotter,\nKjeldoran Royal Mage').

card_in_set('browse', 'CST').
card_original_type('browse'/'CST', 'Enchantment').
card_original_text('browse'/'CST', '').
card_image_name('browse'/'CST', 'browse').
card_uid('browse'/'CST', 'CST:Browse:browse').
card_rarity('browse'/'CST', 'Special').
card_artist('browse'/'CST', 'Phil Foglio').
card_number('browse'/'CST', '25').
card_flavor_text('browse'/'CST', '\"Once great literature—now great litter.\"\n—Jaya Ballard, Task Mage').

card_in_set('casting of bones', 'CST').
card_original_type('casting of bones'/'CST', 'Enchantment — Aura').
card_original_text('casting of bones'/'CST', '').
card_image_name('casting of bones'/'CST', 'casting of bones').
card_uid('casting of bones'/'CST', 'CST:Casting of Bones:casting of bones').
card_rarity('casting of bones'/'CST', 'Special').
card_artist('casting of bones'/'CST', 'Anson Maddocks').
card_number('casting of bones'/'CST', '44b').
card_flavor_text('casting of bones'/'CST', '\"Only a necromancer would create such a foul form of divination.\"\n—Gerda Äagesdotter,\nArchmage of the Unseen').

card_in_set('dark banishing', 'CST').
card_original_type('dark banishing'/'CST', 'Instant').
card_original_text('dark banishing'/'CST', '').
card_image_name('dark banishing'/'CST', 'dark banishing').
card_uid('dark banishing'/'CST', 'CST:Dark Banishing:dark banishing').
card_rarity('dark banishing'/'CST', 'Special').
card_artist('dark banishing'/'CST', 'Drew Tucker').
card_number('dark banishing'/'CST', '119').
card_flavor_text('dark banishing'/'CST', '\"Will not the mountains quake and hills melt at the coming of the darkness? Share this vision with your enemies, Lim-Dûl, and they shall wither.\"\n—Leshrac, Walker of Night').

card_in_set('dark ritual', 'CST').
card_original_type('dark ritual'/'CST', 'Instant').
card_original_text('dark ritual'/'CST', '').
card_image_name('dark ritual'/'CST', 'dark ritual').
card_uid('dark ritual'/'CST', 'CST:Dark Ritual:dark ritual').
card_rarity('dark ritual'/'CST', 'Special').
card_artist('dark ritual'/'CST', 'Justin Hampton').
card_number('dark ritual'/'CST', '120').
card_flavor_text('dark ritual'/'CST', '\"Leshrac, my liege, grant me the power I am due.\"\n—Lim-Dûl, the Necromancer').

card_in_set('deadly insect', 'CST').
card_original_type('deadly insect'/'CST', 'Creature — Insect').
card_original_text('deadly insect'/'CST', '').
card_image_name('deadly insect'/'CST', 'deadly insect').
card_uid('deadly insect'/'CST', 'CST:Deadly Insect:deadly insect').
card_rarity('deadly insect'/'CST', 'Special').
card_artist('deadly insect'/'CST', 'Scott Kirschner').
card_number('deadly insect'/'CST', '86a').
card_flavor_text('deadly insect'/'CST', '\"Beautiful, indeed—but one sting could fell a Giant in a heartbeat.\"\n—Taaveti of Kelsinko,\nElvish Hunter').

card_in_set('death spark', 'CST').
card_original_type('death spark'/'CST', 'Instant').
card_original_text('death spark'/'CST', '').
card_image_name('death spark'/'CST', 'death spark').
card_uid('death spark'/'CST', 'CST:Death Spark:death spark').
card_rarity('death spark'/'CST', 'Special').
card_artist('death spark'/'CST', 'Mark Tedin').
card_number('death spark'/'CST', '70').

card_in_set('disenchant', 'CST').
card_original_type('disenchant'/'CST', 'Instant').
card_original_text('disenchant'/'CST', '').
card_image_name('disenchant'/'CST', 'disenchant').
card_uid('disenchant'/'CST', 'CST:Disenchant:disenchant').
card_rarity('disenchant'/'CST', 'Special').
card_artist('disenchant'/'CST', 'Brian Snõddy').
card_number('disenchant'/'CST', '20').
card_flavor_text('disenchant'/'CST', '\"I implore you not to forget the horrors of the past. You would have us start the Brothers\' War anew!\"\n—Sorine Relicbane, Soldevi Heretic').

card_in_set('drift of the dead', 'CST').
card_original_type('drift of the dead'/'CST', 'Creature — Wall').
card_original_text('drift of the dead'/'CST', '').
card_image_name('drift of the dead'/'CST', 'drift of the dead').
card_uid('drift of the dead'/'CST', 'CST:Drift of the Dead:drift of the dead').
card_rarity('drift of the dead'/'CST', 'Special').
card_artist('drift of the dead'/'CST', 'Brian Snõddy').
card_number('drift of the dead'/'CST', '123').
card_flavor_text('drift of the dead'/'CST', '\"Take their dead, and entomb them in the snow. Risen, they shall serve a new purpose.\"\n—Lim-Dûl, the Necromancer').

card_in_set('essence flare', 'CST').
card_original_type('essence flare'/'CST', 'Enchantment — Aura').
card_original_text('essence flare'/'CST', '').
card_image_name('essence flare'/'CST', 'essence flare').
card_uid('essence flare'/'CST', 'CST:Essence Flare:essence flare').
card_rarity('essence flare'/'CST', 'Special').
card_artist('essence flare'/'CST', 'Richard Kane Ferguson').
card_number('essence flare'/'CST', '69').
card_flavor_text('essence flare'/'CST', 'Never underestimate the power of the soul unleashed.').

card_in_set('forest', 'CST').
card_original_type('forest'/'CST', 'Basic Land — Forest').
card_original_text('forest'/'CST', '').
card_image_name('forest'/'CST', 'forest1').
card_uid('forest'/'CST', 'CST:Forest:forest1').
card_rarity('forest'/'CST', 'Basic Land').
card_artist('forest'/'CST', 'Pat Morrissey').
card_number('forest'/'CST', '381').

card_in_set('forest', 'CST').
card_original_type('forest'/'CST', 'Basic Land — Forest').
card_original_text('forest'/'CST', '').
card_image_name('forest'/'CST', 'forest2').
card_uid('forest'/'CST', 'CST:Forest:forest2').
card_rarity('forest'/'CST', 'Basic Land').
card_artist('forest'/'CST', 'Pat Morrissey').
card_number('forest'/'CST', '382').

card_in_set('forest', 'CST').
card_original_type('forest'/'CST', 'Basic Land — Forest').
card_original_text('forest'/'CST', '').
card_image_name('forest'/'CST', 'forest3').
card_uid('forest'/'CST', 'CST:Forest:forest3').
card_rarity('forest'/'CST', 'Basic Land').
card_artist('forest'/'CST', 'Pat Morrissey').
card_number('forest'/'CST', '383').

card_in_set('gangrenous zombies', 'CST').
card_original_type('gangrenous zombies'/'CST', 'Creature — Zombie').
card_original_text('gangrenous zombies'/'CST', '').
card_image_name('gangrenous zombies'/'CST', 'gangrenous zombies').
card_uid('gangrenous zombies'/'CST', 'CST:Gangrenous Zombies:gangrenous zombies').
card_rarity('gangrenous zombies'/'CST', 'Special').
card_artist('gangrenous zombies'/'CST', 'Brian Snõddy').
card_number('gangrenous zombies'/'CST', '127').

card_in_set('giant trap door spider', 'CST').
card_original_type('giant trap door spider'/'CST', 'Creature — Spider').
card_original_text('giant trap door spider'/'CST', '').
card_image_name('giant trap door spider'/'CST', 'giant trap door spider').
card_uid('giant trap door spider'/'CST', 'CST:Giant Trap Door Spider:giant trap door spider').
card_rarity('giant trap door spider'/'CST', 'Special').
card_artist('giant trap door spider'/'CST', 'Heather Hudson').
card_number('giant trap door spider'/'CST', '293').
card_flavor_text('giant trap door spider'/'CST', '\"So large and so quiet—a combination best avoided.\"\n—Disa the Restless, journal entry').

card_in_set('gorilla shaman', 'CST').
card_original_type('gorilla shaman'/'CST', 'Creature — Ape Shaman').
card_original_text('gorilla shaman'/'CST', '').
card_image_name('gorilla shaman'/'CST', 'gorilla shaman').
card_uid('gorilla shaman'/'CST', 'CST:Gorilla Shaman:gorilla shaman').
card_rarity('gorilla shaman'/'CST', 'Special').
card_artist('gorilla shaman'/'CST', 'Anthony Waters').
card_number('gorilla shaman'/'CST', '72a').
card_flavor_text('gorilla shaman'/'CST', '\"Frankly, destruction is best left to professionals.\"\n—Jaya Ballard, Task Mage').

card_in_set('iceberg', 'CST').
card_original_type('iceberg'/'CST', 'Enchantment').
card_original_text('iceberg'/'CST', '').
card_image_name('iceberg'/'CST', 'iceberg').
card_uid('iceberg'/'CST', 'CST:Iceberg:iceberg').
card_rarity('iceberg'/'CST', 'Special').
card_artist('iceberg'/'CST', 'Jeff A. Menges').
card_number('iceberg'/'CST', '73').

card_in_set('incinerate', 'CST').
card_original_type('incinerate'/'CST', 'Instant').
card_original_text('incinerate'/'CST', '').
card_image_name('incinerate'/'CST', 'incinerate').
card_uid('incinerate'/'CST', 'CST:Incinerate:incinerate').
card_rarity('incinerate'/'CST', 'Special').
card_artist('incinerate'/'CST', 'Mark Poole').
card_number('incinerate'/'CST', '194').
card_flavor_text('incinerate'/'CST', '\"Yes, I think ‘toast\' is an appropriate description.\"\n—Jaya Ballard, Task Mage').

card_in_set('insidious bookworms', 'CST').
card_original_type('insidious bookworms'/'CST', 'Creature — Worm').
card_original_text('insidious bookworms'/'CST', '').
card_image_name('insidious bookworms'/'CST', 'insidious bookworms').
card_uid('insidious bookworms'/'CST', 'CST:Insidious Bookworms:insidious bookworms').
card_rarity('insidious bookworms'/'CST', 'Special').
card_artist('insidious bookworms'/'CST', 'Greg Simanson').
card_number('insidious bookworms'/'CST', '51a').

card_in_set('island', 'CST').
card_original_type('island'/'CST', 'Basic Land — Island').
card_original_text('island'/'CST', '').
card_image_name('island'/'CST', 'island1').
card_uid('island'/'CST', 'CST:Island:island1').
card_rarity('island'/'CST', 'Basic Land').
card_artist('island'/'CST', 'Anson Maddocks').
card_number('island'/'CST', '372').

card_in_set('island', 'CST').
card_original_type('island'/'CST', 'Basic Land — Island').
card_original_text('island'/'CST', '').
card_image_name('island'/'CST', 'island2').
card_uid('island'/'CST', 'CST:Island:island2').
card_rarity('island'/'CST', 'Basic Land').
card_artist('island'/'CST', 'Anson Maddocks').
card_number('island'/'CST', '373').

card_in_set('island', 'CST').
card_original_type('island'/'CST', 'Basic Land — Island').
card_original_text('island'/'CST', '').
card_image_name('island'/'CST', 'island3').
card_uid('island'/'CST', 'CST:Island:island3').
card_rarity('island'/'CST', 'Basic Land').
card_artist('island'/'CST', 'Anson Maddocks').
card_number('island'/'CST', '374').

card_in_set('kjeldoran dead', 'CST').
card_original_type('kjeldoran dead'/'CST', 'Creature — Skeleton').
card_original_text('kjeldoran dead'/'CST', '').
card_image_name('kjeldoran dead'/'CST', 'kjeldoran dead').
card_uid('kjeldoran dead'/'CST', 'CST:Kjeldoran Dead:kjeldoran dead').
card_rarity('kjeldoran dead'/'CST', 'Special').
card_artist('kjeldoran dead'/'CST', 'Melissa A. Benson').
card_number('kjeldoran dead'/'CST', '137').
card_flavor_text('kjeldoran dead'/'CST', '\"They shall kill those whom once they loved.\"\n—Lim-Dûl, the Necromancer').

card_in_set('kjeldoran elite guard', 'CST').
card_original_type('kjeldoran elite guard'/'CST', 'Creature — Human Soldier').
card_original_text('kjeldoran elite guard'/'CST', '').
card_image_name('kjeldoran elite guard'/'CST', 'kjeldoran elite guard').
card_uid('kjeldoran elite guard'/'CST', 'CST:Kjeldoran Elite Guard:kjeldoran elite guard').
card_rarity('kjeldoran elite guard'/'CST', 'Special').
card_artist('kjeldoran elite guard'/'CST', 'Melissa A. Benson').
card_number('kjeldoran elite guard'/'CST', '34').
card_flavor_text('kjeldoran elite guard'/'CST', 'The winged helms of the Guard are put on for pageants—but taken off for war.').

card_in_set('kjeldoran home guard', 'CST').
card_original_type('kjeldoran home guard'/'CST', 'Creature — Human Soldier').
card_original_text('kjeldoran home guard'/'CST', '').
card_image_name('kjeldoran home guard'/'CST', 'kjeldoran home guard').
card_uid('kjeldoran home guard'/'CST', 'CST:Kjeldoran Home Guard:kjeldoran home guard').
card_rarity('kjeldoran home guard'/'CST', 'Special').
card_artist('kjeldoran home guard'/'CST', 'Andi Rusu').
card_number('kjeldoran home guard'/'CST', '8').

card_in_set('kjeldoran pride', 'CST').
card_original_type('kjeldoran pride'/'CST', 'Enchantment — Aura').
card_original_text('kjeldoran pride'/'CST', '').
card_image_name('kjeldoran pride'/'CST', 'kjeldoran pride').
card_uid('kjeldoran pride'/'CST', 'CST:Kjeldoran Pride:kjeldoran pride').
card_rarity('kjeldoran pride'/'CST', 'Special').
card_artist('kjeldoran pride'/'CST', 'Kaja Foglio').
card_number('kjeldoran pride'/'CST', '9b').

card_in_set('lat-nam\'s legacy', 'CST').
card_original_type('lat-nam\'s legacy'/'CST', 'Instant').
card_original_text('lat-nam\'s legacy'/'CST', '').
card_image_name('lat-nam\'s legacy'/'CST', 'lat-nam\'s legacy').
card_uid('lat-nam\'s legacy'/'CST', 'CST:Lat-Nam\'s Legacy:lat-nam\'s legacy').
card_rarity('lat-nam\'s legacy'/'CST', 'Special').
card_artist('lat-nam\'s legacy'/'CST', 'Tom Wänerstrand').
card_number('lat-nam\'s legacy'/'CST', '30b').
card_flavor_text('lat-nam\'s legacy'/'CST', '\"All the knowledge of Lat-Nam could not protect its sages from the Brothers\' War.\"\n—Gerda Äagesdotter, Archmage of the Unseen').

card_in_set('legions of lim-dûl', 'CST').
card_original_type('legions of lim-dûl'/'CST', 'Creature — Zombie').
card_original_text('legions of lim-dûl'/'CST', '').
card_image_name('legions of lim-dûl'/'CST', 'legions of lim-dul').
card_uid('legions of lim-dûl'/'CST', 'CST:Legions of Lim-Dûl:legions of lim-dul').
card_rarity('legions of lim-dûl'/'CST', 'Special').
card_artist('legions of lim-dûl'/'CST', 'Anson Maddocks').
card_number('legions of lim-dûl'/'CST', '142').
card_flavor_text('legions of lim-dûl'/'CST', '\"I have seen the faces of my dead friends among that grim band, and I can bear no more.\"\n—Lucilde Fiksdotter, Leader of the Order of the White Shield.').

card_in_set('mistfolk', 'CST').
card_original_type('mistfolk'/'CST', 'Creature — Illusion').
card_original_text('mistfolk'/'CST', '').
card_image_name('mistfolk'/'CST', 'mistfolk').
card_uid('mistfolk'/'CST', 'CST:Mistfolk:mistfolk').
card_rarity('mistfolk'/'CST', 'Special').
card_artist('mistfolk'/'CST', 'Quinton Hoover').
card_number('mistfolk'/'CST', '84').
card_flavor_text('mistfolk'/'CST', '\"Although my official log will state there is no evidence pointing to the existence of the Mistfolk, my certainty is lessened by the cursed consistency of the expedition\'s eyewitness accounts.\"\n—Disa the Restless, journal entry').

card_in_set('mountain', 'CST').
card_original_type('mountain'/'CST', 'Basic Land — Mountain').
card_original_text('mountain'/'CST', '').
card_image_name('mountain'/'CST', 'mountain1').
card_uid('mountain'/'CST', 'CST:Mountain:mountain1').
card_rarity('mountain'/'CST', 'Basic Land').
card_artist('mountain'/'CST', 'Tom Wänerstrand').
card_number('mountain'/'CST', '378').

card_in_set('mountain', 'CST').
card_original_type('mountain'/'CST', 'Basic Land — Mountain').
card_original_text('mountain'/'CST', '').
card_image_name('mountain'/'CST', 'mountain2').
card_uid('mountain'/'CST', 'CST:Mountain:mountain2').
card_rarity('mountain'/'CST', 'Basic Land').
card_artist('mountain'/'CST', 'Tom Wänerstrand').
card_number('mountain'/'CST', '379').

card_in_set('mountain', 'CST').
card_original_type('mountain'/'CST', 'Basic Land — Mountain').
card_original_text('mountain'/'CST', '').
card_image_name('mountain'/'CST', 'mountain3').
card_uid('mountain'/'CST', 'CST:Mountain:mountain3').
card_rarity('mountain'/'CST', 'Basic Land').
card_artist('mountain'/'CST', 'Tom Wänerstrand').
card_number('mountain'/'CST', '380').

card_in_set('orcish healer', 'CST').
card_original_type('orcish healer'/'CST', 'Creature — Orc Cleric').
card_original_text('orcish healer'/'CST', '').
card_image_name('orcish healer'/'CST', 'orcish healer').
card_uid('orcish healer'/'CST', 'CST:Orcish Healer:orcish healer').
card_rarity('orcish healer'/'CST', 'Special').
card_artist('orcish healer'/'CST', 'Quinton Hoover').
card_number('orcish healer'/'CST', '208').

card_in_set('orcish lumberjack', 'CST').
card_original_type('orcish lumberjack'/'CST', 'Creature — Orc').
card_original_text('orcish lumberjack'/'CST', '').
card_image_name('orcish lumberjack'/'CST', 'orcish lumberjack').
card_uid('orcish lumberjack'/'CST', 'CST:Orcish Lumberjack:orcish lumberjack').
card_rarity('orcish lumberjack'/'CST', 'Special').
card_artist('orcish lumberjack'/'CST', 'Dan Frazier').
card_number('orcish lumberjack'/'CST', '210').
card_flavor_text('orcish lumberjack'/'CST', '\"How did I ever let myself get talked into this project?\"\n—Toothlicker Harj, Orcish Captain').

card_in_set('phantasmal fiend', 'CST').
card_original_type('phantasmal fiend'/'CST', 'Creature — Illusion').
card_original_text('phantasmal fiend'/'CST', '').
card_image_name('phantasmal fiend'/'CST', 'phantasmal fiend').
card_uid('phantasmal fiend'/'CST', 'CST:Phantasmal Fiend:phantasmal fiend').
card_rarity('phantasmal fiend'/'CST', 'Special').
card_artist('phantasmal fiend'/'CST', 'Scott Kirschner').
card_number('phantasmal fiend'/'CST', '57a').

card_in_set('plains', 'CST').
card_original_type('plains'/'CST', 'Basic Land — Plains').
card_original_text('plains'/'CST', '').
card_image_name('plains'/'CST', 'plains1').
card_uid('plains'/'CST', 'CST:Plains:plains1').
card_rarity('plains'/'CST', 'Basic Land').
card_artist('plains'/'CST', 'Christopher Rush').
card_number('plains'/'CST', '369').

card_in_set('plains', 'CST').
card_original_type('plains'/'CST', 'Basic Land — Plains').
card_original_text('plains'/'CST', '').
card_image_name('plains'/'CST', 'plains2').
card_uid('plains'/'CST', 'CST:Plains:plains2').
card_rarity('plains'/'CST', 'Basic Land').
card_artist('plains'/'CST', 'Christopher Rush').
card_number('plains'/'CST', '370').

card_in_set('plains', 'CST').
card_original_type('plains'/'CST', 'Basic Land — Plains').
card_original_text('plains'/'CST', '').
card_image_name('plains'/'CST', 'plains3').
card_uid('plains'/'CST', 'CST:Plains:plains3').
card_rarity('plains'/'CST', 'Basic Land').
card_artist('plains'/'CST', 'Christopher Rush').
card_number('plains'/'CST', '371').

card_in_set('portent', 'CST').
card_original_type('portent'/'CST', 'Sorcery').
card_original_text('portent'/'CST', '').
card_image_name('portent'/'CST', 'portent').
card_uid('portent'/'CST', 'CST:Portent:portent').
card_rarity('portent'/'CST', 'Special').
card_artist('portent'/'CST', 'Liz Danforth').
card_number('portent'/'CST', '90').

card_in_set('reinforcements', 'CST').
card_original_type('reinforcements'/'CST', 'Instant').
card_original_text('reinforcements'/'CST', '').
card_image_name('reinforcements'/'CST', 'reinforcements').
card_uid('reinforcements'/'CST', 'CST:Reinforcements:reinforcements').
card_rarity('reinforcements'/'CST', 'Special').
card_artist('reinforcements'/'CST', 'Diana Vick').
card_number('reinforcements'/'CST', '12b').
card_flavor_text('reinforcements'/'CST', '\"Let them send their legions! I will show them that my truth is stronger than their swords.\"\n—General Varchild').

card_in_set('scars of the veteran', 'CST').
card_original_type('scars of the veteran'/'CST', 'Instant').
card_original_text('scars of the veteran'/'CST', '').
card_image_name('scars of the veteran'/'CST', 'scars of the veteran').
card_uid('scars of the veteran'/'CST', 'CST:Scars of the Veteran:scars of the veteran').
card_rarity('scars of the veteran'/'CST', 'Special').
card_artist('scars of the veteran'/'CST', 'Dan Frazier').
card_number('scars of the veteran'/'CST', '16').

card_in_set('skull catapult', 'CST').
card_original_type('skull catapult'/'CST', 'Artifact').
card_original_text('skull catapult'/'CST', '').
card_image_name('skull catapult'/'CST', 'skull catapult').
card_uid('skull catapult'/'CST', 'CST:Skull Catapult:skull catapult').
card_rarity('skull catapult'/'CST', 'Special').
card_artist('skull catapult'/'CST', 'Bryon Wackwitz').
card_number('skull catapult'/'CST', '336').
card_flavor_text('skull catapult'/'CST', '\"Let any who doubt the evil of using the ancient devices look at this infernal machine. What manner of fiend would design such a sadistic device?\"\n—Sorine Relicbane, Soldevi Heretic').

card_in_set('snow devil', 'CST').
card_original_type('snow devil'/'CST', 'Enchantment — Aura').
card_original_text('snow devil'/'CST', '').
card_image_name('snow devil'/'CST', 'snow devil').
card_uid('snow devil'/'CST', 'CST:Snow Devil:snow devil').
card_rarity('snow devil'/'CST', 'Special').
card_artist('snow devil'/'CST', 'Ken Meyer, Jr.').
card_number('snow devil'/'CST', '100').
card_flavor_text('snow devil'/'CST', '\"Give me wings to fly and speed to strike. In return, the glory I earn shall be yours.\"\n—Steinar Icefist, Balduvian Shaman').

card_in_set('soul burn', 'CST').
card_original_type('soul burn'/'CST', 'Sorcery').
card_original_text('soul burn'/'CST', '').
card_image_name('soul burn'/'CST', 'soul burn').
card_uid('soul burn'/'CST', 'CST:Soul Burn:soul burn').
card_rarity('soul burn'/'CST', 'Special').
card_artist('soul burn'/'CST', 'Rob Alexander').
card_number('soul burn'/'CST', '161').

card_in_set('storm elemental', 'CST').
card_original_type('storm elemental'/'CST', 'Creature — Elemental').
card_original_text('storm elemental'/'CST', '').
card_image_name('storm elemental'/'CST', 'storm elemental').
card_uid('storm elemental'/'CST', 'CST:Storm Elemental:storm elemental').
card_rarity('storm elemental'/'CST', 'Special').
card_artist('storm elemental'/'CST', 'John Matson').
card_number('storm elemental'/'CST', '37').

card_in_set('swamp', 'CST').
card_original_type('swamp'/'CST', 'Basic Land — Swamp').
card_original_text('swamp'/'CST', '').
card_image_name('swamp'/'CST', 'swamp1').
card_uid('swamp'/'CST', 'CST:Swamp:swamp1').
card_rarity('swamp'/'CST', 'Basic Land').
card_artist('swamp'/'CST', 'Douglas Shuler').
card_number('swamp'/'CST', '375').

card_in_set('swamp', 'CST').
card_original_type('swamp'/'CST', 'Basic Land — Swamp').
card_original_text('swamp'/'CST', '').
card_image_name('swamp'/'CST', 'swamp2').
card_uid('swamp'/'CST', 'CST:Swamp:swamp2').
card_rarity('swamp'/'CST', 'Basic Land').
card_artist('swamp'/'CST', 'Douglas Shuler').
card_number('swamp'/'CST', '376').

card_in_set('swamp', 'CST').
card_original_type('swamp'/'CST', 'Basic Land — Swamp').
card_original_text('swamp'/'CST', '').
card_image_name('swamp'/'CST', 'swamp3').
card_uid('swamp'/'CST', 'CST:Swamp:swamp3').
card_rarity('swamp'/'CST', 'Basic Land').
card_artist('swamp'/'CST', 'Douglas Shuler').
card_number('swamp'/'CST', '377').

card_in_set('swords to plowshares', 'CST').
card_original_type('swords to plowshares'/'CST', 'Instant').
card_original_text('swords to plowshares'/'CST', '').
card_image_name('swords to plowshares'/'CST', 'swords to plowshares').
card_uid('swords to plowshares'/'CST', 'CST:Swords to Plowshares:swords to plowshares').
card_rarity('swords to plowshares'/'CST', 'Special').
card_artist('swords to plowshares'/'CST', 'Kaja Foglio').
card_number('swords to plowshares'/'CST', '54').
card_flavor_text('swords to plowshares'/'CST', '\"The so-called Barbarians will not respect us for our military might—they will respect us for our honor.\"\n—Lucilde Fiksdotter, Leader of the Order of the White Shield').

card_in_set('tinder wall', 'CST').
card_original_type('tinder wall'/'CST', 'Creature — Plant Wall').
card_original_text('tinder wall'/'CST', '').
card_image_name('tinder wall'/'CST', 'tinder wall').
card_uid('tinder wall'/'CST', 'CST:Tinder Wall:tinder wall').
card_rarity('tinder wall'/'CST', 'Special').
card_artist('tinder wall'/'CST', 'Rick Emond').
card_number('tinder wall'/'CST', '270').

card_in_set('viscerid drone', 'CST').
card_original_type('viscerid drone'/'CST', 'Creature — Homarid Drone').
card_original_text('viscerid drone'/'CST', '').
card_image_name('viscerid drone'/'CST', 'viscerid drone').
card_uid('viscerid drone'/'CST', 'CST:Viscerid Drone:viscerid drone').
card_rarity('viscerid drone'/'CST', 'Special').
card_artist('viscerid drone'/'CST', 'Heather Hudson').
card_number('viscerid drone'/'CST', '42').
card_flavor_text('viscerid drone'/'CST', 'Not all of Terisiare\'s flooding was natural . . . .').

card_in_set('whalebone glider', 'CST').
card_original_type('whalebone glider'/'CST', 'Artifact').
card_original_text('whalebone glider'/'CST', '').
card_image_name('whalebone glider'/'CST', 'whalebone glider').
card_uid('whalebone glider'/'CST', 'CST:Whalebone Glider:whalebone glider').
card_rarity('whalebone glider'/'CST', 'Special').
card_artist('whalebone glider'/'CST', 'Amy Weber').
card_number('whalebone glider'/'CST', '349').
card_flavor_text('whalebone glider'/'CST', '\"It\'s no Ornithopter, but then I\'m no Urza.\"\n—Arcum Dagsson, Soldevi Machinist').

card_in_set('wings of aesthir', 'CST').
card_original_type('wings of aesthir'/'CST', 'Enchantment — Aura').
card_original_text('wings of aesthir'/'CST', '').
card_image_name('wings of aesthir'/'CST', 'wings of aesthir').
card_uid('wings of aesthir'/'CST', 'CST:Wings of Aesthir:wings of aesthir').
card_rarity('wings of aesthir'/'CST', 'Special').
card_artist('wings of aesthir'/'CST', 'Edward P. Beard, Jr.').
card_number('wings of aesthir'/'CST', '305').
card_flavor_text('wings of aesthir'/'CST', '\"For those of courage, even the sky holds no limit.\"\n—Arnjlot Olasson, Sky Mage').

card_in_set('woolly mammoths', 'CST').
card_original_type('woolly mammoths'/'CST', 'Creature — Elephant').
card_original_text('woolly mammoths'/'CST', '').
card_image_name('woolly mammoths'/'CST', 'woolly mammoths').
card_uid('woolly mammoths'/'CST', 'CST:Woolly Mammoths:woolly mammoths').
card_rarity('woolly mammoths'/'CST', 'Special').
card_artist('woolly mammoths'/'CST', 'Dan Frazier').
card_number('woolly mammoths'/'CST', '278').
card_flavor_text('woolly mammoths'/'CST', '\"Mammoths may be good to ride on, but they\'re certainly bad to fall off of!\"\n—Disa the Restless, journal entry').

card_in_set('zuran spellcaster', 'CST').
card_original_type('zuran spellcaster'/'CST', 'Creature — Human Wizard').
card_original_text('zuran spellcaster'/'CST', '').
card_image_name('zuran spellcaster'/'CST', 'zuran spellcaster').
card_uid('zuran spellcaster'/'CST', 'CST:Zuran Spellcaster:zuran spellcaster').
card_rarity('zuran spellcaster'/'CST', 'Special').
card_artist('zuran spellcaster'/'CST', 'Edward P. Beard, Jr.').
card_number('zuran spellcaster'/'CST', '112').
card_flavor_text('zuran spellcaster'/'CST', '\"A mage must be precise as well as potent; cautious, as well as clever.\"\n—Zur the Enchanter').
