% Portal Three Kingdoms

set('PTK').
set_name('PTK', 'Portal Three Kingdoms').
set_release_date('PTK', '1999-05-01').
set_border('PTK', 'white').
set_type('PTK', 'starter').

card_in_set('alert shu infantry', 'PTK').
card_original_type('alert shu infantry'/'PTK', 'Creature — Soldiers').
card_original_text('alert shu infantry'/'PTK', 'Attacking doesn\'t cause Alert Shu Infantry to tap.').
card_first_print('alert shu infantry', 'PTK').
card_image_name('alert shu infantry'/'PTK', 'alert shu infantry').
card_uid('alert shu infantry'/'PTK', 'PTK:Alert Shu Infantry:alert shu infantry').
card_rarity('alert shu infantry'/'PTK', 'Uncommon').
card_artist('alert shu infantry'/'PTK', 'Solomon Au Yeung').
card_number('alert shu infantry'/'PTK', '1').
card_flavor_text('alert shu infantry'/'PTK', 'Kongming\'s predecessor helped Liu Bei win the battle of Fancheng by recognizing and defeating Cao Ren\'s use of the dreaded \"Eight Gold Locks\" formation.').
card_multiverse_id('alert shu infantry'/'PTK', '10487').

card_in_set('ambition\'s cost', 'PTK').
card_original_type('ambition\'s cost'/'PTK', 'Sorcery').
card_original_text('ambition\'s cost'/'PTK', 'Draw three cards. You lose 3 life.').
card_first_print('ambition\'s cost', 'PTK').
card_image_name('ambition\'s cost'/'PTK', 'ambition\'s cost').
card_uid('ambition\'s cost'/'PTK', 'PTK:Ambition\'s Cost:ambition\'s cost').
card_rarity('ambition\'s cost'/'PTK', 'Rare').
card_artist('ambition\'s cost'/'PTK', 'Junko Taguchi').
card_number('ambition\'s cost'/'PTK', '67').
card_flavor_text('ambition\'s cost'/'PTK', '\"When you give offense to heaven, to whom can you pray?\"\n—Cao Cao, quoting Confucius').
card_multiverse_id('ambition\'s cost'/'PTK', '10493').

card_in_set('balance of power', 'PTK').
card_original_type('balance of power'/'PTK', 'Sorcery').
card_original_text('balance of power'/'PTK', 'If you have fewer cards in your hand than your opponent does, you draw until you have the same number. (When you play Balance of Power it doesn\'t count as in your hand.)').
card_image_name('balance of power'/'PTK', 'balance of power').
card_uid('balance of power'/'PTK', 'PTK:Balance of Power:balance of power').
card_rarity('balance of power'/'PTK', 'Rare').
card_artist('balance of power'/'PTK', 'Quan Xuejun').
card_number('balance of power'/'PTK', '34').
card_multiverse_id('balance of power'/'PTK', '10580').

card_in_set('barbarian general', 'PTK').
card_original_type('barbarian general'/'PTK', 'Creature — Soldier').
card_original_text('barbarian general'/'PTK', 'Horsemanship').
card_first_print('barbarian general', 'PTK').
card_image_name('barbarian general'/'PTK', 'barbarian general').
card_uid('barbarian general'/'PTK', 'PTK:Barbarian General:barbarian general').
card_rarity('barbarian general'/'PTK', 'Uncommon').
card_artist('barbarian general'/'PTK', 'Kuang Sheng').
card_number('barbarian general'/'PTK', '100').
card_flavor_text('barbarian general'/'PTK', '\"Barbarian tribes with their rulers are inferior to Chinese states without them.\"\n—Confucius, The Analects (trans. Lau)').
card_multiverse_id('barbarian general'/'PTK', '10574').

card_in_set('barbarian horde', 'PTK').
card_original_type('barbarian horde'/'PTK', 'Creature — Soldiers').
card_original_text('barbarian horde'/'PTK', '').
card_first_print('barbarian horde', 'PTK').
card_image_name('barbarian horde'/'PTK', 'barbarian horde').
card_uid('barbarian horde'/'PTK', 'PTK:Barbarian Horde:barbarian horde').
card_rarity('barbarian horde'/'PTK', 'Common').
card_artist('barbarian horde'/'PTK', 'Kuang Sheng').
card_number('barbarian horde'/'PTK', '101').
card_flavor_text('barbarian horde'/'PTK', 'Only twenty years after the Sima clan united the empire, invading barbarians divided it again.').
card_multiverse_id('barbarian horde'/'PTK', '10578').

card_in_set('blaze', 'PTK').
card_original_type('blaze'/'PTK', 'Sorcery').
card_original_text('blaze'/'PTK', 'Blaze deals X damage to any one creature or player.').
card_image_name('blaze'/'PTK', 'blaze').
card_uid('blaze'/'PTK', 'PTK:Blaze:blaze').
card_rarity('blaze'/'PTK', 'Uncommon').
card_artist('blaze'/'PTK', 'Qu Xin').
card_number('blaze'/'PTK', '102').
card_flavor_text('blaze'/'PTK', '\"Spreading flames illumined cloud and sea:\nCao Cao went down; \'twas Zhou Yu\'s victory.\"').
card_multiverse_id('blaze'/'PTK', '10507').

card_in_set('borrowing 100,000 arrows', 'PTK').
card_original_type('borrowing 100,000 arrows'/'PTK', 'Sorcery').
card_original_text('borrowing 100,000 arrows'/'PTK', 'For each tapped creature your opponent has in play, you draw a card.').
card_first_print('borrowing 100,000 arrows', 'PTK').
card_image_name('borrowing 100,000 arrows'/'PTK', 'borrowing 100,000 arrows').
card_uid('borrowing 100,000 arrows'/'PTK', 'PTK:Borrowing 100,000 Arrows:borrowing 100,000 arrows').
card_rarity('borrowing 100,000 arrows'/'PTK', 'Uncommon').
card_artist('borrowing 100,000 arrows'/'PTK', 'Song Shikai').
card_number('borrowing 100,000 arrows'/'PTK', '35').
card_flavor_text('borrowing 100,000 arrows'/'PTK', 'Kongming and Lu Su tricked Wei troops into shooting over 100,000 arrows at them to later use against the Wei at Red Cliffs.').
card_multiverse_id('borrowing 100,000 arrows'/'PTK', '10620').

card_in_set('borrowing the east wind', 'PTK').
card_original_type('borrowing the east wind'/'PTK', 'Sorcery').
card_original_text('borrowing the east wind'/'PTK', 'Borrowing the East Wind deals X damage to each player and each creature with horsemanship. (This includes you and your creatures with horsemanship.)').
card_first_print('borrowing the east wind', 'PTK').
card_image_name('borrowing the east wind'/'PTK', 'borrowing the east wind').
card_uid('borrowing the east wind'/'PTK', 'PTK:Borrowing the East Wind:borrowing the east wind').
card_rarity('borrowing the east wind'/'PTK', 'Rare').
card_artist('borrowing the east wind'/'PTK', 'Gao Yan').
card_number('borrowing the east wind'/'PTK', '133').
card_multiverse_id('borrowing the east wind'/'PTK', '10549').

card_in_set('brilliant plan', 'PTK').
card_original_type('brilliant plan'/'PTK', 'Sorcery').
card_original_text('brilliant plan'/'PTK', 'Draw three cards.').
card_first_print('brilliant plan', 'PTK').
card_image_name('brilliant plan'/'PTK', 'brilliant plan').
card_uid('brilliant plan'/'PTK', 'PTK:Brilliant Plan:brilliant plan').
card_rarity('brilliant plan'/'PTK', 'Uncommon').
card_artist('brilliant plan'/'PTK', 'Song Shikai').
card_number('brilliant plan'/'PTK', '36').
card_flavor_text('brilliant plan'/'PTK', 'At Red Cliffs, Kongming and Zhou Yu each wrote his plan for defeating the Wei on the palm of his hand. They laughed as they both revealed the same word, \"Fire.\"').
card_multiverse_id('brilliant plan'/'PTK', '10623').

card_in_set('broken dam', 'PTK').
card_original_type('broken dam'/'PTK', 'Sorcery').
card_original_text('broken dam'/'PTK', 'Tap any one or two creatures without horsemanship. (Tapped creatures can\'t block.)').
card_first_print('broken dam', 'PTK').
card_image_name('broken dam'/'PTK', 'broken dam').
card_uid('broken dam'/'PTK', 'PTK:Broken Dam:broken dam').
card_rarity('broken dam'/'PTK', 'Common').
card_artist('broken dam'/'PTK', 'Jiang Zhuqing').
card_number('broken dam'/'PTK', '37').
card_flavor_text('broken dam'/'PTK', 'Using nature to their advantage, wise Three Kingdoms generals often let dammed rivers loose to destroy their enemies.').
card_multiverse_id('broken dam'/'PTK', '10621').

card_in_set('burning fields', 'PTK').
card_original_type('burning fields'/'PTK', 'Sorcery').
card_original_text('burning fields'/'PTK', 'Burning Fields deals 5 damage to your opponent.').
card_first_print('burning fields', 'PTK').
card_image_name('burning fields'/'PTK', 'burning fields').
card_uid('burning fields'/'PTK', 'PTK:Burning Fields:burning fields').
card_rarity('burning fields'/'PTK', 'Common').
card_artist('burning fields'/'PTK', 'Yang Hong').
card_number('burning fields'/'PTK', '103').
card_flavor_text('burning fields'/'PTK', '\"In raiding and plundering, be like fire, in immovability like a mountain.\"\n—Sun Tzu, Art of War (trans. Giles)').
card_multiverse_id('burning fields'/'PTK', '10555').

card_in_set('burning of xinye', 'PTK').
card_original_type('burning of xinye'/'PTK', 'Sorcery').
card_original_text('burning of xinye'/'PTK', 'You destroy four of your lands and your opponent destroys four of his or her lands. Then Burning of Xinye deals 4 damage to each creature. (This includes your creatures.)').
card_first_print('burning of xinye', 'PTK').
card_image_name('burning of xinye'/'PTK', 'burning of xinye').
card_uid('burning of xinye'/'PTK', 'PTK:Burning of Xinye:burning of xinye').
card_rarity('burning of xinye'/'PTK', 'Rare').
card_artist('burning of xinye'/'PTK', 'Yang Hong').
card_number('burning of xinye'/'PTK', '104').
card_multiverse_id('burning of xinye'/'PTK', '10637').

card_in_set('cao cao, lord of wei', 'PTK').
card_original_type('cao cao, lord of wei'/'PTK', 'Creature — Legend').
card_original_text('cao cao, lord of wei'/'PTK', 'On your turn, before you attack, you may tap Cao Cao to force your opponent to choose and discard two cards from his or her hand. (If your opponent has only one card, he or she discards it.)').
card_first_print('cao cao, lord of wei', 'PTK').
card_image_name('cao cao, lord of wei'/'PTK', 'cao cao, lord of wei').
card_uid('cao cao, lord of wei'/'PTK', 'PTK:Cao Cao, Lord of Wei:cao cao, lord of wei').
card_rarity('cao cao, lord of wei'/'PTK', 'Rare').
card_artist('cao cao, lord of wei'/'PTK', 'Gao Jianzhang').
card_number('cao cao, lord of wei'/'PTK', '68').
card_multiverse_id('cao cao, lord of wei'/'PTK', '10548').

card_in_set('cao ren, wei commander', 'PTK').
card_original_type('cao ren, wei commander'/'PTK', 'Creature — Legend').
card_original_text('cao ren, wei commander'/'PTK', 'Horsemanship\nWhen Cao Ren comes into play, you lose 3 life.').
card_first_print('cao ren, wei commander', 'PTK').
card_image_name('cao ren, wei commander'/'PTK', 'cao ren, wei commander').
card_uid('cao ren, wei commander'/'PTK', 'PTK:Cao Ren, Wei Commander:cao ren, wei commander').
card_rarity('cao ren, wei commander'/'PTK', 'Rare').
card_artist('cao ren, wei commander'/'PTK', 'Junko Taguchi').
card_number('cao ren, wei commander'/'PTK', '69').
card_flavor_text('cao ren, wei commander'/'PTK', 'Cao Cao\'s cousin, Cao Ren was known throughout the three kingdoms as the fiercest of warriors.').
card_multiverse_id('cao ren, wei commander'/'PTK', '10712').

card_in_set('capture of jingzhou', 'PTK').
card_original_type('capture of jingzhou'/'PTK', 'Sorcery').
card_original_text('capture of jingzhou'/'PTK', 'You take another turn after this one.').
card_first_print('capture of jingzhou', 'PTK').
card_image_name('capture of jingzhou'/'PTK', 'capture of jingzhou').
card_uid('capture of jingzhou'/'PTK', 'PTK:Capture of Jingzhou:capture of jingzhou').
card_rarity('capture of jingzhou'/'PTK', 'Rare').
card_artist('capture of jingzhou'/'PTK', 'Jack Wei').
card_number('capture of jingzhou'/'PTK', '38').
card_flavor_text('capture of jingzhou'/'PTK', 'The province of Jingzhou was the fulcrum of the three kingdoms. At separate times it was coveted and controlled by the Wu, the Shu, and the Wei.').
card_multiverse_id('capture of jingzhou'/'PTK', '10619').

card_in_set('champion\'s victory', 'PTK').
card_original_type('champion\'s victory'/'PTK', 'Sorcery').
card_original_text('champion\'s victory'/'PTK', 'Play Champion\'s Victory only after you\'re attacked, before you declare blockers.\nReturn any one attacking creature to its owner\'s hand.').
card_first_print('champion\'s victory', 'PTK').
card_image_name('champion\'s victory'/'PTK', 'champion\'s victory').
card_uid('champion\'s victory'/'PTK', 'PTK:Champion\'s Victory:champion\'s victory').
card_rarity('champion\'s victory'/'PTK', 'Uncommon').
card_artist('champion\'s victory'/'PTK', 'Hong Yan').
card_number('champion\'s victory'/'PTK', '39').
card_multiverse_id('champion\'s victory'/'PTK', '10593').

card_in_set('coercion', 'PTK').
card_original_type('coercion'/'PTK', 'Sorcery').
card_original_text('coercion'/'PTK', 'Look at your opponent\'s hand and choose a card from it. Your opponent discards that card.').
card_image_name('coercion'/'PTK', 'coercion').
card_uid('coercion'/'PTK', 'PTK:Coercion:coercion').
card_rarity('coercion'/'PTK', 'Uncommon').
card_artist('coercion'/'PTK', 'Kang Yu').
card_number('coercion'/'PTK', '70').
card_flavor_text('coercion'/'PTK', 'Though he held the title of prime minister, Cao Cao used the emperor\'s name to exert his own influence on the empire.').
card_multiverse_id('coercion'/'PTK', '10514').

card_in_set('control of the court', 'PTK').
card_original_type('control of the court'/'PTK', 'Sorcery').
card_original_text('control of the court'/'PTK', 'Draw four cards and put them into your hand. Then discard three cards at random from your hand.').
card_first_print('control of the court', 'PTK').
card_image_name('control of the court'/'PTK', 'control of the court').
card_uid('control of the court'/'PTK', 'PTK:Control of the Court:control of the court').
card_rarity('control of the court'/'PTK', 'Uncommon').
card_artist('control of the court'/'PTK', 'Li Yousong').
card_number('control of the court'/'PTK', '105').
card_flavor_text('control of the court'/'PTK', '\"Power-hungry eunuchs, the curse of the dynasty, have thrown the masses of the people into the depths of misery.\"').
card_multiverse_id('control of the court'/'PTK', '10538').

card_in_set('corrupt court official', 'PTK').
card_original_type('corrupt court official'/'PTK', 'Creature — Advisor').
card_original_text('corrupt court official'/'PTK', 'When Corrupt Official comes into play, your opponent chooses and discards a card from his or her hand. (Ignore this effect if your opponent doesn\'t have any cards.)').
card_first_print('corrupt court official', 'PTK').
card_image_name('corrupt court official'/'PTK', 'corrupt court official').
card_uid('corrupt court official'/'PTK', 'PTK:Corrupt Court Official:corrupt court official').
card_rarity('corrupt court official'/'PTK', 'Uncommon').
card_artist('corrupt court official'/'PTK', 'Li Yousong').
card_number('corrupt court official'/'PTK', '71').
card_multiverse_id('corrupt court official'/'PTK', '10595').

card_in_set('corrupt eunuchs', 'PTK').
card_original_type('corrupt eunuchs'/'PTK', 'Creature — Advisor').
card_original_text('corrupt eunuchs'/'PTK', 'When Corrupt Eunuchs comes into play, it deals 2 damage to any one creature. (If you\'re the only one with creatures, deal the damage to one of them.)').
card_first_print('corrupt eunuchs', 'PTK').
card_image_name('corrupt eunuchs'/'PTK', 'corrupt eunuchs').
card_uid('corrupt eunuchs'/'PTK', 'PTK:Corrupt Eunuchs:corrupt eunuchs').
card_rarity('corrupt eunuchs'/'PTK', 'Uncommon').
card_artist('corrupt eunuchs'/'PTK', 'Li Yousong').
card_number('corrupt eunuchs'/'PTK', '106').
card_multiverse_id('corrupt eunuchs'/'PTK', '10734').

card_in_set('council of advisors', 'PTK').
card_original_type('council of advisors'/'PTK', 'Creature — Advisors').
card_original_text('council of advisors'/'PTK', 'When Council of Advisors comes into play, draw a card.').
card_first_print('council of advisors', 'PTK').
card_image_name('council of advisors'/'PTK', 'council of advisors').
card_uid('council of advisors'/'PTK', 'PTK:Council of Advisors:council of advisors').
card_rarity('council of advisors'/'PTK', 'Uncommon').
card_artist('council of advisors'/'PTK', 'Liu Shangying').
card_number('council of advisors'/'PTK', '40').
card_flavor_text('council of advisors'/'PTK', 'Sun Quan\'s long and successful rule in the years 199 to 251 was due to his ability to choose and foster talented advisors and generals.').
card_multiverse_id('council of advisors'/'PTK', '10601').

card_in_set('counterintelligence', 'PTK').
card_original_type('counterintelligence'/'PTK', 'Sorcery').
card_original_text('counterintelligence'/'PTK', 'Return any one or two creatures to their owner\'s hand.').
card_first_print('counterintelligence', 'PTK').
card_image_name('counterintelligence'/'PTK', 'counterintelligence').
card_uid('counterintelligence'/'PTK', 'PTK:Counterintelligence:counterintelligence').
card_rarity('counterintelligence'/'PTK', 'Uncommon').
card_artist('counterintelligence'/'PTK', 'Wang Feng').
card_number('counterintelligence'/'PTK', '41').
card_flavor_text('counterintelligence'/'PTK', 'Before the battle of Red Cliffs, a supposedly sleeping Zhou Yu allowed his old friend, a Wei advisor, to steal a planted letter forged as if from Wei\'s two best admirals.').
card_multiverse_id('counterintelligence'/'PTK', '10628').

card_in_set('cunning advisor', 'PTK').
card_original_type('cunning advisor'/'PTK', 'Creature — Advisor').
card_original_text('cunning advisor'/'PTK', 'On your turn, before you attack, you may tap Cunning Advisor to force your opponent to choose and discard a card from his or her hand.').
card_first_print('cunning advisor', 'PTK').
card_image_name('cunning advisor'/'PTK', 'cunning advisor').
card_uid('cunning advisor'/'PTK', 'PTK:Cunning Advisor:cunning advisor').
card_rarity('cunning advisor'/'PTK', 'Uncommon').
card_artist('cunning advisor'/'PTK', 'Gao Jianzhang').
card_number('cunning advisor'/'PTK', '72').
card_multiverse_id('cunning advisor'/'PTK', '10533').

card_in_set('deception', 'PTK').
card_original_type('deception'/'PTK', 'Sorcery').
card_original_text('deception'/'PTK', 'Your opponent chooses and discards two cards from his or her hand. (If your opponent has only one card, he or she discards it.)').
card_first_print('deception', 'PTK').
card_image_name('deception'/'PTK', 'deception').
card_uid('deception'/'PTK', 'PTK:Deception:deception').
card_rarity('deception'/'PTK', 'Common').
card_artist('deception'/'PTK', 'Wang Feng').
card_number('deception'/'PTK', '73').
card_flavor_text('deception'/'PTK', 'Believing they wrote the forged letter that had been stolen from Zhou Yu, Cao Cao rashly executed his two best admirals for treason.').
card_multiverse_id('deception'/'PTK', '10560').

card_in_set('desert sandstorm', 'PTK').
card_original_type('desert sandstorm'/'PTK', 'Sorcery').
card_original_text('desert sandstorm'/'PTK', 'Desert Sandstorm deals 1 damage to each creature. (This includes your creatures.)').
card_first_print('desert sandstorm', 'PTK').
card_image_name('desert sandstorm'/'PTK', 'desert sandstorm').
card_uid('desert sandstorm'/'PTK', 'PTK:Desert Sandstorm:desert sandstorm').
card_rarity('desert sandstorm'/'PTK', 'Common').
card_artist('desert sandstorm'/'PTK', 'Xu Tan').
card_number('desert sandstorm'/'PTK', '107').
card_flavor_text('desert sandstorm'/'PTK', 'While pursuing the remnants of Yuan Shao\'s forces into the Wuhan Desert, Cao Cao was temporarily turned back by a fierce sandstorm.').
card_multiverse_id('desert sandstorm'/'PTK', '10626').

card_in_set('desperate charge', 'PTK').
card_original_type('desperate charge'/'PTK', 'Sorcery').
card_original_text('desperate charge'/'PTK', 'All your creatures get +2/+0 until the end of the turn.').
card_first_print('desperate charge', 'PTK').
card_image_name('desperate charge'/'PTK', 'desperate charge').
card_uid('desperate charge'/'PTK', 'PTK:Desperate Charge:desperate charge').
card_rarity('desperate charge'/'PTK', 'Uncommon').
card_artist('desperate charge'/'PTK', 'Chen Weidong').
card_number('desperate charge'/'PTK', '74').
card_flavor_text('desperate charge'/'PTK', '\"Lieutenants dishonored, corpses carted home; The general raises troops again to take revenge.\"').
card_multiverse_id('desperate charge'/'PTK', '10512').

card_in_set('diaochan, artful beauty', 'PTK').
card_original_type('diaochan, artful beauty'/'PTK', 'Creature — Legend').
card_original_text('diaochan, artful beauty'/'PTK', 'On your turn, before you attack, you may tap Diaochan to destroy any one creature. Then, your opponent destroys any one creature of his or her choice.').
card_first_print('diaochan, artful beauty', 'PTK').
card_image_name('diaochan, artful beauty'/'PTK', 'diaochan, artful beauty').
card_uid('diaochan, artful beauty'/'PTK', 'PTK:Diaochan, Artful Beauty:diaochan, artful beauty').
card_rarity('diaochan, artful beauty'/'PTK', 'Rare').
card_artist('diaochan, artful beauty'/'PTK', 'Miao Aili').
card_number('diaochan, artful beauty'/'PTK', '108').
card_multiverse_id('diaochan, artful beauty'/'PTK', '10544').

card_in_set('dong zhou, the tyrant', 'PTK').
card_original_type('dong zhou, the tyrant'/'PTK', 'Creature — Legend').
card_original_text('dong zhou, the tyrant'/'PTK', 'When Dong Zhou comes into play, choose one of your opponent\'s creatures. That creature deals damage to him or her equal to its power. (Ignore this effect if your opponent doesn\'t have any creatures in play.)').
card_first_print('dong zhou, the tyrant', 'PTK').
card_image_name('dong zhou, the tyrant'/'PTK', 'dong zhou, the tyrant').
card_uid('dong zhou, the tyrant'/'PTK', 'PTK:Dong Zhou, the Tyrant:dong zhou, the tyrant').
card_rarity('dong zhou, the tyrant'/'PTK', 'Rare').
card_artist('dong zhou, the tyrant'/'PTK', 'Junichi Inoue').
card_number('dong zhou, the tyrant'/'PTK', '109').
card_multiverse_id('dong zhou, the tyrant'/'PTK', '10730').

card_in_set('eightfold maze', 'PTK').
card_original_type('eightfold maze'/'PTK', 'Sorcery').
card_original_text('eightfold maze'/'PTK', 'Play Eightfold Maze only after you\'re attacked, before you declare blockers.\nDestroy any one attacking creature.').
card_first_print('eightfold maze', 'PTK').
card_image_name('eightfold maze'/'PTK', 'eightfold maze').
card_uid('eightfold maze'/'PTK', 'PTK:Eightfold Maze:eightfold maze').
card_rarity('eightfold maze'/'PTK', 'Rare').
card_artist('eightfold maze'/'PTK', 'Shang Huitong').
card_number('eightfold maze'/'PTK', '2').
card_multiverse_id('eightfold maze'/'PTK', '10553').

card_in_set('empty city ruse', 'PTK').
card_original_type('empty city ruse'/'PTK', 'Sorcery').
card_original_text('empty city ruse'/'PTK', 'Your opponent can\'t attack on his or her next turn.').
card_first_print('empty city ruse', 'PTK').
card_image_name('empty city ruse'/'PTK', 'empty city ruse').
card_uid('empty city ruse'/'PTK', 'PTK:Empty City Ruse:empty city ruse').
card_rarity('empty city ruse'/'PTK', 'Uncommon').
card_artist('empty city ruse'/'PTK', 'Qu Xin').
card_number('empty city ruse'/'PTK', '3').
card_flavor_text('empty city ruse'/'PTK', 'Out of time and options, Kongming was forced to bluff at Xicheng. He tricked an army of 150,000 Wei by leaving the city gates open and calmly playing the zither.').
card_multiverse_id('empty city ruse'/'PTK', '10504').

card_in_set('eunuchs\' intrigues', 'PTK').
card_original_type('eunuchs\' intrigues'/'PTK', 'Sorcery').
card_original_text('eunuchs\' intrigues'/'PTK', 'Your opponent chooses one of his or her creatures. Only that creature can block this turn.').
card_first_print('eunuchs\' intrigues', 'PTK').
card_image_name('eunuchs\' intrigues'/'PTK', 'eunuchs\' intrigues').
card_uid('eunuchs\' intrigues'/'PTK', 'PTK:Eunuchs\' Intrigues:eunuchs\' intrigues').
card_rarity('eunuchs\' intrigues'/'PTK', 'Uncommon').
card_artist('eunuchs\' intrigues'/'PTK', 'Li Yousong').
card_number('eunuchs\' intrigues'/'PTK', '110').
card_flavor_text('eunuchs\' intrigues'/'PTK', 'Taking control of powerful court positions one by one, eunuchs eventually brought on the fall of the Han dynasty, leading to the chaos of the Three Kingdoms.').
card_multiverse_id('eunuchs\' intrigues'/'PTK', '10543').

card_in_set('exhaustion', 'PTK').
card_original_type('exhaustion'/'PTK', 'Sorcery').
card_original_text('exhaustion'/'PTK', 'At the beginning of your opponent\'s next turn, he or she skips untapping creatures and lands.').
card_image_name('exhaustion'/'PTK', 'exhaustion').
card_uid('exhaustion'/'PTK', 'PTK:Exhaustion:exhaustion').
card_rarity('exhaustion'/'PTK', 'Rare').
card_artist('exhaustion'/'PTK', 'Qu Xin').
card_number('exhaustion'/'PTK', '42').
card_flavor_text('exhaustion'/'PTK', 'Starving and worn, Cao Cao escaped from the battle of Red Cliffs with only 27 of his original 800,000 men.').
card_multiverse_id('exhaustion'/'PTK', '10527').

card_in_set('extinguish', 'PTK').
card_original_type('extinguish'/'PTK', 'Sorcery').
card_original_text('extinguish'/'PTK', 'Play Extinguish only in response to another player playing a sorcery.\nThat sorcery has no effect, and that player puts it into his or her graveyard.').
card_image_name('extinguish'/'PTK', 'extinguish').
card_uid('extinguish'/'PTK', 'PTK:Extinguish:extinguish').
card_rarity('extinguish'/'PTK', 'Common').
card_artist('extinguish'/'PTK', 'Ding Songjian').
card_number('extinguish'/'PTK', '43').
card_multiverse_id('extinguish'/'PTK', '10528').

card_in_set('false defeat', 'PTK').
card_original_type('false defeat'/'PTK', 'Sorcery').
card_original_text('false defeat'/'PTK', 'Take any one creature card from your graveyard and put that creature into play.').
card_first_print('false defeat', 'PTK').
card_image_name('false defeat'/'PTK', 'false defeat').
card_uid('false defeat'/'PTK', 'PTK:False Defeat:false defeat').
card_rarity('false defeat'/'PTK', 'Common').
card_artist('false defeat'/'PTK', 'Li Wang').
card_number('false defeat'/'PTK', '4').
card_flavor_text('false defeat'/'PTK', '\"All warfare is based on deception.\"\n—Sun Tzu, Art of War (trans. Giles)').
card_multiverse_id('false defeat'/'PTK', '10509').

card_in_set('false mourning', 'PTK').
card_original_type('false mourning'/'PTK', 'Sorcery').
card_original_text('false mourning'/'PTK', 'Take any one card from your graveyard and put that card on the top of your library.').
card_first_print('false mourning', 'PTK').
card_image_name('false mourning'/'PTK', 'false mourning').
card_uid('false mourning'/'PTK', 'PTK:False Mourning:false mourning').
card_rarity('false mourning'/'PTK', 'Uncommon').
card_artist('false mourning'/'PTK', 'Koji').
card_number('false mourning'/'PTK', '134').
card_flavor_text('false mourning'/'PTK', 'Zhou Yu, Sun Ce, and other famous generals feigned their deaths in order to later surprise their opponents.').
card_multiverse_id('false mourning'/'PTK', '10599').

card_in_set('famine', 'PTK').
card_original_type('famine'/'PTK', 'Sorcery').
card_original_text('famine'/'PTK', 'Famine deals 3 damage to each creature and player. (This includes your creatures and you.)').
card_first_print('famine', 'PTK').
card_image_name('famine'/'PTK', 'famine').
card_uid('famine'/'PTK', 'PTK:Famine:famine').
card_rarity('famine'/'PTK', 'Uncommon').
card_artist('famine'/'PTK', 'Sun Nan').
card_number('famine'/'PTK', '75').
card_flavor_text('famine'/'PTK', '\"But it was a year of dearth. People were reduced to eating leaves of jujube trees. Corpses were seen everywhere in the countryside.\"').
card_multiverse_id('famine'/'PTK', '10518').

card_in_set('fire ambush', 'PTK').
card_original_type('fire ambush'/'PTK', 'Sorcery').
card_original_text('fire ambush'/'PTK', 'Fire Ambush deals 3 damage to any one creature or player.').
card_first_print('fire ambush', 'PTK').
card_image_name('fire ambush'/'PTK', 'fire ambush').
card_uid('fire ambush'/'PTK', 'PTK:Fire Ambush:fire ambush').
card_rarity('fire ambush'/'PTK', 'Common').
card_artist('fire ambush'/'PTK', 'Tang Xiaogu').
card_number('fire ambush'/'PTK', '111').
card_flavor_text('fire ambush'/'PTK', '\"With fire he broke the battle at Bowang . . . . Striking fear deep into Cao Cao\'s soul, thus Kongming scored a coup at his debut.\"').
card_multiverse_id('fire ambush'/'PTK', '10632').

card_in_set('fire bowman', 'PTK').
card_original_type('fire bowman'/'PTK', 'Creature — Soldier').
card_original_text('fire bowman'/'PTK', 'On your turn, before you attack, you may destroy Fire Bowman to have it deal 1 damage to any one creature or player.').
card_first_print('fire bowman', 'PTK').
card_image_name('fire bowman'/'PTK', 'fire bowman').
card_uid('fire bowman'/'PTK', 'PTK:Fire Bowman:fire bowman').
card_rarity('fire bowman'/'PTK', 'Uncommon').
card_artist('fire bowman'/'PTK', 'Cai Tingting').
card_number('fire bowman'/'PTK', '112').
card_multiverse_id('fire bowman'/'PTK', '10535').

card_in_set('flanking troops', 'PTK').
card_original_type('flanking troops'/'PTK', 'Creature — Soldiers').
card_original_text('flanking troops'/'PTK', 'When Flanking Troops attacks, you may tap any one creature. (Tapped creatures can\'t block.)').
card_first_print('flanking troops', 'PTK').
card_image_name('flanking troops'/'PTK', 'flanking troops').
card_uid('flanking troops'/'PTK', 'PTK:Flanking Troops:flanking troops').
card_rarity('flanking troops'/'PTK', 'Uncommon').
card_artist('flanking troops'/'PTK', 'Li Wang').
card_number('flanking troops'/'PTK', '5').
card_flavor_text('flanking troops'/'PTK', 'Following the battle of Redcliffs, both Liu Bei and Sun Quan coveted the province of Jingzhou. After Sun Quan\'s troops failed to capture it, Liu Bei\'s succeeded.').
card_multiverse_id('flanking troops'/'PTK', '10486').

card_in_set('forced retreat', 'PTK').
card_original_type('forced retreat'/'PTK', 'Sorcery').
card_original_text('forced retreat'/'PTK', 'Return any one creature from play to the top of its owner\'s library.').
card_first_print('forced retreat', 'PTK').
card_image_name('forced retreat'/'PTK', 'forced retreat').
card_uid('forced retreat'/'PTK', 'PTK:Forced Retreat:forced retreat').
card_rarity('forced retreat'/'PTK', 'Common').
card_artist('forced retreat'/'PTK', 'Huang Qishi').
card_number('forced retreat'/'PTK', '44').
card_flavor_text('forced retreat'/'PTK', '\"Leadership, not numbers, determines victory.\"\n—A Wu commander, before his 5,000 troops forced 15,000 Wei troops to retreat from Ruxu').
card_multiverse_id('forced retreat'/'PTK', '10622').

card_in_set('forest', 'PTK').
card_original_type('forest'/'PTK', 'Land').
card_original_text('forest'/'PTK', 'G').
card_image_name('forest'/'PTK', 'forest1').
card_uid('forest'/'PTK', 'PTK:Forest:forest1').
card_rarity('forest'/'PTK', 'Basic Land').
card_artist('forest'/'PTK', 'Ji Yong').
card_number('forest'/'PTK', '178').
card_multiverse_id('forest'/'PTK', '10532').

card_in_set('forest', 'PTK').
card_original_type('forest'/'PTK', 'Land').
card_original_text('forest'/'PTK', 'G').
card_image_name('forest'/'PTK', 'forest2').
card_uid('forest'/'PTK', 'PTK:Forest:forest2').
card_rarity('forest'/'PTK', 'Basic Land').
card_artist('forest'/'PTK', 'Ji Yong').
card_number('forest'/'PTK', '179').
card_multiverse_id('forest'/'PTK', '10639').

card_in_set('forest', 'PTK').
card_original_type('forest'/'PTK', 'Land').
card_original_text('forest'/'PTK', 'G').
card_image_name('forest'/'PTK', 'forest3').
card_uid('forest'/'PTK', 'PTK:Forest:forest3').
card_rarity('forest'/'PTK', 'Basic Land').
card_artist('forest'/'PTK', 'Ji Yong').
card_number('forest'/'PTK', '180').
card_multiverse_id('forest'/'PTK', '10640').

card_in_set('forest bear', 'PTK').
card_original_type('forest bear'/'PTK', 'Creature — Bear').
card_original_text('forest bear'/'PTK', '').
card_first_print('forest bear', 'PTK').
card_image_name('forest bear'/'PTK', 'forest bear').
card_uid('forest bear'/'PTK', 'PTK:Forest Bear:forest bear').
card_rarity('forest bear'/'PTK', 'Common').
card_artist('forest bear'/'PTK', 'Wang Yuqun').
card_number('forest bear'/'PTK', '135').
card_flavor_text('forest bear'/'PTK', 'Fish and bear paws—one can\'t have both.\n—Chinese idiom meaning\n\"you can\'t have it both ways\"').
card_multiverse_id('forest bear'/'PTK', '10505').

card_in_set('ghostly visit', 'PTK').
card_original_type('ghostly visit'/'PTK', 'Sorcery').
card_original_text('ghostly visit'/'PTK', 'Destroy any one creature that isn\'t black.').
card_first_print('ghostly visit', 'PTK').
card_image_name('ghostly visit'/'PTK', 'ghostly visit').
card_uid('ghostly visit'/'PTK', 'PTK:Ghostly Visit:ghostly visit').
card_rarity('ghostly visit'/'PTK', 'Common').
card_artist('ghostly visit'/'PTK', 'Kang Yu').
card_number('ghostly visit'/'PTK', '76').
card_flavor_text('ghostly visit'/'PTK', 'Cao Cao was haunted to death by the ghosts of the empresses and other high courtiers he had murdered.').
card_multiverse_id('ghostly visit'/'PTK', '10546').

card_in_set('guan yu\'s 1,000-li march', 'PTK').
card_original_type('guan yu\'s 1,000-li march'/'PTK', 'Sorcery').
card_original_text('guan yu\'s 1,000-li march'/'PTK', 'Destroy all tapped creatures. (This includes your tapped creatures.)').
card_first_print('guan yu\'s 1,000-li march', 'PTK').
card_image_name('guan yu\'s 1,000-li march'/'PTK', 'guan yu\'s 1,000-li march').
card_uid('guan yu\'s 1,000-li march'/'PTK', 'PTK:Guan Yu\'s 1,000-Li March:guan yu\'s 1,000-li march').
card_rarity('guan yu\'s 1,000-li march'/'PTK', 'Rare').
card_artist('guan yu\'s 1,000-li march'/'PTK', 'Yang Guangmai').
card_number('guan yu\'s 1,000-li march'/'PTK', '7').
card_flavor_text('guan yu\'s 1,000-li march'/'PTK', '\"He Guan Yu covered the ground on a thousand-li horse; / With dragon blade he took each pass by force.\"').
card_multiverse_id('guan yu\'s 1,000-li march'/'PTK', '10597').

card_in_set('guan yu, sainted warrior', 'PTK').
card_original_type('guan yu, sainted warrior'/'PTK', 'Creature — Legend').
card_original_text('guan yu, sainted warrior'/'PTK', 'Horsemanship\nWhen Guan Yu is put into your graveyard from play, you may shuffle Guan Yu into your library.').
card_first_print('guan yu, sainted warrior', 'PTK').
card_image_name('guan yu, sainted warrior'/'PTK', 'guan yu, sainted warrior').
card_uid('guan yu, sainted warrior'/'PTK', 'PTK:Guan Yu, Sainted Warrior:guan yu, sainted warrior').
card_rarity('guan yu, sainted warrior'/'PTK', 'Rare').
card_artist('guan yu, sainted warrior'/'PTK', 'Zhao Dafu').
card_number('guan yu, sainted warrior'/'PTK', '6').
card_multiverse_id('guan yu, sainted warrior'/'PTK', '10494').

card_in_set('heavy fog', 'PTK').
card_original_type('heavy fog'/'PTK', 'Sorcery').
card_original_text('heavy fog'/'PTK', 'Play Heavy Fog only after you\'re attacked, before you declare blockers.\nThis turn, all damage dealt to you by attacking creatures is reduced to 0.').
card_first_print('heavy fog', 'PTK').
card_image_name('heavy fog'/'PTK', 'heavy fog').
card_uid('heavy fog'/'PTK', 'PTK:Heavy Fog:heavy fog').
card_rarity('heavy fog'/'PTK', 'Uncommon').
card_artist('heavy fog'/'PTK', 'Liu Shangying').
card_number('heavy fog'/'PTK', '136').
card_multiverse_id('heavy fog'/'PTK', '10523').

card_in_set('hua tuo, honored physician', 'PTK').
card_original_type('hua tuo, honored physician'/'PTK', 'Creature — Legend').
card_original_text('hua tuo, honored physician'/'PTK', 'On your turn, before you attack, you may tap Hua Tuo to put any one creature card from your graveyard on the top of your library.').
card_first_print('hua tuo, honored physician', 'PTK').
card_image_name('hua tuo, honored physician'/'PTK', 'hua tuo, honored physician').
card_uid('hua tuo, honored physician'/'PTK', 'PTK:Hua Tuo, Honored Physician:hua tuo, honored physician').
card_rarity('hua tuo, honored physician'/'PTK', 'Rare').
card_artist('hua tuo, honored physician'/'PTK', 'Gao Jianzhang').
card_number('hua tuo, honored physician'/'PTK', '137').
card_multiverse_id('hua tuo, honored physician'/'PTK', '10570').

card_in_set('huang zhong, shu general', 'PTK').
card_original_type('huang zhong, shu general'/'PTK', 'Creature — Legend').
card_original_text('huang zhong, shu general'/'PTK', 'Huang Zhong can\'t be blocked by more than one creature each turn.').
card_first_print('huang zhong, shu general', 'PTK').
card_image_name('huang zhong, shu general'/'PTK', 'huang zhong, shu general').
card_uid('huang zhong, shu general'/'PTK', 'PTK:Huang Zhong, Shu General:huang zhong, shu general').
card_rarity('huang zhong, shu general'/'PTK', 'Rare').
card_artist('huang zhong, shu general'/'PTK', 'Quan Xuejun').
card_number('huang zhong, shu general'/'PTK', '8').
card_flavor_text('huang zhong, shu general'/'PTK', '\"Virile in war, he kept the north in fear;\nHis prodigies subdued the western sphere.\"').
card_multiverse_id('huang zhong, shu general'/'PTK', '10714').

card_in_set('hunting cheetah', 'PTK').
card_original_type('hunting cheetah'/'PTK', 'Creature — Cheetah').
card_original_text('hunting cheetah'/'PTK', 'When Hunting Cheetah successfully damages your opponent, you may search your library for a forest card, reveal that card, and put it into your hand. Shuffle your library afterward.').
card_first_print('hunting cheetah', 'PTK').
card_image_name('hunting cheetah'/'PTK', 'hunting cheetah').
card_uid('hunting cheetah'/'PTK', 'PTK:Hunting Cheetah:hunting cheetah').
card_rarity('hunting cheetah'/'PTK', 'Uncommon').
card_artist('hunting cheetah'/'PTK', 'Fang Yue').
card_number('hunting cheetah'/'PTK', '138').
card_multiverse_id('hunting cheetah'/'PTK', '10572').

card_in_set('imperial edict', 'PTK').
card_original_type('imperial edict'/'PTK', 'Sorcery').
card_original_text('imperial edict'/'PTK', 'Your opponent chooses one of his or her creatures. Destroy that creature.').
card_first_print('imperial edict', 'PTK').
card_image_name('imperial edict'/'PTK', 'imperial edict').
card_uid('imperial edict'/'PTK', 'PTK:Imperial Edict:imperial edict').
card_rarity('imperial edict'/'PTK', 'Common').
card_artist('imperial edict'/'PTK', 'Xu Xiaoming').
card_number('imperial edict'/'PTK', '77').
card_flavor_text('imperial edict'/'PTK', 'Frustrated with Cao Cao\'s control of the imperial court, Emporer Xian secretly issued an edict condemning him, using his own blood as ink.').
card_multiverse_id('imperial edict'/'PTK', '10515').

card_in_set('imperial recruiter', 'PTK').
card_original_type('imperial recruiter'/'PTK', 'Creature — Advisor').
card_original_text('imperial recruiter'/'PTK', 'When Imperial Recruiter comes into play, search your library for a creature card with power no greater than 2, reveal that card, and put it into your hand. Shuffle your library afterward.').
card_image_name('imperial recruiter'/'PTK', 'imperial recruiter').
card_uid('imperial recruiter'/'PTK', 'PTK:Imperial Recruiter:imperial recruiter').
card_rarity('imperial recruiter'/'PTK', 'Uncommon').
card_artist('imperial recruiter'/'PTK', 'Mitsuaki Sagiri').
card_number('imperial recruiter'/'PTK', '113').
card_multiverse_id('imperial recruiter'/'PTK', '10539').

card_in_set('imperial seal', 'PTK').
card_original_type('imperial seal'/'PTK', 'Sorcery').
card_original_text('imperial seal'/'PTK', 'Search your library for any one card. Shuffle your library and put that card on top of it. You lose 2 life.').
card_first_print('imperial seal', 'PTK').
card_image_name('imperial seal'/'PTK', 'imperial seal').
card_uid('imperial seal'/'PTK', 'PTK:Imperial Seal:imperial seal').
card_rarity('imperial seal'/'PTK', 'Rare').
card_artist('imperial seal'/'PTK', 'Li Tie').
card_number('imperial seal'/'PTK', '78').
card_flavor_text('imperial seal'/'PTK', '\"If Heaven has placed it in your hands, it means that the throne is destined to be yours.\"').
card_multiverse_id('imperial seal'/'PTK', '10728').

card_in_set('independent troops', 'PTK').
card_original_type('independent troops'/'PTK', 'Creature — Soldiers').
card_original_text('independent troops'/'PTK', '').
card_first_print('independent troops', 'PTK').
card_image_name('independent troops'/'PTK', 'independent troops').
card_uid('independent troops'/'PTK', 'PTK:Independent Troops:independent troops').
card_rarity('independent troops'/'PTK', 'Common').
card_artist('independent troops'/'PTK', 'Kuang Sheng').
card_number('independent troops'/'PTK', '114').
card_flavor_text('independent troops'/'PTK', '\"The empire, long united, must divide, and long divided, must unite.\"').
card_multiverse_id('independent troops'/'PTK', '10541').

card_in_set('island', 'PTK').
card_original_type('island'/'PTK', 'Land').
card_original_text('island'/'PTK', 'U').
card_image_name('island'/'PTK', 'island1').
card_uid('island'/'PTK', 'PTK:Island:island1').
card_rarity('island'/'PTK', 'Basic Land').
card_artist('island'/'PTK', 'Ku Xueming').
card_number('island'/'PTK', '169').
card_multiverse_id('island'/'PTK', '10551').

card_in_set('island', 'PTK').
card_original_type('island'/'PTK', 'Land').
card_original_text('island'/'PTK', 'U').
card_image_name('island'/'PTK', 'island2').
card_uid('island'/'PTK', 'PTK:Island:island2').
card_rarity('island'/'PTK', 'Basic Land').
card_artist('island'/'PTK', 'Ku Xueming').
card_number('island'/'PTK', '170').
card_multiverse_id('island'/'PTK', '10641').

card_in_set('island', 'PTK').
card_original_type('island'/'PTK', 'Land').
card_original_text('island'/'PTK', 'U').
card_image_name('island'/'PTK', 'island3').
card_uid('island'/'PTK', 'PTK:Island:island3').
card_rarity('island'/'PTK', 'Basic Land').
card_artist('island'/'PTK', 'Ku Xueming').
card_number('island'/'PTK', '171').
card_multiverse_id('island'/'PTK', '10642').

card_in_set('kongming\'s contraptions', 'PTK').
card_original_type('kongming\'s contraptions'/'PTK', 'Creature — Soldiers').
card_original_text('kongming\'s contraptions'/'PTK', 'After you\'re attacked, before you declare blockers, you may tap Kongming\'s Contraptions to have it deal 2 damage to any one attacking creature.').
card_first_print('kongming\'s contraptions', 'PTK').
card_image_name('kongming\'s contraptions'/'PTK', 'kongming\'s contraptions').
card_uid('kongming\'s contraptions'/'PTK', 'PTK:Kongming\'s Contraptions:kongming\'s contraptions').
card_rarity('kongming\'s contraptions'/'PTK', 'Rare').
card_artist('kongming\'s contraptions'/'PTK', 'Jiaming').
card_number('kongming\'s contraptions'/'PTK', '10').
card_multiverse_id('kongming\'s contraptions'/'PTK', '10495').

card_in_set('kongming, \"sleeping dragon\"', 'PTK').
card_original_type('kongming, \"sleeping dragon\"'/'PTK', 'Creature — Legend').
card_original_text('kongming, \"sleeping dragon\"'/'PTK', 'All your other creatures get +1/+1 as long as Kongming is in play.').
card_first_print('kongming, \"sleeping dragon\"', 'PTK').
card_image_name('kongming, \"sleeping dragon\"'/'PTK', 'kongming, sleeping dragon').
card_uid('kongming, \"sleeping dragon\"'/'PTK', 'PTK:Kongming, \"Sleeping Dragon\":kongming, sleeping dragon').
card_rarity('kongming, \"sleeping dragon\"'/'PTK', 'Rare').
card_artist('kongming, \"sleeping dragon\"'/'PTK', 'Gao Yan').
card_number('kongming, \"sleeping dragon\"'/'PTK', '9').
card_flavor_text('kongming, \"sleeping dragon\"'/'PTK', '\"Such a lord as this—all virtues\' height—\nHad never been, nor ever was again.\"').
card_multiverse_id('kongming, \"sleeping dragon\"'/'PTK', '10490').

card_in_set('lady sun', 'PTK').
card_original_type('lady sun'/'PTK', 'Creature — Legend').
card_original_text('lady sun'/'PTK', 'On your turn, before you attack, you may tap Lady Sun to return it and any one other creature to their owners\' hands.').
card_first_print('lady sun', 'PTK').
card_image_name('lady sun'/'PTK', 'lady sun').
card_uid('lady sun'/'PTK', 'PTK:Lady Sun:lady sun').
card_rarity('lady sun'/'PTK', 'Rare').
card_artist('lady sun'/'PTK', 'Miao Aili').
card_number('lady sun'/'PTK', '45').
card_flavor_text('lady sun'/'PTK', 'Sister to Sun Quan and wife to Liu Bei, Lady Sun often felt her loyalty to both tested.').
card_multiverse_id('lady sun'/'PTK', '10721').

card_in_set('lady zhurong, warrior queen', 'PTK').
card_original_type('lady zhurong, warrior queen'/'PTK', 'Creature — Legend').
card_original_text('lady zhurong, warrior queen'/'PTK', 'Horsemanship').
card_first_print('lady zhurong, warrior queen', 'PTK').
card_image_name('lady zhurong, warrior queen'/'PTK', 'lady zhurong, warrior queen').
card_uid('lady zhurong, warrior queen'/'PTK', 'PTK:Lady Zhurong, Warrior Queen:lady zhurong, warrior queen').
card_rarity('lady zhurong, warrior queen'/'PTK', 'Rare').
card_artist('lady zhurong, warrior queen'/'PTK', 'Miao Aili').
card_number('lady zhurong, warrior queen'/'PTK', '139').
card_flavor_text('lady zhurong, warrior queen'/'PTK', '\"A man, and such a fool! I, a woman, will fight them for you.\"\n—Lady Zhurong to her husband Meng Huo, before leading an army against the Shu').
card_multiverse_id('lady zhurong, warrior queen'/'PTK', '10609').

card_in_set('liu bei, lord of shu', 'PTK').
card_original_type('liu bei, lord of shu'/'PTK', 'Creature — Legend').
card_original_text('liu bei, lord of shu'/'PTK', 'Horsemanship\nAs long as you have Guan Yu and/or Zhang Fei in play, Liu Bei gets +2/+2.').
card_first_print('liu bei, lord of shu', 'PTK').
card_image_name('liu bei, lord of shu'/'PTK', 'liu bei, lord of shu').
card_uid('liu bei, lord of shu'/'PTK', 'PTK:Liu Bei, Lord of Shu:liu bei, lord of shu').
card_rarity('liu bei, lord of shu'/'PTK', 'Rare').
card_artist('liu bei, lord of shu'/'PTK', 'Zhao Dafu').
card_number('liu bei, lord of shu'/'PTK', '11').
card_flavor_text('liu bei, lord of shu'/'PTK', '\"Only wisdom and virtue can truly win men\'s devotion.\"\n—Liu Bei').
card_multiverse_id('liu bei, lord of shu'/'PTK', '10499').

card_in_set('lone wolf', 'PTK').
card_original_type('lone wolf'/'PTK', 'Creature — Wolf').
card_original_text('lone wolf'/'PTK', 'When Lone Wolf attacks and is blocked, you may have it deal its damage to the defending player instead of to the creatures blocking it.').
card_image_name('lone wolf'/'PTK', 'lone wolf').
card_uid('lone wolf'/'PTK', 'PTK:Lone Wolf:lone wolf').
card_rarity('lone wolf'/'PTK', 'Uncommon').
card_artist('lone wolf'/'PTK', 'Yang Jun Kwon').
card_number('lone wolf'/'PTK', '140').
card_multiverse_id('lone wolf'/'PTK', '10556').

card_in_set('loyal retainers', 'PTK').
card_original_type('loyal retainers'/'PTK', 'Creature — Advisors').
card_original_text('loyal retainers'/'PTK', 'On your turn, before you attack you may put Loyal Retainers in your graveyard to take any one Legend card from your graveyard and put that creature into play.').
card_first_print('loyal retainers', 'PTK').
card_image_name('loyal retainers'/'PTK', 'loyal retainers').
card_uid('loyal retainers'/'PTK', 'PTK:Loyal Retainers:loyal retainers').
card_rarity('loyal retainers'/'PTK', 'Uncommon').
card_artist('loyal retainers'/'PTK', 'Solomon Au Yeung').
card_number('loyal retainers'/'PTK', '12').
card_multiverse_id('loyal retainers'/'PTK', '10491').

card_in_set('lu bu, master-at-arms', 'PTK').
card_original_type('lu bu, master-at-arms'/'PTK', 'Creature — Legend').
card_original_text('lu bu, master-at-arms'/'PTK', 'Horsemanship\nLu Bu is unaffected by summoning sickness.').
card_image_name('lu bu, master-at-arms'/'PTK', 'lu bu, master-at-arms').
card_uid('lu bu, master-at-arms'/'PTK', 'PTK:Lu Bu, Master-at-Arms:lu bu, master-at-arms').
card_rarity('lu bu, master-at-arms'/'PTK', 'Rare').
card_artist('lu bu, master-at-arms'/'PTK', 'Gao Jianzhang').
card_number('lu bu, master-at-arms'/'PTK', '115').
card_flavor_text('lu bu, master-at-arms'/'PTK', '\"Dong Zhuo\'s man, Lu Bu, warrior without peer, / Far surpassed the champions of his sphere.\"').
card_multiverse_id('lu bu, master-at-arms'/'PTK', '10536').

card_in_set('lu meng, wu general', 'PTK').
card_original_type('lu meng, wu general'/'PTK', 'Creature — Legend').
card_original_text('lu meng, wu general'/'PTK', 'Horsemanship').
card_first_print('lu meng, wu general', 'PTK').
card_image_name('lu meng, wu general'/'PTK', 'lu meng, wu general').
card_uid('lu meng, wu general'/'PTK', 'PTK:Lu Meng, Wu General:lu meng, wu general').
card_rarity('lu meng, wu general'/'PTK', 'Rare').
card_artist('lu meng, wu general'/'PTK', 'Gao Yan').
card_number('lu meng, wu general'/'PTK', '46').
card_flavor_text('lu meng, wu general'/'PTK', 'As the Wu chief commander, Lu Meng conquered Shu-held Jingzhou in 219 by disguising soldiers as merchants on boats filled with hiding troops.').
card_multiverse_id('lu meng, wu general'/'PTK', '10710').

card_in_set('lu su, wu advisor', 'PTK').
card_original_type('lu su, wu advisor'/'PTK', 'Creature — Legend').
card_original_text('lu su, wu advisor'/'PTK', 'On your turn, before you attack, you may tap Lu Su to draw a card.').
card_first_print('lu su, wu advisor', 'PTK').
card_image_name('lu su, wu advisor'/'PTK', 'lu su, wu advisor').
card_uid('lu su, wu advisor'/'PTK', 'PTK:Lu Su, Wu Advisor:lu su, wu advisor').
card_rarity('lu su, wu advisor'/'PTK', 'Rare').
card_artist('lu su, wu advisor'/'PTK', 'Zhao Tan').
card_number('lu su, wu advisor'/'PTK', '47').
card_flavor_text('lu su, wu advisor'/'PTK', 'Lu Su served as an intermediary between the Wu and Shu kingdoms until Zhou Yu\'s death in 210, when he became Wu\'s supreme commander.').
card_multiverse_id('lu su, wu advisor'/'PTK', '10614').

card_in_set('lu xun, scholar general', 'PTK').
card_original_type('lu xun, scholar general'/'PTK', 'Creature — Legend').
card_original_text('lu xun, scholar general'/'PTK', 'Horsemanship\nWhen Lu Xun successfully damages your opponent, you may draw a card.').
card_first_print('lu xun, scholar general', 'PTK').
card_image_name('lu xun, scholar general'/'PTK', 'lu xun, scholar general').
card_uid('lu xun, scholar general'/'PTK', 'PTK:Lu Xun, Scholar General:lu xun, scholar general').
card_rarity('lu xun, scholar general'/'PTK', 'Rare').
card_artist('lu xun, scholar general'/'PTK', 'Xu Xiaoming').
card_number('lu xun, scholar general'/'PTK', '48').
card_multiverse_id('lu xun, scholar general'/'PTK', '10616').

card_in_set('ma chao, western warrior', 'PTK').
card_original_type('ma chao, western warrior'/'PTK', 'Creature — Legend').
card_original_text('ma chao, western warrior'/'PTK', 'Horsemanship\nWhenever Ma Chao attacks and no other creatures do, Ma Chao can\'t be blocked.').
card_first_print('ma chao, western warrior', 'PTK').
card_image_name('ma chao, western warrior'/'PTK', 'ma chao, western warrior').
card_uid('ma chao, western warrior'/'PTK', 'PTK:Ma Chao, Western Warrior:ma chao, western warrior').
card_rarity('ma chao, western warrior'/'PTK', 'Rare').
card_artist('ma chao, western warrior'/'PTK', 'Koji').
card_number('ma chao, western warrior'/'PTK', '116').
card_multiverse_id('ma chao, western warrior'/'PTK', '10729').

card_in_set('marshaling the troops', 'PTK').
card_original_type('marshaling the troops'/'PTK', 'Sorcery').
card_original_text('marshaling the troops'/'PTK', 'Tap any number of your creatures. You gain 4 life for each creature tapped this way. (Tapped creatures can\'t block.)').
card_first_print('marshaling the troops', 'PTK').
card_image_name('marshaling the troops'/'PTK', 'marshaling the troops').
card_uid('marshaling the troops'/'PTK', 'PTK:Marshaling the Troops:marshaling the troops').
card_rarity('marshaling the troops'/'PTK', 'Rare').
card_artist('marshaling the troops'/'PTK', 'Liu Shangying').
card_number('marshaling the troops'/'PTK', '141').
card_multiverse_id('marshaling the troops'/'PTK', '10547').

card_in_set('meng huo\'s horde', 'PTK').
card_original_type('meng huo\'s horde'/'PTK', 'Creature — Soldiers').
card_original_text('meng huo\'s horde'/'PTK', '').
card_first_print('meng huo\'s horde', 'PTK').
card_image_name('meng huo\'s horde'/'PTK', 'meng huo\'s horde').
card_uid('meng huo\'s horde'/'PTK', 'PTK:Meng Huo\'s Horde:meng huo\'s horde').
card_rarity('meng huo\'s horde'/'PTK', 'Common').
card_artist('meng huo\'s horde'/'PTK', 'Li Tie').
card_number('meng huo\'s horde'/'PTK', '143').
card_flavor_text('meng huo\'s horde'/'PTK', 'Through his allies, Meng Huo commanded several hundred thousand troops composed of cavalry, rattan-armored warriors, infantry, naked men with swords, and wild animals.').
card_multiverse_id('meng huo\'s horde'/'PTK', '10503').

card_in_set('meng huo, barbarian king', 'PTK').
card_original_type('meng huo, barbarian king'/'PTK', 'Creature — Legend').
card_original_text('meng huo, barbarian king'/'PTK', 'All your other green creatures get +1/+1 as long as Meng Huo is in play.').
card_first_print('meng huo, barbarian king', 'PTK').
card_image_name('meng huo, barbarian king'/'PTK', 'meng huo, barbarian king').
card_uid('meng huo, barbarian king'/'PTK', 'PTK:Meng Huo, Barbarian King:meng huo, barbarian king').
card_rarity('meng huo, barbarian king'/'PTK', 'Rare').
card_artist('meng huo, barbarian king'/'PTK', 'Yang Guangmai').
card_number('meng huo, barbarian king'/'PTK', '142').
card_flavor_text('meng huo, barbarian king'/'PTK', 'The stubborn Meng Huo was captured and released by Kongming seven times before finally surrendering.').
card_multiverse_id('meng huo, barbarian king'/'PTK', '10732').

card_in_set('misfortune\'s gain', 'PTK').
card_original_type('misfortune\'s gain'/'PTK', 'Sorcery').
card_original_text('misfortune\'s gain'/'PTK', 'Destroy any one creature. That creature\'s owner gains 4 life.').
card_first_print('misfortune\'s gain', 'PTK').
card_image_name('misfortune\'s gain'/'PTK', 'misfortune\'s gain').
card_uid('misfortune\'s gain'/'PTK', 'PTK:Misfortune\'s Gain:misfortune\'s gain').
card_rarity('misfortune\'s gain'/'PTK', 'Common').
card_artist('misfortune\'s gain'/'PTK', 'Jiaming').
card_number('misfortune\'s gain'/'PTK', '13').
card_flavor_text('misfortune\'s gain'/'PTK', 'The mourning families of commanders and generals were often given land, valuables, or money to compensate for their losses.').
card_multiverse_id('misfortune\'s gain'/'PTK', '10579').

card_in_set('mountain', 'PTK').
card_original_type('mountain'/'PTK', 'Land').
card_original_text('mountain'/'PTK', 'R').
card_image_name('mountain'/'PTK', 'mountain1').
card_uid('mountain'/'PTK', 'PTK:Mountain:mountain1').
card_rarity('mountain'/'PTK', 'Basic Land').
card_artist('mountain'/'PTK', 'Qin Jun').
card_number('mountain'/'PTK', '175').
card_multiverse_id('mountain'/'PTK', '10563').

card_in_set('mountain', 'PTK').
card_original_type('mountain'/'PTK', 'Land').
card_original_text('mountain'/'PTK', 'R').
card_image_name('mountain'/'PTK', 'mountain2').
card_uid('mountain'/'PTK', 'PTK:Mountain:mountain2').
card_rarity('mountain'/'PTK', 'Basic Land').
card_artist('mountain'/'PTK', 'Qin Jun').
card_number('mountain'/'PTK', '176').
card_multiverse_id('mountain'/'PTK', '10643').

card_in_set('mountain', 'PTK').
card_original_type('mountain'/'PTK', 'Land').
card_original_text('mountain'/'PTK', 'R').
card_image_name('mountain'/'PTK', 'mountain3').
card_uid('mountain'/'PTK', 'PTK:Mountain:mountain3').
card_rarity('mountain'/'PTK', 'Basic Land').
card_artist('mountain'/'PTK', 'Qin Jun').
card_number('mountain'/'PTK', '177').
card_multiverse_id('mountain'/'PTK', '10644').

card_in_set('mountain bandit', 'PTK').
card_original_type('mountain bandit'/'PTK', 'Creature — Soldier').
card_original_text('mountain bandit'/'PTK', 'Mountain Bandit is unaffected by summoning sickness.').
card_first_print('mountain bandit', 'PTK').
card_image_name('mountain bandit'/'PTK', 'mountain bandit').
card_uid('mountain bandit'/'PTK', 'PTK:Mountain Bandit:mountain bandit').
card_rarity('mountain bandit'/'PTK', 'Common').
card_artist('mountain bandit'/'PTK', 'Xu Xiaoming').
card_number('mountain bandit'/'PTK', '117').
card_flavor_text('mountain bandit'/'PTK', 'Penniless and far from home, many former Yellow Scarves and other soldiers became bandits to survive.').
card_multiverse_id('mountain bandit'/'PTK', '10585').

card_in_set('mystic denial', 'PTK').
card_original_type('mystic denial'/'PTK', 'Sorcery').
card_original_text('mystic denial'/'PTK', 'Play Mystic Denial only in response to another player playing a creature or a sorcery. That card has no effect, and that player puts it into his or her graveyard.').
card_image_name('mystic denial'/'PTK', 'mystic denial').
card_uid('mystic denial'/'PTK', 'PTK:Mystic Denial:mystic denial').
card_rarity('mystic denial'/'PTK', 'Uncommon').
card_artist('mystic denial'/'PTK', 'Kang Yu').
card_number('mystic denial'/'PTK', '49').
card_multiverse_id('mystic denial'/'PTK', '10565').

card_in_set('overwhelming forces', 'PTK').
card_original_type('overwhelming forces'/'PTK', 'Sorcery').
card_original_text('overwhelming forces'/'PTK', 'Destroy all your opponent\'s creatures. you draw a card for each creature destroyed in this way.').
card_image_name('overwhelming forces'/'PTK', 'overwhelming forces').
card_uid('overwhelming forces'/'PTK', 'PTK:Overwhelming Forces:overwhelming forces').
card_rarity('overwhelming forces'/'PTK', 'Rare').
card_artist('overwhelming forces'/'PTK', 'Gao Yan').
card_number('overwhelming forces'/'PTK', '79').
card_flavor_text('overwhelming forces'/'PTK', 'By the year 208, Cao Cao commanded more than 1,000 experienced generals and a million infantry, cavalry, and naval troops.').
card_multiverse_id('overwhelming forces'/'PTK', '10521').

card_in_set('pang tong, \"young phoenix\"', 'PTK').
card_original_type('pang tong, \"young phoenix\"'/'PTK', 'Creature — Legend').
card_original_text('pang tong, \"young phoenix\"'/'PTK', 'On your turn, before you attack, you may tap Pang Tong to give any one creature +0/+2 until the end of the turn.').
card_first_print('pang tong, \"young phoenix\"', 'PTK').
card_image_name('pang tong, \"young phoenix\"'/'PTK', 'pang tong, young phoenix').
card_uid('pang tong, \"young phoenix\"'/'PTK', 'PTK:Pang Tong, \"Young Phoenix\":pang tong, young phoenix').
card_rarity('pang tong, \"young phoenix\"'/'PTK', 'Rare').
card_artist('pang tong, \"young phoenix\"'/'PTK', 'Li Tie').
card_number('pang tong, \"young phoenix\"'/'PTK', '14').
card_flavor_text('pang tong, \"young phoenix\"'/'PTK', '\". . . It was Pang Tong\'s boat-connecting scheme\nThat let Zhou Yu accomplish his great deed.\"').
card_multiverse_id('pang tong, \"young phoenix\"'/'PTK', '10713').

card_in_set('peach garden oath', 'PTK').
card_original_type('peach garden oath'/'PTK', 'Sorcery').
card_original_text('peach garden oath'/'PTK', 'For each creature you have in play, you gain 2 life.').
card_first_print('peach garden oath', 'PTK').
card_image_name('peach garden oath'/'PTK', 'peach garden oath').
card_uid('peach garden oath'/'PTK', 'PTK:Peach Garden Oath:peach garden oath').
card_rarity('peach garden oath'/'PTK', 'Uncommon').
card_artist('peach garden oath'/'PTK', 'Zhao Dafu').
card_number('peach garden oath'/'PTK', '15').
card_flavor_text('peach garden oath'/'PTK', '\"We three, though of separate ancestry, join in brotherhood . . . . We dare not hope to be together always but hereby vow to die the selfsame day.\"\n—Peach Garden Oath').
card_multiverse_id('peach garden oath'/'PTK', '10531').

card_in_set('plains', 'PTK').
card_original_type('plains'/'PTK', 'Land').
card_original_text('plains'/'PTK', 'W').
card_image_name('plains'/'PTK', 'plains1').
card_uid('plains'/'PTK', 'PTK:Plains:plains1').
card_rarity('plains'/'PTK', 'Basic Land').
card_artist('plains'/'PTK', 'He Jiancheng').
card_number('plains'/'PTK', '166').
card_multiverse_id('plains'/'PTK', '10581').

card_in_set('plains', 'PTK').
card_original_type('plains'/'PTK', 'Land').
card_original_text('plains'/'PTK', 'W').
card_image_name('plains'/'PTK', 'plains2').
card_uid('plains'/'PTK', 'PTK:Plains:plains2').
card_rarity('plains'/'PTK', 'Basic Land').
card_artist('plains'/'PTK', 'He Jiancheng').
card_number('plains'/'PTK', '167').
card_multiverse_id('plains'/'PTK', '10645').

card_in_set('plains', 'PTK').
card_original_type('plains'/'PTK', 'Land').
card_original_text('plains'/'PTK', 'W').
card_image_name('plains'/'PTK', 'plains3').
card_uid('plains'/'PTK', 'PTK:Plains:plains3').
card_rarity('plains'/'PTK', 'Basic Land').
card_artist('plains'/'PTK', 'He Jiancheng').
card_number('plains'/'PTK', '168').
card_multiverse_id('plains'/'PTK', '10646').

card_in_set('poison arrow', 'PTK').
card_original_type('poison arrow'/'PTK', 'Sorcery').
card_original_text('poison arrow'/'PTK', 'Destroy any one creature that isn\'t black. You gain 3 life.').
card_first_print('poison arrow', 'PTK').
card_image_name('poison arrow'/'PTK', 'poison arrow').
card_uid('poison arrow'/'PTK', 'PTK:Poison Arrow:poison arrow').
card_rarity('poison arrow'/'PTK', 'Uncommon').
card_artist('poison arrow'/'PTK', 'Li Tie').
card_number('poison arrow'/'PTK', '80').
card_flavor_text('poison arrow'/'PTK', 'In ancient China, not wearing armor could be a fatal mistake. Guan Yu, Zhou Yu, Sun Ce, and other heros were struck by poison arrows.').
card_multiverse_id('poison arrow'/'PTK', '10587').

card_in_set('preemptive strike', 'PTK').
card_original_type('preemptive strike'/'PTK', 'Sorcery').
card_original_text('preemptive strike'/'PTK', 'Play Preemptive Strike only in response to another player playing a creature card.\nThat creature has no effect, and that player puts it into his or her graveyard.').
card_first_print('preemptive strike', 'PTK').
card_image_name('preemptive strike'/'PTK', 'preemptive strike').
card_uid('preemptive strike'/'PTK', 'PTK:Preemptive Strike:preemptive strike').
card_rarity('preemptive strike'/'PTK', 'Common').
card_artist('preemptive strike'/'PTK', 'Jiaming').
card_number('preemptive strike'/'PTK', '50').
card_multiverse_id('preemptive strike'/'PTK', '10530').

card_in_set('rally the troops', 'PTK').
card_original_type('rally the troops'/'PTK', 'Sorcery').
card_original_text('rally the troops'/'PTK', 'Play Rally the Troops only after you\'re attacked, before you declare blockers.\nUntap all your creatures.').
card_image_name('rally the troops'/'PTK', 'rally the troops').
card_uid('rally the troops'/'PTK', 'PTK:Rally the Troops:rally the troops').
card_rarity('rally the troops'/'PTK', 'Uncommon').
card_artist('rally the troops'/'PTK', 'Shang Huitong').
card_number('rally the troops'/'PTK', '16').
card_multiverse_id('rally the troops'/'PTK', '10589').

card_in_set('ravages of war', 'PTK').
card_original_type('ravages of war'/'PTK', 'Sorcery').
card_original_text('ravages of war'/'PTK', 'Destroy all lands. (This includes your lands.)').
card_first_print('ravages of war', 'PTK').
card_image_name('ravages of war'/'PTK', 'ravages of war').
card_uid('ravages of war'/'PTK', 'PTK:Ravages of War:ravages of war').
card_rarity('ravages of war'/'PTK', 'Rare').
card_artist('ravages of war'/'PTK', 'Fang Yue').
card_number('ravages of war'/'PTK', '17').
card_flavor_text('ravages of war'/'PTK', '\"Thorn bushes spring up wherever the army has passed. Lean years follow in the wake of a great war.\" —Lao Tzu, Tao Te Ching (trans. Feng and English)').
card_multiverse_id('ravages of war'/'PTK', '10500').

card_in_set('ravaging horde', 'PTK').
card_original_type('ravaging horde'/'PTK', 'Creature — Soldiers').
card_original_text('ravaging horde'/'PTK', 'When Ravaging Horde comes into play, destroy any one land.').
card_first_print('ravaging horde', 'PTK').
card_image_name('ravaging horde'/'PTK', 'ravaging horde').
card_uid('ravaging horde'/'PTK', 'PTK:Ravaging Horde:ravaging horde').
card_rarity('ravaging horde'/'PTK', 'Uncommon').
card_artist('ravaging horde'/'PTK', 'Liu Jianjian').
card_number('ravaging horde'/'PTK', '118').
card_flavor_text('ravaging horde'/'PTK', 'Upon his return to the capitol after taking soldiers to loot a nearby peaceful town, the prime minister, Dong Zhou, boasted of it as a victory over bandits.').
card_multiverse_id('ravaging horde'/'PTK', '13805').

card_in_set('red cliffs armada', 'PTK').
card_original_type('red cliffs armada'/'PTK', 'Creature — Ships').
card_original_text('red cliffs armada'/'PTK', 'Red Cliffs Armada can\'t attack unless the defending player has an island in play.').
card_first_print('red cliffs armada', 'PTK').
card_image_name('red cliffs armada'/'PTK', 'red cliffs armada').
card_uid('red cliffs armada'/'PTK', 'PTK:Red Cliffs Armada:red cliffs armada').
card_rarity('red cliffs armada'/'PTK', 'Uncommon').
card_artist('red cliffs armada'/'PTK', 'Zhang Jiazhen').
card_number('red cliffs armada'/'PTK', '51').
card_flavor_text('red cliffs armada'/'PTK', 'By the battle of Red Cliffs in the year 208, the Wu kingdom controlled more than 7,000 warships on the Yangtze.').
card_multiverse_id('red cliffs armada'/'PTK', '10501').

card_in_set('relentless assault', 'PTK').
card_original_type('relentless assault'/'PTK', 'Sorcery').
card_original_text('relentless assault'/'PTK', 'Untap all creatures that attacked this turn. You may declare an additional attack this turn.').
card_image_name('relentless assault'/'PTK', 'relentless assault').
card_uid('relentless assault'/'PTK', 'PTK:Relentless Assault:relentless assault').
card_rarity('relentless assault'/'PTK', 'Rare').
card_artist('relentless assault'/'PTK', 'He Jiancheng').
card_number('relentless assault'/'PTK', '119').
card_flavor_text('relentless assault'/'PTK', 'With only 70,000 men under his command at the battle of Guandu, Cao Cao was able to defeat Yuan Shao and his 700,000.').
card_multiverse_id('relentless assault'/'PTK', '10592').

card_in_set('renegade troops', 'PTK').
card_original_type('renegade troops'/'PTK', 'Creature — Soldiers').
card_original_text('renegade troops'/'PTK', 'Renegade Troops is unaffected by summoning sickness.').
card_first_print('renegade troops', 'PTK').
card_image_name('renegade troops'/'PTK', 'renegade troops').
card_uid('renegade troops'/'PTK', 'PTK:Renegade Troops:renegade troops').
card_rarity('renegade troops'/'PTK', 'Uncommon').
card_artist('renegade troops'/'PTK', 'Liu Jianjian').
card_number('renegade troops'/'PTK', '120').
card_flavor_text('renegade troops'/'PTK', '\"Across the land rebellions seethed and swarmed / As vicious war lords swooped down on all sides.\"').
card_multiverse_id('renegade troops'/'PTK', '10576').

card_in_set('return to battle', 'PTK').
card_original_type('return to battle'/'PTK', 'Sorcery').
card_original_text('return to battle'/'PTK', 'Return any one creature card from your graveyard to your hand.').
card_first_print('return to battle', 'PTK').
card_image_name('return to battle'/'PTK', 'return to battle').
card_uid('return to battle'/'PTK', 'PTK:Return to Battle:return to battle').
card_rarity('return to battle'/'PTK', 'Common').
card_artist('return to battle'/'PTK', 'Ding Songjian').
card_number('return to battle'/'PTK', '81').
card_flavor_text('return to battle'/'PTK', '\"Swallowing his eye, the valiant Xiahou Dun fought on; / But Cao Cao\'s vanguard, its commander wounded, could not hold out for long.\"').
card_multiverse_id('return to battle'/'PTK', '10588').

card_in_set('riding red hare', 'PTK').
card_original_type('riding red hare'/'PTK', 'Sorcery').
card_original_text('riding red hare'/'PTK', 'Any one creature gets +3/+3 and gains horsemanship until the end of the turn.').
card_first_print('riding red hare', 'PTK').
card_image_name('riding red hare'/'PTK', 'riding red hare').
card_uid('riding red hare'/'PTK', 'PTK:Riding Red Hare:riding red hare').
card_rarity('riding red hare'/'PTK', 'Common').
card_artist('riding red hare'/'PTK', 'Qi Baocheng').
card_number('riding red hare'/'PTK', '18').
card_flavor_text('riding red hare'/'PTK', 'One of the greatest steeds in the empire, Red Hare could travel a thousand li a day and climb hills as if running on flat ground.').
card_multiverse_id('riding red hare'/'PTK', '10496').

card_in_set('riding the dilu horse', 'PTK').
card_original_type('riding the dilu horse'/'PTK', 'Sorcery').
card_original_text('riding the dilu horse'/'PTK', 'Any one creature gets +2/+2 and gains horsemanship.').
card_first_print('riding the dilu horse', 'PTK').
card_image_name('riding the dilu horse'/'PTK', 'riding the dilu horse').
card_uid('riding the dilu horse'/'PTK', 'PTK:Riding the Dilu Horse:riding the dilu horse').
card_rarity('riding the dilu horse'/'PTK', 'Rare').
card_artist('riding the dilu horse'/'PTK', 'Hong Yan').
card_number('riding the dilu horse'/'PTK', '144').
card_flavor_text('riding the dilu horse'/'PTK', '\"All men have their appointed time; that\'s something no horse can change.\"\n—Liu Bei, after being told that the\nDilu brings its master ill fortune').
card_multiverse_id('riding the dilu horse'/'PTK', '10731').

card_in_set('rockslide ambush', 'PTK').
card_original_type('rockslide ambush'/'PTK', 'Sorcery').
card_original_text('rockslide ambush'/'PTK', 'Rockslide Ambush deals to any one creature damage equal to the number of mountain cards you have in play. (This includes both tapped and untapped mountain cards.)').
card_first_print('rockslide ambush', 'PTK').
card_image_name('rockslide ambush'/'PTK', 'rockslide ambush').
card_uid('rockslide ambush'/'PTK', 'PTK:Rockslide Ambush:rockslide ambush').
card_rarity('rockslide ambush'/'PTK', 'Uncommon').
card_artist('rockslide ambush'/'PTK', 'Inoue Junichi').
card_number('rockslide ambush'/'PTK', '121').
card_multiverse_id('rockslide ambush'/'PTK', '10603').

card_in_set('rolling earthquake', 'PTK').
card_original_type('rolling earthquake'/'PTK', 'Sorcery').
card_original_text('rolling earthquake'/'PTK', 'Rolling Earthquake deals X damage to each player and each creature without horsemanship. (This includes you and your creatures without horsemanship.)').
card_first_print('rolling earthquake', 'PTK').
card_image_name('rolling earthquake'/'PTK', 'rolling earthquake').
card_uid('rolling earthquake'/'PTK', 'PTK:Rolling Earthquake:rolling earthquake').
card_rarity('rolling earthquake'/'PTK', 'Rare').
card_artist('rolling earthquake'/'PTK', 'Yang Hong').
card_number('rolling earthquake'/'PTK', '122').
card_multiverse_id('rolling earthquake'/'PTK', '10526').

card_in_set('sage\'s knowledge', 'PTK').
card_original_type('sage\'s knowledge'/'PTK', 'Sorcery').
card_original_text('sage\'s knowledge'/'PTK', 'Return any one sorcery card from your graveyard to your hand.').
card_first_print('sage\'s knowledge', 'PTK').
card_image_name('sage\'s knowledge'/'PTK', 'sage\'s knowledge').
card_uid('sage\'s knowledge'/'PTK', 'PTK:Sage\'s Knowledge:sage\'s knowledge').
card_rarity('sage\'s knowledge'/'PTK', 'Common').
card_artist('sage\'s knowledge'/'PTK', 'Ding Songjian').
card_number('sage\'s knowledge'/'PTK', '52').
card_flavor_text('sage\'s knowledge'/'PTK', '\"Those who know do not talk. Those who talk do not know.\"\n—Lao Tzu, Tao Te Ching\n(trans. Feng and English)').
card_multiverse_id('sage\'s knowledge'/'PTK', '10524').

card_in_set('shu cavalry', 'PTK').
card_original_type('shu cavalry'/'PTK', 'Creature — Soldiers').
card_original_text('shu cavalry'/'PTK', 'Horsemanship').
card_first_print('shu cavalry', 'PTK').
card_image_name('shu cavalry'/'PTK', 'shu cavalry').
card_uid('shu cavalry'/'PTK', 'PTK:Shu Cavalry:shu cavalry').
card_rarity('shu cavalry'/'PTK', 'Common').
card_artist('shu cavalry'/'PTK', 'Li Xiaohua').
card_number('shu cavalry'/'PTK', '19').
card_flavor_text('shu cavalry'/'PTK', 'In establishing the Shu kingdom, Liu Bei\'s forces fought against Ma Chao at Chengdu. Eventually, Ma Chao surrendered and became one of Liu Bei\'s Tiger Generals.').
card_multiverse_id('shu cavalry'/'PTK', '10635').

card_in_set('shu defender', 'PTK').
card_original_type('shu defender'/'PTK', 'Creature — Soldier').
card_original_text('shu defender'/'PTK', 'When Shu Defender blocks, it gets +0/+2 until the end of the turn.').
card_first_print('shu defender', 'PTK').
card_image_name('shu defender'/'PTK', 'shu defender').
card_uid('shu defender'/'PTK', 'PTK:Shu Defender:shu defender').
card_rarity('shu defender'/'PTK', 'Common').
card_artist('shu defender'/'PTK', 'Sun Nan').
card_number('shu defender'/'PTK', '20').
card_flavor_text('shu defender'/'PTK', 'Confronting Cao Cao\'s army at Steepslope Bridge, Zhang Fei bellowed, \"I am Zhang Fei of Yan! Who dares fight me to the death?\" Cao Cao\'s army cowered and fled.').
card_multiverse_id('shu defender'/'PTK', '10624').

card_in_set('shu elite companions', 'PTK').
card_original_type('shu elite companions'/'PTK', 'Creature — Soldiers').
card_original_text('shu elite companions'/'PTK', 'Horsemanship').
card_first_print('shu elite companions', 'PTK').
card_image_name('shu elite companions'/'PTK', 'shu elite companions').
card_uid('shu elite companions'/'PTK', 'PTK:Shu Elite Companions:shu elite companions').
card_rarity('shu elite companions'/'PTK', 'Uncommon').
card_artist('shu elite companions'/'PTK', 'Zhao Dafu').
card_number('shu elite companions'/'PTK', '21').
card_flavor_text('shu elite companions'/'PTK', 'Throughout the three kingdoms, important generals were often guarded by small groups of expert soldiers known as \"elite companions.\"').
card_multiverse_id('shu elite companions'/'PTK', '10497').

card_in_set('shu elite infantry', 'PTK').
card_original_type('shu elite infantry'/'PTK', 'Creature — Soldiers').
card_original_text('shu elite infantry'/'PTK', '').
card_first_print('shu elite infantry', 'PTK').
card_image_name('shu elite infantry'/'PTK', 'shu elite infantry').
card_uid('shu elite infantry'/'PTK', 'PTK:Shu Elite Infantry:shu elite infantry').
card_rarity('shu elite infantry'/'PTK', 'Common').
card_artist('shu elite infantry'/'PTK', 'Song Shikai').
card_number('shu elite infantry'/'PTK', '22').
card_flavor_text('shu elite infantry'/'PTK', 'Kongming\'s first campaign against the Wei kingdom was a rousing success until an arrogant Shu general, Ma Su, foolishly lost the city of Jieting.').
card_multiverse_id('shu elite infantry'/'PTK', '10627').

card_in_set('shu farmer', 'PTK').
card_original_type('shu farmer'/'PTK', 'Creature — Farmer').
card_original_text('shu farmer'/'PTK', 'On your turn, before you attack, you may tap Shu Farmer to gain 1 life.').
card_first_print('shu farmer', 'PTK').
card_image_name('shu farmer'/'PTK', 'shu farmer').
card_uid('shu farmer'/'PTK', 'PTK:Shu Farmer:shu farmer').
card_rarity('shu farmer'/'PTK', 'Common').
card_artist('shu farmer'/'PTK', 'Li Xiaohua').
card_number('shu farmer'/'PTK', '23').
card_flavor_text('shu farmer'/'PTK', '\"The common folk are ceaselessly active. The fields are fertile and the soil productive, and neither flood nor drought plagues us.\"\n—A Shu diplomat').
card_multiverse_id('shu farmer'/'PTK', '10618').

card_in_set('shu foot soldiers', 'PTK').
card_original_type('shu foot soldiers'/'PTK', 'Creature — Soldiers').
card_original_text('shu foot soldiers'/'PTK', '').
card_first_print('shu foot soldiers', 'PTK').
card_image_name('shu foot soldiers'/'PTK', 'shu foot soldiers').
card_uid('shu foot soldiers'/'PTK', 'PTK:Shu Foot Soldiers:shu foot soldiers').
card_rarity('shu foot soldiers'/'PTK', 'Common').
card_artist('shu foot soldiers'/'PTK', 'Xu Tan').
card_number('shu foot soldiers'/'PTK', '24').
card_flavor_text('shu foot soldiers'/'PTK', 'Liu Bei lost many men at the battle of Runan because of his lack of strategy. It wasn\'t until he met Kongming that he began to truly succeed as a leader.').
card_multiverse_id('shu foot soldiers'/'PTK', '10489').

card_in_set('shu general', 'PTK').
card_original_type('shu general'/'PTK', 'Creature — Soldier').
card_original_text('shu general'/'PTK', 'Horsemanship\nAttacking doesn\'t cause Shu General to tap.').
card_first_print('shu general', 'PTK').
card_image_name('shu general'/'PTK', 'shu general').
card_uid('shu general'/'PTK', 'PTK:Shu General:shu general').
card_rarity('shu general'/'PTK', 'Uncommon').
card_artist('shu general'/'PTK', 'Li Xiaohua').
card_number('shu general'/'PTK', '25').
card_multiverse_id('shu general'/'PTK', '10502').

card_in_set('shu grain caravan', 'PTK').
card_original_type('shu grain caravan'/'PTK', 'Creature — Soldiers').
card_original_text('shu grain caravan'/'PTK', 'When Shu Grain Caravan comes into play, you gain 2 life.').
card_first_print('shu grain caravan', 'PTK').
card_image_name('shu grain caravan'/'PTK', 'shu grain caravan').
card_uid('shu grain caravan'/'PTK', 'PTK:Shu Grain Caravan:shu grain caravan').
card_rarity('shu grain caravan'/'PTK', 'Common').
card_artist('shu grain caravan'/'PTK', 'Li Wang').
card_number('shu grain caravan'/'PTK', '26').
card_flavor_text('shu grain caravan'/'PTK', 'Keeping a million-man army fed was no easy task. Grain and rice caravans were the lifeblood of the empire.').
card_multiverse_id('shu grain caravan'/'PTK', '10617').

card_in_set('shu soldier-farmers', 'PTK').
card_original_type('shu soldier-farmers'/'PTK', 'Creature — Soldiers').
card_original_text('shu soldier-farmers'/'PTK', 'When Shu Soldier-Farmers comes into play, you gain 4 life.').
card_first_print('shu soldier-farmers', 'PTK').
card_image_name('shu soldier-farmers'/'PTK', 'shu soldier-farmers').
card_uid('shu soldier-farmers'/'PTK', 'PTK:Shu Soldier-Farmers:shu soldier-farmers').
card_rarity('shu soldier-farmers'/'PTK', 'Uncommon').
card_artist('shu soldier-farmers'/'PTK', 'Li Xiaohua').
card_number('shu soldier-farmers'/'PTK', '27').
card_flavor_text('shu soldier-farmers'/'PTK', 'During Kongming\'s campaigns against the Wei, his Shu troops rotated from the battlefront to the fields every hundred days.').
card_multiverse_id('shu soldier-farmers'/'PTK', '10488').

card_in_set('sima yi, wei field marshal', 'PTK').
card_original_type('sima yi, wei field marshal'/'PTK', 'Creature — Legend').
card_original_text('sima yi, wei field marshal'/'PTK', 'Sima Yi has power equal to the number of swamp cards you have in play. (This includes both tapped and untapped swamp cards.)').
card_first_print('sima yi, wei field marshal', 'PTK').
card_image_name('sima yi, wei field marshal'/'PTK', 'sima yi, wei field marshal').
card_uid('sima yi, wei field marshal'/'PTK', 'PTK:Sima Yi, Wei Field Marshal:sima yi, wei field marshal').
card_rarity('sima yi, wei field marshal'/'PTK', 'Rare').
card_artist('sima yi, wei field marshal'/'PTK', 'Gao Yan').
card_number('sima yi, wei field marshal'/'PTK', '82').
card_flavor_text('sima yi, wei field marshal'/'PTK', 'Sima Yi fought for four generations of the Cao family before his own grandson became emperor and united the three kingdoms.').
card_multiverse_id('sima yi, wei field marshal'/'PTK', '10630').

card_in_set('slashing tiger', 'PTK').
card_original_type('slashing tiger'/'PTK', 'Creature — Tiger').
card_original_text('slashing tiger'/'PTK', 'When Slashing Tiger attacks and is blocked, it gets +2/+2 until the end of the turn.').
card_first_print('slashing tiger', 'PTK').
card_image_name('slashing tiger'/'PTK', 'slashing tiger').
card_uid('slashing tiger'/'PTK', 'PTK:Slashing Tiger:slashing tiger').
card_rarity('slashing tiger'/'PTK', 'Rare').
card_artist('slashing tiger'/'PTK', 'Yang Jun Kwon').
card_number('slashing tiger'/'PTK', '145').
card_flavor_text('slashing tiger'/'PTK', '\"Unless you enter the tiger\'s lair, you cannot get hold of the tiger\'s cubs.\"\n—Sun Tzu, Art of War (trans. Giles)').
card_multiverse_id('slashing tiger'/'PTK', '10591').

card_in_set('southern elephant', 'PTK').
card_original_type('southern elephant'/'PTK', 'Creature — Elephant').
card_original_text('southern elephant'/'PTK', '').
card_first_print('southern elephant', 'PTK').
card_image_name('southern elephant'/'PTK', 'southern elephant').
card_uid('southern elephant'/'PTK', 'PTK:Southern Elephant:southern elephant').
card_rarity('southern elephant'/'PTK', 'Common').
card_artist('southern elephant'/'PTK', 'Wang Yuqun').
card_number('southern elephant'/'PTK', '146').
card_flavor_text('southern elephant'/'PTK', 'While defending their southern borders, both the Wu and Shu kingdoms fought against the barbarians\' trained elephants.').
card_multiverse_id('southern elephant'/'PTK', '10545').

card_in_set('spoils of victory', 'PTK').
card_original_type('spoils of victory'/'PTK', 'Sorcery').
card_original_text('spoils of victory'/'PTK', 'Search your library for a plains, island, swamp, mountain, or forest card and put that land into play. Shuffle your library afterward.').
card_first_print('spoils of victory', 'PTK').
card_image_name('spoils of victory'/'PTK', 'spoils of victory').
card_uid('spoils of victory'/'PTK', 'PTK:Spoils of Victory:spoils of victory').
card_rarity('spoils of victory'/'PTK', 'Uncommon').
card_artist('spoils of victory'/'PTK', 'Sun Nan').
card_number('spoils of victory'/'PTK', '147').
card_multiverse_id('spoils of victory'/'PTK', '10629').

card_in_set('spring of eternal peace', 'PTK').
card_original_type('spring of eternal peace'/'PTK', 'Sorcery').
card_original_text('spring of eternal peace'/'PTK', 'You gain 8 life.').
card_first_print('spring of eternal peace', 'PTK').
card_image_name('spring of eternal peace'/'PTK', 'spring of eternal peace').
card_uid('spring of eternal peace'/'PTK', 'PTK:Spring of Eternal Peace:spring of eternal peace').
card_rarity('spring of eternal peace'/'PTK', 'Common').
card_artist('spring of eternal peace'/'PTK', 'Wang Yuqun').
card_number('spring of eternal peace'/'PTK', '148').
card_flavor_text('spring of eternal peace'/'PTK', 'Bathing in this spring could cure ailments of body and mind alike.').
card_multiverse_id('spring of eternal peace'/'PTK', '10566').

card_in_set('stalking tiger', 'PTK').
card_original_type('stalking tiger'/'PTK', 'Creature — Tiger').
card_original_text('stalking tiger'/'PTK', 'Stalking Tiger can\'t be blocked by more than one creature each turn.').
card_image_name('stalking tiger'/'PTK', 'stalking tiger').
card_uid('stalking tiger'/'PTK', 'PTK:Stalking Tiger:stalking tiger').
card_rarity('stalking tiger'/'PTK', 'Common').
card_artist('stalking tiger'/'PTK', 'Kang Yu').
card_number('stalking tiger'/'PTK', '149').
card_flavor_text('stalking tiger'/'PTK', 'Throughout Chinese history, generals have often been referred to as tigers for their fierceness, strength, and bravery.').
card_multiverse_id('stalking tiger'/'PTK', '10550').

card_in_set('stolen grain', 'PTK').
card_original_type('stolen grain'/'PTK', 'Sorcery').
card_original_text('stolen grain'/'PTK', 'Stolen Grain deals 5 damage to your opponent. You gain 5 life.').
card_first_print('stolen grain', 'PTK').
card_image_name('stolen grain'/'PTK', 'stolen grain').
card_uid('stolen grain'/'PTK', 'PTK:Stolen Grain:stolen grain').
card_rarity('stolen grain'/'PTK', 'Uncommon').
card_artist('stolen grain'/'PTK', 'LHQ').
card_number('stolen grain'/'PTK', '83').
card_flavor_text('stolen grain'/'PTK', 'At the battle of Guandu, Cao Cao defeated Yuan Shao by raiding his grain depot, leaving him with no way to feed his troops.').
card_multiverse_id('stolen grain'/'PTK', '10554').

card_in_set('stone catapult', 'PTK').
card_original_type('stone catapult'/'PTK', 'Creature — Soldiers').
card_original_text('stone catapult'/'PTK', 'On your turn, before you attack, you may tap Stone Catapult to destroy any one tapped creature that isn\'t black.').
card_first_print('stone catapult', 'PTK').
card_image_name('stone catapult'/'PTK', 'stone catapult').
card_uid('stone catapult'/'PTK', 'PTK:Stone Catapult:stone catapult').
card_rarity('stone catapult'/'PTK', 'Rare').
card_artist('stone catapult'/'PTK', 'Shang Huitong').
card_number('stone catapult'/'PTK', '84').
card_multiverse_id('stone catapult'/'PTK', '10508').

card_in_set('stone rain', 'PTK').
card_original_type('stone rain'/'PTK', 'Sorcery').
card_original_text('stone rain'/'PTK', 'Destroy any one land.').
card_image_name('stone rain'/'PTK', 'stone rain').
card_uid('stone rain'/'PTK', 'PTK:Stone Rain:stone rain').
card_rarity('stone rain'/'PTK', 'Common').
card_artist('stone rain'/'PTK', 'Mitsuaki Sagiri').
card_number('stone rain'/'PTK', '123').
card_flavor_text('stone rain'/'PTK', 'Soldiers often blocked off the steep mountain passes to ambush enemy troops in western and central China.').
card_multiverse_id('stone rain'/'PTK', '10606').

card_in_set('strategic planning', 'PTK').
card_original_type('strategic planning'/'PTK', 'Sorcery').
card_original_text('strategic planning'/'PTK', 'Look at the top three cards of your library. Put one of them into your hand and the rest into your graveyard.').
card_first_print('strategic planning', 'PTK').
card_image_name('strategic planning'/'PTK', 'strategic planning').
card_uid('strategic planning'/'PTK', 'PTK:Strategic Planning:strategic planning').
card_rarity('strategic planning'/'PTK', 'Uncommon').
card_artist('strategic planning'/'PTK', 'Zhang Jiazhen').
card_number('strategic planning'/'PTK', '53').
card_flavor_text('strategic planning'/'PTK', '\"Plans evolved within the tent decide a victory 1,000 li away.\"').
card_multiverse_id('strategic planning'/'PTK', '10602').

card_in_set('straw soldiers', 'PTK').
card_original_type('straw soldiers'/'PTK', 'Creature — Soldiers').
card_original_text('straw soldiers'/'PTK', '').
card_first_print('straw soldiers', 'PTK').
card_image_name('straw soldiers'/'PTK', 'straw soldiers').
card_uid('straw soldiers'/'PTK', 'PTK:Straw Soldiers:straw soldiers').
card_rarity('straw soldiers'/'PTK', 'Common').
card_artist('straw soldiers'/'PTK', 'Cai Tingting').
card_number('straw soldiers'/'PTK', '54').
card_flavor_text('straw soldiers'/'PTK', 'The overnight appearance of miles of \"armed\" Wu forts at Guangling frightened a much vaster Wei force into fleeing for their lives.').
card_multiverse_id('straw soldiers'/'PTK', '10613').

card_in_set('sun ce, young conquerer', 'PTK').
card_original_type('sun ce, young conquerer'/'PTK', 'Creature — Legend').
card_original_text('sun ce, young conquerer'/'PTK', 'Horsemanship\nWhen Sun Ce comes into play, you may return any one creature from play to its owner\'s hand.').
card_first_print('sun ce, young conquerer', 'PTK').
card_image_name('sun ce, young conquerer'/'PTK', 'sun ce, young conquerer').
card_uid('sun ce, young conquerer'/'PTK', 'PTK:Sun Ce, Young Conquerer:sun ce, young conquerer').
card_rarity('sun ce, young conquerer'/'PTK', 'Rare').
card_artist('sun ce, young conquerer'/'PTK', 'Yang Guangmai').
card_number('sun ce, young conquerer'/'PTK', '55').
card_multiverse_id('sun ce, young conquerer'/'PTK', '10709').

card_in_set('sun quan, lord of wu', 'PTK').
card_original_type('sun quan, lord of wu'/'PTK', 'Creature — Legend').
card_original_text('sun quan, lord of wu'/'PTK', 'All your creatures gain horsemanship as long as Sun Quan is in play. (This includes Sun Quan.)').
card_first_print('sun quan, lord of wu', 'PTK').
card_image_name('sun quan, lord of wu'/'PTK', 'sun quan, lord of wu').
card_uid('sun quan, lord of wu'/'PTK', 'PTK:Sun Quan, Lord of Wu:sun quan, lord of wu').
card_rarity('sun quan, lord of wu'/'PTK', 'Rare').
card_artist('sun quan, lord of wu'/'PTK', 'Xu Xiaoming').
card_number('sun quan, lord of wu'/'PTK', '56').
card_flavor_text('sun quan, lord of wu'/'PTK', '\"One score and four he reigned, the Southland king: / A dragon coiled, a tiger poised below the mighty Yangtze.\"').
card_multiverse_id('sun quan, lord of wu'/'PTK', '10513').

card_in_set('swamp', 'PTK').
card_original_type('swamp'/'PTK', 'Land').
card_original_text('swamp'/'PTK', 'B').
card_image_name('swamp'/'PTK', 'swamp1').
card_uid('swamp'/'PTK', 'PTK:Swamp:swamp1').
card_rarity('swamp'/'PTK', 'Basic Land').
card_artist('swamp'/'PTK', 'Wang Chuxiong').
card_number('swamp'/'PTK', '172').
card_multiverse_id('swamp'/'PTK', '10607').

card_in_set('swamp', 'PTK').
card_original_type('swamp'/'PTK', 'Land').
card_original_text('swamp'/'PTK', 'B').
card_image_name('swamp'/'PTK', 'swamp2').
card_uid('swamp'/'PTK', 'PTK:Swamp:swamp2').
card_rarity('swamp'/'PTK', 'Basic Land').
card_artist('swamp'/'PTK', 'Wang Chuxiong').
card_number('swamp'/'PTK', '173').
card_multiverse_id('swamp'/'PTK', '10647').

card_in_set('swamp', 'PTK').
card_original_type('swamp'/'PTK', 'Land').
card_original_text('swamp'/'PTK', 'B').
card_image_name('swamp'/'PTK', 'swamp3').
card_uid('swamp'/'PTK', 'PTK:Swamp:swamp3').
card_rarity('swamp'/'PTK', 'Basic Land').
card_artist('swamp'/'PTK', 'Wang Chuxiong').
card_number('swamp'/'PTK', '174').
card_multiverse_id('swamp'/'PTK', '10648').

card_in_set('taoist hermit', 'PTK').
card_original_type('taoist hermit'/'PTK', 'Creature — Mystic').
card_original_text('taoist hermit'/'PTK', 'Whenever your opponent chooses a creature in play, he or she can\'t choose Taoist Hermit.').
card_first_print('taoist hermit', 'PTK').
card_image_name('taoist hermit'/'PTK', 'taoist hermit').
card_uid('taoist hermit'/'PTK', 'PTK:Taoist Hermit:taoist hermit').
card_rarity('taoist hermit'/'PTK', 'Uncommon').
card_artist('taoist hermit'/'PTK', 'Wang Yuqun').
card_number('taoist hermit'/'PTK', '150').
card_flavor_text('taoist hermit'/'PTK', 'Taoists chose to be hermits for many reasons, but all shared one unchanging goal: to follow the Tao.').
card_multiverse_id('taoist hermit'/'PTK', '10569').

card_in_set('taoist mystic', 'PTK').
card_original_type('taoist mystic'/'PTK', 'Creature — Mystic').
card_original_text('taoist mystic'/'PTK', 'Taoist Mystic can\'t be blocked by creatures with horsemanship.').
card_first_print('taoist mystic', 'PTK').
card_image_name('taoist mystic'/'PTK', 'taoist mystic').
card_uid('taoist mystic'/'PTK', 'PTK:Taoist Mystic:taoist mystic').
card_rarity('taoist mystic'/'PTK', 'Rare').
card_artist('taoist mystic'/'PTK', 'Qu Xin').
card_number('taoist mystic'/'PTK', '151').
card_flavor_text('taoist mystic'/'PTK', 'By appearing in places miles apart at the same time, Zuo Ci exhibited the mystic\'s ability to \"shrink the land.\"').
card_multiverse_id('taoist mystic'/'PTK', '10733').

card_in_set('taunting challenge', 'PTK').
card_original_type('taunting challenge'/'PTK', 'Sorcery').
card_original_text('taunting challenge'/'PTK', 'Choose any one creature. This turn, all creatures able to block it do so.').
card_first_print('taunting challenge', 'PTK').
card_image_name('taunting challenge'/'PTK', 'taunting challenge').
card_uid('taunting challenge'/'PTK', 'PTK:Taunting Challenge:taunting challenge').
card_rarity('taunting challenge'/'PTK', 'Rare').
card_artist('taunting challenge'/'PTK', 'Gao Yan').
card_number('taunting challenge'/'PTK', '152').
card_flavor_text('taunting challenge'/'PTK', 'Incensed by their opponents\' unrelenting taunts, even wise generals were known to rashly lead their troops into battle—often to disastrous defeats.').
card_multiverse_id('taunting challenge'/'PTK', '10492').

card_in_set('three visits', 'PTK').
card_original_type('three visits'/'PTK', 'Sorcery').
card_original_text('three visits'/'PTK', 'Search your library for a forest card and put that forest into play. Shuffle your library afterward.').
card_first_print('three visits', 'PTK').
card_image_name('three visits'/'PTK', 'three visits').
card_uid('three visits'/'PTK', 'PTK:Three Visits:three visits').
card_rarity('three visits'/'PTK', 'Common').
card_artist('three visits'/'PTK', 'Qu Xin').
card_number('three visits'/'PTK', '153').
card_flavor_text('three visits'/'PTK', '\"Trying to meet a worthy man in the wrong way is as bad as closing the door on an invited guest.\"\n—Mencius, a Chinese philosopher').
card_multiverse_id('three visits'/'PTK', '10567').

card_in_set('trained cheetah', 'PTK').
card_original_type('trained cheetah'/'PTK', 'Creature — Cheetah').
card_original_text('trained cheetah'/'PTK', 'When Trained Cheetah attacks and is blocked, it gets +1/+1 until the end of the turn.').
card_first_print('trained cheetah', 'PTK').
card_image_name('trained cheetah'/'PTK', 'trained cheetah').
card_uid('trained cheetah'/'PTK', 'PTK:Trained Cheetah:trained cheetah').
card_rarity('trained cheetah'/'PTK', 'Uncommon').
card_artist('trained cheetah'/'PTK', 'Fang Yue').
card_number('trained cheetah'/'PTK', '154').
card_flavor_text('trained cheetah'/'PTK', '\"King Mulu\'s cheetahs came riding on the winds, charging, with fangs bared and claws flexed.\"').
card_multiverse_id('trained cheetah'/'PTK', '10573').

card_in_set('trained jackal', 'PTK').
card_original_type('trained jackal'/'PTK', 'Creature — Jackal').
card_original_text('trained jackal'/'PTK', '').
card_first_print('trained jackal', 'PTK').
card_image_name('trained jackal'/'PTK', 'trained jackal').
card_uid('trained jackal'/'PTK', 'PTK:Trained Jackal:trained jackal').
card_rarity('trained jackal'/'PTK', 'Common').
card_artist('trained jackal'/'PTK', 'Yang Jun Kwon').
card_number('trained jackal'/'PTK', '155').
card_flavor_text('trained jackal'/'PTK', 'To taunt a man into action, call him a coward. To insult him beyond forgiveness, call him a jackal.').
card_multiverse_id('trained jackal'/'PTK', '10571').

card_in_set('trip wire', 'PTK').
card_original_type('trip wire'/'PTK', 'Sorcery').
card_original_text('trip wire'/'PTK', 'Destroy any one creature with horsemanship.').
card_first_print('trip wire', 'PTK').
card_image_name('trip wire'/'PTK', 'trip wire').
card_uid('trip wire'/'PTK', 'PTK:Trip Wire:trip wire').
card_rarity('trip wire'/'PTK', 'Uncommon').
card_artist('trip wire'/'PTK', 'Hong Yan').
card_number('trip wire'/'PTK', '156').
card_flavor_text('trip wire'/'PTK', 'Trip wire, hooked poles, and sunken pits were commonly used to unhorse riders during the Three Kingdoms period.').
card_multiverse_id('trip wire'/'PTK', '10625').

card_in_set('vengeance', 'PTK').
card_original_type('vengeance'/'PTK', 'Sorcery').
card_original_text('vengeance'/'PTK', 'Destroy any one tapped creature.').
card_image_name('vengeance'/'PTK', 'vengeance').
card_uid('vengeance'/'PTK', 'PTK:Vengeance:vengeance').
card_rarity('vengeance'/'PTK', 'Uncommon').
card_artist('vengeance'/'PTK', 'Solomon Au Yeung').
card_number('vengeance'/'PTK', '28').
card_flavor_text('vengeance'/'PTK', '\"Until I\'ve gnawed their flesh and exterminated their clans, my humiliation will not be effaced.\"\n—Liu Bei, on the death of\nhis oath-brother Guan Yu').
card_multiverse_id('vengeance'/'PTK', '10631').

card_in_set('virtuous charge', 'PTK').
card_original_type('virtuous charge'/'PTK', 'Sorcery').
card_original_text('virtuous charge'/'PTK', 'All your creatures get +1/+1 until the end of the turn.').
card_first_print('virtuous charge', 'PTK').
card_image_name('virtuous charge'/'PTK', 'virtuous charge').
card_uid('virtuous charge'/'PTK', 'PTK:Virtuous Charge:virtuous charge').
card_rarity('virtuous charge'/'PTK', 'Common').
card_artist('virtuous charge'/'PTK', 'Qu Xin').
card_number('virtuous charge'/'PTK', '29').
card_flavor_text('virtuous charge'/'PTK', '\"The empire belongs to no one man but to all in the empire. He who has virtue shall possess it.\"').
card_multiverse_id('virtuous charge'/'PTK', '10596').

card_in_set('volunteer militia', 'PTK').
card_original_type('volunteer militia'/'PTK', 'Creature — Soldier').
card_original_text('volunteer militia'/'PTK', '').
card_image_name('volunteer militia'/'PTK', 'volunteer militia').
card_uid('volunteer militia'/'PTK', 'PTK:Volunteer Militia:volunteer militia').
card_rarity('volunteer militia'/'PTK', 'Common').
card_artist('volunteer militia'/'PTK', 'Lin Yan').
card_number('volunteer militia'/'PTK', '30').
card_flavor_text('volunteer militia'/'PTK', 'After making the Peach Garden Oath, the three brothers raised an army of 500 youths to fight the Yellow Scarves rebellion.').
card_multiverse_id('volunteer militia'/'PTK', '10483').

card_in_set('warrior\'s oath', 'PTK').
card_original_type('warrior\'s oath'/'PTK', 'Sorcery').
card_original_text('warrior\'s oath'/'PTK', 'Take another turn after this one. At the end of that turn, you lose the game. (You don\'t lose if you\'ve already won.)').
card_first_print('warrior\'s oath', 'PTK').
card_image_name('warrior\'s oath'/'PTK', 'warrior\'s oath').
card_uid('warrior\'s oath'/'PTK', 'PTK:Warrior\'s Oath:warrior\'s oath').
card_rarity('warrior\'s oath'/'PTK', 'Rare').
card_artist('warrior\'s oath'/'PTK', 'Mitsuaki Sagiri').
card_number('warrior\'s oath'/'PTK', '124').
card_flavor_text('warrior\'s oath'/'PTK', '\"If I fail, my head is yours.\"\n—Warrior\'s oath common\nduring the Three Kingdoms period').
card_multiverse_id('warrior\'s oath'/'PTK', '10552').

card_in_set('warrior\'s stand', 'PTK').
card_original_type('warrior\'s stand'/'PTK', 'Sorcery').
card_original_text('warrior\'s stand'/'PTK', 'Play Warrior\'s Stand only after you\'re attacked, before you declare blockers.\nAll your creatures get +2/+2 until the end of the turn.').
card_image_name('warrior\'s stand'/'PTK', 'warrior\'s stand').
card_uid('warrior\'s stand'/'PTK', 'PTK:Warrior\'s Stand:warrior\'s stand').
card_rarity('warrior\'s stand'/'PTK', 'Uncommon').
card_artist('warrior\'s stand'/'PTK', 'Zhao Dafu').
card_number('warrior\'s stand'/'PTK', '31').
card_multiverse_id('warrior\'s stand'/'PTK', '10633').

card_in_set('wei ambush force', 'PTK').
card_original_type('wei ambush force'/'PTK', 'Creature — Soldiers').
card_original_text('wei ambush force'/'PTK', 'When Wei Ambush Force attacks, it gets +2/+0 until the end of the turn.').
card_first_print('wei ambush force', 'PTK').
card_image_name('wei ambush force'/'PTK', 'wei ambush force').
card_uid('wei ambush force'/'PTK', 'PTK:Wei Ambush Force:wei ambush force').
card_rarity('wei ambush force'/'PTK', 'Common').
card_artist('wei ambush force'/'PTK', 'Ku Xueming').
card_number('wei ambush force'/'PTK', '85').
card_flavor_text('wei ambush force'/'PTK', 'The battle of Puyang marked the beginning of the end for Lu Bu. He lost the city—and later his life—to Cao Cao.').
card_multiverse_id('wei ambush force'/'PTK', '10557').

card_in_set('wei assassins', 'PTK').
card_original_type('wei assassins'/'PTK', 'Creature — Soldiers').
card_original_text('wei assassins'/'PTK', 'When Wei Assassins comes into play, your opponent chooses one of his or her creatures. Destroy that creature. (Ignore this effect if your opponent has no creatures in play.)').
card_first_print('wei assassins', 'PTK').
card_image_name('wei assassins'/'PTK', 'wei assassins').
card_uid('wei assassins'/'PTK', 'PTK:Wei Assassins:wei assassins').
card_rarity('wei assassins'/'PTK', 'Uncommon').
card_artist('wei assassins'/'PTK', 'Xu Tan').
card_number('wei assassins'/'PTK', '86').
card_multiverse_id('wei assassins'/'PTK', '10583').

card_in_set('wei elite companions', 'PTK').
card_original_type('wei elite companions'/'PTK', 'Creature — Soldiers').
card_original_text('wei elite companions'/'PTK', 'Horsemanship').
card_first_print('wei elite companions', 'PTK').
card_image_name('wei elite companions'/'PTK', 'wei elite companions').
card_uid('wei elite companions'/'PTK', 'PTK:Wei Elite Companions:wei elite companions').
card_rarity('wei elite companions'/'PTK', 'Uncommon').
card_artist('wei elite companions'/'PTK', 'Li Youliang').
card_number('wei elite companions'/'PTK', '87').
card_flavor_text('wei elite companions'/'PTK', 'Cao Cao was more concerned with capabilities than lineage. He excelled at recruiting and retaining men of talent to serve him.').
card_multiverse_id('wei elite companions'/'PTK', '10511').

card_in_set('wei infantry', 'PTK').
card_original_type('wei infantry'/'PTK', 'Creature — Soldiers').
card_original_text('wei infantry'/'PTK', '').
card_first_print('wei infantry', 'PTK').
card_image_name('wei infantry'/'PTK', 'wei infantry').
card_uid('wei infantry'/'PTK', 'PTK:Wei Infantry:wei infantry').
card_rarity('wei infantry'/'PTK', 'Common').
card_artist('wei infantry'/'PTK', 'LHQ').
card_number('wei infantry'/'PTK', '88').
card_flavor_text('wei infantry'/'PTK', 'Wei won the battle of Hefei when Zhang Liao defeated Sun Quan and then foiled a Wu scheme to incite rebellion.').
card_multiverse_id('wei infantry'/'PTK', '10519').

card_in_set('wei night raiders', 'PTK').
card_original_type('wei night raiders'/'PTK', 'Creature — Soldiers').
card_original_text('wei night raiders'/'PTK', 'Horsemanship\nWhen Wei Night Raiders successfully damages your opponent, he or she chooses and discards a card from his or her hand. (Ignore this effect if your opponent doesn\'t have any cards.)').
card_first_print('wei night raiders', 'PTK').
card_image_name('wei night raiders'/'PTK', 'wei night raiders').
card_uid('wei night raiders'/'PTK', 'PTK:Wei Night Raiders:wei night raiders').
card_rarity('wei night raiders'/'PTK', 'Uncommon').
card_artist('wei night raiders'/'PTK', 'Wang Feng').
card_number('wei night raiders'/'PTK', '89').
card_multiverse_id('wei night raiders'/'PTK', '10484').

card_in_set('wei scout', 'PTK').
card_original_type('wei scout'/'PTK', 'Creature — Soldier').
card_original_text('wei scout'/'PTK', 'Horsemanship').
card_first_print('wei scout', 'PTK').
card_image_name('wei scout'/'PTK', 'wei scout').
card_uid('wei scout'/'PTK', 'PTK:Wei Scout:wei scout').
card_rarity('wei scout'/'PTK', 'Common').
card_artist('wei scout'/'PTK', 'Jiang Zhuqing').
card_number('wei scout'/'PTK', '90').
card_flavor_text('wei scout'/'PTK', 'He will win who, prepared himself, waits to take the enemy unprepared.\"\n—Sun Tzu, Art of War (trans. Giles)').
card_multiverse_id('wei scout'/'PTK', '10517').

card_in_set('wei strike force', 'PTK').
card_original_type('wei strike force'/'PTK', 'Creature — Soldiers').
card_original_text('wei strike force'/'PTK', 'Horsemanship').
card_first_print('wei strike force', 'PTK').
card_image_name('wei strike force'/'PTK', 'wei strike force').
card_uid('wei strike force'/'PTK', 'PTK:Wei Strike Force:wei strike force').
card_rarity('wei strike force'/'PTK', 'Common').
card_artist('wei strike force'/'PTK', 'Tang Xiaogu').
card_number('wei strike force'/'PTK', '91').
card_flavor_text('wei strike force'/'PTK', 'At the battle of Chang\'an, Ma Chao defeated the two generals Cao Cao sent to guard the pass but was forced to flee when Cao Cao\'s trickery turned his own ally against him.').
card_multiverse_id('wei strike force'/'PTK', '10561').

card_in_set('wielding the green dragon', 'PTK').
card_original_type('wielding the green dragon'/'PTK', 'Sorcery').
card_original_text('wielding the green dragon'/'PTK', 'Any one creature gets +4/+4 until the end of the turn.').
card_first_print('wielding the green dragon', 'PTK').
card_image_name('wielding the green dragon'/'PTK', 'wielding the green dragon').
card_uid('wielding the green dragon'/'PTK', 'PTK:Wielding the Green Dragon:wielding the green dragon').
card_rarity('wielding the green dragon'/'PTK', 'Common').
card_artist('wielding the green dragon'/'PTK', 'Quan Xuejun').
card_number('wielding the green dragon'/'PTK', '157').
card_flavor_text('wielding the green dragon'/'PTK', 'Named for the eastern part of the sky, the source of energy and renewal, Guan Yu\'s crescent-moon blade weighed over 100 pounds.').
card_multiverse_id('wielding the green dragon'/'PTK', '10562').

card_in_set('wolf pack', 'PTK').
card_original_type('wolf pack'/'PTK', 'Creature — Wolves').
card_original_text('wolf pack'/'PTK', 'When Wolf Pack attacks and is blocked, you may have it deal its damage to the defending player instead of to the creatures blocking it.').
card_first_print('wolf pack', 'PTK').
card_image_name('wolf pack'/'PTK', 'wolf pack').
card_uid('wolf pack'/'PTK', 'PTK:Wolf Pack:wolf pack').
card_rarity('wolf pack'/'PTK', 'Rare').
card_artist('wolf pack'/'PTK', 'Yang Jun Kwon').
card_number('wolf pack'/'PTK', '158').
card_multiverse_id('wolf pack'/'PTK', '10522').

card_in_set('wu admiral', 'PTK').
card_original_type('wu admiral'/'PTK', 'Creature — Soldier').
card_original_text('wu admiral'/'PTK', 'As long as your opponent has an island in play, Wu Admiral gets +1/+1.').
card_first_print('wu admiral', 'PTK').
card_image_name('wu admiral'/'PTK', 'wu admiral').
card_uid('wu admiral'/'PTK', 'PTK:Wu Admiral:wu admiral').
card_rarity('wu admiral'/'PTK', 'Uncommon').
card_artist('wu admiral'/'PTK', 'Zhang Jiazhen').
card_number('wu admiral'/'PTK', '57').
card_flavor_text('wu admiral'/'PTK', 'The Wu kingdom\'s well-trained admirals were integral to the Southlands\' victory at Red Cliffs as well as the kingdom\'s defense.').
card_multiverse_id('wu admiral'/'PTK', '10485').

card_in_set('wu elite cavalry', 'PTK').
card_original_type('wu elite cavalry'/'PTK', 'Creature — Soldiers').
card_original_text('wu elite cavalry'/'PTK', 'Horsemanship').
card_first_print('wu elite cavalry', 'PTK').
card_image_name('wu elite cavalry'/'PTK', 'wu elite cavalry').
card_uid('wu elite cavalry'/'PTK', 'PTK:Wu Elite Cavalry:wu elite cavalry').
card_rarity('wu elite cavalry'/'PTK', 'Common').
card_artist('wu elite cavalry'/'PTK', 'Li Wang').
card_number('wu elite cavalry'/'PTK', '58').
card_flavor_text('wu elite cavalry'/'PTK', 'At the second battle of Ruxu, the brave Wu general Gan Ning raided Cao Cao\'s camp of 400,000 men with only 100 cavalry. Not a single man or horse was lost.').
card_multiverse_id('wu elite cavalry'/'PTK', '10611').

card_in_set('wu infantry', 'PTK').
card_original_type('wu infantry'/'PTK', 'Creature — Soldiers').
card_original_text('wu infantry'/'PTK', '').
card_first_print('wu infantry', 'PTK').
card_image_name('wu infantry'/'PTK', 'wu infantry').
card_uid('wu infantry'/'PTK', 'PTK:Wu Infantry:wu infantry').
card_rarity('wu infantry'/'PTK', 'Common').
card_artist('wu infantry'/'PTK', 'Xu Xiaoming').
card_number('wu infantry'/'PTK', '59').
card_flavor_text('wu infantry'/'PTK', 'The first battle of Hefei was Sun Quan\'s last as a field general. From then on he let his generals command in the field while he directed battle from behind the front lines.').
card_multiverse_id('wu infantry'/'PTK', '10600').

card_in_set('wu light cavalry', 'PTK').
card_original_type('wu light cavalry'/'PTK', 'Creature — Soldiers').
card_original_text('wu light cavalry'/'PTK', 'Horsemanship').
card_first_print('wu light cavalry', 'PTK').
card_image_name('wu light cavalry'/'PTK', 'wu light cavalry').
card_uid('wu light cavalry'/'PTK', 'PTK:Wu Light Cavalry:wu light cavalry').
card_rarity('wu light cavalry'/'PTK', 'Common').
card_artist('wu light cavalry'/'PTK', 'Huang Qishi').
card_number('wu light cavalry'/'PTK', '60').
card_flavor_text('wu light cavalry'/'PTK', 'A cunning strategist, Cao Ren tricked Zhou Yu by letting him enter Nanjun. Zhou Yu thought he was capturing the city until Cao Ren\'s arrows rained down upon him.').
card_multiverse_id('wu light cavalry'/'PTK', '10615').

card_in_set('wu longbowman', 'PTK').
card_original_type('wu longbowman'/'PTK', 'Creature — Soldier').
card_original_text('wu longbowman'/'PTK', 'On your turn, before you attack, you may tap Wu Longbowman to have it deal 1 damage to any one creature or player.').
card_first_print('wu longbowman', 'PTK').
card_image_name('wu longbowman'/'PTK', 'wu longbowman').
card_uid('wu longbowman'/'PTK', 'PTK:Wu Longbowman:wu longbowman').
card_rarity('wu longbowman'/'PTK', 'Uncommon').
card_artist('wu longbowman'/'PTK', 'Xu Tan').
card_number('wu longbowman'/'PTK', '61').
card_multiverse_id('wu longbowman'/'PTK', '10498').

card_in_set('wu scout', 'PTK').
card_original_type('wu scout'/'PTK', 'Creature — Soldier').
card_original_text('wu scout'/'PTK', 'Horsemanship\nWhen Wu Scout comes into play, look at your opponent\'s hand.').
card_first_print('wu scout', 'PTK').
card_image_name('wu scout'/'PTK', 'wu scout').
card_uid('wu scout'/'PTK', 'PTK:Wu Scout:wu scout').
card_rarity('wu scout'/'PTK', 'Common').
card_artist('wu scout'/'PTK', 'Jiaming').
card_number('wu scout'/'PTK', '62').
card_multiverse_id('wu scout'/'PTK', '10612').

card_in_set('wu spy', 'PTK').
card_original_type('wu spy'/'PTK', 'Creature — Soldier').
card_original_text('wu spy'/'PTK', 'When Wu Spy comes into play, look at the top two cards of any player\'s library. Put one of them on the top of that player\'s library and the other in his or her graveyard.').
card_first_print('wu spy', 'PTK').
card_image_name('wu spy'/'PTK', 'wu spy').
card_uid('wu spy'/'PTK', 'PTK:Wu Spy:wu spy').
card_rarity('wu spy'/'PTK', 'Uncommon').
card_artist('wu spy'/'PTK', 'Zhao Tan').
card_number('wu spy'/'PTK', '63').
card_multiverse_id('wu spy'/'PTK', '10529').

card_in_set('wu warship', 'PTK').
card_original_type('wu warship'/'PTK', 'Creature — Ship').
card_original_text('wu warship'/'PTK', 'Wu Warship can\'t attack unless the defending player has an island in play.').
card_first_print('wu warship', 'PTK').
card_image_name('wu warship'/'PTK', 'wu warship').
card_uid('wu warship'/'PTK', 'PTK:Wu Warship:wu warship').
card_rarity('wu warship'/'PTK', 'Common').
card_artist('wu warship'/'PTK', 'Jiang Zhuqing').
card_number('wu warship'/'PTK', '64').
card_flavor_text('wu warship'/'PTK', 'Both Wu and Wei warships patrolled the Yangtze River, the natural border between the two kingdoms.').
card_multiverse_id('wu warship'/'PTK', '10605').

card_in_set('xiahou dun, the one-eyed', 'PTK').
card_original_type('xiahou dun, the one-eyed'/'PTK', 'Creature — Legend').
card_original_text('xiahou dun, the one-eyed'/'PTK', 'Horsemanship\nOn your turn, before you attack, you may put Xiahou Dun into your graveyard to return a black card from your graveyard to your hand.').
card_image_name('xiahou dun, the one-eyed'/'PTK', 'xiahou dun, the one-eyed').
card_uid('xiahou dun, the one-eyed'/'PTK', 'PTK:Xiahou Dun, the One-Eyed:xiahou dun, the one-eyed').
card_rarity('xiahou dun, the one-eyed'/'PTK', 'Rare').
card_artist('xiahou dun, the one-eyed'/'PTK', 'Junko Taguchi').
card_number('xiahou dun, the one-eyed'/'PTK', '92').
card_multiverse_id('xiahou dun, the one-eyed'/'PTK', '10590').

card_in_set('xun yu, wei advisor', 'PTK').
card_original_type('xun yu, wei advisor'/'PTK', 'Creature — Legend').
card_original_text('xun yu, wei advisor'/'PTK', 'On your turn, before you attack, you may tap Xun Yu to give one of your creatures +2/+0 until the end of the turn.').
card_first_print('xun yu, wei advisor', 'PTK').
card_image_name('xun yu, wei advisor'/'PTK', 'xun yu, wei advisor').
card_uid('xun yu, wei advisor'/'PTK', 'PTK:Xun Yu, Wei Advisor:xun yu, wei advisor').
card_rarity('xun yu, wei advisor'/'PTK', 'Rare').
card_artist('xun yu, wei advisor'/'PTK', 'Jack Wei').
card_number('xun yu, wei advisor'/'PTK', '93').
card_flavor_text('xun yu, wei advisor'/'PTK', '\"A splendid talent, admired of all men! His folly lay in serving Cao Cao\'s power.\"').
card_multiverse_id('xun yu, wei advisor'/'PTK', '13831').

card_in_set('yellow scarves cavalry', 'PTK').
card_original_type('yellow scarves cavalry'/'PTK', 'Creature — Soldiers').
card_original_text('yellow scarves cavalry'/'PTK', 'Horsemanship\nYellow Scarves Cavalry can\'t block.').
card_first_print('yellow scarves cavalry', 'PTK').
card_image_name('yellow scarves cavalry'/'PTK', 'yellow scarves cavalry').
card_uid('yellow scarves cavalry'/'PTK', 'PTK:Yellow Scarves Cavalry:yellow scarves cavalry').
card_rarity('yellow scarves cavalry'/'PTK', 'Common').
card_artist('yellow scarves cavalry'/'PTK', 'Chen Weidong').
card_number('yellow scarves cavalry'/'PTK', '125').
card_multiverse_id('yellow scarves cavalry'/'PTK', '10537').

card_in_set('yellow scarves general', 'PTK').
card_original_type('yellow scarves general'/'PTK', 'Creature — Soldier').
card_original_text('yellow scarves general'/'PTK', 'Horsemanship\nYellow Scarves General can\'t block.').
card_first_print('yellow scarves general', 'PTK').
card_image_name('yellow scarves general'/'PTK', 'yellow scarves general').
card_uid('yellow scarves general'/'PTK', 'PTK:Yellow Scarves General:yellow scarves general').
card_rarity('yellow scarves general'/'PTK', 'Rare').
card_artist('yellow scarves general'/'PTK', 'Chen Weidong').
card_number('yellow scarves general'/'PTK', '126').
card_flavor_text('yellow scarves general'/'PTK', 'Zhang Jue, leader of the Yellow Scarves rebellion, was a Taoist master and tutored his soldiers in those arts.').
card_multiverse_id('yellow scarves general'/'PTK', '10577').

card_in_set('yellow scarves troops', 'PTK').
card_original_type('yellow scarves troops'/'PTK', 'Creature — Soldiers').
card_original_text('yellow scarves troops'/'PTK', 'Yellow Scarves Troops can\'t block.').
card_first_print('yellow scarves troops', 'PTK').
card_image_name('yellow scarves troops'/'PTK', 'yellow scarves troops').
card_uid('yellow scarves troops'/'PTK', 'PTK:Yellow Scarves Troops:yellow scarves troops').
card_rarity('yellow scarves troops'/'PTK', 'Common').
card_artist('yellow scarves troops'/'PTK', 'Chen Weidong').
card_number('yellow scarves troops'/'PTK', '127').
card_flavor_text('yellow scarves troops'/'PTK', 'Over 500,000 commoners followed Zhang Jue, General of Heaven, in his attempt to overthrow the corrupt Han dynasty.').
card_multiverse_id('yellow scarves troops'/'PTK', '10542').

card_in_set('young wei recruits', 'PTK').
card_original_type('young wei recruits'/'PTK', 'Creature — Soldiers').
card_original_text('young wei recruits'/'PTK', 'Young Wei Recruits can\'t block.').
card_first_print('young wei recruits', 'PTK').
card_image_name('young wei recruits'/'PTK', 'young wei recruits').
card_uid('young wei recruits'/'PTK', 'PTK:Young Wei Recruits:young wei recruits').
card_rarity('young wei recruits'/'PTK', 'Common').
card_artist('young wei recruits'/'PTK', 'Li Youliang').
card_number('young wei recruits'/'PTK', '94').
card_flavor_text('young wei recruits'/'PTK', '\"To send the common people to war untrained is to throw them away.\"\n—Confucius, The Analects (trans. Lau)').
card_multiverse_id('young wei recruits'/'PTK', '10584').

card_in_set('yuan shao\'s infantry', 'PTK').
card_original_type('yuan shao\'s infantry'/'PTK', 'Creature — Soldiers').
card_original_text('yuan shao\'s infantry'/'PTK', 'Whenever Yuan Shao\'s Infantry attacks and no other creatures do, Yuan Shao\'s Infantry can\'t be blocked.').
card_first_print('yuan shao\'s infantry', 'PTK').
card_image_name('yuan shao\'s infantry'/'PTK', 'yuan shao\'s infantry').
card_uid('yuan shao\'s infantry'/'PTK', 'PTK:Yuan Shao\'s Infantry:yuan shao\'s infantry').
card_rarity('yuan shao\'s infantry'/'PTK', 'Uncommon').
card_artist('yuan shao\'s infantry'/'PTK', 'Lin Yan').
card_number('yuan shao\'s infantry'/'PTK', '129').
card_multiverse_id('yuan shao\'s infantry'/'PTK', '10559').

card_in_set('yuan shao, the indecisive', 'PTK').
card_original_type('yuan shao, the indecisive'/'PTK', 'Creature — Legend').
card_original_text('yuan shao, the indecisive'/'PTK', 'Horsemanship\nEach of your creatures can\'t be blocked by more than one creature each turn as long as Yuan Shao is in play.').
card_first_print('yuan shao, the indecisive', 'PTK').
card_image_name('yuan shao, the indecisive'/'PTK', 'yuan shao, the indecisive').
card_uid('yuan shao, the indecisive'/'PTK', 'PTK:Yuan Shao, the Indecisive:yuan shao, the indecisive').
card_rarity('yuan shao, the indecisive'/'PTK', 'Rare').
card_artist('yuan shao, the indecisive'/'PTK', 'Inoue Junichi').
card_number('yuan shao, the indecisive'/'PTK', '128').
card_multiverse_id('yuan shao, the indecisive'/'PTK', '10516').

card_in_set('zhang fei, fierce warrior', 'PTK').
card_original_type('zhang fei, fierce warrior'/'PTK', 'Creature — Legend').
card_original_text('zhang fei, fierce warrior'/'PTK', 'Horsemanship\nAttacking doesn\'t cause Zhang Fei to tap.').
card_first_print('zhang fei, fierce warrior', 'PTK').
card_image_name('zhang fei, fierce warrior'/'PTK', 'zhang fei, fierce warrior').
card_uid('zhang fei, fierce warrior'/'PTK', 'PTK:Zhang Fei, Fierce Warrior:zhang fei, fierce warrior').
card_rarity('zhang fei, fierce warrior'/'PTK', 'Rare').
card_artist('zhang fei, fierce warrior'/'PTK', 'Zhao Dafu').
card_number('zhang fei, fierce warrior'/'PTK', '32').
card_flavor_text('zhang fei, fierce warrior'/'PTK', 'Zhang Fei\'s uncharacteristic alliance with a defeated Riverlands general, Yan Yan, allowed Shu forces to advance through forty-five Riverlands strongpoints with no casualties.').
card_multiverse_id('zhang fei, fierce warrior'/'PTK', '10604').

card_in_set('zhang he, wei general', 'PTK').
card_original_type('zhang he, wei general'/'PTK', 'Creature — Legend').
card_original_text('zhang he, wei general'/'PTK', 'Horsemanship\nWhen Zhang He attacks, all your other creatures get +1/+0 until the end of the turn.').
card_first_print('zhang he, wei general', 'PTK').
card_image_name('zhang he, wei general'/'PTK', 'zhang he, wei general').
card_uid('zhang he, wei general'/'PTK', 'PTK:Zhang He, Wei General:zhang he, wei general').
card_rarity('zhang he, wei general'/'PTK', 'Rare').
card_artist('zhang he, wei general'/'PTK', 'Jack Wei').
card_number('zhang he, wei general'/'PTK', '95').
card_multiverse_id('zhang he, wei general'/'PTK', '10520').

card_in_set('zhang liao, hero of hefei', 'PTK').
card_original_type('zhang liao, hero of hefei'/'PTK', 'Creature — Legend').
card_original_text('zhang liao, hero of hefei'/'PTK', 'When Zhang Liao successfully damages your opponent, he or she chooses and discard a card from his or her hand. (Ignore this effect if your opponent doesn\'t have any cards.)').
card_first_print('zhang liao, hero of hefei', 'PTK').
card_image_name('zhang liao, hero of hefei'/'PTK', 'zhang liao, hero of hefei').
card_uid('zhang liao, hero of hefei'/'PTK', 'PTK:Zhang Liao, Hero of Hefei:zhang liao, hero of hefei').
card_rarity('zhang liao, hero of hefei'/'PTK', 'Rare').
card_artist('zhang liao, hero of hefei'/'PTK', 'Li Youliang').
card_number('zhang liao, hero of hefei'/'PTK', '96').
card_multiverse_id('zhang liao, hero of hefei'/'PTK', '10727').

card_in_set('zhao zilong, tiger general', 'PTK').
card_original_type('zhao zilong, tiger general'/'PTK', 'Creature — Legend').
card_original_text('zhao zilong, tiger general'/'PTK', 'Horsemanship\nWhen Zhao Zilong blocks, it gets +1/+1 until the end of the turn.').
card_first_print('zhao zilong, tiger general', 'PTK').
card_image_name('zhao zilong, tiger general'/'PTK', 'zhao zilong, tiger general').
card_uid('zhao zilong, tiger general'/'PTK', 'PTK:Zhao Zilong, Tiger General:zhao zilong, tiger general').
card_rarity('zhao zilong, tiger general'/'PTK', 'Rare').
card_artist('zhao zilong, tiger general'/'PTK', 'Quan Xuejun').
card_number('zhao zilong, tiger general'/'PTK', '33').
card_flavor_text('zhao zilong, tiger general'/'PTK', 'Zhao Zilong was a brave and noble warrior. Twice he rescued Liu Bei\'s son, Liu Shan.').
card_multiverse_id('zhao zilong, tiger general'/'PTK', '10686').

card_in_set('zhou yu, chief commander', 'PTK').
card_original_type('zhou yu, chief commander'/'PTK', 'Creature — Legend').
card_original_text('zhou yu, chief commander'/'PTK', 'Zhou Yu can\'t attack unless your opponent has an island in play.').
card_first_print('zhou yu, chief commander', 'PTK').
card_image_name('zhou yu, chief commander'/'PTK', 'zhou yu, chief commander').
card_uid('zhou yu, chief commander'/'PTK', 'PTK:Zhou Yu, Chief Commander:zhou yu, chief commander').
card_rarity('zhou yu, chief commander'/'PTK', 'Rare').
card_artist('zhou yu, chief commander'/'PTK', 'Xu Xiaoming').
card_number('zhou yu, chief commander'/'PTK', '65').
card_flavor_text('zhou yu, chief commander'/'PTK', '\"After making me, Zhou Yu, did you have to make Kongming?\"\n—Zhou Yu crying to heaven on his deathbed').
card_multiverse_id('zhou yu, chief commander'/'PTK', '10525').

card_in_set('zhuge jin, wu strategist', 'PTK').
card_original_type('zhuge jin, wu strategist'/'PTK', 'Creature — Legend').
card_original_text('zhuge jin, wu strategist'/'PTK', 'On your turn, before you attack, you may tap Zhuge Jin to make any one creature unblockable this turn.').
card_first_print('zhuge jin, wu strategist', 'PTK').
card_image_name('zhuge jin, wu strategist'/'PTK', 'zhuge jin, wu strategist').
card_uid('zhuge jin, wu strategist'/'PTK', 'PTK:Zhuge Jin, Wu Strategist:zhuge jin, wu strategist').
card_rarity('zhuge jin, wu strategist'/'PTK', 'Rare').
card_artist('zhuge jin, wu strategist'/'PTK', 'Song Shikai').
card_number('zhuge jin, wu strategist'/'PTK', '66').
card_flavor_text('zhuge jin, wu strategist'/'PTK', 'When Zhuge Jin proposed the marriage of Guan Yu\'s daugher and Sun Quan\'s heir, Guan Yu\'s arrogant refusal led to disaster.').
card_multiverse_id('zhuge jin, wu strategist'/'PTK', '10711').

card_in_set('zodiac dog', 'PTK').
card_original_type('zodiac dog'/'PTK', 'Creature — Dog').
card_original_text('zodiac dog'/'PTK', 'Mountainwalk (If defending player has a mountain in play, Zodiac Dog can\'t be blocked.)').
card_first_print('zodiac dog', 'PTK').
card_image_name('zodiac dog'/'PTK', 'zodiac dog').
card_uid('zodiac dog'/'PTK', 'PTK:Zodiac Dog:zodiac dog').
card_rarity('zodiac dog'/'PTK', 'Common').
card_artist('zodiac dog'/'PTK', 'Qi Baocheng').
card_number('zodiac dog'/'PTK', '130').
card_flavor_text('zodiac dog'/'PTK', '\". . . Jiang Wei alone still strove with might and main: / Nine times more he fought the north—in vain. . . .\"').
card_multiverse_id('zodiac dog'/'PTK', '10534').

card_in_set('zodiac dragon', 'PTK').
card_original_type('zodiac dragon'/'PTK', 'Creature — Dragon').
card_original_text('zodiac dragon'/'PTK', 'If Zodiac Dragon is put into your graveyard, you may return Zodiac Dragon to your hand.').
card_first_print('zodiac dragon', 'PTK').
card_image_name('zodiac dragon'/'PTK', 'zodiac dragon').
card_uid('zodiac dragon'/'PTK', 'PTK:Zodiac Dragon:zodiac dragon').
card_rarity('zodiac dragon'/'PTK', 'Rare').
card_artist('zodiac dragon'/'PTK', 'Qi Baocheng').
card_number('zodiac dragon'/'PTK', '131').
card_flavor_text('zodiac dragon'/'PTK', '\". . . The kingdoms three are now the stuff of dream, / For men to ponder, past all praise or blame.\"').
card_multiverse_id('zodiac dragon'/'PTK', '10510').

card_in_set('zodiac goat', 'PTK').
card_original_type('zodiac goat'/'PTK', 'Creature — Goat').
card_original_text('zodiac goat'/'PTK', 'Mountainwalk (If defending player has a mountain in play, Zodiac Goat can\'t be blocked.)').
card_first_print('zodiac goat', 'PTK').
card_image_name('zodiac goat'/'PTK', 'zodiac goat').
card_uid('zodiac goat'/'PTK', 'PTK:Zodiac Goat:zodiac goat').
card_rarity('zodiac goat'/'PTK', 'Common').
card_artist('zodiac goat'/'PTK', 'Qi Baocheng').
card_number('zodiac goat'/'PTK', '132').
card_flavor_text('zodiac goat'/'PTK', '\". . . Near death in Baidi, having reigned three years, / Bei sadly placed his son in Kongming\'s care. . . .\"').
card_multiverse_id('zodiac goat'/'PTK', '10540').

card_in_set('zodiac horse', 'PTK').
card_original_type('zodiac horse'/'PTK', 'Creature — Horse').
card_original_text('zodiac horse'/'PTK', 'Islandwalk (If defending player has an island in play, Zodiac Horse can\'t be blocked.)').
card_first_print('zodiac horse', 'PTK').
card_image_name('zodiac horse'/'PTK', 'zodiac horse').
card_uid('zodiac horse'/'PTK', 'PTK:Zodiac Horse:zodiac horse').
card_rarity('zodiac horse'/'PTK', 'Uncommon').
card_artist('zodiac horse'/'PTK', 'Ai Desheng').
card_number('zodiac horse'/'PTK', '159').
card_flavor_text('zodiac horse'/'PTK', '\". . . ‘First take Jingzhou, next the Riverlands; / On that rich region, base your own royal stand.\' . . .\"').
card_multiverse_id('zodiac horse'/'PTK', '10598').

card_in_set('zodiac monkey', 'PTK').
card_original_type('zodiac monkey'/'PTK', 'Creature — Monkey').
card_original_text('zodiac monkey'/'PTK', 'Forestwalk (If defending player has a forest in play, Zodaic Monkey can\'t be blocked.)').
card_first_print('zodiac monkey', 'PTK').
card_image_name('zodiac monkey'/'PTK', 'zodiac monkey').
card_uid('zodiac monkey'/'PTK', 'PTK:Zodiac Monkey:zodiac monkey').
card_rarity('zodiac monkey'/'PTK', 'Common').
card_artist('zodiac monkey'/'PTK', 'Ai Desheng').
card_number('zodiac monkey'/'PTK', '160').
card_flavor_text('zodiac monkey'/'PTK', '\". . . By six offensives from the hills of Qi\nKongming sought to change Han\'s destiny. . . .\"').
card_multiverse_id('zodiac monkey'/'PTK', '10558').

card_in_set('zodiac ox', 'PTK').
card_original_type('zodiac ox'/'PTK', 'Creature — Ox').
card_original_text('zodiac ox'/'PTK', 'Swampwalk (If defending player has a swamp in play, Zodiac Ox can\'t be blocked.)').
card_first_print('zodiac ox', 'PTK').
card_image_name('zodiac ox'/'PTK', 'zodiac ox').
card_uid('zodiac ox'/'PTK', 'PTK:Zodiac Ox:zodiac ox').
card_rarity('zodiac ox'/'PTK', 'Uncommon').
card_artist('zodiac ox'/'PTK', 'Ai Desheng').
card_number('zodiac ox'/'PTK', '161').
card_flavor_text('zodiac ox'/'PTK', '\". . . Cao\'s abdication changed the face of all; / No mighty battles marked the Southland\'s fall. . . .\"').
card_multiverse_id('zodiac ox'/'PTK', '10636').

card_in_set('zodiac pig', 'PTK').
card_original_type('zodiac pig'/'PTK', 'Creature — Pig').
card_original_text('zodiac pig'/'PTK', 'Swampwalk (If defending player has a swamp in play, Zodiac Pig can\'t be blocked.)').
card_first_print('zodiac pig', 'PTK').
card_image_name('zodiac pig'/'PTK', 'zodiac pig').
card_uid('zodiac pig'/'PTK', 'PTK:Zodiac Pig:zodiac pig').
card_rarity('zodiac pig'/'PTK', 'Uncommon').
card_artist('zodiac pig'/'PTK', 'Qi Baocheng').
card_number('zodiac pig'/'PTK', '97').
card_flavor_text('zodiac pig'/'PTK', '\". . . Zhong Hui and Deng Ai next led armies west: / And to the Cao, Han\'s hills and streams now passed. . . .\"').
card_multiverse_id('zodiac pig'/'PTK', '10586').

card_in_set('zodiac rabbit', 'PTK').
card_original_type('zodiac rabbit'/'PTK', 'Creature — Rabbit').
card_original_text('zodiac rabbit'/'PTK', 'Forestwalk (If defending player has a forest in play, Zodiac Hare can\'t be blocked.)').
card_first_print('zodiac rabbit', 'PTK').
card_image_name('zodiac rabbit'/'PTK', 'zodiac rabbit').
card_uid('zodiac rabbit'/'PTK', 'PTK:Zodiac Rabbit:zodiac rabbit').
card_rarity('zodiac rabbit'/'PTK', 'Common').
card_artist('zodiac rabbit'/'PTK', 'Ai Desheng').
card_number('zodiac rabbit'/'PTK', '162').
card_flavor_text('zodiac rabbit'/'PTK', '\". . . The world\'s affairs rush on, an endless stream; / A sky-told fate, infinite in reach, dooms all. . . .\"').
card_multiverse_id('zodiac rabbit'/'PTK', '10582').

card_in_set('zodiac rat', 'PTK').
card_original_type('zodiac rat'/'PTK', 'Creature — Rat').
card_original_text('zodiac rat'/'PTK', 'Swampwalk (If defending player has a swamp in play, Zodiac Rat can\'t be blocked.)').
card_first_print('zodiac rat', 'PTK').
card_image_name('zodiac rat'/'PTK', 'zodiac rat').
card_uid('zodiac rat'/'PTK', 'PTK:Zodiac Rat:zodiac rat').
card_rarity('zodiac rat'/'PTK', 'Common').
card_artist('zodiac rat'/'PTK', 'Qi Baocheng').
card_number('zodiac rat'/'PTK', '98').
card_flavor_text('zodiac rat'/'PTK', '\" . . . Cao Pi, Cao Rui, Fang, Mao, and briefly, Huan— / The Sima took the empire in their turn. . . .\"').
card_multiverse_id('zodiac rat'/'PTK', '10564').

card_in_set('zodiac rooster', 'PTK').
card_original_type('zodiac rooster'/'PTK', 'Creature — Rooster').
card_original_text('zodiac rooster'/'PTK', 'Plainswalk (If defending player has a plain in play, Zodiac Rooster can\'t be blocked.)').
card_first_print('zodiac rooster', 'PTK').
card_image_name('zodiac rooster'/'PTK', 'zodiac rooster').
card_uid('zodiac rooster'/'PTK', 'PTK:Zodiac Rooster:zodiac rooster').
card_rarity('zodiac rooster'/'PTK', 'Common').
card_artist('zodiac rooster'/'PTK', 'Ai Desheng').
card_number('zodiac rooster'/'PTK', '163').
card_flavor_text('zodiac rooster'/'PTK', '\". . . But the time of Han had run—could he Kongming not tell?— / That night his master star fell past the hills. . . .\"').
card_multiverse_id('zodiac rooster'/'PTK', '10506').

card_in_set('zodiac snake', 'PTK').
card_original_type('zodiac snake'/'PTK', 'Creature — Snake').
card_original_text('zodiac snake'/'PTK', 'Swampwalk (If defending player has a swamp in play, Zodiac Snake can\'t be blocked.)').
card_first_print('zodiac snake', 'PTK').
card_image_name('zodiac snake'/'PTK', 'zodiac snake').
card_uid('zodiac snake'/'PTK', 'PTK:Zodiac Snake:zodiac snake').
card_rarity('zodiac snake'/'PTK', 'Common').
card_artist('zodiac snake'/'PTK', 'Qi Baocheng').
card_number('zodiac snake'/'PTK', '99').
card_flavor_text('zodiac snake'/'PTK', '\"Thrice Xuande\'s ardent quest led to Nanyang, / Where Sleeping Dragon unveiled Han\'s partition: . . .\"').
card_multiverse_id('zodiac snake'/'PTK', '10608').

card_in_set('zodiac tiger', 'PTK').
card_original_type('zodiac tiger'/'PTK', 'Creature — Tiger').
card_original_text('zodiac tiger'/'PTK', 'Forestwalk (If defending player has a forest in play, Zodiac Tiger can\'t be blocked.)').
card_first_print('zodiac tiger', 'PTK').
card_image_name('zodiac tiger'/'PTK', 'zodiac tiger').
card_uid('zodiac tiger'/'PTK', 'PTK:Zodiac Tiger:zodiac tiger').
card_rarity('zodiac tiger'/'PTK', 'Uncommon').
card_artist('zodiac tiger'/'PTK', 'Ai Desheng').
card_number('zodiac tiger'/'PTK', '164').
card_flavor_text('zodiac tiger'/'PTK', '\". . . Three kings no more—Chenliu, Guiming, Anle. / The fiefs and posts must now be filled anew. . . .\"').
card_multiverse_id('zodiac tiger'/'PTK', '10594').

card_in_set('zuo ci, the mocking sage', 'PTK').
card_original_type('zuo ci, the mocking sage'/'PTK', 'Creature — Legend').
card_original_text('zuo ci, the mocking sage'/'PTK', 'Zuo Ci can\'t be blocked by creatures with horsemanship.\nWhenever your opponent chooses a creature in play, he or she can\'t choose Zuo Ci.').
card_first_print('zuo ci, the mocking sage', 'PTK').
card_image_name('zuo ci, the mocking sage'/'PTK', 'zuo ci, the mocking sage').
card_uid('zuo ci, the mocking sage'/'PTK', 'PTK:Zuo Ci, the Mocking Sage:zuo ci, the mocking sage').
card_rarity('zuo ci, the mocking sage'/'PTK', 'Rare').
card_artist('zuo ci, the mocking sage'/'PTK', 'Wang Yuqun').
card_number('zuo ci, the mocking sage'/'PTK', '165').
card_multiverse_id('zuo ci, the mocking sage'/'PTK', '10610').
