% Lorwyn

set('LRW').
set_name('LRW', 'Lorwyn').
set_release_date('LRW', '2007-10-12').
set_border('LRW', 'black').
set_type('LRW', 'expansion').
set_block('LRW', 'Lorwyn').

card_in_set('adder-staff boggart', 'LRW').
card_original_type('adder-staff boggart'/'LRW', 'Creature — Goblin Warrior').
card_original_text('adder-staff boggart'/'LRW', 'When Adder-Staff Boggart comes into play, clash with an opponent. If you win, put a +1/+1 counter on Adder-Staff Boggart. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_first_print('adder-staff boggart', 'LRW').
card_image_name('adder-staff boggart'/'LRW', 'adder-staff boggart').
card_uid('adder-staff boggart'/'LRW', 'LRW:Adder-Staff Boggart:adder-staff boggart').
card_rarity('adder-staff boggart'/'LRW', 'Common').
card_artist('adder-staff boggart'/'LRW', 'Jeff Miracola').
card_number('adder-staff boggart'/'LRW', '148').
card_multiverse_id('adder-staff boggart'/'LRW', '145992').

card_in_set('æthersnipe', 'LRW').
card_original_type('æthersnipe'/'LRW', 'Creature — Elemental').
card_original_text('æthersnipe'/'LRW', 'When Æthersnipe comes into play, return target nonland permanent to its owner\'s hand.\nEvoke {1}{U}{U} (You may play this spell for its evoke cost. If you do, it\'s sacrificed when it comes into play.)').
card_first_print('æthersnipe', 'LRW').
card_image_name('æthersnipe'/'LRW', 'aethersnipe').
card_uid('æthersnipe'/'LRW', 'LRW:Æthersnipe:aethersnipe').
card_rarity('æthersnipe'/'LRW', 'Common').
card_artist('æthersnipe'/'LRW', 'Zoltan Boros & Gabor Szikszai').
card_number('æthersnipe'/'LRW', '50').
card_multiverse_id('æthersnipe'/'LRW', '145817').

card_in_set('ajani goldmane', 'LRW').
card_original_type('ajani goldmane'/'LRW', 'Planeswalker — Ajani').
card_original_text('ajani goldmane'/'LRW', '+1: You gain 2 life.\n-1: Put a +1/+1 counter on each creature you control. Those creatures gain vigilance until end of turn.\n-6: Put a white Avatar creature token into play with \"This creature\'s power and toughness are each equal to your life total.\"').
card_image_name('ajani goldmane'/'LRW', 'ajani goldmane').
card_uid('ajani goldmane'/'LRW', 'LRW:Ajani Goldmane:ajani goldmane').
card_rarity('ajani goldmane'/'LRW', 'Rare').
card_artist('ajani goldmane'/'LRW', 'Aleksi Briclot').
card_number('ajani goldmane'/'LRW', '1').
card_multiverse_id('ajani goldmane'/'LRW', '140233').

card_in_set('amoeboid changeling', 'LRW').
card_original_type('amoeboid changeling'/'LRW', 'Creature — Shapeshifter').
card_original_text('amoeboid changeling'/'LRW', 'Changeling (This card is every creature type at all times.)\n{T}: Target creature gains all creature types until end of turn.\n{T}: Target creature loses all creature types until end of turn.').
card_first_print('amoeboid changeling', 'LRW').
card_image_name('amoeboid changeling'/'LRW', 'amoeboid changeling').
card_uid('amoeboid changeling'/'LRW', 'LRW:Amoeboid Changeling:amoeboid changeling').
card_rarity('amoeboid changeling'/'LRW', 'Common').
card_artist('amoeboid changeling'/'LRW', 'Nils Hamm').
card_number('amoeboid changeling'/'LRW', '51').
card_multiverse_id('amoeboid changeling'/'LRW', '140339').

card_in_set('ancient amphitheater', 'LRW').
card_original_type('ancient amphitheater'/'LRW', 'Land').
card_original_text('ancient amphitheater'/'LRW', 'As Ancient Amphitheater comes into play, you may reveal a Giant card from your hand. If you don\'t, Ancient Amphitheater comes into play tapped.\n{T}: Add {R} or {W} to your mana pool.').
card_first_print('ancient amphitheater', 'LRW').
card_image_name('ancient amphitheater'/'LRW', 'ancient amphitheater').
card_uid('ancient amphitheater'/'LRW', 'LRW:Ancient Amphitheater:ancient amphitheater').
card_rarity('ancient amphitheater'/'LRW', 'Rare').
card_artist('ancient amphitheater'/'LRW', 'Rob Alexander').
card_number('ancient amphitheater'/'LRW', '266').
card_flavor_text('ancient amphitheater'/'LRW', 'The arbiter Galanda Feudkiller judges Lorwyn\'s squabbles from a lofty perspective.').
card_multiverse_id('ancient amphitheater'/'LRW', '153454').

card_in_set('aquitect\'s will', 'LRW').
card_original_type('aquitect\'s will'/'LRW', 'Tribal Sorcery — Merfolk').
card_original_text('aquitect\'s will'/'LRW', 'Put a flood counter on target land. That land is an Island in addition to its other types as long as it has a flood counter on it. If you control a Merfolk, draw a card.').
card_first_print('aquitect\'s will', 'LRW').
card_image_name('aquitect\'s will'/'LRW', 'aquitect\'s will').
card_uid('aquitect\'s will'/'LRW', 'LRW:Aquitect\'s Will:aquitect\'s will').
card_rarity('aquitect\'s will'/'LRW', 'Common').
card_artist('aquitect\'s will'/'LRW', 'Jeff Easley').
card_number('aquitect\'s will'/'LRW', '52').
card_flavor_text('aquitect\'s will'/'LRW', 'There is nowhere on Lorwyn that the Merrow Lanes cannot go.').
card_multiverse_id('aquitect\'s will'/'LRW', '142354').

card_in_set('arbiter of knollridge', 'LRW').
card_original_type('arbiter of knollridge'/'LRW', 'Creature — Giant Wizard').
card_original_text('arbiter of knollridge'/'LRW', 'Vigilance\nWhen Arbiter of Knollridge comes into play, each player\'s life total becomes the highest life total among all players.').
card_first_print('arbiter of knollridge', 'LRW').
card_image_name('arbiter of knollridge'/'LRW', 'arbiter of knollridge').
card_uid('arbiter of knollridge'/'LRW', 'LRW:Arbiter of Knollridge:arbiter of knollridge').
card_rarity('arbiter of knollridge'/'LRW', 'Rare').
card_artist('arbiter of knollridge'/'LRW', 'Brandon Dorman').
card_number('arbiter of knollridge'/'LRW', '2').
card_flavor_text('arbiter of knollridge'/'LRW', 'Though giants are mortal, they live so long and on such a grand scale that many small folk don\'t believe they ever truly die.').
card_multiverse_id('arbiter of knollridge'/'LRW', '140189').

card_in_set('ashling the pilgrim', 'LRW').
card_original_type('ashling the pilgrim'/'LRW', 'Legendary Creature — Elemental Shaman').
card_original_text('ashling the pilgrim'/'LRW', '{1}{R}: Put a +1/+1 counter on Ashling the Pilgrim. If this is the third time this ability has resolved this turn, remove all +1/+1 counters from Ashling the Pilgrim, and it deals that much damage to each creature and each player.').
card_first_print('ashling the pilgrim', 'LRW').
card_image_name('ashling the pilgrim'/'LRW', 'ashling the pilgrim').
card_uid('ashling the pilgrim'/'LRW', 'LRW:Ashling the Pilgrim:ashling the pilgrim').
card_rarity('ashling the pilgrim'/'LRW', 'Rare').
card_artist('ashling the pilgrim'/'LRW', 'Wayne Reynolds').
card_number('ashling the pilgrim'/'LRW', '149').
card_multiverse_id('ashling the pilgrim'/'LRW', '141822').

card_in_set('ashling\'s prerogative', 'LRW').
card_original_type('ashling\'s prerogative'/'LRW', 'Enchantment').
card_original_text('ashling\'s prerogative'/'LRW', 'As Ashling\'s Prerogative comes into play, choose odd or even. (Zero is even.)\nEach creature with converted mana cost of the chosen value has haste.\nEach creature without converted mana cost of the chosen value comes into play tapped.').
card_first_print('ashling\'s prerogative', 'LRW').
card_image_name('ashling\'s prerogative'/'LRW', 'ashling\'s prerogative').
card_uid('ashling\'s prerogative'/'LRW', 'LRW:Ashling\'s Prerogative:ashling\'s prerogative').
card_rarity('ashling\'s prerogative'/'LRW', 'Rare').
card_artist('ashling\'s prerogative'/'LRW', 'Warren Mahy').
card_number('ashling\'s prerogative'/'LRW', '150').
card_multiverse_id('ashling\'s prerogative'/'LRW', '146595').

card_in_set('auntie\'s hovel', 'LRW').
card_original_type('auntie\'s hovel'/'LRW', 'Land').
card_original_text('auntie\'s hovel'/'LRW', 'As Auntie\'s Hovel comes into play, you may reveal a Goblin card from your hand. If you don\'t, Auntie\'s Hovel comes into play tapped.\n{T}: Add {B} or {R} to your mana pool.').
card_first_print('auntie\'s hovel', 'LRW').
card_image_name('auntie\'s hovel'/'LRW', 'auntie\'s hovel').
card_uid('auntie\'s hovel'/'LRW', 'LRW:Auntie\'s Hovel:auntie\'s hovel').
card_rarity('auntie\'s hovel'/'LRW', 'Rare').
card_artist('auntie\'s hovel'/'LRW', 'Wayne Reynolds').
card_number('auntie\'s hovel'/'LRW', '267').
card_flavor_text('auntie\'s hovel'/'LRW', 'The Stinkdrinker warren\'s hill of salvaged trinkets is large enough to cut a door in.').
card_multiverse_id('auntie\'s hovel'/'LRW', '153457').

card_in_set('austere command', 'LRW').
card_original_type('austere command'/'LRW', 'Sorcery').
card_original_text('austere command'/'LRW', 'Choose two Destroy all artifacts; or destroy all enchantments; or destroy all creatures with converted mana cost 3 or less; or destroy all creatures with converted mana cost 4 or greater.').
card_first_print('austere command', 'LRW').
card_image_name('austere command'/'LRW', 'austere command').
card_uid('austere command'/'LRW', 'LRW:Austere Command:austere command').
card_rarity('austere command'/'LRW', 'Rare').
card_artist('austere command'/'LRW', 'Wayne England').
card_number('austere command'/'LRW', '3').
card_multiverse_id('austere command'/'LRW', '141817').

card_in_set('avian changeling', 'LRW').
card_original_type('avian changeling'/'LRW', 'Creature — Shapeshifter').
card_original_text('avian changeling'/'LRW', 'Changeling (This card is every creature type at all times.)\nFlying').
card_first_print('avian changeling', 'LRW').
card_image_name('avian changeling'/'LRW', 'avian changeling').
card_uid('avian changeling'/'LRW', 'LRW:Avian Changeling:avian changeling').
card_rarity('avian changeling'/'LRW', 'Common').
card_artist('avian changeling'/'LRW', 'Heather Hudson').
card_number('avian changeling'/'LRW', '4').
card_flavor_text('avian changeling'/'LRW', 'Today it flies with the flock. Tomorrow it may wake to find them gone, its body in an unfamiliar form.').
card_multiverse_id('avian changeling'/'LRW', '145813').

card_in_set('axegrinder giant', 'LRW').
card_original_type('axegrinder giant'/'LRW', 'Creature — Giant Warrior').
card_original_text('axegrinder giant'/'LRW', '').
card_first_print('axegrinder giant', 'LRW').
card_image_name('axegrinder giant'/'LRW', 'axegrinder giant').
card_uid('axegrinder giant'/'LRW', 'LRW:Axegrinder Giant:axegrinder giant').
card_rarity('axegrinder giant'/'LRW', 'Common').
card_artist('axegrinder giant'/'LRW', 'Warren Mahy').
card_number('axegrinder giant'/'LRW', '151').
card_flavor_text('axegrinder giant'/'LRW', 'The angriest of giants are often the most skillful weaponsmiths. Their grudges fuel endless sessions at the forge, all the while growling ferociously to themselves.').
card_multiverse_id('axegrinder giant'/'LRW', '145976').

card_in_set('battle mastery', 'LRW').
card_original_type('battle mastery'/'LRW', 'Enchantment — Aura').
card_original_text('battle mastery'/'LRW', 'Enchant creature\nEnchanted creature has double strike.').
card_first_print('battle mastery', 'LRW').
card_image_name('battle mastery'/'LRW', 'battle mastery').
card_uid('battle mastery'/'LRW', 'LRW:Battle Mastery:battle mastery').
card_rarity('battle mastery'/'LRW', 'Uncommon').
card_artist('battle mastery'/'LRW', 'Zoltan Boros & Gabor Szikszai').
card_number('battle mastery'/'LRW', '5').
card_flavor_text('battle mastery'/'LRW', '\"Boom Boom Boots the size of oxcarts, then an axe like a falling sun. Elves scattered. Trees scattered. Even the hills ran for the hills\"\n—Clachan Tales').
card_multiverse_id('battle mastery'/'LRW', '145973').

card_in_set('battlewand oak', 'LRW').
card_original_type('battlewand oak'/'LRW', 'Creature — Treefolk Warrior').
card_original_text('battlewand oak'/'LRW', 'Whenever a Forest comes into play under your control, Battlewand Oak gets +2/+2 until end of turn.\nWhenever you play a Treefolk spell, Battlewand Oak gets +2/+2 until end of turn.').
card_first_print('battlewand oak', 'LRW').
card_image_name('battlewand oak'/'LRW', 'battlewand oak').
card_uid('battlewand oak'/'LRW', 'LRW:Battlewand Oak:battlewand oak').
card_rarity('battlewand oak'/'LRW', 'Common').
card_artist('battlewand oak'/'LRW', 'Steve Prescott').
card_number('battlewand oak'/'LRW', '197').
card_multiverse_id('battlewand oak'/'LRW', '141857').

card_in_set('benthicore', 'LRW').
card_original_type('benthicore'/'LRW', 'Creature — Elemental').
card_original_text('benthicore'/'LRW', 'When Benthicore comes into play, put two 1/1 blue Merfolk Wizard creature tokens into play.\nTap two untapped Merfolk you control: Untap Benthicore. It gains shroud until end of turn. (It can\'t be the target of spells or abilities.)').
card_first_print('benthicore', 'LRW').
card_image_name('benthicore'/'LRW', 'benthicore').
card_uid('benthicore'/'LRW', 'LRW:Benthicore:benthicore').
card_rarity('benthicore'/'LRW', 'Uncommon').
card_artist('benthicore'/'LRW', 'Jim Nelson').
card_number('benthicore'/'LRW', '53').
card_multiverse_id('benthicore'/'LRW', '139696').

card_in_set('black poplar shaman', 'LRW').
card_original_type('black poplar shaman'/'LRW', 'Creature — Treefolk Shaman').
card_original_text('black poplar shaman'/'LRW', '{2}{B}: Regenerate target Treefolk.').
card_first_print('black poplar shaman', 'LRW').
card_image_name('black poplar shaman'/'LRW', 'black poplar shaman').
card_uid('black poplar shaman'/'LRW', 'LRW:Black Poplar Shaman:black poplar shaman').
card_rarity('black poplar shaman'/'LRW', 'Common').
card_artist('black poplar shaman'/'LRW', 'Mark Poole').
card_number('black poplar shaman'/'LRW', '99').
card_flavor_text('black poplar shaman'/'LRW', 'It absorbs the pain of other treefolk, which leaves it bitter, yet addicted to the sensation of agony.').
card_multiverse_id('black poplar shaman'/'LRW', '139454').

card_in_set('blades of velis vel', 'LRW').
card_original_type('blades of velis vel'/'LRW', 'Tribal Instant — Shapeshifter').
card_original_text('blades of velis vel'/'LRW', 'Changeling (This card is every creature type at all times.)\nUp to two target creatures each get +2/+0 and gain all creature types until end of turn.').
card_first_print('blades of velis vel', 'LRW').
card_image_name('blades of velis vel'/'LRW', 'blades of velis vel').
card_uid('blades of velis vel'/'LRW', 'LRW:Blades of Velis Vel:blades of velis vel').
card_rarity('blades of velis vel'/'LRW', 'Common').
card_artist('blades of velis vel'/'LRW', 'Ron Spencer').
card_number('blades of velis vel'/'LRW', '152').
card_flavor_text('blades of velis vel'/'LRW', '\"The changing kind suffers as we do. We must join as one to quench our tyrants!\"').
card_multiverse_id('blades of velis vel'/'LRW', '139480').

card_in_set('blind-spot giant', 'LRW').
card_original_type('blind-spot giant'/'LRW', 'Creature — Giant Warrior').
card_original_text('blind-spot giant'/'LRW', 'Blind-Spot Giant can\'t attack or block unless you control another Giant.').
card_first_print('blind-spot giant', 'LRW').
card_image_name('blind-spot giant'/'LRW', 'blind-spot giant').
card_uid('blind-spot giant'/'LRW', 'LRW:Blind-Spot Giant:blind-spot giant').
card_rarity('blind-spot giant'/'LRW', 'Common').
card_artist('blind-spot giant'/'LRW', 'Jim Murray').
card_number('blind-spot giant'/'LRW', '153').
card_flavor_text('blind-spot giant'/'LRW', 'Among the solitude-loving giantkind, teamwork is unusual. But he appreciates hearing the occasional \"Swing down and to your left.\"').
card_multiverse_id('blind-spot giant'/'LRW', '146597').

card_in_set('bog hoodlums', 'LRW').
card_original_type('bog hoodlums'/'LRW', 'Creature — Goblin Warrior').
card_original_text('bog hoodlums'/'LRW', 'Bog Hoodlums can\'t block.\nWhen Bog Hoodlums comes into play, clash with an opponent. If you win, put a +1/+1 counter on Bog Hoodlums. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_first_print('bog hoodlums', 'LRW').
card_image_name('bog hoodlums'/'LRW', 'bog hoodlums').
card_uid('bog hoodlums'/'LRW', 'LRW:Bog Hoodlums:bog hoodlums').
card_rarity('bog hoodlums'/'LRW', 'Common').
card_artist('bog hoodlums'/'LRW', 'Brandon Dorman').
card_number('bog hoodlums'/'LRW', '100').
card_multiverse_id('bog hoodlums'/'LRW', '145966').

card_in_set('bog-strider ash', 'LRW').
card_original_type('bog-strider ash'/'LRW', 'Creature — Treefolk Shaman').
card_original_text('bog-strider ash'/'LRW', 'Swampwalk\nWhenever a player plays a Goblin spell, you may pay {G}. If you do, you gain 2 life.').
card_first_print('bog-strider ash', 'LRW').
card_image_name('bog-strider ash'/'LRW', 'bog-strider ash').
card_uid('bog-strider ash'/'LRW', 'LRW:Bog-Strider Ash:bog-strider ash').
card_rarity('bog-strider ash'/'LRW', 'Common').
card_artist('bog-strider ash'/'LRW', 'Steven Belledin').
card_number('bog-strider ash'/'LRW', '198').
card_flavor_text('bog-strider ash'/'LRW', '\"If you want to test wisdom, offer it to fools and watch how they tear it up.\"').
card_multiverse_id('bog-strider ash'/'LRW', '139410').

card_in_set('boggart birth rite', 'LRW').
card_original_type('boggart birth rite'/'LRW', 'Tribal Sorcery — Goblin').
card_original_text('boggart birth rite'/'LRW', 'Return target Goblin card from your graveyard to your hand.').
card_first_print('boggart birth rite', 'LRW').
card_image_name('boggart birth rite'/'LRW', 'boggart birth rite').
card_uid('boggart birth rite'/'LRW', 'LRW:Boggart Birth Rite:boggart birth rite').
card_rarity('boggart birth rite'/'LRW', 'Common').
card_artist('boggart birth rite'/'LRW', 'Ralph Horsley').
card_number('boggart birth rite'/'LRW', '101').
card_flavor_text('boggart birth rite'/'LRW', 'Auntie excitedly held up the squalling newborn. \"This one looks like Byoog! Maybe he\'ll tell us what he saw and felt in the beyond.\"').
card_multiverse_id('boggart birth rite'/'LRW', '142359').

card_in_set('boggart forager', 'LRW').
card_original_type('boggart forager'/'LRW', 'Creature — Goblin Rogue').
card_original_text('boggart forager'/'LRW', '{R}, Sacrifice Boggart Forager: Target player shuffles his or her library.').
card_first_print('boggart forager', 'LRW').
card_image_name('boggart forager'/'LRW', 'boggart forager').
card_uid('boggart forager'/'LRW', 'LRW:Boggart Forager:boggart forager').
card_rarity('boggart forager'/'LRW', 'Common').
card_artist('boggart forager'/'LRW', 'Ron Spencer').
card_number('boggart forager'/'LRW', '154').
card_flavor_text('boggart forager'/'LRW', '\"Reach in this hole, lose a hand. Reach in that hole, find a sparkly.\"\n—Auntie wisdom').
card_multiverse_id('boggart forager'/'LRW', '146168').

card_in_set('boggart harbinger', 'LRW').
card_original_type('boggart harbinger'/'LRW', 'Creature — Goblin Shaman').
card_original_text('boggart harbinger'/'LRW', 'When Boggart Harbinger comes into play, you may search your library for a Goblin card, reveal it, then shuffle your library and put that card on top of it.').
card_first_print('boggart harbinger', 'LRW').
card_image_name('boggart harbinger'/'LRW', 'boggart harbinger').
card_uid('boggart harbinger'/'LRW', 'LRW:Boggart Harbinger:boggart harbinger').
card_rarity('boggart harbinger'/'LRW', 'Uncommon').
card_artist('boggart harbinger'/'LRW', 'Steve Prescott').
card_number('boggart harbinger'/'LRW', '102').
card_multiverse_id('boggart harbinger'/'LRW', '139441').

card_in_set('boggart loggers', 'LRW').
card_original_type('boggart loggers'/'LRW', 'Creature — Goblin Rogue').
card_original_text('boggart loggers'/'LRW', 'Forestwalk\n{2}{B}, Sacrifice Boggart Loggers: Destroy target Treefolk or Forest.').
card_first_print('boggart loggers', 'LRW').
card_image_name('boggart loggers'/'LRW', 'boggart loggers').
card_uid('boggart loggers'/'LRW', 'LRW:Boggart Loggers:boggart loggers').
card_rarity('boggart loggers'/'LRW', 'Common').
card_artist('boggart loggers'/'LRW', 'Jesper Ejsing').
card_number('boggart loggers'/'LRW', '103').
card_flavor_text('boggart loggers'/'LRW', 'Auntie Flint lent axes to Nibb and Gyik, thinking they\'d share their experiences with her. She\'s still waiting for them to come back.').
card_multiverse_id('boggart loggers'/'LRW', '139709').

card_in_set('boggart mob', 'LRW').
card_original_type('boggart mob'/'LRW', 'Creature — Goblin Warrior').
card_original_text('boggart mob'/'LRW', 'Champion a Goblin (When this comes into play, sacrifice it unless you remove another Goblin you control from the game. When this leaves play, that card returns to play.)\nWhenever a Goblin you control deals combat damage to a player, you may put a 1/1 black Goblin Rogue creature token into play.').
card_first_print('boggart mob', 'LRW').
card_image_name('boggart mob'/'LRW', 'boggart mob').
card_uid('boggart mob'/'LRW', 'LRW:Boggart Mob:boggart mob').
card_rarity('boggart mob'/'LRW', 'Rare').
card_artist('boggart mob'/'LRW', 'Thomas Denmark').
card_number('boggart mob'/'LRW', '104').
card_multiverse_id('boggart mob'/'LRW', '140230').

card_in_set('boggart shenanigans', 'LRW').
card_original_type('boggart shenanigans'/'LRW', 'Tribal Enchantment — Goblin').
card_original_text('boggart shenanigans'/'LRW', 'Whenever another Goblin you control is put into a graveyard from play, you may have Boggart Shenanigans deal 1 damage to target player.').
card_first_print('boggart shenanigans', 'LRW').
card_image_name('boggart shenanigans'/'LRW', 'boggart shenanigans').
card_uid('boggart shenanigans'/'LRW', 'LRW:Boggart Shenanigans:boggart shenanigans').
card_rarity('boggart shenanigans'/'LRW', 'Uncommon').
card_artist('boggart shenanigans'/'LRW', 'Warren Mahy').
card_number('boggart shenanigans'/'LRW', '155').
card_flavor_text('boggart shenanigans'/'LRW', 'Boggarts revel in discovering new sensations, from the texture of an otter pellet to the squeak of a dying warren mate.').
card_multiverse_id('boggart shenanigans'/'LRW', '139445').

card_in_set('boggart sprite-chaser', 'LRW').
card_original_type('boggart sprite-chaser'/'LRW', 'Creature — Goblin Warrior').
card_original_text('boggart sprite-chaser'/'LRW', 'As long as you control a Faerie, Boggart Sprite-Chaser gets +1/+1 and has flying.').
card_first_print('boggart sprite-chaser', 'LRW').
card_image_name('boggart sprite-chaser'/'LRW', 'boggart sprite-chaser').
card_uid('boggart sprite-chaser'/'LRW', 'LRW:Boggart Sprite-Chaser:boggart sprite-chaser').
card_rarity('boggart sprite-chaser'/'LRW', 'Common').
card_artist('boggart sprite-chaser'/'LRW', 'Mark Poole').
card_number('boggart sprite-chaser'/'LRW', '156').
card_flavor_text('boggart sprite-chaser'/'LRW', '\"Auntie pointed out to the faerie how much mischief a flying boggart could wreak, and a beautiful new friendship was born.\"\n—A tale of Auntie Grub').
card_multiverse_id('boggart sprite-chaser'/'LRW', '146446').

card_in_set('briarhorn', 'LRW').
card_original_type('briarhorn'/'LRW', 'Creature — Elemental').
card_original_text('briarhorn'/'LRW', 'Flash\nWhen Briarhorn comes into play, target creature gets +3/+3 until end of turn.\nEvoke {1}{G} (You may play this spell for its evoke cost. If you do, it\'s sacrificed when it comes into play.)').
card_first_print('briarhorn', 'LRW').
card_image_name('briarhorn'/'LRW', 'briarhorn').
card_uid('briarhorn'/'LRW', 'LRW:Briarhorn:briarhorn').
card_rarity('briarhorn'/'LRW', 'Uncommon').
card_artist('briarhorn'/'LRW', 'Nils Hamm').
card_number('briarhorn'/'LRW', '199').
card_multiverse_id('briarhorn'/'LRW', '142365').

card_in_set('brigid, hero of kinsbaile', 'LRW').
card_original_type('brigid, hero of kinsbaile'/'LRW', 'Legendary Creature — Kithkin Archer').
card_original_text('brigid, hero of kinsbaile'/'LRW', 'First strike\n{T}: Brigid, Hero of Kinsbaile deals 2 damage to each attacking or blocking creature target player controls.').
card_first_print('brigid, hero of kinsbaile', 'LRW').
card_image_name('brigid, hero of kinsbaile'/'LRW', 'brigid, hero of kinsbaile').
card_uid('brigid, hero of kinsbaile'/'LRW', 'LRW:Brigid, Hero of Kinsbaile:brigid, hero of kinsbaile').
card_rarity('brigid, hero of kinsbaile'/'LRW', 'Rare').
card_artist('brigid, hero of kinsbaile'/'LRW', 'Steve Prescott').
card_number('brigid, hero of kinsbaile'/'LRW', '6').
card_flavor_text('brigid, hero of kinsbaile'/'LRW', 'Thanks to one champion archer, the true borders of Kinsbaile extend an arrow\'s flight beyond the buildings.').
card_multiverse_id('brigid, hero of kinsbaile'/'LRW', '141829').

card_in_set('brion stoutarm', 'LRW').
card_original_type('brion stoutarm'/'LRW', 'Legendary Creature — Giant Warrior').
card_original_text('brion stoutarm'/'LRW', 'Lifelink (Whenever this creature deals damage, you gain that much life.)\n{R}, {T}, Sacrifice a creature other than Brion Stoutarm: Brion Stoutarm deals damage equal to the sacrificed creature\'s power to target player.').
card_image_name('brion stoutarm'/'LRW', 'brion stoutarm').
card_uid('brion stoutarm'/'LRW', 'LRW:Brion Stoutarm:brion stoutarm').
card_rarity('brion stoutarm'/'LRW', 'Rare').
card_artist('brion stoutarm'/'LRW', 'Zoltan Boros & Gabor Szikszai').
card_number('brion stoutarm'/'LRW', '246').
card_multiverse_id('brion stoutarm'/'LRW', '140217').

card_in_set('broken ambitions', 'LRW').
card_original_type('broken ambitions'/'LRW', 'Instant').
card_original_text('broken ambitions'/'LRW', 'Counter target spell unless its controller pays {X}. Clash with an opponent. If you win, that spell\'s controller puts the top four cards of his or her library into his or her graveyard. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_first_print('broken ambitions', 'LRW').
card_image_name('broken ambitions'/'LRW', 'broken ambitions').
card_uid('broken ambitions'/'LRW', 'LRW:Broken Ambitions:broken ambitions').
card_rarity('broken ambitions'/'LRW', 'Common').
card_artist('broken ambitions'/'LRW', 'Franz Vohwinkel').
card_number('broken ambitions'/'LRW', '54').
card_multiverse_id('broken ambitions'/'LRW', '145989').

card_in_set('burrenton forge-tender', 'LRW').
card_original_type('burrenton forge-tender'/'LRW', 'Creature — Kithkin Wizard').
card_original_text('burrenton forge-tender'/'LRW', 'Protection from red\nSacrifice Burrenton Forge-Tender: Prevent all damage a red source of your choice would deal this turn.').
card_first_print('burrenton forge-tender', 'LRW').
card_image_name('burrenton forge-tender'/'LRW', 'burrenton forge-tender').
card_uid('burrenton forge-tender'/'LRW', 'LRW:Burrenton Forge-Tender:burrenton forge-tender').
card_rarity('burrenton forge-tender'/'LRW', 'Uncommon').
card_artist('burrenton forge-tender'/'LRW', 'Chuck Lukacs').
card_number('burrenton forge-tender'/'LRW', '7').
card_flavor_text('burrenton forge-tender'/'LRW', '\"We are a clachan of smiths. The forge is as comfortable to us as a small fire during a cool winter\'s evening.\"').
card_multiverse_id('burrenton forge-tender'/'LRW', '139395').

card_in_set('cairn wanderer', 'LRW').
card_original_type('cairn wanderer'/'LRW', 'Creature — Shapeshifter').
card_original_text('cairn wanderer'/'LRW', 'Changeling (This card is every creature type at all times.)\nAs long as a creature card with flying is in a graveyard, Cairn Wanderer has flying. The same is true for fear, first strike, double strike, deathtouch, haste, landwalk, lifelink, protection, reach, trample, shroud, and vigilance.').
card_first_print('cairn wanderer', 'LRW').
card_image_name('cairn wanderer'/'LRW', 'cairn wanderer').
card_uid('cairn wanderer'/'LRW', 'LRW:Cairn Wanderer:cairn wanderer').
card_rarity('cairn wanderer'/'LRW', 'Rare').
card_artist('cairn wanderer'/'LRW', 'Nils Hamm').
card_number('cairn wanderer'/'LRW', '105').
card_multiverse_id('cairn wanderer'/'LRW', '140171').

card_in_set('captivating glance', 'LRW').
card_original_type('captivating glance'/'LRW', 'Enchantment — Aura').
card_original_text('captivating glance'/'LRW', 'Enchant creature\nAt the end of your turn, clash with an opponent. If you win, gain control of enchanted creature. Otherwise, that player gains control of enchanted creature. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_first_print('captivating glance', 'LRW').
card_image_name('captivating glance'/'LRW', 'captivating glance').
card_uid('captivating glance'/'LRW', 'LRW:Captivating Glance:captivating glance').
card_rarity('captivating glance'/'LRW', 'Uncommon').
card_artist('captivating glance'/'LRW', 'Dan Dos Santos').
card_number('captivating glance'/'LRW', '55').
card_multiverse_id('captivating glance'/'LRW', '146172').

card_in_set('caterwauling boggart', 'LRW').
card_original_type('caterwauling boggart'/'LRW', 'Creature — Goblin Shaman').
card_original_text('caterwauling boggart'/'LRW', 'Each Goblin you control can\'t be blocked except by two or more creatures.\nEach Elemental you control can\'t be blocked except by two or more creatures.').
card_first_print('caterwauling boggart', 'LRW').
card_image_name('caterwauling boggart'/'LRW', 'caterwauling boggart').
card_uid('caterwauling boggart'/'LRW', 'LRW:Caterwauling Boggart:caterwauling boggart').
card_rarity('caterwauling boggart'/'LRW', 'Common').
card_artist('caterwauling boggart'/'LRW', 'Steven Belledin').
card_number('caterwauling boggart'/'LRW', '157').
card_flavor_text('caterwauling boggart'/'LRW', '\"As far as I can tell, that frog dangling from the stick serves absolutely no purpose whatsoever.\"\n—Gaddock Teeg').
card_multiverse_id('caterwauling boggart'/'LRW', '139471').

card_in_set('ceaseless searblades', 'LRW').
card_original_type('ceaseless searblades'/'LRW', 'Creature — Elemental Warrior').
card_original_text('ceaseless searblades'/'LRW', 'Whenever you play an activated ability of an Elemental, Ceaseless Searblades gets +1/+0 until end of turn.').
card_first_print('ceaseless searblades', 'LRW').
card_image_name('ceaseless searblades'/'LRW', 'ceaseless searblades').
card_uid('ceaseless searblades'/'LRW', 'LRW:Ceaseless Searblades:ceaseless searblades').
card_rarity('ceaseless searblades'/'LRW', 'Uncommon').
card_artist('ceaseless searblades'/'LRW', 'Jim Murray').
card_number('ceaseless searblades'/'LRW', '158').
card_flavor_text('ceaseless searblades'/'LRW', 'Flamekins\' fires burn cool until they decide otherwise.').
card_multiverse_id('ceaseless searblades'/'LRW', '139466').

card_in_set('cenn\'s heir', 'LRW').
card_original_type('cenn\'s heir'/'LRW', 'Creature — Kithkin Soldier').
card_original_text('cenn\'s heir'/'LRW', 'Whenever Cenn\'s Heir attacks, it gets +1/+1 until end of turn for each other attacking Kithkin.').
card_first_print('cenn\'s heir', 'LRW').
card_image_name('cenn\'s heir'/'LRW', 'cenn\'s heir').
card_uid('cenn\'s heir'/'LRW', 'LRW:Cenn\'s Heir:cenn\'s heir').
card_rarity('cenn\'s heir'/'LRW', 'Common').
card_artist('cenn\'s heir'/'LRW', 'Steven Belledin').
card_number('cenn\'s heir'/'LRW', '8').
card_flavor_text('cenn\'s heir'/'LRW', 'His home clachan\'s familial spirit bolsters his own, but he will be ready to preside over the town as cenn only after he learns to project that strength to others.').
card_multiverse_id('cenn\'s heir'/'LRW', '142360').

card_in_set('chandra nalaar', 'LRW').
card_original_type('chandra nalaar'/'LRW', 'Planeswalker — Chandra').
card_original_text('chandra nalaar'/'LRW', '+1: Chandra Nalaar deals 1 damage to target player.\n-X: Chandra Nalaar deals X damage to target creature.\n-8: Chandra Nalaar deals 10 damage to target player and each creature he or she controls.').
card_first_print('chandra nalaar', 'LRW').
card_image_name('chandra nalaar'/'LRW', 'chandra nalaar').
card_uid('chandra nalaar'/'LRW', 'LRW:Chandra Nalaar:chandra nalaar').
card_rarity('chandra nalaar'/'LRW', 'Rare').
card_artist('chandra nalaar'/'LRW', 'Aleksi Briclot').
card_number('chandra nalaar'/'LRW', '159').
card_multiverse_id('chandra nalaar'/'LRW', '140176').

card_in_set('changeling berserker', 'LRW').
card_original_type('changeling berserker'/'LRW', 'Creature — Shapeshifter').
card_original_text('changeling berserker'/'LRW', 'Changeling (This card is every creature type at all times.)\nHaste\nChampion a creature (When this comes into play, sacrifice it unless you remove another creature you control from the game. When this leaves play, that card returns to play.)').
card_first_print('changeling berserker', 'LRW').
card_image_name('changeling berserker'/'LRW', 'changeling berserker').
card_uid('changeling berserker'/'LRW', 'LRW:Changeling Berserker:changeling berserker').
card_rarity('changeling berserker'/'LRW', 'Uncommon').
card_artist('changeling berserker'/'LRW', 'Warren Mahy').
card_number('changeling berserker'/'LRW', '160').
card_multiverse_id('changeling berserker'/'LRW', '145804').

card_in_set('changeling hero', 'LRW').
card_original_type('changeling hero'/'LRW', 'Creature — Shapeshifter').
card_original_text('changeling hero'/'LRW', 'Changeling (This card is every creature type at all times.)\nChampion a creature (When this comes into play, sacrifice it unless you remove another creature you control from the game. When this leaves play, that card returns to play.)\nLifelink (Whenever this creature deals damage, you gain that much life.)').
card_first_print('changeling hero', 'LRW').
card_image_name('changeling hero'/'LRW', 'changeling hero').
card_uid('changeling hero'/'LRW', 'LRW:Changeling Hero:changeling hero').
card_rarity('changeling hero'/'LRW', 'Uncommon').
card_artist('changeling hero'/'LRW', 'Jeff Miracola').
card_number('changeling hero'/'LRW', '9').
card_multiverse_id('changeling hero'/'LRW', '145968').

card_in_set('changeling titan', 'LRW').
card_original_type('changeling titan'/'LRW', 'Creature — Shapeshifter').
card_original_text('changeling titan'/'LRW', 'Changeling (This card is every creature type at all times.)\nChampion a creature (When this comes into play, sacrifice it unless you remove another creature you control from the game. When this leaves play, that card returns to play.)').
card_first_print('changeling titan', 'LRW').
card_image_name('changeling titan'/'LRW', 'changeling titan').
card_uid('changeling titan'/'LRW', 'LRW:Changeling Titan:changeling titan').
card_rarity('changeling titan'/'LRW', 'Uncommon').
card_artist('changeling titan'/'LRW', 'Jesper Ejsing').
card_number('changeling titan'/'LRW', '200').
card_multiverse_id('changeling titan'/'LRW', '140345').

card_in_set('cloudcrown oak', 'LRW').
card_original_type('cloudcrown oak'/'LRW', 'Creature — Treefolk Warrior').
card_original_text('cloudcrown oak'/'LRW', 'Reach (This can block creatures with flying.)').
card_first_print('cloudcrown oak', 'LRW').
card_image_name('cloudcrown oak'/'LRW', 'cloudcrown oak').
card_uid('cloudcrown oak'/'LRW', 'LRW:Cloudcrown Oak:cloudcrown oak').
card_rarity('cloudcrown oak'/'LRW', 'Common').
card_artist('cloudcrown oak'/'LRW', 'Rebecca Guay').
card_number('cloudcrown oak'/'LRW', '201').
card_flavor_text('cloudcrown oak'/'LRW', '\"Clever folk build their homes near cloudcrowns. If a hawk or even just a faerie tries to swoop in, it\'ll get swatted from here to Cloverdell.\"\n—Calydd, kithkin farmer').
card_multiverse_id('cloudcrown oak'/'LRW', '139492').

card_in_set('cloudgoat ranger', 'LRW').
card_original_type('cloudgoat ranger'/'LRW', 'Creature — Giant Warrior').
card_original_text('cloudgoat ranger'/'LRW', 'When Cloudgoat Ranger comes into play, put three 1/1 white Kithkin Soldier creature tokens into play.\nTap three untapped Kithkin you control: Cloudgoat Ranger gets +2/+0 and gains flying until end of turn.').
card_first_print('cloudgoat ranger', 'LRW').
card_image_name('cloudgoat ranger'/'LRW', 'cloudgoat ranger').
card_uid('cloudgoat ranger'/'LRW', 'LRW:Cloudgoat Ranger:cloudgoat ranger').
card_rarity('cloudgoat ranger'/'LRW', 'Uncommon').
card_artist('cloudgoat ranger'/'LRW', 'Adam Rex').
card_number('cloudgoat ranger'/'LRW', '10').
card_multiverse_id('cloudgoat ranger'/'LRW', '139664').

card_in_set('cloudthresher', 'LRW').
card_original_type('cloudthresher'/'LRW', 'Creature — Elemental').
card_original_text('cloudthresher'/'LRW', 'Flash\nReach (This can block creatures with flying.)\nWhen Cloudthresher comes into play, it deals 2 damage to each creature with flying and each player.\nEvoke {2}{G}{G} (You may play this spell for its evoke cost. If you do, it\'s sacrificed when it comes into play.)').
card_first_print('cloudthresher', 'LRW').
card_image_name('cloudthresher'/'LRW', 'cloudthresher').
card_uid('cloudthresher'/'LRW', 'LRW:Cloudthresher:cloudthresher').
card_rarity('cloudthresher'/'LRW', 'Rare').
card_artist('cloudthresher'/'LRW', 'Christopher Moeller').
card_number('cloudthresher'/'LRW', '202').
card_multiverse_id('cloudthresher'/'LRW', '146173').

card_in_set('colfenor\'s plans', 'LRW').
card_original_type('colfenor\'s plans'/'LRW', 'Enchantment').
card_original_text('colfenor\'s plans'/'LRW', 'When Colfenor\'s Plans comes into play, remove the top seven cards of your library from the game face down.\nYou may look at and play cards removed from the game with Colfenor\'s Plans.\nSkip your draw step.\nYou can\'t play more than one spell each turn.').
card_first_print('colfenor\'s plans', 'LRW').
card_image_name('colfenor\'s plans'/'LRW', 'colfenor\'s plans').
card_uid('colfenor\'s plans'/'LRW', 'LRW:Colfenor\'s Plans:colfenor\'s plans').
card_rarity('colfenor\'s plans'/'LRW', 'Rare').
card_artist('colfenor\'s plans'/'LRW', 'Darrell Riche').
card_number('colfenor\'s plans'/'LRW', '106').
card_multiverse_id('colfenor\'s plans'/'LRW', '146581').

card_in_set('colfenor\'s urn', 'LRW').
card_original_type('colfenor\'s urn'/'LRW', 'Artifact').
card_original_text('colfenor\'s urn'/'LRW', 'Whenever a creature with toughness 4 or greater is put into your graveyard from play, you may remove it from the game.\nAt end of turn, if three or more cards have been removed from the game with Colfenor\'s Urn, sacrifice it. If you do, return those cards to play under their owner\'s control.').
card_first_print('colfenor\'s urn', 'LRW').
card_image_name('colfenor\'s urn'/'LRW', 'colfenor\'s urn').
card_uid('colfenor\'s urn'/'LRW', 'LRW:Colfenor\'s Urn:colfenor\'s urn').
card_rarity('colfenor\'s urn'/'LRW', 'Rare').
card_artist('colfenor\'s urn'/'LRW', 'Jim Pavelec').
card_number('colfenor\'s urn'/'LRW', '254').
card_multiverse_id('colfenor\'s urn'/'LRW', '140209').

card_in_set('consuming bonfire', 'LRW').
card_original_type('consuming bonfire'/'LRW', 'Tribal Sorcery — Elemental').
card_original_text('consuming bonfire'/'LRW', 'Choose one Consuming Bonfire deals 4 damage to target non-Elemental creature; or Consuming Bonfire deals 7 damage to target Treefolk creature.').
card_first_print('consuming bonfire', 'LRW').
card_image_name('consuming bonfire'/'LRW', 'consuming bonfire').
card_uid('consuming bonfire'/'LRW', 'LRW:Consuming Bonfire:consuming bonfire').
card_rarity('consuming bonfire'/'LRW', 'Common').
card_artist('consuming bonfire'/'LRW', 'Randy Gallegos').
card_number('consuming bonfire'/'LRW', '161').
card_flavor_text('consuming bonfire'/'LRW', '\"The elves use treefolk to drive us away. It is time to remove their tools.\"\n—Vessifrus, flamekin demagogue').
card_multiverse_id('consuming bonfire'/'LRW', '139688').

card_in_set('crib swap', 'LRW').
card_original_type('crib swap'/'LRW', 'Tribal Instant — Shapeshifter').
card_original_text('crib swap'/'LRW', 'Changeling (This card is every creature type at all times.)\nRemove target creature from the game. Its controller puts a 1/1 colorless Shapeshifter creature token with changeling into play.').
card_first_print('crib swap', 'LRW').
card_image_name('crib swap'/'LRW', 'crib swap').
card_uid('crib swap'/'LRW', 'LRW:Crib Swap:crib swap').
card_rarity('crib swap'/'LRW', 'Uncommon').
card_artist('crib swap'/'LRW', 'Brandon Dorman').
card_number('crib swap'/'LRW', '11').
card_multiverse_id('crib swap'/'LRW', '143380').

card_in_set('crush underfoot', 'LRW').
card_original_type('crush underfoot'/'LRW', 'Tribal Instant — Giant').
card_original_text('crush underfoot'/'LRW', 'Choose a Giant creature you control. It deals damage equal to its power to target creature.').
card_first_print('crush underfoot', 'LRW').
card_image_name('crush underfoot'/'LRW', 'crush underfoot').
card_uid('crush underfoot'/'LRW', 'LRW:Crush Underfoot:crush underfoot').
card_rarity('crush underfoot'/'LRW', 'Uncommon').
card_artist('crush underfoot'/'LRW', 'Steven Belledin').
card_number('crush underfoot'/'LRW', '162').
card_flavor_text('crush underfoot'/'LRW', 'Five-toed grave\n—Kithkin phrase meaning\n\"a giant\'s footprint\"').
card_multiverse_id('crush underfoot'/'LRW', '143384').

card_in_set('cryptic command', 'LRW').
card_original_type('cryptic command'/'LRW', 'Instant').
card_original_text('cryptic command'/'LRW', 'Choose two Counter target spell; or return target permanent to its owner\'s hand; or tap all creatures your opponents control; or draw a card.').
card_image_name('cryptic command'/'LRW', 'cryptic command').
card_uid('cryptic command'/'LRW', 'LRW:Cryptic Command:cryptic command').
card_rarity('cryptic command'/'LRW', 'Rare').
card_artist('cryptic command'/'LRW', 'Wayne England').
card_number('cryptic command'/'LRW', '56').
card_multiverse_id('cryptic command'/'LRW', '141819').

card_in_set('dauntless dourbark', 'LRW').
card_original_type('dauntless dourbark'/'LRW', 'Creature — Treefolk Warrior').
card_original_text('dauntless dourbark'/'LRW', 'Dauntless Dourbark\'s power and toughness are each equal to the number of Forests you control plus the number of Treefolk you control.\nDauntless Dourbark has trample as long as you control another Treefolk.').
card_image_name('dauntless dourbark'/'LRW', 'dauntless dourbark').
card_uid('dauntless dourbark'/'LRW', 'LRW:Dauntless Dourbark:dauntless dourbark').
card_rarity('dauntless dourbark'/'LRW', 'Rare').
card_artist('dauntless dourbark'/'LRW', 'Jeremy Jarvis').
card_number('dauntless dourbark'/'LRW', '203').
card_multiverse_id('dauntless dourbark'/'LRW', '141851').

card_in_set('dawnfluke', 'LRW').
card_original_type('dawnfluke'/'LRW', 'Creature — Elemental').
card_original_text('dawnfluke'/'LRW', 'Flash\nWhen Dawnfluke comes into play, prevent the next 3 damage that would be dealt to target creature or player this turn.\nEvoke {W} (You may play this spell for its evoke cost. If you do, it\'s sacrificed when it comes into play.)').
card_first_print('dawnfluke', 'LRW').
card_image_name('dawnfluke'/'LRW', 'dawnfluke').
card_uid('dawnfluke'/'LRW', 'LRW:Dawnfluke:dawnfluke').
card_rarity('dawnfluke'/'LRW', 'Common').
card_artist('dawnfluke'/'LRW', 'Mark Zug').
card_number('dawnfluke'/'LRW', '12').
card_multiverse_id('dawnfluke'/'LRW', '145801').

card_in_set('deathrender', 'LRW').
card_original_type('deathrender'/'LRW', 'Artifact — Equipment').
card_original_text('deathrender'/'LRW', 'Equipped creature gets +2/+2.\nWhenever equipped creature is put into a graveyard from play, you may put a creature card from your hand into play and attach Deathrender to it.\nEquip {2}').
card_first_print('deathrender', 'LRW').
card_image_name('deathrender'/'LRW', 'deathrender').
card_uid('deathrender'/'LRW', 'LRW:Deathrender:deathrender').
card_rarity('deathrender'/'LRW', 'Rare').
card_artist('deathrender'/'LRW', 'Martina Pilcerova').
card_number('deathrender'/'LRW', '255').
card_multiverse_id('deathrender'/'LRW', '139727').

card_in_set('deeptread merrow', 'LRW').
card_original_type('deeptread merrow'/'LRW', 'Creature — Merfolk Rogue').
card_original_text('deeptread merrow'/'LRW', '{U}: Deeptread Merrow gains islandwalk until end of turn.').
card_first_print('deeptread merrow', 'LRW').
card_image_name('deeptread merrow'/'LRW', 'deeptread merrow').
card_uid('deeptread merrow'/'LRW', 'LRW:Deeptread Merrow:deeptread merrow').
card_rarity('deeptread merrow'/'LRW', 'Common').
card_artist('deeptread merrow'/'LRW', 'Terese Nielsen & Philip Tan').
card_number('deeptread merrow'/'LRW', '57').
card_flavor_text('deeptread merrow'/'LRW', '\"My success at navigating the Dark Meanders irritates the Inkfathom school. They consider themselves peerless divers, but I try to remind them that they cannot own commodities like bravery and cunning.\"').
card_multiverse_id('deeptread merrow'/'LRW', '142352').

card_in_set('dolmen gate', 'LRW').
card_original_type('dolmen gate'/'LRW', 'Artifact').
card_original_text('dolmen gate'/'LRW', 'Prevent all combat damage that would be dealt to attacking creatures you control.').
card_first_print('dolmen gate', 'LRW').
card_image_name('dolmen gate'/'LRW', 'dolmen gate').
card_uid('dolmen gate'/'LRW', 'LRW:Dolmen Gate:dolmen gate').
card_rarity('dolmen gate'/'LRW', 'Rare').
card_artist('dolmen gate'/'LRW', 'Richard Sardinha').
card_number('dolmen gate'/'LRW', '256').
card_flavor_text('dolmen gate'/'LRW', 'Lorwyn\'s stones resonate with the place from which they were hewed. Though taken far, still they call to their home when silence is upon the land.').
card_multiverse_id('dolmen gate'/'LRW', '140206').

card_in_set('doran, the siege tower', 'LRW').
card_original_type('doran, the siege tower'/'LRW', 'Legendary Creature — Treefolk Shaman').
card_original_text('doran, the siege tower'/'LRW', 'Each creature assigns combat damage equal to its toughness rather than its power.').
card_image_name('doran, the siege tower'/'LRW', 'doran, the siege tower').
card_uid('doran, the siege tower'/'LRW', 'LRW:Doran, the Siege Tower:doran, the siege tower').
card_rarity('doran, the siege tower'/'LRW', 'Rare').
card_artist('doran, the siege tower'/'LRW', 'Mark Zug').
card_number('doran, the siege tower'/'LRW', '247').
card_flavor_text('doran, the siege tower'/'LRW', '\"Each year that passes rings you inwardly with memory and might. Wield your heart, and the world will tremble.\"').
card_multiverse_id('doran, the siege tower'/'LRW', '140201').

card_in_set('dread', 'LRW').
card_original_type('dread'/'LRW', 'Creature — Elemental Incarnation').
card_original_text('dread'/'LRW', 'Fear\nWhenever a creature deals damage to you, destroy it.\nWhen Dread is put into a graveyard from anywhere, shuffle it into its owner\'s library.').
card_first_print('dread', 'LRW').
card_image_name('dread'/'LRW', 'dread').
card_uid('dread'/'LRW', 'LRW:Dread:dread').
card_rarity('dread'/'LRW', 'Rare').
card_artist('dread'/'LRW', 'Matt Cavotta').
card_number('dread'/'LRW', '107').
card_multiverse_id('dread'/'LRW', '140168').

card_in_set('dreamspoiler witches', 'LRW').
card_original_type('dreamspoiler witches'/'LRW', 'Creature — Faerie Wizard').
card_original_text('dreamspoiler witches'/'LRW', 'Flying\nWhenever you play a spell during an opponent\'s turn, you may have target creature get -1/-1 until end of turn.').
card_first_print('dreamspoiler witches', 'LRW').
card_image_name('dreamspoiler witches'/'LRW', 'dreamspoiler witches').
card_uid('dreamspoiler witches'/'LRW', 'LRW:Dreamspoiler Witches:dreamspoiler witches').
card_rarity('dreamspoiler witches'/'LRW', 'Common').
card_artist('dreamspoiler witches'/'LRW', 'Jeff Easley').
card_number('dreamspoiler witches'/'LRW', '108').
card_flavor_text('dreamspoiler witches'/'LRW', 'At night, the faeries steal dreamstuff for their queen. At daybreak, countless creatures wake weak and hollow.').
card_multiverse_id('dreamspoiler witches'/'LRW', '139453').

card_in_set('drowner of secrets', 'LRW').
card_original_type('drowner of secrets'/'LRW', 'Creature — Merfolk Wizard').
card_original_text('drowner of secrets'/'LRW', 'Tap an untapped Merfolk you control: Target player puts the top card of his or her library into his or her graveyard.').
card_first_print('drowner of secrets', 'LRW').
card_image_name('drowner of secrets'/'LRW', 'drowner of secrets').
card_uid('drowner of secrets'/'LRW', 'LRW:Drowner of Secrets:drowner of secrets').
card_rarity('drowner of secrets'/'LRW', 'Uncommon').
card_artist('drowner of secrets'/'LRW', 'Rebecca Guay').
card_number('drowner of secrets'/'LRW', '58').
card_flavor_text('drowner of secrets'/'LRW', 'Merrows consider themselves the keepers of Lorwyn\'s past—and consider it their duty to edit when necessary.').
card_multiverse_id('drowner of secrets'/'LRW', '145979').

card_in_set('ego erasure', 'LRW').
card_original_type('ego erasure'/'LRW', 'Tribal Instant — Shapeshifter').
card_original_text('ego erasure'/'LRW', 'Changeling (This card is every creature type at all times.)\nCreatures target player controls get -2/-0 and lose all creature types until end of turn.').
card_first_print('ego erasure', 'LRW').
card_image_name('ego erasure'/'LRW', 'ego erasure').
card_uid('ego erasure'/'LRW', 'LRW:Ego Erasure:ego erasure').
card_rarity('ego erasure'/'LRW', 'Uncommon').
card_artist('ego erasure'/'LRW', 'Steven Belledin').
card_number('ego erasure'/'LRW', '59').
card_flavor_text('ego erasure'/'LRW', 'When all is taken away, all are equal.').
card_multiverse_id('ego erasure'/'LRW', '139692').

card_in_set('elvish branchbender', 'LRW').
card_original_type('elvish branchbender'/'LRW', 'Creature — Elf Druid').
card_original_text('elvish branchbender'/'LRW', '{T}: Until end of turn, target Forest becomes an X/X Treefolk creature in addition to its other types, where X is the number of Elves you control.').
card_first_print('elvish branchbender', 'LRW').
card_image_name('elvish branchbender'/'LRW', 'elvish branchbender').
card_uid('elvish branchbender'/'LRW', 'LRW:Elvish Branchbender:elvish branchbender').
card_rarity('elvish branchbender'/'LRW', 'Common').
card_artist('elvish branchbender'/'LRW', 'Ralph Horsley').
card_number('elvish branchbender'/'LRW', '204').
card_flavor_text('elvish branchbender'/'LRW', '\"How do the vinebred feel? Fah! We do not ask the puppet how it feels when the puppeteer bids it dance.\"').
card_multiverse_id('elvish branchbender'/'LRW', '146169').

card_in_set('elvish eulogist', 'LRW').
card_original_type('elvish eulogist'/'LRW', 'Creature — Elf Shaman').
card_original_text('elvish eulogist'/'LRW', 'Sacrifice Elvish Eulogist: You gain 1 life for each Elf card in your graveyard.').
card_first_print('elvish eulogist', 'LRW').
card_image_name('elvish eulogist'/'LRW', 'elvish eulogist').
card_uid('elvish eulogist'/'LRW', 'LRW:Elvish Eulogist:elvish eulogist').
card_rarity('elvish eulogist'/'LRW', 'Common').
card_artist('elvish eulogist'/'LRW', 'Ben Thompson').
card_number('elvish eulogist'/'LRW', '205').
card_flavor_text('elvish eulogist'/'LRW', '\"No matter how adept our artistic skill, our effigies can never hope to capture the vibrant beauty of a living elf. Perhaps that is truly why we mourn.\"').
card_multiverse_id('elvish eulogist'/'LRW', '141804').

card_in_set('elvish handservant', 'LRW').
card_original_type('elvish handservant'/'LRW', 'Creature — Elf Warrior').
card_original_text('elvish handservant'/'LRW', 'Whenever a player plays a Giant spell, you may put a +1/+1 counter on Elvish Handservant.').
card_first_print('elvish handservant', 'LRW').
card_image_name('elvish handservant'/'LRW', 'elvish handservant').
card_uid('elvish handservant'/'LRW', 'LRW:Elvish Handservant:elvish handservant').
card_rarity('elvish handservant'/'LRW', 'Common').
card_artist('elvish handservant'/'LRW', 'Steve Prescott').
card_number('elvish handservant'/'LRW', '206').
card_flavor_text('elvish handservant'/'LRW', 'The hardest lesson for any elf to learn is humility. It takes a giant to teach that.').
card_multiverse_id('elvish handservant'/'LRW', '146179').

card_in_set('elvish harbinger', 'LRW').
card_original_type('elvish harbinger'/'LRW', 'Creature — Elf Druid').
card_original_text('elvish harbinger'/'LRW', 'When Elvish Harbinger comes into play, you may search your library for an Elf card, reveal it, then shuffle your library and put that card on top of it.\n{T}: Add one mana of any color to your mana pool.').
card_first_print('elvish harbinger', 'LRW').
card_image_name('elvish harbinger'/'LRW', 'elvish harbinger').
card_uid('elvish harbinger'/'LRW', 'LRW:Elvish Harbinger:elvish harbinger').
card_rarity('elvish harbinger'/'LRW', 'Uncommon').
card_artist('elvish harbinger'/'LRW', 'Larry MacDougall').
card_number('elvish harbinger'/'LRW', '207').
card_multiverse_id('elvish harbinger'/'LRW', '139485').

card_in_set('elvish promenade', 'LRW').
card_original_type('elvish promenade'/'LRW', 'Tribal Sorcery — Elf').
card_original_text('elvish promenade'/'LRW', 'Put a 1/1 green Elf Warrior creature token into play for each Elf you control.').
card_first_print('elvish promenade', 'LRW').
card_image_name('elvish promenade'/'LRW', 'elvish promenade').
card_uid('elvish promenade'/'LRW', 'LRW:Elvish Promenade:elvish promenade').
card_rarity('elvish promenade'/'LRW', 'Uncommon').
card_artist('elvish promenade'/'LRW', 'Steve Ellis').
card_number('elvish promenade'/'LRW', '208').
card_flavor_text('elvish promenade'/'LRW', 'The faultless and immaculate castes form the lower tiers of elvish society, with the exquisite caste above them. At the pinnacle is the perfect, a consummate blend of aristocrat and predator.').
card_multiverse_id('elvish promenade'/'LRW', '139676').

card_in_set('entangling trap', 'LRW').
card_original_type('entangling trap'/'LRW', 'Enchantment').
card_original_text('entangling trap'/'LRW', 'Whenever you clash, tap target creature an opponent controls. If you won, that creature doesn\'t untap during its controller\'s next untap step. (This ability triggers after the clash ends.)').
card_first_print('entangling trap', 'LRW').
card_image_name('entangling trap'/'LRW', 'entangling trap').
card_uid('entangling trap'/'LRW', 'LRW:Entangling Trap:entangling trap').
card_rarity('entangling trap'/'LRW', 'Uncommon').
card_artist('entangling trap'/'LRW', 'Warren Mahy').
card_number('entangling trap'/'LRW', '13').
card_multiverse_id('entangling trap'/'LRW', '146583').

card_in_set('epic proportions', 'LRW').
card_original_type('epic proportions'/'LRW', 'Enchantment — Aura').
card_original_text('epic proportions'/'LRW', 'Flash\nEnchant creature\nEnchanted creature gets +5/+5 and has trample.').
card_first_print('epic proportions', 'LRW').
card_image_name('epic proportions'/'LRW', 'epic proportions').
card_uid('epic proportions'/'LRW', 'LRW:Epic Proportions:epic proportions').
card_rarity('epic proportions'/'LRW', 'Rare').
card_artist('epic proportions'/'LRW', 'Jesper Ejsing').
card_number('epic proportions'/'LRW', '209').
card_flavor_text('epic proportions'/'LRW', 'From mite to mighty.').
card_multiverse_id('epic proportions'/'LRW', '143732').

card_in_set('ethereal whiskergill', 'LRW').
card_original_type('ethereal whiskergill'/'LRW', 'Creature — Elemental').
card_original_text('ethereal whiskergill'/'LRW', 'Flying\nEthereal Whiskergill can\'t attack unless defending player controls an Island.').
card_first_print('ethereal whiskergill', 'LRW').
card_image_name('ethereal whiskergill'/'LRW', 'ethereal whiskergill').
card_uid('ethereal whiskergill'/'LRW', 'LRW:Ethereal Whiskergill:ethereal whiskergill').
card_rarity('ethereal whiskergill'/'LRW', 'Uncommon').
card_artist('ethereal whiskergill'/'LRW', 'Howard Lyon').
card_number('ethereal whiskergill'/'LRW', '60').
card_flavor_text('ethereal whiskergill'/'LRW', 'Fallowsages debate whether the whiskergill is native to the Dark Meanders or merely dreamstuff made real.').
card_multiverse_id('ethereal whiskergill'/'LRW', '139431').

card_in_set('exiled boggart', 'LRW').
card_original_type('exiled boggart'/'LRW', 'Creature — Goblin Rogue').
card_original_text('exiled boggart'/'LRW', 'When Exiled Boggart is put into a graveyard from play, discard a card.').
card_first_print('exiled boggart', 'LRW').
card_image_name('exiled boggart'/'LRW', 'exiled boggart').
card_uid('exiled boggart'/'LRW', 'LRW:Exiled Boggart:exiled boggart').
card_rarity('exiled boggart'/'LRW', 'Common').
card_artist('exiled boggart'/'LRW', 'Pete Venters').
card_number('exiled boggart'/'LRW', '109').
card_flavor_text('exiled boggart'/'LRW', 'Among the boggarts, there is only one real rule: all new treasures and experiences must be shared. Those who hoard their gifts commit the one truly unforgivable sin.').
card_multiverse_id('exiled boggart'/'LRW', '146443').

card_in_set('eyeblight\'s ending', 'LRW').
card_original_type('eyeblight\'s ending'/'LRW', 'Tribal Instant — Elf').
card_original_text('eyeblight\'s ending'/'LRW', 'Destroy target non-Elf creature.').
card_first_print('eyeblight\'s ending', 'LRW').
card_image_name('eyeblight\'s ending'/'LRW', 'eyeblight\'s ending').
card_uid('eyeblight\'s ending'/'LRW', 'LRW:Eyeblight\'s Ending:eyeblight\'s ending').
card_rarity('eyeblight\'s ending'/'LRW', 'Common').
card_artist('eyeblight\'s ending'/'LRW', 'Ron Spears').
card_number('eyeblight\'s ending'/'LRW', '110').
card_flavor_text('eyeblight\'s ending'/'LRW', '\"Those without beauty are Lorwyn\'s greatest tumor. The winnowers have an unpleasant duty, but a necessary one.\"\n—Eidren, perfect of Lys Alana').
card_multiverse_id('eyeblight\'s ending'/'LRW', '139449').

card_in_set('eyes of the wisent', 'LRW').
card_original_type('eyes of the wisent'/'LRW', 'Tribal Enchantment — Elemental').
card_original_text('eyes of the wisent'/'LRW', 'Whenever an opponent plays a blue spell during your turn, you may put a 4/4 green Elemental creature token into play.').
card_first_print('eyes of the wisent', 'LRW').
card_image_name('eyes of the wisent'/'LRW', 'eyes of the wisent').
card_uid('eyes of the wisent'/'LRW', 'LRW:Eyes of the Wisent:eyes of the wisent').
card_rarity('eyes of the wisent'/'LRW', 'Rare').
card_artist('eyes of the wisent'/'LRW', 'Aleksi Briclot').
card_number('eyes of the wisent'/'LRW', '210').
card_flavor_text('eyes of the wisent'/'LRW', 'Calm as a wisent\'s watch\n—Elvish expression meaning \"safe\"').
card_multiverse_id('eyes of the wisent'/'LRW', '143728').

card_in_set('facevaulter', 'LRW').
card_original_type('facevaulter'/'LRW', 'Creature — Goblin Warrior').
card_original_text('facevaulter'/'LRW', '{B}, Sacrifice a Goblin: Facevaulter gets +2/+2 until end of turn.').
card_first_print('facevaulter', 'LRW').
card_image_name('facevaulter'/'LRW', 'facevaulter').
card_uid('facevaulter'/'LRW', 'LRW:Facevaulter:facevaulter').
card_rarity('facevaulter'/'LRW', 'Common').
card_artist('facevaulter'/'LRW', 'Wayne Reynolds').
card_number('facevaulter'/'LRW', '111').
card_flavor_text('facevaulter'/'LRW', 'Boggarts get so excited when they find something new to smash that they really don\'t notice who gets underfoot.').
card_multiverse_id('facevaulter'/'LRW', '142361').

card_in_set('faerie harbinger', 'LRW').
card_original_type('faerie harbinger'/'LRW', 'Creature — Faerie Wizard').
card_original_text('faerie harbinger'/'LRW', 'Flash\nFlying\nWhen Faerie Harbinger comes into play, you may search your library for a Faerie card, reveal it, then shuffle your library and put that card on top of it.').
card_first_print('faerie harbinger', 'LRW').
card_image_name('faerie harbinger'/'LRW', 'faerie harbinger').
card_uid('faerie harbinger'/'LRW', 'LRW:Faerie Harbinger:faerie harbinger').
card_rarity('faerie harbinger'/'LRW', 'Uncommon').
card_artist('faerie harbinger'/'LRW', 'Larry MacDougall').
card_number('faerie harbinger'/'LRW', '61').
card_multiverse_id('faerie harbinger'/'LRW', '139427').

card_in_set('faerie tauntings', 'LRW').
card_original_type('faerie tauntings'/'LRW', 'Tribal Enchantment — Faerie').
card_original_text('faerie tauntings'/'LRW', 'Whenever you play a spell during an opponent\'s turn, you may have each opponent lose 1 life.').
card_first_print('faerie tauntings', 'LRW').
card_image_name('faerie tauntings'/'LRW', 'faerie tauntings').
card_uid('faerie tauntings'/'LRW', 'LRW:Faerie Tauntings:faerie tauntings').
card_rarity('faerie tauntings'/'LRW', 'Uncommon').
card_artist('faerie tauntings'/'LRW', 'Michael Sutfin').
card_number('faerie tauntings'/'LRW', '112').
card_flavor_text('faerie tauntings'/'LRW', 'Beneath the fae\'s constant pranks runs a subtler undercurrent of mockery: the influence of Oona, their hidden queen.').
card_multiverse_id('faerie tauntings'/'LRW', '139685').

card_in_set('faerie trickery', 'LRW').
card_original_type('faerie trickery'/'LRW', 'Tribal Instant — Faerie').
card_original_text('faerie trickery'/'LRW', 'Counter target non-Faerie spell. If that spell is countered this way, remove it from the game instead of putting it into its owner\'s graveyard.').
card_first_print('faerie trickery', 'LRW').
card_image_name('faerie trickery'/'LRW', 'faerie trickery').
card_uid('faerie trickery'/'LRW', 'LRW:Faerie Trickery:faerie trickery').
card_rarity('faerie trickery'/'LRW', 'Common').
card_artist('faerie trickery'/'LRW', 'Terese Nielsen').
card_number('faerie trickery'/'LRW', '62').
card_flavor_text('faerie trickery'/'LRW', 'The fae are so quick and their life spans so short that it\'s difficult to get retribution for their pranks.').
card_multiverse_id('faerie trickery'/'LRW', '139435').

card_in_set('fallowsage', 'LRW').
card_original_type('fallowsage'/'LRW', 'Creature — Merfolk Wizard').
card_original_text('fallowsage'/'LRW', 'Whenever Fallowsage becomes tapped, you may draw a card.').
card_first_print('fallowsage', 'LRW').
card_image_name('fallowsage'/'LRW', 'fallowsage').
card_uid('fallowsage'/'LRW', 'LRW:Fallowsage:fallowsage').
card_rarity('fallowsage'/'LRW', 'Uncommon').
card_artist('fallowsage'/'LRW', 'Paolo Parente').
card_number('fallowsage'/'LRW', '63').
card_flavor_text('fallowsage'/'LRW', 'Memories of ages past are said to swim the minds of lounging fallowsages.').
card_multiverse_id('fallowsage'/'LRW', '145803').

card_in_set('familiar\'s ruse', 'LRW').
card_original_type('familiar\'s ruse'/'LRW', 'Instant').
card_original_text('familiar\'s ruse'/'LRW', 'As an additional cost to play Familiar\'s Ruse, return a creature you control to its owner\'s hand.\nCounter target spell.').
card_first_print('familiar\'s ruse', 'LRW').
card_image_name('familiar\'s ruse'/'LRW', 'familiar\'s ruse').
card_uid('familiar\'s ruse'/'LRW', 'LRW:Familiar\'s Ruse:familiar\'s ruse').
card_rarity('familiar\'s ruse'/'LRW', 'Uncommon').
card_artist('familiar\'s ruse'/'LRW', 'Eric Fortune').
card_number('familiar\'s ruse'/'LRW', '64').
card_flavor_text('familiar\'s ruse'/'LRW', 'Because of their capricious nature, faeries can serve as living lenses for disruptive magic.').
card_multiverse_id('familiar\'s ruse'/'LRW', '146584').

card_in_set('fathom trawl', 'LRW').
card_original_type('fathom trawl'/'LRW', 'Sorcery').
card_original_text('fathom trawl'/'LRW', 'Reveal cards from the top of your library until you reveal three nonland cards. Put the nonland cards revealed this way into your hand, then put the rest of the revealed cards on the bottom of your library in any order.').
card_first_print('fathom trawl', 'LRW').
card_image_name('fathom trawl'/'LRW', 'fathom trawl').
card_uid('fathom trawl'/'LRW', 'LRW:Fathom Trawl:fathom trawl').
card_rarity('fathom trawl'/'LRW', 'Rare').
card_artist('fathom trawl'/'LRW', 'Paul Chadwick').
card_number('fathom trawl'/'LRW', '65').
card_multiverse_id('fathom trawl'/'LRW', '146170').

card_in_set('faultgrinder', 'LRW').
card_original_type('faultgrinder'/'LRW', 'Creature — Elemental').
card_original_text('faultgrinder'/'LRW', 'Trample\nWhen Faultgrinder comes into play, destroy target land.\nEvoke {4}{R} (You may play this spell for its evoke cost. If you do, it\'s sacrificed when it comes into play.)').
card_first_print('faultgrinder', 'LRW').
card_image_name('faultgrinder'/'LRW', 'faultgrinder').
card_uid('faultgrinder'/'LRW', 'LRW:Faultgrinder:faultgrinder').
card_rarity('faultgrinder'/'LRW', 'Common').
card_artist('faultgrinder'/'LRW', 'Anthony S. Waters').
card_number('faultgrinder'/'LRW', '163').
card_multiverse_id('faultgrinder'/'LRW', '145964').

card_in_set('favor of the mighty', 'LRW').
card_original_type('favor of the mighty'/'LRW', 'Tribal Enchantment — Giant').
card_original_text('favor of the mighty'/'LRW', 'Each creature with the highest converted mana cost has protection from all colors.').
card_first_print('favor of the mighty', 'LRW').
card_image_name('favor of the mighty'/'LRW', 'favor of the mighty').
card_uid('favor of the mighty'/'LRW', 'LRW:Favor of the Mighty:favor of the mighty').
card_rarity('favor of the mighty'/'LRW', 'Rare').
card_artist('favor of the mighty'/'LRW', 'Larry MacDougall').
card_number('favor of the mighty'/'LRW', '14').
card_flavor_text('favor of the mighty'/'LRW', '\"What does a mountain fear of a fly? Giants are barely aware of us, let alone afraid.\"\n—Gaddock Teeg').
card_multiverse_id('favor of the mighty'/'LRW', '143683').

card_in_set('fertile ground', 'LRW').
card_original_type('fertile ground'/'LRW', 'Enchantment — Aura').
card_original_text('fertile ground'/'LRW', 'Enchant land\nWhenever enchanted land is tapped for mana, its controller adds one mana of any color to his or her mana pool.').
card_image_name('fertile ground'/'LRW', 'fertile ground').
card_uid('fertile ground'/'LRW', 'LRW:Fertile Ground:fertile ground').
card_rarity('fertile ground'/'LRW', 'Common').
card_artist('fertile ground'/'LRW', 'Mark Tedin').
card_number('fertile ground'/'LRW', '211').
card_flavor_text('fertile ground'/'LRW', 'Pretty, valuable, and delicious—a boggart thief\'s trifecta.').
card_multiverse_id('fertile ground'/'LRW', '139502').

card_in_set('final revels', 'LRW').
card_original_type('final revels'/'LRW', 'Sorcery').
card_original_text('final revels'/'LRW', 'Choose one All creatures get +2/+0 until end of turn; or all creatures get -0/-2 until end of turn.').
card_first_print('final revels', 'LRW').
card_image_name('final revels'/'LRW', 'final revels').
card_uid('final revels'/'LRW', 'LRW:Final Revels:final revels').
card_rarity('final revels'/'LRW', 'Uncommon').
card_artist('final revels'/'LRW', 'Omar Rayyan').
card_number('final revels'/'LRW', '113').
card_flavor_text('final revels'/'LRW', 'One whiff of the sweet, pungent scent leads to euphoria—or to an early grave.').
card_multiverse_id('final revels'/'LRW', '146171').

card_in_set('fire-belly changeling', 'LRW').
card_original_type('fire-belly changeling'/'LRW', 'Creature — Shapeshifter').
card_original_text('fire-belly changeling'/'LRW', 'Changeling (This card is every creature type at all times.)\n{R}: Fire-Belly Changeling gets +1/+0 until end of turn. Play this ability no more than twice each turn.').
card_first_print('fire-belly changeling', 'LRW').
card_image_name('fire-belly changeling'/'LRW', 'fire-belly changeling').
card_uid('fire-belly changeling'/'LRW', 'LRW:Fire-Belly Changeling:fire-belly changeling').
card_rarity('fire-belly changeling'/'LRW', 'Common').
card_artist('fire-belly changeling'/'LRW', 'Randy Gallegos').
card_number('fire-belly changeling'/'LRW', '164').
card_flavor_text('fire-belly changeling'/'LRW', '\"My ears say it hisses. My fingers say it burns.\"\n—Auntie Wort').
card_multiverse_id('fire-belly changeling'/'LRW', '140338').

card_in_set('fistful of force', 'LRW').
card_original_type('fistful of force'/'LRW', 'Instant').
card_original_text('fistful of force'/'LRW', 'Target creature gets +2/+2 until end of turn. Clash with an opponent. If you win, that creature gets an additional +2/+2 and gains trample until end of turn. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_first_print('fistful of force', 'LRW').
card_image_name('fistful of force'/'LRW', 'fistful of force').
card_uid('fistful of force'/'LRW', 'LRW:Fistful of Force:fistful of force').
card_rarity('fistful of force'/'LRW', 'Common').
card_artist('fistful of force'/'LRW', 'Ralph Horsley').
card_number('fistful of force'/'LRW', '212').
card_multiverse_id('fistful of force'/'LRW', '143372').

card_in_set('flamekin bladewhirl', 'LRW').
card_original_type('flamekin bladewhirl'/'LRW', 'Creature — Elemental Warrior').
card_original_text('flamekin bladewhirl'/'LRW', 'As an additional cost to play Flamekin Bladewhirl, reveal an Elemental card from your hand or pay {3}.').
card_first_print('flamekin bladewhirl', 'LRW').
card_image_name('flamekin bladewhirl'/'LRW', 'flamekin bladewhirl').
card_uid('flamekin bladewhirl'/'LRW', 'LRW:Flamekin Bladewhirl:flamekin bladewhirl').
card_rarity('flamekin bladewhirl'/'LRW', 'Uncommon').
card_artist('flamekin bladewhirl'/'LRW', 'Mark Zug').
card_number('flamekin bladewhirl'/'LRW', '165').
card_flavor_text('flamekin bladewhirl'/'LRW', '\"The elves may try to confine us, but they will learn that our blazing spirit can never be suppressed.\"\n—Vessifrus, flamekin demagogue').
card_multiverse_id('flamekin bladewhirl'/'LRW', '140343').

card_in_set('flamekin brawler', 'LRW').
card_original_type('flamekin brawler'/'LRW', 'Creature — Elemental Warrior').
card_original_text('flamekin brawler'/'LRW', '{R}: Flamekin Brawler gets +1/+0 until end of turn.').
card_first_print('flamekin brawler', 'LRW').
card_image_name('flamekin brawler'/'LRW', 'flamekin brawler').
card_uid('flamekin brawler'/'LRW', 'LRW:Flamekin Brawler:flamekin brawler').
card_rarity('flamekin brawler'/'LRW', 'Common').
card_artist('flamekin brawler'/'LRW', 'Daren Bader').
card_number('flamekin brawler'/'LRW', '166').
card_flavor_text('flamekin brawler'/'LRW', 'When he hits people, they stay hit.').
card_multiverse_id('flamekin brawler'/'LRW', '139461').

card_in_set('flamekin harbinger', 'LRW').
card_original_type('flamekin harbinger'/'LRW', 'Creature — Elemental Shaman').
card_original_text('flamekin harbinger'/'LRW', 'When Flamekin Harbinger comes into play, you may search your library for an Elemental card, reveal it, then shuffle your library and put that card on top of it.').
card_first_print('flamekin harbinger', 'LRW').
card_image_name('flamekin harbinger'/'LRW', 'flamekin harbinger').
card_uid('flamekin harbinger'/'LRW', 'LRW:Flamekin Harbinger:flamekin harbinger').
card_rarity('flamekin harbinger'/'LRW', 'Uncommon').
card_artist('flamekin harbinger'/'LRW', 'Steve Prescott').
card_number('flamekin harbinger'/'LRW', '167').
card_multiverse_id('flamekin harbinger'/'LRW', '139463').

card_in_set('flamekin spitfire', 'LRW').
card_original_type('flamekin spitfire'/'LRW', 'Creature — Elemental Shaman').
card_original_text('flamekin spitfire'/'LRW', '{3}{R}: Flamekin Spitfire deals 1 damage to target creature or player.').
card_first_print('flamekin spitfire', 'LRW').
card_image_name('flamekin spitfire'/'LRW', 'flamekin spitfire').
card_uid('flamekin spitfire'/'LRW', 'LRW:Flamekin Spitfire:flamekin spitfire').
card_rarity('flamekin spitfire'/'LRW', 'Uncommon').
card_artist('flamekin spitfire'/'LRW', 'Zoltan Boros & Gabor Szikszai').
card_number('flamekin spitfire'/'LRW', '168').
card_flavor_text('flamekin spitfire'/'LRW', 'Some flamekin warriors explore the art of coherence, an ancient discipline that harnesses the chaos of fire and focuses it with pinpoint precision.').
card_multiverse_id('flamekin spitfire'/'LRW', '139728').

card_in_set('fodder launch', 'LRW').
card_original_type('fodder launch'/'LRW', 'Tribal Sorcery — Goblin').
card_original_text('fodder launch'/'LRW', 'As an additional cost to play Fodder Launch, sacrifice a Goblin.\nTarget creature gets -5/-5 until end of turn. Fodder Launch deals 5 damage to that creature\'s controller.').
card_first_print('fodder launch', 'LRW').
card_image_name('fodder launch'/'LRW', 'fodder launch').
card_uid('fodder launch'/'LRW', 'LRW:Fodder Launch:fodder launch').
card_rarity('fodder launch'/'LRW', 'Uncommon').
card_artist('fodder launch'/'LRW', 'Nils Hamm').
card_number('fodder launch'/'LRW', '114').
card_flavor_text('fodder launch'/'LRW', 'Leave it to a boggart to come up with a projectile as disgusting as it is deadly.').
card_multiverse_id('fodder launch'/'LRW', '146447').

card_in_set('footbottom feast', 'LRW').
card_original_type('footbottom feast'/'LRW', 'Instant').
card_original_text('footbottom feast'/'LRW', 'Put any number of target creature cards from your graveyard on top of your library.\nDraw a card.').
card_first_print('footbottom feast', 'LRW').
card_image_name('footbottom feast'/'LRW', 'footbottom feast').
card_uid('footbottom feast'/'LRW', 'LRW:Footbottom Feast:footbottom feast').
card_rarity('footbottom feast'/'LRW', 'Common').
card_artist('footbottom feast'/'LRW', 'Jim Nelson').
card_number('footbottom feast'/'LRW', '115').
card_flavor_text('footbottom feast'/'LRW', 'The scent of rot and vinegar fills the marsh, inviting boggarts from every warren to reunite and feast.').
card_multiverse_id('footbottom feast'/'LRW', '139440').

card_in_set('forced fruition', 'LRW').
card_original_type('forced fruition'/'LRW', 'Enchantment').
card_original_text('forced fruition'/'LRW', 'Whenever an opponent plays a spell, that player draws seven cards.').
card_first_print('forced fruition', 'LRW').
card_image_name('forced fruition'/'LRW', 'forced fruition').
card_uid('forced fruition'/'LRW', 'LRW:Forced Fruition:forced fruition').
card_rarity('forced fruition'/'LRW', 'Rare').
card_artist('forced fruition'/'LRW', 'William O\'Connor').
card_number('forced fruition'/'LRW', '66').
card_flavor_text('forced fruition'/'LRW', '\"Petals within petals within petals, tadpole. The truth lurks below an opulence of illusion.\"\n—Neerdiv, fallowsage').
card_multiverse_id('forced fruition'/'LRW', '146166').

card_in_set('forest', 'LRW').
card_original_type('forest'/'LRW', 'Basic Land — Forest').
card_original_text('forest'/'LRW', 'G').
card_image_name('forest'/'LRW', 'forest1').
card_uid('forest'/'LRW', 'LRW:Forest:forest1').
card_rarity('forest'/'LRW', 'Basic Land').
card_artist('forest'/'LRW', 'Omar Rayyan').
card_number('forest'/'LRW', '298').
card_multiverse_id('forest'/'LRW', '143618').

card_in_set('forest', 'LRW').
card_original_type('forest'/'LRW', 'Basic Land — Forest').
card_original_text('forest'/'LRW', 'G').
card_image_name('forest'/'LRW', 'forest2').
card_uid('forest'/'LRW', 'LRW:Forest:forest2').
card_rarity('forest'/'LRW', 'Basic Land').
card_artist('forest'/'LRW', 'Rob Alexander').
card_number('forest'/'LRW', '299').
card_multiverse_id('forest'/'LRW', '143625').

card_in_set('forest', 'LRW').
card_original_type('forest'/'LRW', 'Basic Land — Forest').
card_original_text('forest'/'LRW', 'G').
card_image_name('forest'/'LRW', 'forest3').
card_uid('forest'/'LRW', 'LRW:Forest:forest3').
card_rarity('forest'/'LRW', 'Basic Land').
card_artist('forest'/'LRW', 'Steve Prescott').
card_number('forest'/'LRW', '300').
card_multiverse_id('forest'/'LRW', '143617').

card_in_set('forest', 'LRW').
card_original_type('forest'/'LRW', 'Basic Land — Forest').
card_original_text('forest'/'LRW', 'G').
card_image_name('forest'/'LRW', 'forest4').
card_uid('forest'/'LRW', 'LRW:Forest:forest4').
card_rarity('forest'/'LRW', 'Basic Land').
card_artist('forest'/'LRW', 'Darrell Riche').
card_number('forest'/'LRW', '301').
card_multiverse_id('forest'/'LRW', '143633').

card_in_set('gaddock teeg', 'LRW').
card_original_type('gaddock teeg'/'LRW', 'Legendary Creature — Kithkin Advisor').
card_original_text('gaddock teeg'/'LRW', 'Noncreature spells with converted mana cost 4 or greater can\'t be played.\nNoncreature spells with {X} in their mana costs can\'t be played.').
card_first_print('gaddock teeg', 'LRW').
card_image_name('gaddock teeg'/'LRW', 'gaddock teeg').
card_uid('gaddock teeg'/'LRW', 'LRW:Gaddock Teeg:gaddock teeg').
card_rarity('gaddock teeg'/'LRW', 'Rare').
card_artist('gaddock teeg'/'LRW', 'Greg Staples').
card_number('gaddock teeg'/'LRW', '248').
card_flavor_text('gaddock teeg'/'LRW', 'So great is his wisdom and spirit that many who have met him say that they stood before a giant of a man and talked to the wisest of the four winds.').
card_multiverse_id('gaddock teeg'/'LRW', '140188').

card_in_set('galepowder mage', 'LRW').
card_original_type('galepowder mage'/'LRW', 'Creature — Kithkin Wizard').
card_original_text('galepowder mage'/'LRW', 'Flying\nWhenever Galepowder Mage attacks, remove another target creature from the game. Return that card to play under its owner\'s control at end of turn.').
card_first_print('galepowder mage', 'LRW').
card_image_name('galepowder mage'/'LRW', 'galepowder mage').
card_uid('galepowder mage'/'LRW', 'LRW:Galepowder Mage:galepowder mage').
card_rarity('galepowder mage'/'LRW', 'Rare').
card_artist('galepowder mage'/'LRW', 'Jeremy Jarvis').
card_number('galepowder mage'/'LRW', '15').
card_multiverse_id('galepowder mage'/'LRW', '139666').

card_in_set('garruk wildspeaker', 'LRW').
card_original_type('garruk wildspeaker'/'LRW', 'Planeswalker — Garruk').
card_original_text('garruk wildspeaker'/'LRW', '+1: Untap two target lands.\n-1: Put a 3/3 green Beast creature token into play.\n-4: Creatures you control get +3/+3 and gain trample until end of turn.').
card_image_name('garruk wildspeaker'/'LRW', 'garruk wildspeaker').
card_uid('garruk wildspeaker'/'LRW', 'LRW:Garruk Wildspeaker:garruk wildspeaker').
card_rarity('garruk wildspeaker'/'LRW', 'Rare').
card_artist('garruk wildspeaker'/'LRW', 'Aleksi Briclot').
card_number('garruk wildspeaker'/'LRW', '213').
card_multiverse_id('garruk wildspeaker'/'LRW', '140205').

card_in_set('ghostly changeling', 'LRW').
card_original_type('ghostly changeling'/'LRW', 'Creature — Shapeshifter').
card_original_text('ghostly changeling'/'LRW', 'Changeling (This card is every creature type at all times.)\n{1}{B}: Ghostly Changeling gets +1/+1 until end of turn.').
card_first_print('ghostly changeling', 'LRW').
card_image_name('ghostly changeling'/'LRW', 'ghostly changeling').
card_uid('ghostly changeling'/'LRW', 'LRW:Ghostly Changeling:ghostly changeling').
card_rarity('ghostly changeling'/'LRW', 'Uncommon').
card_artist('ghostly changeling'/'LRW', 'Chuck Lukacs').
card_number('ghostly changeling'/'LRW', '116').
card_flavor_text('ghostly changeling'/'LRW', 'In desolate places, changelings may take the shape of fancies, or memories, or fears.').
card_multiverse_id('ghostly changeling'/'LRW', '139447').

card_in_set('giant harbinger', 'LRW').
card_original_type('giant harbinger'/'LRW', 'Creature — Giant Shaman').
card_original_text('giant harbinger'/'LRW', 'When Giant Harbinger comes into play, you may search your library for a Giant card, reveal it, then shuffle your library and put that card on top of it.').
card_first_print('giant harbinger', 'LRW').
card_image_name('giant harbinger'/'LRW', 'giant harbinger').
card_uid('giant harbinger'/'LRW', 'LRW:Giant Harbinger:giant harbinger').
card_rarity('giant harbinger'/'LRW', 'Uncommon').
card_artist('giant harbinger'/'LRW', 'Steve Prescott').
card_number('giant harbinger'/'LRW', '169').
card_multiverse_id('giant harbinger'/'LRW', '139474').

card_in_set('giant\'s ire', 'LRW').
card_original_type('giant\'s ire'/'LRW', 'Tribal Sorcery — Giant').
card_original_text('giant\'s ire'/'LRW', 'Giant\'s Ire deals 4 damage to target player. If you control a Giant, draw a card.').
card_first_print('giant\'s ire', 'LRW').
card_image_name('giant\'s ire'/'LRW', 'giant\'s ire').
card_uid('giant\'s ire'/'LRW', 'LRW:Giant\'s Ire:giant\'s ire').
card_rarity('giant\'s ire'/'LRW', 'Common').
card_artist('giant\'s ire'/'LRW', 'Alex Horley-Orlandelli').
card_number('giant\'s ire'/'LRW', '170').
card_flavor_text('giant\'s ire'/'LRW', 'The only feeling greater than hurling something a mile is crushing something else with it that was really, really irritating you.').
card_multiverse_id('giant\'s ire'/'LRW', '139477').

card_in_set('gilt-leaf ambush', 'LRW').
card_original_type('gilt-leaf ambush'/'LRW', 'Tribal Instant — Elf').
card_original_text('gilt-leaf ambush'/'LRW', 'Put two 1/1 green Elf Warrior creature tokens into play. Clash with an opponent. If you win, those creatures gain deathtouch until end of turn. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost. Whenever a creature with deathtouch deals damage to a creature, destroy that creature.)').
card_first_print('gilt-leaf ambush', 'LRW').
card_image_name('gilt-leaf ambush'/'LRW', 'gilt-leaf ambush').
card_uid('gilt-leaf ambush'/'LRW', 'LRW:Gilt-Leaf Ambush:gilt-leaf ambush').
card_rarity('gilt-leaf ambush'/'LRW', 'Common').
card_artist('gilt-leaf ambush'/'LRW', 'Steve Prescott').
card_number('gilt-leaf ambush'/'LRW', '214').
card_multiverse_id('gilt-leaf ambush'/'LRW', '145984').

card_in_set('gilt-leaf palace', 'LRW').
card_original_type('gilt-leaf palace'/'LRW', 'Land').
card_original_text('gilt-leaf palace'/'LRW', 'As Gilt-Leaf Palace comes into play, you may reveal an Elf card from your hand. If you don\'t, Gilt-Leaf Palace comes into play tapped.\n{T}: Add {B} or {G} to your mana pool.').
card_first_print('gilt-leaf palace', 'LRW').
card_image_name('gilt-leaf palace'/'LRW', 'gilt-leaf palace').
card_uid('gilt-leaf palace'/'LRW', 'LRW:Gilt-Leaf Palace:gilt-leaf palace').
card_rarity('gilt-leaf palace'/'LRW', 'Rare').
card_artist('gilt-leaf palace'/'LRW', 'Christopher Moeller').
card_number('gilt-leaf palace'/'LRW', '268').
card_flavor_text('gilt-leaf palace'/'LRW', 'Dawn\'s Light, greatest palace of Gilt-Leaf Wood, is built on tiers of wood and power.').
card_multiverse_id('gilt-leaf palace'/'LRW', '153455').

card_in_set('gilt-leaf seer', 'LRW').
card_original_type('gilt-leaf seer'/'LRW', 'Creature — Elf Shaman').
card_original_text('gilt-leaf seer'/'LRW', '{G}, {T}: Look at the top two cards of your library, then put them back in any order.').
card_first_print('gilt-leaf seer', 'LRW').
card_image_name('gilt-leaf seer'/'LRW', 'gilt-leaf seer').
card_uid('gilt-leaf seer'/'LRW', 'LRW:Gilt-Leaf Seer:gilt-leaf seer').
card_rarity('gilt-leaf seer'/'LRW', 'Common').
card_artist('gilt-leaf seer'/'LRW', 'Darrell Riche').
card_number('gilt-leaf seer'/'LRW', '215').
card_flavor_text('gilt-leaf seer'/'LRW', 'Desmera blinded her seers so that her beauty would be the last image burned in their memories. The act only deepened their insight.').
card_multiverse_id('gilt-leaf seer'/'LRW', '145812').

card_in_set('glarewielder', 'LRW').
card_original_type('glarewielder'/'LRW', 'Creature — Elemental Shaman').
card_original_text('glarewielder'/'LRW', 'Haste\nWhen Glarewielder comes into play, up to two target creatures can\'t block this turn.\nEvoke {1}{R} (You may play this spell for its evoke cost. If you do, it\'s sacrificed when it comes into play.)').
card_first_print('glarewielder', 'LRW').
card_image_name('glarewielder'/'LRW', 'glarewielder').
card_uid('glarewielder'/'LRW', 'LRW:Glarewielder:glarewielder').
card_rarity('glarewielder'/'LRW', 'Uncommon').
card_artist('glarewielder'/'LRW', 'Nils Hamm').
card_number('glarewielder'/'LRW', '171').
card_multiverse_id('glarewielder'/'LRW', '145981').

card_in_set('glen elendra pranksters', 'LRW').
card_original_type('glen elendra pranksters'/'LRW', 'Creature — Faerie Wizard').
card_original_text('glen elendra pranksters'/'LRW', 'Flying\nWhenever you play a spell during an opponent\'s turn, you may return target creature you control to its owner\'s hand.').
card_first_print('glen elendra pranksters', 'LRW').
card_image_name('glen elendra pranksters'/'LRW', 'glen elendra pranksters').
card_uid('glen elendra pranksters'/'LRW', 'LRW:Glen Elendra Pranksters:glen elendra pranksters').
card_rarity('glen elendra pranksters'/'LRW', 'Uncommon').
card_artist('glen elendra pranksters'/'LRW', 'Omar Rayyan').
card_number('glen elendra pranksters'/'LRW', '67').
card_flavor_text('glen elendra pranksters'/'LRW', 'Victims spirited through a faerie ring find themselves stranded miles away.').
card_multiverse_id('glen elendra pranksters'/'LRW', '146162').

card_in_set('glimmerdust nap', 'LRW').
card_original_type('glimmerdust nap'/'LRW', 'Enchantment — Aura').
card_original_text('glimmerdust nap'/'LRW', 'Enchant tapped creature\nEnchanted creature doesn\'t untap during its controller\'s untap step.').
card_first_print('glimmerdust nap', 'LRW').
card_image_name('glimmerdust nap'/'LRW', 'glimmerdust nap').
card_uid('glimmerdust nap'/'LRW', 'LRW:Glimmerdust Nap:glimmerdust nap').
card_rarity('glimmerdust nap'/'LRW', 'Common').
card_artist('glimmerdust nap'/'LRW', 'Greg Staples').
card_number('glimmerdust nap'/'LRW', '68').
card_flavor_text('glimmerdust nap'/'LRW', 'The dreams of giants are as long as time and as deep as the earth. Thus they are prized by the dream-harvesting fae.').
card_multiverse_id('glimmerdust nap'/'LRW', '139436').

card_in_set('goatnapper', 'LRW').
card_original_type('goatnapper'/'LRW', 'Creature — Goblin Rogue').
card_original_text('goatnapper'/'LRW', 'When Goatnapper comes into play, untap target Goat and gain control of it until end of turn. It gains haste until end of turn.').
card_first_print('goatnapper', 'LRW').
card_image_name('goatnapper'/'LRW', 'goatnapper').
card_uid('goatnapper'/'LRW', 'LRW:Goatnapper:goatnapper').
card_rarity('goatnapper'/'LRW', 'Uncommon').
card_artist('goatnapper'/'LRW', 'Ron Spencer').
card_number('goatnapper'/'LRW', '172').
card_flavor_text('goatnapper'/'LRW', 'Kith goats are just for practice. The real prize, of course, is a giant\'s cloudgoat.').
card_multiverse_id('goatnapper'/'LRW', '142363').

card_in_set('goldmeadow dodger', 'LRW').
card_original_type('goldmeadow dodger'/'LRW', 'Creature — Kithkin Rogue').
card_original_text('goldmeadow dodger'/'LRW', 'Goldmeadow Dodger can\'t be blocked by creatures with power 4 or greater.').
card_first_print('goldmeadow dodger', 'LRW').
card_image_name('goldmeadow dodger'/'LRW', 'goldmeadow dodger').
card_uid('goldmeadow dodger'/'LRW', 'LRW:Goldmeadow Dodger:goldmeadow dodger').
card_rarity('goldmeadow dodger'/'LRW', 'Common').
card_artist('goldmeadow dodger'/'LRW', 'Omar Rayyan').
card_number('goldmeadow dodger'/'LRW', '16').
card_flavor_text('goldmeadow dodger'/'LRW', '\"I\'ve gotten close enough to a giant to smell his breath, but none has ever so much as spotted me. I wonder how long my record can extend?\"').
card_multiverse_id('goldmeadow dodger'/'LRW', '142356').

card_in_set('goldmeadow harrier', 'LRW').
card_original_type('goldmeadow harrier'/'LRW', 'Creature — Kithkin Soldier').
card_original_text('goldmeadow harrier'/'LRW', '{W}, {T}: Tap target creature.').
card_first_print('goldmeadow harrier', 'LRW').
card_image_name('goldmeadow harrier'/'LRW', 'goldmeadow harrier').
card_uid('goldmeadow harrier'/'LRW', 'LRW:Goldmeadow Harrier:goldmeadow harrier').
card_rarity('goldmeadow harrier'/'LRW', 'Common').
card_artist('goldmeadow harrier'/'LRW', 'Steve Prescott').
card_number('goldmeadow harrier'/'LRW', '17').
card_flavor_text('goldmeadow harrier'/'LRW', '\"It\'s a proven fact that sling-stones from the dawn side of the riverbank sail the farthest and truest.\"\n—Deagan, cenn of Burrenton').
card_multiverse_id('goldmeadow harrier'/'LRW', '139397').

card_in_set('goldmeadow stalwart', 'LRW').
card_original_type('goldmeadow stalwart'/'LRW', 'Creature — Kithkin Soldier').
card_original_text('goldmeadow stalwart'/'LRW', 'As an additional cost to play Goldmeadow Stalwart, reveal a Kithkin card from your hand or pay {3}.').
card_first_print('goldmeadow stalwart', 'LRW').
card_image_name('goldmeadow stalwart'/'LRW', 'goldmeadow stalwart').
card_uid('goldmeadow stalwart'/'LRW', 'LRW:Goldmeadow Stalwart:goldmeadow stalwart').
card_rarity('goldmeadow stalwart'/'LRW', 'Uncommon').
card_artist('goldmeadow stalwart'/'LRW', 'Wayne Reynolds').
card_number('goldmeadow stalwart'/'LRW', '18').
card_flavor_text('goldmeadow stalwart'/'LRW', 'The thoughtweft ties a clachan together. Sharing each other\'s hopes and fears, all the village\'s citizens spring into action upon the first threat to any one of them.').
card_multiverse_id('goldmeadow stalwart'/'LRW', '139698').

card_in_set('guardian of cloverdell', 'LRW').
card_original_type('guardian of cloverdell'/'LRW', 'Creature — Treefolk Shaman').
card_original_text('guardian of cloverdell'/'LRW', 'When Guardian of Cloverdell comes into play, put three 1/1 white Kithkin Soldier creature tokens into play.\n{G}, Sacrifice a Kithkin: You gain 1 life.').
card_first_print('guardian of cloverdell', 'LRW').
card_image_name('guardian of cloverdell'/'LRW', 'guardian of cloverdell').
card_uid('guardian of cloverdell'/'LRW', 'LRW:Guardian of Cloverdell:guardian of cloverdell').
card_rarity('guardian of cloverdell'/'LRW', 'Uncommon').
card_artist('guardian of cloverdell'/'LRW', 'Jesper Ejsing').
card_number('guardian of cloverdell'/'LRW', '216').
card_flavor_text('guardian of cloverdell'/'LRW', 'Although they\'re protective of all creatures, many treefolk are especially fond of the empathic kithkin.').
card_multiverse_id('guardian of cloverdell'/'LRW', '139665').

card_in_set('guile', 'LRW').
card_original_type('guile'/'LRW', 'Creature — Elemental Incarnation').
card_original_text('guile'/'LRW', 'Guile can\'t be blocked except by three or more creatures.\nIf a spell or ability you control would counter a spell, instead remove that spell from the game and you may play that card without paying its mana cost.\nWhen Guile is put into a graveyard from anywhere, shuffle it into its owner\'s library.').
card_first_print('guile', 'LRW').
card_image_name('guile'/'LRW', 'guile').
card_uid('guile'/'LRW', 'LRW:Guile:guile').
card_rarity('guile'/'LRW', 'Rare').
card_artist('guile'/'LRW', 'Zoltan Boros & Gabor Szikszai').
card_number('guile'/'LRW', '69').
card_multiverse_id('guile'/'LRW', '140190').

card_in_set('hamletback goliath', 'LRW').
card_original_type('hamletback goliath'/'LRW', 'Creature — Giant Warrior').
card_original_text('hamletback goliath'/'LRW', 'Whenever another creature comes into play, you may put X +1/+1 counters on Hamletback Goliath, where X is that creature\'s power.').
card_image_name('hamletback goliath'/'LRW', 'hamletback goliath').
card_uid('hamletback goliath'/'LRW', 'LRW:Hamletback Goliath:hamletback goliath').
card_rarity('hamletback goliath'/'LRW', 'Rare').
card_artist('hamletback goliath'/'LRW', 'Parente & Brian Snõddy').
card_number('hamletback goliath'/'LRW', '173').
card_flavor_text('hamletback goliath'/'LRW', '\"If you live on a giant\'s back, there\'s only one individual you\'ll ever need to fear.\"\n—Gaddock Teeg').
card_multiverse_id('hamletback goliath'/'LRW', '143682').

card_in_set('harpoon sniper', 'LRW').
card_original_type('harpoon sniper'/'LRW', 'Creature — Merfolk Archer').
card_original_text('harpoon sniper'/'LRW', '{W}, {T}: Harpoon Sniper deals X damage to target attacking or blocking creature, where X is the number of Merfolk you control.').
card_first_print('harpoon sniper', 'LRW').
card_image_name('harpoon sniper'/'LRW', 'harpoon sniper').
card_uid('harpoon sniper'/'LRW', 'LRW:Harpoon Sniper:harpoon sniper').
card_rarity('harpoon sniper'/'LRW', 'Uncommon').
card_artist('harpoon sniper'/'LRW', 'Dominick Domingo').
card_number('harpoon sniper'/'LRW', '19').
card_flavor_text('harpoon sniper'/'LRW', 'Made from whiskergill bones, merrow spinebows can fire bolts through tree trunks.').
card_multiverse_id('harpoon sniper'/'LRW', '139407').

card_in_set('heal the scars', 'LRW').
card_original_type('heal the scars'/'LRW', 'Instant').
card_original_text('heal the scars'/'LRW', 'Regenerate target creature. You gain life equal to that creature\'s toughness.').
card_first_print('heal the scars', 'LRW').
card_image_name('heal the scars'/'LRW', 'heal the scars').
card_uid('heal the scars'/'LRW', 'LRW:Heal the Scars:heal the scars').
card_rarity('heal the scars'/'LRW', 'Common').
card_artist('heal the scars'/'LRW', 'Carl Frank').
card_number('heal the scars'/'LRW', '217').
card_flavor_text('heal the scars'/'LRW', 'Elvish battle-magic has evolved two specialties: inflicting wounds that scar, and healing wounds without scarring. Politics determines the recipients of each.').
card_multiverse_id('heal the scars'/'LRW', '145808').

card_in_set('hearthcage giant', 'LRW').
card_original_type('hearthcage giant'/'LRW', 'Creature — Giant Warrior').
card_original_text('hearthcage giant'/'LRW', 'When Hearthcage Giant comes into play, put two 3/1 red Elemental Shaman creature tokens into play.\nSacrifice an Elemental: Target Giant creature gets +3/+1 until end of turn.').
card_first_print('hearthcage giant', 'LRW').
card_image_name('hearthcage giant'/'LRW', 'hearthcage giant').
card_uid('hearthcage giant'/'LRW', 'LRW:Hearthcage Giant:hearthcage giant').
card_rarity('hearthcage giant'/'LRW', 'Uncommon').
card_artist('hearthcage giant'/'LRW', 'Zoltan Boros & Gabor Szikszai').
card_number('hearthcage giant'/'LRW', '174').
card_flavor_text('hearthcage giant'/'LRW', 'The flamekin are mere kindling for his warmth.').
card_multiverse_id('hearthcage giant'/'LRW', '139691').

card_in_set('heat shimmer', 'LRW').
card_original_type('heat shimmer'/'LRW', 'Sorcery').
card_original_text('heat shimmer'/'LRW', 'Put a token into play that\'s a copy of target creature. It has haste and \"At end of turn, remove this permanent from the game.\"').
card_first_print('heat shimmer', 'LRW').
card_image_name('heat shimmer'/'LRW', 'heat shimmer').
card_uid('heat shimmer'/'LRW', 'LRW:Heat Shimmer:heat shimmer').
card_rarity('heat shimmer'/'LRW', 'Rare').
card_artist('heat shimmer'/'LRW', 'Franz Vohwinkel').
card_number('heat shimmer'/'LRW', '175').
card_flavor_text('heat shimmer'/'LRW', '\"Better to flare out than to gutter.\"\n—Flamekin expression').
card_multiverse_id('heat shimmer'/'LRW', '139468').

card_in_set('herbal poultice', 'LRW').
card_original_type('herbal poultice'/'LRW', 'Artifact').
card_original_text('herbal poultice'/'LRW', '{3}, Sacrifice Herbal Poultice: Regenerate target creature.').
card_first_print('herbal poultice', 'LRW').
card_image_name('herbal poultice'/'LRW', 'herbal poultice').
card_uid('herbal poultice'/'LRW', 'LRW:Herbal Poultice:herbal poultice').
card_rarity('herbal poultice'/'LRW', 'Common').
card_artist('herbal poultice'/'LRW', 'Scott Hampton').
card_number('herbal poultice'/'LRW', '257').
card_flavor_text('herbal poultice'/'LRW', '\"Apply orange leaf to a wound at dawn to clean it, at dusk to prevent the same injury from happening again.\"\n—Kithkin superstition').
card_multiverse_id('herbal poultice'/'LRW', '139740').

card_in_set('hillcomber giant', 'LRW').
card_original_type('hillcomber giant'/'LRW', 'Creature — Giant Scout').
card_original_text('hillcomber giant'/'LRW', 'Mountainwalk').
card_first_print('hillcomber giant', 'LRW').
card_image_name('hillcomber giant'/'LRW', 'hillcomber giant').
card_uid('hillcomber giant'/'LRW', 'LRW:Hillcomber Giant:hillcomber giant').
card_rarity('hillcomber giant'/'LRW', 'Common').
card_artist('hillcomber giant'/'LRW', 'Ralph Horsley').
card_number('hillcomber giant'/'LRW', '20').
card_flavor_text('hillcomber giant'/'LRW', 'The giants believe the fossils they find in Lorwyn\'s rocky heights are dreams frozen in time, and they treasure them.').
card_multiverse_id('hillcomber giant'/'LRW', '145971').

card_in_set('hoarder\'s greed', 'LRW').
card_original_type('hoarder\'s greed'/'LRW', 'Sorcery').
card_original_text('hoarder\'s greed'/'LRW', 'You lose 2 life and draw two cards, then clash with an opponent. If you win, repeat this process. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_first_print('hoarder\'s greed', 'LRW').
card_image_name('hoarder\'s greed'/'LRW', 'hoarder\'s greed').
card_uid('hoarder\'s greed'/'LRW', 'LRW:Hoarder\'s Greed:hoarder\'s greed').
card_rarity('hoarder\'s greed'/'LRW', 'Uncommon').
card_artist('hoarder\'s greed'/'LRW', 'Pete Venters').
card_number('hoarder\'s greed'/'LRW', '117').
card_multiverse_id('hoarder\'s greed'/'LRW', '146160').

card_in_set('hoofprints of the stag', 'LRW').
card_original_type('hoofprints of the stag'/'LRW', 'Tribal Enchantment — Elemental').
card_original_text('hoofprints of the stag'/'LRW', 'Whenever you draw a card, you may put a hoofprint counter on Hoofprints of the Stag.\n{2}{W}, Remove four hoofprint counters from Hoofprints of the Stag: Put a 4/4 white Elemental creature token with flying into play. Play this ability only during your turn.').
card_first_print('hoofprints of the stag', 'LRW').
card_image_name('hoofprints of the stag'/'LRW', 'hoofprints of the stag').
card_uid('hoofprints of the stag'/'LRW', 'LRW:Hoofprints of the Stag:hoofprints of the stag').
card_rarity('hoofprints of the stag'/'LRW', 'Rare').
card_artist('hoofprints of the stag'/'LRW', 'Anthony S. Waters').
card_number('hoofprints of the stag'/'LRW', '21').
card_multiverse_id('hoofprints of the stag'/'LRW', '143730').

card_in_set('horde of notions', 'LRW').
card_original_type('horde of notions'/'LRW', 'Legendary Creature — Elemental').
card_original_text('horde of notions'/'LRW', 'Vigilance, trample, haste\n{W}{U}{B}{R}{G}: You may play target Elemental card from your graveyard without paying its mana cost.').
card_first_print('horde of notions', 'LRW').
card_image_name('horde of notions'/'LRW', 'horde of notions').
card_uid('horde of notions'/'LRW', 'LRW:Horde of Notions:horde of notions').
card_rarity('horde of notions'/'LRW', 'Rare').
card_artist('horde of notions'/'LRW', 'Adam Rex').
card_number('horde of notions'/'LRW', '249').
card_flavor_text('horde of notions'/'LRW', 'Even the oldest treefolk was but an acorn when Lorwyn\'s first mysteries were born.').
card_multiverse_id('horde of notions'/'LRW', '140219').

card_in_set('hornet harasser', 'LRW').
card_original_type('hornet harasser'/'LRW', 'Creature — Goblin Shaman').
card_original_text('hornet harasser'/'LRW', 'When Hornet Harasser is put into a graveyard from play, target creature gets -2/-2 until end of turn.').
card_first_print('hornet harasser', 'LRW').
card_image_name('hornet harasser'/'LRW', 'hornet harasser').
card_uid('hornet harasser'/'LRW', 'LRW:Hornet Harasser:hornet harasser').
card_rarity('hornet harasser'/'LRW', 'Common').
card_artist('hornet harasser'/'LRW', 'Nils Hamm').
card_number('hornet harasser'/'LRW', '118').
card_flavor_text('hornet harasser'/'LRW', '\"And though she didn\'t get her honey, Auntie found something far more interesting.\"\n—A tale of Auntie Grub').
card_multiverse_id('hornet harasser'/'LRW', '142355').

card_in_set('hostility', 'LRW').
card_original_type('hostility'/'LRW', 'Creature — Elemental Incarnation').
card_original_text('hostility'/'LRW', 'Haste\nIf a spell you control would deal damage to an opponent, prevent that damage. Put a 3/1 red Elemental Shaman creature token with haste into play for each 1 damage prevented this way.\nWhen Hostility is put into a graveyard from anywhere, shuffle it into its owner\'s library.').
card_first_print('hostility', 'LRW').
card_image_name('hostility'/'LRW', 'hostility').
card_uid('hostility'/'LRW', 'LRW:Hostility:hostility').
card_rarity('hostility'/'LRW', 'Rare').
card_artist('hostility'/'LRW', 'Omar Rayyan').
card_number('hostility'/'LRW', '176').
card_multiverse_id('hostility'/'LRW', '140164').

card_in_set('howltooth hollow', 'LRW').
card_original_type('howltooth hollow'/'LRW', 'Land').
card_original_text('howltooth hollow'/'LRW', 'Hideaway (This land comes into play tapped. When it does, look at the top four cards of your library, remove one from the game face down, then put the rest on the bottom of your library.)\n{T}: Add {B} to your mana pool.\n{B}, {T}: You may play the removed card without paying its mana cost if each player has no cards in hand.').
card_first_print('howltooth hollow', 'LRW').
card_image_name('howltooth hollow'/'LRW', 'howltooth hollow').
card_uid('howltooth hollow'/'LRW', 'LRW:Howltooth Hollow:howltooth hollow').
card_rarity('howltooth hollow'/'LRW', 'Rare').
card_artist('howltooth hollow'/'LRW', 'John Howe').
card_number('howltooth hollow'/'LRW', '269').
card_multiverse_id('howltooth hollow'/'LRW', '139513').

card_in_set('hunt down', 'LRW').
card_original_type('hunt down'/'LRW', 'Sorcery').
card_original_text('hunt down'/'LRW', 'Target creature blocks target creature this turn if able.').
card_first_print('hunt down', 'LRW').
card_image_name('hunt down'/'LRW', 'hunt down').
card_uid('hunt down'/'LRW', 'LRW:Hunt Down:hunt down').
card_rarity('hunt down'/'LRW', 'Common').
card_artist('hunt down'/'LRW', 'Christopher Moeller').
card_number('hunt down'/'LRW', '218').
card_flavor_text('hunt down'/'LRW', '\"Springjacks and faeries can be difficult to hunt, but my favorite prey are the flamekin. They never fail to put up a worthy fight when cornered.\"').
card_multiverse_id('hunt down'/'LRW', '143611').

card_in_set('hunter of eyeblights', 'LRW').
card_original_type('hunter of eyeblights'/'LRW', 'Creature — Elf Assassin').
card_original_text('hunter of eyeblights'/'LRW', 'When Hunter of Eyeblights comes into play, put a +1/+1 counter on target creature you don\'t control.\n{2}{B}, {T}: Destroy target creature with a counter on it.').
card_first_print('hunter of eyeblights', 'LRW').
card_image_name('hunter of eyeblights'/'LRW', 'hunter of eyeblights').
card_uid('hunter of eyeblights'/'LRW', 'LRW:Hunter of Eyeblights:hunter of eyeblights').
card_rarity('hunter of eyeblights'/'LRW', 'Uncommon').
card_artist('hunter of eyeblights'/'LRW', 'Jesper Ejsing').
card_number('hunter of eyeblights'/'LRW', '119').
card_flavor_text('hunter of eyeblights'/'LRW', 'Snokk ran as fast as he could, but the sound of hooves grew ever louder in his ears.').
card_multiverse_id('hunter of eyeblights'/'LRW', '139723').

card_in_set('hurly-burly', 'LRW').
card_original_type('hurly-burly'/'LRW', 'Sorcery').
card_original_text('hurly-burly'/'LRW', 'Choose one Hurly-Burly deals 1 damage to each creature without flying; or Hurly-Burly deals 1 damage to each creature with flying.').
card_first_print('hurly-burly', 'LRW').
card_image_name('hurly-burly'/'LRW', 'hurly-burly').
card_uid('hurly-burly'/'LRW', 'LRW:Hurly-Burly:hurly-burly').
card_rarity('hurly-burly'/'LRW', 'Common').
card_artist('hurly-burly'/'LRW', 'Steve Prescott').
card_number('hurly-burly'/'LRW', '177').
card_flavor_text('hurly-burly'/'LRW', '\"Things were popping like corn in a skillet. Olly landed in the pig pen with his prize sow on top of him, both squealin\' like boggarts.\"\n—Deagan, cenn of Burrenton').
card_multiverse_id('hurly-burly'/'LRW', '143381').

card_in_set('immaculate magistrate', 'LRW').
card_original_type('immaculate magistrate'/'LRW', 'Creature — Elf Shaman').
card_original_text('immaculate magistrate'/'LRW', '{T}: Put a +1/+1 counter on target creature for each Elf you control.').
card_first_print('immaculate magistrate', 'LRW').
card_image_name('immaculate magistrate'/'LRW', 'immaculate magistrate').
card_uid('immaculate magistrate'/'LRW', 'LRW:Immaculate Magistrate:immaculate magistrate').
card_rarity('immaculate magistrate'/'LRW', 'Rare').
card_artist('immaculate magistrate'/'LRW', 'Jim Nelson').
card_number('immaculate magistrate'/'LRW', '219').
card_flavor_text('immaculate magistrate'/'LRW', 'Elves of the immaculate class weave flora into living creatures—sometimes to endorse an elite warrior, sometimes to create a breathing work of art.').
card_multiverse_id('immaculate magistrate'/'LRW', '139721').

card_in_set('imperious perfect', 'LRW').
card_original_type('imperious perfect'/'LRW', 'Creature — Elf Warrior').
card_original_text('imperious perfect'/'LRW', 'Other Elf creatures you control get +1/+1.\n{G}, {T}: Put a 1/1 green Elf Warrior creature token into play.').
card_image_name('imperious perfect'/'LRW', 'imperious perfect').
card_uid('imperious perfect'/'LRW', 'LRW:Imperious Perfect:imperious perfect').
card_rarity('imperious perfect'/'LRW', 'Uncommon').
card_artist('imperious perfect'/'LRW', 'Scott M. Fischer').
card_number('imperious perfect'/'LRW', '220').
card_flavor_text('imperious perfect'/'LRW', 'In a culture of beauty, the most beautiful are worshipped as gods.').
card_multiverse_id('imperious perfect'/'LRW', '139683').

card_in_set('incandescent soulstoke', 'LRW').
card_original_type('incandescent soulstoke'/'LRW', 'Creature — Elemental Shaman').
card_original_text('incandescent soulstoke'/'LRW', 'Other Elemental creatures you control get +1/+1.\n{1}{R}, {T}: You may put an Elemental creature card from your hand into play. That creature gains haste until end of turn. Sacrifice it at end of turn.').
card_first_print('incandescent soulstoke', 'LRW').
card_image_name('incandescent soulstoke'/'LRW', 'incandescent soulstoke').
card_uid('incandescent soulstoke'/'LRW', 'LRW:Incandescent Soulstoke:incandescent soulstoke').
card_rarity('incandescent soulstoke'/'LRW', 'Rare').
card_artist('incandescent soulstoke'/'LRW', 'Todd Lockwood').
card_number('incandescent soulstoke'/'LRW', '178').
card_multiverse_id('incandescent soulstoke'/'LRW', '139730').

card_in_set('incendiary command', 'LRW').
card_original_type('incendiary command'/'LRW', 'Sorcery').
card_original_text('incendiary command'/'LRW', 'Choose two Incendiary Command deals 4 damage to target player; or Incendiary Command deals 2 damage to each creature; or destroy target nonbasic land; or each player discards all the cards in his or her hand, then draws that many cards.').
card_first_print('incendiary command', 'LRW').
card_image_name('incendiary command'/'LRW', 'incendiary command').
card_uid('incendiary command'/'LRW', 'LRW:Incendiary Command:incendiary command').
card_rarity('incendiary command'/'LRW', 'Rare').
card_artist('incendiary command'/'LRW', 'Wayne England').
card_number('incendiary command'/'LRW', '179').
card_multiverse_id('incendiary command'/'LRW', '141830').

card_in_set('incremental growth', 'LRW').
card_original_type('incremental growth'/'LRW', 'Sorcery').
card_original_text('incremental growth'/'LRW', 'Put a +1/+1 counter on target creature, two +1/+1 counters on another target creature, and three +1/+1 counters on a third target creature.').
card_first_print('incremental growth', 'LRW').
card_image_name('incremental growth'/'LRW', 'incremental growth').
card_uid('incremental growth'/'LRW', 'LRW:Incremental Growth:incremental growth').
card_rarity('incremental growth'/'LRW', 'Uncommon').
card_artist('incremental growth'/'LRW', 'Chuck Lukacs').
card_number('incremental growth'/'LRW', '221').
card_flavor_text('incremental growth'/'LRW', '\"Tie a mouse under a bridge, and your cabbages will triple in size.\"\n—Kithkin superstition').
card_multiverse_id('incremental growth'/'LRW', '146777').

card_in_set('ingot chewer', 'LRW').
card_original_type('ingot chewer'/'LRW', 'Creature — Elemental').
card_original_text('ingot chewer'/'LRW', 'When Ingot Chewer comes into play, destroy target artifact.\nEvoke {R} (You may play this spell for its evoke cost. If you do, it\'s sacrificed when it comes into play.)').
card_first_print('ingot chewer', 'LRW').
card_image_name('ingot chewer'/'LRW', 'ingot chewer').
card_uid('ingot chewer'/'LRW', 'LRW:Ingot Chewer:ingot chewer').
card_rarity('ingot chewer'/'LRW', 'Common').
card_artist('ingot chewer'/'LRW', 'Kev Walker').
card_number('ingot chewer'/'LRW', '180').
card_flavor_text('ingot chewer'/'LRW', 'Elementals are ideas given form. This one is the idea of \"smashitude.\"').
card_multiverse_id('ingot chewer'/'LRW', '139686').

card_in_set('inkfathom divers', 'LRW').
card_original_type('inkfathom divers'/'LRW', 'Creature — Merfolk Soldier').
card_original_text('inkfathom divers'/'LRW', 'Islandwalk\nWhen Inkfathom Divers comes into play, look at the top four cards of your library, then put them back in any order.').
card_first_print('inkfathom divers', 'LRW').
card_image_name('inkfathom divers'/'LRW', 'inkfathom divers').
card_uid('inkfathom divers'/'LRW', 'LRW:Inkfathom Divers:inkfathom divers').
card_rarity('inkfathom divers'/'LRW', 'Common').
card_artist('inkfathom divers'/'LRW', 'Steven Belledin').
card_number('inkfathom divers'/'LRW', '70').
card_flavor_text('inkfathom divers'/'LRW', '\"None appreciate sun and shallows like those who have seen the depths.\"\n—Lianda of the Stonybrook school').
card_multiverse_id('inkfathom divers'/'LRW', '139419').

card_in_set('inner-flame acolyte', 'LRW').
card_original_type('inner-flame acolyte'/'LRW', 'Creature — Elemental Shaman').
card_original_text('inner-flame acolyte'/'LRW', 'When Inner-Flame Acolyte comes into play, target creature gets +2/+0 and gains haste until end of turn.\nEvoke {R} (You may play this spell for its evoke cost. If you do, it\'s sacrificed when it comes into play.)').
card_first_print('inner-flame acolyte', 'LRW').
card_image_name('inner-flame acolyte'/'LRW', 'inner-flame acolyte').
card_uid('inner-flame acolyte'/'LRW', 'LRW:Inner-Flame Acolyte:inner-flame acolyte').
card_rarity('inner-flame acolyte'/'LRW', 'Common').
card_artist('inner-flame acolyte'/'LRW', 'Ron Spears').
card_number('inner-flame acolyte'/'LRW', '181').
card_multiverse_id('inner-flame acolyte'/'LRW', '146599').

card_in_set('inner-flame igniter', 'LRW').
card_original_type('inner-flame igniter'/'LRW', 'Creature — Elemental Warrior').
card_original_text('inner-flame igniter'/'LRW', '{2}{R}: Creatures you control get +1/+0 until end of turn. If this is the third time this ability has resolved this turn, creatures you control gain first strike until end of turn.').
card_first_print('inner-flame igniter', 'LRW').
card_image_name('inner-flame igniter'/'LRW', 'inner-flame igniter').
card_uid('inner-flame igniter'/'LRW', 'LRW:Inner-Flame Igniter:inner-flame igniter').
card_rarity('inner-flame igniter'/'LRW', 'Uncommon').
card_artist('inner-flame igniter'/'LRW', 'Scott Hampton').
card_number('inner-flame igniter'/'LRW', '182').
card_flavor_text('inner-flame igniter'/'LRW', 'A light an army can follow.').
card_multiverse_id('inner-flame igniter'/'LRW', '139462').

card_in_set('island', 'LRW').
card_original_type('island'/'LRW', 'Basic Land — Island').
card_original_text('island'/'LRW', 'U').
card_image_name('island'/'LRW', 'island1').
card_uid('island'/'LRW', 'LRW:Island:island1').
card_rarity('island'/'LRW', 'Basic Land').
card_artist('island'/'LRW', 'Warren Mahy').
card_number('island'/'LRW', '286').
card_multiverse_id('island'/'LRW', '143628').

card_in_set('island', 'LRW').
card_original_type('island'/'LRW', 'Basic Land — Island').
card_original_text('island'/'LRW', 'U').
card_image_name('island'/'LRW', 'island2').
card_uid('island'/'LRW', 'LRW:Island:island2').
card_rarity('island'/'LRW', 'Basic Land').
card_artist('island'/'LRW', 'Ron Spears').
card_number('island'/'LRW', '287').
card_multiverse_id('island'/'LRW', '143624').

card_in_set('island', 'LRW').
card_original_type('island'/'LRW', 'Basic Land — Island').
card_original_text('island'/'LRW', 'U').
card_image_name('island'/'LRW', 'island3').
card_uid('island'/'LRW', 'LRW:Island:island3').
card_rarity('island'/'LRW', 'Basic Land').
card_artist('island'/'LRW', 'Martina Pilcerova').
card_number('island'/'LRW', '288').
card_multiverse_id('island'/'LRW', '143632').

card_in_set('island', 'LRW').
card_original_type('island'/'LRW', 'Basic Land — Island').
card_original_text('island'/'LRW', 'U').
card_image_name('island'/'LRW', 'island4').
card_uid('island'/'LRW', 'LRW:Island:island4').
card_rarity('island'/'LRW', 'Basic Land').
card_artist('island'/'LRW', 'Rob Alexander').
card_number('island'/'LRW', '289').
card_multiverse_id('island'/'LRW', '143619').

card_in_set('jace beleren', 'LRW').
card_original_type('jace beleren'/'LRW', 'Planeswalker — Jace').
card_original_text('jace beleren'/'LRW', '+2: Each player draws a card.\n-1: Target player draws a card.\n-10: Target player puts the top twenty cards of his or her library into his or her graveyard.').
card_image_name('jace beleren'/'LRW', 'jace beleren').
card_uid('jace beleren'/'LRW', 'LRW:Jace Beleren:jace beleren').
card_rarity('jace beleren'/'LRW', 'Rare').
card_artist('jace beleren'/'LRW', 'Aleksi Briclot').
card_number('jace beleren'/'LRW', '71').
card_multiverse_id('jace beleren'/'LRW', '140222').

card_in_set('jagged-scar archers', 'LRW').
card_original_type('jagged-scar archers'/'LRW', 'Creature — Elf Archer').
card_original_text('jagged-scar archers'/'LRW', 'Jagged-Scar Archers\'s power and toughness are each equal to the number of Elves you control.\n{T}: Jagged-Scar Archers deals damage equal to its power to target creature with flying.').
card_first_print('jagged-scar archers', 'LRW').
card_image_name('jagged-scar archers'/'LRW', 'jagged-scar archers').
card_uid('jagged-scar archers'/'LRW', 'LRW:Jagged-Scar Archers:jagged-scar archers').
card_rarity('jagged-scar archers'/'LRW', 'Uncommon').
card_artist('jagged-scar archers'/'LRW', 'Paolo Parente').
card_number('jagged-scar archers'/'LRW', '222').
card_multiverse_id('jagged-scar archers'/'LRW', '139726').

card_in_set('judge of currents', 'LRW').
card_original_type('judge of currents'/'LRW', 'Creature — Merfolk Wizard').
card_original_text('judge of currents'/'LRW', 'Whenever a Merfolk you control becomes tapped, you may gain 1 life.').
card_first_print('judge of currents', 'LRW').
card_image_name('judge of currents'/'LRW', 'judge of currents').
card_uid('judge of currents'/'LRW', 'LRW:Judge of Currents:judge of currents').
card_rarity('judge of currents'/'LRW', 'Common').
card_artist('judge of currents'/'LRW', 'Dan Scott').
card_number('judge of currents'/'LRW', '22').
card_flavor_text('judge of currents'/'LRW', 'Though the currents of the Lanes shift every year, the merrow never lose track of where they are or where they are going.').
card_multiverse_id('judge of currents'/'LRW', '139406').

card_in_set('kinsbaile balloonist', 'LRW').
card_original_type('kinsbaile balloonist'/'LRW', 'Creature — Kithkin Soldier').
card_original_text('kinsbaile balloonist'/'LRW', 'Flying\nWhenever Kinsbaile Balloonist attacks, you may have target creature gain flying until end of turn.').
card_first_print('kinsbaile balloonist', 'LRW').
card_image_name('kinsbaile balloonist'/'LRW', 'kinsbaile balloonist').
card_uid('kinsbaile balloonist'/'LRW', 'LRW:Kinsbaile Balloonist:kinsbaile balloonist').
card_rarity('kinsbaile balloonist'/'LRW', 'Common').
card_artist('kinsbaile balloonist'/'LRW', 'Zoltan Boros & Gabor Szikszai').
card_number('kinsbaile balloonist'/'LRW', '23').
card_flavor_text('kinsbaile balloonist'/'LRW', 'Even when a giant\'s tantrum turns the sky into a chaotic gale, the path of the balloonist never falters.').
card_multiverse_id('kinsbaile balloonist'/'LRW', '139403').

card_in_set('kinsbaile skirmisher', 'LRW').
card_original_type('kinsbaile skirmisher'/'LRW', 'Creature — Kithkin Soldier').
card_original_text('kinsbaile skirmisher'/'LRW', 'When Kinsbaile Skirmisher comes into play, target creature gets +1/+1 until end of turn.').
card_first_print('kinsbaile skirmisher', 'LRW').
card_image_name('kinsbaile skirmisher'/'LRW', 'kinsbaile skirmisher').
card_uid('kinsbaile skirmisher'/'LRW', 'LRW:Kinsbaile Skirmisher:kinsbaile skirmisher').
card_rarity('kinsbaile skirmisher'/'LRW', 'Common').
card_artist('kinsbaile skirmisher'/'LRW', 'Thomas Denmark').
card_number('kinsbaile skirmisher'/'LRW', '24').
card_flavor_text('kinsbaile skirmisher'/'LRW', '\"If a boggart even dares breathe near one of my kin, I\'ll know. And I\'ll not be happy.\"').
card_multiverse_id('kinsbaile skirmisher'/'LRW', '143018').

card_in_set('kithkin daggerdare', 'LRW').
card_original_type('kithkin daggerdare'/'LRW', 'Creature — Kithkin Soldier').
card_original_text('kithkin daggerdare'/'LRW', '{G}, {T}: Target attacking creature gets +2/+2 until end of turn.').
card_first_print('kithkin daggerdare', 'LRW').
card_image_name('kithkin daggerdare'/'LRW', 'kithkin daggerdare').
card_uid('kithkin daggerdare'/'LRW', 'LRW:Kithkin Daggerdare:kithkin daggerdare').
card_rarity('kithkin daggerdare'/'LRW', 'Common').
card_artist('kithkin daggerdare'/'LRW', 'Christopher Moeller').
card_number('kithkin daggerdare'/'LRW', '223').
card_flavor_text('kithkin daggerdare'/'LRW', 'The kith dance their elaborate reels not merely to celebrate the events of their lives but to form an unbreakable bond of loyalty with their kin, a bond stronger than the fear of death itself.').
card_multiverse_id('kithkin daggerdare'/'LRW', '139498').

card_in_set('kithkin greatheart', 'LRW').
card_original_type('kithkin greatheart'/'LRW', 'Creature — Kithkin Soldier').
card_original_text('kithkin greatheart'/'LRW', 'As long as you control a Giant, Kithkin Greatheart gets +1/+1 and has first strike.').
card_first_print('kithkin greatheart', 'LRW').
card_image_name('kithkin greatheart'/'LRW', 'kithkin greatheart').
card_uid('kithkin greatheart'/'LRW', 'LRW:Kithkin Greatheart:kithkin greatheart').
card_rarity('kithkin greatheart'/'LRW', 'Common').
card_artist('kithkin greatheart'/'LRW', 'Greg Staples').
card_number('kithkin greatheart'/'LRW', '25').
card_flavor_text('kithkin greatheart'/'LRW', 'Sometimes a curious giant singles out a \"little one\" to follow for a few days, never realizing the effect it will have on the little one\'s life.').
card_multiverse_id('kithkin greatheart'/'LRW', '146444').

card_in_set('kithkin harbinger', 'LRW').
card_original_type('kithkin harbinger'/'LRW', 'Creature — Kithkin Wizard').
card_original_text('kithkin harbinger'/'LRW', 'When Kithkin Harbinger comes into play, you may search your library for a Kithkin card, reveal it, then shuffle your library and put that card on top of it.').
card_first_print('kithkin harbinger', 'LRW').
card_image_name('kithkin harbinger'/'LRW', 'kithkin harbinger').
card_uid('kithkin harbinger'/'LRW', 'LRW:Kithkin Harbinger:kithkin harbinger').
card_rarity('kithkin harbinger'/'LRW', 'Uncommon').
card_artist('kithkin harbinger'/'LRW', 'Steve Prescott').
card_number('kithkin harbinger'/'LRW', '26').
card_flavor_text('kithkin harbinger'/'LRW', 'Her ears are open to even the softest voice.').
card_multiverse_id('kithkin harbinger'/'LRW', '139399').

card_in_set('kithkin healer', 'LRW').
card_original_type('kithkin healer'/'LRW', 'Creature — Kithkin Cleric').
card_original_text('kithkin healer'/'LRW', '{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.').
card_first_print('kithkin healer', 'LRW').
card_image_name('kithkin healer'/'LRW', 'kithkin healer').
card_uid('kithkin healer'/'LRW', 'LRW:Kithkin Healer:kithkin healer').
card_rarity('kithkin healer'/'LRW', 'Common').
card_artist('kithkin healer'/'LRW', 'Rebecca Guay').
card_number('kithkin healer'/'LRW', '27').
card_flavor_text('kithkin healer'/'LRW', 'The empathetic nature of the thoughtweft allows kithkin healers to treat the cause of an illness rather than fight its symptoms.').
card_multiverse_id('kithkin healer'/'LRW', '145986').

card_in_set('kithkin mourncaller', 'LRW').
card_original_type('kithkin mourncaller'/'LRW', 'Creature — Kithkin Scout').
card_original_text('kithkin mourncaller'/'LRW', 'Whenever an attacking Kithkin or Elf is put into your graveyard from play, you may draw a card.').
card_first_print('kithkin mourncaller', 'LRW').
card_image_name('kithkin mourncaller'/'LRW', 'kithkin mourncaller').
card_uid('kithkin mourncaller'/'LRW', 'LRW:Kithkin Mourncaller:kithkin mourncaller').
card_rarity('kithkin mourncaller'/'LRW', 'Uncommon').
card_artist('kithkin mourncaller'/'LRW', 'Dominick Domingo').
card_number('kithkin mourncaller'/'LRW', '224').
card_flavor_text('kithkin mourncaller'/'LRW', 'Eidren\'s hunts are dangerous affairs. All dread the inevitable recounting of those who died while flushing out his prey.').
card_multiverse_id('kithkin mourncaller'/'LRW', '139497').

card_in_set('knight of meadowgrain', 'LRW').
card_original_type('knight of meadowgrain'/'LRW', 'Creature — Kithkin Knight').
card_original_text('knight of meadowgrain'/'LRW', 'First strike\nLifelink (Whenever this creature deals damage, you gain that much life.)').
card_first_print('knight of meadowgrain', 'LRW').
card_image_name('knight of meadowgrain'/'LRW', 'knight of meadowgrain').
card_uid('knight of meadowgrain'/'LRW', 'LRW:Knight of Meadowgrain:knight of meadowgrain').
card_rarity('knight of meadowgrain'/'LRW', 'Uncommon').
card_artist('knight of meadowgrain'/'LRW', 'Larry MacDougall').
card_number('knight of meadowgrain'/'LRW', '28').
card_flavor_text('knight of meadowgrain'/'LRW', '\"By tradition, we don\'t speak for two days after battle. If our deeds won\'t speak for themselves, what else could be said?\"').
card_multiverse_id('knight of meadowgrain'/'LRW', '139715').

card_in_set('knucklebone witch', 'LRW').
card_original_type('knucklebone witch'/'LRW', 'Creature — Goblin Shaman').
card_original_text('knucklebone witch'/'LRW', 'Whenever a Goblin you control is put into a graveyard from play, you may put a +1/+1 counter on Knucklebone Witch.').
card_first_print('knucklebone witch', 'LRW').
card_image_name('knucklebone witch'/'LRW', 'knucklebone witch').
card_uid('knucklebone witch'/'LRW', 'LRW:Knucklebone Witch:knucklebone witch').
card_rarity('knucklebone witch'/'LRW', 'Rare').
card_artist('knucklebone witch'/'LRW', 'Jim Pavelec').
card_number('knucklebone witch'/'LRW', '120').
card_flavor_text('knucklebone witch'/'LRW', 'Each bone honors its owner\'s best pranks.').
card_multiverse_id('knucklebone witch'/'LRW', '143609').

card_in_set('lace with moonglove', 'LRW').
card_original_type('lace with moonglove'/'LRW', 'Instant').
card_original_text('lace with moonglove'/'LRW', 'Target creature gains deathtouch until end of turn. (Whenever it deals damage to a creature, destroy that creature.)\nDraw a card.').
card_first_print('lace with moonglove', 'LRW').
card_image_name('lace with moonglove'/'LRW', 'lace with moonglove').
card_uid('lace with moonglove'/'LRW', 'LRW:Lace with Moonglove:lace with moonglove').
card_rarity('lace with moonglove'/'LRW', 'Common').
card_artist('lace with moonglove'/'LRW', 'Rebecca Guay').
card_number('lace with moonglove'/'LRW', '225').
card_flavor_text('lace with moonglove'/'LRW', '\"Which is more filled with poison: the flower of the moonglove or the minds of elves?\"\n—Vessifrus, flamekin demagogue').
card_multiverse_id('lace with moonglove'/'LRW', '143376').

card_in_set('lairwatch giant', 'LRW').
card_original_type('lairwatch giant'/'LRW', 'Creature — Giant Warrior').
card_original_text('lairwatch giant'/'LRW', 'Lairwatch Giant can block an additional creature.\nWhenever Lairwatch Giant blocks two or more creatures, it gains first strike until end of turn.').
card_first_print('lairwatch giant', 'LRW').
card_image_name('lairwatch giant'/'LRW', 'lairwatch giant').
card_uid('lairwatch giant'/'LRW', 'LRW:Lairwatch Giant:lairwatch giant').
card_rarity('lairwatch giant'/'LRW', 'Common').
card_artist('lairwatch giant'/'LRW', 'Warren Mahy').
card_number('lairwatch giant'/'LRW', '29').
card_flavor_text('lairwatch giant'/'LRW', 'A giant can brood over a grudge for decades, and woe to those who interrupt him.').
card_multiverse_id('lairwatch giant'/'LRW', '143383').

card_in_set('lammastide weave', 'LRW').
card_original_type('lammastide weave'/'LRW', 'Instant').
card_original_text('lammastide weave'/'LRW', 'Name a card, then target player puts the top card of his or her library into his or her graveyard. If that card is the named card, you gain life equal to its converted mana cost.\nDraw a card.').
card_first_print('lammastide weave', 'LRW').
card_image_name('lammastide weave'/'LRW', 'lammastide weave').
card_uid('lammastide weave'/'LRW', 'LRW:Lammastide Weave:lammastide weave').
card_rarity('lammastide weave'/'LRW', 'Uncommon').
card_artist('lammastide weave'/'LRW', 'Howard Lyon').
card_number('lammastide weave'/'LRW', '226').
card_flavor_text('lammastide weave'/'LRW', '\"A ribbon torn will ward away dark dreams.\"').
card_multiverse_id('lammastide weave'/'LRW', '139404').

card_in_set('lash out', 'LRW').
card_original_type('lash out'/'LRW', 'Instant').
card_original_text('lash out'/'LRW', 'Lash Out deals 3 damage to target creature. Clash with an opponent. If you win, Lash Out deals 3 damage to that creature\'s controller. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_first_print('lash out', 'LRW').
card_image_name('lash out'/'LRW', 'lash out').
card_uid('lash out'/'LRW', 'LRW:Lash Out:lash out').
card_rarity('lash out'/'LRW', 'Common').
card_artist('lash out'/'LRW', 'Scott Hampton').
card_number('lash out'/'LRW', '183').
card_multiverse_id('lash out'/'LRW', '145987').

card_in_set('leaf gilder', 'LRW').
card_original_type('leaf gilder'/'LRW', 'Creature — Elf Druid').
card_original_text('leaf gilder'/'LRW', '{T}: Add {G} to your mana pool.').
card_first_print('leaf gilder', 'LRW').
card_image_name('leaf gilder'/'LRW', 'leaf gilder').
card_uid('leaf gilder'/'LRW', 'LRW:Leaf Gilder:leaf gilder').
card_rarity('leaf gilder'/'LRW', 'Common').
card_artist('leaf gilder'/'LRW', 'Quinton Hoover').
card_number('leaf gilder'/'LRW', '227').
card_flavor_text('leaf gilder'/'LRW', 'Eidren, perfect of Lys Alana, ordered hundreds of trees uprooted and rearranged into a pattern he deemed beautiful. Thus the Gilt-Leaf Wood was born.').
card_multiverse_id('leaf gilder'/'LRW', '139487').

card_in_set('lignify', 'LRW').
card_original_type('lignify'/'LRW', 'Tribal Enchantment — Treefolk Aura').
card_original_text('lignify'/'LRW', 'Enchant creature\nEnchanted creature is a 0/4 Treefolk with no abilities.').
card_first_print('lignify', 'LRW').
card_image_name('lignify'/'LRW', 'lignify').
card_uid('lignify'/'LRW', 'LRW:Lignify:lignify').
card_rarity('lignify'/'LRW', 'Common').
card_artist('lignify'/'LRW', 'Jesper Ejsing').
card_number('lignify'/'LRW', '228').
card_flavor_text('lignify'/'LRW', 'Bulgo paused, puzzled. What was that rustling sound, and why did he feel so stiff? And how could his feet be so thirsty?').
card_multiverse_id('lignify'/'LRW', '139703').

card_in_set('liliana vess', 'LRW').
card_original_type('liliana vess'/'LRW', 'Planeswalker — Liliana').
card_original_text('liliana vess'/'LRW', '+1: Target player discards a card.\n-2: Search your library for a card, then shuffle your library and put that card on top of it.\n-8: Put all creature cards in all graveyards into play under your control.').
card_image_name('liliana vess'/'LRW', 'liliana vess').
card_uid('liliana vess'/'LRW', 'LRW:Liliana Vess:liliana vess').
card_rarity('liliana vess'/'LRW', 'Rare').
card_artist('liliana vess'/'LRW', 'Aleksi Briclot').
card_number('liliana vess'/'LRW', '121').
card_multiverse_id('liliana vess'/'LRW', '140212').

card_in_set('lowland oaf', 'LRW').
card_original_type('lowland oaf'/'LRW', 'Creature — Giant Warrior').
card_original_text('lowland oaf'/'LRW', '{T}: Target Goblin creature you control gets +1/+0 and gains flying until end of turn. Sacrifice that creature at end of turn.').
card_first_print('lowland oaf', 'LRW').
card_image_name('lowland oaf'/'LRW', 'lowland oaf').
card_uid('lowland oaf'/'LRW', 'LRW:Lowland Oaf:lowland oaf').
card_rarity('lowland oaf'/'LRW', 'Common').
card_artist('lowland oaf'/'LRW', 'Jeff Easley').
card_number('lowland oaf'/'LRW', '184').
card_flavor_text('lowland oaf'/'LRW', '\"I don\'t know why the little one was so mad. He said to put him down, and I put him down.\"').
card_multiverse_id('lowland oaf'/'LRW', '140183').

card_in_set('lys alana huntmaster', 'LRW').
card_original_type('lys alana huntmaster'/'LRW', 'Creature — Elf Warrior').
card_original_text('lys alana huntmaster'/'LRW', 'Whenever you play an Elf spell, you may put a 1/1 green Elf Warrior creature token into play.').
card_first_print('lys alana huntmaster', 'LRW').
card_image_name('lys alana huntmaster'/'LRW', 'lys alana huntmaster').
card_uid('lys alana huntmaster'/'LRW', 'LRW:Lys Alana Huntmaster:lys alana huntmaster').
card_rarity('lys alana huntmaster'/'LRW', 'Common').
card_artist('lys alana huntmaster'/'LRW', 'Pete Venters').
card_number('lys alana huntmaster'/'LRW', '229').
card_flavor_text('lys alana huntmaster'/'LRW', 'From the highest tiers of Dawn\'s Light Palace to the deepest shade of Wren\'s Run, the silver notes of the horn shimmer through the air, and all who hear it feel its pull.').
card_multiverse_id('lys alana huntmaster'/'LRW', '139401').

card_in_set('lys alana scarblade', 'LRW').
card_original_type('lys alana scarblade'/'LRW', 'Creature — Elf Assassin').
card_original_text('lys alana scarblade'/'LRW', '{T}, Discard an Elf card: Target creature gets -X/-X until end of turn, where X is the number of Elves you control.').
card_first_print('lys alana scarblade', 'LRW').
card_image_name('lys alana scarblade'/'LRW', 'lys alana scarblade').
card_uid('lys alana scarblade'/'LRW', 'LRW:Lys Alana Scarblade:lys alana scarblade').
card_rarity('lys alana scarblade'/'LRW', 'Uncommon').
card_artist('lys alana scarblade'/'LRW', 'Christopher Moeller').
card_number('lys alana scarblade'/'LRW', '122').
card_flavor_text('lys alana scarblade'/'LRW', 'In beauty-obsessed Lys Alana, one cut of her blade means the difference between a high society feast and raking through the dungheap for scraps.').
card_multiverse_id('lys alana scarblade'/'LRW', '143021').

card_in_set('mad auntie', 'LRW').
card_original_type('mad auntie'/'LRW', 'Creature — Goblin Shaman').
card_original_text('mad auntie'/'LRW', 'Other Goblin creatures you control get +1/+1.\n{T}: Regenerate another target Goblin.').
card_image_name('mad auntie'/'LRW', 'mad auntie').
card_uid('mad auntie'/'LRW', 'LRW:Mad Auntie:mad auntie').
card_rarity('mad auntie'/'LRW', 'Rare').
card_artist('mad auntie'/'LRW', 'Wayne Reynolds').
card_number('mad auntie'/'LRW', '123').
card_flavor_text('mad auntie'/'LRW', 'One part cunning, one part wise, and many, many parts demented.').
card_multiverse_id('mad auntie'/'LRW', '139708').

card_in_set('makeshift mannequin', 'LRW').
card_original_type('makeshift mannequin'/'LRW', 'Instant').
card_original_text('makeshift mannequin'/'LRW', 'Return target creature card from your graveyard to play with a mannequin counter on it. As long as that creature has a mannequin counter on it, it has \"When this creature becomes the target of a spell or ability, sacrifice it.\"').
card_first_print('makeshift mannequin', 'LRW').
card_image_name('makeshift mannequin'/'LRW', 'makeshift mannequin').
card_uid('makeshift mannequin'/'LRW', 'LRW:Makeshift Mannequin:makeshift mannequin').
card_rarity('makeshift mannequin'/'LRW', 'Uncommon').
card_artist('makeshift mannequin'/'LRW', 'Darrell Riche').
card_number('makeshift mannequin'/'LRW', '124').
card_flavor_text('makeshift mannequin'/'LRW', '\"This vulgar mimicry will end now.\"\n—Desmera, perfect of Wren\'s Run').
card_multiverse_id('makeshift mannequin'/'LRW', '139670').

card_in_set('marsh flitter', 'LRW').
card_original_type('marsh flitter'/'LRW', 'Creature — Faerie Rogue').
card_original_text('marsh flitter'/'LRW', 'Flying\nWhen Marsh Flitter comes into play, put two 1/1 black Goblin Rogue creature tokens into play.\nSacrifice a Goblin: Marsh Flitter becomes 3/3 until end of turn.').
card_first_print('marsh flitter', 'LRW').
card_image_name('marsh flitter'/'LRW', 'marsh flitter').
card_uid('marsh flitter'/'LRW', 'LRW:Marsh Flitter:marsh flitter').
card_rarity('marsh flitter'/'LRW', 'Uncommon').
card_artist('marsh flitter'/'LRW', 'Wayne Reynolds').
card_number('marsh flitter'/'LRW', '125').
card_multiverse_id('marsh flitter'/'LRW', '139705').

card_in_set('masked admirers', 'LRW').
card_original_type('masked admirers'/'LRW', 'Creature — Elf Shaman').
card_original_text('masked admirers'/'LRW', 'When Masked Admirers comes into play, draw a card.\nWhenever you play a creature spell, you may pay {G}{G}. If you do, return Masked Admirers from your graveyard to your hand.').
card_first_print('masked admirers', 'LRW').
card_image_name('masked admirers'/'LRW', 'masked admirers').
card_uid('masked admirers'/'LRW', 'LRW:Masked Admirers:masked admirers').
card_rarity('masked admirers'/'LRW', 'Rare').
card_artist('masked admirers'/'LRW', 'Eric Fortune').
card_number('masked admirers'/'LRW', '230').
card_flavor_text('masked admirers'/'LRW', '\"Beauty determines value, and we determine beauty.\"').
card_multiverse_id('masked admirers'/'LRW', '140184').

card_in_set('merrow commerce', 'LRW').
card_original_type('merrow commerce'/'LRW', 'Tribal Enchantment — Merfolk').
card_original_text('merrow commerce'/'LRW', 'At the end of your turn, untap all Merfolk you control.').
card_first_print('merrow commerce', 'LRW').
card_image_name('merrow commerce'/'LRW', 'merrow commerce').
card_uid('merrow commerce'/'LRW', 'LRW:Merrow Commerce:merrow commerce').
card_rarity('merrow commerce'/'LRW', 'Uncommon').
card_artist('merrow commerce'/'LRW', 'Steve Ellis').
card_number('merrow commerce'/'LRW', '72').
card_flavor_text('merrow commerce'/'LRW', 'Schools meet and mingle on Lorwyn\'s riverways. In the bustling interplay, the merrow renew their sense of community as they sharpen their wits and hone their trading skills.').
card_multiverse_id('merrow commerce'/'LRW', '139663').

card_in_set('merrow harbinger', 'LRW').
card_original_type('merrow harbinger'/'LRW', 'Creature — Merfolk Wizard').
card_original_text('merrow harbinger'/'LRW', 'Islandwalk\nWhen Merrow Harbinger comes into play, you may search your library for a Merfolk card, reveal it, then shuffle your library and put that card on top of it.').
card_first_print('merrow harbinger', 'LRW').
card_image_name('merrow harbinger'/'LRW', 'merrow harbinger').
card_uid('merrow harbinger'/'LRW', 'LRW:Merrow Harbinger:merrow harbinger').
card_rarity('merrow harbinger'/'LRW', 'Uncommon').
card_artist('merrow harbinger'/'LRW', 'Steve Prescott').
card_number('merrow harbinger'/'LRW', '73').
card_multiverse_id('merrow harbinger'/'LRW', '139420').

card_in_set('merrow reejerey', 'LRW').
card_original_type('merrow reejerey'/'LRW', 'Creature — Merfolk Soldier').
card_original_text('merrow reejerey'/'LRW', 'Other Merfolk creatures you control get +1/+1.\nWhenever you play a Merfolk spell, you may tap or untap target permanent.').
card_image_name('merrow reejerey'/'LRW', 'merrow reejerey').
card_uid('merrow reejerey'/'LRW', 'LRW:Merrow Reejerey:merrow reejerey').
card_rarity('merrow reejerey'/'LRW', 'Uncommon').
card_artist('merrow reejerey'/'LRW', 'Greg Staples').
card_number('merrow reejerey'/'LRW', '74').
card_flavor_text('merrow reejerey'/'LRW', 'Steady and silent as the deep current, the reejerey guides the course of the school.').
card_multiverse_id('merrow reejerey'/'LRW', '139702').

card_in_set('militia\'s pride', 'LRW').
card_original_type('militia\'s pride'/'LRW', 'Tribal Enchantment — Kithkin').
card_original_text('militia\'s pride'/'LRW', 'Whenever a nontoken creature you control attacks, you may pay {W}. If you do, put a 1/1 white Kithkin Soldier creature token into play tapped and attacking.').
card_first_print('militia\'s pride', 'LRW').
card_image_name('militia\'s pride'/'LRW', 'militia\'s pride').
card_uid('militia\'s pride'/'LRW', 'LRW:Militia\'s Pride:militia\'s pride').
card_rarity('militia\'s pride'/'LRW', 'Rare').
card_artist('militia\'s pride'/'LRW', 'Larry MacDougall').
card_number('militia\'s pride'/'LRW', '30').
card_flavor_text('militia\'s pride'/'LRW', 'If you pick a fight with one kithkin, be ready to fight them all.').
card_multiverse_id('militia\'s pride'/'LRW', '140193').

card_in_set('mirror entity', 'LRW').
card_original_type('mirror entity'/'LRW', 'Creature — Shapeshifter').
card_original_text('mirror entity'/'LRW', 'Changeling (This card is every creature type at all times.)\n{X}: Creatures you control become X/X and gain all creature types until end of turn.').
card_first_print('mirror entity', 'LRW').
card_image_name('mirror entity'/'LRW', 'mirror entity').
card_uid('mirror entity'/'LRW', 'LRW:Mirror Entity:mirror entity').
card_rarity('mirror entity'/'LRW', 'Rare').
card_artist('mirror entity'/'LRW', 'Zoltan Boros & Gabor Szikszai').
card_number('mirror entity'/'LRW', '31').
card_flavor_text('mirror entity'/'LRW', 'Unaware of Lorwyn\'s diversity, it sees only itself, reflected a thousand times over.').
card_multiverse_id('mirror entity'/'LRW', '141818').

card_in_set('mistbind clique', 'LRW').
card_original_type('mistbind clique'/'LRW', 'Creature — Faerie Wizard').
card_original_text('mistbind clique'/'LRW', 'Flash\nFlying\nChampion a Faerie (When this comes into play, sacrifice it unless you remove another Faerie you control from the game. When this leaves play, that card returns to play.)\nWhen a Faerie is championed with Mistbind Clique, tap all lands target player controls.').
card_first_print('mistbind clique', 'LRW').
card_image_name('mistbind clique'/'LRW', 'mistbind clique').
card_uid('mistbind clique'/'LRW', 'LRW:Mistbind Clique:mistbind clique').
card_rarity('mistbind clique'/'LRW', 'Rare').
card_artist('mistbind clique'/'LRW', 'Ben Thompson').
card_number('mistbind clique'/'LRW', '75').
card_multiverse_id('mistbind clique'/'LRW', '141825').

card_in_set('moonglove extract', 'LRW').
card_original_type('moonglove extract'/'LRW', 'Artifact').
card_original_text('moonglove extract'/'LRW', 'Sacrifice Moonglove Extract: Moonglove Extract deals 2 damage to target creature or player.').
card_first_print('moonglove extract', 'LRW').
card_image_name('moonglove extract'/'LRW', 'moonglove extract').
card_uid('moonglove extract'/'LRW', 'LRW:Moonglove Extract:moonglove extract').
card_rarity('moonglove extract'/'LRW', 'Common').
card_artist('moonglove extract'/'LRW', 'Terese Nielsen').
card_number('moonglove extract'/'LRW', '258').
card_flavor_text('moonglove extract'/'LRW', 'Diluted, moonglove can etch living tissue. Concentrated, a drop will kill a giant.').
card_multiverse_id('moonglove extract'/'LRW', '139506').

card_in_set('moonglove winnower', 'LRW').
card_original_type('moonglove winnower'/'LRW', 'Creature — Elf Rogue').
card_original_text('moonglove winnower'/'LRW', 'Deathtouch (Whenever this creature deals damage to a creature, destroy that creature.)').
card_first_print('moonglove winnower', 'LRW').
card_image_name('moonglove winnower'/'LRW', 'moonglove winnower').
card_uid('moonglove winnower'/'LRW', 'LRW:Moonglove Winnower:moonglove winnower').
card_rarity('moonglove winnower'/'LRW', 'Common').
card_artist('moonglove winnower'/'LRW', 'William O\'Connor').
card_number('moonglove winnower'/'LRW', '126').
card_flavor_text('moonglove winnower'/'LRW', 'Winnowers live to eliminate eyeblights, creatures the elves deem too ugly to exist.').
card_multiverse_id('moonglove winnower'/'LRW', '139483').

card_in_set('mosswort bridge', 'LRW').
card_original_type('mosswort bridge'/'LRW', 'Land').
card_original_text('mosswort bridge'/'LRW', 'Hideaway (This land comes into play tapped. When it does, look at the top four cards of your library, remove one from the game face down, then put the rest on the bottom of your library.)\n{T}: Add {G} to your mana pool.\n{G}, {T}: You may play the removed card without paying its mana cost if creatures you control have total power 10 or greater.').
card_first_print('mosswort bridge', 'LRW').
card_image_name('mosswort bridge'/'LRW', 'mosswort bridge').
card_uid('mosswort bridge'/'LRW', 'LRW:Mosswort Bridge:mosswort bridge').
card_rarity('mosswort bridge'/'LRW', 'Rare').
card_artist('mosswort bridge'/'LRW', 'Jeremy Jarvis').
card_number('mosswort bridge'/'LRW', '270').
card_multiverse_id('mosswort bridge'/'LRW', '139515').

card_in_set('mountain', 'LRW').
card_original_type('mountain'/'LRW', 'Basic Land — Mountain').
card_original_text('mountain'/'LRW', 'R').
card_image_name('mountain'/'LRW', 'mountain1').
card_uid('mountain'/'LRW', 'LRW:Mountain:mountain1').
card_rarity('mountain'/'LRW', 'Basic Land').
card_artist('mountain'/'LRW', 'Larry MacDougall').
card_number('mountain'/'LRW', '294').
card_multiverse_id('mountain'/'LRW', '143623').

card_in_set('mountain', 'LRW').
card_original_type('mountain'/'LRW', 'Basic Land — Mountain').
card_original_text('mountain'/'LRW', 'R').
card_image_name('mountain'/'LRW', 'mountain2').
card_uid('mountain'/'LRW', 'LRW:Mountain:mountain2').
card_rarity('mountain'/'LRW', 'Basic Land').
card_artist('mountain'/'LRW', 'Martina Pilcerova').
card_number('mountain'/'LRW', '295').
card_multiverse_id('mountain'/'LRW', '143627').

card_in_set('mountain', 'LRW').
card_original_type('mountain'/'LRW', 'Basic Land — Mountain').
card_original_text('mountain'/'LRW', 'R').
card_image_name('mountain'/'LRW', 'mountain3').
card_uid('mountain'/'LRW', 'LRW:Mountain:mountain3').
card_rarity('mountain'/'LRW', 'Basic Land').
card_artist('mountain'/'LRW', 'Wayne Reynolds').
card_number('mountain'/'LRW', '296').
card_multiverse_id('mountain'/'LRW', '143631').

card_in_set('mountain', 'LRW').
card_original_type('mountain'/'LRW', 'Basic Land — Mountain').
card_original_text('mountain'/'LRW', 'R').
card_image_name('mountain'/'LRW', 'mountain4').
card_uid('mountain'/'LRW', 'LRW:Mountain:mountain4').
card_rarity('mountain'/'LRW', 'Basic Land').
card_artist('mountain'/'LRW', 'Darrell Riche').
card_number('mountain'/'LRW', '297').
card_multiverse_id('mountain'/'LRW', '143626').

card_in_set('mournwhelk', 'LRW').
card_original_type('mournwhelk'/'LRW', 'Creature — Elemental').
card_original_text('mournwhelk'/'LRW', 'When Mournwhelk comes into play, target player discards two cards.\nEvoke {3}{B} (You may play this spell for its evoke cost. If you do, it\'s sacrificed when it comes into play.)').
card_first_print('mournwhelk', 'LRW').
card_image_name('mournwhelk'/'LRW', 'mournwhelk').
card_uid('mournwhelk'/'LRW', 'LRW:Mournwhelk:mournwhelk').
card_rarity('mournwhelk'/'LRW', 'Common').
card_artist('mournwhelk'/'LRW', 'Jeremy Jarvis').
card_number('mournwhelk'/'LRW', '127').
card_flavor_text('mournwhelk'/'LRW', 'It hoards Lorwyn\'s rare sorrows.').
card_multiverse_id('mournwhelk'/'LRW', '145800').

card_in_set('mudbutton torchrunner', 'LRW').
card_original_type('mudbutton torchrunner'/'LRW', 'Creature — Goblin Warrior').
card_original_text('mudbutton torchrunner'/'LRW', 'When Mudbutton Torchrunner is put into a graveyard from play, it deals 3 damage to target creature or player.').
card_first_print('mudbutton torchrunner', 'LRW').
card_image_name('mudbutton torchrunner'/'LRW', 'mudbutton torchrunner').
card_uid('mudbutton torchrunner'/'LRW', 'LRW:Mudbutton Torchrunner:mudbutton torchrunner').
card_rarity('mudbutton torchrunner'/'LRW', 'Common').
card_artist('mudbutton torchrunner'/'LRW', 'Steve Ellis').
card_number('mudbutton torchrunner'/'LRW', '185').
card_flavor_text('mudbutton torchrunner'/'LRW', 'The oil sloshes against his skull as he nears his destination: the Frogtosser Games and the lighting of the Flaming Boggart.').
card_multiverse_id('mudbutton torchrunner'/'LRW', '139742').

card_in_set('mulldrifter', 'LRW').
card_original_type('mulldrifter'/'LRW', 'Creature — Elemental').
card_original_text('mulldrifter'/'LRW', 'Flying\nWhen Mulldrifter comes into play, draw two cards.\nEvoke {2}{U} (You may play this spell for its evoke cost. If you do, it\'s sacrificed when it comes into play.)').
card_image_name('mulldrifter'/'LRW', 'mulldrifter').
card_uid('mulldrifter'/'LRW', 'LRW:Mulldrifter:mulldrifter').
card_rarity('mulldrifter'/'LRW', 'Common').
card_artist('mulldrifter'/'LRW', 'Eric Fortune').
card_number('mulldrifter'/'LRW', '76').
card_multiverse_id('mulldrifter'/'LRW', '145811').

card_in_set('nameless inversion', 'LRW').
card_original_type('nameless inversion'/'LRW', 'Tribal Instant — Shapeshifter').
card_original_text('nameless inversion'/'LRW', 'Changeling (This card is every creature type at all times.)\nTarget creature gets +3/-3 and loses all creature types until end of turn.').
card_image_name('nameless inversion'/'LRW', 'nameless inversion').
card_uid('nameless inversion'/'LRW', 'LRW:Nameless Inversion:nameless inversion').
card_rarity('nameless inversion'/'LRW', 'Common').
card_artist('nameless inversion'/'LRW', 'Jeff Miracola').
card_number('nameless inversion'/'LRW', '128').
card_flavor_text('nameless inversion'/'LRW', 'Just as a changeling\'s influence can have dramatic effects, so too can its sudden withdrawal.').
card_multiverse_id('nameless inversion'/'LRW', '143388').

card_in_set('nath of the gilt-leaf', 'LRW').
card_original_type('nath of the gilt-leaf'/'LRW', 'Legendary Creature — Elf Warrior').
card_original_text('nath of the gilt-leaf'/'LRW', 'At the beginning of your upkeep, you may have target opponent discard a card at random.\nWhenever an opponent discards a card, you may put a 1/1 green Elf Warrior creature token into play.').
card_first_print('nath of the gilt-leaf', 'LRW').
card_image_name('nath of the gilt-leaf'/'LRW', 'nath of the gilt-leaf').
card_uid('nath of the gilt-leaf'/'LRW', 'LRW:Nath of the Gilt-Leaf:nath of the gilt-leaf').
card_rarity('nath of the gilt-leaf'/'LRW', 'Rare').
card_artist('nath of the gilt-leaf'/'LRW', 'Kev Walker').
card_number('nath of the gilt-leaf'/'LRW', '250').
card_flavor_text('nath of the gilt-leaf'/'LRW', 'A savage hunter with a prince\'s bearing.').
card_multiverse_id('nath of the gilt-leaf'/'LRW', '143370').

card_in_set('nath\'s buffoon', 'LRW').
card_original_type('nath\'s buffoon'/'LRW', 'Creature — Goblin Rogue').
card_original_text('nath\'s buffoon'/'LRW', 'Protection from Elves').
card_first_print('nath\'s buffoon', 'LRW').
card_image_name('nath\'s buffoon'/'LRW', 'nath\'s buffoon').
card_uid('nath\'s buffoon'/'LRW', 'LRW:Nath\'s Buffoon:nath\'s buffoon').
card_rarity('nath\'s buffoon'/'LRW', 'Common').
card_artist('nath\'s buffoon'/'LRW', 'Thomas Denmark').
card_number('nath\'s buffoon'/'LRW', '129').
card_flavor_text('nath\'s buffoon'/'LRW', 'Smik learned the elvish dance quickly enough. The most difficult, yet most important step was to stay out of Nath\'s sight until called to perform.').
card_multiverse_id('nath\'s buffoon'/'LRW', '139446').

card_in_set('nath\'s elite', 'LRW').
card_original_type('nath\'s elite'/'LRW', 'Creature — Elf Warrior').
card_original_text('nath\'s elite'/'LRW', 'All creatures able to block Nath\'s Elite do so.\nWhen Nath\'s Elite comes into play, clash with an opponent. If you win, put a +1/+1 counter on Nath\'s Elite. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_first_print('nath\'s elite', 'LRW').
card_image_name('nath\'s elite'/'LRW', 'nath\'s elite').
card_uid('nath\'s elite'/'LRW', 'LRW:Nath\'s Elite:nath\'s elite').
card_rarity('nath\'s elite'/'LRW', 'Common').
card_artist('nath\'s elite'/'LRW', 'Wayne Reynolds').
card_number('nath\'s elite'/'LRW', '231').
card_multiverse_id('nath\'s elite'/'LRW', '145990').

card_in_set('neck snap', 'LRW').
card_original_type('neck snap'/'LRW', 'Instant').
card_original_text('neck snap'/'LRW', 'Destroy target attacking or blocking creature.').
card_first_print('neck snap', 'LRW').
card_image_name('neck snap'/'LRW', 'neck snap').
card_uid('neck snap'/'LRW', 'LRW:Neck Snap:neck snap').
card_rarity('neck snap'/'LRW', 'Common').
card_artist('neck snap'/'LRW', 'Dominick Domingo').
card_number('neck snap'/'LRW', '32').
card_flavor_text('neck snap'/'LRW', '\"We merrows need not be disadvantaged when fighting on land. We lack the vulnerability of those who breathe only through their throats.\"\n—Minnarin, merrow reejerey').
card_multiverse_id('neck snap'/'LRW', '139693').

card_in_set('nectar faerie', 'LRW').
card_original_type('nectar faerie'/'LRW', 'Creature — Faerie Wizard').
card_original_text('nectar faerie'/'LRW', 'Flying\n{B}, {T}: Target Faerie or Elf gains lifelink until end of turn. (Whenever it deals damage, its controller gains that much life.)').
card_first_print('nectar faerie', 'LRW').
card_image_name('nectar faerie'/'LRW', 'nectar faerie').
card_uid('nectar faerie'/'LRW', 'LRW:Nectar Faerie:nectar faerie').
card_rarity('nectar faerie'/'LRW', 'Uncommon').
card_artist('nectar faerie'/'LRW', 'Thomas Denmark').
card_number('nectar faerie'/'LRW', '130').
card_flavor_text('nectar faerie'/'LRW', '\"The unpredictable fae are just as likely to provide a blight as a boon.\"\n—Desmera, perfect of Wren\'s Run').
card_multiverse_id('nectar faerie'/'LRW', '139451').

card_in_set('needle drop', 'LRW').
card_original_type('needle drop'/'LRW', 'Instant').
card_original_text('needle drop'/'LRW', 'Needle Drop deals 1 damage to target creature or player that was dealt damage this turn.\nDraw a card.').
card_first_print('needle drop', 'LRW').
card_image_name('needle drop'/'LRW', 'needle drop').
card_uid('needle drop'/'LRW', 'LRW:Needle Drop:needle drop').
card_rarity('needle drop'/'LRW', 'Common').
card_artist('needle drop'/'LRW', 'Greg Staples').
card_number('needle drop'/'LRW', '186').
card_flavor_text('needle drop'/'LRW', '\"First it was plovers and mulldrifters. Now it\'s knitting needles the size of javelins.\"\n—Calydd, kithkin farmer').
card_multiverse_id('needle drop'/'LRW', '146161').

card_in_set('nettlevine blight', 'LRW').
card_original_type('nettlevine blight'/'LRW', 'Enchantment — Aura').
card_original_text('nettlevine blight'/'LRW', 'Enchant creature or land\nEnchanted permanent has \"At the end of your turn, sacrifice this permanent and attach Nettlevine Blight to a creature or land you control.\"').
card_first_print('nettlevine blight', 'LRW').
card_image_name('nettlevine blight'/'LRW', 'nettlevine blight').
card_uid('nettlevine blight'/'LRW', 'LRW:Nettlevine Blight:nettlevine blight').
card_rarity('nettlevine blight'/'LRW', 'Rare').
card_artist('nettlevine blight'/'LRW', 'Michael Sutfin').
card_number('nettlevine blight'/'LRW', '131').
card_multiverse_id('nettlevine blight'/'LRW', '143731').

card_in_set('nightshade stinger', 'LRW').
card_original_type('nightshade stinger'/'LRW', 'Creature — Faerie Rogue').
card_original_text('nightshade stinger'/'LRW', 'Flying\nNightshade Stinger can\'t block.').
card_first_print('nightshade stinger', 'LRW').
card_image_name('nightshade stinger'/'LRW', 'nightshade stinger').
card_uid('nightshade stinger'/'LRW', 'LRW:Nightshade Stinger:nightshade stinger').
card_rarity('nightshade stinger'/'LRW', 'Common').
card_artist('nightshade stinger'/'LRW', 'Mark Poole').
card_number('nightshade stinger'/'LRW', '132').
card_flavor_text('nightshade stinger'/'LRW', '\"Most faeries are harmless pranksters. Every now and again, though, you get one that crosses over from mischievous to malicious.\"\n—Gaddock Teeg').
card_multiverse_id('nightshade stinger'/'LRW', '139456').

card_in_set('nova chaser', 'LRW').
card_original_type('nova chaser'/'LRW', 'Creature — Elemental Warrior').
card_original_text('nova chaser'/'LRW', 'Trample\nChampion an Elemental (When this comes into play, sacrifice it unless you remove another Elemental you control from the game. When this leaves play, that card returns to play.)').
card_first_print('nova chaser', 'LRW').
card_image_name('nova chaser'/'LRW', 'nova chaser').
card_uid('nova chaser'/'LRW', 'LRW:Nova Chaser:nova chaser').
card_rarity('nova chaser'/'LRW', 'Rare').
card_artist('nova chaser'/'LRW', 'Dan Scott').
card_number('nova chaser'/'LRW', '187').
card_multiverse_id('nova chaser'/'LRW', '140239').

card_in_set('oaken brawler', 'LRW').
card_original_type('oaken brawler'/'LRW', 'Creature — Treefolk Warrior').
card_original_text('oaken brawler'/'LRW', 'When Oaken Brawler comes into play, clash with an opponent. If you win, put a +1/+1 counter on Oaken Brawler. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_first_print('oaken brawler', 'LRW').
card_image_name('oaken brawler'/'LRW', 'oaken brawler').
card_uid('oaken brawler'/'LRW', 'LRW:Oaken Brawler:oaken brawler').
card_rarity('oaken brawler'/'LRW', 'Common').
card_artist('oaken brawler'/'LRW', 'Jim Murray').
card_number('oaken brawler'/'LRW', '33').
card_multiverse_id('oaken brawler'/'LRW', '145988').

card_in_set('oakgnarl warrior', 'LRW').
card_original_type('oakgnarl warrior'/'LRW', 'Creature — Treefolk Warrior').
card_original_text('oakgnarl warrior'/'LRW', 'Vigilance, trample').
card_first_print('oakgnarl warrior', 'LRW').
card_image_name('oakgnarl warrior'/'LRW', 'oakgnarl warrior').
card_uid('oakgnarl warrior'/'LRW', 'LRW:Oakgnarl Warrior:oakgnarl warrior').
card_rarity('oakgnarl warrior'/'LRW', 'Common').
card_artist('oakgnarl warrior'/'LRW', 'Jim Nelson').
card_number('oakgnarl warrior'/'LRW', '232').
card_flavor_text('oakgnarl warrior'/'LRW', '\"Roam as you will, your roots remain in the strong earth of your Rising.\"').
card_multiverse_id('oakgnarl warrior'/'LRW', '139494').

card_in_set('oblivion ring', 'LRW').
card_original_type('oblivion ring'/'LRW', 'Enchantment').
card_original_text('oblivion ring'/'LRW', 'When Oblivion Ring comes into play, remove another target nonland permanent from the game.\nWhen Oblivion Ring leaves play, return the removed card to play under its owner\'s control.').
card_image_name('oblivion ring'/'LRW', 'oblivion ring').
card_uid('oblivion ring'/'LRW', 'LRW:Oblivion Ring:oblivion ring').
card_rarity('oblivion ring'/'LRW', 'Common').
card_artist('oblivion ring'/'LRW', 'Wayne England').
card_number('oblivion ring'/'LRW', '34').
card_flavor_text('oblivion ring'/'LRW', 'A circle of sugar and a word of forbiddance.').
card_multiverse_id('oblivion ring'/'LRW', '139414').

card_in_set('oona\'s prowler', 'LRW').
card_original_type('oona\'s prowler'/'LRW', 'Creature — Faerie Rogue').
card_original_text('oona\'s prowler'/'LRW', 'Flying\nDiscard a card: Oona\'s Prowler gets -2/-0 until end of turn. Any player may play this ability.').
card_first_print('oona\'s prowler', 'LRW').
card_image_name('oona\'s prowler'/'LRW', 'oona\'s prowler').
card_uid('oona\'s prowler'/'LRW', 'LRW:Oona\'s Prowler:oona\'s prowler').
card_rarity('oona\'s prowler'/'LRW', 'Rare').
card_artist('oona\'s prowler'/'LRW', 'Wayne Reynolds').
card_number('oona\'s prowler'/'LRW', '133').
card_flavor_text('oona\'s prowler'/'LRW', 'Deep in Glen Elendra blossoms Oona, queen of the faeries, nourished by secrets and pollinated by stolen dreams.').
card_multiverse_id('oona\'s prowler'/'LRW', '146582').

card_in_set('paperfin rascal', 'LRW').
card_original_type('paperfin rascal'/'LRW', 'Creature — Merfolk Rogue').
card_original_text('paperfin rascal'/'LRW', 'When Paperfin Rascal comes into play, clash with an opponent. If you win, put a +1/+1 counter on Paperfin Rascal. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_first_print('paperfin rascal', 'LRW').
card_image_name('paperfin rascal'/'LRW', 'paperfin rascal').
card_uid('paperfin rascal'/'LRW', 'LRW:Paperfin Rascal:paperfin rascal').
card_rarity('paperfin rascal'/'LRW', 'Common').
card_artist('paperfin rascal'/'LRW', 'Zoltan Boros & Gabor Szikszai').
card_number('paperfin rascal'/'LRW', '77').
card_multiverse_id('paperfin rascal'/'LRW', '145814').

card_in_set('peppersmoke', 'LRW').
card_original_type('peppersmoke'/'LRW', 'Tribal Instant — Faerie').
card_original_text('peppersmoke'/'LRW', 'Target creature gets -1/-1 until end of turn. If you control a Faerie, draw a card.').
card_first_print('peppersmoke', 'LRW').
card_image_name('peppersmoke'/'LRW', 'peppersmoke').
card_uid('peppersmoke'/'LRW', 'LRW:Peppersmoke:peppersmoke').
card_rarity('peppersmoke'/'LRW', 'Common').
card_artist('peppersmoke'/'LRW', 'Rebecca Guay').
card_number('peppersmoke'/'LRW', '134').
card_flavor_text('peppersmoke'/'LRW', 'Like being trapped in a perpetual sneeze, faerie-dust poisoning is both exhilarating and agonizing.').
card_multiverse_id('peppersmoke'/'LRW', '139458').

card_in_set('pestermite', 'LRW').
card_original_type('pestermite'/'LRW', 'Creature — Faerie Rogue').
card_original_text('pestermite'/'LRW', 'Flash\nFlying\nWhen Pestermite comes into play, you may tap or untap target permanent.').
card_first_print('pestermite', 'LRW').
card_image_name('pestermite'/'LRW', 'pestermite').
card_uid('pestermite'/'LRW', 'LRW:Pestermite:pestermite').
card_rarity('pestermite'/'LRW', 'Common').
card_artist('pestermite'/'LRW', 'Christopher Moeller').
card_number('pestermite'/'LRW', '78').
card_flavor_text('pestermite'/'LRW', 'The fae know when they\'re not wanted. That\'s precisely why they show up.').
card_multiverse_id('pestermite'/'LRW', '139428').

card_in_set('plains', 'LRW').
card_original_type('plains'/'LRW', 'Basic Land — Plains').
card_original_text('plains'/'LRW', 'W').
card_image_name('plains'/'LRW', 'plains1').
card_uid('plains'/'LRW', 'LRW:Plains:plains1').
card_rarity('plains'/'LRW', 'Basic Land').
card_artist('plains'/'LRW', 'Warren Mahy').
card_number('plains'/'LRW', '282').
card_multiverse_id('plains'/'LRW', '143621').

card_in_set('plains', 'LRW').
card_original_type('plains'/'LRW', 'Basic Land — Plains').
card_original_text('plains'/'LRW', 'W').
card_image_name('plains'/'LRW', 'plains2').
card_uid('plains'/'LRW', 'LRW:Plains:plains2').
card_rarity('plains'/'LRW', 'Basic Land').
card_artist('plains'/'LRW', 'Fred Fields').
card_number('plains'/'LRW', '283').
card_multiverse_id('plains'/'LRW', '143630').

card_in_set('plains', 'LRW').
card_original_type('plains'/'LRW', 'Basic Land — Plains').
card_original_text('plains'/'LRW', 'W').
card_image_name('plains'/'LRW', 'plains3').
card_uid('plains'/'LRW', 'LRW:Plains:plains3').
card_rarity('plains'/'LRW', 'Basic Land').
card_artist('plains'/'LRW', 'Larry MacDougall').
card_number('plains'/'LRW', '284').
card_multiverse_id('plains'/'LRW', '143622').

card_in_set('plains', 'LRW').
card_original_type('plains'/'LRW', 'Basic Land — Plains').
card_original_text('plains'/'LRW', 'W').
card_image_name('plains'/'LRW', 'plains4').
card_uid('plains'/'LRW', 'LRW:Plains:plains4').
card_rarity('plains'/'LRW', 'Basic Land').
card_artist('plains'/'LRW', 'Omar Rayyan').
card_number('plains'/'LRW', '285').
card_multiverse_id('plains'/'LRW', '143620').

card_in_set('plover knights', 'LRW').
card_original_type('plover knights'/'LRW', 'Creature — Kithkin Knight').
card_original_text('plover knights'/'LRW', 'Flying, first strike').
card_first_print('plover knights', 'LRW').
card_image_name('plover knights'/'LRW', 'plover knights').
card_uid('plover knights'/'LRW', 'LRW:Plover Knights:plover knights').
card_rarity('plover knights'/'LRW', 'Common').
card_artist('plover knights'/'LRW', 'Quinton Hoover').
card_number('plover knights'/'LRW', '35').
card_flavor_text('plover knights'/'LRW', 'The knights are a major attraction at every Lammastide festival. Teams of riders perform daring feats of flight to the delight of all below.').
card_multiverse_id('plover knights'/'LRW', '139396').

card_in_set('pollen lullaby', 'LRW').
card_original_type('pollen lullaby'/'LRW', 'Instant').
card_original_text('pollen lullaby'/'LRW', 'Prevent all combat damage that would be dealt this turn. Clash with an opponent. If you win, creatures that player controls don\'t untap during the player\'s next untap step. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_first_print('pollen lullaby', 'LRW').
card_image_name('pollen lullaby'/'LRW', 'pollen lullaby').
card_uid('pollen lullaby'/'LRW', 'LRW:Pollen Lullaby:pollen lullaby').
card_rarity('pollen lullaby'/'LRW', 'Uncommon').
card_artist('pollen lullaby'/'LRW', 'Warren Mahy').
card_number('pollen lullaby'/'LRW', '36').
card_multiverse_id('pollen lullaby'/'LRW', '145815').

card_in_set('ponder', 'LRW').
card_original_type('ponder'/'LRW', 'Sorcery').
card_original_text('ponder'/'LRW', 'Look at the top three cards of your library, then put them back in any order. You may shuffle your library.\nDraw a card.').
card_image_name('ponder'/'LRW', 'ponder').
card_uid('ponder'/'LRW', 'LRW:Ponder:ponder').
card_rarity('ponder'/'LRW', 'Common').
card_artist('ponder'/'LRW', 'Mark Tedin').
card_number('ponder'/'LRW', '79').
card_flavor_text('ponder'/'LRW', '\"We see the same sky as you, just through a different lens.\"').
card_multiverse_id('ponder'/'LRW', '139512').

card_in_set('primal command', 'LRW').
card_original_type('primal command'/'LRW', 'Sorcery').
card_original_text('primal command'/'LRW', 'Choose two Target player gains 7 life; or put target noncreature permanent on top of its owner\'s library; or target player shuffles his or her graveyard into his or her library; or search your library for a creature card, reveal it, put it into your hand, then shuffle your library.').
card_first_print('primal command', 'LRW').
card_image_name('primal command'/'LRW', 'primal command').
card_uid('primal command'/'LRW', 'LRW:Primal Command:primal command').
card_rarity('primal command'/'LRW', 'Rare').
card_artist('primal command'/'LRW', 'Wayne England').
card_number('primal command'/'LRW', '233').
card_multiverse_id('primal command'/'LRW', '141824').

card_in_set('profane command', 'LRW').
card_original_type('profane command'/'LRW', 'Sorcery').
card_original_text('profane command'/'LRW', 'Choose two Target player loses X life; or return target creature card with converted mana cost X or less from your graveyard to play; or target creature gets -X/-X until end of turn; or up to X target creatures gain fear until end of turn.').
card_first_print('profane command', 'LRW').
card_image_name('profane command'/'LRW', 'profane command').
card_uid('profane command'/'LRW', 'LRW:Profane Command:profane command').
card_rarity('profane command'/'LRW', 'Rare').
card_artist('profane command'/'LRW', 'Wayne England').
card_number('profane command'/'LRW', '135').
card_multiverse_id('profane command'/'LRW', '141814').

card_in_set('protective bubble', 'LRW').
card_original_type('protective bubble'/'LRW', 'Enchantment — Aura').
card_original_text('protective bubble'/'LRW', 'Enchant creature\nEnchanted creature is unblockable and has shroud. (It can\'t be the target of spells or abilities.)').
card_first_print('protective bubble', 'LRW').
card_image_name('protective bubble'/'LRW', 'protective bubble').
card_uid('protective bubble'/'LRW', 'LRW:Protective Bubble:protective bubble').
card_rarity('protective bubble'/'LRW', 'Common').
card_artist('protective bubble'/'LRW', 'Steve Ellis').
card_number('protective bubble'/'LRW', '80').
card_flavor_text('protective bubble'/'LRW', 'Skilled merrow rudders ensure their charges arrive on time and without incident.').
card_multiverse_id('protective bubble'/'LRW', '139687').

card_in_set('prowess of the fair', 'LRW').
card_original_type('prowess of the fair'/'LRW', 'Tribal Enchantment — Elf').
card_original_text('prowess of the fair'/'LRW', 'Whenever another nontoken Elf is put into your graveyard from play, you may put a 1/1 green Elf Warrior creature token into play.').
card_first_print('prowess of the fair', 'LRW').
card_image_name('prowess of the fair'/'LRW', 'prowess of the fair').
card_uid('prowess of the fair'/'LRW', 'LRW:Prowess of the Fair:prowess of the fair').
card_rarity('prowess of the fair'/'LRW', 'Uncommon').
card_artist('prowess of the fair'/'LRW', 'Jeremy Jarvis').
card_number('prowess of the fair'/'LRW', '136').
card_flavor_text('prowess of the fair'/'LRW', 'An elvish duel is a thing of beauty: the warriors\' grace, the crash of steel, then the artful spray of blood.').
card_multiverse_id('prowess of the fair'/'LRW', '146167').

card_in_set('purity', 'LRW').
card_original_type('purity'/'LRW', 'Creature — Elemental Incarnation').
card_original_text('purity'/'LRW', 'Flying\nIf a spell or ability would deal damage to you, prevent that damage. You gain life equal to the damage prevented this way.\nWhen Purity is put into a graveyard from anywhere, shuffle it into its owner\'s library.').
card_first_print('purity', 'LRW').
card_image_name('purity'/'LRW', 'purity').
card_uid('purity'/'LRW', 'LRW:Purity:purity').
card_rarity('purity'/'LRW', 'Rare').
card_artist('purity'/'LRW', 'Warren Mahy').
card_number('purity'/'LRW', '37').
card_multiverse_id('purity'/'LRW', '140214').

card_in_set('quill-slinger boggart', 'LRW').
card_original_type('quill-slinger boggart'/'LRW', 'Creature — Goblin Warrior').
card_original_text('quill-slinger boggart'/'LRW', 'Whenever a player plays a Kithkin spell, you may have target player lose 1 life.').
card_first_print('quill-slinger boggart', 'LRW').
card_image_name('quill-slinger boggart'/'LRW', 'quill-slinger boggart').
card_uid('quill-slinger boggart'/'LRW', 'LRW:Quill-Slinger Boggart:quill-slinger boggart').
card_rarity('quill-slinger boggart'/'LRW', 'Common').
card_artist('quill-slinger boggart'/'LRW', 'Warren Mahy').
card_number('quill-slinger boggart'/'LRW', '137').
card_flavor_text('quill-slinger boggart'/'LRW', '\"A good day in Goldmeadow is one in which I don\'t spend all evening picking quills out of my backside.\"\n—Calydd, kithkin farmer').
card_multiverse_id('quill-slinger boggart'/'LRW', '139469').

card_in_set('rebellion of the flamekin', 'LRW').
card_original_type('rebellion of the flamekin'/'LRW', 'Tribal Enchantment — Elemental').
card_original_text('rebellion of the flamekin'/'LRW', 'Whenever you clash, you may pay {1}. If you do, put a 3/1 red Elemental Shaman creature token into play. If you won, that token gains haste until end of turn. (This ability triggers after the clash ends.)').
card_first_print('rebellion of the flamekin', 'LRW').
card_image_name('rebellion of the flamekin'/'LRW', 'rebellion of the flamekin').
card_uid('rebellion of the flamekin'/'LRW', 'LRW:Rebellion of the Flamekin:rebellion of the flamekin').
card_rarity('rebellion of the flamekin'/'LRW', 'Uncommon').
card_artist('rebellion of the flamekin'/'LRW', 'Dan Dos Santos').
card_number('rebellion of the flamekin'/'LRW', '188').
card_multiverse_id('rebellion of the flamekin'/'LRW', '139679').

card_in_set('rings of brighthearth', 'LRW').
card_original_type('rings of brighthearth'/'LRW', 'Artifact').
card_original_text('rings of brighthearth'/'LRW', 'Whenever you play an activated ability, if it isn\'t a mana ability, you may pay {2}. If you do, copy that ability. You may choose new targets for the copy.').
card_first_print('rings of brighthearth', 'LRW').
card_image_name('rings of brighthearth'/'LRW', 'rings of brighthearth').
card_uid('rings of brighthearth'/'LRW', 'LRW:Rings of Brighthearth:rings of brighthearth').
card_rarity('rings of brighthearth'/'LRW', 'Rare').
card_artist('rings of brighthearth'/'LRW', 'Howard Lyon').
card_number('rings of brighthearth'/'LRW', '259').
card_flavor_text('rings of brighthearth'/'LRW', '\"Without flame, there would be no iron tools, no cooked meals, no purge of old growth to make room for new.\"\n—Brighthearth creed').
card_multiverse_id('rings of brighthearth'/'LRW', '140216').

card_in_set('ringskipper', 'LRW').
card_original_type('ringskipper'/'LRW', 'Creature — Faerie Wizard').
card_original_text('ringskipper'/'LRW', 'Flying\nWhen Ringskipper is put into a graveyard from play, clash with an opponent. If you win, return Ringskipper to its owner\'s hand. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_first_print('ringskipper', 'LRW').
card_image_name('ringskipper'/'LRW', 'ringskipper').
card_uid('ringskipper'/'LRW', 'LRW:Ringskipper:ringskipper').
card_rarity('ringskipper'/'LRW', 'Common').
card_artist('ringskipper'/'LRW', 'Heather Hudson').
card_number('ringskipper'/'LRW', '81').
card_multiverse_id('ringskipper'/'LRW', '145802').

card_in_set('rootgrapple', 'LRW').
card_original_type('rootgrapple'/'LRW', 'Tribal Instant — Treefolk').
card_original_text('rootgrapple'/'LRW', 'Destroy target noncreature permanent. If you control a Treefolk, draw a card.').
card_first_print('rootgrapple', 'LRW').
card_image_name('rootgrapple'/'LRW', 'rootgrapple').
card_uid('rootgrapple'/'LRW', 'LRW:Rootgrapple:rootgrapple').
card_rarity('rootgrapple'/'LRW', 'Common').
card_artist('rootgrapple'/'LRW', 'Alan Pollack').
card_number('rootgrapple'/'LRW', '234').
card_flavor_text('rootgrapple'/'LRW', '\"All the sylvan secrets of this world are etched between my rings. The skinfolk\'s metal aberrations can rot between my roots.\"\n—Colfenor, the Last Yew').
card_multiverse_id('rootgrapple'/'LRW', '139499').

card_in_set('runed stalactite', 'LRW').
card_original_type('runed stalactite'/'LRW', 'Artifact — Equipment').
card_original_text('runed stalactite'/'LRW', 'Equipped creature gets +1/+1 and is every creature type.\nEquip {2}').
card_first_print('runed stalactite', 'LRW').
card_image_name('runed stalactite'/'LRW', 'runed stalactite').
card_uid('runed stalactite'/'LRW', 'LRW:Runed Stalactite:runed stalactite').
card_rarity('runed stalactite'/'LRW', 'Common').
card_artist('runed stalactite'/'LRW', 'Jim Pavelec').
card_number('runed stalactite'/'LRW', '260').
card_flavor_text('runed stalactite'/'LRW', 'When a changeling adopts a form no other changeling has taken, a rune appears in the caverns of Velis Vel to mark the event.').
card_multiverse_id('runed stalactite'/'LRW', '139508').

card_in_set('scarred vinebreeder', 'LRW').
card_original_type('scarred vinebreeder'/'LRW', 'Creature — Elf Shaman').
card_original_text('scarred vinebreeder'/'LRW', '{2}{B}, Remove an Elf card in your graveyard from the game: Scarred Vinebreeder gets +3/+3 until end of turn.').
card_first_print('scarred vinebreeder', 'LRW').
card_image_name('scarred vinebreeder'/'LRW', 'scarred vinebreeder').
card_uid('scarred vinebreeder'/'LRW', 'LRW:Scarred Vinebreeder:scarred vinebreeder').
card_rarity('scarred vinebreeder'/'LRW', 'Common').
card_artist('scarred vinebreeder'/'LRW', 'Alex Horley-Orlandelli').
card_number('scarred vinebreeder'/'LRW', '138').
card_flavor_text('scarred vinebreeder'/'LRW', 'For disfigured elves, there are few choices beyond death or nettlevine.').
card_multiverse_id('scarred vinebreeder'/'LRW', '139674').

card_in_set('scattering stroke', 'LRW').
card_original_type('scattering stroke'/'LRW', 'Instant').
card_original_text('scattering stroke'/'LRW', 'Counter target spell. Clash with an opponent. If you win, at the beginning of your next main phase, you may add {X} to your mana pool, where X is that spell\'s converted mana cost. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_first_print('scattering stroke', 'LRW').
card_image_name('scattering stroke'/'LRW', 'scattering stroke').
card_uid('scattering stroke'/'LRW', 'LRW:Scattering Stroke:scattering stroke').
card_rarity('scattering stroke'/'LRW', 'Uncommon').
card_artist('scattering stroke'/'LRW', 'Franz Vohwinkel').
card_number('scattering stroke'/'LRW', '82').
card_multiverse_id('scattering stroke'/'LRW', '139668').

card_in_set('scion of oona', 'LRW').
card_original_type('scion of oona'/'LRW', 'Creature — Faerie Soldier').
card_original_text('scion of oona'/'LRW', 'Flash\nFlying\nOther Faerie creatures you control get +1/+1.\nOther Faeries you control have shroud. (A permanent with shroud can\'t be the target of spells or abilities.)').
card_first_print('scion of oona', 'LRW').
card_image_name('scion of oona'/'LRW', 'scion of oona').
card_uid('scion of oona'/'LRW', 'LRW:Scion of Oona:scion of oona').
card_rarity('scion of oona'/'LRW', 'Rare').
card_artist('scion of oona'/'LRW', 'Eric Fortune').
card_number('scion of oona'/'LRW', '83').
card_multiverse_id('scion of oona'/'LRW', '139741').

card_in_set('secluded glen', 'LRW').
card_original_type('secluded glen'/'LRW', 'Land').
card_original_text('secluded glen'/'LRW', 'As Secluded Glen comes into play, you may reveal a Faerie card from your hand. If you don\'t, Secluded Glen comes into play tapped.\n{T}: Add {U} or {B} to your mana pool.').
card_first_print('secluded glen', 'LRW').
card_image_name('secluded glen'/'LRW', 'secluded glen').
card_uid('secluded glen'/'LRW', 'LRW:Secluded Glen:secluded glen').
card_rarity('secluded glen'/'LRW', 'Rare').
card_artist('secluded glen'/'LRW', 'Terese Nielsen').
card_number('secluded glen'/'LRW', '271').
card_flavor_text('secluded glen'/'LRW', 'Protected by glamers and guile, Glen Elendra harbors the elusive Oona, queen of the fae.').
card_multiverse_id('secluded glen'/'LRW', '153458').

card_in_set('seedguide ash', 'LRW').
card_original_type('seedguide ash'/'LRW', 'Creature — Treefolk Druid').
card_original_text('seedguide ash'/'LRW', 'When Seedguide Ash is put into a graveyard from play, you may search your library for up to three Forest cards and put them into play tapped. If you do, shuffle your library.').
card_first_print('seedguide ash', 'LRW').
card_image_name('seedguide ash'/'LRW', 'seedguide ash').
card_uid('seedguide ash'/'LRW', 'LRW:Seedguide Ash:seedguide ash').
card_rarity('seedguide ash'/'LRW', 'Uncommon').
card_artist('seedguide ash'/'LRW', 'John Avon').
card_number('seedguide ash'/'LRW', '235').
card_flavor_text('seedguide ash'/'LRW', '\"May you shade three generations of seedlings.\"').
card_multiverse_id('seedguide ash'/'LRW', '146165').

card_in_set('sentinels of glen elendra', 'LRW').
card_original_type('sentinels of glen elendra'/'LRW', 'Creature — Faerie Soldier').
card_original_text('sentinels of glen elendra'/'LRW', 'Flash\nFlying').
card_first_print('sentinels of glen elendra', 'LRW').
card_image_name('sentinels of glen elendra'/'LRW', 'sentinels of glen elendra').
card_uid('sentinels of glen elendra'/'LRW', 'LRW:Sentinels of Glen Elendra:sentinels of glen elendra').
card_rarity('sentinels of glen elendra'/'LRW', 'Common').
card_artist('sentinels of glen elendra'/'LRW', 'Howard Lyon').
card_number('sentinels of glen elendra'/'LRW', '84').
card_flavor_text('sentinels of glen elendra'/'LRW', 'Some say the valley of Glen Elendra is mythical, and that rumors of its existence are nothing but a faerie prank. Others say it is the fae\'s most fiercely guarded secret.').
card_multiverse_id('sentinels of glen elendra'/'LRW', '139426').

card_in_set('sentry oak', 'LRW').
card_original_type('sentry oak'/'LRW', 'Creature — Treefolk Warrior').
card_original_text('sentry oak'/'LRW', 'Defender\nAt the beginning of combat on your turn, you may clash with an opponent. If you win, Sentry Oak gets +2/+0 and loses defender until end of turn. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_first_print('sentry oak', 'LRW').
card_image_name('sentry oak'/'LRW', 'sentry oak').
card_uid('sentry oak'/'LRW', 'LRW:Sentry Oak:sentry oak').
card_rarity('sentry oak'/'LRW', 'Uncommon').
card_artist('sentry oak'/'LRW', 'Warren Mahy').
card_number('sentry oak'/'LRW', '38').
card_multiverse_id('sentry oak'/'LRW', '145970').

card_in_set('shapesharer', 'LRW').
card_original_type('shapesharer'/'LRW', 'Creature — Shapeshifter').
card_original_text('shapesharer'/'LRW', 'Changeling (This card is every creature type at all times.)\n{2}{U}: Target Shapeshifter becomes a copy of target creature until your next turn.').
card_first_print('shapesharer', 'LRW').
card_image_name('shapesharer'/'LRW', 'shapesharer').
card_uid('shapesharer'/'LRW', 'LRW:Shapesharer:shapesharer').
card_rarity('shapesharer'/'LRW', 'Rare').
card_artist('shapesharer'/'LRW', 'Alan Pollack').
card_number('shapesharer'/'LRW', '85').
card_flavor_text('shapesharer'/'LRW', 'One good mimic deserves another.').
card_multiverse_id('shapesharer'/'LRW', '140200').

card_in_set('shelldock isle', 'LRW').
card_original_type('shelldock isle'/'LRW', 'Land').
card_original_text('shelldock isle'/'LRW', 'Hideaway (This land comes into play tapped. When it does, look at the top four cards of your library, remove one from the game face down, then put the rest on the bottom of your library.)\n{T}: Add {U} to your mana pool.\n{U}, {T}: You may play the removed card without paying its mana cost if a library has twenty or fewer cards in it.').
card_first_print('shelldock isle', 'LRW').
card_image_name('shelldock isle'/'LRW', 'shelldock isle').
card_uid('shelldock isle'/'LRW', 'LRW:Shelldock Isle:shelldock isle').
card_rarity('shelldock isle'/'LRW', 'Rare').
card_artist('shelldock isle'/'LRW', 'Mark Tedin').
card_number('shelldock isle'/'LRW', '272').
card_multiverse_id('shelldock isle'/'LRW', '146178').

card_in_set('shields of velis vel', 'LRW').
card_original_type('shields of velis vel'/'LRW', 'Tribal Instant — Shapeshifter').
card_original_text('shields of velis vel'/'LRW', 'Changeling (This card is every creature type at all times.)\nCreatures target player controls get +0/+1 and gain all creature types until end of turn.').
card_first_print('shields of velis vel', 'LRW').
card_image_name('shields of velis vel'/'LRW', 'shields of velis vel').
card_uid('shields of velis vel'/'LRW', 'LRW:Shields of Velis Vel:shields of velis vel').
card_rarity('shields of velis vel'/'LRW', 'Common').
card_artist('shields of velis vel'/'LRW', 'Ralph Horsley').
card_number('shields of velis vel'/'LRW', '39').
card_flavor_text('shields of velis vel'/'LRW', 'Changelings can alter shape based on what the beings around them desire most.').
card_multiverse_id('shields of velis vel'/'LRW', '139411').

card_in_set('shimmering grotto', 'LRW').
card_original_type('shimmering grotto'/'LRW', 'Land').
card_original_text('shimmering grotto'/'LRW', '{T}: Add {1} to your mana pool.\n{1}, {T}: Add one mana of any color to your mana pool.').
card_first_print('shimmering grotto', 'LRW').
card_image_name('shimmering grotto'/'LRW', 'shimmering grotto').
card_uid('shimmering grotto'/'LRW', 'LRW:Shimmering Grotto:shimmering grotto').
card_rarity('shimmering grotto'/'LRW', 'Common').
card_artist('shimmering grotto'/'LRW', 'Alan Pollack').
card_number('shimmering grotto'/'LRW', '273').
card_flavor_text('shimmering grotto'/'LRW', 'Once each year, the sun\'s rays reach the hidden chamber of Velis Vel, and the changelings congregate from afar to bathe in the ever-shifting light.').
card_multiverse_id('shimmering grotto'/'LRW', '141802').

card_in_set('shriekmaw', 'LRW').
card_original_type('shriekmaw'/'LRW', 'Creature — Elemental').
card_original_text('shriekmaw'/'LRW', 'Fear\nWhen Shriekmaw comes into play, destroy target nonartifact, nonblack creature.\nEvoke {1}{B} (You may play this spell for its evoke cost. If you do, it\'s sacrificed when it comes into play.)').
card_image_name('shriekmaw'/'LRW', 'shriekmaw').
card_uid('shriekmaw'/'LRW', 'LRW:Shriekmaw:shriekmaw').
card_rarity('shriekmaw'/'LRW', 'Uncommon').
card_artist('shriekmaw'/'LRW', 'Steve Prescott').
card_number('shriekmaw'/'LRW', '139').
card_multiverse_id('shriekmaw'/'LRW', '146175').

card_in_set('silvergill adept', 'LRW').
card_original_type('silvergill adept'/'LRW', 'Creature — Merfolk Wizard').
card_original_text('silvergill adept'/'LRW', 'As an additional cost to play Silvergill Adept, reveal a Merfolk card from your hand or pay {3}.\nWhen Silvergill Adept comes into play, draw a card.').
card_first_print('silvergill adept', 'LRW').
card_image_name('silvergill adept'/'LRW', 'silvergill adept').
card_uid('silvergill adept'/'LRW', 'LRW:Silvergill Adept:silvergill adept').
card_rarity('silvergill adept'/'LRW', 'Uncommon').
card_artist('silvergill adept'/'LRW', 'Matt Cavotta').
card_number('silvergill adept'/'LRW', '86').
card_flavor_text('silvergill adept'/'LRW', '\"I bring baubles of lore, lost secrets seined from the listening waves.\"').
card_multiverse_id('silvergill adept'/'LRW', '139682').

card_in_set('silvergill douser', 'LRW').
card_original_type('silvergill douser'/'LRW', 'Creature — Merfolk Wizard').
card_original_text('silvergill douser'/'LRW', '{T}: Target creature gets -X/-0 until end of turn, where X is the number of Merfolk and/or Faeries you control.').
card_first_print('silvergill douser', 'LRW').
card_image_name('silvergill douser'/'LRW', 'silvergill douser').
card_uid('silvergill douser'/'LRW', 'LRW:Silvergill Douser:silvergill douser').
card_rarity('silvergill douser'/'LRW', 'Common').
card_artist('silvergill douser'/'LRW', 'Daren Bader').
card_number('silvergill douser'/'LRW', '87').
card_flavor_text('silvergill douser'/'LRW', 'The Silvergill school monitors traffic on the Lanes, ensuring that the riffraff don\'t interfere with travelers.').
card_multiverse_id('silvergill douser'/'LRW', '141853').

card_in_set('skeletal changeling', 'LRW').
card_original_type('skeletal changeling'/'LRW', 'Creature — Shapeshifter').
card_original_text('skeletal changeling'/'LRW', 'Changeling (This card is every creature type at all times.)\n{1}{B}: Regenerate Skeletal Changeling.').
card_first_print('skeletal changeling', 'LRW').
card_image_name('skeletal changeling'/'LRW', 'skeletal changeling').
card_uid('skeletal changeling'/'LRW', 'LRW:Skeletal Changeling:skeletal changeling').
card_rarity('skeletal changeling'/'LRW', 'Common').
card_artist('skeletal changeling'/'LRW', 'Alan Pollack').
card_number('skeletal changeling'/'LRW', '140').
card_flavor_text('skeletal changeling'/'LRW', 'Though they lack true flesh and bone of their own, changelings imitate either with equal ease.').
card_multiverse_id('skeletal changeling'/'LRW', '145809').

card_in_set('smokebraider', 'LRW').
card_original_type('smokebraider'/'LRW', 'Creature — Elemental Shaman').
card_original_text('smokebraider'/'LRW', '{T}: Add two mana in any combination of colors to your mana pool. Spend this mana only to play Elemental spells or activated abilities of Elementals.').
card_first_print('smokebraider', 'LRW').
card_image_name('smokebraider'/'LRW', 'smokebraider').
card_uid('smokebraider'/'LRW', 'LRW:Smokebraider:smokebraider').
card_rarity('smokebraider'/'LRW', 'Common').
card_artist('smokebraider'/'LRW', 'Anthony S. Waters').
card_number('smokebraider'/'LRW', '189').
card_flavor_text('smokebraider'/'LRW', '\"Be silent and listen to your inner fire. Only then can you walk the Path of Flame.\"').
card_multiverse_id('smokebraider'/'LRW', '139465').

card_in_set('soaring hope', 'LRW').
card_original_type('soaring hope'/'LRW', 'Enchantment — Aura').
card_original_text('soaring hope'/'LRW', 'Enchant creature\nWhen Soaring Hope comes into play, you gain 3 life.\nEnchanted creature has flying.\n{W}: Put Soaring Hope on top of its owner\'s library.').
card_first_print('soaring hope', 'LRW').
card_image_name('soaring hope'/'LRW', 'soaring hope').
card_uid('soaring hope'/'LRW', 'LRW:Soaring Hope:soaring hope').
card_rarity('soaring hope'/'LRW', 'Common').
card_artist('soaring hope'/'LRW', 'Martina Pilcerova').
card_number('soaring hope'/'LRW', '40').
card_multiverse_id('soaring hope'/'LRW', '139511').

card_in_set('soulbright flamekin', 'LRW').
card_original_type('soulbright flamekin'/'LRW', 'Creature — Elemental Shaman').
card_original_text('soulbright flamekin'/'LRW', '{2}: Target creature gains trample until end of turn. If this is the third time this ability has resolved this turn, you may add {R}{R}{R}{R}{R}{R}{R}{R} to your mana pool.').
card_first_print('soulbright flamekin', 'LRW').
card_image_name('soulbright flamekin'/'LRW', 'soulbright flamekin').
card_uid('soulbright flamekin'/'LRW', 'LRW:Soulbright Flamekin:soulbright flamekin').
card_rarity('soulbright flamekin'/'LRW', 'Common').
card_artist('soulbright flamekin'/'LRW', 'Kev Walker').
card_number('soulbright flamekin'/'LRW', '190').
card_flavor_text('soulbright flamekin'/'LRW', 'When provoked, a flamekin\'s inner fire burns far hotter than any giant\'s forge.').
card_multiverse_id('soulbright flamekin'/'LRW', '139467').

card_in_set('sower of temptation', 'LRW').
card_original_type('sower of temptation'/'LRW', 'Creature — Faerie Wizard').
card_original_text('sower of temptation'/'LRW', 'Flying\nWhen Sower of Temptation comes into play, gain control of target creature as long as Sower of Temptation remains in play.').
card_first_print('sower of temptation', 'LRW').
card_image_name('sower of temptation'/'LRW', 'sower of temptation').
card_uid('sower of temptation'/'LRW', 'LRW:Sower of Temptation:sower of temptation').
card_rarity('sower of temptation'/'LRW', 'Rare').
card_artist('sower of temptation'/'LRW', 'Christopher Moeller').
card_number('sower of temptation'/'LRW', '88').
card_flavor_text('sower of temptation'/'LRW', 'One glamer leads him far from home. The next washes away his memory that home was ever anywhere but at her side.').
card_multiverse_id('sower of temptation'/'LRW', '140165').

card_in_set('spellstutter sprite', 'LRW').
card_original_type('spellstutter sprite'/'LRW', 'Creature — Faerie Wizard').
card_original_text('spellstutter sprite'/'LRW', 'Flash\nFlying\nWhen Spellstutter Sprite comes into play, counter target spell with converted mana cost X or less, where X is the number of Faeries you control.').
card_image_name('spellstutter sprite'/'LRW', 'spellstutter sprite').
card_uid('spellstutter sprite'/'LRW', 'LRW:Spellstutter Sprite:spellstutter sprite').
card_rarity('spellstutter sprite'/'LRW', 'Common').
card_artist('spellstutter sprite'/'LRW', 'Rebecca Guay').
card_number('spellstutter sprite'/'LRW', '89').
card_multiverse_id('spellstutter sprite'/'LRW', '139429').

card_in_set('spiderwig boggart', 'LRW').
card_original_type('spiderwig boggart'/'LRW', 'Creature — Goblin Shaman').
card_original_text('spiderwig boggart'/'LRW', 'When Spiderwig Boggart comes into play, target creature gains fear until end of turn.').
card_first_print('spiderwig boggart', 'LRW').
card_image_name('spiderwig boggart'/'LRW', 'spiderwig boggart').
card_uid('spiderwig boggart'/'LRW', 'LRW:Spiderwig Boggart:spiderwig boggart').
card_rarity('spiderwig boggart'/'LRW', 'Common').
card_artist('spiderwig boggart'/'LRW', 'Larry MacDougall').
card_number('spiderwig boggart'/'LRW', '141').
card_flavor_text('spiderwig boggart'/'LRW', 'Auntie Flint was the first to pioneer the spiderwig, a mass of arachnids intended to be worn rather than eaten.').
card_multiverse_id('spiderwig boggart'/'LRW', '146445').

card_in_set('spinerock knoll', 'LRW').
card_original_type('spinerock knoll'/'LRW', 'Land').
card_original_text('spinerock knoll'/'LRW', 'Hideaway (This land comes into play tapped. When it does, look at the top four cards of your library, remove one from the game face down, then put the rest on the bottom of your library.)\n{T}: Add {R} to your mana pool.\n{R}, {T}: You may play the removed card without paying its mana cost if an opponent was dealt 7 or more damage this turn.').
card_first_print('spinerock knoll', 'LRW').
card_image_name('spinerock knoll'/'LRW', 'spinerock knoll').
card_uid('spinerock knoll'/'LRW', 'LRW:Spinerock Knoll:spinerock knoll').
card_rarity('spinerock knoll'/'LRW', 'Rare').
card_artist('spinerock knoll'/'LRW', 'Steve Prescott').
card_number('spinerock knoll'/'LRW', '274').
card_multiverse_id('spinerock knoll'/'LRW', '139514').

card_in_set('spring cleaning', 'LRW').
card_original_type('spring cleaning'/'LRW', 'Instant').
card_original_text('spring cleaning'/'LRW', 'Destroy target enchantment. Clash with an opponent. If you win, destroy all enchantments your opponents control. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_first_print('spring cleaning', 'LRW').
card_image_name('spring cleaning'/'LRW', 'spring cleaning').
card_uid('spring cleaning'/'LRW', 'LRW:Spring Cleaning:spring cleaning').
card_rarity('spring cleaning'/'LRW', 'Common').
card_artist('spring cleaning'/'LRW', 'Michael Sutfin').
card_number('spring cleaning'/'LRW', '236').
card_multiverse_id('spring cleaning'/'LRW', '139489').

card_in_set('springjack knight', 'LRW').
card_original_type('springjack knight'/'LRW', 'Creature — Kithkin Knight').
card_original_text('springjack knight'/'LRW', 'Whenever Springjack Knight attacks, clash with an opponent. If you win, target creature gains double strike until end of turn. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_first_print('springjack knight', 'LRW').
card_image_name('springjack knight'/'LRW', 'springjack knight').
card_uid('springjack knight'/'LRW', 'LRW:Springjack Knight:springjack knight').
card_rarity('springjack knight'/'LRW', 'Common').
card_artist('springjack knight'/'LRW', 'Steven Belledin').
card_number('springjack knight'/'LRW', '41').
card_multiverse_id('springjack knight'/'LRW', '143016').

card_in_set('springleaf drum', 'LRW').
card_original_type('springleaf drum'/'LRW', 'Artifact').
card_original_text('springleaf drum'/'LRW', '{T}, Tap an untapped creature you control: Add one mana of any color to your mana pool.').
card_first_print('springleaf drum', 'LRW').
card_image_name('springleaf drum'/'LRW', 'springleaf drum').
card_uid('springleaf drum'/'LRW', 'LRW:Springleaf Drum:springleaf drum').
card_rarity('springleaf drum'/'LRW', 'Common').
card_artist('springleaf drum'/'LRW', 'Cyril Van Der Haegen').
card_number('springleaf drum'/'LRW', '261').
card_flavor_text('springleaf drum'/'LRW', 'After trying mudskippers for an afternoon, Scratch decided that crickcarp made the best noise.').
card_multiverse_id('springleaf drum'/'LRW', '139509').

card_in_set('squeaking pie sneak', 'LRW').
card_original_type('squeaking pie sneak'/'LRW', 'Creature — Goblin Rogue').
card_original_text('squeaking pie sneak'/'LRW', 'As an additional cost to play Squeaking Pie Sneak, reveal a Goblin card from your hand or pay {3}.\nFear').
card_first_print('squeaking pie sneak', 'LRW').
card_image_name('squeaking pie sneak'/'LRW', 'squeaking pie sneak').
card_uid('squeaking pie sneak'/'LRW', 'LRW:Squeaking Pie Sneak:squeaking pie sneak').
card_rarity('squeaking pie sneak'/'LRW', 'Uncommon').
card_artist('squeaking pie sneak'/'LRW', 'Jeff Miracola').
card_number('squeaking pie sneak'/'LRW', '142').
card_flavor_text('squeaking pie sneak'/'LRW', '\"Why do our pies squeak? It\'s all the faeries, mice, and prickly-hogs we stuff inside.\"\n—Borb of the Squeaking Pie warren').
card_multiverse_id('squeaking pie sneak'/'LRW', '139710').

card_in_set('stinkdrinker daredevil', 'LRW').
card_original_type('stinkdrinker daredevil'/'LRW', 'Creature — Goblin Rogue').
card_original_text('stinkdrinker daredevil'/'LRW', 'Giant spells you play cost {2} less to play.').
card_first_print('stinkdrinker daredevil', 'LRW').
card_image_name('stinkdrinker daredevil'/'LRW', 'stinkdrinker daredevil').
card_uid('stinkdrinker daredevil'/'LRW', 'LRW:Stinkdrinker Daredevil:stinkdrinker daredevil').
card_rarity('stinkdrinker daredevil'/'LRW', 'Common').
card_artist('stinkdrinker daredevil'/'LRW', 'Pete Venters').
card_number('stinkdrinker daredevil'/'LRW', '191').
card_flavor_text('stinkdrinker daredevil'/'LRW', 'Boggarts constantly strive to outdo each other with the things they bring back to the warren, each hoping the exploit will become as well-known as those of Auntie Grub.').
card_multiverse_id('stinkdrinker daredevil'/'LRW', '143607').

card_in_set('stonybrook angler', 'LRW').
card_original_type('stonybrook angler'/'LRW', 'Creature — Merfolk Wizard').
card_original_text('stonybrook angler'/'LRW', '{1}{U}, {T}: You may tap or untap target creature.').
card_first_print('stonybrook angler', 'LRW').
card_image_name('stonybrook angler'/'LRW', 'stonybrook angler').
card_uid('stonybrook angler'/'LRW', 'LRW:Stonybrook Angler:stonybrook angler').
card_rarity('stonybrook angler'/'LRW', 'Common').
card_artist('stonybrook angler'/'LRW', 'Larry MacDougall').
card_number('stonybrook angler'/'LRW', '90').
card_flavor_text('stonybrook angler'/'LRW', '\"Water is in the air, the trees, and the earth. Understand its motion, speak its language, and the subtle currents that flow through all living things will fall under your command.\"').
card_multiverse_id('stonybrook angler'/'LRW', '142358').

card_in_set('streambed aquitects', 'LRW').
card_original_type('streambed aquitects'/'LRW', 'Creature — Merfolk Scout').
card_original_text('streambed aquitects'/'LRW', '{T}: Target Merfolk creature gets +1/+1 and gains islandwalk until end of turn.\n{T}: Target land becomes an Island until end of turn.').
card_first_print('streambed aquitects', 'LRW').
card_image_name('streambed aquitects'/'LRW', 'streambed aquitects').
card_uid('streambed aquitects'/'LRW', 'LRW:Streambed Aquitects:streambed aquitects').
card_rarity('streambed aquitects'/'LRW', 'Common').
card_artist('streambed aquitects'/'LRW', 'William O\'Connor').
card_number('streambed aquitects'/'LRW', '91').
card_flavor_text('streambed aquitects'/'LRW', '\"We look in the river and see scattered stones. A merrow looks and sees a map of Lorwyn.\"\n—Illulia, flamekin soulstoke').
card_multiverse_id('streambed aquitects'/'LRW', '139424').

card_in_set('summon the school', 'LRW').
card_original_type('summon the school'/'LRW', 'Tribal Sorcery — Merfolk').
card_original_text('summon the school'/'LRW', 'Put two 1/1 blue Merfolk Wizard creature tokens into play.\nTap four untapped Merfolk you control: Return Summon the School from your graveyard to your hand.').
card_first_print('summon the school', 'LRW').
card_image_name('summon the school'/'LRW', 'summon the school').
card_uid('summon the school'/'LRW', 'LRW:Summon the School:summon the school').
card_rarity('summon the school'/'LRW', 'Uncommon').
card_artist('summon the school'/'LRW', 'Dave Dorman').
card_number('summon the school'/'LRW', '42').
card_flavor_text('summon the school'/'LRW', '\"When merrows talk, listeners grow fins.\"\n—Kithkin saying').
card_multiverse_id('summon the school'/'LRW', '143684').

card_in_set('sunrise sovereign', 'LRW').
card_original_type('sunrise sovereign'/'LRW', 'Creature — Giant Warrior').
card_original_text('sunrise sovereign'/'LRW', 'Other Giant creatures you control get +2/+2 and have trample.').
card_first_print('sunrise sovereign', 'LRW').
card_image_name('sunrise sovereign'/'LRW', 'sunrise sovereign').
card_uid('sunrise sovereign'/'LRW', 'LRW:Sunrise Sovereign:sunrise sovereign').
card_rarity('sunrise sovereign'/'LRW', 'Rare').
card_artist('sunrise sovereign'/'LRW', 'William O\'Connor').
card_number('sunrise sovereign'/'LRW', '192').
card_flavor_text('sunrise sovereign'/'LRW', 'A hundred generations has he mentored, a hundred armies has he crushed beneath his feet, yet only a hundred words has he ever spoken, each more revered than a hundred books.').
card_multiverse_id('sunrise sovereign'/'LRW', '139667').

card_in_set('surge of thoughtweft', 'LRW').
card_original_type('surge of thoughtweft'/'LRW', 'Tribal Instant — Kithkin').
card_original_text('surge of thoughtweft'/'LRW', 'Creatures you control get +1/+1 until end of turn. If you control a Kithkin, draw a card.').
card_first_print('surge of thoughtweft', 'LRW').
card_image_name('surge of thoughtweft'/'LRW', 'surge of thoughtweft').
card_uid('surge of thoughtweft'/'LRW', 'LRW:Surge of Thoughtweft:surge of thoughtweft').
card_rarity('surge of thoughtweft'/'LRW', 'Common').
card_artist('surge of thoughtweft'/'LRW', 'Randy Gallegos').
card_number('surge of thoughtweft'/'LRW', '43').
card_flavor_text('surge of thoughtweft'/'LRW', 'Kithkin weave together their very thoughts, creating a depth of cooperation unknown to other races.').
card_multiverse_id('surge of thoughtweft'/'LRW', '145799').

card_in_set('surgespanner', 'LRW').
card_original_type('surgespanner'/'LRW', 'Creature — Merfolk Wizard').
card_original_text('surgespanner'/'LRW', 'Whenever Surgespanner becomes tapped, you may pay {1}{U}. If you do, return target permanent to its owner\'s hand.').
card_first_print('surgespanner', 'LRW').
card_image_name('surgespanner'/'LRW', 'surgespanner').
card_uid('surgespanner'/'LRW', 'LRW:Surgespanner:surgespanner').
card_rarity('surgespanner'/'LRW', 'Rare').
card_artist('surgespanner'/'LRW', 'Warren Mahy').
card_number('surgespanner'/'LRW', '92').
card_flavor_text('surgespanner'/'LRW', 'They ride on waves of Æther, washing out anything that might pollute the Merrow Lanes.').
card_multiverse_id('surgespanner'/'LRW', '141828').

card_in_set('swamp', 'LRW').
card_original_type('swamp'/'LRW', 'Basic Land — Swamp').
card_original_text('swamp'/'LRW', 'B').
card_image_name('swamp'/'LRW', 'swamp1').
card_uid('swamp'/'LRW', 'LRW:Swamp:swamp1').
card_rarity('swamp'/'LRW', 'Basic Land').
card_artist('swamp'/'LRW', 'Fred Fields').
card_number('swamp'/'LRW', '290').
card_multiverse_id('swamp'/'LRW', '143636').

card_in_set('swamp', 'LRW').
card_original_type('swamp'/'LRW', 'Basic Land — Swamp').
card_original_text('swamp'/'LRW', 'B').
card_image_name('swamp'/'LRW', 'swamp2').
card_uid('swamp'/'LRW', 'LRW:Swamp:swamp2').
card_rarity('swamp'/'LRW', 'Basic Land').
card_artist('swamp'/'LRW', 'Ron Spears').
card_number('swamp'/'LRW', '291').
card_multiverse_id('swamp'/'LRW', '143634').

card_in_set('swamp', 'LRW').
card_original_type('swamp'/'LRW', 'Basic Land — Swamp').
card_original_text('swamp'/'LRW', 'B').
card_image_name('swamp'/'LRW', 'swamp3').
card_uid('swamp'/'LRW', 'LRW:Swamp:swamp3').
card_rarity('swamp'/'LRW', 'Basic Land').
card_artist('swamp'/'LRW', 'Wayne Reynolds').
card_number('swamp'/'LRW', '292').
card_multiverse_id('swamp'/'LRW', '143629').

card_in_set('swamp', 'LRW').
card_original_type('swamp'/'LRW', 'Basic Land — Swamp').
card_original_text('swamp'/'LRW', 'B').
card_image_name('swamp'/'LRW', 'swamp4').
card_uid('swamp'/'LRW', 'LRW:Swamp:swamp4').
card_rarity('swamp'/'LRW', 'Basic Land').
card_artist('swamp'/'LRW', 'Steve Prescott').
card_number('swamp'/'LRW', '293').
card_multiverse_id('swamp'/'LRW', '143635').

card_in_set('sygg, river guide', 'LRW').
card_original_type('sygg, river guide'/'LRW', 'Legendary Creature — Merfolk Wizard').
card_original_text('sygg, river guide'/'LRW', 'Islandwalk\n{1}{W}: Target Merfolk you control gains protection from the color of your choice until end of turn.').
card_first_print('sygg, river guide', 'LRW').
card_image_name('sygg, river guide'/'LRW', 'sygg, river guide').
card_uid('sygg, river guide'/'LRW', 'LRW:Sygg, River Guide:sygg, river guide').
card_rarity('sygg, river guide'/'LRW', 'Rare').
card_artist('sygg, river guide'/'LRW', 'Larry MacDougall').
card_number('sygg, river guide'/'LRW', '251').
card_flavor_text('sygg, river guide'/'LRW', '\"If there\'s a place worth going, the Merrow Lanes already do. And if there\'s a route worth taking, yours truly already has.\"').
card_multiverse_id('sygg, river guide'/'LRW', '140172').

card_in_set('sylvan echoes', 'LRW').
card_original_type('sylvan echoes'/'LRW', 'Enchantment').
card_original_text('sylvan echoes'/'LRW', 'Whenever you clash and win, you may draw a card. (This ability triggers after the clash ends.)').
card_first_print('sylvan echoes', 'LRW').
card_image_name('sylvan echoes'/'LRW', 'sylvan echoes').
card_uid('sylvan echoes'/'LRW', 'LRW:Sylvan Echoes:sylvan echoes').
card_rarity('sylvan echoes'/'LRW', 'Uncommon').
card_artist('sylvan echoes'/'LRW', 'Rebecca Guay').
card_number('sylvan echoes'/'LRW', '237').
card_flavor_text('sylvan echoes'/'LRW', 'It takes a huntmaster\'s eye to discern the contours of mythical prey.').
card_multiverse_id('sylvan echoes'/'LRW', '146601').

card_in_set('tar pitcher', 'LRW').
card_original_type('tar pitcher'/'LRW', 'Creature — Goblin Shaman').
card_original_text('tar pitcher'/'LRW', '{T}, Sacrifice a Goblin: Tar Pitcher deals 2 damage to target creature or player.').
card_first_print('tar pitcher', 'LRW').
card_image_name('tar pitcher'/'LRW', 'tar pitcher').
card_uid('tar pitcher'/'LRW', 'LRW:Tar Pitcher:tar pitcher').
card_rarity('tar pitcher'/'LRW', 'Uncommon').
card_artist('tar pitcher'/'LRW', 'Omar Rayyan').
card_number('tar pitcher'/'LRW', '193').
card_flavor_text('tar pitcher'/'LRW', '\"Auntie Grub had caught a goat, a bleating doe that still squirmed in her arms. At the same time, her warren decided to share with her a sensory greeting of hot tar . . . .\"\n—A tale of Auntie Grub').
card_multiverse_id('tar pitcher'/'LRW', '139470').

card_in_set('tarfire', 'LRW').
card_original_type('tarfire'/'LRW', 'Tribal Instant — Goblin').
card_original_text('tarfire'/'LRW', 'Tarfire deals 2 damage to target creature or player.').
card_first_print('tarfire', 'LRW').
card_image_name('tarfire'/'LRW', 'tarfire').
card_uid('tarfire'/'LRW', 'LRW:Tarfire:tarfire').
card_rarity('tarfire'/'LRW', 'Common').
card_artist('tarfire'/'LRW', 'Omar Rayyan').
card_number('tarfire'/'LRW', '194').
card_flavor_text('tarfire'/'LRW', '\"After Auntie brushed the soot from her eyes, she discovered something wonderful: the fire had turned the goat into something that smelled delicious.\"\n—A tale of Auntie Grub').
card_multiverse_id('tarfire'/'LRW', '139476').

card_in_set('thieving sprite', 'LRW').
card_original_type('thieving sprite'/'LRW', 'Creature — Faerie Rogue').
card_original_text('thieving sprite'/'LRW', 'Flying\nWhen Thieving Sprite comes into play, target player reveals X cards from his or her hand, where X is the number of Faeries you control. You choose one of those cards. That player discards that card.').
card_first_print('thieving sprite', 'LRW').
card_image_name('thieving sprite'/'LRW', 'thieving sprite').
card_uid('thieving sprite'/'LRW', 'LRW:Thieving Sprite:thieving sprite').
card_rarity('thieving sprite'/'LRW', 'Common').
card_artist('thieving sprite'/'LRW', 'Dan Scott').
card_number('thieving sprite'/'LRW', '143').
card_multiverse_id('thieving sprite'/'LRW', '139452').

card_in_set('thorn of amethyst', 'LRW').
card_original_type('thorn of amethyst'/'LRW', 'Artifact').
card_original_text('thorn of amethyst'/'LRW', 'Noncreature spells cost {1} more to play.').
card_first_print('thorn of amethyst', 'LRW').
card_image_name('thorn of amethyst'/'LRW', 'thorn of amethyst').
card_uid('thorn of amethyst'/'LRW', 'LRW:Thorn of Amethyst:thorn of amethyst').
card_rarity('thorn of amethyst'/'LRW', 'Rare').
card_artist('thorn of amethyst'/'LRW', 'Chuck Lukacs').
card_number('thorn of amethyst'/'LRW', '262').
card_flavor_text('thorn of amethyst'/'LRW', 'Mined from a cave in the Dark Meanders, it shines brightest when no one is looking.').
card_multiverse_id('thorn of amethyst'/'LRW', '140166').

card_in_set('thorntooth witch', 'LRW').
card_original_type('thorntooth witch'/'LRW', 'Creature — Treefolk Shaman').
card_original_text('thorntooth witch'/'LRW', 'Whenever you play a Treefolk spell, you may have target creature get +3/-3 until end of turn.').
card_first_print('thorntooth witch', 'LRW').
card_image_name('thorntooth witch'/'LRW', 'thorntooth witch').
card_uid('thorntooth witch'/'LRW', 'LRW:Thorntooth Witch:thorntooth witch').
card_rarity('thorntooth witch'/'LRW', 'Uncommon').
card_artist('thorntooth witch'/'LRW', 'William O\'Connor').
card_number('thorntooth witch'/'LRW', '144').
card_flavor_text('thorntooth witch'/'LRW', 'The crone\'s boughs creaked as she spoke. \"You look peaked, little one. Come, sip from my warm brew. It\'ll have you blooming in no time.\"').
card_multiverse_id('thorntooth witch'/'LRW', '143015').

card_in_set('thoughtseize', 'LRW').
card_original_type('thoughtseize'/'LRW', 'Sorcery').
card_original_text('thoughtseize'/'LRW', 'Target player reveals his or her hand. You choose a nonland card from it. That player discards that card. You lose 2 life.').
card_first_print('thoughtseize', 'LRW').
card_image_name('thoughtseize'/'LRW', 'thoughtseize').
card_uid('thoughtseize'/'LRW', 'LRW:Thoughtseize:thoughtseize').
card_rarity('thoughtseize'/'LRW', 'Rare').
card_artist('thoughtseize'/'LRW', 'Aleksi Briclot').
card_number('thoughtseize'/'LRW', '145').
card_flavor_text('thoughtseize'/'LRW', '\"Any dream is a robust harvest. Still, I prefer the timeworn dreams, heavy with import, that haunt the obsessive mind.\"').
card_multiverse_id('thoughtseize'/'LRW', '145969').

card_in_set('thoughtweft trio', 'LRW').
card_original_type('thoughtweft trio'/'LRW', 'Creature — Kithkin Soldier').
card_original_text('thoughtweft trio'/'LRW', 'First strike, vigilance\nChampion a Kithkin (When this comes into play, sacrifice it unless you remove another Kithkin you control from the game. When this leaves play, that card returns to play.)\nThoughtweft Trio can block any number of creatures.').
card_first_print('thoughtweft trio', 'LRW').
card_image_name('thoughtweft trio'/'LRW', 'thoughtweft trio').
card_uid('thoughtweft trio'/'LRW', 'LRW:Thoughtweft Trio:thoughtweft trio').
card_rarity('thoughtweft trio'/'LRW', 'Rare').
card_artist('thoughtweft trio'/'LRW', 'Wayne Reynolds').
card_number('thoughtweft trio'/'LRW', '44').
card_multiverse_id('thoughtweft trio'/'LRW', '140232').

card_in_set('thousand-year elixir', 'LRW').
card_original_type('thousand-year elixir'/'LRW', 'Artifact').
card_original_text('thousand-year elixir'/'LRW', 'You may play the activated abilities of creatures you control as though those creatures had haste.\n{1}, {T}: Untap target creature.').
card_first_print('thousand-year elixir', 'LRW').
card_image_name('thousand-year elixir'/'LRW', 'thousand-year elixir').
card_uid('thousand-year elixir'/'LRW', 'LRW:Thousand-Year Elixir:thousand-year elixir').
card_rarity('thousand-year elixir'/'LRW', 'Rare').
card_artist('thousand-year elixir'/'LRW', 'Richard Sardinha').
card_number('thousand-year elixir'/'LRW', '263').
card_flavor_text('thousand-year elixir'/'LRW', 'Paradoxically, to tilt the massive jug for a sip, you\'d need the energy of the giant\'s tonic.').
card_multiverse_id('thousand-year elixir'/'LRW', '139505').

card_in_set('thundercloud shaman', 'LRW').
card_original_type('thundercloud shaman'/'LRW', 'Creature — Giant Shaman').
card_original_text('thundercloud shaman'/'LRW', 'When Thundercloud Shaman comes into play, it deals damage equal to the number of Giants you control to each non-Giant creature.').
card_first_print('thundercloud shaman', 'LRW').
card_image_name('thundercloud shaman'/'LRW', 'thundercloud shaman').
card_uid('thundercloud shaman'/'LRW', 'LRW:Thundercloud Shaman:thundercloud shaman').
card_rarity('thundercloud shaman'/'LRW', 'Uncommon').
card_artist('thundercloud shaman'/'LRW', 'Greg Staples').
card_number('thundercloud shaman'/'LRW', '195').
card_flavor_text('thundercloud shaman'/'LRW', 'He cares not for the disasters his storm brings as long as his path ahead is clear.').
card_multiverse_id('thundercloud shaman'/'LRW', '139738').

card_in_set('tideshaper mystic', 'LRW').
card_original_type('tideshaper mystic'/'LRW', 'Creature — Merfolk Wizard').
card_original_text('tideshaper mystic'/'LRW', '{T}: Target land becomes the basic land type of your choice until end of turn. Play this ability only during your turn.').
card_first_print('tideshaper mystic', 'LRW').
card_image_name('tideshaper mystic'/'LRW', 'tideshaper mystic').
card_uid('tideshaper mystic'/'LRW', 'LRW:Tideshaper Mystic:tideshaper mystic').
card_rarity('tideshaper mystic'/'LRW', 'Common').
card_artist('tideshaper mystic'/'LRW', 'Mark Tedin').
card_number('tideshaper mystic'/'LRW', '93').
card_flavor_text('tideshaper mystic'/'LRW', 'He paints with drop and shimmer a world that exists only in the wistful heart.').
card_multiverse_id('tideshaper mystic'/'LRW', '142362').

card_in_set('timber protector', 'LRW').
card_original_type('timber protector'/'LRW', 'Creature — Treefolk Warrior').
card_original_text('timber protector'/'LRW', 'Other Treefolk creatures you control get +1/+1.\nOther Treefolk and Forests you control are indestructible.').
card_first_print('timber protector', 'LRW').
card_image_name('timber protector'/'LRW', 'timber protector').
card_uid('timber protector'/'LRW', 'LRW:Timber Protector:timber protector').
card_rarity('timber protector'/'LRW', 'Rare').
card_artist('timber protector'/'LRW', 'Terese Nielsen & Philip Tan').
card_number('timber protector'/'LRW', '238').
card_flavor_text('timber protector'/'LRW', 'In his presence, an ordinary grove becomes a bastion to turn spells and break armies.').
card_multiverse_id('timber protector'/'LRW', '139690').

card_in_set('treefolk harbinger', 'LRW').
card_original_type('treefolk harbinger'/'LRW', 'Creature — Treefolk Druid').
card_original_text('treefolk harbinger'/'LRW', 'When Treefolk Harbinger comes into play, you may search your library for a Treefolk or Forest card, reveal it, then shuffle your library and put that card on top of it.').
card_first_print('treefolk harbinger', 'LRW').
card_image_name('treefolk harbinger'/'LRW', 'treefolk harbinger').
card_uid('treefolk harbinger'/'LRW', 'LRW:Treefolk Harbinger:treefolk harbinger').
card_rarity('treefolk harbinger'/'LRW', 'Uncommon').
card_artist('treefolk harbinger'/'LRW', 'Larry MacDougall').
card_number('treefolk harbinger'/'LRW', '239').
card_multiverse_id('treefolk harbinger'/'LRW', '139495').

card_in_set('triclopean sight', 'LRW').
card_original_type('triclopean sight'/'LRW', 'Enchantment — Aura').
card_original_text('triclopean sight'/'LRW', 'Flash\nEnchant creature\nWhen Triclopean Sight comes into play, untap enchanted creature.\nEnchanted creature gets +1/+1 and has vigilance.').
card_first_print('triclopean sight', 'LRW').
card_image_name('triclopean sight'/'LRW', 'triclopean sight').
card_uid('triclopean sight'/'LRW', 'LRW:Triclopean Sight:triclopean sight').
card_rarity('triclopean sight'/'LRW', 'Common').
card_artist('triclopean sight'/'LRW', 'Scott Hampton').
card_number('triclopean sight'/'LRW', '45').
card_multiverse_id('triclopean sight'/'LRW', '146580').

card_in_set('turtleshell changeling', 'LRW').
card_original_type('turtleshell changeling'/'LRW', 'Creature — Shapeshifter').
card_original_text('turtleshell changeling'/'LRW', 'Changeling (This card is every creature type at all times.)\n{1}{U}: Switch Turtleshell Changeling\'s power and toughness until end of turn.').
card_first_print('turtleshell changeling', 'LRW').
card_image_name('turtleshell changeling'/'LRW', 'turtleshell changeling').
card_uid('turtleshell changeling'/'LRW', 'LRW:Turtleshell Changeling:turtleshell changeling').
card_rarity('turtleshell changeling'/'LRW', 'Uncommon').
card_artist('turtleshell changeling'/'LRW', 'Ron Spencer').
card_number('turtleshell changeling'/'LRW', '94').
card_flavor_text('turtleshell changeling'/'LRW', 'A changeling involuntarily mimics the nearest being at hand, sometimes trading a borrowed shell for borrowed claws.').
card_multiverse_id('turtleshell changeling'/'LRW', '145807').

card_in_set('twinning glass', 'LRW').
card_original_type('twinning glass'/'LRW', 'Artifact').
card_original_text('twinning glass'/'LRW', '{1}, {T}: You may play a nonland card from your hand without paying its mana cost if it has the same name as a spell that was played this turn.').
card_first_print('twinning glass', 'LRW').
card_image_name('twinning glass'/'LRW', 'twinning glass').
card_uid('twinning glass'/'LRW', 'LRW:Twinning Glass:twinning glass').
card_rarity('twinning glass'/'LRW', 'Rare').
card_artist('twinning glass'/'LRW', 'Franz Vohwinkel').
card_number('twinning glass'/'LRW', '264').
card_flavor_text('twinning glass'/'LRW', 'It takes two to craft a mirror: a practiced metalsmith to silver one side and her own hazy reflection to polish the other.').
card_multiverse_id('twinning glass'/'LRW', '143375').

card_in_set('veteran of the depths', 'LRW').
card_original_type('veteran of the depths'/'LRW', 'Creature — Merfolk Soldier').
card_original_text('veteran of the depths'/'LRW', 'Whenever Veteran of the Depths becomes tapped, you may put a +1/+1 counter on it.').
card_first_print('veteran of the depths', 'LRW').
card_image_name('veteran of the depths'/'LRW', 'veteran of the depths').
card_uid('veteran of the depths'/'LRW', 'LRW:Veteran of the Depths:veteran of the depths').
card_rarity('veteran of the depths'/'LRW', 'Uncommon').
card_artist('veteran of the depths'/'LRW', 'Daren Bader').
card_number('veteran of the depths'/'LRW', '46').
card_flavor_text('veteran of the depths'/'LRW', 'In the backwaters of the Merrow Lanes lie stones scarred with tallies of countless generations, each representing a victory of merrow soldiers.').
card_multiverse_id('veteran of the depths'/'LRW', '139669').

card_in_set('vigor', 'LRW').
card_original_type('vigor'/'LRW', 'Creature — Elemental Incarnation').
card_original_text('vigor'/'LRW', 'Trample\nIf damage would be dealt to a creature you control other than Vigor, prevent that damage. Put a +1/+1 counter on that creature for each 1 damage prevented this way.\nWhen Vigor is put into a graveyard from anywhere, shuffle it into its owner\'s library.').
card_first_print('vigor', 'LRW').
card_image_name('vigor'/'LRW', 'vigor').
card_uid('vigor'/'LRW', 'LRW:Vigor:vigor').
card_rarity('vigor'/'LRW', 'Rare').
card_artist('vigor'/'LRW', 'Jim Murray').
card_number('vigor'/'LRW', '240').
card_multiverse_id('vigor'/'LRW', '140227').

card_in_set('vivid crag', 'LRW').
card_original_type('vivid crag'/'LRW', 'Land').
card_original_text('vivid crag'/'LRW', 'Vivid Crag comes into play tapped with two charge counters on it.\n{T}: Add {R} to your mana pool.\n{T}, Remove a charge counter from Vivid Crag: Add one mana of any color to your mana pool.').
card_first_print('vivid crag', 'LRW').
card_image_name('vivid crag'/'LRW', 'vivid crag').
card_uid('vivid crag'/'LRW', 'LRW:Vivid Crag:vivid crag').
card_rarity('vivid crag'/'LRW', 'Uncommon').
card_artist('vivid crag'/'LRW', 'Martina Pilcerova').
card_number('vivid crag'/'LRW', '275').
card_multiverse_id('vivid crag'/'LRW', '141882').

card_in_set('vivid creek', 'LRW').
card_original_type('vivid creek'/'LRW', 'Land').
card_original_text('vivid creek'/'LRW', 'Vivid Creek comes into play tapped with two charge counters on it.\n{T}: Add {U} to your mana pool.\n{T}, Remove a charge counter from Vivid Creek: Add one mana of any color to your mana pool.').
card_first_print('vivid creek', 'LRW').
card_image_name('vivid creek'/'LRW', 'vivid creek').
card_uid('vivid creek'/'LRW', 'LRW:Vivid Creek:vivid creek').
card_rarity('vivid creek'/'LRW', 'Uncommon').
card_artist('vivid creek'/'LRW', 'Fred Fields').
card_number('vivid creek'/'LRW', '276').
card_multiverse_id('vivid creek'/'LRW', '141881').

card_in_set('vivid grove', 'LRW').
card_original_type('vivid grove'/'LRW', 'Land').
card_original_text('vivid grove'/'LRW', 'Vivid Grove comes into play tapped with two charge counters on it.\n{T}: Add {G} to your mana pool.\n{T}, Remove a charge counter from Vivid Grove: Add one mana of any color to your mana pool.').
card_first_print('vivid grove', 'LRW').
card_image_name('vivid grove'/'LRW', 'vivid grove').
card_uid('vivid grove'/'LRW', 'LRW:Vivid Grove:vivid grove').
card_rarity('vivid grove'/'LRW', 'Uncommon').
card_artist('vivid grove'/'LRW', 'Howard Lyon').
card_number('vivid grove'/'LRW', '277').
card_multiverse_id('vivid grove'/'LRW', '141879').

card_in_set('vivid marsh', 'LRW').
card_original_type('vivid marsh'/'LRW', 'Land').
card_original_text('vivid marsh'/'LRW', 'Vivid Marsh comes into play tapped with two charge counters on it.\n{T}: Add {B} to your mana pool.\n{T}, Remove a charge counter from Vivid Marsh: Add one mana of any color to your mana pool.').
card_first_print('vivid marsh', 'LRW').
card_image_name('vivid marsh'/'LRW', 'vivid marsh').
card_uid('vivid marsh'/'LRW', 'LRW:Vivid Marsh:vivid marsh').
card_rarity('vivid marsh'/'LRW', 'Uncommon').
card_artist('vivid marsh'/'LRW', 'John Avon').
card_number('vivid marsh'/'LRW', '278').
card_multiverse_id('vivid marsh'/'LRW', '141880').

card_in_set('vivid meadow', 'LRW').
card_original_type('vivid meadow'/'LRW', 'Land').
card_original_text('vivid meadow'/'LRW', 'Vivid Meadow comes into play tapped with two charge counters on it.\n{T}: Add {W} to your mana pool.\n{T}, Remove a charge counter from Vivid Meadow: Add one mana of any color to your mana pool.').
card_first_print('vivid meadow', 'LRW').
card_image_name('vivid meadow'/'LRW', 'vivid meadow').
card_uid('vivid meadow'/'LRW', 'LRW:Vivid Meadow:vivid meadow').
card_rarity('vivid meadow'/'LRW', 'Uncommon').
card_artist('vivid meadow'/'LRW', 'Rob Alexander').
card_number('vivid meadow'/'LRW', '279').
card_multiverse_id('vivid meadow'/'LRW', '141883').

card_in_set('wanderer\'s twig', 'LRW').
card_original_type('wanderer\'s twig'/'LRW', 'Artifact').
card_original_text('wanderer\'s twig'/'LRW', '{1}, Sacrifice Wanderer\'s Twig: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.').
card_first_print('wanderer\'s twig', 'LRW').
card_image_name('wanderer\'s twig'/'LRW', 'wanderer\'s twig').
card_uid('wanderer\'s twig'/'LRW', 'LRW:Wanderer\'s Twig:wanderer\'s twig').
card_rarity('wanderer\'s twig'/'LRW', 'Common').
card_artist('wanderer\'s twig'/'LRW', 'Dave Dorman').
card_number('wanderer\'s twig'/'LRW', '265').
card_flavor_text('wanderer\'s twig'/'LRW', 'For every tree who falls, there are countless sprouts waiting to rise.').
card_multiverse_id('wanderer\'s twig'/'LRW', '139510').

card_in_set('wanderwine hub', 'LRW').
card_original_type('wanderwine hub'/'LRW', 'Land').
card_original_text('wanderwine hub'/'LRW', 'As Wanderwine Hub comes into play, you may reveal a Merfolk card from your hand. If you don\'t, Wanderwine Hub comes into play tapped.\n{T}: Add {W} or {U} to your mana pool.').
card_first_print('wanderwine hub', 'LRW').
card_image_name('wanderwine hub'/'LRW', 'wanderwine hub').
card_uid('wanderwine hub'/'LRW', 'LRW:Wanderwine Hub:wanderwine hub').
card_rarity('wanderwine hub'/'LRW', 'Rare').
card_artist('wanderwine hub'/'LRW', 'Warren Mahy').
card_number('wanderwine hub'/'LRW', '280').
card_flavor_text('wanderwine hub'/'LRW', 'Below the great river, a bustling hub channels the flow of merrow trade.').
card_multiverse_id('wanderwine hub'/'LRW', '153456').

card_in_set('wanderwine prophets', 'LRW').
card_original_type('wanderwine prophets'/'LRW', 'Creature — Merfolk Wizard').
card_original_text('wanderwine prophets'/'LRW', 'Champion a Merfolk (When this comes into play, sacrifice it unless you remove another Merfolk you control from the game. When this leaves play, that card returns to play.)\nWhenever Wanderwine Prophets deals combat damage to a player, you may sacrifice a Merfolk. If you do, take an extra turn after this one.').
card_first_print('wanderwine prophets', 'LRW').
card_image_name('wanderwine prophets'/'LRW', 'wanderwine prophets').
card_uid('wanderwine prophets'/'LRW', 'LRW:Wanderwine Prophets:wanderwine prophets').
card_rarity('wanderwine prophets'/'LRW', 'Rare').
card_artist('wanderwine prophets'/'LRW', 'Alex Horley-Orlandelli').
card_number('wanderwine prophets'/'LRW', '95').
card_multiverse_id('wanderwine prophets'/'LRW', '140177').

card_in_set('warren pilferers', 'LRW').
card_original_type('warren pilferers'/'LRW', 'Creature — Goblin Rogue').
card_original_text('warren pilferers'/'LRW', 'When Warren Pilferers comes into play, return target creature card from your graveyard to your hand. If that card is a Goblin card, Warren Pilferers gains haste until end of turn.').
card_first_print('warren pilferers', 'LRW').
card_image_name('warren pilferers'/'LRW', 'warren pilferers').
card_uid('warren pilferers'/'LRW', 'LRW:Warren Pilferers:warren pilferers').
card_rarity('warren pilferers'/'LRW', 'Common').
card_artist('warren pilferers'/'LRW', 'Wayne Reynolds').
card_number('warren pilferers'/'LRW', '146').
card_flavor_text('warren pilferers'/'LRW', '\"What do they need all this stuff for? They\'re dead. We\'re alive. Simple enough.\"').
card_multiverse_id('warren pilferers'/'LRW', '145967').

card_in_set('warren-scourge elf', 'LRW').
card_original_type('warren-scourge elf'/'LRW', 'Creature — Elf Warrior').
card_original_text('warren-scourge elf'/'LRW', 'Protection from Goblins').
card_first_print('warren-scourge elf', 'LRW').
card_image_name('warren-scourge elf'/'LRW', 'warren-scourge elf').
card_uid('warren-scourge elf'/'LRW', 'LRW:Warren-Scourge Elf:warren-scourge elf').
card_rarity('warren-scourge elf'/'LRW', 'Common').
card_artist('warren-scourge elf'/'LRW', 'Christopher Moeller').
card_number('warren-scourge elf'/'LRW', '241').
card_flavor_text('warren-scourge elf'/'LRW', '\"If one can associate any virtue with the eyeblights, it is the talent some achieve in disposing of them. I have seen the slaying of boggarts raised nearly to an art form.\"\n—Fiala, Gilt-Leaf winnower').
card_multiverse_id('warren-scourge elf'/'LRW', '139450').

card_in_set('weed strangle', 'LRW').
card_original_type('weed strangle'/'LRW', 'Sorcery').
card_original_text('weed strangle'/'LRW', 'Destroy target creature. Clash with an opponent. If you win, you gain life equal to that creature\'s toughness. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_first_print('weed strangle', 'LRW').
card_image_name('weed strangle'/'LRW', 'weed strangle').
card_uid('weed strangle'/'LRW', 'LRW:Weed Strangle:weed strangle').
card_rarity('weed strangle'/'LRW', 'Common').
card_artist('weed strangle'/'LRW', 'Jesper Ejsing').
card_number('weed strangle'/'LRW', '147').
card_multiverse_id('weed strangle'/'LRW', '145991').

card_in_set('wellgabber apothecary', 'LRW').
card_original_type('wellgabber apothecary'/'LRW', 'Creature — Merfolk Cleric').
card_original_text('wellgabber apothecary'/'LRW', '{1}{W}: Prevent all damage that would be dealt to target tapped Merfolk or Kithkin creature this turn.').
card_first_print('wellgabber apothecary', 'LRW').
card_image_name('wellgabber apothecary'/'LRW', 'wellgabber apothecary').
card_uid('wellgabber apothecary'/'LRW', 'LRW:Wellgabber Apothecary:wellgabber apothecary').
card_rarity('wellgabber apothecary'/'LRW', 'Common').
card_artist('wellgabber apothecary'/'LRW', 'Brandon Dorman').
card_number('wellgabber apothecary'/'LRW', '47').
card_flavor_text('wellgabber apothecary'/'LRW', '\"You\'ve discovered that boggarts bite, I see. And will the militia be chasing this lot? . . . Ah, you\'re staying in town to avoid Nath\'s hunt. Wise. Now this poultice . . .\"').
card_multiverse_id('wellgabber apothecary'/'LRW', '139400').

card_in_set('whirlpool whelm', 'LRW').
card_original_type('whirlpool whelm'/'LRW', 'Instant').
card_original_text('whirlpool whelm'/'LRW', 'Clash with an opponent, then return target creature to its owner\'s hand. If you win, you may put that creature on top of its owner\'s library instead. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_first_print('whirlpool whelm', 'LRW').
card_image_name('whirlpool whelm'/'LRW', 'whirlpool whelm').
card_uid('whirlpool whelm'/'LRW', 'LRW:Whirlpool Whelm:whirlpool whelm').
card_rarity('whirlpool whelm'/'LRW', 'Common').
card_artist('whirlpool whelm'/'LRW', 'Cyril Van Der Haegen').
card_number('whirlpool whelm'/'LRW', '96').
card_multiverse_id('whirlpool whelm'/'LRW', '145975').

card_in_set('wild ricochet', 'LRW').
card_original_type('wild ricochet'/'LRW', 'Instant').
card_original_text('wild ricochet'/'LRW', 'You may choose new targets for target instant or sorcery spell. Then copy that spell. You may choose new targets for the copy.').
card_first_print('wild ricochet', 'LRW').
card_image_name('wild ricochet'/'LRW', 'wild ricochet').
card_uid('wild ricochet'/'LRW', 'LRW:Wild Ricochet:wild ricochet').
card_rarity('wild ricochet'/'LRW', 'Rare').
card_artist('wild ricochet'/'LRW', 'Dan Scott').
card_number('wild ricochet'/'LRW', '196').
card_flavor_text('wild ricochet'/'LRW', '\"I knew that trick long before your great-grandmother\'s great-grandmother was born.\"').
card_multiverse_id('wild ricochet'/'LRW', '146600').

card_in_set('windbrisk heights', 'LRW').
card_original_type('windbrisk heights'/'LRW', 'Land').
card_original_text('windbrisk heights'/'LRW', 'Hideaway (This land comes into play tapped. When it does, look at the top four cards of your library, remove one from the game face down, then put the rest on the bottom of your library.)\n{T}: Add {W} to your mana pool.\n{W}, {T}: You may play the removed card without paying its mana cost if you attacked with three or more creatures this turn.').
card_first_print('windbrisk heights', 'LRW').
card_image_name('windbrisk heights'/'LRW', 'windbrisk heights').
card_uid('windbrisk heights'/'LRW', 'LRW:Windbrisk Heights:windbrisk heights').
card_rarity('windbrisk heights'/'LRW', 'Rare').
card_artist('windbrisk heights'/'LRW', 'Omar Rayyan').
card_number('windbrisk heights'/'LRW', '281').
card_multiverse_id('windbrisk heights'/'LRW', '145798').

card_in_set('wings of velis vel', 'LRW').
card_original_type('wings of velis vel'/'LRW', 'Tribal Instant — Shapeshifter').
card_original_text('wings of velis vel'/'LRW', 'Changeling (This card is every creature type at all times.)\nTarget creature becomes 4/4, gains all creature types, and gains flying until end of turn.').
card_first_print('wings of velis vel', 'LRW').
card_image_name('wings of velis vel'/'LRW', 'wings of velis vel').
card_uid('wings of velis vel'/'LRW', 'LRW:Wings of Velis Vel:wings of velis vel').
card_rarity('wings of velis vel'/'LRW', 'Common').
card_artist('wings of velis vel'/'LRW', 'Jim Pavelec').
card_number('wings of velis vel'/'LRW', '97').
card_flavor_text('wings of velis vel'/'LRW', 'Changeling magic grants unusual wishes.').
card_multiverse_id('wings of velis vel'/'LRW', '139678').

card_in_set('wispmare', 'LRW').
card_original_type('wispmare'/'LRW', 'Creature — Elemental').
card_original_text('wispmare'/'LRW', 'Flying\nWhen Wispmare comes into play, destroy target enchantment.\nEvoke {W} (You may play this spell for its evoke cost. If you do, it\'s sacrificed when it comes into play.)').
card_first_print('wispmare', 'LRW').
card_image_name('wispmare'/'LRW', 'wispmare').
card_uid('wispmare'/'LRW', 'LRW:Wispmare:wispmare').
card_rarity('wispmare'/'LRW', 'Common').
card_artist('wispmare'/'LRW', 'Eric Fortune').
card_number('wispmare'/'LRW', '48').
card_multiverse_id('wispmare'/'LRW', '145974').

card_in_set('wizened cenn', 'LRW').
card_original_type('wizened cenn'/'LRW', 'Creature — Kithkin Cleric').
card_original_text('wizened cenn'/'LRW', 'Other Kithkin creatures you control get +1/+1.').
card_first_print('wizened cenn', 'LRW').
card_image_name('wizened cenn'/'LRW', 'wizened cenn').
card_uid('wizened cenn'/'LRW', 'LRW:Wizened Cenn:wizened cenn').
card_rarity('wizened cenn'/'LRW', 'Uncommon').
card_artist('wizened cenn'/'LRW', 'Kev Walker').
card_number('wizened cenn'/'LRW', '49').
card_flavor_text('wizened cenn'/'LRW', '\"Thoughtweft binds us together as one, part of an intricate pattern that would unravel if even one thread came loose.\"').
card_multiverse_id('wizened cenn'/'LRW', '139716').

card_in_set('woodland changeling', 'LRW').
card_original_type('woodland changeling'/'LRW', 'Creature — Shapeshifter').
card_original_text('woodland changeling'/'LRW', 'Changeling (This card is every creature type at all times.)').
card_first_print('woodland changeling', 'LRW').
card_image_name('woodland changeling'/'LRW', 'woodland changeling').
card_uid('woodland changeling'/'LRW', 'LRW:Woodland Changeling:woodland changeling').
card_rarity('woodland changeling'/'LRW', 'Common').
card_artist('woodland changeling'/'LRW', 'Franz Vohwinkel').
card_number('woodland changeling'/'LRW', '242').
card_flavor_text('woodland changeling'/'LRW', 'Changelings cannot resist the draw of a new shape, even if doing so would be in their best interests.').
card_multiverse_id('woodland changeling'/'LRW', '145816').

card_in_set('woodland guidance', 'LRW').
card_original_type('woodland guidance'/'LRW', 'Sorcery').
card_original_text('woodland guidance'/'LRW', 'Return target card from your graveyard to your hand. Clash with an opponent. If you win, untap all Forests you control. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)\nRemove Woodland Guidance from the game.').
card_first_print('woodland guidance', 'LRW').
card_image_name('woodland guidance'/'LRW', 'woodland guidance').
card_uid('woodland guidance'/'LRW', 'LRW:Woodland Guidance:woodland guidance').
card_rarity('woodland guidance'/'LRW', 'Uncommon').
card_artist('woodland guidance'/'LRW', 'Richard Sardinha').
card_number('woodland guidance'/'LRW', '243').
card_multiverse_id('woodland guidance'/'LRW', '146594').

card_in_set('wort, boggart auntie', 'LRW').
card_original_type('wort, boggart auntie'/'LRW', 'Legendary Creature — Goblin Shaman').
card_original_text('wort, boggart auntie'/'LRW', 'Fear\nAt the beginning of your upkeep, you may return target Goblin card from your graveyard to your hand.').
card_first_print('wort, boggart auntie', 'LRW').
card_image_name('wort, boggart auntie'/'LRW', 'wort, boggart auntie').
card_uid('wort, boggart auntie'/'LRW', 'LRW:Wort, Boggart Auntie:wort, boggart auntie').
card_rarity('wort, boggart auntie'/'LRW', 'Rare').
card_artist('wort, boggart auntie'/'LRW', 'Larry MacDougall').
card_number('wort, boggart auntie'/'LRW', '252').
card_flavor_text('wort, boggart auntie'/'LRW', 'Auntie always knows which berries to lick, which kithkin to trick, and what to do when either goes wrong.').
card_multiverse_id('wort, boggart auntie'/'LRW', '140226').

card_in_set('wren\'s run packmaster', 'LRW').
card_original_type('wren\'s run packmaster'/'LRW', 'Creature — Elf Warrior').
card_original_text('wren\'s run packmaster'/'LRW', 'Champion an Elf (When this comes into play, sacrifice it unless you remove another Elf you control from the game. When this leaves play, that card returns to play.)\n{2}{G}: Put a 2/2 green Wolf creature token into play.\nEach Wolf you control has deathtouch. (When it deals damage to a creature, destroy that creature.)').
card_image_name('wren\'s run packmaster'/'LRW', 'wren\'s run packmaster').
card_uid('wren\'s run packmaster'/'LRW', 'LRW:Wren\'s Run Packmaster:wren\'s run packmaster').
card_rarity('wren\'s run packmaster'/'LRW', 'Rare').
card_artist('wren\'s run packmaster'/'LRW', 'Mark Zug').
card_number('wren\'s run packmaster'/'LRW', '244').
card_multiverse_id('wren\'s run packmaster'/'LRW', '140194').

card_in_set('wren\'s run vanquisher', 'LRW').
card_original_type('wren\'s run vanquisher'/'LRW', 'Creature — Elf Warrior').
card_original_text('wren\'s run vanquisher'/'LRW', 'As an additional cost to play Wren\'s Run Vanquisher, reveal an Elf card from your hand or pay {3}.\nDeathtouch (Whenever this creature deals damage to a creature, destroy that creature.)').
card_image_name('wren\'s run vanquisher'/'LRW', 'wren\'s run vanquisher').
card_uid('wren\'s run vanquisher'/'LRW', 'LRW:Wren\'s Run Vanquisher:wren\'s run vanquisher').
card_rarity('wren\'s run vanquisher'/'LRW', 'Uncommon').
card_artist('wren\'s run vanquisher'/'LRW', 'Paolo Parente').
card_number('wren\'s run vanquisher'/'LRW', '245').
card_multiverse_id('wren\'s run vanquisher'/'LRW', '139736').

card_in_set('wydwen, the biting gale', 'LRW').
card_original_type('wydwen, the biting gale'/'LRW', 'Legendary Creature — Faerie Wizard').
card_original_text('wydwen, the biting gale'/'LRW', 'Flash\nFlying\n{U}{B}, Pay 1 life: Return Wydwen, the Biting Gale to its owner\'s hand.').
card_first_print('wydwen, the biting gale', 'LRW').
card_image_name('wydwen, the biting gale'/'LRW', 'wydwen, the biting gale').
card_uid('wydwen, the biting gale'/'LRW', 'LRW:Wydwen, the Biting Gale:wydwen, the biting gale').
card_rarity('wydwen, the biting gale'/'LRW', 'Rare').
card_artist('wydwen, the biting gale'/'LRW', 'Matt Cavotta').
card_number('wydwen, the biting gale'/'LRW', '253').
card_flavor_text('wydwen, the biting gale'/'LRW', 'In a world of bright, cloudless skies, she is the dark storm on the horizon.').
card_multiverse_id('wydwen, the biting gale'/'LRW', '140229').

card_in_set('zephyr net', 'LRW').
card_original_type('zephyr net'/'LRW', 'Enchantment — Aura').
card_original_text('zephyr net'/'LRW', 'Enchant creature\nEnchanted creature has defender and flying.').
card_first_print('zephyr net', 'LRW').
card_image_name('zephyr net'/'LRW', 'zephyr net').
card_uid('zephyr net'/'LRW', 'LRW:Zephyr Net:zephyr net').
card_rarity('zephyr net'/'LRW', 'Common').
card_artist('zephyr net'/'LRW', 'Heather Hudson').
card_number('zephyr net'/'LRW', '98').
card_flavor_text('zephyr net'/'LRW', 'Faeries hang beings that interest them as ornaments in the sky, each clique competing to outshine the prize of the last.').
card_multiverse_id('zephyr net'/'LRW', '142357').
