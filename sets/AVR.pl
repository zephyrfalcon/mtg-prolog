% Avacyn Restored

set('AVR').
set_name('AVR', 'Avacyn Restored').
set_release_date('AVR', '2012-05-04').
set_border('AVR', 'black').
set_type('AVR', 'expansion').
set_block('AVR', 'Innistrad').

card_in_set('abundant growth', 'AVR').
card_original_type('abundant growth'/'AVR', 'Enchantment — Aura').
card_original_text('abundant growth'/'AVR', 'Enchant land\nWhen Abundant Growth enters the battlefield, draw a card.\nEnchanted land has \"{T}: Add one mana of any color to your mana pool.\"').
card_first_print('abundant growth', 'AVR').
card_image_name('abundant growth'/'AVR', 'abundant growth').
card_uid('abundant growth'/'AVR', 'AVR:Abundant Growth:abundant growth').
card_rarity('abundant growth'/'AVR', 'Common').
card_artist('abundant growth'/'AVR', 'Vincent Proce').
card_number('abundant growth'/'AVR', '167').
card_multiverse_id('abundant growth'/'AVR', '240017').

card_in_set('aggravate', 'AVR').
card_original_type('aggravate'/'AVR', 'Instant').
card_original_text('aggravate'/'AVR', 'Aggravate deals 1 damage to each creature target player controls. Each creature dealt damage this way attacks this turn if able.').
card_first_print('aggravate', 'AVR').
card_image_name('aggravate'/'AVR', 'aggravate').
card_uid('aggravate'/'AVR', 'AVR:Aggravate:aggravate').
card_rarity('aggravate'/'AVR', 'Uncommon').
card_artist('aggravate'/'AVR', 'Matt Stewart').
card_number('aggravate'/'AVR', '125').
card_flavor_text('aggravate'/'AVR', '\"Barbarians! They burned my favorite chair! We\'ll kill them all!\"\n—Anje Falkenrath').
card_multiverse_id('aggravate'/'AVR', '275707').

card_in_set('alchemist\'s apprentice', 'AVR').
card_original_type('alchemist\'s apprentice'/'AVR', 'Creature — Human Wizard').
card_original_text('alchemist\'s apprentice'/'AVR', 'Sacrifice Alchemist\'s Apprentice: Draw a card.').
card_first_print('alchemist\'s apprentice', 'AVR').
card_image_name('alchemist\'s apprentice'/'AVR', 'alchemist\'s apprentice').
card_uid('alchemist\'s apprentice'/'AVR', 'AVR:Alchemist\'s Apprentice:alchemist\'s apprentice').
card_rarity('alchemist\'s apprentice'/'AVR', 'Common').
card_artist('alchemist\'s apprentice'/'AVR', 'David Palumbo').
card_number('alchemist\'s apprentice'/'AVR', '42').
card_flavor_text('alchemist\'s apprentice'/'AVR', 'Side effects may include foul odors, scalding steam, and spontaneous nonexistence.').
card_multiverse_id('alchemist\'s apprentice'/'AVR', '240031').

card_in_set('alchemist\'s refuge', 'AVR').
card_original_type('alchemist\'s refuge'/'AVR', 'Land').
card_original_text('alchemist\'s refuge'/'AVR', '{T}: Add {1} to your mana pool.\n{G}{U}, {T}: You may cast nonland cards this turn as though they had flash.').
card_first_print('alchemist\'s refuge', 'AVR').
card_image_name('alchemist\'s refuge'/'AVR', 'alchemist\'s refuge').
card_uid('alchemist\'s refuge'/'AVR', 'AVR:Alchemist\'s Refuge:alchemist\'s refuge').
card_rarity('alchemist\'s refuge'/'AVR', 'Rare').
card_artist('alchemist\'s refuge'/'AVR', 'Dan Scott').
card_number('alchemist\'s refuge'/'AVR', '225').
card_flavor_text('alchemist\'s refuge'/'AVR', 'Persecuted alchemists fled deep into Kessig\'s wilds, seeking solitude to continue their ghoulish craft.').
card_multiverse_id('alchemist\'s refuge'/'AVR', '240194').

card_in_set('amass the components', 'AVR').
card_original_type('amass the components'/'AVR', 'Sorcery').
card_original_text('amass the components'/'AVR', 'Draw three cards, then put a card from your hand on the bottom of your library.').
card_first_print('amass the components', 'AVR').
card_image_name('amass the components'/'AVR', 'amass the components').
card_uid('amass the components'/'AVR', 'AVR:Amass the Components:amass the components').
card_rarity('amass the components'/'AVR', 'Common').
card_artist('amass the components'/'AVR', 'Matt Stewart').
card_number('amass the components'/'AVR', '43').
card_flavor_text('amass the components'/'AVR', '\"I knew I\'d find a use for all that newt spittle!\"').
card_multiverse_id('amass the components'/'AVR', '270988').

card_in_set('angel of glory\'s rise', 'AVR').
card_original_type('angel of glory\'s rise'/'AVR', 'Creature — Angel').
card_original_text('angel of glory\'s rise'/'AVR', 'Flying\nWhen Angel of Glory\'s Rise enters the battlefield, exile all Zombies, then return all Human creature cards from your graveyard to the battlefield.').
card_image_name('angel of glory\'s rise'/'AVR', 'angel of glory\'s rise').
card_uid('angel of glory\'s rise'/'AVR', 'AVR:Angel of Glory\'s Rise:angel of glory\'s rise').
card_rarity('angel of glory\'s rise'/'AVR', 'Rare').
card_artist('angel of glory\'s rise'/'AVR', 'James Ryman').
card_number('angel of glory\'s rise'/'AVR', '1').
card_flavor_text('angel of glory\'s rise'/'AVR', '\"Justice isn\'t done until undeath is undone.\"').
card_multiverse_id('angel of glory\'s rise'/'AVR', '239993').

card_in_set('angel of jubilation', 'AVR').
card_original_type('angel of jubilation'/'AVR', 'Creature — Angel').
card_original_text('angel of jubilation'/'AVR', 'Flying\nOther nonblack creatures you control get +1/+1.\nPlayers can\'t pay life or sacrifice creatures to cast spells or activate abilities.').
card_first_print('angel of jubilation', 'AVR').
card_image_name('angel of jubilation'/'AVR', 'angel of jubilation').
card_uid('angel of jubilation'/'AVR', 'AVR:Angel of Jubilation:angel of jubilation').
card_rarity('angel of jubilation'/'AVR', 'Rare').
card_artist('angel of jubilation'/'AVR', 'Terese Nielsen').
card_number('angel of jubilation'/'AVR', '2').
card_flavor_text('angel of jubilation'/'AVR', '\"Though Innistrad is not cleansed of evil, we finally have cause to rejoice.\"').
card_multiverse_id('angel of jubilation'/'AVR', '240120').

card_in_set('angel\'s mercy', 'AVR').
card_original_type('angel\'s mercy'/'AVR', 'Instant').
card_original_text('angel\'s mercy'/'AVR', 'You gain 7 life.').
card_image_name('angel\'s mercy'/'AVR', 'angel\'s mercy').
card_uid('angel\'s mercy'/'AVR', 'AVR:Angel\'s Mercy:angel\'s mercy').
card_rarity('angel\'s mercy'/'AVR', 'Common').
card_artist('angel\'s mercy'/'AVR', 'Greg Staples').
card_number('angel\'s mercy'/'AVR', '3').
card_flavor_text('angel\'s mercy'/'AVR', '\"I understand your fears. I pity your doubts. I absolve you of both.\"').
card_multiverse_id('angel\'s mercy'/'AVR', '240044').

card_in_set('angel\'s tomb', 'AVR').
card_original_type('angel\'s tomb'/'AVR', 'Artifact').
card_original_text('angel\'s tomb'/'AVR', 'Whenever a creature enters the battlefield under your control, you may have Angel\'s Tomb become a 3/3 white Angel artifact creature with flying until end of turn.').
card_first_print('angel\'s tomb', 'AVR').
card_image_name('angel\'s tomb'/'AVR', 'angel\'s tomb').
card_uid('angel\'s tomb'/'AVR', 'AVR:Angel\'s Tomb:angel\'s tomb').
card_rarity('angel\'s tomb'/'AVR', 'Uncommon').
card_artist('angel\'s tomb'/'AVR', 'Dan Scott').
card_number('angel\'s tomb'/'AVR', '211').
card_flavor_text('angel\'s tomb'/'AVR', '\"Faith can quicken the stones themselves with life.\"\n—Writings of Mikaeus').
card_multiverse_id('angel\'s tomb'/'AVR', '240059').

card_in_set('angelic armaments', 'AVR').
card_original_type('angelic armaments'/'AVR', 'Artifact — Equipment').
card_original_text('angelic armaments'/'AVR', 'Equipped creature gets +2/+2, has flying, and is a white Angel in addition to its other colors and types.\nEquip {4}').
card_first_print('angelic armaments', 'AVR').
card_image_name('angelic armaments'/'AVR', 'angelic armaments').
card_uid('angelic armaments'/'AVR', 'AVR:Angelic Armaments:angelic armaments').
card_rarity('angelic armaments'/'AVR', 'Uncommon').
card_artist('angelic armaments'/'AVR', 'Daniel Ljunggren').
card_number('angelic armaments'/'AVR', '212').
card_flavor_text('angelic armaments'/'AVR', 'Forged in dark hours from the flame that would not die.').
card_multiverse_id('angelic armaments'/'AVR', '240074').

card_in_set('angelic wall', 'AVR').
card_original_type('angelic wall'/'AVR', 'Creature — Wall').
card_original_text('angelic wall'/'AVR', 'Defender, flying').
card_image_name('angelic wall'/'AVR', 'angelic wall').
card_uid('angelic wall'/'AVR', 'AVR:Angelic Wall:angelic wall').
card_rarity('angelic wall'/'AVR', 'Common').
card_artist('angelic wall'/'AVR', 'Allen Williams').
card_number('angelic wall'/'AVR', '4').
card_flavor_text('angelic wall'/'AVR', '\"The air stirred as if fanned by angels\' wings, and the enemy was turned aside.\"\n—Tales of Ikarov the Voyager').
card_multiverse_id('angelic wall'/'AVR', '278065').

card_in_set('appetite for brains', 'AVR').
card_original_type('appetite for brains'/'AVR', 'Sorcery').
card_original_text('appetite for brains'/'AVR', 'Target opponent reveals his or her hand. You choose a card from it with converted mana cost 4 or greater and exile that card.').
card_first_print('appetite for brains', 'AVR').
card_image_name('appetite for brains'/'AVR', 'appetite for brains').
card_uid('appetite for brains'/'AVR', 'AVR:Appetite for Brains:appetite for brains').
card_rarity('appetite for brains'/'AVR', 'Uncommon').
card_artist('appetite for brains'/'AVR', 'Michael C. Hayes').
card_number('appetite for brains'/'AVR', '84').
card_flavor_text('appetite for brains'/'AVR', 'Just as with a peach, the first bite is always the juiciest.').
card_multiverse_id('appetite for brains'/'AVR', '279612').

card_in_set('arcane melee', 'AVR').
card_original_type('arcane melee'/'AVR', 'Enchantment').
card_original_text('arcane melee'/'AVR', 'Instant and sorcery spells cost {2} less to cast.').
card_first_print('arcane melee', 'AVR').
card_image_name('arcane melee'/'AVR', 'arcane melee').
card_uid('arcane melee'/'AVR', 'AVR:Arcane Melee:arcane melee').
card_rarity('arcane melee'/'AVR', 'Rare').
card_artist('arcane melee'/'AVR', 'Jaime Jones').
card_number('arcane melee'/'AVR', '44').
card_flavor_text('arcane melee'/'AVR', 'Debates between wizards are never purely academic.').
card_multiverse_id('arcane melee'/'AVR', '270993').

card_in_set('archangel', 'AVR').
card_original_type('archangel'/'AVR', 'Creature — Angel').
card_original_text('archangel'/'AVR', 'Flying, vigilance').
card_image_name('archangel'/'AVR', 'archangel').
card_uid('archangel'/'AVR', 'AVR:Archangel:archangel').
card_rarity('archangel'/'AVR', 'Uncommon').
card_artist('archangel'/'AVR', 'Cynthia Sheppard').
card_number('archangel'/'AVR', '5').
card_flavor_text('archangel'/'AVR', 'The sky rang with the cries of armored seraphs, and the darkness made a tactical retreat.').
card_multiverse_id('archangel'/'AVR', '240205').

card_in_set('archwing dragon', 'AVR').
card_original_type('archwing dragon'/'AVR', 'Creature — Dragon').
card_original_text('archwing dragon'/'AVR', 'Flying, haste\nAt the beginning of the end step, return Archwing Dragon to its owner\'s hand.').
card_first_print('archwing dragon', 'AVR').
card_image_name('archwing dragon'/'AVR', 'archwing dragon').
card_uid('archwing dragon'/'AVR', 'AVR:Archwing Dragon:archwing dragon').
card_rarity('archwing dragon'/'AVR', 'Rare').
card_artist('archwing dragon'/'AVR', 'Daarken').
card_number('archwing dragon'/'AVR', '126').
card_flavor_text('archwing dragon'/'AVR', 'Swifter than an angel, crueler than a demon, and relentless as a ghoul.').
card_multiverse_id('archwing dragon'/'AVR', '278063').

card_in_set('avacyn, angel of hope', 'AVR').
card_original_type('avacyn, angel of hope'/'AVR', 'Legendary Creature — Angel').
card_original_text('avacyn, angel of hope'/'AVR', 'Flying, vigilance\nAvacyn, Angel of Hope and other permanents you control are indestructible.').
card_first_print('avacyn, angel of hope', 'AVR').
card_image_name('avacyn, angel of hope'/'AVR', 'avacyn, angel of hope').
card_uid('avacyn, angel of hope'/'AVR', 'AVR:Avacyn, Angel of Hope:avacyn, angel of hope').
card_rarity('avacyn, angel of hope'/'AVR', 'Mythic Rare').
card_artist('avacyn, angel of hope'/'AVR', 'Jason Chan').
card_number('avacyn, angel of hope'/'AVR', '6').
card_flavor_text('avacyn, angel of hope'/'AVR', 'A golden helix streaked skyward from the Helvault. A thunderous explosion shattered the silver monolith and Avacyn emerged, free from her prison at last.').
card_multiverse_id('avacyn, angel of hope'/'AVR', '239961').

card_in_set('banishing stroke', 'AVR').
card_original_type('banishing stroke'/'AVR', 'Instant').
card_original_text('banishing stroke'/'AVR', 'Put target artifact, creature, or enchantment on the bottom of its owner\'s library.\nMiracle {W} (You may cast this card for its miracle cost when you draw it if it\'s the first card you drew this turn.)').
card_first_print('banishing stroke', 'AVR').
card_image_name('banishing stroke'/'AVR', 'banishing stroke').
card_uid('banishing stroke'/'AVR', 'AVR:Banishing Stroke:banishing stroke').
card_rarity('banishing stroke'/'AVR', 'Uncommon').
card_artist('banishing stroke'/'AVR', 'Igor Kieryluk').
card_number('banishing stroke'/'AVR', '7').
card_multiverse_id('banishing stroke'/'AVR', '240156').

card_in_set('banners raised', 'AVR').
card_original_type('banners raised'/'AVR', 'Instant').
card_original_text('banners raised'/'AVR', 'Creatures you control get +1/+0 until end of turn.').
card_first_print('banners raised', 'AVR').
card_image_name('banners raised'/'AVR', 'banners raised').
card_uid('banners raised'/'AVR', 'AVR:Banners Raised:banners raised').
card_rarity('banners raised'/'AVR', 'Common').
card_artist('banners raised'/'AVR', 'Mike Bierek').
card_number('banners raised'/'AVR', '127').
card_flavor_text('banners raised'/'AVR', 'After the destruction of the Helvault, fearful mobs soon became fearless battalions.').
card_multiverse_id('banners raised'/'AVR', '240014').

card_in_set('barter in blood', 'AVR').
card_original_type('barter in blood'/'AVR', 'Sorcery').
card_original_text('barter in blood'/'AVR', 'Each player sacrifices two creatures.').
card_image_name('barter in blood'/'AVR', 'barter in blood').
card_uid('barter in blood'/'AVR', 'AVR:Barter in Blood:barter in blood').
card_rarity('barter in blood'/'AVR', 'Uncommon').
card_artist('barter in blood'/'AVR', 'Eric Deschamps').
card_number('barter in blood'/'AVR', '85').
card_flavor_text('barter in blood'/'AVR', '\"We must all make sacrifices for the good of Innistrad.\"\n—Sorin Markov').
card_multiverse_id('barter in blood'/'AVR', '240010').

card_in_set('battle hymn', 'AVR').
card_original_type('battle hymn'/'AVR', 'Instant').
card_original_text('battle hymn'/'AVR', 'Add {R} to your mana pool for each creature you control.').
card_first_print('battle hymn', 'AVR').
card_image_name('battle hymn'/'AVR', 'battle hymn').
card_uid('battle hymn'/'AVR', 'AVR:Battle Hymn:battle hymn').
card_rarity('battle hymn'/'AVR', 'Common').
card_artist('battle hymn'/'AVR', 'Nils Hamm').
card_number('battle hymn'/'AVR', '128').
card_flavor_text('battle hymn'/'AVR', '\"A church is just wood and walls. A congregation is the faithful praising the angels.\"\n—Kolman, elder of Gatstaf').
card_multiverse_id('battle hymn'/'AVR', '270996').

card_in_set('bladed bracers', 'AVR').
card_original_type('bladed bracers'/'AVR', 'Artifact — Equipment').
card_original_text('bladed bracers'/'AVR', 'Equipped creature gets +1/+1.\nAs long as equipped creature is a Human or an Angel, it has vigilance.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('bladed bracers', 'AVR').
card_image_name('bladed bracers'/'AVR', 'bladed bracers').
card_uid('bladed bracers'/'AVR', 'AVR:Bladed Bracers:bladed bracers').
card_rarity('bladed bracers'/'AVR', 'Common').
card_artist('bladed bracers'/'AVR', 'Ryan Yee').
card_number('bladed bracers'/'AVR', '213').
card_flavor_text('bladed bracers'/'AVR', 'Forged from the rubble of the Helvault.').
card_multiverse_id('bladed bracers'/'AVR', '239965').

card_in_set('blessings of nature', 'AVR').
card_original_type('blessings of nature'/'AVR', 'Sorcery').
card_original_text('blessings of nature'/'AVR', 'Distribute four +1/+1 counters among any number of target creatures.\nMiracle {G} (You may cast this card for its miracle cost when you draw it if it\'s the first card you drew this turn.)').
card_first_print('blessings of nature', 'AVR').
card_image_name('blessings of nature'/'AVR', 'blessings of nature').
card_uid('blessings of nature'/'AVR', 'AVR:Blessings of Nature:blessings of nature').
card_rarity('blessings of nature'/'AVR', 'Uncommon').
card_artist('blessings of nature'/'AVR', 'Anthony Francisco').
card_number('blessings of nature'/'AVR', '168').
card_multiverse_id('blessings of nature'/'AVR', '240040').

card_in_set('blood artist', 'AVR').
card_original_type('blood artist'/'AVR', 'Creature — Vampire').
card_original_text('blood artist'/'AVR', 'Whenever Blood Artist or another creature dies, target player loses 1 life and you gain 1 life.').
card_first_print('blood artist', 'AVR').
card_image_name('blood artist'/'AVR', 'blood artist').
card_uid('blood artist'/'AVR', 'AVR:Blood Artist:blood artist').
card_rarity('blood artist'/'AVR', 'Uncommon').
card_artist('blood artist'/'AVR', 'Johannes Voss').
card_number('blood artist'/'AVR', '86').
card_flavor_text('blood artist'/'AVR', '\"Great art can never be created without great suffering.\"').
card_multiverse_id('blood artist'/'AVR', '240178').

card_in_set('bloodflow connoisseur', 'AVR').
card_original_type('bloodflow connoisseur'/'AVR', 'Creature — Vampire').
card_original_text('bloodflow connoisseur'/'AVR', 'Sacrifice a creature: Put a +1/+1 counter on Bloodflow Connoisseur.').
card_first_print('bloodflow connoisseur', 'AVR').
card_image_name('bloodflow connoisseur'/'AVR', 'bloodflow connoisseur').
card_uid('bloodflow connoisseur'/'AVR', 'AVR:Bloodflow Connoisseur:bloodflow connoisseur').
card_rarity('bloodflow connoisseur'/'AVR', 'Common').
card_artist('bloodflow connoisseur'/'AVR', 'Slawomir Maniak').
card_number('bloodflow connoisseur'/'AVR', '87').
card_flavor_text('bloodflow connoisseur'/'AVR', '\"Death not for survival but for vanity and pleasure? This is the decadence I sought to curb.\"\n—Sorin Markov').
card_multiverse_id('bloodflow connoisseur'/'AVR', '240196').

card_in_set('bone splinters', 'AVR').
card_original_type('bone splinters'/'AVR', 'Sorcery').
card_original_text('bone splinters'/'AVR', 'As an additional cost to cast Bone Splinters, sacrifice a creature.\nDestroy target creature.').
card_image_name('bone splinters'/'AVR', 'bone splinters').
card_uid('bone splinters'/'AVR', 'AVR:Bone Splinters:bone splinters').
card_rarity('bone splinters'/'AVR', 'Common').
card_artist('bone splinters'/'AVR', 'Nils Hamm').
card_number('bone splinters'/'AVR', '88').
card_flavor_text('bone splinters'/'AVR', '\"Which do you deserve, the heft of a femur or the graceful sharpness of a forearm? Decisions, decisions . . .\"\n—Gisa the Mad').
card_multiverse_id('bone splinters'/'AVR', '239983').

card_in_set('bonfire of the damned', 'AVR').
card_original_type('bonfire of the damned'/'AVR', 'Sorcery').
card_original_text('bonfire of the damned'/'AVR', 'Bonfire of the Damned deals X damage to target player and each creature he or she controls.\nMiracle {X}{R} (You may cast this card for its miracle cost when you draw it if it\'s the first card you drew this turn.)').
card_first_print('bonfire of the damned', 'AVR').
card_image_name('bonfire of the damned'/'AVR', 'bonfire of the damned').
card_uid('bonfire of the damned'/'AVR', 'AVR:Bonfire of the Damned:bonfire of the damned').
card_rarity('bonfire of the damned'/'AVR', 'Mythic Rare').
card_artist('bonfire of the damned'/'AVR', 'James Paick').
card_number('bonfire of the damned'/'AVR', '129').
card_multiverse_id('bonfire of the damned'/'AVR', '271095').

card_in_set('borderland ranger', 'AVR').
card_original_type('borderland ranger'/'AVR', 'Creature — Human Scout').
card_original_text('borderland ranger'/'AVR', 'When Borderland Ranger enters the battlefield, you may search your library for a basic land card, reveal it, and put it into your hand. If you do, shuffle your library.').
card_image_name('borderland ranger'/'AVR', 'borderland ranger').
card_uid('borderland ranger'/'AVR', 'AVR:Borderland Ranger:borderland ranger').
card_rarity('borderland ranger'/'AVR', 'Common').
card_artist('borderland ranger'/'AVR', 'Zoltan Boros').
card_number('borderland ranger'/'AVR', '169').
card_flavor_text('borderland ranger'/'AVR', '\"There\'s only one route from Kessig into Stensia. Unless you come with me.\"').
card_multiverse_id('borderland ranger'/'AVR', '239964').

card_in_set('bower passage', 'AVR').
card_original_type('bower passage'/'AVR', 'Enchantment').
card_original_text('bower passage'/'AVR', 'Creatures with flying can\'t block creatures you control.').
card_first_print('bower passage', 'AVR').
card_image_name('bower passage'/'AVR', 'bower passage').
card_uid('bower passage'/'AVR', 'AVR:Bower Passage:bower passage').
card_rarity('bower passage'/'AVR', 'Uncommon').
card_artist('bower passage'/'AVR', 'Cliff Childs').
card_number('bower passage'/'AVR', '170').
card_flavor_text('bower passage'/'AVR', 'In the darkest hours, some denied Avacyn. Now the Kessig wilds are their only haven.').
card_multiverse_id('bower passage'/'AVR', '278255').

card_in_set('bruna, light of alabaster', 'AVR').
card_original_type('bruna, light of alabaster'/'AVR', 'Legendary Creature — Angel').
card_original_text('bruna, light of alabaster'/'AVR', 'Flying, vigilance\nWhenever Bruna, Light of Alabaster attacks or blocks, you may attach to it any number of Auras on the battlefield and you may put onto the battlefield attached to it any number of Aura cards that could enchant it from your graveyard and/or hand.').
card_first_print('bruna, light of alabaster', 'AVR').
card_image_name('bruna, light of alabaster'/'AVR', 'bruna, light of alabaster').
card_uid('bruna, light of alabaster'/'AVR', 'AVR:Bruna, Light of Alabaster:bruna, light of alabaster').
card_rarity('bruna, light of alabaster'/'AVR', 'Mythic Rare').
card_artist('bruna, light of alabaster'/'AVR', 'Winona Nelson').
card_number('bruna, light of alabaster'/'AVR', '208').
card_multiverse_id('bruna, light of alabaster'/'AVR', '240208').

card_in_set('builder\'s blessing', 'AVR').
card_original_type('builder\'s blessing'/'AVR', 'Enchantment').
card_original_text('builder\'s blessing'/'AVR', 'Untapped creatures you control get +0/+2.').
card_first_print('builder\'s blessing', 'AVR').
card_image_name('builder\'s blessing'/'AVR', 'builder\'s blessing').
card_uid('builder\'s blessing'/'AVR', 'AVR:Builder\'s Blessing:builder\'s blessing').
card_rarity('builder\'s blessing'/'AVR', 'Uncommon').
card_artist('builder\'s blessing'/'AVR', 'John Stanko').
card_number('builder\'s blessing'/'AVR', '8').
card_flavor_text('builder\'s blessing'/'AVR', '\"Mix the mortar with holy wards, or blood will run in the streets.\"\n—Vadvar, Thraben stonewright').
card_multiverse_id('builder\'s blessing'/'AVR', '239990').

card_in_set('burn at the stake', 'AVR').
card_original_type('burn at the stake'/'AVR', 'Sorcery').
card_original_text('burn at the stake'/'AVR', 'As an additional cost to cast Burn at the Stake, tap any number of untapped creatures you control.\nBurn at the Stake deals damage to target creature or player equal to three times the number of creatures tapped this way.').
card_first_print('burn at the stake', 'AVR').
card_image_name('burn at the stake'/'AVR', 'burn at the stake').
card_uid('burn at the stake'/'AVR', 'AVR:Burn at the Stake:burn at the stake').
card_rarity('burn at the stake'/'AVR', 'Rare').
card_artist('burn at the stake'/'AVR', 'Zoltan Boros').
card_number('burn at the stake'/'AVR', '130').
card_multiverse_id('burn at the stake'/'AVR', '271120').

card_in_set('butcher ghoul', 'AVR').
card_original_type('butcher ghoul'/'AVR', 'Creature — Zombie').
card_original_text('butcher ghoul'/'AVR', 'Undying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_first_print('butcher ghoul', 'AVR').
card_image_name('butcher ghoul'/'AVR', 'butcher ghoul').
card_uid('butcher ghoul'/'AVR', 'AVR:Butcher Ghoul:butcher ghoul').
card_rarity('butcher ghoul'/'AVR', 'Common').
card_artist('butcher ghoul'/'AVR', 'Christopher Moeller').
card_number('butcher ghoul'/'AVR', '89').
card_flavor_text('butcher ghoul'/'AVR', 'Without a mind, it doesn\'t fear death. Without a soul, it doesn\'t mind killing.').
card_multiverse_id('butcher ghoul'/'AVR', '271115').

card_in_set('call to serve', 'AVR').
card_original_type('call to serve'/'AVR', 'Enchantment — Aura').
card_original_text('call to serve'/'AVR', 'Enchant nonblack creature\nEnchanted creature gets +1/+2, has flying, and is an Angel in addition to its other types.').
card_first_print('call to serve', 'AVR').
card_image_name('call to serve'/'AVR', 'call to serve').
card_uid('call to serve'/'AVR', 'AVR:Call to Serve:call to serve').
card_rarity('call to serve'/'AVR', 'Common').
card_artist('call to serve'/'AVR', 'Jaime Jones').
card_number('call to serve'/'AVR', '9').
card_flavor_text('call to serve'/'AVR', 'Grace is a gift, never a reward.').
card_multiverse_id('call to serve'/'AVR', '240081').

card_in_set('captain of the mists', 'AVR').
card_original_type('captain of the mists'/'AVR', 'Creature — Human Wizard').
card_original_text('captain of the mists'/'AVR', 'Whenever another Human enters the battlefield under your control, untap Captain of the Mists.\n{1}{U}, {T}: You may tap or untap target permanent.').
card_first_print('captain of the mists', 'AVR').
card_image_name('captain of the mists'/'AVR', 'captain of the mists').
card_uid('captain of the mists'/'AVR', 'AVR:Captain of the Mists:captain of the mists').
card_rarity('captain of the mists'/'AVR', 'Rare').
card_artist('captain of the mists'/'AVR', 'Allen Williams').
card_number('captain of the mists'/'AVR', '45').
card_flavor_text('captain of the mists'/'AVR', '\"I am no mere ship\'s captain. The north wind is my accomplice. The tide is my first mate.\"').
card_multiverse_id('captain of the mists'/'AVR', '275713').

card_in_set('cathars\' crusade', 'AVR').
card_original_type('cathars\' crusade'/'AVR', 'Enchantment').
card_original_text('cathars\' crusade'/'AVR', 'Whenever a creature enters the battlefield under your control, put a +1/+1 counter on each creature you control.').
card_first_print('cathars\' crusade', 'AVR').
card_image_name('cathars\' crusade'/'AVR', 'cathars\' crusade').
card_uid('cathars\' crusade'/'AVR', 'AVR:Cathars\' Crusade:cathars\' crusade').
card_rarity('cathars\' crusade'/'AVR', 'Rare').
card_artist('cathars\' crusade'/'AVR', 'Karl Kopinski').
card_number('cathars\' crusade'/'AVR', '10').
card_flavor_text('cathars\' crusade'/'AVR', 'Avacyn\'s holy warriors kept hope alive in her darkest hours. Now they will carry that hope across Innistrad.').
card_multiverse_id('cathars\' crusade'/'AVR', '276198').

card_in_set('cathedral sanctifier', 'AVR').
card_original_type('cathedral sanctifier'/'AVR', 'Creature — Human Cleric').
card_original_text('cathedral sanctifier'/'AVR', 'When Cathedral Sanctifier enters the battlefield, you gain 3 life.').
card_first_print('cathedral sanctifier', 'AVR').
card_image_name('cathedral sanctifier'/'AVR', 'cathedral sanctifier').
card_uid('cathedral sanctifier'/'AVR', 'AVR:Cathedral Sanctifier:cathedral sanctifier').
card_rarity('cathedral sanctifier'/'AVR', 'Common').
card_artist('cathedral sanctifier'/'AVR', 'Michael C. Hayes').
card_number('cathedral sanctifier'/'AVR', '11').
card_flavor_text('cathedral sanctifier'/'AVR', '\"Evil will soon be vanquished. What Innistrad most needs now is healing.\"').
card_multiverse_id('cathedral sanctifier'/'AVR', '240136').

card_in_set('cavern of souls', 'AVR').
card_original_type('cavern of souls'/'AVR', 'Land').
card_original_text('cavern of souls'/'AVR', 'As Cavern of Souls enters the battlefield, choose a creature type.\n{T}: Add {1} to your mana pool.\n{T}: Add one mana of any color to your mana pool. Spend this mana only to cast a creature spell of the chosen type, and that spell can\'t be countered.').
card_first_print('cavern of souls', 'AVR').
card_image_name('cavern of souls'/'AVR', 'cavern of souls').
card_uid('cavern of souls'/'AVR', 'AVR:Cavern of Souls:cavern of souls').
card_rarity('cavern of souls'/'AVR', 'Rare').
card_artist('cavern of souls'/'AVR', 'Cliff Childs').
card_number('cavern of souls'/'AVR', '226').
card_multiverse_id('cavern of souls'/'AVR', '278058').

card_in_set('champion of lambholt', 'AVR').
card_original_type('champion of lambholt'/'AVR', 'Creature — Human Warrior').
card_original_text('champion of lambholt'/'AVR', 'Creatures with power less than Champion of Lambholt\'s power can\'t block creatures you control.\nWhenever another creature enters the battlefield under your control, put a +1/+1 counter on Champion of Lambholt.').
card_first_print('champion of lambholt', 'AVR').
card_image_name('champion of lambholt'/'AVR', 'champion of lambholt').
card_uid('champion of lambholt'/'AVR', 'AVR:Champion of Lambholt:champion of lambholt').
card_rarity('champion of lambholt'/'AVR', 'Rare').
card_artist('champion of lambholt'/'AVR', 'Christopher Moeller').
card_number('champion of lambholt'/'AVR', '171').
card_multiverse_id('champion of lambholt'/'AVR', '279608').

card_in_set('cloudshift', 'AVR').
card_original_type('cloudshift'/'AVR', 'Instant').
card_original_text('cloudshift'/'AVR', 'Exile target creature you control, then return that card to the battlefield under your control.').
card_first_print('cloudshift', 'AVR').
card_image_name('cloudshift'/'AVR', 'cloudshift').
card_uid('cloudshift'/'AVR', 'AVR:Cloudshift:cloudshift').
card_rarity('cloudshift'/'AVR', 'Common').
card_artist('cloudshift'/'AVR', 'Howard Lyon').
card_number('cloudshift'/'AVR', '12').
card_flavor_text('cloudshift'/'AVR', '\"Even storm clouds bow to worship Avacyn.\"\n—Elder Rimheit').
card_multiverse_id('cloudshift'/'AVR', '240006').

card_in_set('commander\'s authority', 'AVR').
card_original_type('commander\'s authority'/'AVR', 'Enchantment — Aura').
card_original_text('commander\'s authority'/'AVR', 'Enchant creature\nEnchanted creature has \"At the beginning of your upkeep, put a 1/1 white Human creature token onto the battlefield.\"').
card_first_print('commander\'s authority', 'AVR').
card_image_name('commander\'s authority'/'AVR', 'commander\'s authority').
card_uid('commander\'s authority'/'AVR', 'AVR:Commander\'s Authority:commander\'s authority').
card_rarity('commander\'s authority'/'AVR', 'Uncommon').
card_artist('commander\'s authority'/'AVR', 'Johannes Voss').
card_number('commander\'s authority'/'AVR', '13').
card_flavor_text('commander\'s authority'/'AVR', '\"Wear her symbol with honor, a sign of faith upheld and honest deeds bravely done.\"').
card_multiverse_id('commander\'s authority'/'AVR', '278252').

card_in_set('conjurer\'s closet', 'AVR').
card_original_type('conjurer\'s closet'/'AVR', 'Artifact').
card_original_text('conjurer\'s closet'/'AVR', 'At the beginning of your end step, you may exile target creature you control, then return that card to the battlefield under your control.').
card_first_print('conjurer\'s closet', 'AVR').
card_image_name('conjurer\'s closet'/'AVR', 'conjurer\'s closet').
card_uid('conjurer\'s closet'/'AVR', 'AVR:Conjurer\'s Closet:conjurer\'s closet').
card_rarity('conjurer\'s closet'/'AVR', 'Rare').
card_artist('conjurer\'s closet'/'AVR', 'Jason Felix').
card_number('conjurer\'s closet'/'AVR', '214').
card_flavor_text('conjurer\'s closet'/'AVR', '\"Tomorrow wears yesterday\'s face.\"\n—Kordel the Cryptic').
card_multiverse_id('conjurer\'s closet'/'AVR', '240030').

card_in_set('corpse traders', 'AVR').
card_original_type('corpse traders'/'AVR', 'Creature — Human Rogue').
card_original_text('corpse traders'/'AVR', '{2}{B}, Sacrifice a creature: Target opponent reveals his or her hand. You choose a card from it. That player discards that card. Activate this ability only any time you could cast a sorcery.').
card_first_print('corpse traders', 'AVR').
card_image_name('corpse traders'/'AVR', 'corpse traders').
card_uid('corpse traders'/'AVR', 'AVR:Corpse Traders:corpse traders').
card_rarity('corpse traders'/'AVR', 'Uncommon').
card_artist('corpse traders'/'AVR', 'Kev Walker').
card_number('corpse traders'/'AVR', '90').
card_flavor_text('corpse traders'/'AVR', 'Those without breath can\'t complain.').
card_multiverse_id('corpse traders'/'AVR', '239971').

card_in_set('craterhoof behemoth', 'AVR').
card_original_type('craterhoof behemoth'/'AVR', 'Creature — Beast').
card_original_text('craterhoof behemoth'/'AVR', 'Haste\nWhen Craterhoof Behemoth enters the battlefield, creatures you control gain trample and get +X/+X until end of turn, where X is the number of creatures you control.').
card_first_print('craterhoof behemoth', 'AVR').
card_image_name('craterhoof behemoth'/'AVR', 'craterhoof behemoth').
card_uid('craterhoof behemoth'/'AVR', 'AVR:Craterhoof Behemoth:craterhoof behemoth').
card_rarity('craterhoof behemoth'/'AVR', 'Mythic Rare').
card_artist('craterhoof behemoth'/'AVR', 'Chris Rahn').
card_number('craterhoof behemoth'/'AVR', '172').
card_flavor_text('craterhoof behemoth'/'AVR', 'Its footsteps of today are the lakes of tomorrow.').
card_multiverse_id('craterhoof behemoth'/'AVR', '240027').

card_in_set('crippling chill', 'AVR').
card_original_type('crippling chill'/'AVR', 'Instant').
card_original_text('crippling chill'/'AVR', 'Tap target creature. It doesn\'t untap during its controller\'s next untap step.\nDraw a card.').
card_first_print('crippling chill', 'AVR').
card_image_name('crippling chill'/'AVR', 'crippling chill').
card_uid('crippling chill'/'AVR', 'AVR:Crippling Chill:crippling chill').
card_rarity('crippling chill'/'AVR', 'Common').
card_artist('crippling chill'/'AVR', 'Svetlin Velinov').
card_number('crippling chill'/'AVR', '46').
card_flavor_text('crippling chill'/'AVR', 'One breath of the geist turns veins to rivers of ice and freezes hearts midbeat.').
card_multiverse_id('crippling chill'/'AVR', '240042').

card_in_set('crypt creeper', 'AVR').
card_original_type('crypt creeper'/'AVR', 'Creature — Zombie').
card_original_text('crypt creeper'/'AVR', 'Sacrifice Crypt Creeper: Exile target card from a graveyard.').
card_image_name('crypt creeper'/'AVR', 'crypt creeper').
card_uid('crypt creeper'/'AVR', 'AVR:Crypt Creeper:crypt creeper').
card_rarity('crypt creeper'/'AVR', 'Common').
card_artist('crypt creeper'/'AVR', 'Scott Chou').
card_number('crypt creeper'/'AVR', '91').
card_flavor_text('crypt creeper'/'AVR', '\"I guess what they say is true. What doesn\'t kill you makes you stronger. Or at least stranger.\"\n—Captain Eberhart').
card_multiverse_id('crypt creeper'/'AVR', '240104').

card_in_set('cursebreak', 'AVR').
card_original_type('cursebreak'/'AVR', 'Instant').
card_original_text('cursebreak'/'AVR', 'Destroy target enchantment. You gain 2 life.').
card_first_print('cursebreak', 'AVR').
card_image_name('cursebreak'/'AVR', 'cursebreak').
card_uid('cursebreak'/'AVR', 'AVR:Cursebreak:cursebreak').
card_rarity('cursebreak'/'AVR', 'Common').
card_artist('cursebreak'/'AVR', 'Sam Wolfe Connelly').
card_number('cursebreak'/'AVR', '14').
card_flavor_text('cursebreak'/'AVR', 'No longer the village pariah. No longer taunted and shamed. Sigrun was finally free.').
card_multiverse_id('cursebreak'/'AVR', '240124').

card_in_set('dangerous wager', 'AVR').
card_original_type('dangerous wager'/'AVR', 'Instant').
card_original_text('dangerous wager'/'AVR', 'Discard your hand, then draw two cards.').
card_first_print('dangerous wager', 'AVR').
card_image_name('dangerous wager'/'AVR', 'dangerous wager').
card_uid('dangerous wager'/'AVR', 'AVR:Dangerous Wager:dangerous wager').
card_rarity('dangerous wager'/'AVR', 'Common').
card_artist('dangerous wager'/'AVR', 'Drew Baker').
card_number('dangerous wager'/'AVR', '131').
card_flavor_text('dangerous wager'/'AVR', '\"C\'mon friend, take a turn tossing the knucklebones. What\'ve you got to lose?\"\n—Tobias, trader of Erdwal').
card_multiverse_id('dangerous wager'/'AVR', '240202').

card_in_set('dark impostor', 'AVR').
card_original_type('dark impostor'/'AVR', 'Creature — Vampire Assassin').
card_original_text('dark impostor'/'AVR', '{4}{B}{B}: Exile target creature and put a +1/+1 counter on Dark Impostor.\nDark Impostor has all activated abilities of all creature cards exiled with it.').
card_first_print('dark impostor', 'AVR').
card_image_name('dark impostor'/'AVR', 'dark impostor').
card_uid('dark impostor'/'AVR', 'AVR:Dark Impostor:dark impostor').
card_rarity('dark impostor'/'AVR', 'Rare').
card_artist('dark impostor'/'AVR', 'Johannes Voss').
card_number('dark impostor'/'AVR', '92').
card_flavor_text('dark impostor'/'AVR', '\"Ah, this is a nice life. I can see why you were so desperate to keep it.\"').
card_multiverse_id('dark impostor'/'AVR', '240153').

card_in_set('deadeye navigator', 'AVR').
card_original_type('deadeye navigator'/'AVR', 'Creature — Spirit').
card_original_text('deadeye navigator'/'AVR', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Deadeye Navigator is paired with another creature, each of those creatures has \"{1}{U}: Exile this creature, then return it to the battlefield under your control.\"').
card_first_print('deadeye navigator', 'AVR').
card_image_name('deadeye navigator'/'AVR', 'deadeye navigator').
card_uid('deadeye navigator'/'AVR', 'AVR:Deadeye Navigator:deadeye navigator').
card_rarity('deadeye navigator'/'AVR', 'Rare').
card_artist('deadeye navigator'/'AVR', 'Tomasz Jedruszek').
card_number('deadeye navigator'/'AVR', '47').
card_multiverse_id('deadeye navigator'/'AVR', '240022').

card_in_set('death wind', 'AVR').
card_original_type('death wind'/'AVR', 'Instant').
card_original_text('death wind'/'AVR', 'Target creature gets -X/-X until end of turn.').
card_first_print('death wind', 'AVR').
card_image_name('death wind'/'AVR', 'death wind').
card_uid('death wind'/'AVR', 'AVR:Death Wind:death wind').
card_rarity('death wind'/'AVR', 'Common').
card_artist('death wind'/'AVR', 'Tomasz Jedruszek').
card_number('death wind'/'AVR', '93').
card_flavor_text('death wind'/'AVR', 'Leukin stared at the smoldering angel feathers. \"Run!\" he screamed to his patrol. \"We don\'t stand a chance!\"').
card_multiverse_id('death wind'/'AVR', '240207').

card_in_set('defang', 'AVR').
card_original_type('defang'/'AVR', 'Enchantment — Aura').
card_original_text('defang'/'AVR', 'Enchant creature\nPrevent all damage that would be dealt by enchanted creature.').
card_first_print('defang', 'AVR').
card_image_name('defang'/'AVR', 'defang').
card_uid('defang'/'AVR', 'AVR:Defang:defang').
card_rarity('defang'/'AVR', 'Common').
card_artist('defang'/'AVR', 'Steven Belledin').
card_number('defang'/'AVR', '15').
card_flavor_text('defang'/'AVR', 'Once vampires are disarmed, they\'re given wooden swords and forced to assist in the training of Midnight Duelists.').
card_multiverse_id('defang'/'AVR', '240087').

card_in_set('defy death', 'AVR').
card_original_type('defy death'/'AVR', 'Sorcery').
card_original_text('defy death'/'AVR', 'Return target creature card from your graveyard to the battlefield. If it\'s an Angel, put two +1/+1 counters on it.').
card_first_print('defy death', 'AVR').
card_image_name('defy death'/'AVR', 'defy death').
card_uid('defy death'/'AVR', 'AVR:Defy Death:defy death').
card_rarity('defy death'/'AVR', 'Uncommon').
card_artist('defy death'/'AVR', 'Karl Kopinski').
card_number('defy death'/'AVR', '16').
card_flavor_text('defy death'/'AVR', 'Gisela cornered the murderous demon among the graves of the dead villagers. \"A fitting end,\" she murmured as she raised her sword.').
card_multiverse_id('defy death'/'AVR', '239977').

card_in_set('demolish', 'AVR').
card_original_type('demolish'/'AVR', 'Sorcery').
card_original_text('demolish'/'AVR', 'Destroy target artifact or land.').
card_image_name('demolish'/'AVR', 'demolish').
card_uid('demolish'/'AVR', 'AVR:Demolish:demolish').
card_rarity('demolish'/'AVR', 'Common').
card_artist('demolish'/'AVR', 'Raymond Swanland').
card_number('demolish'/'AVR', '132').
card_flavor_text('demolish'/'AVR', '\"To truly defeat a skaberen, you must destroy not the monster but the lab.\"\n—Rem Karolus, Blade of the Inquisitors').
card_multiverse_id('demolish'/'AVR', '240060').

card_in_set('demonic rising', 'AVR').
card_original_type('demonic rising'/'AVR', 'Enchantment').
card_original_text('demonic rising'/'AVR', 'At the beginning of your end step, if you control exactly one creature, put a 5/5 black Demon creature token with flying onto the battlefield.').
card_first_print('demonic rising', 'AVR').
card_image_name('demonic rising'/'AVR', 'demonic rising').
card_uid('demonic rising'/'AVR', 'AVR:Demonic Rising:demonic rising').
card_rarity('demonic rising'/'AVR', 'Rare').
card_artist('demonic rising'/'AVR', 'Trevor Claxton').
card_number('demonic rising'/'AVR', '94').
card_flavor_text('demonic rising'/'AVR', 'Which is the greater folly, summoning the demon or expecting gratitude from it?').
card_multiverse_id('demonic rising'/'AVR', '278060').

card_in_set('demonic taskmaster', 'AVR').
card_original_type('demonic taskmaster'/'AVR', 'Creature — Demon').
card_original_text('demonic taskmaster'/'AVR', 'Flying\nAt the beginning of your upkeep, sacrifice a creature other than Demonic Taskmaster.').
card_first_print('demonic taskmaster', 'AVR').
card_image_name('demonic taskmaster'/'AVR', 'demonic taskmaster').
card_uid('demonic taskmaster'/'AVR', 'AVR:Demonic Taskmaster:demonic taskmaster').
card_rarity('demonic taskmaster'/'AVR', 'Uncommon').
card_artist('demonic taskmaster'/'AVR', 'Chris Rahn').
card_number('demonic taskmaster'/'AVR', '95').
card_flavor_text('demonic taskmaster'/'AVR', 'Subservience to greater power is the only law in the pit.').
card_multiverse_id('demonic taskmaster'/'AVR', '271092').

card_in_set('demonlord of ashmouth', 'AVR').
card_original_type('demonlord of ashmouth'/'AVR', 'Creature — Demon').
card_original_text('demonlord of ashmouth'/'AVR', 'Flying\nWhen Demonlord of Ashmouth enters the battlefield, exile it unless you sacrifice another creature.\nUndying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_first_print('demonlord of ashmouth', 'AVR').
card_image_name('demonlord of ashmouth'/'AVR', 'demonlord of ashmouth').
card_uid('demonlord of ashmouth'/'AVR', 'AVR:Demonlord of Ashmouth:demonlord of ashmouth').
card_rarity('demonlord of ashmouth'/'AVR', 'Rare').
card_artist('demonlord of ashmouth'/'AVR', 'Lucas Graciano').
card_number('demonlord of ashmouth'/'AVR', '96').
card_multiverse_id('demonlord of ashmouth'/'AVR', '240166').

card_in_set('descendants\' path', 'AVR').
card_original_type('descendants\' path'/'AVR', 'Enchantment').
card_original_text('descendants\' path'/'AVR', 'At the beginning of your upkeep, reveal the top card of your library. If it\'s a creature card that shares a creature type with a creature you control, you may cast that card without paying its mana cost. Otherwise, put that card on the bottom of your library.').
card_first_print('descendants\' path', 'AVR').
card_image_name('descendants\' path'/'AVR', 'descendants\' path').
card_uid('descendants\' path'/'AVR', 'AVR:Descendants\' Path:descendants\' path').
card_rarity('descendants\' path'/'AVR', 'Rare').
card_artist('descendants\' path'/'AVR', 'Terese Nielsen').
card_number('descendants\' path'/'AVR', '173').
card_multiverse_id('descendants\' path'/'AVR', '276498').

card_in_set('descent into madness', 'AVR').
card_original_type('descent into madness'/'AVR', 'Enchantment').
card_original_text('descent into madness'/'AVR', 'At the beginning of your upkeep, put a despair counter on Descent into Madness, then each player exiles X permanents he or she controls and/or cards from his or her hand, where X is the number of despair counters on Descent into Madness.').
card_first_print('descent into madness', 'AVR').
card_image_name('descent into madness'/'AVR', 'descent into madness').
card_uid('descent into madness'/'AVR', 'AVR:Descent into Madness:descent into madness').
card_rarity('descent into madness'/'AVR', 'Mythic Rare').
card_artist('descent into madness'/'AVR', 'Anthony Francisco').
card_number('descent into madness'/'AVR', '97').
card_flavor_text('descent into madness'/'AVR', 'The stairs lead down in both directions.').
card_multiverse_id('descent into madness'/'AVR', '240119').

card_in_set('desolate lighthouse', 'AVR').
card_original_type('desolate lighthouse'/'AVR', 'Land').
card_original_text('desolate lighthouse'/'AVR', '{T}: Add {1} to your mana pool.\n{1}{U}{R}, {T}: Draw a card, then discard a card.').
card_first_print('desolate lighthouse', 'AVR').
card_image_name('desolate lighthouse'/'AVR', 'desolate lighthouse').
card_uid('desolate lighthouse'/'AVR', 'AVR:Desolate Lighthouse:desolate lighthouse').
card_rarity('desolate lighthouse'/'AVR', 'Rare').
card_artist('desolate lighthouse'/'AVR', 'Scott Chou').
card_number('desolate lighthouse'/'AVR', '227').
card_flavor_text('desolate lighthouse'/'AVR', 'A lonely sentinel facing gales, hurricanes, and tides of homicidal spirits.').
card_multiverse_id('desolate lighthouse'/'AVR', '240147').

card_in_set('devastation tide', 'AVR').
card_original_type('devastation tide'/'AVR', 'Sorcery').
card_original_text('devastation tide'/'AVR', 'Return all nonland permanents to their owners\' hands.\nMiracle {1}{U} (You may cast this card for its miracle cost when you draw it if it\'s the first card you drew this turn.)').
card_first_print('devastation tide', 'AVR').
card_image_name('devastation tide'/'AVR', 'devastation tide').
card_uid('devastation tide'/'AVR', 'AVR:Devastation Tide:devastation tide').
card_rarity('devastation tide'/'AVR', 'Rare').
card_artist('devastation tide'/'AVR', 'Raymond Swanland').
card_number('devastation tide'/'AVR', '48').
card_flavor_text('devastation tide'/'AVR', 'It\'s hard to argue with a force made of divine will.').
card_multiverse_id('devastation tide'/'AVR', '275720').

card_in_set('devout chaplain', 'AVR').
card_original_type('devout chaplain'/'AVR', 'Creature — Human Cleric').
card_original_text('devout chaplain'/'AVR', '{T}, Tap two untapped Humans you control: Exile target artifact or enchantment.').
card_first_print('devout chaplain', 'AVR').
card_image_name('devout chaplain'/'AVR', 'devout chaplain').
card_uid('devout chaplain'/'AVR', 'AVR:Devout Chaplain:devout chaplain').
card_rarity('devout chaplain'/'AVR', 'Uncommon').
card_artist('devout chaplain'/'AVR', 'Lucas Graciano').
card_number('devout chaplain'/'AVR', '17').
card_flavor_text('devout chaplain'/'AVR', '\"By Avacyn\'s decree, we shall cleanse these relics of their demonic past.\"').
card_multiverse_id('devout chaplain'/'AVR', '276195').

card_in_set('diregraf escort', 'AVR').
card_original_type('diregraf escort'/'AVR', 'Creature — Human Cleric').
card_original_text('diregraf escort'/'AVR', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Diregraf Escort is paired with another creature, both creatures have protection from Zombies.').
card_first_print('diregraf escort', 'AVR').
card_image_name('diregraf escort'/'AVR', 'diregraf escort').
card_uid('diregraf escort'/'AVR', 'AVR:Diregraf Escort:diregraf escort').
card_rarity('diregraf escort'/'AVR', 'Common').
card_artist('diregraf escort'/'AVR', 'Ryan Pancoast').
card_number('diregraf escort'/'AVR', '174').
card_multiverse_id('diregraf escort'/'AVR', '240137').

card_in_set('divine deflection', 'AVR').
card_original_type('divine deflection'/'AVR', 'Instant').
card_original_text('divine deflection'/'AVR', 'Prevent the next X damage that would be dealt to you and/or permanents you control this turn. If damage is prevented this way, Divine Deflection deals that much damage to target creature or player.').
card_first_print('divine deflection', 'AVR').
card_image_name('divine deflection'/'AVR', 'divine deflection').
card_uid('divine deflection'/'AVR', 'AVR:Divine Deflection:divine deflection').
card_rarity('divine deflection'/'AVR', 'Rare').
card_artist('divine deflection'/'AVR', 'Steve Prescott').
card_number('divine deflection'/'AVR', '18').
card_multiverse_id('divine deflection'/'AVR', '240072').

card_in_set('dread slaver', 'AVR').
card_original_type('dread slaver'/'AVR', 'Creature — Zombie Horror').
card_original_text('dread slaver'/'AVR', 'Whenever a creature dealt damage by Dread Slaver this turn dies, return it to the battlefield under your control. That creature is a black Zombie in addition to its other colors and types.').
card_first_print('dread slaver', 'AVR').
card_image_name('dread slaver'/'AVR', 'dread slaver').
card_uid('dread slaver'/'AVR', 'AVR:Dread Slaver:dread slaver').
card_rarity('dread slaver'/'AVR', 'Rare').
card_artist('dread slaver'/'AVR', 'Dave Kendall').
card_number('dread slaver'/'AVR', '98').
card_flavor_text('dread slaver'/'AVR', 'Half a brain rules the mindless.').
card_multiverse_id('dread slaver'/'AVR', '240007').

card_in_set('dreadwaters', 'AVR').
card_original_type('dreadwaters'/'AVR', 'Sorcery').
card_original_text('dreadwaters'/'AVR', 'Target player puts the top X cards of his or her library into his or her graveyard, where X is the number of lands you control.').
card_first_print('dreadwaters', 'AVR').
card_image_name('dreadwaters'/'AVR', 'dreadwaters').
card_uid('dreadwaters'/'AVR', 'AVR:Dreadwaters:dreadwaters').
card_rarity('dreadwaters'/'AVR', 'Common').
card_artist('dreadwaters'/'AVR', 'Cliff Childs').
card_number('dreadwaters'/'AVR', '49').
card_flavor_text('dreadwaters'/'AVR', 'Stray into the Morkrut and it may rise to claim you.').
card_multiverse_id('dreadwaters'/'AVR', '240008').

card_in_set('driver of the dead', 'AVR').
card_original_type('driver of the dead'/'AVR', 'Creature — Vampire').
card_original_text('driver of the dead'/'AVR', 'When Driver of the Dead dies, return target creature card with converted mana cost 2 or less from your graveyard to the battlefield.').
card_first_print('driver of the dead', 'AVR').
card_image_name('driver of the dead'/'AVR', 'driver of the dead').
card_uid('driver of the dead'/'AVR', 'AVR:Driver of the Dead:driver of the dead').
card_rarity('driver of the dead'/'AVR', 'Common').
card_artist('driver of the dead'/'AVR', 'James Ryman').
card_number('driver of the dead'/'AVR', '99').
card_flavor_text('driver of the dead'/'AVR', 'Some vampires resorted to corpse trading after cathars burned their estates and drove them from their ancestral feeding grounds.').
card_multiverse_id('driver of the dead'/'AVR', '239963').

card_in_set('druid\'s familiar', 'AVR').
card_original_type('druid\'s familiar'/'AVR', 'Creature — Bear').
card_original_text('druid\'s familiar'/'AVR', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Druid\'s Familiar is paired with another creature, each of those creatures gets +2/+2.').
card_first_print('druid\'s familiar', 'AVR').
card_image_name('druid\'s familiar'/'AVR', 'druid\'s familiar').
card_uid('druid\'s familiar'/'AVR', 'AVR:Druid\'s Familiar:druid\'s familiar').
card_rarity('druid\'s familiar'/'AVR', 'Uncommon').
card_artist('druid\'s familiar'/'AVR', 'Adam Paquette').
card_number('druid\'s familiar'/'AVR', '175').
card_multiverse_id('druid\'s familiar'/'AVR', '240088').

card_in_set('druids\' repository', 'AVR').
card_original_type('druids\' repository'/'AVR', 'Enchantment').
card_original_text('druids\' repository'/'AVR', 'Whenever a creature you control attacks, put a charge counter on Druids\' Repository.\nRemove a charge counter from Druids\' Repository: Add one mana of any color to your mana pool.').
card_first_print('druids\' repository', 'AVR').
card_image_name('druids\' repository'/'AVR', 'druids\' repository').
card_uid('druids\' repository'/'AVR', 'AVR:Druids\' Repository:druids\' repository').
card_rarity('druids\' repository'/'AVR', 'Rare').
card_artist('druids\' repository'/'AVR', 'Daarken').
card_number('druids\' repository'/'AVR', '176').
card_multiverse_id('druids\' repository'/'AVR', '240135').

card_in_set('dual casting', 'AVR').
card_original_type('dual casting'/'AVR', 'Enchantment — Aura').
card_original_text('dual casting'/'AVR', 'Enchant creature\nEnchanted creature has \"{R}, {T}: Copy target instant or sorcery spell you control. You may choose new targets for the copy.\"').
card_first_print('dual casting', 'AVR').
card_image_name('dual casting'/'AVR', 'dual casting').
card_uid('dual casting'/'AVR', 'AVR:Dual Casting:dual casting').
card_rarity('dual casting'/'AVR', 'Rare').
card_artist('dual casting'/'AVR', 'Johannes Voss').
card_number('dual casting'/'AVR', '133').
card_flavor_text('dual casting'/'AVR', 'Hours after the Helvault opened, his powers returned twice as strong as before.').
card_multiverse_id('dual casting'/'AVR', '240138').

card_in_set('eaten by spiders', 'AVR').
card_original_type('eaten by spiders'/'AVR', 'Instant').
card_original_text('eaten by spiders'/'AVR', 'Destroy target creature with flying and all Equipment attached to that creature.').
card_first_print('eaten by spiders', 'AVR').
card_image_name('eaten by spiders'/'AVR', 'eaten by spiders').
card_uid('eaten by spiders'/'AVR', 'AVR:Eaten by Spiders:eaten by spiders').
card_rarity('eaten by spiders'/'AVR', 'Uncommon').
card_artist('eaten by spiders'/'AVR', 'Slawomir Maniak').
card_number('eaten by spiders'/'AVR', '177').
card_flavor_text('eaten by spiders'/'AVR', 'After sampling many victims, the spider developed a preference for the undead and their accoutrements.').
card_multiverse_id('eaten by spiders'/'AVR', '270987').

card_in_set('elgaud shieldmate', 'AVR').
card_original_type('elgaud shieldmate'/'AVR', 'Creature — Human Soldier').
card_original_text('elgaud shieldmate'/'AVR', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Elgaud Shieldmate is paired with another creature, both creatures have hexproof. (They can\'t be the targets of spells or abilities your opponents control.)').
card_first_print('elgaud shieldmate', 'AVR').
card_image_name('elgaud shieldmate'/'AVR', 'elgaud shieldmate').
card_uid('elgaud shieldmate'/'AVR', 'AVR:Elgaud Shieldmate:elgaud shieldmate').
card_rarity('elgaud shieldmate'/'AVR', 'Common').
card_artist('elgaud shieldmate'/'AVR', 'Anthony Palumbo').
card_number('elgaud shieldmate'/'AVR', '50').
card_multiverse_id('elgaud shieldmate'/'AVR', '239968').

card_in_set('emancipation angel', 'AVR').
card_original_type('emancipation angel'/'AVR', 'Creature — Angel').
card_original_text('emancipation angel'/'AVR', 'Flying\nWhen Emancipation Angel enters the battlefield, return a permanent you control to its owner\'s hand.').
card_first_print('emancipation angel', 'AVR').
card_image_name('emancipation angel'/'AVR', 'emancipation angel').
card_uid('emancipation angel'/'AVR', 'AVR:Emancipation Angel:emancipation angel').
card_rarity('emancipation angel'/'AVR', 'Uncommon').
card_artist('emancipation angel'/'AVR', 'Scott Chou').
card_number('emancipation angel'/'AVR', '19').
card_flavor_text('emancipation angel'/'AVR', '\"You have done your best. I give you leave to rest.\"').
card_multiverse_id('emancipation angel'/'AVR', '239980').

card_in_set('entreat the angels', 'AVR').
card_original_type('entreat the angels'/'AVR', 'Sorcery').
card_original_text('entreat the angels'/'AVR', 'Put X 4/4 white Angel creature tokens with flying onto the battlefield.\nMiracle {X}{W}{W} (You may cast this card for its miracle cost when you draw it if it\'s the first card you drew this turn.)').
card_first_print('entreat the angels', 'AVR').
card_image_name('entreat the angels'/'AVR', 'entreat the angels').
card_uid('entreat the angels'/'AVR', 'AVR:Entreat the Angels:entreat the angels').
card_rarity('entreat the angels'/'AVR', 'Mythic Rare').
card_artist('entreat the angels'/'AVR', 'Todd Lockwood').
card_number('entreat the angels'/'AVR', '20').
card_multiverse_id('entreat the angels'/'AVR', '247426').

card_in_set('essence harvest', 'AVR').
card_original_type('essence harvest'/'AVR', 'Sorcery').
card_original_text('essence harvest'/'AVR', 'Target player loses X life and you gain X life, where X is the greatest power among creatures you control.').
card_first_print('essence harvest', 'AVR').
card_image_name('essence harvest'/'AVR', 'essence harvest').
card_uid('essence harvest'/'AVR', 'AVR:Essence Harvest:essence harvest').
card_rarity('essence harvest'/'AVR', 'Common').
card_artist('essence harvest'/'AVR', 'Daarken').
card_number('essence harvest'/'AVR', '100').
card_flavor_text('essence harvest'/'AVR', 'Captain Lewenheart uncovered evidence of a demon cult among the clergy. The Skirsdag never gave him a chance to prove his case.').
card_multiverse_id('essence harvest'/'AVR', '271114').

card_in_set('evernight shade', 'AVR').
card_original_type('evernight shade'/'AVR', 'Creature — Shade').
card_original_text('evernight shade'/'AVR', '{B}: Evernight Shade gets +1/+1 until end of turn.\nUndying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_first_print('evernight shade', 'AVR').
card_image_name('evernight shade'/'AVR', 'evernight shade').
card_uid('evernight shade'/'AVR', 'AVR:Evernight Shade:evernight shade').
card_rarity('evernight shade'/'AVR', 'Uncommon').
card_artist('evernight shade'/'AVR', 'Nic Klein').
card_number('evernight shade'/'AVR', '101').
card_multiverse_id('evernight shade'/'AVR', '240125').

card_in_set('exquisite blood', 'AVR').
card_original_type('exquisite blood'/'AVR', 'Enchantment').
card_original_text('exquisite blood'/'AVR', 'Whenever an opponent loses life, you gain that much life.').
card_first_print('exquisite blood', 'AVR').
card_image_name('exquisite blood'/'AVR', 'exquisite blood').
card_uid('exquisite blood'/'AVR', 'AVR:Exquisite Blood:exquisite blood').
card_rarity('exquisite blood'/'AVR', 'Rare').
card_artist('exquisite blood'/'AVR', 'Cynthia Sheppard').
card_number('exquisite blood'/'AVR', '102').
card_flavor_text('exquisite blood'/'AVR', 'Even as humans regained the upper hand, some still willingly traded their lives for a chance at immortality.').
card_multiverse_id('exquisite blood'/'AVR', '240134').

card_in_set('falkenrath exterminator', 'AVR').
card_original_type('falkenrath exterminator'/'AVR', 'Creature — Vampire Archer').
card_original_text('falkenrath exterminator'/'AVR', 'Whenever Falkenrath Exterminator deals combat damage to a player, put a +1/+1 counter on it.\n{2}{R}: Falkenrath Exterminator deals damage to target creature equal to the number of +1/+1 counters on Falkenrath Exterminator.').
card_first_print('falkenrath exterminator', 'AVR').
card_image_name('falkenrath exterminator'/'AVR', 'falkenrath exterminator').
card_uid('falkenrath exterminator'/'AVR', 'AVR:Falkenrath Exterminator:falkenrath exterminator').
card_rarity('falkenrath exterminator'/'AVR', 'Uncommon').
card_artist('falkenrath exterminator'/'AVR', 'Winona Nelson').
card_number('falkenrath exterminator'/'AVR', '134').
card_multiverse_id('falkenrath exterminator'/'AVR', '276499').

card_in_set('farbog explorer', 'AVR').
card_original_type('farbog explorer'/'AVR', 'Creature — Human Scout').
card_original_text('farbog explorer'/'AVR', 'Swampwalk (This creature is unblockable as long as defending player controls a Swamp.)').
card_first_print('farbog explorer', 'AVR').
card_image_name('farbog explorer'/'AVR', 'farbog explorer').
card_uid('farbog explorer'/'AVR', 'AVR:Farbog Explorer:farbog explorer').
card_rarity('farbog explorer'/'AVR', 'Common').
card_artist('farbog explorer'/'AVR', 'Scott Chou').
card_number('farbog explorer'/'AVR', '21').
card_flavor_text('farbog explorer'/'AVR', '\"I\'d slog through a thousand swamps to help one soul find rest.\"').
card_multiverse_id('farbog explorer'/'AVR', '240108').

card_in_set('favorable winds', 'AVR').
card_original_type('favorable winds'/'AVR', 'Enchantment').
card_original_text('favorable winds'/'AVR', 'Creatures you control with flying get +1/+1.').
card_first_print('favorable winds', 'AVR').
card_image_name('favorable winds'/'AVR', 'favorable winds').
card_uid('favorable winds'/'AVR', 'AVR:Favorable Winds:favorable winds').
card_rarity('favorable winds'/'AVR', 'Uncommon').
card_artist('favorable winds'/'AVR', 'Winona Nelson').
card_number('favorable winds'/'AVR', '51').
card_flavor_text('favorable winds'/'AVR', 'Long thought to be extinct, flocks of gryffs reappeared with Avacyn\'s return.').
card_multiverse_id('favorable winds'/'AVR', '240131').

card_in_set('fervent cathar', 'AVR').
card_original_type('fervent cathar'/'AVR', 'Creature — Human Knight').
card_original_text('fervent cathar'/'AVR', 'Haste\nWhen Fervent Cathar enters the battlefield, target creature can\'t block this turn.').
card_first_print('fervent cathar', 'AVR').
card_image_name('fervent cathar'/'AVR', 'fervent cathar').
card_uid('fervent cathar'/'AVR', 'AVR:Fervent Cathar:fervent cathar').
card_rarity('fervent cathar'/'AVR', 'Common').
card_artist('fervent cathar'/'AVR', 'Steven Belledin').
card_number('fervent cathar'/'AVR', '135').
card_flavor_text('fervent cathar'/'AVR', '\"I\'ll put my sword down only to die. And I don\'t plan on doing that today.\"').
card_multiverse_id('fervent cathar'/'AVR', '240122').

card_in_set('fettergeist', 'AVR').
card_original_type('fettergeist'/'AVR', 'Creature — Spirit').
card_original_text('fettergeist'/'AVR', 'Flying\nAt the beginning of your upkeep, sacrifice Fettergeist unless you pay {1} for each other creature you control.').
card_first_print('fettergeist', 'AVR').
card_image_name('fettergeist'/'AVR', 'fettergeist').
card_uid('fettergeist'/'AVR', 'AVR:Fettergeist:fettergeist').
card_rarity('fettergeist'/'AVR', 'Uncommon').
card_artist('fettergeist'/'AVR', 'Izzy').
card_number('fettergeist'/'AVR', '52').
card_flavor_text('fettergeist'/'AVR', 'The choices of the living will tether them in death.').
card_multiverse_id('fettergeist'/'AVR', '240080').

card_in_set('fleeting distraction', 'AVR').
card_original_type('fleeting distraction'/'AVR', 'Instant').
card_original_text('fleeting distraction'/'AVR', 'Target creature gets -1/-0 until end of turn.\nDraw a card.').
card_image_name('fleeting distraction'/'AVR', 'fleeting distraction').
card_uid('fleeting distraction'/'AVR', 'AVR:Fleeting Distraction:fleeting distraction').
card_rarity('fleeting distraction'/'AVR', 'Common').
card_artist('fleeting distraction'/'AVR', 'Ryan Yee').
card_number('fleeting distraction'/'AVR', '53').
card_flavor_text('fleeting distraction'/'AVR', 'The first spell taught to young mages, it has spared the lives of many whose hands are too small to hold a sword.').
card_multiverse_id('fleeting distraction'/'AVR', '240162').

card_in_set('flowering lumberknot', 'AVR').
card_original_type('flowering lumberknot'/'AVR', 'Creature — Treefolk').
card_original_text('flowering lumberknot'/'AVR', 'Flowering Lumberknot can\'t attack or block unless it\'s paired with a creature with soulbond.').
card_first_print('flowering lumberknot', 'AVR').
card_image_name('flowering lumberknot'/'AVR', 'flowering lumberknot').
card_uid('flowering lumberknot'/'AVR', 'AVR:Flowering Lumberknot:flowering lumberknot').
card_rarity('flowering lumberknot'/'AVR', 'Common').
card_artist('flowering lumberknot'/'AVR', 'Nic Klein').
card_number('flowering lumberknot'/'AVR', '178').
card_flavor_text('flowering lumberknot'/'AVR', '\"It used to eat us trappers. Now it follows us around like some kind of hound dog.\"\n—Alena, trapper of Kessig').
card_multiverse_id('flowering lumberknot'/'AVR', '240209').

card_in_set('forest', 'AVR').
card_original_type('forest'/'AVR', 'Basic Land — Forest').
card_original_text('forest'/'AVR', 'G').
card_image_name('forest'/'AVR', 'forest1').
card_uid('forest'/'AVR', 'AVR:Forest:forest1').
card_rarity('forest'/'AVR', 'Basic Land').
card_artist('forest'/'AVR', 'James Paick').
card_number('forest'/'AVR', '242').
card_multiverse_id('forest'/'AVR', '269636').

card_in_set('forest', 'AVR').
card_original_type('forest'/'AVR', 'Basic Land — Forest').
card_original_text('forest'/'AVR', 'G').
card_image_name('forest'/'AVR', 'forest2').
card_uid('forest'/'AVR', 'AVR:Forest:forest2').
card_rarity('forest'/'AVR', 'Basic Land').
card_artist('forest'/'AVR', 'Jung Park').
card_number('forest'/'AVR', '243').
card_multiverse_id('forest'/'AVR', '269635').

card_in_set('forest', 'AVR').
card_original_type('forest'/'AVR', 'Basic Land — Forest').
card_original_text('forest'/'AVR', 'G').
card_image_name('forest'/'AVR', 'forest3').
card_uid('forest'/'AVR', 'AVR:Forest:forest3').
card_rarity('forest'/'AVR', 'Basic Land').
card_artist('forest'/'AVR', 'Eytan Zana').
card_number('forest'/'AVR', '244').
card_multiverse_id('forest'/'AVR', '269629').

card_in_set('gallows at willow hill', 'AVR').
card_original_type('gallows at willow hill'/'AVR', 'Artifact').
card_original_text('gallows at willow hill'/'AVR', '{3}, {T}, Tap three untapped Humans you control: Destroy target creature. Its controller puts a 1/1 white Spirit creature token with flying onto the battlefield.').
card_first_print('gallows at willow hill', 'AVR').
card_image_name('gallows at willow hill'/'AVR', 'gallows at willow hill').
card_uid('gallows at willow hill'/'AVR', 'AVR:Gallows at Willow Hill:gallows at willow hill').
card_rarity('gallows at willow hill'/'AVR', 'Rare').
card_artist('gallows at willow hill'/'AVR', 'John Avon').
card_number('gallows at willow hill'/'AVR', '215').
card_multiverse_id('gallows at willow hill'/'AVR', '262674').

card_in_set('galvanic alchemist', 'AVR').
card_original_type('galvanic alchemist'/'AVR', 'Creature — Human Wizard').
card_original_text('galvanic alchemist'/'AVR', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Galvanic Alchemist is paired with another creature, each of those creatures has \"{2}{U}: Untap this creature.\"').
card_first_print('galvanic alchemist', 'AVR').
card_image_name('galvanic alchemist'/'AVR', 'galvanic alchemist').
card_uid('galvanic alchemist'/'AVR', 'AVR:Galvanic Alchemist:galvanic alchemist').
card_rarity('galvanic alchemist'/'AVR', 'Common').
card_artist('galvanic alchemist'/'AVR', 'Svetlin Velinov').
card_number('galvanic alchemist'/'AVR', '54').
card_multiverse_id('galvanic alchemist'/'AVR', '240211').

card_in_set('gang of devils', 'AVR').
card_original_type('gang of devils'/'AVR', 'Creature — Devil').
card_original_text('gang of devils'/'AVR', 'When Gang of Devils dies, it deals 3 damage divided as you choose among one, two, or three target creatures and/or players.').
card_first_print('gang of devils', 'AVR').
card_image_name('gang of devils'/'AVR', 'gang of devils').
card_uid('gang of devils'/'AVR', 'AVR:Gang of Devils:gang of devils').
card_rarity('gang of devils'/'AVR', 'Uncommon').
card_artist('gang of devils'/'AVR', 'Erica Yang').
card_number('gang of devils'/'AVR', '136').
card_flavor_text('gang of devils'/'AVR', '\"Well, how could I know they\'d explode?\"\n—Gregel, township militia').
card_multiverse_id('gang of devils'/'AVR', '240167').

card_in_set('geist snatch', 'AVR').
card_original_type('geist snatch'/'AVR', 'Instant').
card_original_text('geist snatch'/'AVR', 'Counter target creature spell. Put a 1/1 blue Spirit creature token with flying onto the battlefield.').
card_first_print('geist snatch', 'AVR').
card_image_name('geist snatch'/'AVR', 'geist snatch').
card_uid('geist snatch'/'AVR', 'AVR:Geist Snatch:geist snatch').
card_rarity('geist snatch'/'AVR', 'Common').
card_artist('geist snatch'/'AVR', 'Dan Scott').
card_number('geist snatch'/'AVR', '55').
card_flavor_text('geist snatch'/'AVR', 'Angels and demons aren\'t the only ones listening to your prayers.').
card_multiverse_id('geist snatch'/'AVR', '240021').

card_in_set('geist trappers', 'AVR').
card_original_type('geist trappers'/'AVR', 'Creature — Human Warrior').
card_original_text('geist trappers'/'AVR', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Geist Trappers is paired with another creature, both creatures have reach.').
card_first_print('geist trappers', 'AVR').
card_image_name('geist trappers'/'AVR', 'geist trappers').
card_uid('geist trappers'/'AVR', 'AVR:Geist Trappers:geist trappers').
card_rarity('geist trappers'/'AVR', 'Common').
card_artist('geist trappers'/'AVR', 'Anthony Palumbo').
card_number('geist trappers'/'AVR', '179').
card_multiverse_id('geist trappers'/'AVR', '240024').

card_in_set('ghostform', 'AVR').
card_original_type('ghostform'/'AVR', 'Sorcery').
card_original_text('ghostform'/'AVR', 'Up to two target creatures are unblockable this turn.').
card_first_print('ghostform', 'AVR').
card_image_name('ghostform'/'AVR', 'ghostform').
card_uid('ghostform'/'AVR', 'AVR:Ghostform:ghostform').
card_rarity('ghostform'/'AVR', 'Common').
card_artist('ghostform'/'AVR', 'Scott Chou').
card_number('ghostform'/'AVR', '56').
card_flavor_text('ghostform'/'AVR', '\"There\'s no such thing as ‘impenetrable.\'\"').
card_multiverse_id('ghostform'/'AVR', '240003').

card_in_set('ghostly flicker', 'AVR').
card_original_type('ghostly flicker'/'AVR', 'Instant').
card_original_text('ghostly flicker'/'AVR', 'Exile two target artifacts, creatures, and/or lands you control, then return those cards to the battlefield under your control.').
card_first_print('ghostly flicker', 'AVR').
card_image_name('ghostly flicker'/'AVR', 'ghostly flicker').
card_uid('ghostly flicker'/'AVR', 'AVR:Ghostly Flicker:ghostly flicker').
card_rarity('ghostly flicker'/'AVR', 'Common').
card_artist('ghostly flicker'/'AVR', 'Raymond Swanland').
card_number('ghostly flicker'/'AVR', '57').
card_flavor_text('ghostly flicker'/'AVR', 'Sometimes it\'s useful to take a break from existence.').
card_multiverse_id('ghostly flicker'/'AVR', '240101').

card_in_set('ghostly touch', 'AVR').
card_original_type('ghostly touch'/'AVR', 'Enchantment — Aura').
card_original_text('ghostly touch'/'AVR', 'Enchant creature\nEnchanted creature has \"Whenever this creature attacks, you may tap or untap target permanent.\"').
card_first_print('ghostly touch', 'AVR').
card_image_name('ghostly touch'/'AVR', 'ghostly touch').
card_uid('ghostly touch'/'AVR', 'AVR:Ghostly Touch:ghostly touch').
card_rarity('ghostly touch'/'AVR', 'Uncommon').
card_artist('ghostly touch'/'AVR', 'Jason Felix').
card_number('ghostly touch'/'AVR', '58').
card_flavor_text('ghostly touch'/'AVR', '\"Geists wish to make themselves known. I\'m merely the vessel for their efforts.\"').
card_multiverse_id('ghostly touch'/'AVR', '278260').

card_in_set('ghoulflesh', 'AVR').
card_original_type('ghoulflesh'/'AVR', 'Enchantment — Aura').
card_original_text('ghoulflesh'/'AVR', 'Enchant creature\nEnchanted creature gets -1/-1 and is a black Zombie in addition to its other colors and types.').
card_first_print('ghoulflesh', 'AVR').
card_image_name('ghoulflesh'/'AVR', 'ghoulflesh').
card_uid('ghoulflesh'/'AVR', 'AVR:Ghoulflesh:ghoulflesh').
card_rarity('ghoulflesh'/'AVR', 'Common').
card_artist('ghoulflesh'/'AVR', 'Igor Kieryluk').
card_number('ghoulflesh'/'AVR', '103').
card_flavor_text('ghoulflesh'/'AVR', 'The body dies in stages. First the skin, then the muscle. The brain is last, much to the victim\'s dismay.').
card_multiverse_id('ghoulflesh'/'AVR', '239970').

card_in_set('gisela, blade of goldnight', 'AVR').
card_original_type('gisela, blade of goldnight'/'AVR', 'Legendary Creature — Angel').
card_original_text('gisela, blade of goldnight'/'AVR', 'Flying, first strike\nIf a source would deal damage to an opponent or a permanent an opponent controls, that source deals double that damage to that player or permanent instead.\nIf a source would deal damage to you or a permanent you control, prevent half that damage, rounded up.').
card_first_print('gisela, blade of goldnight', 'AVR').
card_image_name('gisela, blade of goldnight'/'AVR', 'gisela, blade of goldnight').
card_uid('gisela, blade of goldnight'/'AVR', 'AVR:Gisela, Blade of Goldnight:gisela, blade of goldnight').
card_rarity('gisela, blade of goldnight'/'AVR', 'Mythic Rare').
card_artist('gisela, blade of goldnight'/'AVR', 'Jason Chan').
card_number('gisela, blade of goldnight'/'AVR', '209').
card_multiverse_id('gisela, blade of goldnight'/'AVR', '240034').

card_in_set('gloom surgeon', 'AVR').
card_original_type('gloom surgeon'/'AVR', 'Creature — Spirit').
card_original_text('gloom surgeon'/'AVR', 'If combat damage would be dealt to Gloom Surgeon, prevent that damage and exile that many cards from the top of your library.').
card_first_print('gloom surgeon', 'AVR').
card_image_name('gloom surgeon'/'AVR', 'gloom surgeon').
card_uid('gloom surgeon'/'AVR', 'AVR:Gloom Surgeon:gloom surgeon').
card_rarity('gloom surgeon'/'AVR', 'Rare').
card_artist('gloom surgeon'/'AVR', 'Volkan Baga').
card_number('gloom surgeon'/'AVR', '104').
card_flavor_text('gloom surgeon'/'AVR', 'He roams the streets of Havengul seeking the sick and delirious so that he may cure them of their lives.').
card_multiverse_id('gloom surgeon'/'AVR', '240165').

card_in_set('gloomwidow', 'AVR').
card_original_type('gloomwidow'/'AVR', 'Creature — Spider').
card_original_text('gloomwidow'/'AVR', 'Reach\nGloomwidow can block only creatures with flying.').
card_image_name('gloomwidow'/'AVR', 'gloomwidow').
card_uid('gloomwidow'/'AVR', 'AVR:Gloomwidow:gloomwidow').
card_rarity('gloomwidow'/'AVR', 'Uncommon').
card_artist('gloomwidow'/'AVR', 'Svetlin Velinov').
card_number('gloomwidow'/'AVR', '180').
card_flavor_text('gloomwidow'/'AVR', 'The abandoned towns on the moors didn\'t stay empty for long.').
card_multiverse_id('gloomwidow'/'AVR', '278254').

card_in_set('goldnight commander', 'AVR').
card_original_type('goldnight commander'/'AVR', 'Creature — Human Cleric Soldier').
card_original_text('goldnight commander'/'AVR', 'Whenever another creature enters the battlefield under your control, creatures you control get +1/+1 until end of turn.').
card_first_print('goldnight commander', 'AVR').
card_image_name('goldnight commander'/'AVR', 'goldnight commander').
card_uid('goldnight commander'/'AVR', 'AVR:Goldnight Commander:goldnight commander').
card_rarity('goldnight commander'/'AVR', 'Uncommon').
card_artist('goldnight commander'/'AVR', 'Chris Rahn').
card_number('goldnight commander'/'AVR', '22').
card_flavor_text('goldnight commander'/'AVR', '\"One faith, but many hands in victory.\"').
card_multiverse_id('goldnight commander'/'AVR', '240029').

card_in_set('goldnight redeemer', 'AVR').
card_original_type('goldnight redeemer'/'AVR', 'Creature — Angel').
card_original_text('goldnight redeemer'/'AVR', 'Flying\nWhen Goldnight Redeemer enters the battlefield, you gain 2 life for each other creature you control.').
card_first_print('goldnight redeemer', 'AVR').
card_image_name('goldnight redeemer'/'AVR', 'goldnight redeemer').
card_uid('goldnight redeemer'/'AVR', 'AVR:Goldnight Redeemer:goldnight redeemer').
card_rarity('goldnight redeemer'/'AVR', 'Uncommon').
card_artist('goldnight redeemer'/'AVR', 'Karl Kopinski').
card_number('goldnight redeemer'/'AVR', '23').
card_flavor_text('goldnight redeemer'/'AVR', 'The Host sings of Avacyn\'s return without a single verse about its own suffering.').
card_multiverse_id('goldnight redeemer'/'AVR', '239998').

card_in_set('grave exchange', 'AVR').
card_original_type('grave exchange'/'AVR', 'Sorcery').
card_original_text('grave exchange'/'AVR', 'Return target creature card from your graveyard to your hand. Target player sacrifices a creature.').
card_first_print('grave exchange', 'AVR').
card_image_name('grave exchange'/'AVR', 'grave exchange').
card_uid('grave exchange'/'AVR', 'AVR:Grave Exchange:grave exchange').
card_rarity('grave exchange'/'AVR', 'Common').
card_artist('grave exchange'/'AVR', 'Sam Wolfe Connelly').
card_number('grave exchange'/'AVR', '105').
card_flavor_text('grave exchange'/'AVR', '\"It\'s a cold, dark journey either way.\"\n—Eruth of Lambholt').
card_multiverse_id('grave exchange'/'AVR', '279611').

card_in_set('griselbrand', 'AVR').
card_original_type('griselbrand'/'AVR', 'Legendary Creature — Demon').
card_original_text('griselbrand'/'AVR', 'Flying, lifelink\nPay 7 life: Draw seven cards.').
card_image_name('griselbrand'/'AVR', 'griselbrand').
card_uid('griselbrand'/'AVR', 'AVR:Griselbrand:griselbrand').
card_rarity('griselbrand'/'AVR', 'Mythic Rare').
card_artist('griselbrand'/'AVR', 'Igor Kieryluk').
card_number('griselbrand'/'AVR', '106').
card_flavor_text('griselbrand'/'AVR', '\"Avacyn emerged from the broken Helvault, but her freedom came at a price—him.\"\n—Thalia, Knight-Cathar').
card_multiverse_id('griselbrand'/'AVR', '239995').

card_in_set('grounded', 'AVR').
card_original_type('grounded'/'AVR', 'Enchantment — Aura').
card_original_text('grounded'/'AVR', 'Enchant creature\nEnchanted creature loses flying.').
card_first_print('grounded', 'AVR').
card_image_name('grounded'/'AVR', 'grounded').
card_uid('grounded'/'AVR', 'AVR:Grounded:grounded').
card_rarity('grounded'/'AVR', 'Common').
card_artist('grounded'/'AVR', 'Greg Staples').
card_number('grounded'/'AVR', '181').
card_flavor_text('grounded'/'AVR', '\"We\'ve tracked Ludevic\'s monster across four provinces. When it lands, make sure it stays put.\"\n—Alena, trapper of Kessig,\nto Halana of Ulvenwald').
card_multiverse_id('grounded'/'AVR', '240009').

card_in_set('gryff vanguard', 'AVR').
card_original_type('gryff vanguard'/'AVR', 'Creature — Human Knight').
card_original_text('gryff vanguard'/'AVR', 'Flying\nWhen Gryff Vanguard enters the battlefield, draw a card.').
card_first_print('gryff vanguard', 'AVR').
card_image_name('gryff vanguard'/'AVR', 'gryff vanguard').
card_uid('gryff vanguard'/'AVR', 'AVR:Gryff Vanguard:gryff vanguard').
card_rarity('gryff vanguard'/'AVR', 'Common').
card_artist('gryff vanguard'/'AVR', 'Jason Chan').
card_number('gryff vanguard'/'AVR', '59').
card_flavor_text('gryff vanguard'/'AVR', '\"Ghouls smashed the door, but I heard the call of the gryffs and knew we were saved.\"\n—Ekka, shopkeeper of Hanweir').
card_multiverse_id('gryff vanguard'/'AVR', '239982').

card_in_set('guise of fire', 'AVR').
card_original_type('guise of fire'/'AVR', 'Enchantment — Aura').
card_original_text('guise of fire'/'AVR', 'Enchant creature\nEnchanted creature gets +1/-1 and attacks each turn if able.').
card_first_print('guise of fire', 'AVR').
card_image_name('guise of fire'/'AVR', 'guise of fire').
card_uid('guise of fire'/'AVR', 'AVR:Guise of Fire:guise of fire').
card_rarity('guise of fire'/'AVR', 'Common').
card_artist('guise of fire'/'AVR', 'Dave Kendall').
card_number('guise of fire'/'AVR', '137').
card_flavor_text('guise of fire'/'AVR', '\"Fire will eventually destroy a zombie, but a fiery zombie destroys a lot of other things first.\"\n—Rem Karolus, Blade of the Inquisitors').
card_multiverse_id('guise of fire'/'AVR', '240002').

card_in_set('hanweir lancer', 'AVR').
card_original_type('hanweir lancer'/'AVR', 'Creature — Human Knight').
card_original_text('hanweir lancer'/'AVR', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Hanweir Lancer is paired with another creature, both creatures have first strike.').
card_first_print('hanweir lancer', 'AVR').
card_image_name('hanweir lancer'/'AVR', 'hanweir lancer').
card_uid('hanweir lancer'/'AVR', 'AVR:Hanweir Lancer:hanweir lancer').
card_rarity('hanweir lancer'/'AVR', 'Common').
card_artist('hanweir lancer'/'AVR', 'Steve Prescott').
card_number('hanweir lancer'/'AVR', '138').
card_multiverse_id('hanweir lancer'/'AVR', '240073').

card_in_set('harvester of souls', 'AVR').
card_original_type('harvester of souls'/'AVR', 'Creature — Demon').
card_original_text('harvester of souls'/'AVR', 'Deathtouch\nWhenever another nontoken creature dies, you may draw a card.').
card_first_print('harvester of souls', 'AVR').
card_image_name('harvester of souls'/'AVR', 'harvester of souls').
card_uid('harvester of souls'/'AVR', 'AVR:Harvester of Souls:harvester of souls').
card_rarity('harvester of souls'/'AVR', 'Rare').
card_artist('harvester of souls'/'AVR', 'Slawomir Maniak').
card_number('harvester of souls'/'AVR', '107').
card_flavor_text('harvester of souls'/'AVR', '\"He is judge, jury, and executioner because he killed them all.\"\n—Bishop Cawle of Elgaud').
card_multiverse_id('harvester of souls'/'AVR', '240199').

card_in_set('haunted guardian', 'AVR').
card_original_type('haunted guardian'/'AVR', 'Artifact Creature — Construct').
card_original_text('haunted guardian'/'AVR', 'Defender, first strike').
card_first_print('haunted guardian', 'AVR').
card_image_name('haunted guardian'/'AVR', 'haunted guardian').
card_uid('haunted guardian'/'AVR', 'AVR:Haunted Guardian:haunted guardian').
card_rarity('haunted guardian'/'AVR', 'Uncommon').
card_artist('haunted guardian'/'AVR', 'Daniel Ljunggren').
card_number('haunted guardian'/'AVR', '216').
card_flavor_text('haunted guardian'/'AVR', '\"Drain the victim\'s blood, sell the corpse, and use the soul on guard duty. No muss, no fuss, no problems.\"\n—Olivia Voldaren').
card_multiverse_id('haunted guardian'/'AVR', '279613').

card_in_set('havengul skaab', 'AVR').
card_original_type('havengul skaab'/'AVR', 'Creature — Zombie Horror').
card_original_text('havengul skaab'/'AVR', 'Whenever Havengul Skaab attacks, return another creature you control to its owner\'s hand.').
card_first_print('havengul skaab', 'AVR').
card_image_name('havengul skaab'/'AVR', 'havengul skaab').
card_uid('havengul skaab'/'AVR', 'AVR:Havengul Skaab:havengul skaab').
card_rarity('havengul skaab'/'AVR', 'Common').
card_artist('havengul skaab'/'AVR', 'Vincent Proce').
card_number('havengul skaab'/'AVR', '60').
card_flavor_text('havengul skaab'/'AVR', 'An abomination so violent that the other fiends tremble at the sight of it.').
card_multiverse_id('havengul skaab'/'AVR', '240094').

card_in_set('havengul vampire', 'AVR').
card_original_type('havengul vampire'/'AVR', 'Creature — Vampire').
card_original_text('havengul vampire'/'AVR', 'Whenever Havengul Vampire deals combat damage to a player, put a +1/+1 counter on it.\nWhenever another creature dies, put a +1/+1 counter on Havengul Vampire.').
card_first_print('havengul vampire', 'AVR').
card_image_name('havengul vampire'/'AVR', 'havengul vampire').
card_uid('havengul vampire'/'AVR', 'AVR:Havengul Vampire:havengul vampire').
card_rarity('havengul vampire'/'AVR', 'Uncommon').
card_artist('havengul vampire'/'AVR', 'James Ryman').
card_number('havengul vampire'/'AVR', '139').
card_flavor_text('havengul vampire'/'AVR', '\"That slowly decreasing pulse—that\'s the beat I waltz to.\"').
card_multiverse_id('havengul vampire'/'AVR', '240152').

card_in_set('heirs of stromkirk', 'AVR').
card_original_type('heirs of stromkirk'/'AVR', 'Creature — Vampire').
card_original_text('heirs of stromkirk'/'AVR', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)\nWhenever Heirs of Stromkirk deals combat damage to a player, put a +1/+1 counter on it.').
card_first_print('heirs of stromkirk', 'AVR').
card_image_name('heirs of stromkirk'/'AVR', 'heirs of stromkirk').
card_uid('heirs of stromkirk'/'AVR', 'AVR:Heirs of Stromkirk:heirs of stromkirk').
card_rarity('heirs of stromkirk'/'AVR', 'Common').
card_artist('heirs of stromkirk'/'AVR', 'Winona Nelson').
card_number('heirs of stromkirk'/'AVR', '140').
card_multiverse_id('heirs of stromkirk'/'AVR', '240201').

card_in_set('herald of war', 'AVR').
card_original_type('herald of war'/'AVR', 'Creature — Angel').
card_original_text('herald of war'/'AVR', 'Flying\nWhenever Herald of War attacks, put a +1/+1 counter on it. \nAngel spells and Human spells you cast cost {1} less to cast for each +1/+1 counter on Herald of War.').
card_first_print('herald of war', 'AVR').
card_image_name('herald of war'/'AVR', 'herald of war').
card_uid('herald of war'/'AVR', 'AVR:Herald of War:herald of war').
card_rarity('herald of war'/'AVR', 'Rare').
card_artist('herald of war'/'AVR', 'Eric Deschamps').
card_number('herald of war'/'AVR', '24').
card_multiverse_id('herald of war'/'AVR', '240161').

card_in_set('holy justiciar', 'AVR').
card_original_type('holy justiciar'/'AVR', 'Creature — Human Cleric').
card_original_text('holy justiciar'/'AVR', '{2}{W}, {T}: Tap target creature. If that creature is a Zombie, exile it.').
card_first_print('holy justiciar', 'AVR').
card_image_name('holy justiciar'/'AVR', 'holy justiciar').
card_uid('holy justiciar'/'AVR', 'AVR:Holy Justiciar:holy justiciar').
card_rarity('holy justiciar'/'AVR', 'Uncommon').
card_artist('holy justiciar'/'AVR', 'David Rapoza').
card_number('holy justiciar'/'AVR', '25').
card_flavor_text('holy justiciar'/'AVR', 'Her eyes see through the rotting flesh and glimpse a soul in agony, begging for the Blessed Sleep.').
card_multiverse_id('holy justiciar'/'AVR', '240025').

card_in_set('homicidal seclusion', 'AVR').
card_original_type('homicidal seclusion'/'AVR', 'Enchantment').
card_original_text('homicidal seclusion'/'AVR', 'As long as you control exactly one creature, that creature gets +3/+1 and has lifelink.').
card_first_print('homicidal seclusion', 'AVR').
card_image_name('homicidal seclusion'/'AVR', 'homicidal seclusion').
card_uid('homicidal seclusion'/'AVR', 'AVR:Homicidal Seclusion:homicidal seclusion').
card_rarity('homicidal seclusion'/'AVR', 'Uncommon').
card_artist('homicidal seclusion'/'AVR', 'Cliff Childs').
card_number('homicidal seclusion'/'AVR', '108').
card_flavor_text('homicidal seclusion'/'AVR', '\"Damned crows and their endless squawking . . . blocks out the voices in my head . . .\"\n—Hermit of Hanne Hall').
card_multiverse_id('homicidal seclusion'/'AVR', '240039').

card_in_set('hound of griselbrand', 'AVR').
card_original_type('hound of griselbrand'/'AVR', 'Creature — Elemental Hound').
card_original_text('hound of griselbrand'/'AVR', 'Double strike\nUndying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_first_print('hound of griselbrand', 'AVR').
card_image_name('hound of griselbrand'/'AVR', 'hound of griselbrand').
card_uid('hound of griselbrand'/'AVR', 'AVR:Hound of Griselbrand:hound of griselbrand').
card_rarity('hound of griselbrand'/'AVR', 'Rare').
card_artist('hound of griselbrand'/'AVR', 'Svetlin Velinov').
card_number('hound of griselbrand'/'AVR', '141').
card_multiverse_id('hound of griselbrand'/'AVR', '240146').

card_in_set('howlgeist', 'AVR').
card_original_type('howlgeist'/'AVR', 'Creature — Spirit Wolf').
card_original_text('howlgeist'/'AVR', 'Creatures with power less than Howlgeist\'s power can\'t block it.\nUndying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_first_print('howlgeist', 'AVR').
card_image_name('howlgeist'/'AVR', 'howlgeist').
card_uid('howlgeist'/'AVR', 'AVR:Howlgeist:howlgeist').
card_rarity('howlgeist'/'AVR', 'Uncommon').
card_artist('howlgeist'/'AVR', 'David Rapoza').
card_number('howlgeist'/'AVR', '182').
card_multiverse_id('howlgeist'/'AVR', '240102').

card_in_set('human frailty', 'AVR').
card_original_type('human frailty'/'AVR', 'Instant').
card_original_text('human frailty'/'AVR', 'Destroy target Human creature.').
card_first_print('human frailty', 'AVR').
card_image_name('human frailty'/'AVR', 'human frailty').
card_uid('human frailty'/'AVR', 'AVR:Human Frailty:human frailty').
card_rarity('human frailty'/'AVR', 'Uncommon').
card_artist('human frailty'/'AVR', 'David Palumbo').
card_number('human frailty'/'AVR', '109').
card_flavor_text('human frailty'/'AVR', '\"I have seen a hundred mortal families rise and fall. I shall outlast a thousand more.\"\n—Olivia Voldaren').
card_multiverse_id('human frailty'/'AVR', '279609').

card_in_set('hunted ghoul', 'AVR').
card_original_type('hunted ghoul'/'AVR', 'Creature — Zombie').
card_original_text('hunted ghoul'/'AVR', 'Hunted Ghoul can\'t block Humans.').
card_first_print('hunted ghoul', 'AVR').
card_image_name('hunted ghoul'/'AVR', 'hunted ghoul').
card_uid('hunted ghoul'/'AVR', 'AVR:Hunted Ghoul:hunted ghoul').
card_rarity('hunted ghoul'/'AVR', 'Common').
card_artist('hunted ghoul'/'AVR', 'Ryan Pancoast').
card_number('hunted ghoul'/'AVR', '110').
card_flavor_text('hunted ghoul'/'AVR', 'Even after Hanweir was saved, Gisa\'s lost ghouls would shamble through, still following the twisted orders of their imprisoned master.').
card_multiverse_id('hunted ghoul'/'AVR', '240057').

card_in_set('infinite reflection', 'AVR').
card_original_type('infinite reflection'/'AVR', 'Enchantment — Aura').
card_original_text('infinite reflection'/'AVR', 'Enchant creature\nWhen Infinite Reflection enters the battlefield attached to a creature, each other nontoken creature you control becomes a copy of that creature.\nNontoken creatures you control enter the battlefield as a copy of enchanted creature.').
card_first_print('infinite reflection', 'AVR').
card_image_name('infinite reflection'/'AVR', 'infinite reflection').
card_uid('infinite reflection'/'AVR', 'AVR:Infinite Reflection:infinite reflection').
card_rarity('infinite reflection'/'AVR', 'Rare').
card_artist('infinite reflection'/'AVR', 'Igor Kieryluk').
card_number('infinite reflection'/'AVR', '61').
card_multiverse_id('infinite reflection'/'AVR', '271117').

card_in_set('into the void', 'AVR').
card_original_type('into the void'/'AVR', 'Sorcery').
card_original_text('into the void'/'AVR', 'Return up to two target creatures to their owners\' hands.').
card_first_print('into the void', 'AVR').
card_image_name('into the void'/'AVR', 'into the void').
card_uid('into the void'/'AVR', 'AVR:Into the Void:into the void').
card_rarity('into the void'/'AVR', 'Uncommon').
card_artist('into the void'/'AVR', 'Daarken').
card_number('into the void'/'AVR', '62').
card_flavor_text('into the void'/'AVR', '\"The cathars have their swords, the inquisitors their axes. I prefer the ‘diplomatic\' approach.\"\n—Terhold, archmage of Drunau').
card_multiverse_id('into the void'/'AVR', '278064').

card_in_set('island', 'AVR').
card_original_type('island'/'AVR', 'Basic Land — Island').
card_original_text('island'/'AVR', 'U').
card_image_name('island'/'AVR', 'island1').
card_uid('island'/'AVR', 'AVR:Island:island1').
card_rarity('island'/'AVR', 'Basic Land').
card_artist('island'/'AVR', 'James Paick').
card_number('island'/'AVR', '233').
card_multiverse_id('island'/'AVR', '269639').

card_in_set('island', 'AVR').
card_original_type('island'/'AVR', 'Basic Land — Island').
card_original_text('island'/'AVR', 'U').
card_image_name('island'/'AVR', 'island2').
card_uid('island'/'AVR', 'AVR:Island:island2').
card_rarity('island'/'AVR', 'Basic Land').
card_artist('island'/'AVR', 'Adam Paquette').
card_number('island'/'AVR', '234').
card_multiverse_id('island'/'AVR', '269625').

card_in_set('island', 'AVR').
card_original_type('island'/'AVR', 'Basic Land — Island').
card_original_text('island'/'AVR', 'U').
card_image_name('island'/'AVR', 'island3').
card_uid('island'/'AVR', 'AVR:Island:island3').
card_rarity('island'/'AVR', 'Basic Land').
card_artist('island'/'AVR', 'Jung Park').
card_number('island'/'AVR', '235').
card_multiverse_id('island'/'AVR', '269633').

card_in_set('joint assault', 'AVR').
card_original_type('joint assault'/'AVR', 'Instant').
card_original_text('joint assault'/'AVR', 'Target creature gets +2/+2 until end of turn. If it\'s paired with a creature, that creature also gets +2/+2 until end of turn.').
card_first_print('joint assault', 'AVR').
card_image_name('joint assault'/'AVR', 'joint assault').
card_uid('joint assault'/'AVR', 'AVR:Joint Assault:joint assault').
card_rarity('joint assault'/'AVR', 'Common').
card_artist('joint assault'/'AVR', 'Raymond Swanland').
card_number('joint assault'/'AVR', '183').
card_flavor_text('joint assault'/'AVR', '\"By my power, werewolves shall become the wolfir, our allies in combat against darkness.\"\n—Avacyn\'s Cursemute Decree').
card_multiverse_id('joint assault'/'AVR', '240049').

card_in_set('kessig malcontents', 'AVR').
card_original_type('kessig malcontents'/'AVR', 'Creature — Human Warrior').
card_original_text('kessig malcontents'/'AVR', 'When Kessig Malcontents enters the battlefield, it deals damage to target player equal to the number of Humans you control.').
card_first_print('kessig malcontents', 'AVR').
card_image_name('kessig malcontents'/'AVR', 'kessig malcontents').
card_uid('kessig malcontents'/'AVR', 'AVR:Kessig Malcontents:kessig malcontents').
card_rarity('kessig malcontents'/'AVR', 'Uncommon').
card_artist('kessig malcontents'/'AVR', 'John Stanko').
card_number('kessig malcontents'/'AVR', '142').
card_flavor_text('kessig malcontents'/'AVR', 'Discontent is a powerful weapon in the hands of a mob.').
card_multiverse_id('kessig malcontents'/'AVR', '240114').

card_in_set('killing wave', 'AVR').
card_original_type('killing wave'/'AVR', 'Sorcery').
card_original_text('killing wave'/'AVR', 'For each creature, its controller sacrifices it unless he or she pays X life.').
card_image_name('killing wave'/'AVR', 'killing wave').
card_uid('killing wave'/'AVR', 'AVR:Killing Wave:killing wave').
card_rarity('killing wave'/'AVR', 'Rare').
card_artist('killing wave'/'AVR', 'Steve Argyle').
card_number('killing wave'/'AVR', '111').
card_flavor_text('killing wave'/'AVR', '\"I come looking for demons and I find a plane full of angels. I hate angels.\"\n—Liliana Vess').
card_multiverse_id('killing wave'/'AVR', '240180').

card_in_set('kruin striker', 'AVR').
card_original_type('kruin striker'/'AVR', 'Creature — Human Warrior').
card_original_text('kruin striker'/'AVR', 'Whenever another creature enters the battlefield under your control, Kruin Striker gets +1/+0 and gains trample until end of turn.').
card_first_print('kruin striker', 'AVR').
card_image_name('kruin striker'/'AVR', 'kruin striker').
card_uid('kruin striker'/'AVR', 'AVR:Kruin Striker:kruin striker').
card_rarity('kruin striker'/'AVR', 'Common').
card_artist('kruin striker'/'AVR', 'Christopher Moeller').
card_number('kruin striker'/'AVR', '143').
card_flavor_text('kruin striker'/'AVR', 'Unhappy with the creation of the wolfir, Rorica broke with her order and led a crusade against the \"reformed werewolves.\"').
card_multiverse_id('kruin striker'/'AVR', '240093').

card_in_set('lair delve', 'AVR').
card_original_type('lair delve'/'AVR', 'Sorcery').
card_original_text('lair delve'/'AVR', 'Reveal the top two cards of your library. Put all creature and land cards revealed this way into your hand and the rest on the bottom of your library in any order.').
card_first_print('lair delve', 'AVR').
card_image_name('lair delve'/'AVR', 'lair delve').
card_uid('lair delve'/'AVR', 'AVR:Lair Delve:lair delve').
card_rarity('lair delve'/'AVR', 'Common').
card_artist('lair delve'/'AVR', 'Jason A. Engle').
card_number('lair delve'/'AVR', '184').
card_flavor_text('lair delve'/'AVR', '\"The fiends have fled. Well, good riddance. Nothing I like more than an empty lair.\"').
card_multiverse_id('lair delve'/'AVR', '271093').

card_in_set('latch seeker', 'AVR').
card_original_type('latch seeker'/'AVR', 'Creature — Spirit').
card_original_text('latch seeker'/'AVR', 'Latch Seeker is unblockable.').
card_image_name('latch seeker'/'AVR', 'latch seeker').
card_uid('latch seeker'/'AVR', 'AVR:Latch Seeker:latch seeker').
card_rarity('latch seeker'/'AVR', 'Uncommon').
card_artist('latch seeker'/'AVR', 'Vincent Proce').
card_number('latch seeker'/'AVR', '63').
card_flavor_text('latch seeker'/'AVR', 'It seeks a tomb that will hold it, a coffin that will give it rest.').
card_multiverse_id('latch seeker'/'AVR', '240005').

card_in_set('leap of faith', 'AVR').
card_original_type('leap of faith'/'AVR', 'Instant').
card_original_text('leap of faith'/'AVR', 'Target creature gains flying until end of turn. Prevent all damage that would be dealt to that creature this turn.').
card_first_print('leap of faith', 'AVR').
card_image_name('leap of faith'/'AVR', 'leap of faith').
card_uid('leap of faith'/'AVR', 'AVR:Leap of Faith:leap of faith').
card_rarity('leap of faith'/'AVR', 'Common').
card_artist('leap of faith'/'AVR', 'Gabor Szikszai').
card_number('leap of faith'/'AVR', '26').
card_flavor_text('leap of faith'/'AVR', 'The finest maneuvers take place in three dimensions.').
card_multiverse_id('leap of faith'/'AVR', '240023').

card_in_set('lightning mauler', 'AVR').
card_original_type('lightning mauler'/'AVR', 'Creature — Human Berserker').
card_original_text('lightning mauler'/'AVR', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Lightning Mauler is paired with another creature, both creatures have haste.').
card_first_print('lightning mauler', 'AVR').
card_image_name('lightning mauler'/'AVR', 'lightning mauler').
card_uid('lightning mauler'/'AVR', 'AVR:Lightning Mauler:lightning mauler').
card_rarity('lightning mauler'/'AVR', 'Uncommon').
card_artist('lightning mauler'/'AVR', 'Dave Kendall').
card_number('lightning mauler'/'AVR', '144').
card_multiverse_id('lightning mauler'/'AVR', '271119').

card_in_set('lightning prowess', 'AVR').
card_original_type('lightning prowess'/'AVR', 'Enchantment — Aura').
card_original_text('lightning prowess'/'AVR', 'Enchant creature\nEnchanted creature has haste and \"{T}: This creature deals 1 damage to target creature or player.\"').
card_first_print('lightning prowess', 'AVR').
card_image_name('lightning prowess'/'AVR', 'lightning prowess').
card_uid('lightning prowess'/'AVR', 'AVR:Lightning Prowess:lightning prowess').
card_rarity('lightning prowess'/'AVR', 'Uncommon').
card_artist('lightning prowess'/'AVR', 'David Rapoza').
card_number('lightning prowess'/'AVR', '145').
card_flavor_text('lightning prowess'/'AVR', '\"Undead flesh is dry and papery. A single spark and ‘poof.\' No more ghoul.\"').
card_multiverse_id('lightning prowess'/'AVR', '240086').

card_in_set('lone revenant', 'AVR').
card_original_type('lone revenant'/'AVR', 'Creature — Spirit').
card_original_text('lone revenant'/'AVR', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)\nWhenever Lone Revenant deals combat damage to a player, if you control no other creatures, look at the top four cards of your library. Put one of them into your hand and the rest on the bottom of your library in any order.').
card_first_print('lone revenant', 'AVR').
card_image_name('lone revenant'/'AVR', 'lone revenant').
card_uid('lone revenant'/'AVR', 'AVR:Lone Revenant:lone revenant').
card_rarity('lone revenant'/'AVR', 'Rare').
card_artist('lone revenant'/'AVR', 'Jaime Jones').
card_number('lone revenant'/'AVR', '64').
card_multiverse_id('lone revenant'/'AVR', '275706').

card_in_set('lunar mystic', 'AVR').
card_original_type('lunar mystic'/'AVR', 'Creature — Human Wizard').
card_original_text('lunar mystic'/'AVR', 'Whenever you cast an instant spell, you may pay {1}. If you do, draw a card.').
card_first_print('lunar mystic', 'AVR').
card_image_name('lunar mystic'/'AVR', 'lunar mystic').
card_uid('lunar mystic'/'AVR', 'AVR:Lunar Mystic:lunar mystic').
card_rarity('lunar mystic'/'AVR', 'Rare').
card_artist('lunar mystic'/'AVR', 'Wesley Burt').
card_number('lunar mystic'/'AVR', '65').
card_flavor_text('lunar mystic'/'AVR', '\"I\'m pleased this world has learned the moon affects more than the tides.\"\n—Tamiyo, the Moon Sage').
card_multiverse_id('lunar mystic'/'AVR', '275717').

card_in_set('maalfeld twins', 'AVR').
card_original_type('maalfeld twins'/'AVR', 'Creature — Zombie').
card_original_text('maalfeld twins'/'AVR', 'When Maalfeld Twins dies, put two 2/2 black Zombie creature tokens onto the battlefield.').
card_first_print('maalfeld twins', 'AVR').
card_image_name('maalfeld twins'/'AVR', 'maalfeld twins').
card_uid('maalfeld twins'/'AVR', 'AVR:Maalfeld Twins:maalfeld twins').
card_rarity('maalfeld twins'/'AVR', 'Uncommon').
card_artist('maalfeld twins'/'AVR', 'Mike Sass').
card_number('maalfeld twins'/'AVR', '112').
card_flavor_text('maalfeld twins'/'AVR', '\"Who am I to break up a family?\"\n—Gisa the Mad').
card_multiverse_id('maalfeld twins'/'AVR', '240012').

card_in_set('mad prophet', 'AVR').
card_original_type('mad prophet'/'AVR', 'Creature — Human Shaman').
card_original_text('mad prophet'/'AVR', 'Haste\n{T}, Discard a card: Draw a card.').
card_first_print('mad prophet', 'AVR').
card_image_name('mad prophet'/'AVR', 'mad prophet').
card_uid('mad prophet'/'AVR', 'AVR:Mad Prophet:mad prophet').
card_rarity('mad prophet'/'AVR', 'Common').
card_artist('mad prophet'/'AVR', 'Wayne Reynolds').
card_number('mad prophet'/'AVR', '146').
card_flavor_text('mad prophet'/'AVR', '\"There\'s no heron in the moon! It\'s a shrew, a five-legged shrew, with a voice like whispering thunder!\"').
card_multiverse_id('mad prophet'/'AVR', '240107').

card_in_set('malicious intent', 'AVR').
card_original_type('malicious intent'/'AVR', 'Enchantment — Aura').
card_original_text('malicious intent'/'AVR', 'Enchant creature\nEnchanted creature has \"{T}: Target creature can\'t block this turn.\"').
card_first_print('malicious intent', 'AVR').
card_image_name('malicious intent'/'AVR', 'malicious intent').
card_uid('malicious intent'/'AVR', 'AVR:Malicious Intent:malicious intent').
card_rarity('malicious intent'/'AVR', 'Common').
card_artist('malicious intent'/'AVR', 'Kev Walker').
card_number('malicious intent'/'AVR', '147').
card_flavor_text('malicious intent'/'AVR', 'As peace returned to Thraben, some cathars made the mistake of letting down their guard.').
card_multiverse_id('malicious intent'/'AVR', '279615').

card_in_set('malignus', 'AVR').
card_original_type('malignus'/'AVR', 'Creature — Elemental Spirit').
card_original_text('malignus'/'AVR', 'Malignus\'s power and toughness are each equal to half the highest life total among your opponents, rounded up.\nDamage that would be dealt by Malignus can\'t be prevented.').
card_first_print('malignus', 'AVR').
card_image_name('malignus'/'AVR', 'malignus').
card_uid('malignus'/'AVR', 'AVR:Malignus:malignus').
card_rarity('malignus'/'AVR', 'Mythic Rare').
card_artist('malignus'/'AVR', 'Jung Park').
card_number('malignus'/'AVR', '148').
card_flavor_text('malignus'/'AVR', 'It descended on Estwald like a sudden storm. Moments later, only ashes and agony remained.').
card_multiverse_id('malignus'/'AVR', '240062').

card_in_set('marrow bats', 'AVR').
card_original_type('marrow bats'/'AVR', 'Creature — Bat Skeleton').
card_original_text('marrow bats'/'AVR', 'Flying\nPay 4 life: Regenerate Marrow Bats.').
card_first_print('marrow bats', 'AVR').
card_image_name('marrow bats'/'AVR', 'marrow bats').
card_uid('marrow bats'/'AVR', 'AVR:Marrow Bats:marrow bats').
card_rarity('marrow bats'/'AVR', 'Uncommon').
card_artist('marrow bats'/'AVR', 'Jason A. Engle').
card_number('marrow bats'/'AVR', '113').
card_flavor_text('marrow bats'/'AVR', '\"No matter how far we push into Stensia, undeath will always remain in these lands.\"\n—Terhold, archmage of Drunau').
card_multiverse_id('marrow bats'/'AVR', '240163').

card_in_set('mass appeal', 'AVR').
card_original_type('mass appeal'/'AVR', 'Sorcery').
card_original_text('mass appeal'/'AVR', 'Draw a card for each Human you control.').
card_first_print('mass appeal', 'AVR').
card_image_name('mass appeal'/'AVR', 'mass appeal').
card_uid('mass appeal'/'AVR', 'AVR:Mass Appeal:mass appeal').
card_rarity('mass appeal'/'AVR', 'Uncommon').
card_artist('mass appeal'/'AVR', 'Christopher Moeller').
card_number('mass appeal'/'AVR', '66').
card_flavor_text('mass appeal'/'AVR', '\"We have emerged triumphant from the darkness. Let our hard-won wisdom guide us to prosperity.\"').
card_multiverse_id('mass appeal'/'AVR', '240095').

card_in_set('mental agony', 'AVR').
card_original_type('mental agony'/'AVR', 'Sorcery').
card_original_text('mental agony'/'AVR', 'Target player discards two cards and loses 2 life.').
card_first_print('mental agony', 'AVR').
card_image_name('mental agony'/'AVR', 'mental agony').
card_uid('mental agony'/'AVR', 'AVR:Mental Agony:mental agony').
card_rarity('mental agony'/'AVR', 'Common').
card_artist('mental agony'/'AVR', 'Greg Staples').
card_number('mental agony'/'AVR', '114').
card_flavor_text('mental agony'/'AVR', '\"His mind is feeble. Feed him to the bats.\"').
card_multiverse_id('mental agony'/'AVR', '239966').

card_in_set('midnight duelist', 'AVR').
card_original_type('midnight duelist'/'AVR', 'Creature — Human Soldier').
card_original_text('midnight duelist'/'AVR', 'Protection from Vampires').
card_first_print('midnight duelist', 'AVR').
card_image_name('midnight duelist'/'AVR', 'midnight duelist').
card_uid('midnight duelist'/'AVR', 'AVR:Midnight Duelist:midnight duelist').
card_rarity('midnight duelist'/'AVR', 'Common').
card_artist('midnight duelist'/'AVR', 'Bud Cook').
card_number('midnight duelist'/'AVR', '27').
card_flavor_text('midnight duelist'/'AVR', 'Avacyn\'s return did not rid him of his desire for revenge.').
card_multiverse_id('midnight duelist'/'AVR', '239994').

card_in_set('midvast protector', 'AVR').
card_original_type('midvast protector'/'AVR', 'Creature — Human Wizard').
card_original_text('midvast protector'/'AVR', 'When Midvast Protector enters the battlefield, target creature you control gains protection from the color of your choice until end of turn.').
card_first_print('midvast protector', 'AVR').
card_image_name('midvast protector'/'AVR', 'midvast protector').
card_uid('midvast protector'/'AVR', 'AVR:Midvast Protector:midvast protector').
card_rarity('midvast protector'/'AVR', 'Common').
card_artist('midvast protector'/'AVR', 'James Ryman').
card_number('midvast protector'/'AVR', '28').
card_flavor_text('midvast protector'/'AVR', '\"I cultivated my faith in Avacyn despite meager returns. Now my faith is repaid.\"').
card_multiverse_id('midvast protector'/'AVR', '278066').

card_in_set('mist raven', 'AVR').
card_original_type('mist raven'/'AVR', 'Creature — Bird').
card_original_text('mist raven'/'AVR', 'Flying\nWhen Mist Raven enters the battlefield, return target creature to its owner\'s hand.').
card_first_print('mist raven', 'AVR').
card_image_name('mist raven'/'AVR', 'mist raven').
card_uid('mist raven'/'AVR', 'AVR:Mist Raven:mist raven').
card_rarity('mist raven'/'AVR', 'Common').
card_artist('mist raven'/'AVR', 'John Avon').
card_number('mist raven'/'AVR', '67').
card_flavor_text('mist raven'/'AVR', '\"When you see a raven, head for the hills. It knows when the ghost-tide is headed in.\"\n—Sved, fisherman of Nephalia').
card_multiverse_id('mist raven'/'AVR', '271121').

card_in_set('misthollow griffin', 'AVR').
card_original_type('misthollow griffin'/'AVR', 'Creature — Griffin').
card_original_text('misthollow griffin'/'AVR', 'Flying\nYou may cast Misthollow Griffin from exile.').
card_first_print('misthollow griffin', 'AVR').
card_image_name('misthollow griffin'/'AVR', 'misthollow griffin').
card_uid('misthollow griffin'/'AVR', 'AVR:Misthollow Griffin:misthollow griffin').
card_rarity('misthollow griffin'/'AVR', 'Mythic Rare').
card_artist('misthollow griffin'/'AVR', 'Jaime Jones').
card_number('misthollow griffin'/'AVR', '68').
card_flavor_text('misthollow griffin'/'AVR', 'For some creatures, existing is only a whim.').
card_multiverse_id('misthollow griffin'/'AVR', '276504').

card_in_set('moonlight geist', 'AVR').
card_original_type('moonlight geist'/'AVR', 'Creature — Spirit').
card_original_text('moonlight geist'/'AVR', 'Flying\n{3}{W}: Prevent all combat damage that would be dealt to and dealt by Moonlight Geist this turn.').
card_first_print('moonlight geist', 'AVR').
card_image_name('moonlight geist'/'AVR', 'moonlight geist').
card_uid('moonlight geist'/'AVR', 'AVR:Moonlight Geist:moonlight geist').
card_rarity('moonlight geist'/'AVR', 'Common').
card_artist('moonlight geist'/'AVR', 'Dan Scott').
card_number('moonlight geist'/'AVR', '29').
card_flavor_text('moonlight geist'/'AVR', 'Wails and whispers are the only weapons she has left.').
card_multiverse_id('moonlight geist'/'AVR', '278059').

card_in_set('moonsilver spear', 'AVR').
card_original_type('moonsilver spear'/'AVR', 'Artifact — Equipment').
card_original_text('moonsilver spear'/'AVR', 'Equipped creature has first strike.\nWhenever equipped creature attacks, put a 4/4 white Angel creature token with flying onto the battlefield.\nEquip {4}').
card_image_name('moonsilver spear'/'AVR', 'moonsilver spear').
card_uid('moonsilver spear'/'AVR', 'AVR:Moonsilver Spear:moonsilver spear').
card_rarity('moonsilver spear'/'AVR', 'Rare').
card_artist('moonsilver spear'/'AVR', 'James Paick').
card_number('moonsilver spear'/'AVR', '217').
card_multiverse_id('moonsilver spear'/'AVR', '240065').

card_in_set('moorland inquisitor', 'AVR').
card_original_type('moorland inquisitor'/'AVR', 'Creature — Human Soldier').
card_original_text('moorland inquisitor'/'AVR', '{2}{W}: Moorland Inquisitor gains first strike until end of turn.').
card_first_print('moorland inquisitor', 'AVR').
card_image_name('moorland inquisitor'/'AVR', 'moorland inquisitor').
card_uid('moorland inquisitor'/'AVR', 'AVR:Moorland Inquisitor:moorland inquisitor').
card_rarity('moorland inquisitor'/'AVR', 'Common').
card_artist('moorland inquisitor'/'AVR', 'David Palumbo').
card_number('moorland inquisitor'/'AVR', '30').
card_flavor_text('moorland inquisitor'/'AVR', 'Inquisitors are taught scripture, philosophy, and the fine art of sharpening an axe.').
card_multiverse_id('moorland inquisitor'/'AVR', '262849').

card_in_set('mountain', 'AVR').
card_original_type('mountain'/'AVR', 'Basic Land — Mountain').
card_original_text('mountain'/'AVR', 'B').
card_image_name('mountain'/'AVR', 'mountain1').
card_uid('mountain'/'AVR', 'AVR:Mountain:mountain1').
card_rarity('mountain'/'AVR', 'Basic Land').
card_artist('mountain'/'AVR', 'James Paick').
card_number('mountain'/'AVR', '239').
card_multiverse_id('mountain'/'AVR', '269632').

card_in_set('mountain', 'AVR').
card_original_type('mountain'/'AVR', 'Basic Land — Mountain').
card_original_text('mountain'/'AVR', 'R').
card_image_name('mountain'/'AVR', 'mountain2').
card_uid('mountain'/'AVR', 'AVR:Mountain:mountain2').
card_rarity('mountain'/'AVR', 'Basic Land').
card_artist('mountain'/'AVR', 'Adam Paquette').
card_number('mountain'/'AVR', '240').
card_multiverse_id('mountain'/'AVR', '269630').

card_in_set('mountain', 'AVR').
card_original_type('mountain'/'AVR', 'Basic Land — Mountain').
card_original_text('mountain'/'AVR', 'R').
card_image_name('mountain'/'AVR', 'mountain3').
card_uid('mountain'/'AVR', 'AVR:Mountain:mountain3').
card_rarity('mountain'/'AVR', 'Basic Land').
card_artist('mountain'/'AVR', 'Eytan Zana').
card_number('mountain'/'AVR', '241').
card_multiverse_id('mountain'/'AVR', '269622').

card_in_set('narstad scrapper', 'AVR').
card_original_type('narstad scrapper'/'AVR', 'Artifact Creature — Construct').
card_original_text('narstad scrapper'/'AVR', '{2}: Narstad Scrapper gets +1/+0 until end of turn.').
card_first_print('narstad scrapper', 'AVR').
card_image_name('narstad scrapper'/'AVR', 'narstad scrapper').
card_uid('narstad scrapper'/'AVR', 'AVR:Narstad Scrapper:narstad scrapper').
card_rarity('narstad scrapper'/'AVR', 'Common').
card_artist('narstad scrapper'/'AVR', 'Steven Belledin').
card_number('narstad scrapper'/'AVR', '218').
card_flavor_text('narstad scrapper'/'AVR', '\"Finally, the principles of corpse animation applied to bloodless materials!\"\n—Ludevic, necro-alchemist').
card_multiverse_id('narstad scrapper'/'AVR', '279607').

card_in_set('natural end', 'AVR').
card_original_type('natural end'/'AVR', 'Instant').
card_original_text('natural end'/'AVR', 'Destroy target artifact or enchantment. You gain 3 life.').
card_first_print('natural end', 'AVR').
card_image_name('natural end'/'AVR', 'natural end').
card_uid('natural end'/'AVR', 'AVR:Natural End:natural end').
card_rarity('natural end'/'AVR', 'Common').
card_artist('natural end'/'AVR', 'Scott Chou').
card_number('natural end'/'AVR', '185').
card_flavor_text('natural end'/'AVR', 'The haunted blade shattered, and the geist drifted gratefully to the Blessed Sleep.').
card_multiverse_id('natural end'/'AVR', '240076').

card_in_set('nearheath pilgrim', 'AVR').
card_original_type('nearheath pilgrim'/'AVR', 'Creature — Human Cleric').
card_original_text('nearheath pilgrim'/'AVR', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Nearheath Pilgrim is paired with another creature, both creatures have lifelink.').
card_first_print('nearheath pilgrim', 'AVR').
card_image_name('nearheath pilgrim'/'AVR', 'nearheath pilgrim').
card_uid('nearheath pilgrim'/'AVR', 'AVR:Nearheath Pilgrim:nearheath pilgrim').
card_rarity('nearheath pilgrim'/'AVR', 'Uncommon').
card_artist('nearheath pilgrim'/'AVR', 'Erica Yang').
card_number('nearheath pilgrim'/'AVR', '31').
card_multiverse_id('nearheath pilgrim'/'AVR', '240132').

card_in_set('necrobite', 'AVR').
card_original_type('necrobite'/'AVR', 'Instant').
card_original_text('necrobite'/'AVR', 'Target creature gains deathtouch until end of turn. Regenerate it.').
card_first_print('necrobite', 'AVR').
card_image_name('necrobite'/'AVR', 'necrobite').
card_uid('necrobite'/'AVR', 'AVR:Necrobite:necrobite').
card_rarity('necrobite'/'AVR', 'Common').
card_artist('necrobite'/'AVR', 'Nils Hamm').
card_number('necrobite'/'AVR', '115').
card_flavor_text('necrobite'/'AVR', 'An undead snake doesn\'t bite in self-defense. It hungers as any zombie, never sated.').
card_multiverse_id('necrobite'/'AVR', '240187').

card_in_set('nephalia smuggler', 'AVR').
card_original_type('nephalia smuggler'/'AVR', 'Creature — Human Rogue').
card_original_text('nephalia smuggler'/'AVR', '{3}{U}, {T}: Exile another target creature you control, then return that card to the battlefield under your control.').
card_first_print('nephalia smuggler', 'AVR').
card_image_name('nephalia smuggler'/'AVR', 'nephalia smuggler').
card_uid('nephalia smuggler'/'AVR', 'AVR:Nephalia Smuggler:nephalia smuggler').
card_rarity('nephalia smuggler'/'AVR', 'Uncommon').
card_artist('nephalia smuggler'/'AVR', 'Matt Stewart').
card_number('nephalia smuggler'/'AVR', '69').
card_flavor_text('nephalia smuggler'/'AVR', '\"My drivers are trustworthy. I removed their tongues myself. Any other questions?\"').
card_multiverse_id('nephalia smuggler'/'AVR', '240159').

card_in_set('nettle swine', 'AVR').
card_original_type('nettle swine'/'AVR', 'Creature — Boar').
card_original_text('nettle swine'/'AVR', '').
card_first_print('nettle swine', 'AVR').
card_image_name('nettle swine'/'AVR', 'nettle swine').
card_uid('nettle swine'/'AVR', 'AVR:Nettle Swine:nettle swine').
card_rarity('nettle swine'/'AVR', 'Common').
card_artist('nettle swine'/'AVR', 'Christopher Moeller').
card_number('nettle swine'/'AVR', '186').
card_flavor_text('nettle swine'/'AVR', '\"I killed one and found bricks and bones in its belly. It had eaten a whole cottage, thatch and all.\"\n—Paulin, Somberwald trapper').
card_multiverse_id('nettle swine'/'AVR', '239978').

card_in_set('nightshade peddler', 'AVR').
card_original_type('nightshade peddler'/'AVR', 'Creature — Human Druid').
card_original_text('nightshade peddler'/'AVR', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Nightshade Peddler is paired with another creature, both creatures have deathtouch.').
card_first_print('nightshade peddler', 'AVR').
card_image_name('nightshade peddler'/'AVR', 'nightshade peddler').
card_uid('nightshade peddler'/'AVR', 'AVR:Nightshade Peddler:nightshade peddler').
card_rarity('nightshade peddler'/'AVR', 'Common').
card_artist('nightshade peddler'/'AVR', 'John Stanko').
card_number('nightshade peddler'/'AVR', '187').
card_multiverse_id('nightshade peddler'/'AVR', '240045').

card_in_set('otherworld atlas', 'AVR').
card_original_type('otherworld atlas'/'AVR', 'Artifact').
card_original_text('otherworld atlas'/'AVR', '{T}: Put a charge counter on Otherworld Atlas.\n{T}: Each player draws a card for each charge counter on Otherworld Atlas.').
card_first_print('otherworld atlas', 'AVR').
card_image_name('otherworld atlas'/'AVR', 'otherworld atlas').
card_uid('otherworld atlas'/'AVR', 'AVR:Otherworld Atlas:otherworld atlas').
card_rarity('otherworld atlas'/'AVR', 'Rare').
card_artist('otherworld atlas'/'AVR', 'Sam Wolfe Connelly').
card_number('otherworld atlas'/'AVR', '219').
card_flavor_text('otherworld atlas'/'AVR', '\"Any fool can open it. But it takes a genius to decipher the blank pages.\"\n—Kordel the Cryptic').
card_multiverse_id('otherworld atlas'/'AVR', '240052').

card_in_set('outwit', 'AVR').
card_original_type('outwit'/'AVR', 'Instant').
card_original_text('outwit'/'AVR', 'Counter target spell that targets a player.').
card_first_print('outwit', 'AVR').
card_image_name('outwit'/'AVR', 'outwit').
card_uid('outwit'/'AVR', 'AVR:Outwit:outwit').
card_rarity('outwit'/'AVR', 'Common').
card_artist('outwit'/'AVR', 'Erica Yang').
card_number('outwit'/'AVR', '70').
card_flavor_text('outwit'/'AVR', 'The student asks how a spell is begun. The master ponders what ends it.').
card_multiverse_id('outwit'/'AVR', '240050').

card_in_set('pathbreaker wurm', 'AVR').
card_original_type('pathbreaker wurm'/'AVR', 'Creature — Wurm').
card_original_text('pathbreaker wurm'/'AVR', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Pathbreaker Wurm is paired with another creature, both creatures have trample.').
card_first_print('pathbreaker wurm', 'AVR').
card_image_name('pathbreaker wurm'/'AVR', 'pathbreaker wurm').
card_uid('pathbreaker wurm'/'AVR', 'AVR:Pathbreaker Wurm:pathbreaker wurm').
card_rarity('pathbreaker wurm'/'AVR', 'Common').
card_artist('pathbreaker wurm'/'AVR', 'Nils Hamm').
card_number('pathbreaker wurm'/'AVR', '188').
card_multiverse_id('pathbreaker wurm'/'AVR', '240103').

card_in_set('peel from reality', 'AVR').
card_original_type('peel from reality'/'AVR', 'Instant').
card_original_text('peel from reality'/'AVR', 'Return target creature you control and target creature you don\'t control to their owners\' hands.').
card_image_name('peel from reality'/'AVR', 'peel from reality').
card_uid('peel from reality'/'AVR', 'AVR:Peel from Reality:peel from reality').
card_rarity('peel from reality'/'AVR', 'Common').
card_artist('peel from reality'/'AVR', 'Jason Felix').
card_number('peel from reality'/'AVR', '71').
card_flavor_text('peel from reality'/'AVR', '\"Soulless demon, you are bound to me. Now we will both dwell in oblivion.\"').
card_multiverse_id('peel from reality'/'AVR', '240141').

card_in_set('pillar of flame', 'AVR').
card_original_type('pillar of flame'/'AVR', 'Sorcery').
card_original_text('pillar of flame'/'AVR', 'Pillar of Flame deals 2 damage to target creature or player. If a creature dealt damage this way would die this turn, exile it instead.').
card_image_name('pillar of flame'/'AVR', 'pillar of flame').
card_uid('pillar of flame'/'AVR', 'AVR:Pillar of Flame:pillar of flame').
card_rarity('pillar of flame'/'AVR', 'Common').
card_artist('pillar of flame'/'AVR', 'Karl Kopinski').
card_number('pillar of flame'/'AVR', '149').
card_flavor_text('pillar of flame'/'AVR', '\"May the worthy spend an eternity in Blessed Sleep. May the wicked find the peace of oblivion.\"').
card_multiverse_id('pillar of flame'/'AVR', '240013').

card_in_set('plains', 'AVR').
card_original_type('plains'/'AVR', 'Basic Land — Plains').
card_original_text('plains'/'AVR', 'W').
card_image_name('plains'/'AVR', 'plains1').
card_uid('plains'/'AVR', 'AVR:Plains:plains1').
card_rarity('plains'/'AVR', 'Basic Land').
card_artist('plains'/'AVR', 'Adam Paquette').
card_number('plains'/'AVR', '230').
card_multiverse_id('plains'/'AVR', '269637').

card_in_set('plains', 'AVR').
card_original_type('plains'/'AVR', 'Basic Land — Plains').
card_original_text('plains'/'AVR', 'W').
card_image_name('plains'/'AVR', 'plains2').
card_uid('plains'/'AVR', 'AVR:Plains:plains2').
card_rarity('plains'/'AVR', 'Basic Land').
card_artist('plains'/'AVR', 'Jung Park').
card_number('plains'/'AVR', '231').
card_multiverse_id('plains'/'AVR', '269634').

card_in_set('plains', 'AVR').
card_original_type('plains'/'AVR', 'Basic Land — Plains').
card_original_text('plains'/'AVR', 'W').
card_image_name('plains'/'AVR', 'plains3').
card_uid('plains'/'AVR', 'AVR:Plains:plains3').
card_rarity('plains'/'AVR', 'Basic Land').
card_artist('plains'/'AVR', 'Eytan Zana').
card_number('plains'/'AVR', '232').
card_multiverse_id('plains'/'AVR', '269638').

card_in_set('polluted dead', 'AVR').
card_original_type('polluted dead'/'AVR', 'Creature — Zombie').
card_original_text('polluted dead'/'AVR', 'When Polluted Dead dies, destroy target land.').
card_first_print('polluted dead', 'AVR').
card_image_name('polluted dead'/'AVR', 'polluted dead').
card_uid('polluted dead'/'AVR', 'AVR:Polluted Dead:polluted dead').
card_rarity('polluted dead'/'AVR', 'Common').
card_artist('polluted dead'/'AVR', 'Jason A. Engle').
card_number('polluted dead'/'AVR', '116').
card_flavor_text('polluted dead'/'AVR', 'After the zombie attack, crops withered on the vine, and the thriving village became a ghost town almost overnight.').
card_multiverse_id('polluted dead'/'AVR', '276197').

card_in_set('predator\'s gambit', 'AVR').
card_original_type('predator\'s gambit'/'AVR', 'Enchantment — Aura').
card_original_text('predator\'s gambit'/'AVR', 'Enchant creature\nEnchanted creature gets +2/+1.\nEnchanted creature has intimidate as long as its controller controls no other creatures. (It can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)').
card_first_print('predator\'s gambit', 'AVR').
card_image_name('predator\'s gambit'/'AVR', 'predator\'s gambit').
card_uid('predator\'s gambit'/'AVR', 'AVR:Predator\'s Gambit:predator\'s gambit').
card_rarity('predator\'s gambit'/'AVR', 'Common').
card_artist('predator\'s gambit'/'AVR', 'Zoltan Boros').
card_number('predator\'s gambit'/'AVR', '117').
card_multiverse_id('predator\'s gambit'/'AVR', '271122').

card_in_set('primal surge', 'AVR').
card_original_type('primal surge'/'AVR', 'Sorcery').
card_original_text('primal surge'/'AVR', 'Exile the top card of your library. If it\'s a permanent card, you may put it onto the battlefield. If you do, repeat this process.').
card_first_print('primal surge', 'AVR').
card_image_name('primal surge'/'AVR', 'primal surge').
card_uid('primal surge'/'AVR', 'AVR:Primal Surge:primal surge').
card_rarity('primal surge'/'AVR', 'Mythic Rare').
card_artist('primal surge'/'AVR', 'David Rapoza').
card_number('primal surge'/'AVR', '189').
card_flavor_text('primal surge'/'AVR', 'With Avacyn\'s return, the flow of life became a tidal wave.').
card_multiverse_id('primal surge'/'AVR', '240158').

card_in_set('raging poltergeist', 'AVR').
card_original_type('raging poltergeist'/'AVR', 'Creature — Spirit').
card_original_text('raging poltergeist'/'AVR', '').
card_first_print('raging poltergeist', 'AVR').
card_image_name('raging poltergeist'/'AVR', 'raging poltergeist').
card_uid('raging poltergeist'/'AVR', 'AVR:Raging Poltergeist:raging poltergeist').
card_rarity('raging poltergeist'/'AVR', 'Common').
card_artist('raging poltergeist'/'AVR', 'Slawomir Maniak').
card_number('raging poltergeist'/'AVR', '150').
card_flavor_text('raging poltergeist'/'AVR', 'Some tried cremating their dead to stop the ghoulcallers. But the dead returned, furious about their fate.').
card_multiverse_id('raging poltergeist'/'AVR', '240188').

card_in_set('rain of thorns', 'AVR').
card_original_type('rain of thorns'/'AVR', 'Sorcery').
card_original_text('rain of thorns'/'AVR', 'Choose one or more — Destroy target artifact; destroy target enchantment; and/or destroy target land.').
card_first_print('rain of thorns', 'AVR').
card_image_name('rain of thorns'/'AVR', 'rain of thorns').
card_uid('rain of thorns'/'AVR', 'AVR:Rain of Thorns:rain of thorns').
card_rarity('rain of thorns'/'AVR', 'Uncommon').
card_artist('rain of thorns'/'AVR', 'Sam Burley').
card_number('rain of thorns'/'AVR', '190').
card_flavor_text('rain of thorns'/'AVR', 'When the forests became havens for evil, the archmages devised new ways to cleanse the wilds.').
card_multiverse_id('rain of thorns'/'AVR', '240169').

card_in_set('reforge the soul', 'AVR').
card_original_type('reforge the soul'/'AVR', 'Sorcery').
card_original_text('reforge the soul'/'AVR', 'Each player discards his or her hand and draws seven cards.\nMiracle {1}{R} (You may cast this card for its miracle cost when you draw it if it\'s the first card you drew this turn.)').
card_first_print('reforge the soul', 'AVR').
card_image_name('reforge the soul'/'AVR', 'reforge the soul').
card_uid('reforge the soul'/'AVR', 'AVR:Reforge the Soul:reforge the soul').
card_rarity('reforge the soul'/'AVR', 'Rare').
card_artist('reforge the soul'/'AVR', 'Jaime Jones').
card_number('reforge the soul'/'AVR', '151').
card_flavor_text('reforge the soul'/'AVR', 'In a wave of spells called the Cursemute, Avacyn cleansed the world with divine fire.').
card_multiverse_id('reforge the soul'/'AVR', '278256').

card_in_set('renegade demon', 'AVR').
card_original_type('renegade demon'/'AVR', 'Creature — Demon').
card_original_text('renegade demon'/'AVR', '').
card_first_print('renegade demon', 'AVR').
card_image_name('renegade demon'/'AVR', 'renegade demon').
card_uid('renegade demon'/'AVR', 'AVR:Renegade Demon:renegade demon').
card_rarity('renegade demon'/'AVR', 'Common').
card_artist('renegade demon'/'AVR', 'Tomasz Jedruszek').
card_number('renegade demon'/'AVR', '118').
card_flavor_text('renegade demon'/'AVR', '\"Have you ever cornered a wounded vampire? That\'s a walk in the cathedral garden in comparison.\"\n—Tristen, Cathar Marshal').
card_multiverse_id('renegade demon'/'AVR', '239960').

card_in_set('restoration angel', 'AVR').
card_original_type('restoration angel'/'AVR', 'Creature — Angel').
card_original_text('restoration angel'/'AVR', 'Flash\nFlying\nWhen Restoration Angel enters the battlefield, you may exile target non-Angel creature you control, then return that card to the battlefield under your control.').
card_image_name('restoration angel'/'AVR', 'restoration angel').
card_uid('restoration angel'/'AVR', 'AVR:Restoration Angel:restoration angel').
card_rarity('restoration angel'/'AVR', 'Rare').
card_artist('restoration angel'/'AVR', 'Johannes Voss').
card_number('restoration angel'/'AVR', '32').
card_multiverse_id('restoration angel'/'AVR', '240096').

card_in_set('revenge of the hunted', 'AVR').
card_original_type('revenge of the hunted'/'AVR', 'Sorcery').
card_original_text('revenge of the hunted'/'AVR', 'Until end of turn, target creature gets +6/+6 and gains trample, and all creatures able to block it this turn do so.\nMiracle {G} (You may cast this card for its miracle cost when you draw it if it\'s the first card you drew this turn.)').
card_first_print('revenge of the hunted', 'AVR').
card_image_name('revenge of the hunted'/'AVR', 'revenge of the hunted').
card_uid('revenge of the hunted'/'AVR', 'AVR:Revenge of the Hunted:revenge of the hunted').
card_rarity('revenge of the hunted'/'AVR', 'Rare').
card_artist('revenge of the hunted'/'AVR', 'Christopher Moeller').
card_number('revenge of the hunted'/'AVR', '191').
card_multiverse_id('revenge of the hunted'/'AVR', '275712').

card_in_set('riders of gavony', 'AVR').
card_original_type('riders of gavony'/'AVR', 'Creature — Human Knight').
card_original_text('riders of gavony'/'AVR', 'Vigilance\nAs Riders of Gavony enters the battlefield, choose a creature type.\nHuman creatures you control have protection from creatures of the chosen type.').
card_first_print('riders of gavony', 'AVR').
card_image_name('riders of gavony'/'AVR', 'riders of gavony').
card_uid('riders of gavony'/'AVR', 'AVR:Riders of Gavony:riders of gavony').
card_rarity('riders of gavony'/'AVR', 'Rare').
card_artist('riders of gavony'/'AVR', 'Volkan Baga').
card_number('riders of gavony'/'AVR', '33').
card_multiverse_id('riders of gavony'/'AVR', '240015').

card_in_set('righteous blow', 'AVR').
card_original_type('righteous blow'/'AVR', 'Instant').
card_original_text('righteous blow'/'AVR', 'Righteous Blow deals 2 damage to target attacking or blocking creature.').
card_first_print('righteous blow', 'AVR').
card_image_name('righteous blow'/'AVR', 'righteous blow').
card_uid('righteous blow'/'AVR', 'AVR:Righteous Blow:righteous blow').
card_rarity('righteous blow'/'AVR', 'Common').
card_artist('righteous blow'/'AVR', 'Clint Cearley').
card_number('righteous blow'/'AVR', '34').
card_flavor_text('righteous blow'/'AVR', '\"Monsters will no longer find safety under the mists of Morkrut!\"\n—Bruna, Light of Alabaster').
card_multiverse_id('righteous blow'/'AVR', '240185').

card_in_set('riot ringleader', 'AVR').
card_original_type('riot ringleader'/'AVR', 'Creature — Human Warrior').
card_original_text('riot ringleader'/'AVR', 'Whenever Riot Ringleader attacks, Human creatures you control get +1/+0 until end of turn.').
card_first_print('riot ringleader', 'AVR').
card_image_name('riot ringleader'/'AVR', 'riot ringleader').
card_uid('riot ringleader'/'AVR', 'AVR:Riot Ringleader:riot ringleader').
card_rarity('riot ringleader'/'AVR', 'Common').
card_artist('riot ringleader'/'AVR', 'Gabor Szikszai').
card_number('riot ringleader'/'AVR', '152').
card_flavor_text('riot ringleader'/'AVR', '\"So the vampires like hot blood, do they? Let\'s see how they handle ours.\"').
card_multiverse_id('riot ringleader'/'AVR', '239987').

card_in_set('rite of ruin', 'AVR').
card_original_type('rite of ruin'/'AVR', 'Sorcery').
card_original_text('rite of ruin'/'AVR', 'Choose an order for artifacts, creatures, and lands. Each player sacrifices one permanent of the first type, sacrifices two of the second type, then sacrifices three of the third type.').
card_first_print('rite of ruin', 'AVR').
card_image_name('rite of ruin'/'AVR', 'rite of ruin').
card_uid('rite of ruin'/'AVR', 'AVR:Rite of Ruin:rite of ruin').
card_rarity('rite of ruin'/'AVR', 'Rare').
card_artist('rite of ruin'/'AVR', 'Clint Cearley').
card_number('rite of ruin'/'AVR', '153').
card_flavor_text('rite of ruin'/'AVR', 'Skirsdag cultists refused to quietly accept Avacyn\'s ascendancy.').
card_multiverse_id('rite of ruin'/'AVR', '240126').

card_in_set('rotcrown ghoul', 'AVR').
card_original_type('rotcrown ghoul'/'AVR', 'Creature — Zombie').
card_original_text('rotcrown ghoul'/'AVR', 'When Rotcrown Ghoul dies, target player puts the top five cards of his or her library into his or her graveyard.').
card_first_print('rotcrown ghoul', 'AVR').
card_image_name('rotcrown ghoul'/'AVR', 'rotcrown ghoul').
card_uid('rotcrown ghoul'/'AVR', 'AVR:Rotcrown Ghoul:rotcrown ghoul').
card_rarity('rotcrown ghoul'/'AVR', 'Common').
card_artist('rotcrown ghoul'/'AVR', 'Dave Kendall').
card_number('rotcrown ghoul'/'AVR', '72').
card_flavor_text('rotcrown ghoul'/'AVR', '\"Don\'t look into its eyes. It\'s thinking things no dead thing should think.\"\n—Captain Eberhart').
card_multiverse_id('rotcrown ghoul'/'AVR', '240058').

card_in_set('rush of blood', 'AVR').
card_original_type('rush of blood'/'AVR', 'Instant').
card_original_text('rush of blood'/'AVR', 'Target creature gets +X/+0 until end of turn, where X is its power.').
card_first_print('rush of blood', 'AVR').
card_image_name('rush of blood'/'AVR', 'rush of blood').
card_uid('rush of blood'/'AVR', 'AVR:Rush of Blood:rush of blood').
card_rarity('rush of blood'/'AVR', 'Uncommon').
card_artist('rush of blood'/'AVR', 'Cynthia Sheppard').
card_number('rush of blood'/'AVR', '154').
card_flavor_text('rush of blood'/'AVR', 'Sometimes it\'s not for sustenance or to feed the dependence. Sometimes it\'s just to feel again.').
card_multiverse_id('rush of blood'/'AVR', '240183').

card_in_set('scalding devil', 'AVR').
card_original_type('scalding devil'/'AVR', 'Creature — Devil').
card_original_text('scalding devil'/'AVR', '{2}{R}: Scalding Devil deals 1 damage to target player.').
card_first_print('scalding devil', 'AVR').
card_image_name('scalding devil'/'AVR', 'scalding devil').
card_uid('scalding devil'/'AVR', 'AVR:Scalding Devil:scalding devil').
card_rarity('scalding devil'/'AVR', 'Common').
card_artist('scalding devil'/'AVR', 'Erica Yang').
card_number('scalding devil'/'AVR', '155').
card_flavor_text('scalding devil'/'AVR', 'Demons massacre. Devils annoy.').
card_multiverse_id('scalding devil'/'AVR', '239984').

card_in_set('scrapskin drake', 'AVR').
card_original_type('scrapskin drake'/'AVR', 'Creature — Zombie Drake').
card_original_text('scrapskin drake'/'AVR', 'Flying\nScrapskin Drake can block only creatures with flying.').
card_first_print('scrapskin drake', 'AVR').
card_image_name('scrapskin drake'/'AVR', 'scrapskin drake').
card_uid('scrapskin drake'/'AVR', 'AVR:Scrapskin Drake:scrapskin drake').
card_rarity('scrapskin drake'/'AVR', 'Common').
card_artist('scrapskin drake'/'AVR', 'Kev Walker').
card_number('scrapskin drake'/'AVR', '73').
card_flavor_text('scrapskin drake'/'AVR', '\"The cathars killed my skaabs down below. Let\'s see how high their swords can reach.\"\n—Ludevic, necro-alchemist').
card_multiverse_id('scrapskin drake'/'AVR', '240035').

card_in_set('scroll of avacyn', 'AVR').
card_original_type('scroll of avacyn'/'AVR', 'Artifact').
card_original_text('scroll of avacyn'/'AVR', '{1}, Sacrifice Scroll of Avacyn: Draw a card. If you control an Angel, you gain 5 life.').
card_first_print('scroll of avacyn', 'AVR').
card_image_name('scroll of avacyn'/'AVR', 'scroll of avacyn').
card_uid('scroll of avacyn'/'AVR', 'AVR:Scroll of Avacyn:scroll of avacyn').
card_rarity('scroll of avacyn'/'AVR', 'Common').
card_artist('scroll of avacyn'/'AVR', 'Cliff Childs').
card_number('scroll of avacyn'/'AVR', '220').
card_flavor_text('scroll of avacyn'/'AVR', 'Words to bless the eye that reads them, telling of a future beyond the reach of fear.').
card_multiverse_id('scroll of avacyn'/'AVR', '240105').

card_in_set('scroll of griselbrand', 'AVR').
card_original_type('scroll of griselbrand'/'AVR', 'Artifact').
card_original_text('scroll of griselbrand'/'AVR', '{1}, Sacrifice Scroll of Griselbrand: Target opponent discards a card. If you control a Demon, that player loses 3 life.').
card_first_print('scroll of griselbrand', 'AVR').
card_image_name('scroll of griselbrand'/'AVR', 'scroll of griselbrand').
card_uid('scroll of griselbrand'/'AVR', 'AVR:Scroll of Griselbrand:scroll of griselbrand').
card_rarity('scroll of griselbrand'/'AVR', 'Common').
card_artist('scroll of griselbrand'/'AVR', 'Cliff Childs').
card_number('scroll of griselbrand'/'AVR', '221').
card_flavor_text('scroll of griselbrand'/'AVR', 'Words no eyes should see, telling of things no sane mind could fathom.').
card_multiverse_id('scroll of griselbrand'/'AVR', '240177').

card_in_set('searchlight geist', 'AVR').
card_original_type('searchlight geist'/'AVR', 'Creature — Spirit').
card_original_text('searchlight geist'/'AVR', 'Flying\n{3}{B}: Searchlight Geist gains deathtouch until end of turn. (Any amount of damage it deals to a creature is enough to destroy it.)').
card_first_print('searchlight geist', 'AVR').
card_image_name('searchlight geist'/'AVR', 'searchlight geist').
card_uid('searchlight geist'/'AVR', 'AVR:Searchlight Geist:searchlight geist').
card_rarity('searchlight geist'/'AVR', 'Common').
card_artist('searchlight geist'/'AVR', 'Steven Belledin').
card_number('searchlight geist'/'AVR', '119').
card_flavor_text('searchlight geist'/'AVR', 'It rises with the fall of darkness, seeking souls to extinguish.').
card_multiverse_id('searchlight geist'/'AVR', '240020').

card_in_set('second guess', 'AVR').
card_original_type('second guess'/'AVR', 'Instant').
card_original_text('second guess'/'AVR', 'Counter target spell that\'s the second spell cast this turn.').
card_first_print('second guess', 'AVR').
card_image_name('second guess'/'AVR', 'second guess').
card_uid('second guess'/'AVR', 'AVR:Second Guess:second guess').
card_rarity('second guess'/'AVR', 'Uncommon').
card_artist('second guess'/'AVR', 'Karl Kopinski').
card_number('second guess'/'AVR', '74').
card_flavor_text('second guess'/'AVR', '\"I see where you\'re going with that. I don\'t like it.\"\n—Dierk, geistmage').
card_multiverse_id('second guess'/'AVR', '240063').

card_in_set('seraph of dawn', 'AVR').
card_original_type('seraph of dawn'/'AVR', 'Creature — Angel').
card_original_text('seraph of dawn'/'AVR', 'Flying\nLifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_first_print('seraph of dawn', 'AVR').
card_image_name('seraph of dawn'/'AVR', 'seraph of dawn').
card_uid('seraph of dawn'/'AVR', 'AVR:Seraph of Dawn:seraph of dawn').
card_rarity('seraph of dawn'/'AVR', 'Common').
card_artist('seraph of dawn'/'AVR', 'Todd Lockwood').
card_number('seraph of dawn'/'AVR', '35').
card_flavor_text('seraph of dawn'/'AVR', 'She parts the clouds so sunlight can reach even the darkest lairs and duskiest tombs.').
card_multiverse_id('seraph of dawn'/'AVR', '240078').

card_in_set('seraph sanctuary', 'AVR').
card_original_type('seraph sanctuary'/'AVR', 'Land').
card_original_text('seraph sanctuary'/'AVR', 'When Seraph Sanctuary enters the battlefield, you gain 1 life.\nWhenever an Angel enters the battlefield under your control, you gain 1 life.\n{T}: Add {1} to your mana pool.').
card_first_print('seraph sanctuary', 'AVR').
card_image_name('seraph sanctuary'/'AVR', 'seraph sanctuary').
card_uid('seraph sanctuary'/'AVR', 'AVR:Seraph Sanctuary:seraph sanctuary').
card_rarity('seraph sanctuary'/'AVR', 'Common').
card_artist('seraph sanctuary'/'AVR', 'David Palumbo').
card_number('seraph sanctuary'/'AVR', '228').
card_multiverse_id('seraph sanctuary'/'AVR', '240175').

card_in_set('sheltering word', 'AVR').
card_original_type('sheltering word'/'AVR', 'Instant').
card_original_text('sheltering word'/'AVR', 'Target creature you control gains hexproof until end of turn. You gain life equal to that creature\'s toughness. (A creature with hexproof can\'t be the target of spells or abilities opponents control.)').
card_first_print('sheltering word', 'AVR').
card_image_name('sheltering word'/'AVR', 'sheltering word').
card_uid('sheltering word'/'AVR', 'AVR:Sheltering Word:sheltering word').
card_rarity('sheltering word'/'AVR', 'Common').
card_artist('sheltering word'/'AVR', 'Igor Kieryluk').
card_number('sheltering word'/'AVR', '192').
card_multiverse_id('sheltering word'/'AVR', '240004').

card_in_set('sigarda, host of herons', 'AVR').
card_original_type('sigarda, host of herons'/'AVR', 'Legendary Creature — Angel').
card_original_text('sigarda, host of herons'/'AVR', 'Flying, hexproof\nSpells and abilities your opponents control can\'t cause you to sacrifice permanents.').
card_first_print('sigarda, host of herons', 'AVR').
card_image_name('sigarda, host of herons'/'AVR', 'sigarda, host of herons').
card_uid('sigarda, host of herons'/'AVR', 'AVR:Sigarda, Host of Herons:sigarda, host of herons').
card_rarity('sigarda, host of herons'/'AVR', 'Mythic Rare').
card_artist('sigarda, host of herons'/'AVR', 'Chris Rahn').
card_number('sigarda, host of herons'/'AVR', '210').
card_flavor_text('sigarda, host of herons'/'AVR', 'Great devotion yields great reward.').
card_multiverse_id('sigarda, host of herons'/'AVR', '240033').

card_in_set('silverblade paladin', 'AVR').
card_original_type('silverblade paladin'/'AVR', 'Creature — Human Knight').
card_original_text('silverblade paladin'/'AVR', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Silverblade Paladin is paired with another creature, both creatures have double strike.').
card_image_name('silverblade paladin'/'AVR', 'silverblade paladin').
card_uid('silverblade paladin'/'AVR', 'AVR:Silverblade Paladin:silverblade paladin').
card_rarity('silverblade paladin'/'AVR', 'Rare').
card_artist('silverblade paladin'/'AVR', 'Jason Chan').
card_number('silverblade paladin'/'AVR', '36').
card_multiverse_id('silverblade paladin'/'AVR', '240155').

card_in_set('slayers\' stronghold', 'AVR').
card_original_type('slayers\' stronghold'/'AVR', 'Land').
card_original_text('slayers\' stronghold'/'AVR', '{T}: Add {1} to your mana pool.\n{R}{W}, {T}: Target creature gets +2/+0 and gains vigilance and haste until end of turn.').
card_first_print('slayers\' stronghold', 'AVR').
card_image_name('slayers\' stronghold'/'AVR', 'slayers\' stronghold').
card_uid('slayers\' stronghold'/'AVR', 'AVR:Slayers\' Stronghold:slayers\' stronghold').
card_rarity('slayers\' stronghold'/'AVR', 'Rare').
card_artist('slayers\' stronghold'/'AVR', 'Karl Kopinski').
card_number('slayers\' stronghold'/'AVR', '229').
card_flavor_text('slayers\' stronghold'/'AVR', 'Its courtyards are lit during every hour of the day so that night may never fall within its walls.').
card_multiverse_id('slayers\' stronghold'/'AVR', '240170').

card_in_set('snare the skies', 'AVR').
card_original_type('snare the skies'/'AVR', 'Instant').
card_original_text('snare the skies'/'AVR', 'Target creature gets +1/+1 and gains reach until end of turn. (It can block creatures with flying.)').
card_first_print('snare the skies', 'AVR').
card_image_name('snare the skies'/'AVR', 'snare the skies').
card_uid('snare the skies'/'AVR', 'AVR:Snare the Skies:snare the skies').
card_rarity('snare the skies'/'AVR', 'Common').
card_artist('snare the skies'/'AVR', 'Ryan Yee').
card_number('snare the skies'/'AVR', '193').
card_flavor_text('snare the skies'/'AVR', 'A hunter\'s precision and a durkvine\'s strength are a potent combination.').
card_multiverse_id('snare the skies'/'AVR', '271090').

card_in_set('somberwald sage', 'AVR').
card_original_type('somberwald sage'/'AVR', 'Creature — Human Druid').
card_original_text('somberwald sage'/'AVR', '{T}: Add three mana of any one color to your mana pool. Spend this mana only to cast creature spells.').
card_first_print('somberwald sage', 'AVR').
card_image_name('somberwald sage'/'AVR', 'somberwald sage').
card_uid('somberwald sage'/'AVR', 'AVR:Somberwald Sage:somberwald sage').
card_rarity('somberwald sage'/'AVR', 'Rare').
card_artist('somberwald sage'/'AVR', 'Steve Argyle').
card_number('somberwald sage'/'AVR', '194').
card_flavor_text('somberwald sage'/'AVR', '\"You can face any danger when all of nature is on your side.\"').
card_multiverse_id('somberwald sage'/'AVR', '275711').

card_in_set('somberwald vigilante', 'AVR').
card_original_type('somberwald vigilante'/'AVR', 'Creature — Human Warrior').
card_original_text('somberwald vigilante'/'AVR', 'Whenever Somberwald Vigilante becomes blocked by a creature, Somberwald Vigilante deals 1 damage to that creature.').
card_first_print('somberwald vigilante', 'AVR').
card_image_name('somberwald vigilante'/'AVR', 'somberwald vigilante').
card_uid('somberwald vigilante'/'AVR', 'AVR:Somberwald Vigilante:somberwald vigilante').
card_rarity('somberwald vigilante'/'AVR', 'Common').
card_artist('somberwald vigilante'/'AVR', 'John Stanko').
card_number('somberwald vigilante'/'AVR', '156').
card_flavor_text('somberwald vigilante'/'AVR', 'He has nothing left but his resentment.').
card_multiverse_id('somberwald vigilante'/'AVR', '240028').

card_in_set('soul of the harvest', 'AVR').
card_original_type('soul of the harvest'/'AVR', 'Creature — Elemental').
card_original_text('soul of the harvest'/'AVR', 'Trample\nWhenever another nontoken creature enters the battlefield under your control, you may draw a card.').
card_first_print('soul of the harvest', 'AVR').
card_image_name('soul of the harvest'/'AVR', 'soul of the harvest').
card_uid('soul of the harvest'/'AVR', 'AVR:Soul of the Harvest:soul of the harvest').
card_rarity('soul of the harvest'/'AVR', 'Rare').
card_artist('soul of the harvest'/'AVR', 'Eytan Zana').
card_number('soul of the harvest'/'AVR', '195').
card_flavor_text('soul of the harvest'/'AVR', 'It\'s there when a seed sprouts, when gourds ripen on the vines, and when the reapers cut the grains under the Harvest Moon.').
card_multiverse_id('soul of the harvest'/'AVR', '240179').

card_in_set('soulcage fiend', 'AVR').
card_original_type('soulcage fiend'/'AVR', 'Creature — Demon').
card_original_text('soulcage fiend'/'AVR', 'When Soulcage Fiend dies, each player loses 3 life.').
card_first_print('soulcage fiend', 'AVR').
card_image_name('soulcage fiend'/'AVR', 'soulcage fiend').
card_uid('soulcage fiend'/'AVR', 'AVR:Soulcage Fiend:soulcage fiend').
card_rarity('soulcage fiend'/'AVR', 'Common').
card_artist('soulcage fiend'/'AVR', 'Jason A. Engle').
card_number('soulcage fiend'/'AVR', '120').
card_flavor_text('soulcage fiend'/'AVR', 'Vowing to free the souls of her children, Kastinne followed the tormentors into the ghastly network of caves below Stensia.').
card_multiverse_id('soulcage fiend'/'AVR', '240150').

card_in_set('spectral gateguards', 'AVR').
card_original_type('spectral gateguards'/'AVR', 'Creature — Spirit Soldier').
card_original_text('spectral gateguards'/'AVR', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Spectral Gateguards is paired with another creature, both creatures have vigilance.').
card_first_print('spectral gateguards', 'AVR').
card_image_name('spectral gateguards'/'AVR', 'spectral gateguards').
card_uid('spectral gateguards'/'AVR', 'AVR:Spectral Gateguards:spectral gateguards').
card_rarity('spectral gateguards'/'AVR', 'Common').
card_artist('spectral gateguards'/'AVR', 'Wayne England').
card_number('spectral gateguards'/'AVR', '37').
card_multiverse_id('spectral gateguards'/'AVR', '240038').

card_in_set('spectral prison', 'AVR').
card_original_type('spectral prison'/'AVR', 'Enchantment — Aura').
card_original_text('spectral prison'/'AVR', 'Enchant creature\nEnchanted creature doesn\'t untap during its controller\'s untap step.\nWhen enchanted creature becomes the target of a spell, sacrifice Spectral Prison.').
card_first_print('spectral prison', 'AVR').
card_image_name('spectral prison'/'AVR', 'spectral prison').
card_uid('spectral prison'/'AVR', 'AVR:Spectral Prison:spectral prison').
card_rarity('spectral prison'/'AVR', 'Common').
card_artist('spectral prison'/'AVR', 'Vincent Proce').
card_number('spectral prison'/'AVR', '75').
card_multiverse_id('spectral prison'/'AVR', '239967').

card_in_set('spirit away', 'AVR').
card_original_type('spirit away'/'AVR', 'Enchantment — Aura').
card_original_text('spirit away'/'AVR', 'Enchant creature\nYou control enchanted creature.\nEnchanted creature gets +2/+2 and has flying.').
card_first_print('spirit away', 'AVR').
card_image_name('spirit away'/'AVR', 'spirit away').
card_uid('spirit away'/'AVR', 'AVR:Spirit Away:spirit away').
card_rarity('spirit away'/'AVR', 'Rare').
card_artist('spirit away'/'AVR', 'Greg Staples').
card_number('spirit away'/'AVR', '76').
card_flavor_text('spirit away'/'AVR', 'The fear of slipping from the geist\'s tenuous grip overwhelmed Tolo\'s joy at his first flight.').
card_multiverse_id('spirit away'/'AVR', '239986').

card_in_set('stern mentor', 'AVR').
card_original_type('stern mentor'/'AVR', 'Creature — Human Wizard').
card_original_text('stern mentor'/'AVR', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Stern Mentor is paired with another creature, each of those creatures has \"{T}: Target player puts the top two cards of his or her library into his or her graveyard.\"').
card_first_print('stern mentor', 'AVR').
card_image_name('stern mentor'/'AVR', 'stern mentor').
card_uid('stern mentor'/'AVR', 'AVR:Stern Mentor:stern mentor').
card_rarity('stern mentor'/'AVR', 'Uncommon').
card_artist('stern mentor'/'AVR', 'Igor Kieryluk').
card_number('stern mentor'/'AVR', '77').
card_multiverse_id('stern mentor'/'AVR', '240069').

card_in_set('stolen goods', 'AVR').
card_original_type('stolen goods'/'AVR', 'Sorcery').
card_original_text('stolen goods'/'AVR', 'Target opponent exiles cards from the top of his or her library until he or she exiles a nonland card. Until end of turn, you may cast that card without paying its mana cost.').
card_first_print('stolen goods', 'AVR').
card_image_name('stolen goods'/'AVR', 'stolen goods').
card_uid('stolen goods'/'AVR', 'AVR:Stolen Goods:stolen goods').
card_rarity('stolen goods'/'AVR', 'Rare').
card_artist('stolen goods'/'AVR', 'Anthony Francisco').
card_number('stolen goods'/'AVR', '78').
card_multiverse_id('stolen goods'/'AVR', '275718').

card_in_set('stonewright', 'AVR').
card_original_type('stonewright'/'AVR', 'Creature — Human Shaman').
card_original_text('stonewright'/'AVR', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Stonewright is paired with another creature, each of those creatures has \"{R}: This creature gets +1/+0 until end of turn.\"').
card_first_print('stonewright', 'AVR').
card_image_name('stonewright'/'AVR', 'stonewright').
card_uid('stonewright'/'AVR', 'AVR:Stonewright:stonewright').
card_rarity('stonewright'/'AVR', 'Uncommon').
card_artist('stonewright'/'AVR', 'Wesley Burt').
card_number('stonewright'/'AVR', '157').
card_multiverse_id('stonewright'/'AVR', '239975').

card_in_set('swamp', 'AVR').
card_original_type('swamp'/'AVR', 'Basic Land — Swamp').
card_original_text('swamp'/'AVR', 'B').
card_image_name('swamp'/'AVR', 'swamp1').
card_uid('swamp'/'AVR', 'AVR:Swamp:swamp1').
card_rarity('swamp'/'AVR', 'Basic Land').
card_artist('swamp'/'AVR', 'James Paick').
card_number('swamp'/'AVR', '236').
card_multiverse_id('swamp'/'AVR', '269631').

card_in_set('swamp', 'AVR').
card_original_type('swamp'/'AVR', 'Basic Land — Swamp').
card_original_text('swamp'/'AVR', 'B').
card_image_name('swamp'/'AVR', 'swamp2').
card_uid('swamp'/'AVR', 'AVR:Swamp:swamp2').
card_rarity('swamp'/'AVR', 'Basic Land').
card_artist('swamp'/'AVR', 'Adam Paquette').
card_number('swamp'/'AVR', '237').
card_multiverse_id('swamp'/'AVR', '269627').

card_in_set('swamp', 'AVR').
card_original_type('swamp'/'AVR', 'Basic Land — Swamp').
card_original_text('swamp'/'AVR', 'B').
card_image_name('swamp'/'AVR', 'swamp3').
card_uid('swamp'/'AVR', 'AVR:Swamp:swamp3').
card_rarity('swamp'/'AVR', 'Basic Land').
card_artist('swamp'/'AVR', 'Jung Park').
card_number('swamp'/'AVR', '238').
card_multiverse_id('swamp'/'AVR', '269641').

card_in_set('tamiyo, the moon sage', 'AVR').
card_original_type('tamiyo, the moon sage'/'AVR', 'Planeswalker — Tamiyo').
card_original_text('tamiyo, the moon sage'/'AVR', '+1: Tap target permanent. It doesn\'t untap during its controller\'s next untap step.\n-2: Draw a card for each tapped creature target player controls.\n-8: You get an emblem with \"You have no maximum hand size\" and \"Whenever a card is put into your graveyard from anywhere, you may return it to your hand.\"').
card_first_print('tamiyo, the moon sage', 'AVR').
card_image_name('tamiyo, the moon sage'/'AVR', 'tamiyo, the moon sage').
card_uid('tamiyo, the moon sage'/'AVR', 'AVR:Tamiyo, the Moon Sage:tamiyo, the moon sage').
card_rarity('tamiyo, the moon sage'/'AVR', 'Mythic Rare').
card_artist('tamiyo, the moon sage'/'AVR', 'Eric Deschamps').
card_number('tamiyo, the moon sage'/'AVR', '79').
card_multiverse_id('tamiyo, the moon sage'/'AVR', '240070').

card_in_set('tandem lookout', 'AVR').
card_original_type('tandem lookout'/'AVR', 'Creature — Human Scout').
card_original_text('tandem lookout'/'AVR', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Tandem Lookout is paired with another creature, each of those creatures has \"Whenever this creature deals damage to an opponent, draw a card.\"').
card_first_print('tandem lookout', 'AVR').
card_image_name('tandem lookout'/'AVR', 'tandem lookout').
card_uid('tandem lookout'/'AVR', 'AVR:Tandem Lookout:tandem lookout').
card_rarity('tandem lookout'/'AVR', 'Uncommon').
card_artist('tandem lookout'/'AVR', 'Kev Walker').
card_number('tandem lookout'/'AVR', '80').
card_multiverse_id('tandem lookout'/'AVR', '240203').

card_in_set('temporal mastery', 'AVR').
card_original_type('temporal mastery'/'AVR', 'Sorcery').
card_original_text('temporal mastery'/'AVR', 'Take an extra turn after this one. Exile Temporal Mastery.\nMiracle {1}{U} (You may cast this card for its miracle cost when you draw it if it\'s the first card you drew this turn.)').
card_first_print('temporal mastery', 'AVR').
card_image_name('temporal mastery'/'AVR', 'temporal mastery').
card_uid('temporal mastery'/'AVR', 'AVR:Temporal Mastery:temporal mastery').
card_rarity('temporal mastery'/'AVR', 'Mythic Rare').
card_artist('temporal mastery'/'AVR', 'Franz Vohwinkel').
card_number('temporal mastery'/'AVR', '81').
card_flavor_text('temporal mastery'/'AVR', 'Time is a marvelous plaything.').
card_multiverse_id('temporal mastery'/'AVR', '240133').

card_in_set('terminus', 'AVR').
card_original_type('terminus'/'AVR', 'Sorcery').
card_original_text('terminus'/'AVR', 'Put all creatures on the bottom of their owners\' libraries.\nMiracle {W} (You may cast this card for its miracle cost when you draw it if it\'s the first card you drew this turn.)').
card_first_print('terminus', 'AVR').
card_image_name('terminus'/'AVR', 'terminus').
card_uid('terminus'/'AVR', 'AVR:Terminus:terminus').
card_rarity('terminus'/'AVR', 'Rare').
card_artist('terminus'/'AVR', 'James Paick').
card_number('terminus'/'AVR', '38').
card_multiverse_id('terminus'/'AVR', '262703').

card_in_set('terrifying presence', 'AVR').
card_original_type('terrifying presence'/'AVR', 'Instant').
card_original_text('terrifying presence'/'AVR', 'Prevent all combat damage that would be dealt by creatures other than target creature this turn.').
card_first_print('terrifying presence', 'AVR').
card_image_name('terrifying presence'/'AVR', 'terrifying presence').
card_uid('terrifying presence'/'AVR', 'AVR:Terrifying Presence:terrifying presence').
card_rarity('terrifying presence'/'AVR', 'Common').
card_artist('terrifying presence'/'AVR', 'Jaime Jones').
card_number('terrifying presence'/'AVR', '196').
card_flavor_text('terrifying presence'/'AVR', 'Elmut had slaughtered vampires, slain werewolves, and destroyed hordes of zombies. Sadly, he never got over his fear of spiders.').
card_multiverse_id('terrifying presence'/'AVR', '240066').

card_in_set('thatcher revolt', 'AVR').
card_original_type('thatcher revolt'/'AVR', 'Sorcery').
card_original_text('thatcher revolt'/'AVR', 'Put three 1/1 red Human creature tokens with haste onto the battlefield. Sacrifice those tokens at the beginning of the next end step.').
card_first_print('thatcher revolt', 'AVR').
card_image_name('thatcher revolt'/'AVR', 'thatcher revolt').
card_uid('thatcher revolt'/'AVR', 'AVR:Thatcher Revolt:thatcher revolt').
card_rarity('thatcher revolt'/'AVR', 'Common').
card_artist('thatcher revolt'/'AVR', 'Ryan Pancoast').
card_number('thatcher revolt'/'AVR', '158').
card_flavor_text('thatcher revolt'/'AVR', 'The dark times left the peasantry ready to fight with whatever weapons were at hand.').
card_multiverse_id('thatcher revolt'/'AVR', '239959').

card_in_set('thraben valiant', 'AVR').
card_original_type('thraben valiant'/'AVR', 'Creature — Human Soldier').
card_original_text('thraben valiant'/'AVR', 'Vigilance').
card_first_print('thraben valiant', 'AVR').
card_image_name('thraben valiant'/'AVR', 'thraben valiant').
card_uid('thraben valiant'/'AVR', 'AVR:Thraben Valiant:thraben valiant').
card_rarity('thraben valiant'/'AVR', 'Common').
card_artist('thraben valiant'/'AVR', 'Jason Chan').
card_number('thraben valiant'/'AVR', '39').
card_flavor_text('thraben valiant'/'AVR', '\"Once more into Devil\'s Breach, soldiers. I want another devil tail for my collection.\"').
card_multiverse_id('thraben valiant'/'AVR', '240099').

card_in_set('thunderbolt', 'AVR').
card_original_type('thunderbolt'/'AVR', 'Instant').
card_original_text('thunderbolt'/'AVR', 'Choose one — Thunderbolt deals 3 damage to target player; or Thunderbolt deals 4 damage to target creature with flying.').
card_image_name('thunderbolt'/'AVR', 'thunderbolt').
card_uid('thunderbolt'/'AVR', 'AVR:Thunderbolt:thunderbolt').
card_rarity('thunderbolt'/'AVR', 'Common').
card_artist('thunderbolt'/'AVR', 'Anthony Francisco').
card_number('thunderbolt'/'AVR', '159').
card_flavor_text('thunderbolt'/'AVR', '\"My next aerial design will use less metal.\"\n—Hadaken, alchemist of Nephalia').
card_multiverse_id('thunderbolt'/'AVR', '240001').

card_in_set('thunderous wrath', 'AVR').
card_original_type('thunderous wrath'/'AVR', 'Instant').
card_original_text('thunderous wrath'/'AVR', 'Thunderous Wrath deals 5 damage to target creature or player.\nMiracle {R} (You may cast this card for its miracle cost when you draw it if it\'s the first card you drew this turn.)').
card_first_print('thunderous wrath', 'AVR').
card_image_name('thunderous wrath'/'AVR', 'thunderous wrath').
card_uid('thunderous wrath'/'AVR', 'AVR:Thunderous Wrath:thunderous wrath').
card_rarity('thunderous wrath'/'AVR', 'Uncommon').
card_artist('thunderous wrath'/'AVR', 'Adam Paquette').
card_number('thunderous wrath'/'AVR', '160').
card_multiverse_id('thunderous wrath'/'AVR', '239985').

card_in_set('tibalt, the fiend-blooded', 'AVR').
card_original_type('tibalt, the fiend-blooded'/'AVR', 'Planeswalker — Tibalt').
card_original_text('tibalt, the fiend-blooded'/'AVR', '+1: Draw a card, then discard a card at random.\n-4: Tibalt, the Fiend-Blooded deals damage equal to the number of cards in target player\'s hand to that player.\n-6: Gain control of all creatures until end of turn. Untap them. They gain haste until end of turn.').
card_first_print('tibalt, the fiend-blooded', 'AVR').
card_image_name('tibalt, the fiend-blooded'/'AVR', 'tibalt, the fiend-blooded').
card_uid('tibalt, the fiend-blooded'/'AVR', 'AVR:Tibalt, the Fiend-Blooded:tibalt, the fiend-blooded').
card_rarity('tibalt, the fiend-blooded'/'AVR', 'Mythic Rare').
card_artist('tibalt, the fiend-blooded'/'AVR', 'Peter Mohrbacher').
card_number('tibalt, the fiend-blooded'/'AVR', '161').
card_multiverse_id('tibalt, the fiend-blooded'/'AVR', '276497').

card_in_set('timberland guide', 'AVR').
card_original_type('timberland guide'/'AVR', 'Creature — Human Scout').
card_original_text('timberland guide'/'AVR', 'When Timberland Guide enters the battlefield, put a +1/+1 counter on target creature.').
card_first_print('timberland guide', 'AVR').
card_image_name('timberland guide'/'AVR', 'timberland guide').
card_uid('timberland guide'/'AVR', 'AVR:Timberland Guide:timberland guide').
card_rarity('timberland guide'/'AVR', 'Common').
card_artist('timberland guide'/'AVR', 'Zoltan Boros').
card_number('timberland guide'/'AVR', '197').
card_flavor_text('timberland guide'/'AVR', '\"Can you build a fire? Track a deer? Identify killer trees? Then you\'ll never survive Kessig without me.\"').
card_multiverse_id('timberland guide'/'AVR', '240048').

card_in_set('tormentor\'s trident', 'AVR').
card_original_type('tormentor\'s trident'/'AVR', 'Artifact — Equipment').
card_original_text('tormentor\'s trident'/'AVR', 'Equipped creature gets +3/+0 and attacks each turn if able.\nEquip {3}').
card_first_print('tormentor\'s trident', 'AVR').
card_image_name('tormentor\'s trident'/'AVR', 'tormentor\'s trident').
card_uid('tormentor\'s trident'/'AVR', 'AVR:Tormentor\'s Trident:tormentor\'s trident').
card_rarity('tormentor\'s trident'/'AVR', 'Uncommon').
card_artist('tormentor\'s trident'/'AVR', 'Anthony Palumbo').
card_number('tormentor\'s trident'/'AVR', '222').
card_flavor_text('tormentor\'s trident'/'AVR', 'To a demon there is no such thing as restraint.').
card_multiverse_id('tormentor\'s trident'/'AVR', '239989').

card_in_set('treacherous pit-dweller', 'AVR').
card_original_type('treacherous pit-dweller'/'AVR', 'Creature — Demon').
card_original_text('treacherous pit-dweller'/'AVR', 'When Treacherous Pit-Dweller enters the battlefield from a graveyard, target opponent gains control of it.\nUndying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_first_print('treacherous pit-dweller', 'AVR').
card_image_name('treacherous pit-dweller'/'AVR', 'treacherous pit-dweller').
card_uid('treacherous pit-dweller'/'AVR', 'AVR:Treacherous Pit-Dweller:treacherous pit-dweller').
card_rarity('treacherous pit-dweller'/'AVR', 'Rare').
card_artist('treacherous pit-dweller'/'AVR', 'Svetlin Velinov').
card_number('treacherous pit-dweller'/'AVR', '121').
card_multiverse_id('treacherous pit-dweller'/'AVR', '271113').

card_in_set('triumph of cruelty', 'AVR').
card_original_type('triumph of cruelty'/'AVR', 'Enchantment').
card_original_text('triumph of cruelty'/'AVR', 'At the beginning of your upkeep, target opponent discards a card if you control the creature with the greatest power or tied for the greatest power.').
card_first_print('triumph of cruelty', 'AVR').
card_image_name('triumph of cruelty'/'AVR', 'triumph of cruelty').
card_uid('triumph of cruelty'/'AVR', 'AVR:Triumph of Cruelty:triumph of cruelty').
card_rarity('triumph of cruelty'/'AVR', 'Uncommon').
card_artist('triumph of cruelty'/'AVR', 'Izzy').
card_number('triumph of cruelty'/'AVR', '122').
card_flavor_text('triumph of cruelty'/'AVR', '\"I\'ve seen corpses prettier than you, beastmage.\"\n—Liliana Vess').
card_multiverse_id('triumph of cruelty'/'AVR', '276502').

card_in_set('triumph of ferocity', 'AVR').
card_original_type('triumph of ferocity'/'AVR', 'Enchantment').
card_original_text('triumph of ferocity'/'AVR', 'At the beginning of your upkeep, draw a card if you control the creature with the greatest power or tied for the greatest power.').
card_first_print('triumph of ferocity', 'AVR').
card_image_name('triumph of ferocity'/'AVR', 'triumph of ferocity').
card_uid('triumph of ferocity'/'AVR', 'AVR:Triumph of Ferocity:triumph of ferocity').
card_rarity('triumph of ferocity'/'AVR', 'Uncommon').
card_artist('triumph of ferocity'/'AVR', 'James Ryman').
card_number('triumph of ferocity'/'AVR', '198').
card_flavor_text('triumph of ferocity'/'AVR', '\"Rid me of this curse, witch, or die with me.\"\n—Garruk Wildspeaker').
card_multiverse_id('triumph of ferocity'/'AVR', '239962').

card_in_set('trusted forcemage', 'AVR').
card_original_type('trusted forcemage'/'AVR', 'Creature — Human Shaman').
card_original_text('trusted forcemage'/'AVR', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Trusted Forcemage is paired with another creature, each of those creatures gets +1/+1.').
card_first_print('trusted forcemage', 'AVR').
card_image_name('trusted forcemage'/'AVR', 'trusted forcemage').
card_uid('trusted forcemage'/'AVR', 'AVR:Trusted Forcemage:trusted forcemage').
card_rarity('trusted forcemage'/'AVR', 'Common').
card_artist('trusted forcemage'/'AVR', 'Cynthia Sheppard').
card_number('trusted forcemage'/'AVR', '199').
card_multiverse_id('trusted forcemage'/'AVR', '240064').

card_in_set('tyrant of discord', 'AVR').
card_original_type('tyrant of discord'/'AVR', 'Creature — Elemental').
card_original_text('tyrant of discord'/'AVR', 'When Tyrant of Discord enters the battlefield, target opponent chooses a permanent he or she controls at random and sacrifices it. If a nonland permanent is sacrificed this way, repeat this process.').
card_first_print('tyrant of discord', 'AVR').
card_image_name('tyrant of discord'/'AVR', 'tyrant of discord').
card_uid('tyrant of discord'/'AVR', 'AVR:Tyrant of Discord:tyrant of discord').
card_rarity('tyrant of discord'/'AVR', 'Rare').
card_artist('tyrant of discord'/'AVR', 'Richard Wright').
card_number('tyrant of discord'/'AVR', '162').
card_multiverse_id('tyrant of discord'/'AVR', '276194').

card_in_set('ulvenwald tracker', 'AVR').
card_original_type('ulvenwald tracker'/'AVR', 'Creature — Human Shaman').
card_original_text('ulvenwald tracker'/'AVR', '{1}{G}, {T}: Target creature you control fights another target creature. (Each deals damage equal to its power to the other.)').
card_first_print('ulvenwald tracker', 'AVR').
card_image_name('ulvenwald tracker'/'AVR', 'ulvenwald tracker').
card_uid('ulvenwald tracker'/'AVR', 'AVR:Ulvenwald Tracker:ulvenwald tracker').
card_rarity('ulvenwald tracker'/'AVR', 'Rare').
card_artist('ulvenwald tracker'/'AVR', 'Christopher Moeller').
card_number('ulvenwald tracker'/'AVR', '200').
card_flavor_text('ulvenwald tracker'/'AVR', '\"Peace will come to Innistrad, but only after all abominations have been dealt with, one by one.\"').
card_multiverse_id('ulvenwald tracker'/'AVR', '240154').

card_in_set('uncanny speed', 'AVR').
card_original_type('uncanny speed'/'AVR', 'Instant').
card_original_text('uncanny speed'/'AVR', 'Target creature gets +3/+0 and gains haste until end of turn.').
card_first_print('uncanny speed', 'AVR').
card_image_name('uncanny speed'/'AVR', 'uncanny speed').
card_uid('uncanny speed'/'AVR', 'AVR:Uncanny Speed:uncanny speed').
card_rarity('uncanny speed'/'AVR', 'Common').
card_artist('uncanny speed'/'AVR', 'Raymond Swanland').
card_number('uncanny speed'/'AVR', '163').
card_flavor_text('uncanny speed'/'AVR', '\"To survive, we must embrace the savagery we knew in our race\'s infancy.\"\n—Edgar Markov').
card_multiverse_id('uncanny speed'/'AVR', '240097').

card_in_set('undead executioner', 'AVR').
card_original_type('undead executioner'/'AVR', 'Creature — Zombie').
card_original_text('undead executioner'/'AVR', 'When Undead Executioner dies, you may have target creature get -2/-2 until end of turn.').
card_first_print('undead executioner', 'AVR').
card_image_name('undead executioner'/'AVR', 'undead executioner').
card_uid('undead executioner'/'AVR', 'AVR:Undead Executioner:undead executioner').
card_rarity('undead executioner'/'AVR', 'Common').
card_artist('undead executioner'/'AVR', 'Dave Kendall').
card_number('undead executioner'/'AVR', '123').
card_flavor_text('undead executioner'/'AVR', 'Heartless killer in life, brainless killer in death.').
card_multiverse_id('undead executioner'/'AVR', '240171').

card_in_set('unhallowed pact', 'AVR').
card_original_type('unhallowed pact'/'AVR', 'Enchantment — Aura').
card_original_text('unhallowed pact'/'AVR', 'Enchant creature\nWhen enchanted creature dies, return that card to the battlefield under your control.').
card_first_print('unhallowed pact', 'AVR').
card_image_name('unhallowed pact'/'AVR', 'unhallowed pact').
card_uid('unhallowed pact'/'AVR', 'AVR:Unhallowed Pact:unhallowed pact').
card_rarity('unhallowed pact'/'AVR', 'Common').
card_artist('unhallowed pact'/'AVR', 'Volkan Baga').
card_number('unhallowed pact'/'AVR', '124').
card_flavor_text('unhallowed pact'/'AVR', 'Agreements with demons seldom end at the grave.').
card_multiverse_id('unhallowed pact'/'AVR', '239974').

card_in_set('vanguard\'s shield', 'AVR').
card_original_type('vanguard\'s shield'/'AVR', 'Artifact — Equipment').
card_original_text('vanguard\'s shield'/'AVR', 'Equipped creature gets +0/+3 and can block an additional creature.\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('vanguard\'s shield', 'AVR').
card_image_name('vanguard\'s shield'/'AVR', 'vanguard\'s shield').
card_uid('vanguard\'s shield'/'AVR', 'AVR:Vanguard\'s Shield:vanguard\'s shield').
card_rarity('vanguard\'s shield'/'AVR', 'Common').
card_artist('vanguard\'s shield'/'AVR', 'Ryan Pancoast').
card_number('vanguard\'s shield'/'AVR', '223').
card_flavor_text('vanguard\'s shield'/'AVR', 'Wars are not fought on a single front.').
card_multiverse_id('vanguard\'s shield'/'AVR', '240198').

card_in_set('vanishment', 'AVR').
card_original_type('vanishment'/'AVR', 'Instant').
card_original_text('vanishment'/'AVR', 'Put target nonland permanent on top of its owner\'s library.\nMiracle {U} (You may cast this card for its miracle cost when you draw it if it\'s the first card you drew this turn.)').
card_first_print('vanishment', 'AVR').
card_image_name('vanishment'/'AVR', 'vanishment').
card_uid('vanishment'/'AVR', 'AVR:Vanishment:vanishment').
card_rarity('vanishment'/'AVR', 'Uncommon').
card_artist('vanishment'/'AVR', 'Daarken').
card_number('vanishment'/'AVR', '82').
card_multiverse_id('vanishment'/'AVR', '240130').

card_in_set('vessel of endless rest', 'AVR').
card_original_type('vessel of endless rest'/'AVR', 'Artifact').
card_original_text('vessel of endless rest'/'AVR', 'When Vessel of Endless Rest enters the battlefield, put target card from a graveyard on the bottom of its owner\'s library.\n{T}: Add one mana of any color to your mana pool.').
card_first_print('vessel of endless rest', 'AVR').
card_image_name('vessel of endless rest'/'AVR', 'vessel of endless rest').
card_uid('vessel of endless rest'/'AVR', 'AVR:Vessel of Endless Rest:vessel of endless rest').
card_rarity('vessel of endless rest'/'AVR', 'Uncommon').
card_artist('vessel of endless rest'/'AVR', 'John Avon').
card_number('vessel of endless rest'/'AVR', '224').
card_multiverse_id('vessel of endless rest'/'AVR', '240149').

card_in_set('vexing devil', 'AVR').
card_original_type('vexing devil'/'AVR', 'Creature — Devil').
card_original_text('vexing devil'/'AVR', 'When Vexing Devil enters the battlefield, any opponent may have it deal 4 damage to him or her. If a player does, sacrifice Vexing Devil.').
card_first_print('vexing devil', 'AVR').
card_image_name('vexing devil'/'AVR', 'vexing devil').
card_uid('vexing devil'/'AVR', 'AVR:Vexing Devil:vexing devil').
card_rarity('vexing devil'/'AVR', 'Rare').
card_artist('vexing devil'/'AVR', 'Lucas Graciano').
card_number('vexing devil'/'AVR', '164').
card_flavor_text('vexing devil'/'AVR', 'It\'s not any fun until someone loses an eye.').
card_multiverse_id('vexing devil'/'AVR', '278257').

card_in_set('vigilante justice', 'AVR').
card_original_type('vigilante justice'/'AVR', 'Enchantment').
card_original_text('vigilante justice'/'AVR', 'Whenever a Human enters the battlefield under your control, Vigilante Justice deals 1 damage to target creature or player.').
card_first_print('vigilante justice', 'AVR').
card_image_name('vigilante justice'/'AVR', 'vigilante justice').
card_uid('vigilante justice'/'AVR', 'AVR:Vigilante Justice:vigilante justice').
card_rarity('vigilante justice'/'AVR', 'Uncommon').
card_artist('vigilante justice'/'AVR', 'Steve Prescott').
card_number('vigilante justice'/'AVR', '165').
card_flavor_text('vigilante justice'/'AVR', 'It begins as a whisper and ends with the red roar of fire.').
card_multiverse_id('vigilante justice'/'AVR', '240032').

card_in_set('voice of the provinces', 'AVR').
card_original_type('voice of the provinces'/'AVR', 'Creature — Angel').
card_original_text('voice of the provinces'/'AVR', 'Flying\nWhen Voice of the Provinces enters the battlefield, put a 1/1 white Human creature token onto the battlefield.').
card_first_print('voice of the provinces', 'AVR').
card_image_name('voice of the provinces'/'AVR', 'voice of the provinces').
card_uid('voice of the provinces'/'AVR', 'AVR:Voice of the Provinces:voice of the provinces').
card_rarity('voice of the provinces'/'AVR', 'Common').
card_artist('voice of the provinces'/'AVR', 'Igor Kieryluk').
card_number('voice of the provinces'/'AVR', '40').
card_flavor_text('voice of the provinces'/'AVR', 'Her horn is heard across Innistrad, lifting the hearts of the righteous.').
card_multiverse_id('voice of the provinces'/'AVR', '240173').

card_in_set('vorstclaw', 'AVR').
card_original_type('vorstclaw'/'AVR', 'Creature — Elemental Horror').
card_original_text('vorstclaw'/'AVR', '').
card_first_print('vorstclaw', 'AVR').
card_image_name('vorstclaw'/'AVR', 'vorstclaw').
card_uid('vorstclaw'/'AVR', 'AVR:Vorstclaw:vorstclaw').
card_rarity('vorstclaw'/'AVR', 'Uncommon').
card_artist('vorstclaw'/'AVR', 'Lucas Graciano').
card_number('vorstclaw'/'AVR', '201').
card_flavor_text('vorstclaw'/'AVR', '\"Where\'d the werewolves go? Maybe that got hungry.\"\n—Halana of Ulvenwald').
card_multiverse_id('vorstclaw'/'AVR', '240204').

card_in_set('wandering wolf', 'AVR').
card_original_type('wandering wolf'/'AVR', 'Creature — Wolf').
card_original_text('wandering wolf'/'AVR', 'Creatures with power less than Wandering Wolf\'s power can\'t block it.').
card_first_print('wandering wolf', 'AVR').
card_image_name('wandering wolf'/'AVR', 'wandering wolf').
card_uid('wandering wolf'/'AVR', 'AVR:Wandering Wolf:wandering wolf').
card_rarity('wandering wolf'/'AVR', 'Common').
card_artist('wandering wolf'/'AVR', 'Tomasz Jedruszek').
card_number('wandering wolf'/'AVR', '202').
card_flavor_text('wandering wolf'/'AVR', 'Humans still feared wolves, though no connection between them and the curse of lycanthropy had ever been proved.').
card_multiverse_id('wandering wolf'/'AVR', '240051').

card_in_set('wild defiance', 'AVR').
card_original_type('wild defiance'/'AVR', 'Enchantment').
card_original_text('wild defiance'/'AVR', 'Whenever a creature you control becomes the target of an instant or sorcery spell, that creature gets +3/+3 until end of turn.').
card_first_print('wild defiance', 'AVR').
card_image_name('wild defiance'/'AVR', 'wild defiance').
card_uid('wild defiance'/'AVR', 'AVR:Wild Defiance:wild defiance').
card_rarity('wild defiance'/'AVR', 'Rare').
card_artist('wild defiance'/'AVR', 'Slawomir Maniak').
card_number('wild defiance'/'AVR', '203').
card_flavor_text('wild defiance'/'AVR', '\"When civilization reaches out its greedy hand, take it off at the wrist.\"\n—Garruk Wildspeaker').
card_multiverse_id('wild defiance'/'AVR', '276199').

card_in_set('wildwood geist', 'AVR').
card_original_type('wildwood geist'/'AVR', 'Creature — Spirit').
card_original_text('wildwood geist'/'AVR', 'Wildwood Geist gets +2/+2 as long as it\'s your turn.').
card_first_print('wildwood geist', 'AVR').
card_image_name('wildwood geist'/'AVR', 'wildwood geist').
card_uid('wildwood geist'/'AVR', 'AVR:Wildwood Geist:wildwood geist').
card_rarity('wildwood geist'/'AVR', 'Common').
card_artist('wildwood geist'/'AVR', 'Lars Grant-West').
card_number('wildwood geist'/'AVR', '204').
card_flavor_text('wildwood geist'/'AVR', 'The geists that dwell in the deep forests of Kessig are as untamable as the woods themselves.').
card_multiverse_id('wildwood geist'/'AVR', '240098').

card_in_set('wingcrafter', 'AVR').
card_original_type('wingcrafter'/'AVR', 'Creature — Human Wizard').
card_original_text('wingcrafter'/'AVR', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Wingcrafter is paired with another creature, both creatures have flying.').
card_first_print('wingcrafter', 'AVR').
card_image_name('wingcrafter'/'AVR', 'wingcrafter').
card_uid('wingcrafter'/'AVR', 'AVR:Wingcrafter:wingcrafter').
card_rarity('wingcrafter'/'AVR', 'Common').
card_artist('wingcrafter'/'AVR', 'Matt Stewart').
card_number('wingcrafter'/'AVR', '83').
card_multiverse_id('wingcrafter'/'AVR', '239979').

card_in_set('wolfir avenger', 'AVR').
card_original_type('wolfir avenger'/'AVR', 'Creature — Wolf Warrior').
card_original_text('wolfir avenger'/'AVR', 'Flash (You may cast this spell any time you could cast an instant.)\n{1}{G}: Regenerate Wolfir Avenger.').
card_first_print('wolfir avenger', 'AVR').
card_image_name('wolfir avenger'/'AVR', 'wolfir avenger').
card_uid('wolfir avenger'/'AVR', 'AVR:Wolfir Avenger:wolfir avenger').
card_rarity('wolfir avenger'/'AVR', 'Uncommon').
card_artist('wolfir avenger'/'AVR', 'Daniel Ljunggren').
card_number('wolfir avenger'/'AVR', '205').
card_flavor_text('wolfir avenger'/'AVR', 'Released from a dark curse and bound to a higher calling.').
card_multiverse_id('wolfir avenger'/'AVR', '276501').

card_in_set('wolfir silverheart', 'AVR').
card_original_type('wolfir silverheart'/'AVR', 'Creature — Wolf Warrior').
card_original_text('wolfir silverheart'/'AVR', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Wolfir Silverheart is paired with another creature, each of those creatures gets +4/+4.').
card_first_print('wolfir silverheart', 'AVR').
card_image_name('wolfir silverheart'/'AVR', 'wolfir silverheart').
card_uid('wolfir silverheart'/'AVR', 'AVR:Wolfir Silverheart:wolfir silverheart').
card_rarity('wolfir silverheart'/'AVR', 'Rare').
card_artist('wolfir silverheart'/'AVR', 'Raymond Swanland').
card_number('wolfir silverheart'/'AVR', '206').
card_multiverse_id('wolfir silverheart'/'AVR', '240090').

card_in_set('yew spirit', 'AVR').
card_original_type('yew spirit'/'AVR', 'Creature — Spirit Treefolk').
card_original_text('yew spirit'/'AVR', '{2}{G}{G}: Yew Spirit gets +X/+X until end of turn, where X is its power.').
card_first_print('yew spirit', 'AVR').
card_image_name('yew spirit'/'AVR', 'yew spirit').
card_uid('yew spirit'/'AVR', 'AVR:Yew Spirit:yew spirit').
card_rarity('yew spirit'/'AVR', 'Uncommon').
card_artist('yew spirit'/'AVR', 'Dan Scott').
card_number('yew spirit'/'AVR', '207').
card_flavor_text('yew spirit'/'AVR', '\"I wonder what dwelled in the primordial forests before humans existed.\"\n—Elder Rimheit').
card_multiverse_id('yew spirit'/'AVR', '240157').

card_in_set('zealous conscripts', 'AVR').
card_original_type('zealous conscripts'/'AVR', 'Creature — Human Warrior').
card_original_text('zealous conscripts'/'AVR', 'Haste\nWhen Zealous Conscripts enters the battlefield, gain control of target permanent until end of turn. Untap that permanent. It gains haste until end of turn.').
card_first_print('zealous conscripts', 'AVR').
card_image_name('zealous conscripts'/'AVR', 'zealous conscripts').
card_uid('zealous conscripts'/'AVR', 'AVR:Zealous Conscripts:zealous conscripts').
card_rarity('zealous conscripts'/'AVR', 'Rare').
card_artist('zealous conscripts'/'AVR', 'Steve Prescott').
card_number('zealous conscripts'/'AVR', '166').
card_multiverse_id('zealous conscripts'/'AVR', '240082').

card_in_set('zealous strike', 'AVR').
card_original_type('zealous strike'/'AVR', 'Instant').
card_original_text('zealous strike'/'AVR', 'Target creature gets +2/+2 and gains first strike until end of turn.').
card_first_print('zealous strike', 'AVR').
card_image_name('zealous strike'/'AVR', 'zealous strike').
card_uid('zealous strike'/'AVR', 'AVR:Zealous Strike:zealous strike').
card_rarity('zealous strike'/'AVR', 'Common').
card_artist('zealous strike'/'AVR', 'Bud Cook').
card_number('zealous strike'/'AVR', '41').
card_flavor_text('zealous strike'/'AVR', '\"Cower, fiend. The night is yours no longer.\"').
card_multiverse_id('zealous strike'/'AVR', '240019').
