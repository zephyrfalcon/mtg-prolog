% Legend Membership

set('pLGM').
set_name('pLGM', 'Legend Membership').
set_release_date('pLGM', '1995-01-01').
set_border('pLGM', 'black').
set_type('pLGM', 'promo').

card_in_set('counterspell', 'pLGM').
card_original_type('counterspell'/'pLGM', 'Instant').
card_original_text('counterspell'/'pLGM', '').
card_image_name('counterspell'/'pLGM', 'counterspell').
card_uid('counterspell'/'pLGM', 'pLGM:Counterspell:counterspell').
card_rarity('counterspell'/'pLGM', 'Special').
card_artist('counterspell'/'pLGM', 'Dom!').
card_number('counterspell'/'pLGM', '1').

card_in_set('incinerate', 'pLGM').
card_original_type('incinerate'/'pLGM', 'Instant').
card_original_text('incinerate'/'pLGM', '').
card_first_print('incinerate', 'pLGM').
card_image_name('incinerate'/'pLGM', 'incinerate').
card_uid('incinerate'/'pLGM', 'pLGM:Incinerate:incinerate').
card_rarity('incinerate'/'pLGM', 'Special').
card_artist('incinerate'/'pLGM', 'Jock').
card_number('incinerate'/'pLGM', '2').
card_flavor_text('incinerate'/'pLGM', '\"Yes, I think \'toast\' is an appropriate description.\"\n—Jaya Ballard, Task Mage').
