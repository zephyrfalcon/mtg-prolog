% From the Vault: Relics

set('V10').
set_name('V10', 'From the Vault: Relics').
set_release_date('V10', '2010-08-27').
set_border('V10', 'black').
set_type('V10', 'from the vault').

card_in_set('æther vial', 'V10').
card_original_type('æther vial'/'V10', 'Artifact').
card_original_text('æther vial'/'V10', 'At the beginning of your upkeep, you may put a charge counter on Æther Vial.\n{T}: You may put a creature card with converted mana cost equal to the number of charge counters on Æther Vial from your hand onto the battlefield.').
card_image_name('æther vial'/'V10', 'aether vial').
card_uid('æther vial'/'V10', 'V10:Æther Vial:aether vial').
card_rarity('æther vial'/'V10', 'Mythic Rare').
card_artist('æther vial'/'V10', 'Karl Kopinski').
card_number('æther vial'/'V10', '1').
card_multiverse_id('æther vial'/'V10', '212630').

card_in_set('black vise', 'V10').
card_original_type('black vise'/'V10', 'Artifact').
card_original_text('black vise'/'V10', 'As Black Vise enters the battlefield, choose an opponent.\nAt the beginning of the chosen player\'s upkeep, Black Vise deals X damage to that player, where X is the number of cards in his or her hand minus 4.').
card_image_name('black vise'/'V10', 'black vise').
card_uid('black vise'/'V10', 'V10:Black Vise:black vise').
card_rarity('black vise'/'V10', 'Mythic Rare').
card_artist('black vise'/'V10', 'Dan Scott').
card_number('black vise'/'V10', '2').
card_multiverse_id('black vise'/'V10', '212636').

card_in_set('isochron scepter', 'V10').
card_original_type('isochron scepter'/'V10', 'Artifact').
card_original_text('isochron scepter'/'V10', 'Imprint — When Isochron Scepter enters the battlefield, you may exile an instant card with converted mana cost 2 or less from your hand.\n{2}, {T}: You may copy the exiled card. If you do, you may cast the copy without paying its mana cost.').
card_image_name('isochron scepter'/'V10', 'isochron scepter').
card_uid('isochron scepter'/'V10', 'V10:Isochron Scepter:isochron scepter').
card_rarity('isochron scepter'/'V10', 'Mythic Rare').
card_artist('isochron scepter'/'V10', 'Chippy').
card_number('isochron scepter'/'V10', '3').
card_multiverse_id('isochron scepter'/'V10', '212626').

card_in_set('ivory tower', 'V10').
card_original_type('ivory tower'/'V10', 'Artifact').
card_original_text('ivory tower'/'V10', 'At the beginning of your upkeep, you gain X life, where X is the number of cards in your hand minus four.').
card_image_name('ivory tower'/'V10', 'ivory tower').
card_uid('ivory tower'/'V10', 'V10:Ivory Tower:ivory tower').
card_rarity('ivory tower'/'V10', 'Mythic Rare').
card_artist('ivory tower'/'V10', 'Jason Chan').
card_number('ivory tower'/'V10', '4').
card_flavor_text('ivory tower'/'V10', 'Valuing scholarship above all else, the inhabitants of the Ivory Tower reward those who sacrifice power for knowledge.').
card_multiverse_id('ivory tower'/'V10', '212639').

card_in_set('jester\'s cap', 'V10').
card_original_type('jester\'s cap'/'V10', 'Artifact').
card_original_text('jester\'s cap'/'V10', '{2}, {T}, Sacrifice Jester\'s Cap: Search target player\'s library for three cards and exile them. Then that player shuffles his or her library.').
card_image_name('jester\'s cap'/'V10', 'jester\'s cap').
card_uid('jester\'s cap'/'V10', 'V10:Jester\'s Cap:jester\'s cap').
card_rarity('jester\'s cap'/'V10', 'Mythic Rare').
card_artist('jester\'s cap'/'V10', 'D. Alexander Gregory').
card_number('jester\'s cap'/'V10', '5').
card_flavor_text('jester\'s cap'/'V10', '\"Know your foes\' strengths as well as their weaknesses.\"\n—Arcum Dagsson, Soldevi machinist').
card_multiverse_id('jester\'s cap'/'V10', '212627').

card_in_set('karn, silver golem', 'V10').
card_original_type('karn, silver golem'/'V10', 'Legendary Artifact Creature — Golem').
card_original_text('karn, silver golem'/'V10', 'Whenever Karn, Silver Golem blocks or becomes blocked, it gets -4/+4 until end of turn.\n{1}: Target noncreature artifact becomes an artifact creature with power and toughness each equal to its converted mana cost until end of turn.').
card_image_name('karn, silver golem'/'V10', 'karn, silver golem').
card_uid('karn, silver golem'/'V10', 'V10:Karn, Silver Golem:karn, silver golem').
card_rarity('karn, silver golem'/'V10', 'Mythic Rare').
card_artist('karn, silver golem'/'V10', 'Mark Zug').
card_number('karn, silver golem'/'V10', '6').
card_multiverse_id('karn, silver golem'/'V10', '212632').

card_in_set('masticore', 'V10').
card_original_type('masticore'/'V10', 'Artifact Creature — Masticore').
card_original_text('masticore'/'V10', 'At the beginning of your upkeep, sacrifice Masticore unless you discard a card.\n{2}: Masticore deals 1 damage to target creature.\n{2}: Regenerate Masticore.').
card_image_name('masticore'/'V10', 'masticore').
card_uid('masticore'/'V10', 'V10:Masticore:masticore').
card_rarity('masticore'/'V10', 'Mythic Rare').
card_artist('masticore'/'V10', 'Steven Belledin').
card_number('masticore'/'V10', '7').
card_multiverse_id('masticore'/'V10', '212629').

card_in_set('memory jar', 'V10').
card_original_type('memory jar'/'V10', 'Artifact').
card_original_text('memory jar'/'V10', '{T}, Sacrifice Memory Jar: Each player exiles all cards from his or her hand face down and draws seven cards. At the beginning of the next end step, each player discards his or her hand and returns to his or her hand each card he or she exiled this way.').
card_image_name('memory jar'/'V10', 'memory jar').
card_uid('memory jar'/'V10', 'V10:Memory Jar:memory jar').
card_rarity('memory jar'/'V10', 'Mythic Rare').
card_artist('memory jar'/'V10', 'Donato Giancola').
card_number('memory jar'/'V10', '8').
card_multiverse_id('memory jar'/'V10', '212633').

card_in_set('mirari', 'V10').
card_original_type('mirari'/'V10', 'Legendary Artifact').
card_original_text('mirari'/'V10', 'Whenever you cast an instant or sorcery spell, you may pay {3}. If you do, copy that spell. You may choose new targets for the copy.').
card_image_name('mirari'/'V10', 'mirari').
card_uid('mirari'/'V10', 'V10:Mirari:mirari').
card_rarity('mirari'/'V10', 'Mythic Rare').
card_artist('mirari'/'V10', 'Donato Giancola').
card_number('mirari'/'V10', '9').
card_flavor_text('mirari'/'V10', '\"It offers you what you want, not what you need.\"\n—Braids, dementia summoner').
card_multiverse_id('mirari'/'V10', '212635').

card_in_set('mox diamond', 'V10').
card_original_type('mox diamond'/'V10', 'Artifact').
card_original_text('mox diamond'/'V10', 'If Mox Diamond would enter the battlefield, you may discard a land card instead. If you do, put Mox Diamond onto the battlefield. If you don\'t, put it into its owner\'s graveyard.\n{T}: Add one mana of any color to your mana pool.').
card_image_name('mox diamond'/'V10', 'mox diamond').
card_uid('mox diamond'/'V10', 'V10:Mox Diamond:mox diamond').
card_rarity('mox diamond'/'V10', 'Mythic Rare').
card_artist('mox diamond'/'V10', 'Volkan Baga').
card_number('mox diamond'/'V10', '10').
card_multiverse_id('mox diamond'/'V10', '212634').

card_in_set('nevinyrral\'s disk', 'V10').
card_original_type('nevinyrral\'s disk'/'V10', 'Artifact').
card_original_text('nevinyrral\'s disk'/'V10', 'Nevinyrral\'s Disk enters the battlefield tapped.\n{1}, {T}: Destroy all artifacts, creatures, and enchantments.').
card_image_name('nevinyrral\'s disk'/'V10', 'nevinyrral\'s disk').
card_uid('nevinyrral\'s disk'/'V10', 'V10:Nevinyrral\'s Disk:nevinyrral\'s disk').
card_rarity('nevinyrral\'s disk'/'V10', 'Mythic Rare').
card_artist('nevinyrral\'s disk'/'V10', 'Steve Argyle').
card_number('nevinyrral\'s disk'/'V10', '11').
card_multiverse_id('nevinyrral\'s disk'/'V10', '212637').

card_in_set('sol ring', 'V10').
card_original_type('sol ring'/'V10', 'Artifact').
card_original_text('sol ring'/'V10', '{T}: Add {2} to your mana pool.').
card_image_name('sol ring'/'V10', 'sol ring').
card_uid('sol ring'/'V10', 'V10:Sol Ring:sol ring').
card_rarity('sol ring'/'V10', 'Mythic Rare').
card_artist('sol ring'/'V10', 'Mike Bierek').
card_number('sol ring'/'V10', '12').
card_flavor_text('sol ring'/'V10', 'Lost to time is the artificer\'s art of trapping light from a distant star in a ring of purest gold.').
card_multiverse_id('sol ring'/'V10', '212638').

card_in_set('sundering titan', 'V10').
card_original_type('sundering titan'/'V10', 'Artifact Creature — Golem').
card_original_text('sundering titan'/'V10', 'When Sundering Titan enters the battlefield or leaves the battlefield, choose a land of each basic land type, then destroy those lands.').
card_image_name('sundering titan'/'V10', 'sundering titan').
card_uid('sundering titan'/'V10', 'V10:Sundering Titan:sundering titan').
card_rarity('sundering titan'/'V10', 'Mythic Rare').
card_artist('sundering titan'/'V10', 'Jim Murray').
card_number('sundering titan'/'V10', '13').
card_multiverse_id('sundering titan'/'V10', '212631').

card_in_set('sword of body and mind', 'V10').
card_original_type('sword of body and mind'/'V10', 'Artifact — Equipment').
card_original_text('sword of body and mind'/'V10', 'Equipped creature gets +2/+2 and has protection from green and from blue.\nWhenever equipped creature deals combat damage to a player, you put a 2/2 green Wolf creature token onto the battlefield and that player puts the top ten cards of his or her library into his or her graveyard.\nEquip {2}').
card_first_print('sword of body and mind', 'V10').
card_image_name('sword of body and mind'/'V10', 'sword of body and mind').
card_uid('sword of body and mind'/'V10', 'V10:Sword of Body and Mind:sword of body and mind').
card_rarity('sword of body and mind'/'V10', 'Mythic Rare').
card_artist('sword of body and mind'/'V10', 'Chris Rahn').
card_number('sword of body and mind'/'V10', '14').
card_multiverse_id('sword of body and mind'/'V10', '212640').

card_in_set('zuran orb', 'V10').
card_original_type('zuran orb'/'V10', 'Artifact').
card_original_text('zuran orb'/'V10', 'Sacrifice a land: You gain 2 life.').
card_image_name('zuran orb'/'V10', 'zuran orb').
card_uid('zuran orb'/'V10', 'V10:Zuran Orb:zuran orb').
card_rarity('zuran orb'/'V10', 'Mythic Rare').
card_artist('zuran orb'/'V10', 'Ryan Pancoast').
card_number('zuran orb'/'V10', '15').
card_flavor_text('zuran orb'/'V10', '\"I will go to any length to achieve my goal. Eternal life is worth any sacrifice.\"\n—Zur the Enchanter').
card_multiverse_id('zuran orb'/'V10', '212628').
