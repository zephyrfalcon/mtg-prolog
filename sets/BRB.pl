% Battle Royale Box Set

set('BRB').
set_name('BRB', 'Battle Royale Box Set').
set_release_date('BRB', '1999-11-12').
set_border('BRB', 'white').
set_type('BRB', 'box').

card_in_set('abyssal specter', 'BRB').
card_original_type('abyssal specter'/'BRB', 'Creature — Specter').
card_original_text('abyssal specter'/'BRB', 'Flying\nWhenever Abyssal Specter deals damage to a player, that player discards a card from his or her hand.').
card_image_name('abyssal specter'/'BRB', 'abyssal specter').
card_uid('abyssal specter'/'BRB', 'BRB:Abyssal Specter:abyssal specter').
card_rarity('abyssal specter'/'BRB', 'Uncommon').
card_artist('abyssal specter'/'BRB', 'George Pratt').
card_multiverse_id('abyssal specter'/'BRB', '21167').

card_in_set('advance scout', 'BRB').
card_original_type('advance scout'/'BRB', 'Creature — Soldier').
card_original_text('advance scout'/'BRB', 'First strike\n{W} Target creature gains first strike until end of turn.').
card_image_name('advance scout'/'BRB', 'advance scout').
card_uid('advance scout'/'BRB', 'BRB:Advance Scout:advance scout').
card_rarity('advance scout'/'BRB', 'Common').
card_artist('advance scout'/'BRB', 'Heather Hudson').
card_flavor_text('advance scout'/'BRB', '\"The soldier\'s path is worn smooth by the tread of many feet—all in one direction, none returning.\"\n—Oracle en-Vec').
card_multiverse_id('advance scout'/'BRB', '21186').

card_in_set('air elemental', 'BRB').
card_original_type('air elemental'/'BRB', 'Creature — Elemental').
card_original_text('air elemental'/'BRB', 'Flying').
card_image_name('air elemental'/'BRB', 'air elemental').
card_uid('air elemental'/'BRB', 'BRB:Air Elemental:air elemental').
card_rarity('air elemental'/'BRB', 'Uncommon').
card_artist('air elemental'/'BRB', 'D. Alexander Gregory').
card_flavor_text('air elemental'/'BRB', 'These spirits of the air are winsome and wild and cannot be truly contained. Only marginally intelligent, they often substitute whimsy for strategy, delighting in mischief and mayhem.').
card_multiverse_id('air elemental'/'BRB', '21121').

card_in_set('angelic page', 'BRB').
card_original_type('angelic page'/'BRB', 'Creature — Spirit').
card_original_text('angelic page'/'BRB', 'Flying\n{T}: Target attacking or blocking creature gets +1/+1 until end of turn.').
card_image_name('angelic page'/'BRB', 'angelic page').
card_uid('angelic page'/'BRB', 'BRB:Angelic Page:angelic page').
card_rarity('angelic page'/'BRB', 'Common').
card_artist('angelic page'/'BRB', 'Rebecca Guay').
card_flavor_text('angelic page'/'BRB', 'If only every message were as perfect as its bearer.').
card_multiverse_id('angelic page'/'BRB', '21177').

card_in_set('arc lightning', 'BRB').
card_original_type('arc lightning'/'BRB', 'Sorcery').
card_original_text('arc lightning'/'BRB', 'Arc Lightning deals 3 damage divided as you choose among any number of target creatures and/or players.').
card_image_name('arc lightning'/'BRB', 'arc lightning').
card_uid('arc lightning'/'BRB', 'BRB:Arc Lightning:arc lightning').
card_rarity('arc lightning'/'BRB', 'Common').
card_artist('arc lightning'/'BRB', 'Andrew Goldhawk').
card_flavor_text('arc lightning'/'BRB', 'Rainclouds don\'t last long in Shiv, but that doesn\'t stop the lightning.').
card_multiverse_id('arc lightning'/'BRB', '21102').

card_in_set('argothian elder', 'BRB').
card_original_type('argothian elder'/'BRB', 'Creature — Elf').
card_original_text('argothian elder'/'BRB', '{T}: Untap two target lands.').
card_image_name('argothian elder'/'BRB', 'argothian elder').
card_uid('argothian elder'/'BRB', 'BRB:Argothian Elder:argothian elder').
card_rarity('argothian elder'/'BRB', 'Uncommon').
card_artist('argothian elder'/'BRB', 'DiTerlizzi').
card_flavor_text('argothian elder'/'BRB', 'Sharpen your ears\n—Elvish expression meaning\n\"grow wiser\"').
card_multiverse_id('argothian elder'/'BRB', '21182').

card_in_set('armored pegasus', 'BRB').
card_original_type('armored pegasus'/'BRB', 'Creature — Pegasus').
card_original_text('armored pegasus'/'BRB', 'Flying').
card_image_name('armored pegasus'/'BRB', 'armored pegasus').
card_uid('armored pegasus'/'BRB', 'BRB:Armored Pegasus:armored pegasus').
card_rarity('armored pegasus'/'BRB', 'Common').
card_artist('armored pegasus'/'BRB', 'Una Fricker').
card_flavor_text('armored pegasus'/'BRB', '\"I always charge a little extra to take on a pegasus. They fly like eagles, kick like mules, and hide like hermits.\"\n—Tahlil en-Dal, bounty hunter').
card_multiverse_id('armored pegasus'/'BRB', '21120').

card_in_set('azure drake', 'BRB').
card_original_type('azure drake'/'BRB', 'Creature — Drake').
card_original_text('azure drake'/'BRB', 'Flying').
card_image_name('azure drake'/'BRB', 'azure drake').
card_uid('azure drake'/'BRB', 'BRB:Azure Drake:azure drake').
card_rarity('azure drake'/'BRB', 'Uncommon').
card_artist('azure drake'/'BRB', 'Janine Johnston').
card_flavor_text('azure drake'/'BRB', 'Little dreamt could seem so cruel\nAs waiting for the wings outspread,\nThe jagged teeth, the burning eyes,\nAnd dagger-claws that clench to nerves.').
card_multiverse_id('azure drake'/'BRB', '21122').

card_in_set('blinking spirit', 'BRB').
card_original_type('blinking spirit'/'BRB', 'Creature — Spirit').
card_original_text('blinking spirit'/'BRB', '{0}: Return Blinking Spirit to its owner\'s hand.').
card_image_name('blinking spirit'/'BRB', 'blinking spirit').
card_uid('blinking spirit'/'BRB', 'BRB:Blinking Spirit:blinking spirit').
card_rarity('blinking spirit'/'BRB', 'Rare').
card_artist('blinking spirit'/'BRB', 'L. A. Williams').
card_flavor_text('blinking spirit'/'BRB', '\"Don\'t look at it! Maybe it\'ll go away!\"\n—Ib Halfheart, goblin tactician').
card_multiverse_id('blinking spirit'/'BRB', '11177').

card_in_set('broken fall', 'BRB').
card_original_type('broken fall'/'BRB', 'Enchantment').
card_original_text('broken fall'/'BRB', 'Return Broken Fall to its owner\'s hand: Regenerate target creature.').
card_image_name('broken fall'/'BRB', 'broken fall').
card_uid('broken fall'/'BRB', 'BRB:Broken Fall:broken fall').
card_rarity('broken fall'/'BRB', 'Common').
card_artist('broken fall'/'BRB', 'Zina Saunders').
card_flavor_text('broken fall'/'BRB', '\"I think I used up all my good luck and all my bad luck in the same fall.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('broken fall'/'BRB', '21099').

card_in_set('cackling fiend', 'BRB').
card_original_type('cackling fiend'/'BRB', 'Creature — Zombie').
card_original_text('cackling fiend'/'BRB', 'When Cackling Fiend comes into play, each opponent discards a card from his or her hand.').
card_image_name('cackling fiend'/'BRB', 'cackling fiend').
card_uid('cackling fiend'/'BRB', 'BRB:Cackling Fiend:cackling fiend').
card_rarity('cackling fiend'/'BRB', 'Common').
card_artist('cackling fiend'/'BRB', 'Brian Despain').
card_flavor_text('cackling fiend'/'BRB', 'Its windpipe is only the first to amplify its maddening laughter.').
card_multiverse_id('cackling fiend'/'BRB', '21168').

card_in_set('catastrophe', 'BRB').
card_original_type('catastrophe'/'BRB', 'Sorcery').
card_original_text('catastrophe'/'BRB', 'Destroy all creatures or all lands. Creatures destroyed this way can\'t be regenerated.').
card_image_name('catastrophe'/'BRB', 'catastrophe').
card_uid('catastrophe'/'BRB', 'BRB:Catastrophe:catastrophe').
card_rarity('catastrophe'/'BRB', 'Rare').
card_artist('catastrophe'/'BRB', 'Andrew Robinson').
card_flavor_text('catastrophe'/'BRB', 'Radiant\'s eyes flashed. \"Go, then,\" the angel spat at Serra, \"and leave this world to those who truly care.\"').
card_multiverse_id('catastrophe'/'BRB', '21143').

card_in_set('cinder marsh', 'BRB').
card_original_type('cinder marsh'/'BRB', 'Land').
card_original_text('cinder marsh'/'BRB', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {B} or {R} to your mana pool. Cinder Marsh doesn\'t untap during your next untap phase.').
card_image_name('cinder marsh'/'BRB', 'cinder marsh').
card_uid('cinder marsh'/'BRB', 'BRB:Cinder Marsh:cinder marsh').
card_rarity('cinder marsh'/'BRB', 'Uncommon').
card_artist('cinder marsh'/'BRB', 'John Matson').
card_multiverse_id('cinder marsh'/'BRB', '21170').

card_in_set('control magic', 'BRB').
card_original_type('control magic'/'BRB', 'Enchant Creature').
card_original_text('control magic'/'BRB', 'You control enchanted creature.').
card_image_name('control magic'/'BRB', 'control magic').
card_uid('control magic'/'BRB', 'BRB:Control Magic:control magic').
card_rarity('control magic'/'BRB', 'Uncommon').
card_artist('control magic'/'BRB', 'Dameon Willich').
card_multiverse_id('control magic'/'BRB', '21141').

card_in_set('counterspell', 'BRB').
card_original_type('counterspell'/'BRB', 'Instant').
card_original_text('counterspell'/'BRB', 'Counter target spell.').
card_image_name('counterspell'/'BRB', 'counterspell').
card_uid('counterspell'/'BRB', 'BRB:Counterspell:counterspell').
card_rarity('counterspell'/'BRB', 'Common').
card_artist('counterspell'/'BRB', 'Hannibal King').
card_multiverse_id('counterspell'/'BRB', '21127').

card_in_set('crazed skirge', 'BRB').
card_original_type('crazed skirge'/'BRB', 'Creature — Imp').
card_original_text('crazed skirge'/'BRB', 'Flying; haste (This creature may attack and {T} the turn it comes under your control.)').
card_image_name('crazed skirge'/'BRB', 'crazed skirge').
card_uid('crazed skirge'/'BRB', 'BRB:Crazed Skirge:crazed skirge').
card_rarity('crazed skirge'/'BRB', 'Uncommon').
card_artist('crazed skirge'/'BRB', 'Ron Spencer').
card_flavor_text('crazed skirge'/'BRB', 'They are Phyrexia\'s couriers; the messages they carry are inscribed on their slick hides.').
card_multiverse_id('crazed skirge'/'BRB', '21165').

card_in_set('curfew', 'BRB').
card_original_type('curfew'/'BRB', 'Instant').
card_original_text('curfew'/'BRB', 'Each player returns a creature he or she controls to its owner\'s hand.').
card_image_name('curfew'/'BRB', 'curfew').
card_uid('curfew'/'BRB', 'BRB:Curfew:curfew').
card_rarity('curfew'/'BRB', 'Common').
card_artist('curfew'/'BRB', 'Randy Gallegos').
card_flavor_text('curfew'/'BRB', '\". . . But I\'m not tired!\"').
card_multiverse_id('curfew'/'BRB', '21137').

card_in_set('dark ritual', 'BRB').
card_original_type('dark ritual'/'BRB', 'Instant').
card_original_text('dark ritual'/'BRB', 'Add {B}{B}{B} to your mana pool.').
card_image_name('dark ritual'/'BRB', 'dark ritual').
card_uid('dark ritual'/'BRB', 'BRB:Dark Ritual:dark ritual').
card_rarity('dark ritual'/'BRB', 'Common').
card_artist('dark ritual'/'BRB', 'Clint Langley').
card_multiverse_id('dark ritual'/'BRB', '21154').

card_in_set('dirtcowl wurm', 'BRB').
card_original_type('dirtcowl wurm'/'BRB', 'Creature — Wurm').
card_original_text('dirtcowl wurm'/'BRB', 'Whenever an opponent plays a land, put a +1/+1 counter on Dirtcowl Wurm.').
card_image_name('dirtcowl wurm'/'BRB', 'dirtcowl wurm').
card_uid('dirtcowl wurm'/'BRB', 'BRB:Dirtcowl Wurm:dirtcowl wurm').
card_rarity('dirtcowl wurm'/'BRB', 'Rare').
card_artist('dirtcowl wurm'/'BRB', 'Dan Frazier').
card_flavor_text('dirtcowl wurm'/'BRB', 'Few of Rath\'s rivers follow a steady course, so many new channels are carved for them.').
card_multiverse_id('dirtcowl wurm'/'BRB', '21196').

card_in_set('disenchant', 'BRB').
card_original_type('disenchant'/'BRB', 'Instant').
card_original_text('disenchant'/'BRB', 'Destroy target artifact or enchantment.').
card_image_name('disenchant'/'BRB', 'disenchant').
card_uid('disenchant'/'BRB', 'BRB:Disenchant:disenchant').
card_rarity('disenchant'/'BRB', 'Common').
card_artist('disenchant'/'BRB', 'Brian Snõddy').
card_flavor_text('disenchant'/'BRB', '\"I implore you not to forget the horrors of the past. You would have us start the Brothers\' War anew\"\n—Sorine Relicbane, Soldevi heretic').
card_multiverse_id('disenchant'/'BRB', '21133').

card_in_set('disruptive student', 'BRB').
card_original_type('disruptive student'/'BRB', 'Creature — Wizard').
card_original_text('disruptive student'/'BRB', '{T}: Counter target spell unless its controller pays {1}.').
card_image_name('disruptive student'/'BRB', 'disruptive student').
card_uid('disruptive student'/'BRB', 'BRB:Disruptive Student:disruptive student').
card_rarity('disruptive student'/'BRB', 'Common').
card_artist('disruptive student'/'BRB', 'Randy Gallegos').
card_flavor_text('disruptive student'/'BRB', '\"Teferi is a problem student. Always late for class. No appreciation for constructive use of time.\"\n—Barrin, progress report').
card_multiverse_id('disruptive student'/'BRB', '21123').

card_in_set('drifting meadow', 'BRB').
card_original_type('drifting meadow'/'BRB', 'Land').
card_original_text('drifting meadow'/'BRB', 'Cycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability any time you could play an instant.)\nDrifting Meadow comes into play tapped.\n{T}: Add {W} to your mana pool.').
card_image_name('drifting meadow'/'BRB', 'drifting meadow').
card_uid('drifting meadow'/'BRB', 'BRB:Drifting Meadow:drifting meadow').
card_rarity('drifting meadow'/'BRB', 'Common').
card_artist('drifting meadow'/'BRB', 'Bob Eggleton').
card_multiverse_id('drifting meadow'/'BRB', '21203').

card_in_set('elvish lyrist', 'BRB').
card_original_type('elvish lyrist'/'BRB', 'Creature — Elf').
card_original_text('elvish lyrist'/'BRB', '{G}, {T}, Sacrifice Elvish Lyrist: Destroy target enchantment.').
card_image_name('elvish lyrist'/'BRB', 'elvish lyrist').
card_uid('elvish lyrist'/'BRB', 'BRB:Elvish Lyrist:elvish lyrist').
card_rarity('elvish lyrist'/'BRB', 'Uncommon').
card_artist('elvish lyrist'/'BRB', 'Rebecca Guay').
card_flavor_text('elvish lyrist'/'BRB', 'Bring the spear of ancient briar;\nBring the torch to light the pyre.\nBring the one who trod our ground;\nBring the spade to dig his mound.').
card_multiverse_id('elvish lyrist'/'BRB', '21188').

card_in_set('exhume', 'BRB').
card_original_type('exhume'/'BRB', 'Sorcery').
card_original_text('exhume'/'BRB', 'Each player puts a creature card from his or her graveyard into play.').
card_image_name('exhume'/'BRB', 'exhume').
card_uid('exhume'/'BRB', 'BRB:Exhume:exhume').
card_rarity('exhume'/'BRB', 'Common').
card_artist('exhume'/'BRB', 'Carl Critchlow').
card_flavor_text('exhume'/'BRB', '\"Death—an outmoded concept. We sleep, and we change.\"\n—Sitrik, birth priest').
card_multiverse_id('exhume'/'BRB', '21153').

card_in_set('fecundity', 'BRB').
card_original_type('fecundity'/'BRB', 'Enchantment').
card_original_text('fecundity'/'BRB', 'Whenever a creature is put into a graveyard from play, that creature\'s controller may draw a card.').
card_image_name('fecundity'/'BRB', 'fecundity').
card_uid('fecundity'/'BRB', 'BRB:Fecundity:fecundity').
card_rarity('fecundity'/'BRB', 'Uncommon').
card_artist('fecundity'/'BRB', 'Rebecca Guay').
card_flavor_text('fecundity'/'BRB', 'Life is eternal. A lifetime is ephemeral.').
card_multiverse_id('fecundity'/'BRB', '21192').

card_in_set('fertile ground', 'BRB').
card_original_type('fertile ground'/'BRB', 'Enchant Land').
card_original_text('fertile ground'/'BRB', 'Whenever enchanted land is tapped for mana, it produces an additional one mana of any color.').
card_image_name('fertile ground'/'BRB', 'fertile ground').
card_uid('fertile ground'/'BRB', 'BRB:Fertile Ground:fertile ground').
card_rarity('fertile ground'/'BRB', 'Common').
card_artist('fertile ground'/'BRB', 'Heather Hudson').
card_flavor_text('fertile ground'/'BRB', 'The forest was too lush for the brothers to despoil—almost.').
card_multiverse_id('fertile ground'/'BRB', '21175').

card_in_set('fire ants', 'BRB').
card_original_type('fire ants'/'BRB', 'Creature — Insect').
card_original_text('fire ants'/'BRB', '{T}: Fire Ants deals 1 damage to each other creature without flying.').
card_image_name('fire ants'/'BRB', 'fire ants').
card_uid('fire ants'/'BRB', 'BRB:Fire Ants:fire ants').
card_rarity('fire ants'/'BRB', 'Uncommon').
card_artist('fire ants'/'BRB', 'Tom Fleming').
card_flavor_text('fire ants'/'BRB', 'Visitors to Shiv fear the dragons, the goblins, or the viashino. Natives fear the ants.').
card_multiverse_id('fire ants'/'BRB', '21163').

card_in_set('flood', 'BRB').
card_original_type('flood'/'BRB', 'Enchantment').
card_original_text('flood'/'BRB', '{U}{U} Tap target creature without flying.').
card_image_name('flood'/'BRB', 'flood').
card_uid('flood'/'BRB', 'BRB:Flood:flood').
card_rarity('flood'/'BRB', 'Common').
card_artist('flood'/'BRB', 'Dennis Detwiller').
card_flavor_text('flood'/'BRB', '\"A dash of cool water does wonders to clear a cluttered battlefield.\"\n—Vibekke Ragnild, Witches and War').
card_multiverse_id('flood'/'BRB', '21139').

card_in_set('forest', 'BRB').
card_original_type('forest'/'BRB', 'Land').
card_original_text('forest'/'BRB', 'G').
card_image_name('forest'/'BRB', 'forest1').
card_uid('forest'/'BRB', 'BRB:Forest:forest1').
card_rarity('forest'/'BRB', 'Basic Land').
card_artist('forest'/'BRB', 'David O\'Connor').
card_multiverse_id('forest'/'BRB', '21119').

card_in_set('forest', 'BRB').
card_original_type('forest'/'BRB', 'Land').
card_original_text('forest'/'BRB', 'G').
card_image_name('forest'/'BRB', 'forest2').
card_uid('forest'/'BRB', 'BRB:Forest:forest2').
card_rarity('forest'/'BRB', 'Basic Land').
card_artist('forest'/'BRB', 'David O\'Connor').
card_multiverse_id('forest'/'BRB', '22347').

card_in_set('forest', 'BRB').
card_original_type('forest'/'BRB', 'Land').
card_original_text('forest'/'BRB', 'G').
card_image_name('forest'/'BRB', 'forest3').
card_uid('forest'/'BRB', 'BRB:Forest:forest3').
card_rarity('forest'/'BRB', 'Basic Land').
card_artist('forest'/'BRB', 'John Avon').
card_multiverse_id('forest'/'BRB', '22351').

card_in_set('forest', 'BRB').
card_original_type('forest'/'BRB', 'Land').
card_original_text('forest'/'BRB', 'G').
card_image_name('forest'/'BRB', 'forest4').
card_uid('forest'/'BRB', 'BRB:Forest:forest4').
card_rarity('forest'/'BRB', 'Basic Land').
card_artist('forest'/'BRB', 'John Avon').
card_multiverse_id('forest'/'BRB', '22349').

card_in_set('forest', 'BRB').
card_original_type('forest'/'BRB', 'Land').
card_original_text('forest'/'BRB', 'G').
card_image_name('forest'/'BRB', 'forest5').
card_uid('forest'/'BRB', 'BRB:Forest:forest5').
card_rarity('forest'/'BRB', 'Basic Land').
card_artist('forest'/'BRB', 'John Avon').
card_multiverse_id('forest'/'BRB', '22348').

card_in_set('forest', 'BRB').
card_original_type('forest'/'BRB', 'Land').
card_original_text('forest'/'BRB', 'G').
card_image_name('forest'/'BRB', 'forest6').
card_uid('forest'/'BRB', 'BRB:Forest:forest6').
card_rarity('forest'/'BRB', 'Basic Land').
card_artist('forest'/'BRB', 'John Avon').
card_multiverse_id('forest'/'BRB', '22352').

card_in_set('forest', 'BRB').
card_original_type('forest'/'BRB', 'Land').
card_original_text('forest'/'BRB', 'G').
card_image_name('forest'/'BRB', 'forest7').
card_uid('forest'/'BRB', 'BRB:Forest:forest7').
card_rarity('forest'/'BRB', 'Basic Land').
card_artist('forest'/'BRB', 'Ji Yong').
card_multiverse_id('forest'/'BRB', '22355').

card_in_set('forest', 'BRB').
card_original_type('forest'/'BRB', 'Land').
card_original_text('forest'/'BRB', 'G').
card_image_name('forest'/'BRB', 'forest8').
card_uid('forest'/'BRB', 'BRB:Forest:forest8').
card_rarity('forest'/'BRB', 'Basic Land').
card_artist('forest'/'BRB', 'Ji Yong').
card_multiverse_id('forest'/'BRB', '22353').

card_in_set('forest', 'BRB').
card_original_type('forest'/'BRB', 'Land').
card_original_text('forest'/'BRB', 'G').
card_image_name('forest'/'BRB', 'forest9').
card_uid('forest'/'BRB', 'BRB:Forest:forest9').
card_rarity('forest'/'BRB', 'Basic Land').
card_artist('forest'/'BRB', 'Ji Yong').
card_multiverse_id('forest'/'BRB', '22354').

card_in_set('giant growth', 'BRB').
card_original_type('giant growth'/'BRB', 'Instant').
card_original_text('giant growth'/'BRB', 'Target creature gets +3/+3 until end of turn.').
card_image_name('giant growth'/'BRB', 'giant growth').
card_uid('giant growth'/'BRB', 'BRB:Giant Growth:giant growth').
card_rarity('giant growth'/'BRB', 'Common').
card_artist('giant growth'/'BRB', 'DiTerlizzi').
card_multiverse_id('giant growth'/'BRB', '21194').

card_in_set('gorilla warrior', 'BRB').
card_original_type('gorilla warrior'/'BRB', 'Creature — Ape').
card_original_text('gorilla warrior'/'BRB', '').
card_image_name('gorilla warrior'/'BRB', 'gorilla warrior').
card_uid('gorilla warrior'/'BRB', 'BRB:Gorilla Warrior:gorilla warrior').
card_rarity('gorilla warrior'/'BRB', 'Common').
card_artist('gorilla warrior'/'BRB', 'John Matson').
card_flavor_text('gorilla warrior'/'BRB', 'They were formidable even before they learned the use of weapons.').
card_multiverse_id('gorilla warrior'/'BRB', '21205').

card_in_set('healing salve', 'BRB').
card_original_type('healing salve'/'BRB', 'Instant').
card_original_text('healing salve'/'BRB', 'Choose one Target player gains 3 life; or prevent the next 3 damage that would be dealt to target creature or player this turn.').
card_image_name('healing salve'/'BRB', 'healing salve').
card_uid('healing salve'/'BRB', 'BRB:Healing Salve:healing salve').
card_rarity('healing salve'/'BRB', 'Common').
card_artist('healing salve'/'BRB', 'Zina Saunders').
card_multiverse_id('healing salve'/'BRB', '21130').

card_in_set('heat ray', 'BRB').
card_original_type('heat ray'/'BRB', 'Instant').
card_original_text('heat ray'/'BRB', 'Heat Ray deals X damage to target creature.').
card_image_name('heat ray'/'BRB', 'heat ray').
card_uid('heat ray'/'BRB', 'BRB:Heat Ray:heat ray').
card_rarity('heat ray'/'BRB', 'Common').
card_artist('heat ray'/'BRB', 'Brian Snõddy').
card_flavor_text('heat ray'/'BRB', 'It\'s not known whether the Thran built the device to forge their wonders or to defend them.').
card_multiverse_id('heat ray'/'BRB', '21159').

card_in_set('hurricane', 'BRB').
card_original_type('hurricane'/'BRB', 'Sorcery').
card_original_text('hurricane'/'BRB', 'Hurricane deals X damage to each creature with flying and each player.').
card_image_name('hurricane'/'BRB', 'hurricane').
card_uid('hurricane'/'BRB', 'BRB:Hurricane:hurricane').
card_rarity('hurricane'/'BRB', 'Uncommon').
card_artist('hurricane'/'BRB', 'Rob Alexander').
card_multiverse_id('hurricane'/'BRB', '21195').

card_in_set('infantry veteran', 'BRB').
card_original_type('infantry veteran'/'BRB', 'Creature — Soldier').
card_original_text('infantry veteran'/'BRB', '{T}: Target attacking creature gets +1/+1 until end of turn.').
card_image_name('infantry veteran'/'BRB', 'infantry veteran').
card_uid('infantry veteran'/'BRB', 'BRB:Infantry Veteran:infantry veteran').
card_rarity('infantry veteran'/'BRB', 'Common').
card_artist('infantry veteran'/'BRB', 'Christopher Rush').
card_flavor_text('infantry veteran'/'BRB', '\"The true dishonor for a soldier is surviving the war.\"\n—Telim\'Tor').
card_multiverse_id('infantry veteran'/'BRB', '21131').

card_in_set('island', 'BRB').
card_original_type('island'/'BRB', 'Land').
card_original_text('island'/'BRB', 'U').
card_image_name('island'/'BRB', 'island1').
card_uid('island'/'BRB', 'BRB:Island:island1').
card_rarity('island'/'BRB', 'Basic Land').
card_artist('island'/'BRB', 'Donato Giancola').
card_multiverse_id('island'/'BRB', '21144').

card_in_set('island', 'BRB').
card_original_type('island'/'BRB', 'Land').
card_original_text('island'/'BRB', 'U').
card_image_name('island'/'BRB', 'island2').
card_uid('island'/'BRB', 'BRB:Island:island2').
card_rarity('island'/'BRB', 'Basic Land').
card_artist('island'/'BRB', 'Donato Giancola').
card_multiverse_id('island'/'BRB', '22364').

card_in_set('island', 'BRB').
card_original_type('island'/'BRB', 'Land').
card_original_text('island'/'BRB', 'U').
card_image_name('island'/'BRB', 'island3').
card_uid('island'/'BRB', 'BRB:Island:island3').
card_rarity('island'/'BRB', 'Basic Land').
card_artist('island'/'BRB', 'Ku Xueming').
card_multiverse_id('island'/'BRB', '22366').

card_in_set('island', 'BRB').
card_original_type('island'/'BRB', 'Land').
card_original_text('island'/'BRB', 'U').
card_image_name('island'/'BRB', 'island4').
card_uid('island'/'BRB', 'BRB:Island:island4').
card_rarity('island'/'BRB', 'Basic Land').
card_artist('island'/'BRB', 'Ku Xueming').
card_multiverse_id('island'/'BRB', '22367').

card_in_set('island', 'BRB').
card_original_type('island'/'BRB', 'Land').
card_original_text('island'/'BRB', 'U').
card_image_name('island'/'BRB', 'island5').
card_uid('island'/'BRB', 'BRB:Island:island5').
card_rarity('island'/'BRB', 'Basic Land').
card_artist('island'/'BRB', 'Ku Xueming').
card_multiverse_id('island'/'BRB', '22365').

card_in_set('land tax', 'BRB').
card_original_type('land tax'/'BRB', 'Enchantment').
card_original_text('land tax'/'BRB', 'At the beginning of your upkeep, if an opponent controls more lands than you, you may search your library for up to three basic land cards, reveal them, then put them into your hand. If you do, shuffle your library.').
card_image_name('land tax'/'BRB', 'land tax').
card_uid('land tax'/'BRB', 'BRB:Land Tax:land tax').
card_rarity('land tax'/'BRB', 'Uncommon').
card_artist('land tax'/'BRB', 'Brian Snõddy').
card_multiverse_id('land tax'/'BRB', '21197').

card_in_set('lhurgoyf', 'BRB').
card_original_type('lhurgoyf'/'BRB', 'Creature — Lhurgoyf').
card_original_text('lhurgoyf'/'BRB', 'Lhurgoyf\'s power is equal to the number of creature cards in all graveyards and its toughness is equal to that number plus 1.').
card_image_name('lhurgoyf'/'BRB', 'lhurgoyf').
card_uid('lhurgoyf'/'BRB', 'BRB:Lhurgoyf:lhurgoyf').
card_rarity('lhurgoyf'/'BRB', 'Rare').
card_artist('lhurgoyf'/'BRB', 'Pete Venters').
card_flavor_text('lhurgoyf'/'BRB', '\"Ach! Hans, run! It\'s the Lhurgoyf!\"\n—Saffi Eriksdotter, last words').
card_multiverse_id('lhurgoyf'/'BRB', '11380').

card_in_set('lightning elemental', 'BRB').
card_original_type('lightning elemental'/'BRB', 'Creature — Elemental').
card_original_text('lightning elemental'/'BRB', 'Haste (This creature may attack and {T} the turn it comes under your control.)').
card_image_name('lightning elemental'/'BRB', 'lightning elemental').
card_uid('lightning elemental'/'BRB', 'BRB:Lightning Elemental:lightning elemental').
card_rarity('lightning elemental'/'BRB', 'Common').
card_artist('lightning elemental'/'BRB', 'D. Alexander Gregory').
card_flavor_text('lightning elemental'/'BRB', 'A lightning elemental once killed an entire tribe of merfolk—simply by going for a swim.').
card_multiverse_id('lightning elemental'/'BRB', '21166').

card_in_set('living death', 'BRB').
card_original_type('living death'/'BRB', 'Sorcery').
card_original_text('living death'/'BRB', 'Set aside all creature cards in all graveyards. Then put each creature that\'s in play into its owner\'s graveyard. Then put each creature card set aside this way into play under its owner\'s control.').
card_image_name('living death'/'BRB', 'living death').
card_uid('living death'/'BRB', 'BRB:Living Death:living death').
card_rarity('living death'/'BRB', 'Rare').
card_artist('living death'/'BRB', 'Charles Gillespie').
card_multiverse_id('living death'/'BRB', '21155').

card_in_set('llanowar elves', 'BRB').
card_original_type('llanowar elves'/'BRB', 'Creature — Elf').
card_original_text('llanowar elves'/'BRB', '{T}: Add {G} to your mana pool.').
card_image_name('llanowar elves'/'BRB', 'llanowar elves').
card_uid('llanowar elves'/'BRB', 'BRB:Llanowar Elves:llanowar elves').
card_rarity('llanowar elves'/'BRB', 'Common').
card_artist('llanowar elves'/'BRB', 'Anson Maddocks').
card_flavor_text('llanowar elves'/'BRB', 'One bone broken for every twig snapped underfoot.\n—Llanowar penalty for trespassing').
card_multiverse_id('llanowar elves'/'BRB', '21107').

card_in_set('man-o\'-war', 'BRB').
card_original_type('man-o\'-war'/'BRB', 'Creature — Jellyfish').
card_original_text('man-o\'-war'/'BRB', 'When Man-o\'-War comes into play, return target creature to its owner\'s hand.').
card_image_name('man-o\'-war'/'BRB', 'man-o\'-war').
card_uid('man-o\'-war'/'BRB', 'BRB:Man-o\'-War:man-o\'-war').
card_rarity('man-o\'-war'/'BRB', 'Common').
card_artist('man-o\'-war'/'BRB', 'Jon J. Muth').
card_flavor_text('man-o\'-war'/'BRB', '\"Beauty to the eye does not always translate to the touch.\"\n—Naimah, Femeref philosopher').
card_multiverse_id('man-o\'-war'/'BRB', '21138').

card_in_set('mana leak', 'BRB').
card_original_type('mana leak'/'BRB', 'Instant').
card_original_text('mana leak'/'BRB', 'Counter target spell unless its controller pays {3}.').
card_image_name('mana leak'/'BRB', 'mana leak').
card_uid('mana leak'/'BRB', 'BRB:Mana Leak:mana leak').
card_rarity('mana leak'/'BRB', 'Common').
card_artist('mana leak'/'BRB', 'Christopher Rush').
card_flavor_text('mana leak'/'BRB', '\"The fatal flaw in every plan is the assumption that you know more than your enemy.\"\n—Volrath').
card_multiverse_id('mana leak'/'BRB', '21126').

card_in_set('maniacal rage', 'BRB').
card_original_type('maniacal rage'/'BRB', 'Enchant Creature').
card_original_text('maniacal rage'/'BRB', 'Enchanted creature gets +2/+2 and can\'t block.').
card_image_name('maniacal rage'/'BRB', 'maniacal rage').
card_uid('maniacal rage'/'BRB', 'BRB:Maniacal Rage:maniacal rage').
card_rarity('maniacal rage'/'BRB', 'Common').
card_artist('maniacal rage'/'BRB', 'Pete Venters').
card_flavor_text('maniacal rage'/'BRB', 'Only by sacrificing any semblance of defense could Gerrard best Greven.').
card_multiverse_id('maniacal rage'/'BRB', '21097').

card_in_set('manta riders', 'BRB').
card_original_type('manta riders'/'BRB', 'Creature — Merfolk').
card_original_text('manta riders'/'BRB', '{U} Manta Riders gains flying until end of turn.').
card_image_name('manta riders'/'BRB', 'manta riders').
card_uid('manta riders'/'BRB', 'BRB:Manta Riders:manta riders').
card_rarity('manta riders'/'BRB', 'Common').
card_artist('manta riders'/'BRB', 'Kaja Foglio').
card_flavor_text('manta riders'/'BRB', '\"Water is firmament to the finned.\"\n—Oracle en-Vec').
card_multiverse_id('manta riders'/'BRB', '21125').

card_in_set('master decoy', 'BRB').
card_original_type('master decoy'/'BRB', 'Creature — Soldier').
card_original_text('master decoy'/'BRB', '{W}, {T}: Tap target creature.').
card_image_name('master decoy'/'BRB', 'master decoy').
card_uid('master decoy'/'BRB', 'BRB:Master Decoy:master decoy').
card_rarity('master decoy'/'BRB', 'Common').
card_artist('master decoy'/'BRB', 'Phil Foglio').
card_flavor_text('master decoy'/'BRB', '\"A skilled decoy can throw your enemies off your trail. A master decoy can survive to do it again.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('master decoy'/'BRB', '21176').

card_in_set('mogg hollows', 'BRB').
card_original_type('mogg hollows'/'BRB', 'Land').
card_original_text('mogg hollows'/'BRB', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {R} or {G} to your mana pool. Mogg Hollows doesn\'t untap during your next untap phase.').
card_image_name('mogg hollows'/'BRB', 'mogg hollows').
card_uid('mogg hollows'/'BRB', 'BRB:Mogg Hollows:mogg hollows').
card_rarity('mogg hollows'/'BRB', 'Uncommon').
card_artist('mogg hollows'/'BRB', 'Jeff Laubenstein').
card_multiverse_id('mogg hollows'/'BRB', '21117').

card_in_set('mountain', 'BRB').
card_original_type('mountain'/'BRB', 'Land').
card_original_text('mountain'/'BRB', 'R').
card_image_name('mountain'/'BRB', 'mountain1').
card_uid('mountain'/'BRB', 'BRB:Mountain:mountain1').
card_rarity('mountain'/'BRB', 'Basic Land').
card_artist('mountain'/'BRB', 'John Avon').
card_multiverse_id('mountain'/'BRB', '22344').

card_in_set('mountain', 'BRB').
card_original_type('mountain'/'BRB', 'Land').
card_original_text('mountain'/'BRB', 'R').
card_image_name('mountain'/'BRB', 'mountain2').
card_uid('mountain'/'BRB', 'BRB:Mountain:mountain2').
card_rarity('mountain'/'BRB', 'Basic Land').
card_artist('mountain'/'BRB', 'Mark Poole').
card_multiverse_id('mountain'/'BRB', '22335').

card_in_set('mountain', 'BRB').
card_original_type('mountain'/'BRB', 'Land').
card_original_text('mountain'/'BRB', 'R').
card_image_name('mountain'/'BRB', 'mountain3').
card_uid('mountain'/'BRB', 'BRB:Mountain:mountain3').
card_rarity('mountain'/'BRB', 'Basic Land').
card_artist('mountain'/'BRB', 'Mark Poole').
card_multiverse_id('mountain'/'BRB', '22336').

card_in_set('mountain', 'BRB').
card_original_type('mountain'/'BRB', 'Land').
card_original_text('mountain'/'BRB', 'R').
card_image_name('mountain'/'BRB', 'mountain4').
card_uid('mountain'/'BRB', 'BRB:Mountain:mountain4').
card_rarity('mountain'/'BRB', 'Basic Land').
card_artist('mountain'/'BRB', 'Rob Alexander').
card_multiverse_id('mountain'/'BRB', '22339').

card_in_set('mountain', 'BRB').
card_original_type('mountain'/'BRB', 'Land').
card_original_text('mountain'/'BRB', 'R').
card_image_name('mountain'/'BRB', 'mountain5').
card_uid('mountain'/'BRB', 'BRB:Mountain:mountain5').
card_rarity('mountain'/'BRB', 'Basic Land').
card_artist('mountain'/'BRB', 'John Avon').
card_multiverse_id('mountain'/'BRB', '22342').

card_in_set('mountain', 'BRB').
card_original_type('mountain'/'BRB', 'Land').
card_original_text('mountain'/'BRB', 'R').
card_image_name('mountain'/'BRB', 'mountain6').
card_uid('mountain'/'BRB', 'BRB:Mountain:mountain6').
card_rarity('mountain'/'BRB', 'Basic Land').
card_artist('mountain'/'BRB', 'John Avon').
card_multiverse_id('mountain'/'BRB', '22334').

card_in_set('mountain', 'BRB').
card_original_type('mountain'/'BRB', 'Land').
card_original_text('mountain'/'BRB', 'R').
card_image_name('mountain'/'BRB', 'mountain7').
card_uid('mountain'/'BRB', 'BRB:Mountain:mountain7').
card_rarity('mountain'/'BRB', 'Basic Land').
card_artist('mountain'/'BRB', 'John Avon').
card_multiverse_id('mountain'/'BRB', '22343').

card_in_set('mountain', 'BRB').
card_original_type('mountain'/'BRB', 'Land').
card_original_text('mountain'/'BRB', 'R').
card_image_name('mountain'/'BRB', 'mountain8').
card_uid('mountain'/'BRB', 'BRB:Mountain:mountain8').
card_rarity('mountain'/'BRB', 'Basic Land').
card_artist('mountain'/'BRB', 'John Avon').
card_multiverse_id('mountain'/'BRB', '21118').

card_in_set('mountain', 'BRB').
card_original_type('mountain'/'BRB', 'Land').
card_original_text('mountain'/'BRB', 'R').
card_image_name('mountain'/'BRB', 'mountain9').
card_uid('mountain'/'BRB', 'BRB:Mountain:mountain9').
card_rarity('mountain'/'BRB', 'Basic Land').
card_artist('mountain'/'BRB', 'John Avon').
card_multiverse_id('mountain'/'BRB', '22340').

card_in_set('nekrataal', 'BRB').
card_original_type('nekrataal'/'BRB', 'Creature — Nekrataal').
card_original_text('nekrataal'/'BRB', 'First strike\nWhen Nekrataal comes into play, destroy target nonartifact, nonblack creature. It can\'t be regenerated.').
card_image_name('nekrataal'/'BRB', 'nekrataal').
card_uid('nekrataal'/'BRB', 'BRB:Nekrataal:nekrataal').
card_rarity('nekrataal'/'BRB', 'Uncommon').
card_artist('nekrataal'/'BRB', 'Adrian Smith').
card_flavor_text('nekrataal'/'BRB', '\"I have seen the horrors Kaervek has freed. My Betrayal is certain—but of Kaervek or of Jamuraa, I cannot say.\"\n—Jolrael').
card_multiverse_id('nekrataal'/'BRB', '15391').

card_in_set('opportunity', 'BRB').
card_original_type('opportunity'/'BRB', 'Instant').
card_original_text('opportunity'/'BRB', 'Target player draws four cards.').
card_image_name('opportunity'/'BRB', 'opportunity').
card_uid('opportunity'/'BRB', 'BRB:Opportunity:opportunity').
card_rarity('opportunity'/'BRB', 'Uncommon').
card_artist('opportunity'/'BRB', 'Ron Spears').
card_flavor_text('opportunity'/'BRB', '\"He cocooned himself alone in his workshop for months. When he finally emerged, all broad grins and excited chatter, I knew he\'d found his answer.\"\n—Barrin, master wizard').
card_multiverse_id('opportunity'/'BRB', '21128').

card_in_set('pacifism', 'BRB').
card_original_type('pacifism'/'BRB', 'Enchant Creature').
card_original_text('pacifism'/'BRB', 'Enchanted creature can\'t attack or block.').
card_image_name('pacifism'/'BRB', 'pacifism').
card_uid('pacifism'/'BRB', 'BRB:Pacifism:pacifism').
card_rarity('pacifism'/'BRB', 'Common').
card_artist('pacifism'/'BRB', 'Robert Bliss').
card_flavor_text('pacifism'/'BRB', 'For the first time in his life, Grakk felt a little warm and fuzzy inside.').
card_multiverse_id('pacifism'/'BRB', '21191').

card_in_set('pestilence', 'BRB').
card_original_type('pestilence'/'BRB', 'Enchantment').
card_original_text('pestilence'/'BRB', 'At end of turn, if there are no creatures in play, sacrifice Pestilence.\n{B} Pestilence deals 1 damage to each creature and each player.').
card_image_name('pestilence'/'BRB', 'pestilence').
card_uid('pestilence'/'BRB', 'BRB:Pestilence:pestilence').
card_rarity('pestilence'/'BRB', 'Uncommon').
card_artist('pestilence'/'BRB', 'Pete Venters').
card_multiverse_id('pestilence'/'BRB', '21149').

card_in_set('phyrexian ghoul', 'BRB').
card_original_type('phyrexian ghoul'/'BRB', 'Creature — Zombie').
card_original_text('phyrexian ghoul'/'BRB', 'Sacrifice a creature: Phyrexian Ghoul gets +2/+2 until end of turn.').
card_image_name('phyrexian ghoul'/'BRB', 'phyrexian ghoul').
card_uid('phyrexian ghoul'/'BRB', 'BRB:Phyrexian Ghoul:phyrexian ghoul').
card_rarity('phyrexian ghoul'/'BRB', 'Common').
card_artist('phyrexian ghoul'/'BRB', 'Pete Venters').
card_flavor_text('phyrexian ghoul'/'BRB', 'Phyrexia wastes nothing. Its food chain is a spiraling cycle.').
card_multiverse_id('phyrexian ghoul'/'BRB', '21164').

card_in_set('pincher beetles', 'BRB').
card_original_type('pincher beetles'/'BRB', 'Creature — Insect').
card_original_text('pincher beetles'/'BRB', 'Pincher Beetles can\'t be the target of spells or abilities.').
card_image_name('pincher beetles'/'BRB', 'pincher beetles').
card_uid('pincher beetles'/'BRB', 'BRB:Pincher Beetles:pincher beetles').
card_rarity('pincher beetles'/'BRB', 'Common').
card_artist('pincher beetles'/'BRB', 'Stephen Daniele').
card_flavor_text('pincher beetles'/'BRB', '\"No fair! Since when does a bug get ta munch on me?\"\n—Squee, goblin cabin hand').
card_multiverse_id('pincher beetles'/'BRB', '21110').

card_in_set('plains', 'BRB').
card_original_type('plains'/'BRB', 'Land').
card_original_text('plains'/'BRB', 'W').
card_image_name('plains'/'BRB', 'plains1').
card_uid('plains'/'BRB', 'BRB:Plains:plains1').
card_rarity('plains'/'BRB', 'Basic Land').
card_artist('plains'/'BRB', 'Pat Morrissey').
card_multiverse_id('plains'/'BRB', '22357').

card_in_set('plains', 'BRB').
card_original_type('plains'/'BRB', 'Land').
card_original_text('plains'/'BRB', 'W').
card_image_name('plains'/'BRB', 'plains2').
card_uid('plains'/'BRB', 'BRB:Plains:plains2').
card_rarity('plains'/'BRB', 'Basic Land').
card_artist('plains'/'BRB', 'Pat Morrissey').
card_multiverse_id('plains'/'BRB', '22356').

card_in_set('plains', 'BRB').
card_original_type('plains'/'BRB', 'Land').
card_original_text('plains'/'BRB', 'W').
card_image_name('plains'/'BRB', 'plains3').
card_uid('plains'/'BRB', 'BRB:Plains:plains3').
card_rarity('plains'/'BRB', 'Basic Land').
card_artist('plains'/'BRB', 'Pat Morrissey').
card_multiverse_id('plains'/'BRB', '21145').

card_in_set('plains', 'BRB').
card_original_type('plains'/'BRB', 'Land').
card_original_text('plains'/'BRB', 'W').
card_image_name('plains'/'BRB', 'plains4').
card_uid('plains'/'BRB', 'BRB:Plains:plains4').
card_rarity('plains'/'BRB', 'Basic Land').
card_artist('plains'/'BRB', 'Douglas Shuler').
card_multiverse_id('plains'/'BRB', '22362').

card_in_set('plains', 'BRB').
card_original_type('plains'/'BRB', 'Land').
card_original_text('plains'/'BRB', 'W').
card_image_name('plains'/'BRB', 'plains5').
card_uid('plains'/'BRB', 'BRB:Plains:plains5').
card_rarity('plains'/'BRB', 'Basic Land').
card_artist('plains'/'BRB', 'Douglas Shuler').
card_multiverse_id('plains'/'BRB', '22361').

card_in_set('plains', 'BRB').
card_original_type('plains'/'BRB', 'Land').
card_original_text('plains'/'BRB', 'W').
card_image_name('plains'/'BRB', 'plains6').
card_uid('plains'/'BRB', 'BRB:Plains:plains6').
card_rarity('plains'/'BRB', 'Basic Land').
card_artist('plains'/'BRB', 'Douglas Shuler').
card_multiverse_id('plains'/'BRB', '22363').

card_in_set('plains', 'BRB').
card_original_type('plains'/'BRB', 'Land').
card_original_text('plains'/'BRB', 'W').
card_image_name('plains'/'BRB', 'plains7').
card_uid('plains'/'BRB', 'BRB:Plains:plains7').
card_rarity('plains'/'BRB', 'Basic Land').
card_artist('plains'/'BRB', 'He Jiancheng').
card_multiverse_id('plains'/'BRB', '22360').

card_in_set('plains', 'BRB').
card_original_type('plains'/'BRB', 'Land').
card_original_text('plains'/'BRB', 'W').
card_image_name('plains'/'BRB', 'plains8').
card_uid('plains'/'BRB', 'BRB:Plains:plains8').
card_rarity('plains'/'BRB', 'Basic Land').
card_artist('plains'/'BRB', 'He Jiancheng').
card_multiverse_id('plains'/'BRB', '22358').

card_in_set('plains', 'BRB').
card_original_type('plains'/'BRB', 'Land').
card_original_text('plains'/'BRB', 'W').
card_image_name('plains'/'BRB', 'plains9').
card_uid('plains'/'BRB', 'BRB:Plains:plains9').
card_rarity('plains'/'BRB', 'Basic Land').
card_artist('plains'/'BRB', 'He Jiancheng').
card_multiverse_id('plains'/'BRB', '22359').

card_in_set('plated rootwalla', 'BRB').
card_original_type('plated rootwalla'/'BRB', 'Creature — Lizard').
card_original_text('plated rootwalla'/'BRB', '{2}{G} Plated Rootwalla gets +3/+3 until end of turn. Play this ability only once each turn.').
card_image_name('plated rootwalla'/'BRB', 'plated rootwalla').
card_uid('plated rootwalla'/'BRB', 'BRB:Plated Rootwalla:plated rootwalla').
card_rarity('plated rootwalla'/'BRB', 'Common').
card_artist('plated rootwalla'/'BRB', 'Randy Elliott').
card_flavor_text('plated rootwalla'/'BRB', '\" . . . And the third little boar built his house out of rootwalla plates . . . .\"\n—Skyshroud children\'s story').
card_multiverse_id('plated rootwalla'/'BRB', '21116').

card_in_set('polluted mire', 'BRB').
card_original_type('polluted mire'/'BRB', 'Land').
card_original_text('polluted mire'/'BRB', 'Cycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability any time you could play an instant.)\nPolluted Mire comes into play tapped.\n{T}: Add {B} to your mana pool.').
card_image_name('polluted mire'/'BRB', 'polluted mire').
card_uid('polluted mire'/'BRB', 'BRB:Polluted Mire:polluted mire').
card_rarity('polluted mire'/'BRB', 'Common').
card_artist('polluted mire'/'BRB', 'Stephen Daniele').
card_multiverse_id('polluted mire'/'BRB', '21202').

card_in_set('prodigal sorcerer', 'BRB').
card_original_type('prodigal sorcerer'/'BRB', 'Creature — Wizard').
card_original_text('prodigal sorcerer'/'BRB', '{T}: Prodigal Sorcerer deals 1 damage to target creature or player.').
card_image_name('prodigal sorcerer'/'BRB', 'prodigal sorcerer').
card_uid('prodigal sorcerer'/'BRB', 'BRB:Prodigal Sorcerer:prodigal sorcerer').
card_rarity('prodigal sorcerer'/'BRB', 'Common').
card_artist('prodigal sorcerer'/'BRB', 'Douglas Shuler').
card_flavor_text('prodigal sorcerer'/'BRB', 'Occasionally members of the Institute of Arcane Study acquire a taste for worldly pleasures. Seldom do they have trouble finding employment.').
card_multiverse_id('prodigal sorcerer'/'BRB', '21129').

card_in_set('raging goblin', 'BRB').
card_original_type('raging goblin'/'BRB', 'Creature — Goblin').
card_original_text('raging goblin'/'BRB', 'Haste (This creature may attack and {T} the turn it comes under your control.)').
card_image_name('raging goblin'/'BRB', 'raging goblin').
card_uid('raging goblin'/'BRB', 'BRB:Raging Goblin:raging goblin').
card_rarity('raging goblin'/'BRB', 'Common').
card_artist('raging goblin'/'BRB', 'Jeff Miracola').
card_flavor_text('raging goblin'/'BRB', 'He raged at the world, at his family, at his life. But mostly he just raged.').
card_multiverse_id('raging goblin'/'BRB', '21162').

card_in_set('ray of command', 'BRB').
card_original_type('ray of command'/'BRB', 'Instant').
card_original_text('ray of command'/'BRB', 'Untap target creature an opponent controls and gain control of it until end of turn. That creature gains haste until end of turn. (It may attack and {T} the turn it comes under your control.)').
card_image_name('ray of command'/'BRB', 'ray of command').
card_uid('ray of command'/'BRB', 'BRB:Ray of Command:ray of command').
card_rarity('ray of command'/'BRB', 'Common').
card_artist('ray of command'/'BRB', 'Andrew Robinson').
card_flavor_text('ray of command'/'BRB', '\"Heel.\"').
card_multiverse_id('ray of command'/'BRB', '21142').

card_in_set('reanimate', 'BRB').
card_original_type('reanimate'/'BRB', 'Sorcery').
card_original_text('reanimate'/'BRB', 'Put target creature card from a graveyard into play under your control. You lose life equal to that creature\'s converted mana cost.').
card_image_name('reanimate'/'BRB', 'reanimate').
card_uid('reanimate'/'BRB', 'BRB:Reanimate:reanimate').
card_rarity('reanimate'/'BRB', 'Uncommon').
card_artist('reanimate'/'BRB', 'Robert Bliss').
card_flavor_text('reanimate'/'BRB', '\"You will learn to earn death.\"\n—Volrath').
card_multiverse_id('reanimate'/'BRB', '21160').

card_in_set('remote isle', 'BRB').
card_original_type('remote isle'/'BRB', 'Land').
card_original_text('remote isle'/'BRB', 'Cycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability any time you could play an instant.)\nRemote Isle comes into play tapped.\n{T}: Add {U} to your mana pool.').
card_image_name('remote isle'/'BRB', 'remote isle').
card_uid('remote isle'/'BRB', 'BRB:Remote Isle:remote isle').
card_rarity('remote isle'/'BRB', 'Common').
card_artist('remote isle'/'BRB', 'Ciruelo').
card_multiverse_id('remote isle'/'BRB', '21200').

card_in_set('river boa', 'BRB').
card_original_type('river boa'/'BRB', 'Creature — Snake').
card_original_text('river boa'/'BRB', 'Islandwalk (This creature is unblockable as long as defending player controls an island.)\n{G} Regenerate River Boa.').
card_image_name('river boa'/'BRB', 'river boa').
card_uid('river boa'/'BRB', 'BRB:River Boa:river boa').
card_rarity('river boa'/'BRB', 'Uncommon').
card_artist('river boa'/'BRB', 'Steve White').
card_flavor_text('river boa'/'BRB', '\"But no one heard the snake\'s gentle hiss for peace over the elephant\'s trumpeting of war.\"\n—Afari, Tales').
card_multiverse_id('river boa'/'BRB', '21109').

card_in_set('rolling thunder', 'BRB').
card_original_type('rolling thunder'/'BRB', 'Sorcery').
card_original_text('rolling thunder'/'BRB', 'Rolling Thunder deals X damage divided as you choose among any number of target creatures and/or players.').
card_image_name('rolling thunder'/'BRB', 'rolling thunder').
card_uid('rolling thunder'/'BRB', 'BRB:Rolling Thunder:rolling thunder').
card_rarity('rolling thunder'/'BRB', 'Common').
card_artist('rolling thunder'/'BRB', 'Richard Thomas').
card_flavor_text('rolling thunder'/'BRB', '\"Such rage,\" thought Vhati, gazing up at the thunderhead from the Predator. \"It is Greven\'s mind manifest.\"').
card_multiverse_id('rolling thunder'/'BRB', '21158').

card_in_set('sadistic glee', 'BRB').
card_original_type('sadistic glee'/'BRB', 'Enchant Creature').
card_original_text('sadistic glee'/'BRB', 'Whenever a creature is put into a graveyard from play, put a +1/+1 counter on enchanted creature.').
card_image_name('sadistic glee'/'BRB', 'sadistic glee').
card_uid('sadistic glee'/'BRB', 'BRB:Sadistic Glee:sadistic glee').
card_rarity('sadistic glee'/'BRB', 'Common').
card_artist('sadistic glee'/'BRB', 'Pete Venters').
card_flavor_text('sadistic glee'/'BRB', '\"First blood isn\'t as important as last blood.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('sadistic glee'/'BRB', '21150').

card_in_set('sanctum custodian', 'BRB').
card_original_type('sanctum custodian'/'BRB', 'Creature — Cleric').
card_original_text('sanctum custodian'/'BRB', '{T}: Prevent the next 2 damage that would be dealt to target creature or player this turn.').
card_image_name('sanctum custodian'/'BRB', 'sanctum custodian').
card_uid('sanctum custodian'/'BRB', 'BRB:Sanctum Custodian:sanctum custodian').
card_rarity('sanctum custodian'/'BRB', 'Common').
card_artist('sanctum custodian'/'BRB', 'Paolo Parente').
card_flavor_text('sanctum custodian'/'BRB', 'Serra told them to guard Urza as he healed. Five years they stood.').
card_multiverse_id('sanctum custodian'/'BRB', '21184').

card_in_set('sanctum guardian', 'BRB').
card_original_type('sanctum guardian'/'BRB', 'Creature — Soldier').
card_original_text('sanctum guardian'/'BRB', 'Sacrifice Sanctum Guardian: The next time a source of your choice would deal damage to target creature or player this turn, prevent that damage.').
card_image_name('sanctum guardian'/'BRB', 'sanctum guardian').
card_uid('sanctum guardian'/'BRB', 'BRB:Sanctum Guardian:sanctum guardian').
card_rarity('sanctum guardian'/'BRB', 'Uncommon').
card_artist('sanctum guardian'/'BRB', 'Donato Giancola').
card_flavor_text('sanctum guardian'/'BRB', '\"Protect our mother in her womb.\"').
card_multiverse_id('sanctum guardian'/'BRB', '21185').

card_in_set('sandstorm', 'BRB').
card_original_type('sandstorm'/'BRB', 'Instant').
card_original_text('sandstorm'/'BRB', 'Sandstorm deals 1 damage to each attacking creature.').
card_image_name('sandstorm'/'BRB', 'sandstorm').
card_uid('sandstorm'/'BRB', 'BRB:Sandstorm:sandstorm').
card_rarity('sandstorm'/'BRB', 'Common').
card_artist('sandstorm'/'BRB', 'Brian Snõddy').
card_flavor_text('sandstorm'/'BRB', 'Even the landscape turned against Sarsour, first rising up and pelting him, then rearranging itself so he could no longer find his way.').
card_multiverse_id('sandstorm'/'BRB', '21193').

card_in_set('scaled wurm', 'BRB').
card_original_type('scaled wurm'/'BRB', 'Creature — Wurm').
card_original_text('scaled wurm'/'BRB', '').
card_image_name('scaled wurm'/'BRB', 'scaled wurm').
card_uid('scaled wurm'/'BRB', 'BRB:Scaled Wurm:scaled wurm').
card_rarity('scaled wurm'/'BRB', 'Common').
card_artist('scaled wurm'/'BRB', 'Daniel Gelon').
card_flavor_text('scaled wurm'/'BRB', '\"Flourishing during the Ice Age, these wurms were the bane of all Kjeldorans. Their great size and ferocity made them the subject of countless nightmares—they embodied the worst of the Ice Age.\"\n—Kjeldor: Ice Civilization').
card_multiverse_id('scaled wurm'/'BRB', '21174').

card_in_set('scryb sprites', 'BRB').
card_original_type('scryb sprites'/'BRB', 'Creature — Faerie').
card_original_text('scryb sprites'/'BRB', 'Flying').
card_image_name('scryb sprites'/'BRB', 'scryb sprites').
card_uid('scryb sprites'/'BRB', 'BRB:Scryb Sprites:scryb sprites').
card_rarity('scryb sprites'/'BRB', 'Common').
card_artist('scryb sprites'/'BRB', 'Amy Weber').
card_flavor_text('scryb sprites'/'BRB', 'The only sound was the gentle clicking of the faeries\' wings. Then those intruders who were still standing turned and fled. One thing was certain: they didn\'t think the Scryb were very funny anymore.').
card_multiverse_id('scryb sprites'/'BRB', '21105').

card_in_set('seasoned marshal', 'BRB').
card_original_type('seasoned marshal'/'BRB', 'Creature — Soldier').
card_original_text('seasoned marshal'/'BRB', 'Whenever Seasoned Marshal attacks, you may tap target creature.').
card_image_name('seasoned marshal'/'BRB', 'seasoned marshal').
card_uid('seasoned marshal'/'BRB', 'BRB:Seasoned Marshal:seasoned marshal').
card_rarity('seasoned marshal'/'BRB', 'Uncommon').
card_artist('seasoned marshal'/'BRB', 'Matthew D. Wilson').
card_flavor_text('seasoned marshal'/'BRB', 'There are only two rules of tactics: never be without a plan, and never rely on it.').
card_multiverse_id('seasoned marshal'/'BRB', '21183').

card_in_set('seeker of skybreak', 'BRB').
card_original_type('seeker of skybreak'/'BRB', 'Creature — Elf').
card_original_text('seeker of skybreak'/'BRB', '{T}: Untap target creature.').
card_image_name('seeker of skybreak'/'BRB', 'seeker of skybreak').
card_uid('seeker of skybreak'/'BRB', 'BRB:Seeker of Skybreak:seeker of skybreak').
card_rarity('seeker of skybreak'/'BRB', 'Common').
card_artist('seeker of skybreak'/'BRB', 'Daren Bader').
card_flavor_text('seeker of skybreak'/'BRB', '\"We shun them not for their dream but for their refusal to let such a noble dream die a noble death.\"\n—Eladamri, Lord of Leaves').
card_multiverse_id('seeker of skybreak'/'BRB', '21108').

card_in_set('sengir vampire', 'BRB').
card_original_type('sengir vampire'/'BRB', 'Creature — Vampire').
card_original_text('sengir vampire'/'BRB', 'Flying\nWhenever a creature is put into a graveyard from play, if Sengir Vampire dealt damage to it this turn, put a +1/+1 counter on Sengir Vampire.').
card_image_name('sengir vampire'/'BRB', 'sengir vampire').
card_uid('sengir vampire'/'BRB', 'BRB:Sengir Vampire:sengir vampire').
card_rarity('sengir vampire'/'BRB', 'Uncommon').
card_artist('sengir vampire'/'BRB', 'Anson Maddocks').
card_multiverse_id('sengir vampire'/'BRB', '21169').

card_in_set('sewer rats', 'BRB').
card_original_type('sewer rats'/'BRB', 'Creature — Rat').
card_original_text('sewer rats'/'BRB', '{B}, Pay 1 life: Sewer Rats gets +1/+0 until end of turn. No more than {B}{B}{B} may be spent this way each turn.').
card_image_name('sewer rats'/'BRB', 'sewer rats').
card_uid('sewer rats'/'BRB', 'BRB:Sewer Rats:sewer rats').
card_rarity('sewer rats'/'BRB', 'Common').
card_artist('sewer rats'/'BRB', 'Martin McKenna').
card_flavor_text('sewer rats'/'BRB', 'You lie down with rats, and the rats run away.\n—Suq\'Ata insult').
card_multiverse_id('sewer rats'/'BRB', '21161').

card_in_set('shower of sparks', 'BRB').
card_original_type('shower of sparks'/'BRB', 'Instant').
card_original_text('shower of sparks'/'BRB', 'Shower of Sparks deals 1 damage to target creature and 1 damage to target player.').
card_image_name('shower of sparks'/'BRB', 'shower of sparks').
card_uid('shower of sparks'/'BRB', 'BRB:Shower of Sparks:shower of sparks').
card_rarity('shower of sparks'/'BRB', 'Common').
card_artist('shower of sparks'/'BRB', 'Christopher Moeller').
card_flavor_text('shower of sparks'/'BRB', 'The viashino had learned how to operate the rig through trial and error—mostly error.').
card_multiverse_id('shower of sparks'/'BRB', '21104').

card_in_set('skyshroud elite', 'BRB').
card_original_type('skyshroud elite'/'BRB', 'Creature — Elf').
card_original_text('skyshroud elite'/'BRB', 'Skyshroud Elite gets +1/+2 as long as an opponent controls a nonbasic land.').
card_image_name('skyshroud elite'/'BRB', 'skyshroud elite').
card_uid('skyshroud elite'/'BRB', 'BRB:Skyshroud Elite:skyshroud elite').
card_rarity('skyshroud elite'/'BRB', 'Uncommon').
card_artist('skyshroud elite'/'BRB', 'Paolo Parente').
card_flavor_text('skyshroud elite'/'BRB', '\"Civilization is a conspiracy to disguise the mutilation of nature.\"\n—Skyshroud elite creed').
card_multiverse_id('skyshroud elite'/'BRB', '21106').

card_in_set('slippery karst', 'BRB').
card_original_type('slippery karst'/'BRB', 'Land').
card_original_text('slippery karst'/'BRB', 'Cycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability any time you could play an instant.)\nSlippery Karst comes into play tapped.\n{T}: Add {G} to your mana pool.').
card_image_name('slippery karst'/'BRB', 'slippery karst').
card_uid('slippery karst'/'BRB', 'BRB:Slippery Karst:slippery karst').
card_rarity('slippery karst'/'BRB', 'Common').
card_artist('slippery karst'/'BRB', 'Stephen Daniele').
card_multiverse_id('slippery karst'/'BRB', '21199').

card_in_set('soltari foot soldier', 'BRB').
card_original_type('soltari foot soldier'/'BRB', 'Creature — Soldier').
card_original_text('soltari foot soldier'/'BRB', 'Shadow (This creature can block or be blocked by only creatures with shadow.)').
card_image_name('soltari foot soldier'/'BRB', 'soltari foot soldier').
card_uid('soltari foot soldier'/'BRB', 'BRB:Soltari Foot Soldier:soltari foot soldier').
card_rarity('soltari foot soldier'/'BRB', 'Common').
card_artist('soltari foot soldier'/'BRB', 'Janet Aulisio').
card_flavor_text('soltari foot soldier'/'BRB', '\"Children of the Ruins, raised to be warriors, know that life begins when another speaks their names.\"\n—Soltari Tales of Life').
card_multiverse_id('soltari foot soldier'/'BRB', '21187').

card_in_set('songstitcher', 'BRB').
card_original_type('songstitcher'/'BRB', 'Creature — Cleric').
card_original_text('songstitcher'/'BRB', '{1}{W} Prevent all combat damage that would be dealt this turn by target attacking creature with flying.').
card_image_name('songstitcher'/'BRB', 'songstitcher').
card_uid('songstitcher'/'BRB', 'BRB:Songstitcher:songstitcher').
card_rarity('songstitcher'/'BRB', 'Uncommon').
card_artist('songstitcher'/'BRB', 'Berry').
card_flavor_text('songstitcher'/'BRB', 'The true names of birds are songs woven into their souls.').
card_multiverse_id('songstitcher'/'BRB', '21134').

card_in_set('soul warden', 'BRB').
card_original_type('soul warden'/'BRB', 'Creature — Cleric').
card_original_text('soul warden'/'BRB', 'Whenever another creature comes into play, you gain 1 life.').
card_image_name('soul warden'/'BRB', 'soul warden').
card_uid('soul warden'/'BRB', 'BRB:Soul Warden:soul warden').
card_rarity('soul warden'/'BRB', 'Common').
card_artist('soul warden'/'BRB', 'Randy Gallegos').
card_flavor_text('soul warden'/'BRB', 'Count carefully the souls and see that none are lost.\n—Vec teaching').
card_multiverse_id('soul warden'/'BRB', '21132').

card_in_set('spike colony', 'BRB').
card_original_type('spike colony'/'BRB', 'Creature — Spike').
card_original_text('spike colony'/'BRB', 'Spike Colony comes into play with four +1/+1 counters on it.\n{2}, Remove a +1/+1 counter from Spike Colony: Put a +1/+1 counter on target creature.').
card_image_name('spike colony'/'BRB', 'spike colony').
card_uid('spike colony'/'BRB', 'BRB:Spike Colony:spike colony').
card_rarity('spike colony'/'BRB', 'Common').
card_artist('spike colony'/'BRB', 'Douglas Shuler').
card_multiverse_id('spike colony'/'BRB', '21181').

card_in_set('spike feeder', 'BRB').
card_original_type('spike feeder'/'BRB', 'Creature — Spike').
card_original_text('spike feeder'/'BRB', 'Spike Feeder comes into play with two +1/+1 counters on it.\n{2}, Remove a +1/+1 counter from Spike Feeder: Put a +1/+1 counter on target creature.\nRemove a +1/+1 counter from Spike Feeder: You gain 2 life.').
card_image_name('spike feeder'/'BRB', 'spike feeder').
card_uid('spike feeder'/'BRB', 'BRB:Spike Feeder:spike feeder').
card_rarity('spike feeder'/'BRB', 'Uncommon').
card_artist('spike feeder'/'BRB', 'Heather Hudson').
card_multiverse_id('spike feeder'/'BRB', '21113').

card_in_set('spike weaver', 'BRB').
card_original_type('spike weaver'/'BRB', 'Creature — Spike').
card_original_text('spike weaver'/'BRB', 'Spike Weaver comes into play with three +1/+1 counters on it.\n{2}, Remove a +1/+1 counter from Spike Weaver: Put a +1/+1 counter on target creature.\n{1}, Remove a +1/+1 counter from Spike Weaver: Prevent all combat damage that would be dealt this turn.').
card_image_name('spike weaver'/'BRB', 'spike weaver').
card_uid('spike weaver'/'BRB', 'BRB:Spike Weaver:spike weaver').
card_rarity('spike weaver'/'BRB', 'Rare').
card_artist('spike weaver'/'BRB', 'Mike Raabe').
card_multiverse_id('spike weaver'/'BRB', '21179').

card_in_set('spike worker', 'BRB').
card_original_type('spike worker'/'BRB', 'Creature — Spike').
card_original_text('spike worker'/'BRB', 'Spike Worker comes into play with two +1/+1 counters on it.\n{2}, Remove a +1/+1 counter from Spike Worker: Put a +1/+1 counter on target creature.').
card_image_name('spike worker'/'BRB', 'spike worker').
card_uid('spike worker'/'BRB', 'BRB:Spike Worker:spike worker').
card_rarity('spike worker'/'BRB', 'Common').
card_artist('spike worker'/'BRB', 'Daniel Gelon').
card_multiverse_id('spike worker'/'BRB', '21198').

card_in_set('steam blast', 'BRB').
card_original_type('steam blast'/'BRB', 'Sorcery').
card_original_text('steam blast'/'BRB', 'Steam Blast deals 2 damage to each creature and each player.').
card_image_name('steam blast'/'BRB', 'steam blast').
card_uid('steam blast'/'BRB', 'BRB:Steam Blast:steam blast').
card_rarity('steam blast'/'BRB', 'Uncommon').
card_artist('steam blast'/'BRB', 'Mike Raabe').
card_flavor_text('steam blast'/'BRB', 'The viashino knew of the cracked pipes but deliberately left them unmended to bolster the rig\'s defenses.').
card_multiverse_id('steam blast'/'BRB', '21204').

card_in_set('subversion', 'BRB').
card_original_type('subversion'/'BRB', 'Enchantment').
card_original_text('subversion'/'BRB', 'At the beginning of your upkeep, each opponent loses 1 life. You gain life equal to the life lost this way.').
card_image_name('subversion'/'BRB', 'subversion').
card_uid('subversion'/'BRB', 'BRB:Subversion:subversion').
card_rarity('subversion'/'BRB', 'Rare').
card_artist('subversion'/'BRB', 'Rob Alexander').
card_flavor_text('subversion'/'BRB', 'Kerrick\'s corrupt domain swelled like a blister on Tolaria\'s skin.').
card_multiverse_id('subversion'/'BRB', '21148').

card_in_set('sun clasp', 'BRB').
card_original_type('sun clasp'/'BRB', 'Enchant Creature').
card_original_text('sun clasp'/'BRB', 'Enchanted creature gets +1/+3.\n{W} Return enchanted creature to its owner\'s hand.').
card_image_name('sun clasp'/'BRB', 'sun clasp').
card_uid('sun clasp'/'BRB', 'BRB:Sun Clasp:sun clasp').
card_rarity('sun clasp'/'BRB', 'Common').
card_artist('sun clasp'/'BRB', 'John Coulthart').
card_flavor_text('sun clasp'/'BRB', '\"And darkness shall be cast from me\nFor my soul resides in the Sun.\"\n—Femeref dirge').
card_multiverse_id('sun clasp'/'BRB', '21190').

card_in_set('swamp', 'BRB').
card_original_type('swamp'/'BRB', 'Land').
card_original_text('swamp'/'BRB', 'B').
card_image_name('swamp'/'BRB', 'swamp1').
card_uid('swamp'/'BRB', 'BRB:Swamp:swamp1').
card_rarity('swamp'/'BRB', 'Basic Land').
card_artist('swamp'/'BRB', 'John Avon').
card_multiverse_id('swamp'/'BRB', '21171').

card_in_set('swamp', 'BRB').
card_original_type('swamp'/'BRB', 'Land').
card_original_text('swamp'/'BRB', 'B').
card_image_name('swamp'/'BRB', 'swamp2').
card_uid('swamp'/'BRB', 'BRB:Swamp:swamp2').
card_rarity('swamp'/'BRB', 'Basic Land').
card_artist('swamp'/'BRB', 'John Avon').
card_multiverse_id('swamp'/'BRB', '22370').

card_in_set('swamp', 'BRB').
card_original_type('swamp'/'BRB', 'Land').
card_original_text('swamp'/'BRB', 'B').
card_image_name('swamp'/'BRB', 'swamp3').
card_uid('swamp'/'BRB', 'BRB:Swamp:swamp3').
card_rarity('swamp'/'BRB', 'Basic Land').
card_artist('swamp'/'BRB', 'John Avon').
card_multiverse_id('swamp'/'BRB', '22369').

card_in_set('swamp', 'BRB').
card_original_type('swamp'/'BRB', 'Land').
card_original_text('swamp'/'BRB', 'B').
card_image_name('swamp'/'BRB', 'swamp4').
card_uid('swamp'/'BRB', 'BRB:Swamp:swamp4').
card_rarity('swamp'/'BRB', 'Basic Land').
card_artist('swamp'/'BRB', 'John Avon').
card_multiverse_id('swamp'/'BRB', '22368').

card_in_set('swords to plowshares', 'BRB').
card_original_type('swords to plowshares'/'BRB', 'Instant').
card_original_text('swords to plowshares'/'BRB', 'Remove target creature from the game. Its controller gains life equal to its power.').
card_image_name('swords to plowshares'/'BRB', 'swords to plowshares').
card_uid('swords to plowshares'/'BRB', 'BRB:Swords to Plowshares:swords to plowshares').
card_rarity('swords to plowshares'/'BRB', 'Uncommon').
card_artist('swords to plowshares'/'BRB', 'Jeff A. Menges').
card_multiverse_id('swords to plowshares'/'BRB', '21172').

card_in_set('symbiosis', 'BRB').
card_original_type('symbiosis'/'BRB', 'Instant').
card_original_text('symbiosis'/'BRB', 'Two target creatures each get +2/+2 until end of turn.').
card_image_name('symbiosis'/'BRB', 'symbiosis').
card_uid('symbiosis'/'BRB', 'BRB:Symbiosis:symbiosis').
card_rarity('symbiosis'/'BRB', 'Common').
card_artist('symbiosis'/'BRB', 'Jeff Miracola').
card_flavor_text('symbiosis'/'BRB', 'Although the elves of Argoth always considered them a nuisance, the pixies made fine allies during the war against the machines.').
card_multiverse_id('symbiosis'/'BRB', '21101').

card_in_set('syphon soul', 'BRB').
card_original_type('syphon soul'/'BRB', 'Sorcery').
card_original_text('syphon soul'/'BRB', 'Syphon Soul deals 2 damage to each other player. You gain life equal to the damage dealt this way.').
card_image_name('syphon soul'/'BRB', 'syphon soul').
card_uid('syphon soul'/'BRB', 'BRB:Syphon Soul:syphon soul').
card_rarity('syphon soul'/'BRB', 'Common').
card_artist('syphon soul'/'BRB', 'Melissa A. Benson').
card_multiverse_id('syphon soul'/'BRB', '21152').

card_in_set('terror', 'BRB').
card_original_type('terror'/'BRB', 'Instant').
card_original_text('terror'/'BRB', 'Destroy target nonartifact, nonblack creature. It can\'t be regenerated.').
card_image_name('terror'/'BRB', 'terror').
card_uid('terror'/'BRB', 'BRB:Terror:terror').
card_rarity('terror'/'BRB', 'Common').
card_artist('terror'/'BRB', 'Ron Spencer').
card_multiverse_id('terror'/'BRB', '21151').

card_in_set('thalakos lowlands', 'BRB').
card_original_type('thalakos lowlands'/'BRB', 'Land').
card_original_text('thalakos lowlands'/'BRB', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {W} or {U} to your mana pool. Thalakos Lowlands doesn\'t untap during your next untap phase.').
card_image_name('thalakos lowlands'/'BRB', 'thalakos lowlands').
card_uid('thalakos lowlands'/'BRB', 'BRB:Thalakos Lowlands:thalakos lowlands').
card_rarity('thalakos lowlands'/'BRB', 'Uncommon').
card_artist('thalakos lowlands'/'BRB', 'Jeff A. Menges').
card_multiverse_id('thalakos lowlands'/'BRB', '21146').

card_in_set('tranquility', 'BRB').
card_original_type('tranquility'/'BRB', 'Sorcery').
card_original_text('tranquility'/'BRB', 'Destroy all enchantments.').
card_image_name('tranquility'/'BRB', 'tranquility').
card_uid('tranquility'/'BRB', 'BRB:Tranquility:tranquility').
card_rarity('tranquility'/'BRB', 'Common').
card_artist('tranquility'/'BRB', 'Douglas Shuler').
card_multiverse_id('tranquility'/'BRB', '21100').

card_in_set('trumpeting armodon', 'BRB').
card_original_type('trumpeting armodon'/'BRB', 'Creature — Elephant').
card_original_text('trumpeting armodon'/'BRB', '{1}{G} Target creature blocks Trumpeting Armodon this turn if able.').
card_image_name('trumpeting armodon'/'BRB', 'trumpeting armodon').
card_uid('trumpeting armodon'/'BRB', 'BRB:Trumpeting Armodon:trumpeting armodon').
card_rarity('trumpeting armodon'/'BRB', 'Uncommon').
card_artist('trumpeting armodon'/'BRB', 'Gary Leach').
card_flavor_text('trumpeting armodon'/'BRB', 'These are its last days. But it will not go quietly.').
card_multiverse_id('trumpeting armodon'/'BRB', '21114').

card_in_set('unnerve', 'BRB').
card_original_type('unnerve'/'BRB', 'Sorcery').
card_original_text('unnerve'/'BRB', 'Each opponent discards two cards from his or her hand.').
card_image_name('unnerve'/'BRB', 'unnerve').
card_uid('unnerve'/'BRB', 'BRB:Unnerve:unnerve').
card_rarity('unnerve'/'BRB', 'Common').
card_artist('unnerve'/'BRB', 'Terese Nielsen').
card_flavor_text('unnerve'/'BRB', '\"If fear is the only tool you have left, then you\'ll never control me.\"\n—Xantcha, to Gix').
card_multiverse_id('unnerve'/'BRB', '21156').

card_in_set('uthden troll', 'BRB').
card_original_type('uthden troll'/'BRB', 'Creature — Troll').
card_original_text('uthden troll'/'BRB', '{R} Regenerate Uthden Troll.').
card_image_name('uthden troll'/'BRB', 'uthden troll').
card_uid('uthden troll'/'BRB', 'BRB:Uthden Troll:uthden troll').
card_rarity('uthden troll'/'BRB', 'Uncommon').
card_artist('uthden troll'/'BRB', 'Douglas Shuler').
card_flavor_text('uthden troll'/'BRB', '\"Oi oi oi, me gotta hurt in \'ere,\nOi oi oi, me smell a ting is near,\nGonna bosh \'n gonna nosh\n\'n da hurt\'ll disappear.\"\n—Troll chant').
card_multiverse_id('uthden troll'/'BRB', '21112').

card_in_set('vec townships', 'BRB').
card_original_type('vec townships'/'BRB', 'Land').
card_original_text('vec townships'/'BRB', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {G} or {W} to your mana pool. Vec Townships doesn\'t untap during your next untap phase.').
card_image_name('vec townships'/'BRB', 'vec townships').
card_uid('vec townships'/'BRB', 'BRB:Vec Townships:vec townships').
card_rarity('vec townships'/'BRB', 'Uncommon').
card_artist('vec townships'/'BRB', 'Eric David Anderson').
card_multiverse_id('vec townships'/'BRB', '21178').

card_in_set('village elder', 'BRB').
card_original_type('village elder'/'BRB', 'Creature — Druid').
card_original_text('village elder'/'BRB', '{G}, {T}, Sacrifice a forest: Regenerate target creature.').
card_image_name('village elder'/'BRB', 'village elder').
card_uid('village elder'/'BRB', 'BRB:Village Elder:village elder').
card_rarity('village elder'/'BRB', 'Common').
card_artist('village elder'/'BRB', 'Donato Giancola').
card_multiverse_id('village elder'/'BRB', '21098').

card_in_set('wall of heat', 'BRB').
card_original_type('wall of heat'/'BRB', 'Creature — Wall').
card_original_text('wall of heat'/'BRB', '(Walls can\'t attack.)').
card_image_name('wall of heat'/'BRB', 'wall of heat').
card_uid('wall of heat'/'BRB', 'BRB:Wall of Heat:wall of heat').
card_rarity('wall of heat'/'BRB', 'Common').
card_artist('wall of heat'/'BRB', 'Richard Thomas').
card_flavor_text('wall of heat'/'BRB', 'At a distance, we mistook the sound for a waterfall . . . .').
card_multiverse_id('wall of heat'/'BRB', '21147').

card_in_set('weakness', 'BRB').
card_original_type('weakness'/'BRB', 'Enchant Creature').
card_original_text('weakness'/'BRB', 'Enchanted creature gets -2/-1.').
card_image_name('weakness'/'BRB', 'weakness').
card_uid('weakness'/'BRB', 'BRB:Weakness:weakness').
card_rarity('weakness'/'BRB', 'Common').
card_artist('weakness'/'BRB', 'Kev Walker').
card_multiverse_id('weakness'/'BRB', '21157').

card_in_set('wildfire emissary', 'BRB').
card_original_type('wildfire emissary'/'BRB', 'Creature — Efreet').
card_original_text('wildfire emissary'/'BRB', 'Protection from white \n{1}{R} Wildfire Emissary gets +1/+0 until end of turn.').
card_image_name('wildfire emissary'/'BRB', 'wildfire emissary').
card_uid('wildfire emissary'/'BRB', 'BRB:Wildfire Emissary:wildfire emissary').
card_rarity('wildfire emissary'/'BRB', 'Uncommon').
card_artist('wildfire emissary'/'BRB', 'Richard Kane Ferguson').
card_flavor_text('wildfire emissary'/'BRB', '\"The efreet is a striding storm with a voice that crackles like fire.\"\n—Qhattib, vizier of Amiqat').
card_multiverse_id('wildfire emissary'/'BRB', '21115').

card_in_set('wind drake', 'BRB').
card_original_type('wind drake'/'BRB', 'Creature — Drake').
card_original_text('wind drake'/'BRB', 'Flying').
card_image_name('wind drake'/'BRB', 'wind drake').
card_uid('wind drake'/'BRB', 'BRB:Wind Drake:wind drake').
card_rarity('wind drake'/'BRB', 'Common').
card_artist('wind drake'/'BRB', 'Zina Saunders').
card_flavor_text('wind drake'/'BRB', '\"No bird soars too high, if he soars with his own wings.\"\n—William Blake,\nThe Marriage of Heaven and Hell').
card_multiverse_id('wind drake'/'BRB', '21124').

card_in_set('windfall', 'BRB').
card_original_type('windfall'/'BRB', 'Sorcery').
card_original_text('windfall'/'BRB', 'Each player discards his or her hand and draws cards equal to the greatest number a player discarded this way.').
card_image_name('windfall'/'BRB', 'windfall').
card_uid('windfall'/'BRB', 'BRB:Windfall:windfall').
card_rarity('windfall'/'BRB', 'Uncommon').
card_artist('windfall'/'BRB', 'Pete Venters').
card_flavor_text('windfall'/'BRB', '\"To fill your mind with knowledge, we must start by emptying it.\"\n—Barrin, master wizard').
card_multiverse_id('windfall'/'BRB', '21135').

card_in_set('wrath of god', 'BRB').
card_original_type('wrath of god'/'BRB', 'Sorcery').
card_original_text('wrath of god'/'BRB', 'Destroy all creatures. They can\'t be regenerated.').
card_image_name('wrath of god'/'BRB', 'wrath of god').
card_uid('wrath of god'/'BRB', 'BRB:Wrath of God:wrath of god').
card_rarity('wrath of god'/'BRB', 'Rare').
card_artist('wrath of god'/'BRB', 'Quinton Hoover').
card_multiverse_id('wrath of god'/'BRB', '21180').
