% Born of the Gods

set('BNG').
set_name('BNG', 'Born of the Gods').
set_release_date('BNG', '2014-02-07').
set_border('BNG', 'black').
set_type('BNG', 'expansion').
set_block('BNG', 'Theros').

card_in_set('acolyte\'s reward', 'BNG').
card_original_type('acolyte\'s reward'/'BNG', 'Instant').
card_original_text('acolyte\'s reward'/'BNG', 'Prevent the next X damage that would be dealt to target creature this turn, where X is your devotion to white. If damage is prevented this way, Acolyte\'s Reward deals that much damage to target creature or player. (Each {W} in the mana costs of permanents you control counts toward your devotion to white.)').
card_first_print('acolyte\'s reward', 'BNG').
card_image_name('acolyte\'s reward'/'BNG', 'acolyte\'s reward').
card_uid('acolyte\'s reward'/'BNG', 'BNG:Acolyte\'s Reward:acolyte\'s reward').
card_rarity('acolyte\'s reward'/'BNG', 'Uncommon').
card_artist('acolyte\'s reward'/'BNG', 'Slawomir Maniak').
card_number('acolyte\'s reward'/'BNG', '1').
card_multiverse_id('acolyte\'s reward'/'BNG', '378373').

card_in_set('aerie worshippers', 'BNG').
card_original_type('aerie worshippers'/'BNG', 'Creature — Human Cleric').
card_original_text('aerie worshippers'/'BNG', 'Inspired — Whenever Aerie Worshippers becomes untapped, you may pay {2}{U}. If you do, put a 2/2 blue Bird enchantment creature token with flying onto the battlefield.').
card_first_print('aerie worshippers', 'BNG').
card_image_name('aerie worshippers'/'BNG', 'aerie worshippers').
card_uid('aerie worshippers'/'BNG', 'BNG:Aerie Worshippers:aerie worshippers').
card_rarity('aerie worshippers'/'BNG', 'Uncommon').
card_artist('aerie worshippers'/'BNG', 'Mike Sass').
card_number('aerie worshippers'/'BNG', '30').
card_flavor_text('aerie worshippers'/'BNG', 'They can conjure stars from a clear sky.').
card_multiverse_id('aerie worshippers'/'BNG', '378402').

card_in_set('akroan conscriptor', 'BNG').
card_original_type('akroan conscriptor'/'BNG', 'Creature — Human Shaman').
card_original_text('akroan conscriptor'/'BNG', 'Heroic — Whenever you cast a spell that targets Akroan Conscriptor, gain control of another target creature until end of turn. Untap that creature. It gains haste until end of turn.').
card_first_print('akroan conscriptor', 'BNG').
card_image_name('akroan conscriptor'/'BNG', 'akroan conscriptor').
card_uid('akroan conscriptor'/'BNG', 'BNG:Akroan Conscriptor:akroan conscriptor').
card_rarity('akroan conscriptor'/'BNG', 'Uncommon').
card_artist('akroan conscriptor'/'BNG', 'James Ryman').
card_number('akroan conscriptor'/'BNG', '87').
card_flavor_text('akroan conscriptor'/'BNG', '\"The time to serve is now.\"').
card_multiverse_id('akroan conscriptor'/'BNG', '378459').

card_in_set('akroan phalanx', 'BNG').
card_original_type('akroan phalanx'/'BNG', 'Creature — Human Soldier').
card_original_text('akroan phalanx'/'BNG', 'Vigilance\n{2}{R}: Creatures you control get +1/+0 until end of turn.').
card_first_print('akroan phalanx', 'BNG').
card_image_name('akroan phalanx'/'BNG', 'akroan phalanx').
card_uid('akroan phalanx'/'BNG', 'BNG:Akroan Phalanx:akroan phalanx').
card_rarity('akroan phalanx'/'BNG', 'Uncommon').
card_artist('akroan phalanx'/'BNG', 'Steve Prescott').
card_number('akroan phalanx'/'BNG', '2').
card_flavor_text('akroan phalanx'/'BNG', 'Shields up, spears out, heels set, hearts firm.').
card_multiverse_id('akroan phalanx'/'BNG', '378374').

card_in_set('akroan skyguard', 'BNG').
card_original_type('akroan skyguard'/'BNG', 'Creature — Human Soldier').
card_original_text('akroan skyguard'/'BNG', 'Flying\nHeroic — Whenever you cast a spell that targets Akroan Skyguard, put a +1/+1 counter on Akroan Skyguard.').
card_first_print('akroan skyguard', 'BNG').
card_image_name('akroan skyguard'/'BNG', 'akroan skyguard').
card_uid('akroan skyguard'/'BNG', 'BNG:Akroan Skyguard:akroan skyguard').
card_rarity('akroan skyguard'/'BNG', 'Common').
card_artist('akroan skyguard'/'BNG', 'Mark Winters').
card_number('akroan skyguard'/'BNG', '3').
card_flavor_text('akroan skyguard'/'BNG', '\"Trust me. When you have earned a god\'s favor, you\'ll know.\"').
card_multiverse_id('akroan skyguard'/'BNG', '378375').

card_in_set('arbiter of the ideal', 'BNG').
card_original_type('arbiter of the ideal'/'BNG', 'Creature — Sphinx').
card_original_text('arbiter of the ideal'/'BNG', 'Flying\nInspired — Whenever Arbiter of the Ideal becomes untapped, reveal the top card of your library. If it\'s an artifact, creature, or land card, you may put it onto the battlefield with a manifestation counter on it. That permanent is an enchantment in addition to its other types.').
card_image_name('arbiter of the ideal'/'BNG', 'arbiter of the ideal').
card_uid('arbiter of the ideal'/'BNG', 'BNG:Arbiter of the Ideal:arbiter of the ideal').
card_rarity('arbiter of the ideal'/'BNG', 'Rare').
card_artist('arbiter of the ideal'/'BNG', 'Svetlin Velinov').
card_number('arbiter of the ideal'/'BNG', '31').
card_multiverse_id('arbiter of the ideal'/'BNG', '378403').

card_in_set('archetype of aggression', 'BNG').
card_original_type('archetype of aggression'/'BNG', 'Enchantment Creature — Human Warrior').
card_original_text('archetype of aggression'/'BNG', 'Creatures you control have trample.\nCreatures your opponents control lose trample and can\'t have or gain trample.').
card_first_print('archetype of aggression', 'BNG').
card_image_name('archetype of aggression'/'BNG', 'archetype of aggression').
card_uid('archetype of aggression'/'BNG', 'BNG:Archetype of Aggression:archetype of aggression').
card_rarity('archetype of aggression'/'BNG', 'Uncommon').
card_artist('archetype of aggression'/'BNG', 'Mathias Kollros').
card_number('archetype of aggression'/'BNG', '88').
card_flavor_text('archetype of aggression'/'BNG', '\"Did Purphoros bless Maikal because of his rage? Or did Maikal\'s rage blossom after he\'d been blessed? Only the gods know.\"\n—Eocles, oracle of Purphoros').
card_multiverse_id('archetype of aggression'/'BNG', '378460').

card_in_set('archetype of courage', 'BNG').
card_original_type('archetype of courage'/'BNG', 'Enchantment Creature — Human Soldier').
card_original_text('archetype of courage'/'BNG', 'Creatures you control have first strike.\nCreatures your opponents control lose first strike and can\'t have or gain first strike.').
card_first_print('archetype of courage', 'BNG').
card_image_name('archetype of courage'/'BNG', 'archetype of courage').
card_uid('archetype of courage'/'BNG', 'BNG:Archetype of Courage:archetype of courage').
card_rarity('archetype of courage'/'BNG', 'Uncommon').
card_artist('archetype of courage'/'BNG', 'Willian Murai').
card_number('archetype of courage'/'BNG', '4').
card_flavor_text('archetype of courage'/'BNG', '\"It has been my experience that soldiers most fervently follow generals who lead by example.\"\n—Elspeth').
card_multiverse_id('archetype of courage'/'BNG', '378376').

card_in_set('archetype of endurance', 'BNG').
card_original_type('archetype of endurance'/'BNG', 'Enchantment Creature — Boar').
card_original_text('archetype of endurance'/'BNG', 'Creatures you control have hexproof.\nCreatures your opponents control lose hexproof and can\'t have or gain hexproof.').
card_first_print('archetype of endurance', 'BNG').
card_image_name('archetype of endurance'/'BNG', 'archetype of endurance').
card_uid('archetype of endurance'/'BNG', 'BNG:Archetype of Endurance:archetype of endurance').
card_rarity('archetype of endurance'/'BNG', 'Uncommon').
card_artist('archetype of endurance'/'BNG', 'Slawomir Maniak').
card_number('archetype of endurance'/'BNG', '116').
card_flavor_text('archetype of endurance'/'BNG', 'Despite its fearsome stature, it is as elusive as a shadow, circling round to stalk those who presume to hunt it.').
card_multiverse_id('archetype of endurance'/'BNG', '378488').

card_in_set('archetype of finality', 'BNG').
card_original_type('archetype of finality'/'BNG', 'Enchantment Creature — Gorgon').
card_original_text('archetype of finality'/'BNG', 'Creatures you control have deathtouch.\nCreatures your opponents control lose deathtouch and can\'t have or gain deathtouch.').
card_first_print('archetype of finality', 'BNG').
card_image_name('archetype of finality'/'BNG', 'archetype of finality').
card_uid('archetype of finality'/'BNG', 'BNG:Archetype of Finality:archetype of finality').
card_rarity('archetype of finality'/'BNG', 'Uncommon').
card_artist('archetype of finality'/'BNG', 'Chris Rahn').
card_number('archetype of finality'/'BNG', '58').
card_flavor_text('archetype of finality'/'BNG', 'She sees mortals not as they wish to be, but as what they will become.').
card_multiverse_id('archetype of finality'/'BNG', '378430').

card_in_set('archetype of imagination', 'BNG').
card_original_type('archetype of imagination'/'BNG', 'Enchantment Creature — Human Wizard').
card_original_text('archetype of imagination'/'BNG', 'Creatures you control have flying.\nCreatures your opponents control lose flying and can\'t have or gain flying.').
card_first_print('archetype of imagination', 'BNG').
card_image_name('archetype of imagination'/'BNG', 'archetype of imagination').
card_uid('archetype of imagination'/'BNG', 'BNG:Archetype of Imagination:archetype of imagination').
card_rarity('archetype of imagination'/'BNG', 'Uncommon').
card_artist('archetype of imagination'/'BNG', 'Robbie Trevino').
card_number('archetype of imagination'/'BNG', '32').
card_flavor_text('archetype of imagination'/'BNG', '\"Is it not the embodiment of our aspirations?\"\n—Prokopios, astronomer of Meletis').
card_multiverse_id('archetype of imagination'/'BNG', '378404').

card_in_set('ashiok\'s adept', 'BNG').
card_original_type('ashiok\'s adept'/'BNG', 'Creature — Human Wizard').
card_original_text('ashiok\'s adept'/'BNG', 'Heroic — Whenever you cast a spell that targets Ashiok\'s Adept, each opponent discards a card.').
card_first_print('ashiok\'s adept', 'BNG').
card_image_name('ashiok\'s adept'/'BNG', 'ashiok\'s adept').
card_uid('ashiok\'s adept'/'BNG', 'BNG:Ashiok\'s Adept:ashiok\'s adept').
card_rarity('ashiok\'s adept'/'BNG', 'Uncommon').
card_artist('ashiok\'s adept'/'BNG', 'Karla Ortiz').
card_number('ashiok\'s adept'/'BNG', '59').
card_flavor_text('ashiok\'s adept'/'BNG', '\"Every nightmare is a caged bird that yearns to be set free.\"\n—Ashiok, Nightmare Weaver').
card_multiverse_id('ashiok\'s adept'/'BNG', '378431').

card_in_set('aspect of hydra', 'BNG').
card_original_type('aspect of hydra'/'BNG', 'Instant').
card_original_text('aspect of hydra'/'BNG', 'Target creature gets +X/+X until end of turn, where X is your devotion to green. (Each {G} in the mana costs of permanents you control counts toward your devotion to green.)').
card_first_print('aspect of hydra', 'BNG').
card_image_name('aspect of hydra'/'BNG', 'aspect of hydra').
card_uid('aspect of hydra'/'BNG', 'BNG:Aspect of Hydra:aspect of hydra').
card_rarity('aspect of hydra'/'BNG', 'Common').
card_artist('aspect of hydra'/'BNG', 'Mark Winters').
card_number('aspect of hydra'/'BNG', '117').
card_flavor_text('aspect of hydra'/'BNG', '\"Even alone I outnumber you.\"').
card_multiverse_id('aspect of hydra'/'BNG', '378489').

card_in_set('asphyxiate', 'BNG').
card_original_type('asphyxiate'/'BNG', 'Sorcery').
card_original_text('asphyxiate'/'BNG', 'Destroy target untapped creature.').
card_first_print('asphyxiate', 'BNG').
card_image_name('asphyxiate'/'BNG', 'asphyxiate').
card_uid('asphyxiate'/'BNG', 'BNG:Asphyxiate:asphyxiate').
card_rarity('asphyxiate'/'BNG', 'Common').
card_artist('asphyxiate'/'BNG', 'Kev Walker').
card_number('asphyxiate'/'BNG', '60').
card_flavor_text('asphyxiate'/'BNG', '\"Some poisons enter through the blood. Some are ingested. Some are inhaled. All ways through which mortals draw strength are paths for poison.\"\n—Agathe, priest of Pharika').
card_multiverse_id('asphyxiate'/'BNG', '378432').

card_in_set('astral cornucopia', 'BNG').
card_original_type('astral cornucopia'/'BNG', 'Artifact').
card_original_text('astral cornucopia'/'BNG', 'Astral Cornucopia enters the battlefield with X charge counters on it.\n{T}: Choose a color. Add one mana of that color to your mana pool for each charge counter on Astral Cornucopia.').
card_first_print('astral cornucopia', 'BNG').
card_image_name('astral cornucopia'/'BNG', 'astral cornucopia').
card_uid('astral cornucopia'/'BNG', 'BNG:Astral Cornucopia:astral cornucopia').
card_rarity('astral cornucopia'/'BNG', 'Rare').
card_artist('astral cornucopia'/'BNG', 'Aleksi Briclot').
card_number('astral cornucopia'/'BNG', '157').
card_multiverse_id('astral cornucopia'/'BNG', '378529').

card_in_set('bile blight', 'BNG').
card_original_type('bile blight'/'BNG', 'Instant').
card_original_text('bile blight'/'BNG', 'Target creature and all other creatures with the same name as that creature get -3/-3 until end of turn.').
card_image_name('bile blight'/'BNG', 'bile blight').
card_uid('bile blight'/'BNG', 'BNG:Bile Blight:bile blight').
card_rarity('bile blight'/'BNG', 'Uncommon').
card_artist('bile blight'/'BNG', 'Vincent Proce').
card_number('bile blight'/'BNG', '61').
card_flavor_text('bile blight'/'BNG', 'Not an arrow loosed, javelin thrown, nor sword raised. None were needed.').
card_multiverse_id('bile blight'/'BNG', '378433').

card_in_set('black oak of odunos', 'BNG').
card_original_type('black oak of odunos'/'BNG', 'Creature — Zombie Treefolk').
card_original_text('black oak of odunos'/'BNG', 'Defender\n{B}, Tap another untapped creature you control: Black Oak of Odunos gets +1/+1 until end of turn.').
card_first_print('black oak of odunos', 'BNG').
card_image_name('black oak of odunos'/'BNG', 'black oak of odunos').
card_uid('black oak of odunos'/'BNG', 'BNG:Black Oak of Odunos:black oak of odunos').
card_rarity('black oak of odunos'/'BNG', 'Uncommon').
card_artist('black oak of odunos'/'BNG', 'Chris Rahn').
card_number('black oak of odunos'/'BNG', '62').
card_flavor_text('black oak of odunos'/'BNG', 'Phenax promised the newly dead souls they would be spared from Erebos. In this, he did not lie.').
card_multiverse_id('black oak of odunos'/'BNG', '378434').

card_in_set('bolt of keranos', 'BNG').
card_original_type('bolt of keranos'/'BNG', 'Sorcery').
card_original_text('bolt of keranos'/'BNG', 'Bolt of Keranos deals 3 damage to target creature or player. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_first_print('bolt of keranos', 'BNG').
card_image_name('bolt of keranos'/'BNG', 'bolt of keranos').
card_uid('bolt of keranos'/'BNG', 'BNG:Bolt of Keranos:bolt of keranos').
card_rarity('bolt of keranos'/'BNG', 'Common').
card_artist('bolt of keranos'/'BNG', 'Karl Kopinski').
card_number('bolt of keranos'/'BNG', '89').
card_flavor_text('bolt of keranos'/'BNG', 'The breakthroughs granted by Keranos, god of epiphany, can be surprisingly literal.').
card_multiverse_id('bolt of keranos'/'BNG', '378461').

card_in_set('brimaz, king of oreskos', 'BNG').
card_original_type('brimaz, king of oreskos'/'BNG', 'Legendary Creature — Cat Soldier').
card_original_text('brimaz, king of oreskos'/'BNG', 'Vigilance \nWhenever Brimaz, King of Oreskos attacks, put a 1/1 white Cat Soldier creature token with vigilance onto the battlefield attacking.\nWhenever Brimaz blocks a creature, put a 1/1 white Cat Soldier creature token with vigilance onto the battlefield blocking that creature.').
card_first_print('brimaz, king of oreskos', 'BNG').
card_image_name('brimaz, king of oreskos'/'BNG', 'brimaz, king of oreskos').
card_uid('brimaz, king of oreskos'/'BNG', 'BNG:Brimaz, King of Oreskos:brimaz, king of oreskos').
card_rarity('brimaz, king of oreskos'/'BNG', 'Mythic Rare').
card_artist('brimaz, king of oreskos'/'BNG', 'Peter Mohrbacher').
card_number('brimaz, king of oreskos'/'BNG', '5').
card_multiverse_id('brimaz, king of oreskos'/'BNG', '378377').

card_in_set('champion of stray souls', 'BNG').
card_original_type('champion of stray souls'/'BNG', 'Creature — Skeleton Warrior').
card_original_text('champion of stray souls'/'BNG', '{3}{B}{B}, {T}, Sacrifice X other creatures: Return X target creature cards from your graveyard to the battlefield.\n{5}{B}{B}: Put Champion of Stray Souls on top of your library from your graveyard.').
card_first_print('champion of stray souls', 'BNG').
card_image_name('champion of stray souls'/'BNG', 'champion of stray souls').
card_uid('champion of stray souls'/'BNG', 'BNG:Champion of Stray Souls:champion of stray souls').
card_rarity('champion of stray souls'/'BNG', 'Mythic Rare').
card_artist('champion of stray souls'/'BNG', 'Aleksi Briclot').
card_number('champion of stray souls'/'BNG', '63').
card_multiverse_id('champion of stray souls'/'BNG', '378435').

card_in_set('charging badger', 'BNG').
card_original_type('charging badger'/'BNG', 'Creature — Badger').
card_original_text('charging badger'/'BNG', 'Trample').
card_first_print('charging badger', 'BNG').
card_image_name('charging badger'/'BNG', 'charging badger').
card_uid('charging badger'/'BNG', 'BNG:Charging Badger:charging badger').
card_rarity('charging badger'/'BNG', 'Common').
card_artist('charging badger'/'BNG', 'Raoul Vitale').
card_number('charging badger'/'BNG', '118').
card_flavor_text('charging badger'/'BNG', '\"If the hierarchies of nature were determined by ferocity alone, the badger would be lord of the beasts.\"\n—Anthousa of Setessa').
card_multiverse_id('charging badger'/'BNG', '378490').

card_in_set('chorus of the tides', 'BNG').
card_original_type('chorus of the tides'/'BNG', 'Creature — Siren').
card_original_text('chorus of the tides'/'BNG', 'Flying\nHeroic — Whenever you cast a spell that targets Chorus of the Tides, scry 1. (To scry 1, look at the top card of your library, then you may put that card on the bottom of your library.)').
card_first_print('chorus of the tides', 'BNG').
card_image_name('chorus of the tides'/'BNG', 'chorus of the tides').
card_uid('chorus of the tides'/'BNG', 'BNG:Chorus of the Tides:chorus of the tides').
card_rarity('chorus of the tides'/'BNG', 'Common').
card_artist('chorus of the tides'/'BNG', 'Steve Prescott').
card_number('chorus of the tides'/'BNG', '33').
card_multiverse_id('chorus of the tides'/'BNG', '378405').

card_in_set('chromanticore', 'BNG').
card_original_type('chromanticore'/'BNG', 'Enchantment Creature — Manticore').
card_original_text('chromanticore'/'BNG', 'Bestow {2}{W}{U}{B}{R}{G} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nFlying, first strike, vigilance, trample, lifelink\nEnchanted creature gets +4/+4 and has flying, first strike, vigilance, trample, and lifelink.').
card_first_print('chromanticore', 'BNG').
card_image_name('chromanticore'/'BNG', 'chromanticore').
card_uid('chromanticore'/'BNG', 'BNG:Chromanticore:chromanticore').
card_rarity('chromanticore'/'BNG', 'Mythic Rare').
card_artist('chromanticore'/'BNG', 'Min Yum').
card_number('chromanticore'/'BNG', '144').
card_multiverse_id('chromanticore'/'BNG', '378516').

card_in_set('claim of erebos', 'BNG').
card_original_type('claim of erebos'/'BNG', 'Enchantment — Aura').
card_original_text('claim of erebos'/'BNG', 'Enchant creature\nEnchanted creature has \"{1}{B}, {T}: Target player loses 2 life.\"').
card_first_print('claim of erebos', 'BNG').
card_image_name('claim of erebos'/'BNG', 'claim of erebos').
card_uid('claim of erebos'/'BNG', 'BNG:Claim of Erebos:claim of erebos').
card_rarity('claim of erebos'/'BNG', 'Common').
card_artist('claim of erebos'/'BNG', 'Zack Stella').
card_number('claim of erebos'/'BNG', '64').
card_flavor_text('claim of erebos'/'BNG', 'Priests of Erebos give an occasional reminder to mortals who have forgotten their mortality.').
card_multiverse_id('claim of erebos'/'BNG', '378436').

card_in_set('courser of kruphix', 'BNG').
card_original_type('courser of kruphix'/'BNG', 'Enchantment Creature — Centaur').
card_original_text('courser of kruphix'/'BNG', 'Play with the top card of your library revealed.\nYou may play the top card of your library if it\'s a land card.\nWhenever a land enters the battlefield under your control, you gain 1 life.').
card_first_print('courser of kruphix', 'BNG').
card_image_name('courser of kruphix'/'BNG', 'courser of kruphix').
card_uid('courser of kruphix'/'BNG', 'BNG:Courser of Kruphix:courser of kruphix').
card_rarity('courser of kruphix'/'BNG', 'Rare').
card_artist('courser of kruphix'/'BNG', 'Eric Deschamps').
card_number('courser of kruphix'/'BNG', '119').
card_multiverse_id('courser of kruphix'/'BNG', '378491').

card_in_set('crypsis', 'BNG').
card_original_type('crypsis'/'BNG', 'Instant').
card_original_text('crypsis'/'BNG', 'Target creature you control gains protection from creatures your opponents control until end of turn. Untap it.').
card_first_print('crypsis', 'BNG').
card_image_name('crypsis'/'BNG', 'crypsis').
card_uid('crypsis'/'BNG', 'BNG:Crypsis:crypsis').
card_rarity('crypsis'/'BNG', 'Common').
card_artist('crypsis'/'BNG', 'Daarken').
card_number('crypsis'/'BNG', '34').
card_flavor_text('crypsis'/'BNG', 'Tritons borrow the camouflage of their oceanic prey to become predators on land.').
card_multiverse_id('crypsis'/'BNG', '378406').

card_in_set('culling mark', 'BNG').
card_original_type('culling mark'/'BNG', 'Sorcery').
card_original_text('culling mark'/'BNG', 'Target creature blocks this turn if able.').
card_first_print('culling mark', 'BNG').
card_image_name('culling mark'/'BNG', 'culling mark').
card_uid('culling mark'/'BNG', 'BNG:Culling Mark:culling mark').
card_rarity('culling mark'/'BNG', 'Common').
card_artist('culling mark'/'BNG', 'Tomasz Jedruszek').
card_number('culling mark'/'BNG', '120').
card_flavor_text('culling mark'/'BNG', 'Hunt without Nylea\'s leave and you may find yourself the next quarry.').
card_multiverse_id('culling mark'/'BNG', '378492').

card_in_set('cyclops of one-eyed pass', 'BNG').
card_original_type('cyclops of one-eyed pass'/'BNG', 'Creature — Cyclops').
card_original_text('cyclops of one-eyed pass'/'BNG', '').
card_first_print('cyclops of one-eyed pass', 'BNG').
card_image_name('cyclops of one-eyed pass'/'BNG', 'cyclops of one-eyed pass').
card_uid('cyclops of one-eyed pass'/'BNG', 'BNG:Cyclops of One-Eyed Pass:cyclops of one-eyed pass').
card_rarity('cyclops of one-eyed pass'/'BNG', 'Common').
card_artist('cyclops of one-eyed pass'/'BNG', 'Kev Walker').
card_number('cyclops of one-eyed pass'/'BNG', '90').
card_flavor_text('cyclops of one-eyed pass'/'BNG', 'The Champion armed herself to face the cyclops, heedless of her companions\' despair.\n\"How will you defeat it with only one spear?\" asked young Althemone.\nThe Champion raised her weapon. \"It has but one eye.\"\n—The Theriad').
card_multiverse_id('cyclops of one-eyed pass'/'BNG', '378462').

card_in_set('dawn to dusk', 'BNG').
card_original_type('dawn to dusk'/'BNG', 'Sorcery').
card_original_text('dawn to dusk'/'BNG', 'Choose one or both — Return target enchantment card from your graveyard to your hand; and/or destroy target enchantment.').
card_first_print('dawn to dusk', 'BNG').
card_image_name('dawn to dusk'/'BNG', 'dawn to dusk').
card_uid('dawn to dusk'/'BNG', 'BNG:Dawn to Dusk:dawn to dusk').
card_rarity('dawn to dusk'/'BNG', 'Uncommon').
card_artist('dawn to dusk'/'BNG', 'Robbie Trevino').
card_number('dawn to dusk'/'BNG', '6').
card_flavor_text('dawn to dusk'/'BNG', '\"The numbers of Nyxborn among us have swelled. Is this a sign of the gods\' favor, or does it portend something else?\"').
card_multiverse_id('dawn to dusk'/'BNG', '378378').

card_in_set('deepwater hypnotist', 'BNG').
card_original_type('deepwater hypnotist'/'BNG', 'Creature — Merfolk Wizard').
card_original_text('deepwater hypnotist'/'BNG', 'Inspired — Whenever Deepwater Hypnotist becomes untapped, target creature an opponent controls gets -3/-0 until end of turn.').
card_first_print('deepwater hypnotist', 'BNG').
card_image_name('deepwater hypnotist'/'BNG', 'deepwater hypnotist').
card_uid('deepwater hypnotist'/'BNG', 'BNG:Deepwater Hypnotist:deepwater hypnotist').
card_rarity('deepwater hypnotist'/'BNG', 'Common').
card_artist('deepwater hypnotist'/'BNG', 'Christopher Moeller').
card_number('deepwater hypnotist'/'BNG', '35').
card_flavor_text('deepwater hypnotist'/'BNG', '\"Watch the waves too long, and you may never look away.\"\n—Meletian proverb').
card_multiverse_id('deepwater hypnotist'/'BNG', '378407').

card_in_set('divination', 'BNG').
card_original_type('divination'/'BNG', 'Sorcery').
card_original_text('divination'/'BNG', 'Draw two cards.').
card_image_name('divination'/'BNG', 'divination').
card_uid('divination'/'BNG', 'BNG:Divination:divination').
card_rarity('divination'/'BNG', 'Common').
card_artist('divination'/'BNG', 'Willian Murai').
card_number('divination'/'BNG', '36').
card_flavor_text('divination'/'BNG', '\"Ask of Keranos that you may understand the stars that mark your destiny. Implore Erebos that you may accept the fate thus divulged.\"').
card_multiverse_id('divination'/'BNG', '378408').

card_in_set('drown in sorrow', 'BNG').
card_original_type('drown in sorrow'/'BNG', 'Sorcery').
card_original_text('drown in sorrow'/'BNG', 'All creatures get -2/-2 until end of turn. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_first_print('drown in sorrow', 'BNG').
card_image_name('drown in sorrow'/'BNG', 'drown in sorrow').
card_uid('drown in sorrow'/'BNG', 'BNG:Drown in Sorrow:drown in sorrow').
card_rarity('drown in sorrow'/'BNG', 'Uncommon').
card_artist('drown in sorrow'/'BNG', 'Noah Bradley').
card_number('drown in sorrow'/'BNG', '65').
card_flavor_text('drown in sorrow'/'BNG', '\"The sad truth is that the whip of Erebos is long enough to wrap around all our throats.\"\n—Perisophia the philosopher').
card_multiverse_id('drown in sorrow'/'BNG', '378437').

card_in_set('eater of hope', 'BNG').
card_original_type('eater of hope'/'BNG', 'Creature — Demon').
card_original_text('eater of hope'/'BNG', 'Flying\n{B}, Sacrifice another creature: Regenerate Eater of Hope.\n{2}{B}, Sacrifice two other creatures: Destroy target creature.').
card_image_name('eater of hope'/'BNG', 'eater of hope').
card_uid('eater of hope'/'BNG', 'BNG:Eater of Hope:eater of hope').
card_rarity('eater of hope'/'BNG', 'Rare').
card_artist('eater of hope'/'BNG', 'Peter Mohrbacher').
card_number('eater of hope'/'BNG', '66').
card_flavor_text('eater of hope'/'BNG', 'Gods can be appeased. Demons, however . . .').
card_multiverse_id('eater of hope'/'BNG', '378438').

card_in_set('eidolon of countless battles', 'BNG').
card_original_type('eidolon of countless battles'/'BNG', 'Enchantment Creature — Spirit').
card_original_text('eidolon of countless battles'/'BNG', 'Bestow {2}{W}{W} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nEidolon of Countless Battles and enchanted creature each get +1/+1 for each creature you control and +1/+1 for each Aura you control.').
card_first_print('eidolon of countless battles', 'BNG').
card_image_name('eidolon of countless battles'/'BNG', 'eidolon of countless battles').
card_uid('eidolon of countless battles'/'BNG', 'BNG:Eidolon of Countless Battles:eidolon of countless battles').
card_rarity('eidolon of countless battles'/'BNG', 'Rare').
card_artist('eidolon of countless battles'/'BNG', 'Raymond Swanland').
card_number('eidolon of countless battles'/'BNG', '7').
card_multiverse_id('eidolon of countless battles'/'BNG', '378379').

card_in_set('elite skirmisher', 'BNG').
card_original_type('elite skirmisher'/'BNG', 'Creature — Human Soldier').
card_original_text('elite skirmisher'/'BNG', 'Heroic — Whenever you cast a spell that targets Elite Skirmisher, you may tap target creature.').
card_first_print('elite skirmisher', 'BNG').
card_image_name('elite skirmisher'/'BNG', 'elite skirmisher').
card_uid('elite skirmisher'/'BNG', 'BNG:Elite Skirmisher:elite skirmisher').
card_rarity('elite skirmisher'/'BNG', 'Common').
card_artist('elite skirmisher'/'BNG', 'Mark Winters').
card_number('elite skirmisher'/'BNG', '8').
card_flavor_text('elite skirmisher'/'BNG', 'Some adopted the tactics of the leonin to combat the ferocity of the minotaurs.').
card_multiverse_id('elite skirmisher'/'BNG', '378380').

card_in_set('ephara\'s enlightenment', 'BNG').
card_original_type('ephara\'s enlightenment'/'BNG', 'Enchantment — Aura').
card_original_text('ephara\'s enlightenment'/'BNG', 'Enchant creature\nWhen Ephara\'s Enlightenment enters the battlefield, put a +1/+1 counter on enchanted creature.\nEnchanted creature has flying.\nWhenever a creature enters the battlefield under your control, you may return Ephara\'s Enlightenment to its owner\'s hand.').
card_first_print('ephara\'s enlightenment', 'BNG').
card_image_name('ephara\'s enlightenment'/'BNG', 'ephara\'s enlightenment').
card_uid('ephara\'s enlightenment'/'BNG', 'BNG:Ephara\'s Enlightenment:ephara\'s enlightenment').
card_rarity('ephara\'s enlightenment'/'BNG', 'Uncommon').
card_artist('ephara\'s enlightenment'/'BNG', 'Wesley Burt').
card_number('ephara\'s enlightenment'/'BNG', '146').
card_multiverse_id('ephara\'s enlightenment'/'BNG', '378518').

card_in_set('ephara\'s radiance', 'BNG').
card_original_type('ephara\'s radiance'/'BNG', 'Enchantment — Aura').
card_original_text('ephara\'s radiance'/'BNG', 'Enchant creature\nEnchanted creature has \"{1}{W}, {T}: You gain 3 life.\"').
card_first_print('ephara\'s radiance', 'BNG').
card_image_name('ephara\'s radiance'/'BNG', 'ephara\'s radiance').
card_uid('ephara\'s radiance'/'BNG', 'BNG:Ephara\'s Radiance:ephara\'s radiance').
card_rarity('ephara\'s radiance'/'BNG', 'Common').
card_artist('ephara\'s radiance'/'BNG', 'James Ryman').
card_number('ephara\'s radiance'/'BNG', '9').
card_flavor_text('ephara\'s radiance'/'BNG', '\"Civilization is the sum of Ephara\'s gifts.\"\n—Olexa of the Twelve').
card_multiverse_id('ephara\'s radiance'/'BNG', '378381').

card_in_set('ephara, god of the polis', 'BNG').
card_original_type('ephara, god of the polis'/'BNG', 'Legendary Enchantment Creature — God').
card_original_text('ephara, god of the polis'/'BNG', 'Indestructible\nAs long as your devotion to white and blue is less than seven, Ephara isn\'t a creature.\nAt the beginning of each upkeep, if you had another creature enter the battlefield under your control last turn, draw a card.').
card_first_print('ephara, god of the polis', 'BNG').
card_image_name('ephara, god of the polis'/'BNG', 'ephara, god of the polis').
card_uid('ephara, god of the polis'/'BNG', 'BNG:Ephara, God of the Polis:ephara, god of the polis').
card_rarity('ephara, god of the polis'/'BNG', 'Mythic Rare').
card_artist('ephara, god of the polis'/'BNG', 'Eric Deschamps').
card_number('ephara, god of the polis'/'BNG', '145').
card_multiverse_id('ephara, god of the polis'/'BNG', '378517').

card_in_set('epiphany storm', 'BNG').
card_original_type('epiphany storm'/'BNG', 'Enchantment — Aura').
card_original_text('epiphany storm'/'BNG', 'Enchant creature\nEnchanted creature has \"{R}, {T}, Discard a card: Draw a card.\"').
card_first_print('epiphany storm', 'BNG').
card_image_name('epiphany storm'/'BNG', 'epiphany storm').
card_uid('epiphany storm'/'BNG', 'BNG:Epiphany Storm:epiphany storm').
card_rarity('epiphany storm'/'BNG', 'Common').
card_artist('epiphany storm'/'BNG', 'Clint Cearley').
card_number('epiphany storm'/'BNG', '91').
card_flavor_text('epiphany storm'/'BNG', 'In Meletis, certain oracles are trained to be divine lightning rods, receiving countless visions in an instant.').
card_multiverse_id('epiphany storm'/'BNG', '378463').

card_in_set('eternity snare', 'BNG').
card_original_type('eternity snare'/'BNG', 'Enchantment — Aura').
card_original_text('eternity snare'/'BNG', 'Enchant creature\nWhen Eternity Snare enters the battlefield, draw a card.\nEnchanted creature doesn\'t untap during its controller\'s untap step.').
card_image_name('eternity snare'/'BNG', 'eternity snare').
card_uid('eternity snare'/'BNG', 'BNG:Eternity Snare:eternity snare').
card_rarity('eternity snare'/'BNG', 'Uncommon').
card_artist('eternity snare'/'BNG', 'Min Yum').
card_number('eternity snare'/'BNG', '37').
card_multiverse_id('eternity snare'/'BNG', '378409').

card_in_set('evanescent intellect', 'BNG').
card_original_type('evanescent intellect'/'BNG', 'Enchantment — Aura').
card_original_text('evanescent intellect'/'BNG', 'Enchant creature\nEnchanted creature has \"{1}{U}, {T}: Target player puts the top three cards of his or her library into his or her graveyard.\"').
card_first_print('evanescent intellect', 'BNG').
card_image_name('evanescent intellect'/'BNG', 'evanescent intellect').
card_uid('evanescent intellect'/'BNG', 'BNG:Evanescent Intellect:evanescent intellect').
card_rarity('evanescent intellect'/'BNG', 'Common').
card_artist('evanescent intellect'/'BNG', 'Dan Scott').
card_number('evanescent intellect'/'BNG', '38').
card_flavor_text('evanescent intellect'/'BNG', 'The mind is a vessel that may be emptied like any other.').
card_multiverse_id('evanescent intellect'/'BNG', '378410').

card_in_set('everflame eidolon', 'BNG').
card_original_type('everflame eidolon'/'BNG', 'Enchantment Creature — Spirit').
card_original_text('everflame eidolon'/'BNG', 'Bestow {2}{R} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\n{R}: Everflame Eidolon gets +1/+0 until end of turn. If it\'s an Aura, enchanted creature gets +1/+0 until end of turn instead.\nEnchanted creature gets +1/+1.').
card_first_print('everflame eidolon', 'BNG').
card_image_name('everflame eidolon'/'BNG', 'everflame eidolon').
card_uid('everflame eidolon'/'BNG', 'BNG:Everflame Eidolon:everflame eidolon').
card_rarity('everflame eidolon'/'BNG', 'Uncommon').
card_artist('everflame eidolon'/'BNG', 'Daarken').
card_number('everflame eidolon'/'BNG', '92').
card_multiverse_id('everflame eidolon'/'BNG', '378464').

card_in_set('excoriate', 'BNG').
card_original_type('excoriate'/'BNG', 'Sorcery').
card_original_text('excoriate'/'BNG', 'Exile target tapped creature.').
card_first_print('excoriate', 'BNG').
card_image_name('excoriate'/'BNG', 'excoriate').
card_uid('excoriate'/'BNG', 'BNG:Excoriate:excoriate').
card_rarity('excoriate'/'BNG', 'Common').
card_artist('excoriate'/'BNG', 'Johann Bodin').
card_number('excoriate'/'BNG', '10').
card_flavor_text('excoriate'/'BNG', 'The demon had flown past the reach of Erebos\'s whip but not the point of the sun god\'s spear.').
card_multiverse_id('excoriate'/'BNG', '378382').

card_in_set('eye gouge', 'BNG').
card_original_type('eye gouge'/'BNG', 'Instant').
card_original_text('eye gouge'/'BNG', 'Target creature gets -1/-1 until end of turn. If it\'s a Cyclops, destroy it.').
card_first_print('eye gouge', 'BNG').
card_image_name('eye gouge'/'BNG', 'eye gouge').
card_uid('eye gouge'/'BNG', 'BNG:Eye Gouge:eye gouge').
card_rarity('eye gouge'/'BNG', 'Common').
card_artist('eye gouge'/'BNG', 'Tyler Jacobson').
card_number('eye gouge'/'BNG', '67').
card_flavor_text('eye gouge'/'BNG', 'One chance. One throw. One perfect hit.').
card_multiverse_id('eye gouge'/'BNG', '378439').

card_in_set('fall of the hammer', 'BNG').
card_original_type('fall of the hammer'/'BNG', 'Instant').
card_original_text('fall of the hammer'/'BNG', 'Target creature you control deals damage equal to its power to another target creature.').
card_first_print('fall of the hammer', 'BNG').
card_image_name('fall of the hammer'/'BNG', 'fall of the hammer').
card_uid('fall of the hammer'/'BNG', 'BNG:Fall of the Hammer:fall of the hammer').
card_rarity('fall of the hammer'/'BNG', 'Common').
card_artist('fall of the hammer'/'BNG', 'Adam Paquette').
card_number('fall of the hammer'/'BNG', '93').
card_flavor_text('fall of the hammer'/'BNG', 'It is unwise to insult the god of the forge. It is also unwise to stand near anyone else who has insulted him.').
card_multiverse_id('fall of the hammer'/'BNG', '378465').

card_in_set('fanatic of xenagos', 'BNG').
card_original_type('fanatic of xenagos'/'BNG', 'Creature — Centaur Warrior').
card_original_text('fanatic of xenagos'/'BNG', 'Trample\nTribute 1 (As this creature enters the battlefield, an opponent of your choice may place a +1/+1 counter on it.)\nWhen Fanatic of Xenagos enters the battlefield, if tribute wasn\'t paid, it gets +1/+1 and gains haste until end of turn.').
card_image_name('fanatic of xenagos'/'BNG', 'fanatic of xenagos').
card_uid('fanatic of xenagos'/'BNG', 'BNG:Fanatic of Xenagos:fanatic of xenagos').
card_rarity('fanatic of xenagos'/'BNG', 'Uncommon').
card_artist('fanatic of xenagos'/'BNG', 'Volkan Baga').
card_number('fanatic of xenagos'/'BNG', '147').
card_multiverse_id('fanatic of xenagos'/'BNG', '378519').

card_in_set('fate unraveler', 'BNG').
card_original_type('fate unraveler'/'BNG', 'Enchantment Creature — Hag').
card_original_text('fate unraveler'/'BNG', 'Whenever an opponent draws a card, Fate Unraveler deals 1 damage to that player.').
card_first_print('fate unraveler', 'BNG').
card_image_name('fate unraveler'/'BNG', 'fate unraveler').
card_uid('fate unraveler'/'BNG', 'BNG:Fate Unraveler:fate unraveler').
card_rarity('fate unraveler'/'BNG', 'Rare').
card_artist('fate unraveler'/'BNG', 'David Palumbo').
card_number('fate unraveler'/'BNG', '68').
card_flavor_text('fate unraveler'/'BNG', '\"Never forget that the life you\'ve built can be undone by a single loose thread.\"\n—Perisophia the philosopher').
card_multiverse_id('fate unraveler'/'BNG', '378440').

card_in_set('fated conflagration', 'BNG').
card_original_type('fated conflagration'/'BNG', 'Instant').
card_original_text('fated conflagration'/'BNG', 'Fated Conflagration deals 5 damage to target creature or planeswalker. If it\'s your turn, scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_image_name('fated conflagration'/'BNG', 'fated conflagration').
card_uid('fated conflagration'/'BNG', 'BNG:Fated Conflagration:fated conflagration').
card_rarity('fated conflagration'/'BNG', 'Rare').
card_artist('fated conflagration'/'BNG', 'Adam Paquette').
card_number('fated conflagration'/'BNG', '94').
card_multiverse_id('fated conflagration'/'BNG', '378466').

card_in_set('fated infatuation', 'BNG').
card_original_type('fated infatuation'/'BNG', 'Instant').
card_original_text('fated infatuation'/'BNG', 'Put a token onto the battlefield that\'s a copy of target creature you control. If it\'s your turn, scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_first_print('fated infatuation', 'BNG').
card_image_name('fated infatuation'/'BNG', 'fated infatuation').
card_uid('fated infatuation'/'BNG', 'BNG:Fated Infatuation:fated infatuation').
card_rarity('fated infatuation'/'BNG', 'Rare').
card_artist('fated infatuation'/'BNG', 'Winona Nelson').
card_number('fated infatuation'/'BNG', '39').
card_multiverse_id('fated infatuation'/'BNG', '378411').

card_in_set('fated intervention', 'BNG').
card_original_type('fated intervention'/'BNG', 'Instant').
card_original_text('fated intervention'/'BNG', 'Put two 3/3 green Centaur enchantment creature tokens onto the battlefield. If it\'s your turn, scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_first_print('fated intervention', 'BNG').
card_image_name('fated intervention'/'BNG', 'fated intervention').
card_uid('fated intervention'/'BNG', 'BNG:Fated Intervention:fated intervention').
card_rarity('fated intervention'/'BNG', 'Rare').
card_artist('fated intervention'/'BNG', 'Svetlin Velinov').
card_number('fated intervention'/'BNG', '121').
card_multiverse_id('fated intervention'/'BNG', '378493').

card_in_set('fated retribution', 'BNG').
card_original_type('fated retribution'/'BNG', 'Instant').
card_original_text('fated retribution'/'BNG', 'Destroy all creatures and planeswalkers. If it\'s your turn, scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_first_print('fated retribution', 'BNG').
card_image_name('fated retribution'/'BNG', 'fated retribution').
card_uid('fated retribution'/'BNG', 'BNG:Fated Retribution:fated retribution').
card_rarity('fated retribution'/'BNG', 'Rare').
card_artist('fated retribution'/'BNG', 'Jonas De Ro').
card_number('fated retribution'/'BNG', '11').
card_multiverse_id('fated retribution'/'BNG', '378383').

card_in_set('fated return', 'BNG').
card_original_type('fated return'/'BNG', 'Instant').
card_original_text('fated return'/'BNG', 'Put target creature card from a graveyard onto the battlefield under your control. It gains indestructible. If it\'s your turn, scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_first_print('fated return', 'BNG').
card_image_name('fated return'/'BNG', 'fated return').
card_uid('fated return'/'BNG', 'BNG:Fated Return:fated return').
card_rarity('fated return'/'BNG', 'Rare').
card_artist('fated return'/'BNG', 'Peter Mohrbacher').
card_number('fated return'/'BNG', '69').
card_multiverse_id('fated return'/'BNG', '378441').

card_in_set('fearsome temper', 'BNG').
card_original_type('fearsome temper'/'BNG', 'Enchantment — Aura').
card_original_text('fearsome temper'/'BNG', 'Enchant creature\nEnchanted creature gets +2/+2 and has \"{2}{R}: Target creature can\'t block this creature this turn.\"').
card_first_print('fearsome temper', 'BNG').
card_image_name('fearsome temper'/'BNG', 'fearsome temper').
card_uid('fearsome temper'/'BNG', 'BNG:Fearsome Temper:fearsome temper').
card_rarity('fearsome temper'/'BNG', 'Common').
card_artist('fearsome temper'/'BNG', 'Seb McKinnon').
card_number('fearsome temper'/'BNG', '95').
card_flavor_text('fearsome temper'/'BNG', 'Purphoros is god of creation and destruction, which makes him the most creatively destructive god in the pantheon.').
card_multiverse_id('fearsome temper'/'BNG', '378467').

card_in_set('felhide brawler', 'BNG').
card_original_type('felhide brawler'/'BNG', 'Creature — Minotaur').
card_original_text('felhide brawler'/'BNG', 'Felhide Brawler can\'t block unless you control another Minotaur.').
card_first_print('felhide brawler', 'BNG').
card_image_name('felhide brawler'/'BNG', 'felhide brawler').
card_uid('felhide brawler'/'BNG', 'BNG:Felhide Brawler:felhide brawler').
card_rarity('felhide brawler'/'BNG', 'Common').
card_artist('felhide brawler'/'BNG', 'Nils Hamm').
card_number('felhide brawler'/'BNG', '70').
card_flavor_text('felhide brawler'/'BNG', 'Burial rites among the Felhide minotaurs involve devouring those who fell in battle, to remove their shame from memory and to fuel the survivors\' revenge.').
card_multiverse_id('felhide brawler'/'BNG', '378442').

card_in_set('felhide spiritbinder', 'BNG').
card_original_type('felhide spiritbinder'/'BNG', 'Creature — Minotaur Shaman').
card_original_text('felhide spiritbinder'/'BNG', 'Inspired — Whenever Felhide Spiritbinder becomes untapped, you may pay {1}{R}. If you do, put a token onto the battlefield that\'s a copy of another target creature except it\'s an enchantment in addition to its other types. It gains haste. Exile it at the beginning of the next end step.').
card_first_print('felhide spiritbinder', 'BNG').
card_image_name('felhide spiritbinder'/'BNG', 'felhide spiritbinder').
card_uid('felhide spiritbinder'/'BNG', 'BNG:Felhide Spiritbinder:felhide spiritbinder').
card_rarity('felhide spiritbinder'/'BNG', 'Rare').
card_artist('felhide spiritbinder'/'BNG', 'Mathias Kollros').
card_number('felhide spiritbinder'/'BNG', '96').
card_multiverse_id('felhide spiritbinder'/'BNG', '378468').

card_in_set('flame-wreathed phoenix', 'BNG').
card_original_type('flame-wreathed phoenix'/'BNG', 'Creature — Phoenix').
card_original_text('flame-wreathed phoenix'/'BNG', 'Flying\nTribute 2 (As this creature enters the battlefield, an opponent of your choice may place two +1/+1 counters on it.)\nWhen Flame-Wreathed Phoenix enters the battlefield, if tribute wasn\'t paid, it gains haste and \"When this creature dies, return it to its owner\'s hand.\"').
card_first_print('flame-wreathed phoenix', 'BNG').
card_image_name('flame-wreathed phoenix'/'BNG', 'flame-wreathed phoenix').
card_uid('flame-wreathed phoenix'/'BNG', 'BNG:Flame-Wreathed Phoenix:flame-wreathed phoenix').
card_rarity('flame-wreathed phoenix'/'BNG', 'Mythic Rare').
card_artist('flame-wreathed phoenix'/'BNG', 'James Ryman').
card_number('flame-wreathed phoenix'/'BNG', '97').
card_multiverse_id('flame-wreathed phoenix'/'BNG', '378469').

card_in_set('flitterstep eidolon', 'BNG').
card_original_type('flitterstep eidolon'/'BNG', 'Enchantment Creature — Spirit').
card_original_text('flitterstep eidolon'/'BNG', 'Bestow {5}{U} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nFlitterstep Eidolon can\'t be blocked.\nEnchanted creature gets +1/+1 and can\'t be blocked.').
card_first_print('flitterstep eidolon', 'BNG').
card_image_name('flitterstep eidolon'/'BNG', 'flitterstep eidolon').
card_uid('flitterstep eidolon'/'BNG', 'BNG:Flitterstep Eidolon:flitterstep eidolon').
card_rarity('flitterstep eidolon'/'BNG', 'Uncommon').
card_artist('flitterstep eidolon'/'BNG', 'Chase Stone').
card_number('flitterstep eidolon'/'BNG', '40').
card_multiverse_id('flitterstep eidolon'/'BNG', '378412').

card_in_set('floodtide serpent', 'BNG').
card_original_type('floodtide serpent'/'BNG', 'Creature — Serpent').
card_original_text('floodtide serpent'/'BNG', 'Floodtide Serpent can\'t attack unless you return an enchantment you control to its owner\'s hand. (This cost is paid as attackers are declared.)').
card_first_print('floodtide serpent', 'BNG').
card_image_name('floodtide serpent'/'BNG', 'floodtide serpent').
card_uid('floodtide serpent'/'BNG', 'BNG:Floodtide Serpent:floodtide serpent').
card_rarity('floodtide serpent'/'BNG', 'Common').
card_artist('floodtide serpent'/'BNG', 'Steven Belledin').
card_number('floodtide serpent'/'BNG', '41').
card_flavor_text('floodtide serpent'/'BNG', '\"Pray to Thassa for what? A bigger monster to eat it? A wave that can wash it away? What then?\"\n—Lindos, merchant of Meletis').
card_multiverse_id('floodtide serpent'/'BNG', '378413').

card_in_set('forgestoker dragon', 'BNG').
card_original_type('forgestoker dragon'/'BNG', 'Creature — Dragon').
card_original_text('forgestoker dragon'/'BNG', 'Flying\n{1}{R}: Forgestoker Dragon deals 1 damage to target creature. That creature can\'t block this combat. Activate this ability only if Forgestoker Dragon is attacking.').
card_image_name('forgestoker dragon'/'BNG', 'forgestoker dragon').
card_uid('forgestoker dragon'/'BNG', 'BNG:Forgestoker Dragon:forgestoker dragon').
card_rarity('forgestoker dragon'/'BNG', 'Rare').
card_artist('forgestoker dragon'/'BNG', 'Todd Lockwood').
card_number('forgestoker dragon'/'BNG', '98').
card_flavor_text('forgestoker dragon'/'BNG', 'The Akroans fashion their helms to honor the dragons, not to protect against them, as that would be of little help.').
card_multiverse_id('forgestoker dragon'/'BNG', '378470').

card_in_set('forlorn pseudamma', 'BNG').
card_original_type('forlorn pseudamma'/'BNG', 'Creature — Zombie').
card_original_text('forlorn pseudamma'/'BNG', 'Intimidate\nInspired — Whenever Forlorn Pseudamma becomes untapped, you may pay {2}{B}. If you do, put a 2/2 black Zombie enchantment creature token onto the battlefield.').
card_first_print('forlorn pseudamma', 'BNG').
card_image_name('forlorn pseudamma'/'BNG', 'forlorn pseudamma').
card_uid('forlorn pseudamma'/'BNG', 'BNG:Forlorn Pseudamma:forlorn pseudamma').
card_rarity('forlorn pseudamma'/'BNG', 'Uncommon').
card_artist('forlorn pseudamma'/'BNG', 'Winona Nelson').
card_number('forlorn pseudamma'/'BNG', '71').
card_flavor_text('forlorn pseudamma'/'BNG', '\"More children taken. This is an evil we will track without mercy.\"\n—Anthousa of Setessa').
card_multiverse_id('forlorn pseudamma'/'BNG', '378443').

card_in_set('forsaken drifters', 'BNG').
card_original_type('forsaken drifters'/'BNG', 'Creature — Zombie').
card_original_text('forsaken drifters'/'BNG', 'When Forsaken Drifters dies, put the top four cards of your library into your graveyard.').
card_first_print('forsaken drifters', 'BNG').
card_image_name('forsaken drifters'/'BNG', 'forsaken drifters').
card_uid('forsaken drifters'/'BNG', 'BNG:Forsaken Drifters:forsaken drifters').
card_rarity('forsaken drifters'/'BNG', 'Common').
card_artist('forsaken drifters'/'BNG', 'Min Yum').
card_number('forsaken drifters'/'BNG', '72').
card_flavor_text('forsaken drifters'/'BNG', 'It is a terrible thing when the path of the Returned leads them back to the Underworld.').
card_multiverse_id('forsaken drifters'/'BNG', '378444').

card_in_set('ghostblade eidolon', 'BNG').
card_original_type('ghostblade eidolon'/'BNG', 'Enchantment Creature — Spirit').
card_original_text('ghostblade eidolon'/'BNG', 'Bestow {5}{W} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nDouble strike (This creature deals both first-strike and regular combat damage.)\nEnchanted creature gets +1/+1 and has double strike.').
card_first_print('ghostblade eidolon', 'BNG').
card_image_name('ghostblade eidolon'/'BNG', 'ghostblade eidolon').
card_uid('ghostblade eidolon'/'BNG', 'BNG:Ghostblade Eidolon:ghostblade eidolon').
card_rarity('ghostblade eidolon'/'BNG', 'Uncommon').
card_artist('ghostblade eidolon'/'BNG', 'Ryan Yee').
card_number('ghostblade eidolon'/'BNG', '12').
card_multiverse_id('ghostblade eidolon'/'BNG', '378384').

card_in_set('gild', 'BNG').
card_original_type('gild'/'BNG', 'Sorcery').
card_original_text('gild'/'BNG', 'Exile target creature. Put a colorless artifact token named Gold onto the battlefield. It has \"Sacrifice this artifact: Add one mana of any color to your mana pool.\"').
card_first_print('gild', 'BNG').
card_image_name('gild'/'BNG', 'gild').
card_uid('gild'/'BNG', 'BNG:Gild:gild').
card_rarity('gild'/'BNG', 'Rare').
card_artist('gild'/'BNG', 'Richard Wright').
card_number('gild'/'BNG', '73').
card_flavor_text('gild'/'BNG', 'Merchants of the Underworld trade in coins of clay. Gold serves another purpose.').
card_multiverse_id('gild'/'BNG', '378445').

card_in_set('glimpse the sun god', 'BNG').
card_original_type('glimpse the sun god'/'BNG', 'Instant').
card_original_text('glimpse the sun god'/'BNG', 'Tap X target creatures. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_first_print('glimpse the sun god', 'BNG').
card_image_name('glimpse the sun god'/'BNG', 'glimpse the sun god').
card_uid('glimpse the sun god'/'BNG', 'BNG:Glimpse the Sun God:glimpse the sun god').
card_rarity('glimpse the sun god'/'BNG', 'Uncommon').
card_artist('glimpse the sun god'/'BNG', 'Aleksi Briclot').
card_number('glimpse the sun god'/'BNG', '13').
card_flavor_text('glimpse the sun god'/'BNG', 'Better to be beneath Heliod\'s notice than beneath his heel.').
card_multiverse_id('glimpse the sun god'/'BNG', '378385').

card_in_set('god-favored general', 'BNG').
card_original_type('god-favored general'/'BNG', 'Creature — Human Soldier').
card_original_text('god-favored general'/'BNG', 'Inspired — Whenever God-Favored General becomes untapped, you may pay {2}{W}. If you do, put two 1/1 white Soldier enchantment creature tokens onto the battlefield.').
card_first_print('god-favored general', 'BNG').
card_image_name('god-favored general'/'BNG', 'god-favored general').
card_uid('god-favored general'/'BNG', 'BNG:God-Favored General:god-favored general').
card_rarity('god-favored general'/'BNG', 'Uncommon').
card_artist('god-favored general'/'BNG', 'David Palumbo').
card_number('god-favored general'/'BNG', '14').
card_flavor_text('god-favored general'/'BNG', 'Someone had to be first to attack, but he was not alone for long.').
card_multiverse_id('god-favored general'/'BNG', '378386').

card_in_set('gorgon\'s head', 'BNG').
card_original_type('gorgon\'s head'/'BNG', 'Artifact — Equipment').
card_original_text('gorgon\'s head'/'BNG', 'Equipped creature has deathtouch.\nEquip {2}').
card_first_print('gorgon\'s head', 'BNG').
card_image_name('gorgon\'s head'/'BNG', 'gorgon\'s head').
card_uid('gorgon\'s head'/'BNG', 'BNG:Gorgon\'s Head:gorgon\'s head').
card_rarity('gorgon\'s head'/'BNG', 'Uncommon').
card_artist('gorgon\'s head'/'BNG', 'Michael C. Hayes').
card_number('gorgon\'s head'/'BNG', '158').
card_flavor_text('gorgon\'s head'/'BNG', '\"You slew the gorgon? Show me.\"\n—King Igalus, last words').
card_multiverse_id('gorgon\'s head'/'BNG', '378530').

card_in_set('graverobber spider', 'BNG').
card_original_type('graverobber spider'/'BNG', 'Creature — Spider').
card_original_text('graverobber spider'/'BNG', 'Reach\n{3}{B}: Graverobber Spider gets +X/+X until end of turn, where X is the number of creature cards in your graveyard. Activate this ability only once each turn.').
card_first_print('graverobber spider', 'BNG').
card_image_name('graverobber spider'/'BNG', 'graverobber spider').
card_uid('graverobber spider'/'BNG', 'BNG:Graverobber Spider:graverobber spider').
card_rarity('graverobber spider'/'BNG', 'Uncommon').
card_artist('graverobber spider'/'BNG', 'Richard Wright').
card_number('graverobber spider'/'BNG', '122').
card_flavor_text('graverobber spider'/'BNG', 'Cloaks woven from its webs are durable and waterproof but said to bring on nightmares.').
card_multiverse_id('graverobber spider'/'BNG', '378494').

card_in_set('great hart', 'BNG').
card_original_type('great hart'/'BNG', 'Creature — Elk').
card_original_text('great hart'/'BNG', '').
card_first_print('great hart', 'BNG').
card_image_name('great hart'/'BNG', 'great hart').
card_uid('great hart'/'BNG', 'BNG:Great Hart:great hart').
card_rarity('great hart'/'BNG', 'Common').
card_artist('great hart'/'BNG', 'Christopher Moeller').
card_number('great hart'/'BNG', '15').
card_flavor_text('great hart'/'BNG', 'The great hart stood like a statue, its hide painted gold by the dawn. The Champion laid down her weapons and stepped forward within an arm\'s length of the beast. The hart, sacred to Heliod and bathed in the god\'s own light, bowed to the Champion, marking her as the Chosen of the Sun God.\n—The Theriad').
card_multiverse_id('great hart'/'BNG', '378387').

card_in_set('griffin dreamfinder', 'BNG').
card_original_type('griffin dreamfinder'/'BNG', 'Creature — Griffin').
card_original_text('griffin dreamfinder'/'BNG', 'Flying\nWhen Griffin Dreamfinder enters the battlefield, return target enchantment card from your graveyard to your hand.').
card_first_print('griffin dreamfinder', 'BNG').
card_image_name('griffin dreamfinder'/'BNG', 'griffin dreamfinder').
card_uid('griffin dreamfinder'/'BNG', 'BNG:Griffin Dreamfinder:griffin dreamfinder').
card_rarity('griffin dreamfinder'/'BNG', 'Common').
card_artist('griffin dreamfinder'/'BNG', 'Adam Paquette').
card_number('griffin dreamfinder'/'BNG', '16').
card_flavor_text('griffin dreamfinder'/'BNG', 'Some griffins are able to pluck mystic auras from the Æther that are hidden from other mortals.').
card_multiverse_id('griffin dreamfinder'/'BNG', '378388').

card_in_set('grisly transformation', 'BNG').
card_original_type('grisly transformation'/'BNG', 'Enchantment — Aura').
card_original_text('grisly transformation'/'BNG', 'Enchant creature\nWhen Grisly Transformation enters the battlefield, draw a card.\nEnchanted creature has intimidate. (It can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)').
card_first_print('grisly transformation', 'BNG').
card_image_name('grisly transformation'/'BNG', 'grisly transformation').
card_uid('grisly transformation'/'BNG', 'BNG:Grisly Transformation:grisly transformation').
card_rarity('grisly transformation'/'BNG', 'Common').
card_artist('grisly transformation'/'BNG', 'Tyler Jacobson').
card_number('grisly transformation'/'BNG', '74').
card_multiverse_id('grisly transformation'/'BNG', '378446').

card_in_set('herald of torment', 'BNG').
card_original_type('herald of torment'/'BNG', 'Enchantment Creature — Demon').
card_original_text('herald of torment'/'BNG', 'Bestow {3}{B}{B} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nFlying\nAt the beginning of your upkeep, you lose 1 life.\nEnchanted creature gets +3/+3 and has flying.').
card_first_print('herald of torment', 'BNG').
card_image_name('herald of torment'/'BNG', 'herald of torment').
card_uid('herald of torment'/'BNG', 'BNG:Herald of Torment:herald of torment').
card_rarity('herald of torment'/'BNG', 'Rare').
card_artist('herald of torment'/'BNG', 'Vance Kovacs').
card_number('herald of torment'/'BNG', '75').
card_multiverse_id('herald of torment'/'BNG', '378447').

card_in_set('hero of iroas', 'BNG').
card_original_type('hero of iroas'/'BNG', 'Creature — Human Soldier').
card_original_text('hero of iroas'/'BNG', 'Aura spells you cast cost {1} less to cast.\nHeroic — Whenever you cast a spell that targets Hero of Iroas, put a +1/+1 counter on Hero of Iroas.').
card_first_print('hero of iroas', 'BNG').
card_image_name('hero of iroas'/'BNG', 'hero of iroas').
card_uid('hero of iroas'/'BNG', 'BNG:Hero of Iroas:hero of iroas').
card_rarity('hero of iroas'/'BNG', 'Rare').
card_artist('hero of iroas'/'BNG', 'Willian Murai').
card_number('hero of iroas'/'BNG', '17').
card_flavor_text('hero of iroas'/'BNG', '\"My left has felled a cyclops. My right has slain a giant. I bring both to every fight.\"').
card_multiverse_id('hero of iroas'/'BNG', '378389').

card_in_set('hero of leina tower', 'BNG').
card_original_type('hero of leina tower'/'BNG', 'Creature — Human Warrior').
card_original_text('hero of leina tower'/'BNG', 'Heroic — Whenever you cast a spell that targets Hero of Leina Tower, you may pay {X}. If you do, put X +1/+1 counters on Hero of Leina Tower.').
card_first_print('hero of leina tower', 'BNG').
card_image_name('hero of leina tower'/'BNG', 'hero of leina tower').
card_uid('hero of leina tower'/'BNG', 'BNG:Hero of Leina Tower:hero of leina tower').
card_rarity('hero of leina tower'/'BNG', 'Rare').
card_artist('hero of leina tower'/'BNG', 'Aaron Miller').
card_number('hero of leina tower'/'BNG', '123').
card_flavor_text('hero of leina tower'/'BNG', '\"You cannot achieve greatness if its seed has not already been planted in your heart.\"').
card_multiverse_id('hero of leina tower'/'BNG', '378495').

card_in_set('heroes\' podium', 'BNG').
card_original_type('heroes\' podium'/'BNG', 'Legendary Artifact').
card_original_text('heroes\' podium'/'BNG', 'Each legendary creature you control gets +1/+1 for each other legendary creature you control.\n{X}, {T}: Look at the top X cards of your library. You may reveal a legendary creature card from among them and put it into your hand. Put the rest on the bottom of your library in a random order.').
card_first_print('heroes\' podium', 'BNG').
card_image_name('heroes\' podium'/'BNG', 'heroes\' podium').
card_uid('heroes\' podium'/'BNG', 'BNG:Heroes\' Podium:heroes\' podium').
card_rarity('heroes\' podium'/'BNG', 'Rare').
card_artist('heroes\' podium'/'BNG', 'Willian Murai').
card_number('heroes\' podium'/'BNG', '159').
card_multiverse_id('heroes\' podium'/'BNG', '378531').

card_in_set('hold at bay', 'BNG').
card_original_type('hold at bay'/'BNG', 'Instant').
card_original_text('hold at bay'/'BNG', 'Prevent the next 7 damage that would be dealt to target creature or player this turn.').
card_first_print('hold at bay', 'BNG').
card_image_name('hold at bay'/'BNG', 'hold at bay').
card_uid('hold at bay'/'BNG', 'BNG:Hold at Bay:hold at bay').
card_rarity('hold at bay'/'BNG', 'Common').
card_artist('hold at bay'/'BNG', 'Nils Hamm').
card_number('hold at bay'/'BNG', '18').
card_flavor_text('hold at bay'/'BNG', 'It was Daeron\'s bravery that brought him to Odunos. But it was the magic of Meletis that brought him home.').
card_multiverse_id('hold at bay'/'BNG', '378390').

card_in_set('hunter\'s prowess', 'BNG').
card_original_type('hunter\'s prowess'/'BNG', 'Sorcery').
card_original_text('hunter\'s prowess'/'BNG', 'Until end of turn, target creature gets +3/+3 and gains trample and \"Whenever this creature deals combat damage to a player, draw that many cards.\"').
card_first_print('hunter\'s prowess', 'BNG').
card_image_name('hunter\'s prowess'/'BNG', 'hunter\'s prowess').
card_uid('hunter\'s prowess'/'BNG', 'BNG:Hunter\'s Prowess:hunter\'s prowess').
card_rarity('hunter\'s prowess'/'BNG', 'Rare').
card_artist('hunter\'s prowess'/'BNG', 'Greg Staples').
card_number('hunter\'s prowess'/'BNG', '124').
card_flavor_text('hunter\'s prowess'/'BNG', 'To be counted among the warriors of Setessa\'s Leina Tower, you must not fear death, only failure.').
card_multiverse_id('hunter\'s prowess'/'BNG', '378496').

card_in_set('impetuous sunchaser', 'BNG').
card_original_type('impetuous sunchaser'/'BNG', 'Creature — Human Soldier').
card_original_text('impetuous sunchaser'/'BNG', 'Flying, haste\nImpetuous Sunchaser attacks each turn if able.').
card_first_print('impetuous sunchaser', 'BNG').
card_image_name('impetuous sunchaser'/'BNG', 'impetuous sunchaser').
card_uid('impetuous sunchaser'/'BNG', 'BNG:Impetuous Sunchaser:impetuous sunchaser').
card_rarity('impetuous sunchaser'/'BNG', 'Common').
card_artist('impetuous sunchaser'/'BNG', 'Cynthia Sheppard').
card_number('impetuous sunchaser'/'BNG', '99').
card_flavor_text('impetuous sunchaser'/'BNG', '\"I will soar higher than the walls of Akros! Higher than the clouds! Higher than Heliod himself!\"').
card_multiverse_id('impetuous sunchaser'/'BNG', '378471').

card_in_set('karametra\'s favor', 'BNG').
card_original_type('karametra\'s favor'/'BNG', 'Enchantment — Aura').
card_original_text('karametra\'s favor'/'BNG', 'Enchant creature\nWhen Karametra\'s Favor enters the battlefield, draw a card.\nEnchanted creature has \"{T}: Add one mana of any color to your mana pool.\"').
card_first_print('karametra\'s favor', 'BNG').
card_image_name('karametra\'s favor'/'BNG', 'karametra\'s favor').
card_uid('karametra\'s favor'/'BNG', 'BNG:Karametra\'s Favor:karametra\'s favor').
card_rarity('karametra\'s favor'/'BNG', 'Common').
card_artist('karametra\'s favor'/'BNG', 'Chase Stone').
card_number('karametra\'s favor'/'BNG', '125').
card_flavor_text('karametra\'s favor'/'BNG', 'The harvest god\'s cornucopia contains the fruits of the fields, the forest, and beyond.').
card_multiverse_id('karametra\'s favor'/'BNG', '378497').

card_in_set('karametra, god of harvests', 'BNG').
card_original_type('karametra, god of harvests'/'BNG', 'Legendary Enchantment Creature — God').
card_original_text('karametra, god of harvests'/'BNG', 'Indestructible\nAs long as your devotion to green and white is less than seven, Karametra isn\'t a creature.\nWhenever you cast a creature spell, you may search your library for a Forest or Plains card, put it onto the battlefield tapped, then shuffle your library.').
card_first_print('karametra, god of harvests', 'BNG').
card_image_name('karametra, god of harvests'/'BNG', 'karametra, god of harvests').
card_uid('karametra, god of harvests'/'BNG', 'BNG:Karametra, God of Harvests:karametra, god of harvests').
card_rarity('karametra, god of harvests'/'BNG', 'Mythic Rare').
card_artist('karametra, god of harvests'/'BNG', 'Eric Deschamps').
card_number('karametra, god of harvests'/'BNG', '148').
card_multiverse_id('karametra, god of harvests'/'BNG', '378520').

card_in_set('kiora\'s follower', 'BNG').
card_original_type('kiora\'s follower'/'BNG', 'Creature — Merfolk').
card_original_text('kiora\'s follower'/'BNG', '{T}: Untap another target permanent.').
card_image_name('kiora\'s follower'/'BNG', 'kiora\'s follower').
card_uid('kiora\'s follower'/'BNG', 'BNG:Kiora\'s Follower:kiora\'s follower').
card_rarity('kiora\'s follower'/'BNG', 'Uncommon').
card_artist('kiora\'s follower'/'BNG', 'Eric Deschamps').
card_number('kiora\'s follower'/'BNG', '150').
card_flavor_text('kiora\'s follower'/'BNG', '\"She may call herself Kiora but I believe she is Thassa, the embodiment of the sea and empress of the depths.\"').
card_multiverse_id('kiora\'s follower'/'BNG', '378522').

card_in_set('kiora, the crashing wave', 'BNG').
card_original_type('kiora, the crashing wave'/'BNG', 'Planeswalker — Kiora').
card_original_text('kiora, the crashing wave'/'BNG', '+1: Until your next turn, prevent all damage that would be dealt to and dealt by target permanent an opponent controls.\n-1: Draw a card. You may play an additional land this turn.\n-5: You get an emblem with \"At the beginning of your end step, put a 9/9 blue Kraken creature token onto the battlefield.\"').
card_first_print('kiora, the crashing wave', 'BNG').
card_image_name('kiora, the crashing wave'/'BNG', 'kiora, the crashing wave').
card_uid('kiora, the crashing wave'/'BNG', 'BNG:Kiora, the Crashing Wave:kiora, the crashing wave').
card_rarity('kiora, the crashing wave'/'BNG', 'Mythic Rare').
card_artist('kiora, the crashing wave'/'BNG', 'Scott M. Fischer').
card_number('kiora, the crashing wave'/'BNG', '149').
card_multiverse_id('kiora, the crashing wave'/'BNG', '378521').

card_in_set('kragma butcher', 'BNG').
card_original_type('kragma butcher'/'BNG', 'Creature — Minotaur Warrior').
card_original_text('kragma butcher'/'BNG', 'Inspired — Whenever Kragma Butcher becomes untapped, it gets +2/+0 until end of turn.').
card_first_print('kragma butcher', 'BNG').
card_image_name('kragma butcher'/'BNG', 'kragma butcher').
card_uid('kragma butcher'/'BNG', 'BNG:Kragma Butcher:kragma butcher').
card_rarity('kragma butcher'/'BNG', 'Common').
card_artist('kragma butcher'/'BNG', 'Daarken').
card_number('kragma butcher'/'BNG', '100').
card_flavor_text('kragma butcher'/'BNG', 'Minotaurs go into battle hungry. The first sight of their enemies\' blood sends them into a flesh-eating rage.').
card_multiverse_id('kragma butcher'/'BNG', '378472').

card_in_set('kraken of the straits', 'BNG').
card_original_type('kraken of the straits'/'BNG', 'Creature — Kraken').
card_original_text('kraken of the straits'/'BNG', 'Creatures with power less than the number of Islands you control can\'t block Kraken of the Straits.').
card_first_print('kraken of the straits', 'BNG').
card_image_name('kraken of the straits'/'BNG', 'kraken of the straits').
card_uid('kraken of the straits'/'BNG', 'BNG:Kraken of the Straits:kraken of the straits').
card_rarity('kraken of the straits'/'BNG', 'Uncommon').
card_artist('kraken of the straits'/'BNG', 'Richard Wright').
card_number('kraken of the straits'/'BNG', '42').
card_flavor_text('kraken of the straits'/'BNG', 'Thassa felt no need to punish the sailors for their folly in crossing the straits. The kraken would do it for her.').
card_multiverse_id('kraken of the straits'/'BNG', '378414').

card_in_set('lightning volley', 'BNG').
card_original_type('lightning volley'/'BNG', 'Instant').
card_original_text('lightning volley'/'BNG', 'Until end of turn, creatures you control gain \"{T}: This creature deals 1 damage to target creature or player.\"').
card_first_print('lightning volley', 'BNG').
card_image_name('lightning volley'/'BNG', 'lightning volley').
card_uid('lightning volley'/'BNG', 'BNG:Lightning Volley:lightning volley').
card_rarity('lightning volley'/'BNG', 'Uncommon').
card_artist('lightning volley'/'BNG', 'John Avon').
card_number('lightning volley'/'BNG', '101').
card_flavor_text('lightning volley'/'BNG', '\"We are the motes of rain that join to make the thundercloud. The power of the storm god surges through us!\"').
card_multiverse_id('lightning volley'/'BNG', '378473').

card_in_set('loyal pegasus', 'BNG').
card_original_type('loyal pegasus'/'BNG', 'Creature — Pegasus').
card_original_text('loyal pegasus'/'BNG', 'Flying\nLoyal Pegasus can\'t attack or block alone.').
card_first_print('loyal pegasus', 'BNG').
card_image_name('loyal pegasus'/'BNG', 'loyal pegasus').
card_uid('loyal pegasus'/'BNG', 'BNG:Loyal Pegasus:loyal pegasus').
card_rarity('loyal pegasus'/'BNG', 'Common').
card_artist('loyal pegasus'/'BNG', 'John Severin Brassell').
card_number('loyal pegasus'/'BNG', '19').
card_flavor_text('loyal pegasus'/'BNG', '\"Even if I should die in the saddle, she will not let me fall to the ground.\"\n—Hypatia of the Winged Horse').
card_multiverse_id('loyal pegasus'/'BNG', '378391').

card_in_set('marshmist titan', 'BNG').
card_original_type('marshmist titan'/'BNG', 'Creature — Giant').
card_original_text('marshmist titan'/'BNG', 'Marshmist Titan costs {X} less to cast, where X is your devotion to black. (Each {B} in the mana costs of permanents you control counts toward your devotion to black.)').
card_first_print('marshmist titan', 'BNG').
card_image_name('marshmist titan'/'BNG', 'marshmist titan').
card_uid('marshmist titan'/'BNG', 'BNG:Marshmist Titan:marshmist titan').
card_rarity('marshmist titan'/'BNG', 'Common').
card_artist('marshmist titan'/'BNG', 'Volkan Baga').
card_number('marshmist titan'/'BNG', '76').
card_flavor_text('marshmist titan'/'BNG', 'A favorite of Erebos, for it has sent many to the Underworld.').
card_multiverse_id('marshmist titan'/'BNG', '378448').

card_in_set('meletis astronomer', 'BNG').
card_original_type('meletis astronomer'/'BNG', 'Creature — Human Wizard').
card_original_text('meletis astronomer'/'BNG', 'Heroic — Whenever you cast a spell that targets Meletis Astronomer, look at the top three cards of your library. You may reveal an enchantment card from among them and put it into your hand. Put the rest on the bottom of your library in any order.').
card_first_print('meletis astronomer', 'BNG').
card_image_name('meletis astronomer'/'BNG', 'meletis astronomer').
card_uid('meletis astronomer'/'BNG', 'BNG:Meletis Astronomer:meletis astronomer').
card_rarity('meletis astronomer'/'BNG', 'Uncommon').
card_artist('meletis astronomer'/'BNG', 'Dan Scott').
card_number('meletis astronomer'/'BNG', '43').
card_multiverse_id('meletis astronomer'/'BNG', '378415').

card_in_set('mindreaver', 'BNG').
card_original_type('mindreaver'/'BNG', 'Creature — Human Wizard').
card_original_text('mindreaver'/'BNG', 'Heroic — Whenever you cast a spell that targets Mindreaver, exile the top three cards of target player\'s library.\n{U}{U}, Sacrifice Mindreaver: Counter target spell with the same name as a card exiled with Mindreaver.').
card_first_print('mindreaver', 'BNG').
card_image_name('mindreaver'/'BNG', 'mindreaver').
card_uid('mindreaver'/'BNG', 'BNG:Mindreaver:mindreaver').
card_rarity('mindreaver'/'BNG', 'Rare').
card_artist('mindreaver'/'BNG', 'Wesley Burt').
card_number('mindreaver'/'BNG', '44').
card_multiverse_id('mindreaver'/'BNG', '378416').

card_in_set('mischief and mayhem', 'BNG').
card_original_type('mischief and mayhem'/'BNG', 'Sorcery').
card_original_text('mischief and mayhem'/'BNG', 'Up to two target creatures each get +4/+4 until end of turn.').
card_first_print('mischief and mayhem', 'BNG').
card_image_name('mischief and mayhem'/'BNG', 'mischief and mayhem').
card_uid('mischief and mayhem'/'BNG', 'BNG:Mischief and Mayhem:mischief and mayhem').
card_rarity('mischief and mayhem'/'BNG', 'Uncommon').
card_artist('mischief and mayhem'/'BNG', 'Zack Stella').
card_number('mischief and mayhem'/'BNG', '126').
card_flavor_text('mischief and mayhem'/'BNG', '\"The false god Xenagos treats the whole world as his rollick site. If his whims are left unchecked, Theros will be reduced to rubble and ashes.\"\n—Elspeth').
card_multiverse_id('mischief and mayhem'/'BNG', '378498').

card_in_set('mogis, god of slaughter', 'BNG').
card_original_type('mogis, god of slaughter'/'BNG', 'Legendary Enchantment Creature — God').
card_original_text('mogis, god of slaughter'/'BNG', 'Indestructible\nAs long as your devotion to black and red is less than seven, Mogis isn\'t a creature.\nAt the beginning of each opponent\'s upkeep, Mogis deals 2 damage to that player unless he or she sacrifices a creature.').
card_first_print('mogis, god of slaughter', 'BNG').
card_image_name('mogis, god of slaughter'/'BNG', 'mogis, god of slaughter').
card_uid('mogis, god of slaughter'/'BNG', 'BNG:Mogis, God of Slaughter:mogis, god of slaughter').
card_rarity('mogis, god of slaughter'/'BNG', 'Mythic Rare').
card_artist('mogis, god of slaughter'/'BNG', 'Peter Mohrbacher').
card_number('mogis, god of slaughter'/'BNG', '151').
card_multiverse_id('mogis, god of slaughter'/'BNG', '378523').

card_in_set('mortal\'s ardor', 'BNG').
card_original_type('mortal\'s ardor'/'BNG', 'Instant').
card_original_text('mortal\'s ardor'/'BNG', 'Target creature gets +1/+1 and gains lifelink until end of turn. (Damage dealt by the creature also causes its controller to gain that much life.)').
card_first_print('mortal\'s ardor', 'BNG').
card_image_name('mortal\'s ardor'/'BNG', 'mortal\'s ardor').
card_uid('mortal\'s ardor'/'BNG', 'BNG:Mortal\'s Ardor:mortal\'s ardor').
card_rarity('mortal\'s ardor'/'BNG', 'Common').
card_artist('mortal\'s ardor'/'BNG', 'Kev Walker').
card_number('mortal\'s ardor'/'BNG', '20').
card_flavor_text('mortal\'s ardor'/'BNG', '\"Such deeds should be common fare for such as us.\"\n—Anax, king of Akros').
card_multiverse_id('mortal\'s ardor'/'BNG', '378392').

card_in_set('mortal\'s resolve', 'BNG').
card_original_type('mortal\'s resolve'/'BNG', 'Instant').
card_original_text('mortal\'s resolve'/'BNG', 'Target creature gets +1/+1 and gains indestructible until end of turn. (Damage and effects that say \"destroy\" don\'t destroy it.)').
card_first_print('mortal\'s resolve', 'BNG').
card_image_name('mortal\'s resolve'/'BNG', 'mortal\'s resolve').
card_uid('mortal\'s resolve'/'BNG', 'BNG:Mortal\'s Resolve:mortal\'s resolve').
card_rarity('mortal\'s resolve'/'BNG', 'Common').
card_artist('mortal\'s resolve'/'BNG', 'Svetlin Velinov').
card_number('mortal\'s resolve'/'BNG', '127').
card_flavor_text('mortal\'s resolve'/'BNG', 'The Fates tried to cut her string but found the thread had turned to iron.').
card_multiverse_id('mortal\'s resolve'/'BNG', '378499').

card_in_set('necrobite', 'BNG').
card_original_type('necrobite'/'BNG', 'Instant').
card_original_text('necrobite'/'BNG', 'Target creature gains deathtouch until end of turn. Regenerate it.').
card_image_name('necrobite'/'BNG', 'necrobite').
card_uid('necrobite'/'BNG', 'BNG:Necrobite:necrobite').
card_rarity('necrobite'/'BNG', 'Common').
card_artist('necrobite'/'BNG', 'Igor Kieryluk').
card_number('necrobite'/'BNG', '77').
card_flavor_text('necrobite'/'BNG', 'A servant of Pharika must know countless balms that heal and soothe—and countless more that do the opposite.').
card_multiverse_id('necrobite'/'BNG', '378449').

card_in_set('nessian demolok', 'BNG').
card_original_type('nessian demolok'/'BNG', 'Creature — Beast').
card_original_text('nessian demolok'/'BNG', 'Tribute 3 (As this creature enters the battlefield, an opponent of your choice may place three +1/+1 counters on it.)\nWhen Nessian Demolok enters the battlefield, if tribute wasn\'t paid, destroy target noncreature permanent.').
card_first_print('nessian demolok', 'BNG').
card_image_name('nessian demolok'/'BNG', 'nessian demolok').
card_uid('nessian demolok'/'BNG', 'BNG:Nessian Demolok:nessian demolok').
card_rarity('nessian demolok'/'BNG', 'Uncommon').
card_artist('nessian demolok'/'BNG', 'Daarken').
card_number('nessian demolok'/'BNG', '128').
card_multiverse_id('nessian demolok'/'BNG', '378500').

card_in_set('nessian wilds ravager', 'BNG').
card_original_type('nessian wilds ravager'/'BNG', 'Creature — Hydra').
card_original_text('nessian wilds ravager'/'BNG', 'Tribute 6 (As this creature enters the battlefield, an opponent of your choice may place six +1/+1 counters on it.)\nWhen Nessian Wilds Ravager enters the battlefield, if tribute wasn\'t paid, you may have Nessian Wilds Ravager fight another target creature. (Each deals damage equal to its power to the other.)').
card_image_name('nessian wilds ravager'/'BNG', 'nessian wilds ravager').
card_uid('nessian wilds ravager'/'BNG', 'BNG:Nessian Wilds Ravager:nessian wilds ravager').
card_rarity('nessian wilds ravager'/'BNG', 'Rare').
card_artist('nessian wilds ravager'/'BNG', 'Richard Wright').
card_number('nessian wilds ravager'/'BNG', '129').
card_multiverse_id('nessian wilds ravager'/'BNG', '378501').

card_in_set('noble quarry', 'BNG').
card_original_type('noble quarry'/'BNG', 'Enchantment Creature — Unicorn').
card_original_text('noble quarry'/'BNG', 'Bestow {5}{G} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nAll creatures able to block Noble Quarry or enchanted creature do so.\nEnchanted creature gets +1/+1.').
card_first_print('noble quarry', 'BNG').
card_image_name('noble quarry'/'BNG', 'noble quarry').
card_uid('noble quarry'/'BNG', 'BNG:Noble Quarry:noble quarry').
card_rarity('noble quarry'/'BNG', 'Uncommon').
card_artist('noble quarry'/'BNG', 'Michael C. Hayes').
card_number('noble quarry'/'BNG', '130').
card_multiverse_id('noble quarry'/'BNG', '378502').

card_in_set('nullify', 'BNG').
card_original_type('nullify'/'BNG', 'Instant').
card_original_text('nullify'/'BNG', 'Counter target creature or Aura spell.').
card_first_print('nullify', 'BNG').
card_image_name('nullify'/'BNG', 'nullify').
card_uid('nullify'/'BNG', 'BNG:Nullify:nullify').
card_rarity('nullify'/'BNG', 'Common').
card_artist('nullify'/'BNG', 'Adam Paquette').
card_number('nullify'/'BNG', '45').
card_flavor_text('nullify'/'BNG', '\"Those who incur Thassa\'s anger will be swallowed whole by her waves, or torn asunder by her bident.\"\n—Thrasios, triton hero').
card_multiverse_id('nullify'/'BNG', '378417').

card_in_set('nyxborn eidolon', 'BNG').
card_original_type('nyxborn eidolon'/'BNG', 'Enchantment Creature — Spirit').
card_original_text('nyxborn eidolon'/'BNG', 'Bestow {4}{B} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nEnchanted creature gets +2/+1.').
card_first_print('nyxborn eidolon', 'BNG').
card_image_name('nyxborn eidolon'/'BNG', 'nyxborn eidolon').
card_uid('nyxborn eidolon'/'BNG', 'BNG:Nyxborn Eidolon:nyxborn eidolon').
card_rarity('nyxborn eidolon'/'BNG', 'Common').
card_artist('nyxborn eidolon'/'BNG', 'Nils Hamm').
card_number('nyxborn eidolon'/'BNG', '78').
card_flavor_text('nyxborn eidolon'/'BNG', 'A body Returned, a soul still lost.').
card_multiverse_id('nyxborn eidolon'/'BNG', '378450').

card_in_set('nyxborn rollicker', 'BNG').
card_original_type('nyxborn rollicker'/'BNG', 'Enchantment Creature — Satyr').
card_original_text('nyxborn rollicker'/'BNG', 'Bestow {1}{R} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nEnchanted creature gets +1/+1.').
card_first_print('nyxborn rollicker', 'BNG').
card_image_name('nyxborn rollicker'/'BNG', 'nyxborn rollicker').
card_uid('nyxborn rollicker'/'BNG', 'BNG:Nyxborn Rollicker:nyxborn rollicker').
card_rarity('nyxborn rollicker'/'BNG', 'Common').
card_artist('nyxborn rollicker'/'BNG', 'Seb McKinnon').
card_number('nyxborn rollicker'/'BNG', '102').
card_multiverse_id('nyxborn rollicker'/'BNG', '378474').

card_in_set('nyxborn shieldmate', 'BNG').
card_original_type('nyxborn shieldmate'/'BNG', 'Enchantment Creature — Human Soldier').
card_original_text('nyxborn shieldmate'/'BNG', 'Bestow {2}{W} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nEnchanted creature gets +1/+2.').
card_first_print('nyxborn shieldmate', 'BNG').
card_image_name('nyxborn shieldmate'/'BNG', 'nyxborn shieldmate').
card_uid('nyxborn shieldmate'/'BNG', 'BNG:Nyxborn Shieldmate:nyxborn shieldmate').
card_rarity('nyxborn shieldmate'/'BNG', 'Common').
card_artist('nyxborn shieldmate'/'BNG', 'Eric Deschamps').
card_number('nyxborn shieldmate'/'BNG', '21').
card_flavor_text('nyxborn shieldmate'/'BNG', 'In Meletis, the walls have ears. In Akros, they have blades.').
card_multiverse_id('nyxborn shieldmate'/'BNG', '378393').

card_in_set('nyxborn triton', 'BNG').
card_original_type('nyxborn triton'/'BNG', 'Enchantment Creature — Merfolk').
card_original_text('nyxborn triton'/'BNG', 'Bestow {4}{U} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nEnchanted creature gets +2/+3.').
card_first_print('nyxborn triton', 'BNG').
card_image_name('nyxborn triton'/'BNG', 'nyxborn triton').
card_uid('nyxborn triton'/'BNG', 'BNG:Nyxborn Triton:nyxborn triton').
card_rarity('nyxborn triton'/'BNG', 'Common').
card_artist('nyxborn triton'/'BNG', 'Clint Cearley').
card_number('nyxborn triton'/'BNG', '46').
card_flavor_text('nyxborn triton'/'BNG', '\"He is Thassa\'s. I could not sway him.\"\n—Kiora').
card_multiverse_id('nyxborn triton'/'BNG', '378418').

card_in_set('nyxborn wolf', 'BNG').
card_original_type('nyxborn wolf'/'BNG', 'Enchantment Creature — Wolf').
card_original_text('nyxborn wolf'/'BNG', 'Bestow {4}{G} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nEnchanted creature gets +3/+1.').
card_first_print('nyxborn wolf', 'BNG').
card_image_name('nyxborn wolf'/'BNG', 'nyxborn wolf').
card_uid('nyxborn wolf'/'BNG', 'BNG:Nyxborn Wolf:nyxborn wolf').
card_rarity('nyxborn wolf'/'BNG', 'Common').
card_artist('nyxborn wolf'/'BNG', 'Lucas Graciano').
card_number('nyxborn wolf'/'BNG', '131').
card_multiverse_id('nyxborn wolf'/'BNG', '378503').

card_in_set('odunos river trawler', 'BNG').
card_original_type('odunos river trawler'/'BNG', 'Creature — Zombie').
card_original_text('odunos river trawler'/'BNG', 'When Odunos River Trawler enters the battlefield, return target enchantment creature card from your graveyard to your hand.\n{W}, Sacrifice Odunos River Trawler: Return target enchantment creature card from your graveyard to your hand.').
card_first_print('odunos river trawler', 'BNG').
card_image_name('odunos river trawler'/'BNG', 'odunos river trawler').
card_uid('odunos river trawler'/'BNG', 'BNG:Odunos River Trawler:odunos river trawler').
card_rarity('odunos river trawler'/'BNG', 'Uncommon').
card_artist('odunos river trawler'/'BNG', 'Seb McKinnon').
card_number('odunos river trawler'/'BNG', '79').
card_multiverse_id('odunos river trawler'/'BNG', '378451').

card_in_set('oracle of bones', 'BNG').
card_original_type('oracle of bones'/'BNG', 'Creature — Minotaur Shaman').
card_original_text('oracle of bones'/'BNG', 'Haste\nTribute 2 (As this creature enters the battlefield, an opponent of your choice may place two +1/+1 counters on it.)\nWhen Oracle of Bones enters the battlefield, if tribute wasn\'t paid, you may cast an instant or sorcery card from your hand without paying its mana cost.').
card_first_print('oracle of bones', 'BNG').
card_image_name('oracle of bones'/'BNG', 'oracle of bones').
card_uid('oracle of bones'/'BNG', 'BNG:Oracle of Bones:oracle of bones').
card_rarity('oracle of bones'/'BNG', 'Rare').
card_artist('oracle of bones'/'BNG', 'Greg Staples').
card_number('oracle of bones'/'BNG', '103').
card_multiverse_id('oracle of bones'/'BNG', '378475').

card_in_set('oracle\'s insight', 'BNG').
card_original_type('oracle\'s insight'/'BNG', 'Enchantment — Aura').
card_original_text('oracle\'s insight'/'BNG', 'Enchant creature\nEnchanted creature has \"{T}: Scry 1, then draw a card.\" (To scry 1, look at the top card of your library, then you may put that card on the bottom of your library.)').
card_first_print('oracle\'s insight', 'BNG').
card_image_name('oracle\'s insight'/'BNG', 'oracle\'s insight').
card_uid('oracle\'s insight'/'BNG', 'BNG:Oracle\'s Insight:oracle\'s insight').
card_rarity('oracle\'s insight'/'BNG', 'Uncommon').
card_artist('oracle\'s insight'/'BNG', 'Raymond Swanland').
card_number('oracle\'s insight'/'BNG', '47').
card_multiverse_id('oracle\'s insight'/'BNG', '378419').

card_in_set('oreskos sun guide', 'BNG').
card_original_type('oreskos sun guide'/'BNG', 'Creature — Cat Monk').
card_original_text('oreskos sun guide'/'BNG', 'Inspired — Whenever Oreskos Sun Guide becomes untapped, you gain 2 life.').
card_first_print('oreskos sun guide', 'BNG').
card_image_name('oreskos sun guide'/'BNG', 'oreskos sun guide').
card_uid('oreskos sun guide'/'BNG', 'BNG:Oreskos Sun Guide:oreskos sun guide').
card_rarity('oreskos sun guide'/'BNG', 'Common').
card_artist('oreskos sun guide'/'BNG', 'Mathias Kollros').
card_number('oreskos sun guide'/'BNG', '22').
card_flavor_text('oreskos sun guide'/'BNG', '\"Let the humans have their pantheon. We need no gods to thrive. Even a mortal such as I can capture a part of the sun\'s power.\"').
card_multiverse_id('oreskos sun guide'/'BNG', '378394').

card_in_set('ornitharch', 'BNG').
card_original_type('ornitharch'/'BNG', 'Creature — Archon').
card_original_text('ornitharch'/'BNG', 'Flying\nTribute 2 (As this creature enters the battlefield, an opponent of your choice may place two +1/+1 counters on it.)\nWhen Ornitharch enters the battlefield, if tribute wasn\'t paid, put two 1/1 white Bird creature tokens with flying onto the battlefield.').
card_first_print('ornitharch', 'BNG').
card_image_name('ornitharch'/'BNG', 'ornitharch').
card_uid('ornitharch'/'BNG', 'BNG:Ornitharch:ornitharch').
card_rarity('ornitharch'/'BNG', 'Uncommon').
card_artist('ornitharch'/'BNG', 'Clint Cearley').
card_number('ornitharch'/'BNG', '23').
card_multiverse_id('ornitharch'/'BNG', '378395').

card_in_set('pain seer', 'BNG').
card_original_type('pain seer'/'BNG', 'Creature — Human Wizard').
card_original_text('pain seer'/'BNG', 'Inspired — Whenever Pain Seer becomes untapped, reveal the top card of your library and put that card into your hand. You lose life equal to that card\'s converted mana cost.').
card_image_name('pain seer'/'BNG', 'pain seer').
card_uid('pain seer'/'BNG', 'BNG:Pain Seer:pain seer').
card_rarity('pain seer'/'BNG', 'Rare').
card_artist('pain seer'/'BNG', 'Tyler Jacobson').
card_number('pain seer'/'BNG', '80').
card_flavor_text('pain seer'/'BNG', 'Every twitching nerve and pulsing vein carries a message, discernible with the right tools.').
card_multiverse_id('pain seer'/'BNG', '378452').

card_in_set('peregrination', 'BNG').
card_original_type('peregrination'/'BNG', 'Sorcery').
card_original_text('peregrination'/'BNG', 'Search your library for up to two basic land cards, reveal those cards, and put one onto the battlefield tapped and the other into your hand. Shuffle your library, then scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_first_print('peregrination', 'BNG').
card_image_name('peregrination'/'BNG', 'peregrination').
card_uid('peregrination'/'BNG', 'BNG:Peregrination:peregrination').
card_rarity('peregrination'/'BNG', 'Uncommon').
card_artist('peregrination'/'BNG', 'Jonas De Ro').
card_number('peregrination'/'BNG', '132').
card_multiverse_id('peregrination'/'BNG', '378504').

card_in_set('perplexing chimera', 'BNG').
card_original_type('perplexing chimera'/'BNG', 'Enchantment Creature — Chimera').
card_original_text('perplexing chimera'/'BNG', 'Whenever an opponent casts a spell, you may exchange control of Perplexing Chimera and that spell. If you do, you may choose new targets for the spell. (If the spell becomes a permanent, you control that permanent.)').
card_first_print('perplexing chimera', 'BNG').
card_image_name('perplexing chimera'/'BNG', 'perplexing chimera').
card_uid('perplexing chimera'/'BNG', 'BNG:Perplexing Chimera:perplexing chimera').
card_rarity('perplexing chimera'/'BNG', 'Rare').
card_artist('perplexing chimera'/'BNG', 'Tyler Jacobson').
card_number('perplexing chimera'/'BNG', '48').
card_multiverse_id('perplexing chimera'/'BNG', '378420').

card_in_set('pharagax giant', 'BNG').
card_original_type('pharagax giant'/'BNG', 'Creature — Giant').
card_original_text('pharagax giant'/'BNG', 'Tribute 2 (As this creature enters the battlefield, an opponent of your choice may place two +1/+1 counters on it.)\nWhen Pharagax Giant enters the battlefield, if tribute wasn\'t paid, Pharagax Giant deals 5 damage to each opponent.').
card_first_print('pharagax giant', 'BNG').
card_image_name('pharagax giant'/'BNG', 'pharagax giant').
card_uid('pharagax giant'/'BNG', 'BNG:Pharagax Giant:pharagax giant').
card_rarity('pharagax giant'/'BNG', 'Common').
card_artist('pharagax giant'/'BNG', 'Ryan Pancoast').
card_number('pharagax giant'/'BNG', '104').
card_multiverse_id('pharagax giant'/'BNG', '378476').

card_in_set('phenax, god of deception', 'BNG').
card_original_type('phenax, god of deception'/'BNG', 'Legendary Enchantment Creature — God').
card_original_text('phenax, god of deception'/'BNG', 'Indestructible\nAs long as your devotion to blue and black is less than seven, Phenax isn\'t a creature.\nCreatures you control have \"{T}: Target player puts the top X cards of his or her library into his or her graveyard, where X is this creature\'s toughness.\"').
card_first_print('phenax, god of deception', 'BNG').
card_image_name('phenax, god of deception'/'BNG', 'phenax, god of deception').
card_uid('phenax, god of deception'/'BNG', 'BNG:Phenax, God of Deception:phenax, god of deception').
card_rarity('phenax, god of deception'/'BNG', 'Mythic Rare').
card_artist('phenax, god of deception'/'BNG', 'Ryan Barger').
card_number('phenax, god of deception'/'BNG', '152').
card_multiverse_id('phenax, god of deception'/'BNG', '378524').

card_in_set('pheres-band raiders', 'BNG').
card_original_type('pheres-band raiders'/'BNG', 'Creature — Centaur Warrior').
card_original_text('pheres-band raiders'/'BNG', 'Inspired — Whenever Pheres-Band Raiders becomes untapped, you may pay {2}{G}. If you do, put a 3/3 green Centaur enchantment creature token onto the battlefield.').
card_first_print('pheres-band raiders', 'BNG').
card_image_name('pheres-band raiders'/'BNG', 'pheres-band raiders').
card_uid('pheres-band raiders'/'BNG', 'BNG:Pheres-Band Raiders:pheres-band raiders').
card_rarity('pheres-band raiders'/'BNG', 'Uncommon').
card_artist('pheres-band raiders'/'BNG', 'Ryan Barger').
card_number('pheres-band raiders'/'BNG', '133').
card_flavor_text('pheres-band raiders'/'BNG', '\"Who can deny the call to battle when it sings in our blood?\"').
card_multiverse_id('pheres-band raiders'/'BNG', '378505').

card_in_set('pheres-band tromper', 'BNG').
card_original_type('pheres-band tromper'/'BNG', 'Creature — Centaur Warrior').
card_original_text('pheres-band tromper'/'BNG', 'Inspired — Whenever Pheres-Band Tromper becomes untapped, put a +1/+1 counter on it.').
card_first_print('pheres-band tromper', 'BNG').
card_image_name('pheres-band tromper'/'BNG', 'pheres-band tromper').
card_uid('pheres-band tromper'/'BNG', 'BNG:Pheres-Band Tromper:pheres-band tromper').
card_rarity('pheres-band tromper'/'BNG', 'Common').
card_artist('pheres-band tromper'/'BNG', 'Volkan Baga').
card_number('pheres-band tromper'/'BNG', '134').
card_flavor_text('pheres-band tromper'/'BNG', 'Trompers are the most feared warriors of the Pheres Band. Anger is their lifeblood. It sustains them and gives them purpose.').
card_multiverse_id('pheres-band tromper'/'BNG', '378506').

card_in_set('pillar of war', 'BNG').
card_original_type('pillar of war'/'BNG', 'Artifact Creature — Golem').
card_original_text('pillar of war'/'BNG', 'Defender\nAs long as Pillar of War is enchanted, it can attack as though it didn\'t have defender.').
card_first_print('pillar of war', 'BNG').
card_image_name('pillar of war'/'BNG', 'pillar of war').
card_uid('pillar of war'/'BNG', 'BNG:Pillar of War:pillar of war').
card_rarity('pillar of war'/'BNG', 'Uncommon').
card_artist('pillar of war'/'BNG', 'Aleksi Briclot').
card_number('pillar of war'/'BNG', '160').
card_flavor_text('pillar of war'/'BNG', 'Just because a temple has no guards doesn\'t mean it\'s undefended.').
card_multiverse_id('pillar of war'/'BNG', '378532').

card_in_set('pinnacle of rage', 'BNG').
card_original_type('pinnacle of rage'/'BNG', 'Sorcery').
card_original_text('pinnacle of rage'/'BNG', 'Pinnacle of Rage deals 3 damage to each of two target creatures and/or players.').
card_first_print('pinnacle of rage', 'BNG').
card_image_name('pinnacle of rage'/'BNG', 'pinnacle of rage').
card_uid('pinnacle of rage'/'BNG', 'BNG:Pinnacle of Rage:pinnacle of rage').
card_rarity('pinnacle of rage'/'BNG', 'Uncommon').
card_artist('pinnacle of rage'/'BNG', 'Noah Bradley').
card_number('pinnacle of rage'/'BNG', '105').
card_flavor_text('pinnacle of rage'/'BNG', 'Mount Sulano erupted not in magma but in anger.').
card_multiverse_id('pinnacle of rage'/'BNG', '378477').

card_in_set('plea for guidance', 'BNG').
card_original_type('plea for guidance'/'BNG', 'Sorcery').
card_original_text('plea for guidance'/'BNG', 'Search your library for up to two enchantment cards, reveal them, and put them into your hand. Then shuffle your library.').
card_first_print('plea for guidance', 'BNG').
card_image_name('plea for guidance'/'BNG', 'plea for guidance').
card_uid('plea for guidance'/'BNG', 'BNG:Plea for Guidance:plea for guidance').
card_rarity('plea for guidance'/'BNG', 'Rare').
card_artist('plea for guidance'/'BNG', 'Terese Nielsen').
card_number('plea for guidance'/'BNG', '24').
card_flavor_text('plea for guidance'/'BNG', 'On Theros, the dove is a symbol not of peace but of discovery. Quick wings herald the arrival of a revelation.').
card_multiverse_id('plea for guidance'/'BNG', '378396').

card_in_set('ragemonger', 'BNG').
card_original_type('ragemonger'/'BNG', 'Creature — Minotaur Shaman').
card_original_text('ragemonger'/'BNG', 'Minotaur spells you cast cost {B}{R} less to cast. This effect reduces only the amount of colored mana you pay. (For example, if you cast a Minotaur spell with mana cost {2}{R}, it costs {2} to cast.)').
card_first_print('ragemonger', 'BNG').
card_image_name('ragemonger'/'BNG', 'ragemonger').
card_uid('ragemonger'/'BNG', 'BNG:Ragemonger:ragemonger').
card_rarity('ragemonger'/'BNG', 'Uncommon').
card_artist('ragemonger'/'BNG', 'Karl Kopinski').
card_number('ragemonger'/'BNG', '153').
card_multiverse_id('ragemonger'/'BNG', '378525').

card_in_set('raised by wolves', 'BNG').
card_original_type('raised by wolves'/'BNG', 'Enchantment — Aura').
card_original_text('raised by wolves'/'BNG', 'Enchant creature\nWhen Raised by Wolves enters the battlefield, put two 2/2 green Wolf creature tokens onto the battlefield.\nEnchanted creature gets +1/+1 for each Wolf you control.').
card_first_print('raised by wolves', 'BNG').
card_image_name('raised by wolves'/'BNG', 'raised by wolves').
card_uid('raised by wolves'/'BNG', 'BNG:Raised by Wolves:raised by wolves').
card_rarity('raised by wolves'/'BNG', 'Uncommon').
card_artist('raised by wolves'/'BNG', 'Raymond Swanland').
card_number('raised by wolves'/'BNG', '135').
card_multiverse_id('raised by wolves'/'BNG', '378507').

card_in_set('reap what is sown', 'BNG').
card_original_type('reap what is sown'/'BNG', 'Instant').
card_original_text('reap what is sown'/'BNG', 'Put a +1/+1 counter on each of up to three target creatures.').
card_first_print('reap what is sown', 'BNG').
card_image_name('reap what is sown'/'BNG', 'reap what is sown').
card_uid('reap what is sown'/'BNG', 'BNG:Reap What Is Sown:reap what is sown').
card_rarity('reap what is sown'/'BNG', 'Uncommon').
card_artist('reap what is sown'/'BNG', 'Cynthia Sheppard').
card_number('reap what is sown'/'BNG', '154').
card_flavor_text('reap what is sown'/'BNG', 'The strength of the people comes from the land. And the bounty of the land comes from Karametra.').
card_multiverse_id('reap what is sown'/'BNG', '378526').

card_in_set('reckless reveler', 'BNG').
card_original_type('reckless reveler'/'BNG', 'Creature — Satyr').
card_original_text('reckless reveler'/'BNG', '{R}, Sacrifice Reckless Reveler: Destroy target artifact.').
card_first_print('reckless reveler', 'BNG').
card_image_name('reckless reveler'/'BNG', 'reckless reveler').
card_uid('reckless reveler'/'BNG', 'BNG:Reckless Reveler:reckless reveler').
card_rarity('reckless reveler'/'BNG', 'Common').
card_artist('reckless reveler'/'BNG', 'Mike Sass').
card_number('reckless reveler'/'BNG', '106').
card_flavor_text('reckless reveler'/'BNG', '\"The gods of Theros are born of the expectations and beliefs of mortals. If I have found godhood, what does that say about their true desires?\"\n—Xenagos, god of revels').
card_multiverse_id('reckless reveler'/'BNG', '378478').

card_in_set('retraction helix', 'BNG').
card_original_type('retraction helix'/'BNG', 'Instant').
card_original_text('retraction helix'/'BNG', 'Until end of turn, target creature gains \"{T}: Return target nonland permanent to its owner\'s hand.\"').
card_first_print('retraction helix', 'BNG').
card_image_name('retraction helix'/'BNG', 'retraction helix').
card_uid('retraction helix'/'BNG', 'BNG:Retraction Helix:retraction helix').
card_rarity('retraction helix'/'BNG', 'Common').
card_artist('retraction helix'/'BNG', 'Phill Simmer').
card_number('retraction helix'/'BNG', '49').
card_flavor_text('retraction helix'/'BNG', 'Where the geometries of the world collapse, a master of the Æther can harvest power beyond comprehending.').
card_multiverse_id('retraction helix'/'BNG', '378421').

card_in_set('revoke existence', 'BNG').
card_original_type('revoke existence'/'BNG', 'Sorcery').
card_original_text('revoke existence'/'BNG', 'Exile target artifact or enchantment.').
card_image_name('revoke existence'/'BNG', 'revoke existence').
card_uid('revoke existence'/'BNG', 'BNG:Revoke Existence:revoke existence').
card_rarity('revoke existence'/'BNG', 'Common').
card_artist('revoke existence'/'BNG', 'Adam Paquette').
card_number('revoke existence'/'BNG', '25').
card_flavor_text('revoke existence'/'BNG', '\"There will come a time when the gods will look at mortals and shudder.\"\n—Uremides the philosopher').
card_multiverse_id('revoke existence'/'BNG', '378397').

card_in_set('rise to the challenge', 'BNG').
card_original_type('rise to the challenge'/'BNG', 'Instant').
card_original_text('rise to the challenge'/'BNG', 'Target creature gets +2/+0 and gains first strike until end of turn.').
card_first_print('rise to the challenge', 'BNG').
card_image_name('rise to the challenge'/'BNG', 'rise to the challenge').
card_uid('rise to the challenge'/'BNG', 'BNG:Rise to the Challenge:rise to the challenge').
card_rarity('rise to the challenge'/'BNG', 'Common').
card_artist('rise to the challenge'/'BNG', 'Anthony Palumbo').
card_number('rise to the challenge'/'BNG', '107').
card_flavor_text('rise to the challenge'/'BNG', 'Plidius wagered that Arissa couldn\'t kill a chimera with her javelins alone. She proved him wrong in a way everyone could see for days to come.').
card_multiverse_id('rise to the challenge'/'BNG', '378479').

card_in_set('sanguimancy', 'BNG').
card_original_type('sanguimancy'/'BNG', 'Sorcery').
card_original_text('sanguimancy'/'BNG', 'You draw X cards and you lose X life, where X is your devotion to black. (Each {B} in the mana costs of permanents you control counts toward your devotion to black.)').
card_first_print('sanguimancy', 'BNG').
card_image_name('sanguimancy'/'BNG', 'sanguimancy').
card_uid('sanguimancy'/'BNG', 'BNG:Sanguimancy:sanguimancy').
card_rarity('sanguimancy'/'BNG', 'Uncommon').
card_artist('sanguimancy'/'BNG', 'Dave Kendall').
card_number('sanguimancy'/'BNG', '81').
card_multiverse_id('sanguimancy'/'BNG', '378453').

card_in_set('satyr firedancer', 'BNG').
card_original_type('satyr firedancer'/'BNG', 'Enchantment Creature — Satyr').
card_original_text('satyr firedancer'/'BNG', 'Whenever an instant or sorcery spell you control deals damage to an opponent, Satyr Firedancer deals that much damage to target creature that player controls.').
card_first_print('satyr firedancer', 'BNG').
card_image_name('satyr firedancer'/'BNG', 'satyr firedancer').
card_uid('satyr firedancer'/'BNG', 'BNG:Satyr Firedancer:satyr firedancer').
card_rarity('satyr firedancer'/'BNG', 'Rare').
card_artist('satyr firedancer'/'BNG', 'Chris Rahn').
card_number('satyr firedancer'/'BNG', '108').
card_flavor_text('satyr firedancer'/'BNG', 'His heart, the coal; his eye, the spark; his hand, the whirling flame.').
card_multiverse_id('satyr firedancer'/'BNG', '378480').

card_in_set('satyr nyx-smith', 'BNG').
card_original_type('satyr nyx-smith'/'BNG', 'Creature — Satyr Shaman').
card_original_text('satyr nyx-smith'/'BNG', 'Haste\nInspired — Whenever Satyr Nyx-Smith becomes untapped, you may pay {2}{R}. If you do, put a 3/1 red Elemental enchantment creature token with haste onto the battlefield.').
card_first_print('satyr nyx-smith', 'BNG').
card_image_name('satyr nyx-smith'/'BNG', 'satyr nyx-smith').
card_uid('satyr nyx-smith'/'BNG', 'BNG:Satyr Nyx-Smith:satyr nyx-smith').
card_rarity('satyr nyx-smith'/'BNG', 'Uncommon').
card_artist('satyr nyx-smith'/'BNG', 'Greg Staples').
card_number('satyr nyx-smith'/'BNG', '109').
card_multiverse_id('satyr nyx-smith'/'BNG', '378481').

card_in_set('satyr wayfinder', 'BNG').
card_original_type('satyr wayfinder'/'BNG', 'Creature — Satyr').
card_original_text('satyr wayfinder'/'BNG', 'When Satyr Wayfinder enters the battlefield, reveal the top four cards of your library. You may put a land card from among them into your hand. Put the rest into your graveyard.').
card_first_print('satyr wayfinder', 'BNG').
card_image_name('satyr wayfinder'/'BNG', 'satyr wayfinder').
card_uid('satyr wayfinder'/'BNG', 'BNG:Satyr Wayfinder:satyr wayfinder').
card_rarity('satyr wayfinder'/'BNG', 'Common').
card_artist('satyr wayfinder'/'BNG', 'Steve Prescott').
card_number('satyr wayfinder'/'BNG', '136').
card_flavor_text('satyr wayfinder'/'BNG', 'The first satyr to wake after a revel must search for the site of the next one.').
card_multiverse_id('satyr wayfinder'/'BNG', '378508').

card_in_set('scourge of skola vale', 'BNG').
card_original_type('scourge of skola vale'/'BNG', 'Creature — Hydra').
card_original_text('scourge of skola vale'/'BNG', 'Trample\nScourge of Skola Vale enters the battlefield with two +1/+1 counters on it.\n{T}, Sacrifice another creature: Put a number of +1/+1 counters on Scourge of Skola Vale equal to the sacrificed creature\'s toughness.').
card_first_print('scourge of skola vale', 'BNG').
card_image_name('scourge of skola vale'/'BNG', 'scourge of skola vale').
card_uid('scourge of skola vale'/'BNG', 'BNG:Scourge of Skola Vale:scourge of skola vale').
card_rarity('scourge of skola vale'/'BNG', 'Rare').
card_artist('scourge of skola vale'/'BNG', 'Dave Kendall').
card_number('scourge of skola vale'/'BNG', '137').
card_multiverse_id('scourge of skola vale'/'BNG', '378509').

card_in_set('scouring sands', 'BNG').
card_original_type('scouring sands'/'BNG', 'Sorcery').
card_original_text('scouring sands'/'BNG', 'Scouring Sands deals 1 damage to each creature your opponents control. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_first_print('scouring sands', 'BNG').
card_image_name('scouring sands'/'BNG', 'scouring sands').
card_uid('scouring sands'/'BNG', 'BNG:Scouring Sands:scouring sands').
card_rarity('scouring sands'/'BNG', 'Common').
card_artist('scouring sands'/'BNG', 'Jonas De Ro').
card_number('scouring sands'/'BNG', '110').
card_flavor_text('scouring sands'/'BNG', 'When Purphoros cleans the ashes from his forge, entire cities vanish.').
card_multiverse_id('scouring sands'/'BNG', '378482').

card_in_set('searing blood', 'BNG').
card_original_type('searing blood'/'BNG', 'Instant').
card_original_text('searing blood'/'BNG', 'Searing Blood deals 2 damage to target creature. When that creature dies this turn, Searing Blood deals 3 damage to the creature\'s controller.').
card_first_print('searing blood', 'BNG').
card_image_name('searing blood'/'BNG', 'searing blood').
card_uid('searing blood'/'BNG', 'BNG:Searing Blood:searing blood').
card_rarity('searing blood'/'BNG', 'Uncommon').
card_artist('searing blood'/'BNG', 'Daniel Ljunggren').
card_number('searing blood'/'BNG', '111').
card_flavor_text('searing blood'/'BNG', 'Purphoros\'s blessing: sometimes a boon, sometimes a bane. Always ablaze.').
card_multiverse_id('searing blood'/'BNG', '378483').

card_in_set('servant of tymaret', 'BNG').
card_original_type('servant of tymaret'/'BNG', 'Creature — Zombie').
card_original_text('servant of tymaret'/'BNG', 'Inspired — Whenever Servant of Tymaret becomes untapped, each opponent loses 1 life. You gain life equal to the life lost this way.\n{2}{B}: Regenerate Servant of Tymaret.').
card_first_print('servant of tymaret', 'BNG').
card_image_name('servant of tymaret'/'BNG', 'servant of tymaret').
card_uid('servant of tymaret'/'BNG', 'BNG:Servant of Tymaret:servant of tymaret').
card_rarity('servant of tymaret'/'BNG', 'Common').
card_artist('servant of tymaret'/'BNG', 'Karl Kopinski').
card_number('servant of tymaret'/'BNG', '82').
card_flavor_text('servant of tymaret'/'BNG', 'Life is most precious to those who have already lost it.').
card_multiverse_id('servant of tymaret'/'BNG', '378454').

card_in_set('setessan oathsworn', 'BNG').
card_original_type('setessan oathsworn'/'BNG', 'Creature — Satyr Warrior').
card_original_text('setessan oathsworn'/'BNG', 'Heroic — Whenever you cast a spell that targets Setessan Oathsworn, put two +1/+1 counters on Setessan Oathsworn.').
card_first_print('setessan oathsworn', 'BNG').
card_image_name('setessan oathsworn'/'BNG', 'setessan oathsworn').
card_uid('setessan oathsworn'/'BNG', 'BNG:Setessan Oathsworn:setessan oathsworn').
card_rarity('setessan oathsworn'/'BNG', 'Common').
card_artist('setessan oathsworn'/'BNG', 'Scott Murphy').
card_number('setessan oathsworn'/'BNG', '138').
card_flavor_text('setessan oathsworn'/'BNG', '\"Setessa is not the city of my birth, but it is the place I fight for, and the place I\'m willing to die for. Does that not make it my home?\"').
card_multiverse_id('setessan oathsworn'/'BNG', '378510').

card_in_set('setessan starbreaker', 'BNG').
card_original_type('setessan starbreaker'/'BNG', 'Creature — Human Warrior').
card_original_text('setessan starbreaker'/'BNG', 'When Setessan Starbreaker enters the battlefield, you may destroy target Aura.').
card_first_print('setessan starbreaker', 'BNG').
card_image_name('setessan starbreaker'/'BNG', 'setessan starbreaker').
card_uid('setessan starbreaker'/'BNG', 'BNG:Setessan Starbreaker:setessan starbreaker').
card_rarity('setessan starbreaker'/'BNG', 'Common').
card_artist('setessan starbreaker'/'BNG', 'Chase Stone').
card_number('setessan starbreaker'/'BNG', '139').
card_flavor_text('setessan starbreaker'/'BNG', '\"Who says you cannot touch the stars?\"').
card_multiverse_id('setessan starbreaker'/'BNG', '378511').

card_in_set('shrike harpy', 'BNG').
card_original_type('shrike harpy'/'BNG', 'Creature — Harpy').
card_original_text('shrike harpy'/'BNG', 'Flying\nTribute 2 (As this creature enters the battlefield, an opponent of your choice may place two +1/+1 counters on it.)\nWhen Shrike Harpy enters the battlefield, if tribute wasn\'t paid, target opponent sacrifices a creature.').
card_first_print('shrike harpy', 'BNG').
card_image_name('shrike harpy'/'BNG', 'shrike harpy').
card_uid('shrike harpy'/'BNG', 'BNG:Shrike Harpy:shrike harpy').
card_rarity('shrike harpy'/'BNG', 'Uncommon').
card_artist('shrike harpy'/'BNG', 'Christopher Moeller').
card_number('shrike harpy'/'BNG', '83').
card_multiverse_id('shrike harpy'/'BNG', '378455').

card_in_set('silent sentinel', 'BNG').
card_original_type('silent sentinel'/'BNG', 'Creature — Archon').
card_original_text('silent sentinel'/'BNG', 'Flying\nWhenever Silent Sentinel attacks, you may return target enchantment card from your graveyard to the battlefield.').
card_image_name('silent sentinel'/'BNG', 'silent sentinel').
card_uid('silent sentinel'/'BNG', 'BNG:Silent Sentinel:silent sentinel').
card_rarity('silent sentinel'/'BNG', 'Rare').
card_artist('silent sentinel'/'BNG', 'Slawomir Maniak').
card_number('silent sentinel'/'BNG', '26').
card_flavor_text('silent sentinel'/'BNG', 'It serves a justice higher than the whims of the gods.').
card_multiverse_id('silent sentinel'/'BNG', '378398').

card_in_set('siren of the fanged coast', 'BNG').
card_original_type('siren of the fanged coast'/'BNG', 'Creature — Siren').
card_original_text('siren of the fanged coast'/'BNG', 'Flying\nTribute 3 (As this creature enters the battlefield, an opponent of your choice may place three +1/+1 counters on it.)\nWhen Siren of the Fanged Coast enters the battlefield, if tribute wasn\'t paid, gain control of target creature.').
card_first_print('siren of the fanged coast', 'BNG').
card_image_name('siren of the fanged coast'/'BNG', 'siren of the fanged coast').
card_uid('siren of the fanged coast'/'BNG', 'BNG:Siren of the Fanged Coast:siren of the fanged coast').
card_rarity('siren of the fanged coast'/'BNG', 'Uncommon').
card_artist('siren of the fanged coast'/'BNG', 'Michael C. Hayes').
card_number('siren of the fanged coast'/'BNG', '50').
card_multiverse_id('siren of the fanged coast'/'BNG', '378422').

card_in_set('siren of the silent song', 'BNG').
card_original_type('siren of the silent song'/'BNG', 'Creature — Zombie Siren').
card_original_text('siren of the silent song'/'BNG', 'Flying\nInspired — Whenever Siren of the Silent Song becomes untapped, each opponent discards a card, then puts the top card of his or her library into his or her graveyard.').
card_first_print('siren of the silent song', 'BNG').
card_image_name('siren of the silent song'/'BNG', 'siren of the silent song').
card_uid('siren of the silent song'/'BNG', 'BNG:Siren of the Silent Song:siren of the silent song').
card_rarity('siren of the silent song'/'BNG', 'Uncommon').
card_artist('siren of the silent song'/'BNG', 'Anthony Palumbo').
card_number('siren of the silent song'/'BNG', '155').
card_multiverse_id('siren of the silent song'/'BNG', '378527').

card_in_set('siren song lyre', 'BNG').
card_original_type('siren song lyre'/'BNG', 'Artifact — Equipment').
card_original_text('siren song lyre'/'BNG', 'Equipped creature has \"{2}, {T}: Tap target creature.\"\nEquip {2}').
card_first_print('siren song lyre', 'BNG').
card_image_name('siren song lyre'/'BNG', 'siren song lyre').
card_uid('siren song lyre'/'BNG', 'BNG:Siren Song Lyre:siren song lyre').
card_rarity('siren song lyre'/'BNG', 'Uncommon').
card_artist('siren song lyre'/'BNG', 'James Paick').
card_number('siren song lyre'/'BNG', '161').
card_flavor_text('siren song lyre'/'BNG', 'A siren once tried to steal it out of jealousy but was caught by the song of its strings.').
card_multiverse_id('siren song lyre'/'BNG', '378533').

card_in_set('skyreaping', 'BNG').
card_original_type('skyreaping'/'BNG', 'Sorcery').
card_original_text('skyreaping'/'BNG', 'Skyreaping deals damage to each creature with flying equal to your devotion to green. (Each {G} in the mana costs of permanents you control counts toward your devotion to green.)').
card_first_print('skyreaping', 'BNG').
card_image_name('skyreaping'/'BNG', 'skyreaping').
card_uid('skyreaping'/'BNG', 'BNG:Skyreaping:skyreaping').
card_rarity('skyreaping'/'BNG', 'Uncommon').
card_artist('skyreaping'/'BNG', 'Seb McKinnon').
card_number('skyreaping'/'BNG', '140').
card_multiverse_id('skyreaping'/'BNG', '378512').

card_in_set('snake of the golden grove', 'BNG').
card_original_type('snake of the golden grove'/'BNG', 'Creature — Snake').
card_original_text('snake of the golden grove'/'BNG', 'Tribute 3 (As this creature enters the battlefield, an opponent of your choice may place three +1/+1 counters on it.)\nWhen Snake of the Golden Grove enters the battlefield, if tribute wasn\'t paid, you gain 4 life.').
card_first_print('snake of the golden grove', 'BNG').
card_image_name('snake of the golden grove'/'BNG', 'snake of the golden grove').
card_uid('snake of the golden grove'/'BNG', 'BNG:Snake of the Golden Grove:snake of the golden grove').
card_rarity('snake of the golden grove'/'BNG', 'Common').
card_artist('snake of the golden grove'/'BNG', 'Mathias Kollros').
card_number('snake of the golden grove'/'BNG', '141').
card_flavor_text('snake of the golden grove'/'BNG', 'Some fruits are best left ungathered.').
card_multiverse_id('snake of the golden grove'/'BNG', '378513').

card_in_set('sphinx\'s disciple', 'BNG').
card_original_type('sphinx\'s disciple'/'BNG', 'Creature — Human Wizard').
card_original_text('sphinx\'s disciple'/'BNG', 'Flying\nInspired — Whenever Sphinx\'s Disciple becomes untapped, draw a card.').
card_first_print('sphinx\'s disciple', 'BNG').
card_image_name('sphinx\'s disciple'/'BNG', 'sphinx\'s disciple').
card_uid('sphinx\'s disciple'/'BNG', 'BNG:Sphinx\'s Disciple:sphinx\'s disciple').
card_rarity('sphinx\'s disciple'/'BNG', 'Common').
card_artist('sphinx\'s disciple'/'BNG', 'Ryan Alexander Lee').
card_number('sphinx\'s disciple'/'BNG', '51').
card_flavor_text('sphinx\'s disciple'/'BNG', 'Few dare to brave the Dakra Isles. Those who return are forever changed.').
card_multiverse_id('sphinx\'s disciple'/'BNG', '378423').

card_in_set('spirit of the labyrinth', 'BNG').
card_original_type('spirit of the labyrinth'/'BNG', 'Enchantment Creature — Spirit').
card_original_text('spirit of the labyrinth'/'BNG', 'Each player can\'t draw more than one card each turn.').
card_first_print('spirit of the labyrinth', 'BNG').
card_image_name('spirit of the labyrinth'/'BNG', 'spirit of the labyrinth').
card_uid('spirit of the labyrinth'/'BNG', 'BNG:Spirit of the Labyrinth:spirit of the labyrinth').
card_rarity('spirit of the labyrinth'/'BNG', 'Rare').
card_artist('spirit of the labyrinth'/'BNG', 'Jason Chan').
card_number('spirit of the labyrinth'/'BNG', '27').
card_flavor_text('spirit of the labyrinth'/'BNG', 'Students at the Dekatia Academy learn that being sent to study with her is a lesson in itself.').
card_multiverse_id('spirit of the labyrinth'/'BNG', '378399').

card_in_set('spiteful returned', 'BNG').
card_original_type('spiteful returned'/'BNG', 'Enchantment Creature — Zombie').
card_original_text('spiteful returned'/'BNG', 'Bestow {3}{B} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nWhenever Spiteful Returned or enchanted creature attacks, defending player loses 2 life.\nEnchanted creature gets +1/+1.').
card_first_print('spiteful returned', 'BNG').
card_image_name('spiteful returned'/'BNG', 'spiteful returned').
card_uid('spiteful returned'/'BNG', 'BNG:Spiteful Returned:spiteful returned').
card_rarity('spiteful returned'/'BNG', 'Uncommon').
card_artist('spiteful returned'/'BNG', 'Raymond Swanland').
card_number('spiteful returned'/'BNG', '84').
card_multiverse_id('spiteful returned'/'BNG', '378456').

card_in_set('springleaf drum', 'BNG').
card_original_type('springleaf drum'/'BNG', 'Artifact').
card_original_text('springleaf drum'/'BNG', '{T}, Tap an untapped creature you control: Add one mana of any color to your mana pool.').
card_image_name('springleaf drum'/'BNG', 'springleaf drum').
card_uid('springleaf drum'/'BNG', 'BNG:Springleaf Drum:springleaf drum').
card_rarity('springleaf drum'/'BNG', 'Uncommon').
card_artist('springleaf drum'/'BNG', 'Seb McKinnon').
card_number('springleaf drum'/'BNG', '162').
card_flavor_text('springleaf drum'/'BNG', '\"The music is in the drum. I simply coax it out to be heard.\"').
card_multiverse_id('springleaf drum'/'BNG', '378534').

card_in_set('stormcaller of keranos', 'BNG').
card_original_type('stormcaller of keranos'/'BNG', 'Creature — Human Shaman').
card_original_text('stormcaller of keranos'/'BNG', 'Haste\n{1}{U}: Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_first_print('stormcaller of keranos', 'BNG').
card_image_name('stormcaller of keranos'/'BNG', 'stormcaller of keranos').
card_uid('stormcaller of keranos'/'BNG', 'BNG:Stormcaller of Keranos:stormcaller of keranos').
card_rarity('stormcaller of keranos'/'BNG', 'Uncommon').
card_artist('stormcaller of keranos'/'BNG', 'Marco Nelor').
card_number('stormcaller of keranos'/'BNG', '112').
card_flavor_text('stormcaller of keranos'/'BNG', 'A prayer answered by Keranos is clear for all to witness.').
card_multiverse_id('stormcaller of keranos'/'BNG', '378484').

card_in_set('stratus walk', 'BNG').
card_original_type('stratus walk'/'BNG', 'Enchantment — Aura').
card_original_text('stratus walk'/'BNG', 'Enchant creature\nWhen Stratus Walk enters the battlefield, draw a card.\nEnchanted creature has flying.\nEnchanted creature can block only creatures with flying.').
card_first_print('stratus walk', 'BNG').
card_image_name('stratus walk'/'BNG', 'stratus walk').
card_uid('stratus walk'/'BNG', 'BNG:Stratus Walk:stratus walk').
card_rarity('stratus walk'/'BNG', 'Common').
card_artist('stratus walk'/'BNG', 'Aaron Miller').
card_number('stratus walk'/'BNG', '52').
card_multiverse_id('stratus walk'/'BNG', '378424').

card_in_set('sudden storm', 'BNG').
card_original_type('sudden storm'/'BNG', 'Instant').
card_original_text('sudden storm'/'BNG', 'Tap up to two target creatures. Those creatures don\'t untap during their controllers\' next untap steps. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_first_print('sudden storm', 'BNG').
card_image_name('sudden storm'/'BNG', 'sudden storm').
card_uid('sudden storm'/'BNG', 'BNG:Sudden Storm:sudden storm').
card_rarity('sudden storm'/'BNG', 'Common').
card_artist('sudden storm'/'BNG', 'Adam Paquette').
card_number('sudden storm'/'BNG', '53').
card_flavor_text('sudden storm'/'BNG', 'No matter how big and strong you are, the sea is bigger and stronger.').
card_multiverse_id('sudden storm'/'BNG', '378425').

card_in_set('sunbond', 'BNG').
card_original_type('sunbond'/'BNG', 'Enchantment — Aura').
card_original_text('sunbond'/'BNG', 'Enchant creature\nEnchanted creature has \"Whenever you gain life, put that many +1/+1 counters on this creature.\"').
card_first_print('sunbond', 'BNG').
card_image_name('sunbond'/'BNG', 'sunbond').
card_uid('sunbond'/'BNG', 'BNG:Sunbond:sunbond').
card_rarity('sunbond'/'BNG', 'Uncommon').
card_artist('sunbond'/'BNG', 'Noah Bradley').
card_number('sunbond'/'BNG', '28').
card_flavor_text('sunbond'/'BNG', '\"I was not chosen for my faith in the gods. Sometimes the gods must put their faith in us.\"\n—Elspeth').
card_multiverse_id('sunbond'/'BNG', '378400').

card_in_set('swordwise centaur', 'BNG').
card_original_type('swordwise centaur'/'BNG', 'Creature — Centaur Warrior').
card_original_text('swordwise centaur'/'BNG', '').
card_first_print('swordwise centaur', 'BNG').
card_image_name('swordwise centaur'/'BNG', 'swordwise centaur').
card_uid('swordwise centaur'/'BNG', 'BNG:Swordwise Centaur:swordwise centaur').
card_rarity('swordwise centaur'/'BNG', 'Common').
card_artist('swordwise centaur'/'BNG', 'Slawomir Maniak').
card_number('swordwise centaur'/'BNG', '142').
card_flavor_text('swordwise centaur'/'BNG', 'The girl who would become the Champion of the Sun hacked furiously at the practice dummy. At last she stopped, breathing heavily, and looked up at her instructor.\n\"So much anger,\" said the centaur. \"I will teach you the ways of war, child. But first you must make peace with yourself.\"\n—The Theriad').
card_multiverse_id('swordwise centaur'/'BNG', '378514').

card_in_set('temple of enlightenment', 'BNG').
card_original_type('temple of enlightenment'/'BNG', 'Land').
card_original_text('temple of enlightenment'/'BNG', 'Temple of Enlightenment enters the battlefield tapped.\nWhen Temple of Enlightenment enters the battlefield, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)\n{T}: Add {W} or {U} to your mana pool.').
card_first_print('temple of enlightenment', 'BNG').
card_image_name('temple of enlightenment'/'BNG', 'temple of enlightenment').
card_uid('temple of enlightenment'/'BNG', 'BNG:Temple of Enlightenment:temple of enlightenment').
card_rarity('temple of enlightenment'/'BNG', 'Rare').
card_artist('temple of enlightenment'/'BNG', 'Svetlin Velinov').
card_number('temple of enlightenment'/'BNG', '163').
card_multiverse_id('temple of enlightenment'/'BNG', '378535').

card_in_set('temple of malice', 'BNG').
card_original_type('temple of malice'/'BNG', 'Land').
card_original_text('temple of malice'/'BNG', 'Temple of Malice enters the battlefield tapped.\nWhen Temple of Malice enters the battlefield, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)\n{T}: Add {B} or {R} to your mana pool.').
card_first_print('temple of malice', 'BNG').
card_image_name('temple of malice'/'BNG', 'temple of malice').
card_uid('temple of malice'/'BNG', 'BNG:Temple of Malice:temple of malice').
card_rarity('temple of malice'/'BNG', 'Rare').
card_artist('temple of malice'/'BNG', 'Sam Burley').
card_number('temple of malice'/'BNG', '164').
card_multiverse_id('temple of malice'/'BNG', '378536').

card_in_set('temple of plenty', 'BNG').
card_original_type('temple of plenty'/'BNG', 'Land').
card_original_text('temple of plenty'/'BNG', 'Temple of Plenty enters the battlefield tapped.\nWhen Temple of Plenty enters the battlefield, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)\n{T}: Add {G} or {W} to your mana pool.').
card_first_print('temple of plenty', 'BNG').
card_image_name('temple of plenty'/'BNG', 'temple of plenty').
card_uid('temple of plenty'/'BNG', 'BNG:Temple of Plenty:temple of plenty').
card_rarity('temple of plenty'/'BNG', 'Rare').
card_artist('temple of plenty'/'BNG', 'Noah Bradley').
card_number('temple of plenty'/'BNG', '165').
card_multiverse_id('temple of plenty'/'BNG', '378537').

card_in_set('thassa\'s rebuff', 'BNG').
card_original_type('thassa\'s rebuff'/'BNG', 'Instant').
card_original_text('thassa\'s rebuff'/'BNG', 'Counter target spell unless its controller pays {X}, where X is your devotion to blue. (Each {U} in the mana costs of permanents you control counts toward your devotion to blue.)').
card_first_print('thassa\'s rebuff', 'BNG').
card_image_name('thassa\'s rebuff'/'BNG', 'thassa\'s rebuff').
card_uid('thassa\'s rebuff'/'BNG', 'BNG:Thassa\'s Rebuff:thassa\'s rebuff').
card_rarity('thassa\'s rebuff'/'BNG', 'Uncommon').
card_artist('thassa\'s rebuff'/'BNG', 'Lucas Graciano').
card_number('thassa\'s rebuff'/'BNG', '54').
card_flavor_text('thassa\'s rebuff'/'BNG', 'To resist Thassa is to deny the rising tide.').
card_multiverse_id('thassa\'s rebuff'/'BNG', '378426').

card_in_set('thunder brute', 'BNG').
card_original_type('thunder brute'/'BNG', 'Creature — Cyclops').
card_original_text('thunder brute'/'BNG', 'Trample\nTribute 3 (As this creature enters the battlefield, an opponent of your choice may place three +1/+1 counters on it.)\nWhen Thunder Brute enters the battlefield, if tribute wasn\'t paid, it gains haste until end of turn.').
card_first_print('thunder brute', 'BNG').
card_image_name('thunder brute'/'BNG', 'thunder brute').
card_uid('thunder brute'/'BNG', 'BNG:Thunder Brute:thunder brute').
card_rarity('thunder brute'/'BNG', 'Uncommon').
card_artist('thunder brute'/'BNG', 'Phill Simmer').
card_number('thunder brute'/'BNG', '113').
card_multiverse_id('thunder brute'/'BNG', '378485').

card_in_set('thunderous might', 'BNG').
card_original_type('thunderous might'/'BNG', 'Enchantment — Aura').
card_original_text('thunderous might'/'BNG', 'Enchant creature\nWhenever enchanted creature attacks, it gets +X/+0 until end of turn, where X is your devotion to red. (Each {R} in the mana costs of permanents you control counts toward your devotion to red.)').
card_first_print('thunderous might', 'BNG').
card_image_name('thunderous might'/'BNG', 'thunderous might').
card_uid('thunderous might'/'BNG', 'BNG:Thunderous Might:thunderous might').
card_rarity('thunderous might'/'BNG', 'Uncommon').
card_artist('thunderous might'/'BNG', 'Jaime Jones').
card_number('thunderous might'/'BNG', '114').
card_multiverse_id('thunderous might'/'BNG', '378486').

card_in_set('tromokratis', 'BNG').
card_original_type('tromokratis'/'BNG', 'Legendary Creature — Kraken').
card_original_text('tromokratis'/'BNG', 'Tromokratis has hexproof unless it\'s attacking or blocking.\nTromokratis can\'t be blocked unless all creatures defending player controls block it. (If any creature that player controls doesn\'t block this creature, it can\'t be blocked.)').
card_image_name('tromokratis'/'BNG', 'tromokratis').
card_uid('tromokratis'/'BNG', 'BNG:Tromokratis:tromokratis').
card_rarity('tromokratis'/'BNG', 'Rare').
card_artist('tromokratis'/'BNG', 'Matt Stewart').
card_number('tromokratis'/'BNG', '55').
card_multiverse_id('tromokratis'/'BNG', '378427').

card_in_set('unravel the æther', 'BNG').
card_original_type('unravel the æther'/'BNG', 'Instant').
card_original_text('unravel the æther'/'BNG', 'Choose target artifact or enchantment. Its owner shuffles it into his or her library.').
card_first_print('unravel the æther', 'BNG').
card_image_name('unravel the æther'/'BNG', 'unravel the aether').
card_uid('unravel the æther'/'BNG', 'BNG:Unravel the Æther:unravel the aether').
card_rarity('unravel the æther'/'BNG', 'Uncommon').
card_artist('unravel the æther'/'BNG', 'Jung Park').
card_number('unravel the æther'/'BNG', '143').
card_flavor_text('unravel the æther'/'BNG', 'Thaumaturges of Theros have learned to weave their magic into the world around them, but it can be pulled apart strand by strand.').
card_multiverse_id('unravel the æther'/'BNG', '378515').

card_in_set('vanguard of brimaz', 'BNG').
card_original_type('vanguard of brimaz'/'BNG', 'Creature — Cat Soldier').
card_original_text('vanguard of brimaz'/'BNG', 'Vigilance\nHeroic — Whenever you cast a spell that targets Vanguard of Brimaz, put a 1/1 white Cat Soldier creature token with vigilance onto the battlefield.').
card_first_print('vanguard of brimaz', 'BNG').
card_image_name('vanguard of brimaz'/'BNG', 'vanguard of brimaz').
card_uid('vanguard of brimaz'/'BNG', 'BNG:Vanguard of Brimaz:vanguard of brimaz').
card_rarity('vanguard of brimaz'/'BNG', 'Uncommon').
card_artist('vanguard of brimaz'/'BNG', 'Mark Zug').
card_number('vanguard of brimaz'/'BNG', '29').
card_flavor_text('vanguard of brimaz'/'BNG', '\"The humans and their gods never blessed me. Only the pride deserves my allegiance.\"').
card_multiverse_id('vanguard of brimaz'/'BNG', '378401').

card_in_set('vortex elemental', 'BNG').
card_original_type('vortex elemental'/'BNG', 'Creature — Elemental').
card_original_text('vortex elemental'/'BNG', '{U}: Put Vortex Elemental and each creature blocking or blocked by it on top of their owners\' libraries, then those players shuffle their libraries.\n{3}{U}{U}: Target creature blocks Vortex Elemental this turn if able.').
card_first_print('vortex elemental', 'BNG').
card_image_name('vortex elemental'/'BNG', 'vortex elemental').
card_uid('vortex elemental'/'BNG', 'BNG:Vortex Elemental:vortex elemental').
card_rarity('vortex elemental'/'BNG', 'Uncommon').
card_artist('vortex elemental'/'BNG', 'Jack Wang').
card_number('vortex elemental'/'BNG', '56').
card_flavor_text('vortex elemental'/'BNG', 'The sea is always hungry.').
card_multiverse_id('vortex elemental'/'BNG', '378428').

card_in_set('warchanter of mogis', 'BNG').
card_original_type('warchanter of mogis'/'BNG', 'Creature — Minotaur Shaman').
card_original_text('warchanter of mogis'/'BNG', 'Inspired — Whenever Warchanter of Mogis becomes untapped, target creature you control gains intimidate until end of turn. (A creature with intimidate can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)').
card_first_print('warchanter of mogis', 'BNG').
card_image_name('warchanter of mogis'/'BNG', 'warchanter of mogis').
card_uid('warchanter of mogis'/'BNG', 'BNG:Warchanter of Mogis:warchanter of mogis').
card_rarity('warchanter of mogis'/'BNG', 'Common').
card_artist('warchanter of mogis'/'BNG', 'Mike Bierek').
card_number('warchanter of mogis'/'BNG', '85').
card_multiverse_id('warchanter of mogis'/'BNG', '378457').

card_in_set('weight of the underworld', 'BNG').
card_original_type('weight of the underworld'/'BNG', 'Enchantment — Aura').
card_original_text('weight of the underworld'/'BNG', 'Enchant creature\nEnchanted creature gets -3/-2.').
card_first_print('weight of the underworld', 'BNG').
card_image_name('weight of the underworld'/'BNG', 'weight of the underworld').
card_uid('weight of the underworld'/'BNG', 'BNG:Weight of the Underworld:weight of the underworld').
card_rarity('weight of the underworld'/'BNG', 'Common').
card_artist('weight of the underworld'/'BNG', 'Wesley Burt').
card_number('weight of the underworld'/'BNG', '86').
card_flavor_text('weight of the underworld'/'BNG', 'Proud Alkmenos, who would not bow to Erebos in death, is now bowed by his own hubris for all eternity.').
card_multiverse_id('weight of the underworld'/'BNG', '378458').

card_in_set('whelming wave', 'BNG').
card_original_type('whelming wave'/'BNG', 'Sorcery').
card_original_text('whelming wave'/'BNG', 'Return all creatures to their owners\' hands except for Krakens, Leviathans, Octopuses, and Serpents.').
card_first_print('whelming wave', 'BNG').
card_image_name('whelming wave'/'BNG', 'whelming wave').
card_uid('whelming wave'/'BNG', 'BNG:Whelming Wave:whelming wave').
card_rarity('whelming wave'/'BNG', 'Rare').
card_artist('whelming wave'/'BNG', 'Slawomir Maniak').
card_number('whelming wave'/'BNG', '57').
card_flavor_text('whelming wave'/'BNG', '\"I can see why this appeals to Thassa.\"\n—Kiora').
card_multiverse_id('whelming wave'/'BNG', '378429').

card_in_set('whims of the fates', 'BNG').
card_original_type('whims of the fates'/'BNG', 'Sorcery').
card_original_text('whims of the fates'/'BNG', 'Starting with you, each player separates all permanents he or she controls into three piles. Then each player chooses one of his or her piles at random and sacrifices those permanents. (Piles can be empty.)').
card_first_print('whims of the fates', 'BNG').
card_image_name('whims of the fates'/'BNG', 'whims of the fates').
card_uid('whims of the fates'/'BNG', 'BNG:Whims of the Fates:whims of the fates').
card_rarity('whims of the fates'/'BNG', 'Rare').
card_artist('whims of the fates'/'BNG', 'Seb McKinnon').
card_number('whims of the fates'/'BNG', '115').
card_multiverse_id('whims of the fates'/'BNG', '378487').

card_in_set('xenagos, god of revels', 'BNG').
card_original_type('xenagos, god of revels'/'BNG', 'Legendary Enchantment Creature — God').
card_original_text('xenagos, god of revels'/'BNG', 'Indestructible\nAs long as your devotion to red and green is less than seven, Xenagos isn\'t a creature.\nAt the beginning of combat on your turn, another target creature you control gains haste and gets +X/+X until end of turn, where X is that creature\'s power.').
card_first_print('xenagos, god of revels', 'BNG').
card_image_name('xenagos, god of revels'/'BNG', 'xenagos, god of revels').
card_uid('xenagos, god of revels'/'BNG', 'BNG:Xenagos, God of Revels:xenagos, god of revels').
card_rarity('xenagos, god of revels'/'BNG', 'Mythic Rare').
card_artist('xenagos, god of revels'/'BNG', 'Jason Chan').
card_number('xenagos, god of revels'/'BNG', '156').
card_multiverse_id('xenagos, god of revels'/'BNG', '378528').
