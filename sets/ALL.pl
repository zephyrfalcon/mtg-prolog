% Alliances

set('ALL').
set_name('ALL', 'Alliances').
set_release_date('ALL', '1996-06-10').
set_border('ALL', 'black').
set_type('ALL', 'expansion').
set_block('ALL', 'Ice Age').

card_in_set('aesthir glider', 'ALL').
card_original_type('aesthir glider'/'ALL', 'Artifact Creature').
card_original_text('aesthir glider'/'ALL', 'Flying\nCannot be assigned to block.').
card_first_print('aesthir glider', 'ALL').
card_image_name('aesthir glider'/'ALL', 'aesthir glider1').
card_uid('aesthir glider'/'ALL', 'ALL:Aesthir Glider:aesthir glider1').
card_rarity('aesthir glider'/'ALL', 'Common').
card_artist('aesthir glider'/'ALL', 'Ruth Thompson').
card_flavor_text('aesthir glider'/'ALL', '\"Sacrilege A noble ally in life, made nothing more than a glorified kite in death\"\n—Arna Kennerüd, Skycaptain').
card_multiverse_id('aesthir glider'/'ALL', '3040').

card_in_set('aesthir glider', 'ALL').
card_original_type('aesthir glider'/'ALL', 'Artifact Creature').
card_original_text('aesthir glider'/'ALL', 'Flying\nCannot be assigned to block.').
card_image_name('aesthir glider'/'ALL', 'aesthir glider2').
card_uid('aesthir glider'/'ALL', 'ALL:Aesthir Glider:aesthir glider2').
card_rarity('aesthir glider'/'ALL', 'Common').
card_artist('aesthir glider'/'ALL', 'Ruth Thompson').
card_flavor_text('aesthir glider'/'ALL', '\"A fine example of the rewards of artifice: a thoroughly obedient steed with wings of Soldevi steel.\"\n—Arcum Dagsson,\nSoldevi Machinist').
card_multiverse_id('aesthir glider'/'ALL', '3041').

card_in_set('agent of stromgald', 'ALL').
card_original_type('agent of stromgald'/'ALL', 'Summon — Knight').
card_original_text('agent of stromgald'/'ALL', '{R}: Add {B} to your mana pool. Play this ability as an interrupt.').
card_first_print('agent of stromgald', 'ALL').
card_image_name('agent of stromgald'/'ALL', 'agent of stromgald1').
card_uid('agent of stromgald'/'ALL', 'ALL:Agent of Stromgald:agent of stromgald1').
card_rarity('agent of stromgald'/'ALL', 'Common').
card_artist('agent of stromgald'/'ALL', 'Alan Rabinowitz').
card_flavor_text('agent of stromgald'/'ALL', '\"The Order of Stromgald spreads its poisonous lies from within Varchild\'s troops, and still she sees only visions of conquest.\"\n—Lovisa Coldeyes, Balduvian Chieftain').
card_multiverse_id('agent of stromgald'/'ALL', '3159').

card_in_set('agent of stromgald', 'ALL').
card_original_type('agent of stromgald'/'ALL', 'Summon — Knight').
card_original_text('agent of stromgald'/'ALL', '{R}: Add {B} to your mana pool. Play this ability as an interrupt.').
card_image_name('agent of stromgald'/'ALL', 'agent of stromgald2').
card_uid('agent of stromgald'/'ALL', 'ALL:Agent of Stromgald:agent of stromgald2').
card_rarity('agent of stromgald'/'ALL', 'Common').
card_artist('agent of stromgald'/'ALL', 'Alan Rabinowitz').
card_flavor_text('agent of stromgald'/'ALL', '\"The ‘fabled\' Order of Stromgald is cast to the four winds, lost for all time. I, for one, give them no thought.\"\n—General Varchild').
card_multiverse_id('agent of stromgald'/'ALL', '3160').

card_in_set('arcane denial', 'ALL').
card_original_type('arcane denial'/'ALL', 'Interrupt').
card_original_text('arcane denial'/'ALL', 'Counter target spell. That spell\'s caster may draw up to two cards at the beginning of the next turn\'s upkeep.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_first_print('arcane denial', 'ALL').
card_image_name('arcane denial'/'ALL', 'arcane denial1').
card_uid('arcane denial'/'ALL', 'ALL:Arcane Denial:arcane denial1').
card_rarity('arcane denial'/'ALL', 'Common').
card_artist('arcane denial'/'ALL', 'Richard Kane Ferguson').
card_multiverse_id('arcane denial'/'ALL', '3097').

card_in_set('arcane denial', 'ALL').
card_original_type('arcane denial'/'ALL', 'Interrupt').
card_original_text('arcane denial'/'ALL', 'Counter target spell. That spell\'s caster may draw up to two cards at the beginning of the next turn\'s upkeep.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_image_name('arcane denial'/'ALL', 'arcane denial2').
card_uid('arcane denial'/'ALL', 'ALL:Arcane Denial:arcane denial2').
card_rarity('arcane denial'/'ALL', 'Common').
card_artist('arcane denial'/'ALL', 'Richard Kane Ferguson').
card_multiverse_id('arcane denial'/'ALL', '3098').

card_in_set('ashnod\'s cylix', 'ALL').
card_original_type('ashnod\'s cylix'/'ALL', 'Artifact').
card_original_text('ashnod\'s cylix'/'ALL', '{3}, {T}: Target player looks at the top three cards of his or her library and puts one of them on top of that library. Remove the remaining two from the game.').
card_first_print('ashnod\'s cylix', 'ALL').
card_image_name('ashnod\'s cylix'/'ALL', 'ashnod\'s cylix').
card_uid('ashnod\'s cylix'/'ALL', 'ALL:Ashnod\'s Cylix:ashnod\'s cylix').
card_rarity('ashnod\'s cylix'/'ALL', 'Rare').
card_artist('ashnod\'s cylix'/'ALL', 'Nicola Leonard').
card_flavor_text('ashnod\'s cylix'/'ALL', 'Few remember that Ashnod\'s defilement of Terisiare\'s resources outstripped even that of her peers.').
card_multiverse_id('ashnod\'s cylix'/'ALL', '3042').

card_in_set('astrolabe', 'ALL').
card_original_type('astrolabe'/'ALL', 'Artifact').
card_original_text('astrolabe'/'ALL', '{1}, {T}: Sacrifice Astrolabe to add two mana of any one color to your mana pool. Play this ability as an interrupt. Draw a card at the beginning of the next turn\'s upkeep.').
card_first_print('astrolabe', 'ALL').
card_image_name('astrolabe'/'ALL', 'astrolabe1').
card_uid('astrolabe'/'ALL', 'ALL:Astrolabe:astrolabe1').
card_rarity('astrolabe'/'ALL', 'Common').
card_artist('astrolabe'/'ALL', 'Amy Weber').
card_multiverse_id('astrolabe'/'ALL', '3044').

card_in_set('astrolabe', 'ALL').
card_original_type('astrolabe'/'ALL', 'Artifact').
card_original_text('astrolabe'/'ALL', '{1}, {T}: Sacrifice Astrolabe to add two mana of any one color to your mana pool. Play this ability as an interrupt. Draw a card at the beginning of the next turn\'s upkeep.').
card_image_name('astrolabe'/'ALL', 'astrolabe2').
card_uid('astrolabe'/'ALL', 'ALL:Astrolabe:astrolabe2').
card_rarity('astrolabe'/'ALL', 'Common').
card_artist('astrolabe'/'ALL', 'Amy Weber').
card_multiverse_id('astrolabe'/'ALL', '3043').

card_in_set('awesome presence', 'ALL').
card_original_type('awesome presence'/'ALL', 'Enchant Creature').
card_original_text('awesome presence'/'ALL', 'Enchanted creature cannot be blocked unless defending player pays an additional {3} for each creature assigned to block enchanted creature.').
card_first_print('awesome presence', 'ALL').
card_image_name('awesome presence'/'ALL', 'awesome presence1').
card_uid('awesome presence'/'ALL', 'ALL:Awesome Presence:awesome presence1').
card_rarity('awesome presence'/'ALL', 'Common').
card_artist('awesome presence'/'ALL', 'Lawrence Snelly').
card_flavor_text('awesome presence'/'ALL', '\"At the core of all power is raw passion.\"Gerda Äagesdotter,\nArchmage of the Unseen').
card_multiverse_id('awesome presence'/'ALL', '3099').

card_in_set('awesome presence', 'ALL').
card_original_type('awesome presence'/'ALL', 'Enchant Creature').
card_original_text('awesome presence'/'ALL', 'Enchanted creature cannot be blocked unless defending player pays an additional {3} for each creature assigned to block enchanted creature.').
card_image_name('awesome presence'/'ALL', 'awesome presence2').
card_uid('awesome presence'/'ALL', 'ALL:Awesome Presence:awesome presence2').
card_rarity('awesome presence'/'ALL', 'Common').
card_artist('awesome presence'/'ALL', 'Lawrence Snelly').
card_flavor_text('awesome presence'/'ALL', '\"I shape my weapons from the fears of my enemies.\"\n—Chaeska, Keeper of Tresserhorn').
card_multiverse_id('awesome presence'/'ALL', '3100').

card_in_set('balduvian dead', 'ALL').
card_original_type('balduvian dead'/'ALL', 'Summon — Zombies').
card_original_text('balduvian dead'/'ALL', '{2}{R}: Remove target summon card in your graveyard from the game to put a Graveborn token into play. Treat this token as a 3/1 black and red creature that can attack the turn it comes into play. Bury Graveborn token at end of turn.').
card_first_print('balduvian dead', 'ALL').
card_image_name('balduvian dead'/'ALL', 'balduvian dead').
card_uid('balduvian dead'/'ALL', 'ALL:Balduvian Dead:balduvian dead').
card_rarity('balduvian dead'/'ALL', 'Uncommon').
card_artist('balduvian dead'/'ALL', 'Mike Kimble').
card_multiverse_id('balduvian dead'/'ALL', '3066').

card_in_set('balduvian horde', 'ALL').
card_original_type('balduvian horde'/'ALL', 'Summon — Barbarians').
card_original_text('balduvian horde'/'ALL', 'When Balduvian Horde comes into play, discard a card at random from your hand or bury Balduvian Horde.').
card_first_print('balduvian horde', 'ALL').
card_image_name('balduvian horde'/'ALL', 'balduvian horde').
card_uid('balduvian horde'/'ALL', 'ALL:Balduvian Horde:balduvian horde').
card_rarity('balduvian horde'/'ALL', 'Rare').
card_artist('balduvian horde'/'ALL', 'Brian Snõddy').
card_flavor_text('balduvian horde'/'ALL', '\"Peace will come only when we have taken Varchild\'s head.\"\n—Lovisa Coldeyes,\nBalduvian Chieftain').
card_multiverse_id('balduvian horde'/'ALL', '3161').

card_in_set('balduvian trading post', 'ALL').
card_original_type('balduvian trading post'/'ALL', 'Land').
card_original_text('balduvian trading post'/'ALL', 'When Balduvian Trading Post comes into play, sacrifice an untapped mountain or bury Balduvian Trading Post.\n{T}: Add {1}{R} to your mana pool.\n{1}, {T}: Balduvian Trading Post deals 1 damage to target attacking creature.').
card_first_print('balduvian trading post', 'ALL').
card_image_name('balduvian trading post'/'ALL', 'balduvian trading post').
card_uid('balduvian trading post'/'ALL', 'ALL:Balduvian Trading Post:balduvian trading post').
card_rarity('balduvian trading post'/'ALL', 'Rare').
card_artist('balduvian trading post'/'ALL', 'Tom Wänerstrand').
card_multiverse_id('balduvian trading post'/'ALL', '3231').

card_in_set('balduvian war-makers', 'ALL').
card_original_type('balduvian war-makers'/'ALL', 'Summon — Barbarians').
card_original_text('balduvian war-makers'/'ALL', 'Rampage: 1\nBalduvian War-Makers can attack the turn it comes into play on your side.').
card_first_print('balduvian war-makers', 'ALL').
card_image_name('balduvian war-makers'/'ALL', 'balduvian war-makers1').
card_uid('balduvian war-makers'/'ALL', 'ALL:Balduvian War-Makers:balduvian war-makers1').
card_rarity('balduvian war-makers'/'ALL', 'Common').
card_artist('balduvian war-makers'/'ALL', 'Mike Kimble').
card_flavor_text('balduvian war-makers'/'ALL', '\"Those mewling Barbarians will cower before my righteous steel!\"\n—General Varchild').
card_multiverse_id('balduvian war-makers'/'ALL', '3162').

card_in_set('balduvian war-makers', 'ALL').
card_original_type('balduvian war-makers'/'ALL', 'Summon — Barbarians').
card_original_text('balduvian war-makers'/'ALL', 'Rampage: 1\nBalduvian War-Makers can attack the turn it comes into play on your side.').
card_image_name('balduvian war-makers'/'ALL', 'balduvian war-makers2').
card_uid('balduvian war-makers'/'ALL', 'ALL:Balduvian War-Makers:balduvian war-makers2').
card_rarity('balduvian war-makers'/'ALL', 'Common').
card_artist('balduvian war-makers'/'ALL', 'Mike Kimble').
card_flavor_text('balduvian war-makers'/'ALL', '\"We send our best to battle, but every man, woman, and child will stand against Varchild\'s butchers.\" —Lovisa Coldeyes, Balduvian Chieftain').
card_multiverse_id('balduvian war-makers'/'ALL', '3163').

card_in_set('benthic explorers', 'ALL').
card_original_type('benthic explorers'/'ALL', 'Summon — Merfolk').
card_original_text('benthic explorers'/'ALL', '{T}: Untap target tapped land an opponent controls to add one mana of any type that land produces to your mana pool.').
card_first_print('benthic explorers', 'ALL').
card_image_name('benthic explorers'/'ALL', 'benthic explorers1').
card_uid('benthic explorers'/'ALL', 'ALL:Benthic Explorers:benthic explorers1').
card_rarity('benthic explorers'/'ALL', 'Common').
card_artist('benthic explorers'/'ALL', 'Greg Simanson').
card_flavor_text('benthic explorers'/'ALL', 'The rising oceans brought new lakes—and new terrors—to Terisiare. The Explorers found their ancient enemies spawning everywhere.').
card_multiverse_id('benthic explorers'/'ALL', '3102').

card_in_set('benthic explorers', 'ALL').
card_original_type('benthic explorers'/'ALL', 'Summon — Merfolk').
card_original_text('benthic explorers'/'ALL', '{T}: Untap target tapped land an opponent controls to add one mana of any type that land produces to your mana pool.').
card_image_name('benthic explorers'/'ALL', 'benthic explorers2').
card_uid('benthic explorers'/'ALL', 'ALL:Benthic Explorers:benthic explorers2').
card_rarity('benthic explorers'/'ALL', 'Common').
card_artist('benthic explorers'/'ALL', 'Greg Simanson').
card_flavor_text('benthic explorers'/'ALL', 'Distant Atlantis charged these Explorers with the task of venturing into unknown waters to track the despised Viscerids.').
card_multiverse_id('benthic explorers'/'ALL', '3101').

card_in_set('bestial fury', 'ALL').
card_original_type('bestial fury'/'ALL', 'Enchant Creature').
card_original_text('bestial fury'/'ALL', 'Draw a card at the beginning of the upkeep of the turn after Bestial Fury comes into play.\nIf enchanted creature attacks and is blocked, it gains trample and gets +4/+0 until end of turn.').
card_first_print('bestial fury', 'ALL').
card_image_name('bestial fury'/'ALL', 'bestial fury1').
card_uid('bestial fury'/'ALL', 'ALL:Bestial Fury:bestial fury1').
card_rarity('bestial fury'/'ALL', 'Common').
card_artist('bestial fury'/'ALL', 'Mike Raabe').
card_multiverse_id('bestial fury'/'ALL', '3165').

card_in_set('bestial fury', 'ALL').
card_original_type('bestial fury'/'ALL', 'Enchant Creature').
card_original_text('bestial fury'/'ALL', 'Draw a card at the beginning of the upkeep of the turn after Bestial Fury comes into play.\nIf enchanted creature attacks and is blocked, it gains trample and gets +4/+0 until end of turn.').
card_image_name('bestial fury'/'ALL', 'bestial fury2').
card_uid('bestial fury'/'ALL', 'ALL:Bestial Fury:bestial fury2').
card_rarity('bestial fury'/'ALL', 'Common').
card_artist('bestial fury'/'ALL', 'Mike Raabe').
card_multiverse_id('bestial fury'/'ALL', '3164').

card_in_set('bounty of the hunt', 'ALL').
card_original_type('bounty of the hunt'/'ALL', 'Instant').
card_original_text('bounty of the hunt'/'ALL', 'You may remove a green card in your hand from the game instead of paying Bounty of the Hunt\'s casting cost.\nPut three +1/+1 counters, distributed any way you choose, on any number of target creatures. Remove these counters at end of turn.').
card_first_print('bounty of the hunt', 'ALL').
card_image_name('bounty of the hunt'/'ALL', 'bounty of the hunt').
card_uid('bounty of the hunt'/'ALL', 'ALL:Bounty of the Hunt:bounty of the hunt').
card_rarity('bounty of the hunt'/'ALL', 'Uncommon').
card_artist('bounty of the hunt'/'ALL', 'Jeff A. Menges').
card_multiverse_id('bounty of the hunt'/'ALL', '3128').

card_in_set('browse', 'ALL').
card_original_type('browse'/'ALL', 'Enchantment').
card_original_text('browse'/'ALL', '{2}{U}{U}: Look at the top five cards of your library and put one of them into your hand. Remove the remaining four from the game.').
card_first_print('browse', 'ALL').
card_image_name('browse'/'ALL', 'browse').
card_uid('browse'/'ALL', 'ALL:Browse:browse').
card_rarity('browse'/'ALL', 'Uncommon').
card_artist('browse'/'ALL', 'Phil Foglio').
card_flavor_text('browse'/'ALL', '\"Once great literature—now great litter.\"\n—Jaya Ballard, Task Mage').
card_multiverse_id('browse'/'ALL', '3103').

card_in_set('burnout', 'ALL').
card_original_type('burnout'/'ALL', 'Interrupt').
card_original_text('burnout'/'ALL', 'Counter target interrupt spell if it is blue.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_first_print('burnout', 'ALL').
card_image_name('burnout'/'ALL', 'burnout').
card_uid('burnout'/'ALL', 'ALL:Burnout:burnout').
card_rarity('burnout'/'ALL', 'Uncommon').
card_artist('burnout'/'ALL', 'Mike Raabe').
card_flavor_text('burnout'/'ALL', '\"GOTCHA!\"\n—Jaya Ballard, Task Mage').
card_multiverse_id('burnout'/'ALL', '3166').

card_in_set('carrier pigeons', 'ALL').
card_original_type('carrier pigeons'/'ALL', 'Summon — Pigeons').
card_original_text('carrier pigeons'/'ALL', 'Flying\nDraw a card at the beginning of the upkeep of the turn after Carrier Pigeons comes into play.').
card_first_print('carrier pigeons', 'ALL').
card_image_name('carrier pigeons'/'ALL', 'carrier pigeons1').
card_uid('carrier pigeons'/'ALL', 'ALL:Carrier Pigeons:carrier pigeons1').
card_rarity('carrier pigeons'/'ALL', 'Common').
card_artist('carrier pigeons'/'ALL', 'Pat Morrissey').
card_flavor_text('carrier pigeons'/'ALL', '\"Birds know no borders! Why, then, should we?\"\n—General Varchild, in a missive to King Darien of Kjeldor').
card_multiverse_id('carrier pigeons'/'ALL', '3191').

card_in_set('carrier pigeons', 'ALL').
card_original_type('carrier pigeons'/'ALL', 'Summon — Pigeons').
card_original_text('carrier pigeons'/'ALL', 'Flying\nDraw a card at the beginning of the upkeep of the turn after Carrier Pigeons comes into play.').
card_image_name('carrier pigeons'/'ALL', 'carrier pigeons2').
card_uid('carrier pigeons'/'ALL', 'ALL:Carrier Pigeons:carrier pigeons2').
card_rarity('carrier pigeons'/'ALL', 'Common').
card_artist('carrier pigeons'/'ALL', 'Pat Morrissey').
card_flavor_text('carrier pigeons'/'ALL', '\"It\'s true I ask no fealty of the flocks—but surely we are more than mere beasts.\" —King Darien of Kjeldor, in a missive to General Varchild').
card_multiverse_id('carrier pigeons'/'ALL', '3190').

card_in_set('casting of bones', 'ALL').
card_original_type('casting of bones'/'ALL', 'Enchant Creature').
card_original_text('casting of bones'/'ALL', 'If enchanted creature is put into the graveyard, draw three cards. Choose and discard one of those cards.').
card_first_print('casting of bones', 'ALL').
card_image_name('casting of bones'/'ALL', 'casting of bones1').
card_uid('casting of bones'/'ALL', 'ALL:Casting of Bones:casting of bones1').
card_rarity('casting of bones'/'ALL', 'Common').
card_artist('casting of bones'/'ALL', 'Anson Maddocks').
card_flavor_text('casting of bones'/'ALL', '\"Only a necromancer would create such a foul form of divination.\"\n—Gerda Äagesdotter,\nArchmage of the Unseen').
card_multiverse_id('casting of bones'/'ALL', '3067').

card_in_set('casting of bones', 'ALL').
card_original_type('casting of bones'/'ALL', 'Enchant Creature').
card_original_text('casting of bones'/'ALL', 'If enchanted creature is put into the graveyard, draw three cards. Choose and discard one of those cards.').
card_image_name('casting of bones'/'ALL', 'casting of bones2').
card_uid('casting of bones'/'ALL', 'ALL:Casting of Bones:casting of bones2').
card_rarity('casting of bones'/'ALL', 'Common').
card_artist('casting of bones'/'ALL', 'Anson Maddocks').
card_flavor_text('casting of bones'/'ALL', '\"Whether whole or not, still we serve Lim-Dûl\'s wishes.\"\n—Chaeska, Keeper of Tresserhorn').
card_multiverse_id('casting of bones'/'ALL', '3068').

card_in_set('chaos harlequin', 'ALL').
card_original_type('chaos harlequin'/'ALL', 'Summon — Harlequin').
card_original_text('chaos harlequin'/'ALL', '{R}: Remove the top card of your library from the game. If that card is a land, Chaos Harlequin gets -4/-0 until end of turn; otherwise, Chaos Harlequin gets +2/+0 until end of turn.').
card_first_print('chaos harlequin', 'ALL').
card_image_name('chaos harlequin'/'ALL', 'chaos harlequin').
card_uid('chaos harlequin'/'ALL', 'ALL:Chaos Harlequin:chaos harlequin').
card_rarity('chaos harlequin'/'ALL', 'Rare').
card_artist('chaos harlequin'/'ALL', 'Alan Rabinowitz').
card_multiverse_id('chaos harlequin'/'ALL', '3167').

card_in_set('contagion', 'ALL').
card_original_type('contagion'/'ALL', 'Instant').
card_original_text('contagion'/'ALL', 'You may pay 1 life and remove a black card in your hand from the game instead of paying Contagion\'s casting cost. Effects that prevent or redirect damage cannot be used to counter this loss of life. Put two -2/-1 counters, distributed any way you choose, on any number of target creatures.').
card_first_print('contagion', 'ALL').
card_image_name('contagion'/'ALL', 'contagion').
card_uid('contagion'/'ALL', 'ALL:Contagion:contagion').
card_rarity('contagion'/'ALL', 'Uncommon').
card_artist('contagion'/'ALL', 'Mike Raabe').
card_multiverse_id('contagion'/'ALL', '3069').

card_in_set('deadly insect', 'ALL').
card_original_type('deadly insect'/'ALL', 'Summon — Insect').
card_original_text('deadly insect'/'ALL', 'Cannot be the target of spells or effects.').
card_first_print('deadly insect', 'ALL').
card_image_name('deadly insect'/'ALL', 'deadly insect1').
card_uid('deadly insect'/'ALL', 'ALL:Deadly Insect:deadly insect1').
card_rarity('deadly insect'/'ALL', 'Common').
card_artist('deadly insect'/'ALL', 'Scott Kirschner').
card_flavor_text('deadly insect'/'ALL', '\"Beautiful, indeed—but one sting could fell a Giant in a heartbeat.\"\n—Taaveti of Kelsinko,\nElvish Hunter').
card_multiverse_id('deadly insect'/'ALL', '3129').

card_in_set('deadly insect', 'ALL').
card_original_type('deadly insect'/'ALL', 'Summon — Insect').
card_original_text('deadly insect'/'ALL', 'Cannot be the target of spells or effects.').
card_image_name('deadly insect'/'ALL', 'deadly insect2').
card_uid('deadly insect'/'ALL', 'ALL:Deadly Insect:deadly insect2').
card_rarity('deadly insect'/'ALL', 'Common').
card_artist('deadly insect'/'ALL', 'Scott Kirschner').
card_flavor_text('deadly insect'/'ALL', '\"There is no time for pain when this beast leaves its terrible sting.\"\n—Jaeuhl Carthalion,\nJuniper Order Advocate').
card_multiverse_id('deadly insect'/'ALL', '3130').

card_in_set('death spark', 'ALL').
card_original_type('death spark'/'ALL', 'Instant').
card_original_text('death spark'/'ALL', 'Death Spark deals 1 damage to target creature or player.\nAt the end of your upkeep, if Death Spark is in your graveyard with a creature card directly above it, you may pay {1} to put Death Spark into your hand.').
card_first_print('death spark', 'ALL').
card_image_name('death spark'/'ALL', 'death spark').
card_uid('death spark'/'ALL', 'ALL:Death Spark:death spark').
card_rarity('death spark'/'ALL', 'Uncommon').
card_artist('death spark'/'ALL', 'Mark Tedin').
card_multiverse_id('death spark'/'ALL', '3168').

card_in_set('diminishing returns', 'ALL').
card_original_type('diminishing returns'/'ALL', 'Sorcery').
card_original_text('diminishing returns'/'ALL', 'Each player shuffles his or her hand and graveyard into his or her library. Remove the top ten cards of your library from the game. Each player draws up to seven cards.').
card_first_print('diminishing returns', 'ALL').
card_image_name('diminishing returns'/'ALL', 'diminishing returns').
card_uid('diminishing returns'/'ALL', 'ALL:Diminishing Returns:diminishing returns').
card_rarity('diminishing returns'/'ALL', 'Rare').
card_artist('diminishing returns'/'ALL', 'L. A. Williams').
card_multiverse_id('diminishing returns'/'ALL', '3104').

card_in_set('diseased vermin', 'ALL').
card_original_type('diseased vermin'/'ALL', 'Summon — Rats').
card_original_text('diseased vermin'/'ALL', 'During your upkeep, Diseased Vermin deals 1 damage to a single target opponent it has previously damaged for each infection counter on Diseased Vermin.\nIf Diseased Vermin damages a player in combat, put an infection counter on it.').
card_first_print('diseased vermin', 'ALL').
card_image_name('diseased vermin'/'ALL', 'diseased vermin').
card_uid('diseased vermin'/'ALL', 'ALL:Diseased Vermin:diseased vermin').
card_rarity('diseased vermin'/'ALL', 'Uncommon').
card_artist('diseased vermin'/'ALL', 'Scott Kirschner').
card_multiverse_id('diseased vermin'/'ALL', '3070').

card_in_set('dystopia', 'ALL').
card_original_type('dystopia'/'ALL', 'Enchantment').
card_original_text('dystopia'/'ALL', 'Cumulative Upkeep: 1 life\nDuring each player\'s upkeep, if that player controls any green or white permanents, he or she sacrifices a green or white permanent.').
card_first_print('dystopia', 'ALL').
card_image_name('dystopia'/'ALL', 'dystopia').
card_uid('dystopia'/'ALL', 'ALL:Dystopia:dystopia').
card_rarity('dystopia'/'ALL', 'Rare').
card_artist('dystopia'/'ALL', 'Ruth Thompson').
card_multiverse_id('dystopia'/'ALL', '3071').

card_in_set('elvish bard', 'ALL').
card_original_type('elvish bard'/'ALL', 'Summon — Elf').
card_original_text('elvish bard'/'ALL', 'All creatures able to block Elvish Bard do so. If this forces a creature to block more attackers than allowed, defending player assigns that creature to block as many of those attackers as allowed.').
card_first_print('elvish bard', 'ALL').
card_image_name('elvish bard'/'ALL', 'elvish bard').
card_uid('elvish bard'/'ALL', 'ALL:Elvish Bard:elvish bard').
card_rarity('elvish bard'/'ALL', 'Uncommon').
card_artist('elvish bard'/'ALL', 'Susan Van Camp').
card_multiverse_id('elvish bard'/'ALL', '3131').

card_in_set('elvish ranger', 'ALL').
card_original_type('elvish ranger'/'ALL', 'Summon — Elf').
card_original_text('elvish ranger'/'ALL', '').
card_first_print('elvish ranger', 'ALL').
card_image_name('elvish ranger'/'ALL', 'elvish ranger1').
card_uid('elvish ranger'/'ALL', 'ALL:Elvish Ranger:elvish ranger1').
card_rarity('elvish ranger'/'ALL', 'Common').
card_artist('elvish ranger'/'ALL', 'Terese Nielsen').
card_flavor_text('elvish ranger'/'ALL', '\"No one may fault an Elvish Ranger\'s heart, but the simplest frailty may fell even the proudest warrior.\"\n—Kaysa, Elder Druid of the Juniper Order').
card_multiverse_id('elvish ranger'/'ALL', '3133').

card_in_set('elvish ranger', 'ALL').
card_original_type('elvish ranger'/'ALL', 'Summon — Elf').
card_original_text('elvish ranger'/'ALL', '').
card_image_name('elvish ranger'/'ALL', 'elvish ranger2').
card_uid('elvish ranger'/'ALL', 'ALL:Elvish Ranger:elvish ranger2').
card_rarity('elvish ranger'/'ALL', 'Common').
card_artist('elvish ranger'/'ALL', 'Terese Nielsen').
card_flavor_text('elvish ranger'/'ALL', '\"How can any Human hope to match our skills? We are children of Fyndhorn, and its sap runs in our veins.\"\n—Taaveti of Kelsinko,\nElvish Hunter').
card_multiverse_id('elvish ranger'/'ALL', '3132').

card_in_set('elvish spirit guide', 'ALL').
card_original_type('elvish spirit guide'/'ALL', 'Summon — Spirit').
card_original_text('elvish spirit guide'/'ALL', 'If Elvish Spirit Guide is in your hand, you may remove it from the game to add {G} to your mana pool. Play this ability as an interrupt.').
card_first_print('elvish spirit guide', 'ALL').
card_image_name('elvish spirit guide'/'ALL', 'elvish spirit guide').
card_uid('elvish spirit guide'/'ALL', 'ALL:Elvish Spirit Guide:elvish spirit guide').
card_rarity('elvish spirit guide'/'ALL', 'Uncommon').
card_artist('elvish spirit guide'/'ALL', 'Julie Baroh').
card_flavor_text('elvish spirit guide'/'ALL', '\"We are never without guidance, if we but seek it.\"\n—Taaveti of Kelsinko, Elvish Hunter').
card_multiverse_id('elvish spirit guide'/'ALL', '3134').

card_in_set('energy arc', 'ALL').
card_original_type('energy arc'/'ALL', 'Instant').
card_original_text('energy arc'/'ALL', 'Untap any number of target creatures. Those creatures neither deal nor receive damage in combat this turn.').
card_first_print('energy arc', 'ALL').
card_image_name('energy arc'/'ALL', 'energy arc').
card_uid('energy arc'/'ALL', 'ALL:Energy Arc:energy arc').
card_rarity('energy arc'/'ALL', 'Uncommon').
card_artist('energy arc'/'ALL', 'Terese Nielsen').
card_flavor_text('energy arc'/'ALL', '\"Relent, and you may transcend your situation.\"\n—Gerda Äagesdotter,\nArchmage of the Unseen').
card_multiverse_id('energy arc'/'ALL', '3221').

card_in_set('enslaved scout', 'ALL').
card_original_type('enslaved scout'/'ALL', 'Summon — Goblin').
card_original_text('enslaved scout'/'ALL', '{2}: Mountainwalk until end of turn').
card_first_print('enslaved scout', 'ALL').
card_image_name('enslaved scout'/'ALL', 'enslaved scout1').
card_uid('enslaved scout'/'ALL', 'ALL:Enslaved Scout:enslaved scout1').
card_rarity('enslaved scout'/'ALL', 'Common').
card_artist('enslaved scout'/'ALL', 'Rebecca Guay').
card_flavor_text('enslaved scout'/'ALL', '\"Her soldiers\' hatred for the Goblins is tempered only by their need for what the Goblins alone hold: knowledge of their mountain home.\"\n—King Darien of Kjeldor').
card_multiverse_id('enslaved scout'/'ALL', '3169').

card_in_set('enslaved scout', 'ALL').
card_original_type('enslaved scout'/'ALL', 'Summon — Goblin').
card_original_text('enslaved scout'/'ALL', '{2}: Mountainwalk until end of turn').
card_image_name('enslaved scout'/'ALL', 'enslaved scout2').
card_uid('enslaved scout'/'ALL', 'ALL:Enslaved Scout:enslaved scout2').
card_rarity('enslaved scout'/'ALL', 'Common').
card_artist('enslaved scout'/'ALL', 'Rebecca Guay').
card_flavor_text('enslaved scout'/'ALL', '\"Even the basest creatures may serve a purpose. Still, their lives need be only as long as the paths we tread.\"\n—General Varchild').
card_multiverse_id('enslaved scout'/'ALL', '3170').

card_in_set('errand of duty', 'ALL').
card_original_type('errand of duty'/'ALL', 'Instant').
card_original_text('errand of duty'/'ALL', 'Put a Knight token into play. Treat this token as a 1/1 white creature with banding.').
card_first_print('errand of duty', 'ALL').
card_image_name('errand of duty'/'ALL', 'errand of duty1').
card_uid('errand of duty'/'ALL', 'ALL:Errand of Duty:errand of duty1').
card_rarity('errand of duty'/'ALL', 'Common').
card_artist('errand of duty'/'ALL', 'Julie Baroh').
card_flavor_text('errand of duty'/'ALL', '\"Call every able sword to our side. Kjeldor will stand triumphant\"\n—King Darien of Kjeldor').
card_multiverse_id('errand of duty'/'ALL', '3193').

card_in_set('errand of duty', 'ALL').
card_original_type('errand of duty'/'ALL', 'Instant').
card_original_text('errand of duty'/'ALL', 'Put a Knight token into play. Treat this token as a 1/1 white creature with banding.').
card_image_name('errand of duty'/'ALL', 'errand of duty2').
card_uid('errand of duty'/'ALL', 'ALL:Errand of Duty:errand of duty2').
card_rarity('errand of duty'/'ALL', 'Common').
card_artist('errand of duty'/'ALL', 'Julie Baroh').
card_flavor_text('errand of duty'/'ALL', '\"May they speed to their task, for the Skyknights alone cannot hold Kjeldor safe.\"\n—Arna Kennerüd, Skycaptain').
card_multiverse_id('errand of duty'/'ALL', '3192').

card_in_set('exile', 'ALL').
card_original_type('exile'/'ALL', 'Instant').
card_original_text('exile'/'ALL', 'Remove target non-white attacking creature from the game. Gain life equal to that creature\'s toughness.').
card_first_print('exile', 'ALL').
card_image_name('exile'/'ALL', 'exile').
card_uid('exile'/'ALL', 'ALL:Exile:exile').
card_rarity('exile'/'ALL', 'Rare').
card_artist('exile'/'ALL', 'Rob Alexander').
card_multiverse_id('exile'/'ALL', '3194').

card_in_set('false demise', 'ALL').
card_original_type('false demise'/'ALL', 'Enchant Creature').
card_original_text('false demise'/'ALL', 'If enchanted creature is put into the graveyard, return that creature to play under your control as though it were just cast.').
card_first_print('false demise', 'ALL').
card_image_name('false demise'/'ALL', 'false demise1').
card_uid('false demise'/'ALL', 'ALL:False Demise:false demise1').
card_rarity('false demise'/'ALL', 'Common').
card_artist('false demise'/'ALL', 'Randy Gallegos').
card_flavor_text('false demise'/'ALL', '\"Far too many of our missing ‘dead\' seem to be turning up in Varchild\'s ranks\"\n—King Darien of Kjeldor').
card_multiverse_id('false demise'/'ALL', '3106').

card_in_set('false demise', 'ALL').
card_original_type('false demise'/'ALL', 'Enchant Creature').
card_original_text('false demise'/'ALL', 'If enchanted creature is put into the graveyard, return that creature to play under your control as though it were just cast.').
card_image_name('false demise'/'ALL', 'false demise2').
card_uid('false demise'/'ALL', 'ALL:False Demise:false demise2').
card_rarity('false demise'/'ALL', 'Common').
card_artist('false demise'/'ALL', 'Randy Gallegos').
card_flavor_text('false demise'/'ALL', '\"Fool Never believe they\'re dead until you see the body\"\n—General Varchild').
card_multiverse_id('false demise'/'ALL', '3105').

card_in_set('fatal lore', 'ALL').
card_original_type('fatal lore'/'ALL', 'Sorcery').
card_original_text('fatal lore'/'ALL', 'Target opponent chooses one: you draw three cards; or you choose and bury up to two target creatures that opponent controls and he or she draws up to three cards.').
card_first_print('fatal lore', 'ALL').
card_image_name('fatal lore'/'ALL', 'fatal lore').
card_uid('fatal lore'/'ALL', 'ALL:Fatal Lore:fatal lore').
card_rarity('fatal lore'/'ALL', 'Rare').
card_artist('fatal lore'/'ALL', 'Lawrence Snelly').
card_flavor_text('fatal lore'/'ALL', '\"All knowledge has its price.\"\n—Gerda Äagesdotter,\nArchmage of the Unseen').
card_multiverse_id('fatal lore'/'ALL', '3072').

card_in_set('feast or famine', 'ALL').
card_original_type('feast or famine'/'ALL', 'Instant').
card_original_text('feast or famine'/'ALL', 'Bury target non-black, non-artifact creature or put a Zombie token into play. Treat this token as a 2/2 black creature.').
card_first_print('feast or famine', 'ALL').
card_image_name('feast or famine'/'ALL', 'feast or famine1').
card_uid('feast or famine'/'ALL', 'ALL:Feast or Famine:feast or famine1').
card_rarity('feast or famine'/'ALL', 'Common').
card_artist('feast or famine'/'ALL', 'Pete Venters').
card_flavor_text('feast or famine'/'ALL', '\"We are not yet free of Lim-Dûl\'s terrors.\"\n—Halvor Arensson, Kjeldoran Priest').
card_multiverse_id('feast or famine'/'ALL', '3074').

card_in_set('feast or famine', 'ALL').
card_original_type('feast or famine'/'ALL', 'Instant').
card_original_text('feast or famine'/'ALL', 'Bury target non-black, non-artifact creature or put a Zombie token into play. Treat this token as a 2/2 black creature.').
card_image_name('feast or famine'/'ALL', 'feast or famine2').
card_uid('feast or famine'/'ALL', 'ALL:Feast or Famine:feast or famine2').
card_rarity('feast or famine'/'ALL', 'Common').
card_artist('feast or famine'/'ALL', 'Pete Venters').
card_flavor_text('feast or famine'/'ALL', '\"The living cannot understand the benefits of death.\"\n—Chaeska, Keeper of Tresserhorn').
card_multiverse_id('feast or famine'/'ALL', '3073').

card_in_set('fevered strength', 'ALL').
card_original_type('fevered strength'/'ALL', 'Instant').
card_original_text('fevered strength'/'ALL', 'Target creature gets +2/+0 until end of turn.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_first_print('fevered strength', 'ALL').
card_image_name('fevered strength'/'ALL', 'fevered strength1').
card_uid('fevered strength'/'ALL', 'ALL:Fevered Strength:fevered strength1').
card_rarity('fevered strength'/'ALL', 'Common').
card_artist('fevered strength'/'ALL', 'Brian Snõddy').
card_flavor_text('fevered strength'/'ALL', '\"The burst of strength brought on by this plague should not be mistaken for a sign of renewed health.\"\n—Kolbjörn, High Honored Druid').
card_multiverse_id('fevered strength'/'ALL', '3075').

card_in_set('fevered strength', 'ALL').
card_original_type('fevered strength'/'ALL', 'Instant').
card_original_text('fevered strength'/'ALL', 'Target creature gets +2/+0 until end of turn.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_image_name('fevered strength'/'ALL', 'fevered strength2').
card_uid('fevered strength'/'ALL', 'ALL:Fevered Strength:fevered strength2').
card_rarity('fevered strength'/'ALL', 'Common').
card_artist('fevered strength'/'ALL', 'Brian Snõddy').
card_flavor_text('fevered strength'/'ALL', '\"The fever clouds the mind as it energizes the body. Both will pass in time.\"\n—Halvor Arensson, Kjeldoran Priest').
card_multiverse_id('fevered strength'/'ALL', '3076').

card_in_set('floodwater dam', 'ALL').
card_original_type('floodwater dam'/'ALL', 'Artifact').
card_original_text('floodwater dam'/'ALL', '{X}{X}{1}, {T}: Tap X target lands.').
card_first_print('floodwater dam', 'ALL').
card_image_name('floodwater dam'/'ALL', 'floodwater dam').
card_uid('floodwater dam'/'ALL', 'ALL:Floodwater Dam:floodwater dam').
card_rarity('floodwater dam'/'ALL', 'Rare').
card_artist('floodwater dam'/'ALL', 'Randy Gallegos').
card_flavor_text('floodwater dam'/'ALL', 'Viscerid dams may be of living creatures as well as bones and mud.').
card_multiverse_id('floodwater dam'/'ALL', '3045').

card_in_set('force of will', 'ALL').
card_original_type('force of will'/'ALL', 'Interrupt').
card_original_text('force of will'/'ALL', 'You may pay 1 life and remove a blue card in your hand from the game instead of paying Force of Will\'s casting cost. Effects that prevent or redirect damage cannot be used to counter this loss of life.\nCounter target spell.').
card_first_print('force of will', 'ALL').
card_image_name('force of will'/'ALL', 'force of will').
card_uid('force of will'/'ALL', 'ALL:Force of Will:force of will').
card_rarity('force of will'/'ALL', 'Uncommon').
card_artist('force of will'/'ALL', 'Terese Nielsen').
card_multiverse_id('force of will'/'ALL', '3107').

card_in_set('foresight', 'ALL').
card_original_type('foresight'/'ALL', 'Sorcery').
card_original_text('foresight'/'ALL', 'Search your library for any three cards and remove them from the game. Shuffle your library afterwards.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_first_print('foresight', 'ALL').
card_image_name('foresight'/'ALL', 'foresight1').
card_uid('foresight'/'ALL', 'ALL:Foresight:foresight1').
card_rarity('foresight'/'ALL', 'Common').
card_artist('foresight'/'ALL', 'Terese Nielsen').
card_multiverse_id('foresight'/'ALL', '3108').

card_in_set('foresight', 'ALL').
card_original_type('foresight'/'ALL', 'Sorcery').
card_original_text('foresight'/'ALL', 'Search your library for any three cards and remove them from the game. Shuffle your library afterwards.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_image_name('foresight'/'ALL', 'foresight2').
card_uid('foresight'/'ALL', 'ALL:Foresight:foresight2').
card_rarity('foresight'/'ALL', 'Common').
card_artist('foresight'/'ALL', 'Terese Nielsen').
card_multiverse_id('foresight'/'ALL', '3109').

card_in_set('fyndhorn druid', 'ALL').
card_original_type('fyndhorn druid'/'ALL', 'Summon — Druid').
card_original_text('fyndhorn druid'/'ALL', 'If Fyndhorn Druid is put into the graveyard the same turn it was blocked, gain 4 life.').
card_first_print('fyndhorn druid', 'ALL').
card_image_name('fyndhorn druid'/'ALL', 'fyndhorn druid1').
card_uid('fyndhorn druid'/'ALL', 'ALL:Fyndhorn Druid:fyndhorn druid1').
card_rarity('fyndhorn druid'/'ALL', 'Common').
card_artist('fyndhorn druid'/'ALL', 'Rob Alexander').
card_flavor_text('fyndhorn druid'/'ALL', '\"All struggles ultimately require sacrifice. It\'s just that some sacrifices are dearer than others.\"\n—Jaeuhl Carthalion,\nJuniper Order Advocate').
card_multiverse_id('fyndhorn druid'/'ALL', '3136').

card_in_set('fyndhorn druid', 'ALL').
card_original_type('fyndhorn druid'/'ALL', 'Summon — Druid').
card_original_text('fyndhorn druid'/'ALL', 'If Fyndhorn Druid is put into the graveyard the same turn it was blocked, gain 4 life.').
card_image_name('fyndhorn druid'/'ALL', 'fyndhorn druid2').
card_uid('fyndhorn druid'/'ALL', 'ALL:Fyndhorn Druid:fyndhorn druid2').
card_rarity('fyndhorn druid'/'ALL', 'Common').
card_artist('fyndhorn druid'/'ALL', 'Rob Alexander').
card_flavor_text('fyndhorn druid'/'ALL', '\"Honor those who honor Fyndhorn above themselves.\"\n—Kolbjörn, High Honored Druid').
card_multiverse_id('fyndhorn druid'/'ALL', '3135').

card_in_set('gargantuan gorilla', 'ALL').
card_original_type('gargantuan gorilla'/'ALL', 'Summon — Gorilla').
card_original_text('gargantuan gorilla'/'ALL', 'During your upkeep, sacrifice a forest, or bury Gargantuan Gorilla and Gargantuan Gorilla deals 7 damage to you. If you sacrifice a snow-covered forest in this way, Gargantuan Gorilla gains trample until end of turn.\n{T}: Gargantuan Gorilla deals an amount of damage equal to its power to any other target creature. That creature deals an amount of damage equal to its power to Gargantuan Gorilla.').
card_first_print('gargantuan gorilla', 'ALL').
card_image_name('gargantuan gorilla'/'ALL', 'gargantuan gorilla').
card_uid('gargantuan gorilla'/'ALL', 'ALL:Gargantuan Gorilla:gargantuan gorilla').
card_rarity('gargantuan gorilla'/'ALL', 'Rare').
card_artist('gargantuan gorilla'/'ALL', 'Greg Simanson').
card_multiverse_id('gargantuan gorilla'/'ALL', '3137').

card_in_set('gift of the woods', 'ALL').
card_original_type('gift of the woods'/'ALL', 'Enchant Creature').
card_original_text('gift of the woods'/'ALL', 'If enchanted creature blocks or is blocked by any creatures, enchanted creature gets +0/+3 until end of turn and you gain 1 life.').
card_first_print('gift of the woods', 'ALL').
card_image_name('gift of the woods'/'ALL', 'gift of the woods1').
card_uid('gift of the woods'/'ALL', 'ALL:Gift of the Woods:gift of the woods1').
card_rarity('gift of the woods'/'ALL', 'Common').
card_artist('gift of the woods'/'ALL', 'Susan Van Camp').
card_multiverse_id('gift of the woods'/'ALL', '3138').

card_in_set('gift of the woods', 'ALL').
card_original_type('gift of the woods'/'ALL', 'Enchant Creature').
card_original_text('gift of the woods'/'ALL', 'If enchanted creature blocks or is blocked by any creatures, enchanted creature gets +0/+3 until end of turn and you gain 1 life.').
card_image_name('gift of the woods'/'ALL', 'gift of the woods2').
card_uid('gift of the woods'/'ALL', 'ALL:Gift of the Woods:gift of the woods2').
card_rarity('gift of the woods'/'ALL', 'Common').
card_artist('gift of the woods'/'ALL', 'Susan Van Camp').
card_multiverse_id('gift of the woods'/'ALL', '3139').

card_in_set('gorilla berserkers', 'ALL').
card_original_type('gorilla berserkers'/'ALL', 'Summon — Gorillas').
card_original_text('gorilla berserkers'/'ALL', 'Trample, rampage: 2\nCannot be blocked by fewer than three creatures.').
card_first_print('gorilla berserkers', 'ALL').
card_image_name('gorilla berserkers'/'ALL', 'gorilla berserkers1').
card_uid('gorilla berserkers'/'ALL', 'ALL:Gorilla Berserkers:gorilla berserkers1').
card_rarity('gorilla berserkers'/'ALL', 'Common').
card_artist('gorilla berserkers'/'ALL', 'John Matson').
card_flavor_text('gorilla berserkers'/'ALL', '\"Their fury is their greatest weapon.\"\n—Taaveti of Kelsinko, Elvish Hunter').
card_multiverse_id('gorilla berserkers'/'ALL', '3140').

card_in_set('gorilla berserkers', 'ALL').
card_original_type('gorilla berserkers'/'ALL', 'Summon — Gorillas').
card_original_text('gorilla berserkers'/'ALL', 'Trample, rampage: 2\nCannot be blocked by fewer than three creatures.').
card_image_name('gorilla berserkers'/'ALL', 'gorilla berserkers2').
card_uid('gorilla berserkers'/'ALL', 'ALL:Gorilla Berserkers:gorilla berserkers2').
card_rarity('gorilla berserkers'/'ALL', 'Common').
card_artist('gorilla berserkers'/'ALL', 'John Matson').
card_flavor_text('gorilla berserkers'/'ALL', '\"Beneath serenity may lie\n a hidden rage.\"\n—Jaeuhl Carthalion,\nJuniper Order Advocate').
card_multiverse_id('gorilla berserkers'/'ALL', '3141').

card_in_set('gorilla chieftain', 'ALL').
card_original_type('gorilla chieftain'/'ALL', 'Summon — Gorilla').
card_original_text('gorilla chieftain'/'ALL', '{1}{G}: Regenerate').
card_first_print('gorilla chieftain', 'ALL').
card_image_name('gorilla chieftain'/'ALL', 'gorilla chieftain1').
card_uid('gorilla chieftain'/'ALL', 'ALL:Gorilla Chieftain:gorilla chieftain1').
card_rarity('gorilla chieftain'/'ALL', 'Common').
card_artist('gorilla chieftain'/'ALL', 'Quinton Hoover').
card_flavor_text('gorilla chieftain'/'ALL', '\"Oh, no—not you again?\"\n—Jaya Ballard, Task Mage').
card_multiverse_id('gorilla chieftain'/'ALL', '3143').

card_in_set('gorilla chieftain', 'ALL').
card_original_type('gorilla chieftain'/'ALL', 'Summon — Gorilla').
card_original_text('gorilla chieftain'/'ALL', '{1}{G}: Regenerate').
card_image_name('gorilla chieftain'/'ALL', 'gorilla chieftain2').
card_uid('gorilla chieftain'/'ALL', 'ALL:Gorilla Chieftain:gorilla chieftain2').
card_rarity('gorilla chieftain'/'ALL', 'Common').
card_artist('gorilla chieftain'/'ALL', 'Quinton Hoover').
card_flavor_text('gorilla chieftain'/'ALL', '\"In the soil of leadership sprout the seeds of immortality.\"\n—Kaysa, Elder Druid of the Juniper Order').
card_multiverse_id('gorilla chieftain'/'ALL', '3142').

card_in_set('gorilla shaman', 'ALL').
card_original_type('gorilla shaman'/'ALL', 'Summon — Gorilla').
card_original_text('gorilla shaman'/'ALL', '{X}{X}{1}: Destroy target non-creature artifact with casting cost equal to X.').
card_first_print('gorilla shaman', 'ALL').
card_image_name('gorilla shaman'/'ALL', 'gorilla shaman1').
card_uid('gorilla shaman'/'ALL', 'ALL:Gorilla Shaman:gorilla shaman1').
card_rarity('gorilla shaman'/'ALL', 'Common').
card_artist('gorilla shaman'/'ALL', 'Anthony Waters').
card_flavor_text('gorilla shaman'/'ALL', '\"Frankly, destruction is best left to professionals.\"\n—Jaya Ballard, Task Mage').
card_multiverse_id('gorilla shaman'/'ALL', '3171').

card_in_set('gorilla shaman', 'ALL').
card_original_type('gorilla shaman'/'ALL', 'Summon — Gorilla').
card_original_text('gorilla shaman'/'ALL', '{X}{X}{1}: Destroy target non-creature artifact with casting cost equal to X.').
card_image_name('gorilla shaman'/'ALL', 'gorilla shaman2').
card_uid('gorilla shaman'/'ALL', 'ALL:Gorilla Shaman:gorilla shaman2').
card_rarity('gorilla shaman'/'ALL', 'Common').
card_artist('gorilla shaman'/'ALL', 'Anthony Waters').
card_flavor_text('gorilla shaman'/'ALL', '\"Each generation teaches the next that artifice is the enemy of natural order.\"\n—Kaysa, Elder Druid of the Juniper Order').
card_multiverse_id('gorilla shaman'/'ALL', '3172').

card_in_set('gorilla war cry', 'ALL').
card_original_type('gorilla war cry'/'ALL', 'Instant').
card_original_text('gorilla war cry'/'ALL', 'Attacking creatures cannot be blocked by only one creature this turn. Play only during combat before defense is chosen.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_first_print('gorilla war cry', 'ALL').
card_image_name('gorilla war cry'/'ALL', 'gorilla war cry1').
card_uid('gorilla war cry'/'ALL', 'ALL:Gorilla War Cry:gorilla war cry1').
card_rarity('gorilla war cry'/'ALL', 'Common').
card_artist('gorilla war cry'/'ALL', 'Bryon Wackwitz').
card_flavor_text('gorilla war cry'/'ALL', '\"The only ‘art\' these beasts possess is the art of noise!\" —Jaya Ballard').
card_multiverse_id('gorilla war cry'/'ALL', '3173').

card_in_set('gorilla war cry', 'ALL').
card_original_type('gorilla war cry'/'ALL', 'Instant').
card_original_text('gorilla war cry'/'ALL', 'Attacking creatures cannot be blocked by only one creature this turn. Play only during combat before defense is chosen.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_image_name('gorilla war cry'/'ALL', 'gorilla war cry2').
card_uid('gorilla war cry'/'ALL', 'ALL:Gorilla War Cry:gorilla war cry2').
card_rarity('gorilla war cry'/'ALL', 'Common').
card_artist('gorilla war cry'/'ALL', 'Bryon Wackwitz').
card_flavor_text('gorilla war cry'/'ALL', '\"Theirs is the art of blood.\" —Jaeuhl Carthalion, Juniper Order Advocate').
card_multiverse_id('gorilla war cry'/'ALL', '3174').

card_in_set('guerrilla tactics', 'ALL').
card_original_type('guerrilla tactics'/'ALL', 'Instant').
card_original_text('guerrilla tactics'/'ALL', 'Guerrilla Tactics deals 2 damage to target creature or player.\nIf a spell or effect controlled by an opponent causes you to discard Guerrilla Tactics from your hand, reveal Guerrilla Tactics to all players, and it deals 4 damage to target creature or player.').
card_first_print('guerrilla tactics', 'ALL').
card_image_name('guerrilla tactics'/'ALL', 'guerrilla tactics1').
card_uid('guerrilla tactics'/'ALL', 'ALL:Guerrilla Tactics:guerrilla tactics1').
card_rarity('guerrilla tactics'/'ALL', 'Common').
card_artist('guerrilla tactics'/'ALL', 'Randy Asplund-Faith').
card_multiverse_id('guerrilla tactics'/'ALL', '3176').

card_in_set('guerrilla tactics', 'ALL').
card_original_type('guerrilla tactics'/'ALL', 'Instant').
card_original_text('guerrilla tactics'/'ALL', 'Guerrilla Tactics deals 2 damage to target creature or player.\nIf a spell or effect controlled by an opponent causes you to discard Guerrilla Tactics from your hand, reveal Guerrilla Tactics to all players, and it deals 4 damage to target creature or player.').
card_image_name('guerrilla tactics'/'ALL', 'guerrilla tactics2').
card_uid('guerrilla tactics'/'ALL', 'ALL:Guerrilla Tactics:guerrilla tactics2').
card_rarity('guerrilla tactics'/'ALL', 'Common').
card_artist('guerrilla tactics'/'ALL', 'Randy Asplund-Faith').
card_multiverse_id('guerrilla tactics'/'ALL', '3175').

card_in_set('gustha\'s scepter', 'ALL').
card_original_type('gustha\'s scepter'/'ALL', 'Artifact').
card_original_text('gustha\'s scepter'/'ALL', 'If Gustha\'s Scepter leaves play or you lose control of it, put all cards under Gustha\'s Scepter into your graveyard.\n{T}: Put any card from your hand face down under Gustha\'s Scepter. You may look at that card at any time.\n{T}: Return any card under Gustha\'s Scepter to your hand.').
card_first_print('gustha\'s scepter', 'ALL').
card_image_name('gustha\'s scepter'/'ALL', 'gustha\'s scepter').
card_uid('gustha\'s scepter'/'ALL', 'ALL:Gustha\'s Scepter:gustha\'s scepter').
card_rarity('gustha\'s scepter'/'ALL', 'Rare').
card_artist('gustha\'s scepter'/'ALL', 'Sandra Everingham').
card_multiverse_id('gustha\'s scepter'/'ALL', '3046').

card_in_set('hail storm', 'ALL').
card_original_type('hail storm'/'ALL', 'Instant').
card_original_text('hail storm'/'ALL', 'Hail Storm deals 2 damage to each attacking creature and 1 damage to you and each creature you control.').
card_first_print('hail storm', 'ALL').
card_image_name('hail storm'/'ALL', 'hail storm').
card_uid('hail storm'/'ALL', 'ALL:Hail Storm:hail storm').
card_rarity('hail storm'/'ALL', 'Uncommon').
card_artist('hail storm'/'ALL', 'Jeff A. Menges').
card_flavor_text('hail storm'/'ALL', '\"If they can\'t take the hail, they\'d better stay home.\"\n—Arna Kennerüd, Skycaptain').
card_multiverse_id('hail storm'/'ALL', '3144').

card_in_set('heart of yavimaya', 'ALL').
card_original_type('heart of yavimaya'/'ALL', 'Land').
card_original_text('heart of yavimaya'/'ALL', 'When Heart of Yavimaya comes into play, sacrifice a forest or bury Heart of Yavimaya.\n{T}: Add {G} to your mana pool.\n{T}: Target creature gets +1/+1 until end of turn.').
card_first_print('heart of yavimaya', 'ALL').
card_image_name('heart of yavimaya'/'ALL', 'heart of yavimaya').
card_uid('heart of yavimaya'/'ALL', 'ALL:Heart of Yavimaya:heart of yavimaya').
card_rarity('heart of yavimaya'/'ALL', 'Rare').
card_artist('heart of yavimaya'/'ALL', 'Pete Venters').
card_multiverse_id('heart of yavimaya'/'ALL', '3232').

card_in_set('helm of obedience', 'ALL').
card_original_type('helm of obedience'/'ALL', 'Artifact').
card_original_text('helm of obedience'/'ALL', '{X}, {T}: Put the top card of target opponent\'s library into his or her graveyard. Continue doing this until you have put X cards or a creature card into that graveyard, whichever occurs first. If the last card put into the graveyard is a creature card, bury Helm of Obedience and put that creature into play under your control as though it were just cast. X cannot be equal to 0.').
card_first_print('helm of obedience', 'ALL').
card_image_name('helm of obedience'/'ALL', 'helm of obedience').
card_uid('helm of obedience'/'ALL', 'ALL:Helm of Obedience:helm of obedience').
card_rarity('helm of obedience'/'ALL', 'Rare').
card_artist('helm of obedience'/'ALL', 'Brian Snõddy').
card_multiverse_id('helm of obedience'/'ALL', '3047').

card_in_set('inheritance', 'ALL').
card_original_type('inheritance'/'ALL', 'Enchantment').
card_original_text('inheritance'/'ALL', '{3}: Draw a card. Use this ability only when a creature is put into the graveyard from play, and only once for each creature put into the graveyard.').
card_first_print('inheritance', 'ALL').
card_image_name('inheritance'/'ALL', 'inheritance').
card_uid('inheritance'/'ALL', 'ALL:Inheritance:inheritance').
card_rarity('inheritance'/'ALL', 'Uncommon').
card_artist('inheritance'/'ALL', 'Kaja Foglio').
card_flavor_text('inheritance'/'ALL', '\"More than lessons may be gained from the past.\"\n—Halvor Arensson, Kjeldoran Priest').
card_multiverse_id('inheritance'/'ALL', '3195').

card_in_set('insidious bookworms', 'ALL').
card_original_type('insidious bookworms'/'ALL', 'Summon — Worms').
card_original_text('insidious bookworms'/'ALL', '{1}{B}: Target player discards a card at random from his or her hand. Use this ability only when Insidious Bookworms is put into the graveyard from play. You cannot spend more than {1}{B} in this way each turn.').
card_first_print('insidious bookworms', 'ALL').
card_image_name('insidious bookworms'/'ALL', 'insidious bookworms1').
card_uid('insidious bookworms'/'ALL', 'ALL:Insidious Bookworms:insidious bookworms1').
card_rarity('insidious bookworms'/'ALL', 'Common').
card_artist('insidious bookworms'/'ALL', 'Greg Simanson').
card_multiverse_id('insidious bookworms'/'ALL', '3077').

card_in_set('insidious bookworms', 'ALL').
card_original_type('insidious bookworms'/'ALL', 'Summon — Worms').
card_original_text('insidious bookworms'/'ALL', '{1}{B}: Target player discards a card at random from his or her hand. Use this ability only when Insidious Bookworms is put into the graveyard from play. You cannot spend more than {1}{B} in this way each turn.').
card_image_name('insidious bookworms'/'ALL', 'insidious bookworms2').
card_uid('insidious bookworms'/'ALL', 'ALL:Insidious Bookworms:insidious bookworms2').
card_rarity('insidious bookworms'/'ALL', 'Common').
card_artist('insidious bookworms'/'ALL', 'Greg Simanson').
card_multiverse_id('insidious bookworms'/'ALL', '3078').

card_in_set('ivory gargoyle', 'ALL').
card_original_type('ivory gargoyle'/'ALL', 'Summon — Gargoyle').
card_original_text('ivory gargoyle'/'ALL', 'Flying\nIf Ivory Gargoyle is put into the graveyard from play, put it into play under owner\'s control at end of turn and skip your next draw phase.\n{4}{W}: Remove Ivory Gargoyle from the game.').
card_first_print('ivory gargoyle', 'ALL').
card_image_name('ivory gargoyle'/'ALL', 'ivory gargoyle').
card_uid('ivory gargoyle'/'ALL', 'ALL:Ivory Gargoyle:ivory gargoyle').
card_rarity('ivory gargoyle'/'ALL', 'Rare').
card_artist('ivory gargoyle'/'ALL', 'Quinton Hoover').
card_multiverse_id('ivory gargoyle'/'ALL', '3196').

card_in_set('juniper order advocate', 'ALL').
card_original_type('juniper order advocate'/'ALL', 'Summon — Knight').
card_original_text('juniper order advocate'/'ALL', 'As long as Juniper Order Advocate is untapped, all green creatures you control get +1/+1.').
card_first_print('juniper order advocate', 'ALL').
card_image_name('juniper order advocate'/'ALL', 'juniper order advocate').
card_uid('juniper order advocate'/'ALL', 'ALL:Juniper Order Advocate:juniper order advocate').
card_rarity('juniper order advocate'/'ALL', 'Uncommon').
card_artist('juniper order advocate'/'ALL', 'Douglas Shuler').
card_flavor_text('juniper order advocate'/'ALL', '\"In these troubled times, we alone carry the hope of reconciliation between Elf and Human.\"\n—Jaeuhl Carthalion,\nJuniper Order Advocate').
card_multiverse_id('juniper order advocate'/'ALL', '3197').

card_in_set('kaysa', 'ALL').
card_original_type('kaysa'/'ALL', 'Summon — Legend').
card_original_text('kaysa'/'ALL', 'All green creatures you control get +1/+1.').
card_first_print('kaysa', 'ALL').
card_image_name('kaysa'/'ALL', 'kaysa').
card_uid('kaysa'/'ALL', 'ALL:Kaysa:kaysa').
card_rarity('kaysa'/'ALL', 'Rare').
card_artist('kaysa'/'ALL', 'Rebecca Guay').
card_flavor_text('kaysa'/'ALL', 'Kaysa speaks as the Elder Druid, but the Yavimaya recognizes only one voice: its own.').
card_multiverse_id('kaysa'/'ALL', '3145').

card_in_set('keeper of tresserhorn', 'ALL').
card_original_type('keeper of tresserhorn'/'ALL', 'Summon — Keeper').
card_original_text('keeper of tresserhorn'/'ALL', 'If Keeper of Tresserhorn attacks and is not blocked, it deals no damage to defending player this turn and that player loses 2 life. Effects that prevent or redirect damage cannot be used to counter this loss of life.').
card_first_print('keeper of tresserhorn', 'ALL').
card_image_name('keeper of tresserhorn'/'ALL', 'keeper of tresserhorn').
card_uid('keeper of tresserhorn'/'ALL', 'ALL:Keeper of Tresserhorn:keeper of tresserhorn').
card_rarity('keeper of tresserhorn'/'ALL', 'Rare').
card_artist('keeper of tresserhorn'/'ALL', 'Z. Plucinski & D. Alexander Gregory').
card_multiverse_id('keeper of tresserhorn'/'ALL', '3079').

card_in_set('kjeldoran escort', 'ALL').
card_original_type('kjeldoran escort'/'ALL', 'Summon — Soldier').
card_original_text('kjeldoran escort'/'ALL', 'Banding').
card_first_print('kjeldoran escort', 'ALL').
card_image_name('kjeldoran escort'/'ALL', 'kjeldoran escort1').
card_uid('kjeldoran escort'/'ALL', 'ALL:Kjeldoran Escort:kjeldoran escort1').
card_rarity('kjeldoran escort'/'ALL', 'Common').
card_artist('kjeldoran escort'/'ALL', 'Bryon Wackwitz').
card_flavor_text('kjeldoran escort'/'ALL', '\"We willingly trade with Kjeldor, but the peace we build must come from both our lands.\"\n—Lovisa Coldeyes,\nBalduvian Chieftain').
card_multiverse_id('kjeldoran escort'/'ALL', '3199').

card_in_set('kjeldoran escort', 'ALL').
card_original_type('kjeldoran escort'/'ALL', 'Summon — Soldier').
card_original_text('kjeldoran escort'/'ALL', 'Banding').
card_image_name('kjeldoran escort'/'ALL', 'kjeldoran escort2').
card_uid('kjeldoran escort'/'ALL', 'ALL:Kjeldoran Escort:kjeldoran escort2').
card_rarity('kjeldoran escort'/'ALL', 'Common').
card_artist('kjeldoran escort'/'ALL', 'Bryon Wackwitz').
card_flavor_text('kjeldoran escort'/'ALL', '\"The first caravan has passed safely through the Karplusan Mountains without incident, forging a trade route to Balduvia. Kjeld himself would never have believed it\"\n—King Darien of Kjeldor').
card_multiverse_id('kjeldoran escort'/'ALL', '3198').

card_in_set('kjeldoran home guard', 'ALL').
card_original_type('kjeldoran home guard'/'ALL', 'Summon — Soldier').
card_original_text('kjeldoran home guard'/'ALL', 'At the end of any combat in which Kjeldoran Home Guard attacked or blocked, put a -0/-1 counter on Kjeldoran Home Guard and put a Deserter token into play. Treat this token as a 0/1 white creature.').
card_first_print('kjeldoran home guard', 'ALL').
card_image_name('kjeldoran home guard'/'ALL', 'kjeldoran home guard').
card_uid('kjeldoran home guard'/'ALL', 'ALL:Kjeldoran Home Guard:kjeldoran home guard').
card_rarity('kjeldoran home guard'/'ALL', 'Uncommon').
card_artist('kjeldoran home guard'/'ALL', 'Andi Rusu').
card_multiverse_id('kjeldoran home guard'/'ALL', '3200').

card_in_set('kjeldoran outpost', 'ALL').
card_original_type('kjeldoran outpost'/'ALL', 'Land').
card_original_text('kjeldoran outpost'/'ALL', 'When Kjeldoran Outpost comes into play, sacrifice a plains or bury Kjeldoran Outpost.\n{T}: Add {W} to your mana pool.\n{1}{W}, {T}: Put a Soldier token into play. Treat this token as a 1/1 white creature.').
card_first_print('kjeldoran outpost', 'ALL').
card_image_name('kjeldoran outpost'/'ALL', 'kjeldoran outpost').
card_uid('kjeldoran outpost'/'ALL', 'ALL:Kjeldoran Outpost:kjeldoran outpost').
card_rarity('kjeldoran outpost'/'ALL', 'Rare').
card_artist('kjeldoran outpost'/'ALL', 'Jeff A. Menges').
card_multiverse_id('kjeldoran outpost'/'ALL', '3233').

card_in_set('kjeldoran pride', 'ALL').
card_original_type('kjeldoran pride'/'ALL', 'Enchant Creature').
card_original_text('kjeldoran pride'/'ALL', 'Enchanted creature gets +1/+2.\n{2}{U}: Switch Kjeldoran Pride from creature it enchants to another creature. Kjeldoran Pride\'s new target must be legal. Treat Kjeldoran Pride as though it were just cast on the new target.').
card_first_print('kjeldoran pride', 'ALL').
card_image_name('kjeldoran pride'/'ALL', 'kjeldoran pride1').
card_uid('kjeldoran pride'/'ALL', 'ALL:Kjeldoran Pride:kjeldoran pride1').
card_rarity('kjeldoran pride'/'ALL', 'Common').
card_artist('kjeldoran pride'/'ALL', 'Kaja Foglio').
card_multiverse_id('kjeldoran pride'/'ALL', '3201').

card_in_set('kjeldoran pride', 'ALL').
card_original_type('kjeldoran pride'/'ALL', 'Enchant Creature').
card_original_text('kjeldoran pride'/'ALL', 'Enchanted creature gets +1/+2.\n{2}{U}: Switch Kjeldoran Pride from creature it enchants to another creature. Kjeldoran Pride\'s new target must be legal. Treat Kjeldoran Pride as though it were just cast on the new target.').
card_image_name('kjeldoran pride'/'ALL', 'kjeldoran pride2').
card_uid('kjeldoran pride'/'ALL', 'ALL:Kjeldoran Pride:kjeldoran pride2').
card_rarity('kjeldoran pride'/'ALL', 'Common').
card_artist('kjeldoran pride'/'ALL', 'Kaja Foglio').
card_multiverse_id('kjeldoran pride'/'ALL', '3202').

card_in_set('krovikan horror', 'ALL').
card_original_type('krovikan horror'/'ALL', 'Summon — Horror').
card_original_text('krovikan horror'/'ALL', 'At the end of any turn, if Krovikan Horror is in your graveyard with a summon card directly above it, you may put Krovikan Horror into your hand.\n{1}: Sacrifice a creature to have Krovikan Horror deal 1 damage to target creature or player.').
card_first_print('krovikan horror', 'ALL').
card_image_name('krovikan horror'/'ALL', 'krovikan horror').
card_uid('krovikan horror'/'ALL', 'ALL:Krovikan Horror:krovikan horror').
card_rarity('krovikan horror'/'ALL', 'Rare').
card_artist('krovikan horror'/'ALL', 'Christopher Rush').
card_multiverse_id('krovikan horror'/'ALL', '3080').

card_in_set('krovikan plague', 'ALL').
card_original_type('krovikan plague'/'ALL', 'Enchant Creature').
card_original_text('krovikan plague'/'ALL', 'Play on a non-wall creature you control.\nDraw a card at the beginning of the upkeep of the turn after Krovikan Plague comes into play.\n{0}: Tap enchanted creature to have Krovikan Plague deal 1 damage to target creature or player. Put a -0/-1 counter on enchanted creature.').
card_first_print('krovikan plague', 'ALL').
card_image_name('krovikan plague'/'ALL', 'krovikan plague').
card_uid('krovikan plague'/'ALL', 'ALL:Krovikan Plague:krovikan plague').
card_rarity('krovikan plague'/'ALL', 'Uncommon').
card_artist('krovikan plague'/'ALL', 'Liz Danforth').
card_multiverse_id('krovikan plague'/'ALL', '3081').

card_in_set('lake of the dead', 'ALL').
card_original_type('lake of the dead'/'ALL', 'Land').
card_original_text('lake of the dead'/'ALL', 'When Lake of the Dead comes into play, sacrifice a swamp or bury Lake of the Dead.\n{T}: Add {B} to your mana pool.\n{T}: Sacrifice a swamp to add {B}{B}{B}{B} to your mana pool.').
card_first_print('lake of the dead', 'ALL').
card_image_name('lake of the dead'/'ALL', 'lake of the dead').
card_uid('lake of the dead'/'ALL', 'ALL:Lake of the Dead:lake of the dead').
card_rarity('lake of the dead'/'ALL', 'Rare').
card_artist('lake of the dead'/'ALL', 'Pete Venters').
card_multiverse_id('lake of the dead'/'ALL', '3234').

card_in_set('lat-nam\'s legacy', 'ALL').
card_original_type('lat-nam\'s legacy'/'ALL', 'Instant').
card_original_text('lat-nam\'s legacy'/'ALL', 'Choose a card from your hand and shuffle that card into your library to draw two cards at the beginning of the next turn\'s upkeep.').
card_first_print('lat-nam\'s legacy', 'ALL').
card_image_name('lat-nam\'s legacy'/'ALL', 'lat-nam\'s legacy1').
card_uid('lat-nam\'s legacy'/'ALL', 'ALL:Lat-Nam\'s Legacy:lat-nam\'s legacy1').
card_rarity('lat-nam\'s legacy'/'ALL', 'Common').
card_artist('lat-nam\'s legacy'/'ALL', 'Tom Wänerstrand').
card_flavor_text('lat-nam\'s legacy'/'ALL', '\"All the knowledge of Lat-Nam could not protect its sages from the Brothers\' War.\" —Gerda Äagesdotter, Archmage of the Unseen').
card_multiverse_id('lat-nam\'s legacy'/'ALL', '3111').

card_in_set('lat-nam\'s legacy', 'ALL').
card_original_type('lat-nam\'s legacy'/'ALL', 'Instant').
card_original_text('lat-nam\'s legacy'/'ALL', 'Choose a card from your hand and shuffle that card into your library to draw two cards at the beginning of the next turn\'s upkeep.').
card_image_name('lat-nam\'s legacy'/'ALL', 'lat-nam\'s legacy2').
card_uid('lat-nam\'s legacy'/'ALL', 'ALL:Lat-Nam\'s Legacy:lat-nam\'s legacy2').
card_rarity('lat-nam\'s legacy'/'ALL', 'Common').
card_artist('lat-nam\'s legacy'/'ALL', 'Tom Wänerstrand').
card_flavor_text('lat-nam\'s legacy'/'ALL', '\"Lat-Nam has bequeathed us its secrets. With them we shall attain true enlightenment.\" —Gerda Äagesdotter, Archmage of the Unseen').
card_multiverse_id('lat-nam\'s legacy'/'ALL', '3110').

card_in_set('library of lat-nam', 'ALL').
card_original_type('library of lat-nam'/'ALL', 'Sorcery').
card_original_text('library of lat-nam'/'ALL', 'Target opponent chooses one: you draw three cards at the beginning of the next turn\'s upkeep; or you search your library for a card, put it into your hand, and then shuffle your library.').
card_first_print('library of lat-nam', 'ALL').
card_image_name('library of lat-nam'/'ALL', 'library of lat-nam').
card_uid('library of lat-nam'/'ALL', 'ALL:Library of Lat-Nam:library of lat-nam').
card_rarity('library of lat-nam'/'ALL', 'Rare').
card_artist('library of lat-nam'/'ALL', 'Alan Rabinowitz').
card_multiverse_id('library of lat-nam'/'ALL', '3112').

card_in_set('lim-dûl\'s high guard', 'ALL').
card_original_type('lim-dûl\'s high guard'/'ALL', 'Summon — Skeleton').
card_original_text('lim-dûl\'s high guard'/'ALL', 'First strike\n{1}{B}: Regenerate').
card_first_print('lim-dûl\'s high guard', 'ALL').
card_image_name('lim-dûl\'s high guard'/'ALL', 'lim-dul\'s high guard1').
card_uid('lim-dûl\'s high guard'/'ALL', 'ALL:Lim-Dûl\'s High Guard:lim-dul\'s high guard1').
card_rarity('lim-dûl\'s high guard'/'ALL', 'Common').
card_artist('lim-dûl\'s high guard'/'ALL', 'Anson Maddocks').
card_flavor_text('lim-dûl\'s high guard'/'ALL', '\"The Guard will forever stand ready. For them, death is merely an inconvenience, not an ending.\" —Chaeska, Keeper of Tresserhorn').
card_multiverse_id('lim-dûl\'s high guard'/'ALL', '3083').

card_in_set('lim-dûl\'s high guard', 'ALL').
card_original_type('lim-dûl\'s high guard'/'ALL', 'Summon — Skeleton').
card_original_text('lim-dûl\'s high guard'/'ALL', 'First strike\n{1}{B}: Regenerate').
card_image_name('lim-dûl\'s high guard'/'ALL', 'lim-dul\'s high guard2').
card_uid('lim-dûl\'s high guard'/'ALL', 'ALL:Lim-Dûl\'s High Guard:lim-dul\'s high guard2').
card_rarity('lim-dûl\'s high guard'/'ALL', 'Common').
card_artist('lim-dûl\'s high guard'/'ALL', 'Anson Maddocks').
card_flavor_text('lim-dûl\'s high guard'/'ALL', '\"Death does not prevent us from service to our absent master. As long as Tresserhorn stands, so shall we.\"\n—Chaeska, Keeper of Tresserhorn').
card_multiverse_id('lim-dûl\'s high guard'/'ALL', '3082').

card_in_set('lim-dûl\'s paladin', 'ALL').
card_original_type('lim-dûl\'s paladin'/'ALL', 'Summon — Paladin').
card_original_text('lim-dûl\'s paladin'/'ALL', 'Trample\nDuring your upkeep, choose and discard a card from your hand, or bury Lim-Dûl\'s Paladin and draw a card.\nIf any creatures are assigned to block it, Lim-Dûl\'s Paladin gets +6/+3 until end of turn.\nIf Lim-Dûl\'s Paladin attacks and is not blocked, it deals no damage to defending player this turn and that player loses 4 life. Effects that prevent or redirect damage cannot be used to counter this loss of life.').
card_first_print('lim-dûl\'s paladin', 'ALL').
card_image_name('lim-dûl\'s paladin'/'ALL', 'lim-dul\'s paladin').
card_uid('lim-dûl\'s paladin'/'ALL', 'ALL:Lim-Dûl\'s Paladin:lim-dul\'s paladin').
card_rarity('lim-dûl\'s paladin'/'ALL', 'Uncommon').
card_artist('lim-dûl\'s paladin'/'ALL', 'Christopher Rush').
card_multiverse_id('lim-dûl\'s paladin'/'ALL', '3222').

card_in_set('lim-dûl\'s vault', 'ALL').
card_original_type('lim-dûl\'s vault'/'ALL', 'Instant').
card_original_text('lim-dûl\'s vault'/'ALL', 'Look at the top five cards of your library. As many times as you choose, you may pay 1 life to put those cards on the bottom of your library and look at the top five cards of your library.\nShuffle all but the top five cards of your library; put those five on top of your library in any order. Effects that prevent or redirect damage cannot be used to counter this loss of life.').
card_first_print('lim-dûl\'s vault', 'ALL').
card_image_name('lim-dûl\'s vault'/'ALL', 'lim-dul\'s vault').
card_uid('lim-dûl\'s vault'/'ALL', 'ALL:Lim-Dûl\'s Vault:lim-dul\'s vault').
card_rarity('lim-dûl\'s vault'/'ALL', 'Uncommon').
card_artist('lim-dûl\'s vault'/'ALL', 'Rob Alexander').
card_multiverse_id('lim-dûl\'s vault'/'ALL', '3223').

card_in_set('lodestone bauble', 'ALL').
card_original_type('lodestone bauble'/'ALL', 'Artifact').
card_original_text('lodestone bauble'/'ALL', '{1}, {T}: Sacrifice Lodestone Bauble to put up to four target basic lands from any player\'s graveyard on top of his or her library in any order. That player draws a card at the beginning of the next turn\'s upkeep.').
card_first_print('lodestone bauble', 'ALL').
card_image_name('lodestone bauble'/'ALL', 'lodestone bauble').
card_uid('lodestone bauble'/'ALL', 'ALL:Lodestone Bauble:lodestone bauble').
card_rarity('lodestone bauble'/'ALL', 'Rare').
card_artist('lodestone bauble'/'ALL', 'Douglas Shuler').
card_multiverse_id('lodestone bauble'/'ALL', '3048').

card_in_set('lord of tresserhorn', 'ALL').
card_original_type('lord of tresserhorn'/'ALL', 'Summon — Legend').
card_original_text('lord of tresserhorn'/'ALL', 'When Lord of Tresserhorn comes into play, pay 2 life and sacrifice two creatures, and target opponent draws two cards. Effects that prevent or redirect damage cannot be used to counter this loss of life.\n{B}: Regenerate').
card_first_print('lord of tresserhorn', 'ALL').
card_image_name('lord of tresserhorn'/'ALL', 'lord of tresserhorn').
card_uid('lord of tresserhorn'/'ALL', 'ALL:Lord of Tresserhorn:lord of tresserhorn').
card_rarity('lord of tresserhorn'/'ALL', 'Rare').
card_artist('lord of tresserhorn'/'ALL', 'Anson Maddocks').
card_multiverse_id('lord of tresserhorn'/'ALL', '3224').

card_in_set('martyrdom', 'ALL').
card_original_type('martyrdom'/'ALL', 'Instant').
card_original_text('martyrdom'/'ALL', 'Until end of turn, you may redirect to target creature you control any amount of damage.').
card_first_print('martyrdom', 'ALL').
card_image_name('martyrdom'/'ALL', 'martyrdom1').
card_uid('martyrdom'/'ALL', 'ALL:Martyrdom:martyrdom1').
card_rarity('martyrdom'/'ALL', 'Common').
card_artist('martyrdom'/'ALL', 'Mark Poole').
card_flavor_text('martyrdom'/'ALL', '\"Martyrs are cheaper than mercenaries, and a far better investment.\"\n—General Varchild').
card_multiverse_id('martyrdom'/'ALL', '3203').

card_in_set('martyrdom', 'ALL').
card_original_type('martyrdom'/'ALL', 'Instant').
card_original_text('martyrdom'/'ALL', 'Until end of turn, you may redirect to target creature you control any amount of damage.').
card_image_name('martyrdom'/'ALL', 'martyrdom2').
card_uid('martyrdom'/'ALL', 'ALL:Martyrdom:martyrdom2').
card_rarity('martyrdom'/'ALL', 'Common').
card_artist('martyrdom'/'ALL', 'Mark Poole').
card_flavor_text('martyrdom'/'ALL', '\"The only true immortality is in dying for a cause.\"\n—Halvor Arensson, Kjeldoran Priest').
card_multiverse_id('martyrdom'/'ALL', '3204').

card_in_set('misfortune', 'ALL').
card_original_type('misfortune'/'ALL', 'Sorcery').
card_original_text('misfortune'/'ALL', 'Target opponent chooses one: you put a +1/+1 counter on each creature you control and gain 4 life; or you put a -1/-1 counter on each creature that opponent controls and Misfortune deals 4 damage to him or her.').
card_first_print('misfortune', 'ALL').
card_image_name('misfortune'/'ALL', 'misfortune').
card_uid('misfortune'/'ALL', 'ALL:Misfortune:misfortune').
card_rarity('misfortune'/'ALL', 'Rare').
card_artist('misfortune'/'ALL', 'Ron Spencer').
card_multiverse_id('misfortune'/'ALL', '3225').

card_in_set('mishra\'s groundbreaker', 'ALL').
card_original_type('mishra\'s groundbreaker'/'ALL', 'Artifact').
card_original_text('mishra\'s groundbreaker'/'ALL', '{T}: Sacrifice Mishra\'s Groundbreaker. Target land becomes a 3/3 artifact creature. That creature still counts as a land.').
card_first_print('mishra\'s groundbreaker', 'ALL').
card_image_name('mishra\'s groundbreaker'/'ALL', 'mishra\'s groundbreaker').
card_uid('mishra\'s groundbreaker'/'ALL', 'ALL:Mishra\'s Groundbreaker:mishra\'s groundbreaker').
card_rarity('mishra\'s groundbreaker'/'ALL', 'Uncommon').
card_artist('mishra\'s groundbreaker'/'ALL', 'Randy Gallegos').
card_flavor_text('mishra\'s groundbreaker'/'ALL', 'The very ground yielded to Mishra\'s wishes.').
card_multiverse_id('mishra\'s groundbreaker'/'ALL', '3049').

card_in_set('misinformation', 'ALL').
card_original_type('misinformation'/'ALL', 'Instant').
card_original_text('misinformation'/'ALL', 'Put up to three target cards from an opponent\'s graveyard on top of his or her library in any order.').
card_first_print('misinformation', 'ALL').
card_image_name('misinformation'/'ALL', 'misinformation').
card_uid('misinformation'/'ALL', 'ALL:Misinformation:misinformation').
card_rarity('misinformation'/'ALL', 'Uncommon').
card_artist('misinformation'/'ALL', 'Richard Kane Ferguson').
card_flavor_text('misinformation'/'ALL', '\"When you cannot rely on your sources, trust your own senses. When you cannot trust those, you must follow your instincts.\" —Lovisa Coldeyes, Balduvian Chieftain').
card_multiverse_id('misinformation'/'ALL', '3084').

card_in_set('mystic compass', 'ALL').
card_original_type('mystic compass'/'ALL', 'Artifact').
card_original_text('mystic compass'/'ALL', '{1}, {T}: Target mana-producing land becomes a basic land type of your choice until end of turn.').
card_first_print('mystic compass', 'ALL').
card_image_name('mystic compass'/'ALL', 'mystic compass').
card_uid('mystic compass'/'ALL', 'ALL:Mystic Compass:mystic compass').
card_rarity('mystic compass'/'ALL', 'Uncommon').
card_artist('mystic compass'/'ALL', 'Amy Weber').
card_flavor_text('mystic compass'/'ALL', '\"And I say north is where I want it to be!\"\n—Jaya Ballard, Task Mage').
card_multiverse_id('mystic compass'/'ALL', '3050').

card_in_set('nature\'s blessing', 'ALL').
card_original_type('nature\'s blessing'/'ALL', 'Enchantment').
card_original_text('nature\'s blessing'/'ALL', '{W}{G}: Choose and discard a card from your hand to have target creature gain banding, first strike, or trample or get a +1/+1 counter.').
card_first_print('nature\'s blessing', 'ALL').
card_image_name('nature\'s blessing'/'ALL', 'nature\'s blessing').
card_uid('nature\'s blessing'/'ALL', 'ALL:Nature\'s Blessing:nature\'s blessing').
card_rarity('nature\'s blessing'/'ALL', 'Uncommon').
card_artist('nature\'s blessing'/'ALL', 'Sandra Everingham').
card_flavor_text('nature\'s blessing'/'ALL', '\"Be open to the blessings, whatever their form.\"\n —Kaysa, Elder Druid of the Juniper Order').
card_multiverse_id('nature\'s blessing'/'ALL', '3226').

card_in_set('nature\'s chosen', 'ALL').
card_original_type('nature\'s chosen'/'ALL', 'Enchant Creature').
card_original_text('nature\'s chosen'/'ALL', 'Play on a creature you control.\n{0}: Untap enchanted creature. Use this ability only during your turn and only once each turn.\n{0}: Tap enchanted creature to untap target artifact, creature, or land. Use this ability only if enchanted creature is white and only once each turn.').
card_first_print('nature\'s chosen', 'ALL').
card_image_name('nature\'s chosen'/'ALL', 'nature\'s chosen').
card_uid('nature\'s chosen'/'ALL', 'ALL:Nature\'s Chosen:nature\'s chosen').
card_rarity('nature\'s chosen'/'ALL', 'Uncommon').
card_artist('nature\'s chosen'/'ALL', 'Rebecca Guay').
card_multiverse_id('nature\'s chosen'/'ALL', '3146').

card_in_set('nature\'s wrath', 'ALL').
card_original_type('nature\'s wrath'/'ALL', 'Enchantment').
card_original_text('nature\'s wrath'/'ALL', 'During your upkeep, pay {G} or bury Nature\'s Wrath.\nWhenever a player puts a swamp or black permanent into play, he or she sacrifices a swamp or black permanent.\nWhenever a player puts an island or a blue permanent into play, he or she sacrifices an island or blue permanent.').
card_first_print('nature\'s wrath', 'ALL').
card_image_name('nature\'s wrath'/'ALL', 'nature\'s wrath').
card_uid('nature\'s wrath'/'ALL', 'ALL:Nature\'s Wrath:nature\'s wrath').
card_rarity('nature\'s wrath'/'ALL', 'Rare').
card_artist('nature\'s wrath'/'ALL', 'Liz Danforth').
card_multiverse_id('nature\'s wrath'/'ALL', '3147').

card_in_set('noble steeds', 'ALL').
card_original_type('noble steeds'/'ALL', 'Enchantment').
card_original_text('noble steeds'/'ALL', '{1}{W}: Target creature gains first strike until end of turn.').
card_first_print('noble steeds', 'ALL').
card_image_name('noble steeds'/'ALL', 'noble steeds1').
card_uid('noble steeds'/'ALL', 'ALL:Noble Steeds:noble steeds1').
card_rarity('noble steeds'/'ALL', 'Common').
card_artist('noble steeds'/'ALL', 'Rebecca Guay').
card_flavor_text('noble steeds'/'ALL', '\"Only an animal born in the wild has sufficient heart to be a true Knight\'s steed.\"\n—King Darien of Kjeldor').
card_multiverse_id('noble steeds'/'ALL', '3206').

card_in_set('noble steeds', 'ALL').
card_original_type('noble steeds'/'ALL', 'Enchantment').
card_original_text('noble steeds'/'ALL', '{1}{W}: Target creature gains first strike until end of turn.').
card_image_name('noble steeds'/'ALL', 'noble steeds2').
card_uid('noble steeds'/'ALL', 'ALL:Noble Steeds:noble steeds2').
card_rarity('noble steeds'/'ALL', 'Common').
card_artist('noble steeds'/'ALL', 'Rebecca Guay').
card_flavor_text('noble steeds'/'ALL', '\"They are fighters as fine as any Knight they carry.\"\n—Arna Kennerüd, Skycaptain').
card_multiverse_id('noble steeds'/'ALL', '3205').

card_in_set('omen of fire', 'ALL').
card_original_type('omen of fire'/'ALL', 'Instant').
card_original_text('omen of fire'/'ALL', 'Return all islands to their owners\' hands. Each player sacrifices a plains or a white permanent for each white permanent he or she controls.').
card_first_print('omen of fire', 'ALL').
card_image_name('omen of fire'/'ALL', 'omen of fire').
card_uid('omen of fire'/'ALL', 'ALL:Omen of Fire:omen of fire').
card_rarity('omen of fire'/'ALL', 'Rare').
card_artist('omen of fire'/'ALL', 'Pete Venters').
card_flavor_text('omen of fire'/'ALL', '\"Let Balduvia burn to warm Kjeldor\'s hearth!\"\n—General Varchild').
card_multiverse_id('omen of fire'/'ALL', '3177').

card_in_set('phantasmal fiend', 'ALL').
card_original_type('phantasmal fiend'/'ALL', 'Summon — Phantasm').
card_original_text('phantasmal fiend'/'ALL', '{B}: +1/-1 until end of turn\n{1}{U}: Switch Phantasmal Fiend\'s power and toughness until end of turn. Effects that alter Phantasmal Fiend\'s power alter its toughness instead, and vice versa.').
card_first_print('phantasmal fiend', 'ALL').
card_image_name('phantasmal fiend'/'ALL', 'phantasmal fiend1').
card_uid('phantasmal fiend'/'ALL', 'ALL:Phantasmal Fiend:phantasmal fiend1').
card_rarity('phantasmal fiend'/'ALL', 'Common').
card_artist('phantasmal fiend'/'ALL', 'Scott Kirschner').
card_multiverse_id('phantasmal fiend'/'ALL', '3086').

card_in_set('phantasmal fiend', 'ALL').
card_original_type('phantasmal fiend'/'ALL', 'Summon — Phantasm').
card_original_text('phantasmal fiend'/'ALL', '{B}: +1/-1 until end of turn\n{1}{U}: Switch Phantasmal Fiend\'s power and toughness until end of turn. Effects that alter Phantasmal Fiend\'s power alter its toughness instead, and vice versa.').
card_image_name('phantasmal fiend'/'ALL', 'phantasmal fiend2').
card_uid('phantasmal fiend'/'ALL', 'ALL:Phantasmal Fiend:phantasmal fiend2').
card_rarity('phantasmal fiend'/'ALL', 'Common').
card_artist('phantasmal fiend'/'ALL', 'Scott Kirschner').
card_multiverse_id('phantasmal fiend'/'ALL', '3085').

card_in_set('phantasmal sphere', 'ALL').
card_original_type('phantasmal sphere'/'ALL', 'Summon — Phantasm').
card_original_text('phantasmal sphere'/'ALL', 'Flying\nAt the beginning of your upkeep, put a +1/+1 counter on Phantasmal Sphere. During your upkeep, pay {1} for each of these +1/+1 counters or bury Phantasmal Sphere.\nIf Phantasmal Sphere leaves play, put an Orb token into play under target opponent\'s control. Treat this token as a */* blue creature with flying, where * is equal to the number of these +1/+1 counters on Phantasmal Sphere.').
card_first_print('phantasmal sphere', 'ALL').
card_image_name('phantasmal sphere'/'ALL', 'phantasmal sphere').
card_uid('phantasmal sphere'/'ALL', 'ALL:Phantasmal Sphere:phantasmal sphere').
card_rarity('phantasmal sphere'/'ALL', 'Rare').
card_artist('phantasmal sphere'/'ALL', 'Mark Tedin').
card_multiverse_id('phantasmal sphere'/'ALL', '3113').

card_in_set('phelddagrif', 'ALL').
card_original_type('phelddagrif'/'ALL', 'Summon — Legend').
card_original_text('phelddagrif'/'ALL', '{W}: Flying until end of turn. Target opponent gains 2 life.\n{U}: Return Phelddagrif to owner\'s hand. Target opponent may draw a card.\n{G}: Trample until end of turn. Put a Hippo token into play under target opponent\'s control. Treat this token as a 1/1 green creature.').
card_first_print('phelddagrif', 'ALL').
card_image_name('phelddagrif'/'ALL', 'phelddagrif').
card_uid('phelddagrif'/'ALL', 'ALL:Phelddagrif:phelddagrif').
card_rarity('phelddagrif'/'ALL', 'Rare').
card_artist('phelddagrif'/'ALL', 'Amy Weber').
card_multiverse_id('phelddagrif'/'ALL', '3227').

card_in_set('phyrexian boon', 'ALL').
card_original_type('phyrexian boon'/'ALL', 'Enchant Creature').
card_original_text('phyrexian boon'/'ALL', 'As long as enchanted creature is black, it gets +2/+1; otherwise, it gets -1/-2.').
card_first_print('phyrexian boon', 'ALL').
card_image_name('phyrexian boon'/'ALL', 'phyrexian boon1').
card_uid('phyrexian boon'/'ALL', 'ALL:Phyrexian Boon:phyrexian boon1').
card_rarity('phyrexian boon'/'ALL', 'Common').
card_artist('phyrexian boon'/'ALL', 'Mark Tedin').
card_flavor_text('phyrexian boon'/'ALL', '\"Phyrexia\'s touch is painful to all but the blackest of hearts.\"\n—Gerda Äagesdotter,\nArchmage of the Unseen').
card_multiverse_id('phyrexian boon'/'ALL', '3088').

card_in_set('phyrexian boon', 'ALL').
card_original_type('phyrexian boon'/'ALL', 'Enchant Creature').
card_original_text('phyrexian boon'/'ALL', 'As long as enchanted creature is black, it gets +2/+1; otherwise, it gets -1/-2.').
card_image_name('phyrexian boon'/'ALL', 'phyrexian boon2').
card_uid('phyrexian boon'/'ALL', 'ALL:Phyrexian Boon:phyrexian boon2').
card_rarity('phyrexian boon'/'ALL', 'Common').
card_artist('phyrexian boon'/'ALL', 'Mark Tedin').
card_flavor_text('phyrexian boon'/'ALL', '\"Dagsson should have paid attention to the lessons of Phyrexia before attempting to create a mechanical utopia.\"\n—Sorine Relicbane, Soldevi Heretic').
card_multiverse_id('phyrexian boon'/'ALL', '3087').

card_in_set('phyrexian devourer', 'ALL').
card_original_type('phyrexian devourer'/'ALL', 'Artifact Creature').
card_original_text('phyrexian devourer'/'ALL', 'If Phyrexian Devourer\'s power is 7 or greater, bury it.\n{0}: Remove the top card of your library from the game to put a +X/+X counter on Phyrexian Devourer, where X is equal to that card\'s casting cost.').
card_first_print('phyrexian devourer', 'ALL').
card_image_name('phyrexian devourer'/'ALL', 'phyrexian devourer').
card_uid('phyrexian devourer'/'ALL', 'ALL:Phyrexian Devourer:phyrexian devourer').
card_rarity('phyrexian devourer'/'ALL', 'Rare').
card_artist('phyrexian devourer'/'ALL', 'Mark Tedin').
card_multiverse_id('phyrexian devourer'/'ALL', '3051').

card_in_set('phyrexian portal', 'ALL').
card_original_type('phyrexian portal'/'ALL', 'Artifact').
card_original_text('phyrexian portal'/'ALL', '{3}: Target opponent looks at the top ten cards of your library and separates them into two face-down piles. Choose one of those piles and remove it from the game. Search the remaining pile and put one of those cards into your hand. Shuffle the remaining cards into your library. Ignore this effect if you have fewer than ten cards in your library.').
card_first_print('phyrexian portal', 'ALL').
card_image_name('phyrexian portal'/'ALL', 'phyrexian portal').
card_uid('phyrexian portal'/'ALL', 'ALL:Phyrexian Portal:phyrexian portal').
card_rarity('phyrexian portal'/'ALL', 'Rare').
card_artist('phyrexian portal'/'ALL', 'Pete Venters').
card_multiverse_id('phyrexian portal'/'ALL', '3052').

card_in_set('phyrexian war beast', 'ALL').
card_original_type('phyrexian war beast'/'ALL', 'Artifact Creature').
card_original_text('phyrexian war beast'/'ALL', 'If Phyrexian War Beast leaves play, sacrifice a land, and Phyrexian War Beast deals 1 damage to you.').
card_first_print('phyrexian war beast', 'ALL').
card_image_name('phyrexian war beast'/'ALL', 'phyrexian war beast1').
card_uid('phyrexian war beast'/'ALL', 'ALL:Phyrexian War Beast:phyrexian war beast1').
card_rarity('phyrexian war beast'/'ALL', 'Common').
card_artist('phyrexian war beast'/'ALL', 'Bill Sienkiewicz').
card_flavor_text('phyrexian war beast'/'ALL', '\"Deal with the spawn of Phyrexia cautiously; only with time may we control them.\"\n—Arcum Dagsson,\nSoldevi Machinist').
card_multiverse_id('phyrexian war beast'/'ALL', '3053').

card_in_set('phyrexian war beast', 'ALL').
card_original_type('phyrexian war beast'/'ALL', 'Artifact Creature').
card_original_text('phyrexian war beast'/'ALL', 'If Phyrexian War Beast leaves play, sacrifice a land, and Phyrexian War Beast deals 1 damage to you.').
card_image_name('phyrexian war beast'/'ALL', 'phyrexian war beast2').
card_uid('phyrexian war beast'/'ALL', 'ALL:Phyrexian War Beast:phyrexian war beast2').
card_rarity('phyrexian war beast'/'ALL', 'Common').
card_artist('phyrexian war beast'/'ALL', 'Bill Sienkiewicz').
card_flavor_text('phyrexian war beast'/'ALL', '\"Knowing its origins, how could they have thought they could control it?\"\n—Sorine Relicbane, Soldevi Heretic').
card_multiverse_id('phyrexian war beast'/'ALL', '3054').

card_in_set('pillage', 'ALL').
card_original_type('pillage'/'ALL', 'Sorcery').
card_original_text('pillage'/'ALL', 'Bury target artifact or land.').
card_first_print('pillage', 'ALL').
card_image_name('pillage'/'ALL', 'pillage').
card_uid('pillage'/'ALL', 'ALL:Pillage:pillage').
card_rarity('pillage'/'ALL', 'Uncommon').
card_artist('pillage'/'ALL', 'Richard Kane Ferguson').
card_flavor_text('pillage'/'ALL', '\"Were they to reduce us to ash, we would clog their throats and sting their eyes in payment.\"\n—Lovisa Coldeyes,\nBalduvian Chieftain').
card_multiverse_id('pillage'/'ALL', '3178').

card_in_set('primitive justice', 'ALL').
card_original_type('primitive justice'/'ALL', 'Sorcery').
card_original_text('primitive justice'/'ALL', 'Destroy target artifact.\nDestroy a target artifact for each {1}{R} you pay in addition to the casting cost.\nDestroy a target artifact and gain 1 life for each {1}{G} you pay in addition to the casting cost.').
card_first_print('primitive justice', 'ALL').
card_image_name('primitive justice'/'ALL', 'primitive justice').
card_uid('primitive justice'/'ALL', 'ALL:Primitive Justice:primitive justice').
card_rarity('primitive justice'/'ALL', 'Uncommon').
card_artist('primitive justice'/'ALL', 'Anthony Waters').
card_multiverse_id('primitive justice'/'ALL', '3179').

card_in_set('pyrokinesis', 'ALL').
card_original_type('pyrokinesis'/'ALL', 'Instant').
card_original_text('pyrokinesis'/'ALL', 'You may remove a red card in your hand from the game instead of paying Pyrokinesis\'s casting cost. Pyrokinesis deals 4 damage, divided any way you choose among any number of target creatures.').
card_first_print('pyrokinesis', 'ALL').
card_image_name('pyrokinesis'/'ALL', 'pyrokinesis').
card_uid('pyrokinesis'/'ALL', 'ALL:Pyrokinesis:pyrokinesis').
card_rarity('pyrokinesis'/'ALL', 'Uncommon').
card_artist('pyrokinesis'/'ALL', 'Ron Spencer').
card_flavor_text('pyrokinesis'/'ALL', '\"Anybody want some . . . toast?\"\n—Jaya Ballard, Task Mage').
card_multiverse_id('pyrokinesis'/'ALL', '3180').

card_in_set('reinforcements', 'ALL').
card_original_type('reinforcements'/'ALL', 'Instant').
card_original_text('reinforcements'/'ALL', 'Put up to three target creature cards from your graveyard on top of your library in any order.').
card_first_print('reinforcements', 'ALL').
card_image_name('reinforcements'/'ALL', 'reinforcements1').
card_uid('reinforcements'/'ALL', 'ALL:Reinforcements:reinforcements1').
card_rarity('reinforcements'/'ALL', 'Common').
card_artist('reinforcements'/'ALL', 'Diana Vick').
card_flavor_text('reinforcements'/'ALL', '\"Let them send their legions I will show them that my truth is stronger than their swords.\"\n—General Varchild').
card_multiverse_id('reinforcements'/'ALL', '3208').

card_in_set('reinforcements', 'ALL').
card_original_type('reinforcements'/'ALL', 'Instant').
card_original_text('reinforcements'/'ALL', 'Put up to three target creature cards from your graveyard on top of your library in any order.').
card_image_name('reinforcements'/'ALL', 'reinforcements2').
card_uid('reinforcements'/'ALL', 'ALL:Reinforcements:reinforcements2').
card_rarity('reinforcements'/'ALL', 'Common').
card_artist('reinforcements'/'ALL', 'Diana Vick').
card_flavor_text('reinforcements'/'ALL', '\"We aided the Balduvians once before. They need but ask and we shall do so again.\"\n—King Darien of Kjeldor').
card_multiverse_id('reinforcements'/'ALL', '3207').

card_in_set('reprisal', 'ALL').
card_original_type('reprisal'/'ALL', 'Instant').
card_original_text('reprisal'/'ALL', 'Bury target creature with power 4 or greater.').
card_first_print('reprisal', 'ALL').
card_image_name('reprisal'/'ALL', 'reprisal1').
card_uid('reprisal'/'ALL', 'ALL:Reprisal:reprisal1').
card_rarity('reprisal'/'ALL', 'Common').
card_artist('reprisal'/'ALL', 'Randy Asplund-Faith').
card_flavor_text('reprisal'/'ALL', '\"The meek shall fight as one, and they shall overcome even the greatest of foes.\"\n—Halvor Arensson, Kjeldoran Priest').
card_multiverse_id('reprisal'/'ALL', '3209').

card_in_set('reprisal', 'ALL').
card_original_type('reprisal'/'ALL', 'Instant').
card_original_text('reprisal'/'ALL', 'Bury target creature with power 4 or greater.').
card_image_name('reprisal'/'ALL', 'reprisal2').
card_uid('reprisal'/'ALL', 'ALL:Reprisal:reprisal2').
card_rarity('reprisal'/'ALL', 'Common').
card_artist('reprisal'/'ALL', 'Randy Asplund-Faith').
card_flavor_text('reprisal'/'ALL', '\"Kjeldor\'s borders are secure, and its citizens are prepared to take up arms in defense of their families.\"\n—King Darien of Kjeldor').
card_multiverse_id('reprisal'/'ALL', '3210').

card_in_set('ritual of the machine', 'ALL').
card_original_type('ritual of the machine'/'ALL', 'Sorcery').
card_original_text('ritual of the machine'/'ALL', 'Sacrifice a creature to gain control of target non-black, non-artifact creature.').
card_first_print('ritual of the machine', 'ALL').
card_image_name('ritual of the machine'/'ALL', 'ritual of the machine').
card_uid('ritual of the machine'/'ALL', 'ALL:Ritual of the Machine:ritual of the machine').
card_rarity('ritual of the machine'/'ALL', 'Rare').
card_artist('ritual of the machine'/'ALL', 'Anson Maddocks').
card_flavor_text('ritual of the machine'/'ALL', '\"Rumors persist of dark deeds performed in the depths of Soldev. When will Dagsson heed the danger therein?\"\n—Sorine Relicbane, Soldevi Heretic').
card_multiverse_id('ritual of the machine'/'ALL', '3089').

card_in_set('rogue skycaptain', 'ALL').
card_original_type('rogue skycaptain'/'ALL', 'Summon — Mercenary').
card_original_text('rogue skycaptain'/'ALL', 'Flying\nAt the beginning of your upkeep, put a wage counter on Rogue Skycaptain. During your upkeep, pay {2} for each wage counter on Rogue Skycaptain, or remove all wage counters from Rogue Skycaptain and target opponent gains control of Rogue Skycaptain.').
card_first_print('rogue skycaptain', 'ALL').
card_image_name('rogue skycaptain'/'ALL', 'rogue skycaptain').
card_uid('rogue skycaptain'/'ALL', 'ALL:Rogue Skycaptain:rogue skycaptain').
card_rarity('rogue skycaptain'/'ALL', 'Rare').
card_artist('rogue skycaptain'/'ALL', 'Randy Asplund-Faith').
card_multiverse_id('rogue skycaptain'/'ALL', '3181').

card_in_set('royal decree', 'ALL').
card_original_type('royal decree'/'ALL', 'Enchantment').
card_original_text('royal decree'/'ALL', 'Cumulative Upkeep: {W}\nWhenever a swamp, mountain, black permanent, or red permanent becomes tapped, Royal Decree deals 1 damage to that permanent\'s controller.').
card_first_print('royal decree', 'ALL').
card_image_name('royal decree'/'ALL', 'royal decree').
card_uid('royal decree'/'ALL', 'ALL:Royal Decree:royal decree').
card_rarity('royal decree'/'ALL', 'Rare').
card_artist('royal decree'/'ALL', 'Pete Venters').
card_multiverse_id('royal decree'/'ALL', '3211').

card_in_set('royal herbalist', 'ALL').
card_original_type('royal herbalist'/'ALL', 'Summon — Cleric').
card_original_text('royal herbalist'/'ALL', '{2}: Remove the top card of your library from the game to gain 1 life.').
card_first_print('royal herbalist', 'ALL').
card_image_name('royal herbalist'/'ALL', 'royal herbalist1').
card_uid('royal herbalist'/'ALL', 'ALL:Royal Herbalist:royal herbalist1').
card_rarity('royal herbalist'/'ALL', 'Common').
card_artist('royal herbalist'/'ALL', 'Douglas Shuler').
card_flavor_text('royal herbalist'/'ALL', '\"Just as an Herbalist heals a wound, so must we heal the rift between Balduvia and Kjeldor.\"\n—King Darien of Kjeldor').
card_multiverse_id('royal herbalist'/'ALL', '3213').

card_in_set('royal herbalist', 'ALL').
card_original_type('royal herbalist'/'ALL', 'Summon — Cleric').
card_original_text('royal herbalist'/'ALL', '{2}: Remove the top card of your library from the game to gain 1 life.').
card_image_name('royal herbalist'/'ALL', 'royal herbalist2').
card_uid('royal herbalist'/'ALL', 'ALL:Royal Herbalist:royal herbalist2').
card_rarity('royal herbalist'/'ALL', 'Common').
card_artist('royal herbalist'/'ALL', 'Douglas Shuler').
card_flavor_text('royal herbalist'/'ALL', '\"It\'s not so easy anymore. Nothing grows in Kjeldor but seaweed.\"\n—Halvor Arensson, Kjeldoran Priest').
card_multiverse_id('royal herbalist'/'ALL', '3212').

card_in_set('scarab of the unseen', 'ALL').
card_original_type('scarab of the unseen'/'ALL', 'Artifact').
card_original_text('scarab of the unseen'/'ALL', '{T}: Sacrifice Scarab of the Unseen to return all enchantments on target permanent you own to their owners\' hand. Draw a card at the beginning of the next turn\'s upkeep.').
card_first_print('scarab of the unseen', 'ALL').
card_image_name('scarab of the unseen'/'ALL', 'scarab of the unseen').
card_uid('scarab of the unseen'/'ALL', 'ALL:Scarab of the Unseen:scarab of the unseen').
card_rarity('scarab of the unseen'/'ALL', 'Uncommon').
card_artist('scarab of the unseen'/'ALL', 'Sandra Everingham').
card_multiverse_id('scarab of the unseen'/'ALL', '3055').

card_in_set('scars of the veteran', 'ALL').
card_original_type('scars of the veteran'/'ALL', 'Instant').
card_original_text('scars of the veteran'/'ALL', 'You may remove a white card in your hand from the game instead of paying Scars of the Veteran\'s casting cost.\nPrevent up to 7 damage to target creature or player. For each 1 damage to a creature prevented by Scars of the Veteran, put a +0/+1 counter on that creature at end of turn.').
card_first_print('scars of the veteran', 'ALL').
card_image_name('scars of the veteran'/'ALL', 'scars of the veteran').
card_uid('scars of the veteran'/'ALL', 'ALL:Scars of the Veteran:scars of the veteran').
card_rarity('scars of the veteran'/'ALL', 'Uncommon').
card_artist('scars of the veteran'/'ALL', 'Dan Frazier').
card_multiverse_id('scars of the veteran'/'ALL', '3214').

card_in_set('school of the unseen', 'ALL').
card_original_type('school of the unseen'/'ALL', 'Land').
card_original_text('school of the unseen'/'ALL', '{T}: Add one colorless mana to your mana pool.\n{2}, {T}: Add one mana of any color to your mana pool.').
card_first_print('school of the unseen', 'ALL').
card_image_name('school of the unseen'/'ALL', 'school of the unseen').
card_uid('school of the unseen'/'ALL', 'ALL:School of the Unseen:school of the unseen').
card_rarity('school of the unseen'/'ALL', 'Uncommon').
card_artist('school of the unseen'/'ALL', 'Pat Morrissey').
card_flavor_text('school of the unseen'/'ALL', 'After the terrible retribution visited upon Lat-Nam, this school of mages chose to hide from the eyes of the world.').
card_multiverse_id('school of the unseen'/'ALL', '3235').

card_in_set('seasoned tactician', 'ALL').
card_original_type('seasoned tactician'/'ALL', 'Summon — Tactician').
card_original_text('seasoned tactician'/'ALL', '{3}: Remove the top four cards of your library from the game to prevent all damage to you from one source.').
card_first_print('seasoned tactician', 'ALL').
card_image_name('seasoned tactician'/'ALL', 'seasoned tactician').
card_uid('seasoned tactician'/'ALL', 'ALL:Seasoned Tactician:seasoned tactician').
card_rarity('seasoned tactician'/'ALL', 'Uncommon').
card_artist('seasoned tactician'/'ALL', 'Dan Frazier').
card_flavor_text('seasoned tactician'/'ALL', '\"The benefits of peace far outweigh the pride of Generals.\"\n—King Darien of Kjeldor').
card_multiverse_id('seasoned tactician'/'ALL', '3215').

card_in_set('sheltered valley', 'ALL').
card_original_type('sheltered valley'/'ALL', 'Land').
card_original_text('sheltered valley'/'ALL', 'When Sheltered Valley comes into play, bury any other Sheltered Valley you control.\nDuring your upkeep, if you control three or fewer lands, gain 1 life.\n{T}: Add one colorless mana to your mana pool.').
card_first_print('sheltered valley', 'ALL').
card_image_name('sheltered valley'/'ALL', 'sheltered valley').
card_uid('sheltered valley'/'ALL', 'ALL:Sheltered Valley:sheltered valley').
card_rarity('sheltered valley'/'ALL', 'Rare').
card_artist('sheltered valley'/'ALL', 'Rob Alexander').
card_multiverse_id('sheltered valley'/'ALL', '3236').

card_in_set('shield sphere', 'ALL').
card_original_type('shield sphere'/'ALL', 'Artifact Creature').
card_original_text('shield sphere'/'ALL', 'Counts as a wall\nIf Shield Sphere is assigned as a blocker, put a -0/-1 counter on it.').
card_first_print('shield sphere', 'ALL').
card_image_name('shield sphere'/'ALL', 'shield sphere').
card_uid('shield sphere'/'ALL', 'ALL:Shield Sphere:shield sphere').
card_rarity('shield sphere'/'ALL', 'Uncommon').
card_artist('shield sphere'/'ALL', 'Alan Rabinowitz').
card_flavor_text('shield sphere'/'ALL', '\"My Soldiers know that they need never fear for their protection.\"\n—King Darien of Kjeldor').
card_multiverse_id('shield sphere'/'ALL', '3056').

card_in_set('sol grail', 'ALL').
card_original_type('sol grail'/'ALL', 'Artifact').
card_original_text('sol grail'/'ALL', 'When Sol Grail comes into play, choose a color.\n{T}: Add one mana of the chosen color to your mana pool. Play this ability as an interrupt.').
card_first_print('sol grail', 'ALL').
card_image_name('sol grail'/'ALL', 'sol grail').
card_uid('sol grail'/'ALL', 'ALL:Sol Grail:sol grail').
card_rarity('sol grail'/'ALL', 'Uncommon').
card_artist('sol grail'/'ALL', 'Christopher Rush').
card_flavor_text('sol grail'/'ALL', '\"Look deep into the Grail, and see there what you desire most.\" —Gerda Äagesdotter, Archmage of the Unseen').
card_multiverse_id('sol grail'/'ALL', '3057').

card_in_set('soldevi adnate', 'ALL').
card_original_type('soldevi adnate'/'ALL', 'Summon — Cleric').
card_original_text('soldevi adnate'/'ALL', '{T}: Sacrifice a black or artifact creature to add an amount of {B} equal to that creature\'s casting cost to your mana pool. Play this ability as an interrupt.').
card_first_print('soldevi adnate', 'ALL').
card_image_name('soldevi adnate'/'ALL', 'soldevi adnate1').
card_uid('soldevi adnate'/'ALL', 'ALL:Soldevi Adnate:soldevi adnate1').
card_rarity('soldevi adnate'/'ALL', 'Common').
card_artist('soldevi adnate'/'ALL', 'Christopher Rush').
card_flavor_text('soldevi adnate'/'ALL', '\"People love to follow fools; they don\'t feel so alone then.\"\n—Sorine Relicbane, Soldevi Heretic').
card_multiverse_id('soldevi adnate'/'ALL', '3090').

card_in_set('soldevi adnate', 'ALL').
card_original_type('soldevi adnate'/'ALL', 'Summon — Cleric').
card_original_text('soldevi adnate'/'ALL', '{T}: Sacrifice a black or artifact creature to add an amount of {B} equal to that creature\'s casting cost to your mana pool. Play this ability as an interrupt.').
card_image_name('soldevi adnate'/'ALL', 'soldevi adnate2').
card_uid('soldevi adnate'/'ALL', 'ALL:Soldevi Adnate:soldevi adnate2').
card_rarity('soldevi adnate'/'ALL', 'Common').
card_artist('soldevi adnate'/'ALL', 'Christopher Rush').
card_flavor_text('soldevi adnate'/'ALL', '\"An idiot cannot hear sense, even when a thousand people speak it.\"\n—Sorine Relicbane, Soldevi Heretic').
card_multiverse_id('soldevi adnate'/'ALL', '3091').

card_in_set('soldevi digger', 'ALL').
card_original_type('soldevi digger'/'ALL', 'Artifact').
card_original_text('soldevi digger'/'ALL', '{2}: Put the top card of your graveyard on the bottom of your library.').
card_first_print('soldevi digger', 'ALL').
card_image_name('soldevi digger'/'ALL', 'soldevi digger').
card_uid('soldevi digger'/'ALL', 'ALL:Soldevi Digger:soldevi digger').
card_rarity('soldevi digger'/'ALL', 'Rare').
card_artist('soldevi digger'/'ALL', 'Amy Weber').
card_flavor_text('soldevi digger'/'ALL', '\"This ceaseless device has helped uncover marvels unreachable by mere flesh.\"\n—Arcum Dagsson,\nSoldevi Machinist').
card_multiverse_id('soldevi digger'/'ALL', '3058').

card_in_set('soldevi excavations', 'ALL').
card_original_type('soldevi excavations'/'ALL', 'Land').
card_original_text('soldevi excavations'/'ALL', 'When Soldevi Excavations comes into play, sacrifice an untapped island or bury Soldevi Excavations.\n{T}: Add {1}{U} to your mana pool.\n{1}, {T}: Look at the top card of your library. You may put that card on the bottom of your library.').
card_first_print('soldevi excavations', 'ALL').
card_image_name('soldevi excavations'/'ALL', 'soldevi excavations').
card_uid('soldevi excavations'/'ALL', 'ALL:Soldevi Excavations:soldevi excavations').
card_rarity('soldevi excavations'/'ALL', 'Rare').
card_artist('soldevi excavations'/'ALL', 'Liz Danforth').
card_multiverse_id('soldevi excavations'/'ALL', '3237').

card_in_set('soldevi heretic', 'ALL').
card_original_type('soldevi heretic'/'ALL', 'Summon — Heretic').
card_original_text('soldevi heretic'/'ALL', '{W}, {T}: Prevent up to 2 damage to any creature. Target opponent may draw a card.').
card_first_print('soldevi heretic', 'ALL').
card_image_name('soldevi heretic'/'ALL', 'soldevi heretic1').
card_uid('soldevi heretic'/'ALL', 'ALL:Soldevi Heretic:soldevi heretic1').
card_rarity('soldevi heretic'/'ALL', 'Common').
card_artist('soldevi heretic'/'ALL', 'Mike Kimble').
card_flavor_text('soldevi heretic'/'ALL', '\"In the arms of tragedy, there is little comfort in being right.\"\n—Sorine Relicbane, Soldevi Heretic').
card_multiverse_id('soldevi heretic'/'ALL', '3115').

card_in_set('soldevi heretic', 'ALL').
card_original_type('soldevi heretic'/'ALL', 'Summon — Heretic').
card_original_text('soldevi heretic'/'ALL', '{W}, {T}: Prevent up to 2 damage to any creature. Target opponent may draw a card.').
card_image_name('soldevi heretic'/'ALL', 'soldevi heretic2').
card_uid('soldevi heretic'/'ALL', 'ALL:Soldevi Heretic:soldevi heretic2').
card_rarity('soldevi heretic'/'ALL', 'Common').
card_artist('soldevi heretic'/'ALL', 'Mike Kimble').
card_flavor_text('soldevi heretic'/'ALL', '\"It cannot be Soldev crushed, and my machines to blame?\"\n—Arcum Dagsson, Soldevi Machinist').
card_multiverse_id('soldevi heretic'/'ALL', '3114').

card_in_set('soldevi sage', 'ALL').
card_original_type('soldevi sage'/'ALL', 'Summon — Wizard').
card_original_text('soldevi sage'/'ALL', '{T}: Sacrifice two lands to draw three cards. Choose and discard one of those cards.').
card_first_print('soldevi sage', 'ALL').
card_image_name('soldevi sage'/'ALL', 'soldevi sage1').
card_uid('soldevi sage'/'ALL', 'ALL:Soldevi Sage:soldevi sage1').
card_rarity('soldevi sage'/'ALL', 'Common').
card_artist('soldevi sage'/'ALL', 'Carol Heyer').
card_flavor_text('soldevi sage'/'ALL', '\"To hide the truth is more than folly—it is fatal.\"\n—Sorine Relicbane, Soldevi Heretic').
card_multiverse_id('soldevi sage'/'ALL', '3117').

card_in_set('soldevi sage', 'ALL').
card_original_type('soldevi sage'/'ALL', 'Summon — Wizard').
card_original_text('soldevi sage'/'ALL', '{T}: Sacrifice two lands to draw three cards. Choose and discard one of those cards.').
card_image_name('soldevi sage'/'ALL', 'soldevi sage2').
card_uid('soldevi sage'/'ALL', 'ALL:Soldevi Sage:soldevi sage2').
card_rarity('soldevi sage'/'ALL', 'Common').
card_artist('soldevi sage'/'ALL', 'Carol Heyer').
card_flavor_text('soldevi sage'/'ALL', '\"Our underground archives grow daily, as our excavators and sages alike dig to uncover hidden wonders.\"\n—Arcum Dagsson,\nSoldevi Machinist').
card_multiverse_id('soldevi sage'/'ALL', '3116').

card_in_set('soldevi sentry', 'ALL').
card_original_type('soldevi sentry'/'ALL', 'Artifact Creature').
card_original_text('soldevi sentry'/'ALL', '{1}: Regenerate. Target opponent may draw a card.').
card_first_print('soldevi sentry', 'ALL').
card_image_name('soldevi sentry'/'ALL', 'soldevi sentry1').
card_uid('soldevi sentry'/'ALL', 'ALL:Soldevi Sentry:soldevi sentry1').
card_rarity('soldevi sentry'/'ALL', 'Common').
card_artist('soldevi sentry'/'ALL', 'Alan Rabinowitz').
card_flavor_text('soldevi sentry'/'ALL', '\"A dreadful invention. What ease is there under the watchful eye of cold steel?\"\n—Sorine Relicbane, Soldevi Heretic').
card_multiverse_id('soldevi sentry'/'ALL', '3060').

card_in_set('soldevi sentry', 'ALL').
card_original_type('soldevi sentry'/'ALL', 'Artifact Creature').
card_original_text('soldevi sentry'/'ALL', '{1}: Regenerate. Target opponent may draw a card.').
card_image_name('soldevi sentry'/'ALL', 'soldevi sentry2').
card_uid('soldevi sentry'/'ALL', 'ALL:Soldevi Sentry:soldevi sentry2').
card_rarity('soldevi sentry'/'ALL', 'Common').
card_artist('soldevi sentry'/'ALL', 'Alan Rabinowitz').
card_flavor_text('soldevi sentry'/'ALL', '\"Our newfound security enables us to turn more of our attentions to the wonders that lie deep within the soil.\"\n—Arcum Dagsson,\nSoldevi Machinist').
card_multiverse_id('soldevi sentry'/'ALL', '3059').

card_in_set('soldevi steam beast', 'ALL').
card_original_type('soldevi steam beast'/'ALL', 'Artifact Creature').
card_original_text('soldevi steam beast'/'ALL', 'Whenever Soldevi Steam Beast becomes tapped, target opponent gains 2 life.\n{2}: Regenerate').
card_first_print('soldevi steam beast', 'ALL').
card_image_name('soldevi steam beast'/'ALL', 'soldevi steam beast1').
card_uid('soldevi steam beast'/'ALL', 'ALL:Soldevi Steam Beast:soldevi steam beast1').
card_rarity('soldevi steam beast'/'ALL', 'Common').
card_artist('soldevi steam beast'/'ALL', 'Bill Sienkiewicz').
card_flavor_text('soldevi steam beast'/'ALL', '\"Dagsson saw the steam as life-giving—until his followers felt its scalding touch.\"\n—Sorine Relicbane, Soldevi Heretic').
card_multiverse_id('soldevi steam beast'/'ALL', '3061').

card_in_set('soldevi steam beast', 'ALL').
card_original_type('soldevi steam beast'/'ALL', 'Artifact Creature').
card_original_text('soldevi steam beast'/'ALL', 'Whenever Soldevi Steam Beast becomes tapped, target opponent gains 2 life.\n{2}: Regenerate').
card_image_name('soldevi steam beast'/'ALL', 'soldevi steam beast2').
card_uid('soldevi steam beast'/'ALL', 'ALL:Soldevi Steam Beast:soldevi steam beast2').
card_rarity('soldevi steam beast'/'ALL', 'Common').
card_artist('soldevi steam beast'/'ALL', 'Bill Sienkiewicz').
card_flavor_text('soldevi steam beast'/'ALL', '\"Nothing has ever broken my heart so much as this—the betrayal of Soldev by my beloved machines\"Arcum Dagsson, Soldevi Machinist').
card_multiverse_id('soldevi steam beast'/'ALL', '3062').

card_in_set('soldier of fortune', 'ALL').
card_original_type('soldier of fortune'/'ALL', 'Summon — Mercenary').
card_original_text('soldier of fortune'/'ALL', '{R}, {T}: Target player shuffles his or her library.').
card_first_print('soldier of fortune', 'ALL').
card_image_name('soldier of fortune'/'ALL', 'soldier of fortune').
card_uid('soldier of fortune'/'ALL', 'ALL:Soldier of Fortune:soldier of fortune').
card_rarity('soldier of fortune'/'ALL', 'Uncommon').
card_artist('soldier of fortune'/'ALL', 'Douglas Shuler').
card_flavor_text('soldier of fortune'/'ALL', '\"Loyalty to coin alone is loyalty nonetheless.\"\n—General Varchild').
card_multiverse_id('soldier of fortune'/'ALL', '3182').

card_in_set('spiny starfish', 'ALL').
card_original_type('spiny starfish'/'ALL', 'Summon — Starfish').
card_original_text('spiny starfish'/'ALL', '{U}: Regenerate\nAt the end of any turn in which Spiny Starfish regenerated, put a Starfish token into play for each time it regenerated that turn. Treat these tokens as 0/1 blue creatures.').
card_first_print('spiny starfish', 'ALL').
card_image_name('spiny starfish'/'ALL', 'spiny starfish').
card_uid('spiny starfish'/'ALL', 'ALL:Spiny Starfish:spiny starfish').
card_rarity('spiny starfish'/'ALL', 'Uncommon').
card_artist('spiny starfish'/'ALL', 'Alan Rabinowitz').
card_multiverse_id('spiny starfish'/'ALL', '3118').

card_in_set('splintering wind', 'ALL').
card_original_type('splintering wind'/'ALL', 'Enchantment').
card_original_text('splintering wind'/'ALL', '{2}{G}: Splintering Wind deals 1 damage to target creature. Put a Splinter token into play. Treat this token as a 1/1 green creature with flying and Cumulative Upkeep: {G}.If this token leaves play, it deals 1 damage to you and to each creature you control.').
card_first_print('splintering wind', 'ALL').
card_image_name('splintering wind'/'ALL', 'splintering wind').
card_uid('splintering wind'/'ALL', 'ALL:Splintering Wind:splintering wind').
card_rarity('splintering wind'/'ALL', 'Rare').
card_artist('splintering wind'/'ALL', 'Ron Spencer').
card_multiverse_id('splintering wind'/'ALL', '3148').

card_in_set('stench of decay', 'ALL').
card_original_type('stench of decay'/'ALL', 'Instant').
card_original_text('stench of decay'/'ALL', 'All non-artifact creatures get -1/-1 until end of turn.').
card_first_print('stench of decay', 'ALL').
card_image_name('stench of decay'/'ALL', 'stench of decay1').
card_uid('stench of decay'/'ALL', 'ALL:Stench of Decay:stench of decay1').
card_rarity('stench of decay'/'ALL', 'Common').
card_artist('stench of decay'/'ALL', 'Heather Hudson').
card_flavor_text('stench of decay'/'ALL', '\"Disa is dead, and I am left. I shall allow no others to succumb to this pestilence.\"\n—Kolbjörn, High Honored Druid').
card_multiverse_id('stench of decay'/'ALL', '3093').

card_in_set('stench of decay', 'ALL').
card_original_type('stench of decay'/'ALL', 'Instant').
card_original_text('stench of decay'/'ALL', 'All non-artifact creatures get -1/-1 until end of turn.').
card_image_name('stench of decay'/'ALL', 'stench of decay2').
card_uid('stench of decay'/'ALL', 'ALL:Stench of Decay:stench of decay2').
card_rarity('stench of decay'/'ALL', 'Common').
card_artist('stench of decay'/'ALL', 'Heather Hudson').
card_flavor_text('stench of decay'/'ALL', '\"My lord, there is only so much I can do. This plague infests Krov worse than any other city.\"\n—Halvor Arensson, Kjeldoran Priest').
card_multiverse_id('stench of decay'/'ALL', '3092').

card_in_set('storm cauldron', 'ALL').
card_original_type('storm cauldron'/'ALL', 'Artifact').
card_original_text('storm cauldron'/'ALL', 'During each player\'s turn, that player may put one additional land into play.\nWhenever a land is tapped for mana, return that land to owner\'s hand.').
card_first_print('storm cauldron', 'ALL').
card_image_name('storm cauldron'/'ALL', 'storm cauldron').
card_uid('storm cauldron'/'ALL', 'ALL:Storm Cauldron:storm cauldron').
card_rarity('storm cauldron'/'ALL', 'Rare').
card_artist('storm cauldron'/'ALL', 'Dan Frazier').
card_multiverse_id('storm cauldron'/'ALL', '3063').

card_in_set('storm crow', 'ALL').
card_original_type('storm crow'/'ALL', 'Summon — Bird').
card_original_text('storm crow'/'ALL', 'Flying').
card_first_print('storm crow', 'ALL').
card_image_name('storm crow'/'ALL', 'storm crow1').
card_uid('storm crow'/'ALL', 'ALL:Storm Crow:storm crow1').
card_rarity('storm crow'/'ALL', 'Common').
card_artist('storm crow'/'ALL', 'Sandra Everingham').
card_flavor_text('storm crow'/'ALL', '\"It tells you that the worst is coming. Do you listen?\n—Lovisa Coldeyes,\nBalduvian Chieftain').
card_multiverse_id('storm crow'/'ALL', '3119').

card_in_set('storm crow', 'ALL').
card_original_type('storm crow'/'ALL', 'Summon — Bird').
card_original_text('storm crow'/'ALL', 'Flying').
card_image_name('storm crow'/'ALL', 'storm crow2').
card_uid('storm crow'/'ALL', 'ALL:Storm Crow:storm crow2').
card_rarity('storm crow'/'ALL', 'Common').
card_artist('storm crow'/'ALL', 'Sandra Everingham').
card_flavor_text('storm crow'/'ALL', '\"Watch for it Right on its tailfeathers will be a storm from your nightmares.\"\n—Arna Kennerüd, Skycaptain').
card_multiverse_id('storm crow'/'ALL', '3120').

card_in_set('storm elemental', 'ALL').
card_original_type('storm elemental'/'ALL', 'Summon — Elemental').
card_original_text('storm elemental'/'ALL', 'Flying\n{U}: Remove the top card of your library from the game to tap target creature with flying.\n{U}: Remove the top card of your library from the game. If that card is a snow-covered land, Storm Elemental gets +1/+1 until end of turn.').
card_first_print('storm elemental', 'ALL').
card_image_name('storm elemental'/'ALL', 'storm elemental').
card_uid('storm elemental'/'ALL', 'ALL:Storm Elemental:storm elemental').
card_rarity('storm elemental'/'ALL', 'Uncommon').
card_artist('storm elemental'/'ALL', 'John Matson').
card_multiverse_id('storm elemental'/'ALL', '3121').

card_in_set('storm shaman', 'ALL').
card_original_type('storm shaman'/'ALL', 'Summon — Cleric').
card_original_text('storm shaman'/'ALL', '{R}: +1/+0 until end of turn').
card_first_print('storm shaman', 'ALL').
card_image_name('storm shaman'/'ALL', 'storm shaman1').
card_uid('storm shaman'/'ALL', 'ALL:Storm Shaman:storm shaman1').
card_rarity('storm shaman'/'ALL', 'Common').
card_artist('storm shaman'/'ALL', 'Carol Heyer').
card_flavor_text('storm shaman'/'ALL', '\"Let the lightning be your warning, and the thunder your battle cry.\"\n—Lovisa Coldeyes,\nBalduvian Chieftain').
card_multiverse_id('storm shaman'/'ALL', '3184').

card_in_set('storm shaman', 'ALL').
card_original_type('storm shaman'/'ALL', 'Summon — Cleric').
card_original_text('storm shaman'/'ALL', '{R}: +1/+0 until end of turn').
card_image_name('storm shaman'/'ALL', 'storm shaman2').
card_uid('storm shaman'/'ALL', 'ALL:Storm Shaman:storm shaman2').
card_rarity('storm shaman'/'ALL', 'Common').
card_artist('storm shaman'/'ALL', 'Carol Heyer').
card_flavor_text('storm shaman'/'ALL', '\"Embrace the storm. Its voice shall echo within you, and its fire shall become your touch\"\n—Lovisa Coldeyes,\nBalduvian Chieftain').
card_multiverse_id('storm shaman'/'ALL', '3183').

card_in_set('stromgald spy', 'ALL').
card_original_type('stromgald spy'/'ALL', 'Summon — Spy').
card_original_text('stromgald spy'/'ALL', 'If Stromgald Spy attacks and is not blocked, you may choose to have it deal no damage to defending player this turn. If you do so, defending player must play with his or her hand face up on the table until Stromgald Spy leaves play.').
card_first_print('stromgald spy', 'ALL').
card_image_name('stromgald spy'/'ALL', 'stromgald spy').
card_uid('stromgald spy'/'ALL', 'ALL:Stromgald Spy:stromgald spy').
card_rarity('stromgald spy'/'ALL', 'Uncommon').
card_artist('stromgald spy'/'ALL', 'Zak Plucinski').
card_multiverse_id('stromgald spy'/'ALL', '3094').

card_in_set('suffocation', 'ALL').
card_original_type('suffocation'/'ALL', 'Instant').
card_original_text('suffocation'/'ALL', 'Play only when a red sorcery or instant deals damage to you. Suffocation deals 4 damage to that spell\'s caster.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_first_print('suffocation', 'ALL').
card_image_name('suffocation'/'ALL', 'suffocation').
card_uid('suffocation'/'ALL', 'ALL:Suffocation:suffocation').
card_rarity('suffocation'/'ALL', 'Uncommon').
card_artist('suffocation'/'ALL', 'L. A. Williams').
card_multiverse_id('suffocation'/'ALL', '3122').

card_in_set('surge of strength', 'ALL').
card_original_type('surge of strength'/'ALL', 'Instant').
card_original_text('surge of strength'/'ALL', 'Choose and discard a red or green card from your hand to have target creature gain trample and get +X/+0 until end of turn, where X is equal to that creature\'s casting cost.').
card_first_print('surge of strength', 'ALL').
card_image_name('surge of strength'/'ALL', 'surge of strength').
card_uid('surge of strength'/'ALL', 'ALL:Surge of Strength:surge of strength').
card_rarity('surge of strength'/'ALL', 'Uncommon').
card_artist('surge of strength'/'ALL', 'Ruth Thompson').
card_multiverse_id('surge of strength'/'ALL', '3228').

card_in_set('sustaining spirit', 'ALL').
card_original_type('sustaining spirit'/'ALL', 'Summon — Guardian').
card_original_text('sustaining spirit'/'ALL', 'Cumulative Upkeep: {1}{W}\nAny damage that would reduce your life total to less than 1 instead reduces it to 1.').
card_first_print('sustaining spirit', 'ALL').
card_image_name('sustaining spirit'/'ALL', 'sustaining spirit').
card_uid('sustaining spirit'/'ALL', 'ALL:Sustaining Spirit:sustaining spirit').
card_rarity('sustaining spirit'/'ALL', 'Rare').
card_artist('sustaining spirit'/'ALL', 'Rebecca Guay').
card_flavor_text('sustaining spirit'/'ALL', '\"Faith is our greatest protector.\"\n—Halvor Arensson, Kjeldoran Priest').
card_multiverse_id('sustaining spirit'/'ALL', '3216').

card_in_set('swamp mosquito', 'ALL').
card_original_type('swamp mosquito'/'ALL', 'Summon — Mosquito').
card_original_text('swamp mosquito'/'ALL', 'Flying\nIf Swamp Mosquito attacks and is not blocked, defending player gets a poison counter. If a player has ten or more poison counters, he or she loses the game.').
card_first_print('swamp mosquito', 'ALL').
card_image_name('swamp mosquito'/'ALL', 'swamp mosquito1').
card_uid('swamp mosquito'/'ALL', 'ALL:Swamp Mosquito:swamp mosquito1').
card_rarity('swamp mosquito'/'ALL', 'Common').
card_artist('swamp mosquito'/'ALL', 'Nicola Leonard').
card_multiverse_id('swamp mosquito'/'ALL', '3096').

card_in_set('swamp mosquito', 'ALL').
card_original_type('swamp mosquito'/'ALL', 'Summon — Mosquito').
card_original_text('swamp mosquito'/'ALL', 'Flying\nIf Swamp Mosquito attacks and is not blocked, defending player gets a poison counter. If a player has ten or more poison counters, he or she loses the game.').
card_image_name('swamp mosquito'/'ALL', 'swamp mosquito2').
card_uid('swamp mosquito'/'ALL', 'ALL:Swamp Mosquito:swamp mosquito2').
card_rarity('swamp mosquito'/'ALL', 'Common').
card_artist('swamp mosquito'/'ALL', 'Nicola Leonard').
card_multiverse_id('swamp mosquito'/'ALL', '3095').

card_in_set('sworn defender', 'ALL').
card_original_type('sworn defender'/'ALL', 'Summon — Knight').
card_original_text('sworn defender'/'ALL', '{1}: Change Sworn Defender\'s power to the toughness of target creature blocking or being blocked by Sworn Defender, minus 1, until end of turn. Change Sworn Defender\'s toughness to 1 plus the power of that creature, until end of turn.').
card_first_print('sworn defender', 'ALL').
card_image_name('sworn defender'/'ALL', 'sworn defender').
card_uid('sworn defender'/'ALL', 'ALL:Sworn Defender:sworn defender').
card_rarity('sworn defender'/'ALL', 'Rare').
card_artist('sworn defender'/'ALL', 'D. Alexander Gregory').
card_multiverse_id('sworn defender'/'ALL', '3217').

card_in_set('taste of paradise', 'ALL').
card_original_type('taste of paradise'/'ALL', 'Sorcery').
card_original_text('taste of paradise'/'ALL', 'Gain 3 life.\nGain 3 life for each {1}{G} you pay in addition to the casting cost.').
card_first_print('taste of paradise', 'ALL').
card_image_name('taste of paradise'/'ALL', 'taste of paradise1').
card_uid('taste of paradise'/'ALL', 'ALL:Taste of Paradise:taste of paradise1').
card_rarity('taste of paradise'/'ALL', 'Common').
card_artist('taste of paradise'/'ALL', 'Lawrence Snelly').
card_flavor_text('taste of paradise'/'ALL', '\"Any who drink of Freyalise\'s bounty drink the mead of rebirth.\"\n—Kaysa, Elder Druid of the Juniper Order').
card_multiverse_id('taste of paradise'/'ALL', '3150').

card_in_set('taste of paradise', 'ALL').
card_original_type('taste of paradise'/'ALL', 'Sorcery').
card_original_text('taste of paradise'/'ALL', 'Gain 3 life.\nGain 3 life for each {1}{G} you pay in addition to the casting cost.').
card_image_name('taste of paradise'/'ALL', 'taste of paradise2').
card_uid('taste of paradise'/'ALL', 'ALL:Taste of Paradise:taste of paradise2').
card_rarity('taste of paradise'/'ALL', 'Common').
card_artist('taste of paradise'/'ALL', 'Lawrence Snelly').
card_flavor_text('taste of paradise'/'ALL', '\"There is, in each of us, the potential to heal and the gift of renewed life.\"\n—Kaysa, Elder Druid of the Juniper Order').
card_multiverse_id('taste of paradise'/'ALL', '3149').

card_in_set('thawing glaciers', 'ALL').
card_original_type('thawing glaciers'/'ALL', 'Land').
card_original_text('thawing glaciers'/'ALL', 'Comes into play tapped.\n{1}, {T}: Search your library for a basic land and put it into play tapped. This does not count towards your one land per turn limit. Shuffle your library afterwards. At end of turn, return Thawing Glaciers to owner\'s hand.').
card_first_print('thawing glaciers', 'ALL').
card_image_name('thawing glaciers'/'ALL', 'thawing glaciers').
card_uid('thawing glaciers'/'ALL', 'ALL:Thawing Glaciers:thawing glaciers').
card_rarity('thawing glaciers'/'ALL', 'Rare').
card_artist('thawing glaciers'/'ALL', 'Jeff A. Menges').
card_multiverse_id('thawing glaciers'/'ALL', '3238').

card_in_set('thought lash', 'ALL').
card_original_type('thought lash'/'ALL', 'Enchantment').
card_original_text('thought lash'/'ALL', 'Cumulative Upkeep: Remove the top card of your library from the game. If you do not, remove your library from the game and bury Thought Lash.\n{0}: Remove the top card of your library from the game to prevent 1 damage to you.').
card_first_print('thought lash', 'ALL').
card_image_name('thought lash'/'ALL', 'thought lash').
card_uid('thought lash'/'ALL', 'ALL:Thought Lash:thought lash').
card_rarity('thought lash'/'ALL', 'Rare').
card_artist('thought lash'/'ALL', 'Mark Tedin').
card_multiverse_id('thought lash'/'ALL', '3123').

card_in_set('tidal control', 'ALL').
card_original_type('tidal control'/'ALL', 'Enchantment').
card_original_text('tidal control'/'ALL', 'Cumulative Upkeep: {2}\nAny player may pay {2} or 2 life to counter target red or green spell. Play this ability as an interrupt. Effects that prevent or redirect damage cannot be used to counter this loss of life.').
card_first_print('tidal control', 'ALL').
card_image_name('tidal control'/'ALL', 'tidal control').
card_uid('tidal control'/'ALL', 'ALL:Tidal Control:tidal control').
card_rarity('tidal control'/'ALL', 'Rare').
card_artist('tidal control'/'ALL', 'Randy Gallegos').
card_multiverse_id('tidal control'/'ALL', '3124').

card_in_set('tornado', 'ALL').
card_original_type('tornado'/'ALL', 'Enchantment').
card_original_text('tornado'/'ALL', 'Cumulative Upkeep: {G}\n{2}{G}: Pay 3 life for each velocity counter on Tornado. Destroy target permanent and put a velocity counter on Tornado. Use this ability only once each turn. Effects that prevent or redirect damage cannot be used to counter this loss of life.').
card_first_print('tornado', 'ALL').
card_image_name('tornado'/'ALL', 'tornado').
card_uid('tornado'/'ALL', 'ALL:Tornado:tornado').
card_rarity('tornado'/'ALL', 'Rare').
card_artist('tornado'/'ALL', 'Susan Van Camp').
card_multiverse_id('tornado'/'ALL', '3151').

card_in_set('undergrowth', 'ALL').
card_original_type('undergrowth'/'ALL', 'Instant').
card_original_text('undergrowth'/'ALL', 'No creatures deal damage in combat this turn. If you pay {2}{R} in addition to the casting cost, Undergrowth does not affect red creatures.').
card_first_print('undergrowth', 'ALL').
card_image_name('undergrowth'/'ALL', 'undergrowth1').
card_uid('undergrowth'/'ALL', 'ALL:Undergrowth:undergrowth1').
card_rarity('undergrowth'/'ALL', 'Common').
card_artist('undergrowth'/'ALL', 'Pat Morrissey').
card_flavor_text('undergrowth'/'ALL', '\"It is the work of sorcery. Burn it!\" —Taaveti of Kelsinko, Elvish Hunter').
card_multiverse_id('undergrowth'/'ALL', '3152').

card_in_set('undergrowth', 'ALL').
card_original_type('undergrowth'/'ALL', 'Instant').
card_original_text('undergrowth'/'ALL', 'No creatures deal damage in combat this turn. If you pay {2}{R} in addition to the casting cost, Undergrowth does not affect red creatures.').
card_image_name('undergrowth'/'ALL', 'undergrowth2').
card_uid('undergrowth'/'ALL', 'ALL:Undergrowth:undergrowth2').
card_rarity('undergrowth'/'ALL', 'Common').
card_artist('undergrowth'/'ALL', 'Pat Morrissey').
card_flavor_text('undergrowth'/'ALL', '\"The art of war is turning obstacle to advantage.\" —Jaeuhl Carthalion, Juniper Order Advocate').
card_multiverse_id('undergrowth'/'ALL', '3153').

card_in_set('unlikely alliance', 'ALL').
card_original_type('unlikely alliance'/'ALL', 'Enchantment').
card_original_text('unlikely alliance'/'ALL', '{1}{W}: Target non-attacking, non-blocking creature gets +0/+2 until end of turn.').
card_first_print('unlikely alliance', 'ALL').
card_image_name('unlikely alliance'/'ALL', 'unlikely alliance').
card_uid('unlikely alliance'/'ALL', 'ALL:Unlikely Alliance:unlikely alliance').
card_rarity('unlikely alliance'/'ALL', 'Uncommon').
card_artist('unlikely alliance'/'ALL', 'Phil Foglio').
card_flavor_text('unlikely alliance'/'ALL', '\"Strange alliances are alliances still, and provide the same protection.\"\n—King Darien of Kjeldor').
card_multiverse_id('unlikely alliance'/'ALL', '3218').

card_in_set('urza\'s engine', 'ALL').
card_original_type('urza\'s engine'/'ALL', 'Artifact Creature').
card_original_text('urza\'s engine'/'ALL', 'Trample\n{3}: Banding until end of turn\n{3}: All creatures banded with Urza\'s Engine gain trample until end of turn.').
card_first_print('urza\'s engine', 'ALL').
card_image_name('urza\'s engine'/'ALL', 'urza\'s engine').
card_uid('urza\'s engine'/'ALL', 'ALL:Urza\'s Engine:urza\'s engine').
card_rarity('urza\'s engine'/'ALL', 'Uncommon').
card_artist('urza\'s engine'/'ALL', 'Greg Simanson').
card_flavor_text('urza\'s engine'/'ALL', '\"Humans and machines working together can be fearsome indeed!\" —Arcum Dagsson').
card_multiverse_id('urza\'s engine'/'ALL', '3064').

card_in_set('varchild\'s crusader', 'ALL').
card_original_type('varchild\'s crusader'/'ALL', 'Summon — Knight').
card_original_text('varchild\'s crusader'/'ALL', '{0}: Varchild\'s Crusader cannot be blocked except by walls this turn. Bury Varchild\'s Crusader at end of turn.').
card_first_print('varchild\'s crusader', 'ALL').
card_image_name('varchild\'s crusader'/'ALL', 'varchild\'s crusader1').
card_uid('varchild\'s crusader'/'ALL', 'ALL:Varchild\'s Crusader:varchild\'s crusader1').
card_rarity('varchild\'s crusader'/'ALL', 'Common').
card_artist('varchild\'s crusader'/'ALL', 'Mark Poole').
card_flavor_text('varchild\'s crusader'/'ALL', '\"We could have peace, but Varchild would rather have the maps of the new Kjeldor drawn in Balduvian blood.\" —King Darien of Kjeldor').
card_multiverse_id('varchild\'s crusader'/'ALL', '3186').

card_in_set('varchild\'s crusader', 'ALL').
card_original_type('varchild\'s crusader'/'ALL', 'Summon — Knight').
card_original_text('varchild\'s crusader'/'ALL', '{0}: Varchild\'s Crusader cannot be blocked except by walls this turn. Bury Varchild\'s Crusader at end of turn.').
card_image_name('varchild\'s crusader'/'ALL', 'varchild\'s crusader2').
card_uid('varchild\'s crusader'/'ALL', 'ALL:Varchild\'s Crusader:varchild\'s crusader2').
card_rarity('varchild\'s crusader'/'ALL', 'Common').
card_artist('varchild\'s crusader'/'ALL', 'Mark Poole').
card_flavor_text('varchild\'s crusader'/'ALL', '\"Every patch of land must belong to Kjeldor, no matter what the cost!\"\n—General Varchild').
card_multiverse_id('varchild\'s crusader'/'ALL', '3185').

card_in_set('varchild\'s war-riders', 'ALL').
card_original_type('varchild\'s war-riders'/'ALL', 'Summon — War-Riders').
card_original_text('varchild\'s war-riders'/'ALL', 'Trample, rampage: 1\nCumulative Upkeep: Put a Survivor token into play under target opponent\'s control. Treat this token as a 1/1 red creature.').
card_first_print('varchild\'s war-riders', 'ALL').
card_image_name('varchild\'s war-riders'/'ALL', 'varchild\'s war-riders').
card_uid('varchild\'s war-riders'/'ALL', 'ALL:Varchild\'s War-Riders:varchild\'s war-riders').
card_rarity('varchild\'s war-riders'/'ALL', 'Rare').
card_artist('varchild\'s war-riders'/'ALL', 'Susan Van Camp').
card_flavor_text('varchild\'s war-riders'/'ALL', '\"What tries to crush our spirit only strengthens our resolve.\" —Lovisa Coldeyes, Balduvian Chieftain').
card_multiverse_id('varchild\'s war-riders'/'ALL', '3187').

card_in_set('veteran\'s voice', 'ALL').
card_original_type('veteran\'s voice'/'ALL', 'Enchant Creature').
card_original_text('veteran\'s voice'/'ALL', 'Play on a creature you control.\n{0}: Tap enchanted creature to give any other target creature +2/+1 until end of turn.').
card_first_print('veteran\'s voice', 'ALL').
card_image_name('veteran\'s voice'/'ALL', 'veteran\'s voice1').
card_uid('veteran\'s voice'/'ALL', 'ALL:Veteran\'s Voice:veteran\'s voice1').
card_rarity('veteran\'s voice'/'ALL', 'Common').
card_artist('veteran\'s voice'/'ALL', 'Andi Rusu').
card_flavor_text('veteran\'s voice'/'ALL', '\"Good soldiers rely first upon their training, then upon their instincts.\" —King Darien of Kjeldor').
card_multiverse_id('veteran\'s voice'/'ALL', '3189').

card_in_set('veteran\'s voice', 'ALL').
card_original_type('veteran\'s voice'/'ALL', 'Enchant Creature').
card_original_text('veteran\'s voice'/'ALL', 'Play on a creature you control.\n{0}: Tap enchanted creature to give any other target creature +2/+1 until end of turn.').
card_image_name('veteran\'s voice'/'ALL', 'veteran\'s voice2').
card_uid('veteran\'s voice'/'ALL', 'ALL:Veteran\'s Voice:veteran\'s voice2').
card_rarity('veteran\'s voice'/'ALL', 'Common').
card_artist('veteran\'s voice'/'ALL', 'Andi Rusu').
card_flavor_text('veteran\'s voice'/'ALL', '\"Teach by example. If your students do not survive, they were not worth the lesson.\"\n—General Varchild').
card_multiverse_id('veteran\'s voice'/'ALL', '3188').

card_in_set('viscerid armor', 'ALL').
card_original_type('viscerid armor'/'ALL', 'Enchant Creature').
card_original_text('viscerid armor'/'ALL', 'Enchanted creature gets +1/+1.\n{1}{U}: Return Viscerid Armor to owner\'s hand.').
card_first_print('viscerid armor', 'ALL').
card_image_name('viscerid armor'/'ALL', 'viscerid armor1').
card_uid('viscerid armor'/'ALL', 'ALL:Viscerid Armor:viscerid armor1').
card_rarity('viscerid armor'/'ALL', 'Common').
card_artist('viscerid armor'/'ALL', 'Heather Hudson').
card_flavor_text('viscerid armor'/'ALL', 'A fallen Viscerid\'s only tribute is to be worn by a comrade.').
card_multiverse_id('viscerid armor'/'ALL', '3126').

card_in_set('viscerid armor', 'ALL').
card_original_type('viscerid armor'/'ALL', 'Enchant Creature').
card_original_text('viscerid armor'/'ALL', 'Enchanted creature gets +1/+1.\n{1}{U}: Return Viscerid Armor to owner\'s hand.').
card_image_name('viscerid armor'/'ALL', 'viscerid armor2').
card_uid('viscerid armor'/'ALL', 'ALL:Viscerid Armor:viscerid armor2').
card_rarity('viscerid armor'/'ALL', 'Common').
card_artist('viscerid armor'/'ALL', 'Heather Hudson').
card_flavor_text('viscerid armor'/'ALL', 'One Viscerid\'s death is often another\'s gain.').
card_multiverse_id('viscerid armor'/'ALL', '3125').

card_in_set('viscerid drone', 'ALL').
card_original_type('viscerid drone'/'ALL', 'Summon — Homarid').
card_original_text('viscerid drone'/'ALL', '{T}: Sacrifice a creature and a swamp to bury target non-artifact creature.\n{T}: Sacrifice a creature and a snow-covered swamp to bury target creature.').
card_first_print('viscerid drone', 'ALL').
card_image_name('viscerid drone'/'ALL', 'viscerid drone').
card_uid('viscerid drone'/'ALL', 'ALL:Viscerid Drone:viscerid drone').
card_rarity('viscerid drone'/'ALL', 'Uncommon').
card_artist('viscerid drone'/'ALL', 'Heather Hudson').
card_flavor_text('viscerid drone'/'ALL', 'Not all of Terisiare\'s flooding was natural . . . .').
card_multiverse_id('viscerid drone'/'ALL', '3127').

card_in_set('wandering mage', 'ALL').
card_original_type('wandering mage'/'ALL', 'Summon — Cleric').
card_original_text('wandering mage'/'ALL', '{W}: Pay 1 life to prevent up to 2 damage to any creature. Effects that prevent or redirect damage cannot be used to counter this loss of life.\n{U}: Prevent 1 damage to any Cleric or Wizard.\n{B}: Put a -1/-1 counter on target creature you control to prevent up to 2 damage to any player.').
card_first_print('wandering mage', 'ALL').
card_image_name('wandering mage'/'ALL', 'wandering mage').
card_uid('wandering mage'/'ALL', 'ALL:Wandering Mage:wandering mage').
card_rarity('wandering mage'/'ALL', 'Rare').
card_artist('wandering mage'/'ALL', 'Pete Venters').
card_multiverse_id('wandering mage'/'ALL', '3229').

card_in_set('whip vine', 'ALL').
card_original_type('whip vine'/'ALL', 'Summon — Wall').
card_original_text('whip vine'/'ALL', 'Can block creatures with flying.\nYou may choose not to untap Whip Vine during your untap phase.\n{T}: Tap target creature with flying blocked by Whip Vine. That creature does not untap during its controller\'s untap phase as long as Whip Vine remains tapped.').
card_first_print('whip vine', 'ALL').
card_image_name('whip vine'/'ALL', 'whip vine1').
card_uid('whip vine'/'ALL', 'ALL:Whip Vine:whip vine1').
card_rarity('whip vine'/'ALL', 'Common').
card_artist('whip vine'/'ALL', 'L. A. Williams').
card_multiverse_id('whip vine'/'ALL', '3154').

card_in_set('whip vine', 'ALL').
card_original_type('whip vine'/'ALL', 'Summon — Wall').
card_original_text('whip vine'/'ALL', 'Can block creatures with flying. You may choose not to untap Whip Vine during your untap phase.\n{T}: Tap target creature with flying blocked by Whip Vine. That creature does not untap during its controller\'s untap phase as long as Whip Vine remains tapped.').
card_image_name('whip vine'/'ALL', 'whip vine2').
card_uid('whip vine'/'ALL', 'ALL:Whip Vine:whip vine2').
card_rarity('whip vine'/'ALL', 'Common').
card_artist('whip vine'/'ALL', 'L. A. Williams').
card_multiverse_id('whip vine'/'ALL', '3155').

card_in_set('whirling catapult', 'ALL').
card_original_type('whirling catapult'/'ALL', 'Artifact').
card_original_text('whirling catapult'/'ALL', '{2}: Remove the top two cards of your library from the game to have Whirling Catapult deal 1 damage to each creature with flying and each player.').
card_first_print('whirling catapult', 'ALL').
card_image_name('whirling catapult'/'ALL', 'whirling catapult').
card_uid('whirling catapult'/'ALL', 'ALL:Whirling Catapult:whirling catapult').
card_rarity('whirling catapult'/'ALL', 'Uncommon').
card_artist('whirling catapult'/'ALL', 'Dan Frazier').
card_flavor_text('whirling catapult'/'ALL', '\"Direct confrontation never was to the Orcs\' taste.\"\n—General Varchild').
card_multiverse_id('whirling catapult'/'ALL', '3065').

card_in_set('wild aesthir', 'ALL').
card_original_type('wild aesthir'/'ALL', 'Summon — Aesthir').
card_original_text('wild aesthir'/'ALL', 'Flying, first strike\n{W}{W}: +2/+0 until end of turn. You cannot spend more than {W}{W} in this way each turn.').
card_first_print('wild aesthir', 'ALL').
card_image_name('wild aesthir'/'ALL', 'wild aesthir1').
card_uid('wild aesthir'/'ALL', 'ALL:Wild Aesthir:wild aesthir1').
card_rarity('wild aesthir'/'ALL', 'Common').
card_artist('wild aesthir'/'ALL', 'Greg Simanson').
card_flavor_text('wild aesthir'/'ALL', '\"High in the Karplusans, death is swift and razor-clawed.\"\n—Arna Kennerüd, Skycaptain').
card_multiverse_id('wild aesthir'/'ALL', '3220').

card_in_set('wild aesthir', 'ALL').
card_original_type('wild aesthir'/'ALL', 'Summon — Aesthir').
card_original_text('wild aesthir'/'ALL', 'Flying, first strike\n{W}{W}: +2/+0 until end of turn. You cannot spend more than {W}{W} in this way each turn.').
card_image_name('wild aesthir'/'ALL', 'wild aesthir2').
card_uid('wild aesthir'/'ALL', 'ALL:Wild Aesthir:wild aesthir2').
card_rarity('wild aesthir'/'ALL', 'Common').
card_artist('wild aesthir'/'ALL', 'Greg Simanson').
card_flavor_text('wild aesthir'/'ALL', '\"What Barbarian secrets do they spy from their lofty perch?\"\n—General Varchild').
card_multiverse_id('wild aesthir'/'ALL', '3219').

card_in_set('winter\'s night', 'ALL').
card_original_type('winter\'s night'/'ALL', 'Enchant World').
card_original_text('winter\'s night'/'ALL', 'Whenever a snow-covered land is tapped for mana, it produces one additional mana of the same type and does not untap during its controller\'s next untap phase.').
card_first_print('winter\'s night', 'ALL').
card_image_name('winter\'s night'/'ALL', 'winter\'s night').
card_uid('winter\'s night'/'ALL', 'ALL:Winter\'s Night:winter\'s night').
card_rarity('winter\'s night'/'ALL', 'Rare').
card_artist('winter\'s night'/'ALL', 'Rob Alexander').
card_multiverse_id('winter\'s night'/'ALL', '3230').

card_in_set('yavimaya ancients', 'ALL').
card_original_type('yavimaya ancients'/'ALL', 'Summon — Treefolk').
card_original_text('yavimaya ancients'/'ALL', '{G}: +1/-2 until end of turn').
card_first_print('yavimaya ancients', 'ALL').
card_image_name('yavimaya ancients'/'ALL', 'yavimaya ancients1').
card_uid('yavimaya ancients'/'ALL', 'ALL:Yavimaya Ancients:yavimaya ancients1').
card_rarity('yavimaya ancients'/'ALL', 'Common').
card_artist('yavimaya ancients'/'ALL', 'Quinton Hoover').
card_flavor_text('yavimaya ancients'/'ALL', '\"The Ancients teach us that if we can but last, we shall prevail.\"\n—Kaysa, Elder Druid of the Juniper Order').
card_multiverse_id('yavimaya ancients'/'ALL', '3156').

card_in_set('yavimaya ancients', 'ALL').
card_original_type('yavimaya ancients'/'ALL', 'Summon — Treefolk').
card_original_text('yavimaya ancients'/'ALL', '{G}: +1/-2 until end of turn').
card_image_name('yavimaya ancients'/'ALL', 'yavimaya ancients2').
card_uid('yavimaya ancients'/'ALL', 'ALL:Yavimaya Ancients:yavimaya ancients2').
card_rarity('yavimaya ancients'/'ALL', 'Common').
card_artist('yavimaya ancients'/'ALL', 'Quinton Hoover').
card_flavor_text('yavimaya ancients'/'ALL', '\"We orphans of Fyndhorn have found no welcome in this alien place.\"\n—Taaveti of Kelsinko, Elvish Hunter').
card_multiverse_id('yavimaya ancients'/'ALL', '3157').

card_in_set('yavimaya ants', 'ALL').
card_original_type('yavimaya ants'/'ALL', 'Summon — Swarm').
card_original_text('yavimaya ants'/'ALL', 'Trample\nCumulative Upkeep: {G}{G}\nYavimaya Ants can attack the turn it comes into play on your side.').
card_first_print('yavimaya ants', 'ALL').
card_image_name('yavimaya ants'/'ALL', 'yavimaya ants').
card_uid('yavimaya ants'/'ALL', 'ALL:Yavimaya Ants:yavimaya ants').
card_rarity('yavimaya ants'/'ALL', 'Uncommon').
card_artist('yavimaya ants'/'ALL', 'Pat Morrissey').
card_flavor_text('yavimaya ants'/'ALL', '\"Few natural forces are as devastating as hunger.\"\n—Kaysa, Elder Druid of the Juniper Order').
card_multiverse_id('yavimaya ants'/'ALL', '3158').
