% 15th Anniversary

set('p15A').
set_name('p15A', '15th Anniversary').
set_release_date('p15A', '2008-04-01').
set_border('p15A', 'black').
set_type('p15A', 'promo').

card_in_set('char', 'p15A').
card_original_type('char'/'p15A', 'Instant').
card_original_text('char'/'p15A', '').
card_image_name('char'/'p15A', 'char').
card_uid('char'/'p15A', 'p15A:Char:char').
card_rarity('char'/'p15A', 'Special').
card_artist('char'/'p15A', 'Dave Dorman').
card_number('char'/'p15A', '1').
card_flavor_text('char'/'p15A', 'If you\'ve never choked on the embers of your own spell, you aren\'t casting it hard enough.').

card_in_set('kamahl, pit fighter', 'p15A').
card_original_type('kamahl, pit fighter'/'p15A', 'Legendary Creature — Human Barbarian').
card_original_text('kamahl, pit fighter'/'p15A', '').
card_image_name('kamahl, pit fighter'/'p15A', 'kamahl, pit fighter').
card_uid('kamahl, pit fighter'/'p15A', 'p15A:Kamahl, Pit Fighter:kamahl, pit fighter').
card_rarity('kamahl, pit fighter'/'p15A', 'Special').
card_artist('kamahl, pit fighter'/'p15A', 'Kev Walker').
card_number('kamahl, pit fighter'/'p15A', '2').
card_flavor_text('kamahl, pit fighter'/'p15A', 'In times when freedom seems lost, great souls arise to reclaim it.').
