% Unhinged

set('UNH').
set_name('UNH', 'Unhinged').
set_release_date('UNH', '2004-11-20').
set_border('UNH', 'silver').
set_type('UNH', 'un').

card_in_set('\"ach! hans, run!\"', 'UNH').
card_original_type('\"ach! hans, run!\"'/'UNH', 'Enchantment').
card_original_text('\"ach! hans, run!\"'/'UNH', 'At the beginning of your upkeep, you may say \"Ach! Hans, run! It\'s the . . .\" and name a creature card. If you do, search your library for the named card, put it into play, then shuffle your library. That creature has haste. Remove it from the game at end of turn.').
card_first_print('\"ach! hans, run!\"', 'UNH').
card_image_name('\"ach! hans, run!\"'/'UNH', 'ach! hans, run!').
card_uid('\"ach! hans, run!\"'/'UNH', 'UNH:\"Ach! Hans, Run!\":ach! hans, run!').
card_rarity('\"ach! hans, run!\"'/'UNH', 'Rare').
card_artist('\"ach! hans, run!\"'/'UNH', 'Quinton Hoover').
card_number('\"ach! hans, run!\"'/'UNH', '116').
card_multiverse_id('\"ach! hans, run!\"'/'UNH', '73935').

card_in_set('_____', 'UNH').
card_original_type('_____'/'UNH', 'Creature — Shapeshifter').
card_original_text('_____'/'UNH', '{1}: This card\'s name becomes the name of your choice. Play this ability anywhere, anytime.').
card_first_print('_____', 'UNH').
card_image_name('_____'/'UNH', '_____').
card_uid('_____'/'UNH', 'UNH:_____:_____').
card_rarity('_____'/'UNH', 'Uncommon').
card_artist('_____'/'UNH', 'Ron Spears').
card_number('_____'/'UNH', '23').
card_flavor_text('_____'/'UNH', '{1}: This card\'s flavor text becomes the flavor text of your choice. (This ability doesn\'t work because it\'s flavor text, not rules text (but neither does this reminder text, so you figure it out).)').
card_multiverse_id('_____'/'UNH', '74252').

card_in_set('aesthetic consultation', 'UNH').
card_original_type('aesthetic consultation'/'UNH', 'Instant').
card_original_text('aesthetic consultation'/'UNH', 'Name an artist. Remove the top six cards of your library from the game, then reveal cards from the top of your library until you reveal a card by the named artist. Put that card in your hand, then remove all the other cards revealed this way from the game.').
card_first_print('aesthetic consultation', 'UNH').
card_image_name('aesthetic consultation'/'UNH', 'aesthetic consultation').
card_uid('aesthetic consultation'/'UNH', 'UNH:Aesthetic Consultation:aesthetic consultation').
card_rarity('aesthetic consultation'/'UNH', 'Rare').
card_artist('aesthetic consultation'/'UNH', 'David “Help Me” Martin').
card_number('aesthetic consultation'/'UNH', '48').
card_multiverse_id('aesthetic consultation'/'UNH', '74284').

card_in_set('ambiguity', 'UNH').
card_original_type('ambiguity'/'UNH', 'Enchantment').
card_original_text('ambiguity'/'UNH', 'Whenever a player plays a spell that counters a spell that has been played or a player plays a spell that comes into play with counters, that player may counter the next spell played or put an additional counter on a permanent that has already been played, but not countered.').
card_first_print('ambiguity', 'UNH').
card_image_name('ambiguity'/'UNH', 'ambiguity').
card_uid('ambiguity'/'UNH', 'UNH:Ambiguity:ambiguity').
card_rarity('ambiguity'/'UNH', 'Rare').
card_artist('ambiguity'/'UNH', 'Stephen Daniele').
card_number('ambiguity'/'UNH', '24').
card_multiverse_id('ambiguity'/'UNH', '74317').

card_in_set('artful looter', 'UNH').
card_original_type('artful looter'/'UNH', 'Creature — Human Wizard').
card_original_text('artful looter'/'UNH', '{T}: Draw a card, then discard a card.\nWhenever a permanent comes into play that shares an artist with another permanent you control, untap Artful Looter.').
card_first_print('artful looter', 'UNH').
card_image_name('artful looter'/'UNH', 'artful looter').
card_uid('artful looter'/'UNH', 'UNH:Artful Looter:artful looter').
card_rarity('artful looter'/'UNH', 'Common').
card_artist('artful looter'/'UNH', 'Heather “Erica Gassalasca-Jape” Hudson').
card_number('artful looter'/'UNH', '25').
card_flavor_text('artful looter'/'UNH', 'PRICELESS PIC PILFERED\nPainting Found Abandoned in Graveyard\nMerfolk Decry Wizards\' Treatment').
card_multiverse_id('artful looter'/'UNH', '74356').

card_in_set('ass whuppin\'', 'UNH').
card_original_type('ass whuppin\''/'UNH', 'Sorcery').
card_original_text('ass whuppin\''/'UNH', 'Destroy target silver-bordered permanent in any game you can see from your seat.').
card_image_name('ass whuppin\''/'UNH', 'ass whuppin\'').
card_uid('ass whuppin\''/'UNH', 'UNH:Ass Whuppin\':ass whuppin\'').
card_rarity('ass whuppin\''/'UNH', 'Rare').
card_artist('ass whuppin\''/'UNH', 'Don Hazeltine').
card_number('ass whuppin\''/'UNH', '117').
card_flavor_text('ass whuppin\''/'UNH', 'Asses to ashes,\nDonkeys to dust.').
card_multiverse_id('ass whuppin\''/'UNH', '74329').

card_in_set('assquatch', 'UNH').
card_original_type('assquatch'/'UNH', 'Creature — Donkey Lord').
card_original_text('assquatch'/'UNH', 'Each other Donkey gets +1½/+1½.\nWhenever another Donkey comes into play, untap target creature and gain control of it until end of turn. That creature gains haste until end of turn.').
card_first_print('assquatch', 'UNH').
card_image_name('assquatch'/'UNH', 'assquatch').
card_uid('assquatch'/'UNH', 'UNH:Assquatch:assquatch').
card_rarity('assquatch'/'UNH', 'Rare').
card_artist('assquatch'/'UNH', 'Jeremy Jarvis').
card_number('assquatch'/'UNH', '71').
card_flavor_text('assquatch'/'UNH', 'Who wants to see a picture of a big hairy ass?').
card_multiverse_id('assquatch'/'UNH', '74251').

card_in_set('atinlay igpay', 'UNH').
card_original_type('atinlay igpay'/'UNH', 'Eaturecray — Igpay').
card_original_text('atinlay igpay'/'UNH', 'Oubleday ikestray\nEneverwhay Atinlay Igpay\'s ontrollercay eaksspay ay onnay-Igpay-Atinlay ordway, acrificesay Atinlay Igpay.').
card_first_print('atinlay igpay', 'UNH').
card_image_name('atinlay igpay'/'UNH', 'atinlay igpay').
card_uid('atinlay igpay'/'UNH', 'UNH:Atinlay Igpay:atinlay igpay').
card_rarity('atinlay igpay'/'UNH', 'Uncommon').
card_artist('atinlay igpay'/'UNH', 'Evkay Alkerway').
card_number('atinlay igpay'/'UNH', '1').
card_flavor_text('atinlay igpay'/'UNH', 'Isthay iecepay ofay avorflay exttay oesn\'tday eallyray aysay anythingay.').
card_multiverse_id('atinlay igpay'/'UNH', '73937').

card_in_set('avatar of me', 'UNH').
card_original_type('avatar of me'/'UNH', 'Creature — Avatar').
card_original_text('avatar of me'/'UNH', 'Avatar of Me costs {1} more to play for each ten years you\'ve been alive.\nAvatar of Me\'s power is equal to your height in feet and its toughness is equal to your American shoe size. Round to the nearest ½.\nAvatar of Me\'s color is the color of your eyes.').
card_first_print('avatar of me', 'UNH').
card_image_name('avatar of me'/'UNH', 'avatar of me').
card_uid('avatar of me'/'UNH', 'UNH:Avatar of Me:avatar of me').
card_rarity('avatar of me'/'UNH', 'Rare').
card_artist('avatar of me'/'UNH', 'Greg Hildebrandt').
card_number('avatar of me'/'UNH', '26').
card_multiverse_id('avatar of me'/'UNH', '74254').

card_in_set('awol', 'UNH').
card_original_type('awol'/'UNH', 'Instant').
card_original_text('awol'/'UNH', 'Remove target attacking creature from the game. Then remove it from the removed-from-game zone and put it into the absolutely-removed-from-the-freaking-game-forever zone.').
card_first_print('awol', 'UNH').
card_image_name('awol'/'UNH', 'awol').
card_uid('awol'/'UNH', 'UNH:AWOL:awol').
card_rarity('awol'/'UNH', 'Common').
card_artist('awol'/'UNH', 'Stephen Tappin').
card_number('awol'/'UNH', '2').
card_multiverse_id('awol'/'UNH', '74231').

card_in_set('b-i-n-g-o', 'UNH').
card_original_type('b-i-n-g-o'/'UNH', 'Creature — Hound').
card_original_text('b-i-n-g-o'/'UNH', 'Trample\nWhenever a player plays a spell, put a chip counter on its converted mana cost.  \nB-I-N-G-O gets +9/+9 for each set of three numbers in a row with chip counters on them.').
card_first_print('b-i-n-g-o', 'UNH').
card_image_name('b-i-n-g-o'/'UNH', 'b-i-n-g-o').
card_uid('b-i-n-g-o'/'UNH', 'UNH:B-I-N-G-O:b-i-n-g-o').
card_rarity('b-i-n-g-o'/'UNH', 'Rare').
card_artist('b-i-n-g-o'/'UNH', 'Ron Spencer').
card_number('b-i-n-g-o'/'UNH', '92').
card_flavor_text('b-i-n-g-o'/'UNH', 'There was a farmer, had a hound . . .').
card_multiverse_id('b-i-n-g-o'/'UNH', '73938').

card_in_set('bad ass', 'UNH').
card_original_type('bad ass'/'UNH', 'Creature — Donkey Zombie').
card_original_text('bad ass'/'UNH', '{1}{B}, Growl: Regenerate Bad Ass.').
card_first_print('bad ass', 'UNH').
card_image_name('bad ass'/'UNH', 'bad ass').
card_uid('bad ass'/'UNH', 'UNH:Bad Ass:bad ass').
card_rarity('bad ass'/'UNH', 'Common').
card_artist('bad ass'/'UNH', 'Thomas M. Baxa').
card_number('bad ass'/'UNH', '49').
card_flavor_text('bad ass'/'UNH', 'He wanders the land in search of Good Ass.').
card_multiverse_id('bad ass'/'UNH', '74350').

card_in_set('blast from the past', 'UNH').
card_original_type('blast from the past'/'UNH', 'Instant').
card_original_text('blast from the past'/'UNH', 'Madness {R}, cycling {1}{R}, kicker {2}{R}, flashback {3}{R}, buyback {4}{R}\nBlast from the Past deals 2 damage to target creature or player.\nIf the kicker cost was paid, put a 1/1 red Goblin creature token into play.').
card_first_print('blast from the past', 'UNH').
card_image_name('blast from the past'/'UNH', 'blast from the past').
card_uid('blast from the past'/'UNH', 'UNH:Blast from the Past:blast from the past').
card_rarity('blast from the past'/'UNH', 'Rare').
card_artist('blast from the past'/'UNH', 'Douglas Shuler').
card_number('blast from the past'/'UNH', '72').
card_multiverse_id('blast from the past'/'UNH', '74227').

card_in_set('bloodletter', 'UNH').
card_original_type('bloodletter'/'UNH', 'Creature — Zombie').
card_original_text('bloodletter'/'UNH', 'When the names of three or more nonland permanents begin with the same letter, sacrifice Bloodletter. If you do, it deals 2 damage to each creature and each player.').
card_first_print('bloodletter', 'UNH').
card_image_name('bloodletter'/'UNH', 'bloodletter').
card_uid('bloodletter'/'UNH', 'UNH:Bloodletter:bloodletter').
card_rarity('bloodletter'/'UNH', 'Common').
card_artist('bloodletter'/'UNH', 'Daren Bader').
card_number('bloodletter'/'UNH', '50').
card_flavor_text('bloodletter'/'UNH', '\"That\'s not an A, it\'s an Æ. Wait, don\'t—\"\n—Hœbrus Væm, Ærathi librarian, last words').
card_multiverse_id('bloodletter'/'UNH', '73979').

card_in_set('booster tutor', 'UNH').
card_original_type('booster tutor'/'UNH', 'Instant').
card_original_text('booster tutor'/'UNH', 'Open a sealed Magic booster pack, reveal the cards, and put one of those cards into your hand. (Remove that card from your deck before beginning a new game.)').
card_image_name('booster tutor'/'UNH', 'booster tutor').
card_uid('booster tutor'/'UNH', 'UNH:Booster Tutor:booster tutor').
card_rarity('booster tutor'/'UNH', 'Uncommon').
card_artist('booster tutor'/'UNH', 'Christopher Rush').
card_number('booster tutor'/'UNH', '51').
card_flavor_text('booster tutor'/'UNH', 'Real men use Arabian Nights boosters.').
card_multiverse_id('booster tutor'/'UNH', '73939').

card_in_set('bosom buddy', 'UNH').
card_original_type('bosom buddy'/'UNH', 'Creature — Elephant Townsfolk').
card_original_text('bosom buddy'/'UNH', 'Whenever you play a spell, you may gain ½ life for each word in that spell\'s name.').
card_first_print('bosom buddy', 'UNH').
card_image_name('bosom buddy'/'UNH', 'bosom buddy').
card_uid('bosom buddy'/'UNH', 'UNH:Bosom Buddy:bosom buddy').
card_rarity('bosom buddy'/'UNH', 'Uncommon').
card_artist('bosom buddy'/'UNH', 'Dan Scott').
card_number('bosom buddy'/'UNH', '3').
card_flavor_text('bosom buddy'/'UNH', '\"Step 1: I believe in a power and toughness greater than myself . . . .\"').
card_multiverse_id('bosom buddy'/'UNH', '73977').

card_in_set('brushstroke paintermage', 'UNH').
card_original_type('brushstroke paintermage'/'UNH', 'Creature — Human Wizard').
card_original_text('brushstroke paintermage'/'UNH', '{T}: Target permanent\'s artist becomes the artist of your choice until end of turn.').
card_first_print('brushstroke paintermage', 'UNH').
card_image_name('brushstroke paintermage'/'UNH', 'brushstroke paintermage').
card_uid('brushstroke paintermage'/'UNH', 'UNH:Brushstroke Paintermage:brushstroke paintermage').
card_rarity('brushstroke paintermage'/'UNH', 'Common').
card_artist('brushstroke paintermage'/'UNH', 'Ron “Don\'t You Dare Change Me” Spears').
card_number('brushstroke paintermage'/'UNH', '27').
card_flavor_text('brushstroke paintermage'/'UNH', 'He gives credit where credit isn\'t due.').
card_multiverse_id('brushstroke paintermage'/'UNH', '74249').

card_in_set('bursting beebles', 'UNH').
card_original_type('bursting beebles'/'UNH', 'Creature — Beeble').
card_original_text('bursting beebles'/'UNH', 'Bursting Beebles is unblockable as long as defending player controls two or more nonland permanents that share an artist.').
card_first_print('bursting beebles', 'UNH').
card_image_name('bursting beebles'/'UNH', 'bursting beebles').
card_uid('bursting beebles'/'UNH', 'UNH:Bursting Beebles:bursting beebles').
card_rarity('bursting beebles'/'UNH', 'Common').
card_artist('bursting beebles'/'UNH', 'David “Beeblemania” Martin').
card_number('bursting beebles'/'UNH', '28').
card_flavor_text('bursting beebles'/'UNH', 'Like thousands of others, the beebles quit Magic for several years following the release of the Mercadian Masques set.').
card_multiverse_id('bursting beebles'/'UNH', '74289').

card_in_set('cardpecker', 'UNH').
card_original_type('cardpecker'/'UNH', 'Creature — Bird').
card_original_text('cardpecker'/'UNH', 'Flying\nGotcha Whenever an opponent touches the table with his or her hand, you may say \"Gotcha\" If you do, return Cardpecker from your graveyard to your hand.').
card_first_print('cardpecker', 'UNH').
card_image_name('cardpecker'/'UNH', 'cardpecker').
card_uid('cardpecker'/'UNH', 'UNH:Cardpecker:cardpecker').
card_rarity('cardpecker'/'UNH', 'Common').
card_artist('cardpecker'/'UNH', 'Richard Sardinha').
card_number('cardpecker'/'UNH', '4').
card_multiverse_id('cardpecker'/'UNH', '74244').

card_in_set('carnivorous death-parrot', 'UNH').
card_original_type('carnivorous death-parrot'/'UNH', 'Creature — Bird').
card_original_text('carnivorous death-parrot'/'UNH', 'Flying\nAt the beginning of your upkeep, sacrifice Carnivorous Death-Parrot unless you say its flavor text.').
card_first_print('carnivorous death-parrot', 'UNH').
card_image_name('carnivorous death-parrot'/'UNH', 'carnivorous death-parrot').
card_uid('carnivorous death-parrot'/'UNH', 'UNH:Carnivorous Death-Parrot:carnivorous death-parrot').
card_rarity('carnivorous death-parrot'/'UNH', 'Common').
card_artist('carnivorous death-parrot'/'UNH', 'Tim Hildebrandt').
card_number('carnivorous death-parrot'/'UNH', '29').
card_flavor_text('carnivorous death-parrot'/'UNH', 'Save a kill spell to deal with this guy.').
card_multiverse_id('carnivorous death-parrot'/'UNH', '74236').

card_in_set('cheap ass', 'UNH').
card_original_type('cheap ass'/'UNH', 'Creature — Donkey Townsfolk').
card_original_text('cheap ass'/'UNH', 'Spells you play cost  less to play.').
card_first_print('cheap ass', 'UNH').
card_image_name('cheap ass'/'UNH', 'cheap ass').
card_uid('cheap ass'/'UNH', 'UNH:Cheap Ass:cheap ass').
card_rarity('cheap ass'/'UNH', 'Common').
card_artist('cheap ass'/'UNH', 'Randy Gallegos').
card_number('cheap ass'/'UNH', '5').
card_flavor_text('cheap ass'/'UNH', 'And you thought Yawgmoth drove a hard bargain.').
card_multiverse_id('cheap ass'/'UNH', '74220').

card_in_set('cheatyface', 'UNH').
card_original_type('cheatyface'/'UNH', 'Creature — Efreet').
card_original_text('cheatyface'/'UNH', 'You may sneak Cheatyface into play at any time without paying for it, but if an opponent catches you right away, that player may remove Cheatyface from the game.\nFlying').
card_first_print('cheatyface', 'UNH').
card_image_name('cheatyface'/'UNH', 'cheatyface').
card_uid('cheatyface'/'UNH', 'UNH:Cheatyface:cheatyface').
card_rarity('cheatyface'/'UNH', 'Uncommon').
card_artist('cheatyface'/'UNH', 'Doug Chaffee').
card_number('cheatyface'/'UNH', '30').
card_multiverse_id('cheatyface'/'UNH', '74306').

card_in_set('circle of protection: art', 'UNH').
card_original_type('circle of protection: art'/'UNH', 'Enchantment').
card_original_text('circle of protection: art'/'UNH', 'As Circle of Protection: Art comes into play, choose an artist.\n{1}{W}: The next time a source of your choice by the chosen artist would deal damage to you this turn, prevent that damage.\n{1}{W}: Return Circle of Protection: Art to its owner\'s hand.').
card_image_name('circle of protection: art'/'UNH', 'circle of protection art').
card_uid('circle of protection: art'/'UNH', 'UNH:Circle of Protection: Art:circle of protection art').
card_rarity('circle of protection: art'/'UNH', 'Common').
card_artist('circle of protection: art'/'UNH', 'Jim “Stop the Da Vinci Beatdown” Pavelec').
card_number('circle of protection: art'/'UNH', '6').
card_multiverse_id('circle of protection: art'/'UNH', '73940').

card_in_set('city of ass', 'UNH').
card_original_type('city of ass'/'UNH', 'Land').
card_original_text('city of ass'/'UNH', 'City of Ass comes into play tapped.\n{T}: Add one and one-half mana of any one color to your mana pool.').
card_first_print('city of ass', 'UNH').
card_image_name('city of ass'/'UNH', 'city of ass').
card_uid('city of ass'/'UNH', 'UNH:City of Ass:city of ass').
card_rarity('city of ass'/'UNH', 'Rare').
card_artist('city of ass'/'UNH', 'John Avon').
card_number('city of ass'/'UNH', '134').
card_flavor_text('city of ass'/'UNH', '\"But . . .\"').
card_multiverse_id('city of ass'/'UNH', '74327').

card_in_set('collector protector', 'UNH').
card_original_type('collector protector'/'UNH', 'Creature — Human Gamer').
card_original_text('collector protector'/'UNH', '{W}, Give an opponent a nonland card you own from outside the game: Prevent the next 1 damage that would be dealt to you or Collector Protector this turn.').
card_first_print('collector protector', 'UNH').
card_image_name('collector protector'/'UNH', 'collector protector').
card_uid('collector protector'/'UNH', 'UNH:Collector Protector:collector protector').
card_rarity('collector protector'/'UNH', 'Rare').
card_artist('collector protector'/'UNH', 'Christopher Rush').
card_number('collector protector'/'UNH', '7').
card_flavor_text('collector protector'/'UNH', '\"Here—have a Mudhole.\"').
card_multiverse_id('collector protector'/'UNH', '74354').

card_in_set('creature guy', 'UNH').
card_original_type('creature guy'/'UNH', 'Creature — Beast').
card_original_text('creature guy'/'UNH', 'Gotcha Whenever an opponent says \"Creature\" or \"Guy,\" you may say \"Gotcha\" If you do, return Creature Guy from your graveyard to your hand.').
card_first_print('creature guy', 'UNH').
card_image_name('creature guy'/'UNH', 'creature guy').
card_uid('creature guy'/'UNH', 'UNH:Creature Guy:creature guy').
card_rarity('creature guy'/'UNH', 'Uncommon').
card_artist('creature guy'/'UNH', 'Jeff Easley').
card_number('creature guy'/'UNH', '93').
card_flavor_text('creature guy'/'UNH', 'There once was a player named Quinn, . . .').
card_multiverse_id('creature guy'/'UNH', '74303').

card_in_set('curse of the fire penguin', 'UNH').
card_original_type('curse of the fire penguin'/'UNH', 'Enchant Creature').
card_original_text('curse of the fire penguin'/'UNH', 'Curse of the Fire Penquin consumes and confuses enchanted creature.\n-----\nCreature Penguin\nTrample\nWhen this creature is put into a graveyard from play, return Curse of the Fire Penguin from your graveyard to play.\n6/5').
card_first_print('curse of the fire penguin', 'UNH').
card_image_name('curse of the fire penguin'/'UNH', 'curse of the fire penguin').
card_uid('curse of the fire penguin'/'UNH', 'UNH:Curse of the Fire Penguin:curse of the fire penguin').
card_rarity('curse of the fire penguin'/'UNH', 'Rare').
card_artist('curse of the fire penguin'/'UNH', 'Matt Thompson').
card_number('curse of the fire penguin'/'UNH', '73').
card_multiverse_id('curse of the fire penguin'/'UNH', '73956').

card_in_set('deal damage', 'UNH').
card_original_type('deal damage'/'UNH', 'Instant').
card_original_text('deal damage'/'UNH', 'Deal Damage deals 4 damage to target creature or player.\nGotcha Whenever an opponent says \"Deal\" or \"Damage,\" you may say \"Gotcha\" If you do, return Deal Damage from your graveyard to your hand.').
card_first_print('deal damage', 'UNH').
card_image_name('deal damage'/'UNH', 'deal damage').
card_uid('deal damage'/'UNH', 'UNH:Deal Damage:deal damage').
card_rarity('deal damage'/'UNH', 'Uncommon').
card_artist('deal damage'/'UNH', 'Scott M. Fischer').
card_number('deal damage'/'UNH', '74').
card_flavor_text('deal damage'/'UNH', 'Until he got singed . . .').
card_multiverse_id('deal damage'/'UNH', '74359').

card_in_set('double header', 'UNH').
card_original_type('double header'/'UNH', 'Creature — Drake').
card_original_text('double header'/'UNH', 'Flying\nWhen Double Header comes into play, you may return target permanent with a two-word name to its owner\'s hand.').
card_first_print('double header', 'UNH').
card_image_name('double header'/'UNH', 'double header').
card_uid('double header'/'UNH', 'UNH:Double Header:double header').
card_rarity('double header'/'UNH', 'Common').
card_artist('double header'/'UNH', 'Richard Sardinha').
card_number('double header'/'UNH', '31').
card_flavor_text('double header'/'UNH', 'Players that don\'t read flavor text aren\'t too bright, sorta smell, and dress funny. But let\'s just keep this between us, okay? They can get kind of violent.').
card_multiverse_id('double header'/'UNH', '74262').

card_in_set('drawn together', 'UNH').
card_original_type('drawn together'/'UNH', 'Enchantment').
card_original_text('drawn together'/'UNH', 'As Drawn Together comes into play, choose an artist.\nCreatures by the chosen artist get +2/+2.').
card_first_print('drawn together', 'UNH').
card_image_name('drawn together'/'UNH', 'drawn together').
card_uid('drawn together'/'UNH', 'UNH:Drawn Together:drawn together').
card_rarity('drawn together'/'UNH', 'Rare').
card_artist('drawn together'/'UNH', 'Pete “Fear Me” Venters').
card_number('drawn together'/'UNH', '8').
card_flavor_text('drawn together'/'UNH', '\"My Pete Venters theme deck is nearly complete. Then I shall be unstoppable Bwahahahaaa\"\n—Pete Venters').
card_multiverse_id('drawn together'/'UNH', '74325').

card_in_set('duh', 'UNH').
card_original_type('duh'/'UNH', 'Instant').
card_original_text('duh'/'UNH', 'Destroy target creature with reminder text. (Reminder text is any italicized text in parentheses that explains rules you already know.)').
card_first_print('duh', 'UNH').
card_image_name('duh'/'UNH', 'duh').
card_uid('duh'/'UNH', 'UNH:Duh:duh').
card_rarity('duh'/'UNH', 'Common').
card_artist('duh'/'UNH', 'Dave Dorman').
card_number('duh'/'UNH', '52').
card_flavor_text('duh'/'UNH', 'Will players understand that the creature is being crushed by two parentheses?\nNot a chance.').
card_multiverse_id('duh'/'UNH', '73943').

card_in_set('dumb ass', 'UNH').
card_original_type('dumb ass'/'UNH', 'Creature — Donkey Barbarian').
card_original_text('dumb ass'/'UNH', 'At the beginning of your upkeep, flip a coin. If you lose the flip, target opponent chooses whether Dumb Ass attacks this turn.').
card_first_print('dumb ass', 'UNH').
card_image_name('dumb ass'/'UNH', 'dumb ass').
card_uid('dumb ass'/'UNH', 'UNH:Dumb Ass:dumb ass').
card_rarity('dumb ass'/'UNH', 'Common').
card_artist('dumb ass'/'UNH', 'Bradley Williams').
card_number('dumb ass'/'UNH', '75').
card_flavor_text('dumb ass'/'UNH', 'Envying the intelligence of goblins is a bad place to be.').
card_multiverse_id('dumb ass'/'UNH', '74287').

card_in_set('elvish house party', 'UNH').
card_original_type('elvish house party'/'UNH', 'Creature — Elf Rogue').
card_original_text('elvish house party'/'UNH', 'Elvish House Party\'s power and toughness are each equal to the current hour, using the twelve-hour system.').
card_first_print('elvish house party', 'UNH').
card_image_name('elvish house party'/'UNH', 'elvish house party').
card_uid('elvish house party'/'UNH', 'UNH:Elvish House Party:elvish house party').
card_rarity('elvish house party'/'UNH', 'Uncommon').
card_artist('elvish house party'/'UNH', 'Thomas Manning').
card_number('elvish house party'/'UNH', '94').
card_flavor_text('elvish house party'/'UNH', 'The party was going strong for nearly an hour past midnight—then all of a sudden it just petered out.').
card_multiverse_id('elvish house party'/'UNH', '73961').

card_in_set('emcee', 'UNH').
card_original_type('emcee'/'UNH', 'Creature — Human Rogue').
card_original_text('emcee'/'UNH', 'Whenever another creature comes into play, you may stand up and say in a deep, booming voice \"Presenting . . . \" and that creature\'s name. If you do, put a +1/+1 counter on that creature.').
card_first_print('emcee', 'UNH').
card_image_name('emcee'/'UNH', 'emcee').
card_uid('emcee'/'UNH', 'UNH:Emcee:emcee').
card_rarity('emcee'/'UNH', 'Uncommon').
card_artist('emcee'/'UNH', 'Quinton Hoover').
card_number('emcee'/'UNH', '9').
card_flavor_text('emcee'/'UNH', 'The ogre appealed the match loss and got it downgraded to a warning.').
card_multiverse_id('emcee'/'UNH', '73944').

card_in_set('enter the dungeon', 'UNH').
card_original_type('enter the dungeon'/'UNH', 'Sorcery').
card_original_text('enter the dungeon'/'UNH', 'Players play a Magic subgame under the table starting at 5 life, using their libraries as their decks. After the subgame ends, the winner searches his or her library for two cards, puts those cards into his or her hand, then shuffles his or her library.').
card_first_print('enter the dungeon', 'UNH').
card_image_name('enter the dungeon'/'UNH', 'enter the dungeon').
card_uid('enter the dungeon'/'UNH', 'UNH:Enter the Dungeon:enter the dungeon').
card_rarity('enter the dungeon'/'UNH', 'Rare').
card_artist('enter the dungeon'/'UNH', 'Luca Zontini').
card_number('enter the dungeon'/'UNH', '53').
card_multiverse_id('enter the dungeon'/'UNH', '74312').

card_in_set('erase (not the urza\'s legacy one)', 'UNH').
card_original_type('erase (not the urza\'s legacy one)'/'UNH', 'Instant').
card_original_text('erase (not the urza\'s legacy one)'/'UNH', 'If you control two or more white permanents that share an artist, you may play Erase (Not the Urza\'s Legacy One) without paying its mana cost.\nRemove target enchantment from the game.').
card_first_print('erase (not the urza\'s legacy one)', 'UNH').
card_image_name('erase (not the urza\'s legacy one)'/'UNH', 'erase (not the urza\'s legacy one)').
card_uid('erase (not the urza\'s legacy one)'/'UNH', 'UNH:Erase (Not the Urza\'s Legacy One):erase (not the urza\'s legacy one)').
card_rarity('erase (not the urza\'s legacy one)'/'UNH', 'Common').
card_artist('erase (not the urza\'s legacy one)'/'UNH', 'Pete “Yes the Urza\'s Legacy One” Venters').
card_number('erase (not the urza\'s legacy one)'/'UNH', '10').
card_flavor_text('erase (not the urza\'s legacy one)'/'UNH', 'Synonyms for \"erase\" we haven\'t used: ablate, abrade, chafe, fray, frazzle, scuff, and scrub.').
card_multiverse_id('erase (not the urza\'s legacy one)'/'UNH', '74275').

card_in_set('eye to eye', 'UNH').
card_original_type('eye to eye'/'UNH', 'Instant').
card_original_text('eye to eye'/'UNH', 'You and target creature\'s controller have a staring contest. If you win, destroy that creature.').
card_first_print('eye to eye', 'UNH').
card_image_name('eye to eye'/'UNH', 'eye to eye').
card_uid('eye to eye'/'UNH', 'UNH:Eye to Eye:eye to eye').
card_rarity('eye to eye'/'UNH', 'Uncommon').
card_artist('eye to eye'/'UNH', 'Michael Sutfin').
card_number('eye to eye'/'UNH', '54').
card_flavor_text('eye to eye'/'UNH', 'True, Gargox had blinded himself for life, but the glass eyes never lost.').
card_multiverse_id('eye to eye'/'UNH', '73936').

card_in_set('face to face', 'UNH').
card_original_type('face to face'/'UNH', 'Sorcery').
card_original_text('face to face'/'UNH', 'You and target opponent play a best two-out-of-three Rock, Paper, Scissors match. If you win, Face to Face deals 5 damage to that opponent.').
card_first_print('face to face', 'UNH').
card_image_name('face to face'/'UNH', 'face to face').
card_uid('face to face'/'UNH', 'UNH:Face to Face:face to face').
card_rarity('face to face'/'UNH', 'Uncommon').
card_artist('face to face'/'UNH', 'Randy Gallegos').
card_number('face to face'/'UNH', '76').
card_flavor_text('face to face'/'UNH', 'Befuddled by this new game, Argg longed for the days of Rock, Rock, Rock.').
card_multiverse_id('face to face'/'UNH', '73966').

card_in_set('farewell to arms', 'UNH').
card_original_type('farewell to arms'/'UNH', 'Enchantment').
card_original_text('farewell to arms'/'UNH', 'As Farewell to Arms comes into play, choose a hand attached to an opponent\'s arm.\nWhen the chosen hand isn\'t behind its owner\'s back, sacrifice Farewell to Arms. If you do, that player discards his or her hand . . . of cards. (The lawyers wouldn\'t let us do it the other way.)').
card_first_print('farewell to arms', 'UNH').
card_image_name('farewell to arms'/'UNH', 'farewell to arms').
card_uid('farewell to arms'/'UNH', 'UNH:Farewell to Arms:farewell to arms').
card_rarity('farewell to arms'/'UNH', 'Common').
card_artist('farewell to arms'/'UNH', 'Ron Lemen').
card_number('farewell to arms'/'UNH', '56').
card_multiverse_id('farewell to arms'/'UNH', '74300').

card_in_set('fascist art director', 'UNH').
card_original_type('fascist art director'/'UNH', 'Creature — Human Horror').
card_original_text('fascist art director'/'UNH', '{W}{W}: Fascist Art Director gains protection from the artist of your choice until end of turn..').
card_first_print('fascist art director', 'UNH').
card_image_name('fascist art director'/'UNH', 'fascist art director').
card_uid('fascist art director'/'UNH', 'UNH:Fascist Art Director:fascist art director').
card_rarity('fascist art director'/'UNH', 'Common').
card_artist('fascist art director'/'UNH', 'Edward P. “Feed Me” Beard, Jr.').
card_number('fascist art director'/'UNH', '11').
card_flavor_text('fascist art director'/'UNH', 'Dear Mr. and Mrs. Cranford,\nAfter careful analysis of Jeremy\'s vocational testing, I feel that he is best suited for a career in either torture or art direction.').
card_multiverse_id('fascist art director'/'UNH', '74348').

card_in_set('fat ass', 'UNH').
card_original_type('fat ass'/'UNH', 'Creature — Donkey Shaman').
card_original_text('fat ass'/'UNH', 'Fat Ass gets +2/+2 and has trample as long as you\'re eating. (Food is in your mouth and you\'re chewing, licking, sucking, or swallowing it.)').
card_first_print('fat ass', 'UNH').
card_image_name('fat ass'/'UNH', 'fat ass').
card_uid('fat ass'/'UNH', 'UNH:Fat Ass:fat ass').
card_rarity('fat ass'/'UNH', 'Common').
card_artist('fat ass'/'UNH', 'Jeremy Jarvis').
card_number('fat ass'/'UNH', '95').
card_flavor_text('fat ass'/'UNH', 'Our lawyers say no matter how funny it would be, we can\'t encourage players to eat the cards. Hear that? Whatever you do, don\'t eat the delicious cards.').
card_multiverse_id('fat ass'/'UNH', '74224').

card_in_set('first come, first served', 'UNH').
card_original_type('first come, first served'/'UNH', 'Enchantment').
card_original_text('first come, first served'/'UNH', 'The attacking or blocking creature with the lowest collector number has first strike. If two or more creatures are tied, they all have first strike.').
card_first_print('first come, first served', 'UNH').
card_image_name('first come, first served'/'UNH', 'first come, first served').
card_uid('first come, first served'/'UNH', 'UNH:First Come, First Served:first come, first served').
card_rarity('first come, first served'/'UNH', 'Uncommon').
card_artist('first come, first served'/'UNH', 'Thomas Gianni').
card_number('first come, first served'/'UNH', '12').
card_flavor_text('first come, first served'/'UNH', 'The Yotian soldiers hoped that one day R&D would make an artifact creature whose name starts with Z. And in their dreams, it\'s a 4/1.').
card_multiverse_id('first come, first served'/'UNH', '74261').

card_in_set('flaccify', 'UNH').
card_original_type('flaccify'/'UNH', 'Instant').
card_original_text('flaccify'/'UNH', 'Counter target spell unless its controller pays {3}.').
card_first_print('flaccify', 'UNH').
card_image_name('flaccify'/'UNH', 'flaccify').
card_uid('flaccify'/'UNH', 'UNH:Flaccify:flaccify').
card_rarity('flaccify'/'UNH', 'Common').
card_artist('flaccify'/'UNH', 'Tim Hildebrandt').
card_number('flaccify'/'UNH', '32').
card_flavor_text('flaccify'/'UNH', '\"Don\'t worry about it. It happens to every mage sooner or later.\"').
card_multiverse_id('flaccify'/'UNH', '74330').

card_in_set('forest', 'UNH').
card_original_type('forest'/'UNH', '(none)').
card_original_text('forest'/'UNH', '').
card_border('forest'/'UNH', 'black').
card_image_name('forest'/'UNH', 'forest').
card_uid('forest'/'UNH', 'UNH:Forest:forest').
card_rarity('forest'/'UNH', 'Basic Land').
card_artist('forest'/'UNH', 'John Avon').
card_number('forest'/'UNH', '140').
card_multiverse_id('forest'/'UNH', '73946').

card_in_set('form of the squirrel', 'UNH').
card_original_type('form of the squirrel'/'UNH', 'Enchantment').
card_original_text('form of the squirrel'/'UNH', 'As Form of the Squirrel comes into play, put a 1/1 green Squirrel creature token into play. You lose the game when it leaves play.\nCreatures can\'t attack you.\nYou can\'t be the target of spells or abilities.\nYou can\'t play spells.').
card_first_print('form of the squirrel', 'UNH').
card_image_name('form of the squirrel'/'UNH', 'form of the squirrel').
card_uid('form of the squirrel'/'UNH', 'UNH:Form of the Squirrel:form of the squirrel').
card_rarity('form of the squirrel'/'UNH', 'Rare').
card_artist('form of the squirrel'/'UNH', 'Carl Critchlow').
card_number('form of the squirrel'/'UNH', '96').
card_multiverse_id('form of the squirrel'/'UNH', '74318').

card_in_set('fraction jackson', 'UNH').
card_original_type('fraction jackson'/'UNH', 'Creature — Human Hero').
card_original_text('fraction jackson'/'UNH', '{G}, {T}: Return target card with a ½ on it from your graveyard to your hand.').
card_first_print('fraction jackson', 'UNH').
card_image_name('fraction jackson'/'UNH', 'fraction jackson').
card_uid('fraction jackson'/'UNH', 'UNH:Fraction Jackson:fraction jackson').
card_rarity('fraction jackson'/'UNH', 'Rare').
card_artist('fraction jackson'/'UNH', 'Mark Poole').
card_number('fraction jackson'/'UNH', '97').
card_flavor_text('fraction jackson'/'UNH', 'Bitten by radioactive beebles in a freak algebra accident, young Ricky Robertson discovered he\'d gained the ability to harness the awesome power of fractions').
card_multiverse_id('fraction jackson'/'UNH', '74301').

card_in_set('framed!', 'UNH').
card_original_type('framed!'/'UNH', 'Instant').
card_original_text('framed!'/'UNH', 'Tap or untap all permanents by the artist of your choice.').
card_first_print('framed!', 'UNH').
card_image_name('framed!'/'UNH', 'framed!').
card_uid('framed!'/'UNH', 'UNH:Framed!:framed!').
card_rarity('framed!'/'UNH', 'Common').
card_artist('framed!'/'UNH', 'Alan “Don\'t Feel Like You Have to Pick Me” Pollack').
card_number('framed!'/'UNH', '33').
card_flavor_text('framed!'/'UNH', 'Who knew an 8˝ by 10˝ could feel like a 2˝ by 4˝?').
card_multiverse_id('framed!'/'UNH', '74218').

card_in_set('frankie peanuts', 'UNH').
card_original_type('frankie peanuts'/'UNH', 'Legendary Creature — Elephant Rogue').
card_original_text('frankie peanuts'/'UNH', 'At the beginning of your upkeep, you may ask target player a yes-or-no question. If you do, that player answers the question truthfully and abides by that answer if able until end of turn.').
card_first_print('frankie peanuts', 'UNH').
card_image_name('frankie peanuts'/'UNH', 'frankie peanuts').
card_uid('frankie peanuts'/'UNH', 'UNH:Frankie Peanuts:frankie peanuts').
card_rarity('frankie peanuts'/'UNH', 'Rare').
card_artist('frankie peanuts'/'UNH', 'Thomas M. Baxa').
card_number('frankie peanuts'/'UNH', '13').
card_flavor_text('frankie peanuts'/'UNH', 'Don\'t cross him or you\'ll end up sleeping with the merfolk.').
card_multiverse_id('frankie peanuts'/'UNH', '73950').

card_in_set('frazzled editor', 'UNH').
card_original_type('frazzled editor'/'UNH', 'Creature — Human Bureaucrat').
card_original_text('frazzled editor'/'UNH', 'Protection from wordy (Something is wordy if it has four or more lines of text in its text box.)').
card_first_print('frazzled editor', 'UNH').
card_image_name('frazzled editor'/'UNH', 'frazzled editor').
card_uid('frazzled editor'/'UNH', 'UNH:Frazzled Editor:frazzled editor').
card_rarity('frazzled editor'/'UNH', 'Common').
card_artist('frazzled editor'/'UNH', 'Jim Pavelec').
card_number('frazzled editor'/'UNH', '77').
card_flavor_text('frazzled editor'/'UNH', 'The pen is mightier than the sword.').
card_multiverse_id('frazzled editor'/'UNH', '73941').

card_in_set('gleemax', 'UNH').
card_original_type('gleemax'/'UNH', 'Legendary Artifact').
card_original_text('gleemax'/'UNH', 'You choose all targets for all spells and abilities.').
card_first_print('gleemax', 'UNH').
card_image_name('gleemax'/'UNH', 'gleemax').
card_uid('gleemax'/'UNH', 'UNH:Gleemax:gleemax').
card_rarity('gleemax'/'UNH', 'Rare').
card_artist('gleemax'/'UNH', 'Richard Thomas').
card_number('gleemax'/'UNH', '121').
card_flavor_text('gleemax'/'UNH', 'Help us . . . R&D under mental domination of alien brain in jar . . . only chance . . . Gleemax\'s blatant disregard for flavor text . . . send help').
card_multiverse_id('gleemax'/'UNH', '73947').

card_in_set('gluetius maximus', 'UNH').
card_original_type('gluetius maximus'/'UNH', 'Creature — Beast').
card_original_text('gluetius maximus'/'UNH', 'As Gluetius Maximus comes into play, an opponent chooses one of your fingers. (Thumbs are fingers, too.)\nWhen the chosen finger isn\'t touching Gluetius Maximus, sacrifice Gluetius Maximus.').
card_first_print('gluetius maximus', 'UNH').
card_image_name('gluetius maximus'/'UNH', 'gluetius maximus').
card_uid('gluetius maximus'/'UNH', 'UNH:Gluetius Maximus:gluetius maximus').
card_rarity('gluetius maximus'/'UNH', 'Uncommon').
card_artist('gluetius maximus'/'UNH', 'Jeff Easley').
card_number('gluetius maximus'/'UNH', '98').
card_multiverse_id('gluetius maximus'/'UNH', '74298').

card_in_set('goblin mime', 'UNH').
card_original_type('goblin mime'/'UNH', 'Creature — Goblin Mime').
card_original_text('goblin mime'/'UNH', 'When you speak, sacrifice Goblin Mime.').
card_image_name('goblin mime'/'UNH', 'goblin mime').
card_uid('goblin mime'/'UNH', 'UNH:Goblin Mime:goblin mime').
card_rarity('goblin mime'/'UNH', 'Common').
card_artist('goblin mime'/'UNH', 'Franz Vohwinkel').
card_number('goblin mime'/'UNH', '78').
card_flavor_text('goblin mime'/'UNH', 'Pretending to be inside an invisible box was easy enough for Brug. It was finding the invisible door that caused him problems.').
card_multiverse_id('goblin mime'/'UNH', '74221').

card_in_set('goblin s.w.a.t. team', 'UNH').
card_original_type('goblin s.w.a.t. team'/'UNH', 'Creature — Goblin Warrior').
card_original_text('goblin s.w.a.t. team'/'UNH', 'Say \"Goblin S.W.A.T. Team\": Put a +1/+1 counter on Goblin S.W.A.T. Team unless an opponent swats the table within five seconds. Play this ability only once each turn.').
card_first_print('goblin s.w.a.t. team', 'UNH').
card_image_name('goblin s.w.a.t. team'/'UNH', 'goblin s.w.a.t. team').
card_uid('goblin s.w.a.t. team'/'UNH', 'UNH:Goblin S.W.A.T. Team:goblin s.w.a.t. team').
card_rarity('goblin s.w.a.t. team'/'UNH', 'Common').
card_artist('goblin s.w.a.t. team'/'UNH', 'Eric Deschamps').
card_number('goblin s.w.a.t. team'/'UNH', '80').
card_flavor_text('goblin s.w.a.t. team'/'UNH', 'They put the \"special\" in special weapons and tactics.').
card_multiverse_id('goblin s.w.a.t. team'/'UNH', '74314').

card_in_set('goblin secret agent', 'UNH').
card_original_type('goblin secret agent'/'UNH', 'Creature — Goblin Rogue').
card_original_text('goblin secret agent'/'UNH', 'First strike\nAt the beginning of your upkeep, reveal a card from your hand at random.').
card_first_print('goblin secret agent', 'UNH').
card_image_name('goblin secret agent'/'UNH', 'goblin secret agent').
card_uid('goblin secret agent'/'UNH', 'UNH:Goblin Secret Agent:goblin secret agent').
card_rarity('goblin secret agent'/'UNH', 'Common').
card_artist('goblin secret agent'/'UNH', 'Tony Szczudlo').
card_number('goblin secret agent'/'UNH', '79').
card_flavor_text('goblin secret agent'/'UNH', 'SECRET AGENT\nPD042198\nLICENSE TO BE KILLED').
card_multiverse_id('goblin secret agent'/'UNH', '74295').

card_in_set('granny\'s payback', 'UNH').
card_original_type('granny\'s payback'/'UNH', 'Sorcery').
card_original_text('granny\'s payback'/'UNH', 'You gain life equal to your age.').
card_image_name('granny\'s payback'/'UNH', 'granny\'s payback').
card_uid('granny\'s payback'/'UNH', 'UNH:Granny\'s Payback:granny\'s payback').
card_rarity('granny\'s payback'/'UNH', 'Uncommon').
card_artist('granny\'s payback'/'UNH', 'Joel Thomas').
card_number('granny\'s payback'/'UNH', '99').
card_flavor_text('granny\'s payback'/'UNH', 'R&D Comments\nBB 10/10: Why does a life-gaining card show an old lady killing people?\nMR (10/10/03): We\'ll fix it in the flavor text.').
card_multiverse_id('granny\'s payback'/'UNH', '74281').

card_in_set('graphic violence', 'UNH').
card_original_type('graphic violence'/'UNH', 'Instant').
card_original_text('graphic violence'/'UNH', 'All creatures by the artist of your choice get +2/+2 and gain trample until end of turn.').
card_first_print('graphic violence', 'UNH').
card_image_name('graphic violence'/'UNH', 'graphic violence').
card_uid('graphic violence'/'UNH', 'UNH:Graphic Violence:graphic violence').
card_rarity('graphic violence'/'UNH', 'Common').
card_artist('graphic violence'/'UNH', 'Wayne “King of” England').
card_number('graphic violence'/'UNH', '100').
card_flavor_text('graphic violence'/'UNH', 'WARNING: THE DOMINARIAN SURGEON GENERAL HAS FOUND THAT SAVAGE BEATINGS CAN LEAD TO CHUMP BLOCKS, LIFE REDUCTION, AND EVEN GAME LOSS.').
card_multiverse_id('graphic violence'/'UNH', '74328').

card_in_set('greater morphling', 'UNH').
card_original_type('greater morphling'/'UNH', 'Creature — Shapeshifter').
card_original_text('greater morphling'/'UNH', '{2}: Greater Morphling gains your choice of banding, bushido 1, double strike, fear, flying, first strike, haste, landwalk of your choice, protection from a color of your choice, provoke, rampage 1, shadow, or trample until end of turn.\n{2}: Greater Morphling becomes the colors of your choice until end of turn.\n{2}: Greater Morphling\'s type becomes the creature type of your choice until end of turn.\n{2}: Greater Morphling\'s expansion symbol becomes the symbol of your choice until end of turn.\n{2}: Greater Morphling\'s artist becomes the artist of your choice until end of turn.\n{2}: Greater Morphling gets +2/-2 or -2/+2 until end of turn.\n{2}: Untap Greater Morphling.').
card_first_print('greater morphling', 'UNH').
card_image_name('greater morphling'/'UNH', 'greater morphling').
card_uid('greater morphling'/'UNH', 'UNH:Greater Morphling:greater morphling').
card_rarity('greater morphling'/'UNH', 'Rare').
card_artist('greater morphling'/'UNH', 'Greg “Six-Pack” Staples').
card_number('greater morphling'/'UNH', '34').
card_multiverse_id('greater morphling'/'UNH', '74296').

card_in_set('head to head', 'UNH').
card_original_type('head to head'/'UNH', 'Instant').
card_original_text('head to head'/'UNH', 'You and target opponent play Seven Questions about the top card of that player\'s library. (That player looks at the card, then you ask up to six yes-or-no questions about the card that he or she answers truthfully. You guess the card\'s name—that\'s question seven—and the player reveals the card.) If you win, prevent all damage that would be dealt this turn by a source of your choice.').
card_first_print('head to head', 'UNH').
card_image_name('head to head'/'UNH', 'head to head').
card_uid('head to head'/'UNH', 'UNH:Head to Head:head to head').
card_rarity('head to head'/'UNH', 'Uncommon').
card_artist('head to head'/'UNH', 'Thomas Gianni').
card_number('head to head'/'UNH', '14').
card_multiverse_id('head to head'/'UNH', '74307').

card_in_set('infernal spawn of infernal spawn of evil', 'UNH').
card_original_type('infernal spawn of infernal spawn of evil'/'UNH', 'Creature — Demon Child').
card_original_text('infernal spawn of infernal spawn of evil'/'UNH', 'Flying, first strike, trample\nIf you say \"I\'m coming, too\" as you search your library, you may pay {1}{B} and reveal Infernal Spawn of Infernal Spawn of Evil from your library to have it deal 2 damage to a player of your choice. Do this no more than once each turn.').
card_first_print('infernal spawn of infernal spawn of evil', 'UNH').
card_image_name('infernal spawn of infernal spawn of evil'/'UNH', 'infernal spawn of infernal spawn of evil').
card_uid('infernal spawn of infernal spawn of evil'/'UNH', 'UNH:Infernal Spawn of Infernal Spawn of Evil:infernal spawn of infernal spawn of evil').
card_rarity('infernal spawn of infernal spawn of evil'/'UNH', 'Rare').
card_artist('infernal spawn of infernal spawn of evil'/'UNH', 'Ron Spencer').
card_number('infernal spawn of infernal spawn of evil'/'UNH', '57').
card_multiverse_id('infernal spawn of infernal spawn of evil'/'UNH', '73981').

card_in_set('island', 'UNH').
card_original_type('island'/'UNH', '(none)').
card_original_text('island'/'UNH', '').
card_border('island'/'UNH', 'black').
card_image_name('island'/'UNH', 'island').
card_uid('island'/'UNH', 'UNH:Island:island').
card_rarity('island'/'UNH', 'Basic Land').
card_artist('island'/'UNH', 'John Avon').
card_number('island'/'UNH', '137').
card_multiverse_id('island'/'UNH', '73951').

card_in_set('johnny, combo player', 'UNH').
card_original_type('johnny, combo player'/'UNH', 'Legendary Creature — Human Gamer').
card_original_text('johnny, combo player'/'UNH', '{4}: Search your library for a card and put that card into your hand. Then shuffle your library.').
card_first_print('johnny, combo player', 'UNH').
card_image_name('johnny, combo player'/'UNH', 'johnny, combo player').
card_uid('johnny, combo player'/'UNH', 'UNH:Johnny, Combo Player:johnny, combo player').
card_rarity('johnny, combo player'/'UNH', 'Rare').
card_artist('johnny, combo player'/'UNH', 'Kensuke Okabayashi').
card_number('johnny, combo player'/'UNH', '35').
card_flavor_text('johnny, combo player'/'UNH', '\"Just wait till I get my Krark-Clan Ironworks, Genesis Chamber, and Grinding Station. Oh yeah, and a second Myr Retriever.\"').
card_multiverse_id('johnny, combo player'/'UNH', '74271').

card_in_set('keeper of the sacred word', 'UNH').
card_original_type('keeper of the sacred word'/'UNH', 'Creature — Human Druid').
card_original_text('keeper of the sacred word'/'UNH', 'As Keeper of the Sacred Word comes into play, choose a word.\nWhenever an opponent says the chosen word, Keeper of the Sacred Word gets +3/+3 until end of turn.').
card_first_print('keeper of the sacred word', 'UNH').
card_image_name('keeper of the sacred word'/'UNH', 'keeper of the sacred word').
card_uid('keeper of the sacred word'/'UNH', 'UNH:Keeper of the Sacred Word:keeper of the sacred word').
card_rarity('keeper of the sacred word'/'UNH', 'Common').
card_artist('keeper of the sacred word'/'UNH', 'Dany Orizio').
card_number('keeper of the sacred word'/'UNH', '101').
card_flavor_text('keeper of the sacred word'/'UNH', 'This is not a subliminal message.').
card_multiverse_id('keeper of the sacred word'/'UNH', '73971').

card_in_set('kill! destroy!', 'UNH').
card_original_type('kill! destroy!'/'UNH', 'Instant').
card_original_text('kill! destroy!'/'UNH', 'Destroy target nonblack creature.\nGotcha Whenever an opponent says \"Kill\" or \"Destroy,\" you may say \"Gotcha\" If you do, return Kill Destroy from your graveyard to your hand.').
card_first_print('kill! destroy!', 'UNH').
card_image_name('kill! destroy!'/'UNH', 'kill! destroy!').
card_uid('kill! destroy!'/'UNH', 'UNH:Kill! Destroy!:kill! destroy!').
card_rarity('kill! destroy!'/'UNH', 'Uncommon').
card_artist('kill! destroy!'/'UNH', 'Corey D. Macourek').
card_number('kill! destroy!'/'UNH', '58').
card_flavor_text('kill! destroy!'/'UNH', 'Getting \"Gotcha\'ed\" again and again.').
card_multiverse_id('kill! destroy!'/'UNH', '74322').

card_in_set('ladies\' knight', 'UNH').
card_original_type('ladies\' knight'/'UNH', 'Creature — Human Knight').
card_original_text('ladies\' knight'/'UNH', 'Flying\nSpells that players wearing at least one item of women\'s clothing play cost {1} less to play. (Women\'s clothing is designed to be worn exclusively by women.)').
card_first_print('ladies\' knight', 'UNH').
card_image_name('ladies\' knight'/'UNH', 'ladies\' knight').
card_uid('ladies\' knight'/'UNH', 'UNH:Ladies\' Knight:ladies\' knight').
card_rarity('ladies\' knight'/'UNH', 'Uncommon').
card_artist('ladies\' knight'/'UNH', 'Ron Spears').
card_number('ladies\' knight'/'UNH', '15').
card_flavor_text('ladies\' knight'/'UNH', '\"I said ‘Put him in a core set, not a corset\'\"\n—Richard Garfield, Ph.D.').
card_multiverse_id('ladies\' knight'/'UNH', '74260').

card_in_set('land aid \'04', 'UNH').
card_original_type('land aid \'04'/'UNH', 'Sorcery').
card_original_text('land aid \'04'/'UNH', 'Search your library for a basic land card, put that card into play tapped, then shuffle your library. If you sang a song the whole time you were searching and shuffling, you may untap that land.').
card_first_print('land aid \'04', 'UNH').
card_image_name('land aid \'04'/'UNH', 'land aid \'04').
card_uid('land aid \'04'/'UNH', 'UNH:Land Aid \'04:land aid \'04').
card_rarity('land aid \'04'/'UNH', 'Common').
card_artist('land aid \'04'/'UNH', 'Jim Nelson').
card_number('land aid \'04'/'UNH', '102').
card_multiverse_id('land aid \'04'/'UNH', '74259').

card_in_set('laughing hyena', 'UNH').
card_original_type('laughing hyena'/'UNH', 'Creature — Hyena').
card_original_text('laughing hyena'/'UNH', 'Gotcha Whenever an opponent laughs, you may say \"Gotcha\" If you do, return Laughing Hyena from your graveyard to your hand.').
card_first_print('laughing hyena', 'UNH').
card_image_name('laughing hyena'/'UNH', 'laughing hyena').
card_uid('laughing hyena'/'UNH', 'UNH:Laughing Hyena:laughing hyena').
card_rarity('laughing hyena'/'UNH', 'Common').
card_artist('laughing hyena'/'UNH', 'Mark Poole').
card_number('laughing hyena'/'UNH', '103').
card_flavor_text('laughing hyena'/'UNH', '\"Two muffins are baking in an oven. One muffin says to the other muffin, ‘It\'s getting hot in here, huh?\' The other muffin says, ‘Aagh A talking muffin\'\"').
card_multiverse_id('laughing hyena'/'UNH', '74293').

card_in_set('letter bomb', 'UNH').
card_original_type('letter bomb'/'UNH', 'Artifact').
card_original_text('letter bomb'/'UNH', 'When Letter Bomb comes into play, sign it and shuffle it into target player\'s library. That player reveals each card he or she draws until Letter Bomb is drawn. When that player draws Letter Bomb, it deals 19½ damage to him or her.').
card_first_print('letter bomb', 'UNH').
card_image_name('letter bomb'/'UNH', 'letter bomb').
card_uid('letter bomb'/'UNH', 'UNH:Letter Bomb:letter bomb').
card_rarity('letter bomb'/'UNH', 'Rare').
card_artist('letter bomb'/'UNH', 'Daniel Gelon').
card_number('letter bomb'/'UNH', '122').
card_flavor_text('letter bomb'/'UNH', '\"Dear Magic R&D, . . .\"').
card_multiverse_id('letter bomb'/'UNH', '74232').

card_in_set('little girl', 'UNH').
card_original_type('little girl'/'UNH', 'Creature — Human Child').
card_original_text('little girl'/'UNH', '').
card_first_print('little girl', 'UNH').
card_image_name('little girl'/'UNH', 'little girl').
card_uid('little girl'/'UNH', 'UNH:Little Girl:little girl').
card_rarity('little girl'/'UNH', 'Common').
card_artist('little girl'/'UNH', 'Rebecca Guay').
card_number('little girl'/'UNH', '16').
card_flavor_text('little girl'/'UNH', 'In the future, she may be a distinguished leader, a great scholar, or a decorated hero. These days all she does is pee the bed.').
card_multiverse_id('little girl'/'UNH', '74257').

card_in_set('look at me, i\'m r&d', 'UNH').
card_original_type('look at me, i\'m r&d'/'UNH', 'Enchantment').
card_original_text('look at me, i\'m r&d'/'UNH', 'As Look at Me, I\'m R&D comes into play, choose a number and a second number one higher or one lower than that number.\nAll instances of the first chosen number on permanents, spells, and cards in any zone are the second chosen number.').
card_first_print('look at me, i\'m r&d', 'UNH').
card_image_name('look at me, i\'m r&d'/'UNH', 'look at me, i\'m r&d').
card_uid('look at me, i\'m r&d'/'UNH', 'UNH:Look at Me, I\'m R&D:look at me, i\'m r&d').
card_rarity('look at me, i\'m r&d'/'UNH', 'Rare').
card_artist('look at me, i\'m r&d'/'UNH', 'spork;').
card_number('look at me, i\'m r&d'/'UNH', '17').
card_multiverse_id('look at me, i\'m r&d'/'UNH', '74360').

card_in_set('loose lips', 'UNH').
card_original_type('loose lips'/'UNH', 'Enchant Creature').
card_original_text('loose lips'/'UNH', 'As Loose Lips comes into play, choose a sentence with eight or fewer words.\nEnchanted creature has flying.\nWhenever enchanted creature deals damage to an opponent, you draw two cards unless that player says the chosen sentence.').
card_first_print('loose lips', 'UNH').
card_image_name('loose lips'/'UNH', 'loose lips').
card_uid('loose lips'/'UNH', 'UNH:Loose Lips:loose lips').
card_rarity('loose lips'/'UNH', 'Common').
card_artist('loose lips'/'UNH', 'John Matson').
card_number('loose lips'/'UNH', '36').
card_flavor_text('loose lips'/'UNH', 'Your sentence can\'t be longer than this one.').
card_multiverse_id('loose lips'/'UNH', '73964').

card_in_set('magical hacker', 'UNH').
card_original_type('magical hacker'/'UNH', 'Creature — Human Gamer').
card_original_text('magical hacker'/'UNH', '{U}: Change the text of target spell or permanent by replacing all instances of + with -, and vice versa, until end of turn.').
card_first_print('magical hacker', 'UNH').
card_image_name('magical hacker'/'UNH', 'magical hacker').
card_uid('magical hacker'/'UNH', 'UNH:Magical Hacker:magical hacker').
card_rarity('magical hacker'/'UNH', 'Uncommon').
card_artist('magical hacker'/'UNH', 'Doug Chaffee').
card_number('magical hacker'/'UNH', '37').
card_flavor_text('magical hacker'/'UNH', '1|= y()u (4|\\| r3@d 7#5, y0|_| /\\r3 @ IVI0/\\/$+3|2 &33|<').
card_multiverse_id('magical hacker'/'UNH', '74256').

card_in_set('man of measure', 'UNH').
card_original_type('man of measure'/'UNH', 'Creature — Human Knight').
card_original_text('man of measure'/'UNH', 'As long as you\'re shorter than an opponent, Man of Measure has first strike and gets +0/+1.\nAs long as you\'re taller than an opponent, Man of Measure gets +1/+0.').
card_first_print('man of measure', 'UNH').
card_image_name('man of measure'/'UNH', 'man of measure').
card_uid('man of measure'/'UNH', 'UNH:Man of Measure:man of measure').
card_rarity('man of measure'/'UNH', 'Common').
card_artist('man of measure'/'UNH', 'Edward P. Beard, Jr.').
card_number('man of measure'/'UNH', '18').
card_flavor_text('man of measure'/'UNH', 'You need to be this tall to play this card.').
card_multiverse_id('man of measure'/'UNH', '74268').

card_in_set('mana flair', 'UNH').
card_original_type('mana flair'/'UNH', 'Instant').
card_original_text('mana flair'/'UNH', 'Add {R} to your mana pool for each nonland permanent by the artist of your choice.').
card_first_print('mana flair', 'UNH').
card_image_name('mana flair'/'UNH', 'mana flair').
card_uid('mana flair'/'UNH', 'UNH:Mana Flair:mana flair').
card_rarity('mana flair'/'UNH', 'Common').
card_artist('mana flair'/'UNH', 'Paolo “That\'s Actually Me” Parente').
card_number('mana flair'/'UNH', '81').
card_flavor_text('mana flair'/'UNH', 'What began as black\nHas slowly shifted to red.\nFickle color pie.').
card_multiverse_id('mana flair'/'UNH', '74266').

card_in_set('mana screw', 'UNH').
card_original_type('mana screw'/'UNH', 'Artifact').
card_original_text('mana screw'/'UNH', '{1}: Flip a coin. If you win the flip, add {2} to your mana pool. Play this ability only any time you could play an instant.').
card_first_print('mana screw', 'UNH').
card_image_name('mana screw'/'UNH', 'mana screw').
card_uid('mana screw'/'UNH', 'UNH:Mana Screw:mana screw').
card_rarity('mana screw'/'UNH', 'Uncommon').
card_artist('mana screw'/'UNH', 'Mike Raabe').
card_number('mana screw'/'UNH', '123').
card_flavor_text('mana screw'/'UNH', 'There was no darker or more evil creation in all the multiverse than that of the mana screw.').
card_multiverse_id('mana screw'/'UNH', '73955').

card_in_set('meddling kids', 'UNH').
card_original_type('meddling kids'/'UNH', 'Creature — Human Child').
card_original_text('meddling kids'/'UNH', 'As Meddling Kids comes into play, choose a word with four or more letters.\nNonland cards with the chosen word in their text box can\'t be played.').
card_first_print('meddling kids', 'UNH').
card_image_name('meddling kids'/'UNH', 'meddling kids').
card_uid('meddling kids'/'UNH', 'UNH:Meddling Kids:meddling kids').
card_rarity('meddling kids'/'UNH', 'Rare').
card_artist('meddling kids'/'UNH', 'Christopher Moeller').
card_number('meddling kids'/'UNH', '118').
card_flavor_text('meddling kids'/'UNH', 'These kids know their four-letter words.').
card_multiverse_id('meddling kids'/'UNH', '74347').

card_in_set('mise', 'UNH').
card_original_type('mise'/'UNH', 'Instant').
card_original_text('mise'/'UNH', 'Name a nonland card, then reveal the top card of your library. If that card is the named card, draw three cards.').
card_image_name('mise'/'UNH', 'mise').
card_uid('mise'/'UNH', 'UNH:Mise:mise').
card_rarity('mise'/'UNH', 'Uncommon').
card_artist('mise'/'UNH', 'Matt Cavotta').
card_number('mise'/'UNH', '38').
card_flavor_text('mise'/'UNH', 'mise \'miz v alter. of might as well (1997) 1: to win when you don\'t deserve to 2: to top-deck the \"tings\" you need 3: to be rewarded by an opponent\'s bad luck 4: to coin a phrase that spreads through the tournament scene like wildfire 5: to fling a monkey 6: to split firewood using a sharp instrument 7: To burn').
card_multiverse_id('mise'/'UNH', '74304').

card_in_set('moniker mage', 'UNH').
card_original_type('moniker mage'/'UNH', 'Creature — Human Wizard').
card_original_text('moniker mage'/'UNH', '{U}, Say your middle name: Moniker Mage can\'t be the target of spells or abilities this turn.\n{U}, Say an opponent\'s middle name: Moniker Mage gains flying until end of turn.').
card_first_print('moniker mage', 'UNH').
card_image_name('moniker mage'/'UNH', 'moniker mage').
card_uid('moniker mage'/'UNH', 'UNH:Moniker Mage:moniker mage').
card_rarity('moniker mage'/'UNH', 'Common').
card_artist('moniker mage'/'UNH', 'Keith Garletts').
card_number('moniker mage'/'UNH', '39').
card_multiverse_id('moniker mage'/'UNH', '74255').

card_in_set('monkey monkey monkey', 'UNH').
card_original_type('monkey monkey monkey'/'UNH', 'Creature — Ape').
card_original_text('monkey monkey monkey'/'UNH', 'As Monkey Monkey Monkey comes into play, choose a letter.\nMonkey Monkey Monkey gets +1/+1 for each nonland permanent whose name begins with the chosen letter.').
card_first_print('monkey monkey monkey', 'UNH').
card_image_name('monkey monkey monkey'/'UNH', 'monkey monkey monkey').
card_uid('monkey monkey monkey'/'UNH', 'UNH:Monkey Monkey Monkey:monkey monkey monkey').
card_rarity('monkey monkey monkey'/'UNH', 'Common').
card_artist('monkey monkey monkey'/'UNH', 'Liz Danforth').
card_number('monkey monkey monkey'/'UNH', '104').
card_flavor_text('monkey monkey monkey'/'UNH', 'Many matches make more madcap monkey mayhem.').
card_multiverse_id('monkey monkey monkey'/'UNH', '74233').

card_in_set('mons\'s goblin waiters', 'UNH').
card_original_type('mons\'s goblin waiters'/'UNH', 'Creature — Goblin Waiter').
card_original_text('mons\'s goblin waiters'/'UNH', 'Sacrifice a creature or land: Add {hr} to your mana pool.').
card_first_print('mons\'s goblin waiters', 'UNH').
card_image_name('mons\'s goblin waiters'/'UNH', 'mons\'s goblin waiters').
card_uid('mons\'s goblin waiters'/'UNH', 'UNH:Mons\'s Goblin Waiters:mons\'s goblin waiters').
card_rarity('mons\'s goblin waiters'/'UNH', 'Common').
card_artist('mons\'s goblin waiters'/'UNH', 'Pete Venters').
card_number('mons\'s goblin waiters'/'UNH', '82').
card_flavor_text('mons\'s goblin waiters'/'UNH', 'Poached Rukh Eggs\nRock\nCooked Rock\nUnicorn-on-the-Cob\nWormfang Turtle Soup').
card_multiverse_id('mons\'s goblin waiters'/'UNH', '73957').

card_in_set('mother of goons', 'UNH').
card_original_type('mother of goons'/'UNH', 'Creature — Human Cleric').
card_original_text('mother of goons'/'UNH', 'Whenever a creature an opponent controls is put into a graveyard from play, sacrifice Mother of Goons unless you insult that creature.').
card_first_print('mother of goons', 'UNH').
card_image_name('mother of goons'/'UNH', 'mother of goons').
card_uid('mother of goons'/'UNH', 'UNH:Mother of Goons:mother of goons').
card_rarity('mother of goons'/'UNH', 'Common').
card_artist('mother of goons'/'UNH', 'Darrell Riche').
card_number('mother of goons'/'UNH', '59').
card_flavor_text('mother of goons'/'UNH', '\"Word to me.\"').
card_multiverse_id('mother of goons'/'UNH', '74292').

card_in_set('mountain', 'UNH').
card_original_type('mountain'/'UNH', '(none)').
card_original_text('mountain'/'UNH', '').
card_border('mountain'/'UNH', 'black').
card_image_name('mountain'/'UNH', 'mountain').
card_uid('mountain'/'UNH', 'UNH:Mountain:mountain').
card_rarity('mountain'/'UNH', 'Basic Land').
card_artist('mountain'/'UNH', 'John Avon').
card_number('mountain'/'UNH', '139').
card_multiverse_id('mountain'/'UNH', '73958').

card_in_set('mouth to mouth', 'UNH').
card_original_type('mouth to mouth'/'UNH', 'Sorcery').
card_original_text('mouth to mouth'/'UNH', 'You and target opponent have a breath-holding contest. If you win, you gain control of target creature that player controls.').
card_first_print('mouth to mouth', 'UNH').
card_image_name('mouth to mouth'/'UNH', 'mouth to mouth').
card_uid('mouth to mouth'/'UNH', 'UNH:Mouth to Mouth:mouth to mouth').
card_rarity('mouth to mouth'/'UNH', 'Uncommon').
card_artist('mouth to mouth'/'UNH', 'Alan Pollack').
card_number('mouth to mouth'/'UNH', '40').
card_flavor_text('mouth to mouth'/'UNH', 'Interesting tidbit: Surveys show that the card Ow has one of the most popular pieces of flavor text in the Unglued expansion.').
card_multiverse_id('mouth to mouth'/'UNH', '73949').

card_in_set('mox lotus', 'UNH').
card_original_type('mox lotus'/'UNH', 'Artifact').
card_original_text('mox lotus'/'UNH', '{T}: Add {∞} to your mana pool.\n{100}: Add one mana of any color to your mana pool.\nYou don\'t lose life due to mana burn.').
card_first_print('mox lotus', 'UNH').
card_image_name('mox lotus'/'UNH', 'mox lotus').
card_uid('mox lotus'/'UNH', 'UNH:Mox Lotus:mox lotus').
card_rarity('mox lotus'/'UNH', 'Rare').
card_artist('mox lotus'/'UNH', 'Kevin Dobler').
card_number('mox lotus'/'UNH', '124').
card_multiverse_id('mox lotus'/'UNH', '74323').

card_in_set('my first tome', 'UNH').
card_original_type('my first tome'/'UNH', 'Artifact').
card_original_text('my first tome'/'UNH', '{1}, {T}: Say the flavor text on a card in your hand. Target opponent guesses that card\'s name. You may reveal that card. If you do and your opponent guessed wrong, draw a card.').
card_first_print('my first tome', 'UNH').
card_image_name('my first tome'/'UNH', 'my first tome').
card_uid('my first tome'/'UNH', 'UNH:My First Tome:my first tome').
card_rarity('my first tome'/'UNH', 'Uncommon').
card_artist('my first tome'/'UNH', 'Heather Hudson').
card_number('my first tome'/'UNH', '125').
card_flavor_text('my first tome'/'UNH', 'This card is named My First Tome.').
card_multiverse_id('my first tome'/'UNH', '73945').

card_in_set('name dropping', 'UNH').
card_original_type('name dropping'/'UNH', 'Enchantment').
card_original_text('name dropping'/'UNH', 'Gotcha Whenever an opponent says a word that\'s in the name of a card in your graveyard, you may say \"Gotcha\" If you do, return that card to your hand.').
card_first_print('name dropping', 'UNH').
card_image_name('name dropping'/'UNH', 'name dropping').
card_uid('name dropping'/'UNH', 'UNH:Name Dropping:name dropping').
card_rarity('name dropping'/'UNH', 'Uncommon').
card_artist('name dropping'/'UNH', 'Tony Szczudlo').
card_number('name dropping'/'UNH', '105').
card_flavor_text('name dropping'/'UNH', 'Gemini (May 21–June 21) This week is a good time to branch out and meet others, but don\'t forget your roots. Avoid commitments and open flame.').
card_multiverse_id('name dropping'/'UNH', '74286').

card_in_set('necro-impotence', 'UNH').
card_original_type('necro-impotence'/'UNH', 'Enchantment').
card_original_text('necro-impotence'/'UNH', 'Skip your untap step.\nAt the beginning of your upkeep, you may pay X life. If you do, untap X permanents.\nPay ½ life: Remove the top card of your library from the game face down. Put that card into your hand at end of turn.').
card_first_print('necro-impotence', 'UNH').
card_image_name('necro-impotence'/'UNH', 'necro-impotence').
card_uid('necro-impotence'/'UNH', 'UNH:Necro-Impotence:necro-impotence').
card_rarity('necro-impotence'/'UNH', 'Rare').
card_artist('necro-impotence'/'UNH', 'Mark Tedin').
card_number('necro-impotence'/'UNH', '60').
card_multiverse_id('necro-impotence'/'UNH', '74326').

card_in_set('now i know my abc\'s', 'UNH').
card_original_type('now i know my abc\'s'/'UNH', 'Enchantment').
card_original_text('now i know my abc\'s'/'UNH', 'At the beginning of your upkeep, if you control permanents with names that include all twenty-six letters of the English alphabet, you win the game.').
card_first_print('now i know my abc\'s', 'UNH').
card_image_name('now i know my abc\'s'/'UNH', 'now i know my abc\'s').
card_uid('now i know my abc\'s'/'UNH', 'UNH:Now I Know My ABC\'s:now i know my abc\'s').
card_rarity('now i know my abc\'s'/'UNH', 'Rare').
card_artist('now i know my abc\'s'/'UNH', 'Tony Szczudlo').
card_number('now i know my abc\'s'/'UNH', '41').
card_flavor_text('now i know my abc\'s'/'UNH', 'The quick onyx goblin jumps over the lazy dwarf.').
card_multiverse_id('now i know my abc\'s'/'UNH', '73959').

card_in_set('number crunch', 'UNH').
card_original_type('number crunch'/'UNH', 'Instant').
card_original_text('number crunch'/'UNH', 'Return target permanent to its owner\'s hand.\nGotcha Whenever an opponent says a number, you may say \"Gotcha\" If you do, return Number Crunch from your graveyard to your hand.').
card_first_print('number crunch', 'UNH').
card_image_name('number crunch'/'UNH', 'number crunch').
card_uid('number crunch'/'UNH', 'UNH:Number Crunch:number crunch').
card_rarity('number crunch'/'UNH', 'Common').
card_artist('number crunch'/'UNH', 'Stephen Daniele').
card_number('number crunch'/'UNH', '42').
card_flavor_text('number crunch'/'UNH', 'Can you digit?').
card_multiverse_id('number crunch'/'UNH', '74337').

card_in_set('old fogey', 'UNH').
card_original_type('old fogey'/'UNH', 'Summon — Dinosaur').
card_original_text('old fogey'/'UNH', 'Phasing, cumulative upkeep {1}, echo, fading 3, bands with other Dinosaurs, protection from Homarids, snow-covered plainswalk, flanking, rampage 2').
card_first_print('old fogey', 'UNH').
card_image_name('old fogey'/'UNH', 'old fogey').
card_uid('old fogey'/'UNH', 'UNH:Old Fogey:old fogey').
card_rarity('old fogey'/'UNH', 'Rare').
card_artist('old fogey'/'UNH', 'Douglas Shuler').
card_number('old fogey'/'UNH', '106').
card_flavor_text('old fogey'/'UNH', '\"These kids today with their collector numbers and their newfangled tap symbol. Twenty Black Lotuses and twenty Plague Rats. Now that\'s real Magic.\"').
card_multiverse_id('old fogey'/'UNH', '74235').

card_in_set('orcish paratroopers', 'UNH').
card_original_type('orcish paratroopers'/'UNH', 'Creature — Orc Paratrooper').
card_original_text('orcish paratroopers'/'UNH', 'When Orcish Paratroopers comes into play, flip it from a height of at least one foot. Sacrifice Orcish Paratroopers unless it lands face up after turning over completely.').
card_first_print('orcish paratroopers', 'UNH').
card_image_name('orcish paratroopers'/'UNH', 'orcish paratroopers').
card_uid('orcish paratroopers'/'UNH', 'UNH:Orcish Paratroopers:orcish paratroopers').
card_rarity('orcish paratroopers'/'UNH', 'Common').
card_artist('orcish paratroopers'/'UNH', 'Matt Thompson').
card_number('orcish paratroopers'/'UNH', '83').
card_flavor_text('orcish paratroopers'/'UNH', 'IOU one parashoot\n—Urk, mogg fanatic').
card_multiverse_id('orcish paratroopers'/'UNH', '73960').

card_in_set('our market research shows that players like really long card names so we made this card to have the absolute longest card name ever elemental', 'UNH').
card_original_type('our market research shows that players like really long card names so we made this card to have the absolute longest card name ever elemental'/'UNH', 'Creature — Elemental').
card_original_text('our market research shows that players like really long card names so we made this card to have the absolute longest card name ever elemental'/'UNH', 'Art rampage 2 (Whenever this becomes blocked by a creature, it gets +2/+2 for each creature in the blocker\'s art beyond the first.)').
card_first_print('our market research shows that players like really long card names so we made this card to have the absolute longest card name ever elemental', 'UNH').
card_image_name('our market research shows that players like really long card names so we made this card to have the absolute longest card name ever elemental'/'UNH', 'our market research shows that players like really long card names so we made').
card_uid('our market research shows that players like really long card names so we made this card to have the absolute longest card name ever elemental'/'UNH', 'UNH:Our Market Research Shows That Players Like Really Long Card Names So We Made this Card to Have the Absolute Longest Card Name Ever Elemental:our market research shows that players like really long card names so we made').
card_rarity('our market research shows that players like really long card names so we made this card to have the absolute longest card name ever elemental'/'UNH', 'Common').
card_artist('our market research shows that players like really long card names so we made this card to have the absolute longest card name ever elemental'/'UNH', 'Greg Hildebrandt').
card_number('our market research shows that players like really long card names so we made this card to have the absolute longest card name ever elemental'/'UNH', '107').
card_flavor_text('our market research shows that players like really long card names so we made this card to have the absolute longest card name ever elemental'/'UNH', 'Just call it OMRSTPLRLCNSWMTCTHTALCNEE for short.').
card_multiverse_id('our market research shows that players like really long card names so we made this card to have the absolute longest card name ever elemental'/'UNH', '74237').

card_in_set('persecute artist', 'UNH').
card_original_type('persecute artist'/'UNH', 'Sorcery').
card_original_text('persecute artist'/'UNH', 'Choose an artist other than Rebecca Guay. Target player reveals his or her hand and discards all nonland cards by the chosen artist.').
card_first_print('persecute artist', 'UNH').
card_image_name('persecute artist'/'UNH', 'persecute artist').
card_uid('persecute artist'/'UNH', 'UNH:Persecute Artist:persecute artist').
card_rarity('persecute artist'/'UNH', 'Uncommon').
card_artist('persecute artist'/'UNH', 'Rebecca “Don\'t Mess with Me” Guay').
card_number('persecute artist'/'UNH', '61').
card_flavor_text('persecute artist'/'UNH', 'The torches and pitchforks were no match for Rebecca\'s fans.').
card_multiverse_id('persecute artist'/'UNH', '74238').

card_in_set('phyrexian librarian', 'UNH').
card_original_type('phyrexian librarian'/'UNH', 'Creature — Horror').
card_original_text('phyrexian librarian'/'UNH', 'Flying, trample\nAt the beginning of your upkeep, remove the top card of your library from the game face up and balance it on your body.\nWhen a balanced card falls or touches another balanced card, sacrifice Phyrexian Librarian.').
card_first_print('phyrexian librarian', 'UNH').
card_image_name('phyrexian librarian'/'UNH', 'phyrexian librarian').
card_uid('phyrexian librarian'/'UNH', 'UNH:Phyrexian Librarian:phyrexian librarian').
card_rarity('phyrexian librarian'/'UNH', 'Uncommon').
card_artist('phyrexian librarian'/'UNH', 'Kev Walker').
card_number('phyrexian librarian'/'UNH', '62').
card_multiverse_id('phyrexian librarian'/'UNH', '73962').

card_in_set('plains', 'UNH').
card_original_type('plains'/'UNH', '(none)').
card_original_text('plains'/'UNH', '').
card_border('plains'/'UNH', 'black').
card_image_name('plains'/'UNH', 'plains').
card_uid('plains'/'UNH', 'UNH:Plains:plains').
card_rarity('plains'/'UNH', 'Basic Land').
card_artist('plains'/'UNH', 'John Avon').
card_number('plains'/'UNH', '136').
card_multiverse_id('plains'/'UNH', '73963').

card_in_set('pointy finger of doom', 'UNH').
card_original_type('pointy finger of doom'/'UNH', 'Artifact').
card_original_text('pointy finger of doom'/'UNH', '{3}, {T}: Spin Pointy Finger of Doom in the middle of the table so that it rotates completely at least once, then destroy the closest permanent the finger points to.').
card_first_print('pointy finger of doom', 'UNH').
card_image_name('pointy finger of doom'/'UNH', 'pointy finger of doom').
card_uid('pointy finger of doom'/'UNH', 'UNH:Pointy Finger of Doom:pointy finger of doom').
card_rarity('pointy finger of doom'/'UNH', 'Rare').
card_artist('pointy finger of doom'/'UNH', 'Martina Pilcerova').
card_number('pointy finger of doom'/'UNH', '126').
card_flavor_text('pointy finger of doom'/'UNH', '\"I would have made a ‘Spin-The-Bottle\' joke if I thought anyone reading this had ever played the game.\"\n—Bucky, flavor text writer').
card_multiverse_id('pointy finger of doom'/'UNH', '74263').

card_in_set('punctuate', 'UNH').
card_original_type('punctuate'/'UNH', 'Instant').
card_original_text('punctuate'/'UNH', 'Punctuate deals damage to target creature equal to half the number of punctuation marks in that creature\'s text box. (The punctuation marks are  ? , ; : - ( ) / \" \' & .)').
card_first_print('punctuate', 'UNH').
card_image_name('punctuate'/'UNH', 'punctuate').
card_uid('punctuate'/'UNH', 'UNH:Punctuate:punctuate').
card_rarity('punctuate'/'UNH', 'Common').
card_artist('punctuate'/'UNH', 'Jim Pavelec').
card_number('punctuate'/'UNH', '84').
card_flavor_text('punctuate'/'UNH', '\"Ooh—right in the colon.\"').
card_multiverse_id('punctuate'/'UNH', '74248').

card_in_set('pygmy giant', 'UNH').
card_original_type('pygmy giant'/'UNH', 'Creature — Giant').
card_original_text('pygmy giant'/'UNH', '{R}, {T}, Sacrifice a creature: Pygmy Giant deals X damage to target creature, where X is a number in the sacrificed creature\'s text box.').
card_first_print('pygmy giant', 'UNH').
card_image_name('pygmy giant'/'UNH', 'pygmy giant').
card_uid('pygmy giant'/'UNH', 'UNH:Pygmy Giant:pygmy giant').
card_rarity('pygmy giant'/'UNH', 'Uncommon').
card_artist('pygmy giant'/'UNH', 'Eric Deschamps').
card_number('pygmy giant'/'UNH', '85').
card_flavor_text('pygmy giant'/'UNH', '\"487. You\'re welcome.\"\n—Bucky, flavor text writer').
card_multiverse_id('pygmy giant'/'UNH', '74333').

card_in_set('question elemental?', 'UNH').
card_original_type('question elemental?'/'UNH', 'Creature — Elemental').
card_original_text('question elemental?'/'UNH', 'Flying\nAre you aware that when you say something that isn\'t a question, the player who first points out this fact gains control of Question Elemental?').
card_first_print('question elemental?', 'UNH').
card_image_name('question elemental?'/'UNH', 'question elemental').
card_uid('question elemental?'/'UNH', 'UNH:Question Elemental?:question elemental').
card_rarity('question elemental?'/'UNH', 'Uncommon').
card_artist('question elemental?'/'UNH', 'Edward P. Beard, Jr.').
card_number('question elemental?'/'UNH', '43').
card_flavor_text('question elemental?'/'UNH', '\"What the . . . ?\"').
card_multiverse_id('question elemental?'/'UNH', '74294').

card_in_set('r&d\'s secret lair', 'UNH').
card_original_type('r&d\'s secret lair'/'UNH', 'Legendary Land').
card_original_text('r&d\'s secret lair'/'UNH', 'Play cards as written. Ignore all errata.\n{T}: Add {1} to your mana pool.').
card_first_print('r&d\'s secret lair', 'UNH').
card_image_name('r&d\'s secret lair'/'UNH', 'r&d\'s secret lair').
card_uid('r&d\'s secret lair'/'UNH', 'UNH:R&D\'s Secret Lair:r&d\'s secret lair').
card_rarity('r&d\'s secret lair'/'UNH', 'Rare').
card_artist('r&d\'s secret lair'/'UNH', 'John Avon').
card_number('r&d\'s secret lair'/'UNH', '135').
card_flavor_text('r&d\'s secret lair'/'UNH', '\"Let them complain. As long as the addictive ink is working, we can do anything we want.\"\nPLAY AT YOUR OWN RISK').
card_multiverse_id('r&d\'s secret lair'/'UNH', '73967').

card_in_set('rare-b-gone', 'UNH').
card_original_type('rare-b-gone'/'UNH', 'Sorcery').
card_original_text('rare-b-gone'/'UNH', 'Each player sacrifices all rare permanents, then reveals his or her hand and discards all rare cards.').
card_first_print('rare-b-gone', 'UNH').
card_image_name('rare-b-gone'/'UNH', 'rare-b-gone').
card_uid('rare-b-gone'/'UNH', 'UNH:Rare-B-Gone:rare-b-gone').
card_rarity('rare-b-gone'/'UNH', 'Rare').
card_artist('rare-b-gone'/'UNH', 'John Matson').
card_number('rare-b-gone'/'UNH', '119').
card_flavor_text('rare-b-gone'/'UNH', 'The only rare you\'ll ever need. Okay, not really.').
card_multiverse_id('rare-b-gone'/'UNH', '73968').

card_in_set('red-hot hottie', 'UNH').
card_original_type('red-hot hottie'/'UNH', 'Creature — Elemental').
card_original_text('red-hot hottie'/'UNH', 'Whenever Red-Hot Hottie deals damage to a creature, put a third-degree-burn counter on that creature. It has \"At the end of each turn, sacrifice this creature unless you scream ‘Aaah\' at the top of your lungs.\"').
card_first_print('red-hot hottie', 'UNH').
card_image_name('red-hot hottie'/'UNH', 'red-hot hottie').
card_uid('red-hot hottie'/'UNH', 'UNH:Red-Hot Hottie:red-hot hottie').
card_rarity('red-hot hottie'/'UNH', 'Common').
card_artist('red-hot hottie'/'UNH', 'Jeremy Jarvis').
card_number('red-hot hottie'/'UNH', '86').
card_flavor_text('red-hot hottie'/'UNH', 'She was winning the Dominarian \"Are You Hot?\" competition until disaster struck during the wet T-shirt round.').
card_multiverse_id('red-hot hottie'/'UNH', '74267').

card_in_set('remodel', 'UNH').
card_original_type('remodel'/'UNH', 'Instant').
card_original_text('remodel'/'UNH', 'If you control two or more green permanents that share an artist, you may play Remodel without paying its mana cost.\nRemove target artifact from the game.').
card_first_print('remodel', 'UNH').
card_image_name('remodel'/'UNH', 'remodel').
card_uid('remodel'/'UNH', 'UNH:Remodel:remodel').
card_rarity('remodel'/'UNH', 'Common').
card_artist('remodel'/'UNH', 'Lars Grant-“Wild Wild”-West').
card_number('remodel'/'UNH', '108').
card_flavor_text('remodel'/'UNH', '6 PM (23) This Old Grove 98055\nA rusted masticore makes a beautiful addition to any garden.').
card_multiverse_id('remodel'/'UNH', '74310').

card_in_set('richard garfield, ph.d.', 'UNH').
card_original_type('richard garfield, ph.d.'/'UNH', 'Legendary Creature — Human Designer').
card_original_text('richard garfield, ph.d.'/'UNH', 'You may play cards as though they were other Magic cards of your choice with the same mana cost. (Mana cost includes color.) You can\'t choose the same card twice.').
card_first_print('richard garfield, ph.d.', 'UNH').
card_image_name('richard garfield, ph.d.'/'UNH', 'richard garfield, ph.d.').
card_uid('richard garfield, ph.d.'/'UNH', 'UNH:Richard Garfield, Ph.D.:richard garfield, ph.d.').
card_rarity('richard garfield, ph.d.'/'UNH', 'Rare').
card_artist('richard garfield, ph.d.'/'UNH', 'Dave Dorman').
card_number('richard garfield, ph.d.'/'UNH', '44').
card_flavor_text('richard garfield, ph.d.'/'UNH', 'And yea he doth spake: \"Let there be Magic.\"').
card_multiverse_id('richard garfield, ph.d.'/'UNH', '74250').

card_in_set('rocket-powered turbo slug', 'UNH').
card_original_type('rocket-powered turbo slug'/'UNH', 'Creature — Slug').
card_original_text('rocket-powered turbo slug'/'UNH', 'Super haste (This may attack the turn before you play it. (You may put this card into play from your hand, tapped and attacking, during your declare attackers step. If you do, you lose the game at the end of your next turn unless you pay this card\'s mana cost during that turn.))').
card_first_print('rocket-powered turbo slug', 'UNH').
card_image_name('rocket-powered turbo slug'/'UNH', 'rocket-powered turbo slug').
card_uid('rocket-powered turbo slug'/'UNH', 'UNH:Rocket-Powered Turbo Slug:rocket-powered turbo slug').
card_rarity('rocket-powered turbo slug'/'UNH', 'Uncommon').
card_artist('rocket-powered turbo slug'/'UNH', 'Franz Vohwinkel').
card_number('rocket-powered turbo slug'/'UNH', '87').
card_multiverse_id('rocket-powered turbo slug'/'UNH', '74324').

card_in_set('rod of spanking', 'UNH').
card_original_type('rod of spanking'/'UNH', 'Artifact').
card_original_text('rod of spanking'/'UNH', '{2}, {T}: Rod of Spanking deals 1 damage to target player. Then untap Rod of Spanking unless that player says \"Thank you, sir. May I have another?\"').
card_first_print('rod of spanking', 'UNH').
card_image_name('rod of spanking'/'UNH', 'rod of spanking').
card_uid('rod of spanking'/'UNH', 'UNH:Rod of Spanking:rod of spanking').
card_rarity('rod of spanking'/'UNH', 'Uncommon').
card_artist('rod of spanking'/'UNH', 'Mark Brill').
card_number('rod of spanking'/'UNH', '127').
card_flavor_text('rod of spanking'/'UNH', 'Minutes before the campus watch busted pledge week at the Alpha Beta Unlimited house . . .').
card_multiverse_id('rod of spanking'/'UNH', '74344').

card_in_set('s.n.o.t.', 'UNH').
card_original_type('s.n.o.t.'/'UNH', 'Creature — Ooze').
card_original_text('s.n.o.t.'/'UNH', 'As S.N.O.T. comes into play, you may stick it onto another creature named S.N.O.T. in play. If you do, all those creatures form a single creature.\nS.N.O.T.\'s power and toughness are equal to the square of the number of S.N.O.T.s stuck together. (One is a 1/1, two are a 4/4, three are a 9/9, and four are a 16/16.)').
card_first_print('s.n.o.t.', 'UNH').
card_image_name('s.n.o.t.'/'UNH', 's.n.o.t.').
card_uid('s.n.o.t.'/'UNH', 'UNH:S.N.O.T.:s.n.o.t.').
card_rarity('s.n.o.t.'/'UNH', 'Common').
card_artist('s.n.o.t.'/'UNH', 'Cyril Van Der Haegen').
card_number('s.n.o.t.'/'UNH', '111').
card_multiverse_id('s.n.o.t.'/'UNH', '73942').

card_in_set('sauté', 'UNH').
card_original_type('sauté'/'UNH', 'Instant').
card_original_text('sauté'/'UNH', 'Sauté deals 3½ damage to target creature or player.').
card_first_print('sauté', 'UNH').
card_image_name('sauté'/'UNH', 'saute').
card_uid('sauté'/'UNH', 'UNH:Sauté:saute').
card_rarity('sauté'/'UNH', 'Common').
card_artist('sauté'/'UNH', 'Jeff Miracola').
card_number('sauté'/'UNH', '88').
card_flavor_text('sauté'/'UNH', '\"Selecting the proper beeble is the key to a good sauté. The pinker the fur and the heartier the yelp, the more succulent the beeble will be when you pop it in your mouth.\"\n—Asmoranomardicadaistinaculdacar,\nThe Underworld Cookbook,').
card_multiverse_id('sauté'/'UNH', '74242').

card_in_set('save life', 'UNH').
card_original_type('save life'/'UNH', 'Instant').
card_original_text('save life'/'UNH', 'Choose one Target player gains 2½ life; or prevent the next 2½ damage that would be dealt to target creature this turn.\nGotcha Whenever an opponent says \"Save\" or \"Life,\" you may say \"Gotcha\" If you do, return Save Life from your graveyard to your hand.').
card_first_print('save life', 'UNH').
card_image_name('save life'/'UNH', 'save life').
card_uid('save life'/'UNH', 'UNH:Save Life:save life').
card_rarity('save life'/'UNH', 'Uncommon').
card_artist('save life'/'UNH', 'Ray Lago').
card_number('save life'/'UNH', '19').
card_flavor_text('save life'/'UNH', 'Who consistently managed to win.').
card_multiverse_id('save life'/'UNH', '74277').

card_in_set('shoe tree', 'UNH').
card_original_type('shoe tree'/'UNH', 'Creature — Treefolk').
card_original_text('shoe tree'/'UNH', 'Shoe Tree comes into play with up to two shoe counters on it. Use your shoes as counters.\nShoe Tree gets +1/+1 for each shoe counter on it.').
card_first_print('shoe tree', 'UNH').
card_image_name('shoe tree'/'UNH', 'shoe tree').
card_uid('shoe tree'/'UNH', 'UNH:Shoe Tree:shoe tree').
card_rarity('shoe tree'/'UNH', 'Common').
card_artist('shoe tree'/'UNH', 'Francis Tsai').
card_number('shoe tree'/'UNH', '109').
card_flavor_text('shoe tree'/'UNH', 'It grows several feet a year.').
card_multiverse_id('shoe tree'/'UNH', '74332').

card_in_set('side to side', 'UNH').
card_original_type('side to side'/'UNH', 'Instant').
card_original_text('side to side'/'UNH', 'You and target opponent arm-wrestle. If you win, put a 3/3 green Ape creature token into play.').
card_first_print('side to side', 'UNH').
card_image_name('side to side'/'UNH', 'side to side').
card_uid('side to side'/'UNH', 'UNH:Side to Side:side to side').
card_rarity('side to side'/'UNH', 'Uncommon').
card_artist('side to side'/'UNH', 'Jim Nelson').
card_number('side to side'/'UNH', '110').
card_flavor_text('side to side'/'UNH', 'His friends at the gym never knew Garanth\'s secret shame. For years he had tried to walk away from professional arm wrestling, but hey—free monkeys.').
card_multiverse_id('side to side'/'UNH', '73948').

card_in_set('six-y beast', 'UNH').
card_original_type('six-y beast'/'UNH', 'Creature — Beast').
card_original_text('six-y beast'/'UNH', 'As Six-y Beast comes into play, you secretly put six or fewer +1/+1 counters on it, then an opponent guesses the number of counters. If that player guesses right, sacrifice Six-y Beast.').
card_first_print('six-y beast', 'UNH').
card_image_name('six-y beast'/'UNH', 'six-y beast').
card_uid('six-y beast'/'UNH', 'UNH:Six-y Beast:six-y beast').
card_rarity('six-y beast'/'UNH', 'Uncommon').
card_artist('six-y beast'/'UNH', 'Anson Maddocks').
card_number('six-y beast'/'UNH', '89').
card_flavor_text('six-y beast'/'UNH', 'Is it six?').
card_multiverse_id('six-y beast'/'UNH', '73934').

card_in_set('smart ass', 'UNH').
card_original_type('smart ass'/'UNH', 'Creature — Donkey Wizard').
card_original_text('smart ass'/'UNH', 'Whenever Smart Ass attacks, name a card. Defending player may reveal his or her hand and show you that the named card isn\'t there. If that player doesn\'t, Smart Ass is unblockable this turn.').
card_first_print('smart ass', 'UNH').
card_image_name('smart ass'/'UNH', 'smart ass').
card_uid('smart ass'/'UNH', 'UNH:Smart Ass:smart ass').
card_rarity('smart ass'/'UNH', 'Common').
card_artist('smart ass'/'UNH', 'Trevor Hairsine').
card_number('smart ass'/'UNH', '45').
card_flavor_text('smart ass'/'UNH', '\"I don\'t get it. Why doesn\'t anybody like me?\"').
card_multiverse_id('smart ass'/'UNH', '74270').

card_in_set('spell counter', 'UNH').
card_original_type('spell counter'/'UNH', 'Instant').
card_original_text('spell counter'/'UNH', 'Counter target spell.\nGotcha Whenever an opponent says \"Spell\" or \"Counter,\" you may say \"Gotcha\" If you do, return Spell Counter from your graveyard to your hand.').
card_first_print('spell counter', 'UNH').
card_image_name('spell counter'/'UNH', 'spell counter').
card_uid('spell counter'/'UNH', 'UNH:Spell Counter:spell counter').
card_rarity('spell counter'/'UNH', 'Uncommon').
card_artist('spell counter'/'UNH', 'Doug Chaffee').
card_number('spell counter'/'UNH', '46').
card_flavor_text('spell counter'/'UNH', 'Playing Unhinged, . . .').
card_multiverse_id('spell counter'/'UNH', '74339').

card_in_set('standing army', 'UNH').
card_original_type('standing army'/'UNH', 'Creature — Human Soldier').
card_original_text('standing army'/'UNH', 'As long as you\'re standing, Standing Army has vigilance. (Attacking doesn\'t cause it to tap.)').
card_first_print('standing army', 'UNH').
card_image_name('standing army'/'UNH', 'standing army').
card_uid('standing army'/'UNH', 'UNH:Standing Army:standing army').
card_rarity('standing army'/'UNH', 'Common').
card_artist('standing army'/'UNH', 'Randy Gallegos').
card_number('standing army'/'UNH', '20').
card_flavor_text('standing army'/'UNH', 'They\'re the chair-men of the bored.').
card_multiverse_id('standing army'/'UNH', '74343').

card_in_set('staying power', 'UNH').
card_original_type('staying power'/'UNH', 'Enchantment').
card_original_text('staying power'/'UNH', 'As long as Staying Power is in play, \"until end of turn\" and \"this turn\" effects don\'t end.').
card_first_print('staying power', 'UNH').
card_image_name('staying power'/'UNH', 'staying power').
card_uid('staying power'/'UNH', 'UNH:Staying Power:staying power').
card_rarity('staying power'/'UNH', 'Rare').
card_artist('staying power'/'UNH', 'Richard Sardinha').
card_number('staying power'/'UNH', '21').
card_flavor_text('staying power'/'UNH', 'Mongo\'s fleas no longer bothered him. But the family of goblins that had moved in behind his left ear was starting to get really irritating.').
card_multiverse_id('staying power'/'UNH', '74276').

card_in_set('stone-cold basilisk', 'UNH').
card_original_type('stone-cold basilisk'/'UNH', 'Creature — Basilisk').
card_original_text('stone-cold basilisk'/'UNH', 'Whenever Stone-Cold Basilisk blocks or becomes blocked by a creature with fewer letters in its name, destroy that creature at end of combat. (Punctuation and spaces aren\'t letters.)\nWhenever an opponent reads Stone-Cold Basilisk, that player is turned to stone until end of turn. Stoned players can\'t attack, block, or play spells or abilities.').
card_first_print('stone-cold basilisk', 'UNH').
card_image_name('stone-cold basilisk'/'UNH', 'stone-cold basilisk').
card_uid('stone-cold basilisk'/'UNH', 'UNH:Stone-Cold Basilisk:stone-cold basilisk').
card_rarity('stone-cold basilisk'/'UNH', 'Uncommon').
card_artist('stone-cold basilisk'/'UNH', 'Don Hazeltine').
card_number('stone-cold basilisk'/'UNH', '112').
card_multiverse_id('stone-cold basilisk'/'UNH', '74246').

card_in_set('stop that', 'UNH').
card_original_type('stop that'/'UNH', 'Instant').
card_original_text('stop that'/'UNH', 'Target player discards a card.\nGotcha Whenever an opponent audibly flicks the cards in his or her hand, you may say \"Gotcha\" If you do, return Stop That from your graveyard to your hand.').
card_first_print('stop that', 'UNH').
card_image_name('stop that'/'UNH', 'stop that').
card_uid('stop that'/'UNH', 'UNH:Stop That:stop that').
card_rarity('stop that'/'UNH', 'Common').
card_artist('stop that'/'UNH', 'Anson Maddocks').
card_number('stop that'/'UNH', '63').
card_flavor_text('stop that'/'UNH', 'Now for a squirt of lemon juice.').
card_multiverse_id('stop that'/'UNH', '74346').

card_in_set('super secret tech', 'UNH').
card_original_type('super secret tech'/'UNH', 'Artifact').
card_original_text('super secret tech'/'UNH', 'All premium spells cost {1} less to play.\nAll premium creatures get +1/+1.').
card_first_print('super secret tech', 'UNH').
card_image_name('super secret tech'/'UNH', 'super secret tech').
card_uid('super secret tech'/'UNH', 'UNH:Super Secret Tech:super secret tech').
card_rarity('super secret tech'/'UNH', 'Rare').
card_artist('super secret tech'/'UNH', 'Dan Frazier').
card_number('super secret tech'/'UNH', '141').
card_flavor_text('super secret tech'/'UNH', '\"Ooh . . . shiny\"').
card_multiverse_id('super secret tech'/'UNH', '74272').

card_in_set('supersize', 'UNH').
card_original_type('supersize'/'UNH', 'Instant').
card_original_text('supersize'/'UNH', 'Target creature gets +3½/+3½ until end of turn.').
card_first_print('supersize', 'UNH').
card_image_name('supersize'/'UNH', 'supersize').
card_uid('supersize'/'UNH', 'UNH:Supersize:supersize').
card_rarity('supersize'/'UNH', 'Common').
card_artist('supersize'/'UNH', 'DiTerlizzi').
card_number('supersize'/'UNH', '113').
card_flavor_text('supersize'/'UNH', 'You want \"mise\" with that?').
card_multiverse_id('supersize'/'UNH', '74315').

card_in_set('swamp', 'UNH').
card_original_type('swamp'/'UNH', '(none)').
card_original_text('swamp'/'UNH', '').
card_border('swamp'/'UNH', 'black').
card_image_name('swamp'/'UNH', 'swamp').
card_uid('swamp'/'UNH', 'UNH:Swamp:swamp').
card_rarity('swamp'/'UNH', 'Basic Land').
card_artist('swamp'/'UNH', 'John Avon').
card_number('swamp'/'UNH', '138').
card_multiverse_id('swamp'/'UNH', '73973').

card_in_set('symbol status', 'UNH').
card_original_type('symbol status'/'UNH', 'Sorcery').
card_original_text('symbol status'/'UNH', 'Put a 1/1 colorless Expansion-Symbol creature token into play for each different expansion symbol among permanents you control.').
card_first_print('symbol status', 'UNH').
card_image_name('symbol status'/'UNH', 'symbol status').
card_uid('symbol status'/'UNH', 'UNH:Symbol Status:symbol status').
card_rarity('symbol status'/'UNH', 'Uncommon').
card_artist('symbol status'/'UNH', 'Liz Danforth').
card_number('symbol status'/'UNH', '114').
card_flavor_text('symbol status'/'UNH', '\"Everyone break into teams. Weaponry, you\'re with me. Nature group, meet by the palm tree. Stoneworks and metalworks, follow the anvil. Everyone else, gather around the giant V.\"\n—Sword of Kaldra').
card_multiverse_id('symbol status'/'UNH', '74265').

card_in_set('tainted monkey', 'UNH').
card_original_type('tainted monkey'/'UNH', 'Creature — Ape').
card_original_text('tainted monkey'/'UNH', '{T}: Choose a word. Target player puts the top card of his or her library into his or her graveyard. If that card has the chosen word in its text box, that player loses 3 life.').
card_first_print('tainted monkey', 'UNH').
card_image_name('tainted monkey'/'UNH', 'tainted monkey').
card_uid('tainted monkey'/'UNH', 'UNH:Tainted Monkey:tainted monkey').
card_rarity('tainted monkey'/'UNH', 'Common').
card_artist('tainted monkey'/'UNH', 'Mark Zug').
card_number('tainted monkey'/'UNH', '64').
card_flavor_text('tainted monkey'/'UNH', '\"C\'mon, choose ‘monkey.\' Everybody loves monkeys\"').
card_multiverse_id('tainted monkey'/'UNH', '73974').

card_in_set('the fallen apart', 'UNH').
card_original_type('the fallen apart'/'UNH', 'Creature — Zombie').
card_original_text('the fallen apart'/'UNH', 'The Fallen Apart comes into play with two arms and two legs.\nWhenever damage is dealt to The Fallen Apart, remove an arm or a leg from it.\nThe Fallen Apart can\'t attack if it has no legs and can\'t block if it has no arms.').
card_first_print('the fallen apart', 'UNH').
card_image_name('the fallen apart'/'UNH', 'the fallen apart').
card_uid('the fallen apart'/'UNH', 'UNH:The Fallen Apart:the fallen apart').
card_rarity('the fallen apart'/'UNH', 'Common').
card_artist('the fallen apart'/'UNH', 'Corey D. Macourek').
card_number('the fallen apart'/'UNH', '55').
card_multiverse_id('the fallen apart'/'UNH', '74288').

card_in_set('time machine', 'UNH').
card_original_type('time machine'/'UNH', 'Artifact').
card_original_text('time machine'/'UNH', '{T}: Remove Time Machine and target nontoken creature you own from the game. Return both cards to play at the beginning of your upkeep on your turn X of the next game you play with the same opponent, where X is the removed creature\'s converted mana cost.').
card_first_print('time machine', 'UNH').
card_image_name('time machine'/'UNH', 'time machine').
card_uid('time machine'/'UNH', 'UNH:Time Machine:time machine').
card_rarity('time machine'/'UNH', 'Rare').
card_artist('time machine'/'UNH', 'John Avon').
card_number('time machine'/'UNH', '128').
card_multiverse_id('time machine'/'UNH', '74342').

card_in_set('togglodyte', 'UNH').
card_original_type('togglodyte'/'UNH', 'Artifact Creature — Golem').
card_original_text('togglodyte'/'UNH', 'Togglodyte comes into play turned on.\nWhenever a player plays a spell, toggle Togglodyte\'s ON/OFF switch.\nAs long as Togglodyte is turned off, it can\'t attack or block, and all damage it would deal is prevented.').
card_first_print('togglodyte', 'UNH').
card_image_name('togglodyte'/'UNH', 'togglodyte').
card_uid('togglodyte'/'UNH', 'UNH:Togglodyte:togglodyte').
card_rarity('togglodyte'/'UNH', 'Uncommon').
card_artist('togglodyte'/'UNH', 'Dan Frazier').
card_number('togglodyte'/'UNH', '129').
card_multiverse_id('togglodyte'/'UNH', '73985').

card_in_set('topsy turvy', 'UNH').
card_original_type('topsy turvy'/'UNH', 'Enchantment').
card_original_text('topsy turvy'/'UNH', 'The phases of each player\'s turn are reversed. (The phases are, in reverse order, end, postcombat main, combat, precombat main, and beginning.)\nIf there are more than two players in the game, the turn order is reversed.').
card_first_print('topsy turvy', 'UNH').
card_image_name('topsy turvy'/'UNH', 'topsy turvy').
card_uid('topsy turvy'/'UNH', 'UNH:Topsy Turvy:topsy turvy').
card_rarity('topsy turvy'/'UNH', 'Rare').
card_artist('topsy turvy'/'UNH', 'Jeff Miracola').
card_number('topsy turvy'/'UNH', '47').
card_flavor_text('topsy turvy'/'UNH', '\"Here on going is heck the what?\"').
card_multiverse_id('topsy turvy'/'UNH', '74321').

card_in_set('touch and go', 'UNH').
card_original_type('touch and go'/'UNH', 'Sorcery').
card_original_text('touch and go'/'UNH', 'Destroy target land.\nGotcha Whenever an opponent touches his or her face, you may say \"Gotcha\" If you do, return Touch and Go from your graveyard to your hand.').
card_first_print('touch and go', 'UNH').
card_image_name('touch and go'/'UNH', 'touch and go').
card_uid('touch and go'/'UNH', 'UNH:Touch and Go:touch and go').
card_rarity('touch and go'/'UNH', 'Common').
card_artist('touch and go'/'UNH', 'Ben Thompson').
card_number('touch and go'/'UNH', '90').
card_flavor_text('touch and go'/'UNH', 'Judge\'s ruling: The interior of the face still counts as the face, so a nose-pick is legitimate grounds for saying \"Gotcha\"').
card_multiverse_id('touch and go'/'UNH', '74349').

card_in_set('toy boat', 'UNH').
card_original_type('toy boat'/'UNH', 'Artifact Creature — Ship').
card_original_text('toy boat'/'UNH', 'Cumulative upkeep—Say \"Toy Boat\" quickly. (At the beginning of your upkeep, put an age counter on Toy Boat, then sacrifice it unless you say \"Toy Boat\" once for each age counter on it—without pausing between or fumbling it.)').
card_first_print('toy boat', 'UNH').
card_image_name('toy boat'/'UNH', 'toy boat').
card_uid('toy boat'/'UNH', 'UNH:Toy Boat:toy boat').
card_rarity('toy boat'/'UNH', 'Uncommon').
card_artist('toy boat'/'UNH', 'Lars Grant-West').
card_number('toy boat'/'UNH', '130').
card_multiverse_id('toy boat'/'UNH', '74234').

card_in_set('uktabi kong', 'UNH').
card_original_type('uktabi kong'/'UNH', 'Creature — Ape').
card_original_text('uktabi kong'/'UNH', 'Trample\nWhen Uktabi Kong comes into play, destroy all artifacts.\nTap two untapped Apes you control: Put a 1/1 green Ape creature token into play.').
card_first_print('uktabi kong', 'UNH').
card_image_name('uktabi kong'/'UNH', 'uktabi kong').
card_uid('uktabi kong'/'UNH', 'UNH:Uktabi Kong:uktabi kong').
card_rarity('uktabi kong'/'UNH', 'Rare').
card_artist('uktabi kong'/'UNH', 'Una Fricker').
card_number('uktabi kong'/'UNH', '115').
card_flavor_text('uktabi kong'/'UNH', '\"I desire the acquisition of a potassium-rich fruit comestible of substantial magnitude.\"').
card_multiverse_id('uktabi kong'/'UNH', '73976').

card_in_set('urza\'s hot tub', 'UNH').
card_original_type('urza\'s hot tub'/'UNH', 'Artifact').
card_original_text('urza\'s hot tub'/'UNH', '{2}, Discard a card: Search your library for a card that shares a complete word in its name with the discarded card, reveal it, and put it into your hand. Then shuffle your library.').
card_first_print('urza\'s hot tub', 'UNH').
card_image_name('urza\'s hot tub'/'UNH', 'urza\'s hot tub').
card_uid('urza\'s hot tub'/'UNH', 'UNH:Urza\'s Hot Tub:urza\'s hot tub').
card_rarity('urza\'s hot tub'/'UNH', 'Uncommon').
card_artist('urza\'s hot tub'/'UNH', 'Stephen Tappin').
card_number('urza\'s hot tub'/'UNH', '131').
card_flavor_text('urza\'s hot tub'/'UNH', 'www.wizards.com/hottub').
card_multiverse_id('urza\'s hot tub'/'UNH', '74345').

card_in_set('vile bile', 'UNH').
card_original_type('vile bile'/'UNH', 'Creature — Ooze').
card_original_text('vile bile'/'UNH', 'Whenever a player\'s skin or fingernail touches Vile Bile, that player loses 2 life.').
card_first_print('vile bile', 'UNH').
card_image_name('vile bile'/'UNH', 'vile bile').
card_uid('vile bile'/'UNH', 'UNH:Vile Bile:vile bile').
card_rarity('vile bile'/'UNH', 'Common').
card_artist('vile bile'/'UNH', 'Tony Szczudlo').
card_number('vile bile'/'UNH', '65').
card_multiverse_id('vile bile'/'UNH', '74319').

card_in_set('water gun balloon game', 'UNH').
card_original_type('water gun balloon game'/'UNH', 'Artifact').
card_original_text('water gun balloon game'/'UNH', 'As Water Gun Balloon Game comes into play, each player puts a pop counter on a 0.\nWhenever a player plays a spell, move that player\'s pop counter up 1.\nWhenever a player\'s pop counter hits 5, that player puts a 5/5 pink Giant Teddy Bear creature token into play and resets all pop counters to 0.').
card_first_print('water gun balloon game', 'UNH').
card_image_name('water gun balloon game'/'UNH', 'water gun balloon game').
card_uid('water gun balloon game'/'UNH', 'UNH:Water Gun Balloon Game:water gun balloon game').
card_rarity('water gun balloon game'/'UNH', 'Rare').
card_artist('water gun balloon game'/'UNH', 'Mark Brill').
card_number('water gun balloon game'/'UNH', '132').
card_multiverse_id('water gun balloon game'/'UNH', '73953').

card_in_set('wet willie of the damned', 'UNH').
card_original_type('wet willie of the damned'/'UNH', 'Sorcery').
card_original_text('wet willie of the damned'/'UNH', 'Wet Willie of the Damned deals 2½ damage to target creature or player and you gain 2½ life.').
card_first_print('wet willie of the damned', 'UNH').
card_image_name('wet willie of the damned'/'UNH', 'wet willie of the damned').
card_uid('wet willie of the damned'/'UNH', 'UNH:Wet Willie of the Damned:wet willie of the damned').
card_rarity('wet willie of the damned'/'UNH', 'Common').
card_artist('wet willie of the damned'/'UNH', 'Greg Staples').
card_number('wet willie of the damned'/'UNH', '66').
card_flavor_text('wet willie of the damned'/'UNH', 'Ever the warrior, Glissa responded with a \"wedgie from beyond Uranus.\"').
card_multiverse_id('wet willie of the damned'/'UNH', '74219').

card_in_set('what', 'UNH').
card_original_type('what'/'UNH', 'Instant').
card_original_text('what'/'UNH', 'Destroy target artifact.').
card_first_print('what', 'UNH').
card_image_name('what'/'UNH', 'who what when where why').
card_uid('what'/'UNH', 'UNH:What:who what when where why').
card_rarity('what'/'UNH', 'Rare').
card_artist('what'/'UNH', 'Dan Frazier').
card_number('what'/'UNH', '120b').
card_multiverse_id('what'/'UNH', '74358').

card_in_set('when', 'UNH').
card_original_type('when'/'UNH', 'Instant').
card_original_text('when'/'UNH', 'Counter target creature spell.').
card_first_print('when', 'UNH').
card_image_name('when'/'UNH', 'who what when where why').
card_uid('when'/'UNH', 'UNH:When:who what when where why').
card_rarity('when'/'UNH', 'Rare').
card_artist('when'/'UNH', 'Dan Frazier').
card_number('when'/'UNH', '120c').
card_multiverse_id('when'/'UNH', '74358').

card_in_set('when fluffy bunnies attack', 'UNH').
card_original_type('when fluffy bunnies attack'/'UNH', 'Instant').
card_original_text('when fluffy bunnies attack'/'UNH', 'Target creature gets -X/-X until end of turn, where X is the number of times the letter of your choice appears in that creature\'s name.').
card_first_print('when fluffy bunnies attack', 'UNH').
card_image_name('when fluffy bunnies attack'/'UNH', 'when fluffy bunnies attack').
card_uid('when fluffy bunnies attack'/'UNH', 'UNH:When Fluffy Bunnies Attack:when fluffy bunnies attack').
card_rarity('when fluffy bunnies attack'/'UNH', 'Common').
card_artist('when fluffy bunnies attack'/'UNH', 'Ray Lago').
card_number('when fluffy bunnies attack'/'UNH', '67').
card_flavor_text('when fluffy bunnies attack'/'UNH', '\"Get it? Bunnies, letters, -X/-X? Me neither.\"\n—Bucky, flavor text writer').
card_multiverse_id('when fluffy bunnies attack'/'UNH', '74274').

card_in_set('where', 'UNH').
card_original_type('where'/'UNH', 'Instant').
card_original_text('where'/'UNH', 'Destroy target land.').
card_first_print('where', 'UNH').
card_image_name('where'/'UNH', 'who what when where why').
card_uid('where'/'UNH', 'UNH:Where:who what when where why').
card_rarity('where'/'UNH', 'Rare').
card_artist('where'/'UNH', 'Dan Frazier').
card_number('where'/'UNH', '120d').
card_multiverse_id('where'/'UNH', '74358').

card_in_set('who', 'UNH').
card_original_type('who'/'UNH', 'Instant').
card_original_text('who'/'UNH', 'Target player gains X life.').
card_first_print('who', 'UNH').
card_image_name('who'/'UNH', 'who what when where why').
card_uid('who'/'UNH', 'UNH:Who:who what when where why').
card_rarity('who'/'UNH', 'Rare').
card_artist('who'/'UNH', 'Dan Frazier').
card_number('who'/'UNH', '120a').
card_multiverse_id('who'/'UNH', '74358').

card_in_set('why', 'UNH').
card_original_type('why'/'UNH', 'Instant').
card_original_text('why'/'UNH', 'Destroy target enchantment.').
card_first_print('why', 'UNH').
card_image_name('why'/'UNH', 'who what when where why').
card_uid('why'/'UNH', 'UNH:Why:who what when where why').
card_rarity('why'/'UNH', 'Rare').
card_artist('why'/'UNH', 'Dan Frazier').
card_number('why'/'UNH', '120e').
card_multiverse_id('why'/'UNH', '74358').

card_in_set('wordmail', 'UNH').
card_original_type('wordmail'/'UNH', 'Enchant Creature').
card_original_text('wordmail'/'UNH', 'Enchanted creature gets +1/+1 for each word in its name.').
card_first_print('wordmail', 'UNH').
card_image_name('wordmail'/'UNH', 'wordmail').
card_uid('wordmail'/'UNH', 'UNH:Wordmail:wordmail').
card_rarity('wordmail'/'UNH', 'Common').
card_artist('wordmail'/'UNH', 'Ron Spencer').
card_number('wordmail'/'UNH', '22').
card_flavor_text('wordmail'/'UNH', '\"Suck on it, Stangg\"\n—Sol\'kanar the Swamp King').
card_multiverse_id('wordmail'/'UNH', '73972').

card_in_set('working stiff', 'UNH').
card_original_type('working stiff'/'UNH', 'Creature — Mummy').
card_original_text('working stiff'/'UNH', 'As Working Stiff comes into play, straighten your arms.\nWhen you bend an elbow, sacrifice Working Stiff.').
card_first_print('working stiff', 'UNH').
card_image_name('working stiff'/'UNH', 'working stiff').
card_uid('working stiff'/'UNH', 'UNH:Working Stiff:working stiff').
card_rarity('working stiff'/'UNH', 'Uncommon').
card_artist('working stiff'/'UNH', 'Darrell Riche').
card_number('working stiff'/'UNH', '68').
card_flavor_text('working stiff'/'UNH', 'Oh, does your nose itch?').
card_multiverse_id('working stiff'/'UNH', '74241').

card_in_set('world-bottling kit', 'UNH').
card_original_type('world-bottling kit'/'UNH', 'Artifact').
card_original_text('world-bottling kit'/'UNH', '{5}, Sacrifice World-Bottling Kit: Choose a Magic set. Remove from the game all permanents with that set\'s expansion symbol except for basic lands.').
card_first_print('world-bottling kit', 'UNH').
card_image_name('world-bottling kit'/'UNH', 'world-bottling kit').
card_uid('world-bottling kit'/'UNH', 'UNH:World-Bottling Kit:world-bottling kit').
card_rarity('world-bottling kit'/'UNH', 'Rare').
card_artist('world-bottling kit'/'UNH', 'Alan Rabinowitz').
card_number('world-bottling kit'/'UNH', '133').
card_flavor_text('world-bottling kit'/'UNH', 'YOUR AD HERE').
card_multiverse_id('world-bottling kit'/'UNH', '74334').

card_in_set('yet another æther vortex', 'UNH').
card_original_type('yet another æther vortex'/'UNH', 'Enchantment').
card_original_text('yet another æther vortex'/'UNH', 'All creatures have haste.\nPlayers play with the top card of their libraries revealed.\nNoninstant, nonsorcery cards on top of a library are in play under their owner\'s control in addition to being in that library.').
card_first_print('yet another æther vortex', 'UNH').
card_image_name('yet another æther vortex'/'UNH', 'yet another aether vortex').
card_uid('yet another æther vortex'/'UNH', 'UNH:Yet Another Æther Vortex:yet another aether vortex').
card_rarity('yet another æther vortex'/'UNH', 'Rare').
card_artist('yet another æther vortex'/'UNH', 'David Day').
card_number('yet another æther vortex'/'UNH', '91').
card_flavor_text('yet another æther vortex'/'UNH', 'It puts the \"vortex\" in \"flavortext.\"').
card_multiverse_id('yet another æther vortex'/'UNH', '74331').

card_in_set('zombie fanboy', 'UNH').
card_original_type('zombie fanboy'/'UNH', 'Creature — Zombie Gamer').
card_original_text('zombie fanboy'/'UNH', 'As Zombie Fanboy comes into play, choose an artist.\nWhenever a permanent by the chosen artist is put into a graveyard, put two +1/+1 counters on Zombie Fanboy.').
card_first_print('zombie fanboy', 'UNH').
card_image_name('zombie fanboy'/'UNH', 'zombie fanboy').
card_uid('zombie fanboy'/'UNH', 'UNH:Zombie Fanboy:zombie fanboy').
card_rarity('zombie fanboy'/'UNH', 'Uncommon').
card_artist('zombie fanboy'/'UNH', 'Matt “I\'m Your Boy” Cavotta').
card_number('zombie fanboy'/'UNH', '69').
card_flavor_text('zombie fanboy'/'UNH', 'The real advantage of being a zombie gamer? No one notices the stench.').
card_multiverse_id('zombie fanboy'/'UNH', '74290').

card_in_set('zzzyxas\'s abyss', 'UNH').
card_original_type('zzzyxas\'s abyss'/'UNH', 'Enchantment').
card_original_text('zzzyxas\'s abyss'/'UNH', 'At the beginning of your upkeep, destroy all nonland permanents with the first name alphabetically among nonland permanents in play.').
card_first_print('zzzyxas\'s abyss', 'UNH').
card_image_name('zzzyxas\'s abyss'/'UNH', 'zzzyxas\'s abyss').
card_uid('zzzyxas\'s abyss'/'UNH', 'UNH:Zzzyxas\'s Abyss:zzzyxas\'s abyss').
card_rarity('zzzyxas\'s abyss'/'UNH', 'Rare').
card_artist('zzzyxas\'s abyss'/'UNH', 'Pete Venters').
card_number('zzzyxas\'s abyss'/'UNH', '70').
card_flavor_text('zzzyxas\'s abyss'/'UNH', 'Tired of always being picked last, the wizard Zzzyxas created the ultimate revenge.').
card_multiverse_id('zzzyxas\'s abyss'/'UNH', '74336').
