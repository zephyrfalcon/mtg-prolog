% Revised Edition

set('3ED').
set_name('3ED', 'Revised Edition').
set_release_date('3ED', '1994-04-01').
set_border('3ED', 'white').
set_type('3ED', 'core').

card_in_set('air elemental', '3ED').
card_original_type('air elemental'/'3ED', 'Summon — Elemental').
card_original_text('air elemental'/'3ED', 'Flying').
card_image_name('air elemental'/'3ED', 'air elemental').
card_uid('air elemental'/'3ED', '3ED:Air Elemental:air elemental').
card_rarity('air elemental'/'3ED', 'Uncommon').
card_artist('air elemental'/'3ED', 'Richard Thomas').
card_flavor_text('air elemental'/'3ED', 'These spirits of the air are winsome and wild, and cannot be truly contained. Only marginally intelligent, they often substitute whimsy for strategy, delighting in mischief and mayhem.').
card_multiverse_id('air elemental'/'3ED', '1189').

card_in_set('aladdin\'s lamp', '3ED').
card_original_type('aladdin\'s lamp'/'3ED', 'Artifact').
card_original_text('aladdin\'s lamp'/'3ED', '{X}, {T}: Instead of drawing a card from the top of your library, draw X cards but choose only one to put in your hand. You must shuffle the leftover cards and put them at the bottom of your library.').
card_image_name('aladdin\'s lamp'/'3ED', 'aladdin\'s lamp').
card_uid('aladdin\'s lamp'/'3ED', '3ED:Aladdin\'s Lamp:aladdin\'s lamp').
card_rarity('aladdin\'s lamp'/'3ED', 'Rare').
card_artist('aladdin\'s lamp'/'3ED', 'Mark Tedin').
card_multiverse_id('aladdin\'s lamp'/'3ED', '1092').

card_in_set('aladdin\'s ring', '3ED').
card_original_type('aladdin\'s ring'/'3ED', 'Artifact').
card_original_text('aladdin\'s ring'/'3ED', '{8}, {T} Aladdin\'s Ring does 4 damage to any target.').
card_image_name('aladdin\'s ring'/'3ED', 'aladdin\'s ring').
card_uid('aladdin\'s ring'/'3ED', '3ED:Aladdin\'s Ring:aladdin\'s ring').
card_rarity('aladdin\'s ring'/'3ED', 'Rare').
card_artist('aladdin\'s ring'/'3ED', 'Dan Frazier').
card_flavor_text('aladdin\'s ring'/'3ED', '\"After these words the magician drew a ring off his finger, and put it on one of Aladdin\'s, saying: \'It is a talisman against all evil, so long as you obey me.\'\" —The Arabian Nights, Junior Classics trans.').
card_multiverse_id('aladdin\'s ring'/'3ED', '1093').

card_in_set('animate artifact', '3ED').
card_original_type('animate artifact'/'3ED', 'Enchant Artifact').
card_original_text('animate artifact'/'3ED', 'Target artifact is now an artifact creature with both power and toughness equal to its casting cost; target retains all its original abilities as well. Has no effect on artifact creatures.').
card_image_name('animate artifact'/'3ED', 'animate artifact').
card_uid('animate artifact'/'3ED', '3ED:Animate Artifact:animate artifact').
card_rarity('animate artifact'/'3ED', 'Uncommon').
card_artist('animate artifact'/'3ED', 'Douglas Shuler').
card_multiverse_id('animate artifact'/'3ED', '1190').

card_in_set('animate dead', '3ED').
card_original_type('animate dead'/'3ED', 'Enchant Dead Creature').
card_original_text('animate dead'/'3ED', 'Any creature in any graveyard comes into play on your side with -1 to its original power. At end of game, or if this enchantment is discarded without removing target creature from play, target creature is returned to its owner\'s graveyard. Target creature may be killed as normal.').
card_image_name('animate dead'/'3ED', 'animate dead').
card_uid('animate dead'/'3ED', '3ED:Animate Dead:animate dead').
card_rarity('animate dead'/'3ED', 'Uncommon').
card_artist('animate dead'/'3ED', 'Anson Maddocks').
card_multiverse_id('animate dead'/'3ED', '1143').

card_in_set('animate wall', '3ED').
card_original_type('animate wall'/'3ED', 'Enchant Wall').
card_original_text('animate wall'/'3ED', 'Target wall can now attack. Target wall\'s power and toughness are unchanged by this enchantment, even if its power is 0.').
card_image_name('animate wall'/'3ED', 'animate wall').
card_uid('animate wall'/'3ED', '3ED:Animate Wall:animate wall').
card_rarity('animate wall'/'3ED', 'Rare').
card_artist('animate wall'/'3ED', 'Dan Frazier').
card_multiverse_id('animate wall'/'3ED', '1327').

card_in_set('ankh of mishra', '3ED').
card_original_type('ankh of mishra'/'3ED', 'Artifact').
card_original_text('ankh of mishra'/'3ED', 'Ankh does 2 damage to anyone who puts a new land into play.').
card_image_name('ankh of mishra'/'3ED', 'ankh of mishra').
card_uid('ankh of mishra'/'3ED', '3ED:Ankh of Mishra:ankh of mishra').
card_rarity('ankh of mishra'/'3ED', 'Rare').
card_artist('ankh of mishra'/'3ED', 'Amy Weber').
card_multiverse_id('ankh of mishra'/'3ED', '1094').

card_in_set('armageddon', '3ED').
card_original_type('armageddon'/'3ED', 'Sorcery').
card_original_text('armageddon'/'3ED', 'All lands in play are destroyed.').
card_image_name('armageddon'/'3ED', 'armageddon').
card_uid('armageddon'/'3ED', '3ED:Armageddon:armageddon').
card_rarity('armageddon'/'3ED', 'Rare').
card_artist('armageddon'/'3ED', 'Jesper Myrfors').
card_multiverse_id('armageddon'/'3ED', '1328').

card_in_set('armageddon clock', '3ED').
card_original_type('armageddon clock'/'3ED', 'Artifact').
card_original_text('armageddon clock'/'3ED', 'Put one counter on Armageddon Clock during each of your upkeeps. At the end of your upkeep, each player takes damage equal to the number of counters on the Clock. Any player may spend {4} during any upkeep to remove a counter.').
card_image_name('armageddon clock'/'3ED', 'armageddon clock').
card_uid('armageddon clock'/'3ED', '3ED:Armageddon Clock:armageddon clock').
card_rarity('armageddon clock'/'3ED', 'Rare').
card_artist('armageddon clock'/'3ED', 'Amy Weber').
card_multiverse_id('armageddon clock'/'3ED', '1095').

card_in_set('aspect of wolf', '3ED').
card_original_type('aspect of wolf'/'3ED', 'Enchant Creature').
card_original_text('aspect of wolf'/'3ED', 'Target creature\'s power and toughness are increased by half the number of forests you have in play, rounding down for power and up for toughness.').
card_image_name('aspect of wolf'/'3ED', 'aspect of wolf').
card_uid('aspect of wolf'/'3ED', '3ED:Aspect of Wolf:aspect of wolf').
card_rarity('aspect of wolf'/'3ED', 'Rare').
card_artist('aspect of wolf'/'3ED', 'Jeff A. Menges').
card_multiverse_id('aspect of wolf'/'3ED', '1235').

card_in_set('atog', '3ED').
card_original_type('atog'/'3ED', 'Summon — Atog').
card_original_text('atog'/'3ED', '{0}: +2/+2. Each time you use this ability, you must sacrifice one of your artifacts in play.').
card_image_name('atog'/'3ED', 'atog').
card_uid('atog'/'3ED', '3ED:Atog:atog').
card_rarity('atog'/'3ED', 'Common').
card_artist('atog'/'3ED', 'Jesper Myrfors').
card_flavor_text('atog'/'3ED', 'The bane of all artificers, the legendary Atogs devoured intricate tools to further their own twisted growth.').
card_multiverse_id('atog'/'3ED', '1280').

card_in_set('bad moon', '3ED').
card_original_type('bad moon'/'3ED', 'Enchantment').
card_original_text('bad moon'/'3ED', 'All black creatures in play gain +1/+1.').
card_image_name('bad moon'/'3ED', 'bad moon').
card_uid('bad moon'/'3ED', '3ED:Bad Moon:bad moon').
card_rarity('bad moon'/'3ED', 'Rare').
card_artist('bad moon'/'3ED', 'Jesper Myrfors').
card_multiverse_id('bad moon'/'3ED', '1144').

card_in_set('badlands', '3ED').
card_original_type('badlands'/'3ED', 'Land').
card_original_text('badlands'/'3ED', '{T}: Add either {R} or {B} to your mana pool.\nCounts as both mountains and swamp and is affected by spells that affect either. If a spell destroys one of these land types, this card is destroyed; if a spell alters one of these land types, the other land type is unaffected.').
card_image_name('badlands'/'3ED', 'badlands').
card_uid('badlands'/'3ED', '3ED:Badlands:badlands').
card_rarity('badlands'/'3ED', 'Rare').
card_artist('badlands'/'3ED', 'Rob Alexander').
card_multiverse_id('badlands'/'3ED', '1376').

card_in_set('balance', '3ED').
card_original_type('balance'/'3ED', 'Sorcery').
card_original_text('balance'/'3ED', 'Whichever player has more lands in play must discard enough lands of his or her choice to equalize the number of lands both players have in play. Next, equalize the cards in hand and then creatures in play the same way. Creatures lost in this manner are considered buried.').
card_image_name('balance'/'3ED', 'balance').
card_uid('balance'/'3ED', '3ED:Balance:balance').
card_rarity('balance'/'3ED', 'Rare').
card_artist('balance'/'3ED', 'Mark Poole').
card_multiverse_id('balance'/'3ED', '1329').

card_in_set('basalt monolith', '3ED').
card_original_type('basalt monolith'/'3ED', 'Artifact').
card_original_text('basalt monolith'/'3ED', '{T}: Add 3 colorless mana to your mana pool. Does not untap as normal during untap phase; you may spend {3} at any other time to untap. Drawing mana from this artifact is played as an interrupt.').
card_image_name('basalt monolith'/'3ED', 'basalt monolith').
card_uid('basalt monolith'/'3ED', '3ED:Basalt Monolith:basalt monolith').
card_rarity('basalt monolith'/'3ED', 'Uncommon').
card_artist('basalt monolith'/'3ED', 'Jesper Myrfors').
card_multiverse_id('basalt monolith'/'3ED', '1096').

card_in_set('bayou', '3ED').
card_original_type('bayou'/'3ED', 'Land').
card_original_text('bayou'/'3ED', '{T}: Add either {B} or {G} to your mana pool.\nCounts as both swamp and forest and is affected by spells that affect either. If a spell destroys one of these land types, this card is destroyed; if a spell alters one of these land types, the other land type is unaffected.').
card_image_name('bayou'/'3ED', 'bayou').
card_uid('bayou'/'3ED', '3ED:Bayou:bayou').
card_rarity('bayou'/'3ED', 'Rare').
card_artist('bayou'/'3ED', 'Jesper Myrfors').
card_multiverse_id('bayou'/'3ED', '1377').

card_in_set('benalish hero', '3ED').
card_original_type('benalish hero'/'3ED', 'Summon — Hero').
card_original_text('benalish hero'/'3ED', 'Bands').
card_image_name('benalish hero'/'3ED', 'benalish hero').
card_uid('benalish hero'/'3ED', '3ED:Benalish Hero:benalish hero').
card_rarity('benalish hero'/'3ED', 'Common').
card_artist('benalish hero'/'3ED', 'Douglas Shuler').
card_flavor_text('benalish hero'/'3ED', 'Benalia has a complex caste system that changes with the lunar year. No matter what the season, the only caste that cannot be attained by either heredity or money is that of the hero.').
card_multiverse_id('benalish hero'/'3ED', '1330').

card_in_set('birds of paradise', '3ED').
card_original_type('birds of paradise'/'3ED', 'Summon — Mana Birds').
card_original_text('birds of paradise'/'3ED', 'Flying\n{T}: Add one mana of any color to your mana pool. This ability is played as an interrupt.').
card_image_name('birds of paradise'/'3ED', 'birds of paradise').
card_uid('birds of paradise'/'3ED', '3ED:Birds of Paradise:birds of paradise').
card_rarity('birds of paradise'/'3ED', 'Rare').
card_artist('birds of paradise'/'3ED', 'Mark Poole').
card_multiverse_id('birds of paradise'/'3ED', '1236').

card_in_set('black knight', '3ED').
card_original_type('black knight'/'3ED', 'Summon — Knight').
card_original_text('black knight'/'3ED', 'Protection from white, first strike').
card_image_name('black knight'/'3ED', 'black knight').
card_uid('black knight'/'3ED', '3ED:Black Knight:black knight').
card_rarity('black knight'/'3ED', 'Uncommon').
card_artist('black knight'/'3ED', 'Jeff A. Menges').
card_flavor_text('black knight'/'3ED', 'Battle doesn\'t need a purpose; the battle is its own purpose. You don\'t ask why a plague spreads or a field burns. Don\'t ask why I fight.').
card_multiverse_id('black knight'/'3ED', '1145').

card_in_set('black vise', '3ED').
card_original_type('black vise'/'3ED', 'Artifact').
card_original_text('black vise'/'3ED', 'If opponent has more than four cards in hand during his or her upkeep, Black Vise does 1 damage to opponent for each card in excess of four.').
card_image_name('black vise'/'3ED', 'black vise').
card_uid('black vise'/'3ED', '3ED:Black Vise:black vise').
card_rarity('black vise'/'3ED', 'Uncommon').
card_artist('black vise'/'3ED', 'Richard Thomas').
card_multiverse_id('black vise'/'3ED', '1097').

card_in_set('black ward', '3ED').
card_original_type('black ward'/'3ED', 'Enchant Creature').
card_original_text('black ward'/'3ED', 'Target creature gains protection from black.').
card_image_name('black ward'/'3ED', 'black ward').
card_uid('black ward'/'3ED', '3ED:Black Ward:black ward').
card_rarity('black ward'/'3ED', 'Uncommon').
card_artist('black ward'/'3ED', 'Dan Frazier').
card_multiverse_id('black ward'/'3ED', '1331').

card_in_set('blessing', '3ED').
card_original_type('blessing'/'3ED', 'Enchant Creature').
card_original_text('blessing'/'3ED', '{W} +1/+1').
card_image_name('blessing'/'3ED', 'blessing').
card_uid('blessing'/'3ED', '3ED:Blessing:blessing').
card_rarity('blessing'/'3ED', 'Rare').
card_artist('blessing'/'3ED', 'Julie Baroh').
card_multiverse_id('blessing'/'3ED', '1332').

card_in_set('blue elemental blast', '3ED').
card_original_type('blue elemental blast'/'3ED', 'Interrupt').
card_original_text('blue elemental blast'/'3ED', 'Counters a red spell being cast or destroys a red card in play.').
card_image_name('blue elemental blast'/'3ED', 'blue elemental blast').
card_uid('blue elemental blast'/'3ED', '3ED:Blue Elemental Blast:blue elemental blast').
card_rarity('blue elemental blast'/'3ED', 'Common').
card_artist('blue elemental blast'/'3ED', 'Richard Thomas').
card_multiverse_id('blue elemental blast'/'3ED', '1191').

card_in_set('blue ward', '3ED').
card_original_type('blue ward'/'3ED', 'Enchant Creature').
card_original_text('blue ward'/'3ED', 'Target creature gains protection from blue.').
card_image_name('blue ward'/'3ED', 'blue ward').
card_uid('blue ward'/'3ED', '3ED:Blue Ward:blue ward').
card_rarity('blue ward'/'3ED', 'Uncommon').
card_artist('blue ward'/'3ED', 'Dan Frazier').
card_multiverse_id('blue ward'/'3ED', '1333').

card_in_set('bog wraith', '3ED').
card_original_type('bog wraith'/'3ED', 'Summon — Wraith').
card_original_text('bog wraith'/'3ED', 'Swampwalk').
card_image_name('bog wraith'/'3ED', 'bog wraith').
card_uid('bog wraith'/'3ED', '3ED:Bog Wraith:bog wraith').
card_rarity('bog wraith'/'3ED', 'Uncommon').
card_artist('bog wraith'/'3ED', 'Jeff A. Menges').
card_flavor_text('bog wraith'/'3ED', '\'Twas in the bogs of Cannelbrae\nMy mate did meet an early grave\n\'Twas nothing left for us to save\nIn the peat-filled bogs of Cannelbrae.').
card_multiverse_id('bog wraith'/'3ED', '1146').

card_in_set('bottle of suleiman', '3ED').
card_original_type('bottle of suleiman'/'3ED', 'Artifact').
card_original_text('bottle of suleiman'/'3ED', '{1}: Flip a coin, with opponent calling heads or tails while coin is in the air. If the flip ends up in opponent\'s favor, Bottle of Suleiman does 5 damage to you. Otherwise, a 5/5 flying Djinn immediately comes into play on your side. Use a counter to represent Djinn. Djinn is treated exactly like a normal artifact creature except that if it leaves play it is removed from the game entirely. No matter how the flip turns out, Bottle of Suleiman is discarded after use.').
card_image_name('bottle of suleiman'/'3ED', 'bottle of suleiman').
card_uid('bottle of suleiman'/'3ED', '3ED:Bottle of Suleiman:bottle of suleiman').
card_rarity('bottle of suleiman'/'3ED', 'Rare').
card_artist('bottle of suleiman'/'3ED', 'Jesper Myrfors').
card_multiverse_id('bottle of suleiman'/'3ED', '1098').

card_in_set('braingeyser', '3ED').
card_original_type('braingeyser'/'3ED', 'Sorcery').
card_original_text('braingeyser'/'3ED', 'Target player must draw X cards.').
card_image_name('braingeyser'/'3ED', 'braingeyser').
card_uid('braingeyser'/'3ED', '3ED:Braingeyser:braingeyser').
card_rarity('braingeyser'/'3ED', 'Rare').
card_artist('braingeyser'/'3ED', 'Mark Tedin').
card_multiverse_id('braingeyser'/'3ED', '1192').

card_in_set('brass man', '3ED').
card_original_type('brass man'/'3ED', 'Artifact Creature').
card_original_text('brass man'/'3ED', 'Brass Man does not untap as normal; you must pay {1} during your upkeep phase to untap it.').
card_image_name('brass man'/'3ED', 'brass man').
card_uid('brass man'/'3ED', '3ED:Brass Man:brass man').
card_rarity('brass man'/'3ED', 'Uncommon').
card_artist('brass man'/'3ED', 'Christopher Rush').
card_multiverse_id('brass man'/'3ED', '1099').

card_in_set('burrowing', '3ED').
card_original_type('burrowing'/'3ED', 'Enchant Creature').
card_original_text('burrowing'/'3ED', 'Target creature gains mountainwalk.').
card_image_name('burrowing'/'3ED', 'burrowing').
card_uid('burrowing'/'3ED', '3ED:Burrowing:burrowing').
card_rarity('burrowing'/'3ED', 'Uncommon').
card_artist('burrowing'/'3ED', 'Mark Poole').
card_multiverse_id('burrowing'/'3ED', '1281').

card_in_set('castle', '3ED').
card_original_type('castle'/'3ED', 'Enchantment').
card_original_text('castle'/'3ED', 'Your untapped creatures gain +0/+2. Attacking creatures do not get this bonus.').
card_image_name('castle'/'3ED', 'castle').
card_uid('castle'/'3ED', '3ED:Castle:castle').
card_rarity('castle'/'3ED', 'Uncommon').
card_artist('castle'/'3ED', 'Dameon Willich').
card_multiverse_id('castle'/'3ED', '1334').

card_in_set('celestial prism', '3ED').
card_original_type('celestial prism'/'3ED', 'Artifact').
card_original_text('celestial prism'/'3ED', '{2}, {T}: Provides 1 mana of any color. This use is played as an interrupt.').
card_image_name('celestial prism'/'3ED', 'celestial prism').
card_uid('celestial prism'/'3ED', '3ED:Celestial Prism:celestial prism').
card_rarity('celestial prism'/'3ED', 'Uncommon').
card_artist('celestial prism'/'3ED', 'Amy Weber').
card_multiverse_id('celestial prism'/'3ED', '1100').

card_in_set('channel', '3ED').
card_original_type('channel'/'3ED', 'Sorcery').
card_original_text('channel'/'3ED', 'Until end of turn, you may add colorless mana to your mana pool at a cost of 1 life per point of mana. These additions are played with the speed of an interrupt. Effects that prevent or redirect damage may not be used to counter this loss of life.').
card_image_name('channel'/'3ED', 'channel').
card_uid('channel'/'3ED', '3ED:Channel:channel').
card_rarity('channel'/'3ED', 'Uncommon').
card_artist('channel'/'3ED', 'Richard Thomas').
card_multiverse_id('channel'/'3ED', '1237').

card_in_set('chaoslace', '3ED').
card_original_type('chaoslace'/'3ED', 'Interrupt').
card_original_text('chaoslace'/'3ED', 'Changes the color of one card either being played or already in play to red. Cost to cast, tap, maintain, or use a special ability of target card remains entirely unchanged.').
card_image_name('chaoslace'/'3ED', 'chaoslace').
card_uid('chaoslace'/'3ED', '3ED:Chaoslace:chaoslace').
card_rarity('chaoslace'/'3ED', 'Rare').
card_artist('chaoslace'/'3ED', 'Dameon Willich').
card_multiverse_id('chaoslace'/'3ED', '1282').

card_in_set('circle of protection: black', '3ED').
card_original_type('circle of protection: black'/'3ED', 'Enchantment').
card_original_text('circle of protection: black'/'3ED', '{1}: Prevents all damage against you from one black source. If a source does damage to you more than once in a turn, you must pay 1 mana each time you want to prevent the damage.').
card_image_name('circle of protection: black'/'3ED', 'circle of protection black').
card_uid('circle of protection: black'/'3ED', '3ED:Circle of Protection: Black:circle of protection black').
card_rarity('circle of protection: black'/'3ED', 'Common').
card_artist('circle of protection: black'/'3ED', 'Jesper Myrfors').
card_multiverse_id('circle of protection: black'/'3ED', '1335').

card_in_set('circle of protection: blue', '3ED').
card_original_type('circle of protection: blue'/'3ED', 'Enchantment').
card_original_text('circle of protection: blue'/'3ED', '{1}: Prevents all damage against you from one blue source. If a source does damage to you more than once in a turn, you must pay 1 mana each time you want to prevent the damage.').
card_image_name('circle of protection: blue'/'3ED', 'circle of protection blue').
card_uid('circle of protection: blue'/'3ED', '3ED:Circle of Protection: Blue:circle of protection blue').
card_rarity('circle of protection: blue'/'3ED', 'Common').
card_artist('circle of protection: blue'/'3ED', 'Dameon Willich').
card_multiverse_id('circle of protection: blue'/'3ED', '1336').

card_in_set('circle of protection: green', '3ED').
card_original_type('circle of protection: green'/'3ED', 'Enchantment').
card_original_text('circle of protection: green'/'3ED', '{1}: Prevents all damage against you from one green source. If a source does damage to you more than once in a turn, you must pay 1 mana each time you want to prevent the damage.').
card_image_name('circle of protection: green'/'3ED', 'circle of protection green').
card_uid('circle of protection: green'/'3ED', '3ED:Circle of Protection: Green:circle of protection green').
card_rarity('circle of protection: green'/'3ED', 'Common').
card_artist('circle of protection: green'/'3ED', 'Sandra Everingham').
card_multiverse_id('circle of protection: green'/'3ED', '1337').

card_in_set('circle of protection: red', '3ED').
card_original_type('circle of protection: red'/'3ED', 'Enchantment').
card_original_text('circle of protection: red'/'3ED', '{1}: Prevents all damage against you from one red source. If a source does damage to you more than once in a turn, you must pay 1 mana each time you want to prevent the damage.').
card_image_name('circle of protection: red'/'3ED', 'circle of protection red').
card_uid('circle of protection: red'/'3ED', '3ED:Circle of Protection: Red:circle of protection red').
card_rarity('circle of protection: red'/'3ED', 'Common').
card_artist('circle of protection: red'/'3ED', 'Mark Tedin').
card_multiverse_id('circle of protection: red'/'3ED', '1338').

card_in_set('circle of protection: white', '3ED').
card_original_type('circle of protection: white'/'3ED', 'Enchantment').
card_original_text('circle of protection: white'/'3ED', '{1}: Prevents all damage against you from one white source. If a source does damage to you more than once in a turn, you must pay 1 mana each time you want to prevent the damage.').
card_image_name('circle of protection: white'/'3ED', 'circle of protection white').
card_uid('circle of protection: white'/'3ED', '3ED:Circle of Protection: White:circle of protection white').
card_rarity('circle of protection: white'/'3ED', 'Common').
card_artist('circle of protection: white'/'3ED', 'Douglas Shuler').
card_multiverse_id('circle of protection: white'/'3ED', '1339').

card_in_set('clockwork beast', '3ED').
card_original_type('clockwork beast'/'3ED', 'Artifact Creature').
card_original_text('clockwork beast'/'3ED', 'Put seven +1/+0 counters on Beast. After Beast attacks or blocks a creature, discard a counter. During the upkeep phase, controller may buy back lost counters for {1} per counter; this taps Beast.').
card_image_name('clockwork beast'/'3ED', 'clockwork beast').
card_uid('clockwork beast'/'3ED', '3ED:Clockwork Beast:clockwork beast').
card_rarity('clockwork beast'/'3ED', 'Rare').
card_artist('clockwork beast'/'3ED', 'Drew Tucker').
card_multiverse_id('clockwork beast'/'3ED', '1101').

card_in_set('clone', '3ED').
card_original_type('clone'/'3ED', 'Summon — Clone').
card_original_text('clone'/'3ED', 'Upon summoning, Clone acquires all characteristics, including color, of any one creature in play on either side; any creature enchantments on original creature are not copied. Clone retains these characteristics even after original creature is destroyed. Clone cannot be summoned if there are no creatures in play.').
card_image_name('clone'/'3ED', 'clone').
card_uid('clone'/'3ED', '3ED:Clone:clone').
card_rarity('clone'/'3ED', 'Uncommon').
card_artist('clone'/'3ED', 'Julie Baroh').
card_multiverse_id('clone'/'3ED', '1193').

card_in_set('cockatrice', '3ED').
card_original_type('cockatrice'/'3ED', 'Summon — Cockatrice').
card_original_text('cockatrice'/'3ED', 'Flying\nAny non-wall creature blocking Cockatrice is destroyed, as is any creature blocked by Cockatrice. Creatures destroyed in this way deal their damage before dying.').
card_image_name('cockatrice'/'3ED', 'cockatrice').
card_uid('cockatrice'/'3ED', '3ED:Cockatrice:cockatrice').
card_rarity('cockatrice'/'3ED', 'Rare').
card_artist('cockatrice'/'3ED', 'Dan Frazier').
card_multiverse_id('cockatrice'/'3ED', '1238').

card_in_set('conservator', '3ED').
card_original_type('conservator'/'3ED', 'Artifact').
card_original_text('conservator'/'3ED', '{3}, {T}: Prevent the loss of up to 2 life.').
card_image_name('conservator'/'3ED', 'conservator').
card_uid('conservator'/'3ED', '3ED:Conservator:conservator').
card_rarity('conservator'/'3ED', 'Uncommon').
card_artist('conservator'/'3ED', 'Amy Weber').
card_multiverse_id('conservator'/'3ED', '1102').

card_in_set('contract from below', '3ED').
card_original_type('contract from below'/'3ED', 'Sorcery').
card_original_text('contract from below'/'3ED', 'Discard your current hand and draw eight new cards, adding the first drawn to your ante. Remove this card from your deck before playing if you are not playing for ante.').
card_image_name('contract from below'/'3ED', 'contract from below').
card_uid('contract from below'/'3ED', '3ED:Contract from Below:contract from below').
card_rarity('contract from below'/'3ED', 'Rare').
card_artist('contract from below'/'3ED', 'Douglas Shuler').
card_multiverse_id('contract from below'/'3ED', '1147').

card_in_set('control magic', '3ED').
card_original_type('control magic'/'3ED', 'Enchant Creature').
card_original_text('control magic'/'3ED', 'You control target creature until enchantment is discarded or game ends. If target creature is already tapped it stays tapped until you can untap it. If destroyed, target creature is put in its owner\'s graveyard.').
card_image_name('control magic'/'3ED', 'control magic').
card_uid('control magic'/'3ED', '3ED:Control Magic:control magic').
card_rarity('control magic'/'3ED', 'Uncommon').
card_artist('control magic'/'3ED', 'Dameon Willich').
card_multiverse_id('control magic'/'3ED', '1194').

card_in_set('conversion', '3ED').
card_original_type('conversion'/'3ED', 'Enchantment').
card_original_text('conversion'/'3ED', 'All mountains are considered basic plains while Conversion is in play. Pay {W}{W} during upkeep or Conversion is discarded.').
card_image_name('conversion'/'3ED', 'conversion').
card_uid('conversion'/'3ED', '3ED:Conversion:conversion').
card_rarity('conversion'/'3ED', 'Uncommon').
card_artist('conversion'/'3ED', 'Jesper Myrfors').
card_multiverse_id('conversion'/'3ED', '1340').

card_in_set('copy artifact', '3ED').
card_original_type('copy artifact'/'3ED', 'Enchantment').
card_original_text('copy artifact'/'3ED', 'Select any artifact in play. This enchantment acts as a duplicate of that artifact; it is affected by cards that affect either enchantments or artifacts. The copy remains even if the original artifact is destroyed. Enchantments on the original artifact are not copied.').
card_image_name('copy artifact'/'3ED', 'copy artifact').
card_uid('copy artifact'/'3ED', '3ED:Copy Artifact:copy artifact').
card_rarity('copy artifact'/'3ED', 'Rare').
card_artist('copy artifact'/'3ED', 'Amy Weber').
card_multiverse_id('copy artifact'/'3ED', '1195').

card_in_set('counterspell', '3ED').
card_original_type('counterspell'/'3ED', 'Interrupt').
card_original_text('counterspell'/'3ED', 'Counters target spell as it is being cast.').
card_image_name('counterspell'/'3ED', 'counterspell').
card_uid('counterspell'/'3ED', '3ED:Counterspell:counterspell').
card_rarity('counterspell'/'3ED', 'Uncommon').
card_artist('counterspell'/'3ED', 'Mark Poole').
card_multiverse_id('counterspell'/'3ED', '1196').

card_in_set('craw wurm', '3ED').
card_original_type('craw wurm'/'3ED', 'Summon — Wurm').
card_original_text('craw wurm'/'3ED', '').
card_image_name('craw wurm'/'3ED', 'craw wurm').
card_uid('craw wurm'/'3ED', '3ED:Craw Wurm:craw wurm').
card_rarity('craw wurm'/'3ED', 'Common').
card_artist('craw wurm'/'3ED', 'Daniel Gelon').
card_flavor_text('craw wurm'/'3ED', 'The most terrifying thing about the Craw Wurm is probably the horrible crashing sound it makes as it speeds through the forest. This noise is so loud it echoes through the trees and seems to come from all directions at once.').
card_multiverse_id('craw wurm'/'3ED', '1239').

card_in_set('creature bond', '3ED').
card_original_type('creature bond'/'3ED', 'Enchant Creature').
card_original_text('creature bond'/'3ED', 'If target creature is placed in the graveyard, Creature Bond does an amount of damage equal to creature\'s toughness to creature\'s controller.').
card_image_name('creature bond'/'3ED', 'creature bond').
card_uid('creature bond'/'3ED', '3ED:Creature Bond:creature bond').
card_rarity('creature bond'/'3ED', 'Common').
card_artist('creature bond'/'3ED', 'Anson Maddocks').
card_multiverse_id('creature bond'/'3ED', '1197').

card_in_set('crumble', '3ED').
card_original_type('crumble'/'3ED', 'Instant').
card_original_text('crumble'/'3ED', 'Buries target artifact. Artifact\'s controller gains life points equal to target artifact\'s casting cost.').
card_image_name('crumble'/'3ED', 'crumble').
card_uid('crumble'/'3ED', '3ED:Crumble:crumble').
card_rarity('crumble'/'3ED', 'Uncommon').
card_artist('crumble'/'3ED', 'Jesper Myrfors').
card_flavor_text('crumble'/'3ED', 'The spirits of Argoth grant new life to those who repent the folly of enslaving their labors to devices.').
card_multiverse_id('crumble'/'3ED', '1240').

card_in_set('crusade', '3ED').
card_original_type('crusade'/'3ED', 'Enchantment').
card_original_text('crusade'/'3ED', 'All white creatures gain +1/+1.').
card_image_name('crusade'/'3ED', 'crusade').
card_uid('crusade'/'3ED', '3ED:Crusade:crusade').
card_rarity('crusade'/'3ED', 'Rare').
card_artist('crusade'/'3ED', 'Mark Poole').
card_multiverse_id('crusade'/'3ED', '1341').

card_in_set('crystal rod', '3ED').
card_original_type('crystal rod'/'3ED', 'Artifact').
card_original_text('crystal rod'/'3ED', '{1}: Any blue spell cast gives you 1 life. Can only give 1 life each time a blue spell is cast.').
card_image_name('crystal rod'/'3ED', 'crystal rod').
card_uid('crystal rod'/'3ED', '3ED:Crystal Rod:crystal rod').
card_rarity('crystal rod'/'3ED', 'Uncommon').
card_artist('crystal rod'/'3ED', 'Amy Weber').
card_multiverse_id('crystal rod'/'3ED', '1103').

card_in_set('cursed land', '3ED').
card_original_type('cursed land'/'3ED', 'Enchant Land').
card_original_text('cursed land'/'3ED', 'Cursed Land does 1 damage to target land\'s controller during his or her upkeep.').
card_image_name('cursed land'/'3ED', 'cursed land').
card_uid('cursed land'/'3ED', '3ED:Cursed Land:cursed land').
card_rarity('cursed land'/'3ED', 'Uncommon').
card_artist('cursed land'/'3ED', 'Jesper Myrfors').
card_multiverse_id('cursed land'/'3ED', '1148').

card_in_set('dancing scimitar', '3ED').
card_original_type('dancing scimitar'/'3ED', 'Artifact Creature').
card_original_text('dancing scimitar'/'3ED', 'Flying').
card_image_name('dancing scimitar'/'3ED', 'dancing scimitar').
card_uid('dancing scimitar'/'3ED', '3ED:Dancing Scimitar:dancing scimitar').
card_rarity('dancing scimitar'/'3ED', 'Rare').
card_artist('dancing scimitar'/'3ED', 'Anson Maddocks').
card_flavor_text('dancing scimitar'/'3ED', 'Bobbing merrily from opponent to opponent, the scimitar began adding playful little flourishes to its strokes; it even turned a couple of somersaults.').
card_multiverse_id('dancing scimitar'/'3ED', '1104').

card_in_set('dark ritual', '3ED').
card_original_type('dark ritual'/'3ED', 'Interrupt').
card_original_text('dark ritual'/'3ED', 'Add {B}{B}{B} to your mana pool.').
card_image_name('dark ritual'/'3ED', 'dark ritual').
card_uid('dark ritual'/'3ED', '3ED:Dark Ritual:dark ritual').
card_rarity('dark ritual'/'3ED', 'Common').
card_artist('dark ritual'/'3ED', 'Sandra Everingham').
card_multiverse_id('dark ritual'/'3ED', '1149').

card_in_set('darkpact', '3ED').
card_original_type('darkpact'/'3ED', 'Sorcery').
card_original_text('darkpact'/'3ED', 'Swap top card of your library with either card of the ante; this swap is permanent. You must have a card in your library to cast this spell. Remove this card from your deck before playing if you are not playing for ante.').
card_image_name('darkpact'/'3ED', 'darkpact').
card_uid('darkpact'/'3ED', '3ED:Darkpact:darkpact').
card_rarity('darkpact'/'3ED', 'Rare').
card_artist('darkpact'/'3ED', 'Quinton Hoover').
card_multiverse_id('darkpact'/'3ED', '1150').

card_in_set('death ward', '3ED').
card_original_type('death ward'/'3ED', 'Instant').
card_original_text('death ward'/'3ED', 'Regenerates target creature.').
card_image_name('death ward'/'3ED', 'death ward').
card_uid('death ward'/'3ED', '3ED:Death Ward:death ward').
card_rarity('death ward'/'3ED', 'Common').
card_artist('death ward'/'3ED', 'Mark Poole').
card_multiverse_id('death ward'/'3ED', '1342').

card_in_set('deathgrip', '3ED').
card_original_type('deathgrip'/'3ED', 'Enchantment').
card_original_text('deathgrip'/'3ED', '{B}{B} Counter a green spell as it is being cast. This ability is played as an interrupt and does not affect green cards already in play.').
card_image_name('deathgrip'/'3ED', 'deathgrip').
card_uid('deathgrip'/'3ED', '3ED:Deathgrip:deathgrip').
card_rarity('deathgrip'/'3ED', 'Uncommon').
card_artist('deathgrip'/'3ED', 'Anson Maddocks').
card_multiverse_id('deathgrip'/'3ED', '1151').

card_in_set('deathlace', '3ED').
card_original_type('deathlace'/'3ED', 'Interrupt').
card_original_text('deathlace'/'3ED', 'Changes the color of one card either being played or already in play to black. Cost to cast, tap, maintain, or use a special ability of target card remains entirely unchanged.').
card_image_name('deathlace'/'3ED', 'deathlace').
card_uid('deathlace'/'3ED', '3ED:Deathlace:deathlace').
card_rarity('deathlace'/'3ED', 'Rare').
card_artist('deathlace'/'3ED', 'Sandra Everingham').
card_multiverse_id('deathlace'/'3ED', '1152').

card_in_set('demonic attorney', '3ED').
card_original_type('demonic attorney'/'3ED', 'Sorcery').
card_original_text('demonic attorney'/'3ED', 'If opponent doesn\'t concede the game immediately, each player must ante an additional card from the top of his or her library. Remove this card from your deck before playing if you are not playing for ante.').
card_image_name('demonic attorney'/'3ED', 'demonic attorney').
card_uid('demonic attorney'/'3ED', '3ED:Demonic Attorney:demonic attorney').
card_rarity('demonic attorney'/'3ED', 'Rare').
card_artist('demonic attorney'/'3ED', 'Daniel Gelon').
card_multiverse_id('demonic attorney'/'3ED', '1153').

card_in_set('demonic hordes', '3ED').
card_original_type('demonic hordes'/'3ED', 'Summon — Demons').
card_original_text('demonic hordes'/'3ED', '{T}: Destroy 1 land.\nPay {B}{B}{B} during your upkeep or the Hordes become tapped and you lose a land of opponent\'s choice.').
card_image_name('demonic hordes'/'3ED', 'demonic hordes').
card_uid('demonic hordes'/'3ED', '3ED:Demonic Hordes:demonic hordes').
card_rarity('demonic hordes'/'3ED', 'Rare').
card_artist('demonic hordes'/'3ED', 'Jesper Myrfors').
card_flavor_text('demonic hordes'/'3ED', 'Created to destroy Dominia, Demons can sometimes be bent to a more focused purpose.').
card_multiverse_id('demonic hordes'/'3ED', '1154').

card_in_set('demonic tutor', '3ED').
card_original_type('demonic tutor'/'3ED', 'Sorcery').
card_original_text('demonic tutor'/'3ED', 'Search your library for one card and take it into your hand. Reshuffle your library afterwards.').
card_image_name('demonic tutor'/'3ED', 'demonic tutor').
card_uid('demonic tutor'/'3ED', '3ED:Demonic Tutor:demonic tutor').
card_rarity('demonic tutor'/'3ED', 'Uncommon').
card_artist('demonic tutor'/'3ED', 'Douglas Shuler').
card_multiverse_id('demonic tutor'/'3ED', '1155').

card_in_set('desert twister', '3ED').
card_original_type('desert twister'/'3ED', 'Sorcery').
card_original_text('desert twister'/'3ED', 'Destroy any card in play.').
card_image_name('desert twister'/'3ED', 'desert twister').
card_uid('desert twister'/'3ED', '3ED:Desert Twister:desert twister').
card_rarity('desert twister'/'3ED', 'Uncommon').
card_artist('desert twister'/'3ED', 'Susan Van Camp').
card_multiverse_id('desert twister'/'3ED', '1241').

card_in_set('dingus egg', '3ED').
card_original_type('dingus egg'/'3ED', 'Artifact').
card_original_text('dingus egg'/'3ED', 'Whenever anyone loses a land, Dingus Egg does 2 damage to that player for each land lost.').
card_image_name('dingus egg'/'3ED', 'dingus egg').
card_uid('dingus egg'/'3ED', '3ED:Dingus Egg:dingus egg').
card_rarity('dingus egg'/'3ED', 'Rare').
card_artist('dingus egg'/'3ED', 'Dan Frazier').
card_multiverse_id('dingus egg'/'3ED', '1105').

card_in_set('disenchant', '3ED').
card_original_type('disenchant'/'3ED', 'Instant').
card_original_text('disenchant'/'3ED', 'Target enchantment or artifact is destroyed.').
card_image_name('disenchant'/'3ED', 'disenchant').
card_uid('disenchant'/'3ED', '3ED:Disenchant:disenchant').
card_rarity('disenchant'/'3ED', 'Common').
card_artist('disenchant'/'3ED', 'Amy Weber').
card_multiverse_id('disenchant'/'3ED', '1343').

card_in_set('disintegrate', '3ED').
card_original_type('disintegrate'/'3ED', 'Sorcery').
card_original_text('disintegrate'/'3ED', 'Disintegrate does X damage to one target. If target dies this turn, target is removed from the game entirely.').
card_image_name('disintegrate'/'3ED', 'disintegrate').
card_uid('disintegrate'/'3ED', '3ED:Disintegrate:disintegrate').
card_rarity('disintegrate'/'3ED', 'Common').
card_artist('disintegrate'/'3ED', 'Anson Maddocks').
card_multiverse_id('disintegrate'/'3ED', '1283').

card_in_set('disrupting scepter', '3ED').
card_original_type('disrupting scepter'/'3ED', 'Artifact').
card_original_text('disrupting scepter'/'3ED', '{3}, {T}: Opponent must discard one card of his or her choice. Can only be used during controller\'s turn.').
card_image_name('disrupting scepter'/'3ED', 'disrupting scepter').
card_uid('disrupting scepter'/'3ED', '3ED:Disrupting Scepter:disrupting scepter').
card_rarity('disrupting scepter'/'3ED', 'Rare').
card_artist('disrupting scepter'/'3ED', 'Dan Frazier').
card_multiverse_id('disrupting scepter'/'3ED', '1106').

card_in_set('dragon engine', '3ED').
card_original_type('dragon engine'/'3ED', 'Artifact Creature').
card_original_text('dragon engine'/'3ED', '{2}: +1/+0').
card_image_name('dragon engine'/'3ED', 'dragon engine').
card_uid('dragon engine'/'3ED', '3ED:Dragon Engine:dragon engine').
card_rarity('dragon engine'/'3ED', 'Rare').
card_artist('dragon engine'/'3ED', 'Anson Maddocks').
card_flavor_text('dragon engine'/'3ED', 'Those who believed the city of Kroog would never fall to Mishra\'s forces severely underestimated the might of his war machines.').
card_multiverse_id('dragon engine'/'3ED', '1107').

card_in_set('dragon whelp', '3ED').
card_original_type('dragon whelp'/'3ED', 'Summon — Dragon').
card_original_text('dragon whelp'/'3ED', 'Flying\n{R} +1/+0; if more than {R}{R}{R} is spent in this way during one turn, Dragon Whelp is killed at end of turn.').
card_image_name('dragon whelp'/'3ED', 'dragon whelp').
card_uid('dragon whelp'/'3ED', '3ED:Dragon Whelp:dragon whelp').
card_rarity('dragon whelp'/'3ED', 'Uncommon').
card_artist('dragon whelp'/'3ED', 'Amy Weber').
card_flavor_text('dragon whelp'/'3ED', '\"O to be a dragon . . . of silkworm size or immense . . .\"\n—Marianne Moore, \"O to Be a Dragon\"').
card_multiverse_id('dragon whelp'/'3ED', '1284').

card_in_set('drain life', '3ED').
card_original_type('drain life'/'3ED', 'Sorcery').
card_original_text('drain life'/'3ED', 'Drain Life does 1 damage to a single target for each {B} spent in addition to the casting cost. Caster gains 1 life for each damage inflicted. If you drain life from a creature, you cannot gain more life than the creature\'s current toughness.').
card_image_name('drain life'/'3ED', 'drain life').
card_uid('drain life'/'3ED', '3ED:Drain Life:drain life').
card_rarity('drain life'/'3ED', 'Common').
card_artist('drain life'/'3ED', 'Douglas Shuler').
card_multiverse_id('drain life'/'3ED', '1156').

card_in_set('drain power', '3ED').
card_original_type('drain power'/'3ED', 'Sorcery').
card_original_text('drain power'/'3ED', 'Opponent must draw all mana from his or her available lands; this mana and all mana in opponent\'s mana pool drains into your mana pool. You can\'t take less than all your opponent\'s mana.').
card_image_name('drain power'/'3ED', 'drain power').
card_uid('drain power'/'3ED', '3ED:Drain Power:drain power').
card_rarity('drain power'/'3ED', 'Rare').
card_artist('drain power'/'3ED', 'Douglas Shuler').
card_multiverse_id('drain power'/'3ED', '1198').

card_in_set('drudge skeletons', '3ED').
card_original_type('drudge skeletons'/'3ED', 'Summon — Skeletons').
card_original_text('drudge skeletons'/'3ED', '{B} Regenerates.').
card_image_name('drudge skeletons'/'3ED', 'drudge skeletons').
card_uid('drudge skeletons'/'3ED', '3ED:Drudge Skeletons:drudge skeletons').
card_rarity('drudge skeletons'/'3ED', 'Common').
card_artist('drudge skeletons'/'3ED', 'Sandra Everingham').
card_flavor_text('drudge skeletons'/'3ED', 'Bones scattered around us joined to form misshapen bodies. We struck at them repeatedly—they fell, but soon formed again, with the same mocking look on their faceless skulls.').
card_multiverse_id('drudge skeletons'/'3ED', '1157').

card_in_set('dwarven warriors', '3ED').
card_original_type('dwarven warriors'/'3ED', 'Summon — Dwarves').
card_original_text('dwarven warriors'/'3ED', '{T}: Make a creature of power no greater than 2 unblockable until end of turn. Other cards may later be used to increase target creature\'s power beyond 2.').
card_image_name('dwarven warriors'/'3ED', 'dwarven warriors').
card_uid('dwarven warriors'/'3ED', '3ED:Dwarven Warriors:dwarven warriors').
card_rarity('dwarven warriors'/'3ED', 'Common').
card_artist('dwarven warriors'/'3ED', 'Douglas Shuler').
card_multiverse_id('dwarven warriors'/'3ED', '1285').

card_in_set('dwarven weaponsmith', '3ED').
card_original_type('dwarven weaponsmith'/'3ED', 'Summon — Dwarves').
card_original_text('dwarven weaponsmith'/'3ED', '{T}: During your upkeep, add a permanent +1/+1 counter to any creature. Each time you use this ability, you must sacrifice one of your artifacts in play.').
card_image_name('dwarven weaponsmith'/'3ED', 'dwarven weaponsmith').
card_uid('dwarven weaponsmith'/'3ED', '3ED:Dwarven Weaponsmith:dwarven weaponsmith').
card_rarity('dwarven weaponsmith'/'3ED', 'Uncommon').
card_artist('dwarven weaponsmith'/'3ED', 'Mark Poole').
card_flavor_text('dwarven weaponsmith'/'3ED', '\"Work with zeal as hammers peal! Melt, anneal, and pound the steel!\"\n—Old Dwarvish forge-chant').
card_multiverse_id('dwarven weaponsmith'/'3ED', '1286').

card_in_set('earth elemental', '3ED').
card_original_type('earth elemental'/'3ED', 'Summon — Elemental').
card_original_text('earth elemental'/'3ED', '').
card_image_name('earth elemental'/'3ED', 'earth elemental').
card_uid('earth elemental'/'3ED', '3ED:Earth Elemental:earth elemental').
card_rarity('earth elemental'/'3ED', 'Uncommon').
card_artist('earth elemental'/'3ED', 'Dan Frazier').
card_flavor_text('earth elemental'/'3ED', 'Earth Elementals have the eternal strength of stone and the endurance of mountains. Primordially connected to the land they inhabit, they take a long-term view of things, scorning the impetuous haste of short-lived mortal creatures.').
card_multiverse_id('earth elemental'/'3ED', '1287').

card_in_set('earthbind', '3ED').
card_original_type('earthbind'/'3ED', 'Enchant Creature').
card_original_text('earthbind'/'3ED', 'If cast on a flying creature, Earthbind removes flying ability and does 2 damage to target creature; this damage occurs only once, at the time Earthbind is cast. If another spell or effect later gives target creature flying ability, Earthbind does not affect this. Earthbind has no effect on non-flying creatures.').
card_image_name('earthbind'/'3ED', 'earthbind').
card_uid('earthbind'/'3ED', '3ED:Earthbind:earthbind').
card_rarity('earthbind'/'3ED', 'Common').
card_artist('earthbind'/'3ED', 'Quinton Hoover').
card_multiverse_id('earthbind'/'3ED', '1288').

card_in_set('earthquake', '3ED').
card_original_type('earthquake'/'3ED', 'Sorcery').
card_original_text('earthquake'/'3ED', 'Does X damage to each player and each non-flying creature in play.').
card_image_name('earthquake'/'3ED', 'earthquake').
card_uid('earthquake'/'3ED', '3ED:Earthquake:earthquake').
card_rarity('earthquake'/'3ED', 'Rare').
card_artist('earthquake'/'3ED', 'Dan Frazier').
card_multiverse_id('earthquake'/'3ED', '1289').

card_in_set('ebony horse', '3ED').
card_original_type('ebony horse'/'3ED', 'Artifact').
card_original_text('ebony horse'/'3ED', '{2}, {T}: Remove one of your attacking creatures from combat. Treat this as if the creature never attacked, except that defenders assigned to block it cannot choose to block another creature.').
card_image_name('ebony horse'/'3ED', 'ebony horse').
card_uid('ebony horse'/'3ED', '3ED:Ebony Horse:ebony horse').
card_rarity('ebony horse'/'3ED', 'Rare').
card_artist('ebony horse'/'3ED', 'Dameon Willich').
card_multiverse_id('ebony horse'/'3ED', '1108').

card_in_set('el-hajjâj', '3ED').
card_original_type('el-hajjâj'/'3ED', 'Summon — El-Hajjâj').
card_original_text('el-hajjâj'/'3ED', 'You gain 1 life for every point of damage El-Hajjâj inflicts.').
card_image_name('el-hajjâj'/'3ED', 'el-hajjaj').
card_uid('el-hajjâj'/'3ED', '3ED:El-Hajjâj:el-hajjaj').
card_rarity('el-hajjâj'/'3ED', 'Rare').
card_artist('el-hajjâj'/'3ED', 'Dameon Willich').
card_multiverse_id('el-hajjâj'/'3ED', '1158').

card_in_set('elvish archers', '3ED').
card_original_type('elvish archers'/'3ED', 'Summon — Elves').
card_original_text('elvish archers'/'3ED', 'First strike').
card_image_name('elvish archers'/'3ED', 'elvish archers').
card_uid('elvish archers'/'3ED', '3ED:Elvish Archers:elvish archers').
card_rarity('elvish archers'/'3ED', 'Rare').
card_artist('elvish archers'/'3ED', 'Anson Maddocks').
card_flavor_text('elvish archers'/'3ED', 'I tell you, there was so many arrows flying about you couldn\'t hardly see the sun. So I says to young Angus, \"Well, at least now we\'re fighting in the shade!\"').
card_multiverse_id('elvish archers'/'3ED', '1242').

card_in_set('energy flux', '3ED').
card_original_type('energy flux'/'3ED', 'Enchantment').
card_original_text('energy flux'/'3ED', 'All artifacts in play now require an upkeep cost of {2} in addition to any other upkeep costs they may have. If the upkeep cost for an artifact is not paid, the artifact must be discarded.').
card_image_name('energy flux'/'3ED', 'energy flux').
card_uid('energy flux'/'3ED', '3ED:Energy Flux:energy flux').
card_rarity('energy flux'/'3ED', 'Uncommon').
card_artist('energy flux'/'3ED', 'Kaja Foglio').
card_multiverse_id('energy flux'/'3ED', '1199').

card_in_set('erg raiders', '3ED').
card_original_type('erg raiders'/'3ED', 'Summon — Raiders').
card_original_text('erg raiders'/'3ED', 'If you do not attack with Raiders, they do 2 damage to you at end of turn. Raiders do no damage to you during the turn in which they are summoned.').
card_image_name('erg raiders'/'3ED', 'erg raiders').
card_uid('erg raiders'/'3ED', '3ED:Erg Raiders:erg raiders').
card_rarity('erg raiders'/'3ED', 'Common').
card_artist('erg raiders'/'3ED', 'Dameon Willich').
card_multiverse_id('erg raiders'/'3ED', '1159').

card_in_set('evil presence', '3ED').
card_original_type('evil presence'/'3ED', 'Enchant Land').
card_original_text('evil presence'/'3ED', 'Target land is now a basic swamp.').
card_image_name('evil presence'/'3ED', 'evil presence').
card_uid('evil presence'/'3ED', '3ED:Evil Presence:evil presence').
card_rarity('evil presence'/'3ED', 'Uncommon').
card_artist('evil presence'/'3ED', 'Sandra Everingham').
card_multiverse_id('evil presence'/'3ED', '1160').

card_in_set('eye for an eye', '3ED').
card_original_type('eye for an eye'/'3ED', 'Instant').
card_original_text('eye for an eye'/'3ED', 'Can be cast only when a creature, spell, or effect does damage to you. Eye for an Eye does an equal amount of damage to the controller of that creature, spell, or effect. If some spell or effect reduces the amount of damage you receive, it does not reduce the damage dealt by Eye for an Eye.').
card_image_name('eye for an eye'/'3ED', 'eye for an eye').
card_uid('eye for an eye'/'3ED', '3ED:Eye for an Eye:eye for an eye').
card_rarity('eye for an eye'/'3ED', 'Rare').
card_artist('eye for an eye'/'3ED', 'Mark Poole').
card_multiverse_id('eye for an eye'/'3ED', '1344').

card_in_set('farmstead', '3ED').
card_original_type('farmstead'/'3ED', 'Enchant Land').
card_original_text('farmstead'/'3ED', 'Target land\'s controller gains 1 life if {W}{W} is spent during controller\'s upkeep. You cannot gain more than 1 life each turn through this enchantment.').
card_image_name('farmstead'/'3ED', 'farmstead').
card_uid('farmstead'/'3ED', '3ED:Farmstead:farmstead').
card_rarity('farmstead'/'3ED', 'Rare').
card_artist('farmstead'/'3ED', 'Mark Poole').
card_multiverse_id('farmstead'/'3ED', '1345').

card_in_set('fastbond', '3ED').
card_original_type('fastbond'/'3ED', 'Enchantment').
card_original_text('fastbond'/'3ED', 'You may put as many lands into play as you want each turn. Fastbond does 1 damage to you for every land beyond the first that you play in a single turn.').
card_image_name('fastbond'/'3ED', 'fastbond').
card_uid('fastbond'/'3ED', '3ED:Fastbond:fastbond').
card_rarity('fastbond'/'3ED', 'Rare').
card_artist('fastbond'/'3ED', 'Mark Poole').
card_multiverse_id('fastbond'/'3ED', '1243').

card_in_set('fear', '3ED').
card_original_type('fear'/'3ED', 'Enchant Creature').
card_original_text('fear'/'3ED', 'Target creature cannot be blocked by any creatures except black creatures and artifact creatures.').
card_image_name('fear'/'3ED', 'fear').
card_uid('fear'/'3ED', '3ED:Fear:fear').
card_rarity('fear'/'3ED', 'Common').
card_artist('fear'/'3ED', 'Mark Poole').
card_multiverse_id('fear'/'3ED', '1161').

card_in_set('feedback', '3ED').
card_original_type('feedback'/'3ED', 'Enchant Enchantment').
card_original_text('feedback'/'3ED', 'Feedback does 1 damage to controller of target enchantment during its controller\'s upkeep.').
card_image_name('feedback'/'3ED', 'feedback').
card_uid('feedback'/'3ED', '3ED:Feedback:feedback').
card_rarity('feedback'/'3ED', 'Uncommon').
card_artist('feedback'/'3ED', 'Quinton Hoover').
card_multiverse_id('feedback'/'3ED', '1200').

card_in_set('fire elemental', '3ED').
card_original_type('fire elemental'/'3ED', 'Summon — Elemental').
card_original_text('fire elemental'/'3ED', '').
card_image_name('fire elemental'/'3ED', 'fire elemental').
card_uid('fire elemental'/'3ED', '3ED:Fire Elemental:fire elemental').
card_rarity('fire elemental'/'3ED', 'Uncommon').
card_artist('fire elemental'/'3ED', 'Melissa A. Benson').
card_flavor_text('fire elemental'/'3ED', 'Fire Elementals are ruthless infernos, annihilating and consuming their foes in a frenzied holocaust. Crackling and blazing, they sear swift, terrible paths, leaving the land charred and scorched in their wake.').
card_multiverse_id('fire elemental'/'3ED', '1290').

card_in_set('fireball', '3ED').
card_original_type('fireball'/'3ED', 'Sorcery').
card_original_text('fireball'/'3ED', 'Fireball does X damage total, divided evenly (round down) among any number of targets. Pay 1 extra mana for each target beyond the first.').
card_image_name('fireball'/'3ED', 'fireball').
card_uid('fireball'/'3ED', '3ED:Fireball:fireball').
card_rarity('fireball'/'3ED', 'Common').
card_artist('fireball'/'3ED', 'Mark Tedin').
card_multiverse_id('fireball'/'3ED', '1291').

card_in_set('firebreathing', '3ED').
card_original_type('firebreathing'/'3ED', 'Enchant Creature').
card_original_text('firebreathing'/'3ED', '{R} +1/+0').
card_image_name('firebreathing'/'3ED', 'firebreathing').
card_uid('firebreathing'/'3ED', '3ED:Firebreathing:firebreathing').
card_rarity('firebreathing'/'3ED', 'Common').
card_artist('firebreathing'/'3ED', 'Dan Frazier').
card_flavor_text('firebreathing'/'3ED', '\"And topples round the dreary west\nA looming bastion fringed with fire.\"\n—Alfred, Lord Tennyson, \"In Memoriam\"').
card_multiverse_id('firebreathing'/'3ED', '1292').

card_in_set('flashfires', '3ED').
card_original_type('flashfires'/'3ED', 'Sorcery').
card_original_text('flashfires'/'3ED', 'All plains in play are destroyed.').
card_image_name('flashfires'/'3ED', 'flashfires').
card_uid('flashfires'/'3ED', '3ED:Flashfires:flashfires').
card_rarity('flashfires'/'3ED', 'Uncommon').
card_artist('flashfires'/'3ED', 'Dameon Willich').
card_multiverse_id('flashfires'/'3ED', '1293').

card_in_set('flight', '3ED').
card_original_type('flight'/'3ED', 'Enchant Creature').
card_original_text('flight'/'3ED', 'Target creature is now a flying creature.').
card_image_name('flight'/'3ED', 'flight').
card_uid('flight'/'3ED', '3ED:Flight:flight').
card_rarity('flight'/'3ED', 'Common').
card_artist('flight'/'3ED', 'Anson Maddocks').
card_multiverse_id('flight'/'3ED', '1201').

card_in_set('flying carpet', '3ED').
card_original_type('flying carpet'/'3ED', 'Artifact').
card_original_text('flying carpet'/'3ED', '{2}, {T}: Gives one creature flying ability until end of turn. If that creature is destroyed before end of turn, so is Flying Carpet.').
card_image_name('flying carpet'/'3ED', 'flying carpet').
card_uid('flying carpet'/'3ED', '3ED:Flying Carpet:flying carpet').
card_rarity('flying carpet'/'3ED', 'Rare').
card_artist('flying carpet'/'3ED', 'Mark Tedin').
card_multiverse_id('flying carpet'/'3ED', '1109').

card_in_set('fog', '3ED').
card_original_type('fog'/'3ED', 'Instant').
card_original_text('fog'/'3ED', 'Creatures attack and block as normal, but none deal any damage or otherwise affect any creature as a result of an attack or block. All attacking creatures are still tapped. Play any time before attack damage is dealt.').
card_image_name('fog'/'3ED', 'fog').
card_uid('fog'/'3ED', '3ED:Fog:fog').
card_rarity('fog'/'3ED', 'Common').
card_artist('fog'/'3ED', 'Jesper Myrfors').
card_multiverse_id('fog'/'3ED', '1244').

card_in_set('force of nature', '3ED').
card_original_type('force of nature'/'3ED', 'Summon — Force').
card_original_text('force of nature'/'3ED', 'Trample\nYou must pay {G}{G}{G}{G} during your upkeep or Force of Nature does 8 damage to you. You may still attack with Force of Nature even if you failed to pay the upkeep.').
card_image_name('force of nature'/'3ED', 'force of nature').
card_uid('force of nature'/'3ED', '3ED:Force of Nature:force of nature').
card_rarity('force of nature'/'3ED', 'Rare').
card_artist('force of nature'/'3ED', 'Douglas Shuler').
card_multiverse_id('force of nature'/'3ED', '1245').

card_in_set('forest', '3ED').
card_original_type('forest'/'3ED', 'Land').
card_original_text('forest'/'3ED', '{T}: Add {G} to your mana pool.').
card_image_name('forest'/'3ED', 'forest1').
card_uid('forest'/'3ED', '3ED:Forest:forest1').
card_rarity('forest'/'3ED', 'Basic Land').
card_artist('forest'/'3ED', 'Christopher Rush').
card_multiverse_id('forest'/'3ED', '1388').

card_in_set('forest', '3ED').
card_original_type('forest'/'3ED', 'Land').
card_original_text('forest'/'3ED', '{T}: Add {G} to your mana pool.').
card_image_name('forest'/'3ED', 'forest2').
card_uid('forest'/'3ED', '3ED:Forest:forest2').
card_rarity('forest'/'3ED', 'Basic Land').
card_artist('forest'/'3ED', 'Christopher Rush').
card_multiverse_id('forest'/'3ED', '1387').

card_in_set('forest', '3ED').
card_original_type('forest'/'3ED', 'Land').
card_original_text('forest'/'3ED', '{T}: Add {G} to your mana pool.').
card_image_name('forest'/'3ED', 'forest3').
card_uid('forest'/'3ED', '3ED:Forest:forest3').
card_rarity('forest'/'3ED', 'Basic Land').
card_artist('forest'/'3ED', 'Christopher Rush').
card_multiverse_id('forest'/'3ED', '1386').

card_in_set('fork', '3ED').
card_original_type('fork'/'3ED', 'Interrupt').
card_original_text('fork'/'3ED', 'Any one sorcery or instant spell just cast is duplicated. Treat Fork as an exact copy of target spell except that Fork remains red. Caster of Fork chooses the copy\'s target.').
card_image_name('fork'/'3ED', 'fork').
card_uid('fork'/'3ED', '3ED:Fork:fork').
card_rarity('fork'/'3ED', 'Rare').
card_artist('fork'/'3ED', 'Amy Weber').
card_multiverse_id('fork'/'3ED', '1294').

card_in_set('frozen shade', '3ED').
card_original_type('frozen shade'/'3ED', 'Summon — Shade').
card_original_text('frozen shade'/'3ED', '{B} +1/+1').
card_image_name('frozen shade'/'3ED', 'frozen shade').
card_uid('frozen shade'/'3ED', '3ED:Frozen Shade:frozen shade').
card_rarity('frozen shade'/'3ED', 'Common').
card_artist('frozen shade'/'3ED', 'Douglas Shuler').
card_flavor_text('frozen shade'/'3ED', '\"There are some qualities—some incorporate things,/ That have a double life, which thus is made/ A type of twin entity which springs/ From matter and light, evinced in solid and shade.\"\n—Edgar Allan Poe, \"Silence\"').
card_multiverse_id('frozen shade'/'3ED', '1162').

card_in_set('fungusaur', '3ED').
card_original_type('fungusaur'/'3ED', 'Summon — Fungusaur').
card_original_text('fungusaur'/'3ED', 'At the end of any turn during which Fungusaur was damaged but not destroyed, put a +1/+1 counter on it.').
card_image_name('fungusaur'/'3ED', 'fungusaur').
card_uid('fungusaur'/'3ED', '3ED:Fungusaur:fungusaur').
card_rarity('fungusaur'/'3ED', 'Rare').
card_artist('fungusaur'/'3ED', 'Daniel Gelon').
card_flavor_text('fungusaur'/'3ED', 'Rather than sheltering her young, the female Fungusaur often injures her own offspring, thereby ensuring their rapid growth.').
card_multiverse_id('fungusaur'/'3ED', '1246').

card_in_set('gaea\'s liege', '3ED').
card_original_type('gaea\'s liege'/'3ED', 'Summon — Gaea\'s Liege').
card_original_text('gaea\'s liege'/'3ED', '{T}: Turn any one land into a basic forest. Mark changed lands with counters, removing the counters when Gaea\'s Liege leaves play. Gaea\'s Liege has power and toughness equal to the number of forests controller has in play; when it\'s attacking, they are equal to the number of forests defending player has in play.').
card_image_name('gaea\'s liege'/'3ED', 'gaea\'s liege').
card_uid('gaea\'s liege'/'3ED', '3ED:Gaea\'s Liege:gaea\'s liege').
card_rarity('gaea\'s liege'/'3ED', 'Rare').
card_artist('gaea\'s liege'/'3ED', 'Dameon Willich').
card_multiverse_id('gaea\'s liege'/'3ED', '1247').

card_in_set('giant growth', '3ED').
card_original_type('giant growth'/'3ED', 'Instant').
card_original_text('giant growth'/'3ED', 'Target creature gains +3/+3 until end of turn.').
card_image_name('giant growth'/'3ED', 'giant growth').
card_uid('giant growth'/'3ED', '3ED:Giant Growth:giant growth').
card_rarity('giant growth'/'3ED', 'Common').
card_artist('giant growth'/'3ED', 'Sandra Everingham').
card_multiverse_id('giant growth'/'3ED', '1248').

card_in_set('giant spider', '3ED').
card_original_type('giant spider'/'3ED', 'Summon — Spider').
card_original_text('giant spider'/'3ED', 'Does not fly, but can block flying creatures.').
card_image_name('giant spider'/'3ED', 'giant spider').
card_uid('giant spider'/'3ED', '3ED:Giant Spider:giant spider').
card_rarity('giant spider'/'3ED', 'Common').
card_artist('giant spider'/'3ED', 'Sandra Everingham').
card_flavor_text('giant spider'/'3ED', 'While it possesses potent venom, the Giant Spider often chooses not to paralyze its victims. Perhaps the creature enjoys the gentle rocking motion caused by its captives\' struggles to escape its web.').
card_multiverse_id('giant spider'/'3ED', '1249').

card_in_set('glasses of urza', '3ED').
card_original_type('glasses of urza'/'3ED', 'Artifact').
card_original_text('glasses of urza'/'3ED', '{T}: You may look at opponent\'s hand.').
card_image_name('glasses of urza'/'3ED', 'glasses of urza').
card_uid('glasses of urza'/'3ED', '3ED:Glasses of Urza:glasses of urza').
card_rarity('glasses of urza'/'3ED', 'Uncommon').
card_artist('glasses of urza'/'3ED', 'Douglas Shuler').
card_multiverse_id('glasses of urza'/'3ED', '1110').

card_in_set('gloom', '3ED').
card_original_type('gloom'/'3ED', 'Enchantment').
card_original_text('gloom'/'3ED', 'White spells cost 3 more mana to cast. White enchantments with activation costs require 3 more mana to use.').
card_image_name('gloom'/'3ED', 'gloom').
card_uid('gloom'/'3ED', '3ED:Gloom:gloom').
card_rarity('gloom'/'3ED', 'Uncommon').
card_artist('gloom'/'3ED', 'Dan Frazier').
card_multiverse_id('gloom'/'3ED', '1163').

card_in_set('goblin balloon brigade', '3ED').
card_original_type('goblin balloon brigade'/'3ED', 'Summon — Goblins').
card_original_text('goblin balloon brigade'/'3ED', '{R} Gains flying ability until end of turn.').
card_image_name('goblin balloon brigade'/'3ED', 'goblin balloon brigade').
card_uid('goblin balloon brigade'/'3ED', '3ED:Goblin Balloon Brigade:goblin balloon brigade').
card_rarity('goblin balloon brigade'/'3ED', 'Uncommon').
card_artist('goblin balloon brigade'/'3ED', 'Andi Rusu').
card_flavor_text('goblin balloon brigade'/'3ED', '\"From up here we can drop rocks and arrows and more rocks!\" \"Uh, yeah boss, but how do we get down?\"').
card_multiverse_id('goblin balloon brigade'/'3ED', '1295').

card_in_set('goblin king', '3ED').
card_original_type('goblin king'/'3ED', 'Summon — Lord').
card_original_text('goblin king'/'3ED', 'All Goblins in play gain mountainwalk and +1/+1 while this card remains in play.').
card_image_name('goblin king'/'3ED', 'goblin king').
card_uid('goblin king'/'3ED', '3ED:Goblin King:goblin king').
card_rarity('goblin king'/'3ED', 'Rare').
card_artist('goblin king'/'3ED', 'Jesper Myrfors').
card_flavor_text('goblin king'/'3ED', 'To become king of the Goblins, one must assassinate the previous king. Thus, only the most foolish seek positions of leadership.').
card_multiverse_id('goblin king'/'3ED', '1296').

card_in_set('granite gargoyle', '3ED').
card_original_type('granite gargoyle'/'3ED', 'Summon — Gargoyle').
card_original_text('granite gargoyle'/'3ED', 'Flying; {R} +0/+1').
card_image_name('granite gargoyle'/'3ED', 'granite gargoyle').
card_uid('granite gargoyle'/'3ED', '3ED:Granite Gargoyle:granite gargoyle').
card_rarity('granite gargoyle'/'3ED', 'Rare').
card_artist('granite gargoyle'/'3ED', 'Christopher Rush').
card_flavor_text('granite gargoyle'/'3ED', '\"While most overworlders fortunately don\'t realize this, Gargoyles can be most delicious, providing you have the appropriate tools to carve them.\"\n—The Underworld Cookbook, by Asmoranomardicadaistinaculdacar').
card_multiverse_id('granite gargoyle'/'3ED', '1297').

card_in_set('gray ogre', '3ED').
card_original_type('gray ogre'/'3ED', 'Summon — Ogre').
card_original_text('gray ogre'/'3ED', '').
card_image_name('gray ogre'/'3ED', 'gray ogre').
card_uid('gray ogre'/'3ED', '3ED:Gray Ogre:gray ogre').
card_rarity('gray ogre'/'3ED', 'Common').
card_artist('gray ogre'/'3ED', 'Dan Frazier').
card_flavor_text('gray ogre'/'3ED', 'The Ogre philosopher Gnerdel believed the purpose of life was to live as high on the food chain as possible. She refused to eat vegetarians, and preferred to live entirely on creatures that preyed on sentient beings.').
card_multiverse_id('gray ogre'/'3ED', '1298').

card_in_set('green ward', '3ED').
card_original_type('green ward'/'3ED', 'Enchant Creature').
card_original_text('green ward'/'3ED', 'Target creature gains protection from green.').
card_image_name('green ward'/'3ED', 'green ward').
card_uid('green ward'/'3ED', '3ED:Green Ward:green ward').
card_rarity('green ward'/'3ED', 'Uncommon').
card_artist('green ward'/'3ED', 'Dan Frazier').
card_multiverse_id('green ward'/'3ED', '1346').

card_in_set('grizzly bears', '3ED').
card_original_type('grizzly bears'/'3ED', 'Summon — Bears').
card_original_text('grizzly bears'/'3ED', '').
card_image_name('grizzly bears'/'3ED', 'grizzly bears').
card_uid('grizzly bears'/'3ED', '3ED:Grizzly Bears:grizzly bears').
card_rarity('grizzly bears'/'3ED', 'Common').
card_artist('grizzly bears'/'3ED', 'Jeff A. Menges').
card_flavor_text('grizzly bears'/'3ED', 'Don\'t try to outrun one of Dominia\'s Grizzlies; it\'ll catch you, knock you down, and eat you. Of course, you could run up a tree. In that case you\'ll get a nice view before it knocks the tree down and eats you.').
card_multiverse_id('grizzly bears'/'3ED', '1250').

card_in_set('guardian angel', '3ED').
card_original_type('guardian angel'/'3ED', 'Instant').
card_original_text('guardian angel'/'3ED', 'Prevents X damage from being dealt to any one target. Any further damage to the same target this turn can be canceled by spending 1 mana per point of damage to be canceled.').
card_image_name('guardian angel'/'3ED', 'guardian angel').
card_uid('guardian angel'/'3ED', '3ED:Guardian Angel:guardian angel').
card_rarity('guardian angel'/'3ED', 'Common').
card_artist('guardian angel'/'3ED', 'Anson Maddocks').
card_multiverse_id('guardian angel'/'3ED', '1347').

card_in_set('healing salve', '3ED').
card_original_type('healing salve'/'3ED', 'Instant').
card_original_text('healing salve'/'3ED', 'Gain 3 life, or prevent up to 3 damage from being dealt to a single target.').
card_image_name('healing salve'/'3ED', 'healing salve').
card_uid('healing salve'/'3ED', '3ED:Healing Salve:healing salve').
card_rarity('healing salve'/'3ED', 'Common').
card_artist('healing salve'/'3ED', 'Dan Frazier').
card_multiverse_id('healing salve'/'3ED', '1348').

card_in_set('helm of chatzuk', '3ED').
card_original_type('helm of chatzuk'/'3ED', 'Artifact').
card_original_text('helm of chatzuk'/'3ED', '{1}, {T}: You may give one creature banding ability until end of turn.').
card_image_name('helm of chatzuk'/'3ED', 'helm of chatzuk').
card_uid('helm of chatzuk'/'3ED', '3ED:Helm of Chatzuk:helm of chatzuk').
card_rarity('helm of chatzuk'/'3ED', 'Rare').
card_artist('helm of chatzuk'/'3ED', 'Mark Tedin').
card_multiverse_id('helm of chatzuk'/'3ED', '1111').

card_in_set('hill giant', '3ED').
card_original_type('hill giant'/'3ED', 'Summon — Giant').
card_original_text('hill giant'/'3ED', '').
card_image_name('hill giant'/'3ED', 'hill giant').
card_uid('hill giant'/'3ED', '3ED:Hill Giant:hill giant').
card_rarity('hill giant'/'3ED', 'Common').
card_artist('hill giant'/'3ED', 'Dan Frazier').
card_flavor_text('hill giant'/'3ED', 'Fortunately, Hill Giants have large blind spots in which a human can easily hide. Unfortunately, these blind spots are beneath the bottoms of their feet.').
card_multiverse_id('hill giant'/'3ED', '1299').

card_in_set('holy armor', '3ED').
card_original_type('holy armor'/'3ED', 'Enchant Creature').
card_original_text('holy armor'/'3ED', 'Target creature gains\n+0/+2. {W} +0/+1').
card_image_name('holy armor'/'3ED', 'holy armor').
card_uid('holy armor'/'3ED', '3ED:Holy Armor:holy armor').
card_rarity('holy armor'/'3ED', 'Common').
card_artist('holy armor'/'3ED', 'Melissa A. Benson').
card_multiverse_id('holy armor'/'3ED', '1349').

card_in_set('holy strength', '3ED').
card_original_type('holy strength'/'3ED', 'Enchant Creature').
card_original_text('holy strength'/'3ED', 'Target creature gains +1/+2.').
card_image_name('holy strength'/'3ED', 'holy strength').
card_uid('holy strength'/'3ED', '3ED:Holy Strength:holy strength').
card_rarity('holy strength'/'3ED', 'Common').
card_artist('holy strength'/'3ED', 'Anson Maddocks').
card_multiverse_id('holy strength'/'3ED', '1350').

card_in_set('howl from beyond', '3ED').
card_original_type('howl from beyond'/'3ED', 'Instant').
card_original_text('howl from beyond'/'3ED', 'Target creature gains +X/+0 until end of turn.').
card_image_name('howl from beyond'/'3ED', 'howl from beyond').
card_uid('howl from beyond'/'3ED', '3ED:Howl from Beyond:howl from beyond').
card_rarity('howl from beyond'/'3ED', 'Common').
card_artist('howl from beyond'/'3ED', 'Mark Poole').
card_multiverse_id('howl from beyond'/'3ED', '1164').

card_in_set('howling mine', '3ED').
card_original_type('howling mine'/'3ED', 'Artifact').
card_original_text('howling mine'/'3ED', 'Each player must draw one extra card during the draw phase of each of his or her turns.').
card_image_name('howling mine'/'3ED', 'howling mine').
card_uid('howling mine'/'3ED', '3ED:Howling Mine:howling mine').
card_rarity('howling mine'/'3ED', 'Rare').
card_artist('howling mine'/'3ED', 'Mark Poole').
card_multiverse_id('howling mine'/'3ED', '1112').

card_in_set('hurkyl\'s recall', '3ED').
card_original_type('hurkyl\'s recall'/'3ED', 'Instant').
card_original_text('hurkyl\'s recall'/'3ED', 'All artifacts in play owned by target player are returned to target player\'s hand. Any enchantments on those artifacts are discarded.').
card_image_name('hurkyl\'s recall'/'3ED', 'hurkyl\'s recall').
card_uid('hurkyl\'s recall'/'3ED', '3ED:Hurkyl\'s Recall:hurkyl\'s recall').
card_rarity('hurkyl\'s recall'/'3ED', 'Rare').
card_artist('hurkyl\'s recall'/'3ED', 'NéNé Thomas').
card_flavor_text('hurkyl\'s recall'/'3ED', 'This spell, like many attributed to Drafna, was actually the work of his wife Hurkyl.').
card_multiverse_id('hurkyl\'s recall'/'3ED', '1202').

card_in_set('hurloon minotaur', '3ED').
card_original_type('hurloon minotaur'/'3ED', 'Summon — Minotaur').
card_original_text('hurloon minotaur'/'3ED', '').
card_image_name('hurloon minotaur'/'3ED', 'hurloon minotaur').
card_uid('hurloon minotaur'/'3ED', '3ED:Hurloon Minotaur:hurloon minotaur').
card_rarity('hurloon minotaur'/'3ED', 'Common').
card_artist('hurloon minotaur'/'3ED', 'Anson Maddocks').
card_flavor_text('hurloon minotaur'/'3ED', 'The Minotaurs of the Hurloon Mountains are known for their love of battle. They are also known for their hymns to the dead, sung for friend and foe alike. These hymns can last for days, filling the mountain valleys with their low, haunting sounds.').
card_multiverse_id('hurloon minotaur'/'3ED', '1300').

card_in_set('hurricane', '3ED').
card_original_type('hurricane'/'3ED', 'Sorcery').
card_original_text('hurricane'/'3ED', 'All players and flying creatures suffer X damage.').
card_image_name('hurricane'/'3ED', 'hurricane').
card_uid('hurricane'/'3ED', '3ED:Hurricane:hurricane').
card_rarity('hurricane'/'3ED', 'Uncommon').
card_artist('hurricane'/'3ED', 'Dameon Willich').
card_multiverse_id('hurricane'/'3ED', '1251').

card_in_set('hypnotic specter', '3ED').
card_original_type('hypnotic specter'/'3ED', 'Summon — Specter').
card_original_text('hypnotic specter'/'3ED', 'Flying\nAn opponent damaged by Specter must discard a card at random from his or her hand. Ignore this effect if opponent has no cards left in hand.').
card_image_name('hypnotic specter'/'3ED', 'hypnotic specter').
card_uid('hypnotic specter'/'3ED', '3ED:Hypnotic Specter:hypnotic specter').
card_rarity('hypnotic specter'/'3ED', 'Uncommon').
card_artist('hypnotic specter'/'3ED', 'Douglas Shuler').
card_flavor_text('hypnotic specter'/'3ED', '\"...There was no trace/ Of aught on that illumined face...\" —Samuel Coleridge, \"Phantom\"').
card_multiverse_id('hypnotic specter'/'3ED', '1165').

card_in_set('instill energy', '3ED').
card_original_type('instill energy'/'3ED', 'Enchant Creature').
card_original_text('instill energy'/'3ED', 'You may untap target creature one additional time during your turn. Target creature may also attack the turn it comes into play.').
card_image_name('instill energy'/'3ED', 'instill energy').
card_uid('instill energy'/'3ED', '3ED:Instill Energy:instill energy').
card_rarity('instill energy'/'3ED', 'Uncommon').
card_artist('instill energy'/'3ED', 'Dameon Willich').
card_multiverse_id('instill energy'/'3ED', '1252').

card_in_set('iron star', '3ED').
card_original_type('iron star'/'3ED', 'Artifact').
card_original_text('iron star'/'3ED', '{1}: Any red spell cast gives you 1 life. Can only give 1 life each time a red spell is cast.').
card_image_name('iron star'/'3ED', 'iron star').
card_uid('iron star'/'3ED', '3ED:Iron Star:iron star').
card_rarity('iron star'/'3ED', 'Uncommon').
card_artist('iron star'/'3ED', 'Dan Frazier').
card_multiverse_id('iron star'/'3ED', '1113').

card_in_set('ironroot treefolk', '3ED').
card_original_type('ironroot treefolk'/'3ED', 'Summon — Treefolk').
card_original_text('ironroot treefolk'/'3ED', '').
card_image_name('ironroot treefolk'/'3ED', 'ironroot treefolk').
card_uid('ironroot treefolk'/'3ED', '3ED:Ironroot Treefolk:ironroot treefolk').
card_rarity('ironroot treefolk'/'3ED', 'Common').
card_artist('ironroot treefolk'/'3ED', 'Jesper Myrfors').
card_flavor_text('ironroot treefolk'/'3ED', 'The mating habits of Treefolk, particularly the stalwart Ironroot Treefolk, are truly absurd. Molasses comes to mind. It\'s amazing the species can survive at all given such protracted periods of mate selection, conjugation, and gestation.').
card_multiverse_id('ironroot treefolk'/'3ED', '1253').

card_in_set('island', '3ED').
card_original_type('island'/'3ED', 'Land').
card_original_text('island'/'3ED', '{T}: Add {U} to your mana pool.').
card_image_name('island'/'3ED', 'island1').
card_uid('island'/'3ED', '3ED:Island:island1').
card_rarity('island'/'3ED', 'Basic Land').
card_artist('island'/'3ED', 'Mark Poole').
card_multiverse_id('island'/'3ED', '1394').

card_in_set('island', '3ED').
card_original_type('island'/'3ED', 'Land').
card_original_text('island'/'3ED', '{T}: Add {U} to your mana pool.').
card_image_name('island'/'3ED', 'island2').
card_uid('island'/'3ED', '3ED:Island:island2').
card_rarity('island'/'3ED', 'Basic Land').
card_artist('island'/'3ED', 'Mark Poole').
card_multiverse_id('island'/'3ED', '1393').

card_in_set('island', '3ED').
card_original_type('island'/'3ED', 'Land').
card_original_text('island'/'3ED', '{T}: Add {U} to your mana pool.').
card_image_name('island'/'3ED', 'island3').
card_uid('island'/'3ED', '3ED:Island:island3').
card_rarity('island'/'3ED', 'Basic Land').
card_artist('island'/'3ED', 'Mark Poole').
card_multiverse_id('island'/'3ED', '1392').

card_in_set('island fish jasconius', '3ED').
card_original_type('island fish jasconius'/'3ED', 'Summon — Island Fish').
card_original_text('island fish jasconius'/'3ED', 'You must pay {U}{U}{U} during your upkeep phase to untap Island Fish. Island Fish cannot attack unless opponent has islands in play. Island Fish is destroyed immediately if at any time you have no islands in play.').
card_image_name('island fish jasconius'/'3ED', 'island fish jasconius').
card_uid('island fish jasconius'/'3ED', '3ED:Island Fish Jasconius:island fish jasconius').
card_rarity('island fish jasconius'/'3ED', 'Rare').
card_artist('island fish jasconius'/'3ED', 'Jesper Myrfors').
card_multiverse_id('island fish jasconius'/'3ED', '1203').

card_in_set('island sanctuary', '3ED').
card_original_type('island sanctuary'/'3ED', 'Enchantment').
card_original_text('island sanctuary'/'3ED', 'You may decline to draw a card from your library during draw phase. In exchange, until start of your next turn the only creatures that may attack you are those that fly or have islandwalk.').
card_image_name('island sanctuary'/'3ED', 'island sanctuary').
card_uid('island sanctuary'/'3ED', '3ED:Island Sanctuary:island sanctuary').
card_rarity('island sanctuary'/'3ED', 'Rare').
card_artist('island sanctuary'/'3ED', 'Mark Poole').
card_multiverse_id('island sanctuary'/'3ED', '1351').

card_in_set('ivory cup', '3ED').
card_original_type('ivory cup'/'3ED', 'Artifact').
card_original_text('ivory cup'/'3ED', '{1}: Any white spell cast gives you 1 life. Can only give 1 life each time a white spell is cast.').
card_image_name('ivory cup'/'3ED', 'ivory cup').
card_uid('ivory cup'/'3ED', '3ED:Ivory Cup:ivory cup').
card_rarity('ivory cup'/'3ED', 'Uncommon').
card_artist('ivory cup'/'3ED', 'Anson Maddocks').
card_multiverse_id('ivory cup'/'3ED', '1114').

card_in_set('ivory tower', '3ED').
card_original_type('ivory tower'/'3ED', 'Artifact').
card_original_text('ivory tower'/'3ED', 'During your upkeep phase, gain 1 life for each card in your hand above four.').
card_image_name('ivory tower'/'3ED', 'ivory tower').
card_uid('ivory tower'/'3ED', '3ED:Ivory Tower:ivory tower').
card_rarity('ivory tower'/'3ED', 'Rare').
card_artist('ivory tower'/'3ED', 'Margaret Organ-Kean').
card_flavor_text('ivory tower'/'3ED', 'Valuing scholarship above all else, the inhabitants of the Ivory Tower reward those who sacrifice power for knowledge.').
card_multiverse_id('ivory tower'/'3ED', '1115').

card_in_set('jade monolith', '3ED').
card_original_type('jade monolith'/'3ED', 'Artifact').
card_original_text('jade monolith'/'3ED', '{1}: You may take damage done to any creature on yourself instead, but you must take all of it. Source of damage is unchanged.').
card_image_name('jade monolith'/'3ED', 'jade monolith').
card_uid('jade monolith'/'3ED', '3ED:Jade Monolith:jade monolith').
card_rarity('jade monolith'/'3ED', 'Rare').
card_artist('jade monolith'/'3ED', 'Anson Maddocks').
card_multiverse_id('jade monolith'/'3ED', '1116').

card_in_set('jandor\'s ring', '3ED').
card_original_type('jandor\'s ring'/'3ED', 'Artifact').
card_original_text('jandor\'s ring'/'3ED', '{2}, {T}: Discard a card you just drew from your library, and draw another card to replace it.').
card_image_name('jandor\'s ring'/'3ED', 'jandor\'s ring').
card_uid('jandor\'s ring'/'3ED', '3ED:Jandor\'s Ring:jandor\'s ring').
card_rarity('jandor\'s ring'/'3ED', 'Rare').
card_artist('jandor\'s ring'/'3ED', 'Dan Frazier').
card_multiverse_id('jandor\'s ring'/'3ED', '1117').

card_in_set('jandor\'s saddlebags', '3ED').
card_original_type('jandor\'s saddlebags'/'3ED', 'Artifact').
card_original_text('jandor\'s saddlebags'/'3ED', '{3}, {T}: Untap a creature.').
card_image_name('jandor\'s saddlebags'/'3ED', 'jandor\'s saddlebags').
card_uid('jandor\'s saddlebags'/'3ED', '3ED:Jandor\'s Saddlebags:jandor\'s saddlebags').
card_rarity('jandor\'s saddlebags'/'3ED', 'Rare').
card_artist('jandor\'s saddlebags'/'3ED', 'Dameon Willich').
card_flavor_text('jandor\'s saddlebags'/'3ED', 'Each day of their journey, Jandor opened the saddlebags and found them full of mutton, quinces, cheese, date rolls, wine, and all manner of delicious and satisfying foods.').
card_multiverse_id('jandor\'s saddlebags'/'3ED', '1118').

card_in_set('jayemdae tome', '3ED').
card_original_type('jayemdae tome'/'3ED', 'Artifact').
card_original_text('jayemdae tome'/'3ED', '{4}, {T}: Draw one extra card.').
card_image_name('jayemdae tome'/'3ED', 'jayemdae tome').
card_uid('jayemdae tome'/'3ED', '3ED:Jayemdae Tome:jayemdae tome').
card_rarity('jayemdae tome'/'3ED', 'Rare').
card_artist('jayemdae tome'/'3ED', 'Mark Tedin').
card_multiverse_id('jayemdae tome'/'3ED', '1119').

card_in_set('juggernaut', '3ED').
card_original_type('juggernaut'/'3ED', 'Artifact Creature').
card_original_text('juggernaut'/'3ED', 'Must attack each turn if possible. Can\'t be blocked by walls.').
card_image_name('juggernaut'/'3ED', 'juggernaut').
card_uid('juggernaut'/'3ED', '3ED:Juggernaut:juggernaut').
card_rarity('juggernaut'/'3ED', 'Uncommon').
card_artist('juggernaut'/'3ED', 'Dan Frazier').
card_flavor_text('juggernaut'/'3ED', 'We had taken refuge in a small cave, thinking the entrance was too narrow for it to follow. To our horror, its gigantic head smashed into the mountainside, ripping itself a new entrance.').
card_multiverse_id('juggernaut'/'3ED', '1120').

card_in_set('jump', '3ED').
card_original_type('jump'/'3ED', 'Instant').
card_original_text('jump'/'3ED', 'Target creature is a flying creature until end of turn.').
card_image_name('jump'/'3ED', 'jump').
card_uid('jump'/'3ED', '3ED:Jump:jump').
card_rarity('jump'/'3ED', 'Common').
card_artist('jump'/'3ED', 'Mark Poole').
card_multiverse_id('jump'/'3ED', '1204').

card_in_set('karma', '3ED').
card_original_type('karma'/'3ED', 'Enchantment').
card_original_text('karma'/'3ED', 'During a player\'s upkeep, Karma does 1 point of damage to that player for each swamp he or she has in play.').
card_image_name('karma'/'3ED', 'karma').
card_uid('karma'/'3ED', '3ED:Karma:karma').
card_rarity('karma'/'3ED', 'Uncommon').
card_artist('karma'/'3ED', 'Richard Thomas').
card_multiverse_id('karma'/'3ED', '1352').

card_in_set('keldon warlord', '3ED').
card_original_type('keldon warlord'/'3ED', 'Summon — Lord').
card_original_text('keldon warlord'/'3ED', 'The *s below are the number of non-wall creatures on your side, including Warlord. Thus if you have two other non-wall creatures, Warlord is 3/3. If one of those creatures is killed during the turn, Warlord immediately becomes 2/2.').
card_image_name('keldon warlord'/'3ED', 'keldon warlord').
card_uid('keldon warlord'/'3ED', '3ED:Keldon Warlord:keldon warlord').
card_rarity('keldon warlord'/'3ED', 'Uncommon').
card_artist('keldon warlord'/'3ED', 'Kev Brockschmidt').
card_multiverse_id('keldon warlord'/'3ED', '1301').

card_in_set('kird ape', '3ED').
card_original_type('kird ape'/'3ED', 'Summon — Ape').
card_original_text('kird ape'/'3ED', 'While controller has forests in play, Kird Ape gains +1/+2.').
card_image_name('kird ape'/'3ED', 'kird ape').
card_uid('kird ape'/'3ED', '3ED:Kird Ape:kird ape').
card_rarity('kird ape'/'3ED', 'Common').
card_artist('kird ape'/'3ED', 'Ken Meyer, Jr.').
card_multiverse_id('kird ape'/'3ED', '1302').

card_in_set('kormus bell', '3ED').
card_original_type('kormus bell'/'3ED', 'Artifact').
card_original_text('kormus bell'/'3ED', 'Treat all swamps in play as 1/1 creatures. Now they can be enchanted, killed, and so forth, and they can be tapped either for mana or to attack.').
card_image_name('kormus bell'/'3ED', 'kormus bell').
card_uid('kormus bell'/'3ED', '3ED:Kormus Bell:kormus bell').
card_rarity('kormus bell'/'3ED', 'Rare').
card_artist('kormus bell'/'3ED', 'Christopher Rush').
card_multiverse_id('kormus bell'/'3ED', '1121').

card_in_set('kudzu', '3ED').
card_original_type('kudzu'/'3ED', 'Enchant Land').
card_original_text('kudzu'/'3ED', 'When target land becomes tapped, it is destroyed. Unless that was the last land in play, Kudzu is not discarded; instead, the player whose land it just destroyed may place it on any other land in play.').
card_image_name('kudzu'/'3ED', 'kudzu').
card_uid('kudzu'/'3ED', '3ED:Kudzu:kudzu').
card_rarity('kudzu'/'3ED', 'Rare').
card_artist('kudzu'/'3ED', 'Mark Poole').
card_multiverse_id('kudzu'/'3ED', '1254').

card_in_set('lance', '3ED').
card_original_type('lance'/'3ED', 'Enchant Creature').
card_original_text('lance'/'3ED', 'Target creature gains first strike.').
card_image_name('lance'/'3ED', 'lance').
card_uid('lance'/'3ED', '3ED:Lance:lance').
card_rarity('lance'/'3ED', 'Uncommon').
card_artist('lance'/'3ED', 'Rob Alexander').
card_multiverse_id('lance'/'3ED', '1353').

card_in_set('ley druid', '3ED').
card_original_type('ley druid'/'3ED', 'Summon — Cleric').
card_original_text('ley druid'/'3ED', '{T}: Untap a land of your choice. This ability is played as an interrupt.').
card_image_name('ley druid'/'3ED', 'ley druid').
card_uid('ley druid'/'3ED', '3ED:Ley Druid:ley druid').
card_rarity('ley druid'/'3ED', 'Uncommon').
card_artist('ley druid'/'3ED', 'Sandra Everingham').
card_flavor_text('ley druid'/'3ED', 'After years of training, the Druid becomes one with nature, drawing power from the land and returning it when needed.').
card_multiverse_id('ley druid'/'3ED', '1255').

card_in_set('library of leng', '3ED').
card_original_type('library of leng'/'3ED', 'Artifact').
card_original_text('library of leng'/'3ED', 'You must skip the discard phase of your turn. If a card forces you to discard, you may choose to discard to top of your library rather than to graveyard. If discard is random, you may look at card before deciding where to discard it.').
card_image_name('library of leng'/'3ED', 'library of leng').
card_uid('library of leng'/'3ED', '3ED:Library of Leng:library of leng').
card_rarity('library of leng'/'3ED', 'Uncommon').
card_artist('library of leng'/'3ED', 'Daniel Gelon').
card_multiverse_id('library of leng'/'3ED', '1122').

card_in_set('lifeforce', '3ED').
card_original_type('lifeforce'/'3ED', 'Enchantment').
card_original_text('lifeforce'/'3ED', '{G}{G}: Counter a black spell as it is being cast. This use is played as an interrupt and does not affect black cards already in play.').
card_image_name('lifeforce'/'3ED', 'lifeforce').
card_uid('lifeforce'/'3ED', '3ED:Lifeforce:lifeforce').
card_rarity('lifeforce'/'3ED', 'Uncommon').
card_artist('lifeforce'/'3ED', 'Dameon Willich').
card_multiverse_id('lifeforce'/'3ED', '1256').

card_in_set('lifelace', '3ED').
card_original_type('lifelace'/'3ED', 'Interrupt').
card_original_text('lifelace'/'3ED', 'Changes the color of one card either being played or already in play to green. Cost to cast, tap, maintain, or use a special ability of target card remains entirely unchanged.').
card_image_name('lifelace'/'3ED', 'lifelace').
card_uid('lifelace'/'3ED', '3ED:Lifelace:lifelace').
card_rarity('lifelace'/'3ED', 'Rare').
card_artist('lifelace'/'3ED', 'Amy Weber').
card_multiverse_id('lifelace'/'3ED', '1257').

card_in_set('lifetap', '3ED').
card_original_type('lifetap'/'3ED', 'Enchantment').
card_original_text('lifetap'/'3ED', 'You gain 1 life each time any forest of opponent\'s becomes tapped.').
card_image_name('lifetap'/'3ED', 'lifetap').
card_uid('lifetap'/'3ED', '3ED:Lifetap:lifetap').
card_rarity('lifetap'/'3ED', 'Uncommon').
card_artist('lifetap'/'3ED', 'Anson Maddocks').
card_multiverse_id('lifetap'/'3ED', '1205').

card_in_set('lightning bolt', '3ED').
card_original_type('lightning bolt'/'3ED', 'Instant').
card_original_text('lightning bolt'/'3ED', 'Lightning Bolt does 3 damage to one target.').
card_image_name('lightning bolt'/'3ED', 'lightning bolt').
card_uid('lightning bolt'/'3ED', '3ED:Lightning Bolt:lightning bolt').
card_rarity('lightning bolt'/'3ED', 'Common').
card_artist('lightning bolt'/'3ED', 'Christopher Rush').
card_multiverse_id('lightning bolt'/'3ED', '1303').

card_in_set('living artifact', '3ED').
card_original_type('living artifact'/'3ED', 'Enchant Artifact').
card_original_text('living artifact'/'3ED', 'Put a counter on target artifact for each life you lose. During your upkeep you may trade one counter for one life, but you can only trade in one counter during each of your upkeeps.').
card_image_name('living artifact'/'3ED', 'living artifact').
card_uid('living artifact'/'3ED', '3ED:Living Artifact:living artifact').
card_rarity('living artifact'/'3ED', 'Rare').
card_artist('living artifact'/'3ED', 'Anson Maddocks').
card_multiverse_id('living artifact'/'3ED', '1258').

card_in_set('living lands', '3ED').
card_original_type('living lands'/'3ED', 'Enchantment').
card_original_text('living lands'/'3ED', 'Treat all forests in play as 1/1 creatures. Now they can be enchanted, killed, and so forth, and they can be tapped either for mana or to attack.').
card_image_name('living lands'/'3ED', 'living lands').
card_uid('living lands'/'3ED', '3ED:Living Lands:living lands').
card_rarity('living lands'/'3ED', 'Rare').
card_artist('living lands'/'3ED', 'Jesper Myrfors').
card_multiverse_id('living lands'/'3ED', '1259').

card_in_set('living wall', '3ED').
card_original_type('living wall'/'3ED', 'Artifact Creature').
card_original_text('living wall'/'3ED', 'Counts as a wall. {1}: Regenerates.').
card_image_name('living wall'/'3ED', 'living wall').
card_uid('living wall'/'3ED', '3ED:Living Wall:living wall').
card_rarity('living wall'/'3ED', 'Uncommon').
card_artist('living wall'/'3ED', 'Anson Maddocks').
card_flavor_text('living wall'/'3ED', 'Some fiendish mage had created a horrifying wall of living flesh, patched together from a jumble of still-recognizable body parts. As we sought to hew our way through it, some unknown power healed the gaping wounds we cut, denying us passage.').
card_multiverse_id('living wall'/'3ED', '1123').

card_in_set('llanowar elves', '3ED').
card_original_type('llanowar elves'/'3ED', 'Summon — Elves').
card_original_text('llanowar elves'/'3ED', '{T}: Add {G} to your mana pool. This ability is played as an interrupt.').
card_image_name('llanowar elves'/'3ED', 'llanowar elves').
card_uid('llanowar elves'/'3ED', '3ED:Llanowar Elves:llanowar elves').
card_rarity('llanowar elves'/'3ED', 'Common').
card_artist('llanowar elves'/'3ED', 'Anson Maddocks').
card_flavor_text('llanowar elves'/'3ED', 'Whenever the Llanowar Elves gather the fruits of their forest, they leave one plant of each type untouched, considering that nature\'s portion.').
card_multiverse_id('llanowar elves'/'3ED', '1260').

card_in_set('lord of atlantis', '3ED').
card_original_type('lord of atlantis'/'3ED', 'Summon — Lord').
card_original_text('lord of atlantis'/'3ED', 'All Merfolk in play gain islandwalk and +1/+1 while this card is in play.').
card_image_name('lord of atlantis'/'3ED', 'lord of atlantis').
card_uid('lord of atlantis'/'3ED', '3ED:Lord of Atlantis:lord of atlantis').
card_rarity('lord of atlantis'/'3ED', 'Rare').
card_artist('lord of atlantis'/'3ED', 'Melissa A. Benson').
card_flavor_text('lord of atlantis'/'3ED', 'A master of tactics, the Lord of Atlantis makes his people bold in battle merely by arriving to lead them.').
card_multiverse_id('lord of atlantis'/'3ED', '1206').

card_in_set('lord of the pit', '3ED').
card_original_type('lord of the pit'/'3ED', 'Summon — Demon').
card_original_text('lord of the pit'/'3ED', 'Flying, trample\nYou must sacrifice one of your own creatures during your upkeep or Lord of the Pit does 7 damage to you. You may still attack with Lord of the Pit even if you failed to sacrifice a creature. Lord of the Pit may not be sacrificed to itself.').
card_image_name('lord of the pit'/'3ED', 'lord of the pit').
card_uid('lord of the pit'/'3ED', '3ED:Lord of the Pit:lord of the pit').
card_rarity('lord of the pit'/'3ED', 'Rare').
card_artist('lord of the pit'/'3ED', 'Mark Tedin').
card_multiverse_id('lord of the pit'/'3ED', '1166').

card_in_set('lure', '3ED').
card_original_type('lure'/'3ED', 'Enchant Creature').
card_original_text('lure'/'3ED', 'All creatures able to block target creature must do so. If a creature has the ability to block more than one creature, Lure does not prevent this. If there is more than one attacking creature with Lure, defender may choose which of them each defending creature blocks.').
card_image_name('lure'/'3ED', 'lure').
card_uid('lure'/'3ED', '3ED:Lure:lure').
card_rarity('lure'/'3ED', 'Uncommon').
card_artist('lure'/'3ED', 'Anson Maddocks').
card_multiverse_id('lure'/'3ED', '1261').

card_in_set('magical hack', '3ED').
card_original_type('magical hack'/'3ED', 'Interrupt').
card_original_text('magical hack'/'3ED', 'Change the text of any card being played or already in play by replacing one basic land type with another. For example, you can change \"swampwalk\" to \"plainswalk.\"').
card_image_name('magical hack'/'3ED', 'magical hack').
card_uid('magical hack'/'3ED', '3ED:Magical Hack:magical hack').
card_rarity('magical hack'/'3ED', 'Rare').
card_artist('magical hack'/'3ED', 'Julie Baroh').
card_multiverse_id('magical hack'/'3ED', '1207').

card_in_set('magnetic mountain', '3ED').
card_original_type('magnetic mountain'/'3ED', 'Enchantment').
card_original_text('magnetic mountain'/'3ED', 'Blue creatures do not untap as normal. During their upkeep phases, players must spend {4} for each blue creature they wish to untap. This cost must be paid in addition to any other untap cost a given blue creature may already require.').
card_image_name('magnetic mountain'/'3ED', 'magnetic mountain').
card_uid('magnetic mountain'/'3ED', '3ED:Magnetic Mountain:magnetic mountain').
card_rarity('magnetic mountain'/'3ED', 'Rare').
card_artist('magnetic mountain'/'3ED', 'Susan Van Camp').
card_multiverse_id('magnetic mountain'/'3ED', '1304').

card_in_set('mahamoti djinn', '3ED').
card_original_type('mahamoti djinn'/'3ED', 'Summon — Djinn').
card_original_text('mahamoti djinn'/'3ED', 'Flying').
card_image_name('mahamoti djinn'/'3ED', 'mahamoti djinn').
card_uid('mahamoti djinn'/'3ED', '3ED:Mahamoti Djinn:mahamoti djinn').
card_rarity('mahamoti djinn'/'3ED', 'Rare').
card_artist('mahamoti djinn'/'3ED', 'Dan Frazier').
card_flavor_text('mahamoti djinn'/'3ED', 'Of royal blood among the spirits of the air, the Mahamoti Djinn rides on the wings of the winds. As dangerous in the gambling hall as he is in battle, he is a master of trickery and misdirection.').
card_multiverse_id('mahamoti djinn'/'3ED', '1208').

card_in_set('mana flare', '3ED').
card_original_type('mana flare'/'3ED', 'Enchantment').
card_original_text('mana flare'/'3ED', 'Whenever either player taps a land for mana, it produces 1 extra mana of the appropriate type.').
card_image_name('mana flare'/'3ED', 'mana flare').
card_uid('mana flare'/'3ED', '3ED:Mana Flare:mana flare').
card_rarity('mana flare'/'3ED', 'Rare').
card_artist('mana flare'/'3ED', 'Christopher Rush').
card_multiverse_id('mana flare'/'3ED', '1305').

card_in_set('mana short', '3ED').
card_original_type('mana short'/'3ED', 'Instant').
card_original_text('mana short'/'3ED', 'All opponent\'s lands are tapped, and opponent\'s mana pool is emptied. Opponent takes no damage from unspent mana.').
card_image_name('mana short'/'3ED', 'mana short').
card_uid('mana short'/'3ED', '3ED:Mana Short:mana short').
card_rarity('mana short'/'3ED', 'Rare').
card_artist('mana short'/'3ED', 'Dameon Willich').
card_multiverse_id('mana short'/'3ED', '1209').

card_in_set('mana vault', '3ED').
card_original_type('mana vault'/'3ED', 'Artifact').
card_original_text('mana vault'/'3ED', '{T}: Add 3 colorless mana to your mana pool. Mana Vault doesn\'t untap normally during untap phase; to untap it, you must pay 4 mana during your upkeep. If Mana Vault remains tapped during upkeep it does 1 damage to you. Drawing mana from this artifact is played as an interrupt.').
card_image_name('mana vault'/'3ED', 'mana vault').
card_uid('mana vault'/'3ED', '3ED:Mana Vault:mana vault').
card_rarity('mana vault'/'3ED', 'Rare').
card_artist('mana vault'/'3ED', 'Mark Tedin').
card_multiverse_id('mana vault'/'3ED', '1124').

card_in_set('manabarbs', '3ED').
card_original_type('manabarbs'/'3ED', 'Enchantment').
card_original_text('manabarbs'/'3ED', 'Whenever mana is drawn from a land, Manabarbs does 1 damage to the land\'s controller.').
card_image_name('manabarbs'/'3ED', 'manabarbs').
card_uid('manabarbs'/'3ED', '3ED:Manabarbs:manabarbs').
card_rarity('manabarbs'/'3ED', 'Rare').
card_artist('manabarbs'/'3ED', 'Christopher Rush').
card_multiverse_id('manabarbs'/'3ED', '1306').

card_in_set('meekstone', '3ED').
card_original_type('meekstone'/'3ED', 'Artifact').
card_original_text('meekstone'/'3ED', 'Any creature with power greater than 2 may not be untapped as normal during the untap phase.').
card_image_name('meekstone'/'3ED', 'meekstone').
card_uid('meekstone'/'3ED', '3ED:Meekstone:meekstone').
card_rarity('meekstone'/'3ED', 'Rare').
card_artist('meekstone'/'3ED', 'Quinton Hoover').
card_multiverse_id('meekstone'/'3ED', '1125').

card_in_set('merfolk of the pearl trident', '3ED').
card_original_type('merfolk of the pearl trident'/'3ED', 'Summon — Merfolk').
card_original_text('merfolk of the pearl trident'/'3ED', '').
card_image_name('merfolk of the pearl trident'/'3ED', 'merfolk of the pearl trident').
card_uid('merfolk of the pearl trident'/'3ED', '3ED:Merfolk of the Pearl Trident:merfolk of the pearl trident').
card_rarity('merfolk of the pearl trident'/'3ED', 'Common').
card_artist('merfolk of the pearl trident'/'3ED', 'Jeff A. Menges').
card_flavor_text('merfolk of the pearl trident'/'3ED', 'Most human scholars believe that Merfolk are the survivors of sunken Atlantis, humans adapted to the water. Merfolk, however, believe that humans sprang forth from Merfolk who adapted themselves in order to explore their last frontier.').
card_multiverse_id('merfolk of the pearl trident'/'3ED', '1210').

card_in_set('mesa pegasus', '3ED').
card_original_type('mesa pegasus'/'3ED', 'Summon — Pegasus').
card_original_text('mesa pegasus'/'3ED', 'Flying, bands').
card_image_name('mesa pegasus'/'3ED', 'mesa pegasus').
card_uid('mesa pegasus'/'3ED', '3ED:Mesa Pegasus:mesa pegasus').
card_rarity('mesa pegasus'/'3ED', 'Common').
card_artist('mesa pegasus'/'3ED', 'Melissa A. Benson').
card_flavor_text('mesa pegasus'/'3ED', 'Before a woman marries in the village of Sursi, she must visit the land of the Mesa Pegasus. Legend has it that if the woman is pure of heart and her love is true, a Mesa Pegasus will appear, blessing her family with long life and good fortune.').
card_multiverse_id('mesa pegasus'/'3ED', '1354').

card_in_set('mijae djinn', '3ED').
card_original_type('mijae djinn'/'3ED', 'Summon — Djinn').
card_original_text('mijae djinn'/'3ED', 'If you choose to attack with Mijae Djinn, flip a coin immediately after attack is announced; opponent calls heads or tails while coin is in the air. If the flip ends up in opponent\'s favor, Mijae Djinn is tapped but does not attack.').
card_image_name('mijae djinn'/'3ED', 'mijae djinn').
card_uid('mijae djinn'/'3ED', '3ED:Mijae Djinn:mijae djinn').
card_rarity('mijae djinn'/'3ED', 'Rare').
card_artist('mijae djinn'/'3ED', 'Susan Van Camp').
card_multiverse_id('mijae djinn'/'3ED', '1307').

card_in_set('millstone', '3ED').
card_original_type('millstone'/'3ED', 'Artifact').
card_original_text('millstone'/'3ED', '{2}, {T}: Take the top two cards from target player\'s library and put them in target player\'s graveyard.').
card_image_name('millstone'/'3ED', 'millstone').
card_uid('millstone'/'3ED', '3ED:Millstone:millstone').
card_rarity('millstone'/'3ED', 'Rare').
card_artist('millstone'/'3ED', 'Kaja Foglio').
card_flavor_text('millstone'/'3ED', 'More than one mage was driven insane by the sound of the Millstone relentlessly grinding away.').
card_multiverse_id('millstone'/'3ED', '1126').

card_in_set('mind twist', '3ED').
card_original_type('mind twist'/'3ED', 'Sorcery').
card_original_text('mind twist'/'3ED', 'Opponent must discard X cards at random from hand. If opponent doesn\'t have enough cards in hand, entire hand is discarded.').
card_image_name('mind twist'/'3ED', 'mind twist').
card_uid('mind twist'/'3ED', '3ED:Mind Twist:mind twist').
card_rarity('mind twist'/'3ED', 'Rare').
card_artist('mind twist'/'3ED', 'Julie Baroh').
card_multiverse_id('mind twist'/'3ED', '1167').

card_in_set('mishra\'s war machine', '3ED').
card_original_type('mishra\'s war machine'/'3ED', 'Artifact Creature').
card_original_text('mishra\'s war machine'/'3ED', 'Bands\nDuring your upkeep, discard one card of your choice from your hand, or Mishra\'s War Machine becomes tapped and does 3 points of damage to you.').
card_image_name('mishra\'s war machine'/'3ED', 'mishra\'s war machine').
card_uid('mishra\'s war machine'/'3ED', '3ED:Mishra\'s War Machine:mishra\'s war machine').
card_rarity('mishra\'s war machine'/'3ED', 'Rare').
card_artist('mishra\'s war machine'/'3ED', 'Amy Weber').
card_multiverse_id('mishra\'s war machine'/'3ED', '1127').

card_in_set('mons\'s goblin raiders', '3ED').
card_original_type('mons\'s goblin raiders'/'3ED', 'Summon — Goblins').
card_original_text('mons\'s goblin raiders'/'3ED', '').
card_image_name('mons\'s goblin raiders'/'3ED', 'mons\'s goblin raiders').
card_uid('mons\'s goblin raiders'/'3ED', '3ED:Mons\'s Goblin Raiders:mons\'s goblin raiders').
card_rarity('mons\'s goblin raiders'/'3ED', 'Common').
card_artist('mons\'s goblin raiders'/'3ED', 'Jeff A. Menges').
card_flavor_text('mons\'s goblin raiders'/'3ED', 'The intricate dynamics of Rundvelt Goblin affairs are often confused with anarchy. The chaos, however, is the chaos of a thundercloud, and direction will sporadically and violently appear. Pashalik Mons and his raiders are the thunderhead that leads in the storm.').
card_multiverse_id('mons\'s goblin raiders'/'3ED', '1308').

card_in_set('mountain', '3ED').
card_original_type('mountain'/'3ED', 'Land').
card_original_text('mountain'/'3ED', '{T}: Add {R} to your mana pool.').
card_image_name('mountain'/'3ED', 'mountain1').
card_uid('mountain'/'3ED', '3ED:Mountain:mountain1').
card_rarity('mountain'/'3ED', 'Basic Land').
card_artist('mountain'/'3ED', 'Douglas Shuler').
card_multiverse_id('mountain'/'3ED', '1391').

card_in_set('mountain', '3ED').
card_original_type('mountain'/'3ED', 'Land').
card_original_text('mountain'/'3ED', '{T}: Add {R} to your mana pool.').
card_image_name('mountain'/'3ED', 'mountain2').
card_uid('mountain'/'3ED', '3ED:Mountain:mountain2').
card_rarity('mountain'/'3ED', 'Basic Land').
card_artist('mountain'/'3ED', 'Douglas Shuler').
card_multiverse_id('mountain'/'3ED', '1389').

card_in_set('mountain', '3ED').
card_original_type('mountain'/'3ED', 'Land').
card_original_text('mountain'/'3ED', '{T}: Add {R} to your mana pool.').
card_image_name('mountain'/'3ED', 'mountain3').
card_uid('mountain'/'3ED', '3ED:Mountain:mountain3').
card_rarity('mountain'/'3ED', 'Basic Land').
card_artist('mountain'/'3ED', 'Douglas Shuler').
card_multiverse_id('mountain'/'3ED', '1390').

card_in_set('nether shadow', '3ED').
card_original_type('nether shadow'/'3ED', 'Summon — Shadow').
card_original_text('nether shadow'/'3ED', 'If Shadow is in graveyard with any combination of cards above it that includes at least three creatures, it can be returned to play during your upkeep. Shadow can attack on same turn summoned or returned to play.').
card_image_name('nether shadow'/'3ED', 'nether shadow').
card_uid('nether shadow'/'3ED', '3ED:Nether Shadow:nether shadow').
card_rarity('nether shadow'/'3ED', 'Rare').
card_artist('nether shadow'/'3ED', 'Christopher Rush').
card_multiverse_id('nether shadow'/'3ED', '1168').

card_in_set('nettling imp', '3ED').
card_original_type('nettling imp'/'3ED', 'Summon — Imp').
card_original_text('nettling imp'/'3ED', '{T}: Force a particular one of opponent\'s non-wall creatures to attack. If target creature cannot attack, it is killed at end of turn. This ability can only be used during opponent\'s turn, before the attack. May not be used on creatures summoned this turn.').
card_image_name('nettling imp'/'3ED', 'nettling imp').
card_uid('nettling imp'/'3ED', '3ED:Nettling Imp:nettling imp').
card_rarity('nettling imp'/'3ED', 'Uncommon').
card_artist('nettling imp'/'3ED', 'Quinton Hoover').
card_multiverse_id('nettling imp'/'3ED', '1169').

card_in_set('nevinyrral\'s disk', '3ED').
card_original_type('nevinyrral\'s disk'/'3ED', 'Artifact').
card_original_text('nevinyrral\'s disk'/'3ED', '{1}: Destroys all creatures, enchantments, and artifacts in play, including Disk itself. Disk begins tapped but can be untapped as usual.').
card_image_name('nevinyrral\'s disk'/'3ED', 'nevinyrral\'s disk').
card_uid('nevinyrral\'s disk'/'3ED', '3ED:Nevinyrral\'s Disk:nevinyrral\'s disk').
card_rarity('nevinyrral\'s disk'/'3ED', 'Rare').
card_artist('nevinyrral\'s disk'/'3ED', 'Mark Tedin').
card_multiverse_id('nevinyrral\'s disk'/'3ED', '1128').

card_in_set('nightmare', '3ED').
card_original_type('nightmare'/'3ED', 'Summon — Nightmare').
card_original_text('nightmare'/'3ED', 'Flying\nNightmare\'s power and toughness both equal the number of swamps its controller has in play.').
card_image_name('nightmare'/'3ED', 'nightmare').
card_uid('nightmare'/'3ED', '3ED:Nightmare:nightmare').
card_rarity('nightmare'/'3ED', 'Rare').
card_artist('nightmare'/'3ED', 'Melissa A. Benson').
card_flavor_text('nightmare'/'3ED', 'The Nightmare arises from its lair in the swamps. As the poisoned land spreads, so does the Nightmare\'s rage and terrifying strength.').
card_multiverse_id('nightmare'/'3ED', '1170').

card_in_set('northern paladin', '3ED').
card_original_type('northern paladin'/'3ED', 'Summon — Paladin').
card_original_text('northern paladin'/'3ED', '{W}{W}, {T}: Destroys a black card in play. Cannot be used to cancel a black spell as it is being cast.').
card_image_name('northern paladin'/'3ED', 'northern paladin').
card_uid('northern paladin'/'3ED', '3ED:Northern Paladin:northern paladin').
card_rarity('northern paladin'/'3ED', 'Rare').
card_artist('northern paladin'/'3ED', 'Douglas Shuler').
card_flavor_text('northern paladin'/'3ED', '\"Look to the north; there you will find aid and comfort.\"\n—The Book of Tal').
card_multiverse_id('northern paladin'/'3ED', '1355').

card_in_set('obsianus golem', '3ED').
card_original_type('obsianus golem'/'3ED', 'Artifact Creature').
card_original_text('obsianus golem'/'3ED', '').
card_image_name('obsianus golem'/'3ED', 'obsianus golem').
card_uid('obsianus golem'/'3ED', '3ED:Obsianus Golem:obsianus golem').
card_rarity('obsianus golem'/'3ED', 'Uncommon').
card_artist('obsianus golem'/'3ED', 'Jesper Myrfors').
card_flavor_text('obsianus golem'/'3ED', '\"The foot stone is connected to the ankle stone, the ankle stone is connected to the leg stone...\"\n—Song of the Artificer').
card_multiverse_id('obsianus golem'/'3ED', '1129').

card_in_set('onulet', '3ED').
card_original_type('onulet'/'3ED', 'Artifact Creature').
card_original_text('onulet'/'3ED', 'If Onulet is placed in the graveyard, its controller gains 2 life.').
card_image_name('onulet'/'3ED', 'onulet').
card_uid('onulet'/'3ED', '3ED:Onulet:onulet').
card_rarity('onulet'/'3ED', 'Rare').
card_artist('onulet'/'3ED', 'Anson Maddocks').
card_flavor_text('onulet'/'3ED', 'An early inspiration for Urza, Tocasia\'s Onulets contained magical essences that could be cannibalized after they stopped functioning.').
card_multiverse_id('onulet'/'3ED', '1130').

card_in_set('orcish artillery', '3ED').
card_original_type('orcish artillery'/'3ED', 'Summon — Orcs').
card_original_text('orcish artillery'/'3ED', '{T}: Orcish Artillery does 2 damage to any target, but it also does 3 damage to you.').
card_image_name('orcish artillery'/'3ED', 'orcish artillery').
card_uid('orcish artillery'/'3ED', '3ED:Orcish Artillery:orcish artillery').
card_rarity('orcish artillery'/'3ED', 'Uncommon').
card_artist('orcish artillery'/'3ED', 'Anson Maddocks').
card_flavor_text('orcish artillery'/'3ED', 'In a rare display of ingenuity, the Orcs invented an incredibly destructive weapon. Most Orcish artillerists are those who dared criticize its effectiveness.').
card_multiverse_id('orcish artillery'/'3ED', '1309').

card_in_set('orcish oriflamme', '3ED').
card_original_type('orcish oriflamme'/'3ED', 'Enchantment').
card_original_text('orcish oriflamme'/'3ED', 'During your attack, all of your attacking creatures gain +1/+0.').
card_image_name('orcish oriflamme'/'3ED', 'orcish oriflamme').
card_uid('orcish oriflamme'/'3ED', '3ED:Orcish Oriflamme:orcish oriflamme').
card_rarity('orcish oriflamme'/'3ED', 'Uncommon').
card_artist('orcish oriflamme'/'3ED', 'Dan Frazier').
card_multiverse_id('orcish oriflamme'/'3ED', '1310').

card_in_set('ornithopter', '3ED').
card_original_type('ornithopter'/'3ED', 'Artifact Creature').
card_original_text('ornithopter'/'3ED', 'Flying').
card_image_name('ornithopter'/'3ED', 'ornithopter').
card_uid('ornithopter'/'3ED', '3ED:Ornithopter:ornithopter').
card_rarity('ornithopter'/'3ED', 'Uncommon').
card_artist('ornithopter'/'3ED', 'Amy Weber').
card_flavor_text('ornithopter'/'3ED', 'Many scholars believe that these creatures were the result of Urza\'s first attempt at mechanical life, perhaps created in his early days as an apprentice to Tocasia.').
card_multiverse_id('ornithopter'/'3ED', '1131').

card_in_set('paralyze', '3ED').
card_original_type('paralyze'/'3ED', 'Enchant Creature').
card_original_text('paralyze'/'3ED', 'Target creature is not untapped as normal during untap phase. Creature\'s controller may spend {4} during his or her upkeep to untap it. Tap target creature when Paralyze is cast.').
card_image_name('paralyze'/'3ED', 'paralyze').
card_uid('paralyze'/'3ED', '3ED:Paralyze:paralyze').
card_rarity('paralyze'/'3ED', 'Common').
card_artist('paralyze'/'3ED', 'Anson Maddocks').
card_multiverse_id('paralyze'/'3ED', '1171').

card_in_set('pearled unicorn', '3ED').
card_original_type('pearled unicorn'/'3ED', 'Summon — Unicorn').
card_original_text('pearled unicorn'/'3ED', '').
card_image_name('pearled unicorn'/'3ED', 'pearled unicorn').
card_uid('pearled unicorn'/'3ED', '3ED:Pearled Unicorn:pearled unicorn').
card_rarity('pearled unicorn'/'3ED', 'Common').
card_artist('pearled unicorn'/'3ED', 'Cornelius Brudi').
card_flavor_text('pearled unicorn'/'3ED', '\"‘Do you know, I always thought Unicorns were fabulous monsters, too? I never saw one alive before!\' ‘Well, now that we have seen each other,\' said the Unicorn, ‘if you\'ll believe in me, I\'ll believe in you.\'\" —Lewis Carroll').
card_multiverse_id('pearled unicorn'/'3ED', '1356').

card_in_set('personal incarnation', '3ED').
card_original_type('personal incarnation'/'3ED', 'Summon — Avatar').
card_original_text('personal incarnation'/'3ED', 'Caster can redirect any or all damage done to Personal Incarnation to self instead. The source of the damage is unchanged. If Personal Incarnation goes to the graveyard, caster loses half his or her remaining life points, rounding up the loss.').
card_image_name('personal incarnation'/'3ED', 'personal incarnation').
card_uid('personal incarnation'/'3ED', '3ED:Personal Incarnation:personal incarnation').
card_rarity('personal incarnation'/'3ED', 'Rare').
card_artist('personal incarnation'/'3ED', 'Kev Brockschmidt').
card_multiverse_id('personal incarnation'/'3ED', '1357').

card_in_set('pestilence', '3ED').
card_original_type('pestilence'/'3ED', 'Enchantment').
card_original_text('pestilence'/'3ED', '{B} Do 1 damage to each creature and to both players.\nIf there are no creatures in play at the end of any turn, Pestilence must be discarded.').
card_image_name('pestilence'/'3ED', 'pestilence').
card_uid('pestilence'/'3ED', '3ED:Pestilence:pestilence').
card_rarity('pestilence'/'3ED', 'Common').
card_artist('pestilence'/'3ED', 'Jesper Myrfors').
card_multiverse_id('pestilence'/'3ED', '1172').

card_in_set('phantasmal forces', '3ED').
card_original_type('phantasmal forces'/'3ED', 'Summon — Phantasm').
card_original_text('phantasmal forces'/'3ED', 'Flying\nController must spend {U} during upkeep to maintain or Phantasmal Forces are destroyed.').
card_image_name('phantasmal forces'/'3ED', 'phantasmal forces').
card_uid('phantasmal forces'/'3ED', '3ED:Phantasmal Forces:phantasmal forces').
card_rarity('phantasmal forces'/'3ED', 'Uncommon').
card_artist('phantasmal forces'/'3ED', 'Mark Poole').
card_flavor_text('phantasmal forces'/'3ED', 'These beings embody the essence of true heroes long dead. Summoned from the dreamrealms, they rise to meet their enemies.').
card_multiverse_id('phantasmal forces'/'3ED', '1211').

card_in_set('phantasmal terrain', '3ED').
card_original_type('phantasmal terrain'/'3ED', 'Enchant Land').
card_original_text('phantasmal terrain'/'3ED', 'Target land changes to any basic land type of caster\'s choice. Land type is set when cast and may not be further altered by this card.').
card_image_name('phantasmal terrain'/'3ED', 'phantasmal terrain').
card_uid('phantasmal terrain'/'3ED', '3ED:Phantasmal Terrain:phantasmal terrain').
card_rarity('phantasmal terrain'/'3ED', 'Common').
card_artist('phantasmal terrain'/'3ED', 'Dameon Willich').
card_multiverse_id('phantasmal terrain'/'3ED', '1212').

card_in_set('phantom monster', '3ED').
card_original_type('phantom monster'/'3ED', 'Summon — Phantasm').
card_original_text('phantom monster'/'3ED', 'Flying').
card_image_name('phantom monster'/'3ED', 'phantom monster').
card_uid('phantom monster'/'3ED', '3ED:Phantom Monster:phantom monster').
card_rarity('phantom monster'/'3ED', 'Uncommon').
card_artist('phantom monster'/'3ED', 'Jesper Myrfors').
card_flavor_text('phantom monster'/'3ED', '\"While, like a ghastly rapid river,\nThrough the pale door,\nA hideous throng rush out forever,\nAnd laugh—but smile no more.\"\n—Edgar Allan Poe, \"The Haunted Palace\"').
card_multiverse_id('phantom monster'/'3ED', '1213').

card_in_set('pirate ship', '3ED').
card_original_type('pirate ship'/'3ED', 'Summon — Ship').
card_original_text('pirate ship'/'3ED', '{T}: Do 1 damage to any target.\nCannot attack unless opponent has islands in play, though controller may still use special ability. Pirate Ship is destroyed immediately if at any time controller has no islands in play.').
card_image_name('pirate ship'/'3ED', 'pirate ship').
card_uid('pirate ship'/'3ED', '3ED:Pirate Ship:pirate ship').
card_rarity('pirate ship'/'3ED', 'Rare').
card_artist('pirate ship'/'3ED', 'Tom Wänerstrand').
card_multiverse_id('pirate ship'/'3ED', '1214').

card_in_set('plague rats', '3ED').
card_original_type('plague rats'/'3ED', 'Summon — Rats').
card_original_text('plague rats'/'3ED', 'The *s below are the number of Plague Rats in play, counting both sides. Thus if there are two Plague Rats in play, each has power and toughness 2/2.').
card_image_name('plague rats'/'3ED', 'plague rats').
card_uid('plague rats'/'3ED', '3ED:Plague Rats:plague rats').
card_rarity('plague rats'/'3ED', 'Common').
card_artist('plague rats'/'3ED', 'Anson Maddocks').
card_flavor_text('plague rats'/'3ED', '\"Should you a Rat to madness tease\nWhy ev\'n a Rat may plague you. . .\"\n—Samuel Coleridge, \"Recantation\"').
card_multiverse_id('plague rats'/'3ED', '1173').

card_in_set('plains', '3ED').
card_original_type('plains'/'3ED', 'Land').
card_original_text('plains'/'3ED', '{T}: Add {W} to your mana pool.').
card_image_name('plains'/'3ED', 'plains1').
card_uid('plains'/'3ED', '3ED:Plains:plains1').
card_rarity('plains'/'3ED', 'Basic Land').
card_artist('plains'/'3ED', 'Jesper Myrfors').
card_multiverse_id('plains'/'3ED', '1397').

card_in_set('plains', '3ED').
card_original_type('plains'/'3ED', 'Land').
card_original_text('plains'/'3ED', '{T}: Add {W} to your mana pool.').
card_image_name('plains'/'3ED', 'plains2').
card_uid('plains'/'3ED', '3ED:Plains:plains2').
card_rarity('plains'/'3ED', 'Basic Land').
card_artist('plains'/'3ED', 'Jesper Myrfors').
card_multiverse_id('plains'/'3ED', '1396').

card_in_set('plains', '3ED').
card_original_type('plains'/'3ED', 'Land').
card_original_text('plains'/'3ED', '{T}: Add {W} to your mana pool.').
card_image_name('plains'/'3ED', 'plains3').
card_uid('plains'/'3ED', '3ED:Plains:plains3').
card_rarity('plains'/'3ED', 'Basic Land').
card_artist('plains'/'3ED', 'Jesper Myrfors').
card_multiverse_id('plains'/'3ED', '1395').

card_in_set('plateau', '3ED').
card_original_type('plateau'/'3ED', 'Land').
card_original_text('plateau'/'3ED', '{T}: Add either {R} or {W} to your mana pool.\nCounts as both mountains and plains and is affected by spells that affect either. If a spell destroys one of these land types, this card is destroyed; if a spell alters one of these land types, the other land type is unaffected.').
card_image_name('plateau'/'3ED', 'plateau').
card_uid('plateau'/'3ED', '3ED:Plateau:plateau').
card_rarity('plateau'/'3ED', 'Rare').
card_artist('plateau'/'3ED', 'Cornelius Brudi').
card_multiverse_id('plateau'/'3ED', '1378').

card_in_set('power leak', '3ED').
card_original_type('power leak'/'3ED', 'Enchant Enchantment').
card_original_text('power leak'/'3ED', 'Target enchantment costs 2 extra mana during the upkeep phase of each of its controller\'s turns. If target enchantment\'s controller cannot or will not pay this extra mana, Power Leak does 1 damage to him or her for each unpaid mana.').
card_image_name('power leak'/'3ED', 'power leak').
card_uid('power leak'/'3ED', '3ED:Power Leak:power leak').
card_rarity('power leak'/'3ED', 'Common').
card_artist('power leak'/'3ED', 'Drew Tucker').
card_multiverse_id('power leak'/'3ED', '1215').

card_in_set('power sink', '3ED').
card_original_type('power sink'/'3ED', 'Interrupt').
card_original_text('power sink'/'3ED', 'Target spell is countered unless its caster spends X more mana. Caster of target spell must draw and spend all available mana from lands and mana pool until X is spent; he or she may also spend mana from other sources if desired. If this is not enough mana, target spell will still be countered.').
card_image_name('power sink'/'3ED', 'power sink').
card_uid('power sink'/'3ED', '3ED:Power Sink:power sink').
card_rarity('power sink'/'3ED', 'Common').
card_artist('power sink'/'3ED', 'Richard Thomas').
card_multiverse_id('power sink'/'3ED', '1216').

card_in_set('power surge', '3ED').
card_original_type('power surge'/'3ED', 'Enchantment').
card_original_text('power surge'/'3ED', 'At the beginning of a player\'s turn, before the untap phase, the player must take a counter for each of his or her lands that is not tapped. During the player\'s upkeep, Power Surge does 1 damage to that player for each counter; the counters are then discarded.').
card_image_name('power surge'/'3ED', 'power surge').
card_uid('power surge'/'3ED', '3ED:Power Surge:power surge').
card_rarity('power surge'/'3ED', 'Rare').
card_artist('power surge'/'3ED', 'Douglas Shuler').
card_multiverse_id('power surge'/'3ED', '1311').

card_in_set('primal clay', '3ED').
card_original_type('primal clay'/'3ED', 'Artifact Creature').
card_original_text('primal clay'/'3ED', 'When you cast Primal Clay, you must choose whether to make it a 1/6 wall, a 3/3 creature, or a 2/2 flying creature. Primal Clay then remains in this form until altered by another card or removed from play.').
card_image_name('primal clay'/'3ED', 'primal clay').
card_uid('primal clay'/'3ED', '3ED:Primal Clay:primal clay').
card_rarity('primal clay'/'3ED', 'Rare').
card_artist('primal clay'/'3ED', 'Kaja Foglio').
card_multiverse_id('primal clay'/'3ED', '1132').

card_in_set('prodigal sorcerer', '3ED').
card_original_type('prodigal sorcerer'/'3ED', 'Summon — Wizard').
card_original_text('prodigal sorcerer'/'3ED', '{T}: Do 1 damage to any target.').
card_image_name('prodigal sorcerer'/'3ED', 'prodigal sorcerer').
card_uid('prodigal sorcerer'/'3ED', '3ED:Prodigal Sorcerer:prodigal sorcerer').
card_rarity('prodigal sorcerer'/'3ED', 'Common').
card_artist('prodigal sorcerer'/'3ED', 'Douglas Shuler').
card_flavor_text('prodigal sorcerer'/'3ED', 'Occasionally a member of the Institute of Arcane Study acquires a taste for worldly pleasures. Seldom do they have trouble finding employment.').
card_multiverse_id('prodigal sorcerer'/'3ED', '1217').

card_in_set('psychic venom', '3ED').
card_original_type('psychic venom'/'3ED', 'Enchant Land').
card_original_text('psychic venom'/'3ED', 'Whenever target land becomes tapped, Psychic Venom does 2 damage to target land\'s controller.').
card_image_name('psychic venom'/'3ED', 'psychic venom').
card_uid('psychic venom'/'3ED', '3ED:Psychic Venom:psychic venom').
card_rarity('psychic venom'/'3ED', 'Common').
card_artist('psychic venom'/'3ED', 'Brian Snõddy').
card_multiverse_id('psychic venom'/'3ED', '1218').

card_in_set('purelace', '3ED').
card_original_type('purelace'/'3ED', 'Interrupt').
card_original_text('purelace'/'3ED', 'Changes the color of one card either being played or already in play to white. Cost to cast, tap, maintain, or use a special ability of target card remains entirely unchanged.').
card_image_name('purelace'/'3ED', 'purelace').
card_uid('purelace'/'3ED', '3ED:Purelace:purelace').
card_rarity('purelace'/'3ED', 'Rare').
card_artist('purelace'/'3ED', 'Sandra Everingham').
card_multiverse_id('purelace'/'3ED', '1358').

card_in_set('raise dead', '3ED').
card_original_type('raise dead'/'3ED', 'Sorcery').
card_original_text('raise dead'/'3ED', 'Bring one creature from your graveyard to your hand.').
card_image_name('raise dead'/'3ED', 'raise dead').
card_uid('raise dead'/'3ED', '3ED:Raise Dead:raise dead').
card_rarity('raise dead'/'3ED', 'Common').
card_artist('raise dead'/'3ED', 'Jeff A. Menges').
card_multiverse_id('raise dead'/'3ED', '1174').

card_in_set('reconstruction', '3ED').
card_original_type('reconstruction'/'3ED', 'Sorcery').
card_original_text('reconstruction'/'3ED', 'Bring one artifact from your graveyard to your hand.').
card_image_name('reconstruction'/'3ED', 'reconstruction').
card_uid('reconstruction'/'3ED', '3ED:Reconstruction:reconstruction').
card_rarity('reconstruction'/'3ED', 'Common').
card_artist('reconstruction'/'3ED', 'Anson Maddocks').
card_flavor_text('reconstruction'/'3ED', 'Tedious research made the Sages of the College of Lat-Nam adept in repairing broken artifacts.').
card_multiverse_id('reconstruction'/'3ED', '1219').

card_in_set('red elemental blast', '3ED').
card_original_type('red elemental blast'/'3ED', 'Interrupt').
card_original_text('red elemental blast'/'3ED', 'Counters a blue spell being cast or destroys a blue card in play.').
card_image_name('red elemental blast'/'3ED', 'red elemental blast').
card_uid('red elemental blast'/'3ED', '3ED:Red Elemental Blast:red elemental blast').
card_rarity('red elemental blast'/'3ED', 'Common').
card_artist('red elemental blast'/'3ED', 'Richard Thomas').
card_multiverse_id('red elemental blast'/'3ED', '1312').

card_in_set('red ward', '3ED').
card_original_type('red ward'/'3ED', 'Enchant Creature').
card_original_text('red ward'/'3ED', 'Target creature gains protection from red.').
card_image_name('red ward'/'3ED', 'red ward').
card_uid('red ward'/'3ED', '3ED:Red Ward:red ward').
card_rarity('red ward'/'3ED', 'Uncommon').
card_artist('red ward'/'3ED', 'Dan Frazier').
card_multiverse_id('red ward'/'3ED', '1359').

card_in_set('regeneration', '3ED').
card_original_type('regeneration'/'3ED', 'Enchant Creature').
card_original_text('regeneration'/'3ED', '{G} Target creature regenerates.').
card_image_name('regeneration'/'3ED', 'regeneration').
card_uid('regeneration'/'3ED', '3ED:Regeneration:regeneration').
card_rarity('regeneration'/'3ED', 'Common').
card_artist('regeneration'/'3ED', 'Quinton Hoover').
card_multiverse_id('regeneration'/'3ED', '1262').

card_in_set('regrowth', '3ED').
card_original_type('regrowth'/'3ED', 'Sorcery').
card_original_text('regrowth'/'3ED', 'Bring any card from your graveyard to your hand.').
card_image_name('regrowth'/'3ED', 'regrowth').
card_uid('regrowth'/'3ED', '3ED:Regrowth:regrowth').
card_rarity('regrowth'/'3ED', 'Uncommon').
card_artist('regrowth'/'3ED', 'Dameon Willich').
card_multiverse_id('regrowth'/'3ED', '1263').

card_in_set('resurrection', '3ED').
card_original_type('resurrection'/'3ED', 'Sorcery').
card_original_text('resurrection'/'3ED', 'Take a creature from your graveyard and put it directly into play. Treat this creature as though it were just summoned.').
card_image_name('resurrection'/'3ED', 'resurrection').
card_uid('resurrection'/'3ED', '3ED:Resurrection:resurrection').
card_rarity('resurrection'/'3ED', 'Uncommon').
card_artist('resurrection'/'3ED', 'Dan Frazier').
card_multiverse_id('resurrection'/'3ED', '1360').

card_in_set('reverse damage', '3ED').
card_original_type('reverse damage'/'3ED', 'Instant').
card_original_text('reverse damage'/'3ED', 'All damage you have taken from any one source this turn is added to your life total instead of subtracted from it.').
card_image_name('reverse damage'/'3ED', 'reverse damage').
card_uid('reverse damage'/'3ED', '3ED:Reverse Damage:reverse damage').
card_rarity('reverse damage'/'3ED', 'Rare').
card_artist('reverse damage'/'3ED', 'Dameon Willich').
card_multiverse_id('reverse damage'/'3ED', '1361').

card_in_set('reverse polarity', '3ED').
card_original_type('reverse polarity'/'3ED', 'Instant').
card_original_text('reverse polarity'/'3ED', 'All damage done to you by artifacts so far this turn is retroactively added to your life total instead of subtracted. Further damage this turn is treated normally.').
card_image_name('reverse polarity'/'3ED', 'reverse polarity').
card_uid('reverse polarity'/'3ED', '3ED:Reverse Polarity:reverse polarity').
card_rarity('reverse polarity'/'3ED', 'Uncommon').
card_artist('reverse polarity'/'3ED', 'Justin Hampton').
card_multiverse_id('reverse polarity'/'3ED', '1362').

card_in_set('righteousness', '3ED').
card_original_type('righteousness'/'3ED', 'Instant').
card_original_text('righteousness'/'3ED', 'Target defending creature gains +7/+7 until end of turn.').
card_image_name('righteousness'/'3ED', 'righteousness').
card_uid('righteousness'/'3ED', '3ED:Righteousness:righteousness').
card_rarity('righteousness'/'3ED', 'Rare').
card_artist('righteousness'/'3ED', 'Douglas Shuler').
card_multiverse_id('righteousness'/'3ED', '1363').

card_in_set('roc of kher ridges', '3ED').
card_original_type('roc of kher ridges'/'3ED', 'Summon — Roc').
card_original_text('roc of kher ridges'/'3ED', 'Flying').
card_image_name('roc of kher ridges'/'3ED', 'roc of kher ridges').
card_uid('roc of kher ridges'/'3ED', '3ED:Roc of Kher Ridges:roc of kher ridges').
card_rarity('roc of kher ridges'/'3ED', 'Rare').
card_artist('roc of kher ridges'/'3ED', 'Andi Rusu').
card_flavor_text('roc of kher ridges'/'3ED', 'We encountered a valley topped with immense boulders and eerie rock formations. Suddenly one of these boulders toppled from its perch and sprouted gargantuan wings, casting a shadow of darkness and sending us fleeing in terror.').
card_multiverse_id('roc of kher ridges'/'3ED', '1313').

card_in_set('rock hydra', '3ED').
card_original_type('rock hydra'/'3ED', 'Summon — Hydra').
card_original_text('rock hydra'/'3ED', 'Put X +1/+1 counters (heads) on Hydra. Each point of damage Hydra suffers kills one head unless controller spends {R} per head. During controller\'s upkeep, new heads may be grown for {R}{R}{R} apiece.').
card_image_name('rock hydra'/'3ED', 'rock hydra').
card_uid('rock hydra'/'3ED', '3ED:Rock Hydra:rock hydra').
card_rarity('rock hydra'/'3ED', 'Rare').
card_artist('rock hydra'/'3ED', 'Jeff A. Menges').
card_multiverse_id('rock hydra'/'3ED', '1314').

card_in_set('rocket launcher', '3ED').
card_original_type('rocket launcher'/'3ED', 'Artifact').
card_original_text('rocket launcher'/'3ED', '{2}: Do 1 damage to any target. Rocket Launcher may not be used until it begins a turn in play on your side. If it is used, Rocket Launcher is destroyed at end of turn.').
card_image_name('rocket launcher'/'3ED', 'rocket launcher').
card_uid('rocket launcher'/'3ED', '3ED:Rocket Launcher:rocket launcher').
card_rarity('rocket launcher'/'3ED', 'Rare').
card_artist('rocket launcher'/'3ED', 'Pete Venters').
card_flavor_text('rocket launcher'/'3ED', 'What these devices lacked in subtlety, they made up in strength.').
card_multiverse_id('rocket launcher'/'3ED', '1133').

card_in_set('rod of ruin', '3ED').
card_original_type('rod of ruin'/'3ED', 'Artifact').
card_original_text('rod of ruin'/'3ED', '{3}, {T}: Rod of Ruin does 1 damage to any target.').
card_image_name('rod of ruin'/'3ED', 'rod of ruin').
card_uid('rod of ruin'/'3ED', '3ED:Rod of Ruin:rod of ruin').
card_rarity('rod of ruin'/'3ED', 'Uncommon').
card_artist('rod of ruin'/'3ED', 'Christopher Rush').
card_multiverse_id('rod of ruin'/'3ED', '1134').

card_in_set('royal assassin', '3ED').
card_original_type('royal assassin'/'3ED', 'Summon — Assassin').
card_original_text('royal assassin'/'3ED', '{T}: Destroy any tapped creature.').
card_image_name('royal assassin'/'3ED', 'royal assassin').
card_uid('royal assassin'/'3ED', '3ED:Royal Assassin:royal assassin').
card_rarity('royal assassin'/'3ED', 'Rare').
card_artist('royal assassin'/'3ED', 'Tom Wänerstrand').
card_flavor_text('royal assassin'/'3ED', 'Trained in the arts of stealth, the royal assassins choose their victims carefully, relying on timing and precision rather than brute force.').
card_multiverse_id('royal assassin'/'3ED', '1175').

card_in_set('sacrifice', '3ED').
card_original_type('sacrifice'/'3ED', 'Interrupt').
card_original_text('sacrifice'/'3ED', 'Sacrifice one of your creatures to add to your mana pool a number of black mana equal to that creature\'s casting cost.').
card_image_name('sacrifice'/'3ED', 'sacrifice').
card_uid('sacrifice'/'3ED', '3ED:Sacrifice:sacrifice').
card_rarity('sacrifice'/'3ED', 'Uncommon').
card_artist('sacrifice'/'3ED', 'Dan Frazier').
card_multiverse_id('sacrifice'/'3ED', '1176').

card_in_set('samite healer', '3ED').
card_original_type('samite healer'/'3ED', 'Summon — Cleric').
card_original_text('samite healer'/'3ED', '{T}: Prevent 1 damage to any target.').
card_image_name('samite healer'/'3ED', 'samite healer').
card_uid('samite healer'/'3ED', '3ED:Samite Healer:samite healer').
card_rarity('samite healer'/'3ED', 'Common').
card_artist('samite healer'/'3ED', 'Tom Wänerstrand').
card_flavor_text('samite healer'/'3ED', 'Healers ultimately acquire the divine gifts of spiritual and physical wholeness. The most devout are also granted the ability to pass physical wholeness on to others.').
card_multiverse_id('samite healer'/'3ED', '1364').

card_in_set('savannah', '3ED').
card_original_type('savannah'/'3ED', 'Land').
card_original_text('savannah'/'3ED', '{T}: Add either {W} or {G} to your mana pool.\nCounts as both plains and forest and is affected by spells that affect either. If a spell destroys one of these land types, this card is destroyed; if a spell alters one of these land types, the other land type is unaffected.').
card_image_name('savannah'/'3ED', 'savannah').
card_uid('savannah'/'3ED', '3ED:Savannah:savannah').
card_rarity('savannah'/'3ED', 'Rare').
card_artist('savannah'/'3ED', 'Rob Alexander').
card_multiverse_id('savannah'/'3ED', '1379').

card_in_set('savannah lions', '3ED').
card_original_type('savannah lions'/'3ED', 'Summon — Lions').
card_original_text('savannah lions'/'3ED', '').
card_image_name('savannah lions'/'3ED', 'savannah lions').
card_uid('savannah lions'/'3ED', '3ED:Savannah Lions:savannah lions').
card_rarity('savannah lions'/'3ED', 'Rare').
card_artist('savannah lions'/'3ED', 'Daniel Gelon').
card_flavor_text('savannah lions'/'3ED', 'The traditional kings of the jungle command a healthy respect in other climates as well. Relying mainly on their stealth and speed, Savannah Lions can take a victim by surprise, even in the endless, flat plains of their homeland.').
card_multiverse_id('savannah lions'/'3ED', '1365').

card_in_set('scathe zombies', '3ED').
card_original_type('scathe zombies'/'3ED', 'Summon — Zombies').
card_original_text('scathe zombies'/'3ED', '').
card_image_name('scathe zombies'/'3ED', 'scathe zombies').
card_uid('scathe zombies'/'3ED', '3ED:Scathe Zombies:scathe zombies').
card_rarity('scathe zombies'/'3ED', 'Common').
card_artist('scathe zombies'/'3ED', 'Jesper Myrfors').
card_flavor_text('scathe zombies'/'3ED', '\"They groaned, they stirred, they all uprose,/ Nor spake, nor moved their eyes;/ It had been strange, even in a dream,/ To have seen those dead men rise.\"/ —Samuel Coleridge, \"The Rime of the Ancient Mariner\"').
card_multiverse_id('scathe zombies'/'3ED', '1177').

card_in_set('scavenging ghoul', '3ED').
card_original_type('scavenging ghoul'/'3ED', 'Summon — Ghoul').
card_original_text('scavenging ghoul'/'3ED', 'At the end of each turn, put one counter on Ghoul for each other creature that was placed in the graveyard during the turn. If Ghoul takes lethal damage, you may use a counter to regenerate it; counters remain until used.').
card_image_name('scavenging ghoul'/'3ED', 'scavenging ghoul').
card_uid('scavenging ghoul'/'3ED', '3ED:Scavenging Ghoul:scavenging ghoul').
card_rarity('scavenging ghoul'/'3ED', 'Uncommon').
card_artist('scavenging ghoul'/'3ED', 'Jeff A. Menges').
card_multiverse_id('scavenging ghoul'/'3ED', '1178').

card_in_set('scrubland', '3ED').
card_original_type('scrubland'/'3ED', 'Land').
card_original_text('scrubland'/'3ED', '{T}: Add either {W} or {B} to your mana pool.\nCounts as both plains and swamp and is affected by spells that affect either. If a spell destroys one of these land types, this card is destroyed; if a spell alters one of these land types, the other land type is unaffected.').
card_image_name('scrubland'/'3ED', 'scrubland').
card_uid('scrubland'/'3ED', '3ED:Scrubland:scrubland').
card_rarity('scrubland'/'3ED', 'Rare').
card_artist('scrubland'/'3ED', 'Jesper Myrfors').
card_multiverse_id('scrubland'/'3ED', '1380').

card_in_set('scryb sprites', '3ED').
card_original_type('scryb sprites'/'3ED', 'Summon — Faeries').
card_original_text('scryb sprites'/'3ED', 'Flying').
card_image_name('scryb sprites'/'3ED', 'scryb sprites').
card_uid('scryb sprites'/'3ED', '3ED:Scryb Sprites:scryb sprites').
card_rarity('scryb sprites'/'3ED', 'Common').
card_artist('scryb sprites'/'3ED', 'Amy Weber').
card_flavor_text('scryb sprites'/'3ED', 'The only sound was the gentle clicking of the Faeries\' wings. Then those intruders who were still standing turned and fled. One thing was certain: they didn\'t think the Scryb were very funny anymore.').
card_multiverse_id('scryb sprites'/'3ED', '1264').

card_in_set('sea serpent', '3ED').
card_original_type('sea serpent'/'3ED', 'Summon — Serpent').
card_original_text('sea serpent'/'3ED', 'Serpent cannot attack unless opponent has islands in play. Serpent is buried immediately if at any time controller has no islands in play.').
card_image_name('sea serpent'/'3ED', 'sea serpent').
card_uid('sea serpent'/'3ED', '3ED:Sea Serpent:sea serpent').
card_rarity('sea serpent'/'3ED', 'Common').
card_artist('sea serpent'/'3ED', 'Jeff A. Menges').
card_flavor_text('sea serpent'/'3ED', 'Legend has it that Serpents used to be bigger, but how could that be?').
card_multiverse_id('sea serpent'/'3ED', '1220').

card_in_set('sedge troll', '3ED').
card_original_type('sedge troll'/'3ED', 'Summon — Troll').
card_original_text('sedge troll'/'3ED', '{B} Regenerates.\nWhile controller has swamps in play, Sedge Troll gains +1/+1.').
card_image_name('sedge troll'/'3ED', 'sedge troll').
card_uid('sedge troll'/'3ED', '3ED:Sedge Troll:sedge troll').
card_rarity('sedge troll'/'3ED', 'Rare').
card_artist('sedge troll'/'3ED', 'Dan Frazier').
card_flavor_text('sedge troll'/'3ED', 'The stench in the hovel was overpowering; something loathsome was cooking. Occasionally something surfaced in the thick paste, but my host would casually push it down before I could make out what it was.').
card_multiverse_id('sedge troll'/'3ED', '1315').

card_in_set('sengir vampire', '3ED').
card_original_type('sengir vampire'/'3ED', 'Summon — Vampire').
card_original_text('sengir vampire'/'3ED', 'Flying\nVampire gets a +1/+1 counter each time a creature dies during a turn in which Vampire damaged it.').
card_image_name('sengir vampire'/'3ED', 'sengir vampire').
card_uid('sengir vampire'/'3ED', '3ED:Sengir Vampire:sengir vampire').
card_rarity('sengir vampire'/'3ED', 'Uncommon').
card_artist('sengir vampire'/'3ED', 'Anson Maddocks').
card_multiverse_id('sengir vampire'/'3ED', '1179').

card_in_set('serendib efreet', '3ED').
card_original_type('serendib efreet'/'3ED', 'Summon — Efreet').
card_original_text('serendib efreet'/'3ED', 'Flying\nSerendib Efreet does 1 damage to you during your upkeep.').
card_image_name('serendib efreet'/'3ED', 'serendib efreet').
card_uid('serendib efreet'/'3ED', '3ED:Serendib Efreet:serendib efreet').
card_rarity('serendib efreet'/'3ED', 'Rare').
card_artist('serendib efreet'/'3ED', 'Jesper Myrfors').
card_multiverse_id('serendib efreet'/'3ED', '1221').

card_in_set('serra angel', '3ED').
card_original_type('serra angel'/'3ED', 'Summon — Angel').
card_original_text('serra angel'/'3ED', 'Flying\nAttacking does not cause Serra Angel to tap.').
card_image_name('serra angel'/'3ED', 'serra angel').
card_uid('serra angel'/'3ED', '3ED:Serra Angel:serra angel').
card_rarity('serra angel'/'3ED', 'Uncommon').
card_artist('serra angel'/'3ED', 'Douglas Shuler').
card_flavor_text('serra angel'/'3ED', 'Born with wings of light and a sword of faith, this heavenly incarnation embodies both fury and purity.').
card_multiverse_id('serra angel'/'3ED', '1366').

card_in_set('shanodin dryads', '3ED').
card_original_type('shanodin dryads'/'3ED', 'Summon — Nymphs').
card_original_text('shanodin dryads'/'3ED', 'Forestwalk').
card_image_name('shanodin dryads'/'3ED', 'shanodin dryads').
card_uid('shanodin dryads'/'3ED', '3ED:Shanodin Dryads:shanodin dryads').
card_rarity('shanodin dryads'/'3ED', 'Common').
card_artist('shanodin dryads'/'3ED', 'Anson Maddocks').
card_flavor_text('shanodin dryads'/'3ED', 'Moving without sound, swift figures pass through branches and undergrowth completely unhindered. One with the trees around them, the Dryads of Shanodin Forest are seen only when they wish to be.').
card_multiverse_id('shanodin dryads'/'3ED', '1265').

card_in_set('shatter', '3ED').
card_original_type('shatter'/'3ED', 'Instant').
card_original_text('shatter'/'3ED', 'Shatter destroys target artifact.').
card_image_name('shatter'/'3ED', 'shatter').
card_uid('shatter'/'3ED', '3ED:Shatter:shatter').
card_rarity('shatter'/'3ED', 'Common').
card_artist('shatter'/'3ED', 'Amy Weber').
card_multiverse_id('shatter'/'3ED', '1316').

card_in_set('shatterstorm', '3ED').
card_original_type('shatterstorm'/'3ED', 'Sorcery').
card_original_text('shatterstorm'/'3ED', 'All artifacts in play are buried.').
card_image_name('shatterstorm'/'3ED', 'shatterstorm').
card_uid('shatterstorm'/'3ED', '3ED:Shatterstorm:shatterstorm').
card_rarity('shatterstorm'/'3ED', 'Uncommon').
card_artist('shatterstorm'/'3ED', 'Mark Poole').
card_flavor_text('shatterstorm'/'3ED', 'Chains of leaping fire and sizzling lightning laid waste the artificers\' handiwork, sparing not a single device.').
card_multiverse_id('shatterstorm'/'3ED', '1317').

card_in_set('shivan dragon', '3ED').
card_original_type('shivan dragon'/'3ED', 'Summon — Dragon').
card_original_text('shivan dragon'/'3ED', 'Flying, {R} +1/+0').
card_image_name('shivan dragon'/'3ED', 'shivan dragon').
card_uid('shivan dragon'/'3ED', '3ED:Shivan Dragon:shivan dragon').
card_rarity('shivan dragon'/'3ED', 'Rare').
card_artist('shivan dragon'/'3ED', 'Melissa A. Benson').
card_flavor_text('shivan dragon'/'3ED', 'While it\'s true most Dragons are cruel, the Shivan Dragon seems to take particular glee in the misery of others, often tormenting its victims much like a cat plays with a mouse before delivering the final blow.').
card_multiverse_id('shivan dragon'/'3ED', '1318').

card_in_set('simulacrum', '3ED').
card_original_type('simulacrum'/'3ED', 'Instant').
card_original_text('simulacrum'/'3ED', 'All damage done to you so far this turn is instead retroactively applied to one of your creatures in play. Even if there\'s more than enough damage to kill the creature, you don\'t suffer any of it. Further damage this turn is treated normally.').
card_image_name('simulacrum'/'3ED', 'simulacrum').
card_uid('simulacrum'/'3ED', '3ED:Simulacrum:simulacrum').
card_rarity('simulacrum'/'3ED', 'Uncommon').
card_artist('simulacrum'/'3ED', 'Mark Poole').
card_multiverse_id('simulacrum'/'3ED', '1180').

card_in_set('siren\'s call', '3ED').
card_original_type('siren\'s call'/'3ED', 'Instant').
card_original_text('siren\'s call'/'3ED', 'All of opponent\'s creatures that can attack must do so. Any non-wall creatures that cannot attack are killed at end of turn. Can be played only during opponent\'s turn, before opponent\'s attack. Creatures summoned this turn are unaffected by Siren\'s Call.').
card_image_name('siren\'s call'/'3ED', 'siren\'s call').
card_uid('siren\'s call'/'3ED', '3ED:Siren\'s Call:siren\'s call').
card_rarity('siren\'s call'/'3ED', 'Uncommon').
card_artist('siren\'s call'/'3ED', 'Anson Maddocks').
card_multiverse_id('siren\'s call'/'3ED', '1222').

card_in_set('sleight of mind', '3ED').
card_original_type('sleight of mind'/'3ED', 'Interrupt').
card_original_text('sleight of mind'/'3ED', 'Change the text of any card being played or already in play by replacing one color word with another. For example, you can change \"Counters red spells\" to \"Counters black spells.\" Sleight of Mind cannot change mana symbols.').
card_image_name('sleight of mind'/'3ED', 'sleight of mind').
card_uid('sleight of mind'/'3ED', '3ED:Sleight of Mind:sleight of mind').
card_rarity('sleight of mind'/'3ED', 'Rare').
card_artist('sleight of mind'/'3ED', 'Mark Poole').
card_multiverse_id('sleight of mind'/'3ED', '1223').

card_in_set('smoke', '3ED').
card_original_type('smoke'/'3ED', 'Enchantment').
card_original_text('smoke'/'3ED', 'Each player can only untap one creature during his or her untap phase.').
card_image_name('smoke'/'3ED', 'smoke').
card_uid('smoke'/'3ED', '3ED:Smoke:smoke').
card_rarity('smoke'/'3ED', 'Rare').
card_artist('smoke'/'3ED', 'Jesper Myrfors').
card_multiverse_id('smoke'/'3ED', '1319').

card_in_set('sol ring', '3ED').
card_original_type('sol ring'/'3ED', 'Artifact').
card_original_text('sol ring'/'3ED', '{T}: Add 2 colorless mana to your mana pool. This ability is played as an interrupt.').
card_image_name('sol ring'/'3ED', 'sol ring').
card_uid('sol ring'/'3ED', '3ED:Sol Ring:sol ring').
card_rarity('sol ring'/'3ED', 'Uncommon').
card_artist('sol ring'/'3ED', 'Mark Tedin').
card_multiverse_id('sol ring'/'3ED', '1135').

card_in_set('sorceress queen', '3ED').
card_original_type('sorceress queen'/'3ED', 'Summon — Sorceress').
card_original_text('sorceress queen'/'3ED', '{T}: Make another creature 0/2 until end of turn. Treat this exactly as if the numbers in the lower right of the target card were 0/2. All special characteristics and enchantments on the creature are unaffected.').
card_image_name('sorceress queen'/'3ED', 'sorceress queen').
card_uid('sorceress queen'/'3ED', '3ED:Sorceress Queen:sorceress queen').
card_rarity('sorceress queen'/'3ED', 'Rare').
card_artist('sorceress queen'/'3ED', 'Kaja Foglio').
card_multiverse_id('sorceress queen'/'3ED', '1181').

card_in_set('soul net', '3ED').
card_original_type('soul net'/'3ED', 'Artifact').
card_original_text('soul net'/'3ED', '{1}: You gain 1 life every time a creature is placed in the graveyard. Can only give 1 life each time a creature is placed in the graveyard.').
card_image_name('soul net'/'3ED', 'soul net').
card_uid('soul net'/'3ED', '3ED:Soul Net:soul net').
card_rarity('soul net'/'3ED', 'Uncommon').
card_artist('soul net'/'3ED', 'Dameon Willich').
card_multiverse_id('soul net'/'3ED', '1136').

card_in_set('spell blast', '3ED').
card_original_type('spell blast'/'3ED', 'Interrupt').
card_original_text('spell blast'/'3ED', 'Target spell is countered; X is casting cost of target spell.').
card_image_name('spell blast'/'3ED', 'spell blast').
card_uid('spell blast'/'3ED', '3ED:Spell Blast:spell blast').
card_rarity('spell blast'/'3ED', 'Common').
card_artist('spell blast'/'3ED', 'Brian Snõddy').
card_multiverse_id('spell blast'/'3ED', '1224').

card_in_set('stasis', '3ED').
card_original_type('stasis'/'3ED', 'Enchantment').
card_original_text('stasis'/'3ED', 'Players do not get an untap phase. Pay {U} during your upkeep or Stasis is destroyed; cards still do not untap until the next untap phase.').
card_image_name('stasis'/'3ED', 'stasis').
card_uid('stasis'/'3ED', '3ED:Stasis:stasis').
card_rarity('stasis'/'3ED', 'Rare').
card_artist('stasis'/'3ED', 'Fay Jones').
card_multiverse_id('stasis'/'3ED', '1225').

card_in_set('steal artifact', '3ED').
card_original_type('steal artifact'/'3ED', 'Enchant Artifact').
card_original_text('steal artifact'/'3ED', 'You control target artifact until enchantment is discarded or game ends. If target artifact was tapped when stolen, it stays tapped until you can untap it. If destroyed, target artifact is put in its owner\'s graveyard.').
card_image_name('steal artifact'/'3ED', 'steal artifact').
card_uid('steal artifact'/'3ED', '3ED:Steal Artifact:steal artifact').
card_rarity('steal artifact'/'3ED', 'Uncommon').
card_artist('steal artifact'/'3ED', 'Amy Weber').
card_multiverse_id('steal artifact'/'3ED', '1226').

card_in_set('stone giant', '3ED').
card_original_type('stone giant'/'3ED', 'Summon — Giant').
card_original_text('stone giant'/'3ED', '{T}: Make one of your own creatures a flying creature until end of turn. Target creature, which must have toughness less than Stone Giant\'s power at the time it gains flying ability, is killed at end of turn.').
card_image_name('stone giant'/'3ED', 'stone giant').
card_uid('stone giant'/'3ED', '3ED:Stone Giant:stone giant').
card_rarity('stone giant'/'3ED', 'Uncommon').
card_artist('stone giant'/'3ED', 'Dameon Willich').
card_flavor_text('stone giant'/'3ED', 'What goes up, must come down.').
card_multiverse_id('stone giant'/'3ED', '1320').

card_in_set('stone rain', '3ED').
card_original_type('stone rain'/'3ED', 'Sorcery').
card_original_text('stone rain'/'3ED', 'Destroys any one land.').
card_image_name('stone rain'/'3ED', 'stone rain').
card_uid('stone rain'/'3ED', '3ED:Stone Rain:stone rain').
card_rarity('stone rain'/'3ED', 'Common').
card_artist('stone rain'/'3ED', 'Daniel Gelon').
card_multiverse_id('stone rain'/'3ED', '1321').

card_in_set('stream of life', '3ED').
card_original_type('stream of life'/'3ED', 'Sorcery').
card_original_text('stream of life'/'3ED', 'Target player gains X life.').
card_image_name('stream of life'/'3ED', 'stream of life').
card_uid('stream of life'/'3ED', '3ED:Stream of Life:stream of life').
card_rarity('stream of life'/'3ED', 'Common').
card_artist('stream of life'/'3ED', 'Mark Poole').
card_multiverse_id('stream of life'/'3ED', '1266').

card_in_set('sunglasses of urza', '3ED').
card_original_type('sunglasses of urza'/'3ED', 'Artifact').
card_original_text('sunglasses of urza'/'3ED', 'White mana in your mana pool can be used as either white or red mana.').
card_image_name('sunglasses of urza'/'3ED', 'sunglasses of urza').
card_uid('sunglasses of urza'/'3ED', '3ED:Sunglasses of Urza:sunglasses of urza').
card_rarity('sunglasses of urza'/'3ED', 'Rare').
card_artist('sunglasses of urza'/'3ED', 'Dan Frazier').
card_multiverse_id('sunglasses of urza'/'3ED', '1137').

card_in_set('swamp', '3ED').
card_original_type('swamp'/'3ED', 'Land').
card_original_text('swamp'/'3ED', '{T}: Add {B} to your mana pool.').
card_image_name('swamp'/'3ED', 'swamp1').
card_uid('swamp'/'3ED', '3ED:Swamp:swamp1').
card_rarity('swamp'/'3ED', 'Basic Land').
card_artist('swamp'/'3ED', 'Dan Frazier').
card_multiverse_id('swamp'/'3ED', '1373').

card_in_set('swamp', '3ED').
card_original_type('swamp'/'3ED', 'Land').
card_original_text('swamp'/'3ED', '{T}: Add {B} to your mana pool.').
card_image_name('swamp'/'3ED', 'swamp2').
card_uid('swamp'/'3ED', '3ED:Swamp:swamp2').
card_rarity('swamp'/'3ED', 'Basic Land').
card_artist('swamp'/'3ED', 'Dan Frazier').
card_multiverse_id('swamp'/'3ED', '1375').

card_in_set('swamp', '3ED').
card_original_type('swamp'/'3ED', 'Land').
card_original_text('swamp'/'3ED', '{T}: Add {B} to your mana pool.').
card_image_name('swamp'/'3ED', 'swamp3').
card_uid('swamp'/'3ED', '3ED:Swamp:swamp3').
card_rarity('swamp'/'3ED', 'Basic Land').
card_artist('swamp'/'3ED', 'Dan Frazier').
card_multiverse_id('swamp'/'3ED', '1374').

card_in_set('swords to plowshares', '3ED').
card_original_type('swords to plowshares'/'3ED', 'Instant').
card_original_text('swords to plowshares'/'3ED', 'Target creature is removed from game entirely. Creature\'s controller gains life points equal to creature\'s power.').
card_image_name('swords to plowshares'/'3ED', 'swords to plowshares').
card_uid('swords to plowshares'/'3ED', '3ED:Swords to Plowshares:swords to plowshares').
card_rarity('swords to plowshares'/'3ED', 'Uncommon').
card_artist('swords to plowshares'/'3ED', 'Jeff A. Menges').
card_multiverse_id('swords to plowshares'/'3ED', '1367').

card_in_set('taiga', '3ED').
card_original_type('taiga'/'3ED', 'Land').
card_original_text('taiga'/'3ED', '{T}: Add either {G} or {R} to your mana pool.\nCounts as both forest and mountains and is affected by spells that affect either. If a spell destroys one of these land types, this card is destroyed; if a spell alters one of these land types, the other land type is unaffected.').
card_image_name('taiga'/'3ED', 'taiga').
card_uid('taiga'/'3ED', '3ED:Taiga:taiga').
card_rarity('taiga'/'3ED', 'Rare').
card_artist('taiga'/'3ED', 'Rob Alexander').
card_multiverse_id('taiga'/'3ED', '1381').

card_in_set('terror', '3ED').
card_original_type('terror'/'3ED', 'Instant').
card_original_text('terror'/'3ED', 'Buries target creature. Cannot target black creatures or artifact creatures.').
card_image_name('terror'/'3ED', 'terror').
card_uid('terror'/'3ED', '3ED:Terror:terror').
card_rarity('terror'/'3ED', 'Common').
card_artist('terror'/'3ED', 'Ron Spencer').
card_multiverse_id('terror'/'3ED', '1182').

card_in_set('the hive', '3ED').
card_original_type('the hive'/'3ED', 'Artifact').
card_original_text('the hive'/'3ED', '{5}, {T}: Creates one Giant Wasp, a 1/1 flying creature. Represent Wasps with tokens, making sure to indicate when each Wasp is tapped. Wasps can\'t attack during the turn created. Treat Wasps like artifact creatures in every way, except that they are removed from the game entirely if they ever leave play. If the Hive is destroyed, the Wasps must still be killed individually.').
card_image_name('the hive'/'3ED', 'the hive').
card_uid('the hive'/'3ED', '3ED:The Hive:the hive').
card_rarity('the hive'/'3ED', 'Rare').
card_artist('the hive'/'3ED', 'Sandra Everingham').
card_multiverse_id('the hive'/'3ED', '1138').

card_in_set('the rack', '3ED').
card_original_type('the rack'/'3ED', 'Artifact').
card_original_text('the rack'/'3ED', 'If opponent has fewer than three cards in hand during his or her upkeep, the Rack does 1 damage to opponent for each card fewer than three.').
card_image_name('the rack'/'3ED', 'the rack').
card_uid('the rack'/'3ED', '3ED:The Rack:the rack').
card_rarity('the rack'/'3ED', 'Uncommon').
card_artist('the rack'/'3ED', 'Richard Thomas').
card_flavor_text('the rack'/'3ED', 'Invented in Mishra\'s earlier days, the Rack was once his most feared creation.').
card_multiverse_id('the rack'/'3ED', '1139').

card_in_set('thicket basilisk', '3ED').
card_original_type('thicket basilisk'/'3ED', 'Summon — Basilisk').
card_original_text('thicket basilisk'/'3ED', 'Any non-wall creature blocking Basilisk is destroyed, as is any creature blocked by Basilisk. Creatures destroyed this way deal their damage before dying.').
card_image_name('thicket basilisk'/'3ED', 'thicket basilisk').
card_uid('thicket basilisk'/'3ED', '3ED:Thicket Basilisk:thicket basilisk').
card_rarity('thicket basilisk'/'3ED', 'Uncommon').
card_artist('thicket basilisk'/'3ED', 'Dan Frazier').
card_flavor_text('thicket basilisk'/'3ED', 'Moss-covered statues littered the area, a macabre monument to the Basilisk\'s power.').
card_multiverse_id('thicket basilisk'/'3ED', '1267').

card_in_set('thoughtlace', '3ED').
card_original_type('thoughtlace'/'3ED', 'Interrupt').
card_original_text('thoughtlace'/'3ED', 'Changes the color of one card either being played or already in play to blue. Cost to cast, tap, maintain, or use a special ability of target card remains entirely unchanged.').
card_image_name('thoughtlace'/'3ED', 'thoughtlace').
card_uid('thoughtlace'/'3ED', '3ED:Thoughtlace:thoughtlace').
card_rarity('thoughtlace'/'3ED', 'Rare').
card_artist('thoughtlace'/'3ED', 'Mark Poole').
card_multiverse_id('thoughtlace'/'3ED', '1227').

card_in_set('throne of bone', '3ED').
card_original_type('throne of bone'/'3ED', 'Artifact').
card_original_text('throne of bone'/'3ED', '{1}: Any black spell cast gives you 1 life. Can only give 1 life each time a black spell is cast.').
card_image_name('throne of bone'/'3ED', 'throne of bone').
card_uid('throne of bone'/'3ED', '3ED:Throne of Bone:throne of bone').
card_rarity('throne of bone'/'3ED', 'Uncommon').
card_artist('throne of bone'/'3ED', 'Anson Maddocks').
card_multiverse_id('throne of bone'/'3ED', '1140').

card_in_set('timber wolves', '3ED').
card_original_type('timber wolves'/'3ED', 'Summon — Wolves').
card_original_text('timber wolves'/'3ED', 'Bands').
card_image_name('timber wolves'/'3ED', 'timber wolves').
card_uid('timber wolves'/'3ED', '3ED:Timber Wolves:timber wolves').
card_rarity('timber wolves'/'3ED', 'Rare').
card_artist('timber wolves'/'3ED', 'Melissa A. Benson').
card_flavor_text('timber wolves'/'3ED', 'Though many think of Wolves as solitary predators, they are actually extremely social animals. During a hunt they often call to each other, which can be quite unsettling for their prey.').
card_multiverse_id('timber wolves'/'3ED', '1268').

card_in_set('titania\'s song', '3ED').
card_original_type('titania\'s song'/'3ED', 'Enchantment').
card_original_text('titania\'s song'/'3ED', 'All non-creature artifacts in play lose all their usual abilities and become artifact creatures with toughness and power both equal to their casting costs. If Titania\'s Song leaves play, artifacts return to normal just before the untap phase of the next turn.').
card_image_name('titania\'s song'/'3ED', 'titania\'s song').
card_uid('titania\'s song'/'3ED', '3ED:Titania\'s Song:titania\'s song').
card_rarity('titania\'s song'/'3ED', 'Rare').
card_artist('titania\'s song'/'3ED', 'Kerstin Kaman').
card_multiverse_id('titania\'s song'/'3ED', '1269').

card_in_set('tranquility', '3ED').
card_original_type('tranquility'/'3ED', 'Sorcery').
card_original_text('tranquility'/'3ED', 'All enchantments in play must be discarded.').
card_image_name('tranquility'/'3ED', 'tranquility').
card_uid('tranquility'/'3ED', '3ED:Tranquility:tranquility').
card_rarity('tranquility'/'3ED', 'Common').
card_artist('tranquility'/'3ED', 'Douglas Shuler').
card_multiverse_id('tranquility'/'3ED', '1270').

card_in_set('tropical island', '3ED').
card_original_type('tropical island'/'3ED', 'Land').
card_original_text('tropical island'/'3ED', '{T}: Add either {G} or {U} to your mana pool.\nCounts as both forest and islands and is affected by spells that affect either. If a spell destroys one of these land types, this card is destroyed; if a spell alters one of these land types, the other land type is unaffected.').
card_image_name('tropical island'/'3ED', 'tropical island').
card_uid('tropical island'/'3ED', '3ED:Tropical Island:tropical island').
card_rarity('tropical island'/'3ED', 'Rare').
card_artist('tropical island'/'3ED', 'Jesper Myrfors').
card_multiverse_id('tropical island'/'3ED', '1382').

card_in_set('tsunami', '3ED').
card_original_type('tsunami'/'3ED', 'Sorcery').
card_original_text('tsunami'/'3ED', 'All islands in play are destroyed.').
card_image_name('tsunami'/'3ED', 'tsunami').
card_uid('tsunami'/'3ED', '3ED:Tsunami:tsunami').
card_rarity('tsunami'/'3ED', 'Uncommon').
card_artist('tsunami'/'3ED', 'Richard Thomas').
card_multiverse_id('tsunami'/'3ED', '1271').

card_in_set('tundra', '3ED').
card_original_type('tundra'/'3ED', 'Land').
card_original_text('tundra'/'3ED', '{T}: Add either {U} or {W} to your mana pool.\nCounts as both islands and plains and is affected by spells that affect either. If a spell destroys one of these land types, this card is destroyed; if a spell alters one of these land types, the other land type is unaffected.').
card_image_name('tundra'/'3ED', 'tundra').
card_uid('tundra'/'3ED', '3ED:Tundra:tundra').
card_rarity('tundra'/'3ED', 'Rare').
card_artist('tundra'/'3ED', 'Jesper Myrfors').
card_multiverse_id('tundra'/'3ED', '1383').

card_in_set('tunnel', '3ED').
card_original_type('tunnel'/'3ED', 'Instant').
card_original_text('tunnel'/'3ED', 'Buries one wall.').
card_image_name('tunnel'/'3ED', 'tunnel').
card_uid('tunnel'/'3ED', '3ED:Tunnel:tunnel').
card_rarity('tunnel'/'3ED', 'Uncommon').
card_artist('tunnel'/'3ED', 'Dan Frazier').
card_multiverse_id('tunnel'/'3ED', '1322').

card_in_set('underground sea', '3ED').
card_original_type('underground sea'/'3ED', 'Land').
card_original_text('underground sea'/'3ED', '{T}: Add either {B} or {U} to your mana pool.\nCounts as both swamp and islands and is affected by spells that affect either. If a spell destroys one of these land types, this card is destroyed; if a spell alters one of these land types, the other land type is unaffected.').
card_image_name('underground sea'/'3ED', 'underground sea').
card_uid('underground sea'/'3ED', '3ED:Underground Sea:underground sea').
card_rarity('underground sea'/'3ED', 'Rare').
card_artist('underground sea'/'3ED', 'Rob Alexander').
card_multiverse_id('underground sea'/'3ED', '1384').

card_in_set('unholy strength', '3ED').
card_original_type('unholy strength'/'3ED', 'Enchant Creature').
card_original_text('unholy strength'/'3ED', 'Target creature gains +2/+1.').
card_image_name('unholy strength'/'3ED', 'unholy strength').
card_uid('unholy strength'/'3ED', '3ED:Unholy Strength:unholy strength').
card_rarity('unholy strength'/'3ED', 'Common').
card_artist('unholy strength'/'3ED', 'Douglas Shuler').
card_multiverse_id('unholy strength'/'3ED', '1183').

card_in_set('unstable mutation', '3ED').
card_original_type('unstable mutation'/'3ED', 'Enchant Creature').
card_original_text('unstable mutation'/'3ED', 'Target creature gains +3/+3. During the upkeep phase of each of its controller\'s turns, put a -1/-1 counter on the creature. These counters remain even if this enchantment is removed before the creature dies.').
card_image_name('unstable mutation'/'3ED', 'unstable mutation').
card_uid('unstable mutation'/'3ED', '3ED:Unstable Mutation:unstable mutation').
card_rarity('unstable mutation'/'3ED', 'Common').
card_artist('unstable mutation'/'3ED', 'Douglas Shuler').
card_multiverse_id('unstable mutation'/'3ED', '1228').

card_in_set('unsummon', '3ED').
card_original_type('unsummon'/'3ED', 'Instant').
card_original_text('unsummon'/'3ED', 'Return target creature to owner\'s hand; enchantments on target creature are discarded.').
card_image_name('unsummon'/'3ED', 'unsummon').
card_uid('unsummon'/'3ED', '3ED:Unsummon:unsummon').
card_rarity('unsummon'/'3ED', 'Common').
card_artist('unsummon'/'3ED', 'Douglas Shuler').
card_multiverse_id('unsummon'/'3ED', '1229').

card_in_set('uthden troll', '3ED').
card_original_type('uthden troll'/'3ED', 'Summon — Troll').
card_original_text('uthden troll'/'3ED', '{R} Regenerates.').
card_image_name('uthden troll'/'3ED', 'uthden troll').
card_uid('uthden troll'/'3ED', '3ED:Uthden Troll:uthden troll').
card_rarity('uthden troll'/'3ED', 'Uncommon').
card_artist('uthden troll'/'3ED', 'Douglas Shuler').
card_flavor_text('uthden troll'/'3ED', '\"Oi oi oi, me gotta hurt in \'ere,\nOi oi oi, me smell a ting is near,\nGonna bosh \'n gonna nosh \'n da\nhurt\'ll disappear.\"\n—Traditional').
card_multiverse_id('uthden troll'/'3ED', '1323').

card_in_set('verduran enchantress', '3ED').
card_original_type('verduran enchantress'/'3ED', 'Summon — Enchantress').
card_original_text('verduran enchantress'/'3ED', 'While Enchantress is in play, you may immediately draw a card from your library each time you cast an enchantment.').
card_image_name('verduran enchantress'/'3ED', 'verduran enchantress').
card_uid('verduran enchantress'/'3ED', '3ED:Verduran Enchantress:verduran enchantress').
card_rarity('verduran enchantress'/'3ED', 'Rare').
card_artist('verduran enchantress'/'3ED', 'Kev Brockschmidt').
card_flavor_text('verduran enchantress'/'3ED', 'Some say magic was first practiced by women, who have always felt strong ties to the land.').
card_multiverse_id('verduran enchantress'/'3ED', '1272').

card_in_set('vesuvan doppelganger', '3ED').
card_original_type('vesuvan doppelganger'/'3ED', 'Summon — Doppelganger').
card_original_text('vesuvan doppelganger'/'3ED', 'Upon summoning, Doppelganger acquires all characteristics except color of any one creature in play on either side; any creature enchantments on the original creature are not copied. During controller\'s upkeep, Doppelganger may take on the characteristics of a different creature in play instead. Doppelganger may continue to copy a creature even after that creature leaves play, but if it switches it won\'t be able to switch back.').
card_image_name('vesuvan doppelganger'/'3ED', 'vesuvan doppelganger').
card_uid('vesuvan doppelganger'/'3ED', '3ED:Vesuvan Doppelganger:vesuvan doppelganger').
card_rarity('vesuvan doppelganger'/'3ED', 'Rare').
card_artist('vesuvan doppelganger'/'3ED', 'Quinton Hoover').
card_multiverse_id('vesuvan doppelganger'/'3ED', '1230').

card_in_set('veteran bodyguard', '3ED').
card_original_type('veteran bodyguard'/'3ED', 'Summon — Bodyguard').
card_original_text('veteran bodyguard'/'3ED', 'Unless Bodyguard is tapped, any damage done to you by unblocked creatures is done instead to Bodyguard. You may not take this damage yourself, though you can prevent it if possible. No more than one Bodyguard of your choice can take damage for you in this manner each turn.').
card_image_name('veteran bodyguard'/'3ED', 'veteran bodyguard').
card_uid('veteran bodyguard'/'3ED', '3ED:Veteran Bodyguard:veteran bodyguard').
card_rarity('veteran bodyguard'/'3ED', 'Rare').
card_artist('veteran bodyguard'/'3ED', 'Douglas Shuler').
card_flavor_text('veteran bodyguard'/'3ED', 'Good bodyguards are hard to find, mainly because they don\'t live long.').
card_multiverse_id('veteran bodyguard'/'3ED', '1368').

card_in_set('volcanic eruption', '3ED').
card_original_type('volcanic eruption'/'3ED', 'Sorcery').
card_original_text('volcanic eruption'/'3ED', 'Destroys X mountains of your choice, and does 1 damage to each player and each creature in play for each mountain destroyed.').
card_image_name('volcanic eruption'/'3ED', 'volcanic eruption').
card_uid('volcanic eruption'/'3ED', '3ED:Volcanic Eruption:volcanic eruption').
card_rarity('volcanic eruption'/'3ED', 'Rare').
card_artist('volcanic eruption'/'3ED', 'Douglas Shuler').
card_multiverse_id('volcanic eruption'/'3ED', '1231').

card_in_set('volcanic island', '3ED').
card_original_type('volcanic island'/'3ED', 'Land').
card_original_text('volcanic island'/'3ED', '{T}: Add either {U} or {R} to your mana pool.\nCounts as both islands and mountains and is affected by spells that affect either. If a spell destroys one of these land types, this card is destroyed; if a spell alters one of these land types, the other land type is unaffected.').
card_image_name('volcanic island'/'3ED', 'volcanic island').
card_uid('volcanic island'/'3ED', '3ED:Volcanic Island:volcanic island').
card_rarity('volcanic island'/'3ED', 'Rare').
card_artist('volcanic island'/'3ED', 'Brian Snõddy').
card_multiverse_id('volcanic island'/'3ED', '1385').

card_in_set('wall of air', '3ED').
card_original_type('wall of air'/'3ED', 'Summon — Wall').
card_original_text('wall of air'/'3ED', 'Flying').
card_image_name('wall of air'/'3ED', 'wall of air').
card_uid('wall of air'/'3ED', '3ED:Wall of Air:wall of air').
card_rarity('wall of air'/'3ED', 'Uncommon').
card_artist('wall of air'/'3ED', 'Richard Thomas').
card_flavor_text('wall of air'/'3ED', '\"This ‘standing windstorm\' can hold us off indefinitely? Ridiculous!\" Saying nothing, she put a pinch of salt on the table. With a bang she clapped her hands, and the salt disappeared, blown away.').
card_multiverse_id('wall of air'/'3ED', '1232').

card_in_set('wall of bone', '3ED').
card_original_type('wall of bone'/'3ED', 'Summon — Wall').
card_original_text('wall of bone'/'3ED', '{B} Regenerates.').
card_image_name('wall of bone'/'3ED', 'wall of bone').
card_uid('wall of bone'/'3ED', '3ED:Wall of Bone:wall of bone').
card_rarity('wall of bone'/'3ED', 'Uncommon').
card_artist('wall of bone'/'3ED', 'Anson Maddocks').
card_flavor_text('wall of bone'/'3ED', 'The Wall of Bone is said to be an aspect of the Great Wall in Hel, where the bones of all sinners wait for Ragnarok, when Hela will call them forth for the final battle.').
card_multiverse_id('wall of bone'/'3ED', '1184').

card_in_set('wall of brambles', '3ED').
card_original_type('wall of brambles'/'3ED', 'Summon — Wall').
card_original_text('wall of brambles'/'3ED', '{G} Regenerates.').
card_image_name('wall of brambles'/'3ED', 'wall of brambles').
card_uid('wall of brambles'/'3ED', '3ED:Wall of Brambles:wall of brambles').
card_rarity('wall of brambles'/'3ED', 'Uncommon').
card_artist('wall of brambles'/'3ED', 'Anson Maddocks').
card_flavor_text('wall of brambles'/'3ED', '\"What else, when chaos draws all forces inward to shape a single leaf.\"\n—Conrad Aiken').
card_multiverse_id('wall of brambles'/'3ED', '1273').

card_in_set('wall of fire', '3ED').
card_original_type('wall of fire'/'3ED', 'Summon — Wall').
card_original_text('wall of fire'/'3ED', '{R} +1/+0').
card_image_name('wall of fire'/'3ED', 'wall of fire').
card_uid('wall of fire'/'3ED', '3ED:Wall of Fire:wall of fire').
card_rarity('wall of fire'/'3ED', 'Uncommon').
card_artist('wall of fire'/'3ED', 'Richard Thomas').
card_flavor_text('wall of fire'/'3ED', 'Conjured from the bowels of hell, the fiery wall forms an impassable barrier, searing the soul of any creature attempting to pass through its terrible bursts of flame.').
card_multiverse_id('wall of fire'/'3ED', '1324').

card_in_set('wall of ice', '3ED').
card_original_type('wall of ice'/'3ED', 'Summon — Wall').
card_original_text('wall of ice'/'3ED', '').
card_image_name('wall of ice'/'3ED', 'wall of ice').
card_uid('wall of ice'/'3ED', '3ED:Wall of Ice:wall of ice').
card_rarity('wall of ice'/'3ED', 'Uncommon').
card_artist('wall of ice'/'3ED', 'Richard Thomas').
card_flavor_text('wall of ice'/'3ED', '\"And through the drifts the snowy cliffs\nDid send a dismal sheen:\nNor shapes of men nor beasts we ken—\nThe ice was all between.\"\n—Samuel Coleridge, \"The Rime of the Ancient Mariner\"').
card_multiverse_id('wall of ice'/'3ED', '1274').

card_in_set('wall of stone', '3ED').
card_original_type('wall of stone'/'3ED', 'Summon — Wall').
card_original_text('wall of stone'/'3ED', '').
card_image_name('wall of stone'/'3ED', 'wall of stone').
card_uid('wall of stone'/'3ED', '3ED:Wall of Stone:wall of stone').
card_rarity('wall of stone'/'3ED', 'Uncommon').
card_artist('wall of stone'/'3ED', 'Dan Frazier').
card_flavor_text('wall of stone'/'3ED', 'The Earth herself lends her strength to these walls of living stone, which possess the stability of ancient mountains. These mighty bulwarks thwart ground-based troops, providing welcome relief for weary warriors who defend the land.').
card_multiverse_id('wall of stone'/'3ED', '1325').

card_in_set('wall of swords', '3ED').
card_original_type('wall of swords'/'3ED', 'Summon — Wall').
card_original_text('wall of swords'/'3ED', 'Flying').
card_image_name('wall of swords'/'3ED', 'wall of swords').
card_uid('wall of swords'/'3ED', '3ED:Wall of Swords:wall of swords').
card_rarity('wall of swords'/'3ED', 'Uncommon').
card_artist('wall of swords'/'3ED', 'Mark Tedin').
card_flavor_text('wall of swords'/'3ED', 'Just as the evil ones approached to slay Justina, she cast a great spell, imbuing her weapons with her own life force. Thus she fulfilled the prophecy: \"In the death of your savior will you find salvation.\"').
card_multiverse_id('wall of swords'/'3ED', '1369').

card_in_set('wall of water', '3ED').
card_original_type('wall of water'/'3ED', 'Summon — Wall').
card_original_text('wall of water'/'3ED', '{U} +1/+0').
card_image_name('wall of water'/'3ED', 'wall of water').
card_uid('wall of water'/'3ED', '3ED:Wall of Water:wall of water').
card_rarity('wall of water'/'3ED', 'Uncommon').
card_artist('wall of water'/'3ED', 'Richard Thomas').
card_flavor_text('wall of water'/'3ED', 'A deafening roar arose as the fury of an enormous vertical river supplanted our serenity. Eddies turned into whirling geysers, leveling everything in their path.').
card_multiverse_id('wall of water'/'3ED', '1233').

card_in_set('wall of wood', '3ED').
card_original_type('wall of wood'/'3ED', 'Summon — Wall').
card_original_text('wall of wood'/'3ED', '').
card_image_name('wall of wood'/'3ED', 'wall of wood').
card_uid('wall of wood'/'3ED', '3ED:Wall of Wood:wall of wood').
card_rarity('wall of wood'/'3ED', 'Common').
card_artist('wall of wood'/'3ED', 'Mark Tedin').
card_flavor_text('wall of wood'/'3ED', 'Everybody knows that to ward off trouble, you knock on wood. But usually it\'s better to make a wall out of the wood and let trouble do the knocking.').
card_multiverse_id('wall of wood'/'3ED', '1275').

card_in_set('wanderlust', '3ED').
card_original_type('wanderlust'/'3ED', 'Enchant Creature').
card_original_text('wanderlust'/'3ED', 'Wanderlust does 1 damage to target creature\'s controller during his or her upkeep.').
card_image_name('wanderlust'/'3ED', 'wanderlust').
card_uid('wanderlust'/'3ED', '3ED:Wanderlust:wanderlust').
card_rarity('wanderlust'/'3ED', 'Uncommon').
card_artist('wanderlust'/'3ED', 'Cornelius Brudi').
card_multiverse_id('wanderlust'/'3ED', '1276').

card_in_set('war mammoth', '3ED').
card_original_type('war mammoth'/'3ED', 'Summon — Mammoth').
card_original_text('war mammoth'/'3ED', 'Trample').
card_image_name('war mammoth'/'3ED', 'war mammoth').
card_uid('war mammoth'/'3ED', '3ED:War Mammoth:war mammoth').
card_rarity('war mammoth'/'3ED', 'Common').
card_artist('war mammoth'/'3ED', 'Jeff A. Menges').
card_flavor_text('war mammoth'/'3ED', 'I didn\'t think Mammoths could ever hold a candle to a well-trained battle horse. Then one day I turned my back on a drunken soldier. His blow never landed; Mi\'cha flung the brute over ten meters.').
card_multiverse_id('war mammoth'/'3ED', '1277').

card_in_set('warp artifact', '3ED').
card_original_type('warp artifact'/'3ED', 'Enchant Artifact').
card_original_text('warp artifact'/'3ED', 'Warp Artifact does 1 damage to target artifact\'s controller during his or her upkeep.').
card_image_name('warp artifact'/'3ED', 'warp artifact').
card_uid('warp artifact'/'3ED', '3ED:Warp Artifact:warp artifact').
card_rarity('warp artifact'/'3ED', 'Rare').
card_artist('warp artifact'/'3ED', 'Amy Weber').
card_multiverse_id('warp artifact'/'3ED', '1185').

card_in_set('water elemental', '3ED').
card_original_type('water elemental'/'3ED', 'Summon — Elemental').
card_original_text('water elemental'/'3ED', '').
card_image_name('water elemental'/'3ED', 'water elemental').
card_uid('water elemental'/'3ED', '3ED:Water Elemental:water elemental').
card_rarity('water elemental'/'3ED', 'Uncommon').
card_artist('water elemental'/'3ED', 'Jeff A. Menges').
card_flavor_text('water elemental'/'3ED', 'Unpredictable as the sea itself, Water Elementals shift without warning from tranquility to tempest. Capricious and fickle, they flow restlessly from one shape to another, expressing their moods with their physical forms.').
card_multiverse_id('water elemental'/'3ED', '1234').

card_in_set('weakness', '3ED').
card_original_type('weakness'/'3ED', 'Enchant Creature').
card_original_text('weakness'/'3ED', 'Target creature loses -2/-1.').
card_image_name('weakness'/'3ED', 'weakness').
card_uid('weakness'/'3ED', '3ED:Weakness:weakness').
card_rarity('weakness'/'3ED', 'Common').
card_artist('weakness'/'3ED', 'Douglas Shuler').
card_multiverse_id('weakness'/'3ED', '1186').

card_in_set('web', '3ED').
card_original_type('web'/'3ED', 'Enchant Creature').
card_original_text('web'/'3ED', 'Target creature gains +0/+2 and can now block flying creatures, though it does not gain flying ability.').
card_image_name('web'/'3ED', 'web').
card_uid('web'/'3ED', '3ED:Web:web').
card_rarity('web'/'3ED', 'Rare').
card_artist('web'/'3ED', 'Rob Alexander').
card_multiverse_id('web'/'3ED', '1278').

card_in_set('wheel of fortune', '3ED').
card_original_type('wheel of fortune'/'3ED', 'Sorcery').
card_original_text('wheel of fortune'/'3ED', 'All players must discard their hands and draw seven new cards.').
card_image_name('wheel of fortune'/'3ED', 'wheel of fortune').
card_uid('wheel of fortune'/'3ED', '3ED:Wheel of Fortune:wheel of fortune').
card_rarity('wheel of fortune'/'3ED', 'Rare').
card_artist('wheel of fortune'/'3ED', 'Daniel Gelon').
card_multiverse_id('wheel of fortune'/'3ED', '1326').

card_in_set('white knight', '3ED').
card_original_type('white knight'/'3ED', 'Summon — Knight').
card_original_text('white knight'/'3ED', 'Protection from black, first strike').
card_image_name('white knight'/'3ED', 'white knight').
card_uid('white knight'/'3ED', '3ED:White Knight:white knight').
card_rarity('white knight'/'3ED', 'Uncommon').
card_artist('white knight'/'3ED', 'Daniel Gelon').
card_flavor_text('white knight'/'3ED', 'Out of the blackness and stench of the engulfing swamp emerged a shimmering figure. Only the splattered armor and ichor-stained sword hinted at the unfathomable evil the knight had just laid waste.').
card_multiverse_id('white knight'/'3ED', '1370').

card_in_set('white ward', '3ED').
card_original_type('white ward'/'3ED', 'Enchant Creature').
card_original_text('white ward'/'3ED', 'Target creature gains protection from white.').
card_image_name('white ward'/'3ED', 'white ward').
card_uid('white ward'/'3ED', '3ED:White Ward:white ward').
card_rarity('white ward'/'3ED', 'Uncommon').
card_artist('white ward'/'3ED', 'Dan Frazier').
card_multiverse_id('white ward'/'3ED', '1371').

card_in_set('wild growth', '3ED').
card_original_type('wild growth'/'3ED', 'Enchant Land').
card_original_text('wild growth'/'3ED', 'Whenever the usual mana is drawn from target land, Wild Growth provides an extra {G}.').
card_image_name('wild growth'/'3ED', 'wild growth').
card_uid('wild growth'/'3ED', '3ED:Wild Growth:wild growth').
card_rarity('wild growth'/'3ED', 'Common').
card_artist('wild growth'/'3ED', 'Mark Poole').
card_multiverse_id('wild growth'/'3ED', '1279').

card_in_set('will-o\'-the-wisp', '3ED').
card_original_type('will-o\'-the-wisp'/'3ED', 'Summon — Will-O\'-The-Wisp').
card_original_text('will-o\'-the-wisp'/'3ED', 'Flying; {B} Regenerates.').
card_image_name('will-o\'-the-wisp'/'3ED', 'will-o\'-the-wisp').
card_uid('will-o\'-the-wisp'/'3ED', '3ED:Will-o\'-the-Wisp:will-o\'-the-wisp').
card_rarity('will-o\'-the-wisp'/'3ED', 'Rare').
card_artist('will-o\'-the-wisp'/'3ED', 'Jesper Myrfors').
card_flavor_text('will-o\'-the-wisp'/'3ED', '\"About, about in reel and rout\nThe death-fires danced at night;\nThe water, like a witch\'s oils,\nBurnt green, and blue and white.\"\n—Samuel Coleridge, \"The Rime of the Ancient Mariner\"').
card_multiverse_id('will-o\'-the-wisp'/'3ED', '1187').

card_in_set('winter orb', '3ED').
card_original_type('winter orb'/'3ED', 'Artifact').
card_original_text('winter orb'/'3ED', 'A player may not untap more than one land during the untap phase of each of his or her turns.').
card_image_name('winter orb'/'3ED', 'winter orb').
card_uid('winter orb'/'3ED', '3ED:Winter Orb:winter orb').
card_rarity('winter orb'/'3ED', 'Rare').
card_artist('winter orb'/'3ED', 'Mark Tedin').
card_multiverse_id('winter orb'/'3ED', '1141').

card_in_set('wooden sphere', '3ED').
card_original_type('wooden sphere'/'3ED', 'Artifact').
card_original_text('wooden sphere'/'3ED', '{1}: Any green spell cast gives you 1 life. Can only give 1 life each time a green spell is cast.').
card_image_name('wooden sphere'/'3ED', 'wooden sphere').
card_uid('wooden sphere'/'3ED', '3ED:Wooden Sphere:wooden sphere').
card_rarity('wooden sphere'/'3ED', 'Uncommon').
card_artist('wooden sphere'/'3ED', 'Mark Tedin').
card_multiverse_id('wooden sphere'/'3ED', '1142').

card_in_set('wrath of god', '3ED').
card_original_type('wrath of god'/'3ED', 'Sorcery').
card_original_text('wrath of god'/'3ED', 'All creatures in play are buried.').
card_image_name('wrath of god'/'3ED', 'wrath of god').
card_uid('wrath of god'/'3ED', '3ED:Wrath of God:wrath of god').
card_rarity('wrath of god'/'3ED', 'Rare').
card_artist('wrath of god'/'3ED', 'Quinton Hoover').
card_multiverse_id('wrath of god'/'3ED', '1372').

card_in_set('zombie master', '3ED').
card_original_type('zombie master'/'3ED', 'Summon — Lord').
card_original_text('zombie master'/'3ED', 'All zombies in play gain swampwalk and \"{B} Regenerates\" for as long as this card remains in play.').
card_image_name('zombie master'/'3ED', 'zombie master').
card_uid('zombie master'/'3ED', '3ED:Zombie Master:zombie master').
card_rarity('zombie master'/'3ED', 'Rare').
card_artist('zombie master'/'3ED', 'Jeff A. Menges').
card_flavor_text('zombie master'/'3ED', 'They say the Zombie Master controlled these foul creatures even before his own death, but now that he is one of them, nothing can make them betray him.').
card_multiverse_id('zombie master'/'3ED', '1188').
