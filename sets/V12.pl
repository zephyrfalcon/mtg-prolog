% From the Vault: Realms

set('V12').
set_name('V12', 'From the Vault: Realms').
set_release_date('V12', '2012-08-31').
set_border('V12', 'black').
set_type('V12', 'from the vault').

card_in_set('ancient tomb', 'V12').
card_original_type('ancient tomb'/'V12', 'Land').
card_original_text('ancient tomb'/'V12', '{T}: Add {2} to your mana pool. Ancient Tomb deals 2 damage to you.').
card_image_name('ancient tomb'/'V12', 'ancient tomb').
card_uid('ancient tomb'/'V12', 'V12:Ancient Tomb:ancient tomb').
card_rarity('ancient tomb'/'V12', 'Mythic Rare').
card_artist('ancient tomb'/'V12', 'Colin MacNeil').
card_number('ancient tomb'/'V12', '1').
card_flavor_text('ancient tomb'/'V12', 'There is no glory to be gained in the kingdom of the dead.\n—Vec tomb inscription').
card_multiverse_id('ancient tomb'/'V12', '288994').

card_in_set('boseiju, who shelters all', 'V12').
card_original_type('boseiju, who shelters all'/'V12', 'Legendary Land').
card_original_text('boseiju, who shelters all'/'V12', 'Boseiju, Who Shelters All enters the battlefield tapped.\n{T}, Pay 2 life: Add {1} to your mana pool. If that mana is spent on an instant or sorcery spell, that spell can\'t be countered by spells or abilities.').
card_image_name('boseiju, who shelters all'/'V12', 'boseiju, who shelters all').
card_uid('boseiju, who shelters all'/'V12', 'V12:Boseiju, Who Shelters All:boseiju, who shelters all').
card_rarity('boseiju, who shelters all'/'V12', 'Mythic Rare').
card_artist('boseiju, who shelters all'/'V12', 'Ralph Horsley').
card_number('boseiju, who shelters all'/'V12', '2').
card_multiverse_id('boseiju, who shelters all'/'V12', '291507').

card_in_set('cephalid coliseum', 'V12').
card_original_type('cephalid coliseum'/'V12', 'Land').
card_original_text('cephalid coliseum'/'V12', '{T}: Add {U} to your mana pool. Cephalid Coliseum deals 1 damage to you.\nThreshold — {U}, {T}, Sacrifice Cephalid Coliseum: Target player draws three cards, then discards three cards. Activate this ability only if seven or more cards are in your graveyard.').
card_image_name('cephalid coliseum'/'V12', 'cephalid coliseum').
card_uid('cephalid coliseum'/'V12', 'V12:Cephalid Coliseum:cephalid coliseum').
card_rarity('cephalid coliseum'/'V12', 'Mythic Rare').
card_artist('cephalid coliseum'/'V12', 'Cliff Childs').
card_number('cephalid coliseum'/'V12', '3').
card_multiverse_id('cephalid coliseum'/'V12', '288995').

card_in_set('desert', 'V12').
card_original_type('desert'/'V12', 'Land — Desert').
card_original_text('desert'/'V12', '{T}: Add {1} to your mana pool.\n{T}: Desert deals 1 damage to target attacking creature. Activate this ability only during the end of combat step.').
card_image_name('desert'/'V12', 'desert').
card_uid('desert'/'V12', 'V12:Desert:desert').
card_rarity('desert'/'V12', 'Mythic Rare').
card_artist('desert'/'V12', 'Glen Angus').
card_number('desert'/'V12', '4').
card_multiverse_id('desert'/'V12', '287341').

card_in_set('dryad arbor', 'V12').
card_original_type('dryad arbor'/'V12', 'Land Creature — Forest Dryad').
card_original_text('dryad arbor'/'V12', 'G').
card_image_name('dryad arbor'/'V12', 'dryad arbor').
card_uid('dryad arbor'/'V12', 'V12:Dryad Arbor:dryad arbor').
card_rarity('dryad arbor'/'V12', 'Mythic Rare').
card_artist('dryad arbor'/'V12', 'Brad Rigney').
card_number('dryad arbor'/'V12', '5').
card_multiverse_id('dryad arbor'/'V12', '282542').

card_in_set('forbidden orchard', 'V12').
card_original_type('forbidden orchard'/'V12', 'Land').
card_original_text('forbidden orchard'/'V12', '{T}: Add one mana of any color to your mana pool.\nWhenever you tap Forbidden Orchard for mana, put a 1/1 colorless Spirit creature token onto the battlefield under target opponent\'s control.').
card_image_name('forbidden orchard'/'V12', 'forbidden orchard').
card_uid('forbidden orchard'/'V12', 'V12:Forbidden Orchard:forbidden orchard').
card_rarity('forbidden orchard'/'V12', 'Mythic Rare').
card_artist('forbidden orchard'/'V12', 'Daniel Ljunggren').
card_number('forbidden orchard'/'V12', '6').
card_multiverse_id('forbidden orchard'/'V12', '282541').

card_in_set('glacial chasm', 'V12').
card_original_type('glacial chasm'/'V12', 'Land').
card_original_text('glacial chasm'/'V12', 'Cumulative upkeep—Pay 2 life.\nWhen Glacial Chasm enters the battlefield, sacrifice a land.\nCreatures you control can\'t attack.\nPrevent all damage that would be dealt to you.').
card_image_name('glacial chasm'/'V12', 'glacial chasm').
card_uid('glacial chasm'/'V12', 'V12:Glacial Chasm:glacial chasm').
card_rarity('glacial chasm'/'V12', 'Mythic Rare').
card_artist('glacial chasm'/'V12', 'Mike Bierek').
card_number('glacial chasm'/'V12', '7').
card_multiverse_id('glacial chasm'/'V12', '288996').

card_in_set('grove of the burnwillows', 'V12').
card_original_type('grove of the burnwillows'/'V12', 'Land').
card_original_text('grove of the burnwillows'/'V12', '{T}: Add {1} to your mana pool.\n{T}: Add {R} or {G} to your mana pool. Each opponent gains 1 life.').
card_image_name('grove of the burnwillows'/'V12', 'grove of the burnwillows').
card_uid('grove of the burnwillows'/'V12', 'V12:Grove of the Burnwillows:grove of the burnwillows').
card_rarity('grove of the burnwillows'/'V12', 'Mythic Rare').
card_artist('grove of the burnwillows'/'V12', 'Cliff Childs').
card_number('grove of the burnwillows'/'V12', '8').
card_flavor_text('grove of the burnwillows'/'V12', 'Spring is the most beautiful season in the grove, when the new leaves open from their ember-buds in a race of leaping flames.').
card_multiverse_id('grove of the burnwillows'/'V12', '287335').

card_in_set('high market', 'V12').
card_original_type('high market'/'V12', 'Land').
card_original_text('high market'/'V12', '{T}: Add {1} to your mana pool.\n{T}, Sacrifice a creature: You gain 1 life.').
card_image_name('high market'/'V12', 'high market').
card_uid('high market'/'V12', 'V12:High Market:high market').
card_rarity('high market'/'V12', 'Mythic Rare').
card_artist('high market'/'V12', 'Carl Critchlow').
card_number('high market'/'V12', '9').
card_flavor_text('high market'/'V12', 'If it can\'t be had here, it can\'t be had on any world.').
card_multiverse_id('high market'/'V12', '287336').

card_in_set('maze of ith', 'V12').
card_original_type('maze of ith'/'V12', 'Land').
card_original_text('maze of ith'/'V12', '{T}: Untap target attacking creature. Prevent all combat damage that would be dealt to and dealt by that creature this turn.').
card_image_name('maze of ith'/'V12', 'maze of ith').
card_uid('maze of ith'/'V12', 'V12:Maze of Ith:maze of ith').
card_rarity('maze of ith'/'V12', 'Mythic Rare').
card_artist('maze of ith'/'V12', 'Yeong-Hao Han').
card_number('maze of ith'/'V12', '10').
card_multiverse_id('maze of ith'/'V12', '287329').

card_in_set('murmuring bosk', 'V12').
card_original_type('murmuring bosk'/'V12', 'Land — Forest').
card_original_text('murmuring bosk'/'V12', '({T}: Add {G} to your mana pool.)\nAs Murmuring Bosk enters the battlefield, you may reveal a Treefolk card from your hand. If you don\'t, Murmuring Bosk enters the battlefield tapped.\n{T}: Add {W} or {B} to your mana pool. Murmuring Bosk deals 1 damage to you.').
card_image_name('murmuring bosk'/'V12', 'murmuring bosk').
card_uid('murmuring bosk'/'V12', 'V12:Murmuring Bosk:murmuring bosk').
card_rarity('murmuring bosk'/'V12', 'Mythic Rare').
card_artist('murmuring bosk'/'V12', 'John Avon').
card_number('murmuring bosk'/'V12', '11').
card_multiverse_id('murmuring bosk'/'V12', '287334').

card_in_set('shivan gorge', 'V12').
card_original_type('shivan gorge'/'V12', 'Legendary Land').
card_original_text('shivan gorge'/'V12', '{T}: Add {1} to your mana pool.\n{2}{R}, {T}: Shivan Gorge deals 1 damage to each opponent.').
card_image_name('shivan gorge'/'V12', 'shivan gorge').
card_uid('shivan gorge'/'V12', 'V12:Shivan Gorge:shivan gorge').
card_rarity('shivan gorge'/'V12', 'Mythic Rare').
card_artist('shivan gorge'/'V12', 'Adam Paquette').
card_number('shivan gorge'/'V12', '12').
card_flavor_text('shivan gorge'/'V12', 'Both forge and pyre.').
card_multiverse_id('shivan gorge'/'V12', '288997').

card_in_set('urborg, tomb of yawgmoth', 'V12').
card_original_type('urborg, tomb of yawgmoth'/'V12', 'Legendary Land').
card_original_text('urborg, tomb of yawgmoth'/'V12', 'Each land is a Swamp in addition to its other land types.').
card_image_name('urborg, tomb of yawgmoth'/'V12', 'urborg, tomb of yawgmoth').
card_uid('urborg, tomb of yawgmoth'/'V12', 'V12:Urborg, Tomb of Yawgmoth:urborg, tomb of yawgmoth').
card_rarity('urborg, tomb of yawgmoth'/'V12', 'Mythic Rare').
card_artist('urborg, tomb of yawgmoth'/'V12', 'John Avon').
card_number('urborg, tomb of yawgmoth'/'V12', '13').
card_flavor_text('urborg, tomb of yawgmoth'/'V12', '\"Yawgmoth\'s corpse is a wound in the universe. His foul blood seeps out, infecting the land with his final curse.\"\n—Lord Windgrace').
card_multiverse_id('urborg, tomb of yawgmoth'/'V12', '287330').

card_in_set('vesuva', 'V12').
card_original_type('vesuva'/'V12', 'Land').
card_original_text('vesuva'/'V12', 'You may have Vesuva enter the battlefield tapped as a copy of any land on the battlefield.').
card_image_name('vesuva'/'V12', 'vesuva').
card_uid('vesuva'/'V12', 'V12:Vesuva:vesuva').
card_rarity('vesuva'/'V12', 'Mythic Rare').
card_artist('vesuva'/'V12', 'Zoltan Boros & Gabor Szikszai').
card_number('vesuva'/'V12', '14').
card_flavor_text('vesuva'/'V12', 'It is everywhere you\'ve ever been.').
card_multiverse_id('vesuva'/'V12', '287332').

card_in_set('windbrisk heights', 'V12').
card_original_type('windbrisk heights'/'V12', 'Land').
card_original_text('windbrisk heights'/'V12', 'Hideaway (This land enters the battlefield tapped. When it does, look at the top four cards of your library, exile one face down, then put the rest on the bottom of your library.)\n{T}: Add {W} to your mana pool.\n{W}, {T}: You may play the exiled card without paying its mana cost if you attacked with three or more creatures this turn.').
card_image_name('windbrisk heights'/'V12', 'windbrisk heights').
card_uid('windbrisk heights'/'V12', 'V12:Windbrisk Heights:windbrisk heights').
card_rarity('windbrisk heights'/'V12', 'Mythic Rare').
card_artist('windbrisk heights'/'V12', 'Omar Rayyan').
card_number('windbrisk heights'/'V12', '15').
card_multiverse_id('windbrisk heights'/'V12', '287338').
