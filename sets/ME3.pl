% Masters Edition III

set('ME3').
set_name('ME3', 'Masters Edition III').
set_release_date('ME3', '2009-09-07').
set_border('ME3', 'black').
set_type('ME3', 'masters').

card_in_set('active volcano', 'ME3').
card_original_type('active volcano'/'ME3', 'Instant').
card_original_text('active volcano'/'ME3', 'Choose one — Destroy target blue permanent; or return target Island to its owner\'s hand.').
card_image_name('active volcano'/'ME3', 'active volcano').
card_uid('active volcano'/'ME3', 'ME3:Active Volcano:active volcano').
card_rarity('active volcano'/'ME3', 'Uncommon').
card_artist('active volcano'/'ME3', 'Justin Hampton').
card_number('active volcano'/'ME3', '85').
card_multiverse_id('active volcano'/'ME3', '201098').

card_in_set('akron legionnaire', 'ME3').
card_original_type('akron legionnaire'/'ME3', 'Creature — Giant Soldier').
card_original_text('akron legionnaire'/'ME3', 'Except for creatures named Akron Legionnaire and artifact creatures, creatures you control can\'t attack.').
card_image_name('akron legionnaire'/'ME3', 'akron legionnaire').
card_uid('akron legionnaire'/'ME3', 'ME3:Akron Legionnaire:akron legionnaire').
card_rarity('akron legionnaire'/'ME3', 'Rare').
card_artist('akron legionnaire'/'ME3', 'Mark Poole').
card_number('akron legionnaire'/'ME3', '1').
card_multiverse_id('akron legionnaire'/'ME3', '201117').

card_in_set('alabaster potion', 'ME3').
card_original_type('alabaster potion'/'ME3', 'Instant').
card_original_text('alabaster potion'/'ME3', 'Choose one — Target player gains X life; or prevent the next X damage that would be dealt to target creature or player this turn.').
card_image_name('alabaster potion'/'ME3', 'alabaster potion').
card_uid('alabaster potion'/'ME3', 'ME3:Alabaster Potion:alabaster potion').
card_rarity('alabaster potion'/'ME3', 'Uncommon').
card_artist('alabaster potion'/'ME3', 'Harold McNeill').
card_number('alabaster potion'/'ME3', '2').
card_flavor_text('alabaster potion'/'ME3', '\"Healing is a matter of time, but it is sometimes also a matter of opportunity.\"\n—D\'Avenant proverb').
card_multiverse_id('alabaster potion'/'ME3', '201118').

card_in_set('all hallow\'s eve', 'ME3').
card_original_type('all hallow\'s eve'/'ME3', 'Sorcery').
card_original_text('all hallow\'s eve'/'ME3', 'Exile All Hallow\'s Eve with two scream counters on it.\nAt the beginning of your upkeep, if All Hallow\'s Eve is exiled with a scream counter on it, remove a scream counter from it. If there are no more scream counters on it, put it into your graveyard and each player returns all creature cards from his or her graveyard to the battlefield.').
card_image_name('all hallow\'s eve'/'ME3', 'all hallow\'s eve').
card_uid('all hallow\'s eve'/'ME3', 'ME3:All Hallow\'s Eve:all hallow\'s eve').
card_rarity('all hallow\'s eve'/'ME3', 'Rare').
card_artist('all hallow\'s eve'/'ME3', 'Christopher Rush').
card_number('all hallow\'s eve'/'ME3', '57').
card_multiverse_id('all hallow\'s eve'/'ME3', '201119').

card_in_set('amrou kithkin', 'ME3').
card_original_type('amrou kithkin'/'ME3', 'Creature — Kithkin').
card_original_text('amrou kithkin'/'ME3', 'Amrou Kithkin can\'t be blocked by creatures with power 3 or greater.').
card_image_name('amrou kithkin'/'ME3', 'amrou kithkin').
card_uid('amrou kithkin'/'ME3', 'ME3:Amrou Kithkin:amrou kithkin').
card_rarity('amrou kithkin'/'ME3', 'Common').
card_artist('amrou kithkin'/'ME3', 'Quinton Hoover').
card_number('amrou kithkin'/'ME3', '3').
card_flavor_text('amrou kithkin'/'ME3', 'Quick and agile, Amrou Kithkin can usually escape from even the most fearsome opponents.').
card_multiverse_id('amrou kithkin'/'ME3', '201265').

card_in_set('anaba ancestor', 'ME3').
card_original_type('anaba ancestor'/'ME3', 'Creature — Minotaur Spirit').
card_original_text('anaba ancestor'/'ME3', '{T}: Another target Minotaur creature gets +1/+1 until end of turn.').
card_image_name('anaba ancestor'/'ME3', 'anaba ancestor').
card_uid('anaba ancestor'/'ME3', 'ME3:Anaba Ancestor:anaba ancestor').
card_rarity('anaba ancestor'/'ME3', 'Common').
card_artist('anaba ancestor'/'ME3', 'Anson Maddocks').
card_number('anaba ancestor'/'ME3', '86').
card_flavor_text('anaba ancestor'/'ME3', '\"The Ancestors are the wisdom of the tribe and the soul of the Homelands. I am eternally in their debt.\"\n—Taysir').
card_multiverse_id('anaba ancestor'/'ME3', '201315').

card_in_set('anaba spirit crafter', 'ME3').
card_original_type('anaba spirit crafter'/'ME3', 'Creature — Minotaur Shaman').
card_original_text('anaba spirit crafter'/'ME3', 'Minotaur creatures get +1/+0.').
card_image_name('anaba spirit crafter'/'ME3', 'anaba spirit crafter').
card_uid('anaba spirit crafter'/'ME3', 'ME3:Anaba Spirit Crafter:anaba spirit crafter').
card_rarity('anaba spirit crafter'/'ME3', 'Common').
card_artist('anaba spirit crafter'/'ME3', 'Anson Maddocks').
card_number('anaba spirit crafter'/'ME3', '87').
card_flavor_text('anaba spirit crafter'/'ME3', '\"The Spirit Crafters sing of all our people. They sing of those lost, of those found, and of those who are yet to be.\"\n—Onatah, Anaba Shaman').
card_multiverse_id('anaba spirit crafter'/'ME3', '201316').

card_in_set('angus mackenzie', 'ME3').
card_original_type('angus mackenzie'/'ME3', 'Legendary Creature — Human Cleric').
card_original_text('angus mackenzie'/'ME3', '{G}{W}{U}, {T}: Prevent all combat damage that would be dealt this turn.').
card_image_name('angus mackenzie'/'ME3', 'angus mackenzie').
card_uid('angus mackenzie'/'ME3', 'ME3:Angus Mackenzie:angus mackenzie').
card_rarity('angus mackenzie'/'ME3', 'Rare').
card_artist('angus mackenzie'/'ME3', 'Bryon Wackwitz').
card_number('angus mackenzie'/'ME3', '141').
card_flavor_text('angus mackenzie'/'ME3', '\"Battles no longer served a purpose in Karakas.\" —Angus Mackenzie, Diary').
card_multiverse_id('angus mackenzie'/'ME3', '201175').

card_in_set('arboria', 'ME3').
card_original_type('arboria'/'ME3', 'World Enchantment').
card_original_text('arboria'/'ME3', 'Creatures can\'t attack a player who didn\'t cast a spell and didn\'t put a card onto the battlefield during his or her last turn.').
card_image_name('arboria'/'ME3', 'arboria').
card_uid('arboria'/'ME3', 'ME3:Arboria:arboria').
card_rarity('arboria'/'ME3', 'Rare').
card_artist('arboria'/'ME3', 'Daniel Gelon').
card_number('arboria'/'ME3', '113').
card_multiverse_id('arboria'/'ME3', '201176').

card_in_set('arcades sabboth', 'ME3').
card_original_type('arcades sabboth'/'ME3', 'Legendary Creature — Elder Dragon').
card_original_text('arcades sabboth'/'ME3', 'Flying\nAt the beginning of your upkeep, sacrifice Arcades Sabboth unless you pay {G}{W}{U}.\nUntapped nonattacking creatures you control get +0/+2.\n{W}: Arcades Sabboth gets +0/+1 until end of turn.').
card_image_name('arcades sabboth'/'ME3', 'arcades sabboth').
card_uid('arcades sabboth'/'ME3', 'ME3:Arcades Sabboth:arcades sabboth').
card_rarity('arcades sabboth'/'ME3', 'Rare').
card_artist('arcades sabboth'/'ME3', 'Edward P. Beard, Jr.').
card_number('arcades sabboth'/'ME3', '142').
card_multiverse_id('arcades sabboth'/'ME3', '201177').

card_in_set('arena of the ancients', 'ME3').
card_original_type('arena of the ancients'/'ME3', 'Artifact').
card_original_text('arena of the ancients'/'ME3', 'Legendary creatures don\'t untap during their controllers\' untap steps.\nWhen Arena of the Ancients enters the battlefield, tap all legendary creatures.').
card_image_name('arena of the ancients'/'ME3', 'arena of the ancients').
card_uid('arena of the ancients'/'ME3', 'ME3:Arena of the Ancients:arena of the ancients').
card_rarity('arena of the ancients'/'ME3', 'Rare').
card_artist('arena of the ancients'/'ME3', 'Tom Wänerstrand').
card_number('arena of the ancients'/'ME3', '188').
card_multiverse_id('arena of the ancients'/'ME3', '201209').

card_in_set('ashes to ashes', 'ME3').
card_original_type('ashes to ashes'/'ME3', 'Sorcery').
card_original_text('ashes to ashes'/'ME3', 'Exile two target nonartifact creatures. Ashes to Ashes deals 5 damage to you.').
card_image_name('ashes to ashes'/'ME3', 'ashes to ashes').
card_uid('ashes to ashes'/'ME3', 'ME3:Ashes to Ashes:ashes to ashes').
card_rarity('ashes to ashes'/'ME3', 'Uncommon').
card_artist('ashes to ashes'/'ME3', 'Drew Tucker').
card_number('ashes to ashes'/'ME3', '58').
card_flavor_text('ashes to ashes'/'ME3', '\"All rivers eventually run to the sea. My job is to sort out who goes first.\"\n—Maeveen O\'Donagh, Memoirs of a Soldier').
card_multiverse_id('ashes to ashes'/'ME3', '201250').

card_in_set('astrolabe', 'ME3').
card_original_type('astrolabe'/'ME3', 'Artifact').
card_original_text('astrolabe'/'ME3', '{1}, {T}, Sacrifice Astrolabe: Add two mana of any one color to your mana pool. Draw a card at the beginning of the next turn\'s upkeep.').
card_image_name('astrolabe'/'ME3', 'astrolabe').
card_uid('astrolabe'/'ME3', 'ME3:Astrolabe:astrolabe').
card_rarity('astrolabe'/'ME3', 'Common').
card_artist('astrolabe'/'ME3', 'Amy Weber').
card_number('astrolabe'/'ME3', '189').
card_multiverse_id('astrolabe'/'ME3', '201218').

card_in_set('axelrod gunnarson', 'ME3').
card_original_type('axelrod gunnarson'/'ME3', 'Legendary Creature — Giant').
card_original_text('axelrod gunnarson'/'ME3', 'Trample\nWhenever a creature dealt damage by Axelrod Gunnarson this turn is put into a graveyard, you gain 1 life and Axelrod deals 1 damage to target player.').
card_image_name('axelrod gunnarson'/'ME3', 'axelrod gunnarson').
card_uid('axelrod gunnarson'/'ME3', 'ME3:Axelrod Gunnarson:axelrod gunnarson').
card_rarity('axelrod gunnarson'/'ME3', 'Uncommon').
card_artist('axelrod gunnarson'/'ME3', 'Scott Kirschner').
card_number('axelrod gunnarson'/'ME3', '143').
card_multiverse_id('axelrod gunnarson'/'ME3', '201178').

card_in_set('banshee', 'ME3').
card_original_type('banshee'/'ME3', 'Creature — Spirit').
card_original_text('banshee'/'ME3', '{X}, {T}: Banshee deals X damage divided evenly between you and target creature or player. Round up for you and down for that creature or player.').
card_image_name('banshee'/'ME3', 'banshee').
card_uid('banshee'/'ME3', 'ME3:Banshee:banshee').
card_rarity('banshee'/'ME3', 'Uncommon').
card_artist('banshee'/'ME3', 'Jesper Myrfors').
card_number('banshee'/'ME3', '59').
card_flavor_text('banshee'/'ME3', 'Some say Banshees are the hounds of Death, baying to herd their prey into the arms of their master.').
card_multiverse_id('banshee'/'ME3', '201251').

card_in_set('barktooth warbeard', 'ME3').
card_original_type('barktooth warbeard'/'ME3', 'Legendary Creature — Human Warrior').
card_original_text('barktooth warbeard'/'ME3', '').
card_image_name('barktooth warbeard'/'ME3', 'barktooth warbeard').
card_uid('barktooth warbeard'/'ME3', 'ME3:Barktooth Warbeard:barktooth warbeard').
card_rarity('barktooth warbeard'/'ME3', 'Common').
card_artist('barktooth warbeard'/'ME3', 'Andi Rusu').
card_number('barktooth warbeard'/'ME3', '144').
card_flavor_text('barktooth warbeard'/'ME3', 'He is devious and cunning, in both appearance and deed. Beware the Warbeard, for this brute bites as well as he barks!').
card_multiverse_id('barktooth warbeard'/'ME3', '201259').

card_in_set('barl\'s cage', 'ME3').
card_original_type('barl\'s cage'/'ME3', 'Artifact').
card_original_text('barl\'s cage'/'ME3', '{3}: Target creature doesn\'t untap during its controller\'s next untap step.').
card_image_name('barl\'s cage'/'ME3', 'barl\'s cage').
card_uid('barl\'s cage'/'ME3', 'ME3:Barl\'s Cage:barl\'s cage').
card_rarity('barl\'s cage'/'ME3', 'Rare').
card_artist('barl\'s cage'/'ME3', 'Tom Wänerstrand').
card_number('barl\'s cage'/'ME3', '190').
card_flavor_text('barl\'s cage'/'ME3', 'For a dozen years the Cage had held Lord Ith, but as the Pretender Mairsil\'s power weakened, so did the bars.').
card_multiverse_id('barl\'s cage'/'ME3', '201190').

card_in_set('bartel runeaxe', 'ME3').
card_original_type('bartel runeaxe'/'ME3', 'Legendary Creature — Giant Warrior').
card_original_text('bartel runeaxe'/'ME3', 'Vigilance\nBartel Runeaxe can\'t be the target of Aura spells.').
card_image_name('bartel runeaxe'/'ME3', 'bartel runeaxe').
card_uid('bartel runeaxe'/'ME3', 'ME3:Bartel Runeaxe:bartel runeaxe').
card_rarity('bartel runeaxe'/'ME3', 'Uncommon').
card_artist('bartel runeaxe'/'ME3', 'Andi Rusu').
card_number('bartel runeaxe'/'ME3', '145').
card_flavor_text('bartel runeaxe'/'ME3', 'Thundering down from Hammerheim, no foe could slow Bartel\'s charge.').
card_multiverse_id('bartel runeaxe'/'ME3', '201181').

card_in_set('bayou', 'ME3').
card_original_type('bayou'/'ME3', 'Land — Swamp Forest').
card_original_text('bayou'/'ME3', '').
card_image_name('bayou'/'ME3', 'bayou').
card_uid('bayou'/'ME3', 'ME3:Bayou:bayou').
card_rarity('bayou'/'ME3', 'Rare').
card_artist('bayou'/'ME3', 'Jesper Myrfors').
card_number('bayou'/'ME3', '204').
card_multiverse_id('bayou'/'ME3', '201400').

card_in_set('bazaar of baghdad', 'ME3').
card_original_type('bazaar of baghdad'/'ME3', 'Land').
card_original_text('bazaar of baghdad'/'ME3', '{T}: Draw two cards, then discard three cards.').
card_image_name('bazaar of baghdad'/'ME3', 'bazaar of baghdad').
card_uid('bazaar of baghdad'/'ME3', 'ME3:Bazaar of Baghdad:bazaar of baghdad').
card_rarity('bazaar of baghdad'/'ME3', 'Rare').
card_artist('bazaar of baghdad'/'ME3', 'Jeff A. Menges').
card_number('bazaar of baghdad'/'ME3', '205').
card_multiverse_id('bazaar of baghdad'/'ME3', '201220').

card_in_set('benthic explorers', 'ME3').
card_original_type('benthic explorers'/'ME3', 'Creature — Merfolk Scout').
card_original_text('benthic explorers'/'ME3', '{T}, Untap a tapped land an opponent controls: Add one mana of any type that land could produce to your mana pool.').
card_image_name('benthic explorers'/'ME3', 'benthic explorers').
card_uid('benthic explorers'/'ME3', 'ME3:Benthic Explorers:benthic explorers').
card_rarity('benthic explorers'/'ME3', 'Common').
card_artist('benthic explorers'/'ME3', 'Greg Simanson').
card_number('benthic explorers'/'ME3', '29').
card_flavor_text('benthic explorers'/'ME3', 'The rising oceans brought new lakes—and new terrors—to Terisiare. The Explorers found their ancient enemies spawning everywhere.').
card_multiverse_id('benthic explorers'/'ME3', '201159').

card_in_set('black vise', 'ME3').
card_original_type('black vise'/'ME3', 'Artifact').
card_original_text('black vise'/'ME3', 'As Black Vise enters the battlefield, choose an opponent.\nAt the beginning of the chosen player\'s upkeep, Black Vise deals X damage to that player, where X is the number of cards in his or her hand minus 4.').
card_image_name('black vise'/'ME3', 'black vise').
card_uid('black vise'/'ME3', 'ME3:Black Vise:black vise').
card_rarity('black vise'/'ME3', 'Rare').
card_artist('black vise'/'ME3', 'Richard Thomas').
card_number('black vise'/'ME3', '191').
card_multiverse_id('black vise'/'ME3', '201239').

card_in_set('blood lust', 'ME3').
card_original_type('blood lust'/'ME3', 'Instant').
card_original_text('blood lust'/'ME3', 'If target creature has toughness 5 or greater, it gets +4/-4 until end of turn. Otherwise, it gets +4/-X until end of turn, where X is its toughness minus 1.').
card_image_name('blood lust'/'ME3', 'blood lust').
card_uid('blood lust'/'ME3', 'ME3:Blood Lust:blood lust').
card_rarity('blood lust'/'ME3', 'Common').
card_artist('blood lust'/'ME3', 'Anson Maddocks').
card_number('blood lust'/'ME3', '88').
card_multiverse_id('blood lust'/'ME3', '201272').

card_in_set('bone flute', 'ME3').
card_original_type('bone flute'/'ME3', 'Artifact').
card_original_text('bone flute'/'ME3', '{2}, {T}: All creatures get -1/-0 until end of turn.').
card_image_name('bone flute'/'ME3', 'bone flute').
card_uid('bone flute'/'ME3', 'ME3:Bone Flute:bone flute').
card_rarity('bone flute'/'ME3', 'Common').
card_artist('bone flute'/'ME3', 'Christopher Rush').
card_number('bone flute'/'ME3', '192').
card_flavor_text('bone flute'/'ME3', 'After the Battle of Pitdown, Lady Ursnell fashioned the first such instrument out of Lord Ursnell\'s left leg.').
card_multiverse_id('bone flute'/'ME3', '201140').

card_in_set('boomerang', 'ME3').
card_original_type('boomerang'/'ME3', 'Instant').
card_original_text('boomerang'/'ME3', 'Return target permanent to its owner\'s hand.').
card_image_name('boomerang'/'ME3', 'boomerang').
card_uid('boomerang'/'ME3', 'ME3:Boomerang:boomerang').
card_rarity('boomerang'/'ME3', 'Common').
card_artist('boomerang'/'ME3', 'Brian Snõddy').
card_number('boomerang'/'ME3', '30').
card_flavor_text('boomerang'/'ME3', '\"O! call back yesterday, bid time return.\"\n—William Shakespeare, King Richard the Second').
card_multiverse_id('boomerang'/'ME3', '201122').

card_in_set('boris devilboon', 'ME3').
card_original_type('boris devilboon'/'ME3', 'Legendary Creature — Zombie Wizard').
card_original_text('boris devilboon'/'ME3', '{2}{B}{R}, {T}: Put a 1/1 black and red Demon creature token named Minor Demon onto the battlefield.').
card_image_name('boris devilboon'/'ME3', 'boris devilboon').
card_uid('boris devilboon'/'ME3', 'ME3:Boris Devilboon:boris devilboon').
card_rarity('boris devilboon'/'ME3', 'Uncommon').
card_artist('boris devilboon'/'ME3', 'Jesper Myrfors').
card_number('boris devilboon'/'ME3', '146').
card_multiverse_id('boris devilboon'/'ME3', '201182').

card_in_set('borrowing 100,000 arrows', 'ME3').
card_original_type('borrowing 100,000 arrows'/'ME3', 'Sorcery').
card_original_text('borrowing 100,000 arrows'/'ME3', 'Draw a card for each tapped creature target opponent controls.').
card_image_name('borrowing 100,000 arrows'/'ME3', 'borrowing 100,000 arrows').
card_uid('borrowing 100,000 arrows'/'ME3', 'ME3:Borrowing 100,000 Arrows:borrowing 100,000 arrows').
card_rarity('borrowing 100,000 arrows'/'ME3', 'Uncommon').
card_artist('borrowing 100,000 arrows'/'ME3', 'Song Shikai').
card_number('borrowing 100,000 arrows'/'ME3', '31').
card_flavor_text('borrowing 100,000 arrows'/'ME3', 'Kongming and Lu Su tricked Wei troops into shooting over 100,000 arrows at them to later use against the Wei at Red Cliffs.').
card_multiverse_id('borrowing 100,000 arrows'/'ME3', '201257').

card_in_set('brilliant plan', 'ME3').
card_original_type('brilliant plan'/'ME3', 'Sorcery').
card_original_text('brilliant plan'/'ME3', 'Draw three cards.').
card_image_name('brilliant plan'/'ME3', 'brilliant plan').
card_uid('brilliant plan'/'ME3', 'ME3:Brilliant Plan:brilliant plan').
card_rarity('brilliant plan'/'ME3', 'Common').
card_artist('brilliant plan'/'ME3', 'Song Shikai').
card_number('brilliant plan'/'ME3', '32').
card_flavor_text('brilliant plan'/'ME3', 'At Red Cliffs, Kongming and Zhou Yu each wrote his plan for defeating Wei on the palm of his hand. They laughed as they both revealed the same word, \"Fire.\"').
card_multiverse_id('brilliant plan'/'ME3', '201290').

card_in_set('burning of xinye', 'ME3').
card_original_type('burning of xinye'/'ME3', 'Sorcery').
card_original_text('burning of xinye'/'ME3', 'Each player sacrifices four lands. Burning of Xinye deals 4 damage to each creature.').
card_image_name('burning of xinye'/'ME3', 'burning of xinye').
card_uid('burning of xinye'/'ME3', 'ME3:Burning of Xinye:burning of xinye').
card_rarity('burning of xinye'/'ME3', 'Rare').
card_artist('burning of xinye'/'ME3', 'Yang Hong').
card_number('burning of xinye'/'ME3', '89').
card_multiverse_id('burning of xinye'/'ME3', '201274').

card_in_set('call to arms', 'ME3').
card_original_type('call to arms'/'ME3', 'Enchantment').
card_original_text('call to arms'/'ME3', 'As Call to Arms enters the battlefield, choose a color and an opponent.\nWhite creatures get +1/+1.\nWhen the chosen color isn\'t the most common color among permanents the chosen player controls, sacrifice Call to Arms.').
card_image_name('call to arms'/'ME3', 'call to arms').
card_uid('call to arms'/'ME3', 'ME3:Call to Arms:call to arms').
card_rarity('call to arms'/'ME3', 'Uncommon').
card_artist('call to arms'/'ME3', 'Randy Gallegos').
card_number('call to arms'/'ME3', '4').
card_multiverse_id('call to arms'/'ME3', '201147').

card_in_set('capture of jingzhou', 'ME3').
card_original_type('capture of jingzhou'/'ME3', 'Sorcery').
card_original_text('capture of jingzhou'/'ME3', 'Take an extra turn after this one.').
card_image_name('capture of jingzhou'/'ME3', 'capture of jingzhou').
card_uid('capture of jingzhou'/'ME3', 'ME3:Capture of Jingzhou:capture of jingzhou').
card_rarity('capture of jingzhou'/'ME3', 'Rare').
card_artist('capture of jingzhou'/'ME3', 'Jack Wei').
card_number('capture of jingzhou'/'ME3', '33').
card_flavor_text('capture of jingzhou'/'ME3', 'The province of Jingzhou was the fulcrum of the three kingdoms. At separate times it was coveted and controlled by the Wu, the Shu, and the Wei.').
card_multiverse_id('capture of jingzhou'/'ME3', '201135').

card_in_set('carrion ants', 'ME3').
card_original_type('carrion ants'/'ME3', 'Creature — Insect').
card_original_text('carrion ants'/'ME3', '{1}: Carrion Ants gets +1/+1 until end of turn.').
card_image_name('carrion ants'/'ME3', 'carrion ants').
card_uid('carrion ants'/'ME3', 'ME3:Carrion Ants:carrion ants').
card_rarity('carrion ants'/'ME3', 'Uncommon').
card_artist('carrion ants'/'ME3', 'Richard Thomas').
card_number('carrion ants'/'ME3', '60').
card_flavor_text('carrion ants'/'ME3', '\"‘War is no picnic,\' my father liked to say. But the ants seemed to disagree.\"\n—General Chanek Valteroth').
card_multiverse_id('carrion ants'/'ME3', '201125').

card_in_set('chain lightning', 'ME3').
card_original_type('chain lightning'/'ME3', 'Sorcery').
card_original_text('chain lightning'/'ME3', 'Chain Lightning deals 3 damage to target creature or player. Then that player or that creature\'s controller may pay {R}{R}. If the player does, he or she may copy this spell and may choose a new target for that copy.').
card_image_name('chain lightning'/'ME3', 'chain lightning').
card_uid('chain lightning'/'ME3', 'ME3:Chain Lightning:chain lightning').
card_rarity('chain lightning'/'ME3', 'Common').
card_artist('chain lightning'/'ME3', 'Sandra Everingham').
card_number('chain lightning'/'ME3', '90').
card_multiverse_id('chain lightning'/'ME3', '201126').

card_in_set('chromium', 'ME3').
card_original_type('chromium'/'ME3', 'Legendary Creature — Elder Dragon').
card_original_text('chromium'/'ME3', 'Flying\nRampage 2 (Whenever this creature becomes blocked, it gets +2/+2 until end of turn for each creature blocking it beyond the first.)\nAt the beginning of your upkeep, sacrifice Chromium unless you pay {W}{U}{B}.').
card_image_name('chromium'/'ME3', 'chromium').
card_uid('chromium'/'ME3', 'ME3:Chromium:chromium').
card_rarity('chromium'/'ME3', 'Rare').
card_artist('chromium'/'ME3', 'Edward P. Beard, Jr.').
card_number('chromium'/'ME3', '147').
card_multiverse_id('chromium'/'ME3', '201184').

card_in_set('cinder storm', 'ME3').
card_original_type('cinder storm'/'ME3', 'Sorcery').
card_original_text('cinder storm'/'ME3', 'Cinder Storm deals 7 damage to target creature or player.').
card_image_name('cinder storm'/'ME3', 'cinder storm').
card_uid('cinder storm'/'ME3', 'ME3:Cinder Storm:cinder storm').
card_rarity('cinder storm'/'ME3', 'Uncommon').
card_artist('cinder storm'/'ME3', 'Mark Tedin').
card_number('cinder storm'/'ME3', '91').
card_flavor_text('cinder storm'/'ME3', 'When the sky\'s rain has turned to fire, what will put it out?').
card_multiverse_id('cinder storm'/'ME3', '201323').

card_in_set('city of shadows', 'ME3').
card_original_type('city of shadows'/'ME3', 'Land').
card_original_text('city of shadows'/'ME3', '{T}, Exile a creature you control: Put a storage counter on City of Shadows.\n{T}: Add {X} to your mana pool, where X is the number of storage counters on City of Shadows.').
card_image_name('city of shadows'/'ME3', 'city of shadows').
card_uid('city of shadows'/'ME3', 'ME3:City of Shadows:city of shadows').
card_rarity('city of shadows'/'ME3', 'Rare').
card_artist('city of shadows'/'ME3', 'Tom Wänerstrand').
card_number('city of shadows'/'ME3', '206').
card_multiverse_id('city of shadows'/'ME3', '201252').

card_in_set('cleanse', 'ME3').
card_original_type('cleanse'/'ME3', 'Sorcery').
card_original_text('cleanse'/'ME3', 'Destroy all black creatures.').
card_image_name('cleanse'/'ME3', 'cleanse').
card_uid('cleanse'/'ME3', 'ME3:Cleanse:cleanse').
card_rarity('cleanse'/'ME3', 'Rare').
card_artist('cleanse'/'ME3', 'Phil Foglio').
card_number('cleanse'/'ME3', '5').
card_flavor_text('cleanse'/'ME3', 'The clouds broke and the sun\'s rays burst forth; each foul beast in its turn faltered, and was gone.').
card_multiverse_id('cleanse'/'ME3', '201127').

card_in_set('coal golem', 'ME3').
card_original_type('coal golem'/'ME3', 'Artifact Creature — Golem').
card_original_text('coal golem'/'ME3', '{3}, Sacrifice Coal Golem: Add {R}{R}{R} to your mana pool.').
card_image_name('coal golem'/'ME3', 'coal golem').
card_uid('coal golem'/'ME3', 'ME3:Coal Golem:coal golem').
card_rarity('coal golem'/'ME3', 'Common').
card_artist('coal golem'/'ME3', 'Christopher Rush').
card_number('coal golem'/'ME3', '193').
card_flavor_text('coal golem'/'ME3', '\"Three such creatures stood burning at the crest of the hill. Only seconds later, the fireball struck our front line.\"\n—Lydia, Countess Brellis').
card_multiverse_id('coal golem'/'ME3', '201206').

card_in_set('concordant crossroads', 'ME3').
card_original_type('concordant crossroads'/'ME3', 'World Enchantment').
card_original_text('concordant crossroads'/'ME3', 'All creatures have haste.').
card_image_name('concordant crossroads'/'ME3', 'concordant crossroads').
card_uid('concordant crossroads'/'ME3', 'ME3:Concordant Crossroads:concordant crossroads').
card_rarity('concordant crossroads'/'ME3', 'Rare').
card_artist('concordant crossroads'/'ME3', 'Amy Weber').
card_number('concordant crossroads'/'ME3', '114').
card_multiverse_id('concordant crossroads'/'ME3', '201185').

card_in_set('corrupt eunuchs', 'ME3').
card_original_type('corrupt eunuchs'/'ME3', 'Creature — Human Advisor').
card_original_text('corrupt eunuchs'/'ME3', 'When Corrupt Eunuchs enters the battlefield, it deals 2 damage to target creature.').
card_image_name('corrupt eunuchs'/'ME3', 'corrupt eunuchs').
card_uid('corrupt eunuchs'/'ME3', 'ME3:Corrupt Eunuchs:corrupt eunuchs').
card_rarity('corrupt eunuchs'/'ME3', 'Uncommon').
card_artist('corrupt eunuchs'/'ME3', 'Li Yousong').
card_number('corrupt eunuchs'/'ME3', '92').
card_multiverse_id('corrupt eunuchs'/'ME3', '201408').

card_in_set('cosmic horror', 'ME3').
card_original_type('cosmic horror'/'ME3', 'Creature — Horror').
card_original_text('cosmic horror'/'ME3', 'First strike\nAt the beginning of your upkeep, unless you pay {3}{B}{B}{B}, sacrifice Cosmic Horror and it deals 7 damage to you.').
card_image_name('cosmic horror'/'ME3', 'cosmic horror').
card_uid('cosmic horror'/'ME3', 'ME3:Cosmic Horror:cosmic horror').
card_rarity('cosmic horror'/'ME3', 'Rare').
card_artist('cosmic horror'/'ME3', 'Jesper Myrfors').
card_number('cosmic horror'/'ME3', '61').
card_flavor_text('cosmic horror'/'ME3', '\"Then flashed the living lightning from her eyes,/ And screams of horror rend th\' affrighted skies.\"\n—Alexander Pope, The Rape of the Lock').
card_multiverse_id('cosmic horror'/'ME3', '201129').

card_in_set('crimson kobolds', 'ME3').
card_original_type('crimson kobolds'/'ME3', 'Creature — — Kobold').
card_original_text('crimson kobolds'/'ME3', 'Crimson Kobolds is red.').
card_image_name('crimson kobolds'/'ME3', 'crimson kobolds').
card_uid('crimson kobolds'/'ME3', 'ME3:Crimson Kobolds:crimson kobolds').
card_rarity('crimson kobolds'/'ME3', 'Common').
card_artist('crimson kobolds'/'ME3', 'Anson Maddocks').
card_number('crimson kobolds'/'ME3', '93').
card_flavor_text('crimson kobolds'/'ME3', '\"Kobolds are harmless.\"\n—Bearand the Bold, epitaph').
card_multiverse_id('crimson kobolds'/'ME3', '201130').

card_in_set('crimson manticore', 'ME3').
card_original_type('crimson manticore'/'ME3', 'Creature — Manticore').
card_original_text('crimson manticore'/'ME3', 'Flying\n{R}, {T}: Crimson Manticore deals 1 damage to target attacking or blocking creature.').
card_image_name('crimson manticore'/'ME3', 'crimson manticore').
card_uid('crimson manticore'/'ME3', 'ME3:Crimson Manticore:crimson manticore').
card_rarity('crimson manticore'/'ME3', 'Uncommon').
card_artist('crimson manticore'/'ME3', 'Daniel Gelon').
card_number('crimson manticore'/'ME3', '94').
card_flavor_text('crimson manticore'/'ME3', 'Noted neither for their good looks nor their charm, manticores can be fearsome allies. As dinner companions, however, they are best left alone.').
card_multiverse_id('crimson manticore'/'ME3', '201131').

card_in_set('d\'avenant archer', 'ME3').
card_original_type('d\'avenant archer'/'ME3', 'Creature — Human Soldier Archer').
card_original_text('d\'avenant archer'/'ME3', '{T}: D\'Avenant Archer deals 1 damage to target attacking or blocking creature.').
card_image_name('d\'avenant archer'/'ME3', 'd\'avenant archer').
card_uid('d\'avenant archer'/'ME3', 'ME3:D\'Avenant Archer:d\'avenant archer').
card_rarity('d\'avenant archer'/'ME3', 'Common').
card_artist('d\'avenant archer'/'ME3', 'Douglas Shuler').
card_number('d\'avenant archer'/'ME3', '6').
card_flavor_text('d\'avenant archer'/'ME3', 'Avenant\'s archers are also trained as poets, so that each arrow is guided by a fragment of verse.').
card_multiverse_id('d\'avenant archer'/'ME3', '201134').

card_in_set('dance of many', 'ME3').
card_original_type('dance of many'/'ME3', 'Enchantment').
card_original_text('dance of many'/'ME3', 'When Dance of Many enters the battlefield, put a token onto the battlefield that\'s a copy of target nontoken creature.\nWhen Dance of Many leaves the battlefield, exile the creature token.\nWhen the creature token leaves the battlefield, sacrifice Dance of Many.\nAt the beginning of your upkeep, sacrifice Dance of Many unless you pay {U}{U}.').
card_image_name('dance of many'/'ME3', 'dance of many').
card_uid('dance of many'/'ME3', 'ME3:Dance of Many:dance of many').
card_rarity('dance of many'/'ME3', 'Uncommon').
card_artist('dance of many'/'ME3', 'Sandra Everingham').
card_number('dance of many'/'ME3', '34').
card_multiverse_id('dance of many'/'ME3', '201253').

card_in_set('demonic torment', 'ME3').
card_original_type('demonic torment'/'ME3', 'Enchantment — Aura').
card_original_text('demonic torment'/'ME3', 'Enchant creature\nEnchanted creature can\'t attack.\nPrevent all combat damage that would be dealt by enchanted creature.').
card_image_name('demonic torment'/'ME3', 'demonic torment').
card_uid('demonic torment'/'ME3', 'ME3:Demonic Torment:demonic torment').
card_rarity('demonic torment'/'ME3', 'Common').
card_artist('demonic torment'/'ME3', 'Anson Maddocks').
card_number('demonic torment'/'ME3', '62').
card_multiverse_id('demonic torment'/'ME3', '201166').

card_in_set('desert twister', 'ME3').
card_original_type('desert twister'/'ME3', 'Sorcery').
card_original_text('desert twister'/'ME3', 'Destroy target permanent.').
card_image_name('desert twister'/'ME3', 'desert twister').
card_uid('desert twister'/'ME3', 'ME3:Desert Twister:desert twister').
card_rarity('desert twister'/'ME3', 'Uncommon').
card_artist('desert twister'/'ME3', 'Susan Van Camp').
card_number('desert twister'/'ME3', '115').
card_multiverse_id('desert twister'/'ME3', '201210').

card_in_set('desperate charge', 'ME3').
card_original_type('desperate charge'/'ME3', 'Sorcery').
card_original_text('desperate charge'/'ME3', 'Creatures you control get +2/+0 until end of turn.').
card_image_name('desperate charge'/'ME3', 'desperate charge').
card_uid('desperate charge'/'ME3', 'ME3:Desperate Charge:desperate charge').
card_rarity('desperate charge'/'ME3', 'Common').
card_artist('desperate charge'/'ME3', 'Chen Weidong').
card_number('desperate charge'/'ME3', '63').
card_flavor_text('desperate charge'/'ME3', '\"Lieutenants dishonored, corpses carted home; The general raises troops again to take revenge.\"').
card_multiverse_id('desperate charge'/'ME3', '206068').

card_in_set('didgeridoo', 'ME3').
card_original_type('didgeridoo'/'ME3', 'Artifact').
card_original_text('didgeridoo'/'ME3', '{3}: You may put a Minotaur permanent card from your hand onto the battlefield.').
card_image_name('didgeridoo'/'ME3', 'didgeridoo').
card_uid('didgeridoo'/'ME3', 'ME3:Didgeridoo:didgeridoo').
card_rarity('didgeridoo'/'ME3', 'Uncommon').
card_artist('didgeridoo'/'ME3', 'Melissa A. Benson').
card_number('didgeridoo'/'ME3', '194').
card_flavor_text('didgeridoo'/'ME3', '\"Play the song of he who delivered us. Play the song of Feroz.\"\n—Onatah, Anaba Shaman').
card_multiverse_id('didgeridoo'/'ME3', '201317').

card_in_set('disenchant', 'ME3').
card_original_type('disenchant'/'ME3', 'Instant').
card_original_text('disenchant'/'ME3', 'Destroy target artifact or enchantment.').
card_image_name('disenchant'/'ME3', 'disenchant').
card_uid('disenchant'/'ME3', 'ME3:Disenchant:disenchant').
card_rarity('disenchant'/'ME3', 'Common').
card_artist('disenchant'/'ME3', 'Amy Weber').
card_number('disenchant'/'ME3', '7').
card_multiverse_id('disenchant'/'ME3', '201162').

card_in_set('disharmony', 'ME3').
card_original_type('disharmony'/'ME3', 'Instant').
card_original_text('disharmony'/'ME3', 'Cast Disharmony only during combat before blockers are declared. \nUntap target attacking creature and remove it from combat. Gain control of that creature until end of turn.').
card_image_name('disharmony'/'ME3', 'disharmony').
card_uid('disharmony'/'ME3', 'ME3:Disharmony:disharmony').
card_rarity('disharmony'/'ME3', 'Uncommon').
card_artist('disharmony'/'ME3', 'Bryon Wackwitz').
card_number('disharmony'/'ME3', '95').
card_multiverse_id('disharmony'/'ME3', '201133').

card_in_set('divine intervention', 'ME3').
card_original_type('divine intervention'/'ME3', 'Enchantment').
card_original_text('divine intervention'/'ME3', 'Divine Intervention enters the battlefield with two intervention counters on it.\nAt the beginning of your upkeep, remove an intervention counter from Divine Intervention. If there are no intervention counters on it, the game is a draw.').
card_image_name('divine intervention'/'ME3', 'divine intervention').
card_uid('divine intervention'/'ME3', 'ME3:Divine Intervention:divine intervention').
card_rarity('divine intervention'/'ME3', 'Rare').
card_artist('divine intervention'/'ME3', 'Anthony S. Waters').
card_number('divine intervention'/'ME3', '8').
card_multiverse_id('divine intervention'/'ME3', '201237').

card_in_set('dong zhou, the tyrant', 'ME3').
card_original_type('dong zhou, the tyrant'/'ME3', 'Legendary Creature — Human Soldier').
card_original_text('dong zhou, the tyrant'/'ME3', 'When Dong Zhou, the Tyrant enters the battlefield, target creature an opponent controls deals damage equal to its power to that player.').
card_image_name('dong zhou, the tyrant'/'ME3', 'dong zhou, the tyrant').
card_uid('dong zhou, the tyrant'/'ME3', 'ME3:Dong Zhou, the Tyrant:dong zhou, the tyrant').
card_rarity('dong zhou, the tyrant'/'ME3', 'Rare').
card_artist('dong zhou, the tyrant'/'ME3', 'Inoue Junichi').
card_number('dong zhou, the tyrant'/'ME3', '96').
card_multiverse_id('dong zhou, the tyrant'/'ME3', '201123').

card_in_set('eightfold maze', 'ME3').
card_original_type('eightfold maze'/'ME3', 'Instant').
card_original_text('eightfold maze'/'ME3', 'Cast Eightfold Maze only during the declare attackers step and only if you are the defending player.\nDestroy target attacking creature.').
card_image_name('eightfold maze'/'ME3', 'eightfold maze').
card_uid('eightfold maze'/'ME3', 'ME3:Eightfold Maze:eightfold maze').
card_rarity('eightfold maze'/'ME3', 'Uncommon').
card_artist('eightfold maze'/'ME3', 'Shang Huitong').
card_number('eightfold maze'/'ME3', '9').
card_multiverse_id('eightfold maze'/'ME3', '201304').

card_in_set('elves of deep shadow', 'ME3').
card_original_type('elves of deep shadow'/'ME3', 'Creature — Elf Druid').
card_original_text('elves of deep shadow'/'ME3', '{T}: Add {B} to your mana pool. Elves of Deep Shadow deals 1 damage to you.').
card_image_name('elves of deep shadow'/'ME3', 'elves of deep shadow').
card_uid('elves of deep shadow'/'ME3', 'ME3:Elves of Deep Shadow:elves of deep shadow').
card_rarity('elves of deep shadow'/'ME3', 'Common').
card_artist('elves of deep shadow'/'ME3', 'Jesper Myrfors').
card_number('elves of deep shadow'/'ME3', '116').
card_flavor_text('elves of deep shadow'/'ME3', 'They are aberrations who have turned on everything we hold sacred. Let them be cast out.\"\n—Ailheen, Speaker of the Council').
card_multiverse_id('elves of deep shadow'/'ME3', '201324').

card_in_set('evil presence', 'ME3').
card_original_type('evil presence'/'ME3', 'Enchantment — Aura').
card_original_text('evil presence'/'ME3', 'Enchant land\nEnchanted land is a Swamp.').
card_image_name('evil presence'/'ME3', 'evil presence').
card_uid('evil presence'/'ME3', 'ME3:Evil Presence:evil presence').
card_rarity('evil presence'/'ME3', 'Common').
card_artist('evil presence'/'ME3', 'Sandra Everingham').
card_number('evil presence'/'ME3', '64').
card_multiverse_id('evil presence'/'ME3', '201261').

card_in_set('exorcist', 'ME3').
card_original_type('exorcist'/'ME3', 'Creature — Human Cleric').
card_original_text('exorcist'/'ME3', '{1}{W}, {T}: Destroy target black creature.').
card_image_name('exorcist'/'ME3', 'exorcist').
card_uid('exorcist'/'ME3', 'ME3:Exorcist:exorcist').
card_rarity('exorcist'/'ME3', 'Uncommon').
card_artist('exorcist'/'ME3', 'Drew Tucker').
card_number('exorcist'/'ME3', '10').
card_flavor_text('exorcist'/'ME3', 'Though they often bore little greater charm than the demons they battled, exorcists were always welcome in Scarwood.').
card_multiverse_id('exorcist'/'ME3', '201255').

card_in_set('faerie noble', 'ME3').
card_original_type('faerie noble'/'ME3', 'Creature — Faerie').
card_original_text('faerie noble'/'ME3', 'Flying\nOther Faerie creatures you control get +0/+1.\n{T}: Other Faerie creatures you control get +1/+0 until end of turn.').
card_image_name('faerie noble'/'ME3', 'faerie noble').
card_uid('faerie noble'/'ME3', 'ME3:Faerie Noble:faerie noble').
card_rarity('faerie noble'/'ME3', 'Uncommon').
card_artist('faerie noble'/'ME3', 'Susan Van Camp').
card_number('faerie noble'/'ME3', '117').
card_flavor_text('faerie noble'/'ME3', '\"Faeries talk all in riddles and tricky bits, ‘cept the Nobles. Now, there\'s some straight talkers.\"\n—Joskun, An-Havva Constable').
card_multiverse_id('faerie noble'/'ME3', '201318').

card_in_set('false defeat', 'ME3').
card_original_type('false defeat'/'ME3', 'Sorcery').
card_original_text('false defeat'/'ME3', 'Return target creature card from your graveyard to the battlefield.').
card_image_name('false defeat'/'ME3', 'false defeat').
card_uid('false defeat'/'ME3', 'ME3:False Defeat:false defeat').
card_rarity('false defeat'/'ME3', 'Uncommon').
card_artist('false defeat'/'ME3', 'Li Wang').
card_number('false defeat'/'ME3', '11').
card_flavor_text('false defeat'/'ME3', '\"All warfare is based on deception.\"\n—Sun Tzu, Art of War (trans. Giles)').
card_multiverse_id('false defeat'/'ME3', '201275').

card_in_set('famine', 'ME3').
card_original_type('famine'/'ME3', 'Sorcery').
card_original_text('famine'/'ME3', 'Famine deals 3 damage to each creature and each player.').
card_image_name('famine'/'ME3', 'famine').
card_uid('famine'/'ME3', 'ME3:Famine:famine').
card_rarity('famine'/'ME3', 'Uncommon').
card_artist('famine'/'ME3', 'Sun Nan').
card_number('famine'/'ME3', '65').
card_flavor_text('famine'/'ME3', '\"But it was a year of dearth. People were reduced to eating leaves of jujube trees. Corpses were seen everywhere in the countryside.\"').
card_multiverse_id('famine'/'ME3', '201276').

card_in_set('fellwar stone', 'ME3').
card_original_type('fellwar stone'/'ME3', 'Artifact').
card_original_text('fellwar stone'/'ME3', '{T}: Add to your mana pool one mana of any color that a land an opponent controls could produce.').
card_image_name('fellwar stone'/'ME3', 'fellwar stone').
card_uid('fellwar stone'/'ME3', 'ME3:Fellwar Stone:fellwar stone').
card_rarity('fellwar stone'/'ME3', 'Common').
card_artist('fellwar stone'/'ME3', 'Quinton Hoover').
card_number('fellwar stone'/'ME3', '195').
card_flavor_text('fellwar stone'/'ME3', '\"What do you have that I cannot obtain?\"\n—Mairsil, called the Pretender').
card_multiverse_id('fellwar stone'/'ME3', '201228').

card_in_set('fevered strength', 'ME3').
card_original_type('fevered strength'/'ME3', 'Instant').
card_original_text('fevered strength'/'ME3', 'Target creature gets +2/+0 until end of turn.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_image_name('fevered strength'/'ME3', 'fevered strength').
card_uid('fevered strength'/'ME3', 'ME3:Fevered Strength:fevered strength').
card_rarity('fevered strength'/'ME3', 'Common').
card_artist('fevered strength'/'ME3', 'Brian Snõddy').
card_number('fevered strength'/'ME3', '66').
card_flavor_text('fevered strength'/'ME3', '\"The burst of strength brought on by this plague should not be mistaken for a sign of renewed health.\"\n—Kolbjörn, High Honored Druid').
card_multiverse_id('fevered strength'/'ME3', '201169').

card_in_set('fire ambush', 'ME3').
card_original_type('fire ambush'/'ME3', 'Sorcery').
card_original_text('fire ambush'/'ME3', 'Fire Ambush deals 3 damage to target creature or player.').
card_image_name('fire ambush'/'ME3', 'fire ambush').
card_uid('fire ambush'/'ME3', 'ME3:Fire Ambush:fire ambush').
card_rarity('fire ambush'/'ME3', 'Common').
card_artist('fire ambush'/'ME3', 'Tang Xiaogu').
card_number('fire ambush'/'ME3', '97').
card_flavor_text('fire ambush'/'ME3', '\"With fire he broke the battle at Bowang . . . . Striking fear deep into Cao Cao\'s soul, thus Kongming scored a coup at his debut.\"').
card_multiverse_id('fire ambush'/'ME3', '201277').

card_in_set('fire drake', 'ME3').
card_original_type('fire drake'/'ME3', 'Creature — Drake').
card_original_text('fire drake'/'ME3', 'Flying\n{R}: Fire Drake gets +1/+0 until end of turn. Activate this ability only once each turn.').
card_image_name('fire drake'/'ME3', 'fire drake').
card_uid('fire drake'/'ME3', 'ME3:Fire Drake:fire drake').
card_rarity('fire drake'/'ME3', 'Common').
card_artist('fire drake'/'ME3', 'Christopher Rush').
card_number('fire drake'/'ME3', '98').
card_flavor_text('fire drake'/'ME3', 'Brimstone marks this drake\'s territory. Unfortunately for travelers, all of the Burning Isles smell likewise.').
card_multiverse_id('fire drake'/'ME3', '201256').

card_in_set('fire sprites', 'ME3').
card_original_type('fire sprites'/'ME3', 'Creature — Faerie').
card_original_text('fire sprites'/'ME3', 'Flying\n{G}, {T}: Add {R} to your mana pool.').
card_image_name('fire sprites'/'ME3', 'fire sprites').
card_uid('fire sprites'/'ME3', 'ME3:Fire Sprites:fire sprites').
card_rarity('fire sprites'/'ME3', 'Common').
card_artist('fire sprites'/'ME3', 'Julie Baroh').
card_number('fire sprites'/'ME3', '118').
card_multiverse_id('fire sprites'/'ME3', '201186').

card_in_set('firestorm phoenix', 'ME3').
card_original_type('firestorm phoenix'/'ME3', 'Creature — Phoenix').
card_original_text('firestorm phoenix'/'ME3', 'Flying\nIf Firestorm Phoenix would be put into a graveyard from the battlefield, return Firestorm Phoenix to its owner\'s hand instead. It can\'t be cast again until its owner\'s next turn.').
card_image_name('firestorm phoenix'/'ME3', 'firestorm phoenix').
card_uid('firestorm phoenix'/'ME3', 'ME3:Firestorm Phoenix:firestorm phoenix').
card_rarity('firestorm phoenix'/'ME3', 'Rare').
card_artist('firestorm phoenix'/'ME3', 'Jeff A. Menges').
card_number('firestorm phoenix'/'ME3', '99').
card_flavor_text('firestorm phoenix'/'ME3', '\"The bird of wonder dies, the maiden phoenix,/ Her ashes new-create another heir/ As great in admiration as herself.\"\n—William Shakespeare, King Henry the Eighth.').
card_multiverse_id('firestorm phoenix'/'ME3', '201216').

card_in_set('flash flood', 'ME3').
card_original_type('flash flood'/'ME3', 'Instant').
card_original_text('flash flood'/'ME3', 'Choose one — Destroy target red permanent; or return target Mountain to its owner\'s hand.').
card_image_name('flash flood'/'ME3', 'flash flood').
card_uid('flash flood'/'ME3', 'ME3:Flash Flood:flash flood').
card_rarity('flash flood'/'ME3', 'Uncommon').
card_artist('flash flood'/'ME3', 'Tom Wänerstrand').
card_number('flash flood'/'ME3', '35').
card_flavor_text('flash flood'/'ME3', 'Many people say that no power can bring the mountains low. Many people are fools.').
card_multiverse_id('flash flood'/'ME3', '201137').

card_in_set('force spike', 'ME3').
card_original_type('force spike'/'ME3', 'Instant').
card_original_text('force spike'/'ME3', 'Counter target spell unless its controller pays {1}.').
card_image_name('force spike'/'ME3', 'force spike').
card_uid('force spike'/'ME3', 'ME3:Force Spike:force spike').
card_rarity('force spike'/'ME3', 'Common').
card_artist('force spike'/'ME3', 'Bryon Wackwitz').
card_number('force spike'/'ME3', '36').
card_multiverse_id('force spike'/'ME3', '201160').

card_in_set('forced retreat', 'ME3').
card_original_type('forced retreat'/'ME3', 'Sorcery').
card_original_text('forced retreat'/'ME3', 'Put target creature on top of its owner\'s library.').
card_image_name('forced retreat'/'ME3', 'forced retreat').
card_uid('forced retreat'/'ME3', 'ME3:Forced Retreat:forced retreat').
card_rarity('forced retreat'/'ME3', 'Common').
card_artist('forced retreat'/'ME3', 'Huang Qishi').
card_number('forced retreat'/'ME3', '37').
card_flavor_text('forced retreat'/'ME3', '\"Leadership, not numbers, determines victory.\"\n—A Wu commander, before his 5,000 troops forced 15,000 Wei troops to retreat from Ruxu').
card_multiverse_id('forced retreat'/'ME3', '201278').

card_in_set('forest', 'ME3').
card_original_type('forest'/'ME3', 'Basic Land — Forest').
card_original_text('forest'/'ME3', 'G').
card_image_name('forest'/'ME3', 'forest1').
card_uid('forest'/'ME3', 'ME3:Forest:forest1').
card_rarity('forest'/'ME3', 'Basic Land').
card_artist('forest'/'ME3', 'Ji Yong').
card_number('forest'/'ME3', '228').
card_multiverse_id('forest'/'ME3', '206069').

card_in_set('forest', 'ME3').
card_original_type('forest'/'ME3', 'Basic Land — Forest').
card_original_text('forest'/'ME3', 'G').
card_image_name('forest'/'ME3', 'forest2').
card_uid('forest'/'ME3', 'ME3:Forest:forest2').
card_rarity('forest'/'ME3', 'Basic Land').
card_artist('forest'/'ME3', 'Ji Yong').
card_number('forest'/'ME3', '229').
card_multiverse_id('forest'/'ME3', '206070').

card_in_set('forest', 'ME3').
card_original_type('forest'/'ME3', 'Basic Land — Forest').
card_original_text('forest'/'ME3', 'G').
card_image_name('forest'/'ME3', 'forest3').
card_uid('forest'/'ME3', 'ME3:Forest:forest3').
card_rarity('forest'/'ME3', 'Basic Land').
card_artist('forest'/'ME3', 'Ji Yong').
card_number('forest'/'ME3', '230').
card_multiverse_id('forest'/'ME3', '206071').

card_in_set('forked lightning', 'ME3').
card_original_type('forked lightning'/'ME3', 'Sorcery').
card_original_text('forked lightning'/'ME3', 'Forked Lightning deals 4 damage divided as you choose among one, two, or three target creatures.').
card_image_name('forked lightning'/'ME3', 'forked lightning').
card_uid('forked lightning'/'ME3', 'ME3:Forked Lightning:forked lightning').
card_rarity('forked lightning'/'ME3', 'Uncommon').
card_artist('forked lightning'/'ME3', 'Ted Naifeh').
card_number('forked lightning'/'ME3', '100').
card_multiverse_id('forked lightning'/'ME3', '201292').

card_in_set('freyalise\'s winds', 'ME3').
card_original_type('freyalise\'s winds'/'ME3', 'Enchantment').
card_original_text('freyalise\'s winds'/'ME3', 'Whenever a permanent becomes tapped, put a wind counter on it. \nIf a permanent with a wind counter on it would untap during its controller\'s untap step, remove all wind counters from it instead.').
card_image_name('freyalise\'s winds'/'ME3', 'freyalise\'s winds').
card_uid('freyalise\'s winds'/'ME3', 'ME3:Freyalise\'s Winds:freyalise\'s winds').
card_rarity('freyalise\'s winds'/'ME3', 'Rare').
card_artist('freyalise\'s winds'/'ME3', 'Mark Tedin').
card_number('freyalise\'s winds'/'ME3', '119').
card_multiverse_id('freyalise\'s winds'/'ME3', '201217').

card_in_set('frost giant', 'ME3').
card_original_type('frost giant'/'ME3', 'Creature — Giant').
card_original_text('frost giant'/'ME3', 'Rampage 2 (Whenever this creature becomes blocked, it gets +2/+2 until end of turn for each creature blocking it beyond the first.)').
card_image_name('frost giant'/'ME3', 'frost giant').
card_uid('frost giant'/'ME3', 'ME3:Frost Giant:frost giant').
card_rarity('frost giant'/'ME3', 'Uncommon').
card_artist('frost giant'/'ME3', 'Daniel Gelon').
card_number('frost giant'/'ME3', '101').
card_flavor_text('frost giant'/'ME3', 'The frost giants have been out in the cold a long, long time, but they have their rage to keep them warm.').
card_multiverse_id('frost giant'/'ME3', '201138').

card_in_set('gabriel angelfire', 'ME3').
card_original_type('gabriel angelfire'/'ME3', 'Legendary Creature — Angel').
card_original_text('gabriel angelfire'/'ME3', 'At the beginning of your upkeep, choose flying, first strike, trample, or rampage 3. Gabriel Angelfire gains that ability until your next upkeep.').
card_image_name('gabriel angelfire'/'ME3', 'gabriel angelfire').
card_uid('gabriel angelfire'/'ME3', 'ME3:Gabriel Angelfire:gabriel angelfire').
card_rarity('gabriel angelfire'/'ME3', 'Rare').
card_artist('gabriel angelfire'/'ME3', 'Daniel Gelon').
card_number('gabriel angelfire'/'ME3', '148').
card_multiverse_id('gabriel angelfire'/'ME3', '201188').

card_in_set('gaea\'s touch', 'ME3').
card_original_type('gaea\'s touch'/'ME3', 'Enchantment').
card_original_text('gaea\'s touch'/'ME3', 'You may play an additional land during your turn if that land is a basic Forest.\nSacrifice Gaea\'s Touch: Add {G}{G} to your mana pool.').
card_image_name('gaea\'s touch'/'ME3', 'gaea\'s touch').
card_uid('gaea\'s touch'/'ME3', 'ME3:Gaea\'s Touch:gaea\'s touch').
card_rarity('gaea\'s touch'/'ME3', 'Uncommon').
card_artist('gaea\'s touch'/'ME3', 'Mark Poole').
card_number('gaea\'s touch'/'ME3', '120').
card_multiverse_id('gaea\'s touch'/'ME3', '201211').

card_in_set('gauntlets of chaos', 'ME3').
card_original_type('gauntlets of chaos'/'ME3', 'Artifact').
card_original_text('gauntlets of chaos'/'ME3', '{5}, Sacrifice Gauntlets of Chaos: Exchange control of target artifact, creature, or land an opponent controls and a permanent you control if they share one of those types. Destroy all Auras attached to those permanents.').
card_image_name('gauntlets of chaos'/'ME3', 'gauntlets of chaos').
card_uid('gauntlets of chaos'/'ME3', 'ME3:Gauntlets of Chaos:gauntlets of chaos').
card_rarity('gauntlets of chaos'/'ME3', 'Rare').
card_artist('gauntlets of chaos'/'ME3', 'Dan Frazier').
card_number('gauntlets of chaos'/'ME3', '196').
card_multiverse_id('gauntlets of chaos'/'ME3', '201235').

card_in_set('ghostly visit', 'ME3').
card_original_type('ghostly visit'/'ME3', 'Sorcery').
card_original_text('ghostly visit'/'ME3', 'Destroy target nonblack creature.').
card_image_name('ghostly visit'/'ME3', 'ghostly visit').
card_uid('ghostly visit'/'ME3', 'ME3:Ghostly Visit:ghostly visit').
card_rarity('ghostly visit'/'ME3', 'Common').
card_artist('ghostly visit'/'ME3', 'Kang Yu').
card_number('ghostly visit'/'ME3', '67').
card_flavor_text('ghostly visit'/'ME3', 'Cao Cao was haunted to death by the ghosts of the empresses and other high courtiers he had murdered.').
card_multiverse_id('ghostly visit'/'ME3', '201279').

card_in_set('ghosts of the damned', 'ME3').
card_original_type('ghosts of the damned'/'ME3', 'Creature — Spirit').
card_original_text('ghosts of the damned'/'ME3', '{T}: Target creature gets -1/-0 until end of turn.').
card_image_name('ghosts of the damned'/'ME3', 'ghosts of the damned').
card_uid('ghosts of the damned'/'ME3', 'ME3:Ghosts of the Damned:ghosts of the damned').
card_rarity('ghosts of the damned'/'ME3', 'Common').
card_artist('ghosts of the damned'/'ME3', 'Edward P. Beard, Jr.').
card_number('ghosts of the damned'/'ME3', '68').
card_flavor_text('ghosts of the damned'/'ME3', 'The voices of the dead ring in the heart long after they have faded from the ears.').
card_multiverse_id('ghosts of the damned'/'ME3', '201139').

card_in_set('giant growth', 'ME3').
card_original_type('giant growth'/'ME3', 'Instant').
card_original_text('giant growth'/'ME3', 'Target creature gets +3/+3 until end of turn.').
card_image_name('giant growth'/'ME3', 'giant growth').
card_uid('giant growth'/'ME3', 'ME3:Giant Growth:giant growth').
card_rarity('giant growth'/'ME3', 'Common').
card_artist('giant growth'/'ME3', 'DiTerlizzi').
card_number('giant growth'/'ME3', '121').
card_multiverse_id('giant growth'/'ME3', '201266').

card_in_set('grim tutor', 'ME3').
card_original_type('grim tutor'/'ME3', 'Sorcery').
card_original_text('grim tutor'/'ME3', 'Search your library for a card and put that card into your hand, then shuffle your library. You lose 3 life.').
card_image_name('grim tutor'/'ME3', 'grim tutor').
card_uid('grim tutor'/'ME3', 'ME3:Grim Tutor:grim tutor').
card_rarity('grim tutor'/'ME3', 'Rare').
card_artist('grim tutor'/'ME3', 'Mark Tedin').
card_number('grim tutor'/'ME3', '69').
card_flavor_text('grim tutor'/'ME3', 'One who goes unpunished never learns.\n—Greek proverb').
card_multiverse_id('grim tutor'/'ME3', '201409').

card_in_set('guan yu\'s 1,000-li march', 'ME3').
card_original_type('guan yu\'s 1,000-li march'/'ME3', 'Sorcery').
card_original_text('guan yu\'s 1,000-li march'/'ME3', 'Destroy all tapped creatures.').
card_image_name('guan yu\'s 1,000-li march'/'ME3', 'guan yu\'s 1,000-li march').
card_uid('guan yu\'s 1,000-li march'/'ME3', 'ME3:Guan Yu\'s 1,000-Li March:guan yu\'s 1,000-li march').
card_rarity('guan yu\'s 1,000-li march'/'ME3', 'Rare').
card_artist('guan yu\'s 1,000-li march'/'ME3', 'Yang Guangmai').
card_number('guan yu\'s 1,000-li march'/'ME3', '13').
card_flavor_text('guan yu\'s 1,000-li march'/'ME3', '\"He Guan Yu covered the ground on a thousand-li horse;/ With dragon blade he took each pass by force.\"').
card_multiverse_id('guan yu\'s 1,000-li march'/'ME3', '201284').

card_in_set('guan yu, sainted warrior', 'ME3').
card_original_type('guan yu, sainted warrior'/'ME3', 'Legendary Creature — Human Soldier Warrior').
card_original_text('guan yu, sainted warrior'/'ME3', 'Horsemanship\nWhen Guan Yu is put into your graveyard from the battlefield, you may shuffle Guan Yu into your library.').
card_image_name('guan yu, sainted warrior'/'ME3', 'guan yu, sainted warrior').
card_uid('guan yu, sainted warrior'/'ME3', 'ME3:Guan Yu, Sainted Warrior:guan yu, sainted warrior').
card_rarity('guan yu, sainted warrior'/'ME3', 'Uncommon').
card_artist('guan yu, sainted warrior'/'ME3', 'Qiao Dafu').
card_number('guan yu, sainted warrior'/'ME3', '12').
card_multiverse_id('guan yu, sainted warrior'/'ME3', '201280').

card_in_set('gwendlyn di corci', 'ME3').
card_original_type('gwendlyn di corci'/'ME3', 'Legendary Creature — Human Rogue').
card_original_text('gwendlyn di corci'/'ME3', '{T}: Target player discards a card at random. Activate this ability only during your turn.').
card_image_name('gwendlyn di corci'/'ME3', 'gwendlyn di corci').
card_uid('gwendlyn di corci'/'ME3', 'ME3:Gwendlyn Di Corci:gwendlyn di corci').
card_rarity('gwendlyn di corci'/'ME3', 'Rare').
card_artist('gwendlyn di corci'/'ME3', 'Julie Baroh').
card_number('gwendlyn di corci'/'ME3', '149').
card_flavor_text('gwendlyn di corci'/'ME3', 'Urborg became the final resting place of many swordsmen of renown, their steel outmatched by the wiles of Gwendlyn.').
card_multiverse_id('gwendlyn di corci'/'ME3', '201191').

card_in_set('halfdane', 'ME3').
card_original_type('halfdane'/'ME3', 'Legendary Creature — Shapeshifter').
card_original_text('halfdane'/'ME3', 'At the beginning of your upkeep, Halfdane\'s power and toughness become equal to the power and toughness of target creature other than Halfdane, until the end of your next upkeep.').
card_image_name('halfdane'/'ME3', 'halfdane').
card_uid('halfdane'/'ME3', 'ME3:Halfdane:halfdane').
card_rarity('halfdane'/'ME3', 'Rare').
card_artist('halfdane'/'ME3', 'Melissa A. Benson').
card_number('halfdane'/'ME3', '150').
card_flavor_text('halfdane'/'ME3', 'Hail from Tolaria the ever changing \'Dane.').
card_multiverse_id('halfdane'/'ME3', '201192').

card_in_set('hammerheim', 'ME3').
card_original_type('hammerheim'/'ME3', 'Legendary Land').
card_original_text('hammerheim'/'ME3', '{T}: Add {R} to your mana pool.\n{T}: Target creature loses all landwalk abilities until end of turn.').
card_image_name('hammerheim'/'ME3', 'hammerheim').
card_uid('hammerheim'/'ME3', 'ME3:Hammerheim:hammerheim').
card_rarity('hammerheim'/'ME3', 'Uncommon').
card_artist('hammerheim'/'ME3', 'Bryon Wackwitz').
card_number('hammerheim'/'ME3', '207').
card_flavor_text('hammerheim'/'ME3', '\"‘Tis distance lends enchantment to the view,/ And robes the mountain in its azure hue.\" —Thomas Campbell, \"Pleasures of Hope\"').
card_multiverse_id('hammerheim'/'ME3', '201242').

card_in_set('hazezon tamar', 'ME3').
card_original_type('hazezon tamar'/'ME3', 'Legendary Creature — Human Warrior').
card_original_text('hazezon tamar'/'ME3', 'At the beginning of your upkeep, if Hazezon Tamar entered the battlefield since the beginning of your last upkeep, put a 1/1 red, green, and white Sand Warrior creature token onto the battlefield for each land you control. Those creatures have \"When a permanent named Hazezon Tamar isn\'t on the battlefield, exile this creature.\"').
card_image_name('hazezon tamar'/'ME3', 'hazezon tamar').
card_uid('hazezon tamar'/'ME3', 'ME3:Hazezon Tamar:hazezon tamar').
card_rarity('hazezon tamar'/'ME3', 'Rare').
card_artist('hazezon tamar'/'ME3', 'Richard Kane Ferguson').
card_number('hazezon tamar'/'ME3', '151').
card_multiverse_id('hazezon tamar'/'ME3', '201193').

card_in_set('heal', 'ME3').
card_original_type('heal'/'ME3', 'Instant').
card_original_text('heal'/'ME3', 'Prevent the next 1 damage that would be dealt to target creature or player this turn.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_image_name('heal'/'ME3', 'heal').
card_uid('heal'/'ME3', 'ME3:Heal:heal').
card_rarity('heal'/'ME3', 'Common').
card_artist('heal'/'ME3', 'Mark Tedin').
card_number('heal'/'ME3', '14').
card_flavor_text('heal'/'ME3', '\"Sometimes even the smallest boon can save a life.\"\n—Halvor Arenson, Kjeldoran Priest').
card_multiverse_id('heal'/'ME3', '201291').

card_in_set('heavy fog', 'ME3').
card_original_type('heavy fog'/'ME3', 'Instant').
card_original_text('heavy fog'/'ME3', 'Cast Heavy Fog only during the declare attackers step and only if you are the defending player.\nPrevent all damage that would be dealt to you this turn by attacking creatures.').
card_image_name('heavy fog'/'ME3', 'heavy fog').
card_uid('heavy fog'/'ME3', 'ME3:Heavy Fog:heavy fog').
card_rarity('heavy fog'/'ME3', 'Common').
card_artist('heavy fog'/'ME3', 'Liu Shangying').
card_number('heavy fog'/'ME3', '122').
card_multiverse_id('heavy fog'/'ME3', '201281').

card_in_set('hellfire', 'ME3').
card_original_type('hellfire'/'ME3', 'Sorcery').
card_original_text('hellfire'/'ME3', 'Destroy all nonblack creatures. Hellfire deals X plus 3 damage to you, where X is the number of creatures put into all graveyards this way.').
card_image_name('hellfire'/'ME3', 'hellfire').
card_uid('hellfire'/'ME3', 'ME3:Hellfire:hellfire').
card_rarity('hellfire'/'ME3', 'Rare').
card_artist('hellfire'/'ME3', 'Pete Venters').
card_number('hellfire'/'ME3', '70').
card_flavor_text('hellfire'/'ME3', '\"High on a throne of royal state . . . insatiate to pursue vain war with heav\'n.\" —John Milton, Paradise Lost').
card_multiverse_id('hellfire'/'ME3', '201141').

card_in_set('hua tuo, honored physician', 'ME3').
card_original_type('hua tuo, honored physician'/'ME3', 'Legendary Creature — Human').
card_original_text('hua tuo, honored physician'/'ME3', '{T}: Put target creature card from your graveyard on top of your library. Activate this ability only during your turn, before attackers are declared.').
card_image_name('hua tuo, honored physician'/'ME3', 'hua tuo, honored physician').
card_uid('hua tuo, honored physician'/'ME3', 'ME3:Hua Tuo, Honored Physician:hua tuo, honored physician').
card_rarity('hua tuo, honored physician'/'ME3', 'Rare').
card_artist('hua tuo, honored physician'/'ME3', 'Gao Jianzhang').
card_number('hua tuo, honored physician'/'ME3', '123').
card_multiverse_id('hua tuo, honored physician'/'ME3', '201273').

card_in_set('hunding gjornersen', 'ME3').
card_original_type('hunding gjornersen'/'ME3', 'Legendary Creature — Human Warrior').
card_original_text('hunding gjornersen'/'ME3', 'Rampage 1 (Whenever this creature becomes blocked, it gets +1/+1 until end of turn for each creature blocking it beyond the first.)').
card_image_name('hunding gjornersen'/'ME3', 'hunding gjornersen').
card_uid('hunding gjornersen'/'ME3', 'ME3:Hunding Gjornersen:hunding gjornersen').
card_rarity('hunding gjornersen'/'ME3', 'Uncommon').
card_artist('hunding gjornersen'/'ME3', 'Richard Thomas').
card_number('hunding gjornersen'/'ME3', '152').
card_flavor_text('hunding gjornersen'/'ME3', '\"You would never guess, at the terrifying sight of the man, that Hunding was as charming a companion as one could wish for.\"').
card_multiverse_id('hunding gjornersen'/'ME3', '201194').

card_in_set('hunting cheetah', 'ME3').
card_original_type('hunting cheetah'/'ME3', 'Creature — Cat').
card_original_text('hunting cheetah'/'ME3', 'Whenever Hunting Cheetah deals damage to an opponent, you may search your library for a Forest card, reveal that card, put it into your hand, then shuffle your library.').
card_image_name('hunting cheetah'/'ME3', 'hunting cheetah').
card_uid('hunting cheetah'/'ME3', 'ME3:Hunting Cheetah:hunting cheetah').
card_rarity('hunting cheetah'/'ME3', 'Common').
card_artist('hunting cheetah'/'ME3', 'Fang Yue').
card_number('hunting cheetah'/'ME3', '124').
card_multiverse_id('hunting cheetah'/'ME3', '201407').

card_in_set('hurloon minotaur', 'ME3').
card_original_type('hurloon minotaur'/'ME3', 'Creature — Minotaur').
card_original_text('hurloon minotaur'/'ME3', '').
card_image_name('hurloon minotaur'/'ME3', 'hurloon minotaur').
card_uid('hurloon minotaur'/'ME3', 'ME3:Hurloon Minotaur:hurloon minotaur').
card_rarity('hurloon minotaur'/'ME3', 'Common').
card_artist('hurloon minotaur'/'ME3', 'Anson Maddocks').
card_number('hurloon minotaur'/'ME3', '102').
card_flavor_text('hurloon minotaur'/'ME3', 'The minotaurs of the Hurloon Mountains are known for their love of battle. They are also known for their hymns to the dead, sung for friend and foe alike. These hymns can last for days, filling the mountain valleys with their low, haunting sounds.').
card_multiverse_id('hurloon minotaur'/'ME3', '201401').

card_in_set('illusionary mask', 'ME3').
card_original_type('illusionary mask'/'ME3', 'Artifact').
card_original_text('illusionary mask'/'ME3', '{X}: You may put a creature card with converted mana cost X or less from your hand onto the battlefield face down as a 0/1 creature. Put X mask counters on that creature. Activate this ability only any time you could cast a sorcery. The creature\'s controller may turn the creature face up any time he or she could cast an instant by removing all mask counters from it. This effect ends if the creature is turned face up.').
card_image_name('illusionary mask'/'ME3', 'illusionary mask').
card_uid('illusionary mask'/'ME3', 'ME3:Illusionary Mask:illusionary mask').
card_rarity('illusionary mask'/'ME3', 'Rare').
card_artist('illusionary mask'/'ME3', 'Amy Weber').
card_number('illusionary mask'/'ME3', '197').
card_multiverse_id('illusionary mask'/'ME3', '201199').

card_in_set('immolation', 'ME3').
card_original_type('immolation'/'ME3', 'Enchantment — Aura').
card_original_text('immolation'/'ME3', 'Enchant creature\nEnchanted creature gets +2/-2.').
card_image_name('immolation'/'ME3', 'immolation').
card_uid('immolation'/'ME3', 'ME3:Immolation:immolation').
card_rarity('immolation'/'ME3', 'Common').
card_artist('immolation'/'ME3', 'Scott Kirschner').
card_number('immolation'/'ME3', '103').
card_multiverse_id('immolation'/'ME3', '201142').

card_in_set('infuse', 'ME3').
card_original_type('infuse'/'ME3', 'Instant').
card_original_text('infuse'/'ME3', 'Untap target artifact, creature, or land.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_image_name('infuse'/'ME3', 'infuse').
card_uid('infuse'/'ME3', 'ME3:Infuse:infuse').
card_rarity('infuse'/'ME3', 'Common').
card_artist('infuse'/'ME3', 'Randy Gallegos').
card_number('infuse'/'ME3', '38').
card_flavor_text('infuse'/'ME3', '\"The potential for change lies in all things. Know a thing\'s nature, know its possibilities, and know it anew.\"\n—Gustha Ebbasdotter, Kjeldoran Royal Mage').
card_multiverse_id('infuse'/'ME3', '201264').

card_in_set('island', 'ME3').
card_original_type('island'/'ME3', 'Basic Land — Island').
card_original_text('island'/'ME3', 'U').
card_image_name('island'/'ME3', 'island1').
card_uid('island'/'ME3', 'ME3:Island:island1').
card_rarity('island'/'ME3', 'Basic Land').
card_artist('island'/'ME3', 'Ku Xueming').
card_number('island'/'ME3', '219').
card_multiverse_id('island'/'ME3', '206074').

card_in_set('island', 'ME3').
card_original_type('island'/'ME3', 'Basic Land — Island').
card_original_text('island'/'ME3', 'U').
card_image_name('island'/'ME3', 'island2').
card_uid('island'/'ME3', 'ME3:Island:island2').
card_rarity('island'/'ME3', 'Basic Land').
card_artist('island'/'ME3', 'Ku Xueming').
card_number('island'/'ME3', '220').
card_multiverse_id('island'/'ME3', '206073').

card_in_set('island', 'ME3').
card_original_type('island'/'ME3', 'Basic Land — Island').
card_original_text('island'/'ME3', 'U').
card_image_name('island'/'ME3', 'island3').
card_uid('island'/'ME3', 'ME3:Island:island3').
card_rarity('island'/'ME3', 'Basic Land').
card_artist('island'/'ME3', 'Ku Xueming').
card_number('island'/'ME3', '221').
card_multiverse_id('island'/'ME3', '206072').

card_in_set('ivory guardians', 'ME3').
card_original_type('ivory guardians'/'ME3', 'Creature — Giant Cleric').
card_original_text('ivory guardians'/'ME3', 'Protection from red\nCreatures named Ivory Guardians get +1/+1 as long as an opponent controls a red permanent.').
card_image_name('ivory guardians'/'ME3', 'ivory guardians').
card_uid('ivory guardians'/'ME3', 'ME3:Ivory Guardians:ivory guardians').
card_rarity('ivory guardians'/'ME3', 'Uncommon').
card_artist('ivory guardians'/'ME3', 'Melissa A. Benson').
card_number('ivory guardians'/'ME3', '15').
card_flavor_text('ivory guardians'/'ME3', 'The elite guard of the Mesa High Priests, the Ivory Guardians, were created to protect the innocent and faithful. Some say their actions are above the law.').
card_multiverse_id('ivory guardians'/'ME3', '201146').

card_in_set('jedit ojanen', 'ME3').
card_original_type('jedit ojanen'/'ME3', 'Legendary Creature — Cat Warrior').
card_original_text('jedit ojanen'/'ME3', '').
card_image_name('jedit ojanen'/'ME3', 'jedit ojanen').
card_uid('jedit ojanen'/'ME3', 'ME3:Jedit Ojanen:jedit ojanen').
card_rarity('jedit ojanen'/'ME3', 'Common').
card_artist('jedit ojanen'/'ME3', 'Mark Poole').
card_number('jedit ojanen'/'ME3', '153').
card_flavor_text('jedit ojanen'/'ME3', 'Mightiest of the cat warriors, Jedit Ojanen forsook the forests of his tribe and took service with the Robaran Mercenaries, rising through their ranks to lead them in the battle for Efrava.').
card_multiverse_id('jedit ojanen'/'ME3', '201128').

card_in_set('jerrard of the closed fist', 'ME3').
card_original_type('jerrard of the closed fist'/'ME3', 'Legendary Creature — Human Knight').
card_original_text('jerrard of the closed fist'/'ME3', '').
card_image_name('jerrard of the closed fist'/'ME3', 'jerrard of the closed fist').
card_uid('jerrard of the closed fist'/'ME3', 'ME3:Jerrard of the Closed Fist:jerrard of the closed fist').
card_rarity('jerrard of the closed fist'/'ME3', 'Common').
card_artist('jerrard of the closed fist'/'ME3', 'Andi Rusu').
card_number('jerrard of the closed fist'/'ME3', '154').
card_flavor_text('jerrard of the closed fist'/'ME3', 'Once, the order of the Closed Fist reached throughout the Kb\'Briann Highlands. Now, Jerrard alone remains to uphold their standard.').
card_multiverse_id('jerrard of the closed fist'/'ME3', '201196').

card_in_set('jungle lion', 'ME3').
card_original_type('jungle lion'/'ME3', 'Creature — Cat').
card_original_text('jungle lion'/'ME3', 'Jungle Lion can\'t block.').
card_image_name('jungle lion'/'ME3', 'jungle lion').
card_uid('jungle lion'/'ME3', 'ME3:Jungle Lion:jungle lion').
card_rarity('jungle lion'/'ME3', 'Common').
card_artist('jungle lion'/'ME3', 'Janine Johnston').
card_number('jungle lion'/'ME3', '125').
card_flavor_text('jungle lion'/'ME3', 'The lion\'s only loyalty is to its hunger.').
card_multiverse_id('jungle lion'/'ME3', '201241').

card_in_set('karakas', 'ME3').
card_original_type('karakas'/'ME3', 'Legendary Land').
card_original_text('karakas'/'ME3', '{T}: Add {W} to your mana pool.\n{T}: Return target legendary creature to its owner\'s hand.').
card_image_name('karakas'/'ME3', 'karakas').
card_uid('karakas'/'ME3', 'ME3:Karakas:karakas').
card_rarity('karakas'/'ME3', 'Rare').
card_artist('karakas'/'ME3', 'Nicola Leonard').
card_number('karakas'/'ME3', '208').
card_flavor_text('karakas'/'ME3', '\"To make a prairie it takes a clover and one bee,/ One clover, and a bee,/ And revery.\"\n—Emily Dickinson').
card_multiverse_id('karakas'/'ME3', '201198').

card_in_set('kei takahashi', 'ME3').
card_original_type('kei takahashi'/'ME3', 'Legendary Creature — Human Cleric').
card_original_text('kei takahashi'/'ME3', '{T}: Prevent the next 2 damage that would be dealt to target creature this turn.').
card_image_name('kei takahashi'/'ME3', 'kei takahashi').
card_uid('kei takahashi'/'ME3', 'ME3:Kei Takahashi:kei takahashi').
card_rarity('kei takahashi'/'ME3', 'Uncommon').
card_artist('kei takahashi'/'ME3', 'Scott Kirschner').
card_number('kei takahashi'/'ME3', '155').
card_multiverse_id('kei takahashi'/'ME3', '201200').

card_in_set('killer bees', 'ME3').
card_original_type('killer bees'/'ME3', 'Creature — Insect').
card_original_text('killer bees'/'ME3', 'Flying\n{G}: Killer Bees gets +1/+1 until end of turn.').
card_image_name('killer bees'/'ME3', 'killer bees').
card_uid('killer bees'/'ME3', 'ME3:Killer Bees:killer bees').
card_rarity('killer bees'/'ME3', 'Uncommon').
card_artist('killer bees'/'ME3', 'Phil Foglio').
card_number('killer bees'/'ME3', '126').
card_flavor_text('killer bees'/'ME3', 'The communal mind produces a savage strategy, yet no one could predict that this vicious crossbreed would unravel the secret of steel.').
card_multiverse_id('killer bees'/'ME3', '201201').

card_in_set('kjeldoran frostbeast', 'ME3').
card_original_type('kjeldoran frostbeast'/'ME3', 'Creature — Elemental Beast').
card_original_text('kjeldoran frostbeast'/'ME3', 'Whenever Kjeldoran Frostbeast blocks or becomes blocked by a creature, destroy that creature at end of combat.').
card_image_name('kjeldoran frostbeast'/'ME3', 'kjeldoran frostbeast').
card_uid('kjeldoran frostbeast'/'ME3', 'ME3:Kjeldoran Frostbeast:kjeldoran frostbeast').
card_rarity('kjeldoran frostbeast'/'ME3', 'Uncommon').
card_artist('kjeldoran frostbeast'/'ME3', 'Mark Poole').
card_number('kjeldoran frostbeast'/'ME3', '156').
card_flavor_text('kjeldoran frostbeast'/'ME3', '\"Two of my Warriors found that the creature was dangerous not only in combat, but also in simple proximity.\"\n—Disa the Restless, journal entry').
card_multiverse_id('kjeldoran frostbeast'/'ME3', '201197').

card_in_set('knowledge vault', 'ME3').
card_original_type('knowledge vault'/'ME3', 'Artifact').
card_original_text('knowledge vault'/'ME3', '{2}, {T}: Exile the top card of your library face down. \n{0}: Sacrifice Knowledge Vault. If you do, discard your hand, then put all cards exiled with Knowledge Vault into their owner\'s hand.\nWhen Knowledge Vault leaves the battlefield, put all cards exiled with Knowledge Vault into their owner\'s graveyard.').
card_image_name('knowledge vault'/'ME3', 'knowledge vault').
card_uid('knowledge vault'/'ME3', 'ME3:Knowledge Vault:knowledge vault').
card_rarity('knowledge vault'/'ME3', 'Uncommon').
card_artist('knowledge vault'/'ME3', 'Anthony S. Waters').
card_number('knowledge vault'/'ME3', '198').
card_multiverse_id('knowledge vault'/'ME3', '201195').

card_in_set('kobold drill sergeant', 'ME3').
card_original_type('kobold drill sergeant'/'ME3', 'Creature — Kobold Soldier').
card_original_text('kobold drill sergeant'/'ME3', 'Other Kobold creatures you control get +0/+1 and have trample.').
card_image_name('kobold drill sergeant'/'ME3', 'kobold drill sergeant').
card_uid('kobold drill sergeant'/'ME3', 'ME3:Kobold Drill Sergeant:kobold drill sergeant').
card_rarity('kobold drill sergeant'/'ME3', 'Uncommon').
card_artist('kobold drill sergeant'/'ME3', 'Julie Baroh').
card_number('kobold drill sergeant'/'ME3', '104').
card_flavor_text('kobold drill sergeant'/'ME3', '\"Joining this army is easy, boy. Just survive your first battle.\"').
card_multiverse_id('kobold drill sergeant'/'ME3', '201148').

card_in_set('kobold overlord', 'ME3').
card_original_type('kobold overlord'/'ME3', 'Creature — Kobold').
card_original_text('kobold overlord'/'ME3', 'Kobold creatures you control have first strike.').
card_image_name('kobold overlord'/'ME3', 'kobold overlord').
card_uid('kobold overlord'/'ME3', 'ME3:Kobold Overlord:kobold overlord').
card_rarity('kobold overlord'/'ME3', 'Uncommon').
card_artist('kobold overlord'/'ME3', 'Julie Baroh').
card_number('kobold overlord'/'ME3', '105').
card_flavor_text('kobold overlord'/'ME3', '\"One for all, all for one; we strike first, and then you\'re done!\" —Oath of the Kobold Musketeers').
card_multiverse_id('kobold overlord'/'ME3', '201149').

card_in_set('kobold taskmaster', 'ME3').
card_original_type('kobold taskmaster'/'ME3', 'Creature — Kobold').
card_original_text('kobold taskmaster'/'ME3', 'Other Kobold creatures you control get +1/+0.').
card_image_name('kobold taskmaster'/'ME3', 'kobold taskmaster').
card_uid('kobold taskmaster'/'ME3', 'ME3:Kobold Taskmaster:kobold taskmaster').
card_rarity('kobold taskmaster'/'ME3', 'Common').
card_artist('kobold taskmaster'/'ME3', 'Randy Asplund-Faith').
card_number('kobold taskmaster'/'ME3', '106').
card_flavor_text('kobold taskmaster'/'ME3', 'The Taskmaster knows that there is no cure for the common Kobold.').
card_multiverse_id('kobold taskmaster'/'ME3', '201150').

card_in_set('kobolds of kher keep', 'ME3').
card_original_type('kobolds of kher keep'/'ME3', 'Creature — Kobold').
card_original_text('kobolds of kher keep'/'ME3', 'Kobolds of Kher Keep is red.').
card_image_name('kobolds of kher keep'/'ME3', 'kobolds of kher keep').
card_uid('kobolds of kher keep'/'ME3', 'ME3:Kobolds of Kher Keep:kobolds of kher keep').
card_rarity('kobolds of kher keep'/'ME3', 'Common').
card_artist('kobolds of kher keep'/'ME3', 'Julie Baroh').
card_number('kobolds of kher keep'/'ME3', '107').
card_flavor_text('kobolds of kher keep'/'ME3', 'Kher Keep is unique among fortresses: impervious to aerial assault but defenseless from the ground.').
card_multiverse_id('kobolds of kher keep'/'ME3', '201151').

card_in_set('kongming, \"sleeping dragon\"', 'ME3').
card_original_type('kongming, \"sleeping dragon\"'/'ME3', 'Legendary Creature — Human Advisor').
card_original_text('kongming, \"sleeping dragon\"'/'ME3', 'Other creatures you control get +1/+1.').
card_image_name('kongming, \"sleeping dragon\"'/'ME3', 'kongming, sleeping dragon').
card_uid('kongming, \"sleeping dragon\"'/'ME3', 'ME3:Kongming, \"Sleeping Dragon\":kongming, sleeping dragon').
card_rarity('kongming, \"sleeping dragon\"'/'ME3', 'Rare').
card_artist('kongming, \"sleeping dragon\"'/'ME3', 'Gao Yan').
card_number('kongming, \"sleeping dragon\"'/'ME3', '16').
card_flavor_text('kongming, \"sleeping dragon\"'/'ME3', '\"Such a lord as this—all virtues\' height—Had never been, nor ever was again.\"').
card_multiverse_id('kongming, \"sleeping dragon\"'/'ME3', '201282').

card_in_set('labyrinth minotaur', 'ME3').
card_original_type('labyrinth minotaur'/'ME3', 'Creature — Minotaur').
card_original_text('labyrinth minotaur'/'ME3', 'Whenever Labyrinth Minotaur blocks a creature, that creature doesn\'t untap during its controller\'s next untap step.').
card_image_name('labyrinth minotaur'/'ME3', 'labyrinth minotaur').
card_uid('labyrinth minotaur'/'ME3', 'ME3:Labyrinth Minotaur:labyrinth minotaur').
card_rarity('labyrinth minotaur'/'ME3', 'Common').
card_artist('labyrinth minotaur'/'ME3', 'Anson Maddocks').
card_number('labyrinth minotaur'/'ME3', '39').
card_flavor_text('labyrinth minotaur'/'ME3', '\"I doubt any labyrinth minotaurs still live—but then again, we minotaurs are stubborn beings.\"\n—Onatah, Anaba Shaman').
card_multiverse_id('labyrinth minotaur'/'ME3', '201319').

card_in_set('lady caleria', 'ME3').
card_original_type('lady caleria'/'ME3', 'Legendary Creature — Human Archer').
card_original_text('lady caleria'/'ME3', '{T}: Lady Caleria deals 3 damage to target attacking or blocking creature.').
card_image_name('lady caleria'/'ME3', 'lady caleria').
card_uid('lady caleria'/'ME3', 'ME3:Lady Caleria:lady caleria').
card_rarity('lady caleria'/'ME3', 'Uncommon').
card_artist('lady caleria'/'ME3', 'Bryon Wackwitz').
card_number('lady caleria'/'ME3', '157').
card_multiverse_id('lady caleria'/'ME3', '201410').

card_in_set('lady evangela', 'ME3').
card_original_type('lady evangela'/'ME3', 'Legendary Creature — Human Cleric').
card_original_text('lady evangela'/'ME3', '{W}{B}, {T}: Prevent all combat damage that would be dealt by target creature this turn.').
card_image_name('lady evangela'/'ME3', 'lady evangela').
card_uid('lady evangela'/'ME3', 'ME3:Lady Evangela:lady evangela').
card_rarity('lady evangela'/'ME3', 'Uncommon').
card_artist('lady evangela'/'ME3', 'Mark Poole').
card_number('lady evangela'/'ME3', '158').
card_flavor_text('lady evangela'/'ME3', '\"When milady was young, the sight of a rainbow would fill her soul with peace. As she grew, she learned to share her rapture with others.\"\n—Lady Gabriella').
card_multiverse_id('lady evangela'/'ME3', '201203').

card_in_set('lady orca', 'ME3').
card_original_type('lady orca'/'ME3', 'Legendary Creature — Demon').
card_original_text('lady orca'/'ME3', '').
card_image_name('lady orca'/'ME3', 'lady orca').
card_uid('lady orca'/'ME3', 'ME3:Lady Orca:lady orca').
card_rarity('lady orca'/'ME3', 'Common').
card_artist('lady orca'/'ME3', 'Sandra Everingham').
card_number('lady orca'/'ME3', '159').
card_flavor_text('lady orca'/'ME3', '\"I do not remember what he said to her. I remember her fiery eyes, fixed upon him for an instant. I remember a flash, and the hot breath of sudden flames made me turn away. When I looked again, Angus was gone.\"\n—A Wayfarer, on meeting Lady Orca').
card_multiverse_id('lady orca'/'ME3', '205867').

card_in_set('land equilibrium', 'ME3').
card_original_type('land equilibrium'/'ME3', 'Enchantment').
card_original_text('land equilibrium'/'ME3', 'If an opponent who controls at least as many lands as you do would put a land onto the battlefield, that player instead puts that land onto the battlefield then sacrifices a land.').
card_image_name('land equilibrium'/'ME3', 'land equilibrium').
card_uid('land equilibrium'/'ME3', 'ME3:Land Equilibrium:land equilibrium').
card_rarity('land equilibrium'/'ME3', 'Rare').
card_artist('land equilibrium'/'ME3', 'Jesper Myrfors').
card_number('land equilibrium'/'ME3', '40').
card_multiverse_id('land equilibrium'/'ME3', '201152').

card_in_set('land tax', 'ME3').
card_original_type('land tax'/'ME3', 'Enchantment').
card_original_text('land tax'/'ME3', 'At the beginning of your upkeep, if an opponent controls more lands than you, you may search your library for up to three basic land cards, reveal them, and put them into your hand. If you do, shuffle your library.').
card_image_name('land tax'/'ME3', 'land tax').
card_uid('land tax'/'ME3', 'ME3:Land Tax:land tax').
card_rarity('land tax'/'ME3', 'Rare').
card_artist('land tax'/'ME3', 'Brian Snõddy').
card_number('land tax'/'ME3', '17').
card_multiverse_id('land tax'/'ME3', '201153').

card_in_set('lesser werewolf', 'ME3').
card_original_type('lesser werewolf'/'ME3', 'Creature — Human Wolf').
card_original_text('lesser werewolf'/'ME3', '{B}: If Lesser Werewolf\'s power is 1 or more, it gets -1/-0 until end of turn and put a -0/-1 counter on target creature blocking or blocked by Lesser Werewolf. Activate this ability only during the declare blockers step.').
card_image_name('lesser werewolf'/'ME3', 'lesser werewolf').
card_uid('lesser werewolf'/'ME3', 'ME3:Lesser Werewolf:lesser werewolf').
card_rarity('lesser werewolf'/'ME3', 'Common').
card_artist('lesser werewolf'/'ME3', 'Quinton Hoover').
card_number('lesser werewolf'/'ME3', '71').
card_multiverse_id('lesser werewolf'/'ME3', '201154').

card_in_set('life chisel', 'ME3').
card_original_type('life chisel'/'ME3', 'Artifact').
card_original_text('life chisel'/'ME3', 'Sacrifice a creature: You gain life equal to the sacrificed creature\'s toughness. Activate this ability only during your upkeep.').
card_image_name('life chisel'/'ME3', 'life chisel').
card_uid('life chisel'/'ME3', 'ME3:Life Chisel:life chisel').
card_rarity('life chisel'/'ME3', 'Rare').
card_artist('life chisel'/'ME3', 'Anthony S. Waters').
card_number('life chisel'/'ME3', '199').
card_multiverse_id('life chisel'/'ME3', '201205').

card_in_set('lightning blow', 'ME3').
card_original_type('lightning blow'/'ME3', 'Instant').
card_original_text('lightning blow'/'ME3', 'Target creature gains first strike until end of turn.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_image_name('lightning blow'/'ME3', 'lightning blow').
card_uid('lightning blow'/'ME3', 'ME3:Lightning Blow:lightning blow').
card_rarity('lightning blow'/'ME3', 'Common').
card_artist('lightning blow'/'ME3', 'Harold McNeill').
card_number('lightning blow'/'ME3', '18').
card_flavor_text('lightning blow'/'ME3', '\"If you do it right, they\'ll never know what hit them.\"\n—General Jarkeld, the Arctic Fox').
card_multiverse_id('lightning blow'/'ME3', '201121').

card_in_set('liu bei, lord of shu', 'ME3').
card_original_type('liu bei, lord of shu'/'ME3', 'Legendary Creature — Human Soldier').
card_original_text('liu bei, lord of shu'/'ME3', 'Horsemanship\nLiu Bei, Lord of Shu gets +2/+2 as long as you control a permanent named Guan Yu, Sainted Warrior or a permanent named Zhang Fei, Fierce Warrior.').
card_image_name('liu bei, lord of shu'/'ME3', 'liu bei, lord of shu').
card_uid('liu bei, lord of shu'/'ME3', 'ME3:Liu Bei, Lord of Shu:liu bei, lord of shu').
card_rarity('liu bei, lord of shu'/'ME3', 'Rare').
card_artist('liu bei, lord of shu'/'ME3', 'Qiao Dafu').
card_number('liu bei, lord of shu'/'ME3', '19').
card_flavor_text('liu bei, lord of shu'/'ME3', '\"Only wisdom and virtue can truly win men\'s devotion.\"\n—Liu Bei').
card_multiverse_id('liu bei, lord of shu'/'ME3', '201283').

card_in_set('living plane', 'ME3').
card_original_type('living plane'/'ME3', 'World Enchantment').
card_original_text('living plane'/'ME3', 'All lands are 1/1 creatures that are still lands.').
card_image_name('living plane'/'ME3', 'living plane').
card_uid('living plane'/'ME3', 'ME3:Living Plane:living plane').
card_rarity('living plane'/'ME3', 'Rare').
card_artist('living plane'/'ME3', 'Bryon Wackwitz').
card_number('living plane'/'ME3', '127').
card_multiverse_id('living plane'/'ME3', '201207').

card_in_set('livonya silone', 'ME3').
card_original_type('livonya silone'/'ME3', 'Legendary Creature — Human Warrior').
card_original_text('livonya silone'/'ME3', 'First strike, legendary landwalk').
card_image_name('livonya silone'/'ME3', 'livonya silone').
card_uid('livonya silone'/'ME3', 'ME3:Livonya Silone:livonya silone').
card_rarity('livonya silone'/'ME3', 'Uncommon').
card_artist('livonya silone'/'ME3', 'Richard Kane Ferguson').
card_number('livonya silone'/'ME3', '160').
card_flavor_text('livonya silone'/'ME3', 'Livonya\'s own nature is a matter of mystery. Nothing is known for sure, merely rumors of unearthly stealth, and unholy alliances.').
card_multiverse_id('livonya silone'/'ME3', '201267').

card_in_set('loyal retainers', 'ME3').
card_original_type('loyal retainers'/'ME3', 'Creature — Human Advisor').
card_original_text('loyal retainers'/'ME3', 'Sacrifice Loyal Retainers: Return target legendary creature card from your graveyard to the battlefield. Activate this ability only during your turn, before attackers are declared.').
card_image_name('loyal retainers'/'ME3', 'loyal retainers').
card_uid('loyal retainers'/'ME3', 'ME3:Loyal Retainers:loyal retainers').
card_rarity('loyal retainers'/'ME3', 'Uncommon').
card_artist('loyal retainers'/'ME3', 'Solomon Au Yeung').
card_number('loyal retainers'/'ME3', '20').
card_multiverse_id('loyal retainers'/'ME3', '201136').

card_in_set('lu bu, master-at-arms', 'ME3').
card_original_type('lu bu, master-at-arms'/'ME3', 'Legendary Creature — Human Soldier Warrior').
card_original_text('lu bu, master-at-arms'/'ME3', 'Horsemanship, haste').
card_image_name('lu bu, master-at-arms'/'ME3', 'lu bu, master-at-arms').
card_uid('lu bu, master-at-arms'/'ME3', 'ME3:Lu Bu, Master-at-Arms:lu bu, master-at-arms').
card_rarity('lu bu, master-at-arms'/'ME3', 'Rare').
card_artist('lu bu, master-at-arms'/'ME3', 'Gao Jianzhang').
card_number('lu bu, master-at-arms'/'ME3', '108').
card_flavor_text('lu bu, master-at-arms'/'ME3', '\"Dong Zhuo\'s man, Lu Bu, warrior without peer,/ Far surpassed the champions of his sphere.\"').
card_multiverse_id('lu bu, master-at-arms'/'ME3', '201285').

card_in_set('lu meng, wu general', 'ME3').
card_original_type('lu meng, wu general'/'ME3', 'Legendary Creature — Human Soldier').
card_original_text('lu meng, wu general'/'ME3', 'Horsemanship').
card_image_name('lu meng, wu general'/'ME3', 'lu meng, wu general').
card_uid('lu meng, wu general'/'ME3', 'ME3:Lu Meng, Wu General:lu meng, wu general').
card_rarity('lu meng, wu general'/'ME3', 'Uncommon').
card_artist('lu meng, wu general'/'ME3', 'Gao Yan').
card_number('lu meng, wu general'/'ME3', '41').
card_flavor_text('lu meng, wu general'/'ME3', 'As the Wu chief commander, Lu Meng conquered Shu-held Jingzhou in 219 by disguising soldiers as merchants on boats filled with hiding troops.').
card_multiverse_id('lu meng, wu general'/'ME3', '201097').

card_in_set('lu xun, scholar general', 'ME3').
card_original_type('lu xun, scholar general'/'ME3', 'Legendary Creature — Human Soldier').
card_original_text('lu xun, scholar general'/'ME3', 'Horsemanship\nWhenever Lu Xun, Scholar General deals damage to an opponent, you may draw a card.').
card_image_name('lu xun, scholar general'/'ME3', 'lu xun, scholar general').
card_uid('lu xun, scholar general'/'ME3', 'ME3:Lu Xun, Scholar General:lu xun, scholar general').
card_rarity('lu xun, scholar general'/'ME3', 'Uncommon').
card_artist('lu xun, scholar general'/'ME3', 'Xu Xiaoming').
card_number('lu xun, scholar general'/'ME3', '42').
card_multiverse_id('lu xun, scholar general'/'ME3', '201286').

card_in_set('mana drain', 'ME3').
card_original_type('mana drain'/'ME3', 'Instant').
card_original_text('mana drain'/'ME3', 'Counter target spell. At the beginning of your next main phase, add {X} to your mana pool, where X is that spell\'s converted mana cost.').
card_image_name('mana drain'/'ME3', 'mana drain').
card_uid('mana drain'/'ME3', 'ME3:Mana Drain:mana drain').
card_rarity('mana drain'/'ME3', 'Rare').
card_artist('mana drain'/'ME3', 'Mark Tedin').
card_number('mana drain'/'ME3', '43').
card_multiverse_id('mana drain'/'ME3', '201156').

card_in_set('mana vortex', 'ME3').
card_original_type('mana vortex'/'ME3', 'Enchantment').
card_original_text('mana vortex'/'ME3', 'When you cast Mana Vortex, you may sacrifice a land. If you don\'t, counter Mana Vortex.\nAt the beginning of each player\'s upkeep, that player sacrifices a land.\nWhen there are no lands on the battlefield, sacrifice Mana Vortex.').
card_image_name('mana vortex'/'ME3', 'mana vortex').
card_uid('mana vortex'/'ME3', 'ME3:Mana Vortex:mana vortex').
card_rarity('mana vortex'/'ME3', 'Rare').
card_artist('mana vortex'/'ME3', 'Douglas Shuler').
card_number('mana vortex'/'ME3', '44').
card_multiverse_id('mana vortex'/'ME3', '201260').

card_in_set('marhault elsdragon', 'ME3').
card_original_type('marhault elsdragon'/'ME3', 'Legendary Creature — Elf Warrior').
card_original_text('marhault elsdragon'/'ME3', 'Rampage 1 (Whenever this creature becomes blocked, it gets +1/+1 until end of turn for each creature blocking it beyond the first.)').
card_image_name('marhault elsdragon'/'ME3', 'marhault elsdragon').
card_uid('marhault elsdragon'/'ME3', 'ME3:Marhault Elsdragon:marhault elsdragon').
card_rarity('marhault elsdragon'/'ME3', 'Uncommon').
card_artist('marhault elsdragon'/'ME3', 'Mark Poole').
card_number('marhault elsdragon'/'ME3', '161').
card_flavor_text('marhault elsdragon'/'ME3', 'Marhault Elsdragon follows a strict philosophy, never letting emotions cloud his thoughts. No chance observer could imagine the rage in his heart.').
card_multiverse_id('marhault elsdragon'/'ME3', '201222').

card_in_set('meng huo\'s horde', 'ME3').
card_original_type('meng huo\'s horde'/'ME3', 'Creature — Human Soldier').
card_original_text('meng huo\'s horde'/'ME3', '').
card_image_name('meng huo\'s horde'/'ME3', 'meng huo\'s horde').
card_uid('meng huo\'s horde'/'ME3', 'ME3:Meng Huo\'s Horde:meng huo\'s horde').
card_rarity('meng huo\'s horde'/'ME3', 'Common').
card_artist('meng huo\'s horde'/'ME3', 'Yang Jun Kwon').
card_number('meng huo\'s horde'/'ME3', '129').
card_flavor_text('meng huo\'s horde'/'ME3', 'Through his allies, Meng Huo commanded several hundred thousand troops composed of cavalry, rattan-armored warriors, infantry, naked men with swords, and wild animals.').
card_multiverse_id('meng huo\'s horde'/'ME3', '201219').

card_in_set('meng huo, barbarian king', 'ME3').
card_original_type('meng huo, barbarian king'/'ME3', 'Legendary Creature — Human Barbarian Soldier').
card_original_text('meng huo, barbarian king'/'ME3', 'Other green creatures you control get +1/+1.').
card_image_name('meng huo, barbarian king'/'ME3', 'meng huo, barbarian king').
card_uid('meng huo, barbarian king'/'ME3', 'ME3:Meng Huo, Barbarian King:meng huo, barbarian king').
card_rarity('meng huo, barbarian king'/'ME3', 'Rare').
card_artist('meng huo, barbarian king'/'ME3', 'Yang Guangmai').
card_number('meng huo, barbarian king'/'ME3', '128').
card_flavor_text('meng huo, barbarian king'/'ME3', 'The stubborn Meng Huo was captured and released by Kongming seven times before finally surrendering.').
card_multiverse_id('meng huo, barbarian king'/'ME3', '201287').

card_in_set('mind twist', 'ME3').
card_original_type('mind twist'/'ME3', 'Sorcery').
card_original_text('mind twist'/'ME3', 'Target player discards X cards at random.').
card_image_name('mind twist'/'ME3', 'mind twist').
card_uid('mind twist'/'ME3', 'ME3:Mind Twist:mind twist').
card_rarity('mind twist'/'ME3', 'Rare').
card_artist('mind twist'/'ME3', 'Julie Baroh').
card_number('mind twist'/'ME3', '72').
card_multiverse_id('mind twist'/'ME3', '201311').

card_in_set('misfortune\'s gain', 'ME3').
card_original_type('misfortune\'s gain'/'ME3', 'Sorcery').
card_original_text('misfortune\'s gain'/'ME3', 'Destroy target creature. Its owner gains 4 life.').
card_image_name('misfortune\'s gain'/'ME3', 'misfortune\'s gain').
card_uid('misfortune\'s gain'/'ME3', 'ME3:Misfortune\'s Gain:misfortune\'s gain').
card_rarity('misfortune\'s gain'/'ME3', 'Common').
card_artist('misfortune\'s gain'/'ME3', 'Jiaming').
card_number('misfortune\'s gain'/'ME3', '21').
card_flavor_text('misfortune\'s gain'/'ME3', 'The mourning families of commanders and generals were often given land, valuables, or money to compensate for their losses.').
card_multiverse_id('misfortune\'s gain'/'ME3', '201288').

card_in_set('mountain', 'ME3').
card_original_type('mountain'/'ME3', 'Basic Land — Mountain').
card_original_text('mountain'/'ME3', 'R').
card_image_name('mountain'/'ME3', 'mountain1').
card_uid('mountain'/'ME3', 'ME3:Mountain:mountain1').
card_rarity('mountain'/'ME3', 'Basic Land').
card_artist('mountain'/'ME3', 'Qin Jun').
card_number('mountain'/'ME3', '225').
card_multiverse_id('mountain'/'ME3', '206077').

card_in_set('mountain', 'ME3').
card_original_type('mountain'/'ME3', 'Basic Land — Mountain').
card_original_text('mountain'/'ME3', 'R').
card_image_name('mountain'/'ME3', 'mountain2').
card_uid('mountain'/'ME3', 'ME3:Mountain:mountain2').
card_rarity('mountain'/'ME3', 'Basic Land').
card_artist('mountain'/'ME3', 'Qin Jun').
card_number('mountain'/'ME3', '226').
card_multiverse_id('mountain'/'ME3', '206076').

card_in_set('mountain', 'ME3').
card_original_type('mountain'/'ME3', 'Basic Land — Mountain').
card_original_text('mountain'/'ME3', 'R').
card_image_name('mountain'/'ME3', 'mountain3').
card_uid('mountain'/'ME3', 'ME3:Mountain:mountain3').
card_rarity('mountain'/'ME3', 'Basic Land').
card_artist('mountain'/'ME3', 'Qin Jun').
card_number('mountain'/'ME3', '227').
card_multiverse_id('mountain'/'ME3', '206075').

card_in_set('nebuchadnezzar', 'ME3').
card_original_type('nebuchadnezzar'/'ME3', 'Legendary Creature — Human Wizard').
card_original_text('nebuchadnezzar'/'ME3', '{X}, {T}: Name a card. Target opponent reveals X cards at random from his or her hand. That player then discards all cards with that name revealed this way. Activate this ability only during your turn.').
card_image_name('nebuchadnezzar'/'ME3', 'nebuchadnezzar').
card_uid('nebuchadnezzar'/'ME3', 'ME3:Nebuchadnezzar:nebuchadnezzar').
card_rarity('nebuchadnezzar'/'ME3', 'Uncommon').
card_artist('nebuchadnezzar'/'ME3', 'Richard Kane Ferguson').
card_number('nebuchadnezzar'/'ME3', '162').
card_multiverse_id('nebuchadnezzar'/'ME3', '201212').

card_in_set('nether void', 'ME3').
card_original_type('nether void'/'ME3', 'World Enchantment').
card_original_text('nether void'/'ME3', 'Whenever a player plays a spell, counter it unless its controller pays {3}.').
card_image_name('nether void'/'ME3', 'nether void').
card_uid('nether void'/'ME3', 'ME3:Nether Void:nether void').
card_rarity('nether void'/'ME3', 'Rare').
card_artist('nether void'/'ME3', 'Harold McNeill').
card_number('nether void'/'ME3', '73').
card_flavor_text('nether void'/'ME3', 'These days, some wizards are finding that they have a little too much spell left at the end of their mana.').
card_multiverse_id('nether void'/'ME3', '201158').

card_in_set('nicol bolas', 'ME3').
card_original_type('nicol bolas'/'ME3', 'Legendary Creature — Elder Dragon').
card_original_text('nicol bolas'/'ME3', 'Flying\nAt the beginning of your upkeep, sacrifice Nicol Bolas unless you pay {U}{B}{R}.\nWhenever Nicol Bolas deals damage to an opponent, that player discards his or her hand.').
card_image_name('nicol bolas'/'ME3', 'nicol bolas').
card_uid('nicol bolas'/'ME3', 'ME3:Nicol Bolas:nicol bolas').
card_rarity('nicol bolas'/'ME3', 'Rare').
card_artist('nicol bolas'/'ME3', 'Edward P. Beard, Jr.').
card_number('nicol bolas'/'ME3', '163').
card_multiverse_id('nicol bolas'/'ME3', '201213').

card_in_set('nova pentacle', 'ME3').
card_original_type('nova pentacle'/'ME3', 'Artifact').
card_original_text('nova pentacle'/'ME3', '{3}, {T}: The next time a source of your choice would deal damage to you this turn, that damage is dealt to target creature of an opponent\'s choice instead.').
card_image_name('nova pentacle'/'ME3', 'nova pentacle').
card_uid('nova pentacle'/'ME3', 'ME3:Nova Pentacle:nova pentacle').
card_rarity('nova pentacle'/'ME3', 'Rare').
card_artist('nova pentacle'/'ME3', 'Richard Thomas').
card_number('nova pentacle'/'ME3', '200').
card_multiverse_id('nova pentacle'/'ME3', '201214').

card_in_set('old man of the sea', 'ME3').
card_original_type('old man of the sea'/'ME3', 'Creature — Djinn').
card_original_text('old man of the sea'/'ME3', 'You may choose not to untap Old Man of the Sea during your untap step.\n{T}: Gain control of target creature with power less than or equal to Old Man of the Sea\'s power as long as Old Man of the Sea remains tapped and that creature\'s power remains less than or equal to Old Man of the Sea\'s power.').
card_image_name('old man of the sea'/'ME3', 'old man of the sea').
card_uid('old man of the sea'/'ME3', 'ME3:Old Man of the Sea:old man of the sea').
card_rarity('old man of the sea'/'ME3', 'Rare').
card_artist('old man of the sea'/'ME3', 'Susan Van Camp').
card_number('old man of the sea'/'ME3', '45').
card_multiverse_id('old man of the sea'/'ME3', '201144').

card_in_set('palladia-mors', 'ME3').
card_original_type('palladia-mors'/'ME3', 'Legendary Creature — Elder Dragon').
card_original_text('palladia-mors'/'ME3', 'Flying, trample\nAt the beginning of your upkeep, sacrifice Palladia-Mors unless you pay {R}{G}{W}.').
card_image_name('palladia-mors'/'ME3', 'palladia-mors').
card_uid('palladia-mors'/'ME3', 'ME3:Palladia-Mors:palladia-mors').
card_rarity('palladia-mors'/'ME3', 'Rare').
card_artist('palladia-mors'/'ME3', 'Edward P. Beard, Jr.').
card_number('palladia-mors'/'ME3', '164').
card_multiverse_id('palladia-mors'/'ME3', '201215').

card_in_set('pavel maliki', 'ME3').
card_original_type('pavel maliki'/'ME3', 'Legendary Creature — Human').
card_original_text('pavel maliki'/'ME3', '{B}{R}: Pavel Maliki gets +1/+0 until end of turn.').
card_image_name('pavel maliki'/'ME3', 'pavel maliki').
card_uid('pavel maliki'/'ME3', 'ME3:Pavel Maliki:pavel maliki').
card_rarity('pavel maliki'/'ME3', 'Uncommon').
card_artist('pavel maliki'/'ME3', 'Andi Rusu').
card_number('pavel maliki'/'ME3', '165').
card_flavor_text('pavel maliki'/'ME3', 'We all know the legend: Pavel wanders the realms, helping those in greatest need. But is this a measure of his generosity, or of his obligation to atone?').
card_multiverse_id('pavel maliki'/'ME3', '205922').

card_in_set('peach garden oath', 'ME3').
card_original_type('peach garden oath'/'ME3', 'Sorcery').
card_original_text('peach garden oath'/'ME3', 'You gain 2 life for each creature you control.').
card_image_name('peach garden oath'/'ME3', 'peach garden oath').
card_uid('peach garden oath'/'ME3', 'ME3:Peach Garden Oath:peach garden oath').
card_rarity('peach garden oath'/'ME3', 'Common').
card_artist('peach garden oath'/'ME3', 'Qiao Dafu').
card_number('peach garden oath'/'ME3', '22').
card_flavor_text('peach garden oath'/'ME3', '\"We three, though of separate ancestry, join in brotherhood. . . . We dare not hope to be together always but hereby vow to die the selfsame day.\"\n—Peach Garden Oath').
card_multiverse_id('peach garden oath'/'ME3', '201296').

card_in_set('plains', 'ME3').
card_original_type('plains'/'ME3', 'Basic Land — Plains').
card_original_text('plains'/'ME3', 'W').
card_image_name('plains'/'ME3', 'plains1').
card_uid('plains'/'ME3', 'ME3:Plains:plains1').
card_rarity('plains'/'ME3', 'Basic Land').
card_artist('plains'/'ME3', 'He Jiancheng').
card_number('plains'/'ME3', '216').
card_multiverse_id('plains'/'ME3', '206080').

card_in_set('plains', 'ME3').
card_original_type('plains'/'ME3', 'Basic Land — Plains').
card_original_text('plains'/'ME3', 'W').
card_image_name('plains'/'ME3', 'plains2').
card_uid('plains'/'ME3', 'ME3:Plains:plains2').
card_rarity('plains'/'ME3', 'Basic Land').
card_artist('plains'/'ME3', 'He Jiancheng').
card_number('plains'/'ME3', '217').
card_multiverse_id('plains'/'ME3', '206078').

card_in_set('plains', 'ME3').
card_original_type('plains'/'ME3', 'Basic Land — Plains').
card_original_text('plains'/'ME3', 'W').
card_image_name('plains'/'ME3', 'plains3').
card_uid('plains'/'ME3', 'ME3:Plains:plains3').
card_rarity('plains'/'ME3', 'Basic Land').
card_artist('plains'/'ME3', 'He Jiancheng').
card_number('plains'/'ME3', '218').
card_multiverse_id('plains'/'ME3', '206079').

card_in_set('plateau', 'ME3').
card_original_type('plateau'/'ME3', 'Land — Mountain Plains').
card_original_text('plateau'/'ME3', '').
card_image_name('plateau'/'ME3', 'plateau').
card_uid('plateau'/'ME3', 'ME3:Plateau:plateau').
card_rarity('plateau'/'ME3', 'Rare').
card_artist('plateau'/'ME3', 'Cornelius Brudi').
card_number('plateau'/'ME3', '209').
card_multiverse_id('plateau'/'ME3', '201402').

card_in_set('princess lucrezia', 'ME3').
card_original_type('princess lucrezia'/'ME3', 'Legendary Creature — Human Wizard').
card_original_text('princess lucrezia'/'ME3', '{T}: Add {U} to your mana pool.').
card_image_name('princess lucrezia'/'ME3', 'princess lucrezia').
card_uid('princess lucrezia'/'ME3', 'ME3:Princess Lucrezia:princess lucrezia').
card_rarity('princess lucrezia'/'ME3', 'Uncommon').
card_artist('princess lucrezia'/'ME3', 'Edward P. Beard, Jr.').
card_number('princess lucrezia'/'ME3', '166').
card_multiverse_id('princess lucrezia'/'ME3', '205869').

card_in_set('raging minotaur', 'ME3').
card_original_type('raging minotaur'/'ME3', 'Creature — Minotaur Berserker').
card_original_text('raging minotaur'/'ME3', 'Haste').
card_image_name('raging minotaur'/'ME3', 'raging minotaur').
card_uid('raging minotaur'/'ME3', 'ME3:Raging Minotaur:raging minotaur').
card_rarity('raging minotaur'/'ME3', 'Common').
card_artist('raging minotaur'/'ME3', 'Scott M. Fischer').
card_number('raging minotaur'/'ME3', '109').
card_flavor_text('raging minotaur'/'ME3', 'The only thing worse than a minotaur with an axe is an angry minotaur with an axe.').
card_multiverse_id('raging minotaur'/'ME3', '201325').

card_in_set('ragnar', 'ME3').
card_original_type('ragnar'/'ME3', 'Legendary Creature — Human Cleric').
card_original_text('ragnar'/'ME3', '{G}{W}{U}, {T}: Regenerate target creature.').
card_image_name('ragnar'/'ME3', 'ragnar').
card_uid('ragnar'/'ME3', 'ME3:Ragnar:ragnar').
card_rarity('ragnar'/'ME3', 'Uncommon').
card_artist('ragnar'/'ME3', 'Melissa A. Benson').
card_number('ragnar'/'ME3', '167').
card_flavor_text('ragnar'/'ME3', '\"On the field of honor, a soldier need have no fear.\"').
card_multiverse_id('ragnar'/'ME3', '201221').

card_in_set('ramirez depietro', 'ME3').
card_original_type('ramirez depietro'/'ME3', 'Legendary Creature — Human Pirate').
card_original_text('ramirez depietro'/'ME3', 'First strike').
card_image_name('ramirez depietro'/'ME3', 'ramirez depietro').
card_uid('ramirez depietro'/'ME3', 'ME3:Ramirez DePietro:ramirez depietro').
card_rarity('ramirez depietro'/'ME3', 'Common').
card_artist('ramirez depietro'/'ME3', 'Phil Foglio').
card_number('ramirez depietro'/'ME3', '168').
card_flavor_text('ramirez depietro'/'ME3', 'Ramirez DePietro is a most flamboyant pirate. Be careful not to believe his tall tales, especially when you ask his age.').
card_multiverse_id('ramirez depietro'/'ME3', '201310').

card_in_set('ramses overdark', 'ME3').
card_original_type('ramses overdark'/'ME3', 'Legendary Creature — Human Assassin').
card_original_text('ramses overdark'/'ME3', '{T}: Destroy target enchanted creature.').
card_image_name('ramses overdark'/'ME3', 'ramses overdark').
card_uid('ramses overdark'/'ME3', 'ME3:Ramses Overdark:ramses overdark').
card_rarity('ramses overdark'/'ME3', 'Uncommon').
card_artist('ramses overdark'/'ME3', 'Richard Kane Ferguson').
card_number('ramses overdark'/'ME3', '169').
card_multiverse_id('ramses overdark'/'ME3', '201223').

card_in_set('rasputin dreamweaver', 'ME3').
card_original_type('rasputin dreamweaver'/'ME3', 'Legendary Creature — Human Wizard').
card_original_text('rasputin dreamweaver'/'ME3', 'Rasputin Dreamweaver enters the battlefield with seven dream counters on it.\nRemove a dream counter from Rasputin: Add {1} to your mana pool.\nRemove a dream counter from Rasputin: Prevent the next 1 damage that would be dealt to Rasputin this turn.\nAt the beginning of your upkeep, if Rasputin started the turn untapped, put a dream counter on it. Rasputin can\'t have more than seven dream counters on it.').
card_image_name('rasputin dreamweaver'/'ME3', 'rasputin dreamweaver').
card_uid('rasputin dreamweaver'/'ME3', 'ME3:Rasputin Dreamweaver:rasputin dreamweaver').
card_rarity('rasputin dreamweaver'/'ME3', 'Rare').
card_artist('rasputin dreamweaver'/'ME3', 'Andi Rusu').
card_number('rasputin dreamweaver'/'ME3', '170').
card_multiverse_id('rasputin dreamweaver'/'ME3', '201224').

card_in_set('recall', 'ME3').
card_original_type('recall'/'ME3', 'Sorcery').
card_original_text('recall'/'ME3', 'Discard X cards, then return a card from your graveyard to your hand for each card discarded this way. Exile Recall.').
card_image_name('recall'/'ME3', 'recall').
card_uid('recall'/'ME3', 'ME3:Recall:recall').
card_rarity('recall'/'ME3', 'Uncommon').
card_artist('recall'/'ME3', 'Brian Snõddy').
card_number('recall'/'ME3', '46').
card_multiverse_id('recall'/'ME3', '201161').

card_in_set('reincarnation', 'ME3').
card_original_type('reincarnation'/'ME3', 'Instant').
card_original_text('reincarnation'/'ME3', 'Choose target creature. When that creature is put into a graveyard this turn, return a creature card from that graveyard to the battlefield under the control of that creature\'s owner.').
card_image_name('reincarnation'/'ME3', 'reincarnation').
card_uid('reincarnation'/'ME3', 'ME3:Reincarnation:reincarnation').
card_rarity('reincarnation'/'ME3', 'Uncommon').
card_artist('reincarnation'/'ME3', 'Edward P. Beard, Jr.').
card_number('reincarnation'/'ME3', '130').
card_multiverse_id('reincarnation'/'ME3', '201258').

card_in_set('remove soul', 'ME3').
card_original_type('remove soul'/'ME3', 'Instant').
card_original_text('remove soul'/'ME3', 'Counter target creature spell.').
card_image_name('remove soul'/'ME3', 'remove soul').
card_uid('remove soul'/'ME3', 'ME3:Remove Soul:remove soul').
card_rarity('remove soul'/'ME3', 'Common').
card_artist('remove soul'/'ME3', 'Brian Snõddy').
card_number('remove soul'/'ME3', '47').
card_flavor_text('remove soul'/'ME3', 'Nethya stiffened suddenly, head cocked as if straining to hear some distant sound, then felt lifeless to the ground.').
card_multiverse_id('remove soul'/'ME3', '201269').

card_in_set('reset', 'ME3').
card_original_type('reset'/'ME3', 'Instant').
card_original_text('reset'/'ME3', 'Cast Reset only during an opponent\'s turn after his or her upkeep step. \nUntap all lands you control.').
card_image_name('reset'/'ME3', 'reset').
card_uid('reset'/'ME3', 'ME3:Reset:reset').
card_rarity('reset'/'ME3', 'Rare').
card_artist('reset'/'ME3', 'Nicola Leonard').
card_number('reset'/'ME3', '48').
card_multiverse_id('reset'/'ME3', '201163').

card_in_set('reveka, wizard savant', 'ME3').
card_original_type('reveka, wizard savant'/'ME3', 'Legendary Creature — Dwarf Wizard').
card_original_text('reveka, wizard savant'/'ME3', '{T}: Reveka, Wizard Savant deals 2 damage to target creature or player and doesn\'t untap during your next untap step.').
card_image_name('reveka, wizard savant'/'ME3', 'reveka, wizard savant').
card_uid('reveka, wizard savant'/'ME3', 'ME3:Reveka, Wizard Savant:reveka, wizard savant').
card_rarity('reveka, wizard savant'/'ME3', 'Uncommon').
card_artist('reveka, wizard savant'/'ME3', 'Susan Van Camp').
card_number('reveka, wizard savant'/'ME3', '49').
card_flavor_text('reveka, wizard savant'/'ME3', '\"It\'s nice to see a sister Dwarf in a position of such power, but why\'d it have to be one of those seafaring muleheads?\"\n—Halina, Dwarven Trader').
card_multiverse_id('reveka, wizard savant'/'ME3', '201309').

card_in_set('riding the dilu horse', 'ME3').
card_original_type('riding the dilu horse'/'ME3', 'Sorcery').
card_original_text('riding the dilu horse'/'ME3', 'Target creature gets +2/+2 and gains horsemanship.').
card_image_name('riding the dilu horse'/'ME3', 'riding the dilu horse').
card_uid('riding the dilu horse'/'ME3', 'ME3:Riding the Dilu Horse:riding the dilu horse').
card_rarity('riding the dilu horse'/'ME3', 'Uncommon').
card_artist('riding the dilu horse'/'ME3', 'Hong Yan').
card_number('riding the dilu horse'/'ME3', '131').
card_flavor_text('riding the dilu horse'/'ME3', '\"All men have their appointed time; that\'s something no horse can change.\"\n—Liu Bei, after being told that the Dilu brings its master ill fortune').
card_multiverse_id('riding the dilu horse'/'ME3', '201322').

card_in_set('riven turnbull', 'ME3').
card_original_type('riven turnbull'/'ME3', 'Legendary Creature — Human Advisor').
card_original_text('riven turnbull'/'ME3', '{T}: Add {B} to your mana pool.').
card_image_name('riven turnbull'/'ME3', 'riven turnbull').
card_uid('riven turnbull'/'ME3', 'ME3:Riven Turnbull:riven turnbull').
card_rarity('riven turnbull'/'ME3', 'Uncommon').
card_artist('riven turnbull'/'ME3', 'Richard Kane Ferguson').
card_number('riven turnbull'/'ME3', '171').
card_flavor_text('riven turnbull'/'ME3', '\"Political violence is a perfectly legitimate answer to the persecution handed down by dignitaries of the state.\"').
card_multiverse_id('riven turnbull'/'ME3', '201225').

card_in_set('rohgahh of kher keep', 'ME3').
card_original_type('rohgahh of kher keep'/'ME3', 'Legendary Creature — Kobold').
card_original_text('rohgahh of kher keep'/'ME3', 'At the beginning of your upkeep, unless you pay {R}{R}{R}, an opponent gains control of Rohgahh of Kher Keep and all creatures you control named Kobolds of Kher Keep. If a player gains control of a creature this way, tap it.\nCreatures you control named Kobolds of Kher Keep get +2/+2.').
card_image_name('rohgahh of kher keep'/'ME3', 'rohgahh of kher keep').
card_uid('rohgahh of kher keep'/'ME3', 'ME3:Rohgahh of Kher Keep:rohgahh of kher keep').
card_rarity('rohgahh of kher keep'/'ME3', 'Rare').
card_artist('rohgahh of kher keep'/'ME3', 'Edward P. Beard, Jr.').
card_number('rohgahh of kher keep'/'ME3', '172').
card_multiverse_id('rohgahh of kher keep'/'ME3', '201226').

card_in_set('rolling earthquake', 'ME3').
card_original_type('rolling earthquake'/'ME3', 'Sorcery').
card_original_text('rolling earthquake'/'ME3', 'Rolling Earthquake deals X damage to each creature without horsemanship and each player.').
card_image_name('rolling earthquake'/'ME3', 'rolling earthquake').
card_uid('rolling earthquake'/'ME3', 'ME3:Rolling Earthquake:rolling earthquake').
card_rarity('rolling earthquake'/'ME3', 'Rare').
card_artist('rolling earthquake'/'ME3', 'Yang Hong').
card_number('rolling earthquake'/'ME3', '110').
card_multiverse_id('rolling earthquake'/'ME3', '201293').

card_in_set('rubinia soulsinger', 'ME3').
card_original_type('rubinia soulsinger'/'ME3', 'Legendary Creature — Faerie').
card_original_text('rubinia soulsinger'/'ME3', 'You may choose not to untap Rubinia Soulsinger during your untap step.\n{T}: Gain control of target creature as long as you control Rubinia and Rubinia remains tapped.').
card_image_name('rubinia soulsinger'/'ME3', 'rubinia soulsinger').
card_uid('rubinia soulsinger'/'ME3', 'ME3:Rubinia Soulsinger:rubinia soulsinger').
card_rarity('rubinia soulsinger'/'ME3', 'Rare').
card_artist('rubinia soulsinger'/'ME3', 'Rob Alexander').
card_number('rubinia soulsinger'/'ME3', '173').
card_multiverse_id('rubinia soulsinger'/'ME3', '201227').

card_in_set('scrubland', 'ME3').
card_original_type('scrubland'/'ME3', 'Land — Plains Swamp').
card_original_text('scrubland'/'ME3', '').
card_image_name('scrubland'/'ME3', 'scrubland').
card_uid('scrubland'/'ME3', 'ME3:Scrubland:scrubland').
card_rarity('scrubland'/'ME3', 'Rare').
card_artist('scrubland'/'ME3', 'Jesper Myrfors').
card_number('scrubland'/'ME3', '210').
card_multiverse_id('scrubland'/'ME3', '201403').

card_in_set('scryb sprites', 'ME3').
card_original_type('scryb sprites'/'ME3', 'Creature — Faerie').
card_original_text('scryb sprites'/'ME3', 'Flying').
card_image_name('scryb sprites'/'ME3', 'scryb sprites').
card_uid('scryb sprites'/'ME3', 'ME3:Scryb Sprites:scryb sprites').
card_rarity('scryb sprites'/'ME3', 'Common').
card_artist('scryb sprites'/'ME3', 'Amy Weber').
card_number('scryb sprites'/'ME3', '132').
card_flavor_text('scryb sprites'/'ME3', 'The only sound was the gentle clicking of the faeries\' wings. Then those intruders who were still standing turned and fled. One thing was certain: they didn\'t think the scryb were very funny anymore.').
card_multiverse_id('scryb sprites'/'ME3', '201404').

card_in_set('shu cavalry', 'ME3').
card_original_type('shu cavalry'/'ME3', 'Creature — Human Soldier').
card_original_text('shu cavalry'/'ME3', 'Horsemanship').
card_image_name('shu cavalry'/'ME3', 'shu cavalry').
card_uid('shu cavalry'/'ME3', 'ME3:Shu Cavalry:shu cavalry').
card_rarity('shu cavalry'/'ME3', 'Common').
card_artist('shu cavalry'/'ME3', 'Li Xiaohua').
card_number('shu cavalry'/'ME3', '23').
card_flavor_text('shu cavalry'/'ME3', 'In establishing the Shu kingdom, Liu Bei\'s forces fought against Ma Chao at Chengdu. Eventually, Ma Chao surrendered and became one of Liu Bei\'s Tiger Generals.').
card_multiverse_id('shu cavalry'/'ME3', '201294').

card_in_set('shu elite companions', 'ME3').
card_original_type('shu elite companions'/'ME3', 'Creature — Human Soldier').
card_original_text('shu elite companions'/'ME3', 'Horsemanship').
card_image_name('shu elite companions'/'ME3', 'shu elite companions').
card_uid('shu elite companions'/'ME3', 'ME3:Shu Elite Companions:shu elite companions').
card_rarity('shu elite companions'/'ME3', 'Common').
card_artist('shu elite companions'/'ME3', 'Qiao Dafu').
card_number('shu elite companions'/'ME3', '24').
card_flavor_text('shu elite companions'/'ME3', 'Throughout the three kingdoms, important generals were often guarded by small groups of expert soldiers known as \"elite companions.\"').
card_multiverse_id('shu elite companions'/'ME3', '201295').

card_in_set('shu general', 'ME3').
card_original_type('shu general'/'ME3', 'Creature — Human Soldier').
card_original_text('shu general'/'ME3', 'Horsemanship, vigilance').
card_image_name('shu general'/'ME3', 'shu general').
card_uid('shu general'/'ME3', 'ME3:Shu General:shu general').
card_rarity('shu general'/'ME3', 'Common').
card_artist('shu general'/'ME3', 'Li Xiaohua').
card_number('shu general'/'ME3', '25').
card_multiverse_id('shu general'/'ME3', '201297').

card_in_set('shu soldier-farmers', 'ME3').
card_original_type('shu soldier-farmers'/'ME3', 'Creature — Human Soldier').
card_original_text('shu soldier-farmers'/'ME3', 'When Shu Soldier-Farmers enters the battlefield, you gain 4 life.').
card_image_name('shu soldier-farmers'/'ME3', 'shu soldier-farmers').
card_uid('shu soldier-farmers'/'ME3', 'ME3:Shu Soldier-Farmers:shu soldier-farmers').
card_rarity('shu soldier-farmers'/'ME3', 'Common').
card_artist('shu soldier-farmers'/'ME3', 'Li Xiaohua').
card_number('shu soldier-farmers'/'ME3', '26').
card_flavor_text('shu soldier-farmers'/'ME3', 'During Kongming\'s campaigns against the Wei, his Shu troops rotated from the battlefront to the fields every hundred days.').
card_multiverse_id('shu soldier-farmers'/'ME3', '201298').

card_in_set('sir shandlar of eberyn', 'ME3').
card_original_type('sir shandlar of eberyn'/'ME3', 'Legendary Creature — Human Knight').
card_original_text('sir shandlar of eberyn'/'ME3', '').
card_image_name('sir shandlar of eberyn'/'ME3', 'sir shandlar of eberyn').
card_uid('sir shandlar of eberyn'/'ME3', 'ME3:Sir Shandlar of Eberyn:sir shandlar of eberyn').
card_rarity('sir shandlar of eberyn'/'ME3', 'Common').
card_artist('sir shandlar of eberyn'/'ME3', 'Andi Rusu').
card_number('sir shandlar of eberyn'/'ME3', '174').
card_flavor_text('sir shandlar of eberyn'/'ME3', '\"Remember Sir Shandlar! Remember and stand firm!\"\n—rallying cry of the Eberyn militia').
card_multiverse_id('sir shandlar of eberyn'/'ME3', '201229').

card_in_set('sivitri scarzam', 'ME3').
card_original_type('sivitri scarzam'/'ME3', 'Legendary Creature — Human').
card_original_text('sivitri scarzam'/'ME3', '').
card_image_name('sivitri scarzam'/'ME3', 'sivitri scarzam').
card_uid('sivitri scarzam'/'ME3', 'ME3:Sivitri Scarzam:sivitri scarzam').
card_rarity('sivitri scarzam'/'ME3', 'Common').
card_artist('sivitri scarzam'/'ME3', 'NéNé Thomas').
card_number('sivitri scarzam'/'ME3', '175').
card_flavor_text('sivitri scarzam'/'ME3', 'Even the brave have cause to tremble at the sight of Sivitri Scarzam. Who else has tamed Scarzam\'s Dragon?').
card_multiverse_id('sivitri scarzam'/'ME3', '205868').

card_in_set('slashing tiger', 'ME3').
card_original_type('slashing tiger'/'ME3', 'Creature — Cat').
card_original_text('slashing tiger'/'ME3', 'Whenever Slashing Tiger becomes blocked, it gets +2/+2 until end of turn.').
card_image_name('slashing tiger'/'ME3', 'slashing tiger').
card_uid('slashing tiger'/'ME3', 'ME3:Slashing Tiger:slashing tiger').
card_rarity('slashing tiger'/'ME3', 'Common').
card_artist('slashing tiger'/'ME3', 'Yang Jun Kwon').
card_number('slashing tiger'/'ME3', '133').
card_flavor_text('slashing tiger'/'ME3', '\"Unless you enter the tiger\'s lair, you cannot get hold of the tiger\'s cubs.\"\n—Sun Tzu, Art of War (trans. Giles)').
card_multiverse_id('slashing tiger'/'ME3', '201187').

card_in_set('sol grail', 'ME3').
card_original_type('sol grail'/'ME3', 'Artifact').
card_original_text('sol grail'/'ME3', 'As Sol Grail enters the battlefield, choose a color.\n{T}: Add one mana of the chosen color to your mana pool.').
card_image_name('sol grail'/'ME3', 'sol grail').
card_uid('sol grail'/'ME3', 'ME3:Sol Grail:sol grail').
card_rarity('sol grail'/'ME3', 'Common').
card_artist('sol grail'/'ME3', 'Christopher Rush').
card_number('sol grail'/'ME3', '201').
card_flavor_text('sol grail'/'ME3', '\"Look deep into the Grail, and see there what you desire most.\"\n—Gerda Äagesdotter, Archmage of the Unseen').
card_multiverse_id('sol grail'/'ME3', '201183').

card_in_set('sorrow\'s path', 'ME3').
card_original_type('sorrow\'s path'/'ME3', 'Land').
card_original_text('sorrow\'s path'/'ME3', '{T}: Choose two target blocking creatures an opponent controls. If each of those creatures could block all creatures that the other is blocking, remove both of them from combat. Each one then blocks all creatures the other was blocking. \nWhenever Sorrow\'s Path becomes tapped, it deals 2 damage to you and each creature you control.').
card_image_name('sorrow\'s path'/'ME3', 'sorrow\'s path').
card_uid('sorrow\'s path'/'ME3', 'ME3:Sorrow\'s Path:sorrow\'s path').
card_rarity('sorrow\'s path'/'ME3', 'Rare').
card_artist('sorrow\'s path'/'ME3', 'Randy Asplund-Faith').
card_number('sorrow\'s path'/'ME3', '211').
card_multiverse_id('sorrow\'s path'/'ME3', '201268').

card_in_set('spectral shield', 'ME3').
card_original_type('spectral shield'/'ME3', 'Enchantment — Aura').
card_original_text('spectral shield'/'ME3', 'Enchant creature\nEnchanted creature gets +0/+2 and can\'t be the target of spells.').
card_image_name('spectral shield'/'ME3', 'spectral shield').
card_uid('spectral shield'/'ME3', 'ME3:Spectral Shield:spectral shield').
card_rarity('spectral shield'/'ME3', 'Uncommon').
card_artist('spectral shield'/'ME3', 'Margaret Organ-Kean').
card_number('spectral shield'/'ME3', '176').
card_flavor_text('spectral shield'/'ME3', '\"What can be a stronger shield than concealment? Have you ever defeated an enemy you didn\'t know existed?\"\n—Gerda Äagesdotter, Archmage of the Unseen').
card_multiverse_id('spectral shield'/'ME3', '201189').

card_in_set('spiny starfish', 'ME3').
card_original_type('spiny starfish'/'ME3', 'Creature — Starfish').
card_original_text('spiny starfish'/'ME3', '{U}: Regenerate Spiny Starfish.\nAt the beginning of the end step, if Spiny Starfish regenerated this turn, put a 0/1 blue Starfish creature token onto the battlefield for each time it regenerated this turn.').
card_image_name('spiny starfish'/'ME3', 'spiny starfish').
card_uid('spiny starfish'/'ME3', 'ME3:Spiny Starfish:spiny starfish').
card_rarity('spiny starfish'/'ME3', 'Uncommon').
card_artist('spiny starfish'/'ME3', 'Alan Rabinowitz').
card_number('spiny starfish'/'ME3', '50').
card_multiverse_id('spiny starfish'/'ME3', '201124').

card_in_set('spirit shackle', 'ME3').
card_original_type('spirit shackle'/'ME3', 'Enchantment — Aura').
card_original_text('spirit shackle'/'ME3', 'Enchant creature\nWhenever enchanted creature becomes tapped, put a -0/-2 counter on it.').
card_image_name('spirit shackle'/'ME3', 'spirit shackle').
card_uid('spirit shackle'/'ME3', 'ME3:Spirit Shackle:spirit shackle').
card_rarity('spirit shackle'/'ME3', 'Common').
card_artist('spirit shackle'/'ME3', 'Edward P. Beard, Jr.').
card_number('spirit shackle'/'ME3', '74').
card_multiverse_id('spirit shackle'/'ME3', '201143').

card_in_set('spoils of victory', 'ME3').
card_original_type('spoils of victory'/'ME3', 'Sorcery').
card_original_text('spoils of victory'/'ME3', 'Search your library for a basic land card and put that card onto the battlefield. Then shuffle your library.').
card_image_name('spoils of victory'/'ME3', 'spoils of victory').
card_uid('spoils of victory'/'ME3', 'ME3:Spoils of Victory:spoils of victory').
card_rarity('spoils of victory'/'ME3', 'Common').
card_artist('spoils of victory'/'ME3', 'Sun Nan').
card_number('spoils of victory'/'ME3', '134').
card_multiverse_id('spoils of victory'/'ME3', '201299').

card_in_set('stangg', 'ME3').
card_original_type('stangg'/'ME3', 'Legendary Creature — Human Warrior').
card_original_text('stangg'/'ME3', 'When Stangg enters the battlefield, if Stangg is on the battlefield, put a legendary 3/4 red and green Human Warrior creature token named Stangg Twin onto the battlefield. When Stangg leaves the battlefield, exile the token. When that token leaves the battlefield, sacrifice Stangg.').
card_image_name('stangg'/'ME3', 'stangg').
card_uid('stangg'/'ME3', 'ME3:Stangg:stangg').
card_rarity('stangg'/'ME3', 'Uncommon').
card_artist('stangg'/'ME3', 'Mark Poole').
card_number('stangg'/'ME3', '177').
card_multiverse_id('stangg'/'ME3', '201231').

card_in_set('stolen grain', 'ME3').
card_original_type('stolen grain'/'ME3', 'Sorcery').
card_original_text('stolen grain'/'ME3', 'Stolen Grain deals 5 damage to target opponent. You gain 5 life.').
card_image_name('stolen grain'/'ME3', 'stolen grain').
card_uid('stolen grain'/'ME3', 'ME3:Stolen Grain:stolen grain').
card_rarity('stolen grain'/'ME3', 'Uncommon').
card_artist('stolen grain'/'ME3', 'LHQ').
card_number('stolen grain'/'ME3', '75').
card_flavor_text('stolen grain'/'ME3', 'At the battle of Guandu, Cao Cao defeated Yuan Shao by raiding his grain depot, leaving him with no way to feed his troops.').
card_multiverse_id('stolen grain'/'ME3', '201289').

card_in_set('storm world', 'ME3').
card_original_type('storm world'/'ME3', 'World Enchantment').
card_original_text('storm world'/'ME3', 'At the beginning of each player\'s upkeep, Storm World deals X damage to that player, where X is 4 minus the number of cards in his or her hand.').
card_image_name('storm world'/'ME3', 'storm world').
card_uid('storm world'/'ME3', 'ME3:Storm World:storm world').
card_rarity('storm world'/'ME3', 'Rare').
card_artist('storm world'/'ME3', 'Christopher Rush').
card_number('storm world'/'ME3', '111').
card_multiverse_id('storm world'/'ME3', '201165').

card_in_set('strategic planning', 'ME3').
card_original_type('strategic planning'/'ME3', 'Sorcery').
card_original_text('strategic planning'/'ME3', 'Look at the top three cards of your library. Put one of them into your hand and the rest into your graveyard.').
card_image_name('strategic planning'/'ME3', 'strategic planning').
card_uid('strategic planning'/'ME3', 'ME3:Strategic Planning:strategic planning').
card_rarity('strategic planning'/'ME3', 'Common').
card_artist('strategic planning'/'ME3', 'Zhang Jiazhen').
card_number('strategic planning'/'ME3', '51').
card_flavor_text('strategic planning'/'ME3', '\"Plans evolved within the tent decide a victory 1,000 li away.\"').
card_multiverse_id('strategic planning'/'ME3', '201300').

card_in_set('sun ce, young conquerer', 'ME3').
card_original_type('sun ce, young conquerer'/'ME3', 'Legendary Creature — Human Soldier').
card_original_text('sun ce, young conquerer'/'ME3', 'Horsemanship\nWhen Sun Ce, Young Conquerer enters the battlefield, you may return target creature to its owner\'s hand.').
card_image_name('sun ce, young conquerer'/'ME3', 'sun ce, young conquerer').
card_uid('sun ce, young conquerer'/'ME3', 'ME3:Sun Ce, Young Conquerer:sun ce, young conquerer').
card_rarity('sun ce, young conquerer'/'ME3', 'Uncommon').
card_artist('sun ce, young conquerer'/'ME3', 'Yang Guangmai').
card_number('sun ce, young conquerer'/'ME3', '52').
card_multiverse_id('sun ce, young conquerer'/'ME3', '201301').

card_in_set('sun quan, lord of wu', 'ME3').
card_original_type('sun quan, lord of wu'/'ME3', 'Legendary Creature — Human Soldier').
card_original_text('sun quan, lord of wu'/'ME3', 'Creatures you control have horsemanship.').
card_image_name('sun quan, lord of wu'/'ME3', 'sun quan, lord of wu').
card_uid('sun quan, lord of wu'/'ME3', 'ME3:Sun Quan, Lord of Wu:sun quan, lord of wu').
card_rarity('sun quan, lord of wu'/'ME3', 'Rare').
card_artist('sun quan, lord of wu'/'ME3', 'Xu Xiaoming').
card_number('sun quan, lord of wu'/'ME3', '53').
card_flavor_text('sun quan, lord of wu'/'ME3', '\"One score and four he reigned, the Southland king: / A dragon coiled, a tiger poised below the mighty Yangtze.\"').
card_multiverse_id('sun quan, lord of wu'/'ME3', '201302').

card_in_set('sunastian falconer', 'ME3').
card_original_type('sunastian falconer'/'ME3', 'Legendary Creature — Human Shaman').
card_original_text('sunastian falconer'/'ME3', '{T}: Add {2} to your mana pool.').
card_image_name('sunastian falconer'/'ME3', 'sunastian falconer').
card_uid('sunastian falconer'/'ME3', 'ME3:Sunastian Falconer:sunastian falconer').
card_rarity('sunastian falconer'/'ME3', 'Uncommon').
card_artist('sunastian falconer'/'ME3', 'Christopher Rush').
card_number('sunastian falconer'/'ME3', '178').
card_flavor_text('sunastian falconer'/'ME3', 'Sunastian has roots in both sorcery and swordplay; he has learned never to depend too heavily on the latter.').
card_multiverse_id('sunastian falconer'/'ME3', '201232').

card_in_set('swamp', 'ME3').
card_original_type('swamp'/'ME3', 'Basic Land — Swamp').
card_original_text('swamp'/'ME3', 'B').
card_image_name('swamp'/'ME3', 'swamp1').
card_uid('swamp'/'ME3', 'ME3:Swamp:swamp1').
card_rarity('swamp'/'ME3', 'Basic Land').
card_artist('swamp'/'ME3', 'Wang Chuxiong').
card_number('swamp'/'ME3', '222').
card_multiverse_id('swamp'/'ME3', '206083').

card_in_set('swamp', 'ME3').
card_original_type('swamp'/'ME3', 'Basic Land — Swamp').
card_original_text('swamp'/'ME3', 'B').
card_image_name('swamp'/'ME3', 'swamp2').
card_uid('swamp'/'ME3', 'ME3:Swamp:swamp2').
card_rarity('swamp'/'ME3', 'Basic Land').
card_artist('swamp'/'ME3', 'Wang Chuxiong').
card_number('swamp'/'ME3', '223').
card_multiverse_id('swamp'/'ME3', '206081').

card_in_set('swamp', 'ME3').
card_original_type('swamp'/'ME3', 'Basic Land — Swamp').
card_original_text('swamp'/'ME3', 'B').
card_image_name('swamp'/'ME3', 'swamp3').
card_uid('swamp'/'ME3', 'ME3:Swamp:swamp3').
card_rarity('swamp'/'ME3', 'Basic Land').
card_artist('swamp'/'ME3', 'Wang Chuxiong').
card_number('swamp'/'ME3', '224').
card_multiverse_id('swamp'/'ME3', '206082').

card_in_set('sword of the ages', 'ME3').
card_original_type('sword of the ages'/'ME3', 'Artifact').
card_original_text('sword of the ages'/'ME3', 'Sword of the Ages enters the battlefield tapped.\n{T}, Exile Sword of the Ages and any number of creatures you control: Sword of the Ages deals X damage to target creature or player, where X is the total power of the creatures exiled this way.').
card_image_name('sword of the ages'/'ME3', 'sword of the ages').
card_uid('sword of the ages'/'ME3', 'ME3:Sword of the Ages:sword of the ages').
card_rarity('sword of the ages'/'ME3', 'Rare').
card_artist('sword of the ages'/'ME3', 'Dan Frazier').
card_number('sword of the ages'/'ME3', '202').
card_multiverse_id('sword of the ages'/'ME3', '201233').

card_in_set('takklemaggot', 'ME3').
card_original_type('takklemaggot'/'ME3', 'Enchantment — Aura').
card_original_text('takklemaggot'/'ME3', 'Enchant creature\nAt the beginning of the upkeep of enchanted creature\'s controller, put a -0/-1 counter on that creature.\nWhen enchanted creature is put into a graveyard, that creature\'s controller chooses a creature that Takklemaggot could enchant. If he or she does, return Takklemaggot to the battlefield under your control attached to that creature. If he or she doesn\'t, return Takklemaggot to the battlefield under your control as a non-Aura enchantment. It loses \"enchant creature\" and gains \"At the beginning of that player\'s upkeep, Takklemaggot deals 1 damage to him or her.\"').
card_image_name('takklemaggot'/'ME3', 'takklemaggot').
card_uid('takklemaggot'/'ME3', 'ME3:Takklemaggot:takklemaggot').
card_rarity('takklemaggot'/'ME3', 'Uncommon').
card_artist('takklemaggot'/'ME3', 'Daniel Gelon').
card_number('takklemaggot'/'ME3', '76').
card_multiverse_id('takklemaggot'/'ME3', '201230').

card_in_set('tetsuo umezawa', 'ME3').
card_original_type('tetsuo umezawa'/'ME3', 'Legendary Creature — Human Archer').
card_original_text('tetsuo umezawa'/'ME3', 'Tetsuo Umezawa can\'t be the target of Aura spells.\n{U}{B}{B}{R}, {T}: Destroy target tapped or blocking creature.').
card_image_name('tetsuo umezawa'/'ME3', 'tetsuo umezawa').
card_uid('tetsuo umezawa'/'ME3', 'ME3:Tetsuo Umezawa:tetsuo umezawa').
card_rarity('tetsuo umezawa'/'ME3', 'Rare').
card_artist('tetsuo umezawa'/'ME3', 'Julie Baroh').
card_number('tetsuo umezawa'/'ME3', '179').
card_multiverse_id('tetsuo umezawa'/'ME3', '201234').

card_in_set('the abyss', 'ME3').
card_original_type('the abyss'/'ME3', 'World Enchantment').
card_original_text('the abyss'/'ME3', 'At the beginning of each player\'s upkeep, destroy target nonartifact creature that player controls of his or her choice. It can\'t be regenerated.').
card_image_name('the abyss'/'ME3', 'the abyss').
card_uid('the abyss'/'ME3', 'ME3:The Abyss:the abyss').
card_rarity('the abyss'/'ME3', 'Rare').
card_artist('the abyss'/'ME3', 'Pete Venters').
card_number('the abyss'/'ME3', '77').
card_flavor_text('the abyss'/'ME3', '\"An immense river of oblivion is sweeping us away into a nameless abyss.\" —Ernest Renan, Souvenirs d\'Enfance et de Jeunesse').
card_multiverse_id('the abyss'/'ME3', '201167').

card_in_set('the lady of the mountain', 'ME3').
card_original_type('the lady of the mountain'/'ME3', 'Legendary Creature — Giant').
card_original_text('the lady of the mountain'/'ME3', '').
card_image_name('the lady of the mountain'/'ME3', 'the lady of the mountain').
card_uid('the lady of the mountain'/'ME3', 'ME3:The Lady of the Mountain:the lady of the mountain').
card_rarity('the lady of the mountain'/'ME3', 'Common').
card_artist('the lady of the mountain'/'ME3', 'Richard Kane Ferguson').
card_number('the lady of the mountain'/'ME3', '180').
card_flavor_text('the lady of the mountain'/'ME3', 'Her given name has been lost in the mists of time. Legend says that her silent vigil will one day be ended by the one who, pure of heart and spirit, calls out that name again.').
card_multiverse_id('the lady of the mountain'/'ME3', '201204').

card_in_set('the tabernacle at pendrell vale', 'ME3').
card_original_type('the tabernacle at pendrell vale'/'ME3', 'Legendary Land').
card_original_text('the tabernacle at pendrell vale'/'ME3', 'All creatures have \"At the beginning of your upkeep, sacrifice this creature unless you pay {1}.\"').
card_image_name('the tabernacle at pendrell vale'/'ME3', 'the tabernacle at pendrell vale').
card_uid('the tabernacle at pendrell vale'/'ME3', 'ME3:The Tabernacle at Pendrell Vale:the tabernacle at pendrell vale').
card_rarity('the tabernacle at pendrell vale'/'ME3', 'Rare').
card_artist('the tabernacle at pendrell vale'/'ME3', 'Nicola Leonard').
card_number('the tabernacle at pendrell vale'/'ME3', '212').
card_multiverse_id('the tabernacle at pendrell vale'/'ME3', '201236').

card_in_set('the wretched', 'ME3').
card_original_type('the wretched'/'ME3', 'Creature — Demon').
card_original_text('the wretched'/'ME3', 'At end of combat, gain control of all creatures blocking The Wretched as long as you control The Wretched.').
card_image_name('the wretched'/'ME3', 'the wretched').
card_uid('the wretched'/'ME3', 'ME3:The Wretched:the wretched').
card_rarity('the wretched'/'ME3', 'Uncommon').
card_artist('the wretched'/'ME3', 'Christopher Rush').
card_number('the wretched'/'ME3', '78').
card_multiverse_id('the wretched'/'ME3', '201168').

card_in_set('three visits', 'ME3').
card_original_type('three visits'/'ME3', 'Sorcery').
card_original_text('three visits'/'ME3', 'Search your library for a Forest card and put that card onto the battlefield. Then shuffle your library.').
card_image_name('three visits'/'ME3', 'three visits').
card_uid('three visits'/'ME3', 'ME3:Three Visits:three visits').
card_rarity('three visits'/'ME3', 'Common').
card_artist('three visits'/'ME3', 'Qu Xin').
card_number('three visits'/'ME3', '135').
card_flavor_text('three visits'/'ME3', '\"Trying to meet a worthy man in the wrong way is as bad as closing the door on an invited guest.\"\n—Mencius, a Chinese philosopher').
card_multiverse_id('three visits'/'ME3', '201303').

card_in_set('tobias andrion', 'ME3').
card_original_type('tobias andrion'/'ME3', 'Legendary Creature — Human Advisor').
card_original_text('tobias andrion'/'ME3', '').
card_image_name('tobias andrion'/'ME3', 'tobias andrion').
card_uid('tobias andrion'/'ME3', 'ME3:Tobias Andrion:tobias andrion').
card_rarity('tobias andrion'/'ME3', 'Common').
card_artist('tobias andrion'/'ME3', 'Andi Rusu').
card_number('tobias andrion'/'ME3', '181').
card_flavor_text('tobias andrion'/'ME3', 'Administrator of the military state of Sheoltun, Tobias is the military right arm of the empire and the figurehead of its freedom.').
card_multiverse_id('tobias andrion'/'ME3', '205870').

card_in_set('tor wauki', 'ME3').
card_original_type('tor wauki'/'ME3', 'Legendary Creature — Human Archer').
card_original_text('tor wauki'/'ME3', '{T}: Tor Wauki deals 2 damage to target attacking or blocking creature.').
card_image_name('tor wauki'/'ME3', 'tor wauki').
card_uid('tor wauki'/'ME3', 'ME3:Tor Wauki:tor wauki').
card_rarity('tor wauki'/'ME3', 'Uncommon').
card_artist('tor wauki'/'ME3', 'Randy Asplund-Faith').
card_number('tor wauki'/'ME3', '182').
card_multiverse_id('tor wauki'/'ME3', '201238').

card_in_set('torsten von ursus', 'ME3').
card_original_type('torsten von ursus'/'ME3', 'Legendary Creature — Human Soldier').
card_original_text('torsten von ursus'/'ME3', '').
card_image_name('torsten von ursus'/'ME3', 'torsten von ursus').
card_uid('torsten von ursus'/'ME3', 'ME3:Torsten Von Ursus:torsten von ursus').
card_rarity('torsten von ursus'/'ME3', 'Common').
card_artist('torsten von ursus'/'ME3', 'Mark Poole').
card_number('torsten von ursus'/'ME3', '183').
card_flavor_text('torsten von ursus'/'ME3', '\"How can you accuse me of evil? Though these deeds be unsavory, no one will argue: good shall follow from them.\"').
card_multiverse_id('torsten von ursus'/'ME3', '201157').

card_in_set('tracker', 'ME3').
card_original_type('tracker'/'ME3', 'Creature — Human').
card_original_text('tracker'/'ME3', '{G}{G}, {T}: Tracker deals damage equal to its power to target creature. That creature deals damage equal to its power to Tracker.').
card_image_name('tracker'/'ME3', 'tracker').
card_uid('tracker'/'ME3', 'ME3:Tracker:tracker').
card_rarity('tracker'/'ME3', 'Uncommon').
card_artist('tracker'/'ME3', 'Jeff A. Menges').
card_number('tracker'/'ME3', '136').
card_multiverse_id('tracker'/'ME3', '201270').

card_in_set('trip wire', 'ME3').
card_original_type('trip wire'/'ME3', 'Sorcery').
card_original_text('trip wire'/'ME3', 'Destroy target creature with horsemanship.').
card_image_name('trip wire'/'ME3', 'trip wire').
card_uid('trip wire'/'ME3', 'ME3:Trip Wire:trip wire').
card_rarity('trip wire'/'ME3', 'Common').
card_artist('trip wire'/'ME3', 'Hong Yan').
card_number('trip wire'/'ME3', '137').
card_flavor_text('trip wire'/'ME3', 'Trip wire, hooked poles, and sunken pits were commonly used to unhorse riders during the Three Kingdoms period.').
card_multiverse_id('trip wire'/'ME3', '201320').

card_in_set('tropical island', 'ME3').
card_original_type('tropical island'/'ME3', 'Land — Forest Island').
card_original_text('tropical island'/'ME3', '').
card_image_name('tropical island'/'ME3', 'tropical island').
card_uid('tropical island'/'ME3', 'ME3:Tropical Island:tropical island').
card_rarity('tropical island'/'ME3', 'Rare').
card_artist('tropical island'/'ME3', 'Jesper Myrfors').
card_number('tropical island'/'ME3', '213').
card_multiverse_id('tropical island'/'ME3', '201405').

card_in_set('tuknir deathlock', 'ME3').
card_original_type('tuknir deathlock'/'ME3', 'Legendary Creature — Human Wizard').
card_original_text('tuknir deathlock'/'ME3', 'Flying\n{R}{G}, {T}: Target creature gets +2/+2 until end of turn.').
card_image_name('tuknir deathlock'/'ME3', 'tuknir deathlock').
card_uid('tuknir deathlock'/'ME3', 'ME3:Tuknir Deathlock:tuknir deathlock').
card_rarity('tuknir deathlock'/'ME3', 'Uncommon').
card_artist('tuknir deathlock'/'ME3', 'Liz Danforth').
card_number('tuknir deathlock'/'ME3', '184').
card_flavor_text('tuknir deathlock'/'ME3', 'An explorer of the Æther, Tuknir often discovers himself in the most unusual physical realms.').
card_multiverse_id('tuknir deathlock'/'ME3', '201240').

card_in_set('urborg', 'ME3').
card_original_type('urborg'/'ME3', 'Legendary Land').
card_original_text('urborg'/'ME3', '{T}: Add {B} to your mana pool.\n{T}: Target creature loses first strike or swampwalk until end of turn.').
card_image_name('urborg'/'ME3', 'urborg').
card_uid('urborg'/'ME3', 'ME3:Urborg:urborg').
card_rarity('urborg'/'ME3', 'Uncommon').
card_artist('urborg'/'ME3', 'Bryon Wackwitz').
card_number('urborg'/'ME3', '214').
card_flavor_text('urborg'/'ME3', '\"Resignedly beneath the sky/The melancholy waters lie./So blend the turrets and shadows there/That all seem pendulous in air,/While from a proud tower in town/Death looks gigantically down.\"\n—Edgar Allan Poe, \"The City in the Sea\"').
card_multiverse_id('urborg'/'ME3', '201202').

card_in_set('vaevictis asmadi', 'ME3').
card_original_type('vaevictis asmadi'/'ME3', 'Legendary Creature — Elder Dragon').
card_original_text('vaevictis asmadi'/'ME3', 'Flying\nAt the beginning of your upkeep, sacrifice Vaevictis Asmadi unless you pay {B}{R}{G}.\n{B}: Vaevictis Asmadi gets +1/+0 until end of turn.\n{R}: Vaevictis Asmadi gets +1/+0 until end of turn.\n{G}: Vaevictis Asmadi gets +1/+0 until end of turn.').
card_image_name('vaevictis asmadi'/'ME3', 'vaevictis asmadi').
card_uid('vaevictis asmadi'/'ME3', 'ME3:Vaevictis Asmadi:vaevictis asmadi').
card_rarity('vaevictis asmadi'/'ME3', 'Rare').
card_artist('vaevictis asmadi'/'ME3', 'Andi Rusu').
card_number('vaevictis asmadi'/'ME3', '185').
card_multiverse_id('vaevictis asmadi'/'ME3', '201243').

card_in_set('volcanic island', 'ME3').
card_original_type('volcanic island'/'ME3', 'Land — Island Mountain').
card_original_text('volcanic island'/'ME3', '').
card_image_name('volcanic island'/'ME3', 'volcanic island').
card_uid('volcanic island'/'ME3', 'ME3:Volcanic Island:volcanic island').
card_rarity('volcanic island'/'ME3', 'Rare').
card_artist('volcanic island'/'ME3', 'Brian Snõddy').
card_number('volcanic island'/'ME3', '215').
card_multiverse_id('volcanic island'/'ME3', '201406').

card_in_set('voodoo doll', 'ME3').
card_original_type('voodoo doll'/'ME3', 'Artifact').
card_original_text('voodoo doll'/'ME3', 'At the beginning of your upkeep, put a pin counter on Voodoo Doll.\nAt the beginning of your end step, if Voodoo Doll is untapped, sacrifice it and it deals damage equal to the number of pin counters on it to you.\n{X}{X}, {T}: Voodoo Doll deals X damage to target creature or player. X is the number of pin counters on Voodoo Doll.').
card_image_name('voodoo doll'/'ME3', 'voodoo doll').
card_uid('voodoo doll'/'ME3', 'ME3:Voodoo Doll:voodoo doll').
card_rarity('voodoo doll'/'ME3', 'Rare').
card_artist('voodoo doll'/'ME3', 'Sandra Everingham').
card_number('voodoo doll'/'ME3', '203').
card_multiverse_id('voodoo doll'/'ME3', '206084').

card_in_set('wall of light', 'ME3').
card_original_type('wall of light'/'ME3', 'Creature — Wall').
card_original_text('wall of light'/'ME3', 'Defender (This creature can\'t attack.)\nProtection from black').
card_image_name('wall of light'/'ME3', 'wall of light').
card_uid('wall of light'/'ME3', 'ME3:Wall of Light:wall of light').
card_rarity('wall of light'/'ME3', 'Common').
card_artist('wall of light'/'ME3', 'Richard Thomas').
card_number('wall of light'/'ME3', '27').
card_flavor_text('wall of light'/'ME3', 'As many attackers were dazzled by the wall\'s beauty as were halted by its force.').
card_multiverse_id('wall of light'/'ME3', '201120').

card_in_set('wandering mage', 'ME3').
card_original_type('wandering mage'/'ME3', 'Creature — Human Cleric Wizard').
card_original_text('wandering mage'/'ME3', '{W}, Pay 1 life: Prevent the next 2 damage that would be dealt to target creature this turn.\n{U}: Prevent the next 1 damage that would be dealt to target Cleric or Wizard creature this turn.\n{B}, Put a -1/-1 counter on a creature you control: Prevent the next 2 damage that would be dealt to target player this turn.').
card_image_name('wandering mage'/'ME3', 'wandering mage').
card_uid('wandering mage'/'ME3', 'ME3:Wandering Mage:wandering mage').
card_rarity('wandering mage'/'ME3', 'Rare').
card_artist('wandering mage'/'ME3', 'Pete Venters').
card_number('wandering mage'/'ME3', '186').
card_multiverse_id('wandering mage'/'ME3', '201179').

card_in_set('wei elite companions', 'ME3').
card_original_type('wei elite companions'/'ME3', 'Creature — Human Soldier').
card_original_text('wei elite companions'/'ME3', 'Horsemanship').
card_image_name('wei elite companions'/'ME3', 'wei elite companions').
card_uid('wei elite companions'/'ME3', 'ME3:Wei Elite Companions:wei elite companions').
card_rarity('wei elite companions'/'ME3', 'Common').
card_artist('wei elite companions'/'ME3', 'Li Youliang').
card_number('wei elite companions'/'ME3', '79').
card_flavor_text('wei elite companions'/'ME3', 'Cao Cao was more concerned with capabilities than lineage. He excelled at recruiting and retaining men of talent to serve him.').
card_multiverse_id('wei elite companions'/'ME3', '201305').

card_in_set('wei infantry', 'ME3').
card_original_type('wei infantry'/'ME3', 'Creature — Human Soldier').
card_original_text('wei infantry'/'ME3', '').
card_image_name('wei infantry'/'ME3', 'wei infantry').
card_uid('wei infantry'/'ME3', 'ME3:Wei Infantry:wei infantry').
card_rarity('wei infantry'/'ME3', 'Common').
card_artist('wei infantry'/'ME3', 'LHQ').
card_number('wei infantry'/'ME3', '80').
card_flavor_text('wei infantry'/'ME3', 'Wei won the battle of Hefei when Zhang Liao defeated Sun Quan and then foiled a Wu scheme to incite rebellion.').
card_multiverse_id('wei infantry'/'ME3', '206117').

card_in_set('wei night raiders', 'ME3').
card_original_type('wei night raiders'/'ME3', 'Creature — Human Soldier').
card_original_text('wei night raiders'/'ME3', 'Horsemanship\nWhenever Wei Night Raiders deals damage to an opponent, that player discards a card.').
card_image_name('wei night raiders'/'ME3', 'wei night raiders').
card_uid('wei night raiders'/'ME3', 'ME3:Wei Night Raiders:wei night raiders').
card_rarity('wei night raiders'/'ME3', 'Uncommon').
card_artist('wei night raiders'/'ME3', 'Wang Feng').
card_number('wei night raiders'/'ME3', '81').
card_multiverse_id('wei night raiders'/'ME3', '201306').

card_in_set('wei strike force', 'ME3').
card_original_type('wei strike force'/'ME3', 'Creature — Human Soldier').
card_original_text('wei strike force'/'ME3', 'Horsemanship').
card_image_name('wei strike force'/'ME3', 'wei strike force').
card_uid('wei strike force'/'ME3', 'ME3:Wei Strike Force:wei strike force').
card_rarity('wei strike force'/'ME3', 'Common').
card_artist('wei strike force'/'ME3', 'Tang Xiaogu').
card_number('wei strike force'/'ME3', '82').
card_flavor_text('wei strike force'/'ME3', 'At the battle of Chang\'an, Ma Chao defeated the two generals Cao Cao sent to guard the pass but was forced to flee when Cao Cao\'s trickery turned his own ally against him.').
card_multiverse_id('wei strike force'/'ME3', '201307').

card_in_set('willow priestess', 'ME3').
card_original_type('willow priestess'/'ME3', 'Creature — Faerie Druid').
card_original_text('willow priestess'/'ME3', '{T}: You may put a Faerie permanent card from your hand onto the battlefield.\n{2}{G}: Target green creature gains protection from black until end of turn.').
card_image_name('willow priestess'/'ME3', 'willow priestess').
card_uid('willow priestess'/'ME3', 'ME3:Willow Priestess:willow priestess').
card_rarity('willow priestess'/'ME3', 'Uncommon').
card_artist('willow priestess'/'ME3', 'Susan Van Camp').
card_number('willow priestess'/'ME3', '138').
card_flavor_text('willow priestess'/'ME3', '\"Those of faith are those of strength.\"\n—Autumn Willow').
card_multiverse_id('willow priestess'/'ME3', '201321').

card_in_set('willow satyr', 'ME3').
card_original_type('willow satyr'/'ME3', 'Creature — Satyr').
card_original_text('willow satyr'/'ME3', 'You may choose not to untap Willow Satyr during your untap step.\n{T}: Gain control of target legendary creature as long as you control Willow Satyr and Willow Satyr remains tapped.').
card_image_name('willow satyr'/'ME3', 'willow satyr').
card_uid('willow satyr'/'ME3', 'ME3:Willow Satyr:willow satyr').
card_rarity('willow satyr'/'ME3', 'Rare').
card_artist('willow satyr'/'ME3', 'Jeff A. Menges').
card_number('willow satyr'/'ME3', '139').
card_multiverse_id('willow satyr'/'ME3', '201244').

card_in_set('wormwood treefolk', 'ME3').
card_original_type('wormwood treefolk'/'ME3', 'Creature — Treefolk').
card_original_text('wormwood treefolk'/'ME3', '{G}{G}: Wormwood Treefolk gains forestwalk until end of turn and deals 2 damage to you.\n{B}{B}: Wormwood Treefolk gains swampwalk until end of turn and deals 2 damage to you.').
card_image_name('wormwood treefolk'/'ME3', 'wormwood treefolk').
card_uid('wormwood treefolk'/'ME3', 'ME3:Wormwood Treefolk:wormwood treefolk').
card_rarity('wormwood treefolk'/'ME3', 'Uncommon').
card_artist('wormwood treefolk'/'ME3', 'Jesper Myrfors').
card_number('wormwood treefolk'/'ME3', '140').
card_multiverse_id('wormwood treefolk'/'ME3', '201271').

card_in_set('wu elite cavalry', 'ME3').
card_original_type('wu elite cavalry'/'ME3', 'Creature — Human Soldier').
card_original_text('wu elite cavalry'/'ME3', 'Horsemanship').
card_image_name('wu elite cavalry'/'ME3', 'wu elite cavalry').
card_uid('wu elite cavalry'/'ME3', 'ME3:Wu Elite Cavalry:wu elite cavalry').
card_rarity('wu elite cavalry'/'ME3', 'Common').
card_artist('wu elite cavalry'/'ME3', 'Li Wang').
card_number('wu elite cavalry'/'ME3', '54').
card_flavor_text('wu elite cavalry'/'ME3', 'At the second battle of Ruxu, the brave Wu general Gan Ning raided Cao Cao\'s camp of 400,000 men with only 100 cavalry. Not a single man or horse was lost.').
card_multiverse_id('wu elite cavalry'/'ME3', '201308').

card_in_set('wu longbowman', 'ME3').
card_original_type('wu longbowman'/'ME3', 'Creature — Human Soldier Archer').
card_original_text('wu longbowman'/'ME3', '{T}: Wu Longbowman deals 1 damage to target creature or player. Activate this ability only during your turn, before attackers are declared.').
card_image_name('wu longbowman'/'ME3', 'wu longbowman').
card_uid('wu longbowman'/'ME3', 'ME3:Wu Longbowman:wu longbowman').
card_rarity('wu longbowman'/'ME3', 'Common').
card_artist('wu longbowman'/'ME3', 'Xu Tan').
card_number('wu longbowman'/'ME3', '55').
card_multiverse_id('wu longbowman'/'ME3', '201254').

card_in_set('wu warship', 'ME3').
card_original_type('wu warship'/'ME3', 'Creature — Human Soldier').
card_original_text('wu warship'/'ME3', 'Wu Warship can\'t attack unless defending player controls an Island.').
card_image_name('wu warship'/'ME3', 'wu warship').
card_uid('wu warship'/'ME3', 'ME3:Wu Warship:wu warship').
card_rarity('wu warship'/'ME3', 'Common').
card_artist('wu warship'/'ME3', 'Jiang Zhuqing').
card_number('wu warship'/'ME3', '56').
card_flavor_text('wu warship'/'ME3', 'Both Wu and Wei warships patrolled the Yangtze River, the natural border between the two kingdoms.').
card_multiverse_id('wu warship'/'ME3', '201262').

card_in_set('xiahou dun, the one-eyed', 'ME3').
card_original_type('xiahou dun, the one-eyed'/'ME3', 'Legendary Creature — Human Soldier').
card_original_text('xiahou dun, the one-eyed'/'ME3', 'Horsemanship\nSacrifice Xiahou Dun, the One-Eyed: Return target black card from your graveyard to your hand. Activate this ability only during your turn, before attackers are declared.').
card_image_name('xiahou dun, the one-eyed'/'ME3', 'xiahou dun, the one-eyed').
card_uid('xiahou dun, the one-eyed'/'ME3', 'ME3:Xiahou Dun, the One-Eyed:xiahou dun, the one-eyed').
card_rarity('xiahou dun, the one-eyed'/'ME3', 'Uncommon').
card_artist('xiahou dun, the one-eyed'/'ME3', 'Junko Taguchi').
card_number('xiahou dun, the one-eyed'/'ME3', '83').
card_multiverse_id('xiahou dun, the one-eyed'/'ME3', '201164').

card_in_set('xira arien', 'ME3').
card_original_type('xira arien'/'ME3', 'Legendary Creature — Insect Wizard').
card_original_text('xira arien'/'ME3', 'Flying\n{B}{R}{G}, {T}: Target player draws a card.').
card_image_name('xira arien'/'ME3', 'xira arien').
card_uid('xira arien'/'ME3', 'ME3:Xira Arien:xira arien').
card_rarity('xira arien'/'ME3', 'Rare').
card_artist('xira arien'/'ME3', 'Melissa A. Benson').
card_number('xira arien'/'ME3', '187').
card_flavor_text('xira arien'/'ME3', 'A regular guest at the Royal Masquerade, Arien is the envy of the Court. She appears in a new costume every hour.').
card_multiverse_id('xira arien'/'ME3', '201208').

card_in_set('young wei recruits', 'ME3').
card_original_type('young wei recruits'/'ME3', 'Creature — Human Soldier').
card_original_text('young wei recruits'/'ME3', 'Young Wei Recruits can\'t block.').
card_image_name('young wei recruits'/'ME3', 'young wei recruits').
card_uid('young wei recruits'/'ME3', 'ME3:Young Wei Recruits:young wei recruits').
card_rarity('young wei recruits'/'ME3', 'Common').
card_artist('young wei recruits'/'ME3', 'Li Youliang').
card_number('young wei recruits'/'ME3', '84').
card_flavor_text('young wei recruits'/'ME3', '\"To send the common people to war untrained is to throw them away.\"\n—Confucius, The Analects (trans. Lau)').
card_multiverse_id('young wei recruits'/'ME3', '201145').

card_in_set('zhang fei, fierce warrior', 'ME3').
card_original_type('zhang fei, fierce warrior'/'ME3', 'Legendary Creature — Human Soldier Warrior').
card_original_text('zhang fei, fierce warrior'/'ME3', 'Horsemanship, vigilance').
card_image_name('zhang fei, fierce warrior'/'ME3', 'zhang fei, fierce warrior').
card_uid('zhang fei, fierce warrior'/'ME3', 'ME3:Zhang Fei, Fierce Warrior:zhang fei, fierce warrior').
card_rarity('zhang fei, fierce warrior'/'ME3', 'Uncommon').
card_artist('zhang fei, fierce warrior'/'ME3', 'Qiao Dafu').
card_number('zhang fei, fierce warrior'/'ME3', '28').
card_flavor_text('zhang fei, fierce warrior'/'ME3', 'Zhang Fei\'s uncharacteristic alliance with a defeated Riverlands general, Yan Yan, allowed Shu forces to advance through forty-five Riverlands strongpoints with no casualties.').
card_multiverse_id('zhang fei, fierce warrior'/'ME3', '201312').

card_in_set('zodiac dragon', 'ME3').
card_original_type('zodiac dragon'/'ME3', 'Creature — Dragon').
card_original_text('zodiac dragon'/'ME3', 'When Zodiac Dragon is put into your graveyard from play, you may return it to your hand.').
card_image_name('zodiac dragon'/'ME3', 'zodiac dragon').
card_uid('zodiac dragon'/'ME3', 'ME3:Zodiac Dragon:zodiac dragon').
card_rarity('zodiac dragon'/'ME3', 'Rare').
card_artist('zodiac dragon'/'ME3', 'Qi Baocheng').
card_number('zodiac dragon'/'ME3', '112').
card_flavor_text('zodiac dragon'/'ME3', '\". . . The kingdoms three are now the stuff of dream,/ For men to ponder, past all praise or blame.\"').
card_multiverse_id('zodiac dragon'/'ME3', '201313').
