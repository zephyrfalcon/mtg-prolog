% Beatdown Box Set

set('BTD').
set_name('BTD', 'Beatdown Box Set').
set_release_date('BTD', '2000-10-01').
set_border('BTD', 'white').
set_type('BTD', 'box').

card_in_set('air elemental', 'BTD').
card_original_type('air elemental'/'BTD', 'Creature — Elemental').
card_original_text('air elemental'/'BTD', 'Flying').
card_image_name('air elemental'/'BTD', 'air elemental').
card_uid('air elemental'/'BTD', 'BTD:Air Elemental:air elemental').
card_rarity('air elemental'/'BTD', 'Uncommon').
card_artist('air elemental'/'BTD', 'Doug Chaffee').
card_number('air elemental'/'BTD', '1').
card_flavor_text('air elemental'/'BTD', 'As insubstantial, and as powerful, as the wind that carries it.').
card_multiverse_id('air elemental'/'BTD', '26654').

card_in_set('balduvian horde', 'BTD').
card_original_type('balduvian horde'/'BTD', 'Creature — Barbarian').
card_original_text('balduvian horde'/'BTD', 'When Balduvian Horde comes into play, sacrifice it unless you discard a card at random from your hand.').
card_image_name('balduvian horde'/'BTD', 'balduvian horde').
card_uid('balduvian horde'/'BTD', 'BTD:Balduvian Horde:balduvian horde').
card_rarity('balduvian horde'/'BTD', 'Rare').
card_artist('balduvian horde'/'BTD', 'Brian Snõddy').
card_number('balduvian horde'/'BTD', '34').
card_multiverse_id('balduvian horde'/'BTD', '26655').

card_in_set('ball lightning', 'BTD').
card_original_type('ball lightning'/'BTD', 'Creature — Ball-Lightning').
card_original_text('ball lightning'/'BTD', 'Trample; haste (This creature may attack and {T} the turn it comes under your control.)\nAt end of turn, sacrifice Ball Lightning.').
card_image_name('ball lightning'/'BTD', 'ball lightning').
card_uid('ball lightning'/'BTD', 'BTD:Ball Lightning:ball lightning').
card_rarity('ball lightning'/'BTD', 'Rare').
card_artist('ball lightning'/'BTD', 'Dave Dorman').
card_number('ball lightning'/'BTD', '35').
card_multiverse_id('ball lightning'/'BTD', '26620').

card_in_set('blizzard elemental', 'BTD').
card_original_type('blizzard elemental'/'BTD', 'Creature — Elemental').
card_original_text('blizzard elemental'/'BTD', 'Flying\n{3}{U} Untap Blizzard Elemental.').
card_image_name('blizzard elemental'/'BTD', 'blizzard elemental').
card_uid('blizzard elemental'/'BTD', 'BTD:Blizzard Elemental:blizzard elemental').
card_rarity('blizzard elemental'/'BTD', 'Rare').
card_artist('blizzard elemental'/'BTD', 'Thomas M. Baxa').
card_number('blizzard elemental'/'BTD', '2').
card_flavor_text('blizzard elemental'/'BTD', 'Students who had seen Rayne argue with Urza were certain she had summoned it to make him seem warmer by comparison.').
card_multiverse_id('blizzard elemental'/'BTD', '26653').

card_in_set('bloodrock cyclops', 'BTD').
card_original_type('bloodrock cyclops'/'BTD', 'Creature — Giant').
card_original_text('bloodrock cyclops'/'BTD', 'Bloodrock Cyclops attacks each turn if able.').
card_image_name('bloodrock cyclops'/'BTD', 'bloodrock cyclops').
card_uid('bloodrock cyclops'/'BTD', 'BTD:Bloodrock Cyclops:bloodrock cyclops').
card_rarity('bloodrock cyclops'/'BTD', 'Common').
card_artist('bloodrock cyclops'/'BTD', 'Tom Wänerstrand').
card_number('bloodrock cyclops'/'BTD', '36').
card_flavor_text('bloodrock cyclops'/'BTD', '\"He\'s big an\' dumb an\' ready to fight. A lot like ol\' Hornhead here.\"\n—Squee, goblin cabin hand,\nscouting report').
card_multiverse_id('bloodrock cyclops'/'BTD', '26626').

card_in_set('bone harvest', 'BTD').
card_original_type('bone harvest'/'BTD', 'Instant').
card_original_text('bone harvest'/'BTD', 'Put any number of target creature cards from your graveyard on top of your library.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_image_name('bone harvest'/'BTD', 'bone harvest').
card_uid('bone harvest'/'BTD', 'BTD:Bone Harvest:bone harvest').
card_rarity('bone harvest'/'BTD', 'Common').
card_artist('bone harvest'/'BTD', 'Greg Simanson').
card_number('bone harvest'/'BTD', '19').
card_flavor_text('bone harvest'/'BTD', '\"Only fools believe they will face my armies but once.\"\n—Kaervek').
card_multiverse_id('bone harvest'/'BTD', '27245').

card_in_set('brainstorm', 'BTD').
card_original_type('brainstorm'/'BTD', 'Instant').
card_original_text('brainstorm'/'BTD', 'Draw three cards, then put two cards from your hand on top of your library in any order.').
card_image_name('brainstorm'/'BTD', 'brainstorm').
card_uid('brainstorm'/'BTD', 'BTD:Brainstorm:brainstorm').
card_rarity('brainstorm'/'BTD', 'Common').
card_artist('brainstorm'/'BTD', 'Christopher Rush').
card_number('brainstorm'/'BTD', '3').
card_flavor_text('brainstorm'/'BTD', '\"I reeled from the blow, and then suddenly, I knew exactly what to do. Within moments victory was mine.\"\n—Gustha Ebbasdotter\nKjeldoran royal mage').
card_multiverse_id('brainstorm'/'BTD', '27246').

card_in_set('clockwork avian', 'BTD').
card_original_type('clockwork avian'/'BTD', 'Artifact Creature').
card_original_text('clockwork avian'/'BTD', 'Clockwork Avian comes into play with four +1/+0 counters on it.\nClockwork Avian can\'t have more than four +1/+0 counters on it.\nAt end of combat, if Clockwork Avian attacked or blocked this turn, remove a +1/+0 counter from it.\n{X}, {T}: Put X +1/+0 counters on Clockwork Avian. Play this ability only during your upkeep.').
card_image_name('clockwork avian'/'BTD', 'clockwork avian').
card_uid('clockwork avian'/'BTD', 'BTD:Clockwork Avian:clockwork avian').
card_rarity('clockwork avian'/'BTD', 'Rare').
card_artist('clockwork avian'/'BTD', 'Randy Asplund-Faith').
card_number('clockwork avian'/'BTD', '69').
card_multiverse_id('clockwork avian'/'BTD', '26606').

card_in_set('clockwork beast', 'BTD').
card_original_type('clockwork beast'/'BTD', 'Artifact Creature').
card_original_text('clockwork beast'/'BTD', 'Clockwork Beast comes into play with seven +1/+0 counters on it.\nClockwork Beast can\'t have more than seven +1/+0 counters on it.\nAt end of combat, if Clockwork Beast attacked or blocked this turn, remove a +1/+0 counter from it.\n{X}, {T}: Put X +1/+0 counters on Clockwork Beast. Play this ability only during your upkeep.').
card_image_name('clockwork beast'/'BTD', 'clockwork beast').
card_uid('clockwork beast'/'BTD', 'BTD:Clockwork Beast:clockwork beast').
card_rarity('clockwork beast'/'BTD', 'Rare').
card_artist('clockwork beast'/'BTD', 'Carl Critchlow').
card_number('clockwork beast'/'BTD', '70').
card_multiverse_id('clockwork beast'/'BTD', '26617').

card_in_set('cloud djinn', 'BTD').
card_original_type('cloud djinn'/'BTD', 'Creature — Djinn').
card_original_text('cloud djinn'/'BTD', 'Flying\nCloud Djinn can block only creatures with flying.').
card_image_name('cloud djinn'/'BTD', 'cloud djinn').
card_uid('cloud djinn'/'BTD', 'BTD:Cloud Djinn:cloud djinn').
card_rarity('cloud djinn'/'BTD', 'Uncommon').
card_artist('cloud djinn'/'BTD', 'Mike Dringenberg').
card_number('cloud djinn'/'BTD', '4').
card_flavor_text('cloud djinn'/'BTD', 'As elusive as the footprint of a djinn\n—Suq\'Ata expression').
card_multiverse_id('cloud djinn'/'BTD', '26625').

card_in_set('cloud elemental', 'BTD').
card_original_type('cloud elemental'/'BTD', 'Creature — Elemental').
card_original_text('cloud elemental'/'BTD', 'Flying\nCloud Elemental can block only creatures with flying.').
card_image_name('cloud elemental'/'BTD', 'cloud elemental').
card_uid('cloud elemental'/'BTD', 'BTD:Cloud Elemental:cloud elemental').
card_rarity('cloud elemental'/'BTD', 'Common').
card_artist('cloud elemental'/'BTD', 'Adam Rex').
card_number('cloud elemental'/'BTD', '5').
card_flavor_text('cloud elemental'/'BTD', '\"You know you\'ve angered the gods when the clouds turn against you.\"\n—Sisay, Captain of the Weatherlight').
card_multiverse_id('cloud elemental'/'BTD', '27247').

card_in_set('coercion', 'BTD').
card_original_type('coercion'/'BTD', 'Sorcery').
card_original_text('coercion'/'BTD', 'Look at target opponent\'s hand and choose a card from it. That player discards that card.').
card_image_name('coercion'/'BTD', 'coercion').
card_uid('coercion'/'BTD', 'BTD:Coercion:coercion').
card_rarity('coercion'/'BTD', 'Common').
card_artist('coercion'/'BTD', 'Pete Venters').
card_number('coercion'/'BTD', '20').
card_flavor_text('coercion'/'BTD', '\"There\'s very little that escapes me, Greven. And you will escape very little if you fail.\"\n—Volrath').
card_multiverse_id('coercion'/'BTD', '27248').

card_in_set('counterspell', 'BTD').
card_original_type('counterspell'/'BTD', 'Instant').
card_original_text('counterspell'/'BTD', 'Counter target spell.').
card_image_name('counterspell'/'BTD', 'counterspell').
card_uid('counterspell'/'BTD', 'BTD:Counterspell:counterspell').
card_rarity('counterspell'/'BTD', 'Common').
card_artist('counterspell'/'BTD', 'L. A. Williams').
card_number('counterspell'/'BTD', '6').
card_multiverse_id('counterspell'/'BTD', '26644').

card_in_set('crash of rhinos', 'BTD').
card_original_type('crash of rhinos'/'BTD', 'Creature — Rhino').
card_original_text('crash of rhinos'/'BTD', 'Trample').
card_image_name('crash of rhinos'/'BTD', 'crash of rhinos').
card_uid('crash of rhinos'/'BTD', 'BTD:Crash of Rhinos:crash of rhinos').
card_rarity('crash of rhinos'/'BTD', 'Common').
card_artist('crash of rhinos'/'BTD', 'Steve White').
card_number('crash of rhinos'/'BTD', '51').
card_flavor_text('crash of rhinos'/'BTD', 'Love is like a rhino, short-sighted and hasty; if it cannot find a way, it will make a way.\n—Femeref adage').
card_multiverse_id('crash of rhinos'/'BTD', '27249').

card_in_set('crashing boars', 'BTD').
card_original_type('crashing boars'/'BTD', 'Creature — Boar').
card_original_text('crashing boars'/'BTD', 'Whenever Crashing Boars attacks, defending player chooses an untapped creature he or she controls. That creature blocks Crashing Boars this turn if able.').
card_image_name('crashing boars'/'BTD', 'crashing boars').
card_uid('crashing boars'/'BTD', 'BTD:Crashing Boars:crashing boars').
card_rarity('crashing boars'/'BTD', 'Uncommon').
card_artist('crashing boars'/'BTD', 'Ron Spencer').
card_number('crashing boars'/'BTD', '52').
card_flavor_text('crashing boars'/'BTD', 'Bores ruin a party. Boars are party to ruin.').
card_multiverse_id('crashing boars'/'BTD', '26640').

card_in_set('dark ritual', 'BTD').
card_original_type('dark ritual'/'BTD', 'Instant').
card_original_text('dark ritual'/'BTD', 'Add {B}{B}{B} to your mana pool.').
card_image_name('dark ritual'/'BTD', 'dark ritual').
card_uid('dark ritual'/'BTD', 'BTD:Dark Ritual:dark ritual').
card_rarity('dark ritual'/'BTD', 'Common').
card_artist('dark ritual'/'BTD', 'Clint Langley').
card_number('dark ritual'/'BTD', '21').
card_multiverse_id('dark ritual'/'BTD', '26632').

card_in_set('deadly insect', 'BTD').
card_original_type('deadly insect'/'BTD', 'Creature — Insect').
card_original_text('deadly insect'/'BTD', 'Deadly Insect can\'t be the target of spells or abilities.').
card_image_name('deadly insect'/'BTD', 'deadly insect').
card_uid('deadly insect'/'BTD', 'BTD:Deadly Insect:deadly insect').
card_rarity('deadly insect'/'BTD', 'Common').
card_artist('deadly insect'/'BTD', 'Randy Gallegos').
card_number('deadly insect'/'BTD', '53').
card_flavor_text('deadly insect'/'BTD', 'With his newfound sense of self-importance, Squee set his sights on ever-bigger bugs.').
card_multiverse_id('deadly insect'/'BTD', '27250').

card_in_set('death stroke', 'BTD').
card_original_type('death stroke'/'BTD', 'Sorcery').
card_original_text('death stroke'/'BTD', 'Destroy target tapped creature.').
card_image_name('death stroke'/'BTD', 'death stroke').
card_uid('death stroke'/'BTD', 'BTD:Death Stroke:death stroke').
card_rarity('death stroke'/'BTD', 'Common').
card_artist('death stroke'/'BTD', 'Colin MacNeil').
card_number('death stroke'/'BTD', '22').
card_flavor_text('death stroke'/'BTD', 'For a sharp second, Selenia froze, and Crovax\'s blade found home, As the angel shattered like glass, Crovax felt his mind collapse—the curse had been fulfilled.').
card_multiverse_id('death stroke'/'BTD', '26629').

card_in_set('diabolic edict', 'BTD').
card_original_type('diabolic edict'/'BTD', 'Instant').
card_original_text('diabolic edict'/'BTD', 'Target player sacrifices a creature.').
card_image_name('diabolic edict'/'BTD', 'diabolic edict').
card_uid('diabolic edict'/'BTD', 'BTD:Diabolic Edict:diabolic edict').
card_rarity('diabolic edict'/'BTD', 'Common').
card_artist('diabolic edict'/'BTD', 'Ron Spencer').
card_number('diabolic edict'/'BTD', '23').
card_flavor_text('diabolic edict'/'BTD', 'Greven il-Vec lifted Vhati off his feet. \"The fall will give you time to think on your failure.\"').
card_multiverse_id('diabolic edict'/'BTD', '27251').

card_in_set('diabolic vision', 'BTD').
card_original_type('diabolic vision'/'BTD', 'Sorcery').
card_original_text('diabolic vision'/'BTD', 'Look at the top five cards of your library. Put one of them into your hand and the rest on top of your library in any order.').
card_image_name('diabolic vision'/'BTD', 'diabolic vision').
card_uid('diabolic vision'/'BTD', 'BTD:Diabolic Vision:diabolic vision').
card_rarity('diabolic vision'/'BTD', 'Uncommon').
card_artist('diabolic vision'/'BTD', 'Anthony S. Waters').
card_number('diabolic vision'/'BTD', '67').
card_flavor_text('diabolic vision'/'BTD', '\"I have seen the true path. I will not warm myself by the fire—I will become the flame.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('diabolic vision'/'BTD', '26613').

card_in_set('drain life', 'BTD').
card_original_type('drain life'/'BTD', 'Sorcery').
card_original_text('drain life'/'BTD', 'Spend only black mana on {X}.\nDrain Life deals X damage to target creature or player. You gain life equal to the damage dealt, but not more life than the player\'s life total before Drain Life dealt damage or the creature\'s toughness.').
card_image_name('drain life'/'BTD', 'drain life').
card_uid('drain life'/'BTD', 'BTD:Drain Life:drain life').
card_rarity('drain life'/'BTD', 'Common').
card_artist('drain life'/'BTD', 'Andrew Robinson').
card_number('drain life'/'BTD', '24').
card_multiverse_id('drain life'/'BTD', '26618').

card_in_set('dwarven ruins', 'BTD').
card_original_type('dwarven ruins'/'BTD', 'Land').
card_original_text('dwarven ruins'/'BTD', 'Dwarven Ruins comes into play tapped.\n{T}: Add {R} to your mana pool.\n{T}, Sacrifice Dwarven Ruins: Add {R}{R} to your mana pool.').
card_image_name('dwarven ruins'/'BTD', 'dwarven ruins').
card_uid('dwarven ruins'/'BTD', 'BTD:Dwarven Ruins:dwarven ruins').
card_rarity('dwarven ruins'/'BTD', 'Uncommon').
card_artist('dwarven ruins'/'BTD', 'Liz Danforth').
card_number('dwarven ruins'/'BTD', '71').
card_multiverse_id('dwarven ruins'/'BTD', '26651').

card_in_set('ebon stronghold', 'BTD').
card_original_type('ebon stronghold'/'BTD', 'Land').
card_original_text('ebon stronghold'/'BTD', 'Ebon Stronghold comes into play tapped.\n{T}: Add {B} to your mana pool.\n{T}, Sacrifice Ebon Stronghold: Add {B}{B} to your mana pool.').
card_image_name('ebon stronghold'/'BTD', 'ebon stronghold').
card_uid('ebon stronghold'/'BTD', 'BTD:Ebon Stronghold:ebon stronghold').
card_rarity('ebon stronghold'/'BTD', 'Uncommon').
card_artist('ebon stronghold'/'BTD', 'Liz Danforth').
card_number('ebon stronghold'/'BTD', '72').
card_multiverse_id('ebon stronghold'/'BTD', '26650').

card_in_set('erhnam djinn', 'BTD').
card_original_type('erhnam djinn'/'BTD', 'Creature — Djinn').
card_original_text('erhnam djinn'/'BTD', 'At the beginning of your upkeep, target non—Wall creature an opponent controls gains forestwalk until your next upkeep. (The creature is unblockable as long as defending player controls a forest.)').
card_image_name('erhnam djinn'/'BTD', 'erhnam djinn').
card_uid('erhnam djinn'/'BTD', 'BTD:Erhnam Djinn:erhnam djinn').
card_rarity('erhnam djinn'/'BTD', 'Uncommon').
card_artist('erhnam djinn'/'BTD', 'Dave Dorman').
card_number('erhnam djinn'/'BTD', '54').
card_multiverse_id('erhnam djinn'/'BTD', '27227').

card_in_set('fallen angel', 'BTD').
card_original_type('fallen angel'/'BTD', 'Creature — Angel').
card_original_text('fallen angel'/'BTD', 'Flying\nSacrifice a creature: Fallen Angel gets +2/+1 until end of turn.').
card_image_name('fallen angel'/'BTD', 'fallen angel').
card_uid('fallen angel'/'BTD', 'BTD:Fallen Angel:fallen angel').
card_rarity('fallen angel'/'BTD', 'Rare').
card_artist('fallen angel'/'BTD', 'Anson Maddocks').
card_number('fallen angel'/'BTD', '25').
card_flavor_text('fallen angel'/'BTD', '\"Angels are simply extensions of truth upon the fabric of life—and there is far more dark than light.\" —Baron Sengir').
card_multiverse_id('fallen angel'/'BTD', '26660').

card_in_set('feral shadow', 'BTD').
card_original_type('feral shadow'/'BTD', 'Creature — Nightstalker').
card_original_text('feral shadow'/'BTD', 'Flying').
card_image_name('feral shadow'/'BTD', 'feral shadow').
card_uid('feral shadow'/'BTD', 'BTD:Feral Shadow:feral shadow').
card_rarity('feral shadow'/'BTD', 'Common').
card_artist('feral shadow'/'BTD', 'Cliff Nielsen').
card_number('feral shadow'/'BTD', '26').
card_flavor_text('feral shadow'/'BTD', '\"The nightstalkers are such a minor power in Urborg. Their sad eagerness to elevate themselves makes them willing tools in the plundering of little Femeref.\"\n—Kaervek').
card_multiverse_id('feral shadow'/'BTD', '27564').

card_in_set('fireball', 'BTD').
card_original_type('fireball'/'BTD', 'Sorcery').
card_original_text('fireball'/'BTD', 'Fireball deals X damage divided evenly, rounded down, among Y plus one target creatures and/or players.').
card_image_name('fireball'/'BTD', 'fireball').
card_uid('fireball'/'BTD', 'BTD:Fireball:fireball').
card_rarity('fireball'/'BTD', 'Common').
card_artist('fireball'/'BTD', 'Mark Tedin').
card_number('fireball'/'BTD', '37').
card_multiverse_id('fireball'/'BTD', '26621').

card_in_set('fog', 'BTD').
card_original_type('fog'/'BTD', 'Instant').
card_original_text('fog'/'BTD', 'Prevent all combat damage that would be dealt this turn.').
card_image_name('fog'/'BTD', 'fog').
card_uid('fog'/'BTD', 'BTD:Fog:fog').
card_rarity('fog'/'BTD', 'Common').
card_artist('fog'/'BTD', 'Harold McNeill').
card_number('fog'/'BTD', '55').
card_multiverse_id('fog'/'BTD', '26648').

card_in_set('fog elemental', 'BTD').
card_original_type('fog elemental'/'BTD', 'Creature — Elemental').
card_original_text('fog elemental'/'BTD', 'Flying\nWhen Fog Elemental attacks or blocks, sacrifice it at end of combat.').
card_image_name('fog elemental'/'BTD', 'fog elemental').
card_uid('fog elemental'/'BTD', 'BTD:Fog Elemental:fog elemental').
card_rarity('fog elemental'/'BTD', 'Common').
card_artist('fog elemental'/'BTD', 'Jon J. Muth').
card_number('fog elemental'/'BTD', '7').
card_flavor_text('fog elemental'/'BTD', '\"I\'ve seen fog so thick you could cut it, but none that could cut me.\"\n—Tahngarth of the Weatherlight').
card_multiverse_id('fog elemental'/'BTD', '26657').

card_in_set('force of nature', 'BTD').
card_original_type('force of nature'/'BTD', 'Creature — Force').
card_original_text('force of nature'/'BTD', 'Trample\nAt the beginning of your upkeep, Force of Nature deals 8 damage to you unless you pay {G}{G}{G}{G}.').
card_image_name('force of nature'/'BTD', 'force of nature').
card_uid('force of nature'/'BTD', 'BTD:Force of Nature:force of nature').
card_rarity('force of nature'/'BTD', 'Rare').
card_artist('force of nature'/'BTD', 'Douglas Shuler').
card_number('force of nature'/'BTD', '56').
card_multiverse_id('force of nature'/'BTD', '26609').

card_in_set('forest', 'BTD').
card_original_type('forest'/'BTD', 'Land').
card_original_text('forest'/'BTD', 'G').
card_image_name('forest'/'BTD', 'forest1').
card_uid('forest'/'BTD', 'BTD:Forest:forest1').
card_rarity('forest'/'BTD', 'Basic Land').
card_artist('forest'/'BTD', 'Douglas Shuler').
card_number('forest'/'BTD', '88').
card_multiverse_id('forest'/'BTD', '27242').

card_in_set('forest', 'BTD').
card_original_type('forest'/'BTD', 'Land').
card_original_text('forest'/'BTD', 'G').
card_image_name('forest'/'BTD', 'forest2').
card_uid('forest'/'BTD', 'BTD:Forest:forest2').
card_rarity('forest'/'BTD', 'Basic Land').
card_artist('forest'/'BTD', 'John Avon').
card_number('forest'/'BTD', '89').
card_multiverse_id('forest'/'BTD', '27243').

card_in_set('forest', 'BTD').
card_original_type('forest'/'BTD', 'Land').
card_original_text('forest'/'BTD', 'G').
card_image_name('forest'/'BTD', 'forest3').
card_uid('forest'/'BTD', 'BTD:Forest:forest3').
card_rarity('forest'/'BTD', 'Basic Land').
card_artist('forest'/'BTD', 'John Avon').
card_number('forest'/'BTD', '90').
card_multiverse_id('forest'/'BTD', '27244').

card_in_set('gaseous form', 'BTD').
card_original_type('gaseous form'/'BTD', 'Enchant Creature').
card_original_text('gaseous form'/'BTD', 'Prevent all combat damage that would be dealt to and dealt by enchanted creature.').
card_image_name('gaseous form'/'BTD', 'gaseous form').
card_uid('gaseous form'/'BTD', 'BTD:Gaseous Form:gaseous form').
card_rarity('gaseous form'/'BTD', 'Common').
card_artist('gaseous form'/'BTD', 'Roger Raupp').
card_number('gaseous form'/'BTD', '8').
card_flavor_text('gaseous form'/'BTD', '\"It\'s hard to hurt anyone when you\'re as transparent as your motives.\"\n—Ertai, wizard adept').
card_multiverse_id('gaseous form'/'BTD', '26641').

card_in_set('giant crab', 'BTD').
card_original_type('giant crab'/'BTD', 'Creature — Crab').
card_original_text('giant crab'/'BTD', '{U} Giant Crab can\'t be the target of spells or abilities this turn.').
card_image_name('giant crab'/'BTD', 'giant crab').
card_uid('giant crab'/'BTD', 'BTD:Giant Crab:giant crab').
card_rarity('giant crab'/'BTD', 'Common').
card_artist('giant crab'/'BTD', 'Tom Kyffin').
card_number('giant crab'/'BTD', '9').
card_flavor_text('giant crab'/'BTD', 'During the giant crabs\' mating season, Skyshroud nights are filled with the clatter of their skirmishes.').
card_multiverse_id('giant crab'/'BTD', '26627').

card_in_set('giant growth', 'BTD').
card_original_type('giant growth'/'BTD', 'Instant').
card_original_text('giant growth'/'BTD', 'Target creature gets +3/+3 until end of turn.').
card_image_name('giant growth'/'BTD', 'giant growth').
card_uid('giant growth'/'BTD', 'BTD:Giant Growth:giant growth').
card_rarity('giant growth'/'BTD', 'Common').
card_artist('giant growth'/'BTD', 'DiTerlizzi').
card_number('giant growth'/'BTD', '57').
card_multiverse_id('giant growth'/'BTD', '26649').

card_in_set('gravedigger', 'BTD').
card_original_type('gravedigger'/'BTD', 'Creature — Zombie').
card_original_text('gravedigger'/'BTD', 'When Gravedigger comes into play, you may return target creature card from your graveyard to your hand.').
card_image_name('gravedigger'/'BTD', 'gravedigger').
card_uid('gravedigger'/'BTD', 'BTD:Gravedigger:gravedigger').
card_rarity('gravedigger'/'BTD', 'Common').
card_artist('gravedigger'/'BTD', 'Dermot Power').
card_number('gravedigger'/'BTD', '27').
card_flavor_text('gravedigger'/'BTD', 'A full coffin is like a full coffer—both are attractive to thieves.').
card_multiverse_id('gravedigger'/'BTD', '27253').

card_in_set('havenwood battleground', 'BTD').
card_original_type('havenwood battleground'/'BTD', 'Land').
card_original_text('havenwood battleground'/'BTD', 'Havenwood Battleground comes into play tapped.\n{T}: Add {G} to your mana pool.\n{T}, Sacrifice Havenwood Battleground: Add {G}{G} to your mana pool.').
card_image_name('havenwood battleground'/'BTD', 'havenwood battleground').
card_uid('havenwood battleground'/'BTD', 'BTD:Havenwood Battleground:havenwood battleground').
card_rarity('havenwood battleground'/'BTD', 'Uncommon').
card_artist('havenwood battleground'/'BTD', 'Liz Danforth').
card_number('havenwood battleground'/'BTD', '73').
card_multiverse_id('havenwood battleground'/'BTD', '26652').

card_in_set('hollow dogs', 'BTD').
card_original_type('hollow dogs'/'BTD', 'Creature — Hound').
card_original_text('hollow dogs'/'BTD', 'Whenever Hollow Dogs attacks, it gets +2/+0 until end of turn.').
card_image_name('hollow dogs'/'BTD', 'hollow dogs').
card_uid('hollow dogs'/'BTD', 'BTD:Hollow Dogs:hollow dogs').
card_rarity('hollow dogs'/'BTD', 'Common').
card_artist('hollow dogs'/'BTD', 'Jeff Miracola').
card_number('hollow dogs'/'BTD', '28').
card_flavor_text('hollow dogs'/'BTD', 'A hollow dog is never empty. It is filled with thirst for the hunt.').
card_multiverse_id('hollow dogs'/'BTD', '27254').

card_in_set('hulking cyclops', 'BTD').
card_original_type('hulking cyclops'/'BTD', 'Creature — Giant').
card_original_text('hulking cyclops'/'BTD', 'Hulking Cyclops can\'t block.').
card_image_name('hulking cyclops'/'BTD', 'hulking cyclops').
card_uid('hulking cyclops'/'BTD', 'BTD:Hulking Cyclops:hulking cyclops').
card_rarity('hulking cyclops'/'BTD', 'Uncommon').
card_artist('hulking cyclops'/'BTD', 'Paolo Parente').
card_number('hulking cyclops'/'BTD', '38').
card_flavor_text('hulking cyclops'/'BTD', 'Anyone can get around a cyclops, but few can stand in its way.').
card_multiverse_id('hulking cyclops'/'BTD', '26624').

card_in_set('impulse', 'BTD').
card_original_type('impulse'/'BTD', 'Instant').
card_original_text('impulse'/'BTD', 'Look at the top four cards of your library. Put one of them into your hand and the rest on the bottom of your library.').
card_image_name('impulse'/'BTD', 'impulse').
card_uid('impulse'/'BTD', 'BTD:Impulse:impulse').
card_rarity('impulse'/'BTD', 'Common').
card_artist('impulse'/'BTD', 'Bryan Talbot').
card_number('impulse'/'BTD', '10').
card_flavor_text('impulse'/'BTD', '\"Controlling time ensures you need never look impulsive again.\"\n—Teferi').
card_multiverse_id('impulse'/'BTD', '26616').

card_in_set('island', 'BTD').
card_original_type('island'/'BTD', 'Land').
card_original_text('island'/'BTD', 'U').
card_image_name('island'/'BTD', 'island1').
card_uid('island'/'BTD', 'BTD:Island:island1').
card_rarity('island'/'BTD', 'Basic Land').
card_artist('island'/'BTD', 'Tony Szczudlo').
card_number('island'/'BTD', '79').
card_multiverse_id('island'/'BTD', '27236').

card_in_set('island', 'BTD').
card_original_type('island'/'BTD', 'Land').
card_original_text('island'/'BTD', 'U').
card_image_name('island'/'BTD', 'island2').
card_uid('island'/'BTD', 'BTD:Island:island2').
card_rarity('island'/'BTD', 'Basic Land').
card_artist('island'/'BTD', 'John Avon').
card_number('island'/'BTD', '80').
card_multiverse_id('island'/'BTD', '27237').

card_in_set('island', 'BTD').
card_original_type('island'/'BTD', 'Land').
card_original_text('island'/'BTD', 'U').
card_image_name('island'/'BTD', 'island3').
card_uid('island'/'BTD', 'BTD:Island:island3').
card_rarity('island'/'BTD', 'Basic Land').
card_artist('island'/'BTD', 'Mark Poole').
card_number('island'/'BTD', '81').
card_multiverse_id('island'/'BTD', '27238').

card_in_set('killer whale', 'BTD').
card_original_type('killer whale'/'BTD', 'Creature — Whale').
card_original_text('killer whale'/'BTD', '{U} Killer Whale gains flying until end of turn.').
card_image_name('killer whale'/'BTD', 'killer whale').
card_uid('killer whale'/'BTD', 'BTD:Killer Whale:killer whale').
card_rarity('killer whale'/'BTD', 'Uncommon').
card_artist('killer whale'/'BTD', 'Stephen Daniele').
card_number('killer whale'/'BTD', '11').
card_flavor_text('killer whale'/'BTD', 'Hunger is like the sea: deep, endless, and unforgiving.').
card_multiverse_id('killer whale'/'BTD', '26638').

card_in_set('kird ape', 'BTD').
card_original_type('kird ape'/'BTD', 'Creature — Ape').
card_original_text('kird ape'/'BTD', 'Kird Ape gets +1/+2 as long as you control a forest.').
card_image_name('kird ape'/'BTD', 'kird ape').
card_uid('kird ape'/'BTD', 'BTD:Kird Ape:kird ape').
card_rarity('kird ape'/'BTD', 'Common').
card_artist('kird ape'/'BTD', 'Ken Meyer, Jr.').
card_number('kird ape'/'BTD', '39').
card_multiverse_id('kird ape'/'BTD', '26605').

card_in_set('lava axe', 'BTD').
card_original_type('lava axe'/'BTD', 'Sorcery').
card_original_text('lava axe'/'BTD', 'Lava Axe deals 5 damage to target player.').
card_image_name('lava axe'/'BTD', 'lava axe').
card_uid('lava axe'/'BTD', 'BTD:Lava Axe:lava axe').
card_rarity('lava axe'/'BTD', 'Common').
card_artist('lava axe'/'BTD', 'Brian Snõddy').
card_number('lava axe'/'BTD', '40').
card_flavor_text('lava axe'/'BTD', '\"Catch!\"').
card_multiverse_id('lava axe'/'BTD', '26643').

card_in_set('leviathan', 'BTD').
card_original_type('leviathan'/'BTD', 'Creature — Leviathan').
card_original_text('leviathan'/'BTD', 'Trample\nLeviathan comes into play tapped and doesn\'t untap during your untap step.\nAt the beginning of your upkeep, you may sacrifice two islands. If you do, untap Leviathan.\nLeviathan can\'t attack unless you sacrifice two islands.').
card_image_name('leviathan'/'BTD', 'leviathan').
card_uid('leviathan'/'BTD', 'BTD:Leviathan:leviathan').
card_rarity('leviathan'/'BTD', 'Rare').
card_artist('leviathan'/'BTD', 'Mark Tedin').
card_number('leviathan'/'BTD', '12').
card_multiverse_id('leviathan'/'BTD', '26619').

card_in_set('lightning bolt', 'BTD').
card_original_type('lightning bolt'/'BTD', 'Instant').
card_original_text('lightning bolt'/'BTD', 'Lightning Bolt deals 3 damage to target creature or player.').
card_image_name('lightning bolt'/'BTD', 'lightning bolt').
card_uid('lightning bolt'/'BTD', 'BTD:Lightning Bolt:lightning bolt').
card_rarity('lightning bolt'/'BTD', 'Common').
card_artist('lightning bolt'/'BTD', 'Christopher Rush').
card_number('lightning bolt'/'BTD', '41').
card_multiverse_id('lightning bolt'/'BTD', '27255').

card_in_set('llanowar elves', 'BTD').
card_original_type('llanowar elves'/'BTD', 'Creature — Elf').
card_original_text('llanowar elves'/'BTD', '{T}: Add {G} to your mana pool.').
card_image_name('llanowar elves'/'BTD', 'llanowar elves').
card_uid('llanowar elves'/'BTD', 'BTD:Llanowar Elves:llanowar elves').
card_rarity('llanowar elves'/'BTD', 'Common').
card_artist('llanowar elves'/'BTD', 'Anson Maddocks').
card_number('llanowar elves'/'BTD', '58').
card_flavor_text('llanowar elves'/'BTD', 'One bone broken for every twig snapped underfoot.\n—Llanowar penalty for trespassing').
card_multiverse_id('llanowar elves'/'BTD', '27256').

card_in_set('lowland giant', 'BTD').
card_original_type('lowland giant'/'BTD', 'Creature — Giant').
card_original_text('lowland giant'/'BTD', '').
card_image_name('lowland giant'/'BTD', 'lowland giant').
card_uid('lowland giant'/'BTD', 'BTD:Lowland Giant:lowland giant').
card_rarity('lowland giant'/'BTD', 'Common').
card_artist('lowland giant'/'BTD', 'Paolo Parente').
card_number('lowland giant'/'BTD', '42').
card_flavor_text('lowland giant'/'BTD', '\"Faugh!\" snorted Tahngarth. \"Why would it make a meal of something like you?\" Squee looked relieved. \"No,\" he continued, \"you\'d make a much better toothpick.\"').
card_multiverse_id('lowland giant'/'BTD', '27257').

card_in_set('mahamoti djinn', 'BTD').
card_original_type('mahamoti djinn'/'BTD', 'Creature — Djinn').
card_original_text('mahamoti djinn'/'BTD', 'Flying').
card_image_name('mahamoti djinn'/'BTD', 'mahamoti djinn').
card_uid('mahamoti djinn'/'BTD', 'BTD:Mahamoti Djinn:mahamoti djinn').
card_rarity('mahamoti djinn'/'BTD', 'Rare').
card_artist('mahamoti djinn'/'BTD', 'Dan Frazier').
card_number('mahamoti djinn'/'BTD', '13').
card_flavor_text('mahamoti djinn'/'BTD', 'Of royal blood among the spirits of the air, the Mahamoti djinn rides on the wings of the winds. As dangerous in the gambling hall as he is in battle, he is a master of trickery and misdirection.').
card_multiverse_id('mahamoti djinn'/'BTD', '26608').

card_in_set('mountain', 'BTD').
card_original_type('mountain'/'BTD', 'Land').
card_original_text('mountain'/'BTD', 'R').
card_image_name('mountain'/'BTD', 'mountain1').
card_uid('mountain'/'BTD', 'BTD:Mountain:mountain1').
card_rarity('mountain'/'BTD', 'Basic Land').
card_artist('mountain'/'BTD', 'John Avon').
card_number('mountain'/'BTD', '85').
card_multiverse_id('mountain'/'BTD', '27239').

card_in_set('mountain', 'BTD').
card_original_type('mountain'/'BTD', 'Land').
card_original_text('mountain'/'BTD', 'R').
card_image_name('mountain'/'BTD', 'mountain2').
card_uid('mountain'/'BTD', 'BTD:Mountain:mountain2').
card_rarity('mountain'/'BTD', 'Basic Land').
card_artist('mountain'/'BTD', 'Douglas Shuler').
card_number('mountain'/'BTD', '86').
card_multiverse_id('mountain'/'BTD', '27240').

card_in_set('mountain', 'BTD').
card_original_type('mountain'/'BTD', 'Land').
card_original_text('mountain'/'BTD', 'R').
card_image_name('mountain'/'BTD', 'mountain3').
card_uid('mountain'/'BTD', 'BTD:Mountain:mountain3').
card_rarity('mountain'/'BTD', 'Basic Land').
card_artist('mountain'/'BTD', 'Rob Alexander').
card_number('mountain'/'BTD', '87').
card_multiverse_id('mountain'/'BTD', '27241').

card_in_set('plated spider', 'BTD').
card_original_type('plated spider'/'BTD', 'Creature — Spider').
card_original_text('plated spider'/'BTD', 'Plated Spider may block as though it had flying.').
card_image_name('plated spider'/'BTD', 'plated spider').
card_uid('plated spider'/'BTD', 'BTD:Plated Spider:plated spider').
card_rarity('plated spider'/'BTD', 'Common').
card_artist('plated spider'/'BTD', 'Ron Spencer').
card_number('plated spider'/'BTD', '59').
card_flavor_text('plated spider'/'BTD', 'Most spiders wait patiently for their prey to arrive. Most spiders aren\'t forty feet tall.').
card_multiverse_id('plated spider'/'BTD', '26661').

card_in_set('polluted mire', 'BTD').
card_original_type('polluted mire'/'BTD', 'Land').
card_original_text('polluted mire'/'BTD', 'Cycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability any time you could play an instant.)\nPolluted Mire comes into play tapped.\n{T}: Add {B} to your mana pool.').
card_image_name('polluted mire'/'BTD', 'polluted mire').
card_uid('polluted mire'/'BTD', 'BTD:Polluted Mire:polluted mire').
card_rarity('polluted mire'/'BTD', 'Common').
card_artist('polluted mire'/'BTD', 'Stephen Daniele').
card_number('polluted mire'/'BTD', '74').
card_multiverse_id('polluted mire'/'BTD', '26637').

card_in_set('power sink', 'BTD').
card_original_type('power sink'/'BTD', 'Instant').
card_original_text('power sink'/'BTD', 'Counter target spell unless its controller pays {X}. If he or she doesn\'t, that player taps all lands he or she controls and empties his or her mana pool.').
card_image_name('power sink'/'BTD', 'power sink').
card_uid('power sink'/'BTD', 'BTD:Power Sink:power sink').
card_rarity('power sink'/'BTD', 'Common').
card_artist('power sink'/'BTD', 'Mark Poole').
card_number('power sink'/'BTD', '14').
card_multiverse_id('power sink'/'BTD', '26658').

card_in_set('quirion elves', 'BTD').
card_original_type('quirion elves'/'BTD', 'Creature — Elf').
card_original_text('quirion elves'/'BTD', 'As Quirion Elves comes into play, choose a color.\n{T}: Add {G} to your mana pool.\n{T}: Add one mana of the chosen color to your mana pool.').
card_image_name('quirion elves'/'BTD', 'quirion elves').
card_uid('quirion elves'/'BTD', 'BTD:Quirion Elves:quirion elves').
card_rarity('quirion elves'/'BTD', 'Common').
card_artist('quirion elves'/'BTD', 'Randy Gallegos').
card_number('quirion elves'/'BTD', '60').
card_multiverse_id('quirion elves'/'BTD', '26614').

card_in_set('raging goblin', 'BTD').
card_original_type('raging goblin'/'BTD', 'Creature — Goblin').
card_original_text('raging goblin'/'BTD', 'Haste (This creature may attack and {T} the turn it comes under your control.)').
card_image_name('raging goblin'/'BTD', 'raging goblin').
card_uid('raging goblin'/'BTD', 'BTD:Raging Goblin:raging goblin').
card_rarity('raging goblin'/'BTD', 'Common').
card_artist('raging goblin'/'BTD', 'Jeff Miracola').
card_number('raging goblin'/'BTD', '43').
card_flavor_text('raging goblin'/'BTD', 'He raged at the world, at his family, at his life. But mostly he just raged.').
card_multiverse_id('raging goblin'/'BTD', '27258').

card_in_set('rampant growth', 'BTD').
card_original_type('rampant growth'/'BTD', 'Sorcery').
card_original_text('rampant growth'/'BTD', 'Search your library for a basic land card and put that card into play tapped. Then shuffle your library.').
card_image_name('rampant growth'/'BTD', 'rampant growth').
card_uid('rampant growth'/'BTD', 'BTD:Rampant Growth:rampant growth').
card_rarity('rampant growth'/'BTD', 'Common').
card_artist('rampant growth'/'BTD', 'Tom Kyffin').
card_number('rampant growth'/'BTD', '61').
card_flavor_text('rampant growth'/'BTD', 'Rath forces plants from its surface as one presses whey from cheese.').
card_multiverse_id('rampant growth'/'BTD', '27259').

card_in_set('remote isle', 'BTD').
card_original_type('remote isle'/'BTD', 'Land').
card_original_text('remote isle'/'BTD', 'Cycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability any time you could play an instant.)\nRemote Isle comes into play tapped.\n{T}: Add {U} to your mana pool.').
card_image_name('remote isle'/'BTD', 'remote isle').
card_uid('remote isle'/'BTD', 'BTD:Remote Isle:remote isle').
card_rarity('remote isle'/'BTD', 'Common').
card_artist('remote isle'/'BTD', 'Ciruelo').
card_number('remote isle'/'BTD', '75').
card_multiverse_id('remote isle'/'BTD', '26636').

card_in_set('scaled wurm', 'BTD').
card_original_type('scaled wurm'/'BTD', 'Creature — Wurm').
card_original_text('scaled wurm'/'BTD', '').
card_image_name('scaled wurm'/'BTD', 'scaled wurm').
card_uid('scaled wurm'/'BTD', 'BTD:Scaled Wurm:scaled wurm').
card_rarity('scaled wurm'/'BTD', 'Common').
card_artist('scaled wurm'/'BTD', 'Daniel Gelon').
card_number('scaled wurm'/'BTD', '62').
card_flavor_text('scaled wurm'/'BTD', '\"Flourishing during the Ice Age, these wurms were the bane of all Kjeldorans. Their great size and ferocity made them the subject of countless nightmares—they embodied the worst of the Ice Age.\"\n—Kjeldor: Ice Civilization').
card_multiverse_id('scaled wurm'/'BTD', '27260').

card_in_set('segmented wurm', 'BTD').
card_original_type('segmented wurm'/'BTD', 'Creature — Wurm').
card_original_text('segmented wurm'/'BTD', 'Whenever Segmented Wurm becomes the target of a spell or ability, put a -1/-1 counter on it.').
card_image_name('segmented wurm'/'BTD', 'segmented wurm').
card_uid('segmented wurm'/'BTD', 'BTD:Segmented Wurm:segmented wurm').
card_rarity('segmented wurm'/'BTD', 'Uncommon').
card_artist('segmented wurm'/'BTD', 'Jeff Miracola').
card_number('segmented wurm'/'BTD', '68').
card_flavor_text('segmented wurm'/'BTD', '\"If only we could so easily leave behind those parts of ourselves that pain us.\"\n—Crovax').
card_multiverse_id('segmented wurm'/'BTD', '26628').

card_in_set('sengir vampire', 'BTD').
card_original_type('sengir vampire'/'BTD', 'Creature — Vampire').
card_original_text('sengir vampire'/'BTD', 'Flying\nWhenever a creature dealt damage by Sengir Vampire this turn is put into a graveyard, put a +1/+1 counter on Sengir Vampire.').
card_image_name('sengir vampire'/'BTD', 'sengir vampire').
card_uid('sengir vampire'/'BTD', 'BTD:Sengir Vampire:sengir vampire').
card_rarity('sengir vampire'/'BTD', 'Uncommon').
card_artist('sengir vampire'/'BTD', 'Jeff Easley').
card_number('sengir vampire'/'BTD', '29').
card_multiverse_id('sengir vampire'/'BTD', '26607').

card_in_set('shambling strider', 'BTD').
card_original_type('shambling strider'/'BTD', 'Creature — Strider').
card_original_text('shambling strider'/'BTD', '{R}{G} Shambling Strider gets +1/-1 until end of turn.').
card_image_name('shambling strider'/'BTD', 'shambling strider').
card_uid('shambling strider'/'BTD', 'BTD:Shambling Strider:shambling strider').
card_rarity('shambling strider'/'BTD', 'Common').
card_artist('shambling strider'/'BTD', 'Douglas Shuler').
card_number('shambling strider'/'BTD', '63').
card_flavor_text('shambling strider'/'BTD', 'Freyalise forbid that any stranger should wander into the striders\' territory.').
card_multiverse_id('shambling strider'/'BTD', '26611').

card_in_set('shivan dragon', 'BTD').
card_original_type('shivan dragon'/'BTD', 'Creature — Dragon').
card_original_text('shivan dragon'/'BTD', 'Flying\n{R} Shivan Dragon gets +1/+0 until end of turn.').
card_image_name('shivan dragon'/'BTD', 'shivan dragon').
card_uid('shivan dragon'/'BTD', 'BTD:Shivan Dragon:shivan dragon').
card_rarity('shivan dragon'/'BTD', 'Rare').
card_artist('shivan dragon'/'BTD', 'Melissa A. Benson').
card_number('shivan dragon'/'BTD', '44').
card_flavor_text('shivan dragon'/'BTD', 'While it\'s true most dragons are cruel, the Shivan dragon seems to take particular glee in the misery of others, often tormenting its victims much like a cat plays with a mouse before delivering the final blow.').
card_multiverse_id('shivan dragon'/'BTD', '26622').

card_in_set('shock', 'BTD').
card_original_type('shock'/'BTD', 'Instant').
card_original_text('shock'/'BTD', 'Shock deals 2 damage to target creature or player.').
card_image_name('shock'/'BTD', 'shock').
card_uid('shock'/'BTD', 'BTD:Shock:shock').
card_rarity('shock'/'BTD', 'Common').
card_artist('shock'/'BTD', 'Randy Gallegos').
card_number('shock'/'BTD', '45').
card_flavor_text('shock'/'BTD', 'Lightning tethers souls to the world.\n—Kor saying').
card_multiverse_id('shock'/'BTD', '26645').

card_in_set('skittering horror', 'BTD').
card_original_type('skittering horror'/'BTD', 'Creature — Horror').
card_original_text('skittering horror'/'BTD', 'When you play a creature spell, sacrifice Skittering Horror.').
card_image_name('skittering horror'/'BTD', 'skittering horror').
card_uid('skittering horror'/'BTD', 'BTD:Skittering Horror:skittering horror').
card_rarity('skittering horror'/'BTD', 'Common').
card_artist('skittering horror'/'BTD', 'Mark Zug').
card_number('skittering horror'/'BTD', '30').
card_flavor_text('skittering horror'/'BTD', '\"This monstrosity will do—for now.\"\n—Davvol, Evincar of Rath').
card_multiverse_id('skittering horror'/'BTD', '26656').

card_in_set('skittering skirge', 'BTD').
card_original_type('skittering skirge'/'BTD', 'Creature — Imp').
card_original_text('skittering skirge'/'BTD', 'Flying\nWhen you play a creature spell, sacrifice Skittering Skirge.').
card_image_name('skittering skirge'/'BTD', 'skittering skirge').
card_uid('skittering skirge'/'BTD', 'BTD:Skittering Skirge:skittering skirge').
card_rarity('skittering skirge'/'BTD', 'Common').
card_artist('skittering skirge'/'BTD', 'Ron Spencer').
card_number('skittering skirge'/'BTD', '31').
card_flavor_text('skittering skirge'/'BTD', 'The imps\' warbling cries echo through Phyrexia\'s towers like those of mourning doves in a cathedral.').
card_multiverse_id('skittering skirge'/'BTD', '27261').

card_in_set('slippery karst', 'BTD').
card_original_type('slippery karst'/'BTD', 'Land').
card_original_text('slippery karst'/'BTD', 'Cycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability any time you could play an instant.)\nSlippery Karst comes into play tapped.\n{T}: Add {G} to your mana pool.').
card_image_name('slippery karst'/'BTD', 'slippery karst').
card_uid('slippery karst'/'BTD', 'BTD:Slippery Karst:slippery karst').
card_rarity('slippery karst'/'BTD', 'Common').
card_artist('slippery karst'/'BTD', 'Stephen Daniele').
card_number('slippery karst'/'BTD', '76').
card_multiverse_id('slippery karst'/'BTD', '26634').

card_in_set('smoldering crater', 'BTD').
card_original_type('smoldering crater'/'BTD', 'Land').
card_original_text('smoldering crater'/'BTD', 'Cycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability any time you could play an instant.)\nSmoldering Crater comes into play tapped.\n{T}: Add {R} to your mana pool.').
card_image_name('smoldering crater'/'BTD', 'smoldering crater').
card_uid('smoldering crater'/'BTD', 'BTD:Smoldering Crater:smoldering crater').
card_rarity('smoldering crater'/'BTD', 'Common').
card_artist('smoldering crater'/'BTD', 'Mark Tedin').
card_number('smoldering crater'/'BTD', '77').
card_multiverse_id('smoldering crater'/'BTD', '26635').

card_in_set('snapping drake', 'BTD').
card_original_type('snapping drake'/'BTD', 'Creature — Drake').
card_original_text('snapping drake'/'BTD', 'Flying').
card_image_name('snapping drake'/'BTD', 'snapping drake').
card_uid('snapping drake'/'BTD', 'BTD:Snapping Drake:snapping drake').
card_rarity('snapping drake'/'BTD', 'Common').
card_artist('snapping drake'/'BTD', 'Christopher Rush').
card_number('snapping drake'/'BTD', '15').
card_flavor_text('snapping drake'/'BTD', 'Drakes claim to be dragons—until the dragons show up.').
card_multiverse_id('snapping drake'/'BTD', '27262').

card_in_set('sonic burst', 'BTD').
card_original_type('sonic burst'/'BTD', 'Instant').
card_original_text('sonic burst'/'BTD', 'As an additional cost to play Sonic Burst, discard a card at random from your hand.\nSonic Burst deals 4 damage to target creature or player.').
card_image_name('sonic burst'/'BTD', 'sonic burst').
card_uid('sonic burst'/'BTD', 'BTD:Sonic Burst:sonic burst').
card_rarity('sonic burst'/'BTD', 'Common').
card_artist('sonic burst'/'BTD', 'Brian Snõddy').
card_number('sonic burst'/'BTD', '46').
card_flavor_text('sonic burst'/'BTD', 'Music scythes the savage beast.').
card_multiverse_id('sonic burst'/'BTD', '26639').

card_in_set('svyelunite temple', 'BTD').
card_original_type('svyelunite temple'/'BTD', 'Land').
card_original_text('svyelunite temple'/'BTD', 'Svyelunite Temple comes into play tapped.\n{T}: Add {U} to your mana pool.\n{T}, Sacrifice Svyelunite Temple: Add {U}{U} to your mana pool.').
card_image_name('svyelunite temple'/'BTD', 'svyelunite temple').
card_uid('svyelunite temple'/'BTD', 'BTD:Svyelunite Temple:svyelunite temple').
card_rarity('svyelunite temple'/'BTD', 'Uncommon').
card_artist('svyelunite temple'/'BTD', 'Liz Danforth').
card_number('svyelunite temple'/'BTD', '78').
card_multiverse_id('svyelunite temple'/'BTD', '26623').

card_in_set('swamp', 'BTD').
card_original_type('swamp'/'BTD', 'Land').
card_original_text('swamp'/'BTD', 'B').
card_image_name('swamp'/'BTD', 'swamp1').
card_uid('swamp'/'BTD', 'BTD:Swamp:swamp1').
card_rarity('swamp'/'BTD', 'Basic Land').
card_artist('swamp'/'BTD', 'John Avon').
card_number('swamp'/'BTD', '82').
card_multiverse_id('swamp'/'BTD', '27233').

card_in_set('swamp', 'BTD').
card_original_type('swamp'/'BTD', 'Land').
card_original_text('swamp'/'BTD', 'B').
card_image_name('swamp'/'BTD', 'swamp2').
card_uid('swamp'/'BTD', 'BTD:Swamp:swamp2').
card_rarity('swamp'/'BTD', 'Basic Land').
card_artist('swamp'/'BTD', 'Romas Kukalis').
card_number('swamp'/'BTD', '83').
card_multiverse_id('swamp'/'BTD', '27234').

card_in_set('swamp', 'BTD').
card_original_type('swamp'/'BTD', 'Land').
card_original_text('swamp'/'BTD', 'B').
card_image_name('swamp'/'BTD', 'swamp3').
card_uid('swamp'/'BTD', 'BTD:Swamp:swamp3').
card_rarity('swamp'/'BTD', 'Basic Land').
card_artist('swamp'/'BTD', 'Douglas Shuler').
card_number('swamp'/'BTD', '84').
card_multiverse_id('swamp'/'BTD', '27235').

card_in_set('talruum minotaur', 'BTD').
card_original_type('talruum minotaur'/'BTD', 'Creature — Minotaur').
card_original_text('talruum minotaur'/'BTD', 'Haste (This creature may attack and {T} the turn it comes under your control.)').
card_image_name('talruum minotaur'/'BTD', 'talruum minotaur').
card_uid('talruum minotaur'/'BTD', 'BTD:Talruum Minotaur:talruum minotaur').
card_rarity('talruum minotaur'/'BTD', 'Common').
card_artist('talruum minotaur'/'BTD', 'Pete Venters').
card_number('talruum minotaur'/'BTD', '47').
card_flavor_text('talruum minotaur'/'BTD', 'Don\'t insult a Talruum unless your mount is swift.\n—Suq\'Ata saying').
card_multiverse_id('talruum minotaur'/'BTD', '26646').

card_in_set('tar pit warrior', 'BTD').
card_original_type('tar pit warrior'/'BTD', 'Creature — Giant').
card_original_text('tar pit warrior'/'BTD', 'When Tar Pit Warrior becomes the target of a spell or ability, sacrifice Tar Pit Warrior.').
card_image_name('tar pit warrior'/'BTD', 'tar pit warrior').
card_uid('tar pit warrior'/'BTD', 'BTD:Tar Pit Warrior:tar pit warrior').
card_rarity('tar pit warrior'/'BTD', 'Common').
card_artist('tar pit warrior'/'BTD', 'George Pratt').
card_number('tar pit warrior'/'BTD', '32').
card_flavor_text('tar pit warrior'/'BTD', '\"The cyclops shrugged off savage blows, but casual insults made him weep.\"\n—Azeworai, \"The Cyclops who Couldn\'t\"').
card_multiverse_id('tar pit warrior'/'BTD', '26615').

card_in_set('terror', 'BTD').
card_original_type('terror'/'BTD', 'Instant').
card_original_text('terror'/'BTD', 'Destroy target nonartifact, nonblack creature. It can\'t be regenerated.').
card_image_name('terror'/'BTD', 'terror').
card_uid('terror'/'BTD', 'BTD:Terror:terror').
card_rarity('terror'/'BTD', 'Common').
card_artist('terror'/'BTD', 'Ron Spencer').
card_number('terror'/'BTD', '33').
card_multiverse_id('terror'/'BTD', '26659').

card_in_set('thunderbolt', 'BTD').
card_original_type('thunderbolt'/'BTD', 'Instant').
card_original_text('thunderbolt'/'BTD', 'Choose one Thunderbolt deals 3 damage to target player; or Thunderbolt deals 4 damage to target creature with flying.').
card_image_name('thunderbolt'/'BTD', 'thunderbolt').
card_uid('thunderbolt'/'BTD', 'BTD:Thunderbolt:thunderbolt').
card_rarity('thunderbolt'/'BTD', 'Common').
card_artist('thunderbolt'/'BTD', 'Dylan Martens').
card_number('thunderbolt'/'BTD', '48').
card_flavor_text('thunderbolt'/'BTD', '\"Most wizards consider a thunderbolt to be a proper retort.\"\n—Ertai, wizard adept').
card_multiverse_id('thunderbolt'/'BTD', '27270').

card_in_set('thundering giant', 'BTD').
card_original_type('thundering giant'/'BTD', 'Creature — Giant').
card_original_text('thundering giant'/'BTD', 'Haste (This creature may attack and {T} the turn it comes under your control.)').
card_image_name('thundering giant'/'BTD', 'thundering giant').
card_uid('thundering giant'/'BTD', 'BTD:Thundering Giant:thundering giant').
card_rarity('thundering giant'/'BTD', 'Uncommon').
card_artist('thundering giant'/'BTD', 'Mark Zug').
card_number('thundering giant'/'BTD', '49').
card_flavor_text('thundering giant'/'BTD', 'The giant was felt a few seconds before he was seen.').
card_multiverse_id('thundering giant'/'BTD', '26633').

card_in_set('tolarian winds', 'BTD').
card_original_type('tolarian winds'/'BTD', 'Instant').
card_original_text('tolarian winds'/'BTD', 'Discard your hand, then draw that many cards.').
card_image_name('tolarian winds'/'BTD', 'tolarian winds').
card_uid('tolarian winds'/'BTD', 'BTD:Tolarian Winds:tolarian winds').
card_rarity('tolarian winds'/'BTD', 'Common').
card_artist('tolarian winds'/'BTD', 'Lawrence Snelly').
card_number('tolarian winds'/'BTD', '16').
card_flavor_text('tolarian winds'/'BTD', 'Afterward, Tolaria\'s winds were like the whispers of lost wizards, calling for life.').
card_multiverse_id('tolarian winds'/'BTD', '27264').

card_in_set('viashino warrior', 'BTD').
card_original_type('viashino warrior'/'BTD', 'Creature — Viashino').
card_original_text('viashino warrior'/'BTD', '').
card_image_name('viashino warrior'/'BTD', 'viashino warrior').
card_uid('viashino warrior'/'BTD', 'BTD:Viashino Warrior:viashino warrior').
card_rarity('viashino warrior'/'BTD', 'Common').
card_artist('viashino warrior'/'BTD', 'Roger Raupp').
card_number('viashino warrior'/'BTD', '50').
card_flavor_text('viashino warrior'/'BTD', '\"When traveling the Great Desert avoid wearing the scales of lizards, for the Viashino rule the sands and look poorly on the skinning of their cousins.\"\n—Zhalfirin Guide to the Desert').
card_multiverse_id('viashino warrior'/'BTD', '27265').

card_in_set('vigilant drake', 'BTD').
card_original_type('vigilant drake'/'BTD', 'Creature — Drake').
card_original_text('vigilant drake'/'BTD', 'Flying\n{2}{U} Untap Vigilant Drake.').
card_image_name('vigilant drake'/'BTD', 'vigilant drake').
card_uid('vigilant drake'/'BTD', 'BTD:Vigilant Drake:vigilant drake').
card_rarity('vigilant drake'/'BTD', 'Common').
card_artist('vigilant drake'/'BTD', 'Greg Staples').
card_number('vigilant drake'/'BTD', '17').
card_flavor_text('vigilant drake'/'BTD', 'Awake and awing in the blink of an eye.').
card_multiverse_id('vigilant drake'/'BTD', '26642').

card_in_set('wayward soul', 'BTD').
card_original_type('wayward soul'/'BTD', 'Creature — Spirit').
card_original_text('wayward soul'/'BTD', 'Flying\n{U} Put Wayward Soul on top of its owner\'s library.').
card_image_name('wayward soul'/'BTD', 'wayward soul').
card_uid('wayward soul'/'BTD', 'BTD:Wayward Soul:wayward soul').
card_rarity('wayward soul'/'BTD', 'Common').
card_artist('wayward soul'/'BTD', 'DiTerlizzi').
card_number('wayward soul'/'BTD', '18').
card_flavor_text('wayward soul'/'BTD', '\"no home    no heart    no hope\"\n—Stronghold graffito').
card_multiverse_id('wayward soul'/'BTD', '27266').

card_in_set('wild growth', 'BTD').
card_original_type('wild growth'/'BTD', 'Enchant Land').
card_original_text('wild growth'/'BTD', 'Whenever enchanted land is tapped for mana, its controller adds {G} to his or her mana pool.').
card_image_name('wild growth'/'BTD', 'wild growth').
card_uid('wild growth'/'BTD', 'BTD:Wild Growth:wild growth').
card_rarity('wild growth'/'BTD', 'Common').
card_artist('wild growth'/'BTD', 'Mike Raabe').
card_number('wild growth'/'BTD', '64').
card_multiverse_id('wild growth'/'BTD', '26647').

card_in_set('woolly spider', 'BTD').
card_original_type('woolly spider'/'BTD', 'Creature — Spider').
card_original_text('woolly spider'/'BTD', 'Woolly Spider may block as though it had flying.\nWhenever Woolly Spider blocks a creature with flying, Woolly Spider gets +0/+2 until end of turn.').
card_image_name('woolly spider'/'BTD', 'woolly spider').
card_uid('woolly spider'/'BTD', 'BTD:Woolly Spider:woolly spider').
card_rarity('woolly spider'/'BTD', 'Common').
card_artist('woolly spider'/'BTD', 'Daniel Gelon').
card_number('woolly spider'/'BTD', '65').
card_flavor_text('woolly spider'/'BTD', '\"We need not fear the forces of the air; I\'ve yet to see a spider without an appetite.\"\n—Taaveti of Kelsinko, elvish hunter').
card_multiverse_id('woolly spider'/'BTD', '27271').

card_in_set('yavimaya wurm', 'BTD').
card_original_type('yavimaya wurm'/'BTD', 'Creature — Wurm').
card_original_text('yavimaya wurm'/'BTD', 'Trample').
card_image_name('yavimaya wurm'/'BTD', 'yavimaya wurm').
card_uid('yavimaya wurm'/'BTD', 'BTD:Yavimaya Wurm:yavimaya wurm').
card_rarity('yavimaya wurm'/'BTD', 'Common').
card_artist('yavimaya wurm'/'BTD', 'Melissa A. Benson').
card_number('yavimaya wurm'/'BTD', '66').
card_flavor_text('yavimaya wurm'/'BTD', 'When the gorilla playfully grabbed the wurm\'s tail, the wurm doubled back and playfully ate the gorilla\'s head.').
card_multiverse_id('yavimaya wurm'/'BTD', '26631').
