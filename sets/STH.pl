% Stronghold

set('STH').
set_name('STH', 'Stronghold').
set_release_date('STH', '1998-03-02').
set_border('STH', 'black').
set_type('STH', 'expansion').
set_block('STH', 'Tempest').

card_in_set('acidic sliver', 'STH').
card_original_type('acidic sliver'/'STH', 'Summon — Sliver').
card_original_text('acidic sliver'/'STH', 'Each Sliver gains \"{2}, Sacrifice this creature: This creature deals 2 damage to target creature or player.\"').
card_first_print('acidic sliver', 'STH').
card_image_name('acidic sliver'/'STH', 'acidic sliver').
card_uid('acidic sliver'/'STH', 'STH:Acidic Sliver:acidic sliver').
card_rarity('acidic sliver'/'STH', 'Uncommon').
card_artist('acidic sliver'/'STH', 'Jeff Miracola').
card_flavor_text('acidic sliver'/'STH', 'The first sliver burst against the cave wall, and others piled in behind to deepen the new tunnel.').
card_multiverse_id('acidic sliver'/'STH', '5108').

card_in_set('amok', 'STH').
card_original_type('amok'/'STH', 'Enchantment').
card_original_text('amok'/'STH', '{1}, Discard a card at random: Put a +1/+1 counter on target creature.').
card_first_print('amok', 'STH').
card_image_name('amok'/'STH', 'amok').
card_uid('amok'/'STH', 'STH:Amok:amok').
card_rarity('amok'/'STH', 'Rare').
card_artist('amok'/'STH', 'Dermot Power').
card_flavor_text('amok'/'STH', 'Crovax\'s rage was horrifying. Within moments only blood and shreds of flesh remained of the shapeshifter.').
card_multiverse_id('amok'/'STH', '5109').

card_in_set('awakening', 'STH').
card_original_type('awakening'/'STH', 'Enchantment').
card_original_text('awakening'/'STH', 'At the beginning of each player\'s upkeep, untap all creatures and lands.').
card_first_print('awakening', 'STH').
card_image_name('awakening'/'STH', 'awakening').
card_uid('awakening'/'STH', 'STH:Awakening:awakening').
card_rarity('awakening'/'STH', 'Rare').
card_artist('awakening'/'STH', 'Dan Frazier').
card_flavor_text('awakening'/'STH', '\"There are times when destiny calls forth a people and demands an action. Now is the time. We are the people. This is our action. Charge!\"\n—Eladamri, Lord of Leaves').
card_multiverse_id('awakening'/'STH', '5110').

card_in_set('bandage', 'STH').
card_original_type('bandage'/'STH', 'Instant').
card_original_text('bandage'/'STH', 'Prevent 1 damage to any creature or player.\nDraw a card.').
card_first_print('bandage', 'STH').
card_image_name('bandage'/'STH', 'bandage').
card_uid('bandage'/'STH', 'STH:Bandage:bandage').
card_rarity('bandage'/'STH', 'Common').
card_artist('bandage'/'STH', 'Rebecca Guay').
card_flavor_text('bandage'/'STH', 'Takara shook with guilt. She knew the last image her father had seen was her sword flashing toward him.').
card_multiverse_id('bandage'/'STH', '5111').

card_in_set('bottomless pit', 'STH').
card_original_type('bottomless pit'/'STH', 'Enchantment').
card_original_text('bottomless pit'/'STH', 'During each player\'s upkeep, that player discards a card at random.').
card_first_print('bottomless pit', 'STH').
card_image_name('bottomless pit'/'STH', 'bottomless pit').
card_uid('bottomless pit'/'STH', 'STH:Bottomless Pit:bottomless pit').
card_rarity('bottomless pit'/'STH', 'Uncommon').
card_artist('bottomless pit'/'STH', 'Kev Walker').
card_flavor_text('bottomless pit'/'STH', '\"I\'m sure it came with the place. I don\'t think you build one on purpose.\"\n—Gerrard').
card_multiverse_id('bottomless pit'/'STH', '5113').

card_in_set('brush with death', 'STH').
card_original_type('brush with death'/'STH', 'Sorcery').
card_original_text('brush with death'/'STH', 'Buyback {2}{B}{B} (You may pay an additional {2}{B}{B} when you play this spell. If you do, put it into your hand instead of your graveyard as part of the spell\'s effect.)\nTarget opponent loses 2 life. You gain 2 life.').
card_first_print('brush with death', 'STH').
card_image_name('brush with death'/'STH', 'brush with death').
card_uid('brush with death'/'STH', 'STH:Brush with Death:brush with death').
card_rarity('brush with death'/'STH', 'Common').
card_artist('brush with death'/'STH', 'Stephen Daniele').
card_multiverse_id('brush with death'/'STH', '5114').

card_in_set('bullwhip', 'STH').
card_original_type('bullwhip'/'STH', 'Artifact').
card_original_text('bullwhip'/'STH', '{2}, {T}: Bullwhip deals 1 damage to target creature. That creature attacks this turn if able.').
card_first_print('bullwhip', 'STH').
card_image_name('bullwhip'/'STH', 'bullwhip').
card_uid('bullwhip'/'STH', 'STH:Bullwhip:bullwhip').
card_rarity('bullwhip'/'STH', 'Uncommon').
card_artist('bullwhip'/'STH', 'Brom').
card_flavor_text('bullwhip'/'STH', '\"Pain is a crude way to enforce obedience, but it is cheap and plentiful.\"\n—Volrath').
card_multiverse_id('bullwhip'/'STH', '5115').

card_in_set('burgeoning', 'STH').
card_original_type('burgeoning'/'STH', 'Enchantment').
card_original_text('burgeoning'/'STH', 'Whenever any opponent plays a land, you may choose a land card from your hand and put it into play.').
card_first_print('burgeoning', 'STH').
card_image_name('burgeoning'/'STH', 'burgeoning').
card_uid('burgeoning'/'STH', 'STH:Burgeoning:burgeoning').
card_rarity('burgeoning'/'STH', 'Rare').
card_artist('burgeoning'/'STH', 'Randy Gallegos').
card_flavor_text('burgeoning'/'STH', '\"The plants said, ‘We will fight the stone with root and stem and seed. We are patient. We will win.\'\"\n—Skyshroud myth of the forest').
card_multiverse_id('burgeoning'/'STH', '5116').

card_in_set('calming licid', 'STH').
card_original_type('calming licid'/'STH', 'Summon — Licid').
card_original_text('calming licid'/'STH', '{W}, {T}: Calming Licid loses this ability and becomes a creature enchantment that reads \"Enchanted creature cannot attack\" instead of a creature. Move Calming Licid onto target creature. You may pay {W} to end this effect.').
card_first_print('calming licid', 'STH').
card_image_name('calming licid'/'STH', 'calming licid').
card_uid('calming licid'/'STH', 'STH:Calming Licid:calming licid').
card_rarity('calming licid'/'STH', 'Uncommon').
card_artist('calming licid'/'STH', 'D. Alexander Gregory').
card_multiverse_id('calming licid'/'STH', '5117').

card_in_set('cannibalize', 'STH').
card_original_type('cannibalize'/'STH', 'Sorcery').
card_original_text('cannibalize'/'STH', 'Choose two target creatures controlled by any one player. Remove one of those creatures from the game and put two +1/+1 counters on the other.').
card_first_print('cannibalize', 'STH').
card_image_name('cannibalize'/'STH', 'cannibalize').
card_uid('cannibalize'/'STH', 'STH:Cannibalize:cannibalize').
card_rarity('cannibalize'/'STH', 'Common').
card_artist('cannibalize'/'STH', 'Robert Bliss').
card_flavor_text('cannibalize'/'STH', '\"Mine.\"').
card_multiverse_id('cannibalize'/'STH', '5118').

card_in_set('carnassid', 'STH').
card_original_type('carnassid'/'STH', 'Summon — Beast').
card_original_text('carnassid'/'STH', 'Trample\n{1}{G} Regenerate Carnassid.').
card_first_print('carnassid', 'STH').
card_image_name('carnassid'/'STH', 'carnassid').
card_uid('carnassid'/'STH', 'STH:Carnassid:carnassid').
card_rarity('carnassid'/'STH', 'Rare').
card_artist('carnassid'/'STH', 'Brom').
card_flavor_text('carnassid'/'STH', 'The hunter would never forget the time or place where he first met the carnassid, for they buried him there.').
card_multiverse_id('carnassid'/'STH', '5217').

card_in_set('change of heart', 'STH').
card_original_type('change of heart'/'STH', 'Instant').
card_original_text('change of heart'/'STH', 'Buyback {3} (You may pay an additional {3} when you play this spell. If you do, put it into your hand instead of your graveyard as part of the spell\'s effect.)\nTarget creature cannot attack this turn.').
card_first_print('change of heart', 'STH').
card_image_name('change of heart'/'STH', 'change of heart').
card_uid('change of heart'/'STH', 'STH:Change of Heart:change of heart').
card_rarity('change of heart'/'STH', 'Common').
card_artist('change of heart'/'STH', 'Ron Spencer').
card_multiverse_id('change of heart'/'STH', '5121').

card_in_set('cloud spirit', 'STH').
card_original_type('cloud spirit'/'STH', 'Summon — Spirit').
card_original_text('cloud spirit'/'STH', 'Flying\nCloud Spirit can block only creatures with flying.').
card_image_name('cloud spirit'/'STH', 'cloud spirit').
card_uid('cloud spirit'/'STH', 'STH:Cloud Spirit:cloud spirit').
card_rarity('cloud spirit'/'STH', 'Common').
card_artist('cloud spirit'/'STH', 'Randy Gallegos').
card_flavor_text('cloud spirit'/'STH', 'Clouds are the soil, dreams are the crop.\n—Kor saying').
card_multiverse_id('cloud spirit'/'STH', '5122').

card_in_set('constant mists', 'STH').
card_original_type('constant mists'/'STH', 'Instant').
card_original_text('constant mists'/'STH', 'Buyback—Sacrifice a land (You may sacrifice a land in addition to any other costs when you play this spell. If you do, put Constant Mists into your hand instead of your graveyard as part of the spell\'s effect.)\nCreatures deal no combat damage this turn.').
card_first_print('constant mists', 'STH').
card_image_name('constant mists'/'STH', 'constant mists').
card_uid('constant mists'/'STH', 'STH:Constant Mists:constant mists').
card_rarity('constant mists'/'STH', 'Uncommon').
card_artist('constant mists'/'STH', 'Dermot Power').
card_multiverse_id('constant mists'/'STH', '5123').

card_in_set('contemplation', 'STH').
card_original_type('contemplation'/'STH', 'Enchantment').
card_original_text('contemplation'/'STH', 'Whenever you successfully cast a spell, gain 1 life.').
card_first_print('contemplation', 'STH').
card_image_name('contemplation'/'STH', 'contemplation').
card_uid('contemplation'/'STH', 'STH:Contemplation:contemplation').
card_rarity('contemplation'/'STH', 'Uncommon').
card_artist('contemplation'/'STH', 'Brom').
card_flavor_text('contemplation'/'STH', '\"How fascinating it is to watch the machinations of one\'s own mind play themselves out.\"\n—Volrath').
card_multiverse_id('contemplation'/'STH', '5125').

card_in_set('contempt', 'STH').
card_original_type('contempt'/'STH', 'Enchant Creature').
card_original_text('contempt'/'STH', 'If enchanted creature attacks, return that creature and Contempt to owner\'s hand at end of combat.').
card_first_print('contempt', 'STH').
card_image_name('contempt'/'STH', 'contempt').
card_uid('contempt'/'STH', 'STH:Contempt:contempt').
card_rarity('contempt'/'STH', 'Common').
card_artist('contempt'/'STH', 'Val Mayerik').
card_flavor_text('contempt'/'STH', '\"Predictable little man. In all these years you taught me so much yet learned so little.\"\n—Volrath, to Starke').
card_multiverse_id('contempt'/'STH', '5126').

card_in_set('conviction', 'STH').
card_original_type('conviction'/'STH', 'Enchant Creature').
card_original_text('conviction'/'STH', 'Enchanted creature gets +1/+3.\n{W} Return Conviction to owner\'s hand.').
card_first_print('conviction', 'STH').
card_image_name('conviction'/'STH', 'conviction').
card_uid('conviction'/'STH', 'STH:Conviction:conviction').
card_rarity('conviction'/'STH', 'Common').
card_artist('conviction'/'STH', 'Paolo Parente').
card_flavor_text('conviction'/'STH', 'It was not the minotaur\'s shoulders but his soul that bore the heaviest weight.').
card_multiverse_id('conviction'/'STH', '5127').

card_in_set('convulsing licid', 'STH').
card_original_type('convulsing licid'/'STH', 'Summon — Licid').
card_original_text('convulsing licid'/'STH', '{R}, {T}: Convulsing Licid loses this ability and becomes a creature enchantment that reads \"Enchanted creature cannot block\" instead of a creature. Move Convulsing Licid onto target creature. You may pay {R} to end this effect.').
card_first_print('convulsing licid', 'STH').
card_image_name('convulsing licid'/'STH', 'convulsing licid').
card_uid('convulsing licid'/'STH', 'STH:Convulsing Licid:convulsing licid').
card_rarity('convulsing licid'/'STH', 'Uncommon').
card_artist('convulsing licid'/'STH', 'Scott Kirschner').
card_multiverse_id('convulsing licid'/'STH', '5129').

card_in_set('corrupting licid', 'STH').
card_original_type('corrupting licid'/'STH', 'Summon — Licid').
card_original_text('corrupting licid'/'STH', '{B}, {T}: Corrupting Licid loses this ability and becomes a creature enchantment that reads \"Enchanted creature cannot be blocked except by artifact creatures and black creatures\" instead of a creature. Move Corrupting Licid onto target creature. You may pay {B} to end this effect.').
card_first_print('corrupting licid', 'STH').
card_image_name('corrupting licid'/'STH', 'corrupting licid').
card_uid('corrupting licid'/'STH', 'STH:Corrupting Licid:corrupting licid').
card_rarity('corrupting licid'/'STH', 'Uncommon').
card_artist('corrupting licid'/'STH', 'Thomas M. Baxa').
card_multiverse_id('corrupting licid'/'STH', '5128').

card_in_set('craven giant', 'STH').
card_original_type('craven giant'/'STH', 'Summon — Giant').
card_original_text('craven giant'/'STH', 'Craven Giant cannot block.').
card_image_name('craven giant'/'STH', 'craven giant').
card_uid('craven giant'/'STH', 'STH:Craven Giant:craven giant').
card_rarity('craven giant'/'STH', 'Common').
card_artist('craven giant'/'STH', 'Brian Snõddy').
card_flavor_text('craven giant'/'STH', 'What is high as a mountain and low as a snake?\n—Dal riddle').
card_multiverse_id('craven giant'/'STH', '5130').

card_in_set('crossbow ambush', 'STH').
card_original_type('crossbow ambush'/'STH', 'Instant').
card_original_text('crossbow ambush'/'STH', 'All creatures you control can block creatures with flying until end of turn.').
card_first_print('crossbow ambush', 'STH').
card_image_name('crossbow ambush'/'STH', 'crossbow ambush').
card_uid('crossbow ambush'/'STH', 'STH:Crossbow Ambush:crossbow ambush').
card_rarity('crossbow ambush'/'STH', 'Common').
card_artist('crossbow ambush'/'STH', 'Kev Walker').
card_flavor_text('crossbow ambush'/'STH', 'There is no drake that flies faster than a crossbow bolt.\n—Vec saying').
card_multiverse_id('crossbow ambush'/'STH', '5131').

card_in_set('crovax the cursed', 'STH').
card_original_type('crovax the cursed'/'STH', 'Summon — Legend').
card_original_text('crovax the cursed'/'STH', 'Crovax the Cursed counts as a Vampire.\nCrovax comes into play with four +1/+1 counters on it.\nDuring your upkeep, sacrifice a creature and put a +1/+1 counter on Crovax, or remove a +1/+1 counter from Crovax.\n{B} Crovax gains flying until end of turn.').
card_first_print('crovax the cursed', 'STH').
card_image_name('crovax the cursed'/'STH', 'crovax the cursed').
card_uid('crovax the cursed'/'STH', 'STH:Crovax the Cursed:crovax the cursed').
card_rarity('crovax the cursed'/'STH', 'Rare').
card_artist('crovax the cursed'/'STH', 'Pete Venters').
card_multiverse_id('crovax the cursed'/'STH', '5106').

card_in_set('crystalline sliver', 'STH').
card_original_type('crystalline sliver'/'STH', 'Summon — Sliver').
card_original_text('crystalline sliver'/'STH', 'Slivers cannot be the target of spells or abilities.').
card_first_print('crystalline sliver', 'STH').
card_image_name('crystalline sliver'/'STH', 'crystalline sliver').
card_uid('crystalline sliver'/'STH', 'STH:Crystalline Sliver:crystalline sliver').
card_rarity('crystalline sliver'/'STH', 'Uncommon').
card_artist('crystalline sliver'/'STH', 'L. A. Williams').
card_flavor_text('crystalline sliver'/'STH', 'Bred as living shields, these slivers have proven unruly—they know they cannot be caught.').
card_multiverse_id('crystalline sliver'/'STH', '5134').

card_in_set('dauthi trapper', 'STH').
card_original_type('dauthi trapper'/'STH', 'Summon — Minion').
card_original_text('dauthi trapper'/'STH', '{T}: Target creature gains shadow until end of turn. (This creature can block or be blocked by only creatures with shadow.)').
card_first_print('dauthi trapper', 'STH').
card_image_name('dauthi trapper'/'STH', 'dauthi trapper').
card_uid('dauthi trapper'/'STH', 'STH:Dauthi Trapper:dauthi trapper').
card_rarity('dauthi trapper'/'STH', 'Uncommon').
card_artist('dauthi trapper'/'STH', 'Thomas M. Baxa').
card_flavor_text('dauthi trapper'/'STH', 'Merfolk tell their young of Dandân, humans of Rag Man. Dal tell tales of the Dauthi, and they are far worse.').
card_multiverse_id('dauthi trapper'/'STH', '5135').

card_in_set('death stroke', 'STH').
card_original_type('death stroke'/'STH', 'Sorcery').
card_original_text('death stroke'/'STH', 'Destroy target tapped creature.').
card_first_print('death stroke', 'STH').
card_image_name('death stroke'/'STH', 'death stroke').
card_uid('death stroke'/'STH', 'STH:Death Stroke:death stroke').
card_rarity('death stroke'/'STH', 'Common').
card_artist('death stroke'/'STH', 'Colin MacNeil').
card_flavor_text('death stroke'/'STH', 'For a sharp second, Selenia froze, and Crovax\'s blade found home. As the angel shattered like glass, Crovax felt his mind collapse—the curse had been fulfilled.').
card_multiverse_id('death stroke'/'STH', '5137').

card_in_set('dream halls', 'STH').
card_original_type('dream halls'/'STH', 'Enchantment').
card_original_text('dream halls'/'STH', 'Instead of paying the casting cost for a spell of any color, its caster may choose and discard a card that shares at least one color with that spell. If the spell has {X} in its casting cost, X is 0.').
card_first_print('dream halls', 'STH').
card_image_name('dream halls'/'STH', 'dream halls').
card_uid('dream halls'/'STH', 'STH:Dream Halls:dream halls').
card_rarity('dream halls'/'STH', 'Rare').
card_artist('dream halls'/'STH', 'Matthew D. Wilson').
card_flavor_text('dream halls'/'STH', 'Within without.').
card_multiverse_id('dream halls'/'STH', '5105').

card_in_set('dream prowler', 'STH').
card_original_type('dream prowler'/'STH', 'Summon — Illusion').
card_original_text('dream prowler'/'STH', 'Dream Prowler is unblockable as long as no other creatures are attacking.').
card_first_print('dream prowler', 'STH').
card_image_name('dream prowler'/'STH', 'dream prowler').
card_uid('dream prowler'/'STH', 'STH:Dream Prowler:dream prowler').
card_rarity('dream prowler'/'STH', 'Common').
card_artist('dream prowler'/'STH', 'Richard Kane Ferguson').
card_flavor_text('dream prowler'/'STH', '\"To think that some find sleep a restful state.\"\n—Volrath').
card_multiverse_id('dream prowler'/'STH', '5140').

card_in_set('duct crawler', 'STH').
card_original_type('duct crawler'/'STH', 'Summon — Insect').
card_original_text('duct crawler'/'STH', '{1}{R} Target creature cannot block Duct Crawler this turn.').
card_first_print('duct crawler', 'STH').
card_image_name('duct crawler'/'STH', 'duct crawler').
card_uid('duct crawler'/'STH', 'STH:Duct Crawler:duct crawler').
card_rarity('duct crawler'/'STH', 'Common').
card_artist('duct crawler'/'STH', 'Stephen Daniele').
card_flavor_text('duct crawler'/'STH', '\"The mountain\'s ducts will be like organ pipes, amplifying the glorious roar of the dying.\"\n—Stronghold architect, journal').
card_multiverse_id('duct crawler'/'STH', '5141').

card_in_set('dungeon shade', 'STH').
card_original_type('dungeon shade'/'STH', 'Summon — Spirit').
card_original_text('dungeon shade'/'STH', 'Flying\n{B} Dungeon Shade gets +1/+1 until end of turn.').
card_first_print('dungeon shade', 'STH').
card_image_name('dungeon shade'/'STH', 'dungeon shade').
card_uid('dungeon shade'/'STH', 'STH:Dungeon Shade:dungeon shade').
card_rarity('dungeon shade'/'STH', 'Common').
card_artist('dungeon shade'/'STH', 'Jason Alexander Behnke').
card_flavor_text('dungeon shade'/'STH', 'A sickness stirs in its eyes, a nightmare born in darkened wails.').
card_multiverse_id('dungeon shade'/'STH', '5142').

card_in_set('elven rite', 'STH').
card_original_type('elven rite'/'STH', 'Sorcery').
card_original_text('elven rite'/'STH', 'Put two +1/+1 counters, distributed any way you choose, on any number of target creatures.').
card_first_print('elven rite', 'STH').
card_image_name('elven rite'/'STH', 'elven rite').
card_uid('elven rite'/'STH', 'STH:Elven Rite:elven rite').
card_rarity('elven rite'/'STH', 'Uncommon').
card_artist('elven rite'/'STH', 'Jeff Miracola').
card_flavor_text('elven rite'/'STH', 'As the bough stretches, so shall you grow. As the roots spread, so shall you thrive.').
card_multiverse_id('elven rite'/'STH', '5144').

card_in_set('endangered armodon', 'STH').
card_original_type('endangered armodon'/'STH', 'Summon — Elephant').
card_original_text('endangered armodon'/'STH', 'If you control any creature with toughness 2 or less, sacrifice Endangered Armodon.').
card_first_print('endangered armodon', 'STH').
card_image_name('endangered armodon'/'STH', 'endangered armodon').
card_uid('endangered armodon'/'STH', 'STH:Endangered Armodon:endangered armodon').
card_rarity('endangered armodon'/'STH', 'Common').
card_artist('endangered armodon'/'STH', 'Kev Walker').
card_flavor_text('endangered armodon'/'STH', 'These are its last days. Its doom to be remembered only until the hunters hunger again.').
card_multiverse_id('endangered armodon'/'STH', '5145').

card_in_set('ensnaring bridge', 'STH').
card_original_type('ensnaring bridge'/'STH', 'Artifact').
card_original_text('ensnaring bridge'/'STH', 'Each creature with power greater than the number of cards in your hand cannot attack.').
card_first_print('ensnaring bridge', 'STH').
card_image_name('ensnaring bridge'/'STH', 'ensnaring bridge').
card_uid('ensnaring bridge'/'STH', 'STH:Ensnaring Bridge:ensnaring bridge').
card_rarity('ensnaring bridge'/'STH', 'Rare').
card_artist('ensnaring bridge'/'STH', 'Pete Venters').
card_flavor_text('ensnaring bridge'/'STH', '\"I expected a fight, but I didn\'t expect it from the building itself.\"\n—Gerrard').
card_multiverse_id('ensnaring bridge'/'STH', '5124').

card_in_set('evacuation', 'STH').
card_original_type('evacuation'/'STH', 'Instant').
card_original_text('evacuation'/'STH', 'Return all creatures to owners\' hands.').
card_first_print('evacuation', 'STH').
card_image_name('evacuation'/'STH', 'evacuation').
card_uid('evacuation'/'STH', 'STH:Evacuation:evacuation').
card_rarity('evacuation'/'STH', 'Rare').
card_artist('evacuation'/'STH', 'Rob Alexander').
card_flavor_text('evacuation'/'STH', 'The first step of every exodus is from the blood and the fire onto the trail.').
card_multiverse_id('evacuation'/'STH', '5147').

card_in_set('fanning the flames', 'STH').
card_original_type('fanning the flames'/'STH', 'Sorcery').
card_original_text('fanning the flames'/'STH', 'Buyback {3} (You may pay an additional {3} when you play this spell. If you do, put it into your hand instead of your graveyard as part of the spell\'s effect.)\nFanning the Flames deals X damage to target creature or player.').
card_first_print('fanning the flames', 'STH').
card_image_name('fanning the flames'/'STH', 'fanning the flames').
card_uid('fanning the flames'/'STH', 'STH:Fanning the Flames:fanning the flames').
card_rarity('fanning the flames'/'STH', 'Uncommon').
card_artist('fanning the flames'/'STH', 'Ron Spencer').
card_multiverse_id('fanning the flames'/'STH', '5148').

card_in_set('flame wave', 'STH').
card_original_type('flame wave'/'STH', 'Sorcery').
card_original_text('flame wave'/'STH', 'Flame Wave deals 4 damage to target player and each creature he or she controls.').
card_first_print('flame wave', 'STH').
card_image_name('flame wave'/'STH', 'flame wave').
card_uid('flame wave'/'STH', 'STH:Flame Wave:flame wave').
card_rarity('flame wave'/'STH', 'Uncommon').
card_artist('flame wave'/'STH', 'Donato Giancola').
card_flavor_text('flame wave'/'STH', '\"I hear the roaring of a wave whose waters are red and whose mists are black.\"\n—Oracle en-Vec').
card_multiverse_id('flame wave'/'STH', '5150').

card_in_set('fling', 'STH').
card_original_type('fling'/'STH', 'Instant').
card_original_text('fling'/'STH', 'Sacrifice a creature: Fling deals damage equal to the sacrificed creature\'s power to target creature or player.').
card_image_name('fling'/'STH', 'fling').
card_uid('fling'/'STH', 'STH:Fling:fling').
card_rarity('fling'/'STH', 'Common').
card_artist('fling'/'STH', 'Paolo Parente').
card_flavor_text('fling'/'STH', 'It\'s raining rats and moggs.').
card_multiverse_id('fling'/'STH', '5151').

card_in_set('flowstone blade', 'STH').
card_original_type('flowstone blade'/'STH', 'Enchant Creature').
card_original_text('flowstone blade'/'STH', '{R} Enchanted creature gets +1/-1 until end of turn.').
card_first_print('flowstone blade', 'STH').
card_image_name('flowstone blade'/'STH', 'flowstone blade').
card_uid('flowstone blade'/'STH', 'STH:Flowstone Blade:flowstone blade').
card_rarity('flowstone blade'/'STH', 'Common').
card_artist('flowstone blade'/'STH', 'L. A. Williams').
card_flavor_text('flowstone blade'/'STH', '\"Remember the fable of the elf who came upon a cave of gold. In trying to free the largest piece, she was crushed by its weight.\"\n—Karn, silver golem').
card_multiverse_id('flowstone blade'/'STH', '5152').

card_in_set('flowstone hellion', 'STH').
card_original_type('flowstone hellion'/'STH', 'Summon — Beast').
card_original_text('flowstone hellion'/'STH', 'Flowstone Hellion is unaffected by summoning sickness.\n{0}: Flowstone Hellion gets +1/-1 until end of turn.').
card_first_print('flowstone hellion', 'STH').
card_image_name('flowstone hellion'/'STH', 'flowstone hellion').
card_uid('flowstone hellion'/'STH', 'STH:Flowstone Hellion:flowstone hellion').
card_rarity('flowstone hellion'/'STH', 'Uncommon').
card_artist('flowstone hellion'/'STH', 'Daren Bader').
card_flavor_text('flowstone hellion'/'STH', 'Volrath leaves no stone untrained.').
card_multiverse_id('flowstone hellion'/'STH', '5153').

card_in_set('flowstone mauler', 'STH').
card_original_type('flowstone mauler'/'STH', 'Summon — Beast').
card_original_text('flowstone mauler'/'STH', 'Trample\n{R} Flowstone Mauler gets +1/-1 until end of turn.').
card_first_print('flowstone mauler', 'STH').
card_image_name('flowstone mauler'/'STH', 'flowstone mauler').
card_uid('flowstone mauler'/'STH', 'STH:Flowstone Mauler:flowstone mauler').
card_rarity('flowstone mauler'/'STH', 'Rare').
card_artist('flowstone mauler'/'STH', 'Paolo Parente').
card_flavor_text('flowstone mauler'/'STH', 'Once these horrifying creatures were perfected, there was no need for armodons.').
card_multiverse_id('flowstone mauler'/'STH', '5154').

card_in_set('flowstone shambler', 'STH').
card_original_type('flowstone shambler'/'STH', 'Summon — Beast').
card_original_text('flowstone shambler'/'STH', '{R} Flowstone Shambler gets +1/-1 until end of turn.').
card_first_print('flowstone shambler', 'STH').
card_image_name('flowstone shambler'/'STH', 'flowstone shambler').
card_uid('flowstone shambler'/'STH', 'STH:Flowstone Shambler:flowstone shambler').
card_rarity('flowstone shambler'/'STH', 'Common').
card_artist('flowstone shambler'/'STH', 'Jim Nelson').
card_flavor_text('flowstone shambler'/'STH', 'Flowstone troops are notoriously difficult to keep in formation.').
card_multiverse_id('flowstone shambler'/'STH', '5156').

card_in_set('foul imp', 'STH').
card_original_type('foul imp'/'STH', 'Summon — Imp').
card_original_text('foul imp'/'STH', 'Flying\nWhen Foul Imp comes into play, lose 2 life.').
card_first_print('foul imp', 'STH').
card_image_name('foul imp'/'STH', 'foul imp').
card_uid('foul imp'/'STH', 'STH:Foul Imp:foul imp').
card_rarity('foul imp'/'STH', 'Common').
card_artist('foul imp'/'STH', 'Jim Nelson').
card_flavor_text('foul imp'/'STH', 'The imp, unaware of its own odor, paused to catch its breath . . . and promptly died.').
card_multiverse_id('foul imp'/'STH', '5158').

card_in_set('furnace spirit', 'STH').
card_original_type('furnace spirit'/'STH', 'Summon — Spirit').
card_original_text('furnace spirit'/'STH', 'Furnace Spirit is unaffected by summoning sickness.\n{R} Furnace Spirit gets +1/+0 until end of turn.').
card_first_print('furnace spirit', 'STH').
card_image_name('furnace spirit'/'STH', 'furnace spirit').
card_uid('furnace spirit'/'STH', 'STH:Furnace Spirit:furnace spirit').
card_rarity('furnace spirit'/'STH', 'Common').
card_artist('furnace spirit'/'STH', 'Jeff Miracola').
card_flavor_text('furnace spirit'/'STH', 'If it can survive the Furnace, you don\'t want it near you.').
card_multiverse_id('furnace spirit'/'STH', '5160').

card_in_set('gliding licid', 'STH').
card_original_type('gliding licid'/'STH', 'Summon — Licid').
card_original_text('gliding licid'/'STH', '{U}, {T}: Gliding Licid loses this ability and becomes a creature enchantment that reads \"Enchanted creature gains flying\" instead of a creature. Move Gliding Licid onto target creature. You may pay {U} to end this effect.').
card_first_print('gliding licid', 'STH').
card_image_name('gliding licid'/'STH', 'gliding licid').
card_uid('gliding licid'/'STH', 'STH:Gliding Licid:gliding licid').
card_rarity('gliding licid'/'STH', 'Uncommon').
card_artist('gliding licid'/'STH', 'Heather Hudson').
card_multiverse_id('gliding licid'/'STH', '5162').

card_in_set('grave pact', 'STH').
card_original_type('grave pact'/'STH', 'Enchantment').
card_original_text('grave pact'/'STH', 'Whenever any creature you control is put into any graveyard, each other player sacrifices a creature.').
card_first_print('grave pact', 'STH').
card_image_name('grave pact'/'STH', 'grave pact').
card_uid('grave pact'/'STH', 'STH:Grave Pact:grave pact').
card_rarity('grave pact'/'STH', 'Rare').
card_artist('grave pact'/'STH', 'Scott Kirschner').
card_flavor_text('grave pact'/'STH', '\"The bonds of loyalty can tie one to the grave.\"\n—Crovax').
card_multiverse_id('grave pact'/'STH', '5136').

card_in_set('hammerhead shark', 'STH').
card_original_type('hammerhead shark'/'STH', 'Summon — Fish').
card_original_text('hammerhead shark'/'STH', 'Hammerhead Shark cannot attack unless defending player controls any islands.').
card_first_print('hammerhead shark', 'STH').
card_image_name('hammerhead shark'/'STH', 'hammerhead shark').
card_uid('hammerhead shark'/'STH', 'STH:Hammerhead Shark:hammerhead shark').
card_rarity('hammerhead shark'/'STH', 'Common').
card_artist('hammerhead shark'/'STH', 'Stephen Daniele').
card_flavor_text('hammerhead shark'/'STH', 'Cross the eyes of a hammerhead and you\'ll dot its teeth.').
card_multiverse_id('hammerhead shark'/'STH', '5163').

card_in_set('heartstone', 'STH').
card_original_type('heartstone'/'STH', 'Artifact').
card_original_text('heartstone'/'STH', 'The cost of each creature ability requiring an activation cost is reduced by {1}. This cannot reduce an ability\'s generic mana cost to less then {1}.').
card_first_print('heartstone', 'STH').
card_image_name('heartstone'/'STH', 'heartstone').
card_uid('heartstone'/'STH', 'STH:Heartstone:heartstone').
card_rarity('heartstone'/'STH', 'Uncommon').
card_artist('heartstone'/'STH', 'John Matson').
card_flavor_text('heartstone'/'STH', '\"Finding a true heartstone is even harder than finding a true heart.\"\n—Hanna').
card_multiverse_id('heartstone'/'STH', '5164').

card_in_set('heat of battle', 'STH').
card_original_type('heat of battle'/'STH', 'Enchantment').
card_original_text('heat of battle'/'STH', 'Whenever any creature blocks, Heat of Battle deals 1 damage to that creature\'s controller.').
card_first_print('heat of battle', 'STH').
card_image_name('heat of battle'/'STH', 'heat of battle').
card_uid('heat of battle'/'STH', 'STH:Heat of Battle:heat of battle').
card_rarity('heat of battle'/'STH', 'Uncommon').
card_artist('heat of battle'/'STH', 'Matthew D. Wilson').
card_flavor_text('heat of battle'/'STH', '\"Takara, it\'s me!\" shouted Starke. It was no use—she obeyed Volrath.').
card_multiverse_id('heat of battle'/'STH', '5165').

card_in_set('hermit druid', 'STH').
card_original_type('hermit druid'/'STH', 'Summon — Druid').
card_original_text('hermit druid'/'STH', '{G}, {T}: Reveal cards from the top of your library until you reveal a basic land card. Put that card into your hand and put all other revealed cards into your graveyard.').
card_first_print('hermit druid', 'STH').
card_image_name('hermit druid'/'STH', 'hermit druid').
card_uid('hermit druid'/'STH', 'STH:Hermit Druid:hermit druid').
card_rarity('hermit druid'/'STH', 'Rare').
card_artist('hermit druid'/'STH', 'Heather Hudson').
card_flavor_text('hermit druid'/'STH', 'Seeking the company of plants ensures that your wits will go to seed.').
card_multiverse_id('hermit druid'/'STH', '5166').

card_in_set('hesitation', 'STH').
card_original_type('hesitation'/'STH', 'Enchantment').
card_original_text('hesitation'/'STH', 'If any spell is played, counter that spell and sacrifice Hesitation.').
card_first_print('hesitation', 'STH').
card_image_name('hesitation'/'STH', 'hesitation').
card_uid('hesitation'/'STH', 'STH:Hesitation:hesitation').
card_rarity('hesitation'/'STH', 'Uncommon').
card_artist('hesitation'/'STH', 'Pete Venters').
card_flavor_text('hesitation'/'STH', 'Gerrard hesitated, looking up at the enemy ship. The future hesitated with him, changed forever by the pause.').
card_multiverse_id('hesitation'/'STH', '5167').

card_in_set('hibernation sliver', 'STH').
card_original_type('hibernation sliver'/'STH', 'Summon — Sliver').
card_original_text('hibernation sliver'/'STH', 'Each Sliver gains \"Pay 2 life: Return this creature to owner\'s hand.\"').
card_first_print('hibernation sliver', 'STH').
card_image_name('hibernation sliver'/'STH', 'hibernation sliver').
card_uid('hibernation sliver'/'STH', 'STH:Hibernation Sliver:hibernation sliver').
card_rarity('hibernation sliver'/'STH', 'Uncommon').
card_artist('hibernation sliver'/'STH', 'Scott Kirschner').
card_flavor_text('hibernation sliver'/'STH', 'Mogglings have been known to play ball with hibernating slivers, completely unaware of their true nature.').
card_multiverse_id('hibernation sliver'/'STH', '5168').

card_in_set('hidden retreat', 'STH').
card_original_type('hidden retreat'/'STH', 'Enchantment').
card_original_text('hidden retreat'/'STH', 'Choose a card in your hand and put it on top of your library: Prevent all damage from an instant or sorcery. (Treat further damage from that source normally.)').
card_first_print('hidden retreat', 'STH').
card_image_name('hidden retreat'/'STH', 'hidden retreat').
card_uid('hidden retreat'/'STH', 'STH:Hidden Retreat:hidden retreat').
card_rarity('hidden retreat'/'STH', 'Rare').
card_artist('hidden retreat'/'STH', 'Terese Nielsen').
card_flavor_text('hidden retreat'/'STH', 'After an hour in the hidey-hole, Squee seriously pondered the advantages of danger over boredom.').
card_multiverse_id('hidden retreat'/'STH', '5169').

card_in_set('honor guard', 'STH').
card_original_type('honor guard'/'STH', 'Summon — Soldier').
card_original_text('honor guard'/'STH', '{W} Honor Guard gets +0/+1 until end of turn.').
card_first_print('honor guard', 'STH').
card_image_name('honor guard'/'STH', 'honor guard').
card_uid('honor guard'/'STH', 'STH:Honor Guard:honor guard').
card_rarity('honor guard'/'STH', 'Common').
card_artist('honor guard'/'STH', 'Joel Biske').
card_flavor_text('honor guard'/'STH', '\"It is not a choice I make, to have this guard. It is the choice of my people, and my duty to them.\"\n—Oracle en-Vec').
card_multiverse_id('honor guard'/'STH', '5170').

card_in_set('horn of greed', 'STH').
card_original_type('horn of greed'/'STH', 'Artifact').
card_original_text('horn of greed'/'STH', 'Whenever any player plays a land, that player draws a card.').
card_first_print('horn of greed', 'STH').
card_image_name('horn of greed'/'STH', 'horn of greed').
card_uid('horn of greed'/'STH', 'STH:Horn of Greed:horn of greed').
card_rarity('horn of greed'/'STH', 'Rare').
card_artist('horn of greed'/'STH', 'Jeff Miracola').
card_flavor_text('horn of greed'/'STH', '\"Rath grows, and I am nourished.\"\n—Volrath').
card_multiverse_id('horn of greed'/'STH', '5172').

card_in_set('hornet cannon', 'STH').
card_original_type('hornet cannon'/'STH', 'Artifact').
card_original_text('hornet cannon'/'STH', '{3}, {T}: Put a Hornet token into play. Treat this token as a 1/1 artifact creature with flying that is unaffected by summoning sickness. At end of turn, destroy the token.').
card_first_print('hornet cannon', 'STH').
card_image_name('hornet cannon'/'STH', 'hornet cannon').
card_uid('hornet cannon'/'STH', 'STH:Hornet Cannon:hornet cannon').
card_rarity('hornet cannon'/'STH', 'Uncommon').
card_artist('hornet cannon'/'STH', 'Ron Spencer').
card_multiverse_id('hornet cannon'/'STH', '5173').

card_in_set('intruder alarm', 'STH').
card_original_type('intruder alarm'/'STH', 'Enchantment').
card_original_text('intruder alarm'/'STH', 'Creatures do not untap during their controllers\' untap phases.\nWhenever any creature comes into play, untap all creatures.').
card_first_print('intruder alarm', 'STH').
card_image_name('intruder alarm'/'STH', 'intruder alarm').
card_uid('intruder alarm'/'STH', 'STH:Intruder Alarm:intruder alarm').
card_rarity('intruder alarm'/'STH', 'Rare').
card_artist('intruder alarm'/'STH', 'Donato Giancola').
card_flavor_text('intruder alarm'/'STH', 'Mirri was startled by Volrath\'s apparition—it had no scent.').
card_multiverse_id('intruder alarm'/'STH', '5174').

card_in_set('invasion plans', 'STH').
card_original_type('invasion plans'/'STH', 'Enchantment').
card_original_text('invasion plans'/'STH', 'Each creature blocks whenever able.\nAttacking player chooses how each creature blocks. (All blocking assignments must still be legal.)').
card_first_print('invasion plans', 'STH').
card_image_name('invasion plans'/'STH', 'invasion plans').
card_uid('invasion plans'/'STH', 'STH:Invasion Plans:invasion plans').
card_rarity('invasion plans'/'STH', 'Rare').
card_artist('invasion plans'/'STH', 'Pete Venters').
card_flavor_text('invasion plans'/'STH', 'Gerrard studied the globe as Mirri kept watch. Suddenly, his eyes widened. \"This is Dominaria!\"').
card_multiverse_id('invasion plans'/'STH', '5175').

card_in_set('jinxed ring', 'STH').
card_original_type('jinxed ring'/'STH', 'Artifact').
card_original_text('jinxed ring'/'STH', 'Whenever any card is put into your graveyard from play, Jinxed Ring deals 1 damage to you.\nSacrifice a creature: Target opponent gains control of Jinxed Ring permanently').
card_first_print('jinxed ring', 'STH').
card_image_name('jinxed ring'/'STH', 'jinxed ring').
card_uid('jinxed ring'/'STH', 'STH:Jinxed Ring:jinxed ring').
card_rarity('jinxed ring'/'STH', 'Rare').
card_artist('jinxed ring'/'STH', 'M. W. Kaluta & DiTerlizzi').
card_multiverse_id('jinxed ring'/'STH', '5177').

card_in_set('lab rats', 'STH').
card_original_type('lab rats'/'STH', 'Sorcery').
card_original_text('lab rats'/'STH', 'Buyback {4} (You may pay an additional {4} when you play this spell. If you do, put it into your hand instead of your graveyard as part of the spell\'s effect.)\nPut a Rat token into play. Treat this token as a 1/1 black creature.').
card_first_print('lab rats', 'STH').
card_image_name('lab rats'/'STH', 'lab rats').
card_uid('lab rats'/'STH', 'STH:Lab Rats:lab rats').
card_rarity('lab rats'/'STH', 'Common').
card_artist('lab rats'/'STH', 'DiTerlizzi').
card_multiverse_id('lab rats'/'STH', '5178').

card_in_set('lancers en-kor', 'STH').
card_original_type('lancers en-kor'/'STH', 'Summon — Soldiers').
card_original_text('lancers en-kor'/'STH', 'Trample\n{0}: Redirect 1 damage from Lancers en-Kor to a creature you control.').
card_first_print('lancers en-kor', 'STH').
card_image_name('lancers en-kor'/'STH', 'lancers en-kor').
card_uid('lancers en-kor'/'STH', 'STH:Lancers en-Kor:lancers en-kor').
card_rarity('lancers en-kor'/'STH', 'Uncommon').
card_artist('lancers en-kor'/'STH', 'Pete Venters').
card_flavor_text('lancers en-kor'/'STH', 'Nearly all sets of Kor twins are trained as lancers.').
card_multiverse_id('lancers en-kor'/'STH', '5179').

card_in_set('leap', 'STH').
card_original_type('leap'/'STH', 'Instant').
card_original_text('leap'/'STH', 'Target creature gains flying until end of turn.\nDraw a card.').
card_first_print('leap', 'STH').
card_image_name('leap'/'STH', 'leap').
card_uid('leap'/'STH', 'STH:Leap:leap').
card_rarity('leap'/'STH', 'Common').
card_artist('leap'/'STH', 'Kev Walker').
card_flavor_text('leap'/'STH', 'Mirri leapt quickly and silently. The guard died likewise.').
card_multiverse_id('leap'/'STH', '5180').

card_in_set('lowland basilisk', 'STH').
card_original_type('lowland basilisk'/'STH', 'Summon — Basilisk').
card_original_text('lowland basilisk'/'STH', 'Whenever Lowland Basilisk damages any creature, destroy that creature at end of combat.').
card_first_print('lowland basilisk', 'STH').
card_image_name('lowland basilisk'/'STH', 'lowland basilisk').
card_uid('lowland basilisk'/'STH', 'STH:Lowland Basilisk:lowland basilisk').
card_rarity('lowland basilisk'/'STH', 'Common').
card_artist('lowland basilisk'/'STH', 'Randy Gallegos').
card_flavor_text('lowland basilisk'/'STH', 'Unlike their cousins, Rathi basilisks turn their victims into puddles of flowstone.').
card_multiverse_id('lowland basilisk'/'STH', '5181').

card_in_set('mana leak', 'STH').
card_original_type('mana leak'/'STH', 'Interrupt').
card_original_text('mana leak'/'STH', 'Counter target spell unless its caster pays an additional {3}.').
card_image_name('mana leak'/'STH', 'mana leak').
card_uid('mana leak'/'STH', 'STH:Mana Leak:mana leak').
card_rarity('mana leak'/'STH', 'Common').
card_artist('mana leak'/'STH', 'Christopher Rush').
card_flavor_text('mana leak'/'STH', '\"The fatal flaw in every plan is the assumption that you know more than your enemy.\"\n—Volrath').
card_multiverse_id('mana leak'/'STH', '5182').

card_in_set('mask of the mimic', 'STH').
card_original_type('mask of the mimic'/'STH', 'Instant').
card_original_text('mask of the mimic'/'STH', 'Sacrifice a creature: Search your library for any copy of target creature card and put it into play. Shuffle your library afterwards.').
card_first_print('mask of the mimic', 'STH').
card_image_name('mask of the mimic'/'STH', 'mask of the mimic').
card_uid('mask of the mimic'/'STH', 'STH:Mask of the Mimic:mask of the mimic').
card_rarity('mask of the mimic'/'STH', 'Uncommon').
card_artist('mask of the mimic'/'STH', 'Heather Hudson').
card_multiverse_id('mask of the mimic'/'STH', '5183').

card_in_set('megrim', 'STH').
card_original_type('megrim'/'STH', 'Enchantment').
card_original_text('megrim'/'STH', 'Whenever any opponent discards a card, Megrim deals 2 damage to him or her.').
card_first_print('megrim', 'STH').
card_image_name('megrim'/'STH', 'megrim').
card_uid('megrim'/'STH', 'STH:Megrim:megrim').
card_rarity('megrim'/'STH', 'Uncommon').
card_artist('megrim'/'STH', 'Donato Giancola').
card_flavor_text('megrim'/'STH', '\"You can run from your pain,\" explained Gerrard to Crovax, \"but take it from experience: you will tire before it does.\"').
card_multiverse_id('megrim'/'STH', '5185').

card_in_set('mind games', 'STH').
card_original_type('mind games'/'STH', 'Instant').
card_original_text('mind games'/'STH', 'Buyback {2}{U} (You may pay an additional {2}{U} when you play this spell. If you do, put it into your hand instead of your graveyard as part of the spell\'s effect.)\nTap target artifact, creature, or land.').
card_first_print('mind games', 'STH').
card_image_name('mind games'/'STH', 'mind games').
card_uid('mind games'/'STH', 'STH:Mind Games:mind games').
card_rarity('mind games'/'STH', 'Common').
card_artist('mind games'/'STH', 'Andrew Robinson').
card_multiverse_id('mind games'/'STH', '5186').

card_in_set('mind peel', 'STH').
card_original_type('mind peel'/'STH', 'Sorcery').
card_original_text('mind peel'/'STH', 'Buyback {2}{B}{B} (You may pay an additional {2}{B}{B} when you play this spell. If you do, put it into your hand instead of your graveyard as part of the spell\'s effect.)\nTarget player chooses and discards a card.').
card_first_print('mind peel', 'STH').
card_image_name('mind peel'/'STH', 'mind peel').
card_uid('mind peel'/'STH', 'STH:Mind Peel:mind peel').
card_rarity('mind peel'/'STH', 'Uncommon').
card_artist('mind peel'/'STH', 'Adam Rex').
card_multiverse_id('mind peel'/'STH', '5187').

card_in_set('mindwarper', 'STH').
card_original_type('mindwarper'/'STH', 'Summon — Spirit').
card_original_text('mindwarper'/'STH', 'Mindwarper comes into play with three +1/+1 counters on it.\n{2}{B}, Remove a +1/+1 counter from Mindwarper: Target player chooses and discards a card. Play this ability as a sorcery.').
card_first_print('mindwarper', 'STH').
card_image_name('mindwarper'/'STH', 'mindwarper').
card_uid('mindwarper'/'STH', 'STH:Mindwarper:mindwarper').
card_rarity('mindwarper'/'STH', 'Rare').
card_artist('mindwarper'/'STH', 'Paolo Parente').
card_multiverse_id('mindwarper'/'STH', '5188').

card_in_set('mob justice', 'STH').
card_original_type('mob justice'/'STH', 'Sorcery').
card_original_text('mob justice'/'STH', 'Mob Justice deals 1 damage to target player for each creature you control.').
card_first_print('mob justice', 'STH').
card_image_name('mob justice'/'STH', 'mob justice').
card_uid('mob justice'/'STH', 'STH:Mob Justice:mob justice').
card_rarity('mob justice'/'STH', 'Common').
card_artist('mob justice'/'STH', 'Ron Spencer').
card_flavor_text('mob justice'/'STH', 'A single stone can start an avalanche.').
card_multiverse_id('mob justice'/'STH', '5101').

card_in_set('mogg bombers', 'STH').
card_original_type('mogg bombers'/'STH', 'Summon — Goblins').
card_original_text('mogg bombers'/'STH', 'If any other creature comes into play, sacrifice Mogg Bombers and it deals 3 damage to target player.').
card_first_print('mogg bombers', 'STH').
card_image_name('mogg bombers'/'STH', 'mogg bombers').
card_uid('mogg bombers'/'STH', 'STH:Mogg Bombers:mogg bombers').
card_rarity('mogg bombers'/'STH', 'Common').
card_artist('mogg bombers'/'STH', 'Dermot Power').
card_flavor_text('mogg bombers'/'STH', 'Behind every great mogg bomber is another mogg with a shovel and a basket.').
card_multiverse_id('mogg bombers'/'STH', '4980').

card_in_set('mogg flunkies', 'STH').
card_original_type('mogg flunkies'/'STH', 'Summon — Goblins').
card_original_text('mogg flunkies'/'STH', 'Mogg Flunkies cannot attack or block during a turn in which no other creature you control attacks or blocks.').
card_first_print('mogg flunkies', 'STH').
card_image_name('mogg flunkies'/'STH', 'mogg flunkies').
card_uid('mogg flunkies'/'STH', 'STH:Mogg Flunkies:mogg flunkies').
card_rarity('mogg flunkies'/'STH', 'Common').
card_artist('mogg flunkies'/'STH', 'Brom').
card_flavor_text('mogg flunkies'/'STH', 'They\'ll attack whatever\'s in front of them—as long as you tell them where that is.').
card_multiverse_id('mogg flunkies'/'STH', '5100').

card_in_set('mogg infestation', 'STH').
card_original_type('mogg infestation'/'STH', 'Sorcery').
card_original_text('mogg infestation'/'STH', 'Destroy all creatures target player controls. For each creature put into any graveyard in this way, put two Goblin tokens into play under that player\'s control. Treat these tokens as 1/1 red creatures.').
card_first_print('mogg infestation', 'STH').
card_image_name('mogg infestation'/'STH', 'mogg infestation').
card_uid('mogg infestation'/'STH', 'STH:Mogg Infestation:mogg infestation').
card_rarity('mogg infestation'/'STH', 'Rare').
card_artist('mogg infestation'/'STH', 'Pete Venters').
card_multiverse_id('mogg infestation'/'STH', '5107').

card_in_set('mogg maniac', 'STH').
card_original_type('mogg maniac'/'STH', 'Summon — Goblin').
card_original_text('mogg maniac'/'STH', 'Whenever Mogg Maniac is dealt damage, it deals an equal amount of damage to target opponent.').
card_first_print('mogg maniac', 'STH').
card_image_name('mogg maniac'/'STH', 'mogg maniac').
card_uid('mogg maniac'/'STH', 'STH:Mogg Maniac:mogg maniac').
card_rarity('mogg maniac'/'STH', 'Uncommon').
card_artist('mogg maniac'/'STH', 'Brian Snõddy').
card_flavor_text('mogg maniac'/'STH', 'Stand clear if he gets an itch.').
card_multiverse_id('mogg maniac'/'STH', '5120').

card_in_set('morgue thrull', 'STH').
card_original_type('morgue thrull'/'STH', 'Summon — Thrull').
card_original_text('morgue thrull'/'STH', 'Sacrifice Morgue Thrull: Put the top three cards of your library into your graveyard.').
card_first_print('morgue thrull', 'STH').
card_image_name('morgue thrull'/'STH', 'morgue thrull').
card_uid('morgue thrull'/'STH', 'STH:Morgue Thrull:morgue thrull').
card_rarity('morgue thrull'/'STH', 'Common').
card_artist('morgue thrull'/'STH', 'Robert Bliss').
card_flavor_text('morgue thrull'/'STH', 'It\'s so hard to find good help these days.').
card_multiverse_id('morgue thrull'/'STH', '5191').

card_in_set('mortuary', 'STH').
card_original_type('mortuary'/'STH', 'Enchantment').
card_original_text('mortuary'/'STH', 'Whenever any creature is put into your graveyard from play, put that creature on top of your library.').
card_first_print('mortuary', 'STH').
card_image_name('mortuary'/'STH', 'mortuary').
card_uid('mortuary'/'STH', 'STH:Mortuary:mortuary').
card_rarity('mortuary'/'STH', 'Rare').
card_artist('mortuary'/'STH', 'Robert Bliss').
card_flavor_text('mortuary'/'STH', '\"Think of them not as failures but as works in progress.\"\n—Volrath').
card_multiverse_id('mortuary'/'STH', '5192').

card_in_set('mox diamond', 'STH').
card_original_type('mox diamond'/'STH', 'Artifact').
card_original_text('mox diamond'/'STH', 'When Mox Diamond comes into play, choose and discard a land card or sacrifice Mox Diamond.\n{T}: Add one mana of any color to your mana pool. Play this ability as a mana source.').
card_first_print('mox diamond', 'STH').
card_image_name('mox diamond'/'STH', 'mox diamond').
card_uid('mox diamond'/'STH', 'STH:Mox Diamond:mox diamond').
card_rarity('mox diamond'/'STH', 'Rare').
card_artist('mox diamond'/'STH', 'Dan Frazier').
card_multiverse_id('mox diamond'/'STH', '5193').

card_in_set('mulch', 'STH').
card_original_type('mulch'/'STH', 'Sorcery').
card_original_text('mulch'/'STH', 'Reveal the top four cards of your library to all players. Put any of those cards that are lands into your hand and the rest into your graveyard.').
card_first_print('mulch', 'STH').
card_image_name('mulch'/'STH', 'mulch').
card_uid('mulch'/'STH', 'STH:Mulch:mulch').
card_rarity('mulch'/'STH', 'Common').
card_artist('mulch'/'STH', 'Rebecca Guay').
card_flavor_text('mulch'/'STH', 'Hope is the one crop that can grow in any climate.').
card_multiverse_id('mulch'/'STH', '5194').

card_in_set('nomads en-kor', 'STH').
card_original_type('nomads en-kor'/'STH', 'Summon — Soldiers').
card_original_text('nomads en-kor'/'STH', '{0}: Redirect 1 damage from Nomads \nen-Kor to a creature you control.').
card_first_print('nomads en-kor', 'STH').
card_image_name('nomads en-kor'/'STH', 'nomads en-kor').
card_uid('nomads en-kor'/'STH', 'STH:Nomads en-Kor:nomads en-kor').
card_rarity('nomads en-kor'/'STH', 'Common').
card_artist('nomads en-kor'/'STH', 'Val Mayerik').
card_flavor_text('nomads en-kor'/'STH', 'The Kor forsake roots for the winding of the path; forsake voices for the silence of the mind; forsake all else for the poverty of isolation.').
card_multiverse_id('nomads en-kor'/'STH', '5196').

card_in_set('overgrowth', 'STH').
card_original_type('overgrowth'/'STH', 'Enchant Land').
card_original_text('overgrowth'/'STH', 'Whenever enchanted land is tapped for mana, it produces an additional {G}{G}.').
card_first_print('overgrowth', 'STH').
card_image_name('overgrowth'/'STH', 'overgrowth').
card_uid('overgrowth'/'STH', 'STH:Overgrowth:overgrowth').
card_rarity('overgrowth'/'STH', 'Common').
card_artist('overgrowth'/'STH', 'Rob Alexander').
card_flavor_text('overgrowth'/'STH', 'Life needs no encouragement.').
card_multiverse_id('overgrowth'/'STH', '5198').

card_in_set('portcullis', 'STH').
card_original_type('portcullis'/'STH', 'Artifact').
card_original_text('portcullis'/'STH', 'Whenever any creature comes into play, if there are two or more other creatures in play, set that creature aside. If Portcullis leaves play, put the creature into play under its owner\'s control.').
card_first_print('portcullis', 'STH').
card_image_name('portcullis'/'STH', 'portcullis').
card_uid('portcullis'/'STH', 'STH:Portcullis:portcullis').
card_rarity('portcullis'/'STH', 'Rare').
card_artist('portcullis'/'STH', 'Kev Walker').
card_multiverse_id('portcullis'/'STH', '5202').

card_in_set('primal rage', 'STH').
card_original_type('primal rage'/'STH', 'Enchantment').
card_original_text('primal rage'/'STH', 'All creatures you control gain trample.').
card_first_print('primal rage', 'STH').
card_image_name('primal rage'/'STH', 'primal rage').
card_uid('primal rage'/'STH', 'STH:Primal Rage:primal rage').
card_rarity('primal rage'/'STH', 'Uncommon').
card_artist('primal rage'/'STH', 'Brian Snõddy').
card_flavor_text('primal rage'/'STH', '\"Charge!\" A great cry went out, and countless elves and Vec soldiers charged up the mountain. Their fury and passion hid the fact that they were horribly outnumbered.').
card_multiverse_id('primal rage'/'STH', '5149').

card_in_set('provoke', 'STH').
card_original_type('provoke'/'STH', 'Instant').
card_original_text('provoke'/'STH', 'Untap target creature you do not control. That creature blocks this turn if able.\nDraw a card.').
card_first_print('provoke', 'STH').
card_image_name('provoke'/'STH', 'provoke').
card_uid('provoke'/'STH', 'STH:Provoke:provoke').
card_rarity('provoke'/'STH', 'Common').
card_artist('provoke'/'STH', 'Terese Nielsen').
card_flavor_text('provoke'/'STH', 'Mirri did not have time to think, only to react.').
card_multiverse_id('provoke'/'STH', '5203').

card_in_set('pursuit of knowledge', 'STH').
card_original_type('pursuit of knowledge'/'STH', 'Enchantment').
card_original_text('pursuit of knowledge'/'STH', 'Skip drawing a card: Put a study counter on Pursuit of Knowledge.\nRemove three study counters from Pursuit of Knowledge, Sacrifice Pursuit of Knowledge: Draw seven cards.').
card_first_print('pursuit of knowledge', 'STH').
card_image_name('pursuit of knowledge'/'STH', 'pursuit of knowledge').
card_uid('pursuit of knowledge'/'STH', 'STH:Pursuit of Knowledge:pursuit of knowledge').
card_rarity('pursuit of knowledge'/'STH', 'Rare').
card_artist('pursuit of knowledge'/'STH', 'DiTerlizzi').
card_multiverse_id('pursuit of knowledge'/'STH', '5204').

card_in_set('rabid rats', 'STH').
card_original_type('rabid rats'/'STH', 'Summon — Rats').
card_original_text('rabid rats'/'STH', '{T}: Target blocking creature gets -1/-1 until end of turn.').
card_first_print('rabid rats', 'STH').
card_image_name('rabid rats'/'STH', 'rabid rats').
card_uid('rabid rats'/'STH', 'STH:Rabid Rats:rabid rats').
card_rarity('rabid rats'/'STH', 'Common').
card_artist('rabid rats'/'STH', 'Matthew D. Wilson').
card_flavor_text('rabid rats'/'STH', 'Every sentence in Volrath\'s prisons ends the same way.').
card_multiverse_id('rabid rats'/'STH', '5205').

card_in_set('ransack', 'STH').
card_original_type('ransack'/'STH', 'Sorcery').
card_original_text('ransack'/'STH', 'Look at the top five cards of target player\'s library. Put any number of those cards on the bottom of that library in any order and the rest on top of the library in any order.').
card_first_print('ransack', 'STH').
card_image_name('ransack'/'STH', 'ransack').
card_uid('ransack'/'STH', 'STH:Ransack:ransack').
card_rarity('ransack'/'STH', 'Uncommon').
card_artist('ransack'/'STH', 'Ron Spencer').
card_multiverse_id('ransack'/'STH', '5206').

card_in_set('rebound', 'STH').
card_original_type('rebound'/'STH', 'Interrupt').
card_original_text('rebound'/'STH', 'Target spell, which targets only a single player, targets another player of your choice instead.').
card_first_print('rebound', 'STH').
card_image_name('rebound'/'STH', 'rebound').
card_uid('rebound'/'STH', 'STH:Rebound:rebound').
card_rarity('rebound'/'STH', 'Uncommon').
card_artist('rebound'/'STH', 'Doug Chaffee').
card_flavor_text('rebound'/'STH', 'Insult, like an arrow, is beyond control once loosed.').
card_multiverse_id('rebound'/'STH', '5209').

card_in_set('reins of power', 'STH').
card_original_type('reins of power'/'STH', 'Instant').
card_original_text('reins of power'/'STH', 'You and target opponent each untap and gain control of all creatures the other controls until end of turn. Those creatures are unaffected by summoning sickness this turn.').
card_first_print('reins of power', 'STH').
card_image_name('reins of power'/'STH', 'reins of power').
card_uid('reins of power'/'STH', 'STH:Reins of Power:reins of power').
card_rarity('reins of power'/'STH', 'Rare').
card_artist('reins of power'/'STH', 'Colin MacNeil').
card_multiverse_id('reins of power'/'STH', '5210').

card_in_set('revenant', 'STH').
card_original_type('revenant'/'STH', 'Summon — Spirit').
card_original_text('revenant'/'STH', 'Flying\nRevenant has power and toughness each equal to the number of creature cards in your graveyard.').
card_image_name('revenant'/'STH', 'revenant').
card_uid('revenant'/'STH', 'STH:Revenant:revenant').
card_rarity('revenant'/'STH', 'Rare').
card_artist('revenant'/'STH', 'Terese Nielsen').
card_flavor_text('revenant'/'STH', '\"Not again.\"\n—Hans').
card_multiverse_id('revenant'/'STH', '5211').

card_in_set('rolling stones', 'STH').
card_original_type('rolling stones'/'STH', 'Enchantment').
card_original_text('rolling stones'/'STH', 'Walls can attack as though they were not Walls.').
card_first_print('rolling stones', 'STH').
card_image_name('rolling stones'/'STH', 'rolling stones').
card_uid('rolling stones'/'STH', 'STH:Rolling Stones:rolling stones').
card_rarity('rolling stones'/'STH', 'Rare').
card_artist('rolling stones'/'STH', 'John Matson').
card_flavor_text('rolling stones'/'STH', 'Walls collapse and kill people all the time. Some are just more aggressive about it.').
card_multiverse_id('rolling stones'/'STH', '5212').

card_in_set('ruination', 'STH').
card_original_type('ruination'/'STH', 'Sorcery').
card_original_text('ruination'/'STH', 'Destroy all nonbasic lands.').
card_first_print('ruination', 'STH').
card_image_name('ruination'/'STH', 'ruination').
card_uid('ruination'/'STH', 'STH:Ruination:ruination').
card_rarity('ruination'/'STH', 'Rare').
card_artist('ruination'/'STH', 'Dermot Power').
card_flavor_text('ruination'/'STH', '\"We have built a wall upon sand. The wall will vanish. The sand will remain.\"\n—Oracle en-Vec').
card_multiverse_id('ruination'/'STH', '5213').

card_in_set('sacred ground', 'STH').
card_original_type('sacred ground'/'STH', 'Enchantment').
card_original_text('sacred ground'/'STH', 'Whenever an effect controlled by any opponent puts a land into your graveyard from play, put that land into play.').
card_first_print('sacred ground', 'STH').
card_image_name('sacred ground'/'STH', 'sacred ground').
card_uid('sacred ground'/'STH', 'STH:Sacred Ground:sacred ground').
card_rarity('sacred ground'/'STH', 'Rare').
card_artist('sacred ground'/'STH', 'Terese Nielsen').
card_flavor_text('sacred ground'/'STH', '\"That which knows itself cannot be shaped to another\'s will.\"\n—Oracle en-Vec').
card_multiverse_id('sacred ground'/'STH', '5214').

card_in_set('samite blessing', 'STH').
card_original_type('samite blessing'/'STH', 'Enchant Creature').
card_original_text('samite blessing'/'STH', 'Enchanted creature gains \"{T}: Prevent all damage to any creature from any one source.\" (Treat further damage from that source normally.)').
card_first_print('samite blessing', 'STH').
card_image_name('samite blessing'/'STH', 'samite blessing').
card_uid('samite blessing'/'STH', 'STH:Samite Blessing:samite blessing').
card_rarity('samite blessing'/'STH', 'Common').
card_artist('samite blessing'/'STH', 'Rebecca Guay').
card_flavor_text('samite blessing'/'STH', 'Knit bone, darn flesh,\nStitch skin, weave breath.\n—Samite healing ritual').
card_multiverse_id('samite blessing'/'STH', '5215').

card_in_set('scapegoat', 'STH').
card_original_type('scapegoat'/'STH', 'Instant').
card_original_text('scapegoat'/'STH', 'Sacrifice a creature: Return any number of target creatures you control to owner\'s hand.').
card_first_print('scapegoat', 'STH').
card_image_name('scapegoat'/'STH', 'scapegoat').
card_uid('scapegoat'/'STH', 'STH:Scapegoat:scapegoat').
card_rarity('scapegoat'/'STH', 'Uncommon').
card_artist('scapegoat'/'STH', 'Daren Bader').
card_flavor_text('scapegoat'/'STH', '\"Enjoy your deception, Vuel,\" Gerrard said as Volrath\'s features melted from the dead shapeshifter. \"I\'ll count this one as practice.\"').
card_multiverse_id('scapegoat'/'STH', '5138').

card_in_set('seething anger', 'STH').
card_original_type('seething anger'/'STH', 'Sorcery').
card_original_text('seething anger'/'STH', 'Buyback {3} (You may pay an additional {3} when you play this spell. If you do, put it into your hand instead of your graveyard as part of the spell\'s effect.)\nTarget creature gets +3/+0 until end of turn.').
card_first_print('seething anger', 'STH').
card_image_name('seething anger'/'STH', 'seething anger').
card_uid('seething anger'/'STH', 'STH:Seething Anger:seething anger').
card_rarity('seething anger'/'STH', 'Common').
card_artist('seething anger'/'STH', 'Val Mayerik').
card_multiverse_id('seething anger'/'STH', '5218').

card_in_set('serpent warrior', 'STH').
card_original_type('serpent warrior'/'STH', 'Summon — Soldier').
card_original_text('serpent warrior'/'STH', 'When Serpent Warrior comes into play, lose 3 life.').
card_image_name('serpent warrior'/'STH', 'serpent warrior').
card_uid('serpent warrior'/'STH', 'STH:Serpent Warrior:serpent warrior').
card_rarity('serpent warrior'/'STH', 'Common').
card_artist('serpent warrior'/'STH', 'Ron Spencer').
card_flavor_text('serpent warrior'/'STH', 'A hiss before dying.').
card_multiverse_id('serpent warrior'/'STH', '5219').

card_in_set('shaman en-kor', 'STH').
card_original_type('shaman en-kor'/'STH', 'Summon — Cleric').
card_original_text('shaman en-kor'/'STH', '{0}: Redirect 1 damage from Shaman en-Kor to a creature you control.\n{1}{W} Redirect to Shaman en-Kor all damage dealt to any one creature from any one source.').
card_first_print('shaman en-kor', 'STH').
card_image_name('shaman en-kor'/'STH', 'shaman en-kor').
card_uid('shaman en-kor'/'STH', 'STH:Shaman en-Kor:shaman en-kor').
card_rarity('shaman en-kor'/'STH', 'Rare').
card_artist('shaman en-kor'/'STH', 'Jeff Miracola').
card_multiverse_id('shaman en-kor'/'STH', '5220').

card_in_set('shard phoenix', 'STH').
card_original_type('shard phoenix'/'STH', 'Summon — Phoenix').
card_original_text('shard phoenix'/'STH', 'Flying\n{R}{R}{R} Put Shard Phoenix into your hand. Use this ability only if Shard Phoenix is in your graveyard and only during your upkeep.\nSacrifice Shard Phoenix: Shard Phoenix deals 2 damage to each creature without flying.').
card_first_print('shard phoenix', 'STH').
card_image_name('shard phoenix'/'STH', 'shard phoenix').
card_uid('shard phoenix'/'STH', 'STH:Shard Phoenix:shard phoenix').
card_rarity('shard phoenix'/'STH', 'Rare').
card_artist('shard phoenix'/'STH', 'Paolo Parente').
card_multiverse_id('shard phoenix'/'STH', '5155').

card_in_set('shifting wall', 'STH').
card_original_type('shifting wall'/'STH', 'Artifact Creature').
card_original_text('shifting wall'/'STH', 'Shifting Wall counts as a Wall. (Walls cannot attack.)\nShifting Wall comes into play with X +1/+1 counters on it.').
card_first_print('shifting wall', 'STH').
card_image_name('shifting wall'/'STH', 'shifting wall').
card_uid('shifting wall'/'STH', 'STH:Shifting Wall:shifting wall').
card_rarity('shifting wall'/'STH', 'Uncommon').
card_artist('shifting wall'/'STH', 'Michael Sutfin').
card_flavor_text('shifting wall'/'STH', '\"You\'d almost think Volrath doesn\'t want me here.\"\n—Gerrard').
card_multiverse_id('shifting wall'/'STH', '5222').

card_in_set('shock', 'STH').
card_original_type('shock'/'STH', 'Instant').
card_original_text('shock'/'STH', 'Shock deals 2 damage to target creature or player.').
card_first_print('shock', 'STH').
card_image_name('shock'/'STH', 'shock').
card_uid('shock'/'STH', 'STH:Shock:shock').
card_rarity('shock'/'STH', 'Common').
card_artist('shock'/'STH', 'Randy Gallegos').
card_flavor_text('shock'/'STH', 'Lightning tethers souls to the world.\n—Kor saying').
card_multiverse_id('shock'/'STH', '5143').

card_in_set('sift', 'STH').
card_original_type('sift'/'STH', 'Sorcery').
card_original_text('sift'/'STH', 'Draw three cards, then choose and discard a card.').
card_first_print('sift', 'STH').
card_image_name('sift'/'STH', 'sift').
card_uid('sift'/'STH', 'STH:Sift:sift').
card_rarity('sift'/'STH', 'Common').
card_artist('sift'/'STH', 'Pete Venters').
card_flavor_text('sift'/'STH', '\"Twice I have let the Legacy slip away. Never again.\"\n—Karn, silver golem').
card_multiverse_id('sift'/'STH', '5223').

card_in_set('silver wyvern', 'STH').
card_original_type('silver wyvern'/'STH', 'Summon — Drake').
card_original_text('silver wyvern'/'STH', 'Flying\n{U} Target spell or ability, which targets only Silver Wyvern, targets another creature of your choice instead. Play this ability as an interrupt.').
card_first_print('silver wyvern', 'STH').
card_image_name('silver wyvern'/'STH', 'silver wyvern').
card_uid('silver wyvern'/'STH', 'STH:Silver Wyvern:silver wyvern').
card_rarity('silver wyvern'/'STH', 'Rare').
card_artist('silver wyvern'/'STH', 'Colin MacNeil').
card_multiverse_id('silver wyvern'/'STH', '5103').

card_in_set('skeleton scavengers', 'STH').
card_original_type('skeleton scavengers'/'STH', 'Summon — Skeletons').
card_original_text('skeleton scavengers'/'STH', 'Skeleton Scavengers comes into play with one +1/+1 counter on it.\nPay {1} for each +1/+1 counter on Skeleton Scavengers: Regenerate Skeleton Scavengers and put a +1/+1 counter on it.').
card_first_print('skeleton scavengers', 'STH').
card_image_name('skeleton scavengers'/'STH', 'skeleton scavengers').
card_uid('skeleton scavengers'/'STH', 'STH:Skeleton Scavengers:skeleton scavengers').
card_rarity('skeleton scavengers'/'STH', 'Rare').
card_artist('skeleton scavengers'/'STH', 'Brian Snõddy').
card_multiverse_id('skeleton scavengers'/'STH', '5226').

card_in_set('skyshroud archer', 'STH').
card_original_type('skyshroud archer'/'STH', 'Summon — Elf').
card_original_text('skyshroud archer'/'STH', '{T}: Target creature with flying gets -1/-1 until end of turn.').
card_first_print('skyshroud archer', 'STH').
card_image_name('skyshroud archer'/'STH', 'skyshroud archer').
card_uid('skyshroud archer'/'STH', 'STH:Skyshroud Archer:skyshroud archer').
card_rarity('skyshroud archer'/'STH', 'Common').
card_artist('skyshroud archer'/'STH', 'Jeff Miracola').
card_flavor_text('skyshroud archer'/'STH', 'In Rath, arrows are guided not by virtue but by necessity.').
card_multiverse_id('skyshroud archer'/'STH', '5228').

card_in_set('skyshroud falcon', 'STH').
card_original_type('skyshroud falcon'/'STH', 'Summon — Bird').
card_original_text('skyshroud falcon'/'STH', 'Flying\nAttacking does not cause Skyshroud Falcon to tap.').
card_first_print('skyshroud falcon', 'STH').
card_image_name('skyshroud falcon'/'STH', 'skyshroud falcon').
card_uid('skyshroud falcon'/'STH', 'STH:Skyshroud Falcon:skyshroud falcon').
card_rarity('skyshroud falcon'/'STH', 'Common').
card_artist('skyshroud falcon'/'STH', 'Mike Raabe').
card_flavor_text('skyshroud falcon'/'STH', 'The falcon slits the sky to let the light drip through.\n—Kor saying').
card_multiverse_id('skyshroud falcon'/'STH', '5224').

card_in_set('skyshroud troopers', 'STH').
card_original_type('skyshroud troopers'/'STH', 'Summon — Elves').
card_original_text('skyshroud troopers'/'STH', '{T}: Add {G} to your mana pool. Play this ability as a mana source.').
card_first_print('skyshroud troopers', 'STH').
card_image_name('skyshroud troopers'/'STH', 'skyshroud troopers').
card_uid('skyshroud troopers'/'STH', 'STH:Skyshroud Troopers:skyshroud troopers').
card_rarity('skyshroud troopers'/'STH', 'Common').
card_artist('skyshroud troopers'/'STH', 'DiTerlizzi').
card_flavor_text('skyshroud troopers'/'STH', '\"We were not made for war. Like flowstone, Volrath shaped us to it.\"\n—Eladamri, Lord of Leaves').
card_multiverse_id('skyshroud troopers'/'STH', '5230').

card_in_set('sliver queen', 'STH').
card_original_type('sliver queen'/'STH', 'Summon — Legend').
card_original_text('sliver queen'/'STH', 'Sliver Queen counts as a Sliver.\n{2}: Put a Sliver token into play. Treat this token as a 1/1 colorless creature.').
card_first_print('sliver queen', 'STH').
card_image_name('sliver queen'/'STH', 'sliver queen').
card_uid('sliver queen'/'STH', 'STH:Sliver Queen:sliver queen').
card_rarity('sliver queen'/'STH', 'Rare').
card_artist('sliver queen'/'STH', 'Ron Spencer').
card_flavor_text('sliver queen'/'STH', 'Her children are ever part of her.').
card_multiverse_id('sliver queen'/'STH', '5233').

card_in_set('smite', 'STH').
card_original_type('smite'/'STH', 'Instant').
card_original_text('smite'/'STH', 'Destroy target blocked creature.').
card_first_print('smite', 'STH').
card_image_name('smite'/'STH', 'smite').
card_uid('smite'/'STH', 'STH:Smite:smite').
card_rarity('smite'/'STH', 'Common').
card_artist('smite'/'STH', 'Daren Bader').
card_flavor_text('smite'/'STH', '\"You\'ve got your childhood wish at last. Now you get to die.\"\n—Gerrard, to Volrath').
card_multiverse_id('smite'/'STH', '5234').

card_in_set('soltari champion', 'STH').
card_original_type('soltari champion'/'STH', 'Summon — Soldier').
card_original_text('soltari champion'/'STH', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nIf Soltari Champion attacks, all other creatures you control get +1/+1 until end of turn.').
card_first_print('soltari champion', 'STH').
card_image_name('soltari champion'/'STH', 'soltari champion').
card_uid('soltari champion'/'STH', 'STH:Soltari Champion:soltari champion').
card_rarity('soltari champion'/'STH', 'Rare').
card_artist('soltari champion'/'STH', 'Adam Rex').
card_multiverse_id('soltari champion'/'STH', '5235').

card_in_set('spike breeder', 'STH').
card_original_type('spike breeder'/'STH', 'Summon — Spike').
card_original_text('spike breeder'/'STH', 'Spike Breeder comes into play with three +1/+1 counters on it.\n{2}, Remove a +1/+1 counter from Spike Breeder: Put a +1/+1 counter on target creature.\n{2}, Remove a +1/+1 counter from Spike Breeder: Put a Spike token into play. Treat this token as a 1/1 green creature.').
card_first_print('spike breeder', 'STH').
card_image_name('spike breeder'/'STH', 'spike breeder').
card_uid('spike breeder'/'STH', 'STH:Spike Breeder:spike breeder').
card_rarity('spike breeder'/'STH', 'Rare').
card_artist('spike breeder'/'STH', 'Adam Rex').
card_multiverse_id('spike breeder'/'STH', '5237').

card_in_set('spike colony', 'STH').
card_original_type('spike colony'/'STH', 'Summon — Spike').
card_original_text('spike colony'/'STH', 'Spike Colony comes into play with four +1/+1 counters on it.\n{2}, Remove a +1/+1 counter from Spike Colony: Put a +1/+1 counter on target creature.').
card_first_print('spike colony', 'STH').
card_image_name('spike colony'/'STH', 'spike colony').
card_uid('spike colony'/'STH', 'STH:Spike Colony:spike colony').
card_rarity('spike colony'/'STH', 'Common').
card_artist('spike colony'/'STH', 'Douglas Shuler').
card_multiverse_id('spike colony'/'STH', '5238').

card_in_set('spike feeder', 'STH').
card_original_type('spike feeder'/'STH', 'Summon — Spike').
card_original_text('spike feeder'/'STH', 'Spike Feeder comes into play with two +1/+1 counters on it.\n{2}, Remove a +1/+1 counter from Spike Feeder: Put a +1/+1 counter on target creature.\nRemove a +1/+1 counter from Spike Feeder: Gain 2 life.').
card_first_print('spike feeder', 'STH').
card_image_name('spike feeder'/'STH', 'spike feeder').
card_uid('spike feeder'/'STH', 'STH:Spike Feeder:spike feeder').
card_rarity('spike feeder'/'STH', 'Uncommon').
card_artist('spike feeder'/'STH', 'Heather Hudson').
card_multiverse_id('spike feeder'/'STH', '5239').

card_in_set('spike soldier', 'STH').
card_original_type('spike soldier'/'STH', 'Summon — Spike').
card_original_text('spike soldier'/'STH', 'Spike Soldier comes into play with three +1/+1 counters on it.\n{2}, Remove a +1/+1 counter from Spike Soldier: Put a +1/+1 counter on target creature.\nRemove a +1/+1 counter from Spike Soldier: Spike Soldier gets +2/+2 until end of turn.').
card_first_print('spike soldier', 'STH').
card_image_name('spike soldier'/'STH', 'spike soldier').
card_uid('spike soldier'/'STH', 'STH:Spike Soldier:spike soldier').
card_rarity('spike soldier'/'STH', 'Uncommon').
card_artist('spike soldier'/'STH', 'Randy Elliott').
card_multiverse_id('spike soldier'/'STH', '5241').

card_in_set('spike worker', 'STH').
card_original_type('spike worker'/'STH', 'Summon — Spike').
card_original_text('spike worker'/'STH', 'Spike Worker comes into play with two +1/+1 counters on it.\n{2}, Remove a +1/+1 counter from Spike Worker: Put a +1/+1 counter on target creature.').
card_first_print('spike worker', 'STH').
card_image_name('spike worker'/'STH', 'spike worker').
card_uid('spike worker'/'STH', 'STH:Spike Worker:spike worker').
card_rarity('spike worker'/'STH', 'Common').
card_artist('spike worker'/'STH', 'Daniel Gelon').
card_multiverse_id('spike worker'/'STH', '5242').

card_in_set('spindrift drake', 'STH').
card_original_type('spindrift drake'/'STH', 'Summon — Drake').
card_original_text('spindrift drake'/'STH', 'Flying\nDuring your upkeep, pay {U} or sacrifice Spindrift Drake.').
card_first_print('spindrift drake', 'STH').
card_image_name('spindrift drake'/'STH', 'spindrift drake').
card_uid('spindrift drake'/'STH', 'STH:Spindrift Drake:spindrift drake').
card_rarity('spindrift drake'/'STH', 'Common').
card_artist('spindrift drake'/'STH', 'Anthony S. Waters').
card_flavor_text('spindrift drake'/'STH', 'Sea brine for blood, washing foam for laughter.').
card_multiverse_id('spindrift drake'/'STH', '5243').

card_in_set('spined sliver', 'STH').
card_original_type('spined sliver'/'STH', 'Summon — Sliver').
card_original_text('spined sliver'/'STH', 'If any Sliver is blocked, it gets +1/+1 until end of turn for each creature blocking it.').
card_first_print('spined sliver', 'STH').
card_image_name('spined sliver'/'STH', 'spined sliver').
card_uid('spined sliver'/'STH', 'STH:Spined Sliver:spined sliver').
card_rarity('spined sliver'/'STH', 'Uncommon').
card_artist('spined sliver'/'STH', 'Ron Spencer').
card_flavor_text('spined sliver'/'STH', '\"Slivers are evil and slivers are sly;\nAnd if you get eaten, then no one will cry.\"\n—Mogg children\'s rhyme').
card_multiverse_id('spined sliver'/'STH', '5190').

card_in_set('spined wurm', 'STH').
card_original_type('spined wurm'/'STH', 'Summon — Wurm').
card_original_text('spined wurm'/'STH', '').
card_image_name('spined wurm'/'STH', 'spined wurm').
card_uid('spined wurm'/'STH', 'STH:Spined Wurm:spined wurm').
card_rarity('spined wurm'/'STH', 'Common').
card_artist('spined wurm'/'STH', 'Keith Parkinson').
card_flavor_text('spined wurm'/'STH', '\"As it moved, the wurm\'s spines gathered up bits of flowstone, which took the shapes of dead villagers\' heads. Each head spoke a single sound, but if taken together, they said, ‘Alas for the living.\'\"\n—Dal myth of the wurm').
card_multiverse_id('spined wurm'/'STH', '5245').

card_in_set('spirit en-kor', 'STH').
card_original_type('spirit en-kor'/'STH', 'Summon — Spirit').
card_original_text('spirit en-kor'/'STH', 'Flying\n{0}: Redirect 1 damage from Spirit en-Kor to a creature you control.').
card_first_print('spirit en-kor', 'STH').
card_image_name('spirit en-kor'/'STH', 'spirit en-kor').
card_uid('spirit en-kor'/'STH', 'STH:Spirit en-Kor:spirit en-kor').
card_rarity('spirit en-kor'/'STH', 'Common').
card_artist('spirit en-kor'/'STH', 'John Matson').
card_flavor_text('spirit en-kor'/'STH', 'Death free throat from thirst, mouth from speech, feet from earth.\n—Kor requiem').
card_multiverse_id('spirit en-kor'/'STH', '5236').

card_in_set('spitting hydra', 'STH').
card_original_type('spitting hydra'/'STH', 'Summon — Hydra').
card_original_text('spitting hydra'/'STH', 'Spitting Hydra comes into play with four +1/+1 counters on it.\n{1}{R}, Remove a +1/+1 counter from Spitting Hydra: Spitting Hydra deals 1 damage to target creature.').
card_first_print('spitting hydra', 'STH').
card_image_name('spitting hydra'/'STH', 'spitting hydra').
card_uid('spitting hydra'/'STH', 'STH:Spitting Hydra:spitting hydra').
card_rarity('spitting hydra'/'STH', 'Rare').
card_artist('spitting hydra'/'STH', 'Daren Bader').
card_multiverse_id('spitting hydra'/'STH', '5246').

card_in_set('stronghold assassin', 'STH').
card_original_type('stronghold assassin'/'STH', 'Summon — Assassin').
card_original_text('stronghold assassin'/'STH', '{T}, Sacrifice a creature: Destroy target nonblack creature.').
card_first_print('stronghold assassin', 'STH').
card_image_name('stronghold assassin'/'STH', 'stronghold assassin').
card_uid('stronghold assassin'/'STH', 'STH:Stronghold Assassin:stronghold assassin').
card_rarity('stronghold assassin'/'STH', 'Rare').
card_artist('stronghold assassin'/'STH', 'Matthew D. Wilson').
card_flavor_text('stronghold assassin'/'STH', 'The assassin sees only throats and hears only heartbeats.').
card_multiverse_id('stronghold assassin'/'STH', '5247').

card_in_set('stronghold taskmaster', 'STH').
card_original_type('stronghold taskmaster'/'STH', 'Summon — Minion').
card_original_text('stronghold taskmaster'/'STH', 'All other black creatures get -1/-1.').
card_first_print('stronghold taskmaster', 'STH').
card_image_name('stronghold taskmaster'/'STH', 'stronghold taskmaster').
card_uid('stronghold taskmaster'/'STH', 'STH:Stronghold Taskmaster:stronghold taskmaster').
card_rarity('stronghold taskmaster'/'STH', 'Uncommon').
card_artist('stronghold taskmaster'/'STH', 'Brom').
card_flavor_text('stronghold taskmaster'/'STH', '\"With the completion of each joyous task, we are closer to Yawgmoth\'s divine vision.\"\n—Stronghold architect, journal').
card_multiverse_id('stronghold taskmaster'/'STH', '5249').

card_in_set('sword of the chosen', 'STH').
card_original_type('sword of the chosen'/'STH', 'Legendary Artifact').
card_original_text('sword of the chosen'/'STH', '{T}: Target legend gets +2/+2 until end of turn.').
card_first_print('sword of the chosen', 'STH').
card_image_name('sword of the chosen'/'STH', 'sword of the chosen').
card_uid('sword of the chosen'/'STH', 'STH:Sword of the Chosen:sword of the chosen').
card_rarity('sword of the chosen'/'STH', 'Rare').
card_artist('sword of the chosen'/'STH', 'Adam Rex').
card_flavor_text('sword of the chosen'/'STH', 'One shard of Selenia yet remained.').
card_multiverse_id('sword of the chosen'/'STH', '5250').

card_in_set('temper', 'STH').
card_original_type('temper'/'STH', 'Instant').
card_original_text('temper'/'STH', 'Prevent up to X damage to target creature. For each 1 damage prevented in this way, put a +1/+1 counter on that creature.').
card_first_print('temper', 'STH').
card_image_name('temper'/'STH', 'temper').
card_uid('temper'/'STH', 'STH:Temper:temper').
card_rarity('temper'/'STH', 'Uncommon').
card_artist('temper'/'STH', 'Matthew D. Wilson').
card_flavor_text('temper'/'STH', '\"I don\'t need a plan, just a goal. The rest will follow on its own.\"\n—Gerrard').
card_multiverse_id('temper'/'STH', '5251').

card_in_set('tempting licid', 'STH').
card_original_type('tempting licid'/'STH', 'Summon — Licid').
card_original_text('tempting licid'/'STH', '{G}, {T}: Tempting Licid loses ability and becomes a creature enchantment that reads \"All creatures able to block enchanted creature do so\" instead of a creature. Move Tempting Licid onto target creature. You may pay {G} to end this effect.').
card_first_print('tempting licid', 'STH').
card_image_name('tempting licid'/'STH', 'tempting licid').
card_uid('tempting licid'/'STH', 'STH:Tempting Licid:tempting licid').
card_rarity('tempting licid'/'STH', 'Uncommon').
card_artist('tempting licid'/'STH', 'Randy Gallegos').
card_multiverse_id('tempting licid'/'STH', '5252').

card_in_set('thalakos deceiver', 'STH').
card_original_type('thalakos deceiver'/'STH', 'Summon — Wizard').
card_original_text('thalakos deceiver'/'STH', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nSacrifice Thalakos Deceiver: Gain control of target creature permanently. Use this ability only if Thalakos Deceiver is attacking and unblocked.').
card_first_print('thalakos deceiver', 'STH').
card_image_name('thalakos deceiver'/'STH', 'thalakos deceiver').
card_uid('thalakos deceiver'/'STH', 'STH:Thalakos Deceiver:thalakos deceiver').
card_rarity('thalakos deceiver'/'STH', 'Rare').
card_artist('thalakos deceiver'/'STH', 'Andrew Robinson').
card_multiverse_id('thalakos deceiver'/'STH', '5253').

card_in_set('tidal surge', 'STH').
card_original_type('tidal surge'/'STH', 'Sorcery').
card_original_text('tidal surge'/'STH', 'Tap up to three target creatures without flying.').
card_image_name('tidal surge'/'STH', 'tidal surge').
card_uid('tidal surge'/'STH', 'STH:Tidal Surge:tidal surge').
card_rarity('tidal surge'/'STH', 'Common').
card_artist('tidal surge'/'STH', 'Doug Chaffee').
card_flavor_text('tidal surge'/'STH', '\"It is nature that gives us our boundaries and nature that enforces them.\"\n—Eladamri, Lord of Leaves').
card_multiverse_id('tidal surge'/'STH', '5255').

card_in_set('tidal warrior', 'STH').
card_original_type('tidal warrior'/'STH', 'Summon — Merfolk').
card_original_text('tidal warrior'/'STH', '{T}: Target land is an island until end of turn.').
card_first_print('tidal warrior', 'STH').
card_image_name('tidal warrior'/'STH', 'tidal warrior').
card_uid('tidal warrior'/'STH', 'STH:Tidal Warrior:tidal warrior').
card_rarity('tidal warrior'/'STH', 'Common').
card_artist('tidal warrior'/'STH', 'Daren Bader').
card_flavor_text('tidal warrior'/'STH', 'The tide of battle favors those who can swim.').
card_multiverse_id('tidal warrior'/'STH', '5256').

card_in_set('torment', 'STH').
card_original_type('torment'/'STH', 'Enchant Creature').
card_original_text('torment'/'STH', 'Enchanted creature gets -3/-0.').
card_first_print('torment', 'STH').
card_image_name('torment'/'STH', 'torment').
card_uid('torment'/'STH', 'STH:Torment:torment').
card_rarity('torment'/'STH', 'Common').
card_artist('torment'/'STH', 'Paolo Parente').
card_flavor_text('torment'/'STH', '\"Volrath has killed me. All that remains of me is the scar!\"\n—Tahngarth').
card_multiverse_id('torment'/'STH', '5257').

card_in_set('tortured existence', 'STH').
card_original_type('tortured existence'/'STH', 'Enchantment').
card_original_text('tortured existence'/'STH', '{B}, Choose and discard a creature card: Return target creature card from your graveyard to your hand.').
card_first_print('tortured existence', 'STH').
card_image_name('tortured existence'/'STH', 'tortured existence').
card_uid('tortured existence'/'STH', 'STH:Tortured Existence:tortured existence').
card_rarity('tortured existence'/'STH', 'Common').
card_artist('tortured existence'/'STH', 'Keith Parkinson').
card_flavor_text('tortured existence'/'STH', '\"There are terrors lurking in the unseen corners of us all.\"\n—Crovax').
card_multiverse_id('tortured existence'/'STH', '5195').

card_in_set('venerable monk', 'STH').
card_original_type('venerable monk'/'STH', 'Summon — Cleric').
card_original_text('venerable monk'/'STH', 'When Venerable Monk comes into play, gain 2 life.').
card_image_name('venerable monk'/'STH', 'venerable monk').
card_uid('venerable monk'/'STH', 'STH:Venerable Monk:venerable monk').
card_rarity('venerable monk'/'STH', 'Common').
card_artist('venerable monk'/'STH', 'Terese Nielsen').
card_flavor_text('venerable monk'/'STH', 'Age wears the flesh but galvanizes the soul.').
card_multiverse_id('venerable monk'/'STH', '5258').

card_in_set('verdant touch', 'STH').
card_original_type('verdant touch'/'STH', 'Sorcery').
card_original_text('verdant touch'/'STH', 'Buyback {3} (You may pay an additional {3} when you play this spell. If you do, put it into your hand instead of your graveyard as part of the spell\'s effect.)\nTarget land becomes a 2/2 creature permanently. (This creature still counts as a land.)').
card_first_print('verdant touch', 'STH').
card_image_name('verdant touch'/'STH', 'verdant touch').
card_uid('verdant touch'/'STH', 'STH:Verdant Touch:verdant touch').
card_rarity('verdant touch'/'STH', 'Rare').
card_artist('verdant touch'/'STH', 'M. W. Kaluta & DiTerlizzi').
card_multiverse_id('verdant touch'/'STH', '5259').

card_in_set('victual sliver', 'STH').
card_original_type('victual sliver'/'STH', 'Summon — Sliver').
card_original_text('victual sliver'/'STH', 'Each Sliver gains \"{2}, Sacrifice this creature: Gain 4 life.\"').
card_first_print('victual sliver', 'STH').
card_image_name('victual sliver'/'STH', 'victual sliver').
card_uid('victual sliver'/'STH', 'STH:Victual Sliver:victual sliver').
card_rarity('victual sliver'/'STH', 'Uncommon').
card_artist('victual sliver'/'STH', 'Terese Nielsen').
card_flavor_text('victual sliver'/'STH', '\"We are kinfolk,\" explained Karn to the sliver queen. \"Just as you need your progeny to complete you, so do I need the pieces of the Legacy to make me whole.\"').
card_multiverse_id('victual sliver'/'STH', '5197').

card_in_set('volrath\'s gardens', 'STH').
card_original_type('volrath\'s gardens'/'STH', 'Enchantment').
card_original_text('volrath\'s gardens'/'STH', '{2}, Tap a creature you control: Gain 2 life. Play this ability as a sorcery.').
card_first_print('volrath\'s gardens', 'STH').
card_image_name('volrath\'s gardens'/'STH', 'volrath\'s gardens').
card_uid('volrath\'s gardens'/'STH', 'STH:Volrath\'s Gardens:volrath\'s gardens').
card_rarity('volrath\'s gardens'/'STH', 'Rare').
card_artist('volrath\'s gardens'/'STH', 'Rob Alexander').
card_flavor_text('volrath\'s gardens'/'STH', '\"Watch yourself,\" warned Starke. \"These plants aren\'t nice when they\'re hungry.\"').
card_multiverse_id('volrath\'s gardens'/'STH', '5260').

card_in_set('volrath\'s laboratory', 'STH').
card_original_type('volrath\'s laboratory'/'STH', 'Artifact').
card_original_text('volrath\'s laboratory'/'STH', 'When you play Volrath\'s Laboratory, choose a color and a creature type.\n{5}, {T}: Put a token creature into play. Treat this token as a 2/2 creature of the chosen color and creature type.').
card_first_print('volrath\'s laboratory', 'STH').
card_image_name('volrath\'s laboratory'/'STH', 'volrath\'s laboratory').
card_uid('volrath\'s laboratory'/'STH', 'STH:Volrath\'s Laboratory:volrath\'s laboratory').
card_rarity('volrath\'s laboratory'/'STH', 'Rare').
card_artist('volrath\'s laboratory'/'STH', 'Brom').
card_multiverse_id('volrath\'s laboratory'/'STH', '5261').

card_in_set('volrath\'s shapeshifter', 'STH').
card_original_type('volrath\'s shapeshifter'/'STH', 'Summon — Shapeshifter').
card_original_text('volrath\'s shapeshifter'/'STH', 'As long as the top card of your graveyard is a creature card, Volrath\'s Shapeshifter is a copy of that card, except that Volrath\'s Shapeshifter retains its abilities.\n{2}: Choose and discard a card.').
card_first_print('volrath\'s shapeshifter', 'STH').
card_image_name('volrath\'s shapeshifter'/'STH', 'volrath\'s shapeshifter').
card_uid('volrath\'s shapeshifter'/'STH', 'STH:Volrath\'s Shapeshifter:volrath\'s shapeshifter').
card_rarity('volrath\'s shapeshifter'/'STH', 'Rare').
card_artist('volrath\'s shapeshifter'/'STH', 'Ron Spencer').
card_multiverse_id('volrath\'s shapeshifter'/'STH', '5262').

card_in_set('volrath\'s stronghold', 'STH').
card_original_type('volrath\'s stronghold'/'STH', 'Legendary Land').
card_original_text('volrath\'s stronghold'/'STH', '{T}: Add one colorless mana to your mana pool.\n{1}{B}, {T}: Put target creature card from your graveyard on top of your library.').
card_first_print('volrath\'s stronghold', 'STH').
card_image_name('volrath\'s stronghold'/'STH', 'volrath\'s stronghold').
card_uid('volrath\'s stronghold'/'STH', 'STH:Volrath\'s Stronghold:volrath\'s stronghold').
card_rarity('volrath\'s stronghold'/'STH', 'Rare').
card_artist('volrath\'s stronghold'/'STH', 'Kev Walker').
card_flavor_text('volrath\'s stronghold'/'STH', 'The seed of a world\'s evil.').
card_multiverse_id('volrath\'s stronghold'/'STH', '5263').

card_in_set('walking dream', 'STH').
card_original_type('walking dream'/'STH', 'Summon — Illusion').
card_original_text('walking dream'/'STH', 'Walking Dream is unblockable.\nWalking Dream does not untap during your untap phase if any opponent controls two or more creatures.').
card_first_print('walking dream', 'STH').
card_image_name('walking dream'/'STH', 'walking dream').
card_uid('walking dream'/'STH', 'STH:Walking Dream:walking dream').
card_rarity('walking dream'/'STH', 'Uncommon').
card_artist('walking dream'/'STH', 'Richard Kane Ferguson').
card_flavor_text('walking dream'/'STH', 'Dreams, by definition, live shorter lives than those who dream them.').
card_multiverse_id('walking dream'/'STH', '5264').

card_in_set('wall of blossoms', 'STH').
card_original_type('wall of blossoms'/'STH', 'Summon — Wall').
card_original_text('wall of blossoms'/'STH', '(Walls cannot attack.)\nWhen Wall of Blossoms comes into play, draw a card.').
card_first_print('wall of blossoms', 'STH').
card_image_name('wall of blossoms'/'STH', 'wall of blossoms').
card_uid('wall of blossoms'/'STH', 'STH:Wall of Blossoms:wall of blossoms').
card_rarity('wall of blossoms'/'STH', 'Uncommon').
card_artist('wall of blossoms'/'STH', 'Heather Hudson').
card_flavor_text('wall of blossoms'/'STH', 'Each flower identical, every leaf and petal disturbingly exact.').
card_multiverse_id('wall of blossoms'/'STH', '5265').

card_in_set('wall of essence', 'STH').
card_original_type('wall of essence'/'STH', 'Summon — Wall').
card_original_text('wall of essence'/'STH', '(Walls cannot attack.)\nFor each 1 combat damage dealt to Wall of Essence, gain 1 life.').
card_first_print('wall of essence', 'STH').
card_image_name('wall of essence'/'STH', 'wall of essence').
card_uid('wall of essence'/'STH', 'STH:Wall of Essence:wall of essence').
card_rarity('wall of essence'/'STH', 'Uncommon').
card_artist('wall of essence'/'STH', 'Adam Rex').
card_flavor_text('wall of essence'/'STH', 'The ceiling and the floor fell in love, but only the wall knew.\n—Dal saying').
card_multiverse_id('wall of essence'/'STH', '5267').

card_in_set('wall of razors', 'STH').
card_original_type('wall of razors'/'STH', 'Summon — Wall').
card_original_text('wall of razors'/'STH', '(Walls cannot attack.)\nFirst strike').
card_first_print('wall of razors', 'STH').
card_image_name('wall of razors'/'STH', 'wall of razors').
card_uid('wall of razors'/'STH', 'STH:Wall of Razors:wall of razors').
card_rarity('wall of razors'/'STH', 'Uncommon').
card_artist('wall of razors'/'STH', 'Michael Sutfin').
card_flavor_text('wall of razors'/'STH', '\"In this blessed structure let the very walls baptize themselves in the blood of intruders.\"\n—Stronghold architect, journal').
card_multiverse_id('wall of razors'/'STH', '5268').

card_in_set('wall of souls', 'STH').
card_original_type('wall of souls'/'STH', 'Summon — Wall').
card_original_text('wall of souls'/'STH', '(Walls cannot attack.)\nWhenever Wall of Souls is dealt combat damage, it deals an equal amount of damage to target opponent.').
card_first_print('wall of souls', 'STH').
card_image_name('wall of souls'/'STH', 'wall of souls').
card_uid('wall of souls'/'STH', 'STH:Wall of Souls:wall of souls').
card_rarity('wall of souls'/'STH', 'Uncommon').
card_artist('wall of souls'/'STH', 'John Matson').
card_flavor_text('wall of souls'/'STH', '\"It is the nature of evil to turn you against yourself.\"\n—Starke').
card_multiverse_id('wall of souls'/'STH', '5269').

card_in_set('wall of tears', 'STH').
card_original_type('wall of tears'/'STH', 'Summon — Wall').
card_original_text('wall of tears'/'STH', '(Walls cannot attack.)\nIf Wall of Tears blocks any creatures, return each of those creatures to owner\'s hand at end of combat.').
card_first_print('wall of tears', 'STH').
card_image_name('wall of tears'/'STH', 'wall of tears').
card_uid('wall of tears'/'STH', 'STH:Wall of Tears:wall of tears').
card_rarity('wall of tears'/'STH', 'Uncommon').
card_artist('wall of tears'/'STH', 'Rebecca Guay').
card_flavor_text('wall of tears'/'STH', '\"Many have been lost to pity.\"\n—Karn, silver golem').
card_multiverse_id('wall of tears'/'STH', '5270').

card_in_set('warrior angel', 'STH').
card_original_type('warrior angel'/'STH', 'Summon — Angel').
card_original_text('warrior angel'/'STH', 'Flying\nFor each 1 damage Warrior Angel deals, gain 1 life.').
card_first_print('warrior angel', 'STH').
card_image_name('warrior angel'/'STH', 'warrior angel').
card_uid('warrior angel'/'STH', 'STH:Warrior Angel:warrior angel').
card_rarity('warrior angel'/'STH', 'Rare').
card_artist('warrior angel'/'STH', 'Brom').
card_flavor_text('warrior angel'/'STH', 'She represents not hope, but hope\'s desperate fury.').
card_multiverse_id('warrior angel'/'STH', '5271').

card_in_set('warrior en-kor', 'STH').
card_original_type('warrior en-kor'/'STH', 'Summon — Knight').
card_original_text('warrior en-kor'/'STH', '{0}: Redirect 1 damage from Warrior en-Kor to a creature you control.').
card_first_print('warrior en-kor', 'STH').
card_image_name('warrior en-kor'/'STH', 'warrior en-kor').
card_uid('warrior en-kor'/'STH', 'STH:Warrior en-Kor:warrior en-kor').
card_rarity('warrior en-kor'/'STH', 'Uncommon').
card_artist('warrior en-kor'/'STH', 'Stephen Daniele').
card_flavor_text('warrior en-kor'/'STH', 'Only a matter as vital as destroying Volrath could bring together the reclusive Kor people.').
card_multiverse_id('warrior en-kor'/'STH', '5272').

card_in_set('youthful knight', 'STH').
card_original_type('youthful knight'/'STH', 'Summon — Knight').
card_original_text('youthful knight'/'STH', 'First strike').
card_first_print('youthful knight', 'STH').
card_image_name('youthful knight'/'STH', 'youthful knight').
card_uid('youthful knight'/'STH', 'STH:Youthful Knight:youthful knight').
card_rarity('youthful knight'/'STH', 'Common').
card_artist('youthful knight'/'STH', 'Rebecca Guay').
card_flavor_text('youthful knight'/'STH', '\"Let no child be without a sword. We will all fight, for if we fail, we will certainly all die.\"\n—Oracle en-Vec').
card_multiverse_id('youthful knight'/'STH', '5274').
