% Zendikar

set('ZEN').
set_name('ZEN', 'Zendikar').
set_release_date('ZEN', '2009-10-02').
set_border('ZEN', 'black').
set_type('ZEN', 'expansion').
set_block('ZEN', 'Zendikar').

card_in_set('adventuring gear', 'ZEN').
card_original_type('adventuring gear'/'ZEN', 'Artifact — Equipment').
card_original_text('adventuring gear'/'ZEN', 'Landfall — Whenever a land enters the battlefield under your control, equipped creature gets +2/+2 until end of turn.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('adventuring gear', 'ZEN').
card_image_name('adventuring gear'/'ZEN', 'adventuring gear').
card_uid('adventuring gear'/'ZEN', 'ZEN:Adventuring Gear:adventuring gear').
card_rarity('adventuring gear'/'ZEN', 'Common').
card_artist('adventuring gear'/'ZEN', 'Howard Lyon').
card_number('adventuring gear'/'ZEN', '195').
card_flavor_text('adventuring gear'/'ZEN', 'An explorer\'s essentials in a wild world.').
card_multiverse_id('adventuring gear'/'ZEN', '178135').

card_in_set('æther figment', 'ZEN').
card_original_type('æther figment'/'ZEN', 'Creature — Illusion').
card_original_text('æther figment'/'ZEN', 'Kicker {3} (You may pay an additional {3} as you cast this spell.)\nÆther Figment is unblockable.\nIf Æther Figment was kicked, it enters the battlefield with two +1/+1 counters on it.').
card_first_print('æther figment', 'ZEN').
card_image_name('æther figment'/'ZEN', 'aether figment').
card_uid('æther figment'/'ZEN', 'ZEN:Æther Figment:aether figment').
card_rarity('æther figment'/'ZEN', 'Uncommon').
card_artist('æther figment'/'ZEN', 'Thomas M. Baxa').
card_number('æther figment'/'ZEN', '40').
card_multiverse_id('æther figment'/'ZEN', '170993').

card_in_set('akoum refuge', 'ZEN').
card_original_type('akoum refuge'/'ZEN', 'Land').
card_original_text('akoum refuge'/'ZEN', 'Akoum Refuge enters the battlefield tapped.\nWhen Akoum Refuge enters the battlefield, you gain 1 life.\n{T}: Add {B} or {R} to your mana pool.').
card_first_print('akoum refuge', 'ZEN').
card_image_name('akoum refuge'/'ZEN', 'akoum refuge').
card_uid('akoum refuge'/'ZEN', 'ZEN:Akoum Refuge:akoum refuge').
card_rarity('akoum refuge'/'ZEN', 'Uncommon').
card_artist('akoum refuge'/'ZEN', 'Fred Fields').
card_number('akoum refuge'/'ZEN', '210').
card_multiverse_id('akoum refuge'/'ZEN', '189638').

card_in_set('archive trap', 'ZEN').
card_original_type('archive trap'/'ZEN', 'Instant — Trap').
card_original_text('archive trap'/'ZEN', 'If an opponent searched his or her library this turn, you may pay {0} rather than pay Archive Trap\'s mana cost.\nTarget opponent puts the top thirteen cards of his or her library into his or her graveyard.').
card_first_print('archive trap', 'ZEN').
card_image_name('archive trap'/'ZEN', 'archive trap').
card_uid('archive trap'/'ZEN', 'ZEN:Archive Trap:archive trap').
card_rarity('archive trap'/'ZEN', 'Rare').
card_artist('archive trap'/'ZEN', 'Jason Felix').
card_number('archive trap'/'ZEN', '41').
card_multiverse_id('archive trap'/'ZEN', '197538').

card_in_set('archmage ascension', 'ZEN').
card_original_type('archmage ascension'/'ZEN', 'Enchantment').
card_original_text('archmage ascension'/'ZEN', 'At the beginning of each end step, if you drew two or more cards this turn, you may put a quest counter on Archmage Ascension.\nAs long as Archmage Ascension has six or more quest counters on it, if you would draw a card, you may instead search your library for a card, put that card into your hand, then shuffle your library.').
card_first_print('archmage ascension', 'ZEN').
card_image_name('archmage ascension'/'ZEN', 'archmage ascension').
card_uid('archmage ascension'/'ZEN', 'ZEN:Archmage Ascension:archmage ascension').
card_rarity('archmage ascension'/'ZEN', 'Rare').
card_artist('archmage ascension'/'ZEN', 'Christopher Moeller').
card_number('archmage ascension'/'ZEN', '42').
card_multiverse_id('archmage ascension'/'ZEN', '197893').

card_in_set('arid mesa', 'ZEN').
card_original_type('arid mesa'/'ZEN', 'Land').
card_original_text('arid mesa'/'ZEN', '{T}, Pay 1 life, Sacrifice Arid Mesa: Search your library for a Mountain or Plains card and put it onto the battlefield. Then shuffle your library.').
card_first_print('arid mesa', 'ZEN').
card_image_name('arid mesa'/'ZEN', 'arid mesa').
card_uid('arid mesa'/'ZEN', 'ZEN:Arid Mesa:arid mesa').
card_rarity('arid mesa'/'ZEN', 'Rare').
card_artist('arid mesa'/'ZEN', 'Raymond Swanland').
card_number('arid mesa'/'ZEN', '211').
card_multiverse_id('arid mesa'/'ZEN', '177584').

card_in_set('armament master', 'ZEN').
card_original_type('armament master'/'ZEN', 'Creature — Kor Soldier').
card_original_text('armament master'/'ZEN', 'Other Kor creatures you control get +2/+2 for each Equipment attached to Armament Master.').
card_first_print('armament master', 'ZEN').
card_image_name('armament master'/'ZEN', 'armament master').
card_uid('armament master'/'ZEN', 'ZEN:Armament Master:armament master').
card_rarity('armament master'/'ZEN', 'Rare').
card_artist('armament master'/'ZEN', 'Steven Belledin').
card_number('armament master'/'ZEN', '1').
card_flavor_text('armament master'/'ZEN', '\"We prepare for the known with daggers, rations, rope, and pitons. But we also prepare for the unknown with billycat tails, pikku roots, hedron chips . . .\"').
card_multiverse_id('armament master'/'ZEN', '190420').

card_in_set('arrow volley trap', 'ZEN').
card_original_type('arrow volley trap'/'ZEN', 'Instant — Trap').
card_original_text('arrow volley trap'/'ZEN', 'If four or more creatures are attacking, you may pay {1}{W} rather than pay Arrow Volley Trap\'s mana cost.\nArrow Volley Trap deals 5 damage divided as you choose among any number of target attacking creatures.').
card_first_print('arrow volley trap', 'ZEN').
card_image_name('arrow volley trap'/'ZEN', 'arrow volley trap').
card_uid('arrow volley trap'/'ZEN', 'ZEN:Arrow Volley Trap:arrow volley trap').
card_rarity('arrow volley trap'/'ZEN', 'Uncommon').
card_artist('arrow volley trap'/'ZEN', 'Steve Argyle').
card_number('arrow volley trap'/'ZEN', '2').
card_multiverse_id('arrow volley trap'/'ZEN', '193404').

card_in_set('bala ged thief', 'ZEN').
card_original_type('bala ged thief'/'ZEN', 'Creature — Human Rogue Ally').
card_original_text('bala ged thief'/'ZEN', 'Whenever Bala Ged Thief or another Ally enters the battlefield under your control, target player reveals a number of cards from his or her hand equal to the number of Allies you control. You choose one of them. That player discards that card.').
card_first_print('bala ged thief', 'ZEN').
card_image_name('bala ged thief'/'ZEN', 'bala ged thief').
card_uid('bala ged thief'/'ZEN', 'ZEN:Bala Ged Thief:bala ged thief').
card_rarity('bala ged thief'/'ZEN', 'Rare').
card_artist('bala ged thief'/'ZEN', 'Matt Cavotta').
card_number('bala ged thief'/'ZEN', '79').
card_multiverse_id('bala ged thief'/'ZEN', '197402').

card_in_set('baloth cage trap', 'ZEN').
card_original_type('baloth cage trap'/'ZEN', 'Instant — Trap').
card_original_text('baloth cage trap'/'ZEN', 'If an opponent had an artifact enter the battlefield under his or her control this turn, you may pay {1}{G} rather than pay Baloth Cage Trap\'s mana cost.\nPut a 4/4 green Beast creature token onto the battlefield.').
card_first_print('baloth cage trap', 'ZEN').
card_image_name('baloth cage trap'/'ZEN', 'baloth cage trap').
card_uid('baloth cage trap'/'ZEN', 'ZEN:Baloth Cage Trap:baloth cage trap').
card_rarity('baloth cage trap'/'ZEN', 'Uncommon').
card_artist('baloth cage trap'/'ZEN', 'Austin Hsu').
card_number('baloth cage trap'/'ZEN', '156').
card_multiverse_id('baloth cage trap'/'ZEN', '197531').

card_in_set('baloth woodcrasher', 'ZEN').
card_original_type('baloth woodcrasher'/'ZEN', 'Creature — Beast').
card_original_text('baloth woodcrasher'/'ZEN', 'Landfall — Whenever a land enters the battlefield under your control, Baloth Woodcrasher gets +4/+4 and gains trample until end of turn.').
card_first_print('baloth woodcrasher', 'ZEN').
card_image_name('baloth woodcrasher'/'ZEN', 'baloth woodcrasher').
card_uid('baloth woodcrasher'/'ZEN', 'ZEN:Baloth Woodcrasher:baloth woodcrasher').
card_rarity('baloth woodcrasher'/'ZEN', 'Uncommon').
card_artist('baloth woodcrasher'/'ZEN', 'Zoltan Boros & Gabor Szikszai').
card_number('baloth woodcrasher'/'ZEN', '157').
card_flavor_text('baloth woodcrasher'/'ZEN', 'Its insatiable hunger quickly depletes a region of prey. It must migrate from place to place to feed its massive bulk.').
card_multiverse_id('baloth woodcrasher'/'ZEN', '192231').

card_in_set('beast hunt', 'ZEN').
card_original_type('beast hunt'/'ZEN', 'Sorcery').
card_original_text('beast hunt'/'ZEN', 'Reveal the top three cards of your library. Put all creature cards revealed this way into your hand and the rest into your graveyard.').
card_image_name('beast hunt'/'ZEN', 'beast hunt').
card_uid('beast hunt'/'ZEN', 'ZEN:Beast Hunt:beast hunt').
card_rarity('beast hunt'/'ZEN', 'Common').
card_artist('beast hunt'/'ZEN', 'Kieran Yanner').
card_number('beast hunt'/'ZEN', '158').
card_flavor_text('beast hunt'/'ZEN', '\"Surely we could tame something besides hurdas and pillarfield oxen!\"\n—Sheyda, Ondu gamekeeper').
card_multiverse_id('beast hunt'/'ZEN', '185703').

card_in_set('beastmaster ascension', 'ZEN').
card_original_type('beastmaster ascension'/'ZEN', 'Enchantment').
card_original_text('beastmaster ascension'/'ZEN', 'Whenever a creature you control attacks, you may put a quest counter on Beastmaster Ascension.\nAs long as Beastmaster Ascension has seven or more quest counters on it, creatures you control get +5/+5.').
card_first_print('beastmaster ascension', 'ZEN').
card_image_name('beastmaster ascension'/'ZEN', 'beastmaster ascension').
card_uid('beastmaster ascension'/'ZEN', 'ZEN:Beastmaster Ascension:beastmaster ascension').
card_rarity('beastmaster ascension'/'ZEN', 'Rare').
card_artist('beastmaster ascension'/'ZEN', 'Alex Horley-Orlandelli').
card_number('beastmaster ascension'/'ZEN', '159').
card_multiverse_id('beastmaster ascension'/'ZEN', '197890').

card_in_set('blade of the bloodchief', 'ZEN').
card_original_type('blade of the bloodchief'/'ZEN', 'Artifact — Equipment').
card_original_text('blade of the bloodchief'/'ZEN', 'Whenever a creature is put into a graveyard from the battlefield, put a +1/+1 counter on equipped creature. If equipped creature is a Vampire, put two +1/+1 counters on it instead.\nEquip {1}').
card_first_print('blade of the bloodchief', 'ZEN').
card_image_name('blade of the bloodchief'/'ZEN', 'blade of the bloodchief').
card_uid('blade of the bloodchief'/'ZEN', 'ZEN:Blade of the Bloodchief:blade of the bloodchief').
card_rarity('blade of the bloodchief'/'ZEN', 'Rare').
card_artist('blade of the bloodchief'/'ZEN', 'Jung Park').
card_number('blade of the bloodchief'/'ZEN', '196').
card_multiverse_id('blade of the bloodchief'/'ZEN', '193397').

card_in_set('bladetusk boar', 'ZEN').
card_original_type('bladetusk boar'/'ZEN', 'Creature — Boar').
card_original_text('bladetusk boar'/'ZEN', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)').
card_first_print('bladetusk boar', 'ZEN').
card_image_name('bladetusk boar'/'ZEN', 'bladetusk boar').
card_uid('bladetusk boar'/'ZEN', 'ZEN:Bladetusk Boar:bladetusk boar').
card_rarity('bladetusk boar'/'ZEN', 'Common').
card_artist('bladetusk boar'/'ZEN', 'Paul Bonner').
card_number('bladetusk boar'/'ZEN', '118').
card_flavor_text('bladetusk boar'/'ZEN', 'Those who dare stand in its path are either brave or mindless. Or in the case of goblins, both.').
card_multiverse_id('bladetusk boar'/'ZEN', '180350').

card_in_set('blazing torch', 'ZEN').
card_original_type('blazing torch'/'ZEN', 'Artifact — Equipment').
card_original_text('blazing torch'/'ZEN', 'Equipped creature can\'t be blocked by Vampires or Zombies.\nEquipped creature has \"{T}, Sacrifice Blazing Torch: Blazing Torch deals 2 damage to target creature or player.\"\nEquip {1}').
card_first_print('blazing torch', 'ZEN').
card_image_name('blazing torch'/'ZEN', 'blazing torch').
card_uid('blazing torch'/'ZEN', 'ZEN:Blazing Torch:blazing torch').
card_rarity('blazing torch'/'ZEN', 'Uncommon').
card_artist('blazing torch'/'ZEN', 'Vance Kovacs').
card_number('blazing torch'/'ZEN', '197').
card_multiverse_id('blazing torch'/'ZEN', '191372').

card_in_set('blood seeker', 'ZEN').
card_original_type('blood seeker'/'ZEN', 'Creature — Vampire Shaman').
card_original_text('blood seeker'/'ZEN', 'Whenever a creature enters the battlefield under an opponent\'s control, you may have that player lose 1 life.').
card_first_print('blood seeker', 'ZEN').
card_image_name('blood seeker'/'ZEN', 'blood seeker').
card_uid('blood seeker'/'ZEN', 'ZEN:Blood Seeker:blood seeker').
card_rarity('blood seeker'/'ZEN', 'Common').
card_artist('blood seeker'/'ZEN', 'Greg Staples').
card_number('blood seeker'/'ZEN', '80').
card_flavor_text('blood seeker'/'ZEN', 'A drop now is all he needs to find you later.').
card_multiverse_id('blood seeker'/'ZEN', '180362').

card_in_set('blood tribute', 'ZEN').
card_original_type('blood tribute'/'ZEN', 'Sorcery').
card_original_text('blood tribute'/'ZEN', 'Kicker—Tap an untapped Vampire you control. (You may tap a Vampire you control in addition to any other costs as you cast this spell.)\nTarget opponent loses half his or her life, rounded up. If Blood Tribute was kicked, you gain life equal to the life lost this way.').
card_first_print('blood tribute', 'ZEN').
card_image_name('blood tribute'/'ZEN', 'blood tribute').
card_uid('blood tribute'/'ZEN', 'ZEN:Blood Tribute:blood tribute').
card_rarity('blood tribute'/'ZEN', 'Rare').
card_artist('blood tribute'/'ZEN', 'Alex Horley-Orlandelli').
card_number('blood tribute'/'ZEN', '81').
card_multiverse_id('blood tribute'/'ZEN', '170995').

card_in_set('bloodchief ascension', 'ZEN').
card_original_type('bloodchief ascension'/'ZEN', 'Enchantment').
card_original_text('bloodchief ascension'/'ZEN', 'At the beginning of each end step, if an opponent lost 2 or more life this turn, you may put a quest counter on Bloodchief Ascension. (Damage causes loss of life.)\nWhenever a card is put into an opponent\'s graveyard from anywhere, if Bloodchief Ascension has three or more quest counters on it, you may have that player lose 2 life. If you do, you gain 2 life.').
card_first_print('bloodchief ascension', 'ZEN').
card_image_name('bloodchief ascension'/'ZEN', 'bloodchief ascension').
card_uid('bloodchief ascension'/'ZEN', 'ZEN:Bloodchief Ascension:bloodchief ascension').
card_rarity('bloodchief ascension'/'ZEN', 'Rare').
card_artist('bloodchief ascension'/'ZEN', 'Adi Granov').
card_number('bloodchief ascension'/'ZEN', '82').
card_multiverse_id('bloodchief ascension'/'ZEN', '197892').

card_in_set('bloodghast', 'ZEN').
card_original_type('bloodghast'/'ZEN', 'Creature — Vampire Spirit').
card_original_text('bloodghast'/'ZEN', 'Bloodghast can\'t block.\nBloodghast has haste as long as an opponent has 10 or less life.\nLandfall — Whenever a land enters the battlefield under your control, you may return Bloodghast from your graveyard to the battlefield.').
card_first_print('bloodghast', 'ZEN').
card_image_name('bloodghast'/'ZEN', 'bloodghast').
card_uid('bloodghast'/'ZEN', 'ZEN:Bloodghast:bloodghast').
card_rarity('bloodghast'/'ZEN', 'Rare').
card_artist('bloodghast'/'ZEN', 'Daarken').
card_number('bloodghast'/'ZEN', '83').
card_multiverse_id('bloodghast'/'ZEN', '192230').

card_in_set('bog tatters', 'ZEN').
card_original_type('bog tatters'/'ZEN', 'Creature — Wraith').
card_original_text('bog tatters'/'ZEN', 'Swampwalk').
card_first_print('bog tatters', 'ZEN').
card_image_name('bog tatters'/'ZEN', 'bog tatters').
card_uid('bog tatters'/'ZEN', 'ZEN:Bog Tatters:bog tatters').
card_rarity('bog tatters'/'ZEN', 'Common').
card_artist('bog tatters'/'ZEN', 'Daarken').
card_number('bog tatters'/'ZEN', '84').
card_flavor_text('bog tatters'/'ZEN', 'A wraith is a tale of brutal slaying told anew whenever it finds a victim.').
card_multiverse_id('bog tatters'/'ZEN', '177500').

card_in_set('bold defense', 'ZEN').
card_original_type('bold defense'/'ZEN', 'Instant').
card_original_text('bold defense'/'ZEN', 'Kicker {3}{W} (You may pay an additional {3}{W} as you cast this spell.)\nCreatures you control get +1/+1 until end of turn. If Bold Defense was kicked, instead creatures you control get +2/+2 and gain first strike until end of turn.').
card_first_print('bold defense', 'ZEN').
card_image_name('bold defense'/'ZEN', 'bold defense').
card_uid('bold defense'/'ZEN', 'ZEN:Bold Defense:bold defense').
card_rarity('bold defense'/'ZEN', 'Common').
card_artist('bold defense'/'ZEN', 'Scott Chou').
card_number('bold defense'/'ZEN', '3').
card_multiverse_id('bold defense'/'ZEN', '180509').

card_in_set('brave the elements', 'ZEN').
card_original_type('brave the elements'/'ZEN', 'Instant').
card_original_text('brave the elements'/'ZEN', 'Choose a color. White creatures you control gain protection from the chosen color until end of turn.').
card_image_name('brave the elements'/'ZEN', 'brave the elements').
card_uid('brave the elements'/'ZEN', 'ZEN:Brave the Elements:brave the elements').
card_rarity('brave the elements'/'ZEN', 'Uncommon').
card_artist('brave the elements'/'ZEN', 'Goran Josic').
card_number('brave the elements'/'ZEN', '4').
card_flavor_text('brave the elements'/'ZEN', '\"Trust me, your lost fingers and toes are nothing compared to the lost treasures of Sejiri.\"\n—Zahr Gada, Halimar expedition leader').
card_multiverse_id('brave the elements'/'ZEN', '185734').

card_in_set('burst lightning', 'ZEN').
card_original_type('burst lightning'/'ZEN', 'Instant').
card_original_text('burst lightning'/'ZEN', 'Kicker {4} (You may pay an additional {4} as you cast this spell.)\nBurst Lightning deals 2 damage to target creature or player. If Burst Lightning was kicked, it deals 4 damage to that creature or player instead.').
card_image_name('burst lightning'/'ZEN', 'burst lightning').
card_uid('burst lightning'/'ZEN', 'ZEN:Burst Lightning:burst lightning').
card_rarity('burst lightning'/'ZEN', 'Common').
card_artist('burst lightning'/'ZEN', 'Vance Kovacs').
card_number('burst lightning'/'ZEN', '119').
card_multiverse_id('burst lightning'/'ZEN', '177558').

card_in_set('caller of gales', 'ZEN').
card_original_type('caller of gales'/'ZEN', 'Creature — Merfolk Wizard').
card_original_text('caller of gales'/'ZEN', '{1}{U}, {T}: Target creature gains flying until end of turn.').
card_first_print('caller of gales', 'ZEN').
card_image_name('caller of gales'/'ZEN', 'caller of gales').
card_uid('caller of gales'/'ZEN', 'ZEN:Caller of Gales:caller of gales').
card_rarity('caller of gales'/'ZEN', 'Common').
card_artist('caller of gales'/'ZEN', 'Alex Horley-Orlandelli').
card_number('caller of gales'/'ZEN', '43').
card_flavor_text('caller of gales'/'ZEN', '\"Some merfolk choose to rest their fins in the water. I believe wisdom exists not only where we were born but where we were told not to go.\"').
card_multiverse_id('caller of gales'/'ZEN', '183417').

card_in_set('cancel', 'ZEN').
card_original_type('cancel'/'ZEN', 'Instant').
card_original_text('cancel'/'ZEN', 'Counter target spell.').
card_image_name('cancel'/'ZEN', 'cancel').
card_uid('cancel'/'ZEN', 'ZEN:Cancel:cancel').
card_rarity('cancel'/'ZEN', 'Common').
card_artist('cancel'/'ZEN', 'Scott Chou').
card_number('cancel'/'ZEN', '44').
card_flavor_text('cancel'/'ZEN', 'Zendikar\'s volatile mana is the basis for many spectacular spells—and some equally astonishing failures.').
card_multiverse_id('cancel'/'ZEN', '189001').

card_in_set('caravan hurda', 'ZEN').
card_original_type('caravan hurda'/'ZEN', 'Creature — Giant').
card_original_text('caravan hurda'/'ZEN', 'Lifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_first_print('caravan hurda', 'ZEN').
card_image_name('caravan hurda'/'ZEN', 'caravan hurda').
card_uid('caravan hurda'/'ZEN', 'ZEN:Caravan Hurda:caravan hurda').
card_rarity('caravan hurda'/'ZEN', 'Common').
card_artist('caravan hurda'/'ZEN', 'Dave Kendall').
card_number('caravan hurda'/'ZEN', '5').
card_flavor_text('caravan hurda'/'ZEN', '\"Not too bright, but good enough for the job required—carrying and walking in a straight line.\"\n—Bruse Tarl, Goma Fada nomad').
card_multiverse_id('caravan hurda'/'ZEN', '185697').

card_in_set('carnage altar', 'ZEN').
card_original_type('carnage altar'/'ZEN', 'Artifact').
card_original_text('carnage altar'/'ZEN', '{3}, Sacrifice a creature: Draw a card.').
card_first_print('carnage altar', 'ZEN').
card_image_name('carnage altar'/'ZEN', 'carnage altar').
card_uid('carnage altar'/'ZEN', 'ZEN:Carnage Altar:carnage altar').
card_rarity('carnage altar'/'ZEN', 'Uncommon').
card_artist('carnage altar'/'ZEN', 'James Paick').
card_number('carnage altar'/'ZEN', '198').
card_flavor_text('carnage altar'/'ZEN', '\"In these bloodstains I will find the fingerprints of our oppressors.\"\n—Anowon, the Ruin Sage').
card_multiverse_id('carnage altar'/'ZEN', '193406').

card_in_set('celestial mantle', 'ZEN').
card_original_type('celestial mantle'/'ZEN', 'Enchantment — Aura').
card_original_text('celestial mantle'/'ZEN', 'Enchant creature\nEnchanted creature gets +3/+3.\nWhenever enchanted creature deals combat damage to a player, double its controller\'s life total.').
card_first_print('celestial mantle', 'ZEN').
card_image_name('celestial mantle'/'ZEN', 'celestial mantle').
card_uid('celestial mantle'/'ZEN', 'ZEN:Celestial Mantle:celestial mantle').
card_rarity('celestial mantle'/'ZEN', 'Rare').
card_artist('celestial mantle'/'ZEN', 'Steve Argyle').
card_number('celestial mantle'/'ZEN', '6').
card_flavor_text('celestial mantle'/'ZEN', 'Upon such armor, even a mountain would break.').
card_multiverse_id('celestial mantle'/'ZEN', '198524').

card_in_set('chandra ablaze', 'ZEN').
card_original_type('chandra ablaze'/'ZEN', 'Planeswalker — Chandra').
card_original_text('chandra ablaze'/'ZEN', '+1: Discard a card. If a red card is discarded this way, Chandra Ablaze deals 4 damage to target creature or player.\n-2: Each player discards his or her hand, then draws three cards.\n-7: Cast any number of red instant and/or sorcery cards from your graveyard without paying their mana costs.').
card_first_print('chandra ablaze', 'ZEN').
card_image_name('chandra ablaze'/'ZEN', 'chandra ablaze').
card_uid('chandra ablaze'/'ZEN', 'ZEN:Chandra Ablaze:chandra ablaze').
card_rarity('chandra ablaze'/'ZEN', 'Mythic Rare').
card_artist('chandra ablaze'/'ZEN', 'Steve Argyle').
card_number('chandra ablaze'/'ZEN', '120').
card_multiverse_id('chandra ablaze'/'ZEN', '195402').

card_in_set('cliff threader', 'ZEN').
card_original_type('cliff threader'/'ZEN', 'Creature — Kor Scout').
card_original_text('cliff threader'/'ZEN', 'Mountainwalk').
card_first_print('cliff threader', 'ZEN').
card_image_name('cliff threader'/'ZEN', 'cliff threader').
card_uid('cliff threader'/'ZEN', 'ZEN:Cliff Threader:cliff threader').
card_rarity('cliff threader'/'ZEN', 'Common').
card_artist('cliff threader'/'ZEN', 'Paul Bonner').
card_number('cliff threader'/'ZEN', '7').
card_flavor_text('cliff threader'/'ZEN', '\"The crossing demands singular focus. Your life consists of these ropes, these hooks, and these rocky crags. Your past is miles below.\"').
card_multiverse_id('cliff threader'/'ZEN', '177546').

card_in_set('cobra trap', 'ZEN').
card_original_type('cobra trap'/'ZEN', 'Instant — Trap').
card_original_text('cobra trap'/'ZEN', 'If a noncreature permanent under your control was destroyed this turn by a spell or ability an opponent controlled, you may pay {G} rather than pay Cobra Trap\'s mana cost.\nPut four 1/1 green Snake creature tokens onto the battlefield.').
card_first_print('cobra trap', 'ZEN').
card_image_name('cobra trap'/'ZEN', 'cobra trap').
card_uid('cobra trap'/'ZEN', 'ZEN:Cobra Trap:cobra trap').
card_rarity('cobra trap'/'ZEN', 'Uncommon').
card_artist('cobra trap'/'ZEN', 'Scott Chou').
card_number('cobra trap'/'ZEN', '160').
card_multiverse_id('cobra trap'/'ZEN', '197535').

card_in_set('conqueror\'s pledge', 'ZEN').
card_original_type('conqueror\'s pledge'/'ZEN', 'Sorcery').
card_original_text('conqueror\'s pledge'/'ZEN', 'Kicker {6} (You may pay an additional {6} as you cast this spell.)\nPut six 1/1 white Kor Soldier creature tokens onto the battlefield. If Conqueror\'s Pledge was kicked, put twelve of those tokens onto the battlefield instead.').
card_first_print('conqueror\'s pledge', 'ZEN').
card_image_name('conqueror\'s pledge'/'ZEN', 'conqueror\'s pledge').
card_uid('conqueror\'s pledge'/'ZEN', 'ZEN:Conqueror\'s Pledge:conqueror\'s pledge').
card_rarity('conqueror\'s pledge'/'ZEN', 'Rare').
card_artist('conqueror\'s pledge'/'ZEN', 'Kev Walker').
card_number('conqueror\'s pledge'/'ZEN', '8').
card_multiverse_id('conqueror\'s pledge'/'ZEN', '195626').

card_in_set('cosi\'s trickster', 'ZEN').
card_original_type('cosi\'s trickster'/'ZEN', 'Creature — Merfolk Wizard').
card_original_text('cosi\'s trickster'/'ZEN', 'Whenever an opponent shuffles his or her library, you may put a +1/+1 counter on Cosi\'s Trickster.').
card_first_print('cosi\'s trickster', 'ZEN').
card_image_name('cosi\'s trickster'/'ZEN', 'cosi\'s trickster').
card_uid('cosi\'s trickster'/'ZEN', 'ZEN:Cosi\'s Trickster:cosi\'s trickster').
card_rarity('cosi\'s trickster'/'ZEN', 'Rare').
card_artist('cosi\'s trickster'/'ZEN', 'Igor Kieryluk').
card_number('cosi\'s trickster'/'ZEN', '45').
card_flavor_text('cosi\'s trickster'/'ZEN', 'She watches the chaos created by the Roil, seeking strength in the patterns of anarchy.').
card_multiverse_id('cosi\'s trickster'/'ZEN', '186322').

card_in_set('crypt of agadeem', 'ZEN').
card_original_type('crypt of agadeem'/'ZEN', 'Land').
card_original_text('crypt of agadeem'/'ZEN', 'Crypt of Agadeem enters the battlefield tapped.\n{T}: Add {B} to your mana pool.\n{2}, {T}: Add {B} to your mana pool for each black creature card in your graveyard.').
card_first_print('crypt of agadeem', 'ZEN').
card_image_name('crypt of agadeem'/'ZEN', 'crypt of agadeem').
card_uid('crypt of agadeem'/'ZEN', 'ZEN:Crypt of Agadeem:crypt of agadeem').
card_rarity('crypt of agadeem'/'ZEN', 'Rare').
card_artist('crypt of agadeem'/'ZEN', 'Jason Felix').
card_number('crypt of agadeem'/'ZEN', '212').
card_multiverse_id('crypt of agadeem'/'ZEN', '190394').

card_in_set('crypt ripper', 'ZEN').
card_original_type('crypt ripper'/'ZEN', 'Creature — Shade').
card_original_text('crypt ripper'/'ZEN', 'Haste\n{B}: Crypt Ripper gets +1/+1 until end of turn.').
card_first_print('crypt ripper', 'ZEN').
card_image_name('crypt ripper'/'ZEN', 'crypt ripper').
card_uid('crypt ripper'/'ZEN', 'ZEN:Crypt Ripper:crypt ripper').
card_rarity('crypt ripper'/'ZEN', 'Common').
card_artist('crypt ripper'/'ZEN', 'Dave Kendall').
card_number('crypt ripper'/'ZEN', '85').
card_flavor_text('crypt ripper'/'ZEN', 'The tender light of the living quickens the pulse of the dead.').
card_multiverse_id('crypt ripper'/'ZEN', '178121').

card_in_set('day of judgment', 'ZEN').
card_original_type('day of judgment'/'ZEN', 'Sorcery').
card_original_text('day of judgment'/'ZEN', 'Destroy all creatures.').
card_image_name('day of judgment'/'ZEN', 'day of judgment').
card_uid('day of judgment'/'ZEN', 'ZEN:Day of Judgment:day of judgment').
card_rarity('day of judgment'/'ZEN', 'Rare').
card_artist('day of judgment'/'ZEN', 'Vincent Proce').
card_number('day of judgment'/'ZEN', '9').
card_flavor_text('day of judgment'/'ZEN', '\"I have seen planes leveled and all life rendered to dust. It brought no pleasure, even to a heart as dark as mine.\"\n—Sorin Markov').
card_multiverse_id('day of judgment'/'ZEN', '186309').

card_in_set('demolish', 'ZEN').
card_original_type('demolish'/'ZEN', 'Sorcery').
card_original_text('demolish'/'ZEN', 'Destroy target artifact or land.').
card_image_name('demolish'/'ZEN', 'demolish').
card_uid('demolish'/'ZEN', 'ZEN:Demolish:demolish').
card_rarity('demolish'/'ZEN', 'Common').
card_artist('demolish'/'ZEN', 'John Avon').
card_number('demolish'/'ZEN', '121').
card_flavor_text('demolish'/'ZEN', '\"This is why I hate settlements. They lull you into a false sense of security.\"\n—Yon Basrel, Oran-Rief survivalist').
card_multiverse_id('demolish'/'ZEN', '190406').

card_in_set('desecrated earth', 'ZEN').
card_original_type('desecrated earth'/'ZEN', 'Sorcery').
card_original_text('desecrated earth'/'ZEN', 'Destroy target land. Its controller discards a card.').
card_first_print('desecrated earth', 'ZEN').
card_image_name('desecrated earth'/'ZEN', 'desecrated earth').
card_uid('desecrated earth'/'ZEN', 'ZEN:Desecrated Earth:desecrated earth').
card_rarity('desecrated earth'/'ZEN', 'Common').
card_artist('desecrated earth'/'ZEN', 'Daarken').
card_number('desecrated earth'/'ZEN', '86').
card_flavor_text('desecrated earth'/'ZEN', '\"The land is swollen with mana, cursed relics, and secrets. If you puncture it, you never know what will burst out.\"\n—Javad Nasrin, Ondu relic hunter').
card_multiverse_id('desecrated earth'/'ZEN', '178137').

card_in_set('devout lightcaster', 'ZEN').
card_original_type('devout lightcaster'/'ZEN', 'Creature — Kor Cleric').
card_original_text('devout lightcaster'/'ZEN', 'Protection from black\nWhen Devout Lightcaster enters the battlefield, exile target black permanent.').
card_first_print('devout lightcaster', 'ZEN').
card_image_name('devout lightcaster'/'ZEN', 'devout lightcaster').
card_uid('devout lightcaster'/'ZEN', 'ZEN:Devout Lightcaster:devout lightcaster').
card_rarity('devout lightcaster'/'ZEN', 'Rare').
card_artist('devout lightcaster'/'ZEN', 'Shelly Wan').
card_number('devout lightcaster'/'ZEN', '10').
card_flavor_text('devout lightcaster'/'ZEN', '\"Goddess, grant us light to banish the world\'s shadows.\"\n—Prayer to Kamsa').
card_multiverse_id('devout lightcaster'/'ZEN', '191374').

card_in_set('disfigure', 'ZEN').
card_original_type('disfigure'/'ZEN', 'Instant').
card_original_text('disfigure'/'ZEN', 'Target creature gets -2/-2 until end of turn.').
card_first_print('disfigure', 'ZEN').
card_image_name('disfigure'/'ZEN', 'disfigure').
card_uid('disfigure'/'ZEN', 'ZEN:Disfigure:disfigure').
card_rarity('disfigure'/'ZEN', 'Common').
card_artist('disfigure'/'ZEN', 'Justin Sweet').
card_number('disfigure'/'ZEN', '87').
card_flavor_text('disfigure'/'ZEN', '\"Brave scar or unfortunate tale? It all depends on your pain threshold.\"').
card_multiverse_id('disfigure'/'ZEN', '180115').

card_in_set('eldrazi monument', 'ZEN').
card_original_type('eldrazi monument'/'ZEN', 'Artifact').
card_original_text('eldrazi monument'/'ZEN', 'Creatures you control get +1/+1, have flying, and are indestructible.\nAt the beginning of your upkeep, sacrifice a creature. If you can\'t, sacrifice Eldrazi Monument.').
card_first_print('eldrazi monument', 'ZEN').
card_image_name('eldrazi monument'/'ZEN', 'eldrazi monument').
card_uid('eldrazi monument'/'ZEN', 'ZEN:Eldrazi Monument:eldrazi monument').
card_rarity('eldrazi monument'/'ZEN', 'Mythic Rare').
card_artist('eldrazi monument'/'ZEN', 'Mark Tedin').
card_number('eldrazi monument'/'ZEN', '199').
card_flavor_text('eldrazi monument'/'ZEN', '\"Gods don\'t die. They merely slumber.\"\n—Ayli, Kamsa cleric').
card_multiverse_id('eldrazi monument'/'ZEN', '193398').

card_in_set('electropotence', 'ZEN').
card_original_type('electropotence'/'ZEN', 'Enchantment').
card_original_text('electropotence'/'ZEN', 'Whenever a creature enters the battlefield under your control, you may pay {2}{R}. If you do, that creature deals damage equal to its power to target creature or player.').
card_first_print('electropotence', 'ZEN').
card_image_name('electropotence'/'ZEN', 'electropotence').
card_uid('electropotence'/'ZEN', 'ZEN:Electropotence:electropotence').
card_rarity('electropotence'/'ZEN', 'Rare').
card_artist('electropotence'/'ZEN', 'Christopher Moeller').
card_number('electropotence'/'ZEN', '122').
card_flavor_text('electropotence'/'ZEN', 'Where life and force collide.').
card_multiverse_id('electropotence'/'ZEN', '191373').

card_in_set('elemental appeal', 'ZEN').
card_original_type('elemental appeal'/'ZEN', 'Sorcery').
card_original_text('elemental appeal'/'ZEN', 'Kicker {5} (You may pay an additional {5} as you cast this spell.)\nPut a 7/1 red Elemental creature token with trample and haste onto the battlefield. Exile it at the beginning of the next end step. If Elemental Appeal was kicked, that creature gets +7/+0 until end of turn.').
card_first_print('elemental appeal', 'ZEN').
card_image_name('elemental appeal'/'ZEN', 'elemental appeal').
card_uid('elemental appeal'/'ZEN', 'ZEN:Elemental Appeal:elemental appeal').
card_rarity('elemental appeal'/'ZEN', 'Rare').
card_artist('elemental appeal'/'ZEN', 'Anthony Francisco').
card_number('elemental appeal'/'ZEN', '123').
card_multiverse_id('elemental appeal'/'ZEN', '192221').

card_in_set('emeria angel', 'ZEN').
card_original_type('emeria angel'/'ZEN', 'Creature — Angel').
card_original_text('emeria angel'/'ZEN', 'Flying\nLandfall — Whenever a land enters the battlefield under your control, you may put a 1/1 white Bird creature token with flying onto the battlefield.').
card_image_name('emeria angel'/'ZEN', 'emeria angel').
card_uid('emeria angel'/'ZEN', 'ZEN:Emeria Angel:emeria angel').
card_rarity('emeria angel'/'ZEN', 'Rare').
card_artist('emeria angel'/'ZEN', 'Jim Murray').
card_number('emeria angel'/'ZEN', '11').
card_flavor_text('emeria angel'/'ZEN', 'When the earth shudders, the sky overflows.').
card_multiverse_id('emeria angel'/'ZEN', '190399').

card_in_set('emeria, the sky ruin', 'ZEN').
card_original_type('emeria, the sky ruin'/'ZEN', 'Land').
card_original_text('emeria, the sky ruin'/'ZEN', 'Emeria, the Sky Ruin enters the battlefield tapped.\nAt the beginning of your upkeep, if you control seven or more Plains, you may return target creature card from your graveyard to the battlefield.\n{T}: Add {W} to your mana pool.').
card_first_print('emeria, the sky ruin', 'ZEN').
card_image_name('emeria, the sky ruin'/'ZEN', 'emeria, the sky ruin').
card_uid('emeria, the sky ruin'/'ZEN', 'ZEN:Emeria, the Sky Ruin:emeria, the sky ruin').
card_rarity('emeria, the sky ruin'/'ZEN', 'Rare').
card_artist('emeria, the sky ruin'/'ZEN', 'Jaime Jones').
card_number('emeria, the sky ruin'/'ZEN', '213').
card_multiverse_id('emeria, the sky ruin'/'ZEN', '190414').

card_in_set('eternity vessel', 'ZEN').
card_original_type('eternity vessel'/'ZEN', 'Artifact').
card_original_text('eternity vessel'/'ZEN', 'Eternity Vessel enters the battlefield with X charge counters on it, where X is your life total.\nLandfall — Whenever a land enters the battlefield under your control, you may have your life total become the number of charge counters on Eternity Vessel.').
card_first_print('eternity vessel', 'ZEN').
card_image_name('eternity vessel'/'ZEN', 'eternity vessel').
card_uid('eternity vessel'/'ZEN', 'ZEN:Eternity Vessel:eternity vessel').
card_rarity('eternity vessel'/'ZEN', 'Mythic Rare').
card_artist('eternity vessel'/'ZEN', 'John Avon').
card_number('eternity vessel'/'ZEN', '200').
card_multiverse_id('eternity vessel'/'ZEN', '189006').

card_in_set('expedition map', 'ZEN').
card_original_type('expedition map'/'ZEN', 'Artifact').
card_original_text('expedition map'/'ZEN', '{2}, {T}, Sacrifice Expedition Map: Search your library for a land card, reveal it, and put it into your hand. Then shuffle your library.').
card_first_print('expedition map', 'ZEN').
card_image_name('expedition map'/'ZEN', 'expedition map').
card_uid('expedition map'/'ZEN', 'ZEN:Expedition Map:expedition map').
card_rarity('expedition map'/'ZEN', 'Common').
card_artist('expedition map'/'ZEN', 'Franz Vohwinkel').
card_number('expedition map'/'ZEN', '201').
card_flavor_text('expedition map'/'ZEN', '\"I use maps to find out where explorers have already been. Then I go the other way.\"\n—Javad Nasrin, Ondu relic hunter').
card_multiverse_id('expedition map'/'ZEN', '193405').

card_in_set('explorer\'s scope', 'ZEN').
card_original_type('explorer\'s scope'/'ZEN', 'Artifact — Equipment').
card_original_text('explorer\'s scope'/'ZEN', 'Whenever equipped creature attacks, look at the top card of your library. If it\'s a land card, you may put it onto the battlefield tapped.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('explorer\'s scope', 'ZEN').
card_image_name('explorer\'s scope'/'ZEN', 'explorer\'s scope').
card_uid('explorer\'s scope'/'ZEN', 'ZEN:Explorer\'s Scope:explorer\'s scope').
card_rarity('explorer\'s scope'/'ZEN', 'Common').
card_artist('explorer\'s scope'/'ZEN', 'Vincent Proce').
card_number('explorer\'s scope'/'ZEN', '202').
card_multiverse_id('explorer\'s scope'/'ZEN', '190396').

card_in_set('feast of blood', 'ZEN').
card_original_type('feast of blood'/'ZEN', 'Sorcery').
card_original_text('feast of blood'/'ZEN', 'Cast Feast of Blood only if you control two or more Vampires.\nDestroy target creature. You gain 4 life.').
card_image_name('feast of blood'/'ZEN', 'feast of blood').
card_uid('feast of blood'/'ZEN', 'ZEN:Feast of Blood:feast of blood').
card_rarity('feast of blood'/'ZEN', 'Uncommon').
card_artist('feast of blood'/'ZEN', 'Jason Felix').
card_number('feast of blood'/'ZEN', '88').
card_flavor_text('feast of blood'/'ZEN', '\"The vampires of this world don\'t know the pleasures of hunger. They gorge themselves without savoring the kill.\"\n—Sorin Markov').
card_multiverse_id('feast of blood'/'ZEN', '189629').

card_in_set('felidar sovereign', 'ZEN').
card_original_type('felidar sovereign'/'ZEN', 'Creature — Cat Beast').
card_original_text('felidar sovereign'/'ZEN', 'Vigilance, lifelink\nAt the beginning of your upkeep, if you have 40 or more life, you win the game.').
card_first_print('felidar sovereign', 'ZEN').
card_image_name('felidar sovereign'/'ZEN', 'felidar sovereign').
card_uid('felidar sovereign'/'ZEN', 'ZEN:Felidar Sovereign:felidar sovereign').
card_rarity('felidar sovereign'/'ZEN', 'Mythic Rare').
card_artist('felidar sovereign'/'ZEN', 'Zoltan Boros & Gabor Szikszai').
card_number('felidar sovereign'/'ZEN', '12').
card_flavor_text('felidar sovereign'/'ZEN', '\"If survival is a game, I\'ve seen the winner.\"\n—Hazir, Sejiri cartographer').
card_multiverse_id('felidar sovereign'/'ZEN', '185743').

card_in_set('forest', 'ZEN').
card_original_type('forest'/'ZEN', 'Basic Land — Forest').
card_original_text('forest'/'ZEN', 'G').
card_image_name('forest'/'ZEN', 'forest1').
card_uid('forest'/'ZEN', 'ZEN:Forest:forest1').
card_rarity('forest'/'ZEN', 'Basic Land').
card_artist('forest'/'ZEN', 'John Avon').
card_number('forest'/'ZEN', '246').
card_multiverse_id('forest'/'ZEN', '195158').

card_in_set('forest', 'ZEN').
card_original_type('forest'/'ZEN', 'Basic Land — Forest').
card_original_text('forest'/'ZEN', 'G').
card_image_name('forest'/'ZEN', 'forest1a').
card_uid('forest'/'ZEN', 'ZEN:Forest:forest1a').
card_rarity('forest'/'ZEN', 'Basic Land').
card_artist('forest'/'ZEN', 'John Avon').
card_number('forest'/'ZEN', '246a').
card_multiverse_id('forest'/'ZEN', '201959').

card_in_set('forest', 'ZEN').
card_original_type('forest'/'ZEN', 'Basic Land — Forest').
card_original_text('forest'/'ZEN', 'G').
card_image_name('forest'/'ZEN', 'forest2').
card_uid('forest'/'ZEN', 'ZEN:Forest:forest2').
card_rarity('forest'/'ZEN', 'Basic Land').
card_artist('forest'/'ZEN', 'Jung Park').
card_number('forest'/'ZEN', '247').
card_multiverse_id('forest'/'ZEN', '201962').

card_in_set('forest', 'ZEN').
card_original_type('forest'/'ZEN', 'Basic Land — Forest').
card_original_text('forest'/'ZEN', 'G').
card_image_name('forest'/'ZEN', 'forest2a').
card_uid('forest'/'ZEN', 'ZEN:Forest:forest2a').
card_rarity('forest'/'ZEN', 'Basic Land').
card_artist('forest'/'ZEN', 'Jung Park').
card_number('forest'/'ZEN', '247a').
card_multiverse_id('forest'/'ZEN', '195192').

card_in_set('forest', 'ZEN').
card_original_type('forest'/'ZEN', 'Basic Land — Forest').
card_original_text('forest'/'ZEN', 'G').
card_image_name('forest'/'ZEN', 'forest3').
card_uid('forest'/'ZEN', 'ZEN:Forest:forest3').
card_rarity('forest'/'ZEN', 'Basic Land').
card_artist('forest'/'ZEN', 'Véronique Meignaud').
card_number('forest'/'ZEN', '248').
card_multiverse_id('forest'/'ZEN', '195177').

card_in_set('forest', 'ZEN').
card_original_type('forest'/'ZEN', 'Basic Land — Forest').
card_original_text('forest'/'ZEN', 'G').
card_image_name('forest'/'ZEN', 'forest3a').
card_uid('forest'/'ZEN', 'ZEN:Forest:forest3a').
card_rarity('forest'/'ZEN', 'Basic Land').
card_artist('forest'/'ZEN', 'Véronique Meignaud').
card_number('forest'/'ZEN', '248a').
card_multiverse_id('forest'/'ZEN', '201960').

card_in_set('forest', 'ZEN').
card_original_type('forest'/'ZEN', 'Basic Land — Forest').
card_original_text('forest'/'ZEN', 'G').
card_image_name('forest'/'ZEN', 'forest4').
card_uid('forest'/'ZEN', 'ZEN:Forest:forest4').
card_rarity('forest'/'ZEN', 'Basic Land').
card_artist('forest'/'ZEN', 'Vincent Proce').
card_number('forest'/'ZEN', '249').
card_multiverse_id('forest'/'ZEN', '201961').

card_in_set('forest', 'ZEN').
card_original_type('forest'/'ZEN', 'Basic Land — Forest').
card_original_text('forest'/'ZEN', 'G').
card_image_name('forest'/'ZEN', 'forest4a').
card_uid('forest'/'ZEN', 'ZEN:Forest:forest4a').
card_rarity('forest'/'ZEN', 'Basic Land').
card_artist('forest'/'ZEN', 'Vincent Proce').
card_number('forest'/'ZEN', '249a').
card_multiverse_id('forest'/'ZEN', '195183').

card_in_set('frontier guide', 'ZEN').
card_original_type('frontier guide'/'ZEN', 'Creature — Elf Scout').
card_original_text('frontier guide'/'ZEN', '{3}{G}, {T}: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_first_print('frontier guide', 'ZEN').
card_image_name('frontier guide'/'ZEN', 'frontier guide').
card_uid('frontier guide'/'ZEN', 'ZEN:Frontier Guide:frontier guide').
card_rarity('frontier guide'/'ZEN', 'Uncommon').
card_artist('frontier guide'/'ZEN', 'Wayne Reynolds').
card_number('frontier guide'/'ZEN', '161').
card_flavor_text('frontier guide'/'ZEN', '\"The elders say Zendikar has no safe, peaceful places. How would they know? They\'ve never looked.\"').
card_multiverse_id('frontier guide'/'ZEN', '180361').

card_in_set('gatekeeper of malakir', 'ZEN').
card_original_type('gatekeeper of malakir'/'ZEN', 'Creature — Vampire Warrior').
card_original_text('gatekeeper of malakir'/'ZEN', 'Kicker {B} (You may pay an additional {B} as you cast this spell.)\nWhen Gatekeeper of Malakir enters the battlefield, if it was kicked, target player sacrifices a creature.').
card_image_name('gatekeeper of malakir'/'ZEN', 'gatekeeper of malakir').
card_uid('gatekeeper of malakir'/'ZEN', 'ZEN:Gatekeeper of Malakir:gatekeeper of malakir').
card_rarity('gatekeeper of malakir'/'ZEN', 'Uncommon').
card_artist('gatekeeper of malakir'/'ZEN', 'Karl Kopinski').
card_number('gatekeeper of malakir'/'ZEN', '89').
card_flavor_text('gatekeeper of malakir'/'ZEN', '\"You may enter the city—once the toll is paid.\"').
card_multiverse_id('gatekeeper of malakir'/'ZEN', '185698').

card_in_set('geyser glider', 'ZEN').
card_original_type('geyser glider'/'ZEN', 'Creature — Elemental Beast').
card_original_text('geyser glider'/'ZEN', 'Landfall — Whenever a land enters the battlefield under your control, Geyser Glider gains flying until end of turn.').
card_first_print('geyser glider', 'ZEN').
card_image_name('geyser glider'/'ZEN', 'geyser glider').
card_uid('geyser glider'/'ZEN', 'ZEN:Geyser Glider:geyser glider').
card_rarity('geyser glider'/'ZEN', 'Uncommon').
card_artist('geyser glider'/'ZEN', 'Warren Mahy').
card_number('geyser glider'/'ZEN', '124').
card_flavor_text('geyser glider'/'ZEN', '\"Quit pontificating, mage. The last thing we want to do is give it more hot air.\"\n—Tala Vertan, Makindi shieldmate').
card_multiverse_id('geyser glider'/'ZEN', '197887').

card_in_set('giant scorpion', 'ZEN').
card_original_type('giant scorpion'/'ZEN', 'Creature — Scorpion').
card_original_text('giant scorpion'/'ZEN', 'Deathtouch (Creatures dealt damage by this creature are destroyed. You can divide this creature\'s combat damage among any of the creatures blocking or blocked by it.)').
card_first_print('giant scorpion', 'ZEN').
card_image_name('giant scorpion'/'ZEN', 'giant scorpion').
card_uid('giant scorpion'/'ZEN', 'ZEN:Giant Scorpion:giant scorpion').
card_rarity('giant scorpion'/'ZEN', 'Common').
card_artist('giant scorpion'/'ZEN', 'Raymond Swanland').
card_number('giant scorpion'/'ZEN', '90').
card_flavor_text('giant scorpion'/'ZEN', 'Its sting hurts, but death is strangely painless.').
card_multiverse_id('giant scorpion'/'ZEN', '192226').

card_in_set('gigantiform', 'ZEN').
card_original_type('gigantiform'/'ZEN', 'Enchantment — Aura').
card_original_text('gigantiform'/'ZEN', 'Kicker {4}\nEnchant creature\nEnchanted creature is 8/8 and has trample.\nWhen Gigantiform enters the battlefield, if it was kicked, you may search your library for a card named Gigantiform, put it onto the battlefield, then shuffle your library.').
card_first_print('gigantiform', 'ZEN').
card_image_name('gigantiform'/'ZEN', 'gigantiform').
card_uid('gigantiform'/'ZEN', 'ZEN:Gigantiform:gigantiform').
card_rarity('gigantiform'/'ZEN', 'Rare').
card_artist('gigantiform'/'ZEN', 'Justin Sweet').
card_number('gigantiform'/'ZEN', '162').
card_multiverse_id('gigantiform'/'ZEN', '195627').

card_in_set('goblin bushwhacker', 'ZEN').
card_original_type('goblin bushwhacker'/'ZEN', 'Creature — Goblin Warrior').
card_original_text('goblin bushwhacker'/'ZEN', 'Kicker {R} (You may pay an additional {R} as you cast this spell.)\nWhen Goblin Bushwhacker enters the battlefield, if it was kicked, creatures you control get +1/+0 and gain haste until end of turn.').
card_first_print('goblin bushwhacker', 'ZEN').
card_image_name('goblin bushwhacker'/'ZEN', 'goblin bushwhacker').
card_uid('goblin bushwhacker'/'ZEN', 'ZEN:Goblin Bushwhacker:goblin bushwhacker').
card_rarity('goblin bushwhacker'/'ZEN', 'Common').
card_artist('goblin bushwhacker'/'ZEN', 'Mark Tedin').
card_number('goblin bushwhacker'/'ZEN', '125').
card_multiverse_id('goblin bushwhacker'/'ZEN', '177501').

card_in_set('goblin guide', 'ZEN').
card_original_type('goblin guide'/'ZEN', 'Creature — Goblin Scout').
card_original_text('goblin guide'/'ZEN', 'Haste\nWhenever Goblin Guide attacks, defending player reveals the top card of his or her library. If it\'s a land card, that player puts it into his or her hand.').
card_image_name('goblin guide'/'ZEN', 'goblin guide').
card_uid('goblin guide'/'ZEN', 'ZEN:Goblin Guide:goblin guide').
card_rarity('goblin guide'/'ZEN', 'Rare').
card_artist('goblin guide'/'ZEN', 'Warren Mahy').
card_number('goblin guide'/'ZEN', '126').
card_flavor_text('goblin guide'/'ZEN', '\"I\'ve been all over this world. I even remember some of those places.\"').
card_multiverse_id('goblin guide'/'ZEN', '170987').

card_in_set('goblin ruinblaster', 'ZEN').
card_original_type('goblin ruinblaster'/'ZEN', 'Creature — Goblin Shaman').
card_original_text('goblin ruinblaster'/'ZEN', 'Kicker {R} (You may pay an additional {R} as you cast this spell.)\nHaste\nWhen Goblin Ruinblaster enters the battlefield, if it was kicked, destroy target nonbasic land.').
card_first_print('goblin ruinblaster', 'ZEN').
card_image_name('goblin ruinblaster'/'ZEN', 'goblin ruinblaster').
card_uid('goblin ruinblaster'/'ZEN', 'ZEN:Goblin Ruinblaster:goblin ruinblaster').
card_rarity('goblin ruinblaster'/'ZEN', 'Uncommon').
card_artist('goblin ruinblaster'/'ZEN', 'Matt Cavotta').
card_number('goblin ruinblaster'/'ZEN', '127').
card_multiverse_id('goblin ruinblaster'/'ZEN', '180411').

card_in_set('goblin shortcutter', 'ZEN').
card_original_type('goblin shortcutter'/'ZEN', 'Creature — Goblin Scout').
card_original_text('goblin shortcutter'/'ZEN', 'When Goblin Shortcutter enters the battlefield, target creature can\'t block this turn.').
card_first_print('goblin shortcutter', 'ZEN').
card_image_name('goblin shortcutter'/'ZEN', 'goblin shortcutter').
card_uid('goblin shortcutter'/'ZEN', 'ZEN:Goblin Shortcutter:goblin shortcutter').
card_rarity('goblin shortcutter'/'ZEN', 'Common').
card_artist('goblin shortcutter'/'ZEN', 'Jesper Ejsing').
card_number('goblin shortcutter'/'ZEN', '128').
card_flavor_text('goblin shortcutter'/'ZEN', '\"Goblins are cheap, but be careful. It\'s a lot easier to steal from a corpse than a customer.\"\n—Samila, Murasa Expeditionary House').
card_multiverse_id('goblin shortcutter'/'ZEN', '180473').

card_in_set('goblin war paint', 'ZEN').
card_original_type('goblin war paint'/'ZEN', 'Enchantment — Aura').
card_original_text('goblin war paint'/'ZEN', 'Enchant creature\nEnchanted creature gets +2/+2 and has haste.').
card_first_print('goblin war paint', 'ZEN').
card_image_name('goblin war paint'/'ZEN', 'goblin war paint').
card_uid('goblin war paint'/'ZEN', 'ZEN:Goblin War Paint:goblin war paint').
card_rarity('goblin war paint'/'ZEN', 'Common').
card_artist('goblin war paint'/'ZEN', 'Austin Hsu').
card_number('goblin war paint'/'ZEN', '129').
card_flavor_text('goblin war paint'/'ZEN', 'War paint made from kolya fruit heightens senses and lessens fear. Unfortunately, fear is usually what keeps you alive.').
card_multiverse_id('goblin war paint'/'ZEN', '185701').

card_in_set('gomazoa', 'ZEN').
card_original_type('gomazoa'/'ZEN', 'Creature — Jellyfish').
card_original_text('gomazoa'/'ZEN', 'Defender, flying\n{T}: Put Gomazoa and each creature it\'s blocking on top of their owners\' libraries, then those players shuffle their libraries.').
card_first_print('gomazoa', 'ZEN').
card_image_name('gomazoa'/'ZEN', 'gomazoa').
card_uid('gomazoa'/'ZEN', 'ZEN:Gomazoa:gomazoa').
card_rarity('gomazoa'/'ZEN', 'Uncommon').
card_artist('gomazoa'/'ZEN', 'Chippy').
card_number('gomazoa'/'ZEN', '46').
card_flavor_text('gomazoa'/'ZEN', 'To explorers, \"point man\" is a polite way of saying \"gomazoa fodder.\"').
card_multiverse_id('gomazoa'/'ZEN', '198523').

card_in_set('grappling hook', 'ZEN').
card_original_type('grappling hook'/'ZEN', 'Artifact — Equipment').
card_original_text('grappling hook'/'ZEN', 'Equipped creature has double strike.\nWhenever equipped creature attacks, you may have target creature block it this turn if able.\nEquip {4}').
card_first_print('grappling hook', 'ZEN').
card_image_name('grappling hook'/'ZEN', 'grappling hook').
card_uid('grappling hook'/'ZEN', 'ZEN:Grappling Hook:grappling hook').
card_rarity('grappling hook'/'ZEN', 'Rare').
card_artist('grappling hook'/'ZEN', 'Philip Straub').
card_number('grappling hook'/'ZEN', '203').
card_flavor_text('grappling hook'/'ZEN', 'Part tool, part weapon, part of the kor.').
card_multiverse_id('grappling hook'/'ZEN', '192220').

card_in_set('graypelt refuge', 'ZEN').
card_original_type('graypelt refuge'/'ZEN', 'Land').
card_original_text('graypelt refuge'/'ZEN', 'Graypelt Refuge enters the battlefield tapped.\nWhen Graypelt Refuge enters the battlefield, you gain 1 life.\n{T}: Add {G} or {W} to your mana pool.').
card_first_print('graypelt refuge', 'ZEN').
card_image_name('graypelt refuge'/'ZEN', 'graypelt refuge').
card_uid('graypelt refuge'/'ZEN', 'ZEN:Graypelt Refuge:graypelt refuge').
card_rarity('graypelt refuge'/'ZEN', 'Uncommon').
card_artist('graypelt refuge'/'ZEN', 'Philip Straub').
card_number('graypelt refuge'/'ZEN', '214').
card_multiverse_id('graypelt refuge'/'ZEN', '189631').

card_in_set('grazing gladehart', 'ZEN').
card_original_type('grazing gladehart'/'ZEN', 'Creature — Antelope').
card_original_text('grazing gladehart'/'ZEN', 'Landfall — Whenever a land enters the battlefield under your control, you may gain 2 life.').
card_first_print('grazing gladehart', 'ZEN').
card_image_name('grazing gladehart'/'ZEN', 'grazing gladehart').
card_uid('grazing gladehart'/'ZEN', 'ZEN:Grazing Gladehart:grazing gladehart').
card_rarity('grazing gladehart'/'ZEN', 'Common').
card_artist('grazing gladehart'/'ZEN', 'Ryan Pancoast').
card_number('grazing gladehart'/'ZEN', '163').
card_flavor_text('grazing gladehart'/'ZEN', '\"Don\'t be fooled. If it were as docile as it looks, it would\'ve died off long ago.\"\n—Yon Basrel, Oran-Rief survivalist').
card_multiverse_id('grazing gladehart'/'ZEN', '189621').

card_in_set('greenweaver druid', 'ZEN').
card_original_type('greenweaver druid'/'ZEN', 'Creature — Elf Druid').
card_original_text('greenweaver druid'/'ZEN', '{T}: Add {G}{G} to your mana pool.').
card_first_print('greenweaver druid', 'ZEN').
card_image_name('greenweaver druid'/'ZEN', 'greenweaver druid').
card_uid('greenweaver druid'/'ZEN', 'ZEN:Greenweaver Druid:greenweaver druid').
card_rarity('greenweaver druid'/'ZEN', 'Uncommon').
card_artist('greenweaver druid'/'ZEN', 'Justin Sweet').
card_number('greenweaver druid'/'ZEN', '164').
card_flavor_text('greenweaver druid'/'ZEN', 'The other tribes call them fanatics, but none deny that the Mul Daya elves have an iron-strong bond to some force greater than themselves.').
card_multiverse_id('greenweaver druid'/'ZEN', '185694').

card_in_set('grim discovery', 'ZEN').
card_original_type('grim discovery'/'ZEN', 'Sorcery').
card_original_text('grim discovery'/'ZEN', 'Choose one or both — Return target creature card from your graveyard to your hand; and/or return target land card from your graveyard to your hand.').
card_first_print('grim discovery', 'ZEN').
card_image_name('grim discovery'/'ZEN', 'grim discovery').
card_uid('grim discovery'/'ZEN', 'ZEN:Grim Discovery:grim discovery').
card_rarity('grim discovery'/'ZEN', 'Common').
card_artist('grim discovery'/'ZEN', 'Christopher Moeller').
card_number('grim discovery'/'ZEN', '91').
card_flavor_text('grim discovery'/'ZEN', 'Few among the living understand just how much of their world is shaped by the ruins of the dead.').
card_multiverse_id('grim discovery'/'ZEN', '180127').

card_in_set('guul draz specter', 'ZEN').
card_original_type('guul draz specter'/'ZEN', 'Creature — Specter').
card_original_text('guul draz specter'/'ZEN', 'Flying\nGuul Draz Specter gets +3/+3 as long as an opponent has no cards in hand.\nWhenever Guul Draz Specter deals combat damage to a player, that player discards a card.').
card_first_print('guul draz specter', 'ZEN').
card_image_name('guul draz specter'/'ZEN', 'guul draz specter').
card_uid('guul draz specter'/'ZEN', 'ZEN:Guul Draz Specter:guul draz specter').
card_rarity('guul draz specter'/'ZEN', 'Rare').
card_artist('guul draz specter'/'ZEN', 'Mark Tedin').
card_number('guul draz specter'/'ZEN', '92').
card_multiverse_id('guul draz specter'/'ZEN', '183418').

card_in_set('guul draz vampire', 'ZEN').
card_original_type('guul draz vampire'/'ZEN', 'Creature — Vampire Rogue').
card_original_text('guul draz vampire'/'ZEN', 'As long as an opponent has 10 or less life, Guul Draz Vampire gets +2/+1 and has intimidate. (It can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)').
card_first_print('guul draz vampire', 'ZEN').
card_image_name('guul draz vampire'/'ZEN', 'guul draz vampire').
card_uid('guul draz vampire'/'ZEN', 'ZEN:Guul Draz Vampire:guul draz vampire').
card_rarity('guul draz vampire'/'ZEN', 'Common').
card_artist('guul draz vampire'/'ZEN', 'Steve Argyle').
card_number('guul draz vampire'/'ZEN', '93').
card_flavor_text('guul draz vampire'/'ZEN', '\"A creature\'s bloodscent is a beacon that cannot be disguised.\"').
card_multiverse_id('guul draz vampire'/'ZEN', '180498').

card_in_set('hagra crocodile', 'ZEN').
card_original_type('hagra crocodile'/'ZEN', 'Creature — Crocodile').
card_original_text('hagra crocodile'/'ZEN', 'Hagra Crocodile can\'t block.\nLandfall — Whenever a land enters the battlefield under your control, Hagra Crocodile gets +2/+2 until end of turn.').
card_first_print('hagra crocodile', 'ZEN').
card_image_name('hagra crocodile'/'ZEN', 'hagra crocodile').
card_uid('hagra crocodile'/'ZEN', 'ZEN:Hagra Crocodile:hagra crocodile').
card_rarity('hagra crocodile'/'ZEN', 'Common').
card_artist('hagra crocodile'/'ZEN', 'Daren Bader').
card_number('hagra crocodile'/'ZEN', '94').
card_flavor_text('hagra crocodile'/'ZEN', 'The creatures of Zendikar are opportunists, eating whatever is available to them. Like goblins. Or boats.').
card_multiverse_id('hagra crocodile'/'ZEN', '170998').

card_in_set('hagra diabolist', 'ZEN').
card_original_type('hagra diabolist'/'ZEN', 'Creature — Ogre Shaman Ally').
card_original_text('hagra diabolist'/'ZEN', 'Whenever Hagra Diabolist or another Ally enters the battlefield under your control, you may have target player lose life equal to the number of Allies you control.').
card_first_print('hagra diabolist', 'ZEN').
card_image_name('hagra diabolist'/'ZEN', 'hagra diabolist').
card_uid('hagra diabolist'/'ZEN', 'ZEN:Hagra Diabolist:hagra diabolist').
card_rarity('hagra diabolist'/'ZEN', 'Uncommon').
card_artist('hagra diabolist'/'ZEN', 'Karl Kopinski').
card_number('hagra diabolist'/'ZEN', '95').
card_flavor_text('hagra diabolist'/'ZEN', '\"When exploring the darkest regions, wickedness can be the best accomplice of all.\"').
card_multiverse_id('hagra diabolist'/'ZEN', '197404').

card_in_set('halo hunter', 'ZEN').
card_original_type('halo hunter'/'ZEN', 'Creature — Demon').
card_original_text('halo hunter'/'ZEN', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)\nWhen Halo Hunter enters the battlefield, destroy target Angel.').
card_first_print('halo hunter', 'ZEN').
card_image_name('halo hunter'/'ZEN', 'halo hunter').
card_uid('halo hunter'/'ZEN', 'ZEN:Halo Hunter:halo hunter').
card_rarity('halo hunter'/'ZEN', 'Rare').
card_artist('halo hunter'/'ZEN', 'Chris Rahn').
card_number('halo hunter'/'ZEN', '96').
card_flavor_text('halo hunter'/'ZEN', 'Hanging on the walls of his lair, the fallen halos cast his depravity in everlasting light.').
card_multiverse_id('halo hunter'/'ZEN', '185752').

card_in_set('harrow', 'ZEN').
card_original_type('harrow'/'ZEN', 'Instant').
card_original_text('harrow'/'ZEN', 'As an additional cost to cast Harrow, sacrifice a land.\nSearch your library for up to two basic land cards and put them onto the battlefield. Then shuffle your library.').
card_image_name('harrow'/'ZEN', 'harrow').
card_uid('harrow'/'ZEN', 'ZEN:Harrow:harrow').
card_rarity('harrow'/'ZEN', 'Common').
card_artist('harrow'/'ZEN', 'Izzy').
card_number('harrow'/'ZEN', '165').
card_flavor_text('harrow'/'ZEN', 'The Roil redraws the world as it passes, leaving only the hedrons unchanged.').
card_multiverse_id('harrow'/'ZEN', '180408').

card_in_set('heartstabber mosquito', 'ZEN').
card_original_type('heartstabber mosquito'/'ZEN', 'Creature — Insect').
card_original_text('heartstabber mosquito'/'ZEN', 'Kicker {2}{B} (You may pay an additional {2}{B} as you cast this spell.)\nFlying\nWhen Heartstabber Mosquito enters the battlefield, if it was kicked, destroy target creature.').
card_first_print('heartstabber mosquito', 'ZEN').
card_image_name('heartstabber mosquito'/'ZEN', 'heartstabber mosquito').
card_uid('heartstabber mosquito'/'ZEN', 'ZEN:Heartstabber Mosquito:heartstabber mosquito').
card_rarity('heartstabber mosquito'/'ZEN', 'Common').
card_artist('heartstabber mosquito'/'ZEN', 'Jason Felix').
card_number('heartstabber mosquito'/'ZEN', '97').
card_multiverse_id('heartstabber mosquito'/'ZEN', '191361').

card_in_set('hedron crab', 'ZEN').
card_original_type('hedron crab'/'ZEN', 'Creature — Crab').
card_original_text('hedron crab'/'ZEN', 'Landfall — Whenever a land enters the battlefield under your control, target player puts the top three cards of his or her library into his or her graveyard.').
card_first_print('hedron crab', 'ZEN').
card_image_name('hedron crab'/'ZEN', 'hedron crab').
card_uid('hedron crab'/'ZEN', 'ZEN:Hedron Crab:hedron crab').
card_rarity('hedron crab'/'ZEN', 'Uncommon').
card_artist('hedron crab'/'ZEN', 'Jesper Ejsing').
card_number('hedron crab'/'ZEN', '47').
card_flavor_text('hedron crab'/'ZEN', 'Hedrons perplex minds both great and small.').
card_multiverse_id('hedron crab'/'ZEN', '180348').

card_in_set('hedron scrabbler', 'ZEN').
card_original_type('hedron scrabbler'/'ZEN', 'Artifact Creature — Construct').
card_original_text('hedron scrabbler'/'ZEN', 'Landfall — Whenever a land enters the battlefield under your control, Hedron Scrabbler gets +1/+1 until end of turn.').
card_first_print('hedron scrabbler', 'ZEN').
card_image_name('hedron scrabbler'/'ZEN', 'hedron scrabbler').
card_uid('hedron scrabbler'/'ZEN', 'ZEN:Hedron Scrabbler:hedron scrabbler').
card_rarity('hedron scrabbler'/'ZEN', 'Common').
card_artist('hedron scrabbler'/'ZEN', 'Jason Felix').
card_number('hedron scrabbler'/'ZEN', '204').
card_flavor_text('hedron scrabbler'/'ZEN', '\"I don\'t care when the hedrons awoke. Why is the question that really matters.\"\n—Anowon, the Ruin Sage').
card_multiverse_id('hedron scrabbler'/'ZEN', '193394').

card_in_set('hellfire mongrel', 'ZEN').
card_original_type('hellfire mongrel'/'ZEN', 'Creature — Elemental Hound').
card_original_text('hellfire mongrel'/'ZEN', 'At the beginning of each opponent\'s upkeep, if that player has two or fewer cards in hand, Hellfire Mongrel deals 2 damage to him or her.').
card_first_print('hellfire mongrel', 'ZEN').
card_image_name('hellfire mongrel'/'ZEN', 'hellfire mongrel').
card_uid('hellfire mongrel'/'ZEN', 'ZEN:Hellfire Mongrel:hellfire mongrel').
card_rarity('hellfire mongrel'/'ZEN', 'Uncommon').
card_artist('hellfire mongrel'/'ZEN', 'Dan Scott').
card_number('hellfire mongrel'/'ZEN', '130').
card_flavor_text('hellfire mongrel'/'ZEN', 'There is no fondness between the hound and its master. There is only a common appreciation of the hunt.').
card_multiverse_id('hellfire mongrel'/'ZEN', '192215').

card_in_set('hellkite charger', 'ZEN').
card_original_type('hellkite charger'/'ZEN', 'Creature — Dragon').
card_original_text('hellkite charger'/'ZEN', 'Flying, haste\nWhenever Hellkite Charger attacks, you may pay {5}{R}{R}. If you do, untap all attacking creatures and after this phase, there is an additional combat phase.').
card_first_print('hellkite charger', 'ZEN').
card_image_name('hellkite charger'/'ZEN', 'hellkite charger').
card_uid('hellkite charger'/'ZEN', 'ZEN:Hellkite Charger:hellkite charger').
card_rarity('hellkite charger'/'ZEN', 'Rare').
card_artist('hellkite charger'/'ZEN', 'Jaime Jones').
card_number('hellkite charger'/'ZEN', '131').
card_multiverse_id('hellkite charger'/'ZEN', '185727').

card_in_set('hideous end', 'ZEN').
card_original_type('hideous end'/'ZEN', 'Instant').
card_original_text('hideous end'/'ZEN', 'Destroy target nonblack creature. Its controller loses 2 life.').
card_image_name('hideous end'/'ZEN', 'hideous end').
card_uid('hideous end'/'ZEN', 'ZEN:Hideous End:hideous end').
card_rarity('hideous end'/'ZEN', 'Common').
card_artist('hideous end'/'ZEN', 'Zoltan Boros & Gabor Szikszai').
card_number('hideous end'/'ZEN', '98').
card_flavor_text('hideous end'/'ZEN', '\"A little dark magic won\'t stop me. The worse the curse, the better the prize.\"\n—Radavi, Joraga relic hunter, last words').
card_multiverse_id('hideous end'/'ZEN', '180435').

card_in_set('highland berserker', 'ZEN').
card_original_type('highland berserker'/'ZEN', 'Creature — Human Berserker Ally').
card_original_text('highland berserker'/'ZEN', 'Whenever Highland Berserker or another Ally enters the battlefield under your control, you may have Ally creatures you control gain first strike until end of turn.').
card_first_print('highland berserker', 'ZEN').
card_image_name('highland berserker'/'ZEN', 'highland berserker').
card_uid('highland berserker'/'ZEN', 'ZEN:Highland Berserker:highland berserker').
card_rarity('highland berserker'/'ZEN', 'Common').
card_artist('highland berserker'/'ZEN', 'Chris Rahn').
card_number('highland berserker'/'ZEN', '132').
card_flavor_text('highland berserker'/'ZEN', 'He gets paid by the axe.').
card_multiverse_id('highland berserker'/'ZEN', '180467').

card_in_set('inferno trap', 'ZEN').
card_original_type('inferno trap'/'ZEN', 'Instant — Trap').
card_original_text('inferno trap'/'ZEN', 'If you\'ve been dealt damage by two or more creatures this turn, you may pay {R} rather than pay Inferno Trap\'s mana cost.\nInferno Trap deals 4 damage to target creature.').
card_first_print('inferno trap', 'ZEN').
card_image_name('inferno trap'/'ZEN', 'inferno trap').
card_uid('inferno trap'/'ZEN', 'ZEN:Inferno Trap:inferno trap').
card_rarity('inferno trap'/'ZEN', 'Uncommon').
card_artist('inferno trap'/'ZEN', 'Philip Straub').
card_number('inferno trap'/'ZEN', '133').
card_flavor_text('inferno trap'/'ZEN', '\"Do you smell something burning?\"').
card_multiverse_id('inferno trap'/'ZEN', '197530').

card_in_set('into the roil', 'ZEN').
card_original_type('into the roil'/'ZEN', 'Instant').
card_original_text('into the roil'/'ZEN', 'Kicker {1}{U} (You may pay an additional {1}{U} as you cast this spell.)\nReturn target nonland permanent to its owner\'s hand. If Into the Roil was kicked, draw a card.').
card_first_print('into the roil', 'ZEN').
card_image_name('into the roil'/'ZEN', 'into the roil').
card_uid('into the roil'/'ZEN', 'ZEN:Into the Roil:into the roil').
card_rarity('into the roil'/'ZEN', 'Common').
card_artist('into the roil'/'ZEN', 'Kieran Yanner').
card_number('into the roil'/'ZEN', '48').
card_flavor_text('into the roil'/'ZEN', '\"Roil tide! Roil tide! Tie yourselves down!\"').
card_multiverse_id('into the roil'/'ZEN', '178151').

card_in_set('iona, shield of emeria', 'ZEN').
card_original_type('iona, shield of emeria'/'ZEN', 'Legendary Creature — Angel').
card_original_text('iona, shield of emeria'/'ZEN', 'Flying\nAs Iona, Shield of Emeria enters the battlefield, choose a color.\nYour opponents can\'t cast spells of the chosen color.').
card_first_print('iona, shield of emeria', 'ZEN').
card_image_name('iona, shield of emeria'/'ZEN', 'iona, shield of emeria').
card_uid('iona, shield of emeria'/'ZEN', 'ZEN:Iona, Shield of Emeria:iona, shield of emeria').
card_rarity('iona, shield of emeria'/'ZEN', 'Mythic Rare').
card_artist('iona, shield of emeria'/'ZEN', 'Jason Chan').
card_number('iona, shield of emeria'/'ZEN', '13').
card_flavor_text('iona, shield of emeria'/'ZEN', 'No more shall the righteous cower before evil.').
card_multiverse_id('iona, shield of emeria'/'ZEN', '190407').

card_in_set('ior ruin expedition', 'ZEN').
card_original_type('ior ruin expedition'/'ZEN', 'Enchantment').
card_original_text('ior ruin expedition'/'ZEN', 'Landfall — Whenever a land enters the battlefield under your control, you may put a quest counter on Ior Ruin Expedition.\nRemove three quest counters from Ior Ruin Expedition and sacrifice it: Draw two cards.').
card_first_print('ior ruin expedition', 'ZEN').
card_image_name('ior ruin expedition'/'ZEN', 'ior ruin expedition').
card_uid('ior ruin expedition'/'ZEN', 'ZEN:Ior Ruin Expedition:ior ruin expedition').
card_rarity('ior ruin expedition'/'ZEN', 'Common').
card_artist('ior ruin expedition'/'ZEN', 'Chris Rahn').
card_number('ior ruin expedition'/'ZEN', '49').
card_multiverse_id('ior ruin expedition'/'ZEN', '185711').

card_in_set('island', 'ZEN').
card_original_type('island'/'ZEN', 'Basic Land — Island').
card_original_text('island'/'ZEN', 'U').
card_image_name('island'/'ZEN', 'island1').
card_uid('island'/'ZEN', 'ZEN:Island:island1').
card_rarity('island'/'ZEN', 'Basic Land').
card_artist('island'/'ZEN', 'John Avon').
card_number('island'/'ZEN', '234').
card_multiverse_id('island'/'ZEN', '195187').

card_in_set('island', 'ZEN').
card_original_type('island'/'ZEN', 'Basic Land — Island').
card_original_text('island'/'ZEN', 'U').
card_image_name('island'/'ZEN', 'island1a').
card_uid('island'/'ZEN', 'ZEN:Island:island1a').
card_rarity('island'/'ZEN', 'Basic Land').
card_artist('island'/'ZEN', 'John Avon').
card_number('island'/'ZEN', '234a').
card_multiverse_id('island'/'ZEN', '201966').

card_in_set('island', 'ZEN').
card_original_type('island'/'ZEN', 'Basic Land — Island').
card_original_text('island'/'ZEN', 'U').
card_image_name('island'/'ZEN', 'island2').
card_uid('island'/'ZEN', 'ZEN:Island:island2').
card_rarity('island'/'ZEN', 'Basic Land').
card_artist('island'/'ZEN', 'Jung Park').
card_number('island'/'ZEN', '235').
card_multiverse_id('island'/'ZEN', '201964').

card_in_set('island', 'ZEN').
card_original_type('island'/'ZEN', 'Basic Land — Island').
card_original_text('island'/'ZEN', 'U').
card_image_name('island'/'ZEN', 'island2a').
card_uid('island'/'ZEN', 'ZEN:Island:island2a').
card_rarity('island'/'ZEN', 'Basic Land').
card_artist('island'/'ZEN', 'Jung Park').
card_number('island'/'ZEN', '235a').
card_multiverse_id('island'/'ZEN', '195165').

card_in_set('island', 'ZEN').
card_original_type('island'/'ZEN', 'Basic Land — Island').
card_original_text('island'/'ZEN', 'U').
card_image_name('island'/'ZEN', 'island3').
card_uid('island'/'ZEN', 'ZEN:Island:island3').
card_rarity('island'/'ZEN', 'Basic Land').
card_artist('island'/'ZEN', 'Véronique Meignaud').
card_number('island'/'ZEN', '236').
card_multiverse_id('island'/'ZEN', '201963').

card_in_set('island', 'ZEN').
card_original_type('island'/'ZEN', 'Basic Land — Island').
card_original_text('island'/'ZEN', 'U').
card_image_name('island'/'ZEN', 'island3a').
card_uid('island'/'ZEN', 'ZEN:Island:island3a').
card_rarity('island'/'ZEN', 'Basic Land').
card_artist('island'/'ZEN', 'Véronique Meignaud').
card_number('island'/'ZEN', '236a').
card_multiverse_id('island'/'ZEN', '195161').

card_in_set('island', 'ZEN').
card_original_type('island'/'ZEN', 'Basic Land — Island').
card_original_text('island'/'ZEN', 'U').
card_image_name('island'/'ZEN', 'island4').
card_uid('island'/'ZEN', 'ZEN:Island:island4').
card_rarity('island'/'ZEN', 'Basic Land').
card_artist('island'/'ZEN', 'Vincent Proce').
card_number('island'/'ZEN', '237').
card_multiverse_id('island'/'ZEN', '201965').

card_in_set('island', 'ZEN').
card_original_type('island'/'ZEN', 'Basic Land — Island').
card_original_text('island'/'ZEN', 'U').
card_image_name('island'/'ZEN', 'island4a').
card_uid('island'/'ZEN', 'ZEN:Island:island4a').
card_rarity('island'/'ZEN', 'Basic Land').
card_artist('island'/'ZEN', 'Vincent Proce').
card_number('island'/'ZEN', '237a').
card_multiverse_id('island'/'ZEN', '195170').

card_in_set('joraga bard', 'ZEN').
card_original_type('joraga bard'/'ZEN', 'Creature — Elf Rogue Ally').
card_original_text('joraga bard'/'ZEN', 'Whenever Joraga Bard or another Ally enters the battlefield under your control, you may have Ally creatures you control gain vigilance until end of turn.').
card_first_print('joraga bard', 'ZEN').
card_image_name('joraga bard'/'ZEN', 'joraga bard').
card_uid('joraga bard'/'ZEN', 'ZEN:Joraga Bard:joraga bard').
card_rarity('joraga bard'/'ZEN', 'Common').
card_artist('joraga bard'/'ZEN', 'Volkan Baga').
card_number('joraga bard'/'ZEN', '166').
card_flavor_text('joraga bard'/'ZEN', 'Adventuring bards recite Chanter\'s Epics, never-ending tales that keep their comrades\' minds alert to danger.').
card_multiverse_id('joraga bard'/'ZEN', '192227').

card_in_set('journey to nowhere', 'ZEN').
card_original_type('journey to nowhere'/'ZEN', 'Enchantment').
card_original_text('journey to nowhere'/'ZEN', 'When Journey to Nowhere enters the battlefield, exile target creature.\nWhen Journey to Nowhere leaves the battlefield, return the exiled card to the battlefield under its owner\'s control.').
card_first_print('journey to nowhere', 'ZEN').
card_image_name('journey to nowhere'/'ZEN', 'journey to nowhere').
card_uid('journey to nowhere'/'ZEN', 'ZEN:Journey to Nowhere:journey to nowhere').
card_rarity('journey to nowhere'/'ZEN', 'Common').
card_artist('journey to nowhere'/'ZEN', 'Warren Mahy').
card_number('journey to nowhere'/'ZEN', '14').
card_multiverse_id('journey to nowhere'/'ZEN', '177505').

card_in_set('jwar isle refuge', 'ZEN').
card_original_type('jwar isle refuge'/'ZEN', 'Land').
card_original_text('jwar isle refuge'/'ZEN', 'Jwar Isle Refuge enters the battlefield tapped.\nWhen Jwar Isle Refuge enters the battlefield, you gain 1 life.\n{T}: Add {U} or {B} to your mana pool.').
card_first_print('jwar isle refuge', 'ZEN').
card_image_name('jwar isle refuge'/'ZEN', 'jwar isle refuge').
card_uid('jwar isle refuge'/'ZEN', 'ZEN:Jwar Isle Refuge:jwar isle refuge').
card_rarity('jwar isle refuge'/'ZEN', 'Uncommon').
card_artist('jwar isle refuge'/'ZEN', 'Cyril Van Der Haegen').
card_number('jwar isle refuge'/'ZEN', '215').
card_multiverse_id('jwar isle refuge'/'ZEN', '189627').

card_in_set('kabira crossroads', 'ZEN').
card_original_type('kabira crossroads'/'ZEN', 'Land').
card_original_text('kabira crossroads'/'ZEN', 'Kabira Crossroads enters the battlefield tapped.\nWhen Kabira Crossroads enters the battlefield, you gain 2 life.\n{T}: Add {W} to your mana pool.').
card_first_print('kabira crossroads', 'ZEN').
card_image_name('kabira crossroads'/'ZEN', 'kabira crossroads').
card_uid('kabira crossroads'/'ZEN', 'ZEN:Kabira Crossroads:kabira crossroads').
card_rarity('kabira crossroads'/'ZEN', 'Common').
card_artist('kabira crossroads'/'ZEN', 'James Paick').
card_number('kabira crossroads'/'ZEN', '216').
card_multiverse_id('kabira crossroads'/'ZEN', '169963').

card_in_set('kabira evangel', 'ZEN').
card_original_type('kabira evangel'/'ZEN', 'Creature — Human Cleric Ally').
card_original_text('kabira evangel'/'ZEN', 'Whenever Kabira Evangel or another Ally enters the battlefield under your control, you may choose a color. If you do, Allies you control gain protection from the chosen color until end of turn.').
card_first_print('kabira evangel', 'ZEN').
card_image_name('kabira evangel'/'ZEN', 'kabira evangel').
card_uid('kabira evangel'/'ZEN', 'ZEN:Kabira Evangel:kabira evangel').
card_rarity('kabira evangel'/'ZEN', 'Rare').
card_artist('kabira evangel'/'ZEN', 'Eric Deschamps').
card_number('kabira evangel'/'ZEN', '15').
card_multiverse_id('kabira evangel'/'ZEN', '180347').

card_in_set('kalitas, bloodchief of ghet', 'ZEN').
card_original_type('kalitas, bloodchief of ghet'/'ZEN', 'Legendary Creature — Vampire Warrior').
card_original_text('kalitas, bloodchief of ghet'/'ZEN', '{B}{B}{B}, {T}: Destroy target creature. If that creature is put into a graveyard this way, put a black Vampire creature token onto the battlefield. Its power is equal to that creature\'s power and its toughness is equal to that creature\'s toughness.').
card_first_print('kalitas, bloodchief of ghet', 'ZEN').
card_image_name('kalitas, bloodchief of ghet'/'ZEN', 'kalitas, bloodchief of ghet').
card_uid('kalitas, bloodchief of ghet'/'ZEN', 'ZEN:Kalitas, Bloodchief of Ghet:kalitas, bloodchief of ghet').
card_rarity('kalitas, bloodchief of ghet'/'ZEN', 'Mythic Rare').
card_artist('kalitas, bloodchief of ghet'/'ZEN', 'Todd Lockwood').
card_number('kalitas, bloodchief of ghet'/'ZEN', '99').
card_multiverse_id('kalitas, bloodchief of ghet'/'ZEN', '185704').

card_in_set('kazandu blademaster', 'ZEN').
card_original_type('kazandu blademaster'/'ZEN', 'Creature — Human Soldier Ally').
card_original_text('kazandu blademaster'/'ZEN', 'First strike, vigilance\nWhenever Kazandu Blademaster or another Ally enters the battlefield under your control, you may put a +1/+1 counter on Kazandu Blademaster.').
card_first_print('kazandu blademaster', 'ZEN').
card_image_name('kazandu blademaster'/'ZEN', 'kazandu blademaster').
card_uid('kazandu blademaster'/'ZEN', 'ZEN:Kazandu Blademaster:kazandu blademaster').
card_rarity('kazandu blademaster'/'ZEN', 'Uncommon').
card_artist('kazandu blademaster'/'ZEN', 'Michael Komarck').
card_number('kazandu blademaster'/'ZEN', '16').
card_flavor_text('kazandu blademaster'/'ZEN', '\"If you hire a sell-sword, you\'d better watch your back. Hire me, and I\'ll watch it for you.\"').
card_multiverse_id('kazandu blademaster'/'ZEN', '191356').

card_in_set('kazandu refuge', 'ZEN').
card_original_type('kazandu refuge'/'ZEN', 'Land').
card_original_text('kazandu refuge'/'ZEN', 'Kazandu Refuge enters the battlefield tapped.\nWhen Kazandu Refuge enters the battlefield, you gain 1 life.\n{T}: Add {R} or {G} to your mana pool.').
card_first_print('kazandu refuge', 'ZEN').
card_image_name('kazandu refuge'/'ZEN', 'kazandu refuge').
card_uid('kazandu refuge'/'ZEN', 'ZEN:Kazandu Refuge:kazandu refuge').
card_rarity('kazandu refuge'/'ZEN', 'Uncommon').
card_artist('kazandu refuge'/'ZEN', 'Franz Vohwinkel').
card_number('kazandu refuge'/'ZEN', '217').
card_multiverse_id('kazandu refuge'/'ZEN', '189635').

card_in_set('kazuul warlord', 'ZEN').
card_original_type('kazuul warlord'/'ZEN', 'Creature — Minotaur Warrior Ally').
card_original_text('kazuul warlord'/'ZEN', 'Whenever Kazuul Warlord or another Ally enters the battlefield under your control, you may put a +1/+1 counter on each Ally creature you control.').
card_first_print('kazuul warlord', 'ZEN').
card_image_name('kazuul warlord'/'ZEN', 'kazuul warlord').
card_uid('kazuul warlord'/'ZEN', 'ZEN:Kazuul Warlord:kazuul warlord').
card_rarity('kazuul warlord'/'ZEN', 'Rare').
card_artist('kazuul warlord'/'ZEN', 'Kev Walker').
card_number('kazuul warlord'/'ZEN', '134').
card_flavor_text('kazuul warlord'/'ZEN', 'On expeditions, minotaurs are worth their weight in gold. Fortunately for their paymasters, most minotaurs don\'t know how much they weigh.').
card_multiverse_id('kazuul warlord'/'ZEN', '190408').

card_in_set('khalni gem', 'ZEN').
card_original_type('khalni gem'/'ZEN', 'Artifact').
card_original_text('khalni gem'/'ZEN', 'When Khalni Gem enters the battlefield, return two lands you control to their owner\'s hand.\n{T}: Add two mana of any one color to your mana pool.').
card_first_print('khalni gem', 'ZEN').
card_image_name('khalni gem'/'ZEN', 'khalni gem').
card_uid('khalni gem'/'ZEN', 'ZEN:Khalni Gem:khalni gem').
card_rarity('khalni gem'/'ZEN', 'Uncommon').
card_artist('khalni gem'/'ZEN', 'Franz Vohwinkel').
card_number('khalni gem'/'ZEN', '205').
card_flavor_text('khalni gem'/'ZEN', 'A fragment of ancient power lost in the heart of Ora Ondar.').
card_multiverse_id('khalni gem'/'ZEN', '198519').

card_in_set('khalni heart expedition', 'ZEN').
card_original_type('khalni heart expedition'/'ZEN', 'Enchantment').
card_original_text('khalni heart expedition'/'ZEN', 'Landfall — Whenever a land enters the battlefield under your control, you may put a quest counter on Khalni Heart Expedition.\nRemove three quest counters from Khalni Heart Expedition and sacrifice it: Search your library for up to two basic land cards, put them onto the battlefield tapped, then shuffle your library.').
card_first_print('khalni heart expedition', 'ZEN').
card_image_name('khalni heart expedition'/'ZEN', 'khalni heart expedition').
card_uid('khalni heart expedition'/'ZEN', 'ZEN:Khalni Heart Expedition:khalni heart expedition').
card_rarity('khalni heart expedition'/'ZEN', 'Common').
card_artist('khalni heart expedition'/'ZEN', 'Jason Chan').
card_number('khalni heart expedition'/'ZEN', '167').
card_multiverse_id('khalni heart expedition'/'ZEN', '186320').

card_in_set('kor aeronaut', 'ZEN').
card_original_type('kor aeronaut'/'ZEN', 'Creature — Kor Soldier').
card_original_text('kor aeronaut'/'ZEN', 'Kicker {1}{W} (You may pay an additional {1}{W} as you cast this spell.)\nFlying\nWhen Kor Aeronaut enters the battlefield, if it was kicked, target creature gains flying until end of turn.').
card_first_print('kor aeronaut', 'ZEN').
card_image_name('kor aeronaut'/'ZEN', 'kor aeronaut').
card_uid('kor aeronaut'/'ZEN', 'ZEN:Kor Aeronaut:kor aeronaut').
card_rarity('kor aeronaut'/'ZEN', 'Uncommon').
card_artist('kor aeronaut'/'ZEN', 'Karl Kopinski').
card_number('kor aeronaut'/'ZEN', '17').
card_multiverse_id('kor aeronaut'/'ZEN', '177530').

card_in_set('kor cartographer', 'ZEN').
card_original_type('kor cartographer'/'ZEN', 'Creature — Kor Scout').
card_original_text('kor cartographer'/'ZEN', 'When Kor Cartographer enters the battlefield, you may search your library for a Plains card, put it onto the battlefield tapped, then shuffle your library.').
card_first_print('kor cartographer', 'ZEN').
card_image_name('kor cartographer'/'ZEN', 'kor cartographer').
card_uid('kor cartographer'/'ZEN', 'ZEN:Kor Cartographer:kor cartographer').
card_rarity('kor cartographer'/'ZEN', 'Common').
card_artist('kor cartographer'/'ZEN', 'Ryan Pancoast').
card_number('kor cartographer'/'ZEN', '18').
card_flavor_text('kor cartographer'/'ZEN', 'Kor have no concept of exploration. They return to homelands forgotten.').
card_multiverse_id('kor cartographer'/'ZEN', '170991').

card_in_set('kor duelist', 'ZEN').
card_original_type('kor duelist'/'ZEN', 'Creature — Kor Soldier').
card_original_text('kor duelist'/'ZEN', 'As long as Kor Duelist is equipped, it has double strike. (It deals both first-strike and regular combat damage.)').
card_image_name('kor duelist'/'ZEN', 'kor duelist').
card_uid('kor duelist'/'ZEN', 'ZEN:Kor Duelist:kor duelist').
card_rarity('kor duelist'/'ZEN', 'Uncommon').
card_artist('kor duelist'/'ZEN', 'Izzy').
card_number('kor duelist'/'ZEN', '19').
card_flavor_text('kor duelist'/'ZEN', '\"Swords cannot reach far enough. Chains cannot strike hard enough. An eternal dilemma, but a simple one.\"').
card_multiverse_id('kor duelist'/'ZEN', '177542').

card_in_set('kor hookmaster', 'ZEN').
card_original_type('kor hookmaster'/'ZEN', 'Creature — Kor Soldier').
card_original_text('kor hookmaster'/'ZEN', 'When Kor Hookmaster enters the battlefield, tap target creature an opponent controls. That creature doesn\'t untap during its controller\'s next untap step.').
card_first_print('kor hookmaster', 'ZEN').
card_image_name('kor hookmaster'/'ZEN', 'kor hookmaster').
card_uid('kor hookmaster'/'ZEN', 'ZEN:Kor Hookmaster:kor hookmaster').
card_rarity('kor hookmaster'/'ZEN', 'Common').
card_artist('kor hookmaster'/'ZEN', 'Wayne Reynolds').
card_number('kor hookmaster'/'ZEN', '20').
card_flavor_text('kor hookmaster'/'ZEN', '\"For us, a rope represents the ties that bind the kor. For you, it\'s more literal.\"').
card_multiverse_id('kor hookmaster'/'ZEN', '178131').

card_in_set('kor outfitter', 'ZEN').
card_original_type('kor outfitter'/'ZEN', 'Creature — Kor Soldier').
card_original_text('kor outfitter'/'ZEN', 'When Kor Outfitter enters the battlefield, you may attach target Equipment you control to target creature you control.').
card_first_print('kor outfitter', 'ZEN').
card_image_name('kor outfitter'/'ZEN', 'kor outfitter').
card_uid('kor outfitter'/'ZEN', 'ZEN:Kor Outfitter:kor outfitter').
card_rarity('kor outfitter'/'ZEN', 'Common').
card_artist('kor outfitter'/'ZEN', 'Kieran Yanner').
card_number('kor outfitter'/'ZEN', '21').
card_flavor_text('kor outfitter'/'ZEN', '\"We take only what we need to survive. Believe me, you will need this.\"').
card_multiverse_id('kor outfitter'/'ZEN', '189637').

card_in_set('kor sanctifiers', 'ZEN').
card_original_type('kor sanctifiers'/'ZEN', 'Creature — Kor Cleric').
card_original_text('kor sanctifiers'/'ZEN', 'Kicker {W} (You may pay an additional {W} as you cast this spell.)\nWhen Kor Sanctifiers enters the battlefield, if it was kicked, destroy target artifact or enchantment.').
card_image_name('kor sanctifiers'/'ZEN', 'kor sanctifiers').
card_uid('kor sanctifiers'/'ZEN', 'ZEN:Kor Sanctifiers:kor sanctifiers').
card_rarity('kor sanctifiers'/'ZEN', 'Common').
card_artist('kor sanctifiers'/'ZEN', 'Dan Scott').
card_number('kor sanctifiers'/'ZEN', '22').
card_flavor_text('kor sanctifiers'/'ZEN', '\"Why keep such trinkets? They only add weight to your travels.\"').
card_multiverse_id('kor sanctifiers'/'ZEN', '192214').

card_in_set('kor skyfisher', 'ZEN').
card_original_type('kor skyfisher'/'ZEN', 'Creature — Kor Soldier').
card_original_text('kor skyfisher'/'ZEN', 'Flying\nWhen Kor Skyfisher enters the battlefield, return a permanent you control to its owner\'s hand.').
card_image_name('kor skyfisher'/'ZEN', 'kor skyfisher').
card_uid('kor skyfisher'/'ZEN', 'ZEN:Kor Skyfisher:kor skyfisher').
card_rarity('kor skyfisher'/'ZEN', 'Common').
card_artist('kor skyfisher'/'ZEN', 'Dan Scott').
card_number('kor skyfisher'/'ZEN', '23').
card_flavor_text('kor skyfisher'/'ZEN', '\"Sometimes I snare the unexpected, but I know its purpose will be revealed in time.\"').
card_multiverse_id('kor skyfisher'/'ZEN', '178145').

card_in_set('kraken hatchling', 'ZEN').
card_original_type('kraken hatchling'/'ZEN', 'Creature — Kraken').
card_original_text('kraken hatchling'/'ZEN', '').
card_first_print('kraken hatchling', 'ZEN').
card_image_name('kraken hatchling'/'ZEN', 'kraken hatchling').
card_uid('kraken hatchling'/'ZEN', 'ZEN:Kraken Hatchling:kraken hatchling').
card_rarity('kraken hatchling'/'ZEN', 'Common').
card_artist('kraken hatchling'/'ZEN', 'Jason Felix').
card_number('kraken hatchling'/'ZEN', '50').
card_flavor_text('kraken hatchling'/'ZEN', 'A spike and a maul are needed to crack their shells, but the taste is worth the effort.').
card_multiverse_id('kraken hatchling'/'ZEN', '177585').

card_in_set('landbind ritual', 'ZEN').
card_original_type('landbind ritual'/'ZEN', 'Sorcery').
card_original_text('landbind ritual'/'ZEN', 'You gain 2 life for each Plains you control.').
card_first_print('landbind ritual', 'ZEN').
card_image_name('landbind ritual'/'ZEN', 'landbind ritual').
card_uid('landbind ritual'/'ZEN', 'ZEN:Landbind Ritual:landbind ritual').
card_rarity('landbind ritual'/'ZEN', 'Uncommon').
card_artist('landbind ritual'/'ZEN', 'Steve Prescott').
card_number('landbind ritual'/'ZEN', '24').
card_flavor_text('landbind ritual'/'ZEN', '\"Honor this place, for our children\'s children will stand here and speak these same words again.\"\n—Ayli, Kamsa cleric').
card_multiverse_id('landbind ritual'/'ZEN', '188999').

card_in_set('lavaball trap', 'ZEN').
card_original_type('lavaball trap'/'ZEN', 'Instant — Trap').
card_original_text('lavaball trap'/'ZEN', 'If an opponent had two or more lands enter the battlefield under his or her control this turn, you may pay {3}{R}{R} rather than pay Lavaball Trap\'s mana cost.\nDestroy two target lands. Lavaball Trap deals 4 damage to each creature.').
card_first_print('lavaball trap', 'ZEN').
card_image_name('lavaball trap'/'ZEN', 'lavaball trap').
card_uid('lavaball trap'/'ZEN', 'ZEN:Lavaball Trap:lavaball trap').
card_rarity('lavaball trap'/'ZEN', 'Rare').
card_artist('lavaball trap'/'ZEN', 'Zoltan Boros & Gabor Szikszai').
card_number('lavaball trap'/'ZEN', '135').
card_multiverse_id('lavaball trap'/'ZEN', '197528').

card_in_set('lethargy trap', 'ZEN').
card_original_type('lethargy trap'/'ZEN', 'Instant — Trap').
card_original_text('lethargy trap'/'ZEN', 'If three or more creatures are attacking, you may pay {U} rather than pay Lethargy Trap\'s mana cost.\nAttacking creatures get -3/-0 until end of turn.').
card_first_print('lethargy trap', 'ZEN').
card_image_name('lethargy trap'/'ZEN', 'lethargy trap').
card_uid('lethargy trap'/'ZEN', 'ZEN:Lethargy Trap:lethargy trap').
card_rarity('lethargy trap'/'ZEN', 'Common').
card_artist('lethargy trap'/'ZEN', 'Anthony Francisco').
card_number('lethargy trap'/'ZEN', '51').
card_flavor_text('lethargy trap'/'ZEN', 'Suddenly, Zurdi didn\'t care about treasure, glory, food . . . or the drakes circling above.').
card_multiverse_id('lethargy trap'/'ZEN', '197534').

card_in_set('living tsunami', 'ZEN').
card_original_type('living tsunami'/'ZEN', 'Creature — Elemental').
card_original_text('living tsunami'/'ZEN', 'Flying\nAt the beginning of your upkeep, sacrifice Living Tsunami unless you return a land you control to its owner\'s hand.').
card_first_print('living tsunami', 'ZEN').
card_image_name('living tsunami'/'ZEN', 'living tsunami').
card_uid('living tsunami'/'ZEN', 'ZEN:Living Tsunami:living tsunami').
card_rarity('living tsunami'/'ZEN', 'Uncommon').
card_artist('living tsunami'/'ZEN', 'Matt Cavotta').
card_number('living tsunami'/'ZEN', '52').
card_flavor_text('living tsunami'/'ZEN', 'At low tide it slumbers. At high tide it devours.').
card_multiverse_id('living tsunami'/'ZEN', '189636').

card_in_set('lorthos, the tidemaker', 'ZEN').
card_original_type('lorthos, the tidemaker'/'ZEN', 'Legendary Creature — Octopus').
card_original_text('lorthos, the tidemaker'/'ZEN', 'Whenever Lorthos, the Tidemaker attacks, you may pay {8}. If you do, tap up to eight target permanents. Those permanents don\'t untap during their controllers\' next untap steps.').
card_first_print('lorthos, the tidemaker', 'ZEN').
card_image_name('lorthos, the tidemaker'/'ZEN', 'lorthos, the tidemaker').
card_uid('lorthos, the tidemaker'/'ZEN', 'ZEN:Lorthos, the Tidemaker:lorthos, the tidemaker').
card_rarity('lorthos, the tidemaker'/'ZEN', 'Mythic Rare').
card_artist('lorthos, the tidemaker'/'ZEN', 'Kekai Kotaki').
card_number('lorthos, the tidemaker'/'ZEN', '53').
card_flavor_text('lorthos, the tidemaker'/'ZEN', 'When Lorthos emerges from his deepwater realm, the tides bow to his will and the coastline cowers in his presence.').
card_multiverse_id('lorthos, the tidemaker'/'ZEN', '191369').

card_in_set('lotus cobra', 'ZEN').
card_original_type('lotus cobra'/'ZEN', 'Creature — Snake').
card_original_text('lotus cobra'/'ZEN', 'Landfall — Whenever a land enters the battlefield under your control, you may add one mana of any color to your mana pool.').
card_image_name('lotus cobra'/'ZEN', 'lotus cobra').
card_uid('lotus cobra'/'ZEN', 'ZEN:Lotus Cobra:lotus cobra').
card_rarity('lotus cobra'/'ZEN', 'Mythic Rare').
card_artist('lotus cobra'/'ZEN', 'Chippy').
card_number('lotus cobra'/'ZEN', '168').
card_flavor_text('lotus cobra'/'ZEN', 'Its scales contain the essence of thousands of lotus blooms.').
card_multiverse_id('lotus cobra'/'ZEN', '185749').

card_in_set('lullmage mentor', 'ZEN').
card_original_type('lullmage mentor'/'ZEN', 'Creature — Merfolk Wizard').
card_original_text('lullmage mentor'/'ZEN', 'Whenever a spell or ability you control counters a spell, you may put a 1/1 blue Merfolk creature token onto the battlefield.\nTap seven untapped Merfolk you control: Counter target spell.').
card_first_print('lullmage mentor', 'ZEN').
card_image_name('lullmage mentor'/'ZEN', 'lullmage mentor').
card_uid('lullmage mentor'/'ZEN', 'ZEN:Lullmage Mentor:lullmage mentor').
card_rarity('lullmage mentor'/'ZEN', 'Rare').
card_artist('lullmage mentor'/'ZEN', 'Jaime Jones').
card_number('lullmage mentor'/'ZEN', '54').
card_flavor_text('lullmage mentor'/'ZEN', '\"Many voices are needed to quiet this land.\"').
card_multiverse_id('lullmage mentor'/'ZEN', '190404').

card_in_set('luminarch ascension', 'ZEN').
card_original_type('luminarch ascension'/'ZEN', 'Enchantment').
card_original_text('luminarch ascension'/'ZEN', 'At the beginning of each opponent\'s end step, if you didn\'t lose life this turn, you may put a quest counter on Luminarch Ascension. (Damage causes loss of life.)\n{1}{W}: Put a 4/4 white Angel creature token with flying onto the battlefield. Activate this ability only if Luminarch Ascension has four or more quest counters on it.').
card_first_print('luminarch ascension', 'ZEN').
card_image_name('luminarch ascension'/'ZEN', 'luminarch ascension').
card_uid('luminarch ascension'/'ZEN', 'ZEN:Luminarch Ascension:luminarch ascension').
card_rarity('luminarch ascension'/'ZEN', 'Rare').
card_artist('luminarch ascension'/'ZEN', 'Michael Komarck').
card_number('luminarch ascension'/'ZEN', '25').
card_multiverse_id('luminarch ascension'/'ZEN', '197889').

card_in_set('magma rift', 'ZEN').
card_original_type('magma rift'/'ZEN', 'Sorcery').
card_original_text('magma rift'/'ZEN', 'As an additional cost to cast Magma Rift, sacrifice a land.\nMagma Rift deals 5 damage to target creature.').
card_first_print('magma rift', 'ZEN').
card_image_name('magma rift'/'ZEN', 'magma rift').
card_uid('magma rift'/'ZEN', 'ZEN:Magma Rift:magma rift').
card_rarity('magma rift'/'ZEN', 'Common').
card_artist('magma rift'/'ZEN', 'Jung Park').
card_number('magma rift'/'ZEN', '136').
card_flavor_text('magma rift'/'ZEN', '\"Lighting a fire needs kindling and heat. You be the kindling. I\'ll bring the heat.\"\n—Chandra Nalaar').
card_multiverse_id('magma rift'/'ZEN', '180352').

card_in_set('magosi, the waterveil', 'ZEN').
card_original_type('magosi, the waterveil'/'ZEN', 'Land').
card_original_text('magosi, the waterveil'/'ZEN', 'Magosi, the Waterveil enters the battlefield tapped.\n{T}: Add {U} to your mana pool.\n{U}, {T}: Put an eon counter on Magosi, the Waterveil. Skip your next turn.\n{T}, Remove an eon counter from Magosi, the Waterveil and return it to its owner\'s hand: Take an extra turn after this one.').
card_first_print('magosi, the waterveil', 'ZEN').
card_image_name('magosi, the waterveil'/'ZEN', 'magosi, the waterveil').
card_uid('magosi, the waterveil'/'ZEN', 'ZEN:Magosi, the Waterveil:magosi, the waterveil').
card_rarity('magosi, the waterveil'/'ZEN', 'Rare').
card_artist('magosi, the waterveil'/'ZEN', 'Eric Deschamps').
card_number('magosi, the waterveil'/'ZEN', '218').
card_multiverse_id('magosi, the waterveil'/'ZEN', '190412').

card_in_set('makindi shieldmate', 'ZEN').
card_original_type('makindi shieldmate'/'ZEN', 'Creature — Kor Soldier Ally').
card_original_text('makindi shieldmate'/'ZEN', 'Defender\nWhenever Makindi Shieldmate or another Ally enters the battlefield under your control, you may put a +1/+1 counter on Makindi Shieldmate.').
card_first_print('makindi shieldmate', 'ZEN').
card_image_name('makindi shieldmate'/'ZEN', 'makindi shieldmate').
card_uid('makindi shieldmate'/'ZEN', 'ZEN:Makindi Shieldmate:makindi shieldmate').
card_rarity('makindi shieldmate'/'ZEN', 'Common').
card_artist('makindi shieldmate'/'ZEN', 'Howard Lyon').
card_number('makindi shieldmate'/'ZEN', '26').
card_flavor_text('makindi shieldmate'/'ZEN', 'The more who rely on him, the more resolute he becomes.').
card_multiverse_id('makindi shieldmate'/'ZEN', '195628').

card_in_set('malakir bloodwitch', 'ZEN').
card_original_type('malakir bloodwitch'/'ZEN', 'Creature — Vampire Shaman').
card_original_text('malakir bloodwitch'/'ZEN', 'Flying, protection from white\nWhen Malakir Bloodwitch enters the battlefield, each opponent loses life equal to the number of Vampires you control. You gain life equal to the life lost this way.').
card_first_print('malakir bloodwitch', 'ZEN').
card_image_name('malakir bloodwitch'/'ZEN', 'malakir bloodwitch').
card_uid('malakir bloodwitch'/'ZEN', 'ZEN:Malakir Bloodwitch:malakir bloodwitch').
card_rarity('malakir bloodwitch'/'ZEN', 'Rare').
card_artist('malakir bloodwitch'/'ZEN', 'Shelly Wan').
card_number('malakir bloodwitch'/'ZEN', '100').
card_multiverse_id('malakir bloodwitch'/'ZEN', '190398').

card_in_set('mark of mutiny', 'ZEN').
card_original_type('mark of mutiny'/'ZEN', 'Sorcery').
card_original_text('mark of mutiny'/'ZEN', 'Gain control of target creature until end of turn. Put a +1/+1 counter on it and untap it. That creature gains haste until end of turn.').
card_first_print('mark of mutiny', 'ZEN').
card_image_name('mark of mutiny'/'ZEN', 'mark of mutiny').
card_uid('mark of mutiny'/'ZEN', 'ZEN:Mark of Mutiny:mark of mutiny').
card_rarity('mark of mutiny'/'ZEN', 'Uncommon').
card_artist('mark of mutiny'/'ZEN', 'Mike Bierek').
card_number('mark of mutiny'/'ZEN', '137').
card_flavor_text('mark of mutiny'/'ZEN', 'The flame of anger is hard to douse once lit.').
card_multiverse_id('mark of mutiny'/'ZEN', '186324').

card_in_set('marsh casualties', 'ZEN').
card_original_type('marsh casualties'/'ZEN', 'Sorcery').
card_original_text('marsh casualties'/'ZEN', 'Kicker {3} (You may pay an additional {3} as you cast this spell.)\nCreatures target player controls get -1/-1 until end of turn. If Marsh Casualties was kicked, those creatures get -2/-2 until end of turn instead.').
card_first_print('marsh casualties', 'ZEN').
card_image_name('marsh casualties'/'ZEN', 'marsh casualties').
card_uid('marsh casualties'/'ZEN', 'ZEN:Marsh Casualties:marsh casualties').
card_rarity('marsh casualties'/'ZEN', 'Uncommon').
card_artist('marsh casualties'/'ZEN', 'Scott Chou').
card_number('marsh casualties'/'ZEN', '101').
card_multiverse_id('marsh casualties'/'ZEN', '177543').

card_in_set('marsh flats', 'ZEN').
card_original_type('marsh flats'/'ZEN', 'Land').
card_original_text('marsh flats'/'ZEN', '{T}, Pay 1 life, Sacrifice Marsh Flats: Search your library for a Plains or Swamp card and put it onto the battlefield. Then shuffle your library.').
card_first_print('marsh flats', 'ZEN').
card_image_name('marsh flats'/'ZEN', 'marsh flats').
card_uid('marsh flats'/'ZEN', 'ZEN:Marsh Flats:marsh flats').
card_rarity('marsh flats'/'ZEN', 'Rare').
card_artist('marsh flats'/'ZEN', 'Izzy').
card_number('marsh flats'/'ZEN', '219').
card_multiverse_id('marsh flats'/'ZEN', '191371').

card_in_set('merfolk seastalkers', 'ZEN').
card_original_type('merfolk seastalkers'/'ZEN', 'Creature — Merfolk Scout').
card_original_text('merfolk seastalkers'/'ZEN', 'Islandwalk\n{2}{U}: Tap target creature without flying.').
card_first_print('merfolk seastalkers', 'ZEN').
card_image_name('merfolk seastalkers'/'ZEN', 'merfolk seastalkers').
card_uid('merfolk seastalkers'/'ZEN', 'ZEN:Merfolk Seastalkers:merfolk seastalkers').
card_rarity('merfolk seastalkers'/'ZEN', 'Uncommon').
card_artist('merfolk seastalkers'/'ZEN', 'Eric Deschamps').
card_number('merfolk seastalkers'/'ZEN', '55').
card_flavor_text('merfolk seastalkers'/'ZEN', '\"Do they seek knowledge or wealth? Are they bandits or benefactors? It depends on who is chanting the tale.\"\n—Nikou, Joraga bard').
card_multiverse_id('merfolk seastalkers'/'ZEN', '189625').

card_in_set('merfolk wayfinder', 'ZEN').
card_original_type('merfolk wayfinder'/'ZEN', 'Creature — Merfolk Scout').
card_original_text('merfolk wayfinder'/'ZEN', 'Flying\nWhen Merfolk Wayfinder enters the battlefield, reveal the top three cards of your library. Put all Island cards revealed this way into your hand and the rest on the bottom of your library in any order.').
card_first_print('merfolk wayfinder', 'ZEN').
card_image_name('merfolk wayfinder'/'ZEN', 'merfolk wayfinder').
card_uid('merfolk wayfinder'/'ZEN', 'ZEN:Merfolk Wayfinder:merfolk wayfinder').
card_rarity('merfolk wayfinder'/'ZEN', 'Uncommon').
card_artist('merfolk wayfinder'/'ZEN', 'Christopher Moeller').
card_number('merfolk wayfinder'/'ZEN', '56').
card_multiverse_id('merfolk wayfinder'/'ZEN', '170978').

card_in_set('mind sludge', 'ZEN').
card_original_type('mind sludge'/'ZEN', 'Sorcery').
card_original_text('mind sludge'/'ZEN', 'Target player discards a card for each Swamp you control.').
card_image_name('mind sludge'/'ZEN', 'mind sludge').
card_uid('mind sludge'/'ZEN', 'ZEN:Mind Sludge:mind sludge').
card_rarity('mind sludge'/'ZEN', 'Uncommon').
card_artist('mind sludge'/'ZEN', 'Howard Lyon').
card_number('mind sludge'/'ZEN', '102').
card_flavor_text('mind sludge'/'ZEN', '\"Guard your thoughts. You never know who might wish to take them from you.\"\n—Kalitas, Bloodchief of Ghet').
card_multiverse_id('mind sludge'/'ZEN', '193403').

card_in_set('mindbreak trap', 'ZEN').
card_original_type('mindbreak trap'/'ZEN', 'Instant — Trap').
card_original_text('mindbreak trap'/'ZEN', 'If an opponent cast three or more spells this turn, you may pay {0} rather than pay Mindbreak Trap\'s mana cost.\nExile any number of target spells.').
card_first_print('mindbreak trap', 'ZEN').
card_image_name('mindbreak trap'/'ZEN', 'mindbreak trap').
card_uid('mindbreak trap'/'ZEN', 'ZEN:Mindbreak Trap:mindbreak trap').
card_rarity('mindbreak trap'/'ZEN', 'Mythic Rare').
card_artist('mindbreak trap'/'ZEN', 'Christopher Moeller').
card_number('mindbreak trap'/'ZEN', '57').
card_flavor_text('mindbreak trap'/'ZEN', '\"Life is a maze. This is one of its dead ends.\"\n—Noyan Dar, Tazeem lullmage').
card_multiverse_id('mindbreak trap'/'ZEN', '197532').

card_in_set('mindless null', 'ZEN').
card_original_type('mindless null'/'ZEN', 'Creature — Zombie').
card_original_text('mindless null'/'ZEN', 'Mindless Null can\'t block unless you control a Vampire.').
card_first_print('mindless null', 'ZEN').
card_image_name('mindless null'/'ZEN', 'mindless null').
card_uid('mindless null'/'ZEN', 'ZEN:Mindless Null:mindless null').
card_rarity('mindless null'/'ZEN', 'Common').
card_artist('mindless null'/'ZEN', 'Karl Kopinski').
card_number('mindless null'/'ZEN', '103').
card_flavor_text('mindless null'/'ZEN', 'Only a bloodchief can create vampires. The rest sire nulls, who become mindless beasts in their former bodies.').
card_multiverse_id('mindless null'/'ZEN', '180415').

card_in_set('mire blight', 'ZEN').
card_original_type('mire blight'/'ZEN', 'Enchantment — Aura').
card_original_text('mire blight'/'ZEN', 'Enchant creature\nWhen enchanted creature is dealt damage, destroy it.').
card_first_print('mire blight', 'ZEN').
card_image_name('mire blight'/'ZEN', 'mire blight').
card_uid('mire blight'/'ZEN', 'ZEN:Mire Blight:mire blight').
card_rarity('mire blight'/'ZEN', 'Common').
card_artist('mire blight'/'ZEN', 'Dave Kendall').
card_number('mire blight'/'ZEN', '104').
card_flavor_text('mire blight'/'ZEN', '\"Is there anything in Guul Draz that doesn\'t suck the life out of you?\"\n—Tarsa, Sea Gate sell-sword').
card_multiverse_id('mire blight'/'ZEN', '190401').

card_in_set('misty rainforest', 'ZEN').
card_original_type('misty rainforest'/'ZEN', 'Land').
card_original_text('misty rainforest'/'ZEN', '{T}, Pay 1 life, Sacrifice Misty Rainforest: Search your library for a Forest or Island card and put it onto the battlefield. Then shuffle your library.').
card_first_print('misty rainforest', 'ZEN').
card_image_name('misty rainforest'/'ZEN', 'misty rainforest').
card_uid('misty rainforest'/'ZEN', 'ZEN:Misty Rainforest:misty rainforest').
card_rarity('misty rainforest'/'ZEN', 'Rare').
card_artist('misty rainforest'/'ZEN', 'Shelly Wan').
card_number('misty rainforest'/'ZEN', '220').
card_multiverse_id('misty rainforest'/'ZEN', '190413').

card_in_set('mold shambler', 'ZEN').
card_original_type('mold shambler'/'ZEN', 'Creature — Fungus Beast').
card_original_text('mold shambler'/'ZEN', 'Kicker {1}{G} (You may pay an additional {1}{G} as you cast this spell.)\nWhen Mold Shambler enters the battlefield, if it was kicked, destroy target noncreature permanent.').
card_first_print('mold shambler', 'ZEN').
card_image_name('mold shambler'/'ZEN', 'mold shambler').
card_uid('mold shambler'/'ZEN', 'ZEN:Mold Shambler:mold shambler').
card_rarity('mold shambler'/'ZEN', 'Common').
card_artist('mold shambler'/'ZEN', 'Karl Kopinski').
card_number('mold shambler'/'ZEN', '169').
card_flavor_text('mold shambler'/'ZEN', 'When civilization encroaches on nature, Zendikar encroaches back.').
card_multiverse_id('mold shambler'/'ZEN', '183414').

card_in_set('molten ravager', 'ZEN').
card_original_type('molten ravager'/'ZEN', 'Creature — Elemental').
card_original_text('molten ravager'/'ZEN', '{R}: Molten Ravager gets +1/+0 until end of turn.').
card_first_print('molten ravager', 'ZEN').
card_image_name('molten ravager'/'ZEN', 'molten ravager').
card_uid('molten ravager'/'ZEN', 'ZEN:Molten Ravager:molten ravager').
card_rarity('molten ravager'/'ZEN', 'Common').
card_artist('molten ravager'/'ZEN', 'Dave Kendall').
card_number('molten ravager'/'ZEN', '138').
card_flavor_text('molten ravager'/'ZEN', '\"Only the foolhardy would venture into the Akoum Mountains without a lullmage to tame the raging rocks and living fires.\"\n—Sachir, Akoum Expeditionary House').
card_multiverse_id('molten ravager'/'ZEN', '191359').

card_in_set('mountain', 'ZEN').
card_original_type('mountain'/'ZEN', 'Basic Land — Mountain').
card_original_text('mountain'/'ZEN', 'R').
card_image_name('mountain'/'ZEN', 'mountain1').
card_uid('mountain'/'ZEN', 'ZEN:Mountain:mountain1').
card_rarity('mountain'/'ZEN', 'Basic Land').
card_artist('mountain'/'ZEN', 'John Avon').
card_number('mountain'/'ZEN', '242').
card_multiverse_id('mountain'/'ZEN', '201968').

card_in_set('mountain', 'ZEN').
card_original_type('mountain'/'ZEN', 'Basic Land — Mountain').
card_original_text('mountain'/'ZEN', 'R').
card_image_name('mountain'/'ZEN', 'mountain1a').
card_uid('mountain'/'ZEN', 'ZEN:Mountain:mountain1a').
card_rarity('mountain'/'ZEN', 'Basic Land').
card_artist('mountain'/'ZEN', 'John Avon').
card_number('mountain'/'ZEN', '242a').
card_multiverse_id('mountain'/'ZEN', '195178').

card_in_set('mountain', 'ZEN').
card_original_type('mountain'/'ZEN', 'Basic Land — Mountain').
card_original_text('mountain'/'ZEN', 'R').
card_image_name('mountain'/'ZEN', 'mountain2').
card_uid('mountain'/'ZEN', 'ZEN:Mountain:mountain2').
card_rarity('mountain'/'ZEN', 'Basic Land').
card_artist('mountain'/'ZEN', 'Jung Park').
card_number('mountain'/'ZEN', '243').
card_multiverse_id('mountain'/'ZEN', '201967').

card_in_set('mountain', 'ZEN').
card_original_type('mountain'/'ZEN', 'Basic Land — Mountain').
card_original_text('mountain'/'ZEN', 'R').
card_image_name('mountain'/'ZEN', 'mountain2a').
card_uid('mountain'/'ZEN', 'ZEN:Mountain:mountain2a').
card_rarity('mountain'/'ZEN', 'Basic Land').
card_artist('mountain'/'ZEN', 'Jung Park').
card_number('mountain'/'ZEN', '243a').
card_multiverse_id('mountain'/'ZEN', '195171').

card_in_set('mountain', 'ZEN').
card_original_type('mountain'/'ZEN', 'Basic Land — Mountain').
card_original_text('mountain'/'ZEN', 'R').
card_image_name('mountain'/'ZEN', 'mountain3').
card_uid('mountain'/'ZEN', 'ZEN:Mountain:mountain3').
card_rarity('mountain'/'ZEN', 'Basic Land').
card_artist('mountain'/'ZEN', 'Véronique Meignaud').
card_number('mountain'/'ZEN', '244').
card_multiverse_id('mountain'/'ZEN', '195181').

card_in_set('mountain', 'ZEN').
card_original_type('mountain'/'ZEN', 'Basic Land — Mountain').
card_original_text('mountain'/'ZEN', 'R').
card_image_name('mountain'/'ZEN', 'mountain3a').
card_uid('mountain'/'ZEN', 'ZEN:Mountain:mountain3a').
card_rarity('mountain'/'ZEN', 'Basic Land').
card_artist('mountain'/'ZEN', 'Véronique Meignaud').
card_number('mountain'/'ZEN', '244a').
card_multiverse_id('mountain'/'ZEN', '201969').

card_in_set('mountain', 'ZEN').
card_original_type('mountain'/'ZEN', 'Basic Land — Mountain').
card_original_text('mountain'/'ZEN', 'R').
card_image_name('mountain'/'ZEN', 'mountain4').
card_uid('mountain'/'ZEN', 'ZEN:Mountain:mountain4').
card_rarity('mountain'/'ZEN', 'Basic Land').
card_artist('mountain'/'ZEN', 'Vincent Proce').
card_number('mountain'/'ZEN', '245').
card_multiverse_id('mountain'/'ZEN', '201970').

card_in_set('mountain', 'ZEN').
card_original_type('mountain'/'ZEN', 'Basic Land — Mountain').
card_original_text('mountain'/'ZEN', 'R').
card_image_name('mountain'/'ZEN', 'mountain4a').
card_uid('mountain'/'ZEN', 'ZEN:Mountain:mountain4a').
card_rarity('mountain'/'ZEN', 'Basic Land').
card_artist('mountain'/'ZEN', 'Vincent Proce').
card_number('mountain'/'ZEN', '245a').
card_multiverse_id('mountain'/'ZEN', '195184').

card_in_set('murasa pyromancer', 'ZEN').
card_original_type('murasa pyromancer'/'ZEN', 'Creature — Human Shaman Ally').
card_original_text('murasa pyromancer'/'ZEN', 'Whenever Murasa Pyromancer or another Ally enters the battlefield under your control, you may have Murasa Pyromancer deal damage to target creature equal to the number of Allies you control.').
card_first_print('murasa pyromancer', 'ZEN').
card_image_name('murasa pyromancer'/'ZEN', 'murasa pyromancer').
card_uid('murasa pyromancer'/'ZEN', 'ZEN:Murasa Pyromancer:murasa pyromancer').
card_rarity('murasa pyromancer'/'ZEN', 'Uncommon').
card_artist('murasa pyromancer'/'ZEN', 'Kekai Kotaki').
card_number('murasa pyromancer'/'ZEN', '139').
card_flavor_text('murasa pyromancer'/'ZEN', '\"Fire is my accomplice. For enough coin, it can be yours as well.\"').
card_multiverse_id('murasa pyromancer'/'ZEN', '195634').

card_in_set('narrow escape', 'ZEN').
card_original_type('narrow escape'/'ZEN', 'Instant').
card_original_text('narrow escape'/'ZEN', 'Return target permanent you control to its owner\'s hand. You gain 4 life.').
card_first_print('narrow escape', 'ZEN').
card_image_name('narrow escape'/'ZEN', 'narrow escape').
card_uid('narrow escape'/'ZEN', 'ZEN:Narrow Escape:narrow escape').
card_rarity('narrow escape'/'ZEN', 'Common').
card_artist('narrow escape'/'ZEN', 'Karl Kopinski').
card_number('narrow escape'/'ZEN', '27').
card_flavor_text('narrow escape'/'ZEN', '\"A good explorer has to be as slippery as a gomazoa, as tough as a scute bug, and luckier than a ten-fingered trapfinder.\"\n—Arhana, Kazandu trapfinder').
card_multiverse_id('narrow escape'/'ZEN', '180360').

card_in_set('needlebite trap', 'ZEN').
card_original_type('needlebite trap'/'ZEN', 'Instant — Trap').
card_original_text('needlebite trap'/'ZEN', 'If an opponent gained life this turn, you may pay {B} rather than pay Needlebite Trap\'s mana cost.\nTarget player loses 5 life and you gain 5 life.').
card_first_print('needlebite trap', 'ZEN').
card_image_name('needlebite trap'/'ZEN', 'needlebite trap').
card_uid('needlebite trap'/'ZEN', 'ZEN:Needlebite Trap:needlebite trap').
card_rarity('needlebite trap'/'ZEN', 'Uncommon').
card_artist('needlebite trap'/'ZEN', 'Daarken').
card_number('needlebite trap'/'ZEN', '105').
card_flavor_text('needlebite trap'/'ZEN', 'The traps around the vampire city of Malakir hint at the thirst of its inhabitants.').
card_multiverse_id('needlebite trap'/'ZEN', '197529').

card_in_set('nimana sell-sword', 'ZEN').
card_original_type('nimana sell-sword'/'ZEN', 'Creature — Human Warrior Ally').
card_original_text('nimana sell-sword'/'ZEN', 'Whenever Nimana Sell-Sword or another Ally enters the battlefield under your control, you may put a +1/+1 counter on Nimana Sell-Sword.').
card_first_print('nimana sell-sword', 'ZEN').
card_image_name('nimana sell-sword'/'ZEN', 'nimana sell-sword').
card_uid('nimana sell-sword'/'ZEN', 'ZEN:Nimana Sell-Sword:nimana sell-sword').
card_rarity('nimana sell-sword'/'ZEN', 'Common').
card_artist('nimana sell-sword'/'ZEN', 'Daarken').
card_number('nimana sell-sword'/'ZEN', '106').
card_flavor_text('nimana sell-sword'/'ZEN', '\"He asked if I had work for him. No wasn\'t the right answer.\"\n—Samila, Murasa Expeditionary House').
card_multiverse_id('nimana sell-sword'/'ZEN', '197403').

card_in_set('nimbus wings', 'ZEN').
card_original_type('nimbus wings'/'ZEN', 'Enchantment — Aura').
card_original_text('nimbus wings'/'ZEN', 'Enchant creature\nEnchanted creature gets +1/+2 and has flying.').
card_first_print('nimbus wings', 'ZEN').
card_image_name('nimbus wings'/'ZEN', 'nimbus wings').
card_uid('nimbus wings'/'ZEN', 'ZEN:Nimbus Wings:nimbus wings').
card_rarity('nimbus wings'/'ZEN', 'Common').
card_artist('nimbus wings'/'ZEN', 'Chris Rahn').
card_number('nimbus wings'/'ZEN', '28').
card_flavor_text('nimbus wings'/'ZEN', 'Explorers may find ways into the Sky Ruin, but they find its secrets well protected by shifting hedrons and Roil winds.').
card_multiverse_id('nimbus wings'/'ZEN', '180346').

card_in_set('nissa revane', 'ZEN').
card_original_type('nissa revane'/'ZEN', 'Planeswalker — Nissa').
card_original_text('nissa revane'/'ZEN', '+1: Search your library for a card named Nissa\'s Chosen and put it onto the battlefield. Then shuffle your library.\n+1: You gain 2 life for each Elf you control.\n-7: Search your library for any number of Elf creature cards and put them onto the battlefield. Then shuffle your library.').
card_image_name('nissa revane'/'ZEN', 'nissa revane').
card_uid('nissa revane'/'ZEN', 'ZEN:Nissa Revane:nissa revane').
card_rarity('nissa revane'/'ZEN', 'Mythic Rare').
card_artist('nissa revane'/'ZEN', 'Jaime Jones').
card_number('nissa revane'/'ZEN', '170').
card_multiverse_id('nissa revane'/'ZEN', '190411').

card_in_set('nissa\'s chosen', 'ZEN').
card_original_type('nissa\'s chosen'/'ZEN', 'Creature — Elf Warrior').
card_original_text('nissa\'s chosen'/'ZEN', 'If Nissa\'s Chosen would be put into a graveyard from the battlefield, put it on the bottom of its owner\'s library instead.').
card_image_name('nissa\'s chosen'/'ZEN', 'nissa\'s chosen').
card_uid('nissa\'s chosen'/'ZEN', 'ZEN:Nissa\'s Chosen:nissa\'s chosen').
card_rarity('nissa\'s chosen'/'ZEN', 'Common').
card_artist('nissa\'s chosen'/'ZEN', 'Jaime Jones').
card_number('nissa\'s chosen'/'ZEN', '171').
card_flavor_text('nissa\'s chosen'/'ZEN', '\"Nissa will save the elves, but only the ones who have proved their worth.\"').
card_multiverse_id('nissa\'s chosen'/'ZEN', '185699').

card_in_set('noble vestige', 'ZEN').
card_original_type('noble vestige'/'ZEN', 'Creature — Spirit').
card_original_text('noble vestige'/'ZEN', 'Flying\n{T}: Prevent the next 1 damage that would be dealt to target player this turn.').
card_first_print('noble vestige', 'ZEN').
card_image_name('noble vestige'/'ZEN', 'noble vestige').
card_uid('noble vestige'/'ZEN', 'ZEN:Noble Vestige:noble vestige').
card_rarity('noble vestige'/'ZEN', 'Common').
card_artist('noble vestige'/'ZEN', 'Jason Chan').
card_number('noble vestige'/'ZEN', '29').
card_flavor_text('noble vestige'/'ZEN', 'Most spirits are chained to this world by their despair, but he remains tethered by his hope for better days.').
card_multiverse_id('noble vestige'/'ZEN', '171001').

card_in_set('ob nixilis, the fallen', 'ZEN').
card_original_type('ob nixilis, the fallen'/'ZEN', 'Legendary Creature — Demon').
card_original_text('ob nixilis, the fallen'/'ZEN', 'Landfall — Whenever a land enters the battlefield under your control, you may have target player lose 3 life. If you do, put three +1/+1 counters on Ob Nixilis, the Fallen.').
card_first_print('ob nixilis, the fallen', 'ZEN').
card_image_name('ob nixilis, the fallen'/'ZEN', 'ob nixilis, the fallen').
card_uid('ob nixilis, the fallen'/'ZEN', 'ZEN:Ob Nixilis, the Fallen:ob nixilis, the fallen').
card_rarity('ob nixilis, the fallen'/'ZEN', 'Mythic Rare').
card_artist('ob nixilis, the fallen'/'ZEN', 'Jason Felix').
card_number('ob nixilis, the fallen'/'ZEN', '107').
card_flavor_text('ob nixilis, the fallen'/'ZEN', 'His spark lost, he plots revenge upon the plane whose corrupting mana fractured his soul.').
card_multiverse_id('ob nixilis, the fallen'/'ZEN', '192219').

card_in_set('obsidian fireheart', 'ZEN').
card_original_type('obsidian fireheart'/'ZEN', 'Creature — Elemental').
card_original_text('obsidian fireheart'/'ZEN', '{1}{R}{R}: Put a blaze counter on target land without a blaze counter on it. As long as that land has a blaze counter on it, it has \"At the beginning of your upkeep, this land deals 1 damage to you.\" (The land continues to burn after Obsidian Fireheart has left the battlefield.)').
card_first_print('obsidian fireheart', 'ZEN').
card_image_name('obsidian fireheart'/'ZEN', 'obsidian fireheart').
card_uid('obsidian fireheart'/'ZEN', 'ZEN:Obsidian Fireheart:obsidian fireheart').
card_rarity('obsidian fireheart'/'ZEN', 'Mythic Rare').
card_artist('obsidian fireheart'/'ZEN', 'Raymond Swanland').
card_number('obsidian fireheart'/'ZEN', '140').
card_multiverse_id('obsidian fireheart'/'ZEN', '192224').

card_in_set('ondu cleric', 'ZEN').
card_original_type('ondu cleric'/'ZEN', 'Creature — Kor Cleric Ally').
card_original_text('ondu cleric'/'ZEN', 'Whenever Ondu Cleric or another Ally enters the battlefield under your control, you may gain life equal to the number of Allies you control.').
card_first_print('ondu cleric', 'ZEN').
card_image_name('ondu cleric'/'ZEN', 'ondu cleric').
card_uid('ondu cleric'/'ZEN', 'ZEN:Ondu Cleric:ondu cleric').
card_rarity('ondu cleric'/'ZEN', 'Common').
card_artist('ondu cleric'/'ZEN', 'Jim Murray').
card_number('ondu cleric'/'ZEN', '30').
card_flavor_text('ondu cleric'/'ZEN', 'A cleric\'s true balm is the confidence he inspires in his compatriots.').
card_multiverse_id('ondu cleric'/'ZEN', '192210').

card_in_set('oracle of mul daya', 'ZEN').
card_original_type('oracle of mul daya'/'ZEN', 'Creature — Elf Shaman').
card_original_text('oracle of mul daya'/'ZEN', 'You may play an additional land on each of your turns.\nPlay with the top card of your library revealed.\nYou may play the top card of your library if it\'s a land card.').
card_first_print('oracle of mul daya', 'ZEN').
card_image_name('oracle of mul daya'/'ZEN', 'oracle of mul daya').
card_uid('oracle of mul daya'/'ZEN', 'ZEN:Oracle of Mul Daya:oracle of mul daya').
card_rarity('oracle of mul daya'/'ZEN', 'Rare').
card_artist('oracle of mul daya'/'ZEN', 'Vance Kovacs').
card_number('oracle of mul daya'/'ZEN', '172').
card_multiverse_id('oracle of mul daya'/'ZEN', '185737').

card_in_set('oran-rief recluse', 'ZEN').
card_original_type('oran-rief recluse'/'ZEN', 'Creature — Spider').
card_original_text('oran-rief recluse'/'ZEN', 'Kicker {2}{G} (You may pay an additional {2}{G} as you cast this spell.)\nReach (This creature can block creatures with flying.)\nWhen Oran-Rief Recluse enters the battlefield, if it was kicked, destroy target creature with flying.').
card_first_print('oran-rief recluse', 'ZEN').
card_image_name('oran-rief recluse'/'ZEN', 'oran-rief recluse').
card_uid('oran-rief recluse'/'ZEN', 'ZEN:Oran-Rief Recluse:oran-rief recluse').
card_rarity('oran-rief recluse'/'ZEN', 'Common').
card_artist('oran-rief recluse'/'ZEN', 'Lars Grant-West').
card_number('oran-rief recluse'/'ZEN', '173').
card_multiverse_id('oran-rief recluse'/'ZEN', '185696').

card_in_set('oran-rief survivalist', 'ZEN').
card_original_type('oran-rief survivalist'/'ZEN', 'Creature — Human Warrior Ally').
card_original_text('oran-rief survivalist'/'ZEN', 'Whenever Oran-Rief Survivalist or another Ally enters the battlefield under your control, you may put a +1/+1 counter on Oran-Rief Survivalist.').
card_first_print('oran-rief survivalist', 'ZEN').
card_image_name('oran-rief survivalist'/'ZEN', 'oran-rief survivalist').
card_uid('oran-rief survivalist'/'ZEN', 'ZEN:Oran-Rief Survivalist:oran-rief survivalist').
card_rarity('oran-rief survivalist'/'ZEN', 'Common').
card_artist('oran-rief survivalist'/'ZEN', 'Kev Walker').
card_number('oran-rief survivalist'/'ZEN', '174').
card_flavor_text('oran-rief survivalist'/'ZEN', '\"I\'m strong enough to survive alone. I\'m smart enough to want companions at my side.\"').
card_multiverse_id('oran-rief survivalist'/'ZEN', '178138').

card_in_set('oran-rief, the vastwood', 'ZEN').
card_original_type('oran-rief, the vastwood'/'ZEN', 'Land').
card_original_text('oran-rief, the vastwood'/'ZEN', 'Oran-Rief, the Vastwood enters the battlefield tapped.\n{T}: Add {G} to your mana pool.\n{T}: Put a +1/+1 counter on each green creature that entered the battlefield this turn.').
card_first_print('oran-rief, the vastwood', 'ZEN').
card_image_name('oran-rief, the vastwood'/'ZEN', 'oran-rief, the vastwood').
card_uid('oran-rief, the vastwood'/'ZEN', 'ZEN:Oran-Rief, the Vastwood:oran-rief, the vastwood').
card_rarity('oran-rief, the vastwood'/'ZEN', 'Rare').
card_artist('oran-rief, the vastwood'/'ZEN', 'Mike Bierek').
card_number('oran-rief, the vastwood'/'ZEN', '221').
card_multiverse_id('oran-rief, the vastwood'/'ZEN', '190417').

card_in_set('paralyzing grasp', 'ZEN').
card_original_type('paralyzing grasp'/'ZEN', 'Enchantment — Aura').
card_original_text('paralyzing grasp'/'ZEN', 'Enchant creature\nEnchanted creature doesn\'t untap during its controller\'s untap step.').
card_first_print('paralyzing grasp', 'ZEN').
card_image_name('paralyzing grasp'/'ZEN', 'paralyzing grasp').
card_uid('paralyzing grasp'/'ZEN', 'ZEN:Paralyzing Grasp:paralyzing grasp').
card_rarity('paralyzing grasp'/'ZEN', 'Common').
card_artist('paralyzing grasp'/'ZEN', 'Izzy').
card_number('paralyzing grasp'/'ZEN', '58').
card_flavor_text('paralyzing grasp'/'ZEN', 'The Halimar Sea Caves are both the breeding ground of giant squids and the dumping ground of hardened criminals.').
card_multiverse_id('paralyzing grasp'/'ZEN', '178109').

card_in_set('pillarfield ox', 'ZEN').
card_original_type('pillarfield ox'/'ZEN', 'Creature — Ox').
card_original_text('pillarfield ox'/'ZEN', '').
card_first_print('pillarfield ox', 'ZEN').
card_image_name('pillarfield ox'/'ZEN', 'pillarfield ox').
card_uid('pillarfield ox'/'ZEN', 'ZEN:Pillarfield Ox:pillarfield ox').
card_rarity('pillarfield ox'/'ZEN', 'Common').
card_artist('pillarfield ox'/'ZEN', 'Andrew Robinson').
card_number('pillarfield ox'/'ZEN', '31').
card_flavor_text('pillarfield ox'/'ZEN', 'These stubborn and unpredictable oxen inspire the plains nomads\' most colorful curses.').
card_multiverse_id('pillarfield ox'/'ZEN', '190395').

card_in_set('piranha marsh', 'ZEN').
card_original_type('piranha marsh'/'ZEN', 'Land').
card_original_text('piranha marsh'/'ZEN', 'Piranha Marsh enters the battlefield tapped.\nWhen Piranha Marsh enters the battlefield, target player loses 1 life.\n{T}: Add {B} to your mana pool.').
card_first_print('piranha marsh', 'ZEN').
card_image_name('piranha marsh'/'ZEN', 'piranha marsh').
card_uid('piranha marsh'/'ZEN', 'ZEN:Piranha Marsh:piranha marsh').
card_rarity('piranha marsh'/'ZEN', 'Common').
card_artist('piranha marsh'/'ZEN', 'Nic Klein').
card_number('piranha marsh'/'ZEN', '222').
card_multiverse_id('piranha marsh'/'ZEN', '169975').

card_in_set('pitfall trap', 'ZEN').
card_original_type('pitfall trap'/'ZEN', 'Instant — Trap').
card_original_text('pitfall trap'/'ZEN', 'If exactly one creature is attacking, you may pay {W} rather than pay Pitfall Trap\'s mana cost.\nDestroy target attacking creature without flying.').
card_first_print('pitfall trap', 'ZEN').
card_image_name('pitfall trap'/'ZEN', 'pitfall trap').
card_uid('pitfall trap'/'ZEN', 'ZEN:Pitfall Trap:pitfall trap').
card_rarity('pitfall trap'/'ZEN', 'Uncommon').
card_artist('pitfall trap'/'ZEN', 'Franz Vohwinkel').
card_number('pitfall trap'/'ZEN', '32').
card_flavor_text('pitfall trap'/'ZEN', 'Each spike is poisoned—the trapmaker\'s idea of mercy.').
card_multiverse_id('pitfall trap'/'ZEN', '197533').

card_in_set('plains', 'ZEN').
card_original_type('plains'/'ZEN', 'Basic Land — Plains').
card_original_text('plains'/'ZEN', 'W').
card_image_name('plains'/'ZEN', 'plains1').
card_uid('plains'/'ZEN', 'ZEN:Plains:plains1').
card_rarity('plains'/'ZEN', 'Basic Land').
card_artist('plains'/'ZEN', 'John Avon').
card_number('plains'/'ZEN', '230').
card_multiverse_id('plains'/'ZEN', '201973').

card_in_set('plains', 'ZEN').
card_original_type('plains'/'ZEN', 'Basic Land — Plains').
card_original_text('plains'/'ZEN', 'W').
card_image_name('plains'/'ZEN', 'plains1a').
card_uid('plains'/'ZEN', 'ZEN:Plains:plains1a').
card_rarity('plains'/'ZEN', 'Basic Land').
card_artist('plains'/'ZEN', 'John Avon').
card_number('plains'/'ZEN', '230a').
card_multiverse_id('plains'/'ZEN', '195179').

card_in_set('plains', 'ZEN').
card_original_type('plains'/'ZEN', 'Basic Land — Plains').
card_original_text('plains'/'ZEN', 'W').
card_image_name('plains'/'ZEN', 'plains2').
card_uid('plains'/'ZEN', 'ZEN:Plains:plains2').
card_rarity('plains'/'ZEN', 'Basic Land').
card_artist('plains'/'ZEN', 'Jung Park').
card_number('plains'/'ZEN', '231').
card_multiverse_id('plains'/'ZEN', '195173').

card_in_set('plains', 'ZEN').
card_original_type('plains'/'ZEN', 'Basic Land — Plains').
card_original_text('plains'/'ZEN', 'W').
card_image_name('plains'/'ZEN', 'plains2a').
card_uid('plains'/'ZEN', 'ZEN:Plains:plains2a').
card_rarity('plains'/'ZEN', 'Basic Land').
card_artist('plains'/'ZEN', 'Jung Park').
card_number('plains'/'ZEN', '231a').
card_multiverse_id('plains'/'ZEN', '201972').

card_in_set('plains', 'ZEN').
card_original_type('plains'/'ZEN', 'Basic Land — Plains').
card_original_text('plains'/'ZEN', 'W').
card_image_name('plains'/'ZEN', 'plains3').
card_uid('plains'/'ZEN', 'ZEN:Plains:plains3').
card_rarity('plains'/'ZEN', 'Basic Land').
card_artist('plains'/'ZEN', 'Véronique Meignaud').
card_number('plains'/'ZEN', '232').
card_multiverse_id('plains'/'ZEN', '195196').

card_in_set('plains', 'ZEN').
card_original_type('plains'/'ZEN', 'Basic Land — Plains').
card_original_text('plains'/'ZEN', 'W').
card_image_name('plains'/'ZEN', 'plains3a').
card_uid('plains'/'ZEN', 'ZEN:Plains:plains3a').
card_rarity('plains'/'ZEN', 'Basic Land').
card_artist('plains'/'ZEN', 'Véronique Meignaud').
card_number('plains'/'ZEN', '232a').
card_multiverse_id('plains'/'ZEN', '201974').

card_in_set('plains', 'ZEN').
card_original_type('plains'/'ZEN', 'Basic Land — Plains').
card_original_text('plains'/'ZEN', 'W').
card_image_name('plains'/'ZEN', 'plains4').
card_uid('plains'/'ZEN', 'ZEN:Plains:plains4').
card_rarity('plains'/'ZEN', 'Basic Land').
card_artist('plains'/'ZEN', 'Vincent Proce').
card_number('plains'/'ZEN', '233').
card_multiverse_id('plains'/'ZEN', '195163').

card_in_set('plains', 'ZEN').
card_original_type('plains'/'ZEN', 'Basic Land — Plains').
card_original_text('plains'/'ZEN', 'W').
card_image_name('plains'/'ZEN', 'plains4a').
card_uid('plains'/'ZEN', 'ZEN:Plains:plains4a').
card_rarity('plains'/'ZEN', 'Basic Land').
card_artist('plains'/'ZEN', 'Vincent Proce').
card_number('plains'/'ZEN', '233a').
card_multiverse_id('plains'/'ZEN', '201971').

card_in_set('plated geopede', 'ZEN').
card_original_type('plated geopede'/'ZEN', 'Creature — Insect').
card_original_text('plated geopede'/'ZEN', 'First strike\nLandfall — Whenever a land enters the battlefield under your control, Plated Geopede gets +2/+2 until end of turn.').
card_first_print('plated geopede', 'ZEN').
card_image_name('plated geopede'/'ZEN', 'plated geopede').
card_uid('plated geopede'/'ZEN', 'ZEN:Plated Geopede:plated geopede').
card_rarity('plated geopede'/'ZEN', 'Common').
card_artist('plated geopede'/'ZEN', 'Eric Deschamps').
card_number('plated geopede'/'ZEN', '141').
card_flavor_text('plated geopede'/'ZEN', '\"Kor armorers buy the scales and claws. Elf oracles buy the rest.\"\n—Nablus, North Hada trapper').
card_multiverse_id('plated geopede'/'ZEN', '170977').

card_in_set('predatory urge', 'ZEN').
card_original_type('predatory urge'/'ZEN', 'Enchantment — Aura').
card_original_text('predatory urge'/'ZEN', 'Enchant creature\nEnchanted creature has \"{T}: This creature deals damage equal to its power to target creature. That creature deals damage equal to its power to this creature.\"').
card_first_print('predatory urge', 'ZEN').
card_image_name('predatory urge'/'ZEN', 'predatory urge').
card_uid('predatory urge'/'ZEN', 'ZEN:Predatory Urge:predatory urge').
card_rarity('predatory urge'/'ZEN', 'Rare').
card_artist('predatory urge'/'ZEN', 'Scott Chou').
card_number('predatory urge'/'ZEN', '175').
card_multiverse_id('predatory urge'/'ZEN', '169958').

card_in_set('primal bellow', 'ZEN').
card_original_type('primal bellow'/'ZEN', 'Instant').
card_original_text('primal bellow'/'ZEN', 'Target creature gets +1/+1 until end of turn for each Forest you control.').
card_first_print('primal bellow', 'ZEN').
card_image_name('primal bellow'/'ZEN', 'primal bellow').
card_uid('primal bellow'/'ZEN', 'ZEN:Primal Bellow:primal bellow').
card_rarity('primal bellow'/'ZEN', 'Uncommon').
card_artist('primal bellow'/'ZEN', 'Daren Bader').
card_number('primal bellow'/'ZEN', '176').
card_flavor_text('primal bellow'/'ZEN', '\"A might spell can be handy against baloths, giants, kraken, the Roil—or if you lose your bearings in the woods.\"\n—Chadir the Navigator').
card_multiverse_id('primal bellow'/'ZEN', '193407').

card_in_set('punishing fire', 'ZEN').
card_original_type('punishing fire'/'ZEN', 'Instant').
card_original_text('punishing fire'/'ZEN', 'Punishing Fire deals 2 damage to target creature or player.\nWhenever an opponent gains life, you may pay {R}. If you do, return Punishing Fire from your graveyard to your hand.').
card_first_print('punishing fire', 'ZEN').
card_image_name('punishing fire'/'ZEN', 'punishing fire').
card_uid('punishing fire'/'ZEN', 'ZEN:Punishing Fire:punishing fire').
card_rarity('punishing fire'/'ZEN', 'Uncommon').
card_artist('punishing fire'/'ZEN', 'Christopher Moeller').
card_number('punishing fire'/'ZEN', '142').
card_multiverse_id('punishing fire'/'ZEN', '192217').

card_in_set('pyromancer ascension', 'ZEN').
card_original_type('pyromancer ascension'/'ZEN', 'Enchantment').
card_original_text('pyromancer ascension'/'ZEN', 'Whenever you cast an instant or sorcery spell that has the same name as a card in your graveyard, you may put a quest counter on Pyromancer Ascension.\nWhenever you cast an instant or sorcery spell while Pyromancer Ascension has two or more quest counters on it, you may copy that spell. You may choose new targets for the copy.').
card_first_print('pyromancer ascension', 'ZEN').
card_image_name('pyromancer ascension'/'ZEN', 'pyromancer ascension').
card_uid('pyromancer ascension'/'ZEN', 'ZEN:Pyromancer Ascension:pyromancer ascension').
card_rarity('pyromancer ascension'/'ZEN', 'Rare').
card_artist('pyromancer ascension'/'ZEN', 'Kev Walker').
card_number('pyromancer ascension'/'ZEN', '143').
card_multiverse_id('pyromancer ascension'/'ZEN', '197891').

card_in_set('quest for ancient secrets', 'ZEN').
card_original_type('quest for ancient secrets'/'ZEN', 'Enchantment').
card_original_text('quest for ancient secrets'/'ZEN', 'Whenever a card is put into your graveyard from anywhere, you may put a quest counter on Quest for Ancient Secrets.\nRemove five quest counters from Quest for Ancient Secrets and sacrifice it: Target player shuffles his or her graveyard into his or her library.').
card_first_print('quest for ancient secrets', 'ZEN').
card_image_name('quest for ancient secrets'/'ZEN', 'quest for ancient secrets').
card_uid('quest for ancient secrets'/'ZEN', 'ZEN:Quest for Ancient Secrets:quest for ancient secrets').
card_rarity('quest for ancient secrets'/'ZEN', 'Uncommon').
card_artist('quest for ancient secrets'/'ZEN', 'Mike Bierek').
card_number('quest for ancient secrets'/'ZEN', '59').
card_multiverse_id('quest for ancient secrets'/'ZEN', '195635').

card_in_set('quest for pure flame', 'ZEN').
card_original_type('quest for pure flame'/'ZEN', 'Enchantment').
card_original_text('quest for pure flame'/'ZEN', 'Whenever a source you control deals damage to an opponent, you may put a quest counter on Quest for Pure Flame.\nRemove four quest counters from Quest for Pure Flame and sacrifice it: If any source you control would deal damage to a creature or player this turn, it deals double that damage to that creature or player instead.').
card_first_print('quest for pure flame', 'ZEN').
card_image_name('quest for pure flame'/'ZEN', 'quest for pure flame').
card_uid('quest for pure flame'/'ZEN', 'ZEN:Quest for Pure Flame:quest for pure flame').
card_rarity('quest for pure flame'/'ZEN', 'Uncommon').
card_artist('quest for pure flame'/'ZEN', 'Cyril Van Der Haegen').
card_number('quest for pure flame'/'ZEN', '144').
card_multiverse_id('quest for pure flame'/'ZEN', '197407').

card_in_set('quest for the gemblades', 'ZEN').
card_original_type('quest for the gemblades'/'ZEN', 'Enchantment').
card_original_text('quest for the gemblades'/'ZEN', 'Whenever a creature you control deals combat damage to a creature, you may put a quest counter on Quest for the Gemblades.\nRemove a quest counter from Quest for the Gemblades and sacrifice it: Put four +1/+1 counters on target creature.').
card_first_print('quest for the gemblades', 'ZEN').
card_image_name('quest for the gemblades'/'ZEN', 'quest for the gemblades').
card_uid('quest for the gemblades'/'ZEN', 'ZEN:Quest for the Gemblades:quest for the gemblades').
card_rarity('quest for the gemblades'/'ZEN', 'Uncommon').
card_artist('quest for the gemblades'/'ZEN', 'Karl Kopinski').
card_number('quest for the gemblades'/'ZEN', '177').
card_multiverse_id('quest for the gemblades'/'ZEN', '195572').

card_in_set('quest for the gravelord', 'ZEN').
card_original_type('quest for the gravelord'/'ZEN', 'Enchantment').
card_original_text('quest for the gravelord'/'ZEN', 'Whenever a creature is put into a graveyard from the battlefield, you may put a quest counter on Quest for the Gravelord.\nRemove three quest counters from Quest for the Gravelord and sacrifice it: Put a 5/5 black Zombie Giant creature token onto the battlefield.').
card_first_print('quest for the gravelord', 'ZEN').
card_image_name('quest for the gravelord'/'ZEN', 'quest for the gravelord').
card_uid('quest for the gravelord'/'ZEN', 'ZEN:Quest for the Gravelord:quest for the gravelord').
card_rarity('quest for the gravelord'/'ZEN', 'Uncommon').
card_artist('quest for the gravelord'/'ZEN', 'Chris Rahn').
card_number('quest for the gravelord'/'ZEN', '108').
card_multiverse_id('quest for the gravelord'/'ZEN', '180448').

card_in_set('quest for the holy relic', 'ZEN').
card_original_type('quest for the holy relic'/'ZEN', 'Enchantment').
card_original_text('quest for the holy relic'/'ZEN', 'Whenever you cast a creature spell, you may put a quest counter on Quest for the Holy Relic.\nRemove five quest counters from Quest for the Holy Relic and sacrifice it: Search your library for an Equipment card, put it onto the battlefield, and attach it to a creature you control. Then shuffle your library.').
card_first_print('quest for the holy relic', 'ZEN').
card_image_name('quest for the holy relic'/'ZEN', 'quest for the holy relic').
card_uid('quest for the holy relic'/'ZEN', 'ZEN:Quest for the Holy Relic:quest for the holy relic').
card_rarity('quest for the holy relic'/'ZEN', 'Uncommon').
card_artist('quest for the holy relic'/'ZEN', 'Greg Staples').
card_number('quest for the holy relic'/'ZEN', '33').
card_multiverse_id('quest for the holy relic'/'ZEN', '197406').

card_in_set('rampaging baloths', 'ZEN').
card_original_type('rampaging baloths'/'ZEN', 'Creature — Beast').
card_original_text('rampaging baloths'/'ZEN', 'Trample\nLandfall — Whenever a land enters the battlefield under your control, you may put a 4/4 green Beast creature token onto the battlefield.').
card_image_name('rampaging baloths'/'ZEN', 'rampaging baloths').
card_uid('rampaging baloths'/'ZEN', 'ZEN:Rampaging Baloths:rampaging baloths').
card_rarity('rampaging baloths'/'ZEN', 'Mythic Rare').
card_artist('rampaging baloths'/'ZEN', 'Steve Prescott').
card_number('rampaging baloths'/'ZEN', '178').
card_flavor_text('rampaging baloths'/'ZEN', '\"When the land is angry, so are they.\"\n—Nissa Revane').
card_multiverse_id('rampaging baloths'/'ZEN', '192222').

card_in_set('ravenous trap', 'ZEN').
card_original_type('ravenous trap'/'ZEN', 'Instant — Trap').
card_original_text('ravenous trap'/'ZEN', 'If an opponent had three or more cards put into his or her graveyard from anywhere this turn, you may pay {0} rather than pay Ravenous Trap\'s mana cost.\nExile all cards from target player\'s graveyard.').
card_first_print('ravenous trap', 'ZEN').
card_image_name('ravenous trap'/'ZEN', 'ravenous trap').
card_uid('ravenous trap'/'ZEN', 'ZEN:Ravenous Trap:ravenous trap').
card_rarity('ravenous trap'/'ZEN', 'Uncommon').
card_artist('ravenous trap'/'ZEN', 'Cyril Van Der Haegen').
card_number('ravenous trap'/'ZEN', '109').
card_multiverse_id('ravenous trap'/'ZEN', '197537').

card_in_set('reckless scholar', 'ZEN').
card_original_type('reckless scholar'/'ZEN', 'Creature — Human Wizard').
card_original_text('reckless scholar'/'ZEN', '{T}: Target player draws a card, then discards a card.').
card_first_print('reckless scholar', 'ZEN').
card_image_name('reckless scholar'/'ZEN', 'reckless scholar').
card_uid('reckless scholar'/'ZEN', 'ZEN:Reckless Scholar:reckless scholar').
card_rarity('reckless scholar'/'ZEN', 'Common').
card_artist('reckless scholar'/'ZEN', 'Steve Prescott').
card_number('reckless scholar'/'ZEN', '60').
card_flavor_text('reckless scholar'/'ZEN', '\"Any good prospector must sift the gold from the sand.\"').
card_multiverse_id('reckless scholar'/'ZEN', '180493').

card_in_set('relic crush', 'ZEN').
card_original_type('relic crush'/'ZEN', 'Instant').
card_original_text('relic crush'/'ZEN', 'Destroy target artifact or enchantment and up to one other target artifact or enchantment.').
card_first_print('relic crush', 'ZEN').
card_image_name('relic crush'/'ZEN', 'relic crush').
card_uid('relic crush'/'ZEN', 'ZEN:Relic Crush:relic crush').
card_rarity('relic crush'/'ZEN', 'Common').
card_artist('relic crush'/'ZEN', 'Steven Belledin').
card_number('relic crush'/'ZEN', '179').
card_flavor_text('relic crush'/'ZEN', 'There are many ruins, but there used to be many more.').
card_multiverse_id('relic crush'/'ZEN', '178115').

card_in_set('rite of replication', 'ZEN').
card_original_type('rite of replication'/'ZEN', 'Sorcery').
card_original_text('rite of replication'/'ZEN', 'Kicker {5} (You may pay an additional {5} as you cast this spell.)\nPut a token onto the battlefield that\'s a copy of target creature. If Rite of Replication was kicked, put five of those tokens onto the battlefield instead.').
card_first_print('rite of replication', 'ZEN').
card_image_name('rite of replication'/'ZEN', 'rite of replication').
card_uid('rite of replication'/'ZEN', 'ZEN:Rite of Replication:rite of replication').
card_rarity('rite of replication'/'ZEN', 'Rare').
card_artist('rite of replication'/'ZEN', 'Matt Cavotta').
card_number('rite of replication'/'ZEN', '61').
card_multiverse_id('rite of replication'/'ZEN', '195630').

card_in_set('river boa', 'ZEN').
card_original_type('river boa'/'ZEN', 'Creature — Snake').
card_original_text('river boa'/'ZEN', 'Islandwalk\n{G}: Regenerate River Boa.').
card_image_name('river boa'/'ZEN', 'river boa').
card_uid('river boa'/'ZEN', 'ZEN:River Boa:river boa').
card_rarity('river boa'/'ZEN', 'Uncommon').
card_artist('river boa'/'ZEN', 'Paul Bonner').
card_number('river boa'/'ZEN', '180').
card_flavor_text('river boa'/'ZEN', '\"Sheath your swords! Cudgels only. I see a new pair of waterproof trek boots slithering away.\"\n—Nablus, North Hada trapper').
card_multiverse_id('river boa'/'ZEN', '197405').

card_in_set('roil elemental', 'ZEN').
card_original_type('roil elemental'/'ZEN', 'Creature — Elemental').
card_original_text('roil elemental'/'ZEN', 'Flying\nLandfall — Whenever a land enters the battlefield under your control, you may gain control of target creature for as long as you control Roil Elemental.').
card_first_print('roil elemental', 'ZEN').
card_image_name('roil elemental'/'ZEN', 'roil elemental').
card_uid('roil elemental'/'ZEN', 'ZEN:Roil Elemental:roil elemental').
card_rarity('roil elemental'/'ZEN', 'Rare').
card_artist('roil elemental'/'ZEN', 'Raymond Swanland').
card_number('roil elemental'/'ZEN', '62').
card_flavor_text('roil elemental'/'ZEN', 'A vortex that devours everything—even the souls of the living.').
card_multiverse_id('roil elemental'/'ZEN', '185712').

card_in_set('ruinous minotaur', 'ZEN').
card_original_type('ruinous minotaur'/'ZEN', 'Creature — Minotaur Warrior').
card_original_text('ruinous minotaur'/'ZEN', 'Whenever Ruinous Minotaur deals damage to an opponent, sacrifice a land.').
card_first_print('ruinous minotaur', 'ZEN').
card_image_name('ruinous minotaur'/'ZEN', 'ruinous minotaur').
card_uid('ruinous minotaur'/'ZEN', 'ZEN:Ruinous Minotaur:ruinous minotaur').
card_rarity('ruinous minotaur'/'ZEN', 'Common').
card_artist('ruinous minotaur'/'ZEN', 'Raymond Swanland').
card_number('ruinous minotaur'/'ZEN', '145').
card_flavor_text('ruinous minotaur'/'ZEN', 'Not every intelligent being is interested in treasure and lost knowledge . . . or even the basics of speech.').
card_multiverse_id('ruinous minotaur'/'ZEN', '189624').

card_in_set('runeflare trap', 'ZEN').
card_original_type('runeflare trap'/'ZEN', 'Instant — Trap').
card_original_text('runeflare trap'/'ZEN', 'If an opponent drew three or more cards this turn, you may pay {R} rather than pay Runeflare Trap\'s mana cost.\nRuneflare Trap deals damage to target player equal to the number of cards in that player\'s hand.').
card_first_print('runeflare trap', 'ZEN').
card_image_name('runeflare trap'/'ZEN', 'runeflare trap').
card_uid('runeflare trap'/'ZEN', 'ZEN:Runeflare Trap:runeflare trap').
card_rarity('runeflare trap'/'ZEN', 'Uncommon').
card_artist('runeflare trap'/'ZEN', 'Franz Vohwinkel').
card_number('runeflare trap'/'ZEN', '146').
card_multiverse_id('runeflare trap'/'ZEN', '197536').

card_in_set('sadistic sacrament', 'ZEN').
card_original_type('sadistic sacrament'/'ZEN', 'Sorcery').
card_original_text('sadistic sacrament'/'ZEN', 'Kicker {7} (You may pay an additional {7} as you cast this spell.)\nSearch target player\'s library for up to three cards, exile them, then that player shuffles his or her library. If Sadistic Sacrament was kicked, instead search that player\'s library for up to fifteen cards, exile them, then that player shuffles his or her library.').
card_first_print('sadistic sacrament', 'ZEN').
card_image_name('sadistic sacrament'/'ZEN', 'sadistic sacrament').
card_uid('sadistic sacrament'/'ZEN', 'ZEN:Sadistic Sacrament:sadistic sacrament').
card_rarity('sadistic sacrament'/'ZEN', 'Rare').
card_artist('sadistic sacrament'/'ZEN', 'Dan Scott').
card_number('sadistic sacrament'/'ZEN', '110').
card_multiverse_id('sadistic sacrament'/'ZEN', '195632').

card_in_set('savage silhouette', 'ZEN').
card_original_type('savage silhouette'/'ZEN', 'Enchantment — Aura').
card_original_text('savage silhouette'/'ZEN', 'Enchant creature\nEnchanted creature gets +2/+2 and has \"{1}{G}: Regenerate this creature.\"').
card_first_print('savage silhouette', 'ZEN').
card_image_name('savage silhouette'/'ZEN', 'savage silhouette').
card_uid('savage silhouette'/'ZEN', 'ZEN:Savage Silhouette:savage silhouette').
card_rarity('savage silhouette'/'ZEN', 'Common').
card_artist('savage silhouette'/'ZEN', 'Dave Kendall').
card_number('savage silhouette'/'ZEN', '181').
card_flavor_text('savage silhouette'/'ZEN', 'Some elves believe that their survival instincts are born of many cycles of reincarnation.').
card_multiverse_id('savage silhouette'/'ZEN', '186316').

card_in_set('scalding tarn', 'ZEN').
card_original_type('scalding tarn'/'ZEN', 'Land').
card_original_text('scalding tarn'/'ZEN', '{T}, Pay 1 life, Sacrifice Scalding Tarn: Search your library for an Island or Mountain card and put it onto the battlefield. Then shuffle your library.').
card_first_print('scalding tarn', 'ZEN').
card_image_name('scalding tarn'/'ZEN', 'scalding tarn').
card_uid('scalding tarn'/'ZEN', 'ZEN:Scalding Tarn:scalding tarn').
card_rarity('scalding tarn'/'ZEN', 'Rare').
card_artist('scalding tarn'/'ZEN', 'Philip Straub').
card_number('scalding tarn'/'ZEN', '223').
card_multiverse_id('scalding tarn'/'ZEN', '190393').

card_in_set('scute mob', 'ZEN').
card_original_type('scute mob'/'ZEN', 'Creature — Insect').
card_original_text('scute mob'/'ZEN', 'At the beginning of your upkeep, if you control five or more lands, put four +1/+1 counters on Scute Mob.').
card_first_print('scute mob', 'ZEN').
card_image_name('scute mob'/'ZEN', 'scute mob').
card_uid('scute mob'/'ZEN', 'ZEN:Scute Mob:scute mob').
card_rarity('scute mob'/'ZEN', 'Rare').
card_artist('scute mob'/'ZEN', 'Zoltan Boros & Gabor Szikszai').
card_number('scute mob'/'ZEN', '182').
card_flavor_text('scute mob'/'ZEN', '\"Survival rule 781: There are always more scute bugs.\"\n—Zurdi, goblin shortcutter').
card_multiverse_id('scute mob'/'ZEN', '180455').

card_in_set('scythe tiger', 'ZEN').
card_original_type('scythe tiger'/'ZEN', 'Creature — Cat').
card_original_text('scythe tiger'/'ZEN', 'Shroud (This creature can\'t be the target of spells or abilities.)\nWhen Scythe Tiger enters the battlefield, sacrifice it unless you sacrifice a land.').
card_first_print('scythe tiger', 'ZEN').
card_image_name('scythe tiger'/'ZEN', 'scythe tiger').
card_uid('scythe tiger'/'ZEN', 'ZEN:Scythe Tiger:scythe tiger').
card_rarity('scythe tiger'/'ZEN', 'Common').
card_artist('scythe tiger'/'ZEN', 'Michael Komarck').
card_number('scythe tiger'/'ZEN', '183').
card_flavor_text('scythe tiger'/'ZEN', 'Instead of adapting to its environment, it adapts its environment to itself.').
card_multiverse_id('scythe tiger'/'ZEN', '178113').

card_in_set('sea gate loremaster', 'ZEN').
card_original_type('sea gate loremaster'/'ZEN', 'Creature — Merfolk Wizard Ally').
card_original_text('sea gate loremaster'/'ZEN', '{T}: Draw a card for each Ally you control.').
card_first_print('sea gate loremaster', 'ZEN').
card_image_name('sea gate loremaster'/'ZEN', 'sea gate loremaster').
card_uid('sea gate loremaster'/'ZEN', 'ZEN:Sea Gate Loremaster:sea gate loremaster').
card_rarity('sea gate loremaster'/'ZEN', 'Rare').
card_artist('sea gate loremaster'/'ZEN', 'Dave Kendall').
card_number('sea gate loremaster'/'ZEN', '63').
card_flavor_text('sea gate loremaster'/'ZEN', '\"He\'s a living library. He remembers everything our band of explorers has seen, and we can use that to our advantage.\"\n—Zahr Gada, Halimar expedition leader').
card_multiverse_id('sea gate loremaster'/'ZEN', '195629').

card_in_set('seascape aerialist', 'ZEN').
card_original_type('seascape aerialist'/'ZEN', 'Creature — Merfolk Wizard Ally').
card_original_text('seascape aerialist'/'ZEN', 'Whenever Seascape Aerialist or another Ally enters the battlefield under your control, you may have Ally creatures you control gain flying until end of turn.').
card_first_print('seascape aerialist', 'ZEN').
card_image_name('seascape aerialist'/'ZEN', 'seascape aerialist').
card_uid('seascape aerialist'/'ZEN', 'ZEN:Seascape Aerialist:seascape aerialist').
card_rarity('seascape aerialist'/'ZEN', 'Uncommon').
card_artist('seascape aerialist'/'ZEN', 'Andrew Robinson').
card_number('seascape aerialist'/'ZEN', '64').
card_flavor_text('seascape aerialist'/'ZEN', '\"The sky is like another sea. It teems with currents, life, and endless possibility.\"').
card_multiverse_id('seascape aerialist'/'ZEN', '195631').

card_in_set('seismic shudder', 'ZEN').
card_original_type('seismic shudder'/'ZEN', 'Instant').
card_original_text('seismic shudder'/'ZEN', 'Seismic Shudder deals 1 damage to each creature without flying.').
card_first_print('seismic shudder', 'ZEN').
card_image_name('seismic shudder'/'ZEN', 'seismic shudder').
card_uid('seismic shudder'/'ZEN', 'ZEN:Seismic Shudder:seismic shudder').
card_rarity('seismic shudder'/'ZEN', 'Common').
card_artist('seismic shudder'/'ZEN', 'Vincent Proce').
card_number('seismic shudder'/'ZEN', '147').
card_flavor_text('seismic shudder'/'ZEN', '\"The land here seems go out of its way to kill you.\"\n—Chandra Nalaar').
card_multiverse_id('seismic shudder'/'ZEN', '180134').

card_in_set('sejiri refuge', 'ZEN').
card_original_type('sejiri refuge'/'ZEN', 'Land').
card_original_text('sejiri refuge'/'ZEN', 'Sejiri Refuge enters the battlefield tapped.\nWhen Sejiri Refuge enters the battlefield, you gain 1 life.\n{T}: Add {W} or {U} to your mana pool.').
card_first_print('sejiri refuge', 'ZEN').
card_image_name('sejiri refuge'/'ZEN', 'sejiri refuge').
card_uid('sejiri refuge'/'ZEN', 'ZEN:Sejiri Refuge:sejiri refuge').
card_rarity('sejiri refuge'/'ZEN', 'Uncommon').
card_artist('sejiri refuge'/'ZEN', 'Ryan Pancoast').
card_number('sejiri refuge'/'ZEN', '224').
card_multiverse_id('sejiri refuge'/'ZEN', '189626').

card_in_set('shatterskull giant', 'ZEN').
card_original_type('shatterskull giant'/'ZEN', 'Creature — Giant Warrior').
card_original_text('shatterskull giant'/'ZEN', '').
card_first_print('shatterskull giant', 'ZEN').
card_image_name('shatterskull giant'/'ZEN', 'shatterskull giant').
card_uid('shatterskull giant'/'ZEN', 'ZEN:Shatterskull Giant:shatterskull giant').
card_rarity('shatterskull giant'/'ZEN', 'Common').
card_artist('shatterskull giant'/'ZEN', 'Kekai Kotaki').
card_number('shatterskull giant'/'ZEN', '148').
card_flavor_text('shatterskull giant'/'ZEN', '\"Now I know why they call it Shatterskull Pass. Rocks fell all night like hammers smashing an anvil. It\'s no wonder the giants are angry all the time.\"\n—Mitra, Bala Ged missionary').
card_multiverse_id('shatterskull giant'/'ZEN', '180505').

card_in_set('shepherd of the lost', 'ZEN').
card_original_type('shepherd of the lost'/'ZEN', 'Creature — Angel').
card_original_text('shepherd of the lost'/'ZEN', 'Flying, first strike, vigilance').
card_first_print('shepherd of the lost', 'ZEN').
card_image_name('shepherd of the lost'/'ZEN', 'shepherd of the lost').
card_uid('shepherd of the lost'/'ZEN', 'ZEN:Shepherd of the Lost:shepherd of the lost').
card_rarity('shepherd of the lost'/'ZEN', 'Uncommon').
card_artist('shepherd of the lost'/'ZEN', 'Kekai Kotaki').
card_number('shepherd of the lost'/'ZEN', '34').
card_flavor_text('shepherd of the lost'/'ZEN', '\"Should you fall in the wilds, lift your voice to the Sky Realm. The one who answers will be your salvation.\"\n—Emeria\'s Creed').
card_multiverse_id('shepherd of the lost'/'ZEN', '193399').

card_in_set('shieldmate\'s blessing', 'ZEN').
card_original_type('shieldmate\'s blessing'/'ZEN', 'Instant').
card_original_text('shieldmate\'s blessing'/'ZEN', 'Prevent the next 3 damage that would be dealt to target creature or player this turn.').
card_first_print('shieldmate\'s blessing', 'ZEN').
card_image_name('shieldmate\'s blessing'/'ZEN', 'shieldmate\'s blessing').
card_uid('shieldmate\'s blessing'/'ZEN', 'ZEN:Shieldmate\'s Blessing:shieldmate\'s blessing').
card_rarity('shieldmate\'s blessing'/'ZEN', 'Common').
card_artist('shieldmate\'s blessing'/'ZEN', 'Mike Bierek').
card_number('shieldmate\'s blessing'/'ZEN', '35').
card_flavor_text('shieldmate\'s blessing'/'ZEN', '\"Even land dwellers may call for Emeria\'s grace in times of need.\"\n—Emeria\'s Creed').
card_multiverse_id('shieldmate\'s blessing'/'ZEN', '178119').

card_in_set('shoal serpent', 'ZEN').
card_original_type('shoal serpent'/'ZEN', 'Creature — Serpent').
card_original_text('shoal serpent'/'ZEN', 'Defender\nLandfall — Whenever a land enters the battlefield under your control, Shoal Serpent loses defender until end of turn.').
card_first_print('shoal serpent', 'ZEN').
card_image_name('shoal serpent'/'ZEN', 'shoal serpent').
card_uid('shoal serpent'/'ZEN', 'ZEN:Shoal Serpent:shoal serpent').
card_rarity('shoal serpent'/'ZEN', 'Common').
card_artist('shoal serpent'/'ZEN', 'Trevor Claxton').
card_number('shoal serpent'/'ZEN', '65').
card_flavor_text('shoal serpent'/'ZEN', '\"It\'s like a reef that runs aground on ships.\"\n—Jaby, Silundi Sea nomad').
card_multiverse_id('shoal serpent'/'ZEN', '177517').

card_in_set('sky ruin drake', 'ZEN').
card_original_type('sky ruin drake'/'ZEN', 'Creature — Drake').
card_original_text('sky ruin drake'/'ZEN', 'Flying').
card_first_print('sky ruin drake', 'ZEN').
card_image_name('sky ruin drake'/'ZEN', 'sky ruin drake').
card_uid('sky ruin drake'/'ZEN', 'ZEN:Sky Ruin Drake:sky ruin drake').
card_rarity('sky ruin drake'/'ZEN', 'Common').
card_artist('sky ruin drake'/'ZEN', 'Izzy').
card_number('sky ruin drake'/'ZEN', '66').
card_flavor_text('sky ruin drake'/'ZEN', '\"Hold up your spears. And try not to look like food.\"\n—Tarsa, Sea Gate sell-sword').
card_multiverse_id('sky ruin drake'/'ZEN', '192211').

card_in_set('slaughter cry', 'ZEN').
card_original_type('slaughter cry'/'ZEN', 'Instant').
card_original_text('slaughter cry'/'ZEN', 'Target creature gets +3/+0 and gains first strike until end of turn.').
card_first_print('slaughter cry', 'ZEN').
card_image_name('slaughter cry'/'ZEN', 'slaughter cry').
card_uid('slaughter cry'/'ZEN', 'ZEN:Slaughter Cry:slaughter cry').
card_rarity('slaughter cry'/'ZEN', 'Common').
card_artist('slaughter cry'/'ZEN', 'Matt Cavotta').
card_number('slaughter cry'/'ZEN', '149').
card_flavor_text('slaughter cry'/'ZEN', '\"Since when did ‘AIIIEEEE!\' become a negotiation tactic?\"\n—Nikou, Joraga bard').
card_multiverse_id('slaughter cry'/'ZEN', '178136').

card_in_set('soaring seacliff', 'ZEN').
card_original_type('soaring seacliff'/'ZEN', 'Land').
card_original_text('soaring seacliff'/'ZEN', 'Soaring Seacliff enters the battlefield tapped.\nWhen Soaring Seacliff enters the battlefield, target creature gains flying until end of turn.\n{T}: Add {U} to your mana pool.').
card_first_print('soaring seacliff', 'ZEN').
card_image_name('soaring seacliff'/'ZEN', 'soaring seacliff').
card_uid('soaring seacliff'/'ZEN', 'ZEN:Soaring Seacliff:soaring seacliff').
card_rarity('soaring seacliff'/'ZEN', 'Common').
card_artist('soaring seacliff'/'ZEN', 'Izzy').
card_number('soaring seacliff'/'ZEN', '225').
card_multiverse_id('soaring seacliff'/'ZEN', '177541').

card_in_set('sorin markov', 'ZEN').
card_original_type('sorin markov'/'ZEN', 'Planeswalker — Sorin').
card_original_text('sorin markov'/'ZEN', '+2: Sorin Markov deals 2 damage to target creature or player and you gain 2 life.\n-3: Target opponent\'s life total becomes 10.\n-7: You control target player\'s next turn.').
card_first_print('sorin markov', 'ZEN').
card_image_name('sorin markov'/'ZEN', 'sorin markov').
card_uid('sorin markov'/'ZEN', 'ZEN:Sorin Markov:sorin markov').
card_rarity('sorin markov'/'ZEN', 'Mythic Rare').
card_artist('sorin markov'/'ZEN', 'Michael Komarck').
card_number('sorin markov'/'ZEN', '111').
card_multiverse_id('sorin markov'/'ZEN', '195403').

card_in_set('soul stair expedition', 'ZEN').
card_original_type('soul stair expedition'/'ZEN', 'Enchantment').
card_original_text('soul stair expedition'/'ZEN', 'Landfall — Whenever a land enters the battlefield under your control, you may put a quest counter on Soul Stair Expedition.\nRemove three quest counters from Soul Stair Expedition and sacrifice it: Return up to two target creature cards from your graveyard to your hand.').
card_first_print('soul stair expedition', 'ZEN').
card_image_name('soul stair expedition'/'ZEN', 'soul stair expedition').
card_uid('soul stair expedition'/'ZEN', 'ZEN:Soul Stair Expedition:soul stair expedition').
card_rarity('soul stair expedition'/'ZEN', 'Common').
card_artist('soul stair expedition'/'ZEN', 'Anthony Francisco').
card_number('soul stair expedition'/'ZEN', '112').
card_multiverse_id('soul stair expedition'/'ZEN', '177586').

card_in_set('spell pierce', 'ZEN').
card_original_type('spell pierce'/'ZEN', 'Instant').
card_original_text('spell pierce'/'ZEN', 'Counter target noncreature spell unless its controller pays {2}.').
card_first_print('spell pierce', 'ZEN').
card_image_name('spell pierce'/'ZEN', 'spell pierce').
card_uid('spell pierce'/'ZEN', 'ZEN:Spell Pierce:spell pierce').
card_rarity('spell pierce'/'ZEN', 'Common').
card_artist('spell pierce'/'ZEN', 'Vance Kovacs').
card_number('spell pierce'/'ZEN', '67').
card_flavor_text('spell pierce'/'ZEN', '\"There\'s a hole in your plan.\"\n—Noyan Dar, Tazeem lullmage').
card_multiverse_id('spell pierce'/'ZEN', '178144').

card_in_set('sphinx of jwar isle', 'ZEN').
card_original_type('sphinx of jwar isle'/'ZEN', 'Creature — Sphinx').
card_original_text('sphinx of jwar isle'/'ZEN', 'Flying, shroud\nYou may look at the top card of your library. (You may do this at any time.)').
card_first_print('sphinx of jwar isle', 'ZEN').
card_image_name('sphinx of jwar isle'/'ZEN', 'sphinx of jwar isle').
card_uid('sphinx of jwar isle'/'ZEN', 'ZEN:Sphinx of Jwar Isle:sphinx of jwar isle').
card_rarity('sphinx of jwar isle'/'ZEN', 'Rare').
card_artist('sphinx of jwar isle'/'ZEN', 'Justin Sweet').
card_number('sphinx of jwar isle'/'ZEN', '68').
card_flavor_text('sphinx of jwar isle'/'ZEN', '\"Even if the sphinxes do know the location of every relic, getting one to talk is harder than just searching yourself.\"\n—Sachir, Akoum Expeditionary House').
card_multiverse_id('sphinx of jwar isle'/'ZEN', '185709').

card_in_set('sphinx of lost truths', 'ZEN').
card_original_type('sphinx of lost truths'/'ZEN', 'Creature — Sphinx').
card_original_text('sphinx of lost truths'/'ZEN', 'Kicker {1}{U} (You may pay an additional {1}{U} as you cast this spell.)\nFlying\nWhen Sphinx of Lost Truths enters the battlefield, draw three cards. Then if it wasn\'t kicked, discard three cards.').
card_first_print('sphinx of lost truths', 'ZEN').
card_image_name('sphinx of lost truths'/'ZEN', 'sphinx of lost truths').
card_uid('sphinx of lost truths'/'ZEN', 'ZEN:Sphinx of Lost Truths:sphinx of lost truths').
card_rarity('sphinx of lost truths'/'ZEN', 'Rare').
card_artist('sphinx of lost truths'/'ZEN', 'Shelly Wan').
card_number('sphinx of lost truths'/'ZEN', '69').
card_multiverse_id('sphinx of lost truths'/'ZEN', '185751').

card_in_set('spidersilk net', 'ZEN').
card_original_type('spidersilk net'/'ZEN', 'Artifact — Equipment').
card_original_text('spidersilk net'/'ZEN', 'Equipped creature gets +0/+2 and has reach. (It can block creatures with flying.)\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('spidersilk net', 'ZEN').
card_image_name('spidersilk net'/'ZEN', 'spidersilk net').
card_uid('spidersilk net'/'ZEN', 'ZEN:Spidersilk Net:spidersilk net').
card_rarity('spidersilk net'/'ZEN', 'Common').
card_artist('spidersilk net'/'ZEN', 'Zoltan Boros & Gabor Szikszai').
card_number('spidersilk net'/'ZEN', '206').
card_multiverse_id('spidersilk net'/'ZEN', '178134').

card_in_set('spire barrage', 'ZEN').
card_original_type('spire barrage'/'ZEN', 'Sorcery').
card_original_text('spire barrage'/'ZEN', 'Spire Barrage deals damage to target creature or player equal to the number of Mountains you control.').
card_first_print('spire barrage', 'ZEN').
card_image_name('spire barrage'/'ZEN', 'spire barrage').
card_uid('spire barrage'/'ZEN', 'ZEN:Spire Barrage:spire barrage').
card_rarity('spire barrage'/'ZEN', 'Common').
card_artist('spire barrage'/'ZEN', 'Ryan Pancoast').
card_number('spire barrage'/'ZEN', '150').
card_flavor_text('spire barrage'/'ZEN', 'Goblin lessons include the 2,071 tips for survival. Frek only remembered 2,070.').
card_multiverse_id('spire barrage'/'ZEN', '180495').

card_in_set('spreading seas', 'ZEN').
card_original_type('spreading seas'/'ZEN', 'Enchantment — Aura').
card_original_text('spreading seas'/'ZEN', 'Enchant land\nWhen Spreading Seas enters the battlefield, draw a card.\nEnchanted land is an Island.').
card_first_print('spreading seas', 'ZEN').
card_image_name('spreading seas'/'ZEN', 'spreading seas').
card_uid('spreading seas'/'ZEN', 'ZEN:Spreading Seas:spreading seas').
card_rarity('spreading seas'/'ZEN', 'Common').
card_artist('spreading seas'/'ZEN', 'Jung Park').
card_number('spreading seas'/'ZEN', '70').
card_flavor_text('spreading seas'/'ZEN', 'Most inhabitants of Zendikar have given up on the idea of an accurate map.').
card_multiverse_id('spreading seas'/'ZEN', '190405').

card_in_set('steppe lynx', 'ZEN').
card_original_type('steppe lynx'/'ZEN', 'Creature — Cat').
card_original_text('steppe lynx'/'ZEN', 'Landfall — Whenever a land enters the battlefield under your control, Steppe Lynx gets +2/+2 until end of turn.').
card_first_print('steppe lynx', 'ZEN').
card_image_name('steppe lynx'/'ZEN', 'steppe lynx').
card_uid('steppe lynx'/'ZEN', 'ZEN:Steppe Lynx:steppe lynx').
card_rarity('steppe lynx'/'ZEN', 'Common').
card_artist('steppe lynx'/'ZEN', 'Nic Klein').
card_number('steppe lynx'/'ZEN', '36').
card_flavor_text('steppe lynx'/'ZEN', 'Nothing quickens the predator\'s blood like the unfamiliar scents of new hunting grounds and the mewling cries of new prey.').
card_multiverse_id('steppe lynx'/'ZEN', '171012').

card_in_set('stonework puma', 'ZEN').
card_original_type('stonework puma'/'ZEN', 'Artifact Creature — Cat Ally').
card_original_text('stonework puma'/'ZEN', '').
card_first_print('stonework puma', 'ZEN').
card_image_name('stonework puma'/'ZEN', 'stonework puma').
card_uid('stonework puma'/'ZEN', 'ZEN:Stonework Puma:stonework puma').
card_rarity('stonework puma'/'ZEN', 'Common').
card_artist('stonework puma'/'ZEN', 'Christopher Moeller').
card_number('stonework puma'/'ZEN', '207').
card_flavor_text('stonework puma'/'ZEN', '\"We suffer uneasy ground, unstable alliances, and unpredictable magic. Something you can truly trust is worth more than a chest of gold.\"\n—Nikou, Joraga bard').
card_multiverse_id('stonework puma'/'ZEN', '190397').

card_in_set('summoner\'s bane', 'ZEN').
card_original_type('summoner\'s bane'/'ZEN', 'Instant').
card_original_text('summoner\'s bane'/'ZEN', 'Counter target creature spell. Put a 2/2 blue Illusion creature token onto the battlefield.').
card_first_print('summoner\'s bane', 'ZEN').
card_image_name('summoner\'s bane'/'ZEN', 'summoner\'s bane').
card_uid('summoner\'s bane'/'ZEN', 'ZEN:Summoner\'s Bane:summoner\'s bane').
card_rarity('summoner\'s bane'/'ZEN', 'Uncommon').
card_artist('summoner\'s bane'/'ZEN', 'Cyril Van Der Haegen').
card_number('summoner\'s bane'/'ZEN', '71').
card_flavor_text('summoner\'s bane'/'ZEN', '\"I don\'t need to have the perfect plan. My foe just has to have an imperfect one.\"\n—Jace Beleren').
card_multiverse_id('summoner\'s bane'/'ZEN', '191360').

card_in_set('summoning trap', 'ZEN').
card_original_type('summoning trap'/'ZEN', 'Instant — Trap').
card_original_text('summoning trap'/'ZEN', 'If a creature spell you cast this turn was countered by a spell or ability an opponent controlled, you may pay {0} rather than pay Summoning Trap\'s mana cost.\nLook at the top seven cards of your library. You may put a creature card from among them onto the battlefield. Put the rest on the bottom of your library in any order.').
card_first_print('summoning trap', 'ZEN').
card_image_name('summoning trap'/'ZEN', 'summoning trap').
card_uid('summoning trap'/'ZEN', 'ZEN:Summoning Trap:summoning trap').
card_rarity('summoning trap'/'ZEN', 'Rare').
card_artist('summoning trap'/'ZEN', 'Kieran Yanner').
card_number('summoning trap'/'ZEN', '184').
card_multiverse_id('summoning trap'/'ZEN', '197527').

card_in_set('sunspring expedition', 'ZEN').
card_original_type('sunspring expedition'/'ZEN', 'Enchantment').
card_original_text('sunspring expedition'/'ZEN', 'Landfall — Whenever a land enters the battlefield under your control, you may put a quest counter on Sunspring Expedition.\nRemove three quest counters from Sunspring Expedition and sacrifice it: You gain 8 life.').
card_first_print('sunspring expedition', 'ZEN').
card_image_name('sunspring expedition'/'ZEN', 'sunspring expedition').
card_uid('sunspring expedition'/'ZEN', 'ZEN:Sunspring Expedition:sunspring expedition').
card_rarity('sunspring expedition'/'ZEN', 'Common').
card_artist('sunspring expedition'/'ZEN', 'Chris J. Anderson').
card_number('sunspring expedition'/'ZEN', '37').
card_multiverse_id('sunspring expedition'/'ZEN', '177551').

card_in_set('surrakar marauder', 'ZEN').
card_original_type('surrakar marauder'/'ZEN', 'Creature — Surrakar').
card_original_text('surrakar marauder'/'ZEN', 'Landfall — Whenever a land enters the battlefield under your control, Surrakar Marauder gains intimidate until end of turn. (It can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)').
card_first_print('surrakar marauder', 'ZEN').
card_image_name('surrakar marauder'/'ZEN', 'surrakar marauder').
card_uid('surrakar marauder'/'ZEN', 'ZEN:Surrakar Marauder:surrakar marauder').
card_rarity('surrakar marauder'/'ZEN', 'Common').
card_artist('surrakar marauder'/'ZEN', 'Kev Walker').
card_number('surrakar marauder'/'ZEN', '113').
card_multiverse_id('surrakar marauder'/'ZEN', '180465').

card_in_set('swamp', 'ZEN').
card_original_type('swamp'/'ZEN', 'Basic Land — Swamp').
card_original_text('swamp'/'ZEN', 'B').
card_image_name('swamp'/'ZEN', 'swamp1').
card_uid('swamp'/'ZEN', 'ZEN:Swamp:swamp1').
card_rarity('swamp'/'ZEN', 'Basic Land').
card_artist('swamp'/'ZEN', 'John Avon').
card_number('swamp'/'ZEN', '238').
card_multiverse_id('swamp'/'ZEN', '201977').

card_in_set('swamp', 'ZEN').
card_original_type('swamp'/'ZEN', 'Basic Land — Swamp').
card_original_text('swamp'/'ZEN', 'B').
card_image_name('swamp'/'ZEN', 'swamp1a').
card_uid('swamp'/'ZEN', 'ZEN:Swamp:swamp1a').
card_rarity('swamp'/'ZEN', 'Basic Land').
card_artist('swamp'/'ZEN', 'John Avon').
card_number('swamp'/'ZEN', '238a').
card_multiverse_id('swamp'/'ZEN', '195172').

card_in_set('swamp', 'ZEN').
card_original_type('swamp'/'ZEN', 'Basic Land — Swamp').
card_original_text('swamp'/'ZEN', 'B').
card_image_name('swamp'/'ZEN', 'swamp2').
card_uid('swamp'/'ZEN', 'ZEN:Swamp:swamp2').
card_rarity('swamp'/'ZEN', 'Basic Land').
card_artist('swamp'/'ZEN', 'Jung Park').
card_number('swamp'/'ZEN', '239').
card_multiverse_id('swamp'/'ZEN', '195194').

card_in_set('swamp', 'ZEN').
card_original_type('swamp'/'ZEN', 'Basic Land — Swamp').
card_original_text('swamp'/'ZEN', 'B').
card_image_name('swamp'/'ZEN', 'swamp2a').
card_uid('swamp'/'ZEN', 'ZEN:Swamp:swamp2a').
card_rarity('swamp'/'ZEN', 'Basic Land').
card_artist('swamp'/'ZEN', 'Jung Park').
card_number('swamp'/'ZEN', '239a').
card_multiverse_id('swamp'/'ZEN', '201978').

card_in_set('swamp', 'ZEN').
card_original_type('swamp'/'ZEN', 'Basic Land — Swamp').
card_original_text('swamp'/'ZEN', 'B').
card_image_name('swamp'/'ZEN', 'swamp3').
card_uid('swamp'/'ZEN', 'ZEN:Swamp:swamp3').
card_rarity('swamp'/'ZEN', 'Basic Land').
card_artist('swamp'/'ZEN', 'Véronique Meignaud').
card_number('swamp'/'ZEN', '240').
card_multiverse_id('swamp'/'ZEN', '201976').

card_in_set('swamp', 'ZEN').
card_original_type('swamp'/'ZEN', 'Basic Land — Swamp').
card_original_text('swamp'/'ZEN', 'B').
card_image_name('swamp'/'ZEN', 'swamp3a').
card_uid('swamp'/'ZEN', 'ZEN:Swamp:swamp3a').
card_rarity('swamp'/'ZEN', 'Basic Land').
card_artist('swamp'/'ZEN', 'Véronique Meignaud').
card_number('swamp'/'ZEN', '240a').
card_multiverse_id('swamp'/'ZEN', '195159').

card_in_set('swamp', 'ZEN').
card_original_type('swamp'/'ZEN', 'Basic Land — Swamp').
card_original_text('swamp'/'ZEN', 'B').
card_image_name('swamp'/'ZEN', 'swamp4').
card_uid('swamp'/'ZEN', 'ZEN:Swamp:swamp4').
card_rarity('swamp'/'ZEN', 'Basic Land').
card_artist('swamp'/'ZEN', 'Vincent Proce').
card_number('swamp'/'ZEN', '241').
card_multiverse_id('swamp'/'ZEN', '195157').

card_in_set('swamp', 'ZEN').
card_original_type('swamp'/'ZEN', 'Basic Land — Swamp').
card_original_text('swamp'/'ZEN', 'B').
card_image_name('swamp'/'ZEN', 'swamp4a').
card_uid('swamp'/'ZEN', 'ZEN:Swamp:swamp4a').
card_rarity('swamp'/'ZEN', 'Basic Land').
card_artist('swamp'/'ZEN', 'Vincent Proce').
card_number('swamp'/'ZEN', '241a').
card_multiverse_id('swamp'/'ZEN', '201975').

card_in_set('tajuru archer', 'ZEN').
card_original_type('tajuru archer'/'ZEN', 'Creature — Elf Archer Ally').
card_original_text('tajuru archer'/'ZEN', 'Whenever Tajuru Archer or another Ally enters the battlefield under your control, you may have Tajuru Archer deal damage to target creature with flying equal to the number of Allies you control.').
card_first_print('tajuru archer', 'ZEN').
card_image_name('tajuru archer'/'ZEN', 'tajuru archer').
card_uid('tajuru archer'/'ZEN', 'ZEN:Tajuru Archer:tajuru archer').
card_rarity('tajuru archer'/'ZEN', 'Uncommon').
card_artist('tajuru archer'/'ZEN', 'Chris Rahn').
card_number('tajuru archer'/'ZEN', '185').
card_multiverse_id('tajuru archer'/'ZEN', '180457').

card_in_set('tanglesap', 'ZEN').
card_original_type('tanglesap'/'ZEN', 'Instant').
card_original_text('tanglesap'/'ZEN', 'Prevent all combat damage that would be dealt this turn by creatures without trample.').
card_first_print('tanglesap', 'ZEN').
card_image_name('tanglesap'/'ZEN', 'tanglesap').
card_uid('tanglesap'/'ZEN', 'ZEN:Tanglesap:tanglesap').
card_rarity('tanglesap'/'ZEN', 'Common').
card_artist('tanglesap'/'ZEN', 'Trevor Claxton').
card_number('tanglesap'/'ZEN', '186').
card_flavor_text('tanglesap'/'ZEN', '\"The blood of the forest is beholden to no one.\"\n—Nissa Revane').
card_multiverse_id('tanglesap'/'ZEN', '191355').

card_in_set('teetering peaks', 'ZEN').
card_original_type('teetering peaks'/'ZEN', 'Land').
card_original_text('teetering peaks'/'ZEN', 'Teetering Peaks enters the battlefield tapped.\nWhen Teetering Peaks enters the battlefield, target creature gets +2/+0 until end of turn.\n{T}: Add {R} to your mana pool.').
card_image_name('teetering peaks'/'ZEN', 'teetering peaks').
card_uid('teetering peaks'/'ZEN', 'ZEN:Teetering Peaks:teetering peaks').
card_rarity('teetering peaks'/'ZEN', 'Common').
card_artist('teetering peaks'/'ZEN', 'Fred Fields').
card_number('teetering peaks'/'ZEN', '226').
card_multiverse_id('teetering peaks'/'ZEN', '177549').

card_in_set('tempest owl', 'ZEN').
card_original_type('tempest owl'/'ZEN', 'Creature — Bird').
card_original_text('tempest owl'/'ZEN', 'Kicker {4}{U} (You may pay an additional {4}{U} as you cast this spell.)\nFlying\nWhen Tempest Owl enters the battlefield, if it was kicked, tap up to three target permanents.').
card_first_print('tempest owl', 'ZEN').
card_image_name('tempest owl'/'ZEN', 'tempest owl').
card_uid('tempest owl'/'ZEN', 'ZEN:Tempest Owl:tempest owl').
card_rarity('tempest owl'/'ZEN', 'Common').
card_artist('tempest owl'/'ZEN', 'Dan Scott').
card_number('tempest owl'/'ZEN', '72').
card_multiverse_id('tempest owl'/'ZEN', '180482').

card_in_set('terra stomper', 'ZEN').
card_original_type('terra stomper'/'ZEN', 'Creature — Beast').
card_original_text('terra stomper'/'ZEN', 'Terra Stomper can\'t be countered.\nTrample').
card_first_print('terra stomper', 'ZEN').
card_image_name('terra stomper'/'ZEN', 'terra stomper').
card_uid('terra stomper'/'ZEN', 'ZEN:Terra Stomper:terra stomper').
card_rarity('terra stomper'/'ZEN', 'Rare').
card_artist('terra stomper'/'ZEN', 'Goran Josic').
card_number('terra stomper'/'ZEN', '187').
card_flavor_text('terra stomper'/'ZEN', 'Sometimes violent earthquakes, hurtling boulders, and unseasonable dust storms are wrongly attributed to the Roil.').
card_multiverse_id('terra stomper'/'ZEN', '186321').

card_in_set('territorial baloth', 'ZEN').
card_original_type('territorial baloth'/'ZEN', 'Creature — Beast').
card_original_text('territorial baloth'/'ZEN', 'Landfall — Whenever a land enters the battlefield under your control, Territorial Baloth gets +2/+2 until end of turn.').
card_first_print('territorial baloth', 'ZEN').
card_image_name('territorial baloth'/'ZEN', 'territorial baloth').
card_uid('territorial baloth'/'ZEN', 'ZEN:Territorial Baloth:territorial baloth').
card_rarity('territorial baloth'/'ZEN', 'Common').
card_artist('territorial baloth'/'ZEN', 'Jesper Ejsing').
card_number('territorial baloth'/'ZEN', '188').
card_flavor_text('territorial baloth'/'ZEN', 'Its territory is defined by wherever it is at the moment.').
card_multiverse_id('territorial baloth'/'ZEN', '170971').

card_in_set('timbermaw larva', 'ZEN').
card_original_type('timbermaw larva'/'ZEN', 'Creature — Beast').
card_original_text('timbermaw larva'/'ZEN', 'Whenever Timbermaw Larva attacks, it gets +1/+1 until end of turn for each Forest you control.').
card_first_print('timbermaw larva', 'ZEN').
card_image_name('timbermaw larva'/'ZEN', 'timbermaw larva').
card_uid('timbermaw larva'/'ZEN', 'ZEN:Timbermaw Larva:timbermaw larva').
card_rarity('timbermaw larva'/'ZEN', 'Common').
card_artist('timbermaw larva'/'ZEN', 'Matt Cavotta').
card_number('timbermaw larva'/'ZEN', '189').
card_flavor_text('timbermaw larva'/'ZEN', '\"You can\'t trust your eyes. A tree might look healthy, but its insides tell a different tale.\"\n—Zahr Gada, Halimar expedition leader').
card_multiverse_id('timbermaw larva'/'ZEN', '180458').

card_in_set('torch slinger', 'ZEN').
card_original_type('torch slinger'/'ZEN', 'Creature — Goblin Shaman').
card_original_text('torch slinger'/'ZEN', 'Kicker {1}{R} (You may pay an additional {1}{R} as you cast this spell.)\nWhen Torch Slinger enters the battlefield, if it was kicked, it deals 2 damage to target creature.').
card_first_print('torch slinger', 'ZEN').
card_image_name('torch slinger'/'ZEN', 'torch slinger').
card_uid('torch slinger'/'ZEN', 'ZEN:Torch Slinger:torch slinger').
card_rarity('torch slinger'/'ZEN', 'Common').
card_artist('torch slinger'/'ZEN', 'Andrew Robinson').
card_number('torch slinger'/'ZEN', '151').
card_flavor_text('torch slinger'/'ZEN', 'Once thrown, twice burned.').
card_multiverse_id('torch slinger'/'ZEN', '180423').

card_in_set('trailblazer\'s boots', 'ZEN').
card_original_type('trailblazer\'s boots'/'ZEN', 'Artifact — Equipment').
card_original_text('trailblazer\'s boots'/'ZEN', 'Equipped creature has nonbasic landwalk. (It\'s unblockable as long as defending player controls a nonbasic land.)\nEquip {2}').
card_first_print('trailblazer\'s boots', 'ZEN').
card_image_name('trailblazer\'s boots'/'ZEN', 'trailblazer\'s boots').
card_uid('trailblazer\'s boots'/'ZEN', 'ZEN:Trailblazer\'s Boots:trailblazer\'s boots').
card_rarity('trailblazer\'s boots'/'ZEN', 'Uncommon').
card_artist('trailblazer\'s boots'/'ZEN', 'Zoltan Boros & Gabor Szikszai').
card_number('trailblazer\'s boots'/'ZEN', '208').
card_flavor_text('trailblazer\'s boots'/'ZEN', 'It\'s much easier to list the places they haven\'t been.').
card_multiverse_id('trailblazer\'s boots'/'ZEN', '190410').

card_in_set('trapfinder\'s trick', 'ZEN').
card_original_type('trapfinder\'s trick'/'ZEN', 'Sorcery').
card_original_text('trapfinder\'s trick'/'ZEN', 'Target player reveals his or her hand and discards all Trap cards.').
card_first_print('trapfinder\'s trick', 'ZEN').
card_image_name('trapfinder\'s trick'/'ZEN', 'trapfinder\'s trick').
card_uid('trapfinder\'s trick'/'ZEN', 'ZEN:Trapfinder\'s Trick:trapfinder\'s trick').
card_rarity('trapfinder\'s trick'/'ZEN', 'Common').
card_artist('trapfinder\'s trick'/'ZEN', 'Philip Straub').
card_number('trapfinder\'s trick'/'ZEN', '73').
card_flavor_text('trapfinder\'s trick'/'ZEN', '\"At some point, every trapfinder will lose a hunk of flesh. It\'s just a question of how much—and whether it\'ll grow back.\"\n—Arhana, Kazandu trapfinder').
card_multiverse_id('trapfinder\'s trick'/'ZEN', '192212').

card_in_set('trapmaker\'s snare', 'ZEN').
card_original_type('trapmaker\'s snare'/'ZEN', 'Instant').
card_original_text('trapmaker\'s snare'/'ZEN', 'Search your library for a Trap card, reveal it, and put it into your hand. Then shuffle your library.').
card_first_print('trapmaker\'s snare', 'ZEN').
card_image_name('trapmaker\'s snare'/'ZEN', 'trapmaker\'s snare').
card_uid('trapmaker\'s snare'/'ZEN', 'ZEN:Trapmaker\'s Snare:trapmaker\'s snare').
card_rarity('trapmaker\'s snare'/'ZEN', 'Uncommon').
card_artist('trapmaker\'s snare'/'ZEN', 'Daarken').
card_number('trapmaker\'s snare'/'ZEN', '74').
card_flavor_text('trapmaker\'s snare'/'ZEN', '\"Trapmaking is an art. The trap is a corporeal riddle, a battle of wits between its creator and whatever it lures inside.\"').
card_multiverse_id('trapmaker\'s snare'/'ZEN', '192223').

card_in_set('trusty machete', 'ZEN').
card_original_type('trusty machete'/'ZEN', 'Artifact — Equipment').
card_original_text('trusty machete'/'ZEN', 'Equipped creature gets +2/+1.\nEquip {2}').
card_first_print('trusty machete', 'ZEN').
card_image_name('trusty machete'/'ZEN', 'trusty machete').
card_uid('trusty machete'/'ZEN', 'ZEN:Trusty Machete:trusty machete').
card_rarity('trusty machete'/'ZEN', 'Uncommon').
card_artist('trusty machete'/'ZEN', 'Raymond Swanland').
card_number('trusty machete'/'ZEN', '209').
card_flavor_text('trusty machete'/'ZEN', '\"Until this expedition is done, that blade is your guardian, your liberator, and your best friend all rolled into one.\"\n—Yon Basrel, Oran-Rief survivalist').
card_multiverse_id('trusty machete'/'ZEN', '193396').

card_in_set('tuktuk grunts', 'ZEN').
card_original_type('tuktuk grunts'/'ZEN', 'Creature — Goblin Warrior Ally').
card_original_text('tuktuk grunts'/'ZEN', 'Haste\nWhenever Tuktuk Grunts or another Ally enters the battlefield under your control, you may put a +1/+1 counter on Tuktuk Grunts.').
card_first_print('tuktuk grunts', 'ZEN').
card_image_name('tuktuk grunts'/'ZEN', 'tuktuk grunts').
card_uid('tuktuk grunts'/'ZEN', 'ZEN:Tuktuk Grunts:tuktuk grunts').
card_rarity('tuktuk grunts'/'ZEN', 'Common').
card_artist('tuktuk grunts'/'ZEN', 'Mike Bierek').
card_number('tuktuk grunts'/'ZEN', '152').
card_flavor_text('tuktuk grunts'/'ZEN', 'Goblins are more aggressive with a crowd to cheer them on.').
card_multiverse_id('tuktuk grunts'/'ZEN', '192229').

card_in_set('turntimber basilisk', 'ZEN').
card_original_type('turntimber basilisk'/'ZEN', 'Creature — Basilisk').
card_original_text('turntimber basilisk'/'ZEN', 'Deathtouch (Creatures dealt damage by this creature are destroyed. You can divide this creature\'s combat damage among any of the creatures blocking or blocked by it.)\nLandfall — Whenever a land enters the battlefield under your control, you may have target creature block Turntimber Basilisk this turn if able.').
card_first_print('turntimber basilisk', 'ZEN').
card_image_name('turntimber basilisk'/'ZEN', 'turntimber basilisk').
card_uid('turntimber basilisk'/'ZEN', 'ZEN:Turntimber Basilisk:turntimber basilisk').
card_rarity('turntimber basilisk'/'ZEN', 'Uncommon').
card_artist('turntimber basilisk'/'ZEN', 'Goran Josic').
card_number('turntimber basilisk'/'ZEN', '190').
card_multiverse_id('turntimber basilisk'/'ZEN', '178140').

card_in_set('turntimber grove', 'ZEN').
card_original_type('turntimber grove'/'ZEN', 'Land').
card_original_text('turntimber grove'/'ZEN', 'Turntimber Grove enters the battlefield tapped.\nWhen Turntimber Grove enters the battlefield, target creature gets +1/+1 until end of turn.\n{T}: Add {G} to your mana pool.').
card_first_print('turntimber grove', 'ZEN').
card_image_name('turntimber grove'/'ZEN', 'turntimber grove').
card_uid('turntimber grove'/'ZEN', 'ZEN:Turntimber Grove:turntimber grove').
card_rarity('turntimber grove'/'ZEN', 'Common').
card_artist('turntimber grove'/'ZEN', 'Rob Alexander').
card_number('turntimber grove'/'ZEN', '227').
card_multiverse_id('turntimber grove'/'ZEN', '169977').

card_in_set('turntimber ranger', 'ZEN').
card_original_type('turntimber ranger'/'ZEN', 'Creature — Elf Scout Ally').
card_original_text('turntimber ranger'/'ZEN', 'Whenever Turntimber Ranger or another Ally enters the battlefield under your control, you may put a 2/2 green Wolf creature token onto the battlefield. If you do, put a +1/+1 counter on Turntimber Ranger.').
card_first_print('turntimber ranger', 'ZEN').
card_image_name('turntimber ranger'/'ZEN', 'turntimber ranger').
card_uid('turntimber ranger'/'ZEN', 'ZEN:Turntimber Ranger:turntimber ranger').
card_rarity('turntimber ranger'/'ZEN', 'Rare').
card_artist('turntimber ranger'/'ZEN', 'Wayne Reynolds').
card_number('turntimber ranger'/'ZEN', '191').
card_flavor_text('turntimber ranger'/'ZEN', 'The lone wolf dies alone.').
card_multiverse_id('turntimber ranger'/'ZEN', '169971').

card_in_set('umara raptor', 'ZEN').
card_original_type('umara raptor'/'ZEN', 'Creature — Bird Ally').
card_original_text('umara raptor'/'ZEN', 'Flying\nWhenever Umara Raptor or another Ally enters the battlefield under your control, you may put a +1/+1 counter on Umara Raptor.').
card_first_print('umara raptor', 'ZEN').
card_image_name('umara raptor'/'ZEN', 'umara raptor').
card_uid('umara raptor'/'ZEN', 'ZEN:Umara Raptor:umara raptor').
card_rarity('umara raptor'/'ZEN', 'Common').
card_artist('umara raptor'/'ZEN', 'Sam Wood').
card_number('umara raptor'/'ZEN', '75').
card_flavor_text('umara raptor'/'ZEN', 'Messenger, weapon, friend.').
card_multiverse_id('umara raptor'/'ZEN', '180447').

card_in_set('unstable footing', 'ZEN').
card_original_type('unstable footing'/'ZEN', 'Instant').
card_original_text('unstable footing'/'ZEN', 'Kicker {3}{R} (You may pay an additional {3}{R} as you cast this spell.)\nDamage can\'t be prevented this turn. If Unstable Footing was kicked, it deals 5 damage to target player.').
card_first_print('unstable footing', 'ZEN').
card_image_name('unstable footing'/'ZEN', 'unstable footing').
card_uid('unstable footing'/'ZEN', 'ZEN:Unstable Footing:unstable footing').
card_rarity('unstable footing'/'ZEN', 'Uncommon').
card_artist('unstable footing'/'ZEN', 'Nic Klein').
card_number('unstable footing'/'ZEN', '153').
card_flavor_text('unstable footing'/'ZEN', 'A long way down is shorter than you think.').
card_multiverse_id('unstable footing'/'ZEN', '192209').

card_in_set('valakut, the molten pinnacle', 'ZEN').
card_original_type('valakut, the molten pinnacle'/'ZEN', 'Land').
card_original_text('valakut, the molten pinnacle'/'ZEN', 'Valakut, the Molten Pinnacle enters the battlefield tapped.\nWhenever a Mountain enters the battlefield under your control, if you control at least five other Mountains, you may have Valakut, the Molten Pinnacle deal 3 damage to target creature or player.\n{T}: Add {R} to your mana pool.').
card_image_name('valakut, the molten pinnacle'/'ZEN', 'valakut, the molten pinnacle').
card_uid('valakut, the molten pinnacle'/'ZEN', 'ZEN:Valakut, the Molten Pinnacle:valakut, the molten pinnacle').
card_rarity('valakut, the molten pinnacle'/'ZEN', 'Rare').
card_artist('valakut, the molten pinnacle'/'ZEN', 'Kieran Yanner').
card_number('valakut, the molten pinnacle'/'ZEN', '228').
card_multiverse_id('valakut, the molten pinnacle'/'ZEN', '190400').

card_in_set('vampire hexmage', 'ZEN').
card_original_type('vampire hexmage'/'ZEN', 'Creature — Vampire Shaman').
card_original_text('vampire hexmage'/'ZEN', 'First strike\nSacrifice Vampire Hexmage: Remove all counters from target permanent.').
card_first_print('vampire hexmage', 'ZEN').
card_image_name('vampire hexmage'/'ZEN', 'vampire hexmage').
card_uid('vampire hexmage'/'ZEN', 'ZEN:Vampire Hexmage:vampire hexmage').
card_rarity('vampire hexmage'/'ZEN', 'Uncommon').
card_artist('vampire hexmage'/'ZEN', 'Eric Deschamps').
card_number('vampire hexmage'/'ZEN', '114').
card_flavor_text('vampire hexmage'/'ZEN', 'When the blood hunt lost its thrill, she looked for less tangible means of domination.').
card_multiverse_id('vampire hexmage'/'ZEN', '192232').

card_in_set('vampire lacerator', 'ZEN').
card_original_type('vampire lacerator'/'ZEN', 'Creature — Vampire Warrior').
card_original_text('vampire lacerator'/'ZEN', 'At the beginning of your upkeep, you lose 1 life unless an opponent has 10 or less life.').
card_first_print('vampire lacerator', 'ZEN').
card_image_name('vampire lacerator'/'ZEN', 'vampire lacerator').
card_uid('vampire lacerator'/'ZEN', 'ZEN:Vampire Lacerator:vampire lacerator').
card_rarity('vampire lacerator'/'ZEN', 'Common').
card_artist('vampire lacerator'/'ZEN', 'Steve Argyle').
card_number('vampire lacerator'/'ZEN', '115').
card_flavor_text('vampire lacerator'/'ZEN', 'The lacerators cut themselves before each hunt. They must feed before the sun rises or bleed to death.').
card_multiverse_id('vampire lacerator'/'ZEN', '192225').

card_in_set('vampire nighthawk', 'ZEN').
card_original_type('vampire nighthawk'/'ZEN', 'Creature — Vampire Shaman').
card_original_text('vampire nighthawk'/'ZEN', 'Flying\nDeathtouch (Creatures dealt damage by this creature are destroyed. You can divide this creature\'s combat damage among any of the creatures blocking or blocked by it.)\nLifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_image_name('vampire nighthawk'/'ZEN', 'vampire nighthawk').
card_uid('vampire nighthawk'/'ZEN', 'ZEN:Vampire Nighthawk:vampire nighthawk').
card_rarity('vampire nighthawk'/'ZEN', 'Uncommon').
card_artist('vampire nighthawk'/'ZEN', 'Jason Chan').
card_number('vampire nighthawk'/'ZEN', '116').
card_multiverse_id('vampire nighthawk'/'ZEN', '185707').

card_in_set('vampire\'s bite', 'ZEN').
card_original_type('vampire\'s bite'/'ZEN', 'Instant').
card_original_text('vampire\'s bite'/'ZEN', 'Kicker {2}{B} (You may pay an additional {2}{B} as you cast this spell.)\nTarget creature gets +3/+0 until end of turn. If Vampire\'s Bite was kicked, that creature gains lifelink until end of turn. (Damage dealt by the creature also causes its controller to gain that much life.)').
card_first_print('vampire\'s bite', 'ZEN').
card_image_name('vampire\'s bite'/'ZEN', 'vampire\'s bite').
card_uid('vampire\'s bite'/'ZEN', 'ZEN:Vampire\'s Bite:vampire\'s bite').
card_rarity('vampire\'s bite'/'ZEN', 'Common').
card_artist('vampire\'s bite'/'ZEN', 'Christopher Moeller').
card_number('vampire\'s bite'/'ZEN', '117').
card_multiverse_id('vampire\'s bite'/'ZEN', '178150').

card_in_set('vastwood gorger', 'ZEN').
card_original_type('vastwood gorger'/'ZEN', 'Creature — Wurm').
card_original_text('vastwood gorger'/'ZEN', '').
card_first_print('vastwood gorger', 'ZEN').
card_image_name('vastwood gorger'/'ZEN', 'vastwood gorger').
card_uid('vastwood gorger'/'ZEN', 'ZEN:Vastwood Gorger:vastwood gorger').
card_rarity('vastwood gorger'/'ZEN', 'Common').
card_artist('vastwood gorger'/'ZEN', 'Kieran Yanner').
card_number('vastwood gorger'/'ZEN', '192').
card_flavor_text('vastwood gorger'/'ZEN', '\"I\'ve known true ferocity and power. Those brazen ‘planar-walkers\' have no idea what wild really means.\"\n—Chadir the Navigator').
card_multiverse_id('vastwood gorger'/'ZEN', '180454').

card_in_set('verdant catacombs', 'ZEN').
card_original_type('verdant catacombs'/'ZEN', 'Land').
card_original_text('verdant catacombs'/'ZEN', '{T}, Pay 1 life, Sacrifice Verdant Catacombs: Search your library for a Swamp or Forest card and put it onto the battlefield. Then shuffle your library.').
card_first_print('verdant catacombs', 'ZEN').
card_image_name('verdant catacombs'/'ZEN', 'verdant catacombs').
card_uid('verdant catacombs'/'ZEN', 'ZEN:Verdant Catacombs:verdant catacombs').
card_rarity('verdant catacombs'/'ZEN', 'Rare').
card_artist('verdant catacombs'/'ZEN', 'Vance Kovacs').
card_number('verdant catacombs'/'ZEN', '229').
card_multiverse_id('verdant catacombs'/'ZEN', '193400').

card_in_set('vines of vastwood', 'ZEN').
card_original_type('vines of vastwood'/'ZEN', 'Instant').
card_original_text('vines of vastwood'/'ZEN', 'Kicker {G} (You may pay an additional {G} as you cast this spell.)\nTarget creature can\'t be the target of spells or abilities your opponents control this turn. If Vines of Vastwood was kicked, that creature gets +4/+4 until end of turn.').
card_first_print('vines of vastwood', 'ZEN').
card_image_name('vines of vastwood'/'ZEN', 'vines of vastwood').
card_uid('vines of vastwood'/'ZEN', 'ZEN:Vines of Vastwood:vines of vastwood').
card_rarity('vines of vastwood'/'ZEN', 'Common').
card_artist('vines of vastwood'/'ZEN', 'Christopher Moeller').
card_number('vines of vastwood'/'ZEN', '193').
card_multiverse_id('vines of vastwood'/'ZEN', '177571').

card_in_set('warren instigator', 'ZEN').
card_original_type('warren instigator'/'ZEN', 'Creature — Goblin Berserker').
card_original_text('warren instigator'/'ZEN', 'Double strike\nWhenever Warren Instigator deals damage to an opponent, you may put a Goblin creature card from your hand onto the battlefield.').
card_first_print('warren instigator', 'ZEN').
card_image_name('warren instigator'/'ZEN', 'warren instigator').
card_uid('warren instigator'/'ZEN', 'ZEN:Warren Instigator:warren instigator').
card_rarity('warren instigator'/'ZEN', 'Mythic Rare').
card_artist('warren instigator'/'ZEN', 'Andrew Robinson').
card_number('warren instigator'/'ZEN', '154').
card_flavor_text('warren instigator'/'ZEN', '\"Danger! Danger! Come out of the safety of your holes!\"').
card_multiverse_id('warren instigator'/'ZEN', '191368').

card_in_set('welkin tern', 'ZEN').
card_original_type('welkin tern'/'ZEN', 'Creature — Bird').
card_original_text('welkin tern'/'ZEN', 'Flying\nWelkin Tern can block only creatures with flying.').
card_first_print('welkin tern', 'ZEN').
card_image_name('welkin tern'/'ZEN', 'welkin tern').
card_uid('welkin tern'/'ZEN', 'ZEN:Welkin Tern:welkin tern').
card_rarity('welkin tern'/'ZEN', 'Common').
card_artist('welkin tern'/'ZEN', 'Austin Hsu').
card_number('welkin tern'/'ZEN', '76').
card_flavor_text('welkin tern'/'ZEN', '\"The sky hedrons are covered with tern nests. It\'s as though the birds have given up on the land altogether.\"\n—Ilori, merfolk falconer').
card_multiverse_id('welkin tern'/'ZEN', '191353').

card_in_set('whiplash trap', 'ZEN').
card_original_type('whiplash trap'/'ZEN', 'Instant — Trap').
card_original_text('whiplash trap'/'ZEN', 'If an opponent had two or more creatures enter the battlefield under his or her control this turn, you may pay {U} rather than pay Whiplash Trap\'s mana cost.\nReturn two target creatures to their owners\' hands.').
card_image_name('whiplash trap'/'ZEN', 'whiplash trap').
card_uid('whiplash trap'/'ZEN', 'ZEN:Whiplash Trap:whiplash trap').
card_rarity('whiplash trap'/'ZEN', 'Common').
card_artist('whiplash trap'/'ZEN', 'Zoltan Boros & Gabor Szikszai').
card_number('whiplash trap'/'ZEN', '77').
card_multiverse_id('whiplash trap'/'ZEN', '197526').

card_in_set('windborne charge', 'ZEN').
card_original_type('windborne charge'/'ZEN', 'Sorcery').
card_original_text('windborne charge'/'ZEN', 'Two target creatures you control each get +2/+2 and gain flying until end of turn.').
card_first_print('windborne charge', 'ZEN').
card_image_name('windborne charge'/'ZEN', 'windborne charge').
card_uid('windborne charge'/'ZEN', 'ZEN:Windborne Charge:windborne charge').
card_rarity('windborne charge'/'ZEN', 'Uncommon').
card_artist('windborne charge'/'ZEN', 'Ryan Pancoast').
card_number('windborne charge'/'ZEN', '38').
card_flavor_text('windborne charge'/'ZEN', 'The merfolk call the sky goddess Emeria. The kor call her Kamsa. The two races agree on little except that she offers many blessings to the faithful.').
card_multiverse_id('windborne charge'/'ZEN', '190418').

card_in_set('windrider eel', 'ZEN').
card_original_type('windrider eel'/'ZEN', 'Creature — Fish').
card_original_text('windrider eel'/'ZEN', 'Flying\nLandfall — Whenever a land enters the battlefield under your control, Windrider Eel gets +2/+2 until end of turn.').
card_first_print('windrider eel', 'ZEN').
card_image_name('windrider eel'/'ZEN', 'windrider eel').
card_uid('windrider eel'/'ZEN', 'ZEN:Windrider Eel:windrider eel').
card_rarity('windrider eel'/'ZEN', 'Common').
card_artist('windrider eel'/'ZEN', 'Austin Hsu').
card_number('windrider eel'/'ZEN', '78').
card_flavor_text('windrider eel'/'ZEN', '\"The best spot to hook one is right behind the gills.\"\n—Rana Cloudwake, kor skyfisher').
card_multiverse_id('windrider eel'/'ZEN', '170982').

card_in_set('world queller', 'ZEN').
card_original_type('world queller'/'ZEN', 'Creature — Avatar').
card_original_text('world queller'/'ZEN', 'At the beginning of your upkeep, you may choose a card type. If you do, each player sacrifices a permanent of that type.').
card_first_print('world queller', 'ZEN').
card_image_name('world queller'/'ZEN', 'world queller').
card_uid('world queller'/'ZEN', 'ZEN:World Queller:world queller').
card_rarity('world queller'/'ZEN', 'Rare').
card_artist('world queller'/'ZEN', 'James Paick').
card_number('world queller'/'ZEN', '39').
card_flavor_text('world queller'/'ZEN', '\"Why fight the world when you know who will win?\"\n—Nissa Revane').
card_multiverse_id('world queller'/'ZEN', '191376').

card_in_set('zektar shrine expedition', 'ZEN').
card_original_type('zektar shrine expedition'/'ZEN', 'Enchantment').
card_original_text('zektar shrine expedition'/'ZEN', 'Landfall — Whenever a land enters the battlefield under your control, you may put a quest counter on Zektar Shrine Expedition.\nRemove three quest counters from Zektar Shrine Expedition and sacrifice it: Put a 7/1 red Elemental creature token with trample and haste onto the battlefield. Exile it at the beginning of the next end step.').
card_first_print('zektar shrine expedition', 'ZEN').
card_image_name('zektar shrine expedition'/'ZEN', 'zektar shrine expedition').
card_uid('zektar shrine expedition'/'ZEN', 'ZEN:Zektar Shrine Expedition:zektar shrine expedition').
card_rarity('zektar shrine expedition'/'ZEN', 'Common').
card_artist('zektar shrine expedition'/'ZEN', 'Justin Sweet').
card_number('zektar shrine expedition'/'ZEN', '155').
card_multiverse_id('zektar shrine expedition'/'ZEN', '183403').

card_in_set('zendikar farguide', 'ZEN').
card_original_type('zendikar farguide'/'ZEN', 'Creature — Elemental').
card_original_text('zendikar farguide'/'ZEN', 'Forestwalk').
card_first_print('zendikar farguide', 'ZEN').
card_image_name('zendikar farguide'/'ZEN', 'zendikar farguide').
card_uid('zendikar farguide'/'ZEN', 'ZEN:Zendikar Farguide:zendikar farguide').
card_rarity('zendikar farguide'/'ZEN', 'Common').
card_artist('zendikar farguide'/'ZEN', 'Vincent Proce').
card_number('zendikar farguide'/'ZEN', '194').
card_flavor_text('zendikar farguide'/'ZEN', 'Expeditions follow the paths it leaves in its wake.').
card_multiverse_id('zendikar farguide'/'ZEN', '189620').
