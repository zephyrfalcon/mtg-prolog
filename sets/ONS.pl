% Onslaught

set('ONS').
set_name('ONS', 'Onslaught').
set_release_date('ONS', '2002-10-07').
set_border('ONS', 'black').
set_type('ONS', 'expansion').
set_block('ONS', 'Onslaught').

card_in_set('accursed centaur', 'ONS').
card_original_type('accursed centaur'/'ONS', 'Creature — Zombie Centaur').
card_original_text('accursed centaur'/'ONS', 'When Accursed Centaur comes into play, sacrifice a creature.').
card_first_print('accursed centaur', 'ONS').
card_image_name('accursed centaur'/'ONS', 'accursed centaur').
card_uid('accursed centaur'/'ONS', 'ONS:Accursed Centaur:accursed centaur').
card_rarity('accursed centaur'/'ONS', 'Common').
card_artist('accursed centaur'/'ONS', 'Jerry Tiritilli').
card_number('accursed centaur'/'ONS', '123').
card_flavor_text('accursed centaur'/'ONS', '\"The Cabal mocks the natural order. For its minions, death is just a pause between duties.\"\n—Kamahl, druid acolyte').
card_multiverse_id('accursed centaur'/'ONS', '26715').

card_in_set('æther charge', 'ONS').
card_original_type('æther charge'/'ONS', 'Enchantment').
card_original_text('æther charge'/'ONS', 'Whenever a Beast comes into play under your control, you may have it deal 4 damage to target opponent.').
card_first_print('æther charge', 'ONS').
card_image_name('æther charge'/'ONS', 'aether charge').
card_uid('æther charge'/'ONS', 'ONS:Æther Charge:aether charge').
card_rarity('æther charge'/'ONS', 'Uncommon').
card_artist('æther charge'/'ONS', 'Mark Brill').
card_number('æther charge'/'ONS', '184').
card_flavor_text('æther charge'/'ONS', '\"Is it just me, or does that meteor have teeth?\"').
card_multiverse_id('æther charge'/'ONS', '12425').

card_in_set('aggravated assault', 'ONS').
card_original_type('aggravated assault'/'ONS', 'Enchantment').
card_original_text('aggravated assault'/'ONS', '{3}{R}{R}: Untap all creatures you control. After this phase, there is an additional combat phase followed by an additional main phase. Play this ability only any time you could play a sorcery.').
card_first_print('aggravated assault', 'ONS').
card_image_name('aggravated assault'/'ONS', 'aggravated assault').
card_uid('aggravated assault'/'ONS', 'ONS:Aggravated Assault:aggravated assault').
card_rarity('aggravated assault'/'ONS', 'Rare').
card_artist('aggravated assault'/'ONS', 'Greg Staples').
card_number('aggravated assault'/'ONS', '185').
card_multiverse_id('aggravated assault'/'ONS', '40195').

card_in_set('airborne aid', 'ONS').
card_original_type('airborne aid'/'ONS', 'Sorcery').
card_original_text('airborne aid'/'ONS', 'Draw a card for each Bird in play.').
card_first_print('airborne aid', 'ONS').
card_image_name('airborne aid'/'ONS', 'airborne aid').
card_uid('airborne aid'/'ONS', 'ONS:Airborne Aid:airborne aid').
card_rarity('airborne aid'/'ONS', 'Common').
card_artist('airborne aid'/'ONS', 'Bradley Williams').
card_number('airborne aid'/'ONS', '62').
card_flavor_text('airborne aid'/'ONS', '\"We help our Southern Order comrades as much as we can. But the Cabal\'s insidious influence spreads, dividing our forces and leaving new arrivals to fend for themselves.\"\n—Commander Eesha').
card_multiverse_id('airborne aid'/'ONS', '39889').

card_in_set('airdrop condor', 'ONS').
card_original_type('airdrop condor'/'ONS', 'Creature — Bird').
card_original_text('airdrop condor'/'ONS', 'Flying\n{1}{R}, Sacrifice a Goblin: Airdrop Condor deals damage equal to the sacrificed Goblin\'s power to target creature or player.').
card_first_print('airdrop condor', 'ONS').
card_image_name('airdrop condor'/'ONS', 'airdrop condor').
card_uid('airdrop condor'/'ONS', 'ONS:Airdrop Condor:airdrop condor').
card_rarity('airdrop condor'/'ONS', 'Uncommon').
card_artist('airdrop condor'/'ONS', 'Glen Angus').
card_number('airdrop condor'/'ONS', '186').
card_flavor_text('airdrop condor'/'ONS', 'It has two kinds of droppings, neither of which is particularly pleasant.').
card_multiverse_id('airdrop condor'/'ONS', '39755').

card_in_set('akroma\'s blessing', 'ONS').
card_original_type('akroma\'s blessing'/'ONS', 'Instant').
card_original_text('akroma\'s blessing'/'ONS', 'Creatures you control gain protection from the color of your choice until end of turn.\nCycling {W} ({W}, Discard this card from your hand: Draw a card.)').
card_first_print('akroma\'s blessing', 'ONS').
card_image_name('akroma\'s blessing'/'ONS', 'akroma\'s blessing').
card_uid('akroma\'s blessing'/'ONS', 'ONS:Akroma\'s Blessing:akroma\'s blessing').
card_rarity('akroma\'s blessing'/'ONS', 'Uncommon').
card_artist('akroma\'s blessing'/'ONS', 'Adam Rex').
card_number('akroma\'s blessing'/'ONS', '1').
card_flavor_text('akroma\'s blessing'/'ONS', 'The clerics saw her as a divine gift. She saw them only as allies in her war against Phage.').
card_multiverse_id('akroma\'s blessing'/'ONS', '41162').

card_in_set('akroma\'s vengeance', 'ONS').
card_original_type('akroma\'s vengeance'/'ONS', 'Sorcery').
card_original_text('akroma\'s vengeance'/'ONS', 'Destroy all artifacts, creatures, and enchantments.\nCycling {3} ({3}, Discard this card from your hand: Draw a card.)').
card_first_print('akroma\'s vengeance', 'ONS').
card_image_name('akroma\'s vengeance'/'ONS', 'akroma\'s vengeance').
card_uid('akroma\'s vengeance'/'ONS', 'ONS:Akroma\'s Vengeance:akroma\'s vengeance').
card_rarity('akroma\'s vengeance'/'ONS', 'Rare').
card_artist('akroma\'s vengeance'/'ONS', 'Greg & Tim Hildebrandt').
card_number('akroma\'s vengeance'/'ONS', '2').
card_flavor_text('akroma\'s vengeance'/'ONS', 'Ixidor had only to imagine their ruin and Akroma made it so.').
card_multiverse_id('akroma\'s vengeance'/'ONS', '41168').

card_in_set('ancestor\'s prophet', 'ONS').
card_original_type('ancestor\'s prophet'/'ONS', 'Creature — Cleric Lord').
card_original_text('ancestor\'s prophet'/'ONS', 'Tap five untapped Clerics you control: You gain 10 life.').
card_first_print('ancestor\'s prophet', 'ONS').
card_image_name('ancestor\'s prophet'/'ONS', 'ancestor\'s prophet').
card_uid('ancestor\'s prophet'/'ONS', 'ONS:Ancestor\'s Prophet:ancestor\'s prophet').
card_rarity('ancestor\'s prophet'/'ONS', 'Rare').
card_artist('ancestor\'s prophet'/'ONS', 'Kev Walker').
card_number('ancestor\'s prophet'/'ONS', '3').
card_flavor_text('ancestor\'s prophet'/'ONS', '\"We have faced horrors of war and terrors beyond imagining. We will overcome the uncertainties of this new life.\"').
card_multiverse_id('ancestor\'s prophet'/'ONS', '39882').

card_in_set('animal magnetism', 'ONS').
card_original_type('animal magnetism'/'ONS', 'Sorcery').
card_original_text('animal magnetism'/'ONS', 'Reveal the top five cards of your library. An opponent chooses a creature card from among them. Put that card into play and the rest into your graveyard.').
card_first_print('animal magnetism', 'ONS').
card_image_name('animal magnetism'/'ONS', 'animal magnetism').
card_uid('animal magnetism'/'ONS', 'ONS:Animal Magnetism:animal magnetism').
card_rarity('animal magnetism'/'ONS', 'Rare').
card_artist('animal magnetism'/'ONS', 'Ron Spears').
card_number('animal magnetism'/'ONS', '245').
card_multiverse_id('animal magnetism'/'ONS', '42190').

card_in_set('annex', 'ONS').
card_original_type('annex'/'ONS', 'Enchant Land').
card_original_text('annex'/'ONS', 'You control enchanted land.').
card_first_print('annex', 'ONS').
card_image_name('annex'/'ONS', 'annex').
card_uid('annex'/'ONS', 'ONS:Annex:annex').
card_rarity('annex'/'ONS', 'Uncommon').
card_artist('annex'/'ONS', 'John Avon').
card_number('annex'/'ONS', '63').
card_flavor_text('annex'/'ONS', 'Most refugees left their ancestral homes behind when they came to Otaria. A few decided to bring them along.').
card_multiverse_id('annex'/'ONS', '41463').

card_in_set('anurid murkdiver', 'ONS').
card_original_type('anurid murkdiver'/'ONS', 'Creature — Zombie Beast').
card_original_text('anurid murkdiver'/'ONS', 'Swampwalk').
card_first_print('anurid murkdiver', 'ONS').
card_image_name('anurid murkdiver'/'ONS', 'anurid murkdiver').
card_uid('anurid murkdiver'/'ONS', 'ONS:Anurid Murkdiver:anurid murkdiver').
card_rarity('anurid murkdiver'/'ONS', 'Common').
card_artist('anurid murkdiver'/'ONS', 'Dany Orizio').
card_number('anurid murkdiver'/'ONS', '124').
card_flavor_text('anurid murkdiver'/'ONS', 'Aphetto\'s swamps are alive with the dead.').
card_multiverse_id('anurid murkdiver'/'ONS', '43638').

card_in_set('aphetto alchemist', 'ONS').
card_original_type('aphetto alchemist'/'ONS', 'Creature — Wizard').
card_original_text('aphetto alchemist'/'ONS', '{T}: Untap target artifact or creature.\nMorph {U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('aphetto alchemist', 'ONS').
card_image_name('aphetto alchemist'/'ONS', 'aphetto alchemist').
card_uid('aphetto alchemist'/'ONS', 'ONS:Aphetto Alchemist:aphetto alchemist').
card_rarity('aphetto alchemist'/'ONS', 'Uncommon').
card_artist('aphetto alchemist'/'ONS', 'Ron Spears').
card_number('aphetto alchemist'/'ONS', '64').
card_flavor_text('aphetto alchemist'/'ONS', 'He brews trouble.').
card_multiverse_id('aphetto alchemist'/'ONS', '39530').

card_in_set('aphetto dredging', 'ONS').
card_original_type('aphetto dredging'/'ONS', 'Sorcery').
card_original_text('aphetto dredging'/'ONS', 'Return up to three target creature cards of the creature type of your choice from your graveyard to your hand.').
card_first_print('aphetto dredging', 'ONS').
card_image_name('aphetto dredging'/'ONS', 'aphetto dredging').
card_uid('aphetto dredging'/'ONS', 'ONS:Aphetto Dredging:aphetto dredging').
card_rarity('aphetto dredging'/'ONS', 'Common').
card_artist('aphetto dredging'/'ONS', 'Monte Michael Moore').
card_number('aphetto dredging'/'ONS', '125').
card_flavor_text('aphetto dredging'/'ONS', 'Phage became both executioner and savior, helping others to the same rebirth she had found.').
card_multiverse_id('aphetto dredging'/'ONS', '39704').

card_in_set('aphetto grifter', 'ONS').
card_original_type('aphetto grifter'/'ONS', 'Creature — Wizard').
card_original_text('aphetto grifter'/'ONS', 'Tap two untapped Wizards you control: Tap target permanent.').
card_first_print('aphetto grifter', 'ONS').
card_image_name('aphetto grifter'/'ONS', 'aphetto grifter').
card_uid('aphetto grifter'/'ONS', 'ONS:Aphetto Grifter:aphetto grifter').
card_rarity('aphetto grifter'/'ONS', 'Uncommon').
card_artist('aphetto grifter'/'ONS', 'Greg Staples').
card_number('aphetto grifter'/'ONS', '65').
card_flavor_text('aphetto grifter'/'ONS', 'Aphetto con artists started working in pairs to make it less likely they\'d be the victims of con artists.').
card_multiverse_id('aphetto grifter'/'ONS', '39419').

card_in_set('aphetto vulture', 'ONS').
card_original_type('aphetto vulture'/'ONS', 'Creature — Zombie Bird').
card_original_text('aphetto vulture'/'ONS', 'Flying\nWhen Aphetto Vulture is put into a graveyard from play, you may put target Zombie card from your graveyard on top of your library.').
card_first_print('aphetto vulture', 'ONS').
card_image_name('aphetto vulture'/'ONS', 'aphetto vulture').
card_uid('aphetto vulture'/'ONS', 'ONS:Aphetto Vulture:aphetto vulture').
card_rarity('aphetto vulture'/'ONS', 'Uncommon').
card_artist('aphetto vulture'/'ONS', 'Tony Szczudlo').
card_number('aphetto vulture'/'ONS', '126').
card_multiverse_id('aphetto vulture'/'ONS', '39906').

card_in_set('arcanis the omnipotent', 'ONS').
card_original_type('arcanis the omnipotent'/'ONS', 'Creature — Wizard Legend').
card_original_text('arcanis the omnipotent'/'ONS', '{T}: Draw three cards.\n{2}{U}{U}: Return Arcanis the Omnipotent to its owner\'s hand.').
card_first_print('arcanis the omnipotent', 'ONS').
card_image_name('arcanis the omnipotent'/'ONS', 'arcanis the omnipotent').
card_uid('arcanis the omnipotent'/'ONS', 'ONS:Arcanis the Omnipotent:arcanis the omnipotent').
card_rarity('arcanis the omnipotent'/'ONS', 'Rare').
card_artist('arcanis the omnipotent'/'ONS', 'Justin Sweet').
card_number('arcanis the omnipotent'/'ONS', '66').
card_flavor_text('arcanis the omnipotent'/'ONS', 'He has journeyed where none have been before. Now he returns to ensure that none follow.').
card_multiverse_id('arcanis the omnipotent'/'ONS', '39932').

card_in_set('artificial evolution', 'ONS').
card_original_type('artificial evolution'/'ONS', 'Instant').
card_original_text('artificial evolution'/'ONS', 'Change the text of target spell or permanent by replacing all instances of one creature type with another. The new creature type can\'t be Legend or Wall. (This effect doesn\'t end at end of turn.)').
card_first_print('artificial evolution', 'ONS').
card_image_name('artificial evolution'/'ONS', 'artificial evolution').
card_uid('artificial evolution'/'ONS', 'ONS:Artificial Evolution:artificial evolution').
card_rarity('artificial evolution'/'ONS', 'Rare').
card_artist('artificial evolution'/'ONS', 'Greg Staples').
card_number('artificial evolution'/'ONS', '67').
card_multiverse_id('artificial evolution'/'ONS', '39923').

card_in_set('ascending aven', 'ONS').
card_original_type('ascending aven'/'ONS', 'Creature — Bird Soldier').
card_original_text('ascending aven'/'ONS', 'Flying\nAscending Aven may block only creatures with flying.\nMorph {2}{U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('ascending aven', 'ONS').
card_image_name('ascending aven'/'ONS', 'ascending aven').
card_uid('ascending aven'/'ONS', 'ONS:Ascending Aven:ascending aven').
card_rarity('ascending aven'/'ONS', 'Common').
card_artist('ascending aven'/'ONS', 'Ron Spencer').
card_number('ascending aven'/'ONS', '68').
card_multiverse_id('ascending aven'/'ONS', '25879').

card_in_set('astral slide', 'ONS').
card_original_type('astral slide'/'ONS', 'Enchantment').
card_original_text('astral slide'/'ONS', 'Whenever a player cycles a card, you may remove target creature from the game. If you do, return that creature to play under its owner\'s control at end of turn.').
card_image_name('astral slide'/'ONS', 'astral slide').
card_uid('astral slide'/'ONS', 'ONS:Astral Slide:astral slide').
card_rarity('astral slide'/'ONS', 'Uncommon').
card_artist('astral slide'/'ONS', 'Ron Spears').
card_number('astral slide'/'ONS', '4').
card_flavor_text('astral slide'/'ONS', '\"The hum of the universe is never off-key.\"\n—Mystic elder').
card_multiverse_id('astral slide'/'ONS', '41148').

card_in_set('aura extraction', 'ONS').
card_original_type('aura extraction'/'ONS', 'Instant').
card_original_text('aura extraction'/'ONS', 'Put target enchantment on top of its owner\'s library.\nCycling {2} ({2}, Discard this card from your hand: Draw a card.)').
card_first_print('aura extraction', 'ONS').
card_image_name('aura extraction'/'ONS', 'aura extraction').
card_uid('aura extraction'/'ONS', 'ONS:Aura Extraction:aura extraction').
card_rarity('aura extraction'/'ONS', 'Uncommon').
card_artist('aura extraction'/'ONS', 'Luca Zontini').
card_number('aura extraction'/'ONS', '5').
card_flavor_text('aura extraction'/'ONS', 'Every day, Order clerics contain as much of the Mirari\'s energy as possible, hoping to delay Otaria\'s demise.').
card_multiverse_id('aura extraction'/'ONS', '35326').

card_in_set('aurification', 'ONS').
card_original_type('aurification'/'ONS', 'Enchantment').
card_original_text('aurification'/'ONS', 'Whenever a creature deals damage to you, put a gold counter on it.\nEach creature with a gold counter on it is a Wall in addition to its other creature types. (Walls can\'t attack.)\nWhen Aurification leaves play, remove all gold counters from all creatures.').
card_first_print('aurification', 'ONS').
card_image_name('aurification'/'ONS', 'aurification').
card_uid('aurification'/'ONS', 'ONS:Aurification:aurification').
card_rarity('aurification'/'ONS', 'Rare').
card_artist('aurification'/'ONS', 'Gary Ruddell').
card_number('aurification'/'ONS', '6').
card_multiverse_id('aurification'/'ONS', '39900').

card_in_set('avarax', 'ONS').
card_original_type('avarax'/'ONS', 'Creature — Beast').
card_original_text('avarax'/'ONS', 'Haste\nWhen Avarax comes into play, you may search your library for a card named Avarax, reveal it, and put it into your hand. If you do, shuffle your library.\n{1}{R}: Avarax gets +1/+0 until end of turn.').
card_first_print('avarax', 'ONS').
card_image_name('avarax'/'ONS', 'avarax').
card_uid('avarax'/'ONS', 'ONS:Avarax:avarax').
card_rarity('avarax'/'ONS', 'Uncommon').
card_artist('avarax'/'ONS', 'Greg Staples').
card_number('avarax'/'ONS', '187').
card_multiverse_id('avarax'/'ONS', '43331').

card_in_set('aven brigadier', 'ONS').
card_original_type('aven brigadier'/'ONS', 'Creature — Bird Soldier').
card_original_text('aven brigadier'/'ONS', 'Flying\nAll other Birds get +1/+1.\nAll other Soldiers get +1/+1.').
card_first_print('aven brigadier', 'ONS').
card_image_name('aven brigadier'/'ONS', 'aven brigadier').
card_uid('aven brigadier'/'ONS', 'ONS:Aven Brigadier:aven brigadier').
card_rarity('aven brigadier'/'ONS', 'Rare').
card_artist('aven brigadier'/'ONS', 'Greg Staples').
card_number('aven brigadier'/'ONS', '7').
card_flavor_text('aven brigadier'/'ONS', 'He represents what little pride the Order has left.').
card_multiverse_id('aven brigadier'/'ONS', '40131').

card_in_set('aven fateshaper', 'ONS').
card_original_type('aven fateshaper'/'ONS', 'Creature — Bird Wizard').
card_original_text('aven fateshaper'/'ONS', 'Flying\nWhen Aven Fateshaper comes into play, look at the top four cards of your library, then put them back in any order.\n{4}{U}: Look at the top four cards of your library, then put them back in any order.').
card_first_print('aven fateshaper', 'ONS').
card_image_name('aven fateshaper'/'ONS', 'aven fateshaper').
card_uid('aven fateshaper'/'ONS', 'ONS:Aven Fateshaper:aven fateshaper').
card_rarity('aven fateshaper'/'ONS', 'Uncommon').
card_artist('aven fateshaper'/'ONS', 'Anthony S. Waters').
card_number('aven fateshaper'/'ONS', '69').
card_multiverse_id('aven fateshaper'/'ONS', '40655').

card_in_set('aven soulgazer', 'ONS').
card_original_type('aven soulgazer'/'ONS', 'Creature — Bird Cleric').
card_original_text('aven soulgazer'/'ONS', 'Flying\n{2}{W}: Look at target face-down creature.').
card_first_print('aven soulgazer', 'ONS').
card_image_name('aven soulgazer'/'ONS', 'aven soulgazer').
card_uid('aven soulgazer'/'ONS', 'ONS:Aven Soulgazer:aven soulgazer').
card_rarity('aven soulgazer'/'ONS', 'Uncommon').
card_artist('aven soulgazer'/'ONS', 'John Avon').
card_number('aven soulgazer'/'ONS', '8').
card_flavor_text('aven soulgazer'/'ONS', '\"Every question has a proper answer. Every soul has a proper place.\"').
card_multiverse_id('aven soulgazer'/'ONS', '39520').

card_in_set('backslide', 'ONS').
card_original_type('backslide'/'ONS', 'Instant').
card_original_text('backslide'/'ONS', 'Turn target creature with morph face down.\nCycling {U} ({U}, Discard this card from your hand: Draw a card.)').
card_first_print('backslide', 'ONS').
card_image_name('backslide'/'ONS', 'backslide').
card_uid('backslide'/'ONS', 'ONS:Backslide:backslide').
card_rarity('backslide'/'ONS', 'Common').
card_artist('backslide'/'ONS', 'Pete Venters').
card_number('backslide'/'ONS', '70').
card_flavor_text('backslide'/'ONS', 'Some things are better left unknown.').
card_multiverse_id('backslide'/'ONS', '26592').

card_in_set('barkhide mauler', 'ONS').
card_original_type('barkhide mauler'/'ONS', 'Creature — Beast').
card_original_text('barkhide mauler'/'ONS', 'Cycling {2} ({2}, Discard this card from your hand: Draw a card.)').
card_first_print('barkhide mauler', 'ONS').
card_image_name('barkhide mauler'/'ONS', 'barkhide mauler').
card_uid('barkhide mauler'/'ONS', 'ONS:Barkhide Mauler:barkhide mauler').
card_rarity('barkhide mauler'/'ONS', 'Common').
card_artist('barkhide mauler'/'ONS', 'Iain McCaig').
card_number('barkhide mauler'/'ONS', '246').
card_flavor_text('barkhide mauler'/'ONS', 'Anywhere else they would be hunted for their skins, but in Wirewood, they are safe.').
card_multiverse_id('barkhide mauler'/'ONS', '34196').

card_in_set('barren moor', 'ONS').
card_original_type('barren moor'/'ONS', 'Land').
card_original_text('barren moor'/'ONS', 'Barren Moor comes into play tapped.\n{T}: Add {B} to your mana pool.\nCycling {B} ({B}, Discard this card from your hand: Draw a card.)').
card_first_print('barren moor', 'ONS').
card_image_name('barren moor'/'ONS', 'barren moor').
card_uid('barren moor'/'ONS', 'ONS:Barren Moor:barren moor').
card_rarity('barren moor'/'ONS', 'Common').
card_artist('barren moor'/'ONS', 'Heather Hudson').
card_number('barren moor'/'ONS', '312').
card_multiverse_id('barren moor'/'ONS', '41139').

card_in_set('battering craghorn', 'ONS').
card_original_type('battering craghorn'/'ONS', 'Creature — Beast').
card_original_text('battering craghorn'/'ONS', 'First strike\nMorph {1}{R}{R} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('battering craghorn', 'ONS').
card_image_name('battering craghorn'/'ONS', 'battering craghorn').
card_uid('battering craghorn'/'ONS', 'ONS:Battering Craghorn:battering craghorn').
card_rarity('battering craghorn'/'ONS', 'Common').
card_artist('battering craghorn'/'ONS', 'Matt Cavotta').
card_number('battering craghorn'/'ONS', '188').
card_flavor_text('battering craghorn'/'ONS', 'Their skeletons can be found all over Skirk Ridge, tangled in each other\'s horns.').
card_multiverse_id('battering craghorn'/'ONS', '39486').

card_in_set('battlefield medic', 'ONS').
card_original_type('battlefield medic'/'ONS', 'Creature — Cleric').
card_original_text('battlefield medic'/'ONS', '{T}: Prevent the next X damage that would be dealt to target creature this turn, where X is the number of Clerics in play.').
card_first_print('battlefield medic', 'ONS').
card_image_name('battlefield medic'/'ONS', 'battlefield medic').
card_uid('battlefield medic'/'ONS', 'ONS:Battlefield Medic:battlefield medic').
card_rarity('battlefield medic'/'ONS', 'Common').
card_artist('battlefield medic'/'ONS', 'Matt Thompson').
card_number('battlefield medic'/'ONS', '9').
card_flavor_text('battlefield medic'/'ONS', '\"Death never stops to rest. Neither can we.\"').
card_multiverse_id('battlefield medic'/'ONS', '39430').

card_in_set('biorhythm', 'ONS').
card_original_type('biorhythm'/'ONS', 'Sorcery').
card_original_text('biorhythm'/'ONS', 'Each player\'s life total becomes the number of creatures he or she controls.').
card_first_print('biorhythm', 'ONS').
card_image_name('biorhythm'/'ONS', 'biorhythm').
card_uid('biorhythm'/'ONS', 'ONS:Biorhythm:biorhythm').
card_rarity('biorhythm'/'ONS', 'Rare').
card_artist('biorhythm'/'ONS', 'Ron Spears').
card_number('biorhythm'/'ONS', '247').
card_flavor_text('biorhythm'/'ONS', '\"I have seen life\'s purpose, and now it is my own.\"\n—Kamahl, druid acolyte').
card_multiverse_id('biorhythm'/'ONS', '39913').

card_in_set('birchlore rangers', 'ONS').
card_original_type('birchlore rangers'/'ONS', 'Creature — Elf').
card_original_text('birchlore rangers'/'ONS', 'Tap two untapped Elves you control: Add one mana of any color to your mana pool.\nMorph {G} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('birchlore rangers', 'ONS').
card_image_name('birchlore rangers'/'ONS', 'birchlore rangers').
card_uid('birchlore rangers'/'ONS', 'ONS:Birchlore Rangers:birchlore rangers').
card_rarity('birchlore rangers'/'ONS', 'Common').
card_artist('birchlore rangers'/'ONS', 'Dany Orizio').
card_number('birchlore rangers'/'ONS', '248').
card_multiverse_id('birchlore rangers'/'ONS', '39836').

card_in_set('blackmail', 'ONS').
card_original_type('blackmail'/'ONS', 'Sorcery').
card_original_text('blackmail'/'ONS', 'Target player reveals three cards from his or her hand and you choose one of them. That player discards that card.').
card_first_print('blackmail', 'ONS').
card_image_name('blackmail'/'ONS', 'blackmail').
card_uid('blackmail'/'ONS', 'ONS:Blackmail:blackmail').
card_rarity('blackmail'/'ONS', 'Uncommon').
card_artist('blackmail'/'ONS', 'Christopher Moeller').
card_number('blackmail'/'ONS', '127').
card_flavor_text('blackmail'/'ONS', 'Even the most virtuous person is only one secret away from being owned by the Cabal.').
card_multiverse_id('blackmail'/'ONS', '39709').

card_in_set('blatant thievery', 'ONS').
card_original_type('blatant thievery'/'ONS', 'Sorcery').
card_original_text('blatant thievery'/'ONS', 'For each opponent, gain control of target permanent that player controls. (This effect doesn\'t end at end of turn.)').
card_first_print('blatant thievery', 'ONS').
card_image_name('blatant thievery'/'ONS', 'blatant thievery').
card_uid('blatant thievery'/'ONS', 'ONS:Blatant Thievery:blatant thievery').
card_rarity('blatant thievery'/'ONS', 'Rare').
card_artist('blatant thievery'/'ONS', 'Ron Spencer').
card_number('blatant thievery'/'ONS', '71').
card_flavor_text('blatant thievery'/'ONS', '\"I\'ll leave subtlety to the rich.\"').
card_multiverse_id('blatant thievery'/'ONS', '41156').

card_in_set('blistering firecat', 'ONS').
card_original_type('blistering firecat'/'ONS', 'Creature — Cat').
card_original_text('blistering firecat'/'ONS', 'Trample, haste\nAt end of turn, sacrifice Blistering Firecat.\nMorph {R}{R} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('blistering firecat', 'ONS').
card_image_name('blistering firecat'/'ONS', 'blistering firecat').
card_uid('blistering firecat'/'ONS', 'ONS:Blistering Firecat:blistering firecat').
card_rarity('blistering firecat'/'ONS', 'Rare').
card_artist('blistering firecat'/'ONS', 'Arnie Swekel').
card_number('blistering firecat'/'ONS', '189').
card_multiverse_id('blistering firecat'/'ONS', '39737').

card_in_set('bloodline shaman', 'ONS').
card_original_type('bloodline shaman'/'ONS', 'Creature — Elf Wizard').
card_original_text('bloodline shaman'/'ONS', '{T}: Choose a creature type. Reveal the top card of your library. If that card is a creature card of the chosen type, put it into your hand. Otherwise, put it into your graveyard.').
card_first_print('bloodline shaman', 'ONS').
card_image_name('bloodline shaman'/'ONS', 'bloodline shaman').
card_uid('bloodline shaman'/'ONS', 'ONS:Bloodline Shaman:bloodline shaman').
card_rarity('bloodline shaman'/'ONS', 'Uncommon').
card_artist('bloodline shaman'/'ONS', 'Rebecca Guay').
card_number('bloodline shaman'/'ONS', '249').
card_flavor_text('bloodline shaman'/'ONS', 'Every creature of the forest has a name, and she knows them all.').
card_multiverse_id('bloodline shaman'/'ONS', '39903').

card_in_set('bloodstained mire', 'ONS').
card_original_type('bloodstained mire'/'ONS', 'Land').
card_original_text('bloodstained mire'/'ONS', '{T}, Pay 1 life, Sacrifice Bloodstained Mire: Search your library for a swamp or mountain card and put it into play. Then shuffle your library.').
card_image_name('bloodstained mire'/'ONS', 'bloodstained mire').
card_uid('bloodstained mire'/'ONS', 'ONS:Bloodstained Mire:bloodstained mire').
card_rarity('bloodstained mire'/'ONS', 'Rare').
card_artist('bloodstained mire'/'ONS', 'Rob Alexander').
card_number('bloodstained mire'/'ONS', '313').
card_multiverse_id('bloodstained mire'/'ONS', '39505').

card_in_set('boneknitter', 'ONS').
card_original_type('boneknitter'/'ONS', 'Creature — Zombie Cleric').
card_original_text('boneknitter'/'ONS', '{1}{B}: Regenerate target Zombie.\nMorph {2}{B} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('boneknitter', 'ONS').
card_image_name('boneknitter'/'ONS', 'boneknitter').
card_uid('boneknitter'/'ONS', 'ONS:Boneknitter:boneknitter').
card_rarity('boneknitter'/'ONS', 'Uncommon').
card_artist('boneknitter'/'ONS', 'Pete Venters').
card_number('boneknitter'/'ONS', '128').
card_flavor_text('boneknitter'/'ONS', 'Nothing comes together as easily as it comes apart.').
card_multiverse_id('boneknitter'/'ONS', '39547').

card_in_set('break open', 'ONS').
card_original_type('break open'/'ONS', 'Instant').
card_original_text('break open'/'ONS', 'Turn target face-down creature an opponent controls face up.').
card_first_print('break open', 'ONS').
card_image_name('break open'/'ONS', 'break open').
card_uid('break open'/'ONS', 'ONS:Break Open:break open').
card_rarity('break open'/'ONS', 'Common').
card_artist('break open'/'ONS', 'Alex Horley-Orlandelli').
card_number('break open'/'ONS', '190').
card_flavor_text('break open'/'ONS', 'There are two ways to resolve puzzling situations: thoughtful contemplation or force. After thoughtful contemplation, most barbarians choose force.').
card_multiverse_id('break open'/'ONS', '39868').

card_in_set('brightstone ritual', 'ONS').
card_original_type('brightstone ritual'/'ONS', 'Instant').
card_original_text('brightstone ritual'/'ONS', 'Add {R} to your mana pool for each Goblin in play.').
card_first_print('brightstone ritual', 'ONS').
card_image_name('brightstone ritual'/'ONS', 'brightstone ritual').
card_uid('brightstone ritual'/'ONS', 'ONS:Brightstone Ritual:brightstone ritual').
card_rarity('brightstone ritual'/'ONS', 'Common').
card_artist('brightstone ritual'/'ONS', 'Wayne England').
card_number('brightstone ritual'/'ONS', '191').
card_flavor_text('brightstone ritual'/'ONS', 'Wizards fought over the stone to exploit its power. Goblins fight over it because it\'s shiny.').
card_multiverse_id('brightstone ritual'/'ONS', '39846').

card_in_set('broodhatch nantuko', 'ONS').
card_original_type('broodhatch nantuko'/'ONS', 'Creature — Insect Druid').
card_original_text('broodhatch nantuko'/'ONS', 'Whenever Broodhatch Nantuko is dealt damage, you may put that many 1/1 green Insect creature tokens into play.\nMorph {2}{G} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('broodhatch nantuko', 'ONS').
card_image_name('broodhatch nantuko'/'ONS', 'broodhatch nantuko').
card_uid('broodhatch nantuko'/'ONS', 'ONS:Broodhatch Nantuko:broodhatch nantuko').
card_rarity('broodhatch nantuko'/'ONS', 'Uncommon').
card_artist('broodhatch nantuko'/'ONS', 'Keith Garletts').
card_number('broodhatch nantuko'/'ONS', '250').
card_multiverse_id('broodhatch nantuko'/'ONS', '39742').

card_in_set('butcher orgg', 'ONS').
card_original_type('butcher orgg'/'ONS', 'Creature — Orgg').
card_original_text('butcher orgg'/'ONS', 'You may divide Butcher Orgg\'s combat damage as you choose among defending player and/or any number of creatures he or she controls.').
card_first_print('butcher orgg', 'ONS').
card_image_name('butcher orgg'/'ONS', 'butcher orgg').
card_uid('butcher orgg'/'ONS', 'ONS:Butcher Orgg:butcher orgg').
card_rarity('butcher orgg'/'ONS', 'Rare').
card_artist('butcher orgg'/'ONS', 'Kev Walker').
card_number('butcher orgg'/'ONS', '192').
card_flavor_text('butcher orgg'/'ONS', 'It can kill you with three arms tied behind its back.').
card_multiverse_id('butcher orgg'/'ONS', '39658').

card_in_set('cabal archon', 'ONS').
card_original_type('cabal archon'/'ONS', 'Creature — Cleric').
card_original_text('cabal archon'/'ONS', '{B}, Sacrifice a Cleric: Target player loses 2 life and you gain 2 life.').
card_first_print('cabal archon', 'ONS').
card_image_name('cabal archon'/'ONS', 'cabal archon').
card_uid('cabal archon'/'ONS', 'ONS:Cabal Archon:cabal archon').
card_rarity('cabal archon'/'ONS', 'Uncommon').
card_artist('cabal archon'/'ONS', 'Pete Venters').
card_number('cabal archon'/'ONS', '129').
card_flavor_text('cabal archon'/'ONS', '\"You are weak. I am strong. The protocol is obvious.\"').
card_multiverse_id('cabal archon'/'ONS', '39545').

card_in_set('cabal executioner', 'ONS').
card_original_type('cabal executioner'/'ONS', 'Creature — Cleric').
card_original_text('cabal executioner'/'ONS', 'Whenever Cabal Executioner deals combat damage to a player, that player sacrifices a creature.\nMorph {3}{B}{B} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('cabal executioner', 'ONS').
card_image_name('cabal executioner'/'ONS', 'cabal executioner').
card_uid('cabal executioner'/'ONS', 'ONS:Cabal Executioner:cabal executioner').
card_rarity('cabal executioner'/'ONS', 'Uncommon').
card_artist('cabal executioner'/'ONS', 'Rebecca Guay').
card_number('cabal executioner'/'ONS', '130').
card_multiverse_id('cabal executioner'/'ONS', '39851').

card_in_set('cabal slaver', 'ONS').
card_original_type('cabal slaver'/'ONS', 'Creature — Cleric').
card_original_text('cabal slaver'/'ONS', 'Whenever a Goblin deals combat damage to a player, that player discards a card from his or her hand.').
card_first_print('cabal slaver', 'ONS').
card_image_name('cabal slaver'/'ONS', 'cabal slaver').
card_uid('cabal slaver'/'ONS', 'ONS:Cabal Slaver:cabal slaver').
card_rarity('cabal slaver'/'ONS', 'Uncommon').
card_artist('cabal slaver'/'ONS', 'Pete Venters').
card_number('cabal slaver'/'ONS', '131').
card_flavor_text('cabal slaver'/'ONS', '\"In its natural habitat, the goblin is stupid, yet devious. Now it\'s stupid, devious, and powerful.\"').
card_multiverse_id('cabal slaver'/'ONS', '39467').

card_in_set('callous oppressor', 'ONS').
card_original_type('callous oppressor'/'ONS', 'Creature — Cephalid').
card_original_text('callous oppressor'/'ONS', 'You may choose not to untap Callous Oppressor during your untap step.\nAs Callous Oppressor comes into play, an opponent chooses a creature type.\n{T}: Gain control of target creature that isn\'t of the chosen type as long as Callous Oppressor remains tapped.').
card_first_print('callous oppressor', 'ONS').
card_image_name('callous oppressor'/'ONS', 'callous oppressor').
card_uid('callous oppressor'/'ONS', 'ONS:Callous Oppressor:callous oppressor').
card_rarity('callous oppressor'/'ONS', 'Rare').
card_artist('callous oppressor'/'ONS', 'Justin Sweet').
card_number('callous oppressor'/'ONS', '72').
card_multiverse_id('callous oppressor'/'ONS', '39620').

card_in_set('catapult master', 'ONS').
card_original_type('catapult master'/'ONS', 'Creature — Soldier Lord').
card_original_text('catapult master'/'ONS', 'Tap five untapped Soldiers you control: Remove target creature from the game.').
card_first_print('catapult master', 'ONS').
card_image_name('catapult master'/'ONS', 'catapult master').
card_uid('catapult master'/'ONS', 'ONS:Catapult Master:catapult master').
card_rarity('catapult master'/'ONS', 'Rare').
card_artist('catapult master'/'ONS', 'Terese Nielsen').
card_number('catapult master'/'ONS', '10').
card_flavor_text('catapult master'/'ONS', '\"There\'s no ‘I\' in ‘team,\' but there\'s a ‘we\' in ‘weapon.\'\"').
card_multiverse_id('catapult master'/'ONS', '19680').

card_in_set('catapult squad', 'ONS').
card_original_type('catapult squad'/'ONS', 'Creature — Soldier').
card_original_text('catapult squad'/'ONS', 'Tap two untapped Soldiers you control: Catapult Squad deals 2 damage to target attacking or blocking creature.').
card_first_print('catapult squad', 'ONS').
card_image_name('catapult squad'/'ONS', 'catapult squad').
card_uid('catapult squad'/'ONS', 'ONS:Catapult Squad:catapult squad').
card_rarity('catapult squad'/'ONS', 'Uncommon').
card_artist('catapult squad'/'ONS', 'Brian Snõddy').
card_number('catapult squad'/'ONS', '11').
card_flavor_text('catapult squad'/'ONS', 'Together they could hit anything between the heavens and the horizon.').
card_multiverse_id('catapult squad'/'ONS', '39702').

card_in_set('centaur glade', 'ONS').
card_original_type('centaur glade'/'ONS', 'Enchantment').
card_original_text('centaur glade'/'ONS', '{2}{G}{G}: Put a 3/3 green Centaur creature token into play.').
card_first_print('centaur glade', 'ONS').
card_image_name('centaur glade'/'ONS', 'centaur glade').
card_uid('centaur glade'/'ONS', 'ONS:Centaur Glade:centaur glade').
card_rarity('centaur glade'/'ONS', 'Uncommon').
card_artist('centaur glade'/'ONS', 'Alex Horley-Orlandelli').
card_number('centaur glade'/'ONS', '251').
card_flavor_text('centaur glade'/'ONS', 'The Mirari called to the centaurs, and all who heard it were forever changed.').
card_multiverse_id('centaur glade'/'ONS', '39678').

card_in_set('chain of acid', 'ONS').
card_original_type('chain of acid'/'ONS', 'Sorcery').
card_original_text('chain of acid'/'ONS', 'Destroy target noncreature permanent. Then that permanent\'s controller may copy this spell and may choose a new target for that copy.').
card_first_print('chain of acid', 'ONS').
card_image_name('chain of acid'/'ONS', 'chain of acid').
card_uid('chain of acid'/'ONS', 'ONS:Chain of Acid:chain of acid').
card_rarity('chain of acid'/'ONS', 'Uncommon').
card_artist('chain of acid'/'ONS', 'Ron Spencer').
card_number('chain of acid'/'ONS', '252').
card_multiverse_id('chain of acid'/'ONS', '12445').

card_in_set('chain of plasma', 'ONS').
card_original_type('chain of plasma'/'ONS', 'Instant').
card_original_text('chain of plasma'/'ONS', 'Chain of Plasma deals 3 damage to target creature or player. Then that player or that creature\'s controller may discard a card from his or her hand. If the player does, he or she may copy this spell and may choose a new target for that copy.').
card_first_print('chain of plasma', 'ONS').
card_image_name('chain of plasma'/'ONS', 'chain of plasma').
card_uid('chain of plasma'/'ONS', 'ONS:Chain of Plasma:chain of plasma').
card_rarity('chain of plasma'/'ONS', 'Uncommon').
card_artist('chain of plasma'/'ONS', 'Gary Ruddell').
card_number('chain of plasma'/'ONS', '193').
card_multiverse_id('chain of plasma'/'ONS', '12449').

card_in_set('chain of silence', 'ONS').
card_original_type('chain of silence'/'ONS', 'Instant').
card_original_text('chain of silence'/'ONS', 'Prevent all damage target creature would deal this turn. That creature\'s controller may sacrifice a land. If the player does, he or she may copy this spell and may choose a new target for that copy.').
card_first_print('chain of silence', 'ONS').
card_image_name('chain of silence'/'ONS', 'chain of silence').
card_uid('chain of silence'/'ONS', 'ONS:Chain of Silence:chain of silence').
card_rarity('chain of silence'/'ONS', 'Uncommon').
card_artist('chain of silence'/'ONS', 'Randy Gallegos').
card_number('chain of silence'/'ONS', '12').
card_multiverse_id('chain of silence'/'ONS', '12446').

card_in_set('chain of smog', 'ONS').
card_original_type('chain of smog'/'ONS', 'Sorcery').
card_original_text('chain of smog'/'ONS', 'Target player discards two cards from his or her hand. That player may copy this spell and may choose a new target for that copy.').
card_first_print('chain of smog', 'ONS').
card_image_name('chain of smog'/'ONS', 'chain of smog').
card_uid('chain of smog'/'ONS', 'ONS:Chain of Smog:chain of smog').
card_rarity('chain of smog'/'ONS', 'Uncommon').
card_artist('chain of smog'/'ONS', 'Greg Staples').
card_number('chain of smog'/'ONS', '132').
card_multiverse_id('chain of smog'/'ONS', '12448').

card_in_set('chain of vapor', 'ONS').
card_original_type('chain of vapor'/'ONS', 'Instant').
card_original_text('chain of vapor'/'ONS', 'Return target nonland permanent to its owner\'s hand. Then that permanent\'s controller may sacrifice a land. If the player does, he or she may copy this spell and may choose a new target for that copy.').
card_first_print('chain of vapor', 'ONS').
card_image_name('chain of vapor'/'ONS', 'chain of vapor').
card_uid('chain of vapor'/'ONS', 'ONS:Chain of Vapor:chain of vapor').
card_rarity('chain of vapor'/'ONS', 'Uncommon').
card_artist('chain of vapor'/'ONS', 'Carl Critchlow').
card_number('chain of vapor'/'ONS', '73').
card_multiverse_id('chain of vapor'/'ONS', '12447').

card_in_set('charging slateback', 'ONS').
card_original_type('charging slateback'/'ONS', 'Creature — Beast').
card_original_text('charging slateback'/'ONS', 'Charging Slateback can\'t block.\nMorph {4}{R} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('charging slateback', 'ONS').
card_image_name('charging slateback'/'ONS', 'charging slateback').
card_uid('charging slateback'/'ONS', 'ONS:Charging Slateback:charging slateback').
card_rarity('charging slateback'/'ONS', 'Common').
card_artist('charging slateback'/'ONS', 'Mark Tedin').
card_number('charging slateback'/'ONS', '194').
card_flavor_text('charging slateback'/'ONS', 'Goblins prize its hide for building rock sled runners.').
card_multiverse_id('charging slateback'/'ONS', '39469').

card_in_set('choking tethers', 'ONS').
card_original_type('choking tethers'/'ONS', 'Instant').
card_original_text('choking tethers'/'ONS', 'Tap up to four target creatures.\nCycling {1}{U} ({1}{U}, Discard this card from your hand: Draw a card.)\nWhen you cycle Choking Tethers, you may tap target creature.').
card_first_print('choking tethers', 'ONS').
card_image_name('choking tethers'/'ONS', 'choking tethers').
card_uid('choking tethers'/'ONS', 'ONS:Choking Tethers:choking tethers').
card_rarity('choking tethers'/'ONS', 'Common').
card_artist('choking tethers'/'ONS', 'Carl Critchlow').
card_number('choking tethers'/'ONS', '74').
card_multiverse_id('choking tethers'/'ONS', '41161').

card_in_set('circle of solace', 'ONS').
card_original_type('circle of solace'/'ONS', 'Enchantment').
card_original_text('circle of solace'/'ONS', 'As Circle of Solace comes into play, choose a creature type.\n{1}{W}: The next time a creature of the chosen type would deal damage to you this turn, prevent that damage.').
card_first_print('circle of solace', 'ONS').
card_image_name('circle of solace'/'ONS', 'circle of solace').
card_uid('circle of solace'/'ONS', 'ONS:Circle of Solace:circle of solace').
card_rarity('circle of solace'/'ONS', 'Rare').
card_artist('circle of solace'/'ONS', 'Greg & Tim Hildebrandt').
card_number('circle of solace'/'ONS', '13').
card_multiverse_id('circle of solace'/'ONS', '39615').

card_in_set('clone', 'ONS').
card_original_type('clone'/'ONS', 'Creature — Clone').
card_original_text('clone'/'ONS', 'As Clone comes into play, you may choose a creature in play. If you do, Clone comes into play as a copy of that creature.').
card_image_name('clone'/'ONS', 'clone').
card_uid('clone'/'ONS', 'ONS:Clone:clone').
card_rarity('clone'/'ONS', 'Rare').
card_artist('clone'/'ONS', 'Carl Critchlow').
card_number('clone'/'ONS', '75').
card_multiverse_id('clone'/'ONS', '39533').

card_in_set('commando raid', 'ONS').
card_original_type('commando raid'/'ONS', 'Instant').
card_original_text('commando raid'/'ONS', 'Until end of turn, target creature you control gains \"When this creature deals combat damage to a player, you may have it deal damage equal to its power to target creature that player controls.\"').
card_first_print('commando raid', 'ONS').
card_image_name('commando raid'/'ONS', 'commando raid').
card_uid('commando raid'/'ONS', 'ONS:Commando Raid:commando raid').
card_rarity('commando raid'/'ONS', 'Uncommon').
card_artist('commando raid'/'ONS', 'Ron Spencer').
card_number('commando raid'/'ONS', '195').
card_multiverse_id('commando raid'/'ONS', '34397').

card_in_set('complicate', 'ONS').
card_original_type('complicate'/'ONS', 'Instant').
card_original_text('complicate'/'ONS', 'Counter target spell unless its controller pays {3}.\nCycling {2}{U} ({2}{U}, Discard this card from your hand: Draw a card.)\nWhen you cycle Complicate, you may counter target spell unless its controller pays {1}.').
card_first_print('complicate', 'ONS').
card_image_name('complicate'/'ONS', 'complicate').
card_uid('complicate'/'ONS', 'ONS:Complicate:complicate').
card_rarity('complicate'/'ONS', 'Uncommon').
card_artist('complicate'/'ONS', 'Scott M. Fischer').
card_number('complicate'/'ONS', '76').
card_multiverse_id('complicate'/'ONS', '41146').

card_in_set('contested cliffs', 'ONS').
card_original_type('contested cliffs'/'ONS', 'Land').
card_original_text('contested cliffs'/'ONS', '{T}: Add {1} to your mana pool.\n{R}{G}, {T}: Choose target Beast you control and target creature an opponent controls. Each creature deals damage equal to its power to the other.').
card_first_print('contested cliffs', 'ONS').
card_image_name('contested cliffs'/'ONS', 'contested cliffs').
card_uid('contested cliffs'/'ONS', 'ONS:Contested Cliffs:contested cliffs').
card_rarity('contested cliffs'/'ONS', 'Rare').
card_artist('contested cliffs'/'ONS', 'Anthony S. Waters').
card_number('contested cliffs'/'ONS', '314').
card_multiverse_id('contested cliffs'/'ONS', '40540').

card_in_set('convalescent care', 'ONS').
card_original_type('convalescent care'/'ONS', 'Enchantment').
card_original_text('convalescent care'/'ONS', 'At the beginning of your upkeep, if you have 5 life or less, you gain 3 life and draw a card.').
card_first_print('convalescent care', 'ONS').
card_image_name('convalescent care'/'ONS', 'convalescent care').
card_uid('convalescent care'/'ONS', 'ONS:Convalescent Care:convalescent care').
card_rarity('convalescent care'/'ONS', 'Rare').
card_artist('convalescent care'/'ONS', 'Greg & Tim Hildebrandt').
card_number('convalescent care'/'ONS', '14').
card_flavor_text('convalescent care'/'ONS', 'Enlightenment comes most swiftly at life\'s end.').
card_multiverse_id('convalescent care'/'ONS', '19122').

card_in_set('cover of darkness', 'ONS').
card_original_type('cover of darkness'/'ONS', 'Enchantment').
card_original_text('cover of darkness'/'ONS', 'As Cover of Darkness comes into play, choose a creature type.\nCreatures of the chosen type have fear. (They can\'t be blocked except by artifact creatures and/or black creatures.)').
card_first_print('cover of darkness', 'ONS').
card_image_name('cover of darkness'/'ONS', 'cover of darkness').
card_uid('cover of darkness'/'ONS', 'ONS:Cover of Darkness:cover of darkness').
card_rarity('cover of darkness'/'ONS', 'Rare').
card_artist('cover of darkness'/'ONS', 'Kev Walker').
card_number('cover of darkness'/'ONS', '133').
card_multiverse_id('cover of darkness'/'ONS', '40129').

card_in_set('crafty pathmage', 'ONS').
card_original_type('crafty pathmage'/'ONS', 'Creature — Wizard').
card_original_text('crafty pathmage'/'ONS', '{T}: Target creature with power 2 or less is unblockable this turn.').
card_first_print('crafty pathmage', 'ONS').
card_image_name('crafty pathmage'/'ONS', 'crafty pathmage').
card_uid('crafty pathmage'/'ONS', 'ONS:Crafty Pathmage:crafty pathmage').
card_rarity('crafty pathmage'/'ONS', 'Common').
card_artist('crafty pathmage'/'ONS', 'Wayne England').
card_number('crafty pathmage'/'ONS', '77').
card_flavor_text('crafty pathmage'/'ONS', 'The most valuable commodity in the pits is a way out.').
card_multiverse_id('crafty pathmage'/'ONS', '40060').

card_in_set('crowd favorites', 'ONS').
card_original_type('crowd favorites'/'ONS', 'Creature — Soldier').
card_original_text('crowd favorites'/'ONS', '{3}{W}: Tap target creature.\n{3}{W}: Crowd Favorites gets +0/+5 until end of turn.').
card_first_print('crowd favorites', 'ONS').
card_image_name('crowd favorites'/'ONS', 'crowd favorites').
card_uid('crowd favorites'/'ONS', 'ONS:Crowd Favorites:crowd favorites').
card_rarity('crowd favorites'/'ONS', 'Uncommon').
card_artist('crowd favorites'/'ONS', 'Roger Raupp').
card_number('crowd favorites'/'ONS', '15').
card_flavor_text('crowd favorites'/'ONS', '\"The rabble likes them. Make sure they win, then book them for tomorrow.\"\n—Cabal Patriarch').
card_multiverse_id('crowd favorites'/'ONS', '40653').

card_in_set('crown of ascension', 'ONS').
card_original_type('crown of ascension'/'ONS', 'Enchant Creature').
card_original_text('crown of ascension'/'ONS', 'Enchanted creature has flying.\nSacrifice Crown of Ascension: Enchanted creature and other creatures that share a creature type with it gain flying until end of turn.').
card_first_print('crown of ascension', 'ONS').
card_image_name('crown of ascension'/'ONS', 'crown of ascension').
card_uid('crown of ascension'/'ONS', 'ONS:Crown of Ascension:crown of ascension').
card_rarity('crown of ascension'/'ONS', 'Common').
card_artist('crown of ascension'/'ONS', 'Bradley Williams').
card_number('crown of ascension'/'ONS', '78').
card_flavor_text('crown of ascension'/'ONS', '\"Wisdom, clear my eyes.\"').
card_multiverse_id('crown of ascension'/'ONS', '39883').

card_in_set('crown of awe', 'ONS').
card_original_type('crown of awe'/'ONS', 'Enchant Creature').
card_original_text('crown of awe'/'ONS', 'Enchanted creature has protection from black and from red.\nSacrifice Crown of Awe: Enchanted creature and other creatures that share a creature type with it gain protection from black and from red until end of turn.').
card_first_print('crown of awe', 'ONS').
card_image_name('crown of awe'/'ONS', 'crown of awe').
card_uid('crown of awe'/'ONS', 'ONS:Crown of Awe:crown of awe').
card_rarity('crown of awe'/'ONS', 'Common').
card_artist('crown of awe'/'ONS', 'Randy Elliott').
card_number('crown of awe'/'ONS', '16').
card_flavor_text('crown of awe'/'ONS', '\"Honor, guide my step.\"').
card_multiverse_id('crown of awe'/'ONS', '39431').

card_in_set('crown of fury', 'ONS').
card_original_type('crown of fury'/'ONS', 'Enchant Creature').
card_original_text('crown of fury'/'ONS', 'Enchanted creature gets +1/+0 and has first strike.\nSacrifice Crown of Fury: Enchanted creature and other creatures that share a creature type with it get +1/+0 and gain first strike until end of turn.').
card_first_print('crown of fury', 'ONS').
card_image_name('crown of fury'/'ONS', 'crown of fury').
card_uid('crown of fury'/'ONS', 'ONS:Crown of Fury:crown of fury').
card_rarity('crown of fury'/'ONS', 'Common').
card_artist('crown of fury'/'ONS', 'Bradley Williams').
card_number('crown of fury'/'ONS', '196').
card_flavor_text('crown of fury'/'ONS', '\"Passion, fire my heart.\"').
card_multiverse_id('crown of fury'/'ONS', '40058').

card_in_set('crown of suspicion', 'ONS').
card_original_type('crown of suspicion'/'ONS', 'Enchant Creature').
card_original_text('crown of suspicion'/'ONS', 'Enchanted creature gets +2/-1.\nSacrifice Crown of Suspicion: Enchanted creature and other creatures that share a creature type with it get +2/-1 until end of turn.').
card_first_print('crown of suspicion', 'ONS').
card_image_name('crown of suspicion'/'ONS', 'crown of suspicion').
card_uid('crown of suspicion'/'ONS', 'ONS:Crown of Suspicion:crown of suspicion').
card_rarity('crown of suspicion'/'ONS', 'Common').
card_artist('crown of suspicion'/'ONS', 'Wayne England').
card_number('crown of suspicion'/'ONS', '134').
card_flavor_text('crown of suspicion'/'ONS', '\"Darkness, hide my fear.\"').
card_multiverse_id('crown of suspicion'/'ONS', '40059').

card_in_set('crown of vigor', 'ONS').
card_original_type('crown of vigor'/'ONS', 'Enchant Creature').
card_original_text('crown of vigor'/'ONS', 'Enchanted creature gets +1/+1.\nSacrifice Crown of Vigor: Enchanted creature and other creatures that share a creature type with it get +1/+1 until end of turn.').
card_first_print('crown of vigor', 'ONS').
card_image_name('crown of vigor'/'ONS', 'crown of vigor').
card_uid('crown of vigor'/'ONS', 'ONS:Crown of Vigor:crown of vigor').
card_rarity('crown of vigor'/'ONS', 'Common').
card_artist('crown of vigor'/'ONS', 'Matt Cavotta').
card_number('crown of vigor'/'ONS', '253').
card_flavor_text('crown of vigor'/'ONS', '\"Life, be my shield.\"').
card_multiverse_id('crown of vigor'/'ONS', '39926').

card_in_set('crude rampart', 'ONS').
card_original_type('crude rampart'/'ONS', 'Creature — Wall').
card_original_text('crude rampart'/'ONS', '(Walls can\'t attack.)\nMorph {4}{W} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('crude rampart', 'ONS').
card_image_name('crude rampart'/'ONS', 'crude rampart').
card_uid('crude rampart'/'ONS', 'ONS:Crude Rampart:crude rampart').
card_rarity('crude rampart'/'ONS', 'Uncommon').
card_artist('crude rampart'/'ONS', 'Sam Wood').
card_number('crude rampart'/'ONS', '17').
card_flavor_text('crude rampart'/'ONS', 'Success is one part inspiration, nine parts desperation.').
card_multiverse_id('crude rampart'/'ONS', '39425').

card_in_set('cruel revival', 'ONS').
card_original_type('cruel revival'/'ONS', 'Instant').
card_original_text('cruel revival'/'ONS', 'Destroy target non-Zombie creature. It can\'t be regenerated. Return up to one target Zombie card from your graveyard to your hand.').
card_first_print('cruel revival', 'ONS').
card_image_name('cruel revival'/'ONS', 'cruel revival').
card_uid('cruel revival'/'ONS', 'ONS:Cruel Revival:cruel revival').
card_rarity('cruel revival'/'ONS', 'Common').
card_artist('cruel revival'/'ONS', 'Greg Staples').
card_number('cruel revival'/'ONS', '135').
card_flavor_text('cruel revival'/'ONS', 'Few Cabal fighters fear death. They fear what follows it.').
card_multiverse_id('cruel revival'/'ONS', '10720').

card_in_set('cryptic gateway', 'ONS').
card_original_type('cryptic gateway'/'ONS', 'Artifact').
card_original_text('cryptic gateway'/'ONS', 'Tap two untapped creatures you control: You may put a creature card from your hand into play that shares a creature type with each creature tapped this way.').
card_first_print('cryptic gateway', 'ONS').
card_image_name('cryptic gateway'/'ONS', 'cryptic gateway').
card_uid('cryptic gateway'/'ONS', 'ONS:Cryptic Gateway:cryptic gateway').
card_rarity('cryptic gateway'/'ONS', 'Rare').
card_artist('cryptic gateway'/'ONS', 'David Martin').
card_number('cryptic gateway'/'ONS', '306').
card_flavor_text('cryptic gateway'/'ONS', 'Its lock changes to fit each key.').
card_multiverse_id('cryptic gateway'/'ONS', '39701').

card_in_set('custody battle', 'ONS').
card_original_type('custody battle'/'ONS', 'Enchant Creature').
card_original_text('custody battle'/'ONS', 'Enchanted creature has \"At the beginning of your upkeep, target opponent gains control of this creature unless you sacrifice a land.\"').
card_first_print('custody battle', 'ONS').
card_image_name('custody battle'/'ONS', 'custody battle').
card_uid('custody battle'/'ONS', 'ONS:Custody Battle:custody battle').
card_rarity('custody battle'/'ONS', 'Uncommon').
card_artist('custody battle'/'ONS', 'Greg & Tim Hildebrandt').
card_number('custody battle'/'ONS', '197').
card_flavor_text('custody battle'/'ONS', 'Goblins resolve disputes by splitting everything straight down the middle.').
card_multiverse_id('custody battle'/'ONS', '39919').

card_in_set('daru cavalier', 'ONS').
card_original_type('daru cavalier'/'ONS', 'Creature — Soldier').
card_original_text('daru cavalier'/'ONS', 'First strike\nWhen Daru Cavalier comes into play, you may search your library for a card named Daru Cavalier, reveal it, and put it into your hand. If you do, shuffle your library.').
card_first_print('daru cavalier', 'ONS').
card_image_name('daru cavalier'/'ONS', 'daru cavalier').
card_uid('daru cavalier'/'ONS', 'ONS:Daru Cavalier:daru cavalier').
card_rarity('daru cavalier'/'ONS', 'Common').
card_artist('daru cavalier'/'ONS', 'Dany Orizio').
card_number('daru cavalier'/'ONS', '18').
card_multiverse_id('daru cavalier'/'ONS', '39523').

card_in_set('daru encampment', 'ONS').
card_original_type('daru encampment'/'ONS', 'Land').
card_original_text('daru encampment'/'ONS', '{T}: Add {1} to your mana pool.\n{W}, {T}: Target Soldier gets +1/+1 until end of turn.').
card_first_print('daru encampment', 'ONS').
card_image_name('daru encampment'/'ONS', 'daru encampment').
card_uid('daru encampment'/'ONS', 'ONS:Daru Encampment:daru encampment').
card_rarity('daru encampment'/'ONS', 'Uncommon').
card_artist('daru encampment'/'ONS', 'Tony Szczudlo').
card_number('daru encampment'/'ONS', '315').
card_multiverse_id('daru encampment'/'ONS', '13210').

card_in_set('daru healer', 'ONS').
card_original_type('daru healer'/'ONS', 'Creature — Cleric').
card_original_text('daru healer'/'ONS', '{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.\nMorph {W} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('daru healer', 'ONS').
card_image_name('daru healer'/'ONS', 'daru healer').
card_uid('daru healer'/'ONS', 'ONS:Daru Healer:daru healer').
card_rarity('daru healer'/'ONS', 'Common').
card_artist('daru healer'/'ONS', 'Dany Orizio').
card_number('daru healer'/'ONS', '19').
card_multiverse_id('daru healer'/'ONS', '39717').

card_in_set('daru lancer', 'ONS').
card_original_type('daru lancer'/'ONS', 'Creature — Soldier').
card_original_text('daru lancer'/'ONS', 'First strike\nMorph {2}{W}{W} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('daru lancer', 'ONS').
card_image_name('daru lancer'/'ONS', 'daru lancer').
card_uid('daru lancer'/'ONS', 'ONS:Daru Lancer:daru lancer').
card_rarity('daru lancer'/'ONS', 'Common').
card_artist('daru lancer'/'ONS', 'Brian Snõddy').
card_number('daru lancer'/'ONS', '20').
card_flavor_text('daru lancer'/'ONS', 'Although the Order frowned upon his preparations for the pits, behind closed doors most saw the fights as a necessary evil.').
card_multiverse_id('daru lancer'/'ONS', '39705').

card_in_set('daunting defender', 'ONS').
card_original_type('daunting defender'/'ONS', 'Creature — Cleric').
card_original_text('daunting defender'/'ONS', 'If a source would deal damage to a Cleric you control, prevent 1 of that damage.').
card_first_print('daunting defender', 'ONS').
card_image_name('daunting defender'/'ONS', 'daunting defender').
card_uid('daunting defender'/'ONS', 'ONS:Daunting Defender:daunting defender').
card_rarity('daunting defender'/'ONS', 'Common').
card_artist('daunting defender'/'ONS', 'Carl Critchlow').
card_number('daunting defender'/'ONS', '21').
card_flavor_text('daunting defender'/'ONS', '\"I will wield the stone of my homeland as though it were the fists of the Ancestor.\"').
card_multiverse_id('daunting defender'/'ONS', '8785').

card_in_set('dawning purist', 'ONS').
card_original_type('dawning purist'/'ONS', 'Creature — Cleric').
card_original_text('dawning purist'/'ONS', 'Whenever Dawning Purist deals combat damage to a player, you may destroy target enchantment that player controls.\nMorph {1}{W} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('dawning purist', 'ONS').
card_image_name('dawning purist'/'ONS', 'dawning purist').
card_uid('dawning purist'/'ONS', 'ONS:Dawning Purist:dawning purist').
card_rarity('dawning purist'/'ONS', 'Uncommon').
card_artist('dawning purist'/'ONS', 'Brian Snõddy').
card_number('dawning purist'/'ONS', '22').
card_multiverse_id('dawning purist'/'ONS', '39711').

card_in_set('death match', 'ONS').
card_original_type('death match'/'ONS', 'Enchantment').
card_original_text('death match'/'ONS', 'Whenever a creature comes into play, that creature\'s controller may have target creature of his or her choice get -3/-3 until end of turn.').
card_first_print('death match', 'ONS').
card_image_name('death match'/'ONS', 'death match').
card_uid('death match'/'ONS', 'ONS:Death Match:death match').
card_rarity('death match'/'ONS', 'Rare').
card_artist('death match'/'ONS', 'rk post').
card_number('death match'/'ONS', '136').
card_flavor_text('death match'/'ONS', 'The Cabal breeds many things for the pit fights, but it never breeds compassion.').
card_multiverse_id('death match'/'ONS', '39556').

card_in_set('death pulse', 'ONS').
card_original_type('death pulse'/'ONS', 'Instant').
card_original_text('death pulse'/'ONS', 'Target creature gets -4/-4 until end of turn.\nCycling {1}{B}{B} ({1}{B}{B}, Discard this card from your hand: Draw a card.)\nWhen you cycle Death Pulse, you may have target creature get -1/-1 until end of turn.').
card_first_print('death pulse', 'ONS').
card_image_name('death pulse'/'ONS', 'death pulse').
card_uid('death pulse'/'ONS', 'ONS:Death Pulse:death pulse').
card_rarity('death pulse'/'ONS', 'Uncommon').
card_artist('death pulse'/'ONS', 'Tony Szczudlo').
card_number('death pulse'/'ONS', '137').
card_multiverse_id('death pulse'/'ONS', '41142').

card_in_set('defensive maneuvers', 'ONS').
card_original_type('defensive maneuvers'/'ONS', 'Instant').
card_original_text('defensive maneuvers'/'ONS', 'Creatures of the type of your choice get +0/+4 until end of turn.').
card_first_print('defensive maneuvers', 'ONS').
card_image_name('defensive maneuvers'/'ONS', 'defensive maneuvers').
card_uid('defensive maneuvers'/'ONS', 'ONS:Defensive Maneuvers:defensive maneuvers').
card_rarity('defensive maneuvers'/'ONS', 'Common').
card_artist('defensive maneuvers'/'ONS', 'Luca Zontini').
card_number('defensive maneuvers'/'ONS', '23').
card_flavor_text('defensive maneuvers'/'ONS', '\"Only on the battlefield can we repay all the Order has given us.\"').
card_multiverse_id('defensive maneuvers'/'ONS', '39822').

card_in_set('demystify', 'ONS').
card_original_type('demystify'/'ONS', 'Instant').
card_original_text('demystify'/'ONS', 'Destroy target enchantment.').
card_first_print('demystify', 'ONS').
card_image_name('demystify'/'ONS', 'demystify').
card_uid('demystify'/'ONS', 'ONS:Demystify:demystify').
card_rarity('demystify'/'ONS', 'Common').
card_artist('demystify'/'ONS', 'Christopher Rush').
card_number('demystify'/'ONS', '24').
card_flavor_text('demystify'/'ONS', '\"The truth will outshine all lies.\"').
card_multiverse_id('demystify'/'ONS', '41152').

card_in_set('dirge of dread', 'ONS').
card_original_type('dirge of dread'/'ONS', 'Sorcery').
card_original_text('dirge of dread'/'ONS', 'All creatures gain fear until end of turn. (They can\'t be blocked except by artifact creatures and/or black creatures.)\nCycling {1}{B} ({1}{B}, Discard this card from your hand: Draw a card.)\nWhen you cycle Dirge of Dread, you may have target creature gain fear until end of turn.').
card_first_print('dirge of dread', 'ONS').
card_image_name('dirge of dread'/'ONS', 'dirge of dread').
card_uid('dirge of dread'/'ONS', 'ONS:Dirge of Dread:dirge of dread').
card_rarity('dirge of dread'/'ONS', 'Common').
card_artist('dirge of dread'/'ONS', 'Heather Hudson').
card_number('dirge of dread'/'ONS', '138').
card_multiverse_id('dirge of dread'/'ONS', '41157').

card_in_set('disciple of grace', 'ONS').
card_original_type('disciple of grace'/'ONS', 'Creature — Cleric').
card_original_text('disciple of grace'/'ONS', 'Protection from black\nCycling {2} ({2}, Discard this card from your hand: Draw a card.)').
card_image_name('disciple of grace'/'ONS', 'disciple of grace').
card_uid('disciple of grace'/'ONS', 'ONS:Disciple of Grace:disciple of grace').
card_rarity('disciple of grace'/'ONS', 'Common').
card_artist('disciple of grace'/'ONS', 'Thomas M. Baxa').
card_number('disciple of grace'/'ONS', '25').
card_flavor_text('disciple of grace'/'ONS', 'Dread was as foreign to her as the landscape ahead.').
card_multiverse_id('disciple of grace'/'ONS', '41133').

card_in_set('disciple of malice', 'ONS').
card_original_type('disciple of malice'/'ONS', 'Creature — Cleric').
card_original_text('disciple of malice'/'ONS', 'Protection from white\nCycling {2} ({2}, Discard this card from your hand: Draw a card.)').
card_first_print('disciple of malice', 'ONS').
card_image_name('disciple of malice'/'ONS', 'disciple of malice').
card_uid('disciple of malice'/'ONS', 'ONS:Disciple of Malice:disciple of malice').
card_rarity('disciple of malice'/'ONS', 'Common').
card_artist('disciple of malice'/'ONS', 'Matt Cavotta').
card_number('disciple of malice'/'ONS', '139').
card_flavor_text('disciple of malice'/'ONS', 'The lantern he carried did not light his way—it signaled his approach.').
card_multiverse_id('disciple of malice'/'ONS', '41134').

card_in_set('discombobulate', 'ONS').
card_original_type('discombobulate'/'ONS', 'Instant').
card_original_text('discombobulate'/'ONS', 'Counter target spell. Look at the top four cards of your library, then put them back in any order.').
card_first_print('discombobulate', 'ONS').
card_image_name('discombobulate'/'ONS', 'discombobulate').
card_uid('discombobulate'/'ONS', 'ONS:Discombobulate:discombobulate').
card_rarity('discombobulate'/'ONS', 'Uncommon').
card_artist('discombobulate'/'ONS', 'Alex Horley-Orlandelli').
card_number('discombobulate'/'ONS', '79').
card_flavor_text('discombobulate'/'ONS', '\"I said ‘pick his brain,\' not ‘tear off his head.\'\"\n—Riptide Project researcher').
card_multiverse_id('discombobulate'/'ONS', '39442').

card_in_set('dispersing orb', 'ONS').
card_original_type('dispersing orb'/'ONS', 'Enchantment').
card_original_text('dispersing orb'/'ONS', '{3}{U}, Sacrifice a permanent: Return target permanent to its owner\'s hand.').
card_first_print('dispersing orb', 'ONS').
card_image_name('dispersing orb'/'ONS', 'dispersing orb').
card_uid('dispersing orb'/'ONS', 'ONS:Dispersing Orb:dispersing orb').
card_rarity('dispersing orb'/'ONS', 'Uncommon').
card_artist('dispersing orb'/'ONS', 'Rebecca Guay').
card_number('dispersing orb'/'ONS', '80').
card_flavor_text('dispersing orb'/'ONS', '\"Like the seas, the Æther is fickle and ever-changing. If we can control one, we can master the other.\"\n—Riptide Project director').
card_multiverse_id('dispersing orb'/'ONS', '40128').

card_in_set('disruptive pitmage', 'ONS').
card_original_type('disruptive pitmage'/'ONS', 'Creature — Wizard').
card_original_text('disruptive pitmage'/'ONS', '{T}: Counter target spell unless its controller pays {1}.\nMorph {U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('disruptive pitmage', 'ONS').
card_image_name('disruptive pitmage'/'ONS', 'disruptive pitmage').
card_uid('disruptive pitmage'/'ONS', 'ONS:Disruptive Pitmage:disruptive pitmage').
card_rarity('disruptive pitmage'/'ONS', 'Common').
card_artist('disruptive pitmage'/'ONS', 'Darrell Riche').
card_number('disruptive pitmage'/'ONS', '81').
card_flavor_text('disruptive pitmage'/'ONS', '\"Show weakness to hide your strength.\"').
card_multiverse_id('disruptive pitmage'/'ONS', '39527').

card_in_set('dive bomber', 'ONS').
card_original_type('dive bomber'/'ONS', 'Creature — Bird Soldier').
card_original_text('dive bomber'/'ONS', 'Flying\n{T}, Sacrifice Dive Bomber: Dive Bomber deals 2 damage to target attacking or blocking creature.').
card_first_print('dive bomber', 'ONS').
card_image_name('dive bomber'/'ONS', 'dive bomber').
card_uid('dive bomber'/'ONS', 'ONS:Dive Bomber:dive bomber').
card_rarity('dive bomber'/'ONS', 'Common').
card_artist('dive bomber'/'ONS', 'Randy Gallegos').
card_number('dive bomber'/'ONS', '26').
card_flavor_text('dive bomber'/'ONS', '\"Your graves will lie beneath my final nest.\"').
card_multiverse_id('dive bomber'/'ONS', '39872').

card_in_set('doom cannon', 'ONS').
card_original_type('doom cannon'/'ONS', 'Artifact').
card_original_text('doom cannon'/'ONS', 'As Doom Cannon comes into play, choose a creature type.\n{3}, {T}, Sacrifice a creature of the chosen type: Doom Cannon deals 3 damage to target creature or player.').
card_first_print('doom cannon', 'ONS').
card_image_name('doom cannon'/'ONS', 'doom cannon').
card_uid('doom cannon'/'ONS', 'ONS:Doom Cannon:doom cannon').
card_rarity('doom cannon'/'ONS', 'Rare').
card_artist('doom cannon'/'ONS', 'Matthew Mitchell').
card_number('doom cannon'/'ONS', '307').
card_multiverse_id('doom cannon'/'ONS', '40197').

card_in_set('doomed necromancer', 'ONS').
card_original_type('doomed necromancer'/'ONS', 'Creature — Cleric Mercenary').
card_original_text('doomed necromancer'/'ONS', '{B}, {T}, Sacrifice Doomed Necromancer: Return target creature card from your graveyard to play.').
card_first_print('doomed necromancer', 'ONS').
card_image_name('doomed necromancer'/'ONS', 'doomed necromancer').
card_uid('doomed necromancer'/'ONS', 'ONS:Doomed Necromancer:doomed necromancer').
card_rarity('doomed necromancer'/'ONS', 'Rare').
card_artist('doomed necromancer'/'ONS', 'Mark Brill').
card_number('doomed necromancer'/'ONS', '140').
card_flavor_text('doomed necromancer'/'ONS', '\"His sacrifice shall not be forgotten. Now toss his body over there with the others.\"\n—Phage the Untouchable').
card_multiverse_id('doomed necromancer'/'ONS', '39549').

card_in_set('doubtless one', 'ONS').
card_original_type('doubtless one'/'ONS', 'Creature — Cleric Avatar').
card_original_text('doubtless one'/'ONS', 'Doubtless One\'s power and toughness are each equal to the number of Clerics in play.\nWhenever Doubtless One deals damage, you gain that much life.').
card_first_print('doubtless one', 'ONS').
card_image_name('doubtless one'/'ONS', 'doubtless one').
card_uid('doubtless one'/'ONS', 'ONS:Doubtless One:doubtless one').
card_rarity('doubtless one'/'ONS', 'Uncommon').
card_artist('doubtless one'/'ONS', 'Justin Sweet').
card_number('doubtless one'/'ONS', '27').
card_flavor_text('doubtless one'/'ONS', '\"Share your devotion with me.\"').
card_multiverse_id('doubtless one'/'ONS', '39743').

card_in_set('dragon roost', 'ONS').
card_original_type('dragon roost'/'ONS', 'Enchantment').
card_original_text('dragon roost'/'ONS', '{5}{R}{R}: Put a 5/5 red Dragon creature token with flying into play.').
card_first_print('dragon roost', 'ONS').
card_image_name('dragon roost'/'ONS', 'dragon roost').
card_uid('dragon roost'/'ONS', 'ONS:Dragon Roost:dragon roost').
card_rarity('dragon roost'/'ONS', 'Rare').
card_artist('dragon roost'/'ONS', 'Luca Zontini').
card_number('dragon roost'/'ONS', '198').
card_flavor_text('dragon roost'/'ONS', 'A powerful mage created the roost to guard a portal between the planes. The mage is long gone, but dragons still keep watch at the gate.').
card_multiverse_id('dragon roost'/'ONS', '39665').

card_in_set('dream chisel', 'ONS').
card_original_type('dream chisel'/'ONS', 'Artifact').
card_original_text('dream chisel'/'ONS', 'Face-down creature spells you play cost {1} less to play.').
card_first_print('dream chisel', 'ONS').
card_image_name('dream chisel'/'ONS', 'dream chisel').
card_uid('dream chisel'/'ONS', 'ONS:Dream Chisel:dream chisel').
card_rarity('dream chisel'/'ONS', 'Rare').
card_artist('dream chisel'/'ONS', 'Ron Spears').
card_number('dream chisel'/'ONS', '308').
card_flavor_text('dream chisel'/'ONS', 'Itself a product of Ixidor\'s tortured psyche, the chisel brings his darkest dreams to life.').
card_multiverse_id('dream chisel'/'ONS', '40196').

card_in_set('dwarven blastminer', 'ONS').
card_original_type('dwarven blastminer'/'ONS', 'Creature — Dwarf').
card_original_text('dwarven blastminer'/'ONS', '{2}{R}, {T}: Destroy target nonbasic land.\nMorph {R} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('dwarven blastminer', 'ONS').
card_image_name('dwarven blastminer'/'ONS', 'dwarven blastminer').
card_uid('dwarven blastminer'/'ONS', 'ONS:Dwarven Blastminer:dwarven blastminer').
card_rarity('dwarven blastminer'/'ONS', 'Uncommon').
card_artist('dwarven blastminer'/'ONS', 'Gary Ruddell').
card_number('dwarven blastminer'/'ONS', '199').
card_flavor_text('dwarven blastminer'/'ONS', '\"Life is too short for something like a hunk of rock to get in my way.\"').
card_multiverse_id('dwarven blastminer'/'ONS', '39869').

card_in_set('ebonblade reaper', 'ONS').
card_original_type('ebonblade reaper'/'ONS', 'Creature — Cleric').
card_original_text('ebonblade reaper'/'ONS', 'Whenever Ebonblade Reaper attacks, you lose half your life, rounded up.\nWhenever Ebonblade Reaper deals combat damage to a player, that player loses half his or her life, rounded up.\nMorph {3}{B}{B} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('ebonblade reaper', 'ONS').
card_image_name('ebonblade reaper'/'ONS', 'ebonblade reaper').
card_uid('ebonblade reaper'/'ONS', 'ONS:Ebonblade Reaper:ebonblade reaper').
card_rarity('ebonblade reaper'/'ONS', 'Rare').
card_artist('ebonblade reaper'/'ONS', 'Wayne England').
card_number('ebonblade reaper'/'ONS', '141').
card_multiverse_id('ebonblade reaper'/'ONS', '39644').

card_in_set('elven riders', 'ONS').
card_original_type('elven riders'/'ONS', 'Creature — Elf').
card_original_text('elven riders'/'ONS', 'Elven Riders can\'t be blocked except by creatures with flying and/or Walls.').
card_image_name('elven riders'/'ONS', 'elven riders').
card_uid('elven riders'/'ONS', 'ONS:Elven Riders:elven riders').
card_rarity('elven riders'/'ONS', 'Uncommon').
card_artist('elven riders'/'ONS', 'Darrell Riche').
card_number('elven riders'/'ONS', '254').
card_flavor_text('elven riders'/'ONS', '\"Wirewood cannot hide great size. Only with speed and skill can we survive here.\"').
card_multiverse_id('elven riders'/'ONS', '40056').

card_in_set('elvish guidance', 'ONS').
card_original_type('elvish guidance'/'ONS', 'Enchant Land').
card_original_text('elvish guidance'/'ONS', 'Whenever enchanted land is tapped for mana, its controller adds {G} to his or her mana pool for each Elf in play.').
card_first_print('elvish guidance', 'ONS').
card_image_name('elvish guidance'/'ONS', 'elvish guidance').
card_uid('elvish guidance'/'ONS', 'ONS:Elvish Guidance:elvish guidance').
card_rarity('elvish guidance'/'ONS', 'Common').
card_artist('elvish guidance'/'ONS', 'Greg & Tim Hildebrandt').
card_number('elvish guidance'/'ONS', '255').
card_flavor_text('elvish guidance'/'ONS', '\"Old home never forgotten, new home ours forever.\"').
card_multiverse_id('elvish guidance'/'ONS', '34763').

card_in_set('elvish pathcutter', 'ONS').
card_original_type('elvish pathcutter'/'ONS', 'Creature — Elf').
card_original_text('elvish pathcutter'/'ONS', '{2}{G}: Target Elf gains forestwalk until end of turn.').
card_first_print('elvish pathcutter', 'ONS').
card_image_name('elvish pathcutter'/'ONS', 'elvish pathcutter').
card_uid('elvish pathcutter'/'ONS', 'ONS:Elvish Pathcutter:elvish pathcutter').
card_rarity('elvish pathcutter'/'ONS', 'Common').
card_artist('elvish pathcutter'/'ONS', 'Todd Lockwood').
card_number('elvish pathcutter'/'ONS', '256').
card_flavor_text('elvish pathcutter'/'ONS', 'In harsh times, the strongest currency is cooperation.').
card_multiverse_id('elvish pathcutter'/'ONS', '39494').

card_in_set('elvish pioneer', 'ONS').
card_original_type('elvish pioneer'/'ONS', 'Creature — Elf Druid').
card_original_text('elvish pioneer'/'ONS', 'When Elvish Pioneer comes into play, you may put a basic land card from your hand into play tapped.').
card_first_print('elvish pioneer', 'ONS').
card_image_name('elvish pioneer'/'ONS', 'elvish pioneer').
card_uid('elvish pioneer'/'ONS', 'ONS:Elvish Pioneer:elvish pioneer').
card_rarity('elvish pioneer'/'ONS', 'Common').
card_artist('elvish pioneer'/'ONS', 'Christopher Rush').
card_number('elvish pioneer'/'ONS', '257').
card_flavor_text('elvish pioneer'/'ONS', '\"Destruction is the work of an afternoon. Creation is the work of a lifetime.\"\n—Kamahl, druid acolyte').
card_multiverse_id('elvish pioneer'/'ONS', '12463').

card_in_set('elvish scrapper', 'ONS').
card_original_type('elvish scrapper'/'ONS', 'Creature — Elf').
card_original_text('elvish scrapper'/'ONS', '{G}, {T}, Sacrifice Elvish Scrapper: Destroy target artifact.').
card_first_print('elvish scrapper', 'ONS').
card_image_name('elvish scrapper'/'ONS', 'elvish scrapper').
card_uid('elvish scrapper'/'ONS', 'ONS:Elvish Scrapper:elvish scrapper').
card_rarity('elvish scrapper'/'ONS', 'Uncommon').
card_artist('elvish scrapper'/'ONS', 'Edward P. Beard, Jr.').
card_number('elvish scrapper'/'ONS', '258').
card_flavor_text('elvish scrapper'/'ONS', '\"The stories tell of a distant time when machines overran the forests, destroying everything that lived. That time will not come again.\"').
card_multiverse_id('elvish scrapper'/'ONS', '13778').

card_in_set('elvish vanguard', 'ONS').
card_original_type('elvish vanguard'/'ONS', 'Creature — Elf').
card_original_text('elvish vanguard'/'ONS', 'Whenever another Elf comes into play, put a +1/+1 counter on Elvish Vanguard.').
card_first_print('elvish vanguard', 'ONS').
card_image_name('elvish vanguard'/'ONS', 'elvish vanguard').
card_uid('elvish vanguard'/'ONS', 'ONS:Elvish Vanguard:elvish vanguard').
card_rarity('elvish vanguard'/'ONS', 'Rare').
card_artist('elvish vanguard'/'ONS', 'Glen Angus').
card_number('elvish vanguard'/'ONS', '259').
card_flavor_text('elvish vanguard'/'ONS', '\"Our lives are woven together like the trees\' branches over our heads, forming a canopy that protects us all.\"').
card_multiverse_id('elvish vanguard'/'ONS', '5656').

card_in_set('elvish warrior', 'ONS').
card_original_type('elvish warrior'/'ONS', 'Creature — Elf').
card_original_text('elvish warrior'/'ONS', '').
card_first_print('elvish warrior', 'ONS').
card_image_name('elvish warrior'/'ONS', 'elvish warrior').
card_uid('elvish warrior'/'ONS', 'ONS:Elvish Warrior:elvish warrior').
card_rarity('elvish warrior'/'ONS', 'Common').
card_artist('elvish warrior'/'ONS', 'Christopher Moeller').
card_number('elvish warrior'/'ONS', '260').
card_flavor_text('elvish warrior'/'ONS', '\"My tales of war are the stories most asked for around the fires at night, but they\'re the ones I care least to tell.\"').
card_multiverse_id('elvish warrior'/'ONS', '39927').

card_in_set('embermage goblin', 'ONS').
card_original_type('embermage goblin'/'ONS', 'Creature — Goblin Wizard').
card_original_text('embermage goblin'/'ONS', 'When Embermage Goblin comes into play, you may search your library for a card named Embermage Goblin, reveal it, and put it into your hand. If you do, shuffle your library.\n{T}: Embermage Goblin deals 1 damage to target creature or player.').
card_first_print('embermage goblin', 'ONS').
card_image_name('embermage goblin'/'ONS', 'embermage goblin').
card_uid('embermage goblin'/'ONS', 'ONS:Embermage Goblin:embermage goblin').
card_rarity('embermage goblin'/'ONS', 'Uncommon').
card_artist('embermage goblin'/'ONS', 'Pete Venters').
card_number('embermage goblin'/'ONS', '200').
card_multiverse_id('embermage goblin'/'ONS', '39589').

card_in_set('enchantress\'s presence', 'ONS').
card_original_type('enchantress\'s presence'/'ONS', 'Enchantment').
card_original_text('enchantress\'s presence'/'ONS', 'Whenever you play an enchantment spell, draw a card.').
card_first_print('enchantress\'s presence', 'ONS').
card_image_name('enchantress\'s presence'/'ONS', 'enchantress\'s presence').
card_uid('enchantress\'s presence'/'ONS', 'ONS:Enchantress\'s Presence:enchantress\'s presence').
card_rarity('enchantress\'s presence'/'ONS', 'Rare').
card_artist('enchantress\'s presence'/'ONS', 'Rebecca Guay').
card_number('enchantress\'s presence'/'ONS', '261').
card_flavor_text('enchantress\'s presence'/'ONS', '\"The wise learn from successes as well as mistakes.\"').
card_multiverse_id('enchantress\'s presence'/'ONS', '35514').

card_in_set('endemic plague', 'ONS').
card_original_type('endemic plague'/'ONS', 'Sorcery').
card_original_text('endemic plague'/'ONS', 'As an additional cost to play Endemic Plague, sacrifice a creature.\nDestroy all creatures that share a creature type with the sacrificed creature. They can\'t be regenerated.').
card_first_print('endemic plague', 'ONS').
card_image_name('endemic plague'/'ONS', 'endemic plague').
card_uid('endemic plague'/'ONS', 'ONS:Endemic Plague:endemic plague').
card_rarity('endemic plague'/'ONS', 'Rare').
card_artist('endemic plague'/'ONS', 'Nelson DeCastro').
card_number('endemic plague'/'ONS', '142').
card_multiverse_id('endemic plague'/'ONS', '39612').

card_in_set('entrails feaster', 'ONS').
card_original_type('entrails feaster'/'ONS', 'Creature — Zombie Cat').
card_original_text('entrails feaster'/'ONS', 'At the beginning of your upkeep, you may remove a creature card in a graveyard from the game. If you do, put a +1/+1 counter on Entrails Feaster. If you don\'t, tap Entrails Feaster.').
card_first_print('entrails feaster', 'ONS').
card_image_name('entrails feaster'/'ONS', 'entrails feaster').
card_uid('entrails feaster'/'ONS', 'ONS:Entrails Feaster:entrails feaster').
card_rarity('entrails feaster'/'ONS', 'Rare').
card_artist('entrails feaster'/'ONS', 'John Matson').
card_number('entrails feaster'/'ONS', '143').
card_multiverse_id('entrails feaster'/'ONS', '10699').

card_in_set('erratic explosion', 'ONS').
card_original_type('erratic explosion'/'ONS', 'Sorcery').
card_original_text('erratic explosion'/'ONS', 'Choose target creature or player. Reveal cards from the top of your library until you reveal a nonland card. Erratic Explosion deals damage equal to that card\'s converted mana cost to that creature or player. Put the revealed cards on the bottom of your library in any order.').
card_first_print('erratic explosion', 'ONS').
card_image_name('erratic explosion'/'ONS', 'erratic explosion').
card_uid('erratic explosion'/'ONS', 'ONS:Erratic Explosion:erratic explosion').
card_rarity('erratic explosion'/'ONS', 'Common').
card_artist('erratic explosion'/'ONS', 'Gary Ruddell').
card_number('erratic explosion'/'ONS', '201').
card_multiverse_id('erratic explosion'/'ONS', '12484').

card_in_set('essence fracture', 'ONS').
card_original_type('essence fracture'/'ONS', 'Sorcery').
card_original_text('essence fracture'/'ONS', 'Return two target creatures to their owners\' hands.\nCycling {2}{U} ({2}{U}, Discard this card from your hand: Draw a card.)').
card_first_print('essence fracture', 'ONS').
card_image_name('essence fracture'/'ONS', 'essence fracture').
card_uid('essence fracture'/'ONS', 'ONS:Essence Fracture:essence fracture').
card_rarity('essence fracture'/'ONS', 'Uncommon').
card_artist('essence fracture'/'ONS', 'Wayne England').
card_number('essence fracture'/'ONS', '82').
card_flavor_text('essence fracture'/'ONS', '\"Shaping reality is simply a matter of knowing where to apply pressure.\"\n—Ixidor, reality sculptor').
card_multiverse_id('essence fracture'/'ONS', '41135').

card_in_set('everglove courier', 'ONS').
card_original_type('everglove courier'/'ONS', 'Creature — Elf').
card_original_text('everglove courier'/'ONS', 'You may choose not to untap Everglove Courier during your untap step.\n{2}{G}, {T}: As long as Everglove Courier remains tapped, target Elf gets +2/+2 and has trample.').
card_first_print('everglove courier', 'ONS').
card_image_name('everglove courier'/'ONS', 'everglove courier').
card_uid('everglove courier'/'ONS', 'ONS:Everglove Courier:everglove courier').
card_rarity('everglove courier'/'ONS', 'Uncommon').
card_artist('everglove courier'/'ONS', 'Darrell Riche').
card_number('everglove courier'/'ONS', '262').
card_multiverse_id('everglove courier'/'ONS', '39593').

card_in_set('exalted angel', 'ONS').
card_original_type('exalted angel'/'ONS', 'Creature — Angel').
card_original_text('exalted angel'/'ONS', 'Flying\nWhenever Exalted Angel deals damage, you gain that much life.\nMorph {2}{W}{W} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_image_name('exalted angel'/'ONS', 'exalted angel').
card_uid('exalted angel'/'ONS', 'ONS:Exalted Angel:exalted angel').
card_rarity('exalted angel'/'ONS', 'Rare').
card_artist('exalted angel'/'ONS', 'Michael Sutfin').
card_number('exalted angel'/'ONS', '28').
card_multiverse_id('exalted angel'/'ONS', '39718').

card_in_set('explosive vegetation', 'ONS').
card_original_type('explosive vegetation'/'ONS', 'Sorcery').
card_original_text('explosive vegetation'/'ONS', 'Search your library for up to two basic land cards and put them into play tapped. Then shuffle your library.').
card_first_print('explosive vegetation', 'ONS').
card_image_name('explosive vegetation'/'ONS', 'explosive vegetation').
card_uid('explosive vegetation'/'ONS', 'ONS:Explosive Vegetation:explosive vegetation').
card_rarity('explosive vegetation'/'ONS', 'Uncommon').
card_artist('explosive vegetation'/'ONS', 'John Avon').
card_number('explosive vegetation'/'ONS', '263').
card_flavor_text('explosive vegetation'/'ONS', 'Torching Krosa would be pointless. It grows faster than it burns.').
card_multiverse_id('explosive vegetation'/'ONS', '41172').

card_in_set('fade from memory', 'ONS').
card_original_type('fade from memory'/'ONS', 'Instant').
card_original_text('fade from memory'/'ONS', 'Remove target card in a graveyard from the game.\nCycling {B} ({B}, Discard this card from your hand: Draw a card.)').
card_first_print('fade from memory', 'ONS').
card_image_name('fade from memory'/'ONS', 'fade from memory').
card_uid('fade from memory'/'ONS', 'ONS:Fade from Memory:fade from memory').
card_rarity('fade from memory'/'ONS', 'Uncommon').
card_artist('fade from memory'/'ONS', 'David Martin').
card_number('fade from memory'/'ONS', '144').
card_flavor_text('fade from memory'/'ONS', 'Proper burial is a luxury Otarians can no longer afford.').
card_multiverse_id('fade from memory'/'ONS', '41165').

card_in_set('fallen cleric', 'ONS').
card_original_type('fallen cleric'/'ONS', 'Creature — Zombie Cleric').
card_original_text('fallen cleric'/'ONS', 'Protection from Clerics\nMorph {4}{B} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('fallen cleric', 'ONS').
card_image_name('fallen cleric'/'ONS', 'fallen cleric').
card_uid('fallen cleric'/'ONS', 'ONS:Fallen Cleric:fallen cleric').
card_rarity('fallen cleric'/'ONS', 'Common').
card_artist('fallen cleric'/'ONS', 'Dave Dorman').
card_number('fallen cleric'/'ONS', '145').
card_flavor_text('fallen cleric'/'ONS', 'The most terrifying zombies are those with just a hint of their old selves left.').
card_multiverse_id('fallen cleric'/'ONS', '39849').

card_in_set('false cure', 'ONS').
card_original_type('false cure'/'ONS', 'Instant').
card_original_text('false cure'/'ONS', 'Until end of turn, whenever a player gains life, that player loses 2 life for each 1 life he or she gained.').
card_first_print('false cure', 'ONS').
card_image_name('false cure'/'ONS', 'false cure').
card_uid('false cure'/'ONS', 'ONS:False Cure:false cure').
card_rarity('false cure'/'ONS', 'Rare').
card_artist('false cure'/'ONS', 'Bradley Williams').
card_number('false cure'/'ONS', '146').
card_flavor_text('false cure'/'ONS', '\"I do unto others as others have done unto me.\"\n—Phage the Untouchable').
card_multiverse_id('false cure'/'ONS', '26436').

card_in_set('feeding frenzy', 'ONS').
card_original_type('feeding frenzy'/'ONS', 'Instant').
card_original_text('feeding frenzy'/'ONS', 'Target creature gets -X/-X until end of turn, where X is the number of Zombies in play.').
card_first_print('feeding frenzy', 'ONS').
card_image_name('feeding frenzy'/'ONS', 'feeding frenzy').
card_uid('feeding frenzy'/'ONS', 'ONS:Feeding Frenzy:feeding frenzy').
card_rarity('feeding frenzy'/'ONS', 'Uncommon').
card_artist('feeding frenzy'/'ONS', 'Nelson DeCastro').
card_number('feeding frenzy'/'ONS', '147').
card_flavor_text('feeding frenzy'/'ONS', 'It wasn\'t as much a strategy as a dim instinct to drown their prey.').
card_multiverse_id('feeding frenzy'/'ONS', '41169').

card_in_set('festering goblin', 'ONS').
card_original_type('festering goblin'/'ONS', 'Creature — Zombie Goblin').
card_original_text('festering goblin'/'ONS', 'When Festering Goblin is put into a graveyard from play, target creature gets -1/-1 until end of turn.').
card_first_print('festering goblin', 'ONS').
card_image_name('festering goblin'/'ONS', 'festering goblin').
card_uid('festering goblin'/'ONS', 'ONS:Festering Goblin:festering goblin').
card_rarity('festering goblin'/'ONS', 'Common').
card_artist('festering goblin'/'ONS', 'Thomas M. Baxa').
card_number('festering goblin'/'ONS', '148').
card_flavor_text('festering goblin'/'ONS', 'In life, it was a fetid, disease-ridden thing. In death, not much changed.').
card_multiverse_id('festering goblin'/'ONS', '39905').

card_in_set('fever charm', 'ONS').
card_original_type('fever charm'/'ONS', 'Instant').
card_original_text('fever charm'/'ONS', 'Choose one Target creature gains haste until end of turn; or target creature gets +2/+0 until end of turn; or Fever Charm deals 3 damage to target Wizard.').
card_first_print('fever charm', 'ONS').
card_image_name('fever charm'/'ONS', 'fever charm').
card_uid('fever charm'/'ONS', 'ONS:Fever Charm:fever charm').
card_rarity('fever charm'/'ONS', 'Common').
card_artist('fever charm'/'ONS', 'David Martin').
card_number('fever charm'/'ONS', '202').
card_multiverse_id('fever charm'/'ONS', '39484').

card_in_set('flamestick courier', 'ONS').
card_original_type('flamestick courier'/'ONS', 'Creature — Goblin').
card_original_text('flamestick courier'/'ONS', 'You may choose not to untap Flamestick Courier during your untap step.\n{2}{R}, {T}: As long as Flamestick Courier remains tapped, target Goblin gets +2/+2 and has haste.').
card_first_print('flamestick courier', 'ONS').
card_image_name('flamestick courier'/'ONS', 'flamestick courier').
card_uid('flamestick courier'/'ONS', 'ONS:Flamestick Courier:flamestick courier').
card_rarity('flamestick courier'/'ONS', 'Uncommon').
card_artist('flamestick courier'/'ONS', 'Luca Zontini').
card_number('flamestick courier'/'ONS', '203').
card_multiverse_id('flamestick courier'/'ONS', '39592').

card_in_set('fleeting aven', 'ONS').
card_original_type('fleeting aven'/'ONS', 'Creature — Bird Wizard').
card_original_text('fleeting aven'/'ONS', 'Flying\nWhenever a player cycles a card, return Fleeting Aven to its owner\'s hand.').
card_first_print('fleeting aven', 'ONS').
card_image_name('fleeting aven'/'ONS', 'fleeting aven').
card_uid('fleeting aven'/'ONS', 'ONS:Fleeting Aven:fleeting aven').
card_rarity('fleeting aven'/'ONS', 'Uncommon').
card_artist('fleeting aven'/'ONS', 'Iain McCaig').
card_number('fleeting aven'/'ONS', '83').
card_flavor_text('fleeting aven'/'ONS', '\"Don\'t become so enthralled with magic that you forget you can fly without it.\"').
card_multiverse_id('fleeting aven'/'ONS', '41149').

card_in_set('flooded strand', 'ONS').
card_original_type('flooded strand'/'ONS', 'Land').
card_original_text('flooded strand'/'ONS', '{T}, Pay 1 life, Sacrifice Flooded Strand: Search your library for a plains or island card and put it into play. Then shuffle your library.').
card_image_name('flooded strand'/'ONS', 'flooded strand').
card_uid('flooded strand'/'ONS', 'ONS:Flooded Strand:flooded strand').
card_rarity('flooded strand'/'ONS', 'Rare').
card_artist('flooded strand'/'ONS', 'Rob Alexander').
card_number('flooded strand'/'ONS', '316').
card_multiverse_id('flooded strand'/'ONS', '39503').

card_in_set('foothill guide', 'ONS').
card_original_type('foothill guide'/'ONS', 'Creature — Cleric').
card_original_text('foothill guide'/'ONS', 'Protection from Goblins\nMorph {W} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('foothill guide', 'ONS').
card_image_name('foothill guide'/'ONS', 'foothill guide').
card_uid('foothill guide'/'ONS', 'ONS:Foothill Guide:foothill guide').
card_rarity('foothill guide'/'ONS', 'Common').
card_artist('foothill guide'/'ONS', 'Eric Peterson').
card_number('foothill guide'/'ONS', '29').
card_flavor_text('foothill guide'/'ONS', '\"Like any pests, goblins must be repelled quickly or they multiply.\"').
card_multiverse_id('foothill guide'/'ONS', '34410').

card_in_set('forest', 'ONS').
card_original_type('forest'/'ONS', 'Land').
card_original_text('forest'/'ONS', 'G').
card_image_name('forest'/'ONS', 'forest1').
card_uid('forest'/'ONS', 'ONS:Forest:forest1').
card_rarity('forest'/'ONS', 'Basic Land').
card_artist('forest'/'ONS', 'John Avon').
card_number('forest'/'ONS', '347').
card_multiverse_id('forest'/'ONS', '40116').

card_in_set('forest', 'ONS').
card_original_type('forest'/'ONS', 'Land').
card_original_text('forest'/'ONS', 'G').
card_image_name('forest'/'ONS', 'forest2').
card_uid('forest'/'ONS', 'ONS:Forest:forest2').
card_rarity('forest'/'ONS', 'Basic Land').
card_artist('forest'/'ONS', 'John Matson').
card_number('forest'/'ONS', '348').
card_multiverse_id('forest'/'ONS', '40115').

card_in_set('forest', 'ONS').
card_original_type('forest'/'ONS', 'Land').
card_original_text('forest'/'ONS', 'G').
card_image_name('forest'/'ONS', 'forest3').
card_uid('forest'/'ONS', 'ONS:Forest:forest3').
card_rarity('forest'/'ONS', 'Basic Land').
card_artist('forest'/'ONS', 'John Avon').
card_number('forest'/'ONS', '349').
card_multiverse_id('forest'/'ONS', '40106').

card_in_set('forest', 'ONS').
card_original_type('forest'/'ONS', 'Land').
card_original_text('forest'/'ONS', 'G').
card_image_name('forest'/'ONS', 'forest4').
card_uid('forest'/'ONS', 'ONS:Forest:forest4').
card_rarity('forest'/'ONS', 'Basic Land').
card_artist('forest'/'ONS', 'David Martin').
card_number('forest'/'ONS', '350').
card_multiverse_id('forest'/'ONS', '40105').

card_in_set('forgotten cave', 'ONS').
card_original_type('forgotten cave'/'ONS', 'Land').
card_original_text('forgotten cave'/'ONS', 'Forgotten Cave comes into play tapped.\n{T}: Add {R} to your mana pool.\nCycling {R} ({R}, Discard this card from your hand: Draw a card.)').
card_first_print('forgotten cave', 'ONS').
card_image_name('forgotten cave'/'ONS', 'forgotten cave').
card_uid('forgotten cave'/'ONS', 'ONS:Forgotten Cave:forgotten cave').
card_rarity('forgotten cave'/'ONS', 'Common').
card_artist('forgotten cave'/'ONS', 'Tony Szczudlo').
card_number('forgotten cave'/'ONS', '317').
card_multiverse_id('forgotten cave'/'ONS', '41140').

card_in_set('frightshroud courier', 'ONS').
card_original_type('frightshroud courier'/'ONS', 'Creature — Zombie').
card_original_text('frightshroud courier'/'ONS', 'You may choose not to untap Frightshroud Courier during your untap step.\n{2}{B}, {T}: As long as Frightshroud Courier remains tapped, target Zombie gets +2/+2 and has fear. (It can\'t be blocked except by artifact creatures and/or black creatures.)').
card_first_print('frightshroud courier', 'ONS').
card_image_name('frightshroud courier'/'ONS', 'frightshroud courier').
card_uid('frightshroud courier'/'ONS', 'ONS:Frightshroud Courier:frightshroud courier').
card_rarity('frightshroud courier'/'ONS', 'Uncommon').
card_artist('frightshroud courier'/'ONS', 'Ron Spears').
card_number('frightshroud courier'/'ONS', '149').
card_multiverse_id('frightshroud courier'/'ONS', '39594').

card_in_set('future sight', 'ONS').
card_original_type('future sight'/'ONS', 'Enchantment').
card_original_text('future sight'/'ONS', 'Play with the top card of your library revealed.\nYou may play the top card of your library as though it were in your hand.').
card_first_print('future sight', 'ONS').
card_image_name('future sight'/'ONS', 'future sight').
card_uid('future sight'/'ONS', 'ONS:Future Sight:future sight').
card_rarity('future sight'/'ONS', 'Rare').
card_artist('future sight'/'ONS', 'Matt Cavotta').
card_number('future sight'/'ONS', '84').
card_flavor_text('future sight'/'ONS', '\"My past holds only pain and loss. I will conquer it by creating the perfect future.\"\n—Ixidor, reality sculptor').
card_multiverse_id('future sight'/'ONS', '39628').

card_in_set('gangrenous goliath', 'ONS').
card_original_type('gangrenous goliath'/'ONS', 'Creature — Zombie Giant').
card_original_text('gangrenous goliath'/'ONS', 'Tap three untapped Clerics you control: Return Gangrenous Goliath from your graveyard to your hand.').
card_first_print('gangrenous goliath', 'ONS').
card_image_name('gangrenous goliath'/'ONS', 'gangrenous goliath').
card_uid('gangrenous goliath'/'ONS', 'ONS:Gangrenous Goliath:gangrenous goliath').
card_rarity('gangrenous goliath'/'ONS', 'Rare').
card_artist('gangrenous goliath'/'ONS', 'Justin Sweet').
card_number('gangrenous goliath'/'ONS', '150').
card_flavor_text('gangrenous goliath'/'ONS', 'Cabal clerics don\'t heal the sick—they heal the dead.').
card_multiverse_id('gangrenous goliath'/'ONS', '39610').

card_in_set('ghosthelm courier', 'ONS').
card_original_type('ghosthelm courier'/'ONS', 'Creature — Wizard').
card_original_text('ghosthelm courier'/'ONS', 'You may choose not to untap Ghosthelm Courier during your untap step.\n{2}{U}, {T}: As long as Ghosthelm Courier remains tapped, target Wizard gets +2/+2 and can\'t be the target of spells or abilities.').
card_first_print('ghosthelm courier', 'ONS').
card_image_name('ghosthelm courier'/'ONS', 'ghosthelm courier').
card_uid('ghosthelm courier'/'ONS', 'ONS:Ghosthelm Courier:ghosthelm courier').
card_rarity('ghosthelm courier'/'ONS', 'Uncommon').
card_artist('ghosthelm courier'/'ONS', 'Edward P. Beard, Jr.').
card_number('ghosthelm courier'/'ONS', '85').
card_multiverse_id('ghosthelm courier'/'ONS', '39595').

card_in_set('gigapede', 'ONS').
card_original_type('gigapede'/'ONS', 'Creature — Insect').
card_original_text('gigapede'/'ONS', 'Gigapede can\'t be the target of spells or abilities.\nAt the beginning of your upkeep, if Gigapede is in your graveyard, you may discard a card from your hand. If you do, return Gigapede to your hand.').
card_first_print('gigapede', 'ONS').
card_image_name('gigapede'/'ONS', 'gigapede').
card_uid('gigapede'/'ONS', 'ONS:Gigapede:gigapede').
card_rarity('gigapede'/'ONS', 'Rare').
card_artist('gigapede'/'ONS', 'Glen Angus').
card_number('gigapede'/'ONS', '264').
card_multiverse_id('gigapede'/'ONS', '39578').

card_in_set('glarecaster', 'ONS').
card_original_type('glarecaster'/'ONS', 'Creature — Bird Cleric').
card_original_text('glarecaster'/'ONS', 'Flying\n{5}{W}: The next time damage would be dealt to Glarecaster or you this turn, that damage is dealt to target creature or player instead.').
card_first_print('glarecaster', 'ONS').
card_image_name('glarecaster'/'ONS', 'glarecaster').
card_uid('glarecaster'/'ONS', 'ONS:Glarecaster:glarecaster').
card_rarity('glarecaster'/'ONS', 'Rare').
card_artist('glarecaster'/'ONS', 'Dan Frazier').
card_number('glarecaster'/'ONS', '30').
card_multiverse_id('glarecaster'/'ONS', '39423').

card_in_set('glory seeker', 'ONS').
card_original_type('glory seeker'/'ONS', 'Creature — Soldier').
card_original_text('glory seeker'/'ONS', '').
card_first_print('glory seeker', 'ONS').
card_image_name('glory seeker'/'ONS', 'glory seeker').
card_uid('glory seeker'/'ONS', 'ONS:Glory Seeker:glory seeker').
card_rarity('glory seeker'/'ONS', 'Common').
card_artist('glory seeker'/'ONS', 'Dave Dorman').
card_number('glory seeker'/'ONS', '31').
card_flavor_text('glory seeker'/'ONS', 'The turning of the tide always begins with one soldier\'s decision to head back into the fray.').
card_multiverse_id('glory seeker'/'ONS', '39712').

card_in_set('gluttonous zombie', 'ONS').
card_original_type('gluttonous zombie'/'ONS', 'Creature — Zombie').
card_original_text('gluttonous zombie'/'ONS', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)').
card_first_print('gluttonous zombie', 'ONS').
card_image_name('gluttonous zombie'/'ONS', 'gluttonous zombie').
card_uid('gluttonous zombie'/'ONS', 'ONS:Gluttonous Zombie:gluttonous zombie').
card_rarity('gluttonous zombie'/'ONS', 'Uncommon').
card_artist('gluttonous zombie'/'ONS', 'Thomas M. Baxa').
card_number('gluttonous zombie'/'ONS', '151').
card_flavor_text('gluttonous zombie'/'ONS', 'Greed and gluttony led him to death. Now they are his greatest assets.').
card_multiverse_id('gluttonous zombie'/'ONS', '33713').

card_in_set('goblin burrows', 'ONS').
card_original_type('goblin burrows'/'ONS', 'Land').
card_original_text('goblin burrows'/'ONS', '{T}: Add {1} to your mana pool.\n{1}{R}, {T}: Target Goblin gets +2/+0 until end of turn.').
card_first_print('goblin burrows', 'ONS').
card_image_name('goblin burrows'/'ONS', 'goblin burrows').
card_uid('goblin burrows'/'ONS', 'ONS:Goblin Burrows:goblin burrows').
card_rarity('goblin burrows'/'ONS', 'Uncommon').
card_artist('goblin burrows'/'ONS', 'David Martin').
card_number('goblin burrows'/'ONS', '318').
card_multiverse_id('goblin burrows'/'ONS', '13012').

card_in_set('goblin machinist', 'ONS').
card_original_type('goblin machinist'/'ONS', 'Creature — Goblin').
card_original_text('goblin machinist'/'ONS', '{2}{R}: Reveal cards from the top of your library until you reveal a nonland card. Goblin Machinist gets +X/+0 until end of turn, where X is that card\'s converted mana cost. Put the revealed cards on the bottom of your library in any order.').
card_first_print('goblin machinist', 'ONS').
card_image_name('goblin machinist'/'ONS', 'goblin machinist').
card_uid('goblin machinist'/'ONS', 'ONS:Goblin Machinist:goblin machinist').
card_rarity('goblin machinist'/'ONS', 'Uncommon').
card_artist('goblin machinist'/'ONS', 'Doug Chaffee').
card_number('goblin machinist'/'ONS', '204').
card_multiverse_id('goblin machinist'/'ONS', '39942').

card_in_set('goblin piledriver', 'ONS').
card_original_type('goblin piledriver'/'ONS', 'Creature — Goblin').
card_original_text('goblin piledriver'/'ONS', 'Protection from blue\nWhenever Goblin Piledriver attacks, it gets +2/+0 until end of turn for each other attacking Goblin.').
card_image_name('goblin piledriver'/'ONS', 'goblin piledriver').
card_uid('goblin piledriver'/'ONS', 'ONS:Goblin Piledriver:goblin piledriver').
card_rarity('goblin piledriver'/'ONS', 'Rare').
card_artist('goblin piledriver'/'ONS', 'Matt Cavotta').
card_number('goblin piledriver'/'ONS', '205').
card_flavor_text('goblin piledriver'/'ONS', 'Throw enough goblins at any problem and it should go away. At the very least, there\'ll be fewer goblins.').
card_multiverse_id('goblin piledriver'/'ONS', '40193').

card_in_set('goblin pyromancer', 'ONS').
card_original_type('goblin pyromancer'/'ONS', 'Creature — Goblin Wizard').
card_original_text('goblin pyromancer'/'ONS', 'When Goblin Pyromancer comes into play, all Goblins get +3/+0 until end of turn.\nAt end of turn, destroy all Goblins.').
card_first_print('goblin pyromancer', 'ONS').
card_image_name('goblin pyromancer'/'ONS', 'goblin pyromancer').
card_uid('goblin pyromancer'/'ONS', 'ONS:Goblin Pyromancer:goblin pyromancer').
card_rarity('goblin pyromancer'/'ONS', 'Rare').
card_artist('goblin pyromancer'/'ONS', 'Edward P. Beard, Jr.').
card_number('goblin pyromancer'/'ONS', '206').
card_flavor_text('goblin pyromancer'/'ONS', '\"The good news is, we figured out how the wand works. The bad news is, we figured out how the wand works.\"').
card_multiverse_id('goblin pyromancer'/'ONS', '39840').

card_in_set('goblin sharpshooter', 'ONS').
card_original_type('goblin sharpshooter'/'ONS', 'Creature — Goblin').
card_original_text('goblin sharpshooter'/'ONS', 'Goblin Sharpshooter doesn\'t untap during your untap step.\nWhenever a creature is put into a graveyard from play, untap Goblin Sharpshooter.\n{T}: Goblin Sharpshooter deals 1 damage to target creature or player.').
card_first_print('goblin sharpshooter', 'ONS').
card_image_name('goblin sharpshooter'/'ONS', 'goblin sharpshooter').
card_uid('goblin sharpshooter'/'ONS', 'ONS:Goblin Sharpshooter:goblin sharpshooter').
card_rarity('goblin sharpshooter'/'ONS', 'Rare').
card_artist('goblin sharpshooter'/'ONS', 'Greg Staples').
card_number('goblin sharpshooter'/'ONS', '207').
card_multiverse_id('goblin sharpshooter'/'ONS', '33695').

card_in_set('goblin sky raider', 'ONS').
card_original_type('goblin sky raider'/'ONS', 'Creature — Goblin').
card_original_text('goblin sky raider'/'ONS', 'Flying').
card_first_print('goblin sky raider', 'ONS').
card_image_name('goblin sky raider'/'ONS', 'goblin sky raider').
card_uid('goblin sky raider'/'ONS', 'ONS:Goblin Sky Raider:goblin sky raider').
card_rarity('goblin sky raider'/'ONS', 'Common').
card_artist('goblin sky raider'/'ONS', 'Daren Bader').
card_number('goblin sky raider'/'ONS', '208').
card_flavor_text('goblin sky raider'/'ONS', 'The goblin word for \"flying\" is more accurately translated as \"falling slowly.\"').
card_multiverse_id('goblin sky raider'/'ONS', '39477').

card_in_set('goblin sledder', 'ONS').
card_original_type('goblin sledder'/'ONS', 'Creature — Goblin').
card_original_text('goblin sledder'/'ONS', 'Sacrifice a Goblin: Target creature gets +1/+1 until end of turn.').
card_first_print('goblin sledder', 'ONS').
card_image_name('goblin sledder'/'ONS', 'goblin sledder').
card_uid('goblin sledder'/'ONS', 'ONS:Goblin Sledder:goblin sledder').
card_rarity('goblin sledder'/'ONS', 'Common').
card_artist('goblin sledder'/'ONS', 'Ron Spencer').
card_number('goblin sledder'/'ONS', '209').
card_flavor_text('goblin sledder'/'ONS', '\"Let\'s play ‘sled.\' Here\'s how it works: you\'re the sled.\"').
card_multiverse_id('goblin sledder'/'ONS', '39826').

card_in_set('goblin taskmaster', 'ONS').
card_original_type('goblin taskmaster'/'ONS', 'Creature — Goblin').
card_original_text('goblin taskmaster'/'ONS', '{1}{R}: Target Goblin gets +1/+0 until end of turn.\nMorph {R} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('goblin taskmaster', 'ONS').
card_image_name('goblin taskmaster'/'ONS', 'goblin taskmaster').
card_uid('goblin taskmaster'/'ONS', 'ONS:Goblin Taskmaster:goblin taskmaster').
card_rarity('goblin taskmaster'/'ONS', 'Common').
card_artist('goblin taskmaster'/'ONS', 'Trevor Hairsine').
card_number('goblin taskmaster'/'ONS', '210').
card_flavor_text('goblin taskmaster'/'ONS', 'For some reason, goblin fighting school isn\'t as crowded on day two.').
card_multiverse_id('goblin taskmaster'/'ONS', '39722').

card_in_set('grand coliseum', 'ONS').
card_original_type('grand coliseum'/'ONS', 'Land').
card_original_text('grand coliseum'/'ONS', 'Grand Coliseum comes into play tapped.\n{T}: Add {1} to your mana pool.\n{T}: Add one mana of any color to your mana pool. Grand Coliseum deals 1 damage to you.').
card_first_print('grand coliseum', 'ONS').
card_image_name('grand coliseum'/'ONS', 'grand coliseum').
card_uid('grand coliseum'/'ONS', 'ONS:Grand Coliseum:grand coliseum').
card_rarity('grand coliseum'/'ONS', 'Rare').
card_artist('grand coliseum'/'ONS', 'Carl Critchlow').
card_number('grand coliseum'/'ONS', '319').
card_multiverse_id('grand coliseum'/'ONS', '39647').

card_in_set('grand melee', 'ONS').
card_original_type('grand melee'/'ONS', 'Enchantment').
card_original_text('grand melee'/'ONS', 'All creatures attack each turn if able.\nAll creatures block each turn if able.').
card_first_print('grand melee', 'ONS').
card_image_name('grand melee'/'ONS', 'grand melee').
card_uid('grand melee'/'ONS', 'ONS:Grand Melee:grand melee').
card_rarity('grand melee'/'ONS', 'Rare').
card_artist('grand melee'/'ONS', 'Trevor Hairsine').
card_number('grand melee'/'ONS', '211').
card_flavor_text('grand melee'/'ONS', 'Hot blood washes away cold reason.').
card_multiverse_id('grand melee'/'ONS', '31788').

card_in_set('grassland crusader', 'ONS').
card_original_type('grassland crusader'/'ONS', 'Creature — Cleric Soldier').
card_original_text('grassland crusader'/'ONS', '{T}: Target Elf or Soldier gets +2/+2 until end of turn.').
card_first_print('grassland crusader', 'ONS').
card_image_name('grassland crusader'/'ONS', 'grassland crusader').
card_uid('grassland crusader'/'ONS', 'ONS:Grassland Crusader:grassland crusader').
card_rarity('grassland crusader'/'ONS', 'Common').
card_artist('grassland crusader'/'ONS', 'Mark Tedin').
card_number('grassland crusader'/'ONS', '32').
card_flavor_text('grassland crusader'/'ONS', '\"Everyone deserves a chance to live. My job is to make sure they get it.\"').
card_multiverse_id('grassland crusader'/'ONS', '39715').

card_in_set('gratuitous violence', 'ONS').
card_original_type('gratuitous violence'/'ONS', 'Enchantment').
card_original_text('gratuitous violence'/'ONS', 'If a creature you control would deal damage to a creature or player, it deals double that damage to that creature or player instead.').
card_first_print('gratuitous violence', 'ONS').
card_image_name('gratuitous violence'/'ONS', 'gratuitous violence').
card_uid('gratuitous violence'/'ONS', 'ONS:Gratuitous Violence:gratuitous violence').
card_rarity('gratuitous violence'/'ONS', 'Rare').
card_artist('gratuitous violence'/'ONS', 'Christopher Moeller').
card_number('gratuitous violence'/'ONS', '212').
card_flavor_text('gratuitous violence'/'ONS', 'Only the Cabal could make a fight to the death more deadly.').
card_multiverse_id('gratuitous violence'/'ONS', '39666').

card_in_set('gravel slinger', 'ONS').
card_original_type('gravel slinger'/'ONS', 'Creature — Soldier').
card_original_text('gravel slinger'/'ONS', '{T}: Gravel Slinger deals 1 damage to target attacking or blocking creature.\nMorph {1}{W} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('gravel slinger', 'ONS').
card_image_name('gravel slinger'/'ONS', 'gravel slinger').
card_uid('gravel slinger'/'ONS', 'ONS:Gravel Slinger:gravel slinger').
card_rarity('gravel slinger'/'ONS', 'Common').
card_artist('gravel slinger'/'ONS', 'Kev Walker').
card_number('gravel slinger'/'ONS', '33').
card_multiverse_id('gravel slinger'/'ONS', '39716').

card_in_set('gravespawn sovereign', 'ONS').
card_original_type('gravespawn sovereign'/'ONS', 'Creature — Zombie Lord').
card_original_text('gravespawn sovereign'/'ONS', 'Tap five untapped Zombies you control: Put target creature card from a graveyard into play under your control.').
card_first_print('gravespawn sovereign', 'ONS').
card_image_name('gravespawn sovereign'/'ONS', 'gravespawn sovereign').
card_uid('gravespawn sovereign'/'ONS', 'ONS:Gravespawn Sovereign:gravespawn sovereign').
card_rarity('gravespawn sovereign'/'ONS', 'Rare').
card_artist('gravespawn sovereign'/'ONS', 'Adam Rex').
card_number('gravespawn sovereign'/'ONS', '152').
card_flavor_text('gravespawn sovereign'/'ONS', 'The Cabal never expected its creations to create servants of their own.').
card_multiverse_id('gravespawn sovereign'/'ONS', '10722').

card_in_set('graxiplon', 'ONS').
card_original_type('graxiplon'/'ONS', 'Creature — Beast').
card_original_text('graxiplon'/'ONS', 'Graxiplon is unblockable unless defending player controls three or more creatures that share a creature type.').
card_first_print('graxiplon', 'ONS').
card_image_name('graxiplon'/'ONS', 'graxiplon').
card_uid('graxiplon'/'ONS', 'ONS:Graxiplon:graxiplon').
card_rarity('graxiplon'/'ONS', 'Uncommon').
card_artist('graxiplon'/'ONS', 'Iain McCaig').
card_number('graxiplon'/'ONS', '86').
card_flavor_text('graxiplon'/'ONS', '\"Attempts to revive this ancient species have not been entirely successful, but results indicate potential for future development.\"\n—Riptide Project researcher').
card_multiverse_id('graxiplon'/'ONS', '29714').

card_in_set('grinning demon', 'ONS').
card_original_type('grinning demon'/'ONS', 'Creature — Demon').
card_original_text('grinning demon'/'ONS', 'At the beginning of your upkeep, you lose 2 life.\nMorph {2}{B}{B} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('grinning demon', 'ONS').
card_image_name('grinning demon'/'ONS', 'grinning demon').
card_uid('grinning demon'/'ONS', 'ONS:Grinning Demon:grinning demon').
card_rarity('grinning demon'/'ONS', 'Rare').
card_artist('grinning demon'/'ONS', 'Mark Zug').
card_number('grinning demon'/'ONS', '153').
card_flavor_text('grinning demon'/'ONS', 'It\'s drawn to the scent of screaming.').
card_multiverse_id('grinning demon'/'ONS', '39734').

card_in_set('gustcloak harrier', 'ONS').
card_original_type('gustcloak harrier'/'ONS', 'Creature — Bird Soldier').
card_original_text('gustcloak harrier'/'ONS', 'Flying\nWhenever Gustcloak Harrier becomes blocked, you may untap it and remove it from combat.').
card_first_print('gustcloak harrier', 'ONS').
card_image_name('gustcloak harrier'/'ONS', 'gustcloak harrier').
card_uid('gustcloak harrier'/'ONS', 'ONS:Gustcloak Harrier:gustcloak harrier').
card_rarity('gustcloak harrier'/'ONS', 'Common').
card_artist('gustcloak harrier'/'ONS', 'Dan Frazier').
card_number('gustcloak harrier'/'ONS', '34').
card_flavor_text('gustcloak harrier'/'ONS', 'Banking steeply, the aven streaked toward the ground—and vanished.').
card_multiverse_id('gustcloak harrier'/'ONS', '39432').

card_in_set('gustcloak runner', 'ONS').
card_original_type('gustcloak runner'/'ONS', 'Creature — Soldier').
card_original_text('gustcloak runner'/'ONS', 'Whenever Gustcloak Runner becomes blocked, you may untap it and remove it from combat.').
card_first_print('gustcloak runner', 'ONS').
card_image_name('gustcloak runner'/'ONS', 'gustcloak runner').
card_uid('gustcloak runner'/'ONS', 'ONS:Gustcloak Runner:gustcloak runner').
card_rarity('gustcloak runner'/'ONS', 'Common').
card_artist('gustcloak runner'/'ONS', 'Glen Angus').
card_number('gustcloak runner'/'ONS', '35').
card_flavor_text('gustcloak runner'/'ONS', 'Cabal spies trying to follow the Order\'s movements are constantly frustrated by tracks that stop dead.').
card_multiverse_id('gustcloak runner'/'ONS', '39420').

card_in_set('gustcloak savior', 'ONS').
card_original_type('gustcloak savior'/'ONS', 'Creature — Bird Soldier').
card_original_text('gustcloak savior'/'ONS', 'Flying\nWhenever a creature you control becomes blocked, you may untap that creature and remove it from combat.').
card_first_print('gustcloak savior', 'ONS').
card_image_name('gustcloak savior'/'ONS', 'gustcloak savior').
card_uid('gustcloak savior'/'ONS', 'ONS:Gustcloak Savior:gustcloak savior').
card_rarity('gustcloak savior'/'ONS', 'Rare').
card_artist('gustcloak savior'/'ONS', 'Jim Nelson').
card_number('gustcloak savior'/'ONS', '36').
card_flavor_text('gustcloak savior'/'ONS', '\"Our death-arrows flew in high arcs towards the aven. And then . . . nothing.\"\n—Coliseum guard').
card_multiverse_id('gustcloak savior'/'ONS', '39602').

card_in_set('gustcloak sentinel', 'ONS').
card_original_type('gustcloak sentinel'/'ONS', 'Creature — Soldier').
card_original_text('gustcloak sentinel'/'ONS', 'Whenever Gustcloak Sentinel becomes blocked, you may untap it and remove it from combat.').
card_first_print('gustcloak sentinel', 'ONS').
card_image_name('gustcloak sentinel'/'ONS', 'gustcloak sentinel').
card_uid('gustcloak sentinel'/'ONS', 'ONS:Gustcloak Sentinel:gustcloak sentinel').
card_rarity('gustcloak sentinel'/'ONS', 'Uncommon').
card_artist('gustcloak sentinel'/'ONS', 'Mark Zug').
card_number('gustcloak sentinel'/'ONS', '37').
card_flavor_text('gustcloak sentinel'/'ONS', 'Entire platoons have mysteriously vanished from battle, leaving enemy weapons to slice through empty air.').
card_multiverse_id('gustcloak sentinel'/'ONS', '39509').

card_in_set('gustcloak skirmisher', 'ONS').
card_original_type('gustcloak skirmisher'/'ONS', 'Creature — Bird Soldier').
card_original_text('gustcloak skirmisher'/'ONS', 'Flying\nWhenever Gustcloak Skirmisher becomes blocked, you may untap it and remove it from combat.').
card_first_print('gustcloak skirmisher', 'ONS').
card_image_name('gustcloak skirmisher'/'ONS', 'gustcloak skirmisher').
card_uid('gustcloak skirmisher'/'ONS', 'ONS:Gustcloak Skirmisher:gustcloak skirmisher').
card_rarity('gustcloak skirmisher'/'ONS', 'Uncommon').
card_artist('gustcloak skirmisher'/'ONS', 'Dan Frazier').
card_number('gustcloak skirmisher'/'ONS', '38').
card_flavor_text('gustcloak skirmisher'/'ONS', 'They\'re trained in the art of pressing their luck.').
card_multiverse_id('gustcloak skirmisher'/'ONS', '39839').

card_in_set('harsh mercy', 'ONS').
card_original_type('harsh mercy'/'ONS', 'Sorcery').
card_original_text('harsh mercy'/'ONS', 'Each player chooses a creature type. Destroy all creatures that aren\'t of a type chosen this way. They can\'t be regenerated.').
card_first_print('harsh mercy', 'ONS').
card_image_name('harsh mercy'/'ONS', 'harsh mercy').
card_uid('harsh mercy'/'ONS', 'ONS:Harsh Mercy:harsh mercy').
card_rarity('harsh mercy'/'ONS', 'Rare').
card_artist('harsh mercy'/'ONS', 'John Matson').
card_number('harsh mercy'/'ONS', '39').
card_flavor_text('harsh mercy'/'ONS', '\"There is no greater burden than choosing who to save.\"\n—Kamahl, druid acolyte').
card_multiverse_id('harsh mercy'/'ONS', '39614').

card_in_set('haunted cadaver', 'ONS').
card_original_type('haunted cadaver'/'ONS', 'Creature — Zombie').
card_original_text('haunted cadaver'/'ONS', 'Whenever Haunted Cadaver deals combat damage to a player, you may sacrifice it. If you do, that player discards three cards from his or her hand.\nMorph {1}{B} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('haunted cadaver', 'ONS').
card_image_name('haunted cadaver'/'ONS', 'haunted cadaver').
card_uid('haunted cadaver'/'ONS', 'ONS:Haunted Cadaver:haunted cadaver').
card_rarity('haunted cadaver'/'ONS', 'Common').
card_artist('haunted cadaver'/'ONS', 'Randy Gallegos').
card_number('haunted cadaver'/'ONS', '154').
card_multiverse_id('haunted cadaver'/'ONS', '39852').

card_in_set('head games', 'ONS').
card_original_type('head games'/'ONS', 'Sorcery').
card_original_text('head games'/'ONS', 'Target opponent puts the cards from his or her hand on top of his or her library. Search that player\'s library for that many cards. The player puts those cards into his or her hand, then shuffles his or her library.').
card_first_print('head games', 'ONS').
card_image_name('head games'/'ONS', 'head games').
card_uid('head games'/'ONS', 'ONS:Head Games:head games').
card_rarity('head games'/'ONS', 'Rare').
card_artist('head games'/'ONS', 'Terese Nielsen').
card_number('head games'/'ONS', '155').
card_multiverse_id('head games'/'ONS', '41132').

card_in_set('headhunter', 'ONS').
card_original_type('headhunter'/'ONS', 'Creature — Cleric').
card_original_text('headhunter'/'ONS', 'Whenever Headhunter deals combat damage to a player, that player discards a card from his or her hand.\nMorph {B} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('headhunter', 'ONS').
card_image_name('headhunter'/'ONS', 'headhunter').
card_uid('headhunter'/'ONS', 'ONS:Headhunter:headhunter').
card_rarity('headhunter'/'ONS', 'Uncommon').
card_artist('headhunter'/'ONS', 'Matt Cavotta').
card_number('headhunter'/'ONS', '156').
card_multiverse_id('headhunter'/'ONS', '39726').

card_in_set('heedless one', 'ONS').
card_original_type('heedless one'/'ONS', 'Creature — Elf Avatar').
card_original_text('heedless one'/'ONS', 'Trample\nHeedless One\'s power and toughness are each equal to the number of Elves in play.').
card_first_print('heedless one', 'ONS').
card_image_name('heedless one'/'ONS', 'heedless one').
card_uid('heedless one'/'ONS', 'ONS:Heedless One:heedless one').
card_rarity('heedless one'/'ONS', 'Uncommon').
card_artist('heedless one'/'ONS', 'Mark Zug').
card_number('heedless one'/'ONS', '265').
card_flavor_text('heedless one'/'ONS', '\"Channel your vitality through me.\"').
card_multiverse_id('heedless one'/'ONS', '39744').

card_in_set('hystrodon', 'ONS').
card_original_type('hystrodon'/'ONS', 'Creature — Beast').
card_original_text('hystrodon'/'ONS', 'Trample\nWhenever Hystrodon deals combat damage to a player, you may draw a card.\nMorph {1}{G}{G} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('hystrodon', 'ONS').
card_image_name('hystrodon'/'ONS', 'hystrodon').
card_uid('hystrodon'/'ONS', 'ONS:Hystrodon:hystrodon').
card_rarity('hystrodon'/'ONS', 'Rare').
card_artist('hystrodon'/'ONS', 'Anthony S. Waters').
card_number('hystrodon'/'ONS', '266').
card_multiverse_id('hystrodon'/'ONS', '34952').

card_in_set('imagecrafter', 'ONS').
card_original_type('imagecrafter'/'ONS', 'Creature — Wizard').
card_original_text('imagecrafter'/'ONS', '{T}: Choose a creature type other than Legend or Wall. Target creature\'s type becomes that type until end of turn.').
card_first_print('imagecrafter', 'ONS').
card_image_name('imagecrafter'/'ONS', 'imagecrafter').
card_uid('imagecrafter'/'ONS', 'ONS:Imagecrafter:imagecrafter').
card_rarity('imagecrafter'/'ONS', 'Common').
card_artist('imagecrafter'/'ONS', 'Terese Nielsen').
card_number('imagecrafter'/'ONS', '87').
card_flavor_text('imagecrafter'/'ONS', 'When Otarians learned not to trust wizards, the wizards learned to adapt.').
card_multiverse_id('imagecrafter'/'ONS', '39884').

card_in_set('improvised armor', 'ONS').
card_original_type('improvised armor'/'ONS', 'Enchant Creature').
card_original_text('improvised armor'/'ONS', 'Enchanted creature gets +2/+5.\nCycling {3} ({3}, Discard this card from your hand: Draw a card.)').
card_first_print('improvised armor', 'ONS').
card_image_name('improvised armor'/'ONS', 'improvised armor').
card_uid('improvised armor'/'ONS', 'ONS:Improvised Armor:improvised armor').
card_rarity('improvised armor'/'ONS', 'Uncommon').
card_artist('improvised armor'/'ONS', 'Alan Pollack').
card_number('improvised armor'/'ONS', '40').
card_flavor_text('improvised armor'/'ONS', '\"I trust what I make for myself. Do you trust more what is made for you by another?\"').
card_multiverse_id('improvised armor'/'ONS', '41163').

card_in_set('infest', 'ONS').
card_original_type('infest'/'ONS', 'Sorcery').
card_original_text('infest'/'ONS', 'All creatures get -2/-2 until end of turn.').
card_image_name('infest'/'ONS', 'infest').
card_uid('infest'/'ONS', 'ONS:Infest:infest').
card_rarity('infest'/'ONS', 'Uncommon').
card_artist('infest'/'ONS', 'Ben Thompson').
card_number('infest'/'ONS', '157').
card_flavor_text('infest'/'ONS', '\"It is from Phage that this evil springs, and to her it shall return.\"\n—Akroma, angelic avenger').
card_multiverse_id('infest'/'ONS', '23280').

card_in_set('information dealer', 'ONS').
card_original_type('information dealer'/'ONS', 'Creature — Wizard').
card_original_text('information dealer'/'ONS', '{T}: Look at the top X cards of your library, where X is the number of Wizards in play, then put them back in any order.').
card_first_print('information dealer', 'ONS').
card_image_name('information dealer'/'ONS', 'information dealer').
card_uid('information dealer'/'ONS', 'ONS:Information Dealer:information dealer').
card_rarity('information dealer'/'ONS', 'Common').
card_artist('information dealer'/'ONS', 'Jerry Tiritilli').
card_number('information dealer'/'ONS', '88').
card_flavor_text('information dealer'/'ONS', '\"One wizard is a suspect. Two wizards are a conspiracy.\"\n—Elvish refugee').
card_multiverse_id('information dealer'/'ONS', '39437').

card_in_set('inspirit', 'ONS').
card_original_type('inspirit'/'ONS', 'Instant').
card_original_text('inspirit'/'ONS', 'Untap target creature. It gets +2/+4 until end of turn.').
card_first_print('inspirit', 'ONS').
card_image_name('inspirit'/'ONS', 'inspirit').
card_uid('inspirit'/'ONS', 'ONS:Inspirit:inspirit').
card_rarity('inspirit'/'ONS', 'Uncommon').
card_artist('inspirit'/'ONS', 'Keith Garletts').
card_number('inspirit'/'ONS', '41').
card_flavor_text('inspirit'/'ONS', '\"We will force the Cabal into their own pits!\"').
card_multiverse_id('inspirit'/'ONS', '41154').

card_in_set('insurrection', 'ONS').
card_original_type('insurrection'/'ONS', 'Sorcery').
card_original_text('insurrection'/'ONS', 'Untap all creatures and gain control of them until end of turn. They gain haste until end of turn.').
card_first_print('insurrection', 'ONS').
card_image_name('insurrection'/'ONS', 'insurrection').
card_uid('insurrection'/'ONS', 'ONS:Insurrection:insurrection').
card_rarity('insurrection'/'ONS', 'Rare').
card_artist('insurrection'/'ONS', 'Mark Zug').
card_number('insurrection'/'ONS', '213').
card_flavor_text('insurrection'/'ONS', '\"Maybe they wanted to be on the winning side for once.\"\n—Matoc, lavamancer').
card_multiverse_id('insurrection'/'ONS', '35457').

card_in_set('invigorating boon', 'ONS').
card_original_type('invigorating boon'/'ONS', 'Enchantment').
card_original_text('invigorating boon'/'ONS', 'Whenever a player cycles a card, you may put a +1/+1 counter on target creature.').
card_first_print('invigorating boon', 'ONS').
card_image_name('invigorating boon'/'ONS', 'invigorating boon').
card_uid('invigorating boon'/'ONS', 'ONS:Invigorating Boon:invigorating boon').
card_rarity('invigorating boon'/'ONS', 'Uncommon').
card_artist('invigorating boon'/'ONS', 'Edward P. Beard, Jr.').
card_number('invigorating boon'/'ONS', '267').
card_flavor_text('invigorating boon'/'ONS', 'The Mirari\'s echoes rang in the scouts\' minds long after they had returned from the Krosan Forest.').
card_multiverse_id('invigorating boon'/'ONS', '41151').

card_in_set('ironfist crusher', 'ONS').
card_original_type('ironfist crusher'/'ONS', 'Creature — Soldier').
card_original_text('ironfist crusher'/'ONS', 'Ironfist Crusher may block any number of creatures.\nMorph {3}{W} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('ironfist crusher', 'ONS').
card_image_name('ironfist crusher'/'ONS', 'ironfist crusher').
card_uid('ironfist crusher'/'ONS', 'ONS:Ironfist Crusher:ironfist crusher').
card_rarity('ironfist crusher'/'ONS', 'Uncommon').
card_artist('ironfist crusher'/'ONS', 'Iain McCaig').
card_number('ironfist crusher'/'ONS', '42').
card_multiverse_id('ironfist crusher'/'ONS', '39517').

card_in_set('island', 'ONS').
card_original_type('island'/'ONS', 'Land').
card_original_text('island'/'ONS', 'U').
card_image_name('island'/'ONS', 'island1').
card_uid('island'/'ONS', 'ONS:Island:island1').
card_rarity('island'/'ONS', 'Basic Land').
card_artist('island'/'ONS', 'Tony Szczudlo').
card_number('island'/'ONS', '335').
card_multiverse_id('island'/'ONS', '40120').

card_in_set('island', 'ONS').
card_original_type('island'/'ONS', 'Land').
card_original_text('island'/'ONS', 'U').
card_image_name('island'/'ONS', 'island2').
card_uid('island'/'ONS', 'ONS:Island:island2').
card_rarity('island'/'ONS', 'Basic Land').
card_artist('island'/'ONS', 'Bradley Williams').
card_number('island'/'ONS', '336').
card_multiverse_id('island'/'ONS', '40119').

card_in_set('island', 'ONS').
card_original_type('island'/'ONS', 'Land').
card_original_text('island'/'ONS', 'U').
card_image_name('island'/'ONS', 'island3').
card_uid('island'/'ONS', 'ONS:Island:island3').
card_rarity('island'/'ONS', 'Basic Land').
card_artist('island'/'ONS', 'Matt Thompson').
card_number('island'/'ONS', '337').
card_multiverse_id('island'/'ONS', '40118').

card_in_set('island', 'ONS').
card_original_type('island'/'ONS', 'Land').
card_original_text('island'/'ONS', 'U').
card_image_name('island'/'ONS', 'island4').
card_uid('island'/'ONS', 'ONS:Island:island4').
card_rarity('island'/'ONS', 'Basic Land').
card_artist('island'/'ONS', 'Randy Elliott').
card_number('island'/'ONS', '338').
card_multiverse_id('island'/'ONS', '40117').

card_in_set('ixidor\'s will', 'ONS').
card_original_type('ixidor\'s will'/'ONS', 'Instant').
card_original_text('ixidor\'s will'/'ONS', 'Counter target spell unless its controller pays {2} for each Wizard in play.').
card_first_print('ixidor\'s will', 'ONS').
card_image_name('ixidor\'s will'/'ONS', 'ixidor\'s will').
card_uid('ixidor\'s will'/'ONS', 'ONS:Ixidor\'s Will:ixidor\'s will').
card_rarity('ixidor\'s will'/'ONS', 'Common').
card_artist('ixidor\'s will'/'ONS', 'Eric Peterson').
card_number('ixidor\'s will'/'ONS', '90').
card_flavor_text('ixidor\'s will'/'ONS', '\"Some dreams should not come to be.\"').
card_multiverse_id('ixidor\'s will'/'ONS', '39843').

card_in_set('ixidor, reality sculptor', 'ONS').
card_original_type('ixidor, reality sculptor'/'ONS', 'Creature — Wizard Legend').
card_original_text('ixidor, reality sculptor'/'ONS', 'Face-down creatures get +1/+1.\n{2}{U}: Turn target face-down creature face up.').
card_first_print('ixidor, reality sculptor', 'ONS').
card_image_name('ixidor, reality sculptor'/'ONS', 'ixidor, reality sculptor').
card_uid('ixidor, reality sculptor'/'ONS', 'ONS:Ixidor, Reality Sculptor:ixidor, reality sculptor').
card_rarity('ixidor, reality sculptor'/'ONS', 'Rare').
card_artist('ixidor, reality sculptor'/'ONS', 'Kev Walker').
card_number('ixidor, reality sculptor'/'ONS', '89').
card_flavor_text('ixidor, reality sculptor'/'ONS', '\"Reality has exiled me. I am no longer bound by its laws.\"').
card_multiverse_id('ixidor, reality sculptor'/'ONS', '40578').

card_in_set('jareth, leonine titan', 'ONS').
card_original_type('jareth, leonine titan'/'ONS', 'Creature — Cat Giant Legend').
card_original_text('jareth, leonine titan'/'ONS', 'Whenever Jareth, Leonine Titan blocks, it gets +7/+7 until end of turn.\n{W}: Jareth gains protection from the color of your choice until end of turn.').
card_first_print('jareth, leonine titan', 'ONS').
card_image_name('jareth, leonine titan'/'ONS', 'jareth, leonine titan').
card_uid('jareth, leonine titan'/'ONS', 'ONS:Jareth, Leonine Titan:jareth, leonine titan').
card_rarity('jareth, leonine titan'/'ONS', 'Rare').
card_artist('jareth, leonine titan'/'ONS', 'Daren Bader').
card_number('jareth, leonine titan'/'ONS', '43').
card_flavor_text('jareth, leonine titan'/'ONS', 'Light\'s champion in the stronghold of darkness.').
card_multiverse_id('jareth, leonine titan'/'ONS', '39861').

card_in_set('kaboom!', 'ONS').
card_original_type('kaboom!'/'ONS', 'Sorcery').
card_original_text('kaboom!'/'ONS', 'Choose any number of target players. For each of those players, reveal cards from the top of your library until you reveal a nonland card. Kaboom! deals damage equal to that card\'s converted mana cost to that player, then you put the revealed cards on the bottom of your library in any order.').
card_first_print('kaboom!', 'ONS').
card_image_name('kaboom!'/'ONS', 'kaboom!').
card_uid('kaboom!'/'ONS', 'ONS:Kaboom!:kaboom!').
card_rarity('kaboom!'/'ONS', 'Rare').
card_artist('kaboom!'/'ONS', 'Glen Angus').
card_number('kaboom!'/'ONS', '214').
card_multiverse_id('kaboom!'/'ONS', '39559').

card_in_set('kamahl\'s summons', 'ONS').
card_original_type('kamahl\'s summons'/'ONS', 'Sorcery').
card_original_text('kamahl\'s summons'/'ONS', 'Each player may reveal any number of creature cards from his or her hand. Then each player puts a 2/2 green Bear creature token into play for each card he or she revealed this way.').
card_first_print('kamahl\'s summons', 'ONS').
card_image_name('kamahl\'s summons'/'ONS', 'kamahl\'s summons').
card_uid('kamahl\'s summons'/'ONS', 'ONS:Kamahl\'s Summons:kamahl\'s summons').
card_rarity('kamahl\'s summons'/'ONS', 'Uncommon').
card_artist('kamahl\'s summons'/'ONS', 'Anthony S. Waters').
card_number('kamahl\'s summons'/'ONS', '269').
card_flavor_text('kamahl\'s summons'/'ONS', 'As Krosa unleashed the peace in Kamahl, he unleashed the fury in Krosa.').
card_multiverse_id('kamahl\'s summons'/'ONS', '34228').

card_in_set('kamahl, fist of krosa', 'ONS').
card_original_type('kamahl, fist of krosa'/'ONS', 'Creature — Druid Legend').
card_original_text('kamahl, fist of krosa'/'ONS', '{G}: Target land becomes a 1/1 creature until end of turn. It\'s still a land.\n{2}{G}{G}{G}: Creatures you control get +3/+3 and gain trample until end of turn.').
card_first_print('kamahl, fist of krosa', 'ONS').
card_image_name('kamahl, fist of krosa'/'ONS', 'kamahl, fist of krosa').
card_uid('kamahl, fist of krosa'/'ONS', 'ONS:Kamahl, Fist of Krosa:kamahl, fist of krosa').
card_rarity('kamahl, fist of krosa'/'ONS', 'Rare').
card_artist('kamahl, fist of krosa'/'ONS', 'Matthew D. Wilson').
card_number('kamahl, fist of krosa'/'ONS', '268').
card_flavor_text('kamahl, fist of krosa'/'ONS', '\"My mind has changed. My strength has not.\"').
card_multiverse_id('kamahl, fist of krosa'/'ONS', '40538').

card_in_set('krosan colossus', 'ONS').
card_original_type('krosan colossus'/'ONS', 'Creature — Beast').
card_original_text('krosan colossus'/'ONS', 'Morph {6}{G}{G} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('krosan colossus', 'ONS').
card_image_name('krosan colossus'/'ONS', 'krosan colossus').
card_uid('krosan colossus'/'ONS', 'ONS:Krosan Colossus:krosan colossus').
card_rarity('krosan colossus'/'ONS', 'Rare').
card_artist('krosan colossus'/'ONS', 'Kev Walker').
card_number('krosan colossus'/'ONS', '270').
card_flavor_text('krosan colossus'/'ONS', 'When it walked, it altered geography. When it bellowed, it changed the weather.').
card_multiverse_id('krosan colossus'/'ONS', '39669').

card_in_set('krosan groundshaker', 'ONS').
card_original_type('krosan groundshaker'/'ONS', 'Creature — Beast').
card_original_text('krosan groundshaker'/'ONS', '{G}: Target Beast gains trample until end of turn.').
card_first_print('krosan groundshaker', 'ONS').
card_image_name('krosan groundshaker'/'ONS', 'krosan groundshaker').
card_uid('krosan groundshaker'/'ONS', 'ONS:Krosan Groundshaker:krosan groundshaker').
card_rarity('krosan groundshaker'/'ONS', 'Uncommon').
card_artist('krosan groundshaker'/'ONS', 'Wayne England').
card_number('krosan groundshaker'/'ONS', '271').
card_flavor_text('krosan groundshaker'/'ONS', 'You know it\'s coming when you hear the distant thunder. You know where it\'s been when you see the path of broken trees.').
card_multiverse_id('krosan groundshaker'/'ONS', '41056').

card_in_set('krosan tusker', 'ONS').
card_original_type('krosan tusker'/'ONS', 'Creature — Beast').
card_original_text('krosan tusker'/'ONS', 'Cycling {2}{G} ({2}{G}, Discard this card from your hand: Draw a card.)\nWhen you cycle Krosan Tusker, you may search your library for a basic land card, reveal that card, and put it into your hand. Then shuffle your library.').
card_image_name('krosan tusker'/'ONS', 'krosan tusker').
card_uid('krosan tusker'/'ONS', 'ONS:Krosan Tusker:krosan tusker').
card_rarity('krosan tusker'/'ONS', 'Common').
card_artist('krosan tusker'/'ONS', 'Kev Walker').
card_number('krosan tusker'/'ONS', '272').
card_multiverse_id('krosan tusker'/'ONS', '43479').

card_in_set('lavamancer\'s skill', 'ONS').
card_original_type('lavamancer\'s skill'/'ONS', 'Enchant Creature').
card_original_text('lavamancer\'s skill'/'ONS', 'Enchanted creature has \"{T}: This creature deals 1 damage to target creature.\"\nIf enchanted creature is a Wizard, it has \"{T}: This creature deals 2 damage to target creature.\"').
card_first_print('lavamancer\'s skill', 'ONS').
card_image_name('lavamancer\'s skill'/'ONS', 'lavamancer\'s skill').
card_uid('lavamancer\'s skill'/'ONS', 'ONS:Lavamancer\'s Skill:lavamancer\'s skill').
card_rarity('lavamancer\'s skill'/'ONS', 'Common').
card_artist('lavamancer\'s skill'/'ONS', 'Monte Michael Moore').
card_number('lavamancer\'s skill'/'ONS', '215').
card_multiverse_id('lavamancer\'s skill'/'ONS', '39479').

card_in_set('lay waste', 'ONS').
card_original_type('lay waste'/'ONS', 'Sorcery').
card_original_text('lay waste'/'ONS', 'Destroy target land.\nCycling {2} ({2}, Discard this card from your hand: Draw a card.)').
card_image_name('lay waste'/'ONS', 'lay waste').
card_uid('lay waste'/'ONS', 'ONS:Lay Waste:lay waste').
card_rarity('lay waste'/'ONS', 'Common').
card_artist('lay waste'/'ONS', 'Carl Critchlow').
card_number('lay waste'/'ONS', '216').
card_flavor_text('lay waste'/'ONS', 'Skirk Ridge had survived earthquakes, mudslides, and meteor showers. Then the goblins moved in.').
card_multiverse_id('lay waste'/'ONS', '41158').

card_in_set('leery fogbeast', 'ONS').
card_original_type('leery fogbeast'/'ONS', 'Creature — Beast').
card_original_text('leery fogbeast'/'ONS', 'Whenever Leery Fogbeast becomes blocked, prevent all combat damage that would be dealt this turn.').
card_first_print('leery fogbeast', 'ONS').
card_image_name('leery fogbeast'/'ONS', 'leery fogbeast').
card_uid('leery fogbeast'/'ONS', 'ONS:Leery Fogbeast:leery fogbeast').
card_rarity('leery fogbeast'/'ONS', 'Common').
card_artist('leery fogbeast'/'ONS', 'Matt Cavotta').
card_number('leery fogbeast'/'ONS', '273').
card_flavor_text('leery fogbeast'/'ONS', 'It emerges from the mists only to feed.').
card_multiverse_id('leery fogbeast'/'ONS', '39675').

card_in_set('lightning rift', 'ONS').
card_original_type('lightning rift'/'ONS', 'Enchantment').
card_original_text('lightning rift'/'ONS', 'Whenever a player cycles a card, you may pay {1}. If you do, Lightning Rift deals 2 damage to target creature or player.').
card_image_name('lightning rift'/'ONS', 'lightning rift').
card_uid('lightning rift'/'ONS', 'ONS:Lightning Rift:lightning rift').
card_rarity('lightning rift'/'ONS', 'Uncommon').
card_artist('lightning rift'/'ONS', 'Eric Peterson').
card_number('lightning rift'/'ONS', '217').
card_flavor_text('lightning rift'/'ONS', 'Options will cost you, but a lack of them will cost you even more.').
card_multiverse_id('lightning rift'/'ONS', '41147').

card_in_set('lonely sandbar', 'ONS').
card_original_type('lonely sandbar'/'ONS', 'Land').
card_original_text('lonely sandbar'/'ONS', 'Lonely Sandbar comes into play tapped.\n{T}: Add {U} to your mana pool.\nCycling {U} ({U}, Discard this card from your hand: Draw a card.)').
card_first_print('lonely sandbar', 'ONS').
card_image_name('lonely sandbar'/'ONS', 'lonely sandbar').
card_uid('lonely sandbar'/'ONS', 'ONS:Lonely Sandbar:lonely sandbar').
card_rarity('lonely sandbar'/'ONS', 'Common').
card_artist('lonely sandbar'/'ONS', 'Heather Hudson').
card_number('lonely sandbar'/'ONS', '320').
card_multiverse_id('lonely sandbar'/'ONS', '41138').

card_in_set('mage\'s guile', 'ONS').
card_original_type('mage\'s guile'/'ONS', 'Instant').
card_original_text('mage\'s guile'/'ONS', 'Target creature can\'t be the target of spells or abilities this turn.\nCycling {U} ({U}, Discard this card from your hand: Draw a card.)').
card_first_print('mage\'s guile', 'ONS').
card_image_name('mage\'s guile'/'ONS', 'mage\'s guile').
card_uid('mage\'s guile'/'ONS', 'ONS:Mage\'s Guile:mage\'s guile').
card_rarity('mage\'s guile'/'ONS', 'Common').
card_artist('mage\'s guile'/'ONS', 'Edward P. Beard, Jr.').
card_number('mage\'s guile'/'ONS', '91').
card_flavor_text('mage\'s guile'/'ONS', '\"Next time, don\'t bother.\"').
card_multiverse_id('mage\'s guile'/'ONS', '41155').

card_in_set('mana echoes', 'ONS').
card_original_type('mana echoes'/'ONS', 'Enchantment').
card_original_text('mana echoes'/'ONS', 'Whenever a creature comes into play, you may add {1} to your mana pool for each creature you control that shares a creature type with it.').
card_first_print('mana echoes', 'ONS').
card_image_name('mana echoes'/'ONS', 'mana echoes').
card_uid('mana echoes'/'ONS', 'ONS:Mana Echoes:mana echoes').
card_rarity('mana echoes'/'ONS', 'Rare').
card_artist('mana echoes'/'ONS', 'Christopher Moeller').
card_number('mana echoes'/'ONS', '218').
card_flavor_text('mana echoes'/'ONS', 'When the ground is saturated with mana, even the lightest footstep can bring it to the surface.').
card_multiverse_id('mana echoes'/'ONS', '39571').

card_in_set('meddle', 'ONS').
card_original_type('meddle'/'ONS', 'Instant').
card_original_text('meddle'/'ONS', 'If target spell has only one target and that target is a creature, change that spell\'s target to another creature.').
card_image_name('meddle'/'ONS', 'meddle').
card_uid('meddle'/'ONS', 'ONS:Meddle:meddle').
card_rarity('meddle'/'ONS', 'Uncommon').
card_artist('meddle'/'ONS', 'Brian Snõddy').
card_number('meddle'/'ONS', '92').
card_flavor_text('meddle'/'ONS', 'Strength may win the fight, but style wins the crowd.').
card_multiverse_id('meddle'/'ONS', '39451').

card_in_set('menacing ogre', 'ONS').
card_original_type('menacing ogre'/'ONS', 'Creature — Ogre').
card_original_text('menacing ogre'/'ONS', 'Trample, haste\nWhen Menacing Ogre comes into play, each player secretly chooses a number. Then those numbers are revealed. Each player with the highest number loses that much life. If you are one of those players, put two +1/+1 counters on Menacing Ogre.').
card_first_print('menacing ogre', 'ONS').
card_image_name('menacing ogre'/'ONS', 'menacing ogre').
card_uid('menacing ogre'/'ONS', 'ONS:Menacing Ogre:menacing ogre').
card_rarity('menacing ogre'/'ONS', 'Rare').
card_artist('menacing ogre'/'ONS', 'Ron Spencer').
card_number('menacing ogre'/'ONS', '219').
card_multiverse_id('menacing ogre'/'ONS', '35164').

card_in_set('misery charm', 'ONS').
card_original_type('misery charm'/'ONS', 'Instant').
card_original_text('misery charm'/'ONS', 'Choose one Destroy target Cleric; or return target Cleric card from your graveyard to your hand; or target player loses 2 life.').
card_first_print('misery charm', 'ONS').
card_image_name('misery charm'/'ONS', 'misery charm').
card_uid('misery charm'/'ONS', 'ONS:Misery Charm:misery charm').
card_rarity('misery charm'/'ONS', 'Common').
card_artist('misery charm'/'ONS', 'David Martin').
card_number('misery charm'/'ONS', '158').
card_multiverse_id('misery charm'/'ONS', '39466').

card_in_set('mistform dreamer', 'ONS').
card_original_type('mistform dreamer'/'ONS', 'Creature — Illusion').
card_original_text('mistform dreamer'/'ONS', 'Flying\n{1}: Mistform Dreamer\'s type becomes the creature type of your choice until end of turn.').
card_first_print('mistform dreamer', 'ONS').
card_image_name('mistform dreamer'/'ONS', 'mistform dreamer').
card_uid('mistform dreamer'/'ONS', 'ONS:Mistform Dreamer:mistform dreamer').
card_rarity('mistform dreamer'/'ONS', 'Common').
card_artist('mistform dreamer'/'ONS', 'Matthew Mitchell').
card_number('mistform dreamer'/'ONS', '93').
card_flavor_text('mistform dreamer'/'ONS', 'Devotion, the second myth of reality: The faithful are most hurt by the objects of their faith.').
card_multiverse_id('mistform dreamer'/'ONS', '39436').

card_in_set('mistform mask', 'ONS').
card_original_type('mistform mask'/'ONS', 'Enchant Creature').
card_original_text('mistform mask'/'ONS', '{1}: Enchanted creature\'s type becomes the creature type of your choice until end of turn.').
card_first_print('mistform mask', 'ONS').
card_image_name('mistform mask'/'ONS', 'mistform mask').
card_uid('mistform mask'/'ONS', 'ONS:Mistform Mask:mistform mask').
card_rarity('mistform mask'/'ONS', 'Common').
card_artist('mistform mask'/'ONS', 'Monte Michael Moore').
card_number('mistform mask'/'ONS', '94').
card_flavor_text('mistform mask'/'ONS', 'Trust, the fifth myth of reality: Every truth holds the seed of betrayal.').
card_multiverse_id('mistform mask'/'ONS', '39449').

card_in_set('mistform mutant', 'ONS').
card_original_type('mistform mutant'/'ONS', 'Creature — Illusion Mutant').
card_original_text('mistform mutant'/'ONS', '{1}{U}: Choose a creature type other than Legend or Wall. Target creature\'s type becomes that type until end of turn.').
card_first_print('mistform mutant', 'ONS').
card_image_name('mistform mutant'/'ONS', 'mistform mutant').
card_uid('mistform mutant'/'ONS', 'ONS:Mistform Mutant:mistform mutant').
card_rarity('mistform mutant'/'ONS', 'Uncommon').
card_artist('mistform mutant'/'ONS', 'John Avon').
card_number('mistform mutant'/'ONS', '95').
card_flavor_text('mistform mutant'/'ONS', 'Familiarity, the first myth of reality: What you know the best, you observe the least.').
card_multiverse_id('mistform mutant'/'ONS', '39529').

card_in_set('mistform shrieker', 'ONS').
card_original_type('mistform shrieker'/'ONS', 'Creature — Illusion').
card_original_text('mistform shrieker'/'ONS', 'Flying\n{1}: Mistform Shrieker\'s type becomes the creature type of your choice until end of turn.\nMorph {3}{U}{U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('mistform shrieker', 'ONS').
card_image_name('mistform shrieker'/'ONS', 'mistform shrieker').
card_uid('mistform shrieker'/'ONS', 'ONS:Mistform Shrieker:mistform shrieker').
card_rarity('mistform shrieker'/'ONS', 'Uncommon').
card_artist('mistform shrieker'/'ONS', 'Glen Angus').
card_number('mistform shrieker'/'ONS', '96').
card_multiverse_id('mistform shrieker'/'ONS', '39525').

card_in_set('mistform skyreaver', 'ONS').
card_original_type('mistform skyreaver'/'ONS', 'Creature — Illusion').
card_original_text('mistform skyreaver'/'ONS', 'Flying\n{1}: Mistform Skyreaver\'s type becomes the creature type of your choice until end of turn.').
card_first_print('mistform skyreaver', 'ONS').
card_image_name('mistform skyreaver'/'ONS', 'mistform skyreaver').
card_uid('mistform skyreaver'/'ONS', 'ONS:Mistform Skyreaver:mistform skyreaver').
card_rarity('mistform skyreaver'/'ONS', 'Rare').
card_artist('mistform skyreaver'/'ONS', 'Anthony S. Waters').
card_number('mistform skyreaver'/'ONS', '97').
card_flavor_text('mistform skyreaver'/'ONS', 'Conviction, the third myth of reality: Only those who seek the truth can be deceived.').
card_multiverse_id('mistform skyreaver'/'ONS', '39619').

card_in_set('mistform stalker', 'ONS').
card_original_type('mistform stalker'/'ONS', 'Creature — Illusion').
card_original_text('mistform stalker'/'ONS', '{1}: Mistform Stalker\'s type becomes the creature type of your choice until end of turn.\n{2}{U}{U}: Mistform Stalker gets +2/+2 and gains flying until end of turn.').
card_first_print('mistform stalker', 'ONS').
card_image_name('mistform stalker'/'ONS', 'mistform stalker').
card_uid('mistform stalker'/'ONS', 'ONS:Mistform Stalker:mistform stalker').
card_rarity('mistform stalker'/'ONS', 'Uncommon').
card_artist('mistform stalker'/'ONS', 'Randy Gallegos').
card_number('mistform stalker'/'ONS', '98').
card_multiverse_id('mistform stalker'/'ONS', '39441').

card_in_set('mistform wall', 'ONS').
card_original_type('mistform wall'/'ONS', 'Creature — Illusion Wall').
card_original_text('mistform wall'/'ONS', '(Walls can\'t attack.)\n{1}: Mistform Wall\'s type becomes the creature type of your choice until end of turn.').
card_first_print('mistform wall', 'ONS').
card_image_name('mistform wall'/'ONS', 'mistform wall').
card_uid('mistform wall'/'ONS', 'ONS:Mistform Wall:mistform wall').
card_rarity('mistform wall'/'ONS', 'Common').
card_artist('mistform wall'/'ONS', 'Franz Vohwinkel').
card_number('mistform wall'/'ONS', '99').
card_flavor_text('mistform wall'/'ONS', 'Fellowship, the fourth myth of reality: As the tides of war shift, so do loyalties.').
card_multiverse_id('mistform wall'/'ONS', '39526').

card_in_set('mobilization', 'ONS').
card_original_type('mobilization'/'ONS', 'Enchantment').
card_original_text('mobilization'/'ONS', 'Attacking doesn\'t cause Soldiers to tap.\n{2}{W}: Put a 1/1 white Soldier creature token into play.').
card_first_print('mobilization', 'ONS').
card_image_name('mobilization'/'ONS', 'mobilization').
card_uid('mobilization'/'ONS', 'ONS:Mobilization:mobilization').
card_rarity('mobilization'/'ONS', 'Rare').
card_artist('mobilization'/'ONS', 'Carl Critchlow').
card_number('mobilization'/'ONS', '44').
card_flavor_text('mobilization'/'ONS', 'Newcomers to Otaria find themselves at the bottom of the heap. In the pits, they at least have the chance to climb atop a heap of bodies.').
card_multiverse_id('mobilization'/'ONS', '39699').

card_in_set('mountain', 'ONS').
card_original_type('mountain'/'ONS', 'Land').
card_original_text('mountain'/'ONS', 'R').
card_image_name('mountain'/'ONS', 'mountain1').
card_uid('mountain'/'ONS', 'ONS:Mountain:mountain1').
card_rarity('mountain'/'ONS', 'Basic Land').
card_artist('mountain'/'ONS', 'Tony Szczudlo').
card_number('mountain'/'ONS', '343').
card_multiverse_id('mountain'/'ONS', '40110').

card_in_set('mountain', 'ONS').
card_original_type('mountain'/'ONS', 'Land').
card_original_text('mountain'/'ONS', 'R').
card_image_name('mountain'/'ONS', 'mountain2').
card_uid('mountain'/'ONS', 'ONS:Mountain:mountain2').
card_rarity('mountain'/'ONS', 'Basic Land').
card_artist('mountain'/'ONS', 'Sam Wood').
card_number('mountain'/'ONS', '344').
card_multiverse_id('mountain'/'ONS', '40109').

card_in_set('mountain', 'ONS').
card_original_type('mountain'/'ONS', 'Land').
card_original_text('mountain'/'ONS', 'R').
card_image_name('mountain'/'ONS', 'mountain3').
card_uid('mountain'/'ONS', 'ONS:Mountain:mountain3').
card_rarity('mountain'/'ONS', 'Basic Land').
card_artist('mountain'/'ONS', 'David Day').
card_number('mountain'/'ONS', '345').
card_multiverse_id('mountain'/'ONS', '40108').

card_in_set('mountain', 'ONS').
card_original_type('mountain'/'ONS', 'Land').
card_original_text('mountain'/'ONS', 'R').
card_image_name('mountain'/'ONS', 'mountain4').
card_uid('mountain'/'ONS', 'ONS:Mountain:mountain4').
card_rarity('mountain'/'ONS', 'Basic Land').
card_artist('mountain'/'ONS', 'Heather Hudson').
card_number('mountain'/'ONS', '346').
card_multiverse_id('mountain'/'ONS', '40107').

card_in_set('mythic proportions', 'ONS').
card_original_type('mythic proportions'/'ONS', 'Enchant Creature').
card_original_text('mythic proportions'/'ONS', 'Enchanted creature gets +8/+8 and has trample.').
card_first_print('mythic proportions', 'ONS').
card_image_name('mythic proportions'/'ONS', 'mythic proportions').
card_uid('mythic proportions'/'ONS', 'ONS:Mythic Proportions:mythic proportions').
card_rarity('mythic proportions'/'ONS', 'Rare').
card_artist('mythic proportions'/'ONS', 'Jim Nelson').
card_number('mythic proportions'/'ONS', '274').
card_flavor_text('mythic proportions'/'ONS', 'The blood of Krosa turns rational beings into primal forces.').
card_multiverse_id('mythic proportions'/'ONS', '34684').

card_in_set('nameless one', 'ONS').
card_original_type('nameless one'/'ONS', 'Creature — Wizard Avatar').
card_original_text('nameless one'/'ONS', 'Nameless One\'s power and toughness are each equal to the number of Wizards in play.\nMorph {2}{U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('nameless one', 'ONS').
card_image_name('nameless one'/'ONS', 'nameless one').
card_uid('nameless one'/'ONS', 'ONS:Nameless One:nameless one').
card_rarity('nameless one'/'ONS', 'Uncommon').
card_artist('nameless one'/'ONS', 'Mark Tedin').
card_number('nameless one'/'ONS', '100').
card_flavor_text('nameless one'/'ONS', '\"Open your mind to me.\"').
card_multiverse_id('nameless one'/'ONS', '39516').

card_in_set('nantuko husk', 'ONS').
card_original_type('nantuko husk'/'ONS', 'Creature — Zombie Insect').
card_original_text('nantuko husk'/'ONS', 'Sacrifice a creature: Nantuko Husk gets +2/+2 until end of turn.').
card_first_print('nantuko husk', 'ONS').
card_image_name('nantuko husk'/'ONS', 'nantuko husk').
card_uid('nantuko husk'/'ONS', 'ONS:Nantuko Husk:nantuko husk').
card_rarity('nantuko husk'/'ONS', 'Common').
card_artist('nantuko husk'/'ONS', 'Carl Critchlow').
card_number('nantuko husk'/'ONS', '159').
card_flavor_text('nantuko husk'/'ONS', 'The soul sheds light, and death is its shadow. When the light dims, life and death embrace.\n—Nantuko teaching').
card_multiverse_id('nantuko husk'/'ONS', '42186').

card_in_set('naturalize', 'ONS').
card_original_type('naturalize'/'ONS', 'Instant').
card_original_text('naturalize'/'ONS', 'Destroy target artifact or enchantment.').
card_first_print('naturalize', 'ONS').
card_image_name('naturalize'/'ONS', 'naturalize').
card_uid('naturalize'/'ONS', 'ONS:Naturalize:naturalize').
card_rarity('naturalize'/'ONS', 'Common').
card_artist('naturalize'/'ONS', 'Ron Spears').
card_number('naturalize'/'ONS', '275').
card_flavor_text('naturalize'/'ONS', '\"From here, let the world be reborn.\"').
card_multiverse_id('naturalize'/'ONS', '35414').

card_in_set('nosy goblin', 'ONS').
card_original_type('nosy goblin'/'ONS', 'Creature — Goblin').
card_original_text('nosy goblin'/'ONS', '{T}, Sacrifice Nosy Goblin: Destroy target face-down creature.').
card_first_print('nosy goblin', 'ONS').
card_image_name('nosy goblin'/'ONS', 'nosy goblin').
card_uid('nosy goblin'/'ONS', 'ONS:Nosy Goblin:nosy goblin').
card_rarity('nosy goblin'/'ONS', 'Common').
card_artist('nosy goblin'/'ONS', 'Thomas M. Baxa').
card_number('nosy goblin'/'ONS', '220').
card_flavor_text('nosy goblin'/'ONS', 'To his surprise, Furt discovered that the strange creatures were not at all like bugs.').
card_multiverse_id('nosy goblin'/'ONS', '39710').

card_in_set('nova cleric', 'ONS').
card_original_type('nova cleric'/'ONS', 'Creature — Cleric').
card_original_text('nova cleric'/'ONS', '{2}{W}, {T}, Sacrifice Nova Cleric: Destroy all enchantments.').
card_first_print('nova cleric', 'ONS').
card_image_name('nova cleric'/'ONS', 'nova cleric').
card_uid('nova cleric'/'ONS', 'ONS:Nova Cleric:nova cleric').
card_rarity('nova cleric'/'ONS', 'Uncommon').
card_artist('nova cleric'/'ONS', 'Alan Pollack').
card_number('nova cleric'/'ONS', '45').
card_flavor_text('nova cleric'/'ONS', '\"Our noblest thoughts are our very first and our very last.\"').
card_multiverse_id('nova cleric'/'ONS', '39732').

card_in_set('oblation', 'ONS').
card_original_type('oblation'/'ONS', 'Instant').
card_original_text('oblation'/'ONS', 'The owner of target nonland permanent shuffles it into his or her library, then draws two cards.').
card_first_print('oblation', 'ONS').
card_image_name('oblation'/'ONS', 'oblation').
card_uid('oblation'/'ONS', 'ONS:Oblation:oblation').
card_rarity('oblation'/'ONS', 'Rare').
card_artist('oblation'/'ONS', 'Doug Chaffee').
card_number('oblation'/'ONS', '46').
card_flavor_text('oblation'/'ONS', '\"A richer people could give more but they could never give as much.\"').
card_multiverse_id('oblation'/'ONS', '39933').

card_in_set('oversold cemetery', 'ONS').
card_original_type('oversold cemetery'/'ONS', 'Enchantment').
card_original_text('oversold cemetery'/'ONS', 'At the beginning of your upkeep, if you have four or more creature cards in your graveyard, you may return target creature card from your graveyard to your hand.').
card_first_print('oversold cemetery', 'ONS').
card_image_name('oversold cemetery'/'ONS', 'oversold cemetery').
card_uid('oversold cemetery'/'ONS', 'ONS:Oversold Cemetery:oversold cemetery').
card_rarity('oversold cemetery'/'ONS', 'Rare').
card_artist('oversold cemetery'/'ONS', 'Thomas M. Baxa').
card_number('oversold cemetery'/'ONS', '160').
card_multiverse_id('oversold cemetery'/'ONS', '33693').

card_in_set('overwhelming instinct', 'ONS').
card_original_type('overwhelming instinct'/'ONS', 'Enchantment').
card_original_text('overwhelming instinct'/'ONS', 'Whenever you attack with three or more creatures, draw a card.').
card_first_print('overwhelming instinct', 'ONS').
card_image_name('overwhelming instinct'/'ONS', 'overwhelming instinct').
card_uid('overwhelming instinct'/'ONS', 'ONS:Overwhelming Instinct:overwhelming instinct').
card_rarity('overwhelming instinct'/'ONS', 'Uncommon').
card_artist('overwhelming instinct'/'ONS', 'Ron Spears').
card_number('overwhelming instinct'/'ONS', '276').
card_flavor_text('overwhelming instinct'/'ONS', 'The biggest difference between a victory and a massacre is which side you\'re on.').
card_multiverse_id('overwhelming instinct'/'ONS', '34394').

card_in_set('pacifism', 'ONS').
card_original_type('pacifism'/'ONS', 'Enchant Creature').
card_original_text('pacifism'/'ONS', 'Enchanted creature can\'t attack or block.').
card_image_name('pacifism'/'ONS', 'pacifism').
card_uid('pacifism'/'ONS', 'ONS:Pacifism:pacifism').
card_rarity('pacifism'/'ONS', 'Common').
card_artist('pacifism'/'ONS', 'Matthew D. Wilson').
card_number('pacifism'/'ONS', '47').
card_flavor_text('pacifism'/'ONS', 'Even those born to battle could only lay their blades at Akroma\'s feet.').
card_multiverse_id('pacifism'/'ONS', '19729').

card_in_set('patriarch\'s bidding', 'ONS').
card_original_type('patriarch\'s bidding'/'ONS', 'Sorcery').
card_original_text('patriarch\'s bidding'/'ONS', 'Each player chooses a creature type. Each player returns all creature cards of a type chosen this way from his or her graveyard to play.').
card_first_print('patriarch\'s bidding', 'ONS').
card_image_name('patriarch\'s bidding'/'ONS', 'patriarch\'s bidding').
card_uid('patriarch\'s bidding'/'ONS', 'ONS:Patriarch\'s Bidding:patriarch\'s bidding').
card_rarity('patriarch\'s bidding'/'ONS', 'Rare').
card_artist('patriarch\'s bidding'/'ONS', 'Ben Thompson').
card_number('patriarch\'s bidding'/'ONS', '161').
card_flavor_text('patriarch\'s bidding'/'ONS', '\"Family plots are so convenient.\"\n—Cabal Patriarch').
card_multiverse_id('patriarch\'s bidding'/'ONS', '26747').

card_in_set('pearlspear courier', 'ONS').
card_original_type('pearlspear courier'/'ONS', 'Creature — Soldier').
card_original_text('pearlspear courier'/'ONS', 'You may choose not to untap Pearlspear Courier during your untap step.\n{2}{W}, {T}: As long as Pearlspear Courier remains tapped, target Soldier gets +2/+2 and has \"Attacking doesn\'t cause this creature to tap.\"').
card_first_print('pearlspear courier', 'ONS').
card_image_name('pearlspear courier'/'ONS', 'pearlspear courier').
card_uid('pearlspear courier'/'ONS', 'ONS:Pearlspear Courier:pearlspear courier').
card_rarity('pearlspear courier'/'ONS', 'Uncommon').
card_artist('pearlspear courier'/'ONS', 'Dany Orizio').
card_number('pearlspear courier'/'ONS', '48').
card_multiverse_id('pearlspear courier'/'ONS', '39596').

card_in_set('peer pressure', 'ONS').
card_original_type('peer pressure'/'ONS', 'Sorcery').
card_original_text('peer pressure'/'ONS', 'Choose a creature type. If you control more creatures of that type than any other player, you gain control of all creatures of that type. (This effect doesn\'t end at end of turn.)').
card_first_print('peer pressure', 'ONS').
card_image_name('peer pressure'/'ONS', 'peer pressure').
card_uid('peer pressure'/'ONS', 'ONS:Peer Pressure:peer pressure').
card_rarity('peer pressure'/'ONS', 'Rare').
card_artist('peer pressure'/'ONS', 'Edward P. Beard, Jr.').
card_number('peer pressure'/'ONS', '101').
card_multiverse_id('peer pressure'/'ONS', '39629').

card_in_set('piety charm', 'ONS').
card_original_type('piety charm'/'ONS', 'Instant').
card_original_text('piety charm'/'ONS', 'Choose one Destroy target enchant creature; or target Soldier gets +2/+2 until end of turn; or attacking doesn\'t cause creatures you control to tap this turn.').
card_first_print('piety charm', 'ONS').
card_image_name('piety charm'/'ONS', 'piety charm').
card_uid('piety charm'/'ONS', 'ONS:Piety Charm:piety charm').
card_rarity('piety charm'/'ONS', 'Common').
card_artist('piety charm'/'ONS', 'David Martin').
card_number('piety charm'/'ONS', '49').
card_multiverse_id('piety charm'/'ONS', '39433').

card_in_set('pinpoint avalanche', 'ONS').
card_original_type('pinpoint avalanche'/'ONS', 'Instant').
card_original_text('pinpoint avalanche'/'ONS', 'Pinpoint Avalanche deals 4 damage to target creature. The damage can\'t be prevented.').
card_first_print('pinpoint avalanche', 'ONS').
card_image_name('pinpoint avalanche'/'ONS', 'pinpoint avalanche').
card_uid('pinpoint avalanche'/'ONS', 'ONS:Pinpoint Avalanche:pinpoint avalanche').
card_rarity('pinpoint avalanche'/'ONS', 'Common').
card_artist('pinpoint avalanche'/'ONS', 'Darrell Riche').
card_number('pinpoint avalanche'/'ONS', '221').
card_flavor_text('pinpoint avalanche'/'ONS', '\"Some solve problems by thinking and talking. Others use rocks.\"\n—Toggo, goblin weaponsmith').
card_multiverse_id('pinpoint avalanche'/'ONS', '40537').

card_in_set('plains', 'ONS').
card_original_type('plains'/'ONS', 'Land').
card_original_text('plains'/'ONS', 'W').
card_image_name('plains'/'ONS', 'plains1').
card_uid('plains'/'ONS', 'ONS:Plains:plains1').
card_rarity('plains'/'ONS', 'Basic Land').
card_artist('plains'/'ONS', 'Rob Alexander').
card_number('plains'/'ONS', '331').
card_multiverse_id('plains'/'ONS', '40124').

card_in_set('plains', 'ONS').
card_original_type('plains'/'ONS', 'Land').
card_original_text('plains'/'ONS', 'W').
card_image_name('plains'/'ONS', 'plains2').
card_uid('plains'/'ONS', 'ONS:Plains:plains2').
card_rarity('plains'/'ONS', 'Basic Land').
card_artist('plains'/'ONS', 'Matthew Mitchell').
card_number('plains'/'ONS', '332').
card_multiverse_id('plains'/'ONS', '40123').

card_in_set('plains', 'ONS').
card_original_type('plains'/'ONS', 'Land').
card_original_text('plains'/'ONS', 'W').
card_image_name('plains'/'ONS', 'plains3').
card_uid('plains'/'ONS', 'ONS:Plains:plains3').
card_rarity('plains'/'ONS', 'Basic Land').
card_artist('plains'/'ONS', 'David Martin').
card_number('plains'/'ONS', '333').
card_multiverse_id('plains'/'ONS', '40122').

card_in_set('plains', 'ONS').
card_original_type('plains'/'ONS', 'Land').
card_original_text('plains'/'ONS', 'W').
card_image_name('plains'/'ONS', 'plains4').
card_uid('plains'/'ONS', 'ONS:Plains:plains4').
card_rarity('plains'/'ONS', 'Basic Land').
card_artist('plains'/'ONS', 'David Day').
card_number('plains'/'ONS', '334').
card_multiverse_id('plains'/'ONS', '40121').

card_in_set('polluted delta', 'ONS').
card_original_type('polluted delta'/'ONS', 'Land').
card_original_text('polluted delta'/'ONS', '{T}, Pay 1 life, Sacrifice Polluted Delta: Search your library for an island or swamp card and put it into play. Then shuffle your library.').
card_image_name('polluted delta'/'ONS', 'polluted delta').
card_uid('polluted delta'/'ONS', 'ONS:Polluted Delta:polluted delta').
card_rarity('polluted delta'/'ONS', 'Rare').
card_artist('polluted delta'/'ONS', 'Rob Alexander').
card_number('polluted delta'/'ONS', '321').
card_multiverse_id('polluted delta'/'ONS', '39504').

card_in_set('primal boost', 'ONS').
card_original_type('primal boost'/'ONS', 'Instant').
card_original_text('primal boost'/'ONS', 'Target creature gets +4/+4 until end of turn.\nCycling {2}{G} ({2}{G}, Discard this card from your hand: Draw a card.)\nWhen you cycle Primal Boost, you may have target creature get +1/+1 until end of turn.').
card_first_print('primal boost', 'ONS').
card_image_name('primal boost'/'ONS', 'primal boost').
card_uid('primal boost'/'ONS', 'ONS:Primal Boost:primal boost').
card_rarity('primal boost'/'ONS', 'Uncommon').
card_artist('primal boost'/'ONS', 'Eric Peterson').
card_number('primal boost'/'ONS', '277').
card_multiverse_id('primal boost'/'ONS', '41145').

card_in_set('profane prayers', 'ONS').
card_original_type('profane prayers'/'ONS', 'Sorcery').
card_original_text('profane prayers'/'ONS', 'Profane Prayers deals X damage to target creature or player and you gain X life, where X is the number of Clerics in play.').
card_first_print('profane prayers', 'ONS').
card_image_name('profane prayers'/'ONS', 'profane prayers').
card_uid('profane prayers'/'ONS', 'ONS:Profane Prayers:profane prayers').
card_rarity('profane prayers'/'ONS', 'Common').
card_artist('profane prayers'/'ONS', 'Alan Pollack').
card_number('profane prayers'/'ONS', '162').
card_flavor_text('profane prayers'/'ONS', '\"We have ways to make you talk, but you have nothing interesting to say.\"').
card_multiverse_id('profane prayers'/'ONS', '39845').

card_in_set('prowling pangolin', 'ONS').
card_original_type('prowling pangolin'/'ONS', 'Creature — Beast').
card_original_text('prowling pangolin'/'ONS', 'When Prowling Pangolin comes into play, any player may sacrifice two creatures. If a player does, sacrifice Prowling Pangolin.').
card_first_print('prowling pangolin', 'ONS').
card_image_name('prowling pangolin'/'ONS', 'prowling pangolin').
card_uid('prowling pangolin'/'ONS', 'ONS:Prowling Pangolin:prowling pangolin').
card_rarity('prowling pangolin'/'ONS', 'Uncommon').
card_artist('prowling pangolin'/'ONS', 'Heather Hudson').
card_number('prowling pangolin'/'ONS', '163').
card_flavor_text('prowling pangolin'/'ONS', 'It\'s always hungry, yet easily sated.').
card_multiverse_id('prowling pangolin'/'ONS', '26428').

card_in_set('psychic trance', 'ONS').
card_original_type('psychic trance'/'ONS', 'Instant').
card_original_text('psychic trance'/'ONS', 'Until end of turn, Wizards you control gain \"{T}: Counter target spell.\"').
card_first_print('psychic trance', 'ONS').
card_image_name('psychic trance'/'ONS', 'psychic trance').
card_uid('psychic trance'/'ONS', 'ONS:Psychic Trance:psychic trance').
card_rarity('psychic trance'/'ONS', 'Rare').
card_artist('psychic trance'/'ONS', 'Rebecca Guay').
card_number('psychic trance'/'ONS', '102').
card_flavor_text('psychic trance'/'ONS', 'The Riptide Project was perhaps the only school devoted to preventing the spread of knowledge.').
card_multiverse_id('psychic trance'/'ONS', '40173').

card_in_set('quicksilver dragon', 'ONS').
card_original_type('quicksilver dragon'/'ONS', 'Creature — Dragon').
card_original_text('quicksilver dragon'/'ONS', 'Flying\n{U}: If target spell has only one target and that target is Quicksilver Dragon, change that spell\'s target to another creature.\nMorph {4}{U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('quicksilver dragon', 'ONS').
card_image_name('quicksilver dragon'/'ONS', 'quicksilver dragon').
card_uid('quicksilver dragon'/'ONS', 'ONS:Quicksilver Dragon:quicksilver dragon').
card_rarity('quicksilver dragon'/'ONS', 'Rare').
card_artist('quicksilver dragon'/'ONS', 'Ron Spencer').
card_number('quicksilver dragon'/'ONS', '103').
card_multiverse_id('quicksilver dragon'/'ONS', '39539').

card_in_set('ravenous baloth', 'ONS').
card_original_type('ravenous baloth'/'ONS', 'Creature — Beast').
card_original_text('ravenous baloth'/'ONS', 'Sacrifice a Beast: You gain 4 life.').
card_image_name('ravenous baloth'/'ONS', 'ravenous baloth').
card_uid('ravenous baloth'/'ONS', 'ONS:Ravenous Baloth:ravenous baloth').
card_rarity('ravenous baloth'/'ONS', 'Rare').
card_artist('ravenous baloth'/'ONS', 'Arnie Swekel').
card_number('ravenous baloth'/'ONS', '278').
card_flavor_text('ravenous baloth'/'ONS', '\"All we know about the Krosan Forest we have learned from those few who made it out alive.\"\n—Elvish refugee').
card_multiverse_id('ravenous baloth'/'ONS', '12444').

card_in_set('read the runes', 'ONS').
card_original_type('read the runes'/'ONS', 'Instant').
card_original_text('read the runes'/'ONS', 'Draw X cards. For each card drawn this way, discard a card from your hand unless you sacrifice a permanent.').
card_first_print('read the runes', 'ONS').
card_image_name('read the runes'/'ONS', 'read the runes').
card_uid('read the runes'/'ONS', 'ONS:Read the Runes:read the runes').
card_rarity('read the runes'/'ONS', 'Rare').
card_artist('read the runes'/'ONS', 'Alan Pollack').
card_number('read the runes'/'ONS', '104').
card_flavor_text('read the runes'/'ONS', '\"The world is a puzzle, and the mind is its key.\"').
card_multiverse_id('read the runes'/'ONS', '9839').

card_in_set('reckless one', 'ONS').
card_original_type('reckless one'/'ONS', 'Creature — Goblin Avatar').
card_original_text('reckless one'/'ONS', 'Haste\nReckless One\'s power and toughness are each equal to the number of Goblins in play.').
card_first_print('reckless one', 'ONS').
card_image_name('reckless one'/'ONS', 'reckless one').
card_uid('reckless one'/'ONS', 'ONS:Reckless One:reckless one').
card_rarity('reckless one'/'ONS', 'Uncommon').
card_artist('reckless one'/'ONS', 'Ron Spencer').
card_number('reckless one'/'ONS', '222').
card_flavor_text('reckless one'/'ONS', '\"Release chaos with me!\"').
card_multiverse_id('reckless one'/'ONS', '39746').

card_in_set('reminisce', 'ONS').
card_original_type('reminisce'/'ONS', 'Sorcery').
card_original_text('reminisce'/'ONS', 'Target player shuffles his or her graveyard into his or her library.').
card_first_print('reminisce', 'ONS').
card_image_name('reminisce'/'ONS', 'reminisce').
card_uid('reminisce'/'ONS', 'ONS:Reminisce:reminisce').
card_rarity('reminisce'/'ONS', 'Uncommon').
card_artist('reminisce'/'ONS', 'Bradley Williams').
card_number('reminisce'/'ONS', '105').
card_flavor_text('reminisce'/'ONS', '\"Leave the door to the past even slightly ajar and it could be blown off its hinges.\"').
card_multiverse_id('reminisce'/'ONS', '41462').

card_in_set('renewed faith', 'ONS').
card_original_type('renewed faith'/'ONS', 'Instant').
card_original_text('renewed faith'/'ONS', 'You gain 6 life.\nCycling {1}{W} ({1}{W}, Discard this card from your hand: Draw a card.)\nWhen you cycle Renewed Faith, you may gain 2 life.').
card_first_print('renewed faith', 'ONS').
card_image_name('renewed faith'/'ONS', 'renewed faith').
card_uid('renewed faith'/'ONS', 'ONS:Renewed Faith:renewed faith').
card_rarity('renewed faith'/'ONS', 'Common').
card_artist('renewed faith'/'ONS', 'Dave Dorman').
card_number('renewed faith'/'ONS', '50').
card_multiverse_id('renewed faith'/'ONS', '41153').

card_in_set('righteous cause', 'ONS').
card_original_type('righteous cause'/'ONS', 'Enchantment').
card_original_text('righteous cause'/'ONS', 'Whenever a creature attacks, you gain 1 life.').
card_first_print('righteous cause', 'ONS').
card_image_name('righteous cause'/'ONS', 'righteous cause').
card_uid('righteous cause'/'ONS', 'ONS:Righteous Cause:righteous cause').
card_rarity('righteous cause'/'ONS', 'Uncommon').
card_artist('righteous cause'/'ONS', 'Scott M. Fischer').
card_number('righteous cause'/'ONS', '51').
card_flavor_text('righteous cause'/'ONS', '\"Until the world unites in vengeful fury and Phage is destroyed, I will not stay my hand.\"\n—Akroma, angelic avenger').
card_multiverse_id('righteous cause'/'ONS', '40656').

card_in_set('riptide biologist', 'ONS').
card_original_type('riptide biologist'/'ONS', 'Creature — Wizard').
card_original_text('riptide biologist'/'ONS', 'Protection from Beasts\nMorph {2}{U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('riptide biologist', 'ONS').
card_image_name('riptide biologist'/'ONS', 'riptide biologist').
card_uid('riptide biologist'/'ONS', 'ONS:Riptide Biologist:riptide biologist').
card_rarity('riptide biologist'/'ONS', 'Common').
card_artist('riptide biologist'/'ONS', 'Justin Sweet').
card_number('riptide biologist'/'ONS', '106').
card_flavor_text('riptide biologist'/'ONS', '\"I gave it two choices: life in the lab or death in the hunt.\"').
card_multiverse_id('riptide biologist'/'ONS', '39848').

card_in_set('riptide chronologist', 'ONS').
card_original_type('riptide chronologist'/'ONS', 'Creature — Wizard').
card_original_text('riptide chronologist'/'ONS', '{U}, Sacrifice Riptide Chronologist: Untap all creatures of the type of your choice.').
card_first_print('riptide chronologist', 'ONS').
card_image_name('riptide chronologist'/'ONS', 'riptide chronologist').
card_uid('riptide chronologist'/'ONS', 'ONS:Riptide Chronologist:riptide chronologist').
card_rarity('riptide chronologist'/'ONS', 'Uncommon').
card_artist('riptide chronologist'/'ONS', 'Roger Raupp').
card_number('riptide chronologist'/'ONS', '107').
card_flavor_text('riptide chronologist'/'ONS', 'The wizard consulted the Riptide Project about how to further his research. He wasn\'t prepared for what they told him.').
card_multiverse_id('riptide chronologist'/'ONS', '39887').

card_in_set('riptide entrancer', 'ONS').
card_original_type('riptide entrancer'/'ONS', 'Creature — Wizard').
card_original_text('riptide entrancer'/'ONS', 'Whenever Riptide Entrancer deals combat damage to a player, you may sacrifice it. If you do, gain control of target creature that player controls. (This effect doesn\'t end at end of turn.)\nMorph {U}{U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('riptide entrancer', 'ONS').
card_image_name('riptide entrancer'/'ONS', 'riptide entrancer').
card_uid('riptide entrancer'/'ONS', 'ONS:Riptide Entrancer:riptide entrancer').
card_rarity('riptide entrancer'/'ONS', 'Rare').
card_artist('riptide entrancer'/'ONS', 'Scott Hampton').
card_number('riptide entrancer'/'ONS', '108').
card_multiverse_id('riptide entrancer'/'ONS', '39727').

card_in_set('riptide laboratory', 'ONS').
card_original_type('riptide laboratory'/'ONS', 'Land').
card_original_text('riptide laboratory'/'ONS', '{T}: Add {1} to your mana pool.\n{1}{U}, {T}: Return target Wizard you control to its owner\'s hand.').
card_first_print('riptide laboratory', 'ONS').
card_image_name('riptide laboratory'/'ONS', 'riptide laboratory').
card_uid('riptide laboratory'/'ONS', 'ONS:Riptide Laboratory:riptide laboratory').
card_rarity('riptide laboratory'/'ONS', 'Rare').
card_artist('riptide laboratory'/'ONS', 'John Avon').
card_number('riptide laboratory'/'ONS', '322').
card_multiverse_id('riptide laboratory'/'ONS', '10704').

card_in_set('riptide replicator', 'ONS').
card_original_type('riptide replicator'/'ONS', 'Artifact').
card_original_text('riptide replicator'/'ONS', 'As Riptide Replicator comes into play, choose a color and a creature type.\nRiptide Replicator comes into play with X charge counters on it.\n{4}, {T}: Put an X/X creature token of the chosen color and type into play, where X is the number of charge counters on Riptide Replicator.').
card_first_print('riptide replicator', 'ONS').
card_image_name('riptide replicator'/'ONS', 'riptide replicator').
card_uid('riptide replicator'/'ONS', 'ONS:Riptide Replicator:riptide replicator').
card_rarity('riptide replicator'/'ONS', 'Rare').
card_artist('riptide replicator'/'ONS', 'Doug Chaffee').
card_number('riptide replicator'/'ONS', '309').
card_multiverse_id('riptide replicator'/'ONS', '40198').

card_in_set('riptide shapeshifter', 'ONS').
card_original_type('riptide shapeshifter'/'ONS', 'Creature — Shapeshifter').
card_original_text('riptide shapeshifter'/'ONS', '{2}{U}{U}, Sacrifice Riptide Shapeshifter: Choose a creature type. Reveal cards from the top of your library until you reveal a creature card of that type. Put that card into play and shuffle the rest into your library.').
card_first_print('riptide shapeshifter', 'ONS').
card_image_name('riptide shapeshifter'/'ONS', 'riptide shapeshifter').
card_uid('riptide shapeshifter'/'ONS', 'ONS:Riptide Shapeshifter:riptide shapeshifter').
card_rarity('riptide shapeshifter'/'ONS', 'Uncommon').
card_artist('riptide shapeshifter'/'ONS', 'Arnie Swekel').
card_number('riptide shapeshifter'/'ONS', '109').
card_multiverse_id('riptide shapeshifter'/'ONS', '25812').

card_in_set('risky move', 'ONS').
card_original_type('risky move'/'ONS', 'Enchantment').
card_original_text('risky move'/'ONS', 'At the beginning of each player\'s upkeep, that player gains control of Risky Move.\nWhen you gain control of Risky Move from another player, choose a creature you control and an opponent. Flip a coin. If you lose the flip, that opponent gains control of that creature.').
card_first_print('risky move', 'ONS').
card_image_name('risky move'/'ONS', 'risky move').
card_uid('risky move'/'ONS', 'ONS:Risky Move:risky move').
card_rarity('risky move'/'ONS', 'Rare').
card_artist('risky move'/'ONS', 'Jerry Tiritilli').
card_number('risky move'/'ONS', '223').
card_multiverse_id('risky move'/'ONS', '39663').

card_in_set('rorix bladewing', 'ONS').
card_original_type('rorix bladewing'/'ONS', 'Creature — Dragon Legend').
card_original_text('rorix bladewing'/'ONS', 'Flying, haste').
card_first_print('rorix bladewing', 'ONS').
card_image_name('rorix bladewing'/'ONS', 'rorix bladewing').
card_uid('rorix bladewing'/'ONS', 'ONS:Rorix Bladewing:rorix bladewing').
card_rarity('rorix bladewing'/'ONS', 'Rare').
card_artist('rorix bladewing'/'ONS', 'Darrell Riche').
card_number('rorix bladewing'/'ONS', '224').
card_flavor_text('rorix bladewing'/'ONS', 'In the smoldering ashes of Shiv, a few dragons strive to rebuild their native land. The rest seek any opportunity to restore the broken pride of their race.').
card_multiverse_id('rorix bladewing'/'ONS', '39859').

card_in_set('rotlung reanimator', 'ONS').
card_original_type('rotlung reanimator'/'ONS', 'Creature — Zombie Cleric').
card_original_text('rotlung reanimator'/'ONS', 'Whenever Rotlung Reanimator or another Cleric is put into a graveyard from play, put a 2/2 black Zombie creature token into play.').
card_first_print('rotlung reanimator', 'ONS').
card_image_name('rotlung reanimator'/'ONS', 'rotlung reanimator').
card_uid('rotlung reanimator'/'ONS', 'ONS:Rotlung Reanimator:rotlung reanimator').
card_rarity('rotlung reanimator'/'ONS', 'Rare').
card_artist('rotlung reanimator'/'ONS', 'Thomas M. Baxa').
card_number('rotlung reanimator'/'ONS', '164').
card_multiverse_id('rotlung reanimator'/'ONS', '39640').

card_in_set('rummaging wizard', 'ONS').
card_original_type('rummaging wizard'/'ONS', 'Creature — Wizard').
card_original_text('rummaging wizard'/'ONS', '{2}{U}: Look at the top card of your library. You may put that card into your graveyard.').
card_first_print('rummaging wizard', 'ONS').
card_image_name('rummaging wizard'/'ONS', 'rummaging wizard').
card_uid('rummaging wizard'/'ONS', 'ONS:Rummaging Wizard:rummaging wizard').
card_rarity('rummaging wizard'/'ONS', 'Uncommon').
card_artist('rummaging wizard'/'ONS', 'Jerry Tiritilli').
card_number('rummaging wizard'/'ONS', '110').
card_flavor_text('rummaging wizard'/'ONS', '\"I\'ve got everything you\'d ever need right here. Just give me some time to find it.\"').
card_multiverse_id('rummaging wizard'/'ONS', '40601').

card_in_set('run wild', 'ONS').
card_original_type('run wild'/'ONS', 'Instant').
card_original_text('run wild'/'ONS', 'Until end of turn, target creature gains trample and \"{G}: Regenerate this creature.\"').
card_first_print('run wild', 'ONS').
card_image_name('run wild'/'ONS', 'run wild').
card_uid('run wild'/'ONS', 'ONS:Run Wild:run wild').
card_rarity('run wild'/'ONS', 'Uncommon').
card_artist('run wild'/'ONS', 'Alan Pollack').
card_number('run wild'/'ONS', '279').
card_flavor_text('run wild'/'ONS', 'Wirewood\'s beasts didn\'t seem to mind when the elves moved in. In fact, they hardly noticed them underfoot.').
card_multiverse_id('run wild'/'ONS', '41160').

card_in_set('sage aven', 'ONS').
card_original_type('sage aven'/'ONS', 'Creature — Bird Wizard').
card_original_text('sage aven'/'ONS', 'Flying\nWhen Sage Aven comes into play, look at the top four cards of your library, then put them back in any order.').
card_first_print('sage aven', 'ONS').
card_image_name('sage aven'/'ONS', 'sage aven').
card_uid('sage aven'/'ONS', 'ONS:Sage Aven:sage aven').
card_rarity('sage aven'/'ONS', 'Common').
card_artist('sage aven'/'ONS', 'Randy Gallegos').
card_number('sage aven'/'ONS', '111').
card_flavor_text('sage aven'/'ONS', 'From their mountain aeries, aven scholars see far more than the distant horizon.').
card_multiverse_id('sage aven'/'ONS', '39440').

card_in_set('sandskin', 'ONS').
card_original_type('sandskin'/'ONS', 'Enchant Creature').
card_original_text('sandskin'/'ONS', 'Prevent all combat damage that would be dealt to and dealt by enchanted creature.').
card_first_print('sandskin', 'ONS').
card_image_name('sandskin'/'ONS', 'sandskin').
card_uid('sandskin'/'ONS', 'ONS:Sandskin:sandskin').
card_rarity('sandskin'/'ONS', 'Common').
card_artist('sandskin'/'ONS', 'Glen Angus').
card_number('sandskin'/'ONS', '52').
card_flavor_text('sandskin'/'ONS', '\"Those who live by the sword will die by the sword. I choose to do neither.\"').
card_multiverse_id('sandskin'/'ONS', '41170').

card_in_set('screaming seahawk', 'ONS').
card_original_type('screaming seahawk'/'ONS', 'Creature — Bird').
card_original_text('screaming seahawk'/'ONS', 'Flying\nWhen Screaming Seahawk comes into play, you may search your library for a card named Screaming Seahawk, reveal it, and put it into your hand. If you do, shuffle your library.').
card_first_print('screaming seahawk', 'ONS').
card_image_name('screaming seahawk'/'ONS', 'screaming seahawk').
card_uid('screaming seahawk'/'ONS', 'ONS:Screaming Seahawk:screaming seahawk').
card_rarity('screaming seahawk'/'ONS', 'Common').
card_artist('screaming seahawk'/'ONS', 'Heather Hudson').
card_number('screaming seahawk'/'ONS', '112').
card_multiverse_id('screaming seahawk'/'ONS', '39445').

card_in_set('screeching buzzard', 'ONS').
card_original_type('screeching buzzard'/'ONS', 'Creature — Bird').
card_original_text('screeching buzzard'/'ONS', 'Flying\nWhen Screeching Buzzard is put into a graveyard from play, each opponent discards a card from his or her hand.').
card_first_print('screeching buzzard', 'ONS').
card_image_name('screeching buzzard'/'ONS', 'screeching buzzard').
card_uid('screeching buzzard'/'ONS', 'ONS:Screeching Buzzard:screeching buzzard').
card_rarity('screeching buzzard'/'ONS', 'Common').
card_artist('screeching buzzard'/'ONS', 'Heather Hudson').
card_number('screeching buzzard'/'ONS', '165').
card_multiverse_id('screeching buzzard'/'ONS', '39455').

card_in_set('sea\'s claim', 'ONS').
card_original_type('sea\'s claim'/'ONS', 'Enchant Land').
card_original_text('sea\'s claim'/'ONS', 'Enchanted land is an island.').
card_first_print('sea\'s claim', 'ONS').
card_image_name('sea\'s claim'/'ONS', 'sea\'s claim').
card_uid('sea\'s claim'/'ONS', 'ONS:Sea\'s Claim:sea\'s claim').
card_rarity('sea\'s claim'/'ONS', 'Common').
card_artist('sea\'s claim'/'ONS', 'Alan Pollack').
card_number('sea\'s claim'/'ONS', '113').
card_flavor_text('sea\'s claim'/'ONS', '\"My vengeance will drown my enemies as my storms drown the desert.\"\n—Ixidor, reality sculptor').
card_multiverse_id('sea\'s claim'/'ONS', '39446').

card_in_set('searing flesh', 'ONS').
card_original_type('searing flesh'/'ONS', 'Sorcery').
card_original_text('searing flesh'/'ONS', 'Searing Flesh deals 7 damage to target opponent.').
card_first_print('searing flesh', 'ONS').
card_image_name('searing flesh'/'ONS', 'searing flesh').
card_uid('searing flesh'/'ONS', 'ONS:Searing Flesh:searing flesh').
card_rarity('searing flesh'/'ONS', 'Uncommon').
card_artist('searing flesh'/'ONS', 'Pete Venters').
card_number('searing flesh'/'ONS', '225').
card_flavor_text('searing flesh'/'ONS', 'Zemoo found out the hard way that \"inflammable\" and \"flammable\" mean the same thing.').
card_multiverse_id('searing flesh'/'ONS', '35386').

card_in_set('seaside haven', 'ONS').
card_original_type('seaside haven'/'ONS', 'Land').
card_original_text('seaside haven'/'ONS', '{T}: Add {1} to your mana pool.\n{W}{U}, {T}, Sacrifice a Bird: Draw a card.').
card_first_print('seaside haven', 'ONS').
card_image_name('seaside haven'/'ONS', 'seaside haven').
card_uid('seaside haven'/'ONS', 'ONS:Seaside Haven:seaside haven').
card_rarity('seaside haven'/'ONS', 'Uncommon').
card_artist('seaside haven'/'ONS', 'Mark Brill').
card_number('seaside haven'/'ONS', '323').
card_multiverse_id('seaside haven'/'ONS', '40539').

card_in_set('secluded steppe', 'ONS').
card_original_type('secluded steppe'/'ONS', 'Land').
card_original_text('secluded steppe'/'ONS', 'Secluded Steppe comes into play tapped.\n{T}: Add {W} to your mana pool.\nCycling {W} ({W}, Discard this card from your hand: Draw a card.)').
card_first_print('secluded steppe', 'ONS').
card_image_name('secluded steppe'/'ONS', 'secluded steppe').
card_uid('secluded steppe'/'ONS', 'ONS:Secluded Steppe:secluded steppe').
card_rarity('secluded steppe'/'ONS', 'Common').
card_artist('secluded steppe'/'ONS', 'Heather Hudson').
card_number('secluded steppe'/'ONS', '324').
card_multiverse_id('secluded steppe'/'ONS', '41137').

card_in_set('serpentine basilisk', 'ONS').
card_original_type('serpentine basilisk'/'ONS', 'Creature — Basilisk').
card_original_text('serpentine basilisk'/'ONS', 'Whenever Serpentine Basilisk deals combat damage to a creature, destroy that creature at end of combat.\nMorph {1}{G}{G} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('serpentine basilisk', 'ONS').
card_image_name('serpentine basilisk'/'ONS', 'serpentine basilisk').
card_uid('serpentine basilisk'/'ONS', 'ONS:Serpentine Basilisk:serpentine basilisk').
card_rarity('serpentine basilisk'/'ONS', 'Uncommon').
card_artist('serpentine basilisk'/'ONS', 'Franz Vohwinkel').
card_number('serpentine basilisk'/'ONS', '280').
card_multiverse_id('serpentine basilisk'/'ONS', '39731').

card_in_set('severed legion', 'ONS').
card_original_type('severed legion'/'ONS', 'Creature — Zombie').
card_original_text('severed legion'/'ONS', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)').
card_first_print('severed legion', 'ONS').
card_image_name('severed legion'/'ONS', 'severed legion').
card_uid('severed legion'/'ONS', 'ONS:Severed Legion:severed legion').
card_rarity('severed legion'/'ONS', 'Common').
card_artist('severed legion'/'ONS', 'Dany Orizio').
card_number('severed legion'/'ONS', '166').
card_flavor_text('severed legion'/'ONS', 'No one in Aphetto answers a knock at the door after sundown.').
card_multiverse_id('severed legion'/'ONS', '13204').

card_in_set('shade\'s breath', 'ONS').
card_original_type('shade\'s breath'/'ONS', 'Instant').
card_original_text('shade\'s breath'/'ONS', 'Until end of turn, each creature you control becomes black, its creature type becomes Shade, and it gains \"{B}: This creature gets +1/+1 until end of turn.\"').
card_first_print('shade\'s breath', 'ONS').
card_image_name('shade\'s breath'/'ONS', 'shade\'s breath').
card_uid('shade\'s breath'/'ONS', 'ONS:Shade\'s Breath:shade\'s breath').
card_rarity('shade\'s breath'/'ONS', 'Uncommon').
card_artist('shade\'s breath'/'ONS', 'Franz Vohwinkel').
card_number('shade\'s breath'/'ONS', '167').
card_flavor_text('shade\'s breath'/'ONS', 'The grim business of the Coliseum casts long shadows.').
card_multiverse_id('shade\'s breath'/'ONS', '41279').

card_in_set('shaleskin bruiser', 'ONS').
card_original_type('shaleskin bruiser'/'ONS', 'Creature — Beast').
card_original_text('shaleskin bruiser'/'ONS', 'Trample\nWhenever Shaleskin Bruiser attacks, it gets +3/+0 until end of turn for each other attacking Beast.').
card_first_print('shaleskin bruiser', 'ONS').
card_image_name('shaleskin bruiser'/'ONS', 'shaleskin bruiser').
card_uid('shaleskin bruiser'/'ONS', 'ONS:Shaleskin Bruiser:shaleskin bruiser').
card_rarity('shaleskin bruiser'/'ONS', 'Uncommon').
card_artist('shaleskin bruiser'/'ONS', 'Mark Zug').
card_number('shaleskin bruiser'/'ONS', '226').
card_flavor_text('shaleskin bruiser'/'ONS', 'Its only predators are the elements.').
card_multiverse_id('shaleskin bruiser'/'ONS', '39656').

card_in_set('shared triumph', 'ONS').
card_original_type('shared triumph'/'ONS', 'Enchantment').
card_original_text('shared triumph'/'ONS', 'As Shared Triumph comes into play, choose a creature type.\nCreatures of the chosen type get +1/+1.').
card_first_print('shared triumph', 'ONS').
card_image_name('shared triumph'/'ONS', 'shared triumph').
card_uid('shared triumph'/'ONS', 'ONS:Shared Triumph:shared triumph').
card_rarity('shared triumph'/'ONS', 'Rare').
card_artist('shared triumph'/'ONS', 'Mark Brill').
card_number('shared triumph'/'ONS', '53').
card_flavor_text('shared triumph'/'ONS', '\"Win together, die alone.\"').
card_multiverse_id('shared triumph'/'ONS', '40127').

card_in_set('shepherd of rot', 'ONS').
card_original_type('shepherd of rot'/'ONS', 'Creature — Zombie Cleric').
card_original_text('shepherd of rot'/'ONS', '{T}: Each player loses 1 life for each Zombie in play.').
card_first_print('shepherd of rot', 'ONS').
card_image_name('shepherd of rot'/'ONS', 'shepherd of rot').
card_uid('shepherd of rot'/'ONS', 'ONS:Shepherd of Rot:shepherd of rot').
card_rarity('shepherd of rot'/'ONS', 'Common').
card_artist('shepherd of rot'/'ONS', 'Greg Staples').
card_number('shepherd of rot'/'ONS', '168').
card_flavor_text('shepherd of rot'/'ONS', '\"My priests swore loyalty to the Cabal not until death, but beyond it.\"\n—Cabal Patriarch').
card_multiverse_id('shepherd of rot'/'ONS', '39464').

card_in_set('shieldmage elder', 'ONS').
card_original_type('shieldmage elder'/'ONS', 'Creature — Cleric Wizard').
card_original_text('shieldmage elder'/'ONS', 'Tap two untapped Clerics you control: Prevent all damage target creature would deal this turn.\nTap two untapped Wizards you control: Prevent all damage target spell would deal this turn.').
card_first_print('shieldmage elder', 'ONS').
card_image_name('shieldmage elder'/'ONS', 'shieldmage elder').
card_uid('shieldmage elder'/'ONS', 'ONS:Shieldmage Elder:shieldmage elder').
card_rarity('shieldmage elder'/'ONS', 'Uncommon').
card_artist('shieldmage elder'/'ONS', 'Christopher Moeller').
card_number('shieldmage elder'/'ONS', '54').
card_flavor_text('shieldmage elder'/'ONS', '\"You may fight if I deem your cause worthy.\"').
card_multiverse_id('shieldmage elder'/'ONS', '39879').

card_in_set('shock', 'ONS').
card_original_type('shock'/'ONS', 'Instant').
card_original_text('shock'/'ONS', 'Shock deals 2 damage to target creature or player.').
card_image_name('shock'/'ONS', 'shock').
card_uid('shock'/'ONS', 'ONS:Shock:shock').
card_rarity('shock'/'ONS', 'Common').
card_artist('shock'/'ONS', 'Edward P. Beard, Jr.').
card_number('shock'/'ONS', '227').
card_flavor_text('shock'/'ONS', '\"I love lightning It\'s my best invention since the rock.\"\n—Toggo, goblin weaponsmith').
card_multiverse_id('shock'/'ONS', '39482').

card_in_set('sigil of the new dawn', 'ONS').
card_original_type('sigil of the new dawn'/'ONS', 'Enchantment').
card_original_text('sigil of the new dawn'/'ONS', 'Whenever a creature is put into your graveyard from play, you may pay {1}{W}. If you do, return that card to your hand.').
card_first_print('sigil of the new dawn', 'ONS').
card_image_name('sigil of the new dawn'/'ONS', 'sigil of the new dawn').
card_uid('sigil of the new dawn'/'ONS', 'ONS:Sigil of the New Dawn:sigil of the new dawn').
card_rarity('sigil of the new dawn'/'ONS', 'Rare').
card_artist('sigil of the new dawn'/'ONS', 'Tony Szczudlo').
card_number('sigil of the new dawn'/'ONS', '55').
card_flavor_text('sigil of the new dawn'/'ONS', '\"We lay our dead to rest in a beautiful bed, in hopes that the Ancestor will think it a fit cradle for the next life.\"\n—Mystic elder').
card_multiverse_id('sigil of the new dawn'/'ONS', '39605').

card_in_set('silent specter', 'ONS').
card_original_type('silent specter'/'ONS', 'Creature — Specter').
card_original_text('silent specter'/'ONS', 'Flying\nWhenever Silent Specter deals combat damage to a player, that player discards two cards from his or her hand.\nMorph {3}{B}{B} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_image_name('silent specter'/'ONS', 'silent specter').
card_uid('silent specter'/'ONS', 'ONS:Silent Specter:silent specter').
card_rarity('silent specter'/'ONS', 'Rare').
card_artist('silent specter'/'ONS', 'Daren Bader').
card_number('silent specter'/'ONS', '169').
card_multiverse_id('silent specter'/'ONS', '41280').

card_in_set('silklash spider', 'ONS').
card_original_type('silklash spider'/'ONS', 'Creature — Spider').
card_original_text('silklash spider'/'ONS', 'Silklash Spider may block as though it had flying.\n{X}{G}{G}: Silklash Spider deals X damage to each creature with flying.').
card_first_print('silklash spider', 'ONS').
card_image_name('silklash spider'/'ONS', 'silklash spider').
card_uid('silklash spider'/'ONS', 'ONS:Silklash Spider:silklash spider').
card_rarity('silklash spider'/'ONS', 'Rare').
card_artist('silklash spider'/'ONS', 'Iain McCaig').
card_number('silklash spider'/'ONS', '281').
card_flavor_text('silklash spider'/'ONS', 'The only thing that flies over the Krosan Forest is the wind.').
card_multiverse_id('silklash spider'/'ONS', '39687').

card_in_set('silvos, rogue elemental', 'ONS').
card_original_type('silvos, rogue elemental'/'ONS', 'Creature — Elemental Legend').
card_original_text('silvos, rogue elemental'/'ONS', 'Trample\n{G}: Regenerate Silvos, Rogue Elemental.').
card_first_print('silvos, rogue elemental', 'ONS').
card_image_name('silvos, rogue elemental'/'ONS', 'silvos, rogue elemental').
card_uid('silvos, rogue elemental'/'ONS', 'ONS:Silvos, Rogue Elemental:silvos, rogue elemental').
card_rarity('silvos, rogue elemental'/'ONS', 'Rare').
card_artist('silvos, rogue elemental'/'ONS', 'Carl Critchlow').
card_number('silvos, rogue elemental'/'ONS', '282').
card_flavor_text('silvos, rogue elemental'/'ONS', 'He was born of the Mirari, thrust out of his homeland before he was even aware. Left without purpose or meaning, he found both in the pits.').
card_multiverse_id('silvos, rogue elemental'/'ONS', '39860').

card_in_set('skirk commando', 'ONS').
card_original_type('skirk commando'/'ONS', 'Creature — Goblin').
card_original_text('skirk commando'/'ONS', 'Whenever Skirk Commando deals combat damage to a player, you may have it deal 2 damage to target creature that player controls.\nMorph {2}{R} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('skirk commando', 'ONS').
card_image_name('skirk commando'/'ONS', 'skirk commando').
card_uid('skirk commando'/'ONS', 'ONS:Skirk Commando:skirk commando').
card_rarity('skirk commando'/'ONS', 'Common').
card_artist('skirk commando'/'ONS', 'Dave Dorman').
card_number('skirk commando'/'ONS', '228').
card_multiverse_id('skirk commando'/'ONS', '12417').

card_in_set('skirk fire marshal', 'ONS').
card_original_type('skirk fire marshal'/'ONS', 'Creature — Goblin Lord').
card_original_text('skirk fire marshal'/'ONS', 'Protection from red\nTap five untapped Goblins you control: Skirk Fire Marshal deals 10 damage to each creature and each player.').
card_first_print('skirk fire marshal', 'ONS').
card_image_name('skirk fire marshal'/'ONS', 'skirk fire marshal').
card_uid('skirk fire marshal'/'ONS', 'ONS:Skirk Fire Marshal:skirk fire marshal').
card_rarity('skirk fire marshal'/'ONS', 'Rare').
card_artist('skirk fire marshal'/'ONS', 'Greg & Tim Hildebrandt').
card_number('skirk fire marshal'/'ONS', '229').
card_flavor_text('skirk fire marshal'/'ONS', 'He\'s boss because he\'s smart enough to get out of the way.').
card_multiverse_id('skirk fire marshal'/'ONS', '40194').

card_in_set('skirk prospector', 'ONS').
card_original_type('skirk prospector'/'ONS', 'Creature — Goblin').
card_original_text('skirk prospector'/'ONS', 'Sacrifice a Goblin: Add {R} to your mana pool.').
card_first_print('skirk prospector', 'ONS').
card_image_name('skirk prospector'/'ONS', 'skirk prospector').
card_uid('skirk prospector'/'ONS', 'ONS:Skirk Prospector:skirk prospector').
card_rarity('skirk prospector'/'ONS', 'Common').
card_artist('skirk prospector'/'ONS', 'Doug Chaffee').
card_number('skirk prospector'/'ONS', '230').
card_flavor_text('skirk prospector'/'ONS', '\"I like goblins. They make funny little popping sounds when they die.\"\n—Braids, dementia summoner').
card_multiverse_id('skirk prospector'/'ONS', '39563').

card_in_set('skittish valesk', 'ONS').
card_original_type('skittish valesk'/'ONS', 'Creature — Beast').
card_original_text('skittish valesk'/'ONS', 'At the beginning of your upkeep, flip a coin. If you lose the flip, turn Skittish Valesk face down.\nMorph {5}{R} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('skittish valesk', 'ONS').
card_image_name('skittish valesk'/'ONS', 'skittish valesk').
card_uid('skittish valesk'/'ONS', 'ONS:Skittish Valesk:skittish valesk').
card_rarity('skittish valesk'/'ONS', 'Uncommon').
card_artist('skittish valesk'/'ONS', 'Alan Pollack').
card_number('skittish valesk'/'ONS', '231').
card_multiverse_id('skittish valesk'/'ONS', '39754').

card_in_set('slate of ancestry', 'ONS').
card_original_type('slate of ancestry'/'ONS', 'Artifact').
card_original_text('slate of ancestry'/'ONS', '{4}, {T}, Discard your hand: Draw a card for each creature you control.').
card_first_print('slate of ancestry', 'ONS').
card_image_name('slate of ancestry'/'ONS', 'slate of ancestry').
card_uid('slate of ancestry'/'ONS', 'ONS:Slate of Ancestry:slate of ancestry').
card_rarity('slate of ancestry'/'ONS', 'Rare').
card_artist('slate of ancestry'/'ONS', 'Corey D. Macourek').
card_number('slate of ancestry'/'ONS', '310').
card_flavor_text('slate of ancestry'/'ONS', 'The pattern of life can be studied like a book, if you know how to read it.').
card_multiverse_id('slate of ancestry'/'ONS', '39696').

card_in_set('slice and dice', 'ONS').
card_original_type('slice and dice'/'ONS', 'Sorcery').
card_original_text('slice and dice'/'ONS', 'Slice and Dice deals 4 damage to each creature.\nCycling {2}{R} ({2}{R}, Discard this card from your hand: Draw a card.)\nWhen you cycle Slice and Dice, you may have it deal 1 damage to each creature.').
card_image_name('slice and dice'/'ONS', 'slice and dice').
card_uid('slice and dice'/'ONS', 'ONS:Slice and Dice:slice and dice').
card_rarity('slice and dice'/'ONS', 'Uncommon').
card_artist('slice and dice'/'ONS', 'Mark Brill').
card_number('slice and dice'/'ONS', '232').
card_multiverse_id('slice and dice'/'ONS', '41171').

card_in_set('slipstream eel', 'ONS').
card_original_type('slipstream eel'/'ONS', 'Creature — Beast').
card_original_text('slipstream eel'/'ONS', 'Slipstream Eel can\'t attack unless defending player controls an island.\nCycling {1}{U} ({1}{U}, Discard this card from your hand: Draw a card.)').
card_first_print('slipstream eel', 'ONS').
card_image_name('slipstream eel'/'ONS', 'slipstream eel').
card_uid('slipstream eel'/'ONS', 'ONS:Slipstream Eel:slipstream eel').
card_rarity('slipstream eel'/'ONS', 'Common').
card_artist('slipstream eel'/'ONS', 'Mark Tedin').
card_number('slipstream eel'/'ONS', '114').
card_flavor_text('slipstream eel'/'ONS', '\"It\'s a fine way to travel, if you don\'t mind the smell.\"').
card_multiverse_id('slipstream eel'/'ONS', '39706').

card_in_set('smother', 'ONS').
card_original_type('smother'/'ONS', 'Instant').
card_original_text('smother'/'ONS', 'Destroy target creature with converted mana cost 3 or less. It can\'t be regenerated.').
card_image_name('smother'/'ONS', 'smother').
card_uid('smother'/'ONS', 'ONS:Smother:smother').
card_rarity('smother'/'ONS', 'Uncommon').
card_artist('smother'/'ONS', 'Carl Critchlow').
card_number('smother'/'ONS', '170').
card_flavor_text('smother'/'ONS', '\"I can\'t hear them scream, but at least I don\'t have to listen to them beg.\"\n—Phage the Untouchable').
card_multiverse_id('smother'/'ONS', '41023').

card_in_set('snapping thragg', 'ONS').
card_original_type('snapping thragg'/'ONS', 'Creature — Beast').
card_original_text('snapping thragg'/'ONS', 'Whenever Snapping Thragg deals combat damage to a player, you may have it deal 3 damage to target creature that player controls.\nMorph {4}{R}{R} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('snapping thragg', 'ONS').
card_image_name('snapping thragg'/'ONS', 'snapping thragg').
card_uid('snapping thragg'/'ONS', 'ONS:Snapping Thragg:snapping thragg').
card_rarity('snapping thragg'/'ONS', 'Uncommon').
card_artist('snapping thragg'/'ONS', 'Iain McCaig').
card_number('snapping thragg'/'ONS', '233').
card_multiverse_id('snapping thragg'/'ONS', '39729').

card_in_set('snarling undorak', 'ONS').
card_original_type('snarling undorak'/'ONS', 'Creature — Beast').
card_original_text('snarling undorak'/'ONS', '{2}{G}: Target Beast gets +1/+1 until end of turn.\nMorph {1}{G}{G} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('snarling undorak', 'ONS').
card_image_name('snarling undorak'/'ONS', 'snarling undorak').
card_uid('snarling undorak'/'ONS', 'ONS:Snarling Undorak:snarling undorak').
card_rarity('snarling undorak'/'ONS', 'Common').
card_artist('snarling undorak'/'ONS', 'Justin Sweet').
card_number('snarling undorak'/'ONS', '283').
card_flavor_text('snarling undorak'/'ONS', 'Most creatures in the Krosan Forest feared the Mirari\'s power. A few fed upon it.').
card_multiverse_id('snarling undorak'/'ONS', '39492').

card_in_set('solar blast', 'ONS').
card_original_type('solar blast'/'ONS', 'Instant').
card_original_text('solar blast'/'ONS', 'Solar Blast deals 3 damage to target creature or player.\nCycling {1}{R}{R} ({1}{R}{R}, Discard this card from your hand: Draw a card.)\nWhen you cycle Solar Blast, you may have it deal 1 damage to target creature or player.').
card_first_print('solar blast', 'ONS').
card_image_name('solar blast'/'ONS', 'solar blast').
card_uid('solar blast'/'ONS', 'ONS:Solar Blast:solar blast').
card_rarity('solar blast'/'ONS', 'Common').
card_artist('solar blast'/'ONS', 'Greg Staples').
card_number('solar blast'/'ONS', '234').
card_multiverse_id('solar blast'/'ONS', '41143').

card_in_set('soulless one', 'ONS').
card_original_type('soulless one'/'ONS', 'Creature — Zombie Avatar').
card_original_text('soulless one'/'ONS', 'Soulless One\'s power and toughness are each equal to the number of Zombies in play plus the number of Zombie cards in all graveyards.').
card_first_print('soulless one', 'ONS').
card_image_name('soulless one'/'ONS', 'soulless one').
card_uid('soulless one'/'ONS', 'ONS:Soulless One:soulless one').
card_rarity('soulless one'/'ONS', 'Uncommon').
card_artist('soulless one'/'ONS', 'Thomas M. Baxa').
card_number('soulless one'/'ONS', '171').
card_flavor_text('soulless one'/'ONS', '\"Surrender your soul to me.\"').
card_multiverse_id('soulless one'/'ONS', '39745').

card_in_set('sparksmith', 'ONS').
card_original_type('sparksmith'/'ONS', 'Creature — Goblin').
card_original_text('sparksmith'/'ONS', '{T}: Sparksmith deals X damage to target creature and X damage to you, where X is the number of Goblins in play.').
card_image_name('sparksmith'/'ONS', 'sparksmith').
card_uid('sparksmith'/'ONS', 'ONS:Sparksmith:sparksmith').
card_rarity('sparksmith'/'ONS', 'Common').
card_artist('sparksmith'/'ONS', 'Jim Nelson').
card_number('sparksmith'/'ONS', '235').
card_flavor_text('sparksmith'/'ONS', '\"If it didn\'t hurt, how would I know it worked?\"').
card_multiverse_id('sparksmith'/'ONS', '39476').

card_in_set('spined basher', 'ONS').
card_original_type('spined basher'/'ONS', 'Creature — Zombie Beast').
card_original_text('spined basher'/'ONS', 'Morph {2}{B} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('spined basher', 'ONS').
card_image_name('spined basher'/'ONS', 'spined basher').
card_uid('spined basher'/'ONS', 'ONS:Spined Basher:spined basher').
card_rarity('spined basher'/'ONS', 'Common').
card_artist('spined basher'/'ONS', 'Thomas M. Baxa').
card_number('spined basher'/'ONS', '172').
card_flavor_text('spined basher'/'ONS', '\"How cute. Does it do any other tricks?\"\n—Braids, dementia summoner').
card_multiverse_id('spined basher'/'ONS', '39725').

card_in_set('spitfire handler', 'ONS').
card_original_type('spitfire handler'/'ONS', 'Creature — Goblin').
card_original_text('spitfire handler'/'ONS', 'Spitfire Handler can\'t block creatures with power greater than Spitfire Handler\'s power.\n{R}: Spitfire Handler gets +1/+0 until end of turn.').
card_first_print('spitfire handler', 'ONS').
card_image_name('spitfire handler'/'ONS', 'spitfire handler').
card_uid('spitfire handler'/'ONS', 'ONS:Spitfire Handler:spitfire handler').
card_rarity('spitfire handler'/'ONS', 'Uncommon').
card_artist('spitfire handler'/'ONS', 'Jim Nelson').
card_number('spitfire handler'/'ONS', '236').
card_flavor_text('spitfire handler'/'ONS', '\"Wait \'til Toggo sees this!\"').
card_multiverse_id('spitfire handler'/'ONS', '39655').

card_in_set('spitting gourna', 'ONS').
card_original_type('spitting gourna'/'ONS', 'Creature — Beast').
card_original_text('spitting gourna'/'ONS', 'Spitting Gourna may block as though it had flying.\nMorph {4}{G} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('spitting gourna', 'ONS').
card_image_name('spitting gourna'/'ONS', 'spitting gourna').
card_uid('spitting gourna'/'ONS', 'ONS:Spitting Gourna:spitting gourna').
card_rarity('spitting gourna'/'ONS', 'Common').
card_artist('spitting gourna'/'ONS', 'Heather Hudson').
card_number('spitting gourna'/'ONS', '284').
card_multiverse_id('spitting gourna'/'ONS', '39720').

card_in_set('spurred wolverine', 'ONS').
card_original_type('spurred wolverine'/'ONS', 'Creature — Beast').
card_original_text('spurred wolverine'/'ONS', 'Tap two untapped Beasts you control: Target creature gains first strike until end of turn.').
card_first_print('spurred wolverine', 'ONS').
card_image_name('spurred wolverine'/'ONS', 'spurred wolverine').
card_uid('spurred wolverine'/'ONS', 'ONS:Spurred Wolverine:spurred wolverine').
card_rarity('spurred wolverine'/'ONS', 'Common').
card_artist('spurred wolverine'/'ONS', 'Daren Bader').
card_number('spurred wolverine'/'ONS', '237').
card_flavor_text('spurred wolverine'/'ONS', 'After a few painful experiences, goblins learned not to pick their noses around the beasts.').
card_multiverse_id('spurred wolverine'/'ONS', '39835').

card_in_set('spy network', 'ONS').
card_original_type('spy network'/'ONS', 'Instant').
card_original_text('spy network'/'ONS', 'Look at target player\'s hand, the top card of that player\'s library, and any face-down creatures he or she controls. Look at the top four cards of your library, then put them back in any order.').
card_first_print('spy network', 'ONS').
card_image_name('spy network'/'ONS', 'spy network').
card_uid('spy network'/'ONS', 'ONS:Spy Network:spy network').
card_rarity('spy network'/'ONS', 'Common').
card_artist('spy network'/'ONS', 'Ron Spears').
card_number('spy network'/'ONS', '115').
card_multiverse_id('spy network'/'ONS', '39444').

card_in_set('stag beetle', 'ONS').
card_original_type('stag beetle'/'ONS', 'Creature — Insect').
card_original_text('stag beetle'/'ONS', 'Stag Beetle comes into play with X +1/+1 counters on it, where X is the number of other creatures in play.').
card_first_print('stag beetle', 'ONS').
card_image_name('stag beetle'/'ONS', 'stag beetle').
card_uid('stag beetle'/'ONS', 'ONS:Stag Beetle:stag beetle').
card_rarity('stag beetle'/'ONS', 'Rare').
card_artist('stag beetle'/'ONS', 'Anthony S. Waters').
card_number('stag beetle'/'ONS', '285').
card_flavor_text('stag beetle'/'ONS', 'Its voice crackles with the hum of a thousand wings beating at once.').
card_multiverse_id('stag beetle'/'ONS', '39676').

card_in_set('standardize', 'ONS').
card_original_type('standardize'/'ONS', 'Instant').
card_original_text('standardize'/'ONS', 'Choose a creature type other than Legend or Wall. Each creature\'s type becomes that type until end of turn.').
card_first_print('standardize', 'ONS').
card_image_name('standardize'/'ONS', 'standardize').
card_uid('standardize'/'ONS', 'ONS:Standardize:standardize').
card_rarity('standardize'/'ONS', 'Rare').
card_artist('standardize'/'ONS', 'Justin Sweet').
card_number('standardize'/'ONS', '116').
card_flavor_text('standardize'/'ONS', 'At that point, the wizards\' argument got a lot uglier.').
card_multiverse_id('standardize'/'ONS', '40205').

card_in_set('starlit sanctum', 'ONS').
card_original_type('starlit sanctum'/'ONS', 'Land').
card_original_text('starlit sanctum'/'ONS', '{T}: Add {1} to your mana pool.\n{W}, {T}, Sacrifice a Cleric: You gain life equal to that Cleric\'s toughness.\n{B}, {T}, Sacrifice a Cleric: Target player loses life equal to that Cleric\'s power.').
card_first_print('starlit sanctum', 'ONS').
card_image_name('starlit sanctum'/'ONS', 'starlit sanctum').
card_uid('starlit sanctum'/'ONS', 'ONS:Starlit Sanctum:starlit sanctum').
card_rarity('starlit sanctum'/'ONS', 'Uncommon').
card_artist('starlit sanctum'/'ONS', 'Ben Thompson').
card_number('starlit sanctum'/'ONS', '325').
card_multiverse_id('starlit sanctum'/'ONS', '40542').

card_in_set('starstorm', 'ONS').
card_original_type('starstorm'/'ONS', 'Instant').
card_original_text('starstorm'/'ONS', 'Starstorm deals X damage to each creature.\nCycling {3} ({3}, Discard this card from your hand: Draw a card.)').
card_first_print('starstorm', 'ONS').
card_image_name('starstorm'/'ONS', 'starstorm').
card_uid('starstorm'/'ONS', 'ONS:Starstorm:starstorm').
card_rarity('starstorm'/'ONS', 'Rare').
card_artist('starstorm'/'ONS', 'David Martin').
card_number('starstorm'/'ONS', '238').
card_flavor_text('starstorm'/'ONS', 'Pardic barbarians didn\'t complain when the Order started blaming every crisis on the Cabal.').
card_multiverse_id('starstorm'/'ONS', '41167').

card_in_set('steely resolve', 'ONS').
card_original_type('steely resolve'/'ONS', 'Enchantment').
card_original_text('steely resolve'/'ONS', 'As Steely Resolve comes into play, choose a creature type.\nCreatures of the chosen type can\'t be the targets of spells or abilities.').
card_first_print('steely resolve', 'ONS').
card_image_name('steely resolve'/'ONS', 'steely resolve').
card_uid('steely resolve'/'ONS', 'ONS:Steely Resolve:steely resolve').
card_rarity('steely resolve'/'ONS', 'Rare').
card_artist('steely resolve'/'ONS', 'Greg Staples').
card_number('steely resolve'/'ONS', '286').
card_flavor_text('steely resolve'/'ONS', 'No one in Wirewood understands what is happening. They just know it\'s unnatural—and coming from Krosa.').
card_multiverse_id('steely resolve'/'ONS', '40130').

card_in_set('strongarm tactics', 'ONS').
card_original_type('strongarm tactics'/'ONS', 'Sorcery').
card_original_text('strongarm tactics'/'ONS', 'Each player discards a card from his or her hand. Then each player who didn\'t discard a creature card this way loses 4 life.').
card_first_print('strongarm tactics', 'ONS').
card_image_name('strongarm tactics'/'ONS', 'strongarm tactics').
card_uid('strongarm tactics'/'ONS', 'ONS:Strongarm Tactics:strongarm tactics').
card_rarity('strongarm tactics'/'ONS', 'Rare').
card_artist('strongarm tactics'/'ONS', 'Greg & Tim Hildebrandt').
card_number('strongarm tactics'/'ONS', '173').
card_flavor_text('strongarm tactics'/'ONS', '\"If you can\'t pay your gambling debts, you just might be tomorrow\'s main attraction at the Grand Coliseum.\"').
card_multiverse_id('strongarm tactics'/'ONS', '26349').

card_in_set('sunfire balm', 'ONS').
card_original_type('sunfire balm'/'ONS', 'Instant').
card_original_text('sunfire balm'/'ONS', 'Prevent the next 4 damage that would be dealt to target creature or player this turn.\nCycling {1}{W} ({1}{W}, Discard this card from your hand: Draw a card.)\nWhen you cycle Sunfire Balm, you may prevent the next 1 damage that would be dealt to target creature or player this turn.').
card_first_print('sunfire balm', 'ONS').
card_image_name('sunfire balm'/'ONS', 'sunfire balm').
card_uid('sunfire balm'/'ONS', 'ONS:Sunfire Balm:sunfire balm').
card_rarity('sunfire balm'/'ONS', 'Uncommon').
card_artist('sunfire balm'/'ONS', 'Monte Michael Moore').
card_number('sunfire balm'/'ONS', '56').
card_multiverse_id('sunfire balm'/'ONS', '41144').

card_in_set('supreme inquisitor', 'ONS').
card_original_type('supreme inquisitor'/'ONS', 'Creature — Wizard Lord').
card_original_text('supreme inquisitor'/'ONS', 'Tap five untapped Wizards you control: Search target player\'s library for up to five cards and remove them from the game. Then that player shuffles his or her library.').
card_first_print('supreme inquisitor', 'ONS').
card_image_name('supreme inquisitor'/'ONS', 'supreme inquisitor').
card_uid('supreme inquisitor'/'ONS', 'ONS:Supreme Inquisitor:supreme inquisitor').
card_rarity('supreme inquisitor'/'ONS', 'Rare').
card_artist('supreme inquisitor'/'ONS', 'rk post').
card_number('supreme inquisitor'/'ONS', '117').
card_flavor_text('supreme inquisitor'/'ONS', '\"It\'s hard to fight on an empty mind.\"').
card_multiverse_id('supreme inquisitor'/'ONS', '39862').

card_in_set('swamp', 'ONS').
card_original_type('swamp'/'ONS', 'Land').
card_original_text('swamp'/'ONS', 'B').
card_image_name('swamp'/'ONS', 'swamp1').
card_uid('swamp'/'ONS', 'ONS:Swamp:swamp1').
card_rarity('swamp'/'ONS', 'Basic Land').
card_artist('swamp'/'ONS', 'Tony Szczudlo').
card_number('swamp'/'ONS', '339').
card_multiverse_id('swamp'/'ONS', '40114').

card_in_set('swamp', 'ONS').
card_original_type('swamp'/'ONS', 'Land').
card_original_text('swamp'/'ONS', 'B').
card_image_name('swamp'/'ONS', 'swamp2').
card_uid('swamp'/'ONS', 'ONS:Swamp:swamp2').
card_rarity('swamp'/'ONS', 'Basic Land').
card_artist('swamp'/'ONS', 'Doug Chaffee').
card_number('swamp'/'ONS', '340').
card_multiverse_id('swamp'/'ONS', '40113').

card_in_set('swamp', 'ONS').
card_original_type('swamp'/'ONS', 'Land').
card_original_text('swamp'/'ONS', 'B').
card_image_name('swamp'/'ONS', 'swamp3').
card_uid('swamp'/'ONS', 'ONS:Swamp:swamp3').
card_rarity('swamp'/'ONS', 'Basic Land').
card_artist('swamp'/'ONS', 'Dan Frazier').
card_number('swamp'/'ONS', '341').
card_multiverse_id('swamp'/'ONS', '40112').

card_in_set('swamp', 'ONS').
card_original_type('swamp'/'ONS', 'Land').
card_original_text('swamp'/'ONS', 'B').
card_image_name('swamp'/'ONS', 'swamp4').
card_uid('swamp'/'ONS', 'ONS:Swamp:swamp4').
card_rarity('swamp'/'ONS', 'Basic Land').
card_artist('swamp'/'ONS', 'Pete Venters').
card_number('swamp'/'ONS', '342').
card_multiverse_id('swamp'/'ONS', '40111').

card_in_set('swat', 'ONS').
card_original_type('swat'/'ONS', 'Instant').
card_original_text('swat'/'ONS', 'Destroy target creature with power 2 or less.\nCycling {2} ({2}, Discard this card from your hand: Draw a card.)').
card_image_name('swat'/'ONS', 'swat').
card_uid('swat'/'ONS', 'ONS:Swat:swat').
card_rarity('swat'/'ONS', 'Common').
card_artist('swat'/'ONS', 'rk post').
card_number('swat'/'ONS', '174').
card_flavor_text('swat'/'ONS', 'Few who cross Phage have the chance to repeat the mistake.').
card_multiverse_id('swat'/'ONS', '41136').

card_in_set('symbiotic beast', 'ONS').
card_original_type('symbiotic beast'/'ONS', 'Creature — Beast').
card_original_text('symbiotic beast'/'ONS', 'When Symbiotic Beast is put into a graveyard from play, put four 1/1 green Insect creature tokens into play.').
card_first_print('symbiotic beast', 'ONS').
card_image_name('symbiotic beast'/'ONS', 'symbiotic beast').
card_uid('symbiotic beast'/'ONS', 'ONS:Symbiotic Beast:symbiotic beast').
card_rarity('symbiotic beast'/'ONS', 'Uncommon').
card_artist('symbiotic beast'/'ONS', 'Franz Vohwinkel').
card_number('symbiotic beast'/'ONS', '287').
card_flavor_text('symbiotic beast'/'ONS', 'The insects found a meal in the carrion of the beast\'s prey. The beast found a spiffy new hairstyle.').
card_multiverse_id('symbiotic beast'/'ONS', '39924').

card_in_set('symbiotic elf', 'ONS').
card_original_type('symbiotic elf'/'ONS', 'Creature — Elf').
card_original_text('symbiotic elf'/'ONS', 'When Symbiotic Elf is put into a graveyard from play, put two 1/1 green Insect creature tokens into play.').
card_first_print('symbiotic elf', 'ONS').
card_image_name('symbiotic elf'/'ONS', 'symbiotic elf').
card_uid('symbiotic elf'/'ONS', 'ONS:Symbiotic Elf:symbiotic elf').
card_rarity('symbiotic elf'/'ONS', 'Common').
card_artist('symbiotic elf'/'ONS', 'Wayne England').
card_number('symbiotic elf'/'ONS', '288').
card_flavor_text('symbiotic elf'/'ONS', 'The elves arriving in Wirewood had no homes, let alone weapons, but they knew that the forest would always provide.').
card_multiverse_id('symbiotic elf'/'ONS', '32216').

card_in_set('symbiotic wurm', 'ONS').
card_original_type('symbiotic wurm'/'ONS', 'Creature — Wurm').
card_original_text('symbiotic wurm'/'ONS', 'When Symbiotic Wurm is put into a graveyard from play, put seven 1/1 green Insect creature tokens into play.').
card_first_print('symbiotic wurm', 'ONS').
card_image_name('symbiotic wurm'/'ONS', 'symbiotic wurm').
card_uid('symbiotic wurm'/'ONS', 'ONS:Symbiotic Wurm:symbiotic wurm').
card_rarity('symbiotic wurm'/'ONS', 'Rare').
card_artist('symbiotic wurm'/'ONS', 'Matt Cavotta').
card_number('symbiotic wurm'/'ONS', '289').
card_flavor_text('symbiotic wurm'/'ONS', 'The insects keep the wurm\'s hide free from parasites. In return, the wurm doesn\'t eat the insects.').
card_multiverse_id('symbiotic wurm'/'ONS', '39925').

card_in_set('syphon mind', 'ONS').
card_original_type('syphon mind'/'ONS', 'Sorcery').
card_original_text('syphon mind'/'ONS', 'Each other player discards a card from his or her hand. You draw a card for each card discarded this way.').
card_first_print('syphon mind', 'ONS').
card_image_name('syphon mind'/'ONS', 'syphon mind').
card_uid('syphon mind'/'ONS', 'ONS:Syphon Mind:syphon mind').
card_rarity('syphon mind'/'ONS', 'Common').
card_artist('syphon mind'/'ONS', 'Jeff Easley').
card_number('syphon mind'/'ONS', '175').
card_flavor_text('syphon mind'/'ONS', 'When tempers run high, it\'s easy to lose your head.').
card_multiverse_id('syphon mind'/'ONS', '39825').

card_in_set('syphon soul', 'ONS').
card_original_type('syphon soul'/'ONS', 'Sorcery').
card_original_text('syphon soul'/'ONS', 'Syphon Soul deals 2 damage to each other player. You gain life equal to the damage dealt this way.').
card_image_name('syphon soul'/'ONS', 'syphon soul').
card_uid('syphon soul'/'ONS', 'ONS:Syphon Soul:syphon soul').
card_rarity('syphon soul'/'ONS', 'Common').
card_artist('syphon soul'/'ONS', 'Ron Spears').
card_number('syphon soul'/'ONS', '176').
card_flavor_text('syphon soul'/'ONS', 'As Phage drank their energy, a vague memory of Jeska stirred. Then she lost herself again in the joy of her victims\' suffering.').
card_multiverse_id('syphon soul'/'ONS', '39468').

card_in_set('taunting elf', 'ONS').
card_original_type('taunting elf'/'ONS', 'Creature — Elf').
card_original_text('taunting elf'/'ONS', 'All creatures able to block Taunting Elf do so.').
card_image_name('taunting elf'/'ONS', 'taunting elf').
card_uid('taunting elf'/'ONS', 'ONS:Taunting Elf:taunting elf').
card_rarity('taunting elf'/'ONS', 'Common').
card_artist('taunting elf'/'ONS', 'Rebecca Guay').
card_number('taunting elf'/'ONS', '290').
card_flavor_text('taunting elf'/'ONS', 'The safety of the village depends on the beast thinking with its stomach.').
card_multiverse_id('taunting elf'/'ONS', '39890').

card_in_set('tempting wurm', 'ONS').
card_original_type('tempting wurm'/'ONS', 'Creature — Wurm').
card_original_text('tempting wurm'/'ONS', 'When Tempting Wurm comes into play, each opponent may put any number of artifact, creature, enchantment, and/or land cards from his or her hand into play.').
card_first_print('tempting wurm', 'ONS').
card_image_name('tempting wurm'/'ONS', 'tempting wurm').
card_uid('tempting wurm'/'ONS', 'ONS:Tempting Wurm:tempting wurm').
card_rarity('tempting wurm'/'ONS', 'Rare').
card_artist('tempting wurm'/'ONS', 'Bob Petillo').
card_number('tempting wurm'/'ONS', '291').
card_multiverse_id('tempting wurm'/'ONS', '39929').

card_in_set('tephraderm', 'ONS').
card_original_type('tephraderm'/'ONS', 'Creature — Beast').
card_original_text('tephraderm'/'ONS', 'Whenever a creature deals damage to Tephraderm, Tephraderm deals that much damage to that creature.\nWhenever a spell deals damage to Tephraderm, Tephraderm deals that much damage to that spell\'s controller.').
card_first_print('tephraderm', 'ONS').
card_image_name('tephraderm'/'ONS', 'tephraderm').
card_uid('tephraderm'/'ONS', 'ONS:Tephraderm:tephraderm').
card_rarity('tephraderm'/'ONS', 'Rare').
card_artist('tephraderm'/'ONS', 'Paolo Parente').
card_number('tephraderm'/'ONS', '239').
card_multiverse_id('tephraderm'/'ONS', '39659').

card_in_set('thoughtbound primoc', 'ONS').
card_original_type('thoughtbound primoc'/'ONS', 'Creature — Bird Beast').
card_original_text('thoughtbound primoc'/'ONS', 'Flying\nAt the beginning of your upkeep, if a player controls more Wizards than any other player, he or she gains control of Thoughtbound Primoc.').
card_first_print('thoughtbound primoc', 'ONS').
card_image_name('thoughtbound primoc'/'ONS', 'thoughtbound primoc').
card_uid('thoughtbound primoc'/'ONS', 'ONS:Thoughtbound Primoc:thoughtbound primoc').
card_rarity('thoughtbound primoc'/'ONS', 'Uncommon').
card_artist('thoughtbound primoc'/'ONS', 'Jeff Miracola').
card_number('thoughtbound primoc'/'ONS', '240').
card_flavor_text('thoughtbound primoc'/'ONS', 'Efforts to use them as spies failed when they kept reporting to the enemy.').
card_multiverse_id('thoughtbound primoc'/'ONS', '39528').

card_in_set('thrashing mudspawn', 'ONS').
card_original_type('thrashing mudspawn'/'ONS', 'Creature — Beast').
card_original_text('thrashing mudspawn'/'ONS', 'Whenever Thrashing Mudspawn is dealt damage, you lose that much life.\nMorph {1}{B}{B} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('thrashing mudspawn', 'ONS').
card_image_name('thrashing mudspawn'/'ONS', 'thrashing mudspawn').
card_uid('thrashing mudspawn'/'ONS', 'ONS:Thrashing Mudspawn:thrashing mudspawn').
card_rarity('thrashing mudspawn'/'ONS', 'Uncommon').
card_artist('thrashing mudspawn'/'ONS', 'Thomas M. Baxa').
card_number('thrashing mudspawn'/'ONS', '177').
card_flavor_text('thrashing mudspawn'/'ONS', 'It just obeys you. It doesn\'t like you.').
card_multiverse_id('thrashing mudspawn'/'ONS', '39853').

card_in_set('threaten', 'ONS').
card_original_type('threaten'/'ONS', 'Sorcery').
card_original_text('threaten'/'ONS', 'Untap target creature and gain control of it until end of turn. That creature gains haste until end of turn.').
card_first_print('threaten', 'ONS').
card_image_name('threaten'/'ONS', 'threaten').
card_uid('threaten'/'ONS', 'ONS:Threaten:threaten').
card_rarity('threaten'/'ONS', 'Uncommon').
card_artist('threaten'/'ONS', 'Mark Brill').
card_number('threaten'/'ONS', '241').
card_flavor_text('threaten'/'ONS', 'Goblins\' motivational techniques are crude, but effective.').
card_multiverse_id('threaten'/'ONS', '43736').

card_in_set('thunder of hooves', 'ONS').
card_original_type('thunder of hooves'/'ONS', 'Sorcery').
card_original_text('thunder of hooves'/'ONS', 'Thunder of Hooves deals X damage to each creature without flying and each player, where X is the number of Beasts in play.').
card_first_print('thunder of hooves', 'ONS').
card_image_name('thunder of hooves'/'ONS', 'thunder of hooves').
card_uid('thunder of hooves'/'ONS', 'ONS:Thunder of Hooves:thunder of hooves').
card_rarity('thunder of hooves'/'ONS', 'Uncommon').
card_artist('thunder of hooves'/'ONS', 'Jim Nelson').
card_number('thunder of hooves'/'ONS', '242').
card_multiverse_id('thunder of hooves'/'ONS', '39847').

card_in_set('towering baloth', 'ONS').
card_original_type('towering baloth'/'ONS', 'Creature — Beast').
card_original_text('towering baloth'/'ONS', 'Morph {6}{G} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('towering baloth', 'ONS').
card_image_name('towering baloth'/'ONS', 'towering baloth').
card_uid('towering baloth'/'ONS', 'ONS:Towering Baloth:towering baloth').
card_rarity('towering baloth'/'ONS', 'Uncommon').
card_artist('towering baloth'/'ONS', 'Arnie Swekel').
card_number('towering baloth'/'ONS', '292').
card_flavor_text('towering baloth'/'ONS', 'The Mirari twisted all that lived in the Krosan Forest into gross mockeries of their former selves.').
card_multiverse_id('towering baloth'/'ONS', '39741').

card_in_set('trade secrets', 'ONS').
card_original_type('trade secrets'/'ONS', 'Sorcery').
card_original_text('trade secrets'/'ONS', 'Target opponent draws two cards, then you draw up to four cards. That opponent may repeat this process as many times as he or she chooses.').
card_first_print('trade secrets', 'ONS').
card_image_name('trade secrets'/'ONS', 'trade secrets').
card_uid('trade secrets'/'ONS', 'ONS:Trade Secrets:trade secrets').
card_rarity('trade secrets'/'ONS', 'Rare').
card_artist('trade secrets'/'ONS', 'Ron Spears').
card_number('trade secrets'/'ONS', '118').
card_multiverse_id('trade secrets'/'ONS', '39537').

card_in_set('tranquil thicket', 'ONS').
card_original_type('tranquil thicket'/'ONS', 'Land').
card_original_text('tranquil thicket'/'ONS', 'Tranquil Thicket comes into play tapped.\n{T}: Add {G} to your mana pool.\nCycling {G} ({G}, Discard this card from your hand: Draw a card.)').
card_first_print('tranquil thicket', 'ONS').
card_image_name('tranquil thicket'/'ONS', 'tranquil thicket').
card_uid('tranquil thicket'/'ONS', 'ONS:Tranquil Thicket:tranquil thicket').
card_rarity('tranquil thicket'/'ONS', 'Common').
card_artist('tranquil thicket'/'ONS', 'Heather Hudson').
card_number('tranquil thicket'/'ONS', '326').
card_multiverse_id('tranquil thicket'/'ONS', '41141').

card_in_set('treespring lorian', 'ONS').
card_original_type('treespring lorian'/'ONS', 'Creature — Beast').
card_original_text('treespring lorian'/'ONS', 'Morph {5}{G} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('treespring lorian', 'ONS').
card_image_name('treespring lorian'/'ONS', 'treespring lorian').
card_uid('treespring lorian'/'ONS', 'ONS:Treespring Lorian:treespring lorian').
card_rarity('treespring lorian'/'ONS', 'Common').
card_artist('treespring lorian'/'ONS', 'Heather Hudson').
card_number('treespring lorian'/'ONS', '293').
card_flavor_text('treespring lorian'/'ONS', 'No matter your strength, the Krosan Forest is stronger. No matter your speed, the Wirewood Forest is faster.').
card_multiverse_id('treespring lorian'/'ONS', '39721').

card_in_set('tribal golem', 'ONS').
card_original_type('tribal golem'/'ONS', 'Artifact Creature — Golem').
card_original_text('tribal golem'/'ONS', 'Tribal Golem has trample as long as you control a Beast, haste as long as you control a Goblin, first strike as long as you control a Soldier, flying as long as you control a Wizard, and \"{B}: Regenerate Tribal Golem\" as long as you control a Zombie.').
card_first_print('tribal golem', 'ONS').
card_image_name('tribal golem'/'ONS', 'tribal golem').
card_uid('tribal golem'/'ONS', 'ONS:Tribal Golem:tribal golem').
card_rarity('tribal golem'/'ONS', 'Rare').
card_artist('tribal golem'/'ONS', 'Edward P. Beard, Jr.').
card_number('tribal golem'/'ONS', '311').
card_multiverse_id('tribal golem'/'ONS', '30764').

card_in_set('tribal unity', 'ONS').
card_original_type('tribal unity'/'ONS', 'Instant').
card_original_text('tribal unity'/'ONS', 'Creatures of the type of your choice get +X/+X until end of turn.').
card_first_print('tribal unity', 'ONS').
card_image_name('tribal unity'/'ONS', 'tribal unity').
card_uid('tribal unity'/'ONS', 'ONS:Tribal Unity:tribal unity').
card_rarity('tribal unity'/'ONS', 'Uncommon').
card_artist('tribal unity'/'ONS', 'Ron Spears').
card_number('tribal unity'/'ONS', '294').
card_flavor_text('tribal unity'/'ONS', 'Kamahl left the violence of his former life behind him, but he still believes in the power of muscle.').
card_multiverse_id('tribal unity'/'ONS', '39677').

card_in_set('trickery charm', 'ONS').
card_original_type('trickery charm'/'ONS', 'Instant').
card_original_text('trickery charm'/'ONS', 'Choose one Target creature gains flying until end of turn; or target creature\'s type becomes the creature type of your choice until end of turn; or look at the top four cards of your library, then put them back in any order.').
card_first_print('trickery charm', 'ONS').
card_image_name('trickery charm'/'ONS', 'trickery charm').
card_uid('trickery charm'/'ONS', 'ONS:Trickery Charm:trickery charm').
card_rarity('trickery charm'/'ONS', 'Common').
card_artist('trickery charm'/'ONS', 'David Martin').
card_number('trickery charm'/'ONS', '119').
card_multiverse_id('trickery charm'/'ONS', '39450').

card_in_set('true believer', 'ONS').
card_original_type('true believer'/'ONS', 'Creature — Cleric').
card_original_text('true believer'/'ONS', 'You can\'t be the target of spells or abilities.').
card_first_print('true believer', 'ONS').
card_image_name('true believer'/'ONS', 'true believer').
card_uid('true believer'/'ONS', 'ONS:True Believer:true believer').
card_rarity('true believer'/'ONS', 'Rare').
card_artist('true believer'/'ONS', 'Alex Horley-Orlandelli').
card_number('true believer'/'ONS', '57').
card_flavor_text('true believer'/'ONS', 'So great is his certainty that mere facts cannot shake it.').
card_multiverse_id('true believer'/'ONS', '39606').

card_in_set('undead gladiator', 'ONS').
card_original_type('undead gladiator'/'ONS', 'Creature — Zombie Barbarian').
card_original_text('undead gladiator'/'ONS', '{1}{B}, Discard a card from your hand: Return Undead Gladiator from your graveyard to your hand. Play this ability only during your upkeep.\nCycling {1}{B} ({1}{B}, Discard this card from your hand: Draw a card.)').
card_first_print('undead gladiator', 'ONS').
card_image_name('undead gladiator'/'ONS', 'undead gladiator').
card_uid('undead gladiator'/'ONS', 'ONS:Undead Gladiator:undead gladiator').
card_rarity('undead gladiator'/'ONS', 'Rare').
card_artist('undead gladiator'/'ONS', 'Jeff Easley').
card_number('undead gladiator'/'ONS', '178').
card_multiverse_id('undead gladiator'/'ONS', '41166').

card_in_set('unholy grotto', 'ONS').
card_original_type('unholy grotto'/'ONS', 'Land').
card_original_text('unholy grotto'/'ONS', '{T}: Add {1} to your mana pool.\n{B}, {T}: Put target Zombie card from your graveyard on top of your library.').
card_first_print('unholy grotto', 'ONS').
card_image_name('unholy grotto'/'ONS', 'unholy grotto').
card_uid('unholy grotto'/'ONS', 'ONS:Unholy Grotto:unholy grotto').
card_rarity('unholy grotto'/'ONS', 'Rare').
card_artist('unholy grotto'/'ONS', 'John Avon').
card_number('unholy grotto'/'ONS', '327').
card_multiverse_id('unholy grotto'/'ONS', '40541').

card_in_set('unified strike', 'ONS').
card_original_type('unified strike'/'ONS', 'Instant').
card_original_text('unified strike'/'ONS', 'Remove target attacking creature from the game if its power is less than or equal to the number of Soldiers in play.').
card_first_print('unified strike', 'ONS').
card_image_name('unified strike'/'ONS', 'unified strike').
card_uid('unified strike'/'ONS', 'ONS:Unified Strike:unified strike').
card_rarity('unified strike'/'ONS', 'Common').
card_artist('unified strike'/'ONS', 'Dave Dorman').
card_number('unified strike'/'ONS', '58').
card_flavor_text('unified strike'/'ONS', 'The tall, reedy grass of the Daru Plains rustles loudly in the winds, a perfect cover for any ambush.').
card_multiverse_id('unified strike'/'ONS', '39427').

card_in_set('venomspout brackus', 'ONS').
card_original_type('venomspout brackus'/'ONS', 'Creature — Beast').
card_original_text('venomspout brackus'/'ONS', '{1}{G}, {T}: Venomspout Brackus deals 5 damage to target attacking or blocking creature with flying.\nMorph {3}{G}{G} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('venomspout brackus', 'ONS').
card_image_name('venomspout brackus'/'ONS', 'venomspout brackus').
card_uid('venomspout brackus'/'ONS', 'ONS:Venomspout Brackus:venomspout brackus').
card_rarity('venomspout brackus'/'ONS', 'Uncommon').
card_artist('venomspout brackus'/'ONS', 'Ron Spencer').
card_number('venomspout brackus'/'ONS', '295').
card_multiverse_id('venomspout brackus'/'ONS', '39579').

card_in_set('visara the dreadful', 'ONS').
card_original_type('visara the dreadful'/'ONS', 'Creature — Gorgon Legend').
card_original_text('visara the dreadful'/'ONS', 'Flying\n{T}: Destroy target creature. It can\'t be regenerated.').
card_first_print('visara the dreadful', 'ONS').
card_image_name('visara the dreadful'/'ONS', 'visara the dreadful').
card_uid('visara the dreadful'/'ONS', 'ONS:Visara the Dreadful:visara the dreadful').
card_rarity('visara the dreadful'/'ONS', 'Rare').
card_artist('visara the dreadful'/'ONS', 'Kev Walker').
card_number('visara the dreadful'/'ONS', '179').
card_flavor_text('visara the dreadful'/'ONS', '\"My eyes are my strongest feature.\"').
card_multiverse_id('visara the dreadful'/'ONS', '39863').

card_in_set('vitality charm', 'ONS').
card_original_type('vitality charm'/'ONS', 'Instant').
card_original_text('vitality charm'/'ONS', 'Choose one Put a 1/1 green Insect creature token into play; or target creature gets +1/+1 and gains trample until end of turn; or regenerate target Beast.').
card_first_print('vitality charm', 'ONS').
card_image_name('vitality charm'/'ONS', 'vitality charm').
card_uid('vitality charm'/'ONS', 'ONS:Vitality Charm:vitality charm').
card_rarity('vitality charm'/'ONS', 'Common').
card_artist('vitality charm'/'ONS', 'David Martin').
card_number('vitality charm'/'ONS', '296').
card_multiverse_id('vitality charm'/'ONS', '39501').

card_in_set('voice of the woods', 'ONS').
card_original_type('voice of the woods'/'ONS', 'Creature — Elf Lord').
card_original_text('voice of the woods'/'ONS', 'Tap five untapped Elves you control: Put a 7/7 green Elemental creature token with trample into play.').
card_first_print('voice of the woods', 'ONS').
card_image_name('voice of the woods'/'ONS', 'voice of the woods').
card_uid('voice of the woods'/'ONS', 'ONS:Voice of the Woods:voice of the woods').
card_rarity('voice of the woods'/'ONS', 'Rare').
card_artist('voice of the woods'/'ONS', 'Pete Venters').
card_number('voice of the woods'/'ONS', '297').
card_flavor_text('voice of the woods'/'ONS', 'The ritual of making draws upon the elves\' memories and pasts. And elves have long memories and ancient pasts.').
card_multiverse_id('voice of the woods'/'ONS', '40174').

card_in_set('voidmage prodigy', 'ONS').
card_original_type('voidmage prodigy'/'ONS', 'Creature — Wizard').
card_original_text('voidmage prodigy'/'ONS', '{U}{U}, Sacrifice a Wizard: Counter target spell.\nMorph {U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_image_name('voidmage prodigy'/'ONS', 'voidmage prodigy').
card_uid('voidmage prodigy'/'ONS', 'ONS:Voidmage Prodigy:voidmage prodigy').
card_rarity('voidmage prodigy'/'ONS', 'Rare').
card_artist('voidmage prodigy'/'ONS', 'Scott M. Fischer').
card_number('voidmage prodigy'/'ONS', '120').
card_multiverse_id('voidmage prodigy'/'ONS', '40101').

card_in_set('walking desecration', 'ONS').
card_original_type('walking desecration'/'ONS', 'Creature — Zombie').
card_original_text('walking desecration'/'ONS', '{B}, {T}: Creatures of the type of your choice attack this turn if able.').
card_first_print('walking desecration', 'ONS').
card_image_name('walking desecration'/'ONS', 'walking desecration').
card_uid('walking desecration'/'ONS', 'ONS:Walking Desecration:walking desecration').
card_rarity('walking desecration'/'ONS', 'Uncommon').
card_artist('walking desecration'/'ONS', 'Daren Bader').
card_number('walking desecration'/'ONS', '180').
card_flavor_text('walking desecration'/'ONS', 'Such sacrilege turns blinding grief into blinding rage.').
card_multiverse_id('walking desecration'/'ONS', '39552').

card_in_set('wall of mulch', 'ONS').
card_original_type('wall of mulch'/'ONS', 'Creature — Wall').
card_original_text('wall of mulch'/'ONS', '(Walls can\'t attack.)\n{G}, Sacrifice a Wall: Draw a card.').
card_first_print('wall of mulch', 'ONS').
card_image_name('wall of mulch'/'ONS', 'wall of mulch').
card_uid('wall of mulch'/'ONS', 'ONS:Wall of Mulch:wall of mulch').
card_rarity('wall of mulch'/'ONS', 'Uncommon').
card_artist('wall of mulch'/'ONS', 'Anthony S. Waters').
card_number('wall of mulch'/'ONS', '298').
card_flavor_text('wall of mulch'/'ONS', 'Mulch is the fabric of life in the forest. Plants live in it, they die in it, and then they become part of it, feeding countless generations to come.').
card_multiverse_id('wall of mulch'/'ONS', '39495').

card_in_set('wave of indifference', 'ONS').
card_original_type('wave of indifference'/'ONS', 'Sorcery').
card_original_text('wave of indifference'/'ONS', 'X target creatures can\'t block this turn.').
card_first_print('wave of indifference', 'ONS').
card_image_name('wave of indifference'/'ONS', 'wave of indifference').
card_uid('wave of indifference'/'ONS', 'ONS:Wave of Indifference:wave of indifference').
card_rarity('wave of indifference'/'ONS', 'Common').
card_artist('wave of indifference'/'ONS', 'Greg & Tim Hildebrandt').
card_number('wave of indifference'/'ONS', '243').
card_flavor_text('wave of indifference'/'ONS', '\"Darius?\"\n\"Yeah?\"\n\"There\'s a goblin sneaking up on you.\"\n\"So?\"\n\"Just sayin\'.\"').
card_multiverse_id('wave of indifference'/'ONS', '39483').

card_in_set('weathered wayfarer', 'ONS').
card_original_type('weathered wayfarer'/'ONS', 'Creature — Nomad Cleric').
card_original_text('weathered wayfarer'/'ONS', '{W}, {T}: Search your library for a land card, reveal it, and put it into your hand. Then shuffle your library. Play this ability only if an opponent controls more lands than you.').
card_first_print('weathered wayfarer', 'ONS').
card_image_name('weathered wayfarer'/'ONS', 'weathered wayfarer').
card_uid('weathered wayfarer'/'ONS', 'ONS:Weathered Wayfarer:weathered wayfarer').
card_rarity('weathered wayfarer'/'ONS', 'Rare').
card_artist('weathered wayfarer'/'ONS', 'Greg & Tim Hildebrandt').
card_number('weathered wayfarer'/'ONS', '59').
card_multiverse_id('weathered wayfarer'/'ONS', '39515').

card_in_set('weird harvest', 'ONS').
card_original_type('weird harvest'/'ONS', 'Sorcery').
card_original_text('weird harvest'/'ONS', 'Each player may search his or her library for up to X creature cards, reveal those cards, and put them into his or her hand. Then each player who searched his or her library this way shuffles it.').
card_first_print('weird harvest', 'ONS').
card_image_name('weird harvest'/'ONS', 'weird harvest').
card_uid('weird harvest'/'ONS', 'ONS:Weird Harvest:weird harvest').
card_rarity('weird harvest'/'ONS', 'Rare').
card_artist('weird harvest'/'ONS', 'Bob Petillo').
card_number('weird harvest'/'ONS', '299').
card_flavor_text('weird harvest'/'ONS', 'Krosa\'s distorted groves bear strange fruit.').
card_multiverse_id('weird harvest'/'ONS', '39680').

card_in_set('wellwisher', 'ONS').
card_original_type('wellwisher'/'ONS', 'Creature — Elf').
card_original_text('wellwisher'/'ONS', '{T}: You gain 1 life for each Elf in play.').
card_first_print('wellwisher', 'ONS').
card_image_name('wellwisher'/'ONS', 'wellwisher').
card_uid('wellwisher'/'ONS', 'ONS:Wellwisher:wellwisher').
card_rarity('wellwisher'/'ONS', 'Common').
card_artist('wellwisher'/'ONS', 'Christopher Rush').
card_number('wellwisher'/'ONS', '300').
card_flavor_text('wellwisher'/'ONS', '\"Close your ears to the voice of greed, and you can turn a gift for one into a gift for many.\"').
card_multiverse_id('wellwisher'/'ONS', '39497').

card_in_set('wheel and deal', 'ONS').
card_original_type('wheel and deal'/'ONS', 'Instant').
card_original_text('wheel and deal'/'ONS', 'Any number of target opponents each discards his or her hand and draws seven cards.\nDraw a card.').
card_first_print('wheel and deal', 'ONS').
card_image_name('wheel and deal'/'ONS', 'wheel and deal').
card_uid('wheel and deal'/'ONS', 'ONS:Wheel and Deal:wheel and deal').
card_rarity('wheel and deal'/'ONS', 'Rare').
card_artist('wheel and deal'/'ONS', 'Alan Pollack').
card_number('wheel and deal'/'ONS', '121').
card_multiverse_id('wheel and deal'/'ONS', '42184').

card_in_set('whipcorder', 'ONS').
card_original_type('whipcorder'/'ONS', 'Creature — Soldier Rebel').
card_original_text('whipcorder'/'ONS', '{W}, {T}: Tap target creature.\nMorph {W} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_image_name('whipcorder'/'ONS', 'whipcorder').
card_uid('whipcorder'/'ONS', 'ONS:Whipcorder:whipcorder').
card_rarity('whipcorder'/'ONS', 'Uncommon').
card_artist('whipcorder'/'ONS', 'Ron Spencer').
card_number('whipcorder'/'ONS', '60').
card_flavor_text('whipcorder'/'ONS', 'His bolas whirl like galaxies, but it\'s his enemies who see stars.').
card_multiverse_id('whipcorder'/'ONS', '41024').

card_in_set('windswept heath', 'ONS').
card_original_type('windswept heath'/'ONS', 'Land').
card_original_text('windswept heath'/'ONS', '{T}, Pay 1 life, Sacrifice Windswept Heath: Search your library for a forest or plains card and put it into play. Then shuffle your library.').
card_image_name('windswept heath'/'ONS', 'windswept heath').
card_uid('windswept heath'/'ONS', 'ONS:Windswept Heath:windswept heath').
card_rarity('windswept heath'/'ONS', 'Rare').
card_artist('windswept heath'/'ONS', 'Anthony S. Waters').
card_number('windswept heath'/'ONS', '328').
card_multiverse_id('windswept heath'/'ONS', '39507').

card_in_set('wirewood elf', 'ONS').
card_original_type('wirewood elf'/'ONS', 'Creature — Elf').
card_original_text('wirewood elf'/'ONS', '{T}: Add {G} to your mana pool.').
card_first_print('wirewood elf', 'ONS').
card_image_name('wirewood elf'/'ONS', 'wirewood elf').
card_uid('wirewood elf'/'ONS', 'ONS:Wirewood Elf:wirewood elf').
card_rarity('wirewood elf'/'ONS', 'Common').
card_artist('wirewood elf'/'ONS', 'Jerry Tiritilli').
card_number('wirewood elf'/'ONS', '301').
card_flavor_text('wirewood elf'/'ONS', '\"The land belongs to nature as far as our eyes can see. The higher we climb, the more we can see.\"').
card_multiverse_id('wirewood elf'/'ONS', '39828').

card_in_set('wirewood herald', 'ONS').
card_original_type('wirewood herald'/'ONS', 'Creature — Elf').
card_original_text('wirewood herald'/'ONS', 'When Wirewood Herald is put into a graveyard from play, you may search your library for an Elf card. If you do, reveal that card and put it into your hand. Then shuffle your library.').
card_first_print('wirewood herald', 'ONS').
card_image_name('wirewood herald'/'ONS', 'wirewood herald').
card_uid('wirewood herald'/'ONS', 'ONS:Wirewood Herald:wirewood herald').
card_rarity('wirewood herald'/'ONS', 'Common').
card_artist('wirewood herald'/'ONS', 'Alex Horley-Orlandelli').
card_number('wirewood herald'/'ONS', '302').
card_flavor_text('wirewood herald'/'ONS', 'The goblins laughed as the elf ran away, until more came back.').
card_multiverse_id('wirewood herald'/'ONS', '39907').

card_in_set('wirewood lodge', 'ONS').
card_original_type('wirewood lodge'/'ONS', 'Land').
card_original_text('wirewood lodge'/'ONS', '{T}: Add {1} to your mana pool.\n{G}, {T}: Untap target Elf.').
card_first_print('wirewood lodge', 'ONS').
card_image_name('wirewood lodge'/'ONS', 'wirewood lodge').
card_uid('wirewood lodge'/'ONS', 'ONS:Wirewood Lodge:wirewood lodge').
card_rarity('wirewood lodge'/'ONS', 'Uncommon').
card_artist('wirewood lodge'/'ONS', 'Anthony S. Waters').
card_number('wirewood lodge'/'ONS', '329').
card_multiverse_id('wirewood lodge'/'ONS', '34764').

card_in_set('wirewood pride', 'ONS').
card_original_type('wirewood pride'/'ONS', 'Instant').
card_original_text('wirewood pride'/'ONS', 'Target creature gets +X/+X until end of turn, where X is the number of Elves in play.').
card_first_print('wirewood pride', 'ONS').
card_image_name('wirewood pride'/'ONS', 'wirewood pride').
card_uid('wirewood pride'/'ONS', 'ONS:Wirewood Pride:wirewood pride').
card_rarity('wirewood pride'/'ONS', 'Common').
card_artist('wirewood pride'/'ONS', 'Dave Dorman').
card_number('wirewood pride'/'ONS', '303').
card_flavor_text('wirewood pride'/'ONS', '\"Though you may leave this haven we have found, may our strength never leave you.\"').
card_multiverse_id('wirewood pride'/'ONS', '39841').

card_in_set('wirewood savage', 'ONS').
card_original_type('wirewood savage'/'ONS', 'Creature — Elf').
card_original_text('wirewood savage'/'ONS', 'Whenever a Beast comes into play, you may draw a card.').
card_first_print('wirewood savage', 'ONS').
card_image_name('wirewood savage'/'ONS', 'wirewood savage').
card_uid('wirewood savage'/'ONS', 'ONS:Wirewood Savage:wirewood savage').
card_rarity('wirewood savage'/'ONS', 'Common').
card_artist('wirewood savage'/'ONS', 'DiTerlizzi').
card_number('wirewood savage'/'ONS', '304').
card_flavor_text('wirewood savage'/'ONS', '\"She is truly Wirewood\'s child now.\"\n—Elvish refugee').
card_multiverse_id('wirewood savage'/'ONS', '39703').

card_in_set('withering hex', 'ONS').
card_original_type('withering hex'/'ONS', 'Enchant Creature').
card_original_text('withering hex'/'ONS', 'Whenever a player cycles a card, put a plague counter on Withering Hex.\nEnchanted creature gets -1/-1 for each plague counter on Withering Hex.').
card_first_print('withering hex', 'ONS').
card_image_name('withering hex'/'ONS', 'withering hex').
card_uid('withering hex'/'ONS', 'ONS:Withering Hex:withering hex').
card_rarity('withering hex'/'ONS', 'Uncommon').
card_artist('withering hex'/'ONS', 'Greg & Tim Hildebrandt').
card_number('withering hex'/'ONS', '181').
card_multiverse_id('withering hex'/'ONS', '41150').

card_in_set('wooded foothills', 'ONS').
card_original_type('wooded foothills'/'ONS', 'Land').
card_original_text('wooded foothills'/'ONS', '{T}, Pay 1 life, Sacrifice Wooded Foothills: Search your library for a mountain or forest card and put it into play. Then shuffle your library.').
card_image_name('wooded foothills'/'ONS', 'wooded foothills').
card_uid('wooded foothills'/'ONS', 'ONS:Wooded Foothills:wooded foothills').
card_rarity('wooded foothills'/'ONS', 'Rare').
card_artist('wooded foothills'/'ONS', 'Rob Alexander').
card_number('wooded foothills'/'ONS', '330').
card_multiverse_id('wooded foothills'/'ONS', '39506').

card_in_set('words of war', 'ONS').
card_original_type('words of war'/'ONS', 'Enchantment').
card_original_text('words of war'/'ONS', '{1}: The next time you would draw a card this turn, Words of War deals 2 damage to target creature or player instead.').
card_first_print('words of war', 'ONS').
card_image_name('words of war'/'ONS', 'words of war').
card_uid('words of war'/'ONS', 'ONS:Words of War:words of war').
card_rarity('words of war'/'ONS', 'Rare').
card_artist('words of war'/'ONS', 'Justin Sweet').
card_number('words of war'/'ONS', '244').
card_flavor_text('words of war'/'ONS', '\"Passions can\'t be shackled by laws or mastered with logic. The choice is freedom or death.\"\n—Volume IV, The Book of Fire').
card_multiverse_id('words of war'/'ONS', '40191').

card_in_set('words of waste', 'ONS').
card_original_type('words of waste'/'ONS', 'Enchantment').
card_original_text('words of waste'/'ONS', '{1}: The next time you would draw a card this turn, each opponent discards a card from his or her hand instead.').
card_first_print('words of waste', 'ONS').
card_image_name('words of waste'/'ONS', 'words of waste').
card_uid('words of waste'/'ONS', 'ONS:Words of Waste:words of waste').
card_rarity('words of waste'/'ONS', 'Rare').
card_artist('words of waste'/'ONS', 'Jerry Tiritilli').
card_number('words of waste'/'ONS', '182').
card_flavor_text('words of waste'/'ONS', '\"Terror corrupts order and paralyzes instinct.\"\n—Volume III, The Book of Decay').
card_multiverse_id('words of waste'/'ONS', '40190').

card_in_set('words of wilding', 'ONS').
card_original_type('words of wilding'/'ONS', 'Enchantment').
card_original_text('words of wilding'/'ONS', '{1}: The next time you would draw a card this turn, put a 2/2 green Bear creature token into play instead.').
card_first_print('words of wilding', 'ONS').
card_image_name('words of wilding'/'ONS', 'words of wilding').
card_uid('words of wilding'/'ONS', 'ONS:Words of Wilding:words of wilding').
card_rarity('words of wilding'/'ONS', 'Rare').
card_artist('words of wilding'/'ONS', 'Wayne England').
card_number('words of wilding'/'ONS', '305').
card_flavor_text('words of wilding'/'ONS', '\"Instinct is undaunted by terror, unchained by logic. It is the path from which all other paths diverge.\"\n—Volume V, The Book of Life').
card_multiverse_id('words of wilding'/'ONS', '39684').

card_in_set('words of wind', 'ONS').
card_original_type('words of wind'/'ONS', 'Enchantment').
card_original_text('words of wind'/'ONS', '{1}: The next time you would draw a card this turn, each player returns a permanent he or she controls to its owner\'s hand instead.').
card_first_print('words of wind', 'ONS').
card_image_name('words of wind'/'ONS', 'words of wind').
card_uid('words of wind'/'ONS', 'ONS:Words of Wind:words of wind').
card_rarity('words of wind'/'ONS', 'Rare').
card_artist('words of wind'/'ONS', 'Eric Peterson').
card_number('words of wind'/'ONS', '122').
card_flavor_text('words of wind'/'ONS', '\"Be logical in all things. Do not allow instinct or passion to cloud your mind.\"\n—Volume II, The Book of Insight').
card_multiverse_id('words of wind'/'ONS', '40188').

card_in_set('words of worship', 'ONS').
card_original_type('words of worship'/'ONS', 'Enchantment').
card_original_text('words of worship'/'ONS', '{1}: The next time you would draw a card this turn, you gain 5 life instead.').
card_first_print('words of worship', 'ONS').
card_image_name('words of worship'/'ONS', 'words of worship').
card_uid('words of worship'/'ONS', 'ONS:Words of Worship:words of worship').
card_rarity('words of worship'/'ONS', 'Rare').
card_artist('words of worship'/'ONS', 'Rebecca Guay').
card_number('words of worship'/'ONS', '61').
card_flavor_text('words of worship'/'ONS', '\"The faithful don\'t succumb to terror, nor are they ruled by passion. They adhere to order, for order is life.\"\n—Volume I, The Book of Faith').
card_multiverse_id('words of worship'/'ONS', '40189').

card_in_set('wretched anurid', 'ONS').
card_original_type('wretched anurid'/'ONS', 'Creature — Zombie Beast').
card_original_text('wretched anurid'/'ONS', 'Whenever another creature comes into play, you lose 1 life.').
card_first_print('wretched anurid', 'ONS').
card_image_name('wretched anurid'/'ONS', 'wretched anurid').
card_uid('wretched anurid'/'ONS', 'ONS:Wretched Anurid:wretched anurid').
card_rarity('wretched anurid'/'ONS', 'Common').
card_artist('wretched anurid'/'ONS', 'Glen Angus').
card_number('wretched anurid'/'ONS', '183').
card_flavor_text('wretched anurid'/'ONS', 'The only prince inside this frog is the one it ate.').
card_multiverse_id('wretched anurid'/'ONS', '26689').
