% Duel Decks Anthology, Garruk vs. Liliana

set('DD3_GVL').
set_name('DD3_GVL', 'Duel Decks Anthology, Garruk vs. Liliana').
set_release_date('DD3_GVL', '2014-12-05').
set_border('DD3_GVL', 'black').
set_type('DD3_GVL', 'duel deck').

card_in_set('albino troll', 'DD3_GVL').
card_original_type('albino troll'/'DD3_GVL', 'Creature — Troll').
card_original_text('albino troll'/'DD3_GVL', 'Echo {1}{G} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\n{1}{G}: Regenerate Albino Troll.').
card_image_name('albino troll'/'DD3_GVL', 'albino troll').
card_uid('albino troll'/'DD3_GVL', 'DD3_GVL:Albino Troll:albino troll').
card_rarity('albino troll'/'DD3_GVL', 'Uncommon').
card_artist('albino troll'/'DD3_GVL', 'Paolo Parente').
card_number('albino troll'/'DD3_GVL', '3').
card_multiverse_id('albino troll'/'DD3_GVL', '393878').

card_in_set('bad moon', 'DD3_GVL').
card_original_type('bad moon'/'DD3_GVL', 'Enchantment').
card_original_text('bad moon'/'DD3_GVL', 'Black creatures get +1/+1.').
card_image_name('bad moon'/'DD3_GVL', 'bad moon').
card_uid('bad moon'/'DD3_GVL', 'DD3_GVL:Bad Moon:bad moon').
card_rarity('bad moon'/'DD3_GVL', 'Rare').
card_artist('bad moon'/'DD3_GVL', 'Gary Leach').
card_number('bad moon'/'DD3_GVL', '48').
card_multiverse_id('bad moon'/'DD3_GVL', '393879').

card_in_set('basking rootwalla', 'DD3_GVL').
card_original_type('basking rootwalla'/'DD3_GVL', 'Creature — Lizard').
card_original_text('basking rootwalla'/'DD3_GVL', '{1}{G}: Basking Rootwalla gets +2/+2 until end of turn. Activate this ability only once each turn.\nMadness {0} (If you discard this card, you may cast it for its madness cost instead of putting it into your graveyard.)').
card_image_name('basking rootwalla'/'DD3_GVL', 'basking rootwalla').
card_uid('basking rootwalla'/'DD3_GVL', 'DD3_GVL:Basking Rootwalla:basking rootwalla').
card_rarity('basking rootwalla'/'DD3_GVL', 'Common').
card_artist('basking rootwalla'/'DD3_GVL', 'Heather Hudson').
card_number('basking rootwalla'/'DD3_GVL', '2').
card_multiverse_id('basking rootwalla'/'DD3_GVL', '393880').

card_in_set('beast attack', 'DD3_GVL').
card_original_type('beast attack'/'DD3_GVL', 'Instant').
card_original_text('beast attack'/'DD3_GVL', 'Put a 4/4 green Beast creature token onto the battlefield.\nFlashback {2}{G}{G}{G} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('beast attack'/'DD3_GVL', 'beast attack').
card_uid('beast attack'/'DD3_GVL', 'DD3_GVL:Beast Attack:beast attack').
card_rarity('beast attack'/'DD3_GVL', 'Uncommon').
card_artist('beast attack'/'DD3_GVL', 'Ciruelo').
card_number('beast attack'/'DD3_GVL', '23').
card_multiverse_id('beast attack'/'DD3_GVL', '393881').

card_in_set('blastoderm', 'DD3_GVL').
card_original_type('blastoderm'/'DD3_GVL', 'Creature — Beast').
card_original_text('blastoderm'/'DD3_GVL', 'Shroud (This creature can\'t be the target of spells or abilities.)\nFading 3 (This creature enters the battlefield with three fade counters on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)').
card_image_name('blastoderm'/'DD3_GVL', 'blastoderm').
card_uid('blastoderm'/'DD3_GVL', 'DD3_GVL:Blastoderm:blastoderm').
card_rarity('blastoderm'/'DD3_GVL', 'Common').
card_artist('blastoderm'/'DD3_GVL', 'Nils Hamm').
card_number('blastoderm'/'DD3_GVL', '7').
card_multiverse_id('blastoderm'/'DD3_GVL', '393882').

card_in_set('corrupt', 'DD3_GVL').
card_original_type('corrupt'/'DD3_GVL', 'Sorcery').
card_original_text('corrupt'/'DD3_GVL', 'Corrupt deals damage equal to the number of Swamps you control to target creature or player. You gain life equal to the damage dealt this way.').
card_image_name('corrupt'/'DD3_GVL', 'corrupt').
card_uid('corrupt'/'DD3_GVL', 'DD3_GVL:Corrupt:corrupt').
card_rarity('corrupt'/'DD3_GVL', 'Uncommon').
card_artist('corrupt'/'DD3_GVL', 'Dave Allsop').
card_number('corrupt'/'DD3_GVL', '57').
card_flavor_text('corrupt'/'DD3_GVL', 'There are things beneath the murk for whom the bog is a stew, a thin broth in need of meat.').
card_multiverse_id('corrupt'/'DD3_GVL', '393883').

card_in_set('deathgreeter', 'DD3_GVL').
card_original_type('deathgreeter'/'DD3_GVL', 'Creature — Human Shaman').
card_original_text('deathgreeter'/'DD3_GVL', 'Whenever another creature dies, you may gain 1 life.').
card_image_name('deathgreeter'/'DD3_GVL', 'deathgreeter').
card_uid('deathgreeter'/'DD3_GVL', 'DD3_GVL:Deathgreeter:deathgreeter').
card_rarity('deathgreeter'/'DD3_GVL', 'Common').
card_artist('deathgreeter'/'DD3_GVL', 'Dominick Domingo').
card_number('deathgreeter'/'DD3_GVL', '33').
card_flavor_text('deathgreeter'/'DD3_GVL', '\"The bones of allies grant wisdom. The bones of enemies grant strength. The bones of dragons grant life eternal.\"').
card_multiverse_id('deathgreeter'/'DD3_GVL', '393884').

card_in_set('drudge skeletons', 'DD3_GVL').
card_original_type('drudge skeletons'/'DD3_GVL', 'Creature — Skeleton').
card_original_text('drudge skeletons'/'DD3_GVL', '{B}: Regenerate Drudge Skeletons.').
card_image_name('drudge skeletons'/'DD3_GVL', 'drudge skeletons').
card_uid('drudge skeletons'/'DD3_GVL', 'DD3_GVL:Drudge Skeletons:drudge skeletons').
card_rarity('drudge skeletons'/'DD3_GVL', 'Common').
card_artist('drudge skeletons'/'DD3_GVL', 'Daarken').
card_number('drudge skeletons'/'DD3_GVL', '36').
card_flavor_text('drudge skeletons'/'DD3_GVL', '\"The dead make good soldiers. They can\'t disobey orders, never surrender, and don\'t stop fighting when a random body part falls off.\"\n—Nevinyrral, Necromancer\'s Handbook').
card_multiverse_id('drudge skeletons'/'DD3_GVL', '393885').

card_in_set('elephant guide', 'DD3_GVL').
card_original_type('elephant guide'/'DD3_GVL', 'Enchantment — Aura').
card_original_text('elephant guide'/'DD3_GVL', 'Enchant creature\nEnchanted creature gets +3/+3.\nWhen enchanted creature dies, put a 3/3 green Elephant creature token onto the battlefield.').
card_image_name('elephant guide'/'DD3_GVL', 'elephant guide').
card_uid('elephant guide'/'DD3_GVL', 'DD3_GVL:Elephant Guide:elephant guide').
card_rarity('elephant guide'/'DD3_GVL', 'Uncommon').
card_artist('elephant guide'/'DD3_GVL', 'Jim Nelson').
card_number('elephant guide'/'DD3_GVL', '18').
card_flavor_text('elephant guide'/'DD3_GVL', 'Nature\'s strength outlives the strong.').
card_multiverse_id('elephant guide'/'DD3_GVL', '393886').

card_in_set('enslave', 'DD3_GVL').
card_original_type('enslave'/'DD3_GVL', 'Enchantment — Aura').
card_original_text('enslave'/'DD3_GVL', 'Enchant creature\nYou control enchanted creature.\nAt the beginning of your upkeep, enchanted creature deals 1 damage to its owner.').
card_image_name('enslave'/'DD3_GVL', 'enslave').
card_uid('enslave'/'DD3_GVL', 'DD3_GVL:Enslave:enslave').
card_rarity('enslave'/'DD3_GVL', 'Uncommon').
card_artist('enslave'/'DD3_GVL', 'Zoltan Boros & Gabor Szikszai').
card_number('enslave'/'DD3_GVL', '58').
card_multiverse_id('enslave'/'DD3_GVL', '393887').

card_in_set('faerie macabre', 'DD3_GVL').
card_original_type('faerie macabre'/'DD3_GVL', 'Creature — Faerie Rogue').
card_original_text('faerie macabre'/'DD3_GVL', 'Flying\nDiscard Faerie Macabre: Exile up to two target cards from graveyards.').
card_image_name('faerie macabre'/'DD3_GVL', 'faerie macabre').
card_uid('faerie macabre'/'DD3_GVL', 'DD3_GVL:Faerie Macabre:faerie macabre').
card_rarity('faerie macabre'/'DD3_GVL', 'Common').
card_artist('faerie macabre'/'DD3_GVL', 'rk post').
card_number('faerie macabre'/'DD3_GVL', '42').
card_flavor_text('faerie macabre'/'DD3_GVL', 'The line between dream and death is gauzy and fragile. She leads those too near it from one side to the other.').
card_multiverse_id('faerie macabre'/'DD3_GVL', '393888').

card_in_set('fleshbag marauder', 'DD3_GVL').
card_original_type('fleshbag marauder'/'DD3_GVL', 'Creature — Zombie Warrior').
card_original_text('fleshbag marauder'/'DD3_GVL', 'When Fleshbag Marauder enters the battlefield, each player sacrifices a creature.').
card_image_name('fleshbag marauder'/'DD3_GVL', 'fleshbag marauder').
card_uid('fleshbag marauder'/'DD3_GVL', 'DD3_GVL:Fleshbag Marauder:fleshbag marauder').
card_rarity('fleshbag marauder'/'DD3_GVL', 'Uncommon').
card_artist('fleshbag marauder'/'DD3_GVL', 'Pete Venters').
card_number('fleshbag marauder'/'DD3_GVL', '38').
card_flavor_text('fleshbag marauder'/'DD3_GVL', 'Grixis is a world where the only things found in abundance are death and decay. Corpses, whole or in part, are the standard currency among necromancers and demons.').
card_multiverse_id('fleshbag marauder'/'DD3_GVL', '393889').

card_in_set('forest', 'DD3_GVL').
card_original_type('forest'/'DD3_GVL', 'Basic Land — Forest').
card_original_text('forest'/'DD3_GVL', 'G').
card_image_name('forest'/'DD3_GVL', 'forest1').
card_uid('forest'/'DD3_GVL', 'DD3_GVL:Forest:forest1').
card_rarity('forest'/'DD3_GVL', 'Basic Land').
card_artist('forest'/'DD3_GVL', 'Rob Alexander').
card_number('forest'/'DD3_GVL', '28').
card_multiverse_id('forest'/'DD3_GVL', '393891').

card_in_set('forest', 'DD3_GVL').
card_original_type('forest'/'DD3_GVL', 'Basic Land — Forest').
card_original_text('forest'/'DD3_GVL', 'G').
card_image_name('forest'/'DD3_GVL', 'forest2').
card_uid('forest'/'DD3_GVL', 'DD3_GVL:Forest:forest2').
card_rarity('forest'/'DD3_GVL', 'Basic Land').
card_artist('forest'/'DD3_GVL', 'Glen Angus').
card_number('forest'/'DD3_GVL', '29').
card_multiverse_id('forest'/'DD3_GVL', '393892').

card_in_set('forest', 'DD3_GVL').
card_original_type('forest'/'DD3_GVL', 'Basic Land — Forest').
card_original_text('forest'/'DD3_GVL', 'G').
card_image_name('forest'/'DD3_GVL', 'forest3').
card_uid('forest'/'DD3_GVL', 'DD3_GVL:Forest:forest3').
card_rarity('forest'/'DD3_GVL', 'Basic Land').
card_artist('forest'/'DD3_GVL', 'Steven Belledin').
card_number('forest'/'DD3_GVL', '30').
card_multiverse_id('forest'/'DD3_GVL', '393890').

card_in_set('forest', 'DD3_GVL').
card_original_type('forest'/'DD3_GVL', 'Basic Land — Forest').
card_original_text('forest'/'DD3_GVL', 'G').
card_image_name('forest'/'DD3_GVL', 'forest4').
card_uid('forest'/'DD3_GVL', 'DD3_GVL:Forest:forest4').
card_rarity('forest'/'DD3_GVL', 'Basic Land').
card_artist('forest'/'DD3_GVL', 'Jim Nelson').
card_number('forest'/'DD3_GVL', '31').
card_multiverse_id('forest'/'DD3_GVL', '393893').

card_in_set('garruk wildspeaker', 'DD3_GVL').
card_original_type('garruk wildspeaker'/'DD3_GVL', 'Planeswalker — Garruk').
card_original_text('garruk wildspeaker'/'DD3_GVL', '+1: Untap two target lands.\n−1: Put a 3/3 green Beast creature token onto the battlefield.\n−4: Creatures you control get +3/+3 and gain trample until end of turn.').
card_image_name('garruk wildspeaker'/'DD3_GVL', 'garruk wildspeaker').
card_uid('garruk wildspeaker'/'DD3_GVL', 'DD3_GVL:Garruk Wildspeaker:garruk wildspeaker').
card_rarity('garruk wildspeaker'/'DD3_GVL', 'Mythic Rare').
card_artist('garruk wildspeaker'/'DD3_GVL', 'Terese Nielsen').
card_number('garruk wildspeaker'/'DD3_GVL', '1').
card_multiverse_id('garruk wildspeaker'/'DD3_GVL', '393894').

card_in_set('genju of the cedars', 'DD3_GVL').
card_original_type('genju of the cedars'/'DD3_GVL', 'Enchantment — Aura').
card_original_text('genju of the cedars'/'DD3_GVL', 'Enchant Forest\n{2}: Enchanted Forest becomes a 4/4 green Spirit creature until end of turn. It\'s still a land.\nWhen enchanted Forest is put into a graveyard, you may return Genju of the Cedars from your graveyard to your hand.').
card_image_name('genju of the cedars'/'DD3_GVL', 'genju of the cedars').
card_uid('genju of the cedars'/'DD3_GVL', 'DD3_GVL:Genju of the Cedars:genju of the cedars').
card_rarity('genju of the cedars'/'DD3_GVL', 'Uncommon').
card_artist('genju of the cedars'/'DD3_GVL', 'Arnie Swekel').
card_number('genju of the cedars'/'DD3_GVL', '13').
card_multiverse_id('genju of the cedars'/'DD3_GVL', '393895').

card_in_set('genju of the fens', 'DD3_GVL').
card_original_type('genju of the fens'/'DD3_GVL', 'Enchantment — Aura').
card_original_text('genju of the fens'/'DD3_GVL', 'Enchant Swamp\n{2}: Until end of turn, enchanted Swamp becomes a 2/2 black Spirit creature with \"{B}: This creature gets +1/+1 until end of turn.\" It\'s still a land.\nWhen enchanted Swamp is put into a graveyard, you may return Genju of the Fens from your graveyard to your hand.').
card_image_name('genju of the fens'/'DD3_GVL', 'genju of the fens').
card_uid('genju of the fens'/'DD3_GVL', 'DD3_GVL:Genju of the Fens:genju of the fens').
card_rarity('genju of the fens'/'DD3_GVL', 'Uncommon').
card_artist('genju of the fens'/'DD3_GVL', 'Tsutomu Kawade').
card_number('genju of the fens'/'DD3_GVL', '47').
card_multiverse_id('genju of the fens'/'DD3_GVL', '393896').

card_in_set('ghost-lit stalker', 'DD3_GVL').
card_original_type('ghost-lit stalker'/'DD3_GVL', 'Creature — Spirit').
card_original_text('ghost-lit stalker'/'DD3_GVL', '{4}{B}, {T}: Target player discards two cards. Activate this ability only any time you could cast a sorcery.\nChannel — {5}{B}{B}, Discard Ghost-Lit Stalker: Target player discards four cards. Activate this ability only any time you could cast a sorcery.').
card_image_name('ghost-lit stalker'/'DD3_GVL', 'ghost-lit stalker').
card_uid('ghost-lit stalker'/'DD3_GVL', 'DD3_GVL:Ghost-Lit Stalker:ghost-lit stalker').
card_rarity('ghost-lit stalker'/'DD3_GVL', 'Uncommon').
card_artist('ghost-lit stalker'/'DD3_GVL', 'Hideaki Takamura').
card_number('ghost-lit stalker'/'DD3_GVL', '34').
card_multiverse_id('ghost-lit stalker'/'DD3_GVL', '393897').

card_in_set('giant growth', 'DD3_GVL').
card_original_type('giant growth'/'DD3_GVL', 'Instant').
card_original_text('giant growth'/'DD3_GVL', 'Target creature gets +3/+3 until end of turn.').
card_image_name('giant growth'/'DD3_GVL', 'giant growth').
card_uid('giant growth'/'DD3_GVL', 'DD3_GVL:Giant Growth:giant growth').
card_rarity('giant growth'/'DD3_GVL', 'Common').
card_artist('giant growth'/'DD3_GVL', 'Matt Cavotta').
card_number('giant growth'/'DD3_GVL', '14').
card_multiverse_id('giant growth'/'DD3_GVL', '393898').

card_in_set('harmonize', 'DD3_GVL').
card_original_type('harmonize'/'DD3_GVL', 'Sorcery').
card_original_text('harmonize'/'DD3_GVL', 'Draw three cards.').
card_image_name('harmonize'/'DD3_GVL', 'harmonize').
card_uid('harmonize'/'DD3_GVL', 'DD3_GVL:Harmonize:harmonize').
card_rarity('harmonize'/'DD3_GVL', 'Uncommon').
card_artist('harmonize'/'DD3_GVL', 'Paul Lee').
card_number('harmonize'/'DD3_GVL', '21').
card_flavor_text('harmonize'/'DD3_GVL', '\"Words lie. People lie. The land tells the truth.\"').
card_multiverse_id('harmonize'/'DD3_GVL', '393899').

card_in_set('hideous end', 'DD3_GVL').
card_original_type('hideous end'/'DD3_GVL', 'Instant').
card_original_text('hideous end'/'DD3_GVL', 'Destroy target nonblack creature. Its controller loses 2 life.').
card_image_name('hideous end'/'DD3_GVL', 'hideous end').
card_uid('hideous end'/'DD3_GVL', 'DD3_GVL:Hideous End:hideous end').
card_rarity('hideous end'/'DD3_GVL', 'Common').
card_artist('hideous end'/'DD3_GVL', 'Zoltan Boros & Gabor Szikszai').
card_number('hideous end'/'DD3_GVL', '52').
card_flavor_text('hideous end'/'DD3_GVL', '\"A little dark magic won\'t stop me. The worse the curse, the better the prize.\"\n—Radavi, Joraga relic hunter, last words').
card_multiverse_id('hideous end'/'DD3_GVL', '393900').

card_in_set('howling banshee', 'DD3_GVL').
card_original_type('howling banshee'/'DD3_GVL', 'Creature — Spirit').
card_original_text('howling banshee'/'DD3_GVL', 'Flying\nWhen Howling Banshee enters the battlefield, each player loses 3 life.').
card_image_name('howling banshee'/'DD3_GVL', 'howling banshee').
card_uid('howling banshee'/'DD3_GVL', 'DD3_GVL:Howling Banshee:howling banshee').
card_rarity('howling banshee'/'DD3_GVL', 'Uncommon').
card_artist('howling banshee'/'DD3_GVL', 'Andrew Robinson').
card_number('howling banshee'/'DD3_GVL', '43').
card_flavor_text('howling banshee'/'DD3_GVL', 'Villagers cloaked the town in magical silence, but their ears still bled.').
card_multiverse_id('howling banshee'/'DD3_GVL', '393901').

card_in_set('ichor slick', 'DD3_GVL').
card_original_type('ichor slick'/'DD3_GVL', 'Sorcery').
card_original_text('ichor slick'/'DD3_GVL', 'Target creature gets -3/-3 until end of turn.\nCycling {2} ({2}, Discard this card: Draw a card.)\nMadness {3}{B} (If you discard this card, you may cast it for its madness cost instead of putting it into your graveyard.)').
card_image_name('ichor slick'/'DD3_GVL', 'ichor slick').
card_uid('ichor slick'/'DD3_GVL', 'DD3_GVL:Ichor Slick:ichor slick').
card_rarity('ichor slick'/'DD3_GVL', 'Common').
card_artist('ichor slick'/'DD3_GVL', 'Zoltan Boros & Gabor Szikszai').
card_number('ichor slick'/'DD3_GVL', '51').
card_multiverse_id('ichor slick'/'DD3_GVL', '393902').

card_in_set('indrik stomphowler', 'DD3_GVL').
card_original_type('indrik stomphowler'/'DD3_GVL', 'Creature — Beast').
card_original_text('indrik stomphowler'/'DD3_GVL', 'When Indrik Stomphowler enters the battlefield, destroy target artifact or enchantment.').
card_image_name('indrik stomphowler'/'DD3_GVL', 'indrik stomphowler').
card_uid('indrik stomphowler'/'DD3_GVL', 'DD3_GVL:Indrik Stomphowler:indrik stomphowler').
card_rarity('indrik stomphowler'/'DD3_GVL', 'Uncommon').
card_artist('indrik stomphowler'/'DD3_GVL', 'Carl Critchlow').
card_number('indrik stomphowler'/'DD3_GVL', '10').
card_flavor_text('indrik stomphowler'/'DD3_GVL', 'An indrik\'s howl has destructive power much subtler than that of its crushing foot.').
card_multiverse_id('indrik stomphowler'/'DD3_GVL', '393903').

card_in_set('invigorate', 'DD3_GVL').
card_original_type('invigorate'/'DD3_GVL', 'Instant').
card_original_text('invigorate'/'DD3_GVL', 'If you control a Forest, rather than pay Invigorate\'s mana cost, you may have an opponent gain 3 life.\nTarget creature gets +4/+4 until end of turn.').
card_image_name('invigorate'/'DD3_GVL', 'invigorate').
card_uid('invigorate'/'DD3_GVL', 'DD3_GVL:Invigorate:invigorate').
card_rarity('invigorate'/'DD3_GVL', 'Common').
card_artist('invigorate'/'DD3_GVL', 'Dan Frazier').
card_number('invigorate'/'DD3_GVL', '19').
card_multiverse_id('invigorate'/'DD3_GVL', '393904').

card_in_set('keening banshee', 'DD3_GVL').
card_original_type('keening banshee'/'DD3_GVL', 'Creature — Spirit').
card_original_text('keening banshee'/'DD3_GVL', 'Flying\nWhen Keening Banshee enters the battlefield, target creature gets -2/-2 until end of turn.').
card_image_name('keening banshee'/'DD3_GVL', 'keening banshee').
card_uid('keening banshee'/'DD3_GVL', 'DD3_GVL:Keening Banshee:keening banshee').
card_rarity('keening banshee'/'DD3_GVL', 'Uncommon').
card_artist('keening banshee'/'DD3_GVL', 'Robert Bliss').
card_number('keening banshee'/'DD3_GVL', '44').
card_flavor_text('keening banshee'/'DD3_GVL', 'Her cold wail echoes in the alleys and under-eaves, finding the unfortunate and turning their blood to ice.').
card_multiverse_id('keening banshee'/'DD3_GVL', '393905').

card_in_set('krosan tusker', 'DD3_GVL').
card_original_type('krosan tusker'/'DD3_GVL', 'Creature — Boar Beast').
card_original_text('krosan tusker'/'DD3_GVL', 'Cycling {2}{G} ({2}{G}, Discard this card: Draw a card.)\nWhen you cycle Krosan Tusker, you may search your library for a basic land card, reveal that card, put it into your hand, then shuffle your library.').
card_image_name('krosan tusker'/'DD3_GVL', 'krosan tusker').
card_uid('krosan tusker'/'DD3_GVL', 'DD3_GVL:Krosan Tusker:krosan tusker').
card_rarity('krosan tusker'/'DD3_GVL', 'Common').
card_artist('krosan tusker'/'DD3_GVL', 'Kev Walker').
card_number('krosan tusker'/'DD3_GVL', '11').
card_multiverse_id('krosan tusker'/'DD3_GVL', '393906').

card_in_set('lignify', 'DD3_GVL').
card_original_type('lignify'/'DD3_GVL', 'Tribal Enchantment — Treefolk Aura').
card_original_text('lignify'/'DD3_GVL', 'Enchant creature\nEnchanted creature loses all abilities and is a Treefolk with base power and toughness 0/4.').
card_image_name('lignify'/'DD3_GVL', 'lignify').
card_uid('lignify'/'DD3_GVL', 'DD3_GVL:Lignify:lignify').
card_rarity('lignify'/'DD3_GVL', 'Common').
card_artist('lignify'/'DD3_GVL', 'Jesper Ejsing').
card_number('lignify'/'DD3_GVL', '16').
card_flavor_text('lignify'/'DD3_GVL', 'Bulgo paused, puzzled. What was that rustling sound, and why did he feel so stiff? And how could his feet be so thirsty?').
card_multiverse_id('lignify'/'DD3_GVL', '393907').

card_in_set('liliana vess', 'DD3_GVL').
card_original_type('liliana vess'/'DD3_GVL', 'Planeswalker — Liliana').
card_original_text('liliana vess'/'DD3_GVL', '+1: Target player discards a card.\n−2: Search your library for a card, then shuffle your library and put that card on top of it.\n−8: Put all creature cards in all graveyards onto the battlefield under your control.').
card_image_name('liliana vess'/'DD3_GVL', 'liliana vess').
card_uid('liliana vess'/'DD3_GVL', 'DD3_GVL:Liliana Vess:liliana vess').
card_rarity('liliana vess'/'DD3_GVL', 'Mythic Rare').
card_artist('liliana vess'/'DD3_GVL', 'Terese Nielsen').
card_number('liliana vess'/'DD3_GVL', '32').
card_multiverse_id('liliana vess'/'DD3_GVL', '393908').

card_in_set('mutilate', 'DD3_GVL').
card_original_type('mutilate'/'DD3_GVL', 'Sorcery').
card_original_text('mutilate'/'DD3_GVL', 'All creatures get -1/-1 until end of turn for each Swamp you control.').
card_image_name('mutilate'/'DD3_GVL', 'mutilate').
card_uid('mutilate'/'DD3_GVL', 'DD3_GVL:Mutilate:mutilate').
card_rarity('mutilate'/'DD3_GVL', 'Rare').
card_artist('mutilate'/'DD3_GVL', 'Zoltan Boros & Gabor Szikszai').
card_number('mutilate'/'DD3_GVL', '55').
card_flavor_text('mutilate'/'DD3_GVL', 'Liliana developed a fondness for the moment when roaring becomes squealing.').
card_multiverse_id('mutilate'/'DD3_GVL', '393909').

card_in_set('nature\'s lore', 'DD3_GVL').
card_original_type('nature\'s lore'/'DD3_GVL', 'Sorcery').
card_original_text('nature\'s lore'/'DD3_GVL', 'Search your library for a Forest card and put that card onto the battlefield. Then shuffle your library.').
card_image_name('nature\'s lore'/'DD3_GVL', 'nature\'s lore').
card_uid('nature\'s lore'/'DD3_GVL', 'DD3_GVL:Nature\'s Lore:nature\'s lore').
card_rarity('nature\'s lore'/'DD3_GVL', 'Common').
card_artist('nature\'s lore'/'DD3_GVL', 'Terese Nielsen').
card_number('nature\'s lore'/'DD3_GVL', '17').
card_flavor_text('nature\'s lore'/'DD3_GVL', 'Nature\'s secrets can be read on every tree, every branch, every leaf.').
card_multiverse_id('nature\'s lore'/'DD3_GVL', '393910').

card_in_set('overrun', 'DD3_GVL').
card_original_type('overrun'/'DD3_GVL', 'Sorcery').
card_original_text('overrun'/'DD3_GVL', 'Creatures you control get +3/+3 and gain trample until end of turn.').
card_image_name('overrun'/'DD3_GVL', 'overrun').
card_uid('overrun'/'DD3_GVL', 'DD3_GVL:Overrun:overrun').
card_rarity('overrun'/'DD3_GVL', 'Uncommon').
card_artist('overrun'/'DD3_GVL', 'Carl Critchlow').
card_number('overrun'/'DD3_GVL', '24').
card_flavor_text('overrun'/'DD3_GVL', 'Nature doesn\'t walk.').
card_multiverse_id('overrun'/'DD3_GVL', '393911').

card_in_set('phyrexian rager', 'DD3_GVL').
card_original_type('phyrexian rager'/'DD3_GVL', 'Creature — Horror').
card_original_text('phyrexian rager'/'DD3_GVL', 'When Phyrexian Rager enters the battlefield, you draw a card and you lose 1 life.').
card_image_name('phyrexian rager'/'DD3_GVL', 'phyrexian rager').
card_uid('phyrexian rager'/'DD3_GVL', 'DD3_GVL:Phyrexian Rager:phyrexian rager').
card_rarity('phyrexian rager'/'DD3_GVL', 'Common').
card_artist('phyrexian rager'/'DD3_GVL', 'Mark Tedin').
card_number('phyrexian rager'/'DD3_GVL', '39').
card_flavor_text('phyrexian rager'/'DD3_GVL', 'It takes no prisoners, but it keeps the choicest bits for Phyrexia.').
card_multiverse_id('phyrexian rager'/'DD3_GVL', '393912').

card_in_set('plated slagwurm', 'DD3_GVL').
card_original_type('plated slagwurm'/'DD3_GVL', 'Creature — Wurm').
card_original_text('plated slagwurm'/'DD3_GVL', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)').
card_image_name('plated slagwurm'/'DD3_GVL', 'plated slagwurm').
card_uid('plated slagwurm'/'DD3_GVL', 'DD3_GVL:Plated Slagwurm:plated slagwurm').
card_rarity('plated slagwurm'/'DD3_GVL', 'Rare').
card_artist('plated slagwurm'/'DD3_GVL', 'Justin Sweet').
card_number('plated slagwurm'/'DD3_GVL', '12').
card_flavor_text('plated slagwurm'/'DD3_GVL', 'Beneath the Tangle, the wurm tunnels stretch . . . wide as a stone\'s throw, long as forever, deep as you dare.').
card_multiverse_id('plated slagwurm'/'DD3_GVL', '393913').

card_in_set('polluted mire', 'DD3_GVL').
card_original_type('polluted mire'/'DD3_GVL', 'Land').
card_original_text('polluted mire'/'DD3_GVL', 'Polluted Mire enters the battlefield tapped.\n{T}: Add {B} to your mana pool.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_image_name('polluted mire'/'DD3_GVL', 'polluted mire').
card_uid('polluted mire'/'DD3_GVL', 'DD3_GVL:Polluted Mire:polluted mire').
card_rarity('polluted mire'/'DD3_GVL', 'Common').
card_artist('polluted mire'/'DD3_GVL', 'Stephen Daniele').
card_number('polluted mire'/'DD3_GVL', '59').
card_multiverse_id('polluted mire'/'DD3_GVL', '393914').

card_in_set('rancor', 'DD3_GVL').
card_original_type('rancor'/'DD3_GVL', 'Enchantment — Aura').
card_original_text('rancor'/'DD3_GVL', 'Enchant creature\nEnchanted creature gets +2/+0 and has trample.\nWhen Rancor is put into a graveyard from the battlefield, return Rancor to its owner\'s hand.').
card_image_name('rancor'/'DD3_GVL', 'rancor').
card_uid('rancor'/'DD3_GVL', 'DD3_GVL:Rancor:rancor').
card_rarity('rancor'/'DD3_GVL', 'Common').
card_artist('rancor'/'DD3_GVL', 'Kev Walker').
card_number('rancor'/'DD3_GVL', '15').
card_flavor_text('rancor'/'DD3_GVL', 'Hatred outlives the hateful.').
card_multiverse_id('rancor'/'DD3_GVL', '393915').

card_in_set('ravenous baloth', 'DD3_GVL').
card_original_type('ravenous baloth'/'DD3_GVL', 'Creature — Beast').
card_original_text('ravenous baloth'/'DD3_GVL', 'Sacrifice a Beast: You gain 4 life.').
card_image_name('ravenous baloth'/'DD3_GVL', 'ravenous baloth').
card_uid('ravenous baloth'/'DD3_GVL', 'DD3_GVL:Ravenous Baloth:ravenous baloth').
card_rarity('ravenous baloth'/'DD3_GVL', 'Rare').
card_artist('ravenous baloth'/'DD3_GVL', 'Arnie Swekel').
card_number('ravenous baloth'/'DD3_GVL', '8').
card_flavor_text('ravenous baloth'/'DD3_GVL', '\"All we know about the Krosan Forest we have learned from those few who made it out alive.\"\n—Elvish refugee').
card_multiverse_id('ravenous baloth'/'DD3_GVL', '393916').

card_in_set('ravenous rats', 'DD3_GVL').
card_original_type('ravenous rats'/'DD3_GVL', 'Creature — Rat').
card_original_text('ravenous rats'/'DD3_GVL', 'When Ravenous Rats enters the battlefield, target opponent discards a card.').
card_image_name('ravenous rats'/'DD3_GVL', 'ravenous rats').
card_uid('ravenous rats'/'DD3_GVL', 'DD3_GVL:Ravenous Rats:ravenous rats').
card_rarity('ravenous rats'/'DD3_GVL', 'Common').
card_artist('ravenous rats'/'DD3_GVL', 'Carl Critchlow').
card_number('ravenous rats'/'DD3_GVL', '37').
card_flavor_text('ravenous rats'/'DD3_GVL', 'Nothing is sacred to rats. Everything is simply another meal.').
card_multiverse_id('ravenous rats'/'DD3_GVL', '393917').

card_in_set('rise from the grave', 'DD3_GVL').
card_original_type('rise from the grave'/'DD3_GVL', 'Sorcery').
card_original_text('rise from the grave'/'DD3_GVL', 'Put target creature card from a graveyard onto the battlefield under your control. That creature is a black Zombie in addition to its other colors and types.').
card_image_name('rise from the grave'/'DD3_GVL', 'rise from the grave').
card_uid('rise from the grave'/'DD3_GVL', 'DD3_GVL:Rise from the Grave:rise from the grave').
card_rarity('rise from the grave'/'DD3_GVL', 'Uncommon').
card_artist('rise from the grave'/'DD3_GVL', 'Vance Kovacs').
card_number('rise from the grave'/'DD3_GVL', '56').
card_flavor_text('rise from the grave'/'DD3_GVL', '\"Death is no excuse for disobedience.\"\n—Liliana Vess').
card_multiverse_id('rise from the grave'/'DD3_GVL', '393918').

card_in_set('rude awakening', 'DD3_GVL').
card_original_type('rude awakening'/'DD3_GVL', 'Sorcery').
card_original_text('rude awakening'/'DD3_GVL', 'Choose one — \n• Untap all lands you control.\n• Until end of turn, lands you control become 2/2 creatures that are still lands.\nEntwine {2}{G} (Choose both if you pay the entwine cost.)').
card_image_name('rude awakening'/'DD3_GVL', 'rude awakening').
card_uid('rude awakening'/'DD3_GVL', 'DD3_GVL:Rude Awakening:rude awakening').
card_rarity('rude awakening'/'DD3_GVL', 'Rare').
card_artist('rude awakening'/'DD3_GVL', 'Ittoku').
card_number('rude awakening'/'DD3_GVL', '22').
card_multiverse_id('rude awakening'/'DD3_GVL', '393919').

card_in_set('serrated arrows', 'DD3_GVL').
card_original_type('serrated arrows'/'DD3_GVL', 'Artifact').
card_original_text('serrated arrows'/'DD3_GVL', 'Serrated Arrows enters the battlefield with three arrowhead counters on it.\nAt the beginning of your upkeep, if there are no arrowhead counters on Serrated Arrows, sacrifice it.\n{T}, Remove an arrowhead counter from Serrated Arrows: Put a -1/-1 counter on target creature.').
card_image_name('serrated arrows'/'DD3_GVL', 'serrated arrows').
card_uid('serrated arrows'/'DD3_GVL', 'DD3_GVL:Serrated Arrows:serrated arrows').
card_rarity('serrated arrows'/'DD3_GVL', 'Common').
card_artist('serrated arrows'/'DD3_GVL', 'John Avon').
card_number('serrated arrows'/'DD3_GVL', '20').
card_multiverse_id('serrated arrows'/'DD3_GVL', '393920').

card_in_set('sign in blood', 'DD3_GVL').
card_original_type('sign in blood'/'DD3_GVL', 'Sorcery').
card_original_text('sign in blood'/'DD3_GVL', 'Target player draws two cards and loses 2 life.').
card_image_name('sign in blood'/'DD3_GVL', 'sign in blood').
card_uid('sign in blood'/'DD3_GVL', 'DD3_GVL:Sign in Blood:sign in blood').
card_rarity('sign in blood'/'DD3_GVL', 'Common').
card_artist('sign in blood'/'DD3_GVL', 'Howard Lyon').
card_number('sign in blood'/'DD3_GVL', '49').
card_flavor_text('sign in blood'/'DD3_GVL', '\"You know I accept only one currency here, and yet you have sought me out. Why now do you hesitate?\"\n—Xathrid demon').
card_multiverse_id('sign in blood'/'DD3_GVL', '393921').

card_in_set('skeletal vampire', 'DD3_GVL').
card_original_type('skeletal vampire'/'DD3_GVL', 'Creature — Vampire Skeleton').
card_original_text('skeletal vampire'/'DD3_GVL', 'Flying\nWhen Skeletal Vampire enters the battlefield, put two 1/1 black Bat creature tokens with flying onto the battlefield.\n{3}{B}{B}, Sacrifice a Bat: Put two 1/1 black Bat creature tokens with flying onto the battlefield.\nSacrifice a Bat: Regenerate Skeletal Vampire.').
card_image_name('skeletal vampire'/'DD3_GVL', 'skeletal vampire').
card_uid('skeletal vampire'/'DD3_GVL', 'DD3_GVL:Skeletal Vampire:skeletal vampire').
card_rarity('skeletal vampire'/'DD3_GVL', 'Rare').
card_artist('skeletal vampire'/'DD3_GVL', 'Wayne Reynolds').
card_number('skeletal vampire'/'DD3_GVL', '46').
card_multiverse_id('skeletal vampire'/'DD3_GVL', '393922').

card_in_set('slippery karst', 'DD3_GVL').
card_original_type('slippery karst'/'DD3_GVL', 'Land').
card_original_text('slippery karst'/'DD3_GVL', 'Slippery Karst enters the battlefield tapped.\n{T}: Add {G} to your mana pool.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_image_name('slippery karst'/'DD3_GVL', 'slippery karst').
card_uid('slippery karst'/'DD3_GVL', 'DD3_GVL:Slippery Karst:slippery karst').
card_rarity('slippery karst'/'DD3_GVL', 'Common').
card_artist('slippery karst'/'DD3_GVL', 'Stephen Daniele').
card_number('slippery karst'/'DD3_GVL', '26').
card_multiverse_id('slippery karst'/'DD3_GVL', '393923').

card_in_set('snuff out', 'DD3_GVL').
card_original_type('snuff out'/'DD3_GVL', 'Instant').
card_original_text('snuff out'/'DD3_GVL', 'If you control a Swamp, you may pay 4 life rather than pay Snuff Out\'s mana cost.\nDestroy target nonblack creature. It can\'t be regenerated.').
card_image_name('snuff out'/'DD3_GVL', 'snuff out').
card_uid('snuff out'/'DD3_GVL', 'DD3_GVL:Snuff Out:snuff out').
card_rarity('snuff out'/'DD3_GVL', 'Common').
card_artist('snuff out'/'DD3_GVL', 'Steve Argyle').
card_number('snuff out'/'DD3_GVL', '53').
card_flavor_text('snuff out'/'DD3_GVL', '\"The bigger they are, the faster they rot.\"').
card_multiverse_id('snuff out'/'DD3_GVL', '393924').

card_in_set('stampeding wildebeests', 'DD3_GVL').
card_original_type('stampeding wildebeests'/'DD3_GVL', 'Creature — Antelope Beast').
card_original_text('stampeding wildebeests'/'DD3_GVL', 'Trample\nAt the beginning of your upkeep, return a green creature you control to its owner\'s hand.').
card_image_name('stampeding wildebeests'/'DD3_GVL', 'stampeding wildebeests').
card_uid('stampeding wildebeests'/'DD3_GVL', 'DD3_GVL:Stampeding Wildebeests:stampeding wildebeests').
card_rarity('stampeding wildebeests'/'DD3_GVL', 'Uncommon').
card_artist('stampeding wildebeests'/'DD3_GVL', 'Randy Gallegos').
card_number('stampeding wildebeests'/'DD3_GVL', '9').
card_flavor_text('stampeding wildebeests'/'DD3_GVL', 'The most violent and destructive storms in Femeref occur on cloudless days.').
card_multiverse_id('stampeding wildebeests'/'DD3_GVL', '393925').

card_in_set('swamp', 'DD3_GVL').
card_original_type('swamp'/'DD3_GVL', 'Basic Land — Swamp').
card_original_text('swamp'/'DD3_GVL', 'B').
card_image_name('swamp'/'DD3_GVL', 'swamp1').
card_uid('swamp'/'DD3_GVL', 'DD3_GVL:Swamp:swamp1').
card_rarity('swamp'/'DD3_GVL', 'Basic Land').
card_artist('swamp'/'DD3_GVL', 'Stephan Martiniere').
card_number('swamp'/'DD3_GVL', '60').
card_multiverse_id('swamp'/'DD3_GVL', '393928').

card_in_set('swamp', 'DD3_GVL').
card_original_type('swamp'/'DD3_GVL', 'Basic Land — Swamp').
card_original_text('swamp'/'DD3_GVL', 'B').
card_image_name('swamp'/'DD3_GVL', 'swamp2').
card_uid('swamp'/'DD3_GVL', 'DD3_GVL:Swamp:swamp2').
card_rarity('swamp'/'DD3_GVL', 'Basic Land').
card_artist('swamp'/'DD3_GVL', 'Christopher Moeller').
card_number('swamp'/'DD3_GVL', '61').
card_multiverse_id('swamp'/'DD3_GVL', '393927').

card_in_set('swamp', 'DD3_GVL').
card_original_type('swamp'/'DD3_GVL', 'Basic Land — Swamp').
card_original_text('swamp'/'DD3_GVL', 'B').
card_image_name('swamp'/'DD3_GVL', 'swamp3').
card_uid('swamp'/'DD3_GVL', 'DD3_GVL:Swamp:swamp3').
card_rarity('swamp'/'DD3_GVL', 'Basic Land').
card_artist('swamp'/'DD3_GVL', 'Anthony S. Waters').
card_number('swamp'/'DD3_GVL', '62').
card_multiverse_id('swamp'/'DD3_GVL', '393926').

card_in_set('swamp', 'DD3_GVL').
card_original_type('swamp'/'DD3_GVL', 'Basic Land — Swamp').
card_original_text('swamp'/'DD3_GVL', 'B').
card_image_name('swamp'/'DD3_GVL', 'swamp4').
card_uid('swamp'/'DD3_GVL', 'DD3_GVL:Swamp:swamp4').
card_rarity('swamp'/'DD3_GVL', 'Basic Land').
card_artist('swamp'/'DD3_GVL', 'Richard Wright').
card_number('swamp'/'DD3_GVL', '63').
card_multiverse_id('swamp'/'DD3_GVL', '393929').

card_in_set('tendrils of corruption', 'DD3_GVL').
card_original_type('tendrils of corruption'/'DD3_GVL', 'Instant').
card_original_text('tendrils of corruption'/'DD3_GVL', 'Tendrils of Corruption deals X damage to target creature and you gain X life, where X is the number of Swamps you control.').
card_image_name('tendrils of corruption'/'DD3_GVL', 'tendrils of corruption').
card_uid('tendrils of corruption'/'DD3_GVL', 'DD3_GVL:Tendrils of Corruption:tendrils of corruption').
card_rarity('tendrils of corruption'/'DD3_GVL', 'Common').
card_artist('tendrils of corruption'/'DD3_GVL', 'Vance Kovacs').
card_number('tendrils of corruption'/'DD3_GVL', '54').
card_flavor_text('tendrils of corruption'/'DD3_GVL', 'Darkness doesn\'t always send its minions to do its dirty work.').
card_multiverse_id('tendrils of corruption'/'DD3_GVL', '393930').

card_in_set('treetop village', 'DD3_GVL').
card_original_type('treetop village'/'DD3_GVL', 'Land').
card_original_text('treetop village'/'DD3_GVL', 'Treetop Village enters the battlefield tapped.\n{T}: Add {G} to your mana pool.\n{1}{G}: Treetop Village becomes a 3/3 green Ape creature with trample until end of turn. It\'s still a land.').
card_image_name('treetop village'/'DD3_GVL', 'treetop village').
card_uid('treetop village'/'DD3_GVL', 'DD3_GVL:Treetop Village:treetop village').
card_rarity('treetop village'/'DD3_GVL', 'Uncommon').
card_artist('treetop village'/'DD3_GVL', 'Rob Alexander').
card_number('treetop village'/'DD3_GVL', '27').
card_multiverse_id('treetop village'/'DD3_GVL', '393931').

card_in_set('twisted abomination', 'DD3_GVL').
card_original_type('twisted abomination'/'DD3_GVL', 'Creature — Zombie Mutant').
card_original_text('twisted abomination'/'DD3_GVL', '{B}: Regenerate Twisted Abomination.\nSwampcycling {2} ({2}, Discard this card: Search your library for a Swamp card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('twisted abomination'/'DD3_GVL', 'twisted abomination').
card_uid('twisted abomination'/'DD3_GVL', 'DD3_GVL:Twisted Abomination:twisted abomination').
card_rarity('twisted abomination'/'DD3_GVL', 'Common').
card_artist('twisted abomination'/'DD3_GVL', 'Daren Bader').
card_number('twisted abomination'/'DD3_GVL', '45').
card_multiverse_id('twisted abomination'/'DD3_GVL', '393932').

card_in_set('urborg syphon-mage', 'DD3_GVL').
card_original_type('urborg syphon-mage'/'DD3_GVL', 'Creature — Human Spellshaper').
card_original_text('urborg syphon-mage'/'DD3_GVL', '{2}{B}, {T}, Discard a card: Each other player loses 2 life. You gain life equal to the life lost this way.').
card_image_name('urborg syphon-mage'/'DD3_GVL', 'urborg syphon-mage').
card_uid('urborg syphon-mage'/'DD3_GVL', 'DD3_GVL:Urborg Syphon-Mage:urborg syphon-mage').
card_rarity('urborg syphon-mage'/'DD3_GVL', 'Common').
card_artist('urborg syphon-mage'/'DD3_GVL', 'Greg Staples').
card_number('urborg syphon-mage'/'DD3_GVL', '40').
card_flavor_text('urborg syphon-mage'/'DD3_GVL', '\"I have become a gourmet of sorts. Each soul has its own distinctive flavor. The art is in inviting the right company to the feast.\"').
card_multiverse_id('urborg syphon-mage'/'DD3_GVL', '393933').

card_in_set('vampire bats', 'DD3_GVL').
card_original_type('vampire bats'/'DD3_GVL', 'Creature — Bat').
card_original_text('vampire bats'/'DD3_GVL', 'Flying\n{B}: Vampire Bats gets +1/+0 until end of turn. Activate this ability no more than twice each turn.').
card_image_name('vampire bats'/'DD3_GVL', 'vampire bats').
card_uid('vampire bats'/'DD3_GVL', 'DD3_GVL:Vampire Bats:vampire bats').
card_rarity('vampire bats'/'DD3_GVL', 'Common').
card_artist('vampire bats'/'DD3_GVL', 'Chippy').
card_number('vampire bats'/'DD3_GVL', '35').
card_multiverse_id('vampire bats'/'DD3_GVL', '393934').

card_in_set('vicious hunger', 'DD3_GVL').
card_original_type('vicious hunger'/'DD3_GVL', 'Sorcery').
card_original_text('vicious hunger'/'DD3_GVL', 'Vicious Hunger deals 2 damage to target creature and you gain 2 life.').
card_image_name('vicious hunger'/'DD3_GVL', 'vicious hunger').
card_uid('vicious hunger'/'DD3_GVL', 'DD3_GVL:Vicious Hunger:vicious hunger').
card_rarity('vicious hunger'/'DD3_GVL', 'Common').
card_artist('vicious hunger'/'DD3_GVL', 'Massimilano Frezzato').
card_number('vicious hunger'/'DD3_GVL', '50').
card_flavor_text('vicious hunger'/'DD3_GVL', 'Only the most ravenous soul feeds on the health of the weak.').
card_multiverse_id('vicious hunger'/'DD3_GVL', '393935').

card_in_set('vine trellis', 'DD3_GVL').
card_original_type('vine trellis'/'DD3_GVL', 'Creature — Plant Wall').
card_original_text('vine trellis'/'DD3_GVL', 'Defender\n{T}: Add {G} to your mana pool.').
card_image_name('vine trellis'/'DD3_GVL', 'vine trellis').
card_uid('vine trellis'/'DD3_GVL', 'DD3_GVL:Vine Trellis:vine trellis').
card_rarity('vine trellis'/'DD3_GVL', 'Common').
card_artist('vine trellis'/'DD3_GVL', 'DiTerlizzi').
card_number('vine trellis'/'DD3_GVL', '4').
card_flavor_text('vine trellis'/'DD3_GVL', 'Nature twists knots stronger than any rope.').
card_multiverse_id('vine trellis'/'DD3_GVL', '393936').

card_in_set('wall of bone', 'DD3_GVL').
card_original_type('wall of bone'/'DD3_GVL', 'Creature — Skeleton Wall').
card_original_text('wall of bone'/'DD3_GVL', 'Defender\n{B}: Regenerate Wall of Bone.').
card_image_name('wall of bone'/'DD3_GVL', 'wall of bone').
card_uid('wall of bone'/'DD3_GVL', 'DD3_GVL:Wall of Bone:wall of bone').
card_rarity('wall of bone'/'DD3_GVL', 'Uncommon').
card_artist('wall of bone'/'DD3_GVL', 'Jaime Jones').
card_number('wall of bone'/'DD3_GVL', '41').
card_flavor_text('wall of bone'/'DD3_GVL', '\"Graveyards are sacrilege! A waste of perfectly good bones.\"\n—Keren-Dur, necromancer lord').
card_multiverse_id('wall of bone'/'DD3_GVL', '393937').

card_in_set('wild mongrel', 'DD3_GVL').
card_original_type('wild mongrel'/'DD3_GVL', 'Creature — Hound').
card_original_text('wild mongrel'/'DD3_GVL', 'Discard a card: Wild Mongrel gets +1/+1 and becomes the color of your choice until end of turn.').
card_image_name('wild mongrel'/'DD3_GVL', 'wild mongrel').
card_uid('wild mongrel'/'DD3_GVL', 'DD3_GVL:Wild Mongrel:wild mongrel').
card_rarity('wild mongrel'/'DD3_GVL', 'Common').
card_artist('wild mongrel'/'DD3_GVL', 'Anthony S. Waters').
card_number('wild mongrel'/'DD3_GVL', '5').
card_flavor_text('wild mongrel'/'DD3_GVL', 'It teaches you to play dead.').
card_multiverse_id('wild mongrel'/'DD3_GVL', '393938').

card_in_set('windstorm', 'DD3_GVL').
card_original_type('windstorm'/'DD3_GVL', 'Instant').
card_original_text('windstorm'/'DD3_GVL', 'Windstorm deals X damage to each creature with flying.').
card_image_name('windstorm'/'DD3_GVL', 'windstorm').
card_uid('windstorm'/'DD3_GVL', 'DD3_GVL:Windstorm:windstorm').
card_rarity('windstorm'/'DD3_GVL', 'Uncommon').
card_artist('windstorm'/'DD3_GVL', 'Rob Alexander').
card_number('windstorm'/'DD3_GVL', '25').
card_flavor_text('windstorm'/'DD3_GVL', '\"We don\'t like interlopers and thieves cluttering our skies.\"\n—Dionus, elvish archdruid').
card_multiverse_id('windstorm'/'DD3_GVL', '393939').

card_in_set('wirewood savage', 'DD3_GVL').
card_original_type('wirewood savage'/'DD3_GVL', 'Creature — Elf').
card_original_text('wirewood savage'/'DD3_GVL', 'Whenever a Beast enters the battlefield, you may draw a card.').
card_image_name('wirewood savage'/'DD3_GVL', 'wirewood savage').
card_uid('wirewood savage'/'DD3_GVL', 'DD3_GVL:Wirewood Savage:wirewood savage').
card_rarity('wirewood savage'/'DD3_GVL', 'Common').
card_artist('wirewood savage'/'DD3_GVL', 'DiTerlizzi').
card_number('wirewood savage'/'DD3_GVL', '6').
card_flavor_text('wirewood savage'/'DD3_GVL', '\"She is truly Wirewood\'s child now.\"\n—Elvish refugee').
card_multiverse_id('wirewood savage'/'DD3_GVL', '393940').
