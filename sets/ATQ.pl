% Antiquities

set('ATQ').
set_name('ATQ', 'Antiquities').
set_release_date('ATQ', '1994-03-01').
set_border('ATQ', 'black').
set_type('ATQ', 'expansion').

card_in_set('amulet of kroog', 'ATQ').
card_original_type('amulet of kroog'/'ATQ', 'Mono Artifact').
card_original_text('amulet of kroog'/'ATQ', '{2}: Prevent 1 damage to any target.').
card_first_print('amulet of kroog', 'ATQ').
card_image_name('amulet of kroog'/'ATQ', 'amulet of kroog').
card_uid('amulet of kroog'/'ATQ', 'ATQ:Amulet of Kroog:amulet of kroog').
card_rarity('amulet of kroog'/'ATQ', 'Common').
card_artist('amulet of kroog'/'ATQ', 'Margaret Organ-Kean').
card_flavor_text('amulet of kroog'/'ATQ', 'Among the first allies Urza gained were the people of Kroog. As a sign of friendship, Urza gave the healers of the city potent amulets; afterwards, thousands journeyed to Kroog in hope of healing, greatly adding to the city\'s glory.').
card_multiverse_id('amulet of kroog'/'ATQ', '992').

card_in_set('argivian archaeologist', 'ATQ').
card_original_type('argivian archaeologist'/'ATQ', 'Summon — Archaeologist').
card_original_text('argivian archaeologist'/'ATQ', '{W}{W} Tap to bring one artifact from your graveyard to your hand.').
card_first_print('argivian archaeologist', 'ATQ').
card_image_name('argivian archaeologist'/'ATQ', 'argivian archaeologist').
card_uid('argivian archaeologist'/'ATQ', 'ATQ:Argivian Archaeologist:argivian archaeologist').
card_rarity('argivian archaeologist'/'ATQ', 'Rare').
card_artist('argivian archaeologist'/'ATQ', 'Amy Weber').
card_flavor_text('argivian archaeologist'/'ATQ', 'Fascinated by the lore of ancient struggles, the Archaeologist searches incessantly for remnants of an earlier, more powerful era.').
card_multiverse_id('argivian archaeologist'/'ATQ', '1064').

card_in_set('argivian blacksmith', 'ATQ').
card_original_type('argivian blacksmith'/'ATQ', 'Summon — Smith').
card_original_text('argivian blacksmith'/'ATQ', 'Tap to prevent up to 2 damage to target artifact creature.').
card_first_print('argivian blacksmith', 'ATQ').
card_image_name('argivian blacksmith'/'ATQ', 'argivian blacksmith').
card_uid('argivian blacksmith'/'ATQ', 'ATQ:Argivian Blacksmith:argivian blacksmith').
card_rarity('argivian blacksmith'/'ATQ', 'Common').
card_artist('argivian blacksmith'/'ATQ', 'Kerstin Kaman').
card_flavor_text('argivian blacksmith'/'ATQ', 'Through years of study and training, the Blacksmiths of Argive became adept at reassembling the mangled remains of the strange, mechanical creatures abounding in their native land.').
card_multiverse_id('argivian blacksmith'/'ATQ', '1065').

card_in_set('argothian pixies', 'ATQ').
card_original_type('argothian pixies'/'ATQ', 'Summon — Faeries').
card_original_text('argothian pixies'/'ATQ', 'Cannot be blocked by artifact creatures. Any damage Argothian Pixies take from artifact creatures is reduced to 0.').
card_first_print('argothian pixies', 'ATQ').
card_image_name('argothian pixies'/'ATQ', 'argothian pixies').
card_uid('argothian pixies'/'ATQ', 'ATQ:Argothian Pixies:argothian pixies').
card_rarity('argothian pixies'/'ATQ', 'Common').
card_artist('argothian pixies'/'ATQ', 'Amy Weber').
card_flavor_text('argothian pixies'/'ATQ', 'After the rape of Argoth Forest during the rule of the artificers, the Pixies of Argoth bent their magic to more practical ends.').
card_multiverse_id('argothian pixies'/'ATQ', '1050').

card_in_set('argothian treefolk', 'ATQ').
card_original_type('argothian treefolk'/'ATQ', 'Summon — Treefolk').
card_original_text('argothian treefolk'/'ATQ', 'Any damage Argothian Treefolk take from an artifact source is reduced to 0.').
card_first_print('argothian treefolk', 'ATQ').
card_image_name('argothian treefolk'/'ATQ', 'argothian treefolk').
card_uid('argothian treefolk'/'ATQ', 'ATQ:Argothian Treefolk:argothian treefolk').
card_rarity('argothian treefolk'/'ATQ', 'Common').
card_artist('argothian treefolk'/'ATQ', 'Amy Weber').
card_flavor_text('argothian treefolk'/'ATQ', 'Haunting cries we hear in our dreams\nAs the forest dies, a death from\nmachines.').
card_multiverse_id('argothian treefolk'/'ATQ', '1051').

card_in_set('armageddon clock', 'ATQ').
card_original_type('armageddon clock'/'ATQ', 'Continuous Artifact').
card_original_text('armageddon clock'/'ATQ', 'Put one counter on Armageddon Clock during each of your upkeeps. At the end of your upkeep, each player takes damage equal to the number of counters on the Clock. Any player may spend {4} during any upkeep to remove a counter.').
card_first_print('armageddon clock', 'ATQ').
card_image_name('armageddon clock'/'ATQ', 'armageddon clock').
card_uid('armageddon clock'/'ATQ', 'ATQ:Armageddon Clock:armageddon clock').
card_rarity('armageddon clock'/'ATQ', 'Uncommon').
card_artist('armageddon clock'/'ATQ', 'Amy Weber').
card_multiverse_id('armageddon clock'/'ATQ', '993').

card_in_set('artifact blast', 'ATQ').
card_original_type('artifact blast'/'ATQ', 'Interrupt').
card_original_text('artifact blast'/'ATQ', 'Counters any artifact as it is being cast.').
card_first_print('artifact blast', 'ATQ').
card_image_name('artifact blast'/'ATQ', 'artifact blast').
card_uid('artifact blast'/'ATQ', 'ATQ:Artifact Blast:artifact blast').
card_rarity('artifact blast'/'ATQ', 'Common').
card_artist('artifact blast'/'ATQ', 'Mark Poole').
card_flavor_text('artifact blast'/'ATQ', 'The first line of defense against Urza and Mishra, the Artifact Blast achieved widespread fame until an unlucky mage discovered it was useless on the devices the brothers had already created.').
card_multiverse_id('artifact blast'/'ATQ', '1057').

card_in_set('artifact possession', 'ATQ').
card_original_type('artifact possession'/'ATQ', 'Enchant Artifact').
card_original_text('artifact possession'/'ATQ', 'Artifact Possession does 2 damage to target artifact\'s controller each time target artifact is tapped or its activation cost is paid. Has no effect if cast on a continuous artifact.').
card_first_print('artifact possession', 'ATQ').
card_image_name('artifact possession'/'ATQ', 'artifact possession').
card_uid('artifact possession'/'ATQ', 'ATQ:Artifact Possession:artifact possession').
card_rarity('artifact possession'/'ATQ', 'Common').
card_artist('artifact possession'/'ATQ', 'Christopher Rush').
card_flavor_text('artifact possession'/'ATQ', 'Any black mage could coax a Thraxodemon to inhabit a magical device.').
card_multiverse_id('artifact possession'/'ATQ', '1036').

card_in_set('artifact ward', 'ATQ').
card_original_type('artifact ward'/'ATQ', 'Enchant Creature').
card_original_text('artifact ward'/'ATQ', 'Target creature cannot be blocked by artifact creatures, and any damage taken from an artifact source is reduced to 0. Target creature is unaffected by any artifact effects that target it.').
card_first_print('artifact ward', 'ATQ').
card_image_name('artifact ward'/'ATQ', 'artifact ward').
card_uid('artifact ward'/'ATQ', 'ATQ:Artifact Ward:artifact ward').
card_rarity('artifact ward'/'ATQ', 'Common').
card_artist('artifact ward'/'ATQ', 'Douglas Shuler').
card_multiverse_id('artifact ward'/'ATQ', '1066').

card_in_set('ashnod\'s altar', 'ATQ').
card_original_type('ashnod\'s altar'/'ATQ', 'Poly Artifact').
card_original_text('ashnod\'s altar'/'ATQ', '{0}: Sacrifice one of your creatures to add 2 colorless mana to your mana pool. This effect is played as an interrupt. You may not sacrifice a creature that is already on its way to the graveyard.').
card_first_print('ashnod\'s altar', 'ATQ').
card_image_name('ashnod\'s altar'/'ATQ', 'ashnod\'s altar').
card_uid('ashnod\'s altar'/'ATQ', 'ATQ:Ashnod\'s Altar:ashnod\'s altar').
card_rarity('ashnod\'s altar'/'ATQ', 'Uncommon').
card_artist('ashnod\'s altar'/'ATQ', 'Anson Maddocks').
card_multiverse_id('ashnod\'s altar'/'ATQ', '994').

card_in_set('ashnod\'s battle gear', 'ATQ').
card_original_type('ashnod\'s battle gear'/'ATQ', 'Mono Artifact').
card_original_text('ashnod\'s battle gear'/'ATQ', '{2}: Give a creature of yours +2/-2 as long as Ashnod\'s Battle Gear is tapped. You may choose not to untap Ashnod\'s Battle Gear during untap phase.').
card_first_print('ashnod\'s battle gear', 'ATQ').
card_image_name('ashnod\'s battle gear'/'ATQ', 'ashnod\'s battle gear').
card_uid('ashnod\'s battle gear'/'ATQ', 'ATQ:Ashnod\'s Battle Gear:ashnod\'s battle gear').
card_rarity('ashnod\'s battle gear'/'ATQ', 'Uncommon').
card_artist('ashnod\'s battle gear'/'ATQ', 'Mark Poole').
card_flavor_text('ashnod\'s battle gear'/'ATQ', 'This horrid invention clearly illustrates why Mishra\'s lieutenant was feared as much by her troops as by her foes.').
card_multiverse_id('ashnod\'s battle gear'/'ATQ', '995').

card_in_set('ashnod\'s transmogrant', 'ATQ').
card_original_type('ashnod\'s transmogrant'/'ATQ', 'Mono Artifact').
card_original_text('ashnod\'s transmogrant'/'ATQ', 'Target non-artifact creature gains +1/+1 and is now considered an artifact creature, though it retains its original color. Discard Ashnod\'s Transmogrant after it is used.').
card_first_print('ashnod\'s transmogrant', 'ATQ').
card_image_name('ashnod\'s transmogrant'/'ATQ', 'ashnod\'s transmogrant').
card_uid('ashnod\'s transmogrant'/'ATQ', 'ATQ:Ashnod\'s Transmogrant:ashnod\'s transmogrant').
card_rarity('ashnod\'s transmogrant'/'ATQ', 'Uncommon').
card_artist('ashnod\'s transmogrant'/'ATQ', 'Mark Tedin').
card_flavor_text('ashnod\'s transmogrant'/'ATQ', 'Ashnod found few willing to trade their humanity for the power she offered them.').
card_multiverse_id('ashnod\'s transmogrant'/'ATQ', '996').

card_in_set('atog', 'ATQ').
card_original_type('atog'/'ATQ', 'Summon — Atog').
card_original_text('atog'/'ATQ', '{0}: +2/+2 until end of turn. Each time you use this ability, you must choose one of your artifacts in play and place it in the graveyard. This artifact cannot be one that is already on its way to the graveyard, and artifact creatures killed this way may not be regenerated.').
card_first_print('atog', 'ATQ').
card_image_name('atog'/'ATQ', 'atog').
card_uid('atog'/'ATQ', 'ATQ:Atog:atog').
card_rarity('atog'/'ATQ', 'Common').
card_artist('atog'/'ATQ', 'Jesper Myrfors').
card_multiverse_id('atog'/'ATQ', '1058').

card_in_set('battering ram', 'ATQ').
card_original_type('battering ram'/'ATQ', 'Artifact Creature').
card_original_text('battering ram'/'ATQ', 'Bands, but only when attacking. Any wall blocking Battering Ram is destroyed. Walls destroyed this way deal their damage before dying.').
card_first_print('battering ram', 'ATQ').
card_image_name('battering ram'/'ATQ', 'battering ram').
card_uid('battering ram'/'ATQ', 'ATQ:Battering Ram:battering ram').
card_rarity('battering ram'/'ATQ', 'Common').
card_artist('battering ram'/'ATQ', 'Jeff A. Menges').
card_flavor_text('battering ram'/'ATQ', 'By the time Mishra was defeated, no mage was foolish enough to rely heavily on walls.').
card_multiverse_id('battering ram'/'ATQ', '997').

card_in_set('bronze tablet', 'ATQ').
card_original_type('bronze tablet'/'ATQ', 'Mono Artifact').
card_original_text('bronze tablet'/'ATQ', '{4}: Target any card opponent has in play; remove it and Bronze Tablet from game. You become owner of that card, and your opponent becomes owner of Bronze Tablet. Exchange is permanent; play as interrupt. Opponent can prevent exchange by spending 10 life; this discards Bronze Tablet. Damage-preventing effects cannot counter such loss of life. Bronze Tablet comes into play tapped. Remove this card from deck if not playing for ante.').
card_first_print('bronze tablet', 'ATQ').
card_image_name('bronze tablet'/'ATQ', 'bronze tablet').
card_uid('bronze tablet'/'ATQ', 'ATQ:Bronze Tablet:bronze tablet').
card_rarity('bronze tablet'/'ATQ', 'Rare').
card_artist('bronze tablet'/'ATQ', 'Tom Wänerstrand').
card_multiverse_id('bronze tablet'/'ATQ', '998').

card_in_set('candelabra of tawnos', 'ATQ').
card_original_type('candelabra of tawnos'/'ATQ', 'Mono Artifact').
card_original_text('candelabra of tawnos'/'ATQ', '{X} Untap X separate lands.').
card_first_print('candelabra of tawnos', 'ATQ').
card_image_name('candelabra of tawnos'/'ATQ', 'candelabra of tawnos').
card_uid('candelabra of tawnos'/'ATQ', 'ATQ:Candelabra of Tawnos:candelabra of tawnos').
card_rarity('candelabra of tawnos'/'ATQ', 'Rare').
card_artist('candelabra of tawnos'/'ATQ', 'Douglas Shuler').
card_flavor_text('candelabra of tawnos'/'ATQ', 'Tawnos learned quickly from Urza that utter simplicity often led to wondrous, yet subtle utility.').
card_multiverse_id('candelabra of tawnos'/'ATQ', '999').

card_in_set('circle of protection: artifacts', 'ATQ').
card_original_type('circle of protection: artifacts'/'ATQ', 'Enchantment').
card_original_text('circle of protection: artifacts'/'ATQ', '{2}: Prevents all damage against you from any one artifact source. If a source does damage to you more than once in a turn, you must pay {2} each time you want to prevent the damage.').
card_first_print('circle of protection: artifacts', 'ATQ').
card_image_name('circle of protection: artifacts'/'ATQ', 'circle of protection artifacts').
card_uid('circle of protection: artifacts'/'ATQ', 'ATQ:Circle of Protection: Artifacts:circle of protection artifacts').
card_rarity('circle of protection: artifacts'/'ATQ', 'Uncommon').
card_artist('circle of protection: artifacts'/'ATQ', 'Pete Venters').
card_multiverse_id('circle of protection: artifacts'/'ATQ', '1067').

card_in_set('citanul druid', 'ATQ').
card_original_type('citanul druid'/'ATQ', 'Summon — Druid').
card_original_text('citanul druid'/'ATQ', 'Druid gains a +1/+1 counter each time opponent casts an artifact.').
card_first_print('citanul druid', 'ATQ').
card_image_name('citanul druid'/'ATQ', 'citanul druid').
card_uid('citanul druid'/'ATQ', 'ATQ:Citanul Druid:citanul druid').
card_rarity('citanul druid'/'ATQ', 'Uncommon').
card_artist('citanul druid'/'ATQ', 'Jeff A. Menges').
card_flavor_text('citanul druid'/'ATQ', 'Driven mad by the fall of Argoth, the Citanul Druids found peace only in battle.').
card_multiverse_id('citanul druid'/'ATQ', '1052').

card_in_set('clay statue', 'ATQ').
card_original_type('clay statue'/'ATQ', 'Artifact Creature').
card_original_text('clay statue'/'ATQ', '{2}: Regenerates').
card_first_print('clay statue', 'ATQ').
card_image_name('clay statue'/'ATQ', 'clay statue').
card_uid('clay statue'/'ATQ', 'ATQ:Clay Statue:clay statue').
card_rarity('clay statue'/'ATQ', 'Common').
card_artist('clay statue'/'ATQ', 'Jesper Myrfors').
card_flavor_text('clay statue'/'ATQ', 'Tawnos won fame as Urza\'s greatest assistant. After he created these warriors, Urza ended his apprenticeship, promoting him directly to the rank of master.').
card_multiverse_id('clay statue'/'ATQ', '1000').

card_in_set('clockwork avian', 'ATQ').
card_original_type('clockwork avian'/'ATQ', 'Artifact Creature').
card_original_text('clockwork avian'/'ATQ', 'Flying\nPut four +1/+0 counters on Avian. After Avian attacks or blocks a creature, discard a counter.\nDuring his or her upkeep, controller may buy back lost counters for {1} per counter; this taps Avian.').
card_first_print('clockwork avian', 'ATQ').
card_image_name('clockwork avian'/'ATQ', 'clockwork avian').
card_uid('clockwork avian'/'ATQ', 'ATQ:Clockwork Avian:clockwork avian').
card_rarity('clockwork avian'/'ATQ', 'Rare').
card_artist('clockwork avian'/'ATQ', 'Randy Asplund-Faith').
card_multiverse_id('clockwork avian'/'ATQ', '1001').

card_in_set('colossus of sardia', 'ATQ').
card_original_type('colossus of sardia'/'ATQ', 'Artifact Creature').
card_original_text('colossus of sardia'/'ATQ', 'Trample\nColossus does not untap normally during untap phase; you may spend {9} during your upkeep phase to untap Colossus.').
card_first_print('colossus of sardia', 'ATQ').
card_image_name('colossus of sardia'/'ATQ', 'colossus of sardia').
card_uid('colossus of sardia'/'ATQ', 'ATQ:Colossus of Sardia:colossus of sardia').
card_rarity('colossus of sardia'/'ATQ', 'Rare').
card_artist('colossus of sardia'/'ATQ', 'Jesper Myrfors').
card_flavor_text('colossus of sardia'/'ATQ', 'From the Sardian mountains wakes ancient doom:\nWarrior born from a rocky womb.').
card_multiverse_id('colossus of sardia'/'ATQ', '1002').

card_in_set('coral helm', 'ATQ').
card_original_type('coral helm'/'ATQ', 'Poly Artifact').
card_original_text('coral helm'/'ATQ', '{3}: Give target creature +2/+2 until end of turn. Each time you use this ability, you must discard one card at random from your hand. Coral Helm cannot be used if you have no cards in your hand.').
card_first_print('coral helm', 'ATQ').
card_image_name('coral helm'/'ATQ', 'coral helm').
card_uid('coral helm'/'ATQ', 'ATQ:Coral Helm:coral helm').
card_rarity('coral helm'/'ATQ', 'Rare').
card_artist('coral helm'/'ATQ', 'Amy Weber').
card_multiverse_id('coral helm'/'ATQ', '1003').

card_in_set('crumble', 'ATQ').
card_original_type('crumble'/'ATQ', 'Instant').
card_original_text('crumble'/'ATQ', 'Destroy target artifact; artifact creatures may not regenerate. Artifact\'s controller gains life points equal to target artifact\'s casting cost.').
card_first_print('crumble', 'ATQ').
card_image_name('crumble'/'ATQ', 'crumble').
card_uid('crumble'/'ATQ', 'ATQ:Crumble:crumble').
card_rarity('crumble'/'ATQ', 'Common').
card_artist('crumble'/'ATQ', 'Jesper Myrfors').
card_flavor_text('crumble'/'ATQ', 'The spirits of Argoth grant new life to those who repent the folly of enslaving their labors to devices.').
card_multiverse_id('crumble'/'ATQ', '1053').

card_in_set('cursed rack', 'ATQ').
card_original_type('cursed rack'/'ATQ', 'Continuous Artifact').
card_original_text('cursed rack'/'ATQ', 'Opponent must discard down to four cards during his or her discard phase.').
card_first_print('cursed rack', 'ATQ').
card_image_name('cursed rack'/'ATQ', 'cursed rack').
card_uid('cursed rack'/'ATQ', 'ATQ:Cursed Rack:cursed rack').
card_rarity('cursed rack'/'ATQ', 'Uncommon').
card_artist('cursed rack'/'ATQ', 'Richard Thomas').
card_flavor_text('cursed rack'/'ATQ', 'Ashnod invented several torture techniques that could make victims even miles away beg for mercy as if the End had come.').
card_multiverse_id('cursed rack'/'ATQ', '1004').

card_in_set('damping field', 'ATQ').
card_original_type('damping field'/'ATQ', 'Enchantment').
card_original_text('damping field'/'ATQ', 'Players may not untap more than one artifact during each of their own untap phases.').
card_first_print('damping field', 'ATQ').
card_image_name('damping field'/'ATQ', 'damping field').
card_uid('damping field'/'ATQ', 'ATQ:Damping Field:damping field').
card_rarity('damping field'/'ATQ', 'Uncommon').
card_artist('damping field'/'ATQ', 'Justin Hampton').
card_flavor_text('damping field'/'ATQ', 'Eventually, mages learned to harness the power of natural damping fields and use it for their own ends.').
card_multiverse_id('damping field'/'ATQ', '1068').

card_in_set('detonate', 'ATQ').
card_original_type('detonate'/'ATQ', 'Sorcery').
card_original_text('detonate'/'ATQ', 'Targets any artifact; X is the casting cost of target artifact. Target artifact is destroyed, and Detonate does X points of damage to artifact\'s controller. Artifact creatures destroyed in this manner may not be regenerated.').
card_first_print('detonate', 'ATQ').
card_image_name('detonate'/'ATQ', 'detonate').
card_uid('detonate'/'ATQ', 'ATQ:Detonate:detonate').
card_rarity('detonate'/'ATQ', 'Uncommon').
card_artist('detonate'/'ATQ', 'Randy Asplund-Faith').
card_multiverse_id('detonate'/'ATQ', '1059').

card_in_set('drafna\'s restoration', 'ATQ').
card_original_type('drafna\'s restoration'/'ATQ', 'Sorcery').
card_original_text('drafna\'s restoration'/'ATQ', 'Take any number of artifacts of your choice from target player\'s graveyard and place them on top of that player\'s library, in any order.').
card_first_print('drafna\'s restoration', 'ATQ').
card_image_name('drafna\'s restoration'/'ATQ', 'drafna\'s restoration').
card_uid('drafna\'s restoration'/'ATQ', 'ATQ:Drafna\'s Restoration:drafna\'s restoration').
card_rarity('drafna\'s restoration'/'ATQ', 'Common').
card_artist('drafna\'s restoration'/'ATQ', 'Amy Weber').
card_flavor_text('drafna\'s restoration'/'ATQ', 'Drafna, founder of the College of Lat-Nam, could create a working model from even the smallest remnants of a newly unearthed artifact.').
card_multiverse_id('drafna\'s restoration'/'ATQ', '1037').

card_in_set('dragon engine', 'ATQ').
card_original_type('dragon engine'/'ATQ', 'Artifact Creature').
card_original_text('dragon engine'/'ATQ', '{2}: +1/+0 until end of turn.').
card_first_print('dragon engine', 'ATQ').
card_image_name('dragon engine'/'ATQ', 'dragon engine').
card_uid('dragon engine'/'ATQ', 'ATQ:Dragon Engine:dragon engine').
card_rarity('dragon engine'/'ATQ', 'Common').
card_artist('dragon engine'/'ATQ', 'Anson Maddocks').
card_flavor_text('dragon engine'/'ATQ', 'Those who believed the city of Kroog would never fall to Mishra\'s forces severely underestimated the might of his war machines.').
card_multiverse_id('dragon engine'/'ATQ', '1005').

card_in_set('dwarven weaponsmith', 'ATQ').
card_original_type('dwarven weaponsmith'/'ATQ', 'Summon — Dwarves').
card_original_text('dwarven weaponsmith'/'ATQ', 'Tap during your upkeep to add a permanent +1/+1 counter to any creature. Each time you use this ability, you must choose one of your artifacts in play and place it in the graveyard. This artifact cannot be one that is already on its way to the graveyard, and artifact creatures killed in this way may not be regenerated.').
card_first_print('dwarven weaponsmith', 'ATQ').
card_image_name('dwarven weaponsmith'/'ATQ', 'dwarven weaponsmith').
card_uid('dwarven weaponsmith'/'ATQ', 'ATQ:Dwarven Weaponsmith:dwarven weaponsmith').
card_rarity('dwarven weaponsmith'/'ATQ', 'Uncommon').
card_artist('dwarven weaponsmith'/'ATQ', 'Mark Poole').
card_multiverse_id('dwarven weaponsmith'/'ATQ', '1060').

card_in_set('energy flux', 'ATQ').
card_original_type('energy flux'/'ATQ', 'Enchantment').
card_original_text('energy flux'/'ATQ', 'All artifacts in play now require an upkeep cost of {2} in addition to any other upkeep costs they may have. If the upkeep cost for an artifact is not paid, the artifact must be discarded.').
card_first_print('energy flux', 'ATQ').
card_image_name('energy flux'/'ATQ', 'energy flux').
card_uid('energy flux'/'ATQ', 'ATQ:Energy Flux:energy flux').
card_rarity('energy flux'/'ATQ', 'Uncommon').
card_artist('energy flux'/'ATQ', 'Kaja Foglio').
card_multiverse_id('energy flux'/'ATQ', '1038').

card_in_set('feldon\'s cane', 'ATQ').
card_original_type('feldon\'s cane'/'ATQ', 'Mono Artifact').
card_original_text('feldon\'s cane'/'ATQ', '{0}: Reshuffle your graveyard into your library. If Feldon\'s Cane is used, remove it from the game, returning it to its owner\'s deck only when the game is over.').
card_first_print('feldon\'s cane', 'ATQ').
card_image_name('feldon\'s cane'/'ATQ', 'feldon\'s cane').
card_uid('feldon\'s cane'/'ATQ', 'ATQ:Feldon\'s Cane:feldon\'s cane').
card_rarity('feldon\'s cane'/'ATQ', 'Uncommon').
card_artist('feldon\'s cane'/'ATQ', 'Mark Tedin').
card_flavor_text('feldon\'s cane'/'ATQ', 'Feldon found the first of these canes frozen in the Ronom Glacier.').
card_multiverse_id('feldon\'s cane'/'ATQ', '1006').

card_in_set('gaea\'s avenger', 'ATQ').
card_original_type('gaea\'s avenger'/'ATQ', 'Summon — Gaea\'s Avenger').
card_original_text('gaea\'s avenger'/'ATQ', 'The *s below are the number of artifacts opponent has in play.').
card_first_print('gaea\'s avenger', 'ATQ').
card_image_name('gaea\'s avenger'/'ATQ', 'gaea\'s avenger').
card_uid('gaea\'s avenger'/'ATQ', 'ATQ:Gaea\'s Avenger:gaea\'s avenger').
card_rarity('gaea\'s avenger'/'ATQ', 'Rare').
card_artist('gaea\'s avenger'/'ATQ', 'Pete Venters').
card_flavor_text('gaea\'s avenger'/'ATQ', 'After the destruction of Argoth, Gaea was willing to instill a portion of her own powers into some of her more vengeful followers.').
card_multiverse_id('gaea\'s avenger'/'ATQ', '1054').

card_in_set('gate to phyrexia', 'ATQ').
card_original_type('gate to phyrexia'/'ATQ', 'Enchantment').
card_original_text('gate to phyrexia'/'ATQ', 'Sacrifice one of your creatures during your upkeep to destroy any one artifact. You may not sacrifice a creature that is already on its way to the graveyard.').
card_first_print('gate to phyrexia', 'ATQ').
card_image_name('gate to phyrexia'/'ATQ', 'gate to phyrexia').
card_uid('gate to phyrexia'/'ATQ', 'ATQ:Gate to Phyrexia:gate to phyrexia').
card_rarity('gate to phyrexia'/'ATQ', 'Uncommon').
card_artist('gate to phyrexia'/'ATQ', 'Sandra Everingham').
card_flavor_text('gate to phyrexia'/'ATQ', '\"The warm rain of grease on my face immediately made it clear I had entered Phyrexia.\" —Jarsyl, Diary').
card_multiverse_id('gate to phyrexia'/'ATQ', '1039').

card_in_set('goblin artisans', 'ATQ').
card_original_type('goblin artisans'/'ATQ', 'Summon — Goblins').
card_original_text('goblin artisans'/'ATQ', 'You may tap Goblin Artisans as you cast an artifact. Then flip a coin; opponent calls heads or tails while coin is in the air. If the flip ends up in opponent\'s favor, your artifact is countered. Otherwise, draw another card from your library. You can only use this ability once for each time you cast an artifact.').
card_first_print('goblin artisans', 'ATQ').
card_image_name('goblin artisans'/'ATQ', 'goblin artisans').
card_uid('goblin artisans'/'ATQ', 'ATQ:Goblin Artisans:goblin artisans').
card_rarity('goblin artisans'/'ATQ', 'Uncommon').
card_artist('goblin artisans'/'ATQ', 'Julie Baroh').
card_multiverse_id('goblin artisans'/'ATQ', '1061').

card_in_set('golgothian sylex', 'ATQ').
card_original_type('golgothian sylex'/'ATQ', 'Mono Artifact').
card_original_text('golgothian sylex'/'ATQ', '{1}: All cards from the Antiquities expansion, including Golgothian Sylex, must be discarded from play.').
card_first_print('golgothian sylex', 'ATQ').
card_image_name('golgothian sylex'/'ATQ', 'golgothian sylex').
card_uid('golgothian sylex'/'ATQ', 'ATQ:Golgothian Sylex:golgothian sylex').
card_rarity('golgothian sylex'/'ATQ', 'Rare').
card_artist('golgothian sylex'/'ATQ', 'Kerstin Kaman').
card_flavor_text('golgothian sylex'/'ATQ', 'From their earliest educations, the brothers had known that no human contrivance could stand against the true masters of Dominia.').
card_multiverse_id('golgothian sylex'/'ATQ', '1007').

card_in_set('grapeshot catapult', 'ATQ').
card_original_type('grapeshot catapult'/'ATQ', 'Artifact Creature').
card_original_text('grapeshot catapult'/'ATQ', 'Tap to deal 1 damage to target flying creature.').
card_first_print('grapeshot catapult', 'ATQ').
card_image_name('grapeshot catapult'/'ATQ', 'grapeshot catapult').
card_uid('grapeshot catapult'/'ATQ', 'ATQ:Grapeshot Catapult:grapeshot catapult').
card_rarity('grapeshot catapult'/'ATQ', 'Common').
card_artist('grapeshot catapult'/'ATQ', 'Dan Frazier').
card_flavor_text('grapeshot catapult'/'ATQ', 'For years scholars debated whether these were Urza\'s or Mishra\'s creations. Recent research suggests they were invented by the brothers\' original master, Tocasia, and that both used these devices.').
card_multiverse_id('grapeshot catapult'/'ATQ', '1008').

card_in_set('haunting wind', 'ATQ').
card_original_type('haunting wind'/'ATQ', 'Enchantment').
card_original_text('haunting wind'/'ATQ', 'Each time an artifact in play is tapped or its activation cost is paid, Haunting Wind does 1 damage to that artifact\'s controller. Is not triggered by continuous artifacts.').
card_first_print('haunting wind', 'ATQ').
card_image_name('haunting wind'/'ATQ', 'haunting wind').
card_uid('haunting wind'/'ATQ', 'ATQ:Haunting Wind:haunting wind').
card_rarity('haunting wind'/'ATQ', 'Uncommon').
card_artist('haunting wind'/'ATQ', 'Jeff A. Menges').
card_flavor_text('haunting wind'/'ATQ', 'These devices lured so many spirits that sometimes entire battlefields would become haunted at once.').
card_multiverse_id('haunting wind'/'ATQ', '1040').

card_in_set('hurkyl\'s recall', 'ATQ').
card_original_type('hurkyl\'s recall'/'ATQ', 'Instant').
card_original_text('hurkyl\'s recall'/'ATQ', 'All artifacts in play owned by target player are returned to target player\'s hand. Any enchantments on those artifacts are discarded. Cannot be played during the damage-dealing phase of an attack.').
card_first_print('hurkyl\'s recall', 'ATQ').
card_image_name('hurkyl\'s recall'/'ATQ', 'hurkyl\'s recall').
card_uid('hurkyl\'s recall'/'ATQ', 'ATQ:Hurkyl\'s Recall:hurkyl\'s recall').
card_rarity('hurkyl\'s recall'/'ATQ', 'Rare').
card_artist('hurkyl\'s recall'/'ATQ', 'NéNé Thomas').
card_flavor_text('hurkyl\'s recall'/'ATQ', 'This spell, attributed to Drafna, was actually the work of his wife Hurkyl.').
card_multiverse_id('hurkyl\'s recall'/'ATQ', '1041').

card_in_set('ivory tower', 'ATQ').
card_original_type('ivory tower'/'ATQ', 'Continuous Artifact').
card_original_text('ivory tower'/'ATQ', 'During your upkeep phase, gain 1 life for each card in your hand above four.').
card_first_print('ivory tower', 'ATQ').
card_image_name('ivory tower'/'ATQ', 'ivory tower').
card_uid('ivory tower'/'ATQ', 'ATQ:Ivory Tower:ivory tower').
card_rarity('ivory tower'/'ATQ', 'Uncommon').
card_artist('ivory tower'/'ATQ', 'Margaret Organ-Kean').
card_flavor_text('ivory tower'/'ATQ', 'Valuing scholarship above all else, the inhabitants of the Ivory Tower reward those who sacrifice power for knowledge.').
card_multiverse_id('ivory tower'/'ATQ', '1009').

card_in_set('jalum tome', 'ATQ').
card_original_type('jalum tome'/'ATQ', 'Mono Artifact').
card_original_text('jalum tome'/'ATQ', '{2}: Draw a card from your library, then immediately discard a card of your choice to your graveyard.').
card_first_print('jalum tome', 'ATQ').
card_image_name('jalum tome'/'ATQ', 'jalum tome').
card_uid('jalum tome'/'ATQ', 'ATQ:Jalum Tome:jalum tome').
card_rarity('jalum tome'/'ATQ', 'Uncommon').
card_artist('jalum tome'/'ATQ', 'Tom Wänerstrand').
card_flavor_text('jalum tome'/'ATQ', 'This timeworn relic was responsible for many of Urza\'s victories, though he never fully comprehended its mystical runes.').
card_multiverse_id('jalum tome'/'ATQ', '1010').

card_in_set('martyrs of korlis', 'ATQ').
card_original_type('martyrs of korlis'/'ATQ', 'Summon — Bodyguard').
card_original_text('martyrs of korlis'/'ATQ', 'Unless Martyrs of Korlis is tapped, any damage done to you by artifacts is instead applied to Martyrs of Korlis. You may not take this damage yourself, though you may prevent it if possible. No more than one Bodyguard of your choice can take damage for you in this manner each turn.').
card_first_print('martyrs of korlis', 'ATQ').
card_image_name('martyrs of korlis'/'ATQ', 'martyrs of korlis').
card_uid('martyrs of korlis'/'ATQ', 'ATQ:Martyrs of Korlis:martyrs of korlis').
card_rarity('martyrs of korlis'/'ATQ', 'Uncommon').
card_artist('martyrs of korlis'/'ATQ', 'Margaret Organ-Kean').
card_multiverse_id('martyrs of korlis'/'ATQ', '1069').

card_in_set('mightstone', 'ATQ').
card_original_type('mightstone'/'ATQ', 'Continuous Artifact').
card_original_text('mightstone'/'ATQ', 'All attacking creatures gain +1/+0.').
card_first_print('mightstone', 'ATQ').
card_image_name('mightstone'/'ATQ', 'mightstone').
card_uid('mightstone'/'ATQ', 'ATQ:Mightstone:mightstone').
card_rarity('mightstone'/'ATQ', 'Uncommon').
card_artist('mightstone'/'ATQ', 'Pete Venters').
card_flavor_text('mightstone'/'ATQ', 'While exploring the sacred cave of Koilos with his brother Mishra and their master Tocasia, Urza fell behind in the Hall of Tagsin, where he discovered the remarkable Mightstone.').
card_multiverse_id('mightstone'/'ATQ', '1011').

card_in_set('millstone', 'ATQ').
card_original_type('millstone'/'ATQ', 'Mono Artifact').
card_original_text('millstone'/'ATQ', '{2}: Take the top two cards from target player\'s library and put them in target player\'s graveyard.').
card_first_print('millstone', 'ATQ').
card_image_name('millstone'/'ATQ', 'millstone').
card_uid('millstone'/'ATQ', 'ATQ:Millstone:millstone').
card_rarity('millstone'/'ATQ', 'Uncommon').
card_artist('millstone'/'ATQ', 'Kaja Foglio').
card_flavor_text('millstone'/'ATQ', 'More than one mage was driven insane by the sound of the Millstone relentlessly grinding away.').
card_multiverse_id('millstone'/'ATQ', '1012').

card_in_set('mishra\'s factory', 'ATQ').
card_original_type('mishra\'s factory'/'ATQ', 'Land').
card_original_text('mishra\'s factory'/'ATQ', 'Tap to add 1 colorless mana to your mana pool or give any Assembly Worker +1/+1 until end of turn.\n{1}: Mishra\'s Factory becomes an Assembly Worker, a 2/2 artifact creature, until end of turn. Assembly Worker is still considered a land as well.').
card_first_print('mishra\'s factory', 'ATQ').
card_image_name('mishra\'s factory'/'ATQ', 'mishra\'s factory1').
card_uid('mishra\'s factory'/'ATQ', 'ATQ:Mishra\'s Factory:mishra\'s factory1').
card_rarity('mishra\'s factory'/'ATQ', 'Rare').
card_artist('mishra\'s factory'/'ATQ', 'Kaja & Phil Foglio').
card_multiverse_id('mishra\'s factory'/'ATQ', '1072').

card_in_set('mishra\'s factory', 'ATQ').
card_original_type('mishra\'s factory'/'ATQ', 'Land').
card_original_text('mishra\'s factory'/'ATQ', 'Tap to add 1 colorless mana to your mana pool or give any Assembly Worker +1/+1 until end of turn.\n{1}: Mishra\'s Factory becomes an Assembly Worker, a 2/2 artifact creature, until end of turn. Assembly Worker is still considered a land as well.').
card_image_name('mishra\'s factory'/'ATQ', 'mishra\'s factory2').
card_uid('mishra\'s factory'/'ATQ', 'ATQ:Mishra\'s Factory:mishra\'s factory2').
card_rarity('mishra\'s factory'/'ATQ', 'Uncommon').
card_artist('mishra\'s factory'/'ATQ', 'Kaja & Phil Foglio').
card_multiverse_id('mishra\'s factory'/'ATQ', '1074').

card_in_set('mishra\'s factory', 'ATQ').
card_original_type('mishra\'s factory'/'ATQ', 'Land').
card_original_text('mishra\'s factory'/'ATQ', 'Tap to add 1 colorless mana to your mana pool or give any Assembly Worker +1/+1 until end of turn.\n{1}: Mishra\'s Factory becomes an Assembly Worker, a 2/2 artifact creature, until end of turn. Assembly Worker is still considered a land as well.').
card_image_name('mishra\'s factory'/'ATQ', 'mishra\'s factory3').
card_uid('mishra\'s factory'/'ATQ', 'ATQ:Mishra\'s Factory:mishra\'s factory3').
card_rarity('mishra\'s factory'/'ATQ', 'Rare').
card_artist('mishra\'s factory'/'ATQ', 'Kaja & Phil Foglio').
card_multiverse_id('mishra\'s factory'/'ATQ', '1071').

card_in_set('mishra\'s factory', 'ATQ').
card_original_type('mishra\'s factory'/'ATQ', 'Land').
card_original_text('mishra\'s factory'/'ATQ', 'Tap to add 1 colorless mana to your mana pool or give any Assembly Worker +1/+1 until end of turn.\n{1}: Mishra\'s Factory becomes an Assembly Worker, a 2/2 artifact creature, until end of turn. Assembly Worker is still considered a land as well.').
card_image_name('mishra\'s factory'/'ATQ', 'mishra\'s factory4').
card_uid('mishra\'s factory'/'ATQ', 'ATQ:Mishra\'s Factory:mishra\'s factory4').
card_rarity('mishra\'s factory'/'ATQ', 'Rare').
card_artist('mishra\'s factory'/'ATQ', 'Kaja & Phil Foglio').
card_multiverse_id('mishra\'s factory'/'ATQ', '1073').

card_in_set('mishra\'s war machine', 'ATQ').
card_original_type('mishra\'s war machine'/'ATQ', 'Artifact Creature').
card_original_text('mishra\'s war machine'/'ATQ', 'Bands\nDuring your upkeep, discard one card of your choice from your hand, or Mishra\'s War Machine becomes tapped and does 3 points of damage to you.').
card_first_print('mishra\'s war machine', 'ATQ').
card_image_name('mishra\'s war machine'/'ATQ', 'mishra\'s war machine').
card_uid('mishra\'s war machine'/'ATQ', 'ATQ:Mishra\'s War Machine:mishra\'s war machine').
card_rarity('mishra\'s war machine'/'ATQ', 'Rare').
card_artist('mishra\'s war machine'/'ATQ', 'Amy Weber').
card_multiverse_id('mishra\'s war machine'/'ATQ', '1013').

card_in_set('mishra\'s workshop', 'ATQ').
card_original_type('mishra\'s workshop'/'ATQ', 'Land').
card_original_text('mishra\'s workshop'/'ATQ', 'Tap to add 3 colorless mana to your mana pool. This mana may only be used to cast artifacts.').
card_first_print('mishra\'s workshop', 'ATQ').
card_image_name('mishra\'s workshop'/'ATQ', 'mishra\'s workshop').
card_uid('mishra\'s workshop'/'ATQ', 'ATQ:Mishra\'s Workshop:mishra\'s workshop').
card_rarity('mishra\'s workshop'/'ATQ', 'Rare').
card_artist('mishra\'s workshop'/'ATQ', 'Kaja Foglio').
card_flavor_text('mishra\'s workshop'/'ATQ', 'Though he eventually came to despise Tocasia, Mishra listened well to her lessons on clarity of purpose. Unlike his brother, he focused his mind on a single goal.').
card_multiverse_id('mishra\'s workshop'/'ATQ', '1075').

card_in_set('obelisk of undoing', 'ATQ').
card_original_type('obelisk of undoing'/'ATQ', 'Mono Artifact').
card_original_text('obelisk of undoing'/'ATQ', '{6}: Return any of your permanents in play to your hand; enchantments on that permanent are discarded. Can be used only on permanents you cast.').
card_first_print('obelisk of undoing', 'ATQ').
card_image_name('obelisk of undoing'/'ATQ', 'obelisk of undoing').
card_uid('obelisk of undoing'/'ATQ', 'ATQ:Obelisk of Undoing:obelisk of undoing').
card_rarity('obelisk of undoing'/'ATQ', 'Rare').
card_artist('obelisk of undoing'/'ATQ', 'Tom Wänerstrand').
card_flavor_text('obelisk of undoing'/'ATQ', 'The Battle of Tomakul taught Urza not to rely on fickle reinforcements.').
card_multiverse_id('obelisk of undoing'/'ATQ', '1014').

card_in_set('onulet', 'ATQ').
card_original_type('onulet'/'ATQ', 'Artifact Creature').
card_original_text('onulet'/'ATQ', 'If Onulet goes to the graveyard, its controller gains 2 life.').
card_first_print('onulet', 'ATQ').
card_image_name('onulet'/'ATQ', 'onulet').
card_uid('onulet'/'ATQ', 'ATQ:Onulet:onulet').
card_rarity('onulet'/'ATQ', 'Uncommon').
card_artist('onulet'/'ATQ', 'Anson Maddocks').
card_flavor_text('onulet'/'ATQ', 'An early inspiration for Urza, Tocasia\'s Onulets contained magical essences that could be cannibalized after they stopped functioning.').
card_multiverse_id('onulet'/'ATQ', '1015').

card_in_set('orcish mechanics', 'ATQ').
card_original_type('orcish mechanics'/'ATQ', 'Summon — Orcs').
card_original_text('orcish mechanics'/'ATQ', 'Tap to do 2 points of damage to any target. Each time you use this ability, you must choose one of your artifacts in play and place it in the graveyard. This artifact cannot be one already on its way to the graveyard, and artifact creatures killed this way may not be regenerated.').
card_first_print('orcish mechanics', 'ATQ').
card_image_name('orcish mechanics'/'ATQ', 'orcish mechanics').
card_uid('orcish mechanics'/'ATQ', 'ATQ:Orcish Mechanics:orcish mechanics').
card_rarity('orcish mechanics'/'ATQ', 'Common').
card_artist('orcish mechanics'/'ATQ', 'Pete Venters').
card_multiverse_id('orcish mechanics'/'ATQ', '1062').

card_in_set('ornithopter', 'ATQ').
card_original_type('ornithopter'/'ATQ', 'Artifact Creature').
card_original_text('ornithopter'/'ATQ', 'Flying').
card_first_print('ornithopter', 'ATQ').
card_image_name('ornithopter'/'ATQ', 'ornithopter').
card_uid('ornithopter'/'ATQ', 'ATQ:Ornithopter:ornithopter').
card_rarity('ornithopter'/'ATQ', 'Common').
card_artist('ornithopter'/'ATQ', 'Amy Weber').
card_flavor_text('ornithopter'/'ATQ', 'Many scholars believe that these creatures were the result of Urza\'s first attempt at mechanical life, perhaps created in his early days as an apprentice to Tocasia.').
card_multiverse_id('ornithopter'/'ATQ', '1016').

card_in_set('phyrexian gremlins', 'ATQ').
card_original_type('phyrexian gremlins'/'ATQ', 'Summon — Gremlins').
card_original_text('phyrexian gremlins'/'ATQ', 'Tap Gremlins to tap an artifact. As long as Gremlins remain tapped and in play, that artifact does not untap as normal during its controller\'s untap phase. You may choose not to untap Gremlins during your untap phase.').
card_first_print('phyrexian gremlins', 'ATQ').
card_image_name('phyrexian gremlins'/'ATQ', 'phyrexian gremlins').
card_uid('phyrexian gremlins'/'ATQ', 'ATQ:Phyrexian Gremlins:phyrexian gremlins').
card_rarity('phyrexian gremlins'/'ATQ', 'Common').
card_artist('phyrexian gremlins'/'ATQ', 'Amy Weber').
card_multiverse_id('phyrexian gremlins'/'ATQ', '1042').

card_in_set('power artifact', 'ATQ').
card_original_type('power artifact'/'ATQ', 'Enchant Artifact').
card_original_text('power artifact'/'ATQ', 'The activation cost of target artifact is reduced by {2}. If this would reduce target artifact\'s activation cost below {1}, target artifact\'s activation cost becomes {1}. Power Artifact has no effect on artifacts that have no activation cost or whose activation cost is {0}.').
card_first_print('power artifact', 'ATQ').
card_image_name('power artifact'/'ATQ', 'power artifact').
card_uid('power artifact'/'ATQ', 'ATQ:Power Artifact:power artifact').
card_rarity('power artifact'/'ATQ', 'Uncommon').
card_artist('power artifact'/'ATQ', 'Douglas Shuler').
card_multiverse_id('power artifact'/'ATQ', '1043').

card_in_set('powerleech', 'ATQ').
card_original_type('powerleech'/'ATQ', 'Enchantment').
card_original_text('powerleech'/'ATQ', 'Gain 1 life whenever one of opponent\'s artifacts becomes tapped, or whenever the activation cost of one of opponent\'s artifacts is paid. Is not triggered by continuous artifacts.').
card_first_print('powerleech', 'ATQ').
card_image_name('powerleech'/'ATQ', 'powerleech').
card_uid('powerleech'/'ATQ', 'ATQ:Powerleech:powerleech').
card_rarity('powerleech'/'ATQ', 'Uncommon').
card_artist('powerleech'/'ATQ', 'Christopher Rush').
card_flavor_text('powerleech'/'ATQ', 'The Forest of Argoth has developed a resistance to mechanical intrusion.').
card_multiverse_id('powerleech'/'ATQ', '1055').

card_in_set('priest of yawgmoth', 'ATQ').
card_original_type('priest of yawgmoth'/'ATQ', 'Summon — Cleric').
card_original_text('priest of yawgmoth'/'ATQ', 'Tap to add an amount of black mana equal to target artifact\'s casting cost to your mana pool. This effect is played as an interrupt. Target artifact, which must belong to you, is discarded. This artifact cannot be one that is already on its way to the graveyard, and artifact creatures killed this way may not be regenerated.').
card_first_print('priest of yawgmoth', 'ATQ').
card_image_name('priest of yawgmoth'/'ATQ', 'priest of yawgmoth').
card_uid('priest of yawgmoth'/'ATQ', 'ATQ:Priest of Yawgmoth:priest of yawgmoth').
card_rarity('priest of yawgmoth'/'ATQ', 'Common').
card_artist('priest of yawgmoth'/'ATQ', 'Mark Tedin').
card_multiverse_id('priest of yawgmoth'/'ATQ', '1044').

card_in_set('primal clay', 'ATQ').
card_original_type('primal clay'/'ATQ', 'Artifact Creature').
card_original_text('primal clay'/'ATQ', 'When you cast Primal Clay, you must choose whether to make it a 1/6 wall, a 3/3 creature, or a 2/2 flying creature. Primal Clay then remains in this form until altered by another card or removed from play.').
card_first_print('primal clay', 'ATQ').
card_image_name('primal clay'/'ATQ', 'primal clay').
card_uid('primal clay'/'ATQ', 'ATQ:Primal Clay:primal clay').
card_rarity('primal clay'/'ATQ', 'Uncommon').
card_artist('primal clay'/'ATQ', 'Kaja Foglio').
card_multiverse_id('primal clay'/'ATQ', '1017').

card_in_set('rakalite', 'ATQ').
card_original_type('rakalite'/'ATQ', 'Poly Artifact').
card_original_text('rakalite'/'ATQ', '{2}: Prevent 1 damage to any target. If Rakalite is used, it returns to its owner\'s hand at end of turn; all enchantments on Rakalite are then discarded.').
card_first_print('rakalite', 'ATQ').
card_image_name('rakalite'/'ATQ', 'rakalite').
card_uid('rakalite'/'ATQ', 'ATQ:Rakalite:rakalite').
card_rarity('rakalite'/'ATQ', 'Uncommon').
card_artist('rakalite'/'ATQ', 'Christopher Rush').
card_flavor_text('rakalite'/'ATQ', 'Urza was the first to understand that the war would not be lost for lack of power, but for lack of troops.').
card_multiverse_id('rakalite'/'ATQ', '1018').

card_in_set('reconstruction', 'ATQ').
card_original_type('reconstruction'/'ATQ', 'Sorcery').
card_original_text('reconstruction'/'ATQ', 'Bring one artifact from your graveyard to your hand.').
card_first_print('reconstruction', 'ATQ').
card_image_name('reconstruction'/'ATQ', 'reconstruction').
card_uid('reconstruction'/'ATQ', 'ATQ:Reconstruction:reconstruction').
card_rarity('reconstruction'/'ATQ', 'Common').
card_artist('reconstruction'/'ATQ', 'Anson Maddocks').
card_flavor_text('reconstruction'/'ATQ', 'Tedious research made the Sages of the College of Lat-Nam adept in repairing broken artifacts.').
card_multiverse_id('reconstruction'/'ATQ', '1045').

card_in_set('reverse polarity', 'ATQ').
card_original_type('reverse polarity'/'ATQ', 'Instant').
card_original_text('reverse polarity'/'ATQ', 'All damage done to you so far this turn by artifacts is retroactively added to your life total instead of subtracted. Further damage this turn is treated normally.').
card_first_print('reverse polarity', 'ATQ').
card_image_name('reverse polarity'/'ATQ', 'reverse polarity').
card_uid('reverse polarity'/'ATQ', 'ATQ:Reverse Polarity:reverse polarity').
card_rarity('reverse polarity'/'ATQ', 'Common').
card_artist('reverse polarity'/'ATQ', 'Justin Hampton').
card_multiverse_id('reverse polarity'/'ATQ', '1070').

card_in_set('rocket launcher', 'ATQ').
card_original_type('rocket launcher'/'ATQ', 'Poly Artifact').
card_original_text('rocket launcher'/'ATQ', '{2}: Do 1 damage to any target. Rocket Launcher may not be used until it begins a turn in play on your side. If it is used, Rocket Launcher is destroyed at end of turn.').
card_first_print('rocket launcher', 'ATQ').
card_image_name('rocket launcher'/'ATQ', 'rocket launcher').
card_uid('rocket launcher'/'ATQ', 'ATQ:Rocket Launcher:rocket launcher').
card_rarity('rocket launcher'/'ATQ', 'Uncommon').
card_artist('rocket launcher'/'ATQ', 'Pete Venters').
card_flavor_text('rocket launcher'/'ATQ', 'What these devices lacked in subtlety, they made up in strength.').
card_multiverse_id('rocket launcher'/'ATQ', '1019').

card_in_set('sage of lat-nam', 'ATQ').
card_original_type('sage of lat-nam'/'ATQ', 'Summon — Sage').
card_original_text('sage of lat-nam'/'ATQ', 'Tap to draw a card from your library. Each time you use this ability, you must choose one of your artifacts in play and place it in the graveyard. This artifact cannot be one that is already on its way to the graveyard, and artifact creatures killed this way may not be regenerated.').
card_first_print('sage of lat-nam', 'ATQ').
card_image_name('sage of lat-nam'/'ATQ', 'sage of lat-nam').
card_uid('sage of lat-nam'/'ATQ', 'ATQ:Sage of Lat-Nam:sage of lat-nam').
card_rarity('sage of lat-nam'/'ATQ', 'Common').
card_artist('sage of lat-nam'/'ATQ', 'Pete Venters').
card_multiverse_id('sage of lat-nam'/'ATQ', '1046').

card_in_set('shapeshifter', 'ATQ').
card_original_type('shapeshifter'/'ATQ', 'Artifact Creature').
card_original_text('shapeshifter'/'ATQ', 'The *s below represent any number from 0 to 6. You set * when Shapeshifter is cast, and you may change it during your upkeep.').
card_first_print('shapeshifter', 'ATQ').
card_image_name('shapeshifter'/'ATQ', 'shapeshifter').
card_uid('shapeshifter'/'ATQ', 'ATQ:Shapeshifter:shapeshifter').
card_rarity('shapeshifter'/'ATQ', 'Rare').
card_artist('shapeshifter'/'ATQ', 'Dan Frazier').
card_flavor_text('shapeshifter'/'ATQ', 'Born like a Phoenix from the Flame,\nBut neither Bulk nor Shape the same.\n—Jonathan Swift, \"Vanbrug\'s House\"').
card_multiverse_id('shapeshifter'/'ATQ', '1020').

card_in_set('shatterstorm', 'ATQ').
card_original_type('shatterstorm'/'ATQ', 'Sorcery').
card_original_text('shatterstorm'/'ATQ', 'All artifacts in play are discarded.\nArtifact creatures cannot be regenerated.').
card_first_print('shatterstorm', 'ATQ').
card_image_name('shatterstorm'/'ATQ', 'shatterstorm').
card_uid('shatterstorm'/'ATQ', 'ATQ:Shatterstorm:shatterstorm').
card_rarity('shatterstorm'/'ATQ', 'Rare').
card_artist('shatterstorm'/'ATQ', 'Mark Poole').
card_flavor_text('shatterstorm'/'ATQ', 'Chains of leaping fire and sizzling lightning laid waste the artificers\' handiwork, sparing not a single device.').
card_multiverse_id('shatterstorm'/'ATQ', '1063').

card_in_set('staff of zegon', 'ATQ').
card_original_type('staff of zegon'/'ATQ', 'Mono Artifact').
card_original_text('staff of zegon'/'ATQ', '{3}: Target creature loses -2/-0 until end of turn. Creatures with power less than 1 deal no damage.').
card_first_print('staff of zegon', 'ATQ').
card_image_name('staff of zegon'/'ATQ', 'staff of zegon').
card_uid('staff of zegon'/'ATQ', 'ATQ:Staff of Zegon:staff of zegon').
card_rarity('staff of zegon'/'ATQ', 'Common').
card_artist('staff of zegon'/'ATQ', 'Mark Poole').
card_flavor_text('staff of zegon'/'ATQ', 'Though Mishra was impressed by the staves Ashnod had created for Zegon\'s defense, he understood they only hinted at her full potential.').
card_multiverse_id('staff of zegon'/'ATQ', '1021').

card_in_set('strip mine', 'ATQ').
card_original_type('strip mine'/'ATQ', 'Land').
card_original_text('strip mine'/'ATQ', 'Tap to add 1 colorless mana to your mana pool or place Strip Mine in your graveyard and destroy one land of your choice.').
card_first_print('strip mine', 'ATQ').
card_image_name('strip mine'/'ATQ', 'strip mine1').
card_uid('strip mine'/'ATQ', 'ATQ:Strip Mine:strip mine1').
card_rarity('strip mine'/'ATQ', 'Rare').
card_artist('strip mine'/'ATQ', 'Daniel Gelon').
card_flavor_text('strip mine'/'ATQ', 'Unlike previous conflicts, the war between Urza and Mishra made Dominia itself a casualty of war.').
card_multiverse_id('strip mine'/'ATQ', '1078').

card_in_set('strip mine', 'ATQ').
card_original_type('strip mine'/'ATQ', 'Land').
card_original_text('strip mine'/'ATQ', 'Tap to add 1 colorless mana to your mana pool or place Strip Mine in your graveyard and destroy one land of your choice.').
card_image_name('strip mine'/'ATQ', 'strip mine2').
card_uid('strip mine'/'ATQ', 'ATQ:Strip Mine:strip mine2').
card_rarity('strip mine'/'ATQ', 'Rare').
card_artist('strip mine'/'ATQ', 'Daniel Gelon').
card_flavor_text('strip mine'/'ATQ', 'Unlike previous conflicts, the war between Urza and Mishra made Dominia itself a casualty of war.').
card_multiverse_id('strip mine'/'ATQ', '1077').

card_in_set('strip mine', 'ATQ').
card_original_type('strip mine'/'ATQ', 'Land').
card_original_text('strip mine'/'ATQ', 'Tap to add 1 colorless mana to your mana pool or place Strip Mine in your graveyard and destroy one land of your choice.').
card_image_name('strip mine'/'ATQ', 'strip mine3').
card_uid('strip mine'/'ATQ', 'ATQ:Strip Mine:strip mine3').
card_rarity('strip mine'/'ATQ', 'Rare').
card_artist('strip mine'/'ATQ', 'Daniel Gelon').
card_flavor_text('strip mine'/'ATQ', 'Unlike previous conflicts, the war between Urza and Mishra made Dominia itself a casualty of war.').
card_multiverse_id('strip mine'/'ATQ', '1076').

card_in_set('strip mine', 'ATQ').
card_original_type('strip mine'/'ATQ', 'Land').
card_original_text('strip mine'/'ATQ', 'Tap to add 1 colorless mana to your mana pool or place Strip Mine in your graveyard and destroy one land of your choice.').
card_image_name('strip mine'/'ATQ', 'strip mine4').
card_uid('strip mine'/'ATQ', 'ATQ:Strip Mine:strip mine4').
card_rarity('strip mine'/'ATQ', 'Uncommon').
card_artist('strip mine'/'ATQ', 'Daniel Gelon').
card_flavor_text('strip mine'/'ATQ', 'Unlike previous conflicts, the war between Urza and Mishra made Dominia itself a casualty of war.').
card_multiverse_id('strip mine'/'ATQ', '1079').

card_in_set('su-chi', 'ATQ').
card_original_type('su-chi'/'ATQ', 'Artifact Creature').
card_original_text('su-chi'/'ATQ', 'If Su-Chi goes to the graveyard, its controller gains 4 colorless mana.').
card_first_print('su-chi', 'ATQ').
card_image_name('su-chi'/'ATQ', 'su-chi').
card_uid('su-chi'/'ATQ', 'ATQ:Su-Chi:su-chi').
card_rarity('su-chi'/'ATQ', 'Uncommon').
card_artist('su-chi'/'ATQ', 'Christopher Rush').
card_flavor_text('su-chi'/'ATQ', 'Flawed copies of relics from the Thran Empire, the Su-Chi were inherently unstable but provided useful knowledge for Tocasia\'s students.').
card_multiverse_id('su-chi'/'ATQ', '1022').

card_in_set('tablet of epityr', 'ATQ').
card_original_type('tablet of epityr'/'ATQ', 'Poly Artifact').
card_original_text('tablet of epityr'/'ATQ', '{1}: You gain 1 life every time one of your artifacts goes to the graveyard. Can only give 1 life each time an artifact reaches the graveyard.').
card_first_print('tablet of epityr', 'ATQ').
card_image_name('tablet of epityr'/'ATQ', 'tablet of epityr').
card_uid('tablet of epityr'/'ATQ', 'ATQ:Tablet of Epityr:tablet of epityr').
card_rarity('tablet of epityr'/'ATQ', 'Common').
card_artist('tablet of epityr'/'ATQ', 'Christopher Rush').
card_flavor_text('tablet of epityr'/'ATQ', 'Originally considered the work of Urza, this tablet was created by forgers seeking to imitate Urza\'s masterpieces.').
card_multiverse_id('tablet of epityr'/'ATQ', '1023').

card_in_set('tawnos\'s coffin', 'ATQ').
card_original_type('tawnos\'s coffin'/'ATQ', 'Mono Artifact').
card_original_text('tawnos\'s coffin'/'ATQ', '{3}: Select a creature in play; that creature is considered out of play as long as Coffin remains tapped. Hence the creature cannot be the target of spells and cannot receive damage, use special powers, attack, or defend. All counters and enchantments on the creature remain but are also out of play. If Coffin is untapped or removed, creature returns to play tapped. You may choose not to untap Coffin during the untap phase.').
card_first_print('tawnos\'s coffin', 'ATQ').
card_image_name('tawnos\'s coffin'/'ATQ', 'tawnos\'s coffin').
card_uid('tawnos\'s coffin'/'ATQ', 'ATQ:Tawnos\'s Coffin:tawnos\'s coffin').
card_rarity('tawnos\'s coffin'/'ATQ', 'Rare').
card_artist('tawnos\'s coffin'/'ATQ', 'Christopher Rush').
card_multiverse_id('tawnos\'s coffin'/'ATQ', '1024').

card_in_set('tawnos\'s wand', 'ATQ').
card_original_type('tawnos\'s wand'/'ATQ', 'Mono Artifact').
card_original_text('tawnos\'s wand'/'ATQ', '{2}: Make a creature of power no greater than 2 unblockable by all creatures except artifact creatures until end of turn. Other cards may be used to increase target creature\'s power beyond 2 after defense is chosen.').
card_first_print('tawnos\'s wand', 'ATQ').
card_image_name('tawnos\'s wand'/'ATQ', 'tawnos\'s wand').
card_uid('tawnos\'s wand'/'ATQ', 'ATQ:Tawnos\'s Wand:tawnos\'s wand').
card_rarity('tawnos\'s wand'/'ATQ', 'Uncommon').
card_artist('tawnos\'s wand'/'ATQ', 'Douglas Shuler').
card_multiverse_id('tawnos\'s wand'/'ATQ', '1025').

card_in_set('tawnos\'s weaponry', 'ATQ').
card_original_type('tawnos\'s weaponry'/'ATQ', 'Mono Artifact').
card_original_text('tawnos\'s weaponry'/'ATQ', '{2}: Target creature gains +1/+1 as long as Tawnos\'s Weaponry remains tapped. You may choose not to untap Tawnos\'s Weaponry during untap phase.').
card_first_print('tawnos\'s weaponry', 'ATQ').
card_image_name('tawnos\'s weaponry'/'ATQ', 'tawnos\'s weaponry').
card_uid('tawnos\'s weaponry'/'ATQ', 'ATQ:Tawnos\'s Weaponry:tawnos\'s weaponry').
card_rarity('tawnos\'s weaponry'/'ATQ', 'Uncommon').
card_artist('tawnos\'s weaponry'/'ATQ', 'Dan Frazier').
card_flavor_text('tawnos\'s weaponry'/'ATQ', 'When Urza\'s war machines became too costly, Tawnos\'s weaponry replaced them.').
card_multiverse_id('tawnos\'s weaponry'/'ATQ', '1026').

card_in_set('tetravus', 'ATQ').
card_original_type('tetravus'/'ATQ', 'Artifact Creature').
card_original_text('tetravus'/'ATQ', 'Flying\nTetravus gets three +1/+1 counters when cast. During your upkeep, you may move each of these counters on or off Tetravus.  Counters moved off of Tetravus become independent 1/1 flying artifact creatures.  If such a creature dies, the counter is removed from play.  Such creatures may not have enchantments cast on them, and they do not share any enchantments on Tetravus.').
card_first_print('tetravus', 'ATQ').
card_image_name('tetravus'/'ATQ', 'tetravus').
card_uid('tetravus'/'ATQ', 'ATQ:Tetravus:tetravus').
card_rarity('tetravus'/'ATQ', 'Rare').
card_artist('tetravus'/'ATQ', 'Mark Tedin').
card_multiverse_id('tetravus'/'ATQ', '1027').

card_in_set('the rack', 'ATQ').
card_original_type('the rack'/'ATQ', 'Continuous Artifact').
card_original_text('the rack'/'ATQ', 'If opponent has fewer than three cards in hand during his or her upkeep, the Rack does 1 damage to opponent for each card fewer than three.').
card_first_print('the rack', 'ATQ').
card_image_name('the rack'/'ATQ', 'the rack').
card_uid('the rack'/'ATQ', 'ATQ:The Rack:the rack').
card_rarity('the rack'/'ATQ', 'Uncommon').
card_artist('the rack'/'ATQ', 'Richard Thomas').
card_flavor_text('the rack'/'ATQ', 'Invented in Mishra\'s earlier days, the Rack was once his most feared creation.').
card_multiverse_id('the rack'/'ATQ', '1028').

card_in_set('titania\'s song', 'ATQ').
card_original_type('titania\'s song'/'ATQ', 'Enchantment').
card_original_text('titania\'s song'/'ATQ', 'All non-creature artifacts in play lose all their usual abilities and become artifact creatures with toughness and power both equal to their casting costs. If Titania\'s Song leaves play, artifacts return to normal just before the untap phase of the next turn.').
card_first_print('titania\'s song', 'ATQ').
card_image_name('titania\'s song'/'ATQ', 'titania\'s song').
card_uid('titania\'s song'/'ATQ', 'ATQ:Titania\'s Song:titania\'s song').
card_rarity('titania\'s song'/'ATQ', 'Uncommon').
card_artist('titania\'s song'/'ATQ', 'Kerstin Kaman').
card_multiverse_id('titania\'s song'/'ATQ', '1056').

card_in_set('transmute artifact', 'ATQ').
card_original_type('transmute artifact'/'ATQ', 'Sorcery').
card_original_text('transmute artifact'/'ATQ', 'Search through your library for one artifact and immediately place it into play; also, choose any artifact in play that you control and place it in its owner\'s graveyard. If the new artifact has a casting cost greater than that of the discarded one, you must pay the difference or Transmute Artifact fails and both artifacts are discarded. Shuffle your library after playing this card.').
card_first_print('transmute artifact', 'ATQ').
card_image_name('transmute artifact'/'ATQ', 'transmute artifact').
card_uid('transmute artifact'/'ATQ', 'ATQ:Transmute Artifact:transmute artifact').
card_rarity('transmute artifact'/'ATQ', 'Uncommon').
card_artist('transmute artifact'/'ATQ', 'Anson Maddocks').
card_multiverse_id('transmute artifact'/'ATQ', '1047').

card_in_set('triskelion', 'ATQ').
card_original_type('triskelion'/'ATQ', 'Artifact Creature').
card_original_text('triskelion'/'ATQ', 'Triskelion gets three +1/+1 counters when cast. Controller may discard a +1/+1 counter at any time to do 1 damage to any target.').
card_first_print('triskelion', 'ATQ').
card_image_name('triskelion'/'ATQ', 'triskelion').
card_uid('triskelion'/'ATQ', 'ATQ:Triskelion:triskelion').
card_rarity('triskelion'/'ATQ', 'Rare').
card_artist('triskelion'/'ATQ', 'Douglas Shuler').
card_flavor_text('triskelion'/'ATQ', 'A brainchild of Tawnos, the Triskelion proved its versatility and usefulness in many of the later battles between the brothers.').
card_multiverse_id('triskelion'/'ATQ', '1029').

card_in_set('urza\'s avenger', 'ATQ').
card_original_type('urza\'s avenger'/'ATQ', 'Artifact Creature').
card_original_text('urza\'s avenger'/'ATQ', '{0}: Avenger loses -1/-1 and gains one of your choice of flying, banding, first strike, or trample until end of turn. Attribute losses and ability gains are cumulative.').
card_first_print('urza\'s avenger', 'ATQ').
card_image_name('urza\'s avenger'/'ATQ', 'urza\'s avenger').
card_uid('urza\'s avenger'/'ATQ', 'ATQ:Urza\'s Avenger:urza\'s avenger').
card_rarity('urza\'s avenger'/'ATQ', 'Rare').
card_artist('urza\'s avenger'/'ATQ', 'Amy Weber').
card_flavor_text('urza\'s avenger'/'ATQ', 'Unable to settle on just one design, Urza decided to create one versatile being.').
card_multiverse_id('urza\'s avenger'/'ATQ', '1030').

card_in_set('urza\'s chalice', 'ATQ').
card_original_type('urza\'s chalice'/'ATQ', 'Poly Artifact').
card_original_text('urza\'s chalice'/'ATQ', '{1}: Any artifact cast by any player gives you 1 life. Can only give 1 life each time an artifact is cast.').
card_first_print('urza\'s chalice', 'ATQ').
card_image_name('urza\'s chalice'/'ATQ', 'urza\'s chalice').
card_uid('urza\'s chalice'/'ATQ', 'ATQ:Urza\'s Chalice:urza\'s chalice').
card_rarity('urza\'s chalice'/'ATQ', 'Common').
card_artist('urza\'s chalice'/'ATQ', 'Jeff A. Menges').
card_flavor_text('urza\'s chalice'/'ATQ', 'When sorely wounded or tired, Urza would often retreat to the workshops of his apprentices. They were greatly amazed at how much better he looked each time he took a sip of water.').
card_multiverse_id('urza\'s chalice'/'ATQ', '1031').

card_in_set('urza\'s mine', 'ATQ').
card_original_type('urza\'s mine'/'ATQ', 'Land').
card_original_text('urza\'s mine'/'ATQ', 'Tap to add 1 colorless mana to your mana pool. If you have Urza\'s Mine, Urza\'s Tower, and Urza\'s Power Plant in play at the same time, tap to add 2 colorless mana to your mana pool.').
card_first_print('urza\'s mine', 'ATQ').
card_image_name('urza\'s mine'/'ATQ', 'urza\'s mine1').
card_uid('urza\'s mine'/'ATQ', 'ATQ:Urza\'s Mine:urza\'s mine1').
card_rarity('urza\'s mine'/'ATQ', 'Common').
card_artist('urza\'s mine'/'ATQ', 'Anson Maddocks').
card_flavor_text('urza\'s mine'/'ATQ', 'Mines became common as cities during the days of the artificers.').
card_multiverse_id('urza\'s mine'/'ATQ', '1082').

card_in_set('urza\'s mine', 'ATQ').
card_original_type('urza\'s mine'/'ATQ', 'Land').
card_original_text('urza\'s mine'/'ATQ', 'Tap to add 1 colorless mana to your mana pool. If you have Urza\'s Mine, Urza\'s Tower, and Urza\'s Power Plant in play at the same time, tap to add 2 colorless mana to your mana pool.').
card_image_name('urza\'s mine'/'ATQ', 'urza\'s mine2').
card_uid('urza\'s mine'/'ATQ', 'ATQ:Urza\'s Mine:urza\'s mine2').
card_rarity('urza\'s mine'/'ATQ', 'Uncommon').
card_artist('urza\'s mine'/'ATQ', 'Anson Maddocks').
card_flavor_text('urza\'s mine'/'ATQ', 'Mines became common as cities during the days of the artificers.').
card_multiverse_id('urza\'s mine'/'ATQ', '1081').

card_in_set('urza\'s mine', 'ATQ').
card_original_type('urza\'s mine'/'ATQ', 'Land').
card_original_text('urza\'s mine'/'ATQ', 'Tap to add 1 colorless mana to your mana pool. If you have Urza\'s Mine, Urza\'s Tower, and Urza\'s Power Plant in play at the same time, tap to add 2 colorless mana to your mana pool.').
card_image_name('urza\'s mine'/'ATQ', 'urza\'s mine3').
card_uid('urza\'s mine'/'ATQ', 'ATQ:Urza\'s Mine:urza\'s mine3').
card_rarity('urza\'s mine'/'ATQ', 'Uncommon').
card_artist('urza\'s mine'/'ATQ', 'Anson Maddocks').
card_flavor_text('urza\'s mine'/'ATQ', 'Mines became common as cities during the days of the artificers.').
card_multiverse_id('urza\'s mine'/'ATQ', '1083').

card_in_set('urza\'s mine', 'ATQ').
card_original_type('urza\'s mine'/'ATQ', 'Land').
card_original_text('urza\'s mine'/'ATQ', 'Tap to add 1 colorless mana to your mana pool. If you have Urza\'s Mine, Urza\'s Tower, and Urza\'s Power Plant in play at the same time, tap to add 2 colorless mana to your mana pool.').
card_image_name('urza\'s mine'/'ATQ', 'urza\'s mine4').
card_uid('urza\'s mine'/'ATQ', 'ATQ:Urza\'s Mine:urza\'s mine4').
card_rarity('urza\'s mine'/'ATQ', 'Common').
card_artist('urza\'s mine'/'ATQ', 'Anson Maddocks').
card_flavor_text('urza\'s mine'/'ATQ', 'Mines became common as cities during the days of the artificers.').
card_multiverse_id('urza\'s mine'/'ATQ', '1080').

card_in_set('urza\'s miter', 'ATQ').
card_original_type('urza\'s miter'/'ATQ', 'Poly Artifact').
card_original_text('urza\'s miter'/'ATQ', '{3}: Draw one card from your library every time an artifact of yours goes to the graveyard. Can only let you draw one card per artifact destruction. May not be used when you destroy an artifact to gain benefits from another card.').
card_first_print('urza\'s miter', 'ATQ').
card_image_name('urza\'s miter'/'ATQ', 'urza\'s miter').
card_uid('urza\'s miter'/'ATQ', 'ATQ:Urza\'s Miter:urza\'s miter').
card_rarity('urza\'s miter'/'ATQ', 'Rare').
card_artist('urza\'s miter'/'ATQ', 'Randy Asplund-Faith').
card_multiverse_id('urza\'s miter'/'ATQ', '1032').

card_in_set('urza\'s power plant', 'ATQ').
card_original_type('urza\'s power plant'/'ATQ', 'Land').
card_original_text('urza\'s power plant'/'ATQ', 'Tap to add 1 colorless mana to your mana pool. If you have Urza\'s Mine, Urza\'s Tower, and Urza\'s Power Plant in play at the same time, tap to add 2 colorless mana to your mana pool.').
card_first_print('urza\'s power plant', 'ATQ').
card_image_name('urza\'s power plant'/'ATQ', 'urza\'s power plant1').
card_uid('urza\'s power plant'/'ATQ', 'ATQ:Urza\'s Power Plant:urza\'s power plant1').
card_rarity('urza\'s power plant'/'ATQ', 'Common').
card_artist('urza\'s power plant'/'ATQ', 'Mark Tedin').
card_flavor_text('urza\'s power plant'/'ATQ', 'Artifact construction requires immense resources.').
card_multiverse_id('urza\'s power plant'/'ATQ', '1085').

card_in_set('urza\'s power plant', 'ATQ').
card_original_type('urza\'s power plant'/'ATQ', 'Land').
card_original_text('urza\'s power plant'/'ATQ', 'Tap to add 1 colorless mana to your mana pool. If you have Urza\'s Mine, Urza\'s Tower, and Urza\'s Power Plant in play at the same time, tap to add 2 colorless mana to your mana pool.').
card_image_name('urza\'s power plant'/'ATQ', 'urza\'s power plant2').
card_uid('urza\'s power plant'/'ATQ', 'ATQ:Urza\'s Power Plant:urza\'s power plant2').
card_rarity('urza\'s power plant'/'ATQ', 'Uncommon').
card_artist('urza\'s power plant'/'ATQ', 'Mark Tedin').
card_flavor_text('urza\'s power plant'/'ATQ', 'Artifact construction requires immense resources.').
card_multiverse_id('urza\'s power plant'/'ATQ', '1084').

card_in_set('urza\'s power plant', 'ATQ').
card_original_type('urza\'s power plant'/'ATQ', 'Land').
card_original_text('urza\'s power plant'/'ATQ', 'Tap to add 1 colorless mana to your mana pool. If you have Urza\'s Mine, Urza\'s Tower, and Urza\'s Power Plant in play at the same time, tap to add 2 colorless mana to your mana pool.').
card_image_name('urza\'s power plant'/'ATQ', 'urza\'s power plant3').
card_uid('urza\'s power plant'/'ATQ', 'ATQ:Urza\'s Power Plant:urza\'s power plant3').
card_rarity('urza\'s power plant'/'ATQ', 'Uncommon').
card_artist('urza\'s power plant'/'ATQ', 'Mark Tedin').
card_flavor_text('urza\'s power plant'/'ATQ', 'Artifact construction requires immense resources.').
card_multiverse_id('urza\'s power plant'/'ATQ', '1087').

card_in_set('urza\'s power plant', 'ATQ').
card_original_type('urza\'s power plant'/'ATQ', 'Land').
card_original_text('urza\'s power plant'/'ATQ', 'Tap to add 1 colorless mana to your mana pool. If you have Urza\'s Mine, Urza\'s Tower, and Urza\'s Power Plant in play at the same time, tap to add 2 colorless mana to your mana pool.').
card_image_name('urza\'s power plant'/'ATQ', 'urza\'s power plant4').
card_uid('urza\'s power plant'/'ATQ', 'ATQ:Urza\'s Power Plant:urza\'s power plant4').
card_rarity('urza\'s power plant'/'ATQ', 'Common').
card_artist('urza\'s power plant'/'ATQ', 'Mark Tedin').
card_flavor_text('urza\'s power plant'/'ATQ', 'Artifact construction requires immense resources.').
card_multiverse_id('urza\'s power plant'/'ATQ', '1086').

card_in_set('urza\'s tower', 'ATQ').
card_original_type('urza\'s tower'/'ATQ', 'Land').
card_original_text('urza\'s tower'/'ATQ', 'Tap to add 1 colorless mana to your mana pool. If you have Urza\'s Mine, Urza\'s Tower, and Urza\'s Power Plant in play at the same time, tap to add 3 colorless mana to your mana pool.').
card_first_print('urza\'s tower', 'ATQ').
card_image_name('urza\'s tower'/'ATQ', 'urza\'s tower1').
card_uid('urza\'s tower'/'ATQ', 'ATQ:Urza\'s Tower:urza\'s tower1').
card_rarity('urza\'s tower'/'ATQ', 'Uncommon').
card_artist('urza\'s tower'/'ATQ', 'Mark Poole').
card_flavor_text('urza\'s tower'/'ATQ', 'Urza always put Tocasia\'s lessons on resource-gathering to effective use.').
card_multiverse_id('urza\'s tower'/'ATQ', '1088').

card_in_set('urza\'s tower', 'ATQ').
card_original_type('urza\'s tower'/'ATQ', 'Land').
card_original_text('urza\'s tower'/'ATQ', 'Tap to add 1 colorless mana to your mana pool. If you have Urza\'s Mine, Urza\'s Tower, and Urza\'s Power Plant in play at the same time, tap to add 3 colorless mana to your mana pool.').
card_image_name('urza\'s tower'/'ATQ', 'urza\'s tower2').
card_uid('urza\'s tower'/'ATQ', 'ATQ:Urza\'s Tower:urza\'s tower2').
card_rarity('urza\'s tower'/'ATQ', 'Uncommon').
card_artist('urza\'s tower'/'ATQ', 'Mark Poole').
card_flavor_text('urza\'s tower'/'ATQ', 'Urza always put Tocasia\'s lessons on resource-gathering to effective use.').
card_multiverse_id('urza\'s tower'/'ATQ', '1091').

card_in_set('urza\'s tower', 'ATQ').
card_original_type('urza\'s tower'/'ATQ', 'Land').
card_original_text('urza\'s tower'/'ATQ', 'Tap to add 1 colorless mana to your mana pool. If you have Urza\'s Mine, Urza\'s Tower, and Urza\'s Power Plant in play at the same time, tap to add 3 colorless mana to your mana pool.').
card_image_name('urza\'s tower'/'ATQ', 'urza\'s tower3').
card_uid('urza\'s tower'/'ATQ', 'ATQ:Urza\'s Tower:urza\'s tower3').
card_rarity('urza\'s tower'/'ATQ', 'Common').
card_artist('urza\'s tower'/'ATQ', 'Mark Poole').
card_flavor_text('urza\'s tower'/'ATQ', 'Urza always put Tocasia\'s lessons on resource-gathering to effective use.').
card_multiverse_id('urza\'s tower'/'ATQ', '1090').

card_in_set('urza\'s tower', 'ATQ').
card_original_type('urza\'s tower'/'ATQ', 'Land').
card_original_text('urza\'s tower'/'ATQ', 'Tap to add 1 colorless mana to your mana pool. If you have Urza\'s Mine, Urza\'s Tower, and Urza\'s Power Plant in play at the same time, tap to add 3 colorless mana to your mana pool.').
card_image_name('urza\'s tower'/'ATQ', 'urza\'s tower4').
card_uid('urza\'s tower'/'ATQ', 'ATQ:Urza\'s Tower:urza\'s tower4').
card_rarity('urza\'s tower'/'ATQ', 'Uncommon').
card_artist('urza\'s tower'/'ATQ', 'Mark Poole').
card_flavor_text('urza\'s tower'/'ATQ', 'Urza always put Tocasia\'s lessons on resource-gathering to effective use.').
card_multiverse_id('urza\'s tower'/'ATQ', '1089').

card_in_set('wall of spears', 'ATQ').
card_original_type('wall of spears'/'ATQ', 'Artifact Creature').
card_original_text('wall of spears'/'ATQ', 'First strike, counts as a wall.').
card_first_print('wall of spears', 'ATQ').
card_image_name('wall of spears'/'ATQ', 'wall of spears').
card_uid('wall of spears'/'ATQ', 'ATQ:Wall of Spears:wall of spears').
card_rarity('wall of spears'/'ATQ', 'Uncommon').
card_artist('wall of spears'/'ATQ', 'Sandra Everingham').
card_flavor_text('wall of spears'/'ATQ', 'Even the most conservative generals revised their tactics after the Battle of Sarinth, during which a handful of peasant-pikemen held off a trio of rampaging Craw Wurms.').
card_multiverse_id('wall of spears'/'ATQ', '1033').

card_in_set('weakstone', 'ATQ').
card_original_type('weakstone'/'ATQ', 'Continuous Artifact').
card_original_text('weakstone'/'ATQ', 'All attacking creatures lose -1/-0. Creatures with power less than 1 deal no damage.').
card_first_print('weakstone', 'ATQ').
card_image_name('weakstone'/'ATQ', 'weakstone').
card_uid('weakstone'/'ATQ', 'ATQ:Weakstone:weakstone').
card_rarity('weakstone'/'ATQ', 'Uncommon').
card_artist('weakstone'/'ATQ', 'Justin Hampton').
card_flavor_text('weakstone'/'ATQ', 'During the brothers\' childhood, Tocasia took them to explore the sacred cave of Koilos. There, in the Hall of Tagsin, Mishra discovered the mysterious weakstone.').
card_multiverse_id('weakstone'/'ATQ', '1034').

card_in_set('xenic poltergeist', 'ATQ').
card_original_type('xenic poltergeist'/'ATQ', 'Summon — Poltergeist').
card_original_text('xenic poltergeist'/'ATQ', 'Tap to turn target non-creature artifact into an artifact creature with both power and toughness equal to its casting cost. This transformation lasts until your next upkeep; target retains all its original abilities as well.').
card_first_print('xenic poltergeist', 'ATQ').
card_image_name('xenic poltergeist'/'ATQ', 'xenic poltergeist').
card_uid('xenic poltergeist'/'ATQ', 'ATQ:Xenic Poltergeist:xenic poltergeist').
card_rarity('xenic poltergeist'/'ATQ', 'Uncommon').
card_artist('xenic poltergeist'/'ATQ', 'Dan Frazier').
card_multiverse_id('xenic poltergeist'/'ATQ', '1048').

card_in_set('yawgmoth demon', 'ATQ').
card_original_type('yawgmoth demon'/'ATQ', 'Summon — Demon').
card_original_text('yawgmoth demon'/'ATQ', 'Flying, first strike\nDuring your upkeep, choose one of your artifacts in play and place it in the graveyard, or Yawgmoth Demon becomes tapped and deals 2 points of damage to you. Artifact creatures destroyed this way may not be regenerated.').
card_first_print('yawgmoth demon', 'ATQ').
card_image_name('yawgmoth demon'/'ATQ', 'yawgmoth demon').
card_uid('yawgmoth demon'/'ATQ', 'ATQ:Yawgmoth Demon:yawgmoth demon').
card_rarity('yawgmoth demon'/'ATQ', 'Rare').
card_artist('yawgmoth demon'/'ATQ', 'Sandra Everingham').
card_multiverse_id('yawgmoth demon'/'ATQ', '1049').

card_in_set('yotian soldier', 'ATQ').
card_original_type('yotian soldier'/'ATQ', 'Artifact Creature').
card_original_text('yotian soldier'/'ATQ', 'Attacking does not cause Yotian Soldier to tap.').
card_first_print('yotian soldier', 'ATQ').
card_image_name('yotian soldier'/'ATQ', 'yotian soldier').
card_uid('yotian soldier'/'ATQ', 'ATQ:Yotian Soldier:yotian soldier').
card_rarity('yotian soldier'/'ATQ', 'Common').
card_artist('yotian soldier'/'ATQ', 'Christopher Rush').
card_flavor_text('yotian soldier'/'ATQ', 'After Kroog was destroyed while most of its defenders were at his side, Urza vowed that none of his allies would ever need to fear for their own defense again, even while laying siege to a city far from their homes.').
card_multiverse_id('yotian soldier'/'ATQ', '1035').
