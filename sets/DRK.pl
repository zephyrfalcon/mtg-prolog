% The Dark

set('DRK').
set_name('DRK', 'The Dark').
set_release_date('DRK', '1994-08-01').
set_border('DRK', 'black').
set_type('DRK', 'expansion').

card_in_set('amnesia', 'DRK').
card_original_type('amnesia'/'DRK', 'Sorcery').
card_original_text('amnesia'/'DRK', 'Look at target player\'s hand. Target player discards all non-land cards in his or her hand.').
card_first_print('amnesia', 'DRK').
card_image_name('amnesia'/'DRK', 'amnesia').
card_uid('amnesia'/'DRK', 'DRK:Amnesia:amnesia').
card_rarity('amnesia'/'DRK', 'Uncommon').
card_artist('amnesia'/'DRK', 'Mark Poole').
card_flavor_text('amnesia'/'DRK', '\"When one has witnessed the unspeakable, \'tis sometimes better to forget.\" —Vervamon the Elder').
card_multiverse_id('amnesia'/'DRK', '1746').

card_in_set('angry mob', 'DRK').
card_original_type('angry mob'/'DRK', 'Summon — Mob').
card_original_text('angry mob'/'DRK', 'Trample\nDuring your turn, the *s below are both equal to the total number of swamps all opponents control. During any other player\'s turn, * equals 0.').
card_first_print('angry mob', 'DRK').
card_image_name('angry mob'/'DRK', 'angry mob').
card_uid('angry mob'/'DRK', 'DRK:Angry Mob:angry mob').
card_rarity('angry mob'/'DRK', 'Uncommon').
card_artist('angry mob'/'DRK', 'Drew Tucker').
card_multiverse_id('angry mob'/'DRK', '1801').

card_in_set('apprentice wizard', 'DRK').
card_original_type('apprentice wizard'/'DRK', 'Summon — Wizard').
card_original_text('apprentice wizard'/'DRK', '{U}, {T}: Add {3} to your mana pool. This ability is played as an interrupt.').
card_first_print('apprentice wizard', 'DRK').
card_image_name('apprentice wizard'/'DRK', 'apprentice wizard').
card_uid('apprentice wizard'/'DRK', 'DRK:Apprentice Wizard:apprentice wizard').
card_rarity('apprentice wizard'/'DRK', 'Rare').
card_artist('apprentice wizard'/'DRK', 'Dan Frazier').
card_multiverse_id('apprentice wizard'/'DRK', '1747').

card_in_set('ashes to ashes', 'DRK').
card_original_type('ashes to ashes'/'DRK', 'Sorcery').
card_original_text('ashes to ashes'/'DRK', 'Ashes to Ashes removes two target non-artifact creatures from the game and does 5 damage to you.').
card_first_print('ashes to ashes', 'DRK').
card_image_name('ashes to ashes'/'DRK', 'ashes to ashes').
card_uid('ashes to ashes'/'DRK', 'DRK:Ashes to Ashes:ashes to ashes').
card_rarity('ashes to ashes'/'DRK', 'Common').
card_artist('ashes to ashes'/'DRK', 'Drew Tucker').
card_flavor_text('ashes to ashes'/'DRK', '\"All rivers eventually run to the sea. My job is to sort out who goes first.\"\n—Maeveen O\'Donagh, Memoirs of a Soldier').
card_multiverse_id('ashes to ashes'/'DRK', '1728').

card_in_set('ball lightning', 'DRK').
card_original_type('ball lightning'/'DRK', 'Summon — Ball Lightning').
card_original_text('ball lightning'/'DRK', 'Trample\nBall Lightning may attack on the turn during which it is summoned. Ball Lightning is buried at the end of the turn during which it is summoned.').
card_first_print('ball lightning', 'DRK').
card_image_name('ball lightning'/'DRK', 'ball lightning').
card_uid('ball lightning'/'DRK', 'DRK:Ball Lightning:ball lightning').
card_rarity('ball lightning'/'DRK', 'Rare').
card_artist('ball lightning'/'DRK', 'Quinton Hoover').
card_multiverse_id('ball lightning'/'DRK', '1783').

card_in_set('banshee', 'DRK').
card_original_type('banshee'/'DRK', 'Summon — Banshee').
card_original_text('banshee'/'DRK', '{X}, {T}: Banshee does X damage—half (rounded up) to you and half (rounded down) to any one target.').
card_first_print('banshee', 'DRK').
card_image_name('banshee'/'DRK', 'banshee').
card_uid('banshee'/'DRK', 'DRK:Banshee:banshee').
card_rarity('banshee'/'DRK', 'Uncommon').
card_artist('banshee'/'DRK', 'Jesper Myrfors').
card_flavor_text('banshee'/'DRK', 'Some say Banshees are the hounds of Death, baying to herd their prey into the arms of their master.').
card_multiverse_id('banshee'/'DRK', '1729').

card_in_set('barl\'s cage', 'DRK').
card_original_type('barl\'s cage'/'DRK', 'Artifact').
card_original_text('barl\'s cage'/'DRK', '{3}: Target creature does not untap as normal during its controller\'s next untap phase.').
card_first_print('barl\'s cage', 'DRK').
card_image_name('barl\'s cage'/'DRK', 'barl\'s cage').
card_uid('barl\'s cage'/'DRK', 'DRK:Barl\'s Cage:barl\'s cage').
card_rarity('barl\'s cage'/'DRK', 'Rare').
card_artist('barl\'s cage'/'DRK', 'Tom Wänerstrand').
card_flavor_text('barl\'s cage'/'DRK', 'For a dozen years the Cage had held Lord Ith, but as the Pretender Mairsil\'s power weakened, so did the bars.').
card_multiverse_id('barl\'s cage'/'DRK', '1708').

card_in_set('blood moon', 'DRK').
card_original_type('blood moon'/'DRK', 'Enchantment').
card_original_text('blood moon'/'DRK', 'All non-basic lands are now basic mountains.').
card_first_print('blood moon', 'DRK').
card_image_name('blood moon'/'DRK', 'blood moon').
card_uid('blood moon'/'DRK', 'DRK:Blood Moon:blood moon').
card_rarity('blood moon'/'DRK', 'Rare').
card_artist('blood moon'/'DRK', 'Tom Wänerstrand').
card_flavor_text('blood moon'/'DRK', 'Heavy light flooded across the landscape, cloaking everything in deep crimson.').
card_multiverse_id('blood moon'/'DRK', '1784').

card_in_set('blood of the martyr', 'DRK').
card_original_type('blood of the martyr'/'DRK', 'Instant').
card_original_text('blood of the martyr'/'DRK', 'For the remainder of the turn, you may redirect damage done to any number of creatures to yourself instead.').
card_first_print('blood of the martyr', 'DRK').
card_image_name('blood of the martyr'/'DRK', 'blood of the martyr').
card_uid('blood of the martyr'/'DRK', 'DRK:Blood of the Martyr:blood of the martyr').
card_rarity('blood of the martyr'/'DRK', 'Uncommon').
card_artist('blood of the martyr'/'DRK', 'Christopher Rush').
card_flavor_text('blood of the martyr'/'DRK', 'The willow knows what the storm does not: that the power to endure harm outlives the power to inflict it.').
card_multiverse_id('blood of the martyr'/'DRK', '1802').

card_in_set('bog imp', 'DRK').
card_original_type('bog imp'/'DRK', 'Summon — Imp').
card_original_text('bog imp'/'DRK', 'Flying').
card_first_print('bog imp', 'DRK').
card_image_name('bog imp'/'DRK', 'bog imp').
card_uid('bog imp'/'DRK', 'DRK:Bog Imp:bog imp').
card_rarity('bog imp'/'DRK', 'Common').
card_artist('bog imp'/'DRK', 'Ron Spencer').
card_flavor_text('bog imp'/'DRK', 'On guard for larger dangers, we underestimated the power and speed of the Imp\'s muck-crusted claws.').
card_multiverse_id('bog imp'/'DRK', '1730').

card_in_set('bog rats', 'DRK').
card_original_type('bog rats'/'DRK', 'Summon — Rats').
card_original_text('bog rats'/'DRK', 'Cannot be blocked by walls.').
card_first_print('bog rats', 'DRK').
card_image_name('bog rats'/'DRK', 'bog rats').
card_uid('bog rats'/'DRK', 'DRK:Bog Rats:bog rats').
card_rarity('bog rats'/'DRK', 'Common').
card_artist('bog rats'/'DRK', 'Ron Spencer').
card_flavor_text('bog rats'/'DRK', 'Their stench was vile and strong enough, but not nearly as powerful as their hunger.').
card_multiverse_id('bog rats'/'DRK', '1731').

card_in_set('bone flute', 'DRK').
card_original_type('bone flute'/'DRK', 'Artifact').
card_original_text('bone flute'/'DRK', '{2}, {T}: All creatures get -1/-0 until end of turn.').
card_first_print('bone flute', 'DRK').
card_image_name('bone flute'/'DRK', 'bone flute').
card_uid('bone flute'/'DRK', 'DRK:Bone Flute:bone flute').
card_rarity('bone flute'/'DRK', 'Uncommon').
card_artist('bone flute'/'DRK', 'Christopher Rush').
card_flavor_text('bone flute'/'DRK', 'After the Battle of Pitdown, Lady Ursnell fashioned the first such instrument out of Lord Ursnell\'s left leg.').
card_multiverse_id('bone flute'/'DRK', '1709').

card_in_set('book of rass', 'DRK').
card_original_type('book of rass'/'DRK', 'Artifact').
card_original_text('book of rass'/'DRK', '{2}: Pay 2 life to draw one card. Effects that prevent or redirect damage may not be used to counter this loss of life.').
card_first_print('book of rass', 'DRK').
card_image_name('book of rass'/'DRK', 'book of rass').
card_uid('book of rass'/'DRK', 'DRK:Book of Rass:book of rass').
card_rarity('book of rass'/'DRK', 'Uncommon').
card_artist('book of rass'/'DRK', 'Sandra Everingham').
card_multiverse_id('book of rass'/'DRK', '1710').

card_in_set('brainwash', 'DRK').
card_original_type('brainwash'/'DRK', 'Enchant Creature').
card_original_text('brainwash'/'DRK', 'Target creature may not attack unless its controller pays {3} in addition to any other costs required for the creature to attack.').
card_first_print('brainwash', 'DRK').
card_image_name('brainwash'/'DRK', 'brainwash').
card_uid('brainwash'/'DRK', 'DRK:Brainwash:brainwash').
card_rarity('brainwash'/'DRK', 'Common').
card_artist('brainwash'/'DRK', 'Pete Venters').
card_flavor_text('brainwash'/'DRK', '\"They\'re not your friends; they despise you. I\'m the only one you can count on. Trust me.\"').
card_multiverse_id('brainwash'/'DRK', '1803').

card_in_set('brothers of fire', 'DRK').
card_original_type('brothers of fire'/'DRK', 'Summon — Brothers').
card_original_text('brothers of fire'/'DRK', '{1}{R}{R} Brothers of Fire do 1 damage to any target and 1 damage to you.').
card_first_print('brothers of fire', 'DRK').
card_image_name('brothers of fire'/'DRK', 'brothers of fire').
card_uid('brothers of fire'/'DRK', 'DRK:Brothers of Fire:brothers of fire').
card_rarity('brothers of fire'/'DRK', 'Uncommon').
card_artist('brothers of fire'/'DRK', 'Mark Tedin').
card_flavor_text('brothers of fire'/'DRK', 'Fire is never a gentle master.').
card_multiverse_id('brothers of fire'/'DRK', '1785').

card_in_set('carnivorous plant', 'DRK').
card_original_type('carnivorous plant'/'DRK', 'Summon — Wall').
card_original_text('carnivorous plant'/'DRK', '').
card_first_print('carnivorous plant', 'DRK').
card_image_name('carnivorous plant'/'DRK', 'carnivorous plant').
card_uid('carnivorous plant'/'DRK', 'DRK:Carnivorous Plant:carnivorous plant').
card_rarity('carnivorous plant'/'DRK', 'Common').
card_artist('carnivorous plant'/'DRK', 'Quinton Hoover').
card_flavor_text('carnivorous plant'/'DRK', '\"It had a mouth like that of a great beast, and gnashed its teeth as it strained to reach us. I am thankful it possessed no means of locomotion.\" —Vervamon the Elder').
card_multiverse_id('carnivorous plant'/'DRK', '1765').

card_in_set('cave people', 'DRK').
card_original_type('cave people'/'DRK', 'Summon — Cave People').
card_original_text('cave people'/'DRK', 'If declared as an attacker, Cave People get +1/-2 until end of turn.\n{1}{R}{R}, {T}: Target creature gains mountainwalk until end of turn.').
card_first_print('cave people', 'DRK').
card_image_name('cave people'/'DRK', 'cave people').
card_uid('cave people'/'DRK', 'DRK:Cave People:cave people').
card_rarity('cave people'/'DRK', 'Uncommon').
card_artist('cave people'/'DRK', 'Drew Tucker').
card_multiverse_id('cave people'/'DRK', '1786').

card_in_set('city of shadows', 'DRK').
card_original_type('city of shadows'/'DRK', 'Land').
card_original_text('city of shadows'/'DRK', '{T}: Sacrifice one of your creatures, but remove it from the game instead of placing it in your graveyard. Put a counter on City of Shadows. \n{T}: Add X colorless mana to your mana pool, where X is the number of counters on City of Shadows.').
card_first_print('city of shadows', 'DRK').
card_image_name('city of shadows'/'DRK', 'city of shadows').
card_uid('city of shadows'/'DRK', 'DRK:City of Shadows:city of shadows').
card_rarity('city of shadows'/'DRK', 'Rare').
card_artist('city of shadows'/'DRK', 'Tom Wänerstrand').
card_multiverse_id('city of shadows'/'DRK', '1823').

card_in_set('cleansing', 'DRK').
card_original_type('cleansing'/'DRK', 'Sorcery').
card_original_text('cleansing'/'DRK', 'All land is destroyed. Players may prevent Cleansing from destroying specific lands by paying 1 life for each land they wish to protect. Effects that prevent or redirect damage may not be used to counter this loss of life.').
card_first_print('cleansing', 'DRK').
card_image_name('cleansing'/'DRK', 'cleansing').
card_uid('cleansing'/'DRK', 'DRK:Cleansing:cleansing').
card_rarity('cleansing'/'DRK', 'Rare').
card_artist('cleansing'/'DRK', 'Pete Venters').
card_multiverse_id('cleansing'/'DRK', '1804').

card_in_set('coal golem', 'DRK').
card_original_type('coal golem'/'DRK', 'Artifact Creature').
card_original_text('coal golem'/'DRK', '{3}: Sacrifice Coal Golem to add {R}{R}{R} to your mana pool. This ability is played as an interrupt.').
card_first_print('coal golem', 'DRK').
card_image_name('coal golem'/'DRK', 'coal golem').
card_uid('coal golem'/'DRK', 'DRK:Coal Golem:coal golem').
card_rarity('coal golem'/'DRK', 'Uncommon').
card_artist('coal golem'/'DRK', 'Christopher Rush').
card_flavor_text('coal golem'/'DRK', '\"Three such creatures stood burning at the crest of the hill. Only seconds later, the Fireball struck our front line.\" —Lydia, Countess Brellis').
card_multiverse_id('coal golem'/'DRK', '1711').

card_in_set('curse artifact', 'DRK').
card_original_type('curse artifact'/'DRK', 'Enchant Artifact').
card_original_text('curse artifact'/'DRK', 'During his or her upkeep, controller of target artifact may choose to bury target artifact. If controller chooses not to bury target artifact, Curse Artifact does 2 damage to him or her.').
card_first_print('curse artifact', 'DRK').
card_image_name('curse artifact'/'DRK', 'curse artifact').
card_uid('curse artifact'/'DRK', 'DRK:Curse Artifact:curse artifact').
card_rarity('curse artifact'/'DRK', 'Uncommon').
card_artist('curse artifact'/'DRK', 'Mark Tedin').
card_flavor_text('curse artifact'/'DRK', 'Voska feared the artifact had come too easily.').
card_multiverse_id('curse artifact'/'DRK', '1732').

card_in_set('dance of many', 'DRK').
card_original_type('dance of many'/'DRK', 'Enchantment').
card_original_text('dance of many'/'DRK', 'When Dance of Many is brought into play, choose a target summon card in play. Then put a token creature in play and treat it as if you have just brought an exact copy of target summon card into play. If Dance of Many leaves play, remove token creature from game. If token creature leaves play, destroy Dance of Many. If you do not pay {U}{U} during your upkeep, Dance of Many is destroyed.').
card_first_print('dance of many', 'DRK').
card_image_name('dance of many'/'DRK', 'dance of many').
card_uid('dance of many'/'DRK', 'DRK:Dance of Many:dance of many').
card_rarity('dance of many'/'DRK', 'Rare').
card_artist('dance of many'/'DRK', 'Sandra Everingham').
card_multiverse_id('dance of many'/'DRK', '1748').

card_in_set('dark heart of the wood', 'DRK').
card_original_type('dark heart of the wood'/'DRK', 'Enchantment').
card_original_text('dark heart of the wood'/'DRK', 'You may sacrifice a forest to gain 3 life.\nCounts as both a black card and a green card.').
card_first_print('dark heart of the wood', 'DRK').
card_image_name('dark heart of the wood'/'DRK', 'dark heart of the wood').
card_uid('dark heart of the wood'/'DRK', 'DRK:Dark Heart of the Wood:dark heart of the wood').
card_rarity('dark heart of the wood'/'DRK', 'Common').
card_artist('dark heart of the wood'/'DRK', 'Christopher Rush').
card_flavor_text('dark heart of the wood'/'DRK', 'Even the Goblins shun this haunted place, where the tree limbs twist in agony and the ground seems to scuttle under your feet.').
card_multiverse_id('dark heart of the wood'/'DRK', '1820').

card_in_set('dark sphere', 'DRK').
card_original_type('dark sphere'/'DRK', 'Artifact').
card_original_text('dark sphere'/'DRK', '{T}: Sacrifice Dark Sphere to prevent half of the damage done to you by a single source, rounded down.').
card_first_print('dark sphere', 'DRK').
card_image_name('dark sphere'/'DRK', 'dark sphere').
card_uid('dark sphere'/'DRK', 'DRK:Dark Sphere:dark sphere').
card_rarity('dark sphere'/'DRK', 'Uncommon').
card_artist('dark sphere'/'DRK', 'Mark Tedin').
card_flavor_text('dark sphere'/'DRK', '\"I was struck senseless for a moment, but revived when the strange curiosity I carried fell to the ground, screaming like a dying animal.\" —Barl, Lord Ith').
card_multiverse_id('dark sphere'/'DRK', '1712').

card_in_set('deep water', 'DRK').
card_original_type('deep water'/'DRK', 'Enchantment').
card_original_text('deep water'/'DRK', '{U} All mana-producing lands you control provide {U} instead of their normal mana until end of turn.').
card_first_print('deep water', 'DRK').
card_image_name('deep water'/'DRK', 'deep water').
card_uid('deep water'/'DRK', 'DRK:Deep Water:deep water').
card_rarity('deep water'/'DRK', 'Common').
card_artist('deep water'/'DRK', 'Jeff A. Menges').
card_multiverse_id('deep water'/'DRK', '1749').

card_in_set('diabolic machine', 'DRK').
card_original_type('diabolic machine'/'DRK', 'Artifact Creature').
card_original_text('diabolic machine'/'DRK', '{3}: Regenerates').
card_first_print('diabolic machine', 'DRK').
card_image_name('diabolic machine'/'DRK', 'diabolic machine').
card_uid('diabolic machine'/'DRK', 'DRK:Diabolic Machine:diabolic machine').
card_rarity('diabolic machine'/'DRK', 'Uncommon').
card_artist('diabolic machine'/'DRK', 'Anson Maddocks').
card_flavor_text('diabolic machine'/'DRK', '\"The bolts of our ballistae smashed into the monstrous thing, but our hopes died in our chests as its gears continued turning.\" —Sevti Mukul, The Fall of Alsoor').
card_multiverse_id('diabolic machine'/'DRK', '1713').

card_in_set('drowned', 'DRK').
card_original_type('drowned'/'DRK', 'Summon — Zombies').
card_original_text('drowned'/'DRK', '{B} Regenerates').
card_first_print('drowned', 'DRK').
card_image_name('drowned'/'DRK', 'drowned').
card_uid('drowned'/'DRK', 'DRK:Drowned:drowned').
card_rarity('drowned'/'DRK', 'Common').
card_artist('drowned'/'DRK', 'Quinton Hoover').
card_flavor_text('drowned'/'DRK', 'We asked Captain Soll what became of the Serafina, but all he said was, \"Ships that go down shouldn\'t come back up.\"').
card_multiverse_id('drowned'/'DRK', '1750').

card_in_set('dust to dust', 'DRK').
card_original_type('dust to dust'/'DRK', 'Sorcery').
card_original_text('dust to dust'/'DRK', 'Removes two target artifacts from the game.').
card_first_print('dust to dust', 'DRK').
card_image_name('dust to dust'/'DRK', 'dust to dust').
card_uid('dust to dust'/'DRK', 'DRK:Dust to Dust:dust to dust').
card_rarity('dust to dust'/'DRK', 'Common').
card_artist('dust to dust'/'DRK', 'Drew Tucker').
card_flavor_text('dust to dust'/'DRK', 'Tervish never noticed that the amulet had vanished. It had disappeared not only from his possession, but from his memory as well.').
card_multiverse_id('dust to dust'/'DRK', '1805').

card_in_set('eater of the dead', 'DRK').
card_original_type('eater of the dead'/'DRK', 'Summon — Eater').
card_original_text('eater of the dead'/'DRK', '{0}: Take one creature from any graveyard and remove it from the game. Untap Eater of the Dead.').
card_first_print('eater of the dead', 'DRK').
card_image_name('eater of the dead'/'DRK', 'eater of the dead').
card_uid('eater of the dead'/'DRK', 'DRK:Eater of the Dead:eater of the dead').
card_rarity('eater of the dead'/'DRK', 'Uncommon').
card_artist('eater of the dead'/'DRK', 'Jesper Myrfors').
card_flavor_text('eater of the dead'/'DRK', 'Even the putrid muscles of the dead can provide strength to those loathsome enough to consume them.').
card_multiverse_id('eater of the dead'/'DRK', '1733').

card_in_set('electric eel', 'DRK').
card_original_type('electric eel'/'DRK', 'Summon — Eel').
card_original_text('electric eel'/'DRK', '{R}{R} +2/+0; Electric Eel does 1 damage to you. Electric Eel does 1 damage to you when it is brought into play.').
card_first_print('electric eel', 'DRK').
card_image_name('electric eel'/'DRK', 'electric eel').
card_uid('electric eel'/'DRK', 'DRK:Electric Eel:electric eel').
card_rarity('electric eel'/'DRK', 'Uncommon').
card_artist('electric eel'/'DRK', 'Anson Maddocks').
card_multiverse_id('electric eel'/'DRK', '1751').

card_in_set('elves of deep shadow', 'DRK').
card_original_type('elves of deep shadow'/'DRK', 'Summon — Elves').
card_original_text('elves of deep shadow'/'DRK', '{T}: Add {B} to your mana pool, and Elves of Deep Shadow do 1 damage to you. This ability is played as an interrupt.').
card_first_print('elves of deep shadow', 'DRK').
card_image_name('elves of deep shadow'/'DRK', 'elves of deep shadow').
card_uid('elves of deep shadow'/'DRK', 'DRK:Elves of Deep Shadow:elves of deep shadow').
card_rarity('elves of deep shadow'/'DRK', 'Uncommon').
card_artist('elves of deep shadow'/'DRK', 'Jesper Myrfors').
card_flavor_text('elves of deep shadow'/'DRK', '\"They are aberrations who have turned on everything we hold sacred. Let them be cast out.\" —Ailheen, Speaker of the Council').
card_multiverse_id('elves of deep shadow'/'DRK', '1766').

card_in_set('erosion', 'DRK').
card_original_type('erosion'/'DRK', 'Enchant Land').
card_original_text('erosion'/'DRK', 'Target land is destroyed unless its controller pays {1} or pays 1 life during his or her upkeep. Effects that prevent or redirect damage may not be used to counter this loss of life.').
card_first_print('erosion', 'DRK').
card_image_name('erosion'/'DRK', 'erosion').
card_uid('erosion'/'DRK', 'DRK:Erosion:erosion').
card_rarity('erosion'/'DRK', 'Common').
card_artist('erosion'/'DRK', 'Pete Venters').
card_multiverse_id('erosion'/'DRK', '1752').

card_in_set('eternal flame', 'DRK').
card_original_type('eternal flame'/'DRK', 'Sorcery').
card_original_text('eternal flame'/'DRK', 'Eternal Flame does an amount of damage to your opponent equal to the number of mountains you control, but it also does half that amount of damage to you, rounding up.').
card_first_print('eternal flame', 'DRK').
card_image_name('eternal flame'/'DRK', 'eternal flame').
card_uid('eternal flame'/'DRK', 'DRK:Eternal Flame:eternal flame').
card_rarity('eternal flame'/'DRK', 'Rare').
card_artist('eternal flame'/'DRK', 'Mark Poole').
card_multiverse_id('eternal flame'/'DRK', '1787').

card_in_set('exorcist', 'DRK').
card_original_type('exorcist'/'DRK', 'Summon — Exorcist').
card_original_text('exorcist'/'DRK', '{1}{W}, {T}: Target black creature is destroyed.').
card_first_print('exorcist', 'DRK').
card_image_name('exorcist'/'DRK', 'exorcist').
card_uid('exorcist'/'DRK', 'DRK:Exorcist:exorcist').
card_rarity('exorcist'/'DRK', 'Rare').
card_artist('exorcist'/'DRK', 'Drew Tucker').
card_flavor_text('exorcist'/'DRK', 'Though they often bore little greater charm than the demons they battled, exorcists were always welcome in Scarwood.').
card_multiverse_id('exorcist'/'DRK', '1806').

card_in_set('fasting', 'DRK').
card_original_type('fasting'/'DRK', 'Enchantment').
card_original_text('fasting'/'DRK', 'You may choose to skip your draw phase; if you do so, you gain 2 life. If you draw a card for any reason, Fasting is destroyed. During your upkeep, put a hunger counter on Fasting. When Fasting has five hunger counters on it, it is destroyed.').
card_first_print('fasting', 'DRK').
card_image_name('fasting'/'DRK', 'fasting').
card_uid('fasting'/'DRK', 'DRK:Fasting:fasting').
card_rarity('fasting'/'DRK', 'Uncommon').
card_artist('fasting'/'DRK', 'Douglas Shuler').
card_multiverse_id('fasting'/'DRK', '1807').

card_in_set('fellwar stone', 'DRK').
card_original_type('fellwar stone'/'DRK', 'Artifact').
card_original_text('fellwar stone'/'DRK', '{T}: Add 1 mana to your mana pool. This mana may be of any color that any of opponent\'s lands can produce. This ability is played as an interrupt.').
card_first_print('fellwar stone', 'DRK').
card_image_name('fellwar stone'/'DRK', 'fellwar stone').
card_uid('fellwar stone'/'DRK', 'DRK:Fellwar Stone:fellwar stone').
card_rarity('fellwar stone'/'DRK', 'Uncommon').
card_artist('fellwar stone'/'DRK', 'Quinton Hoover').
card_flavor_text('fellwar stone'/'DRK', '\"What do you have that I cannot obtain?\"\n—Mairsil, called the Pretender').
card_multiverse_id('fellwar stone'/'DRK', '1714').

card_in_set('festival', 'DRK').
card_original_type('festival'/'DRK', 'Instant').
card_original_text('festival'/'DRK', 'Opponent may not declare an attack this turn. Play during opponent\'s upkeep phase.').
card_first_print('festival', 'DRK').
card_image_name('festival'/'DRK', 'festival').
card_uid('festival'/'DRK', 'DRK:Festival:festival').
card_rarity('festival'/'DRK', 'Common').
card_artist('festival'/'DRK', 'Mark Poole').
card_flavor_text('festival'/'DRK', 'Only after the townsfolk had drawn us into their merry celebration did we discover that their holiday rituals held a deeper purpose.').
card_multiverse_id('festival'/'DRK', '1808').

card_in_set('fire and brimstone', 'DRK').
card_original_type('fire and brimstone'/'DRK', 'Instant').
card_original_text('fire and brimstone'/'DRK', 'Fire and Brimstone does 4 damage to target player and 4 damage to you. Can only be used during a turn in which target player has declared an attack.').
card_first_print('fire and brimstone', 'DRK').
card_image_name('fire and brimstone'/'DRK', 'fire and brimstone').
card_uid('fire and brimstone'/'DRK', 'DRK:Fire and Brimstone:fire and brimstone').
card_rarity('fire and brimstone'/'DRK', 'Uncommon').
card_artist('fire and brimstone'/'DRK', 'Jeff A. Menges').
card_multiverse_id('fire and brimstone'/'DRK', '1809').

card_in_set('fire drake', 'DRK').
card_original_type('fire drake'/'DRK', 'Summon — Drake').
card_original_text('fire drake'/'DRK', 'Flying\n{R} +1/+0 until end of turn. No more than {R} may be spent in this way each turn.').
card_first_print('fire drake', 'DRK').
card_image_name('fire drake'/'DRK', 'fire drake').
card_uid('fire drake'/'DRK', 'DRK:Fire Drake:fire drake').
card_rarity('fire drake'/'DRK', 'Uncommon').
card_artist('fire drake'/'DRK', 'Christopher Rush').
card_multiverse_id('fire drake'/'DRK', '1788').

card_in_set('fissure', 'DRK').
card_original_type('fissure'/'DRK', 'Instant').
card_original_text('fissure'/'DRK', 'Target land or creature is buried.').
card_first_print('fissure', 'DRK').
card_image_name('fissure'/'DRK', 'fissure').
card_uid('fissure'/'DRK', 'DRK:Fissure:fissure').
card_rarity('fissure'/'DRK', 'Common').
card_artist('fissure'/'DRK', 'Douglas Shuler').
card_flavor_text('fissure'/'DRK', '\"Must not all things at the last be swallowed up in death?\" —Plato').
card_multiverse_id('fissure'/'DRK', '1789').

card_in_set('flood', 'DRK').
card_original_type('flood'/'DRK', 'Enchantment').
card_original_text('flood'/'DRK', '{U}{U} Target non-flying creature becomes tapped.').
card_first_print('flood', 'DRK').
card_image_name('flood'/'DRK', 'flood').
card_uid('flood'/'DRK', 'DRK:Flood:flood').
card_rarity('flood'/'DRK', 'Uncommon').
card_artist('flood'/'DRK', 'Dennis Detwiller').
card_flavor_text('flood'/'DRK', '\"A dash of cool water does wonders to clear a cluttered battlefield.\"\n—Vibekke Ragnild, Witches and War').
card_multiverse_id('flood'/'DRK', '1753').

card_in_set('fountain of youth', 'DRK').
card_original_type('fountain of youth'/'DRK', 'Artifact').
card_original_text('fountain of youth'/'DRK', '{2}, {T}: Gain 1 life.').
card_first_print('fountain of youth', 'DRK').
card_image_name('fountain of youth'/'DRK', 'fountain of youth').
card_uid('fountain of youth'/'DRK', 'DRK:Fountain of Youth:fountain of youth').
card_rarity('fountain of youth'/'DRK', 'Uncommon').
card_artist('fountain of youth'/'DRK', 'Daniel Gelon').
card_flavor_text('fountain of youth'/'DRK', 'The Fountain had stood in the town square for centuries, but only the pigeons knew its secret.').
card_multiverse_id('fountain of youth'/'DRK', '1715').

card_in_set('frankenstein\'s monster', 'DRK').
card_original_type('frankenstein\'s monster'/'DRK', 'Summon — Monster').
card_original_text('frankenstein\'s monster'/'DRK', 'When Frankenstein\'s Monster is brought into play, if you do not take X creatures from your graveyard and remove them from the game, Frankenstein\'s Monster is countered. For each creature removed from your graveyard in this way, you may choose to give Frankenstein\'s Monster a permanent +2/+0, +1/+1, or +0/+2.').
card_first_print('frankenstein\'s monster', 'DRK').
card_image_name('frankenstein\'s monster'/'DRK', 'frankenstein\'s monster').
card_uid('frankenstein\'s monster'/'DRK', 'DRK:Frankenstein\'s Monster:frankenstein\'s monster').
card_rarity('frankenstein\'s monster'/'DRK', 'Rare').
card_artist('frankenstein\'s monster'/'DRK', 'Anson Maddocks').
card_multiverse_id('frankenstein\'s monster'/'DRK', '1734').

card_in_set('gaea\'s touch', 'DRK').
card_original_type('gaea\'s touch'/'DRK', 'Enchantment').
card_original_text('gaea\'s touch'/'DRK', 'You may put one additional land in play during each of your turns, but that land must be a basic forest. You may sacrifice Gaea\'s Touch to add {G}{G} to your mana pool. This ability is played as an interrupt.').
card_first_print('gaea\'s touch', 'DRK').
card_image_name('gaea\'s touch'/'DRK', 'gaea\'s touch').
card_uid('gaea\'s touch'/'DRK', 'DRK:Gaea\'s Touch:gaea\'s touch').
card_rarity('gaea\'s touch'/'DRK', 'Common').
card_artist('gaea\'s touch'/'DRK', 'Mark Poole').
card_multiverse_id('gaea\'s touch'/'DRK', '1767').

card_in_set('ghost ship', 'DRK').
card_original_type('ghost ship'/'DRK', 'Summon — Ship').
card_original_text('ghost ship'/'DRK', 'Flying, {U}{U}{U} Regenerates').
card_first_print('ghost ship', 'DRK').
card_image_name('ghost ship'/'DRK', 'ghost ship').
card_uid('ghost ship'/'DRK', 'DRK:Ghost Ship:ghost ship').
card_rarity('ghost ship'/'DRK', 'Common').
card_artist('ghost ship'/'DRK', 'Tom Wänerstrand').
card_flavor_text('ghost ship'/'DRK', '\"That phantom prow split the storm as lightning cast its long shadow on the battlefield below.\" —Mireille Gaetane, The Valeriad').
card_multiverse_id('ghost ship'/'DRK', '1754').

card_in_set('giant shark', 'DRK').
card_original_type('giant shark'/'DRK', 'Summon — Shark').
card_original_text('giant shark'/'DRK', 'If Giant Shark blocks or is blocked by a creature that has taken damage this turn, Giant Shark gains +2/+0 and trample until end of turn. Giant Shark cannot attack unless opponent controls at least one island. Giant Shark is buried immediately if at any time controller controls no islands.').
card_first_print('giant shark', 'DRK').
card_image_name('giant shark'/'DRK', 'giant shark').
card_uid('giant shark'/'DRK', 'DRK:Giant Shark:giant shark').
card_rarity('giant shark'/'DRK', 'Common').
card_artist('giant shark'/'DRK', 'Tom Wänerstrand').
card_multiverse_id('giant shark'/'DRK', '1755').

card_in_set('goblin caves', 'DRK').
card_original_type('goblin caves'/'DRK', 'Enchant Land').
card_original_text('goblin caves'/'DRK', 'If target land is a basic mountain, all Goblins gain +0/+2.').
card_first_print('goblin caves', 'DRK').
card_image_name('goblin caves'/'DRK', 'goblin caves').
card_uid('goblin caves'/'DRK', 'DRK:Goblin Caves:goblin caves').
card_rarity('goblin caves'/'DRK', 'Common').
card_artist('goblin caves'/'DRK', 'Drew Tucker').
card_flavor_text('goblin caves'/'DRK', 'The stench of countless generations of unspeakable activities was enough to loosen both our footing and our stomachs.').
card_multiverse_id('goblin caves'/'DRK', '1790').

card_in_set('goblin digging team', 'DRK').
card_original_type('goblin digging team'/'DRK', 'Summon — Goblins').
card_original_text('goblin digging team'/'DRK', '{T}: Sacrifice Goblin Digging Team to destroy target wall.').
card_first_print('goblin digging team', 'DRK').
card_image_name('goblin digging team'/'DRK', 'goblin digging team').
card_uid('goblin digging team'/'DRK', 'DRK:Goblin Digging Team:goblin digging team').
card_rarity('goblin digging team'/'DRK', 'Common').
card_artist('goblin digging team'/'DRK', 'Ron Spencer').
card_flavor_text('goblin digging team'/'DRK', '\"From down here we can make the whole wall collapse!\" \"Uh, yeah, boss, but how do we get out?\"').
card_multiverse_id('goblin digging team'/'DRK', '1791').

card_in_set('goblin hero', 'DRK').
card_original_type('goblin hero'/'DRK', 'Summon — Goblin').
card_original_text('goblin hero'/'DRK', '').
card_first_print('goblin hero', 'DRK').
card_image_name('goblin hero'/'DRK', 'goblin hero').
card_uid('goblin hero'/'DRK', 'DRK:Goblin Hero:goblin hero').
card_rarity('goblin hero'/'DRK', 'Common').
card_artist('goblin hero'/'DRK', 'Mark Tedin').
card_flavor_text('goblin hero'/'DRK', 'They attacked in an orgy of rage and madness, but only one seemed as focused on killing us as on the sheer joy of battle.').
card_multiverse_id('goblin hero'/'DRK', '1792').

card_in_set('goblin rock sled', 'DRK').
card_original_type('goblin rock sled'/'DRK', 'Summon — Rock Sled').
card_original_text('goblin rock sled'/'DRK', 'Trample\nRock Sled may not attack unless opponent controls at least one mountain. Rock Sled does not untap as normal during your untap phase if it attacked during your last turn.').
card_first_print('goblin rock sled', 'DRK').
card_image_name('goblin rock sled'/'DRK', 'goblin rock sled').
card_uid('goblin rock sled'/'DRK', 'DRK:Goblin Rock Sled:goblin rock sled').
card_rarity('goblin rock sled'/'DRK', 'Common').
card_artist('goblin rock sled'/'DRK', 'Dennis Detwiller').
card_multiverse_id('goblin rock sled'/'DRK', '1793').

card_in_set('goblin shrine', 'DRK').
card_original_type('goblin shrine'/'DRK', 'Enchant Land').
card_original_text('goblin shrine'/'DRK', 'If target land is a basic mountain, all Goblins gain +1/0. Goblin Shrine does 1 damage to all Goblins if it leaves play.').
card_first_print('goblin shrine', 'DRK').
card_image_name('goblin shrine'/'DRK', 'goblin shrine').
card_uid('goblin shrine'/'DRK', 'DRK:Goblin Shrine:goblin shrine').
card_rarity('goblin shrine'/'DRK', 'Common').
card_artist('goblin shrine'/'DRK', 'Ron Spencer').
card_flavor_text('goblin shrine'/'DRK', '\"I knew it weren\'t no ordinary pile of—you know.\" —Norin the Wary').
card_multiverse_id('goblin shrine'/'DRK', '1794').

card_in_set('goblin wizard', 'DRK').
card_original_type('goblin wizard'/'DRK', 'Summon — Goblin').
card_original_text('goblin wizard'/'DRK', '{T}: Take a Goblin from your hand and put it directly into play. Treat this goblin as if it were just summoned.\n{R} Target Goblin gains protection from white until end of turn.').
card_first_print('goblin wizard', 'DRK').
card_image_name('goblin wizard'/'DRK', 'goblin wizard').
card_uid('goblin wizard'/'DRK', 'DRK:Goblin Wizard:goblin wizard').
card_rarity('goblin wizard'/'DRK', 'Rare').
card_artist('goblin wizard'/'DRK', 'Daniel Gelon').
card_multiverse_id('goblin wizard'/'DRK', '1795').

card_in_set('goblins of the flarg', 'DRK').
card_original_type('goblins of the flarg'/'DRK', 'Summon — Goblins').
card_original_text('goblins of the flarg'/'DRK', 'Mountainwalk\nGoblins of the Flarg are buried if controller controls any Dwarves.').
card_first_print('goblins of the flarg', 'DRK').
card_image_name('goblins of the flarg'/'DRK', 'goblins of the flarg').
card_uid('goblins of the flarg'/'DRK', 'DRK:Goblins of the Flarg:goblins of the flarg').
card_rarity('goblins of the flarg'/'DRK', 'Common').
card_artist('goblins of the flarg'/'DRK', 'Tom Wänerstrand').
card_multiverse_id('goblins of the flarg'/'DRK', '1796').

card_in_set('grave robbers', 'DRK').
card_original_type('grave robbers'/'DRK', 'Summon — Robbers').
card_original_text('grave robbers'/'DRK', '{B}, {T}: Take one artifact from any graveyard and remove it from the game. Gain 2 life.').
card_first_print('grave robbers', 'DRK').
card_image_name('grave robbers'/'DRK', 'grave robbers').
card_uid('grave robbers'/'DRK', 'DRK:Grave Robbers:grave robbers').
card_rarity('grave robbers'/'DRK', 'Rare').
card_artist('grave robbers'/'DRK', 'Quinton Hoover').
card_flavor_text('grave robbers'/'DRK', '\"If you don\'t have your health, you don\'t have anything.\" —Proverb').
card_multiverse_id('grave robbers'/'DRK', '1735').

card_in_set('hidden path', 'DRK').
card_original_type('hidden path'/'DRK', 'Enchantment').
card_original_text('hidden path'/'DRK', 'All green creatures gain forestwalk.').
card_first_print('hidden path', 'DRK').
card_image_name('hidden path'/'DRK', 'hidden path').
card_uid('hidden path'/'DRK', 'DRK:Hidden Path:hidden path').
card_rarity('hidden path'/'DRK', 'Rare').
card_artist('hidden path'/'DRK', 'Rob Alexander').
card_flavor_text('hidden path'/'DRK', '\"Where moments before we were lost beyond hope, the strange, floating lights showed us the way and restored our morale.\" —Vervamon the Elder').
card_multiverse_id('hidden path'/'DRK', '1768').

card_in_set('holy light', 'DRK').
card_original_type('holy light'/'DRK', 'Instant').
card_original_text('holy light'/'DRK', 'All non-white creatures get -1/-1 until end of turn.').
card_first_print('holy light', 'DRK').
card_image_name('holy light'/'DRK', 'holy light').
card_uid('holy light'/'DRK', 'DRK:Holy Light:holy light').
card_rarity('holy light'/'DRK', 'Common').
card_artist('holy light'/'DRK', 'Drew Tucker').
card_flavor_text('holy light'/'DRK', '\"Bathed in hallowed light, the infidels looked upon the impurities of their souls and despaired.\" —The Book of Tal').
card_multiverse_id('holy light'/'DRK', '1810').

card_in_set('inferno', 'DRK').
card_original_type('inferno'/'DRK', 'Instant').
card_original_text('inferno'/'DRK', 'Inferno does 6 damage to all players and all creatures.').
card_first_print('inferno', 'DRK').
card_image_name('inferno'/'DRK', 'inferno').
card_uid('inferno'/'DRK', 'DRK:Inferno:inferno').
card_rarity('inferno'/'DRK', 'Rare').
card_artist('inferno'/'DRK', 'Randy Asplund-Faith').
card_flavor_text('inferno'/'DRK', '\"Any scrap of compassion that still existed in my soul was permanently snuffed out when they cast me out into the flames.\" —Mairsil, called the Pretender').
card_multiverse_id('inferno'/'DRK', '1797').

card_in_set('inquisition', 'DRK').
card_original_type('inquisition'/'DRK', 'Sorcery').
card_original_text('inquisition'/'DRK', 'Look at target player\'s hand. Inquisition does 1 damage to target player for each white card in his or her hand.').
card_first_print('inquisition', 'DRK').
card_image_name('inquisition'/'DRK', 'inquisition').
card_uid('inquisition'/'DRK', 'DRK:Inquisition:inquisition').
card_rarity('inquisition'/'DRK', 'Common').
card_artist('inquisition'/'DRK', 'Anson Maddocks').
card_flavor_text('inquisition'/'DRK', 'Many of those entrusted to Primata Delphine\'s care tended to express themselves with screams.').
card_multiverse_id('inquisition'/'DRK', '1736').

card_in_set('knights of thorn', 'DRK').
card_original_type('knights of thorn'/'DRK', 'Summon — Knights').
card_original_text('knights of thorn'/'DRK', 'Protection from red, banding').
card_first_print('knights of thorn', 'DRK').
card_image_name('knights of thorn'/'DRK', 'knights of thorn').
card_uid('knights of thorn'/'DRK', 'DRK:Knights of Thorn:knights of thorn').
card_rarity('knights of thorn'/'DRK', 'Rare').
card_artist('knights of thorn'/'DRK', 'Christopher Rush').
card_flavor_text('knights of thorn'/'DRK', '\"With a great cry, the Goblin host broke and ran as the first wave of Knights penetrated its ranks.\" —Tivadar of Thorn, History of the Goblin Wars').
card_multiverse_id('knights of thorn'/'DRK', '1811').

card_in_set('land leeches', 'DRK').
card_original_type('land leeches'/'DRK', 'Summon — Leeches').
card_original_text('land leeches'/'DRK', 'First strike').
card_first_print('land leeches', 'DRK').
card_image_name('land leeches'/'DRK', 'land leeches').
card_uid('land leeches'/'DRK', 'DRK:Land Leeches:land leeches').
card_rarity('land leeches'/'DRK', 'Common').
card_artist('land leeches'/'DRK', 'Quinton Hoover').
card_flavor_text('land leeches'/'DRK', '\"The standard cure for leeches requires the application of burning embers. Alternative methods must be devised should an ember of sufficient size prove more harmful than the leech.\" —Vervamon the Elder').
card_multiverse_id('land leeches'/'DRK', '1769').

card_in_set('leviathan', 'DRK').
card_original_type('leviathan'/'DRK', 'Summon — Leviathan').
card_original_text('leviathan'/'DRK', 'Trample\nLeviathan comes into play tapped, and does not untap as normal during your untap phase.\nSacrifice two islands during your upkeep phase to untap Leviathan.\nLeviathan may not attack unless you sacrifice two islands during your attack.').
card_first_print('leviathan', 'DRK').
card_image_name('leviathan'/'DRK', 'leviathan').
card_uid('leviathan'/'DRK', 'DRK:Leviathan:leviathan').
card_rarity('leviathan'/'DRK', 'Rare').
card_artist('leviathan'/'DRK', 'Mark Tedin').
card_multiverse_id('leviathan'/'DRK', '1756').

card_in_set('living armor', 'DRK').
card_original_type('living armor'/'DRK', 'Artifact').
card_original_text('living armor'/'DRK', '{T}: Sacrifice Living Armor to put a +0/+X counter on target creature, where X is target creature\'s casting cost.').
card_first_print('living armor', 'DRK').
card_image_name('living armor'/'DRK', 'living armor').
card_uid('living armor'/'DRK', 'DRK:Living Armor:living armor').
card_rarity('living armor'/'DRK', 'Uncommon').
card_artist('living armor'/'DRK', 'Anson Maddocks').
card_flavor_text('living armor'/'DRK', 'Though it affords excellent protection, few don this armor. The process is uncomfortable and not easily reversed.').
card_multiverse_id('living armor'/'DRK', '1716').

card_in_set('lurker', 'DRK').
card_original_type('lurker'/'DRK', 'Summon — Lurker').
card_original_text('lurker'/'DRK', 'Lurker may not be the target of any spell unless Lurker was declared as an attacker or blocker this turn.').
card_first_print('lurker', 'DRK').
card_image_name('lurker'/'DRK', 'lurker').
card_uid('lurker'/'DRK', 'DRK:Lurker:lurker').
card_rarity('lurker'/'DRK', 'Rare').
card_artist('lurker'/'DRK', 'Anson Maddocks').
card_flavor_text('lurker'/'DRK', '\"Each night we felt it watching us from the darkness beyond our fire. We only had one pack horse left.\"\n—Maeveen O\'Donagh, Memoirs of a Soldier').
card_multiverse_id('lurker'/'DRK', '1770').

card_in_set('mana clash', 'DRK').
card_original_type('mana clash'/'DRK', 'Sorcery').
card_original_text('mana clash'/'DRK', 'You and target player each flip a coin. Mana Clash does 1 damage to any player whose coin comes up tails. Repeat this process until both players\' coins come up heads at the same time.').
card_first_print('mana clash', 'DRK').
card_image_name('mana clash'/'DRK', 'mana clash').
card_uid('mana clash'/'DRK', 'DRK:Mana Clash:mana clash').
card_rarity('mana clash'/'DRK', 'Rare').
card_artist('mana clash'/'DRK', 'Mark Tedin').
card_multiverse_id('mana clash'/'DRK', '1798').

card_in_set('mana vortex', 'DRK').
card_original_type('mana vortex'/'DRK', 'Enchantment').
card_original_text('mana vortex'/'DRK', 'Each player who controls land sacrifices one land during his or her upkeep. If at any time there are no lands in play, Mana Vortex is destroyed. If you do not sacrifice a land when Mana Vortex is cast, Mana Vortex is countered.').
card_first_print('mana vortex', 'DRK').
card_image_name('mana vortex'/'DRK', 'mana vortex').
card_uid('mana vortex'/'DRK', 'DRK:Mana Vortex:mana vortex').
card_rarity('mana vortex'/'DRK', 'Rare').
card_artist('mana vortex'/'DRK', 'Douglas Shuler').
card_multiverse_id('mana vortex'/'DRK', '1757').

card_in_set('marsh gas', 'DRK').
card_original_type('marsh gas'/'DRK', 'Instant').
card_original_text('marsh gas'/'DRK', 'All creatures get -2/-0 until end of turn.').
card_first_print('marsh gas', 'DRK').
card_image_name('marsh gas'/'DRK', 'marsh gas').
card_uid('marsh gas'/'DRK', 'DRK:Marsh Gas:marsh gas').
card_rarity('marsh gas'/'DRK', 'Common').
card_artist('marsh gas'/'DRK', 'Douglas Shuler').
card_flavor_text('marsh gas'/'DRK', '\"Comes right outta th\' ground. If ya can smell it, it\'s too late.\" —Keevy Bogsbury').
card_multiverse_id('marsh gas'/'DRK', '1737').

card_in_set('marsh goblins', 'DRK').
card_original_type('marsh goblins'/'DRK', 'Summon — Goblins').
card_original_text('marsh goblins'/'DRK', 'Swampwalk\nCounts as both a black card and a red card.').
card_first_print('marsh goblins', 'DRK').
card_image_name('marsh goblins'/'DRK', 'marsh goblins').
card_uid('marsh goblins'/'DRK', 'DRK:Marsh Goblins:marsh goblins').
card_rarity('marsh goblins'/'DRK', 'Common').
card_artist('marsh goblins'/'DRK', 'Quinton Hoover').
card_flavor_text('marsh goblins'/'DRK', 'Even the other Goblin races shun the Marsh Goblins, thanks to certain unwholesome customs they practice.').
card_multiverse_id('marsh goblins'/'DRK', '1821').

card_in_set('marsh viper', 'DRK').
card_original_type('marsh viper'/'DRK', 'Summon — Viper').
card_original_text('marsh viper'/'DRK', 'If Marsh Viper damages opponent, opponent gets two poison counters. If opponent ever has ten or more poison counters, opponent loses game.').
card_first_print('marsh viper', 'DRK').
card_image_name('marsh viper'/'DRK', 'marsh viper').
card_uid('marsh viper'/'DRK', 'DRK:Marsh Viper:marsh viper').
card_rarity('marsh viper'/'DRK', 'Common').
card_artist('marsh viper'/'DRK', 'Ron Spencer').
card_flavor_text('marsh viper'/'DRK', '\"All we had left were their black and bloated bodies.\" —Maeveen O\'Donagh, Memoirs of a Soldier').
card_multiverse_id('marsh viper'/'DRK', '1771').

card_in_set('martyr\'s cry', 'DRK').
card_original_type('martyr\'s cry'/'DRK', 'Sorcery').
card_original_text('martyr\'s cry'/'DRK', 'All white creatures are removed from the game. Players must draw one card for each white creature they control that is lost in this manner.').
card_first_print('martyr\'s cry', 'DRK').
card_image_name('martyr\'s cry'/'DRK', 'martyr\'s cry').
card_uid('martyr\'s cry'/'DRK', 'DRK:Martyr\'s Cry:martyr\'s cry').
card_rarity('martyr\'s cry'/'DRK', 'Rare').
card_artist('martyr\'s cry'/'DRK', 'Jeff A. Menges').
card_flavor_text('martyr\'s cry'/'DRK', '\"It is only fitting that one such as I should die in pursuit of knowledge.\" —Vervamon the Elder').
card_multiverse_id('martyr\'s cry'/'DRK', '1812').

card_in_set('maze of ith', 'DRK').
card_original_type('maze of ith'/'DRK', 'Land').
card_original_text('maze of ith'/'DRK', '{T}: Target attacking creature becomes untapped. This creature neither deals nor receives damage as a result of combat.').
card_first_print('maze of ith', 'DRK').
card_image_name('maze of ith'/'DRK', 'maze of ith').
card_uid('maze of ith'/'DRK', 'DRK:Maze of Ith:maze of ith').
card_rarity('maze of ith'/'DRK', 'Uncommon').
card_artist('maze of ith'/'DRK', 'Anson Maddocks').
card_multiverse_id('maze of ith'/'DRK', '1824').

card_in_set('merfolk assassin', 'DRK').
card_original_type('merfolk assassin'/'DRK', 'Summon — Merfolk').
card_original_text('merfolk assassin'/'DRK', '{T}: Destroy target creature that has islandwalk.').
card_first_print('merfolk assassin', 'DRK').
card_image_name('merfolk assassin'/'DRK', 'merfolk assassin').
card_uid('merfolk assassin'/'DRK', 'DRK:Merfolk Assassin:merfolk assassin').
card_rarity('merfolk assassin'/'DRK', 'Uncommon').
card_artist('merfolk assassin'/'DRK', 'Dennis Detwiller').
card_multiverse_id('merfolk assassin'/'DRK', '1758').

card_in_set('mind bomb', 'DRK').
card_original_type('mind bomb'/'DRK', 'Sorcery').
card_original_text('mind bomb'/'DRK', 'Mind Bomb does 3 damage to each player. All players may discard up to three cards of their choice from their hands. Each card a player discards in this manner prevents 1 damage to that player from Mind Bomb.').
card_first_print('mind bomb', 'DRK').
card_image_name('mind bomb'/'DRK', 'mind bomb').
card_uid('mind bomb'/'DRK', 'DRK:Mind Bomb:mind bomb').
card_rarity('mind bomb'/'DRK', 'Rare').
card_artist('mind bomb'/'DRK', 'Mark Tedin').
card_multiverse_id('mind bomb'/'DRK', '1759').

card_in_set('miracle worker', 'DRK').
card_original_type('miracle worker'/'DRK', 'Summon — Miracle Worker').
card_original_text('miracle worker'/'DRK', '{T}: Destroy target enchantment card on a creature you control.').
card_first_print('miracle worker', 'DRK').
card_image_name('miracle worker'/'DRK', 'miracle worker').
card_uid('miracle worker'/'DRK', 'DRK:Miracle Worker:miracle worker').
card_rarity('miracle worker'/'DRK', 'Common').
card_artist('miracle worker'/'DRK', 'Ron Spencer').
card_flavor_text('miracle worker'/'DRK', '\"Those blessed hands could bring surcease to even the most tainted soul.\" —Sister Betje, Miracles of the Saints').
card_multiverse_id('miracle worker'/'DRK', '1813').

card_in_set('morale', 'DRK').
card_original_type('morale'/'DRK', 'Instant').
card_original_text('morale'/'DRK', 'All attacking creatures gain +1/+1 until end of turn.').
card_first_print('morale', 'DRK').
card_image_name('morale'/'DRK', 'morale').
card_uid('morale'/'DRK', 'DRK:Morale:morale').
card_rarity('morale'/'DRK', 'Common').
card_artist('morale'/'DRK', 'Mark Poole').
card_flavor_text('morale'/'DRK', '\"After Lacjsi\'s speech, the Knights grew determined to crush their ancient enemies clan by clan.\" —Tivadar of Thorn, History of the Goblin Wars').
card_multiverse_id('morale'/'DRK', '1814').

card_in_set('murk dwellers', 'DRK').
card_original_type('murk dwellers'/'DRK', 'Summon — Murk Dwellers').
card_original_text('murk dwellers'/'DRK', 'When attacking, Murk Dwellers gain +2/+0 if not blocked.').
card_first_print('murk dwellers', 'DRK').
card_image_name('murk dwellers'/'DRK', 'murk dwellers').
card_uid('murk dwellers'/'DRK', 'DRK:Murk Dwellers:murk dwellers').
card_rarity('murk dwellers'/'DRK', 'Common').
card_artist('murk dwellers'/'DRK', 'Drew Tucker').
card_flavor_text('murk dwellers'/'DRK', 'When Raganorn unsealed the catacombs, he found more than the dead and their treasures.').
card_multiverse_id('murk dwellers'/'DRK', '1738').

card_in_set('nameless race', 'DRK').
card_original_type('nameless race'/'DRK', 'Summon — Nameless Race').
card_original_text('nameless race'/'DRK', 'Trample\nPay * life when bringing Nameless Race into play. Effects that prevent or redirect damage may not be used to counter this loss of life. When Nameless Race is brought into play, * may not be greater than the total number of white cards all opponents have in play and in their graveyards.').
card_first_print('nameless race', 'DRK').
card_image_name('nameless race'/'DRK', 'nameless race').
card_uid('nameless race'/'DRK', 'DRK:Nameless Race:nameless race').
card_rarity('nameless race'/'DRK', 'Rare').
card_artist('nameless race'/'DRK', 'Quinton Hoover').
card_multiverse_id('nameless race'/'DRK', '1739').

card_in_set('necropolis', 'DRK').
card_original_type('necropolis'/'DRK', 'Artifact Creature').
card_original_text('necropolis'/'DRK', 'Counts as a wall.\n{0}: Take a creature in your graveyard and remove it from the game. Put X +0/+1 counters on Necropolis, where X is the removed creature\'s casting cost.').
card_first_print('necropolis', 'DRK').
card_image_name('necropolis'/'DRK', 'necropolis').
card_uid('necropolis'/'DRK', 'DRK:Necropolis:necropolis').
card_rarity('necropolis'/'DRK', 'Uncommon').
card_artist('necropolis'/'DRK', 'NéNé Thomas').
card_multiverse_id('necropolis'/'DRK', '1717').

card_in_set('niall silvain', 'DRK').
card_original_type('niall silvain'/'DRK', 'Summon — Niall Silvain').
card_original_text('niall silvain'/'DRK', '{G}{G}{G}{G}, {T}: Target creature is regenerated.').
card_first_print('niall silvain', 'DRK').
card_image_name('niall silvain'/'DRK', 'niall silvain').
card_uid('niall silvain'/'DRK', 'DRK:Niall Silvain:niall silvain').
card_rarity('niall silvain'/'DRK', 'Rare').
card_artist('niall silvain'/'DRK', 'Christopher Rush').
card_flavor_text('niall silvain'/'DRK', 'This is his domain, and while you remain here you must value all life as you value your own.').
card_multiverse_id('niall silvain'/'DRK', '1772').

card_in_set('orc general', 'DRK').
card_original_type('orc general'/'DRK', 'Summon — General').
card_original_text('orc general'/'DRK', '{T}: Sacrifice one Orc or Goblin to give all Orcs +1/+1 until end of turn.').
card_first_print('orc general', 'DRK').
card_image_name('orc general'/'DRK', 'orc general').
card_uid('orc general'/'DRK', 'DRK:Orc General:orc general').
card_rarity('orc general'/'DRK', 'Uncommon').
card_artist('orc general'/'DRK', 'Jesper Myrfors').
card_flavor_text('orc general'/'DRK', '\"Your army must fear you more than the enemy. Only then will you triumph.\" —Malga Phlegmtooth').
card_multiverse_id('orc general'/'DRK', '1799').

card_in_set('people of the woods', 'DRK').
card_original_type('people of the woods'/'DRK', 'Summon — People of the Woods').
card_original_text('people of the woods'/'DRK', 'The * below represents the number of forests controlled by People of the Woods\' controller.').
card_first_print('people of the woods', 'DRK').
card_image_name('people of the woods'/'DRK', 'people of the woods').
card_uid('people of the woods'/'DRK', 'DRK:People of the Woods:people of the woods').
card_rarity('people of the woods'/'DRK', 'Uncommon').
card_artist('people of the woods'/'DRK', 'Drew Tucker').
card_flavor_text('people of the woods'/'DRK', '\"Their rain of arrows left only myself alive, cowering within a tree hollow. They did not even come out to loot the bodies.\" —Vervamon the Elder').
card_multiverse_id('people of the woods'/'DRK', '1773').

card_in_set('pikemen', 'DRK').
card_original_type('pikemen'/'DRK', 'Summon — Pikemen').
card_original_text('pikemen'/'DRK', 'Banding, first strike').
card_first_print('pikemen', 'DRK').
card_image_name('pikemen'/'DRK', 'pikemen').
card_uid('pikemen'/'DRK', 'DRK:Pikemen:pikemen').
card_rarity('pikemen'/'DRK', 'Common').
card_artist('pikemen'/'DRK', 'Dennis Detwiller').
card_flavor_text('pikemen'/'DRK', '\"As the cavalry bore down, we faced them with swords drawn and pikes hidden in the grass at our feet. ‘Don\'t lift your pikes \'til I give the word,\' I said.\" —Maeveen O\'Donagh, Memoirs of a Soldier').
card_multiverse_id('pikemen'/'DRK', '1815').

card_in_set('preacher', 'DRK').
card_original_type('preacher'/'DRK', 'Summon — Preacher').
card_original_text('preacher'/'DRK', '{T}: Gain control of one of opponent\'s creatures. Opponent chooses which target creature you control. If Preacher becomes untapped, you lose control of this creature; you may choose not to untap Preacher as normal during your untap phase. You also lose control of the creature if Preacher leaves play or at end of game.').
card_first_print('preacher', 'DRK').
card_image_name('preacher'/'DRK', 'preacher').
card_uid('preacher'/'DRK', 'DRK:Preacher:preacher').
card_rarity('preacher'/'DRK', 'Rare').
card_artist('preacher'/'DRK', 'Quinton Hoover').
card_multiverse_id('preacher'/'DRK', '1816').

card_in_set('psychic allergy', 'DRK').
card_original_type('psychic allergy'/'DRK', 'Enchantment').
card_original_text('psychic allergy'/'DRK', 'Choose a color when casting Psychic Allergy. During opponent\'s upkeep, Psychic Allergy does 1 damage to opponent for each card of this color that he or she controls. Sacrifice two islands during your upkeep or Psychic Allergy is destroyed.').
card_first_print('psychic allergy', 'DRK').
card_image_name('psychic allergy'/'DRK', 'psychic allergy').
card_uid('psychic allergy'/'DRK', 'DRK:Psychic Allergy:psychic allergy').
card_rarity('psychic allergy'/'DRK', 'Rare').
card_artist('psychic allergy'/'DRK', 'Mark Tedin').
card_multiverse_id('psychic allergy'/'DRK', '1760').

card_in_set('rag man', 'DRK').
card_original_type('rag man'/'DRK', 'Summon — Rag Man').
card_original_text('rag man'/'DRK', '{B}{B}{B}, {T}: Look at opponent\'s hand.\nIf opponent has any creature cards in hand, he or she discards one of them at random. This ability can only be used during controller\'s turn.').
card_first_print('rag man', 'DRK').
card_image_name('rag man'/'DRK', 'rag man').
card_uid('rag man'/'DRK', 'DRK:Rag Man:rag man').
card_rarity('rag man'/'DRK', 'Rare').
card_artist('rag man'/'DRK', 'Daniel Gelon').
card_flavor_text('rag man'/'DRK', '\"Aw, he\'s just a silly, dirty little man. What\'s to be afraid of?\"').
card_multiverse_id('rag man'/'DRK', '1740').

card_in_set('reflecting mirror', 'DRK').
card_original_type('reflecting mirror'/'DRK', 'Artifact').
card_original_text('reflecting mirror'/'DRK', '{X}, {T}: Target spell, which targets you, targets the player of your choice instead. X is twice the casting cost of target spell. This ability is played as an interrupt.').
card_first_print('reflecting mirror', 'DRK').
card_image_name('reflecting mirror'/'DRK', 'reflecting mirror').
card_uid('reflecting mirror'/'DRK', 'DRK:Reflecting Mirror:reflecting mirror').
card_rarity('reflecting mirror'/'DRK', 'Uncommon').
card_artist('reflecting mirror'/'DRK', 'Mark Poole').
card_multiverse_id('reflecting mirror'/'DRK', '1718').

card_in_set('riptide', 'DRK').
card_original_type('riptide'/'DRK', 'Instant').
card_original_text('riptide'/'DRK', 'All blue creatures become tapped.').
card_first_print('riptide', 'DRK').
card_image_name('riptide'/'DRK', 'riptide').
card_uid('riptide'/'DRK', 'DRK:Riptide:riptide').
card_rarity('riptide'/'DRK', 'Common').
card_artist('riptide'/'DRK', 'Randy Asplund-Faith').
card_multiverse_id('riptide'/'DRK', '1761').

card_in_set('runesword', 'DRK').
card_original_type('runesword'/'DRK', 'Artifact').
card_original_text('runesword'/'DRK', '{3}, {T}: Target attacking creature gains +2/+0 until end of turn. Any creature damaged by target creature may not be regenerated this turn; if such a creature is placed in the graveyard this turn, remove it from the game. If target creature leaves play before end of turn, Runesword is buried.').
card_first_print('runesword', 'DRK').
card_image_name('runesword'/'DRK', 'runesword').
card_uid('runesword'/'DRK', 'DRK:Runesword:runesword').
card_rarity('runesword'/'DRK', 'Uncommon').
card_artist('runesword'/'DRK', 'Christopher Rush').
card_multiverse_id('runesword'/'DRK', '1719').

card_in_set('safe haven', 'DRK').
card_original_type('safe haven'/'DRK', 'Land').
card_original_text('safe haven'/'DRK', '{2}, {T}: Remove target creature you control from game. This ability is played as an interrupt.\nDuring your upkeep, sacrifice Safe Haven to return all creatures it has removed from game directly into play. Treat this as if they were just summoned.').
card_first_print('safe haven', 'DRK').
card_image_name('safe haven'/'DRK', 'safe haven').
card_uid('safe haven'/'DRK', 'DRK:Safe Haven:safe haven').
card_rarity('safe haven'/'DRK', 'Rare').
card_artist('safe haven'/'DRK', 'Christopher Rush').
card_multiverse_id('safe haven'/'DRK', '1825').

card_in_set('savaen elves', 'DRK').
card_original_type('savaen elves'/'DRK', 'Summon — Elves').
card_original_text('savaen elves'/'DRK', '{G}{G}, {T}: Target enchant land is destroyed.').
card_first_print('savaen elves', 'DRK').
card_image_name('savaen elves'/'DRK', 'savaen elves').
card_uid('savaen elves'/'DRK', 'DRK:Savaen Elves:savaen elves').
card_rarity('savaen elves'/'DRK', 'Common').
card_artist('savaen elves'/'DRK', 'Ron Spencer').
card_flavor_text('savaen elves'/'DRK', '\"Purity of magic can only come from purity of the land. How can a meal nourish if the ingredients are spoiled?\" —Sidaine of Savaen').
card_multiverse_id('savaen elves'/'DRK', '1774').

card_in_set('scarecrow', 'DRK').
card_original_type('scarecrow'/'DRK', 'Artifact Creature').
card_original_text('scarecrow'/'DRK', '{6}, {T}: Until end of turn, all damage done to you by flying creatures is reduced to 0.').
card_first_print('scarecrow', 'DRK').
card_image_name('scarecrow'/'DRK', 'scarecrow').
card_uid('scarecrow'/'DRK', 'DRK:Scarecrow:scarecrow').
card_rarity('scarecrow'/'DRK', 'Uncommon').
card_artist('scarecrow'/'DRK', 'Anson Maddocks').
card_flavor_text('scarecrow'/'DRK', 'There was more malice in its button eyes than should have been possible in something that had never known life.').
card_multiverse_id('scarecrow'/'DRK', '1720').

card_in_set('scarwood bandits', 'DRK').
card_original_type('scarwood bandits'/'DRK', 'Summon — Bandits').
card_original_text('scarwood bandits'/'DRK', 'Forestwalk\n{2}{G}, {T}: Take control of target artifact. Opponent may counter this action by paying {2}. You lose control of target artifact if Scarwood Bandits leave play or at end of game.').
card_first_print('scarwood bandits', 'DRK').
card_image_name('scarwood bandits'/'DRK', 'scarwood bandits').
card_uid('scarwood bandits'/'DRK', 'DRK:Scarwood Bandits:scarwood bandits').
card_rarity('scarwood bandits'/'DRK', 'Rare').
card_artist('scarwood bandits'/'DRK', 'Mark Poole').
card_multiverse_id('scarwood bandits'/'DRK', '1775').

card_in_set('scarwood goblins', 'DRK').
card_original_type('scarwood goblins'/'DRK', 'Summon — Goblins').
card_original_text('scarwood goblins'/'DRK', 'Counts as both a green card and a red card.').
card_first_print('scarwood goblins', 'DRK').
card_image_name('scarwood goblins'/'DRK', 'scarwood goblins').
card_uid('scarwood goblins'/'DRK', 'DRK:Scarwood Goblins:scarwood goblins').
card_rarity('scarwood goblins'/'DRK', 'Common').
card_artist('scarwood goblins'/'DRK', 'Ron Spencer').
card_flavor_text('scarwood goblins'/'DRK', 'Larger and more cunning than most Goblins, Scarwood Goblins are thankfully found only in isolated pockets.').
card_multiverse_id('scarwood goblins'/'DRK', '1822').

card_in_set('scarwood hag', 'DRK').
card_original_type('scarwood hag'/'DRK', 'Summon — Hag').
card_original_text('scarwood hag'/'DRK', '{G}{G}{G}{G}, {T}: Target creature gains forestwalk until end of turn.\n{T}: Target creature loses forestwalk until end of turn.').
card_first_print('scarwood hag', 'DRK').
card_image_name('scarwood hag'/'DRK', 'scarwood hag').
card_uid('scarwood hag'/'DRK', 'DRK:Scarwood Hag:scarwood hag').
card_rarity('scarwood hag'/'DRK', 'Uncommon').
card_artist('scarwood hag'/'DRK', 'Anson Maddocks').
card_multiverse_id('scarwood hag'/'DRK', '1776').

card_in_set('scavenger folk', 'DRK').
card_original_type('scavenger folk'/'DRK', 'Summon — Scavenger Folk').
card_original_text('scavenger folk'/'DRK', '{G}, {T}: Sacrifice Scavenger Folk to destroy target artifact.').
card_first_print('scavenger folk', 'DRK').
card_image_name('scavenger folk'/'DRK', 'scavenger folk').
card_uid('scavenger folk'/'DRK', 'DRK:Scavenger Folk:scavenger folk').
card_rarity('scavenger folk'/'DRK', 'Common').
card_artist('scavenger folk'/'DRK', 'Dennis Detwiller').
card_flavor_text('scavenger folk'/'DRK', 'String, weapons, wax, or jewels—it makes no difference. Leave nothing unguarded in Scarwood.').
card_multiverse_id('scavenger folk'/'DRK', '1777').

card_in_set('season of the witch', 'DRK').
card_original_type('season of the witch'/'DRK', 'Enchantment').
card_original_text('season of the witch'/'DRK', 'At the end of each player\'s turn, all of his or her untapped creatures that could have attacked but did not are destroyed. If you do not pay 2 life during your upkeep, Season of the Witch is destroyed. Effects that prevent or redirect damage may not be used to counter this loss of life.').
card_first_print('season of the witch', 'DRK').
card_image_name('season of the witch'/'DRK', 'season of the witch').
card_uid('season of the witch'/'DRK', 'DRK:Season of the Witch:season of the witch').
card_rarity('season of the witch'/'DRK', 'Rare').
card_artist('season of the witch'/'DRK', 'Jesper Myrfors').
card_multiverse_id('season of the witch'/'DRK', '1741').

card_in_set('sisters of the flame', 'DRK').
card_original_type('sisters of the flame'/'DRK', 'Summon — Sisters').
card_original_text('sisters of the flame'/'DRK', '{T}: Add {R} to your mana pool. This ability is played as an interrupt.').
card_first_print('sisters of the flame', 'DRK').
card_image_name('sisters of the flame'/'DRK', 'sisters of the flame').
card_uid('sisters of the flame'/'DRK', 'DRK:Sisters of the Flame:sisters of the flame').
card_rarity('sisters of the flame'/'DRK', 'Uncommon').
card_artist('sisters of the flame'/'DRK', 'Jesper Myrfors').
card_flavor_text('sisters of the flame'/'DRK', 'We are many wicks sharing a common tallow; we feed the skies with the ashes of our prey.').
card_multiverse_id('sisters of the flame'/'DRK', '1800').

card_in_set('skull of orm', 'DRK').
card_original_type('skull of orm'/'DRK', 'Artifact').
card_original_text('skull of orm'/'DRK', '{5}, {T}: Bring one enchantment card from your graveyard to your hand.').
card_first_print('skull of orm', 'DRK').
card_image_name('skull of orm'/'DRK', 'skull of orm').
card_uid('skull of orm'/'DRK', 'DRK:Skull of Orm:skull of orm').
card_rarity('skull of orm'/'DRK', 'Uncommon').
card_artist('skull of orm'/'DRK', 'Tom Wänerstrand').
card_flavor_text('skull of orm'/'DRK', 'Though lifeless, the Skull still possessed a strange power over the flow of magic.').
card_multiverse_id('skull of orm'/'DRK', '1721').

card_in_set('sorrow\'s path', 'DRK').
card_original_type('sorrow\'s path'/'DRK', 'Land').
card_original_text('sorrow\'s path'/'DRK', '{T}: Exchange two of opponent\'s blocking creatures. This exchange may not cause an illegal block. Sorrow\'s Path does 2 damage to you and 2 damage to each creature you control whenever it is tapped.').
card_first_print('sorrow\'s path', 'DRK').
card_image_name('sorrow\'s path'/'DRK', 'sorrow\'s path').
card_uid('sorrow\'s path'/'DRK', 'DRK:Sorrow\'s Path:sorrow\'s path').
card_rarity('sorrow\'s path'/'DRK', 'Rare').
card_artist('sorrow\'s path'/'DRK', 'Randy Asplund-Faith').
card_multiverse_id('sorrow\'s path'/'DRK', '1826').

card_in_set('spitting slug', 'DRK').
card_original_type('spitting slug'/'DRK', 'Summon — Slug').
card_original_text('spitting slug'/'DRK', '{1}{G} Spitting Slug gains first strike until end of turn. If this ability is not activated, all creatures blocking or blocked by Spitting Slug gain first strike until end of turn.').
card_first_print('spitting slug', 'DRK').
card_image_name('spitting slug'/'DRK', 'spitting slug').
card_uid('spitting slug'/'DRK', 'DRK:Spitting Slug:spitting slug').
card_rarity('spitting slug'/'DRK', 'Uncommon').
card_artist('spitting slug'/'DRK', 'Anson Maddocks').
card_multiverse_id('spitting slug'/'DRK', '1778').

card_in_set('squire', 'DRK').
card_original_type('squire'/'DRK', 'Summon — Squire').
card_original_text('squire'/'DRK', '').
card_first_print('squire', 'DRK').
card_image_name('squire'/'DRK', 'squire').
card_uid('squire'/'DRK', 'DRK:Squire:squire').
card_rarity('squire'/'DRK', 'Common').
card_artist('squire'/'DRK', 'Dennis Detwiller').
card_flavor_text('squire'/'DRK', '\"Of twenty yeer of age he was, I gesse. Of his stature he was of even lengthe. And wonderly deliver, and greete of strengthe.\" —Geoffrey Chaucer, The Canterbury Tales').
card_multiverse_id('squire'/'DRK', '1817').

card_in_set('standing stones', 'DRK').
card_original_type('standing stones'/'DRK', 'Artifact').
card_original_text('standing stones'/'DRK', '{1}, {T}: Pay 1 life and add 1 mana of any color to your mana pool. This ability is played as an interrupt. Effects that prevent or redirect damage may not be used to counter this loss of life.').
card_first_print('standing stones', 'DRK').
card_image_name('standing stones'/'DRK', 'standing stones').
card_uid('standing stones'/'DRK', 'DRK:Standing Stones:standing stones').
card_rarity('standing stones'/'DRK', 'Uncommon').
card_artist('standing stones'/'DRK', 'Sandra Everingham').
card_multiverse_id('standing stones'/'DRK', '1722').

card_in_set('stone calendar', 'DRK').
card_original_type('stone calendar'/'DRK', 'Artifact').
card_original_text('stone calendar'/'DRK', 'Your spells cost up to 1 less to cast; casting cost of spells cannot go below 0.').
card_first_print('stone calendar', 'DRK').
card_image_name('stone calendar'/'DRK', 'stone calendar').
card_uid('stone calendar'/'DRK', 'DRK:Stone Calendar:stone calendar').
card_rarity('stone calendar'/'DRK', 'Rare').
card_artist('stone calendar'/'DRK', 'Amy Weber').
card_flavor_text('stone calendar'/'DRK', 'The Pretender Mairsil ordered a great Calendar drawn up to show when the paths to the Dark Lands were strongest.').
card_multiverse_id('stone calendar'/'DRK', '1723').

card_in_set('sunken city', 'DRK').
card_original_type('sunken city'/'DRK', 'Enchantment').
card_original_text('sunken city'/'DRK', 'All blue creatures gain +1/+1.\nIf you do not pay {U}{U} during your upkeep, Sunken City is destroyed.').
card_first_print('sunken city', 'DRK').
card_image_name('sunken city'/'DRK', 'sunken city').
card_uid('sunken city'/'DRK', 'DRK:Sunken City:sunken city').
card_rarity('sunken city'/'DRK', 'Common').
card_artist('sunken city'/'DRK', 'Jesper Myrfors').
card_multiverse_id('sunken city'/'DRK', '1762').

card_in_set('tangle kelp', 'DRK').
card_original_type('tangle kelp'/'DRK', 'Enchant Creature').
card_original_text('tangle kelp'/'DRK', 'Target creature does not untap during its controller\'s untap phase if it attacked during its controller\'s last turn. Target creature becomes tapped when Tangle Kelp is cast.').
card_first_print('tangle kelp', 'DRK').
card_image_name('tangle kelp'/'DRK', 'tangle kelp').
card_uid('tangle kelp'/'DRK', 'DRK:Tangle Kelp:tangle kelp').
card_rarity('tangle kelp'/'DRK', 'Uncommon').
card_artist('tangle kelp'/'DRK', 'Rob Alexander').
card_multiverse_id('tangle kelp'/'DRK', '1763').

card_in_set('the fallen', 'DRK').
card_original_type('the fallen'/'DRK', 'Summon — Fallen').
card_original_text('the fallen'/'DRK', 'During its controller\'s upkeep, The Fallen does 1 damage to each opponent it has previously damaged.').
card_first_print('the fallen', 'DRK').
card_image_name('the fallen'/'DRK', 'the fallen').
card_uid('the fallen'/'DRK', 'DRK:The Fallen:the fallen').
card_rarity('the fallen'/'DRK', 'Uncommon').
card_artist('the fallen'/'DRK', 'Jesper Myrfors').
card_flavor_text('the fallen'/'DRK', 'Magic often masters those who cannot master it.').
card_multiverse_id('the fallen'/'DRK', '1742').

card_in_set('tivadar\'s crusade', 'DRK').
card_original_type('tivadar\'s crusade'/'DRK', 'Sorcery').
card_original_text('tivadar\'s crusade'/'DRK', 'All Goblins are destroyed.').
card_first_print('tivadar\'s crusade', 'DRK').
card_image_name('tivadar\'s crusade'/'DRK', 'tivadar\'s crusade').
card_uid('tivadar\'s crusade'/'DRK', 'DRK:Tivadar\'s Crusade:tivadar\'s crusade').
card_rarity('tivadar\'s crusade'/'DRK', 'Uncommon').
card_artist('tivadar\'s crusade'/'DRK', 'Dennis Detwiller').
card_multiverse_id('tivadar\'s crusade'/'DRK', '1818').

card_in_set('tormod\'s crypt', 'DRK').
card_original_type('tormod\'s crypt'/'DRK', 'Artifact').
card_original_text('tormod\'s crypt'/'DRK', '{T}: Sacrifice Tormod\'s Crypt to remove all cards in target player\'s graveyard from the game.').
card_first_print('tormod\'s crypt', 'DRK').
card_image_name('tormod\'s crypt'/'DRK', 'tormod\'s crypt').
card_uid('tormod\'s crypt'/'DRK', 'DRK:Tormod\'s Crypt:tormod\'s crypt').
card_rarity('tormod\'s crypt'/'DRK', 'Uncommon').
card_artist('tormod\'s crypt'/'DRK', 'Christopher Rush').
card_flavor_text('tormod\'s crypt'/'DRK', 'The dark opening seemed to breathe the cold, damp air of the dead earth in a steady rhythm.').
card_multiverse_id('tormod\'s crypt'/'DRK', '1724').

card_in_set('tower of coireall', 'DRK').
card_original_type('tower of coireall'/'DRK', 'Artifact').
card_original_text('tower of coireall'/'DRK', '{T}: Target creature cannot be blocked by walls until end of turn.').
card_first_print('tower of coireall', 'DRK').
card_image_name('tower of coireall'/'DRK', 'tower of coireall').
card_uid('tower of coireall'/'DRK', 'DRK:Tower of Coireall:tower of coireall').
card_rarity('tower of coireall'/'DRK', 'Uncommon').
card_artist('tower of coireall'/'DRK', 'Dan Frazier').
card_multiverse_id('tower of coireall'/'DRK', '1725').

card_in_set('tracker', 'DRK').
card_original_type('tracker'/'DRK', 'Summon — Tracker').
card_original_text('tracker'/'DRK', '{G}{G}, {T}: Tracker does an amount of damage equal to its power to target creature. Target creature does an amount of damage equal to its power to Tracker.').
card_first_print('tracker', 'DRK').
card_image_name('tracker'/'DRK', 'tracker').
card_uid('tracker'/'DRK', 'DRK:Tracker:tracker').
card_rarity('tracker'/'DRK', 'Rare').
card_artist('tracker'/'DRK', 'Jeff A. Menges').
card_multiverse_id('tracker'/'DRK', '1779').

card_in_set('uncle istvan', 'DRK').
card_original_type('uncle istvan'/'DRK', 'Summon — Uncle Istvan').
card_original_text('uncle istvan'/'DRK', 'All damage done to Uncle Istvan by creatures is reduced to 0.').
card_first_print('uncle istvan', 'DRK').
card_image_name('uncle istvan'/'DRK', 'uncle istvan').
card_uid('uncle istvan'/'DRK', 'DRK:Uncle Istvan:uncle istvan').
card_rarity('uncle istvan'/'DRK', 'Uncommon').
card_artist('uncle istvan'/'DRK', 'Daniel Gelon').
card_flavor_text('uncle istvan'/'DRK', 'Solitude drove the old hermit insane. Now he only keeps company with those he can catch.').
card_multiverse_id('uncle istvan'/'DRK', '1743').

card_in_set('venom', 'DRK').
card_original_type('venom'/'DRK', 'Enchant Creature').
card_original_text('venom'/'DRK', 'All non-wall creatures target creature blocks or is blocked by are destroyed at the end of combat.').
card_first_print('venom', 'DRK').
card_image_name('venom'/'DRK', 'venom').
card_uid('venom'/'DRK', 'DRK:Venom:venom').
card_rarity('venom'/'DRK', 'Common').
card_artist('venom'/'DRK', 'Tom Wänerstrand').
card_flavor_text('venom'/'DRK', '\"I told him it was just a flesh wound, a wee scratch, but the next time I looked at him, poor Tadhg was dead and gone.\" —Maeveen O\'Donagh, Memoirs of a Soldier').
card_multiverse_id('venom'/'DRK', '1780').

card_in_set('wand of ith', 'DRK').
card_original_type('wand of ith'/'DRK', 'Artifact').
card_original_text('wand of ith'/'DRK', '{3}, {T}: Look at one card at random from target player\'s hand. If the card is not a land, target player must choose either to discard it or pay an amount of life equal to its casting cost. If the card is a land, target player must choose either to discard it or pay 1 life. Effects that prevent or redirect damage may not be used to counter this loss of life. Can only be used during controller\'s turn.').
card_first_print('wand of ith', 'DRK').
card_image_name('wand of ith'/'DRK', 'wand of ith').
card_uid('wand of ith'/'DRK', 'DRK:Wand of Ith:wand of ith').
card_rarity('wand of ith'/'DRK', 'Uncommon').
card_artist('wand of ith'/'DRK', 'Quinton Hoover').
card_multiverse_id('wand of ith'/'DRK', '1726').

card_in_set('war barge', 'DRK').
card_original_type('war barge'/'DRK', 'Artifact').
card_original_text('war barge'/'DRK', '{3}: Target creature gains islandwalk until end of turn. If War Barge leaves play this turn, target creature is buried.').
card_first_print('war barge', 'DRK').
card_image_name('war barge'/'DRK', 'war barge').
card_uid('war barge'/'DRK', 'DRK:War Barge:war barge').
card_rarity('war barge'/'DRK', 'Uncommon').
card_artist('war barge'/'DRK', 'Tom Wänerstrand').
card_multiverse_id('war barge'/'DRK', '1727').

card_in_set('water wurm', 'DRK').
card_original_type('water wurm'/'DRK', 'Summon — Wurm').
card_original_text('water wurm'/'DRK', 'Water Wurm gains +0/+1 if opponent controls at least one island.').
card_first_print('water wurm', 'DRK').
card_image_name('water wurm'/'DRK', 'water wurm').
card_uid('water wurm'/'DRK', 'DRK:Water Wurm:water wurm').
card_rarity('water wurm'/'DRK', 'Common').
card_artist('water wurm'/'DRK', 'Ron Spencer').
card_multiverse_id('water wurm'/'DRK', '1764').

card_in_set('whippoorwill', 'DRK').
card_original_type('whippoorwill'/'DRK', 'Summon — Whippoorwill').
card_original_text('whippoorwill'/'DRK', '{G}{G}, {T}: Until end of turn, target creature may not regenerate and damage done to target creature may not be prevented or redirected. If target creature goes to the graveyard, remove it from game.').
card_first_print('whippoorwill', 'DRK').
card_image_name('whippoorwill'/'DRK', 'whippoorwill').
card_uid('whippoorwill'/'DRK', 'DRK:Whippoorwill:whippoorwill').
card_rarity('whippoorwill'/'DRK', 'Uncommon').
card_artist('whippoorwill'/'DRK', 'Douglas Shuler').
card_flavor_text('whippoorwill'/'DRK', 'If the Whippoorwill remains silent, the soul has not reached its reward.').
card_multiverse_id('whippoorwill'/'DRK', '1781').

card_in_set('witch hunter', 'DRK').
card_original_type('witch hunter'/'DRK', 'Summon — Hunter').
card_original_text('witch hunter'/'DRK', '{T}: Witch Hunter does 1 damage to target player.\n{1}{W}{W}, {T}: Return target creature opponent controls from play to owner\'s hand. Enchantments on target creature are destroyed.').
card_first_print('witch hunter', 'DRK').
card_image_name('witch hunter'/'DRK', 'witch hunter').
card_uid('witch hunter'/'DRK', 'DRK:Witch Hunter:witch hunter').
card_rarity('witch hunter'/'DRK', 'Rare').
card_artist('witch hunter'/'DRK', 'Jesper Myrfors').
card_multiverse_id('witch hunter'/'DRK', '1819').

card_in_set('word of binding', 'DRK').
card_original_type('word of binding'/'DRK', 'Sorcery').
card_original_text('word of binding'/'DRK', 'X target creatures become tapped.').
card_first_print('word of binding', 'DRK').
card_image_name('word of binding'/'DRK', 'word of binding').
card_uid('word of binding'/'DRK', 'DRK:Word of Binding:word of binding').
card_rarity('word of binding'/'DRK', 'Common').
card_artist('word of binding'/'DRK', 'Ron Spencer').
card_flavor_text('word of binding'/'DRK', '\"That was the worst experience of my days, standing there helpless as they killed my whole troop.\" —Maeveen O\'Donagh, Memoirs of a Soldier').
card_multiverse_id('word of binding'/'DRK', '1744').

card_in_set('worms of the earth', 'DRK').
card_original_type('worms of the earth'/'DRK', 'Enchantment').
card_original_text('worms of the earth'/'DRK', 'No new land may be brought into play. During any player\'s upkeep, any player may destroy Worms of the Earth by sacrificing two lands or taking 5 damage from Worms of the Earth.').
card_first_print('worms of the earth', 'DRK').
card_image_name('worms of the earth'/'DRK', 'worms of the earth').
card_uid('worms of the earth'/'DRK', 'DRK:Worms of the Earth:worms of the earth').
card_rarity('worms of the earth'/'DRK', 'Rare').
card_artist('worms of the earth'/'DRK', 'Anson Maddocks').
card_flavor_text('worms of the earth'/'DRK', 'The ground collapsed, leaving nothing but the great Worms\' mucous residues.').
card_multiverse_id('worms of the earth'/'DRK', '1745').

card_in_set('wormwood treefolk', 'DRK').
card_original_type('wormwood treefolk'/'DRK', 'Summon — Treefolk').
card_original_text('wormwood treefolk'/'DRK', '{G}{G} Wormwood Treefolk gains forestwalk until end of turn and does 2 damage to you.\n{B}{B} Wormwood Treefolk gains swampwalk until end of turn and does 2 damage to you.').
card_first_print('wormwood treefolk', 'DRK').
card_image_name('wormwood treefolk'/'DRK', 'wormwood treefolk').
card_uid('wormwood treefolk'/'DRK', 'DRK:Wormwood Treefolk:wormwood treefolk').
card_rarity('wormwood treefolk'/'DRK', 'Rare').
card_artist('wormwood treefolk'/'DRK', 'Jesper Myrfors').
card_multiverse_id('wormwood treefolk'/'DRK', '1782').
