% Shadowmoor

set('SHM').
set_name('SHM', 'Shadowmoor').
set_release_date('SHM', '2008-05-02').
set_border('SHM', 'black').
set_type('SHM', 'expansion').
set_block('SHM', 'Shadowmoor').

card_in_set('advice from the fae', 'SHM').
card_original_type('advice from the fae'/'SHM', 'Sorcery').
card_original_text('advice from the fae'/'SHM', '({2/U} can be paid with any two mana or with {U}. This card\'s converted mana cost is 6.)\nLook at the top five cards of your library. If you control more creatures than any other player, put two of those cards into your hand. Otherwise, put one of them into your hand. Then put the rest on the bottom of your library in any order.').
card_first_print('advice from the fae', 'SHM').
card_image_name('advice from the fae'/'SHM', 'advice from the fae').
card_uid('advice from the fae'/'SHM', 'SHM:Advice from the Fae:advice from the fae').
card_rarity('advice from the fae'/'SHM', 'Uncommon').
card_artist('advice from the fae'/'SHM', 'Chippy').
card_number('advice from the fae'/'SHM', '28').
card_multiverse_id('advice from the fae'/'SHM', '154408').

card_in_set('æthertow', 'SHM').
card_original_type('æthertow'/'SHM', 'Instant').
card_original_text('æthertow'/'SHM', 'Put target attacking or blocking creature on top of its owner\'s library.\nConspire (As you play this spell, you may tap two untapped creatures you control that share a color with it. When you do, copy it and you may choose a new target for the copy.)').
card_first_print('æthertow', 'SHM').
card_image_name('æthertow'/'SHM', 'aethertow').
card_uid('æthertow'/'SHM', 'SHM:Æthertow:aethertow').
card_rarity('æthertow'/'SHM', 'Common').
card_artist('æthertow'/'SHM', 'Warren Mahy').
card_number('æthertow'/'SHM', '136').
card_multiverse_id('æthertow'/'SHM', '158750').

card_in_set('aphotic wisps', 'SHM').
card_original_type('aphotic wisps'/'SHM', 'Instant').
card_original_text('aphotic wisps'/'SHM', 'Target creature becomes black and gains fear until end of turn.\nDraw a card.').
card_first_print('aphotic wisps', 'SHM').
card_image_name('aphotic wisps'/'SHM', 'aphotic wisps').
card_uid('aphotic wisps'/'SHM', 'SHM:Aphotic Wisps:aphotic wisps').
card_rarity('aphotic wisps'/'SHM', 'Common').
card_artist('aphotic wisps'/'SHM', 'Jim Nelson').
card_number('aphotic wisps'/'SHM', '55').
card_flavor_text('aphotic wisps'/'SHM', 'Merrows skulk the silty bogs around the Wanderbrine, their very thoughts stained with evil.').
card_multiverse_id('aphotic wisps'/'SHM', '154392').

card_in_set('apothecary initiate', 'SHM').
card_original_type('apothecary initiate'/'SHM', 'Creature — Kithkin Cleric').
card_original_text('apothecary initiate'/'SHM', 'Whenever a player plays a white spell, you may pay {1}. If you do, you gain 1 life.').
card_first_print('apothecary initiate', 'SHM').
card_image_name('apothecary initiate'/'SHM', 'apothecary initiate').
card_uid('apothecary initiate'/'SHM', 'SHM:Apothecary Initiate:apothecary initiate').
card_rarity('apothecary initiate'/'SHM', 'Common').
card_artist('apothecary initiate'/'SHM', 'Kev Walker').
card_number('apothecary initiate'/'SHM', '1').
card_flavor_text('apothecary initiate'/'SHM', 'Kithkin jealously hoard their knowledge of poultices and remedies so that no outside threat can benefit from their wisdom.').
card_multiverse_id('apothecary initiate'/'SHM', '141929').

card_in_set('armored ascension', 'SHM').
card_original_type('armored ascension'/'SHM', 'Enchantment — Aura').
card_original_text('armored ascension'/'SHM', 'Enchant creature\nEnchanted creature gets +1/+1 for each Plains you control and has flying.').
card_first_print('armored ascension', 'SHM').
card_image_name('armored ascension'/'SHM', 'armored ascension').
card_uid('armored ascension'/'SHM', 'SHM:Armored Ascension:armored ascension').
card_rarity('armored ascension'/'SHM', 'Uncommon').
card_artist('armored ascension'/'SHM', 'Matt Cavotta').
card_number('armored ascension'/'SHM', '2').
card_flavor_text('armored ascension'/'SHM', '\"Why enchant feathers to make them sturdy when we can enchant shields to make them fly?\"\n—Braenna, cobblesmith').
card_multiverse_id('armored ascension'/'SHM', '146041').

card_in_set('ashenmoor cohort', 'SHM').
card_original_type('ashenmoor cohort'/'SHM', 'Creature — Elemental Warrior').
card_original_text('ashenmoor cohort'/'SHM', 'Ashenmoor Cohort gets +1/+1 as long as you control another black creature.').
card_first_print('ashenmoor cohort', 'SHM').
card_image_name('ashenmoor cohort'/'SHM', 'ashenmoor cohort').
card_uid('ashenmoor cohort'/'SHM', 'SHM:Ashenmoor Cohort:ashenmoor cohort').
card_rarity('ashenmoor cohort'/'SHM', 'Common').
card_artist('ashenmoor cohort'/'SHM', 'Lucio Parrillo').
card_number('ashenmoor cohort'/'SHM', '56').
card_flavor_text('ashenmoor cohort'/'SHM', '\"None will concede a place for us in this world. If it is not for us, neither shall it be for them. We rest not until they choke in its cold ashes.\"\n—Illulia of Nighthearth').
card_multiverse_id('ashenmoor cohort'/'SHM', '142060').

card_in_set('ashenmoor gouger', 'SHM').
card_original_type('ashenmoor gouger'/'SHM', 'Creature — Elemental Warrior').
card_original_text('ashenmoor gouger'/'SHM', 'Ashenmoor Gouger can\'t block.').
card_first_print('ashenmoor gouger', 'SHM').
card_image_name('ashenmoor gouger'/'SHM', 'ashenmoor gouger').
card_uid('ashenmoor gouger'/'SHM', 'SHM:Ashenmoor Gouger:ashenmoor gouger').
card_rarity('ashenmoor gouger'/'SHM', 'Uncommon').
card_artist('ashenmoor gouger'/'SHM', 'Matt Cavotta').
card_number('ashenmoor gouger'/'SHM', '180').
card_flavor_text('ashenmoor gouger'/'SHM', 'After his hands had crumbled away, leaving only wickedly sharp points, he decided his only purpose was war.').
card_multiverse_id('ashenmoor gouger'/'SHM', '153981').

card_in_set('ashenmoor liege', 'SHM').
card_original_type('ashenmoor liege'/'SHM', 'Creature — Elemental Knight').
card_original_text('ashenmoor liege'/'SHM', 'Other black creatures you control get +1/+1.\nOther red creatures you control get +1/+1.\nWhenever Ashenmoor Liege becomes the target of a spell or ability an opponent controls, that player loses 4 life.').
card_first_print('ashenmoor liege', 'SHM').
card_image_name('ashenmoor liege'/'SHM', 'ashenmoor liege').
card_uid('ashenmoor liege'/'SHM', 'SHM:Ashenmoor Liege:ashenmoor liege').
card_rarity('ashenmoor liege'/'SHM', 'Rare').
card_artist('ashenmoor liege'/'SHM', 'Mark Zug').
card_number('ashenmoor liege'/'SHM', '181').
card_flavor_text('ashenmoor liege'/'SHM', 'Cinders carry an ancestral grudge that makes them slow to trust and quick to retaliate.').
card_multiverse_id('ashenmoor liege'/'SHM', '146065').

card_in_set('augury adept', 'SHM').
card_original_type('augury adept'/'SHM', 'Creature — Kithkin Wizard').
card_original_text('augury adept'/'SHM', 'Whenever Augury Adept deals combat damage to a player, reveal the top card of your library and put that card into your hand. You gain life equal to its converted mana cost.').
card_first_print('augury adept', 'SHM').
card_image_name('augury adept'/'SHM', 'augury adept').
card_uid('augury adept'/'SHM', 'SHM:Augury Adept:augury adept').
card_rarity('augury adept'/'SHM', 'Rare').
card_artist('augury adept'/'SHM', 'Steve Prescott').
card_number('augury adept'/'SHM', '137').
card_flavor_text('augury adept'/'SHM', 'Moonstones grant limited foresight but unlimited advantages.').
card_multiverse_id('augury adept'/'SHM', '147407').

card_in_set('ballynock cohort', 'SHM').
card_original_type('ballynock cohort'/'SHM', 'Creature — Kithkin Soldier').
card_original_text('ballynock cohort'/'SHM', 'First strike\nBallynock Cohort gets +1/+1 as long as you control another white creature.').
card_first_print('ballynock cohort', 'SHM').
card_image_name('ballynock cohort'/'SHM', 'ballynock cohort').
card_uid('ballynock cohort'/'SHM', 'SHM:Ballynock Cohort:ballynock cohort').
card_rarity('ballynock cohort'/'SHM', 'Common').
card_artist('ballynock cohort'/'SHM', 'Jesper Ejsing').
card_number('ballynock cohort'/'SHM', '3').
card_flavor_text('ballynock cohort'/'SHM', 'A kithkin\'s worst enemy is solitude.').
card_multiverse_id('ballynock cohort'/'SHM', '142045').

card_in_set('barkshell blessing', 'SHM').
card_original_type('barkshell blessing'/'SHM', 'Instant').
card_original_text('barkshell blessing'/'SHM', 'Target creature gets +2/+2 until end of turn.\nConspire (As you play this spell, you may tap two untapped creatures you control that share a color with it. When you do, copy it and you may choose a new target for the copy.)').
card_first_print('barkshell blessing', 'SHM').
card_image_name('barkshell blessing'/'SHM', 'barkshell blessing').
card_uid('barkshell blessing'/'SHM', 'SHM:Barkshell Blessing:barkshell blessing').
card_rarity('barkshell blessing'/'SHM', 'Common').
card_artist('barkshell blessing'/'SHM', 'Steven Belledin').
card_number('barkshell blessing'/'SHM', '224').
card_multiverse_id('barkshell blessing'/'SHM', '158772').

card_in_set('barrenton cragtreads', 'SHM').
card_original_type('barrenton cragtreads'/'SHM', 'Creature — Kithkin Scout').
card_original_text('barrenton cragtreads'/'SHM', 'Barrenton Cragtreads can\'t be blocked by red creatures.').
card_first_print('barrenton cragtreads', 'SHM').
card_image_name('barrenton cragtreads'/'SHM', 'barrenton cragtreads').
card_uid('barrenton cragtreads'/'SHM', 'SHM:Barrenton Cragtreads:barrenton cragtreads').
card_rarity('barrenton cragtreads'/'SHM', 'Common').
card_artist('barrenton cragtreads'/'SHM', 'Daren Bader').
card_number('barrenton cragtreads'/'SHM', '138').
card_flavor_text('barrenton cragtreads'/'SHM', '\"Boggarts are easy to get around. Just toss some mutton in another direction. Giants are a little harder. You have to be quick to avoid their steps. Cinders are the difficult ones, but even they have fears to be exploited.\"').
card_multiverse_id('barrenton cragtreads'/'SHM', '142000').

card_in_set('barrenton medic', 'SHM').
card_original_type('barrenton medic'/'SHM', 'Creature — Kithkin Cleric').
card_original_text('barrenton medic'/'SHM', '{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.\nPut a -1/-1 counter on Barrenton Medic: Untap Barrenton Medic.').
card_first_print('barrenton medic', 'SHM').
card_image_name('barrenton medic'/'SHM', 'barrenton medic').
card_uid('barrenton medic'/'SHM', 'SHM:Barrenton Medic:barrenton medic').
card_rarity('barrenton medic'/'SHM', 'Common').
card_artist('barrenton medic'/'SHM', 'Trevor Hairsine').
card_number('barrenton medic'/'SHM', '4').
card_flavor_text('barrenton medic'/'SHM', 'His dreams are filled with noxious powders and clinking vials.').
card_multiverse_id('barrenton medic'/'SHM', '135451').

card_in_set('beseech the queen', 'SHM').
card_original_type('beseech the queen'/'SHM', 'Sorcery').
card_original_text('beseech the queen'/'SHM', '({2/B} can be paid with any two mana or with {B}. This card\'s converted mana cost is 6.)\nSearch your library for a card with converted mana cost less than or equal to the number of lands you control, reveal it, and put it into your hand. Then shuffle your library.').
card_first_print('beseech the queen', 'SHM').
card_image_name('beseech the queen'/'SHM', 'beseech the queen').
card_uid('beseech the queen'/'SHM', 'SHM:Beseech the Queen:beseech the queen').
card_rarity('beseech the queen'/'SHM', 'Uncommon').
card_artist('beseech the queen'/'SHM', 'Jason Chan').
card_number('beseech the queen'/'SHM', '57').
card_flavor_text('beseech the queen'/'SHM', 'Those who hear her go mad with inspiration.').
card_multiverse_id('beseech the queen'/'SHM', '152079').

card_in_set('biting tether', 'SHM').
card_original_type('biting tether'/'SHM', 'Enchantment — Aura').
card_original_text('biting tether'/'SHM', 'Enchant creature\nYou control enchanted creature.\nAt the beginning of your upkeep, put a -1/-1 counter on enchanted creature.').
card_first_print('biting tether', 'SHM').
card_image_name('biting tether'/'SHM', 'biting tether').
card_uid('biting tether'/'SHM', 'SHM:Biting Tether:biting tether').
card_rarity('biting tether'/'SHM', 'Uncommon').
card_artist('biting tether'/'SHM', 'Thomas Denmark').
card_number('biting tether'/'SHM', '29').
card_flavor_text('biting tether'/'SHM', 'A chain of lies as strong as steel.').
card_multiverse_id('biting tether'/'SHM', '135485').

card_in_set('blazethorn scarecrow', 'SHM').
card_original_type('blazethorn scarecrow'/'SHM', 'Artifact Creature — Scarecrow').
card_original_text('blazethorn scarecrow'/'SHM', 'Blazethorn Scarecrow has haste as long as you control a red creature.\nBlazethorn Scarecrow has wither as long as you control a green creature. (It deals damage to creatures in the form of -1/-1 counters.)').
card_first_print('blazethorn scarecrow', 'SHM').
card_image_name('blazethorn scarecrow'/'SHM', 'blazethorn scarecrow').
card_uid('blazethorn scarecrow'/'SHM', 'SHM:Blazethorn Scarecrow:blazethorn scarecrow').
card_rarity('blazethorn scarecrow'/'SHM', 'Common').
card_artist('blazethorn scarecrow'/'SHM', 'Dave Kendall').
card_number('blazethorn scarecrow'/'SHM', '246').
card_multiverse_id('blazethorn scarecrow'/'SHM', '151632').

card_in_set('blight sickle', 'SHM').
card_original_type('blight sickle'/'SHM', 'Artifact — Equipment').
card_original_text('blight sickle'/'SHM', 'Equipped creature gets +1/+0 and has wither. (It deals damage to creatures in the form of -1/-1 counters.)\nEquip {2}').
card_first_print('blight sickle', 'SHM').
card_image_name('blight sickle'/'SHM', 'blight sickle').
card_uid('blight sickle'/'SHM', 'SHM:Blight Sickle:blight sickle').
card_rarity('blight sickle'/'SHM', 'Common').
card_artist('blight sickle'/'SHM', 'John Avon').
card_number('blight sickle'/'SHM', '247').
card_flavor_text('blight sickle'/'SHM', 'Its scars cut deeper than its blade.').
card_multiverse_id('blight sickle'/'SHM', '129135').

card_in_set('blistering dieflyn', 'SHM').
card_original_type('blistering dieflyn'/'SHM', 'Creature — Imp').
card_original_text('blistering dieflyn'/'SHM', 'Flying\n{B/R}: Blistering Dieflyn gets +1/+0 until end of turn.').
card_first_print('blistering dieflyn', 'SHM').
card_image_name('blistering dieflyn'/'SHM', 'blistering dieflyn').
card_uid('blistering dieflyn'/'SHM', 'SHM:Blistering Dieflyn:blistering dieflyn').
card_rarity('blistering dieflyn'/'SHM', 'Common').
card_artist('blistering dieflyn'/'SHM', 'Scott Altmann').
card_number('blistering dieflyn'/'SHM', '82').
card_flavor_text('blistering dieflyn'/'SHM', 'Any kithkin smith would love to catch a dieflyn for her kiln, relieving her of ever having to gather fuel again.').
card_multiverse_id('blistering dieflyn'/'SHM', '141988').

card_in_set('bloodmark mentor', 'SHM').
card_original_type('bloodmark mentor'/'SHM', 'Creature — Goblin Warrior').
card_original_text('bloodmark mentor'/'SHM', 'Red creatures you control have first strike.').
card_first_print('bloodmark mentor', 'SHM').
card_image_name('bloodmark mentor'/'SHM', 'bloodmark mentor').
card_uid('bloodmark mentor'/'SHM', 'SHM:Bloodmark Mentor:bloodmark mentor').
card_rarity('bloodmark mentor'/'SHM', 'Uncommon').
card_artist('bloodmark mentor'/'SHM', 'Dave Allsop').
card_number('bloodmark mentor'/'SHM', '83').
card_flavor_text('bloodmark mentor'/'SHM', 'Boggarts divide the world into two categories: things you can eat, and things you have to chase down and pummel before you can eat.').
card_multiverse_id('bloodmark mentor'/'SHM', '142062').

card_in_set('bloodshed fever', 'SHM').
card_original_type('bloodshed fever'/'SHM', 'Enchantment — Aura').
card_original_text('bloodshed fever'/'SHM', 'Enchant creature\nEnchanted creature attacks each turn if able.').
card_first_print('bloodshed fever', 'SHM').
card_image_name('bloodshed fever'/'SHM', 'bloodshed fever').
card_uid('bloodshed fever'/'SHM', 'SHM:Bloodshed Fever:bloodshed fever').
card_rarity('bloodshed fever'/'SHM', 'Common').
card_artist('bloodshed fever'/'SHM', 'Dan Scott').
card_number('bloodshed fever'/'SHM', '84').
card_flavor_text('bloodshed fever'/'SHM', 'Cinder mages enjoy seeing their victims in the throes of bloodlust—and seeing the regret that inevitably follows.').
card_multiverse_id('bloodshed fever'/'SHM', '159395').

card_in_set('blowfly infestation', 'SHM').
card_original_type('blowfly infestation'/'SHM', 'Enchantment').
card_original_text('blowfly infestation'/'SHM', 'Whenever a creature is put into a graveyard from play, if it had a -1/-1 counter on it, put a -1/-1 counter on target creature.').
card_first_print('blowfly infestation', 'SHM').
card_image_name('blowfly infestation'/'SHM', 'blowfly infestation').
card_uid('blowfly infestation'/'SHM', 'SHM:Blowfly Infestation:blowfly infestation').
card_rarity('blowfly infestation'/'SHM', 'Uncommon').
card_artist('blowfly infestation'/'SHM', 'Drew Tucker').
card_number('blowfly infestation'/'SHM', '58').
card_flavor_text('blowfly infestation'/'SHM', 'When the giant Tollek Worldslayer fell, a corruption erupted from his corpse to carry on his name.').
card_multiverse_id('blowfly infestation'/'SHM', '135429').

card_in_set('boartusk liege', 'SHM').
card_original_type('boartusk liege'/'SHM', 'Creature — Goblin Knight').
card_original_text('boartusk liege'/'SHM', 'Trample\nOther red creatures you control get +1/+1.\nOther green creatures you control get +1/+1.').
card_first_print('boartusk liege', 'SHM').
card_image_name('boartusk liege'/'SHM', 'boartusk liege').
card_uid('boartusk liege'/'SHM', 'SHM:Boartusk Liege:boartusk liege').
card_rarity('boartusk liege'/'SHM', 'Rare').
card_artist('boartusk liege'/'SHM', 'Jesper Ejsing').
card_number('boartusk liege'/'SHM', '202').
card_flavor_text('boartusk liege'/'SHM', 'The boar leads its rider to victory in battle, but it doesn\'t know how close it is to becoming the victory feast.').
card_multiverse_id('boartusk liege'/'SHM', '147428').

card_in_set('boggart arsonists', 'SHM').
card_original_type('boggart arsonists'/'SHM', 'Creature — Goblin Rogue').
card_original_text('boggart arsonists'/'SHM', 'Plainswalk\n{2}{R}, Sacrifice Boggart Arsonists: Destroy target Scarecrow or Plains.').
card_first_print('boggart arsonists', 'SHM').
card_image_name('boggart arsonists'/'SHM', 'boggart arsonists').
card_uid('boggart arsonists'/'SHM', 'SHM:Boggart Arsonists:boggart arsonists').
card_rarity('boggart arsonists'/'SHM', 'Common').
card_artist('boggart arsonists'/'SHM', 'Jesper Ejsing').
card_number('boggart arsonists'/'SHM', '85').
card_flavor_text('boggart arsonists'/'SHM', 'The Aurora transformed Nibb and Gyik from mischief-makers into feral marauders. They still don\'t know much about tool safety.').
card_multiverse_id('boggart arsonists'/'SHM', '158684').

card_in_set('boggart ram-gang', 'SHM').
card_original_type('boggart ram-gang'/'SHM', 'Creature — Goblin Warrior').
card_original_text('boggart ram-gang'/'SHM', 'Haste\nWither (This deals damage to creatures in the form of -1/-1 counters.)').
card_image_name('boggart ram-gang'/'SHM', 'boggart ram-gang').
card_uid('boggart ram-gang'/'SHM', 'SHM:Boggart Ram-Gang:boggart ram-gang').
card_rarity('boggart ram-gang'/'SHM', 'Uncommon').
card_artist('boggart ram-gang'/'SHM', 'Dave Allsop').
card_number('boggart ram-gang'/'SHM', '203').
card_flavor_text('boggart ram-gang'/'SHM', '\"We\'re going to need a bigger gate.\"\n—Bowen, Barrenton guardcaptain').
card_multiverse_id('boggart ram-gang'/'SHM', '153970').

card_in_set('boon reflection', 'SHM').
card_original_type('boon reflection'/'SHM', 'Enchantment').
card_original_text('boon reflection'/'SHM', 'If you would gain life, you gain twice that much life instead.').
card_first_print('boon reflection', 'SHM').
card_image_name('boon reflection'/'SHM', 'boon reflection').
card_uid('boon reflection'/'SHM', 'SHM:Boon Reflection:boon reflection').
card_rarity('boon reflection'/'SHM', 'Rare').
card_artist('boon reflection'/'SHM', 'Terese Nielsen & Ron Spencer').
card_number('boon reflection'/'SHM', '5').
card_flavor_text('boon reflection'/'SHM', 'Kithkin healers chant the clan songs of both their parents over the broth to double its curative effect.').
card_multiverse_id('boon reflection'/'SHM', '146751').

card_in_set('briarberry cohort', 'SHM').
card_original_type('briarberry cohort'/'SHM', 'Creature — Faerie Soldier').
card_original_text('briarberry cohort'/'SHM', 'Flying\nBriarberry Cohort gets +1/+1 as long as you control another blue creature.').
card_first_print('briarberry cohort', 'SHM').
card_image_name('briarberry cohort'/'SHM', 'briarberry cohort').
card_uid('briarberry cohort'/'SHM', 'SHM:Briarberry Cohort:briarberry cohort').
card_rarity('briarberry cohort'/'SHM', 'Common').
card_artist('briarberry cohort'/'SHM', 'Carl Critchlow').
card_number('briarberry cohort'/'SHM', '30').
card_flavor_text('briarberry cohort'/'SHM', 'A clique can cause far more mischief than one faerie on its own.').
card_multiverse_id('briarberry cohort'/'SHM', '146043').

card_in_set('burn trail', 'SHM').
card_original_type('burn trail'/'SHM', 'Sorcery').
card_original_text('burn trail'/'SHM', 'Burn Trail deals 3 damage to target creature or player.\nConspire (As you play this spell, you may tap two untapped creatures you control that share a color with it. When you do, copy it and you may choose a new target for the copy.)').
card_first_print('burn trail', 'SHM').
card_image_name('burn trail'/'SHM', 'burn trail').
card_uid('burn trail'/'SHM', 'SHM:Burn Trail:burn trail').
card_rarity('burn trail'/'SHM', 'Common').
card_artist('burn trail'/'SHM', 'Nils Hamm').
card_number('burn trail'/'SHM', '86').
card_multiverse_id('burn trail'/'SHM', '158688').

card_in_set('cauldron of souls', 'SHM').
card_original_type('cauldron of souls'/'SHM', 'Artifact').
card_original_text('cauldron of souls'/'SHM', '{T}: Choose any number of target creatures. Each of those creatures gains persist until end of turn. (When it\'s put into a graveyard from play, if it had no -1/-1 counters on it, return it to play under its owner\'s control with a -1/-1 counter on it.)').
card_first_print('cauldron of souls', 'SHM').
card_image_name('cauldron of souls'/'SHM', 'cauldron of souls').
card_uid('cauldron of souls'/'SHM', 'SHM:Cauldron of Souls:cauldron of souls').
card_rarity('cauldron of souls'/'SHM', 'Rare').
card_artist('cauldron of souls'/'SHM', 'Ron Brown').
card_number('cauldron of souls'/'SHM', '248').
card_multiverse_id('cauldron of souls'/'SHM', '152065').

card_in_set('cemetery puca', 'SHM').
card_original_type('cemetery puca'/'SHM', 'Creature — Shapeshifter').
card_original_text('cemetery puca'/'SHM', 'Whenever a creature is put into a graveyard from play, you may pay {1}. If you do, Cemetery Puca becomes a copy of that creature and gains this ability.').
card_first_print('cemetery puca', 'SHM').
card_image_name('cemetery puca'/'SHM', 'cemetery puca').
card_uid('cemetery puca'/'SHM', 'SHM:Cemetery Puca:cemetery puca').
card_rarity('cemetery puca'/'SHM', 'Rare').
card_artist('cemetery puca'/'SHM', 'Omar Rayyan').
card_number('cemetery puca'/'SHM', '158').
card_flavor_text('cemetery puca'/'SHM', '\"\'Tis a strange creature indeed that chooses the vanquished over the victor.\"\n—Awylla, elvish safewright').
card_multiverse_id('cemetery puca'/'SHM', '158767').

card_in_set('cerulean wisps', 'SHM').
card_original_type('cerulean wisps'/'SHM', 'Instant').
card_original_text('cerulean wisps'/'SHM', 'Target creature becomes blue until end of turn. Untap that creature.\nDraw a card.').
card_first_print('cerulean wisps', 'SHM').
card_image_name('cerulean wisps'/'SHM', 'cerulean wisps').
card_uid('cerulean wisps'/'SHM', 'SHM:Cerulean Wisps:cerulean wisps').
card_rarity('cerulean wisps'/'SHM', 'Common').
card_artist('cerulean wisps'/'SHM', 'Jim Nelson').
card_number('cerulean wisps'/'SHM', '31').
card_flavor_text('cerulean wisps'/'SHM', '\"If you see ghostly lights by the river, eat three twigs of marshroot to ward off kelpies.\"\n—Kithkin superstition').
card_multiverse_id('cerulean wisps'/'SHM', '158683').

card_in_set('chainbreaker', 'SHM').
card_original_type('chainbreaker'/'SHM', 'Artifact Creature — Scarecrow').
card_original_text('chainbreaker'/'SHM', 'Chainbreaker comes into play with two -1/-1 counters on it.\n{3}, {T}: Remove a -1/-1 counter from target creature.').
card_first_print('chainbreaker', 'SHM').
card_image_name('chainbreaker'/'SHM', 'chainbreaker').
card_uid('chainbreaker'/'SHM', 'SHM:Chainbreaker:chainbreaker').
card_rarity('chainbreaker'/'SHM', 'Common').
card_artist('chainbreaker'/'SHM', 'Jeff Miracola').
card_number('chainbreaker'/'SHM', '249').
card_flavor_text('chainbreaker'/'SHM', 'Scarecrows deemed too malevolent to roam free are shackled to boulders or dolmen stones. Their animosity usually outlives the chains.').
card_multiverse_id('chainbreaker'/'SHM', '147419').

card_in_set('cinderbones', 'SHM').
card_original_type('cinderbones'/'SHM', 'Creature — Elemental Skeleton').
card_original_text('cinderbones'/'SHM', 'Wither (This deals damage to creatures in the form of -1/-1 counters.)\n{1}{B}: Regenerate Cinderbones.').
card_first_print('cinderbones', 'SHM').
card_image_name('cinderbones'/'SHM', 'cinderbones').
card_uid('cinderbones'/'SHM', 'SHM:Cinderbones:cinderbones').
card_rarity('cinderbones'/'SHM', 'Common').
card_artist('cinderbones'/'SHM', 'Carl Critchlow').
card_number('cinderbones'/'SHM', '59').
card_flavor_text('cinderbones'/'SHM', 'Not all coals lie quietly in their beds of cold ash.').
card_multiverse_id('cinderbones'/'SHM', '142051').

card_in_set('cinderhaze wretch', 'SHM').
card_original_type('cinderhaze wretch'/'SHM', 'Creature — Elemental Shaman').
card_original_text('cinderhaze wretch'/'SHM', '{T}: Target player discards a card. Play this ability only during your turn.\nPut a -1/-1 counter on Cinderhaze Wretch: Untap Cinderhaze Wretch.').
card_first_print('cinderhaze wretch', 'SHM').
card_image_name('cinderhaze wretch'/'SHM', 'cinderhaze wretch').
card_uid('cinderhaze wretch'/'SHM', 'SHM:Cinderhaze Wretch:cinderhaze wretch').
card_rarity('cinderhaze wretch'/'SHM', 'Common').
card_artist('cinderhaze wretch'/'SHM', 'Steven Belledin').
card_number('cinderhaze wretch'/'SHM', '60').
card_flavor_text('cinderhaze wretch'/'SHM', 'Thick smoke billows and twists to form disturbing faces that cackle and click their teeth to the popping embers.').
card_multiverse_id('cinderhaze wretch'/'SHM', '135439').

card_in_set('consign to dream', 'SHM').
card_original_type('consign to dream'/'SHM', 'Instant').
card_original_text('consign to dream'/'SHM', 'Return target permanent to its owner\'s hand. If that permanent is red or green, put it on top of its owner\'s library instead.').
card_first_print('consign to dream', 'SHM').
card_image_name('consign to dream'/'SHM', 'consign to dream').
card_uid('consign to dream'/'SHM', 'SHM:Consign to Dream:consign to dream').
card_rarity('consign to dream'/'SHM', 'Common').
card_artist('consign to dream'/'SHM', 'Richard Kane Ferguson').
card_number('consign to dream'/'SHM', '32').
card_flavor_text('consign to dream'/'SHM', '\"Dreams are fleeting. Reality even more so.\"\n—Oona, queen of the fae').
card_multiverse_id('consign to dream'/'SHM', '142032').

card_in_set('corrosive mentor', 'SHM').
card_original_type('corrosive mentor'/'SHM', 'Creature — Elemental Rogue').
card_original_text('corrosive mentor'/'SHM', 'Black creatures you control have wither. (They deal damage to creatures in the form of -1/-1 counters.)').
card_first_print('corrosive mentor', 'SHM').
card_image_name('corrosive mentor'/'SHM', 'corrosive mentor').
card_uid('corrosive mentor'/'SHM', 'SHM:Corrosive Mentor:corrosive mentor').
card_rarity('corrosive mentor'/'SHM', 'Uncommon').
card_artist('corrosive mentor'/'SHM', 'Daarken').
card_number('corrosive mentor'/'SHM', '61').
card_flavor_text('corrosive mentor'/'SHM', 'Guttering cinders stoke their dying flames by snuffing out the lights of others.').
card_multiverse_id('corrosive mentor'/'SHM', '142034').

card_in_set('corrupt', 'SHM').
card_original_type('corrupt'/'SHM', 'Sorcery').
card_original_text('corrupt'/'SHM', 'Corrupt deals damage equal to the number of Swamps you control to target creature or player. You gain life equal to the damage dealt this way.').
card_image_name('corrupt'/'SHM', 'corrupt').
card_uid('corrupt'/'SHM', 'SHM:Corrupt:corrupt').
card_rarity('corrupt'/'SHM', 'Uncommon').
card_artist('corrupt'/'SHM', 'Dave Allsop').
card_number('corrupt'/'SHM', '62').
card_flavor_text('corrupt'/'SHM', 'There are things beneath the murk for whom the bog is a stew, a thin broth in need of meat.').
card_multiverse_id('corrupt'/'SHM', '146013').

card_in_set('counterbore', 'SHM').
card_original_type('counterbore'/'SHM', 'Instant').
card_original_text('counterbore'/'SHM', 'Counter target spell. Search its controller\'s graveyard, hand, and library for all cards with the same name as that spell and remove them from the game. Then that player shuffles his or her library.').
card_first_print('counterbore', 'SHM').
card_image_name('counterbore'/'SHM', 'counterbore').
card_uid('counterbore'/'SHM', 'SHM:Counterbore:counterbore').
card_rarity('counterbore'/'SHM', 'Rare').
card_artist('counterbore'/'SHM', 'Wayne England').
card_number('counterbore'/'SHM', '33').
card_multiverse_id('counterbore'/'SHM', '153966').

card_in_set('crabapple cohort', 'SHM').
card_original_type('crabapple cohort'/'SHM', 'Creature — Treefolk Warrior').
card_original_text('crabapple cohort'/'SHM', 'Crabapple Cohort gets +1/+1 as long as you control another green creature.').
card_first_print('crabapple cohort', 'SHM').
card_image_name('crabapple cohort'/'SHM', 'crabapple cohort').
card_uid('crabapple cohort'/'SHM', 'SHM:Crabapple Cohort:crabapple cohort').
card_rarity('crabapple cohort'/'SHM', 'Common').
card_artist('crabapple cohort'/'SHM', 'Richard Whitters').
card_number('crabapple cohort'/'SHM', '109').
card_flavor_text('crabapple cohort'/'SHM', 'Seven wives made seven pies from seven apples, each plucked from its branches. Now bare and bitter, it comes to exact its price: one apple, one bone.').
card_multiverse_id('crabapple cohort'/'SHM', '146009').

card_in_set('cragganwick cremator', 'SHM').
card_original_type('cragganwick cremator'/'SHM', 'Creature — Giant Shaman').
card_original_text('cragganwick cremator'/'SHM', 'When Cragganwick Cremator comes into play, discard a card at random. If you discard a creature card this way, Cragganwick Cremator deals damage equal to that card\'s power to target player.').
card_first_print('cragganwick cremator', 'SHM').
card_image_name('cragganwick cremator'/'SHM', 'cragganwick cremator').
card_uid('cragganwick cremator'/'SHM', 'SHM:Cragganwick Cremator:cragganwick cremator').
card_rarity('cragganwick cremator'/'SHM', 'Rare').
card_artist('cragganwick cremator'/'SHM', 'Jeremy Enecio').
card_number('cragganwick cremator'/'SHM', '87').
card_flavor_text('cragganwick cremator'/'SHM', 'Each night that his bowl fills, a village mourns.').
card_multiverse_id('cragganwick cremator'/'SHM', '159402').

card_in_set('crimson wisps', 'SHM').
card_original_type('crimson wisps'/'SHM', 'Instant').
card_original_text('crimson wisps'/'SHM', 'Target creature becomes red and gains haste until end of turn.\nDraw a card.').
card_first_print('crimson wisps', 'SHM').
card_image_name('crimson wisps'/'SHM', 'crimson wisps').
card_uid('crimson wisps'/'SHM', 'SHM:Crimson Wisps:crimson wisps').
card_rarity('crimson wisps'/'SHM', 'Common').
card_artist('crimson wisps'/'SHM', 'Jim Nelson').
card_number('crimson wisps'/'SHM', '88').
card_flavor_text('crimson wisps'/'SHM', 'The whispers are incomprehensible, yet cinders always hear a message of urgency.').
card_multiverse_id('crimson wisps'/'SHM', '146060').

card_in_set('crowd of cinders', 'SHM').
card_original_type('crowd of cinders'/'SHM', 'Creature — Elemental').
card_original_text('crowd of cinders'/'SHM', 'Fear\nCrowd of Cinders\'s power and toughness are each equal to the number of black permanents you control.').
card_first_print('crowd of cinders', 'SHM').
card_image_name('crowd of cinders'/'SHM', 'crowd of cinders').
card_uid('crowd of cinders'/'SHM', 'SHM:Crowd of Cinders:crowd of cinders').
card_rarity('crowd of cinders'/'SHM', 'Uncommon').
card_artist('crowd of cinders'/'SHM', 'Carl Frank').
card_number('crowd of cinders'/'SHM', '63').
card_flavor_text('crowd of cinders'/'SHM', 'They envy the life-giving heat so much that they tear it from those who still possess it.').
card_multiverse_id('crowd of cinders'/'SHM', '146051').

card_in_set('cultbrand cinder', 'SHM').
card_original_type('cultbrand cinder'/'SHM', 'Creature — Elemental Shaman').
card_original_text('cultbrand cinder'/'SHM', 'When Cultbrand Cinder comes into play, put a -1/-1 counter on target creature.').
card_first_print('cultbrand cinder', 'SHM').
card_image_name('cultbrand cinder'/'SHM', 'cultbrand cinder').
card_uid('cultbrand cinder'/'SHM', 'SHM:Cultbrand Cinder:cultbrand cinder').
card_rarity('cultbrand cinder'/'SHM', 'Common').
card_artist('cultbrand cinder'/'SHM', 'Christopher Moeller').
card_number('cultbrand cinder'/'SHM', '182').
card_flavor_text('cultbrand cinder'/'SHM', '\"Your seared flesh will be the first step in your journey to dark enlightenment.\"').
card_multiverse_id('cultbrand cinder'/'SHM', '142068').

card_in_set('curse of chains', 'SHM').
card_original_type('curse of chains'/'SHM', 'Enchantment — Aura').
card_original_text('curse of chains'/'SHM', 'Enchant creature\nAt the beginning of each upkeep, tap enchanted creature.').
card_first_print('curse of chains', 'SHM').
card_image_name('curse of chains'/'SHM', 'curse of chains').
card_uid('curse of chains'/'SHM', 'SHM:Curse of Chains:curse of chains').
card_rarity('curse of chains'/'SHM', 'Common').
card_artist('curse of chains'/'SHM', 'Drew Tucker').
card_number('curse of chains'/'SHM', '139').
card_flavor_text('curse of chains'/'SHM', 'The giant\'s real punishment was the fleeting moment when he was allowed to stand before being dragged down to his knees again.').
card_multiverse_id('curse of chains'/'SHM', '146079').

card_in_set('cursecatcher', 'SHM').
card_original_type('cursecatcher'/'SHM', 'Creature — Merfolk Wizard').
card_original_text('cursecatcher'/'SHM', 'Sacrifice Cursecatcher: Counter target instant or sorcery spell unless its controller pays {1}.').
card_first_print('cursecatcher', 'SHM').
card_image_name('cursecatcher'/'SHM', 'cursecatcher').
card_uid('cursecatcher'/'SHM', 'SHM:Cursecatcher:cursecatcher').
card_rarity('cursecatcher'/'SHM', 'Uncommon').
card_artist('cursecatcher'/'SHM', 'Warren Mahy').
card_number('cursecatcher'/'SHM', '34').
card_flavor_text('cursecatcher'/'SHM', '\"Of all the things that can be stolen, opportunity is the most valuable.\"').
card_multiverse_id('cursecatcher'/'SHM', '158763').

card_in_set('dawnglow infusion', 'SHM').
card_original_type('dawnglow infusion'/'SHM', 'Sorcery').
card_original_text('dawnglow infusion'/'SHM', 'You gain X life if {G} was spent to play Dawnglow Infusion and X life if {W} was spent to play it. (Do both if {G}{W} was spent.)').
card_first_print('dawnglow infusion', 'SHM').
card_image_name('dawnglow infusion'/'SHM', 'dawnglow infusion').
card_uid('dawnglow infusion'/'SHM', 'SHM:Dawnglow Infusion:dawnglow infusion').
card_rarity('dawnglow infusion'/'SHM', 'Uncommon').
card_artist('dawnglow infusion'/'SHM', 'Rebecca Guay').
card_number('dawnglow infusion'/'SHM', '225').
card_flavor_text('dawnglow infusion'/'SHM', 'Dawnglove bloom is a potent nostrum, but the dawnglow brewed from its sap is nothing less than life itself.').
card_multiverse_id('dawnglow infusion'/'SHM', '158747').

card_in_set('deep-slumber titan', 'SHM').
card_original_type('deep-slumber titan'/'SHM', 'Creature — Giant Warrior').
card_original_text('deep-slumber titan'/'SHM', 'Deep-Slumber Titan comes into play tapped.\nDeep-Slumber Titan doesn\'t untap during your untap step.\nWhenever Deep-Slumber Titan is dealt damage, untap it.').
card_first_print('deep-slumber titan', 'SHM').
card_image_name('deep-slumber titan'/'SHM', 'deep-slumber titan').
card_uid('deep-slumber titan'/'SHM', 'SHM:Deep-Slumber Titan:deep-slumber titan').
card_rarity('deep-slumber titan'/'SHM', 'Rare').
card_artist('deep-slumber titan'/'SHM', 'Steve Prescott').
card_number('deep-slumber titan'/'SHM', '89').
card_flavor_text('deep-slumber titan'/'SHM', 'Do not disturb.').
card_multiverse_id('deep-slumber titan'/'SHM', '159396').

card_in_set('deepchannel mentor', 'SHM').
card_original_type('deepchannel mentor'/'SHM', 'Creature — Merfolk Rogue').
card_original_text('deepchannel mentor'/'SHM', 'Blue creatures you control are unblockable.').
card_first_print('deepchannel mentor', 'SHM').
card_image_name('deepchannel mentor'/'SHM', 'deepchannel mentor').
card_uid('deepchannel mentor'/'SHM', 'SHM:Deepchannel Mentor:deepchannel mentor').
card_rarity('deepchannel mentor'/'SHM', 'Uncommon').
card_artist('deepchannel mentor'/'SHM', 'Jesper Ejsing').
card_number('deepchannel mentor'/'SHM', '35').
card_flavor_text('deepchannel mentor'/'SHM', 'The rivers can no longer provide safe passage for travelers and commerce. They serve only as highways for raiders and channels for blood and woe.').
card_multiverse_id('deepchannel mentor'/'SHM', '141981').

card_in_set('demigod of revenge', 'SHM').
card_original_type('demigod of revenge'/'SHM', 'Creature — Spirit Avatar').
card_original_text('demigod of revenge'/'SHM', 'Flying, haste\nWhen you play Demigod of Revenge, return all cards named Demigod of Revenge from your graveyard to play.').
card_image_name('demigod of revenge'/'SHM', 'demigod of revenge').
card_uid('demigod of revenge'/'SHM', 'SHM:Demigod of Revenge:demigod of revenge').
card_rarity('demigod of revenge'/'SHM', 'Rare').
card_artist('demigod of revenge'/'SHM', 'Jim Murray').
card_number('demigod of revenge'/'SHM', '183').
card_flavor_text('demigod of revenge'/'SHM', '\"His laugh, a bellowing, deathly din, slices through the heavens, making them bleed.\"\n—The Seer\'s Parables').
card_multiverse_id('demigod of revenge'/'SHM', '153972').

card_in_set('deus of calamity', 'SHM').
card_original_type('deus of calamity'/'SHM', 'Creature — Spirit Avatar').
card_original_text('deus of calamity'/'SHM', 'Trample\nWhenever Deus of Calamity deals 6 or more damage to an opponent, destroy target land that player controls.').
card_first_print('deus of calamity', 'SHM').
card_image_name('deus of calamity'/'SHM', 'deus of calamity').
card_uid('deus of calamity'/'SHM', 'SHM:Deus of Calamity:deus of calamity').
card_rarity('deus of calamity'/'SHM', 'Rare').
card_artist('deus of calamity'/'SHM', 'Todd Lockwood').
card_number('deus of calamity'/'SHM', '204').
card_flavor_text('deus of calamity'/'SHM', '\"He bears the marks of ages upon his skin, memories of dreams long dead and best left buried.\"\n—The Seer\'s Parables').
card_multiverse_id('deus of calamity'/'SHM', '146773').

card_in_set('devoted druid', 'SHM').
card_original_type('devoted druid'/'SHM', 'Creature — Elf Druid').
card_original_text('devoted druid'/'SHM', '{T}: Add {G} to your mana pool.\nPut a -1/-1 counter on Devoted Druid: Untap Devoted Druid.').
card_first_print('devoted druid', 'SHM').
card_image_name('devoted druid'/'SHM', 'devoted druid').
card_uid('devoted druid'/'SHM', 'SHM:Devoted Druid:devoted druid').
card_rarity('devoted druid'/'SHM', 'Common').
card_artist('devoted druid'/'SHM', 'Darrell Riche').
card_number('devoted druid'/'SHM', '110').
card_flavor_text('devoted druid'/'SHM', '\"Mana lurks in all things, even in the depths of one\'s own heart.\"').
card_multiverse_id('devoted druid'/'SHM', '135500').

card_in_set('din of the fireherd', 'SHM').
card_original_type('din of the fireherd'/'SHM', 'Sorcery').
card_original_text('din of the fireherd'/'SHM', 'Put a 5/5 black and red Elemental creature token into play. Target opponent sacrifices a creature for each black creature you control, then sacrifices a land for each red creature you control.').
card_first_print('din of the fireherd', 'SHM').
card_image_name('din of the fireherd'/'SHM', 'din of the fireherd').
card_uid('din of the fireherd'/'SHM', 'SHM:Din of the Fireherd:din of the fireherd').
card_rarity('din of the fireherd'/'SHM', 'Rare').
card_artist('din of the fireherd'/'SHM', 'Dave Dorman').
card_number('din of the fireherd'/'SHM', '184').
card_flavor_text('din of the fireherd'/'SHM', 'Vengeance in stampede form.').
card_multiverse_id('din of the fireherd'/'SHM', '146733').

card_in_set('dire undercurrents', 'SHM').
card_original_type('dire undercurrents'/'SHM', 'Enchantment').
card_original_text('dire undercurrents'/'SHM', 'Whenever a blue creature comes into play under your control, you may have target player draw a card.\nWhenever a black creature comes into play under your control, you may have target player discard a card.').
card_first_print('dire undercurrents', 'SHM').
card_image_name('dire undercurrents'/'SHM', 'dire undercurrents').
card_uid('dire undercurrents'/'SHM', 'SHM:Dire Undercurrents:dire undercurrents').
card_rarity('dire undercurrents'/'SHM', 'Rare').
card_artist('dire undercurrents'/'SHM', 'Franz Vohwinkel').
card_number('dire undercurrents'/'SHM', '159').
card_multiverse_id('dire undercurrents'/'SHM', '153311').

card_in_set('disturbing plot', 'SHM').
card_original_type('disturbing plot'/'SHM', 'Sorcery').
card_original_text('disturbing plot'/'SHM', 'Return target creature card in a graveyard to its owner\'s hand.\nConspire (As you play this spell, you may tap two untapped creatures you control that share a color with it. When you do, copy it and you may choose a new target for the copy.)').
card_first_print('disturbing plot', 'SHM').
card_image_name('disturbing plot'/'SHM', 'disturbing plot').
card_uid('disturbing plot'/'SHM', 'SHM:Disturbing Plot:disturbing plot').
card_rarity('disturbing plot'/'SHM', 'Common').
card_artist('disturbing plot'/'SHM', 'Larry MacDougall').
card_number('disturbing plot'/'SHM', '64').
card_multiverse_id('disturbing plot'/'SHM', '158689').

card_in_set('dramatic entrance', 'SHM').
card_original_type('dramatic entrance'/'SHM', 'Instant').
card_original_text('dramatic entrance'/'SHM', 'You may put a green creature card from your hand into play.').
card_first_print('dramatic entrance', 'SHM').
card_image_name('dramatic entrance'/'SHM', 'dramatic entrance').
card_uid('dramatic entrance'/'SHM', 'SHM:Dramatic Entrance:dramatic entrance').
card_rarity('dramatic entrance'/'SHM', 'Rare').
card_artist('dramatic entrance'/'SHM', 'Mike Dringenberg').
card_number('dramatic entrance'/'SHM', '111').
card_flavor_text('dramatic entrance'/'SHM', 'Some things aren\'t worth waiting for.').
card_multiverse_id('dramatic entrance'/'SHM', '154003').

card_in_set('dream salvage', 'SHM').
card_original_type('dream salvage'/'SHM', 'Instant').
card_original_text('dream salvage'/'SHM', 'Draw cards equal to the number of cards target opponent discarded this turn.').
card_first_print('dream salvage', 'SHM').
card_image_name('dream salvage'/'SHM', 'dream salvage').
card_uid('dream salvage'/'SHM', 'SHM:Dream Salvage:dream salvage').
card_rarity('dream salvage'/'SHM', 'Uncommon').
card_artist('dream salvage'/'SHM', 'Howard Lyon').
card_number('dream salvage'/'SHM', '160').
card_flavor_text('dream salvage'/'SHM', 'Awylla tried hard to forget the horrors she had seen, but someone else had developed a keen interest in them.').
card_multiverse_id('dream salvage'/'SHM', '158774').

card_in_set('drove of elves', 'SHM').
card_original_type('drove of elves'/'SHM', 'Creature — Elf').
card_original_text('drove of elves'/'SHM', 'Drove of Elves\'s power and toughness are each equal to the number of green permanents you control.\nDrove of Elves can\'t be the target of spells or abilities your opponents control.').
card_first_print('drove of elves', 'SHM').
card_image_name('drove of elves'/'SHM', 'drove of elves').
card_uid('drove of elves'/'SHM', 'SHM:Drove of Elves:drove of elves').
card_rarity('drove of elves'/'SHM', 'Uncommon').
card_artist('drove of elves'/'SHM', 'Larry MacDougall').
card_number('drove of elves'/'SHM', '112').
card_flavor_text('drove of elves'/'SHM', '\"The light of beauty protects our journeys through darkness.\"').
card_multiverse_id('drove of elves'/'SHM', '157878').

card_in_set('drowner initiate', 'SHM').
card_original_type('drowner initiate'/'SHM', 'Creature — Merfolk Wizard').
card_original_text('drowner initiate'/'SHM', 'Whenever a player plays a blue spell, you may pay {1}. If you do, target player puts the top two cards of his or her library into his or her graveyard.').
card_first_print('drowner initiate', 'SHM').
card_image_name('drowner initiate'/'SHM', 'drowner initiate').
card_uid('drowner initiate'/'SHM', 'SHM:Drowner Initiate:drowner initiate').
card_rarity('drowner initiate'/'SHM', 'Common').
card_artist('drowner initiate'/'SHM', 'E. M. Gist').
card_number('drowner initiate'/'SHM', '36').
card_flavor_text('drowner initiate'/'SHM', 'Nothing satisfies a merrow as much as drowning what it cannot have.').
card_multiverse_id('drowner initiate'/'SHM', '142005').

card_in_set('dusk urchins', 'SHM').
card_original_type('dusk urchins'/'SHM', 'Creature — Ouphe').
card_original_text('dusk urchins'/'SHM', 'Whenever Dusk Urchins attacks or blocks, put a -1/-1 counter on it.\nWhen Dusk Urchins is put into a graveyard from play, draw a card for each -1/-1 counter on it.').
card_first_print('dusk urchins', 'SHM').
card_image_name('dusk urchins'/'SHM', 'dusk urchins').
card_uid('dusk urchins'/'SHM', 'SHM:Dusk Urchins:dusk urchins').
card_rarity('dusk urchins'/'SHM', 'Rare').
card_artist('dusk urchins'/'SHM', 'Darrell Riche').
card_number('dusk urchins'/'SHM', '65').
card_flavor_text('dusk urchins'/'SHM', 'They spawn from shadow, wreaking chaos until shadow claims them again.').
card_multiverse_id('dusk urchins'/'SHM', '146002').

card_in_set('elemental mastery', 'SHM').
card_original_type('elemental mastery'/'SHM', 'Enchantment — Aura').
card_original_text('elemental mastery'/'SHM', 'Enchant creature\nEnchanted creature has \"{T}: Put X 1/1 red Elemental creature tokens with haste into play, where X is this creature\'s power. Remove them from the game at end of turn.\"').
card_first_print('elemental mastery', 'SHM').
card_image_name('elemental mastery'/'SHM', 'elemental mastery').
card_uid('elemental mastery'/'SHM', 'SHM:Elemental Mastery:elemental mastery').
card_rarity('elemental mastery'/'SHM', 'Rare').
card_artist('elemental mastery'/'SHM', 'Cyril Van Der Haegen').
card_number('elemental mastery'/'SHM', '90').
card_multiverse_id('elemental mastery'/'SHM', '146087').

card_in_set('elsewhere flask', 'SHM').
card_original_type('elsewhere flask'/'SHM', 'Artifact').
card_original_text('elsewhere flask'/'SHM', 'When Elsewhere Flask comes into play, draw a card.\nSacrifice Elsewhere Flask: Choose a basic land type. Each land you control becomes that type until end of turn.').
card_first_print('elsewhere flask', 'SHM').
card_image_name('elsewhere flask'/'SHM', 'elsewhere flask').
card_uid('elsewhere flask'/'SHM', 'SHM:Elsewhere Flask:elsewhere flask').
card_rarity('elsewhere flask'/'SHM', 'Common').
card_artist('elsewhere flask'/'SHM', 'Carl Frank').
card_number('elsewhere flask'/'SHM', '250').
card_flavor_text('elsewhere flask'/'SHM', 'One night Oona bottled the sky. The next night she aligned the stars and released it.').
card_multiverse_id('elsewhere flask'/'SHM', '142004').

card_in_set('elvish hexhunter', 'SHM').
card_original_type('elvish hexhunter'/'SHM', 'Creature — Elf Shaman').
card_original_text('elvish hexhunter'/'SHM', '{G/W}, {T}, Sacrifice Elvish Hexhunter: Destroy target enchantment.').
card_first_print('elvish hexhunter', 'SHM').
card_image_name('elvish hexhunter'/'SHM', 'elvish hexhunter').
card_uid('elvish hexhunter'/'SHM', 'SHM:Elvish Hexhunter:elvish hexhunter').
card_rarity('elvish hexhunter'/'SHM', 'Common').
card_artist('elvish hexhunter'/'SHM', 'Alex Horley-Orlandelli').
card_number('elvish hexhunter'/'SHM', '226').
card_flavor_text('elvish hexhunter'/'SHM', '\"All manner of curses, blights, and luckspoils infect Shadowmoor. We stalk them, one perilous quest at a time.\"').
card_multiverse_id('elvish hexhunter'/'SHM', '141958').

card_in_set('ember gale', 'SHM').
card_original_type('ember gale'/'SHM', 'Sorcery').
card_original_text('ember gale'/'SHM', 'Creatures target player controls can\'t block this turn. Ember Gale deals 1 damage to each white and/or blue creature that player controls.').
card_first_print('ember gale', 'SHM').
card_image_name('ember gale'/'SHM', 'ember gale').
card_uid('ember gale'/'SHM', 'SHM:Ember Gale:ember gale').
card_rarity('ember gale'/'SHM', 'Common').
card_artist('ember gale'/'SHM', 'Steve Prescott').
card_number('ember gale'/'SHM', '91').
card_flavor_text('ember gale'/'SHM', '\"Terrible wind. It whipped sparks into our skin, then stoked them even hotter!\"\n—Bowen, Barrenton guardcaptain').
card_multiverse_id('ember gale'/'SHM', '158766').

card_in_set('emberstrike duo', 'SHM').
card_original_type('emberstrike duo'/'SHM', 'Creature — Elemental Warrior Shaman').
card_original_text('emberstrike duo'/'SHM', 'Whenever you play a black spell, Emberstrike Duo gets +1/+1 until end of turn.\nWhenever you play a red spell, Emberstrike Duo gains first strike until end of turn.').
card_first_print('emberstrike duo', 'SHM').
card_image_name('emberstrike duo'/'SHM', 'emberstrike duo').
card_uid('emberstrike duo'/'SHM', 'SHM:Emberstrike Duo:emberstrike duo').
card_rarity('emberstrike duo'/'SHM', 'Common').
card_artist('emberstrike duo'/'SHM', 'Aleksi Briclot').
card_number('emberstrike duo'/'SHM', '185').
card_multiverse_id('emberstrike duo'/'SHM', '153307').

card_in_set('enchanted evening', 'SHM').
card_original_type('enchanted evening'/'SHM', 'Enchantment').
card_original_text('enchanted evening'/'SHM', 'All permanents are enchantments in addition to their other types.').
card_first_print('enchanted evening', 'SHM').
card_image_name('enchanted evening'/'SHM', 'enchanted evening').
card_uid('enchanted evening'/'SHM', 'SHM:Enchanted Evening:enchanted evening').
card_rarity('enchanted evening'/'SHM', 'Rare').
card_artist('enchanted evening'/'SHM', 'Rebecca Guay').
card_number('enchanted evening'/'SHM', '140').
card_flavor_text('enchanted evening'/'SHM', 'In the reflection, she saw the rough shell of the world sloughing away, revealing the truth shining underneath.').
card_multiverse_id('enchanted evening'/'SHM', '151979').

card_in_set('everlasting torment', 'SHM').
card_original_type('everlasting torment'/'SHM', 'Enchantment').
card_original_text('everlasting torment'/'SHM', 'Players can\'t gain life.\nDamage can\'t be prevented.\nAll damage is dealt as though its source had wither. (A source with wither deals damage to creatures in the form of -1/-1 counters.)').
card_first_print('everlasting torment', 'SHM').
card_image_name('everlasting torment'/'SHM', 'everlasting torment').
card_uid('everlasting torment'/'SHM', 'SHM:Everlasting Torment:everlasting torment').
card_rarity('everlasting torment'/'SHM', 'Rare').
card_artist('everlasting torment'/'SHM', 'Richard Kane Ferguson').
card_number('everlasting torment'/'SHM', '186').
card_flavor_text('everlasting torment'/'SHM', 'When night fell, the plane itself was scarred.').
card_multiverse_id('everlasting torment'/'SHM', '142036').

card_in_set('faerie macabre', 'SHM').
card_original_type('faerie macabre'/'SHM', 'Creature — Faerie Rogue').
card_original_text('faerie macabre'/'SHM', 'Flying\nDiscard Faerie Macabre: Remove up to two target cards in graveyards from the game.').
card_first_print('faerie macabre', 'SHM').
card_image_name('faerie macabre'/'SHM', 'faerie macabre').
card_uid('faerie macabre'/'SHM', 'SHM:Faerie Macabre:faerie macabre').
card_rarity('faerie macabre'/'SHM', 'Common').
card_artist('faerie macabre'/'SHM', 'rk post').
card_number('faerie macabre'/'SHM', '66').
card_flavor_text('faerie macabre'/'SHM', 'The line between dream and death is gauzy and fragile. She leads those too near it from one side to the other.').
card_multiverse_id('faerie macabre'/'SHM', '153964').

card_in_set('faerie swarm', 'SHM').
card_original_type('faerie swarm'/'SHM', 'Creature — Faerie').
card_original_text('faerie swarm'/'SHM', 'Flying\nFaerie Swarm\'s power and toughness are each equal to the number of blue permanents you control.').
card_first_print('faerie swarm', 'SHM').
card_image_name('faerie swarm'/'SHM', 'faerie swarm').
card_uid('faerie swarm'/'SHM', 'SHM:Faerie Swarm:faerie swarm').
card_rarity('faerie swarm'/'SHM', 'Uncommon').
card_artist('faerie swarm'/'SHM', 'Zoltan Boros & Gabor Szikszai').
card_number('faerie swarm'/'SHM', '37').
card_flavor_text('faerie swarm'/'SHM', 'Untouched by the Aurora, Oona\'s faeries greeted the night like any other day.').
card_multiverse_id('faerie swarm'/'SHM', '158685').

card_in_set('farhaven elf', 'SHM').
card_original_type('farhaven elf'/'SHM', 'Creature — Elf Druid').
card_original_text('farhaven elf'/'SHM', 'When Farhaven Elf comes into play, you may search your library for a basic land card and put it into play tapped. If you do, shuffle your library.').
card_first_print('farhaven elf', 'SHM').
card_image_name('farhaven elf'/'SHM', 'farhaven elf').
card_uid('farhaven elf'/'SHM', 'SHM:Farhaven Elf:farhaven elf').
card_rarity('farhaven elf'/'SHM', 'Common').
card_artist('farhaven elf'/'SHM', 'Brandon Kitkouski').
card_number('farhaven elf'/'SHM', '113').
card_flavor_text('farhaven elf'/'SHM', '\"Verdant bloom does exist. It merely hides for its own safety.\"').
card_multiverse_id('farhaven elf'/'SHM', '153289').

card_in_set('fate transfer', 'SHM').
card_original_type('fate transfer'/'SHM', 'Instant').
card_original_text('fate transfer'/'SHM', 'Move all counters from target creature onto another target creature.').
card_first_print('fate transfer', 'SHM').
card_image_name('fate transfer'/'SHM', 'fate transfer').
card_uid('fate transfer'/'SHM', 'SHM:Fate Transfer:fate transfer').
card_rarity('fate transfer'/'SHM', 'Common').
card_artist('fate transfer'/'SHM', 'Wayne England').
card_number('fate transfer'/'SHM', '161').
card_flavor_text('fate transfer'/'SHM', '\"The cinders find strength in suffering, the elves in beauty. But if you make an elf suffer, he gets weaker. I wonder what happens if you make a cinder beautiful?\"\n—Hrugash, leech bonder').
card_multiverse_id('fate transfer'/'SHM', '147411').

card_in_set('fire-lit thicket', 'SHM').
card_original_type('fire-lit thicket'/'SHM', 'Land').
card_original_text('fire-lit thicket'/'SHM', '{T}: Add {1} to your mana pool.\n{R/G}, {T}: Add {R}{R}, {R}{G}, or {G}{G} to your mana pool.').
card_first_print('fire-lit thicket', 'SHM').
card_image_name('fire-lit thicket'/'SHM', 'fire-lit thicket').
card_uid('fire-lit thicket'/'SHM', 'SHM:Fire-Lit Thicket:fire-lit thicket').
card_rarity('fire-lit thicket'/'SHM', 'Rare').
card_artist('fire-lit thicket'/'SHM', 'Ralph Horsley').
card_number('fire-lit thicket'/'SHM', '271').
card_flavor_text('fire-lit thicket'/'SHM', 'Occupancy of the den changes from night to night as boggart gangs fight for control of a place they aren\'t willing to simply eat.').
card_multiverse_id('fire-lit thicket'/'SHM', '146753').

card_in_set('firespout', 'SHM').
card_original_type('firespout'/'SHM', 'Sorcery').
card_original_text('firespout'/'SHM', 'Firespout deals 3 damage to each creature without flying if {R} was spent to play Firespout and 3 damage to each creature with flying if {G} was spent to play it. (Do both if {R}{G} was spent.)').
card_first_print('firespout', 'SHM').
card_image_name('firespout'/'SHM', 'firespout').
card_uid('firespout'/'SHM', 'SHM:Firespout:firespout').
card_rarity('firespout'/'SHM', 'Uncommon').
card_artist('firespout'/'SHM', 'Jeff Miracola').
card_number('firespout'/'SHM', '205').
card_multiverse_id('firespout'/'SHM', '153314').

card_in_set('fists of the demigod', 'SHM').
card_original_type('fists of the demigod'/'SHM', 'Enchantment — Aura').
card_original_text('fists of the demigod'/'SHM', 'Enchant creature\nAs long as enchanted creature is black, it gets +1/+1 and has wither. (It deals damage to creatures in the form of -1/-1 counters.)\nAs long as enchanted creature is red, it gets +1/+1 and has first strike.').
card_first_print('fists of the demigod', 'SHM').
card_image_name('fists of the demigod'/'SHM', 'fists of the demigod').
card_uid('fists of the demigod'/'SHM', 'SHM:Fists of the Demigod:fists of the demigod').
card_rarity('fists of the demigod'/'SHM', 'Common').
card_artist('fists of the demigod'/'SHM', 'Dan Scott').
card_number('fists of the demigod'/'SHM', '187').
card_multiverse_id('fists of the demigod'/'SHM', '158753').

card_in_set('flame javelin', 'SHM').
card_original_type('flame javelin'/'SHM', 'Instant').
card_original_text('flame javelin'/'SHM', '({2/R} can be paid with any two mana or with {R}. This card\'s converted mana cost is 6.)\nFlame Javelin deals 4 damage to target creature or player.').
card_image_name('flame javelin'/'SHM', 'flame javelin').
card_uid('flame javelin'/'SHM', 'SHM:Flame Javelin:flame javelin').
card_rarity('flame javelin'/'SHM', 'Uncommon').
card_artist('flame javelin'/'SHM', 'Trevor Hairsine').
card_number('flame javelin'/'SHM', '92').
card_flavor_text('flame javelin'/'SHM', 'Gyara Spearhurler would have been renowned for her deadly accuracy, if it weren\'t for her deadly accuracy.').
card_multiverse_id('flame javelin'/'SHM', '146017').

card_in_set('flourishing defenses', 'SHM').
card_original_type('flourishing defenses'/'SHM', 'Enchantment').
card_original_text('flourishing defenses'/'SHM', 'Whenever a -1/-1 counter is placed on a creature, you may put a 1/1 green Elf Warrior creature token into play.').
card_first_print('flourishing defenses', 'SHM').
card_image_name('flourishing defenses'/'SHM', 'flourishing defenses').
card_uid('flourishing defenses'/'SHM', 'SHM:Flourishing Defenses:flourishing defenses').
card_rarity('flourishing defenses'/'SHM', 'Uncommon').
card_artist('flourishing defenses'/'SHM', 'Dan Scott').
card_number('flourishing defenses'/'SHM', '114').
card_flavor_text('flourishing defenses'/'SHM', 'Many horrors take the bait of brave elf volunteers walking alone, only to fall to the blades of rescuers lurking nearby.').
card_multiverse_id('flourishing defenses'/'SHM', '158764').

card_in_set('flow of ideas', 'SHM').
card_original_type('flow of ideas'/'SHM', 'Sorcery').
card_original_text('flow of ideas'/'SHM', 'Draw a card for each Island you control.').
card_image_name('flow of ideas'/'SHM', 'flow of ideas').
card_uid('flow of ideas'/'SHM', 'SHM:Flow of Ideas:flow of ideas').
card_rarity('flow of ideas'/'SHM', 'Uncommon').
card_artist('flow of ideas'/'SHM', 'Mike Dringenberg').
card_number('flow of ideas'/'SHM', '38').
card_flavor_text('flow of ideas'/'SHM', '\"Beware the merrows, whose envy and covetousness surpasses even that of the ravenous whirlpools where they dwell.\"\n—The Book of Other Folk').
card_multiverse_id('flow of ideas'/'SHM', '158692').

card_in_set('forest', 'SHM').
card_original_type('forest'/'SHM', 'Basic Land — Forest').
card_original_text('forest'/'SHM', 'G').
card_image_name('forest'/'SHM', 'forest1').
card_uid('forest'/'SHM', 'SHM:Forest:forest1').
card_rarity('forest'/'SHM', 'Basic Land').
card_artist('forest'/'SHM', 'Larry MacDougall').
card_number('forest'/'SHM', '298').
card_multiverse_id('forest'/'SHM', '157877').

card_in_set('forest', 'SHM').
card_original_type('forest'/'SHM', 'Basic Land — Forest').
card_original_text('forest'/'SHM', 'G').
card_image_name('forest'/'SHM', 'forest2').
card_uid('forest'/'SHM', 'SHM:Forest:forest2').
card_rarity('forest'/'SHM', 'Basic Land').
card_artist('forest'/'SHM', 'Rob Alexander').
card_number('forest'/'SHM', '299').
card_multiverse_id('forest'/'SHM', '157870').

card_in_set('forest', 'SHM').
card_original_type('forest'/'SHM', 'Basic Land — Forest').
card_original_text('forest'/'SHM', 'G').
card_image_name('forest'/'SHM', 'forest3').
card_uid('forest'/'SHM', 'SHM:Forest:forest3').
card_rarity('forest'/'SHM', 'Basic Land').
card_artist('forest'/'SHM', 'Chippy').
card_number('forest'/'SHM', '300').
card_multiverse_id('forest'/'SHM', '158242').

card_in_set('forest', 'SHM').
card_original_type('forest'/'SHM', 'Basic Land — Forest').
card_original_text('forest'/'SHM', 'G').
card_image_name('forest'/'SHM', 'forest4').
card_uid('forest'/'SHM', 'SHM:Forest:forest4').
card_rarity('forest'/'SHM', 'Basic Land').
card_artist('forest'/'SHM', 'Steve Prescott').
card_number('forest'/'SHM', '301').
card_multiverse_id('forest'/'SHM', '158241').

card_in_set('fossil find', 'SHM').
card_original_type('fossil find'/'SHM', 'Sorcery').
card_original_text('fossil find'/'SHM', 'Return a card at random from your graveyard to your hand, then reorder your graveyard as you choose.').
card_first_print('fossil find', 'SHM').
card_image_name('fossil find'/'SHM', 'fossil find').
card_uid('fossil find'/'SHM', 'SHM:Fossil Find:fossil find').
card_rarity('fossil find'/'SHM', 'Uncommon').
card_artist('fossil find'/'SHM', 'Dave DeVries').
card_number('fossil find'/'SHM', '206').
card_flavor_text('fossil find'/'SHM', '\"Some secrets want to be revealed. They burst through the soil of their own accord.\"\n—Dindun of Kulrath Mine').
card_multiverse_id('fossil find'/'SHM', '146736').

card_in_set('foxfire oak', 'SHM').
card_original_type('foxfire oak'/'SHM', 'Creature — Treefolk Shaman').
card_original_text('foxfire oak'/'SHM', '{R/G}{R/G}{R/G}: Foxfire Oak gets +3/+0 until end of turn.').
card_first_print('foxfire oak', 'SHM').
card_image_name('foxfire oak'/'SHM', 'foxfire oak').
card_uid('foxfire oak'/'SHM', 'SHM:Foxfire Oak:foxfire oak').
card_rarity('foxfire oak'/'SHM', 'Common').
card_artist('foxfire oak'/'SHM', 'Dave Kendall').
card_number('foxfire oak'/'SHM', '115').
card_flavor_text('foxfire oak'/'SHM', '\"The brethren shall blaze with unnatural fire, and the flame shall consume and purify our rage.\"\n—Treefolk catastrophe myth').
card_multiverse_id('foxfire oak'/'SHM', '141998').

card_in_set('fracturing gust', 'SHM').
card_original_type('fracturing gust'/'SHM', 'Instant').
card_original_text('fracturing gust'/'SHM', 'Destroy all artifacts and enchantments. You gain 2 life for each permanent destroyed this way.').
card_first_print('fracturing gust', 'SHM').
card_image_name('fracturing gust'/'SHM', 'fracturing gust').
card_uid('fracturing gust'/'SHM', 'SHM:Fracturing Gust:fracturing gust').
card_rarity('fracturing gust'/'SHM', 'Rare').
card_artist('fracturing gust'/'SHM', 'Michael Sutfin').
card_number('fracturing gust'/'SHM', '227').
card_flavor_text('fracturing gust'/'SHM', 'Elvish dawnhands test a relic\'s worthiness before collecting it for the safehold. If it can\'t stand up to a stiff breeze, it\'s left behind.').
card_multiverse_id('fracturing gust'/'SHM', '146759').

card_in_set('fulminator mage', 'SHM').
card_original_type('fulminator mage'/'SHM', 'Creature — Elemental Shaman').
card_original_text('fulminator mage'/'SHM', 'Sacrifice Fulminator Mage: Destroy target nonbasic land.').
card_first_print('fulminator mage', 'SHM').
card_image_name('fulminator mage'/'SHM', 'fulminator mage').
card_uid('fulminator mage'/'SHM', 'SHM:Fulminator Mage:fulminator mage').
card_rarity('fulminator mage'/'SHM', 'Rare').
card_artist('fulminator mage'/'SHM', 'rk post').
card_number('fulminator mage'/'SHM', '188').
card_flavor_text('fulminator mage'/'SHM', '\"Unsafe Terrain Ahead—Turn Back\"\n—Sign near the former location of Pyrtagh Cairn').
card_multiverse_id('fulminator mage'/'SHM', '142009').

card_in_set('furystoke giant', 'SHM').
card_original_type('furystoke giant'/'SHM', 'Creature — Giant Warrior').
card_original_text('furystoke giant'/'SHM', 'When Furystoke Giant comes into play, other creatures you control gain \"{T}: This creature deals 2 damage to target creature or player\" until end of turn.\nPersist (When this creature is put into a graveyard from play, if it had no -1/-1 counters on it, return it to play under its owner\'s control with a -1/-1 counter on it.)').
card_first_print('furystoke giant', 'SHM').
card_image_name('furystoke giant'/'SHM', 'furystoke giant').
card_uid('furystoke giant'/'SHM', 'SHM:Furystoke Giant:furystoke giant').
card_rarity('furystoke giant'/'SHM', 'Rare').
card_artist('furystoke giant'/'SHM', 'Ralph Horsley').
card_number('furystoke giant'/'SHM', '93').
card_multiverse_id('furystoke giant'/'SHM', '152069').

card_in_set('ghastlord of fugue', 'SHM').
card_original_type('ghastlord of fugue'/'SHM', 'Creature — Spirit Avatar').
card_original_text('ghastlord of fugue'/'SHM', 'Ghastlord of Fugue is unblockable.\nWhenever Ghastlord of Fugue deals combat damage to a player, that player reveals his or her hand. Choose a card from it. That player removes that card from the game.').
card_first_print('ghastlord of fugue', 'SHM').
card_image_name('ghastlord of fugue'/'SHM', 'ghastlord of fugue').
card_uid('ghastlord of fugue'/'SHM', 'SHM:Ghastlord of Fugue:ghastlord of fugue').
card_rarity('ghastlord of fugue'/'SHM', 'Rare').
card_artist('ghastlord of fugue'/'SHM', 'Mike Dringenberg').
card_number('ghastlord of fugue'/'SHM', '162').
card_flavor_text('ghastlord of fugue'/'SHM', '\"He is a ghost that has never known life.\"\n—The Seer\'s Parables').
card_multiverse_id('ghastlord of fugue'/'SHM', '153297').

card_in_set('ghastly discovery', 'SHM').
card_original_type('ghastly discovery'/'SHM', 'Sorcery').
card_original_text('ghastly discovery'/'SHM', 'Draw two cards, then discard a card.\nConspire (As you play this spell, you may tap two untapped creatures you control that share a color with it. When you do, copy it.)').
card_first_print('ghastly discovery', 'SHM').
card_image_name('ghastly discovery'/'SHM', 'ghastly discovery').
card_uid('ghastly discovery'/'SHM', 'SHM:Ghastly Discovery:ghastly discovery').
card_rarity('ghastly discovery'/'SHM', 'Common').
card_artist('ghastly discovery'/'SHM', 'Howard Lyon').
card_number('ghastly discovery'/'SHM', '39').
card_flavor_text('ghastly discovery'/'SHM', 'Korrigans, spirits bound to sources of water, shriek when they come upon their own drowned corpses.').
card_multiverse_id('ghastly discovery'/'SHM', '158682').

card_in_set('giantbaiting', 'SHM').
card_original_type('giantbaiting'/'SHM', 'Sorcery').
card_original_text('giantbaiting'/'SHM', 'Put a 4/4 red and green Giant Warrior creature token with haste into play. Remove it from the game at end of turn.\nConspire (As you play this spell, you may tap two untapped creatures you control that share a color with it. When you do, copy it.)').
card_first_print('giantbaiting', 'SHM').
card_image_name('giantbaiting'/'SHM', 'giantbaiting').
card_uid('giantbaiting'/'SHM', 'SHM:Giantbaiting:giantbaiting').
card_rarity('giantbaiting'/'SHM', 'Common').
card_artist('giantbaiting'/'SHM', 'Trevor Hairsine').
card_number('giantbaiting'/'SHM', '207').
card_multiverse_id('giantbaiting'/'SHM', '158693').

card_in_set('glamer spinners', 'SHM').
card_original_type('glamer spinners'/'SHM', 'Creature — Faerie Wizard').
card_original_text('glamer spinners'/'SHM', 'Flash\nFlying\nWhen Glamer Spinners comes into play, attach all Auras enchanting target permanent to another permanent with the same controller.').
card_first_print('glamer spinners', 'SHM').
card_image_name('glamer spinners'/'SHM', 'glamer spinners').
card_uid('glamer spinners'/'SHM', 'SHM:Glamer Spinners:glamer spinners').
card_rarity('glamer spinners'/'SHM', 'Uncommon').
card_artist('glamer spinners'/'SHM', 'Jesper Ejsing').
card_number('glamer spinners'/'SHM', '141').
card_multiverse_id('glamer spinners'/'SHM', '142031').

card_in_set('gleeful sabotage', 'SHM').
card_original_type('gleeful sabotage'/'SHM', 'Sorcery').
card_original_text('gleeful sabotage'/'SHM', 'Destroy target artifact or enchantment.\nConspire (As you play this spell, you may tap two untapped creatures you control that share a color with it. When you do, copy it and you may choose a new target for the copy.)').
card_first_print('gleeful sabotage', 'SHM').
card_image_name('gleeful sabotage'/'SHM', 'gleeful sabotage').
card_uid('gleeful sabotage'/'SHM', 'SHM:Gleeful Sabotage:gleeful sabotage').
card_rarity('gleeful sabotage'/'SHM', 'Common').
card_artist('gleeful sabotage'/'SHM', 'Todd Lockwood').
card_number('gleeful sabotage'/'SHM', '116').
card_multiverse_id('gleeful sabotage'/'SHM', '158681').

card_in_set('glen elendra liege', 'SHM').
card_original_type('glen elendra liege'/'SHM', 'Creature — Faerie Knight').
card_original_text('glen elendra liege'/'SHM', 'Flying\nOther blue creatures you control get +1/+1.\nOther black creatures you control get +1/+1.').
card_first_print('glen elendra liege', 'SHM').
card_image_name('glen elendra liege'/'SHM', 'glen elendra liege').
card_uid('glen elendra liege'/'SHM', 'SHM:Glen Elendra Liege:glen elendra liege').
card_rarity('glen elendra liege'/'SHM', 'Rare').
card_artist('glen elendra liege'/'SHM', 'Kev Walker').
card_number('glen elendra liege'/'SHM', '163').
card_flavor_text('glen elendra liege'/'SHM', 'Those who displease Oona soon learn the extent of the armies she commands.').
card_multiverse_id('glen elendra liege'/'SHM', '146743').

card_in_set('gloomlance', 'SHM').
card_original_type('gloomlance'/'SHM', 'Sorcery').
card_original_text('gloomlance'/'SHM', 'Destroy target creature. If that creature was green or white, its controller discards a card.').
card_first_print('gloomlance', 'SHM').
card_image_name('gloomlance'/'SHM', 'gloomlance').
card_uid('gloomlance'/'SHM', 'SHM:Gloomlance:gloomlance').
card_rarity('gloomlance'/'SHM', 'Common').
card_artist('gloomlance'/'SHM', 'Zoltan Boros & Gabor Szikszai').
card_number('gloomlance'/'SHM', '67').
card_flavor_text('gloomlance'/'SHM', 'Death stings the victim only once, but those left behind feel the pain of loss with every passing memory.').
card_multiverse_id('gloomlance'/'SHM', '158757').

card_in_set('gloomwidow', 'SHM').
card_original_type('gloomwidow'/'SHM', 'Creature — Spider').
card_original_text('gloomwidow'/'SHM', 'Reach\nGloomwidow can\'t block creatures without flying.').
card_first_print('gloomwidow', 'SHM').
card_image_name('gloomwidow'/'SHM', 'gloomwidow').
card_uid('gloomwidow'/'SHM', 'SHM:Gloomwidow:gloomwidow').
card_rarity('gloomwidow'/'SHM', 'Uncommon').
card_artist('gloomwidow'/'SHM', 'Mark Tedin').
card_number('gloomwidow'/'SHM', '117').
card_flavor_text('gloomwidow'/'SHM', 'When gloomwidows mature, they abandon venom in favor of massive webs that span the eaves of cliffs.').
card_multiverse_id('gloomwidow'/'SHM', '147375').

card_in_set('gloomwidow\'s feast', 'SHM').
card_original_type('gloomwidow\'s feast'/'SHM', 'Instant').
card_original_text('gloomwidow\'s feast'/'SHM', 'Destroy target creature with flying. If that creature was blue or black, put a 1/2 green Spider creature token with reach into play. (It can block creatures with flying.)').
card_first_print('gloomwidow\'s feast', 'SHM').
card_image_name('gloomwidow\'s feast'/'SHM', 'gloomwidow\'s feast').
card_uid('gloomwidow\'s feast'/'SHM', 'SHM:Gloomwidow\'s Feast:gloomwidow\'s feast').
card_rarity('gloomwidow\'s feast'/'SHM', 'Common').
card_artist('gloomwidow\'s feast'/'SHM', 'Thomas M. Baxa').
card_number('gloomwidow\'s feast'/'SHM', '118').
card_flavor_text('gloomwidow\'s feast'/'SHM', 'Each night, gloomwidows cull fresh prey for their ravenous hatchlings.').
card_multiverse_id('gloomwidow\'s feast'/'SHM', '154394').

card_in_set('gnarled effigy', 'SHM').
card_original_type('gnarled effigy'/'SHM', 'Artifact').
card_original_text('gnarled effigy'/'SHM', '{4}, {T}: Put a -1/-1 counter on target creature.').
card_first_print('gnarled effigy', 'SHM').
card_image_name('gnarled effigy'/'SHM', 'gnarled effigy').
card_uid('gnarled effigy'/'SHM', 'SHM:Gnarled Effigy:gnarled effigy').
card_rarity('gnarled effigy'/'SHM', 'Uncommon').
card_artist('gnarled effigy'/'SHM', 'Ron Brown').
card_number('gnarled effigy'/'SHM', '251').
card_flavor_text('gnarled effigy'/'SHM', '\"Bits of fallen scarecrow, laces of elfskin leather, teeth of an axeshark merrow . . . An industrious soul can find new uses for the most mundane items.\"\n—Mowagh the Gwyllion').
card_multiverse_id('gnarled effigy'/'SHM', '146015').

card_in_set('godhead of awe', 'SHM').
card_original_type('godhead of awe'/'SHM', 'Creature — Spirit Avatar').
card_original_text('godhead of awe'/'SHM', 'Flying\nOther creatures are 1/1.').
card_first_print('godhead of awe', 'SHM').
card_image_name('godhead of awe'/'SHM', 'godhead of awe').
card_uid('godhead of awe'/'SHM', 'SHM:Godhead of Awe:godhead of awe').
card_rarity('godhead of awe'/'SHM', 'Rare').
card_artist('godhead of awe'/'SHM', 'Mark Zug').
card_number('godhead of awe'/'SHM', '142').
card_flavor_text('godhead of awe'/'SHM', '\"What she saw crawling upon this world repulsed her. Yet she could not tear her gaze away.\"\n—The Seer\'s Parables').
card_multiverse_id('godhead of awe'/'SHM', '142019').

card_in_set('goldenglow moth', 'SHM').
card_original_type('goldenglow moth'/'SHM', 'Creature — Insect').
card_original_text('goldenglow moth'/'SHM', 'Flying\nWhenever Goldenglow Moth blocks, you may gain 4 life.').
card_first_print('goldenglow moth', 'SHM').
card_image_name('goldenglow moth'/'SHM', 'goldenglow moth').
card_uid('goldenglow moth'/'SHM', 'SHM:Goldenglow Moth:goldenglow moth').
card_rarity('goldenglow moth'/'SHM', 'Common').
card_artist('goldenglow moth'/'SHM', 'Howard Lyon').
card_number('goldenglow moth'/'SHM', '6').
card_flavor_text('goldenglow moth'/'SHM', 'Ordinary moths follow it, drawn to its light.').
card_multiverse_id('goldenglow moth'/'SHM', '159413').

card_in_set('gravelgill axeshark', 'SHM').
card_original_type('gravelgill axeshark'/'SHM', 'Creature — Merfolk Soldier').
card_original_text('gravelgill axeshark'/'SHM', 'Persist (When this creature is put into a graveyard from play, if it had no -1/-1 counters on it, return it to play under its owner\'s control with a -1/-1 counter on it.)').
card_first_print('gravelgill axeshark', 'SHM').
card_image_name('gravelgill axeshark'/'SHM', 'gravelgill axeshark').
card_uid('gravelgill axeshark'/'SHM', 'SHM:Gravelgill Axeshark:gravelgill axeshark').
card_rarity('gravelgill axeshark'/'SHM', 'Common').
card_artist('gravelgill axeshark'/'SHM', 'Dave Kendall').
card_number('gravelgill axeshark'/'SHM', '164').
card_flavor_text('gravelgill axeshark'/'SHM', '\"Their sharp scales would make good armor, if they\'d just stay dead.\"\n—Taeryn, cinder armorkeep').
card_multiverse_id('gravelgill axeshark'/'SHM', '141935').

card_in_set('gravelgill duo', 'SHM').
card_original_type('gravelgill duo'/'SHM', 'Creature — Merfolk Rogue Warrior').
card_original_text('gravelgill duo'/'SHM', 'Whenever you play a blue spell, Gravelgill Duo gets +1/+1 until end of turn.\nWhenever you play a black spell, Gravelgill Duo gains fear until end of turn.').
card_first_print('gravelgill duo', 'SHM').
card_image_name('gravelgill duo'/'SHM', 'gravelgill duo').
card_uid('gravelgill duo'/'SHM', 'SHM:Gravelgill Duo:gravelgill duo').
card_rarity('gravelgill duo'/'SHM', 'Common').
card_artist('gravelgill duo'/'SHM', 'Brandon Kitkouski').
card_number('gravelgill duo'/'SHM', '165').
card_multiverse_id('gravelgill duo'/'SHM', '153308').

card_in_set('graven cairns', 'SHM').
card_original_type('graven cairns'/'SHM', 'Land').
card_original_text('graven cairns'/'SHM', '{T}: Add {1} to your mana pool.\n{B/R}, {T}: Add {B}{B}, {B}{R}, or {R}{R} to your mana pool.').
card_image_name('graven cairns'/'SHM', 'graven cairns').
card_uid('graven cairns'/'SHM', 'SHM:Graven Cairns:graven cairns').
card_rarity('graven cairns'/'SHM', 'Rare').
card_artist('graven cairns'/'SHM', 'Rob Alexander').
card_number('graven cairns'/'SHM', '272').
card_flavor_text('graven cairns'/'SHM', '\"Here did the Extinguisher steal the light from the world. Here will we sacrifice her to bring the Great Rekindling to our people.\"\n—Illulia of Nighthearth').
card_multiverse_id('graven cairns'/'SHM', '146764').

card_in_set('greater auramancy', 'SHM').
card_original_type('greater auramancy'/'SHM', 'Enchantment').
card_original_text('greater auramancy'/'SHM', 'Other enchantments you control have shroud.\nEnchanted creatures you control have shroud.').
card_first_print('greater auramancy', 'SHM').
card_image_name('greater auramancy'/'SHM', 'greater auramancy').
card_uid('greater auramancy'/'SHM', 'SHM:Greater Auramancy:greater auramancy').
card_rarity('greater auramancy'/'SHM', 'Rare').
card_artist('greater auramancy'/'SHM', 'Chuck Lukacs').
card_number('greater auramancy'/'SHM', '7').
card_flavor_text('greater auramancy'/'SHM', 'A delicate veil of glamers can prove as sturdy as iron.').
card_multiverse_id('greater auramancy'/'SHM', '147425').

card_in_set('grief tyrant', 'SHM').
card_original_type('grief tyrant'/'SHM', 'Creature — Horror').
card_original_text('grief tyrant'/'SHM', 'Grief Tyrant comes into play with four -1/-1 counters on it.\nWhen Grief Tyrant is put into a graveyard from play, put a -1/-1 counter on target creature for each -1/-1 counter on Grief Tyrant.').
card_first_print('grief tyrant', 'SHM').
card_image_name('grief tyrant'/'SHM', 'grief tyrant').
card_uid('grief tyrant'/'SHM', 'SHM:Grief Tyrant:grief tyrant').
card_rarity('grief tyrant'/'SHM', 'Uncommon').
card_artist('grief tyrant'/'SHM', 'Izzy').
card_number('grief tyrant'/'SHM', '189').
card_multiverse_id('grief tyrant'/'SHM', '146035').

card_in_set('grim poppet', 'SHM').
card_original_type('grim poppet'/'SHM', 'Artifact Creature — Scarecrow').
card_original_text('grim poppet'/'SHM', 'Grim Poppet comes into play with three -1/-1 counters on it.\nRemove a -1/-1 counter from Grim Poppet: Put a -1/-1 counter on another target creature.').
card_first_print('grim poppet', 'SHM').
card_image_name('grim poppet'/'SHM', 'grim poppet').
card_uid('grim poppet'/'SHM', 'SHM:Grim Poppet:grim poppet').
card_rarity('grim poppet'/'SHM', 'Rare').
card_artist('grim poppet'/'SHM', 'Kev Walker').
card_number('grim poppet'/'SHM', '252').
card_flavor_text('grim poppet'/'SHM', 'Beware a scarecrow bearing gifts—especially sloshing vessels of acid.').
card_multiverse_id('grim poppet'/'SHM', '146739').

card_in_set('guttural response', 'SHM').
card_original_type('guttural response'/'SHM', 'Instant').
card_original_text('guttural response'/'SHM', 'Counter target blue instant spell.').
card_first_print('guttural response', 'SHM').
card_image_name('guttural response'/'SHM', 'guttural response').
card_uid('guttural response'/'SHM', 'SHM:Guttural Response:guttural response').
card_rarity('guttural response'/'SHM', 'Uncommon').
card_artist('guttural response'/'SHM', 'Matt Cavotta').
card_number('guttural response'/'SHM', '208').
card_flavor_text('guttural response'/'SHM', 'Wort had a raiding-bellow that could shatter steel, melt auras, and slice countermagic in half.').
card_multiverse_id('guttural response'/'SHM', '158773').

card_in_set('heap doll', 'SHM').
card_original_type('heap doll'/'SHM', 'Artifact Creature — Scarecrow').
card_original_text('heap doll'/'SHM', 'Sacrifice Heap Doll: Remove target card in a graveyard from the game.').
card_first_print('heap doll', 'SHM').
card_image_name('heap doll'/'SHM', 'heap doll').
card_uid('heap doll'/'SHM', 'SHM:Heap Doll:heap doll').
card_rarity('heap doll'/'SHM', 'Uncommon').
card_artist('heap doll'/'SHM', 'John Avon').
card_number('heap doll'/'SHM', '253').
card_flavor_text('heap doll'/'SHM', '\"I know one night it won\'t come back. Then I\'ll know it\'s truly done its job.\"\n—Braenna, cobblesmith').
card_multiverse_id('heap doll'/'SHM', '154395').

card_in_set('heartmender', 'SHM').
card_original_type('heartmender'/'SHM', 'Creature — Elemental').
card_original_text('heartmender'/'SHM', 'At the beginning of your upkeep, remove a -1/-1 counter from each creature you control.\nPersist (When this creature is put into a graveyard from play, if it had no -1/-1 counters on it, return it to play under its owner\'s control with a -1/-1 counter on it.)').
card_first_print('heartmender', 'SHM').
card_image_name('heartmender'/'SHM', 'heartmender').
card_uid('heartmender'/'SHM', 'SHM:Heartmender:heartmender').
card_rarity('heartmender'/'SHM', 'Rare').
card_artist('heartmender'/'SHM', 'Rebecca Guay').
card_number('heartmender'/'SHM', '228').
card_multiverse_id('heartmender'/'SHM', '153312').

card_in_set('helm of the ghastlord', 'SHM').
card_original_type('helm of the ghastlord'/'SHM', 'Enchantment — Aura').
card_original_text('helm of the ghastlord'/'SHM', 'Enchant creature\nAs long as enchanted creature is blue, it gets +1/+1 and has \"Whenever this creature deals damage to an opponent, draw a card.\"\nAs long as enchanted creature is black, it gets +1/+1 and has \"Whenever this creature deals damage to an opponent, that player discards a card.\"').
card_first_print('helm of the ghastlord', 'SHM').
card_image_name('helm of the ghastlord'/'SHM', 'helm of the ghastlord').
card_uid('helm of the ghastlord'/'SHM', 'SHM:Helm of the Ghastlord:helm of the ghastlord').
card_rarity('helm of the ghastlord'/'SHM', 'Common').
card_artist('helm of the ghastlord'/'SHM', 'Franz Vohwinkel').
card_number('helm of the ghastlord'/'SHM', '166').
card_multiverse_id('helm of the ghastlord'/'SHM', '158769').

card_in_set('hollowborn barghest', 'SHM').
card_original_type('hollowborn barghest'/'SHM', 'Creature — Demon Hound').
card_original_text('hollowborn barghest'/'SHM', 'At the beginning of your upkeep, if you have no cards in hand, each opponent loses 2 life.\nAt the beginning of each opponent\'s upkeep, if that player has no cards in hand, he or she loses 2 life.').
card_first_print('hollowborn barghest', 'SHM').
card_image_name('hollowborn barghest'/'SHM', 'hollowborn barghest').
card_uid('hollowborn barghest'/'SHM', 'SHM:Hollowborn Barghest:hollowborn barghest').
card_rarity('hollowborn barghest'/'SHM', 'Rare').
card_artist('hollowborn barghest'/'SHM', 'Eric Fortune').
card_number('hollowborn barghest'/'SHM', '68').
card_flavor_text('hollowborn barghest'/'SHM', 'It leaves a trail of crumpled trees and dashed dreams.').
card_multiverse_id('hollowborn barghest'/'SHM', '146095').

card_in_set('hollowsage', 'SHM').
card_original_type('hollowsage'/'SHM', 'Creature — Merfolk Wizard').
card_original_text('hollowsage'/'SHM', 'Whenever Hollowsage becomes untapped, you may have target player discard a card.').
card_first_print('hollowsage', 'SHM').
card_image_name('hollowsage'/'SHM', 'hollowsage').
card_uid('hollowsage'/'SHM', 'SHM:Hollowsage:hollowsage').
card_rarity('hollowsage'/'SHM', 'Uncommon').
card_artist('hollowsage'/'SHM', 'Lucio Parrillo').
card_number('hollowsage'/'SHM', '69').
card_flavor_text('hollowsage'/'SHM', 'Dreams of dark nights to come are said to stir in the minds of scheming hollowsages.').
card_multiverse_id('hollowsage'/'SHM', '158234').

card_in_set('horde of boggarts', 'SHM').
card_original_type('horde of boggarts'/'SHM', 'Creature — Goblin').
card_original_text('horde of boggarts'/'SHM', 'Horde of Boggarts\'s power and toughness are each equal to the number of red permanents you control.\nHorde of Boggarts can\'t be blocked except by two or more creatures.').
card_first_print('horde of boggarts', 'SHM').
card_image_name('horde of boggarts'/'SHM', 'horde of boggarts').
card_uid('horde of boggarts'/'SHM', 'SHM:Horde of Boggarts:horde of boggarts').
card_rarity('horde of boggarts'/'SHM', 'Uncommon').
card_artist('horde of boggarts'/'SHM', 'Steve Prescott').
card_number('horde of boggarts'/'SHM', '94').
card_flavor_text('horde of boggarts'/'SHM', 'Strategies don\'t come easily to the boggarts\' feral minds, but full-on assault hasn\'t failed them yet.').
card_multiverse_id('horde of boggarts'/'SHM', '154409').

card_in_set('howl of the night pack', 'SHM').
card_original_type('howl of the night pack'/'SHM', 'Sorcery').
card_original_text('howl of the night pack'/'SHM', 'Put a 2/2 green Wolf creature token into play for each Forest you control.').
card_first_print('howl of the night pack', 'SHM').
card_image_name('howl of the night pack'/'SHM', 'howl of the night pack').
card_uid('howl of the night pack'/'SHM', 'SHM:Howl of the Night Pack:howl of the night pack').
card_rarity('howl of the night pack'/'SHM', 'Uncommon').
card_artist('howl of the night pack'/'SHM', 'Lars Grant-West').
card_number('howl of the night pack'/'SHM', '119').
card_flavor_text('howl of the night pack'/'SHM', 'The murderous horrors of Raven\'s Run are legendary, but even that haunted place goes quiet when the night wolves howl.').
card_multiverse_id('howl of the night pack'/'SHM', '153996').

card_in_set('hungry spriggan', 'SHM').
card_original_type('hungry spriggan'/'SHM', 'Creature — Goblin Warrior').
card_original_text('hungry spriggan'/'SHM', 'Trample\nWhenever Hungry Spriggan attacks, it gets +3/+3 until end of turn.').
card_first_print('hungry spriggan', 'SHM').
card_image_name('hungry spriggan'/'SHM', 'hungry spriggan').
card_uid('hungry spriggan'/'SHM', 'SHM:Hungry Spriggan:hungry spriggan').
card_rarity('hungry spriggan'/'SHM', 'Common').
card_artist('hungry spriggan'/'SHM', 'Drew Tucker').
card_number('hungry spriggan'/'SHM', '120').
card_flavor_text('hungry spriggan'/'SHM', 'If a spriggan\'s eyes are larger than its stomach, it has ways to remedy the situation.').
card_multiverse_id('hungry spriggan'/'SHM', '153997').

card_in_set('illuminated folio', 'SHM').
card_original_type('illuminated folio'/'SHM', 'Artifact').
card_original_text('illuminated folio'/'SHM', '{1}, {T}, Reveal two cards from your hand that share a color: Draw a card.').
card_first_print('illuminated folio', 'SHM').
card_image_name('illuminated folio'/'SHM', 'illuminated folio').
card_uid('illuminated folio'/'SHM', 'SHM:Illuminated Folio:illuminated folio').
card_rarity('illuminated folio'/'SHM', 'Uncommon').
card_artist('illuminated folio'/'SHM', 'Jim Pavelec').
card_number('illuminated folio'/'SHM', '254').
card_flavor_text('illuminated folio'/'SHM', 'Most such relics remain clasped shut for fear of what ominous truths might be carved into the diptych inside.').
card_multiverse_id('illuminated folio'/'SHM', '146040').

card_in_set('impromptu raid', 'SHM').
card_original_type('impromptu raid'/'SHM', 'Enchantment').
card_original_text('impromptu raid'/'SHM', '{2}{R/G}: Reveal the top card of your library. If it isn\'t a creature card, put it into your graveyard. Otherwise, put that card into play. That creature has haste. Sacrifice it at end of turn.').
card_first_print('impromptu raid', 'SHM').
card_image_name('impromptu raid'/'SHM', 'impromptu raid').
card_uid('impromptu raid'/'SHM', 'SHM:Impromptu Raid:impromptu raid').
card_rarity('impromptu raid'/'SHM', 'Rare').
card_artist('impromptu raid'/'SHM', 'Randy Gallegos').
card_number('impromptu raid'/'SHM', '209').
card_flavor_text('impromptu raid'/'SHM', 'For the Scuzzback gang, it\'s always \"raid o\'clock.\"').
card_multiverse_id('impromptu raid'/'SHM', '146097').

card_in_set('incremental blight', 'SHM').
card_original_type('incremental blight'/'SHM', 'Sorcery').
card_original_text('incremental blight'/'SHM', 'Put a -1/-1 counter on target creature, two -1/-1 counters on another target creature, and three -1/-1 counters on a third target creature.').
card_first_print('incremental blight', 'SHM').
card_image_name('incremental blight'/'SHM', 'incremental blight').
card_uid('incremental blight'/'SHM', 'SHM:Incremental Blight:incremental blight').
card_rarity('incremental blight'/'SHM', 'Uncommon').
card_artist('incremental blight'/'SHM', 'Chuck Lukacs').
card_number('incremental blight'/'SHM', '70').
card_flavor_text('incremental blight'/'SHM', 'Shadowmoor\'s main crops are rot, slime, and despair.').
card_multiverse_id('incremental blight'/'SHM', '147433').

card_in_set('inescapable brute', 'SHM').
card_original_type('inescapable brute'/'SHM', 'Creature — Giant Warrior').
card_original_text('inescapable brute'/'SHM', 'Wither (This deals damage to creatures in the form of -1/-1 counters.)\nInescapable Brute must be blocked if able.').
card_first_print('inescapable brute', 'SHM').
card_image_name('inescapable brute'/'SHM', 'inescapable brute').
card_uid('inescapable brute'/'SHM', 'SHM:Inescapable Brute:inescapable brute').
card_rarity('inescapable brute'/'SHM', 'Common').
card_artist('inescapable brute'/'SHM', 'Nils Hamm').
card_number('inescapable brute'/'SHM', '95').
card_flavor_text('inescapable brute'/'SHM', 'If you think you made it past him alive, he\'s just teasing you.').
card_multiverse_id('inescapable brute'/'SHM', '147429').

card_in_set('inkfathom infiltrator', 'SHM').
card_original_type('inkfathom infiltrator'/'SHM', 'Creature — Merfolk Rogue').
card_original_text('inkfathom infiltrator'/'SHM', 'Inkfathom Infiltrator can\'t block and is unblockable.').
card_first_print('inkfathom infiltrator', 'SHM').
card_image_name('inkfathom infiltrator'/'SHM', 'inkfathom infiltrator').
card_uid('inkfathom infiltrator'/'SHM', 'SHM:Inkfathom Infiltrator:inkfathom infiltrator').
card_rarity('inkfathom infiltrator'/'SHM', 'Uncommon').
card_artist('inkfathom infiltrator'/'SHM', 'Jesper Ejsing').
card_number('inkfathom infiltrator'/'SHM', '167').
card_flavor_text('inkfathom infiltrator'/'SHM', 'No one can navigate the dank maze of Shadowmoor\'s rivers better than the cruel and covetous merrows. They use the currents to steal—and to steal away.').
card_multiverse_id('inkfathom infiltrator'/'SHM', '154401').

card_in_set('inkfathom witch', 'SHM').
card_original_type('inkfathom witch'/'SHM', 'Creature — Merfolk Wizard').
card_original_text('inkfathom witch'/'SHM', 'Fear\n{2}{U}{B}: Each unblocked creature becomes 4/1 until end of turn.').
card_first_print('inkfathom witch', 'SHM').
card_image_name('inkfathom witch'/'SHM', 'inkfathom witch').
card_uid('inkfathom witch'/'SHM', 'SHM:Inkfathom Witch:inkfathom witch').
card_rarity('inkfathom witch'/'SHM', 'Uncommon').
card_artist('inkfathom witch'/'SHM', 'Larry MacDougall').
card_number('inkfathom witch'/'SHM', '168').
card_flavor_text('inkfathom witch'/'SHM', 'The murk of the Wanderbrine concealed unseemly rituals designed to bring out the worst in merrowkind.').
card_multiverse_id('inkfathom witch'/'SHM', '157879').

card_in_set('inquisitor\'s snare', 'SHM').
card_original_type('inquisitor\'s snare'/'SHM', 'Instant').
card_original_text('inquisitor\'s snare'/'SHM', 'Prevent all damage target attacking or blocking creature would deal this turn. If that creature is black or red, destroy it.').
card_first_print('inquisitor\'s snare', 'SHM').
card_image_name('inquisitor\'s snare'/'SHM', 'inquisitor\'s snare').
card_uid('inquisitor\'s snare'/'SHM', 'SHM:Inquisitor\'s Snare:inquisitor\'s snare').
card_rarity('inquisitor\'s snare'/'SHM', 'Common').
card_artist('inquisitor\'s snare'/'SHM', 'Michael Sutfin').
card_number('inquisitor\'s snare'/'SHM', '8').
card_flavor_text('inquisitor\'s snare'/'SHM', 'Driven by their paranoia, kithkin inflict on their foes all the agonies they believe would have been inflicted on them.').
card_multiverse_id('inquisitor\'s snare'/'SHM', '158765').

card_in_set('intimidator initiate', 'SHM').
card_original_type('intimidator initiate'/'SHM', 'Creature — Goblin Shaman').
card_original_text('intimidator initiate'/'SHM', 'Whenever a player plays a red spell, you may pay {1}. If you do, target creature can\'t block this turn.').
card_first_print('intimidator initiate', 'SHM').
card_image_name('intimidator initiate'/'SHM', 'intimidator initiate').
card_uid('intimidator initiate'/'SHM', 'SHM:Intimidator Initiate:intimidator initiate').
card_rarity('intimidator initiate'/'SHM', 'Common').
card_artist('intimidator initiate'/'SHM', 'Ralph Horsley').
card_number('intimidator initiate'/'SHM', '96').
card_flavor_text('intimidator initiate'/'SHM', 'Don\'t stand in his way, for his way is full of pointy things and fire.').
card_multiverse_id('intimidator initiate'/'SHM', '142026').

card_in_set('island', 'SHM').
card_original_type('island'/'SHM', 'Basic Land — Island').
card_original_text('island'/'SHM', 'U').
card_image_name('island'/'SHM', 'island1').
card_uid('island'/'SHM', 'SHM:Island:island1').
card_rarity('island'/'SHM', 'Basic Land').
card_artist('island'/'SHM', 'Omar Rayyan').
card_number('island'/'SHM', '286').
card_multiverse_id('island'/'SHM', '158237').

card_in_set('island', 'SHM').
card_original_type('island'/'SHM', 'Basic Land — Island').
card_original_text('island'/'SHM', 'U').
card_image_name('island'/'SHM', 'island2').
card_uid('island'/'SHM', 'SHM:Island:island2').
card_rarity('island'/'SHM', 'Basic Land').
card_artist('island'/'SHM', 'Warren Mahy').
card_number('island'/'SHM', '287').
card_multiverse_id('island'/'SHM', '157874').

card_in_set('island', 'SHM').
card_original_type('island'/'SHM', 'Basic Land — Island').
card_original_text('island'/'SHM', 'U').
card_image_name('island'/'SHM', 'island3').
card_uid('island'/'SHM', 'SHM:Island:island3').
card_rarity('island'/'SHM', 'Basic Land').
card_artist('island'/'SHM', 'Brandon Kitkouski').
card_number('island'/'SHM', '288').
card_multiverse_id('island'/'SHM', '157875').

card_in_set('island', 'SHM').
card_original_type('island'/'SHM', 'Basic Land — Island').
card_original_text('island'/'SHM', 'U').
card_image_name('island'/'SHM', 'island4').
card_uid('island'/'SHM', 'SHM:Island:island4').
card_rarity('island'/'SHM', 'Basic Land').
card_artist('island'/'SHM', 'Rob Alexander').
card_number('island'/'SHM', '289').
card_multiverse_id('island'/'SHM', '157883').

card_in_set('isleback spawn', 'SHM').
card_original_type('isleback spawn'/'SHM', 'Creature — Kraken').
card_original_text('isleback spawn'/'SHM', 'Shroud\nIsleback Spawn gets +4/+8 as long as a library has twenty or fewer cards in it.').
card_first_print('isleback spawn', 'SHM').
card_image_name('isleback spawn'/'SHM', 'isleback spawn').
card_uid('isleback spawn'/'SHM', 'SHM:Isleback Spawn:isleback spawn').
card_rarity('isleback spawn'/'SHM', 'Rare').
card_artist('isleback spawn'/'SHM', 'Mark Tedin').
card_number('isleback spawn'/'SHM', '40').
card_flavor_text('isleback spawn'/'SHM', 'The more deadly the lake monster, the shorter the tale.').
card_multiverse_id('isleback spawn'/'SHM', '147370').

card_in_set('jaws of stone', 'SHM').
card_original_type('jaws of stone'/'SHM', 'Sorcery').
card_original_text('jaws of stone'/'SHM', 'Jaws of Stone deals X damage divided as you choose among any number of target creatures and/or players, where X is the number of Mountains you control as you play Jaws of Stone.').
card_first_print('jaws of stone', 'SHM').
card_image_name('jaws of stone'/'SHM', 'jaws of stone').
card_uid('jaws of stone'/'SHM', 'SHM:Jaws of Stone:jaws of stone').
card_rarity('jaws of stone'/'SHM', 'Uncommon').
card_artist('jaws of stone'/'SHM', 'Zoltan Boros & Gabor Szikszai').
card_number('jaws of stone'/'SHM', '97').
card_flavor_text('jaws of stone'/'SHM', 'When giants find their mountain homes infested, they have a whole range of solutions.').
card_multiverse_id('jaws of stone'/'SHM', '146070').

card_in_set('juvenile gloomwidow', 'SHM').
card_original_type('juvenile gloomwidow'/'SHM', 'Creature — Spider').
card_original_text('juvenile gloomwidow'/'SHM', 'Reach (This can block creatures with flying.)\nWither (This deals damage to creatures in the form of -1/-1 counters.)').
card_first_print('juvenile gloomwidow', 'SHM').
card_image_name('juvenile gloomwidow'/'SHM', 'juvenile gloomwidow').
card_uid('juvenile gloomwidow'/'SHM', 'SHM:Juvenile Gloomwidow:juvenile gloomwidow').
card_rarity('juvenile gloomwidow'/'SHM', 'Common').
card_artist('juvenile gloomwidow'/'SHM', 'Thomas M. Baxa').
card_number('juvenile gloomwidow'/'SHM', '121').
card_flavor_text('juvenile gloomwidow'/'SHM', 'Gloomwidow venom is particularly virulent during the spider\'s first years, when it does most of its hunting near the forest floor.').
card_multiverse_id('juvenile gloomwidow'/'SHM', '153976').

card_in_set('kinscaer harpoonist', 'SHM').
card_original_type('kinscaer harpoonist'/'SHM', 'Creature — Kithkin Soldier').
card_original_text('kinscaer harpoonist'/'SHM', 'Flying\nWhenever Kinscaer Harpoonist attacks, you may have target creature lose flying until end of turn.').
card_first_print('kinscaer harpoonist', 'SHM').
card_image_name('kinscaer harpoonist'/'SHM', 'kinscaer harpoonist').
card_uid('kinscaer harpoonist'/'SHM', 'SHM:Kinscaer Harpoonist:kinscaer harpoonist').
card_rarity('kinscaer harpoonist'/'SHM', 'Common').
card_artist('kinscaer harpoonist'/'SHM', 'Thomas M. Baxa').
card_number('kinscaer harpoonist'/'SHM', '41').
card_flavor_text('kinscaer harpoonist'/'SHM', 'The suspicious kithkin of Kinscaer patrol the skies, their eyes attuned to the shapes of outsiders.').
card_multiverse_id('kinscaer harpoonist'/'SHM', '157876').

card_in_set('kitchen finks', 'SHM').
card_original_type('kitchen finks'/'SHM', 'Creature — Ouphe').
card_original_text('kitchen finks'/'SHM', 'When Kitchen Finks comes into play, you gain 2 life.\nPersist (When this creature is put into a graveyard from play, if it had no -1/-1 counters on it, return it to play under its owner\'s control with a -1/-1 counter on it.)').
card_image_name('kitchen finks'/'SHM', 'kitchen finks').
card_uid('kitchen finks'/'SHM', 'SHM:Kitchen Finks:kitchen finks').
card_rarity('kitchen finks'/'SHM', 'Uncommon').
card_artist('kitchen finks'/'SHM', 'Kev Walker').
card_number('kitchen finks'/'SHM', '229').
card_flavor_text('kitchen finks'/'SHM', 'Accept one favor from an ouphe, and you\'re doomed to accept another.').
card_multiverse_id('kitchen finks'/'SHM', '141976').

card_in_set('kithkin rabble', 'SHM').
card_original_type('kithkin rabble'/'SHM', 'Creature — Kithkin').
card_original_text('kithkin rabble'/'SHM', 'Vigilance\nKithkin Rabble\'s power and toughness are each equal to the number of white permanents you control.').
card_first_print('kithkin rabble', 'SHM').
card_image_name('kithkin rabble'/'SHM', 'kithkin rabble').
card_uid('kithkin rabble'/'SHM', 'SHM:Kithkin Rabble:kithkin rabble').
card_rarity('kithkin rabble'/'SHM', 'Uncommon').
card_artist('kithkin rabble'/'SHM', 'Omar Rayyan').
card_number('kithkin rabble'/'SHM', '9').
card_flavor_text('kithkin rabble'/'SHM', 'If even the slightest hint of panic enters the thoughtweft, bakers, potters, and even medics drop their spoons and salves to take up arms.').
card_multiverse_id('kithkin rabble'/'SHM', '158695').

card_in_set('kithkin shielddare', 'SHM').
card_original_type('kithkin shielddare'/'SHM', 'Creature — Kithkin Soldier').
card_original_text('kithkin shielddare'/'SHM', '{W}, {T}: Target blocking creature gets +2/+2 until end of turn.').
card_first_print('kithkin shielddare', 'SHM').
card_image_name('kithkin shielddare'/'SHM', 'kithkin shielddare').
card_uid('kithkin shielddare'/'SHM', 'SHM:Kithkin Shielddare:kithkin shielddare').
card_rarity('kithkin shielddare'/'SHM', 'Common').
card_artist('kithkin shielddare'/'SHM', 'Christopher Moeller').
card_number('kithkin shielddare'/'SHM', '10').
card_flavor_text('kithkin shielddare'/'SHM', 'The nova glyph is a potent symbol. A shield embossed with it can resist the force of even the most determined giant.').
card_multiverse_id('kithkin shielddare'/'SHM', '158238').

card_in_set('knacksaw clique', 'SHM').
card_original_type('knacksaw clique'/'SHM', 'Creature — Faerie Rogue').
card_original_text('knacksaw clique'/'SHM', 'Flying\n{1}{U}, {Q}: Target opponent removes the top card of his or her library from the game. Until end of turn, you may play that card. ({Q} is the untap symbol.)').
card_first_print('knacksaw clique', 'SHM').
card_image_name('knacksaw clique'/'SHM', 'knacksaw clique').
card_uid('knacksaw clique'/'SHM', 'SHM:Knacksaw Clique:knacksaw clique').
card_rarity('knacksaw clique'/'SHM', 'Rare').
card_artist('knacksaw clique'/'SHM', 'Steven Belledin').
card_number('knacksaw clique'/'SHM', '42').
card_flavor_text('knacksaw clique'/'SHM', 'Most of the Knacksaw clique\'s skills were hewn from the minds of others.').
card_multiverse_id('knacksaw clique'/'SHM', '146036').

card_in_set('knollspine dragon', 'SHM').
card_original_type('knollspine dragon'/'SHM', 'Creature — Dragon').
card_original_text('knollspine dragon'/'SHM', 'Flying\nWhen Knollspine Dragon comes into play, you may discard your hand and draw cards equal to the damage dealt to target opponent this turn.').
card_first_print('knollspine dragon', 'SHM').
card_image_name('knollspine dragon'/'SHM', 'knollspine dragon').
card_uid('knollspine dragon'/'SHM', 'SHM:Knollspine Dragon:knollspine dragon').
card_rarity('knollspine dragon'/'SHM', 'Rare').
card_artist('knollspine dragon'/'SHM', 'Steve Prescott').
card_number('knollspine dragon'/'SHM', '98').
card_flavor_text('knollspine dragon'/'SHM', 'It woke to find a vast world in need of a master.').
card_multiverse_id('knollspine dragon'/'SHM', '147398').

card_in_set('knollspine invocation', 'SHM').
card_original_type('knollspine invocation'/'SHM', 'Enchantment').
card_original_text('knollspine invocation'/'SHM', '{X}, Discard a card with converted mana cost X: Knollspine Invocation deals X damage to target creature or player.').
card_first_print('knollspine invocation', 'SHM').
card_image_name('knollspine invocation'/'SHM', 'knollspine invocation').
card_uid('knollspine invocation'/'SHM', 'SHM:Knollspine Invocation:knollspine invocation').
card_rarity('knollspine invocation'/'SHM', 'Rare').
card_artist('knollspine invocation'/'SHM', 'Dave Dorman').
card_number('knollspine invocation'/'SHM', '99').
card_flavor_text('knollspine invocation'/'SHM', 'Witches tempted by the power of flame gathered to study in the ruins of Spinerock Knoll.').
card_multiverse_id('knollspine invocation'/'SHM', '153977').

card_in_set('kulrath knight', 'SHM').
card_original_type('kulrath knight'/'SHM', 'Creature — Elemental Knight').
card_original_text('kulrath knight'/'SHM', 'Flying\nWither (This deals damage to creatures in the form of -1/-1 counters.)\nCreatures your opponents control with counters on them can\'t attack or block.').
card_first_print('kulrath knight', 'SHM').
card_image_name('kulrath knight'/'SHM', 'kulrath knight').
card_uid('kulrath knight'/'SHM', 'SHM:Kulrath Knight:kulrath knight').
card_rarity('kulrath knight'/'SHM', 'Uncommon').
card_artist('kulrath knight'/'SHM', 'Daarken').
card_number('kulrath knight'/'SHM', '190').
card_flavor_text('kulrath knight'/'SHM', 'Mounted cinders circle the blasted peak at Kulrath, hunting the Extinguisher.').
card_multiverse_id('kulrath knight'/'SHM', '147423').

card_in_set('last breath', 'SHM').
card_original_type('last breath'/'SHM', 'Instant').
card_original_text('last breath'/'SHM', 'Remove target creature with power 2 or less from the game. Its controller gains 4 life.').
card_image_name('last breath'/'SHM', 'last breath').
card_uid('last breath'/'SHM', 'SHM:Last Breath:last breath').
card_rarity('last breath'/'SHM', 'Common').
card_artist('last breath'/'SHM', 'Thomas Denmark').
card_number('last breath'/'SHM', '11').
card_flavor_text('last breath'/'SHM', '\"Tsk. You\'d think those nasty merrows would know how to hold their breath.\"\n—Olka, mistmeadow witch').
card_multiverse_id('last breath'/'SHM', '147424').

card_in_set('leech bonder', 'SHM').
card_original_type('leech bonder'/'SHM', 'Creature — Merfolk Soldier').
card_original_text('leech bonder'/'SHM', 'Leech Bonder comes into play with two -1/-1 counters on it.\n{U}, {Q}: Move a counter from target creature onto another target creature. ({Q} is the untap symbol.)').
card_first_print('leech bonder', 'SHM').
card_image_name('leech bonder'/'SHM', 'leech bonder').
card_uid('leech bonder'/'SHM', 'SHM:Leech Bonder:leech bonder').
card_rarity('leech bonder'/'SHM', 'Uncommon').
card_artist('leech bonder'/'SHM', 'E. M. Gist').
card_number('leech bonder'/'SHM', '43').
card_flavor_text('leech bonder'/'SHM', 'Some don\'t know his face, but his pets are a dead giveaway.').
card_multiverse_id('leech bonder'/'SHM', '147406').

card_in_set('leechridden swamp', 'SHM').
card_original_type('leechridden swamp'/'SHM', 'Land — Swamp').
card_original_text('leechridden swamp'/'SHM', '({T}: Add {B} to your mana pool.)\nLeechridden Swamp comes into play tapped.\n{B}, {T}: Each opponent loses 1 life. Play this ability only if you control two or more black permanents.').
card_first_print('leechridden swamp', 'SHM').
card_image_name('leechridden swamp'/'SHM', 'leechridden swamp').
card_uid('leechridden swamp'/'SHM', 'SHM:Leechridden Swamp:leechridden swamp').
card_rarity('leechridden swamp'/'SHM', 'Uncommon').
card_artist('leechridden swamp'/'SHM', 'Lars Grant-West').
card_number('leechridden swamp'/'SHM', '273').
card_multiverse_id('leechridden swamp'/'SHM', '142020').

card_in_set('loamdragger giant', 'SHM').
card_original_type('loamdragger giant'/'SHM', 'Creature — Giant Warrior').
card_original_text('loamdragger giant'/'SHM', '').
card_first_print('loamdragger giant', 'SHM').
card_image_name('loamdragger giant'/'SHM', 'loamdragger giant').
card_uid('loamdragger giant'/'SHM', 'SHM:Loamdragger Giant:loamdragger giant').
card_rarity('loamdragger giant'/'SHM', 'Common').
card_artist('loamdragger giant'/'SHM', 'Pete Venters').
card_number('loamdragger giant'/'SHM', '210').
card_flavor_text('loamdragger giant'/'SHM', 'Giants sleep soundly and long, sometimes for long enough that a crust of earth and moss grows over them. But inevitably something disturbs their slumber, and they wake unhappy.').
card_multiverse_id('loamdragger giant'/'SHM', '142001').

card_in_set('loch korrigan', 'SHM').
card_original_type('loch korrigan'/'SHM', 'Creature — Spirit').
card_original_text('loch korrigan'/'SHM', '{U/B}: Loch Korrigan gets +1/+1 until end of turn.').
card_first_print('loch korrigan', 'SHM').
card_image_name('loch korrigan'/'SHM', 'loch korrigan').
card_uid('loch korrigan'/'SHM', 'SHM:Loch Korrigan:loch korrigan').
card_rarity('loch korrigan'/'SHM', 'Common').
card_artist('loch korrigan'/'SHM', 'Daarken').
card_number('loch korrigan'/'SHM', '71').
card_flavor_text('loch korrigan'/'SHM', '\"Don\'t look upon still waters without first breaking the surface. The korrigan will catch you with her gaze and drag you to your death.\"\n—Kithkin superstition').
card_multiverse_id('loch korrigan'/'SHM', '147414').

card_in_set('lockjaw snapper', 'SHM').
card_original_type('lockjaw snapper'/'SHM', 'Artifact Creature — Scarecrow').
card_original_text('lockjaw snapper'/'SHM', 'Wither (This deals damage to creatures in the form of -1/-1 counters.)\nWhen Lockjaw Snapper is put into a graveyard from play, put a -1/-1 counter on each creature with a -1/-1 counter on it.').
card_first_print('lockjaw snapper', 'SHM').
card_image_name('lockjaw snapper'/'SHM', 'lockjaw snapper').
card_uid('lockjaw snapper'/'SHM', 'SHM:Lockjaw Snapper:lockjaw snapper').
card_rarity('lockjaw snapper'/'SHM', 'Uncommon').
card_artist('lockjaw snapper'/'SHM', 'Daren Bader').
card_number('lockjaw snapper'/'SHM', '255').
card_multiverse_id('lockjaw snapper'/'SHM', '154411').

card_in_set('lurebound scarecrow', 'SHM').
card_original_type('lurebound scarecrow'/'SHM', 'Artifact Creature — Scarecrow').
card_original_text('lurebound scarecrow'/'SHM', 'As Lurebound Scarecrow comes into play, choose a color.\nWhen you control no permanents of the chosen color, sacrifice Lurebound Scarecrow.').
card_first_print('lurebound scarecrow', 'SHM').
card_image_name('lurebound scarecrow'/'SHM', 'lurebound scarecrow').
card_uid('lurebound scarecrow'/'SHM', 'SHM:Lurebound Scarecrow:lurebound scarecrow').
card_rarity('lurebound scarecrow'/'SHM', 'Uncommon').
card_artist('lurebound scarecrow'/'SHM', 'Nils Hamm').
card_number('lurebound scarecrow'/'SHM', '256').
card_flavor_text('lurebound scarecrow'/'SHM', 'It\'s a fool\'s race to run if all is lost if it be won.').
card_multiverse_id('lurebound scarecrow'/'SHM', '154397').

card_in_set('madblind mountain', 'SHM').
card_original_type('madblind mountain'/'SHM', 'Land — Mountain').
card_original_text('madblind mountain'/'SHM', '({T}: Add {R} to your mana pool.)\nMadblind Mountain comes into play tapped.\n{R}, {T}: Shuffle your library. Play this ability only if you control two or more red permanents.').
card_first_print('madblind mountain', 'SHM').
card_image_name('madblind mountain'/'SHM', 'madblind mountain').
card_uid('madblind mountain'/'SHM', 'SHM:Madblind Mountain:madblind mountain').
card_rarity('madblind mountain'/'SHM', 'Uncommon').
card_artist('madblind mountain'/'SHM', 'Fred Fields').
card_number('madblind mountain'/'SHM', '274').
card_multiverse_id('madblind mountain'/'SHM', '141959').

card_in_set('mana reflection', 'SHM').
card_original_type('mana reflection'/'SHM', 'Enchantment').
card_original_text('mana reflection'/'SHM', 'If you tap a permanent for mana, it produces twice as much of that mana instead.').
card_first_print('mana reflection', 'SHM').
card_image_name('mana reflection'/'SHM', 'mana reflection').
card_uid('mana reflection'/'SHM', 'SHM:Mana Reflection:mana reflection').
card_rarity('mana reflection'/'SHM', 'Rare').
card_artist('mana reflection'/'SHM', 'Terese Nielsen & Ron Spencer').
card_number('mana reflection'/'SHM', '122').
card_flavor_text('mana reflection'/'SHM', 'Despite its darkness, Shadowmoor doesn\'t want for growth. Legions of creeping things thrive in the shade.').
card_multiverse_id('mana reflection'/'SHM', '146750').

card_in_set('manaforge cinder', 'SHM').
card_original_type('manaforge cinder'/'SHM', 'Creature — Elemental Shaman').
card_original_text('manaforge cinder'/'SHM', '{1}: Add {B} or {R} to your mana pool. Play this ability no more than three times each turn.').
card_first_print('manaforge cinder', 'SHM').
card_image_name('manaforge cinder'/'SHM', 'manaforge cinder').
card_uid('manaforge cinder'/'SHM', 'SHM:Manaforge Cinder:manaforge cinder').
card_rarity('manaforge cinder'/'SHM', 'Common').
card_artist('manaforge cinder'/'SHM', 'Izzy').
card_number('manaforge cinder'/'SHM', '191').
card_flavor_text('manaforge cinder'/'SHM', 'Cinder mystics set traps for sluaghs and other spirits of flame, seeking to extract the secrets of the fire their own hearts have lost.').
card_multiverse_id('manaforge cinder'/'SHM', '141978').

card_in_set('manamorphose', 'SHM').
card_original_type('manamorphose'/'SHM', 'Instant').
card_original_text('manamorphose'/'SHM', 'Add two mana in any combination of colors to your mana pool.\nDraw a card.').
card_first_print('manamorphose', 'SHM').
card_image_name('manamorphose'/'SHM', 'manamorphose').
card_uid('manamorphose'/'SHM', 'SHM:Manamorphose:manamorphose').
card_rarity('manamorphose'/'SHM', 'Common').
card_artist('manamorphose'/'SHM', 'Jeff Miracola').
card_number('manamorphose'/'SHM', '211').
card_flavor_text('manamorphose'/'SHM', 'For a moment, objects of pure mana glimmered in the wonderstruck boggart\'s hands. In the next moment, they were in his mouth, as he chewed contentedly.').
card_multiverse_id('manamorphose'/'SHM', '153968').

card_in_set('mass calcify', 'SHM').
card_original_type('mass calcify'/'SHM', 'Sorcery').
card_original_text('mass calcify'/'SHM', 'Destroy all nonwhite creatures.').
card_first_print('mass calcify', 'SHM').
card_image_name('mass calcify'/'SHM', 'mass calcify').
card_uid('mass calcify'/'SHM', 'SHM:Mass Calcify:mass calcify').
card_rarity('mass calcify'/'SHM', 'Rare').
card_artist('mass calcify'/'SHM', 'Brandon Kitkouski').
card_number('mass calcify'/'SHM', '12').
card_flavor_text('mass calcify'/'SHM', 'The dead serve as their own tombstones.').
card_multiverse_id('mass calcify'/'SHM', '146053').

card_in_set('medicine runner', 'SHM').
card_original_type('medicine runner'/'SHM', 'Creature — Elf Cleric').
card_original_text('medicine runner'/'SHM', 'When Medicine Runner comes into play, you may remove a counter from target permanent.').
card_first_print('medicine runner', 'SHM').
card_image_name('medicine runner'/'SHM', 'medicine runner').
card_uid('medicine runner'/'SHM', 'SHM:Medicine Runner:medicine runner').
card_rarity('medicine runner'/'SHM', 'Common').
card_artist('medicine runner'/'SHM', 'Chippy').
card_number('medicine runner'/'SHM', '230').
card_flavor_text('medicine runner'/'SHM', 'Elvish healers carry vials of the balm known as dawnglow, reserving it for those wounds that herbs cannot treat.').
card_multiverse_id('medicine runner'/'SHM', '141977').

card_in_set('memory plunder', 'SHM').
card_original_type('memory plunder'/'SHM', 'Instant').
card_original_text('memory plunder'/'SHM', 'You may play target instant or sorcery card in an opponent\'s graveyard without paying its mana cost.').
card_first_print('memory plunder', 'SHM').
card_image_name('memory plunder'/'SHM', 'memory plunder').
card_uid('memory plunder'/'SHM', 'SHM:Memory Plunder:memory plunder').
card_rarity('memory plunder'/'SHM', 'Rare').
card_artist('memory plunder'/'SHM', 'Dan Scott').
card_number('memory plunder'/'SHM', '169').
card_flavor_text('memory plunder'/'SHM', '\"Vengeance is the echo of the victim\'s own crime.\"\n—Grensch, merrow cutthroat').
card_multiverse_id('memory plunder'/'SHM', '146742').

card_in_set('memory sluice', 'SHM').
card_original_type('memory sluice'/'SHM', 'Sorcery').
card_original_text('memory sluice'/'SHM', 'Target player puts the top four cards of his or her library into his or her graveyard.\nConspire (As you play this spell, you may tap two untapped creatures you control that share a color with it. When you do, copy it and you may choose a new target for the copy.)').
card_first_print('memory sluice', 'SHM').
card_image_name('memory sluice'/'SHM', 'memory sluice').
card_uid('memory sluice'/'SHM', 'SHM:Memory Sluice:memory sluice').
card_rarity('memory sluice'/'SHM', 'Common').
card_artist('memory sluice'/'SHM', 'Wayne England').
card_number('memory sluice'/'SHM', '170').
card_multiverse_id('memory sluice'/'SHM', '158759').

card_in_set('mercy killing', 'SHM').
card_original_type('mercy killing'/'SHM', 'Instant').
card_original_text('mercy killing'/'SHM', 'Target creature\'s controller sacrifices it, then puts X 1/1 green and white Elf Warrior creature tokens into play, where X is that creature\'s power.').
card_first_print('mercy killing', 'SHM').
card_image_name('mercy killing'/'SHM', 'mercy killing').
card_uid('mercy killing'/'SHM', 'SHM:Mercy Killing:mercy killing').
card_rarity('mercy killing'/'SHM', 'Uncommon').
card_artist('mercy killing'/'SHM', 'Dave Kendall').
card_number('mercy killing'/'SHM', '231').
card_flavor_text('mercy killing'/'SHM', '\"We will give you the peace you seek, though you may not yet know you seek it.\"').
card_multiverse_id('mercy killing'/'SHM', '158771').

card_in_set('merrow grimeblotter', 'SHM').
card_original_type('merrow grimeblotter'/'SHM', 'Creature — Merfolk Wizard').
card_original_text('merrow grimeblotter'/'SHM', '{1}{U/B}, {Q}: Target creature gets -2/-0 until end of turn. ({Q} is the untap symbol.)').
card_first_print('merrow grimeblotter', 'SHM').
card_image_name('merrow grimeblotter'/'SHM', 'merrow grimeblotter').
card_uid('merrow grimeblotter'/'SHM', 'SHM:Merrow Grimeblotter:merrow grimeblotter').
card_rarity('merrow grimeblotter'/'SHM', 'Uncommon').
card_artist('merrow grimeblotter'/'SHM', 'Cyril Van Der Haegen').
card_number('merrow grimeblotter'/'SHM', '171').
card_flavor_text('merrow grimeblotter'/'SHM', 'Grimeblotters spend so much time in the Dark Meanders that they\'re able to bring a piece with them wherever they go.').
card_multiverse_id('merrow grimeblotter'/'SHM', '141937').

card_in_set('merrow wavebreakers', 'SHM').
card_original_type('merrow wavebreakers'/'SHM', 'Creature — Merfolk Soldier').
card_original_text('merrow wavebreakers'/'SHM', '{1}{U}, {Q}: Merrow Wavebreakers gains flying until end of turn. ({Q} is the untap symbol.)').
card_first_print('merrow wavebreakers', 'SHM').
card_image_name('merrow wavebreakers'/'SHM', 'merrow wavebreakers').
card_uid('merrow wavebreakers'/'SHM', 'SHM:Merrow Wavebreakers:merrow wavebreakers').
card_rarity('merrow wavebreakers'/'SHM', 'Common').
card_artist('merrow wavebreakers'/'SHM', 'Alex Horley-Orlandelli').
card_number('merrow wavebreakers'/'SHM', '44').
card_flavor_text('merrow wavebreakers'/'SHM', 'The merrows\' prey have retreated from the shore, so they have learned to follow.').
card_multiverse_id('merrow wavebreakers'/'SHM', '153319').

card_in_set('midnight banshee', 'SHM').
card_original_type('midnight banshee'/'SHM', 'Creature — Spirit').
card_original_text('midnight banshee'/'SHM', 'Wither (This deals damage to creatures in the form of -1/-1 counters.)\nAt the beginning of your upkeep, put a -1/-1 counter on each nonblack creature.').
card_first_print('midnight banshee', 'SHM').
card_image_name('midnight banshee'/'SHM', 'midnight banshee').
card_uid('midnight banshee'/'SHM', 'SHM:Midnight Banshee:midnight banshee').
card_rarity('midnight banshee'/'SHM', 'Rare').
card_artist('midnight banshee'/'SHM', 'Daarken').
card_number('midnight banshee'/'SHM', '72').
card_flavor_text('midnight banshee'/'SHM', 'Many have heard the beginning of its low, sustained shriek but few the end.').
card_multiverse_id('midnight banshee'/'SHM', '147431').

card_in_set('mine excavation', 'SHM').
card_original_type('mine excavation'/'SHM', 'Sorcery').
card_original_text('mine excavation'/'SHM', 'Return target artifact or enchantment card in a graveyard to its owner\'s hand.\nConspire (As you play this spell, you may tap two untapped creatures you control that share a color with it. When you do, copy it and you may choose a new target for the copy.)').
card_first_print('mine excavation', 'SHM').
card_image_name('mine excavation'/'SHM', 'mine excavation').
card_uid('mine excavation'/'SHM', 'SHM:Mine Excavation:mine excavation').
card_rarity('mine excavation'/'SHM', 'Common').
card_artist('mine excavation'/'SHM', 'Chippy').
card_number('mine excavation'/'SHM', '13').
card_multiverse_id('mine excavation'/'SHM', '158756').

card_in_set('mirrorweave', 'SHM').
card_original_type('mirrorweave'/'SHM', 'Instant').
card_original_text('mirrorweave'/'SHM', 'Each other creature becomes a copy of target nonlegendary creature until end of turn.').
card_first_print('mirrorweave', 'SHM').
card_image_name('mirrorweave'/'SHM', 'mirrorweave').
card_uid('mirrorweave'/'SHM', 'SHM:Mirrorweave:mirrorweave').
card_rarity('mirrorweave'/'SHM', 'Rare').
card_artist('mirrorweave'/'SHM', 'Jim Pavelec').
card_number('mirrorweave'/'SHM', '143').
card_flavor_text('mirrorweave'/'SHM', '\"Those who are different are untrustworthy, unpredictable. Put your safety in the hands of your own kind.\"\n—Bowen, Barrenton guardcaptain').
card_multiverse_id('mirrorweave'/'SHM', '153973').

card_in_set('mistmeadow skulk', 'SHM').
card_original_type('mistmeadow skulk'/'SHM', 'Creature — Kithkin Rogue').
card_original_text('mistmeadow skulk'/'SHM', 'Lifelink, protection from converted mana cost 3 or greater').
card_image_name('mistmeadow skulk'/'SHM', 'mistmeadow skulk').
card_uid('mistmeadow skulk'/'SHM', 'SHM:Mistmeadow Skulk:mistmeadow skulk').
card_rarity('mistmeadow skulk'/'SHM', 'Uncommon').
card_artist('mistmeadow skulk'/'SHM', 'Omar Rayyan').
card_number('mistmeadow skulk'/'SHM', '14').
card_flavor_text('mistmeadow skulk'/'SHM', 'Doyo suspected the boggarts of brewing a plot against his crop, so he scythed away his grain to clear the sightlines.').
card_multiverse_id('mistmeadow skulk'/'SHM', '158690').

card_in_set('mistmeadow witch', 'SHM').
card_original_type('mistmeadow witch'/'SHM', 'Creature — Kithkin Wizard').
card_original_text('mistmeadow witch'/'SHM', '{2}{W}{U}: Remove target creature from the game. Return that card to play under its owner\'s control at end of turn.').
card_first_print('mistmeadow witch', 'SHM').
card_image_name('mistmeadow witch'/'SHM', 'mistmeadow witch').
card_uid('mistmeadow witch'/'SHM', 'SHM:Mistmeadow Witch:mistmeadow witch').
card_rarity('mistmeadow witch'/'SHM', 'Uncommon').
card_artist('mistmeadow witch'/'SHM', 'Greg Staples').
card_number('mistmeadow witch'/'SHM', '144').
card_flavor_text('mistmeadow witch'/'SHM', 'Olka collected the evening mist for years, studying its secrets. Once she learned its essence, she could vanish with a thought.').
card_multiverse_id('mistmeadow witch'/'SHM', '157881').

card_in_set('mistveil plains', 'SHM').
card_original_type('mistveil plains'/'SHM', 'Land — Plains').
card_original_text('mistveil plains'/'SHM', '({T}: Add {W} to your mana pool.)\nMistveil Plains comes into play tapped.\n{W}, {T}: Put target card in your graveyard on the bottom of your library. Play this ability only if you control two or more white permanents.').
card_first_print('mistveil plains', 'SHM').
card_image_name('mistveil plains'/'SHM', 'mistveil plains').
card_uid('mistveil plains'/'SHM', 'SHM:Mistveil Plains:mistveil plains').
card_rarity('mistveil plains'/'SHM', 'Uncommon').
card_artist('mistveil plains'/'SHM', 'Rob Alexander').
card_number('mistveil plains'/'SHM', '275').
card_multiverse_id('mistveil plains'/'SHM', '142014').

card_in_set('moonring island', 'SHM').
card_original_type('moonring island'/'SHM', 'Land — Island').
card_original_text('moonring island'/'SHM', '({T}: Add {U} to your mana pool.)\nMoonring Island comes into play tapped.\n{U}, {T}: Look at the top card of target player\'s library. Play this ability only if you control two or more blue permanents.').
card_first_print('moonring island', 'SHM').
card_image_name('moonring island'/'SHM', 'moonring island').
card_uid('moonring island'/'SHM', 'SHM:Moonring Island:moonring island').
card_rarity('moonring island'/'SHM', 'Uncommon').
card_artist('moonring island'/'SHM', 'Brandon Kitkouski').
card_number('moonring island'/'SHM', '276').
card_multiverse_id('moonring island'/'SHM', '141943').

card_in_set('morselhoarder', 'SHM').
card_original_type('morselhoarder'/'SHM', 'Creature — Elemental').
card_original_text('morselhoarder'/'SHM', 'Morselhoarder comes into play with two -1/-1 counters on it.\nRemove a -1/-1 counter from Morselhoarder: Add one mana of any color to your mana pool.').
card_first_print('morselhoarder', 'SHM').
card_image_name('morselhoarder'/'SHM', 'morselhoarder').
card_uid('morselhoarder'/'SHM', 'SHM:Morselhoarder:morselhoarder').
card_rarity('morselhoarder'/'SHM', 'Common').
card_artist('morselhoarder'/'SHM', 'Anthony S. Waters').
card_number('morselhoarder'/'SHM', '212').
card_flavor_text('morselhoarder'/'SHM', 'It scours the hills for living matter, savoring even the tang of poisonous fungi.').
card_multiverse_id('morselhoarder'/'SHM', '159407').

card_in_set('mossbridge troll', 'SHM').
card_original_type('mossbridge troll'/'SHM', 'Creature — Troll').
card_original_text('mossbridge troll'/'SHM', 'If Mossbridge Troll would be destroyed, regenerate it.\nTap any number of untapped creatures you control other than Mossbridge Troll with total power 10 or greater: Mossbridge Troll gets +20/+20 until end of turn.').
card_first_print('mossbridge troll', 'SHM').
card_image_name('mossbridge troll'/'SHM', 'mossbridge troll').
card_uid('mossbridge troll'/'SHM', 'SHM:Mossbridge Troll:mossbridge troll').
card_rarity('mossbridge troll'/'SHM', 'Rare').
card_artist('mossbridge troll'/'SHM', 'Jeremy Jarvis').
card_number('mossbridge troll'/'SHM', '123').
card_multiverse_id('mossbridge troll'/'SHM', '146021').

card_in_set('mountain', 'SHM').
card_original_type('mountain'/'SHM', 'Basic Land — Mountain').
card_original_text('mountain'/'SHM', 'R').
card_image_name('mountain'/'SHM', 'mountain1').
card_uid('mountain'/'SHM', 'SHM:Mountain:mountain1').
card_rarity('mountain'/'SHM', 'Basic Land').
card_artist('mountain'/'SHM', 'Dave Kendall').
card_number('mountain'/'SHM', '294').
card_multiverse_id('mountain'/'SHM', '158240').

card_in_set('mountain', 'SHM').
card_original_type('mountain'/'SHM', 'Basic Land — Mountain').
card_original_text('mountain'/'SHM', 'R').
card_image_name('mountain'/'SHM', 'mountain2').
card_uid('mountain'/'SHM', 'SHM:Mountain:mountain2').
card_rarity('mountain'/'SHM', 'Basic Land').
card_artist('mountain'/'SHM', 'Brandon Kitkouski').
card_number('mountain'/'SHM', '295').
card_multiverse_id('mountain'/'SHM', '157888').

card_in_set('mountain', 'SHM').
card_original_type('mountain'/'SHM', 'Basic Land — Mountain').
card_original_text('mountain'/'SHM', 'R').
card_image_name('mountain'/'SHM', 'mountain3').
card_uid('mountain'/'SHM', 'SHM:Mountain:mountain3').
card_rarity('mountain'/'SHM', 'Basic Land').
card_artist('mountain'/'SHM', 'rk post').
card_number('mountain'/'SHM', '296').
card_multiverse_id('mountain'/'SHM', '157885').

card_in_set('mountain', 'SHM').
card_original_type('mountain'/'SHM', 'Basic Land — Mountain').
card_original_text('mountain'/'SHM', 'R').
card_image_name('mountain'/'SHM', 'mountain4').
card_uid('mountain'/'SHM', 'SHM:Mountain:mountain4').
card_rarity('mountain'/'SHM', 'Basic Land').
card_artist('mountain'/'SHM', 'Steve Prescott').
card_number('mountain'/'SHM', '297').
card_multiverse_id('mountain'/'SHM', '157882').

card_in_set('mudbrawler cohort', 'SHM').
card_original_type('mudbrawler cohort'/'SHM', 'Creature — Goblin Warrior').
card_original_text('mudbrawler cohort'/'SHM', 'Haste\nMudbrawler Cohort gets +1/+1 as long as you control another red creature.').
card_first_print('mudbrawler cohort', 'SHM').
card_image_name('mudbrawler cohort'/'SHM', 'mudbrawler cohort').
card_uid('mudbrawler cohort'/'SHM', 'SHM:Mudbrawler Cohort:mudbrawler cohort').
card_rarity('mudbrawler cohort'/'SHM', 'Common').
card_artist('mudbrawler cohort'/'SHM', 'Lucio Parrillo').
card_number('mudbrawler cohort'/'SHM', '100').
card_flavor_text('mudbrawler cohort'/'SHM', 'Before a raid, the members of Wort\'s gang egg each other on with tales of their hunger and what they\'ll do to sate it, each outdoing the last.').
card_multiverse_id('mudbrawler cohort'/'SHM', '146062').

card_in_set('mudbrawler raiders', 'SHM').
card_original_type('mudbrawler raiders'/'SHM', 'Creature — Goblin Warrior').
card_original_text('mudbrawler raiders'/'SHM', 'Mudbrawler Raiders can\'t be blocked by blue creatures.').
card_first_print('mudbrawler raiders', 'SHM').
card_image_name('mudbrawler raiders'/'SHM', 'mudbrawler raiders').
card_uid('mudbrawler raiders'/'SHM', 'SHM:Mudbrawler Raiders:mudbrawler raiders').
card_rarity('mudbrawler raiders'/'SHM', 'Common').
card_artist('mudbrawler raiders'/'SHM', 'Ron Spencer').
card_number('mudbrawler raiders'/'SHM', '213').
card_flavor_text('mudbrawler raiders'/'SHM', 'To reach the ravine of the Wanderbrine River, they were told to take the shortcut \"through the mountains.\" They took the directions literally.').
card_multiverse_id('mudbrawler raiders'/'SHM', '141992').

card_in_set('murderous redcap', 'SHM').
card_original_type('murderous redcap'/'SHM', 'Creature — Goblin Assassin').
card_original_text('murderous redcap'/'SHM', 'When Murderous Redcap comes into play, it deals damage equal to its power to target creature or player.\nPersist (When this creature is put into a graveyard from play, if it had no -1/-1 counters on it, return it to play under its owner\'s control with a -1/-1 counter on it.)').
card_image_name('murderous redcap'/'SHM', 'murderous redcap').
card_uid('murderous redcap'/'SHM', 'SHM:Murderous Redcap:murderous redcap').
card_rarity('murderous redcap'/'SHM', 'Uncommon').
card_artist('murderous redcap'/'SHM', 'Dave Allsop').
card_number('murderous redcap'/'SHM', '192').
card_multiverse_id('murderous redcap'/'SHM', '153298').

card_in_set('mystic gate', 'SHM').
card_original_type('mystic gate'/'SHM', 'Land').
card_original_text('mystic gate'/'SHM', '{T}: Add {1} to your mana pool.\n{W/U}, {T}: Add {W}{W}, {W}{U}, or {U}{U} to your mana pool.').
card_first_print('mystic gate', 'SHM').
card_image_name('mystic gate'/'SHM', 'mystic gate').
card_uid('mystic gate'/'SHM', 'SHM:Mystic Gate:mystic gate').
card_rarity('mystic gate'/'SHM', 'Rare').
card_artist('mystic gate'/'SHM', 'Fred Fields').
card_number('mystic gate'/'SHM', '277').
card_flavor_text('mystic gate'/'SHM', 'The gate of every kithkin doun is a cunning trap, intended to spill visitors into an oubliette from which there is no escape.').
card_multiverse_id('mystic gate'/'SHM', '146746').

card_in_set('niveous wisps', 'SHM').
card_original_type('niveous wisps'/'SHM', 'Instant').
card_original_text('niveous wisps'/'SHM', 'Target creature becomes white until end of turn. Tap that creature.\nDraw a card.').
card_first_print('niveous wisps', 'SHM').
card_image_name('niveous wisps'/'SHM', 'niveous wisps').
card_uid('niveous wisps'/'SHM', 'SHM:Niveous Wisps:niveous wisps').
card_rarity('niveous wisps'/'SHM', 'Common').
card_artist('niveous wisps'/'SHM', 'Jim Nelson').
card_number('niveous wisps'/'SHM', '15').
card_flavor_text('niveous wisps'/'SHM', 'In a world devoid of sun, illumination comes from cheerless fires or the wandering spirits of the dead.').
card_multiverse_id('niveous wisps'/'SHM', '158694').

card_in_set('nurturer initiate', 'SHM').
card_original_type('nurturer initiate'/'SHM', 'Creature — Elf Shaman').
card_original_text('nurturer initiate'/'SHM', 'Whenever a player plays a green spell, you may pay {1}. If you do, target creature gets +1/+1 until end of turn.').
card_first_print('nurturer initiate', 'SHM').
card_image_name('nurturer initiate'/'SHM', 'nurturer initiate').
card_uid('nurturer initiate'/'SHM', 'SHM:Nurturer Initiate:nurturer initiate').
card_rarity('nurturer initiate'/'SHM', 'Common').
card_artist('nurturer initiate'/'SHM', 'Jim Pavelec').
card_number('nurturer initiate'/'SHM', '124').
card_flavor_text('nurturer initiate'/'SHM', 'The elves alone preserve the few shreds of beauty left. There is no one else who cares.').
card_multiverse_id('nurturer initiate'/'SHM', '142035').

card_in_set('old ghastbark', 'SHM').
card_original_type('old ghastbark'/'SHM', 'Creature — Treefolk Warrior').
card_original_text('old ghastbark'/'SHM', '').
card_first_print('old ghastbark', 'SHM').
card_image_name('old ghastbark'/'SHM', 'old ghastbark').
card_uid('old ghastbark'/'SHM', 'SHM:Old Ghastbark:old ghastbark').
card_rarity('old ghastbark'/'SHM', 'Common').
card_artist('old ghastbark'/'SHM', 'Thomas M. Baxa').
card_number('old ghastbark'/'SHM', '232').
card_flavor_text('old ghastbark'/'SHM', '\"Beware of trees that talk. Their words are threats. And mind the ones that sway and creak. They too threaten us, but in a foreign tongue.\"\n—The Book of Other Folk').
card_multiverse_id('old ghastbark'/'SHM', '142007').

card_in_set('oona\'s gatewarden', 'SHM').
card_original_type('oona\'s gatewarden'/'SHM', 'Creature — Faerie Soldier').
card_original_text('oona\'s gatewarden'/'SHM', 'Flying, defender\nWither (This deals damage to creatures in the form of -1/-1 counters.)').
card_first_print('oona\'s gatewarden', 'SHM').
card_image_name('oona\'s gatewarden'/'SHM', 'oona\'s gatewarden').
card_uid('oona\'s gatewarden'/'SHM', 'SHM:Oona\'s Gatewarden:oona\'s gatewarden').
card_rarity('oona\'s gatewarden'/'SHM', 'Common').
card_artist('oona\'s gatewarden'/'SHM', 'Mike Dringenberg').
card_number('oona\'s gatewarden'/'SHM', '173').
card_flavor_text('oona\'s gatewarden'/'SHM', '\"So now you\'ve seen Glen Elendra. Take a good look. It will be the last thing you\'ll ever see.\"').
card_multiverse_id('oona\'s gatewarden'/'SHM', '141975').

card_in_set('oona, queen of the fae', 'SHM').
card_original_type('oona, queen of the fae'/'SHM', 'Legendary Creature — Faerie Wizard').
card_original_text('oona, queen of the fae'/'SHM', 'Flying\n{X}{U/B}: Choose a color. Target opponent removes the top X cards of his or her library from the game. For each card of the chosen color removed this way, put a 1/1 blue and black Faerie Rogue creature token with flying into play.').
card_first_print('oona, queen of the fae', 'SHM').
card_image_name('oona, queen of the fae'/'SHM', 'oona, queen of the fae').
card_uid('oona, queen of the fae'/'SHM', 'SHM:Oona, Queen of the Fae:oona, queen of the fae').
card_rarity('oona, queen of the fae'/'SHM', 'Rare').
card_artist('oona, queen of the fae'/'SHM', 'Adam Rex').
card_number('oona, queen of the fae'/'SHM', '172').
card_multiverse_id('oona, queen of the fae'/'SHM', '152063').

card_in_set('oracle of nectars', 'SHM').
card_original_type('oracle of nectars'/'SHM', 'Creature — Elf Cleric').
card_original_text('oracle of nectars'/'SHM', '{X}, {T}: You gain X life.').
card_first_print('oracle of nectars', 'SHM').
card_image_name('oracle of nectars'/'SHM', 'oracle of nectars').
card_uid('oracle of nectars'/'SHM', 'SHM:Oracle of Nectars:oracle of nectars').
card_rarity('oracle of nectars'/'SHM', 'Rare').
card_artist('oracle of nectars'/'SHM', 'Brandon Kitkouski').
card_number('oracle of nectars'/'SHM', '233').
card_flavor_text('oracle of nectars'/'SHM', 'When elves find a fount of beauty, they protect it. Where there is beauty, there is hope.').
card_multiverse_id('oracle of nectars'/'SHM', '159414').

card_in_set('order of whiteclay', 'SHM').
card_original_type('order of whiteclay'/'SHM', 'Creature — Kithkin Cleric').
card_original_text('order of whiteclay'/'SHM', '{1}{W}{W}, {Q}: Return target creature card with converted mana cost 3 or less from your graveyard to play. ({Q} is the untap symbol.)').
card_first_print('order of whiteclay', 'SHM').
card_image_name('order of whiteclay'/'SHM', 'order of whiteclay').
card_uid('order of whiteclay'/'SHM', 'SHM:Order of Whiteclay:order of whiteclay').
card_rarity('order of whiteclay'/'SHM', 'Rare').
card_artist('order of whiteclay'/'SHM', 'Steven Belledin').
card_number('order of whiteclay'/'SHM', '16').
card_flavor_text('order of whiteclay'/'SHM', 'Made from the clay of burial mounds, the face paint of the priests is a sign of their respect for those whose rest they interrupt.').
card_multiverse_id('order of whiteclay'/'SHM', '146007').

card_in_set('oversoul of dusk', 'SHM').
card_original_type('oversoul of dusk'/'SHM', 'Creature — Spirit Avatar').
card_original_text('oversoul of dusk'/'SHM', 'Protection from blue, from black, and from red').
card_first_print('oversoul of dusk', 'SHM').
card_image_name('oversoul of dusk'/'SHM', 'oversoul of dusk').
card_uid('oversoul of dusk'/'SHM', 'SHM:Oversoul of Dusk:oversoul of dusk').
card_rarity('oversoul of dusk'/'SHM', 'Rare').
card_artist('oversoul of dusk'/'SHM', 'Scott M. Fischer').
card_number('oversoul of dusk'/'SHM', '234').
card_flavor_text('oversoul of dusk'/'SHM', '\"Some say she hid the sun herself, a desperate act to save it from its ultimate extinction.\"\n—The Seer\'s Parables').
card_multiverse_id('oversoul of dusk'/'SHM', '146735').

card_in_set('painter\'s servant', 'SHM').
card_original_type('painter\'s servant'/'SHM', 'Artifact Creature — Scarecrow').
card_original_text('painter\'s servant'/'SHM', 'As Painter\'s Servant comes into play, choose a color.\nAll cards that aren\'t in play, spells, and permanents are the chosen color in addition to their other colors.').
card_first_print('painter\'s servant', 'SHM').
card_image_name('painter\'s servant'/'SHM', 'painter\'s servant').
card_uid('painter\'s servant'/'SHM', 'SHM:Painter\'s Servant:painter\'s servant').
card_rarity('painter\'s servant'/'SHM', 'Rare').
card_artist('painter\'s servant'/'SHM', 'Mike Dringenberg').
card_number('painter\'s servant'/'SHM', '257').
card_flavor_text('painter\'s servant'/'SHM', 'It gathers hues from the twilight mist so that its master can paint a better world.').
card_multiverse_id('painter\'s servant'/'SHM', '146022').

card_in_set('pale wayfarer', 'SHM').
card_original_type('pale wayfarer'/'SHM', 'Creature — Spirit Giant').
card_original_text('pale wayfarer'/'SHM', '{2}{W}{W}, {Q}: Target creature gains protection from the color of its controller\'s choice until end of turn. ({Q} is the untap symbol.)').
card_first_print('pale wayfarer', 'SHM').
card_image_name('pale wayfarer'/'SHM', 'pale wayfarer').
card_uid('pale wayfarer'/'SHM', 'SHM:Pale Wayfarer:pale wayfarer').
card_rarity('pale wayfarer'/'SHM', 'Uncommon').
card_artist('pale wayfarer'/'SHM', 'Heather Hudson').
card_number('pale wayfarer'/'SHM', '17').
card_flavor_text('pale wayfarer'/'SHM', 'As it wandered Shadowmoor, it did not remember its crime—only its shame.').
card_multiverse_id('pale wayfarer'/'SHM', '153310').

card_in_set('parapet watchers', 'SHM').
card_original_type('parapet watchers'/'SHM', 'Creature — Kithkin Soldier').
card_original_text('parapet watchers'/'SHM', '{W/U}: Parapet Watchers gets +0/+1 until end of turn.').
card_first_print('parapet watchers', 'SHM').
card_image_name('parapet watchers'/'SHM', 'parapet watchers').
card_uid('parapet watchers'/'SHM', 'SHM:Parapet Watchers:parapet watchers').
card_rarity('parapet watchers'/'SHM', 'Common').
card_artist('parapet watchers'/'SHM', 'Scott Altmann').
card_number('parapet watchers'/'SHM', '45').
card_flavor_text('parapet watchers'/'SHM', 'A kithkin doun is not so much a town as a fortress, built to withstand the constantly besieging darkness. Only those most watchful and trustworthy are tasked with guarding its walls.').
card_multiverse_id('parapet watchers'/'SHM', '152059').

card_in_set('pili-pala', 'SHM').
card_original_type('pili-pala'/'SHM', 'Artifact Creature — Scarecrow').
card_original_text('pili-pala'/'SHM', 'Flying\n{2}, {Q}: Add one mana of any color to your mana pool. ({Q} is the untap symbol.)').
card_first_print('pili-pala', 'SHM').
card_image_name('pili-pala'/'SHM', 'pili-pala').
card_uid('pili-pala'/'SHM', 'SHM:Pili-Pala:pili-pala').
card_rarity('pili-pala'/'SHM', 'Common').
card_artist('pili-pala'/'SHM', 'Ron Spencer').
card_number('pili-pala'/'SHM', '258').
card_flavor_text('pili-pala'/'SHM', 'It wasn\'t really expected to fly. Then again, it wasn\'t expected to move, either.').
card_multiverse_id('pili-pala'/'SHM', '147381').

card_in_set('plague of vermin', 'SHM').
card_original_type('plague of vermin'/'SHM', 'Sorcery').
card_original_text('plague of vermin'/'SHM', 'Starting with you, each player may pay any amount of life. Repeat this process until no one pays life. Each player puts a 1/1 black Rat creature token into play for each 1 life he or she paid this way.').
card_first_print('plague of vermin', 'SHM').
card_image_name('plague of vermin'/'SHM', 'plague of vermin').
card_uid('plague of vermin'/'SHM', 'SHM:Plague of Vermin:plague of vermin').
card_rarity('plague of vermin'/'SHM', 'Rare').
card_artist('plague of vermin'/'SHM', 'Carl Critchlow').
card_number('plague of vermin'/'SHM', '73').
card_multiverse_id('plague of vermin'/'SHM', '159406').

card_in_set('plains', 'SHM').
card_original_type('plains'/'SHM', 'Basic Land — Plains').
card_original_text('plains'/'SHM', 'W').
card_image_name('plains'/'SHM', 'plains1').
card_uid('plains'/'SHM', 'SHM:Plains:plains1').
card_rarity('plains'/'SHM', 'Basic Land').
card_artist('plains'/'SHM', 'Omar Rayyan').
card_number('plains'/'SHM', '282').
card_multiverse_id('plains'/'SHM', '158235').

card_in_set('plains', 'SHM').
card_original_type('plains'/'SHM', 'Basic Land — Plains').
card_original_text('plains'/'SHM', 'W').
card_image_name('plains'/'SHM', 'plains2').
card_uid('plains'/'SHM', 'SHM:Plains:plains2').
card_rarity('plains'/'SHM', 'Basic Land').
card_artist('plains'/'SHM', 'Lars Grant-West').
card_number('plains'/'SHM', '283').
card_multiverse_id('plains'/'SHM', '158236').

card_in_set('plains', 'SHM').
card_original_type('plains'/'SHM', 'Basic Land — Plains').
card_original_text('plains'/'SHM', 'W').
card_image_name('plains'/'SHM', 'plains3').
card_uid('plains'/'SHM', 'SHM:Plains:plains3').
card_rarity('plains'/'SHM', 'Basic Land').
card_artist('plains'/'SHM', 'Dave Kendall').
card_number('plains'/'SHM', '284').
card_multiverse_id('plains'/'SHM', '157887').

card_in_set('plains', 'SHM').
card_original_type('plains'/'SHM', 'Basic Land — Plains').
card_original_text('plains'/'SHM', 'W').
card_image_name('plains'/'SHM', 'plains4').
card_uid('plains'/'SHM', 'SHM:Plains:plains4').
card_rarity('plains'/'SHM', 'Basic Land').
card_artist('plains'/'SHM', 'Larry MacDougall').
card_number('plains'/'SHM', '285').
card_multiverse_id('plains'/'SHM', '157873').

card_in_set('plumeveil', 'SHM').
card_original_type('plumeveil'/'SHM', 'Creature — Elemental').
card_original_text('plumeveil'/'SHM', 'Flash\nFlying, defender').
card_first_print('plumeveil', 'SHM').
card_image_name('plumeveil'/'SHM', 'plumeveil').
card_uid('plumeveil'/'SHM', 'SHM:Plumeveil:plumeveil').
card_rarity('plumeveil'/'SHM', 'Uncommon').
card_artist('plumeveil'/'SHM', 'Nils Hamm').
card_number('plumeveil'/'SHM', '145').
card_flavor_text('plumeveil'/'SHM', '\"It was vast, a great sheet of soaring wings, and equally silent. It caught us unawares and blocked our view of the kithkin stronghold.\"\n—Grensch, merrow cutthroat').
card_multiverse_id('plumeveil'/'SHM', '153980').

card_in_set('poison the well', 'SHM').
card_original_type('poison the well'/'SHM', 'Sorcery').
card_original_text('poison the well'/'SHM', 'Destroy target land. Poison the Well deals 2 damage to that land\'s controller.').
card_first_print('poison the well', 'SHM').
card_image_name('poison the well'/'SHM', 'poison the well').
card_uid('poison the well'/'SHM', 'SHM:Poison the Well:poison the well').
card_rarity('poison the well'/'SHM', 'Common').
card_artist('poison the well'/'SHM', 'Zoltan Boros & Gabor Szikszai').
card_number('poison the well'/'SHM', '193').
card_flavor_text('poison the well'/'SHM', 'Wells that provide clean, unhaunted water are rare enough to be worth building an entire town around.').
card_multiverse_id('poison the well'/'SHM', '147427').

card_in_set('polluted bonds', 'SHM').
card_original_type('polluted bonds'/'SHM', 'Enchantment').
card_original_text('polluted bonds'/'SHM', 'Whenever a land comes into play under an opponent\'s control, that player loses 2 life and you gain 2 life.').
card_first_print('polluted bonds', 'SHM').
card_image_name('polluted bonds'/'SHM', 'polluted bonds').
card_uid('polluted bonds'/'SHM', 'SHM:Polluted Bonds:polluted bonds').
card_rarity('polluted bonds'/'SHM', 'Rare').
card_artist('polluted bonds'/'SHM', 'Michael Sutfin').
card_number('polluted bonds'/'SHM', '74').
card_flavor_text('polluted bonds'/'SHM', 'The mystic bond between spellcasters and the land is just as vulnerable to attack as the casters themselves.').
card_multiverse_id('polluted bonds'/'SHM', '146766').

card_in_set('power of fire', 'SHM').
card_original_type('power of fire'/'SHM', 'Enchantment — Aura').
card_original_text('power of fire'/'SHM', 'Enchant creature\nEnchanted creature has \"{T}: This creature deals 1 damage to target creature or player.\"').
card_first_print('power of fire', 'SHM').
card_image_name('power of fire'/'SHM', 'power of fire').
card_uid('power of fire'/'SHM', 'SHM:Power of Fire:power of fire').
card_rarity('power of fire'/'SHM', 'Common').
card_artist('power of fire'/'SHM', 'Trevor Hairsine').
card_number('power of fire'/'SHM', '101').
card_flavor_text('power of fire'/'SHM', 'The cinders believe their flame was stolen by one they call the Extinguisher. All who now wield fire draw their desperate wrath.').
card_multiverse_id('power of fire'/'SHM', '152159').

card_in_set('presence of gond', 'SHM').
card_original_type('presence of gond'/'SHM', 'Enchantment — Aura').
card_original_text('presence of gond'/'SHM', 'Enchant creature\nEnchanted creature has \"{T}: Put a 1/1 green Elf Warrior creature token into play.\"').
card_first_print('presence of gond', 'SHM').
card_image_name('presence of gond'/'SHM', 'presence of gond').
card_uid('presence of gond'/'SHM', 'SHM:Presence of Gond:presence of gond').
card_rarity('presence of gond'/'SHM', 'Common').
card_artist('presence of gond'/'SHM', 'Brandon Kitkouski').
card_number('presence of gond'/'SHM', '125').
card_flavor_text('presence of gond'/'SHM', '\"Here lies Gond, hero of Safehold Taldwen. May he ever guide our quest.\"').
card_multiverse_id('presence of gond'/'SHM', '158768').

card_in_set('prismatic omen', 'SHM').
card_original_type('prismatic omen'/'SHM', 'Enchantment').
card_original_text('prismatic omen'/'SHM', 'Lands you control are every basic land type in addition to their other types.').
card_first_print('prismatic omen', 'SHM').
card_image_name('prismatic omen'/'SHM', 'prismatic omen').
card_uid('prismatic omen'/'SHM', 'SHM:Prismatic Omen:prismatic omen').
card_rarity('prismatic omen'/'SHM', 'Rare').
card_artist('prismatic omen'/'SHM', 'John Avon').
card_number('prismatic omen'/'SHM', '126').
card_flavor_text('prismatic omen'/'SHM', 'In times of portent, the land sculpts itself in accordance with the sigils burned on the sky.').
card_multiverse_id('prismatic omen'/'SHM', '151989').

card_in_set('prismwake merrow', 'SHM').
card_original_type('prismwake merrow'/'SHM', 'Creature — Merfolk Wizard').
card_original_text('prismwake merrow'/'SHM', 'Flash\nWhen Prismwake Merrow comes into play, target permanent becomes the color or colors of your choice until end of turn.').
card_first_print('prismwake merrow', 'SHM').
card_image_name('prismwake merrow'/'SHM', 'prismwake merrow').
card_uid('prismwake merrow'/'SHM', 'SHM:Prismwake Merrow:prismwake merrow').
card_rarity('prismwake merrow'/'SHM', 'Common').
card_artist('prismwake merrow'/'SHM', 'Wayne England').
card_number('prismwake merrow'/'SHM', '46').
card_flavor_text('prismwake merrow'/'SHM', '\"Seeing is believing\" is a fact the merrows ruthlessly exploit in their dealings with the other residents of Shadowmoor.').
card_multiverse_id('prismwake merrow'/'SHM', '154402').

card_in_set('prison term', 'SHM').
card_original_type('prison term'/'SHM', 'Enchantment — Aura').
card_original_text('prison term'/'SHM', 'Enchant creature\nEnchanted creature can\'t attack or block and its activated abilities can\'t be played.\nWhenever a creature comes into play under an opponent\'s control, you may attach Prison Term to that creature.').
card_first_print('prison term', 'SHM').
card_image_name('prison term'/'SHM', 'prison term').
card_uid('prison term'/'SHM', 'SHM:Prison Term:prison term').
card_rarity('prison term'/'SHM', 'Uncommon').
card_artist('prison term'/'SHM', 'Zoltan Boros & Gabor Szikszai').
card_number('prison term'/'SHM', '18').
card_flavor_text('prison term'/'SHM', 'Clear out the cell. Bring in the next prisoner.').
card_multiverse_id('prison term'/'SHM', '146001').

card_in_set('puca\'s mischief', 'SHM').
card_original_type('puca\'s mischief'/'SHM', 'Enchantment').
card_original_text('puca\'s mischief'/'SHM', 'At the beginning of your upkeep, you may exchange control of target nonland permanent you control and target nonland permanent an opponent controls with an equal or lesser converted mana cost.').
card_first_print('puca\'s mischief', 'SHM').
card_image_name('puca\'s mischief'/'SHM', 'puca\'s mischief').
card_uid('puca\'s mischief'/'SHM', 'SHM:Puca\'s Mischief:puca\'s mischief').
card_rarity('puca\'s mischief'/'SHM', 'Rare').
card_artist('puca\'s mischief'/'SHM', 'Scott Altmann').
card_number('puca\'s mischief'/'SHM', '47').
card_multiverse_id('puca\'s mischief'/'SHM', '141996').

card_in_set('puncture bolt', 'SHM').
card_original_type('puncture bolt'/'SHM', 'Instant').
card_original_text('puncture bolt'/'SHM', 'Puncture Bolt deals 1 damage to target creature. Put a -1/-1 counter on that creature.').
card_first_print('puncture bolt', 'SHM').
card_image_name('puncture bolt'/'SHM', 'puncture bolt').
card_uid('puncture bolt'/'SHM', 'SHM:Puncture Bolt:puncture bolt').
card_rarity('puncture bolt'/'SHM', 'Common').
card_artist('puncture bolt'/'SHM', 'Franz Vohwinkel').
card_number('puncture bolt'/'SHM', '102').
card_flavor_text('puncture bolt'/'SHM', 'Cinder pyromancers measure their success by counting their enemies\' smoking holes.').
card_multiverse_id('puncture bolt'/'SHM', '146071').

card_in_set('puppeteer clique', 'SHM').
card_original_type('puppeteer clique'/'SHM', 'Creature — Faerie Wizard').
card_original_text('puppeteer clique'/'SHM', 'Flying\nWhen Puppeteer Clique comes into play, put target creature card in an opponent\'s graveyard into play under your control. It has haste. At the end of your turn, remove it from the game.\nPersist (When this creature is put into a graveyard from play, if it had no -1/-1 counters on it, return it to play under its owner\'s control with a -1/-1 counter on it.)').
card_first_print('puppeteer clique', 'SHM').
card_image_name('puppeteer clique'/'SHM', 'puppeteer clique').
card_uid('puppeteer clique'/'SHM', 'SHM:Puppeteer Clique:puppeteer clique').
card_rarity('puppeteer clique'/'SHM', 'Rare').
card_artist('puppeteer clique'/'SHM', 'Daren Bader').
card_number('puppeteer clique'/'SHM', '75').
card_multiverse_id('puppeteer clique'/'SHM', '146761').

card_in_set('puresight merrow', 'SHM').
card_original_type('puresight merrow'/'SHM', 'Creature — Merfolk Wizard').
card_original_text('puresight merrow'/'SHM', '{W/U}, {Q}: Look at the top card of your library. You may remove that card from the game. ({Q} is the untap symbol.)').
card_first_print('puresight merrow', 'SHM').
card_image_name('puresight merrow'/'SHM', 'puresight merrow').
card_uid('puresight merrow'/'SHM', 'SHM:Puresight Merrow:puresight merrow').
card_rarity('puresight merrow'/'SHM', 'Uncommon').
card_artist('puresight merrow'/'SHM', 'Carl Critchlow').
card_number('puresight merrow'/'SHM', '146').
card_flavor_text('puresight merrow'/'SHM', 'Stripped of his sight, he was free to see only the ideal.').
card_multiverse_id('puresight merrow'/'SHM', '158745').

card_in_set('put away', 'SHM').
card_original_type('put away'/'SHM', 'Instant').
card_original_text('put away'/'SHM', 'Counter target spell. You may shuffle up to one target card from your graveyard into your library.').
card_first_print('put away', 'SHM').
card_image_name('put away'/'SHM', 'put away').
card_uid('put away'/'SHM', 'SHM:Put Away:put away').
card_rarity('put away'/'SHM', 'Common').
card_artist('put away'/'SHM', 'Matt Cavotta').
card_number('put away'/'SHM', '48').
card_flavor_text('put away'/'SHM', 'Kithkin spellbottlers spend half their time preserving old memories and the other half punishing the new.').
card_multiverse_id('put away'/'SHM', '159398').

card_in_set('pyre charger', 'SHM').
card_original_type('pyre charger'/'SHM', 'Creature — Elemental Warrior').
card_original_text('pyre charger'/'SHM', 'Haste\n{R}: Pyre Charger gets +1/+0 until end of turn.').
card_first_print('pyre charger', 'SHM').
card_image_name('pyre charger'/'SHM', 'pyre charger').
card_uid('pyre charger'/'SHM', 'SHM:Pyre Charger:pyre charger').
card_rarity('pyre charger'/'SHM', 'Uncommon').
card_artist('pyre charger'/'SHM', 'Mark Zug').
card_number('pyre charger'/'SHM', '103').
card_flavor_text('pyre charger'/'SHM', 'His blade was forged over coals of moaning treefolk, curved at the optimum angle for severing heads, and heated to volcanic temperatures by his touch.').
card_multiverse_id('pyre charger'/'SHM', '159399').

card_in_set('rage reflection', 'SHM').
card_original_type('rage reflection'/'SHM', 'Enchantment').
card_original_text('rage reflection'/'SHM', 'Creatures you control have double strike.').
card_first_print('rage reflection', 'SHM').
card_image_name('rage reflection'/'SHM', 'rage reflection').
card_uid('rage reflection'/'SHM', 'SHM:Rage Reflection:rage reflection').
card_rarity('rage reflection'/'SHM', 'Rare').
card_artist('rage reflection'/'SHM', 'Terese Nielsen & Ron Spencer').
card_number('rage reflection'/'SHM', '104').
card_flavor_text('rage reflection'/'SHM', 'Vengeance is a dish best served twice.').
card_multiverse_id('rage reflection'/'SHM', '146727').

card_in_set('raking canopy', 'SHM').
card_original_type('raking canopy'/'SHM', 'Enchantment').
card_original_text('raking canopy'/'SHM', 'Whenever a creature with flying attacks you, Raking Canopy deals 4 damage to it.').
card_first_print('raking canopy', 'SHM').
card_image_name('raking canopy'/'SHM', 'raking canopy').
card_uid('raking canopy'/'SHM', 'SHM:Raking Canopy:raking canopy').
card_rarity('raking canopy'/'SHM', 'Uncommon').
card_artist('raking canopy'/'SHM', 'Heather Hudson').
card_number('raking canopy'/'SHM', '127').
card_flavor_text('raking canopy'/'SHM', '\"Raven\'s Run has awoken, and ravens will no longer cry in its branches.\"\n—Roon Ghastbark').
card_multiverse_id('raking canopy'/'SHM', '158691').

card_in_set('rattleblaze scarecrow', 'SHM').
card_original_type('rattleblaze scarecrow'/'SHM', 'Artifact Creature — Scarecrow').
card_original_text('rattleblaze scarecrow'/'SHM', 'Rattleblaze Scarecrow has persist as long as you control a black creature. (When this creature is put into a graveyard from play, if it had no -1/-1 counters on it, return it to play under its owner\'s control with a -1/-1 counter on it.)\nRattleblaze Scarecrow has haste as long as you control a red creature.').
card_first_print('rattleblaze scarecrow', 'SHM').
card_image_name('rattleblaze scarecrow'/'SHM', 'rattleblaze scarecrow').
card_uid('rattleblaze scarecrow'/'SHM', 'SHM:Rattleblaze Scarecrow:rattleblaze scarecrow').
card_rarity('rattleblaze scarecrow'/'SHM', 'Common').
card_artist('rattleblaze scarecrow'/'SHM', 'Trevor Hairsine').
card_number('rattleblaze scarecrow'/'SHM', '259').
card_multiverse_id('rattleblaze scarecrow'/'SHM', '151631').

card_in_set('raven\'s run dragoon', 'SHM').
card_original_type('raven\'s run dragoon'/'SHM', 'Creature — Elf Knight').
card_original_text('raven\'s run dragoon'/'SHM', 'Raven\'s Run Dragoon can\'t be blocked by black creatures.').
card_first_print('raven\'s run dragoon', 'SHM').
card_image_name('raven\'s run dragoon'/'SHM', 'raven\'s run dragoon').
card_uid('raven\'s run dragoon'/'SHM', 'SHM:Raven\'s Run Dragoon:raven\'s run dragoon').
card_rarity('raven\'s run dragoon'/'SHM', 'Common').
card_artist('raven\'s run dragoon'/'SHM', 'Daren Bader').
card_number('raven\'s run dragoon'/'SHM', '235').
card_flavor_text('raven\'s run dragoon'/'SHM', '\"I have a gift. The ability to sense encroaching darkness has saved many lives. And yet constantly feeling the force of so much ugliness is a terrible burden.\"').
card_multiverse_id('raven\'s run dragoon'/'SHM', '142037').

card_in_set('reaper king', 'SHM').
card_original_type('reaper king'/'SHM', 'Legendary Artifact Creature — Scarecrow').
card_original_text('reaper king'/'SHM', '({2/W} can be paid with any two mana or with {W}. This card\'s converted mana cost is 10.)\nOther Scarecrow creatures you control get +1/+1.\nWhenever another Scarecrow comes into play under your control, destroy target permanent.').
card_first_print('reaper king', 'SHM').
card_image_name('reaper king'/'SHM', 'reaper king').
card_uid('reaper king'/'SHM', 'SHM:Reaper King:reaper king').
card_rarity('reaper king'/'SHM', 'Rare').
card_artist('reaper king'/'SHM', 'Jim Murray').
card_number('reaper king'/'SHM', '260').
card_flavor_text('reaper king'/'SHM', 'It\'s harvest time.').
card_multiverse_id('reaper king'/'SHM', '159408').

card_in_set('reflecting pool', 'SHM').
card_original_type('reflecting pool'/'SHM', 'Land').
card_original_text('reflecting pool'/'SHM', '{T}: Add to your mana pool one mana of any type that a land you control could produce.').
card_image_name('reflecting pool'/'SHM', 'reflecting pool').
card_uid('reflecting pool'/'SHM', 'SHM:Reflecting Pool:reflecting pool').
card_rarity('reflecting pool'/'SHM', 'Rare').
card_artist('reflecting pool'/'SHM', 'Fred Fields').
card_number('reflecting pool'/'SHM', '278').
card_flavor_text('reflecting pool'/'SHM', 'Does it reflect the future that once was or the past that can never be?').
card_multiverse_id('reflecting pool'/'SHM', '152158').

card_in_set('reknit', 'SHM').
card_original_type('reknit'/'SHM', 'Instant').
card_original_text('reknit'/'SHM', 'Regenerate target permanent.').
card_first_print('reknit', 'SHM').
card_image_name('reknit'/'SHM', 'reknit').
card_uid('reknit'/'SHM', 'SHM:Reknit:reknit').
card_rarity('reknit'/'SHM', 'Uncommon').
card_artist('reknit'/'SHM', 'Zoltan Boros & Gabor Szikszai').
card_number('reknit'/'SHM', '236').
card_flavor_text('reknit'/'SHM', '\"An axe may break upon a ribbon if the ribbon\'s will is the stronger.\"\n—Awylla, elvish safewright').
card_multiverse_id('reknit'/'SHM', '154403').

card_in_set('repel intruders', 'SHM').
card_original_type('repel intruders'/'SHM', 'Instant').
card_original_text('repel intruders'/'SHM', 'Put two 1/1 white Kithkin Soldier creature tokens into play if {W} was spent to play Repel Intruders. Counter up to one target creature spell if {U} was spent to play Repel Intruders. (Do both if {W}{U} was spent.)').
card_first_print('repel intruders', 'SHM').
card_image_name('repel intruders'/'SHM', 'repel intruders').
card_uid('repel intruders'/'SHM', 'SHM:Repel Intruders:repel intruders').
card_rarity('repel intruders'/'SHM', 'Uncommon').
card_artist('repel intruders'/'SHM', 'Trevor Hairsine').
card_number('repel intruders'/'SHM', '147').
card_multiverse_id('repel intruders'/'SHM', '158761').

card_in_set('resplendent mentor', 'SHM').
card_original_type('resplendent mentor'/'SHM', 'Creature — Kithkin Cleric').
card_original_text('resplendent mentor'/'SHM', 'White creatures you control have \"{T}: You gain 1 life.\"').
card_first_print('resplendent mentor', 'SHM').
card_image_name('resplendent mentor'/'SHM', 'resplendent mentor').
card_uid('resplendent mentor'/'SHM', 'SHM:Resplendent Mentor:resplendent mentor').
card_rarity('resplendent mentor'/'SHM', 'Uncommon').
card_artist('resplendent mentor'/'SHM', 'Franz Vohwinkel').
card_number('resplendent mentor'/'SHM', '19').
card_flavor_text('resplendent mentor'/'SHM', 'Thoughtweft gives new meaning to the phrase \"common knowledge.\"').
card_multiverse_id('resplendent mentor'/'SHM', '141930').

card_in_set('revelsong horn', 'SHM').
card_original_type('revelsong horn'/'SHM', 'Artifact').
card_original_text('revelsong horn'/'SHM', '{1}, {T}, Tap an untapped creature you control: Target creature gets +1/+1 until end of turn.').
card_first_print('revelsong horn', 'SHM').
card_image_name('revelsong horn'/'SHM', 'revelsong horn').
card_uid('revelsong horn'/'SHM', 'SHM:Revelsong Horn:revelsong horn').
card_rarity('revelsong horn'/'SHM', 'Uncommon').
card_artist('revelsong horn'/'SHM', 'Franz Vohwinkel').
card_number('revelsong horn'/'SHM', '261').
card_flavor_text('revelsong horn'/'SHM', 'A deflated sigh breathed into the horn emerges as an inspiring melody.').
card_multiverse_id('revelsong horn'/'SHM', '146047').

card_in_set('rhys the redeemed', 'SHM').
card_original_type('rhys the redeemed'/'SHM', 'Legendary Creature — Elf Warrior').
card_original_text('rhys the redeemed'/'SHM', '{2}{G/W}, {T}: Put a 1/1 green and white Elf Warrior creature token into play.\n{4}{G/W}{G/W}, {T}: For each creature token you control, put a token into play that\'s a copy of that creature.').
card_first_print('rhys the redeemed', 'SHM').
card_image_name('rhys the redeemed'/'SHM', 'rhys the redeemed').
card_uid('rhys the redeemed'/'SHM', 'SHM:Rhys the Redeemed:rhys the redeemed').
card_rarity('rhys the redeemed'/'SHM', 'Rare').
card_artist('rhys the redeemed'/'SHM', 'Steve Prescott').
card_number('rhys the redeemed'/'SHM', '237').
card_flavor_text('rhys the redeemed'/'SHM', 'Whole again in honor and horn.').
card_multiverse_id('rhys the redeemed'/'SHM', '147393').

card_in_set('rite of consumption', 'SHM').
card_original_type('rite of consumption'/'SHM', 'Sorcery').
card_original_text('rite of consumption'/'SHM', 'As an additional cost to play Rite of Consumption, sacrifice a creature.\nRite of Consumption deals damage equal to the sacrificed creature\'s power to target player. You gain life equal to the damage dealt this way.').
card_first_print('rite of consumption', 'SHM').
card_image_name('rite of consumption'/'SHM', 'rite of consumption').
card_uid('rite of consumption'/'SHM', 'SHM:Rite of Consumption:rite of consumption').
card_rarity('rite of consumption'/'SHM', 'Common').
card_artist('rite of consumption'/'SHM', 'Ron Spencer').
card_number('rite of consumption'/'SHM', '76').
card_multiverse_id('rite of consumption'/'SHM', '159400').

card_in_set('river kelpie', 'SHM').
card_original_type('river kelpie'/'SHM', 'Creature — Beast').
card_original_text('river kelpie'/'SHM', 'Whenever River Kelpie or another permanent is put into play from a graveyard, draw a card.\nWhenever a spell is played from a graveyard, draw a card.\nPersist (When this creature is put into a graveyard from play, if it had no -1/-1 counters on it, return it to play under its owner\'s control with a -1/-1 counter on it.)').
card_first_print('river kelpie', 'SHM').
card_image_name('river kelpie'/'SHM', 'river kelpie').
card_uid('river kelpie'/'SHM', 'SHM:River Kelpie:river kelpie').
card_rarity('river kelpie'/'SHM', 'Rare').
card_artist('river kelpie'/'SHM', 'Jeff Easley').
card_number('river kelpie'/'SHM', '49').
card_multiverse_id('river kelpie'/'SHM', '146094').

card_in_set('river\'s grasp', 'SHM').
card_original_type('river\'s grasp'/'SHM', 'Sorcery').
card_original_text('river\'s grasp'/'SHM', 'If {U} was spent to play River\'s Grasp, return up to one target creature to its owner\'s hand. If {B} was spent to play River\'s Grasp, target player reveals his or her hand, you choose a nonland card from it, then that player discards that card. (Do both if {U}{B} was spent.)').
card_first_print('river\'s grasp', 'SHM').
card_image_name('river\'s grasp'/'SHM', 'river\'s grasp').
card_uid('river\'s grasp'/'SHM', 'SHM:River\'s Grasp:river\'s grasp').
card_rarity('river\'s grasp'/'SHM', 'Uncommon').
card_artist('river\'s grasp'/'SHM', 'Steven Belledin').
card_number('river\'s grasp'/'SHM', '174').
card_multiverse_id('river\'s grasp'/'SHM', '158755').

card_in_set('rosheen meanderer', 'SHM').
card_original_type('rosheen meanderer'/'SHM', 'Legendary Creature — Giant Shaman').
card_original_text('rosheen meanderer'/'SHM', '{T}: Add {4} to your mana pool. Spend this mana only on costs that contain {X}.').
card_first_print('rosheen meanderer', 'SHM').
card_image_name('rosheen meanderer'/'SHM', 'rosheen meanderer').
card_uid('rosheen meanderer'/'SHM', 'SHM:Rosheen Meanderer:rosheen meanderer').
card_rarity('rosheen meanderer'/'SHM', 'Rare').
card_artist('rosheen meanderer'/'SHM', 'Aleksi Briclot').
card_number('rosheen meanderer'/'SHM', '214').
card_flavor_text('rosheen meanderer'/'SHM', 'Night after night, Rosheen babbled about a bygone sunlit world, her every word dismissed as a madwoman\'s ravings.').
card_multiverse_id('rosheen meanderer'/'SHM', '159412').

card_in_set('roughshod mentor', 'SHM').
card_original_type('roughshod mentor'/'SHM', 'Creature — Giant Warrior').
card_original_text('roughshod mentor'/'SHM', 'Green creatures you control have trample.').
card_first_print('roughshod mentor', 'SHM').
card_image_name('roughshod mentor'/'SHM', 'roughshod mentor').
card_uid('roughshod mentor'/'SHM', 'SHM:Roughshod Mentor:roughshod mentor').
card_rarity('roughshod mentor'/'SHM', 'Uncommon').
card_artist('roughshod mentor'/'SHM', 'Steven Belledin').
card_number('roughshod mentor'/'SHM', '128').
card_flavor_text('roughshod mentor'/'SHM', 'He didn\'t hear the cries of the treefolk whose branches he snapped or of the elves caught underfoot. He had eyes only for the path ahead.').
card_multiverse_id('roughshod mentor'/'SHM', '141972').

card_in_set('rune-cervin rider', 'SHM').
card_original_type('rune-cervin rider'/'SHM', 'Creature — Elf Knight').
card_original_text('rune-cervin rider'/'SHM', 'Flying\n{G/W}{G/W}: Rune-Cervin Rider gets +1/+1 until end of turn.').
card_first_print('rune-cervin rider', 'SHM').
card_image_name('rune-cervin rider'/'SHM', 'rune-cervin rider').
card_uid('rune-cervin rider'/'SHM', 'SHM:Rune-Cervin Rider:rune-cervin rider').
card_rarity('rune-cervin rider'/'SHM', 'Common').
card_artist('rune-cervin rider'/'SHM', 'Dan Scott').
card_number('rune-cervin rider'/'SHM', '20').
card_flavor_text('rune-cervin rider'/'SHM', 'Things of beauty are in constant peril. The riders whisk them to safety, ahead of the encroaching darkness.').
card_multiverse_id('rune-cervin rider'/'SHM', '142003').

card_in_set('runed halo', 'SHM').
card_original_type('runed halo'/'SHM', 'Enchantment').
card_original_text('runed halo'/'SHM', 'As Runed Halo comes into play, name a card.\nYou have protection from the chosen name. (You can\'t be targeted, dealt damage, or enchanted by anything with that name.)').
card_first_print('runed halo', 'SHM').
card_image_name('runed halo'/'SHM', 'runed halo').
card_uid('runed halo'/'SHM', 'SHM:Runed Halo:runed halo').
card_rarity('runed halo'/'SHM', 'Rare').
card_artist('runed halo'/'SHM', 'Steve Prescott').
card_number('runed halo'/'SHM', '21').
card_multiverse_id('runed halo'/'SHM', '154005').

card_in_set('runes of the deus', 'SHM').
card_original_type('runes of the deus'/'SHM', 'Enchantment — Aura').
card_original_text('runes of the deus'/'SHM', 'Enchant creature\nAs long as enchanted creature is red, it gets +1/+1 and has double strike. (It deals both first-strike and regular combat damage.)\nAs long as enchanted creature is green, it gets +1/+1 and has trample.').
card_first_print('runes of the deus', 'SHM').
card_image_name('runes of the deus'/'SHM', 'runes of the deus').
card_uid('runes of the deus'/'SHM', 'SHM:Runes of the Deus:runes of the deus').
card_rarity('runes of the deus'/'SHM', 'Common').
card_artist('runes of the deus'/'SHM', 'Warren Mahy').
card_number('runes of the deus'/'SHM', '215').
card_multiverse_id('runes of the deus'/'SHM', '158754').

card_in_set('rustrazor butcher', 'SHM').
card_original_type('rustrazor butcher'/'SHM', 'Creature — Goblin Warrior').
card_original_text('rustrazor butcher'/'SHM', 'First strike\nWither (This deals damage to creatures in the form of -1/-1 counters.)').
card_first_print('rustrazor butcher', 'SHM').
card_image_name('rustrazor butcher'/'SHM', 'rustrazor butcher').
card_uid('rustrazor butcher'/'SHM', 'SHM:Rustrazor Butcher:rustrazor butcher').
card_rarity('rustrazor butcher'/'SHM', 'Common').
card_artist('rustrazor butcher'/'SHM', 'Ron Spencer').
card_number('rustrazor butcher'/'SHM', '105').
card_flavor_text('rustrazor butcher'/'SHM', 'A Bloodwort\'s blade is salted with blood and peppered with rust, seasoning and slaughtering in a single swipe.').
card_multiverse_id('rustrazor butcher'/'SHM', '147389').

card_in_set('safehold duo', 'SHM').
card_original_type('safehold duo'/'SHM', 'Creature — Elf Warrior Shaman').
card_original_text('safehold duo'/'SHM', 'Whenever you play a green spell, Safehold Duo gets +1/+1 until end of turn.\nWhenever you play a white spell, Safehold Duo gains vigilance until end of turn.').
card_first_print('safehold duo', 'SHM').
card_image_name('safehold duo'/'SHM', 'safehold duo').
card_uid('safehold duo'/'SHM', 'SHM:Safehold Duo:safehold duo').
card_rarity('safehold duo'/'SHM', 'Common').
card_artist('safehold duo'/'SHM', 'Izzy').
card_number('safehold duo'/'SHM', '238').
card_multiverse_id('safehold duo'/'SHM', '153963').

card_in_set('safehold elite', 'SHM').
card_original_type('safehold elite'/'SHM', 'Creature — Elf Scout').
card_original_text('safehold elite'/'SHM', 'Persist (When this creature is put into a graveyard from play, if it had no -1/-1 counters on it, return it to play under its owner\'s control with a -1/-1 counter on it.)').
card_first_print('safehold elite', 'SHM').
card_image_name('safehold elite'/'SHM', 'safehold elite').
card_uid('safehold elite'/'SHM', 'SHM:Safehold Elite:safehold elite').
card_rarity('safehold elite'/'SHM', 'Common').
card_artist('safehold elite'/'SHM', 'Richard Whitters').
card_number('safehold elite'/'SHM', '239').
card_flavor_text('safehold elite'/'SHM', '\"I refuse to die—not at the hands of one such as you.\"').
card_multiverse_id('safehold elite'/'SHM', '146077').

card_in_set('safehold sentry', 'SHM').
card_original_type('safehold sentry'/'SHM', 'Creature — Elf Warrior').
card_original_text('safehold sentry'/'SHM', '{2}{W}, {Q}: Safehold Sentry gets +0/+2 until end of turn. ({Q} is the untap symbol.)').
card_first_print('safehold sentry', 'SHM').
card_image_name('safehold sentry'/'SHM', 'safehold sentry').
card_uid('safehold sentry'/'SHM', 'SHM:Safehold Sentry:safehold sentry').
card_rarity('safehold sentry'/'SHM', 'Common').
card_artist('safehold sentry'/'SHM', 'William O\'Connor').
card_number('safehold sentry'/'SHM', '22').
card_flavor_text('safehold sentry'/'SHM', '\"These bracers were worn by my father and by his mother before him. Boggart fangs have shattered on them. Cinder flames have withered at their touch. While I wear them, the safehold will not fall.\"').
card_multiverse_id('safehold sentry'/'SHM', '147394').

card_in_set('safewright quest', 'SHM').
card_original_type('safewright quest'/'SHM', 'Sorcery').
card_original_text('safewright quest'/'SHM', 'Search your library for a Forest or Plains card, reveal it, and put it into your hand. Then shuffle your library.').
card_first_print('safewright quest', 'SHM').
card_image_name('safewright quest'/'SHM', 'safewright quest').
card_uid('safewright quest'/'SHM', 'SHM:Safewright Quest:safewright quest').
card_rarity('safewright quest'/'SHM', 'Common').
card_artist('safewright quest'/'SHM', 'Dan Scott').
card_number('safewright quest'/'SHM', '240').
card_flavor_text('safewright quest'/'SHM', 'Elves search for sources of beauty in a nighted world. Meanwhile, the nighted world searches for the blood of elves.').
card_multiverse_id('safewright quest'/'SHM', '142038').

card_in_set('sapseep forest', 'SHM').
card_original_type('sapseep forest'/'SHM', 'Land — Forest').
card_original_text('sapseep forest'/'SHM', '({T}: Add {G} to your mana pool.)\nSapseep Forest comes into play tapped.\n{G}, {T}: You gain 1 life. Play this ability only if you control two or more green permanents.').
card_first_print('sapseep forest', 'SHM').
card_image_name('sapseep forest'/'SHM', 'sapseep forest').
card_uid('sapseep forest'/'SHM', 'SHM:Sapseep Forest:sapseep forest').
card_rarity('sapseep forest'/'SHM', 'Uncommon').
card_artist('sapseep forest'/'SHM', 'Aleksi Briclot').
card_number('sapseep forest'/'SHM', '279').
card_multiverse_id('sapseep forest'/'SHM', '142048').

card_in_set('savor the moment', 'SHM').
card_original_type('savor the moment'/'SHM', 'Sorcery').
card_original_text('savor the moment'/'SHM', 'Take an extra turn after this one. Skip the untap step of that turn.').
card_first_print('savor the moment', 'SHM').
card_image_name('savor the moment'/'SHM', 'savor the moment').
card_uid('savor the moment'/'SHM', 'SHM:Savor the Moment:savor the moment').
card_rarity('savor the moment'/'SHM', 'Rare').
card_artist('savor the moment'/'SHM', 'Warren Mahy').
card_number('savor the moment'/'SHM', '50').
card_flavor_text('savor the moment'/'SHM', 'The merrow took a moment for herself. She never gave it back.').
card_multiverse_id('savor the moment'/'SHM', '159404').

card_in_set('scar', 'SHM').
card_original_type('scar'/'SHM', 'Instant').
card_original_text('scar'/'SHM', 'Put a -1/-1 counter on target creature.').
card_first_print('scar', 'SHM').
card_image_name('scar'/'SHM', 'scar').
card_uid('scar'/'SHM', 'SHM:Scar:scar').
card_rarity('scar'/'SHM', 'Common').
card_artist('scar'/'SHM', 'Pete Venters').
card_number('scar'/'SHM', '194').
card_flavor_text('scar'/'SHM', '\"What is a scar? A sign of courage in the face of danger? A mere trick of flesh? Or a constant reminder of agonizing pain, and the follies of war.\"\n—Illulia of Nighthearth').
card_multiverse_id('scar'/'SHM', '142049').

card_in_set('scarscale ritual', 'SHM').
card_original_type('scarscale ritual'/'SHM', 'Sorcery').
card_original_text('scarscale ritual'/'SHM', 'As an additional cost to play Scarscale Ritual, put a -1/-1 counter on a creature you control.\nDraw two cards.').
card_first_print('scarscale ritual', 'SHM').
card_image_name('scarscale ritual'/'SHM', 'scarscale ritual').
card_uid('scarscale ritual'/'SHM', 'SHM:Scarscale Ritual:scarscale ritual').
card_rarity('scarscale ritual'/'SHM', 'Common').
card_artist('scarscale ritual'/'SHM', 'Richard Sardinha').
card_number('scarscale ritual'/'SHM', '175').
card_flavor_text('scarscale ritual'/'SHM', 'Some words are too important to trust to something as fragile as memory.').
card_multiverse_id('scarscale ritual'/'SHM', '154396').

card_in_set('scrapbasket', 'SHM').
card_original_type('scrapbasket'/'SHM', 'Artifact Creature — Scarecrow').
card_original_text('scrapbasket'/'SHM', '{1}: Scrapbasket becomes all colors until end of turn.').
card_first_print('scrapbasket', 'SHM').
card_image_name('scrapbasket'/'SHM', 'scrapbasket').
card_uid('scrapbasket'/'SHM', 'SHM:Scrapbasket:scrapbasket').
card_rarity('scrapbasket'/'SHM', 'Common').
card_artist('scrapbasket'/'SHM', 'Heather Hudson').
card_number('scrapbasket'/'SHM', '262').
card_flavor_text('scrapbasket'/'SHM', 'Once a tool of kithkin farmers, scarecrows of all shapes now skitter and lurch across Shadowmoor, animated by residual rustic magics.').
card_multiverse_id('scrapbasket'/'SHM', '147435').

card_in_set('scuttlemutt', 'SHM').
card_original_type('scuttlemutt'/'SHM', 'Artifact Creature — Scarecrow').
card_original_text('scuttlemutt'/'SHM', '{T}: Add one mana of any color to your mana pool.\n{T}: Target creature becomes the color or colors of your choice until end of turn.').
card_first_print('scuttlemutt', 'SHM').
card_image_name('scuttlemutt'/'SHM', 'scuttlemutt').
card_uid('scuttlemutt'/'SHM', 'SHM:Scuttlemutt:scuttlemutt').
card_rarity('scuttlemutt'/'SHM', 'Common').
card_artist('scuttlemutt'/'SHM', 'Jeremy Jarvis').
card_number('scuttlemutt'/'SHM', '263').
card_flavor_text('scuttlemutt'/'SHM', 'Built to shuttle goods from the river, it took off one day carrying a cauldron of dyes.').
card_multiverse_id('scuttlemutt'/'SHM', '142067').

card_in_set('scuzzback marauders', 'SHM').
card_original_type('scuzzback marauders'/'SHM', 'Creature — Goblin Warrior').
card_original_text('scuzzback marauders'/'SHM', 'Trample\nPersist (When this creature is put into a graveyard from play, if it had no -1/-1 counters on it, return it to play under its owner\'s control with a -1/-1 counter on it.)').
card_first_print('scuzzback marauders', 'SHM').
card_image_name('scuzzback marauders'/'SHM', 'scuzzback marauders').
card_uid('scuzzback marauders'/'SHM', 'SHM:Scuzzback Marauders:scuzzback marauders').
card_rarity('scuzzback marauders'/'SHM', 'Common').
card_artist('scuzzback marauders'/'SHM', 'Pete Venters').
card_number('scuzzback marauders'/'SHM', '216').
card_multiverse_id('scuzzback marauders'/'SHM', '152078').

card_in_set('scuzzback scrapper', 'SHM').
card_original_type('scuzzback scrapper'/'SHM', 'Creature — Goblin Warrior').
card_original_text('scuzzback scrapper'/'SHM', 'Wither (This deals damage to creatures in the form of -1/-1 counters.)').
card_first_print('scuzzback scrapper', 'SHM').
card_image_name('scuzzback scrapper'/'SHM', 'scuzzback scrapper').
card_uid('scuzzback scrapper'/'SHM', 'SHM:Scuzzback Scrapper:scuzzback scrapper').
card_rarity('scuzzback scrapper'/'SHM', 'Common').
card_artist('scuzzback scrapper'/'SHM', 'Scott Altmann').
card_number('scuzzback scrapper'/'SHM', '217').
card_flavor_text('scuzzback scrapper'/'SHM', 'The Scuzzback gang scavenges rusty armor covered in barbed protrusions. No threat is more effective than the threat of infection.').
card_multiverse_id('scuzzback scrapper'/'SHM', '142052').

card_in_set('seedcradle witch', 'SHM').
card_original_type('seedcradle witch'/'SHM', 'Creature — Elf Shaman').
card_original_text('seedcradle witch'/'SHM', '{2}{G}{W}: Target creature gets +3/+3 until end of turn. Untap that creature.').
card_first_print('seedcradle witch', 'SHM').
card_image_name('seedcradle witch'/'SHM', 'seedcradle witch').
card_uid('seedcradle witch'/'SHM', 'SHM:Seedcradle Witch:seedcradle witch').
card_rarity('seedcradle witch'/'SHM', 'Uncommon').
card_artist('seedcradle witch'/'SHM', 'Steven Belledin').
card_number('seedcradle witch'/'SHM', '241').
card_flavor_text('seedcradle witch'/'SHM', 'She whispered a prayer for strength, and her wishes wafted away like seeds on the wind.').
card_multiverse_id('seedcradle witch'/'SHM', '157872').

card_in_set('shield of the oversoul', 'SHM').
card_original_type('shield of the oversoul'/'SHM', 'Enchantment — Aura').
card_original_text('shield of the oversoul'/'SHM', 'Enchant creature\nAs long as enchanted creature is green, it gets +1/+1 and is indestructible. (Lethal damage and effects that say \"destroy\" don\'t destroy it. If its toughness is 0 or less, it\'s still put into its owner\'s graveyard.)\nAs long as enchanted creature is white, it gets +1/+1 and has flying.').
card_first_print('shield of the oversoul', 'SHM').
card_image_name('shield of the oversoul'/'SHM', 'shield of the oversoul').
card_uid('shield of the oversoul'/'SHM', 'SHM:Shield of the Oversoul:shield of the oversoul').
card_rarity('shield of the oversoul'/'SHM', 'Common').
card_artist('shield of the oversoul'/'SHM', 'Steven Belledin').
card_number('shield of the oversoul'/'SHM', '242').
card_multiverse_id('shield of the oversoul'/'SHM', '158762').

card_in_set('sickle ripper', 'SHM').
card_original_type('sickle ripper'/'SHM', 'Creature — Elemental Warrior').
card_original_text('sickle ripper'/'SHM', 'Wither (This deals damage to creatures in the form of -1/-1 counters.)').
card_first_print('sickle ripper', 'SHM').
card_image_name('sickle ripper'/'SHM', 'sickle ripper').
card_uid('sickle ripper'/'SHM', 'SHM:Sickle Ripper:sickle ripper').
card_rarity('sickle ripper'/'SHM', 'Common').
card_artist('sickle ripper'/'SHM', 'Dan Scott').
card_number('sickle ripper'/'SHM', '77').
card_flavor_text('sickle ripper'/'SHM', 'His sickle was forged in the heat of another cinder\'s funeral pyre.').
card_multiverse_id('sickle ripper'/'SHM', '154010').

card_in_set('silkbind faerie', 'SHM').
card_original_type('silkbind faerie'/'SHM', 'Creature — Faerie Rogue').
card_original_text('silkbind faerie'/'SHM', 'Flying\n{1}{W/U}, {Q}: Tap target creature. ({Q} is the untap symbol.)').
card_first_print('silkbind faerie', 'SHM').
card_image_name('silkbind faerie'/'SHM', 'silkbind faerie').
card_uid('silkbind faerie'/'SHM', 'SHM:Silkbind Faerie:silkbind faerie').
card_rarity('silkbind faerie'/'SHM', 'Common').
card_artist('silkbind faerie'/'SHM', 'Matt Cavotta').
card_number('silkbind faerie'/'SHM', '148').
card_flavor_text('silkbind faerie'/'SHM', '\"The bigger they are, the more fun it is to watch them fall flat on their faces.\"').
card_multiverse_id('silkbind faerie'/'SHM', '142027').

card_in_set('sinking feeling', 'SHM').
card_original_type('sinking feeling'/'SHM', 'Enchantment — Aura').
card_original_text('sinking feeling'/'SHM', 'Enchant creature\nEnchanted creature doesn\'t untap during its controller\'s untap step.\nEnchanted creature has \"{1}, Put a -1/-1 counter on this creature: Untap this creature.\"').
card_first_print('sinking feeling', 'SHM').
card_image_name('sinking feeling'/'SHM', 'sinking feeling').
card_uid('sinking feeling'/'SHM', 'SHM:Sinking Feeling:sinking feeling').
card_rarity('sinking feeling'/'SHM', 'Common').
card_artist('sinking feeling'/'SHM', 'Ron Brown').
card_number('sinking feeling'/'SHM', '51').
card_multiverse_id('sinking feeling'/'SHM', '158760').

card_in_set('slinking giant', 'SHM').
card_original_type('slinking giant'/'SHM', 'Creature — Giant Rogue').
card_original_text('slinking giant'/'SHM', 'Wither (This deals damage to creatures in the form of -1/-1 counters.)\nWhenever Slinking Giant blocks or becomes blocked, it gets -3/-0 until end of turn.').
card_first_print('slinking giant', 'SHM').
card_image_name('slinking giant'/'SHM', 'slinking giant').
card_uid('slinking giant'/'SHM', 'SHM:Slinking Giant:slinking giant').
card_rarity('slinking giant'/'SHM', 'Uncommon').
card_artist('slinking giant'/'SHM', 'Dave Kendall').
card_number('slinking giant'/'SHM', '106').
card_flavor_text('slinking giant'/'SHM', '\"I need a better hiding place.\"').
card_multiverse_id('slinking giant'/'SHM', '153995').

card_in_set('smash to smithereens', 'SHM').
card_original_type('smash to smithereens'/'SHM', 'Instant').
card_original_text('smash to smithereens'/'SHM', 'Destroy target artifact. Smash to Smithereens deals 3 damage to that artifact\'s controller.').
card_first_print('smash to smithereens', 'SHM').
card_image_name('smash to smithereens'/'SHM', 'smash to smithereens').
card_uid('smash to smithereens'/'SHM', 'SHM:Smash to Smithereens:smash to smithereens').
card_rarity('smash to smithereens'/'SHM', 'Common').
card_artist('smash to smithereens'/'SHM', 'Pete Venters').
card_number('smash to smithereens'/'SHM', '107').
card_flavor_text('smash to smithereens'/'SHM', 'The giant Tarvik dreamed that trinkets and machines caused all the world\'s woe. When he awoke from his troubled sleep, he took the name Tarvik Relicsmasher.').
card_multiverse_id('smash to smithereens'/'SHM', '158243').

card_in_set('smolder initiate', 'SHM').
card_original_type('smolder initiate'/'SHM', 'Creature — Elemental Shaman').
card_original_text('smolder initiate'/'SHM', 'Whenever a player plays a black spell, you may pay {1}. If you do, target player loses 1 life.').
card_first_print('smolder initiate', 'SHM').
card_image_name('smolder initiate'/'SHM', 'smolder initiate').
card_uid('smolder initiate'/'SHM', 'SHM:Smolder Initiate:smolder initiate').
card_rarity('smolder initiate'/'SHM', 'Common').
card_artist('smolder initiate'/'SHM', 'Chippy').
card_number('smolder initiate'/'SHM', '78').
card_flavor_text('smolder initiate'/'SHM', '\"Life is a circle. Death is a vicious circle.\"').
card_multiverse_id('smolder initiate'/'SHM', '147376').

card_in_set('somnomancer', 'SHM').
card_original_type('somnomancer'/'SHM', 'Creature — Kithkin Wizard').
card_original_text('somnomancer'/'SHM', 'When Somnomancer comes into play, you may tap target creature.').
card_first_print('somnomancer', 'SHM').
card_image_name('somnomancer'/'SHM', 'somnomancer').
card_uid('somnomancer'/'SHM', 'SHM:Somnomancer:somnomancer').
card_rarity('somnomancer'/'SHM', 'Common').
card_artist('somnomancer'/'SHM', 'Lars Grant-West').
card_number('somnomancer'/'SHM', '149').
card_flavor_text('somnomancer'/'SHM', '\"Are you tired? You look tired.\"').
card_multiverse_id('somnomancer'/'SHM', '147399').

card_in_set('sootstoke kindler', 'SHM').
card_original_type('sootstoke kindler'/'SHM', 'Creature — Elemental Shaman').
card_original_text('sootstoke kindler'/'SHM', 'Haste\n{T}: Target black or red creature gains haste until end of turn.').
card_first_print('sootstoke kindler', 'SHM').
card_image_name('sootstoke kindler'/'SHM', 'sootstoke kindler').
card_uid('sootstoke kindler'/'SHM', 'SHM:Sootstoke Kindler:sootstoke kindler').
card_rarity('sootstoke kindler'/'SHM', 'Common').
card_artist('sootstoke kindler'/'SHM', 'Mark Zug').
card_number('sootstoke kindler'/'SHM', '195').
card_flavor_text('sootstoke kindler'/'SHM', '\"Nothing warms my heart better than a spirit kindled with rage.\"').
card_multiverse_id('sootstoke kindler'/'SHM', '153989').

card_in_set('sootwalkers', 'SHM').
card_original_type('sootwalkers'/'SHM', 'Creature — Elemental Rogue').
card_original_text('sootwalkers'/'SHM', 'Sootwalkers can\'t be blocked by white creatures.').
card_first_print('sootwalkers', 'SHM').
card_image_name('sootwalkers'/'SHM', 'sootwalkers').
card_uid('sootwalkers'/'SHM', 'SHM:Sootwalkers:sootwalkers').
card_rarity('sootwalkers'/'SHM', 'Common').
card_artist('sootwalkers'/'SHM', 'Nils Hamm').
card_number('sootwalkers'/'SHM', '196').
card_flavor_text('sootwalkers'/'SHM', '\"Why allow the fires of others to burn, when ours do not? Why leave them content, while we suffer? If there is to be misery, let it be borne by all.\"').
card_multiverse_id('sootwalkers'/'SHM', '141936').

card_in_set('spawnwrithe', 'SHM').
card_original_type('spawnwrithe'/'SHM', 'Creature — Elemental').
card_original_text('spawnwrithe'/'SHM', 'Trample\nWhenever Spawnwrithe deals combat damage to a player, put a token into play that\'s a copy of Spawnwrithe.').
card_first_print('spawnwrithe', 'SHM').
card_image_name('spawnwrithe'/'SHM', 'spawnwrithe').
card_uid('spawnwrithe'/'SHM', 'SHM:Spawnwrithe:spawnwrithe').
card_rarity('spawnwrithe'/'SHM', 'Rare').
card_artist('spawnwrithe'/'SHM', 'Daarken').
card_number('spawnwrithe'/'SHM', '129').
card_flavor_text('spawnwrithe'/'SHM', 'Its victims feel only an itchy, wriggling feeling just under their skin. By then, it\'s far too late.').
card_multiverse_id('spawnwrithe'/'SHM', '158687').

card_in_set('spectral procession', 'SHM').
card_original_type('spectral procession'/'SHM', 'Sorcery').
card_original_text('spectral procession'/'SHM', '({2/W} can be paid with any two mana or with {W}. This card\'s converted mana cost is 6.)\nPut three 1/1 white Spirit creature tokens with flying into play.').
card_first_print('spectral procession', 'SHM').
card_image_name('spectral procession'/'SHM', 'spectral procession').
card_uid('spectral procession'/'SHM', 'SHM:Spectral Procession:spectral procession').
card_rarity('spectral procession'/'SHM', 'Uncommon').
card_artist('spectral procession'/'SHM', 'Jeremy Enecio').
card_number('spectral procession'/'SHM', '23').
card_flavor_text('spectral procession'/'SHM', '\"The dead have it easy. They suffer no more. If breaking their rest helps the living, so be it.\"\n—Olka, mistmeadow witch').
card_multiverse_id('spectral procession'/'SHM', '152070').

card_in_set('spell syphon', 'SHM').
card_original_type('spell syphon'/'SHM', 'Instant').
card_original_text('spell syphon'/'SHM', 'Counter target spell unless its controller pays {1} for each blue permanent you control.').
card_first_print('spell syphon', 'SHM').
card_image_name('spell syphon'/'SHM', 'spell syphon').
card_uid('spell syphon'/'SHM', 'SHM:Spell Syphon:spell syphon').
card_rarity('spell syphon'/'SHM', 'Common').
card_artist('spell syphon'/'SHM', 'John Avon').
card_number('spell syphon'/'SHM', '52').
card_flavor_text('spell syphon'/'SHM', 'Warrik Brutehexer felt betrayed that his aura favored the depths of the Wanderbrine River over its rightful place, hovering around him.').
card_multiverse_id('spell syphon'/'SHM', '153999').

card_in_set('spiteflame witch', 'SHM').
card_original_type('spiteflame witch'/'SHM', 'Creature — Elemental Shaman').
card_original_text('spiteflame witch'/'SHM', '{B}{R}: Each player loses 1 life.').
card_first_print('spiteflame witch', 'SHM').
card_image_name('spiteflame witch'/'SHM', 'spiteflame witch').
card_uid('spiteflame witch'/'SHM', 'SHM:Spiteflame Witch:spiteflame witch').
card_rarity('spiteflame witch'/'SHM', 'Uncommon').
card_artist('spiteflame witch'/'SHM', 'William O\'Connor').
card_number('spiteflame witch'/'SHM', '197').
card_flavor_text('spiteflame witch'/'SHM', 'Cinder beliefs revolve around the Path of Sorrow, a spiritual descent into oblivion. They don\'t mind taking others on their journey down.').
card_multiverse_id('spiteflame witch'/'SHM', '157880').

card_in_set('spiteful visions', 'SHM').
card_original_type('spiteful visions'/'SHM', 'Enchantment').
card_original_text('spiteful visions'/'SHM', 'At the beginning of each player\'s draw step, that player draws a card.\nWhenever a player draws a card, Spiteful Visions deals 1 damage to that player.').
card_first_print('spiteful visions', 'SHM').
card_image_name('spiteful visions'/'SHM', 'spiteful visions').
card_uid('spiteful visions'/'SHM', 'SHM:Spiteful Visions:spiteful visions').
card_rarity('spiteful visions'/'SHM', 'Rare').
card_artist('spiteful visions'/'SHM', 'Brandon Kitkouski').
card_number('spiteful visions'/'SHM', '198').
card_flavor_text('spiteful visions'/'SHM', 'Knowledge brings the sting of disillusionment, but the pain teaches perspective.').
card_multiverse_id('spiteful visions'/'SHM', '146089').

card_in_set('splitting headache', 'SHM').
card_original_type('splitting headache'/'SHM', 'Sorcery').
card_original_text('splitting headache'/'SHM', 'Choose one Target player discards two cards; or target player reveals his or her hand, you choose a card from it, then that player discards that card.').
card_first_print('splitting headache', 'SHM').
card_image_name('splitting headache'/'SHM', 'splitting headache').
card_uid('splitting headache'/'SHM', 'SHM:Splitting Headache:splitting headache').
card_rarity('splitting headache'/'SHM', 'Common').
card_artist('splitting headache'/'SHM', 'Thomas Denmark').
card_number('splitting headache'/'SHM', '79').
card_flavor_text('splitting headache'/'SHM', 'All faeries delve for dreams. But not all wait for their victims to fall asleep.').
card_multiverse_id('splitting headache'/'SHM', '147378').

card_in_set('steel of the godhead', 'SHM').
card_original_type('steel of the godhead'/'SHM', 'Enchantment — Aura').
card_original_text('steel of the godhead'/'SHM', 'Enchant creature\nAs long as enchanted creature is white, it gets +1/+1 and has lifelink. (Whenever it deals damage, its controller gains that much life.)\nAs long as enchanted creature is blue, it gets +1/+1 and is unblockable.').
card_first_print('steel of the godhead', 'SHM').
card_image_name('steel of the godhead'/'SHM', 'steel of the godhead').
card_uid('steel of the godhead'/'SHM', 'SHM:Steel of the Godhead:steel of the godhead').
card_rarity('steel of the godhead'/'SHM', 'Common').
card_artist('steel of the godhead'/'SHM', 'Jason Chan').
card_number('steel of the godhead'/'SHM', '150').
card_multiverse_id('steel of the godhead'/'SHM', '158749').

card_in_set('strip bare', 'SHM').
card_original_type('strip bare'/'SHM', 'Instant').
card_original_text('strip bare'/'SHM', 'Destroy all Auras and Equipment attached to target creature.').
card_first_print('strip bare', 'SHM').
card_image_name('strip bare'/'SHM', 'strip bare').
card_uid('strip bare'/'SHM', 'SHM:Strip Bare:strip bare').
card_rarity('strip bare'/'SHM', 'Common').
card_artist('strip bare'/'SHM', 'Ralph Horsley').
card_number('strip bare'/'SHM', '24').
card_flavor_text('strip bare'/'SHM', '\"All glamers lifted, all lies revealed, all flesh exposed.\"\n—Awylla, elvish safewright').
card_multiverse_id('strip bare'/'SHM', '159409').

card_in_set('sunken ruins', 'SHM').
card_original_type('sunken ruins'/'SHM', 'Land').
card_original_text('sunken ruins'/'SHM', '{T}: Add {1} to your mana pool.\n{U/B}, {T}: Add {U}{U}, {U}{B}, or {B}{B} to your mana pool.').
card_first_print('sunken ruins', 'SHM').
card_image_name('sunken ruins'/'SHM', 'sunken ruins').
card_uid('sunken ruins'/'SHM', 'SHM:Sunken Ruins:sunken ruins').
card_rarity('sunken ruins'/'SHM', 'Rare').
card_artist('sunken ruins'/'SHM', 'Warren Mahy').
card_number('sunken ruins'/'SHM', '280').
card_flavor_text('sunken ruins'/'SHM', 'Without the care of the tideshapers, the Lanes lie neglected. Some have flooded entire towns; others dried to cracked mud.').
card_multiverse_id('sunken ruins'/'SHM', '146729').

card_in_set('swamp', 'SHM').
card_original_type('swamp'/'SHM', 'Basic Land — Swamp').
card_original_text('swamp'/'SHM', 'B').
card_image_name('swamp'/'SHM', 'swamp1').
card_uid('swamp'/'SHM', 'SHM:Swamp:swamp1').
card_rarity('swamp'/'SHM', 'Basic Land').
card_artist('swamp'/'SHM', 'Lars Grant-West').
card_number('swamp'/'SHM', '290').
card_multiverse_id('swamp'/'SHM', '157889').

card_in_set('swamp', 'SHM').
card_original_type('swamp'/'SHM', 'Basic Land — Swamp').
card_original_text('swamp'/'SHM', 'B').
card_image_name('swamp'/'SHM', 'swamp2').
card_uid('swamp'/'SHM', 'SHM:Swamp:swamp2').
card_rarity('swamp'/'SHM', 'Basic Land').
card_artist('swamp'/'SHM', 'Warren Mahy').
card_number('swamp'/'SHM', '291').
card_multiverse_id('swamp'/'SHM', '157871').

card_in_set('swamp', 'SHM').
card_original_type('swamp'/'SHM', 'Basic Land — Swamp').
card_original_text('swamp'/'SHM', 'B').
card_image_name('swamp'/'SHM', 'swamp3').
card_uid('swamp'/'SHM', 'SHM:Swamp:swamp3').
card_rarity('swamp'/'SHM', 'Basic Land').
card_artist('swamp'/'SHM', 'rk post').
card_number('swamp'/'SHM', '292').
card_multiverse_id('swamp'/'SHM', '158239').

card_in_set('swamp', 'SHM').
card_original_type('swamp'/'SHM', 'Basic Land — Swamp').
card_original_text('swamp'/'SHM', 'B').
card_image_name('swamp'/'SHM', 'swamp4').
card_uid('swamp'/'SHM', 'SHM:Swamp:swamp4').
card_rarity('swamp'/'SHM', 'Basic Land').
card_artist('swamp'/'SHM', 'Chippy').
card_number('swamp'/'SHM', '293').
card_multiverse_id('swamp'/'SHM', '157886').

card_in_set('swans of bryn argoll', 'SHM').
card_original_type('swans of bryn argoll'/'SHM', 'Creature — Bird Spirit').
card_original_text('swans of bryn argoll'/'SHM', 'Flying\nIf a source would deal damage to Swans of Bryn Argoll, prevent that damage. The source\'s controller draws cards equal to the damage prevented this way.').
card_first_print('swans of bryn argoll', 'SHM').
card_image_name('swans of bryn argoll'/'SHM', 'swans of bryn argoll').
card_uid('swans of bryn argoll'/'SHM', 'SHM:Swans of Bryn Argoll:swans of bryn argoll').
card_rarity('swans of bryn argoll'/'SHM', 'Rare').
card_artist('swans of bryn argoll'/'SHM', 'Eric Fortune').
card_number('swans of bryn argoll'/'SHM', '151').
card_flavor_text('swans of bryn argoll'/'SHM', 'Any being that harms them quickly learns its lesson.').
card_multiverse_id('swans of bryn argoll'/'SHM', '146754').

card_in_set('sygg, river cutthroat', 'SHM').
card_original_type('sygg, river cutthroat'/'SHM', 'Legendary Creature — Merfolk Rogue').
card_original_text('sygg, river cutthroat'/'SHM', 'At end of turn, if an opponent lost 3 or more life this turn, you may draw a card. (Damage causes loss of life.)').
card_first_print('sygg, river cutthroat', 'SHM').
card_image_name('sygg, river cutthroat'/'SHM', 'sygg, river cutthroat').
card_uid('sygg, river cutthroat'/'SHM', 'SHM:Sygg, River Cutthroat:sygg, river cutthroat').
card_rarity('sygg, river cutthroat'/'SHM', 'Rare').
card_artist('sygg, river cutthroat'/'SHM', 'Jeremy Enecio').
card_number('sygg, river cutthroat'/'SHM', '176').
card_flavor_text('sygg, river cutthroat'/'SHM', '\"It\'s not a matter of deserving. It\'s a matter of strength. The power to hold versus the power to take.\"').
card_multiverse_id('sygg, river cutthroat'/'SHM', '147377').

card_in_set('tatterkite', 'SHM').
card_original_type('tatterkite'/'SHM', 'Artifact Creature — Scarecrow').
card_original_text('tatterkite'/'SHM', 'Flying\nTatterkite can\'t have counters placed on it.').
card_first_print('tatterkite', 'SHM').
card_image_name('tatterkite'/'SHM', 'tatterkite').
card_uid('tatterkite'/'SHM', 'SHM:Tatterkite:tatterkite').
card_rarity('tatterkite'/'SHM', 'Uncommon').
card_artist('tatterkite'/'SHM', 'Ron Brown').
card_number('tatterkite'/'SHM', '264').
card_flavor_text('tatterkite'/'SHM', '\"I rigged it together from wicker and scrap, yet somehow I find it unalterably perfect.\"\n—Braenna, cobblesmith').
card_multiverse_id('tatterkite'/'SHM', '142030').

card_in_set('tattermunge duo', 'SHM').
card_original_type('tattermunge duo'/'SHM', 'Creature — Goblin Warrior Shaman').
card_original_text('tattermunge duo'/'SHM', 'Whenever you play a red spell, Tattermunge Duo gets +1/+1 until end of turn.\nWhenever you play a green spell, Tattermunge Duo gains forestwalk until end of turn.').
card_first_print('tattermunge duo', 'SHM').
card_image_name('tattermunge duo'/'SHM', 'tattermunge duo').
card_uid('tattermunge duo'/'SHM', 'SHM:Tattermunge Duo:tattermunge duo').
card_rarity('tattermunge duo'/'SHM', 'Common').
card_artist('tattermunge duo'/'SHM', 'Jesper Ejsing').
card_number('tattermunge duo'/'SHM', '218').
card_multiverse_id('tattermunge duo'/'SHM', '153282').

card_in_set('tattermunge maniac', 'SHM').
card_original_type('tattermunge maniac'/'SHM', 'Creature — Goblin Warrior').
card_original_text('tattermunge maniac'/'SHM', 'Tattermunge Maniac attacks each turn if able.').
card_first_print('tattermunge maniac', 'SHM').
card_image_name('tattermunge maniac'/'SHM', 'tattermunge maniac').
card_uid('tattermunge maniac'/'SHM', 'SHM:Tattermunge Maniac:tattermunge maniac').
card_rarity('tattermunge maniac'/'SHM', 'Uncommon').
card_artist('tattermunge maniac'/'SHM', 'Matt Cavotta').
card_number('tattermunge maniac'/'SHM', '219').
card_flavor_text('tattermunge maniac'/'SHM', 'It shows up at one meal wearing the carcass of the last.').
card_multiverse_id('tattermunge maniac'/'SHM', '142013').

card_in_set('tattermunge witch', 'SHM').
card_original_type('tattermunge witch'/'SHM', 'Creature — Goblin Shaman').
card_original_text('tattermunge witch'/'SHM', '{R}{G}: Each blocked creature gets +1/+0 and gains trample until end of turn.').
card_first_print('tattermunge witch', 'SHM').
card_image_name('tattermunge witch'/'SHM', 'tattermunge witch').
card_uid('tattermunge witch'/'SHM', 'SHM:Tattermunge Witch:tattermunge witch').
card_rarity('tattermunge witch'/'SHM', 'Uncommon').
card_artist('tattermunge witch'/'SHM', 'Warren Mahy').
card_number('tattermunge witch'/'SHM', '220').
card_flavor_text('tattermunge witch'/'SHM', 'The Tattermunge gang has no words for the spell that whips their instinctual rage into a frenzy. They only know they like it.').
card_multiverse_id('tattermunge witch'/'SHM', '157884').

card_in_set('thistledown duo', 'SHM').
card_original_type('thistledown duo'/'SHM', 'Creature — Kithkin Soldier Wizard').
card_original_text('thistledown duo'/'SHM', 'Whenever you play a white spell, Thistledown Duo gets +1/+1 until end of turn.\nWhenever you play a blue spell, Thistledown Duo gains flying until end of turn.').
card_first_print('thistledown duo', 'SHM').
card_image_name('thistledown duo'/'SHM', 'thistledown duo').
card_uid('thistledown duo'/'SHM', 'SHM:Thistledown Duo:thistledown duo').
card_rarity('thistledown duo'/'SHM', 'Common').
card_artist('thistledown duo'/'SHM', 'Zoltan Boros & Gabor Szikszai').
card_number('thistledown duo'/'SHM', '152').
card_multiverse_id('thistledown duo'/'SHM', '153301').

card_in_set('thistledown liege', 'SHM').
card_original_type('thistledown liege'/'SHM', 'Creature — Kithkin Knight').
card_original_text('thistledown liege'/'SHM', 'Flash\nOther white creatures you control get +1/+1.\nOther blue creatures you control get +1/+1.').
card_first_print('thistledown liege', 'SHM').
card_image_name('thistledown liege'/'SHM', 'thistledown liege').
card_uid('thistledown liege'/'SHM', 'SHM:Thistledown Liege:thistledown liege').
card_rarity('thistledown liege'/'SHM', 'Rare').
card_artist('thistledown liege'/'SHM', 'Adam Rex').
card_number('thistledown liege'/'SHM', '153').
card_flavor_text('thistledown liege'/'SHM', 'The thoughtweft is his informant, and he its devoted guardian.').
card_multiverse_id('thistledown liege'/'SHM', '147409').

card_in_set('thornwatch scarecrow', 'SHM').
card_original_type('thornwatch scarecrow'/'SHM', 'Artifact Creature — Scarecrow').
card_original_text('thornwatch scarecrow'/'SHM', 'Thornwatch Scarecrow has wither as long as you control a green creature. (It deals damage to creatures in the form of -1/-1 counters.)\nThornwatch Scarecrow has vigilance as long as you control a white creature.').
card_first_print('thornwatch scarecrow', 'SHM').
card_image_name('thornwatch scarecrow'/'SHM', 'thornwatch scarecrow').
card_uid('thornwatch scarecrow'/'SHM', 'SHM:Thornwatch Scarecrow:thornwatch scarecrow').
card_rarity('thornwatch scarecrow'/'SHM', 'Common').
card_artist('thornwatch scarecrow'/'SHM', 'Chuck Lukacs').
card_number('thornwatch scarecrow'/'SHM', '265').
card_multiverse_id('thornwatch scarecrow'/'SHM', '151634').

card_in_set('thought reflection', 'SHM').
card_original_type('thought reflection'/'SHM', 'Enchantment').
card_original_text('thought reflection'/'SHM', 'If you would draw a card, draw two cards instead.').
card_first_print('thought reflection', 'SHM').
card_image_name('thought reflection'/'SHM', 'thought reflection').
card_uid('thought reflection'/'SHM', 'SHM:Thought Reflection:thought reflection').
card_rarity('thought reflection'/'SHM', 'Rare').
card_artist('thought reflection'/'SHM', 'Terese Nielsen & Ron Spencer').
card_number('thought reflection'/'SHM', '53').
card_flavor_text('thought reflection'/'SHM', 'Knowledge fills the mind of a fool and opens the mind of the sage.').
card_multiverse_id('thought reflection'/'SHM', '146728').

card_in_set('thoughtweft gambit', 'SHM').
card_original_type('thoughtweft gambit'/'SHM', 'Instant').
card_original_text('thoughtweft gambit'/'SHM', 'Tap all creatures your opponents control and untap all creatures you control.').
card_first_print('thoughtweft gambit', 'SHM').
card_image_name('thoughtweft gambit'/'SHM', 'thoughtweft gambit').
card_uid('thoughtweft gambit'/'SHM', 'SHM:Thoughtweft Gambit:thoughtweft gambit').
card_rarity('thoughtweft gambit'/'SHM', 'Uncommon').
card_artist('thoughtweft gambit'/'SHM', 'Steve Prescott').
card_number('thoughtweft gambit'/'SHM', '154').
card_flavor_text('thoughtweft gambit'/'SHM', 'The kithkin mind-bond is even tighter in Shadowmoor, reinforcing the unity of their community to the point of xenophobia.').
card_multiverse_id('thoughtweft gambit'/'SHM', '146024').

card_in_set('toil to renown', 'SHM').
card_original_type('toil to renown'/'SHM', 'Sorcery').
card_original_text('toil to renown'/'SHM', 'You gain 1 life for each tapped artifact, creature, and land you control.').
card_first_print('toil to renown', 'SHM').
card_image_name('toil to renown'/'SHM', 'toil to renown').
card_uid('toil to renown'/'SHM', 'SHM:Toil to Renown:toil to renown').
card_rarity('toil to renown'/'SHM', 'Common').
card_artist('toil to renown'/'SHM', 'Larry MacDougall').
card_number('toil to renown'/'SHM', '130').
card_flavor_text('toil to renown'/'SHM', 'The last survivor of her patrol, the warrior returned expecting disappointment and scorn. Instead she found gratitude. \"You are alive. That is reason to celebrate.\"').
card_multiverse_id('toil to renown'/'SHM', '159410').

card_in_set('torpor dust', 'SHM').
card_original_type('torpor dust'/'SHM', 'Enchantment — Aura').
card_original_text('torpor dust'/'SHM', 'Flash\nEnchant creature\nEnchanted creature gets -3/-0.').
card_first_print('torpor dust', 'SHM').
card_image_name('torpor dust'/'SHM', 'torpor dust').
card_uid('torpor dust'/'SHM', 'SHM:Torpor Dust:torpor dust').
card_rarity('torpor dust'/'SHM', 'Common').
card_artist('torpor dust'/'SHM', 'Jesper Ejsing').
card_number('torpor dust'/'SHM', '177').
card_flavor_text('torpor dust'/'SHM', '\"Some folk these days are too restless to dream the dreams we need. We need to teach them to stop and catch their breath.\"').
card_multiverse_id('torpor dust'/'SHM', '142053').

card_in_set('torrent of souls', 'SHM').
card_original_type('torrent of souls'/'SHM', 'Sorcery').
card_original_text('torrent of souls'/'SHM', 'Return up to one target creature card from your graveyard to play if {B} was spent to play Torrent of Souls. Creatures target player controls get +2/+0 and gain haste until end of turn if {R} was spent to play Torrent of Souls. (Do both if {B}{R} was spent.)').
card_first_print('torrent of souls', 'SHM').
card_image_name('torrent of souls'/'SHM', 'torrent of souls').
card_uid('torrent of souls'/'SHM', 'SHM:Torrent of Souls:torrent of souls').
card_rarity('torrent of souls'/'SHM', 'Uncommon').
card_artist('torrent of souls'/'SHM', 'Ian Edward Ameling').
card_number('torrent of souls'/'SHM', '199').
card_multiverse_id('torrent of souls'/'SHM', '158751').

card_in_set('torture', 'SHM').
card_original_type('torture'/'SHM', 'Enchantment — Aura').
card_original_text('torture'/'SHM', 'Enchant creature\n{1}{B}: Put a -1/-1 counter on enchanted creature.').
card_image_name('torture'/'SHM', 'torture').
card_uid('torture'/'SHM', 'SHM:Torture:torture').
card_rarity('torture'/'SHM', 'Common').
card_artist('torture'/'SHM', 'E. M. Gist').
card_number('torture'/'SHM', '80').
card_flavor_text('torture'/'SHM', 'What the vigor of life builds up, agony tears down again.').
card_multiverse_id('torture'/'SHM', '142070').

card_in_set('tower above', 'SHM').
card_original_type('tower above'/'SHM', 'Sorcery').
card_original_text('tower above'/'SHM', '({2/G} can be paid with any two mana or with {G}. This card\'s converted mana cost is 6.)\nUntil end of turn, target creature gets +4/+4 and gains trample, wither, and \"When this creature attacks, target creature blocks it this turn if able.\" (It deals damage to creatures in the form of -1/-1 counters.)').
card_first_print('tower above', 'SHM').
card_image_name('tower above'/'SHM', 'tower above').
card_uid('tower above'/'SHM', 'SHM:Tower Above:tower above').
card_rarity('tower above'/'SHM', 'Uncommon').
card_artist('tower above'/'SHM', 'Thomas Denmark').
card_number('tower above'/'SHM', '131').
card_multiverse_id('tower above'/'SHM', '152075').

card_in_set('traitor\'s roar', 'SHM').
card_original_type('traitor\'s roar'/'SHM', 'Sorcery').
card_original_text('traitor\'s roar'/'SHM', 'Tap target untapped creature. It deals damage equal to its power to its controller.\nConspire (As you play this spell, you may tap two untapped creatures you control that share a color with it. When you do, copy it and you may choose a new target for the copy.)').
card_first_print('traitor\'s roar', 'SHM').
card_image_name('traitor\'s roar'/'SHM', 'traitor\'s roar').
card_uid('traitor\'s roar'/'SHM', 'SHM:Traitor\'s Roar:traitor\'s roar').
card_rarity('traitor\'s roar'/'SHM', 'Common').
card_artist('traitor\'s roar'/'SHM', 'Jim Pavelec').
card_number('traitor\'s roar'/'SHM', '200').
card_multiverse_id('traitor\'s roar'/'SHM', '158746').

card_in_set('trip noose', 'SHM').
card_original_type('trip noose'/'SHM', 'Artifact').
card_original_text('trip noose'/'SHM', '{2}, {T}: Tap target creature.').
card_first_print('trip noose', 'SHM').
card_image_name('trip noose'/'SHM', 'trip noose').
card_uid('trip noose'/'SHM', 'SHM:Trip Noose:trip noose').
card_rarity('trip noose'/'SHM', 'Uncommon').
card_artist('trip noose'/'SHM', 'Randy Gallegos').
card_number('trip noose'/'SHM', '266').
card_flavor_text('trip noose'/'SHM', 'A taut slipknot trigger is the only thing standing between you and standing.').
card_multiverse_id('trip noose'/'SHM', '147420').

card_in_set('turn to mist', 'SHM').
card_original_type('turn to mist'/'SHM', 'Instant').
card_original_text('turn to mist'/'SHM', 'Remove target creature from the game. Return that card to play under its owner\'s control at end of turn.').
card_first_print('turn to mist', 'SHM').
card_image_name('turn to mist'/'SHM', 'turn to mist').
card_uid('turn to mist'/'SHM', 'SHM:Turn to Mist:turn to mist').
card_rarity('turn to mist'/'SHM', 'Common').
card_artist('turn to mist'/'SHM', 'Greg Staples').
card_number('turn to mist'/'SHM', '155').
card_flavor_text('turn to mist'/'SHM', '\"Did you expect me to dodge? How quaint.\"\n—Olka, mistmeadow witch').
card_multiverse_id('turn to mist'/'SHM', '158752').

card_in_set('twilight shepherd', 'SHM').
card_original_type('twilight shepherd'/'SHM', 'Creature — Angel').
card_original_text('twilight shepherd'/'SHM', 'Flying, vigilance\nWhen Twilight Shepherd comes into play, return to your hand all cards in your graveyard put there from play this turn.\nPersist (When this creature is put into a graveyard from play, if it had no -1/-1 counters on it, return it to play under its owner\'s control with a -1/-1 counter on it.)').
card_first_print('twilight shepherd', 'SHM').
card_image_name('twilight shepherd'/'SHM', 'twilight shepherd').
card_uid('twilight shepherd'/'SHM', 'SHM:Twilight Shepherd:twilight shepherd').
card_rarity('twilight shepherd'/'SHM', 'Rare').
card_artist('twilight shepherd'/'SHM', 'Jason Chan').
card_number('twilight shepherd'/'SHM', '25').
card_multiverse_id('twilight shepherd'/'SHM', '152060').

card_in_set('tyrannize', 'SHM').
card_original_type('tyrannize'/'SHM', 'Sorcery').
card_original_text('tyrannize'/'SHM', 'Target player discards his or her hand unless he or she pays 7 life.').
card_first_print('tyrannize', 'SHM').
card_image_name('tyrannize'/'SHM', 'tyrannize').
card_uid('tyrannize'/'SHM', 'SHM:Tyrannize:tyrannize').
card_rarity('tyrannize'/'SHM', 'Rare').
card_artist('tyrannize'/'SHM', 'Mark Tedin').
card_number('tyrannize'/'SHM', '201').
card_flavor_text('tyrannize'/'SHM', '\"I can stab you where it hurts, or where you will feel absolutely nothing.\"\n—Illulia of Nighthearth').
card_multiverse_id('tyrannize'/'SHM', '146767').

card_in_set('umbral mantle', 'SHM').
card_original_type('umbral mantle'/'SHM', 'Artifact — Equipment').
card_original_text('umbral mantle'/'SHM', 'Equipped creature has \"{3}, {Q}: This creature gets +2/+2 until end of turn.\" ({Q} is the untap symbol.)\nEquip {0}').
card_first_print('umbral mantle', 'SHM').
card_image_name('umbral mantle'/'SHM', 'umbral mantle').
card_uid('umbral mantle'/'SHM', 'SHM:Umbral Mantle:umbral mantle').
card_rarity('umbral mantle'/'SHM', 'Uncommon').
card_artist('umbral mantle'/'SHM', 'Richard Sardinha').
card_number('umbral mantle'/'SHM', '267').
card_flavor_text('umbral mantle'/'SHM', 'It harnesses the power of night in a land where daylight fears to show itself.').
card_multiverse_id('umbral mantle'/'SHM', '153317').

card_in_set('valleymaker', 'SHM').
card_original_type('valleymaker'/'SHM', 'Creature — Giant Shaman').
card_original_text('valleymaker'/'SHM', '{T}, Sacrifice a Mountain: Valleymaker deals 3 damage to target creature.\n{T}, Sacrifice a Forest: Choose a player. That player adds {G}{G}{G} to his or her mana pool.').
card_first_print('valleymaker', 'SHM').
card_image_name('valleymaker'/'SHM', 'valleymaker').
card_uid('valleymaker'/'SHM', 'SHM:Valleymaker:valleymaker').
card_rarity('valleymaker'/'SHM', 'Rare').
card_artist('valleymaker'/'SHM', 'Randy Gallegos').
card_number('valleymaker'/'SHM', '221').
card_flavor_text('valleymaker'/'SHM', 'Their home uprooted by a giant, the gang of boggarts mourned their old stomping ground.').
card_multiverse_id('valleymaker'/'SHM', '159397').

card_in_set('vexing shusher', 'SHM').
card_original_type('vexing shusher'/'SHM', 'Creature — Goblin Shaman').
card_original_text('vexing shusher'/'SHM', 'Vexing Shusher can\'t be countered.\n{R/G}: Target spell can\'t be countered by spells or abilities.').
card_image_name('vexing shusher'/'SHM', 'vexing shusher').
card_uid('vexing shusher'/'SHM', 'SHM:Vexing Shusher:vexing shusher').
card_rarity('vexing shusher'/'SHM', 'Rare').
card_artist('vexing shusher'/'SHM', 'Cyril Van Der Haegen').
card_number('vexing shusher'/'SHM', '222').
card_flavor_text('vexing shusher'/'SHM', 'The stench of bloodcap mushrooms on the breath is enough to ward off even the most potent magics, especially when combined with a special chant: \"Hushhh.\"').
card_multiverse_id('vexing shusher'/'SHM', '146016').

card_in_set('viridescent wisps', 'SHM').
card_original_type('viridescent wisps'/'SHM', 'Instant').
card_original_text('viridescent wisps'/'SHM', 'Target creature becomes green and gets +1/+0 until end of turn.\nDraw a card.').
card_first_print('viridescent wisps', 'SHM').
card_image_name('viridescent wisps'/'SHM', 'viridescent wisps').
card_uid('viridescent wisps'/'SHM', 'SHM:Viridescent Wisps:viridescent wisps').
card_rarity('viridescent wisps'/'SHM', 'Common').
card_artist('viridescent wisps'/'SHM', 'Jim Nelson').
card_number('viridescent wisps'/'SHM', '132').
card_flavor_text('viridescent wisps'/'SHM', 'In the wisps that night, the once timid boggart saw all the terrible, bloody things he was capable of. And it was glorious.').
card_multiverse_id('viridescent wisps'/'SHM', '158758').

card_in_set('wanderbrine rootcutters', 'SHM').
card_original_type('wanderbrine rootcutters'/'SHM', 'Creature — Merfolk Rogue').
card_original_text('wanderbrine rootcutters'/'SHM', 'Wanderbrine Rootcutters can\'t be blocked by green creatures.').
card_first_print('wanderbrine rootcutters', 'SHM').
card_image_name('wanderbrine rootcutters'/'SHM', 'wanderbrine rootcutters').
card_uid('wanderbrine rootcutters'/'SHM', 'SHM:Wanderbrine Rootcutters:wanderbrine rootcutters').
card_rarity('wanderbrine rootcutters'/'SHM', 'Common').
card_artist('wanderbrine rootcutters'/'SHM', 'Chippy').
card_number('wanderbrine rootcutters'/'SHM', '178').
card_flavor_text('wanderbrine rootcutters'/'SHM', 'Most dirtwalkers only know of the vicious merrows that dwell in the shallows. They can\'t begin to fathom the wickedness that skulks in the Dark Meanders.').
card_multiverse_id('wanderbrine rootcutters'/'SHM', '142012').

card_in_set('wasp lancer', 'SHM').
card_original_type('wasp lancer'/'SHM', 'Creature — Faerie Soldier').
card_original_text('wasp lancer'/'SHM', 'Flying').
card_first_print('wasp lancer', 'SHM').
card_image_name('wasp lancer'/'SHM', 'wasp lancer').
card_uid('wasp lancer'/'SHM', 'SHM:Wasp Lancer:wasp lancer').
card_rarity('wasp lancer'/'SHM', 'Uncommon').
card_artist('wasp lancer'/'SHM', 'Warren Mahy').
card_number('wasp lancer'/'SHM', '179').
card_flavor_text('wasp lancer'/'SHM', '\"I doubt that faeries understand how short their lives are, compared to the rest of us. If they did, would they so readily charge into battle, heedless of the danger before them?\"\n—Awylla, elvish safewright').
card_multiverse_id('wasp lancer'/'SHM', '153967').

card_in_set('watchwing scarecrow', 'SHM').
card_original_type('watchwing scarecrow'/'SHM', 'Artifact Creature — Scarecrow').
card_original_text('watchwing scarecrow'/'SHM', 'Watchwing Scarecrow has vigilance as long as you control a white creature.\nWatchwing Scarecrow has flying as long as you control a blue creature.').
card_first_print('watchwing scarecrow', 'SHM').
card_image_name('watchwing scarecrow'/'SHM', 'watchwing scarecrow').
card_uid('watchwing scarecrow'/'SHM', 'SHM:Watchwing Scarecrow:watchwing scarecrow').
card_rarity('watchwing scarecrow'/'SHM', 'Common').
card_artist('watchwing scarecrow'/'SHM', 'Chuck Lukacs').
card_number('watchwing scarecrow'/'SHM', '268').
card_flavor_text('watchwing scarecrow'/'SHM', 'The wings are held in place by wicker rods. The rods are held in place by pure faith.').
card_multiverse_id('watchwing scarecrow'/'SHM', '151630').

card_in_set('wheel of sun and moon', 'SHM').
card_original_type('wheel of sun and moon'/'SHM', 'Enchantment — Aura').
card_original_text('wheel of sun and moon'/'SHM', 'Enchant player\nIf a card would be put into enchanted player\'s graveyard from anywhere, instead that card is revealed and put on the bottom of that player\'s library.').
card_first_print('wheel of sun and moon', 'SHM').
card_image_name('wheel of sun and moon'/'SHM', 'wheel of sun and moon').
card_uid('wheel of sun and moon'/'SHM', 'SHM:Wheel of Sun and Moon:wheel of sun and moon').
card_rarity('wheel of sun and moon'/'SHM', 'Rare').
card_artist('wheel of sun and moon'/'SHM', 'Zoltan Boros & Gabor Szikszai').
card_number('wheel of sun and moon'/'SHM', '243').
card_flavor_text('wheel of sun and moon'/'SHM', 'Every life ends, but life itself never does.').
card_multiverse_id('wheel of sun and moon'/'SHM', '146740').

card_in_set('whimwader', 'SHM').
card_original_type('whimwader'/'SHM', 'Creature — Elemental').
card_original_text('whimwader'/'SHM', 'Whimwader can\'t attack unless defending player controls a blue permanent.').
card_first_print('whimwader', 'SHM').
card_image_name('whimwader'/'SHM', 'whimwader').
card_uid('whimwader'/'SHM', 'SHM:Whimwader:whimwader').
card_rarity('whimwader'/'SHM', 'Common').
card_artist('whimwader'/'SHM', 'Jeff Easley').
card_number('whimwader'/'SHM', '54').
card_flavor_text('whimwader'/'SHM', '\"Thought? Deceit? Progress? Now you\'re just making it hungry.\"\n—Abral, kithkin alchemist').
card_multiverse_id('whimwader'/'SHM', '141957').

card_in_set('wicker warcrawler', 'SHM').
card_original_type('wicker warcrawler'/'SHM', 'Artifact Creature — Scarecrow').
card_original_text('wicker warcrawler'/'SHM', 'Whenever Wicker Warcrawler attacks or blocks, put a -1/-1 counter on it at end of combat.').
card_first_print('wicker warcrawler', 'SHM').
card_image_name('wicker warcrawler'/'SHM', 'wicker warcrawler').
card_uid('wicker warcrawler'/'SHM', 'SHM:Wicker Warcrawler:wicker warcrawler').
card_rarity('wicker warcrawler'/'SHM', 'Uncommon').
card_artist('wicker warcrawler'/'SHM', 'Carl Critchlow').
card_number('wicker warcrawler'/'SHM', '269').
card_flavor_text('wicker warcrawler'/'SHM', 'It\'s the twisted creation of a kithkin diviner who dreamed of very large crows.').
card_multiverse_id('wicker warcrawler'/'SHM', '154410').

card_in_set('wild swing', 'SHM').
card_original_type('wild swing'/'SHM', 'Sorcery').
card_original_text('wild swing'/'SHM', 'Choose three target nonenchantment permanents. Destroy one of them at random.').
card_first_print('wild swing', 'SHM').
card_image_name('wild swing'/'SHM', 'wild swing').
card_uid('wild swing'/'SHM', 'SHM:Wild Swing:wild swing').
card_rarity('wild swing'/'SHM', 'Uncommon').
card_artist('wild swing'/'SHM', 'Richard Kane Ferguson').
card_number('wild swing'/'SHM', '108').
card_flavor_text('wild swing'/'SHM', 'Though the shaman rarely got what she wanted, she was never disappointed in the result.').
card_multiverse_id('wild swing'/'SHM', '158686').

card_in_set('wildslayer elves', 'SHM').
card_original_type('wildslayer elves'/'SHM', 'Creature — Elf Warrior').
card_original_text('wildslayer elves'/'SHM', 'Wither (This deals damage to creatures in the form of -1/-1 counters.)').
card_first_print('wildslayer elves', 'SHM').
card_image_name('wildslayer elves'/'SHM', 'wildslayer elves').
card_uid('wildslayer elves'/'SHM', 'SHM:Wildslayer Elves:wildslayer elves').
card_rarity('wildslayer elves'/'SHM', 'Common').
card_artist('wildslayer elves'/'SHM', 'Dave Kendall').
card_number('wildslayer elves'/'SHM', '133').
card_flavor_text('wildslayer elves'/'SHM', 'Some elves battled too long in the deep shadow, their swords dipped too often in tainted flesh and poisoned blood.').
card_multiverse_id('wildslayer elves'/'SHM', '135436').

card_in_set('wilt-leaf cavaliers', 'SHM').
card_original_type('wilt-leaf cavaliers'/'SHM', 'Creature — Elf Knight').
card_original_text('wilt-leaf cavaliers'/'SHM', 'Vigilance').
card_image_name('wilt-leaf cavaliers'/'SHM', 'wilt-leaf cavaliers').
card_uid('wilt-leaf cavaliers'/'SHM', 'SHM:Wilt-Leaf Cavaliers:wilt-leaf cavaliers').
card_rarity('wilt-leaf cavaliers'/'SHM', 'Uncommon').
card_artist('wilt-leaf cavaliers'/'SHM', 'Steve Prescott').
card_number('wilt-leaf cavaliers'/'SHM', '244').
card_flavor_text('wilt-leaf cavaliers'/'SHM', 'Every elf in Shadowmoor is charged from birth with a terrible duty: to strike back against the ugliness and darkness, even though they are all around and seemingly without end.').
card_multiverse_id('wilt-leaf cavaliers'/'SHM', '153962').

card_in_set('wilt-leaf liege', 'SHM').
card_original_type('wilt-leaf liege'/'SHM', 'Creature — Elf Knight').
card_original_text('wilt-leaf liege'/'SHM', 'Other green creatures you control get +1/+1.\nOther white creatures you control get +1/+1.\nIf a spell or ability an opponent controls causes you to discard Wilt-Leaf Liege, put it into play instead of putting it into your graveyard.').
card_first_print('wilt-leaf liege', 'SHM').
card_image_name('wilt-leaf liege'/'SHM', 'wilt-leaf liege').
card_uid('wilt-leaf liege'/'SHM', 'SHM:Wilt-Leaf Liege:wilt-leaf liege').
card_rarity('wilt-leaf liege'/'SHM', 'Rare').
card_artist('wilt-leaf liege'/'SHM', 'Jason Chan').
card_number('wilt-leaf liege'/'SHM', '245').
card_multiverse_id('wilt-leaf liege'/'SHM', '147439').

card_in_set('windbrisk raptor', 'SHM').
card_original_type('windbrisk raptor'/'SHM', 'Creature — Bird').
card_original_text('windbrisk raptor'/'SHM', 'Flying\nAttacking creatures you control have lifelink.').
card_first_print('windbrisk raptor', 'SHM').
card_image_name('windbrisk raptor'/'SHM', 'windbrisk raptor').
card_uid('windbrisk raptor'/'SHM', 'SHM:Windbrisk Raptor:windbrisk raptor').
card_rarity('windbrisk raptor'/'SHM', 'Rare').
card_artist('windbrisk raptor'/'SHM', 'Omar Rayyan').
card_number('windbrisk raptor'/'SHM', '26').
card_flavor_text('windbrisk raptor'/'SHM', 'It awakened to gloom-fouled skies and responded with a righteous rage that shook the heavens.').
card_multiverse_id('windbrisk raptor'/'SHM', '146090').

card_in_set('wingrattle scarecrow', 'SHM').
card_original_type('wingrattle scarecrow'/'SHM', 'Artifact Creature — Scarecrow').
card_original_text('wingrattle scarecrow'/'SHM', 'Wingrattle Scarecrow has flying as long as you control a blue creature.\nWingrattle Scarecrow has persist as long as you control a black creature. (When this creature is put into a graveyard from play, if it had no -1/-1 counters on it, return it to play under its owner\'s control with a -1/-1 counter on it.)').
card_first_print('wingrattle scarecrow', 'SHM').
card_image_name('wingrattle scarecrow'/'SHM', 'wingrattle scarecrow').
card_uid('wingrattle scarecrow'/'SHM', 'SHM:Wingrattle Scarecrow:wingrattle scarecrow').
card_rarity('wingrattle scarecrow'/'SHM', 'Common').
card_artist('wingrattle scarecrow'/'SHM', 'Trevor Hairsine').
card_number('wingrattle scarecrow'/'SHM', '270').
card_multiverse_id('wingrattle scarecrow'/'SHM', '151633').

card_in_set('witherscale wurm', 'SHM').
card_original_type('witherscale wurm'/'SHM', 'Creature — Wurm').
card_original_text('witherscale wurm'/'SHM', 'Whenever Witherscale Wurm blocks or becomes blocked by a creature, that creature gains wither until end of turn. (It deals damage to creatures in the form of -1/-1 counters.)\nWhenever Witherscale Wurm deals damage to an opponent, remove all -1/-1 counters from it.').
card_first_print('witherscale wurm', 'SHM').
card_image_name('witherscale wurm'/'SHM', 'witherscale wurm').
card_uid('witherscale wurm'/'SHM', 'SHM:Witherscale Wurm:witherscale wurm').
card_rarity('witherscale wurm'/'SHM', 'Rare').
card_artist('witherscale wurm'/'SHM', 'Thomas M. Baxa').
card_number('witherscale wurm'/'SHM', '134').
card_multiverse_id('witherscale wurm'/'SHM', '152068').

card_in_set('woeleecher', 'SHM').
card_original_type('woeleecher'/'SHM', 'Creature — Elemental').
card_original_text('woeleecher'/'SHM', '{W}, {T}: Remove a -1/-1 counter from target creature. If you do, you gain 2 life.').
card_first_print('woeleecher', 'SHM').
card_image_name('woeleecher'/'SHM', 'woeleecher').
card_uid('woeleecher'/'SHM', 'SHM:Woeleecher:woeleecher').
card_rarity('woeleecher'/'SHM', 'Common').
card_artist('woeleecher'/'SHM', 'Izzy').
card_number('woeleecher'/'SHM', '27').
card_flavor_text('woeleecher'/'SHM', 'One head senses the curse. The second loosens it from the soul. The third drinks it as an invigorating mist.').
card_multiverse_id('woeleecher'/'SHM', '153985').

card_in_set('wooded bastion', 'SHM').
card_original_type('wooded bastion'/'SHM', 'Land').
card_original_text('wooded bastion'/'SHM', '{T}: Add {1} to your mana pool.\n{G/W}, {T}: Add {G}{G}, {G}{W}, or {W}{W} to your mana pool.').
card_first_print('wooded bastion', 'SHM').
card_image_name('wooded bastion'/'SHM', 'wooded bastion').
card_uid('wooded bastion'/'SHM', 'SHM:Wooded Bastion:wooded bastion').
card_rarity('wooded bastion'/'SHM', 'Rare').
card_artist('wooded bastion'/'SHM', 'Christopher Moeller').
card_number('wooded bastion'/'SHM', '281').
card_flavor_text('wooded bastion'/'SHM', 'The elves of Wilt-Leaf Wood continually expand the palisade surrounding the sanctuary. They hope to one day enclose the entire forest within its walls.').
card_multiverse_id('wooded bastion'/'SHM', '146747').

card_in_set('woodfall primus', 'SHM').
card_original_type('woodfall primus'/'SHM', 'Creature — Treefolk Shaman').
card_original_text('woodfall primus'/'SHM', 'Trample\nWhen Woodfall Primus comes into play, destroy target noncreature permanent.\nPersist (When this creature is put into a graveyard from play, if it had no -1/-1 counters on it, return it to play under its owner\'s control with a -1/-1 counter on it.)').
card_first_print('woodfall primus', 'SHM').
card_image_name('woodfall primus'/'SHM', 'woodfall primus').
card_uid('woodfall primus'/'SHM', 'SHM:Woodfall Primus:woodfall primus').
card_rarity('woodfall primus'/'SHM', 'Rare').
card_artist('woodfall primus'/'SHM', 'Adam Rex').
card_number('woodfall primus'/'SHM', '135').
card_multiverse_id('woodfall primus'/'SHM', '151987').

card_in_set('worldpurge', 'SHM').
card_original_type('worldpurge'/'SHM', 'Sorcery').
card_original_text('worldpurge'/'SHM', 'Return all permanents to their owners\' hands. Each player chooses up to seven cards in his or her hand, then shuffles the rest into his or her library. Empty all mana pools.').
card_first_print('worldpurge', 'SHM').
card_image_name('worldpurge'/'SHM', 'worldpurge').
card_uid('worldpurge'/'SHM', 'SHM:Worldpurge:worldpurge').
card_rarity('worldpurge'/'SHM', 'Rare').
card_artist('worldpurge'/'SHM', 'Chippy').
card_number('worldpurge'/'SHM', '156').
card_flavor_text('worldpurge'/'SHM', 'The Aurora is not the only magic capable of remaking a world.').
card_multiverse_id('worldpurge'/'SHM', '141991').

card_in_set('wort, the raidmother', 'SHM').
card_original_type('wort, the raidmother'/'SHM', 'Legendary Creature — Goblin Shaman').
card_original_text('wort, the raidmother'/'SHM', 'When Wort, the Raidmother comes into play, put two 1/1 red and green Goblin Warrior creature tokens into play.\nEach red or green instant or sorcery spell you play has conspire. (As you play the spell, you may tap two untapped creatures you control that share a color with it. When you do, copy it and you may choose new targets for the copy.)').
card_first_print('wort, the raidmother', 'SHM').
card_image_name('wort, the raidmother'/'SHM', 'wort, the raidmother').
card_uid('wort, the raidmother'/'SHM', 'SHM:Wort, the Raidmother:wort, the raidmother').
card_rarity('wort, the raidmother'/'SHM', 'Rare').
card_artist('wort, the raidmother'/'SHM', 'Dave Allsop').
card_number('wort, the raidmother'/'SHM', '223').
card_multiverse_id('wort, the raidmother'/'SHM', '147379').

card_in_set('wound reflection', 'SHM').
card_original_type('wound reflection'/'SHM', 'Enchantment').
card_original_text('wound reflection'/'SHM', 'At the end of each turn, each opponent loses life equal to the life he or she lost this turn. (Damage causes loss of life.)').
card_first_print('wound reflection', 'SHM').
card_image_name('wound reflection'/'SHM', 'wound reflection').
card_uid('wound reflection'/'SHM', 'SHM:Wound Reflection:wound reflection').
card_rarity('wound reflection'/'SHM', 'Rare').
card_artist('wound reflection'/'SHM', 'Terese Nielsen & Ron Spencer').
card_number('wound reflection'/'SHM', '81').
card_flavor_text('wound reflection'/'SHM', 'The mission of the Nighthearth, Illulia\'s cult of murderous cinders, is to intensify every pain suffered in Shadowmoor.').
card_multiverse_id('wound reflection'/'SHM', '146762').

card_in_set('zealous guardian', 'SHM').
card_original_type('zealous guardian'/'SHM', 'Creature — Kithkin Soldier').
card_original_text('zealous guardian'/'SHM', 'Flash').
card_first_print('zealous guardian', 'SHM').
card_image_name('zealous guardian'/'SHM', 'zealous guardian').
card_uid('zealous guardian'/'SHM', 'SHM:Zealous Guardian:zealous guardian').
card_rarity('zealous guardian'/'SHM', 'Common').
card_artist('zealous guardian'/'SHM', 'Steven Belledin').
card_number('zealous guardian'/'SHM', '157').
card_flavor_text('zealous guardian'/'SHM', 'Parapet watchers patrol the outer edges of the doun, signaling to others who wait patiently in shadow.').
card_multiverse_id('zealous guardian'/'SHM', '142028').
