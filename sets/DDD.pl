% Duel Decks: Garruk vs. Liliana

set('DDD').
set_name('DDD', 'Duel Decks: Garruk vs. Liliana').
set_release_date('DDD', '2009-10-30').
set_border('DDD', 'black').
set_type('DDD', 'duel deck').

card_in_set('albino troll', 'DDD').
card_original_type('albino troll'/'DDD', 'Creature — Troll').
card_original_text('albino troll'/'DDD', 'Echo {1}{G} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\n{1}{G}: Regenerate Albino Troll.').
card_image_name('albino troll'/'DDD', 'albino troll').
card_uid('albino troll'/'DDD', 'DDD:Albino Troll:albino troll').
card_rarity('albino troll'/'DDD', 'Uncommon').
card_artist('albino troll'/'DDD', 'Paolo Parente').
card_number('albino troll'/'DDD', '3').
card_multiverse_id('albino troll'/'DDD', '201781').

card_in_set('bad moon', 'DDD').
card_original_type('bad moon'/'DDD', 'Enchantment').
card_original_text('bad moon'/'DDD', 'Black creatures get +1/+1.').
card_image_name('bad moon'/'DDD', 'bad moon').
card_uid('bad moon'/'DDD', 'DDD:Bad Moon:bad moon').
card_rarity('bad moon'/'DDD', 'Rare').
card_artist('bad moon'/'DDD', 'Gary Leach').
card_number('bad moon'/'DDD', '48').
card_multiverse_id('bad moon'/'DDD', '201820').

card_in_set('basking rootwalla', 'DDD').
card_original_type('basking rootwalla'/'DDD', 'Creature — Lizard').
card_original_text('basking rootwalla'/'DDD', '{1}{G}: Basking Rootwalla gets +2/+2 until end of turn. Activate this ability only once each turn.\nMadness {0} (If you discard this card, you may cast it for its madness cost instead of putting it into your graveyard.)').
card_image_name('basking rootwalla'/'DDD', 'basking rootwalla').
card_uid('basking rootwalla'/'DDD', 'DDD:Basking Rootwalla:basking rootwalla').
card_rarity('basking rootwalla'/'DDD', 'Common').
card_artist('basking rootwalla'/'DDD', 'Heather Hudson').
card_number('basking rootwalla'/'DDD', '2').
card_multiverse_id('basking rootwalla'/'DDD', '201836').

card_in_set('beast', 'DDD').
card_original_type('beast'/'DDD', 'Creature — Beast').
card_original_text('beast'/'DDD', '').
card_first_print('beast', 'DDD').
card_image_name('beast'/'DDD', 'beast1').
card_uid('beast'/'DDD', 'DDD:Beast:beast1').
card_rarity('beast'/'DDD', 'Common').
card_artist('beast'/'DDD', 'John Donahue').
card_number('beast'/'DDD', 'T1').
card_multiverse_id('beast'/'DDD', '201842').

card_in_set('beast', 'DDD').
card_original_type('beast'/'DDD', 'Creature — Beast').
card_original_text('beast'/'DDD', '').
card_image_name('beast'/'DDD', 'beast2').
card_uid('beast'/'DDD', 'DDD:Beast:beast2').
card_rarity('beast'/'DDD', 'Common').
card_artist('beast'/'DDD', 'Steve Prescott').
card_number('beast'/'DDD', 'T2').
card_multiverse_id('beast'/'DDD', '201844').

card_in_set('beast attack', 'DDD').
card_original_type('beast attack'/'DDD', 'Instant').
card_original_text('beast attack'/'DDD', 'Put a 4/4 green Beast creature token onto the battlefield.\nFlashback {2}{G}{G}{G} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('beast attack'/'DDD', 'beast attack').
card_uid('beast attack'/'DDD', 'DDD:Beast Attack:beast attack').
card_rarity('beast attack'/'DDD', 'Uncommon').
card_artist('beast attack'/'DDD', 'Ciruelo').
card_number('beast attack'/'DDD', '23').
card_multiverse_id('beast attack'/'DDD', '201824').

card_in_set('blastoderm', 'DDD').
card_original_type('blastoderm'/'DDD', 'Creature — Beast').
card_original_text('blastoderm'/'DDD', 'Shroud (This creature can\'t be the target of spells or abilities.)\nFading 3 (This creature enters the battlefield with three fade counters on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)').
card_image_name('blastoderm'/'DDD', 'blastoderm').
card_uid('blastoderm'/'DDD', 'DDD:Blastoderm:blastoderm').
card_rarity('blastoderm'/'DDD', 'Common').
card_artist('blastoderm'/'DDD', 'Nils Hamm').
card_number('blastoderm'/'DDD', '7').
card_multiverse_id('blastoderm'/'DDD', '201831').

card_in_set('corrupt', 'DDD').
card_original_type('corrupt'/'DDD', 'Sorcery').
card_original_text('corrupt'/'DDD', 'Corrupt deals damage equal to the number of Swamps you control to target creature or player. You gain life equal to the damage dealt this way.').
card_image_name('corrupt'/'DDD', 'corrupt').
card_uid('corrupt'/'DDD', 'DDD:Corrupt:corrupt').
card_rarity('corrupt'/'DDD', 'Uncommon').
card_artist('corrupt'/'DDD', 'Dave Allsop').
card_number('corrupt'/'DDD', '57').
card_flavor_text('corrupt'/'DDD', 'There are things beneath the murk for whom the bog is a stew, a thin broth in need of meat.').
card_multiverse_id('corrupt'/'DDD', '201821').

card_in_set('deathgreeter', 'DDD').
card_original_type('deathgreeter'/'DDD', 'Creature — Human Shaman').
card_original_text('deathgreeter'/'DDD', 'Whenever another creature is put into a graveyard from the battlefield, you may gain 1 life.').
card_image_name('deathgreeter'/'DDD', 'deathgreeter').
card_uid('deathgreeter'/'DDD', 'DDD:Deathgreeter:deathgreeter').
card_rarity('deathgreeter'/'DDD', 'Common').
card_artist('deathgreeter'/'DDD', 'Dominick Domingo').
card_number('deathgreeter'/'DDD', '33').
card_flavor_text('deathgreeter'/'DDD', '\"The bones of allies grant wisdom. The bones of enemies grant strength. The bones of dragons grant life eternal.\"').
card_multiverse_id('deathgreeter'/'DDD', '202143').

card_in_set('drudge skeletons', 'DDD').
card_original_type('drudge skeletons'/'DDD', 'Creature — Skeleton').
card_original_text('drudge skeletons'/'DDD', '{B}: Regenerate Drudge Skeletons.').
card_image_name('drudge skeletons'/'DDD', 'drudge skeletons').
card_uid('drudge skeletons'/'DDD', 'DDD:Drudge Skeletons:drudge skeletons').
card_rarity('drudge skeletons'/'DDD', 'Common').
card_artist('drudge skeletons'/'DDD', 'Daarken').
card_number('drudge skeletons'/'DDD', '36').
card_flavor_text('drudge skeletons'/'DDD', '\"The dead make good soldiers. They can\'t disobey orders, never surrender, and don\'t stop fighting when a random body part falls off.\"\n—Nevinyrral, Necromancer\'s Handbook').
card_multiverse_id('drudge skeletons'/'DDD', '202275').

card_in_set('elephant', 'DDD').
card_original_type('elephant'/'DDD', 'Creature — Elephant').
card_original_text('elephant'/'DDD', '').
card_first_print('elephant', 'DDD').
card_image_name('elephant'/'DDD', 'elephant').
card_uid('elephant'/'DDD', 'DDD:Elephant:elephant').
card_rarity('elephant'/'DDD', 'Common').
card_artist('elephant'/'DDD', 'Arnie Swekel').
card_number('elephant'/'DDD', 'T3').
card_multiverse_id('elephant'/'DDD', '201843').

card_in_set('elephant guide', 'DDD').
card_original_type('elephant guide'/'DDD', 'Enchantment — Aura').
card_original_text('elephant guide'/'DDD', 'Enchant creature\nEnchanted creature gets +3/+3.\nWhen enchanted creature is put into a graveyard, put a 3/3 green Elephant creature token onto the battlefield.').
card_image_name('elephant guide'/'DDD', 'elephant guide').
card_uid('elephant guide'/'DDD', 'DDD:Elephant Guide:elephant guide').
card_rarity('elephant guide'/'DDD', 'Uncommon').
card_artist('elephant guide'/'DDD', 'Jim Nelson').
card_number('elephant guide'/'DDD', '18').
card_flavor_text('elephant guide'/'DDD', 'Nature\'s strength outlives the strong.').
card_multiverse_id('elephant guide'/'DDD', '201832').

card_in_set('enslave', 'DDD').
card_original_type('enslave'/'DDD', 'Enchantment — Aura').
card_original_text('enslave'/'DDD', 'Enchant creature\nYou control enchanted creature.\nAt the beginning of your upkeep, enchanted creature deals 1 damage to its owner.').
card_image_name('enslave'/'DDD', 'enslave').
card_uid('enslave'/'DDD', 'DDD:Enslave:enslave').
card_rarity('enslave'/'DDD', 'Uncommon').
card_artist('enslave'/'DDD', 'Zoltan Boros & Gabor Szikszai').
card_number('enslave'/'DDD', '58').
card_multiverse_id('enslave'/'DDD', '201816').

card_in_set('faerie macabre', 'DDD').
card_original_type('faerie macabre'/'DDD', 'Creature — Faerie Rogue').
card_original_text('faerie macabre'/'DDD', 'Flying\nDiscard Faerie Macabre: Exile up to two target cards from graveyards.').
card_image_name('faerie macabre'/'DDD', 'faerie macabre').
card_uid('faerie macabre'/'DDD', 'DDD:Faerie Macabre:faerie macabre').
card_rarity('faerie macabre'/'DDD', 'Common').
card_artist('faerie macabre'/'DDD', 'rk post').
card_number('faerie macabre'/'DDD', '42').
card_flavor_text('faerie macabre'/'DDD', 'The line between dream and death is gauzy and fragile. She leads those too near it from one side to the other.').
card_multiverse_id('faerie macabre'/'DDD', '201822').

card_in_set('fleshbag marauder', 'DDD').
card_original_type('fleshbag marauder'/'DDD', 'Creature — Zombie Warrior').
card_original_text('fleshbag marauder'/'DDD', 'When Fleshbag Marauder enters the battlefield, each player sacrifices a creature.').
card_image_name('fleshbag marauder'/'DDD', 'fleshbag marauder').
card_uid('fleshbag marauder'/'DDD', 'DDD:Fleshbag Marauder:fleshbag marauder').
card_rarity('fleshbag marauder'/'DDD', 'Uncommon').
card_artist('fleshbag marauder'/'DDD', 'Pete Venters').
card_number('fleshbag marauder'/'DDD', '38').
card_flavor_text('fleshbag marauder'/'DDD', 'Grixis is a world where the only things found in abundance are death and decay. Corpses, whole or in part, are the standard currency among necromancers and demons.').
card_multiverse_id('fleshbag marauder'/'DDD', '202144').

card_in_set('forest', 'DDD').
card_original_type('forest'/'DDD', 'Basic Land — Forest').
card_original_text('forest'/'DDD', 'G').
card_image_name('forest'/'DDD', 'forest1').
card_uid('forest'/'DDD', 'DDD:Forest:forest1').
card_rarity('forest'/'DDD', 'Basic Land').
card_artist('forest'/'DDD', 'Rob Alexander').
card_number('forest'/'DDD', '28').
card_multiverse_id('forest'/'DDD', '204966').

card_in_set('forest', 'DDD').
card_original_type('forest'/'DDD', 'Basic Land — Forest').
card_original_text('forest'/'DDD', 'G').
card_image_name('forest'/'DDD', 'forest2').
card_uid('forest'/'DDD', 'DDD:Forest:forest2').
card_rarity('forest'/'DDD', 'Basic Land').
card_artist('forest'/'DDD', 'Glen Angus').
card_number('forest'/'DDD', '29').
card_multiverse_id('forest'/'DDD', '204965').

card_in_set('forest', 'DDD').
card_original_type('forest'/'DDD', 'Basic Land — Forest').
card_original_text('forest'/'DDD', 'G').
card_image_name('forest'/'DDD', 'forest3').
card_uid('forest'/'DDD', 'DDD:Forest:forest3').
card_rarity('forest'/'DDD', 'Basic Land').
card_artist('forest'/'DDD', 'Steven Belledin').
card_number('forest'/'DDD', '30').
card_multiverse_id('forest'/'DDD', '204967').

card_in_set('forest', 'DDD').
card_original_type('forest'/'DDD', 'Basic Land — Forest').
card_original_text('forest'/'DDD', 'G').
card_image_name('forest'/'DDD', 'forest4').
card_uid('forest'/'DDD', 'DDD:Forest:forest4').
card_rarity('forest'/'DDD', 'Basic Land').
card_artist('forest'/'DDD', 'Jim Nelson').
card_number('forest'/'DDD', '31').
card_multiverse_id('forest'/'DDD', '204964').

card_in_set('garruk wildspeaker', 'DDD').
card_original_type('garruk wildspeaker'/'DDD', 'Planeswalker — Garruk').
card_original_text('garruk wildspeaker'/'DDD', '+1: Untap two target lands.\n-1: Put a 3/3 green Beast creature token onto the battlefield.\n-4: Creatures you control get +3/+3 and gain trample until end of turn.').
card_image_name('garruk wildspeaker'/'DDD', 'garruk wildspeaker').
card_uid('garruk wildspeaker'/'DDD', 'DDD:Garruk Wildspeaker:garruk wildspeaker').
card_rarity('garruk wildspeaker'/'DDD', 'Mythic Rare').
card_artist('garruk wildspeaker'/'DDD', 'Terese Nielsen').
card_number('garruk wildspeaker'/'DDD', '1').
card_multiverse_id('garruk wildspeaker'/'DDD', '201347').

card_in_set('genju of the cedars', 'DDD').
card_original_type('genju of the cedars'/'DDD', 'Enchantment — Aura').
card_original_text('genju of the cedars'/'DDD', 'Enchant Forest\n{2}: Enchanted Forest becomes a 4/4 green Spirit creature until end of turn. It\'s still a land.\nWhen enchanted Forest is put into a graveyard, you may return Genju of the Cedars from your graveyard to your hand.').
card_image_name('genju of the cedars'/'DDD', 'genju of the cedars').
card_uid('genju of the cedars'/'DDD', 'DDD:Genju of the Cedars:genju of the cedars').
card_rarity('genju of the cedars'/'DDD', 'Uncommon').
card_artist('genju of the cedars'/'DDD', 'Arnie Swekel').
card_number('genju of the cedars'/'DDD', '13').
card_multiverse_id('genju of the cedars'/'DDD', '201792').

card_in_set('genju of the fens', 'DDD').
card_original_type('genju of the fens'/'DDD', 'Enchantment — Aura').
card_original_text('genju of the fens'/'DDD', 'Enchant Swamp\n{2}: Until end of turn, enchanted Swamp becomes a 2/2 black Spirit creature with \"{B}: This creature gets +1/+1 until end of turn.\" It\'s still a land.\nWhen enchanted Swamp is put into a graveyard, you may return Genju of the Fens from your graveyard to your hand.').
card_image_name('genju of the fens'/'DDD', 'genju of the fens').
card_uid('genju of the fens'/'DDD', 'DDD:Genju of the Fens:genju of the fens').
card_rarity('genju of the fens'/'DDD', 'Uncommon').
card_artist('genju of the fens'/'DDD', 'Tsutomu Kawade').
card_number('genju of the fens'/'DDD', '47').
card_multiverse_id('genju of the fens'/'DDD', '201793').

card_in_set('ghost-lit stalker', 'DDD').
card_original_type('ghost-lit stalker'/'DDD', 'Creature — Spirit').
card_original_text('ghost-lit stalker'/'DDD', '{4}{B}, {T}: Target player discards two cards. Activate this ability only any time you could cast a sorcery.\nChannel — {5}{B}{B}, Discard Ghost-Lit Stalker: Target player discards four cards. Activate this ability only any time you could cast a sorcery.').
card_image_name('ghost-lit stalker'/'DDD', 'ghost-lit stalker').
card_uid('ghost-lit stalker'/'DDD', 'DDD:Ghost-Lit Stalker:ghost-lit stalker').
card_rarity('ghost-lit stalker'/'DDD', 'Uncommon').
card_artist('ghost-lit stalker'/'DDD', 'Hideaki Takamura').
card_number('ghost-lit stalker'/'DDD', '34').
card_multiverse_id('ghost-lit stalker'/'DDD', '202274').

card_in_set('giant growth', 'DDD').
card_original_type('giant growth'/'DDD', 'Instant').
card_original_text('giant growth'/'DDD', 'Target creature gets +3/+3 until end of turn.').
card_image_name('giant growth'/'DDD', 'giant growth').
card_uid('giant growth'/'DDD', 'DDD:Giant Growth:giant growth').
card_rarity('giant growth'/'DDD', 'Common').
card_artist('giant growth'/'DDD', 'Matt Cavotta').
card_number('giant growth'/'DDD', '14').
card_multiverse_id('giant growth'/'DDD', '201839').

card_in_set('harmonize', 'DDD').
card_original_type('harmonize'/'DDD', 'Sorcery').
card_original_text('harmonize'/'DDD', 'Draw three cards.').
card_image_name('harmonize'/'DDD', 'harmonize').
card_uid('harmonize'/'DDD', 'DDD:Harmonize:harmonize').
card_rarity('harmonize'/'DDD', 'Uncommon').
card_artist('harmonize'/'DDD', 'Paul Lee').
card_number('harmonize'/'DDD', '21').
card_flavor_text('harmonize'/'DDD', '\"Words lie. People lie. The land tells the truth.\"').
card_multiverse_id('harmonize'/'DDD', '201817').

card_in_set('hideous end', 'DDD').
card_original_type('hideous end'/'DDD', 'Instant').
card_original_text('hideous end'/'DDD', 'Destroy target nonblack creature. Its controller loses 2 life.').
card_image_name('hideous end'/'DDD', 'hideous end').
card_uid('hideous end'/'DDD', 'DDD:Hideous End:hideous end').
card_rarity('hideous end'/'DDD', 'Common').
card_artist('hideous end'/'DDD', 'Zoltan Boros & Gabor Szikszai').
card_number('hideous end'/'DDD', '52').
card_flavor_text('hideous end'/'DDD', '\"A little dark magic won\'t stop me. The worse the curse, the better the prize.\"\n—Radavi, Joraga relic hunter, last words').
card_multiverse_id('hideous end'/'DDD', '202147').

card_in_set('howling banshee', 'DDD').
card_original_type('howling banshee'/'DDD', 'Creature — Spirit').
card_original_text('howling banshee'/'DDD', 'Flying\nWhen Howling Banshee enters the battlefield, each player loses 3 life.').
card_image_name('howling banshee'/'DDD', 'howling banshee').
card_uid('howling banshee'/'DDD', 'DDD:Howling Banshee:howling banshee').
card_rarity('howling banshee'/'DDD', 'Uncommon').
card_artist('howling banshee'/'DDD', 'Andrew Robinson').
card_number('howling banshee'/'DDD', '43').
card_flavor_text('howling banshee'/'DDD', 'Villagers cloaked the town in magical silence, but their ears still bled.').
card_multiverse_id('howling banshee'/'DDD', '202148').

card_in_set('ichor slick', 'DDD').
card_original_type('ichor slick'/'DDD', 'Sorcery').
card_original_text('ichor slick'/'DDD', 'Target creature gets -3/-3 until end of turn.\nCycling {2} ({2}, Discard this card: Draw a card.)\nMadness {3}{B} (If you discard this card, you may cast it for its madness cost instead of putting it into your graveyard.)').
card_image_name('ichor slick'/'DDD', 'ichor slick').
card_uid('ichor slick'/'DDD', 'DDD:Ichor Slick:ichor slick').
card_rarity('ichor slick'/'DDD', 'Common').
card_artist('ichor slick'/'DDD', 'Zoltan Boros & Gabor Szikszai').
card_number('ichor slick'/'DDD', '51').
card_multiverse_id('ichor slick'/'DDD', '202145').

card_in_set('indrik stomphowler', 'DDD').
card_original_type('indrik stomphowler'/'DDD', 'Creature — Beast').
card_original_text('indrik stomphowler'/'DDD', 'When Indrik Stomphowler enters the battlefield, destroy target artifact or enchantment.').
card_image_name('indrik stomphowler'/'DDD', 'indrik stomphowler').
card_uid('indrik stomphowler'/'DDD', 'DDD:Indrik Stomphowler:indrik stomphowler').
card_rarity('indrik stomphowler'/'DDD', 'Uncommon').
card_artist('indrik stomphowler'/'DDD', 'Carl Critchlow').
card_number('indrik stomphowler'/'DDD', '10').
card_flavor_text('indrik stomphowler'/'DDD', 'An indrik\'s howl has destructive power much subtler than that of its crushing foot.').
card_multiverse_id('indrik stomphowler'/'DDD', '201834').

card_in_set('invigorate', 'DDD').
card_original_type('invigorate'/'DDD', 'Instant').
card_original_text('invigorate'/'DDD', 'If you control a Forest, rather than pay Invigorate\'s mana cost, you may have an opponent gain 3 life.\nTarget creature gets +4/+4 until end of turn.').
card_image_name('invigorate'/'DDD', 'invigorate').
card_uid('invigorate'/'DDD', 'DDD:Invigorate:invigorate').
card_rarity('invigorate'/'DDD', 'Common').
card_artist('invigorate'/'DDD', 'Dan Frazier').
card_number('invigorate'/'DDD', '19').
card_multiverse_id('invigorate'/'DDD', '202261').

card_in_set('keening banshee', 'DDD').
card_original_type('keening banshee'/'DDD', 'Creature — Spirit').
card_original_text('keening banshee'/'DDD', 'Flying\nWhen Keening Banshee enters the battlefield, target creature gets -2/-2 until end of turn.').
card_image_name('keening banshee'/'DDD', 'keening banshee').
card_uid('keening banshee'/'DDD', 'DDD:Keening Banshee:keening banshee').
card_rarity('keening banshee'/'DDD', 'Uncommon').
card_artist('keening banshee'/'DDD', 'Robert Bliss').
card_number('keening banshee'/'DDD', '44').
card_flavor_text('keening banshee'/'DDD', 'Her cold wail echoes in the alleys and under-eaves, finding the unfortunate and turning their blood to ice.').
card_multiverse_id('keening banshee'/'DDD', '201796').

card_in_set('krosan tusker', 'DDD').
card_original_type('krosan tusker'/'DDD', 'Creature — Boar Beast').
card_original_text('krosan tusker'/'DDD', 'Cycling {2}{G} ({2}{G}, Discard this card: Draw a card.)\nWhen you cycle Krosan Tusker, you may search your library for a basic land card, reveal that card, put it into your hand, then shuffle your library.').
card_image_name('krosan tusker'/'DDD', 'krosan tusker').
card_uid('krosan tusker'/'DDD', 'DDD:Krosan Tusker:krosan tusker').
card_rarity('krosan tusker'/'DDD', 'Common').
card_artist('krosan tusker'/'DDD', 'Kev Walker').
card_number('krosan tusker'/'DDD', '11').
card_multiverse_id('krosan tusker'/'DDD', '201829').

card_in_set('lignify', 'DDD').
card_original_type('lignify'/'DDD', 'Tribal Enchantment — Treefolk Aura').
card_original_text('lignify'/'DDD', 'Enchant creature\nEnchanted creature is a 0/4 Treefolk with no abilities.').
card_image_name('lignify'/'DDD', 'lignify').
card_uid('lignify'/'DDD', 'DDD:Lignify:lignify').
card_rarity('lignify'/'DDD', 'Common').
card_artist('lignify'/'DDD', 'Jesper Ejsing').
card_number('lignify'/'DDD', '16').
card_flavor_text('lignify'/'DDD', 'Bulgo paused, puzzled. What was that rustling sound, and why did he feel so stiff? And how could his feet be so thirsty?').
card_multiverse_id('lignify'/'DDD', '201837').

card_in_set('liliana vess', 'DDD').
card_original_type('liliana vess'/'DDD', 'Planeswalker — Liliana').
card_original_text('liliana vess'/'DDD', '+1: Target player discards a card.\n-2: Search your library for a card, then shuffle your library and put that card on top of it.\n-8: Put all creature cards in all graveyards onto the battlefield under your control.').
card_image_name('liliana vess'/'DDD', 'liliana vess').
card_uid('liliana vess'/'DDD', 'DDD:Liliana Vess:liliana vess').
card_rarity('liliana vess'/'DDD', 'Mythic Rare').
card_artist('liliana vess'/'DDD', 'Terese Nielsen').
card_number('liliana vess'/'DDD', '32').
card_multiverse_id('liliana vess'/'DDD', '201348').

card_in_set('mutilate', 'DDD').
card_original_type('mutilate'/'DDD', 'Sorcery').
card_original_text('mutilate'/'DDD', 'All creatures get -1/-1 until end of turn for each Swamp you control.').
card_image_name('mutilate'/'DDD', 'mutilate').
card_uid('mutilate'/'DDD', 'DDD:Mutilate:mutilate').
card_rarity('mutilate'/'DDD', 'Rare').
card_artist('mutilate'/'DDD', 'Zoltan Boros & Gabor Szikszai').
card_number('mutilate'/'DDD', '55').
card_flavor_text('mutilate'/'DDD', 'Liliana developed a fondness for the moment when roaring becomes squealing.').
card_multiverse_id('mutilate'/'DDD', '201819').

card_in_set('nature\'s lore', 'DDD').
card_original_type('nature\'s lore'/'DDD', 'Sorcery').
card_original_text('nature\'s lore'/'DDD', 'Search your library for a Forest card and put that card onto the battlefield. Then shuffle your library.').
card_image_name('nature\'s lore'/'DDD', 'nature\'s lore').
card_uid('nature\'s lore'/'DDD', 'DDD:Nature\'s Lore:nature\'s lore').
card_rarity('nature\'s lore'/'DDD', 'Common').
card_artist('nature\'s lore'/'DDD', 'Terese Nielsen').
card_number('nature\'s lore'/'DDD', '17').
card_flavor_text('nature\'s lore'/'DDD', 'Nature\'s secrets can be read on every tree, every branch, every leaf.').
card_multiverse_id('nature\'s lore'/'DDD', '201840').

card_in_set('overrun', 'DDD').
card_original_type('overrun'/'DDD', 'Sorcery').
card_original_text('overrun'/'DDD', 'Creatures you control get +3/+3 and gain trample until end of turn.').
card_image_name('overrun'/'DDD', 'overrun').
card_uid('overrun'/'DDD', 'DDD:Overrun:overrun').
card_rarity('overrun'/'DDD', 'Uncommon').
card_artist('overrun'/'DDD', 'Carl Critchlow').
card_number('overrun'/'DDD', '24').
card_flavor_text('overrun'/'DDD', 'Nature doesn\'t walk.').
card_multiverse_id('overrun'/'DDD', '201779').

card_in_set('phyrexian rager', 'DDD').
card_original_type('phyrexian rager'/'DDD', 'Creature — Horror').
card_original_text('phyrexian rager'/'DDD', 'When Phyrexian Rager enters the battlefield, you draw a card and you lose 1 life.').
card_image_name('phyrexian rager'/'DDD', 'phyrexian rager').
card_uid('phyrexian rager'/'DDD', 'DDD:Phyrexian Rager:phyrexian rager').
card_rarity('phyrexian rager'/'DDD', 'Common').
card_artist('phyrexian rager'/'DDD', 'Mark Tedin').
card_number('phyrexian rager'/'DDD', '39').
card_flavor_text('phyrexian rager'/'DDD', 'It takes no prisoners, but it keeps the choicest bits for Phyrexia.').
card_multiverse_id('phyrexian rager'/'DDD', '201790').

card_in_set('plated slagwurm', 'DDD').
card_original_type('plated slagwurm'/'DDD', 'Creature — Wurm').
card_original_text('plated slagwurm'/'DDD', 'Plated Slagwurm can\'t be the target of spells or abilities your opponents control.').
card_image_name('plated slagwurm'/'DDD', 'plated slagwurm').
card_uid('plated slagwurm'/'DDD', 'DDD:Plated Slagwurm:plated slagwurm').
card_rarity('plated slagwurm'/'DDD', 'Rare').
card_artist('plated slagwurm'/'DDD', 'Justin Sweet').
card_number('plated slagwurm'/'DDD', '12').
card_flavor_text('plated slagwurm'/'DDD', 'Beneath the Tangle, the wurm tunnels stretch . . . wide as a stone\'s throw, long as forever, deep as you dare.').
card_multiverse_id('plated slagwurm'/'DDD', '201835').

card_in_set('polluted mire', 'DDD').
card_original_type('polluted mire'/'DDD', 'Land').
card_original_text('polluted mire'/'DDD', 'Polluted Mire enters the battlefield tapped.\n{T}: Add {B} to your mana pool.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_image_name('polluted mire'/'DDD', 'polluted mire').
card_uid('polluted mire'/'DDD', 'DDD:Polluted Mire:polluted mire').
card_rarity('polluted mire'/'DDD', 'Common').
card_artist('polluted mire'/'DDD', 'Stephen Daniele').
card_number('polluted mire'/'DDD', '59').
card_multiverse_id('polluted mire'/'DDD', '201782').

card_in_set('rancor', 'DDD').
card_original_type('rancor'/'DDD', 'Enchantment — Aura').
card_original_text('rancor'/'DDD', 'Enchant creature\nEnchanted creature gets +2/+0 and has trample.\nWhen Rancor is put into a graveyard from the battlefield, return Rancor to its owner\'s hand.').
card_image_name('rancor'/'DDD', 'rancor').
card_uid('rancor'/'DDD', 'DDD:Rancor:rancor').
card_rarity('rancor'/'DDD', 'Common').
card_artist('rancor'/'DDD', 'Kev Walker').
card_number('rancor'/'DDD', '15').
card_flavor_text('rancor'/'DDD', 'Hatred outlives the hateful.').
card_multiverse_id('rancor'/'DDD', '201838').

card_in_set('ravenous baloth', 'DDD').
card_original_type('ravenous baloth'/'DDD', 'Creature — Beast').
card_original_text('ravenous baloth'/'DDD', 'Sacrifice a Beast: You gain 4 life.').
card_image_name('ravenous baloth'/'DDD', 'ravenous baloth').
card_uid('ravenous baloth'/'DDD', 'DDD:Ravenous Baloth:ravenous baloth').
card_rarity('ravenous baloth'/'DDD', 'Rare').
card_artist('ravenous baloth'/'DDD', 'Arnie Swekel').
card_number('ravenous baloth'/'DDD', '8').
card_flavor_text('ravenous baloth'/'DDD', '\"All we know about the Krosan Forest we have learned from those few who made it out alive.\"\n—Elvish refugee').
card_multiverse_id('ravenous baloth'/'DDD', '201882').

card_in_set('ravenous rats', 'DDD').
card_original_type('ravenous rats'/'DDD', 'Creature — Rat').
card_original_text('ravenous rats'/'DDD', 'When Ravenous Rats enters the battlefield, target opponent discards a card.').
card_image_name('ravenous rats'/'DDD', 'ravenous rats').
card_uid('ravenous rats'/'DDD', 'DDD:Ravenous Rats:ravenous rats').
card_rarity('ravenous rats'/'DDD', 'Common').
card_artist('ravenous rats'/'DDD', 'Carl Critchlow').
card_number('ravenous rats'/'DDD', '37').
card_flavor_text('ravenous rats'/'DDD', 'Nothing is sacred to rats. Everything is simply another meal.').
card_multiverse_id('ravenous rats'/'DDD', '201791').

card_in_set('rise from the grave', 'DDD').
card_original_type('rise from the grave'/'DDD', 'Sorcery').
card_original_text('rise from the grave'/'DDD', 'Put target creature card in a graveyard onto the battlefield under your control. That creature is a black Zombie in addition to its other colors and types.').
card_image_name('rise from the grave'/'DDD', 'rise from the grave').
card_uid('rise from the grave'/'DDD', 'DDD:Rise from the Grave:rise from the grave').
card_rarity('rise from the grave'/'DDD', 'Uncommon').
card_artist('rise from the grave'/'DDD', 'Vance Kovacs').
card_number('rise from the grave'/'DDD', '56').
card_flavor_text('rise from the grave'/'DDD', '\"Death is no excuse for disobedience.\"\n—Liliana Vess').
card_multiverse_id('rise from the grave'/'DDD', '201800').

card_in_set('rude awakening', 'DDD').
card_original_type('rude awakening'/'DDD', 'Sorcery').
card_original_text('rude awakening'/'DDD', 'Choose one — Untap all lands you control; or until end of turn, lands you control become 2/2 creatures that are still lands.\nEntwine {2}{G} (Choose both if you pay the entwine cost.)').
card_image_name('rude awakening'/'DDD', 'rude awakening').
card_uid('rude awakening'/'DDD', 'DDD:Rude Awakening:rude awakening').
card_rarity('rude awakening'/'DDD', 'Rare').
card_artist('rude awakening'/'DDD', 'Ittoku').
card_number('rude awakening'/'DDD', '22').
card_multiverse_id('rude awakening'/'DDD', '202281').

card_in_set('serrated arrows', 'DDD').
card_original_type('serrated arrows'/'DDD', 'Artifact').
card_original_text('serrated arrows'/'DDD', 'Serrated Arrows enters the battlefield with three arrowhead counters on it.\nAt the beginning of your upkeep, if there are no arrowhead counters on Serrated Arrows, sacrifice it.\n{T}, Remove an arrowhead counter from Serrated Arrows: Put a -1/-1 counter on target creature.').
card_image_name('serrated arrows'/'DDD', 'serrated arrows').
card_uid('serrated arrows'/'DDD', 'DDD:Serrated Arrows:serrated arrows').
card_rarity('serrated arrows'/'DDD', 'Common').
card_artist('serrated arrows'/'DDD', 'John Avon').
card_number('serrated arrows'/'DDD', '20').
card_multiverse_id('serrated arrows'/'DDD', '202280').

card_in_set('sign in blood', 'DDD').
card_original_type('sign in blood'/'DDD', 'Sorcery').
card_original_text('sign in blood'/'DDD', 'Target player draws two cards and loses 2 life.').
card_image_name('sign in blood'/'DDD', 'sign in blood').
card_uid('sign in blood'/'DDD', 'DDD:Sign in Blood:sign in blood').
card_rarity('sign in blood'/'DDD', 'Common').
card_artist('sign in blood'/'DDD', 'Howard Lyon').
card_number('sign in blood'/'DDD', '49').
card_flavor_text('sign in blood'/'DDD', '\"You know I accept only one currency here, and yet you have sought me out. Why now do you hesitate?\"\n—Xathrid demon').
card_multiverse_id('sign in blood'/'DDD', '201801').

card_in_set('skeletal vampire', 'DDD').
card_original_type('skeletal vampire'/'DDD', 'Creature — Vampire Skeleton').
card_original_text('skeletal vampire'/'DDD', 'Flying\nWhen Skeletal Vampire enters the battlefield, put two 1/1 black Bat creature tokens onto the battlefield.\n{3}{B}{B}, Sacrifice a Bat: Put two 1/1 black Bat creature tokens with flying onto the battlefield.\nSacrifice a Bat: Regenerate Skeletal Vampire.').
card_image_name('skeletal vampire'/'DDD', 'skeletal vampire').
card_uid('skeletal vampire'/'DDD', 'DDD:Skeletal Vampire:skeletal vampire').
card_rarity('skeletal vampire'/'DDD', 'Rare').
card_artist('skeletal vampire'/'DDD', 'Wayne Reynolds').
card_number('skeletal vampire'/'DDD', '46').
card_multiverse_id('skeletal vampire'/'DDD', '201809').

card_in_set('slippery karst', 'DDD').
card_original_type('slippery karst'/'DDD', 'Land').
card_original_text('slippery karst'/'DDD', 'Slippery Karst enters the battlefield tapped.\n{T}: Add {G} to your mana pool.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_image_name('slippery karst'/'DDD', 'slippery karst').
card_uid('slippery karst'/'DDD', 'DDD:Slippery Karst:slippery karst').
card_rarity('slippery karst'/'DDD', 'Common').
card_artist('slippery karst'/'DDD', 'Stephen Daniele').
card_number('slippery karst'/'DDD', '26').
card_multiverse_id('slippery karst'/'DDD', '201784').

card_in_set('snuff out', 'DDD').
card_original_type('snuff out'/'DDD', 'Instant').
card_original_text('snuff out'/'DDD', 'If you control a Swamp, you may pay 4 life rather than pay Snuff Out\'s mana cost.\nDestroy target nonblack creature. It can\'t be regenerated.').
card_image_name('snuff out'/'DDD', 'snuff out').
card_uid('snuff out'/'DDD', 'DDD:Snuff Out:snuff out').
card_rarity('snuff out'/'DDD', 'Common').
card_artist('snuff out'/'DDD', 'Steve Argyle').
card_number('snuff out'/'DDD', '53').
card_flavor_text('snuff out'/'DDD', '\"The bigger they are, the faster they rot.\"').
card_multiverse_id('snuff out'/'DDD', '201794').

card_in_set('stampeding wildebeests', 'DDD').
card_original_type('stampeding wildebeests'/'DDD', 'Creature — Antelope Beast').
card_original_text('stampeding wildebeests'/'DDD', 'Trample\nAt the beginning of your upkeep, return a green creature you control to its owner\'s hand.').
card_image_name('stampeding wildebeests'/'DDD', 'stampeding wildebeests').
card_uid('stampeding wildebeests'/'DDD', 'DDD:Stampeding Wildebeests:stampeding wildebeests').
card_rarity('stampeding wildebeests'/'DDD', 'Uncommon').
card_artist('stampeding wildebeests'/'DDD', 'Randy Gallegos').
card_number('stampeding wildebeests'/'DDD', '9').
card_flavor_text('stampeding wildebeests'/'DDD', 'The most violent and destructive storms in Femeref occur on cloudless days.').
card_multiverse_id('stampeding wildebeests'/'DDD', '201881').

card_in_set('swamp', 'DDD').
card_original_type('swamp'/'DDD', 'Basic Land — Swamp').
card_original_text('swamp'/'DDD', 'B').
card_image_name('swamp'/'DDD', 'swamp1').
card_uid('swamp'/'DDD', 'DDD:Swamp:swamp1').
card_rarity('swamp'/'DDD', 'Basic Land').
card_artist('swamp'/'DDD', 'Stephan Martiniere').
card_number('swamp'/'DDD', '60').
card_multiverse_id('swamp'/'DDD', '204970').

card_in_set('swamp', 'DDD').
card_original_type('swamp'/'DDD', 'Basic Land — Swamp').
card_original_text('swamp'/'DDD', 'B').
card_image_name('swamp'/'DDD', 'swamp2').
card_uid('swamp'/'DDD', 'DDD:Swamp:swamp2').
card_rarity('swamp'/'DDD', 'Basic Land').
card_artist('swamp'/'DDD', 'Christopher Moeller').
card_number('swamp'/'DDD', '61').
card_multiverse_id('swamp'/'DDD', '204969').

card_in_set('swamp', 'DDD').
card_original_type('swamp'/'DDD', 'Basic Land — Swamp').
card_original_text('swamp'/'DDD', 'B').
card_image_name('swamp'/'DDD', 'swamp3').
card_uid('swamp'/'DDD', 'DDD:Swamp:swamp3').
card_rarity('swamp'/'DDD', 'Basic Land').
card_artist('swamp'/'DDD', 'Anthony S. Waters').
card_number('swamp'/'DDD', '62').
card_multiverse_id('swamp'/'DDD', '204968').

card_in_set('swamp', 'DDD').
card_original_type('swamp'/'DDD', 'Basic Land — Swamp').
card_original_text('swamp'/'DDD', 'B').
card_image_name('swamp'/'DDD', 'swamp4').
card_uid('swamp'/'DDD', 'DDD:Swamp:swamp4').
card_rarity('swamp'/'DDD', 'Basic Land').
card_artist('swamp'/'DDD', 'Richard Wright').
card_number('swamp'/'DDD', '63').
card_multiverse_id('swamp'/'DDD', '204971').

card_in_set('tendrils of corruption', 'DDD').
card_original_type('tendrils of corruption'/'DDD', 'Instant').
card_original_text('tendrils of corruption'/'DDD', 'Tendrils of Corruption deals X damage to target creature and you gain X life, where X is the number of Swamps you control.').
card_image_name('tendrils of corruption'/'DDD', 'tendrils of corruption').
card_uid('tendrils of corruption'/'DDD', 'DDD:Tendrils of Corruption:tendrils of corruption').
card_rarity('tendrils of corruption'/'DDD', 'Common').
card_artist('tendrils of corruption'/'DDD', 'Vance Kovacs').
card_number('tendrils of corruption'/'DDD', '54').
card_flavor_text('tendrils of corruption'/'DDD', 'Darkness doesn\'t always send its minions to do its dirty work.').
card_multiverse_id('tendrils of corruption'/'DDD', '201812').

card_in_set('treetop village', 'DDD').
card_original_type('treetop village'/'DDD', 'Land').
card_original_text('treetop village'/'DDD', 'Treetop Village enters the battlefield tapped.\n{T}: Add {G} to your mana pool.\n{1}{G}: Treetop Village becomes a 3/3 green Ape creature with trample until end of turn. It\'s still a land.').
card_image_name('treetop village'/'DDD', 'treetop village').
card_uid('treetop village'/'DDD', 'DDD:Treetop Village:treetop village').
card_rarity('treetop village'/'DDD', 'Uncommon').
card_artist('treetop village'/'DDD', 'Rob Alexander').
card_number('treetop village'/'DDD', '27').
card_multiverse_id('treetop village'/'DDD', '202279').

card_in_set('twisted abomination', 'DDD').
card_original_type('twisted abomination'/'DDD', 'Creature — Zombie Mutant').
card_original_text('twisted abomination'/'DDD', '{B}: Regenerate Twisted Abomination.\nSwampcycling {2} ({2}, Discard this card: Search your library for a Swamp card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('twisted abomination'/'DDD', 'twisted abomination').
card_uid('twisted abomination'/'DDD', 'DDD:Twisted Abomination:twisted abomination').
card_rarity('twisted abomination'/'DDD', 'Common').
card_artist('twisted abomination'/'DDD', 'Daren Bader').
card_number('twisted abomination'/'DDD', '45').
card_multiverse_id('twisted abomination'/'DDD', '201813').

card_in_set('urborg syphon-mage', 'DDD').
card_original_type('urborg syphon-mage'/'DDD', 'Creature — Human Spellshaper').
card_original_text('urborg syphon-mage'/'DDD', '{2}{B}, {T}, Discard a card: Each other player loses 2 life. You gain life equal to the life lost this way.').
card_image_name('urborg syphon-mage'/'DDD', 'urborg syphon-mage').
card_uid('urborg syphon-mage'/'DDD', 'DDD:Urborg Syphon-Mage:urborg syphon-mage').
card_rarity('urborg syphon-mage'/'DDD', 'Common').
card_artist('urborg syphon-mage'/'DDD', 'Greg Staples').
card_number('urborg syphon-mage'/'DDD', '40').
card_flavor_text('urborg syphon-mage'/'DDD', '\"I have become a gourmet of sorts. Each soul has its own distinctive flavor. The art is in inviting the right company to the feast.\"').
card_multiverse_id('urborg syphon-mage'/'DDD', '202146').

card_in_set('vampire bats', 'DDD').
card_original_type('vampire bats'/'DDD', 'Creature — Bat').
card_original_text('vampire bats'/'DDD', 'Flying\n{B}: Vampire Bats gets +1/+0 until end of turn. Activate this ability no more than twice each turn.').
card_image_name('vampire bats'/'DDD', 'vampire bats').
card_uid('vampire bats'/'DDD', 'DDD:Vampire Bats:vampire bats').
card_rarity('vampire bats'/'DDD', 'Common').
card_artist('vampire bats'/'DDD', 'Chippy').
card_number('vampire bats'/'DDD', '35').
card_multiverse_id('vampire bats'/'DDD', '202276').

card_in_set('vicious hunger', 'DDD').
card_original_type('vicious hunger'/'DDD', 'Sorcery').
card_original_text('vicious hunger'/'DDD', 'Vicious Hunger deals 2 damage to target creature and you gain 2 life.').
card_image_name('vicious hunger'/'DDD', 'vicious hunger').
card_uid('vicious hunger'/'DDD', 'DDD:Vicious Hunger:vicious hunger').
card_rarity('vicious hunger'/'DDD', 'Common').
card_artist('vicious hunger'/'DDD', 'Massimilano Frezzato').
card_number('vicious hunger'/'DDD', '50').
card_flavor_text('vicious hunger'/'DDD', 'Only the most ravenous soul feeds on the health of the weak.').
card_multiverse_id('vicious hunger'/'DDD', '202262').

card_in_set('vine trellis', 'DDD').
card_original_type('vine trellis'/'DDD', 'Creature — Plant Wall').
card_original_text('vine trellis'/'DDD', 'Defender\n{T}: Add {G} to your mana pool.').
card_image_name('vine trellis'/'DDD', 'vine trellis').
card_uid('vine trellis'/'DDD', 'DDD:Vine Trellis:vine trellis').
card_rarity('vine trellis'/'DDD', 'Common').
card_artist('vine trellis'/'DDD', 'DiTerlizzi').
card_number('vine trellis'/'DDD', '4').
card_flavor_text('vine trellis'/'DDD', 'Nature twists knots stronger than any rope.').
card_multiverse_id('vine trellis'/'DDD', '201845').

card_in_set('wall of bone', 'DDD').
card_original_type('wall of bone'/'DDD', 'Creature — Skeleton Wall').
card_original_text('wall of bone'/'DDD', 'Defender\n{B}: Regenerate Wall of Bone.').
card_image_name('wall of bone'/'DDD', 'wall of bone').
card_uid('wall of bone'/'DDD', 'DDD:Wall of Bone:wall of bone').
card_rarity('wall of bone'/'DDD', 'Uncommon').
card_artist('wall of bone'/'DDD', 'Jaime Jones').
card_number('wall of bone'/'DDD', '41').
card_flavor_text('wall of bone'/'DDD', '\"Graveyards are sacrilege! A waste of perfectly good bones.\"\n—Keren-Dur, necromancer lord').
card_multiverse_id('wall of bone'/'DDD', '202277').

card_in_set('wild mongrel', 'DDD').
card_original_type('wild mongrel'/'DDD', 'Creature — Hound').
card_original_text('wild mongrel'/'DDD', 'Discard a card: Wild Mongrel gets +1/+1 and becomes the color of your choice until end of turn.').
card_image_name('wild mongrel'/'DDD', 'wild mongrel').
card_uid('wild mongrel'/'DDD', 'DDD:Wild Mongrel:wild mongrel').
card_rarity('wild mongrel'/'DDD', 'Common').
card_artist('wild mongrel'/'DDD', 'Anthony S. Waters').
card_number('wild mongrel'/'DDD', '5').
card_flavor_text('wild mongrel'/'DDD', 'It teaches you to play dead.').
card_multiverse_id('wild mongrel'/'DDD', '201826').

card_in_set('windstorm', 'DDD').
card_original_type('windstorm'/'DDD', 'Instant').
card_original_text('windstorm'/'DDD', 'Windstorm deals X damage to each creature with flying.').
card_image_name('windstorm'/'DDD', 'windstorm').
card_uid('windstorm'/'DDD', 'DDD:Windstorm:windstorm').
card_rarity('windstorm'/'DDD', 'Uncommon').
card_artist('windstorm'/'DDD', 'Rob Alexander').
card_number('windstorm'/'DDD', '25').
card_flavor_text('windstorm'/'DDD', '\"We don\'t like interlopers and thieves cluttering our skies.\"\n—Dionus, elvish archdruid').
card_multiverse_id('windstorm'/'DDD', '201780').

card_in_set('wirewood savage', 'DDD').
card_original_type('wirewood savage'/'DDD', 'Creature — Elf').
card_original_text('wirewood savage'/'DDD', 'Whenever a Beast enters the battlefield, you may draw a card.').
card_image_name('wirewood savage'/'DDD', 'wirewood savage').
card_uid('wirewood savage'/'DDD', 'DDD:Wirewood Savage:wirewood savage').
card_rarity('wirewood savage'/'DDD', 'Common').
card_artist('wirewood savage'/'DDD', 'DiTerlizzi').
card_number('wirewood savage'/'DDD', '6').
card_flavor_text('wirewood savage'/'DDD', '\"She is truly Wirewood\'s child now.\"\n—Elvish refugee').
card_multiverse_id('wirewood savage'/'DDD', '201830').
