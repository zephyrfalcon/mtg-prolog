% Duel Decks: Knights vs. Dragons

set('DDG').
set_name('DDG', 'Duel Decks: Knights vs. Dragons').
set_release_date('DDG', '2011-04-01').
set_border('DDG', 'black').
set_type('DDG', 'duel deck').

card_in_set('alaborn cavalier', 'DDG').
card_original_type('alaborn cavalier'/'DDG', 'Creature — Human Knight').
card_original_text('alaborn cavalier'/'DDG', 'Whenever Alaborn Cavalier attacks, you may tap target creature.').
card_image_name('alaborn cavalier'/'DDG', 'alaborn cavalier').
card_uid('alaborn cavalier'/'DDG', 'DDG:Alaborn Cavalier:alaborn cavalier').
card_rarity('alaborn cavalier'/'DDG', 'Uncommon').
card_artist('alaborn cavalier'/'DDG', 'Kev Walker').
card_number('alaborn cavalier'/'DDG', '18').
card_flavor_text('alaborn cavalier'/'DDG', '\"Course he ran! I wouldn\'t want to stare down that barrel, either!\"\n—Alaborn soldier').
card_multiverse_id('alaborn cavalier'/'DDG', '243435').

card_in_set('armillary sphere', 'DDG').
card_original_type('armillary sphere'/'DDG', 'Artifact').
card_original_text('armillary sphere'/'DDG', '{2}, {T}, Sacrifice Armillary Sphere: Search your library for up to two basic land cards, reveal them, and put them into your hand. Then shuffle your library.').
card_image_name('armillary sphere'/'DDG', 'armillary sphere').
card_uid('armillary sphere'/'DDG', 'DDG:Armillary Sphere:armillary sphere').
card_rarity('armillary sphere'/'DDG', 'Common').
card_artist('armillary sphere'/'DDG', 'Franz Vohwinkel').
card_number('armillary sphere'/'DDG', '62').
card_flavor_text('armillary sphere'/'DDG', 'The mysterious purpose of two of the rings had eluded Esper mages—until now.').
card_multiverse_id('armillary sphere'/'DDG', '243480').

card_in_set('benalish lancer', 'DDG').
card_original_type('benalish lancer'/'DDG', 'Creature — Human Knight').
card_original_text('benalish lancer'/'DDG', 'Kicker {2}{W} (You may pay an additional {2}{W} as you cast this spell.)\nIf Benalish Lancer was kicked, it enters the battlefield with two +1/+1 counters on it and with first strike.').
card_image_name('benalish lancer'/'DDG', 'benalish lancer').
card_uid('benalish lancer'/'DDG', 'DDG:Benalish Lancer:benalish lancer').
card_rarity('benalish lancer'/'DDG', 'Common').
card_artist('benalish lancer'/'DDG', 'Paolo Parente').
card_number('benalish lancer'/'DDG', '12').
card_multiverse_id('benalish lancer'/'DDG', '243429').

card_in_set('bloodmark mentor', 'DDG').
card_original_type('bloodmark mentor'/'DDG', 'Creature — Goblin Warrior').
card_original_text('bloodmark mentor'/'DDG', 'Red creatures you control have first strike.').
card_image_name('bloodmark mentor'/'DDG', 'bloodmark mentor').
card_uid('bloodmark mentor'/'DDG', 'DDG:Bloodmark Mentor:bloodmark mentor').
card_rarity('bloodmark mentor'/'DDG', 'Uncommon').
card_artist('bloodmark mentor'/'DDG', 'Dave Allsop').
card_number('bloodmark mentor'/'DDG', '50').
card_flavor_text('bloodmark mentor'/'DDG', 'Boggarts divide the world into two categories: things you can eat, and things you have to chase down and pummel before you can eat.').
card_multiverse_id('bloodmark mentor'/'DDG', '243467').

card_in_set('bogardan hellkite', 'DDG').
card_original_type('bogardan hellkite'/'DDG', 'Creature — Dragon').
card_original_text('bogardan hellkite'/'DDG', 'Flash\nFlying\nWhen Bogardan Hellkite enters the battlefield, it deals 5 damage divided as you choose among any number of target creatures and/or players.').
card_image_name('bogardan hellkite'/'DDG', 'bogardan hellkite').
card_uid('bogardan hellkite'/'DDG', 'DDG:Bogardan Hellkite:bogardan hellkite').
card_rarity('bogardan hellkite'/'DDG', 'Mythic Rare').
card_artist('bogardan hellkite'/'DDG', 'Wayne Reynolds').
card_number('bogardan hellkite'/'DDG', '47').
card_multiverse_id('bogardan hellkite'/'DDG', '243464').

card_in_set('bogardan rager', 'DDG').
card_original_type('bogardan rager'/'DDG', 'Creature — Elemental').
card_original_text('bogardan rager'/'DDG', 'Flash (You may cast this spell any time you could cast an instant.)\nWhen Bogardan Rager enters the battlefield, target creature gets +4/+0 until end of turn.').
card_image_name('bogardan rager'/'DDG', 'bogardan rager').
card_uid('bogardan rager'/'DDG', 'DDG:Bogardan Rager:bogardan rager').
card_rarity('bogardan rager'/'DDG', 'Common').
card_artist('bogardan rager'/'DDG', 'Clint Langley').
card_number('bogardan rager'/'DDG', '57').
card_flavor_text('bogardan rager'/'DDG', 'In the erupting heart of Bogardan, it\'s hard to tell hurtling volcanic rocks from pouncing volcanic beasts.').
card_multiverse_id('bogardan rager'/'DDG', '243475').

card_in_set('breath of darigaaz', 'DDG').
card_original_type('breath of darigaaz'/'DDG', 'Sorcery').
card_original_text('breath of darigaaz'/'DDG', 'Kicker {2} (You may pay an additional {2} as you cast this spell.)\nBreath of Darigaaz deals 1 damage to each creature without flying and each player. If Breath of Darigaaz was kicked, it deals 4 damage to each creature without flying and each player instead.').
card_image_name('breath of darigaaz'/'DDG', 'breath of darigaaz').
card_uid('breath of darigaaz'/'DDG', 'DDG:Breath of Darigaaz:breath of darigaaz').
card_rarity('breath of darigaaz'/'DDG', 'Uncommon').
card_artist('breath of darigaaz'/'DDG', 'Greg & Tim Hildebrandt').
card_number('breath of darigaaz'/'DDG', '64').
card_multiverse_id('breath of darigaaz'/'DDG', '243482').

card_in_set('captive flame', 'DDG').
card_original_type('captive flame'/'DDG', 'Enchantment').
card_original_text('captive flame'/'DDG', '{R}: Target creature gets +1/+0 until end of turn.').
card_image_name('captive flame'/'DDG', 'captive flame').
card_uid('captive flame'/'DDG', 'DDG:Captive Flame:captive flame').
card_rarity('captive flame'/'DDG', 'Uncommon').
card_artist('captive flame'/'DDG', 'Keith Garletts').
card_number('captive flame'/'DDG', '68').
card_flavor_text('captive flame'/'DDG', '\"Kills and cooks your food in one easy stroke.\"').
card_multiverse_id('captive flame'/'DDG', '243485').

card_in_set('caravan escort', 'DDG').
card_original_type('caravan escort'/'DDG', 'Creature — Human Knight').
card_original_text('caravan escort'/'DDG', 'Level up {2} ({2}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-4\n2/2\nLEVEL 5+\n5/5\nFirst strike').
card_image_name('caravan escort'/'DDG', 'caravan escort').
card_uid('caravan escort'/'DDG', 'DDG:Caravan Escort:caravan escort').
card_rarity('caravan escort'/'DDG', 'Common').
card_artist('caravan escort'/'DDG', 'Goran Josic').
card_number('caravan escort'/'DDG', '2').
card_multiverse_id('caravan escort'/'DDG', '243418').

card_in_set('cinder wall', 'DDG').
card_original_type('cinder wall'/'DDG', 'Creature — Wall').
card_original_text('cinder wall'/'DDG', 'Defender\nWhen Cinder Wall blocks, destroy it at end of combat.').
card_image_name('cinder wall'/'DDG', 'cinder wall').
card_uid('cinder wall'/'DDG', 'DDG:Cinder Wall:cinder wall').
card_rarity('cinder wall'/'DDG', 'Common').
card_artist('cinder wall'/'DDG', 'Anthony S. Waters').
card_number('cinder wall'/'DDG', '48').
card_flavor_text('cinder wall'/'DDG', '\"The smell of roasted flesh hung over the valley for hours.\"\n—Onean sergeant').
card_multiverse_id('cinder wall'/'DDG', '243466').

card_in_set('claws of valakut', 'DDG').
card_original_type('claws of valakut'/'DDG', 'Enchantment — Aura').
card_original_text('claws of valakut'/'DDG', 'Enchant creature\nEnchanted creature gets +1/+0 for each Mountain you control and has first strike.').
card_image_name('claws of valakut'/'DDG', 'claws of valakut').
card_uid('claws of valakut'/'DDG', 'DDG:Claws of Valakut:claws of valakut').
card_rarity('claws of valakut'/'DDG', 'Common').
card_artist('claws of valakut'/'DDG', 'Igor Kieryluk').
card_number('claws of valakut'/'DDG', '72').
card_flavor_text('claws of valakut'/'DDG', '\"Swords may sunder. Shields may fail. My will is stronger than steel, and my flesh and bone far more deadly.\"').
card_multiverse_id('claws of valakut'/'DDG', '243489').

card_in_set('cone of flame', 'DDG').
card_original_type('cone of flame'/'DDG', 'Sorcery').
card_original_text('cone of flame'/'DDG', 'Cone of Flame deals 1 damage to target creature or player, 2 damage to another target creature or player, and 3 damage to a third target creature or player.').
card_image_name('cone of flame'/'DDG', 'cone of flame').
card_uid('cone of flame'/'DDG', 'DDG:Cone of Flame:cone of flame').
card_rarity('cone of flame'/'DDG', 'Uncommon').
card_artist('cone of flame'/'DDG', 'Chippy').
card_number('cone of flame'/'DDG', '75').
card_multiverse_id('cone of flame'/'DDG', '243492').

card_in_set('dragon fodder', 'DDG').
card_original_type('dragon fodder'/'DDG', 'Sorcery').
card_original_text('dragon fodder'/'DDG', 'Put two 1/1 red Goblin creature tokens onto the battlefield.').
card_image_name('dragon fodder'/'DDG', 'dragon fodder').
card_uid('dragon fodder'/'DDG', 'DDG:Dragon Fodder:dragon fodder').
card_rarity('dragon fodder'/'DDG', 'Common').
card_artist('dragon fodder'/'DDG', 'Jaime Jones').
card_number('dragon fodder'/'DDG', '65').
card_flavor_text('dragon fodder'/'DDG', 'Goblins journey to the sacrificial peaks in pairs so that the rare survivor might be able to relate the details of the other\'s grisly demise.').
card_multiverse_id('dragon fodder'/'DDG', '243468').

card_in_set('dragon whelp', 'DDG').
card_original_type('dragon whelp'/'DDG', 'Creature — Dragon').
card_original_text('dragon whelp'/'DDG', 'Flying\n{R}: Dragon Whelp gets +1/+0 until end of turn. If this ability has been activated four or more times this turn, sacrifice Dragon Whelp at the beginning of the next end step.').
card_image_name('dragon whelp'/'DDG', 'dragon whelp').
card_uid('dragon whelp'/'DDG', 'DDG:Dragon Whelp:dragon whelp').
card_rarity('dragon whelp'/'DDG', 'Uncommon').
card_artist('dragon whelp'/'DDG', 'Steven Belledin').
card_number('dragon whelp'/'DDG', '54').
card_multiverse_id('dragon whelp'/'DDG', '243472').

card_in_set('dragon\'s claw', 'DDG').
card_original_type('dragon\'s claw'/'DDG', 'Artifact').
card_original_text('dragon\'s claw'/'DDG', 'Whenever a player casts a red spell, you may gain 1 life.').
card_image_name('dragon\'s claw'/'DDG', 'dragon\'s claw').
card_uid('dragon\'s claw'/'DDG', 'DDG:Dragon\'s Claw:dragon\'s claw').
card_rarity('dragon\'s claw'/'DDG', 'Uncommon').
card_artist('dragon\'s claw'/'DDG', 'Alan Pollack').
card_number('dragon\'s claw'/'DDG', '63').
card_flavor_text('dragon\'s claw'/'DDG', 'Though no longer attached to the hand, it still holds its adversary in its grasp.').
card_multiverse_id('dragon\'s claw'/'DDG', '243481').

card_in_set('dragonspeaker shaman', 'DDG').
card_original_type('dragonspeaker shaman'/'DDG', 'Creature — Human Barbarian Shaman').
card_original_text('dragonspeaker shaman'/'DDG', 'Dragon spells you cast cost {2} less to cast.').
card_image_name('dragonspeaker shaman'/'DDG', 'dragonspeaker shaman').
card_uid('dragonspeaker shaman'/'DDG', 'DDG:Dragonspeaker Shaman:dragonspeaker shaman').
card_rarity('dragonspeaker shaman'/'DDG', 'Uncommon').
card_artist('dragonspeaker shaman'/'DDG', 'Kev Walker').
card_number('dragonspeaker shaman'/'DDG', '53').
card_flavor_text('dragonspeaker shaman'/'DDG', '\"We speak the dragons\' language of flame and rage. They speak our language of fury and honor. Together we shall weave a tale of destruction without equal.\"').
card_multiverse_id('dragonspeaker shaman'/'DDG', '243471').

card_in_set('edge of autumn', 'DDG').
card_original_type('edge of autumn'/'DDG', 'Sorcery').
card_original_text('edge of autumn'/'DDG', 'If you control four or fewer lands, search your library for a basic land card, put it onto the battlefield tapped, then shuffle your library.\nCycling—Sacrifice a land. (Sacrifice a land, Discard this card: Draw a card.)').
card_image_name('edge of autumn'/'DDG', 'edge of autumn').
card_uid('edge of autumn'/'DDG', 'DDG:Edge of Autumn:edge of autumn').
card_rarity('edge of autumn'/'DDG', 'Common').
card_artist('edge of autumn'/'DDG', 'Jean-Sébastien Rossbach').
card_number('edge of autumn'/'DDG', '25').
card_flavor_text('edge of autumn'/'DDG', '\"At harvest are seeds of next year sown.\"').
card_multiverse_id('edge of autumn'/'DDG', '243442').

card_in_set('fiery fall', 'DDG').
card_original_type('fiery fall'/'DDG', 'Instant').
card_original_text('fiery fall'/'DDG', 'Fiery Fall deals 5 damage to target creature.\nBasic landcycling {1}{R} ({1}{R}, Discard this card: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('fiery fall'/'DDG', 'fiery fall').
card_uid('fiery fall'/'DDG', 'DDG:Fiery Fall:fiery fall').
card_rarity('fiery fall'/'DDG', 'Common').
card_artist('fiery fall'/'DDG', 'Daarken').
card_number('fiery fall'/'DDG', '76').
card_flavor_text('fiery fall'/'DDG', 'Jund feasts on the unprepared.').
card_multiverse_id('fiery fall'/'DDG', '243493').

card_in_set('fire-belly changeling', 'DDG').
card_original_type('fire-belly changeling'/'DDG', 'Creature — Shapeshifter').
card_original_text('fire-belly changeling'/'DDG', 'Changeling (This card is every creature type at all times.)\n{R}: Fire-Belly Changeling gets +1/+0 until end of turn. Activate this ability no more than twice each turn.').
card_image_name('fire-belly changeling'/'DDG', 'fire-belly changeling').
card_uid('fire-belly changeling'/'DDG', 'DDG:Fire-Belly Changeling:fire-belly changeling').
card_rarity('fire-belly changeling'/'DDG', 'Common').
card_artist('fire-belly changeling'/'DDG', 'Randy Gallegos').
card_number('fire-belly changeling'/'DDG', '51').
card_flavor_text('fire-belly changeling'/'DDG', '\"My ears say it hisses. My fingers say it burns.\"\n—Auntie Wort').
card_multiverse_id('fire-belly changeling'/'DDG', '243469').

card_in_set('forest', 'DDG').
card_original_type('forest'/'DDG', 'Basic Land — Forest').
card_original_text('forest'/'DDG', 'G').
card_image_name('forest'/'DDG', 'forest1').
card_uid('forest'/'DDG', 'DDG:Forest:forest1').
card_rarity('forest'/'DDG', 'Basic Land').
card_artist('forest'/'DDG', 'Scott Bailey').
card_number('forest'/'DDG', '43').
card_multiverse_id('forest'/'DDG', '243460').

card_in_set('forest', 'DDG').
card_original_type('forest'/'DDG', 'Basic Land — Forest').
card_original_text('forest'/'DDG', 'G').
card_image_name('forest'/'DDG', 'forest2').
card_uid('forest'/'DDG', 'DDG:Forest:forest2').
card_rarity('forest'/'DDG', 'Basic Land').
card_artist('forest'/'DDG', 'Mike Ploog').
card_number('forest'/'DDG', '44').
card_multiverse_id('forest'/'DDG', '243461').

card_in_set('forest', 'DDG').
card_original_type('forest'/'DDG', 'Basic Land — Forest').
card_original_text('forest'/'DDG', 'G').
card_image_name('forest'/'DDG', 'forest3').
card_uid('forest'/'DDG', 'DDG:Forest:forest3').
card_rarity('forest'/'DDG', 'Basic Land').
card_artist('forest'/'DDG', 'Larry Elmore').
card_number('forest'/'DDG', '45').
card_multiverse_id('forest'/'DDG', '243462').

card_in_set('forest', 'DDG').
card_original_type('forest'/'DDG', 'Basic Land — Forest').
card_original_text('forest'/'DDG', 'G').
card_image_name('forest'/'DDG', 'forest4').
card_uid('forest'/'DDG', 'DDG:Forest:forest4').
card_rarity('forest'/'DDG', 'Basic Land').
card_artist('forest'/'DDG', 'Michael Komarck').
card_number('forest'/'DDG', '46').
card_multiverse_id('forest'/'DDG', '243463').

card_in_set('ghostfire', 'DDG').
card_original_type('ghostfire'/'DDG', 'Instant').
card_original_text('ghostfire'/'DDG', 'Ghostfire is colorless.\nGhostfire deals 3 damage to target creature or player.').
card_image_name('ghostfire'/'DDG', 'ghostfire').
card_uid('ghostfire'/'DDG', 'DDG:Ghostfire:ghostfire').
card_rarity('ghostfire'/'DDG', 'Common').
card_artist('ghostfire'/'DDG', 'Cyril Van Der Haegen').
card_number('ghostfire'/'DDG', '69').
card_flavor_text('ghostfire'/'DDG', 'Only those gifted with the eye of Ugin, the spirit dragon, can see his fiery breath.').
card_multiverse_id('ghostfire'/'DDG', '243486').

card_in_set('grasslands', 'DDG').
card_original_type('grasslands'/'DDG', 'Land').
card_original_text('grasslands'/'DDG', 'Grasslands enters the battlefield tapped.\n{T}, Sacrifice Grasslands: Search your library for a Forest or Plains card and put it onto the battlefield. Then shuffle your library.').
card_image_name('grasslands'/'DDG', 'grasslands').
card_uid('grasslands'/'DDG', 'DDG:Grasslands:grasslands').
card_rarity('grasslands'/'DDG', 'Uncommon').
card_artist('grasslands'/'DDG', 'John Avon').
card_number('grasslands'/'DDG', '35').
card_multiverse_id('grasslands'/'DDG', '243452').

card_in_set('griffin guide', 'DDG').
card_original_type('griffin guide'/'DDG', 'Enchantment — Aura').
card_original_text('griffin guide'/'DDG', 'Enchant creature\nEnchanted creature gets +2/+2 and has flying.\nWhen enchanted creature is put into a graveyard, put a 2/2 white Griffin creature token with flying onto the battlefield.').
card_image_name('griffin guide'/'DDG', 'griffin guide').
card_uid('griffin guide'/'DDG', 'DDG:Griffin Guide:griffin guide').
card_rarity('griffin guide'/'DDG', 'Uncommon').
card_artist('griffin guide'/'DDG', 'Jim Nelson').
card_number('griffin guide'/'DDG', '33').
card_multiverse_id('griffin guide'/'DDG', '243450').

card_in_set('harm\'s way', 'DDG').
card_original_type('harm\'s way'/'DDG', 'Instant').
card_original_text('harm\'s way'/'DDG', 'The next 2 damage that a source of your choice would deal to you and/or permanents you control this turn is dealt to target creature or player instead.').
card_image_name('harm\'s way'/'DDG', 'harm\'s way').
card_uid('harm\'s way'/'DDG', 'DDG:Harm\'s Way:harm\'s way').
card_rarity('harm\'s way'/'DDG', 'Uncommon').
card_artist('harm\'s way'/'DDG', 'Dan Scott').
card_number('harm\'s way'/'DDG', '23').
card_flavor_text('harm\'s way'/'DDG', 'Even when fate is already written, there\'s always time to change the names.').
card_multiverse_id('harm\'s way'/'DDG', '243440').

card_in_set('henge guardian', 'DDG').
card_original_type('henge guardian'/'DDG', 'Artifact Creature — Dragon Wurm').
card_original_text('henge guardian'/'DDG', '{2}: Henge Guardian gains trample until end of turn.').
card_image_name('henge guardian'/'DDG', 'henge guardian').
card_uid('henge guardian'/'DDG', 'DDG:Henge Guardian:henge guardian').
card_rarity('henge guardian'/'DDG', 'Uncommon').
card_artist('henge guardian'/'DDG', 'Chippy').
card_number('henge guardian'/'DDG', '55').
card_flavor_text('henge guardian'/'DDG', 'Like so many Thran relics, the wurm engine kept operating long after its creators were gone.').
card_multiverse_id('henge guardian'/'DDG', '243473').

card_in_set('heroes\' reunion', 'DDG').
card_original_type('heroes\' reunion'/'DDG', 'Instant').
card_original_text('heroes\' reunion'/'DDG', 'Target player gains 7 life.').
card_image_name('heroes\' reunion'/'DDG', 'heroes\' reunion').
card_uid('heroes\' reunion'/'DDG', 'DDG:Heroes\' Reunion:heroes\' reunion').
card_rarity('heroes\' reunion'/'DDG', 'Uncommon').
card_artist('heroes\' reunion'/'DDG', 'Terese Nielsen').
card_number('heroes\' reunion'/'DDG', '29').
card_flavor_text('heroes\' reunion'/'DDG', '\"You helped save my people from a Phyrexian fate. Did you think I wouldn\'t return the favor?\"\n—Eladamri, to Gerrard').
card_multiverse_id('heroes\' reunion'/'DDG', '243446').

card_in_set('jaws of stone', 'DDG').
card_original_type('jaws of stone'/'DDG', 'Sorcery').
card_original_text('jaws of stone'/'DDG', 'Jaws of Stone deals X damage divided as you choose among any number of target creatures and/or players, where X is the number of Mountains you control as you cast Jaws of Stone.').
card_image_name('jaws of stone'/'DDG', 'jaws of stone').
card_uid('jaws of stone'/'DDG', 'DDG:Jaws of Stone:jaws of stone').
card_rarity('jaws of stone'/'DDG', 'Uncommon').
card_artist('jaws of stone'/'DDG', 'Zoltan Boros & Gabor Szikszai').
card_number('jaws of stone'/'DDG', '77').
card_flavor_text('jaws of stone'/'DDG', 'When giants find their mountain homes infested, they have a whole range of solutions.').
card_multiverse_id('jaws of stone'/'DDG', '243494').

card_in_set('juniper order ranger', 'DDG').
card_original_type('juniper order ranger'/'DDG', 'Creature — Human Knight').
card_original_text('juniper order ranger'/'DDG', 'Whenever another creature enters the battlefield under your control, put a +1/+1 counter on that creature and a +1/+1 counter on Juniper Order Ranger.').
card_image_name('juniper order ranger'/'DDG', 'juniper order ranger').
card_uid('juniper order ranger'/'DDG', 'DDG:Juniper Order Ranger:juniper order ranger').
card_rarity('juniper order ranger'/'DDG', 'Uncommon').
card_artist('juniper order ranger'/'DDG', 'Greg Hildebrandt').
card_number('juniper order ranger'/'DDG', '21').
card_flavor_text('juniper order ranger'/'DDG', 'To protect the alliance between Kjeldor and Yavimaya, they trained in the ways of both.').
card_multiverse_id('juniper order ranger'/'DDG', '243438').

card_in_set('kabira vindicator', 'DDG').
card_original_type('kabira vindicator'/'DDG', 'Creature — Human Knight').
card_original_text('kabira vindicator'/'DDG', 'Level up {2}{W} ({2}{W}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 2-4\n3/6\nOther creatures you control get +1/+1.\nLEVEL 5+\n4/8\nOther creatures you control get +2/+2.').
card_image_name('kabira vindicator'/'DDG', 'kabira vindicator').
card_uid('kabira vindicator'/'DDG', 'DDG:Kabira Vindicator:kabira vindicator').
card_rarity('kabira vindicator'/'DDG', 'Uncommon').
card_artist('kabira vindicator'/'DDG', 'Steven Belledin').
card_number('kabira vindicator'/'DDG', '16').
card_multiverse_id('kabira vindicator'/'DDG', '243433').

card_in_set('kilnmouth dragon', 'DDG').
card_original_type('kilnmouth dragon'/'DDG', 'Creature — Dragon').
card_original_text('kilnmouth dragon'/'DDG', 'Amplify 3 (As this creature enters the battlefield, put three +1/+1 counters on it for each Dragon card you reveal in your hand.)\nFlying\n{T}: Kilnmouth Dragon deals damage equal to the number of +1/+1 counters on it to target creature or player.').
card_image_name('kilnmouth dragon'/'DDG', 'kilnmouth dragon').
card_uid('kilnmouth dragon'/'DDG', 'DDG:Kilnmouth Dragon:kilnmouth dragon').
card_rarity('kilnmouth dragon'/'DDG', 'Rare').
card_artist('kilnmouth dragon'/'DDG', 'Carl Critchlow').
card_number('kilnmouth dragon'/'DDG', '59').
card_multiverse_id('kilnmouth dragon'/'DDG', '243477').

card_in_set('kinsbaile cavalier', 'DDG').
card_original_type('kinsbaile cavalier'/'DDG', 'Creature — Kithkin Knight').
card_original_text('kinsbaile cavalier'/'DDG', 'Knight creatures you control have double strike.').
card_image_name('kinsbaile cavalier'/'DDG', 'kinsbaile cavalier').
card_uid('kinsbaile cavalier'/'DDG', 'DDG:Kinsbaile Cavalier:kinsbaile cavalier').
card_rarity('kinsbaile cavalier'/'DDG', 'Rare').
card_artist('kinsbaile cavalier'/'DDG', 'Wayne Reynolds').
card_number('kinsbaile cavalier'/'DDG', '17').
card_flavor_text('kinsbaile cavalier'/'DDG', 'Ambidexterity is common among kithkin. The thoughtweft links the minds of the left- and right-handed, giving each the knack of the other.').
card_multiverse_id('kinsbaile cavalier'/'DDG', '243434').

card_in_set('knight exemplar', 'DDG').
card_original_type('knight exemplar'/'DDG', 'Creature — Human Knight').
card_original_text('knight exemplar'/'DDG', 'First strike\nOther Knight creatures you control get +1/+1 and are indestructible.').
card_image_name('knight exemplar'/'DDG', 'knight exemplar').
card_uid('knight exemplar'/'DDG', 'DDG:Knight Exemplar:knight exemplar').
card_rarity('knight exemplar'/'DDG', 'Rare').
card_artist('knight exemplar'/'DDG', 'Jason Chan').
card_number('knight exemplar'/'DDG', '14').
card_flavor_text('knight exemplar'/'DDG', '\"If you think you are brave enough to walk the path of honor, follow me into the dragon\'s den.\"').
card_multiverse_id('knight exemplar'/'DDG', '243431').

card_in_set('knight of cliffhaven', 'DDG').
card_original_type('knight of cliffhaven'/'DDG', 'Creature — Kor Knight').
card_original_text('knight of cliffhaven'/'DDG', 'Level up {3} ({3}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-3\n2/3\nFlying\nLEVEL 4+\n4/4\nFlying, vigilance').
card_image_name('knight of cliffhaven'/'DDG', 'knight of cliffhaven').
card_uid('knight of cliffhaven'/'DDG', 'DDG:Knight of Cliffhaven:knight of cliffhaven').
card_rarity('knight of cliffhaven'/'DDG', 'Common').
card_artist('knight of cliffhaven'/'DDG', 'Matt Cavotta').
card_number('knight of cliffhaven'/'DDG', '4').
card_multiverse_id('knight of cliffhaven'/'DDG', '243421').

card_in_set('knight of meadowgrain', 'DDG').
card_original_type('knight of meadowgrain'/'DDG', 'Creature — Kithkin Knight').
card_original_text('knight of meadowgrain'/'DDG', 'First strike, lifelink').
card_image_name('knight of meadowgrain'/'DDG', 'knight of meadowgrain').
card_uid('knight of meadowgrain'/'DDG', 'DDG:Knight of Meadowgrain:knight of meadowgrain').
card_rarity('knight of meadowgrain'/'DDG', 'Uncommon').
card_artist('knight of meadowgrain'/'DDG', 'Larry MacDougall').
card_number('knight of meadowgrain'/'DDG', '5').
card_flavor_text('knight of meadowgrain'/'DDG', '\"By tradition, we don\'t speak for two days after battle. If our deeds won\'t speak for themselves, what else could be said?\"').
card_multiverse_id('knight of meadowgrain'/'DDG', '243422').

card_in_set('knight of the reliquary', 'DDG').
card_original_type('knight of the reliquary'/'DDG', 'Creature — Human Knight').
card_original_text('knight of the reliquary'/'DDG', 'Knight of the Reliquary gets +1/+1 for each land card in your graveyard.\n{T}, Sacrifice a Forest or Plains: Search your library for a land card, put it onto the battlefield, then shuffle your library.').
card_image_name('knight of the reliquary'/'DDG', 'knight of the reliquary').
card_uid('knight of the reliquary'/'DDG', 'DDG:Knight of the Reliquary:knight of the reliquary').
card_rarity('knight of the reliquary'/'DDG', 'Mythic Rare').
card_artist('knight of the reliquary'/'DDG', 'Wayne Reynolds').
card_number('knight of the reliquary'/'DDG', '1').
card_multiverse_id('knight of the reliquary'/'DDG', '243416').

card_in_set('knight of the white orchid', 'DDG').
card_original_type('knight of the white orchid'/'DDG', 'Creature — Human Knight').
card_original_text('knight of the white orchid'/'DDG', 'First strike\nWhen Knight of the White Orchid enters the battlefield, if an opponent controls more lands than you, you may search your library for a Plains card, put it onto the battlefield, then shuffle your library.').
card_image_name('knight of the white orchid'/'DDG', 'knight of the white orchid').
card_uid('knight of the white orchid'/'DDG', 'DDG:Knight of the White Orchid:knight of the white orchid').
card_rarity('knight of the white orchid'/'DDG', 'Rare').
card_artist('knight of the white orchid'/'DDG', 'Mark Zug').
card_number('knight of the white orchid'/'DDG', '6').
card_flavor_text('knight of the white orchid'/'DDG', 'Both guide and guard on open plain.').
card_multiverse_id('knight of the white orchid'/'DDG', '243423').

card_in_set('knotvine paladin', 'DDG').
card_original_type('knotvine paladin'/'DDG', 'Creature — Human Knight').
card_original_text('knotvine paladin'/'DDG', 'Whenever Knotvine Paladin attacks, it gets +1/+1 until end of turn for each untapped creature you control.').
card_image_name('knotvine paladin'/'DDG', 'knotvine paladin').
card_uid('knotvine paladin'/'DDG', 'DDG:Knotvine Paladin:knotvine paladin').
card_rarity('knotvine paladin'/'DDG', 'Rare').
card_artist('knotvine paladin'/'DDG', 'Matt Stewart').
card_number('knotvine paladin'/'DDG', '10').
card_flavor_text('knotvine paladin'/'DDG', '\"He is part of a new breed of guardian, one who embraces the best of both his world and ours.\"\n—Mayael the Anima').
card_multiverse_id('knotvine paladin'/'DDG', '243427').

card_in_set('leonin skyhunter', 'DDG').
card_original_type('leonin skyhunter'/'DDG', 'Creature — Cat Knight').
card_original_text('leonin skyhunter'/'DDG', 'Flying').
card_image_name('leonin skyhunter'/'DDG', 'leonin skyhunter').
card_uid('leonin skyhunter'/'DDG', 'DDG:Leonin Skyhunter:leonin skyhunter').
card_rarity('leonin skyhunter'/'DDG', 'Uncommon').
card_artist('leonin skyhunter'/'DDG', 'Kev Walker').
card_number('leonin skyhunter'/'DDG', '7').
card_flavor_text('leonin skyhunter'/'DDG', 'The skyhunters were born when the first leonin gazed at the heavens and wished to hunt the birds overhead.').
card_multiverse_id('leonin skyhunter'/'DDG', '243424').

card_in_set('lionheart maverick', 'DDG').
card_original_type('lionheart maverick'/'DDG', 'Creature — Human Knight').
card_original_text('lionheart maverick'/'DDG', 'Vigilance\n{4}{W}: Lionheart Maverick gets +1/+2 until end of turn.').
card_image_name('lionheart maverick'/'DDG', 'lionheart maverick').
card_uid('lionheart maverick'/'DDG', 'DDG:Lionheart Maverick:lionheart maverick').
card_rarity('lionheart maverick'/'DDG', 'Common').
card_artist('lionheart maverick'/'DDG', 'Hideaki Takamura').
card_number('lionheart maverick'/'DDG', '3').
card_flavor_text('lionheart maverick'/'DDG', '\"Your signet is no symbol of power. It marks only your need for numbers to aid you. What do you do, guild-rat, now that you face my blade alone?\"').
card_multiverse_id('lionheart maverick'/'DDG', '243420').

card_in_set('loxodon warhammer', 'DDG').
card_original_type('loxodon warhammer'/'DDG', 'Artifact — Equipment').
card_original_text('loxodon warhammer'/'DDG', 'Equipped creature gets +3/+0 and has trample and lifelink.\nEquip {3}').
card_image_name('loxodon warhammer'/'DDG', 'loxodon warhammer').
card_uid('loxodon warhammer'/'DDG', 'DDG:Loxodon Warhammer:loxodon warhammer').
card_rarity('loxodon warhammer'/'DDG', 'Rare').
card_artist('loxodon warhammer'/'DDG', 'Terese Nielsen').
card_number('loxodon warhammer'/'DDG', '31').
card_flavor_text('loxodon warhammer'/'DDG', '\"Tusks to splinter skulls. Steel to shatter scale.\"\n—Warhammer inscription').
card_multiverse_id('loxodon warhammer'/'DDG', '243448').

card_in_set('mighty leap', 'DDG').
card_original_type('mighty leap'/'DDG', 'Instant').
card_original_text('mighty leap'/'DDG', 'Target creature gets +2/+2 and gains flying until end of turn.').
card_image_name('mighty leap'/'DDG', 'mighty leap').
card_uid('mighty leap'/'DDG', 'DDG:Mighty Leap:mighty leap').
card_rarity('mighty leap'/'DDG', 'Common').
card_artist('mighty leap'/'DDG', 'rk post').
card_number('mighty leap'/'DDG', '26').
card_flavor_text('mighty leap'/'DDG', '\"The southern fortress taken by invaders? Heh, sure . . . when elephants fly.\"\n—Brezard Skeinbow, captain of the guard').
card_multiverse_id('mighty leap'/'DDG', '243443').

card_in_set('mordant dragon', 'DDG').
card_original_type('mordant dragon'/'DDG', 'Creature — Dragon').
card_original_text('mordant dragon'/'DDG', 'Flying\n{1}{R}: Mordant Dragon gets +1/+0 until end of turn.\nWhenever Mordant Dragon deals combat damage to a player, you may have it deal that much damage to target creature that player controls.').
card_image_name('mordant dragon'/'DDG', 'mordant dragon').
card_uid('mordant dragon'/'DDG', 'DDG:Mordant Dragon:mordant dragon').
card_rarity('mordant dragon'/'DDG', 'Rare').
card_artist('mordant dragon'/'DDG', 'Scott Chou').
card_number('mordant dragon'/'DDG', '58').
card_multiverse_id('mordant dragon'/'DDG', '243476').

card_in_set('mountain', 'DDG').
card_original_type('mountain'/'DDG', 'Basic Land — Mountain').
card_original_text('mountain'/'DDG', 'R').
card_image_name('mountain'/'DDG', 'mountain1').
card_uid('mountain'/'DDG', 'DDG:Mountain:mountain1').
card_rarity('mountain'/'DDG', 'Basic Land').
card_artist('mountain'/'DDG', 'Aleksi Briclot').
card_number('mountain'/'DDG', '78').
card_multiverse_id('mountain'/'DDG', '243495').

card_in_set('mountain', 'DDG').
card_original_type('mountain'/'DDG', 'Basic Land — Mountain').
card_original_text('mountain'/'DDG', 'R').
card_image_name('mountain'/'DDG', 'mountain2').
card_uid('mountain'/'DDG', 'DDG:Mountain:mountain2').
card_rarity('mountain'/'DDG', 'Basic Land').
card_artist('mountain'/'DDG', 'Aleksi Briclot').
card_number('mountain'/'DDG', '79').
card_multiverse_id('mountain'/'DDG', '243496').

card_in_set('mountain', 'DDG').
card_original_type('mountain'/'DDG', 'Basic Land — Mountain').
card_original_text('mountain'/'DDG', 'R').
card_image_name('mountain'/'DDG', 'mountain3').
card_uid('mountain'/'DDG', 'DDG:Mountain:mountain3').
card_rarity('mountain'/'DDG', 'Basic Land').
card_artist('mountain'/'DDG', 'Glen Angus').
card_number('mountain'/'DDG', '80').
card_multiverse_id('mountain'/'DDG', '243497').

card_in_set('mountain', 'DDG').
card_original_type('mountain'/'DDG', 'Basic Land — Mountain').
card_original_text('mountain'/'DDG', 'R').
card_image_name('mountain'/'DDG', 'mountain4').
card_uid('mountain'/'DDG', 'DDG:Mountain:mountain4').
card_rarity('mountain'/'DDG', 'Basic Land').
card_artist('mountain'/'DDG', 'Matt Cavotta').
card_number('mountain'/'DDG', '81').
card_multiverse_id('mountain'/'DDG', '243498').

card_in_set('mudbutton torchrunner', 'DDG').
card_original_type('mudbutton torchrunner'/'DDG', 'Creature — Goblin Warrior').
card_original_text('mudbutton torchrunner'/'DDG', 'When Mudbutton Torchrunner is put into a graveyard from the battlefield, it deals 3 damage to target creature or player.').
card_image_name('mudbutton torchrunner'/'DDG', 'mudbutton torchrunner').
card_uid('mudbutton torchrunner'/'DDG', 'DDG:Mudbutton Torchrunner:mudbutton torchrunner').
card_rarity('mudbutton torchrunner'/'DDG', 'Common').
card_artist('mudbutton torchrunner'/'DDG', 'Steve Ellis').
card_number('mudbutton torchrunner'/'DDG', '52').
card_flavor_text('mudbutton torchrunner'/'DDG', 'The oil sloshes against his skull as he nears his destination: the Frogtosser Games and the lighting of the Flaming Boggart.').
card_multiverse_id('mudbutton torchrunner'/'DDG', '243470').

card_in_set('oblivion ring', 'DDG').
card_original_type('oblivion ring'/'DDG', 'Enchantment').
card_original_text('oblivion ring'/'DDG', 'When Oblivion Ring enters the battlefield, exile another target nonland permanent.\nWhen Oblivion Ring leaves the battlefield, return the exiled card to the battlefield under its owner\'s control.').
card_image_name('oblivion ring'/'DDG', 'oblivion ring').
card_uid('oblivion ring'/'DDG', 'DDG:Oblivion Ring:oblivion ring').
card_rarity('oblivion ring'/'DDG', 'Common').
card_artist('oblivion ring'/'DDG', 'Chuck Lukacs').
card_number('oblivion ring'/'DDG', '34').
card_multiverse_id('oblivion ring'/'DDG', '243451').

card_in_set('paladin of prahv', 'DDG').
card_original_type('paladin of prahv'/'DDG', 'Creature — Human Knight').
card_original_text('paladin of prahv'/'DDG', 'Whenever Paladin of Prahv deals damage, you gain that much life.\nForecast — {1}{W}, Reveal Paladin of Prahv from your hand: Whenever target creature deals damage this turn, you gain that much life. (Activate this ability only during your upkeep and only once each turn.)').
card_image_name('paladin of prahv'/'DDG', 'paladin of prahv').
card_uid('paladin of prahv'/'DDG', 'DDG:Paladin of Prahv:paladin of prahv').
card_rarity('paladin of prahv'/'DDG', 'Uncommon').
card_artist('paladin of prahv'/'DDG', 'William Simpson').
card_number('paladin of prahv'/'DDG', '22').
card_multiverse_id('paladin of prahv'/'DDG', '243439').

card_in_set('plains', 'DDG').
card_original_type('plains'/'DDG', 'Basic Land — Plains').
card_original_text('plains'/'DDG', 'W').
card_image_name('plains'/'DDG', 'plains1').
card_uid('plains'/'DDG', 'DDG:Plains:plains1').
card_rarity('plains'/'DDG', 'Basic Land').
card_artist('plains'/'DDG', 'Scott Bailey').
card_number('plains'/'DDG', '39').
card_multiverse_id('plains'/'DDG', '243456').

card_in_set('plains', 'DDG').
card_original_type('plains'/'DDG', 'Basic Land — Plains').
card_original_text('plains'/'DDG', 'W').
card_image_name('plains'/'DDG', 'plains2').
card_uid('plains'/'DDG', 'DDG:Plains:plains2').
card_rarity('plains'/'DDG', 'Basic Land').
card_artist('plains'/'DDG', 'Scott Bailey').
card_number('plains'/'DDG', '40').
card_multiverse_id('plains'/'DDG', '243457').

card_in_set('plains', 'DDG').
card_original_type('plains'/'DDG', 'Basic Land — Plains').
card_original_text('plains'/'DDG', 'W').
card_image_name('plains'/'DDG', 'plains3').
card_uid('plains'/'DDG', 'DDG:Plains:plains3').
card_rarity('plains'/'DDG', 'Basic Land').
card_artist('plains'/'DDG', 'Michael Komarck').
card_number('plains'/'DDG', '41').
card_multiverse_id('plains'/'DDG', '243458').

card_in_set('plains', 'DDG').
card_original_type('plains'/'DDG', 'Basic Land — Plains').
card_original_text('plains'/'DDG', 'W').
card_image_name('plains'/'DDG', 'plains4').
card_uid('plains'/'DDG', 'DDG:Plains:plains4').
card_rarity('plains'/'DDG', 'Basic Land').
card_artist('plains'/'DDG', 'Michael Komarck').
card_number('plains'/'DDG', '42').
card_multiverse_id('plains'/'DDG', '243459').

card_in_set('plover knights', 'DDG').
card_original_type('plover knights'/'DDG', 'Creature — Kithkin Knight').
card_original_text('plover knights'/'DDG', 'Flying, first strike').
card_image_name('plover knights'/'DDG', 'plover knights').
card_uid('plover knights'/'DDG', 'DDG:Plover Knights:plover knights').
card_rarity('plover knights'/'DDG', 'Common').
card_artist('plover knights'/'DDG', 'Quinton Hoover').
card_number('plover knights'/'DDG', '20').
card_flavor_text('plover knights'/'DDG', 'The knights are a major attraction at every Lammastide festival. Teams of riders perform daring feats of flight to the delight of all below.').
card_multiverse_id('plover knights'/'DDG', '243437').

card_in_set('punishing fire', 'DDG').
card_original_type('punishing fire'/'DDG', 'Instant').
card_original_text('punishing fire'/'DDG', 'Punishing Fire deals 2 damage to target creature or player.\nWhenever an opponent gains life, you may pay {R}. If you do, return Punishing Fire from your graveyard to your hand.').
card_image_name('punishing fire'/'DDG', 'punishing fire').
card_uid('punishing fire'/'DDG', 'DDG:Punishing Fire:punishing fire').
card_rarity('punishing fire'/'DDG', 'Uncommon').
card_artist('punishing fire'/'DDG', 'Greg Staples').
card_number('punishing fire'/'DDG', '66').
card_multiverse_id('punishing fire'/'DDG', '243483').

card_in_set('reciprocate', 'DDG').
card_original_type('reciprocate'/'DDG', 'Instant').
card_original_text('reciprocate'/'DDG', 'Exile target creature that dealt damage to you this turn.').
card_image_name('reciprocate'/'DDG', 'reciprocate').
card_uid('reciprocate'/'DDG', 'DDG:Reciprocate:reciprocate').
card_rarity('reciprocate'/'DDG', 'Uncommon').
card_artist('reciprocate'/'DDG', 'Pat Lee').
card_number('reciprocate'/'DDG', '24').
card_flavor_text('reciprocate'/'DDG', '\"Just as the noble soul calls virtue to itself, the evil soul summons harm.\"\n—Teachings of Eight-and-a-Half-Tails').
card_multiverse_id('reciprocate'/'DDG', '243441').

card_in_set('reprisal', 'DDG').
card_original_type('reprisal'/'DDG', 'Instant').
card_original_text('reprisal'/'DDG', 'Destroy target creature with power 4 or greater. It can\'t be regenerated.').
card_image_name('reprisal'/'DDG', 'reprisal').
card_uid('reprisal'/'DDG', 'DDG:Reprisal:reprisal').
card_rarity('reprisal'/'DDG', 'Uncommon').
card_artist('reprisal'/'DDG', 'Ciruelo').
card_number('reprisal'/'DDG', '27').
card_flavor_text('reprisal'/'DDG', '\"Fear this no more!\"').
card_multiverse_id('reprisal'/'DDG', '243444').

card_in_set('seething song', 'DDG').
card_original_type('seething song'/'DDG', 'Instant').
card_original_text('seething song'/'DDG', 'Add {R}{R}{R}{R}{R} to your mana pool.').
card_image_name('seething song'/'DDG', 'seething song').
card_uid('seething song'/'DDG', 'DDG:Seething Song:seething song').
card_rarity('seething song'/'DDG', 'Common').
card_artist('seething song'/'DDG', 'Jaime Jones').
card_number('seething song'/'DDG', '70').
card_flavor_text('seething song'/'DDG', 'If your flesh can withstand the flame, the spirit of the dragon will burn within your soul.').
card_multiverse_id('seething song'/'DDG', '243487').

card_in_set('seismic strike', 'DDG').
card_original_type('seismic strike'/'DDG', 'Instant').
card_original_text('seismic strike'/'DDG', 'Seismic Strike deals damage to target creature equal to the number of Mountains you control.').
card_image_name('seismic strike'/'DDG', 'seismic strike').
card_uid('seismic strike'/'DDG', 'DDG:Seismic Strike:seismic strike').
card_rarity('seismic strike'/'DDG', 'Common').
card_artist('seismic strike'/'DDG', 'Christopher Moeller').
card_number('seismic strike'/'DDG', '71').
card_flavor_text('seismic strike'/'DDG', '\"Life up here is simple. Adapt to the ways of the mountains and they will reward you. Fight them and they will end you.\"\n—Kezim, prodigal pyromancer').
card_multiverse_id('seismic strike'/'DDG', '243488').

card_in_set('sejiri steppe', 'DDG').
card_original_type('sejiri steppe'/'DDG', 'Land').
card_original_text('sejiri steppe'/'DDG', 'Sejiri Steppe enters the battlefield tapped.\nWhen Sejiri Steppe enters the battlefield, target creature you control gains protection from the color of your choice until end of turn.\n{T}: Add {W} to your mana pool.').
card_image_name('sejiri steppe'/'DDG', 'sejiri steppe').
card_uid('sejiri steppe'/'DDG', 'DDG:Sejiri Steppe:sejiri steppe').
card_rarity('sejiri steppe'/'DDG', 'Common').
card_artist('sejiri steppe'/'DDG', 'Anthony Francisco').
card_number('sejiri steppe'/'DDG', '36').
card_multiverse_id('sejiri steppe'/'DDG', '243453').

card_in_set('selesnya sanctuary', 'DDG').
card_original_type('selesnya sanctuary'/'DDG', 'Land').
card_original_text('selesnya sanctuary'/'DDG', 'Selesnya Sanctuary enters the battlefield tapped.\nWhen Selesnya Sanctuary enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {G}{W} to your mana pool.').
card_image_name('selesnya sanctuary'/'DDG', 'selesnya sanctuary').
card_uid('selesnya sanctuary'/'DDG', 'DDG:Selesnya Sanctuary:selesnya sanctuary').
card_rarity('selesnya sanctuary'/'DDG', 'Common').
card_artist('selesnya sanctuary'/'DDG', 'John Avon').
card_number('selesnya sanctuary'/'DDG', '37').
card_multiverse_id('selesnya sanctuary'/'DDG', '243454').

card_in_set('shiv\'s embrace', 'DDG').
card_original_type('shiv\'s embrace'/'DDG', 'Enchantment — Aura').
card_original_text('shiv\'s embrace'/'DDG', 'Enchant creature\nEnchanted creature gets +2/+2 and has flying.\n{R}: Enchanted creature gets +1/+0 until end of turn.').
card_image_name('shiv\'s embrace'/'DDG', 'shiv\'s embrace').
card_uid('shiv\'s embrace'/'DDG', 'DDG:Shiv\'s Embrace:shiv\'s embrace').
card_rarity('shiv\'s embrace'/'DDG', 'Uncommon').
card_artist('shiv\'s embrace'/'DDG', 'Dave Kendall').
card_number('shiv\'s embrace'/'DDG', '74').
card_multiverse_id('shiv\'s embrace'/'DDG', '243491').

card_in_set('shivan hellkite', 'DDG').
card_original_type('shivan hellkite'/'DDG', 'Creature — Dragon').
card_original_text('shivan hellkite'/'DDG', 'Flying\n{1}{R}: Shivan Hellkite deals 1 damage to target creature or player.').
card_image_name('shivan hellkite'/'DDG', 'shivan hellkite').
card_uid('shivan hellkite'/'DDG', 'DDG:Shivan Hellkite:shivan hellkite').
card_rarity('shivan hellkite'/'DDG', 'Rare').
card_artist('shivan hellkite'/'DDG', 'Kev Walker').
card_number('shivan hellkite'/'DDG', '60').
card_flavor_text('shivan hellkite'/'DDG', 'A dragon\'s scale can be carved into a mighty shield, provided you can procure a dragontooth to cut it.').
card_multiverse_id('shivan hellkite'/'DDG', '243478').

card_in_set('sigil blessing', 'DDG').
card_original_type('sigil blessing'/'DDG', 'Instant').
card_original_text('sigil blessing'/'DDG', 'Until end of turn, target creature you control gets +3/+3 and other creatures you control get +1/+1.').
card_image_name('sigil blessing'/'DDG', 'sigil blessing').
card_uid('sigil blessing'/'DDG', 'DDG:Sigil Blessing:sigil blessing').
card_rarity('sigil blessing'/'DDG', 'Common').
card_artist('sigil blessing'/'DDG', 'Matt Stewart').
card_number('sigil blessing'/'DDG', '30').
card_flavor_text('sigil blessing'/'DDG', '\"For unwavering commitment and unflinching strength, the Order of the White Orchid confers its sigil. Rise, knight-captain, and do your duty.\"').
card_multiverse_id('sigil blessing'/'DDG', '243447').

card_in_set('silver knight', 'DDG').
card_original_type('silver knight'/'DDG', 'Creature — Human Knight').
card_original_text('silver knight'/'DDG', 'First strike, protection from red').
card_image_name('silver knight'/'DDG', 'silver knight').
card_uid('silver knight'/'DDG', 'DDG:Silver Knight:silver knight').
card_rarity('silver knight'/'DDG', 'Uncommon').
card_artist('silver knight'/'DDG', 'Steve Prescott').
card_number('silver knight'/'DDG', '8').
card_flavor_text('silver knight'/'DDG', '\"Hammer out your doubts as you forge your weapon. To slay a dragon, your resolve must be stronger than your steel.\"').
card_multiverse_id('silver knight'/'DDG', '243425').

card_in_set('skirk prospector', 'DDG').
card_original_type('skirk prospector'/'DDG', 'Creature — Goblin').
card_original_text('skirk prospector'/'DDG', 'Sacrifice a Goblin: Add {R} to your mana pool.').
card_image_name('skirk prospector'/'DDG', 'skirk prospector').
card_uid('skirk prospector'/'DDG', 'DDG:Skirk Prospector:skirk prospector').
card_rarity('skirk prospector'/'DDG', 'Common').
card_artist('skirk prospector'/'DDG', 'Doug Chaffee').
card_number('skirk prospector'/'DDG', '49').
card_flavor_text('skirk prospector'/'DDG', '\"I like goblins. They make funny little popping sounds when they die.\"\n—Braids, dementia summoner').
card_multiverse_id('skirk prospector'/'DDG', '243465').

card_in_set('skyhunter patrol', 'DDG').
card_original_type('skyhunter patrol'/'DDG', 'Creature — Cat Knight').
card_original_text('skyhunter patrol'/'DDG', 'Flying, first strike').
card_image_name('skyhunter patrol'/'DDG', 'skyhunter patrol').
card_uid('skyhunter patrol'/'DDG', 'DDG:Skyhunter Patrol:skyhunter patrol').
card_rarity('skyhunter patrol'/'DDG', 'Common').
card_artist('skyhunter patrol'/'DDG', 'Matt Cavotta').
card_number('skyhunter patrol'/'DDG', '19').
card_flavor_text('skyhunter patrol'/'DDG', '\"We leonin have come to rule the plains by taking to the skies.\"\n—Raksha Golden Cub').
card_multiverse_id('skyhunter patrol'/'DDG', '243436').

card_in_set('spidersilk armor', 'DDG').
card_original_type('spidersilk armor'/'DDG', 'Enchantment').
card_original_text('spidersilk armor'/'DDG', 'Creatures you control get +0/+1 and have reach. (They can block creatures with flying.)').
card_image_name('spidersilk armor'/'DDG', 'spidersilk armor').
card_uid('spidersilk armor'/'DDG', 'DDG:Spidersilk Armor:spidersilk armor').
card_rarity('spidersilk armor'/'DDG', 'Common').
card_artist('spidersilk armor'/'DDG', 'Scott Hampton').
card_number('spidersilk armor'/'DDG', '32').
card_flavor_text('spidersilk armor'/'DDG', 'It hardly weighs anything, but it takes all day to remove.').
card_multiverse_id('spidersilk armor'/'DDG', '243449').

card_in_set('spitting earth', 'DDG').
card_original_type('spitting earth'/'DDG', 'Sorcery').
card_original_text('spitting earth'/'DDG', 'Spitting Earth deals damage to target creature equal to the number of Mountains you control.').
card_image_name('spitting earth'/'DDG', 'spitting earth').
card_uid('spitting earth'/'DDG', 'DDG:Spitting Earth:spitting earth').
card_rarity('spitting earth'/'DDG', 'Common').
card_artist('spitting earth'/'DDG', 'Michael Koelsch').
card_number('spitting earth'/'DDG', '67').
card_flavor_text('spitting earth'/'DDG', '\"Within each of you is the strength of a landslide.\"\n—Kumano, to his pupils').
card_multiverse_id('spitting earth'/'DDG', '243484').

card_in_set('steward of valeron', 'DDG').
card_original_type('steward of valeron'/'DDG', 'Creature — Human Druid Knight').
card_original_text('steward of valeron'/'DDG', 'Vigilance\n{T}: Add {G} to your mana pool.').
card_image_name('steward of valeron'/'DDG', 'steward of valeron').
card_uid('steward of valeron'/'DDG', 'DDG:Steward of Valeron:steward of valeron').
card_rarity('steward of valeron'/'DDG', 'Common').
card_artist('steward of valeron'/'DDG', 'Greg Staples').
card_number('steward of valeron'/'DDG', '11').
card_flavor_text('steward of valeron'/'DDG', 'Knight-stewards guard the Sun-Dappled Court, a grove of immense, sculptured olive trees that represent Valeron\'s twelve noble families.').
card_multiverse_id('steward of valeron'/'DDG', '243428').

card_in_set('temporary insanity', 'DDG').
card_original_type('temporary insanity'/'DDG', 'Instant').
card_original_text('temporary insanity'/'DDG', 'Untap target creature with power less than the number of cards in your graveyard and gain control of it until end of turn. That creature gains haste until end of turn.').
card_image_name('temporary insanity'/'DDG', 'temporary insanity').
card_uid('temporary insanity'/'DDG', 'DDG:Temporary Insanity:temporary insanity').
card_rarity('temporary insanity'/'DDG', 'Uncommon').
card_artist('temporary insanity'/'DDG', 'Mark Romanoski').
card_number('temporary insanity'/'DDG', '73').
card_multiverse_id('temporary insanity'/'DDG', '243490').

card_in_set('test of faith', 'DDG').
card_original_type('test of faith'/'DDG', 'Instant').
card_original_text('test of faith'/'DDG', 'Prevent the next 3 damage that would be dealt to target creature this turn, and put a +1/+1 counter on that creature for each 1 damage prevented this way.').
card_image_name('test of faith'/'DDG', 'test of faith').
card_uid('test of faith'/'DDG', 'DDG:Test of Faith:test of faith').
card_rarity('test of faith'/'DDG', 'Uncommon').
card_artist('test of faith'/'DDG', 'Vance Kovacs').
card_number('test of faith'/'DDG', '28').
card_flavor_text('test of faith'/'DDG', 'Those who survive the test bear a mark of power anyone can recognize.').
card_multiverse_id('test of faith'/'DDG', '243445').

card_in_set('thunder dragon', 'DDG').
card_original_type('thunder dragon'/'DDG', 'Creature — Dragon').
card_original_text('thunder dragon'/'DDG', 'Flying\nWhen Thunder Dragon enters the battlefield, it deals 3 damage to each creature without flying.').
card_image_name('thunder dragon'/'DDG', 'thunder dragon').
card_uid('thunder dragon'/'DDG', 'DDG:Thunder Dragon:thunder dragon').
card_rarity('thunder dragon'/'DDG', 'Rare').
card_artist('thunder dragon'/'DDG', 'Chris Rahn').
card_number('thunder dragon'/'DDG', '61').
card_flavor_text('thunder dragon'/'DDG', 'Sometimes primitive cultures attributed natural phenomena to monsters. Sometimes they were right.').
card_multiverse_id('thunder dragon'/'DDG', '243479').

card_in_set('treetop village', 'DDG').
card_original_type('treetop village'/'DDG', 'Land').
card_original_text('treetop village'/'DDG', 'Treetop Village enters the battlefield tapped.\n{T}: Add {G} to your mana pool.\n{1}{G}: Treetop Village becomes a 3/3 green Ape creature with trample until end of turn. It\'s still a land.').
card_image_name('treetop village'/'DDG', 'treetop village').
card_uid('treetop village'/'DDG', 'DDG:Treetop Village:treetop village').
card_rarity('treetop village'/'DDG', 'Uncommon').
card_artist('treetop village'/'DDG', 'Rob Alexander').
card_number('treetop village'/'DDG', '38').
card_multiverse_id('treetop village'/'DDG', '243455').

card_in_set('voracious dragon', 'DDG').
card_original_type('voracious dragon'/'DDG', 'Creature — Dragon').
card_original_text('voracious dragon'/'DDG', 'Flying\nDevour 1 (As this enters the battlefield, you may sacrifice any number of creatures. This creature enters the battlefield with that many +1/+1 counters on it.)\nWhen Voracious Dragon enters the battlefield, it deals damage to target creature or player equal to twice the number of Goblins it devoured.').
card_image_name('voracious dragon'/'DDG', 'voracious dragon').
card_uid('voracious dragon'/'DDG', 'DDG:Voracious Dragon:voracious dragon').
card_rarity('voracious dragon'/'DDG', 'Rare').
card_artist('voracious dragon'/'DDG', 'Dominick Domingo').
card_number('voracious dragon'/'DDG', '56').
card_multiverse_id('voracious dragon'/'DDG', '243474').

card_in_set('white knight', 'DDG').
card_original_type('white knight'/'DDG', 'Creature — Human Knight').
card_original_text('white knight'/'DDG', 'First strike, protection from black').
card_image_name('white knight'/'DDG', 'white knight').
card_uid('white knight'/'DDG', 'DDG:White Knight:white knight').
card_rarity('white knight'/'DDG', 'Uncommon').
card_artist('white knight'/'DDG', 'Christopher Moeller').
card_number('white knight'/'DDG', '9').
card_flavor_text('white knight'/'DDG', 'Out of the blackness and stench of the engulfing swamp emerged a shimmering figure. Only the splattered armor and ichor-stained sword hinted at the unfathomable evil the knight had just laid waste.').
card_multiverse_id('white knight'/'DDG', '243426').

card_in_set('wilt-leaf cavaliers', 'DDG').
card_original_type('wilt-leaf cavaliers'/'DDG', 'Creature — Elf Knight').
card_original_text('wilt-leaf cavaliers'/'DDG', 'Vigilance').
card_image_name('wilt-leaf cavaliers'/'DDG', 'wilt-leaf cavaliers').
card_uid('wilt-leaf cavaliers'/'DDG', 'DDG:Wilt-Leaf Cavaliers:wilt-leaf cavaliers').
card_rarity('wilt-leaf cavaliers'/'DDG', 'Uncommon').
card_artist('wilt-leaf cavaliers'/'DDG', 'Steve Prescott').
card_number('wilt-leaf cavaliers'/'DDG', '15').
card_flavor_text('wilt-leaf cavaliers'/'DDG', 'Every elf in Shadowmoor is charged from birth with a terrible duty: to strike back against the ugliness and darkness, even though they are all around and seemingly without end.').
card_multiverse_id('wilt-leaf cavaliers'/'DDG', '243432').

card_in_set('zhalfirin commander', 'DDG').
card_original_type('zhalfirin commander'/'DDG', 'Creature — Human Knight').
card_original_text('zhalfirin commander'/'DDG', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)\n{1}{W}{W}: Target Knight creature gets +1/+1 until end of turn.').
card_image_name('zhalfirin commander'/'DDG', 'zhalfirin commander').
card_uid('zhalfirin commander'/'DDG', 'DDG:Zhalfirin Commander:zhalfirin commander').
card_rarity('zhalfirin commander'/'DDG', 'Uncommon').
card_artist('zhalfirin commander'/'DDG', 'Stuart Griffin').
card_number('zhalfirin commander'/'DDG', '13').
card_flavor_text('zhalfirin commander'/'DDG', '\"Command is the act of balancing on a vine and convincing others that it is firm ground.\"\n—Sidar Jabari').
card_multiverse_id('zhalfirin commander'/'DDG', '243430').
