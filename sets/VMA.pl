% Vintage Masters

set('VMA').
set_name('VMA', 'Vintage Masters').
set_release_date('VMA', '2014-06-16').
set_border('VMA', 'black').
set_type('VMA', 'masters').

card_in_set('academy elite', 'VMA').
card_original_type('academy elite'/'VMA', 'Creature — Human Wizard').
card_original_text('academy elite'/'VMA', 'Academy Elite enters the battlefield with X +1/+1 counters on it, where X is the number of instant and sorcery cards in all graveyards.\n{2}{U}, Remove a +1/+1 counter from Academy Elite: Draw a card, then discard a card.').
card_image_name('academy elite'/'VMA', 'academy elite').
card_uid('academy elite'/'VMA', 'VMA:Academy Elite:academy elite').
card_rarity('academy elite'/'VMA', 'Rare').
card_artist('academy elite'/'VMA', 'Volkan Baga').
card_number('academy elite'/'VMA', '55').
card_multiverse_id('academy elite'/'VMA', '382835').

card_in_set('addle', 'VMA').
card_original_type('addle'/'VMA', 'Sorcery').
card_original_text('addle'/'VMA', 'Choose a color. Target player reveals his or her hand and you choose a card of that color from it. That player discards that card.').
card_image_name('addle'/'VMA', 'addle').
card_uid('addle'/'VMA', 'VMA:Addle:addle').
card_rarity('addle'/'VMA', 'Common').
card_artist('addle'/'VMA', 'Ron Spears').
card_number('addle'/'VMA', '103').
card_flavor_text('addle'/'VMA', '\"I\'ll wring out your tiny mind like a sponge.\"\n—Urborg witch').
card_multiverse_id('addle'/'VMA', '382836').

card_in_set('æther mutation', 'VMA').
card_original_type('æther mutation'/'VMA', 'Sorcery').
card_original_text('æther mutation'/'VMA', 'Return target creature to its owner\'s hand. Put X 1/1 green Saproling creature tokens onto the battlefield, where X is that creature\'s converted mana cost.').
card_image_name('æther mutation'/'VMA', 'aether mutation').
card_uid('æther mutation'/'VMA', 'VMA:Æther Mutation:aether mutation').
card_rarity('æther mutation'/'VMA', 'Uncommon').
card_artist('æther mutation'/'VMA', 'Ron Spencer').
card_number('æther mutation'/'VMA', '241').
card_multiverse_id('æther mutation'/'VMA', '382837').

card_in_set('afterlife', 'VMA').
card_original_type('afterlife'/'VMA', 'Instant').
card_original_text('afterlife'/'VMA', 'Destroy target creature. It can\'t be regenerated. Its controller puts a 1/1 white Spirit creature token with flying onto the battlefield.').
card_image_name('afterlife'/'VMA', 'afterlife').
card_uid('afterlife'/'VMA', 'VMA:Afterlife:afterlife').
card_rarity('afterlife'/'VMA', 'Common').
card_artist('afterlife'/'VMA', 'Brian Snõddy').
card_number('afterlife'/'VMA', '10').
card_multiverse_id('afterlife'/'VMA', '382838').

card_in_set('aftershock', 'VMA').
card_original_type('aftershock'/'VMA', 'Sorcery').
card_original_text('aftershock'/'VMA', 'Destroy target artifact, creature, or land. Aftershock deals 3 damage to you.').
card_image_name('aftershock'/'VMA', 'aftershock').
card_uid('aftershock'/'VMA', 'VMA:Aftershock:aftershock').
card_rarity('aftershock'/'VMA', 'Common').
card_artist('aftershock'/'VMA', 'Hannibal King').
card_number('aftershock'/'VMA', '149').
card_flavor_text('aftershock'/'VMA', '\"Every act of destruction has a repercussion.\"\n—Karn, silver golem').
card_multiverse_id('aftershock'/'VMA', '382839').

card_in_set('akroma\'s blessing', 'VMA').
card_original_type('akroma\'s blessing'/'VMA', 'Instant').
card_original_text('akroma\'s blessing'/'VMA', 'Choose a color. Creatures you control gain protection from the chosen color until end of turn.\nCycling {W} ({W}, Discard this card: Draw a card.)').
card_image_name('akroma\'s blessing'/'VMA', 'akroma\'s blessing').
card_uid('akroma\'s blessing'/'VMA', 'VMA:Akroma\'s Blessing:akroma\'s blessing').
card_rarity('akroma\'s blessing'/'VMA', 'Uncommon').
card_artist('akroma\'s blessing'/'VMA', 'Adam Rex').
card_number('akroma\'s blessing'/'VMA', '11').
card_flavor_text('akroma\'s blessing'/'VMA', 'The clerics saw her as a divine gift. She saw them only as allies in her war against Phage.').
card_multiverse_id('akroma\'s blessing'/'VMA', '382840').

card_in_set('ancestral recall', 'VMA').
card_original_type('ancestral recall'/'VMA', 'Instant').
card_original_text('ancestral recall'/'VMA', 'Target player draws three cards.').
card_image_name('ancestral recall'/'VMA', 'ancestral recall').
card_uid('ancestral recall'/'VMA', 'VMA:Ancestral Recall:ancestral recall').
card_rarity('ancestral recall'/'VMA', 'Special').
card_artist('ancestral recall'/'VMA', 'Ryan Pancoast').
card_number('ancestral recall'/'VMA', '1').
card_multiverse_id('ancestral recall'/'VMA', '382841').

card_in_set('ancient tomb', 'VMA').
card_original_type('ancient tomb'/'VMA', 'Land').
card_original_text('ancient tomb'/'VMA', '{T}: Add {2} to your mana pool. Ancient Tomb deals 2 damage to you.').
card_image_name('ancient tomb'/'VMA', 'ancient tomb').
card_uid('ancient tomb'/'VMA', 'VMA:Ancient Tomb:ancient tomb').
card_rarity('ancient tomb'/'VMA', 'Rare').
card_artist('ancient tomb'/'VMA', 'Colin MacNeil').
card_number('ancient tomb'/'VMA', '289').
card_flavor_text('ancient tomb'/'VMA', 'There is no glory to be gained in the kingdom of the dead.\n—Vec tomb inscription').
card_multiverse_id('ancient tomb'/'VMA', '382842').

card_in_set('animate dead', 'VMA').
card_original_type('animate dead'/'VMA', 'Enchantment — Aura').
card_original_text('animate dead'/'VMA', 'Enchant creature card in a graveyard\nWhen Animate Dead enters the battlefield, if it\'s on the battlefield, it loses \"enchant creature card in a graveyard\" and gains \"enchant creature put onto the battlefield with Animate Dead.\" Return enchanted creature card to the battlefield under your control and attach Animate Dead to it. When Animate Dead leaves the battlefield, that creature\'s controller sacrifices it.\nEnchanted creature gets -1/-0.').
card_image_name('animate dead'/'VMA', 'animate dead').
card_uid('animate dead'/'VMA', 'VMA:Animate Dead:animate dead').
card_rarity('animate dead'/'VMA', 'Uncommon').
card_artist('animate dead'/'VMA', 'Anthony Jones').
card_number('animate dead'/'VMA', '104').
card_multiverse_id('animate dead'/'VMA', '382843').

card_in_set('ankh of mishra', 'VMA').
card_original_type('ankh of mishra'/'VMA', 'Artifact').
card_original_text('ankh of mishra'/'VMA', 'Whenever a land enters the battlefield, Ankh of Mishra deals 2 damage to that land\'s controller.').
card_image_name('ankh of mishra'/'VMA', 'ankh of mishra').
card_uid('ankh of mishra'/'VMA', 'VMA:Ankh of Mishra:ankh of mishra').
card_rarity('ankh of mishra'/'VMA', 'Rare').
card_artist('ankh of mishra'/'VMA', 'Ian Miller').
card_number('ankh of mishra'/'VMA', '263').
card_flavor_text('ankh of mishra'/'VMA', 'Tawnos finally cracked the puzzle: the strange structures did not house Mishra\'s malevolent creations but were themselves among his creations.').
card_multiverse_id('ankh of mishra'/'VMA', '382844').

card_in_set('aquamoeba', 'VMA').
card_original_type('aquamoeba'/'VMA', 'Creature — Elemental Beast').
card_original_text('aquamoeba'/'VMA', 'Discard a card: Switch Aquamoeba\'s power and toughness until end of turn.').
card_image_name('aquamoeba'/'VMA', 'aquamoeba').
card_uid('aquamoeba'/'VMA', 'VMA:Aquamoeba:aquamoeba').
card_rarity('aquamoeba'/'VMA', 'Common').
card_artist('aquamoeba'/'VMA', 'Arnie Swekel').
card_number('aquamoeba'/'VMA', '56').
card_flavor_text('aquamoeba'/'VMA', 'Some tides need no moon.').
card_multiverse_id('aquamoeba'/'VMA', '382845').

card_in_set('armadillo cloak', 'VMA').
card_original_type('armadillo cloak'/'VMA', 'Enchantment — Aura').
card_original_text('armadillo cloak'/'VMA', 'Enchant creature\nEnchanted creature gets +2/+2 and has trample.\nWhenever enchanted creature deals damage, you gain that much life.').
card_image_name('armadillo cloak'/'VMA', 'armadillo cloak').
card_uid('armadillo cloak'/'VMA', 'VMA:Armadillo Cloak:armadillo cloak').
card_rarity('armadillo cloak'/'VMA', 'Uncommon').
card_artist('armadillo cloak'/'VMA', 'Wayne Reynolds').
card_number('armadillo cloak'/'VMA', '242').
card_flavor_text('armadillo cloak'/'VMA', '\"Don\'t laugh. It works.\"').
card_multiverse_id('armadillo cloak'/'VMA', '382846').

card_in_set('armageddon', 'VMA').
card_original_type('armageddon'/'VMA', 'Sorcery').
card_original_text('armageddon'/'VMA', 'Destroy all lands.').
card_image_name('armageddon'/'VMA', 'armageddon').
card_uid('armageddon'/'VMA', 'VMA:Armageddon:armageddon').
card_rarity('armageddon'/'VMA', 'Mythic Rare').
card_artist('armageddon'/'VMA', 'John Avon').
card_number('armageddon'/'VMA', '12').
card_flavor_text('armageddon'/'VMA', 'War destroys more than just the land. War destroys hope.').
card_multiverse_id('armageddon'/'VMA', '382847').

card_in_set('armor of thorns', 'VMA').
card_original_type('armor of thorns'/'VMA', 'Enchantment — Aura').
card_original_text('armor of thorns'/'VMA', 'You may cast Armor of Thorns as though it had flash. If you cast it any time a sorcery couldn\'t have been cast, the controller of the permanent it becomes sacrifices it at the beginning of the next cleanup step.\nEnchant nonblack creature\nEnchanted creature gets +2/+2.').
card_image_name('armor of thorns'/'VMA', 'armor of thorns').
card_uid('armor of thorns'/'VMA', 'VMA:Armor of Thorns:armor of thorns').
card_rarity('armor of thorns'/'VMA', 'Common').
card_artist('armor of thorns'/'VMA', 'Alan Rabinowitz').
card_number('armor of thorns'/'VMA', '194').
card_multiverse_id('armor of thorns'/'VMA', '382848').

card_in_set('arrogant wurm', 'VMA').
card_original_type('arrogant wurm'/'VMA', 'Creature — Wurm').
card_original_text('arrogant wurm'/'VMA', 'Trample\nMadness {2}{G} (If you discard this card, you may cast it for its madness cost instead of putting it into your graveyard.)').
card_image_name('arrogant wurm'/'VMA', 'arrogant wurm').
card_uid('arrogant wurm'/'VMA', 'VMA:Arrogant Wurm:arrogant wurm').
card_rarity('arrogant wurm'/'VMA', 'Common').
card_artist('arrogant wurm'/'VMA', 'John Avon').
card_number('arrogant wurm'/'VMA', '195').
card_flavor_text('arrogant wurm'/'VMA', 'It\'s hard to be humble when the whole world is bite-size.').
card_multiverse_id('arrogant wurm'/'VMA', '382849').

card_in_set('astral slide', 'VMA').
card_original_type('astral slide'/'VMA', 'Enchantment').
card_original_text('astral slide'/'VMA', 'Whenever a player cycles a card, you may exile target creature. If you do, return that card to the battlefield under its owner\'s control at the beginning of the next end step.').
card_image_name('astral slide'/'VMA', 'astral slide').
card_uid('astral slide'/'VMA', 'VMA:Astral Slide:astral slide').
card_rarity('astral slide'/'VMA', 'Uncommon').
card_artist('astral slide'/'VMA', 'Ron Spears').
card_number('astral slide'/'VMA', '13').
card_flavor_text('astral slide'/'VMA', '\"The hum of the universe is never off-key.\"\n—Mystic elder').
card_multiverse_id('astral slide'/'VMA', '382850').

card_in_set('bad river', 'VMA').
card_original_type('bad river'/'VMA', 'Land').
card_original_text('bad river'/'VMA', 'Bad River enters the battlefield tapped.\n{T}, Sacrifice Bad River: Search your library for an Island or Swamp card and put it onto the battlefield. Then shuffle your library.').
card_image_name('bad river'/'VMA', 'bad river').
card_uid('bad river'/'VMA', 'VMA:Bad River:bad river').
card_rarity('bad river'/'VMA', 'Uncommon').
card_artist('bad river'/'VMA', 'Terese Nielsen').
card_number('bad river'/'VMA', '290').
card_multiverse_id('bad river'/'VMA', '382851').

card_in_set('badlands', 'VMA').
card_original_type('badlands'/'VMA', 'Land — Swamp Mountain').
card_original_text('badlands'/'VMA', '').
card_image_name('badlands'/'VMA', 'badlands').
card_uid('badlands'/'VMA', 'VMA:Badlands:badlands').
card_rarity('badlands'/'VMA', 'Rare').
card_artist('badlands'/'VMA', 'Daarken').
card_number('badlands'/'VMA', '291').
card_multiverse_id('badlands'/'VMA', '382852').

card_in_set('balance', 'VMA').
card_original_type('balance'/'VMA', 'Sorcery').
card_original_text('balance'/'VMA', 'Each player chooses a number of lands he or she controls equal to the number of lands controlled by the player who controls the fewest, then sacrifices the rest. Players discard cards and sacrifice creatures the same way.').
card_image_name('balance'/'VMA', 'balance').
card_uid('balance'/'VMA', 'VMA:Balance:balance').
card_rarity('balance'/'VMA', 'Mythic Rare').
card_artist('balance'/'VMA', 'Kev Walker').
card_number('balance'/'VMA', '14').
card_multiverse_id('balance'/'VMA', '382853').

card_in_set('baleful force', 'VMA').
card_original_type('baleful force'/'VMA', 'Creature — Elemental').
card_original_text('baleful force'/'VMA', 'At the beginning of each upkeep, you draw a card and you lose 1 life.').
card_image_name('baleful force'/'VMA', 'baleful force').
card_uid('baleful force'/'VMA', 'VMA:Baleful Force:baleful force').
card_rarity('baleful force'/'VMA', 'Rare').
card_artist('baleful force'/'VMA', 'Eytan Zana').
card_number('baleful force'/'VMA', '105').
card_flavor_text('baleful force'/'VMA', '\"As with any malevolent being, the trick is knowing how long you can afford to keep it in your service.\"\n—Liliana Vess').
card_multiverse_id('baleful force'/'VMA', '382854').

card_in_set('baleful strix', 'VMA').
card_original_type('baleful strix'/'VMA', 'Artifact Creature — Bird').
card_original_text('baleful strix'/'VMA', 'Flying, deathtouch\nWhen Baleful Strix enters the battlefield, draw a card.').
card_image_name('baleful strix'/'VMA', 'baleful strix').
card_uid('baleful strix'/'VMA', 'VMA:Baleful Strix:baleful strix').
card_rarity('baleful strix'/'VMA', 'Rare').
card_artist('baleful strix'/'VMA', 'Nils Hamm').
card_number('baleful strix'/'VMA', '243').
card_flavor_text('baleful strix'/'VMA', 'Its beak rends flesh and bone, exposing the tender marrow of dream.').
card_multiverse_id('baleful strix'/'VMA', '382855').

card_in_set('barren moor', 'VMA').
card_original_type('barren moor'/'VMA', 'Land').
card_original_text('barren moor'/'VMA', 'Barren Moor enters the battlefield tapped.\n{T}: Add {B} to your mana pool.\nCycling {B} ({B}, Discard this card: Draw a card.)').
card_image_name('barren moor'/'VMA', 'barren moor').
card_uid('barren moor'/'VMA', 'VMA:Barren Moor:barren moor').
card_rarity('barren moor'/'VMA', 'Common').
card_artist('barren moor'/'VMA', 'Heather Hudson').
card_number('barren moor'/'VMA', '292').
card_multiverse_id('barren moor'/'VMA', '382856').

card_in_set('basandra, battle seraph', 'VMA').
card_original_type('basandra, battle seraph'/'VMA', 'Legendary Creature — Angel').
card_original_text('basandra, battle seraph'/'VMA', 'Flying\nPlayers can\'t cast spells during combat.\n{R}: Target creature attacks this turn if able.').
card_image_name('basandra, battle seraph'/'VMA', 'basandra, battle seraph').
card_uid('basandra, battle seraph'/'VMA', 'VMA:Basandra, Battle Seraph:basandra, battle seraph').
card_rarity('basandra, battle seraph'/'VMA', 'Rare').
card_artist('basandra, battle seraph'/'VMA', 'Terese Nielsen').
card_number('basandra, battle seraph'/'VMA', '244').
card_flavor_text('basandra, battle seraph'/'VMA', '\"Why listen to the mumblings of wizards when the lash speaks true?\"').
card_multiverse_id('basandra, battle seraph'/'VMA', '382857').

card_in_set('basking rootwalla', 'VMA').
card_original_type('basking rootwalla'/'VMA', 'Creature — Lizard').
card_original_text('basking rootwalla'/'VMA', '{1}{G}: Basking Rootwalla gets +2/+2 until end of turn. Activate this ability only once each turn.\nMadness {0} (If you discard this card, you may cast it for its madness cost instead of putting it into your graveyard.)').
card_image_name('basking rootwalla'/'VMA', 'basking rootwalla').
card_uid('basking rootwalla'/'VMA', 'VMA:Basking Rootwalla:basking rootwalla').
card_rarity('basking rootwalla'/'VMA', 'Common').
card_artist('basking rootwalla'/'VMA', 'Heather Hudson').
card_number('basking rootwalla'/'VMA', '196').
card_multiverse_id('basking rootwalla'/'VMA', '382858').

card_in_set('battle screech', 'VMA').
card_original_type('battle screech'/'VMA', 'Sorcery').
card_original_text('battle screech'/'VMA', 'Put two 1/1 white Bird creature tokens with flying onto the battlefield.\nFlashback—Tap three untapped white creatures you control. (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('battle screech'/'VMA', 'battle screech').
card_uid('battle screech'/'VMA', 'VMA:Battle Screech:battle screech').
card_rarity('battle screech'/'VMA', 'Common').
card_artist('battle screech'/'VMA', 'Randy Gallegos').
card_number('battle screech'/'VMA', '15').
card_multiverse_id('battle screech'/'VMA', '382859').

card_in_set('bayou', 'VMA').
card_original_type('bayou'/'VMA', 'Land — Swamp Forest').
card_original_text('bayou'/'VMA', '').
card_image_name('bayou'/'VMA', 'bayou').
card_uid('bayou'/'VMA', 'VMA:Bayou:bayou').
card_rarity('bayou'/'VMA', 'Rare').
card_artist('bayou'/'VMA', 'Karl Kopinski').
card_number('bayou'/'VMA', '293').
card_multiverse_id('bayou'/'VMA', '382860').

card_in_set('bazaar of baghdad', 'VMA').
card_original_type('bazaar of baghdad'/'VMA', 'Land').
card_original_text('bazaar of baghdad'/'VMA', '{T}: Draw two cards, then discard three cards.').
card_image_name('bazaar of baghdad'/'VMA', 'bazaar of baghdad').
card_uid('bazaar of baghdad'/'VMA', 'VMA:Bazaar of Baghdad:bazaar of baghdad').
card_rarity('bazaar of baghdad'/'VMA', 'Mythic Rare').
card_artist('bazaar of baghdad'/'VMA', 'Christopher Moeller').
card_number('bazaar of baghdad'/'VMA', '294').
card_multiverse_id('bazaar of baghdad'/'VMA', '382861').

card_in_set('beetleback chief', 'VMA').
card_original_type('beetleback chief'/'VMA', 'Creature — Goblin Warrior').
card_original_text('beetleback chief'/'VMA', 'When Beetleback Chief enters the battlefield, put two 1/1 red Goblin creature tokens onto the battlefield.').
card_image_name('beetleback chief'/'VMA', 'beetleback chief').
card_uid('beetleback chief'/'VMA', 'VMA:Beetleback Chief:beetleback chief').
card_rarity('beetleback chief'/'VMA', 'Common').
card_artist('beetleback chief'/'VMA', 'Wayne England').
card_number('beetleback chief'/'VMA', '150').
card_flavor_text('beetleback chief'/'VMA', 'Whether trained, ridden, or eaten, few goblin military innovations have rivaled the bug.').
card_multiverse_id('beetleback chief'/'VMA', '382862').

card_in_set('benalish trapper', 'VMA').
card_original_type('benalish trapper'/'VMA', 'Creature — Human Soldier').
card_original_text('benalish trapper'/'VMA', '{W}, {T}: Tap target creature.').
card_image_name('benalish trapper'/'VMA', 'benalish trapper').
card_uid('benalish trapper'/'VMA', 'VMA:Benalish Trapper:benalish trapper').
card_rarity('benalish trapper'/'VMA', 'Common').
card_artist('benalish trapper'/'VMA', 'Ken Meyer, Jr.').
card_number('benalish trapper'/'VMA', '16').
card_flavor_text('benalish trapper'/'VMA', '\"I\'m up here. You\'re down there. Now who\'s the lower life form?\"').
card_multiverse_id('benalish trapper'/'VMA', '382863').

card_in_set('benevolent bodyguard', 'VMA').
card_original_type('benevolent bodyguard'/'VMA', 'Creature — Human Cleric').
card_original_text('benevolent bodyguard'/'VMA', 'Sacrifice Benevolent Bodyguard: Target creature you control gains protection from the color of your choice until end of turn.').
card_image_name('benevolent bodyguard'/'VMA', 'benevolent bodyguard').
card_uid('benevolent bodyguard'/'VMA', 'VMA:Benevolent Bodyguard:benevolent bodyguard').
card_rarity('benevolent bodyguard'/'VMA', 'Common').
card_artist('benevolent bodyguard'/'VMA', 'Roger Raupp').
card_number('benevolent bodyguard'/'VMA', '17').
card_flavor_text('benevolent bodyguard'/'VMA', '\"My destiny is to save others so their destinies may be achieved.\"').
card_multiverse_id('benevolent bodyguard'/'VMA', '382864').

card_in_set('berserk', 'VMA').
card_original_type('berserk'/'VMA', 'Instant').
card_original_text('berserk'/'VMA', 'Cast Berserk only before the combat damage step.\nTarget creature gains trample and gets +X/+0 until end of turn, where X is its power. At the beginning of the next end step, destroy that creature if it attacked this turn.').
card_image_name('berserk'/'VMA', 'berserk').
card_uid('berserk'/'VMA', 'VMA:Berserk:berserk').
card_rarity('berserk'/'VMA', 'Rare').
card_artist('berserk'/'VMA', 'Steve Prescott').
card_number('berserk'/'VMA', '197').
card_multiverse_id('berserk'/'VMA', '382865').

card_in_set('black lotus', 'VMA').
card_original_type('black lotus'/'VMA', 'Artifact').
card_original_text('black lotus'/'VMA', '{T}, Sacrifice Black Lotus: Add three mana of any one color to your mana pool.').
card_image_name('black lotus'/'VMA', 'black lotus').
card_uid('black lotus'/'VMA', 'VMA:Black Lotus:black lotus').
card_rarity('black lotus'/'VMA', 'Special').
card_artist('black lotus'/'VMA', 'Chris Rahn').
card_number('black lotus'/'VMA', '4').
card_multiverse_id('black lotus'/'VMA', '382866').

card_in_set('blastoderm', 'VMA').
card_original_type('blastoderm'/'VMA', 'Creature — Beast').
card_original_text('blastoderm'/'VMA', 'Shroud (This creature can\'t be the target of spells or abilities.)\nFading 3 (This creature enters the battlefield with three fade counters on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)').
card_image_name('blastoderm'/'VMA', 'blastoderm').
card_uid('blastoderm'/'VMA', 'VMA:Blastoderm:blastoderm').
card_rarity('blastoderm'/'VMA', 'Uncommon').
card_artist('blastoderm'/'VMA', 'Nils Hamm').
card_number('blastoderm'/'VMA', '198').
card_multiverse_id('blastoderm'/'VMA', '382867').

card_in_set('blazing specter', 'VMA').
card_original_type('blazing specter'/'VMA', 'Creature — Specter').
card_original_text('blazing specter'/'VMA', 'Flying, haste\nWhenever Blazing Specter deals combat damage to a player, that player discards a card.').
card_image_name('blazing specter'/'VMA', 'blazing specter').
card_uid('blazing specter'/'VMA', 'VMA:Blazing Specter:blazing specter').
card_rarity('blazing specter'/'VMA', 'Uncommon').
card_artist('blazing specter'/'VMA', 'Marc Fishman').
card_number('blazing specter'/'VMA', '245').
card_multiverse_id('blazing specter'/'VMA', '382868').

card_in_set('brago, king eternal', 'VMA').
card_original_type('brago, king eternal'/'VMA', 'Legendary Creature — Spirit').
card_original_text('brago, king eternal'/'VMA', 'Flying\nWhen Brago, King Eternal deals combat damage to a player, exile any number of target nonland permanents you control, then return those cards to the battlefield under their owner\'s control.').
card_image_name('brago, king eternal'/'VMA', 'brago, king eternal').
card_uid('brago, king eternal'/'VMA', 'VMA:Brago, King Eternal:brago, king eternal').
card_rarity('brago, king eternal'/'VMA', 'Rare').
card_artist('brago, king eternal'/'VMA', 'Karla Ortiz').
card_number('brago, king eternal'/'VMA', '246').
card_flavor_text('brago, king eternal'/'VMA', '\"My rule persists beyond death itself.\"').
card_multiverse_id('brago, king eternal'/'VMA', '382869').

card_in_set('brain freeze', 'VMA').
card_original_type('brain freeze'/'VMA', 'Instant').
card_original_text('brain freeze'/'VMA', 'Target player puts the top three cards of his or her library into his or her graveyard.\nStorm (When you cast this spell, copy it for each spell cast before it this turn. You may choose new targets for the copies.)').
card_image_name('brain freeze'/'VMA', 'brain freeze').
card_uid('brain freeze'/'VMA', 'VMA:Brain Freeze:brain freeze').
card_rarity('brain freeze'/'VMA', 'Uncommon').
card_artist('brain freeze'/'VMA', 'Tim Hildebrandt').
card_number('brain freeze'/'VMA', '57').
card_multiverse_id('brain freeze'/'VMA', '382870').

card_in_set('brainstorm', 'VMA').
card_original_type('brainstorm'/'VMA', 'Instant').
card_original_text('brainstorm'/'VMA', 'Draw three cards, then put two cards from your hand on top of your library in any order.').
card_image_name('brainstorm'/'VMA', 'brainstorm').
card_uid('brainstorm'/'VMA', 'VMA:Brainstorm:brainstorm').
card_rarity('brainstorm'/'VMA', 'Common').
card_artist('brainstorm'/'VMA', 'Chris Rahn').
card_number('brainstorm'/'VMA', '58').
card_multiverse_id('brainstorm'/'VMA', '382871').

card_in_set('breath of life', 'VMA').
card_original_type('breath of life'/'VMA', 'Sorcery').
card_original_text('breath of life'/'VMA', 'Return target creature card from your graveyard to the battlefield.').
card_image_name('breath of life'/'VMA', 'breath of life').
card_uid('breath of life'/'VMA', 'VMA:Breath of Life:breath of life').
card_rarity('breath of life'/'VMA', 'Uncommon').
card_artist('breath of life'/'VMA', 'Roger Raupp').
card_number('breath of life'/'VMA', '18').
card_flavor_text('breath of life'/'VMA', '\"Nothing can stop the power of life—not even death.\"\n—Onean cleric').
card_multiverse_id('breath of life'/'VMA', '382872').

card_in_set('brilliant halo', 'VMA').
card_original_type('brilliant halo'/'VMA', 'Enchantment — Aura').
card_original_text('brilliant halo'/'VMA', 'Enchant creature\nEnchanted creature gets +1/+2.\nWhen Brilliant Halo is put into a graveyard from the battlefield, return Brilliant Halo to its owner\'s hand.').
card_image_name('brilliant halo'/'VMA', 'brilliant halo').
card_uid('brilliant halo'/'VMA', 'VMA:Brilliant Halo:brilliant halo').
card_rarity('brilliant halo'/'VMA', 'Common').
card_artist('brilliant halo'/'VMA', 'Randy Gallegos').
card_number('brilliant halo'/'VMA', '19').
card_multiverse_id('brilliant halo'/'VMA', '382873').

card_in_set('brindle shoat', 'VMA').
card_original_type('brindle shoat'/'VMA', 'Creature — Boar').
card_original_text('brindle shoat'/'VMA', 'When Brindle Shoat dies, put a 3/3 green Boar creature token onto the battlefield.').
card_image_name('brindle shoat'/'VMA', 'brindle shoat').
card_uid('brindle shoat'/'VMA', 'VMA:Brindle Shoat:brindle shoat').
card_rarity('brindle shoat'/'VMA', 'Common').
card_artist('brindle shoat'/'VMA', 'Steven Belledin').
card_number('brindle shoat'/'VMA', '199').
card_flavor_text('brindle shoat'/'VMA', 'Hunters lure the stripling boar into the open, hoping to trap greater prey.').
card_multiverse_id('brindle shoat'/'VMA', '382874').

card_in_set('burning of xinye', 'VMA').
card_original_type('burning of xinye'/'VMA', 'Sorcery').
card_original_text('burning of xinye'/'VMA', 'You destroy four lands you control, then target opponent destroys four lands he or she controls. Then Burning of Xinye deals 4 damage to each creature.').
card_image_name('burning of xinye'/'VMA', 'burning of xinye').
card_uid('burning of xinye'/'VMA', 'VMA:Burning of Xinye:burning of xinye').
card_rarity('burning of xinye'/'VMA', 'Rare').
card_artist('burning of xinye'/'VMA', 'Yang Hong').
card_number('burning of xinye'/'VMA', '151').
card_multiverse_id('burning of xinye'/'VMA', '382875').

card_in_set('burning wish', 'VMA').
card_original_type('burning wish'/'VMA', 'Sorcery').
card_original_text('burning wish'/'VMA', 'You may choose a sorcery card you own from outside the game, reveal that card, and put it into your hand. Exile Burning Wish.').
card_image_name('burning wish'/'VMA', 'burning wish').
card_uid('burning wish'/'VMA', 'VMA:Burning Wish:burning wish').
card_rarity('burning wish'/'VMA', 'Rare').
card_artist('burning wish'/'VMA', 'Scott M. Fischer').
card_number('burning wish'/'VMA', '152').
card_flavor_text('burning wish'/'VMA', 'She wished for a weapon, but not for the skill to wield it.').
card_multiverse_id('burning wish'/'VMA', '382876').

card_in_set('cabal ritual', 'VMA').
card_original_type('cabal ritual'/'VMA', 'Instant').
card_original_text('cabal ritual'/'VMA', 'Add {B}{B}{B} to your mana pool.\nThreshold — Add {B}{B}{B}{B}{B} to your mana pool instead if seven or more cards are in your graveyard.').
card_image_name('cabal ritual'/'VMA', 'cabal ritual').
card_uid('cabal ritual'/'VMA', 'VMA:Cabal Ritual:cabal ritual').
card_rarity('cabal ritual'/'VMA', 'Uncommon').
card_artist('cabal ritual'/'VMA', 'Greg Hildebrandt').
card_number('cabal ritual'/'VMA', '106').
card_flavor_text('cabal ritual'/'VMA', '\"Each syllable chills your veins. Each word rattles your mind.\"\n—Cabal Patriarch').
card_multiverse_id('cabal ritual'/'VMA', '382877').

card_in_set('caldera lake', 'VMA').
card_original_type('caldera lake'/'VMA', 'Land').
card_original_text('caldera lake'/'VMA', 'Caldera Lake enters the battlefield tapped.\n{T}: Add {1} to your mana pool.\n{T}: Add {U} or {R} to your mana pool. Caldera Lake deals 1 damage to you.').
card_image_name('caldera lake'/'VMA', 'caldera lake').
card_uid('caldera lake'/'VMA', 'VMA:Caldera Lake:caldera lake').
card_rarity('caldera lake'/'VMA', 'Uncommon').
card_artist('caldera lake'/'VMA', 'Allen Williams').
card_number('caldera lake'/'VMA', '295').
card_multiverse_id('caldera lake'/'VMA', '382878').

card_in_set('carnophage', 'VMA').
card_original_type('carnophage'/'VMA', 'Creature — Zombie').
card_original_text('carnophage'/'VMA', 'At the beginning of your upkeep, tap Carnophage unless you pay 1 life.').
card_image_name('carnophage'/'VMA', 'carnophage').
card_uid('carnophage'/'VMA', 'VMA:Carnophage:carnophage').
card_rarity('carnophage'/'VMA', 'Common').
card_artist('carnophage'/'VMA', 'Pete Venters').
card_number('carnophage'/'VMA', '107').
card_flavor_text('carnophage'/'VMA', 'Eating is all it knows.').
card_multiverse_id('carnophage'/'VMA', '382879').

card_in_set('chain lightning', 'VMA').
card_original_type('chain lightning'/'VMA', 'Sorcery').
card_original_text('chain lightning'/'VMA', 'Chain Lightning deals 3 damage to target creature or player. Then that player or that creature\'s controller may pay {R}{R}. If the player does, he or she may copy this spell and may choose a new target for that copy.').
card_image_name('chain lightning'/'VMA', 'chain lightning').
card_uid('chain lightning'/'VMA', 'VMA:Chain Lightning:chain lightning').
card_rarity('chain lightning'/'VMA', 'Common').
card_artist('chain lightning'/'VMA', 'Christopher Moeller').
card_number('chain lightning'/'VMA', '153').
card_multiverse_id('chain lightning'/'VMA', '382880').

card_in_set('chainer\'s edict', 'VMA').
card_original_type('chainer\'s edict'/'VMA', 'Sorcery').
card_original_text('chainer\'s edict'/'VMA', 'Target player sacrifices a creature.\nFlashback {5}{B}{B} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('chainer\'s edict'/'VMA', 'chainer\'s edict').
card_uid('chainer\'s edict'/'VMA', 'VMA:Chainer\'s Edict:chainer\'s edict').
card_rarity('chainer\'s edict'/'VMA', 'Common').
card_artist('chainer\'s edict'/'VMA', 'Mark Zug').
card_number('chainer\'s edict'/'VMA', '108').
card_multiverse_id('chainer\'s edict'/'VMA', '382881').

card_in_set('channel', 'VMA').
card_original_type('channel'/'VMA', 'Sorcery').
card_original_text('channel'/'VMA', 'Until end of turn, any time you could activate a mana ability, you may pay 1 life. If you do, add {1} to your mana pool.').
card_image_name('channel'/'VMA', 'channel').
card_uid('channel'/'VMA', 'VMA:Channel:channel').
card_rarity('channel'/'VMA', 'Mythic Rare').
card_artist('channel'/'VMA', 'Rebecca Guay').
card_number('channel'/'VMA', '200').
card_multiverse_id('channel'/'VMA', '382882').

card_in_set('chaos warp', 'VMA').
card_original_type('chaos warp'/'VMA', 'Instant').
card_original_text('chaos warp'/'VMA', 'The owner of target permanent shuffles it into his or her library, then reveals the top card of his or her library. If it\'s a permanent card, he or she puts it onto the battlefield.').
card_image_name('chaos warp'/'VMA', 'chaos warp').
card_uid('chaos warp'/'VMA', 'VMA:Chaos Warp:chaos warp').
card_rarity('chaos warp'/'VMA', 'Rare').
card_artist('chaos warp'/'VMA', 'Trevor Claxton').
card_number('chaos warp'/'VMA', '154').
card_multiverse_id('chaos warp'/'VMA', '382883').

card_in_set('chartooth cougar', 'VMA').
card_original_type('chartooth cougar'/'VMA', 'Creature — Cat Beast').
card_original_text('chartooth cougar'/'VMA', '{R}: Chartooth Cougar gets +1/+0 until end of turn.\nMountaincycling {2} ({2}, Discard this card: Search your library for a Mountain card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('chartooth cougar'/'VMA', 'chartooth cougar').
card_uid('chartooth cougar'/'VMA', 'VMA:Chartooth Cougar:chartooth cougar').
card_rarity('chartooth cougar'/'VMA', 'Common').
card_artist('chartooth cougar'/'VMA', 'Chase Stone').
card_number('chartooth cougar'/'VMA', '155').
card_multiverse_id('chartooth cougar'/'VMA', '382884').

card_in_set('chimeric idol', 'VMA').
card_original_type('chimeric idol'/'VMA', 'Artifact').
card_original_text('chimeric idol'/'VMA', '{0}: Tap all lands you control. Chimeric Idol becomes a 3/3 Turtle artifact creature until end of turn.').
card_image_name('chimeric idol'/'VMA', 'chimeric idol').
card_uid('chimeric idol'/'VMA', 'VMA:Chimeric Idol:chimeric idol').
card_rarity('chimeric idol'/'VMA', 'Uncommon').
card_artist('chimeric idol'/'VMA', 'Mark Tedin').
card_number('chimeric idol'/'VMA', '264').
card_flavor_text('chimeric idol'/'VMA', 'After a chimeric idol attacked them, the Keldons smashed all unfamiliar statues.').
card_multiverse_id('chimeric idol'/'VMA', '382885').

card_in_set('choking sands', 'VMA').
card_original_type('choking sands'/'VMA', 'Sorcery').
card_original_text('choking sands'/'VMA', 'Destroy target non-Swamp land. If that land was nonbasic, Choking Sands deals 2 damage to the land\'s controller.').
card_image_name('choking sands'/'VMA', 'choking sands').
card_uid('choking sands'/'VMA', 'VMA:Choking Sands:choking sands').
card_rarity('choking sands'/'VMA', 'Common').
card_artist('choking sands'/'VMA', 'Roger Raupp').
card_number('choking sands'/'VMA', '109').
card_flavor_text('choking sands'/'VMA', '\"The people wiped the sand from their eyes and cursed—and left the barren land to the hyenas and vipers.\"\n—Afari, Tales').
card_multiverse_id('choking sands'/'VMA', '382886').

card_in_set('choking tethers', 'VMA').
card_original_type('choking tethers'/'VMA', 'Instant').
card_original_text('choking tethers'/'VMA', 'Tap up to four target creatures.\nCycling {1}{U} ({1}{U}, Discard this card: Draw a card.)\nWhen you cycle Choking Tethers, you may tap target creature.').
card_image_name('choking tethers'/'VMA', 'choking tethers').
card_uid('choking tethers'/'VMA', 'VMA:Choking Tethers:choking tethers').
card_rarity('choking tethers'/'VMA', 'Common').
card_artist('choking tethers'/'VMA', 'Carl Critchlow').
card_number('choking tethers'/'VMA', '59').
card_multiverse_id('choking tethers'/'VMA', '382887').

card_in_set('circular logic', 'VMA').
card_original_type('circular logic'/'VMA', 'Instant').
card_original_text('circular logic'/'VMA', 'Counter target spell unless its controller pays {1} for each card in your graveyard.\nMadness {U} (If you discard this card, you may cast it for its madness cost instead of putting it into your graveyard.)').
card_image_name('circular logic'/'VMA', 'circular logic').
card_uid('circular logic'/'VMA', 'VMA:Circular Logic:circular logic').
card_rarity('circular logic'/'VMA', 'Common').
card_artist('circular logic'/'VMA', 'Anthony S. Waters').
card_number('circular logic'/'VMA', '60').
card_multiverse_id('circular logic'/'VMA', '382888').

card_in_set('city in a bottle', 'VMA').
card_original_type('city in a bottle'/'VMA', 'Artifact').
card_original_text('city in a bottle'/'VMA', 'Whenever a nontoken permanent originally printed in the Arabian Nights expansion other than City in a Bottle is on the battlefield, its controller sacrifices it.\nPlayers can\'t play cards originally printed in the Arabian Nights expansion.').
card_image_name('city in a bottle'/'VMA', 'city in a bottle').
card_uid('city in a bottle'/'VMA', 'VMA:City in a Bottle:city in a bottle').
card_rarity('city in a bottle'/'VMA', 'Mythic Rare').
card_artist('city in a bottle'/'VMA', 'Daniel Ljunggren').
card_number('city in a bottle'/'VMA', '265').
card_multiverse_id('city in a bottle'/'VMA', '382889').

card_in_set('claws of wirewood', 'VMA').
card_original_type('claws of wirewood'/'VMA', 'Sorcery').
card_original_text('claws of wirewood'/'VMA', 'Claws of Wirewood deals 3 damage to each creature with flying and each player.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_image_name('claws of wirewood'/'VMA', 'claws of wirewood').
card_uid('claws of wirewood'/'VMA', 'VMA:Claws of Wirewood:claws of wirewood').
card_rarity('claws of wirewood'/'VMA', 'Common').
card_artist('claws of wirewood'/'VMA', 'Tony Szczudlo').
card_number('claws of wirewood'/'VMA', '201').
card_flavor_text('claws of wirewood'/'VMA', 'They say the forest has eyes. They never mention its claws.').
card_multiverse_id('claws of wirewood'/'VMA', '382890').

card_in_set('clickslither', 'VMA').
card_original_type('clickslither'/'VMA', 'Creature — Insect').
card_original_text('clickslither'/'VMA', 'Haste\nSacrifice a Goblin: Clickslither gets +2/+2 and gains trample until end of turn.').
card_image_name('clickslither'/'VMA', 'clickslither').
card_uid('clickslither'/'VMA', 'VMA:Clickslither:clickslither').
card_rarity('clickslither'/'VMA', 'Rare').
card_artist('clickslither'/'VMA', 'Kev Walker').
card_number('clickslither'/'VMA', '156').
card_flavor_text('clickslither'/'VMA', 'The least popular goblins get the outer caves.').
card_multiverse_id('clickslither'/'VMA', '382891').

card_in_set('cloud djinn', 'VMA').
card_original_type('cloud djinn'/'VMA', 'Creature — Djinn').
card_original_text('cloud djinn'/'VMA', 'Flying\nCloud Djinn can block only creatures with flying.').
card_image_name('cloud djinn'/'VMA', 'cloud djinn').
card_uid('cloud djinn'/'VMA', 'VMA:Cloud Djinn:cloud djinn').
card_rarity('cloud djinn'/'VMA', 'Uncommon').
card_artist('cloud djinn'/'VMA', 'Mike Dringenberg').
card_number('cloud djinn'/'VMA', '61').
card_flavor_text('cloud djinn'/'VMA', 'As elusive as the footprint of a djinn\n—Suq\'Ata expression').
card_multiverse_id('cloud djinn'/'VMA', '382892').

card_in_set('cloud of faeries', 'VMA').
card_original_type('cloud of faeries'/'VMA', 'Creature — Faerie').
card_original_text('cloud of faeries'/'VMA', 'Flying\nWhen Cloud of Faeries enters the battlefield, untap up to two lands.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_image_name('cloud of faeries'/'VMA', 'cloud of faeries').
card_uid('cloud of faeries'/'VMA', 'VMA:Cloud of Faeries:cloud of faeries').
card_rarity('cloud of faeries'/'VMA', 'Uncommon').
card_artist('cloud of faeries'/'VMA', 'Melissa A. Benson').
card_number('cloud of faeries'/'VMA', '62').
card_multiverse_id('cloud of faeries'/'VMA', '382893').

card_in_set('coercive portal', 'VMA').
card_original_type('coercive portal'/'VMA', 'Artifact').
card_original_text('coercive portal'/'VMA', 'Will of the council — At the beginning of your upkeep, starting with you, each player votes for carnage or homage. If carnage gets more votes, sacrifice Coercive Portal and destroy all nonland permanents. If homage gets more votes or the vote is tied, draw a card.').
card_image_name('coercive portal'/'VMA', 'coercive portal').
card_uid('coercive portal'/'VMA', 'VMA:Coercive Portal:coercive portal').
card_rarity('coercive portal'/'VMA', 'Mythic Rare').
card_artist('coercive portal'/'VMA', 'Yeong-Hao Han').
card_number('coercive portal'/'VMA', '266').
card_multiverse_id('coercive portal'/'VMA', '382894').

card_in_set('control magic', 'VMA').
card_original_type('control magic'/'VMA', 'Enchantment — Aura').
card_original_text('control magic'/'VMA', 'Enchant creature\nYou control enchanted creature.').
card_image_name('control magic'/'VMA', 'control magic').
card_uid('control magic'/'VMA', 'VMA:Control Magic:control magic').
card_rarity('control magic'/'VMA', 'Rare').
card_artist('control magic'/'VMA', 'Clint Cearley').
card_number('control magic'/'VMA', '63').
card_flavor_text('control magic'/'VMA', '\"Do as I think, not as I do.\"\n—Jace Beleren').
card_multiverse_id('control magic'/'VMA', '382895').

card_in_set('council\'s judgment', 'VMA').
card_original_type('council\'s judgment'/'VMA', 'Sorcery').
card_original_text('council\'s judgment'/'VMA', 'Will of the council — Starting with you, each player votes for a nonland permanent you don\'t control. Exile each permanent with the most votes or tied for most votes.').
card_image_name('council\'s judgment'/'VMA', 'council\'s judgment').
card_uid('council\'s judgment'/'VMA', 'VMA:Council\'s Judgment:council\'s judgment').
card_rarity('council\'s judgment'/'VMA', 'Rare').
card_artist('council\'s judgment'/'VMA', 'Kev Walker').
card_number('council\'s judgment'/'VMA', '20').
card_flavor_text('council\'s judgment'/'VMA', 'When its power is threatened, the council speaks with a unified voice.').
card_multiverse_id('council\'s judgment'/'VMA', '382896').

card_in_set('counterspell', 'VMA').
card_original_type('counterspell'/'VMA', 'Instant').
card_original_text('counterspell'/'VMA', 'Counter target spell.').
card_image_name('counterspell'/'VMA', 'counterspell').
card_uid('counterspell'/'VMA', 'VMA:Counterspell:counterspell').
card_rarity('counterspell'/'VMA', 'Common').
card_artist('counterspell'/'VMA', 'Jason Chan').
card_number('counterspell'/'VMA', '64').
card_flavor_text('counterspell'/'VMA', 'The pyromancer summoned up her mightiest onslaught of fire and rage. Jace feigned interest.').
card_multiverse_id('counterspell'/'VMA', '382897').

card_in_set('crater hellion', 'VMA').
card_original_type('crater hellion'/'VMA', 'Creature — Hellion Beast').
card_original_text('crater hellion'/'VMA', 'Echo {4}{R}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Crater Hellion enters the battlefield, it deals 4 damage to each other creature.').
card_image_name('crater hellion'/'VMA', 'crater hellion').
card_uid('crater hellion'/'VMA', 'VMA:Crater Hellion:crater hellion').
card_rarity('crater hellion'/'VMA', 'Rare').
card_artist('crater hellion'/'VMA', 'Daren Bader').
card_number('crater hellion'/'VMA', '157').
card_multiverse_id('crater hellion'/'VMA', '382898').

card_in_set('crescendo of war', 'VMA').
card_original_type('crescendo of war'/'VMA', 'Enchantment').
card_original_text('crescendo of war'/'VMA', 'At the beginning of each upkeep, put a strife counter on Crescendo of War.\nAttacking creatures get +1/+0 for each strife counter on Crescendo of War.\nBlocking creatures you control get +1/+0 for each strife counter on Crescendo of War.').
card_image_name('crescendo of war'/'VMA', 'crescendo of war').
card_uid('crescendo of war'/'VMA', 'VMA:Crescendo of War:crescendo of war').
card_rarity('crescendo of war'/'VMA', 'Rare').
card_artist('crescendo of war'/'VMA', 'Daarken').
card_number('crescendo of war'/'VMA', '21').
card_multiverse_id('crescendo of war'/'VMA', '382899').

card_in_set('crovax the cursed', 'VMA').
card_original_type('crovax the cursed'/'VMA', 'Legendary Creature — Vampire').
card_original_text('crovax the cursed'/'VMA', 'Crovax the Cursed enters the battlefield with four +1/+1 counters on it.\nAt the beginning of your upkeep, you may sacrifice a creature. If you do, put a +1/+1 counter on Crovax. If you don\'t, remove a +1/+1 counter from Crovax.\n{B}: Crovax gains flying until end of turn.').
card_image_name('crovax the cursed'/'VMA', 'crovax the cursed').
card_uid('crovax the cursed'/'VMA', 'VMA:Crovax the Cursed:crovax the cursed').
card_rarity('crovax the cursed'/'VMA', 'Rare').
card_artist('crovax the cursed'/'VMA', 'Pete Venters').
card_number('crovax the cursed'/'VMA', '110').
card_multiverse_id('crovax the cursed'/'VMA', '382900').

card_in_set('cruel bargain', 'VMA').
card_original_type('cruel bargain'/'VMA', 'Sorcery').
card_original_text('cruel bargain'/'VMA', 'Draw four cards. You lose half your life, rounded up.').
card_image_name('cruel bargain'/'VMA', 'cruel bargain').
card_uid('cruel bargain'/'VMA', 'VMA:Cruel Bargain:cruel bargain').
card_rarity('cruel bargain'/'VMA', 'Rare').
card_artist('cruel bargain'/'VMA', 'Adrian Smith').
card_number('cruel bargain'/'VMA', '111').
card_multiverse_id('cruel bargain'/'VMA', '382901').

card_in_set('cursed scroll', 'VMA').
card_original_type('cursed scroll'/'VMA', 'Artifact').
card_original_text('cursed scroll'/'VMA', '{3}, {T}: Name a card. Reveal a card at random from your hand. If it\'s the named card, Cursed Scroll deals 2 damage to target creature or player.').
card_image_name('cursed scroll'/'VMA', 'cursed scroll').
card_uid('cursed scroll'/'VMA', 'VMA:Cursed Scroll:cursed scroll').
card_rarity('cursed scroll'/'VMA', 'Rare').
card_artist('cursed scroll'/'VMA', 'D. Alexander Gregory').
card_number('cursed scroll'/'VMA', '267').
card_multiverse_id('cursed scroll'/'VMA', '382902').

card_in_set('dack fayden', 'VMA').
card_original_type('dack fayden'/'VMA', 'Planeswalker — Dack').
card_original_text('dack fayden'/'VMA', '+1: Target player draws two cards, then discards two cards.\n-2: Gain control of target artifact.\n-6: You get an emblem with \"Whenever you cast a spell that targets one or more permanents, gain control of those permanents.\"').
card_image_name('dack fayden'/'VMA', 'dack fayden').
card_uid('dack fayden'/'VMA', 'VMA:Dack Fayden:dack fayden').
card_rarity('dack fayden'/'VMA', 'Mythic Rare').
card_artist('dack fayden'/'VMA', 'Eric Deschamps').
card_number('dack fayden'/'VMA', '247').
card_multiverse_id('dack fayden'/'VMA', '382903').

card_in_set('dack\'s duplicate', 'VMA').
card_original_type('dack\'s duplicate'/'VMA', 'Creature — Shapeshifter').
card_original_text('dack\'s duplicate'/'VMA', 'You may have Dack\'s Duplicate enter the battlefield as a copy of any creature on the battlefield except it gains haste and dethrone. (Whenever it attacks the player with the most life or tied for most life, put a +1/+1 counter on it.)').
card_image_name('dack\'s duplicate'/'VMA', 'dack\'s duplicate').
card_uid('dack\'s duplicate'/'VMA', 'VMA:Dack\'s Duplicate:dack\'s duplicate').
card_rarity('dack\'s duplicate'/'VMA', 'Rare').
card_artist('dack\'s duplicate'/'VMA', 'Karl Kopinski').
card_number('dack\'s duplicate'/'VMA', '248').
card_multiverse_id('dack\'s duplicate'/'VMA', '382904').

card_in_set('dark hatchling', 'VMA').
card_original_type('dark hatchling'/'VMA', 'Creature — Horror').
card_original_text('dark hatchling'/'VMA', 'Flying\nWhen Dark Hatchling enters the battlefield, destroy target nonblack creature. It can\'t be regenerated.').
card_image_name('dark hatchling'/'VMA', 'dark hatchling').
card_uid('dark hatchling'/'VMA', 'VMA:Dark Hatchling:dark hatchling').
card_rarity('dark hatchling'/'VMA', 'Uncommon').
card_artist('dark hatchling'/'VMA', 'Brad Rigney').
card_number('dark hatchling'/'VMA', '112').
card_multiverse_id('dark hatchling'/'VMA', '382905').

card_in_set('dark ritual', 'VMA').
card_original_type('dark ritual'/'VMA', 'Instant').
card_original_text('dark ritual'/'VMA', 'Add {B}{B}{B} to your mana pool.').
card_image_name('dark ritual'/'VMA', 'dark ritual').
card_uid('dark ritual'/'VMA', 'VMA:Dark Ritual:dark ritual').
card_rarity('dark ritual'/'VMA', 'Common').
card_artist('dark ritual'/'VMA', 'Clint Langley').
card_number('dark ritual'/'VMA', '113').
card_flavor_text('dark ritual'/'VMA', '\"Leshrac, my liege, grant me the power I am due.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('dark ritual'/'VMA', '382906').

card_in_set('dauthi mercenary', 'VMA').
card_original_type('dauthi mercenary'/'VMA', 'Creature — Dauthi Knight Mercenary').
card_original_text('dauthi mercenary'/'VMA', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\n{1}{B}: Dauthi Mercenary gets +1/+0 until end of turn').
card_image_name('dauthi mercenary'/'VMA', 'dauthi mercenary').
card_uid('dauthi mercenary'/'VMA', 'VMA:Dauthi Mercenary:dauthi mercenary').
card_rarity('dauthi mercenary'/'VMA', 'Common').
card_artist('dauthi mercenary'/'VMA', 'Matthew D. Wilson').
card_number('dauthi mercenary'/'VMA', '114').
card_flavor_text('dauthi mercenary'/'VMA', '\"The dauthi believe they dignify murder by paying for it.\"\n—Lyna, soltari emissary').
card_multiverse_id('dauthi mercenary'/'VMA', '382907').

card_in_set('death grasp', 'VMA').
card_original_type('death grasp'/'VMA', 'Sorcery').
card_original_text('death grasp'/'VMA', 'Death Grasp deals X damage to target creature or player. You gain X life.').
card_image_name('death grasp'/'VMA', 'death grasp').
card_uid('death grasp'/'VMA', 'VMA:Death Grasp:death grasp').
card_rarity('death grasp'/'VMA', 'Uncommon').
card_artist('death grasp'/'VMA', 'Raymond Swanland').
card_number('death grasp'/'VMA', '249').
card_flavor_text('death grasp'/'VMA', 'Plate mail, the skin of the chest, the rib cage, the heart—all useless against the mage who can reach directly for the soul.').
card_multiverse_id('death grasp'/'VMA', '382908').

card_in_set('death\'s-head buzzard', 'VMA').
card_original_type('death\'s-head buzzard'/'VMA', 'Creature — Bird').
card_original_text('death\'s-head buzzard'/'VMA', 'Flying\nWhen Death\'s-Head Buzzard dies, all creatures get -1/-1 until end of turn.').
card_image_name('death\'s-head buzzard'/'VMA', 'death\'s-head buzzard').
card_uid('death\'s-head buzzard'/'VMA', 'VMA:Death\'s-Head Buzzard:death\'s-head buzzard').
card_rarity('death\'s-head buzzard'/'VMA', 'Common').
card_artist('death\'s-head buzzard'/'VMA', 'Marcelo Vignali').
card_number('death\'s-head buzzard'/'VMA', '115').
card_flavor_text('death\'s-head buzzard'/'VMA', 'Infested with vermin, ever hungering, dropping from night\'s sky.').
card_multiverse_id('death\'s-head buzzard'/'VMA', '382909').

card_in_set('deathreap ritual', 'VMA').
card_original_type('deathreap ritual'/'VMA', 'Enchantment').
card_original_text('deathreap ritual'/'VMA', 'Morbid — At the beginning of each end step, if a creature died this turn, you may draw a card.').
card_image_name('deathreap ritual'/'VMA', 'deathreap ritual').
card_uid('deathreap ritual'/'VMA', 'VMA:Deathreap Ritual:deathreap ritual').
card_rarity('deathreap ritual'/'VMA', 'Uncommon').
card_artist('deathreap ritual'/'VMA', 'Steve Argyle').
card_number('deathreap ritual'/'VMA', '250').
card_flavor_text('deathreap ritual'/'VMA', '\"All who set foot in Paliano are pawns in someone\'s play for power.\"\n—Marchesa, the Black Rose').
card_multiverse_id('deathreap ritual'/'VMA', '382910').

card_in_set('decree of justice', 'VMA').
card_original_type('decree of justice'/'VMA', 'Sorcery').
card_original_text('decree of justice'/'VMA', 'Put X 4/4 white Angel creature tokens with flying onto the battlefield.\nCycling {2}{W} ({2}{W}, Discard this card: Draw a card.)\nWhen you cycle Decree of Justice, you may pay {X}. If you do, put X 1/1 white Soldier creature tokens onto the battlefield.').
card_image_name('decree of justice'/'VMA', 'decree of justice').
card_uid('decree of justice'/'VMA', 'VMA:Decree of Justice:decree of justice').
card_rarity('decree of justice'/'VMA', 'Rare').
card_artist('decree of justice'/'VMA', 'Adam Rex').
card_number('decree of justice'/'VMA', '22').
card_multiverse_id('decree of justice'/'VMA', '382911').

card_in_set('deep analysis', 'VMA').
card_original_type('deep analysis'/'VMA', 'Sorcery').
card_original_text('deep analysis'/'VMA', 'Target player draws two cards.\nFlashback—{1}{U}, Pay 3 life. (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('deep analysis'/'VMA', 'deep analysis').
card_uid('deep analysis'/'VMA', 'VMA:Deep Analysis:deep analysis').
card_rarity('deep analysis'/'VMA', 'Common').
card_artist('deep analysis'/'VMA', 'Daren Bader').
card_number('deep analysis'/'VMA', '65').
card_flavor_text('deep analysis'/'VMA', '\"The specimen seems to be broken.\"').
card_multiverse_id('deep analysis'/'VMA', '382912').

card_in_set('deftblade elite', 'VMA').
card_original_type('deftblade elite'/'VMA', 'Creature — Human Soldier').
card_original_text('deftblade elite'/'VMA', 'Provoke (When this attacks, you may have target creature defending player controls untap and block it if able.)\n{1}{W}: Prevent all combat damage that would be dealt to and dealt by Deftblade Elite this turn.').
card_image_name('deftblade elite'/'VMA', 'deftblade elite').
card_uid('deftblade elite'/'VMA', 'VMA:Deftblade Elite:deftblade elite').
card_rarity('deftblade elite'/'VMA', 'Common').
card_artist('deftblade elite'/'VMA', 'Alan Pollack').
card_number('deftblade elite'/'VMA', '23').
card_multiverse_id('deftblade elite'/'VMA', '382913').

card_in_set('demonic tutor', 'VMA').
card_original_type('demonic tutor'/'VMA', 'Sorcery').
card_original_text('demonic tutor'/'VMA', 'Search your library for a card and put that card into your hand. Then shuffle your library.').
card_image_name('demonic tutor'/'VMA', 'demonic tutor').
card_uid('demonic tutor'/'VMA', 'VMA:Demonic Tutor:demonic tutor').
card_rarity('demonic tutor'/'VMA', 'Mythic Rare').
card_artist('demonic tutor'/'VMA', 'Scott Chou').
card_number('demonic tutor'/'VMA', '116').
card_multiverse_id('demonic tutor'/'VMA', '382914').

card_in_set('deranged hermit', 'VMA').
card_original_type('deranged hermit'/'VMA', 'Creature — Elf').
card_original_text('deranged hermit'/'VMA', 'Echo {3}{G}{G} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Deranged Hermit enters the battlefield, put four 1/1 green Squirrel creature tokens onto the battlefield.\nSquirrel creatures get +1/+1.').
card_image_name('deranged hermit'/'VMA', 'deranged hermit').
card_uid('deranged hermit'/'VMA', 'VMA:Deranged Hermit:deranged hermit').
card_rarity('deranged hermit'/'VMA', 'Rare').
card_artist('deranged hermit'/'VMA', 'Kev Walker').
card_number('deranged hermit'/'VMA', '202').
card_multiverse_id('deranged hermit'/'VMA', '382915').

card_in_set('desert twister', 'VMA').
card_original_type('desert twister'/'VMA', 'Sorcery').
card_original_text('desert twister'/'VMA', 'Destroy target permanent.').
card_image_name('desert twister'/'VMA', 'desert twister').
card_uid('desert twister'/'VMA', 'VMA:Desert Twister:desert twister').
card_rarity('desert twister'/'VMA', 'Uncommon').
card_artist('desert twister'/'VMA', 'Noah Bradley').
card_number('desert twister'/'VMA', '203').
card_flavor_text('desert twister'/'VMA', 'Massive, mindless, and imbued with one terrible purpose.').
card_multiverse_id('desert twister'/'VMA', '382916').

card_in_set('devout witness', 'VMA').
card_original_type('devout witness'/'VMA', 'Creature — Human Spellshaper').
card_original_text('devout witness'/'VMA', '{1}{W}, {T}, Discard a card: Destroy target artifact or enchantment.').
card_image_name('devout witness'/'VMA', 'devout witness').
card_uid('devout witness'/'VMA', 'VMA:Devout Witness:devout witness').
card_rarity('devout witness'/'VMA', 'Uncommon').
card_artist('devout witness'/'VMA', 'Don Hazeltine').
card_number('devout witness'/'VMA', '24').
card_flavor_text('devout witness'/'VMA', 'The Cho-Arrim fought Mercadia\'s decadence with more than just swords.').
card_multiverse_id('devout witness'/'VMA', '382917').

card_in_set('drakestown forgotten', 'VMA').
card_original_type('drakestown forgotten'/'VMA', 'Creature — Zombie').
card_original_text('drakestown forgotten'/'VMA', 'Drakestown Forgotten enters the battlefield with X +1/+1 counters on it, where X is the number of creature cards in all graveyards.\n{2}{B}, Remove a +1/+1 counter from Drakestown Forgotten: Target creature gets -1/-1 until end of turn.').
card_image_name('drakestown forgotten'/'VMA', 'drakestown forgotten').
card_uid('drakestown forgotten'/'VMA', 'VMA:Drakestown Forgotten:drakestown forgotten').
card_rarity('drakestown forgotten'/'VMA', 'Rare').
card_artist('drakestown forgotten'/'VMA', 'Steve Prescott').
card_number('drakestown forgotten'/'VMA', '117').
card_multiverse_id('drakestown forgotten'/'VMA', '382918').

card_in_set('dreampod druid', 'VMA').
card_original_type('dreampod druid'/'VMA', 'Creature — Human Druid').
card_original_text('dreampod druid'/'VMA', 'At the beginning of each upkeep, if Dreampod Druid is enchanted, put a 1/1 green Saproling creature token onto the battlefield.').
card_image_name('dreampod druid'/'VMA', 'dreampod druid').
card_uid('dreampod druid'/'VMA', 'VMA:Dreampod Druid:dreampod druid').
card_rarity('dreampod druid'/'VMA', 'Uncommon').
card_artist('dreampod druid'/'VMA', 'Wayne Reynolds').
card_number('dreampod druid'/'VMA', '204').
card_flavor_text('dreampod druid'/'VMA', '\"Don\'t mistake my creations for mere vegetation. They are my children, loyal and fierce.\"').
card_multiverse_id('dreampod druid'/'VMA', '382919').

card_in_set('edric, spymaster of trest', 'VMA').
card_original_type('edric, spymaster of trest'/'VMA', 'Legendary Creature — Elf Rogue').
card_original_text('edric, spymaster of trest'/'VMA', 'Whenever a creature deals combat damage to one of your opponents, its controller may draw a card.').
card_image_name('edric, spymaster of trest'/'VMA', 'edric, spymaster of trest').
card_uid('edric, spymaster of trest'/'VMA', 'VMA:Edric, Spymaster of Trest:edric, spymaster of trest').
card_rarity('edric, spymaster of trest'/'VMA', 'Rare').
card_artist('edric, spymaster of trest'/'VMA', 'Volkan Baga').
card_number('edric, spymaster of trest'/'VMA', '251').
card_flavor_text('edric, spymaster of trest'/'VMA', '\"I am not at liberty to reveal my sources, but I can assure you, the price on your head is high.\"').
card_multiverse_id('edric, spymaster of trest'/'VMA', '382920').

card_in_set('elephant guide', 'VMA').
card_original_type('elephant guide'/'VMA', 'Enchantment — Aura').
card_original_text('elephant guide'/'VMA', 'Enchant creature\nEnchanted creature gets +3/+3.\nWhen enchanted creature dies, put a 3/3 green Elephant creature token onto the battlefield.').
card_image_name('elephant guide'/'VMA', 'elephant guide').
card_uid('elephant guide'/'VMA', 'VMA:Elephant Guide:elephant guide').
card_rarity('elephant guide'/'VMA', 'Common').
card_artist('elephant guide'/'VMA', 'Tomasz Jedruszek').
card_number('elephant guide'/'VMA', '205').
card_flavor_text('elephant guide'/'VMA', 'Nature\'s strength outlives the strong.').
card_multiverse_id('elephant guide'/'VMA', '382921').

card_in_set('elvish aberration', 'VMA').
card_original_type('elvish aberration'/'VMA', 'Creature — Elf Mutant').
card_original_text('elvish aberration'/'VMA', '{T}: Add {G}{G}{G} to your mana pool.\nForestcycling {2} ({2}, Discard this card: Search your library for a Forest card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('elvish aberration'/'VMA', 'elvish aberration').
card_uid('elvish aberration'/'VMA', 'VMA:Elvish Aberration:elvish aberration').
card_rarity('elvish aberration'/'VMA', 'Common').
card_artist('elvish aberration'/'VMA', 'Matt Cavotta').
card_number('elvish aberration'/'VMA', '206').
card_multiverse_id('elvish aberration'/'VMA', '382922').

card_in_set('empyrial armor', 'VMA').
card_original_type('empyrial armor'/'VMA', 'Enchantment — Aura').
card_original_text('empyrial armor'/'VMA', 'Enchant creature\nEnchanted creature gets +1/+1 for each card in your hand.').
card_image_name('empyrial armor'/'VMA', 'empyrial armor').
card_uid('empyrial armor'/'VMA', 'VMA:Empyrial Armor:empyrial armor').
card_rarity('empyrial armor'/'VMA', 'Uncommon').
card_artist('empyrial armor'/'VMA', 'D. Alexander Gregory').
card_number('empyrial armor'/'VMA', '25').
card_flavor_text('empyrial armor'/'VMA', '\"An angel appeared in the smoldering skies above the fray, her clothes as flames, her armor as fire.\"\n—\"Hymn of Angelfire\"').
card_multiverse_id('empyrial armor'/'VMA', '382923').

card_in_set('ephemeron', 'VMA').
card_original_type('ephemeron'/'VMA', 'Creature — Illusion').
card_original_text('ephemeron'/'VMA', 'Flying\nDiscard a card: Return Ephemeron to its owner\'s hand.').
card_image_name('ephemeron'/'VMA', 'ephemeron').
card_uid('ephemeron'/'VMA', 'VMA:Ephemeron:ephemeron').
card_rarity('ephemeron'/'VMA', 'Rare').
card_artist('ephemeron'/'VMA', 'Keith Parkinson').
card_number('ephemeron'/'VMA', '66').
card_flavor_text('ephemeron'/'VMA', 'From nothing came teeth.').
card_multiverse_id('ephemeron'/'VMA', '382924').

card_in_set('erhnam djinn', 'VMA').
card_original_type('erhnam djinn'/'VMA', 'Creature — Djinn').
card_original_text('erhnam djinn'/'VMA', 'At the beginning of your upkeep, target non-Wall creature an opponent controls gains forestwalk until your next upkeep.').
card_image_name('erhnam djinn'/'VMA', 'erhnam djinn').
card_uid('erhnam djinn'/'VMA', 'VMA:Erhnam Djinn:erhnam djinn').
card_rarity('erhnam djinn'/'VMA', 'Uncommon').
card_artist('erhnam djinn'/'VMA', 'Greg Staples').
card_number('erhnam djinn'/'VMA', '207').
card_flavor_text('erhnam djinn'/'VMA', 'He provides a safe path to nowhere.').
card_multiverse_id('erhnam djinn'/'VMA', '382925').

card_in_set('eternal dragon', 'VMA').
card_original_type('eternal dragon'/'VMA', 'Creature — Dragon Spirit').
card_original_text('eternal dragon'/'VMA', 'Flying\n{3}{W}{W}: Return Eternal Dragon from your graveyard to your hand. Activate this ability only during your upkeep.\nPlainscycling {2} ({2}, Discard this card: Search your library for a Plains card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('eternal dragon'/'VMA', 'eternal dragon').
card_uid('eternal dragon'/'VMA', 'VMA:Eternal Dragon:eternal dragon').
card_rarity('eternal dragon'/'VMA', 'Rare').
card_artist('eternal dragon'/'VMA', 'Adam Rex').
card_number('eternal dragon'/'VMA', '26').
card_multiverse_id('eternal dragon'/'VMA', '382926').

card_in_set('eureka', 'VMA').
card_original_type('eureka'/'VMA', 'Sorcery').
card_original_text('eureka'/'VMA', 'Starting with you, each player may put a permanent card from his or her hand onto the battlefield. Repeat this process until no one puts a card onto the battlefield.').
card_image_name('eureka'/'VMA', 'eureka').
card_uid('eureka'/'VMA', 'VMA:Eureka:eureka').
card_rarity('eureka'/'VMA', 'Mythic Rare').
card_artist('eureka'/'VMA', 'Ryan Pancoast').
card_number('eureka'/'VMA', '208').
card_multiverse_id('eureka'/'VMA', '382927').

card_in_set('exile', 'VMA').
card_original_type('exile'/'VMA', 'Instant').
card_original_text('exile'/'VMA', 'Exile target nonwhite attacking creature. You gain life equal to its toughness.').
card_image_name('exile'/'VMA', 'exile').
card_uid('exile'/'VMA', 'VMA:Exile:exile').
card_rarity('exile'/'VMA', 'Common').
card_artist('exile'/'VMA', 'Sam Wolfe Connelly').
card_number('exile'/'VMA', '27').
card_multiverse_id('exile'/'VMA', '382928').

card_in_set('expunge', 'VMA').
card_original_type('expunge'/'VMA', 'Instant').
card_original_text('expunge'/'VMA', 'Destroy target nonartifact, nonblack creature. It can\'t be regenerated.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_image_name('expunge'/'VMA', 'expunge').
card_uid('expunge'/'VMA', 'VMA:Expunge:expunge').
card_rarity('expunge'/'VMA', 'Common').
card_artist('expunge'/'VMA', 'Christopher Moeller').
card_number('expunge'/'VMA', '118').
card_multiverse_id('expunge'/'VMA', '382929').

card_in_set('fact or fiction', 'VMA').
card_original_type('fact or fiction'/'VMA', 'Instant').
card_original_text('fact or fiction'/'VMA', 'Reveal the top five cards of your library. An opponent separates those cards into two piles. Put one pile into your hand and the other into your graveyard.').
card_image_name('fact or fiction'/'VMA', 'fact or fiction').
card_uid('fact or fiction'/'VMA', 'VMA:Fact or Fiction:fact or fiction').
card_rarity('fact or fiction'/'VMA', 'Uncommon').
card_artist('fact or fiction'/'VMA', 'Matt Cavotta').
card_number('fact or fiction'/'VMA', '67').
card_flavor_text('fact or fiction'/'VMA', '\"Try to pretend like you understand what\'s important.\"').
card_multiverse_id('fact or fiction'/'VMA', '382930').

card_in_set('fallen askari', 'VMA').
card_original_type('fallen askari'/'VMA', 'Creature — Human Knight').
card_original_text('fallen askari'/'VMA', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)\nFallen Askari can\'t block.').
card_image_name('fallen askari'/'VMA', 'fallen askari').
card_uid('fallen askari'/'VMA', 'VMA:Fallen Askari:fallen askari').
card_rarity('fallen askari'/'VMA', 'Common').
card_artist('fallen askari'/'VMA', 'Adrian Smith').
card_number('fallen askari'/'VMA', '119').
card_flavor_text('fallen askari'/'VMA', 'In troubled times, there are few greater sorrows than a wayward savior.').
card_multiverse_id('fallen askari'/'VMA', '382931').

card_in_set('falter', 'VMA').
card_original_type('falter'/'VMA', 'Instant').
card_original_text('falter'/'VMA', 'Creatures without flying can\'t block this turn.').
card_image_name('falter'/'VMA', 'falter').
card_uid('falter'/'VMA', 'VMA:Falter:falter').
card_rarity('falter'/'VMA', 'Common').
card_artist('falter'/'VMA', 'Mike Raabe').
card_number('falter'/'VMA', '158').
card_flavor_text('falter'/'VMA', 'Like a sleeping dragon, Shiv stirs and groans at times.').
card_multiverse_id('falter'/'VMA', '382932').

card_in_set('famine', 'VMA').
card_original_type('famine'/'VMA', 'Sorcery').
card_original_text('famine'/'VMA', 'Famine deals 3 damage to each creature and each player.').
card_image_name('famine'/'VMA', 'famine').
card_uid('famine'/'VMA', 'VMA:Famine:famine').
card_rarity('famine'/'VMA', 'Uncommon').
card_artist('famine'/'VMA', 'Karla Ortiz').
card_number('famine'/'VMA', '120').
card_flavor_text('famine'/'VMA', '\"Better to starve to death than be bored to death.\"\n—Liliana Vess').
card_multiverse_id('famine'/'VMA', '382933').

card_in_set('fastbond', 'VMA').
card_original_type('fastbond'/'VMA', 'Enchantment').
card_original_text('fastbond'/'VMA', 'You may play any number of lands on each of your turns.\nWhenever you play a land, if it wasn\'t the first land you played this turn, Fastbond deals 1 damage to you.').
card_image_name('fastbond'/'VMA', 'fastbond').
card_uid('fastbond'/'VMA', 'VMA:Fastbond:fastbond').
card_rarity('fastbond'/'VMA', 'Mythic Rare').
card_artist('fastbond'/'VMA', 'Nils Hamm').
card_number('fastbond'/'VMA', '209').
card_multiverse_id('fastbond'/'VMA', '382934').

card_in_set('fireblast', 'VMA').
card_original_type('fireblast'/'VMA', 'Instant').
card_original_text('fireblast'/'VMA', 'You may sacrifice two Mountains rather than pay Fireblast\'s mana cost.\nFireblast deals 4 damage to target creature or player.').
card_image_name('fireblast'/'VMA', 'fireblast').
card_uid('fireblast'/'VMA', 'VMA:Fireblast:fireblast').
card_rarity('fireblast'/'VMA', 'Uncommon').
card_artist('fireblast'/'VMA', 'Mike Bierek').
card_number('fireblast'/'VMA', '159').
card_flavor_text('fireblast'/'VMA', 'Embermages aren\'t well known for their diplomatic skills.').
card_multiverse_id('fireblast'/'VMA', '382935').

card_in_set('fires of yavimaya', 'VMA').
card_original_type('fires of yavimaya'/'VMA', 'Enchantment').
card_original_text('fires of yavimaya'/'VMA', 'Creatures you control have haste.\nSacrifice Fires of Yavimaya: Target creature gets +2/+2 until end of turn.').
card_image_name('fires of yavimaya'/'VMA', 'fires of yavimaya').
card_uid('fires of yavimaya'/'VMA', 'VMA:Fires of Yavimaya:fires of yavimaya').
card_rarity('fires of yavimaya'/'VMA', 'Uncommon').
card_artist('fires of yavimaya'/'VMA', 'Izzy').
card_number('fires of yavimaya'/'VMA', '252').
card_flavor_text('fires of yavimaya'/'VMA', 'Yavimaya lights the quickest path to battle.').
card_multiverse_id('fires of yavimaya'/'VMA', '382936').

card_in_set('flametongue kavu', 'VMA').
card_original_type('flametongue kavu'/'VMA', 'Creature — Kavu').
card_original_text('flametongue kavu'/'VMA', 'When Flametongue Kavu enters the battlefield, it deals 4 damage to target creature.').
card_image_name('flametongue kavu'/'VMA', 'flametongue kavu').
card_uid('flametongue kavu'/'VMA', 'VMA:Flametongue Kavu:flametongue kavu').
card_rarity('flametongue kavu'/'VMA', 'Uncommon').
card_artist('flametongue kavu'/'VMA', 'Slawomir Maniak').
card_number('flametongue kavu'/'VMA', '160').
card_flavor_text('flametongue kavu'/'VMA', '\"For dim-witted, thick-skulled genetic mutants, they have pretty good aim.\"\n—Sisay, captain of the Weatherlight').
card_multiverse_id('flametongue kavu'/'VMA', '382937').

card_in_set('fledgling djinn', 'VMA').
card_original_type('fledgling djinn'/'VMA', 'Creature — Djinn').
card_original_text('fledgling djinn'/'VMA', 'Flying\nAt the beginning of your upkeep, Fledgling Djinn deals 1 damage to you.').
card_image_name('fledgling djinn'/'VMA', 'fledgling djinn').
card_uid('fledgling djinn'/'VMA', 'VMA:Fledgling Djinn:fledgling djinn').
card_rarity('fledgling djinn'/'VMA', 'Common').
card_artist('fledgling djinn'/'VMA', 'Thomas Gianni').
card_number('fledgling djinn'/'VMA', '121').
card_flavor_text('fledgling djinn'/'VMA', '\"The young can be quite dangerous. Trust me, I should know.\"\n—Ertai, wizard adept').
card_multiverse_id('fledgling djinn'/'VMA', '382938').

card_in_set('flood plain', 'VMA').
card_original_type('flood plain'/'VMA', 'Land').
card_original_text('flood plain'/'VMA', 'Flood Plain enters the battlefield tapped.\n{T}, Sacrifice Flood Plain: Search your library for a Plains or Island card and put it onto the battlefield. Then shuffle your library.').
card_image_name('flood plain'/'VMA', 'flood plain').
card_uid('flood plain'/'VMA', 'VMA:Flood Plain:flood plain').
card_rarity('flood plain'/'VMA', 'Uncommon').
card_artist('flood plain'/'VMA', 'Pat Lewis').
card_number('flood plain'/'VMA', '296').
card_multiverse_id('flood plain'/'VMA', '382939').

card_in_set('flowstone hellion', 'VMA').
card_original_type('flowstone hellion'/'VMA', 'Creature — Hellion Beast').
card_original_text('flowstone hellion'/'VMA', 'Haste\n{0}: Flowstone Hellion gets +1/-1 until end of turn.').
card_image_name('flowstone hellion'/'VMA', 'flowstone hellion').
card_uid('flowstone hellion'/'VMA', 'VMA:Flowstone Hellion:flowstone hellion').
card_rarity('flowstone hellion'/'VMA', 'Uncommon').
card_artist('flowstone hellion'/'VMA', 'Daren Bader').
card_number('flowstone hellion'/'VMA', '161').
card_flavor_text('flowstone hellion'/'VMA', 'Volrath leaves no stone untrained.').
card_multiverse_id('flowstone hellion'/'VMA', '382940').

card_in_set('flowstone sculpture', 'VMA').
card_original_type('flowstone sculpture'/'VMA', 'Artifact Creature — Shapeshifter').
card_original_text('flowstone sculpture'/'VMA', '{2}, Discard a card: Put a +1/+1 counter on Flowstone Sculpture or Flowstone Sculpture gains flying, first strike, or trample. (This effect lasts indefinitely.)').
card_image_name('flowstone sculpture'/'VMA', 'flowstone sculpture').
card_uid('flowstone sculpture'/'VMA', 'VMA:Flowstone Sculpture:flowstone sculpture').
card_rarity('flowstone sculpture'/'VMA', 'Rare').
card_artist('flowstone sculpture'/'VMA', 'Hannibal King').
card_number('flowstone sculpture'/'VMA', '268').
card_flavor_text('flowstone sculpture'/'VMA', 'The sculptor, weary of his work, created art that would finish itself.').
card_multiverse_id('flowstone sculpture'/'VMA', '382941').

card_in_set('flusterstorm', 'VMA').
card_original_type('flusterstorm'/'VMA', 'Instant').
card_original_text('flusterstorm'/'VMA', 'Counter target instant or sorcery spell unless its controller pays {1}.\nStorm (When you cast this spell, copy it for each spell cast before it this turn. You may choose new targets for the copies.)').
card_image_name('flusterstorm'/'VMA', 'flusterstorm').
card_uid('flusterstorm'/'VMA', 'VMA:Flusterstorm:flusterstorm').
card_rarity('flusterstorm'/'VMA', 'Rare').
card_artist('flusterstorm'/'VMA', 'Erica Yang').
card_number('flusterstorm'/'VMA', '68').
card_multiverse_id('flusterstorm'/'VMA', '382942').

card_in_set('force of will', 'VMA').
card_original_type('force of will'/'VMA', 'Instant').
card_original_text('force of will'/'VMA', 'You may pay 1 life and exile a blue card from your hand rather than pay Force of Will\'s mana cost.\nCounter target spell.').
card_image_name('force of will'/'VMA', 'force of will').
card_uid('force of will'/'VMA', 'VMA:Force of Will:force of will').
card_rarity('force of will'/'VMA', 'Rare').
card_artist('force of will'/'VMA', 'Matt Stewart').
card_number('force of will'/'VMA', '69').
card_multiverse_id('force of will'/'VMA', '382943').

card_in_set('forgotten cave', 'VMA').
card_original_type('forgotten cave'/'VMA', 'Land').
card_original_text('forgotten cave'/'VMA', 'Forgotten Cave enters the battlefield tapped.\n{T}: Add {R} to your mana pool.\nCycling {R} ({R}, Discard this card: Draw a card.)').
card_image_name('forgotten cave'/'VMA', 'forgotten cave').
card_uid('forgotten cave'/'VMA', 'VMA:Forgotten Cave:forgotten cave').
card_rarity('forgotten cave'/'VMA', 'Common').
card_artist('forgotten cave'/'VMA', 'Noah Bradley').
card_number('forgotten cave'/'VMA', '297').
card_multiverse_id('forgotten cave'/'VMA', '382944').

card_in_set('frantic search', 'VMA').
card_original_type('frantic search'/'VMA', 'Instant').
card_original_text('frantic search'/'VMA', 'Draw two cards, then discard two cards. Untap up to three lands.').
card_image_name('frantic search'/'VMA', 'frantic search').
card_uid('frantic search'/'VMA', 'VMA:Frantic Search:frantic search').
card_rarity('frantic search'/'VMA', 'Common').
card_artist('frantic search'/'VMA', 'Jeff Miracola').
card_number('frantic search'/'VMA', '70').
card_flavor_text('frantic search'/'VMA', 'Motivation was high in the academy once students realized flunking their exams could kill them.').
card_multiverse_id('frantic search'/'VMA', '382945').

card_in_set('future sight', 'VMA').
card_original_type('future sight'/'VMA', 'Enchantment').
card_original_text('future sight'/'VMA', 'Play with the top card of your library revealed.\nYou may play the top card of your library.').
card_image_name('future sight'/'VMA', 'future sight').
card_uid('future sight'/'VMA', 'VMA:Future Sight:future sight').
card_rarity('future sight'/'VMA', 'Rare').
card_artist('future sight'/'VMA', 'Dan Scott').
card_number('future sight'/'VMA', '71').
card_multiverse_id('future sight'/'VMA', '382946').

card_in_set('fyndhorn elves', 'VMA').
card_original_type('fyndhorn elves'/'VMA', 'Creature — Elf Druid').
card_original_text('fyndhorn elves'/'VMA', '{T}: Add {G} to your mana pool.').
card_image_name('fyndhorn elves'/'VMA', 'fyndhorn elves').
card_uid('fyndhorn elves'/'VMA', 'VMA:Fyndhorn Elves:fyndhorn elves').
card_rarity('fyndhorn elves'/'VMA', 'Common').
card_artist('fyndhorn elves'/'VMA', 'Igor Kieryluk').
card_number('fyndhorn elves'/'VMA', '210').
card_flavor_text('fyndhorn elves'/'VMA', '\"We could no more abandon the forest than the stars could abandon the night sky.\"').
card_multiverse_id('fyndhorn elves'/'VMA', '382947').

card_in_set('gaea\'s embrace', 'VMA').
card_original_type('gaea\'s embrace'/'VMA', 'Enchantment — Aura').
card_original_text('gaea\'s embrace'/'VMA', 'Enchant creature\nEnchanted creature gets +3/+3 and has trample.\n{G}: Regenerate enchanted creature.').
card_image_name('gaea\'s embrace'/'VMA', 'gaea\'s embrace').
card_uid('gaea\'s embrace'/'VMA', 'VMA:Gaea\'s Embrace:gaea\'s embrace').
card_rarity('gaea\'s embrace'/'VMA', 'Uncommon').
card_artist('gaea\'s embrace'/'VMA', 'Paolo Parente').
card_number('gaea\'s embrace'/'VMA', '211').
card_flavor_text('gaea\'s embrace'/'VMA', 'The forest rose to the battle, not to save the people, but to save itself.').
card_multiverse_id('gaea\'s embrace'/'VMA', '382948').

card_in_set('gamble', 'VMA').
card_original_type('gamble'/'VMA', 'Sorcery').
card_original_text('gamble'/'VMA', 'Search your library for a card, put that card into your hand, discard a card at random, then shuffle your library.').
card_image_name('gamble'/'VMA', 'gamble').
card_uid('gamble'/'VMA', 'VMA:Gamble:gamble').
card_rarity('gamble'/'VMA', 'Rare').
card_artist('gamble'/'VMA', 'Andrew Goldhawk').
card_number('gamble'/'VMA', '162').
card_flavor_text('gamble'/'VMA', 'When you\'ve got nothing, you might as well trade it for something else.').
card_multiverse_id('gamble'/'VMA', '382949').

card_in_set('genesis', 'VMA').
card_original_type('genesis'/'VMA', 'Creature — Incarnation').
card_original_text('genesis'/'VMA', 'At the beginning of your upkeep, if Genesis is in your graveyard, you may pay {2}{G}. If you do, return target creature card from your graveyard to your hand.').
card_image_name('genesis'/'VMA', 'genesis').
card_uid('genesis'/'VMA', 'VMA:Genesis:genesis').
card_rarity('genesis'/'VMA', 'Rare').
card_artist('genesis'/'VMA', 'Mark Zug').
card_number('genesis'/'VMA', '212').
card_flavor_text('genesis'/'VMA', '\"First through the Riftstone was Genesis—and the world was lifeless no more.\"\n—Scroll of Beginnings').
card_multiverse_id('genesis'/'VMA', '382950').

card_in_set('gerrard\'s battle cry', 'VMA').
card_original_type('gerrard\'s battle cry'/'VMA', 'Enchantment').
card_original_text('gerrard\'s battle cry'/'VMA', '{2}{W}: Creatures you control get +1/+1 until end of turn.').
card_image_name('gerrard\'s battle cry'/'VMA', 'gerrard\'s battle cry').
card_uid('gerrard\'s battle cry'/'VMA', 'VMA:Gerrard\'s Battle Cry:gerrard\'s battle cry').
card_rarity('gerrard\'s battle cry'/'VMA', 'Rare').
card_artist('gerrard\'s battle cry'/'VMA', 'Val Mayerik').
card_number('gerrard\'s battle cry'/'VMA', '28').
card_flavor_text('gerrard\'s battle cry'/'VMA', 'Gerrard grinned and drew his sword. \"This won\'t be a fair fight,\" he called to his crew. \"They should have brought a second ship!\"').
card_multiverse_id('gerrard\'s battle cry'/'VMA', '382951').

card_in_set('giant mantis', 'VMA').
card_original_type('giant mantis'/'VMA', 'Creature — Insect').
card_original_text('giant mantis'/'VMA', 'Reach (This creature can block creatures with flying.)').
card_image_name('giant mantis'/'VMA', 'giant mantis').
card_uid('giant mantis'/'VMA', 'VMA:Giant Mantis:giant mantis').
card_rarity('giant mantis'/'VMA', 'Common').
card_artist('giant mantis'/'VMA', 'Randy Gallegos').
card_number('giant mantis'/'VMA', '213').
card_flavor_text('giant mantis'/'VMA', '\"I hate insects of every sort. The only mercy is that they are generally small.\"\n—Mwani, Mtenda goatherd').
card_multiverse_id('giant mantis'/'VMA', '382952').

card_in_set('giant strength', 'VMA').
card_original_type('giant strength'/'VMA', 'Enchantment — Aura').
card_original_text('giant strength'/'VMA', 'Enchant creature\nEnchanted creature gets +2/+2.').
card_image_name('giant strength'/'VMA', 'giant strength').
card_uid('giant strength'/'VMA', 'VMA:Giant Strength:giant strength').
card_rarity('giant strength'/'VMA', 'Common').
card_artist('giant strength'/'VMA', 'Pete Venters').
card_number('giant strength'/'VMA', '163').
card_flavor_text('giant strength'/'VMA', '\"Crovax seems filled only with shadows,\" thought Orim. \"Where does his strength come from?\"').
card_multiverse_id('giant strength'/'VMA', '382953').

card_in_set('gigapede', 'VMA').
card_original_type('gigapede'/'VMA', 'Creature — Insect').
card_original_text('gigapede'/'VMA', 'Shroud (This creature can\'t be the target of spells or abilities.)\nAt the beginning of your upkeep, if Gigapede is in your graveyard, you may discard a card. If you do, return Gigapede to your hand.').
card_image_name('gigapede'/'VMA', 'gigapede').
card_uid('gigapede'/'VMA', 'VMA:Gigapede:gigapede').
card_rarity('gigapede'/'VMA', 'Rare').
card_artist('gigapede'/'VMA', 'Glen Angus').
card_number('gigapede'/'VMA', '214').
card_multiverse_id('gigapede'/'VMA', '382954').

card_in_set('gilded light', 'VMA').
card_original_type('gilded light'/'VMA', 'Instant').
card_original_text('gilded light'/'VMA', 'You gain shroud until end of turn. (You can\'t be the target of spells or abilities.)\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_image_name('gilded light'/'VMA', 'gilded light').
card_uid('gilded light'/'VMA', 'VMA:Gilded Light:gilded light').
card_rarity('gilded light'/'VMA', 'Common').
card_artist('gilded light'/'VMA', 'John Avon').
card_number('gilded light'/'VMA', '29').
card_flavor_text('gilded light'/'VMA', '\"Whoever survives the first blow lives to land the second.\"').
card_multiverse_id('gilded light'/'VMA', '382955').

card_in_set('goblin commando', 'VMA').
card_original_type('goblin commando'/'VMA', 'Creature — Goblin').
card_original_text('goblin commando'/'VMA', 'When Goblin Commando enters the battlefield, it deals 2 damage to target creature.').
card_image_name('goblin commando'/'VMA', 'goblin commando').
card_uid('goblin commando'/'VMA', 'VMA:Goblin Commando:goblin commando').
card_rarity('goblin commando'/'VMA', 'Common').
card_artist('goblin commando'/'VMA', 'Todd Lockwood').
card_number('goblin commando'/'VMA', '164').
card_flavor_text('goblin commando'/'VMA', 'With a commando around, somebody\'s gonna get hurt.').
card_multiverse_id('goblin commando'/'VMA', '382956').

card_in_set('goblin general', 'VMA').
card_original_type('goblin general'/'VMA', 'Creature — Goblin Warrior').
card_original_text('goblin general'/'VMA', 'Whenever Goblin General attacks, Goblin creatures you control get +1/+1 until end of turn.').
card_image_name('goblin general'/'VMA', 'goblin general').
card_uid('goblin general'/'VMA', 'VMA:Goblin General:goblin general').
card_rarity('goblin general'/'VMA', 'Common').
card_artist('goblin general'/'VMA', 'Keith Parkinson').
card_number('goblin general'/'VMA', '165').
card_flavor_text('goblin general'/'VMA', 'Lead, follow, or run around like crazy.').
card_multiverse_id('goblin general'/'VMA', '382957').

card_in_set('goblin goon', 'VMA').
card_original_type('goblin goon'/'VMA', 'Creature — Goblin Mutant').
card_original_text('goblin goon'/'VMA', 'Goblin Goon can\'t attack unless you control more creatures than defending player.\nGoblin Goon can\'t block unless you control more creatures than attacking player.').
card_image_name('goblin goon'/'VMA', 'goblin goon').
card_uid('goblin goon'/'VMA', 'VMA:Goblin Goon:goblin goon').
card_rarity('goblin goon'/'VMA', 'Uncommon').
card_artist('goblin goon'/'VMA', 'Greg Staples').
card_number('goblin goon'/'VMA', '166').
card_flavor_text('goblin goon'/'VMA', 'Giant-sized body. Goblin-sized brain.').
card_multiverse_id('goblin goon'/'VMA', '382958').

card_in_set('goblin lackey', 'VMA').
card_original_type('goblin lackey'/'VMA', 'Creature — Goblin').
card_original_text('goblin lackey'/'VMA', 'Whenever Goblin Lackey deals damage to a player, you may put a Goblin permanent card from your hand onto the battlefield.').
card_image_name('goblin lackey'/'VMA', 'goblin lackey').
card_uid('goblin lackey'/'VMA', 'VMA:Goblin Lackey:goblin lackey').
card_rarity('goblin lackey'/'VMA', 'Rare').
card_artist('goblin lackey'/'VMA', 'Christopher Moeller').
card_number('goblin lackey'/'VMA', '167').
card_flavor_text('goblin lackey'/'VMA', 'At the first sign of enemy scouts, Jula grinned and strapped on his diplomacy helmet.').
card_multiverse_id('goblin lackey'/'VMA', '382959').

card_in_set('goblin matron', 'VMA').
card_original_type('goblin matron'/'VMA', 'Creature — Goblin').
card_original_text('goblin matron'/'VMA', 'When Goblin Matron enters the battlefield, you may search your library for a Goblin card, reveal that card, and put it into your hand. If you do, shuffle your library.').
card_image_name('goblin matron'/'VMA', 'goblin matron').
card_uid('goblin matron'/'VMA', 'VMA:Goblin Matron:goblin matron').
card_rarity('goblin matron'/'VMA', 'Common').
card_artist('goblin matron'/'VMA', 'Dan Frazier').
card_number('goblin matron'/'VMA', '168').
card_multiverse_id('goblin matron'/'VMA', '382960').

card_in_set('goblin patrol', 'VMA').
card_original_type('goblin patrol'/'VMA', 'Creature — Goblin').
card_original_text('goblin patrol'/'VMA', 'Echo {R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)').
card_image_name('goblin patrol'/'VMA', 'goblin patrol').
card_uid('goblin patrol'/'VMA', 'VMA:Goblin Patrol:goblin patrol').
card_rarity('goblin patrol'/'VMA', 'Common').
card_artist('goblin patrol'/'VMA', 'Greg Staples').
card_number('goblin patrol'/'VMA', '169').
card_flavor_text('goblin patrol'/'VMA', '\"Take the sharp metal stick and make a lotta holes.\"\n—Jula, goblin raider').
card_multiverse_id('goblin patrol'/'VMA', '382961').

card_in_set('goblin piledriver', 'VMA').
card_original_type('goblin piledriver'/'VMA', 'Creature — Goblin Warrior').
card_original_text('goblin piledriver'/'VMA', 'Protection from blue\nWhenever Goblin Piledriver attacks, it gets +2/+0 until end of turn for each other attacking Goblin.').
card_image_name('goblin piledriver'/'VMA', 'goblin piledriver').
card_uid('goblin piledriver'/'VMA', 'VMA:Goblin Piledriver:goblin piledriver').
card_rarity('goblin piledriver'/'VMA', 'Rare').
card_artist('goblin piledriver'/'VMA', 'Dave Kendall').
card_number('goblin piledriver'/'VMA', '170').
card_multiverse_id('goblin piledriver'/'VMA', '382962').

card_in_set('goblin ringleader', 'VMA').
card_original_type('goblin ringleader'/'VMA', 'Creature — Goblin').
card_original_text('goblin ringleader'/'VMA', 'Haste\nWhen Goblin Ringleader enters the battlefield, reveal the top four cards of your library. Put all Goblin cards revealed this way into your hand and the rest on the bottom of your library in any order.').
card_image_name('goblin ringleader'/'VMA', 'goblin ringleader').
card_uid('goblin ringleader'/'VMA', 'VMA:Goblin Ringleader:goblin ringleader').
card_rarity('goblin ringleader'/'VMA', 'Uncommon').
card_artist('goblin ringleader'/'VMA', 'Gabor Szikszai').
card_number('goblin ringleader'/'VMA', '171').
card_multiverse_id('goblin ringleader'/'VMA', '382963').

card_in_set('goblin settler', 'VMA').
card_original_type('goblin settler'/'VMA', 'Creature — Goblin').
card_original_text('goblin settler'/'VMA', 'When Goblin Settler enters the battlefield, destroy target land.').
card_image_name('goblin settler'/'VMA', 'goblin settler').
card_uid('goblin settler'/'VMA', 'VMA:Goblin Settler:goblin settler').
card_rarity('goblin settler'/'VMA', 'Uncommon').
card_artist('goblin settler'/'VMA', 'Carl Critchlow').
card_number('goblin settler'/'VMA', '172').
card_flavor_text('goblin settler'/'VMA', 'Be it ever so crumbled, there\'s no place like home.').
card_multiverse_id('goblin settler'/'VMA', '382964').

card_in_set('goblin trenches', 'VMA').
card_original_type('goblin trenches'/'VMA', 'Enchantment').
card_original_text('goblin trenches'/'VMA', '{2}, Sacrifice a land: Put two 1/1 red and white Goblin Soldier creature tokens onto the battlefield.').
card_image_name('goblin trenches'/'VMA', 'goblin trenches').
card_uid('goblin trenches'/'VMA', 'VMA:Goblin Trenches:goblin trenches').
card_rarity('goblin trenches'/'VMA', 'Uncommon').
card_artist('goblin trenches'/'VMA', 'Wayne England').
card_number('goblin trenches'/'VMA', '253').
card_flavor_text('goblin trenches'/'VMA', 'The ground rose and formed into thousands of tiny warriors. This fight was far from over.').
card_multiverse_id('goblin trenches'/'VMA', '382965').

card_in_set('goblin warchief', 'VMA').
card_original_type('goblin warchief'/'VMA', 'Creature — Goblin Warrior').
card_original_text('goblin warchief'/'VMA', 'Goblin spells you cast cost {1} less to cast.\nGoblin creatures you control have haste.').
card_image_name('goblin warchief'/'VMA', 'goblin warchief').
card_uid('goblin warchief'/'VMA', 'VMA:Goblin Warchief:goblin warchief').
card_rarity('goblin warchief'/'VMA', 'Uncommon').
card_artist('goblin warchief'/'VMA', 'Greg & Tim Hildebrandt').
card_number('goblin warchief'/'VMA', '173').
card_flavor_text('goblin warchief'/'VMA', 'They poured from the Skirk Ridge like lava, burning and devouring everything in their path.').
card_multiverse_id('goblin warchief'/'VMA', '382966').

card_in_set('grand coliseum', 'VMA').
card_original_type('grand coliseum'/'VMA', 'Land').
card_original_text('grand coliseum'/'VMA', 'Grand Coliseum enters the battlefield tapped.\n{T}: Add {1} to your mana pool.\n{T}: Add one mana of any color to your mana pool. Grand Coliseum deals 1 damage to you.').
card_image_name('grand coliseum'/'VMA', 'grand coliseum').
card_uid('grand coliseum'/'VMA', 'VMA:Grand Coliseum:grand coliseum').
card_rarity('grand coliseum'/'VMA', 'Rare').
card_artist('grand coliseum'/'VMA', 'Carl Critchlow').
card_number('grand coliseum'/'VMA', '298').
card_multiverse_id('grand coliseum'/'VMA', '382967').

card_in_set('grasslands', 'VMA').
card_original_type('grasslands'/'VMA', 'Land').
card_original_text('grasslands'/'VMA', 'Grasslands enters the battlefield tapped.\n{T}, Sacrifice Grasslands: Search your library for a Forest or Plains card and put it onto the battlefield. Then shuffle your library.').
card_image_name('grasslands'/'VMA', 'grasslands').
card_uid('grasslands'/'VMA', 'VMA:Grasslands:grasslands').
card_rarity('grasslands'/'VMA', 'Uncommon').
card_artist('grasslands'/'VMA', 'John Avon').
card_number('grasslands'/'VMA', '299').
card_multiverse_id('grasslands'/'VMA', '382968').

card_in_set('grenzo, dungeon warden', 'VMA').
card_original_type('grenzo, dungeon warden'/'VMA', 'Legendary Creature — Goblin Rogue').
card_original_text('grenzo, dungeon warden'/'VMA', 'Grenzo, Dungeon Warden enters the battlefield with X +1/+1 counters on it.\n{2}: Put the bottom card of your library into your graveyard. If it\'s a creature card with power less than or equal to Grenzo\'s power, put it onto the battlefield.').
card_image_name('grenzo, dungeon warden'/'VMA', 'grenzo, dungeon warden').
card_uid('grenzo, dungeon warden'/'VMA', 'VMA:Grenzo, Dungeon Warden:grenzo, dungeon warden').
card_rarity('grenzo, dungeon warden'/'VMA', 'Rare').
card_artist('grenzo, dungeon warden'/'VMA', 'Lucas Graciano').
card_number('grenzo, dungeon warden'/'VMA', '254').
card_multiverse_id('grenzo, dungeon warden'/'VMA', '382969').

card_in_set('grizzly fate', 'VMA').
card_original_type('grizzly fate'/'VMA', 'Sorcery').
card_original_text('grizzly fate'/'VMA', 'Put two 2/2 green Bear creature tokens onto the battlefield.\nThreshold — Put four 2/2 green Bear creature tokens onto the battlefield instead if seven or more cards are in your graveyard.\nFlashback {5}{G}{G} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('grizzly fate'/'VMA', 'grizzly fate').
card_uid('grizzly fate'/'VMA', 'VMA:Grizzly Fate:grizzly fate').
card_rarity('grizzly fate'/'VMA', 'Uncommon').
card_artist('grizzly fate'/'VMA', 'Dave Dorman').
card_number('grizzly fate'/'VMA', '215').
card_multiverse_id('grizzly fate'/'VMA', '382970').

card_in_set('gush', 'VMA').
card_original_type('gush'/'VMA', 'Instant').
card_original_text('gush'/'VMA', 'You may return two Islands you control to their owner\'s hand rather than pay Gush\'s mana cost.\nDraw two cards.').
card_image_name('gush'/'VMA', 'gush').
card_uid('gush'/'VMA', 'VMA:Gush:gush').
card_rarity('gush'/'VMA', 'Uncommon').
card_artist('gush'/'VMA', 'Kev Walker').
card_number('gush'/'VMA', '72').
card_flavor_text('gush'/'VMA', 'Don\'t trust your secrets to the sea.').
card_multiverse_id('gush'/'VMA', '382971').

card_in_set('gustcloak harrier', 'VMA').
card_original_type('gustcloak harrier'/'VMA', 'Creature — Bird Soldier').
card_original_text('gustcloak harrier'/'VMA', 'Flying\nWhenever Gustcloak Harrier becomes blocked, you may untap it and remove it from combat.').
card_image_name('gustcloak harrier'/'VMA', 'gustcloak harrier').
card_uid('gustcloak harrier'/'VMA', 'VMA:Gustcloak Harrier:gustcloak harrier').
card_rarity('gustcloak harrier'/'VMA', 'Common').
card_artist('gustcloak harrier'/'VMA', 'Dan Frazier').
card_number('gustcloak harrier'/'VMA', '30').
card_flavor_text('gustcloak harrier'/'VMA', 'Banking steeply, the aven streaked toward the ground—and vanished.').
card_multiverse_id('gustcloak harrier'/'VMA', '382972').

card_in_set('hermit druid', 'VMA').
card_original_type('hermit druid'/'VMA', 'Creature — Human Druid').
card_original_text('hermit druid'/'VMA', '{G}, {T}: Reveal cards from the top of your library until you reveal a basic land card. Put that card into your hand and all other cards revealed this way into your graveyard.').
card_image_name('hermit druid'/'VMA', 'hermit druid').
card_uid('hermit druid'/'VMA', 'VMA:Hermit Druid:hermit druid').
card_rarity('hermit druid'/'VMA', 'Rare').
card_artist('hermit druid'/'VMA', 'Heather Hudson').
card_number('hermit druid'/'VMA', '216').
card_flavor_text('hermit druid'/'VMA', 'Seeking the company of plants ensures that your wits will go to seed.').
card_multiverse_id('hermit druid'/'VMA', '382973').

card_in_set('high tide', 'VMA').
card_original_type('high tide'/'VMA', 'Instant').
card_original_text('high tide'/'VMA', 'Until end of turn, whenever a player taps an Island for mana, that player adds {U} to his or her mana pool (in addition to the mana the land produces).').
card_image_name('high tide'/'VMA', 'high tide').
card_uid('high tide'/'VMA', 'VMA:High Tide:high tide').
card_rarity('high tide'/'VMA', 'Uncommon').
card_artist('high tide'/'VMA', 'Eric Deschamps').
card_number('high tide'/'VMA', '73').
card_flavor_text('high tide'/'VMA', '\"There\'s nothing like the high seas to clear your head or escape pursuers.\"').
card_multiverse_id('high tide'/'VMA', '382974').

card_in_set('hulking goblin', 'VMA').
card_original_type('hulking goblin'/'VMA', 'Creature — Goblin').
card_original_text('hulking goblin'/'VMA', 'Hulking Goblin can\'t block.').
card_image_name('hulking goblin'/'VMA', 'hulking goblin').
card_uid('hulking goblin'/'VMA', 'VMA:Hulking Goblin:hulking goblin').
card_rarity('hulking goblin'/'VMA', 'Common').
card_artist('hulking goblin'/'VMA', 'Pete Venters').
card_number('hulking goblin'/'VMA', '174').
card_flavor_text('hulking goblin'/'VMA', 'The bigger they are, the harder they avoid work.').
card_multiverse_id('hulking goblin'/'VMA', '382975').

card_in_set('hymn to tourach', 'VMA').
card_original_type('hymn to tourach'/'VMA', 'Sorcery').
card_original_text('hymn to tourach'/'VMA', 'Target player discards two cards at random.').
card_image_name('hymn to tourach'/'VMA', 'hymn to tourach').
card_uid('hymn to tourach'/'VMA', 'VMA:Hymn to Tourach:hymn to tourach').
card_rarity('hymn to tourach'/'VMA', 'Uncommon').
card_artist('hymn to tourach'/'VMA', 'Greg Staples').
card_number('hymn to tourach'/'VMA', '122').
card_flavor_text('hymn to tourach'/'VMA', 'The priests plead for your anguish and pray for your despair.').
card_multiverse_id('hymn to tourach'/'VMA', '382976').

card_in_set('ichorid', 'VMA').
card_original_type('ichorid'/'VMA', 'Creature — Horror').
card_original_text('ichorid'/'VMA', 'Haste\nAt the beginning of the end step, sacrifice Ichorid.\nAt the beginning of your upkeep, if Ichorid is in your graveyard, you may exile a black creature card other than Ichorid from your graveyard. If you do, return Ichorid to the battlefield.').
card_image_name('ichorid'/'VMA', 'ichorid').
card_uid('ichorid'/'VMA', 'VMA:Ichorid:ichorid').
card_rarity('ichorid'/'VMA', 'Rare').
card_artist('ichorid'/'VMA', 'rk post').
card_number('ichorid'/'VMA', '123').
card_multiverse_id('ichorid'/'VMA', '382977').

card_in_set('ivory tower', 'VMA').
card_original_type('ivory tower'/'VMA', 'Artifact').
card_original_text('ivory tower'/'VMA', 'At the beginning of your upkeep, you gain X life, where X is the number of cards in your hand minus 4.').
card_image_name('ivory tower'/'VMA', 'ivory tower').
card_uid('ivory tower'/'VMA', 'VMA:Ivory Tower:ivory tower').
card_rarity('ivory tower'/'VMA', 'Uncommon').
card_artist('ivory tower'/'VMA', 'Jason Chan').
card_number('ivory tower'/'VMA', '269').
card_flavor_text('ivory tower'/'VMA', 'Valuing scholarship above all else, the inhabitants of the Ivory Tower reward those who sacrifice power for knowledge.').
card_multiverse_id('ivory tower'/'VMA', '382978').

card_in_set('jace, the mind sculptor', 'VMA').
card_original_type('jace, the mind sculptor'/'VMA', 'Planeswalker — Jace').
card_original_text('jace, the mind sculptor'/'VMA', '+2: Look at the top card of target player\'s library. You may put that card on the bottom of that player\'s library.\n0: Draw three cards, then put two cards from your hand on top of your library in any order.\n-1: Return target creature to its owner\'s hand.\n-12: Exile all cards from target player\'s library, then that player shuffles his or her hand into his or her library.').
card_image_name('jace, the mind sculptor'/'VMA', 'jace, the mind sculptor').
card_uid('jace, the mind sculptor'/'VMA', 'VMA:Jace, the Mind Sculptor:jace, the mind sculptor').
card_rarity('jace, the mind sculptor'/'VMA', 'Mythic Rare').
card_artist('jace, the mind sculptor'/'VMA', 'Jason Chan').
card_number('jace, the mind sculptor'/'VMA', '74').
card_multiverse_id('jace, the mind sculptor'/'VMA', '382979').

card_in_set('jareth, leonine titan', 'VMA').
card_original_type('jareth, leonine titan'/'VMA', 'Legendary Creature — Cat Giant').
card_original_text('jareth, leonine titan'/'VMA', 'Whenever Jareth, Leonine Titan blocks, it gets +7/+7 until end of turn.\n{W}: Jareth gains protection from the color of your choice until end of turn.').
card_image_name('jareth, leonine titan'/'VMA', 'jareth, leonine titan').
card_uid('jareth, leonine titan'/'VMA', 'VMA:Jareth, Leonine Titan:jareth, leonine titan').
card_rarity('jareth, leonine titan'/'VMA', 'Rare').
card_artist('jareth, leonine titan'/'VMA', 'Daren Bader').
card_number('jareth, leonine titan'/'VMA', '31').
card_flavor_text('jareth, leonine titan'/'VMA', 'Light\'s champion in the stronghold of darkness.').
card_multiverse_id('jareth, leonine titan'/'VMA', '382980').

card_in_set('jungle wurm', 'VMA').
card_original_type('jungle wurm'/'VMA', 'Creature — Wurm').
card_original_text('jungle wurm'/'VMA', 'Whenever Jungle Wurm becomes blocked, it gets -1/-1 until end of turn for each creature blocking it beyond the first.').
card_image_name('jungle wurm'/'VMA', 'jungle wurm').
card_uid('jungle wurm'/'VMA', 'VMA:Jungle Wurm:jungle wurm').
card_rarity('jungle wurm'/'VMA', 'Common').
card_artist('jungle wurm'/'VMA', 'Tom Kyffin').
card_number('jungle wurm'/'VMA', '217').
card_flavor_text('jungle wurm'/'VMA', 'Broad as a baobab—and about as smart.').
card_multiverse_id('jungle wurm'/'VMA', '382981').

card_in_set('kaervek\'s torch', 'VMA').
card_original_type('kaervek\'s torch'/'VMA', 'Sorcery').
card_original_text('kaervek\'s torch'/'VMA', 'As long as Kaervek\'s Torch is on the stack, spells that target it cost {2} more to cast.\nKaervek\'s Torch deals X damage to target creature or player.').
card_image_name('kaervek\'s torch'/'VMA', 'kaervek\'s torch').
card_uid('kaervek\'s torch'/'VMA', 'VMA:Kaervek\'s Torch:kaervek\'s torch').
card_rarity('kaervek\'s torch'/'VMA', 'Uncommon').
card_artist('kaervek\'s torch'/'VMA', 'John Coulthart').
card_number('kaervek\'s torch'/'VMA', '175').
card_flavor_text('kaervek\'s torch'/'VMA', 'The pulsing heat of the midday Sun burns in the Lion\'s eye.\n—Stone inscription, source unknown').
card_multiverse_id('kaervek\'s torch'/'VMA', '382982').

card_in_set('karmic guide', 'VMA').
card_original_type('karmic guide'/'VMA', 'Creature — Angel Spirit').
card_original_text('karmic guide'/'VMA', 'Flying, protection from black\nEcho {3}{W}{W} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Karmic Guide enters the battlefield, return target creature card from your graveyard to the battlefield.').
card_image_name('karmic guide'/'VMA', 'karmic guide').
card_uid('karmic guide'/'VMA', 'VMA:Karmic Guide:karmic guide').
card_rarity('karmic guide'/'VMA', 'Rare').
card_artist('karmic guide'/'VMA', 'Allen Williams').
card_number('karmic guide'/'VMA', '32').
card_multiverse_id('karmic guide'/'VMA', '382983').

card_in_set('karn, silver golem', 'VMA').
card_original_type('karn, silver golem'/'VMA', 'Legendary Artifact Creature — Golem').
card_original_text('karn, silver golem'/'VMA', 'Whenever Karn, Silver Golem blocks or becomes blocked, it gets -4/+4 until end of turn.\n{1}: Target noncreature artifact becomes an artifact creature with power and toughness each equal to its converted mana cost until end of turn.').
card_image_name('karn, silver golem'/'VMA', 'karn, silver golem').
card_uid('karn, silver golem'/'VMA', 'VMA:Karn, Silver Golem:karn, silver golem').
card_rarity('karn, silver golem'/'VMA', 'Rare').
card_artist('karn, silver golem'/'VMA', 'Mark Zug').
card_number('karn, silver golem'/'VMA', '270').
card_multiverse_id('karn, silver golem'/'VMA', '382984').

card_in_set('keeneye aven', 'VMA').
card_original_type('keeneye aven'/'VMA', 'Creature — Bird Soldier').
card_original_text('keeneye aven'/'VMA', 'Flying\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_image_name('keeneye aven'/'VMA', 'keeneye aven').
card_uid('keeneye aven'/'VMA', 'VMA:Keeneye Aven:keeneye aven').
card_rarity('keeneye aven'/'VMA', 'Common').
card_artist('keeneye aven'/'VMA', 'Greg Hildebrandt').
card_number('keeneye aven'/'VMA', '75').
card_flavor_text('keeneye aven'/'VMA', '\"I have no need of a map. The continent itself guides my way.\"').
card_multiverse_id('keeneye aven'/'VMA', '382985').

card_in_set('keldon necropolis', 'VMA').
card_original_type('keldon necropolis'/'VMA', 'Legendary Land').
card_original_text('keldon necropolis'/'VMA', '{T}: Add {1} to your mana pool.\n{4}{R}, {T}, Sacrifice a creature: Keldon Necropolis deals 2 damage to target creature or player.').
card_image_name('keldon necropolis'/'VMA', 'keldon necropolis').
card_uid('keldon necropolis'/'VMA', 'VMA:Keldon Necropolis:keldon necropolis').
card_rarity('keldon necropolis'/'VMA', 'Rare').
card_artist('keldon necropolis'/'VMA', 'Franz Vohwinkel').
card_number('keldon necropolis'/'VMA', '300').
card_multiverse_id('keldon necropolis'/'VMA', '382986').

card_in_set('kezzerdrix', 'VMA').
card_original_type('kezzerdrix'/'VMA', 'Creature — Rabbit Beast').
card_original_text('kezzerdrix'/'VMA', 'First strike\nAt the beginning of your upkeep, if your opponents control no creatures, Kezzerdrix deals 4 damage to you.').
card_image_name('kezzerdrix'/'VMA', 'kezzerdrix').
card_uid('kezzerdrix'/'VMA', 'VMA:Kezzerdrix:kezzerdrix').
card_rarity('kezzerdrix'/'VMA', 'Uncommon').
card_artist('kezzerdrix'/'VMA', 'Matthew D. Wilson').
card_number('kezzerdrix'/'VMA', '124').
card_multiverse_id('kezzerdrix'/'VMA', '382987').

card_in_set('killer whale', 'VMA').
card_original_type('killer whale'/'VMA', 'Creature — Whale').
card_original_text('killer whale'/'VMA', '{U}: Killer Whale gains flying until end of turn.').
card_image_name('killer whale'/'VMA', 'killer whale').
card_uid('killer whale'/'VMA', 'VMA:Killer Whale:killer whale').
card_rarity('killer whale'/'VMA', 'Common').
card_artist('killer whale'/'VMA', 'Stephen Daniele').
card_number('killer whale'/'VMA', '76').
card_flavor_text('killer whale'/'VMA', 'Hunger is like the sea: deep, endless, and unforgiving.').
card_multiverse_id('killer whale'/'VMA', '382988').

card_in_set('kindle', 'VMA').
card_original_type('kindle'/'VMA', 'Instant').
card_original_text('kindle'/'VMA', 'Kindle deals X damage to target creature or player, where X is 2 plus the number of cards named Kindle in all graveyards.').
card_image_name('kindle'/'VMA', 'kindle').
card_uid('kindle'/'VMA', 'VMA:Kindle:kindle').
card_rarity('kindle'/'VMA', 'Common').
card_artist('kindle'/'VMA', 'Donato Giancola').
card_number('kindle'/'VMA', '176').
card_flavor_text('kindle'/'VMA', 'Hope of deliverance is scorched by the fire of futility.').
card_multiverse_id('kindle'/'VMA', '382989').

card_in_set('kjeldoran outpost', 'VMA').
card_original_type('kjeldoran outpost'/'VMA', 'Land').
card_original_text('kjeldoran outpost'/'VMA', 'If Kjeldoran Outpost would enter the battlefield, sacrifice a Plains instead. If you do, put Kjeldoran Outpost onto the battlefield. If you don\'t, put it into its owner\'s graveyard.\n{T}: Add {W} to your mana pool.\n{1}{W}, {T}: Put a 1/1 white Soldier creature token onto the battlefield.').
card_image_name('kjeldoran outpost'/'VMA', 'kjeldoran outpost').
card_uid('kjeldoran outpost'/'VMA', 'VMA:Kjeldoran Outpost:kjeldoran outpost').
card_rarity('kjeldoran outpost'/'VMA', 'Rare').
card_artist('kjeldoran outpost'/'VMA', 'Noah Bradley').
card_number('kjeldoran outpost'/'VMA', '301').
card_multiverse_id('kjeldoran outpost'/'VMA', '382990').

card_in_set('kongming, \"sleeping dragon\"', 'VMA').
card_original_type('kongming, \"sleeping dragon\"'/'VMA', 'Legendary Creature — Human Advisor').
card_original_text('kongming, \"sleeping dragon\"'/'VMA', 'Other creatures you control get +1/+1.').
card_image_name('kongming, \"sleeping dragon\"'/'VMA', 'kongming, sleeping dragon').
card_uid('kongming, \"sleeping dragon\"'/'VMA', 'VMA:Kongming, \"Sleeping Dragon\":kongming, sleeping dragon').
card_rarity('kongming, \"sleeping dragon\"'/'VMA', 'Rare').
card_artist('kongming, \"sleeping dragon\"'/'VMA', 'Gao Yan').
card_number('kongming, \"sleeping dragon\"'/'VMA', '33').
card_flavor_text('kongming, \"sleeping dragon\"'/'VMA', '\"Such a lord as this—all virtues\' height—\nHad never been, nor ever was again.\"').
card_multiverse_id('kongming, \"sleeping dragon\"'/'VMA', '382991').

card_in_set('krosan tusker', 'VMA').
card_original_type('krosan tusker'/'VMA', 'Creature — Boar Beast').
card_original_text('krosan tusker'/'VMA', 'Cycling {2}{G} ({2}{G}, Discard this card: Draw a card.)\nWhen you cycle Krosan Tusker, you may search your library for a basic land card, reveal that card, put it into your hand, then shuffle your library.').
card_image_name('krosan tusker'/'VMA', 'krosan tusker').
card_uid('krosan tusker'/'VMA', 'VMA:Krosan Tusker:krosan tusker').
card_rarity('krosan tusker'/'VMA', 'Common').
card_artist('krosan tusker'/'VMA', 'Kev Walker').
card_number('krosan tusker'/'VMA', '218').
card_multiverse_id('krosan tusker'/'VMA', '382992').

card_in_set('krosan vorine', 'VMA').
card_original_type('krosan vorine'/'VMA', 'Creature — Cat Beast').
card_original_text('krosan vorine'/'VMA', 'Provoke (When this attacks, you may have target creature defending player controls untap and block it if able.)\nKrosan Vorine can\'t be blocked by more than one creature.').
card_image_name('krosan vorine'/'VMA', 'krosan vorine').
card_uid('krosan vorine'/'VMA', 'VMA:Krosan Vorine:krosan vorine').
card_rarity('krosan vorine'/'VMA', 'Common').
card_artist('krosan vorine'/'VMA', 'Carl Critchlow').
card_number('krosan vorine'/'VMA', '219').
card_multiverse_id('krosan vorine'/'VMA', '382993').

card_in_set('krovikan sorcerer', 'VMA').
card_original_type('krovikan sorcerer'/'VMA', 'Creature — Human Wizard').
card_original_text('krovikan sorcerer'/'VMA', '{T}, Discard a nonblack card: Draw a card.\n{T}, Discard a black card: Draw two cards, then discard one of them.').
card_image_name('krovikan sorcerer'/'VMA', 'krovikan sorcerer').
card_uid('krovikan sorcerer'/'VMA', 'VMA:Krovikan Sorcerer:krovikan sorcerer').
card_rarity('krovikan sorcerer'/'VMA', 'Common').
card_artist('krovikan sorcerer'/'VMA', 'Igor Kieryluk').
card_number('krovikan sorcerer'/'VMA', '77').
card_flavor_text('krovikan sorcerer'/'VMA', '\"These sorcerers always seem to have another surprise up their sleeves.\"\n—Zur the Enchanter').
card_multiverse_id('krovikan sorcerer'/'VMA', '382994').

card_in_set('lake of the dead', 'VMA').
card_original_type('lake of the dead'/'VMA', 'Land').
card_original_text('lake of the dead'/'VMA', 'If Lake of the Dead would enter the battlefield, sacrifice a Swamp instead. If you do, put Lake of the Dead onto the battlefield. If you don\'t, put it into its owner\'s graveyard.\n{T}: Add {B} to your mana pool.\n{T}, Sacrifice a Swamp: Add {B}{B}{B}{B} to your mana pool.').
card_image_name('lake of the dead'/'VMA', 'lake of the dead').
card_uid('lake of the dead'/'VMA', 'VMA:Lake of the Dead:lake of the dead').
card_rarity('lake of the dead'/'VMA', 'Rare').
card_artist('lake of the dead'/'VMA', 'Eytan Zana').
card_number('lake of the dead'/'VMA', '302').
card_multiverse_id('lake of the dead'/'VMA', '382995').

card_in_set('laquatus\'s champion', 'VMA').
card_original_type('laquatus\'s champion'/'VMA', 'Creature — Nightmare Horror').
card_original_text('laquatus\'s champion'/'VMA', 'When Laquatus\'s Champion enters the battlefield, target player loses 6 life.\nWhen Laquatus\'s Champion leaves the battlefield, that player gains 6 life.\n{B}: Regenerate Laquatus\'s Champion.').
card_image_name('laquatus\'s champion'/'VMA', 'laquatus\'s champion').
card_uid('laquatus\'s champion'/'VMA', 'VMA:Laquatus\'s Champion:laquatus\'s champion').
card_rarity('laquatus\'s champion'/'VMA', 'Rare').
card_artist('laquatus\'s champion'/'VMA', 'Greg Staples').
card_number('laquatus\'s champion'/'VMA', '125').
card_flavor_text('laquatus\'s champion'/'VMA', 'Chainer\'s dark gift to a darker soul.').
card_multiverse_id('laquatus\'s champion'/'VMA', '382996').

card_in_set('library of alexandria', 'VMA').
card_original_type('library of alexandria'/'VMA', 'Land').
card_original_text('library of alexandria'/'VMA', '{T}: Add {1} to your mana pool.\n{T}: Draw a card. Activate this ability only if you have exactly seven cards in hand.').
card_image_name('library of alexandria'/'VMA', 'library of alexandria').
card_uid('library of alexandria'/'VMA', 'VMA:Library of Alexandria:library of alexandria').
card_rarity('library of alexandria'/'VMA', 'Mythic Rare').
card_artist('library of alexandria'/'VMA', 'Drew Baker').
card_number('library of alexandria'/'VMA', '303').
card_multiverse_id('library of alexandria'/'VMA', '382997').

card_in_set('lightning dragon', 'VMA').
card_original_type('lightning dragon'/'VMA', 'Creature — Dragon').
card_original_text('lightning dragon'/'VMA', 'Flying\nEcho {2}{R}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\n{R}: Lightning Dragon gets +1/+0 until end of turn.').
card_image_name('lightning dragon'/'VMA', 'lightning dragon').
card_uid('lightning dragon'/'VMA', 'VMA:Lightning Dragon:lightning dragon').
card_rarity('lightning dragon'/'VMA', 'Rare').
card_artist('lightning dragon'/'VMA', 'Ron Spencer').
card_number('lightning dragon'/'VMA', '177').
card_multiverse_id('lightning dragon'/'VMA', '382998').

card_in_set('lightning rift', 'VMA').
card_original_type('lightning rift'/'VMA', 'Enchantment').
card_original_text('lightning rift'/'VMA', 'Whenever a player cycles a card, you may pay {1}. If you do, Lightning Rift deals 2 damage to target creature or player.').
card_image_name('lightning rift'/'VMA', 'lightning rift').
card_uid('lightning rift'/'VMA', 'VMA:Lightning Rift:lightning rift').
card_rarity('lightning rift'/'VMA', 'Uncommon').
card_artist('lightning rift'/'VMA', 'Eric Peterson').
card_number('lightning rift'/'VMA', '178').
card_flavor_text('lightning rift'/'VMA', 'Options will cost you, but a lack of them will cost you even more.').
card_multiverse_id('lightning rift'/'VMA', '382999').

card_in_set('lion\'s eye diamond', 'VMA').
card_original_type('lion\'s eye diamond'/'VMA', 'Artifact').
card_original_text('lion\'s eye diamond'/'VMA', 'Sacrifice Lion\'s Eye Diamond, Discard your hand: Add three mana of any one color to your mana pool. Activate this ability only any time you could cast an instant.').
card_image_name('lion\'s eye diamond'/'VMA', 'lion\'s eye diamond').
card_uid('lion\'s eye diamond'/'VMA', 'VMA:Lion\'s Eye Diamond:lion\'s eye diamond').
card_rarity('lion\'s eye diamond'/'VMA', 'Mythic Rare').
card_artist('lion\'s eye diamond'/'VMA', 'Lindsey Look').
card_number('lion\'s eye diamond'/'VMA', '271').
card_flavor_text('lion\'s eye diamond'/'VMA', 'Held in the lion\'s eye\n—Zhalfirin saying meaning \"caught in the moment of crisis\"').
card_multiverse_id('lion\'s eye diamond'/'VMA', '383000').

card_in_set('living death', 'VMA').
card_original_type('living death'/'VMA', 'Sorcery').
card_original_text('living death'/'VMA', 'Each player exiles all creature cards from his or her graveyard, then sacrifices all creatures he or she controls, then puts all cards he or she exiled this way onto the battlefield.').
card_image_name('living death'/'VMA', 'living death').
card_uid('living death'/'VMA', 'VMA:Living Death:living death').
card_rarity('living death'/'VMA', 'Rare').
card_artist('living death'/'VMA', 'Mark Winters').
card_number('living death'/'VMA', '126').
card_multiverse_id('living death'/'VMA', '383001').

card_in_set('lonely sandbar', 'VMA').
card_original_type('lonely sandbar'/'VMA', 'Land').
card_original_text('lonely sandbar'/'VMA', 'Lonely Sandbar enters the battlefield tapped.\n{T}: Add {U} to your mana pool.\nCycling {U} ({U}, Discard this card: Draw a card.)').
card_image_name('lonely sandbar'/'VMA', 'lonely sandbar').
card_uid('lonely sandbar'/'VMA', 'VMA:Lonely Sandbar:lonely sandbar').
card_rarity('lonely sandbar'/'VMA', 'Common').
card_artist('lonely sandbar'/'VMA', 'Heather Hudson').
card_number('lonely sandbar'/'VMA', '304').
card_multiverse_id('lonely sandbar'/'VMA', '383002').

card_in_set('lurking evil', 'VMA').
card_original_type('lurking evil'/'VMA', 'Enchantment').
card_original_text('lurking evil'/'VMA', 'Pay half your life, rounded up: Lurking Evil becomes a 4/4 Horror creature with flying.').
card_image_name('lurking evil'/'VMA', 'lurking evil').
card_uid('lurking evil'/'VMA', 'VMA:Lurking Evil:lurking evil').
card_rarity('lurking evil'/'VMA', 'Uncommon').
card_artist('lurking evil'/'VMA', 'Scott Kirschner').
card_number('lurking evil'/'VMA', '127').
card_flavor_text('lurking evil'/'VMA', '\"Ash is our air, darkness our flesh.\"\n—Phyrexian Scriptures').
card_multiverse_id('lurking evil'/'VMA', '383003').

card_in_set('magister of worth', 'VMA').
card_original_type('magister of worth'/'VMA', 'Creature — Angel').
card_original_text('magister of worth'/'VMA', 'Flying\nWill of the council — When Magister of Worth enters the battlefield, starting with you, each player votes for grace or condemnation. If grace gets more votes, each player returns each creature card from his or her graveyard to the battlefield. If condemnation gets more votes or the vote is tied, destroy all creatures other than Magister of Worth.').
card_image_name('magister of worth'/'VMA', 'magister of worth').
card_uid('magister of worth'/'VMA', 'VMA:Magister of Worth:magister of worth').
card_rarity('magister of worth'/'VMA', 'Rare').
card_artist('magister of worth'/'VMA', 'John Stanko').
card_number('magister of worth'/'VMA', '255').
card_multiverse_id('magister of worth'/'VMA', '383004').

card_in_set('man-o\'-war', 'VMA').
card_original_type('man-o\'-war'/'VMA', 'Creature — Jellyfish').
card_original_text('man-o\'-war'/'VMA', 'When Man-o\'-War enters the battlefield, return target creature to its owner\'s hand.').
card_image_name('man-o\'-war'/'VMA', 'man-o\'-war').
card_uid('man-o\'-war'/'VMA', 'VMA:Man-o\'-War:man-o\'-war').
card_rarity('man-o\'-war'/'VMA', 'Common').
card_artist('man-o\'-war'/'VMA', 'Jon J. Muth').
card_number('man-o\'-war'/'VMA', '79').
card_flavor_text('man-o\'-war'/'VMA', '\"Beauty to the eye does not always translate to the touch.\"\n—Naimah, Femeref philosopher').
card_multiverse_id('man-o\'-war'/'VMA', '383009').

card_in_set('mana crypt', 'VMA').
card_original_type('mana crypt'/'VMA', 'Artifact').
card_original_text('mana crypt'/'VMA', 'At the beginning of your upkeep, flip a coin. If you lose the flip, Mana Crypt deals 3 damage to you.\n{T}: Add {2} to your mana pool.').
card_image_name('mana crypt'/'VMA', 'mana crypt').
card_uid('mana crypt'/'VMA', 'VMA:Mana Crypt:mana crypt').
card_rarity('mana crypt'/'VMA', 'Mythic Rare').
card_artist('mana crypt'/'VMA', 'Matt Stewart').
card_number('mana crypt'/'VMA', '272').
card_multiverse_id('mana crypt'/'VMA', '383005').

card_in_set('mana drain', 'VMA').
card_original_type('mana drain'/'VMA', 'Instant').
card_original_text('mana drain'/'VMA', 'Counter target spell. At the beginning of your next main phase, add {X} to your mana pool, where X is that spell\'s converted mana cost.').
card_image_name('mana drain'/'VMA', 'mana drain').
card_uid('mana drain'/'VMA', 'VMA:Mana Drain:mana drain').
card_rarity('mana drain'/'VMA', 'Mythic Rare').
card_artist('mana drain'/'VMA', 'Mike Bierek').
card_number('mana drain'/'VMA', '78').
card_multiverse_id('mana drain'/'VMA', '383006').

card_in_set('mana prism', 'VMA').
card_original_type('mana prism'/'VMA', 'Artifact').
card_original_text('mana prism'/'VMA', '{T}: Add {1} to your mana pool.\n{1}, {T}: Add one mana of any color to your mana pool.').
card_image_name('mana prism'/'VMA', 'mana prism').
card_uid('mana prism'/'VMA', 'VMA:Mana Prism:mana prism').
card_rarity('mana prism'/'VMA', 'Common').
card_artist('mana prism'/'VMA', 'Margaret Organ-Kean').
card_number('mana prism'/'VMA', '273').
card_multiverse_id('mana prism'/'VMA', '383007').

card_in_set('mana vault', 'VMA').
card_original_type('mana vault'/'VMA', 'Artifact').
card_original_text('mana vault'/'VMA', 'Mana Vault doesn\'t untap during your untap step.\nAt the beginning of your upkeep, you may pay {4}. If you do, untap Mana Vault.\nAt the beginning of your draw step, if Mana Vault is tapped, it deals 1 damage to you.\n{T}: Add {3} to your mana pool.').
card_image_name('mana vault'/'VMA', 'mana vault').
card_uid('mana vault'/'VMA', 'VMA:Mana Vault:mana vault').
card_rarity('mana vault'/'VMA', 'Rare').
card_artist('mana vault'/'VMA', 'Christine Choi').
card_number('mana vault'/'VMA', '274').
card_multiverse_id('mana vault'/'VMA', '383008').

card_in_set('marchesa, the black rose', 'VMA').
card_original_type('marchesa, the black rose'/'VMA', 'Legendary Creature — Human Wizard').
card_original_text('marchesa, the black rose'/'VMA', 'Dethrone (Whenever this creature attacks the player with the most life or tied for most life, put a +1/+1 counter on it.)\nOther creatures you control have dethrone.\nWhenever a creature you control with a +1/+1 counter on it dies, return that card to the battlefield under your control at the beginning of the next end step.').
card_image_name('marchesa, the black rose'/'VMA', 'marchesa, the black rose').
card_uid('marchesa, the black rose'/'VMA', 'VMA:Marchesa, the Black Rose:marchesa, the black rose').
card_rarity('marchesa, the black rose'/'VMA', 'Mythic Rare').
card_artist('marchesa, the black rose'/'VMA', 'Matt Stewart').
card_number('marchesa, the black rose'/'VMA', '256').
card_multiverse_id('marchesa, the black rose'/'VMA', '383010').

card_in_set('masticore', 'VMA').
card_original_type('masticore'/'VMA', 'Artifact Creature — Masticore').
card_original_text('masticore'/'VMA', 'At the beginning of your upkeep, sacrifice Masticore unless you discard a card.\n{2}: Masticore deals 1 damage to target creature.\n{2}: Regenerate Masticore.').
card_image_name('masticore'/'VMA', 'masticore').
card_uid('masticore'/'VMA', 'VMA:Masticore:masticore').
card_rarity('masticore'/'VMA', 'Rare').
card_artist('masticore'/'VMA', 'Steven Belledin').
card_number('masticore'/'VMA', '275').
card_multiverse_id('masticore'/'VMA', '383011').

card_in_set('memory jar', 'VMA').
card_original_type('memory jar'/'VMA', 'Artifact').
card_original_text('memory jar'/'VMA', '{T}, Sacrifice Memory Jar: Each player exiles all cards from his or her hand face down and draws seven cards. At the beginning of the next end step, each player discards his or her hand and returns to his or her hand each card he or she exiled this way.').
card_image_name('memory jar'/'VMA', 'memory jar').
card_uid('memory jar'/'VMA', 'VMA:Memory Jar:memory jar').
card_rarity('memory jar'/'VMA', 'Mythic Rare').
card_artist('memory jar'/'VMA', 'Donato Giancola').
card_number('memory jar'/'VMA', '276').
card_multiverse_id('memory jar'/'VMA', '383012').

card_in_set('mesmeric fiend', 'VMA').
card_original_type('mesmeric fiend'/'VMA', 'Creature — Nightmare Horror').
card_original_text('mesmeric fiend'/'VMA', 'When Mesmeric Fiend enters the battlefield, target opponent reveals his or her hand and you choose a nonland card from it. Exile that card.\nWhen Mesmeric Fiend leaves the battlefield, return the exiled card to its owner\'s hand.').
card_image_name('mesmeric fiend'/'VMA', 'mesmeric fiend').
card_uid('mesmeric fiend'/'VMA', 'VMA:Mesmeric Fiend:mesmeric fiend').
card_rarity('mesmeric fiend'/'VMA', 'Common').
card_artist('mesmeric fiend'/'VMA', 'Dana Knutson').
card_number('mesmeric fiend'/'VMA', '128').
card_multiverse_id('mesmeric fiend'/'VMA', '383013').

card_in_set('mind\'s desire', 'VMA').
card_original_type('mind\'s desire'/'VMA', 'Sorcery').
card_original_text('mind\'s desire'/'VMA', 'Shuffle your library. Then exile the top card of your library. Until end of turn, you may play that card without paying its mana cost. (If it has X in its mana cost, X is 0.)\nStorm (When you cast this spell, copy it for each spell cast before it this turn.)').
card_image_name('mind\'s desire'/'VMA', 'mind\'s desire').
card_uid('mind\'s desire'/'VMA', 'VMA:Mind\'s Desire:mind\'s desire').
card_rarity('mind\'s desire'/'VMA', 'Rare').
card_artist('mind\'s desire'/'VMA', 'Anthony Francisco').
card_number('mind\'s desire'/'VMA', '80').
card_multiverse_id('mind\'s desire'/'VMA', '383014').

card_in_set('mishra\'s workshop', 'VMA').
card_original_type('mishra\'s workshop'/'VMA', 'Land').
card_original_text('mishra\'s workshop'/'VMA', '{T}: Add {3} to your mana pool. Spend this mana only to cast artifact spells.').
card_image_name('mishra\'s workshop'/'VMA', 'mishra\'s workshop').
card_uid('mishra\'s workshop'/'VMA', 'VMA:Mishra\'s Workshop:mishra\'s workshop').
card_rarity('mishra\'s workshop'/'VMA', 'Mythic Rare').
card_artist('mishra\'s workshop'/'VMA', 'Sam Burley').
card_number('mishra\'s workshop'/'VMA', '305').
card_flavor_text('mishra\'s workshop'/'VMA', 'Though he eventually came to despise Tocasia, Mishra listened well to her lessons on clarity of purpose. Unlike his brother, he focused his mind on a single goal.').
card_multiverse_id('mishra\'s workshop'/'VMA', '383015').

card_in_set('mistmoon griffin', 'VMA').
card_original_type('mistmoon griffin'/'VMA', 'Creature — Griffin').
card_original_text('mistmoon griffin'/'VMA', 'Flying\nWhen Mistmoon Griffin dies, exile Mistmoon Griffin, then return the top creature card of your graveyard to the battlefield.').
card_image_name('mistmoon griffin'/'VMA', 'mistmoon griffin').
card_uid('mistmoon griffin'/'VMA', 'VMA:Mistmoon Griffin:mistmoon griffin').
card_rarity('mistmoon griffin'/'VMA', 'Common').
card_artist('mistmoon griffin'/'VMA', 'David A. Cherry').
card_number('mistmoon griffin'/'VMA', '34').
card_multiverse_id('mistmoon griffin'/'VMA', '383016').

card_in_set('morphling', 'VMA').
card_original_type('morphling'/'VMA', 'Creature — Shapeshifter').
card_original_text('morphling'/'VMA', '{U}: Untap Morphling.\n{U}: Morphling gains flying until end of turn.\n{U}: Morphling gains shroud until end of turn. (It can\'t be the target of spells or abilities.)\n{1}: Morphling gets +1/-1 until end of turn.\n{1}: Morphling gets -1/+1 until end of turn.').
card_image_name('morphling'/'VMA', 'morphling').
card_uid('morphling'/'VMA', 'VMA:Morphling:morphling').
card_rarity('morphling'/'VMA', 'Mythic Rare').
card_artist('morphling'/'VMA', 'rk post').
card_number('morphling'/'VMA', '81').
card_multiverse_id('morphling'/'VMA', '383017').

card_in_set('mountain valley', 'VMA').
card_original_type('mountain valley'/'VMA', 'Land').
card_original_text('mountain valley'/'VMA', 'Mountain Valley enters the battlefield tapped.\n{T}, Sacrifice Mountain Valley: Search your library for a Mountain or Forest card and put it onto the battlefield. Then shuffle your library.').
card_image_name('mountain valley'/'VMA', 'mountain valley').
card_uid('mountain valley'/'VMA', 'VMA:Mountain Valley:mountain valley').
card_rarity('mountain valley'/'VMA', 'Uncommon').
card_artist('mountain valley'/'VMA', 'Kari Johnson').
card_number('mountain valley'/'VMA', '306').
card_multiverse_id('mountain valley'/'VMA', '383018').

card_in_set('mox emerald', 'VMA').
card_original_type('mox emerald'/'VMA', 'Artifact').
card_original_text('mox emerald'/'VMA', '{T}: Add {G} to your mana pool.').
card_image_name('mox emerald'/'VMA', 'mox emerald').
card_uid('mox emerald'/'VMA', 'VMA:Mox Emerald:mox emerald').
card_rarity('mox emerald'/'VMA', 'Special').
card_artist('mox emerald'/'VMA', 'Volkan Baga').
card_number('mox emerald'/'VMA', '5').
card_multiverse_id('mox emerald'/'VMA', '383019').

card_in_set('mox jet', 'VMA').
card_original_type('mox jet'/'VMA', 'Artifact').
card_original_text('mox jet'/'VMA', '{T}: Add {B} to your mana pool.').
card_image_name('mox jet'/'VMA', 'mox jet').
card_uid('mox jet'/'VMA', 'VMA:Mox Jet:mox jet').
card_rarity('mox jet'/'VMA', 'Special').
card_artist('mox jet'/'VMA', 'Volkan Baga').
card_number('mox jet'/'VMA', '6').
card_multiverse_id('mox jet'/'VMA', '383020').

card_in_set('mox pearl', 'VMA').
card_original_type('mox pearl'/'VMA', 'Artifact').
card_original_text('mox pearl'/'VMA', '{T}: Add {W} to your mana pool.').
card_image_name('mox pearl'/'VMA', 'mox pearl').
card_uid('mox pearl'/'VMA', 'VMA:Mox Pearl:mox pearl').
card_rarity('mox pearl'/'VMA', 'Special').
card_artist('mox pearl'/'VMA', 'Volkan Baga').
card_number('mox pearl'/'VMA', '7').
card_multiverse_id('mox pearl'/'VMA', '383021').

card_in_set('mox ruby', 'VMA').
card_original_type('mox ruby'/'VMA', 'Artifact').
card_original_text('mox ruby'/'VMA', '{T}: Add {R} to your mana pool.').
card_image_name('mox ruby'/'VMA', 'mox ruby').
card_uid('mox ruby'/'VMA', 'VMA:Mox Ruby:mox ruby').
card_rarity('mox ruby'/'VMA', 'Special').
card_artist('mox ruby'/'VMA', 'Volkan Baga').
card_number('mox ruby'/'VMA', '8').
card_multiverse_id('mox ruby'/'VMA', '383022').

card_in_set('mox sapphire', 'VMA').
card_original_type('mox sapphire'/'VMA', 'Artifact').
card_original_text('mox sapphire'/'VMA', '{T}: Add {U} to your mana pool.').
card_image_name('mox sapphire'/'VMA', 'mox sapphire').
card_uid('mox sapphire'/'VMA', 'VMA:Mox Sapphire:mox sapphire').
card_rarity('mox sapphire'/'VMA', 'Special').
card_artist('mox sapphire'/'VMA', 'Volkan Baga').
card_number('mox sapphire'/'VMA', '9').
card_multiverse_id('mox sapphire'/'VMA', '383023').

card_in_set('muzzio, visionary architect', 'VMA').
card_original_type('muzzio, visionary architect'/'VMA', 'Legendary Creature — Human Artificer').
card_original_text('muzzio, visionary architect'/'VMA', '{3}{U}, {T}: Look at the top X cards of your library, where X is the highest converted mana cost among artifacts you control. You may reveal an artifact card from among them and put it onto the battlefield. Put the rest on the bottom of your library in any order.').
card_image_name('muzzio, visionary architect'/'VMA', 'muzzio, visionary architect').
card_uid('muzzio, visionary architect'/'VMA', 'VMA:Muzzio, Visionary Architect:muzzio, visionary architect').
card_rarity('muzzio, visionary architect'/'VMA', 'Mythic Rare').
card_artist('muzzio, visionary architect'/'VMA', 'Volkan Baga').
card_number('muzzio, visionary architect'/'VMA', '82').
card_multiverse_id('muzzio, visionary architect'/'VMA', '383024').

card_in_set('mystic zealot', 'VMA').
card_original_type('mystic zealot'/'VMA', 'Creature — Human Nomad Mystic').
card_original_text('mystic zealot'/'VMA', 'Threshold — As long as seven or more cards are in your graveyard, Mystic Zealot gets +1/+1 and has flying.').
card_image_name('mystic zealot'/'VMA', 'mystic zealot').
card_uid('mystic zealot'/'VMA', 'VMA:Mystic Zealot:mystic zealot').
card_rarity('mystic zealot'/'VMA', 'Uncommon').
card_artist('mystic zealot'/'VMA', 'Paolo Parente').
card_number('mystic zealot'/'VMA', '35').
card_flavor_text('mystic zealot'/'VMA', 'Nomad youths aspire to one of two roles in the tribe: priest or warrior. Their secret dream is to become both.').
card_multiverse_id('mystic zealot'/'VMA', '383025').

card_in_set('nature\'s lore', 'VMA').
card_original_type('nature\'s lore'/'VMA', 'Sorcery').
card_original_text('nature\'s lore'/'VMA', 'Search your library for a Forest card and put that card onto the battlefield. Then shuffle your library.').
card_image_name('nature\'s lore'/'VMA', 'nature\'s lore').
card_uid('nature\'s lore'/'VMA', 'VMA:Nature\'s Lore:nature\'s lore').
card_rarity('nature\'s lore'/'VMA', 'Common').
card_artist('nature\'s lore'/'VMA', 'Terese Nielsen').
card_number('nature\'s lore'/'VMA', '220').
card_flavor_text('nature\'s lore'/'VMA', 'Nature\'s secrets can be read on every tree, every branch, every leaf.').
card_multiverse_id('nature\'s lore'/'VMA', '383026').

card_in_set('nature\'s ruin', 'VMA').
card_original_type('nature\'s ruin'/'VMA', 'Sorcery').
card_original_text('nature\'s ruin'/'VMA', 'Destroy all green creatures.').
card_image_name('nature\'s ruin'/'VMA', 'nature\'s ruin').
card_uid('nature\'s ruin'/'VMA', 'VMA:Nature\'s Ruin:nature\'s ruin').
card_rarity('nature\'s ruin'/'VMA', 'Rare').
card_artist('nature\'s ruin'/'VMA', 'Mike Dringenberg').
card_number('nature\'s ruin'/'VMA', '129').
card_flavor_text('nature\'s ruin'/'VMA', 'One chill blast—the exhalation of the grave.').
card_multiverse_id('nature\'s ruin'/'VMA', '383027').

card_in_set('necropotence', 'VMA').
card_original_type('necropotence'/'VMA', 'Enchantment').
card_original_text('necropotence'/'VMA', 'Skip your draw step.\nWhenever you discard a card, exile that card from your graveyard.\nPay 1 life: Exile the top card of your library face down. Put that card into your hand at the beginning of your next end step.').
card_image_name('necropotence'/'VMA', 'necropotence').
card_uid('necropotence'/'VMA', 'VMA:Necropotence:necropotence').
card_rarity('necropotence'/'VMA', 'Rare').
card_artist('necropotence'/'VMA', 'Dave Kendall').
card_number('necropotence'/'VMA', '130').
card_multiverse_id('necropotence'/'VMA', '383028').

card_in_set('nevinyrral\'s disk', 'VMA').
card_original_type('nevinyrral\'s disk'/'VMA', 'Artifact').
card_original_text('nevinyrral\'s disk'/'VMA', 'Nevinyrral\'s Disk enters the battlefield tapped.\n{1}, {T}: Destroy all artifacts, creatures, and enchantments.').
card_image_name('nevinyrral\'s disk'/'VMA', 'nevinyrral\'s disk').
card_uid('nevinyrral\'s disk'/'VMA', 'VMA:Nevinyrral\'s Disk:nevinyrral\'s disk').
card_rarity('nevinyrral\'s disk'/'VMA', 'Rare').
card_artist('nevinyrral\'s disk'/'VMA', 'Steve Argyle').
card_number('nevinyrral\'s disk'/'VMA', '277').
card_multiverse_id('nevinyrral\'s disk'/'VMA', '383029').

card_in_set('nightscape familiar', 'VMA').
card_original_type('nightscape familiar'/'VMA', 'Creature — Zombie').
card_original_text('nightscape familiar'/'VMA', 'Blue spells and red spells you cast cost {1} less to cast.\n{1}{B}: Regenerate Nightscape Familiar.').
card_image_name('nightscape familiar'/'VMA', 'nightscape familiar').
card_uid('nightscape familiar'/'VMA', 'VMA:Nightscape Familiar:nightscape familiar').
card_rarity('nightscape familiar'/'VMA', 'Common').
card_artist('nightscape familiar'/'VMA', 'Jeff Easley').
card_number('nightscape familiar'/'VMA', '131').
card_flavor_text('nightscape familiar'/'VMA', 'Nightscape masters don\'t stop at raising the spirit of a fallen battlemage. They raise the flesh along with it.').
card_multiverse_id('nightscape familiar'/'VMA', '383030').

card_in_set('noble templar', 'VMA').
card_original_type('noble templar'/'VMA', 'Creature — Human Cleric Soldier').
card_original_text('noble templar'/'VMA', 'Vigilance\nPlainscycling {2} ({2}, Discard this card: Search your library for a Plains card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('noble templar'/'VMA', 'noble templar').
card_uid('noble templar'/'VMA', 'VMA:Noble Templar:noble templar').
card_rarity('noble templar'/'VMA', 'Common').
card_artist('noble templar'/'VMA', 'Alex Horley-Orlandelli').
card_number('noble templar'/'VMA', '36').
card_multiverse_id('noble templar'/'VMA', '383031').

card_in_set('norwood priestess', 'VMA').
card_original_type('norwood priestess'/'VMA', 'Creature — Elf Druid').
card_original_text('norwood priestess'/'VMA', '{T}: You may put a green creature card from your hand onto the battlefield. Activate this ability only during your turn, before attackers are declared.').
card_image_name('norwood priestess'/'VMA', 'norwood priestess').
card_uid('norwood priestess'/'VMA', 'VMA:Norwood Priestess:norwood priestess').
card_rarity('norwood priestess'/'VMA', 'Rare').
card_artist('norwood priestess'/'VMA', 'Melissa A. Benson').
card_number('norwood priestess'/'VMA', '221').
card_multiverse_id('norwood priestess'/'VMA', '383032').

card_in_set('nostalgic dreams', 'VMA').
card_original_type('nostalgic dreams'/'VMA', 'Sorcery').
card_original_text('nostalgic dreams'/'VMA', 'As an additional cost to cast Nostalgic Dreams, discard X cards.\nReturn X target cards from your graveyard to your hand. Exile Nostalgic Dreams.').
card_image_name('nostalgic dreams'/'VMA', 'nostalgic dreams').
card_uid('nostalgic dreams'/'VMA', 'VMA:Nostalgic Dreams:nostalgic dreams').
card_rarity('nostalgic dreams'/'VMA', 'Uncommon').
card_artist('nostalgic dreams'/'VMA', 'Darrell Riche').
card_number('nostalgic dreams'/'VMA', '222').
card_flavor_text('nostalgic dreams'/'VMA', 'Seton dreams of life renewed.').
card_multiverse_id('nostalgic dreams'/'VMA', '383033').

card_in_set('null rod', 'VMA').
card_original_type('null rod'/'VMA', 'Artifact').
card_original_text('null rod'/'VMA', 'Activated abilities of artifacts can\'t be activated.').
card_image_name('null rod'/'VMA', 'null rod').
card_uid('null rod'/'VMA', 'VMA:Null Rod:null rod').
card_rarity('null rod'/'VMA', 'Rare').
card_artist('null rod'/'VMA', 'Anson Maddocks').
card_number('null rod'/'VMA', '278').
card_flavor_text('null rod'/'VMA', 'Gerrard: \"But it doesn\'t do anything!\"\nHanna: \"No—it does nothing.\"').
card_multiverse_id('null rod'/'VMA', '383034').

card_in_set('oath of druids', 'VMA').
card_original_type('oath of druids'/'VMA', 'Enchantment').
card_original_text('oath of druids'/'VMA', 'At the beginning of each player\'s upkeep, that player chooses target player who controls more creatures than he or she does and is his or her opponent. The first player may reveal cards from the top of his or her library until he or she reveals a creature card. If he or she does, that player puts that card onto the battlefield and all other cards revealed this way into his or her graveyard.').
card_image_name('oath of druids'/'VMA', 'oath of druids').
card_uid('oath of druids'/'VMA', 'VMA:Oath of Druids:oath of druids').
card_rarity('oath of druids'/'VMA', 'Mythic Rare').
card_artist('oath of druids'/'VMA', 'Daren Bader').
card_number('oath of druids'/'VMA', '223').
card_multiverse_id('oath of druids'/'VMA', '383035').

card_in_set('obsessive search', 'VMA').
card_original_type('obsessive search'/'VMA', 'Instant').
card_original_text('obsessive search'/'VMA', 'Draw a card.\nMadness {U} (If you discard this card, you may cast it for its madness cost instead of putting it into your graveyard.)').
card_image_name('obsessive search'/'VMA', 'obsessive search').
card_uid('obsessive search'/'VMA', 'VMA:Obsessive Search:obsessive search').
card_rarity('obsessive search'/'VMA', 'Common').
card_artist('obsessive search'/'VMA', 'Jim Nelson').
card_number('obsessive search'/'VMA', '83').
card_flavor_text('obsessive search'/'VMA', 'The question strained his sanity. The answer snapped it in half.').
card_multiverse_id('obsessive search'/'VMA', '383036').

card_in_set('ophidian', 'VMA').
card_original_type('ophidian'/'VMA', 'Creature — Snake').
card_original_text('ophidian'/'VMA', 'Whenever Ophidian attacks and isn\'t blocked, you may draw a card. If you do, Ophidian assigns no combat damage this turn.').
card_image_name('ophidian'/'VMA', 'ophidian').
card_uid('ophidian'/'VMA', 'VMA:Ophidian:ophidian').
card_rarity('ophidian'/'VMA', 'Common').
card_artist('ophidian'/'VMA', 'Cliff Nielsen').
card_number('ophidian'/'VMA', '84').
card_multiverse_id('ophidian'/'VMA', '383037').

card_in_set('orcish lumberjack', 'VMA').
card_original_type('orcish lumberjack'/'VMA', 'Creature — Orc').
card_original_text('orcish lumberjack'/'VMA', '{T}, Sacrifice a Forest: Add three mana in any combination of {R} and/or {G} to your mana pool.').
card_image_name('orcish lumberjack'/'VMA', 'orcish lumberjack').
card_uid('orcish lumberjack'/'VMA', 'VMA:Orcish Lumberjack:orcish lumberjack').
card_rarity('orcish lumberjack'/'VMA', 'Common').
card_artist('orcish lumberjack'/'VMA', 'Steve Prescott').
card_number('orcish lumberjack'/'VMA', '179').
card_flavor_text('orcish lumberjack'/'VMA', '\"Wood is wasted in the dirt.\"\n—Orcish proverb').
card_multiverse_id('orcish lumberjack'/'VMA', '383038').

card_in_set('owl familiar', 'VMA').
card_original_type('owl familiar'/'VMA', 'Creature — Bird').
card_original_text('owl familiar'/'VMA', 'Flying\nWhen Owl Familiar enters the battlefield, draw a card, then discard a card.').
card_image_name('owl familiar'/'VMA', 'owl familiar').
card_uid('owl familiar'/'VMA', 'VMA:Owl Familiar:owl familiar').
card_rarity('owl familiar'/'VMA', 'Common').
card_artist('owl familiar'/'VMA', 'Janine Johnston').
card_number('owl familiar'/'VMA', '85').
card_multiverse_id('owl familiar'/'VMA', '383039').

card_in_set('palinchron', 'VMA').
card_original_type('palinchron'/'VMA', 'Creature — Illusion').
card_original_text('palinchron'/'VMA', 'Flying\nWhen Palinchron enters the battlefield, untap up to seven lands.\n{2}{U}{U}: Return Palinchron to its owner\'s hand.').
card_image_name('palinchron'/'VMA', 'palinchron').
card_uid('palinchron'/'VMA', 'VMA:Palinchron:palinchron').
card_rarity('palinchron'/'VMA', 'Rare').
card_artist('palinchron'/'VMA', 'Matthew D. Wilson').
card_number('palinchron'/'VMA', '86').
card_multiverse_id('palinchron'/'VMA', '383040').

card_in_set('parallax wave', 'VMA').
card_original_type('parallax wave'/'VMA', 'Enchantment').
card_original_text('parallax wave'/'VMA', 'Fading 5 (This enchantment enters the battlefield with five fade counters on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)\nRemove a fade counter from Parallax Wave: Exile target creature.\nWhen Parallax Wave leaves the battlefield, each player returns to the battlefield all cards he or she owns exiled with Parallax Wave.').
card_image_name('parallax wave'/'VMA', 'parallax wave').
card_uid('parallax wave'/'VMA', 'VMA:Parallax Wave:parallax wave').
card_rarity('parallax wave'/'VMA', 'Rare').
card_artist('parallax wave'/'VMA', 'Greg Staples').
card_number('parallax wave'/'VMA', '37').
card_multiverse_id('parallax wave'/'VMA', '383041').

card_in_set('paralyze', 'VMA').
card_original_type('paralyze'/'VMA', 'Enchantment — Aura').
card_original_text('paralyze'/'VMA', 'Enchant creature\nWhen Paralyze enters the battlefield, tap enchanted creature.\nEnchanted creature doesn\'t untap during its controller\'s untap step.\nAt the beginning of the upkeep of enchanted creature\'s controller, that player may pay {4}. If he or she does, untap the creature.').
card_image_name('paralyze'/'VMA', 'paralyze').
card_uid('paralyze'/'VMA', 'VMA:Paralyze:paralyze').
card_rarity('paralyze'/'VMA', 'Common').
card_artist('paralyze'/'VMA', 'Ron Spencer').
card_number('paralyze'/'VMA', '132').
card_multiverse_id('paralyze'/'VMA', '383042').

card_in_set('penumbra wurm', 'VMA').
card_original_type('penumbra wurm'/'VMA', 'Creature — Wurm').
card_original_text('penumbra wurm'/'VMA', 'Trample\nWhen Penumbra Wurm dies, put a 6/6 black Wurm creature token with trample onto the battlefield.').
card_image_name('penumbra wurm'/'VMA', 'penumbra wurm').
card_uid('penumbra wurm'/'VMA', 'VMA:Penumbra Wurm:penumbra wurm').
card_rarity('penumbra wurm'/'VMA', 'Uncommon').
card_artist('penumbra wurm'/'VMA', 'Daarken').
card_number('penumbra wurm'/'VMA', '224').
card_multiverse_id('penumbra wurm'/'VMA', '383043').

card_in_set('phantom nomad', 'VMA').
card_original_type('phantom nomad'/'VMA', 'Creature — Spirit Nomad').
card_original_text('phantom nomad'/'VMA', 'Phantom Nomad enters the battlefield with two +1/+1 counters on it.\nIf damage would be dealt to Phantom Nomad, prevent that damage. Remove a +1/+1 counter from Phantom Nomad.').
card_image_name('phantom nomad'/'VMA', 'phantom nomad').
card_uid('phantom nomad'/'VMA', 'VMA:Phantom Nomad:phantom nomad').
card_rarity('phantom nomad'/'VMA', 'Common').
card_artist('phantom nomad'/'VMA', 'Jim Nelson').
card_number('phantom nomad'/'VMA', '38').
card_multiverse_id('phantom nomad'/'VMA', '383044').

card_in_set('phyrexian defiler', 'VMA').
card_original_type('phyrexian defiler'/'VMA', 'Creature — Carrier').
card_original_text('phyrexian defiler'/'VMA', '{T}, Sacrifice Phyrexian Defiler: Target creature gets -3/-3 until end of turn.').
card_image_name('phyrexian defiler'/'VMA', 'phyrexian defiler').
card_uid('phyrexian defiler'/'VMA', 'VMA:Phyrexian Defiler:phyrexian defiler').
card_rarity('phyrexian defiler'/'VMA', 'Uncommon').
card_artist('phyrexian defiler'/'VMA', 'DiTerlizzi').
card_number('phyrexian defiler'/'VMA', '133').
card_flavor_text('phyrexian defiler'/'VMA', '\"The third stage of the illness: muscle aches and persistent cough.\"\n—Phyrexian progress notes').
card_multiverse_id('phyrexian defiler'/'VMA', '383045').

card_in_set('pianna, nomad captain', 'VMA').
card_original_type('pianna, nomad captain'/'VMA', 'Legendary Creature — Human Nomad').
card_original_text('pianna, nomad captain'/'VMA', 'Whenever Pianna, Nomad Captain attacks, attacking creatures get +1/+1 until end of turn.').
card_image_name('pianna, nomad captain'/'VMA', 'pianna, nomad captain').
card_uid('pianna, nomad captain'/'VMA', 'VMA:Pianna, Nomad Captain:pianna, nomad captain').
card_rarity('pianna, nomad captain'/'VMA', 'Uncommon').
card_artist('pianna, nomad captain'/'VMA', 'D. Alexander Gregory').
card_number('pianna, nomad captain'/'VMA', '39').
card_flavor_text('pianna, nomad captain'/'VMA', 'Some find inspiration in their swords. Others find it in their leaders.').
card_multiverse_id('pianna, nomad captain'/'VMA', '383046').

card_in_set('pillaging horde', 'VMA').
card_original_type('pillaging horde'/'VMA', 'Creature — Human Barbarian').
card_original_text('pillaging horde'/'VMA', 'When Pillaging Horde enters the battlefield, sacrifice it unless you discard a card at random.').
card_image_name('pillaging horde'/'VMA', 'pillaging horde').
card_uid('pillaging horde'/'VMA', 'VMA:Pillaging Horde:pillaging horde').
card_rarity('pillaging horde'/'VMA', 'Uncommon').
card_artist('pillaging horde'/'VMA', 'Kev Walker').
card_number('pillaging horde'/'VMA', '180').
card_multiverse_id('pillaging horde'/'VMA', '383047').

card_in_set('pine barrens', 'VMA').
card_original_type('pine barrens'/'VMA', 'Land').
card_original_text('pine barrens'/'VMA', 'Pine Barrens enters the battlefield tapped.\n{T}: Add {1} to your mana pool.\n{T}: Add {B} or {G} to your mana pool. Pine Barrens deals 1 damage to you.').
card_image_name('pine barrens'/'VMA', 'pine barrens').
card_uid('pine barrens'/'VMA', 'VMA:Pine Barrens:pine barrens').
card_rarity('pine barrens'/'VMA', 'Uncommon').
card_artist('pine barrens'/'VMA', 'Rebecca Guay').
card_number('pine barrens'/'VMA', '307').
card_multiverse_id('pine barrens'/'VMA', '383048').

card_in_set('plateau', 'VMA').
card_original_type('plateau'/'VMA', 'Land — Mountain Plains').
card_original_text('plateau'/'VMA', '').
card_image_name('plateau'/'VMA', 'plateau').
card_uid('plateau'/'VMA', 'VMA:Plateau:plateau').
card_rarity('plateau'/'VMA', 'Rare').
card_artist('plateau'/'VMA', 'Noah Bradley').
card_number('plateau'/'VMA', '308').
card_multiverse_id('plateau'/'VMA', '383049').

card_in_set('plea for power', 'VMA').
card_original_type('plea for power'/'VMA', 'Sorcery').
card_original_text('plea for power'/'VMA', 'Will of the council — Starting with you, each player votes for time or knowledge. If time gets more votes, take an extra turn after this one. If knowledge gets more votes or the vote is tied, draw three cards.').
card_image_name('plea for power'/'VMA', 'plea for power').
card_uid('plea for power'/'VMA', 'VMA:Plea for Power:plea for power').
card_rarity('plea for power'/'VMA', 'Rare').
card_artist('plea for power'/'VMA', 'John Severin Brassell').
card_number('plea for power'/'VMA', '87').
card_multiverse_id('plea for power'/'VMA', '383050').

card_in_set('power sink', 'VMA').
card_original_type('power sink'/'VMA', 'Instant').
card_original_text('power sink'/'VMA', 'Counter target spell unless its controller pays {X}. If he or she doesn\'t, that player taps all lands with mana abilities he or she controls and empties his or her mana pool.').
card_image_name('power sink'/'VMA', 'power sink').
card_uid('power sink'/'VMA', 'VMA:Power Sink:power sink').
card_rarity('power sink'/'VMA', 'Uncommon').
card_artist('power sink'/'VMA', 'Andrew Robinson').
card_number('power sink'/'VMA', '88').
card_multiverse_id('power sink'/'VMA', '383051').

card_in_set('predator, flagship', 'VMA').
card_original_type('predator, flagship'/'VMA', 'Legendary Artifact').
card_original_text('predator, flagship'/'VMA', '{2}: Target creature gains flying until end of turn.\n{5}, {T}: Destroy target creature with flying.').
card_image_name('predator, flagship'/'VMA', 'predator, flagship').
card_uid('predator, flagship'/'VMA', 'VMA:Predator, Flagship:predator, flagship').
card_rarity('predator, flagship'/'VMA', 'Rare').
card_artist('predator, flagship'/'VMA', 'Mark Tedin').
card_number('predator, flagship'/'VMA', '279').
card_flavor_text('predator, flagship'/'VMA', '\"The scourge of Skyshroud is airborne once more.\"\n—Oracle en-Vec').
card_multiverse_id('predator, flagship'/'VMA', '383052').

card_in_set('predatory nightstalker', 'VMA').
card_original_type('predatory nightstalker'/'VMA', 'Creature — Nightstalker').
card_original_text('predatory nightstalker'/'VMA', 'When Predatory Nightstalker enters the battlefield, you may have target opponent sacrifice a creature.').
card_image_name('predatory nightstalker'/'VMA', 'predatory nightstalker').
card_uid('predatory nightstalker'/'VMA', 'VMA:Predatory Nightstalker:predatory nightstalker').
card_rarity('predatory nightstalker'/'VMA', 'Common').
card_artist('predatory nightstalker'/'VMA', 'D. Alexander Gregory').
card_number('predatory nightstalker'/'VMA', '134').
card_multiverse_id('predatory nightstalker'/'VMA', '383053').

card_in_set('prophetic bolt', 'VMA').
card_original_type('prophetic bolt'/'VMA', 'Instant').
card_original_text('prophetic bolt'/'VMA', 'Prophetic Bolt deals 4 damage to target creature or player. Look at the top four cards of your library. Put one of those cards into your hand and the rest on the bottom of your library in any order.').
card_image_name('prophetic bolt'/'VMA', 'prophetic bolt').
card_uid('prophetic bolt'/'VMA', 'VMA:Prophetic Bolt:prophetic bolt').
card_rarity('prophetic bolt'/'VMA', 'Uncommon').
card_artist('prophetic bolt'/'VMA', 'Slawomir Maniak').
card_number('prophetic bolt'/'VMA', '257').
card_multiverse_id('prophetic bolt'/'VMA', '383054').

card_in_set('provoke', 'VMA').
card_original_type('provoke'/'VMA', 'Instant').
card_original_text('provoke'/'VMA', 'Untap target creature you don\'t control. That creature blocks this turn if able.\nDraw a card.').
card_image_name('provoke'/'VMA', 'provoke').
card_uid('provoke'/'VMA', 'VMA:Provoke:provoke').
card_rarity('provoke'/'VMA', 'Common').
card_artist('provoke'/'VMA', 'Terese Nielsen').
card_number('provoke'/'VMA', '225').
card_flavor_text('provoke'/'VMA', 'Mirri did not have time to think, only to react.').
card_multiverse_id('provoke'/'VMA', '383055').

card_in_set('psychatog', 'VMA').
card_original_type('psychatog'/'VMA', 'Creature — Atog').
card_original_text('psychatog'/'VMA', 'Discard a card: Psychatog gets +1/+1 until end of turn.\nExile two cards from your graveyard: Psychatog gets +1/+1 until end of turn.').
card_image_name('psychatog'/'VMA', 'psychatog').
card_uid('psychatog'/'VMA', 'VMA:Psychatog:psychatog').
card_rarity('psychatog'/'VMA', 'Uncommon').
card_artist('psychatog'/'VMA', 'Izzy').
card_number('psychatog'/'VMA', '258').
card_multiverse_id('psychatog'/'VMA', '383056').

card_in_set('putrid imp', 'VMA').
card_original_type('putrid imp'/'VMA', 'Creature — Zombie Imp').
card_original_text('putrid imp'/'VMA', 'Discard a card: Putrid Imp gains flying until end of turn.\nThreshold — As long as seven or more cards are in your graveyard, Putrid Imp gets +1/+1 and can\'t block.').
card_image_name('putrid imp'/'VMA', 'putrid imp').
card_uid('putrid imp'/'VMA', 'VMA:Putrid Imp:putrid imp').
card_rarity('putrid imp'/'VMA', 'Common').
card_artist('putrid imp'/'VMA', 'Wayne England').
card_number('putrid imp'/'VMA', '135').
card_multiverse_id('putrid imp'/'VMA', '383057').

card_in_set('radiant\'s judgment', 'VMA').
card_original_type('radiant\'s judgment'/'VMA', 'Instant').
card_original_text('radiant\'s judgment'/'VMA', 'Destroy target creature with power 4 or greater.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_image_name('radiant\'s judgment'/'VMA', 'radiant\'s judgment').
card_uid('radiant\'s judgment'/'VMA', 'VMA:Radiant\'s Judgment:radiant\'s judgment').
card_rarity('radiant\'s judgment'/'VMA', 'Common').
card_artist('radiant\'s judgment'/'VMA', 'Greg Staples').
card_number('radiant\'s judgment'/'VMA', '41').
card_multiverse_id('radiant\'s judgment'/'VMA', '383059').

card_in_set('radiant, archangel', 'VMA').
card_original_type('radiant, archangel'/'VMA', 'Legendary Creature — Angel').
card_original_text('radiant, archangel'/'VMA', 'Flying, vigilance\nRadiant, Archangel gets +1/+1 for each other creature with flying on the battlefield.').
card_image_name('radiant, archangel'/'VMA', 'radiant, archangel').
card_uid('radiant, archangel'/'VMA', 'VMA:Radiant, Archangel:radiant, archangel').
card_rarity('radiant, archangel'/'VMA', 'Uncommon').
card_artist('radiant, archangel'/'VMA', 'Michael Sutfin').
card_number('radiant, archangel'/'VMA', '40').
card_multiverse_id('radiant, archangel'/'VMA', '383058').

card_in_set('realm seekers', 'VMA').
card_original_type('realm seekers'/'VMA', 'Creature — Elf Scout').
card_original_text('realm seekers'/'VMA', 'Realm Seekers enters the battlefield with X +1/+1 counters on it, where X is the total number of cards in all players\' hands.\n{2}{G}, Remove a +1/+1 counter from Realm Seekers: Search your library for a land card, reveal it, put it into your hand, then shuffle your library.').
card_image_name('realm seekers'/'VMA', 'realm seekers').
card_uid('realm seekers'/'VMA', 'VMA:Realm Seekers:realm seekers').
card_rarity('realm seekers'/'VMA', 'Rare').
card_artist('realm seekers'/'VMA', 'Mike Sass').
card_number('realm seekers'/'VMA', '226').
card_multiverse_id('realm seekers'/'VMA', '383060').

card_in_set('reanimate', 'VMA').
card_original_type('reanimate'/'VMA', 'Sorcery').
card_original_text('reanimate'/'VMA', 'Put target creature card from a graveyard onto the battlefield under your control. You lose life equal to its converted mana cost.').
card_image_name('reanimate'/'VMA', 'reanimate').
card_uid('reanimate'/'VMA', 'VMA:Reanimate:reanimate').
card_rarity('reanimate'/'VMA', 'Uncommon').
card_artist('reanimate'/'VMA', 'Robert Bliss').
card_number('reanimate'/'VMA', '136').
card_flavor_text('reanimate'/'VMA', '\"You will learn to earn death.\"\n—Volrath').
card_multiverse_id('reanimate'/'VMA', '383061').

card_in_set('reckless charge', 'VMA').
card_original_type('reckless charge'/'VMA', 'Sorcery').
card_original_text('reckless charge'/'VMA', 'Target creature gets +3/+0 and gains haste until end of turn.\nFlashback {2}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('reckless charge'/'VMA', 'reckless charge').
card_uid('reckless charge'/'VMA', 'VMA:Reckless Charge:reckless charge').
card_rarity('reckless charge'/'VMA', 'Common').
card_artist('reckless charge'/'VMA', 'Scott M. Fischer').
card_number('reckless charge'/'VMA', '181').
card_multiverse_id('reckless charge'/'VMA', '383062').

card_in_set('recurring nightmare', 'VMA').
card_original_type('recurring nightmare'/'VMA', 'Enchantment').
card_original_text('recurring nightmare'/'VMA', 'Sacrifice a creature, Return Recurring Nightmare to its owner\'s hand: Return target creature card from your graveyard to the battlefield. Activate this ability only any time you could cast a sorcery.').
card_image_name('recurring nightmare'/'VMA', 'recurring nightmare').
card_uid('recurring nightmare'/'VMA', 'VMA:Recurring Nightmare:recurring nightmare').
card_rarity('recurring nightmare'/'VMA', 'Rare').
card_artist('recurring nightmare'/'VMA', 'Jeff Laubenstein').
card_number('recurring nightmare'/'VMA', '137').
card_flavor_text('recurring nightmare'/'VMA', '\"I am confined by sleep and defined by nightmare.\"\n—Crovax').
card_multiverse_id('recurring nightmare'/'VMA', '383063').

card_in_set('regrowth', 'VMA').
card_original_type('regrowth'/'VMA', 'Sorcery').
card_original_text('regrowth'/'VMA', 'Return target card from your graveyard to your hand.').
card_image_name('regrowth'/'VMA', 'regrowth').
card_uid('regrowth'/'VMA', 'VMA:Regrowth:regrowth').
card_rarity('regrowth'/'VMA', 'Rare').
card_artist('regrowth'/'VMA', 'Dan Scott').
card_number('regrowth'/'VMA', '227').
card_multiverse_id('regrowth'/'VMA', '383064').

card_in_set('reign of the pit', 'VMA').
card_original_type('reign of the pit'/'VMA', 'Sorcery').
card_original_text('reign of the pit'/'VMA', 'Each player sacrifices a creature. Put an X/X black Demon creature token with flying onto the battlefield, where X is the total power of the creatures sacrificed this way.').
card_image_name('reign of the pit'/'VMA', 'reign of the pit').
card_uid('reign of the pit'/'VMA', 'VMA:Reign of the Pit:reign of the pit').
card_rarity('reign of the pit'/'VMA', 'Rare').
card_artist('reign of the pit'/'VMA', 'Evan Shipard').
card_number('reign of the pit'/'VMA', '138').
card_multiverse_id('reign of the pit'/'VMA', '383065').

card_in_set('renewed faith', 'VMA').
card_original_type('renewed faith'/'VMA', 'Instant').
card_original_text('renewed faith'/'VMA', 'You gain 6 life.\nCycling {1}{W} ({1}{W}, Discard this card: Draw a card.)\nWhen you cycle Renewed Faith, you may gain 2 life.').
card_image_name('renewed faith'/'VMA', 'renewed faith').
card_uid('renewed faith'/'VMA', 'VMA:Renewed Faith:renewed faith').
card_rarity('renewed faith'/'VMA', 'Common').
card_artist('renewed faith'/'VMA', 'Dave Dorman').
card_number('renewed faith'/'VMA', '42').
card_multiverse_id('renewed faith'/'VMA', '383066').

card_in_set('repel', 'VMA').
card_original_type('repel'/'VMA', 'Instant').
card_original_text('repel'/'VMA', 'Put target creature on top of its owner\'s library.').
card_image_name('repel'/'VMA', 'repel').
card_uid('repel'/'VMA', 'VMA:Repel:repel').
card_rarity('repel'/'VMA', 'Common').
card_artist('repel'/'VMA', 'Terese Nielsen').
card_number('repel'/'VMA', '89').
card_flavor_text('repel'/'VMA', '\"I have a hunch we\'ll be seeing that one again soon.\"\n—Ambassador Laquatus').
card_multiverse_id('repel'/'VMA', '383067').

card_in_set('rescind', 'VMA').
card_original_type('rescind'/'VMA', 'Instant').
card_original_text('rescind'/'VMA', 'Return target permanent to its owner\'s hand.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_image_name('rescind'/'VMA', 'rescind').
card_uid('rescind'/'VMA', 'VMA:Rescind:rescind').
card_rarity('rescind'/'VMA', 'Common').
card_artist('rescind'/'VMA', 'Adam Rex').
card_number('rescind'/'VMA', '90').
card_multiverse_id('rescind'/'VMA', '383068').

card_in_set('reviving vapors', 'VMA').
card_original_type('reviving vapors'/'VMA', 'Instant').
card_original_text('reviving vapors'/'VMA', 'Reveal the top three cards of your library and put one of them into your hand. You gain life equal to that card\'s converted mana cost. Put all other cards revealed this way into your graveyard.').
card_image_name('reviving vapors'/'VMA', 'reviving vapors').
card_uid('reviving vapors'/'VMA', 'VMA:Reviving Vapors:reviving vapors').
card_rarity('reviving vapors'/'VMA', 'Uncommon').
card_artist('reviving vapors'/'VMA', 'Pete Venters').
card_number('reviving vapors'/'VMA', '259').
card_multiverse_id('reviving vapors'/'VMA', '383069').

card_in_set('ring of gix', 'VMA').
card_original_type('ring of gix'/'VMA', 'Artifact').
card_original_text('ring of gix'/'VMA', 'Echo {3} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\n{1}, {T}: Tap target artifact, creature, or land.').
card_image_name('ring of gix'/'VMA', 'ring of gix').
card_uid('ring of gix'/'VMA', 'VMA:Ring of Gix:ring of gix').
card_rarity('ring of gix'/'VMA', 'Rare').
card_artist('ring of gix'/'VMA', 'Mark Tedin').
card_number('ring of gix'/'VMA', '280').
card_flavor_text('ring of gix'/'VMA', 'Not every cage is made of bars.').
card_multiverse_id('ring of gix'/'VMA', '383070').

card_in_set('rites of initiation', 'VMA').
card_original_type('rites of initiation'/'VMA', 'Instant').
card_original_text('rites of initiation'/'VMA', 'Discard any number of cards at random. Creatures you control get +1/+0 until end of turn for each card discarded this way.').
card_image_name('rites of initiation'/'VMA', 'rites of initiation').
card_uid('rites of initiation'/'VMA', 'VMA:Rites of Initiation:rites of initiation').
card_rarity('rites of initiation'/'VMA', 'Uncommon').
card_artist('rites of initiation'/'VMA', 'Bradley Williams').
card_number('rites of initiation'/'VMA', '182').
card_multiverse_id('rites of initiation'/'VMA', '383071').

card_in_set('roar of the wurm', 'VMA').
card_original_type('roar of the wurm'/'VMA', 'Sorcery').
card_original_text('roar of the wurm'/'VMA', 'Put a 6/6 green Wurm creature token onto the battlefield.\nFlashback {3}{G} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('roar of the wurm'/'VMA', 'roar of the wurm').
card_uid('roar of the wurm'/'VMA', 'VMA:Roar of the Wurm:roar of the wurm').
card_rarity('roar of the wurm'/'VMA', 'Uncommon').
card_artist('roar of the wurm'/'VMA', 'Kev Walker').
card_number('roar of the wurm'/'VMA', '228').
card_multiverse_id('roar of the wurm'/'VMA', '383072').

card_in_set('rocky tar pit', 'VMA').
card_original_type('rocky tar pit'/'VMA', 'Land').
card_original_text('rocky tar pit'/'VMA', 'Rocky Tar Pit enters the battlefield tapped.\n{T}, Sacrifice Rocky Tar Pit: Search your library for a Swamp or Mountain card and put it onto the battlefield. Then shuffle your library.').
card_image_name('rocky tar pit'/'VMA', 'rocky tar pit').
card_uid('rocky tar pit'/'VMA', 'VMA:Rocky Tar Pit:rocky tar pit').
card_rarity('rocky tar pit'/'VMA', 'Uncommon').
card_artist('rocky tar pit'/'VMA', 'Jeff Miracola').
card_number('rocky tar pit'/'VMA', '309').
card_multiverse_id('rocky tar pit'/'VMA', '383073').

card_in_set('rofellos, llanowar emissary', 'VMA').
card_original_type('rofellos, llanowar emissary'/'VMA', 'Legendary Creature — Elf Druid').
card_original_text('rofellos, llanowar emissary'/'VMA', '{T}: Add {G} to your mana pool for each Forest you control.').
card_image_name('rofellos, llanowar emissary'/'VMA', 'rofellos, llanowar emissary').
card_uid('rofellos, llanowar emissary'/'VMA', 'VMA:Rofellos, Llanowar Emissary:rofellos, llanowar emissary').
card_rarity('rofellos, llanowar emissary'/'VMA', 'Rare').
card_artist('rofellos, llanowar emissary'/'VMA', 'Michael Sutfin').
card_number('rofellos, llanowar emissary'/'VMA', '229').
card_multiverse_id('rofellos, llanowar emissary'/'VMA', '383074').

card_in_set('rorix bladewing', 'VMA').
card_original_type('rorix bladewing'/'VMA', 'Legendary Creature — Dragon').
card_original_text('rorix bladewing'/'VMA', 'Flying, haste').
card_image_name('rorix bladewing'/'VMA', 'rorix bladewing').
card_uid('rorix bladewing'/'VMA', 'VMA:Rorix Bladewing:rorix bladewing').
card_rarity('rorix bladewing'/'VMA', 'Rare').
card_artist('rorix bladewing'/'VMA', 'Darrell Riche').
card_number('rorix bladewing'/'VMA', '183').
card_flavor_text('rorix bladewing'/'VMA', 'In the smoldering ashes of Shiv, a few dragons strive to rebuild their native land. The rest seek any opportunity to restore the broken pride of their race.').
card_multiverse_id('rorix bladewing'/'VMA', '383075').

card_in_set('salt flats', 'VMA').
card_original_type('salt flats'/'VMA', 'Land').
card_original_text('salt flats'/'VMA', 'Salt Flats enters the battlefield tapped.\n{T}: Add {1} to your mana pool.\n{T}: Add {W} or {B} to your mana pool. Salt Flats deals 1 damage to you.').
card_image_name('salt flats'/'VMA', 'salt flats').
card_uid('salt flats'/'VMA', 'VMA:Salt Flats:salt flats').
card_rarity('salt flats'/'VMA', 'Uncommon').
card_artist('salt flats'/'VMA', 'Scott Kirschner').
card_number('salt flats'/'VMA', '310').
card_multiverse_id('salt flats'/'VMA', '383076').

card_in_set('saproling burst', 'VMA').
card_original_type('saproling burst'/'VMA', 'Enchantment').
card_original_text('saproling burst'/'VMA', 'Fading 7 (This enchantment enters the battlefield with seven fade counters on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)\nRemove a fade counter from Saproling Burst: Put a green Saproling creature token onto the battlefield. It has \"This creature\'s power and toughness are each equal to the number of fade counters on Saproling Burst.\"\nWhen Saproling Burst leaves the battlefield, destroy all tokens put onto the battlefield with Saproling Burst. They can\'t be regenerated.').
card_image_name('saproling burst'/'VMA', 'saproling burst').
card_uid('saproling burst'/'VMA', 'VMA:Saproling Burst:saproling burst').
card_rarity('saproling burst'/'VMA', 'Rare').
card_artist('saproling burst'/'VMA', 'Carl Critchlow').
card_number('saproling burst'/'VMA', '230').
card_multiverse_id('saproling burst'/'VMA', '383077').

card_in_set('sarcomancy', 'VMA').
card_original_type('sarcomancy'/'VMA', 'Enchantment').
card_original_text('sarcomancy'/'VMA', 'When Sarcomancy enters the battlefield, put a 2/2 black Zombie creature token onto the battlefield.\nAt the beginning of your upkeep, if there are no Zombies on the battlefield, Sarcomancy deals 1 damage to you.').
card_image_name('sarcomancy'/'VMA', 'sarcomancy').
card_uid('sarcomancy'/'VMA', 'VMA:Sarcomancy:sarcomancy').
card_rarity('sarcomancy'/'VMA', 'Uncommon').
card_artist('sarcomancy'/'VMA', 'Daren Bader').
card_number('sarcomancy'/'VMA', '139').
card_multiverse_id('sarcomancy'/'VMA', '383078').

card_in_set('savannah', 'VMA').
card_original_type('savannah'/'VMA', 'Land — Forest Plains').
card_original_text('savannah'/'VMA', '').
card_image_name('savannah'/'VMA', 'savannah').
card_uid('savannah'/'VMA', 'VMA:Savannah:savannah').
card_rarity('savannah'/'VMA', 'Rare').
card_artist('savannah'/'VMA', 'Charles Urbach').
card_number('savannah'/'VMA', '311').
card_multiverse_id('savannah'/'VMA', '383079').

card_in_set('scabland', 'VMA').
card_original_type('scabland'/'VMA', 'Land').
card_original_text('scabland'/'VMA', 'Scabland enters the battlefield tapped.\n{T}: Add {1} to your mana pool.\n{T}: Add {R} or {W} to your mana pool. Scabland deals 1 damage to you.').
card_image_name('scabland'/'VMA', 'scabland').
card_uid('scabland'/'VMA', 'VMA:Scabland:scabland').
card_rarity('scabland'/'VMA', 'Uncommon').
card_artist('scabland'/'VMA', 'Andrew Robinson').
card_number('scabland'/'VMA', '312').
card_multiverse_id('scabland'/'VMA', '383080').

card_in_set('scourge of the throne', 'VMA').
card_original_type('scourge of the throne'/'VMA', 'Creature — Dragon').
card_original_text('scourge of the throne'/'VMA', 'Flying\nDethrone (Whenever this creature attacks the player with the most life or tied for most life, put a +1/+1 counter on it.)\nWhenever Scourge of the Throne attacks for the first time each turn, if it\'s attacking the player with the most life or tied for most life, untap all attacking creatures. After this phase, there is an additional combat phase.').
card_image_name('scourge of the throne'/'VMA', 'scourge of the throne').
card_uid('scourge of the throne'/'VMA', 'VMA:Scourge of the Throne:scourge of the throne').
card_rarity('scourge of the throne'/'VMA', 'Mythic Rare').
card_artist('scourge of the throne'/'VMA', 'Michael Komarck').
card_number('scourge of the throne'/'VMA', '184').
card_multiverse_id('scourge of the throne'/'VMA', '383081').

card_in_set('scrivener', 'VMA').
card_original_type('scrivener'/'VMA', 'Creature — Human Wizard').
card_original_text('scrivener'/'VMA', 'When Scrivener enters the battlefield, you may return target instant card from your graveyard to your hand.').
card_image_name('scrivener'/'VMA', 'scrivener').
card_uid('scrivener'/'VMA', 'VMA:Scrivener:scrivener').
card_rarity('scrivener'/'VMA', 'Common').
card_artist('scrivener'/'VMA', 'Kev Walker').
card_number('scrivener'/'VMA', '91').
card_flavor_text('scrivener'/'VMA', 'A good memory is no match for a good scribe.').
card_multiverse_id('scrivener'/'VMA', '383082').

card_in_set('scrubland', 'VMA').
card_original_type('scrubland'/'VMA', 'Land — Plains Swamp').
card_original_text('scrubland'/'VMA', '').
card_image_name('scrubland'/'VMA', 'scrubland').
card_uid('scrubland'/'VMA', 'VMA:Scrubland:scrubland').
card_rarity('scrubland'/'VMA', 'Rare').
card_artist('scrubland'/'VMA', 'Eytan Zana').
card_number('scrubland'/'VMA', '313').
card_multiverse_id('scrubland'/'VMA', '383083').

card_in_set('sea drake', 'VMA').
card_original_type('sea drake'/'VMA', 'Creature — Drake').
card_original_text('sea drake'/'VMA', 'Flying\nWhen Sea Drake enters the battlefield, return two target lands you control to their owner\'s hand.').
card_image_name('sea drake'/'VMA', 'sea drake').
card_uid('sea drake'/'VMA', 'VMA:Sea Drake:sea drake').
card_rarity('sea drake'/'VMA', 'Rare').
card_artist('sea drake'/'VMA', 'Rebecca Guay').
card_number('sea drake'/'VMA', '92').
card_multiverse_id('sea drake'/'VMA', '383084').

card_in_set('seal of cleansing', 'VMA').
card_original_type('seal of cleansing'/'VMA', 'Enchantment').
card_original_text('seal of cleansing'/'VMA', 'Sacrifice Seal of Cleansing: Destroy target artifact or enchantment.').
card_image_name('seal of cleansing'/'VMA', 'seal of cleansing').
card_uid('seal of cleansing'/'VMA', 'VMA:Seal of Cleansing:seal of cleansing').
card_rarity('seal of cleansing'/'VMA', 'Common').
card_artist('seal of cleansing'/'VMA', 'Christopher Moeller').
card_number('seal of cleansing'/'VMA', '43').
card_flavor_text('seal of cleansing'/'VMA', '\"I am the purifier, the light that clears all shadows.\"\n—Seal inscription').
card_multiverse_id('seal of cleansing'/'VMA', '383085').

card_in_set('secluded steppe', 'VMA').
card_original_type('secluded steppe'/'VMA', 'Land').
card_original_text('secluded steppe'/'VMA', 'Secluded Steppe enters the battlefield tapped.\n{T}: Add {W} to your mana pool.\nCycling {W} ({W}, Discard this card: Draw a card.)').
card_image_name('secluded steppe'/'VMA', 'secluded steppe').
card_uid('secluded steppe'/'VMA', 'VMA:Secluded Steppe:secluded steppe').
card_rarity('secluded steppe'/'VMA', 'Common').
card_artist('secluded steppe'/'VMA', 'Heather Hudson').
card_number('secluded steppe'/'VMA', '314').
card_multiverse_id('secluded steppe'/'VMA', '383086').

card_in_set('selvala, explorer returned', 'VMA').
card_original_type('selvala, explorer returned'/'VMA', 'Legendary Creature — Elf Scout').
card_original_text('selvala, explorer returned'/'VMA', 'Parley — {T}: Each player reveals the top card of his or her library. For each nonland card revealed this way, add {G} to your mana pool and you gain 1 life. Then each player draws a card.').
card_image_name('selvala, explorer returned'/'VMA', 'selvala, explorer returned').
card_uid('selvala, explorer returned'/'VMA', 'VMA:Selvala, Explorer Returned:selvala, explorer returned').
card_rarity('selvala, explorer returned'/'VMA', 'Rare').
card_artist('selvala, explorer returned'/'VMA', 'Tyler Jacobson').
card_number('selvala, explorer returned'/'VMA', '260').
card_flavor_text('selvala, explorer returned'/'VMA', '\"The Lowlands refuse to suffer at the whims of the High City.\"').
card_multiverse_id('selvala, explorer returned'/'VMA', '383087').

card_in_set('serendib efreet', 'VMA').
card_original_type('serendib efreet'/'VMA', 'Creature — Efreet').
card_original_text('serendib efreet'/'VMA', 'Flying\nAt the beginning of your upkeep, Serendib Efreet deals 1 damage to you.').
card_image_name('serendib efreet'/'VMA', 'serendib efreet').
card_uid('serendib efreet'/'VMA', 'VMA:Serendib Efreet:serendib efreet').
card_rarity('serendib efreet'/'VMA', 'Uncommon').
card_artist('serendib efreet'/'VMA', 'Matt Stewart').
card_number('serendib efreet'/'VMA', '93').
card_flavor_text('serendib efreet'/'VMA', 'Summoners of efreeti remember only the power of command, never the sting of regret.').
card_multiverse_id('serendib efreet'/'VMA', '383088').

card_in_set('shelter', 'VMA').
card_original_type('shelter'/'VMA', 'Instant').
card_original_text('shelter'/'VMA', 'Target creature you control gains protection from the color of your choice until end of turn.\nDraw a card.').
card_image_name('shelter'/'VMA', 'shelter').
card_uid('shelter'/'VMA', 'VMA:Shelter:shelter').
card_rarity('shelter'/'VMA', 'Common').
card_artist('shelter'/'VMA', 'Christopher Moeller').
card_number('shelter'/'VMA', '44').
card_flavor_text('shelter'/'VMA', 'Good strategists seize opportunities. Great strategists make their own.').
card_multiverse_id('shelter'/'VMA', '383089').

card_in_set('shivan wurm', 'VMA').
card_original_type('shivan wurm'/'VMA', 'Creature — Wurm').
card_original_text('shivan wurm'/'VMA', 'Trample\nWhen Shivan Wurm enters the battlefield, return a red or green creature you control to its owner\'s hand.').
card_image_name('shivan wurm'/'VMA', 'shivan wurm').
card_uid('shivan wurm'/'VMA', 'VMA:Shivan Wurm:shivan wurm').
card_rarity('shivan wurm'/'VMA', 'Rare').
card_artist('shivan wurm'/'VMA', 'Scott M. Fischer').
card_number('shivan wurm'/'VMA', '261').
card_multiverse_id('shivan wurm'/'VMA', '383090').

card_in_set('sidar jabari', 'VMA').
card_original_type('sidar jabari'/'VMA', 'Legendary Creature — Human Knight').
card_original_text('sidar jabari'/'VMA', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)\nWhenever Sidar Jabari attacks, tap target creature defending player controls.').
card_image_name('sidar jabari'/'VMA', 'sidar jabari').
card_uid('sidar jabari'/'VMA', 'VMA:Sidar Jabari:sidar jabari').
card_rarity('sidar jabari'/'VMA', 'Uncommon').
card_artist('sidar jabari'/'VMA', 'Gerry Grace').
card_number('sidar jabari'/'VMA', '45').
card_flavor_text('sidar jabari'/'VMA', '\"Prophecy is whatever one makes of it, but destiny cannot be changed.\"\n—Sidar Jabari').
card_multiverse_id('sidar jabari'/'VMA', '383091').

card_in_set('silvos, rogue elemental', 'VMA').
card_original_type('silvos, rogue elemental'/'VMA', 'Legendary Creature — Elemental').
card_original_text('silvos, rogue elemental'/'VMA', 'Trample\n{G}: Regenerate Silvos, Rogue Elemental.').
card_image_name('silvos, rogue elemental'/'VMA', 'silvos, rogue elemental').
card_uid('silvos, rogue elemental'/'VMA', 'VMA:Silvos, Rogue Elemental:silvos, rogue elemental').
card_rarity('silvos, rogue elemental'/'VMA', 'Rare').
card_artist('silvos, rogue elemental'/'VMA', 'Carl Critchlow').
card_number('silvos, rogue elemental'/'VMA', '231').
card_flavor_text('silvos, rogue elemental'/'VMA', 'He was born of the Mirari, thrust out of his homeland before he was even aware. Left without purpose or meaning, he found both in the pits.').
card_multiverse_id('silvos, rogue elemental'/'VMA', '383092').

card_in_set('simian grunts', 'VMA').
card_original_type('simian grunts'/'VMA', 'Creature — Ape').
card_original_text('simian grunts'/'VMA', 'Flash\nEcho {2}{G} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)').
card_image_name('simian grunts'/'VMA', 'simian grunts').
card_uid('simian grunts'/'VMA', 'VMA:Simian Grunts:simian grunts').
card_rarity('simian grunts'/'VMA', 'Common').
card_artist('simian grunts'/'VMA', 'Pete Venters').
card_number('simian grunts'/'VMA', '232').
card_flavor_text('simian grunts'/'VMA', 'These monkeys mean business.').
card_multiverse_id('simian grunts'/'VMA', '383093').

card_in_set('skirge familiar', 'VMA').
card_original_type('skirge familiar'/'VMA', 'Creature — Imp').
card_original_text('skirge familiar'/'VMA', 'Flying\nDiscard a card: Add {B} to your mana pool.').
card_image_name('skirge familiar'/'VMA', 'skirge familiar').
card_uid('skirge familiar'/'VMA', 'VMA:Skirge Familiar:skirge familiar').
card_rarity('skirge familiar'/'VMA', 'Common').
card_artist('skirge familiar'/'VMA', 'Ron Spencer').
card_number('skirge familiar'/'VMA', '140').
card_flavor_text('skirge familiar'/'VMA', 'The first Yawgmoth priest to harness their power was rewarded with several unique mutilations.').
card_multiverse_id('skirge familiar'/'VMA', '383094').

card_in_set('skirk drill sergeant', 'VMA').
card_original_type('skirk drill sergeant'/'VMA', 'Creature — Goblin').
card_original_text('skirk drill sergeant'/'VMA', 'Whenever Skirk Drill Sergeant or another Goblin dies, you may pay {2}{R}. If you do, reveal the top card of your library. If it\'s a Goblin permanent card, put it onto the battlefield. Otherwise, put it into your graveyard.').
card_image_name('skirk drill sergeant'/'VMA', 'skirk drill sergeant').
card_uid('skirk drill sergeant'/'VMA', 'VMA:Skirk Drill Sergeant:skirk drill sergeant').
card_rarity('skirk drill sergeant'/'VMA', 'Common').
card_artist('skirk drill sergeant'/'VMA', 'Alex Horley-Orlandelli').
card_number('skirk drill sergeant'/'VMA', '185').
card_flavor_text('skirk drill sergeant'/'VMA', '\"I order you to volunteer.\"').
card_multiverse_id('skirk drill sergeant'/'VMA', '383095').

card_in_set('skirk prospector', 'VMA').
card_original_type('skirk prospector'/'VMA', 'Creature — Goblin').
card_original_text('skirk prospector'/'VMA', 'Sacrifice a Goblin: Add {R} to your mana pool.').
card_image_name('skirk prospector'/'VMA', 'skirk prospector').
card_uid('skirk prospector'/'VMA', 'VMA:Skirk Prospector:skirk prospector').
card_rarity('skirk prospector'/'VMA', 'Common').
card_artist('skirk prospector'/'VMA', 'Doug Chaffee').
card_number('skirk prospector'/'VMA', '186').
card_flavor_text('skirk prospector'/'VMA', '\"I like goblins. They make funny little popping sounds when they die.\"\n—Braids, dementia summoner').
card_multiverse_id('skirk prospector'/'VMA', '383096').

card_in_set('skullclamp', 'VMA').
card_original_type('skullclamp'/'VMA', 'Artifact — Equipment').
card_original_text('skullclamp'/'VMA', 'Equipped creature gets +1/-1.\nWhenever equipped creature dies, draw two cards.\nEquip {1}').
card_image_name('skullclamp'/'VMA', 'skullclamp').
card_uid('skullclamp'/'VMA', 'VMA:Skullclamp:skullclamp').
card_rarity('skullclamp'/'VMA', 'Mythic Rare').
card_artist('skullclamp'/'VMA', 'Daniel Ljunggren').
card_number('skullclamp'/'VMA', '281').
card_multiverse_id('skullclamp'/'VMA', '383097').

card_in_set('skyshroud forest', 'VMA').
card_original_type('skyshroud forest'/'VMA', 'Land').
card_original_text('skyshroud forest'/'VMA', 'Skyshroud Forest enters the battlefield tapped.\n{T}: Add {1} to your mana pool.\n{T}: Add {G} or {U} to your mana pool. Skyshroud Forest deals 1 damage to you.').
card_image_name('skyshroud forest'/'VMA', 'skyshroud forest').
card_uid('skyshroud forest'/'VMA', 'VMA:Skyshroud Forest:skyshroud forest').
card_rarity('skyshroud forest'/'VMA', 'Uncommon').
card_artist('skyshroud forest'/'VMA', 'Roger Raupp').
card_number('skyshroud forest'/'VMA', '315').
card_multiverse_id('skyshroud forest'/'VMA', '383098').

card_in_set('skywing aven', 'VMA').
card_original_type('skywing aven'/'VMA', 'Creature — Bird Soldier').
card_original_text('skywing aven'/'VMA', 'Flying\nDiscard a card: Return Skywing Aven to its owner\'s hand.').
card_image_name('skywing aven'/'VMA', 'skywing aven').
card_uid('skywing aven'/'VMA', 'VMA:Skywing Aven:skywing aven').
card_rarity('skywing aven'/'VMA', 'Common').
card_artist('skywing aven'/'VMA', 'Matt Cavotta').
card_number('skywing aven'/'VMA', '94').
card_flavor_text('skywing aven'/'VMA', '\"I am as the wind that bears me: harsh yet gentle, fleeting yet ever-present. Together we fly beyond imagination.\"').
card_multiverse_id('skywing aven'/'VMA', '383099').

card_in_set('smokestack', 'VMA').
card_original_type('smokestack'/'VMA', 'Artifact').
card_original_text('smokestack'/'VMA', 'At the beginning of your upkeep, you may put a soot counter on Smokestack.\nAt the beginning of each player\'s upkeep, that player sacrifices a permanent for each soot counter on Smokestack.').
card_image_name('smokestack'/'VMA', 'smokestack').
card_uid('smokestack'/'VMA', 'VMA:Smokestack:smokestack').
card_rarity('smokestack'/'VMA', 'Rare').
card_artist('smokestack'/'VMA', 'Daniel Ljunggren').
card_number('smokestack'/'VMA', '282').
card_multiverse_id('smokestack'/'VMA', '383100').

card_in_set('sol ring', 'VMA').
card_original_type('sol ring'/'VMA', 'Artifact').
card_original_text('sol ring'/'VMA', '{T}: Add {2} to your mana pool.').
card_image_name('sol ring'/'VMA', 'sol ring').
card_uid('sol ring'/'VMA', 'VMA:Sol Ring:sol ring').
card_rarity('sol ring'/'VMA', 'Mythic Rare').
card_artist('sol ring'/'VMA', 'Mike Bierek').
card_number('sol ring'/'VMA', '283').
card_flavor_text('sol ring'/'VMA', 'Lost to time is the artificer\'s art of trapping light from a distant star in a ring of purest gold.').
card_multiverse_id('sol ring'/'VMA', '383101').

card_in_set('solar blast', 'VMA').
card_original_type('solar blast'/'VMA', 'Instant').
card_original_text('solar blast'/'VMA', 'Solar Blast deals 3 damage to target creature or player.\nCycling {1}{R}{R} ({1}{R}{R}, Discard this card: Draw a card.)\nWhen you cycle Solar Blast, you may have it deal 1 damage to target creature or player.').
card_image_name('solar blast'/'VMA', 'solar blast').
card_uid('solar blast'/'VMA', 'VMA:Solar Blast:solar blast').
card_rarity('solar blast'/'VMA', 'Common').
card_artist('solar blast'/'VMA', 'Greg Staples').
card_number('solar blast'/'VMA', '187').
card_multiverse_id('solar blast'/'VMA', '383102').

card_in_set('soltari emissary', 'VMA').
card_original_type('soltari emissary'/'VMA', 'Creature — Soltari Soldier').
card_original_text('soltari emissary'/'VMA', '{W}: Soltari Emissary gains shadow until end of turn. (It can block or be blocked by only creatures with shadow.)').
card_image_name('soltari emissary'/'VMA', 'soltari emissary').
card_uid('soltari emissary'/'VMA', 'VMA:Soltari Emissary:soltari emissary').
card_rarity('soltari emissary'/'VMA', 'Common').
card_artist('soltari emissary'/'VMA', 'Adam Rex').
card_number('soltari emissary'/'VMA', '46').
card_flavor_text('soltari emissary'/'VMA', 'Alone at the portal, Ertai began his meditation. He realized immediately that he was not alone.').
card_multiverse_id('soltari emissary'/'VMA', '383103').

card_in_set('soltari trooper', 'VMA').
card_original_type('soltari trooper'/'VMA', 'Creature — Soltari Soldier').
card_original_text('soltari trooper'/'VMA', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nWhenever Soltari Trooper attacks, it gets +1/+1 until end of turn.').
card_image_name('soltari trooper'/'VMA', 'soltari trooper').
card_uid('soltari trooper'/'VMA', 'VMA:Soltari Trooper:soltari trooper').
card_rarity('soltari trooper'/'VMA', 'Common').
card_artist('soltari trooper'/'VMA', 'Kev Walker').
card_number('soltari trooper'/'VMA', '47').
card_flavor_text('soltari trooper'/'VMA', '\"Dauthi blood is soltari wine.\"\n—Soltari Tales of Life').
card_multiverse_id('soltari trooper'/'VMA', '383104').

card_in_set('spark spray', 'VMA').
card_original_type('spark spray'/'VMA', 'Instant').
card_original_text('spark spray'/'VMA', 'Spark Spray deals 1 damage to target creature or player.\nCycling {R} ({R}, Discard this card: Draw a card.)').
card_image_name('spark spray'/'VMA', 'spark spray').
card_uid('spark spray'/'VMA', 'VMA:Spark Spray:spark spray').
card_rarity('spark spray'/'VMA', 'Common').
card_artist('spark spray'/'VMA', 'Pete Venters').
card_number('spark spray'/'VMA', '188').
card_flavor_text('spark spray'/'VMA', 'It\'s the only kind of shower goblins will tolerate.').
card_multiverse_id('spark spray'/'VMA', '383105').

card_in_set('sphere of resistance', 'VMA').
card_original_type('sphere of resistance'/'VMA', 'Artifact').
card_original_text('sphere of resistance'/'VMA', 'Spells cost {1} more to cast.').
card_image_name('sphere of resistance'/'VMA', 'sphere of resistance').
card_uid('sphere of resistance'/'VMA', 'VMA:Sphere of Resistance:sphere of resistance').
card_rarity('sphere of resistance'/'VMA', 'Rare').
card_artist('sphere of resistance'/'VMA', 'Doug Chaffee').
card_number('sphere of resistance'/'VMA', '284').
card_flavor_text('sphere of resistance'/'VMA', 'A sphere pushes equally in all directions.').
card_multiverse_id('sphere of resistance'/'VMA', '383106').

card_in_set('spinal graft', 'VMA').
card_original_type('spinal graft'/'VMA', 'Enchantment — Aura').
card_original_text('spinal graft'/'VMA', 'Enchant creature\nEnchanted creature gets +3/+3.\nWhen enchanted creature becomes the target of a spell or ability, destroy that creature. It can\'t be regenerated.').
card_image_name('spinal graft'/'VMA', 'spinal graft').
card_uid('spinal graft'/'VMA', 'VMA:Spinal Graft:spinal graft').
card_rarity('spinal graft'/'VMA', 'Common').
card_artist('spinal graft'/'VMA', 'Ron Spencer').
card_number('spinal graft'/'VMA', '141').
card_flavor_text('spinal graft'/'VMA', '\"This one has not screamed enough to show effective implantation. Kill it.\"\n—Volrath').
card_multiverse_id('spinal graft'/'VMA', '383107').

card_in_set('spirit cairn', 'VMA').
card_original_type('spirit cairn'/'VMA', 'Enchantment').
card_original_text('spirit cairn'/'VMA', 'Whenever a player discards a card, you may pay {W}. If you do, put a 1/1 white Spirit creature token with flying onto the battlefield.').
card_image_name('spirit cairn'/'VMA', 'spirit cairn').
card_uid('spirit cairn'/'VMA', 'VMA:Spirit Cairn:spirit cairn').
card_rarity('spirit cairn'/'VMA', 'Rare').
card_artist('spirit cairn'/'VMA', 'Gary Ruddell').
card_number('spirit cairn'/'VMA', '48').
card_flavor_text('spirit cairn'/'VMA', 'It marks the border between here and hereafter.').
card_multiverse_id('spirit cairn'/'VMA', '383108').

card_in_set('spirit mirror', 'VMA').
card_original_type('spirit mirror'/'VMA', 'Enchantment').
card_original_text('spirit mirror'/'VMA', 'At the beginning of your upkeep, if there are no Reflection tokens on the battlefield, put a 2/2 white Reflection creature token onto the battlefield.\n{0}: Destroy target Reflection.').
card_image_name('spirit mirror'/'VMA', 'spirit mirror').
card_uid('spirit mirror'/'VMA', 'VMA:Spirit Mirror:spirit mirror').
card_rarity('spirit mirror'/'VMA', 'Rare').
card_artist('spirit mirror'/'VMA', 'D. Alexander Gregory').
card_number('spirit mirror'/'VMA', '49').
card_multiverse_id('spirit mirror'/'VMA', '383109').

card_in_set('spiritmonger', 'VMA').
card_original_type('spiritmonger'/'VMA', 'Creature — Beast').
card_original_text('spiritmonger'/'VMA', 'Whenever Spiritmonger deals damage to a creature, put a +1/+1 counter on Spiritmonger.\n{B}: Regenerate Spiritmonger.\n{G}: Spiritmonger becomes the color of your choice until end of turn.').
card_image_name('spiritmonger'/'VMA', 'spiritmonger').
card_uid('spiritmonger'/'VMA', 'VMA:Spiritmonger:spiritmonger').
card_rarity('spiritmonger'/'VMA', 'Rare').
card_artist('spiritmonger'/'VMA', 'Kev Walker').
card_number('spiritmonger'/'VMA', '262').
card_multiverse_id('spiritmonger'/'VMA', '383110').

card_in_set('starstorm', 'VMA').
card_original_type('starstorm'/'VMA', 'Instant').
card_original_text('starstorm'/'VMA', 'Starstorm deals X damage to each creature.\nCycling {3} ({3}, Discard this card: Draw a card.)').
card_image_name('starstorm'/'VMA', 'starstorm').
card_uid('starstorm'/'VMA', 'VMA:Starstorm:starstorm').
card_rarity('starstorm'/'VMA', 'Rare').
card_artist('starstorm'/'VMA', 'Jonas De Ro').
card_number('starstorm'/'VMA', '189').
card_flavor_text('starstorm'/'VMA', 'Ever since the disaster, the city\'s survivors viewed the star-strewn sky with dread.').
card_multiverse_id('starstorm'/'VMA', '383111').

card_in_set('stoic champion', 'VMA').
card_original_type('stoic champion'/'VMA', 'Creature — Human Soldier').
card_original_text('stoic champion'/'VMA', 'Whenever a player cycles a card, Stoic Champion gets +2/+2 until end of turn.').
card_image_name('stoic champion'/'VMA', 'stoic champion').
card_uid('stoic champion'/'VMA', 'VMA:Stoic Champion:stoic champion').
card_rarity('stoic champion'/'VMA', 'Uncommon').
card_artist('stoic champion'/'VMA', 'Greg Hildebrandt').
card_number('stoic champion'/'VMA', '50').
card_flavor_text('stoic champion'/'VMA', 'His outer calm belies his inner fury.').
card_multiverse_id('stoic champion'/'VMA', '383112').

card_in_set('strip mine', 'VMA').
card_original_type('strip mine'/'VMA', 'Land').
card_original_text('strip mine'/'VMA', '{T}: Add {1} to your mana pool.\n{T}, Sacrifice Strip Mine: Destroy target land.').
card_image_name('strip mine'/'VMA', 'strip mine').
card_uid('strip mine'/'VMA', 'VMA:Strip Mine:strip mine').
card_rarity('strip mine'/'VMA', 'Rare').
card_artist('strip mine'/'VMA', 'John Avon').
card_number('strip mine'/'VMA', '316').
card_flavor_text('strip mine'/'VMA', 'Unlike previous conflicts, the war between Urza and Mishra made Dominaria itself a casualty of war.').
card_multiverse_id('strip mine'/'VMA', '383113').

card_in_set('stroke of genius', 'VMA').
card_original_type('stroke of genius'/'VMA', 'Instant').
card_original_text('stroke of genius'/'VMA', 'Target player draws X cards.').
card_image_name('stroke of genius'/'VMA', 'stroke of genius').
card_uid('stroke of genius'/'VMA', 'VMA:Stroke of Genius:stroke of genius').
card_rarity('stroke of genius'/'VMA', 'Rare').
card_artist('stroke of genius'/'VMA', 'Stephen Daniele').
card_number('stroke of genius'/'VMA', '95').
card_flavor_text('stroke of genius'/'VMA', 'After a hundred failed experiments, Urza was stunned to find that common silver passed through the portal undamaged. He immediately designed a golem made of the metal.').
card_multiverse_id('stroke of genius'/'VMA', '383114').

card_in_set('su-chi', 'VMA').
card_original_type('su-chi'/'VMA', 'Artifact Creature — Construct').
card_original_text('su-chi'/'VMA', 'When Su-Chi dies, add {4} to your mana pool.').
card_image_name('su-chi'/'VMA', 'su-chi').
card_uid('su-chi'/'VMA', 'VMA:Su-Chi:su-chi').
card_rarity('su-chi'/'VMA', 'Uncommon').
card_artist('su-chi'/'VMA', 'Robbie Trevino').
card_number('su-chi'/'VMA', '285').
card_flavor_text('su-chi'/'VMA', 'Flawed copies of relics from the Thran Empire, the su-chi were inherently unstable but provided useful knowledge for Tocasia\'s students.').
card_multiverse_id('su-chi'/'VMA', '383115').

card_in_set('sudden strength', 'VMA').
card_original_type('sudden strength'/'VMA', 'Instant').
card_original_text('sudden strength'/'VMA', 'Target creature gets +3/+3 until end of turn.\nDraw a card.').
card_image_name('sudden strength'/'VMA', 'sudden strength').
card_uid('sudden strength'/'VMA', 'VMA:Sudden Strength:sudden strength').
card_rarity('sudden strength'/'VMA', 'Common').
card_artist('sudden strength'/'VMA', 'Alan Pollack').
card_number('sudden strength'/'VMA', '233').
card_flavor_text('sudden strength'/'VMA', 'The Mirari\'s magic transformed the Krosan Forest—and its inhabitants.').
card_multiverse_id('sudden strength'/'VMA', '383116').

card_in_set('sulfuric vortex', 'VMA').
card_original_type('sulfuric vortex'/'VMA', 'Enchantment').
card_original_text('sulfuric vortex'/'VMA', 'At the beginning of each player\'s upkeep, Sulfuric Vortex deals 2 damage to that player.\nIf a player would gain life, that player gains no life instead.').
card_image_name('sulfuric vortex'/'VMA', 'sulfuric vortex').
card_uid('sulfuric vortex'/'VMA', 'VMA:Sulfuric Vortex:sulfuric vortex').
card_rarity('sulfuric vortex'/'VMA', 'Rare').
card_artist('sulfuric vortex'/'VMA', 'Greg Staples').
card_number('sulfuric vortex'/'VMA', '190').
card_multiverse_id('sulfuric vortex'/'VMA', '383117').

card_in_set('survival of the fittest', 'VMA').
card_original_type('survival of the fittest'/'VMA', 'Enchantment').
card_original_text('survival of the fittest'/'VMA', '{G}, Discard a creature card: Search your library for a creature card, reveal that card, and put it into your hand. Then shuffle your library.').
card_image_name('survival of the fittest'/'VMA', 'survival of the fittest').
card_uid('survival of the fittest'/'VMA', 'VMA:Survival of the Fittest:survival of the fittest').
card_rarity('survival of the fittest'/'VMA', 'Rare').
card_artist('survival of the fittest'/'VMA', 'Shelly Wan').
card_number('survival of the fittest'/'VMA', '234').
card_multiverse_id('survival of the fittest'/'VMA', '383118').

card_in_set('swords to plowshares', 'VMA').
card_original_type('swords to plowshares'/'VMA', 'Instant').
card_original_text('swords to plowshares'/'VMA', 'Exile target creature. Its controller gains life equal to its power.').
card_image_name('swords to plowshares'/'VMA', 'swords to plowshares').
card_uid('swords to plowshares'/'VMA', 'VMA:Swords to Plowshares:swords to plowshares').
card_rarity('swords to plowshares'/'VMA', 'Uncommon').
card_artist('swords to plowshares'/'VMA', 'Terese Nielsen').
card_number('swords to plowshares'/'VMA', '51').
card_flavor_text('swords to plowshares'/'VMA', 'The smallest seed of regret can bloom into redemption.').
card_multiverse_id('swords to plowshares'/'VMA', '383119').

card_in_set('sylvan library', 'VMA').
card_original_type('sylvan library'/'VMA', 'Enchantment').
card_original_text('sylvan library'/'VMA', 'At the beginning of your draw step, you may draw two additional cards. If you do, choose two cards in your hand drawn this turn. For each of those cards, pay 4 life or put the card on top of your library.').
card_image_name('sylvan library'/'VMA', 'sylvan library').
card_uid('sylvan library'/'VMA', 'VMA:Sylvan Library:sylvan library').
card_rarity('sylvan library'/'VMA', 'Rare').
card_artist('sylvan library'/'VMA', 'Yeong-Hao Han').
card_number('sylvan library'/'VMA', '235').
card_multiverse_id('sylvan library'/'VMA', '383120').

card_in_set('symbiotic wurm', 'VMA').
card_original_type('symbiotic wurm'/'VMA', 'Creature — Wurm').
card_original_text('symbiotic wurm'/'VMA', 'When Symbiotic Wurm dies, put seven 1/1 green Insect creature tokens onto the battlefield.').
card_image_name('symbiotic wurm'/'VMA', 'symbiotic wurm').
card_uid('symbiotic wurm'/'VMA', 'VMA:Symbiotic Wurm:symbiotic wurm').
card_rarity('symbiotic wurm'/'VMA', 'Uncommon').
card_artist('symbiotic wurm'/'VMA', 'Matt Cavotta').
card_number('symbiotic wurm'/'VMA', '236').
card_flavor_text('symbiotic wurm'/'VMA', 'The insects keep the wurm\'s hide free from parasites. In return, the wurm doesn\'t eat the insects.').
card_multiverse_id('symbiotic wurm'/'VMA', '383121').

card_in_set('taiga', 'VMA').
card_original_type('taiga'/'VMA', 'Land — Mountain Forest').
card_original_text('taiga'/'VMA', '').
card_image_name('taiga'/'VMA', 'taiga').
card_uid('taiga'/'VMA', 'VMA:Taiga:taiga').
card_rarity('taiga'/'VMA', 'Rare').
card_artist('taiga'/'VMA', 'Sam Burley').
card_number('taiga'/'VMA', '317').
card_multiverse_id('taiga'/'VMA', '383122').

card_in_set('tangle', 'VMA').
card_original_type('tangle'/'VMA', 'Instant').
card_original_text('tangle'/'VMA', 'Prevent all combat damage that would be dealt this turn.\nEach attacking creature doesn\'t untap during its controller\'s next untap step.').
card_image_name('tangle'/'VMA', 'tangle').
card_uid('tangle'/'VMA', 'VMA:Tangle:tangle').
card_rarity('tangle'/'VMA', 'Common').
card_artist('tangle'/'VMA', 'John Avon').
card_number('tangle'/'VMA', '237').
card_flavor_text('tangle'/'VMA', 'Gaea battles rampant death with abundant life.').
card_multiverse_id('tangle'/'VMA', '383123').

card_in_set('temporal fissure', 'VMA').
card_original_type('temporal fissure'/'VMA', 'Sorcery').
card_original_text('temporal fissure'/'VMA', 'Return target permanent to its owner\'s hand.\nStorm (When you cast this spell, copy it for each spell cast before it this turn. You may choose new targets for the copies.)').
card_image_name('temporal fissure'/'VMA', 'temporal fissure').
card_uid('temporal fissure'/'VMA', 'VMA:Temporal Fissure:temporal fissure').
card_rarity('temporal fissure'/'VMA', 'Common').
card_artist('temporal fissure'/'VMA', 'Seb McKinnon').
card_number('temporal fissure'/'VMA', '96').
card_multiverse_id('temporal fissure'/'VMA', '383124').

card_in_set('tendrils of agony', 'VMA').
card_original_type('tendrils of agony'/'VMA', 'Sorcery').
card_original_text('tendrils of agony'/'VMA', 'Target player loses 2 life and you gain 2 life.\nStorm (When you cast this spell, copy it for each spell cast before it this turn. You may choose new targets for the copies.)').
card_image_name('tendrils of agony'/'VMA', 'tendrils of agony').
card_uid('tendrils of agony'/'VMA', 'VMA:Tendrils of Agony:tendrils of agony').
card_rarity('tendrils of agony'/'VMA', 'Uncommon').
card_artist('tendrils of agony'/'VMA', 'Volkan Baga').
card_number('tendrils of agony'/'VMA', '142').
card_multiverse_id('tendrils of agony'/'VMA', '383125').

card_in_set('teroh\'s faithful', 'VMA').
card_original_type('teroh\'s faithful'/'VMA', 'Creature — Human Cleric').
card_original_text('teroh\'s faithful'/'VMA', 'When Teroh\'s Faithful enters the battlefield, you gain 4 life.').
card_image_name('teroh\'s faithful'/'VMA', 'teroh\'s faithful').
card_uid('teroh\'s faithful'/'VMA', 'VMA:Teroh\'s Faithful:teroh\'s faithful').
card_rarity('teroh\'s faithful'/'VMA', 'Common').
card_artist('teroh\'s faithful'/'VMA', 'Tim Hildebrandt').
card_number('teroh\'s faithful'/'VMA', '52').
card_flavor_text('teroh\'s faithful'/'VMA', 'The light of reason follows them even into battle.').
card_multiverse_id('teroh\'s faithful'/'VMA', '383126').

card_in_set('thalakos drifters', 'VMA').
card_original_type('thalakos drifters'/'VMA', 'Creature — Thalakos').
card_original_text('thalakos drifters'/'VMA', 'Discard a card: Thalakos Drifters gains shadow until end of turn. (It can block or be blocked by only creatures with shadow.)').
card_image_name('thalakos drifters'/'VMA', 'thalakos drifters').
card_uid('thalakos drifters'/'VMA', 'VMA:Thalakos Drifters:thalakos drifters').
card_rarity('thalakos drifters'/'VMA', 'Uncommon').
card_artist('thalakos drifters'/'VMA', 'Andrew Robinson').
card_number('thalakos drifters'/'VMA', '97').
card_flavor_text('thalakos drifters'/'VMA', 'They drift in and out of shadow and ever on toward madness.').
card_multiverse_id('thalakos drifters'/'VMA', '383127').

card_in_set('thawing glaciers', 'VMA').
card_original_type('thawing glaciers'/'VMA', 'Land').
card_original_text('thawing glaciers'/'VMA', 'Thawing Glaciers enters the battlefield tapped.\n{1}, {T}: Search your library for a basic land card, put that card onto the battlefield tapped, then shuffle your library. Return Thawing Glaciers to its owner\'s hand at the beginning of the next cleanup step.').
card_image_name('thawing glaciers'/'VMA', 'thawing glaciers').
card_uid('thawing glaciers'/'VMA', 'VMA:Thawing Glaciers:thawing glaciers').
card_rarity('thawing glaciers'/'VMA', 'Rare').
card_artist('thawing glaciers'/'VMA', 'Jim Nelson').
card_number('thawing glaciers'/'VMA', '318').
card_multiverse_id('thawing glaciers'/'VMA', '383128').

card_in_set('thopter squadron', 'VMA').
card_original_type('thopter squadron'/'VMA', 'Artifact Creature — Thopter').
card_original_text('thopter squadron'/'VMA', 'Flying\nThopter Squadron enters the battlefield with three +1/+1 counters on it.\n{1}, Remove a +1/+1 counter from Thopter Squadron: Put a 1/1 colorless Thopter artifact creature token with flying onto the battlefield. Activate this ability only any time you could cast a sorcery.\n{1}, Sacrifice another Thopter: Put a +1/+1 counter on Thopter Squadron. Activate this ability only any time you could cast a sorcery.').
card_image_name('thopter squadron'/'VMA', 'thopter squadron').
card_uid('thopter squadron'/'VMA', 'VMA:Thopter Squadron:thopter squadron').
card_rarity('thopter squadron'/'VMA', 'Uncommon').
card_artist('thopter squadron'/'VMA', 'Doug Chaffee').
card_number('thopter squadron'/'VMA', '286').
card_multiverse_id('thopter squadron'/'VMA', '383129').

card_in_set('time vault', 'VMA').
card_original_type('time vault'/'VMA', 'Artifact').
card_original_text('time vault'/'VMA', 'Time Vault enters the battlefield tapped.\nTime Vault doesn\'t untap during your untap step.\nIf you would begin your turn while Time Vault is tapped, you may skip that turn instead. If you do, untap Time Vault.\n{T}: Take an extra turn after this one.').
card_image_name('time vault'/'VMA', 'time vault').
card_uid('time vault'/'VMA', 'VMA:Time Vault:time vault').
card_rarity('time vault'/'VMA', 'Mythic Rare').
card_artist('time vault'/'VMA', 'Yeong-Hao Han').
card_number('time vault'/'VMA', '287').
card_multiverse_id('time vault'/'VMA', '383130').

card_in_set('time walk', 'VMA').
card_original_type('time walk'/'VMA', 'Sorcery').
card_original_text('time walk'/'VMA', 'Take an extra turn after this one.').
card_image_name('time walk'/'VMA', 'time walk').
card_uid('time walk'/'VMA', 'VMA:Time Walk:time walk').
card_rarity('time walk'/'VMA', 'Special').
card_artist('time walk'/'VMA', 'Chris Rahn').
card_number('time walk'/'VMA', '2').
card_multiverse_id('time walk'/'VMA', '383131').

card_in_set('timetwister', 'VMA').
card_original_type('timetwister'/'VMA', 'Sorcery').
card_original_text('timetwister'/'VMA', 'Each player shuffles his or her hand and graveyard into his or her library, then draws seven cards. (Then put Timetwister into its owner\'s graveyard.)').
card_image_name('timetwister'/'VMA', 'timetwister').
card_uid('timetwister'/'VMA', 'VMA:Timetwister:timetwister').
card_rarity('timetwister'/'VMA', 'Special').
card_artist('timetwister'/'VMA', 'Matt Stewart').
card_number('timetwister'/'VMA', '3').
card_multiverse_id('timetwister'/'VMA', '383132').

card_in_set('tolarian academy', 'VMA').
card_original_type('tolarian academy'/'VMA', 'Legendary Land').
card_original_text('tolarian academy'/'VMA', '{T}: Add {U} to your mana pool for each artifact you control.').
card_image_name('tolarian academy'/'VMA', 'tolarian academy').
card_uid('tolarian academy'/'VMA', 'VMA:Tolarian Academy:tolarian academy').
card_rarity('tolarian academy'/'VMA', 'Mythic Rare').
card_artist('tolarian academy'/'VMA', 'Stephen Daniele').
card_number('tolarian academy'/'VMA', '319').
card_flavor_text('tolarian academy'/'VMA', 'The academy worked with time—until time ran out.').
card_multiverse_id('tolarian academy'/'VMA', '383133').

card_in_set('tradewind rider', 'VMA').
card_original_type('tradewind rider'/'VMA', 'Creature — Spirit').
card_original_text('tradewind rider'/'VMA', 'Flying\n{T}, Tap two untapped creatures you control: Return target permanent to its owner\'s hand.').
card_image_name('tradewind rider'/'VMA', 'tradewind rider').
card_uid('tradewind rider'/'VMA', 'VMA:Tradewind Rider:tradewind rider').
card_rarity('tradewind rider'/'VMA', 'Rare').
card_artist('tradewind rider'/'VMA', 'John Matson').
card_number('tradewind rider'/'VMA', '98').
card_flavor_text('tradewind rider'/'VMA', 'It is said that the wind will blow the world past if you wait long enough.').
card_multiverse_id('tradewind rider'/'VMA', '383134').

card_in_set('tranquil thicket', 'VMA').
card_original_type('tranquil thicket'/'VMA', 'Land').
card_original_text('tranquil thicket'/'VMA', 'Tranquil Thicket enters the battlefield tapped.\n{T}: Add {G} to your mana pool.\nCycling {G} ({G}, Discard this card: Draw a card.)').
card_image_name('tranquil thicket'/'VMA', 'tranquil thicket').
card_uid('tranquil thicket'/'VMA', 'VMA:Tranquil Thicket:tranquil thicket').
card_rarity('tranquil thicket'/'VMA', 'Common').
card_artist('tranquil thicket'/'VMA', 'Heather Hudson').
card_number('tranquil thicket'/'VMA', '320').
card_multiverse_id('tranquil thicket'/'VMA', '383135').

card_in_set('triangle of war', 'VMA').
card_original_type('triangle of war'/'VMA', 'Artifact').
card_original_text('triangle of war'/'VMA', '{2}, Sacrifice Triangle of War: Target creature you control fights target creature an opponent controls. (Each deals damage equal to its power to the other.)').
card_image_name('triangle of war'/'VMA', 'triangle of war').
card_uid('triangle of war'/'VMA', 'VMA:Triangle of War:triangle of war').
card_rarity('triangle of war'/'VMA', 'Uncommon').
card_artist('triangle of war'/'VMA', 'Ian Miller').
card_number('triangle of war'/'VMA', '288').
card_flavor_text('triangle of war'/'VMA', 'The Zhalfirin war triangle represents a trinity of might, faith, and guile.').
card_multiverse_id('triangle of war'/'VMA', '383136').

card_in_set('tribute to the wild', 'VMA').
card_original_type('tribute to the wild'/'VMA', 'Instant').
card_original_text('tribute to the wild'/'VMA', 'Each opponent sacrifices an artifact or enchantment.').
card_image_name('tribute to the wild'/'VMA', 'tribute to the wild').
card_uid('tribute to the wild'/'VMA', 'VMA:Tribute to the Wild:tribute to the wild').
card_rarity('tribute to the wild'/'VMA', 'Common').
card_artist('tribute to the wild'/'VMA', 'Hideaki Takamura').
card_number('tribute to the wild'/'VMA', '238').
card_flavor_text('tribute to the wild'/'VMA', '\"You may enter, but leave those lifeless things of your world behind.\"\n—Modruni, maro-sorcerer').
card_multiverse_id('tribute to the wild'/'VMA', '383137').

card_in_set('tropical island', 'VMA').
card_original_type('tropical island'/'VMA', 'Land — Forest Island').
card_original_text('tropical island'/'VMA', '').
card_image_name('tropical island'/'VMA', 'tropical island').
card_uid('tropical island'/'VMA', 'VMA:Tropical Island:tropical island').
card_rarity('tropical island'/'VMA', 'Rare').
card_artist('tropical island'/'VMA', 'Franz Vohwinkel').
card_number('tropical island'/'VMA', '321').
card_multiverse_id('tropical island'/'VMA', '383138').

card_in_set('tundra', 'VMA').
card_original_type('tundra'/'VMA', 'Land — Plains Island').
card_original_text('tundra'/'VMA', '').
card_image_name('tundra'/'VMA', 'tundra').
card_uid('tundra'/'VMA', 'VMA:Tundra:tundra').
card_rarity('tundra'/'VMA', 'Rare').
card_artist('tundra'/'VMA', 'Lars Grant-West').
card_number('tundra'/'VMA', '322').
card_multiverse_id('tundra'/'VMA', '383139').

card_in_set('turnabout', 'VMA').
card_original_type('turnabout'/'VMA', 'Instant').
card_original_text('turnabout'/'VMA', 'Choose artifact, creature, or land. Tap all untapped permanents of the chosen type target player controls, or untap all tapped permanents of that type that player controls.').
card_image_name('turnabout'/'VMA', 'turnabout').
card_uid('turnabout'/'VMA', 'VMA:Turnabout:turnabout').
card_rarity('turnabout'/'VMA', 'Uncommon').
card_artist('turnabout'/'VMA', 'Steve Prescott').
card_number('turnabout'/'VMA', '99').
card_multiverse_id('turnabout'/'VMA', '383140').

card_in_set('tyrant\'s choice', 'VMA').
card_original_type('tyrant\'s choice'/'VMA', 'Sorcery').
card_original_text('tyrant\'s choice'/'VMA', 'Will of the council — Starting with you, each player votes for death or torture. If death gets more votes, each opponent sacrifices a creature. If torture gets more votes or the vote is tied, each opponent loses 4 life.').
card_image_name('tyrant\'s choice'/'VMA', 'tyrant\'s choice').
card_uid('tyrant\'s choice'/'VMA', 'VMA:Tyrant\'s Choice:tyrant\'s choice').
card_rarity('tyrant\'s choice'/'VMA', 'Common').
card_artist('tyrant\'s choice'/'VMA', 'Daarken').
card_number('tyrant\'s choice'/'VMA', '143').
card_multiverse_id('tyrant\'s choice'/'VMA', '383141').

card_in_set('underground sea', 'VMA').
card_original_type('underground sea'/'VMA', 'Land — Island Swamp').
card_original_text('underground sea'/'VMA', '').
card_image_name('underground sea'/'VMA', 'underground sea').
card_uid('underground sea'/'VMA', 'VMA:Underground Sea:underground sea').
card_rarity('underground sea'/'VMA', 'Rare').
card_artist('underground sea'/'VMA', 'Cliff Childs').
card_number('underground sea'/'VMA', '323').
card_multiverse_id('underground sea'/'VMA', '383142').

card_in_set('upheaval', 'VMA').
card_original_type('upheaval'/'VMA', 'Sorcery').
card_original_text('upheaval'/'VMA', 'Return all permanents to their owners\' hands.').
card_image_name('upheaval'/'VMA', 'upheaval').
card_uid('upheaval'/'VMA', 'VMA:Upheaval:upheaval').
card_rarity('upheaval'/'VMA', 'Mythic Rare').
card_artist('upheaval'/'VMA', 'Kev Walker').
card_number('upheaval'/'VMA', '100').
card_flavor_text('upheaval'/'VMA', 'The calm comes after the storm.').
card_multiverse_id('upheaval'/'VMA', '383143').

card_in_set('urborg uprising', 'VMA').
card_original_type('urborg uprising'/'VMA', 'Sorcery').
card_original_text('urborg uprising'/'VMA', 'Return up to two target creature cards from your graveyard to your hand.\nDraw a card.').
card_image_name('urborg uprising'/'VMA', 'urborg uprising').
card_uid('urborg uprising'/'VMA', 'VMA:Urborg Uprising:urborg uprising').
card_rarity('urborg uprising'/'VMA', 'Common').
card_artist('urborg uprising'/'VMA', 'Adam Rex').
card_number('urborg uprising'/'VMA', '144').
card_flavor_text('urborg uprising'/'VMA', 'In Urborg, few things are less permanent than death.').
card_multiverse_id('urborg uprising'/'VMA', '383144').

card_in_set('vampiric tutor', 'VMA').
card_original_type('vampiric tutor'/'VMA', 'Instant').
card_original_text('vampiric tutor'/'VMA', 'Search your library for a card, then shuffle your library and put that card on top of it. You lose 2 life.').
card_image_name('vampiric tutor'/'VMA', 'vampiric tutor').
card_uid('vampiric tutor'/'VMA', 'VMA:Vampiric Tutor:vampiric tutor').
card_rarity('vampiric tutor'/'VMA', 'Rare').
card_artist('vampiric tutor'/'VMA', 'Gary Leach').
card_number('vampiric tutor'/'VMA', '145').
card_flavor_text('vampiric tutor'/'VMA', '\"I write upon clean white parchment with a sharp quill and the blood of my students, divining their secrets.\"\n—Shauku, Endbringer').
card_multiverse_id('vampiric tutor'/'VMA', '383145').

card_in_set('visara the dreadful', 'VMA').
card_original_type('visara the dreadful'/'VMA', 'Legendary Creature — Gorgon').
card_original_text('visara the dreadful'/'VMA', 'Flying\n{T}: Destroy target creature. It can\'t be regenerated.').
card_image_name('visara the dreadful'/'VMA', 'visara the dreadful').
card_uid('visara the dreadful'/'VMA', 'VMA:Visara the Dreadful:visara the dreadful').
card_rarity('visara the dreadful'/'VMA', 'Rare').
card_artist('visara the dreadful'/'VMA', 'Kev Walker').
card_number('visara the dreadful'/'VMA', '146').
card_flavor_text('visara the dreadful'/'VMA', '\"My eyes are my strongest feature.\"').
card_multiverse_id('visara the dreadful'/'VMA', '383146').

card_in_set('volcanic island', 'VMA').
card_original_type('volcanic island'/'VMA', 'Land — Island Mountain').
card_original_text('volcanic island'/'VMA', '').
card_image_name('volcanic island'/'VMA', 'volcanic island').
card_uid('volcanic island'/'VMA', 'VMA:Volcanic Island:volcanic island').
card_rarity('volcanic island'/'VMA', 'Rare').
card_artist('volcanic island'/'VMA', 'Noah Bradley').
card_number('volcanic island'/'VMA', '324').
card_multiverse_id('volcanic island'/'VMA', '383147').

card_in_set('volrath\'s shapeshifter', 'VMA').
card_original_type('volrath\'s shapeshifter'/'VMA', 'Creature — Shapeshifter').
card_original_text('volrath\'s shapeshifter'/'VMA', 'As long as the top card of your graveyard is a creature card, Volrath\'s Shapeshifter has the full text of that card and has the text \"{2}: Discard a card.\" (Volrath\'s Shapeshifter has that card\'s name, mana cost, color, types, abilities, power, and toughness.)\n{2}: Discard a card.').
card_image_name('volrath\'s shapeshifter'/'VMA', 'volrath\'s shapeshifter').
card_uid('volrath\'s shapeshifter'/'VMA', 'VMA:Volrath\'s Shapeshifter:volrath\'s shapeshifter').
card_rarity('volrath\'s shapeshifter'/'VMA', 'Rare').
card_artist('volrath\'s shapeshifter'/'VMA', 'Ron Spencer').
card_number('volrath\'s shapeshifter'/'VMA', '101').
card_multiverse_id('volrath\'s shapeshifter'/'VMA', '383148').

card_in_set('wall of diffusion', 'VMA').
card_original_type('wall of diffusion'/'VMA', 'Creature — Wall').
card_original_text('wall of diffusion'/'VMA', 'Defender\nWall of Diffusion can block creatures with shadow as though Wall of Diffusion had shadow.').
card_image_name('wall of diffusion'/'VMA', 'wall of diffusion').
card_uid('wall of diffusion'/'VMA', 'VMA:Wall of Diffusion:wall of diffusion').
card_rarity('wall of diffusion'/'VMA', 'Common').
card_artist('wall of diffusion'/'VMA', 'DiTerlizzi').
card_number('wall of diffusion'/'VMA', '191').
card_flavor_text('wall of diffusion'/'VMA', '\"The injury was being caught between this world and our own; the insult was to find walls within.\"\n—Lyna, soltari emissary').
card_multiverse_id('wall of diffusion'/'VMA', '383149').

card_in_set('waterfront bouncer', 'VMA').
card_original_type('waterfront bouncer'/'VMA', 'Creature — Merfolk Spellshaper').
card_original_text('waterfront bouncer'/'VMA', '{U}, {T}, Discard a card: Return target creature to its owner\'s hand.').
card_image_name('waterfront bouncer'/'VMA', 'waterfront bouncer').
card_uid('waterfront bouncer'/'VMA', 'VMA:Waterfront Bouncer:waterfront bouncer').
card_rarity('waterfront bouncer'/'VMA', 'Uncommon').
card_artist('waterfront bouncer'/'VMA', 'Paolo Parente').
card_number('waterfront bouncer'/'VMA', '102').
card_flavor_text('waterfront bouncer'/'VMA', 'Closing time comes earlier to some than to others.').
card_multiverse_id('waterfront bouncer'/'VMA', '383150').

card_in_set('wheel of fortune', 'VMA').
card_original_type('wheel of fortune'/'VMA', 'Sorcery').
card_original_text('wheel of fortune'/'VMA', 'Each player discards his or her hand and draws seven cards.').
card_image_name('wheel of fortune'/'VMA', 'wheel of fortune').
card_uid('wheel of fortune'/'VMA', 'VMA:Wheel of Fortune:wheel of fortune').
card_rarity('wheel of fortune'/'VMA', 'Mythic Rare').
card_artist('wheel of fortune'/'VMA', 'John Matson').
card_number('wheel of fortune'/'VMA', '192').
card_multiverse_id('wheel of fortune'/'VMA', '383151').

card_in_set('wild mongrel', 'VMA').
card_original_type('wild mongrel'/'VMA', 'Creature — Hound').
card_original_text('wild mongrel'/'VMA', 'Discard a card: Wild Mongrel gets +1/+1 and becomes the color of your choice until end of turn.').
card_image_name('wild mongrel'/'VMA', 'wild mongrel').
card_uid('wild mongrel'/'VMA', 'VMA:Wild Mongrel:wild mongrel').
card_rarity('wild mongrel'/'VMA', 'Common').
card_artist('wild mongrel'/'VMA', 'Anthony S. Waters').
card_number('wild mongrel'/'VMA', '239').
card_flavor_text('wild mongrel'/'VMA', 'It teaches you to play dead.').
card_multiverse_id('wild mongrel'/'VMA', '383152').

card_in_set('winds of rath', 'VMA').
card_original_type('winds of rath'/'VMA', 'Sorcery').
card_original_text('winds of rath'/'VMA', 'Destroy all creatures that aren\'t enchanted. They can\'t be regenerated.').
card_image_name('winds of rath'/'VMA', 'winds of rath').
card_uid('winds of rath'/'VMA', 'VMA:Winds of Rath:winds of rath').
card_rarity('winds of rath'/'VMA', 'Rare').
card_artist('winds of rath'/'VMA', 'Drew Tucker').
card_number('winds of rath'/'VMA', '53').
card_flavor_text('winds of rath'/'VMA', '\"There shall be a vast shout and then a vaster silence.\"\n—Oracle en-Vec').
card_multiverse_id('winds of rath'/'VMA', '383153').

card_in_set('worldgorger dragon', 'VMA').
card_original_type('worldgorger dragon'/'VMA', 'Creature — Nightmare Dragon').
card_original_text('worldgorger dragon'/'VMA', 'Flying, trample\nWhen Worldgorger Dragon enters the battlefield, exile all other permanents you control.\nWhen Worldgorger Dragon leaves the battlefield, return the exiled cards to the battlefield under their owners\' control.').
card_image_name('worldgorger dragon'/'VMA', 'worldgorger dragon').
card_uid('worldgorger dragon'/'VMA', 'VMA:Worldgorger Dragon:worldgorger dragon').
card_rarity('worldgorger dragon'/'VMA', 'Rare').
card_artist('worldgorger dragon'/'VMA', 'Wayne England').
card_number('worldgorger dragon'/'VMA', '193').
card_multiverse_id('worldgorger dragon'/'VMA', '383154').

card_in_set('yavimaya elder', 'VMA').
card_original_type('yavimaya elder'/'VMA', 'Creature — Human Druid').
card_original_text('yavimaya elder'/'VMA', 'When Yavimaya Elder dies, you may search your library for up to two basic land cards, reveal them, and put them into your hand. If you do, shuffle your library.\n{2}, Sacrifice Yavimaya Elder: Draw a card.').
card_image_name('yavimaya elder'/'VMA', 'yavimaya elder').
card_uid('yavimaya elder'/'VMA', 'VMA:Yavimaya Elder:yavimaya elder').
card_rarity('yavimaya elder'/'VMA', 'Uncommon').
card_artist('yavimaya elder'/'VMA', 'Matt Cavotta').
card_number('yavimaya elder'/'VMA', '240').
card_multiverse_id('yavimaya elder'/'VMA', '383155').

card_in_set('yavimaya hollow', 'VMA').
card_original_type('yavimaya hollow'/'VMA', 'Legendary Land').
card_original_text('yavimaya hollow'/'VMA', '{T}: Add {1} to your mana pool.\n{G}, {T}: Regenerate target creature.').
card_image_name('yavimaya hollow'/'VMA', 'yavimaya hollow').
card_uid('yavimaya hollow'/'VMA', 'VMA:Yavimaya Hollow:yavimaya hollow').
card_rarity('yavimaya hollow'/'VMA', 'Rare').
card_artist('yavimaya hollow'/'VMA', 'Douglas Shuler').
card_number('yavimaya hollow'/'VMA', '325').
card_flavor_text('yavimaya hollow'/'VMA', 'For all its traps and defenses, Yavimaya has its havens, too.').
card_multiverse_id('yavimaya hollow'/'VMA', '383156').

card_in_set('yawgmoth\'s bargain', 'VMA').
card_original_type('yawgmoth\'s bargain'/'VMA', 'Enchantment').
card_original_text('yawgmoth\'s bargain'/'VMA', 'Skip your draw step.\nPay 1 life: Draw a card.').
card_image_name('yawgmoth\'s bargain'/'VMA', 'yawgmoth\'s bargain').
card_uid('yawgmoth\'s bargain'/'VMA', 'VMA:Yawgmoth\'s Bargain:yawgmoth\'s bargain').
card_rarity('yawgmoth\'s bargain'/'VMA', 'Mythic Rare').
card_artist('yawgmoth\'s bargain'/'VMA', 'Michael Sutfin').
card_number('yawgmoth\'s bargain'/'VMA', '147').
card_flavor_text('yawgmoth\'s bargain'/'VMA', 'He craves only one commodity.').
card_multiverse_id('yawgmoth\'s bargain'/'VMA', '383157').

card_in_set('yawgmoth\'s will', 'VMA').
card_original_type('yawgmoth\'s will'/'VMA', 'Sorcery').
card_original_text('yawgmoth\'s will'/'VMA', 'Until end of turn, you may play cards from your graveyard.\nIf a card would be put into your graveyard from anywhere this turn, exile that card instead.').
card_image_name('yawgmoth\'s will'/'VMA', 'yawgmoth\'s will').
card_uid('yawgmoth\'s will'/'VMA', 'VMA:Yawgmoth\'s Will:yawgmoth\'s will').
card_rarity('yawgmoth\'s will'/'VMA', 'Mythic Rare').
card_artist('yawgmoth\'s will'/'VMA', 'Ron Spencer').
card_number('yawgmoth\'s will'/'VMA', '148').
card_multiverse_id('yawgmoth\'s will'/'VMA', '383158').

card_in_set('zhalfirin crusader', 'VMA').
card_original_type('zhalfirin crusader'/'VMA', 'Creature — Human Knight').
card_original_text('zhalfirin crusader'/'VMA', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)\n{1}{W}: The next 1 damage that would be dealt to Zhalfirin Crusader this turn is dealt to target creature or player instead.').
card_image_name('zhalfirin crusader'/'VMA', 'zhalfirin crusader').
card_uid('zhalfirin crusader'/'VMA', 'VMA:Zhalfirin Crusader:zhalfirin crusader').
card_rarity('zhalfirin crusader'/'VMA', 'Rare').
card_artist('zhalfirin crusader'/'VMA', 'Alan Rabinowitz').
card_number('zhalfirin crusader'/'VMA', '54').
card_flavor_text('zhalfirin crusader'/'VMA', '\"War is the crucible of leadership.\"\n—Rashida Scalebane').
card_multiverse_id('zhalfirin crusader'/'VMA', '383159').
