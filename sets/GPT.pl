% Guildpact

set('GPT').
set_name('GPT', 'Guildpact').
set_release_date('GPT', '2006-02-03').
set_border('GPT', 'black').
set_type('GPT', 'expansion').
set_block('GPT', 'Ravnica').

card_in_set('absolver thrull', 'GPT').
card_original_type('absolver thrull'/'GPT', 'Creature — Thrull Cleric').
card_original_text('absolver thrull'/'GPT', 'Haunt (When this card is put into a graveyard from play, remove it from the game haunting target creature.)\nWhen Absolver Thrull comes into play or the creature it haunts is put into a graveyard, destroy target enchantment.').
card_first_print('absolver thrull', 'GPT').
card_image_name('absolver thrull'/'GPT', 'absolver thrull').
card_uid('absolver thrull'/'GPT', 'GPT:Absolver Thrull:absolver thrull').
card_rarity('absolver thrull'/'GPT', 'Common').
card_artist('absolver thrull'/'GPT', 'Rob Alexander').
card_number('absolver thrull'/'GPT', '1').
card_multiverse_id('absolver thrull'/'GPT', '96953').
card_watermark('absolver thrull'/'GPT', 'Orzhov').

card_in_set('abyssal nocturnus', 'GPT').
card_original_type('abyssal nocturnus'/'GPT', 'Creature — Horror').
card_original_text('abyssal nocturnus'/'GPT', 'Whenever an opponent discards a card, Abyssal Nocturnus gets +2/+2 and gains fear until end of turn.').
card_first_print('abyssal nocturnus', 'GPT').
card_image_name('abyssal nocturnus'/'GPT', 'abyssal nocturnus').
card_uid('abyssal nocturnus'/'GPT', 'GPT:Abyssal Nocturnus:abyssal nocturnus').
card_rarity('abyssal nocturnus'/'GPT', 'Rare').
card_artist('abyssal nocturnus'/'GPT', 'Jon Foster').
card_number('abyssal nocturnus'/'GPT', '43').
card_flavor_text('abyssal nocturnus'/'GPT', '\"I fear to keep an open mind, for there are those who would use it as a door to my soul.\"\n—Voka Schlak,\nMauzam Asylum inmate, diary').
card_multiverse_id('abyssal nocturnus'/'GPT', '96858').

card_in_set('ætherplasm', 'GPT').
card_original_type('ætherplasm'/'GPT', 'Creature — Illusion').
card_original_text('ætherplasm'/'GPT', 'Whenever Ætherplasm blocks a creature, you may return Ætherplasm to its owner\'s hand. If you do, you may put a creature card from your hand into play blocking that creature.').
card_first_print('ætherplasm', 'GPT').
card_image_name('ætherplasm'/'GPT', 'aetherplasm').
card_uid('ætherplasm'/'GPT', 'GPT:Ætherplasm:aetherplasm').
card_rarity('ætherplasm'/'GPT', 'Uncommon').
card_artist('ætherplasm'/'GPT', 'Nottsuo').
card_number('ætherplasm'/'GPT', '22').
card_flavor_text('ætherplasm'/'GPT', 'Its fickle form holds hints of a thousand former identities.').
card_multiverse_id('ætherplasm'/'GPT', '96924').

card_in_set('agent of masks', 'GPT').
card_original_type('agent of masks'/'GPT', 'Creature — Human Advisor').
card_original_text('agent of masks'/'GPT', 'At the beginning of your upkeep, each opponent loses 1 life. You gain life equal to the life lost this way.').
card_first_print('agent of masks', 'GPT').
card_image_name('agent of masks'/'GPT', 'agent of masks').
card_uid('agent of masks'/'GPT', 'GPT:Agent of Masks:agent of masks').
card_rarity('agent of masks'/'GPT', 'Uncommon').
card_artist('agent of masks'/'GPT', 'Matt Thompson').
card_number('agent of masks'/'GPT', '100').
card_flavor_text('agent of masks'/'GPT', '\"You say that I am two-faced? Enough with the flattery. We have business to conduct.\"').
card_multiverse_id('agent of masks'/'GPT', '96892').
card_watermark('agent of masks'/'GPT', 'Orzhov').

card_in_set('angel of despair', 'GPT').
card_original_type('angel of despair'/'GPT', 'Creature — Angel').
card_original_text('angel of despair'/'GPT', 'Flying\nWhen Angel of Despair comes into play, destroy target permanent.').
card_first_print('angel of despair', 'GPT').
card_image_name('angel of despair'/'GPT', 'angel of despair').
card_uid('angel of despair'/'GPT', 'GPT:Angel of Despair:angel of despair').
card_rarity('angel of despair'/'GPT', 'Rare').
card_artist('angel of despair'/'GPT', 'Todd Lockwood').
card_number('angel of despair'/'GPT', '101').
card_flavor_text('angel of despair'/'GPT', '\"I feel in them a sense of duty and commitment, yet I can feel nothing else. It is as if their duty is to an empty void.\"\n—Razia').
card_multiverse_id('angel of despair'/'GPT', '83869').
card_watermark('angel of despair'/'GPT', 'Orzhov').

card_in_set('battering wurm', 'GPT').
card_original_type('battering wurm'/'GPT', 'Creature — Wurm').
card_original_text('battering wurm'/'GPT', 'Bloodthirst 1 (If an opponent was dealt damage this turn, this creature comes into play with a +1/+1 counter on it.)\nCreatures with power less than Battering Wurm\'s power can\'t block it.').
card_first_print('battering wurm', 'GPT').
card_image_name('battering wurm'/'GPT', 'battering wurm').
card_uid('battering wurm'/'GPT', 'GPT:Battering Wurm:battering wurm').
card_rarity('battering wurm'/'GPT', 'Uncommon').
card_artist('battering wurm'/'GPT', 'Darrell Riche').
card_number('battering wurm'/'GPT', '79').
card_multiverse_id('battering wurm'/'GPT', '96964').
card_watermark('battering wurm'/'GPT', 'Gruul').

card_in_set('beastmaster\'s magemark', 'GPT').
card_original_type('beastmaster\'s magemark'/'GPT', 'Enchantment — Aura').
card_original_text('beastmaster\'s magemark'/'GPT', 'Enchant creature\nCreatures you control that are enchanted get +1/+1.\nWhenever a creature you control that\'s enchanted becomes blocked, it gets +1/+1 until end of turn for each creature blocking it.').
card_first_print('beastmaster\'s magemark', 'GPT').
card_image_name('beastmaster\'s magemark'/'GPT', 'beastmaster\'s magemark').
card_uid('beastmaster\'s magemark'/'GPT', 'GPT:Beastmaster\'s Magemark:beastmaster\'s magemark').
card_rarity('beastmaster\'s magemark'/'GPT', 'Common').
card_artist('beastmaster\'s magemark'/'GPT', 'Brandon Kitkouski').
card_number('beastmaster\'s magemark'/'GPT', '80').
card_multiverse_id('beastmaster\'s magemark'/'GPT', '107689').

card_in_set('belfry spirit', 'GPT').
card_original_type('belfry spirit'/'GPT', 'Creature — Spirit').
card_original_text('belfry spirit'/'GPT', 'Flying\nHaunt (When this card is put into a graveyard from play, remove it from the game haunting target creature.)\nWhen Belfry Spirit comes into play or the creature it haunts is put into a graveyard, put two 1/1 black Bat creature tokens with flying into play.').
card_first_print('belfry spirit', 'GPT').
card_image_name('belfry spirit'/'GPT', 'belfry spirit').
card_uid('belfry spirit'/'GPT', 'GPT:Belfry Spirit:belfry spirit').
card_rarity('belfry spirit'/'GPT', 'Uncommon').
card_artist('belfry spirit'/'GPT', 'Daren Bader').
card_number('belfry spirit'/'GPT', '2').
card_multiverse_id('belfry spirit'/'GPT', '96962').
card_watermark('belfry spirit'/'GPT', 'Orzhov').

card_in_set('benediction of moons', 'GPT').
card_original_type('benediction of moons'/'GPT', 'Sorcery').
card_original_text('benediction of moons'/'GPT', 'You gain 1 life for each player.\nHaunt (When this spell card is put into a graveyard after resolving, remove it from the game haunting target creature.)\nWhen the creature Benediction of Moons haunts is put into a graveyard, you gain 1 life for each player.').
card_first_print('benediction of moons', 'GPT').
card_image_name('benediction of moons'/'GPT', 'benediction of moons').
card_uid('benediction of moons'/'GPT', 'GPT:Benediction of Moons:benediction of moons').
card_rarity('benediction of moons'/'GPT', 'Common').
card_artist('benediction of moons'/'GPT', 'Matt Cavotta').
card_number('benediction of moons'/'GPT', '3').
card_multiverse_id('benediction of moons'/'GPT', '96951').
card_watermark('benediction of moons'/'GPT', 'Orzhov').

card_in_set('bioplasm', 'GPT').
card_original_type('bioplasm'/'GPT', 'Creature — Ooze').
card_original_text('bioplasm'/'GPT', 'Whenever Bioplasm attacks, remove the top card of your library from the game. If it\'s a creature card, Bioplasm gets +X/+Y until end of turn, where X is the removed creature card\'s power and Y is its toughness. (A * on a card not in play is 0.)').
card_first_print('bioplasm', 'GPT').
card_image_name('bioplasm'/'GPT', 'bioplasm').
card_uid('bioplasm'/'GPT', 'GPT:Bioplasm:bioplasm').
card_rarity('bioplasm'/'GPT', 'Rare').
card_artist('bioplasm'/'GPT', 'Jon Foster').
card_number('bioplasm'/'GPT', '81').
card_multiverse_id('bioplasm'/'GPT', '96857').

card_in_set('blind hunter', 'GPT').
card_original_type('blind hunter'/'GPT', 'Creature — Bat').
card_original_text('blind hunter'/'GPT', 'Flying\nHaunt (When this card is put into a graveyard from play, remove it from the game haunting target creature.)\nWhen Blind Hunter comes into play or the creature it haunts is put into a graveyard, target player loses 2 life and you gain 2 life.').
card_first_print('blind hunter', 'GPT').
card_image_name('blind hunter'/'GPT', 'blind hunter').
card_uid('blind hunter'/'GPT', 'GPT:Blind Hunter:blind hunter').
card_rarity('blind hunter'/'GPT', 'Common').
card_artist('blind hunter'/'GPT', 'Warren Mahy').
card_number('blind hunter'/'GPT', '102').
card_multiverse_id('blind hunter'/'GPT', '97227').
card_watermark('blind hunter'/'GPT', 'Orzhov').

card_in_set('bloodscale prowler', 'GPT').
card_original_type('bloodscale prowler'/'GPT', 'Creature — Viashino Warrior').
card_original_text('bloodscale prowler'/'GPT', 'Bloodthirst 1 (If an opponent was dealt damage this turn, this creature comes into play with a +1/+1 counter on it.)').
card_first_print('bloodscale prowler', 'GPT').
card_image_name('bloodscale prowler'/'GPT', 'bloodscale prowler').
card_uid('bloodscale prowler'/'GPT', 'GPT:Bloodscale Prowler:bloodscale prowler').
card_rarity('bloodscale prowler'/'GPT', 'Common').
card_artist('bloodscale prowler'/'GPT', 'Lars Grant-West').
card_number('bloodscale prowler'/'GPT', '64').
card_flavor_text('bloodscale prowler'/'GPT', 'It tracks its victims by the scent of their breath, preferring to dine on those who recently ate well.').
card_multiverse_id('bloodscale prowler'/'GPT', '96836').
card_watermark('bloodscale prowler'/'GPT', 'Gruul').

card_in_set('borborygmos', 'GPT').
card_original_type('borborygmos'/'GPT', 'Legendary Creature — Cyclops').
card_original_text('borborygmos'/'GPT', 'Trample\nWhenever Borborygmos deals combat damage to a player, put a +1/+1 counter on each creature you control.').
card_first_print('borborygmos', 'GPT').
card_image_name('borborygmos'/'GPT', 'borborygmos').
card_uid('borborygmos'/'GPT', 'GPT:Borborygmos:borborygmos').
card_rarity('borborygmos'/'GPT', 'Rare').
card_artist('borborygmos'/'GPT', 'Todd Lockwood').
card_number('borborygmos'/'GPT', '103').
card_flavor_text('borborygmos'/'GPT', '\"It\'s easy to see why those Gruul dirtbags follow him—the only orders he gives are ‘Crush them!\' and ‘We eat!\'\"\n—Teysa').
card_multiverse_id('borborygmos'/'GPT', '97229').
card_watermark('borborygmos'/'GPT', 'Gruul').

card_in_set('burning-tree bloodscale', 'GPT').
card_original_type('burning-tree bloodscale'/'GPT', 'Creature — Viashino Berserker').
card_original_text('burning-tree bloodscale'/'GPT', 'Bloodthirst 1 (If an opponent was dealt damage this turn, this creature comes into play with a +1/+1 counter on it.)\n{2}{R}: Target creature can\'t block Burning-Tree Bloodscale this turn.\n{2}{G}: Target creature blocks Burning-Tree Bloodscale this turn if able.').
card_first_print('burning-tree bloodscale', 'GPT').
card_image_name('burning-tree bloodscale'/'GPT', 'burning-tree bloodscale').
card_uid('burning-tree bloodscale'/'GPT', 'GPT:Burning-Tree Bloodscale:burning-tree bloodscale').
card_rarity('burning-tree bloodscale'/'GPT', 'Common').
card_artist('burning-tree bloodscale'/'GPT', 'Kev Walker').
card_number('burning-tree bloodscale'/'GPT', '104').
card_multiverse_id('burning-tree bloodscale'/'GPT', '96898').
card_watermark('burning-tree bloodscale'/'GPT', 'Gruul').

card_in_set('burning-tree shaman', 'GPT').
card_original_type('burning-tree shaman'/'GPT', 'Creature — Centaur Shaman').
card_original_text('burning-tree shaman'/'GPT', 'Whenever a player plays an activated ability that isn\'t a mana ability, Burning-Tree Shaman deals 1 damage to that player.').
card_first_print('burning-tree shaman', 'GPT').
card_image_name('burning-tree shaman'/'GPT', 'burning-tree shaman').
card_uid('burning-tree shaman'/'GPT', 'GPT:Burning-Tree Shaman:burning-tree shaman').
card_rarity('burning-tree shaman'/'GPT', 'Rare').
card_artist('burning-tree shaman'/'GPT', 'Dan Scott').
card_number('burning-tree shaman'/'GPT', '105').
card_flavor_text('burning-tree shaman'/'GPT', 'Gruul shamans are bent on punishing the civilized. Any act more complex than rubbing sticks together or eating with utensils is met with the stinging burn of their magic.').
card_multiverse_id('burning-tree shaman'/'GPT', '97205').
card_watermark('burning-tree shaman'/'GPT', 'Gruul').

card_in_set('castigate', 'GPT').
card_original_type('castigate'/'GPT', 'Sorcery').
card_original_text('castigate'/'GPT', 'Target opponent reveals his or her hand. Choose a nonland card from it. Remove that card from the game.').
card_image_name('castigate'/'GPT', 'castigate').
card_uid('castigate'/'GPT', 'GPT:Castigate:castigate').
card_rarity('castigate'/'GPT', 'Common').
card_artist('castigate'/'GPT', 'Darrell Riche').
card_number('castigate'/'GPT', '106').
card_flavor_text('castigate'/'GPT', '\"We have no need for military might. We wield two of the sharpest swords ever forged: Faith in our left hand, Wealth in our right.\"\n—Vuliev of the Ghost Council').
card_multiverse_id('castigate'/'GPT', '97219').
card_watermark('castigate'/'GPT', 'Orzhov').

card_in_set('caustic rain', 'GPT').
card_original_type('caustic rain'/'GPT', 'Sorcery').
card_original_text('caustic rain'/'GPT', 'Remove target land from the game.').
card_first_print('caustic rain', 'GPT').
card_image_name('caustic rain'/'GPT', 'caustic rain').
card_uid('caustic rain'/'GPT', 'GPT:Caustic Rain:caustic rain').
card_rarity('caustic rain'/'GPT', 'Uncommon').
card_artist('caustic rain'/'GPT', 'Luca Zontini').
card_number('caustic rain'/'GPT', '44').
card_flavor_text('caustic rain'/'GPT', '\"Looking out the great windows of Vitu-Ghazi at the foundry stacks belching their smoke to the sky, I wonder when the sky will take its vengeance.\"\n—Heruj, Selesnya initiate').
card_multiverse_id('caustic rain'/'GPT', '96969').

card_in_set('cerebral vortex', 'GPT').
card_original_type('cerebral vortex'/'GPT', 'Instant').
card_original_text('cerebral vortex'/'GPT', 'Target player draws two cards, then Cerebral Vortex deals damage to that player equal to the number of cards he or she has drawn this turn.').
card_first_print('cerebral vortex', 'GPT').
card_image_name('cerebral vortex'/'GPT', 'cerebral vortex').
card_uid('cerebral vortex'/'GPT', 'GPT:Cerebral Vortex:cerebral vortex').
card_rarity('cerebral vortex'/'GPT', 'Rare').
card_artist('cerebral vortex'/'GPT', 'Jeremy Jarvis').
card_number('cerebral vortex'/'GPT', '107').
card_flavor_text('cerebral vortex'/'GPT', 'Izzet brains and Izzet boilers: contents under pressure.').
card_multiverse_id('cerebral vortex'/'GPT', '96907').
card_watermark('cerebral vortex'/'GPT', 'Izzet').

card_in_set('conjurer\'s ban', 'GPT').
card_original_type('conjurer\'s ban'/'GPT', 'Sorcery').
card_original_text('conjurer\'s ban'/'GPT', 'Name a card. Until your next turn, the named card can\'t be played.\nDraw a card.').
card_first_print('conjurer\'s ban', 'GPT').
card_image_name('conjurer\'s ban'/'GPT', 'conjurer\'s ban').
card_uid('conjurer\'s ban'/'GPT', 'GPT:Conjurer\'s Ban:conjurer\'s ban').
card_rarity('conjurer\'s ban'/'GPT', 'Uncommon').
card_artist('conjurer\'s ban'/'GPT', 'Pete Venters').
card_number('conjurer\'s ban'/'GPT', '108').
card_flavor_text('conjurer\'s ban'/'GPT', 'Orzhov faithful file past to have their minds purged of \"impure\" desires. There, the guiltwardens eliminate any thoughts of hope or self-sufficiency.').
card_multiverse_id('conjurer\'s ban'/'GPT', '97194').
card_watermark('conjurer\'s ban'/'GPT', 'Orzhov').

card_in_set('crash landing', 'GPT').
card_original_type('crash landing'/'GPT', 'Instant').
card_original_text('crash landing'/'GPT', 'Target creature with flying loses flying until end of turn. Crash Landing deals damage to that creature equal to the number of Forests you control.').
card_first_print('crash landing', 'GPT').
card_image_name('crash landing'/'GPT', 'crash landing').
card_uid('crash landing'/'GPT', 'GPT:Crash Landing:crash landing').
card_rarity('crash landing'/'GPT', 'Uncommon').
card_artist('crash landing'/'GPT', 'Darrell Riche').
card_number('crash landing'/'GPT', '82').
card_flavor_text('crash landing'/'GPT', '\"The other guilds think they\'re untouchable. It\'s time we brought them back down to earth.\"\n—Ghut Rak, Gruul guildmage').
card_multiverse_id('crash landing'/'GPT', '96918').

card_in_set('cremate', 'GPT').
card_original_type('cremate'/'GPT', 'Instant').
card_original_text('cremate'/'GPT', 'Remove target card in a graveyard from the game.\nDraw a card.').
card_image_name('cremate'/'GPT', 'cremate').
card_uid('cremate'/'GPT', 'GPT:Cremate:cremate').
card_rarity('cremate'/'GPT', 'Common').
card_artist('cremate'/'GPT', 'Paolo Parente').
card_number('cremate'/'GPT', '45').
card_flavor_text('cremate'/'GPT', 'Cremation of the dead is not a religious ritual in Ravnica. It\'s a business designed to keep the Golgari from growing in numbers.').
card_multiverse_id('cremate'/'GPT', '96936').

card_in_set('cry of contrition', 'GPT').
card_original_type('cry of contrition'/'GPT', 'Sorcery').
card_original_text('cry of contrition'/'GPT', 'Target player discards a card.\nHaunt (When this spell card is put into a graveyard after resolving, remove it from the game haunting target creature.)\nWhen the creature Cry of Contrition haunts is put into a graveyard, target player discards a card.').
card_first_print('cry of contrition', 'GPT').
card_image_name('cry of contrition'/'GPT', 'cry of contrition').
card_uid('cry of contrition'/'GPT', 'GPT:Cry of Contrition:cry of contrition').
card_rarity('cry of contrition'/'GPT', 'Common').
card_artist('cry of contrition'/'GPT', 'Daren Bader').
card_number('cry of contrition'/'GPT', '46').
card_multiverse_id('cry of contrition'/'GPT', '96961').
card_watermark('cry of contrition'/'GPT', 'Orzhov').

card_in_set('cryptwailing', 'GPT').
card_original_type('cryptwailing'/'GPT', 'Enchantment').
card_original_text('cryptwailing'/'GPT', '{1}, Remove two creature cards in your graveyard from the game: Target player discards a card. Play this ability only any time you could play a sorcery.').
card_first_print('cryptwailing', 'GPT').
card_image_name('cryptwailing'/'GPT', 'cryptwailing').
card_uid('cryptwailing'/'GPT', 'GPT:Cryptwailing:cryptwailing').
card_rarity('cryptwailing'/'GPT', 'Uncommon').
card_artist('cryptwailing'/'GPT', 'Nick Percival').
card_number('cryptwailing'/'GPT', '47').
card_flavor_text('cryptwailing'/'GPT', '\"Piece the tragic notes together and is it not a melody?\"\n—Teysa').
card_multiverse_id('cryptwailing'/'GPT', '96876').

card_in_set('crystal seer', 'GPT').
card_original_type('crystal seer'/'GPT', 'Creature — Vedalken Wizard').
card_original_text('crystal seer'/'GPT', 'When Crystal Seer comes into play, look at the top four cards of your library, then put them back in any order.\n{4}{U}: Return Crystal Seer to its owner\'s hand.').
card_first_print('crystal seer', 'GPT').
card_image_name('crystal seer'/'GPT', 'crystal seer').
card_uid('crystal seer'/'GPT', 'GPT:Crystal Seer:crystal seer').
card_rarity('crystal seer'/'GPT', 'Common').
card_artist('crystal seer'/'GPT', 'Glen Angus').
card_number('crystal seer'/'GPT', '23').
card_flavor_text('crystal seer'/'GPT', '\"Surprise is a useless, untidy emotion—the plaything of goblins and fools.\"').
card_multiverse_id('crystal seer'/'GPT', '83733').

card_in_set('culling sun', 'GPT').
card_original_type('culling sun'/'GPT', 'Sorcery').
card_original_text('culling sun'/'GPT', 'Destroy each creature with converted mana cost 3 or less.').
card_first_print('culling sun', 'GPT').
card_image_name('culling sun'/'GPT', 'culling sun').
card_uid('culling sun'/'GPT', 'GPT:Culling Sun:culling sun').
card_rarity('culling sun'/'GPT', 'Rare').
card_artist('culling sun'/'GPT', 'Daren Bader').
card_number('culling sun'/'GPT', '109').
card_flavor_text('culling sun'/'GPT', '\"Who truly runs this city? Look to the sky on hallowed days, and see whose sigil is stamped clearly onto the heavens.\"\n—Vuliev of the Ghost Council').
card_multiverse_id('culling sun'/'GPT', '97215').
card_watermark('culling sun'/'GPT', 'Orzhov').

card_in_set('daggerclaw imp', 'GPT').
card_original_type('daggerclaw imp'/'GPT', 'Creature — Imp').
card_original_text('daggerclaw imp'/'GPT', 'Flying\nDaggerclaw Imp can\'t block.').
card_first_print('daggerclaw imp', 'GPT').
card_image_name('daggerclaw imp'/'GPT', 'daggerclaw imp').
card_uid('daggerclaw imp'/'GPT', 'GPT:Daggerclaw Imp:daggerclaw imp').
card_rarity('daggerclaw imp'/'GPT', 'Uncommon').
card_artist('daggerclaw imp'/'GPT', 'Pete Venters').
card_number('daggerclaw imp'/'GPT', '48').
card_flavor_text('daggerclaw imp'/'GPT', 'The Simic use the claws as scalpels, while the Rakdos use them for tattooing and torture. The Gruul use them to pick their teeth after lunching on the rest of the carcass.').
card_multiverse_id('daggerclaw imp'/'GPT', '96949').

card_in_set('debtors\' knell', 'GPT').
card_original_type('debtors\' knell'/'GPT', 'Enchantment').
card_original_text('debtors\' knell'/'GPT', '({W/B} can be paid with either {W} or {B}.)\nAt the beginning of your upkeep, put target creature card in a graveyard into play under your control.').
card_first_print('debtors\' knell', 'GPT').
card_image_name('debtors\' knell'/'GPT', 'debtors\' knell').
card_uid('debtors\' knell'/'GPT', 'GPT:Debtors\' Knell:debtors\' knell').
card_rarity('debtors\' knell'/'GPT', 'Rare').
card_artist('debtors\' knell'/'GPT', 'Kev Walker').
card_number('debtors\' knell'/'GPT', '141').
card_flavor_text('debtors\' knell'/'GPT', 'One moment, conscious only of a sense of repose. The next moment, hearing the trudge of his own footsteps. He sighed and squinted into the glare ahead.').
card_multiverse_id('debtors\' knell'/'GPT', '97193').
card_watermark('debtors\' knell'/'GPT', 'Orzhov').

card_in_set('djinn illuminatus', 'GPT').
card_original_type('djinn illuminatus'/'GPT', 'Creature — Djinn').
card_original_text('djinn illuminatus'/'GPT', '({U/R} can be paid with either {U} or {R}.)\nFlying\nEach instant and sorcery spell you play has replicate. The replicate cost is equal to its mana cost. (When you play it, copy it for each time you paid its replicate cost. You may choose new targets for the copies.)').
card_image_name('djinn illuminatus'/'GPT', 'djinn illuminatus').
card_uid('djinn illuminatus'/'GPT', 'GPT:Djinn Illuminatus:djinn illuminatus').
card_rarity('djinn illuminatus'/'GPT', 'Rare').
card_artist('djinn illuminatus'/'GPT', 'Carl Critchlow').
card_number('djinn illuminatus'/'GPT', '142').
card_multiverse_id('djinn illuminatus'/'GPT', '97197').
card_watermark('djinn illuminatus'/'GPT', 'Izzet').

card_in_set('douse in gloom', 'GPT').
card_original_type('douse in gloom'/'GPT', 'Instant').
card_original_text('douse in gloom'/'GPT', 'Douse in Gloom deals 2 damage to target creature and you gain 2 life.').
card_first_print('douse in gloom', 'GPT').
card_image_name('douse in gloom'/'GPT', 'douse in gloom').
card_uid('douse in gloom'/'GPT', 'GPT:Douse in Gloom:douse in gloom').
card_rarity('douse in gloom'/'GPT', 'Common').
card_artist('douse in gloom'/'GPT', 'Kev Walker').
card_number('douse in gloom'/'GPT', '49').
card_flavor_text('douse in gloom'/'GPT', 'Orzhov prisoners are steeped in a blackened brew that robs their souls of strength. Patriarchs drink that brew to extend their own lives.').
card_multiverse_id('douse in gloom'/'GPT', '97221').

card_in_set('droning bureaucrats', 'GPT').
card_original_type('droning bureaucrats'/'GPT', 'Creature — Human Advisor').
card_original_text('droning bureaucrats'/'GPT', '{X}, {T}: Each creature with converted mana cost X can\'t attack or block this turn.').
card_first_print('droning bureaucrats', 'GPT').
card_image_name('droning bureaucrats'/'GPT', 'droning bureaucrats').
card_uid('droning bureaucrats'/'GPT', 'GPT:Droning Bureaucrats:droning bureaucrats').
card_rarity('droning bureaucrats'/'GPT', 'Uncommon').
card_artist('droning bureaucrats'/'GPT', 'Kev Walker').
card_number('droning bureaucrats'/'GPT', '4').
card_flavor_text('droning bureaucrats'/'GPT', '\" . . . and you must also apply for an application license, file documents 136(iv) and 22-C and -D in triplicate, pay all requisite fees, request a . . .\"').
card_multiverse_id('droning bureaucrats'/'GPT', '96929').

card_in_set('drowned rusalka', 'GPT').
card_original_type('drowned rusalka'/'GPT', 'Creature — Spirit').
card_original_text('drowned rusalka'/'GPT', '{U}, Sacrifice a creature: Discard a card, then draw a card.').
card_first_print('drowned rusalka', 'GPT').
card_image_name('drowned rusalka'/'GPT', 'drowned rusalka').
card_uid('drowned rusalka'/'GPT', 'GPT:Drowned Rusalka:drowned rusalka').
card_rarity('drowned rusalka'/'GPT', 'Uncommon').
card_artist('drowned rusalka'/'GPT', 'Dany Orizio').
card_number('drowned rusalka'/'GPT', '24').
card_flavor_text('drowned rusalka'/'GPT', 'Lurking below the current, she thirsts until others come to drink.').
card_multiverse_id('drowned rusalka'/'GPT', '107687').

card_in_set('dryad sophisticate', 'GPT').
card_original_type('dryad sophisticate'/'GPT', 'Creature — Dryad').
card_original_text('dryad sophisticate'/'GPT', 'Nonbasic landwalk').
card_first_print('dryad sophisticate', 'GPT').
card_image_name('dryad sophisticate'/'GPT', 'dryad sophisticate').
card_uid('dryad sophisticate'/'GPT', 'GPT:Dryad Sophisticate:dryad sophisticate').
card_rarity('dryad sophisticate'/'GPT', 'Uncommon').
card_artist('dryad sophisticate'/'GPT', 'Ron Spears').
card_number('dryad sophisticate'/'GPT', '83').
card_flavor_text('dryad sophisticate'/'GPT', 'From Tin Street to the Outcast Mansions, she walks unbidden and unbound, her graceful silence never shaken by the riot of city life around her.').
card_multiverse_id('dryad sophisticate'/'GPT', '96848').

card_in_set('dune-brood nephilim', 'GPT').
card_original_type('dune-brood nephilim'/'GPT', 'Creature — Nephilim').
card_original_text('dune-brood nephilim'/'GPT', 'Whenever Dune-Brood Nephilim deals combat damage to a player, put a 1/1 colorless Sand creature token into play for each land you control.').
card_first_print('dune-brood nephilim', 'GPT').
card_image_name('dune-brood nephilim'/'GPT', 'dune-brood nephilim').
card_uid('dune-brood nephilim'/'GPT', 'GPT:Dune-Brood Nephilim:dune-brood nephilim').
card_rarity('dune-brood nephilim'/'GPT', 'Rare').
card_artist('dune-brood nephilim'/'GPT', 'Jim Murray').
card_number('dune-brood nephilim'/'GPT', '110').
card_flavor_text('dune-brood nephilim'/'GPT', 'When it awoke, it spawned nameless thousands to herald its arrival.').
card_multiverse_id('dune-brood nephilim'/'GPT', '107091').

card_in_set('earth surge', 'GPT').
card_original_type('earth surge'/'GPT', 'Enchantment').
card_original_text('earth surge'/'GPT', 'Each land gets +2/+2 as long as it\'s a creature.').
card_first_print('earth surge', 'GPT').
card_image_name('earth surge'/'GPT', 'earth surge').
card_uid('earth surge'/'GPT', 'GPT:Earth Surge:earth surge').
card_rarity('earth surge'/'GPT', 'Rare').
card_artist('earth surge'/'GPT', 'Luca Zontini').
card_number('earth surge'/'GPT', '84').
card_flavor_text('earth surge'/'GPT', '\"Some gardeners sing to plants. I tell them stories—stories of how they once ruled the world, of the mortals who destroyed their kind, and of revenge.\"\n—Bougrat, druid of the Cult of Yore').
card_multiverse_id('earth surge'/'GPT', '97238').

card_in_set('electrolyze', 'GPT').
card_original_type('electrolyze'/'GPT', 'Instant').
card_original_text('electrolyze'/'GPT', 'Electrolyze deals 2 damage divided as you choose among any number of target creatures and/or players.\nDraw a card.').
card_image_name('electrolyze'/'GPT', 'electrolyze').
card_uid('electrolyze'/'GPT', 'GPT:Electrolyze:electrolyze').
card_rarity('electrolyze'/'GPT', 'Uncommon').
card_artist('electrolyze'/'GPT', 'Zoltan Boros & Gabor Szikszai').
card_number('electrolyze'/'GPT', '111').
card_flavor_text('electrolyze'/'GPT', 'The Izzet learn something from every lesson they teach.').
card_multiverse_id('electrolyze'/'GPT', '96829').
card_watermark('electrolyze'/'GPT', 'Izzet').

card_in_set('exhumer thrull', 'GPT').
card_original_type('exhumer thrull'/'GPT', 'Creature — Thrull').
card_original_text('exhumer thrull'/'GPT', 'Haunt (When this card is put into a graveyard from play, remove it from the game haunting target creature.)\nWhen Exhumer Thrull comes into play or the creature it haunts is put into a graveyard, return target creature card from your graveyard to your hand.').
card_first_print('exhumer thrull', 'GPT').
card_image_name('exhumer thrull'/'GPT', 'exhumer thrull').
card_uid('exhumer thrull'/'GPT', 'GPT:Exhumer Thrull:exhumer thrull').
card_rarity('exhumer thrull'/'GPT', 'Uncommon').
card_artist('exhumer thrull'/'GPT', 'Warren Mahy').
card_number('exhumer thrull'/'GPT', '50').
card_multiverse_id('exhumer thrull'/'GPT', '97225').
card_watermark('exhumer thrull'/'GPT', 'Orzhov').

card_in_set('fencer\'s magemark', 'GPT').
card_original_type('fencer\'s magemark'/'GPT', 'Enchantment — Aura').
card_original_text('fencer\'s magemark'/'GPT', 'Enchant creature\nCreatures you control that are enchanted get +1/+1 and have first strike.').
card_first_print('fencer\'s magemark', 'GPT').
card_image_name('fencer\'s magemark'/'GPT', 'fencer\'s magemark').
card_uid('fencer\'s magemark'/'GPT', 'GPT:Fencer\'s Magemark:fencer\'s magemark').
card_rarity('fencer\'s magemark'/'GPT', 'Common').
card_artist('fencer\'s magemark'/'GPT', 'Brandon Kitkouski').
card_number('fencer\'s magemark'/'GPT', '65').
card_flavor_text('fencer\'s magemark'/'GPT', '\"The first blow is the most important. It often negates the need for a second.\"\n—Agrus Kos').
card_multiverse_id('fencer\'s magemark'/'GPT', '107681').

card_in_set('feral animist', 'GPT').
card_original_type('feral animist'/'GPT', 'Creature — Goblin Shaman').
card_original_text('feral animist'/'GPT', '{3}: Feral Animist gets +X/+0 until end of turn, where X is its power.').
card_first_print('feral animist', 'GPT').
card_image_name('feral animist'/'GPT', 'feral animist').
card_uid('feral animist'/'GPT', 'GPT:Feral Animist:feral animist').
card_rarity('feral animist'/'GPT', 'Uncommon').
card_artist('feral animist'/'GPT', 'Ron Spears').
card_number('feral animist'/'GPT', '112').
card_flavor_text('feral animist'/'GPT', 'He chanted over a pot of boiling blood and honey. His brain buzzed like a nest of hornets, and his muscles rippled with the might of the bear.').
card_multiverse_id('feral animist'/'GPT', '96945').
card_watermark('feral animist'/'GPT', 'Gruul').

card_in_set('frazzle', 'GPT').
card_original_type('frazzle'/'GPT', 'Instant').
card_original_text('frazzle'/'GPT', 'Counter target nonblue spell.').
card_first_print('frazzle', 'GPT').
card_image_name('frazzle'/'GPT', 'frazzle').
card_uid('frazzle'/'GPT', 'GPT:Frazzle:frazzle').
card_rarity('frazzle'/'GPT', 'Uncommon').
card_artist('frazzle'/'GPT', 'Pete Venters').
card_number('frazzle'/'GPT', '25').
card_flavor_text('frazzle'/'GPT', 'The chant was interrupted, the components cooked, and Zok worried that it was his brain he smelled smoking.').
card_multiverse_id('frazzle'/'GPT', '96890').

card_in_set('gatherer of graces', 'GPT').
card_original_type('gatherer of graces'/'GPT', 'Creature — Human Druid').
card_original_text('gatherer of graces'/'GPT', 'Gatherer of Graces gets +1/+1 for each Aura attached to it.\nSacrifice an Aura: Regenerate Gatherer of Graces.').
card_first_print('gatherer of graces', 'GPT').
card_image_name('gatherer of graces'/'GPT', 'gatherer of graces').
card_uid('gatherer of graces'/'GPT', 'GPT:Gatherer of Graces:gatherer of graces').
card_rarity('gatherer of graces'/'GPT', 'Uncommon').
card_artist('gatherer of graces'/'GPT', 'Heather Hudson').
card_number('gatherer of graces'/'GPT', '85').
card_flavor_text('gatherer of graces'/'GPT', '\"Garb is for common folk. I prefer to wear insight and zeal, ambition and flame.\"').
card_multiverse_id('gatherer of graces'/'GPT', '96877').

card_in_set('gelectrode', 'GPT').
card_original_type('gelectrode'/'GPT', 'Creature — Weird').
card_original_text('gelectrode'/'GPT', '{T}: Gelectrode deals 1 damage to target creature or player.\nWhenever you play an instant or sorcery spell, you may untap Gelectrode.').
card_first_print('gelectrode', 'GPT').
card_image_name('gelectrode'/'GPT', 'gelectrode').
card_uid('gelectrode'/'GPT', 'GPT:Gelectrode:gelectrode').
card_rarity('gelectrode'/'GPT', 'Uncommon').
card_artist('gelectrode'/'GPT', 'Dan Scott').
card_number('gelectrode'/'GPT', '113').
card_flavor_text('gelectrode'/'GPT', '\"Diametrically opposing energies in self-sealed plasmodermic bubbles make great pets!\"\n—Trivaz, Izzet mage').
card_multiverse_id('gelectrode'/'GPT', '96891').
card_watermark('gelectrode'/'GPT', 'Izzet').

card_in_set('ghor-clan bloodscale', 'GPT').
card_original_type('ghor-clan bloodscale'/'GPT', 'Creature — Viashino Warrior').
card_original_text('ghor-clan bloodscale'/'GPT', 'First strike\n{3}{G}: Ghor-Clan Bloodscale gets +2/+2 until end of turn. Play this ability only once each turn.').
card_first_print('ghor-clan bloodscale', 'GPT').
card_image_name('ghor-clan bloodscale'/'GPT', 'ghor-clan bloodscale').
card_uid('ghor-clan bloodscale'/'GPT', 'GPT:Ghor-Clan Bloodscale:ghor-clan bloodscale').
card_rarity('ghor-clan bloodscale'/'GPT', 'Uncommon').
card_artist('ghor-clan bloodscale'/'GPT', 'Paolo Parente').
card_number('ghor-clan bloodscale'/'GPT', '66').
card_flavor_text('ghor-clan bloodscale'/'GPT', 'At mealtimes, the Gruul customarily omit such niceties as using forks or killing the entrée.').
card_multiverse_id('ghor-clan bloodscale'/'GPT', '96834').
card_watermark('ghor-clan bloodscale'/'GPT', 'Gruul').

card_in_set('ghor-clan savage', 'GPT').
card_original_type('ghor-clan savage'/'GPT', 'Creature — Centaur Berserker').
card_original_text('ghor-clan savage'/'GPT', 'Bloodthirst 3 (If an opponent was dealt damage this turn, this creature comes into play with three +1/+1 counters on it.)').
card_first_print('ghor-clan savage', 'GPT').
card_image_name('ghor-clan savage'/'GPT', 'ghor-clan savage').
card_uid('ghor-clan savage'/'GPT', 'GPT:Ghor-Clan Savage:ghor-clan savage').
card_rarity('ghor-clan savage'/'GPT', 'Common').
card_artist('ghor-clan savage'/'GPT', 'Greg Staples').
card_number('ghor-clan savage'/'GPT', '86').
card_flavor_text('ghor-clan savage'/'GPT', 'The snap of sinew. The crunch of bone. A cry for mercy. These are the music of the Gruul.').
card_multiverse_id('ghor-clan savage'/'GPT', '96849').
card_watermark('ghor-clan savage'/'GPT', 'Gruul').

card_in_set('ghost council of orzhova', 'GPT').
card_original_type('ghost council of orzhova'/'GPT', 'Legendary Creature — Spirit Lord').
card_original_text('ghost council of orzhova'/'GPT', 'When Ghost Council of Orzhova comes into play, target opponent loses 1 life and you gain 1 life.\n{1}, Sacrifice a creature: Remove Ghost Council of Orzhova from the game. Return it to play under its owner\'s control at end of turn.').
card_first_print('ghost council of orzhova', 'GPT').
card_image_name('ghost council of orzhova'/'GPT', 'ghost council of orzhova').
card_uid('ghost council of orzhova'/'GPT', 'GPT:Ghost Council of Orzhova:ghost council of orzhova').
card_rarity('ghost council of orzhova'/'GPT', 'Rare').
card_artist('ghost council of orzhova'/'GPT', 'Greg Staples').
card_number('ghost council of orzhova'/'GPT', '114').
card_multiverse_id('ghost council of orzhova'/'GPT', '96908').
card_watermark('ghost council of orzhova'/'GPT', 'Orzhov').

card_in_set('ghost warden', 'GPT').
card_original_type('ghost warden'/'GPT', 'Creature — Spirit').
card_original_text('ghost warden'/'GPT', '{T}: Target creature gets +1/+1 until end of turn.').
card_first_print('ghost warden', 'GPT').
card_image_name('ghost warden'/'GPT', 'ghost warden').
card_uid('ghost warden'/'GPT', 'GPT:Ghost Warden:ghost warden').
card_rarity('ghost warden'/'GPT', 'Common').
card_artist('ghost warden'/'GPT', 'Ittoku').
card_number('ghost warden'/'GPT', '5').
card_flavor_text('ghost warden'/'GPT', '\"I thought of fate as an iron lattice, intricate but rigidly unchangeable. That was until some force bent fate\'s bars to spare my life.\"\n—Ilromov, traveling storyteller').
card_multiverse_id('ghost warden'/'GPT', '96901').

card_in_set('ghostway', 'GPT').
card_original_type('ghostway'/'GPT', 'Instant').
card_original_text('ghostway'/'GPT', 'Remove each creature you control from the game. Return those creatures to play under their owners\' control at end of turn.').
card_first_print('ghostway', 'GPT').
card_image_name('ghostway'/'GPT', 'ghostway').
card_uid('ghostway'/'GPT', 'GPT:Ghostway:ghostway').
card_rarity('ghostway'/'GPT', 'Rare').
card_artist('ghostway'/'GPT', 'Jim Murray').
card_number('ghostway'/'GPT', '6').
card_flavor_text('ghostway'/'GPT', '\"I watched its blade swing through me, but I was hollow, empty. I saw its face contort in rage but could not hear it snarl.\"\n—Klattic, Boros legionnaire').
card_multiverse_id('ghostway'/'GPT', '96842').

card_in_set('giant solifuge', 'GPT').
card_original_type('giant solifuge'/'GPT', 'Creature — Insect').
card_original_text('giant solifuge'/'GPT', '({R/G} can be paid with either {R} or {G}.)\nTrample, haste\nGiant Solifuge can\'t be the target of spells or abilities.').
card_first_print('giant solifuge', 'GPT').
card_image_name('giant solifuge'/'GPT', 'giant solifuge').
card_uid('giant solifuge'/'GPT', 'GPT:Giant Solifuge:giant solifuge').
card_rarity('giant solifuge'/'GPT', 'Rare').
card_artist('giant solifuge'/'GPT', 'Pat Lee').
card_number('giant solifuge'/'GPT', '143').
card_flavor_text('giant solifuge'/'GPT', '\"We respect all lifeforms, but this one we respect from a distance.\"\n—Mandor, Selesnya ranger').
card_multiverse_id('giant solifuge'/'GPT', '106576').
card_watermark('giant solifuge'/'GPT', 'Gruul').

card_in_set('gigadrowse', 'GPT').
card_original_type('gigadrowse'/'GPT', 'Instant').
card_original_text('gigadrowse'/'GPT', 'Replicate {U} (When you play this spell, copy it for each time you paid its replicate cost. You may choose new targets for the copies.)\nTap target permanent.').
card_first_print('gigadrowse', 'GPT').
card_image_name('gigadrowse'/'GPT', 'gigadrowse').
card_uid('gigadrowse'/'GPT', 'GPT:Gigadrowse:gigadrowse').
card_rarity('gigadrowse'/'GPT', 'Common').
card_artist('gigadrowse'/'GPT', 'Alex Horley-Orlandelli').
card_number('gigadrowse'/'GPT', '26').
card_multiverse_id('gigadrowse'/'GPT', '96864').
card_watermark('gigadrowse'/'GPT', 'Izzet').

card_in_set('glint-eye nephilim', 'GPT').
card_original_type('glint-eye nephilim'/'GPT', 'Creature — Nephilim').
card_original_text('glint-eye nephilim'/'GPT', 'Whenever Glint-Eye Nephilim deals combat damage to a player, draw that many cards.\n{1}, Discard a card: Glint-Eye Nephilim gets +1/+1 until end of turn.').
card_first_print('glint-eye nephilim', 'GPT').
card_image_name('glint-eye nephilim'/'GPT', 'glint-eye nephilim').
card_uid('glint-eye nephilim'/'GPT', 'GPT:Glint-Eye Nephilim:glint-eye nephilim').
card_rarity('glint-eye nephilim'/'GPT', 'Rare').
card_artist('glint-eye nephilim'/'GPT', 'Mark Zug').
card_number('glint-eye nephilim'/'GPT', '115').
card_flavor_text('glint-eye nephilim'/'GPT', 'When it awoke, it shook the plane with the thunder of its craving.').
card_multiverse_id('glint-eye nephilim'/'GPT', '107094').

card_in_set('goblin flectomancer', 'GPT').
card_original_type('goblin flectomancer'/'GPT', 'Creature — Goblin Wizard').
card_original_text('goblin flectomancer'/'GPT', 'Sacrifice Goblin Flectomancer: You may change the targets of target instant or sorcery spell.').
card_first_print('goblin flectomancer', 'GPT').
card_image_name('goblin flectomancer'/'GPT', 'goblin flectomancer').
card_uid('goblin flectomancer'/'GPT', 'GPT:Goblin Flectomancer:goblin flectomancer').
card_rarity('goblin flectomancer'/'GPT', 'Uncommon').
card_artist('goblin flectomancer'/'GPT', 'Matt Cavotta').
card_number('goblin flectomancer'/'GPT', '116').
card_flavor_text('goblin flectomancer'/'GPT', 'Rerouting magic through a goblin\'s brain is the surest way to throw it wildly off course.').
card_multiverse_id('goblin flectomancer'/'GPT', '97211').
card_watermark('goblin flectomancer'/'GPT', 'Izzet').

card_in_set('godless shrine', 'GPT').
card_original_type('godless shrine'/'GPT', 'Land — Plains Swamp').
card_original_text('godless shrine'/'GPT', '({T}: Add {W} or {B} to your mana pool.)\nAs Godless Shrine comes into play, you may pay 2 life. If you don\'t, Godless Shrine comes into play tapped instead.').
card_first_print('godless shrine', 'GPT').
card_image_name('godless shrine'/'GPT', 'godless shrine').
card_uid('godless shrine'/'GPT', 'GPT:Godless Shrine:godless shrine').
card_rarity('godless shrine'/'GPT', 'Rare').
card_artist('godless shrine'/'GPT', 'Rob Alexander').
card_number('godless shrine'/'GPT', '157').
card_multiverse_id('godless shrine'/'GPT', '96935').
card_watermark('godless shrine'/'GPT', 'Orzhov').

card_in_set('graven dominator', 'GPT').
card_original_type('graven dominator'/'GPT', 'Creature — Gargoyle').
card_original_text('graven dominator'/'GPT', 'Flying\nHaunt (When this card is put into a graveyard from play, remove it from the game haunting target creature.)\nWhen Graven Dominator comes into play or the creature it haunts is put into a graveyard, each other creature becomes 1/1 until end of turn.').
card_first_print('graven dominator', 'GPT').
card_image_name('graven dominator'/'GPT', 'graven dominator').
card_uid('graven dominator'/'GPT', 'GPT:Graven Dominator:graven dominator').
card_rarity('graven dominator'/'GPT', 'Rare').
card_artist('graven dominator'/'GPT', 'Carl Critchlow').
card_number('graven dominator'/'GPT', '7').
card_multiverse_id('graven dominator'/'GPT', '97228').
card_watermark('graven dominator'/'GPT', 'Orzhov').

card_in_set('gristleback', 'GPT').
card_original_type('gristleback'/'GPT', 'Creature — Beast').
card_original_text('gristleback'/'GPT', 'Bloodthirst 1 (If an opponent was dealt damage this turn, this creature comes into play with a +1/+1 counter on it.)\nSacrifice Gristleback: You gain life equal to Gristleback\'s power.').
card_first_print('gristleback', 'GPT').
card_image_name('gristleback'/'GPT', 'gristleback').
card_uid('gristleback'/'GPT', 'GPT:Gristleback:gristleback').
card_rarity('gristleback'/'GPT', 'Uncommon').
card_artist('gristleback'/'GPT', 'Lars Grant-West').
card_number('gristleback'/'GPT', '87').
card_flavor_text('gristleback'/'GPT', 'Loyal in battle, hearty in stew.').
card_multiverse_id('gristleback'/'GPT', '96833').
card_watermark('gristleback'/'GPT', 'Gruul').

card_in_set('gruul guildmage', 'GPT').
card_original_type('gruul guildmage'/'GPT', 'Creature — Human Shaman').
card_original_text('gruul guildmage'/'GPT', '({R/G} can be paid with either {R} or {G}.)\n{3}{R}, Sacrifice a land: Gruul Guildmage deals 2 damage to target player.\n{3}{G}: Target creature gets +2/+2 until end of turn.').
card_image_name('gruul guildmage'/'GPT', 'gruul guildmage').
card_uid('gruul guildmage'/'GPT', 'GPT:Gruul Guildmage:gruul guildmage').
card_rarity('gruul guildmage'/'GPT', 'Uncommon').
card_artist('gruul guildmage'/'GPT', 'Paolo Parente').
card_number('gruul guildmage'/'GPT', '144').
card_multiverse_id('gruul guildmage'/'GPT', '96837').
card_watermark('gruul guildmage'/'GPT', 'Gruul').

card_in_set('gruul nodorog', 'GPT').
card_original_type('gruul nodorog'/'GPT', 'Creature — Beast').
card_original_text('gruul nodorog'/'GPT', '{R}: Gruul Nodorog can\'t be blocked this turn except by two or more creatures.').
card_first_print('gruul nodorog', 'GPT').
card_image_name('gruul nodorog'/'GPT', 'gruul nodorog').
card_uid('gruul nodorog'/'GPT', 'GPT:Gruul Nodorog:gruul nodorog').
card_rarity('gruul nodorog'/'GPT', 'Common').
card_artist('gruul nodorog'/'GPT', 'Pete Venters').
card_number('gruul nodorog'/'GPT', '88').
card_flavor_text('gruul nodorog'/'GPT', 'Petting the nodorog\n—Gruul expression meaning\n\"making a really big mistake\"').
card_multiverse_id('gruul nodorog'/'GPT', '107683').
card_watermark('gruul nodorog'/'GPT', 'Gruul').

card_in_set('gruul scrapper', 'GPT').
card_original_type('gruul scrapper'/'GPT', 'Creature — Human Berserker').
card_original_text('gruul scrapper'/'GPT', 'When Gruul Scrapper comes into play, if {R} was spent to play Gruul Scrapper, it gains haste until end of turn.').
card_first_print('gruul scrapper', 'GPT').
card_image_name('gruul scrapper'/'GPT', 'gruul scrapper').
card_uid('gruul scrapper'/'GPT', 'GPT:Gruul Scrapper:gruul scrapper').
card_rarity('gruul scrapper'/'GPT', 'Common').
card_artist('gruul scrapper'/'GPT', 'Liam Sharp').
card_number('gruul scrapper'/'GPT', '89').
card_flavor_text('gruul scrapper'/'GPT', 'The Gruul aren\'t satisfied with just smashing things. They continue smashing other things with the things they just smashed.').
card_multiverse_id('gruul scrapper'/'GPT', '96887').
card_watermark('gruul scrapper'/'GPT', 'Gruul').

card_in_set('gruul signet', 'GPT').
card_original_type('gruul signet'/'GPT', 'Artifact').
card_original_text('gruul signet'/'GPT', '{1}, {T}: Add {R}{G} to your mana pool.').
card_first_print('gruul signet', 'GPT').
card_image_name('gruul signet'/'GPT', 'gruul signet').
card_uid('gruul signet'/'GPT', 'GPT:Gruul Signet:gruul signet').
card_rarity('gruul signet'/'GPT', 'Common').
card_artist('gruul signet'/'GPT', 'Tim Hildebrandt').
card_number('gruul signet'/'GPT', '150').
card_flavor_text('gruul signet'/'GPT', 'Gruul territorial markings need not be legible. The blood, snot, and muck used to smear them are unmistakably Gruul.').
card_multiverse_id('gruul signet'/'GPT', '96859').
card_watermark('gruul signet'/'GPT', 'Gruul').

card_in_set('gruul turf', 'GPT').
card_original_type('gruul turf'/'GPT', 'Land').
card_original_text('gruul turf'/'GPT', 'Gruul Turf comes into play tapped.\nWhen Gruul Turf comes into play, return a land you control to its owner\'s hand.\n{T}: Add {R}{G} to your mana pool.').
card_first_print('gruul turf', 'GPT').
card_image_name('gruul turf'/'GPT', 'gruul turf').
card_uid('gruul turf'/'GPT', 'GPT:Gruul Turf:gruul turf').
card_rarity('gruul turf'/'GPT', 'Common').
card_artist('gruul turf'/'GPT', 'John Avon').
card_number('gruul turf'/'GPT', '158').
card_multiverse_id('gruul turf'/'GPT', '97223').
card_watermark('gruul turf'/'GPT', 'Gruul').

card_in_set('gruul war plow', 'GPT').
card_original_type('gruul war plow'/'GPT', 'Artifact').
card_original_text('gruul war plow'/'GPT', 'Creatures you control have trample.\n{1}{R}{G}: Gruul War Plow becomes a 4/4 Juggernaut artifact creature until end of turn.').
card_first_print('gruul war plow', 'GPT').
card_image_name('gruul war plow'/'GPT', 'gruul war plow').
card_uid('gruul war plow'/'GPT', 'GPT:Gruul War Plow:gruul war plow').
card_rarity('gruul war plow'/'GPT', 'Rare').
card_artist('gruul war plow'/'GPT', 'Martina Pilcerova').
card_number('gruul war plow'/'GPT', '151').
card_flavor_text('gruul war plow'/'GPT', '\"Steering apparatus?! What for? Rip it out, sharpen it, and lash it to the front!\"\n—Ktank, Gruul plowmaster').
card_multiverse_id('gruul war plow'/'GPT', '107098').
card_watermark('gruul war plow'/'GPT', 'Gruul').

card_in_set('guardian\'s magemark', 'GPT').
card_original_type('guardian\'s magemark'/'GPT', 'Enchantment — Aura').
card_original_text('guardian\'s magemark'/'GPT', 'You may play Guardian\'s Magemark any time you could play an instant.\nEnchant creature\nCreatures you control that are enchanted get +1/+1.').
card_first_print('guardian\'s magemark', 'GPT').
card_image_name('guardian\'s magemark'/'GPT', 'guardian\'s magemark').
card_uid('guardian\'s magemark'/'GPT', 'GPT:Guardian\'s Magemark:guardian\'s magemark').
card_rarity('guardian\'s magemark'/'GPT', 'Common').
card_artist('guardian\'s magemark'/'GPT', 'Brandon Kitkouski').
card_number('guardian\'s magemark'/'GPT', '8').
card_flavor_text('guardian\'s magemark'/'GPT', 'The soldier moved with fluid poise, his awareness extending far beyond five senses.').
card_multiverse_id('guardian\'s magemark'/'GPT', '107688').

card_in_set('harrier griffin', 'GPT').
card_original_type('harrier griffin'/'GPT', 'Creature — Griffin').
card_original_text('harrier griffin'/'GPT', 'Flying\nAt the beginning of your upkeep, tap target creature.').
card_first_print('harrier griffin', 'GPT').
card_image_name('harrier griffin'/'GPT', 'harrier griffin').
card_uid('harrier griffin'/'GPT', 'GPT:Harrier Griffin:harrier griffin').
card_rarity('harrier griffin'/'GPT', 'Uncommon').
card_artist('harrier griffin'/'GPT', 'Jim Nelson').
card_number('harrier griffin'/'GPT', '9').
card_flavor_text('harrier griffin'/'GPT', 'Moon Market merchants sell a bottled scent to cutpurses and other criminals. Those doused in the liquid become \"griffin bait.\"').
card_multiverse_id('harrier griffin'/'GPT', '96843').

card_in_set('hatching plans', 'GPT').
card_original_type('hatching plans'/'GPT', 'Enchantment').
card_original_text('hatching plans'/'GPT', 'When Hatching Plans is put into a graveyard from play, draw three cards.').
card_first_print('hatching plans', 'GPT').
card_image_name('hatching plans'/'GPT', 'hatching plans').
card_uid('hatching plans'/'GPT', 'GPT:Hatching Plans:hatching plans').
card_rarity('hatching plans'/'GPT', 'Rare').
card_artist('hatching plans'/'GPT', 'Heather Hudson').
card_number('hatching plans'/'GPT', '27').
card_flavor_text('hatching plans'/'GPT', 'So wondrous to behold, so delicate and finely crafted—and yet, such a pleasure to smash.').
card_multiverse_id('hatching plans'/'GPT', '96873').

card_in_set('hissing miasma', 'GPT').
card_original_type('hissing miasma'/'GPT', 'Enchantment').
card_original_text('hissing miasma'/'GPT', 'Whenever a creature attacks you, its controller loses 1 life.').
card_first_print('hissing miasma', 'GPT').
card_image_name('hissing miasma'/'GPT', 'hissing miasma').
card_uid('hissing miasma'/'GPT', 'GPT:Hissing Miasma:hissing miasma').
card_rarity('hissing miasma'/'GPT', 'Uncommon').
card_artist('hissing miasma'/'GPT', 'Ben Thompson').
card_number('hissing miasma'/'GPT', '51').
card_flavor_text('hissing miasma'/'GPT', 'All that remains when the fog finishes are neat piles of polished bones, like acid-etched glass. Orzhov servants collect them to make altar chimes.').
card_multiverse_id('hissing miasma'/'GPT', '96922').

card_in_set('hypervolt grasp', 'GPT').
card_original_type('hypervolt grasp'/'GPT', 'Enchantment — Aura').
card_original_text('hypervolt grasp'/'GPT', 'Enchant creature\nEnchanted creature has \"{T}: This creature deals 1 damage to target creature or player.\"\n{1}{U}: Return Hypervolt Grasp to its owner\'s hand.').
card_first_print('hypervolt grasp', 'GPT').
card_image_name('hypervolt grasp'/'GPT', 'hypervolt grasp').
card_uid('hypervolt grasp'/'GPT', 'GPT:Hypervolt Grasp:hypervolt grasp').
card_rarity('hypervolt grasp'/'GPT', 'Uncommon').
card_artist('hypervolt grasp'/'GPT', 'Mark Romanoski').
card_number('hypervolt grasp'/'GPT', '67').
card_flavor_text('hypervolt grasp'/'GPT', 'Zap on. Zap off.').
card_multiverse_id('hypervolt grasp'/'GPT', '96840').
card_watermark('hypervolt grasp'/'GPT', 'Izzet').

card_in_set('infiltrator\'s magemark', 'GPT').
card_original_type('infiltrator\'s magemark'/'GPT', 'Enchantment — Aura').
card_original_text('infiltrator\'s magemark'/'GPT', 'Enchant creature\nCreatures you control that are enchanted get +1/+1 and can\'t be blocked except by creatures with defender.').
card_first_print('infiltrator\'s magemark', 'GPT').
card_image_name('infiltrator\'s magemark'/'GPT', 'infiltrator\'s magemark').
card_uid('infiltrator\'s magemark'/'GPT', 'GPT:Infiltrator\'s Magemark:infiltrator\'s magemark').
card_rarity('infiltrator\'s magemark'/'GPT', 'Common').
card_artist('infiltrator\'s magemark'/'GPT', 'Brandon Kitkouski').
card_number('infiltrator\'s magemark'/'GPT', '28').
card_flavor_text('infiltrator\'s magemark'/'GPT', '\"In secrecy is strength. None can oppose one who cannot be found.\"\n—Szadek').
card_multiverse_id('infiltrator\'s magemark'/'GPT', '107684').

card_in_set('ink-treader nephilim', 'GPT').
card_original_type('ink-treader nephilim'/'GPT', 'Creature — Nephilim').
card_original_text('ink-treader nephilim'/'GPT', 'Whenever a player plays an instant or sorcery spell, if Ink-Treader Nephilim is the only target of that spell, copy the spell for each other creature that spell could target. Each copy targets a different one of those creatures.').
card_first_print('ink-treader nephilim', 'GPT').
card_image_name('ink-treader nephilim'/'GPT', 'ink-treader nephilim').
card_uid('ink-treader nephilim'/'GPT', 'GPT:Ink-Treader Nephilim:ink-treader nephilim').
card_rarity('ink-treader nephilim'/'GPT', 'Rare').
card_artist('ink-treader nephilim'/'GPT', 'Christopher Moeller').
card_number('ink-treader nephilim'/'GPT', '117').
card_flavor_text('ink-treader nephilim'/'GPT', 'When it awoke, the mirrors of the world reflected only darkness.').
card_multiverse_id('ink-treader nephilim'/'GPT', '107092').

card_in_set('invoke the firemind', 'GPT').
card_original_type('invoke the firemind'/'GPT', 'Sorcery').
card_original_text('invoke the firemind'/'GPT', 'Choose one Draw X cards; or Invoke the Firemind deals X damage to target creature or player.').
card_first_print('invoke the firemind', 'GPT').
card_image_name('invoke the firemind'/'GPT', 'invoke the firemind').
card_uid('invoke the firemind'/'GPT', 'GPT:Invoke the Firemind:invoke the firemind').
card_rarity('invoke the firemind'/'GPT', 'Rare').
card_artist('invoke the firemind'/'GPT', 'Zoltan Boros & Gabor Szikszai').
card_number('invoke the firemind'/'GPT', '118').
card_flavor_text('invoke the firemind'/'GPT', 'To those in tune with the Firemind, there is no difference between knowledge and flame.').
card_multiverse_id('invoke the firemind'/'GPT', '83630').
card_watermark('invoke the firemind'/'GPT', 'Izzet').

card_in_set('izzet boilerworks', 'GPT').
card_original_type('izzet boilerworks'/'GPT', 'Land').
card_original_text('izzet boilerworks'/'GPT', 'Izzet Boilerworks comes into play tapped.\nWhen Izzet Boilerworks comes into play, return a land you control to its owner\'s hand.\n{T}: Add {U}{R} to your mana pool.').
card_first_print('izzet boilerworks', 'GPT').
card_image_name('izzet boilerworks'/'GPT', 'izzet boilerworks').
card_uid('izzet boilerworks'/'GPT', 'GPT:Izzet Boilerworks:izzet boilerworks').
card_rarity('izzet boilerworks'/'GPT', 'Common').
card_artist('izzet boilerworks'/'GPT', 'John Avon').
card_number('izzet boilerworks'/'GPT', '159').
card_multiverse_id('izzet boilerworks'/'GPT', '97220').
card_watermark('izzet boilerworks'/'GPT', 'Izzet').

card_in_set('izzet chronarch', 'GPT').
card_original_type('izzet chronarch'/'GPT', 'Creature — Human Wizard').
card_original_text('izzet chronarch'/'GPT', 'When Izzet Chronarch comes into play, return target instant or sorcery card from your graveyard to your hand.').
card_first_print('izzet chronarch', 'GPT').
card_image_name('izzet chronarch'/'GPT', 'izzet chronarch').
card_uid('izzet chronarch'/'GPT', 'GPT:Izzet Chronarch:izzet chronarch').
card_rarity('izzet chronarch'/'GPT', 'Common').
card_artist('izzet chronarch'/'GPT', 'Nick Percival').
card_number('izzet chronarch'/'GPT', '119').
card_flavor_text('izzet chronarch'/'GPT', 'He ensures not only whether but also when and where the lightning strikes twice.').
card_multiverse_id('izzet chronarch'/'GPT', '97202').
card_watermark('izzet chronarch'/'GPT', 'Izzet').

card_in_set('izzet guildmage', 'GPT').
card_original_type('izzet guildmage'/'GPT', 'Creature — Human Wizard').
card_original_text('izzet guildmage'/'GPT', '({U/R} can be paid with either {U} or {R}.)\n{2}{U}: Copy target instant spell you control with converted mana cost 2 or less. You may choose new targets for the copy.\n{2}{R}: Copy target sorcery spell you control with converted mana cost 2 or less. You may choose new targets for the copy.').
card_first_print('izzet guildmage', 'GPT').
card_image_name('izzet guildmage'/'GPT', 'izzet guildmage').
card_uid('izzet guildmage'/'GPT', 'GPT:Izzet Guildmage:izzet guildmage').
card_rarity('izzet guildmage'/'GPT', 'Uncommon').
card_artist('izzet guildmage'/'GPT', 'Jim Murray').
card_number('izzet guildmage'/'GPT', '145').
card_multiverse_id('izzet guildmage'/'GPT', '96966').
card_watermark('izzet guildmage'/'GPT', 'Izzet').

card_in_set('izzet signet', 'GPT').
card_original_type('izzet signet'/'GPT', 'Artifact').
card_original_text('izzet signet'/'GPT', '{1}, {T}: Add {U}{R} to your mana pool.').
card_first_print('izzet signet', 'GPT').
card_image_name('izzet signet'/'GPT', 'izzet signet').
card_uid('izzet signet'/'GPT', 'GPT:Izzet Signet:izzet signet').
card_rarity('izzet signet'/'GPT', 'Common').
card_artist('izzet signet'/'GPT', 'Greg Hildebrandt').
card_number('izzet signet'/'GPT', '152').
card_flavor_text('izzet signet'/'GPT', 'The Izzet signet is redesigned often, each time becoming closer to a vanity portrait of Niv-Mizzet.').
card_multiverse_id('izzet signet'/'GPT', '96867').
card_watermark('izzet signet'/'GPT', 'Izzet').

card_in_set('killer instinct', 'GPT').
card_original_type('killer instinct'/'GPT', 'Enchantment').
card_original_text('killer instinct'/'GPT', 'At the beginning of your upkeep, reveal the top card of your library. If it\'s a creature card, put it into play. That creature gains haste until end of turn. Sacrifice it at end of turn.').
card_first_print('killer instinct', 'GPT').
card_image_name('killer instinct'/'GPT', 'killer instinct').
card_uid('killer instinct'/'GPT', 'GPT:Killer Instinct:killer instinct').
card_rarity('killer instinct'/'GPT', 'Rare').
card_artist('killer instinct'/'GPT', 'Christopher Moeller').
card_number('killer instinct'/'GPT', '120').
card_flavor_text('killer instinct'/'GPT', '\"Take the bridge, men! Victory! Victory is ou— Retreat! RETREAT!\"').
card_multiverse_id('killer instinct'/'GPT', '97207').
card_watermark('killer instinct'/'GPT', 'Gruul').

card_in_set('leap of flame', 'GPT').
card_original_type('leap of flame'/'GPT', 'Instant').
card_original_text('leap of flame'/'GPT', 'Replicate {U}{R} (When you play this spell, copy it for each time you paid its replicate cost. You may choose new targets for the copies.)\nTarget creature gets +1/+0 and gains flying and first strike until end of turn.').
card_first_print('leap of flame', 'GPT').
card_image_name('leap of flame'/'GPT', 'leap of flame').
card_uid('leap of flame'/'GPT', 'GPT:Leap of Flame:leap of flame').
card_rarity('leap of flame'/'GPT', 'Common').
card_artist('leap of flame'/'GPT', 'Greg Hildebrandt').
card_number('leap of flame'/'GPT', '121').
card_multiverse_id('leap of flame'/'GPT', '107680').
card_watermark('leap of flame'/'GPT', 'Izzet').

card_in_set('leyline of lifeforce', 'GPT').
card_original_type('leyline of lifeforce'/'GPT', 'Enchantment').
card_original_text('leyline of lifeforce'/'GPT', 'If Leyline of Lifeforce is in your opening hand, you may begin the game with it in play.\nCreature spells can\'t be countered.').
card_first_print('leyline of lifeforce', 'GPT').
card_image_name('leyline of lifeforce'/'GPT', 'leyline of lifeforce').
card_uid('leyline of lifeforce'/'GPT', 'GPT:Leyline of Lifeforce:leyline of lifeforce').
card_rarity('leyline of lifeforce'/'GPT', 'Rare').
card_artist('leyline of lifeforce'/'GPT', 'Kev Walker').
card_number('leyline of lifeforce'/'GPT', '90').
card_flavor_text('leyline of lifeforce'/'GPT', 'Where sovereignty and flesh converge.').
card_multiverse_id('leyline of lifeforce'/'GPT', '107695').

card_in_set('leyline of lightning', 'GPT').
card_original_type('leyline of lightning'/'GPT', 'Enchantment').
card_original_text('leyline of lightning'/'GPT', 'If Leyline of Lightning is in your opening hand, you may begin the game with it in play.\nWhenever you play a spell, you may pay {1}. If you do, Leyline of Lightning deals 1 damage to target player.').
card_first_print('leyline of lightning', 'GPT').
card_image_name('leyline of lightning'/'GPT', 'leyline of lightning').
card_uid('leyline of lightning'/'GPT', 'GPT:Leyline of Lightning:leyline of lightning').
card_rarity('leyline of lightning'/'GPT', 'Rare').
card_artist('leyline of lightning'/'GPT', 'Paolo Parente').
card_number('leyline of lightning'/'GPT', '68').
card_flavor_text('leyline of lightning'/'GPT', 'Where storm and spellcraft converge.').
card_multiverse_id('leyline of lightning'/'GPT', '107686').

card_in_set('leyline of singularity', 'GPT').
card_original_type('leyline of singularity'/'GPT', 'Enchantment').
card_original_text('leyline of singularity'/'GPT', 'If Leyline of Singularity is in your opening hand, you may begin the game with it in play.\nAll nonland permanents are legendary.').
card_first_print('leyline of singularity', 'GPT').
card_image_name('leyline of singularity'/'GPT', 'leyline of singularity').
card_uid('leyline of singularity'/'GPT', 'GPT:Leyline of Singularity:leyline of singularity').
card_rarity('leyline of singularity'/'GPT', 'Rare').
card_artist('leyline of singularity'/'GPT', 'Zoltan Boros & Gabor Szikszai').
card_number('leyline of singularity'/'GPT', '29').
card_flavor_text('leyline of singularity'/'GPT', 'Where renown and solitude converge.').
card_multiverse_id('leyline of singularity'/'GPT', '107697').

card_in_set('leyline of the meek', 'GPT').
card_original_type('leyline of the meek'/'GPT', 'Enchantment').
card_original_text('leyline of the meek'/'GPT', 'If Leyline of the Meek is in your opening hand, you may begin the game with it in play.\nCreature tokens get +1/+1.').
card_first_print('leyline of the meek', 'GPT').
card_image_name('leyline of the meek'/'GPT', 'leyline of the meek').
card_uid('leyline of the meek'/'GPT', 'GPT:Leyline of the Meek:leyline of the meek').
card_rarity('leyline of the meek'/'GPT', 'Rare').
card_artist('leyline of the meek'/'GPT', 'Mark Zug').
card_number('leyline of the meek'/'GPT', '10').
card_flavor_text('leyline of the meek'/'GPT', 'Where strength and humility converge.').
card_multiverse_id('leyline of the meek'/'GPT', '107699').

card_in_set('leyline of the void', 'GPT').
card_original_type('leyline of the void'/'GPT', 'Enchantment').
card_original_text('leyline of the void'/'GPT', 'If Leyline of the Void is in your opening hand, you may begin the game with it in play.\nIf a card would be put into an opponent\'s graveyard, remove it from the game instead.').
card_first_print('leyline of the void', 'GPT').
card_image_name('leyline of the void'/'GPT', 'leyline of the void').
card_uid('leyline of the void'/'GPT', 'GPT:Leyline of the Void:leyline of the void').
card_rarity('leyline of the void'/'GPT', 'Rare').
card_artist('leyline of the void'/'GPT', 'Adam Rex').
card_number('leyline of the void'/'GPT', '52').
card_flavor_text('leyline of the void'/'GPT', 'Where treachery and oblivion converge.').
card_multiverse_id('leyline of the void'/'GPT', '107682').

card_in_set('lionheart maverick', 'GPT').
card_original_type('lionheart maverick'/'GPT', 'Creature — Human Knight').
card_original_text('lionheart maverick'/'GPT', 'Vigilance\n{4}{W}: Lionheart Maverick gets +1/+2 until end of turn.').
card_first_print('lionheart maverick', 'GPT').
card_image_name('lionheart maverick'/'GPT', 'lionheart maverick').
card_uid('lionheart maverick'/'GPT', 'GPT:Lionheart Maverick:lionheart maverick').
card_rarity('lionheart maverick'/'GPT', 'Common').
card_artist('lionheart maverick'/'GPT', 'Hideaki Takamura').
card_number('lionheart maverick'/'GPT', '11').
card_flavor_text('lionheart maverick'/'GPT', '\"Your signet is no symbol of power. It marks only your need for numbers to aid you. What do you do, guild-rat, now that you face my blade alone?\"').
card_multiverse_id('lionheart maverick'/'GPT', '96841').

card_in_set('living inferno', 'GPT').
card_original_type('living inferno'/'GPT', 'Creature — Elemental').
card_original_text('living inferno'/'GPT', '{T}: Living Inferno deals damage equal to its power divided as you choose among any number of target creatures. Each of those creatures deals damage equal to its power to Living Inferno.').
card_first_print('living inferno', 'GPT').
card_image_name('living inferno'/'GPT', 'living inferno').
card_uid('living inferno'/'GPT', 'GPT:Living Inferno:living inferno').
card_rarity('living inferno'/'GPT', 'Rare').
card_artist('living inferno'/'GPT', 'John Avon').
card_number('living inferno'/'GPT', '69').
card_multiverse_id('living inferno'/'GPT', '96874').

card_in_set('martyred rusalka', 'GPT').
card_original_type('martyred rusalka'/'GPT', 'Creature — Spirit').
card_original_text('martyred rusalka'/'GPT', '{W}, Sacrifice a creature: Target creature can\'t attack this turn.').
card_first_print('martyred rusalka', 'GPT').
card_image_name('martyred rusalka'/'GPT', 'martyred rusalka').
card_uid('martyred rusalka'/'GPT', 'GPT:Martyred Rusalka:martyred rusalka').
card_rarity('martyred rusalka'/'GPT', 'Uncommon').
card_artist('martyred rusalka'/'GPT', 'Alex Horley-Orlandelli').
card_number('martyred rusalka'/'GPT', '12').
card_flavor_text('martyred rusalka'/'GPT', 'Rakdos cultists hanged her for sport in the township square. Her ghost now stands vigil at what has become known as the Tree of Weeping.').
card_multiverse_id('martyred rusalka'/'GPT', '107696').

card_in_set('mimeofacture', 'GPT').
card_original_type('mimeofacture'/'GPT', 'Sorcery').
card_original_text('mimeofacture'/'GPT', 'Replicate {3}{U} (When you play this spell, copy it for each time you paid its replicate cost. You may choose new targets for the copies.)\nChoose target permanent an opponent controls. Search that player\'s library for a card with the same name and put it into play under your control. Then that player shuffles his or her library.').
card_first_print('mimeofacture', 'GPT').
card_image_name('mimeofacture'/'GPT', 'mimeofacture').
card_uid('mimeofacture'/'GPT', 'GPT:Mimeofacture:mimeofacture').
card_rarity('mimeofacture'/'GPT', 'Rare').
card_artist('mimeofacture'/'GPT', 'Dan Scott').
card_number('mimeofacture'/'GPT', '30').
card_multiverse_id('mimeofacture'/'GPT', '106568').
card_watermark('mimeofacture'/'GPT', 'Izzet').

card_in_set('mizzium transreliquat', 'GPT').
card_original_type('mizzium transreliquat'/'GPT', 'Artifact').
card_original_text('mizzium transreliquat'/'GPT', '{3}: Mizzium Transreliquat becomes a copy of target artifact until end of turn.\n{1}{U}{R}: Mizzium Transreliquat becomes a copy of target artifact and gains this ability.').
card_first_print('mizzium transreliquat', 'GPT').
card_image_name('mizzium transreliquat'/'GPT', 'mizzium transreliquat').
card_uid('mizzium transreliquat'/'GPT', 'GPT:Mizzium Transreliquat:mizzium transreliquat').
card_rarity('mizzium transreliquat'/'GPT', 'Rare').
card_artist('mizzium transreliquat'/'GPT', 'John Avon').
card_number('mizzium transreliquat'/'GPT', '153').
card_flavor_text('mizzium transreliquat'/'GPT', '\"What is it? Um . . . what do you want it to be?\"\n—Juzba, Izzet tinker').
card_multiverse_id('mizzium transreliquat'/'GPT', '107096').
card_watermark('mizzium transreliquat'/'GPT', 'Izzet').

card_in_set('moratorium stone', 'GPT').
card_original_type('moratorium stone'/'GPT', 'Artifact').
card_original_text('moratorium stone'/'GPT', '{2}, {T}: Remove target card in a graveyard from the game.\n{2}{W}{B}, {T}, Sacrifice Moratorium Stone: Remove from the game target nonland card in a graveyard, all other cards in graveyards with the same name as that card, and all permanents with that name.').
card_first_print('moratorium stone', 'GPT').
card_image_name('moratorium stone'/'GPT', 'moratorium stone').
card_uid('moratorium stone'/'GPT', 'GPT:Moratorium Stone:moratorium stone').
card_rarity('moratorium stone'/'GPT', 'Rare').
card_artist('moratorium stone'/'GPT', 'Martina Pilcerova').
card_number('moratorium stone'/'GPT', '154').
card_multiverse_id('moratorium stone'/'GPT', '97192').
card_watermark('moratorium stone'/'GPT', 'Orzhov').

card_in_set('mortify', 'GPT').
card_original_type('mortify'/'GPT', 'Instant').
card_original_text('mortify'/'GPT', 'Destroy target creature or enchantment.').
card_image_name('mortify'/'GPT', 'mortify').
card_uid('mortify'/'GPT', 'GPT:Mortify:mortify').
card_rarity('mortify'/'GPT', 'Uncommon').
card_artist('mortify'/'GPT', 'Glen Angus').
card_number('mortify'/'GPT', '122').
card_flavor_text('mortify'/'GPT', 'The eyes let flow with tears, then blood, then the very soul—the whole wrung inside out, dripping down into the blackened puddle of the past.').
card_multiverse_id('mortify'/'GPT', '96930').
card_watermark('mortify'/'GPT', 'Orzhov').

card_in_set('mourning thrull', 'GPT').
card_original_type('mourning thrull'/'GPT', 'Creature — Thrull').
card_original_text('mourning thrull'/'GPT', '({W/B} can be paid with either {W} or {B}.)\nFlying\nWhenever Mourning Thrull deals damage, you gain that much life.').
card_first_print('mourning thrull', 'GPT').
card_image_name('mourning thrull'/'GPT', 'mourning thrull').
card_uid('mourning thrull'/'GPT', 'GPT:Mourning Thrull:mourning thrull').
card_rarity('mourning thrull'/'GPT', 'Common').
card_artist('mourning thrull'/'GPT', 'Dany Orizio').
card_number('mourning thrull'/'GPT', '146').
card_flavor_text('mourning thrull'/'GPT', 'During their ascent to spirithood, patriarchs of Orzhova shed both flesh and emotion. This earthly slag is collected and formed into thrulls.').
card_multiverse_id('mourning thrull'/'GPT', '96910').
card_watermark('mourning thrull'/'GPT', 'Orzhov').

card_in_set('necromancer\'s magemark', 'GPT').
card_original_type('necromancer\'s magemark'/'GPT', 'Enchantment — Aura').
card_original_text('necromancer\'s magemark'/'GPT', 'Enchant creature\nCreatures you control that are enchanted get +1/+1.\nIf a creature you control that\'s enchanted would be put into a graveyard, return it to its owner\'s hand instead.').
card_first_print('necromancer\'s magemark', 'GPT').
card_image_name('necromancer\'s magemark'/'GPT', 'necromancer\'s magemark').
card_uid('necromancer\'s magemark'/'GPT', 'GPT:Necromancer\'s Magemark:necromancer\'s magemark').
card_rarity('necromancer\'s magemark'/'GPT', 'Common').
card_artist('necromancer\'s magemark'/'GPT', 'Brandon Kitkouski').
card_number('necromancer\'s magemark'/'GPT', '53').
card_multiverse_id('necromancer\'s magemark'/'GPT', '107685').

card_in_set('niv-mizzet, the firemind', 'GPT').
card_original_type('niv-mizzet, the firemind'/'GPT', 'Legendary Creature — Dragon Wizard').
card_original_text('niv-mizzet, the firemind'/'GPT', 'Flying\nWhenever you draw a card, Niv-Mizzet, the Firemind deals 1 damage to target creature or player.\n{T}: Draw a card.').
card_first_print('niv-mizzet, the firemind', 'GPT').
card_image_name('niv-mizzet, the firemind'/'GPT', 'niv-mizzet, the firemind').
card_uid('niv-mizzet, the firemind'/'GPT', 'GPT:Niv-Mizzet, the Firemind:niv-mizzet, the firemind').
card_rarity('niv-mizzet, the firemind'/'GPT', 'Rare').
card_artist('niv-mizzet, the firemind'/'GPT', 'Todd Lockwood').
card_number('niv-mizzet, the firemind'/'GPT', '123').
card_flavor_text('niv-mizzet, the firemind'/'GPT', '\"(Z–>)90° – (E–N²W)90°t = 1\"').
card_multiverse_id('niv-mizzet, the firemind'/'GPT', '96952').
card_watermark('niv-mizzet, the firemind'/'GPT', 'Izzet').

card_in_set('nivix, aerie of the firemind', 'GPT').
card_original_type('nivix, aerie of the firemind'/'GPT', 'Land').
card_original_text('nivix, aerie of the firemind'/'GPT', '{T}: Add {1} to your mana pool.\n{2}{U}{R}, {T}: Remove the top card of your library from the game. Until your next turn, you may play that card if it\'s an instant or sorcery.').
card_first_print('nivix, aerie of the firemind', 'GPT').
card_image_name('nivix, aerie of the firemind'/'GPT', 'nivix, aerie of the firemind').
card_uid('nivix, aerie of the firemind'/'GPT', 'GPT:Nivix, Aerie of the Firemind:nivix, aerie of the firemind').
card_rarity('nivix, aerie of the firemind'/'GPT', 'Uncommon').
card_artist('nivix, aerie of the firemind'/'GPT', 'Martina Pilcerova').
card_number('nivix, aerie of the firemind'/'GPT', '160').
card_flavor_text('nivix, aerie of the firemind'/'GPT', 'Niv-Mizzet\'s genius and vanity reverberate throughout the mirrored halls of Nivix.').
card_multiverse_id('nivix, aerie of the firemind'/'GPT', '96937').
card_watermark('nivix, aerie of the firemind'/'GPT', 'Izzet').

card_in_set('ogre savant', 'GPT').
card_original_type('ogre savant'/'GPT', 'Creature — Ogre Wizard').
card_original_text('ogre savant'/'GPT', 'When Ogre Savant comes into play, if {U} was spent to play Ogre Savant, return target creature to its owner\'s hand.').
card_first_print('ogre savant', 'GPT').
card_image_name('ogre savant'/'GPT', 'ogre savant').
card_uid('ogre savant'/'GPT', 'GPT:Ogre Savant:ogre savant').
card_rarity('ogre savant'/'GPT', 'Common').
card_artist('ogre savant'/'GPT', 'Paolo Parente').
card_number('ogre savant'/'GPT', '70').
card_flavor_text('ogre savant'/'GPT', 'He\'s an oxymoron.').
card_multiverse_id('ogre savant'/'GPT', '97236').
card_watermark('ogre savant'/'GPT', 'Izzet').

card_in_set('order of the stars', 'GPT').
card_original_type('order of the stars'/'GPT', 'Creature — Human Cleric').
card_original_text('order of the stars'/'GPT', 'Defender (This creature can\'t attack.)\nAs Order of the Stars comes into play, choose a color.\nOrder of the Stars has protection from the chosen color.').
card_first_print('order of the stars', 'GPT').
card_image_name('order of the stars'/'GPT', 'order of the stars').
card_uid('order of the stars'/'GPT', 'GPT:Order of the Stars:order of the stars').
card_rarity('order of the stars'/'GPT', 'Uncommon').
card_artist('order of the stars'/'GPT', 'Heather Hudson').
card_number('order of the stars'/'GPT', '13').
card_flavor_text('order of the stars'/'GPT', 'As stoic as the marble halls, as unblinking as the stars upon their shields.').
card_multiverse_id('order of the stars'/'GPT', '96938').

card_in_set('orzhov basilica', 'GPT').
card_original_type('orzhov basilica'/'GPT', 'Land').
card_original_text('orzhov basilica'/'GPT', 'Orzhov Basilica comes into play tapped.\nWhen Orzhov Basilica comes into play, return a land you control to its owner\'s hand.\n{T}: Add {W}{B} to your mana pool.').
card_first_print('orzhov basilica', 'GPT').
card_image_name('orzhov basilica'/'GPT', 'orzhov basilica').
card_uid('orzhov basilica'/'GPT', 'GPT:Orzhov Basilica:orzhov basilica').
card_rarity('orzhov basilica'/'GPT', 'Common').
card_artist('orzhov basilica'/'GPT', 'John Avon').
card_number('orzhov basilica'/'GPT', '161').
card_multiverse_id('orzhov basilica'/'GPT', '96871').
card_watermark('orzhov basilica'/'GPT', 'Orzhov').

card_in_set('orzhov euthanist', 'GPT').
card_original_type('orzhov euthanist'/'GPT', 'Creature — Human Assassin').
card_original_text('orzhov euthanist'/'GPT', 'Haunt (When this card is put into a graveyard from play, remove it from the game haunting target creature.)\nWhen Orzhov Euthanist comes into play or the creature it haunts is put into a graveyard, destroy target creature that was dealt damage this turn.').
card_first_print('orzhov euthanist', 'GPT').
card_image_name('orzhov euthanist'/'GPT', 'orzhov euthanist').
card_uid('orzhov euthanist'/'GPT', 'GPT:Orzhov Euthanist:orzhov euthanist').
card_rarity('orzhov euthanist'/'GPT', 'Common').
card_artist('orzhov euthanist'/'GPT', 'Zoltan Boros & Gabor Szikszai').
card_number('orzhov euthanist'/'GPT', '54').
card_multiverse_id('orzhov euthanist'/'GPT', '96893').
card_watermark('orzhov euthanist'/'GPT', 'Orzhov').

card_in_set('orzhov guildmage', 'GPT').
card_original_type('orzhov guildmage'/'GPT', 'Creature — Human Wizard').
card_original_text('orzhov guildmage'/'GPT', '({W/B} can be paid with either {W} or {B}.)\n{2}{W}: Target player gains 1 life.\n{2}{B}: Each player loses 1 life.').
card_first_print('orzhov guildmage', 'GPT').
card_image_name('orzhov guildmage'/'GPT', 'orzhov guildmage').
card_uid('orzhov guildmage'/'GPT', 'GPT:Orzhov Guildmage:orzhov guildmage').
card_rarity('orzhov guildmage'/'GPT', 'Uncommon').
card_artist('orzhov guildmage'/'GPT', 'Greg Staples').
card_number('orzhov guildmage'/'GPT', '147').
card_multiverse_id('orzhov guildmage'/'GPT', '96921').
card_watermark('orzhov guildmage'/'GPT', 'Orzhov').

card_in_set('orzhov pontiff', 'GPT').
card_original_type('orzhov pontiff'/'GPT', 'Creature — Human Cleric').
card_original_text('orzhov pontiff'/'GPT', 'Haunt (When this card is put into a graveyard from play, remove it from the game haunting target creature.)\nWhen Orzhov Pontiff comes into play or the creature it haunts is put into a graveyard, choose one creatures you control get +1/+1 until end of turn; or creatures you don\'t control get -1/-1 until end of turn.').
card_first_print('orzhov pontiff', 'GPT').
card_image_name('orzhov pontiff'/'GPT', 'orzhov pontiff').
card_uid('orzhov pontiff'/'GPT', 'GPT:Orzhov Pontiff:orzhov pontiff').
card_rarity('orzhov pontiff'/'GPT', 'Rare').
card_artist('orzhov pontiff'/'GPT', 'Adam Rex').
card_number('orzhov pontiff'/'GPT', '124').
card_multiverse_id('orzhov pontiff'/'GPT', '96844').
card_watermark('orzhov pontiff'/'GPT', 'Orzhov').

card_in_set('orzhov signet', 'GPT').
card_original_type('orzhov signet'/'GPT', 'Artifact').
card_original_text('orzhov signet'/'GPT', '{1}, {T}: Add {W}{B} to your mana pool.').
card_first_print('orzhov signet', 'GPT').
card_image_name('orzhov signet'/'GPT', 'orzhov signet').
card_uid('orzhov signet'/'GPT', 'GPT:Orzhov Signet:orzhov signet').
card_rarity('orzhov signet'/'GPT', 'Common').
card_artist('orzhov signet'/'GPT', 'Tim Hildebrandt').
card_number('orzhov signet'/'GPT', '155').
card_flavor_text('orzhov signet'/'GPT', 'The form of the sigil is just as important as the sigil itself. If it\'s carried on a medallion, its bearer is a master. If it\'s tattooed on the body, its bearer is a slave.').
card_multiverse_id('orzhov signet'/'GPT', '96870').
card_watermark('orzhov signet'/'GPT', 'Orzhov').

card_in_set('orzhova, the church of deals', 'GPT').
card_original_type('orzhova, the church of deals'/'GPT', 'Land').
card_original_text('orzhova, the church of deals'/'GPT', '{T}: Add {1} to your mana pool.\n{3}{W}{B}, {T}: Target player loses 1 life and you gain 1 life.').
card_first_print('orzhova, the church of deals', 'GPT').
card_image_name('orzhova, the church of deals'/'GPT', 'orzhova, the church of deals').
card_uid('orzhova, the church of deals'/'GPT', 'GPT:Orzhova, the Church of Deals:orzhova, the church of deals').
card_rarity('orzhova, the church of deals'/'GPT', 'Uncommon').
card_artist('orzhova, the church of deals'/'GPT', 'Martina Pilcerova').
card_number('orzhova, the church of deals'/'GPT', '162').
card_flavor_text('orzhova, the church of deals'/'GPT', 'Unlike most churches, at Orzhova it\'s best to pray before you arrive.').
card_multiverse_id('orzhova, the church of deals'/'GPT', '97206').
card_watermark('orzhova, the church of deals'/'GPT', 'Orzhov').

card_in_set('ostiary thrull', 'GPT').
card_original_type('ostiary thrull'/'GPT', 'Creature — Thrull').
card_original_text('ostiary thrull'/'GPT', '{W}, {T}: Tap target creature.').
card_first_print('ostiary thrull', 'GPT').
card_image_name('ostiary thrull'/'GPT', 'ostiary thrull').
card_uid('ostiary thrull'/'GPT', 'GPT:Ostiary Thrull:ostiary thrull').
card_rarity('ostiary thrull'/'GPT', 'Common').
card_artist('ostiary thrull'/'GPT', 'Ron Spencer').
card_number('ostiary thrull'/'GPT', '55').
card_flavor_text('ostiary thrull'/'GPT', 'Orzhov churches don\'t pass the plate for collection. They charge for admission.').
card_multiverse_id('ostiary thrull'/'GPT', '96974').
card_watermark('ostiary thrull'/'GPT', 'Orzhov').

card_in_set('parallectric feedback', 'GPT').
card_original_type('parallectric feedback'/'GPT', 'Instant').
card_original_text('parallectric feedback'/'GPT', 'Parallectric Feedback deals damage to target spell\'s controller equal to that spell\'s converted mana cost.').
card_first_print('parallectric feedback', 'GPT').
card_image_name('parallectric feedback'/'GPT', 'parallectric feedback').
card_uid('parallectric feedback'/'GPT', 'GPT:Parallectric Feedback:parallectric feedback').
card_rarity('parallectric feedback'/'GPT', 'Rare').
card_artist('parallectric feedback'/'GPT', 'Mitch Cotie').
card_number('parallectric feedback'/'GPT', '71').
card_flavor_text('parallectric feedback'/'GPT', '\"You can\'t have so many people living so close together, all practicing so much magic, and not expect some feedback.\"').
card_multiverse_id('parallectric feedback'/'GPT', '97217').

card_in_set('petrahydrox', 'GPT').
card_original_type('petrahydrox'/'GPT', 'Creature — Weird').
card_original_text('petrahydrox'/'GPT', '({U/R} can be paid with either {U} or {R}.)\nWhen Petrahydrox becomes the target of a spell or ability, return Petrahydrox to its owner\'s hand.').
card_first_print('petrahydrox', 'GPT').
card_image_name('petrahydrox'/'GPT', 'petrahydrox').
card_uid('petrahydrox'/'GPT', 'GPT:Petrahydrox:petrahydrox').
card_rarity('petrahydrox'/'GPT', 'Common').
card_artist('petrahydrox'/'GPT', 'Anthony S. Waters').
card_number('petrahydrox'/'GPT', '148').
card_flavor_text('petrahydrox'/'GPT', 'It departs through the streets\' fissures with a sound like gravel pouring into a pond.').
card_multiverse_id('petrahydrox'/'GPT', '96897').
card_watermark('petrahydrox'/'GPT', 'Izzet').

card_in_set('petrified wood-kin', 'GPT').
card_original_type('petrified wood-kin'/'GPT', 'Creature — Elemental Warrior').
card_original_text('petrified wood-kin'/'GPT', 'Petrified Wood-Kin can\'t be countered.\nBloodthirst X (This creature comes into play with X +1/+1 counters on it, where X is the damage dealt to your opponents this turn.)\nProtection from instants').
card_first_print('petrified wood-kin', 'GPT').
card_image_name('petrified wood-kin'/'GPT', 'petrified wood-kin').
card_uid('petrified wood-kin'/'GPT', 'GPT:Petrified Wood-Kin:petrified wood-kin').
card_rarity('petrified wood-kin'/'GPT', 'Rare').
card_artist('petrified wood-kin'/'GPT', 'Kev Walker').
card_number('petrified wood-kin'/'GPT', '91').
card_multiverse_id('petrified wood-kin'/'GPT', '96853').
card_watermark('petrified wood-kin'/'GPT', 'Gruul').

card_in_set('pillory of the sleepless', 'GPT').
card_original_type('pillory of the sleepless'/'GPT', 'Enchantment — Aura').
card_original_text('pillory of the sleepless'/'GPT', 'Enchant creature\nEnchanted creature can\'t attack or block.\nEnchanted creature has \"At the beginning of your upkeep, you lose 1 life.\"').
card_first_print('pillory of the sleepless', 'GPT').
card_image_name('pillory of the sleepless'/'GPT', 'pillory of the sleepless').
card_uid('pillory of the sleepless'/'GPT', 'GPT:Pillory of the Sleepless:pillory of the sleepless').
card_rarity('pillory of the sleepless'/'GPT', 'Common').
card_artist('pillory of the sleepless'/'GPT', 'Mark Romanoski').
card_number('pillory of the sleepless'/'GPT', '125').
card_flavor_text('pillory of the sleepless'/'GPT', 'Which is worse—the sleep which never ends or that which never comes?').
card_multiverse_id('pillory of the sleepless'/'GPT', '96882').
card_watermark('pillory of the sleepless'/'GPT', 'Orzhov').

card_in_set('plagued rusalka', 'GPT').
card_original_type('plagued rusalka'/'GPT', 'Creature — Spirit').
card_original_text('plagued rusalka'/'GPT', '{B}, Sacrifice a creature: Target creature gets -1/-1 until end of turn.').
card_first_print('plagued rusalka', 'GPT').
card_image_name('plagued rusalka'/'GPT', 'plagued rusalka').
card_uid('plagued rusalka'/'GPT', 'GPT:Plagued Rusalka:plagued rusalka').
card_rarity('plagued rusalka'/'GPT', 'Uncommon').
card_artist('plagued rusalka'/'GPT', 'Alex Horley-Orlandelli').
card_number('plagued rusalka'/'GPT', '56').
card_flavor_text('plagued rusalka'/'GPT', '\"Look at her, once filled with innocence. Death has a way of wringing away such . . . deficiencies.\"\n—Savra').
card_multiverse_id('plagued rusalka'/'GPT', '107691').

card_in_set('poisonbelly ogre', 'GPT').
card_original_type('poisonbelly ogre'/'GPT', 'Creature — Ogre Warrior').
card_original_text('poisonbelly ogre'/'GPT', 'Whenever another creature comes into play, its controller loses 1 life.').
card_first_print('poisonbelly ogre', 'GPT').
card_image_name('poisonbelly ogre'/'GPT', 'poisonbelly ogre').
card_uid('poisonbelly ogre'/'GPT', 'GPT:Poisonbelly Ogre:poisonbelly ogre').
card_rarity('poisonbelly ogre'/'GPT', 'Common').
card_artist('poisonbelly ogre'/'GPT', 'Zoltan Boros & Gabor Szikszai').
card_number('poisonbelly ogre'/'GPT', '57').
card_flavor_text('poisonbelly ogre'/'GPT', 'Some claim it\'s a failed Simic experiment, others say a Golgari creation. Both guilds deny all responsibility, while secretly studying its strange virulence.').
card_multiverse_id('poisonbelly ogre'/'GPT', '107099').

card_in_set('predatory focus', 'GPT').
card_original_type('predatory focus'/'GPT', 'Sorcery').
card_original_text('predatory focus'/'GPT', 'You may have creatures you control deal their combat damage to defending player this turn as though they weren\'t blocked.').
card_first_print('predatory focus', 'GPT').
card_image_name('predatory focus'/'GPT', 'predatory focus').
card_uid('predatory focus'/'GPT', 'GPT:Predatory Focus:predatory focus').
card_rarity('predatory focus'/'GPT', 'Uncommon').
card_artist('predatory focus'/'GPT', 'Zoltan Boros & Gabor Szikszai').
card_number('predatory focus'/'GPT', '92').
card_flavor_text('predatory focus'/'GPT', 'Instinct blood-tinted his world until he could see only one thing clearly: his prey.').
card_multiverse_id('predatory focus'/'GPT', '96931').

card_in_set('primeval light', 'GPT').
card_original_type('primeval light'/'GPT', 'Sorcery').
card_original_text('primeval light'/'GPT', 'Destroy all enchantments target player controls.').
card_first_print('primeval light', 'GPT').
card_image_name('primeval light'/'GPT', 'primeval light').
card_uid('primeval light'/'GPT', 'GPT:Primeval Light:primeval light').
card_rarity('primeval light'/'GPT', 'Uncommon').
card_artist('primeval light'/'GPT', 'Jim Nelson').
card_number('primeval light'/'GPT', '93').
card_flavor_text('primeval light'/'GPT', '\"Look to the old tomes, those bound in hides and written in yarberry ink. They hold the spells that can free us of the clutter of modern magecraft.\"\n—Dravash, dowsing shaman').
card_multiverse_id('primeval light'/'GPT', '107694').

card_in_set('pyromatics', 'GPT').
card_original_type('pyromatics'/'GPT', 'Instant').
card_original_text('pyromatics'/'GPT', 'Replicate {1}{R} (When you play this spell, copy it for each time you paid its replicate cost. You may choose new targets for the copies.)\nPyromatics deals 1 damage to target creature or player.').
card_first_print('pyromatics', 'GPT').
card_image_name('pyromatics'/'GPT', 'pyromatics').
card_uid('pyromatics'/'GPT', 'GPT:Pyromatics:pyromatics').
card_rarity('pyromatics'/'GPT', 'Common').
card_artist('pyromatics'/'GPT', 'Glen Angus').
card_number('pyromatics'/'GPT', '72').
card_multiverse_id('pyromatics'/'GPT', '96845').
card_watermark('pyromatics'/'GPT', 'Izzet').

card_in_set('quicken', 'GPT').
card_original_type('quicken'/'GPT', 'Instant').
card_original_text('quicken'/'GPT', 'The next sorcery spell you play this turn can be played any time you could play an instant.\nDraw a card.').
card_first_print('quicken', 'GPT').
card_image_name('quicken'/'GPT', 'quicken').
card_uid('quicken'/'GPT', 'GPT:Quicken:quicken').
card_rarity('quicken'/'GPT', 'Rare').
card_artist('quicken'/'GPT', 'Aleksi Briclot').
card_number('quicken'/'GPT', '31').
card_flavor_text('quicken'/'GPT', 'A skilled Izzet chronarch can carry out an epic vendetta between the fall of one hourglass grain and the next.').
card_multiverse_id('quicken'/'GPT', '96826').

card_in_set('rabble-rouser', 'GPT').
card_original_type('rabble-rouser'/'GPT', 'Creature — Goblin Shaman').
card_original_text('rabble-rouser'/'GPT', 'Bloodthirst 1 (If an opponent was dealt damage this turn, this creature comes into play with a +1/+1 counter on it.)\n{R}, {T}: Attacking creatures get +X/+0 until end of turn, where X is Rabble-Rouser\'s power.').
card_first_print('rabble-rouser', 'GPT').
card_image_name('rabble-rouser'/'GPT', 'rabble-rouser').
card_uid('rabble-rouser'/'GPT', 'GPT:Rabble-Rouser:rabble-rouser').
card_rarity('rabble-rouser'/'GPT', 'Uncommon').
card_artist('rabble-rouser'/'GPT', 'Justin Norman').
card_number('rabble-rouser'/'GPT', '73').
card_multiverse_id('rabble-rouser'/'GPT', '97200').
card_watermark('rabble-rouser'/'GPT', 'Gruul').

card_in_set('repeal', 'GPT').
card_original_type('repeal'/'GPT', 'Instant').
card_original_text('repeal'/'GPT', 'Return target nonland permanent with converted mana cost X to its owner\'s hand.\nDraw a card.').
card_first_print('repeal', 'GPT').
card_image_name('repeal'/'GPT', 'repeal').
card_uid('repeal'/'GPT', 'GPT:Repeal:repeal').
card_rarity('repeal'/'GPT', 'Common').
card_artist('repeal'/'GPT', 'Dan Scott').
card_number('repeal'/'GPT', '32').
card_flavor_text('repeal'/'GPT', '\"Your deed cannot be undone. You, however, can be.\"\n—Agosto, Azorius imperator').
card_multiverse_id('repeal'/'GPT', '96827').

card_in_set('restless bones', 'GPT').
card_original_type('restless bones'/'GPT', 'Creature — Skeleton').
card_original_text('restless bones'/'GPT', '{3}{B}, {T}: Target creature gains swampwalk until end of turn.\n{1}{B}: Regenerate Restless Bones.').
card_first_print('restless bones', 'GPT').
card_image_name('restless bones'/'GPT', 'restless bones').
card_uid('restless bones'/'GPT', 'GPT:Restless Bones:restless bones').
card_rarity('restless bones'/'GPT', 'Common').
card_artist('restless bones'/'GPT', 'Glenn Fabry').
card_number('restless bones'/'GPT', '58').
card_flavor_text('restless bones'/'GPT', '\"We mourn our dead. We shroud our dead. We bury our dead. Too often, it seems, we must kill our dead again.\"\n—Grazda, veteran armorer').
card_multiverse_id('restless bones'/'GPT', '96855').

card_in_set('revenant patriarch', 'GPT').
card_original_type('revenant patriarch'/'GPT', 'Creature — Spirit').
card_original_text('revenant patriarch'/'GPT', 'When Revenant Patriarch comes into play, if {W} was spent to play Revenant Patriarch, target player skips his or her next combat phase.\nRevenant Patriarch can\'t block.').
card_first_print('revenant patriarch', 'GPT').
card_image_name('revenant patriarch'/'GPT', 'revenant patriarch').
card_uid('revenant patriarch'/'GPT', 'GPT:Revenant Patriarch:revenant patriarch').
card_rarity('revenant patriarch'/'GPT', 'Uncommon').
card_artist('revenant patriarch'/'GPT', 'Nick Percival').
card_number('revenant patriarch'/'GPT', '59').
card_flavor_text('revenant patriarch'/'GPT', '\"The Golgari raise the bodies of their dead to serve. We raise the spirits of our dead to lead.\"').
card_multiverse_id('revenant patriarch'/'GPT', '96972').
card_watermark('revenant patriarch'/'GPT', 'Orzhov').

card_in_set('rumbling slum', 'GPT').
card_original_type('rumbling slum'/'GPT', 'Creature — Elemental').
card_original_text('rumbling slum'/'GPT', 'At the beginning of your upkeep, Rumbling Slum deals 1 damage to each player.').
card_first_print('rumbling slum', 'GPT').
card_image_name('rumbling slum'/'GPT', 'rumbling slum').
card_uid('rumbling slum'/'GPT', 'GPT:Rumbling Slum:rumbling slum').
card_rarity('rumbling slum'/'GPT', 'Rare').
card_artist('rumbling slum'/'GPT', 'Carl Critchlow').
card_number('rumbling slum'/'GPT', '126').
card_flavor_text('rumbling slum'/'GPT', 'The Orzhov contract the Izzet to animate slum districts and banish them to the wastes. The Gruul adopt them and send them back to the city for vengeance.').
card_multiverse_id('rumbling slum'/'GPT', '97199').
card_watermark('rumbling slum'/'GPT', 'Gruul').

card_in_set('runeboggle', 'GPT').
card_original_type('runeboggle'/'GPT', 'Instant').
card_original_text('runeboggle'/'GPT', 'Counter target spell unless its controller pays {1}.\nDraw a card.').
card_first_print('runeboggle', 'GPT').
card_image_name('runeboggle'/'GPT', 'runeboggle').
card_uid('runeboggle'/'GPT', 'GPT:Runeboggle:runeboggle').
card_rarity('runeboggle'/'GPT', 'Common').
card_artist('runeboggle'/'GPT', 'Ron Spencer').
card_number('runeboggle'/'GPT', '33').
card_flavor_text('runeboggle'/'GPT', '\"Ever try to count hyperactive schoolchildren while someone shouts random numbers in your ear? It\'s like that.\"\n—Geetra, frustrated mage').
card_multiverse_id('runeboggle'/'GPT', '96916').

card_in_set('sanguine praetor', 'GPT').
card_original_type('sanguine praetor'/'GPT', 'Creature — Avatar').
card_original_text('sanguine praetor'/'GPT', '{B}, Sacrifice a creature: Destroy each creature with the same converted mana cost as the sacrificed creature.').
card_first_print('sanguine praetor', 'GPT').
card_image_name('sanguine praetor'/'GPT', 'sanguine praetor').
card_uid('sanguine praetor'/'GPT', 'GPT:Sanguine Praetor:sanguine praetor').
card_rarity('sanguine praetor'/'GPT', 'Rare').
card_artist('sanguine praetor'/'GPT', 'rk post').
card_number('sanguine praetor'/'GPT', '60').
card_flavor_text('sanguine praetor'/'GPT', '\"Our fealty to guilds dooms us. The old gods shall resurface. Our skins will wave upon the guild-masts over emptied streets, and our bones will clatter in the wind.\"\n—Ilromov, traveling storyteller').
card_multiverse_id('sanguine praetor'/'GPT', '97190').

card_in_set('savage twister', 'GPT').
card_original_type('savage twister'/'GPT', 'Sorcery').
card_original_text('savage twister'/'GPT', 'Savage Twister deals X damage to each creature.').
card_image_name('savage twister'/'GPT', 'savage twister').
card_uid('savage twister'/'GPT', 'GPT:Savage Twister:savage twister').
card_rarity('savage twister'/'GPT', 'Uncommon').
card_artist('savage twister'/'GPT', 'Luca Zontini').
card_number('savage twister'/'GPT', '127').
card_flavor_text('savage twister'/'GPT', '\"Nature is the ultimate mindless destroyer, capable of power and ferocity no army can match, and the Gruul follow its example.\"\n—Trigori, Azorius senator').
card_multiverse_id('savage twister'/'GPT', '97216').
card_watermark('savage twister'/'GPT', 'Gruul').

card_in_set('scab-clan mauler', 'GPT').
card_original_type('scab-clan mauler'/'GPT', 'Creature — Human Berserker').
card_original_text('scab-clan mauler'/'GPT', 'Bloodthirst 2 (If an opponent was dealt damage this turn, this creature comes into play with two +1/+1 counters on it.)\nTrample').
card_first_print('scab-clan mauler', 'GPT').
card_image_name('scab-clan mauler'/'GPT', 'scab-clan mauler').
card_uid('scab-clan mauler'/'GPT', 'GPT:Scab-Clan Mauler:scab-clan mauler').
card_rarity('scab-clan mauler'/'GPT', 'Common').
card_artist('scab-clan mauler'/'GPT', 'Liam Sharp').
card_number('scab-clan mauler'/'GPT', '128').
card_flavor_text('scab-clan mauler'/'GPT', 'They inflict pain to forget their own and break foes to feel whole.').
card_multiverse_id('scab-clan mauler'/'GPT', '96904').
card_watermark('scab-clan mauler'/'GPT', 'Gruul').

card_in_set('schismotivate', 'GPT').
card_original_type('schismotivate'/'GPT', 'Instant').
card_original_text('schismotivate'/'GPT', 'Target creature gets +4/+0 until end of turn. Another target creature gets -4/-0 until end of turn.').
card_first_print('schismotivate', 'GPT').
card_image_name('schismotivate'/'GPT', 'schismotivate').
card_uid('schismotivate'/'GPT', 'GPT:Schismotivate:schismotivate').
card_rarity('schismotivate'/'GPT', 'Uncommon').
card_artist('schismotivate'/'GPT', 'Ron Spencer').
card_number('schismotivate'/'GPT', '129').
card_flavor_text('schismotivate'/'GPT', '\"I ran the experiment through the brain of a schizophrenic lab-goblin. My burners\' flames fell asleep while the beakers jumped about and shattered. Success!\"\n—Myznar, Izzet psychomancer').
card_multiverse_id('schismotivate'/'GPT', '97204').
card_watermark('schismotivate'/'GPT', 'Izzet').

card_in_set('scorched rusalka', 'GPT').
card_original_type('scorched rusalka'/'GPT', 'Creature — Spirit').
card_original_text('scorched rusalka'/'GPT', '{R}, Sacrifice a creature: Scorched Rusalka deals 1 damage to target player.').
card_first_print('scorched rusalka', 'GPT').
card_image_name('scorched rusalka'/'GPT', 'scorched rusalka').
card_uid('scorched rusalka'/'GPT', 'GPT:Scorched Rusalka:scorched rusalka').
card_rarity('scorched rusalka'/'GPT', 'Uncommon').
card_artist('scorched rusalka'/'GPT', 'Luca Zontini').
card_number('scorched rusalka'/'GPT', '74').
card_flavor_text('scorched rusalka'/'GPT', 'Each small blaze she sets jogs her memory, letting her piece together the mystery of her own fiery end.').
card_multiverse_id('scorched rusalka'/'GPT', '107698').

card_in_set('seize the soul', 'GPT').
card_original_type('seize the soul'/'GPT', 'Instant').
card_original_text('seize the soul'/'GPT', 'Destroy target nonwhite nonblack creature. Put a 1/1 white Spirit creature token with flying into play.\nHaunt\nWhen the creature Seize the Soul haunts is put into a graveyard, destroy target nonwhite nonblack creature. Put a 1/1 white Spirit creature token with flying into play.').
card_first_print('seize the soul', 'GPT').
card_image_name('seize the soul'/'GPT', 'seize the soul').
card_uid('seize the soul'/'GPT', 'GPT:Seize the Soul:seize the soul').
card_rarity('seize the soul'/'GPT', 'Rare').
card_artist('seize the soul'/'GPT', 'Aleksi Briclot').
card_number('seize the soul'/'GPT', '61').
card_multiverse_id('seize the soul'/'GPT', '96888').
card_watermark('seize the soul'/'GPT', 'Orzhov').

card_in_set('shadow lance', 'GPT').
card_original_type('shadow lance'/'GPT', 'Enchantment — Aura').
card_original_text('shadow lance'/'GPT', 'Enchant creature\nEnchanted creature has first strike.\n{1}{B}: Enchanted creature gets +2/+2 until end of turn.').
card_first_print('shadow lance', 'GPT').
card_image_name('shadow lance'/'GPT', 'shadow lance').
card_uid('shadow lance'/'GPT', 'GPT:Shadow Lance:shadow lance').
card_rarity('shadow lance'/'GPT', 'Uncommon').
card_artist('shadow lance'/'GPT', 'Hideaki Takamura').
card_number('shadow lance'/'GPT', '14').
card_flavor_text('shadow lance'/'GPT', 'Shadow lances are crafted from harvested souls. The more wicked the sinner, the keener the blade.').
card_multiverse_id('shadow lance'/'GPT', '96944').
card_watermark('shadow lance'/'GPT', 'Orzhov').

card_in_set('shattering spree', 'GPT').
card_original_type('shattering spree'/'GPT', 'Sorcery').
card_original_text('shattering spree'/'GPT', 'Replicate {R} (When you play this spell, copy it for each time you paid its replicate cost. You may choose new targets for the copies.)\nDestroy target artifact.').
card_first_print('shattering spree', 'GPT').
card_image_name('shattering spree'/'GPT', 'shattering spree').
card_uid('shattering spree'/'GPT', 'GPT:Shattering Spree:shattering spree').
card_rarity('shattering spree'/'GPT', 'Uncommon').
card_artist('shattering spree'/'GPT', 'Pat Lee').
card_number('shattering spree'/'GPT', '75').
card_multiverse_id('shattering spree'/'GPT', '97233').
card_watermark('shattering spree'/'GPT', 'Izzet').

card_in_set('shrieking grotesque', 'GPT').
card_original_type('shrieking grotesque'/'GPT', 'Creature — Gargoyle').
card_original_text('shrieking grotesque'/'GPT', 'Flying\nWhen Shrieking Grotesque comes into play, if {B} was spent to play Shrieking Grotesque, target player discards a card.').
card_first_print('shrieking grotesque', 'GPT').
card_image_name('shrieking grotesque'/'GPT', 'shrieking grotesque').
card_uid('shrieking grotesque'/'GPT', 'GPT:Shrieking Grotesque:shrieking grotesque').
card_rarity('shrieking grotesque'/'GPT', 'Common').
card_artist('shrieking grotesque'/'GPT', 'Dany Orizio').
card_number('shrieking grotesque'/'GPT', '15').
card_flavor_text('shrieking grotesque'/'GPT', 'Orzhov mage-sculptors bring their stone to life before they carve it. The shrieking begins as soon as the mouths are formed.').
card_multiverse_id('shrieking grotesque'/'GPT', '96954').
card_watermark('shrieking grotesque'/'GPT', 'Orzhov').

card_in_set('siege of towers', 'GPT').
card_original_type('siege of towers'/'GPT', 'Sorcery').
card_original_text('siege of towers'/'GPT', 'Replicate {1}{R} (When you play this spell, copy it for each time you paid its replicate cost. You may choose new targets for the copies.)\nTarget Mountain becomes a 3/1 creature. It\'s still a land.').
card_first_print('siege of towers', 'GPT').
card_image_name('siege of towers'/'GPT', 'siege of towers').
card_uid('siege of towers'/'GPT', 'GPT:Siege of Towers:siege of towers').
card_rarity('siege of towers'/'GPT', 'Rare').
card_artist('siege of towers'/'GPT', 'Anthony S. Waters').
card_number('siege of towers'/'GPT', '76').
card_multiverse_id('siege of towers'/'GPT', '97234').
card_watermark('siege of towers'/'GPT', 'Izzet').

card_in_set('silhana ledgewalker', 'GPT').
card_original_type('silhana ledgewalker'/'GPT', 'Creature — Elf Rogue').
card_original_text('silhana ledgewalker'/'GPT', 'Silhana Ledgewalker can\'t be blocked except by creatures with flying.\nSilhana Ledgewalker can\'t be the target of spells or abilities your opponents control.').
card_first_print('silhana ledgewalker', 'GPT').
card_image_name('silhana ledgewalker'/'GPT', 'silhana ledgewalker').
card_uid('silhana ledgewalker'/'GPT', 'GPT:Silhana Ledgewalker:silhana ledgewalker').
card_rarity('silhana ledgewalker'/'GPT', 'Common').
card_artist('silhana ledgewalker'/'GPT', 'James Wong').
card_number('silhana ledgewalker'/'GPT', '94').
card_flavor_text('silhana ledgewalker'/'GPT', 'Street folk call them \"spire mice,\" but behind the mockery is an unspoken envy of the ledgewalkers\' skill at avoiding harm.').
card_multiverse_id('silhana ledgewalker'/'GPT', '96825').

card_in_set('silhana starfletcher', 'GPT').
card_original_type('silhana starfletcher'/'GPT', 'Creature — Elf Druid Archer').
card_original_text('silhana starfletcher'/'GPT', 'As Silhana Starfletcher comes into play, choose a color.\n{T}: Add one mana of the chosen color to your mana pool.\nSilhana Starfletcher can block as though it had flying.').
card_first_print('silhana starfletcher', 'GPT').
card_image_name('silhana starfletcher'/'GPT', 'silhana starfletcher').
card_uid('silhana starfletcher'/'GPT', 'GPT:Silhana Starfletcher:silhana starfletcher').
card_rarity('silhana starfletcher'/'GPT', 'Common').
card_artist('silhana starfletcher'/'GPT', 'Alex Horley-Orlandelli').
card_number('silhana starfletcher'/'GPT', '95').
card_flavor_text('silhana starfletcher'/'GPT', 'In light he finds both gift and weapon.').
card_multiverse_id('silhana starfletcher'/'GPT', '96863').

card_in_set('sinstriker\'s will', 'GPT').
card_original_type('sinstriker\'s will'/'GPT', 'Enchantment — Aura').
card_original_text('sinstriker\'s will'/'GPT', 'Enchant creature\nEnchanted creature has \"{T}: This creature deals damage equal to its power to target attacking or blocking creature.\"').
card_first_print('sinstriker\'s will', 'GPT').
card_image_name('sinstriker\'s will'/'GPT', 'sinstriker\'s will').
card_uid('sinstriker\'s will'/'GPT', 'GPT:Sinstriker\'s Will:sinstriker\'s will').
card_rarity('sinstriker\'s will'/'GPT', 'Uncommon').
card_artist('sinstriker\'s will'/'GPT', 'Ben Thompson').
card_number('sinstriker\'s will'/'GPT', '16').
card_flavor_text('sinstriker\'s will'/'GPT', '\"Steel rusts. Arrows break. But righteousness is always strong, always sharp.\"').
card_multiverse_id('sinstriker\'s will'/'GPT', '96889').

card_in_set('skarrg, the rage pits', 'GPT').
card_original_type('skarrg, the rage pits'/'GPT', 'Land').
card_original_text('skarrg, the rage pits'/'GPT', '{T}: Add {1} to your mana pool.\n{R}{G}, {T}: Target creature gets +1/+1 and gains trample until end of turn.').
card_first_print('skarrg, the rage pits', 'GPT').
card_image_name('skarrg, the rage pits'/'GPT', 'skarrg, the rage pits').
card_uid('skarrg, the rage pits'/'GPT', 'GPT:Skarrg, the Rage Pits:skarrg, the rage pits').
card_rarity('skarrg, the rage pits'/'GPT', 'Uncommon').
card_artist('skarrg, the rage pits'/'GPT', 'Martina Pilcerova').
card_number('skarrg, the rage pits'/'GPT', '163').
card_flavor_text('skarrg, the rage pits'/'GPT', '\"This palace will be our fire-spit, and roasted prince our victory meal. Send in the torch-pigs!\"\n—Ghut Rak, Gruul guildmage').
card_multiverse_id('skarrg, the rage pits'/'GPT', '96970').
card_watermark('skarrg, the rage pits'/'GPT', 'Gruul').

card_in_set('skarrgan firebird', 'GPT').
card_original_type('skarrgan firebird'/'GPT', 'Creature — Phoenix').
card_original_text('skarrgan firebird'/'GPT', 'Bloodthirst 3 (If an opponent was dealt damage this turn, this creature comes into play with three +1/+1 counters on it.)\nFlying\n{R}{R}{R}: Return Skarrgan Firebird from your graveyard to your hand. Play this ability only if an opponent was dealt damage this turn.').
card_first_print('skarrgan firebird', 'GPT').
card_image_name('skarrgan firebird'/'GPT', 'skarrgan firebird').
card_uid('skarrgan firebird'/'GPT', 'GPT:Skarrgan Firebird:skarrgan firebird').
card_rarity('skarrgan firebird'/'GPT', 'Rare').
card_artist('skarrgan firebird'/'GPT', 'Kev Walker').
card_number('skarrgan firebird'/'GPT', '77').
card_multiverse_id('skarrgan firebird'/'GPT', '96957').
card_watermark('skarrgan firebird'/'GPT', 'Gruul').

card_in_set('skarrgan pit-skulk', 'GPT').
card_original_type('skarrgan pit-skulk'/'GPT', 'Creature — Human Warrior').
card_original_text('skarrgan pit-skulk'/'GPT', 'Bloodthirst 1 (If an opponent was dealt damage this turn, this creature comes into play with a +1/+1 counter on it.)\nCreatures with power less than Skarrgan Pit-Skulk\'s power can\'t block it.').
card_first_print('skarrgan pit-skulk', 'GPT').
card_image_name('skarrgan pit-skulk'/'GPT', 'skarrgan pit-skulk').
card_uid('skarrgan pit-skulk'/'GPT', 'GPT:Skarrgan Pit-Skulk:skarrgan pit-skulk').
card_rarity('skarrgan pit-skulk'/'GPT', 'Common').
card_artist('skarrgan pit-skulk'/'GPT', 'Liam Sharp').
card_number('skarrgan pit-skulk'/'GPT', '96').
card_multiverse_id('skarrgan pit-skulk'/'GPT', '96940').
card_watermark('skarrgan pit-skulk'/'GPT', 'Gruul').

card_in_set('skarrgan skybreaker', 'GPT').
card_original_type('skarrgan skybreaker'/'GPT', 'Creature — Giant Shaman').
card_original_text('skarrgan skybreaker'/'GPT', 'Bloodthirst 3 (If an opponent was dealt damage this turn, this creature comes into play with three +1/+1 counters on it.)\n{1}, Sacrifice Skarrgan Skybreaker: Skarrgan Skybreaker deals damage equal to its power to target creature or player.').
card_first_print('skarrgan skybreaker', 'GPT').
card_image_name('skarrgan skybreaker'/'GPT', 'skarrgan skybreaker').
card_uid('skarrgan skybreaker'/'GPT', 'GPT:Skarrgan Skybreaker:skarrgan skybreaker').
card_rarity('skarrgan skybreaker'/'GPT', 'Uncommon').
card_artist('skarrgan skybreaker'/'GPT', 'Dan Scott').
card_number('skarrgan skybreaker'/'GPT', '130').
card_flavor_text('skarrgan skybreaker'/'GPT', '\"The sky isn\'t falling—it\'s being thrown at us!\"\n—Otak, Tin Street shopkeep').
card_multiverse_id('skarrgan skybreaker'/'GPT', '97222').
card_watermark('skarrgan skybreaker'/'GPT', 'Gruul').

card_in_set('skeletal vampire', 'GPT').
card_original_type('skeletal vampire'/'GPT', 'Creature — Vampire Skeleton').
card_original_text('skeletal vampire'/'GPT', 'Flying\nWhen Skeletal Vampire comes into play, put two 1/1 black Bat creature tokens with flying into play.\n{3}{B}{B}, Sacrifice a Bat: Put two 1/1 black Bat creature tokens with flying into play.\nSacrifice a Bat: Regenerate Skeletal Vampire.').
card_first_print('skeletal vampire', 'GPT').
card_image_name('skeletal vampire'/'GPT', 'skeletal vampire').
card_uid('skeletal vampire'/'GPT', 'GPT:Skeletal Vampire:skeletal vampire').
card_rarity('skeletal vampire'/'GPT', 'Rare').
card_artist('skeletal vampire'/'GPT', 'Wayne Reynolds').
card_number('skeletal vampire'/'GPT', '62').
card_multiverse_id('skeletal vampire'/'GPT', '96899').

card_in_set('sky swallower', 'GPT').
card_original_type('sky swallower'/'GPT', 'Creature — Leviathan').
card_original_text('sky swallower'/'GPT', 'Flying\nWhen Sky Swallower comes into play, target opponent gains control of all other permanents you control.').
card_first_print('sky swallower', 'GPT').
card_image_name('sky swallower'/'GPT', 'sky swallower').
card_uid('sky swallower'/'GPT', 'GPT:Sky Swallower:sky swallower').
card_rarity('sky swallower'/'GPT', 'Rare').
card_artist('sky swallower'/'GPT', 'rk post').
card_number('sky swallower'/'GPT', '34').
card_flavor_text('sky swallower'/'GPT', 'Would you trade the world for such a pet?').
card_multiverse_id('sky swallower'/'GPT', '96835').

card_in_set('skyrider trainee', 'GPT').
card_original_type('skyrider trainee'/'GPT', 'Creature — Human Soldier').
card_original_text('skyrider trainee'/'GPT', 'As long as Skyrider Trainee is enchanted, it has flying.').
card_first_print('skyrider trainee', 'GPT').
card_image_name('skyrider trainee'/'GPT', 'skyrider trainee').
card_uid('skyrider trainee'/'GPT', 'GPT:Skyrider Trainee:skyrider trainee').
card_rarity('skyrider trainee'/'GPT', 'Common').
card_artist('skyrider trainee'/'GPT', 'Adam Rex').
card_number('skyrider trainee'/'GPT', '17').
card_flavor_text('skyrider trainee'/'GPT', '\"Of course I plan on going up. Kang here is the most trusted \'fin on the squad. Afraid? Me? No, I\'m just . . . waiting for the right wind conditions.\"').
card_multiverse_id('skyrider trainee'/'GPT', '96838').

card_in_set('smogsteed rider', 'GPT').
card_original_type('smogsteed rider'/'GPT', 'Creature — Human Wizard').
card_original_text('smogsteed rider'/'GPT', 'Whenever Smogsteed Rider attacks, each other attacking creature gains fear until end of turn.').
card_first_print('smogsteed rider', 'GPT').
card_image_name('smogsteed rider'/'GPT', 'smogsteed rider').
card_uid('smogsteed rider'/'GPT', 'GPT:Smogsteed Rider:smogsteed rider').
card_rarity('smogsteed rider'/'GPT', 'Uncommon').
card_artist('smogsteed rider'/'GPT', 'Alex Horley-Orlandelli').
card_number('smogsteed rider'/'GPT', '63').
card_flavor_text('smogsteed rider'/'GPT', '\"An entire order of mages who draw their power from Ravnica\'s smog? Yet another indicator of our world\'s decay.\"\n—Dravash, dowsing shaman').
card_multiverse_id('smogsteed rider'/'GPT', '96932').

card_in_set('souls of the faultless', 'GPT').
card_original_type('souls of the faultless'/'GPT', 'Creature — Spirit').
card_original_text('souls of the faultless'/'GPT', 'Defender (This creature can\'t attack.)\nWhenever Souls of the Faultless is dealt combat damage, you gain that much life and attacking player loses that much life.').
card_first_print('souls of the faultless', 'GPT').
card_image_name('souls of the faultless'/'GPT', 'souls of the faultless').
card_uid('souls of the faultless'/'GPT', 'GPT:Souls of the Faultless:souls of the faultless').
card_rarity('souls of the faultless'/'GPT', 'Uncommon').
card_artist('souls of the faultless'/'GPT', 'Pat Lee').
card_number('souls of the faultless'/'GPT', '131').
card_flavor_text('souls of the faultless'/'GPT', '\"More horrible than their empty forms are their noble eyes. I dare not strike.\"\n—Klattic, Boros legionnaire').
card_multiverse_id('souls of the faultless'/'GPT', '97231').
card_watermark('souls of the faultless'/'GPT', 'Orzhov').

card_in_set('spelltithe enforcer', 'GPT').
card_original_type('spelltithe enforcer'/'GPT', 'Creature — Elephant Wizard').
card_original_text('spelltithe enforcer'/'GPT', 'Whenever an opponent plays a spell, that player sacrifices a permanent unless he or she pays {1}.').
card_first_print('spelltithe enforcer', 'GPT').
card_image_name('spelltithe enforcer'/'GPT', 'spelltithe enforcer').
card_uid('spelltithe enforcer'/'GPT', 'GPT:Spelltithe Enforcer:spelltithe enforcer').
card_rarity('spelltithe enforcer'/'GPT', 'Rare').
card_artist('spelltithe enforcer'/'GPT', 'Greg Staples').
card_number('spelltithe enforcer'/'GPT', '18').
card_flavor_text('spelltithe enforcer'/'GPT', 'His assistants calculate the amount of the tribute. He measures out the punishment for delinquency.').
card_multiverse_id('spelltithe enforcer'/'GPT', '96865').

card_in_set('starved rusalka', 'GPT').
card_original_type('starved rusalka'/'GPT', 'Creature — Spirit').
card_original_text('starved rusalka'/'GPT', '{G}, Sacrifice a creature: You gain 1 life.').
card_first_print('starved rusalka', 'GPT').
card_image_name('starved rusalka'/'GPT', 'starved rusalka').
card_uid('starved rusalka'/'GPT', 'GPT:Starved Rusalka:starved rusalka').
card_rarity('starved rusalka'/'GPT', 'Uncommon').
card_artist('starved rusalka'/'GPT', 'Dany Orizio').
card_number('starved rusalka'/'GPT', '97').
card_flavor_text('starved rusalka'/'GPT', '\"No more wretched fate is there than to waste away. If you are slain, your ghost may at least avenge itself upon your killer. But what release is there for one slain by Time?\"\n—Ilromov, traveling storyteller').
card_multiverse_id('starved rusalka'/'GPT', '107692').

card_in_set('steam vents', 'GPT').
card_original_type('steam vents'/'GPT', 'Land — Island Mountain').
card_original_text('steam vents'/'GPT', '({T}: Add {U} or {R} to your mana pool.)\nAs Steam Vents comes into play, you may pay 2 life. If you don\'t, Steam Vents comes into play tapped instead.').
card_first_print('steam vents', 'GPT').
card_image_name('steam vents'/'GPT', 'steam vents').
card_uid('steam vents'/'GPT', 'GPT:Steam Vents:steam vents').
card_rarity('steam vents'/'GPT', 'Rare').
card_artist('steam vents'/'GPT', 'Rob Alexander').
card_number('steam vents'/'GPT', '164').
card_multiverse_id('steam vents'/'GPT', '96923').
card_watermark('steam vents'/'GPT', 'Izzet').

card_in_set('steamcore weird', 'GPT').
card_original_type('steamcore weird'/'GPT', 'Creature — Weird').
card_original_text('steamcore weird'/'GPT', 'When Steamcore Weird comes into play, if {R} was spent to play Steamcore Weird, it deals 2 damage to target creature or player.').
card_first_print('steamcore weird', 'GPT').
card_image_name('steamcore weird'/'GPT', 'steamcore weird').
card_uid('steamcore weird'/'GPT', 'GPT:Steamcore Weird:steamcore weird').
card_rarity('steamcore weird'/'GPT', 'Common').
card_artist('steamcore weird'/'GPT', 'Justin Norman').
card_number('steamcore weird'/'GPT', '35').
card_flavor_text('steamcore weird'/'GPT', 'Like many Izzet creations, weirds are based on wild contradictions yet somehow manage to work.').
card_multiverse_id('steamcore weird'/'GPT', '97201').
card_watermark('steamcore weird'/'GPT', 'Izzet').

card_in_set('stitch in time', 'GPT').
card_original_type('stitch in time'/'GPT', 'Sorcery').
card_original_text('stitch in time'/'GPT', 'Flip a coin. If you win the flip, take an extra turn after this one.').
card_first_print('stitch in time', 'GPT').
card_image_name('stitch in time'/'GPT', 'stitch in time').
card_uid('stitch in time'/'GPT', 'GPT:Stitch in Time:stitch in time').
card_rarity('stitch in time'/'GPT', 'Rare').
card_artist('stitch in time'/'GPT', 'Jon Foster').
card_number('stitch in time'/'GPT', '132').
card_flavor_text('stitch in time'/'GPT', 'Quyzl was told by his mentor to \"make more time\" for his studies.').
card_multiverse_id('stitch in time'/'GPT', '96928').
card_watermark('stitch in time'/'GPT', 'Izzet').

card_in_set('stomping ground', 'GPT').
card_original_type('stomping ground'/'GPT', 'Land — Mountain Forest').
card_original_text('stomping ground'/'GPT', '({T}: Add {R} or {G} to your mana pool.)\nAs Stomping Ground comes into play, you may pay 2 life. If you don\'t, Stomping Ground comes into play tapped instead.').
card_first_print('stomping ground', 'GPT').
card_image_name('stomping ground'/'GPT', 'stomping ground').
card_uid('stomping ground'/'GPT', 'GPT:Stomping Ground:stomping ground').
card_rarity('stomping ground'/'GPT', 'Rare').
card_artist('stomping ground'/'GPT', 'Rob Alexander').
card_number('stomping ground'/'GPT', '165').
card_multiverse_id('stomping ground'/'GPT', '96896').
card_watermark('stomping ground'/'GPT', 'Gruul').

card_in_set('storm herd', 'GPT').
card_original_type('storm herd'/'GPT', 'Sorcery').
card_original_text('storm herd'/'GPT', 'Put X 1/1 white Pegasus creature tokens with flying into play, where X is your life total.').
card_first_print('storm herd', 'GPT').
card_image_name('storm herd'/'GPT', 'storm herd').
card_uid('storm herd'/'GPT', 'GPT:Storm Herd:storm herd').
card_rarity('storm herd'/'GPT', 'Rare').
card_artist('storm herd'/'GPT', 'Jim Nelson').
card_number('storm herd'/'GPT', '19').
card_flavor_text('storm herd'/'GPT', '\"When you hear thunder on a cloudless day, take cover and brace for the coming of the storm herd.\"\n—Skotov, Tin Street basket vendor').
card_multiverse_id('storm herd'/'GPT', '96968').

card_in_set('stratozeppelid', 'GPT').
card_original_type('stratozeppelid'/'GPT', 'Creature — Beast').
card_original_text('stratozeppelid'/'GPT', 'Flying\nStratozeppelid can block only creatures with flying.').
card_first_print('stratozeppelid', 'GPT').
card_image_name('stratozeppelid'/'GPT', 'stratozeppelid').
card_uid('stratozeppelid'/'GPT', 'GPT:Stratozeppelid:stratozeppelid').
card_rarity('stratozeppelid'/'GPT', 'Uncommon').
card_artist('stratozeppelid'/'GPT', 'Ittoku').
card_number('stratozeppelid'/'GPT', '36').
card_flavor_text('stratozeppelid'/'GPT', 'The \"Days of Darkness\" mark the stratozeppelids\' annual migration, when for five days they blot out the sun and trumpet their passing.').
card_multiverse_id('stratozeppelid'/'GPT', '96927').

card_in_set('streetbreaker wurm', 'GPT').
card_original_type('streetbreaker wurm'/'GPT', 'Creature — Wurm').
card_original_text('streetbreaker wurm'/'GPT', '').
card_first_print('streetbreaker wurm', 'GPT').
card_image_name('streetbreaker wurm'/'GPT', 'streetbreaker wurm').
card_uid('streetbreaker wurm'/'GPT', 'GPT:Streetbreaker Wurm:streetbreaker wurm').
card_rarity('streetbreaker wurm'/'GPT', 'Common').
card_artist('streetbreaker wurm'/'GPT', 'Greg Hildebrandt').
card_number('streetbreaker wurm'/'GPT', '133').
card_flavor_text('streetbreaker wurm'/'GPT', 'The Orzhov run a construction unit whose only business is rebuilding the wreckage left in the wake of wurms. When business is slow, they pay Golgari carnomancers to lure the wurms to the surface.').
card_multiverse_id('streetbreaker wurm'/'GPT', '106575').
card_watermark('streetbreaker wurm'/'GPT', 'Gruul').

card_in_set('sword of the paruns', 'GPT').
card_original_type('sword of the paruns'/'GPT', 'Artifact — Equipment').
card_original_text('sword of the paruns'/'GPT', 'As long as equipped creature is tapped, tapped creatures you control get +2/+0.\nAs long as equipped creature is untapped, untapped creatures you control get +0/+2.\n{3}: Tap or untap equipped creature.\nEquip {3}').
card_first_print('sword of the paruns', 'GPT').
card_image_name('sword of the paruns'/'GPT', 'sword of the paruns').
card_uid('sword of the paruns'/'GPT', 'GPT:Sword of the Paruns:sword of the paruns').
card_rarity('sword of the paruns'/'GPT', 'Rare').
card_artist('sword of the paruns'/'GPT', 'Tim Hildebrandt').
card_number('sword of the paruns'/'GPT', '156').
card_multiverse_id('sword of the paruns'/'GPT', '107095').

card_in_set('teysa, orzhov scion', 'GPT').
card_original_type('teysa, orzhov scion'/'GPT', 'Legendary Creature — Human Advisor').
card_original_text('teysa, orzhov scion'/'GPT', 'Sacrifice three white creatures: Remove target creature from the game.\nWhenever another black creature you control is put into a graveyard from play, put a 1/1 white Spirit creature token with flying into play.').
card_first_print('teysa, orzhov scion', 'GPT').
card_image_name('teysa, orzhov scion'/'GPT', 'teysa, orzhov scion').
card_uid('teysa, orzhov scion'/'GPT', 'GPT:Teysa, Orzhov Scion:teysa, orzhov scion').
card_rarity('teysa, orzhov scion'/'GPT', 'Rare').
card_artist('teysa, orzhov scion'/'GPT', 'Todd Lockwood').
card_number('teysa, orzhov scion'/'GPT', '134').
card_multiverse_id('teysa, orzhov scion'/'GPT', '83546').
card_watermark('teysa, orzhov scion'/'GPT', 'Orzhov').

card_in_set('thunderheads', 'GPT').
card_original_type('thunderheads'/'GPT', 'Instant').
card_original_text('thunderheads'/'GPT', 'Replicate {2}{U} (When you play this spell, copy it for each time you paid its replicate cost.)\nPut a 3/3 blue Weird creature token with defender and flying into play. Remove it from the game at end of turn.').
card_first_print('thunderheads', 'GPT').
card_image_name('thunderheads'/'GPT', 'thunderheads').
card_uid('thunderheads'/'GPT', 'GPT:Thunderheads:thunderheads').
card_rarity('thunderheads'/'GPT', 'Uncommon').
card_artist('thunderheads'/'GPT', 'Hideaki Takamura').
card_number('thunderheads'/'GPT', '37').
card_flavor_text('thunderheads'/'GPT', 'The clouds grew thick, and then they grew teeth.').
card_multiverse_id('thunderheads'/'GPT', '96946').
card_watermark('thunderheads'/'GPT', 'Izzet').

card_in_set('tibor and lumia', 'GPT').
card_original_type('tibor and lumia'/'GPT', 'Legendary Creature — Human Wizard').
card_original_text('tibor and lumia'/'GPT', 'Whenever you play a blue spell, target creature gains flying until end of turn.\nWhenever you play a red spell, Tibor and Lumia deals 1 damage to each creature without flying.').
card_first_print('tibor and lumia', 'GPT').
card_image_name('tibor and lumia'/'GPT', 'tibor and lumia').
card_uid('tibor and lumia'/'GPT', 'GPT:Tibor and Lumia:tibor and lumia').
card_rarity('tibor and lumia'/'GPT', 'Rare').
card_artist('tibor and lumia'/'GPT', 'Jim Murray').
card_number('tibor and lumia'/'GPT', '135').
card_flavor_text('tibor and lumia'/'GPT', 'Tracing the horizon in a dance of wind and fire.').
card_multiverse_id('tibor and lumia'/'GPT', '97208').
card_watermark('tibor and lumia'/'GPT', 'Izzet').

card_in_set('tin street hooligan', 'GPT').
card_original_type('tin street hooligan'/'GPT', 'Creature — Goblin Rogue').
card_original_text('tin street hooligan'/'GPT', 'When Tin Street Hooligan comes into play, if {G} was spent to play Tin Street Hooligan, destroy target artifact.').
card_first_print('tin street hooligan', 'GPT').
card_image_name('tin street hooligan'/'GPT', 'tin street hooligan').
card_uid('tin street hooligan'/'GPT', 'GPT:Tin Street Hooligan:tin street hooligan').
card_rarity('tin street hooligan'/'GPT', 'Common').
card_artist('tin street hooligan'/'GPT', 'Luca Zontini').
card_number('tin street hooligan'/'GPT', '78').
card_flavor_text('tin street hooligan'/'GPT', '\"Rauck-Chauv\'s like a holiday! Only it isn\'t on the calendars, and instead of dancing you knock people flat, and instead of giving gifts you break stuff.\"').
card_multiverse_id('tin street hooligan'/'GPT', '96960').
card_watermark('tin street hooligan'/'GPT', 'Gruul').

card_in_set('to arms!', 'GPT').
card_original_type('to arms!'/'GPT', 'Instant').
card_original_text('to arms!'/'GPT', 'Untap all creatures you control.\nDraw a card.').
card_first_print('to arms!', 'GPT').
card_image_name('to arms!'/'GPT', 'to arms!').
card_uid('to arms!'/'GPT', 'GPT:To Arms!:to arms!').
card_rarity('to arms!'/'GPT', 'Uncommon').
card_artist('to arms!'/'GPT', 'Aleksi Briclot').
card_number('to arms!'/'GPT', '20').
card_flavor_text('to arms!'/'GPT', '\"The call came, spell-borne through mortar and stone to the ear of every soldier. The call came, and the advantage was ours.\"\n—Lodusz, captain of the watch').
card_multiverse_id('to arms!'/'GPT', '83739').

card_in_set('torch drake', 'GPT').
card_original_type('torch drake'/'GPT', 'Creature — Drake').
card_original_text('torch drake'/'GPT', 'Flying\n{1}{R}: Torch Drake gets +1/+0 until end of turn.').
card_first_print('torch drake', 'GPT').
card_image_name('torch drake'/'GPT', 'torch drake').
card_uid('torch drake'/'GPT', 'GPT:Torch Drake:torch drake').
card_rarity('torch drake'/'GPT', 'Common').
card_artist('torch drake'/'GPT', 'Daren Bader').
card_number('torch drake'/'GPT', '38').
card_flavor_text('torch drake'/'GPT', '\"Drakes? Bah! Things that breathe don\'t interest me. It breathes fire, you say? Well, that\'s a different story!\"\n—Zataz, Izzet clockwork artificer').
card_multiverse_id('torch drake'/'GPT', '97218').
card_watermark('torch drake'/'GPT', 'Izzet').

card_in_set('train of thought', 'GPT').
card_original_type('train of thought'/'GPT', 'Sorcery').
card_original_text('train of thought'/'GPT', 'Replicate {1}{U} (When you play this spell, copy it for each time you paid its replicate cost.)\nDraw a card.').
card_first_print('train of thought', 'GPT').
card_image_name('train of thought'/'GPT', 'train of thought').
card_uid('train of thought'/'GPT', 'GPT:Train of Thought:train of thought').
card_rarity('train of thought'/'GPT', 'Common').
card_artist('train of thought'/'GPT', 'Matt Thompson').
card_number('train of thought'/'GPT', '39').
card_flavor_text('train of thought'/'GPT', '\"But then . . . oh, but . . . which means . . . which would lead to . . . exactly!\"').
card_multiverse_id('train of thought'/'GPT', '96906').
card_watermark('train of thought'/'GPT', 'Izzet').

card_in_set('ulasht, the hate seed', 'GPT').
card_original_type('ulasht, the hate seed'/'GPT', 'Legendary Creature — Hydra').
card_original_text('ulasht, the hate seed'/'GPT', 'Ulasht, the Hate Seed comes into play with a +1/+1 counter on it for each other red creature you control and a +1/+1 counter on it for each other green creature you control.\n{1}, Remove a +1/+1 counter from Ulasht: Choose one Ulasht deals 1 damage to target creature; or put a 1/1 green Saproling creature token into play.').
card_first_print('ulasht, the hate seed', 'GPT').
card_image_name('ulasht, the hate seed'/'GPT', 'ulasht, the hate seed').
card_uid('ulasht, the hate seed'/'GPT', 'GPT:Ulasht, the Hate Seed:ulasht, the hate seed').
card_rarity('ulasht, the hate seed'/'GPT', 'Rare').
card_artist('ulasht, the hate seed'/'GPT', 'Nottsuo').
card_number('ulasht, the hate seed'/'GPT', '136').
card_multiverse_id('ulasht, the hate seed'/'GPT', '97212').
card_watermark('ulasht, the hate seed'/'GPT', 'Gruul').

card_in_set('vacuumelt', 'GPT').
card_original_type('vacuumelt'/'GPT', 'Sorcery').
card_original_text('vacuumelt'/'GPT', 'Replicate {2}{U} (When you play this spell, copy it for each time you paid its replicate cost. You may choose new targets for the copies.)\nReturn target creature to its owner\'s hand.').
card_first_print('vacuumelt', 'GPT').
card_image_name('vacuumelt'/'GPT', 'vacuumelt').
card_uid('vacuumelt'/'GPT', 'GPT:Vacuumelt:vacuumelt').
card_rarity('vacuumelt'/'GPT', 'Uncommon').
card_artist('vacuumelt'/'GPT', 'Nottsuo').
card_number('vacuumelt'/'GPT', '40').
card_multiverse_id('vacuumelt'/'GPT', '96851').
card_watermark('vacuumelt'/'GPT', 'Izzet').

card_in_set('vedalken plotter', 'GPT').
card_original_type('vedalken plotter'/'GPT', 'Creature — Vedalken Wizard').
card_original_text('vedalken plotter'/'GPT', 'When Vedalken Plotter comes into play, exchange control of target land you control and target land an opponent controls.').
card_first_print('vedalken plotter', 'GPT').
card_image_name('vedalken plotter'/'GPT', 'vedalken plotter').
card_uid('vedalken plotter'/'GPT', 'GPT:Vedalken Plotter:vedalken plotter').
card_rarity('vedalken plotter'/'GPT', 'Uncommon').
card_artist('vedalken plotter'/'GPT', 'Greg Staples').
card_number('vedalken plotter'/'GPT', '41').
card_flavor_text('vedalken plotter'/'GPT', '\"Fair? At what point in our negotiations did you convince yourself my goal was to be fair?\"').
card_multiverse_id('vedalken plotter'/'GPT', '96920').

card_in_set('vertigo spawn', 'GPT').
card_original_type('vertigo spawn'/'GPT', 'Creature — Illusion').
card_original_text('vertigo spawn'/'GPT', 'Defender (This creature can\'t attack.)\nWhenever Vertigo Spawn blocks a creature, tap that creature. It doesn\'t untap during its controller\'s next untap step.').
card_first_print('vertigo spawn', 'GPT').
card_image_name('vertigo spawn'/'GPT', 'vertigo spawn').
card_uid('vertigo spawn'/'GPT', 'GPT:Vertigo Spawn:vertigo spawn').
card_rarity('vertigo spawn'/'GPT', 'Uncommon').
card_artist('vertigo spawn'/'GPT', 'Ittoku').
card_number('vertigo spawn'/'GPT', '42').
card_multiverse_id('vertigo spawn'/'GPT', '96860').

card_in_set('wee dragonauts', 'GPT').
card_original_type('wee dragonauts'/'GPT', 'Creature — Faerie Wizard').
card_original_text('wee dragonauts'/'GPT', 'Flying\nWhenever you play an instant or sorcery spell, Wee Dragonauts gets +2/+0 until end of turn.').
card_image_name('wee dragonauts'/'GPT', 'wee dragonauts').
card_uid('wee dragonauts'/'GPT', 'GPT:Wee Dragonauts:wee dragonauts').
card_rarity('wee dragonauts'/'GPT', 'Common').
card_artist('wee dragonauts'/'GPT', 'Greg Staples').
card_number('wee dragonauts'/'GPT', '137').
card_flavor_text('wee dragonauts'/'GPT', '\"The blazekite is a simple concept, really—just a vehicular application of dragscoop ionics and electropropulsion magnetronics.\"\n—Juzba, Izzet tinker').
card_multiverse_id('wee dragonauts'/'GPT', '96903').
card_watermark('wee dragonauts'/'GPT', 'Izzet').

card_in_set('wild cantor', 'GPT').
card_original_type('wild cantor'/'GPT', 'Creature — Human Druid').
card_original_text('wild cantor'/'GPT', '({R/G} can be paid with either {R} or {G}.)\nSacrifice Wild Cantor: Add one mana of any color to your mana pool.').
card_first_print('wild cantor', 'GPT').
card_image_name('wild cantor'/'GPT', 'wild cantor').
card_uid('wild cantor'/'GPT', 'GPT:Wild Cantor:wild cantor').
card_rarity('wild cantor'/'GPT', 'Common').
card_artist('wild cantor'/'GPT', 'Glenn Fabry').
card_number('wild cantor'/'GPT', '149').
card_flavor_text('wild cantor'/'GPT', 'They are the voice of the wild, crying out with nature\'s fury and bringing forth its primeval might.').
card_multiverse_id('wild cantor'/'GPT', '96934').
card_watermark('wild cantor'/'GPT', 'Gruul').

card_in_set('wildsize', 'GPT').
card_original_type('wildsize'/'GPT', 'Instant').
card_original_text('wildsize'/'GPT', 'Target creature gets +2/+2 and gains trample until end of turn.\nDraw a card.').
card_first_print('wildsize', 'GPT').
card_image_name('wildsize'/'GPT', 'wildsize').
card_uid('wildsize'/'GPT', 'GPT:Wildsize:wildsize').
card_rarity('wildsize'/'GPT', 'Common').
card_artist('wildsize'/'GPT', 'Jim Murray').
card_number('wildsize'/'GPT', '98').
card_flavor_text('wildsize'/'GPT', 'Two times the size, four times the smell.').
card_multiverse_id('wildsize'/'GPT', '107097').

card_in_set('witch-maw nephilim', 'GPT').
card_original_type('witch-maw nephilim'/'GPT', 'Creature — Nephilim').
card_original_text('witch-maw nephilim'/'GPT', 'Whenever you play a spell, you may put two +1/+1 counters on Witch-Maw Nephilim.\nWhenever Witch-Maw Nephilim attacks, it gains trample until end of turn if its power is 10 or greater.').
card_first_print('witch-maw nephilim', 'GPT').
card_image_name('witch-maw nephilim'/'GPT', 'witch-maw nephilim').
card_uid('witch-maw nephilim'/'GPT', 'GPT:Witch-Maw Nephilim:witch-maw nephilim').
card_rarity('witch-maw nephilim'/'GPT', 'Rare').
card_artist('witch-maw nephilim'/'GPT', 'Greg Staples').
card_number('witch-maw nephilim'/'GPT', '138').
card_flavor_text('witch-maw nephilim'/'GPT', 'When it awoke, it shattered the hillsides to make way for its passage.').
card_multiverse_id('witch-maw nephilim'/'GPT', '107090').

card_in_set('withstand', 'GPT').
card_original_type('withstand'/'GPT', 'Instant').
card_original_text('withstand'/'GPT', 'Prevent the next 3 damage that would be dealt to target creature or player this turn.\nDraw a card.').
card_first_print('withstand', 'GPT').
card_image_name('withstand'/'GPT', 'withstand').
card_uid('withstand'/'GPT', 'GPT:Withstand:withstand').
card_rarity('withstand'/'GPT', 'Common').
card_artist('withstand'/'GPT', 'Ron Spears').
card_number('withstand'/'GPT', '21').
card_flavor_text('withstand'/'GPT', '\"Defense is as much a part of war as offense, the shield as important a tool as the sword.\"\n—Alovnek, Boros guildmage').
card_multiverse_id('withstand'/'GPT', '97203').

card_in_set('wreak havoc', 'GPT').
card_original_type('wreak havoc'/'GPT', 'Sorcery').
card_original_text('wreak havoc'/'GPT', 'Wreak Havoc can\'t be countered by spells or abilities.\nDestroy target artifact or land.').
card_first_print('wreak havoc', 'GPT').
card_image_name('wreak havoc'/'GPT', 'wreak havoc').
card_uid('wreak havoc'/'GPT', 'GPT:Wreak Havoc:wreak havoc').
card_rarity('wreak havoc'/'GPT', 'Uncommon').
card_artist('wreak havoc'/'GPT', 'Wayne Reynolds').
card_number('wreak havoc'/'GPT', '139').
card_flavor_text('wreak havoc'/'GPT', '\"Crush them!\"\n—Borborygmos').
card_multiverse_id('wreak havoc'/'GPT', '107693').
card_watermark('wreak havoc'/'GPT', 'Gruul').

card_in_set('wurmweaver coil', 'GPT').
card_original_type('wurmweaver coil'/'GPT', 'Enchantment — Aura').
card_original_text('wurmweaver coil'/'GPT', 'Enchant green creature\nEnchanted creature gets +6/+6.\n{G}{G}{G}, Sacrifice Wurmweaver Coil: Put a 6/6 green Wurm creature token into play.').
card_first_print('wurmweaver coil', 'GPT').
card_image_name('wurmweaver coil'/'GPT', 'wurmweaver coil').
card_uid('wurmweaver coil'/'GPT', 'GPT:Wurmweaver Coil:wurmweaver coil').
card_rarity('wurmweaver coil'/'GPT', 'Rare').
card_artist('wurmweaver coil'/'GPT', 'Mitch Cotie').
card_number('wurmweaver coil'/'GPT', '99').
card_flavor_text('wurmweaver coil'/'GPT', 'Some use magic to spy through walls. Others use it to crush them.').
card_multiverse_id('wurmweaver coil'/'GPT', '97230').

card_in_set('yore-tiller nephilim', 'GPT').
card_original_type('yore-tiller nephilim'/'GPT', 'Creature — Nephilim').
card_original_text('yore-tiller nephilim'/'GPT', 'Whenever Yore-Tiller Nephilim attacks, return target creature card from your graveyard to play tapped and attacking.').
card_first_print('yore-tiller nephilim', 'GPT').
card_image_name('yore-tiller nephilim'/'GPT', 'yore-tiller nephilim').
card_uid('yore-tiller nephilim'/'GPT', 'GPT:Yore-Tiller Nephilim:yore-tiller nephilim').
card_rarity('yore-tiller nephilim'/'GPT', 'Rare').
card_artist('yore-tiller nephilim'/'GPT', 'Jeremy Jarvis').
card_number('yore-tiller nephilim'/'GPT', '140').
card_flavor_text('yore-tiller nephilim'/'GPT', 'When it awoke, the worms of the earth hissed in a chorus of beckoning.').
card_multiverse_id('yore-tiller nephilim'/'GPT', '107093').
