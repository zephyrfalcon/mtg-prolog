% Summer of Magic

set('pSUM').
set_name('pSUM', 'Summer of Magic').
set_release_date('pSUM', '2007-07-21').
set_border('pSUM', 'black').
set_type('pSUM', 'promo').

card_in_set('faerie conclave', 'pSUM').
card_original_type('faerie conclave'/'pSUM', 'Land').
card_original_text('faerie conclave'/'pSUM', '').
card_image_name('faerie conclave'/'pSUM', 'faerie conclave').
card_uid('faerie conclave'/'pSUM', 'pSUM:Faerie Conclave:faerie conclave').
card_rarity('faerie conclave'/'pSUM', 'Special').
card_artist('faerie conclave'/'pSUM', 'Stephan Martiniere').
card_number('faerie conclave'/'pSUM', '1').

card_in_set('treetop village', 'pSUM').
card_original_type('treetop village'/'pSUM', 'Land').
card_original_text('treetop village'/'pSUM', '').
card_image_name('treetop village'/'pSUM', 'treetop village').
card_uid('treetop village'/'pSUM', 'pSUM:Treetop Village:treetop village').
card_rarity('treetop village'/'pSUM', 'Special').
card_artist('treetop village'/'pSUM', 'Rob Alexander').
card_number('treetop village'/'pSUM', '2').
