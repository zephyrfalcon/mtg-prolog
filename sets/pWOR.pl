% Worlds

set('pWOR').
set_name('pWOR', 'Worlds').
set_release_date('pWOR', '1999-08-04').
set_border('pWOR', 'black').
set_type('pWOR', 'promo').

card_in_set('balduvian horde', 'pWOR').
card_original_type('balduvian horde'/'pWOR', 'Creature — Human Barbarian').
card_original_text('balduvian horde'/'pWOR', '').
card_image_name('balduvian horde'/'pWOR', 'balduvian horde').
card_uid('balduvian horde'/'pWOR', 'pWOR:Balduvian Horde:balduvian horde').
card_rarity('balduvian horde'/'pWOR', 'Special').
card_artist('balduvian horde'/'pWOR', 'Brian Snõddy').
card_number('balduvian horde'/'pWOR', '1').
