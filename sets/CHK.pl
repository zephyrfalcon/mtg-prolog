% Champions of Kamigawa

set('CHK').
set_name('CHK', 'Champions of Kamigawa').
set_release_date('CHK', '2004-10-01').
set_border('CHK', 'black').
set_type('CHK', 'expansion').
set_block('CHK', 'Kamigawa').

card_in_set('akki avalanchers', 'CHK').
card_original_type('akki avalanchers'/'CHK', 'Creature — Goblin Warrior').
card_original_text('akki avalanchers'/'CHK', 'Sacrifice a land: Akki Avalanchers gets +2/+0 until end of turn. Play this ability only once each turn.').
card_first_print('akki avalanchers', 'CHK').
card_image_name('akki avalanchers'/'CHK', 'akki avalanchers').
card_uid('akki avalanchers'/'CHK', 'CHK:Akki Avalanchers:akki avalanchers').
card_rarity('akki avalanchers'/'CHK', 'Common').
card_artist('akki avalanchers'/'CHK', 'Matt Thompson').
card_number('akki avalanchers'/'CHK', '151').
card_flavor_text('akki avalanchers'/'CHK', 'Among Godo\'s hordes, \"beware of falling rocks\" came to mean \"akki live nearby.\"').
card_multiverse_id('akki avalanchers'/'CHK', '75277').

card_in_set('akki coalflinger', 'CHK').
card_original_type('akki coalflinger'/'CHK', 'Creature — Goblin Shaman').
card_original_text('akki coalflinger'/'CHK', 'First strike\n{R}, {T}: Attacking creatures gain first strike until end of turn.').
card_first_print('akki coalflinger', 'CHK').
card_image_name('akki coalflinger'/'CHK', 'akki coalflinger').
card_uid('akki coalflinger'/'CHK', 'CHK:Akki Coalflinger:akki coalflinger').
card_rarity('akki coalflinger'/'CHK', 'Uncommon').
card_artist('akki coalflinger'/'CHK', 'Nottsuo').
card_number('akki coalflinger'/'CHK', '152').
card_flavor_text('akki coalflinger'/'CHK', 'No matter where you find them, goblins love rocks.').
card_multiverse_id('akki coalflinger'/'CHK', '50338').

card_in_set('akki lavarunner', 'CHK').
card_original_type('akki lavarunner'/'CHK', 'Creature — Goblin Warrior').
card_original_text('akki lavarunner'/'CHK', 'Haste\nWhenever Akki Lavarunner deals damage to an opponent, flip it.\n-----\nTok-Tok, Volcano Born\nLegendary Creature Goblin Shaman\n2/2\nProtection from red\nIf a red source would deal damage to a player, it deals that much damage plus 1 to that player instead.').
card_first_print('akki lavarunner', 'CHK').
card_image_name('akki lavarunner'/'CHK', 'akki lavarunner').
card_uid('akki lavarunner'/'CHK', 'CHK:Akki Lavarunner:akki lavarunner').
card_rarity('akki lavarunner'/'CHK', 'Rare').
card_artist('akki lavarunner'/'CHK', 'Matt Cavotta').
card_number('akki lavarunner'/'CHK', '153a').
card_multiverse_id('akki lavarunner'/'CHK', '78694').

card_in_set('akki rockspeaker', 'CHK').
card_original_type('akki rockspeaker'/'CHK', 'Creature — Goblin Shaman').
card_original_text('akki rockspeaker'/'CHK', 'When Akki Rockspeaker comes into play, add {R} to your mana pool.').
card_first_print('akki rockspeaker', 'CHK').
card_image_name('akki rockspeaker'/'CHK', 'akki rockspeaker').
card_uid('akki rockspeaker'/'CHK', 'CHK:Akki Rockspeaker:akki rockspeaker').
card_rarity('akki rockspeaker'/'CHK', 'Common').
card_artist('akki rockspeaker'/'CHK', 'David Martin').
card_number('akki rockspeaker'/'CHK', '154').
card_flavor_text('akki rockspeaker'/'CHK', 'The lava-proof shells of akki evolved over centuries, as akki pranks almost always got seriously out of hand.').
card_multiverse_id('akki rockspeaker'/'CHK', '77919').

card_in_set('akki underminer', 'CHK').
card_original_type('akki underminer'/'CHK', 'Creature — Goblin Rogue Shaman').
card_original_text('akki underminer'/'CHK', 'Whenever Akki Underminer deals combat damage to a player, that player sacrifices a permanent.').
card_first_print('akki underminer', 'CHK').
card_image_name('akki underminer'/'CHK', 'akki underminer').
card_uid('akki underminer'/'CHK', 'CHK:Akki Underminer:akki underminer').
card_rarity('akki underminer'/'CHK', 'Uncommon').
card_artist('akki underminer'/'CHK', 'Thomas M. Baxa').
card_number('akki underminer'/'CHK', '155').
card_flavor_text('akki underminer'/'CHK', '\"Deep inside the Sokenzan Mountains, a band of akki discovered a cache of ancient items of power. Their ensuing spree of destruction became known as ‘The Three Days of Fun.\'\"\n—Observations of the Kami War').
card_multiverse_id('akki underminer'/'CHK', '75399').

card_in_set('ashen-skin zubera', 'CHK').
card_original_type('ashen-skin zubera'/'CHK', 'Creature — Zubera Spirit').
card_original_text('ashen-skin zubera'/'CHK', 'When Ashen-Skin Zubera is put into a graveyard from play, target opponent discards a card for each Zubera put into a graveyard from play this turn.').
card_first_print('ashen-skin zubera', 'CHK').
card_image_name('ashen-skin zubera'/'CHK', 'ashen-skin zubera').
card_uid('ashen-skin zubera'/'CHK', 'CHK:Ashen-Skin Zubera:ashen-skin zubera').
card_rarity('ashen-skin zubera'/'CHK', 'Common').
card_artist('ashen-skin zubera'/'CHK', 'Wayne Reynolds').
card_number('ashen-skin zubera'/'CHK', '101').
card_flavor_text('ashen-skin zubera'/'CHK', 'When the Honden of Night\'s Reach began to crumble, its attendants swarmed Kamigawa to haunt mortal dreams.').
card_multiverse_id('ashen-skin zubera'/'CHK', '80509').

card_in_set('aura of dominion', 'CHK').
card_original_type('aura of dominion'/'CHK', 'Enchant Creature').
card_original_text('aura of dominion'/'CHK', '{1}, Tap an untapped creature you control: Untap enchanted creature.').
card_first_print('aura of dominion', 'CHK').
card_image_name('aura of dominion'/'CHK', 'aura of dominion').
card_uid('aura of dominion'/'CHK', 'CHK:Aura of Dominion:aura of dominion').
card_rarity('aura of dominion'/'CHK', 'Uncommon').
card_artist('aura of dominion'/'CHK', 'Randy Gallegos').
card_number('aura of dominion'/'CHK', '51').
card_flavor_text('aura of dominion'/'CHK', '\"Lies and deceit do yield results, but legitimate authority is the ultimate form of control.\"\n—Meloku the Clouded Mirror').
card_multiverse_id('aura of dominion'/'CHK', '75353').

card_in_set('autumn-tail, kitsune sage', 'CHK').
card_original_type('autumn-tail, kitsune sage'/'CHK', 'Creature — Fox Wizard').
card_original_text('autumn-tail, kitsune sage'/'CHK', 'At end of turn, if Kitsune Mystic is enchanted by two or more enchantments, flip it.\n-----\nAutumn-Tail, Kitsune Sage\nLegendary Creature Fox Wizard\n4/5\n{1}: Move target enchantment enchanting a creature to another creature.').
card_first_print('autumn-tail, kitsune sage', 'CHK').
card_image_name('autumn-tail, kitsune sage'/'CHK', 'autumn-tail, kitsune sage').
card_uid('autumn-tail, kitsune sage'/'CHK', 'CHK:Autumn-Tail, Kitsune Sage:autumn-tail, kitsune sage').
card_rarity('autumn-tail, kitsune sage'/'CHK', 'Rare').
card_artist('autumn-tail, kitsune sage'/'CHK', 'Jim Murray').
card_number('autumn-tail, kitsune sage'/'CHK', '28b').
card_multiverse_id('autumn-tail, kitsune sage'/'CHK', '78695').

card_in_set('azami, lady of scrolls', 'CHK').
card_original_type('azami, lady of scrolls'/'CHK', 'Legendary Creature — Human Wizard').
card_original_text('azami, lady of scrolls'/'CHK', 'Tap an untapped Wizard you control: Draw a card.').
card_first_print('azami, lady of scrolls', 'CHK').
card_image_name('azami, lady of scrolls'/'CHK', 'azami, lady of scrolls').
card_uid('azami, lady of scrolls'/'CHK', 'CHK:Azami, Lady of Scrolls:azami, lady of scrolls').
card_rarity('azami, lady of scrolls'/'CHK', 'Rare').
card_artist('azami, lady of scrolls'/'CHK', 'Ittoku').
card_number('azami, lady of scrolls'/'CHK', '52').
card_flavor_text('azami, lady of scrolls'/'CHK', '\"Choices belong to those with the luxuries of time and distance. We have neither. I recommend we proceed with the plan to destroy all shrines of the kami.\"\n—Lady Azami, letter to Sensei Hisoka').
card_multiverse_id('azami, lady of scrolls'/'CHK', '78697').

card_in_set('azusa, lost but seeking', 'CHK').
card_original_type('azusa, lost but seeking'/'CHK', 'Legendary Creature — Human Monk').
card_original_text('azusa, lost but seeking'/'CHK', 'You may play two additional lands on each of your turns.').
card_first_print('azusa, lost but seeking', 'CHK').
card_image_name('azusa, lost but seeking'/'CHK', 'azusa, lost but seeking').
card_uid('azusa, lost but seeking'/'CHK', 'CHK:Azusa, Lost but Seeking:azusa, lost but seeking').
card_rarity('azusa, lost but seeking'/'CHK', 'Rare').
card_artist('azusa, lost but seeking'/'CHK', 'Todd Lockwood').
card_number('azusa, lost but seeking'/'CHK', '201').
card_flavor_text('azusa, lost but seeking'/'CHK', '\"I do not miss Jukai Forest. It is not my home. My home is Kamigawa, its people my family. Wherever I set my pack and rest my head, I am home.\"').
card_multiverse_id('azusa, lost but seeking'/'CHK', '80283').

card_in_set('battle-mad ronin', 'CHK').
card_original_type('battle-mad ronin'/'CHK', 'Creature — Human Samurai').
card_original_text('battle-mad ronin'/'CHK', 'Bushido 2 (When this blocks or becomes blocked, it gets +2/+2 until end of turn.)\nBattle-Mad Ronin attacks each turn if able.').
card_first_print('battle-mad ronin', 'CHK').
card_image_name('battle-mad ronin'/'CHK', 'battle-mad ronin').
card_uid('battle-mad ronin'/'CHK', 'CHK:Battle-Mad Ronin:battle-mad ronin').
card_rarity('battle-mad ronin'/'CHK', 'Common').
card_artist('battle-mad ronin'/'CHK', 'Wayne England').
card_number('battle-mad ronin'/'CHK', '156').
card_flavor_text('battle-mad ronin'/'CHK', '\"I fought fiercely, bravely, and without mercy. Had I not struck down my captain in the heat of battle, I might have become a hero instead of an outcast.\"').
card_multiverse_id('battle-mad ronin'/'CHK', '50383').

card_in_set('befoul', 'CHK').
card_original_type('befoul'/'CHK', 'Sorcery').
card_original_text('befoul'/'CHK', 'Destroy target land or nonblack creature. It can\'t be regenerated.').
card_image_name('befoul'/'CHK', 'befoul').
card_uid('befoul'/'CHK', 'CHK:Befoul:befoul').
card_rarity('befoul'/'CHK', 'Common').
card_artist('befoul'/'CHK', 'Luca Zontini').
card_number('befoul'/'CHK', '102').
card_flavor_text('befoul'/'CHK', '\"When the rampaging kami at Reito had crushed the opposing militia, swarms of minor kami swept over the battlefield to consume all that remained.\"\n—Great Battles of Kamigawa').
card_multiverse_id('befoul'/'CHK', '50291').

card_in_set('ben-ben, akki hermit', 'CHK').
card_original_type('ben-ben, akki hermit'/'CHK', 'Legendary Creature — Goblin Shaman').
card_original_text('ben-ben, akki hermit'/'CHK', '{T}: Ben-Ben, Akki Hermit deals damage to target attacking creature equal to the number of untapped Mountains you control.').
card_first_print('ben-ben, akki hermit', 'CHK').
card_image_name('ben-ben, akki hermit'/'CHK', 'ben-ben, akki hermit').
card_uid('ben-ben, akki hermit'/'CHK', 'CHK:Ben-Ben, Akki Hermit:ben-ben, akki hermit').
card_rarity('ben-ben, akki hermit'/'CHK', 'Rare').
card_artist('ben-ben, akki hermit'/'CHK', 'Greg Staples').
card_number('ben-ben, akki hermit'/'CHK', '157').
card_flavor_text('ben-ben, akki hermit'/'CHK', 'Some akki thought of Ben-Ben as a kami of trickery in disguise. They hunted for him in the maze of warrens, often falling prey to his traps.').
card_multiverse_id('ben-ben, akki hermit'/'CHK', '79248').

card_in_set('blessed breath', 'CHK').
card_original_type('blessed breath'/'CHK', 'Instant — Arcane').
card_original_text('blessed breath'/'CHK', 'Target creature you control gains protection from the color of your choice until end of turn.\nSplice onto Arcane {W} (As you play an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_first_print('blessed breath', 'CHK').
card_image_name('blessed breath'/'CHK', 'blessed breath').
card_uid('blessed breath'/'CHK', 'CHK:Blessed Breath:blessed breath').
card_rarity('blessed breath'/'CHK', 'Common').
card_artist('blessed breath'/'CHK', 'Tsutomu Kawade').
card_number('blessed breath'/'CHK', '1').
card_multiverse_id('blessed breath'/'CHK', '50451').

card_in_set('blind with anger', 'CHK').
card_original_type('blind with anger'/'CHK', 'Instant — Arcane').
card_original_text('blind with anger'/'CHK', 'Untap target nonlegendary creature and gain control of it until end of turn. That creature gains haste until end of turn.').
card_first_print('blind with anger', 'CHK').
card_image_name('blind with anger'/'CHK', 'blind with anger').
card_uid('blind with anger'/'CHK', 'CHK:Blind with Anger:blind with anger').
card_rarity('blind with anger'/'CHK', 'Uncommon').
card_artist('blind with anger'/'CHK', 'Dave Dorman').
card_number('blind with anger'/'CHK', '158').
card_flavor_text('blind with anger'/'CHK', '\"I can post only the most honorable and strong-willed samurai to defend your stronghold. I have seen what can happen to lesser men.\"\n—General Takeno, letter to Lord Konda').
card_multiverse_id('blind with anger'/'CHK', '77925').

card_in_set('blood rites', 'CHK').
card_original_type('blood rites'/'CHK', 'Enchantment').
card_original_text('blood rites'/'CHK', '{1}{R}, Sacrifice a creature: Blood Rites deals 2 damage to target creature or player.').
card_first_print('blood rites', 'CHK').
card_image_name('blood rites'/'CHK', 'blood rites').
card_uid('blood rites'/'CHK', 'CHK:Blood Rites:blood rites').
card_rarity('blood rites'/'CHK', 'Uncommon').
card_artist('blood rites'/'CHK', 'Paolo Parente').
card_number('blood rites'/'CHK', '159').
card_flavor_text('blood rites'/'CHK', '\"The threat of the kami was made worse by the blood rituals of the ogres, who freed terrifying oni to wander Kamigawa unhindered.\"\n—Observations of the Kami War').
card_multiverse_id('blood rites'/'CHK', '50509').

card_in_set('blood speaker', 'CHK').
card_original_type('blood speaker'/'CHK', 'Creature — Ogre Shaman').
card_original_text('blood speaker'/'CHK', 'At the beginning of your upkeep, you may sacrifice Blood Speaker. If you do, search your library for a Demon card, reveal that card, and put it into your hand. Then shuffle your library.\nWhenever a Demon comes into play under your control, return Blood Speaker from your graveyard to your hand.').
card_first_print('blood speaker', 'CHK').
card_image_name('blood speaker'/'CHK', 'blood speaker').
card_uid('blood speaker'/'CHK', 'CHK:Blood Speaker:blood speaker').
card_rarity('blood speaker'/'CHK', 'Uncommon').
card_artist('blood speaker'/'CHK', 'Adam Rex').
card_number('blood speaker'/'CHK', '103').
card_multiverse_id('blood speaker'/'CHK', '79166').

card_in_set('bloodthirsty ogre', 'CHK').
card_original_type('bloodthirsty ogre'/'CHK', 'Creature — Ogre Warrior Shaman').
card_original_text('bloodthirsty ogre'/'CHK', '{T}: Put a devotion counter on Bloodthirsty Ogre.\n{T}: Target creature gets -X/-X until end of turn, where X is the number of devotion counters on Bloodthirsty Ogre. Play this ability only if you control a Demon.').
card_first_print('bloodthirsty ogre', 'CHK').
card_image_name('bloodthirsty ogre'/'CHK', 'bloodthirsty ogre').
card_uid('bloodthirsty ogre'/'CHK', 'CHK:Bloodthirsty Ogre:bloodthirsty ogre').
card_rarity('bloodthirsty ogre'/'CHK', 'Uncommon').
card_artist('bloodthirsty ogre'/'CHK', 'Thomas M. Baxa').
card_number('bloodthirsty ogre'/'CHK', '104').
card_multiverse_id('bloodthirsty ogre'/'CHK', '79113').

card_in_set('boseiju, who shelters all', 'CHK').
card_original_type('boseiju, who shelters all'/'CHK', 'Legendary Land').
card_original_text('boseiju, who shelters all'/'CHK', 'Boseiju, Who Shelters All comes into play tapped.\n{T}, Pay 2 life: Add {1} to your mana pool. If that mana is spent on an instant or sorcery spell, that spell can\'t be countered by spells or abilities.').
card_first_print('boseiju, who shelters all', 'CHK').
card_image_name('boseiju, who shelters all'/'CHK', 'boseiju, who shelters all').
card_uid('boseiju, who shelters all'/'CHK', 'CHK:Boseiju, Who Shelters All:boseiju, who shelters all').
card_rarity('boseiju, who shelters all'/'CHK', 'Rare').
card_artist('boseiju, who shelters all'/'CHK', 'Ralph Horsley').
card_number('boseiju, who shelters all'/'CHK', '273').
card_multiverse_id('boseiju, who shelters all'/'CHK', '75305').

card_in_set('brothers yamazaki', 'CHK').
card_original_type('brothers yamazaki'/'CHK', 'Legendary Creature — Human Samurai').
card_original_text('brothers yamazaki'/'CHK', 'Bushido 1 (When this blocks or becomes blocked, it gets +1/+1 until end of turn.)\nIf there are exactly two permanents named Brothers Yamazaki in play, the \"legend rule\" doesn\'t apply to them.\nEach other creature named Brothers Yamazaki gets +2/+2 and has haste.').
card_first_print('brothers yamazaki', 'CHK').
card_image_name('brothers yamazaki'/'CHK', 'brothers yamazaki1').
card_uid('brothers yamazaki'/'CHK', 'CHK:Brothers Yamazaki:brothers yamazaki1').
card_rarity('brothers yamazaki'/'CHK', 'Uncommon').
card_artist('brothers yamazaki'/'CHK', 'Ron Spears').
card_number('brothers yamazaki'/'CHK', '160a').
card_multiverse_id('brothers yamazaki'/'CHK', '78968').

card_in_set('brothers yamazaki', 'CHK').
card_original_type('brothers yamazaki'/'CHK', 'Legendary Creature — Human Samurai').
card_original_text('brothers yamazaki'/'CHK', 'Bushido 1 (When this blocks or becomes blocked, it gets +1/+1 until end of turn.)\nIf there are exactly two permanents named Brothers Yamazaki in play, the \"legend rule\" doesn\'t apply to them.\nEach other creature named Brothers Yamazaki gets +2/+2 and has haste.').
card_image_name('brothers yamazaki'/'CHK', 'brothers yamazaki2').
card_uid('brothers yamazaki'/'CHK', 'CHK:Brothers Yamazaki:brothers yamazaki2').
card_rarity('brothers yamazaki'/'CHK', 'Uncommon').
card_artist('brothers yamazaki'/'CHK', 'Ron Spears').
card_number('brothers yamazaki'/'CHK', '160b').
card_multiverse_id('brothers yamazaki'/'CHK', '85106').

card_in_set('brutal deceiver', 'CHK').
card_original_type('brutal deceiver'/'CHK', 'Creature — Spirit').
card_original_text('brutal deceiver'/'CHK', '{1}: Look at the top card of your library.\n{2}: Reveal the top card of your library. If it\'s a land, Brutal Deceiver gets +1/+0 and gains first strike until end of turn. Play this ability only once each turn.').
card_first_print('brutal deceiver', 'CHK').
card_image_name('brutal deceiver'/'CHK', 'brutal deceiver').
card_uid('brutal deceiver'/'CHK', 'CHK:Brutal Deceiver:brutal deceiver').
card_rarity('brutal deceiver'/'CHK', 'Common').
card_artist('brutal deceiver'/'CHK', 'Jon Foster').
card_number('brutal deceiver'/'CHK', '161').
card_multiverse_id('brutal deceiver'/'CHK', '76638').

card_in_set('budoka gardener', 'CHK').
card_original_type('budoka gardener'/'CHK', 'Creature — Human Monk').
card_original_text('budoka gardener'/'CHK', '{T}: You may put a land card from your hand into play. If you control ten or more lands, flip Budoka Gardener.\n-----\nDokai, Weaver of Life\nLegendary Creature Human Monk\n3/3\n{4}{G}{G}, {T}: Put an X/X green Elemental creature token into play, where X is the number of lands you control.').
card_first_print('budoka gardener', 'CHK').
card_image_name('budoka gardener'/'CHK', 'budoka gardener').
card_uid('budoka gardener'/'CHK', 'CHK:Budoka Gardener:budoka gardener').
card_rarity('budoka gardener'/'CHK', 'Rare').
card_artist('budoka gardener'/'CHK', 'Kev Walker').
card_number('budoka gardener'/'CHK', '202a').
card_multiverse_id('budoka gardener'/'CHK', '78687').

card_in_set('burr grafter', 'CHK').
card_original_type('burr grafter'/'CHK', 'Creature — Spirit').
card_original_text('burr grafter'/'CHK', 'Sacrifice Burr Grafter: Target creature gets +2/+2 until end of turn.\nSoulshift 3 (When this is put into a graveyard from play, you may return target Spirit card with converted mana cost 3 or less from your graveyard to your hand.)').
card_first_print('burr grafter', 'CHK').
card_image_name('burr grafter'/'CHK', 'burr grafter').
card_uid('burr grafter'/'CHK', 'CHK:Burr Grafter:burr grafter').
card_rarity('burr grafter'/'CHK', 'Common').
card_artist('burr grafter'/'CHK', 'Heather Hudson').
card_number('burr grafter'/'CHK', '203').
card_multiverse_id('burr grafter'/'CHK', '50305').

card_in_set('bushi tenderfoot', 'CHK').
card_original_type('bushi tenderfoot'/'CHK', 'Creature — Human Soldier').
card_original_text('bushi tenderfoot'/'CHK', 'When a creature dealt damage by Bushi Tenderfoot this turn is put into a graveyard, flip Bushi Tenderfoot.\n-----\nKenzo the Hardhearted\nLegendary Creature Human Samurai\n3/4\nDouble strike; bushido 2 (When this blocks or becomes blocked, it gets +2/+2 until end of turn.)').
card_first_print('bushi tenderfoot', 'CHK').
card_image_name('bushi tenderfoot'/'CHK', 'bushi tenderfoot').
card_uid('bushi tenderfoot'/'CHK', 'CHK:Bushi Tenderfoot:bushi tenderfoot').
card_rarity('bushi tenderfoot'/'CHK', 'Uncommon').
card_artist('bushi tenderfoot'/'CHK', 'Mark Zug').
card_number('bushi tenderfoot'/'CHK', '2a').
card_multiverse_id('bushi tenderfoot'/'CHK', '78600').

card_in_set('cage of hands', 'CHK').
card_original_type('cage of hands'/'CHK', 'Enchant Creature').
card_original_text('cage of hands'/'CHK', 'Enchanted creature can\'t attack or block.\n{1}{W}: Return Cage of Hands to its owner\'s hand.').
card_first_print('cage of hands', 'CHK').
card_image_name('cage of hands'/'CHK', 'cage of hands').
card_uid('cage of hands'/'CHK', 'CHK:Cage of Hands:cage of hands').
card_rarity('cage of hands'/'CHK', 'Common').
card_artist('cage of hands'/'CHK', 'Mark Tedin').
card_number('cage of hands'/'CHK', '3').
card_flavor_text('cage of hands'/'CHK', '\"Our own actions built the prisons that now hold us. Our hands reached too far and tried to hold too much.\"\n—Dosan the Falling Leaf').
card_multiverse_id('cage of hands'/'CHK', '50233').

card_in_set('call to glory', 'CHK').
card_original_type('call to glory'/'CHK', 'Instant').
card_original_text('call to glory'/'CHK', 'Untap all creatures you control. Samurai you control get +1/+1 until end of turn.').
card_first_print('call to glory', 'CHK').
card_image_name('call to glory'/'CHK', 'call to glory').
card_uid('call to glory'/'CHK', 'CHK:Call to Glory:call to glory').
card_rarity('call to glory'/'CHK', 'Common').
card_artist('call to glory'/'CHK', 'Wayne Reynolds').
card_number('call to glory'/'CHK', '4').
card_flavor_text('call to glory'/'CHK', '\"General Takeno glared at us as if we were the enemy. ‘The day is not over yet,\' he shouted, ‘and unless you have a nezumi\'s heart, you will stand and fight\'\"\n—Battle of Akagi River: A Survivor\'s Tale').
card_multiverse_id('call to glory'/'CHK', '77140').

card_in_set('callous deceiver', 'CHK').
card_original_type('callous deceiver'/'CHK', 'Creature — Spirit').
card_original_text('callous deceiver'/'CHK', '{1}: Look at the top card of your library.\n{2}: Reveal the top card of your library. If it\'s a land, Callous Deceiver gets +1/+0 and gains flying until end of turn. Play this ability only once each turn.').
card_first_print('callous deceiver', 'CHK').
card_image_name('callous deceiver'/'CHK', 'callous deceiver').
card_uid('callous deceiver'/'CHK', 'CHK:Callous Deceiver:callous deceiver').
card_rarity('callous deceiver'/'CHK', 'Common').
card_artist('callous deceiver'/'CHK', 'Kensuke Okabayashi').
card_number('callous deceiver'/'CHK', '53').
card_multiverse_id('callous deceiver'/'CHK', '78190').

card_in_set('candles\' glow', 'CHK').
card_original_type('candles\' glow'/'CHK', 'Instant — Arcane').
card_original_text('candles\' glow'/'CHK', 'Prevent the next 3 damage that would be dealt to target creature or player this turn. You gain 1 life for each damage prevented this way.\nSplice onto Arcane {1}{W} (As you play an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_first_print('candles\' glow', 'CHK').
card_image_name('candles\' glow'/'CHK', 'candles\' glow').
card_uid('candles\' glow'/'CHK', 'CHK:Candles\' Glow:candles\' glow').
card_rarity('candles\' glow'/'CHK', 'Uncommon').
card_artist('candles\' glow'/'CHK', 'Alan Pollack').
card_number('candles\' glow'/'CHK', '5').
card_multiverse_id('candles\' glow'/'CHK', '80292').

card_in_set('cleanfall', 'CHK').
card_original_type('cleanfall'/'CHK', 'Sorcery — Arcane').
card_original_text('cleanfall'/'CHK', 'Destroy all enchantments.').
card_first_print('cleanfall', 'CHK').
card_image_name('cleanfall'/'CHK', 'cleanfall').
card_uid('cleanfall'/'CHK', 'CHK:Cleanfall:cleanfall').
card_rarity('cleanfall'/'CHK', 'Uncommon').
card_artist('cleanfall'/'CHK', 'Daren Bader').
card_number('cleanfall'/'CHK', '6').
card_flavor_text('cleanfall'/'CHK', 'During the war, the rites of purification were still effective, but the kami, not the kitsune, chose what to cleanse.').
card_multiverse_id('cleanfall'/'CHK', '80500').

card_in_set('cloudcrest lake', 'CHK').
card_original_type('cloudcrest lake'/'CHK', 'Land').
card_original_text('cloudcrest lake'/'CHK', '{T}: Add {1} to your mana pool.\n{T}: Add {W} or {U} to your mana pool. Cloudcrest Lake doesn\'t untap during your next untap step.').
card_first_print('cloudcrest lake', 'CHK').
card_image_name('cloudcrest lake'/'CHK', 'cloudcrest lake').
card_uid('cloudcrest lake'/'CHK', 'CHK:Cloudcrest Lake:cloudcrest lake').
card_rarity('cloudcrest lake'/'CHK', 'Uncommon').
card_artist('cloudcrest lake'/'CHK', 'John Avon').
card_number('cloudcrest lake'/'CHK', '274').
card_multiverse_id('cloudcrest lake'/'CHK', '79115').

card_in_set('commune with nature', 'CHK').
card_original_type('commune with nature'/'CHK', 'Sorcery').
card_original_text('commune with nature'/'CHK', 'Look at the top five cards of your library. You may reveal a creature card from among them and put it into your hand. Put the rest on the bottom of your library in any order.').
card_first_print('commune with nature', 'CHK').
card_image_name('commune with nature'/'CHK', 'commune with nature').
card_uid('commune with nature'/'CHK', 'CHK:Commune with Nature:commune with nature').
card_rarity('commune with nature'/'CHK', 'Common').
card_artist('commune with nature'/'CHK', 'Edward P. Beard, Jr.').
card_number('commune with nature'/'CHK', '204').
card_multiverse_id('commune with nature'/'CHK', '76639').

card_in_set('consuming vortex', 'CHK').
card_original_type('consuming vortex'/'CHK', 'Instant — Arcane').
card_original_text('consuming vortex'/'CHK', 'Return target creature to its owner\'s hand.\nSplice onto Arcane {3}{U} (As you play an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_first_print('consuming vortex', 'CHK').
card_image_name('consuming vortex'/'CHK', 'consuming vortex').
card_uid('consuming vortex'/'CHK', 'CHK:Consuming Vortex:consuming vortex').
card_rarity('consuming vortex'/'CHK', 'Common').
card_artist('consuming vortex'/'CHK', 'Pete Venters').
card_number('consuming vortex'/'CHK', '54').
card_multiverse_id('consuming vortex'/'CHK', '80255').

card_in_set('counsel of the soratami', 'CHK').
card_original_type('counsel of the soratami'/'CHK', 'Sorcery').
card_original_text('counsel of the soratami'/'CHK', 'Draw two cards.').
card_first_print('counsel of the soratami', 'CHK').
card_image_name('counsel of the soratami'/'CHK', 'counsel of the soratami').
card_uid('counsel of the soratami'/'CHK', 'CHK:Counsel of the Soratami:counsel of the soratami').
card_rarity('counsel of the soratami'/'CHK', 'Common').
card_artist('counsel of the soratami'/'CHK', 'Randy Gallegos').
card_number('counsel of the soratami'/'CHK', '55').
card_flavor_text('counsel of the soratami'/'CHK', '\"Wisdom is not the counting of all the drops in a waterfall. Wisdom is learning why the water seeks the earth.\"').
card_multiverse_id('counsel of the soratami'/'CHK', '50219').

card_in_set('cranial extraction', 'CHK').
card_original_type('cranial extraction'/'CHK', 'Sorcery — Arcane').
card_original_text('cranial extraction'/'CHK', 'Name a nonland card. Search target player\'s graveyard, hand, and library for all cards with that name and remove them from the game. Then that player shuffles his or her library.').
card_first_print('cranial extraction', 'CHK').
card_image_name('cranial extraction'/'CHK', 'cranial extraction').
card_uid('cranial extraction'/'CHK', 'CHK:Cranial Extraction:cranial extraction').
card_rarity('cranial extraction'/'CHK', 'Rare').
card_artist('cranial extraction'/'CHK', 'Dave Allsop').
card_number('cranial extraction'/'CHK', '105').
card_multiverse_id('cranial extraction'/'CHK', '80281').

card_in_set('cruel deceiver', 'CHK').
card_original_type('cruel deceiver'/'CHK', 'Creature — Spirit').
card_original_text('cruel deceiver'/'CHK', '{1}: Look at the top card of your library.\n{2}: Reveal the top card of your library. If it\'s a land, Cruel Deceiver gains \"Whenever Cruel Deceiver deals damage to a creature, destroy that creature\" until end of turn. Play this ability only once each turn.').
card_first_print('cruel deceiver', 'CHK').
card_image_name('cruel deceiver'/'CHK', 'cruel deceiver').
card_uid('cruel deceiver'/'CHK', 'CHK:Cruel Deceiver:cruel deceiver').
card_rarity('cruel deceiver'/'CHK', 'Common').
card_artist('cruel deceiver'/'CHK', 'Nottsuo').
card_number('cruel deceiver'/'CHK', '106').
card_multiverse_id('cruel deceiver'/'CHK', '75258').

card_in_set('crushing pain', 'CHK').
card_original_type('crushing pain'/'CHK', 'Instant — Arcane').
card_original_text('crushing pain'/'CHK', 'Crushing Pain deals 6 damage to target creature that was dealt damage this turn.').
card_first_print('crushing pain', 'CHK').
card_image_name('crushing pain'/'CHK', 'crushing pain').
card_uid('crushing pain'/'CHK', 'CHK:Crushing Pain:crushing pain').
card_rarity('crushing pain'/'CHK', 'Common').
card_artist('crushing pain'/'CHK', 'Carl Critchlow').
card_number('crushing pain'/'CHK', '162').
card_flavor_text('crushing pain'/'CHK', 'It is said that the mere touch of a kami is like embracing the sun itself.').
card_multiverse_id('crushing pain'/'CHK', '50514').

card_in_set('cursed ronin', 'CHK').
card_original_type('cursed ronin'/'CHK', 'Creature — Human Samurai').
card_original_text('cursed ronin'/'CHK', 'Bushido 1 (When this blocks or becomes blocked, it gets +1/+1 until end of turn.)\n{B}: Cursed Ronin gets +1/+1 until end of turn.').
card_first_print('cursed ronin', 'CHK').
card_image_name('cursed ronin'/'CHK', 'cursed ronin').
card_uid('cursed ronin'/'CHK', 'CHK:Cursed Ronin:cursed ronin').
card_rarity('cursed ronin'/'CHK', 'Common').
card_artist('cursed ronin'/'CHK', 'Carl Critchlow').
card_number('cursed ronin'/'CHK', '107').
card_flavor_text('cursed ronin'/'CHK', '\"You are fortunate, my enemy. You have paid the price but once. I never stop paying.\"').
card_multiverse_id('cursed ronin'/'CHK', '75262').

card_in_set('cut the tethers', 'CHK').
card_original_type('cut the tethers'/'CHK', 'Sorcery').
card_original_text('cut the tethers'/'CHK', 'For each Spirit, return it to its owner\'s hand unless that player pays {3}.').
card_first_print('cut the tethers', 'CHK').
card_image_name('cut the tethers'/'CHK', 'cut the tethers').
card_uid('cut the tethers'/'CHK', 'CHK:Cut the Tethers:cut the tethers').
card_rarity('cut the tethers'/'CHK', 'Uncommon').
card_artist('cut the tethers'/'CHK', 'Ron Spears').
card_number('cut the tethers'/'CHK', '56').
card_flavor_text('cut the tethers'/'CHK', '\"You cannot bar the path of gods. You can only divert their journey for a while.\"\n—Sensei Hisoka').
card_multiverse_id('cut the tethers'/'CHK', '79229').

card_in_set('dampen thought', 'CHK').
card_original_type('dampen thought'/'CHK', 'Instant — Arcane').
card_original_text('dampen thought'/'CHK', 'Target player puts the top four cards of his or her library into his or her graveyard.\nSplice onto Arcane {1}{U} (As you play an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_first_print('dampen thought', 'CHK').
card_image_name('dampen thought'/'CHK', 'dampen thought').
card_uid('dampen thought'/'CHK', 'CHK:Dampen Thought:dampen thought').
card_rarity('dampen thought'/'CHK', 'Uncommon').
card_artist('dampen thought'/'CHK', 'Arnie Swekel').
card_number('dampen thought'/'CHK', '57').
card_multiverse_id('dampen thought'/'CHK', '80284').

card_in_set('dance of shadows', 'CHK').
card_original_type('dance of shadows'/'CHK', 'Sorcery — Arcane').
card_original_text('dance of shadows'/'CHK', 'Creatures you control get +1/+0 and gain fear until end of turn.').
card_first_print('dance of shadows', 'CHK').
card_image_name('dance of shadows'/'CHK', 'dance of shadows').
card_uid('dance of shadows'/'CHK', 'CHK:Dance of Shadows:dance of shadows').
card_rarity('dance of shadows'/'CHK', 'Uncommon').
card_artist('dance of shadows'/'CHK', 'Chippy').
card_number('dance of shadows'/'CHK', '108').
card_flavor_text('dance of shadows'/'CHK', '\"Only one man survived, barely sane. He got out two words before collapsing: shadows\' shadows.\"').
card_multiverse_id('dance of shadows'/'CHK', '79094').

card_in_set('deathcurse ogre', 'CHK').
card_original_type('deathcurse ogre'/'CHK', 'Creature — Ogre Warrior').
card_original_text('deathcurse ogre'/'CHK', 'When Deathcurse Ogre is put into a graveyard from play, each player loses 3 life.').
card_first_print('deathcurse ogre', 'CHK').
card_image_name('deathcurse ogre'/'CHK', 'deathcurse ogre').
card_uid('deathcurse ogre'/'CHK', 'CHK:Deathcurse Ogre:deathcurse ogre').
card_rarity('deathcurse ogre'/'CHK', 'Common').
card_artist('deathcurse ogre'/'CHK', 'Mark Tedin').
card_number('deathcurse ogre'/'CHK', '109').
card_flavor_text('deathcurse ogre'/'CHK', 'After their worship of oni began, only a few of Kamigawa\'s ogres remained in the bitter cold of the Tendo Peaks. Most were drawn to the darkness of Takenuma.').
card_multiverse_id('deathcurse ogre'/'CHK', '80522').

card_in_set('desperate ritual', 'CHK').
card_original_type('desperate ritual'/'CHK', 'Instant — Arcane').
card_original_text('desperate ritual'/'CHK', 'Add {R}{R}{R} to your mana pool.\nSplice onto Arcane {1}{R} (As you play an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_first_print('desperate ritual', 'CHK').
card_image_name('desperate ritual'/'CHK', 'desperate ritual').
card_uid('desperate ritual'/'CHK', 'CHK:Desperate Ritual:desperate ritual').
card_rarity('desperate ritual'/'CHK', 'Common').
card_artist('desperate ritual'/'CHK', 'Darrell Riche').
card_number('desperate ritual'/'CHK', '163').
card_multiverse_id('desperate ritual'/'CHK', '80275').

card_in_set('devoted retainer', 'CHK').
card_original_type('devoted retainer'/'CHK', 'Creature — Human Samurai').
card_original_text('devoted retainer'/'CHK', 'Bushido 1 (When this blocks or becomes blocked, it gets +1/+1 until end of turn.)').
card_first_print('devoted retainer', 'CHK').
card_image_name('devoted retainer'/'CHK', 'devoted retainer').
card_uid('devoted retainer'/'CHK', 'CHK:Devoted Retainer:devoted retainer').
card_rarity('devoted retainer'/'CHK', 'Common').
card_artist('devoted retainer'/'CHK', 'Greg Hildebrandt').
card_number('devoted retainer'/'CHK', '7').
card_flavor_text('devoted retainer'/'CHK', 'Deep within Eiganjo Castle lay the Palace of Infinite Halls, a seemingly endless network of corridors once guarded by a seemingly endless legion of samurai.').
card_multiverse_id('devoted retainer'/'CHK', '50440').

card_in_set('devouring greed', 'CHK').
card_original_type('devouring greed'/'CHK', 'Sorcery — Arcane').
card_original_text('devouring greed'/'CHK', 'As an additional cost to play Devouring Greed, you may sacrifice any number of Spirits.\nTarget player loses 2 life plus 2 life for each Spirit sacrificed this way. You gain that much life.').
card_first_print('devouring greed', 'CHK').
card_image_name('devouring greed'/'CHK', 'devouring greed').
card_uid('devouring greed'/'CHK', 'CHK:Devouring Greed:devouring greed').
card_rarity('devouring greed'/'CHK', 'Common').
card_artist('devouring greed'/'CHK', 'Vance Kovacs').
card_number('devouring greed'/'CHK', '110').
card_multiverse_id('devouring greed'/'CHK', '79151').

card_in_set('devouring rage', 'CHK').
card_original_type('devouring rage'/'CHK', 'Instant — Arcane').
card_original_text('devouring rage'/'CHK', 'As an additional cost to play Devouring Rage, you may sacrifice any number of Spirits.\nTarget creature gets +3/+0 until end of turn. For each Spirit sacrificed this way, that creature gets an additional +3/+0 until end of turn.').
card_first_print('devouring rage', 'CHK').
card_image_name('devouring rage'/'CHK', 'devouring rage').
card_uid('devouring rage'/'CHK', 'CHK:Devouring Rage:devouring rage').
card_rarity('devouring rage'/'CHK', 'Common').
card_artist('devouring rage'/'CHK', 'Vance Kovacs').
card_number('devouring rage'/'CHK', '164').
card_multiverse_id('devouring rage'/'CHK', '75401').

card_in_set('distress', 'CHK').
card_original_type('distress'/'CHK', 'Sorcery').
card_original_text('distress'/'CHK', 'Target player reveals his or her hand. You choose a nonland card from it. That player discards that card.').
card_first_print('distress', 'CHK').
card_image_name('distress'/'CHK', 'distress').
card_uid('distress'/'CHK', 'CHK:Distress:distress').
card_rarity('distress'/'CHK', 'Common').
card_artist('distress'/'CHK', 'Michael Sutfin').
card_number('distress'/'CHK', '111').
card_flavor_text('distress'/'CHK', '\"Today I asked Master Dosan what the ogre mages did with the humans they sacrificed. He gave me a hard look and said to think no more on the matter.\"\n—Meditation journal of young budoka').
card_multiverse_id('distress'/'CHK', '79129').

card_in_set('dokai, weaver of life', 'CHK').
card_original_type('dokai, weaver of life'/'CHK', 'Creature — Human Monk').
card_original_text('dokai, weaver of life'/'CHK', '{T}: You may put a land card from your hand into play. If you control ten or more lands, flip Budoka Gardener.\n-----\nDokai, Weaver of Life\nLegendary Creature Human Monk\n3/3\n{4}{G}{G}, {T}: Put an X/X green Elemental creature token into play, where X is the number of lands you control.').
card_first_print('dokai, weaver of life', 'CHK').
card_image_name('dokai, weaver of life'/'CHK', 'dokai, weaver of life').
card_uid('dokai, weaver of life'/'CHK', 'CHK:Dokai, Weaver of Life:dokai, weaver of life').
card_rarity('dokai, weaver of life'/'CHK', 'Rare').
card_artist('dokai, weaver of life'/'CHK', 'Kev Walker').
card_number('dokai, weaver of life'/'CHK', '202b').
card_multiverse_id('dokai, weaver of life'/'CHK', '78687').

card_in_set('dosan the falling leaf', 'CHK').
card_original_type('dosan the falling leaf'/'CHK', 'Legendary Creature — Human Monk').
card_original_text('dosan the falling leaf'/'CHK', 'Players can play spells only during their own turns.').
card_first_print('dosan the falling leaf', 'CHK').
card_image_name('dosan the falling leaf'/'CHK', 'dosan the falling leaf').
card_uid('dosan the falling leaf'/'CHK', 'CHK:Dosan the Falling Leaf:dosan the falling leaf').
card_rarity('dosan the falling leaf'/'CHK', 'Rare').
card_artist('dosan the falling leaf'/'CHK', 'Mark Zug').
card_number('dosan the falling leaf'/'CHK', '205').
card_flavor_text('dosan the falling leaf'/'CHK', '\"Each night as Master Dosan prays to the kami, the hate he receives in return withers his body a little more. Though the kami are slowly killing him, still he continues his prayers.\"\n—Meditation journal of a young budoka').
card_multiverse_id('dosan the falling leaf'/'CHK', '80524').

card_in_set('dripping-tongue zubera', 'CHK').
card_original_type('dripping-tongue zubera'/'CHK', 'Creature — Zubera Spirit').
card_original_text('dripping-tongue zubera'/'CHK', 'When Dripping-Tongue Zubera is put into a graveyard from play, put a 1/1 colorless Spirit creature token into play for each Zubera put into a graveyard from play this turn.').
card_first_print('dripping-tongue zubera', 'CHK').
card_image_name('dripping-tongue zubera'/'CHK', 'dripping-tongue zubera').
card_uid('dripping-tongue zubera'/'CHK', 'CHK:Dripping-Tongue Zubera:dripping-tongue zubera').
card_rarity('dripping-tongue zubera'/'CHK', 'Common').
card_artist('dripping-tongue zubera'/'CHK', 'Tsutomu Kawade').
card_number('dripping-tongue zubera'/'CHK', '206').
card_flavor_text('dripping-tongue zubera'/'CHK', 'When the Honden of Life\'s Web was destroyed, its attendants swarmed Kamigawa to ensure mortal defeat.').
card_multiverse_id('dripping-tongue zubera'/'CHK', '80511').

card_in_set('earthshaker', 'CHK').
card_original_type('earthshaker'/'CHK', 'Creature — Spirit').
card_original_text('earthshaker'/'CHK', 'Whenever you play a Spirit or Arcane spell, Earthshaker deals 2 damage to each creature without flying.').
card_first_print('earthshaker', 'CHK').
card_image_name('earthshaker'/'CHK', 'earthshaker').
card_uid('earthshaker'/'CHK', 'CHK:Earthshaker:earthshaker').
card_rarity('earthshaker'/'CHK', 'Uncommon').
card_artist('earthshaker'/'CHK', 'Ron Spencer').
card_number('earthshaker'/'CHK', '165').
card_flavor_text('earthshaker'/'CHK', 'It scaled the Sokenzan Mountains in search of Kumano\'s secret. The mountain shook for two days, and the kami never returned.').
card_multiverse_id('earthshaker'/'CHK', '79214').

card_in_set('eerie procession', 'CHK').
card_original_type('eerie procession'/'CHK', 'Sorcery — Arcane').
card_original_text('eerie procession'/'CHK', 'Search your library for an Arcane card, reveal that card, and put it into your hand. Then shuffle your library.').
card_first_print('eerie procession', 'CHK').
card_image_name('eerie procession'/'CHK', 'eerie procession').
card_uid('eerie procession'/'CHK', 'CHK:Eerie Procession:eerie procession').
card_rarity('eerie procession'/'CHK', 'Uncommon').
card_artist('eerie procession'/'CHK', 'Jim Murray').
card_number('eerie procession'/'CHK', '58').
card_flavor_text('eerie procession'/'CHK', '\"Though in years past speculation was not encouraged about the strange ways of kami, now we must understand their motivations, if such is even possible to the mortal mind.\"\n—Lady Azami').
card_multiverse_id('eerie procession'/'CHK', '80243').

card_in_set('eiganjo castle', 'CHK').
card_original_type('eiganjo castle'/'CHK', 'Legendary Land').
card_original_text('eiganjo castle'/'CHK', '{T}: Add {W} to your mana pool.\n{W}, {T}: Prevent the next 2 damage that would be dealt to target legendary creature this turn.').
card_first_print('eiganjo castle', 'CHK').
card_image_name('eiganjo castle'/'CHK', 'eiganjo castle').
card_uid('eiganjo castle'/'CHK', 'CHK:Eiganjo Castle:eiganjo castle').
card_rarity('eiganjo castle'/'CHK', 'Rare').
card_artist('eiganjo castle'/'CHK', 'Wayne England').
card_number('eiganjo castle'/'CHK', '275').
card_flavor_text('eiganjo castle'/'CHK', 'Since the war began, the castle\'s walls mark the only place on Kamigawa where no kami has ever set foot.').
card_multiverse_id('eiganjo castle'/'CHK', '79205').

card_in_set('eight-and-a-half-tails', 'CHK').
card_original_type('eight-and-a-half-tails'/'CHK', 'Legendary Creature — Fox Cleric').
card_original_text('eight-and-a-half-tails'/'CHK', '{1}{W}: Target permanent you control gains protection from white until end of turn.\n{1}: Target spell or permanent becomes white until end of turn.').
card_first_print('eight-and-a-half-tails', 'CHK').
card_image_name('eight-and-a-half-tails'/'CHK', 'eight-and-a-half-tails').
card_uid('eight-and-a-half-tails'/'CHK', 'CHK:Eight-and-a-Half-Tails:eight-and-a-half-tails').
card_rarity('eight-and-a-half-tails'/'CHK', 'Rare').
card_artist('eight-and-a-half-tails'/'CHK', 'Daren Bader').
card_number('eight-and-a-half-tails'/'CHK', '8').
card_flavor_text('eight-and-a-half-tails'/'CHK', '\"Virtue is an inner light that can prevail in every soul.\"').
card_multiverse_id('eight-and-a-half-tails'/'CHK', '50296').

card_in_set('ember-fist zubera', 'CHK').
card_original_type('ember-fist zubera'/'CHK', 'Creature — Zubera Spirit').
card_original_text('ember-fist zubera'/'CHK', 'When Ember-Fist Zubera is put into a graveyard from play, it deals damage to target creature or player equal to the number of Zubera put into all graveyards from play this turn.').
card_first_print('ember-fist zubera', 'CHK').
card_image_name('ember-fist zubera'/'CHK', 'ember-fist zubera').
card_uid('ember-fist zubera'/'CHK', 'CHK:Ember-Fist Zubera:ember-fist zubera').
card_rarity('ember-fist zubera'/'CHK', 'Common').
card_artist('ember-fist zubera'/'CHK', 'Ron Spencer').
card_number('ember-fist zubera'/'CHK', '166').
card_flavor_text('ember-fist zubera'/'CHK', 'When the Honden of Infinite Rage shattered, its attendants swarmed Kamigawa to sow mortal destruction.').
card_multiverse_id('ember-fist zubera'/'CHK', '80508').

card_in_set('ethereal haze', 'CHK').
card_original_type('ethereal haze'/'CHK', 'Instant — Arcane').
card_original_text('ethereal haze'/'CHK', 'Prevent all damage that would be dealt by creatures this turn.').
card_first_print('ethereal haze', 'CHK').
card_image_name('ethereal haze'/'CHK', 'ethereal haze').
card_uid('ethereal haze'/'CHK', 'CHK:Ethereal Haze:ethereal haze').
card_rarity('ethereal haze'/'CHK', 'Common').
card_artist('ethereal haze'/'CHK', 'Chris Appelhans').
card_number('ethereal haze'/'CHK', '9').
card_flavor_text('ethereal haze'/'CHK', '\"Imagine a dove flying through smoke. Does the dove injure the smoke? Does the smoke impede the dove?\"\n—Teachings of Eight-and-a-Half-Tails').
card_multiverse_id('ethereal haze'/'CHK', '80526').

card_in_set('eye of nowhere', 'CHK').
card_original_type('eye of nowhere'/'CHK', 'Sorcery — Arcane').
card_original_text('eye of nowhere'/'CHK', 'Return target permanent to its owner\'s hand.').
card_first_print('eye of nowhere', 'CHK').
card_image_name('eye of nowhere'/'CHK', 'eye of nowhere').
card_uid('eye of nowhere'/'CHK', 'CHK:Eye of Nowhere:eye of nowhere').
card_rarity('eye of nowhere'/'CHK', 'Common').
card_artist('eye of nowhere'/'CHK', 'Alan Pollack').
card_number('eye of nowhere'/'CHK', '59').
card_flavor_text('eye of nowhere'/'CHK', '\"Once we prayed to the kaijin for safe voyage. Now we only pray that we can escape their gaze.\"\n—Hayato, master sailor').
card_multiverse_id('eye of nowhere'/'CHK', '79155').

card_in_set('feast of worms', 'CHK').
card_original_type('feast of worms'/'CHK', 'Sorcery — Arcane').
card_original_text('feast of worms'/'CHK', 'Destroy target land. If that land is legendary, its controller sacrifices another land.').
card_first_print('feast of worms', 'CHK').
card_image_name('feast of worms'/'CHK', 'feast of worms').
card_uid('feast of worms'/'CHK', 'CHK:Feast of Worms:feast of worms').
card_rarity('feast of worms'/'CHK', 'Uncommon').
card_artist('feast of worms'/'CHK', 'Chippy').
card_number('feast of worms'/'CHK', '207').
card_flavor_text('feast of worms'/'CHK', '\"The dust beneath our feet was once part of a mighty civilization. Shall we too provide the path for a future generation?\"\n—Sensei Golden-Tail').
card_multiverse_id('feast of worms'/'CHK', '75402').

card_in_set('feral deceiver', 'CHK').
card_original_type('feral deceiver'/'CHK', 'Creature — Spirit').
card_original_text('feral deceiver'/'CHK', '{1}: Look at the top card of your library.\n{2}: Reveal the top card of your library. If it\'s a land, Feral Deceiver gets +2/+2 and gains trample until end of turn. Play this ability only once each turn.').
card_first_print('feral deceiver', 'CHK').
card_image_name('feral deceiver'/'CHK', 'feral deceiver').
card_uid('feral deceiver'/'CHK', 'CHK:Feral Deceiver:feral deceiver').
card_rarity('feral deceiver'/'CHK', 'Common').
card_artist('feral deceiver'/'CHK', 'Glen Angus').
card_number('feral deceiver'/'CHK', '208').
card_multiverse_id('feral deceiver'/'CHK', '50236').

card_in_set('field of reality', 'CHK').
card_original_type('field of reality'/'CHK', 'Enchant Creature').
card_original_text('field of reality'/'CHK', 'Enchanted creature can\'t be blocked by Spirits.\n{1}{U}: Return Field of Reality to its owner\'s hand.').
card_first_print('field of reality', 'CHK').
card_image_name('field of reality'/'CHK', 'field of reality').
card_uid('field of reality'/'CHK', 'CHK:Field of Reality:field of reality').
card_rarity('field of reality'/'CHK', 'Common').
card_artist('field of reality'/'CHK', 'Christopher Rush').
card_number('field of reality'/'CHK', '60').
card_flavor_text('field of reality'/'CHK', '\"The scholars of the Minamo School understood the veil between their world and that of the kami. Moreover, they knew how to exploit it.\"\n—Observations of the Kami War').
card_multiverse_id('field of reality'/'CHK', '50272').

card_in_set('floating-dream zubera', 'CHK').
card_original_type('floating-dream zubera'/'CHK', 'Creature — Zubera Spirit').
card_original_text('floating-dream zubera'/'CHK', 'When Floating-Dream Zubera is put into a graveyard from play, draw a card for each Zubera put into a graveyard from play this turn.').
card_first_print('floating-dream zubera', 'CHK').
card_image_name('floating-dream zubera'/'CHK', 'floating-dream zubera').
card_uid('floating-dream zubera'/'CHK', 'CHK:Floating-Dream Zubera:floating-dream zubera').
card_rarity('floating-dream zubera'/'CHK', 'Common').
card_artist('floating-dream zubera'/'CHK', 'Shishizaru').
card_number('floating-dream zubera'/'CHK', '61').
card_flavor_text('floating-dream zubera'/'CHK', 'When the Honden of Seeing Winds was forgotten, its attendants swarmed Kamigawa to uncover mortal secrets.').
card_multiverse_id('floating-dream zubera'/'CHK', '80507').

card_in_set('forbidden orchard', 'CHK').
card_original_type('forbidden orchard'/'CHK', 'Land').
card_original_text('forbidden orchard'/'CHK', '{T}: Add one mana of any color to your mana pool.\nWhenever you tap Forbidden Orchard for mana, put a 1/1 colorless Spirit creature token into play under target opponent\'s control.').
card_first_print('forbidden orchard', 'CHK').
card_image_name('forbidden orchard'/'CHK', 'forbidden orchard').
card_uid('forbidden orchard'/'CHK', 'CHK:Forbidden Orchard:forbidden orchard').
card_rarity('forbidden orchard'/'CHK', 'Rare').
card_artist('forbidden orchard'/'CHK', 'Dany Orizio').
card_number('forbidden orchard'/'CHK', '276').
card_multiverse_id('forbidden orchard'/'CHK', '79252').

card_in_set('forest', 'CHK').
card_original_type('forest'/'CHK', 'Basic Land — Forest').
card_original_text('forest'/'CHK', 'G').
card_image_name('forest'/'CHK', 'forest1').
card_uid('forest'/'CHK', 'CHK:Forest:forest1').
card_rarity('forest'/'CHK', 'Basic Land').
card_artist('forest'/'CHK', 'Rob Alexander').
card_number('forest'/'CHK', '303').
card_multiverse_id('forest'/'CHK', '79062').

card_in_set('forest', 'CHK').
card_original_type('forest'/'CHK', 'Basic Land — Forest').
card_original_text('forest'/'CHK', 'G').
card_image_name('forest'/'CHK', 'forest2').
card_uid('forest'/'CHK', 'CHK:Forest:forest2').
card_rarity('forest'/'CHK', 'Basic Land').
card_artist('forest'/'CHK', 'Rob Alexander').
card_number('forest'/'CHK', '304').
card_multiverse_id('forest'/'CHK', '79068').

card_in_set('forest', 'CHK').
card_original_type('forest'/'CHK', 'Basic Land — Forest').
card_original_text('forest'/'CHK', 'G').
card_image_name('forest'/'CHK', 'forest3').
card_uid('forest'/'CHK', 'CHK:Forest:forest3').
card_rarity('forest'/'CHK', 'Basic Land').
card_artist('forest'/'CHK', 'Rob Alexander').
card_number('forest'/'CHK', '305').
card_multiverse_id('forest'/'CHK', '79079').

card_in_set('forest', 'CHK').
card_original_type('forest'/'CHK', 'Basic Land — Forest').
card_original_text('forest'/'CHK', 'G').
card_image_name('forest'/'CHK', 'forest4').
card_uid('forest'/'CHK', 'CHK:Forest:forest4').
card_rarity('forest'/'CHK', 'Basic Land').
card_artist('forest'/'CHK', 'Rob Alexander').
card_number('forest'/'CHK', '306').
card_multiverse_id('forest'/'CHK', '79078').

card_in_set('frostwielder', 'CHK').
card_original_type('frostwielder'/'CHK', 'Creature — Human Shaman').
card_original_text('frostwielder'/'CHK', '{T}: Frostwielder deals 1 damage to target creature or player.\nIf a creature dealt damage by Frostwielder this turn would be put into a graveyard, remove it from the game instead.').
card_first_print('frostwielder', 'CHK').
card_image_name('frostwielder'/'CHK', 'frostwielder').
card_uid('frostwielder'/'CHK', 'CHK:Frostwielder:frostwielder').
card_rarity('frostwielder'/'CHK', 'Common').
card_artist('frostwielder'/'CHK', 'Christopher Moeller').
card_number('frostwielder'/'CHK', '167').
card_multiverse_id('frostwielder'/'CHK', '50285').

card_in_set('gale force', 'CHK').
card_original_type('gale force'/'CHK', 'Sorcery').
card_original_text('gale force'/'CHK', 'Gale Force deals 5 damage to each creature with flying.').
card_first_print('gale force', 'CHK').
card_image_name('gale force'/'CHK', 'gale force').
card_uid('gale force'/'CHK', 'CHK:Gale Force:gale force').
card_rarity('gale force'/'CHK', 'Uncommon').
card_artist('gale force'/'CHK', 'Lars Grant-West').
card_number('gale force'/'CHK', '209').
card_flavor_text('gale force'/'CHK', 'Everything the kami had done, they did with more force. Gentle breezes became typhoons, rolling rivers turned to crushing rapids, and gentle growth became overnight masses of thorns and vines.').
card_multiverse_id('gale force'/'CHK', '79244').

card_in_set('general\'s kabuto', 'CHK').
card_original_type('general\'s kabuto'/'CHK', 'Artifact — Equipment').
card_original_text('general\'s kabuto'/'CHK', 'Equipped creature can\'t be the target of spells or abilities.\nPrevent all combat damage that would be dealt to equipped creature.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('general\'s kabuto', 'CHK').
card_image_name('general\'s kabuto'/'CHK', 'general\'s kabuto').
card_uid('general\'s kabuto'/'CHK', 'CHK:General\'s Kabuto:general\'s kabuto').
card_rarity('general\'s kabuto'/'CHK', 'Rare').
card_artist('general\'s kabuto'/'CHK', 'Alex Horley-Orlandelli').
card_number('general\'s kabuto'/'CHK', '251').
card_multiverse_id('general\'s kabuto'/'CHK', '79862').

card_in_set('ghostly prison', 'CHK').
card_original_type('ghostly prison'/'CHK', 'Enchantment').
card_original_text('ghostly prison'/'CHK', 'Creatures can\'t attack you unless their controller pays {2} for each creature attacking you. (This cost is paid as attackers are declared.)').
card_image_name('ghostly prison'/'CHK', 'ghostly prison').
card_uid('ghostly prison'/'CHK', 'CHK:Ghostly Prison:ghostly prison').
card_rarity('ghostly prison'/'CHK', 'Uncommon').
card_artist('ghostly prison'/'CHK', 'Lars Grant-West').
card_number('ghostly prison'/'CHK', '10').
card_flavor_text('ghostly prison'/'CHK', 'Destroyed in one of the first battles of the Kami War, the town of Reito still grieved.').
card_multiverse_id('ghostly prison'/'CHK', '75328').

card_in_set('gibbering kami', 'CHK').
card_original_type('gibbering kami'/'CHK', 'Creature — Spirit').
card_original_text('gibbering kami'/'CHK', 'Flying\nSoulshift 3 (When this is put into a graveyard from play, you may return target Spirit card with converted mana cost 3 or less from your graveyard to your hand.)').
card_first_print('gibbering kami', 'CHK').
card_image_name('gibbering kami'/'CHK', 'gibbering kami').
card_uid('gibbering kami'/'CHK', 'CHK:Gibbering Kami:gibbering kami').
card_rarity('gibbering kami'/'CHK', 'Common').
card_artist('gibbering kami'/'CHK', 'Jim Pavelec').
card_number('gibbering kami'/'CHK', '112').
card_multiverse_id('gibbering kami'/'CHK', '78966').

card_in_set('gifts ungiven', 'CHK').
card_original_type('gifts ungiven'/'CHK', 'Instant').
card_original_text('gifts ungiven'/'CHK', 'Search your library for four cards with different names and reveal them. Target opponent chooses two of those cards. Put the chosen cards into your graveyard and the rest into your hand. Then shuffle your library.').
card_first_print('gifts ungiven', 'CHK').
card_image_name('gifts ungiven'/'CHK', 'gifts ungiven').
card_uid('gifts ungiven'/'CHK', 'CHK:Gifts Ungiven:gifts ungiven').
card_rarity('gifts ungiven'/'CHK', 'Rare').
card_artist('gifts ungiven'/'CHK', 'D. Alexander Gregory').
card_number('gifts ungiven'/'CHK', '62').
card_multiverse_id('gifts ungiven'/'CHK', '79090').

card_in_set('glacial ray', 'CHK').
card_original_type('glacial ray'/'CHK', 'Instant — Arcane').
card_original_text('glacial ray'/'CHK', 'Glacial Ray deals 2 damage to target creature or player.\nSplice onto Arcane {1}{R} (As you play an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_image_name('glacial ray'/'CHK', 'glacial ray').
card_uid('glacial ray'/'CHK', 'CHK:Glacial Ray:glacial ray').
card_rarity('glacial ray'/'CHK', 'Common').
card_artist('glacial ray'/'CHK', 'Jim Murray').
card_number('glacial ray'/'CHK', '168').
card_multiverse_id('glacial ray'/'CHK', '80245').

card_in_set('glimpse of nature', 'CHK').
card_original_type('glimpse of nature'/'CHK', 'Sorcery').
card_original_text('glimpse of nature'/'CHK', 'Whenever you play a creature spell this turn, draw a card.').
card_first_print('glimpse of nature', 'CHK').
card_image_name('glimpse of nature'/'CHK', 'glimpse of nature').
card_uid('glimpse of nature'/'CHK', 'CHK:Glimpse of Nature:glimpse of nature').
card_rarity('glimpse of nature'/'CHK', 'Rare').
card_artist('glimpse of nature'/'CHK', 'Shishizaru').
card_number('glimpse of nature'/'CHK', '210').
card_flavor_text('glimpse of nature'/'CHK', 'Dosan sat in repose for many hours. He made no motion, no sound at all. And as he sat, nature revealed itself to him.').
card_multiverse_id('glimpse of nature'/'CHK', '75241').

card_in_set('godo, bandit warlord', 'CHK').
card_original_type('godo, bandit warlord'/'CHK', 'Legendary Creature — Human Barbarian').
card_original_text('godo, bandit warlord'/'CHK', 'When Godo, Bandit Warlord comes into play, you may search your library for an Equipment card and put it into play. If you do, shuffle your library.\nWhenever Godo attacks for the first time each turn, untap it and all Samurai you control. After this phase, you get an additional combat phase.').
card_first_print('godo, bandit warlord', 'CHK').
card_image_name('godo, bandit warlord'/'CHK', 'godo, bandit warlord').
card_uid('godo, bandit warlord'/'CHK', 'CHK:Godo, Bandit Warlord:godo, bandit warlord').
card_rarity('godo, bandit warlord'/'CHK', 'Rare').
card_artist('godo, bandit warlord'/'CHK', 'Paolo Parente').
card_number('godo, bandit warlord'/'CHK', '169').
card_multiverse_id('godo, bandit warlord'/'CHK', '78192').

card_in_set('goka the unjust', 'CHK').
card_original_type('goka the unjust'/'CHK', 'Creature — Ogre Shaman').
card_original_text('goka the unjust'/'CHK', '{T}: Initiate of Blood deals 1 damage to target creature that was dealt damage this turn. When that creature is put into a graveyard this turn, flip Initiate of Blood.\n-----\nGoka the Unjust\nLegendary Creature Ogre Shaman\n4/4\n{T}: Goka the Unjust deals 4 damage to target creature that was dealt damage this turn.').
card_first_print('goka the unjust', 'CHK').
card_image_name('goka the unjust'/'CHK', 'goka the unjust').
card_uid('goka the unjust'/'CHK', 'CHK:Goka the Unjust:goka the unjust').
card_rarity('goka the unjust'/'CHK', 'Uncommon').
card_artist('goka the unjust'/'CHK', 'Carl Critchlow').
card_number('goka the unjust'/'CHK', '173b').
card_multiverse_id('goka the unjust'/'CHK', '78688').

card_in_set('graceful adept', 'CHK').
card_original_type('graceful adept'/'CHK', 'Creature — Human Wizard').
card_original_text('graceful adept'/'CHK', 'You have no maximum hand size.').
card_first_print('graceful adept', 'CHK').
card_image_name('graceful adept'/'CHK', 'graceful adept').
card_uid('graceful adept'/'CHK', 'CHK:Graceful Adept:graceful adept').
card_rarity('graceful adept'/'CHK', 'Uncommon').
card_artist('graceful adept'/'CHK', 'Scott M. Fischer').
card_number('graceful adept'/'CHK', '63').
card_flavor_text('graceful adept'/'CHK', '\"When you have mastered my lessons, it will seem as though the whole of the world has opened up to your mind and nothing is beyond your grasp.\"\n—Lady Azami').
card_multiverse_id('graceful adept'/'CHK', '79230').

card_in_set('guardian of solitude', 'CHK').
card_original_type('guardian of solitude'/'CHK', 'Creature — Spirit').
card_original_text('guardian of solitude'/'CHK', 'Whenever you play a Spirit or Arcane spell, target creature gains flying until end of turn.').
card_first_print('guardian of solitude', 'CHK').
card_image_name('guardian of solitude'/'CHK', 'guardian of solitude').
card_uid('guardian of solitude'/'CHK', 'CHK:Guardian of Solitude:guardian of solitude').
card_rarity('guardian of solitude'/'CHK', 'Uncommon').
card_artist('guardian of solitude'/'CHK', 'Stephen Tappin').
card_number('guardian of solitude'/'CHK', '64').
card_flavor_text('guardian of solitude'/'CHK', '\"It seemed an easy thing, to step into the nothingness, to fall, to die. But then, for an instant, I saw it, eyes filled with endless sorrow, and I turned back to face my pain.\"\n—Snow-Fur, kitsune poet').
card_multiverse_id('guardian of solitude'/'CHK', '79194').

card_in_set('gutwrencher oni', 'CHK').
card_original_type('gutwrencher oni'/'CHK', 'Creature — Demon Spirit').
card_original_text('gutwrencher oni'/'CHK', 'Trample\nAt the beginning of your upkeep, discard a card if you don\'t control an Ogre.').
card_first_print('gutwrencher oni', 'CHK').
card_image_name('gutwrencher oni'/'CHK', 'gutwrencher oni').
card_uid('gutwrencher oni'/'CHK', 'CHK:Gutwrencher Oni:gutwrencher oni').
card_rarity('gutwrencher oni'/'CHK', 'Uncommon').
card_artist('gutwrencher oni'/'CHK', 'Hideaki Takamura').
card_number('gutwrencher oni'/'CHK', '113').
card_flavor_text('gutwrencher oni'/'CHK', '\"Blood drips. Blood sings. Blood devours all and only blood remains.\"\n—Ogre chant').
card_multiverse_id('gutwrencher oni'/'CHK', '79088').

card_in_set('hair-strung koto', 'CHK').
card_original_type('hair-strung koto'/'CHK', 'Artifact').
card_original_text('hair-strung koto'/'CHK', 'Tap an untapped creature you control: Target player puts the top card of his or her library into his or her graveyard.').
card_first_print('hair-strung koto', 'CHK').
card_image_name('hair-strung koto'/'CHK', 'hair-strung koto').
card_uid('hair-strung koto'/'CHK', 'CHK:Hair-Strung Koto:hair-strung koto').
card_rarity('hair-strung koto'/'CHK', 'Rare').
card_artist('hair-strung koto'/'CHK', 'Rebecca Guay').
card_number('hair-strung koto'/'CHK', '252').
card_flavor_text('hair-strung koto'/'CHK', '\"The Kami War drove many members of Konda\'s court insane. As their spiritual world turned against them, so too did their minds turn from reality.\"\n—The History of Kamigawa').
card_multiverse_id('hair-strung koto'/'CHK', '79219').

card_in_set('hall of the bandit lord', 'CHK').
card_original_type('hall of the bandit lord'/'CHK', 'Legendary Land').
card_original_text('hall of the bandit lord'/'CHK', 'Hall of the Bandit Lord comes into play tapped.\n{T}, Pay 3 life: Add {1} to your mana pool. If that mana is spent on a creature spell, that creature has haste.').
card_first_print('hall of the bandit lord', 'CHK').
card_image_name('hall of the bandit lord'/'CHK', 'hall of the bandit lord').
card_uid('hall of the bandit lord'/'CHK', 'CHK:Hall of the Bandit Lord:hall of the bandit lord').
card_rarity('hall of the bandit lord'/'CHK', 'Rare').
card_artist('hall of the bandit lord'/'CHK', 'Paolo Parente').
card_number('hall of the bandit lord'/'CHK', '277').
card_multiverse_id('hall of the bandit lord'/'CHK', '77924').

card_in_set('hana kami', 'CHK').
card_original_type('hana kami'/'CHK', 'Creature — Spirit').
card_original_text('hana kami'/'CHK', '{1}{G}, Sacrifice Hana Kami: Return target Arcane card from your graveyard to your hand.').
card_first_print('hana kami', 'CHK').
card_image_name('hana kami'/'CHK', 'hana kami').
card_uid('hana kami'/'CHK', 'CHK:Hana Kami:hana kami').
card_rarity('hana kami'/'CHK', 'Uncommon').
card_artist('hana kami'/'CHK', 'Rebecca Guay').
card_number('hana kami'/'CHK', '211').
card_flavor_text('hana kami'/'CHK', 'It grew in lands lit by pride and watered by tears.').
card_multiverse_id('hana kami'/'CHK', '77138').

card_in_set('hanabi blast', 'CHK').
card_original_type('hanabi blast'/'CHK', 'Instant').
card_original_text('hanabi blast'/'CHK', 'Hanabi Blast deals 2 damage to target creature or player. Return Hanabi Blast to its owner\'s hand, then discard a card at random.').
card_first_print('hanabi blast', 'CHK').
card_image_name('hanabi blast'/'CHK', 'hanabi blast').
card_uid('hanabi blast'/'CHK', 'CHK:Hanabi Blast:hanabi blast').
card_rarity('hanabi blast'/'CHK', 'Uncommon').
card_artist('hanabi blast'/'CHK', 'Paolo Parente').
card_number('hanabi blast'/'CHK', '170').
card_flavor_text('hanabi blast'/'CHK', 'The most powerful of akki fire spells were developed at the cost of blood, toil, tears, sweat, and usually a nose or two.').
card_multiverse_id('hanabi blast'/'CHK', '75357').

card_in_set('hankyu', 'CHK').
card_original_type('hankyu'/'CHK', 'Artifact — Equipment').
card_original_text('hankyu'/'CHK', 'Equipped creature has \"{T}: Put an aim counter on Hankyu\" and \"{T}, Remove all aim counters from Hankyu: This creature deals damage to target creature or player equal to the number of aim counters removed.\"\nEquip {4} ({4}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('hankyu', 'CHK').
card_image_name('hankyu'/'CHK', 'hankyu').
card_uid('hankyu'/'CHK', 'CHK:Hankyu:hankyu').
card_rarity('hankyu'/'CHK', 'Uncommon').
card_artist('hankyu'/'CHK', 'Ben Thompson').
card_number('hankyu'/'CHK', '253').
card_multiverse_id('hankyu'/'CHK', '50469').

card_in_set('harsh deceiver', 'CHK').
card_original_type('harsh deceiver'/'CHK', 'Creature — Spirit').
card_original_text('harsh deceiver'/'CHK', '{1}: Look at the top card of your library.\n{2}: Reveal the top card of your library. If it\'s a land, untap Harsh Deceiver and it gets +1/+1 until end of turn. Play this ability only once each turn.').
card_first_print('harsh deceiver', 'CHK').
card_image_name('harsh deceiver'/'CHK', 'harsh deceiver').
card_uid('harsh deceiver'/'CHK', 'CHK:Harsh Deceiver:harsh deceiver').
card_rarity('harsh deceiver'/'CHK', 'Common').
card_artist('harsh deceiver'/'CHK', 'Heather Hudson').
card_number('harsh deceiver'/'CHK', '11').
card_multiverse_id('harsh deceiver'/'CHK', '50343').

card_in_set('he who hungers', 'CHK').
card_original_type('he who hungers'/'CHK', 'Legendary Creature — Spirit').
card_original_text('he who hungers'/'CHK', 'Flying\n{1}, Sacrifice a Spirit: Target opponent reveals his or her hand. Choose a card from it. That player discards that card. Play this ability only any time you could play a sorcery.\nSoulshift 4').
card_first_print('he who hungers', 'CHK').
card_image_name('he who hungers'/'CHK', 'he who hungers').
card_uid('he who hungers'/'CHK', 'CHK:He Who Hungers:he who hungers').
card_rarity('he who hungers'/'CHK', 'Rare').
card_artist('he who hungers'/'CHK', 'Kev Walker').
card_number('he who hungers'/'CHK', '114').
card_multiverse_id('he who hungers'/'CHK', '78602').

card_in_set('heartbeat of spring', 'CHK').
card_original_type('heartbeat of spring'/'CHK', 'Enchantment').
card_original_text('heartbeat of spring'/'CHK', 'Whenever a player taps a land for mana, that player adds one mana of that type to his or her mana pool.').
card_first_print('heartbeat of spring', 'CHK').
card_image_name('heartbeat of spring'/'CHK', 'heartbeat of spring').
card_uid('heartbeat of spring'/'CHK', 'CHK:Heartbeat of Spring:heartbeat of spring').
card_rarity('heartbeat of spring'/'CHK', 'Rare').
card_artist('heartbeat of spring'/'CHK', 'Rob Alexander').
card_number('heartbeat of spring'/'CHK', '212').
card_flavor_text('heartbeat of spring'/'CHK', '\"It is true that we monks hold the key to paradise, but most don\'t understand that the paradise we guard is the one within.\"\n—Diary of Azusa').
card_multiverse_id('heartbeat of spring'/'CHK', '50461').

card_in_set('hearth kami', 'CHK').
card_original_type('hearth kami'/'CHK', 'Creature — Spirit').
card_original_text('hearth kami'/'CHK', '{X}, Sacrifice Hearth Kami: Destroy target artifact with converted mana cost X.').
card_first_print('hearth kami', 'CHK').
card_image_name('hearth kami'/'CHK', 'hearth kami').
card_uid('hearth kami'/'CHK', 'CHK:Hearth Kami:hearth kami').
card_rarity('hearth kami'/'CHK', 'Common').
card_artist('hearth kami'/'CHK', 'Luca Zontini').
card_number('hearth kami'/'CHK', '171').
card_flavor_text('hearth kami'/'CHK', '\"Every treachery, great or small, begets a spirit that rages at the injustice. Given the opportunity, each will return that treachery to its owner tenfold.\"\n—Sensei Hisoka').
card_multiverse_id('hearth kami'/'CHK', '77139').

card_in_set('hideous laughter', 'CHK').
card_original_type('hideous laughter'/'CHK', 'Instant — Arcane').
card_original_text('hideous laughter'/'CHK', 'All creatures get -2/-2 until end of turn.\nSplice onto Arcane {3}{B}{B} (As you play an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_first_print('hideous laughter', 'CHK').
card_image_name('hideous laughter'/'CHK', 'hideous laughter').
card_uid('hideous laughter'/'CHK', 'CHK:Hideous Laughter:hideous laughter').
card_rarity('hideous laughter'/'CHK', 'Uncommon').
card_artist('hideous laughter'/'CHK', 'Greg Staples').
card_number('hideous laughter'/'CHK', '115').
card_multiverse_id('hideous laughter'/'CHK', '79204').

card_in_set('hikari, twilight guardian', 'CHK').
card_original_type('hikari, twilight guardian'/'CHK', 'Legendary Creature — Spirit').
card_original_text('hikari, twilight guardian'/'CHK', 'Flying\nWhenever you play a Spirit or Arcane spell, you may remove Hikari, Twilight Guardian from the game. If you do, return it to play under its owner\'s control at end of turn.').
card_first_print('hikari, twilight guardian', 'CHK').
card_image_name('hikari, twilight guardian'/'CHK', 'hikari, twilight guardian').
card_uid('hikari, twilight guardian'/'CHK', 'CHK:Hikari, Twilight Guardian:hikari, twilight guardian').
card_rarity('hikari, twilight guardian'/'CHK', 'Rare').
card_artist('hikari, twilight guardian'/'CHK', 'Glen Angus').
card_number('hikari, twilight guardian'/'CHK', '12').
card_multiverse_id('hikari, twilight guardian'/'CHK', '79222').

card_in_set('hinder', 'CHK').
card_original_type('hinder'/'CHK', 'Instant').
card_original_text('hinder'/'CHK', 'Counter target spell. If it\'s countered this way, put that card on the top or bottom of its owner\'s library instead of that player\'s graveyard.').
card_image_name('hinder'/'CHK', 'hinder').
card_uid('hinder'/'CHK', 'CHK:Hinder:hinder').
card_rarity('hinder'/'CHK', 'Uncommon').
card_artist('hinder'/'CHK', 'Wayne Reynolds').
card_number('hinder'/'CHK', '65').
card_flavor_text('hinder'/'CHK', '\"Do not react to force in kind. Turn it aside. Direct it to where it can do no harm.\"\n—Meloku the Clouded Mirror').
card_multiverse_id('hinder'/'CHK', '50426').

card_in_set('hisoka\'s defiance', 'CHK').
card_original_type('hisoka\'s defiance'/'CHK', 'Instant').
card_original_text('hisoka\'s defiance'/'CHK', 'Counter target Spirit or Arcane spell.').
card_first_print('hisoka\'s defiance', 'CHK').
card_image_name('hisoka\'s defiance'/'CHK', 'hisoka\'s defiance').
card_uid('hisoka\'s defiance'/'CHK', 'CHK:Hisoka\'s Defiance:hisoka\'s defiance').
card_rarity('hisoka\'s defiance'/'CHK', 'Common').
card_artist('hisoka\'s defiance'/'CHK', 'Greg Hildebrandt').
card_number('hisoka\'s defiance'/'CHK', '67').
card_flavor_text('hisoka\'s defiance'/'CHK', '\"With every passing day, the kami shape our world to suit their will. I, for one, would not see them succeed.\"').
card_multiverse_id('hisoka\'s defiance'/'CHK', '50439').

card_in_set('hisoka\'s guard', 'CHK').
card_original_type('hisoka\'s guard'/'CHK', 'Creature — Human Wizard').
card_original_text('hisoka\'s guard'/'CHK', 'You may choose not to untap Hisoka\'s Guard during your untap step.\n{1}{U}, {T}: As long as Hisoka\'s Guard remains tapped, target creature you control other than Hisoka\'s Guard can\'t be the target of spells or abilities.').
card_first_print('hisoka\'s guard', 'CHK').
card_image_name('hisoka\'s guard'/'CHK', 'hisoka\'s guard').
card_uid('hisoka\'s guard'/'CHK', 'CHK:Hisoka\'s Guard:hisoka\'s guard').
card_rarity('hisoka\'s guard'/'CHK', 'Common').
card_artist('hisoka\'s guard'/'CHK', 'Wayne England').
card_number('hisoka\'s guard'/'CHK', '68').
card_multiverse_id('hisoka\'s guard'/'CHK', '79149').

card_in_set('hisoka, minamo sensei', 'CHK').
card_original_type('hisoka, minamo sensei'/'CHK', 'Legendary Creature — Human Wizard').
card_original_text('hisoka, minamo sensei'/'CHK', '{2}{U}, Discard a card: Counter target spell if it has the same converted mana cost as the discarded card.').
card_first_print('hisoka, minamo sensei', 'CHK').
card_image_name('hisoka, minamo sensei'/'CHK', 'hisoka, minamo sensei').
card_uid('hisoka, minamo sensei'/'CHK', 'CHK:Hisoka, Minamo Sensei:hisoka, minamo sensei').
card_rarity('hisoka, minamo sensei'/'CHK', 'Rare').
card_artist('hisoka, minamo sensei'/'CHK', 'Donato Giancola').
card_number('hisoka, minamo sensei'/'CHK', '66').
card_flavor_text('hisoka, minamo sensei'/'CHK', '\"By all rights we should have perished in the Kami War. Our perseverence is a tribute to mortal ingenuity. And perhaps a few forgotten secrets found in the nick of time.\"').
card_multiverse_id('hisoka, minamo sensei'/'CHK', '50474').

card_in_set('hold the line', 'CHK').
card_original_type('hold the line'/'CHK', 'Instant').
card_original_text('hold the line'/'CHK', 'Blocking creatures get +7/+7 until end of turn.').
card_first_print('hold the line', 'CHK').
card_image_name('hold the line'/'CHK', 'hold the line').
card_uid('hold the line'/'CHK', 'CHK:Hold the Line:hold the line').
card_rarity('hold the line'/'CHK', 'Rare').
card_artist('hold the line'/'CHK', 'Ron Spears').
card_number('hold the line'/'CHK', '13').
card_flavor_text('hold the line'/'CHK', '\"Forgive me, Master Kami, but in the interest of my people I must halt your advance.\"').
card_multiverse_id('hold the line'/'CHK', '79144').

card_in_set('honden of cleansing fire', 'CHK').
card_original_type('honden of cleansing fire'/'CHK', 'Legendary Enchantment — Shrine').
card_original_text('honden of cleansing fire'/'CHK', 'At the beginning of your upkeep, you gain 2 life for each Shrine you control.').
card_first_print('honden of cleansing fire', 'CHK').
card_image_name('honden of cleansing fire'/'CHK', 'honden of cleansing fire').
card_uid('honden of cleansing fire'/'CHK', 'CHK:Honden of Cleansing Fire:honden of cleansing fire').
card_rarity('honden of cleansing fire'/'CHK', 'Uncommon').
card_artist('honden of cleansing fire'/'CHK', 'Greg Staples').
card_number('honden of cleansing fire'/'CHK', '14').
card_flavor_text('honden of cleansing fire'/'CHK', 'To the sorrow of all, its fire was turned toward those who worshipped it.').
card_multiverse_id('honden of cleansing fire'/'CHK', '79159').

card_in_set('honden of infinite rage', 'CHK').
card_original_type('honden of infinite rage'/'CHK', 'Legendary Enchantment — Shrine').
card_original_text('honden of infinite rage'/'CHK', 'At the beginning of your upkeep, Honden of Infinite Rage deals damage to target creature or player equal to the number of Shrines you control.').
card_first_print('honden of infinite rage', 'CHK').
card_image_name('honden of infinite rage'/'CHK', 'honden of infinite rage').
card_uid('honden of infinite rage'/'CHK', 'CHK:Honden of Infinite Rage:honden of infinite rage').
card_rarity('honden of infinite rage'/'CHK', 'Uncommon').
card_artist('honden of infinite rage'/'CHK', 'John Avon').
card_number('honden of infinite rage'/'CHK', '172').
card_flavor_text('honden of infinite rage'/'CHK', 'To the sorrow of all, its rage became focused on those who once stoked it.').
card_multiverse_id('honden of infinite rage'/'CHK', '79110').

card_in_set('honden of life\'s web', 'CHK').
card_original_type('honden of life\'s web'/'CHK', 'Legendary Enchantment — Shrine').
card_original_text('honden of life\'s web'/'CHK', 'At the beginning of your upkeep, put a 1/1 colorless Spirit creature token into play for each Shrine you control.').
card_first_print('honden of life\'s web', 'CHK').
card_image_name('honden of life\'s web'/'CHK', 'honden of life\'s web').
card_uid('honden of life\'s web'/'CHK', 'CHK:Honden of Life\'s Web:honden of life\'s web').
card_rarity('honden of life\'s web'/'CHK', 'Uncommon').
card_artist('honden of life\'s web'/'CHK', 'Rob Alexander').
card_number('honden of life\'s web'/'CHK', '213').
card_flavor_text('honden of life\'s web'/'CHK', 'To the sorrow of all, its web became a net that strangled those who helped weave it.').
card_multiverse_id('honden of life\'s web'/'CHK', '79180').

card_in_set('honden of night\'s reach', 'CHK').
card_original_type('honden of night\'s reach'/'CHK', 'Legendary Enchantment — Shrine').
card_original_text('honden of night\'s reach'/'CHK', 'At the beginning of your upkeep, target opponent discards a card for each Shrine you control.').
card_first_print('honden of night\'s reach', 'CHK').
card_image_name('honden of night\'s reach'/'CHK', 'honden of night\'s reach').
card_uid('honden of night\'s reach'/'CHK', 'CHK:Honden of Night\'s Reach:honden of night\'s reach').
card_rarity('honden of night\'s reach'/'CHK', 'Uncommon').
card_artist('honden of night\'s reach'/'CHK', 'Jim Nelson').
card_number('honden of night\'s reach'/'CHK', '116').
card_flavor_text('honden of night\'s reach'/'CHK', 'To the sorrow of all, its dark reach grasped and crushed those who guarded its silent vigil.').
card_multiverse_id('honden of night\'s reach'/'CHK', '79098').

card_in_set('honden of seeing winds', 'CHK').
card_original_type('honden of seeing winds'/'CHK', 'Legendary Enchantment — Shrine').
card_original_text('honden of seeing winds'/'CHK', 'At the beginning of your upkeep, draw a card for each Shrine you control.').
card_first_print('honden of seeing winds', 'CHK').
card_image_name('honden of seeing winds'/'CHK', 'honden of seeing winds').
card_uid('honden of seeing winds'/'CHK', 'CHK:Honden of Seeing Winds:honden of seeing winds').
card_rarity('honden of seeing winds'/'CHK', 'Uncommon').
card_artist('honden of seeing winds'/'CHK', 'Martina Pilcerova').
card_number('honden of seeing winds'/'CHK', '69').
card_flavor_text('honden of seeing winds'/'CHK', 'To the sorrow of all, its winds found sin in the hearts of those who once learned from its wisdom.').
card_multiverse_id('honden of seeing winds'/'CHK', '79176').

card_in_set('honor-worn shaku', 'CHK').
card_original_type('honor-worn shaku'/'CHK', 'Artifact').
card_original_text('honor-worn shaku'/'CHK', '{T}: Add {1} to your mana pool.\nTap an untapped legendary permanent you control: Untap Honor-Worn Shaku.').
card_first_print('honor-worn shaku', 'CHK').
card_image_name('honor-worn shaku'/'CHK', 'honor-worn shaku').
card_uid('honor-worn shaku'/'CHK', 'CHK:Honor-Worn Shaku:honor-worn shaku').
card_rarity('honor-worn shaku'/'CHK', 'Uncommon').
card_artist('honor-worn shaku'/'CHK', 'Tony Szczudlo').
card_number('honor-worn shaku'/'CHK', '254').
card_flavor_text('honor-worn shaku'/'CHK', 'Before being presented to its lord, each shaku is blessed by every hero of every region within that lord\'s domain.').
card_multiverse_id('honor-worn shaku'/'CHK', '77143').

card_in_set('horizon seed', 'CHK').
card_original_type('horizon seed'/'CHK', 'Creature — Spirit').
card_original_text('horizon seed'/'CHK', 'Whenever you play a Spirit or Arcane spell, regenerate target creature.').
card_first_print('horizon seed', 'CHK').
card_image_name('horizon seed'/'CHK', 'horizon seed').
card_uid('horizon seed'/'CHK', 'CHK:Horizon Seed:horizon seed').
card_rarity('horizon seed'/'CHK', 'Uncommon').
card_artist('horizon seed'/'CHK', 'Matt Cavotta').
card_number('horizon seed'/'CHK', '15').
card_flavor_text('horizon seed'/'CHK', 'In peaceful times, these beings escorted the honored kami to their new shrines. In the Kami War, they became the medics of an unstoppable army.').
card_multiverse_id('horizon seed'/'CHK', '79226').

card_in_set('horobi, death\'s wail', 'CHK').
card_original_type('horobi, death\'s wail'/'CHK', 'Legendary Creature — Spirit').
card_original_text('horobi, death\'s wail'/'CHK', 'Flying\nWhenever a creature becomes the target of a spell or ability, destroy that creature.').
card_first_print('horobi, death\'s wail', 'CHK').
card_image_name('horobi, death\'s wail'/'CHK', 'horobi, death\'s wail').
card_uid('horobi, death\'s wail'/'CHK', 'CHK:Horobi, Death\'s Wail:horobi, death\'s wail').
card_rarity('horobi, death\'s wail'/'CHK', 'Rare').
card_artist('horobi, death\'s wail'/'CHK', 'John Bolton').
card_number('horobi, death\'s wail'/'CHK', '117').
card_flavor_text('horobi, death\'s wail'/'CHK', '\"From the ashes of Reito rose a new kami. And thereafter at every battle came Horobi, Death\'s Wail.\"\n—The History of Kamigawa').
card_multiverse_id('horobi, death\'s wail'/'CHK', '78854').

card_in_set('humble budoka', 'CHK').
card_original_type('humble budoka'/'CHK', 'Creature — Human Monk').
card_original_text('humble budoka'/'CHK', 'Humble Budoka can\'t be the target of spells or abilities.').
card_first_print('humble budoka', 'CHK').
card_image_name('humble budoka'/'CHK', 'humble budoka').
card_uid('humble budoka'/'CHK', 'CHK:Humble Budoka:humble budoka').
card_rarity('humble budoka'/'CHK', 'Common').
card_artist('humble budoka'/'CHK', 'Christopher Moeller').
card_number('humble budoka'/'CHK', '214').
card_flavor_text('humble budoka'/'CHK', 'Each time wanderers entered the forest seeking enlightenment, Dosan was there, waiting for them to arrive.').
card_multiverse_id('humble budoka'/'CHK', '78684').

card_in_set('hundred-talon kami', 'CHK').
card_original_type('hundred-talon kami'/'CHK', 'Creature — Spirit').
card_original_text('hundred-talon kami'/'CHK', 'Flying\nSoulshift 4 (When this is put into a graveyard from play, you may return target Spirit card with converted mana cost 4 or less from your graveyard to your hand.)').
card_first_print('hundred-talon kami', 'CHK').
card_image_name('hundred-talon kami'/'CHK', 'hundred-talon kami').
card_uid('hundred-talon kami'/'CHK', 'CHK:Hundred-Talon Kami:hundred-talon kami').
card_rarity('hundred-talon kami'/'CHK', 'Common').
card_artist('hundred-talon kami'/'CHK', 'Paolo Parente').
card_number('hundred-talon kami'/'CHK', '16').
card_multiverse_id('hundred-talon kami'/'CHK', '79145').

card_in_set('imi statue', 'CHK').
card_original_type('imi statue'/'CHK', 'Artifact').
card_original_text('imi statue'/'CHK', 'Players can\'t untap more than one artifact during their untap steps.').
card_first_print('imi statue', 'CHK').
card_image_name('imi statue'/'CHK', 'imi statue').
card_uid('imi statue'/'CHK', 'CHK:Imi Statue:imi statue').
card_rarity('imi statue'/'CHK', 'Rare').
card_artist('imi statue'/'CHK', 'Todd Lockwood').
card_number('imi statue'/'CHK', '255').
card_flavor_text('imi statue'/'CHK', '\"Just looking at it fills me with dread . . . . Yet since it arrived, I\'ve done little else. My blades have dulled and my business has failed, and still I cannot look away.\"\n—Keisaku, master swordmaker').
card_multiverse_id('imi statue'/'CHK', '50300').

card_in_set('iname, death aspect', 'CHK').
card_original_type('iname, death aspect'/'CHK', 'Legendary Creature — Spirit').
card_original_text('iname, death aspect'/'CHK', 'When Iname, Death Aspect comes into play, you may search your library for any number of Spirit cards and put them into your graveyard. If you do, shuffle your library.').
card_first_print('iname, death aspect', 'CHK').
card_image_name('iname, death aspect'/'CHK', 'iname, death aspect').
card_uid('iname, death aspect'/'CHK', 'CHK:Iname, Death Aspect:iname, death aspect').
card_rarity('iname, death aspect'/'CHK', 'Rare').
card_artist('iname, death aspect'/'CHK', 'Justin Sweet').
card_number('iname, death aspect'/'CHK', '118').
card_flavor_text('iname, death aspect'/'CHK', 'Iname revels in sadistic glee at the crushing of souls, but soon mourns the lives so cruelly cut short. So the cycle begins anew.').
card_multiverse_id('iname, death aspect'/'CHK', '80278').

card_in_set('iname, life aspect', 'CHK').
card_original_type('iname, life aspect'/'CHK', 'Legendary Creature — Spirit').
card_original_text('iname, life aspect'/'CHK', 'When Iname, Life Aspect is put into a graveyard from play, you may remove Iname, Life Aspect from the game. If you do, return any number of target Spirit cards from your graveyard to your hand.').
card_first_print('iname, life aspect', 'CHK').
card_image_name('iname, life aspect'/'CHK', 'iname, life aspect').
card_uid('iname, life aspect'/'CHK', 'CHK:Iname, Life Aspect:iname, life aspect').
card_rarity('iname, life aspect'/'CHK', 'Rare').
card_artist('iname, life aspect'/'CHK', 'Justin Sweet').
card_number('iname, life aspect'/'CHK', '215').
card_flavor_text('iname, life aspect'/'CHK', 'Iname rejoices in the dawn of a new life, but soon becomes jealous of the simple joys denied him by his station. So the cycle begins anew.').
card_multiverse_id('iname, life aspect'/'CHK', '80282').

card_in_set('indomitable will', 'CHK').
card_original_type('indomitable will'/'CHK', 'Enchant Creature').
card_original_text('indomitable will'/'CHK', 'You may play Indomitable Will any time you could play an instant.\nEnchanted creature gets +1/+2.').
card_first_print('indomitable will', 'CHK').
card_image_name('indomitable will'/'CHK', 'indomitable will').
card_uid('indomitable will'/'CHK', 'CHK:Indomitable Will:indomitable will').
card_rarity('indomitable will'/'CHK', 'Common').
card_artist('indomitable will'/'CHK', 'Christopher Rush').
card_number('indomitable will'/'CHK', '17').
card_flavor_text('indomitable will'/'CHK', '\"None of you are yet samurai. Defend your homes with honor, die for your cause, and perhaps then you will be worthy of the word.\"\n—General Takeno').
card_multiverse_id('indomitable will'/'CHK', '50499').

card_in_set('initiate of blood', 'CHK').
card_original_type('initiate of blood'/'CHK', 'Creature — Ogre Shaman').
card_original_text('initiate of blood'/'CHK', '{T}: Initiate of Blood deals 1 damage to target creature that was dealt damage this turn. When that creature is put into a graveyard this turn, flip Initiate of Blood.\n-----\nGoka the Unjust\nLegendary Creature Ogre Shaman\n4/4\n{T}: Goka the Unjust deals 4 damage to target creature that was dealt damage this turn.').
card_first_print('initiate of blood', 'CHK').
card_image_name('initiate of blood'/'CHK', 'initiate of blood').
card_uid('initiate of blood'/'CHK', 'CHK:Initiate of Blood:initiate of blood').
card_rarity('initiate of blood'/'CHK', 'Uncommon').
card_artist('initiate of blood'/'CHK', 'Carl Critchlow').
card_number('initiate of blood'/'CHK', '173a').
card_multiverse_id('initiate of blood'/'CHK', '78688').

card_in_set('innocence kami', 'CHK').
card_original_type('innocence kami'/'CHK', 'Creature — Spirit').
card_original_text('innocence kami'/'CHK', '{W}, {T}: Tap target creature.\nWhenever you play a Spirit or Arcane spell, untap Innocence Kami.').
card_first_print('innocence kami', 'CHK').
card_image_name('innocence kami'/'CHK', 'innocence kami').
card_uid('innocence kami'/'CHK', 'CHK:Innocence Kami:innocence kami').
card_rarity('innocence kami'/'CHK', 'Uncommon').
card_artist('innocence kami'/'CHK', 'Mark Zug').
card_number('innocence kami'/'CHK', '18').
card_flavor_text('innocence kami'/'CHK', 'Her voice was light, her substance music.').
card_multiverse_id('innocence kami'/'CHK', '76636').

card_in_set('isamaru, hound of konda', 'CHK').
card_original_type('isamaru, hound of konda'/'CHK', 'Legendary Creature — Hound').
card_original_text('isamaru, hound of konda'/'CHK', '').
card_first_print('isamaru, hound of konda', 'CHK').
card_image_name('isamaru, hound of konda'/'CHK', 'isamaru, hound of konda').
card_uid('isamaru, hound of konda'/'CHK', 'CHK:Isamaru, Hound of Konda:isamaru, hound of konda').
card_rarity('isamaru, hound of konda'/'CHK', 'Rare').
card_artist('isamaru, hound of konda'/'CHK', 'Christopher Moeller').
card_number('isamaru, hound of konda'/'CHK', '19').
card_flavor_text('isamaru, hound of konda'/'CHK', 'The hound sniffed the air and let slip a low growl. General Takeno looked down at the faithful Isamaru and calmed him with a touch. \"Alert the men. The kami are coming.\"').
card_multiverse_id('isamaru, hound of konda'/'CHK', '79217').

card_in_set('island', 'CHK').
card_original_type('island'/'CHK', 'Basic Land — Island').
card_original_text('island'/'CHK', 'U').
card_image_name('island'/'CHK', 'island1').
card_uid('island'/'CHK', 'CHK:Island:island1').
card_rarity('island'/'CHK', 'Basic Land').
card_artist('island'/'CHK', 'Martina Pilcerova').
card_number('island'/'CHK', '291').
card_multiverse_id('island'/'CHK', '79061').

card_in_set('island', 'CHK').
card_original_type('island'/'CHK', 'Basic Land — Island').
card_original_text('island'/'CHK', 'U').
card_image_name('island'/'CHK', 'island2').
card_uid('island'/'CHK', 'CHK:Island:island2').
card_rarity('island'/'CHK', 'Basic Land').
card_artist('island'/'CHK', 'Martina Pilcerova').
card_number('island'/'CHK', '292').
card_multiverse_id('island'/'CHK', '79069').

card_in_set('island', 'CHK').
card_original_type('island'/'CHK', 'Basic Land — Island').
card_original_text('island'/'CHK', 'U').
card_image_name('island'/'CHK', 'island3').
card_uid('island'/'CHK', 'CHK:Island:island3').
card_rarity('island'/'CHK', 'Basic Land').
card_artist('island'/'CHK', 'Martina Pilcerova').
card_number('island'/'CHK', '293').
card_multiverse_id('island'/'CHK', '79071').

card_in_set('island', 'CHK').
card_original_type('island'/'CHK', 'Basic Land — Island').
card_original_text('island'/'CHK', 'U').
card_image_name('island'/'CHK', 'island4').
card_uid('island'/'CHK', 'CHK:Island:island4').
card_rarity('island'/'CHK', 'Basic Land').
card_artist('island'/'CHK', 'Martina Pilcerova').
card_number('island'/'CHK', '294').
card_multiverse_id('island'/'CHK', '79074').

card_in_set('jade idol', 'CHK').
card_original_type('jade idol'/'CHK', 'Artifact').
card_original_text('jade idol'/'CHK', 'Whenever you play a Spirit or Arcane spell, Jade Idol becomes a 4/4 Spirit artifact creature until end of turn.').
card_first_print('jade idol', 'CHK').
card_image_name('jade idol'/'CHK', 'jade idol').
card_uid('jade idol'/'CHK', 'CHK:Jade Idol:jade idol').
card_rarity('jade idol'/'CHK', 'Uncommon').
card_artist('jade idol'/'CHK', 'Ben Thompson').
card_number('jade idol'/'CHK', '256').
card_flavor_text('jade idol'/'CHK', 'Before the Kami War, the shishi were symbolic guardians, protecting the shrines where they stood. But after the kami turned on the material world, shishi began pouncing from their perches to attack would-be supplicants.').
card_multiverse_id('jade idol'/'CHK', '80531').

card_in_set('journeyer\'s kite', 'CHK').
card_original_type('journeyer\'s kite'/'CHK', 'Artifact').
card_original_text('journeyer\'s kite'/'CHK', '{3}, {T}: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.').
card_first_print('journeyer\'s kite', 'CHK').
card_image_name('journeyer\'s kite'/'CHK', 'journeyer\'s kite').
card_uid('journeyer\'s kite'/'CHK', 'CHK:Journeyer\'s Kite:journeyer\'s kite').
card_rarity('journeyer\'s kite'/'CHK', 'Rare').
card_artist('journeyer\'s kite'/'CHK', 'Hiro Izawa').
card_number('journeyer\'s kite'/'CHK', '257').
card_flavor_text('journeyer\'s kite'/'CHK', '\"From the clouds, you can see as far as the distant horizon. It\'s a reminder of the infinite possibilities of everyday life.\"\n—Noboru, master kitemaker').
card_multiverse_id('journeyer\'s kite'/'CHK', '79233').

card_in_set('joyous respite', 'CHK').
card_original_type('joyous respite'/'CHK', 'Sorcery — Arcane').
card_original_text('joyous respite'/'CHK', 'You gain 1 life for each land you control.').
card_first_print('joyous respite', 'CHK').
card_image_name('joyous respite'/'CHK', 'joyous respite').
card_uid('joyous respite'/'CHK', 'CHK:Joyous Respite:joyous respite').
card_rarity('joyous respite'/'CHK', 'Common').
card_artist('joyous respite'/'CHK', 'Rebecca Guay').
card_number('joyous respite'/'CHK', '216').
card_flavor_text('joyous respite'/'CHK', '\"I have been gifted by the kami with long life. So far, they have not seen fit to withdraw their gift. There may yet be a way to stop the killing without more blood, human or kami, being spilled.\"\n—Dosan the Falling Leaf').
card_multiverse_id('joyous respite'/'CHK', '78981').

card_in_set('jugan, the rising star', 'CHK').
card_original_type('jugan, the rising star'/'CHK', 'Legendary Creature — Dragon Spirit').
card_original_text('jugan, the rising star'/'CHK', 'Flying\nWhen Jugan, the Rising Star is put into a graveyard from play, you may distribute five +1/+1 counters among any number of target creatures.').
card_first_print('jugan, the rising star', 'CHK').
card_image_name('jugan, the rising star'/'CHK', 'jugan, the rising star').
card_uid('jugan, the rising star'/'CHK', 'CHK:Jugan, the Rising Star:jugan, the rising star').
card_rarity('jugan, the rising star'/'CHK', 'Rare').
card_artist('jugan, the rising star'/'CHK', 'Shishizaru').
card_number('jugan, the rising star'/'CHK', '217').
card_multiverse_id('jugan, the rising star'/'CHK', '50247').

card_in_set('jukai messenger', 'CHK').
card_original_type('jukai messenger'/'CHK', 'Creature — Human Monk').
card_original_text('jukai messenger'/'CHK', 'Forestwalk').
card_first_print('jukai messenger', 'CHK').
card_image_name('jukai messenger'/'CHK', 'jukai messenger').
card_uid('jukai messenger'/'CHK', 'CHK:Jukai Messenger:jukai messenger').
card_rarity('jukai messenger'/'CHK', 'Common').
card_artist('jukai messenger'/'CHK', 'Terese Nielsen').
card_number('jukai messenger'/'CHK', '218').
card_flavor_text('jukai messenger'/'CHK', 'Dosan\'s monks, deep in spiritual thought, could walk through the thickest of forests without leaving a mark, even the leaves beneath their feet unaffected by their passing.').
card_multiverse_id('jukai messenger'/'CHK', '80525').

card_in_set('junkyo bell', 'CHK').
card_original_type('junkyo bell'/'CHK', 'Artifact').
card_original_text('junkyo bell'/'CHK', 'At the beginning of your upkeep, you may have target creature you control get +X/+X until end of turn, where X is the number of creatures you control. If you do, sacrifice that creature at end of turn.').
card_first_print('junkyo bell', 'CHK').
card_image_name('junkyo bell'/'CHK', 'junkyo bell').
card_uid('junkyo bell'/'CHK', 'CHK:Junkyo Bell:junkyo bell').
card_rarity('junkyo bell'/'CHK', 'Rare').
card_artist('junkyo bell'/'CHK', 'Kensuke Okabayashi').
card_number('junkyo bell'/'CHK', '258').
card_multiverse_id('junkyo bell'/'CHK', '80280').

card_in_set('jushi apprentice', 'CHK').
card_original_type('jushi apprentice'/'CHK', 'Creature — Human Wizard').
card_original_text('jushi apprentice'/'CHK', '{2}{U}, {T}: Draw a card. If you have nine or more cards in hand, flip Jushi Apprentice.\n-----\nTomoya the Revealer\nLegendary Creature Human Wizard\n2/3\n{3}{U}{U}, {T}: Target player draws X cards, where X is the number of cards in your hand.').
card_first_print('jushi apprentice', 'CHK').
card_image_name('jushi apprentice'/'CHK', 'jushi apprentice').
card_uid('jushi apprentice'/'CHK', 'CHK:Jushi Apprentice:jushi apprentice').
card_rarity('jushi apprentice'/'CHK', 'Rare').
card_artist('jushi apprentice'/'CHK', 'Glen Angus').
card_number('jushi apprentice'/'CHK', '70a').
card_multiverse_id('jushi apprentice'/'CHK', '78686').

card_in_set('kabuto moth', 'CHK').
card_original_type('kabuto moth'/'CHK', 'Creature — Spirit').
card_original_text('kabuto moth'/'CHK', 'Flying\n{T}: Target creature gets +1/+2 until end of turn.').
card_first_print('kabuto moth', 'CHK').
card_image_name('kabuto moth'/'CHK', 'kabuto moth').
card_uid('kabuto moth'/'CHK', 'CHK:Kabuto Moth:kabuto moth').
card_rarity('kabuto moth'/'CHK', 'Common').
card_artist('kabuto moth'/'CHK', 'Tomas Giorello').
card_number('kabuto moth'/'CHK', '20').
card_flavor_text('kabuto moth'/'CHK', '\"Many great warriors died in the first days of the war, as the spirits of their weaponry turned against them with terrifying rage.\"\n—Observations of the Kami War').
card_multiverse_id('kabuto moth'/'CHK', '79105').

card_in_set('kami of ancient law', 'CHK').
card_original_type('kami of ancient law'/'CHK', 'Creature — Spirit').
card_original_text('kami of ancient law'/'CHK', 'Sacrifice Kami of Ancient Law: Destroy target enchantment.').
card_first_print('kami of ancient law', 'CHK').
card_image_name('kami of ancient law'/'CHK', 'kami of ancient law').
card_uid('kami of ancient law'/'CHK', 'CHK:Kami of Ancient Law:kami of ancient law').
card_rarity('kami of ancient law'/'CHK', 'Common').
card_artist('kami of ancient law'/'CHK', 'Mark Tedin').
card_number('kami of ancient law'/'CHK', '21').
card_flavor_text('kami of ancient law'/'CHK', '\"Duty and law are the foundation on which civilization stands. They must not fall, for when they do, they take everything with them.\"\n—Lord Konda').
card_multiverse_id('kami of ancient law'/'CHK', '50349').

card_in_set('kami of fire\'s roar', 'CHK').
card_original_type('kami of fire\'s roar'/'CHK', 'Creature — Spirit').
card_original_text('kami of fire\'s roar'/'CHK', 'Whenever you play a Spirit or Arcane spell, target creature can\'t block this turn.').
card_first_print('kami of fire\'s roar', 'CHK').
card_image_name('kami of fire\'s roar'/'CHK', 'kami of fire\'s roar').
card_uid('kami of fire\'s roar'/'CHK', 'CHK:Kami of Fire\'s Roar:kami of fire\'s roar').
card_rarity('kami of fire\'s roar'/'CHK', 'Common').
card_artist('kami of fire\'s roar'/'CHK', 'Dave Dorman').
card_number('kami of fire\'s roar'/'CHK', '174').
card_flavor_text('kami of fire\'s roar'/'CHK', '\"I can hear the shamans chanting in the hills. They say their magic will protect us from the kami, that our gold has bought our safety. But no one sleeps soundly tonight.\"\n—Scroll fragment from the ruins of Reito').
card_multiverse_id('kami of fire\'s roar'/'CHK', '79195').

card_in_set('kami of lunacy', 'CHK').
card_original_type('kami of lunacy'/'CHK', 'Creature — Spirit').
card_original_text('kami of lunacy'/'CHK', 'Flying\nSoulshift 5 (When this is put into a graveyard from play, you may return target Spirit card with converted mana cost 5 or less from your graveyard to your hand.)').
card_first_print('kami of lunacy', 'CHK').
card_image_name('kami of lunacy'/'CHK', 'kami of lunacy').
card_uid('kami of lunacy'/'CHK', 'CHK:Kami of Lunacy:kami of lunacy').
card_rarity('kami of lunacy'/'CHK', 'Uncommon').
card_artist('kami of lunacy'/'CHK', 'Daren Bader').
card_number('kami of lunacy'/'CHK', '119').
card_multiverse_id('kami of lunacy'/'CHK', '77921').

card_in_set('kami of old stone', 'CHK').
card_original_type('kami of old stone'/'CHK', 'Creature — Spirit').
card_original_text('kami of old stone'/'CHK', '').
card_first_print('kami of old stone', 'CHK').
card_image_name('kami of old stone'/'CHK', 'kami of old stone').
card_uid('kami of old stone'/'CHK', 'CHK:Kami of Old Stone:kami of old stone').
card_rarity('kami of old stone'/'CHK', 'Uncommon').
card_artist('kami of old stone'/'CHK', 'Stephen Tappin').
card_number('kami of old stone'/'CHK', '22').
card_flavor_text('kami of old stone'/'CHK', '\"There was a wall here. Now it is dust. A tower rose here. Now it is fallen. An army fought here. Now it is dead. A spirit was here. It is all that remains.\"\n—Snow-Fur, kitsune poet').
card_multiverse_id('kami of old stone'/'CHK', '79236').

card_in_set('kami of the hunt', 'CHK').
card_original_type('kami of the hunt'/'CHK', 'Creature — Spirit').
card_original_text('kami of the hunt'/'CHK', 'Whenever you play a Spirit or Arcane spell, Kami of the Hunt gets +1/+1 until end of turn.').
card_first_print('kami of the hunt', 'CHK').
card_image_name('kami of the hunt'/'CHK', 'kami of the hunt').
card_uid('kami of the hunt'/'CHK', 'CHK:Kami of the Hunt:kami of the hunt').
card_rarity('kami of the hunt'/'CHK', 'Common').
card_artist('kami of the hunt'/'CHK', 'Alex Horley-Orlandelli').
card_number('kami of the hunt'/'CHK', '219').
card_flavor_text('kami of the hunt'/'CHK', '\"Don\'t worry, Jiro. The kami would never attack us this close to home . . . . Jiro?\"\n—Hoto, temple guardian, last words').
card_multiverse_id('kami of the hunt'/'CHK', '79198').

card_in_set('kami of the painted road', 'CHK').
card_original_type('kami of the painted road'/'CHK', 'Creature — Spirit').
card_original_text('kami of the painted road'/'CHK', 'Whenever you play a Spirit or Arcane spell, Kami of the Painted Road gains protection from the color of your choice until end of turn.').
card_first_print('kami of the painted road', 'CHK').
card_image_name('kami of the painted road'/'CHK', 'kami of the painted road').
card_uid('kami of the painted road'/'CHK', 'CHK:Kami of the Painted Road:kami of the painted road').
card_rarity('kami of the painted road'/'CHK', 'Common').
card_artist('kami of the painted road'/'CHK', 'Ron Spencer').
card_number('kami of the painted road'/'CHK', '23').
card_flavor_text('kami of the painted road'/'CHK', 'In ancient times, precepts of kami law were inscribed onto bridges as a gesture of respect. During the war, humans regarded them as warnings of where not to travel.').
card_multiverse_id('kami of the painted road'/'CHK', '77136').

card_in_set('kami of the palace fields', 'CHK').
card_original_type('kami of the palace fields'/'CHK', 'Creature — Spirit').
card_original_text('kami of the palace fields'/'CHK', 'Flying, first strike\nSoulshift 5 (When this is put into a graveyard from play, you may return target Spirit card with converted mana cost 5 or less from your graveyard to your hand.)').
card_first_print('kami of the palace fields', 'CHK').
card_image_name('kami of the palace fields'/'CHK', 'kami of the palace fields').
card_uid('kami of the palace fields'/'CHK', 'CHK:Kami of the Palace Fields:kami of the palace fields').
card_rarity('kami of the palace fields'/'CHK', 'Uncommon').
card_artist('kami of the palace fields'/'CHK', 'Matt Cavotta').
card_number('kami of the palace fields'/'CHK', '24').
card_multiverse_id('kami of the palace fields'/'CHK', '50367').

card_in_set('kami of the waning moon', 'CHK').
card_original_type('kami of the waning moon'/'CHK', 'Creature — Spirit').
card_original_text('kami of the waning moon'/'CHK', 'Flying\nWhenever you play a Spirit or Arcane spell, target creature gains fear until end of turn.').
card_first_print('kami of the waning moon', 'CHK').
card_image_name('kami of the waning moon'/'CHK', 'kami of the waning moon').
card_uid('kami of the waning moon'/'CHK', 'CHK:Kami of the Waning Moon:kami of the waning moon').
card_rarity('kami of the waning moon'/'CHK', 'Common').
card_artist('kami of the waning moon'/'CHK', 'Matt Thompson').
card_number('kami of the waning moon'/'CHK', '120').
card_flavor_text('kami of the waning moon'/'CHK', '\"For a moment, Reito\'s defenders regrouped. Then wailing kami reappeared to send them scattering like flocks of frightened birds.\"\n—Great Battles of Kamigawa').
card_multiverse_id('kami of the waning moon'/'CHK', '50311').

card_in_set('kami of twisted reflection', 'CHK').
card_original_type('kami of twisted reflection'/'CHK', 'Creature — Spirit').
card_original_text('kami of twisted reflection'/'CHK', 'Sacrifice Kami of Twisted Reflection: Return target creature you control to its owner\'s hand.').
card_first_print('kami of twisted reflection', 'CHK').
card_image_name('kami of twisted reflection'/'CHK', 'kami of twisted reflection').
card_uid('kami of twisted reflection'/'CHK', 'CHK:Kami of Twisted Reflection:kami of twisted reflection').
card_rarity('kami of twisted reflection'/'CHK', 'Common').
card_artist('kami of twisted reflection'/'CHK', 'Mark Tedin').
card_number('kami of twisted reflection'/'CHK', '71').
card_flavor_text('kami of twisted reflection'/'CHK', 'Its form reflected humanity as it stood during the Kami War: disjointed, confused, and incomplete.').
card_multiverse_id('kami of twisted reflection'/'CHK', '50228').

card_in_set('kashi-tribe reaver', 'CHK').
card_original_type('kashi-tribe reaver'/'CHK', 'Creature — Snake Warrior').
card_original_text('kashi-tribe reaver'/'CHK', 'Whenever Kashi-Tribe Reaver deals combat damage to a creature, tap that creature and it doesn\'t untap during its controller\'s next untap step.\n{1}{G}: Regenerate Kashi-Tribe Reaver.').
card_first_print('kashi-tribe reaver', 'CHK').
card_image_name('kashi-tribe reaver'/'CHK', 'kashi-tribe reaver').
card_uid('kashi-tribe reaver'/'CHK', 'CHK:Kashi-Tribe Reaver:kashi-tribe reaver').
card_rarity('kashi-tribe reaver'/'CHK', 'Uncommon').
card_artist('kashi-tribe reaver'/'CHK', 'Anthony S. Waters').
card_number('kashi-tribe reaver'/'CHK', '220').
card_multiverse_id('kashi-tribe reaver'/'CHK', '76634').

card_in_set('kashi-tribe warriors', 'CHK').
card_original_type('kashi-tribe warriors'/'CHK', 'Creature — Snake Warrior').
card_original_text('kashi-tribe warriors'/'CHK', 'Whenever Kashi-Tribe Warriors deals combat damage to a creature, tap that creature and it doesn\'t untap during its controller\'s next untap step.').
card_first_print('kashi-tribe warriors', 'CHK').
card_image_name('kashi-tribe warriors'/'CHK', 'kashi-tribe warriors').
card_uid('kashi-tribe warriors'/'CHK', 'CHK:Kashi-Tribe Warriors:kashi-tribe warriors').
card_rarity('kashi-tribe warriors'/'CHK', 'Common').
card_artist('kashi-tribe warriors'/'CHK', 'Stephen Tappin').
card_number('kashi-tribe warriors'/'CHK', '221').
card_flavor_text('kashi-tribe warriors'/'CHK', 'The orochi and the monks had always had an unspoken agreement: live and let live. But when the kami began raging, some warrior youths began questioning that agreement.').
card_multiverse_id('kashi-tribe warriors'/'CHK', '50342').

card_in_set('keiga, the tide star', 'CHK').
card_original_type('keiga, the tide star'/'CHK', 'Legendary Creature — Dragon Spirit').
card_original_text('keiga, the tide star'/'CHK', 'Flying\nWhen Keiga, the Tide Star is put into a graveyard from play, gain control of target creature.').
card_first_print('keiga, the tide star', 'CHK').
card_image_name('keiga, the tide star'/'CHK', 'keiga, the tide star').
card_uid('keiga, the tide star'/'CHK', 'CHK:Keiga, the Tide Star:keiga, the tide star').
card_rarity('keiga, the tide star'/'CHK', 'Rare').
card_artist('keiga, the tide star'/'CHK', 'Ittoku').
card_number('keiga, the tide star'/'CHK', '72').
card_multiverse_id('keiga, the tide star'/'CHK', '75286').

card_in_set('kenzo the hardhearted', 'CHK').
card_original_type('kenzo the hardhearted'/'CHK', 'Creature — Human Soldier').
card_original_text('kenzo the hardhearted'/'CHK', 'When a creature dealt damage by Bushi Tenderfoot this turn is put into a graveyard, flip Bushi Tenderfoot.\n-----\nKenzo the Hardhearted\nLegendary Creature Human Samurai\n3/4\nDouble strike; bushido 2 (When this blocks or becomes blocked, it gets +2/+2 until end of turn.)').
card_first_print('kenzo the hardhearted', 'CHK').
card_image_name('kenzo the hardhearted'/'CHK', 'kenzo the hardhearted').
card_uid('kenzo the hardhearted'/'CHK', 'CHK:Kenzo the Hardhearted:kenzo the hardhearted').
card_rarity('kenzo the hardhearted'/'CHK', 'Uncommon').
card_artist('kenzo the hardhearted'/'CHK', 'Mark Zug').
card_number('kenzo the hardhearted'/'CHK', '2b').
card_multiverse_id('kenzo the hardhearted'/'CHK', '78600').

card_in_set('kiki-jiki, mirror breaker', 'CHK').
card_original_type('kiki-jiki, mirror breaker'/'CHK', 'Legendary Creature — Goblin Shaman').
card_original_text('kiki-jiki, mirror breaker'/'CHK', 'Haste\n{T}: Put a creature token into play that\'s a copy of target nonlegendary creature you control. That creature token has haste. Sacrifice it at end of turn.').
card_first_print('kiki-jiki, mirror breaker', 'CHK').
card_image_name('kiki-jiki, mirror breaker'/'CHK', 'kiki-jiki, mirror breaker').
card_uid('kiki-jiki, mirror breaker'/'CHK', 'CHK:Kiki-Jiki, Mirror Breaker:kiki-jiki, mirror breaker').
card_rarity('kiki-jiki, mirror breaker'/'CHK', 'Rare').
card_artist('kiki-jiki, mirror breaker'/'CHK', 'Pete Venters').
card_number('kiki-jiki, mirror breaker'/'CHK', '175').
card_multiverse_id('kiki-jiki, mirror breaker'/'CHK', '50321').

card_in_set('kiku, night\'s flower', 'CHK').
card_original_type('kiku, night\'s flower'/'CHK', 'Legendary Creature — Human Assassin').
card_original_text('kiku, night\'s flower'/'CHK', '{2}{B}{B}, {T}: Target creature deals damage to itself equal to its power.').
card_first_print('kiku, night\'s flower', 'CHK').
card_image_name('kiku, night\'s flower'/'CHK', 'kiku, night\'s flower').
card_uid('kiku, night\'s flower'/'CHK', 'CHK:Kiku, Night\'s Flower:kiku, night\'s flower').
card_rarity('kiku, night\'s flower'/'CHK', 'Rare').
card_artist('kiku, night\'s flower'/'CHK', 'Jim Murray').
card_number('kiku, night\'s flower'/'CHK', '121').
card_flavor_text('kiku, night\'s flower'/'CHK', '\"A wanderer has told me of an assassin in the Takenuma Swamp who uses her dark arts to animate her enemies\' shadows against them. A wild tale, but it explains much.\"\n—Diary of Azusa').
card_multiverse_id('kiku, night\'s flower'/'CHK', '78962').

card_in_set('kitsune blademaster', 'CHK').
card_original_type('kitsune blademaster'/'CHK', 'Creature — Fox Samurai').
card_original_text('kitsune blademaster'/'CHK', 'First strike\nBushido 1 (When this blocks or becomes blocked, it gets +1/+1 until end of turn.)').
card_first_print('kitsune blademaster', 'CHK').
card_image_name('kitsune blademaster'/'CHK', 'kitsune blademaster').
card_uid('kitsune blademaster'/'CHK', 'CHK:Kitsune Blademaster:kitsune blademaster').
card_rarity('kitsune blademaster'/'CHK', 'Common').
card_artist('kitsune blademaster'/'CHK', 'Keith Garletts').
card_number('kitsune blademaster'/'CHK', '25').
card_flavor_text('kitsune blademaster'/'CHK', 'Those kitsune trained in the blade preferred to fight with a blade-catching jitte in the off hand, buying them just enough time to deliver the first deadly cut.').
card_multiverse_id('kitsune blademaster'/'CHK', '50441').

card_in_set('kitsune diviner', 'CHK').
card_original_type('kitsune diviner'/'CHK', 'Creature — Fox Cleric').
card_original_text('kitsune diviner'/'CHK', '{T}: Tap target Spirit.').
card_first_print('kitsune diviner', 'CHK').
card_image_name('kitsune diviner'/'CHK', 'kitsune diviner').
card_uid('kitsune diviner'/'CHK', 'CHK:Kitsune Diviner:kitsune diviner').
card_rarity('kitsune diviner'/'CHK', 'Common').
card_artist('kitsune diviner'/'CHK', 'Pete Venters').
card_number('kitsune diviner'/'CHK', '26').
card_flavor_text('kitsune diviner'/'CHK', 'She who once led prayers honoring the kami adapted to use her influence to stay them.').
card_multiverse_id('kitsune diviner'/'CHK', '76652').

card_in_set('kitsune healer', 'CHK').
card_original_type('kitsune healer'/'CHK', 'Creature — Fox Cleric').
card_original_text('kitsune healer'/'CHK', '{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.\n{T}: Prevent all damage that would be dealt to target legendary creature this turn.').
card_first_print('kitsune healer', 'CHK').
card_image_name('kitsune healer'/'CHK', 'kitsune healer').
card_uid('kitsune healer'/'CHK', 'CHK:Kitsune Healer:kitsune healer').
card_rarity('kitsune healer'/'CHK', 'Common').
card_artist('kitsune healer'/'CHK', 'Michael Sutfin').
card_number('kitsune healer'/'CHK', '27').
card_multiverse_id('kitsune healer'/'CHK', '50437').

card_in_set('kitsune mystic', 'CHK').
card_original_type('kitsune mystic'/'CHK', 'Creature — Fox Wizard').
card_original_text('kitsune mystic'/'CHK', 'At end of turn, if Kitsune Mystic is enchanted by two or more enchantments, flip it.\n-----\nAutumn-Tail, Kitsune Sage\nLegendary Creature Fox Wizard\n4/5\n{1}: Move target enchantment enchanting a creature to another creature.').
card_first_print('kitsune mystic', 'CHK').
card_image_name('kitsune mystic'/'CHK', 'kitsune mystic').
card_uid('kitsune mystic'/'CHK', 'CHK:Kitsune Mystic:kitsune mystic').
card_rarity('kitsune mystic'/'CHK', 'Rare').
card_artist('kitsune mystic'/'CHK', 'Jim Murray').
card_number('kitsune mystic'/'CHK', '28a').
card_multiverse_id('kitsune mystic'/'CHK', '78695').

card_in_set('kitsune riftwalker', 'CHK').
card_original_type('kitsune riftwalker'/'CHK', 'Creature — Fox Wizard').
card_original_text('kitsune riftwalker'/'CHK', 'Protection from Spirits and from Arcane').
card_first_print('kitsune riftwalker', 'CHK').
card_image_name('kitsune riftwalker'/'CHK', 'kitsune riftwalker').
card_uid('kitsune riftwalker'/'CHK', 'CHK:Kitsune Riftwalker:kitsune riftwalker').
card_rarity('kitsune riftwalker'/'CHK', 'Common').
card_artist('kitsune riftwalker'/'CHK', 'Pete Venters').
card_number('kitsune riftwalker'/'CHK', '29').
card_flavor_text('kitsune riftwalker'/'CHK', 'The wake of his passage shoved aside the influence of the kami.').
card_multiverse_id('kitsune riftwalker'/'CHK', '50412').

card_in_set('kodama of the north tree', 'CHK').
card_original_type('kodama of the north tree'/'CHK', 'Legendary Creature — Spirit').
card_original_text('kodama of the north tree'/'CHK', 'Trample\nKodama of the North Tree can\'t be the target of spells or abilities.').
card_first_print('kodama of the north tree', 'CHK').
card_image_name('kodama of the north tree'/'CHK', 'kodama of the north tree').
card_uid('kodama of the north tree'/'CHK', 'CHK:Kodama of the North Tree:kodama of the north tree').
card_rarity('kodama of the north tree'/'CHK', 'Rare').
card_artist('kodama of the north tree'/'CHK', 'Shishizaru').
card_number('kodama of the north tree'/'CHK', '222').
card_flavor_text('kodama of the north tree'/'CHK', '\"The monks of the North Tree rarely saw their kodama until the Kami War, when it woke like a slumbering, angry bear.\"\n—\"Poem of the Five Trees\"').
card_multiverse_id('kodama of the north tree'/'CHK', '50488').

card_in_set('kodama of the south tree', 'CHK').
card_original_type('kodama of the south tree'/'CHK', 'Legendary Creature — Spirit').
card_original_text('kodama of the south tree'/'CHK', 'Whenever you play a Spirit or Arcane spell, each other creature you control gets +1/+1 and gains trample until end of turn.').
card_first_print('kodama of the south tree', 'CHK').
card_image_name('kodama of the south tree'/'CHK', 'kodama of the south tree').
card_uid('kodama of the south tree'/'CHK', 'CHK:Kodama of the South Tree:kodama of the south tree').
card_rarity('kodama of the south tree'/'CHK', 'Rare').
card_artist('kodama of the south tree'/'CHK', 'Ron Spears').
card_number('kodama of the south tree'/'CHK', '223').
card_flavor_text('kodama of the south tree'/'CHK', '\"The monks of the South Tree had always reveled beneath their kodama\'s friendly gaze. During the Kami War, this gaze became fierce and full of hate.\"\n—\"Poem of the Five Trees\"').
card_multiverse_id('kodama of the south tree'/'CHK', '80288').

card_in_set('kodama\'s might', 'CHK').
card_original_type('kodama\'s might'/'CHK', 'Instant — Arcane').
card_original_text('kodama\'s might'/'CHK', 'Target creature gets +2/+2 until end of turn.\nSplice onto Arcane {G} (As you play an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_first_print('kodama\'s might', 'CHK').
card_image_name('kodama\'s might'/'CHK', 'kodama\'s might').
card_uid('kodama\'s might'/'CHK', 'CHK:Kodama\'s Might:kodama\'s might').
card_rarity('kodama\'s might'/'CHK', 'Common').
card_artist('kodama\'s might'/'CHK', 'Terese Nielsen').
card_number('kodama\'s might'/'CHK', '224').
card_multiverse_id('kodama\'s might'/'CHK', '81702').

card_in_set('kodama\'s reach', 'CHK').
card_original_type('kodama\'s reach'/'CHK', 'Sorcery — Arcane').
card_original_text('kodama\'s reach'/'CHK', 'Search your library for two basic land cards, reveal those cards, and put one into play tapped and the other into your hand. Then shuffle your library.').
card_first_print('kodama\'s reach', 'CHK').
card_image_name('kodama\'s reach'/'CHK', 'kodama\'s reach').
card_uid('kodama\'s reach'/'CHK', 'CHK:Kodama\'s Reach:kodama\'s reach').
card_rarity('kodama\'s reach'/'CHK', 'Common').
card_artist('kodama\'s reach'/'CHK', 'Heather Hudson').
card_number('kodama\'s reach'/'CHK', '225').
card_flavor_text('kodama\'s reach'/'CHK', '\"The land grows only where the kami will it.\"\n—Dosan the Falling Leaf').
card_multiverse_id('kodama\'s reach'/'CHK', '50299').

card_in_set('kokusho, the evening star', 'CHK').
card_original_type('kokusho, the evening star'/'CHK', 'Legendary Creature — Dragon Spirit').
card_original_text('kokusho, the evening star'/'CHK', 'Flying\nWhen Kokusho, the Evening Star is put into a graveyard from play, each opponent loses 5 life. You gain life equal to the life lost this way.').
card_first_print('kokusho, the evening star', 'CHK').
card_image_name('kokusho, the evening star'/'CHK', 'kokusho, the evening star').
card_uid('kokusho, the evening star'/'CHK', 'CHK:Kokusho, the Evening Star:kokusho, the evening star').
card_rarity('kokusho, the evening star'/'CHK', 'Rare').
card_artist('kokusho, the evening star'/'CHK', 'Tsutomu Kawade').
card_number('kokusho, the evening star'/'CHK', '122').
card_multiverse_id('kokusho, the evening star'/'CHK', '50445').

card_in_set('konda\'s banner', 'CHK').
card_original_type('konda\'s banner'/'CHK', 'Legendary Artifact — Equipment').
card_original_text('konda\'s banner'/'CHK', 'Konda\'s Banner can be attached only to a legendary creature.\nCreatures that share a color with equipped creature get +1/+1.\nCreatures that share a creature type with equipped creature get +1/+1.\nEquip {2}').
card_first_print('konda\'s banner', 'CHK').
card_image_name('konda\'s banner'/'CHK', 'konda\'s banner').
card_uid('konda\'s banner'/'CHK', 'CHK:Konda\'s Banner:konda\'s banner').
card_rarity('konda\'s banner'/'CHK', 'Rare').
card_artist('konda\'s banner'/'CHK', 'Donato Giancola').
card_number('konda\'s banner'/'CHK', '259').
card_multiverse_id('konda\'s banner'/'CHK', '76637').

card_in_set('konda\'s hatamoto', 'CHK').
card_original_type('konda\'s hatamoto'/'CHK', 'Creature — Human Samurai').
card_original_text('konda\'s hatamoto'/'CHK', 'Bushido 1 (When this blocks or becomes blocked, it gets +1/+1 until end of turn.)\nAs long as you control a legendary Samurai, Konda\'s Hatamoto gets +1/+2 and has vigilance. (Attacking doesn\'t cause this creature to tap.)').
card_first_print('konda\'s hatamoto', 'CHK').
card_image_name('konda\'s hatamoto'/'CHK', 'konda\'s hatamoto').
card_uid('konda\'s hatamoto'/'CHK', 'CHK:Konda\'s Hatamoto:konda\'s hatamoto').
card_rarity('konda\'s hatamoto'/'CHK', 'Uncommon').
card_artist('konda\'s hatamoto'/'CHK', 'Lars Grant-West').
card_number('konda\'s hatamoto'/'CHK', '31').
card_multiverse_id('konda\'s hatamoto'/'CHK', '79201').

card_in_set('konda, lord of eiganjo', 'CHK').
card_original_type('konda, lord of eiganjo'/'CHK', 'Legendary Creature — Human Samurai').
card_original_text('konda, lord of eiganjo'/'CHK', 'Vigilance (Attacking doesn\'t cause this creature to tap.)\nBushido 5 (When this blocks or becomes blocked, it gets +5/+5 until end of turn.)\nKonda, Lord of Eiganjo is indestructible.').
card_first_print('konda, lord of eiganjo', 'CHK').
card_image_name('konda, lord of eiganjo'/'CHK', 'konda, lord of eiganjo').
card_uid('konda, lord of eiganjo'/'CHK', 'CHK:Konda, Lord of Eiganjo:konda, lord of eiganjo').
card_rarity('konda, lord of eiganjo'/'CHK', 'Rare').
card_artist('konda, lord of eiganjo'/'CHK', 'John Bolton').
card_number('konda, lord of eiganjo'/'CHK', '30').
card_multiverse_id('konda, lord of eiganjo'/'CHK', '78594').

card_in_set('kumano\'s pupils', 'CHK').
card_original_type('kumano\'s pupils'/'CHK', 'Creature — Human Shaman').
card_original_text('kumano\'s pupils'/'CHK', 'If a creature dealt damage by Kumano\'s Pupils this turn would be put into a graveyard, remove it from the game instead.').
card_first_print('kumano\'s pupils', 'CHK').
card_image_name('kumano\'s pupils'/'CHK', 'kumano\'s pupils').
card_uid('kumano\'s pupils'/'CHK', 'CHK:Kumano\'s Pupils:kumano\'s pupils').
card_rarity('kumano\'s pupils'/'CHK', 'Uncommon').
card_artist('kumano\'s pupils'/'CHK', 'Greg Hildebrandt').
card_number('kumano\'s pupils'/'CHK', '177').
card_flavor_text('kumano\'s pupils'/'CHK', '\"Long before he reluctantly joined the war, stories spread of Kumano\'s followers winning victories against the kami.\"\n—Observations of the Kami War').
card_multiverse_id('kumano\'s pupils'/'CHK', '75267').

card_in_set('kumano, master yamabushi', 'CHK').
card_original_type('kumano, master yamabushi'/'CHK', 'Legendary Creature — Human Shaman').
card_original_text('kumano, master yamabushi'/'CHK', '{1}{R}: Kumano, Master Yamabushi deals 1 damage to target creature or player.\nIf a creature dealt damage by Kumano this turn would be put into a graveyard, remove it from the game instead.').
card_first_print('kumano, master yamabushi', 'CHK').
card_image_name('kumano, master yamabushi'/'CHK', 'kumano, master yamabushi').
card_uid('kumano, master yamabushi'/'CHK', 'CHK:Kumano, Master Yamabushi:kumano, master yamabushi').
card_rarity('kumano, master yamabushi'/'CHK', 'Rare').
card_artist('kumano, master yamabushi'/'CHK', 'Adam Rex').
card_number('kumano, master yamabushi'/'CHK', '176').
card_multiverse_id('kumano, master yamabushi'/'CHK', '78964').

card_in_set('kuro, pitlord', 'CHK').
card_original_type('kuro, pitlord'/'CHK', 'Legendary Creature — Demon Spirit').
card_original_text('kuro, pitlord'/'CHK', 'At the beginning of your upkeep, sacrifice Kuro, Pitlord unless you pay {B}{B}{B}{B}.\nPay 1 life: Target creature gets -1/-1 until end of turn.').
card_first_print('kuro, pitlord', 'CHK').
card_image_name('kuro, pitlord'/'CHK', 'kuro, pitlord').
card_uid('kuro, pitlord'/'CHK', 'CHK:Kuro, Pitlord:kuro, pitlord').
card_rarity('kuro, pitlord'/'CHK', 'Rare').
card_artist('kuro, pitlord'/'CHK', 'Jon Foster').
card_number('kuro, pitlord'/'CHK', '123').
card_multiverse_id('kuro, pitlord'/'CHK', '82423').

card_in_set('kusari-gama', 'CHK').
card_original_type('kusari-gama'/'CHK', 'Artifact — Equipment').
card_original_text('kusari-gama'/'CHK', 'Equipped creature has \"{2}: This creature gets +1/+0 until end of turn.\"\nWhenever equipped creature deals damage to a blocking creature, Kusari-Gama deals that much damage to each other creature defending player controls.\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('kusari-gama', 'CHK').
card_image_name('kusari-gama'/'CHK', 'kusari-gama').
card_uid('kusari-gama'/'CHK', 'CHK:Kusari-Gama:kusari-gama').
card_rarity('kusari-gama'/'CHK', 'Rare').
card_artist('kusari-gama'/'CHK', 'Tomas Giorello').
card_number('kusari-gama'/'CHK', '260').
card_multiverse_id('kusari-gama'/'CHK', '75269').

card_in_set('lantern kami', 'CHK').
card_original_type('lantern kami'/'CHK', 'Creature — Spirit').
card_original_text('lantern kami'/'CHK', 'Flying').
card_first_print('lantern kami', 'CHK').
card_image_name('lantern kami'/'CHK', 'lantern kami').
card_uid('lantern kami'/'CHK', 'CHK:Lantern Kami:lantern kami').
card_rarity('lantern kami'/'CHK', 'Common').
card_artist('lantern kami'/'CHK', 'John Avon').
card_number('lantern kami'/'CHK', '32').
card_flavor_text('lantern kami'/'CHK', '\"Snuffing the light of a lantern without saying a small prayer is bad luck. Though its flame can only singe, its soul can repeatedly visit misery on one\'s house.\"\n—Teachings of Eight-and-a-Half-Tails').
card_multiverse_id('lantern kami'/'CHK', '75319').

card_in_set('lantern-lit graveyard', 'CHK').
card_original_type('lantern-lit graveyard'/'CHK', 'Land').
card_original_text('lantern-lit graveyard'/'CHK', '{T}: Add {1} to your mana pool.\n{T}: Add {B} or {R} to your mana pool. Lantern-Lit Graveyard doesn\'t untap during your next untap step.').
card_first_print('lantern-lit graveyard', 'CHK').
card_image_name('lantern-lit graveyard'/'CHK', 'lantern-lit graveyard').
card_uid('lantern-lit graveyard'/'CHK', 'CHK:Lantern-Lit Graveyard:lantern-lit graveyard').
card_rarity('lantern-lit graveyard'/'CHK', 'Uncommon').
card_artist('lantern-lit graveyard'/'CHK', 'John Avon').
card_number('lantern-lit graveyard'/'CHK', '278').
card_multiverse_id('lantern-lit graveyard'/'CHK', '79080').

card_in_set('lava spike', 'CHK').
card_original_type('lava spike'/'CHK', 'Sorcery — Arcane').
card_original_text('lava spike'/'CHK', 'Lava Spike deals 3 damage to target player.').
card_first_print('lava spike', 'CHK').
card_image_name('lava spike'/'CHK', 'lava spike').
card_uid('lava spike'/'CHK', 'CHK:Lava Spike:lava spike').
card_rarity('lava spike'/'CHK', 'Common').
card_artist('lava spike'/'CHK', 'Mark Tedin').
card_number('lava spike'/'CHK', '178').
card_flavor_text('lava spike'/'CHK', 'Some kami attacks during the war were rife with trickery, subterfuge, and subtlety, draining hope and pride from the mortal world. Other attacks were a lot more straightforward.').
card_multiverse_id('lava spike'/'CHK', '79084').

card_in_set('lifted by clouds', 'CHK').
card_original_type('lifted by clouds'/'CHK', 'Instant — Arcane').
card_original_text('lifted by clouds'/'CHK', 'Target creature gains flying until end of turn.\nSplice onto Arcane {1}{U} (As you play an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_first_print('lifted by clouds', 'CHK').
card_image_name('lifted by clouds'/'CHK', 'lifted by clouds').
card_uid('lifted by clouds'/'CHK', 'CHK:Lifted by Clouds:lifted by clouds').
card_rarity('lifted by clouds'/'CHK', 'Common').
card_artist('lifted by clouds'/'CHK', 'Darrell Riche').
card_number('lifted by clouds'/'CHK', '73').
card_multiverse_id('lifted by clouds'/'CHK', '50320').

card_in_set('long-forgotten gohei', 'CHK').
card_original_type('long-forgotten gohei'/'CHK', 'Artifact').
card_original_text('long-forgotten gohei'/'CHK', 'Arcane spells you play cost {1} less to play.\nSpirits you control get +1/+1.').
card_first_print('long-forgotten gohei', 'CHK').
card_image_name('long-forgotten gohei'/'CHK', 'long-forgotten gohei').
card_uid('long-forgotten gohei'/'CHK', 'CHK:Long-Forgotten Gohei:long-forgotten gohei').
card_rarity('long-forgotten gohei'/'CHK', 'Rare').
card_artist('long-forgotten gohei'/'CHK', 'Alan Pollack').
card_number('long-forgotten gohei'/'CHK', '261').
card_flavor_text('long-forgotten gohei'/'CHK', 'Long ago, the priests would wave the gohei to call down the gods. Now it lies forgotten, but the spirits still feel its pull.').
card_multiverse_id('long-forgotten gohei'/'CHK', '80532').

card_in_set('lure', 'CHK').
card_original_type('lure'/'CHK', 'Enchant Creature').
card_original_text('lure'/'CHK', 'All creatures able to block enchanted creature do so.').
card_image_name('lure'/'CHK', 'lure').
card_uid('lure'/'CHK', 'CHK:Lure:lure').
card_rarity('lure'/'CHK', 'Uncommon').
card_artist('lure'/'CHK', 'D. Alexander Gregory').
card_number('lure'/'CHK', '226').
card_flavor_text('lure'/'CHK', 'Jukai Forest was known for its secrets, which drew in many wayfarers and wizards. But it was those same secrets that kept most of those seekers from ever being heard from again.').
card_multiverse_id('lure'/'CHK', '79152').

card_in_set('mana seism', 'CHK').
card_original_type('mana seism'/'CHK', 'Sorcery').
card_original_text('mana seism'/'CHK', 'Sacrifice any number of lands. Add {1} to your mana pool for each land sacrificed this way.').
card_first_print('mana seism', 'CHK').
card_image_name('mana seism'/'CHK', 'mana seism').
card_uid('mana seism'/'CHK', 'CHK:Mana Seism:mana seism').
card_rarity('mana seism'/'CHK', 'Uncommon').
card_artist('mana seism'/'CHK', 'Edward P. Beard, Jr.').
card_number('mana seism'/'CHK', '179').
card_flavor_text('mana seism'/'CHK', '\"It is the nature of humanity not to worry about tomorrow, especially when there\'s a good chance they won\'t live to see the end of today.\"\n—Kiku, Night\'s Flower').
card_multiverse_id('mana seism'/'CHK', '79133').

card_in_set('marrow-gnawer', 'CHK').
card_original_type('marrow-gnawer'/'CHK', 'Legendary Creature — Rat Rogue').
card_original_text('marrow-gnawer'/'CHK', 'All Rats have fear.\n{T}, Sacrifice a Rat: Put X 1/1 black Rat creature tokens into play, where X is the number of Rats you control.').
card_first_print('marrow-gnawer', 'CHK').
card_image_name('marrow-gnawer'/'CHK', 'marrow-gnawer').
card_uid('marrow-gnawer'/'CHK', 'CHK:Marrow-Gnawer:marrow-gnawer').
card_rarity('marrow-gnawer'/'CHK', 'Rare').
card_artist('marrow-gnawer'/'CHK', 'Wayne Reynolds').
card_number('marrow-gnawer'/'CHK', '124').
card_flavor_text('marrow-gnawer'/'CHK', 'Marrow-Gnawer united three nezumi gangs when he slew their leaders in a single night. Now they call him their first lord.').
card_multiverse_id('marrow-gnawer'/'CHK', '50253').

card_in_set('masako the humorless', 'CHK').
card_original_type('masako the humorless'/'CHK', 'Legendary Creature — Human Advisor').
card_original_text('masako the humorless'/'CHK', 'You may play Masako the Humorless any time you could play an instant.\nTapped creatures you control may block as though they were untapped.').
card_first_print('masako the humorless', 'CHK').
card_image_name('masako the humorless'/'CHK', 'masako the humorless').
card_uid('masako the humorless'/'CHK', 'CHK:Masako the Humorless:masako the humorless').
card_rarity('masako the humorless'/'CHK', 'Rare').
card_artist('masako the humorless'/'CHK', 'Ben Thompson').
card_number('masako the humorless'/'CHK', '33').
card_flavor_text('masako the humorless'/'CHK', 'Konda\'s servants dared not neglect their duties for a moment under Masako\'s icy gaze, knowing that what she saw, Lord Konda would hear.').
card_multiverse_id('masako the humorless'/'CHK', '79215').

card_in_set('matsu-tribe decoy', 'CHK').
card_original_type('matsu-tribe decoy'/'CHK', 'Creature — Snake Warrior').
card_original_text('matsu-tribe decoy'/'CHK', '{2}{G}: Target creature blocks Matsu-Tribe Decoy this turn if able.\nWhenever Matsu-Tribe Decoy deals combat damage to a creature, tap that creature and it doesn\'t untap during its controller\'s next untap step.').
card_first_print('matsu-tribe decoy', 'CHK').
card_image_name('matsu-tribe decoy'/'CHK', 'matsu-tribe decoy').
card_uid('matsu-tribe decoy'/'CHK', 'CHK:Matsu-Tribe Decoy:matsu-tribe decoy').
card_rarity('matsu-tribe decoy'/'CHK', 'Common').
card_artist('matsu-tribe decoy'/'CHK', 'Alan Pollack').
card_number('matsu-tribe decoy'/'CHK', '227').
card_multiverse_id('matsu-tribe decoy'/'CHK', '75247').

card_in_set('meloku the clouded mirror', 'CHK').
card_original_type('meloku the clouded mirror'/'CHK', 'Legendary Creature — Moonfolk Wizard').
card_original_text('meloku the clouded mirror'/'CHK', 'Flying\n{1}, Return a land you control to its owner\'s hand: Put a 1/1 blue Illusion creature token with flying into play.').
card_first_print('meloku the clouded mirror', 'CHK').
card_image_name('meloku the clouded mirror'/'CHK', 'meloku the clouded mirror').
card_uid('meloku the clouded mirror'/'CHK', 'CHK:Meloku the Clouded Mirror:meloku the clouded mirror').
card_rarity('meloku the clouded mirror'/'CHK', 'Rare').
card_artist('meloku the clouded mirror'/'CHK', 'Scott M. Fischer').
card_number('meloku the clouded mirror'/'CHK', '74').
card_flavor_text('meloku the clouded mirror'/'CHK', 'He loved his cities in the clouds. When he traveled to the lands below, he brought many reminders of his home.').
card_multiverse_id('meloku the clouded mirror'/'CHK', '75268').

card_in_set('midnight covenant', 'CHK').
card_original_type('midnight covenant'/'CHK', 'Enchant Creature').
card_original_text('midnight covenant'/'CHK', 'Enchanted creature has \"{B}: This creature gets +1/+1 until end of turn.\"').
card_first_print('midnight covenant', 'CHK').
card_image_name('midnight covenant'/'CHK', 'midnight covenant').
card_uid('midnight covenant'/'CHK', 'CHK:Midnight Covenant:midnight covenant').
card_rarity('midnight covenant'/'CHK', 'Common').
card_artist('midnight covenant'/'CHK', 'Pete Venters').
card_number('midnight covenant'/'CHK', '125').
card_flavor_text('midnight covenant'/'CHK', '\"Not all mortals fought the kami. The ogres revered the oni, while some humans made pacts based upon empty promises.\"\n—The History of Kamigawa').
card_multiverse_id('midnight covenant'/'CHK', '50449').

card_in_set('minamo, school at water\'s edge', 'CHK').
card_original_type('minamo, school at water\'s edge'/'CHK', 'Legendary Land').
card_original_text('minamo, school at water\'s edge'/'CHK', '{T}: Add {U} to your mana pool.\n{U}, {T}: Untap target legendary permanent.').
card_first_print('minamo, school at water\'s edge', 'CHK').
card_image_name('minamo, school at water\'s edge'/'CHK', 'minamo, school at water\'s edge').
card_uid('minamo, school at water\'s edge'/'CHK', 'CHK:Minamo, School at Water\'s Edge:minamo, school at water\'s edge').
card_rarity('minamo, school at water\'s edge'/'CHK', 'Rare').
card_artist('minamo, school at water\'s edge'/'CHK', 'Jeremy Jarvis').
card_number('minamo, school at water\'s edge'/'CHK', '279').
card_flavor_text('minamo, school at water\'s edge'/'CHK', 'Its students graduate the school and enter history.').
card_multiverse_id('minamo, school at water\'s edge'/'CHK', '79179').

card_in_set('mindblaze', 'CHK').
card_original_type('mindblaze'/'CHK', 'Sorcery').
card_original_text('mindblaze'/'CHK', 'Name a nonland card and choose a number greater than 0. Target player reveals his or her library. If that library contains exactly the chosen number of the named card, Mindblaze deals 8 damage to that player. Then that player shuffles his or her library.').
card_first_print('mindblaze', 'CHK').
card_image_name('mindblaze'/'CHK', 'mindblaze').
card_uid('mindblaze'/'CHK', 'CHK:Mindblaze:mindblaze').
card_rarity('mindblaze'/'CHK', 'Rare').
card_artist('mindblaze'/'CHK', 'John Avon').
card_number('mindblaze'/'CHK', '180').
card_multiverse_id('mindblaze'/'CHK', '75248').

card_in_set('moonring mirror', 'CHK').
card_original_type('moonring mirror'/'CHK', 'Artifact').
card_original_text('moonring mirror'/'CHK', 'Whenever you draw a card, remove the top card of your library from the game face down.\nAt the beginning of your upkeep, you may remove your hand from the game face down. If you do, put into your hand all other cards you own removed from the game with Moonring Mirror.').
card_first_print('moonring mirror', 'CHK').
card_image_name('moonring mirror'/'CHK', 'moonring mirror').
card_uid('moonring mirror'/'CHK', 'CHK:Moonring Mirror:moonring mirror').
card_rarity('moonring mirror'/'CHK', 'Rare').
card_artist('moonring mirror'/'CHK', 'Christopher Rush').
card_number('moonring mirror'/'CHK', '262').
card_multiverse_id('moonring mirror'/'CHK', '73907').

card_in_set('moss kami', 'CHK').
card_original_type('moss kami'/'CHK', 'Creature — Spirit').
card_original_text('moss kami'/'CHK', 'Trample').
card_first_print('moss kami', 'CHK').
card_image_name('moss kami'/'CHK', 'moss kami').
card_uid('moss kami'/'CHK', 'CHK:Moss Kami:moss kami').
card_rarity('moss kami'/'CHK', 'Common').
card_artist('moss kami'/'CHK', 'Hugh Jamieson').
card_number('moss kami'/'CHK', '228').
card_flavor_text('moss kami'/'CHK', 'Kami manifest in the form of whatever they embody in the physical plane. Some tower in the shape of moss or bark, while others take forms beyond mortal imagining.').
card_multiverse_id('moss kami'/'CHK', '76641').

card_in_set('mothrider samurai', 'CHK').
card_original_type('mothrider samurai'/'CHK', 'Creature — Human Samurai').
card_original_text('mothrider samurai'/'CHK', 'Flying\nBushido 1 (When this blocks or becomes blocked, it gets +1/+1 until end of turn.)').
card_first_print('mothrider samurai', 'CHK').
card_image_name('mothrider samurai'/'CHK', 'mothrider samurai').
card_uid('mothrider samurai'/'CHK', 'CHK:Mothrider Samurai:mothrider samurai').
card_rarity('mothrider samurai'/'CHK', 'Common').
card_artist('mothrider samurai'/'CHK', 'Mark Zug').
card_number('mothrider samurai'/'CHK', '34').
card_flavor_text('mothrider samurai'/'CHK', 'When the night blossoms open, the wings of Eiganjo take flight.').
card_multiverse_id('mothrider samurai'/'CHK', '50385').

card_in_set('mountain', 'CHK').
card_original_type('mountain'/'CHK', 'Basic Land — Mountain').
card_original_text('mountain'/'CHK', 'R').
card_image_name('mountain'/'CHK', 'mountain1').
card_uid('mountain'/'CHK', 'CHK:Mountain:mountain1').
card_rarity('mountain'/'CHK', 'Basic Land').
card_artist('mountain'/'CHK', 'John Avon').
card_number('mountain'/'CHK', '299').
card_multiverse_id('mountain'/'CHK', '79064').

card_in_set('mountain', 'CHK').
card_original_type('mountain'/'CHK', 'Basic Land — Mountain').
card_original_text('mountain'/'CHK', 'R').
card_image_name('mountain'/'CHK', 'mountain2').
card_uid('mountain'/'CHK', 'CHK:Mountain:mountain2').
card_rarity('mountain'/'CHK', 'Basic Land').
card_artist('mountain'/'CHK', 'John Avon').
card_number('mountain'/'CHK', '300').
card_multiverse_id('mountain'/'CHK', '79070').

card_in_set('mountain', 'CHK').
card_original_type('mountain'/'CHK', 'Basic Land — Mountain').
card_original_text('mountain'/'CHK', 'R').
card_image_name('mountain'/'CHK', 'mountain3').
card_uid('mountain'/'CHK', 'CHK:Mountain:mountain3').
card_rarity('mountain'/'CHK', 'Basic Land').
card_artist('mountain'/'CHK', 'John Avon').
card_number('mountain'/'CHK', '301').
card_multiverse_id('mountain'/'CHK', '79075').

card_in_set('mountain', 'CHK').
card_original_type('mountain'/'CHK', 'Basic Land — Mountain').
card_original_text('mountain'/'CHK', 'R').
card_image_name('mountain'/'CHK', 'mountain4').
card_uid('mountain'/'CHK', 'CHK:Mountain:mountain4').
card_rarity('mountain'/'CHK', 'Basic Land').
card_artist('mountain'/'CHK', 'John Avon').
card_number('mountain'/'CHK', '302').
card_multiverse_id('mountain'/'CHK', '79077').

card_in_set('myojin of cleansing fire', 'CHK').
card_original_type('myojin of cleansing fire'/'CHK', 'Legendary Creature — Spirit').
card_original_text('myojin of cleansing fire'/'CHK', 'Myojin of Cleansing Fire comes into play with a divinity counter on it if you played it from your hand.\nMyojin of Cleansing Fire is indestructible as long as it has a divinity counter on it.\nRemove a divinity counter from Myojin of Cleansing Fire: Destroy each other creature.').
card_first_print('myojin of cleansing fire', 'CHK').
card_image_name('myojin of cleansing fire'/'CHK', 'myojin of cleansing fire').
card_uid('myojin of cleansing fire'/'CHK', 'CHK:Myojin of Cleansing Fire:myojin of cleansing fire').
card_rarity('myojin of cleansing fire'/'CHK', 'Rare').
card_artist('myojin of cleansing fire'/'CHK', 'Kev Walker').
card_number('myojin of cleansing fire'/'CHK', '35').
card_multiverse_id('myojin of cleansing fire'/'CHK', '78681').

card_in_set('myojin of infinite rage', 'CHK').
card_original_type('myojin of infinite rage'/'CHK', 'Legendary Creature — Spirit').
card_original_text('myojin of infinite rage'/'CHK', 'Myojin of Infinite Rage comes into play with a divinity counter on it if you played it from your hand.\nMyojin of Infinite Rage is indestructible as long as it has a divinity counter on it.\nRemove a divinity counter from Myojin of Infinite Rage: Destroy all lands.').
card_first_print('myojin of infinite rage', 'CHK').
card_image_name('myojin of infinite rage'/'CHK', 'myojin of infinite rage').
card_uid('myojin of infinite rage'/'CHK', 'CHK:Myojin of Infinite Rage:myojin of infinite rage').
card_rarity('myojin of infinite rage'/'CHK', 'Rare').
card_artist('myojin of infinite rage'/'CHK', 'Kev Walker').
card_number('myojin of infinite rage'/'CHK', '181').
card_multiverse_id('myojin of infinite rage'/'CHK', '78986').

card_in_set('myojin of life\'s web', 'CHK').
card_original_type('myojin of life\'s web'/'CHK', 'Legendary Creature — Spirit').
card_original_text('myojin of life\'s web'/'CHK', 'Myojin of Life\'s Web comes into play with a divinity counter on it if you played it from your hand.\nMyojin of Life\'s Web is indestructible as long as it has a divinity counter on it.\nRemove a divinity counter from Myojin of Life\'s Web: Put any number of creature cards from your hand into play.').
card_first_print('myojin of life\'s web', 'CHK').
card_image_name('myojin of life\'s web'/'CHK', 'myojin of life\'s web').
card_uid('myojin of life\'s web'/'CHK', 'CHK:Myojin of Life\'s Web:myojin of life\'s web').
card_rarity('myojin of life\'s web'/'CHK', 'Rare').
card_artist('myojin of life\'s web'/'CHK', 'Kev Walker').
card_number('myojin of life\'s web'/'CHK', '229').
card_multiverse_id('myojin of life\'s web'/'CHK', '78976').

card_in_set('myojin of night\'s reach', 'CHK').
card_original_type('myojin of night\'s reach'/'CHK', 'Legendary Creature — Spirit').
card_original_text('myojin of night\'s reach'/'CHK', 'Myojin of Night\'s Reach comes into play with a divinity counter on it if you played it from your hand.\nMyojin of Night\'s Reach is indestructible as long as it has a divinity counter on it.\nRemove a divinity counter from Myojin of Night\'s Reach: Each opponent discards his or her hand.').
card_first_print('myojin of night\'s reach', 'CHK').
card_image_name('myojin of night\'s reach'/'CHK', 'myojin of night\'s reach').
card_uid('myojin of night\'s reach'/'CHK', 'CHK:Myojin of Night\'s Reach:myojin of night\'s reach').
card_rarity('myojin of night\'s reach'/'CHK', 'Rare').
card_artist('myojin of night\'s reach'/'CHK', 'Kev Walker').
card_number('myojin of night\'s reach'/'CHK', '126').
card_multiverse_id('myojin of night\'s reach'/'CHK', '79112').

card_in_set('myojin of seeing winds', 'CHK').
card_original_type('myojin of seeing winds'/'CHK', 'Legendary Creature — Spirit').
card_original_text('myojin of seeing winds'/'CHK', 'Myojin of Seeing Winds comes into play with a divinity counter on it if you played it from your hand.\nMyojin of Seeing Winds is indestructible as long as it has a divinity counter on it.\nRemove a divinity counter from Myojin of Seeing Winds: Draw a card for each permanent you control.').
card_first_print('myojin of seeing winds', 'CHK').
card_image_name('myojin of seeing winds'/'CHK', 'myojin of seeing winds').
card_uid('myojin of seeing winds'/'CHK', 'CHK:Myojin of Seeing Winds:myojin of seeing winds').
card_rarity('myojin of seeing winds'/'CHK', 'Rare').
card_artist('myojin of seeing winds'/'CHK', 'Kev Walker').
card_number('myojin of seeing winds'/'CHK', '75').
card_multiverse_id('myojin of seeing winds'/'CHK', '78608').

card_in_set('mystic restraints', 'CHK').
card_original_type('mystic restraints'/'CHK', 'Enchant Creature').
card_original_text('mystic restraints'/'CHK', 'You may play Mystic Restraints any time you could play an instant.\nWhen Mystic Restraints comes into play, tap enchanted creature.\nEnchanted creature doesn\'t untap during its controller\'s untap step.').
card_first_print('mystic restraints', 'CHK').
card_image_name('mystic restraints'/'CHK', 'mystic restraints').
card_uid('mystic restraints'/'CHK', 'CHK:Mystic Restraints:mystic restraints').
card_rarity('mystic restraints'/'CHK', 'Common').
card_artist('mystic restraints'/'CHK', 'Christopher Rush').
card_number('mystic restraints'/'CHK', '76').
card_multiverse_id('mystic restraints'/'CHK', '79188').

card_in_set('nagao, bound by honor', 'CHK').
card_original_type('nagao, bound by honor'/'CHK', 'Legendary Creature — Human Samurai').
card_original_text('nagao, bound by honor'/'CHK', 'Bushido 1 (When this blocks or becomes blocked, it gets +1/+1 until end of turn.)\nWhenever Nagao, Bound by Honor attacks, Samurai you control get +1/+1 until end of turn.').
card_first_print('nagao, bound by honor', 'CHK').
card_image_name('nagao, bound by honor'/'CHK', 'nagao, bound by honor').
card_uid('nagao, bound by honor'/'CHK', 'CHK:Nagao, Bound by Honor:nagao, bound by honor').
card_rarity('nagao, bound by honor'/'CHK', 'Uncommon').
card_artist('nagao, bound by honor'/'CHK', 'Dave Dorman').
card_number('nagao, bound by honor'/'CHK', '36').
card_multiverse_id('nagao, bound by honor'/'CHK', '79136').

card_in_set('nature\'s will', 'CHK').
card_original_type('nature\'s will'/'CHK', 'Enchantment').
card_original_text('nature\'s will'/'CHK', 'Whenever one or more creatures you control deal combat damage to a player, tap all lands that player controls and untap all lands you control.').
card_first_print('nature\'s will', 'CHK').
card_image_name('nature\'s will'/'CHK', 'nature\'s will').
card_uid('nature\'s will'/'CHK', 'CHK:Nature\'s Will:nature\'s will').
card_rarity('nature\'s will'/'CHK', 'Rare').
card_artist('nature\'s will'/'CHK', 'Mitch Cotie').
card_number('nature\'s will'/'CHK', '230').
card_flavor_text('nature\'s will'/'CHK', '\"Without the kami to speak to nature on our behalf, we must beg help from nature directly.\"\n—Seshiro the Anointed').
card_multiverse_id('nature\'s will'/'CHK', '80527').

card_in_set('nezumi bone-reader', 'CHK').
card_original_type('nezumi bone-reader'/'CHK', 'Creature — Rat Shaman').
card_original_text('nezumi bone-reader'/'CHK', '{B}, Sacrifice a creature: Target player discards a card. Play this ability only any time you could play a sorcery.').
card_first_print('nezumi bone-reader', 'CHK').
card_image_name('nezumi bone-reader'/'CHK', 'nezumi bone-reader').
card_uid('nezumi bone-reader'/'CHK', 'CHK:Nezumi Bone-Reader:nezumi bone-reader').
card_rarity('nezumi bone-reader'/'CHK', 'Uncommon').
card_artist('nezumi bone-reader'/'CHK', 'Dan Scott').
card_number('nezumi bone-reader'/'CHK', '127').
card_flavor_text('nezumi bone-reader'/'CHK', 'Four generations ago, old Split-Tail stole a bundle of scrolls from the Minamo School. Soon after, the first nezumi shamans were born.').
card_multiverse_id('nezumi bone-reader'/'CHK', '79857').

card_in_set('nezumi cutthroat', 'CHK').
card_original_type('nezumi cutthroat'/'CHK', 'Creature — Rat Warrior').
card_original_text('nezumi cutthroat'/'CHK', 'Fear\nNezumi Cutthroat can\'t block.').
card_first_print('nezumi cutthroat', 'CHK').
card_image_name('nezumi cutthroat'/'CHK', 'nezumi cutthroat').
card_uid('nezumi cutthroat'/'CHK', 'CHK:Nezumi Cutthroat:nezumi cutthroat').
card_rarity('nezumi cutthroat'/'CHK', 'Common').
card_artist('nezumi cutthroat'/'CHK', 'Carl Critchlow').
card_number('nezumi cutthroat'/'CHK', '128').
card_flavor_text('nezumi cutthroat'/'CHK', '\"These nezumi, they disgust me. The things they will do for money no other thinking creature would consider. This, of course, makes them useful beyond words.\"\n—Meloku the Clouded Mirror').
card_multiverse_id('nezumi cutthroat'/'CHK', '80521').

card_in_set('nezumi graverobber', 'CHK').
card_original_type('nezumi graverobber'/'CHK', 'Creature — Rat Rogue').
card_original_text('nezumi graverobber'/'CHK', '{1}{B}: Remove target card in an opponent\'s graveyard from the game. If no cards are in that graveyard, flip Nezumi Graverobber.\n-----\nNighteyes the Desecrator\nLegendary Creature Rat Wizard\n4/2\n{4}{B}: Put target creature card in a graveyard into play under your control.').
card_first_print('nezumi graverobber', 'CHK').
card_image_name('nezumi graverobber'/'CHK', 'nezumi graverobber').
card_uid('nezumi graverobber'/'CHK', 'CHK:Nezumi Graverobber:nezumi graverobber').
card_rarity('nezumi graverobber'/'CHK', 'Uncommon').
card_artist('nezumi graverobber'/'CHK', 'Jim Nelson').
card_number('nezumi graverobber'/'CHK', '129a').
card_multiverse_id('nezumi graverobber'/'CHK', '78678').

card_in_set('nezumi ronin', 'CHK').
card_original_type('nezumi ronin'/'CHK', 'Creature — Rat Samurai').
card_original_text('nezumi ronin'/'CHK', 'Bushido 1 (When this blocks or becomes blocked, it gets +1/+1 until end of turn.)').
card_first_print('nezumi ronin', 'CHK').
card_image_name('nezumi ronin'/'CHK', 'nezumi ronin').
card_uid('nezumi ronin'/'CHK', 'CHK:Nezumi Ronin:nezumi ronin').
card_rarity('nezumi ronin'/'CHK', 'Common').
card_artist('nezumi ronin'/'CHK', 'Scott M. Fischer').
card_number('nezumi ronin'/'CHK', '130').
card_flavor_text('nezumi ronin'/'CHK', '\"Some nezumi became as skilled in the samurai arts as the humans and kitsune. Yet no lord would have them, so they sold their swords to the highest bidder.\"\n—The History of Kamigawa').
card_multiverse_id('nezumi ronin'/'CHK', '50512').

card_in_set('nezumi shortfang', 'CHK').
card_original_type('nezumi shortfang'/'CHK', 'Creature — Rat Rogue').
card_original_text('nezumi shortfang'/'CHK', '{1}{B}, {T}: Target opponent discards a card. Then if that player has no cards in hand, flip Nezumi Shortfang.\n-----\nStabwhisker the Odious\nLegendary Creature Rat Shaman\n3/3\nAt the beginning of each opponent\'s upkeep, that player loses 1 life for each card fewer than three in his or her hand.').
card_first_print('nezumi shortfang', 'CHK').
card_image_name('nezumi shortfang'/'CHK', 'nezumi shortfang').
card_uid('nezumi shortfang'/'CHK', 'CHK:Nezumi Shortfang:nezumi shortfang').
card_rarity('nezumi shortfang'/'CHK', 'Rare').
card_artist('nezumi shortfang'/'CHK', 'Daren Bader').
card_number('nezumi shortfang'/'CHK', '131a').
card_multiverse_id('nezumi shortfang'/'CHK', '78679').

card_in_set('night dealings', 'CHK').
card_original_type('night dealings'/'CHK', 'Enchantment').
card_original_text('night dealings'/'CHK', 'Whenever a source you control deals damage to another player, put that many theft counters on Night Dealings.\n{2}{B}{B}, Remove X theft counters from Night Dealings: Search your library for a nonland card with converted mana cost X, reveal it, and put it into your hand. Then shuffle your library.').
card_first_print('night dealings', 'CHK').
card_image_name('night dealings'/'CHK', 'night dealings').
card_uid('night dealings'/'CHK', 'CHK:Night Dealings:night dealings').
card_rarity('night dealings'/'CHK', 'Rare').
card_artist('night dealings'/'CHK', 'Darrell Riche').
card_number('night dealings'/'CHK', '132').
card_multiverse_id('night dealings'/'CHK', '75334').

card_in_set('night of souls\' betrayal', 'CHK').
card_original_type('night of souls\' betrayal'/'CHK', 'Legendary Enchantment').
card_original_text('night of souls\' betrayal'/'CHK', 'All creatures get -1/-1.').
card_first_print('night of souls\' betrayal', 'CHK').
card_image_name('night of souls\' betrayal'/'CHK', 'night of souls\' betrayal').
card_uid('night of souls\' betrayal'/'CHK', 'CHK:Night of Souls\' Betrayal:night of souls\' betrayal').
card_rarity('night of souls\' betrayal'/'CHK', 'Rare').
card_artist('night of souls\' betrayal'/'CHK', 'Greg Staples').
card_number('night of souls\' betrayal'/'CHK', '133').
card_flavor_text('night of souls\' betrayal'/'CHK', '\"How can we wage war against ourselves? What happens when the kami of our very souls rise against us? I answer simply: We cannot. We die. There can be no victory in this war.\"\n—Sensei Hisoka, letter to Lord Konda').
card_multiverse_id('night of souls\' betrayal'/'CHK', '78991').

card_in_set('nighteyes the desecrator', 'CHK').
card_original_type('nighteyes the desecrator'/'CHK', 'Creature — Rat Rogue').
card_original_text('nighteyes the desecrator'/'CHK', '{1}{B}: Remove target card in an opponent\'s graveyard from the game. If no cards are in that graveyard, flip Nezumi Graverobber.\n-----\nNighteyes the Desecrator\nLegendary Creature Rat Wizard\n4/2\n{4}{B}: Put target creature card in a graveyard into play under your control.').
card_first_print('nighteyes the desecrator', 'CHK').
card_image_name('nighteyes the desecrator'/'CHK', 'nighteyes the desecrator').
card_uid('nighteyes the desecrator'/'CHK', 'CHK:Nighteyes the Desecrator:nighteyes the desecrator').
card_rarity('nighteyes the desecrator'/'CHK', 'Uncommon').
card_artist('nighteyes the desecrator'/'CHK', 'Jim Nelson').
card_number('nighteyes the desecrator'/'CHK', '129b').
card_multiverse_id('nighteyes the desecrator'/'CHK', '78678').

card_in_set('nine-ringed bo', 'CHK').
card_original_type('nine-ringed bo'/'CHK', 'Artifact').
card_original_text('nine-ringed bo'/'CHK', '{T}: Nine-Ringed Bo deals 1 damage to target Spirit. If that creature would be put into a graveyard this turn, remove it from the game instead.').
card_first_print('nine-ringed bo', 'CHK').
card_image_name('nine-ringed bo'/'CHK', 'nine-ringed bo').
card_uid('nine-ringed bo'/'CHK', 'CHK:Nine-Ringed Bo:nine-ringed bo').
card_rarity('nine-ringed bo'/'CHK', 'Uncommon').
card_artist('nine-ringed bo'/'CHK', 'Ralph Horsley').
card_number('nine-ringed bo'/'CHK', '263').
card_flavor_text('nine-ringed bo'/'CHK', '\"We received an anonymous letter suggesting that Kumano holds a secret to defeating the kami, but he is nowhere to be found.\"\n—General Takeno, letter to Lord Konda').
card_multiverse_id('nine-ringed bo'/'CHK', '50413').

card_in_set('no-dachi', 'CHK').
card_original_type('no-dachi'/'CHK', 'Artifact — Equipment').
card_original_text('no-dachi'/'CHK', 'Equipped creature gets +2/+0 and has first strike.\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('no-dachi', 'CHK').
card_image_name('no-dachi'/'CHK', 'no-dachi').
card_uid('no-dachi'/'CHK', 'CHK:No-Dachi:no-dachi').
card_rarity('no-dachi'/'CHK', 'Uncommon').
card_artist('no-dachi'/'CHK', 'Christopher Rush').
card_number('no-dachi'/'CHK', '264').
card_flavor_text('no-dachi'/'CHK', 'Not keen-edged enough for cutting armor, the no-dachi slew by folding its victim rapidly and violently under the weight of its impact.').
card_multiverse_id('no-dachi'/'CHK', '75265').

card_in_set('numai outcast', 'CHK').
card_original_type('numai outcast'/'CHK', 'Creature — Human Samurai').
card_original_text('numai outcast'/'CHK', 'Bushido 2 (When this blocks or becomes blocked, it gets +2/+2 until end of turn.)\n{B}, Pay 5 life: Regenerate Numai Outcast.').
card_first_print('numai outcast', 'CHK').
card_image_name('numai outcast'/'CHK', 'numai outcast').
card_uid('numai outcast'/'CHK', 'CHK:Numai Outcast:numai outcast').
card_rarity('numai outcast'/'CHK', 'Uncommon').
card_artist('numai outcast'/'CHK', 'Adam Rex').
card_number('numai outcast'/'CHK', '134').
card_flavor_text('numai outcast'/'CHK', '\"Beware the blade of dishonor. It kills more silently than war, more quickly than age.\"\n—Sensei Golden-Tail').
card_multiverse_id('numai outcast'/'CHK', '50431').

card_in_set('oathkeeper, takeno\'s daisho', 'CHK').
card_original_type('oathkeeper, takeno\'s daisho'/'CHK', 'Legendary Artifact — Equipment').
card_original_text('oathkeeper, takeno\'s daisho'/'CHK', 'Equipped creature gets +3/+1.\nWhenever equipped creature is put into a graveyard from play, return that card to play under your control if it\'s a Samurai.\nWhen Oathkeeper, Takeno\'s Daisho is put into a graveyard from play, remove equipped creature from the game.\nEquip {2}').
card_first_print('oathkeeper, takeno\'s daisho', 'CHK').
card_image_name('oathkeeper, takeno\'s daisho'/'CHK', 'oathkeeper, takeno\'s daisho').
card_uid('oathkeeper, takeno\'s daisho'/'CHK', 'CHK:Oathkeeper, Takeno\'s Daisho:oathkeeper, takeno\'s daisho').
card_rarity('oathkeeper, takeno\'s daisho'/'CHK', 'Rare').
card_artist('oathkeeper, takeno\'s daisho'/'CHK', 'Arnie Swekel').
card_number('oathkeeper, takeno\'s daisho'/'CHK', '265').
card_multiverse_id('oathkeeper, takeno\'s daisho'/'CHK', '50484').

card_in_set('okina, temple to the grandfathers', 'CHK').
card_original_type('okina, temple to the grandfathers'/'CHK', 'Legendary Land').
card_original_text('okina, temple to the grandfathers'/'CHK', '{T}: Add {G} to your mana pool.\n{G}, {T}: Target legendary creature gets +1/+1 until end of turn.').
card_first_print('okina, temple to the grandfathers', 'CHK').
card_image_name('okina, temple to the grandfathers'/'CHK', 'okina, temple to the grandfathers').
card_uid('okina, temple to the grandfathers'/'CHK', 'CHK:Okina, Temple to the Grandfathers:okina, temple to the grandfathers').
card_rarity('okina, temple to the grandfathers'/'CHK', 'Rare').
card_artist('okina, temple to the grandfathers'/'CHK', 'Keith Garletts').
card_number('okina, temple to the grandfathers'/'CHK', '280').
card_flavor_text('okina, temple to the grandfathers'/'CHK', 'If a land can be said to have a heart, Okina is the heart of Kamigawa.').
card_multiverse_id('okina, temple to the grandfathers'/'CHK', '80251').

card_in_set('oni possession', 'CHK').
card_original_type('oni possession'/'CHK', 'Enchant Creature').
card_original_text('oni possession'/'CHK', 'At the beginning of your upkeep, sacrifice a creature.\nEnchanted creature gets +3/+3 and has trample.\nEnchanted creature is a Demon Spirit.').
card_first_print('oni possession', 'CHK').
card_image_name('oni possession'/'CHK', 'oni possession').
card_uid('oni possession'/'CHK', 'CHK:Oni Possession:oni possession').
card_rarity('oni possession'/'CHK', 'Uncommon').
card_artist('oni possession'/'CHK', 'Aleksi Briclot').
card_number('oni possession'/'CHK', '135').
card_multiverse_id('oni possession'/'CHK', '50394').

card_in_set('orbweaver kumo', 'CHK').
card_original_type('orbweaver kumo'/'CHK', 'Creature — Spirit').
card_original_text('orbweaver kumo'/'CHK', 'Orbweaver Kumo may block as though it had flying.\nWhenever you play a Spirit or Arcane spell, Orbweaver Kumo gains forestwalk until end of turn.').
card_first_print('orbweaver kumo', 'CHK').
card_image_name('orbweaver kumo'/'CHK', 'orbweaver kumo').
card_uid('orbweaver kumo'/'CHK', 'CHK:Orbweaver Kumo:orbweaver kumo').
card_rarity('orbweaver kumo'/'CHK', 'Uncommon').
card_artist('orbweaver kumo'/'CHK', 'Dan Scott').
card_number('orbweaver kumo'/'CHK', '231').
card_multiverse_id('orbweaver kumo'/'CHK', '50261').

card_in_set('order of the sacred bell', 'CHK').
card_original_type('order of the sacred bell'/'CHK', 'Creature — Human Monk').
card_original_text('order of the sacred bell'/'CHK', '').
card_first_print('order of the sacred bell', 'CHK').
card_image_name('order of the sacred bell'/'CHK', 'order of the sacred bell').
card_uid('order of the sacred bell'/'CHK', 'CHK:Order of the Sacred Bell:order of the sacred bell').
card_rarity('order of the sacred bell'/'CHK', 'Common').
card_artist('order of the sacred bell'/'CHK', 'Carl Critchlow').
card_number('order of the sacred bell'/'CHK', '232').
card_flavor_text('order of the sacred bell'/'CHK', '\"Our bodies are weak compared to those of the kami, our knowledge limited, and our magic poor. Yet we draw breath. That alone is reason to hope.\"').
card_multiverse_id('order of the sacred bell'/'CHK', '50487').

card_in_set('ore gorger', 'CHK').
card_original_type('ore gorger'/'CHK', 'Creature — Spirit').
card_original_text('ore gorger'/'CHK', 'Whenever you play a Spirit or Arcane spell, you may destroy target nonbasic land.').
card_first_print('ore gorger', 'CHK').
card_image_name('ore gorger'/'CHK', 'ore gorger').
card_uid('ore gorger'/'CHK', 'CHK:Ore Gorger:ore gorger').
card_rarity('ore gorger'/'CHK', 'Uncommon').
card_artist('ore gorger'/'CHK', 'rk post').
card_number('ore gorger'/'CHK', '182').
card_flavor_text('ore gorger'/'CHK', '\"We\'ve stumbled upon a network of caves not on our maps. We can only hope it is safe to spend the night.\"\n—Lost Battalion, message to General Takeno').
card_multiverse_id('ore gorger'/'CHK', '78984').

card_in_set('orochi eggwatcher', 'CHK').
card_original_type('orochi eggwatcher'/'CHK', 'Creature — Snake Shaman').
card_original_text('orochi eggwatcher'/'CHK', '{2}{G}, {T}: Put a 1/1 green Snake creature token into play. If you control ten or more creatures, flip Orochi Eggwatcher.\n-----\nShidako, Broodmistress\nLegendary Creature Snake Shaman\n3/3\n{G}, Sacrifice a creature: Target creature gets +3/+3 until end of turn.').
card_first_print('orochi eggwatcher', 'CHK').
card_image_name('orochi eggwatcher'/'CHK', 'orochi eggwatcher').
card_uid('orochi eggwatcher'/'CHK', 'CHK:Orochi Eggwatcher:orochi eggwatcher').
card_rarity('orochi eggwatcher'/'CHK', 'Uncommon').
card_artist('orochi eggwatcher'/'CHK', 'Dan Scott').
card_number('orochi eggwatcher'/'CHK', '233a').
card_multiverse_id('orochi eggwatcher'/'CHK', '78975').

card_in_set('orochi hatchery', 'CHK').
card_original_type('orochi hatchery'/'CHK', 'Artifact').
card_original_text('orochi hatchery'/'CHK', 'Orochi Hatchery comes into play with X charge counters on it.\n{5}, {T}: Put a 1/1 green Snake creature token into play for each charge counter on Orochi Hatchery.').
card_first_print('orochi hatchery', 'CHK').
card_image_name('orochi hatchery'/'CHK', 'orochi hatchery').
card_uid('orochi hatchery'/'CHK', 'CHK:Orochi Hatchery:orochi hatchery').
card_rarity('orochi hatchery'/'CHK', 'Rare').
card_artist('orochi hatchery'/'CHK', 'Alex Horley-Orlandelli').
card_number('orochi hatchery'/'CHK', '266').
card_multiverse_id('orochi hatchery'/'CHK', '80241').

card_in_set('orochi leafcaller', 'CHK').
card_original_type('orochi leafcaller'/'CHK', 'Creature — Snake Shaman').
card_original_text('orochi leafcaller'/'CHK', '{G}: Add one mana of any color to your mana pool.').
card_first_print('orochi leafcaller', 'CHK').
card_image_name('orochi leafcaller'/'CHK', 'orochi leafcaller').
card_uid('orochi leafcaller'/'CHK', 'CHK:Orochi Leafcaller:orochi leafcaller').
card_rarity('orochi leafcaller'/'CHK', 'Common').
card_artist('orochi leafcaller'/'CHK', 'Joel Thomas').
card_number('orochi leafcaller'/'CHK', '234').
card_flavor_text('orochi leafcaller'/'CHK', '\"Only orochi hatched from eggs ‘touched by the kami\' may become shamans. These days, there are fewer and fewer new shamans being born.\"').
card_multiverse_id('orochi leafcaller'/'CHK', '50319').

card_in_set('orochi ranger', 'CHK').
card_original_type('orochi ranger'/'CHK', 'Creature — Snake Warrior').
card_original_text('orochi ranger'/'CHK', 'Whenever Orochi Ranger deals combat damage to a creature, tap that creature and it doesn\'t untap during its controller\'s next untap step.').
card_first_print('orochi ranger', 'CHK').
card_image_name('orochi ranger'/'CHK', 'orochi ranger').
card_uid('orochi ranger'/'CHK', 'CHK:Orochi Ranger:orochi ranger').
card_rarity('orochi ranger'/'CHK', 'Common').
card_artist('orochi ranger'/'CHK', 'Greg Hildebrandt').
card_number('orochi ranger'/'CHK', '235').
card_flavor_text('orochi ranger'/'CHK', '\"The young come to me, confused. They have been taught to respect the kami, and now they must fight them? I do not know what to say.\"\n—Sachi, to her father').
card_multiverse_id('orochi ranger'/'CHK', '50308').

card_in_set('orochi sustainer', 'CHK').
card_original_type('orochi sustainer'/'CHK', 'Creature — Snake Shaman').
card_original_text('orochi sustainer'/'CHK', '{T}: Add {G} to your mana pool.').
card_first_print('orochi sustainer', 'CHK').
card_image_name('orochi sustainer'/'CHK', 'orochi sustainer').
card_uid('orochi sustainer'/'CHK', 'CHK:Orochi Sustainer:orochi sustainer').
card_rarity('orochi sustainer'/'CHK', 'Common').
card_artist('orochi sustainer'/'CHK', 'rk post').
card_number('orochi sustainer'/'CHK', '236').
card_flavor_text('orochi sustainer'/'CHK', '\"Nothing is the same anymore. The forests are not as lush, the waters not as pure, the air not as clear. Without the kami to direct and sustain the forces of nature, it\'s only a matter of time before we feel their neglect.\"').
card_multiverse_id('orochi sustainer'/'CHK', '75403').

card_in_set('otherworldly journey', 'CHK').
card_original_type('otherworldly journey'/'CHK', 'Instant — Arcane').
card_original_text('otherworldly journey'/'CHK', 'Remove target creature from the game. At end of turn, return that creature to play under its owner\'s control with a +1/+1 counter on it.').
card_first_print('otherworldly journey', 'CHK').
card_image_name('otherworldly journey'/'CHK', 'otherworldly journey').
card_uid('otherworldly journey'/'CHK', 'CHK:Otherworldly Journey:otherworldly journey').
card_rarity('otherworldly journey'/'CHK', 'Uncommon').
card_artist('otherworldly journey'/'CHK', 'Vance Kovacs').
card_number('otherworldly journey'/'CHK', '37').
card_flavor_text('otherworldly journey'/'CHK', '\"The landscape shimmered and I felt a chill breeze. When my vision cleared, I found myself alone among the corpses of my fallen friends.\"\n—Journal found in Numai').
card_multiverse_id('otherworldly journey'/'CHK', '50256').

card_in_set('pain kami', 'CHK').
card_original_type('pain kami'/'CHK', 'Creature — Spirit').
card_original_text('pain kami'/'CHK', '{X}{R}, Sacrifice Pain Kami: Pain Kami deals X damage to target creature.').
card_first_print('pain kami', 'CHK').
card_image_name('pain kami'/'CHK', 'pain kami').
card_uid('pain kami'/'CHK', 'CHK:Pain Kami:pain kami').
card_rarity('pain kami'/'CHK', 'Uncommon').
card_artist('pain kami'/'CHK', 'Tomas Giorello').
card_number('pain kami'/'CHK', '183').
card_flavor_text('pain kami'/'CHK', '\"All kami are our enemies now,\nA very tough lesson to learn.\nBut it\'s one that\'s taken quickly,\nWhen you feel what it\'s like to burn\"\n—Ku-Ku, akki poet').
card_multiverse_id('pain kami'/'CHK', '50266').

card_in_set('painwracker oni', 'CHK').
card_original_type('painwracker oni'/'CHK', 'Creature — Demon Spirit').
card_original_text('painwracker oni'/'CHK', 'Fear\nAt the beginning of your upkeep, sacrifice a creature if you don\'t control an Ogre.').
card_first_print('painwracker oni', 'CHK').
card_image_name('painwracker oni'/'CHK', 'painwracker oni').
card_uid('painwracker oni'/'CHK', 'CHK:Painwracker Oni:painwracker oni').
card_rarity('painwracker oni'/'CHK', 'Uncommon').
card_artist('painwracker oni'/'CHK', 'Hideaki Takamura').
card_number('painwracker oni'/'CHK', '136').
card_flavor_text('painwracker oni'/'CHK', '\"Blood flows. Blood calls. Blood devours all and only blood remains.\"\n—Ogre chant').
card_multiverse_id('painwracker oni'/'CHK', '79119').

card_in_set('part the veil', 'CHK').
card_original_type('part the veil'/'CHK', 'Instant — Arcane').
card_original_text('part the veil'/'CHK', 'Return all creatures you control to their owner\'s hand.').
card_first_print('part the veil', 'CHK').
card_image_name('part the veil'/'CHK', 'part the veil').
card_uid('part the veil'/'CHK', 'CHK:Part the Veil:part the veil').
card_rarity('part the veil'/'CHK', 'Rare').
card_artist('part the veil'/'CHK', 'Arnie Swekel').
card_number('part the veil'/'CHK', '77').
card_flavor_text('part the veil'/'CHK', 'At the waterfall, the border between the humanoid and the spirit worlds was weakest. The kami moved across it with ease and at will.').
card_multiverse_id('part the veil'/'CHK', '80291').

card_in_set('peer through depths', 'CHK').
card_original_type('peer through depths'/'CHK', 'Instant — Arcane').
card_original_text('peer through depths'/'CHK', 'Look at the top five cards of your library. You may reveal an instant or sorcery card from among them and put it into your hand. Put the rest on the bottom of your library in any order.').
card_first_print('peer through depths', 'CHK').
card_image_name('peer through depths'/'CHK', 'peer through depths').
card_uid('peer through depths'/'CHK', 'CHK:Peer Through Depths:peer through depths').
card_rarity('peer through depths'/'CHK', 'Common').
card_artist('peer through depths'/'CHK', 'Anthony S. Waters').
card_number('peer through depths'/'CHK', '78').
card_multiverse_id('peer through depths'/'CHK', '78690').

card_in_set('petals of insight', 'CHK').
card_original_type('petals of insight'/'CHK', 'Sorcery — Arcane').
card_original_text('petals of insight'/'CHK', 'Look at the top three cards of your library. You may put those cards on the bottom of your library in any order. If you do, return Petals of Insight to its owner\'s hand. Otherwise, draw three cards.').
card_first_print('petals of insight', 'CHK').
card_image_name('petals of insight'/'CHK', 'petals of insight').
card_uid('petals of insight'/'CHK', 'CHK:Petals of Insight:petals of insight').
card_rarity('petals of insight'/'CHK', 'Uncommon').
card_artist('petals of insight'/'CHK', 'Anthony S. Waters').
card_number('petals of insight'/'CHK', '79').
card_multiverse_id('petals of insight'/'CHK', '50254').

card_in_set('pinecrest ridge', 'CHK').
card_original_type('pinecrest ridge'/'CHK', 'Land').
card_original_text('pinecrest ridge'/'CHK', '{T}: Add {1} to your mana pool.\n{T}: Add {R} or {G} to your mana pool. Pinecrest Ridge doesn\'t untap during your next untap step.').
card_first_print('pinecrest ridge', 'CHK').
card_image_name('pinecrest ridge'/'CHK', 'pinecrest ridge').
card_uid('pinecrest ridge'/'CHK', 'CHK:Pinecrest Ridge:pinecrest ridge').
card_rarity('pinecrest ridge'/'CHK', 'Uncommon').
card_artist('pinecrest ridge'/'CHK', 'John Avon').
card_number('pinecrest ridge'/'CHK', '281').
card_multiverse_id('pinecrest ridge'/'CHK', '79132').

card_in_set('pious kitsune', 'CHK').
card_original_type('pious kitsune'/'CHK', 'Creature — Fox Cleric').
card_original_text('pious kitsune'/'CHK', 'At the beginning of your upkeep, put a devotion counter on Pious Kitsune. Then if a creature named Eight-and-a-Half-Tails is in play, you gain 1 life for each devotion counter on Pious Kitsune.\n{T}, Remove a devotion counter from Pious Kitsune: You gain 1 life.').
card_first_print('pious kitsune', 'CHK').
card_image_name('pious kitsune'/'CHK', 'pious kitsune').
card_uid('pious kitsune'/'CHK', 'CHK:Pious Kitsune:pious kitsune').
card_rarity('pious kitsune'/'CHK', 'Common').
card_artist('pious kitsune'/'CHK', 'Anthony S. Waters').
card_number('pious kitsune'/'CHK', '38').
card_multiverse_id('pious kitsune'/'CHK', '79256').

card_in_set('plains', 'CHK').
card_original_type('plains'/'CHK', 'Basic Land — Plains').
card_original_text('plains'/'CHK', 'W').
card_image_name('plains'/'CHK', 'plains1').
card_uid('plains'/'CHK', 'CHK:Plains:plains1').
card_rarity('plains'/'CHK', 'Basic Land').
card_artist('plains'/'CHK', 'Greg Staples').
card_number('plains'/'CHK', '287').
card_multiverse_id('plains'/'CHK', '79060').

card_in_set('plains', 'CHK').
card_original_type('plains'/'CHK', 'Basic Land — Plains').
card_original_text('plains'/'CHK', 'W').
card_image_name('plains'/'CHK', 'plains2').
card_uid('plains'/'CHK', 'CHK:Plains:plains2').
card_rarity('plains'/'CHK', 'Basic Land').
card_artist('plains'/'CHK', 'Greg Staples').
card_number('plains'/'CHK', '288').
card_multiverse_id('plains'/'CHK', '79063').

card_in_set('plains', 'CHK').
card_original_type('plains'/'CHK', 'Basic Land — Plains').
card_original_text('plains'/'CHK', 'W').
card_image_name('plains'/'CHK', 'plains3').
card_uid('plains'/'CHK', 'CHK:Plains:plains3').
card_rarity('plains'/'CHK', 'Basic Land').
card_artist('plains'/'CHK', 'Greg Staples').
card_number('plains'/'CHK', '289').
card_multiverse_id('plains'/'CHK', '79072').

card_in_set('plains', 'CHK').
card_original_type('plains'/'CHK', 'Basic Land — Plains').
card_original_text('plains'/'CHK', 'W').
card_image_name('plains'/'CHK', 'plains4').
card_uid('plains'/'CHK', 'CHK:Plains:plains4').
card_rarity('plains'/'CHK', 'Basic Land').
card_artist('plains'/'CHK', 'Greg Staples').
card_number('plains'/'CHK', '290').
card_multiverse_id('plains'/'CHK', '79076').

card_in_set('psychic puppetry', 'CHK').
card_original_type('psychic puppetry'/'CHK', 'Instant — Arcane').
card_original_text('psychic puppetry'/'CHK', 'Tap or untap target permanent.\nSplice onto Arcane {U} (As you play an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_first_print('psychic puppetry', 'CHK').
card_image_name('psychic puppetry'/'CHK', 'psychic puppetry').
card_uid('psychic puppetry'/'CHK', 'CHK:Psychic Puppetry:psychic puppetry').
card_rarity('psychic puppetry'/'CHK', 'Common').
card_artist('psychic puppetry'/'CHK', 'Joel Thomas').
card_number('psychic puppetry'/'CHK', '80').
card_multiverse_id('psychic puppetry'/'CHK', '80242').

card_in_set('pull under', 'CHK').
card_original_type('pull under'/'CHK', 'Instant — Arcane').
card_original_text('pull under'/'CHK', 'Target creature gets -5/-5 until end of turn.').
card_first_print('pull under', 'CHK').
card_image_name('pull under'/'CHK', 'pull under').
card_uid('pull under'/'CHK', 'CHK:Pull Under:pull under').
card_rarity('pull under'/'CHK', 'Common').
card_artist('pull under'/'CHK', 'Ron Spencer').
card_number('pull under'/'CHK', '137').
card_flavor_text('pull under'/'CHK', '\"Although nowhere on Kamigawa was safe during the war, the Takenuma Swamp was the most horrifying. The rotting bamboo itself rebelled against its mortal inhabitants, pulling them into unmarked graves.\"\n—Observations of the Kami War').
card_multiverse_id('pull under'/'CHK', '78972').

card_in_set('quiet purity', 'CHK').
card_original_type('quiet purity'/'CHK', 'Instant — Arcane').
card_original_text('quiet purity'/'CHK', 'Destroy target enchantment.').
card_first_print('quiet purity', 'CHK').
card_image_name('quiet purity'/'CHK', 'quiet purity').
card_uid('quiet purity'/'CHK', 'CHK:Quiet Purity:quiet purity').
card_rarity('quiet purity'/'CHK', 'Common').
card_artist('quiet purity'/'CHK', 'Shishizaru').
card_number('quiet purity'/'CHK', '39').
card_flavor_text('quiet purity'/'CHK', '\"When the Kami of Utter Silence passes, walls lose their secrets. Scrolls lose their lessons. Tapestries lose their beauty. Where there was meaning, there is only stillness.\"\n—Sensei Hisoka').
card_multiverse_id('quiet purity'/'CHK', '50313').

card_in_set('rag dealer', 'CHK').
card_original_type('rag dealer'/'CHK', 'Creature — Human Rogue').
card_original_text('rag dealer'/'CHK', '{2}{B}, {T}: Remove up to three target cards in a single graveyard from the game.').
card_first_print('rag dealer', 'CHK').
card_image_name('rag dealer'/'CHK', 'rag dealer').
card_uid('rag dealer'/'CHK', 'CHK:Rag Dealer:rag dealer').
card_rarity('rag dealer'/'CHK', 'Common').
card_artist('rag dealer'/'CHK', 'Ralph Horsley').
card_number('rag dealer'/'CHK', '138').
card_flavor_text('rag dealer'/'CHK', '\"After General Takeno found the Oathkeeper amidst the bamboo marshes, more scavengers braved the swamp\'s nezumi, oni, and kami in hopes of glory.\"\n—The History of Kamigawa').
card_multiverse_id('rag dealer'/'CHK', '79237').

card_in_set('ragged veins', 'CHK').
card_original_type('ragged veins'/'CHK', 'Enchant Creature').
card_original_text('ragged veins'/'CHK', 'You may play Ragged Veins any time you could play an instant.\nWhenever enchanted creature is dealt damage, its controller loses that much life.').
card_first_print('ragged veins', 'CHK').
card_image_name('ragged veins'/'CHK', 'ragged veins').
card_uid('ragged veins'/'CHK', 'CHK:Ragged Veins:ragged veins').
card_rarity('ragged veins'/'CHK', 'Common').
card_artist('ragged veins'/'CHK', 'Chippy').
card_number('ragged veins'/'CHK', '139').
card_multiverse_id('ragged veins'/'CHK', '79158').

card_in_set('reach through mists', 'CHK').
card_original_type('reach through mists'/'CHK', 'Instant — Arcane').
card_original_text('reach through mists'/'CHK', 'Draw a card.').
card_first_print('reach through mists', 'CHK').
card_image_name('reach through mists'/'CHK', 'reach through mists').
card_uid('reach through mists'/'CHK', 'CHK:Reach Through Mists:reach through mists').
card_rarity('reach through mists'/'CHK', 'Common').
card_artist('reach through mists'/'CHK', 'Anthony S. Waters').
card_number('reach through mists'/'CHK', '81').
card_flavor_text('reach through mists'/'CHK', '\"Know one part of the name, obsession begins. Know two parts, paranoia sets in. Know three parts, madness descends. Know all, and only the kami know what will become of you.\"\n—Lady Azami').
card_multiverse_id('reach through mists'/'CHK', '79247').

card_in_set('reciprocate', 'CHK').
card_original_type('reciprocate'/'CHK', 'Instant').
card_original_text('reciprocate'/'CHK', 'Remove from the game target creature that dealt damage to you this turn.').
card_image_name('reciprocate'/'CHK', 'reciprocate').
card_uid('reciprocate'/'CHK', 'CHK:Reciprocate:reciprocate').
card_rarity('reciprocate'/'CHK', 'Uncommon').
card_artist('reciprocate'/'CHK', 'Pat Lee').
card_number('reciprocate'/'CHK', '40').
card_flavor_text('reciprocate'/'CHK', '\"Just as the noble soul calls virtue to itself, the evil soul summons harm.\"\n—Teachings of Eight-and-a-Half-Tails').
card_multiverse_id('reciprocate'/'CHK', '50405').

card_in_set('reito lantern', 'CHK').
card_original_type('reito lantern'/'CHK', 'Artifact').
card_original_text('reito lantern'/'CHK', '{3}: Put target card in a graveyard on the bottom of its owner\'s library.').
card_first_print('reito lantern', 'CHK').
card_image_name('reito lantern'/'CHK', 'reito lantern').
card_uid('reito lantern'/'CHK', 'CHK:Reito Lantern:reito lantern').
card_rarity('reito lantern'/'CHK', 'Uncommon').
card_artist('reito lantern'/'CHK', 'Tim Hildebrandt').
card_number('reito lantern'/'CHK', '267').
card_flavor_text('reito lantern'/'CHK', 'Lanterns carved from the mystic stones of the Reito Mines were said to light the way of lost souls.').
card_multiverse_id('reito lantern'/'CHK', '80273').

card_in_set('rend flesh', 'CHK').
card_original_type('rend flesh'/'CHK', 'Instant — Arcane').
card_original_text('rend flesh'/'CHK', 'Destroy target non-Spirit creature.').
card_first_print('rend flesh', 'CHK').
card_image_name('rend flesh'/'CHK', 'rend flesh').
card_uid('rend flesh'/'CHK', 'CHK:Rend Flesh:rend flesh').
card_rarity('rend flesh'/'CHK', 'Common').
card_artist('rend flesh'/'CHK', 'Stephen Tappin').
card_number('rend flesh'/'CHK', '140').
card_flavor_text('rend flesh'/'CHK', '\"The Reito Massacre was a testament to the kami\'s unstoppable power. The human defenders might as well have been moths battling a forest fire.\"\n—Great Battles of Kamigawa').
card_multiverse_id('rend flesh'/'CHK', '75259').

card_in_set('rend spirit', 'CHK').
card_original_type('rend spirit'/'CHK', 'Instant').
card_original_text('rend spirit'/'CHK', 'Destroy target Spirit.').
card_first_print('rend spirit', 'CHK').
card_image_name('rend spirit'/'CHK', 'rend spirit').
card_uid('rend spirit'/'CHK', 'CHK:Rend Spirit:rend spirit').
card_rarity('rend spirit'/'CHK', 'Common').
card_artist('rend spirit'/'CHK', 'Stephen Tappin').
card_number('rend spirit'/'CHK', '141').
card_flavor_text('rend spirit'/'CHK', '\"The battle at Ganzan Pass was a testament to human endurance. The untrained and frightened group escaped three legions of kami, leaving more than their share of slain foes in their wake.\"\n—Great Battles of Kamigawa').
card_multiverse_id('rend spirit'/'CHK', '50293').

card_in_set('reverse the sands', 'CHK').
card_original_type('reverse the sands'/'CHK', 'Sorcery').
card_original_text('reverse the sands'/'CHK', 'Redistribute any number of players\' life totals. (Each of those players gets one life total back.)').
card_first_print('reverse the sands', 'CHK').
card_image_name('reverse the sands'/'CHK', 'reverse the sands').
card_uid('reverse the sands'/'CHK', 'CHK:Reverse the Sands:reverse the sands').
card_rarity('reverse the sands'/'CHK', 'Rare').
card_artist('reverse the sands'/'CHK', 'Jeremy Jarvis').
card_number('reverse the sands'/'CHK', '41').
card_flavor_text('reverse the sands'/'CHK', 'Worse than the years of aging was the burden of memory. The young monk lost his youth and gained a vicarious lifetime of hardship and woe.').
card_multiverse_id('reverse the sands'/'CHK', '79125').

card_in_set('reweave', 'CHK').
card_original_type('reweave'/'CHK', 'Instant — Arcane').
card_original_text('reweave'/'CHK', 'Target permanent\'s controller sacrifices it. That player reveals cards from the top of his or her library until he or she reveals a card that shares a card type with the sacrificed permanent. The player puts that card into play, then shuffles his or her library.\nSplice onto Arcane {2}{U}{U}').
card_first_print('reweave', 'CHK').
card_image_name('reweave'/'CHK', 'reweave').
card_uid('reweave'/'CHK', 'CHK:Reweave:reweave').
card_rarity('reweave'/'CHK', 'Rare').
card_artist('reweave'/'CHK', 'Alex Horley-Orlandelli').
card_number('reweave'/'CHK', '82').
card_multiverse_id('reweave'/'CHK', '79863').

card_in_set('river kaijin', 'CHK').
card_original_type('river kaijin'/'CHK', 'Creature — Spirit').
card_original_text('river kaijin'/'CHK', '').
card_first_print('river kaijin', 'CHK').
card_image_name('river kaijin'/'CHK', 'river kaijin').
card_uid('river kaijin'/'CHK', 'CHK:River Kaijin:river kaijin').
card_rarity('river kaijin'/'CHK', 'Common').
card_artist('river kaijin'/'CHK', 'Luca Zontini').
card_number('river kaijin'/'CHK', '83').
card_flavor_text('river kaijin'/'CHK', '\"Since the war began, fishermen don\'t sing to the kaijin to ask for full nets. They guard the shore with thrice-blessed spears, saving the nets for their wives to pack their belongings and leave.\"\n—Hayato, master sailor').
card_multiverse_id('river kaijin'/'CHK', '78586').

card_in_set('ronin houndmaster', 'CHK').
card_original_type('ronin houndmaster'/'CHK', 'Creature — Human Samurai').
card_original_text('ronin houndmaster'/'CHK', 'Haste\nBushido 1 (When this blocks or becomes blocked, it gets +1/+1 until end of turn.)').
card_first_print('ronin houndmaster', 'CHK').
card_image_name('ronin houndmaster'/'CHK', 'ronin houndmaster').
card_uid('ronin houndmaster'/'CHK', 'CHK:Ronin Houndmaster:ronin houndmaster').
card_rarity('ronin houndmaster'/'CHK', 'Common').
card_artist('ronin houndmaster'/'CHK', 'Edward P. Beard, Jr.').
card_number('ronin houndmaster'/'CHK', '184').
card_flavor_text('ronin houndmaster'/'CHK', 'Some samurai fell so far out of grace that only dogs would keep them company.').
card_multiverse_id('ronin houndmaster'/'CHK', '50262').

card_in_set('rootrunner', 'CHK').
card_original_type('rootrunner'/'CHK', 'Creature — Spirit').
card_original_text('rootrunner'/'CHK', '{G}{G}, Sacrifice Rootrunner: Put target land on the top of its owner\'s library.\nSoulshift 3 (When this is put into a graveyard from play, you may return target Spirit card with converted mana cost 3 or less from your graveyard to your hand.)').
card_first_print('rootrunner', 'CHK').
card_image_name('rootrunner'/'CHK', 'rootrunner').
card_uid('rootrunner'/'CHK', 'CHK:Rootrunner:rootrunner').
card_rarity('rootrunner'/'CHK', 'Uncommon').
card_artist('rootrunner'/'CHK', 'Adam Rex').
card_number('rootrunner'/'CHK', '237').
card_multiverse_id('rootrunner'/'CHK', '75367').

card_in_set('ryusei, the falling star', 'CHK').
card_original_type('ryusei, the falling star'/'CHK', 'Legendary Creature — Dragon Spirit').
card_original_text('ryusei, the falling star'/'CHK', 'Flying\nWhen Ryusei, the Falling Star is put into a graveyard from play, it deals 5 damage to each creature without flying.').
card_image_name('ryusei, the falling star'/'CHK', 'ryusei, the falling star').
card_uid('ryusei, the falling star'/'CHK', 'CHK:Ryusei, the Falling Star:ryusei, the falling star').
card_rarity('ryusei, the falling star'/'CHK', 'Rare').
card_artist('ryusei, the falling star'/'CHK', 'Nottsuo').
card_number('ryusei, the falling star'/'CHK', '185').
card_multiverse_id('ryusei, the falling star'/'CHK', '79242').

card_in_set('sachi, daughter of seshiro', 'CHK').
card_original_type('sachi, daughter of seshiro'/'CHK', 'Legendary Creature — Snake Shaman').
card_original_text('sachi, daughter of seshiro'/'CHK', 'Other Snakes you control get +0/+1.\nShamans you control have \"{T}: Add {G}{G} to your mana pool.\"').
card_first_print('sachi, daughter of seshiro', 'CHK').
card_image_name('sachi, daughter of seshiro'/'CHK', 'sachi, daughter of seshiro').
card_uid('sachi, daughter of seshiro'/'CHK', 'CHK:Sachi, Daughter of Seshiro:sachi, daughter of seshiro').
card_rarity('sachi, daughter of seshiro'/'CHK', 'Uncommon').
card_artist('sachi, daughter of seshiro'/'CHK', 'Nottsuo').
card_number('sachi, daughter of seshiro'/'CHK', '238').
card_flavor_text('sachi, daughter of seshiro'/'CHK', '\"The warriors deal in poison and fangs. We shamans remember why our ancestors crawled with their bellies to the earth.\"').
card_multiverse_id('sachi, daughter of seshiro'/'CHK', '80254').

card_in_set('sakura-tribe elder', 'CHK').
card_original_type('sakura-tribe elder'/'CHK', 'Creature — Snake Shaman').
card_original_text('sakura-tribe elder'/'CHK', 'Sacrifice Sakura-Tribe Elder: Search your library for a basic land card, put that card into play tapped, then shuffle your library.').
card_image_name('sakura-tribe elder'/'CHK', 'sakura-tribe elder').
card_uid('sakura-tribe elder'/'CHK', 'CHK:Sakura-Tribe Elder:sakura-tribe elder').
card_rarity('sakura-tribe elder'/'CHK', 'Common').
card_artist('sakura-tribe elder'/'CHK', 'Carl Critchlow').
card_number('sakura-tribe elder'/'CHK', '239').
card_flavor_text('sakura-tribe elder'/'CHK', 'There were no tombstones in orochi territory. Slain warriors were buried with a tree sapling, so they would become a part of the forest after death.').
card_multiverse_id('sakura-tribe elder'/'CHK', '50510').

card_in_set('samurai enforcers', 'CHK').
card_original_type('samurai enforcers'/'CHK', 'Creature — Human Samurai').
card_original_text('samurai enforcers'/'CHK', 'Bushido 2 (When this blocks or becomes blocked, it gets +2/+2 until end of turn.)').
card_first_print('samurai enforcers', 'CHK').
card_image_name('samurai enforcers'/'CHK', 'samurai enforcers').
card_uid('samurai enforcers'/'CHK', 'CHK:Samurai Enforcers:samurai enforcers').
card_rarity('samurai enforcers'/'CHK', 'Uncommon').
card_artist('samurai enforcers'/'CHK', 'Mitch Cotie').
card_number('samurai enforcers'/'CHK', '42').
card_flavor_text('samurai enforcers'/'CHK', 'From the moment they swore their oaths, they belonged to their lord, sword and soul.').
card_multiverse_id('samurai enforcers'/'CHK', '50408').

card_in_set('samurai of the pale curtain', 'CHK').
card_original_type('samurai of the pale curtain'/'CHK', 'Creature — Fox Samurai').
card_original_text('samurai of the pale curtain'/'CHK', 'Bushido 1 (When this blocks or becomes blocked, it gets +1/+1 until end of turn.)\nIf a permanent would be put into a graveyard, remove it from the game instead.').
card_first_print('samurai of the pale curtain', 'CHK').
card_image_name('samurai of the pale curtain'/'CHK', 'samurai of the pale curtain').
card_uid('samurai of the pale curtain'/'CHK', 'CHK:Samurai of the Pale Curtain:samurai of the pale curtain').
card_rarity('samurai of the pale curtain'/'CHK', 'Uncommon').
card_artist('samurai of the pale curtain'/'CHK', 'Christopher Moeller').
card_number('samurai of the pale curtain'/'CHK', '43').
card_multiverse_id('samurai of the pale curtain'/'CHK', '75381').

card_in_set('scuttling death', 'CHK').
card_original_type('scuttling death'/'CHK', 'Creature — Spirit').
card_original_text('scuttling death'/'CHK', 'Sacrifice Scuttling Death: Target creature gets -1/-1 until end of turn.\nSoulshift 4 (When this is put into a graveyard from play, you may return target Spirit card with converted mana cost 4 or less from your graveyard to your hand.)').
card_first_print('scuttling death', 'CHK').
card_image_name('scuttling death'/'CHK', 'scuttling death').
card_uid('scuttling death'/'CHK', 'CHK:Scuttling Death:scuttling death').
card_rarity('scuttling death'/'CHK', 'Common').
card_artist('scuttling death'/'CHK', 'Thomas M. Baxa').
card_number('scuttling death'/'CHK', '142').
card_multiverse_id('scuttling death'/'CHK', '77922').

card_in_set('seizan, perverter of truth', 'CHK').
card_original_type('seizan, perverter of truth'/'CHK', 'Legendary Creature — Demon Spirit').
card_original_text('seizan, perverter of truth'/'CHK', 'At the beginning of each player\'s upkeep, that player loses 2 life and draws two cards.').
card_first_print('seizan, perverter of truth', 'CHK').
card_image_name('seizan, perverter of truth'/'CHK', 'seizan, perverter of truth').
card_uid('seizan, perverter of truth'/'CHK', 'CHK:Seizan, Perverter of Truth:seizan, perverter of truth').
card_rarity('seizan, perverter of truth'/'CHK', 'Rare').
card_artist('seizan, perverter of truth'/'CHK', 'Kev Walker').
card_number('seizan, perverter of truth'/'CHK', '143').
card_flavor_text('seizan, perverter of truth'/'CHK', '\"If you would taste the wisdom of the oni, be prepared to salt it with your blood.\"\n—Kiku, Night\'s Flower').
card_multiverse_id('seizan, perverter of truth'/'CHK', '78965').

card_in_set('sensei golden-tail', 'CHK').
card_original_type('sensei golden-tail'/'CHK', 'Legendary Creature — Fox Samurai').
card_original_text('sensei golden-tail'/'CHK', 'Bushido 1 (When this blocks or becomes blocked, it gets +1/+1 until end of turn.)\n{1}{W}, {T}: Put a training counter on target creature. That creature gains bushido 1 and becomes a Samurai in addition to its other creature types. Play this ability only any time you could play a sorcery.').
card_first_print('sensei golden-tail', 'CHK').
card_image_name('sensei golden-tail'/'CHK', 'sensei golden-tail').
card_uid('sensei golden-tail'/'CHK', 'CHK:Sensei Golden-Tail:sensei golden-tail').
card_rarity('sensei golden-tail'/'CHK', 'Rare').
card_artist('sensei golden-tail'/'CHK', 'Stephen Tappin').
card_number('sensei golden-tail'/'CHK', '44').
card_multiverse_id('sensei golden-tail'/'CHK', '78683').

card_in_set('sensei\'s divining top', 'CHK').
card_original_type('sensei\'s divining top'/'CHK', 'Artifact').
card_original_text('sensei\'s divining top'/'CHK', '{1}: Look at the top three cards of your library, then put them back in any order.\n{T}: Draw a card, then put Sensei\'s Divining Top on top of its owner\'s library.').
card_first_print('sensei\'s divining top', 'CHK').
card_image_name('sensei\'s divining top'/'CHK', 'sensei\'s divining top').
card_uid('sensei\'s divining top'/'CHK', 'CHK:Sensei\'s Divining Top:sensei\'s divining top').
card_rarity('sensei\'s divining top'/'CHK', 'Uncommon').
card_artist('sensei\'s divining top'/'CHK', 'Michael Sutfin').
card_number('sensei\'s divining top'/'CHK', '268').
card_multiverse_id('sensei\'s divining top'/'CHK', '50400').

card_in_set('serpent skin', 'CHK').
card_original_type('serpent skin'/'CHK', 'Enchant Creature').
card_original_text('serpent skin'/'CHK', 'You may play Serpent Skin any time you could play an instant.\nEnchanted creature gets +1/+1.\n{G}: Regenerate enchanted creature.').
card_first_print('serpent skin', 'CHK').
card_image_name('serpent skin'/'CHK', 'serpent skin').
card_uid('serpent skin'/'CHK', 'CHK:Serpent Skin:serpent skin').
card_rarity('serpent skin'/'CHK', 'Common').
card_artist('serpent skin'/'CHK', 'Rob Alexander').
card_number('serpent skin'/'CHK', '240').
card_multiverse_id('serpent skin'/'CHK', '75404').

card_in_set('seshiro the anointed', 'CHK').
card_original_type('seshiro the anointed'/'CHK', 'Legendary Creature — Snake Monk').
card_original_text('seshiro the anointed'/'CHK', 'Other Snakes you control get +2/+2.\nWhenever a Snake you control deals combat damage to a player, you may draw a card.').
card_first_print('seshiro the anointed', 'CHK').
card_image_name('seshiro the anointed'/'CHK', 'seshiro the anointed').
card_uid('seshiro the anointed'/'CHK', 'CHK:Seshiro the Anointed:seshiro the anointed').
card_rarity('seshiro the anointed'/'CHK', 'Rare').
card_artist('seshiro the anointed'/'CHK', 'Daren Bader').
card_number('seshiro the anointed'/'CHK', '241').
card_flavor_text('seshiro the anointed'/'CHK', 'His family was the first to reach out to the human monks. He soon knew as many koans as he did blade strikes.').
card_multiverse_id('seshiro the anointed'/'CHK', '80256').

card_in_set('shell of the last kappa', 'CHK').
card_original_type('shell of the last kappa'/'CHK', 'Legendary Artifact').
card_original_text('shell of the last kappa'/'CHK', '{3}, {T}: Remove from the game target instant or sorcery spell that targets you. (The spell has no effect.)\n{3}, {T}, Sacrifice Shell of the Last Kappa: You may play a card removed from the game with Shell of the Last Kappa without paying its mana cost.').
card_first_print('shell of the last kappa', 'CHK').
card_image_name('shell of the last kappa'/'CHK', 'shell of the last kappa').
card_uid('shell of the last kappa'/'CHK', 'CHK:Shell of the Last Kappa:shell of the last kappa').
card_rarity('shell of the last kappa'/'CHK', 'Rare').
card_artist('shell of the last kappa'/'CHK', 'David Martin').
card_number('shell of the last kappa'/'CHK', '269').
card_multiverse_id('shell of the last kappa'/'CHK', '75278').

card_in_set('shidako, broodmistress', 'CHK').
card_original_type('shidako, broodmistress'/'CHK', 'Creature — Snake Shaman').
card_original_text('shidako, broodmistress'/'CHK', '{2}{G}, {T}: Put a 1/1 green Snake creature token into play. If you control ten or more creatures, flip Orochi Eggwatcher.\n-----\nShidako, Broodmistress\nLegendary Creature Snake Shaman\n3/3\n{G}, Sacrifice a creature: Target creature gets +3/+3 until end of turn.').
card_first_print('shidako, broodmistress', 'CHK').
card_image_name('shidako, broodmistress'/'CHK', 'shidako, broodmistress').
card_uid('shidako, broodmistress'/'CHK', 'CHK:Shidako, Broodmistress:shidako, broodmistress').
card_rarity('shidako, broodmistress'/'CHK', 'Uncommon').
card_artist('shidako, broodmistress'/'CHK', 'Dan Scott').
card_number('shidako, broodmistress'/'CHK', '233b').
card_multiverse_id('shidako, broodmistress'/'CHK', '78975').

card_in_set('shimatsu the bloodcloaked', 'CHK').
card_original_type('shimatsu the bloodcloaked'/'CHK', 'Legendary Creature — Demon Spirit').
card_original_text('shimatsu the bloodcloaked'/'CHK', 'As Shimatsu the Bloodcloaked comes into play, sacrifice any number of permanents. Shimatsu comes into play with that many +1/+1 counters on it.').
card_first_print('shimatsu the bloodcloaked', 'CHK').
card_image_name('shimatsu the bloodcloaked'/'CHK', 'shimatsu the bloodcloaked').
card_uid('shimatsu the bloodcloaked'/'CHK', 'CHK:Shimatsu the Bloodcloaked:shimatsu the bloodcloaked').
card_rarity('shimatsu the bloodcloaked'/'CHK', 'Rare').
card_artist('shimatsu the bloodcloaked'/'CHK', 'Dave Allsop').
card_number('shimatsu the bloodcloaked'/'CHK', '186').
card_flavor_text('shimatsu the bloodcloaked'/'CHK', 'Their dominion over dark and destructive forces twisted the oni into beings of pure malevolence.').
card_multiverse_id('shimatsu the bloodcloaked'/'CHK', '50318').

card_in_set('shinka, the bloodsoaked keep', 'CHK').
card_original_type('shinka, the bloodsoaked keep'/'CHK', 'Legendary Land').
card_original_text('shinka, the bloodsoaked keep'/'CHK', '{T}: Add {R} to your mana pool.\n{R}, {T}: Target legendary creature gains first strike until end of turn.').
card_first_print('shinka, the bloodsoaked keep', 'CHK').
card_image_name('shinka, the bloodsoaked keep'/'CHK', 'shinka, the bloodsoaked keep').
card_uid('shinka, the bloodsoaked keep'/'CHK', 'CHK:Shinka, the Bloodsoaked Keep:shinka, the bloodsoaked keep').
card_rarity('shinka, the bloodsoaked keep'/'CHK', 'Rare').
card_artist('shinka, the bloodsoaked keep'/'CHK', 'Thomas M. Baxa').
card_number('shinka, the bloodsoaked keep'/'CHK', '282').
card_flavor_text('shinka, the bloodsoaked keep'/'CHK', 'The glow from within looks inviting, but woe awaits whomever finds out who stokes the fire or what simmers in the pot.').
card_multiverse_id('shinka, the bloodsoaked keep'/'CHK', '79121').

card_in_set('shisato, whispering hunter', 'CHK').
card_original_type('shisato, whispering hunter'/'CHK', 'Legendary Creature — Snake Warrior').
card_original_text('shisato, whispering hunter'/'CHK', 'At the beginning of your upkeep, sacrifice a Snake.\nWhenever Shisato, Whispering Hunter deals combat damage to a player, that player skips his or her next untap step.').
card_first_print('shisato, whispering hunter', 'CHK').
card_image_name('shisato, whispering hunter'/'CHK', 'shisato, whispering hunter').
card_uid('shisato, whispering hunter'/'CHK', 'CHK:Shisato, Whispering Hunter:shisato, whispering hunter').
card_rarity('shisato, whispering hunter'/'CHK', 'Rare').
card_artist('shisato, whispering hunter'/'CHK', 'John Bolton').
card_number('shisato, whispering hunter'/'CHK', '242').
card_multiverse_id('shisato, whispering hunter'/'CHK', '78853').

card_in_set('shizo, death\'s storehouse', 'CHK').
card_original_type('shizo, death\'s storehouse'/'CHK', 'Legendary Land').
card_original_text('shizo, death\'s storehouse'/'CHK', '{T}: Add {B} to your mana pool.\n{B}, {T}: Target legendary creature gains fear until end of turn.').
card_first_print('shizo, death\'s storehouse', 'CHK').
card_image_name('shizo, death\'s storehouse'/'CHK', 'shizo, death\'s storehouse').
card_uid('shizo, death\'s storehouse'/'CHK', 'CHK:Shizo, Death\'s Storehouse:shizo, death\'s storehouse').
card_rarity('shizo, death\'s storehouse'/'CHK', 'Rare').
card_artist('shizo, death\'s storehouse'/'CHK', 'John Matson').
card_number('shizo, death\'s storehouse'/'CHK', '283').
card_flavor_text('shizo, death\'s storehouse'/'CHK', 'Centuries ago, Shizo was a verdant field of wildflowers. After 891 samurai died in a single battle on its grasses, it became a haunted moor.').
card_multiverse_id('shizo, death\'s storehouse'/'CHK', '79186').

card_in_set('sideswipe', 'CHK').
card_original_type('sideswipe'/'CHK', 'Instant').
card_original_text('sideswipe'/'CHK', 'You may change any targets of target Arcane spell.').
card_first_print('sideswipe', 'CHK').
card_image_name('sideswipe'/'CHK', 'sideswipe').
card_uid('sideswipe'/'CHK', 'CHK:Sideswipe:sideswipe').
card_rarity('sideswipe'/'CHK', 'Uncommon').
card_artist('sideswipe'/'CHK', 'Ron Spears').
card_number('sideswipe'/'CHK', '187').
card_flavor_text('sideswipe'/'CHK', 'Hisoka\'s wizards struggled for years to master the art of redirection that came so naturally to the shamans of Ganzan Pass.').
card_multiverse_id('sideswipe'/'CHK', '80523').

card_in_set('sift through sands', 'CHK').
card_original_type('sift through sands'/'CHK', 'Instant — Arcane').
card_original_text('sift through sands'/'CHK', 'Draw two cards, then discard a card.\nIf you played a spell named Peer Through Depths and a spell named Reach Through Mists this turn, you may search your library for a card named The Unspeakable, put it into play, then shuffle your library.').
card_first_print('sift through sands', 'CHK').
card_image_name('sift through sands'/'CHK', 'sift through sands').
card_uid('sift through sands'/'CHK', 'CHK:Sift Through Sands:sift through sands').
card_rarity('sift through sands'/'CHK', 'Common').
card_artist('sift through sands'/'CHK', 'Anthony S. Waters').
card_number('sift through sands'/'CHK', '84').
card_multiverse_id('sift through sands'/'CHK', '50459').

card_in_set('silent-chant zubera', 'CHK').
card_original_type('silent-chant zubera'/'CHK', 'Creature — Zubera Spirit').
card_original_text('silent-chant zubera'/'CHK', 'When Silent-Chant Zubera is put into a graveyard from play, you gain 2 life for each Zubera put into a graveyard from play this turn.').
card_first_print('silent-chant zubera', 'CHK').
card_image_name('silent-chant zubera'/'CHK', 'silent-chant zubera').
card_uid('silent-chant zubera'/'CHK', 'CHK:Silent-Chant Zubera:silent-chant zubera').
card_rarity('silent-chant zubera'/'CHK', 'Common').
card_artist('silent-chant zubera'/'CHK', 'Ben Thompson').
card_number('silent-chant zubera'/'CHK', '45').
card_flavor_text('silent-chant zubera'/'CHK', 'When the Honden of Cleansing Fire was abandoned, its attendants swarmed Kamigawa to erode mortal will.').
card_multiverse_id('silent-chant zubera'/'CHK', '80510').

card_in_set('sire of the storm', 'CHK').
card_original_type('sire of the storm'/'CHK', 'Creature — Spirit').
card_original_text('sire of the storm'/'CHK', 'Flying\nWhenever you play a Spirit or Arcane spell, you may draw a card.').
card_first_print('sire of the storm', 'CHK').
card_image_name('sire of the storm'/'CHK', 'sire of the storm').
card_uid('sire of the storm'/'CHK', 'CHK:Sire of the Storm:sire of the storm').
card_rarity('sire of the storm'/'CHK', 'Uncommon').
card_artist('sire of the storm'/'CHK', 'Arnie Swekel').
card_number('sire of the storm'/'CHK', '85').
card_flavor_text('sire of the storm'/'CHK', 'This storm blows gales through the dreams of men.').
card_multiverse_id('sire of the storm'/'CHK', '50442').

card_in_set('soilshaper', 'CHK').
card_original_type('soilshaper'/'CHK', 'Creature — Spirit').
card_original_text('soilshaper'/'CHK', 'Whenever you play a Spirit or Arcane spell, target land becomes a 3/3 creature until end of turn. It\'s still a land.').
card_first_print('soilshaper', 'CHK').
card_image_name('soilshaper'/'CHK', 'soilshaper').
card_uid('soilshaper'/'CHK', 'CHK:Soilshaper:soilshaper').
card_rarity('soilshaper'/'CHK', 'Uncommon').
card_artist('soilshaper'/'CHK', 'Thomas M. Baxa').
card_number('soilshaper'/'CHK', '243').
card_flavor_text('soilshaper'/'CHK', 'It spoke with the voice of nature, but its words were curses spat upon humankind.').
card_multiverse_id('soilshaper'/'CHK', '79177').

card_in_set('sokenzan bruiser', 'CHK').
card_original_type('sokenzan bruiser'/'CHK', 'Creature — Ogre Warrior').
card_original_text('sokenzan bruiser'/'CHK', 'Mountainwalk').
card_first_print('sokenzan bruiser', 'CHK').
card_image_name('sokenzan bruiser'/'CHK', 'sokenzan bruiser').
card_uid('sokenzan bruiser'/'CHK', 'CHK:Sokenzan Bruiser:sokenzan bruiser').
card_rarity('sokenzan bruiser'/'CHK', 'Common').
card_artist('sokenzan bruiser'/'CHK', 'Paolo Parente').
card_number('sokenzan bruiser'/'CHK', '188').
card_flavor_text('sokenzan bruiser'/'CHK', '\"We camped near the Sokenzan Mountains. Though I know its inhabitants are sparse, I hear cries coming from the highest peaks every night, as if the mountains themselves bellow for vengeance.\"\n—Lost Battalion, message to General Takeno').
card_multiverse_id('sokenzan bruiser'/'CHK', '79140').

card_in_set('soratami cloudskater', 'CHK').
card_original_type('soratami cloudskater'/'CHK', 'Creature — Moonfolk Rogue').
card_original_text('soratami cloudskater'/'CHK', 'Flying\n{2}, Return a land you control to its owner\'s hand: Draw a card, then discard a card.').
card_first_print('soratami cloudskater', 'CHK').
card_image_name('soratami cloudskater'/'CHK', 'soratami cloudskater').
card_uid('soratami cloudskater'/'CHK', 'CHK:Soratami Cloudskater:soratami cloudskater').
card_rarity('soratami cloudskater'/'CHK', 'Common').
card_artist('soratami cloudskater'/'CHK', 'Michael Sutfin').
card_number('soratami cloudskater'/'CHK', '86').
card_flavor_text('soratami cloudskater'/'CHK', '\"You hide your actions from eyes on the ground, but nothing escapes the clouds.\"').
card_multiverse_id('soratami cloudskater'/'CHK', '50301').

card_in_set('soratami mirror-guard', 'CHK').
card_original_type('soratami mirror-guard'/'CHK', 'Creature — Moonfolk Wizard').
card_original_text('soratami mirror-guard'/'CHK', 'Flying\n{2}, Return a land you control to its owner\'s hand: Target creature with power 2 or less is unblockable this turn.').
card_first_print('soratami mirror-guard', 'CHK').
card_image_name('soratami mirror-guard'/'CHK', 'soratami mirror-guard').
card_uid('soratami mirror-guard'/'CHK', 'CHK:Soratami Mirror-Guard:soratami mirror-guard').
card_rarity('soratami mirror-guard'/'CHK', 'Common').
card_artist('soratami mirror-guard'/'CHK', 'Wayne England').
card_number('soratami mirror-guard'/'CHK', '87').
card_multiverse_id('soratami mirror-guard'/'CHK', '80414').

card_in_set('soratami mirror-mage', 'CHK').
card_original_type('soratami mirror-mage'/'CHK', 'Creature — Moonfolk Wizard').
card_original_text('soratami mirror-mage'/'CHK', 'Flying\n{3}, Return three lands you control to their owner\'s hand: Return target creature to its owner\'s hand.').
card_first_print('soratami mirror-mage', 'CHK').
card_image_name('soratami mirror-mage'/'CHK', 'soratami mirror-mage').
card_uid('soratami mirror-mage'/'CHK', 'CHK:Soratami Mirror-Mage:soratami mirror-mage').
card_rarity('soratami mirror-mage'/'CHK', 'Uncommon').
card_artist('soratami mirror-mage'/'CHK', 'Ron Spears').
card_number('soratami mirror-mage'/'CHK', '88').
card_flavor_text('soratami mirror-mage'/'CHK', '\"The clouds obey my whims, and you\'ll obey theirs.\"').
card_multiverse_id('soratami mirror-mage'/'CHK', '80410').

card_in_set('soratami rainshaper', 'CHK').
card_original_type('soratami rainshaper'/'CHK', 'Creature — Moonfolk Wizard').
card_original_text('soratami rainshaper'/'CHK', 'Flying\n{3}, Return a land you control to its owner\'s hand: Target creature you control can\'t be the target of spells or abilities this turn.').
card_first_print('soratami rainshaper', 'CHK').
card_image_name('soratami rainshaper'/'CHK', 'soratami rainshaper').
card_uid('soratami rainshaper'/'CHK', 'CHK:Soratami Rainshaper:soratami rainshaper').
card_rarity('soratami rainshaper'/'CHK', 'Common').
card_artist('soratami rainshaper'/'CHK', 'Ittoku').
card_number('soratami rainshaper'/'CHK', '89').
card_multiverse_id('soratami rainshaper'/'CHK', '80413').

card_in_set('soratami savant', 'CHK').
card_original_type('soratami savant'/'CHK', 'Creature — Moonfolk Wizard').
card_original_text('soratami savant'/'CHK', 'Flying\n{3}, Return a land you control to its owner\'s hand: Counter target spell unless its controller pays {3}.').
card_first_print('soratami savant', 'CHK').
card_image_name('soratami savant'/'CHK', 'soratami savant').
card_uid('soratami savant'/'CHK', 'CHK:Soratami Savant:soratami savant').
card_rarity('soratami savant'/'CHK', 'Uncommon').
card_artist('soratami savant'/'CHK', 'Jim Nelson').
card_number('soratami savant'/'CHK', '90').
card_flavor_text('soratami savant'/'CHK', '\"To prevent, one must first predict.\"').
card_multiverse_id('soratami savant'/'CHK', '80417').

card_in_set('soratami seer', 'CHK').
card_original_type('soratami seer'/'CHK', 'Creature — Moonfolk Wizard').
card_original_text('soratami seer'/'CHK', 'Flying\n{4}, Return two lands you control to their owner\'s hand: Discard your hand, then draw that many cards.').
card_first_print('soratami seer', 'CHK').
card_image_name('soratami seer'/'CHK', 'soratami seer').
card_uid('soratami seer'/'CHK', 'CHK:Soratami Seer:soratami seer').
card_rarity('soratami seer'/'CHK', 'Uncommon').
card_artist('soratami seer'/'CHK', 'Glen Angus').
card_number('soratami seer'/'CHK', '91').
card_flavor_text('soratami seer'/'CHK', 'Their mirrors show two worlds: that which is and that which should be.').
card_multiverse_id('soratami seer'/'CHK', '80415').

card_in_set('sosuke, son of seshiro', 'CHK').
card_original_type('sosuke, son of seshiro'/'CHK', 'Legendary Creature — Snake Warrior').
card_original_text('sosuke, son of seshiro'/'CHK', 'Other Snakes you control get +1/+0.\nWhenever a Warrior you control deals combat damage to a creature, destroy that creature at end of combat.').
card_first_print('sosuke, son of seshiro', 'CHK').
card_image_name('sosuke, son of seshiro'/'CHK', 'sosuke, son of seshiro').
card_uid('sosuke, son of seshiro'/'CHK', 'CHK:Sosuke, Son of Seshiro:sosuke, son of seshiro').
card_rarity('sosuke, son of seshiro'/'CHK', 'Uncommon').
card_artist('sosuke, son of seshiro'/'CHK', 'Carl Critchlow').
card_number('sosuke, son of seshiro'/'CHK', '244').
card_multiverse_id('sosuke, son of seshiro'/'CHK', '78989').

card_in_set('soul of magma', 'CHK').
card_original_type('soul of magma'/'CHK', 'Creature — Spirit').
card_original_text('soul of magma'/'CHK', 'Whenever you play a Spirit or Arcane spell, Soul of Magma deals 1 damage to target creature.').
card_first_print('soul of magma', 'CHK').
card_image_name('soul of magma'/'CHK', 'soul of magma').
card_uid('soul of magma'/'CHK', 'CHK:Soul of Magma:soul of magma').
card_rarity('soul of magma'/'CHK', 'Common').
card_artist('soul of magma'/'CHK', 'Darrell Riche').
card_number('soul of magma'/'CHK', '189').
card_flavor_text('soul of magma'/'CHK', '\"In every other mind, the battle was lost. General Takeno alone was not touched by despair. Drawing his blade, he was attack and rallying cry in one.\"\n—Battle of Akagi River: A Survivor\'s Tale').
card_multiverse_id('soul of magma'/'CHK', '79253').

card_in_set('soulblast', 'CHK').
card_original_type('soulblast'/'CHK', 'Instant').
card_original_text('soulblast'/'CHK', 'As an additional cost to play Soulblast, sacrifice all creatures you control.\nSoulblast deals damage to target creature or player equal to the total power of the sacrificed creatures.').
card_first_print('soulblast', 'CHK').
card_image_name('soulblast'/'CHK', 'soulblast').
card_uid('soulblast'/'CHK', 'CHK:Soulblast:soulblast').
card_rarity('soulblast'/'CHK', 'Rare').
card_artist('soulblast'/'CHK', 'Wayne Reynolds').
card_number('soulblast'/'CHK', '190').
card_multiverse_id('soulblast'/'CHK', '80246').

card_in_set('soulless revival', 'CHK').
card_original_type('soulless revival'/'CHK', 'Instant — Arcane').
card_original_text('soulless revival'/'CHK', 'Return target creature card from your graveyard to your hand.\nSplice onto Arcane {1}{B} (As you play an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_first_print('soulless revival', 'CHK').
card_image_name('soulless revival'/'CHK', 'soulless revival').
card_uid('soulless revival'/'CHK', 'CHK:Soulless Revival:soulless revival').
card_rarity('soulless revival'/'CHK', 'Common').
card_artist('soulless revival'/'CHK', 'Ron Spencer').
card_number('soulless revival'/'CHK', '144').
card_multiverse_id('soulless revival'/'CHK', '50288').

card_in_set('squelch', 'CHK').
card_original_type('squelch'/'CHK', 'Instant').
card_original_text('squelch'/'CHK', 'Counter target activated ability. (Mana abilities can\'t be targeted.)\nDraw a card.').
card_first_print('squelch', 'CHK').
card_image_name('squelch'/'CHK', 'squelch').
card_uid('squelch'/'CHK', 'CHK:Squelch:squelch').
card_rarity('squelch'/'CHK', 'Uncommon').
card_artist('squelch'/'CHK', 'Matt Cavotta').
card_number('squelch'/'CHK', '92').
card_flavor_text('squelch'/'CHK', 'Oku-Doku had gone through all the motions: the same akki cursewords, the same ingredients with their horrid stink, the same rude gestures. Yet not a person died.').
card_multiverse_id('squelch'/'CHK', '80290').

card_in_set('stabwhisker the odious', 'CHK').
card_original_type('stabwhisker the odious'/'CHK', 'Creature — Rat Rogue').
card_original_text('stabwhisker the odious'/'CHK', '{1}{B}, {T}: Target opponent discards a card. Then if that player has no cards in hand, flip Nezumi Shortfang.\n-----\nStabwhisker the Odious\nLegendary Creature Rat Shaman\n3/3\nAt the beginning of each opponent\'s upkeep, that player loses 1 life for each card fewer than three in his or her hand.').
card_first_print('stabwhisker the odious', 'CHK').
card_image_name('stabwhisker the odious'/'CHK', 'stabwhisker the odious').
card_uid('stabwhisker the odious'/'CHK', 'CHK:Stabwhisker the Odious:stabwhisker the odious').
card_rarity('stabwhisker the odious'/'CHK', 'Rare').
card_artist('stabwhisker the odious'/'CHK', 'Daren Bader').
card_number('stabwhisker the odious'/'CHK', '131b').
card_multiverse_id('stabwhisker the odious'/'CHK', '78679').

card_in_set('stone rain', 'CHK').
card_original_type('stone rain'/'CHK', 'Sorcery').
card_original_text('stone rain'/'CHK', 'Destroy target land.').
card_image_name('stone rain'/'CHK', 'stone rain').
card_uid('stone rain'/'CHK', 'CHK:Stone Rain:stone rain').
card_rarity('stone rain'/'CHK', 'Common').
card_artist('stone rain'/'CHK', 'Greg Staples').
card_number('stone rain'/'CHK', '191').
card_flavor_text('stone rain'/'CHK', '\"The kami struck with gouts of fire, showers of rock, and the combined rage of the spirit world. Only Eiganjo Castle remained standing, glistening untouched in the sun.\"\n—The History of Kamigawa').
card_multiverse_id('stone rain'/'CHK', '79223').

card_in_set('strange inversion', 'CHK').
card_original_type('strange inversion'/'CHK', 'Instant — Arcane').
card_original_text('strange inversion'/'CHK', 'Switch target creature\'s power and toughness until end of turn.\nSplice onto Arcane {1}{R} (As you play an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_first_print('strange inversion', 'CHK').
card_image_name('strange inversion'/'CHK', 'strange inversion').
card_uid('strange inversion'/'CHK', 'CHK:Strange Inversion:strange inversion').
card_rarity('strange inversion'/'CHK', 'Uncommon').
card_artist('strange inversion'/'CHK', 'Khang Le').
card_number('strange inversion'/'CHK', '192').
card_multiverse_id('strange inversion'/'CHK', '82000').

card_in_set('strength of cedars', 'CHK').
card_original_type('strength of cedars'/'CHK', 'Instant — Arcane').
card_original_text('strength of cedars'/'CHK', 'Target creature gets +X/+X until end of turn, where X is the number of lands you control.').
card_first_print('strength of cedars', 'CHK').
card_image_name('strength of cedars'/'CHK', 'strength of cedars').
card_uid('strength of cedars'/'CHK', 'CHK:Strength of Cedars:strength of cedars').
card_rarity('strength of cedars'/'CHK', 'Uncommon').
card_artist('strength of cedars'/'CHK', 'Edward P. Beard, Jr.').
card_number('strength of cedars'/'CHK', '245').
card_flavor_text('strength of cedars'/'CHK', '\"No sooner had the beast collapsed under the blows of Takeno\'s sword than the earth rippled and the creature rose again to face him.\"\n—Battle of Akagi River: A Survivor\'s Tale').
card_multiverse_id('strength of cedars'/'CHK', '75366').

card_in_set('struggle for sanity', 'CHK').
card_original_type('struggle for sanity'/'CHK', 'Sorcery').
card_original_text('struggle for sanity'/'CHK', 'Target opponent reveals his or her hand. That player sets aside a card from it, then you set aside a card from it. Repeat this process until all cards in that hand have been set aside. That player returns the cards he or she set aside to his or her hand and puts the rest into his or her graveyard.').
card_first_print('struggle for sanity', 'CHK').
card_image_name('struggle for sanity'/'CHK', 'struggle for sanity').
card_uid('struggle for sanity'/'CHK', 'CHK:Struggle for Sanity:struggle for sanity').
card_rarity('struggle for sanity'/'CHK', 'Uncommon').
card_artist('struggle for sanity'/'CHK', 'Randy Gallegos').
card_number('struggle for sanity'/'CHK', '145').
card_multiverse_id('struggle for sanity'/'CHK', '79168').

card_in_set('student of elements', 'CHK').
card_original_type('student of elements'/'CHK', 'Creature — Human Wizard').
card_original_text('student of elements'/'CHK', 'When Student of Elements has flying, flip it.\n-----\nTobita, Master of Winds\nLegendary Creature Human Wizard\n3/3\nCreatures you control have flying.').
card_first_print('student of elements', 'CHK').
card_image_name('student of elements'/'CHK', 'student of elements').
card_uid('student of elements'/'CHK', 'CHK:Student of Elements:student of elements').
card_rarity('student of elements'/'CHK', 'Uncommon').
card_artist('student of elements'/'CHK', 'Ittoku').
card_number('student of elements'/'CHK', '93a').
card_multiverse_id('student of elements'/'CHK', '78691').

card_in_set('swallowing plague', 'CHK').
card_original_type('swallowing plague'/'CHK', 'Sorcery — Arcane').
card_original_text('swallowing plague'/'CHK', 'Swallowing Plague deals X damage to target creature and you gain X life.').
card_first_print('swallowing plague', 'CHK').
card_image_name('swallowing plague'/'CHK', 'swallowing plague').
card_uid('swallowing plague'/'CHK', 'CHK:Swallowing Plague:swallowing plague').
card_rarity('swallowing plague'/'CHK', 'Uncommon').
card_artist('swallowing plague'/'CHK', 'Dave Dorman').
card_number('swallowing plague'/'CHK', '146').
card_flavor_text('swallowing plague'/'CHK', '\"We are recalling all forces from the Takenuma Swamp. We lose men daily to kami attacks, and it seems our defeats only encourage them further.\"\n—General Takeno, letter to Lord Konda').
card_multiverse_id('swallowing plague'/'CHK', '79089').

card_in_set('swamp', 'CHK').
card_original_type('swamp'/'CHK', 'Basic Land — Swamp').
card_original_text('swamp'/'CHK', 'B').
card_image_name('swamp'/'CHK', 'swamp1').
card_uid('swamp'/'CHK', 'CHK:Swamp:swamp1').
card_rarity('swamp'/'CHK', 'Basic Land').
card_artist('swamp'/'CHK', 'Jim Nelson').
card_number('swamp'/'CHK', '295').
card_multiverse_id('swamp'/'CHK', '79065').

card_in_set('swamp', 'CHK').
card_original_type('swamp'/'CHK', 'Basic Land — Swamp').
card_original_text('swamp'/'CHK', 'B').
card_image_name('swamp'/'CHK', 'swamp2').
card_uid('swamp'/'CHK', 'CHK:Swamp:swamp2').
card_rarity('swamp'/'CHK', 'Basic Land').
card_artist('swamp'/'CHK', 'Jim Nelson').
card_number('swamp'/'CHK', '296').
card_multiverse_id('swamp'/'CHK', '79066').

card_in_set('swamp', 'CHK').
card_original_type('swamp'/'CHK', 'Basic Land — Swamp').
card_original_text('swamp'/'CHK', 'B').
card_image_name('swamp'/'CHK', 'swamp3').
card_uid('swamp'/'CHK', 'CHK:Swamp:swamp3').
card_rarity('swamp'/'CHK', 'Basic Land').
card_artist('swamp'/'CHK', 'Jim Nelson').
card_number('swamp'/'CHK', '297').
card_multiverse_id('swamp'/'CHK', '79067').

card_in_set('swamp', 'CHK').
card_original_type('swamp'/'CHK', 'Basic Land — Swamp').
card_original_text('swamp'/'CHK', 'B').
card_image_name('swamp'/'CHK', 'swamp4').
card_uid('swamp'/'CHK', 'CHK:Swamp:swamp4').
card_rarity('swamp'/'CHK', 'Basic Land').
card_artist('swamp'/'CHK', 'Jim Nelson').
card_number('swamp'/'CHK', '298').
card_multiverse_id('swamp'/'CHK', '79073').

card_in_set('swirl the mists', 'CHK').
card_original_type('swirl the mists'/'CHK', 'Enchantment').
card_original_text('swirl the mists'/'CHK', 'As Swirl the Mists comes into play, choose a color word.\nAll instances of color words on spells and permanents become the chosen color word.').
card_first_print('swirl the mists', 'CHK').
card_image_name('swirl the mists'/'CHK', 'swirl the mists').
card_uid('swirl the mists'/'CHK', 'CHK:Swirl the Mists:swirl the mists').
card_rarity('swirl the mists'/'CHK', 'Rare').
card_artist('swirl the mists'/'CHK', 'Arnie Swekel').
card_number('swirl the mists'/'CHK', '94').
card_multiverse_id('swirl the mists'/'CHK', '80276').

card_in_set('takeno, samurai general', 'CHK').
card_original_type('takeno, samurai general'/'CHK', 'Legendary Creature — Human Samurai').
card_original_text('takeno, samurai general'/'CHK', 'Bushido 2 (When this blocks or becomes blocked, it gets +2/+2 until end of turn.)\nEach other Samurai you control gets +1/+1 for each point of bushido it has.').
card_first_print('takeno, samurai general', 'CHK').
card_image_name('takeno, samurai general'/'CHK', 'takeno, samurai general').
card_uid('takeno, samurai general'/'CHK', 'CHK:Takeno, Samurai General:takeno, samurai general').
card_rarity('takeno, samurai general'/'CHK', 'Rare').
card_artist('takeno, samurai general'/'CHK', 'Matt Cavotta').
card_number('takeno, samurai general'/'CHK', '46').
card_multiverse_id('takeno, samurai general'/'CHK', '75302').

card_in_set('tatsumasa, the dragon\'s fang', 'CHK').
card_original_type('tatsumasa, the dragon\'s fang'/'CHK', 'Legendary Artifact — Equipment').
card_original_text('tatsumasa, the dragon\'s fang'/'CHK', 'Equipped creature gets +5/+5.\n{6}, Remove Tatsumasa, the Dragon\'s Fang from the game: Put a 5/5 blue Dragon Spirit creature token with flying into play. Return Tatsumasa to play under its owner\'s control when that token is put into a graveyard.\nEquip {3}').
card_first_print('tatsumasa, the dragon\'s fang', 'CHK').
card_image_name('tatsumasa, the dragon\'s fang'/'CHK', 'tatsumasa, the dragon\'s fang').
card_uid('tatsumasa, the dragon\'s fang'/'CHK', 'CHK:Tatsumasa, the Dragon\'s Fang:tatsumasa, the dragon\'s fang').
card_rarity('tatsumasa, the dragon\'s fang'/'CHK', 'Rare').
card_artist('tatsumasa, the dragon\'s fang'/'CHK', 'Martina Pilcerova').
card_number('tatsumasa, the dragon\'s fang'/'CHK', '270').
card_multiverse_id('tatsumasa, the dragon\'s fang'/'CHK', '75291').

card_in_set('teller of tales', 'CHK').
card_original_type('teller of tales'/'CHK', 'Creature — Spirit').
card_original_text('teller of tales'/'CHK', 'Flying\nWhenever you play a Spirit or Arcane spell, tap or untap target creature.').
card_first_print('teller of tales', 'CHK').
card_image_name('teller of tales'/'CHK', 'teller of tales').
card_uid('teller of tales'/'CHK', 'CHK:Teller of Tales:teller of tales').
card_rarity('teller of tales'/'CHK', 'Common').
card_artist('teller of tales'/'CHK', 'Jim Murray').
card_number('teller of tales'/'CHK', '95').
card_flavor_text('teller of tales'/'CHK', 'Words never uttered by mortals flowed incessantly from its many mouths.').
card_multiverse_id('teller of tales'/'CHK', '75285').

card_in_set('tenza, godo\'s maul', 'CHK').
card_original_type('tenza, godo\'s maul'/'CHK', 'Legendary Artifact — Equipment').
card_original_text('tenza, godo\'s maul'/'CHK', 'Equipped creature gets +1/+1. As long as it\'s legendary, it gets an additional +2/+2. As long as it\'s red, it has trample.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('tenza, godo\'s maul', 'CHK').
card_image_name('tenza, godo\'s maul'/'CHK', 'tenza, godo\'s maul').
card_uid('tenza, godo\'s maul'/'CHK', 'CHK:Tenza, Godo\'s Maul:tenza, godo\'s maul').
card_rarity('tenza, godo\'s maul'/'CHK', 'Uncommon').
card_artist('tenza, godo\'s maul'/'CHK', 'Paolo Parente').
card_number('tenza, godo\'s maul'/'CHK', '271').
card_multiverse_id('tenza, godo\'s maul'/'CHK', '79231').

card_in_set('terashi\'s cry', 'CHK').
card_original_type('terashi\'s cry'/'CHK', 'Sorcery — Arcane').
card_original_text('terashi\'s cry'/'CHK', 'Tap up to three target creatures.').
card_first_print('terashi\'s cry', 'CHK').
card_image_name('terashi\'s cry'/'CHK', 'terashi\'s cry').
card_uid('terashi\'s cry'/'CHK', 'CHK:Terashi\'s Cry:terashi\'s cry').
card_rarity('terashi\'s cry'/'CHK', 'Common').
card_artist('terashi\'s cry'/'CHK', 'Jim Murray').
card_number('terashi\'s cry'/'CHK', '47').
card_flavor_text('terashi\'s cry'/'CHK', '\"The sun kami is terrible to behold. When its temper flares, best seek shade and pray for forgiveness.\"\n—Sensei Golden-Tail').
card_multiverse_id('terashi\'s cry'/'CHK', '80285').

card_in_set('the unspeakable', 'CHK').
card_original_type('the unspeakable'/'CHK', 'Legendary Creature — Spirit').
card_original_text('the unspeakable'/'CHK', 'Flying, trample\nWhenever The Unspeakable deals combat damage to a player, you may return target Arcane card from your graveyard to your hand.').
card_first_print('the unspeakable', 'CHK').
card_image_name('the unspeakable'/'CHK', 'the unspeakable').
card_uid('the unspeakable'/'CHK', 'CHK:The Unspeakable:the unspeakable').
card_rarity('the unspeakable'/'CHK', 'Rare').
card_artist('the unspeakable'/'CHK', 'Khang Le').
card_number('the unspeakable'/'CHK', '98').
card_flavor_text('the unspeakable'/'CHK', 'It is madness that drives men to seek forbidden knowledge, and madness has given it form.').
card_multiverse_id('the unspeakable'/'CHK', '78693').

card_in_set('thief of hope', 'CHK').
card_original_type('thief of hope'/'CHK', 'Creature — Spirit').
card_original_text('thief of hope'/'CHK', 'Whenever you play a Spirit or Arcane spell, target opponent loses 1 life and you gain 1 life.\nSoulshift 2 (When this is put into a graveyard from play, you may return target Spirit card with converted mana cost 2 or less from your graveyard to your hand.)').
card_first_print('thief of hope', 'CHK').
card_image_name('thief of hope'/'CHK', 'thief of hope').
card_uid('thief of hope'/'CHK', 'CHK:Thief of Hope:thief of hope').
card_rarity('thief of hope'/'CHK', 'Uncommon').
card_artist('thief of hope'/'CHK', 'Tim Hildebrandt').
card_number('thief of hope'/'CHK', '147').
card_multiverse_id('thief of hope'/'CHK', '77920').

card_in_set('thoughtbind', 'CHK').
card_original_type('thoughtbind'/'CHK', 'Instant').
card_original_text('thoughtbind'/'CHK', 'Counter target spell with converted mana cost 4 or less.').
card_first_print('thoughtbind', 'CHK').
card_image_name('thoughtbind'/'CHK', 'thoughtbind').
card_uid('thoughtbind'/'CHK', 'CHK:Thoughtbind:thoughtbind').
card_rarity('thoughtbind'/'CHK', 'Common').
card_artist('thoughtbind'/'CHK', 'Rob Alexander').
card_number('thoughtbind'/'CHK', '96').
card_flavor_text('thoughtbind'/'CHK', '\"As the rest of the mortal world waged war, Lady Azami and her students invaded tomes of knowledge. Their search yielded spells critical in the fight.\"\n—Observations of the Kami War').
card_multiverse_id('thoughtbind'/'CHK', '50433').

card_in_set('thousand-legged kami', 'CHK').
card_original_type('thousand-legged kami'/'CHK', 'Creature — Spirit').
card_original_text('thousand-legged kami'/'CHK', 'Soulshift 7 (When this is put into a graveyard from play, you may return target Spirit card with converted mana cost 7 or less from your graveyard to your hand.)').
card_first_print('thousand-legged kami', 'CHK').
card_image_name('thousand-legged kami'/'CHK', 'thousand-legged kami').
card_uid('thousand-legged kami'/'CHK', 'CHK:Thousand-legged Kami:thousand-legged kami').
card_rarity('thousand-legged kami'/'CHK', 'Uncommon').
card_artist('thousand-legged kami'/'CHK', 'Nottsuo').
card_number('thousand-legged kami'/'CHK', '246').
card_multiverse_id('thousand-legged kami'/'CHK', '50438').

card_in_set('through the breach', 'CHK').
card_original_type('through the breach'/'CHK', 'Instant — Arcane').
card_original_text('through the breach'/'CHK', 'Put a creature card from your hand into play. That creature has haste. Sacrifice that creature at end of turn.\nSplice onto Arcane {2}{R}{R} (As you play an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_first_print('through the breach', 'CHK').
card_image_name('through the breach'/'CHK', 'through the breach').
card_uid('through the breach'/'CHK', 'CHK:Through the Breach:through the breach').
card_rarity('through the breach'/'CHK', 'Rare').
card_artist('through the breach'/'CHK', 'Hugh Jamieson').
card_number('through the breach'/'CHK', '193').
card_multiverse_id('through the breach'/'CHK', '80250').

card_in_set('tide of war', 'CHK').
card_original_type('tide of war'/'CHK', 'Enchantment').
card_original_text('tide of war'/'CHK', 'Whenever one or more creatures blocks, flip a coin. If you win the flip, the defending player sacrifices all blocking creatures. Otherwise, the attacking player sacrifices the blocked creatures.').
card_first_print('tide of war', 'CHK').
card_image_name('tide of war'/'CHK', 'tide of war').
card_uid('tide of war'/'CHK', 'CHK:Tide of War:tide of war').
card_rarity('tide of war'/'CHK', 'Rare').
card_artist('tide of war'/'CHK', 'Wayne Reynolds').
card_number('tide of war'/'CHK', '194').
card_multiverse_id('tide of war'/'CHK', '78606').

card_in_set('time of need', 'CHK').
card_original_type('time of need'/'CHK', 'Sorcery').
card_original_text('time of need'/'CHK', 'Search your library for a legendary creature card, reveal it, and put it into your hand. Then shuffle your library.').
card_first_print('time of need', 'CHK').
card_image_name('time of need'/'CHK', 'time of need').
card_uid('time of need'/'CHK', 'CHK:Time of Need:time of need').
card_rarity('time of need'/'CHK', 'Uncommon').
card_artist('time of need'/'CHK', 'Dany Orizio').
card_number('time of need'/'CHK', '247').
card_flavor_text('time of need'/'CHK', '\"When the kumo attacked the monks\' sacred shrine, a mournful toll for help echoed through the forest. Thus began the Battle of Silk, which would last six years.\"\n—Great Battles of Kamigawa').
card_multiverse_id('time of need'/'CHK', '78987').

card_in_set('time stop', 'CHK').
card_original_type('time stop'/'CHK', 'Instant').
card_original_text('time stop'/'CHK', 'End the turn. (Remove all spells and abilities on the stack from the game, including this card. The player whose turn it is discards down to his or her maximum hand size. Damage wears off, and \"this turn\" and \"until end of turn\" effects end.)').
card_first_print('time stop', 'CHK').
card_image_name('time stop'/'CHK', 'time stop').
card_uid('time stop'/'CHK', 'CHK:Time Stop:time stop').
card_rarity('time stop'/'CHK', 'Rare').
card_artist('time stop'/'CHK', 'Scott M. Fischer').
card_number('time stop'/'CHK', '97').
card_multiverse_id('time stop'/'CHK', '78184').

card_in_set('tobita, master of winds', 'CHK').
card_original_type('tobita, master of winds'/'CHK', 'Creature — Human Wizard').
card_original_text('tobita, master of winds'/'CHK', 'When Student of Elements has flying, flip it.\n-----\nTobita, Master of Winds\nLegendary Creature Human Wizard\n3/3\nCreatures you control have flying.').
card_first_print('tobita, master of winds', 'CHK').
card_image_name('tobita, master of winds'/'CHK', 'tobita, master of winds').
card_uid('tobita, master of winds'/'CHK', 'CHK:Tobita, Master of Winds:tobita, master of winds').
card_rarity('tobita, master of winds'/'CHK', 'Uncommon').
card_artist('tobita, master of winds'/'CHK', 'Ittoku').
card_number('tobita, master of winds'/'CHK', '93b').
card_multiverse_id('tobita, master of winds'/'CHK', '78691').

card_in_set('tok-tok, volcano born', 'CHK').
card_original_type('tok-tok, volcano born'/'CHK', 'Creature — Goblin Warrior').
card_original_text('tok-tok, volcano born'/'CHK', 'Haste\nWhenever Akki Lavarunner deals damage to an opponent, flip it.\n-----\nTok-Tok, Volcano Born\nLegendary Creature Goblin Shaman\n2/2\nProtection from red\nIf a red source would deal damage to a player, it deals that much damage plus 1 to that player instead.').
card_first_print('tok-tok, volcano born', 'CHK').
card_image_name('tok-tok, volcano born'/'CHK', 'tok-tok, volcano born').
card_uid('tok-tok, volcano born'/'CHK', 'CHK:Tok-Tok, Volcano Born:tok-tok, volcano born').
card_rarity('tok-tok, volcano born'/'CHK', 'Rare').
card_artist('tok-tok, volcano born'/'CHK', 'Matt Cavotta').
card_number('tok-tok, volcano born'/'CHK', '153b').
card_multiverse_id('tok-tok, volcano born'/'CHK', '78694').

card_in_set('tomoya the revealer', 'CHK').
card_original_type('tomoya the revealer'/'CHK', 'Creature — Human Wizard').
card_original_text('tomoya the revealer'/'CHK', '{2}{U}, {T}: Draw a card. If you have nine or more cards in hand, flip Jushi Apprentice.\n-----\nTomoya the Revealer\nLegendary Creature Human Wizard\n2/3\n{3}{U}{U}, {T}: Target player draws X cards, where X is the number of cards in your hand.').
card_first_print('tomoya the revealer', 'CHK').
card_image_name('tomoya the revealer'/'CHK', 'tomoya the revealer').
card_uid('tomoya the revealer'/'CHK', 'CHK:Tomoya the Revealer:tomoya the revealer').
card_rarity('tomoya the revealer'/'CHK', 'Rare').
card_artist('tomoya the revealer'/'CHK', 'Glen Angus').
card_number('tomoya the revealer'/'CHK', '70b').
card_multiverse_id('tomoya the revealer'/'CHK', '78686').

card_in_set('tranquil garden', 'CHK').
card_original_type('tranquil garden'/'CHK', 'Land').
card_original_text('tranquil garden'/'CHK', '{T}: Add {1} to your mana pool.\n{T}: Add {G} or {W} to your mana pool. Tranquil Garden doesn\'t untap during your next untap step.').
card_first_print('tranquil garden', 'CHK').
card_image_name('tranquil garden'/'CHK', 'tranquil garden').
card_uid('tranquil garden'/'CHK', 'CHK:Tranquil Garden:tranquil garden').
card_rarity('tranquil garden'/'CHK', 'Uncommon').
card_artist('tranquil garden'/'CHK', 'John Avon').
card_number('tranquil garden'/'CHK', '284').
card_multiverse_id('tranquil garden'/'CHK', '79196').

card_in_set('uba mask', 'CHK').
card_original_type('uba mask'/'CHK', 'Artifact').
card_original_text('uba mask'/'CHK', 'If a player would draw a card, that player removes that card from the game face up instead.\nEach player may play cards he or she removed from the game with Uba Mask this turn.').
card_first_print('uba mask', 'CHK').
card_image_name('uba mask'/'CHK', 'uba mask').
card_uid('uba mask'/'CHK', 'CHK:Uba Mask:uba mask').
card_rarity('uba mask'/'CHK', 'Rare').
card_artist('uba mask'/'CHK', 'Randy Gallegos').
card_number('uba mask'/'CHK', '272').
card_multiverse_id('uba mask'/'CHK', '75254').

card_in_set('uncontrollable anger', 'CHK').
card_original_type('uncontrollable anger'/'CHK', 'Enchant Creature').
card_original_text('uncontrollable anger'/'CHK', 'You may play Uncontrollable Anger any time you could play an instant.\nEnchanted creature gets +2/+2 and attacks each turn if able.').
card_first_print('uncontrollable anger', 'CHK').
card_image_name('uncontrollable anger'/'CHK', 'uncontrollable anger').
card_uid('uncontrollable anger'/'CHK', 'CHK:Uncontrollable Anger:uncontrollable anger').
card_rarity('uncontrollable anger'/'CHK', 'Common').
card_artist('uncontrollable anger'/'CHK', 'Matt Thompson').
card_number('uncontrollable anger'/'CHK', '195').
card_flavor_text('uncontrollable anger'/'CHK', 'To an akki warrior, gravity is the direction of the nearest unsmashed face.').
card_multiverse_id('uncontrollable anger'/'CHK', '50315').

card_in_set('unearthly blizzard', 'CHK').
card_original_type('unearthly blizzard'/'CHK', 'Sorcery — Arcane').
card_original_text('unearthly blizzard'/'CHK', 'Up to three target creatures can\'t block this turn.').
card_first_print('unearthly blizzard', 'CHK').
card_image_name('unearthly blizzard'/'CHK', 'unearthly blizzard').
card_uid('unearthly blizzard'/'CHK', 'CHK:Unearthly Blizzard:unearthly blizzard').
card_rarity('unearthly blizzard'/'CHK', 'Common').
card_artist('unearthly blizzard'/'CHK', 'Joel Thomas').
card_number('unearthly blizzard'/'CHK', '196').
card_flavor_text('unearthly blizzard'/'CHK', '\"We are trapped. The mountains and blinding kami storms have made us hopelessly lost. We are starving. In the name of all things sacred, please, send help . . . .\"\n—Lost Battalion, final message to General Takeno').
card_multiverse_id('unearthly blizzard'/'CHK', '50379').

card_in_set('unnatural speed', 'CHK').
card_original_type('unnatural speed'/'CHK', 'Instant — Arcane').
card_original_text('unnatural speed'/'CHK', 'Target creature gains haste until end of turn.').
card_first_print('unnatural speed', 'CHK').
card_image_name('unnatural speed'/'CHK', 'unnatural speed').
card_uid('unnatural speed'/'CHK', 'CHK:Unnatural Speed:unnatural speed').
card_rarity('unnatural speed'/'CHK', 'Common').
card_artist('unnatural speed'/'CHK', 'Wayne Reynolds').
card_number('unnatural speed'/'CHK', '197').
card_flavor_text('unnatural speed'/'CHK', '\"How can we hope to match the speed of lightning? The fury of storms? The power of mountains? The answer is simple. We cannot. I advise against this war.\"\n—Sensei Hisoka, letter to Lord Konda').
card_multiverse_id('unnatural speed'/'CHK', '77926').

card_in_set('untaidake, the cloud keeper', 'CHK').
card_original_type('untaidake, the cloud keeper'/'CHK', 'Legendary Land').
card_original_text('untaidake, the cloud keeper'/'CHK', 'Untaidake, the Cloud Keeper comes into play tapped.\n{T}, Pay 2 life: Add {2} to your mana pool. Spend this mana only to play legendary spells.').
card_first_print('untaidake, the cloud keeper', 'CHK').
card_image_name('untaidake, the cloud keeper'/'CHK', 'untaidake, the cloud keeper').
card_uid('untaidake, the cloud keeper'/'CHK', 'CHK:Untaidake, the Cloud Keeper:untaidake, the cloud keeper').
card_rarity('untaidake, the cloud keeper'/'CHK', 'Rare').
card_artist('untaidake, the cloud keeper'/'CHK', 'John Avon').
card_number('untaidake, the cloud keeper'/'CHK', '285').
card_flavor_text('untaidake, the cloud keeper'/'CHK', 'Untaidake is the needle that weaves the fabric of creation.').
card_multiverse_id('untaidake, the cloud keeper'/'CHK', '79218').

card_in_set('uyo, silent prophet', 'CHK').
card_original_type('uyo, silent prophet'/'CHK', 'Legendary Creature — Moonfolk Wizard').
card_original_text('uyo, silent prophet'/'CHK', 'Flying\n{2}, Return two lands you control to their owner\'s hand: Copy target instant or sorcery spell. You may choose new targets for the copy.').
card_first_print('uyo, silent prophet', 'CHK').
card_image_name('uyo, silent prophet'/'CHK', 'uyo, silent prophet').
card_uid('uyo, silent prophet'/'CHK', 'CHK:Uyo, Silent Prophet:uyo, silent prophet').
card_rarity('uyo, silent prophet'/'CHK', 'Rare').
card_artist('uyo, silent prophet'/'CHK', 'John Bolton').
card_number('uyo, silent prophet'/'CHK', '99').
card_multiverse_id('uyo, silent prophet'/'CHK', '78855').

card_in_set('vassal\'s duty', 'CHK').
card_original_type('vassal\'s duty'/'CHK', 'Enchantment').
card_original_text('vassal\'s duty'/'CHK', '{1}: The next 1 damage that would be dealt to target legendary creature you control this turn is dealt to you instead.').
card_first_print('vassal\'s duty', 'CHK').
card_image_name('vassal\'s duty'/'CHK', 'vassal\'s duty').
card_uid('vassal\'s duty'/'CHK', 'CHK:Vassal\'s Duty:vassal\'s duty').
card_rarity('vassal\'s duty'/'CHK', 'Rare').
card_artist('vassal\'s duty'/'CHK', 'Dave Dorman').
card_number('vassal\'s duty'/'CHK', '48').
card_flavor_text('vassal\'s duty'/'CHK', '\"My life is yours, my lord. There is no greater service than to yield it for your safety.\"').
card_multiverse_id('vassal\'s duty'/'CHK', '79199').

card_in_set('venerable kumo', 'CHK').
card_original_type('venerable kumo'/'CHK', 'Creature — Spirit').
card_original_text('venerable kumo'/'CHK', 'Venerable Kumo may block as though it had flying.\nSoulshift 4 (When this is put into a graveyard from play, you may return target Spirit card with converted mana cost 4 or less from your graveyard to your hand.)').
card_first_print('venerable kumo', 'CHK').
card_image_name('venerable kumo'/'CHK', 'venerable kumo').
card_uid('venerable kumo'/'CHK', 'CHK:Venerable Kumo:venerable kumo').
card_rarity('venerable kumo'/'CHK', 'Common').
card_artist('venerable kumo'/'CHK', 'Carl Critchlow').
card_number('venerable kumo'/'CHK', '248').
card_multiverse_id('venerable kumo'/'CHK', '78979').

card_in_set('vigilance', 'CHK').
card_original_type('vigilance'/'CHK', 'Enchant Creature').
card_original_text('vigilance'/'CHK', 'Enchanted creature has vigilance. (Attacking doesn\'t cause it to tap.)').
card_first_print('vigilance', 'CHK').
card_image_name('vigilance'/'CHK', 'vigilance').
card_uid('vigilance'/'CHK', 'CHK:Vigilance:vigilance').
card_rarity('vigilance'/'CHK', 'Common').
card_artist('vigilance'/'CHK', 'Tsutomu Kawade').
card_number('vigilance'/'CHK', '49').
card_flavor_text('vigilance'/'CHK', '\"Put a spear in a peasant\'s hands, and you have an expendable troop. Put a purpose in his heart, and you win a warrior.\"\n—Sensei Golden-Tail').
card_multiverse_id('vigilance'/'CHK', '50414').

card_in_set('villainous ogre', 'CHK').
card_original_type('villainous ogre'/'CHK', 'Creature — Ogre Warrior').
card_original_text('villainous ogre'/'CHK', 'Villainous Ogre can\'t block.\nAs long as you control a Demon, Villainous Ogre has \"{B}: Regenerate Villainous Ogre.\"').
card_first_print('villainous ogre', 'CHK').
card_image_name('villainous ogre'/'CHK', 'villainous ogre').
card_uid('villainous ogre'/'CHK', 'CHK:Villainous Ogre:villainous ogre').
card_rarity('villainous ogre'/'CHK', 'Common').
card_artist('villainous ogre'/'CHK', 'Tony Szczudlo').
card_number('villainous ogre'/'CHK', '148').
card_flavor_text('villainous ogre'/'CHK', '\"The war saw the ogres emerge from their caves, reeking of blood, with the power of oni in their veins.\"\n—Observations of the Kami War').
card_multiverse_id('villainous ogre'/'CHK', '80520').

card_in_set('vine kami', 'CHK').
card_original_type('vine kami'/'CHK', 'Creature — Spirit').
card_original_text('vine kami'/'CHK', 'Vine Kami can\'t be blocked except by two or more creatures.\nSoulshift 6 (When this is put into a graveyard from play, you may return target Spirit card with converted mana cost 6 or less from your graveyard to your hand.)').
card_first_print('vine kami', 'CHK').
card_image_name('vine kami'/'CHK', 'vine kami').
card_uid('vine kami'/'CHK', 'CHK:Vine Kami:vine kami').
card_rarity('vine kami'/'CHK', 'Common').
card_artist('vine kami'/'CHK', 'Tsutomu Kawade').
card_number('vine kami'/'CHK', '249').
card_multiverse_id('vine kami'/'CHK', '82001').

card_in_set('waking nightmare', 'CHK').
card_original_type('waking nightmare'/'CHK', 'Sorcery — Arcane').
card_original_text('waking nightmare'/'CHK', 'Target player discards two cards.').
card_first_print('waking nightmare', 'CHK').
card_image_name('waking nightmare'/'CHK', 'waking nightmare').
card_uid('waking nightmare'/'CHK', 'CHK:Waking Nightmare:waking nightmare').
card_rarity('waking nightmare'/'CHK', 'Common').
card_artist('waking nightmare'/'CHK', 'Mitch Cotie').
card_number('waking nightmare'/'CHK', '149').
card_flavor_text('waking nightmare'/'CHK', '\"Once each year, the oni and other evil spirits paraded through villages to disturb mortals\' sleep. During the war, this parade became a nightly event.\"\n—Observations of the Kami War').
card_multiverse_id('waking nightmare'/'CHK', '78963').

card_in_set('wandering ones', 'CHK').
card_original_type('wandering ones'/'CHK', 'Creature — Spirit').
card_original_text('wandering ones'/'CHK', '').
card_first_print('wandering ones', 'CHK').
card_image_name('wandering ones'/'CHK', 'wandering ones').
card_uid('wandering ones'/'CHK', 'CHK:Wandering Ones:wandering ones').
card_rarity('wandering ones'/'CHK', 'Common').
card_artist('wandering ones'/'CHK', 'Heather Hudson').
card_number('wandering ones'/'CHK', '100').
card_flavor_text('wandering ones'/'CHK', '\"I saw them once, when I was a child. They led me to my parents\' arms when I was lost. Why have they abandoned me now? Why won\'t they take me home again?\"\n—Unnamed beggar').
card_multiverse_id('wandering ones'/'CHK', '50345').

card_in_set('waterveil cavern', 'CHK').
card_original_type('waterveil cavern'/'CHK', 'Land').
card_original_text('waterveil cavern'/'CHK', '{T}: Add {1} to your mana pool.\n{T}: Add {U} or {B} to your mana pool. Waterveil Cavern doesn\'t untap during your next untap step.').
card_first_print('waterveil cavern', 'CHK').
card_image_name('waterveil cavern'/'CHK', 'waterveil cavern').
card_uid('waterveil cavern'/'CHK', 'CHK:Waterveil Cavern:waterveil cavern').
card_rarity('waterveil cavern'/'CHK', 'Uncommon').
card_artist('waterveil cavern'/'CHK', 'John Avon').
card_number('waterveil cavern'/'CHK', '286').
card_multiverse_id('waterveil cavern'/'CHK', '79191').

card_in_set('wear away', 'CHK').
card_original_type('wear away'/'CHK', 'Instant — Arcane').
card_original_text('wear away'/'CHK', 'Destroy target artifact or enchantment.\nSplice onto Arcane {3}{G} (As you play an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_first_print('wear away', 'CHK').
card_image_name('wear away'/'CHK', 'wear away').
card_uid('wear away'/'CHK', 'CHK:Wear Away:wear away').
card_rarity('wear away'/'CHK', 'Common').
card_artist('wear away'/'CHK', 'Tim Hildebrandt').
card_number('wear away'/'CHK', '250').
card_multiverse_id('wear away'/'CHK', '50268').

card_in_set('wicked akuba', 'CHK').
card_original_type('wicked akuba'/'CHK', 'Creature — Spirit').
card_original_text('wicked akuba'/'CHK', '{B}: Target player dealt damage by Wicked Akuba this turn loses 1 life.').
card_first_print('wicked akuba', 'CHK').
card_image_name('wicked akuba'/'CHK', 'wicked akuba').
card_uid('wicked akuba'/'CHK', 'CHK:Wicked Akuba:wicked akuba').
card_rarity('wicked akuba'/'CHK', 'Common').
card_artist('wicked akuba'/'CHK', 'Ittoku').
card_number('wicked akuba'/'CHK', '150').
card_flavor_text('wicked akuba'/'CHK', 'The sound of children weeping is a song that fills its heart with joy.').
card_multiverse_id('wicked akuba'/'CHK', '50432').

card_in_set('yamabushi\'s flame', 'CHK').
card_original_type('yamabushi\'s flame'/'CHK', 'Instant').
card_original_text('yamabushi\'s flame'/'CHK', 'Yamabushi\'s Flame deals 3 damage to target creature or player. If a creature dealt damage this way would be put into a graveyard this turn, remove it from the game instead.').
card_first_print('yamabushi\'s flame', 'CHK').
card_image_name('yamabushi\'s flame'/'CHK', 'yamabushi\'s flame').
card_uid('yamabushi\'s flame'/'CHK', 'CHK:Yamabushi\'s Flame:yamabushi\'s flame').
card_rarity('yamabushi\'s flame'/'CHK', 'Common').
card_artist('yamabushi\'s flame'/'CHK', 'Christopher Moeller').
card_number('yamabushi\'s flame'/'CHK', '198').
card_multiverse_id('yamabushi\'s flame'/'CHK', '50292').

card_in_set('yamabushi\'s storm', 'CHK').
card_original_type('yamabushi\'s storm'/'CHK', 'Sorcery').
card_original_text('yamabushi\'s storm'/'CHK', 'Yamabushi\'s Storm deals 1 damage to each creature. If a creature dealt damage this way would be put into a graveyard this turn, remove it from the game instead.').
card_first_print('yamabushi\'s storm', 'CHK').
card_image_name('yamabushi\'s storm'/'CHK', 'yamabushi\'s storm').
card_uid('yamabushi\'s storm'/'CHK', 'CHK:Yamabushi\'s Storm:yamabushi\'s storm').
card_rarity('yamabushi\'s storm'/'CHK', 'Common').
card_artist('yamabushi\'s storm'/'CHK', 'Wayne England').
card_number('yamabushi\'s storm'/'CHK', '199').
card_multiverse_id('yamabushi\'s storm'/'CHK', '50346').

card_in_set('yosei, the morning star', 'CHK').
card_original_type('yosei, the morning star'/'CHK', 'Legendary Creature — Dragon Spirit').
card_original_text('yosei, the morning star'/'CHK', 'Flying\nWhen Yosei, the Morning Star is put into a graveyard from play, target player skips his or her next untap step. Tap up to five target permanents that player controls.').
card_first_print('yosei, the morning star', 'CHK').
card_image_name('yosei, the morning star'/'CHK', 'yosei, the morning star').
card_uid('yosei, the morning star'/'CHK', 'CHK:Yosei, the Morning Star:yosei, the morning star').
card_rarity('yosei, the morning star'/'CHK', 'Rare').
card_artist('yosei, the morning star'/'CHK', 'Hiro Izawa').
card_number('yosei, the morning star'/'CHK', '50').
card_multiverse_id('yosei, the morning star'/'CHK', '78590').

card_in_set('zo-zu the punisher', 'CHK').
card_original_type('zo-zu the punisher'/'CHK', 'Legendary Creature — Goblin Warrior').
card_original_text('zo-zu the punisher'/'CHK', 'Whenever a land comes into play, Zo-Zu the Punisher deals 2 damage to that land\'s controller.').
card_first_print('zo-zu the punisher', 'CHK').
card_image_name('zo-zu the punisher'/'CHK', 'zo-zu the punisher').
card_uid('zo-zu the punisher'/'CHK', 'CHK:Zo-Zu the Punisher:zo-zu the punisher').
card_rarity('zo-zu the punisher'/'CHK', 'Rare').
card_artist('zo-zu the punisher'/'CHK', 'Matt Cavotta').
card_number('zo-zu the punisher'/'CHK', '200').
card_flavor_text('zo-zu the punisher'/'CHK', '\"He can cause a lot of pain and do it with no fuss. That\'s all good, but I just wish he didn\'t do it to us\"\n—Ku-Ku, akki poet').
card_multiverse_id('zo-zu the punisher'/'CHK', '80274').
