% Darksteel

set('DST').
set_name('DST', 'Darksteel').
set_release_date('DST', '2004-02-06').
set_border('DST', 'black').
set_type('DST', 'expansion').
set_block('DST', 'Mirrodin').

card_in_set('æther snap', 'DST').
card_original_type('æther snap'/'DST', 'Sorcery').
card_original_text('æther snap'/'DST', 'Remove all counters from all permanents and remove all tokens from the game.').
card_first_print('æther snap', 'DST').
card_image_name('æther snap'/'DST', 'aether snap').
card_uid('æther snap'/'DST', 'DST:Æther Snap:aether snap').
card_rarity('æther snap'/'DST', 'Rare').
card_artist('æther snap'/'DST', 'Kev Walker').
card_number('æther snap'/'DST', '37').
card_flavor_text('æther snap'/'DST', '\"May you wake to find you were only ever a dream.\"\n—Mephidross curse').
card_multiverse_id('æther snap'/'DST', '49075').

card_in_set('æther vial', 'DST').
card_original_type('æther vial'/'DST', 'Artifact').
card_original_text('æther vial'/'DST', 'At the beginning of your upkeep, you may put a charge counter on Æther Vial.\n{T}: You may put a creature card with converted mana cost equal to the number of charge counters on Æther Vial from your hand into play.').
card_first_print('æther vial', 'DST').
card_image_name('æther vial'/'DST', 'aether vial').
card_uid('æther vial'/'DST', 'DST:Æther Vial:aether vial').
card_rarity('æther vial'/'DST', 'Uncommon').
card_artist('æther vial'/'DST', 'Greg Hildebrandt').
card_number('æther vial'/'DST', '91').
card_multiverse_id('æther vial'/'DST', '48146').

card_in_set('ageless entity', 'DST').
card_original_type('ageless entity'/'DST', 'Creature — Elemental').
card_original_text('ageless entity'/'DST', 'Whenever you gain life, put that many +1/+1 counters on Ageless Entity.').
card_first_print('ageless entity', 'DST').
card_image_name('ageless entity'/'DST', 'ageless entity').
card_uid('ageless entity'/'DST', 'DST:Ageless Entity:ageless entity').
card_rarity('ageless entity'/'DST', 'Rare').
card_artist('ageless entity'/'DST', 'Jeff Miracola').
card_number('ageless entity'/'DST', '73').
card_flavor_text('ageless entity'/'DST', 'Tel-Jilad\'s sworn protectors are the trolls, yet more fearsome protectors were created by the Tangle itself.').
card_multiverse_id('ageless entity'/'DST', '46148').

card_in_set('angel\'s feather', 'DST').
card_original_type('angel\'s feather'/'DST', 'Artifact').
card_original_text('angel\'s feather'/'DST', 'Whenever a player plays a white spell, you may gain 1 life.').
card_first_print('angel\'s feather', 'DST').
card_image_name('angel\'s feather'/'DST', 'angel\'s feather').
card_uid('angel\'s feather'/'DST', 'DST:Angel\'s Feather:angel\'s feather').
card_rarity('angel\'s feather'/'DST', 'Uncommon').
card_artist('angel\'s feather'/'DST', 'Alan Pollack').
card_number('angel\'s feather'/'DST', '92').
card_flavor_text('angel\'s feather'/'DST', 'If taken, it cuts the hand that clutches it. If given, it heals the hand that holds it.').
card_multiverse_id('angel\'s feather'/'DST', '72686').

card_in_set('arcane spyglass', 'DST').
card_original_type('arcane spyglass'/'DST', 'Artifact').
card_original_text('arcane spyglass'/'DST', '{2}, {T}, Sacrifice a land: Draw a card and put a charge counter on Arcane Spyglass.\nRemove three charge counters from Arcane Spyglass: Draw a card.').
card_first_print('arcane spyglass', 'DST').
card_image_name('arcane spyglass'/'DST', 'arcane spyglass').
card_uid('arcane spyglass'/'DST', 'DST:Arcane Spyglass:arcane spyglass').
card_rarity('arcane spyglass'/'DST', 'Common').
card_artist('arcane spyglass'/'DST', 'Trevor Hairsine').
card_number('arcane spyglass'/'DST', '93').
card_flavor_text('arcane spyglass'/'DST', 'It breaches not physical distances but temporal ones.').
card_multiverse_id('arcane spyglass'/'DST', '10670').

card_in_set('arcbound bruiser', 'DST').
card_original_type('arcbound bruiser'/'DST', 'Artifact Creature').
card_original_text('arcbound bruiser'/'DST', 'Modular 3 (This comes into play with three +1/+1 counters on it. When it\'s put into a graveyard, you may put its +1/+1 counters on target artifact creature.)').
card_first_print('arcbound bruiser', 'DST').
card_image_name('arcbound bruiser'/'DST', 'arcbound bruiser').
card_uid('arcbound bruiser'/'DST', 'DST:Arcbound Bruiser:arcbound bruiser').
card_rarity('arcbound bruiser'/'DST', 'Common').
card_artist('arcbound bruiser'/'DST', 'Christopher Moeller').
card_number('arcbound bruiser'/'DST', '94').
card_flavor_text('arcbound bruiser'/'DST', 'Break in case of emergency.').
card_multiverse_id('arcbound bruiser'/'DST', '46129').

card_in_set('arcbound crusher', 'DST').
card_original_type('arcbound crusher'/'DST', 'Artifact Creature').
card_original_text('arcbound crusher'/'DST', 'Trample\nWhenever another artifact comes into play, put a +1/+1 counter on Arcbound Crusher.\nModular 1 (This comes into play with a +1/+1 counter on it. When it\'s put into a graveyard, you may put its +1/+1 counters on target artifact creature.)').
card_first_print('arcbound crusher', 'DST').
card_image_name('arcbound crusher'/'DST', 'arcbound crusher').
card_uid('arcbound crusher'/'DST', 'DST:Arcbound Crusher:arcbound crusher').
card_rarity('arcbound crusher'/'DST', 'Uncommon').
card_artist('arcbound crusher'/'DST', 'Michael Sutfin').
card_number('arcbound crusher'/'DST', '95').
card_multiverse_id('arcbound crusher'/'DST', '50937').

card_in_set('arcbound fiend', 'DST').
card_original_type('arcbound fiend'/'DST', 'Artifact Creature').
card_original_text('arcbound fiend'/'DST', 'Fear\nAt the beginning of your upkeep, you may move a +1/+1 counter from target creature onto Arcbound Fiend.\nModular 3 (This comes into play with three +1/+1 counters on it. When it\'s put into a graveyard, you may put its +1/+1 counters on target artifact creature.)').
card_first_print('arcbound fiend', 'DST').
card_image_name('arcbound fiend'/'DST', 'arcbound fiend').
card_uid('arcbound fiend'/'DST', 'DST:Arcbound Fiend:arcbound fiend').
card_rarity('arcbound fiend'/'DST', 'Uncommon').
card_artist('arcbound fiend'/'DST', 'Ittoku').
card_number('arcbound fiend'/'DST', '96').
card_multiverse_id('arcbound fiend'/'DST', '50938').

card_in_set('arcbound hybrid', 'DST').
card_original_type('arcbound hybrid'/'DST', 'Artifact Creature').
card_original_text('arcbound hybrid'/'DST', 'Haste\nModular 2 (This comes into play with two +1/+1 counters on it. When it\'s put into a graveyard, you may put its +1/+1 counters on target artifact creature.)').
card_first_print('arcbound hybrid', 'DST').
card_image_name('arcbound hybrid'/'DST', 'arcbound hybrid').
card_uid('arcbound hybrid'/'DST', 'DST:Arcbound Hybrid:arcbound hybrid').
card_rarity('arcbound hybrid'/'DST', 'Common').
card_artist('arcbound hybrid'/'DST', 'Alan Pollack').
card_number('arcbound hybrid'/'DST', '97').
card_multiverse_id('arcbound hybrid'/'DST', '46101').

card_in_set('arcbound lancer', 'DST').
card_original_type('arcbound lancer'/'DST', 'Artifact Creature').
card_original_text('arcbound lancer'/'DST', 'First strike\nModular 4 (This comes into play with four +1/+1 counters on it. When it\'s put into a graveyard, you may put its +1/+1 counters on target artifact creature.)').
card_first_print('arcbound lancer', 'DST').
card_image_name('arcbound lancer'/'DST', 'arcbound lancer').
card_uid('arcbound lancer'/'DST', 'DST:Arcbound Lancer:arcbound lancer').
card_rarity('arcbound lancer'/'DST', 'Uncommon').
card_artist('arcbound lancer'/'DST', 'John Avon').
card_number('arcbound lancer'/'DST', '98').
card_multiverse_id('arcbound lancer'/'DST', '46130').

card_in_set('arcbound overseer', 'DST').
card_original_type('arcbound overseer'/'DST', 'Artifact Creature — Golem').
card_original_text('arcbound overseer'/'DST', 'At the beginning of your upkeep, put a +1/+1 counter on each creature with modular you control.\nModular 6 (This comes into play with six +1/+1 counters on it. When it\'s put into a graveyard, you may put its +1/+1 counters on target artifact creature.)').
card_first_print('arcbound overseer', 'DST').
card_image_name('arcbound overseer'/'DST', 'arcbound overseer').
card_uid('arcbound overseer'/'DST', 'DST:Arcbound Overseer:arcbound overseer').
card_rarity('arcbound overseer'/'DST', 'Rare').
card_artist('arcbound overseer'/'DST', 'Carl Critchlow').
card_number('arcbound overseer'/'DST', '99').
card_multiverse_id('arcbound overseer'/'DST', '39639').

card_in_set('arcbound ravager', 'DST').
card_original_type('arcbound ravager'/'DST', 'Artifact Creature').
card_original_text('arcbound ravager'/'DST', 'Sacrifice an artifact: Put a +1/+1 counter on Arcbound Ravager.\nModular 1 (This comes into play with a +1/+1 counter on it. When it\'s put into a graveyard, you may put its +1/+1 counters on target artifact creature.)').
card_first_print('arcbound ravager', 'DST').
card_image_name('arcbound ravager'/'DST', 'arcbound ravager').
card_uid('arcbound ravager'/'DST', 'DST:Arcbound Ravager:arcbound ravager').
card_rarity('arcbound ravager'/'DST', 'Rare').
card_artist('arcbound ravager'/'DST', 'Carl Critchlow').
card_number('arcbound ravager'/'DST', '100').
card_multiverse_id('arcbound ravager'/'DST', '50943').

card_in_set('arcbound reclaimer', 'DST').
card_original_type('arcbound reclaimer'/'DST', 'Artifact Creature — Golem').
card_original_text('arcbound reclaimer'/'DST', 'Remove a +1/+1 counter from Arcbound Reclaimer: Put target artifact card from your graveyard on top of your library.\nModular 2 (This comes into play with two +1/+1 counters on it. When it\'s put into a graveyard, you may put its +1/+1 counters on target artifact creature.)').
card_first_print('arcbound reclaimer', 'DST').
card_image_name('arcbound reclaimer'/'DST', 'arcbound reclaimer').
card_uid('arcbound reclaimer'/'DST', 'DST:Arcbound Reclaimer:arcbound reclaimer').
card_rarity('arcbound reclaimer'/'DST', 'Rare').
card_artist('arcbound reclaimer'/'DST', 'Jon Foster').
card_number('arcbound reclaimer'/'DST', '101').
card_multiverse_id('arcbound reclaimer'/'DST', '50528').

card_in_set('arcbound slith', 'DST').
card_original_type('arcbound slith'/'DST', 'Artifact Creature — Slith').
card_original_text('arcbound slith'/'DST', 'Whenever Arcbound Slith deals combat damage to a player, put a +1/+1 counter on it.\nModular 1 (This comes into play with a +1/+1 counter on it. When it\'s put into a graveyard, you may put its +1/+1 counters on target artifact creature.)').
card_first_print('arcbound slith', 'DST').
card_image_name('arcbound slith'/'DST', 'arcbound slith').
card_uid('arcbound slith'/'DST', 'DST:Arcbound Slith:arcbound slith').
card_rarity('arcbound slith'/'DST', 'Uncommon').
card_artist('arcbound slith'/'DST', 'Vance Kovacs').
card_number('arcbound slith'/'DST', '102').
card_multiverse_id('arcbound slith'/'DST', '50939').

card_in_set('arcbound stinger', 'DST').
card_original_type('arcbound stinger'/'DST', 'Artifact Creature').
card_original_text('arcbound stinger'/'DST', 'Flying\nModular 1 (This comes into play with a +1/+1 counter on it. When it\'s put into a graveyard, you may put its +1/+1 counters on target artifact creature.)').
card_first_print('arcbound stinger', 'DST').
card_image_name('arcbound stinger'/'DST', 'arcbound stinger').
card_uid('arcbound stinger'/'DST', 'DST:Arcbound Stinger:arcbound stinger').
card_rarity('arcbound stinger'/'DST', 'Common').
card_artist('arcbound stinger'/'DST', 'Darrell Riche').
card_number('arcbound stinger'/'DST', '103').
card_multiverse_id('arcbound stinger'/'DST', '46131').

card_in_set('arcbound worker', 'DST').
card_original_type('arcbound worker'/'DST', 'Artifact Creature').
card_original_text('arcbound worker'/'DST', 'Modular 1 (This comes into play with a +1/+1 counter on it. When it\'s put into a graveyard, you may put its +1/+1 counters on target artifact creature.)').
card_first_print('arcbound worker', 'DST').
card_image_name('arcbound worker'/'DST', 'arcbound worker').
card_uid('arcbound worker'/'DST', 'DST:Arcbound Worker:arcbound worker').
card_rarity('arcbound worker'/'DST', 'Common').
card_artist('arcbound worker'/'DST', 'Darrell Riche').
card_number('arcbound worker'/'DST', '104').
card_flavor_text('arcbound worker'/'DST', 'The parts are as strong as the whole.').
card_multiverse_id('arcbound worker'/'DST', '48604').

card_in_set('auriok glaivemaster', 'DST').
card_original_type('auriok glaivemaster'/'DST', 'Creature — Human Soldier').
card_original_text('auriok glaivemaster'/'DST', 'As long as Auriok Glaivemaster is equipped, it gets +1/+1 and has first strike.').
card_first_print('auriok glaivemaster', 'DST').
card_image_name('auriok glaivemaster'/'DST', 'auriok glaivemaster').
card_uid('auriok glaivemaster'/'DST', 'DST:Auriok Glaivemaster:auriok glaivemaster').
card_rarity('auriok glaivemaster'/'DST', 'Common').
card_artist('auriok glaivemaster'/'DST', 'John Matson').
card_number('auriok glaivemaster'/'DST', '1').
card_flavor_text('auriok glaivemaster'/'DST', '\"Give me a bit of steel and I\'ll deliver it into the hearts of my enemies.\"').
card_multiverse_id('auriok glaivemaster'/'DST', '48988').

card_in_set('auriok siege sled', 'DST').
card_original_type('auriok siege sled'/'DST', 'Artifact Creature').
card_original_text('auriok siege sled'/'DST', '{1}: Target artifact creature blocks Auriok Siege Sled this turn if able.\n{1}: Target artifact creature can\'t block Auriok Siege Sled this turn.').
card_first_print('auriok siege sled', 'DST').
card_image_name('auriok siege sled'/'DST', 'auriok siege sled').
card_uid('auriok siege sled'/'DST', 'DST:Auriok Siege Sled:auriok siege sled').
card_rarity('auriok siege sled'/'DST', 'Uncommon').
card_artist('auriok siege sled'/'DST', 'Jim Murray').
card_number('auriok siege sled'/'DST', '105').
card_flavor_text('auriok siege sled'/'DST', 'Everything in its path is either pushed aside or ground under.').
card_multiverse_id('auriok siege sled'/'DST', '43556').

card_in_set('barbed lightning', 'DST').
card_original_type('barbed lightning'/'DST', 'Instant').
card_original_text('barbed lightning'/'DST', 'Choose one Barbed Lightning deals 3 damage to target creature; or Barbed Lightning deals 3 damage to target player.\nEntwine {2} (Choose both if you pay the entwine cost.)').
card_first_print('barbed lightning', 'DST').
card_image_name('barbed lightning'/'DST', 'barbed lightning').
card_uid('barbed lightning'/'DST', 'DST:Barbed Lightning:barbed lightning').
card_rarity('barbed lightning'/'DST', 'Common').
card_artist('barbed lightning'/'DST', 'Hugh Jamieson').
card_number('barbed lightning'/'DST', '55').
card_flavor_text('barbed lightning'/'DST', 'Every creature on Mirrodin is a lightning rod.').
card_multiverse_id('barbed lightning'/'DST', '49012').

card_in_set('blinkmoth nexus', 'DST').
card_original_type('blinkmoth nexus'/'DST', 'Land').
card_original_text('blinkmoth nexus'/'DST', '{T}: Add {1} to your mana pool.\n{1}: Blinkmoth Nexus becomes a 1/1 Blinkmoth artifact creature with flying until end of turn. It\'s still a land.\n{1}, {T}: Target Blinkmoth gets +1/+1 until end of turn.').
card_first_print('blinkmoth nexus', 'DST').
card_image_name('blinkmoth nexus'/'DST', 'blinkmoth nexus').
card_uid('blinkmoth nexus'/'DST', 'DST:Blinkmoth Nexus:blinkmoth nexus').
card_rarity('blinkmoth nexus'/'DST', 'Rare').
card_artist('blinkmoth nexus'/'DST', 'Brian Snõddy').
card_number('blinkmoth nexus'/'DST', '163').
card_multiverse_id('blinkmoth nexus'/'DST', '39439').

card_in_set('burden of greed', 'DST').
card_original_type('burden of greed'/'DST', 'Instant').
card_original_text('burden of greed'/'DST', 'Target player loses 1 life for each tapped artifact he or she controls.').
card_first_print('burden of greed', 'DST').
card_image_name('burden of greed'/'DST', 'burden of greed').
card_uid('burden of greed'/'DST', 'DST:Burden of Greed:burden of greed').
card_rarity('burden of greed'/'DST', 'Common').
card_artist('burden of greed'/'DST', 'Vance Kovacs').
card_number('burden of greed'/'DST', '38').
card_flavor_text('burden of greed'/'DST', '\"The one who dies with the most toys is still dead.\"\n—Geth, keeper of the Vault').
card_multiverse_id('burden of greed'/'DST', '43557').

card_in_set('carry away', 'DST').
card_original_type('carry away'/'DST', 'Enchant Equipment').
card_original_text('carry away'/'DST', 'When Carry Away comes into play, unattach enchanted Equipment.\nYou control enchanted Equipment.').
card_first_print('carry away', 'DST').
card_image_name('carry away'/'DST', 'carry away').
card_uid('carry away'/'DST', 'DST:Carry Away:carry away').
card_rarity('carry away'/'DST', 'Uncommon').
card_artist('carry away'/'DST', 'Jim Nelson').
card_number('carry away'/'DST', '19').
card_flavor_text('carry away'/'DST', 'Stealing goblin weapons would be easier if the goblins were smart enough to let go.').
card_multiverse_id('carry away'/'DST', '48572').

card_in_set('chimeric egg', 'DST').
card_original_type('chimeric egg'/'DST', 'Artifact').
card_original_text('chimeric egg'/'DST', 'Whenever an opponent plays a nonartifact spell, put a charge counter on Chimeric Egg.\nRemove three charge counters from Chimeric Egg: Chimeric Egg becomes a 6/6 artifact creature with trample until end of turn.').
card_first_print('chimeric egg', 'DST').
card_image_name('chimeric egg'/'DST', 'chimeric egg').
card_uid('chimeric egg'/'DST', 'DST:Chimeric Egg:chimeric egg').
card_rarity('chimeric egg'/'DST', 'Uncommon').
card_artist('chimeric egg'/'DST', 'Michael Sutfin').
card_number('chimeric egg'/'DST', '106').
card_multiverse_id('chimeric egg'/'DST', '49097').

card_in_set('chittering rats', 'DST').
card_original_type('chittering rats'/'DST', 'Creature — Rat').
card_original_text('chittering rats'/'DST', 'When Chittering Rats comes into play, target opponent puts a card from his or her hand on top of his or her library.').
card_first_print('chittering rats', 'DST').
card_image_name('chittering rats'/'DST', 'chittering rats').
card_uid('chittering rats'/'DST', 'DST:Chittering Rats:chittering rats').
card_rarity('chittering rats'/'DST', 'Common').
card_artist('chittering rats'/'DST', 'Tom Wänerstrand').
card_number('chittering rats'/'DST', '39').
card_flavor_text('chittering rats'/'DST', 'Bottom feeders sometimes rise to the top.').
card_multiverse_id('chittering rats'/'DST', '46096').

card_in_set('chromescale drake', 'DST').
card_original_type('chromescale drake'/'DST', 'Creature — Drake').
card_original_text('chromescale drake'/'DST', 'Affinity for artifacts (This spell costs {1} less to play for each artifact you control.)\nFlying\nWhen Chromescale Drake comes into play, reveal the top three cards of your library. Put all artifact cards revealed this way into your hand and the rest into your graveyard.').
card_first_print('chromescale drake', 'DST').
card_image_name('chromescale drake'/'DST', 'chromescale drake').
card_uid('chromescale drake'/'DST', 'DST:Chromescale Drake:chromescale drake').
card_rarity('chromescale drake'/'DST', 'Rare').
card_artist('chromescale drake'/'DST', 'Ben Thompson').
card_number('chromescale drake'/'DST', '20').
card_multiverse_id('chromescale drake'/'DST', '48127').

card_in_set('coretapper', 'DST').
card_original_type('coretapper'/'DST', 'Artifact Creature — Myr').
card_original_text('coretapper'/'DST', '{T}: Put a charge counter on target artifact.\nSacrifice Coretapper: Put two charge counters on target artifact.').
card_first_print('coretapper', 'DST').
card_image_name('coretapper'/'DST', 'coretapper').
card_uid('coretapper'/'DST', 'DST:Coretapper:coretapper').
card_rarity('coretapper'/'DST', 'Uncommon').
card_artist('coretapper'/'DST', 'Dany Orizio').
card_number('coretapper'/'DST', '107').
card_flavor_text('coretapper'/'DST', 'It converts the faintest surges of power from Mirrodin\'s core into usable energy, providing endless power for Memnarch\'s creations on the surface.').
card_multiverse_id('coretapper'/'DST', '49016').

card_in_set('crazed goblin', 'DST').
card_original_type('crazed goblin'/'DST', 'Creature — Goblin Warrior').
card_original_text('crazed goblin'/'DST', 'Crazed Goblin attacks each turn if able.').
card_first_print('crazed goblin', 'DST').
card_image_name('crazed goblin'/'DST', 'crazed goblin').
card_uid('crazed goblin'/'DST', 'DST:Crazed Goblin:crazed goblin').
card_rarity('crazed goblin'/'DST', 'Common').
card_artist('crazed goblin'/'DST', 'Darrell Riche').
card_number('crazed goblin'/'DST', '56').
card_flavor_text('crazed goblin'/'DST', 'Because fighting is easier than figuring out what else to do.').
card_multiverse_id('crazed goblin'/'DST', '46078').

card_in_set('darksteel brute', 'DST').
card_original_type('darksteel brute'/'DST', 'Artifact').
card_original_text('darksteel brute'/'DST', 'Darksteel Brute is indestructible. (\"Destroy\" effects and lethal damage don\'t destroy it.)\n{3}: Darksteel Brute becomes a 2/2 artifact creature until end of turn.').
card_first_print('darksteel brute', 'DST').
card_image_name('darksteel brute'/'DST', 'darksteel brute').
card_uid('darksteel brute'/'DST', 'DST:Darksteel Brute:darksteel brute').
card_rarity('darksteel brute'/'DST', 'Uncommon').
card_artist('darksteel brute'/'DST', 'Nottsuo').
card_number('darksteel brute'/'DST', '108').
card_multiverse_id('darksteel brute'/'DST', '43563').

card_in_set('darksteel citadel', 'DST').
card_original_type('darksteel citadel'/'DST', 'Artifact Land').
card_original_text('darksteel citadel'/'DST', 'Darksteel Citadel is indestructible. (\"Destroy\" effects and lethal damage don\'t destroy it.)\n{T}: Add {1} to your mana pool.').
card_first_print('darksteel citadel', 'DST').
card_image_name('darksteel citadel'/'DST', 'darksteel citadel').
card_uid('darksteel citadel'/'DST', 'DST:Darksteel Citadel:darksteel citadel').
card_rarity('darksteel citadel'/'DST', 'Common').
card_artist('darksteel citadel'/'DST', 'John Avon').
card_number('darksteel citadel'/'DST', '164').
card_flavor_text('darksteel citadel'/'DST', 'Panopticon, forge of the Darksteel Eye, home of Mirrodin\'s keeper.').
card_multiverse_id('darksteel citadel'/'DST', '45415').

card_in_set('darksteel colossus', 'DST').
card_original_type('darksteel colossus'/'DST', 'Artifact Creature').
card_original_text('darksteel colossus'/'DST', 'Trample\nDarksteel Colossus is indestructible. (\"Destroy\" effects and lethal damage don\'t destroy it.)\nIf Darksteel Colossus would be put into a graveyard from anywhere, reveal Darksteel Colossus and shuffle it into its owner\'s library instead.').
card_first_print('darksteel colossus', 'DST').
card_image_name('darksteel colossus'/'DST', 'darksteel colossus').
card_uid('darksteel colossus'/'DST', 'DST:Darksteel Colossus:darksteel colossus').
card_rarity('darksteel colossus'/'DST', 'Rare').
card_artist('darksteel colossus'/'DST', 'Carl Critchlow').
card_number('darksteel colossus'/'DST', '109').
card_multiverse_id('darksteel colossus'/'DST', '48158').

card_in_set('darksteel forge', 'DST').
card_original_type('darksteel forge'/'DST', 'Artifact').
card_original_text('darksteel forge'/'DST', 'Artifacts you control are indestructible. (\"Destroy\" effects and lethal damage don\'t destroy them.)').
card_first_print('darksteel forge', 'DST').
card_image_name('darksteel forge'/'DST', 'darksteel forge').
card_uid('darksteel forge'/'DST', 'DST:Darksteel Forge:darksteel forge').
card_rarity('darksteel forge'/'DST', 'Rare').
card_artist('darksteel forge'/'DST', 'Martina Pilcerova').
card_number('darksteel forge'/'DST', '110').
card_flavor_text('darksteel forge'/'DST', '\"Did it have this shape upon Mirrodin\'s creation, or did some inconceivable force shape the unshapable?\"\n—Pontifex, elder researcher').
card_multiverse_id('darksteel forge'/'DST', '48072').

card_in_set('darksteel gargoyle', 'DST').
card_original_type('darksteel gargoyle'/'DST', 'Artifact Creature — Gargoyle').
card_original_text('darksteel gargoyle'/'DST', 'Flying\nDarksteel Gargoyle is indestructible. (\"Destroy\" effects and lethal damage don\'t destroy it.)').
card_first_print('darksteel gargoyle', 'DST').
card_image_name('darksteel gargoyle'/'DST', 'darksteel gargoyle').
card_uid('darksteel gargoyle'/'DST', 'DST:Darksteel Gargoyle:darksteel gargoyle').
card_rarity('darksteel gargoyle'/'DST', 'Uncommon').
card_artist('darksteel gargoyle'/'DST', 'Ron Spencer').
card_number('darksteel gargoyle'/'DST', '111').
card_flavor_text('darksteel gargoyle'/'DST', 'The ultimate treasure is one that guards itself.').
card_multiverse_id('darksteel gargoyle'/'DST', '46136').

card_in_set('darksteel ingot', 'DST').
card_original_type('darksteel ingot'/'DST', 'Artifact').
card_original_text('darksteel ingot'/'DST', 'Darksteel Ingot is indestructible. (\"Destroy\" effects and lethal damage don\'t destroy it.)\n{T}: Add one mana of any color to your mana pool.').
card_image_name('darksteel ingot'/'DST', 'darksteel ingot').
card_uid('darksteel ingot'/'DST', 'DST:Darksteel Ingot:darksteel ingot').
card_rarity('darksteel ingot'/'DST', 'Common').
card_artist('darksteel ingot'/'DST', 'Martina Pilcerova').
card_number('darksteel ingot'/'DST', '112').
card_multiverse_id('darksteel ingot'/'DST', '48048').

card_in_set('darksteel pendant', 'DST').
card_original_type('darksteel pendant'/'DST', 'Artifact').
card_original_text('darksteel pendant'/'DST', 'Darksteel Pendant is indestructible. (\"Destroy\" effects and lethal damage don\'t destroy it.)\n{1}, {T}: Look at the top card of your library. You may put that card on the bottom of your library.').
card_first_print('darksteel pendant', 'DST').
card_image_name('darksteel pendant'/'DST', 'darksteel pendant').
card_uid('darksteel pendant'/'DST', 'DST:Darksteel Pendant:darksteel pendant').
card_rarity('darksteel pendant'/'DST', 'Common').
card_artist('darksteel pendant'/'DST', 'Terese Nielsen').
card_number('darksteel pendant'/'DST', '113').
card_multiverse_id('darksteel pendant'/'DST', '39485').

card_in_set('darksteel reactor', 'DST').
card_original_type('darksteel reactor'/'DST', 'Artifact').
card_original_text('darksteel reactor'/'DST', 'Darksteel Reactor is indestructible. (\"Destroy\" effects and lethal damage don\'t destroy it.)\nAt the beginning of your upkeep, you may put a charge counter on Darksteel Reactor.\nWhen Darksteel Reactor has twenty or more charge counters on it, you win the game.').
card_first_print('darksteel reactor', 'DST').
card_image_name('darksteel reactor'/'DST', 'darksteel reactor').
card_uid('darksteel reactor'/'DST', 'DST:Darksteel Reactor:darksteel reactor').
card_rarity('darksteel reactor'/'DST', 'Rare').
card_artist('darksteel reactor'/'DST', 'Kev Walker').
card_number('darksteel reactor'/'DST', '114').
card_multiverse_id('darksteel reactor'/'DST', '43548').

card_in_set('death cloud', 'DST').
card_original_type('death cloud'/'DST', 'Sorcery').
card_original_text('death cloud'/'DST', 'Each player loses X life, then discards X cards from his or her hand, then sacrifices X creatures, then sacrifices X lands.').
card_first_print('death cloud', 'DST').
card_image_name('death cloud'/'DST', 'death cloud').
card_uid('death cloud'/'DST', 'DST:Death Cloud:death cloud').
card_rarity('death cloud'/'DST', 'Rare').
card_artist('death cloud'/'DST', 'Stephen Tappin').
card_number('death cloud'/'DST', '40').
card_flavor_text('death cloud'/'DST', 'The swarm\'s million wings stir the foulest of breezes.').
card_multiverse_id('death cloud'/'DST', '50647').

card_in_set('death-mask duplicant', 'DST').
card_original_type('death-mask duplicant'/'DST', 'Artifact Creature — Shapeshifter').
card_original_text('death-mask duplicant'/'DST', 'Imprint {1}: Remove target creature card in your graveyard from the game. (The removed card is imprinted on this artifact.)\nAs long as an imprinted creature card has flying, Death-Mask Duplicant has flying. The same is true for fear, first strike, double strike, haste, landwalk, protection, and trample.').
card_first_print('death-mask duplicant', 'DST').
card_image_name('death-mask duplicant'/'DST', 'death-mask duplicant').
card_uid('death-mask duplicant'/'DST', 'DST:Death-Mask Duplicant:death-mask duplicant').
card_rarity('death-mask duplicant'/'DST', 'Uncommon').
card_artist('death-mask duplicant'/'DST', 'Thomas M. Baxa').
card_number('death-mask duplicant'/'DST', '115').
card_multiverse_id('death-mask duplicant'/'DST', '46023').

card_in_set('demon\'s horn', 'DST').
card_original_type('demon\'s horn'/'DST', 'Artifact').
card_original_text('demon\'s horn'/'DST', 'Whenever a player plays a black spell, you may gain 1 life.').
card_first_print('demon\'s horn', 'DST').
card_image_name('demon\'s horn'/'DST', 'demon\'s horn').
card_uid('demon\'s horn'/'DST', 'DST:Demon\'s Horn:demon\'s horn').
card_rarity('demon\'s horn'/'DST', 'Uncommon').
card_artist('demon\'s horn'/'DST', 'Alan Pollack').
card_number('demon\'s horn'/'DST', '116').
card_flavor_text('demon\'s horn'/'DST', 'Its curve mimics the twists of life and death.').
card_multiverse_id('demon\'s horn'/'DST', '72682').

card_in_set('dismantle', 'DST').
card_original_type('dismantle'/'DST', 'Sorcery').
card_original_text('dismantle'/'DST', 'Destroy target artifact. If that artifact had counters on it, put that many +1/+1 counters or charge counters on an artifact you control.').
card_first_print('dismantle', 'DST').
card_image_name('dismantle'/'DST', 'dismantle').
card_uid('dismantle'/'DST', 'DST:Dismantle:dismantle').
card_rarity('dismantle'/'DST', 'Uncommon').
card_artist('dismantle'/'DST', 'Brian Snõddy').
card_number('dismantle'/'DST', '57').
card_flavor_text('dismantle'/'DST', '\"If only the living were so easily salvaged.\"\n—Memnarch').
card_multiverse_id('dismantle'/'DST', '43540').

card_in_set('dragon\'s claw', 'DST').
card_original_type('dragon\'s claw'/'DST', 'Artifact').
card_original_text('dragon\'s claw'/'DST', 'Whenever a player plays a red spell, you may gain 1 life.').
card_first_print('dragon\'s claw', 'DST').
card_image_name('dragon\'s claw'/'DST', 'dragon\'s claw').
card_uid('dragon\'s claw'/'DST', 'DST:Dragon\'s Claw:dragon\'s claw').
card_rarity('dragon\'s claw'/'DST', 'Uncommon').
card_artist('dragon\'s claw'/'DST', 'Alan Pollack').
card_number('dragon\'s claw'/'DST', '117').
card_flavor_text('dragon\'s claw'/'DST', 'Though no longer attached to the hand, it still holds its adversary in its grasp.').
card_multiverse_id('dragon\'s claw'/'DST', '72683').

card_in_set('drill-skimmer', 'DST').
card_original_type('drill-skimmer'/'DST', 'Artifact Creature').
card_original_text('drill-skimmer'/'DST', 'Flying\nDrill-Skimmer can\'t be the target of spells or abilities as long as you control another artifact creature.').
card_first_print('drill-skimmer', 'DST').
card_image_name('drill-skimmer'/'DST', 'drill-skimmer').
card_uid('drill-skimmer'/'DST', 'DST:Drill-Skimmer:drill-skimmer').
card_rarity('drill-skimmer'/'DST', 'Common').
card_artist('drill-skimmer'/'DST', 'Mark Zug').
card_number('drill-skimmer'/'DST', '118').
card_flavor_text('drill-skimmer'/'DST', 'In close proximity, their magnetic energies merge and form a field of protection.').
card_multiverse_id('drill-skimmer'/'DST', '51056').

card_in_set('drooling ogre', 'DST').
card_original_type('drooling ogre'/'DST', 'Creature — Ogre').
card_original_text('drooling ogre'/'DST', 'Whenever a player plays an artifact spell, that player gains control of Drooling Ogre. (This effect doesn\'t end at end of turn.)').
card_first_print('drooling ogre', 'DST').
card_image_name('drooling ogre'/'DST', 'drooling ogre').
card_uid('drooling ogre'/'DST', 'DST:Drooling Ogre:drooling ogre').
card_rarity('drooling ogre'/'DST', 'Common').
card_artist('drooling ogre'/'DST', 'Brian Snõddy').
card_number('drooling ogre'/'DST', '58').
card_flavor_text('drooling ogre'/'DST', '\"Is it the stomping kind or the munching kind?\"\n—Slobad, goblin tinkerer').
card_multiverse_id('drooling ogre'/'DST', '49089').

card_in_set('dross golem', 'DST').
card_original_type('dross golem'/'DST', 'Artifact Creature — Golem').
card_original_text('dross golem'/'DST', 'Affinity for Swamps (This spell costs {1} less to play for each Swamp you control.)\nFear').
card_first_print('dross golem', 'DST').
card_image_name('dross golem'/'DST', 'dross golem').
card_uid('dross golem'/'DST', 'DST:Dross Golem:dross golem').
card_rarity('dross golem'/'DST', 'Common').
card_artist('dross golem'/'DST', 'Adam Rex').
card_number('dross golem'/'DST', '119').
card_flavor_text('dross golem'/'DST', 'The longer it skulks through the corrosive gases of Mephidross, the more it adopts their thirst for death.').
card_multiverse_id('dross golem'/'DST', '46157').

card_in_set('eater of days', 'DST').
card_original_type('eater of days'/'DST', 'Artifact Creature — Leviathan').
card_original_text('eater of days'/'DST', 'Flying, trample\nWhen Eater of Days comes into play, you skip your next two turns.').
card_first_print('eater of days', 'DST').
card_image_name('eater of days'/'DST', 'eater of days').
card_uid('eater of days'/'DST', 'DST:Eater of Days:eater of days').
card_rarity('eater of days'/'DST', 'Rare').
card_artist('eater of days'/'DST', 'Mark Tedin').
card_number('eater of days'/'DST', '120').
card_flavor_text('eater of days'/'DST', 'When Mirrodin\'s varied civilizations developed ways to fight the levelers, Memnarch upped the stakes.').
card_multiverse_id('eater of days'/'DST', '46165').

card_in_set('echoing calm', 'DST').
card_original_type('echoing calm'/'DST', 'Instant').
card_original_text('echoing calm'/'DST', 'Destroy target enchantment and all other enchantments with the same name as that enchantment.').
card_first_print('echoing calm', 'DST').
card_image_name('echoing calm'/'DST', 'echoing calm').
card_uid('echoing calm'/'DST', 'DST:Echoing Calm:echoing calm').
card_rarity('echoing calm'/'DST', 'Common').
card_artist('echoing calm'/'DST', 'Greg Staples').
card_number('echoing calm'/'DST', '2').
card_flavor_text('echoing calm'/'DST', 'A single light unleashes a hail of cleansing.').
card_multiverse_id('echoing calm'/'DST', '43592').

card_in_set('echoing courage', 'DST').
card_original_type('echoing courage'/'DST', 'Instant').
card_original_text('echoing courage'/'DST', 'Target creature and all other creatures with the same name as that creature get +2/+2 until end of turn.').
card_first_print('echoing courage', 'DST').
card_image_name('echoing courage'/'DST', 'echoing courage').
card_uid('echoing courage'/'DST', 'DST:Echoing Courage:echoing courage').
card_rarity('echoing courage'/'DST', 'Common').
card_artist('echoing courage'/'DST', 'Greg Staples').
card_number('echoing courage'/'DST', '74').
card_flavor_text('echoing courage'/'DST', 'A single seed unleashes a flurry of growth.').
card_multiverse_id('echoing courage'/'DST', '46169').

card_in_set('echoing decay', 'DST').
card_original_type('echoing decay'/'DST', 'Instant').
card_original_text('echoing decay'/'DST', 'Target creature and all other creatures with the same name as that creature get -2/-2 until end of turn.').
card_first_print('echoing decay', 'DST').
card_image_name('echoing decay'/'DST', 'echoing decay').
card_uid('echoing decay'/'DST', 'DST:Echoing Decay:echoing decay').
card_rarity('echoing decay'/'DST', 'Common').
card_artist('echoing decay'/'DST', 'Greg Staples').
card_number('echoing decay'/'DST', '41').
card_flavor_text('echoing decay'/'DST', 'A single fear unleashes a torrent of nightmares.').
card_multiverse_id('echoing decay'/'DST', '46176').

card_in_set('echoing ruin', 'DST').
card_original_type('echoing ruin'/'DST', 'Sorcery').
card_original_text('echoing ruin'/'DST', 'Destroy target artifact and all other artifacts with the same name as that artifact.').
card_first_print('echoing ruin', 'DST').
card_image_name('echoing ruin'/'DST', 'echoing ruin').
card_uid('echoing ruin'/'DST', 'DST:Echoing Ruin:echoing ruin').
card_rarity('echoing ruin'/'DST', 'Common').
card_artist('echoing ruin'/'DST', 'Greg Staples').
card_number('echoing ruin'/'DST', '59').
card_flavor_text('echoing ruin'/'DST', 'A single misstep unleashes an avalanche of ruin.').
card_multiverse_id('echoing ruin'/'DST', '46155').

card_in_set('echoing truth', 'DST').
card_original_type('echoing truth'/'DST', 'Instant').
card_original_text('echoing truth'/'DST', 'Return target nonland permanent and all other permanents with the same name as that permanent to their owners\' hands.').
card_first_print('echoing truth', 'DST').
card_image_name('echoing truth'/'DST', 'echoing truth').
card_uid('echoing truth'/'DST', 'DST:Echoing Truth:echoing truth').
card_rarity('echoing truth'/'DST', 'Common').
card_artist('echoing truth'/'DST', 'Greg Staples').
card_number('echoing truth'/'DST', '21').
card_flavor_text('echoing truth'/'DST', 'A single lie unleashes a tide of disbelief.').
card_multiverse_id('echoing truth'/'DST', '46162').

card_in_set('emissary of despair', 'DST').
card_original_type('emissary of despair'/'DST', 'Creature — Spirit').
card_original_text('emissary of despair'/'DST', 'Flying\nWhenever Emissary of Despair deals combat damage to a player, that player loses 1 life for each artifact he or she controls.').
card_first_print('emissary of despair', 'DST').
card_image_name('emissary of despair'/'DST', 'emissary of despair').
card_uid('emissary of despair'/'DST', 'DST:Emissary of Despair:emissary of despair').
card_rarity('emissary of despair'/'DST', 'Uncommon').
card_artist('emissary of despair'/'DST', 'rk post').
card_number('emissary of despair'/'DST', '42').
card_multiverse_id('emissary of despair'/'DST', '45469').

card_in_set('emissary of hope', 'DST').
card_original_type('emissary of hope'/'DST', 'Creature — Spirit').
card_original_text('emissary of hope'/'DST', 'Flying\nWhenever Emissary of Hope deals combat damage to a player, you gain 1 life for each artifact that player controls.').
card_first_print('emissary of hope', 'DST').
card_image_name('emissary of hope'/'DST', 'emissary of hope').
card_uid('emissary of hope'/'DST', 'DST:Emissary of Hope:emissary of hope').
card_rarity('emissary of hope'/'DST', 'Uncommon').
card_artist('emissary of hope'/'DST', 'rk post').
card_number('emissary of hope'/'DST', '3').
card_multiverse_id('emissary of hope'/'DST', '48153').

card_in_set('essence drain', 'DST').
card_original_type('essence drain'/'DST', 'Sorcery').
card_original_text('essence drain'/'DST', 'Essence Drain deals 3 damage to target creature or player and you gain 3 life.').
card_first_print('essence drain', 'DST').
card_image_name('essence drain'/'DST', 'essence drain').
card_uid('essence drain'/'DST', 'DST:Essence Drain:essence drain').
card_rarity('essence drain'/'DST', 'Common').
card_artist('essence drain'/'DST', 'Tony Szczudlo').
card_number('essence drain'/'DST', '43').
card_flavor_text('essence drain'/'DST', 'Mephidross claims all life within it. What cannot be twisted is absorbed.').
card_multiverse_id('essence drain'/'DST', '50536').

card_in_set('fangren firstborn', 'DST').
card_original_type('fangren firstborn'/'DST', 'Creature — Beast').
card_original_text('fangren firstborn'/'DST', 'Whenever Fangren Firstborn attacks, put a +1/+1 counter on each attacking creature.').
card_first_print('fangren firstborn', 'DST').
card_image_name('fangren firstborn'/'DST', 'fangren firstborn').
card_uid('fangren firstborn'/'DST', 'DST:Fangren Firstborn:fangren firstborn').
card_rarity('fangren firstborn'/'DST', 'Rare').
card_artist('fangren firstborn'/'DST', 'Tim Hildebrandt').
card_number('fangren firstborn'/'DST', '75').
card_flavor_text('fangren firstborn'/'DST', 'In the Tangle, bad is often followed by worse.').
card_multiverse_id('fangren firstborn'/'DST', '46058').

card_in_set('fireball', 'DST').
card_original_type('fireball'/'DST', 'Sorcery').
card_original_text('fireball'/'DST', 'Fireball deals X damage divided evenly, rounded down, among any number of target creatures and/or players.\nAs an additional cost to play Fireball, pay {1} for each target beyond the first.').
card_image_name('fireball'/'DST', 'fireball').
card_uid('fireball'/'DST', 'DST:Fireball:fireball').
card_rarity('fireball'/'DST', 'Uncommon').
card_artist('fireball'/'DST', 'Dave Dorman').
card_number('fireball'/'DST', '60').
card_flavor_text('fireball'/'DST', 'The spell fell upon the crowd like a dragon, ancient and full of death.').
card_multiverse_id('fireball'/'DST', '46768').

card_in_set('flamebreak', 'DST').
card_original_type('flamebreak'/'DST', 'Sorcery').
card_original_text('flamebreak'/'DST', 'Flamebreak deals 3 damage to each creature without flying and each player. Creatures dealt damage this way can\'t be regenerated this turn.').
card_first_print('flamebreak', 'DST').
card_image_name('flamebreak'/'DST', 'flamebreak').
card_uid('flamebreak'/'DST', 'DST:Flamebreak:flamebreak').
card_rarity('flamebreak'/'DST', 'Rare').
card_artist('flamebreak'/'DST', 'Trevor Hairsine').
card_number('flamebreak'/'DST', '61').
card_flavor_text('flamebreak'/'DST', '\"Now I\'m thirsty.\"\n—Korva, Vulshok battlemaster').
card_multiverse_id('flamebreak'/'DST', '50539').

card_in_set('furnace dragon', 'DST').
card_original_type('furnace dragon'/'DST', 'Creature — Dragon').
card_original_text('furnace dragon'/'DST', 'Affinity for artifacts (This spell costs {1} less to play for each artifact you control.)\nFlying\nWhen Furnace Dragon comes into play, if you played it from your hand, remove all artifacts from the game.').
card_first_print('furnace dragon', 'DST').
card_image_name('furnace dragon'/'DST', 'furnace dragon').
card_uid('furnace dragon'/'DST', 'DST:Furnace Dragon:furnace dragon').
card_rarity('furnace dragon'/'DST', 'Rare').
card_artist('furnace dragon'/'DST', 'Matthew D. Wilson').
card_number('furnace dragon'/'DST', '62').
card_multiverse_id('furnace dragon'/'DST', '46060').

card_in_set('gemini engine', 'DST').
card_original_type('gemini engine'/'DST', 'Artifact Creature').
card_original_text('gemini engine'/'DST', 'Whenever Gemini Engine attacks, put an attacking Twin artifact creature token into play. Its power is equal to Gemini Engine\'s power and its toughness is equal to Gemini Engine\'s toughness. Sacrifice the token at end of combat.').
card_first_print('gemini engine', 'DST').
card_image_name('gemini engine'/'DST', 'gemini engine').
card_uid('gemini engine'/'DST', 'DST:Gemini Engine:gemini engine').
card_rarity('gemini engine'/'DST', 'Rare').
card_artist('gemini engine'/'DST', 'Nottsuo').
card_number('gemini engine'/'DST', '121').
card_multiverse_id('gemini engine'/'DST', '46168').

card_in_set('genesis chamber', 'DST').
card_original_type('genesis chamber'/'DST', 'Artifact').
card_original_text('genesis chamber'/'DST', 'Whenever a nontoken creature comes into play, if Genesis Chamber is untapped, that creature\'s controller puts a 1/1 Myr artifact creature token into play.').
card_first_print('genesis chamber', 'DST').
card_image_name('genesis chamber'/'DST', 'genesis chamber').
card_uid('genesis chamber'/'DST', 'DST:Genesis Chamber:genesis chamber').
card_rarity('genesis chamber'/'DST', 'Uncommon').
card_artist('genesis chamber'/'DST', 'Mark Tedin').
card_number('genesis chamber'/'DST', '122').
card_flavor_text('genesis chamber'/'DST', '\"As the experimental population grows, so do the number of myr. It\'s a perfect equilibrium.\"\n—Memnarch').
card_multiverse_id('genesis chamber'/'DST', '46160').

card_in_set('geth\'s grimoire', 'DST').
card_original_type('geth\'s grimoire'/'DST', 'Artifact').
card_original_text('geth\'s grimoire'/'DST', 'Whenever an opponent discards a card from his or her hand, you may draw a card.').
card_first_print('geth\'s grimoire', 'DST').
card_image_name('geth\'s grimoire'/'DST', 'geth\'s grimoire').
card_uid('geth\'s grimoire'/'DST', 'DST:Geth\'s Grimoire:geth\'s grimoire').
card_rarity('geth\'s grimoire'/'DST', 'Uncommon').
card_artist('geth\'s grimoire'/'DST', 'Heather Hudson').
card_number('geth\'s grimoire'/'DST', '123').
card_flavor_text('geth\'s grimoire'/'DST', 'The book shrieks horribly until its cover is opened, granting the spirit inside a moment\'s rest from the painful stirrings of the book\'s contents.').
card_multiverse_id('geth\'s grimoire'/'DST', '51070').

card_in_set('goblin archaeologist', 'DST').
card_original_type('goblin archaeologist'/'DST', 'Creature — Goblin Artificer').
card_original_text('goblin archaeologist'/'DST', '{R}, {T}: Flip a coin. If you win the flip, destroy target artifact and untap Goblin Archaeologist. If you lose the flip, sacrifice Goblin Archaeologist.').
card_first_print('goblin archaeologist', 'DST').
card_image_name('goblin archaeologist'/'DST', 'goblin archaeologist').
card_uid('goblin archaeologist'/'DST', 'DST:Goblin Archaeologist:goblin archaeologist').
card_rarity('goblin archaeologist'/'DST', 'Uncommon').
card_artist('goblin archaeologist'/'DST', 'Jim Nelson').
card_number('goblin archaeologist'/'DST', '63').
card_flavor_text('goblin archaeologist'/'DST', 'Always easily impressed, Durg was about to be blown away.').
card_multiverse_id('goblin archaeologist'/'DST', '46735').

card_in_set('greater harvester', 'DST').
card_original_type('greater harvester'/'DST', 'Creature — Horror').
card_original_text('greater harvester'/'DST', 'At the beginning of your upkeep, sacrifice a permanent.\nWhenever Greater Harvester deals combat damage to a player, that player sacrifices two permanents.').
card_first_print('greater harvester', 'DST').
card_image_name('greater harvester'/'DST', 'greater harvester').
card_uid('greater harvester'/'DST', 'DST:Greater Harvester:greater harvester').
card_rarity('greater harvester'/'DST', 'Rare').
card_artist('greater harvester'/'DST', 'Daren Bader').
card_number('greater harvester'/'DST', '44').
card_multiverse_id('greater harvester'/'DST', '49837').

card_in_set('grimclaw bats', 'DST').
card_original_type('grimclaw bats'/'DST', 'Creature — Bat').
card_original_text('grimclaw bats'/'DST', 'Flying\n{B}, Pay 1 life: Grimclaw Bats gets +1/+1 until end of turn.').
card_first_print('grimclaw bats', 'DST').
card_image_name('grimclaw bats'/'DST', 'grimclaw bats').
card_uid('grimclaw bats'/'DST', 'DST:Grimclaw Bats:grimclaw bats').
card_rarity('grimclaw bats'/'DST', 'Common').
card_artist('grimclaw bats'/'DST', 'Tom Wänerstrand').
card_number('grimclaw bats'/'DST', '45').
card_flavor_text('grimclaw bats'/'DST', 'Even on a world of metal, there are those who thirst for blood.').
card_multiverse_id('grimclaw bats'/'DST', '46100').

card_in_set('hallow', 'DST').
card_original_type('hallow'/'DST', 'Instant').
card_original_text('hallow'/'DST', 'Prevent all damage target spell would deal this turn. You gain life equal to the damage prevented this way.').
card_first_print('hallow', 'DST').
card_image_name('hallow'/'DST', 'hallow').
card_uid('hallow'/'DST', 'DST:Hallow:hallow').
card_rarity('hallow'/'DST', 'Common').
card_artist('hallow'/'DST', 'Alex Horley-Orlandelli').
card_number('hallow'/'DST', '4').
card_flavor_text('hallow'/'DST', '\"Bend with the storm, then reply in kind.\"').
card_multiverse_id('hallow'/'DST', '29513').

card_in_set('heartseeker', 'DST').
card_original_type('heartseeker'/'DST', 'Artifact — Equipment').
card_original_text('heartseeker'/'DST', 'Equipped creature gets +2/+1 and has \"{T}, Unattach Heartseeker: Destroy target creature.\"\nEquip {5} ({5}: Attach to target creature you control. Equip only as a sorcery. This card comes into play unattached and stays in play if the creature leaves play.)').
card_first_print('heartseeker', 'DST').
card_image_name('heartseeker'/'DST', 'heartseeker').
card_uid('heartseeker'/'DST', 'DST:Heartseeker:heartseeker').
card_rarity('heartseeker'/'DST', 'Rare').
card_artist('heartseeker'/'DST', 'Michael Sutfin').
card_number('heartseeker'/'DST', '124').
card_multiverse_id('heartseeker'/'DST', '51074').

card_in_set('hoverguard observer', 'DST').
card_original_type('hoverguard observer'/'DST', 'Creature — Drone').
card_original_text('hoverguard observer'/'DST', 'Flying\nHoverguard Observer can block only creatures with flying.').
card_first_print('hoverguard observer', 'DST').
card_image_name('hoverguard observer'/'DST', 'hoverguard observer').
card_uid('hoverguard observer'/'DST', 'DST:Hoverguard Observer:hoverguard observer').
card_rarity('hoverguard observer'/'DST', 'Uncommon').
card_artist('hoverguard observer'/'DST', 'Kev Walker').
card_number('hoverguard observer'/'DST', '22').
card_flavor_text('hoverguard observer'/'DST', 'It could be made to run silently, but its low hum reminds the Neurok that they are always watched.').
card_multiverse_id('hoverguard observer'/'DST', '49020').

card_in_set('hunger of the nim', 'DST').
card_original_type('hunger of the nim'/'DST', 'Sorcery').
card_original_text('hunger of the nim'/'DST', 'Target creature gets +1/+0 until end of turn for each artifact you control.').
card_first_print('hunger of the nim', 'DST').
card_image_name('hunger of the nim'/'DST', 'hunger of the nim').
card_uid('hunger of the nim'/'DST', 'DST:Hunger of the Nim:hunger of the nim').
card_rarity('hunger of the nim'/'DST', 'Common').
card_artist('hunger of the nim'/'DST', 'Puddnhead').
card_number('hunger of the nim'/'DST', '46').
card_flavor_text('hunger of the nim'/'DST', 'Their pain is so great, they can\'t help but share it with others.').
card_multiverse_id('hunger of the nim'/'DST', '46088').

card_in_set('infested roothold', 'DST').
card_original_type('infested roothold'/'DST', 'Creature — Wall').
card_original_text('infested roothold'/'DST', '(Walls can\'t attack.)\nProtection from artifacts\nWhenever an opponent plays an artifact spell, you may put a 1/1 green Insect creature token into play.').
card_first_print('infested roothold', 'DST').
card_image_name('infested roothold'/'DST', 'infested roothold').
card_uid('infested roothold'/'DST', 'DST:Infested Roothold:infested roothold').
card_rarity('infested roothold'/'DST', 'Uncommon').
card_artist('infested roothold'/'DST', 'Terese Nielsen').
card_number('infested roothold'/'DST', '76').
card_multiverse_id('infested roothold'/'DST', '48598').

card_in_set('inflame', 'DST').
card_original_type('inflame'/'DST', 'Instant').
card_original_text('inflame'/'DST', 'Inflame deals 2 damage to each creature dealt damage this turn.').
card_image_name('inflame'/'DST', 'inflame').
card_uid('inflame'/'DST', 'DST:Inflame:inflame').
card_rarity('inflame'/'DST', 'Common').
card_artist('inflame'/'DST', 'Hugh Jamieson').
card_number('inflame'/'DST', '64').
card_flavor_text('inflame'/'DST', 'A Vulshok weapon strikes with the fire of the forge that made it.').
card_multiverse_id('inflame'/'DST', '48120').

card_in_set('juggernaut', 'DST').
card_original_type('juggernaut'/'DST', 'Artifact Creature').
card_original_text('juggernaut'/'DST', 'Juggernaut attacks each turn if able.\nJuggernaut can\'t be blocked by Walls.').
card_image_name('juggernaut'/'DST', 'juggernaut').
card_uid('juggernaut'/'DST', 'DST:Juggernaut:juggernaut').
card_rarity('juggernaut'/'DST', 'Uncommon').
card_artist('juggernaut'/'DST', 'Arnie Swekel').
card_number('juggernaut'/'DST', '125').
card_flavor_text('juggernaut'/'DST', 'The goblins built it far larger than the cave opening, but to their glee, it smashed into the mountainside, ripping itself a new exit.').
card_multiverse_id('juggernaut'/'DST', '43579').

card_in_set('karstoderm', 'DST').
card_original_type('karstoderm'/'DST', 'Creature — Beast').
card_original_text('karstoderm'/'DST', 'Karstoderm comes into play with five +1/+1 counters on it.\nWhenever an artifact comes into play, remove a +1/+1 counter from Karstoderm.').
card_first_print('karstoderm', 'DST').
card_image_name('karstoderm'/'DST', 'karstoderm').
card_uid('karstoderm'/'DST', 'DST:Karstoderm:karstoderm').
card_rarity('karstoderm'/'DST', 'Uncommon').
card_artist('karstoderm'/'DST', 'Tony Szczudlo').
card_number('karstoderm'/'DST', '77').
card_multiverse_id('karstoderm'/'DST', '39478').

card_in_set('kraken\'s eye', 'DST').
card_original_type('kraken\'s eye'/'DST', 'Artifact').
card_original_text('kraken\'s eye'/'DST', 'Whenever a player plays a blue spell, you may gain 1 life.').
card_first_print('kraken\'s eye', 'DST').
card_image_name('kraken\'s eye'/'DST', 'kraken\'s eye').
card_uid('kraken\'s eye'/'DST', 'DST:Kraken\'s Eye:kraken\'s eye').
card_rarity('kraken\'s eye'/'DST', 'Uncommon').
card_artist('kraken\'s eye'/'DST', 'Alan Pollack').
card_number('kraken\'s eye'/'DST', '126').
card_flavor_text('kraken\'s eye'/'DST', 'Bright as a mirror, dark as the sea.').
card_multiverse_id('kraken\'s eye'/'DST', '49049').

card_in_set('krark-clan stoker', 'DST').
card_original_type('krark-clan stoker'/'DST', 'Creature — Goblin Shaman').
card_original_text('krark-clan stoker'/'DST', '{T}, Sacrifice an artifact: Add {R}{R} to your mana pool.').
card_first_print('krark-clan stoker', 'DST').
card_image_name('krark-clan stoker'/'DST', 'krark-clan stoker').
card_uid('krark-clan stoker'/'DST', 'DST:Krark-Clan Stoker:krark-clan stoker').
card_rarity('krark-clan stoker'/'DST', 'Common').
card_artist('krark-clan stoker'/'DST', 'Pete Venters').
card_number('krark-clan stoker'/'DST', '65').
card_flavor_text('krark-clan stoker'/'DST', 'Fallen enemies are fed to the scavengers. Fallen friends are thrown into the Great Furnace, their souls returned to the Great Mother.').
card_multiverse_id('krark-clan stoker'/'DST', '43491').

card_in_set('last word', 'DST').
card_original_type('last word'/'DST', 'Instant').
card_original_text('last word'/'DST', 'Last Word can\'t be countered by spells or abilities.\nCounter target spell.').
card_first_print('last word', 'DST').
card_image_name('last word'/'DST', 'last word').
card_uid('last word'/'DST', 'DST:Last Word:last word').
card_rarity('last word'/'DST', 'Rare').
card_artist('last word'/'DST', 'Scott M. Fischer').
card_number('last word'/'DST', '23').
card_flavor_text('last word'/'DST', '\"Someday, someone will best me. But it won\'t be today, and it won\'t be you.\"').
card_multiverse_id('last word'/'DST', '50929').

card_in_set('leonin battlemage', 'DST').
card_original_type('leonin battlemage'/'DST', 'Creature — Cat Wizard').
card_original_text('leonin battlemage'/'DST', '{T}: Target creature gets +1/+1 until end of turn. \nWhenever you play a spell, you may untap Leonin Battlemage.').
card_first_print('leonin battlemage', 'DST').
card_image_name('leonin battlemage'/'DST', 'leonin battlemage').
card_uid('leonin battlemage'/'DST', 'DST:Leonin Battlemage:leonin battlemage').
card_rarity('leonin battlemage'/'DST', 'Uncommon').
card_artist('leonin battlemage'/'DST', 'Stephen Tappin').
card_number('leonin battlemage'/'DST', '5').
card_flavor_text('leonin battlemage'/'DST', 'The strongest of the pride are measured not by the steel in their hands but by the steel in their souls.').
card_multiverse_id('leonin battlemage'/'DST', '44307').

card_in_set('leonin bola', 'DST').
card_original_type('leonin bola'/'DST', 'Artifact — Equipment').
card_original_text('leonin bola'/'DST', 'Equipped creature has \"{T}, Unattach Leonin Bola: Tap target creature.\"\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery. This card comes into play unattached and stays in play if the creature leaves play.)').
card_first_print('leonin bola', 'DST').
card_image_name('leonin bola'/'DST', 'leonin bola').
card_uid('leonin bola'/'DST', 'DST:Leonin Bola:leonin bola').
card_rarity('leonin bola'/'DST', 'Common').
card_artist('leonin bola'/'DST', 'Christopher Moeller').
card_number('leonin bola'/'DST', '127').
card_multiverse_id('leonin bola'/'DST', '46142').

card_in_set('leonin shikari', 'DST').
card_original_type('leonin shikari'/'DST', 'Creature — Cat Soldier').
card_original_text('leonin shikari'/'DST', 'You may play equip abilities any time you could play an instant.').
card_first_print('leonin shikari', 'DST').
card_image_name('leonin shikari'/'DST', 'leonin shikari').
card_uid('leonin shikari'/'DST', 'DST:Leonin Shikari:leonin shikari').
card_rarity('leonin shikari'/'DST', 'Rare').
card_artist('leonin shikari'/'DST', 'Wayne England').
card_number('leonin shikari'/'DST', '6').
card_flavor_text('leonin shikari'/'DST', 'Her instinct is as sharp as her blade.').
card_multiverse_id('leonin shikari'/'DST', '48579').

card_in_set('lich\'s tomb', 'DST').
card_original_type('lich\'s tomb'/'DST', 'Artifact').
card_original_text('lich\'s tomb'/'DST', 'You don\'t lose the game for having 0 or less life.\nWhenever you lose life, sacrifice a permanent for each 1 life you lost. (Damage causes loss of life.)').
card_first_print('lich\'s tomb', 'DST').
card_image_name('lich\'s tomb'/'DST', 'lich\'s tomb').
card_uid('lich\'s tomb'/'DST', 'DST:Lich\'s Tomb:lich\'s tomb').
card_rarity('lich\'s tomb'/'DST', 'Rare').
card_artist('lich\'s tomb'/'DST', 'Matt Cavotta').
card_number('lich\'s tomb'/'DST', '128').
card_multiverse_id('lich\'s tomb'/'DST', '46134').

card_in_set('loxodon mystic', 'DST').
card_original_type('loxodon mystic'/'DST', 'Creature — Elephant Cleric').
card_original_text('loxodon mystic'/'DST', '{W}, {T}: Tap target creature.').
card_first_print('loxodon mystic', 'DST').
card_image_name('loxodon mystic'/'DST', 'loxodon mystic').
card_uid('loxodon mystic'/'DST', 'DST:Loxodon Mystic:loxodon mystic').
card_rarity('loxodon mystic'/'DST', 'Common').
card_artist('loxodon mystic'/'DST', 'Edward P. Beard, Jr.').
card_number('loxodon mystic'/'DST', '7').
card_flavor_text('loxodon mystic'/'DST', 'As the nim storm across the plains, a lone priest stands in their path. At his silent gesture, the battle line breaks, and the tide turns.').
card_multiverse_id('loxodon mystic'/'DST', '39562').

card_in_set('machinate', 'DST').
card_original_type('machinate'/'DST', 'Instant').
card_original_text('machinate'/'DST', 'Look at the top X cards of your library, where X is the number of artifacts you control. Put one of those cards into your hand and the rest on the bottom of your library in any order.').
card_first_print('machinate', 'DST').
card_image_name('machinate'/'DST', 'machinate').
card_uid('machinate'/'DST', 'DST:Machinate:machinate').
card_rarity('machinate'/'DST', 'Common').
card_artist('machinate'/'DST', 'Wayne England').
card_number('machinate'/'DST', '24').
card_flavor_text('machinate'/'DST', 'As long as there are questions to be asked, the vedalken will ask them.').
card_multiverse_id('machinate'/'DST', '49829').

card_in_set('magnetic flux', 'DST').
card_original_type('magnetic flux'/'DST', 'Instant').
card_original_text('magnetic flux'/'DST', 'Artifact creatures you control gain flying until end of turn.').
card_first_print('magnetic flux', 'DST').
card_image_name('magnetic flux'/'DST', 'magnetic flux').
card_uid('magnetic flux'/'DST', 'DST:Magnetic Flux:magnetic flux').
card_rarity('magnetic flux'/'DST', 'Common').
card_artist('magnetic flux'/'DST', 'Alan Rabinowitz').
card_number('magnetic flux'/'DST', '25').
card_flavor_text('magnetic flux'/'DST', 'After the researcher\'s astonishing success, his assistants spent six hours finding a way to return the specimens safely to the ground.').
card_multiverse_id('magnetic flux'/'DST', '43559').

card_in_set('memnarch', 'DST').
card_original_type('memnarch'/'DST', 'Artifact Creature — Wizard Legend').
card_original_text('memnarch'/'DST', '{1}{U}{U}: Target permanent becomes an artifact in addition to its other types. (This effect doesn\'t end at end of turn.)\n{3}{U}: Gain control of target artifact. (This effect doesn\'t end at end of turn.)').
card_first_print('memnarch', 'DST').
card_image_name('memnarch'/'DST', 'memnarch').
card_uid('memnarch'/'DST', 'DST:Memnarch:memnarch').
card_rarity('memnarch'/'DST', 'Rare').
card_artist('memnarch'/'DST', 'Carl Critchlow').
card_number('memnarch'/'DST', '129').
card_flavor_text('memnarch'/'DST', 'In the blur between metal and flesh, Memnarch found madness.').
card_multiverse_id('memnarch'/'DST', '51054').

card_in_set('mephitic ooze', 'DST').
card_original_type('mephitic ooze'/'DST', 'Creature — Ooze').
card_original_text('mephitic ooze'/'DST', 'Mephitic Ooze gets +1/+0 for each artifact you control.\nWhenever Mephitic Ooze deals combat damage to a creature, destroy that creature. The creature can\'t be regenerated.').
card_first_print('mephitic ooze', 'DST').
card_image_name('mephitic ooze'/'DST', 'mephitic ooze').
card_uid('mephitic ooze'/'DST', 'DST:Mephitic Ooze:mephitic ooze').
card_rarity('mephitic ooze'/'DST', 'Rare').
card_artist('mephitic ooze'/'DST', 'Alex Horley-Orlandelli').
card_number('mephitic ooze'/'DST', '47').
card_flavor_text('mephitic ooze'/'DST', 'In Mephidross, refuse doesn\'t last long—it\'s all scavenged, reanimated, or consumed.').
card_multiverse_id('mephitic ooze'/'DST', '46154').

card_in_set('metal fatigue', 'DST').
card_original_type('metal fatigue'/'DST', 'Instant').
card_original_text('metal fatigue'/'DST', 'Tap all artifacts.').
card_first_print('metal fatigue', 'DST').
card_image_name('metal fatigue'/'DST', 'metal fatigue').
card_uid('metal fatigue'/'DST', 'DST:Metal Fatigue:metal fatigue').
card_rarity('metal fatigue'/'DST', 'Common').
card_artist('metal fatigue'/'DST', 'Arnie Swekel').
card_number('metal fatigue'/'DST', '8').
card_flavor_text('metal fatigue'/'DST', 'The Auriok have fought the metal hordes for so long now that knowing how to cripple them has become an instinct.').
card_multiverse_id('metal fatigue'/'DST', '45191').

card_in_set('mirrodin\'s core', 'DST').
card_original_type('mirrodin\'s core'/'DST', 'Land').
card_original_text('mirrodin\'s core'/'DST', '{T}: Add {1} to your mana pool.\n{T}: Put a charge counter on Mirrodin\'s Core.\n{T}, Remove a charge counter from Mirrodin\'s Core: Add one mana of any color to your mana pool.').
card_first_print('mirrodin\'s core', 'DST').
card_image_name('mirrodin\'s core'/'DST', 'mirrodin\'s core').
card_uid('mirrodin\'s core'/'DST', 'DST:Mirrodin\'s Core:mirrodin\'s core').
card_rarity('mirrodin\'s core'/'DST', 'Uncommon').
card_artist('mirrodin\'s core'/'DST', 'Greg Staples').
card_number('mirrodin\'s core'/'DST', '165').
card_multiverse_id('mirrodin\'s core'/'DST', '39421').

card_in_set('murderous spoils', 'DST').
card_original_type('murderous spoils'/'DST', 'Instant').
card_original_text('murderous spoils'/'DST', 'Destroy target nonblack creature. It can\'t be regenerated. You gain control of all Equipment that was attached to it. (This effect doesn\'t end at end of turn.)').
card_first_print('murderous spoils', 'DST').
card_image_name('murderous spoils'/'DST', 'murderous spoils').
card_uid('murderous spoils'/'DST', 'DST:Murderous Spoils:murderous spoils').
card_rarity('murderous spoils'/'DST', 'Uncommon').
card_artist('murderous spoils'/'DST', 'Adam Rex').
card_number('murderous spoils'/'DST', '48').
card_flavor_text('murderous spoils'/'DST', 'Scavengers are always the first to pay their respects.').
card_multiverse_id('murderous spoils'/'DST', '39377').

card_in_set('mycosynth lattice', 'DST').
card_original_type('mycosynth lattice'/'DST', 'Artifact').
card_original_text('mycosynth lattice'/'DST', 'All permanents are artifacts in addition to their other types.\nAll cards that aren\'t in play, spells, and permanents are colorless.\nPlayers may spend mana as though it were mana of any color.').
card_first_print('mycosynth lattice', 'DST').
card_image_name('mycosynth lattice'/'DST', 'mycosynth lattice').
card_uid('mycosynth lattice'/'DST', 'DST:Mycosynth Lattice:mycosynth lattice').
card_rarity('mycosynth lattice'/'DST', 'Rare').
card_artist('mycosynth lattice'/'DST', 'Anthony S. Waters').
card_number('mycosynth lattice'/'DST', '130').
card_multiverse_id('mycosynth lattice'/'DST', '50527').

card_in_set('myr landshaper', 'DST').
card_original_type('myr landshaper'/'DST', 'Artifact Creature — Myr').
card_original_text('myr landshaper'/'DST', '{T}: Target land becomes an artifact in addition to its other types until end of turn.').
card_first_print('myr landshaper', 'DST').
card_image_name('myr landshaper'/'DST', 'myr landshaper').
card_uid('myr landshaper'/'DST', 'DST:Myr Landshaper:myr landshaper').
card_rarity('myr landshaper'/'DST', 'Common').
card_artist('myr landshaper'/'DST', 'Greg Staples').
card_number('myr landshaper'/'DST', '131').
card_flavor_text('myr landshaper'/'DST', 'One thing the myr can\'t do is enjoy the scenery.').
card_multiverse_id('myr landshaper'/'DST', '45428').

card_in_set('myr matrix', 'DST').
card_original_type('myr matrix'/'DST', 'Artifact').
card_original_text('myr matrix'/'DST', 'Myr Matrix is indestructible. (\"Destroy\" effects and lethal damage don\'t destroy it.)\nAll Myr get +1/+1.\n{5}: Put a 1/1 Myr artifact creature token into play.').
card_first_print('myr matrix', 'DST').
card_image_name('myr matrix'/'DST', 'myr matrix').
card_uid('myr matrix'/'DST', 'DST:Myr Matrix:myr matrix').
card_rarity('myr matrix'/'DST', 'Rare').
card_artist('myr matrix'/'DST', 'Mark Tedin').
card_number('myr matrix'/'DST', '132').
card_multiverse_id('myr matrix'/'DST', '48049').

card_in_set('myr moonvessel', 'DST').
card_original_type('myr moonvessel'/'DST', 'Artifact Creature — Myr').
card_original_text('myr moonvessel'/'DST', 'When Myr Moonvessel is put into a graveyard from play, add {1} to your mana pool.').
card_first_print('myr moonvessel', 'DST').
card_image_name('myr moonvessel'/'DST', 'myr moonvessel').
card_uid('myr moonvessel'/'DST', 'DST:Myr Moonvessel:myr moonvessel').
card_rarity('myr moonvessel'/'DST', 'Common').
card_artist('myr moonvessel'/'DST', 'Dany Orizio').
card_number('myr moonvessel'/'DST', '133').
card_flavor_text('myr moonvessel'/'DST', 'Memnarch created the myr with three qualities in mind: dependability, controllability, and disposability.').
card_multiverse_id('myr moonvessel'/'DST', '46112').

card_in_set('nemesis mask', 'DST').
card_original_type('nemesis mask'/'DST', 'Artifact — Equipment').
card_original_text('nemesis mask'/'DST', 'All creatures able to block equipped creature do so.\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery. This card comes into play unattached and stays in play if the creature leaves play.)').
card_first_print('nemesis mask', 'DST').
card_image_name('nemesis mask'/'DST', 'nemesis mask').
card_uid('nemesis mask'/'DST', 'DST:Nemesis Mask:nemesis mask').
card_rarity('nemesis mask'/'DST', 'Uncommon').
card_artist('nemesis mask'/'DST', 'Adam Rex').
card_number('nemesis mask'/'DST', '134').
card_multiverse_id('nemesis mask'/'DST', '46172').

card_in_set('neurok prodigy', 'DST').
card_original_type('neurok prodigy'/'DST', 'Creature — Human Wizard').
card_original_text('neurok prodigy'/'DST', 'Flying\nDiscard an artifact card from your hand: Return Neurok Prodigy to its owner\'s hand.').
card_first_print('neurok prodigy', 'DST').
card_image_name('neurok prodigy'/'DST', 'neurok prodigy').
card_uid('neurok prodigy'/'DST', 'DST:Neurok Prodigy:neurok prodigy').
card_rarity('neurok prodigy'/'DST', 'Common').
card_artist('neurok prodigy'/'DST', 'Puddnhead').
card_number('neurok prodigy'/'DST', '26').
card_flavor_text('neurok prodigy'/'DST', '\"Why should I be bound by rules when I can see so far beyond them?\"').
card_multiverse_id('neurok prodigy'/'DST', '46116').

card_in_set('neurok transmuter', 'DST').
card_original_type('neurok transmuter'/'DST', 'Creature — Human Wizard').
card_original_text('neurok transmuter'/'DST', '{U}: Target creature becomes an artifact in addition to its other types until end of turn.\n{U}: Until end of turn, target artifact creature becomes blue and isn\'t an artifact.').
card_first_print('neurok transmuter', 'DST').
card_image_name('neurok transmuter'/'DST', 'neurok transmuter').
card_uid('neurok transmuter'/'DST', 'DST:Neurok Transmuter:neurok transmuter').
card_rarity('neurok transmuter'/'DST', 'Uncommon').
card_artist('neurok transmuter'/'DST', 'Mark Zug').
card_number('neurok transmuter'/'DST', '27').
card_multiverse_id('neurok transmuter'/'DST', '49839').

card_in_set('nim abomination', 'DST').
card_original_type('nim abomination'/'DST', 'Creature — Zombie').
card_original_text('nim abomination'/'DST', 'At the end of your turn, if Nim Abomination is untapped, you lose 3 life.').
card_first_print('nim abomination', 'DST').
card_image_name('nim abomination'/'DST', 'nim abomination').
card_uid('nim abomination'/'DST', 'DST:Nim Abomination:nim abomination').
card_rarity('nim abomination'/'DST', 'Uncommon').
card_artist('nim abomination'/'DST', 'Jim Murray').
card_number('nim abomination'/'DST', '49').
card_flavor_text('nim abomination'/'DST', 'It is one of the oldest nim, steeped so long in the Dross that it has lost all vestiges of humanity.').
card_multiverse_id('nim abomination'/'DST', '43502').

card_in_set('nourish', 'DST').
card_original_type('nourish'/'DST', 'Instant').
card_original_text('nourish'/'DST', 'You gain 6 life.').
card_first_print('nourish', 'DST').
card_image_name('nourish'/'DST', 'nourish').
card_uid('nourish'/'DST', 'DST:Nourish:nourish').
card_rarity('nourish'/'DST', 'Common').
card_artist('nourish'/'DST', 'Scott M. Fischer').
card_number('nourish'/'DST', '78').
card_flavor_text('nourish'/'DST', 'Gelfruit grows abundantly wherever Sylvok druids live, as if the Tangle knows that humans are not as hardy as the beasts or as nimble as the elves.').
card_multiverse_id('nourish'/'DST', '46076').

card_in_set('oxidda golem', 'DST').
card_original_type('oxidda golem'/'DST', 'Artifact Creature — Golem').
card_original_text('oxidda golem'/'DST', 'Affinity for Mountains (This spell costs {1} less to play for each Mountain you control.)\nHaste').
card_first_print('oxidda golem', 'DST').
card_image_name('oxidda golem'/'DST', 'oxidda golem').
card_uid('oxidda golem'/'DST', 'DST:Oxidda Golem:oxidda golem').
card_rarity('oxidda golem'/'DST', 'Common').
card_artist('oxidda golem'/'DST', 'Greg Staples').
card_number('oxidda golem'/'DST', '135').
card_flavor_text('oxidda golem'/'DST', 'The longer it patrols the smoldering crags of the Oxidda Chain, the more it adopts their fiery temper.').
card_multiverse_id('oxidda golem'/'DST', '46164').

card_in_set('oxidize', 'DST').
card_original_type('oxidize'/'DST', 'Instant').
card_original_text('oxidize'/'DST', 'Destroy target artifact. It can\'t be regenerated.').
card_image_name('oxidize'/'DST', 'oxidize').
card_uid('oxidize'/'DST', 'DST:Oxidize:oxidize').
card_rarity('oxidize'/'DST', 'Uncommon').
card_artist('oxidize'/'DST', 'Kev Walker').
card_number('oxidize'/'DST', '79').
card_flavor_text('oxidize'/'DST', '\"Ashes to ashes, rust to rust.\"\n—Viridian shaman').
card_multiverse_id('oxidize'/'DST', '46012').

card_in_set('panoptic mirror', 'DST').
card_original_type('panoptic mirror'/'DST', 'Artifact').
card_original_text('panoptic mirror'/'DST', 'Imprint {X}, {T}: You may remove an instant or sorcery card with converted mana cost X in your hand from the game. (That card is imprinted on this artifact.)\nAt the beginning of your upkeep, you may copy an imprinted instant or sorcery card and play the copy without paying its mana cost.').
card_first_print('panoptic mirror', 'DST').
card_image_name('panoptic mirror'/'DST', 'panoptic mirror').
card_uid('panoptic mirror'/'DST', 'DST:Panoptic Mirror:panoptic mirror').
card_rarity('panoptic mirror'/'DST', 'Rare').
card_artist('panoptic mirror'/'DST', 'Glen Angus').
card_number('panoptic mirror'/'DST', '136').
card_multiverse_id('panoptic mirror'/'DST', '47930').

card_in_set('pristine angel', 'DST').
card_original_type('pristine angel'/'DST', 'Creature — Angel').
card_original_text('pristine angel'/'DST', 'Flying\nAs long as Pristine Angel is untapped, it has protection from artifacts and from all colors.\nWhenever you play a spell, you may untap Pristine Angel.').
card_first_print('pristine angel', 'DST').
card_image_name('pristine angel'/'DST', 'pristine angel').
card_uid('pristine angel'/'DST', 'DST:Pristine Angel:pristine angel').
card_rarity('pristine angel'/'DST', 'Rare').
card_artist('pristine angel'/'DST', 'Scott M. Fischer').
card_number('pristine angel'/'DST', '9').
card_multiverse_id('pristine angel'/'DST', '47587').

card_in_set('psychic overload', 'DST').
card_original_type('psychic overload'/'DST', 'Enchant Permanent').
card_original_text('psychic overload'/'DST', 'When Psychic Overload comes into play, tap enchanted permanent.\nEnchanted permanent doesn\'t untap during its controller\'s untap step.\nEnchanted permanent has \"Discard two artifact cards from your hand: Untap this permanent.\"').
card_first_print('psychic overload', 'DST').
card_image_name('psychic overload'/'DST', 'psychic overload').
card_uid('psychic overload'/'DST', 'DST:Psychic Overload:psychic overload').
card_rarity('psychic overload'/'DST', 'Uncommon').
card_artist('psychic overload'/'DST', 'Jeremy Jarvis').
card_number('psychic overload'/'DST', '28').
card_multiverse_id('psychic overload'/'DST', '48163').

card_in_set('pteron ghost', 'DST').
card_original_type('pteron ghost'/'DST', 'Creature — Spirit').
card_original_text('pteron ghost'/'DST', 'Flying\nSacrifice Pteron Ghost: Regenerate target artifact.').
card_first_print('pteron ghost', 'DST').
card_image_name('pteron ghost'/'DST', 'pteron ghost').
card_uid('pteron ghost'/'DST', 'DST:Pteron Ghost:pteron ghost').
card_rarity('pteron ghost'/'DST', 'Common').
card_artist('pteron ghost'/'DST', 'Edward P. Beard, Jr.').
card_number('pteron ghost'/'DST', '10').
card_flavor_text('pteron ghost'/'DST', 'Defending to the death isn\'t always enough.').
card_multiverse_id('pteron ghost'/'DST', '49035').

card_in_set('pulse of the dross', 'DST').
card_original_type('pulse of the dross'/'DST', 'Sorcery').
card_original_text('pulse of the dross'/'DST', 'Target player reveals three cards from his or her hand and you choose one of them. That player discards that card. Then if that player has more cards in hand than you, return Pulse of the Dross to its owner\'s hand.').
card_first_print('pulse of the dross', 'DST').
card_image_name('pulse of the dross'/'DST', 'pulse of the dross').
card_uid('pulse of the dross'/'DST', 'DST:Pulse of the Dross:pulse of the dross').
card_rarity('pulse of the dross'/'DST', 'Rare').
card_artist('pulse of the dross'/'DST', 'Paolo Parente').
card_number('pulse of the dross'/'DST', '50').
card_flavor_text('pulse of the dross'/'DST', 'Before tyrants or torturers, there was evil.').
card_multiverse_id('pulse of the dross'/'DST', '39692').

card_in_set('pulse of the fields', 'DST').
card_original_type('pulse of the fields'/'DST', 'Instant').
card_original_text('pulse of the fields'/'DST', 'You gain 4 life. Then if an opponent has more life than you, return Pulse of the Fields to its owner\'s hand.').
card_first_print('pulse of the fields', 'DST').
card_image_name('pulse of the fields'/'DST', 'pulse of the fields').
card_uid('pulse of the fields'/'DST', 'DST:Pulse of the Fields:pulse of the fields').
card_rarity('pulse of the fields'/'DST', 'Rare').
card_artist('pulse of the fields'/'DST', 'Paolo Parente').
card_number('pulse of the fields'/'DST', '11').
card_flavor_text('pulse of the fields'/'DST', 'Before religion or civilization, there was order.').
card_multiverse_id('pulse of the fields'/'DST', '39697').

card_in_set('pulse of the forge', 'DST').
card_original_type('pulse of the forge'/'DST', 'Instant').
card_original_text('pulse of the forge'/'DST', 'Pulse of the Forge deals 4 damage to target player. Then if that player has more life than you, return Pulse of the Forge to its owner\'s hand.').
card_first_print('pulse of the forge', 'DST').
card_image_name('pulse of the forge'/'DST', 'pulse of the forge').
card_uid('pulse of the forge'/'DST', 'DST:Pulse of the Forge:pulse of the forge').
card_rarity('pulse of the forge'/'DST', 'Rare').
card_artist('pulse of the forge'/'DST', 'Paolo Parente').
card_number('pulse of the forge'/'DST', '66').
card_flavor_text('pulse of the forge'/'DST', 'Before wars or weapons, there was anger.').
card_multiverse_id('pulse of the forge'/'DST', '39410').

card_in_set('pulse of the grid', 'DST').
card_original_type('pulse of the grid'/'DST', 'Instant').
card_original_text('pulse of the grid'/'DST', 'Draw two cards, then discard a card from your hand. Then if an opponent has more cards in hand than you, return Pulse of the Grid to its owner\'s hand.').
card_first_print('pulse of the grid', 'DST').
card_image_name('pulse of the grid'/'DST', 'pulse of the grid').
card_uid('pulse of the grid'/'DST', 'DST:Pulse of the Grid:pulse of the grid').
card_rarity('pulse of the grid'/'DST', 'Rare').
card_artist('pulse of the grid'/'DST', 'Wayne England').
card_number('pulse of the grid'/'DST', '29').
card_flavor_text('pulse of the grid'/'DST', 'Before labs or lectures, there was insight.').
card_multiverse_id('pulse of the grid'/'DST', '45975').

card_in_set('pulse of the tangle', 'DST').
card_original_type('pulse of the tangle'/'DST', 'Sorcery').
card_original_text('pulse of the tangle'/'DST', 'Put a 3/3 green Beast creature token into play. Then if an opponent controls more creatures than you, return Pulse of the Tangle to its owner\'s hand.').
card_first_print('pulse of the tangle', 'DST').
card_image_name('pulse of the tangle'/'DST', 'pulse of the tangle').
card_uid('pulse of the tangle'/'DST', 'DST:Pulse of the Tangle:pulse of the tangle').
card_rarity('pulse of the tangle'/'DST', 'Rare').
card_artist('pulse of the tangle'/'DST', 'Wayne England').
card_number('pulse of the tangle'/'DST', '80').
card_flavor_text('pulse of the tangle'/'DST', 'Before predators or prey, there was life.').
card_multiverse_id('pulse of the tangle'/'DST', '39690').

card_in_set('purge', 'DST').
card_original_type('purge'/'DST', 'Instant').
card_original_text('purge'/'DST', 'Destroy target artifact creature or black creature. It can\'t be regenerated.').
card_first_print('purge', 'DST').
card_image_name('purge'/'DST', 'purge').
card_uid('purge'/'DST', 'DST:Purge:purge').
card_rarity('purge'/'DST', 'Uncommon').
card_artist('purge'/'DST', 'Pete Venters').
card_number('purge'/'DST', '12').
card_flavor_text('purge'/'DST', 'For the first and last time, the horrific creature experienced terror.').
card_multiverse_id('purge'/'DST', '34479').

card_in_set('quicksilver behemoth', 'DST').
card_original_type('quicksilver behemoth'/'DST', 'Creature — Beast').
card_original_text('quicksilver behemoth'/'DST', 'Affinity for artifacts (This spell costs {1} less to play for each artifact you control.)\nWhen Quicksilver Behemoth attacks or blocks, return it to its owner\'s hand at end of combat. (Return it only if it\'s in play.)').
card_first_print('quicksilver behemoth', 'DST').
card_image_name('quicksilver behemoth'/'DST', 'quicksilver behemoth').
card_uid('quicksilver behemoth'/'DST', 'DST:Quicksilver Behemoth:quicksilver behemoth').
card_rarity('quicksilver behemoth'/'DST', 'Common').
card_artist('quicksilver behemoth'/'DST', 'Anthony S. Waters').
card_number('quicksilver behemoth'/'DST', '30').
card_multiverse_id('quicksilver behemoth'/'DST', '47799').

card_in_set('razor golem', 'DST').
card_original_type('razor golem'/'DST', 'Artifact Creature — Golem').
card_original_text('razor golem'/'DST', 'Affinity for Plains (This spell costs {1} less to play for each Plains you control.)\nAttacking doesn\'t cause Razor Golem to tap.').
card_first_print('razor golem', 'DST').
card_image_name('razor golem'/'DST', 'razor golem').
card_uid('razor golem'/'DST', 'DST:Razor Golem:razor golem').
card_rarity('razor golem'/'DST', 'Common').
card_artist('razor golem'/'DST', 'Christopher Moeller').
card_number('razor golem'/'DST', '137').
card_flavor_text('razor golem'/'DST', 'The longer it stands among the deadly blades of the Razor Fields, the more it adopts their harsh exterior.').
card_multiverse_id('razor golem'/'DST', '46143').

card_in_set('reap and sow', 'DST').
card_original_type('reap and sow'/'DST', 'Sorcery').
card_original_text('reap and sow'/'DST', 'Choose one Destroy target land; or search your library for a land card, put that card into play, then shuffle your library.\nEntwine {1}{G} (Choose both if you pay the entwine cost.)').
card_first_print('reap and sow', 'DST').
card_image_name('reap and sow'/'DST', 'reap and sow').
card_uid('reap and sow'/'DST', 'DST:Reap and Sow:reap and sow').
card_rarity('reap and sow'/'DST', 'Common').
card_artist('reap and sow'/'DST', 'Rob Alexander').
card_number('reap and sow'/'DST', '81').
card_multiverse_id('reap and sow'/'DST', '43503').

card_in_set('rebuking ceremony', 'DST').
card_original_type('rebuking ceremony'/'DST', 'Sorcery').
card_original_text('rebuking ceremony'/'DST', 'Put two target artifacts on top of their owners\' libraries.').
card_first_print('rebuking ceremony', 'DST').
card_image_name('rebuking ceremony'/'DST', 'rebuking ceremony').
card_uid('rebuking ceremony'/'DST', 'DST:Rebuking Ceremony:rebuking ceremony').
card_rarity('rebuking ceremony'/'DST', 'Rare').
card_artist('rebuking ceremony'/'DST', 'Ittoku').
card_number('rebuking ceremony'/'DST', '82').
card_flavor_text('rebuking ceremony'/'DST', 'From the Radix at the hub of the Tangle all the way out to the Edges of Forgetting, a Viridian elf\'s life is built around the temporary.').
card_multiverse_id('rebuking ceremony'/'DST', '50533').

card_in_set('reshape', 'DST').
card_original_type('reshape'/'DST', 'Sorcery').
card_original_text('reshape'/'DST', 'As an additional cost to play Reshape, sacrifice an artifact.\nSearch your library for an artifact card with converted mana cost X or less and put it into play. Then shuffle your library.').
card_first_print('reshape', 'DST').
card_image_name('reshape'/'DST', 'reshape').
card_uid('reshape'/'DST', 'DST:Reshape:reshape').
card_rarity('reshape'/'DST', 'Rare').
card_artist('reshape'/'DST', 'Jon Foster').
card_number('reshape'/'DST', '31').
card_flavor_text('reshape'/'DST', '\"Today\'s paperweight, tomorrow\'s leveler.\"\n—Bruenna, Neurok leader').
card_multiverse_id('reshape'/'DST', '48147').

card_in_set('retract', 'DST').
card_original_type('retract'/'DST', 'Instant').
card_original_text('retract'/'DST', 'Return all artifacts you control to their owner\'s hand.').
card_first_print('retract', 'DST').
card_image_name('retract'/'DST', 'retract').
card_uid('retract'/'DST', 'DST:Retract:retract').
card_rarity('retract'/'DST', 'Rare').
card_artist('retract'/'DST', 'Matt Cavotta').
card_number('retract'/'DST', '32').
card_flavor_text('retract'/'DST', '\"Secrets are meant to remain secret.\"\n—Memnarch').
card_multiverse_id('retract'/'DST', '48573').

card_in_set('ritual of restoration', 'DST').
card_original_type('ritual of restoration'/'DST', 'Sorcery').
card_original_text('ritual of restoration'/'DST', 'Return target artifact card from your graveyard to your hand.').
card_first_print('ritual of restoration', 'DST').
card_image_name('ritual of restoration'/'DST', 'ritual of restoration').
card_uid('ritual of restoration'/'DST', 'DST:Ritual of Restoration:ritual of restoration').
card_rarity('ritual of restoration'/'DST', 'Common').
card_artist('ritual of restoration'/'DST', 'Dany Orizio').
card_number('ritual of restoration'/'DST', '13').
card_flavor_text('ritual of restoration'/'DST', 'Leonin raising ceremonies take place at the white sun\'s dawn, the rebirth of artifacts mirroring the birth of a new day.').
card_multiverse_id('ritual of restoration'/'DST', '48576').

card_in_set('roaring slagwurm', 'DST').
card_original_type('roaring slagwurm'/'DST', 'Creature — Wurm').
card_original_text('roaring slagwurm'/'DST', 'Whenever Roaring Slagwurm attacks, tap all artifacts.').
card_first_print('roaring slagwurm', 'DST').
card_image_name('roaring slagwurm'/'DST', 'roaring slagwurm').
card_uid('roaring slagwurm'/'DST', 'DST:Roaring Slagwurm:roaring slagwurm').
card_rarity('roaring slagwurm'/'DST', 'Rare').
card_artist('roaring slagwurm'/'DST', 'David Martin').
card_number('roaring slagwurm'/'DST', '83').
card_flavor_text('roaring slagwurm'/'DST', 'Wurms compete for nesting grounds near the Radix. The young who hatch closest have the best chance for survival.').
card_multiverse_id('roaring slagwurm'/'DST', '46083').

card_in_set('savage beating', 'DST').
card_original_type('savage beating'/'DST', 'Instant').
card_original_text('savage beating'/'DST', 'Play Savage Beating only during your turn and only during combat.\nChoose one Creatures you control gain double strike until end of turn; or untap all creatures you control and after this phase, there is an additional combat phase.\nEntwine {1}{R}').
card_first_print('savage beating', 'DST').
card_image_name('savage beating'/'DST', 'savage beating').
card_uid('savage beating'/'DST', 'DST:Savage Beating:savage beating').
card_rarity('savage beating'/'DST', 'Rare').
card_artist('savage beating'/'DST', 'Matt Thompson').
card_number('savage beating'/'DST', '67').
card_multiverse_id('savage beating'/'DST', '43506').

card_in_set('scavenging scarab', 'DST').
card_original_type('scavenging scarab'/'DST', 'Creature — Insect').
card_original_text('scavenging scarab'/'DST', 'Scavenging Scarab can\'t block.').
card_first_print('scavenging scarab', 'DST').
card_image_name('scavenging scarab'/'DST', 'scavenging scarab').
card_uid('scavenging scarab'/'DST', 'DST:Scavenging Scarab:scavenging scarab').
card_rarity('scavenging scarab'/'DST', 'Common').
card_artist('scavenging scarab'/'DST', 'Jeff Easley').
card_number('scavenging scarab'/'DST', '51').
card_flavor_text('scavenging scarab'/'DST', 'The beetles feed not on the flesh of corpses but on the metal, grinding out the iron and steel to add to their own bulky shells.').
card_multiverse_id('scavenging scarab'/'DST', '46093').

card_in_set('screams from within', 'DST').
card_original_type('screams from within'/'DST', 'Enchant Creature').
card_original_text('screams from within'/'DST', 'Enchanted creature gets -1/-1.\nWhen enchanted creature is put into a graveyard, return Screams from Within from your graveyard to play.').
card_first_print('screams from within', 'DST').
card_image_name('screams from within'/'DST', 'screams from within').
card_uid('screams from within'/'DST', 'DST:Screams from Within:screams from within').
card_rarity('screams from within'/'DST', 'Uncommon').
card_artist('screams from within'/'DST', 'Hugh Jamieson').
card_number('screams from within'/'DST', '52').
card_flavor_text('screams from within'/'DST', 'There\'s no escaping an enemy that lives inside your head.').
card_multiverse_id('screams from within'/'DST', '43517').

card_in_set('scrounge', 'DST').
card_original_type('scrounge'/'DST', 'Sorcery').
card_original_text('scrounge'/'DST', 'Target opponent chooses an artifact card in his or her graveyard. Put that card into play under your control.').
card_first_print('scrounge', 'DST').
card_image_name('scrounge'/'DST', 'scrounge').
card_uid('scrounge'/'DST', 'DST:Scrounge:scrounge').
card_rarity('scrounge'/'DST', 'Uncommon').
card_artist('scrounge'/'DST', 'Pete Venters').
card_number('scrounge'/'DST', '53').
card_flavor_text('scrounge'/'DST', 'When leonin settlers abandon their homes to the nim, items of value are often left behind.').
card_multiverse_id('scrounge'/'DST', '46568').

card_in_set('second sight', 'DST').
card_original_type('second sight'/'DST', 'Instant').
card_original_text('second sight'/'DST', 'Choose one Look at the top five cards of target opponent\'s library, then put them back in any order; or look at the top five cards of your library, then put them back in any order.\nEntwine {U} (Choose both if you pay the entwine cost.)').
card_first_print('second sight', 'DST').
card_image_name('second sight'/'DST', 'second sight').
card_uid('second sight'/'DST', 'DST:Second Sight:second sight').
card_rarity('second sight'/'DST', 'Uncommon').
card_artist('second sight'/'DST', 'Luca Zontini').
card_number('second sight'/'DST', '33').
card_multiverse_id('second sight'/'DST', '43510').

card_in_set('serum powder', 'DST').
card_original_type('serum powder'/'DST', 'Artifact').
card_original_text('serum powder'/'DST', '{T}: Add {1} to your mana pool.\nAny time you could mulligan and Serum Powder is in your hand, you may remove your hand from the game, then draw that many cards. (You can do this in addition to taking mulligans.)').
card_first_print('serum powder', 'DST').
card_image_name('serum powder'/'DST', 'serum powder').
card_uid('serum powder'/'DST', 'DST:Serum Powder:serum powder').
card_rarity('serum powder'/'DST', 'Rare').
card_artist('serum powder'/'DST', 'Matt Thompson').
card_number('serum powder'/'DST', '138').
card_multiverse_id('serum powder'/'DST', '48920').

card_in_set('shield of kaldra', 'DST').
card_original_type('shield of kaldra'/'DST', 'Legendary Artifact — Equipment').
card_original_text('shield of kaldra'/'DST', 'Equipment named Sword of Kaldra, Shield of Kaldra, and Helm of Kaldra are indestructible.\nEquipped creature is indestructible. (\"Destroy\" effects and lethal damage don\'t destroy it.)\nEquip {4}').
card_image_name('shield of kaldra'/'DST', 'shield of kaldra').
card_uid('shield of kaldra'/'DST', 'DST:Shield of Kaldra:shield of kaldra').
card_rarity('shield of kaldra'/'DST', 'Rare').
card_artist('shield of kaldra'/'DST', 'Donato Giancola').
card_number('shield of kaldra'/'DST', '139').
card_multiverse_id('shield of kaldra'/'DST', '48582').

card_in_set('shriveling rot', 'DST').
card_original_type('shriveling rot'/'DST', 'Instant').
card_original_text('shriveling rot'/'DST', 'Choose one Until end of turn, whenever a creature is dealt damage, destroy it; or until end of turn, whenever a creature is put into a graveyard from play, that creature\'s controller loses life equal to its toughness.\nEntwine {2}{B} (Choose both if you pay the entwine cost.)').
card_first_print('shriveling rot', 'DST').
card_image_name('shriveling rot'/'DST', 'shriveling rot').
card_uid('shriveling rot'/'DST', 'DST:Shriveling Rot:shriveling rot').
card_rarity('shriveling rot'/'DST', 'Rare').
card_artist('shriveling rot'/'DST', 'Alex Horley-Orlandelli').
card_number('shriveling rot'/'DST', '54').
card_multiverse_id('shriveling rot'/'DST', '47792').

card_in_set('shunt', 'DST').
card_original_type('shunt'/'DST', 'Instant').
card_original_text('shunt'/'DST', 'Change the target of target spell with a single target.').
card_first_print('shunt', 'DST').
card_image_name('shunt'/'DST', 'shunt').
card_uid('shunt'/'DST', 'DST:Shunt:shunt').
card_rarity('shunt'/'DST', 'Rare').
card_artist('shunt'/'DST', 'Greg Hildebrandt').
card_number('shunt'/'DST', '68').
card_flavor_text('shunt'/'DST', '\"You might want to work on your aim.\"').
card_multiverse_id('shunt'/'DST', '49836').

card_in_set('skullclamp', 'DST').
card_original_type('skullclamp'/'DST', 'Artifact — Equipment').
card_original_text('skullclamp'/'DST', 'Equipped creature gets +1/-1.\nWhen equipped creature is put into a graveyard, draw two cards.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery. This card comes into play unattached and stays in play if the creature leaves play.)').
card_first_print('skullclamp', 'DST').
card_image_name('skullclamp'/'DST', 'skullclamp').
card_uid('skullclamp'/'DST', 'DST:Skullclamp:skullclamp').
card_rarity('skullclamp'/'DST', 'Uncommon').
card_artist('skullclamp'/'DST', 'Luca Zontini').
card_number('skullclamp'/'DST', '140').
card_multiverse_id('skullclamp'/'DST', '48197').

card_in_set('slobad, goblin tinkerer', 'DST').
card_original_type('slobad, goblin tinkerer'/'DST', 'Creature — Goblin Artificer Legend').
card_original_text('slobad, goblin tinkerer'/'DST', 'Sacrifice an artifact: Target artifact becomes indestructible until end of turn. (\"Destroy\" effects and lethal damage don\'t destroy that artifact.)').
card_first_print('slobad, goblin tinkerer', 'DST').
card_image_name('slobad, goblin tinkerer'/'DST', 'slobad, goblin tinkerer').
card_uid('slobad, goblin tinkerer'/'DST', 'DST:Slobad, Goblin Tinkerer:slobad, goblin tinkerer').
card_rarity('slobad, goblin tinkerer'/'DST', 'Rare').
card_artist('slobad, goblin tinkerer'/'DST', 'Kev Walker').
card_number('slobad, goblin tinkerer'/'DST', '69').
card_flavor_text('slobad, goblin tinkerer'/'DST', '\"I used to joke that he had been exiled for being too smart. Now I know why he never laughed.\"\n—Glissa Sunseeker').
card_multiverse_id('slobad, goblin tinkerer'/'DST', '51055').

card_in_set('soulscour', 'DST').
card_original_type('soulscour'/'DST', 'Sorcery').
card_original_text('soulscour'/'DST', 'Destroy all nonartifact permanents.').
card_first_print('soulscour', 'DST').
card_image_name('soulscour'/'DST', 'soulscour').
card_uid('soulscour'/'DST', 'DST:Soulscour:soulscour').
card_rarity('soulscour'/'DST', 'Rare').
card_artist('soulscour'/'DST', 'Kev Walker').
card_number('soulscour'/'DST', '14').
card_flavor_text('soulscour'/'DST', '\"I have seen the end of times, a future in which all our kind are torn from this world.\"\n—Ushanti, leonin shaman').
card_multiverse_id('soulscour'/'DST', '43511').

card_in_set('spawning pit', 'DST').
card_original_type('spawning pit'/'DST', 'Artifact').
card_original_text('spawning pit'/'DST', 'Sacrifice a creature: Put a charge counter on Spawning Pit.\n{1}, Remove two charge counters from Spawning Pit: Put a 2/2 Spawn artifact creature token into play.').
card_first_print('spawning pit', 'DST').
card_image_name('spawning pit'/'DST', 'spawning pit').
card_uid('spawning pit'/'DST', 'DST:Spawning Pit:spawning pit').
card_rarity('spawning pit'/'DST', 'Uncommon').
card_artist('spawning pit'/'DST', 'Tony Szczudlo').
card_number('spawning pit'/'DST', '141').
card_multiverse_id('spawning pit'/'DST', '26732').

card_in_set('specter\'s shroud', 'DST').
card_original_type('specter\'s shroud'/'DST', 'Artifact — Equipment').
card_original_text('specter\'s shroud'/'DST', 'Equipped creature gets +1/+0.\nWhenever equipped creature deals combat damage to a player, that player discards a card from his or her hand.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery. This card comes into play unattached and stays in play if the creature leaves play.)').
card_first_print('specter\'s shroud', 'DST').
card_image_name('specter\'s shroud'/'DST', 'specter\'s shroud').
card_uid('specter\'s shroud'/'DST', 'DST:Specter\'s Shroud:specter\'s shroud').
card_rarity('specter\'s shroud'/'DST', 'Uncommon').
card_artist('specter\'s shroud'/'DST', 'Greg Hildebrandt').
card_number('specter\'s shroud'/'DST', '142').
card_multiverse_id('specter\'s shroud'/'DST', '49094').

card_in_set('spellbinder', 'DST').
card_original_type('spellbinder'/'DST', 'Artifact — Equipment').
card_original_text('spellbinder'/'DST', 'Imprint When Spellbinder comes into play, you may remove an instant card in your hand from the game.\nWhenever equipped creature deals combat damage to a player, you may copy the imprinted instant card and play the copy without paying its mana cost.\nEquip {4}').
card_first_print('spellbinder', 'DST').
card_image_name('spellbinder'/'DST', 'spellbinder').
card_uid('spellbinder'/'DST', 'DST:Spellbinder:spellbinder').
card_rarity('spellbinder'/'DST', 'Rare').
card_artist('spellbinder'/'DST', 'Ron Spencer').
card_number('spellbinder'/'DST', '143').
card_multiverse_id('spellbinder'/'DST', '50540').

card_in_set('spincrusher', 'DST').
card_original_type('spincrusher'/'DST', 'Artifact Creature').
card_original_text('spincrusher'/'DST', 'Whenever Spincrusher blocks, put a +1/+1 counter on it. \nRemove a +1/+1 counter from Spincrusher: Spincrusher is unblockable this turn.').
card_first_print('spincrusher', 'DST').
card_image_name('spincrusher'/'DST', 'spincrusher').
card_uid('spincrusher'/'DST', 'DST:Spincrusher:spincrusher').
card_rarity('spincrusher'/'DST', 'Uncommon').
card_artist('spincrusher'/'DST', 'Greg Staples').
card_number('spincrusher'/'DST', '144').
card_flavor_text('spincrusher'/'DST', 'It rolls with the punches.').
card_multiverse_id('spincrusher'/'DST', '50529').

card_in_set('spire golem', 'DST').
card_original_type('spire golem'/'DST', 'Artifact Creature — Golem').
card_original_text('spire golem'/'DST', 'Affinity for Islands (This spell costs {1} less to play for each Island you control.)\nFlying').
card_first_print('spire golem', 'DST').
card_image_name('spire golem'/'DST', 'spire golem').
card_uid('spire golem'/'DST', 'DST:Spire Golem:spire golem').
card_rarity('spire golem'/'DST', 'Common').
card_artist('spire golem'/'DST', 'Daren Bader').
card_number('spire golem'/'DST', '145').
card_flavor_text('spire golem'/'DST', 'The longer it soars above the shimmering swirls of the Quicksilver Sea, the more it adopts their unpredictability.').
card_multiverse_id('spire golem'/'DST', '46150').

card_in_set('stand together', 'DST').
card_original_type('stand together'/'DST', 'Instant').
card_original_text('stand together'/'DST', 'Put two +1/+1 counters on target creature and two +1/+1 counters on another target creature.').
card_first_print('stand together', 'DST').
card_image_name('stand together'/'DST', 'stand together').
card_uid('stand together'/'DST', 'DST:Stand Together:stand together').
card_rarity('stand together'/'DST', 'Uncommon').
card_artist('stand together'/'DST', 'Luca Zontini').
card_number('stand together'/'DST', '84').
card_flavor_text('stand together'/'DST', 'Strange times make for strange allies.').
card_multiverse_id('stand together'/'DST', '49438').

card_in_set('steelshaper apprentice', 'DST').
card_original_type('steelshaper apprentice'/'DST', 'Creature — Human Soldier').
card_original_text('steelshaper apprentice'/'DST', '{W}, {T}, Return Steelshaper Apprentice to its owner\'s hand: Search your library for an Equipment card, reveal that card, and put it into your hand. Then shuffle your library.').
card_first_print('steelshaper apprentice', 'DST').
card_image_name('steelshaper apprentice'/'DST', 'steelshaper apprentice').
card_uid('steelshaper apprentice'/'DST', 'DST:Steelshaper Apprentice:steelshaper apprentice').
card_rarity('steelshaper apprentice'/'DST', 'Rare').
card_artist('steelshaper apprentice'/'DST', 'Tim Hildebrandt').
card_number('steelshaper apprentice'/'DST', '15').
card_flavor_text('steelshaper apprentice'/'DST', 'Theirs is a craft brought about by necessity, now raised to the level of art.').
card_multiverse_id('steelshaper apprentice'/'DST', '49078').

card_in_set('stir the pride', 'DST').
card_original_type('stir the pride'/'DST', 'Instant').
card_original_text('stir the pride'/'DST', 'Choose one Creatures you control get +2/+2 until end of turn; or until end of turn, creatures you control gain \"Whenever this creature deals damage, you gain that much life.\"\nEntwine {1}{W} (Choose both if you pay the entwine cost.)').
card_first_print('stir the pride', 'DST').
card_image_name('stir the pride'/'DST', 'stir the pride').
card_uid('stir the pride'/'DST', 'DST:Stir the Pride:stir the pride').
card_rarity('stir the pride'/'DST', 'Uncommon').
card_artist('stir the pride'/'DST', 'Matt Cavotta').
card_number('stir the pride'/'DST', '16').
card_multiverse_id('stir the pride'/'DST', '45961').

card_in_set('sundering titan', 'DST').
card_original_type('sundering titan'/'DST', 'Artifact Creature').
card_original_text('sundering titan'/'DST', 'When Sundering Titan comes into play, choose a land of each basic land type, then destroy those lands.\nWhen Sundering Titan leaves play, choose a land of each basic land type, then destroy those lands.').
card_first_print('sundering titan', 'DST').
card_image_name('sundering titan'/'DST', 'sundering titan').
card_uid('sundering titan'/'DST', 'DST:Sundering Titan:sundering titan').
card_rarity('sundering titan'/'DST', 'Rare').
card_artist('sundering titan'/'DST', 'Jim Murray').
card_number('sundering titan'/'DST', '146').
card_multiverse_id('sundering titan'/'DST', '42084').

card_in_set('surestrike trident', 'DST').
card_original_type('surestrike trident'/'DST', 'Artifact — Equipment').
card_original_text('surestrike trident'/'DST', 'Equipped creature has first strike and \"{T}, Unattach Surestrike Trident: This creature deals damage equal to its power to target player.\"\nEquip {4} ({4}: Attach to target creature you control. Equip only as a sorcery. This card comes into play unattached and stays in play if the creature leaves play.)').
card_first_print('surestrike trident', 'DST').
card_image_name('surestrike trident'/'DST', 'surestrike trident').
card_uid('surestrike trident'/'DST', 'DST:Surestrike Trident:surestrike trident').
card_rarity('surestrike trident'/'DST', 'Uncommon').
card_artist('surestrike trident'/'DST', 'Ben Thompson').
card_number('surestrike trident'/'DST', '147').
card_multiverse_id('surestrike trident'/'DST', '46158').

card_in_set('sword of fire and ice', 'DST').
card_original_type('sword of fire and ice'/'DST', 'Artifact — Equipment').
card_original_text('sword of fire and ice'/'DST', 'Equipped creature gets +2/+2 and has protection from red and from blue.\nWhenever equipped creature deals combat damage to a player, Sword of Fire and Ice deals 2 damage to target creature or player and you draw a card.\nEquip {2}').
card_image_name('sword of fire and ice'/'DST', 'sword of fire and ice').
card_uid('sword of fire and ice'/'DST', 'DST:Sword of Fire and Ice:sword of fire and ice').
card_rarity('sword of fire and ice'/'DST', 'Rare').
card_artist('sword of fire and ice'/'DST', 'Mark Zug').
card_number('sword of fire and ice'/'DST', '148').
card_multiverse_id('sword of fire and ice'/'DST', '46429').

card_in_set('sword of light and shadow', 'DST').
card_original_type('sword of light and shadow'/'DST', 'Artifact — Equipment').
card_original_text('sword of light and shadow'/'DST', 'Equipped creature gets +2/+2 and has protection from white and from black.\nWhenever equipped creature deals combat damage to a player, you gain 3 life and you may return up to one target creature card from your graveyard to your hand.\nEquip {2}').
card_image_name('sword of light and shadow'/'DST', 'sword of light and shadow').
card_uid('sword of light and shadow'/'DST', 'DST:Sword of Light and Shadow:sword of light and shadow').
card_rarity('sword of light and shadow'/'DST', 'Rare').
card_artist('sword of light and shadow'/'DST', 'Mark Zug').
card_number('sword of light and shadow'/'DST', '149').
card_multiverse_id('sword of light and shadow'/'DST', '47453').

card_in_set('synod artificer', 'DST').
card_original_type('synod artificer'/'DST', 'Creature — Vedalken Artificer').
card_original_text('synod artificer'/'DST', '{X}, {T}: Tap X target noncreature artifacts.\n{X}, {T}: Untap X target noncreature artifacts.').
card_first_print('synod artificer', 'DST').
card_image_name('synod artificer'/'DST', 'synod artificer').
card_uid('synod artificer'/'DST', 'DST:Synod Artificer:synod artificer').
card_rarity('synod artificer'/'DST', 'Rare').
card_artist('synod artificer'/'DST', 'Mark Zug').
card_number('synod artificer'/'DST', '34').
card_flavor_text('synod artificer'/'DST', 'Memnarch shared a touch of his power with the vedalken in return for their service. It is a debt he intends to collect.').
card_multiverse_id('synod artificer'/'DST', '49021').

card_in_set('talon of pain', 'DST').
card_original_type('talon of pain'/'DST', 'Artifact').
card_original_text('talon of pain'/'DST', 'Whenever a source you control other than Talon of Pain deals damage to an opponent, put a charge counter on Talon of Pain.\n{X}, {T}, Remove X charge counters from Talon of Pain: Talon of Pain deals X damage to target creature or player.').
card_first_print('talon of pain', 'DST').
card_image_name('talon of pain'/'DST', 'talon of pain').
card_uid('talon of pain'/'DST', 'DST:Talon of Pain:talon of pain').
card_rarity('talon of pain'/'DST', 'Uncommon').
card_artist('talon of pain'/'DST', 'Daren Bader').
card_number('talon of pain'/'DST', '150').
card_multiverse_id('talon of pain'/'DST', '46111').

card_in_set('tangle golem', 'DST').
card_original_type('tangle golem'/'DST', 'Artifact Creature — Golem').
card_original_text('tangle golem'/'DST', 'Affinity for Forests (This spell costs {1} less to play for each Forest you control.)').
card_first_print('tangle golem', 'DST').
card_image_name('tangle golem'/'DST', 'tangle golem').
card_uid('tangle golem'/'DST', 'DST:Tangle Golem:tangle golem').
card_rarity('tangle golem'/'DST', 'Common').
card_artist('tangle golem'/'DST', 'Arnie Swekel').
card_number('tangle golem'/'DST', '151').
card_flavor_text('tangle golem'/'DST', 'The longer it weaves through the twisted vines of the Tangle, the more it adopts their hidden strength.').
card_multiverse_id('tangle golem'/'DST', '46171').

card_in_set('tangle spider', 'DST').
card_original_type('tangle spider'/'DST', 'Creature — Spider').
card_original_text('tangle spider'/'DST', 'You may play Tangle Spider any time you could play an instant.\nTangle Spider may block as though it had flying.').
card_first_print('tangle spider', 'DST').
card_image_name('tangle spider'/'DST', 'tangle spider').
card_uid('tangle spider'/'DST', 'DST:Tangle Spider:tangle spider').
card_rarity('tangle spider'/'DST', 'Common').
card_artist('tangle spider'/'DST', 'Terese Nielsen').
card_number('tangle spider'/'DST', '85').
card_flavor_text('tangle spider'/'DST', 'They\'re considered good luck by the elves they don\'t eat.').
card_multiverse_id('tangle spider'/'DST', '46548').

card_in_set('tanglewalker', 'DST').
card_original_type('tanglewalker'/'DST', 'Creature — Dryad').
card_original_text('tanglewalker'/'DST', 'Creatures you control are unblockable as long as defending player controls an artifact land.').
card_first_print('tanglewalker', 'DST').
card_image_name('tanglewalker'/'DST', 'tanglewalker').
card_uid('tanglewalker'/'DST', 'DST:Tanglewalker:tanglewalker').
card_rarity('tanglewalker'/'DST', 'Uncommon').
card_artist('tanglewalker'/'DST', 'Pete Venters').
card_number('tanglewalker'/'DST', '86').
card_flavor_text('tanglewalker'/'DST', 'In bringing life to Mirrodin, Memnarch was more successful than he knew—for Mirrodin began to take on a life of its own.').
card_multiverse_id('tanglewalker'/'DST', '43547').

card_in_set('tears of rage', 'DST').
card_original_type('tears of rage'/'DST', 'Instant').
card_original_text('tears of rage'/'DST', 'Play Tears of Rage only during the declare attackers step.\nAttacking creatures you control get +X/+0 until end of turn, where X is the number of attacking creatures. Sacrifice those creatures at end of turn.').
card_first_print('tears of rage', 'DST').
card_image_name('tears of rage'/'DST', 'tears of rage').
card_uid('tears of rage'/'DST', 'DST:Tears of Rage:tears of rage').
card_rarity('tears of rage'/'DST', 'Uncommon').
card_artist('tears of rage'/'DST', 'Pete Venters').
card_number('tears of rage'/'DST', '70').
card_multiverse_id('tears of rage'/'DST', '21293').

card_in_set('tel-jilad outrider', 'DST').
card_original_type('tel-jilad outrider'/'DST', 'Creature — Elf Warrior').
card_original_text('tel-jilad outrider'/'DST', 'Protection from artifacts').
card_first_print('tel-jilad outrider', 'DST').
card_image_name('tel-jilad outrider'/'DST', 'tel-jilad outrider').
card_uid('tel-jilad outrider'/'DST', 'DST:Tel-Jilad Outrider:tel-jilad outrider').
card_rarity('tel-jilad outrider'/'DST', 'Common').
card_artist('tel-jilad outrider'/'DST', 'Paolo Parente').
card_number('tel-jilad outrider'/'DST', '87').
card_flavor_text('tel-jilad outrider'/'DST', '\"When the levelers come again, we shall be ready for them.\"').
card_multiverse_id('tel-jilad outrider'/'DST', '46002').

card_in_set('tel-jilad wolf', 'DST').
card_original_type('tel-jilad wolf'/'DST', 'Creature — Wolf').
card_original_text('tel-jilad wolf'/'DST', 'Whenever Tel-Jilad Wolf becomes blocked by an artifact creature, Tel-Jilad Wolf gets +3/+3 until end of turn.').
card_first_print('tel-jilad wolf', 'DST').
card_image_name('tel-jilad wolf'/'DST', 'tel-jilad wolf').
card_uid('tel-jilad wolf'/'DST', 'DST:Tel-Jilad Wolf:tel-jilad wolf').
card_rarity('tel-jilad wolf'/'DST', 'Common').
card_artist('tel-jilad wolf'/'DST', 'Paolo Parente').
card_number('tel-jilad wolf'/'DST', '88').
card_flavor_text('tel-jilad wolf'/'DST', 'Thanks to the wolves, Tel-Jilad is the one area of Mirrodin the myr don\'t see often.').
card_multiverse_id('tel-jilad wolf'/'DST', '46008').

card_in_set('test of faith', 'DST').
card_original_type('test of faith'/'DST', 'Instant').
card_original_text('test of faith'/'DST', 'Prevent the next 3 damage that would be dealt to target creature this turn, and put a +1/+1 counter on that creature for each 1 damage prevented this way.').
card_first_print('test of faith', 'DST').
card_image_name('test of faith'/'DST', 'test of faith').
card_uid('test of faith'/'DST', 'DST:Test of Faith:test of faith').
card_rarity('test of faith'/'DST', 'Uncommon').
card_artist('test of faith'/'DST', 'Vance Kovacs').
card_number('test of faith'/'DST', '17').
card_flavor_text('test of faith'/'DST', 'Those who survive the test bear a mark of power anyone can recognize.').
card_multiverse_id('test of faith'/'DST', '43587').

card_in_set('thought dissector', 'DST').
card_original_type('thought dissector'/'DST', 'Artifact').
card_original_text('thought dissector'/'DST', '{X}, {T}: Target opponent reveals cards from the top of his or her library until an artifact card or X cards are revealed, whichever comes first. If an artifact card is revealed this way, put it into play under your control and sacrifice Thought Dissector. Put the rest of the revealed cards into that player\'s graveyard.').
card_first_print('thought dissector', 'DST').
card_image_name('thought dissector'/'DST', 'thought dissector').
card_uid('thought dissector'/'DST', 'DST:Thought Dissector:thought dissector').
card_rarity('thought dissector'/'DST', 'Rare').
card_artist('thought dissector'/'DST', 'Matt Cavotta').
card_number('thought dissector'/'DST', '152').
card_multiverse_id('thought dissector'/'DST', '50521').

card_in_set('thunderstaff', 'DST').
card_original_type('thunderstaff'/'DST', 'Artifact').
card_original_text('thunderstaff'/'DST', 'If Thunderstaff is untapped and a creature would deal combat damage to you, prevent 1 of that damage.\n{2}, {T}: Attacking creatures get +1/+0 until end of turn.').
card_first_print('thunderstaff', 'DST').
card_image_name('thunderstaff'/'DST', 'thunderstaff').
card_uid('thunderstaff'/'DST', 'DST:Thunderstaff:thunderstaff').
card_rarity('thunderstaff'/'DST', 'Uncommon').
card_artist('thunderstaff'/'DST', 'Kaja Foglio').
card_number('thunderstaff'/'DST', '153').
card_multiverse_id('thunderstaff'/'DST', '50530').

card_in_set('trinisphere', 'DST').
card_original_type('trinisphere'/'DST', 'Artifact').
card_original_text('trinisphere'/'DST', 'As long as Trinisphere is untapped, each spell that would cost less than three mana to play costs three mana to play. (Additional mana in the cost may be paid with any color of mana or colorless mana. For example, a spell that would cost {1}{B} to play costs {2}{B} to play instead.)').
card_first_print('trinisphere', 'DST').
card_image_name('trinisphere'/'DST', 'trinisphere').
card_uid('trinisphere'/'DST', 'DST:Trinisphere:trinisphere').
card_rarity('trinisphere'/'DST', 'Rare').
card_artist('trinisphere'/'DST', 'Tim Hildebrandt').
card_number('trinisphere'/'DST', '154').
card_multiverse_id('trinisphere'/'DST', '43545').

card_in_set('turn the tables', 'DST').
card_original_type('turn the tables'/'DST', 'Instant').
card_original_text('turn the tables'/'DST', 'All combat damage that would be dealt to you this turn is dealt to target attacking creature instead.').
card_first_print('turn the tables', 'DST').
card_image_name('turn the tables'/'DST', 'turn the tables').
card_uid('turn the tables'/'DST', 'DST:Turn the Tables:turn the tables').
card_rarity('turn the tables'/'DST', 'Rare').
card_artist('turn the tables'/'DST', 'Christopher Moeller').
card_number('turn the tables'/'DST', '18').
card_flavor_text('turn the tables'/'DST', '\"Your arrogance is my best weapon.\"').
card_multiverse_id('turn the tables'/'DST', '49033').

card_in_set('unforge', 'DST').
card_original_type('unforge'/'DST', 'Instant').
card_original_text('unforge'/'DST', 'Destroy target Equipment. If that Equipment was attached to a creature, Unforge deals 2 damage to that creature.').
card_first_print('unforge', 'DST').
card_image_name('unforge'/'DST', 'unforge').
card_uid('unforge'/'DST', 'DST:Unforge:unforge').
card_rarity('unforge'/'DST', 'Common').
card_artist('unforge'/'DST', 'Kev Walker').
card_number('unforge'/'DST', '71').
card_flavor_text('unforge'/'DST', 'Compared to the fire of the forge, the strength of steel is nothing.').
card_multiverse_id('unforge'/'DST', '51616').

card_in_set('ur-golem\'s eye', 'DST').
card_original_type('ur-golem\'s eye'/'DST', 'Artifact').
card_original_text('ur-golem\'s eye'/'DST', '{T}: Add {2} to your mana pool.').
card_first_print('ur-golem\'s eye', 'DST').
card_image_name('ur-golem\'s eye'/'DST', 'ur-golem\'s eye').
card_uid('ur-golem\'s eye'/'DST', 'DST:Ur-Golem\'s Eye:ur-golem\'s eye').
card_rarity('ur-golem\'s eye'/'DST', 'Common').
card_artist('ur-golem\'s eye'/'DST', 'Heather Hudson').
card_number('ur-golem\'s eye'/'DST', '155').
card_flavor_text('ur-golem\'s eye'/'DST', 'It still stares accusingly at Memnarch, though millennia have passed since he separated the eye from its body.').
card_multiverse_id('ur-golem\'s eye'/'DST', '49110').

card_in_set('vedalken engineer', 'DST').
card_original_type('vedalken engineer'/'DST', 'Creature — Vedalken Artificer').
card_original_text('vedalken engineer'/'DST', '{T}: Add two mana of any one color to your mana pool. Spend this mana only to play artifact spells or activated abilities of artifacts.').
card_first_print('vedalken engineer', 'DST').
card_image_name('vedalken engineer'/'DST', 'vedalken engineer').
card_uid('vedalken engineer'/'DST', 'DST:Vedalken Engineer:vedalken engineer').
card_rarity('vedalken engineer'/'DST', 'Common').
card_artist('vedalken engineer'/'DST', 'Lars Grant-West').
card_number('vedalken engineer'/'DST', '35').
card_flavor_text('vedalken engineer'/'DST', 'Art is unknown to the vedalken—for them, all creations must serve a purpose.').
card_multiverse_id('vedalken engineer'/'DST', '48148').

card_in_set('vex', 'DST').
card_original_type('vex'/'DST', 'Instant').
card_original_text('vex'/'DST', 'Counter target spell. That spell\'s controller may draw a card.').
card_first_print('vex', 'DST').
card_image_name('vex'/'DST', 'vex').
card_uid('vex'/'DST', 'DST:Vex:vex').
card_rarity('vex'/'DST', 'Common').
card_artist('vex'/'DST', 'Brian Snõddy').
card_number('vex'/'DST', '36').
card_flavor_text('vex'/'DST', '\"The druids would be better off if they spent more time in the lab and less time watching trees grow.\"\n—Pontifex, elder researcher').
card_multiverse_id('vex'/'DST', '49085').

card_in_set('viridian acolyte', 'DST').
card_original_type('viridian acolyte'/'DST', 'Creature — Elf Shaman').
card_original_text('viridian acolyte'/'DST', '{1}, {T}: Add one mana of any color to your mana pool.').
card_first_print('viridian acolyte', 'DST').
card_image_name('viridian acolyte'/'DST', 'viridian acolyte').
card_uid('viridian acolyte'/'DST', 'DST:Viridian Acolyte:viridian acolyte').
card_rarity('viridian acolyte'/'DST', 'Common').
card_artist('viridian acolyte'/'DST', 'D. Alexander Gregory').
card_number('viridian acolyte'/'DST', '89').
card_flavor_text('viridian acolyte'/'DST', '\"We have no sun to guide our dead, so I tend the prismatic flames of the spirit fires that show the souls their way. When I die, I hope there is one left to guide my spirit home.\"').
card_multiverse_id('viridian acolyte'/'DST', '15190').

card_in_set('viridian zealot', 'DST').
card_original_type('viridian zealot'/'DST', 'Creature — Elf Warrior').
card_original_text('viridian zealot'/'DST', '{1}{G}, Sacrifice Viridian Zealot: Destroy target artifact or enchantment.').
card_first_print('viridian zealot', 'DST').
card_image_name('viridian zealot'/'DST', 'viridian zealot').
card_uid('viridian zealot'/'DST', 'DST:Viridian Zealot:viridian zealot').
card_rarity('viridian zealot'/'DST', 'Rare').
card_artist('viridian zealot'/'DST', 'Kev Walker').
card_number('viridian zealot'/'DST', '90').
card_flavor_text('viridian zealot'/'DST', '\"I will fight only the way nature intended—and nature intended us to win.\"').
card_multiverse_id('viridian zealot'/'DST', '46547').

card_in_set('voltaic construct', 'DST').
card_original_type('voltaic construct'/'DST', 'Artifact Creature — Golem').
card_original_text('voltaic construct'/'DST', '{2}: Untap target artifact creature.').
card_first_print('voltaic construct', 'DST').
card_image_name('voltaic construct'/'DST', 'voltaic construct').
card_uid('voltaic construct'/'DST', 'DST:Voltaic Construct:voltaic construct').
card_rarity('voltaic construct'/'DST', 'Uncommon').
card_artist('voltaic construct'/'DST', 'Jeff Easley').
card_number('voltaic construct'/'DST', '156').
card_flavor_text('voltaic construct'/'DST', 'The undead may outlast the living, but those who have never lived outlast them both.').
card_multiverse_id('voltaic construct'/'DST', '48141').

card_in_set('vulshok morningstar', 'DST').
card_original_type('vulshok morningstar'/'DST', 'Artifact — Equipment').
card_original_text('vulshok morningstar'/'DST', 'Equipped creature gets +2/+2.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery. This card comes into play unattached and stays in play if the creature leaves play.)').
card_first_print('vulshok morningstar', 'DST').
card_image_name('vulshok morningstar'/'DST', 'vulshok morningstar').
card_uid('vulshok morningstar'/'DST', 'DST:Vulshok Morningstar:vulshok morningstar').
card_rarity('vulshok morningstar'/'DST', 'Common').
card_artist('vulshok morningstar'/'DST', 'David Martin').
card_number('vulshok morningstar'/'DST', '157').
card_multiverse_id('vulshok morningstar'/'DST', '49758').

card_in_set('vulshok war boar', 'DST').
card_original_type('vulshok war boar'/'DST', 'Creature — Beast').
card_original_text('vulshok war boar'/'DST', 'When Vulshok War Boar comes into play, sacrifice it unless you sacrifice an artifact.').
card_first_print('vulshok war boar', 'DST').
card_image_name('vulshok war boar'/'DST', 'vulshok war boar').
card_uid('vulshok war boar'/'DST', 'DST:Vulshok War Boar:vulshok war boar').
card_rarity('vulshok war boar'/'DST', 'Uncommon').
card_artist('vulshok war boar'/'DST', 'Pete Venters').
card_number('vulshok war boar'/'DST', '72').
card_flavor_text('vulshok war boar'/'DST', 'It must eat its weight in metal daily, or its furnacelike belly will consume itself for lack of fuel.').
card_multiverse_id('vulshok war boar'/'DST', '45455').

card_in_set('wand of the elements', 'DST').
card_original_type('wand of the elements'/'DST', 'Artifact').
card_original_text('wand of the elements'/'DST', '{T}, Sacrifice an Island: Put a 2/2 blue Elemental creature token with flying into play.\n{T}, Sacrifice a Mountain: Put a 3/3 red Elemental creature token into play.').
card_first_print('wand of the elements', 'DST').
card_image_name('wand of the elements'/'DST', 'wand of the elements').
card_uid('wand of the elements'/'DST', 'DST:Wand of the Elements:wand of the elements').
card_rarity('wand of the elements'/'DST', 'Rare').
card_artist('wand of the elements'/'DST', 'Thomas M. Baxa').
card_number('wand of the elements'/'DST', '158').
card_flavor_text('wand of the elements'/'DST', 'It gives legs to the earth and wings to the sky.').
card_multiverse_id('wand of the elements'/'DST', '49775').

card_in_set('well of lost dreams', 'DST').
card_original_type('well of lost dreams'/'DST', 'Artifact').
card_original_text('well of lost dreams'/'DST', 'Whenever you gain life, you may pay {X}, where X is less than or equal to the amount of life you gained. If you do, draw X cards.').
card_first_print('well of lost dreams', 'DST').
card_image_name('well of lost dreams'/'DST', 'well of lost dreams').
card_uid('well of lost dreams'/'DST', 'DST:Well of Lost Dreams:well of lost dreams').
card_rarity('well of lost dreams'/'DST', 'Rare').
card_artist('well of lost dreams'/'DST', 'Jeff Miracola').
card_number('well of lost dreams'/'DST', '159').
card_flavor_text('well of lost dreams'/'DST', 'Some say the knowledge lost during the Ritual of Rebuking is returned through the well\'s waters.').
card_multiverse_id('well of lost dreams'/'DST', '48993').

card_in_set('whispersilk cloak', 'DST').
card_original_type('whispersilk cloak'/'DST', 'Artifact — Equipment').
card_original_text('whispersilk cloak'/'DST', 'Equipped creature is unblockable and can\'t be the target of spells or abilities.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery. This card comes into play unattached and stays in play if the creature leaves play.)').
card_first_print('whispersilk cloak', 'DST').
card_image_name('whispersilk cloak'/'DST', 'whispersilk cloak').
card_uid('whispersilk cloak'/'DST', 'DST:Whispersilk Cloak:whispersilk cloak').
card_rarity('whispersilk cloak'/'DST', 'Common').
card_artist('whispersilk cloak'/'DST', 'Luca Zontini').
card_number('whispersilk cloak'/'DST', '160').
card_multiverse_id('whispersilk cloak'/'DST', '46030').

card_in_set('wirefly hive', 'DST').
card_original_type('wirefly hive'/'DST', 'Artifact').
card_original_text('wirefly hive'/'DST', '{3}, {T}: Flip a coin. If you win the flip, put a 2/2 Wirefly artifact creature token with flying into play. If you lose the flip, destroy all Wireflies.').
card_first_print('wirefly hive', 'DST').
card_image_name('wirefly hive'/'DST', 'wirefly hive').
card_uid('wirefly hive'/'DST', 'DST:Wirefly Hive:wirefly hive').
card_rarity('wirefly hive'/'DST', 'Uncommon').
card_artist('wirefly hive'/'DST', 'Nottsuo').
card_number('wirefly hive'/'DST', '161').
card_flavor_text('wirefly hive'/'DST', '\"The lifespan of a wirefly can be precisely described as ‘short.\'\"\n—Bruenna, Neurok leader').
card_multiverse_id('wirefly hive'/'DST', '49026').

card_in_set('wurm\'s tooth', 'DST').
card_original_type('wurm\'s tooth'/'DST', 'Artifact').
card_original_text('wurm\'s tooth'/'DST', 'Whenever a player plays a green spell, you may gain 1 life.').
card_first_print('wurm\'s tooth', 'DST').
card_image_name('wurm\'s tooth'/'DST', 'wurm\'s tooth').
card_uid('wurm\'s tooth'/'DST', 'DST:Wurm\'s Tooth:wurm\'s tooth').
card_rarity('wurm\'s tooth'/'DST', 'Uncommon').
card_artist('wurm\'s tooth'/'DST', 'Alan Pollack').
card_number('wurm\'s tooth'/'DST', '162').
card_flavor_text('wurm\'s tooth'/'DST', 'A wurm knows nothing of deception. If it opens its mouth, it plans to eat you.').
card_multiverse_id('wurm\'s tooth'/'DST', '72684').
