% Visions

set('VIS').
set_name('VIS', 'Visions').
set_release_date('VIS', '1997-02-03').
set_border('VIS', 'black').
set_type('VIS', 'expansion').
set_block('VIS', 'Mirage').

card_in_set('aku djinn', 'VIS').
card_original_type('aku djinn'/'VIS', 'Summon — Djinn').
card_original_text('aku djinn'/'VIS', 'Trample\nDuring your upkeep, each opponent puts a +1/+1 counter on each creature he or she controls.').
card_first_print('aku djinn', 'VIS').
card_image_name('aku djinn'/'VIS', 'aku djinn').
card_uid('aku djinn'/'VIS', 'VIS:Aku Djinn:aku djinn').
card_rarity('aku djinn'/'VIS', 'Rare').
card_artist('aku djinn'/'VIS', 'Terese Nielsen').
card_flavor_text('aku djinn'/'VIS', '\"Your arrogance amazes me, Kaervek. Did you not know the price you would pay?\"\n—Mangara').
card_multiverse_id('aku djinn'/'VIS', '3608').

card_in_set('anvil of bogardan', 'VIS').
card_original_type('anvil of bogardan'/'VIS', 'Artifact').
card_original_text('anvil of bogardan'/'VIS', 'Each player skips his or her discard phase.\nDuring each player\'s draw phase, that player draws an additional card and then chooses and discards a card.').
card_first_print('anvil of bogardan', 'VIS').
card_image_name('anvil of bogardan'/'VIS', 'anvil of bogardan').
card_uid('anvil of bogardan'/'VIS', 'VIS:Anvil of Bogardan:anvil of bogardan').
card_rarity('anvil of bogardan'/'VIS', 'Rare').
card_artist('anvil of bogardan'/'VIS', 'Roger Raupp').
card_multiverse_id('anvil of bogardan'/'VIS', '3589').

card_in_set('archangel', 'VIS').
card_original_type('archangel'/'VIS', 'Summon — Angel').
card_original_text('archangel'/'VIS', 'Flying\nAttacking does not cause Archangel to tap.').
card_first_print('archangel', 'VIS').
card_image_name('archangel'/'VIS', 'archangel').
card_uid('archangel'/'VIS', 'VIS:Archangel:archangel').
card_rarity('archangel'/'VIS', 'Rare').
card_artist('archangel'/'VIS', 'Christopher Rush').
card_flavor_text('archangel'/'VIS', '\"My mother once told me angels sing their swords\' names with each strike.\"\n—Sisay, Captain of the Weatherlight').
card_multiverse_id('archangel'/'VIS', '3708').

card_in_set('army ants', 'VIS').
card_original_type('army ants'/'VIS', 'Summon — Insects').
card_original_text('army ants'/'VIS', '{T}, Sacrifice a land: Destroy target land.').
card_first_print('army ants', 'VIS').
card_image_name('army ants'/'VIS', 'army ants').
card_uid('army ants'/'VIS', 'VIS:Army Ants:army ants').
card_rarity('army ants'/'VIS', 'Uncommon').
card_artist('army ants'/'VIS', 'G. Darrow & I. Rabarot').
card_flavor_text('army ants'/'VIS', '\"Do not underestimate the power of many against one.\"\n—Sidar Jabari').
card_multiverse_id('army ants'/'VIS', '3733').

card_in_set('betrayal', 'VIS').
card_original_type('betrayal'/'VIS', 'Enchant Creature').
card_original_text('betrayal'/'VIS', 'Play only on a creature an opponent controls.\nIf enchanted creature becomes tapped, draw a card.').
card_first_print('betrayal', 'VIS').
card_image_name('betrayal'/'VIS', 'betrayal').
card_uid('betrayal'/'VIS', 'VIS:Betrayal:betrayal').
card_rarity('betrayal'/'VIS', 'Common').
card_artist('betrayal'/'VIS', 'Gary Leach').
card_flavor_text('betrayal'/'VIS', '\"Sometimes a burden can be borne only on the shoulders of a willing enemy.\"\n—Suq\'Ata aphorism').
card_multiverse_id('betrayal'/'VIS', '3633').

card_in_set('blanket of night', 'VIS').
card_original_type('blanket of night'/'VIS', 'Enchantment').
card_original_text('blanket of night'/'VIS', 'Each mana-producing land is a swamp in addition to its normal land type.').
card_first_print('blanket of night', 'VIS').
card_image_name('blanket of night'/'VIS', 'blanket of night').
card_uid('blanket of night'/'VIS', 'VIS:Blanket of Night:blanket of night').
card_rarity('blanket of night'/'VIS', 'Uncommon').
card_artist('blanket of night'/'VIS', 'Cliff Nielsen').
card_flavor_text('blanket of night'/'VIS', '\"Kaervek imprisoned Mangara out of jealousy, but he taints Jamuraa simply out of spite.\"\n—Asmira, Holy Avenger').
card_multiverse_id('blanket of night'/'VIS', '3609').

card_in_set('bogardan phoenix', 'VIS').
card_original_type('bogardan phoenix'/'VIS', 'Summon — Phoenix').
card_original_text('bogardan phoenix'/'VIS', 'Flying\nIf Bogardan Phoenix is put into any graveyard from play and has no death counter on it, return Bogardan Phoenix to play and put a death counter on it.\nIf Bogardan Phoenix is put into any graveyard from play and has a death counter on it, remove it from the game.').
card_first_print('bogardan phoenix', 'VIS').
card_image_name('bogardan phoenix'/'VIS', 'bogardan phoenix').
card_uid('bogardan phoenix'/'VIS', 'VIS:Bogardan Phoenix:bogardan phoenix').
card_rarity('bogardan phoenix'/'VIS', 'Rare').
card_artist('bogardan phoenix'/'VIS', 'David O\'Connor').
card_multiverse_id('bogardan phoenix'/'VIS', '3683').

card_in_set('brass-talon chimera', 'VIS').
card_original_type('brass-talon chimera'/'VIS', 'Artifact Creature').
card_original_text('brass-talon chimera'/'VIS', 'First strike\nBrass-Talon Chimera counts as a Chimera.\nSacrifice Brass-Talon Chimera: Put a +2/+2 counter on target Chimera and that Chimera gains first strike permanently.').
card_first_print('brass-talon chimera', 'VIS').
card_image_name('brass-talon chimera'/'VIS', 'brass-talon chimera').
card_uid('brass-talon chimera'/'VIS', 'VIS:Brass-Talon Chimera:brass-talon chimera').
card_rarity('brass-talon chimera'/'VIS', 'Uncommon').
card_artist('brass-talon chimera'/'VIS', 'Mike Dringenberg').
card_multiverse_id('brass-talon chimera'/'VIS', '3590').

card_in_set('breathstealer\'s crypt', 'VIS').
card_original_type('breathstealer\'s crypt'/'VIS', 'Enchantment').
card_original_text('breathstealer\'s crypt'/'VIS', 'Whenever any player draws a card, he or she reveals that card. If the card is a creature card, that player pays 3 life or discards the card.').
card_first_print('breathstealer\'s crypt', 'VIS').
card_image_name('breathstealer\'s crypt'/'VIS', 'breathstealer\'s crypt').
card_uid('breathstealer\'s crypt'/'VIS', 'VIS:Breathstealer\'s Crypt:breathstealer\'s crypt').
card_rarity('breathstealer\'s crypt'/'VIS', 'Rare').
card_artist('breathstealer\'s crypt'/'VIS', 'Blackie del Rio').
card_flavor_text('breathstealer\'s crypt'/'VIS', '\"Far too many soldiers have died in their beds.\"\n—Telim\'Tor').
card_multiverse_id('breathstealer\'s crypt'/'VIS', '3734').

card_in_set('breezekeeper', 'VIS').
card_original_type('breezekeeper'/'VIS', 'Summon — Djinn').
card_original_text('breezekeeper'/'VIS', 'Flying, phasing').
card_first_print('breezekeeper', 'VIS').
card_image_name('breezekeeper'/'VIS', 'breezekeeper').
card_uid('breezekeeper'/'VIS', 'VIS:Breezekeeper:breezekeeper').
card_rarity('breezekeeper'/'VIS', 'Common').
card_artist('breezekeeper'/'VIS', 'Adam Rex').
card_flavor_text('breezekeeper'/'VIS', '\"He blows a gust across the cliff to vanish in a breath of wind.\"\n—\"Song of the Wind Being\"').
card_multiverse_id('breezekeeper'/'VIS', '3634').

card_in_set('brood of cockroaches', 'VIS').
card_original_type('brood of cockroaches'/'VIS', 'Summon — Insects').
card_original_text('brood of cockroaches'/'VIS', 'If Brood of Cockroaches is put into your graveyard from play, pay 1 life and return Brood of Cockroaches to your hand at end of turn.').
card_first_print('brood of cockroaches', 'VIS').
card_image_name('brood of cockroaches'/'VIS', 'brood of cockroaches').
card_uid('brood of cockroaches'/'VIS', 'VIS:Brood of Cockroaches:brood of cockroaches').
card_rarity('brood of cockroaches'/'VIS', 'Uncommon').
card_artist('brood of cockroaches'/'VIS', 'G. Darrow & I. Rabarot').
card_flavor_text('brood of cockroaches'/'VIS', '\"It\'s like waking on a bed of a thousand olives during an earthquake of subtle force.\"\n—Afari, Tales').
card_multiverse_id('brood of cockroaches'/'VIS', '3610').

card_in_set('bull elephant', 'VIS').
card_original_type('bull elephant'/'VIS', 'Summon — Elephant').
card_original_text('bull elephant'/'VIS', 'When Bull Elephant comes into play, return two forests you control to owner\'s hand or bury Bull Elephant.').
card_image_name('bull elephant'/'VIS', 'bull elephant').
card_uid('bull elephant'/'VIS', 'VIS:Bull Elephant:bull elephant').
card_rarity('bull elephant'/'VIS', 'Common').
card_artist('bull elephant'/'VIS', 'Steve White').
card_flavor_text('bull elephant'/'VIS', 'Four gray trees and a long, coiling snake. What am I?\n—Zhalfirin riddle').
card_multiverse_id('bull elephant'/'VIS', '3658').

card_in_set('chronatog', 'VIS').
card_original_type('chronatog'/'VIS', 'Summon — Atog').
card_original_text('chronatog'/'VIS', 'Skip your next turn: Chronatog gets +3/+3 until end of turn. Use this ability only once each turn.').
card_first_print('chronatog', 'VIS').
card_image_name('chronatog'/'VIS', 'chronatog').
card_uid('chronatog'/'VIS', 'VIS:Chronatog:chronatog').
card_rarity('chronatog'/'VIS', 'Rare').
card_artist('chronatog'/'VIS', 'Christopher Rush').
card_flavor_text('chronatog'/'VIS', 'For the chronatog, there is no meal like the present.').
card_multiverse_id('chronatog'/'VIS', '3635').

card_in_set('city of solitude', 'VIS').
card_original_type('city of solitude'/'VIS', 'Enchantment').
card_original_text('city of solitude'/'VIS', 'Each player may play spells and abilities only during his or her turn.').
card_first_print('city of solitude', 'VIS').
card_image_name('city of solitude'/'VIS', 'city of solitude').
card_uid('city of solitude'/'VIS', 'VIS:City of Solitude:city of solitude').
card_rarity('city of solitude'/'VIS', 'Rare').
card_artist('city of solitude'/'VIS', 'Romas Kukalis').
card_flavor_text('city of solitude'/'VIS', '\"My horizon was the arcing petals, my new home resplendent with spires of weeds and pillows of fragrant pollen.\"\n—Naimah, Femeref philosopher').
card_multiverse_id('city of solitude'/'VIS', '3659').

card_in_set('cloud elemental', 'VIS').
card_original_type('cloud elemental'/'VIS', 'Summon — Elemental').
card_original_text('cloud elemental'/'VIS', 'Flying\nCloud Elemental can block only creatures with flying.').
card_first_print('cloud elemental', 'VIS').
card_image_name('cloud elemental'/'VIS', 'cloud elemental').
card_uid('cloud elemental'/'VIS', 'VIS:Cloud Elemental:cloud elemental').
card_rarity('cloud elemental'/'VIS', 'Common').
card_artist('cloud elemental'/'VIS', 'Adam Rex').
card_flavor_text('cloud elemental'/'VIS', '\"You know you\'ve angered the gods when the clouds turn against you.\"\n—Sisay, Captain of the Weatherlight').
card_multiverse_id('cloud elemental'/'VIS', '3636').

card_in_set('coercion', 'VIS').
card_original_type('coercion'/'VIS', 'Sorcery').
card_original_text('coercion'/'VIS', 'Look at target opponent\'s hand. Choose a card from that player\'s hand. That player discards that card.').
card_first_print('coercion', 'VIS').
card_image_name('coercion'/'VIS', 'coercion').
card_uid('coercion'/'VIS', 'VIS:Coercion:coercion').
card_rarity('coercion'/'VIS', 'Common').
card_artist('coercion'/'VIS', 'DiTerlizzi').
card_flavor_text('coercion'/'VIS', 'A rhino\'s bargain\n—Femeref expression meaning \"a situation with no choices\"').
card_multiverse_id('coercion'/'VIS', '3611').

card_in_set('coral atoll', 'VIS').
card_original_type('coral atoll'/'VIS', 'Land').
card_original_text('coral atoll'/'VIS', 'Coral Atoll comes into play tapped. \nWhen Coral Atoll comes into play, return an untapped island you control to owner\'s hand or bury Coral Atoll.\n{T}: Add {U} and one colorless mana to your mana pool.').
card_first_print('coral atoll', 'VIS').
card_image_name('coral atoll'/'VIS', 'coral atoll').
card_uid('coral atoll'/'VIS', 'VIS:Coral Atoll:coral atoll').
card_rarity('coral atoll'/'VIS', 'Uncommon').
card_artist('coral atoll'/'VIS', 'John Avon').
card_multiverse_id('coral atoll'/'VIS', '3748').

card_in_set('corrosion', 'VIS').
card_original_type('corrosion'/'VIS', 'Enchantment').
card_original_text('corrosion'/'VIS', 'Cumulative upkeep {1}\nDuring your upkeep, put a rust counter on each artifact target opponent controls. If the number of rust counters on an artifact equals or exceeds that artifact\'s casting cost, bury the artifact.\nIf Corrosion leaves play, remove all rust counters from the game.').
card_first_print('corrosion', 'VIS').
card_image_name('corrosion'/'VIS', 'corrosion').
card_uid('corrosion'/'VIS', 'VIS:Corrosion:corrosion').
card_rarity('corrosion'/'VIS', 'Rare').
card_artist('corrosion'/'VIS', 'Michael Danza').
card_multiverse_id('corrosion'/'VIS', '3735').

card_in_set('creeping mold', 'VIS').
card_original_type('creeping mold'/'VIS', 'Sorcery').
card_original_text('creeping mold'/'VIS', 'Destroy target artifact, land, or enchantment.').
card_image_name('creeping mold'/'VIS', 'creeping mold').
card_uid('creeping mold'/'VIS', 'VIS:Creeping Mold:creeping mold').
card_rarity('creeping mold'/'VIS', 'Uncommon').
card_artist('creeping mold'/'VIS', 'Dave Seeley').
card_flavor_text('creeping mold'/'VIS', '\"Mold could catch you.\"\n—Suq\'Ata insult').
card_multiverse_id('creeping mold'/'VIS', '3660').

card_in_set('crypt rats', 'VIS').
card_original_type('crypt rats'/'VIS', 'Summon — Rats').
card_original_text('crypt rats'/'VIS', '{X} Crypt Rats deals X damage to each creature and player. Spend only black mana in this way.').
card_first_print('crypt rats', 'VIS').
card_image_name('crypt rats'/'VIS', 'crypt rats').
card_uid('crypt rats'/'VIS', 'VIS:Crypt Rats:crypt rats').
card_rarity('crypt rats'/'VIS', 'Common').
card_artist('crypt rats'/'VIS', 'Paul Lee').
card_flavor_text('crypt rats'/'VIS', '\"Once I dreamt of death, but now it dreams of me / And only rats and rotting flesh can hear my silent plea.\"\n—Mundungu chant').
card_multiverse_id('crypt rats'/'VIS', '3612').

card_in_set('daraja griffin', 'VIS').
card_original_type('daraja griffin'/'VIS', 'Summon — Griffin').
card_original_text('daraja griffin'/'VIS', 'Flying\nSacrifice Daraja Griffin: Destroy target black creature.').
card_first_print('daraja griffin', 'VIS').
card_image_name('daraja griffin'/'VIS', 'daraja griffin').
card_uid('daraja griffin'/'VIS', 'VIS:Daraja Griffin:daraja griffin').
card_rarity('daraja griffin'/'VIS', 'Uncommon').
card_artist('daraja griffin'/'VIS', 'Stuart Griffin').
card_flavor_text('daraja griffin'/'VIS', '\"And the flamingos said, ‘Get out of our nest—we can\'t be seen with the likes of you!\' So, the griffin ate them.\"\n—Azeworai, \"The Ugly Bird\"').
card_multiverse_id('daraja griffin'/'VIS', '3709').

card_in_set('dark privilege', 'VIS').
card_original_type('dark privilege'/'VIS', 'Enchant Creature').
card_original_text('dark privilege'/'VIS', 'Enchanted creature gets +1/+1.\nSacrifice a creature: Regenerate enchanted creature.').
card_image_name('dark privilege'/'VIS', 'dark privilege').
card_uid('dark privilege'/'VIS', 'VIS:Dark Privilege:dark privilege').
card_rarity('dark privilege'/'VIS', 'Common').
card_artist('dark privilege'/'VIS', 'Tom Kyffin').
card_flavor_text('dark privilege'/'VIS', '\"As you breathe your last, understand why I accept such forbidden gifts.\"\n—Purraj of Urborg').
card_multiverse_id('dark privilege'/'VIS', '3613').

card_in_set('death watch', 'VIS').
card_original_type('death watch'/'VIS', 'Enchant Creature').
card_original_text('death watch'/'VIS', 'If enchanted creature is put into any graveyard, that creature\'s controller loses an amount of life equal to its power and you gain an amount of life equal to its toughness.').
card_first_print('death watch', 'VIS').
card_image_name('death watch'/'VIS', 'death watch').
card_uid('death watch'/'VIS', 'VIS:Death Watch:death watch').
card_rarity('death watch'/'VIS', 'Common').
card_artist('death watch'/'VIS', 'Brian Horton').
card_flavor_text('death watch'/'VIS', '\"Watch death wait / Wait, death, watch / Steal breath swiftly / but steal life slowly.\"\n—Suq\'Ata dirge').
card_multiverse_id('death watch'/'VIS', '3614').

card_in_set('desertion', 'VIS').
card_original_type('desertion'/'VIS', 'Interrupt').
card_original_text('desertion'/'VIS', 'Counter target spell. If that spell is an artifact or summon spell, put that card into play under your control as though it were just played.').
card_first_print('desertion', 'VIS').
card_image_name('desertion'/'VIS', 'desertion').
card_uid('desertion'/'VIS', 'VIS:Desertion:desertion').
card_rarity('desertion'/'VIS', 'Rare').
card_artist('desertion'/'VIS', 'Richard Kane Ferguson').
card_flavor_text('desertion'/'VIS', 'First the insult, then the injury.').
card_multiverse_id('desertion'/'VIS', '3637').

card_in_set('desolation', 'VIS').
card_original_type('desolation'/'VIS', 'Enchantment').
card_original_text('desolation'/'VIS', 'At the end of each turn, each player who tapped a land for mana during that turn sacrifices a land. If a plains is sacrificed in this way, Desolation deals 2 damage to that plains\' controller.').
card_first_print('desolation', 'VIS').
card_image_name('desolation'/'VIS', 'desolation').
card_uid('desolation'/'VIS', 'VIS:Desolation:desolation').
card_rarity('desolation'/'VIS', 'Uncommon').
card_artist('desolation'/'VIS', 'George Pratt').
card_flavor_text('desolation'/'VIS', '\"Kill a creature, destroy the present. Kill the land, destroy the future.\"\n—Kaervek').
card_multiverse_id('desolation'/'VIS', '3615').

card_in_set('diamond kaleidoscope', 'VIS').
card_original_type('diamond kaleidoscope'/'VIS', 'Artifact').
card_original_text('diamond kaleidoscope'/'VIS', '{3}, {T}: Put a Prism token into play. Treat this token as a 0/1 artifact creature.\nSacrifice a Prism token: Add one mana of any color to your mana pool. Play this ability as a mana source.').
card_first_print('diamond kaleidoscope', 'VIS').
card_image_name('diamond kaleidoscope'/'VIS', 'diamond kaleidoscope').
card_uid('diamond kaleidoscope'/'VIS', 'VIS:Diamond Kaleidoscope:diamond kaleidoscope').
card_rarity('diamond kaleidoscope'/'VIS', 'Rare').
card_artist('diamond kaleidoscope'/'VIS', 'Ron Spencer').
card_flavor_text('diamond kaleidoscope'/'VIS', '\"A pretty toy. Now show me its tactical applications.\"\n—Telim\'Tor').
card_multiverse_id('diamond kaleidoscope'/'VIS', '3591').

card_in_set('dormant volcano', 'VIS').
card_original_type('dormant volcano'/'VIS', 'Land').
card_original_text('dormant volcano'/'VIS', 'Dormant Volcano comes into play tapped. \nWhen Dormant Volcano comes into play, return an untapped mountain you control to owner\'s hand or bury Dormant Volcano.\n{T}: Add {R} and one colorless mana to your mana pool.').
card_first_print('dormant volcano', 'VIS').
card_image_name('dormant volcano'/'VIS', 'dormant volcano').
card_uid('dormant volcano'/'VIS', 'VIS:Dormant Volcano:dormant volcano').
card_rarity('dormant volcano'/'VIS', 'Uncommon').
card_artist('dormant volcano'/'VIS', 'John Avon').
card_multiverse_id('dormant volcano'/'VIS', '3749').

card_in_set('dragon mask', 'VIS').
card_original_type('dragon mask'/'VIS', 'Artifact').
card_original_text('dragon mask'/'VIS', '{3}, {T}: Target creature you control gets +2/+2 until end of turn. At end of turn, if that creature is in play, return it to owner\'s hand.').
card_first_print('dragon mask', 'VIS').
card_image_name('dragon mask'/'VIS', 'dragon mask').
card_uid('dragon mask'/'VIS', 'VIS:Dragon Mask:dragon mask').
card_rarity('dragon mask'/'VIS', 'Uncommon').
card_artist('dragon mask'/'VIS', 'Craig Hooper').
card_flavor_text('dragon mask'/'VIS', '\"With no further options, I was forced to don the mask.\"\n—Rashida Scalebane').
card_multiverse_id('dragon mask'/'VIS', '3592').

card_in_set('dream tides', 'VIS').
card_original_type('dream tides'/'VIS', 'Enchantment').
card_original_text('dream tides'/'VIS', 'Creatures do not untap during their controllers\' untap phase.\nEach nongreen creature\'s controller may pay an additional {2} during his or her upkeep to untap that creature.').
card_first_print('dream tides', 'VIS').
card_image_name('dream tides'/'VIS', 'dream tides').
card_uid('dream tides'/'VIS', 'VIS:Dream Tides:dream tides').
card_rarity('dream tides'/'VIS', 'Uncommon').
card_artist('dream tides'/'VIS', 'Jerry Tiritilli').
card_multiverse_id('dream tides'/'VIS', '3638').

card_in_set('dwarven vigilantes', 'VIS').
card_original_type('dwarven vigilantes'/'VIS', 'Summon — Dwarves').
card_original_text('dwarven vigilantes'/'VIS', 'If Dwarven Vigilantes attacks and is not blocked, you may choose to have it deal no combat damage this turn. If you do, Dwarven Vigilantes deals an amount of damage equal to its power to target creature.').
card_first_print('dwarven vigilantes', 'VIS').
card_image_name('dwarven vigilantes'/'VIS', 'dwarven vigilantes').
card_uid('dwarven vigilantes'/'VIS', 'VIS:Dwarven Vigilantes:dwarven vigilantes').
card_rarity('dwarven vigilantes'/'VIS', 'Common').
card_artist('dwarven vigilantes'/'VIS', 'Pete Venters').
card_flavor_text('dwarven vigilantes'/'VIS', 'Some dwarves can only be pushed so far.').
card_multiverse_id('dwarven vigilantes'/'VIS', '3684').

card_in_set('elephant grass', 'VIS').
card_original_type('elephant grass'/'VIS', 'Enchantment').
card_original_text('elephant grass'/'VIS', 'Cumulative upkeep {1}\nBlack creatures cannot attack you. Nonblack creatures cannot attack you unless their controller pays an additional {2} for each attacking creature.').
card_first_print('elephant grass', 'VIS').
card_image_name('elephant grass'/'VIS', 'elephant grass').
card_uid('elephant grass'/'VIS', 'VIS:Elephant Grass:elephant grass').
card_rarity('elephant grass'/'VIS', 'Uncommon').
card_artist('elephant grass'/'VIS', 'Tony Roberts').
card_flavor_text('elephant grass'/'VIS', '\"How have I angered nature that she would imprison me in a labyrinth of grass?\"\n—Kasib ibn Naji, Letters').
card_multiverse_id('elephant grass'/'VIS', '3661').

card_in_set('elkin lair', 'VIS').
card_original_type('elkin lair'/'VIS', 'Enchant World').
card_original_text('elkin lair'/'VIS', 'During each player\'s upkeep, that player chooses a card at random from his or her hand and sets it aside face up. The player may play that card as though it were in his or her hand. If the player does not play the card by end of turn, bury that card.').
card_first_print('elkin lair', 'VIS').
card_image_name('elkin lair'/'VIS', 'elkin lair').
card_uid('elkin lair'/'VIS', 'VIS:Elkin Lair:elkin lair').
card_rarity('elkin lair'/'VIS', 'Rare').
card_artist('elkin lair'/'VIS', 'Jerry Tiritilli').
card_multiverse_id('elkin lair'/'VIS', '3685').

card_in_set('elven cache', 'VIS').
card_original_type('elven cache'/'VIS', 'Sorcery').
card_original_text('elven cache'/'VIS', 'Return target card from your graveyard to your hand.').
card_first_print('elven cache', 'VIS').
card_image_name('elven cache'/'VIS', 'elven cache').
card_uid('elven cache'/'VIS', 'VIS:Elven Cache:elven cache').
card_rarity('elven cache'/'VIS', 'Common').
card_artist('elven cache'/'VIS', 'John Matson').
card_flavor_text('elven cache'/'VIS', '\"The elves can hide better than most can look.\"\n—Mwani, Mtenda herder').
card_multiverse_id('elven cache'/'VIS', '3662').

card_in_set('emerald charm', 'VIS').
card_original_type('emerald charm'/'VIS', 'Instant').
card_original_text('emerald charm'/'VIS', 'Choose one Untap target permanent; or destroy target global enchantment; or target creature loses flying until end of turn.').
card_first_print('emerald charm', 'VIS').
card_image_name('emerald charm'/'VIS', 'emerald charm').
card_uid('emerald charm'/'VIS', 'VIS:Emerald Charm:emerald charm').
card_rarity('emerald charm'/'VIS', 'Common').
card_artist('emerald charm'/'VIS', 'Greg Spalenka').
card_multiverse_id('emerald charm'/'VIS', '3663').

card_in_set('equipoise', 'VIS').
card_original_type('equipoise'/'VIS', 'Enchantment').
card_original_text('equipoise'/'VIS', 'During your upkeep, for each land target player controls in excess of the number of lands you control, target land he or she controls phases out. Repeat this process for artifacts and then for creatures.').
card_first_print('equipoise', 'VIS').
card_image_name('equipoise'/'VIS', 'equipoise').
card_uid('equipoise'/'VIS', 'VIS:Equipoise:equipoise').
card_rarity('equipoise'/'VIS', 'Rare').
card_artist('equipoise'/'VIS', 'Adam Rex').
card_multiverse_id('equipoise'/'VIS', '3710').

card_in_set('everglades', 'VIS').
card_original_type('everglades'/'VIS', 'Land').
card_original_text('everglades'/'VIS', 'Everglades comes into play tapped.\nWhen Everglades comes into play, return an untapped swamp you control to owner\'s hand or bury Everglades.\n{T}: Add {B} and one colorless mana to your mana pool.').
card_first_print('everglades', 'VIS').
card_image_name('everglades'/'VIS', 'everglades').
card_uid('everglades'/'VIS', 'VIS:Everglades:everglades').
card_rarity('everglades'/'VIS', 'Uncommon').
card_artist('everglades'/'VIS', 'Bob Eggleton').
card_multiverse_id('everglades'/'VIS', '3750').

card_in_set('eye of singularity', 'VIS').
card_original_type('eye of singularity'/'VIS', 'Enchant World').
card_original_text('eye of singularity'/'VIS', 'When Eye of Singularity comes into play, bury all permanents with the same name except basic lands.\nWhenever any permanent other than a basic land comes into play, bury any permanent already in play with the same name.').
card_first_print('eye of singularity', 'VIS').
card_image_name('eye of singularity'/'VIS', 'eye of singularity').
card_uid('eye of singularity'/'VIS', 'VIS:Eye of Singularity:eye of singularity').
card_rarity('eye of singularity'/'VIS', 'Rare').
card_artist('eye of singularity'/'VIS', 'Eric Peterson').
card_multiverse_id('eye of singularity'/'VIS', '3711').

card_in_set('fallen askari', 'VIS').
card_original_type('fallen askari'/'VIS', 'Summon — Knight').
card_original_text('fallen askari'/'VIS', 'Flanking (Whenever a creature without flanking is assigned to block this creature, the blocking creature gets -1/-1 until end of turn.)\nFallen Askari cannot block.').
card_first_print('fallen askari', 'VIS').
card_image_name('fallen askari'/'VIS', 'fallen askari').
card_uid('fallen askari'/'VIS', 'VIS:Fallen Askari:fallen askari').
card_rarity('fallen askari'/'VIS', 'Common').
card_artist('fallen askari'/'VIS', 'Adrian Smith').
card_flavor_text('fallen askari'/'VIS', 'In troubled times, there are few greater sorrows than a wayward savior.').
card_multiverse_id('fallen askari'/'VIS', '3616').

card_in_set('femeref enchantress', 'VIS').
card_original_type('femeref enchantress'/'VIS', 'Summon — Enchantress').
card_original_text('femeref enchantress'/'VIS', 'Whenever an enchantment is put into any graveyard from play, draw a card.').
card_first_print('femeref enchantress', 'VIS').
card_image_name('femeref enchantress'/'VIS', 'femeref enchantress').
card_uid('femeref enchantress'/'VIS', 'VIS:Femeref Enchantress:femeref enchantress').
card_rarity('femeref enchantress'/'VIS', 'Rare').
card_artist('femeref enchantress'/'VIS', 'D. Alexander Gregory').
card_flavor_text('femeref enchantress'/'VIS', '\"Then she spread a fine dust over the land, like a butterfly shaking its wing.\"\n—\"The Enchantress,\" Femeref tale').
card_multiverse_id('femeref enchantress'/'VIS', '3736').

card_in_set('feral instinct', 'VIS').
card_original_type('feral instinct'/'VIS', 'Instant').
card_original_text('feral instinct'/'VIS', 'Target creature gets +1/+1 until end of turn.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_first_print('feral instinct', 'VIS').
card_image_name('feral instinct'/'VIS', 'feral instinct').
card_uid('feral instinct'/'VIS', 'VIS:Feral Instinct:feral instinct').
card_rarity('feral instinct'/'VIS', 'Common').
card_artist('feral instinct'/'VIS', 'Una Fricker').
card_flavor_text('feral instinct'/'VIS', '\"If a rhino\'s head is lower than its shoulders, trust me—it is not praying.\"\n—Mwani, Mtenda herder').
card_multiverse_id('feral instinct'/'VIS', '3664').

card_in_set('fireblast', 'VIS').
card_original_type('fireblast'/'VIS', 'Instant').
card_original_text('fireblast'/'VIS', 'You may sacrifice two mountains instead of paying Fireblast\'s casting cost. Fireblast deals 4 damage to target creature or player.').
card_first_print('fireblast', 'VIS').
card_image_name('fireblast'/'VIS', 'fireblast').
card_uid('fireblast'/'VIS', 'VIS:Fireblast:fireblast').
card_rarity('fireblast'/'VIS', 'Common').
card_artist('fireblast'/'VIS', 'Michael Danza').
card_flavor_text('fireblast'/'VIS', 'Embermages aren\'t well known for their diplomatic skills.').
card_multiverse_id('fireblast'/'VIS', '3686').

card_in_set('firestorm hellkite', 'VIS').
card_original_type('firestorm hellkite'/'VIS', 'Summon — Dragon').
card_original_text('firestorm hellkite'/'VIS', 'Flying, trample\nCumulative upkeep {U}{R}').
card_first_print('firestorm hellkite', 'VIS').
card_image_name('firestorm hellkite'/'VIS', 'firestorm hellkite').
card_uid('firestorm hellkite'/'VIS', 'VIS:Firestorm Hellkite:firestorm hellkite').
card_rarity('firestorm hellkite'/'VIS', 'Rare').
card_artist('firestorm hellkite'/'VIS', 'Pete Venters').
card_flavor_text('firestorm hellkite'/'VIS', '\"If it can scar the sky with fire, do not share its destination.\"\n—Mwani, Mtenda herder').
card_multiverse_id('firestorm hellkite'/'VIS', '3737').

card_in_set('flooded shoreline', 'VIS').
card_original_type('flooded shoreline'/'VIS', 'Enchantment').
card_original_text('flooded shoreline'/'VIS', '{U}{U}, Return two islands you control to owner\'s hand: Return target creature to owner\'s hand.').
card_first_print('flooded shoreline', 'VIS').
card_image_name('flooded shoreline'/'VIS', 'flooded shoreline').
card_uid('flooded shoreline'/'VIS', 'VIS:Flooded Shoreline:flooded shoreline').
card_rarity('flooded shoreline'/'VIS', 'Rare').
card_artist('flooded shoreline'/'VIS', 'Romas Kukalis').
card_flavor_text('flooded shoreline'/'VIS', '\"Some say the sea is jealous of the land and wishes to climb its heights to meet the sky.\"\n—Afari, Tales').
card_multiverse_id('flooded shoreline'/'VIS', '3639').

card_in_set('forbidden ritual', 'VIS').
card_original_type('forbidden ritual'/'VIS', 'Sorcery').
card_original_text('forbidden ritual'/'VIS', 'Sacrifice a card in play: Target opponent loses 2 life unless he or she sacrifices a permanent or chooses and discards a card.\nYou may repeat this process as many times as you choose.').
card_first_print('forbidden ritual', 'VIS').
card_image_name('forbidden ritual'/'VIS', 'forbidden ritual').
card_uid('forbidden ritual'/'VIS', 'VIS:Forbidden Ritual:forbidden ritual').
card_rarity('forbidden ritual'/'VIS', 'Rare').
card_artist('forbidden ritual'/'VIS', 'Christopher Rush').
card_multiverse_id('forbidden ritual'/'VIS', '3617').

card_in_set('foreshadow', 'VIS').
card_original_type('foreshadow'/'VIS', 'Instant').
card_original_text('foreshadow'/'VIS', 'Name a card. Put the top card from target opponent\'s library into his or her graveyard. If that card is the one named, draw a card.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_first_print('foreshadow', 'VIS').
card_image_name('foreshadow'/'VIS', 'foreshadow').
card_uid('foreshadow'/'VIS', 'VIS:Foreshadow:foreshadow').
card_rarity('foreshadow'/'VIS', 'Uncommon').
card_artist('foreshadow'/'VIS', 'George Pratt').
card_multiverse_id('foreshadow'/'VIS', '3640').

card_in_set('freewind falcon', 'VIS').
card_original_type('freewind falcon'/'VIS', 'Summon — Falcon').
card_original_text('freewind falcon'/'VIS', 'Flying, protection from red').
card_first_print('freewind falcon', 'VIS').
card_image_name('freewind falcon'/'VIS', 'freewind falcon').
card_uid('freewind falcon'/'VIS', 'VIS:Freewind Falcon:freewind falcon').
card_rarity('freewind falcon'/'VIS', 'Common').
card_artist('freewind falcon'/'VIS', 'Una Fricker').
card_flavor_text('freewind falcon'/'VIS', '\"That does it! I\'m going back to hunting chickens!\"\n—Rhirhok, goblin archer').
card_multiverse_id('freewind falcon'/'VIS', '3712').

card_in_set('funeral charm', 'VIS').
card_original_type('funeral charm'/'VIS', 'Instant').
card_original_text('funeral charm'/'VIS', 'Choose one Target player chooses and discards a card; or target creature gets +2/-1 until end of turn; or target creature gains swampwalk until end of turn. (If defending player controls any swamps, that creature is unblockable.)').
card_first_print('funeral charm', 'VIS').
card_image_name('funeral charm'/'VIS', 'funeral charm').
card_uid('funeral charm'/'VIS', 'VIS:Funeral Charm:funeral charm').
card_rarity('funeral charm'/'VIS', 'Common').
card_artist('funeral charm'/'VIS', 'Greg Spalenka').
card_multiverse_id('funeral charm'/'VIS', '3618').

card_in_set('giant caterpillar', 'VIS').
card_original_type('giant caterpillar'/'VIS', 'Summon — Caterpillar').
card_original_text('giant caterpillar'/'VIS', '{G}, Sacrifice Giant Caterpillar: Put a Butterfly token into play at end of turn. Treat this token as a 1/1 green creature with flying.').
card_first_print('giant caterpillar', 'VIS').
card_image_name('giant caterpillar'/'VIS', 'giant caterpillar').
card_uid('giant caterpillar'/'VIS', 'VIS:Giant Caterpillar:giant caterpillar').
card_rarity('giant caterpillar'/'VIS', 'Common').
card_artist('giant caterpillar'/'VIS', 'Zina Saunders').
card_flavor_text('giant caterpillar'/'VIS', '\"‘I\'ve seen hornworms big as a man\'s fist,\' the traveler said, and nodded soberly when our jaws went slack at his ignorance.\"\n—Afari, Tales').
card_multiverse_id('giant caterpillar'/'VIS', '3665').

card_in_set('goblin recruiter', 'VIS').
card_original_type('goblin recruiter'/'VIS', 'Summon — Goblin').
card_original_text('goblin recruiter'/'VIS', 'When Goblin Recruiter comes into play, search your library for any number of Goblin cards. Reveal those cards to all players. Shuffle your library, then put the cards on top of your library in any order.').
card_first_print('goblin recruiter', 'VIS').
card_image_name('goblin recruiter'/'VIS', 'goblin recruiter').
card_uid('goblin recruiter'/'VIS', 'VIS:Goblin Recruiter:goblin recruiter').
card_rarity('goblin recruiter'/'VIS', 'Uncommon').
card_artist('goblin recruiter'/'VIS', 'Scott Kirschner').
card_flavor_text('goblin recruiter'/'VIS', '\"Next!\"').
card_multiverse_id('goblin recruiter'/'VIS', '3687').

card_in_set('goblin swine-rider', 'VIS').
card_original_type('goblin swine-rider'/'VIS', 'Summon — Goblin').
card_original_text('goblin swine-rider'/'VIS', 'If Goblin Swine-Rider is blocked, it deals 2 damage to each attacking creature and 2 damage to each blocking creature.').
card_first_print('goblin swine-rider', 'VIS').
card_image_name('goblin swine-rider'/'VIS', 'goblin swine-rider').
card_uid('goblin swine-rider'/'VIS', 'VIS:Goblin Swine-Rider:goblin swine-rider').
card_rarity('goblin swine-rider'/'VIS', 'Common').
card_artist('goblin swine-rider'/'VIS', 'G. Darrow & I. Rabarot').
card_flavor_text('goblin swine-rider'/'VIS', '\"May you get the mount you deserve.\"\n—Suq\'Ata curse').
card_multiverse_id('goblin swine-rider'/'VIS', '3688').

card_in_set('gossamer chains', 'VIS').
card_original_type('gossamer chains'/'VIS', 'Enchantment').
card_original_text('gossamer chains'/'VIS', 'Return Gossamer Chains to owner\'s hand: Target unblocked creature deals no combat damage this turn.').
card_first_print('gossamer chains', 'VIS').
card_image_name('gossamer chains'/'VIS', 'gossamer chains').
card_uid('gossamer chains'/'VIS', 'VIS:Gossamer Chains:gossamer chains').
card_rarity('gossamer chains'/'VIS', 'Common').
card_artist('gossamer chains'/'VIS', 'Steve Luke').
card_flavor_text('gossamer chains'/'VIS', '\"Harah swung his sword mightily, but still the air held him fast.\"\n—Azeworai, \"Kenechi\'s Dream\"').
card_multiverse_id('gossamer chains'/'VIS', '3713').

card_in_set('griffin canyon', 'VIS').
card_original_type('griffin canyon'/'VIS', 'Land').
card_original_text('griffin canyon'/'VIS', '{T}: Add one colorless mana to your mana pool.\n{T}: Untap target Griffin. That Griffin gets +1/+1 until end of turn.').
card_first_print('griffin canyon', 'VIS').
card_image_name('griffin canyon'/'VIS', 'griffin canyon').
card_uid('griffin canyon'/'VIS', 'VIS:Griffin Canyon:griffin canyon').
card_rarity('griffin canyon'/'VIS', 'Rare').
card_artist('griffin canyon'/'VIS', 'Stuart Griffin').
card_flavor_text('griffin canyon'/'VIS', '\"Where are falling feathers deadlier than falling rock?\"\n—The One Thousand Questions').
card_multiverse_id('griffin canyon'/'VIS', '3751').

card_in_set('guiding spirit', 'VIS').
card_original_type('guiding spirit'/'VIS', 'Summon — Angel').
card_original_text('guiding spirit'/'VIS', 'Flying\n{T}: If the top card of target player\'s graveyard is a creature card, put that card on the top of that player\'s library.').
card_first_print('guiding spirit', 'VIS').
card_image_name('guiding spirit'/'VIS', 'guiding spirit').
card_uid('guiding spirit'/'VIS', 'VIS:Guiding Spirit:guiding spirit').
card_rarity('guiding spirit'/'VIS', 'Rare').
card_artist('guiding spirit'/'VIS', 'Terese Nielsen').
card_flavor_text('guiding spirit'/'VIS', '\"Retainer of eternal Sun! Life flash again upon thy wings.\"\n—\"Song to the Sun,\" Femeref song').
card_multiverse_id('guiding spirit'/'VIS', '3738').

card_in_set('hearth charm', 'VIS').
card_original_type('hearth charm'/'VIS', 'Instant').
card_original_text('hearth charm'/'VIS', 'Choose one Destroy target artifact creature; or all attacking creatures get +1/+0 until end of turn; or target creature with power 2 or less is unblockable this turn.').
card_first_print('hearth charm', 'VIS').
card_image_name('hearth charm'/'VIS', 'hearth charm').
card_uid('hearth charm'/'VIS', 'VIS:Hearth Charm:hearth charm').
card_rarity('hearth charm'/'VIS', 'Common').
card_artist('hearth charm'/'VIS', 'Greg Spalenka').
card_multiverse_id('hearth charm'/'VIS', '3689').

card_in_set('heat wave', 'VIS').
card_original_type('heat wave'/'VIS', 'Enchantment').
card_original_text('heat wave'/'VIS', 'Cumulative upkeep {R}\nBlue creatures cannot block creatures you control. Nonblue creatures cannot block creatures you control unless their controller pays an additional 1 life for each blocking creature.').
card_first_print('heat wave', 'VIS').
card_image_name('heat wave'/'VIS', 'heat wave').
card_uid('heat wave'/'VIS', 'VIS:Heat Wave:heat wave').
card_rarity('heat wave'/'VIS', 'Uncommon').
card_artist('heat wave'/'VIS', 'Alan Rabinowitz').
card_multiverse_id('heat wave'/'VIS', '3690').

card_in_set('helm of awakening', 'VIS').
card_original_type('helm of awakening'/'VIS', 'Artifact').
card_original_text('helm of awakening'/'VIS', 'All spells cost one generic mana less to play.').
card_first_print('helm of awakening', 'VIS').
card_image_name('helm of awakening'/'VIS', 'helm of awakening').
card_uid('helm of awakening'/'VIS', 'VIS:Helm of Awakening:helm of awakening').
card_rarity('helm of awakening'/'VIS', 'Uncommon').
card_artist('helm of awakening'/'VIS', 'Adam Rex').
card_flavor_text('helm of awakening'/'VIS', '\"This little prize will cover your head, your spells, your bets, and your behind!\"\n—Pashad ibn Asim, Suq\'Ata trader').
card_multiverse_id('helm of awakening'/'VIS', '3593').

card_in_set('honorable passage', 'VIS').
card_original_type('honorable passage'/'VIS', 'Instant').
card_original_text('honorable passage'/'VIS', 'Prevent all damage to you or target creature from any one source. If that source is red, Honorable Passage deals to the source\'s controller an amount of damage equal to the amount of damage prevented.').
card_first_print('honorable passage', 'VIS').
card_image_name('honorable passage'/'VIS', 'honorable passage').
card_uid('honorable passage'/'VIS', 'VIS:Honorable Passage:honorable passage').
card_rarity('honorable passage'/'VIS', 'Uncommon').
card_artist('honorable passage'/'VIS', 'Jeff Miracola').
card_multiverse_id('honorable passage'/'VIS', '3714').

card_in_set('hope charm', 'VIS').
card_original_type('hope charm'/'VIS', 'Instant').
card_original_text('hope charm'/'VIS', 'Choose one Target creature gains first strike until end of turn; or target player gains 2 life; or destroy target local enchantment.').
card_first_print('hope charm', 'VIS').
card_image_name('hope charm'/'VIS', 'hope charm').
card_uid('hope charm'/'VIS', 'VIS:Hope Charm:hope charm').
card_rarity('hope charm'/'VIS', 'Common').
card_artist('hope charm'/'VIS', 'Greg Spalenka').
card_multiverse_id('hope charm'/'VIS', '3715').

card_in_set('hulking cyclops', 'VIS').
card_original_type('hulking cyclops'/'VIS', 'Summon — Cyclops').
card_original_text('hulking cyclops'/'VIS', 'Hulking Cyclops cannot block.').
card_first_print('hulking cyclops', 'VIS').
card_image_name('hulking cyclops'/'VIS', 'hulking cyclops').
card_uid('hulking cyclops'/'VIS', 'VIS:Hulking Cyclops:hulking cyclops').
card_rarity('hulking cyclops'/'VIS', 'Uncommon').
card_artist('hulking cyclops'/'VIS', 'DiTerlizzi').
card_flavor_text('hulking cyclops'/'VIS', '\"Some say a great cyclops turns the world in its hands, looking down upon it with its one bright eye.\"\n—Afari, Tales').
card_multiverse_id('hulking cyclops'/'VIS', '3691').

card_in_set('impulse', 'VIS').
card_original_type('impulse'/'VIS', 'Instant').
card_original_text('impulse'/'VIS', 'Look at the top four cards of your library. Put one of them into your hand and the rest on the bottom of your library. Shuffle your library afterwards.').
card_first_print('impulse', 'VIS').
card_image_name('impulse'/'VIS', 'impulse').
card_uid('impulse'/'VIS', 'VIS:Impulse:impulse').
card_rarity('impulse'/'VIS', 'Common').
card_artist('impulse'/'VIS', 'Bryan Talbot').
card_flavor_text('impulse'/'VIS', '\"Controlling time ensures you need never look impulsive again.\"\n—Teferi').
card_multiverse_id('impulse'/'VIS', '3641').

card_in_set('infantry veteran', 'VIS').
card_original_type('infantry veteran'/'VIS', 'Summon — Soldier').
card_original_text('infantry veteran'/'VIS', '{T}: Target attacking creature gets +1/+1 until end of turn.').
card_first_print('infantry veteran', 'VIS').
card_image_name('infantry veteran'/'VIS', 'infantry veteran').
card_uid('infantry veteran'/'VIS', 'VIS:Infantry Veteran:infantry veteran').
card_rarity('infantry veteran'/'VIS', 'Common').
card_artist('infantry veteran'/'VIS', 'Christopher Rush').
card_flavor_text('infantry veteran'/'VIS', '\"The true dishonor for a soldier is surviving the war.\"\n—Telim\'Tor').
card_multiverse_id('infantry veteran'/'VIS', '3716').

card_in_set('infernal harvest', 'VIS').
card_original_type('infernal harvest'/'VIS', 'Sorcery').
card_original_text('infernal harvest'/'VIS', 'Return X swamps you control to owner\'s hand: Infernal Harvest deals X damage, divided any way you choose, among any number of target creatures.').
card_first_print('infernal harvest', 'VIS').
card_image_name('infernal harvest'/'VIS', 'infernal harvest').
card_uid('infernal harvest'/'VIS', 'VIS:Infernal Harvest:infernal harvest').
card_rarity('infernal harvest'/'VIS', 'Common').
card_artist('infernal harvest'/'VIS', 'Nathalie Hertz').
card_flavor_text('infernal harvest'/'VIS', '\"I offered you eternal life; I just didn\'t say where.\"\n—Shauku, Endbringer').
card_multiverse_id('infernal harvest'/'VIS', '3619').

card_in_set('inspiration', 'VIS').
card_original_type('inspiration'/'VIS', 'Instant').
card_original_text('inspiration'/'VIS', 'Target player draws two cards.').
card_first_print('inspiration', 'VIS').
card_image_name('inspiration'/'VIS', 'inspiration').
card_uid('inspiration'/'VIS', 'VIS:Inspiration:inspiration').
card_rarity('inspiration'/'VIS', 'Common').
card_artist('inspiration'/'VIS', 'Zina Saunders').
card_flavor_text('inspiration'/'VIS', '\"Madness and genius are separated only by degrees of success.\"\n—Sidar Jabari').
card_multiverse_id('inspiration'/'VIS', '3642').

card_in_set('iron-heart chimera', 'VIS').
card_original_type('iron-heart chimera'/'VIS', 'Artifact Creature').
card_original_text('iron-heart chimera'/'VIS', 'Attacking does not cause Iron-Heart Chimera to tap.\nIron-Heart Chimera counts as a Chimera.\nSacrifice Iron-Heart Chimera: Put a +2/+2 counter on target Chimera and attacking any turn does not cause that Chimera to tap.').
card_first_print('iron-heart chimera', 'VIS').
card_image_name('iron-heart chimera'/'VIS', 'iron-heart chimera').
card_uid('iron-heart chimera'/'VIS', 'VIS:Iron-Heart Chimera:iron-heart chimera').
card_rarity('iron-heart chimera'/'VIS', 'Uncommon').
card_artist('iron-heart chimera'/'VIS', 'Mike Dringenberg').
card_multiverse_id('iron-heart chimera'/'VIS', '3594').

card_in_set('jamuraan lion', 'VIS').
card_original_type('jamuraan lion'/'VIS', 'Summon — Lion').
card_original_text('jamuraan lion'/'VIS', '{W}, {T}: Target creature cannot block this turn.').
card_first_print('jamuraan lion', 'VIS').
card_image_name('jamuraan lion'/'VIS', 'jamuraan lion').
card_uid('jamuraan lion'/'VIS', 'VIS:Jamuraan Lion:jamuraan lion').
card_rarity('jamuraan lion'/'VIS', 'Common').
card_artist('jamuraan lion'/'VIS', 'Stuart Griffin').
card_flavor_text('jamuraan lion'/'VIS', 'The lion blinked\n—Femeref expression meaning \"situation avoided\"').
card_multiverse_id('jamuraan lion'/'VIS', '3717').

card_in_set('juju bubble', 'VIS').
card_original_type('juju bubble'/'VIS', 'Artifact').
card_original_text('juju bubble'/'VIS', 'Cumulative upkeep {1}\nIf you play a card, bury Juju Bubble.\n{2}: Gain 1 life.').
card_first_print('juju bubble', 'VIS').
card_image_name('juju bubble'/'VIS', 'juju bubble').
card_uid('juju bubble'/'VIS', 'VIS:Juju Bubble:juju bubble').
card_rarity('juju bubble'/'VIS', 'Uncommon').
card_artist('juju bubble'/'VIS', 'Donato Giancola').
card_flavor_text('juju bubble'/'VIS', '\"If my heart had a lid, I would lift it and show you my joy.\"\n—From Suq\'Ata wedding ceremony').
card_multiverse_id('juju bubble'/'VIS', '3595').

card_in_set('jungle basin', 'VIS').
card_original_type('jungle basin'/'VIS', 'Land').
card_original_text('jungle basin'/'VIS', 'Jungle Basin comes into play tapped. \nWhen Jungle Basin comes into play, return an untapped forest you control to owner\'s hand or bury Jungle Basin.\n{T}: Add {G} and one colorless mana to your mana pool.').
card_first_print('jungle basin', 'VIS').
card_image_name('jungle basin'/'VIS', 'jungle basin').
card_uid('jungle basin'/'VIS', 'VIS:Jungle Basin:jungle basin').
card_rarity('jungle basin'/'VIS', 'Uncommon').
card_artist('jungle basin'/'VIS', 'John Avon').
card_multiverse_id('jungle basin'/'VIS', '3752').

card_in_set('kaervek\'s spite', 'VIS').
card_original_type('kaervek\'s spite'/'VIS', 'Instant').
card_original_text('kaervek\'s spite'/'VIS', 'Sacrifice all permanents, Discard your hand: Target player loses 5 life.').
card_first_print('kaervek\'s spite', 'VIS').
card_image_name('kaervek\'s spite'/'VIS', 'kaervek\'s spite').
card_uid('kaervek\'s spite'/'VIS', 'VIS:Kaervek\'s Spite:kaervek\'s spite').
card_rarity('kaervek\'s spite'/'VIS', 'Rare').
card_artist('kaervek\'s spite'/'VIS', 'Bryan Talbot').
card_flavor_text('kaervek\'s spite'/'VIS', '\"The end justifies the means. What do I care if I rule over the dead rather than over the living? The dead ask fewer questions.\"\n—Kaervek').
card_multiverse_id('kaervek\'s spite'/'VIS', '3620').

card_in_set('karoo', 'VIS').
card_original_type('karoo'/'VIS', 'Land').
card_original_text('karoo'/'VIS', 'Karoo comes into play tapped.\nWhen Karoo comes into play, return an untapped plains you control to owner\'s hand or bury Karoo.\n{T}: Add {W} and one colorless mana to your mana pool.').
card_first_print('karoo', 'VIS').
card_image_name('karoo'/'VIS', 'karoo').
card_uid('karoo'/'VIS', 'VIS:Karoo:karoo').
card_rarity('karoo'/'VIS', 'Uncommon').
card_artist('karoo'/'VIS', 'Zina Saunders').
card_multiverse_id('karoo'/'VIS', '3753').

card_in_set('katabatic winds', 'VIS').
card_original_type('katabatic winds'/'VIS', 'Enchantment').
card_original_text('katabatic winds'/'VIS', 'Phasing\nCreatures with flying cannot attack, block, or use any ability that includes {T} in the activation cost.').
card_first_print('katabatic winds', 'VIS').
card_image_name('katabatic winds'/'VIS', 'katabatic winds').
card_uid('katabatic winds'/'VIS', 'VIS:Katabatic Winds:katabatic winds').
card_rarity('katabatic winds'/'VIS', 'Rare').
card_artist('katabatic winds'/'VIS', 'Gary Gianni').
card_flavor_text('katabatic winds'/'VIS', 'Wind will win, whatever we wish.').
card_multiverse_id('katabatic winds'/'VIS', '3666').

card_in_set('keeper of kookus', 'VIS').
card_original_type('keeper of kookus'/'VIS', 'Summon — Goblin').
card_original_text('keeper of kookus'/'VIS', '{R} Protection from red until end of turn.').
card_first_print('keeper of kookus', 'VIS').
card_image_name('keeper of kookus'/'VIS', 'keeper of kookus').
card_uid('keeper of kookus'/'VIS', 'VIS:Keeper of Kookus:keeper of kookus').
card_rarity('keeper of kookus'/'VIS', 'Common').
card_artist('keeper of kookus'/'VIS', 'Scott Hampton').
card_flavor_text('keeper of kookus'/'VIS', 'Wanted: Thick-skinned goblin for guarding mean ol\' Kookus. Must like fires. Must heal quickly.').
card_multiverse_id('keeper of kookus'/'VIS', '3692').

card_in_set('king cheetah', 'VIS').
card_original_type('king cheetah'/'VIS', 'Summon — Cheetah').
card_original_text('king cheetah'/'VIS', 'You may choose to play King Cheetah whenever you could play an instant.').
card_image_name('king cheetah'/'VIS', 'king cheetah').
card_uid('king cheetah'/'VIS', 'VIS:King Cheetah:king cheetah').
card_rarity('king cheetah'/'VIS', 'Common').
card_artist('king cheetah'/'VIS', 'Terese Nielsen').
card_flavor_text('king cheetah'/'VIS', 'If you find yourself and a friend being chased by a King Cheetah, you have but one chance: Trip your friend.\n—Suq\'Ata wisdom').
card_multiverse_id('king cheetah'/'VIS', '3667').

card_in_set('knight of the mists', 'VIS').
card_original_type('knight of the mists'/'VIS', 'Summon — Knight').
card_original_text('knight of the mists'/'VIS', 'Flanking (Whenever a creature without flanking is assigned to block this creature, the blocking creature gets -1/-1 until end of turn.)\nWhen Knight of the Mists comes into play, pay {U} or bury target Knight.').
card_first_print('knight of the mists', 'VIS').
card_image_name('knight of the mists'/'VIS', 'knight of the mists').
card_uid('knight of the mists'/'VIS', 'VIS:Knight of the Mists:knight of the mists').
card_rarity('knight of the mists'/'VIS', 'Common').
card_artist('knight of the mists'/'VIS', 'Harold McNeill').
card_flavor_text('knight of the mists'/'VIS', 'Fear the mists, for they are armed.').
card_multiverse_id('knight of the mists'/'VIS', '3643').

card_in_set('knight of valor', 'VIS').
card_original_type('knight of valor'/'VIS', 'Summon — Knight').
card_original_text('knight of valor'/'VIS', 'Flanking (Whenever a creature without flanking is assigned to block this creature, the blocking creature gets -1/-1 until end of turn.)\n{1}{W} Each creature without flanking blocking Knight of Valor gets -1/-1 until end of turn. Use this ability only once each turn.').
card_first_print('knight of valor', 'VIS').
card_image_name('knight of valor'/'VIS', 'knight of valor').
card_uid('knight of valor'/'VIS', 'VIS:Knight of Valor:knight of valor').
card_rarity('knight of valor'/'VIS', 'Common').
card_artist('knight of valor'/'VIS', 'Jeff Miracola').
card_multiverse_id('knight of valor'/'VIS', '3718').

card_in_set('kookus', 'VIS').
card_original_type('kookus'/'VIS', 'Summon — Djinn').
card_original_text('kookus'/'VIS', 'Trample\nDuring your upkeep, if you do not control at least one Keeper of Kookus, Kookus deals 3 damage to you and attacks this turn if able.\n{R} +1/+0 until end of turn.').
card_first_print('kookus', 'VIS').
card_image_name('kookus'/'VIS', 'kookus').
card_uid('kookus'/'VIS', 'VIS:Kookus:kookus').
card_rarity('kookus'/'VIS', 'Rare').
card_artist('kookus'/'VIS', 'Scott Hampton').
card_multiverse_id('kookus'/'VIS', '3693').

card_in_set('kyscu drake', 'VIS').
card_original_type('kyscu drake'/'VIS', 'Summon — Drake').
card_original_text('kyscu drake'/'VIS', 'Flying\n{G} +0/+1 until end of turn. You cannot spend more than {G} in this way each turn.\nSacrifice Kyscu Drake and Spitting Drake: Search your library for Viashivan Dragon and put it into play. Shuffle your library afterwards.').
card_first_print('kyscu drake', 'VIS').
card_image_name('kyscu drake'/'VIS', 'kyscu drake').
card_uid('kyscu drake'/'VIS', 'VIS:Kyscu Drake:kyscu drake').
card_rarity('kyscu drake'/'VIS', 'Uncommon').
card_artist('kyscu drake'/'VIS', 'G. Darrow & I. Rabarot').
card_multiverse_id('kyscu drake'/'VIS', '3668').

card_in_set('lead-belly chimera', 'VIS').
card_original_type('lead-belly chimera'/'VIS', 'Artifact Creature').
card_original_text('lead-belly chimera'/'VIS', 'Trample\nLead-Belly Chimera counts as a Chimera.\nSacrifice Lead-Belly Chimera: Put a +2/+2 counter on target Chimera and that Chimera gains trample permanently.').
card_first_print('lead-belly chimera', 'VIS').
card_image_name('lead-belly chimera'/'VIS', 'lead-belly chimera').
card_uid('lead-belly chimera'/'VIS', 'VIS:Lead-Belly Chimera:lead-belly chimera').
card_rarity('lead-belly chimera'/'VIS', 'Uncommon').
card_artist('lead-belly chimera'/'VIS', 'Mike Dringenberg').
card_multiverse_id('lead-belly chimera'/'VIS', '3596').

card_in_set('lichenthrope', 'VIS').
card_original_type('lichenthrope'/'VIS', 'Summon — Lichenthrope').
card_original_text('lichenthrope'/'VIS', 'For each 1 damage dealt to Lichenthrope, put a -1/-1 counter on it instead.\nDuring your upkeep, remove one of these -1/-1 counters from Lichenthrope.').
card_first_print('lichenthrope', 'VIS').
card_image_name('lichenthrope'/'VIS', 'lichenthrope').
card_uid('lichenthrope'/'VIS', 'VIS:Lichenthrope:lichenthrope').
card_rarity('lichenthrope'/'VIS', 'Rare').
card_artist('lichenthrope'/'VIS', 'Bob Eggleton').
card_multiverse_id('lichenthrope'/'VIS', '3669').

card_in_set('lightning cloud', 'VIS').
card_original_type('lightning cloud'/'VIS', 'Enchantment').
card_original_text('lightning cloud'/'VIS', '{R} Lightning Cloud deals 1 damage to target creature or player. Use this ability only when a red spell is successfully cast and only once for each such spell.').
card_first_print('lightning cloud', 'VIS').
card_image_name('lightning cloud'/'VIS', 'lightning cloud').
card_uid('lightning cloud'/'VIS', 'VIS:Lightning Cloud:lightning cloud').
card_rarity('lightning cloud'/'VIS', 'Rare').
card_artist('lightning cloud'/'VIS', 'John Matson').
card_flavor_text('lightning cloud'/'VIS', 'The mightiest clouds sit upon spires of fire.\n—Femeref adage').
card_multiverse_id('lightning cloud'/'VIS', '3694').

card_in_set('longbow archer', 'VIS').
card_original_type('longbow archer'/'VIS', 'Summon — Archer').
card_original_text('longbow archer'/'VIS', 'First strike\nLongbow Archer can block creatures with flying.').
card_first_print('longbow archer', 'VIS').
card_image_name('longbow archer'/'VIS', 'longbow archer').
card_uid('longbow archer'/'VIS', 'VIS:Longbow Archer:longbow archer').
card_rarity('longbow archer'/'VIS', 'Uncommon').
card_artist('longbow archer'/'VIS', 'Eric Peterson').
card_flavor_text('longbow archer'/'VIS', '\"If it bears wings, I will pin it to the skies over Tefemburu.\"\n—Ruya, Zhalfirin archer').
card_multiverse_id('longbow archer'/'VIS', '3719').

card_in_set('magma mine', 'VIS').
card_original_type('magma mine'/'VIS', 'Artifact').
card_original_text('magma mine'/'VIS', '{4}: Put a pressure counter on Magma Mine.\n{T}, Sacrifice Magam Mine: For each pressure counter on it, Magma Mine deals 1 damage to target creature or player.').
card_first_print('magma mine', 'VIS').
card_image_name('magma mine'/'VIS', 'magma mine').
card_uid('magma mine'/'VIS', 'VIS:Magma Mine:magma mine').
card_rarity('magma mine'/'VIS', 'Uncommon').
card_artist('magma mine'/'VIS', 'Ron Spencer').
card_flavor_text('magma mine'/'VIS', 'BOOM!').
card_multiverse_id('magma mine'/'VIS', '3597').

card_in_set('man-o\'-war', 'VIS').
card_original_type('man-o\'-war'/'VIS', 'Summon — Jellyfish').
card_original_text('man-o\'-war'/'VIS', 'When Man-o\'-War comes into play, return target creature to owner\'s hand.').
card_image_name('man-o\'-war'/'VIS', 'man-o\'-war').
card_uid('man-o\'-war'/'VIS', 'VIS:Man-o\'-War:man-o\'-war').
card_rarity('man-o\'-war'/'VIS', 'Common').
card_artist('man-o\'-war'/'VIS', 'Jon J. Muth').
card_flavor_text('man-o\'-war'/'VIS', '\"Beauty to the eye does not always translate to the touch.\"\n—Naimah, Femeref philosopher').
card_multiverse_id('man-o\'-war'/'VIS', '3644').

card_in_set('matopi golem', 'VIS').
card_original_type('matopi golem'/'VIS', 'Artifact Creature').
card_original_text('matopi golem'/'VIS', '{1}: Regenerate and put a -1/-1 counter on Matopi Golem.').
card_first_print('matopi golem', 'VIS').
card_image_name('matopi golem'/'VIS', 'matopi golem').
card_uid('matopi golem'/'VIS', 'VIS:Matopi Golem:matopi golem').
card_rarity('matopi golem'/'VIS', 'Uncommon').
card_artist('matopi golem'/'VIS', 'Tom Kyffin').
card_flavor_text('matopi golem'/'VIS', 'Some potmakers claim to use the mud of such creatures to make their finest bowls—and that the bowls scream when fired.').
card_multiverse_id('matopi golem'/'VIS', '3598').

card_in_set('miraculous recovery', 'VIS').
card_original_type('miraculous recovery'/'VIS', 'Instant').
card_original_text('miraculous recovery'/'VIS', 'Put target creature card from your graveyard into play and put a +1/+1 counter on that creature. Treat the creature as though it were just played.').
card_first_print('miraculous recovery', 'VIS').
card_image_name('miraculous recovery'/'VIS', 'miraculous recovery').
card_uid('miraculous recovery'/'VIS', 'VIS:Miraculous Recovery:miraculous recovery').
card_rarity('miraculous recovery'/'VIS', 'Uncommon').
card_artist('miraculous recovery'/'VIS', 'Brian Horton').
card_flavor_text('miraculous recovery'/'VIS', '\"You stop breathing for just a few minutes and everyone jumps to conclusions.\"\n—Zarkuu, necrosavant').
card_multiverse_id('miraculous recovery'/'VIS', '3720').

card_in_set('mob mentality', 'VIS').
card_original_type('mob mentality'/'VIS', 'Enchant Creature').
card_original_text('mob mentality'/'VIS', 'Enchanted creature gains trample.\nIf all non-Wall creatures you control attack, enchanted creature gets +*/+0 until end of turn, where * is equal to the number of attacking creatures.').
card_first_print('mob mentality', 'VIS').
card_image_name('mob mentality'/'VIS', 'mob mentality').
card_uid('mob mentality'/'VIS', 'VIS:Mob Mentality:mob mentality').
card_rarity('mob mentality'/'VIS', 'Uncommon').
card_artist('mob mentality'/'VIS', 'Douglas Shuler').
card_flavor_text('mob mentality'/'VIS', '\"Why is loud stupidity so infectious?\"\n—Rana, Suq\'Ata market fool').
card_multiverse_id('mob mentality'/'VIS', '3695').

card_in_set('mortal wound', 'VIS').
card_original_type('mortal wound'/'VIS', 'Enchant Creature').
card_original_text('mortal wound'/'VIS', 'If damage is dealt to enchanted creature, destroy it.').
card_first_print('mortal wound', 'VIS').
card_image_name('mortal wound'/'VIS', 'mortal wound').
card_uid('mortal wound'/'VIS', 'VIS:Mortal Wound:mortal wound').
card_rarity('mortal wound'/'VIS', 'Common').
card_artist('mortal wound'/'VIS', 'Kev Walker').
card_flavor_text('mortal wound'/'VIS', '\"Their tears spill over Jamuraa. Mixed with blood, they wash everything red.\"\n—\"Love Song of Night and Day\"').
card_multiverse_id('mortal wound'/'VIS', '3670').

card_in_set('mundungu', 'VIS').
card_original_type('mundungu'/'VIS', 'Summon — Wizard').
card_original_text('mundungu'/'VIS', '{T}: Counter target spell unless that spell\'s caster pays an additional {1} and 1 life. Play this ability as an interrupt.').
card_first_print('mundungu', 'VIS').
card_image_name('mundungu'/'VIS', 'mundungu').
card_uid('mundungu'/'VIS', 'VIS:Mundungu:mundungu').
card_rarity('mundungu'/'VIS', 'Uncommon').
card_artist('mundungu'/'VIS', 'Terese Nielsen').
card_flavor_text('mundungu'/'VIS', '\"Our buried kings silently await the revolt of dead peasants.\"\n—Tuwile, mundungu of Aku').
card_multiverse_id('mundungu'/'VIS', '3739').

card_in_set('mystic veil', 'VIS').
card_original_type('mystic veil'/'VIS', 'Enchant Creature').
card_original_text('mystic veil'/'VIS', 'You may choose to play Mystic Veil as an instant; if you do, bury it at end of turn.\nEnchanted creature cannot be the target of spells or effects.').
card_first_print('mystic veil', 'VIS').
card_image_name('mystic veil'/'VIS', 'mystic veil').
card_uid('mystic veil'/'VIS', 'VIS:Mystic Veil:mystic veil').
card_rarity('mystic veil'/'VIS', 'Common').
card_artist('mystic veil'/'VIS', 'D. Alexander Gregory').
card_flavor_text('mystic veil'/'VIS', '\"Magic is my thread, my will the needle. I weave over you as if you never were.\"\n—Poetics of Hanan').
card_multiverse_id('mystic veil'/'VIS', '3645').

card_in_set('natural order', 'VIS').
card_original_type('natural order'/'VIS', 'Sorcery').
card_original_text('natural order'/'VIS', 'Sacrifice a green creature: Search your library for a green creature card and put it into play as though it were just played. Shuffle your library afterwards.').
card_first_print('natural order', 'VIS').
card_image_name('natural order'/'VIS', 'natural order').
card_uid('natural order'/'VIS', 'VIS:Natural Order:natural order').
card_rarity('natural order'/'VIS', 'Rare').
card_artist('natural order'/'VIS', 'Terese Nielsen').
card_flavor_text('natural order'/'VIS', '. . . but the price of Mangara\'s freedom was Asmira\'s life.').
card_multiverse_id('natural order'/'VIS', '3671').

card_in_set('necromancy', 'VIS').
card_original_type('necromancy'/'VIS', 'Enchantment').
card_original_text('necromancy'/'VIS', 'You may choose to play Necromancy as an instant; if you do, bury it at end of turn.\nWhen you play Necromancy, choose target creature card in any graveyard. When Necromancy comes into play, put that creature into play as though it were just played and Necromancy becomes a creature enchantment that targets the creature. If Necromancy leaves play, bury the creature.').
card_first_print('necromancy', 'VIS').
card_image_name('necromancy'/'VIS', 'necromancy').
card_uid('necromancy'/'VIS', 'VIS:Necromancy:necromancy').
card_rarity('necromancy'/'VIS', 'Uncommon').
card_artist('necromancy'/'VIS', 'Pete Venters').
card_multiverse_id('necromancy'/'VIS', '3621').

card_in_set('necrosavant', 'VIS').
card_original_type('necrosavant'/'VIS', 'Summon — Necrosavant').
card_original_text('necrosavant'/'VIS', '{3}{B}{B}, Sacrifice a creature: Put Necrosavant into play. Use this ability only during your upkeep and only if Necrosavant is in your graveyard.').
card_image_name('necrosavant'/'VIS', 'necrosavant').
card_uid('necrosavant'/'VIS', 'VIS:Necrosavant:necrosavant').
card_rarity('necrosavant'/'VIS', 'Rare').
card_artist('necrosavant'/'VIS', 'John Coulthart').
card_flavor_text('necrosavant'/'VIS', '\"Ah, I remember my first death.\"\n—Zarkuu, necrosavant').
card_multiverse_id('necrosavant'/'VIS', '3622').

card_in_set('nekrataal', 'VIS').
card_original_type('nekrataal'/'VIS', 'Summon — Nekrataal').
card_original_text('nekrataal'/'VIS', 'First strike\nWhen Nekrataal comes into play, bury target nonartifact, nonblack creature.').
card_first_print('nekrataal', 'VIS').
card_image_name('nekrataal'/'VIS', 'nekrataal').
card_uid('nekrataal'/'VIS', 'VIS:Nekrataal:nekrataal').
card_rarity('nekrataal'/'VIS', 'Uncommon').
card_artist('nekrataal'/'VIS', 'Adrian Smith').
card_flavor_text('nekrataal'/'VIS', '\"I have seen the horrors Kaervek has freed. My betrayal is certain—but of Kaervek or of Jamuraa, I cannot say.\"\n—Jolrael').
card_multiverse_id('nekrataal'/'VIS', '3623').

card_in_set('ogre enforcer', 'VIS').
card_original_type('ogre enforcer'/'VIS', 'Summon — Ogre').
card_original_text('ogre enforcer'/'VIS', 'Ogre Enforcer cannot be destroyed by lethal damage unless a single source deals enough damage to destroy it.').
card_first_print('ogre enforcer', 'VIS').
card_image_name('ogre enforcer'/'VIS', 'ogre enforcer').
card_uid('ogre enforcer'/'VIS', 'VIS:Ogre Enforcer:ogre enforcer').
card_rarity('ogre enforcer'/'VIS', 'Rare').
card_artist('ogre enforcer'/'VIS', 'Pete Venters').
card_flavor_text('ogre enforcer'/'VIS', '\"Ate up all of his siblings in the first week. A good beginning for one destined to command.\"\n—Suka, ogre matron').
card_multiverse_id('ogre enforcer'/'VIS', '3696').

card_in_set('ovinomancer', 'VIS').
card_original_type('ovinomancer'/'VIS', 'Summon — Sorcerer').
card_original_text('ovinomancer'/'VIS', 'When Ovinomancer comes into play, return three basic lands you control to owner\'s hand or bury Ovinomancer.\n{T}, Return Ovinomancer to owner\'s hand: Bury target creature and put a Sheep token into play under the control of the creature\'s controller. Treat this token as a 0/1 green creature.').
card_image_name('ovinomancer'/'VIS', 'ovinomancer').
card_uid('ovinomancer'/'VIS', 'VIS:Ovinomancer:ovinomancer').
card_rarity('ovinomancer'/'VIS', 'Uncommon').
card_artist('ovinomancer'/'VIS', 'Kev Walker').
card_multiverse_id('ovinomancer'/'VIS', '3646').

card_in_set('panther warriors', 'VIS').
card_original_type('panther warriors'/'VIS', 'Summon — Cat Warriors').
card_original_text('panther warriors'/'VIS', '').
card_first_print('panther warriors', 'VIS').
card_image_name('panther warriors'/'VIS', 'panther warriors').
card_uid('panther warriors'/'VIS', 'VIS:Panther Warriors:panther warriors').
card_rarity('panther warriors'/'VIS', 'Common').
card_artist('panther warriors'/'VIS', 'Cecil Fernando').
card_flavor_text('panther warriors'/'VIS', '\"When you run you\'re graceful and swift, sleek as a powerful panther.\"\n—\"Love Song of Night and Day\"').
card_multiverse_id('panther warriors'/'VIS', '3672').

card_in_set('parapet', 'VIS').
card_original_type('parapet'/'VIS', 'Enchantment').
card_original_text('parapet'/'VIS', 'You may choose to play Parapet as an instant; if you do, bury it at end of turn.\nAll creatures you control get +0/+1.').
card_first_print('parapet', 'VIS').
card_image_name('parapet'/'VIS', 'parapet').
card_uid('parapet'/'VIS', 'VIS:Parapet:parapet').
card_rarity('parapet'/'VIS', 'Common').
card_artist('parapet'/'VIS', 'Mark Poole').
card_flavor_text('parapet'/'VIS', 'There is no wall that can\'t be improved by making it taller.').
card_multiverse_id('parapet'/'VIS', '3721').

card_in_set('peace talks', 'VIS').
card_original_type('peace talks'/'VIS', 'Sorcery').
card_original_text('peace talks'/'VIS', 'During this turn and the next one, players cannot declare an attack and cannot play spells or abilities that target any permanent or player.').
card_image_name('peace talks'/'VIS', 'peace talks').
card_uid('peace talks'/'VIS', 'VIS:Peace Talks:peace talks').
card_rarity('peace talks'/'VIS', 'Uncommon').
card_artist('peace talks'/'VIS', 'Roger Raupp').
card_flavor_text('peace talks'/'VIS', '\"By the tongues of a thousand serpents, this time I do not lie.\"\n—Ahmahz il Kin, Suq\'Ata diplomat').
card_multiverse_id('peace talks'/'VIS', '3722').

card_in_set('phyrexian marauder', 'VIS').
card_original_type('phyrexian marauder'/'VIS', 'Artifact Creature').
card_original_text('phyrexian marauder'/'VIS', 'Phryexian Marauder comes into play with X +1/+1 counters on it.\nPhyrexian Marauder cannot block.\nPhyrexian Marauder cannot attack unless you pay {1} for each +1/+1 counter on it.').
card_first_print('phyrexian marauder', 'VIS').
card_image_name('phyrexian marauder'/'VIS', 'phyrexian marauder').
card_uid('phyrexian marauder'/'VIS', 'VIS:Phyrexian Marauder:phyrexian marauder').
card_rarity('phyrexian marauder'/'VIS', 'Rare').
card_artist('phyrexian marauder'/'VIS', 'David Seeley').
card_multiverse_id('phyrexian marauder'/'VIS', '3599').

card_in_set('phyrexian walker', 'VIS').
card_original_type('phyrexian walker'/'VIS', 'Artifact Creature').
card_original_text('phyrexian walker'/'VIS', '').
card_first_print('phyrexian walker', 'VIS').
card_image_name('phyrexian walker'/'VIS', 'phyrexian walker').
card_uid('phyrexian walker'/'VIS', 'VIS:Phyrexian Walker:phyrexian walker').
card_rarity('phyrexian walker'/'VIS', 'Common').
card_artist('phyrexian walker'/'VIS', 'Bryan Talbot').
card_flavor_text('phyrexian walker'/'VIS', '\"I have heard terrible tales of black rains, ashen fields, and metal that screams. I have consoled myself that the tales were a myth of some fevered mind. But today I saw a walker—and now I fear the truth.\"\n—Kasib Ibn Naji, Letters').
card_multiverse_id('phyrexian walker'/'VIS', '3600').

card_in_set('pillar tombs of aku', 'VIS').
card_original_type('pillar tombs of aku'/'VIS', 'Enchant World').
card_original_text('pillar tombs of aku'/'VIS', 'During each player\'s upkeep, that player sacrifices a creature, or that player loses 5 life and you bury Pillar Tombs of Aku.').
card_first_print('pillar tombs of aku', 'VIS').
card_image_name('pillar tombs of aku'/'VIS', 'pillar tombs of aku').
card_uid('pillar tombs of aku'/'VIS', 'VIS:Pillar Tombs of Aku:pillar tombs of aku').
card_rarity('pillar tombs of aku'/'VIS', 'Rare').
card_artist('pillar tombs of aku'/'VIS', 'Terese Nielsen').
card_flavor_text('pillar tombs of aku'/'VIS', '\"Aku, City of Hidden Graves / Aku, City of Lost Kings / Aku, welcome me.\"\n—Pillar tomb inscription').
card_multiverse_id('pillar tombs of aku'/'VIS', '3624').

card_in_set('prosperity', 'VIS').
card_original_type('prosperity'/'VIS', 'Sorcery').
card_original_text('prosperity'/'VIS', 'Each player draws X cards.').
card_first_print('prosperity', 'VIS').
card_image_name('prosperity'/'VIS', 'prosperity').
card_uid('prosperity'/'VIS', 'VIS:Prosperity:prosperity').
card_rarity('prosperity'/'VIS', 'Uncommon').
card_artist('prosperity'/'VIS', 'Dan Frazier').
card_flavor_text('prosperity'/'VIS', '\"Wealth is a good thing, compared to poverty—your food is better, your robes are softer, and your companions have bathed more recently.\"\n—Kipkemboi, Kukemssa pirate').
card_multiverse_id('prosperity'/'VIS', '3647').

card_in_set('pygmy hippo', 'VIS').
card_original_type('pygmy hippo'/'VIS', 'Summon — Hippopotamus').
card_original_text('pygmy hippo'/'VIS', 'If Pygmy Hippo attacks and is not blocked, you may choose to have it deal no combat damage this turn. If you do, defending player draws all mana from his or her lands and then his or her mana pool is emptied. After combat, add an equal amount of colorless mana to your mana pool.').
card_first_print('pygmy hippo', 'VIS').
card_image_name('pygmy hippo'/'VIS', 'pygmy hippo').
card_uid('pygmy hippo'/'VIS', 'VIS:Pygmy Hippo:pygmy hippo').
card_rarity('pygmy hippo'/'VIS', 'Rare').
card_artist('pygmy hippo'/'VIS', 'Steve White').
card_multiverse_id('pygmy hippo'/'VIS', '3740').

card_in_set('python', 'VIS').
card_original_type('python'/'VIS', 'Summon — Python').
card_original_text('python'/'VIS', '').
card_first_print('python', 'VIS').
card_image_name('python'/'VIS', 'python').
card_uid('python'/'VIS', 'VIS:Python:python').
card_rarity('python'/'VIS', 'Common').
card_artist('python'/'VIS', 'Steve White').
card_flavor_text('python'/'VIS', '\"How can you claim the gods are merciless when they robbed the snake of its limbs to give the other creatures a sporting chance?\"\n—Hakim, Loreweaver').
card_multiverse_id('python'/'VIS', '3625').

card_in_set('quicksand', 'VIS').
card_original_type('quicksand'/'VIS', 'Land').
card_original_text('quicksand'/'VIS', '{T}: Add one colorless mana to your mana pool.\n{T}, Sacrifice Quicksand: Target attacking creature without flying gets -1/-2 until end of turn.').
card_first_print('quicksand', 'VIS').
card_image_name('quicksand'/'VIS', 'quicksand').
card_uid('quicksand'/'VIS', 'VIS:Quicksand:quicksand').
card_rarity('quicksand'/'VIS', 'Uncommon').
card_artist('quicksand'/'VIS', 'Roger Raupp').
card_multiverse_id('quicksand'/'VIS', '3754').

card_in_set('quirion druid', 'VIS').
card_original_type('quirion druid'/'VIS', 'Summon — Druid').
card_original_text('quirion druid'/'VIS', '{G}, {T}: Target land becomes a 2/2 green creature permanently. That land still counts as a land.').
card_first_print('quirion druid', 'VIS').
card_image_name('quirion druid'/'VIS', 'quirion druid').
card_uid('quirion druid'/'VIS', 'VIS:Quirion Druid:quirion druid').
card_rarity('quirion druid'/'VIS', 'Rare').
card_artist('quirion druid'/'VIS', 'John Matson').
card_flavor_text('quirion druid'/'VIS', '\"The land has been gracious enough to let you tread upon her for years. That privilege is about to end.\"\n—Liefellen, Quirion exarch').
card_multiverse_id('quirion druid'/'VIS', '3673').

card_in_set('quirion ranger', 'VIS').
card_original_type('quirion ranger'/'VIS', 'Summon — Elf').
card_original_text('quirion ranger'/'VIS', 'Return a forest you control to owner\'s hand: Untap target creature. Use this ability only once each turn.').
card_first_print('quirion ranger', 'VIS').
card_image_name('quirion ranger'/'VIS', 'quirion ranger').
card_uid('quirion ranger'/'VIS', 'VIS:Quirion Ranger:quirion ranger').
card_rarity('quirion ranger'/'VIS', 'Common').
card_artist('quirion ranger'/'VIS', 'Tom Kyffin').
card_flavor_text('quirion ranger'/'VIS', '\"Respect the earth, for it will one day be your shield and another day your blanket.\"\n—Liefellen, Quirion exarch').
card_multiverse_id('quirion ranger'/'VIS', '3674').

card_in_set('raging gorilla', 'VIS').
card_original_type('raging gorilla'/'VIS', 'Summon — Gorilla').
card_original_text('raging gorilla'/'VIS', 'If Raging Gorilla blocks or is blocked, it gets +2/-2 until end of turn.').
card_first_print('raging gorilla', 'VIS').
card_image_name('raging gorilla'/'VIS', 'raging gorilla').
card_uid('raging gorilla'/'VIS', 'VIS:Raging Gorilla:raging gorilla').
card_rarity('raging gorilla'/'VIS', 'Common').
card_artist('raging gorilla'/'VIS', 'Tom Kyffin').
card_flavor_text('raging gorilla'/'VIS', '\"Every temper tantrum makes another angry gorilla.\"\n—Femeref children\'s myth').
card_multiverse_id('raging gorilla'/'VIS', '3697').

card_in_set('rainbow efreet', 'VIS').
card_original_type('rainbow efreet'/'VIS', 'Summon — Efreet').
card_original_text('rainbow efreet'/'VIS', 'Flying\n{U}{U}: Phase out').
card_first_print('rainbow efreet', 'VIS').
card_image_name('rainbow efreet'/'VIS', 'rainbow efreet').
card_uid('rainbow efreet'/'VIS', 'VIS:Rainbow Efreet:rainbow efreet').
card_rarity('rainbow efreet'/'VIS', 'Rare').
card_artist('rainbow efreet'/'VIS', 'Nathalie Hertz').
card_flavor_text('rainbow efreet'/'VIS', '\"A beauty made more so by its fleeting visitations.\"\n—Teferi').
card_multiverse_id('rainbow efreet'/'VIS', '3648').

card_in_set('relentless assault', 'VIS').
card_original_type('relentless assault'/'VIS', 'Sorcery').
card_original_text('relentless assault'/'VIS', 'Untap all creatures that attacked this turn. You may declare an additional attack during your main phase this turn.').
card_first_print('relentless assault', 'VIS').
card_image_name('relentless assault'/'VIS', 'relentless assault').
card_uid('relentless assault'/'VIS', 'VIS:Relentless Assault:relentless assault').
card_rarity('relentless assault'/'VIS', 'Rare').
card_artist('relentless assault'/'VIS', 'G. Darrow & I. Rabarot').
card_flavor_text('relentless assault'/'VIS', '\"Flog and Squee / Up the tree / See the army / Flee, flee, flee.\"\n—Goblin nursery rhyme/war cry').
card_multiverse_id('relentless assault'/'VIS', '3698').

card_in_set('relic ward', 'VIS').
card_original_type('relic ward'/'VIS', 'Enchant Artifact').
card_original_text('relic ward'/'VIS', 'You may choose to play Relic Ward as an instant; if you do, bury it at end of turn.\nEnchanted artifact cannot be the target of spells or effects.').
card_first_print('relic ward', 'VIS').
card_image_name('relic ward'/'VIS', 'relic ward').
card_uid('relic ward'/'VIS', 'VIS:Relic Ward:relic ward').
card_rarity('relic ward'/'VIS', 'Uncommon').
card_artist('relic ward'/'VIS', 'John Coulthart').
card_flavor_text('relic ward'/'VIS', 'Asmira\'s prayers alone should have freed Mangara from the amber prison . . .').
card_multiverse_id('relic ward'/'VIS', '3723').

card_in_set('remedy', 'VIS').
card_original_type('remedy'/'VIS', 'Instant').
card_original_text('remedy'/'VIS', 'Prevent up to 5 damage total to any number of creatures and/or players.').
card_first_print('remedy', 'VIS').
card_image_name('remedy'/'VIS', 'remedy').
card_uid('remedy'/'VIS', 'VIS:Remedy:remedy').
card_rarity('remedy'/'VIS', 'Common').
card_artist('remedy'/'VIS', 'Zina Saunders').
card_flavor_text('remedy'/'VIS', '\"These things will protect you while I\'m gone, remind you of my love for you.\"\n—\"Love Song of Night and Day\"').
card_multiverse_id('remedy'/'VIS', '3724').

card_in_set('resistance fighter', 'VIS').
card_original_type('resistance fighter'/'VIS', 'Summon — Soldier').
card_original_text('resistance fighter'/'VIS', 'Sacrifice Resistance Fighter: Target creature deals no combat damage this turn.').
card_first_print('resistance fighter', 'VIS').
card_image_name('resistance fighter'/'VIS', 'resistance fighter').
card_uid('resistance fighter'/'VIS', 'VIS:Resistance Fighter:resistance fighter').
card_rarity('resistance fighter'/'VIS', 'Common').
card_artist('resistance fighter'/'VIS', 'Cecil Fernando').
card_flavor_text('resistance fighter'/'VIS', '\"My soldiers fought without hesitation, died without doubt.\"\n—Sidar Jabari').
card_multiverse_id('resistance fighter'/'VIS', '3725').

card_in_set('retribution of the meek', 'VIS').
card_original_type('retribution of the meek'/'VIS', 'Sorcery').
card_original_text('retribution of the meek'/'VIS', 'Bury all creatures with power 4 or greater.').
card_first_print('retribution of the meek', 'VIS').
card_image_name('retribution of the meek'/'VIS', 'retribution of the meek').
card_uid('retribution of the meek'/'VIS', 'VIS:Retribution of the Meek:retribution of the meek').
card_rarity('retribution of the meek'/'VIS', 'Rare').
card_artist('retribution of the meek'/'VIS', 'Nathalie Hertz').
card_flavor_text('retribution of the meek'/'VIS', '\"Tread upon the meek, and they shall wound your feet and make you crawl.\"\n—Asmira, Holy Avenger').
card_multiverse_id('retribution of the meek'/'VIS', '3726').

card_in_set('righteous aura', 'VIS').
card_original_type('righteous aura'/'VIS', 'Enchantment').
card_original_text('righteous aura'/'VIS', '{W}, Pay 2 life: Prevent all damage to you from any one source.').
card_first_print('righteous aura', 'VIS').
card_image_name('righteous aura'/'VIS', 'righteous aura').
card_uid('righteous aura'/'VIS', 'VIS:Righteous Aura:righteous aura').
card_rarity('righteous aura'/'VIS', 'Common').
card_artist('righteous aura'/'VIS', 'Jeff Miracola').
card_flavor_text('righteous aura'/'VIS', '\"Good is not a ‘thing.\' You can neither touch nor own it. No, Good is a vision we all share and strive to make real.\"\n—Asmira, Holy Avenger').
card_multiverse_id('righteous aura'/'VIS', '3727').

card_in_set('righteous war', 'VIS').
card_original_type('righteous war'/'VIS', 'Enchantment').
card_original_text('righteous war'/'VIS', 'All white creatures you control gain protection from black.\nAll black creatures you control gain protection from white.').
card_first_print('righteous war', 'VIS').
card_image_name('righteous war'/'VIS', 'righteous war').
card_uid('righteous war'/'VIS', 'VIS:Righteous War:righteous war').
card_rarity('righteous war'/'VIS', 'Rare').
card_artist('righteous war'/'VIS', 'Ian Miller').
card_flavor_text('righteous war'/'VIS', '\"This is a war without neutrality.\"\n—Asmira, Holy Avenger').
card_multiverse_id('righteous war'/'VIS', '3741').

card_in_set('river boa', 'VIS').
card_original_type('river boa'/'VIS', 'Summon — Snake').
card_original_text('river boa'/'VIS', 'Islandwalk (If defending player controls an island, this creature is unblockable.)\n{G} Regenerate').
card_first_print('river boa', 'VIS').
card_image_name('river boa'/'VIS', 'river boa').
card_uid('river boa'/'VIS', 'VIS:River Boa:river boa').
card_rarity('river boa'/'VIS', 'Common').
card_artist('river boa'/'VIS', 'Steve White').
card_flavor_text('river boa'/'VIS', '\"But no one heard the snake\'s gentle hiss for peace over the elephant\'s trumpeting of war.\"\n—Afari, Tales').
card_multiverse_id('river boa'/'VIS', '3675').

card_in_set('rock slide', 'VIS').
card_original_type('rock slide'/'VIS', 'Instant').
card_original_text('rock slide'/'VIS', 'Rock Slide deals X damage, divided any way you choose, among any number of target attacking or blocking creatures without flying.').
card_first_print('rock slide', 'VIS').
card_image_name('rock slide'/'VIS', 'rock slide').
card_uid('rock slide'/'VIS', 'VIS:Rock Slide:rock slide').
card_rarity('rock slide'/'VIS', 'Common').
card_artist('rock slide'/'VIS', 'Mike Kerr').
card_flavor_text('rock slide'/'VIS', '\"Good ol\' rock. Nothing beats rock.\"\n—Rhirhok, goblin archer').
card_multiverse_id('rock slide'/'VIS', '3699').

card_in_set('rowen', 'VIS').
card_original_type('rowen'/'VIS', 'Enchantment').
card_original_text('rowen'/'VIS', 'During your draw phase, reveal the first card you draw to all players. If that card is a basic land, draw a card.').
card_first_print('rowen', 'VIS').
card_image_name('rowen'/'VIS', 'rowen').
card_uid('rowen'/'VIS', 'VIS:Rowen:rowen').
card_rarity('rowen'/'VIS', 'Rare').
card_artist('rowen'/'VIS', 'Jon J. Muth').
card_flavor_text('rowen'/'VIS', '\"I\'ve dreamt of a second harvest but fear I will not see it.\"\n—Asmira, Holy Avenger').
card_multiverse_id('rowen'/'VIS', '3676').

card_in_set('sands of time', 'VIS').
card_original_type('sands of time'/'VIS', 'Artifact').
card_original_text('sands of time'/'VIS', 'Each player skips his or her untap phase.\nAt the beginning of each player\'s turn, untap each tapped artifact, creature, and land he or she controls and tap each untapped artifact, creature, and land he or she controls.').
card_first_print('sands of time', 'VIS').
card_image_name('sands of time'/'VIS', 'sands of time').
card_uid('sands of time'/'VIS', 'VIS:Sands of Time:sands of time').
card_rarity('sands of time'/'VIS', 'Rare').
card_artist('sands of time'/'VIS', 'Paul Lee').
card_flavor_text('sands of time'/'VIS', '\"But once, with a magician\'s help, Time was stopped and Day stood still.\"\n—\"Love Song of Night and Day\"').
card_multiverse_id('sands of time'/'VIS', '3601').

card_in_set('scalebane\'s elite', 'VIS').
card_original_type('scalebane\'s elite'/'VIS', 'Summon — Soldiers').
card_original_text('scalebane\'s elite'/'VIS', 'Protection from black').
card_first_print('scalebane\'s elite', 'VIS').
card_image_name('scalebane\'s elite'/'VIS', 'scalebane\'s elite').
card_uid('scalebane\'s elite'/'VIS', 'VIS:Scalebane\'s Elite:scalebane\'s elite').
card_rarity('scalebane\'s elite'/'VIS', 'Uncommon').
card_artist('scalebane\'s elite'/'VIS', 'Steve Luke').
card_flavor_text('scalebane\'s elite'/'VIS', '\"With Rashida\'s blades, my guiding vision and the luck of the blessed, today we will free Mangara!\"\n—Asmira, Holy Avenger').
card_multiverse_id('scalebane\'s elite'/'VIS', '3742').

card_in_set('shimmering efreet', 'VIS').
card_original_type('shimmering efreet'/'VIS', 'Summon — Efreet').
card_original_text('shimmering efreet'/'VIS', 'Flying, phasing\nWhen Shimmering Efreet phases in, target creature phases out.').
card_first_print('shimmering efreet', 'VIS').
card_image_name('shimmering efreet'/'VIS', 'shimmering efreet').
card_uid('shimmering efreet'/'VIS', 'VIS:Shimmering Efreet:shimmering efreet').
card_rarity('shimmering efreet'/'VIS', 'Uncommon').
card_artist('shimmering efreet'/'VIS', 'Thomas Gianni').
card_flavor_text('shimmering efreet'/'VIS', 'Life or death: which is the illusion?\"\n—Naimah, Femeref philosopher').
card_multiverse_id('shimmering efreet'/'VIS', '3649').

card_in_set('shrieking drake', 'VIS').
card_original_type('shrieking drake'/'VIS', 'Summon — Drake').
card_original_text('shrieking drake'/'VIS', 'Flying\nWhen Shrieking Drake comes into play, return a creature you control to owner\'s hand.').
card_first_print('shrieking drake', 'VIS').
card_image_name('shrieking drake'/'VIS', 'shrieking drake').
card_uid('shrieking drake'/'VIS', 'VIS:Shrieking Drake:shrieking drake').
card_rarity('shrieking drake'/'VIS', 'Common').
card_artist('shrieking drake'/'VIS', 'Ian Miller').
card_flavor_text('shrieking drake'/'VIS', '\"Kaervek believes the drakes\' cries herald his victory; in truth, they mourn aloud for his impending demise.\"\n—Teferi').
card_multiverse_id('shrieking drake'/'VIS', '3650').

card_in_set('simoon', 'VIS').
card_original_type('simoon'/'VIS', 'Instant').
card_original_text('simoon'/'VIS', 'Simoon deals 1 damage to each creature target opponent controls.').
card_first_print('simoon', 'VIS').
card_image_name('simoon'/'VIS', 'simoon').
card_uid('simoon'/'VIS', 'VIS:Simoon:simoon').
card_rarity('simoon'/'VIS', 'Uncommon').
card_artist('simoon'/'VIS', 'Randy Gallegos').
card_flavor_text('simoon'/'VIS', '\"The black-and-white sacred monkey holds her children to her, and waits.\"\n—\"Love Song of Night and Day\"').
card_multiverse_id('simoon'/'VIS', '3743').

card_in_set('sisay\'s ring', 'VIS').
card_original_type('sisay\'s ring'/'VIS', 'Artifact').
card_original_text('sisay\'s ring'/'VIS', '{T}: Add two colorless mana to your mana pool. Play this ability as a mana source.').
card_first_print('sisay\'s ring', 'VIS').
card_image_name('sisay\'s ring'/'VIS', 'sisay\'s ring').
card_uid('sisay\'s ring'/'VIS', 'VIS:Sisay\'s Ring:sisay\'s ring').
card_rarity('sisay\'s ring'/'VIS', 'Common').
card_artist('sisay\'s ring'/'VIS', 'Donato Giancola').
card_flavor_text('sisay\'s ring'/'VIS', '\"With this ring, you have friends in worlds you\'ve never heard of.\"\n—Sisay, Captain of the Weatherlight').
card_multiverse_id('sisay\'s ring'/'VIS', '3602').

card_in_set('snake basket', 'VIS').
card_original_type('snake basket'/'VIS', 'Artifact').
card_original_text('snake basket'/'VIS', '{X}, Sacrifice Snake Basket: Put X Cobra tokens into play. Treat these tokens as 1/1 green creatures. Play this ability as a sorcery.').
card_first_print('snake basket', 'VIS').
card_image_name('snake basket'/'VIS', 'snake basket').
card_uid('snake basket'/'VIS', 'VIS:Snake Basket:snake basket').
card_rarity('snake basket'/'VIS', 'Rare').
card_artist('snake basket'/'VIS', 'Roger Raupp').
card_flavor_text('snake basket'/'VIS', '\"Uh, does anyone have a flute?\"\n—Rana, Suq\'Ata market fool').
card_multiverse_id('snake basket'/'VIS', '3603').

card_in_set('solfatara', 'VIS').
card_original_type('solfatara'/'VIS', 'Instant').
card_original_text('solfatara'/'VIS', 'Target player cannot play any land cards this turn. Draw a card at the beginning of the next turn\'s upkeep.').
card_first_print('solfatara', 'VIS').
card_image_name('solfatara'/'VIS', 'solfatara').
card_uid('solfatara'/'VIS', 'VIS:Solfatara:solfatara').
card_rarity('solfatara'/'VIS', 'Common').
card_artist('solfatara'/'VIS', 'Omaha Perez').
card_flavor_text('solfatara'/'VIS', '\"A burst of hot air tossed the moon into the air, and the moon liked it up there so much it never needed the ground again.\"\n—Azeworai, \"Glitter Moon\"').
card_multiverse_id('solfatara'/'VIS', '3700').

card_in_set('song of blood', 'VIS').
card_original_type('song of blood'/'VIS', 'Sorcery').
card_original_text('song of blood'/'VIS', 'Put the top four cards from your library into your graveyard. For each creature card put into your graveyard in this way, all creatures that attack this turn get +1/+0 until end of turn.').
card_first_print('song of blood', 'VIS').
card_image_name('song of blood'/'VIS', 'song of blood').
card_uid('song of blood'/'VIS', 'VIS:Song of Blood:song of blood').
card_rarity('song of blood'/'VIS', 'Common').
card_artist('song of blood'/'VIS', 'Eric Peterson').
card_flavor_text('song of blood'/'VIS', 'Purraj sang slaughter and danced death.').
card_multiverse_id('song of blood'/'VIS', '3701').

card_in_set('spider climb', 'VIS').
card_original_type('spider climb'/'VIS', 'Enchant Creature').
card_original_text('spider climb'/'VIS', 'You may choose to play Spider Climb as an instant; if you do, bury it at end of turn.\nEnchanted creature gets +0/+3 and can block creatures with flying.').
card_first_print('spider climb', 'VIS').
card_image_name('spider climb'/'VIS', 'spider climb').
card_uid('spider climb'/'VIS', 'VIS:Spider Climb:spider climb').
card_rarity('spider climb'/'VIS', 'Common').
card_artist('spider climb'/'VIS', 'Ron Spencer').
card_multiverse_id('spider climb'/'VIS', '3677').

card_in_set('spitting drake', 'VIS').
card_original_type('spitting drake'/'VIS', 'Summon — Drake').
card_original_text('spitting drake'/'VIS', 'Flying\n{R} +1/+0 until end of turn. You cannot spend more than {R} in this way each turn.').
card_first_print('spitting drake', 'VIS').
card_image_name('spitting drake'/'VIS', 'spitting drake').
card_uid('spitting drake'/'VIS', 'VIS:Spitting Drake:spitting drake').
card_rarity('spitting drake'/'VIS', 'Uncommon').
card_artist('spitting drake'/'VIS', 'G. Darrow & I. Rabarot').
card_flavor_text('spitting drake'/'VIS', 'It prefers its meals cooked.').
card_multiverse_id('spitting drake'/'VIS', '3702').

card_in_set('squandered resources', 'VIS').
card_original_type('squandered resources'/'VIS', 'Enchantment').
card_original_text('squandered resources'/'VIS', 'Sacrifice a land: Add to your mana pool one mana of any type the sacrificed land could produce. Play this ability as a mana source.').
card_first_print('squandered resources', 'VIS').
card_image_name('squandered resources'/'VIS', 'squandered resources').
card_uid('squandered resources'/'VIS', 'VIS:Squandered Resources:squandered resources').
card_rarity('squandered resources'/'VIS', 'Rare').
card_artist('squandered resources'/'VIS', 'Romas Kukalis').
card_flavor_text('squandered resources'/'VIS', '\"He traded sand for skins, skins for gold, gold for life. In the end, he traded life for sand.\"\n—Afari, Tales').
card_multiverse_id('squandered resources'/'VIS', '3744').

card_in_set('stampeding wildebeests', 'VIS').
card_original_type('stampeding wildebeests'/'VIS', 'Summon — Wildebeests').
card_original_text('stampeding wildebeests'/'VIS', 'Trample\nDuring your upkeep, return a green creature you control to owner\'s hand.').
card_first_print('stampeding wildebeests', 'VIS').
card_image_name('stampeding wildebeests'/'VIS', 'stampeding wildebeests').
card_uid('stampeding wildebeests'/'VIS', 'VIS:Stampeding Wildebeests:stampeding wildebeests').
card_rarity('stampeding wildebeests'/'VIS', 'Uncommon').
card_artist('stampeding wildebeests'/'VIS', 'Randy Gallegos').
card_flavor_text('stampeding wildebeests'/'VIS', 'Prayers for rain are answered with the thunder of hooves.').
card_multiverse_id('stampeding wildebeests'/'VIS', '3678').

card_in_set('suleiman\'s legacy', 'VIS').
card_original_type('suleiman\'s legacy'/'VIS', 'Enchantment').
card_original_text('suleiman\'s legacy'/'VIS', 'When Suleiman\'s Legacy comes into play, bury all Djinns and Efreets.\nWhenever a Djinn or Efreet comes into play, bury it.').
card_first_print('suleiman\'s legacy', 'VIS').
card_image_name('suleiman\'s legacy'/'VIS', 'suleiman\'s legacy').
card_uid('suleiman\'s legacy'/'VIS', 'VIS:Suleiman\'s Legacy:suleiman\'s legacy').
card_rarity('suleiman\'s legacy'/'VIS', 'Rare').
card_artist('suleiman\'s legacy'/'VIS', 'Kaja Foglio').
card_flavor_text('suleiman\'s legacy'/'VIS', 'With Suleiman\'s ascension to power, the djinn and efreets of ancient Rabiah learned humility.').
card_multiverse_id('suleiman\'s legacy'/'VIS', '3745').

card_in_set('summer bloom', 'VIS').
card_original_type('summer bloom'/'VIS', 'Sorcery').
card_original_text('summer bloom'/'VIS', 'You may play up to three additional lands this turn.').
card_first_print('summer bloom', 'VIS').
card_image_name('summer bloom'/'VIS', 'summer bloom').
card_uid('summer bloom'/'VIS', 'VIS:Summer Bloom:summer bloom').
card_rarity('summer bloom'/'VIS', 'Uncommon').
card_artist('summer bloom'/'VIS', 'Nicola Leonard').
card_flavor_text('summer bloom'/'VIS', '\"Our love is like the river in the summer season of long rains. / For a little while it spilled its banks, flooding the crops in the fields.\"\n—\"Love Song of Night and Day\"').
card_multiverse_id('summer bloom'/'VIS', '3679').

card_in_set('sun clasp', 'VIS').
card_original_type('sun clasp'/'VIS', 'Enchant Creature').
card_original_text('sun clasp'/'VIS', 'Enchanted creature gets +1/+3.\n{W} Return enchanted creature to owner\'s hand.').
card_first_print('sun clasp', 'VIS').
card_image_name('sun clasp'/'VIS', 'sun clasp').
card_uid('sun clasp'/'VIS', 'VIS:Sun Clasp:sun clasp').
card_rarity('sun clasp'/'VIS', 'Common').
card_artist('sun clasp'/'VIS', 'John Coulthart').
card_flavor_text('sun clasp'/'VIS', '\"And darkness shall be cast from me\nFor my soul resides in the Sun.\"\n—Femeref dirge').
card_multiverse_id('sun clasp'/'VIS', '3728').

card_in_set('suq\'ata assassin', 'VIS').
card_original_type('suq\'ata assassin'/'VIS', 'Summon — Assassin').
card_original_text('suq\'ata assassin'/'VIS', 'Suq\'Ata Assassin cannot be blocked except by artifact or black creatures.\nIf Suq\'Ata Assassin attacks and is not blocked, defending player gets a poison counter. If any player has ten or more poison counters, he or she loses the game.').
card_first_print('suq\'ata assassin', 'VIS').
card_image_name('suq\'ata assassin'/'VIS', 'suq\'ata assassin').
card_uid('suq\'ata assassin'/'VIS', 'VIS:Suq\'Ata Assassin:suq\'ata assassin').
card_rarity('suq\'ata assassin'/'VIS', 'Uncommon').
card_artist('suq\'ata assassin'/'VIS', 'Gary Gianni').
card_multiverse_id('suq\'ata assassin'/'VIS', '3626').

card_in_set('suq\'ata lancer', 'VIS').
card_original_type('suq\'ata lancer'/'VIS', 'Summon — Knight').
card_original_text('suq\'ata lancer'/'VIS', 'Flanking (Whenever a creature without flanking is assigned to block this creature, the blocking creature gets -1/-1 until end of turn.)\nSuq\'Ata Lancer is unaffected by summoning sickness.').
card_first_print('suq\'ata lancer', 'VIS').
card_image_name('suq\'ata lancer'/'VIS', 'suq\'ata lancer').
card_uid('suq\'ata lancer'/'VIS', 'VIS:Suq\'Ata Lancer:suq\'ata lancer').
card_rarity('suq\'ata lancer'/'VIS', 'Common').
card_artist('suq\'ata lancer'/'VIS', 'Jeff Miracola').
card_flavor_text('suq\'ata lancer'/'VIS', '\"Never stop \'til you see your lance come out the other side.\"\n—Telim\'Tor').
card_multiverse_id('suq\'ata lancer'/'VIS', '3703').

card_in_set('talruum champion', 'VIS').
card_original_type('talruum champion'/'VIS', 'Summon — Minotaur').
card_original_text('talruum champion'/'VIS', 'First strike\nWhenever Talruum Champion blocks or is blocked by any creature, that creature loses first strike until end of turn.').
card_first_print('talruum champion', 'VIS').
card_image_name('talruum champion'/'VIS', 'talruum champion').
card_uid('talruum champion'/'VIS', 'VIS:Talruum Champion:talruum champion').
card_rarity('talruum champion'/'VIS', 'Common').
card_artist('talruum champion'/'VIS', 'Pete Venters').
card_flavor_text('talruum champion'/'VIS', 'In the Talruum language, there is no word for \"surprised.\"').
card_multiverse_id('talruum champion'/'VIS', '3704').

card_in_set('talruum piper', 'VIS').
card_original_type('talruum piper'/'VIS', 'Summon — Minotaur').
card_original_text('talruum piper'/'VIS', 'All creatures with flying able to block Talruum Piper do so.').
card_first_print('talruum piper', 'VIS').
card_image_name('talruum piper'/'VIS', 'talruum piper').
card_uid('talruum piper'/'VIS', 'VIS:Talruum Piper:talruum piper').
card_rarity('talruum piper'/'VIS', 'Uncommon').
card_artist('talruum piper'/'VIS', 'Pete Venters').
card_flavor_text('talruum piper'/'VIS', 'When the Talruum began to play, the dragons fell from the sky to squash the obnoxious noise.').
card_multiverse_id('talruum piper'/'VIS', '3705').

card_in_set('tar pit warrior', 'VIS').
card_original_type('tar pit warrior'/'VIS', 'Summon — Cyclops').
card_original_text('tar pit warrior'/'VIS', 'If Tar Pit Warrior is the target of a spell or effect, bury Tar Pit Warrior.').
card_first_print('tar pit warrior', 'VIS').
card_image_name('tar pit warrior'/'VIS', 'tar pit warrior').
card_uid('tar pit warrior'/'VIS', 'VIS:Tar Pit Warrior:tar pit warrior').
card_rarity('tar pit warrior'/'VIS', 'Common').
card_artist('tar pit warrior'/'VIS', 'George Pratt').
card_flavor_text('tar pit warrior'/'VIS', '\"The cyclops shrugged off savage blows, but casual insults made him weep.\"\n—Azeworai, \"The Cyclops who Couldn\'t\"').
card_multiverse_id('tar pit warrior'/'VIS', '3627').

card_in_set('teferi\'s honor guard', 'VIS').
card_original_type('teferi\'s honor guard'/'VIS', 'Summon — Knight').
card_original_text('teferi\'s honor guard'/'VIS', 'Flanking (Whenever a creature without flanking is assigned to block this creature, the blocking creature gets -1/-1 until end of turn.)\n{U}{U} Phase out').
card_first_print('teferi\'s honor guard', 'VIS').
card_image_name('teferi\'s honor guard'/'VIS', 'teferi\'s honor guard').
card_uid('teferi\'s honor guard'/'VIS', 'VIS:Teferi\'s Honor Guard:teferi\'s honor guard').
card_rarity('teferi\'s honor guard'/'VIS', 'Uncommon').
card_artist('teferi\'s honor guard'/'VIS', 'Cecil Fernando').
card_flavor_text('teferi\'s honor guard'/'VIS', '\"They may seem ceremonial, but their swords are still sharp.\"\n—Kipkemboi, Kukemssa pirate').
card_multiverse_id('teferi\'s honor guard'/'VIS', '3729').

card_in_set('teferi\'s puzzle box', 'VIS').
card_original_type('teferi\'s puzzle box'/'VIS', 'Artifact').
card_original_text('teferi\'s puzzle box'/'VIS', 'During each player\'s draw phase, that player counts the cards in his or her hand, puts those cards on the bottom of his or her library, and then draws that number of cards.').
card_first_print('teferi\'s puzzle box', 'VIS').
card_image_name('teferi\'s puzzle box'/'VIS', 'teferi\'s puzzle box').
card_uid('teferi\'s puzzle box'/'VIS', 'VIS:Teferi\'s Puzzle Box:teferi\'s puzzle box').
card_rarity('teferi\'s puzzle box'/'VIS', 'Rare').
card_artist('teferi\'s puzzle box'/'VIS', 'Kaja Foglio').
card_multiverse_id('teferi\'s puzzle box'/'VIS', '3604').

card_in_set('teferi\'s realm', 'VIS').
card_original_type('teferi\'s realm'/'VIS', 'Enchant World').
card_original_text('teferi\'s realm'/'VIS', 'At the beginning of each player\'s upkeep, that player chooses artifacts, creatures, lands, or global enchantments. All cards of that type phase out.').
card_first_print('teferi\'s realm', 'VIS').
card_image_name('teferi\'s realm'/'VIS', 'teferi\'s realm').
card_uid('teferi\'s realm'/'VIS', 'VIS:Teferi\'s Realm:teferi\'s realm').
card_rarity('teferi\'s realm'/'VIS', 'Rare').
card_artist('teferi\'s realm'/'VIS', 'Alan Rabinowitz').
card_flavor_text('teferi\'s realm'/'VIS', '\"Fire is dead. Water has killed him.\"\n—From The Stories of Nature').
card_multiverse_id('teferi\'s realm'/'VIS', '3651').

card_in_set('tempest drake', 'VIS').
card_original_type('tempest drake'/'VIS', 'Summon — Drake').
card_original_text('tempest drake'/'VIS', 'Flying\nAttacking does not cause Tempest Drake to tap.').
card_first_print('tempest drake', 'VIS').
card_image_name('tempest drake'/'VIS', 'tempest drake').
card_uid('tempest drake'/'VIS', 'VIS:Tempest Drake:tempest drake').
card_rarity('tempest drake'/'VIS', 'Uncommon').
card_artist('tempest drake'/'VIS', 'Gerry Grace').
card_flavor_text('tempest drake'/'VIS', '\"Its speed will be equaled only by the swiftness of my enemy\'s fall.\"\n—Kaervek').
card_multiverse_id('tempest drake'/'VIS', '3746').

card_in_set('three wishes', 'VIS').
card_original_type('three wishes'/'VIS', 'Instant').
card_original_text('three wishes'/'VIS', 'Take the top three cards from your library, look at them, and set them aside face down. You may play those cards as though they were in your hand. At the beginning of your next turn, bury any of those cards not played.').
card_first_print('three wishes', 'VIS').
card_image_name('three wishes'/'VIS', 'three wishes').
card_uid('three wishes'/'VIS', 'VIS:Three Wishes:three wishes').
card_rarity('three wishes'/'VIS', 'Rare').
card_artist('three wishes'/'VIS', 'George Pratt').
card_multiverse_id('three wishes'/'VIS', '3652').

card_in_set('time and tide', 'VIS').
card_original_type('time and tide'/'VIS', 'Instant').
card_original_text('time and tide'/'VIS', 'All creatures that are phased out phase in and all creatures with phasing phase out.').
card_first_print('time and tide', 'VIS').
card_image_name('time and tide'/'VIS', 'time and tide').
card_uid('time and tide'/'VIS', 'VIS:Time and Tide:time and tide').
card_rarity('time and tide'/'VIS', 'Uncommon').
card_artist('time and tide'/'VIS', 'George Pratt').
card_flavor_text('time and tide'/'VIS', '\"Time may heal all wounds, but what heals time?\"\n—Teferi').
card_multiverse_id('time and tide'/'VIS', '3653').

card_in_set('tin-wing chimera', 'VIS').
card_original_type('tin-wing chimera'/'VIS', 'Artifact Creature').
card_original_text('tin-wing chimera'/'VIS', 'Flying\nTin-Wing Chimera counts as a Chimera.\nSacrifice Tin-Wing Chimera: Put a +2/+2 counter on target Chimera and that Chimera gains flying permanently.').
card_first_print('tin-wing chimera', 'VIS').
card_image_name('tin-wing chimera'/'VIS', 'tin-wing chimera').
card_uid('tin-wing chimera'/'VIS', 'VIS:Tin-Wing Chimera:tin-wing chimera').
card_rarity('tin-wing chimera'/'VIS', 'Uncommon').
card_artist('tin-wing chimera'/'VIS', 'Mike Dringenberg').
card_multiverse_id('tin-wing chimera'/'VIS', '3605').

card_in_set('tithe', 'VIS').
card_original_type('tithe'/'VIS', 'Instant').
card_original_text('tithe'/'VIS', 'Search your library for a plains card. If you control fewer lands than target opponent, you may search your library for an additional plains card. Reveal those cards to all players and put them into your hand. Shuffle your library afterwards.').
card_first_print('tithe', 'VIS').
card_image_name('tithe'/'VIS', 'tithe').
card_uid('tithe'/'VIS', 'VIS:Tithe:tithe').
card_rarity('tithe'/'VIS', 'Rare').
card_artist('tithe'/'VIS', 'Jon J. Muth').
card_multiverse_id('tithe'/'VIS', '3730').

card_in_set('tremor', 'VIS').
card_original_type('tremor'/'VIS', 'Sorcery').
card_original_text('tremor'/'VIS', 'Tremor deals 1 damage to each creature without flying.').
card_first_print('tremor', 'VIS').
card_image_name('tremor'/'VIS', 'tremor').
card_uid('tremor'/'VIS', 'VIS:Tremor:tremor').
card_rarity('tremor'/'VIS', 'Common').
card_artist('tremor'/'VIS', 'Michael Danza').
card_flavor_text('tremor'/'VIS', '\"Where do you run when the earth becomes your enemy?\"\n—Naimah, Femeref philosopher').
card_multiverse_id('tremor'/'VIS', '3706').

card_in_set('triangle of war', 'VIS').
card_original_type('triangle of war'/'VIS', 'Artifact').
card_original_text('triangle of war'/'VIS', '{2}, Sacrifice Triangle of War: Choose target creature you control and target creature an opponent controls. Each creature deals an amount of damage equal to its power to the other.').
card_first_print('triangle of war', 'VIS').
card_image_name('triangle of war'/'VIS', 'triangle of war').
card_uid('triangle of war'/'VIS', 'VIS:Triangle of War:triangle of war').
card_rarity('triangle of war'/'VIS', 'Rare').
card_artist('triangle of war'/'VIS', 'Ian Miller').
card_flavor_text('triangle of war'/'VIS', 'The Zhalfirin war triangle represents a trinity of might, faith, and guile.').
card_multiverse_id('triangle of war'/'VIS', '3606').

card_in_set('uktabi orangutan', 'VIS').
card_original_type('uktabi orangutan'/'VIS', 'Summon — Ape').
card_original_text('uktabi orangutan'/'VIS', 'When Uktabi Orangutan comes into play, destroy target artifact.').
card_image_name('uktabi orangutan'/'VIS', 'uktabi orangutan').
card_uid('uktabi orangutan'/'VIS', 'VIS:Uktabi Orangutan:uktabi orangutan').
card_rarity('uktabi orangutan'/'VIS', 'Uncommon').
card_artist('uktabi orangutan'/'VIS', 'Una Fricker').
card_flavor_text('uktabi orangutan'/'VIS', '\"Is it true that the apes wear furs of gold when they marry?\"\n—Rana, Suq\'Ata market fool').
card_multiverse_id('uktabi orangutan'/'VIS', '3680').

card_in_set('undiscovered paradise', 'VIS').
card_original_type('undiscovered paradise'/'VIS', 'Land').
card_original_text('undiscovered paradise'/'VIS', '{T}: Add one mana of any color to your mana pool. At the beginning of your next untap phase, return Undiscovered Paradise to owner\'s hand.').
card_first_print('undiscovered paradise', 'VIS').
card_image_name('undiscovered paradise'/'VIS', 'undiscovered paradise').
card_uid('undiscovered paradise'/'VIS', 'VIS:Undiscovered Paradise:undiscovered paradise').
card_rarity('undiscovered paradise'/'VIS', 'Rare').
card_artist('undiscovered paradise'/'VIS', 'David O\'Connor').
card_flavor_text('undiscovered paradise'/'VIS', 'Jolrael\'s choice of homes was far better than her choice of allies.').
card_multiverse_id('undiscovered paradise'/'VIS', '3755').

card_in_set('undo', 'VIS').
card_original_type('undo'/'VIS', 'Sorcery').
card_original_text('undo'/'VIS', 'Return two target creatures to owner\'s hand.').
card_first_print('undo', 'VIS').
card_image_name('undo'/'VIS', 'undo').
card_uid('undo'/'VIS', 'VIS:Undo:undo').
card_rarity('undo'/'VIS', 'Common').
card_artist('undo'/'VIS', 'Terese Nielsen').
card_flavor_text('undo'/'VIS', '\"Oft have I wished to undo past deeds, but never did I imagine they would be undone for me.\"\n—Naimah, Femeref philosopher').
card_multiverse_id('undo'/'VIS', '3654').

card_in_set('urborg mindsucker', 'VIS').
card_original_type('urborg mindsucker'/'VIS', 'Summon — Mindsucker').
card_original_text('urborg mindsucker'/'VIS', '{B}, Sacrifice Urborg Mindsucker: Target opponent discards a card at random. Play this ability as a sorcery.').
card_image_name('urborg mindsucker'/'VIS', 'urborg mindsucker').
card_uid('urborg mindsucker'/'VIS', 'VIS:Urborg Mindsucker:urborg mindsucker').
card_rarity('urborg mindsucker'/'VIS', 'Common').
card_artist('urborg mindsucker'/'VIS', 'Tony Diterlizzi').
card_flavor_text('urborg mindsucker'/'VIS', '\"My pet will pick the ripe fruit from your tortured brain as if it were hanging from a vine.\"\n—Kaervek').
card_multiverse_id('urborg mindsucker'/'VIS', '3628').

card_in_set('vampiric tutor', 'VIS').
card_original_type('vampiric tutor'/'VIS', 'Instant').
card_original_text('vampiric tutor'/'VIS', 'Pay 2 life: Search your library for any one card. Shuffle your library, then put that card on top of your library.').
card_first_print('vampiric tutor', 'VIS').
card_image_name('vampiric tutor'/'VIS', 'vampiric tutor').
card_uid('vampiric tutor'/'VIS', 'VIS:Vampiric Tutor:vampiric tutor').
card_rarity('vampiric tutor'/'VIS', 'Rare').
card_artist('vampiric tutor'/'VIS', 'Gary Leach').
card_flavor_text('vampiric tutor'/'VIS', '\"I write upon clean white parchment with a sharp quill and the blood of my students, divining their secrets.\"\n—Shauku, Endbringer').
card_multiverse_id('vampiric tutor'/'VIS', '3629').

card_in_set('vampirism', 'VIS').
card_original_type('vampirism'/'VIS', 'Enchant Creature').
card_original_text('vampirism'/'VIS', 'Draw a card at the beginning of the upkeep of the turn after Vampirism comes into play.\nEnchanted creature gets +1/+1 for each other creature you control. All other creatures you control get -1/-1.').
card_image_name('vampirism'/'VIS', 'vampirism').
card_uid('vampirism'/'VIS', 'VIS:Vampirism:vampirism').
card_rarity('vampirism'/'VIS', 'Uncommon').
card_artist('vampirism'/'VIS', 'Gary Leach').
card_multiverse_id('vampirism'/'VIS', '3630').

card_in_set('vanishing', 'VIS').
card_original_type('vanishing'/'VIS', 'Enchant Creature').
card_original_text('vanishing'/'VIS', '{U}{U} Enchanted creature phases out.').
card_first_print('vanishing', 'VIS').
card_image_name('vanishing'/'VIS', 'vanishing').
card_uid('vanishing'/'VIS', 'VIS:Vanishing:vanishing').
card_rarity('vanishing'/'VIS', 'Common').
card_artist('vanishing'/'VIS', 'John Matson').
card_flavor_text('vanishing'/'VIS', '\"Careless, like a child with fire, so was I with time.\"\n—Teferi').
card_multiverse_id('vanishing'/'VIS', '3655').

card_in_set('viashino sandstalker', 'VIS').
card_original_type('viashino sandstalker'/'VIS', 'Summon — Viashino').
card_original_text('viashino sandstalker'/'VIS', 'Viashino Sandstalker is unaffected by summoning sickness.\nAt the end of any turn, return Viashino Sandstalker to owner\'s hand.').
card_image_name('viashino sandstalker'/'VIS', 'viashino sandstalker').
card_uid('viashino sandstalker'/'VIS', 'VIS:Viashino Sandstalker:viashino sandstalker').
card_rarity('viashino sandstalker'/'VIS', 'Uncommon').
card_artist('viashino sandstalker'/'VIS', 'Andrew Robinson').
card_flavor_text('viashino sandstalker'/'VIS', '\"Some believe Sandstalkers to be illusions; those with scars know better.\"\n—Zhalfirin Guide to the Desert').
card_multiverse_id('viashino sandstalker'/'VIS', '3707').

card_in_set('viashivan dragon', 'VIS').
card_original_type('viashivan dragon'/'VIS', 'Summon — Dragon').
card_original_text('viashivan dragon'/'VIS', 'Flying\n{R} +1/+0 until end of turn\n{G} +0/+1 until end of turn').
card_first_print('viashivan dragon', 'VIS').
card_image_name('viashivan dragon'/'VIS', 'viashivan dragon').
card_uid('viashivan dragon'/'VIS', 'VIS:Viashivan Dragon:viashivan dragon').
card_rarity('viashivan dragon'/'VIS', 'Rare').
card_artist('viashivan dragon'/'VIS', 'Ian Miller').
card_flavor_text('viashivan dragon'/'VIS', 'The Viashivan understand that cruelty arises from opportunity.').
card_multiverse_id('viashivan dragon'/'VIS', '3747').

card_in_set('vision charm', 'VIS').
card_original_type('vision charm'/'VIS', 'Instant').
card_original_text('vision charm'/'VIS', 'Choose one Target artifact phases out; or put the top four cards from target player\'s library into his or her graveyard; or all lands of one type are basic lands of your choice until end of turn.').
card_first_print('vision charm', 'VIS').
card_image_name('vision charm'/'VIS', 'vision charm').
card_uid('vision charm'/'VIS', 'VIS:Vision Charm:vision charm').
card_rarity('vision charm'/'VIS', 'Common').
card_artist('vision charm'/'VIS', 'Greg Spalenka').
card_multiverse_id('vision charm'/'VIS', '3656').

card_in_set('wake of vultures', 'VIS').
card_original_type('wake of vultures'/'VIS', 'Summon — Vultures').
card_original_text('wake of vultures'/'VIS', 'Flying\n{1}{B}, Sacrifice a creature: Regenerate').
card_first_print('wake of vultures', 'VIS').
card_image_name('wake of vultures'/'VIS', 'wake of vultures').
card_uid('wake of vultures'/'VIS', 'VIS:Wake of Vultures:wake of vultures').
card_rarity('wake of vultures'/'VIS', 'Common').
card_artist('wake of vultures'/'VIS', 'Jeff Miracola').
card_flavor_text('wake of vultures'/'VIS', '\"So the vulture said to the griffin, ‘You gonna eat that?\'\"\n—Azeworai, \"The Ugly Bird\"').
card_multiverse_id('wake of vultures'/'VIS', '3631').

card_in_set('wand of denial', 'VIS').
card_original_type('wand of denial'/'VIS', 'Artifact').
card_original_text('wand of denial'/'VIS', '{T}: Look at the top card of target player\'s library. If that card is a nonland card, you may pay 2 life to put it into that player\'s graveyard.').
card_first_print('wand of denial', 'VIS').
card_image_name('wand of denial'/'VIS', 'wand of denial').
card_uid('wand of denial'/'VIS', 'VIS:Wand of Denial:wand of denial').
card_rarity('wand of denial'/'VIS', 'Rare').
card_artist('wand of denial'/'VIS', 'Steve Luke').
card_flavor_text('wand of denial'/'VIS', 'You\'ll never miss what you never had.').
card_multiverse_id('wand of denial'/'VIS', '3607').

card_in_set('warrior\'s honor', 'VIS').
card_original_type('warrior\'s honor'/'VIS', 'Instant').
card_original_text('warrior\'s honor'/'VIS', 'All creatures you control get +1/+1 until end of turn.').
card_first_print('warrior\'s honor', 'VIS').
card_image_name('warrior\'s honor'/'VIS', 'warrior\'s honor').
card_uid('warrior\'s honor'/'VIS', 'VIS:Warrior\'s Honor:warrior\'s honor').
card_rarity('warrior\'s honor'/'VIS', 'Common').
card_artist('warrior\'s honor'/'VIS', 'D. Alexander Gregory').
card_flavor_text('warrior\'s honor'/'VIS', '\"We are to be bound together as the reeds of a basket.\"\n—Asmira, Rashida, and Jabari, unity chant').
card_multiverse_id('warrior\'s honor'/'VIS', '3731').

card_in_set('warthog', 'VIS').
card_original_type('warthog'/'VIS', 'Summon — Warthog').
card_original_text('warthog'/'VIS', 'Swampwalk (If defending player controls any swamps, this creature is unblockable.)').
card_first_print('warthog', 'VIS').
card_image_name('warthog'/'VIS', 'warthog').
card_uid('warthog'/'VIS', 'VIS:Warthog:warthog').
card_rarity('warthog'/'VIS', 'Common').
card_artist('warthog'/'VIS', 'Steve White').
card_flavor_text('warthog'/'VIS', '\"Too much work—it takes a long time to break them in, and more than a few recruits.\"\n—Grebog, goblin swine-rider').
card_multiverse_id('warthog'/'VIS', '3681').

card_in_set('waterspout djinn', 'VIS').
card_original_type('waterspout djinn'/'VIS', 'Summon — Djinn').
card_original_text('waterspout djinn'/'VIS', 'Flying\nDuring your upkeep, return an untapped island you control to owner\'s hand or bury Waterspout Djinn.').
card_first_print('waterspout djinn', 'VIS').
card_image_name('waterspout djinn'/'VIS', 'waterspout djinn').
card_uid('waterspout djinn'/'VIS', 'VIS:Waterspout Djinn:waterspout djinn').
card_rarity('waterspout djinn'/'VIS', 'Uncommon').
card_artist('waterspout djinn'/'VIS', 'Thomas Gianni').
card_flavor_text('waterspout djinn'/'VIS', '\"Fly us higher, out of its storm.\"\n—Sisay, Captain of the Weatherlight').
card_multiverse_id('waterspout djinn'/'VIS', '3657').

card_in_set('wicked reward', 'VIS').
card_original_type('wicked reward'/'VIS', 'Instant').
card_original_text('wicked reward'/'VIS', 'Sacrifice a creature: Target creature gets +4/+2 until end of turn.').
card_image_name('wicked reward'/'VIS', 'wicked reward').
card_uid('wicked reward'/'VIS', 'VIS:Wicked Reward:wicked reward').
card_rarity('wicked reward'/'VIS', 'Common').
card_artist('wicked reward'/'VIS', 'D. Alexander Gregory').
card_flavor_text('wicked reward'/'VIS', '\"The blood on my hands is merely proof of my ambition.\"\n—Kaervek').
card_multiverse_id('wicked reward'/'VIS', '3632').

card_in_set('wind shear', 'VIS').
card_original_type('wind shear'/'VIS', 'Instant').
card_original_text('wind shear'/'VIS', 'All attacking creatures with flying get -2/-2 and lose flying until end of turn.').
card_first_print('wind shear', 'VIS').
card_image_name('wind shear'/'VIS', 'wind shear').
card_uid('wind shear'/'VIS', 'VIS:Wind Shear:wind shear').
card_rarity('wind shear'/'VIS', 'Uncommon').
card_artist('wind shear'/'VIS', 'John Matson').
card_flavor_text('wind shear'/'VIS', '\"As the winds abated, there was a shower of dragonscales, then nothing more.\"\n—Azeworai, \"The Unruly Wind\"').
card_multiverse_id('wind shear'/'VIS', '3682').

card_in_set('zhalfirin crusader', 'VIS').
card_original_type('zhalfirin crusader'/'VIS', 'Summon — Knight').
card_original_text('zhalfirin crusader'/'VIS', 'Flanking (Whenever a creature without flanking is assigned to block this creature, the blocking creature gets -1/-1 until end of turn.)\n{1}{W} Redirect 1 damage from Zhalfirin Crusader to target creature or player.').
card_first_print('zhalfirin crusader', 'VIS').
card_image_name('zhalfirin crusader'/'VIS', 'zhalfirin crusader').
card_uid('zhalfirin crusader'/'VIS', 'VIS:Zhalfirin Crusader:zhalfirin crusader').
card_rarity('zhalfirin crusader'/'VIS', 'Rare').
card_artist('zhalfirin crusader'/'VIS', 'Alan Rabinowitz').
card_flavor_text('zhalfirin crusader'/'VIS', '\"War is the crucible of leadership.\"\n—Rashida Scalebane').
card_multiverse_id('zhalfirin crusader'/'VIS', '3732').
