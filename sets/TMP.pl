% Tempest

set('TMP').
set_name('TMP', 'Tempest').
set_release_date('TMP', '1997-10-14').
set_border('TMP', 'black').
set_type('TMP', 'expansion').
set_block('TMP', 'Tempest').

card_in_set('abandon hope', 'TMP').
card_original_type('abandon hope'/'TMP', 'Sorcery').
card_original_text('abandon hope'/'TMP', 'Choose and discard X cards: Look at target opponent\'s hand and choose X of those cards. That player discards the chosen cards.').
card_first_print('abandon hope', 'TMP').
card_image_name('abandon hope'/'TMP', 'abandon hope').
card_uid('abandon hope'/'TMP', 'TMP:Abandon Hope:abandon hope').
card_rarity('abandon hope'/'TMP', 'Uncommon').
card_artist('abandon hope'/'TMP', 'Alan Pollack').
card_flavor_text('abandon hope'/'TMP', 'As Gerrard\'s form vanished into the maw of trees, Hanna mouthed a silent plea, mourning a crushed dream.').
card_multiverse_id('abandon hope'/'TMP', '4635').

card_in_set('advance scout', 'TMP').
card_original_type('advance scout'/'TMP', 'Summon — Soldier').
card_original_text('advance scout'/'TMP', 'First strike\n{W}: Target creature gains first strike until end of turn.').
card_first_print('advance scout', 'TMP').
card_image_name('advance scout'/'TMP', 'advance scout').
card_uid('advance scout'/'TMP', 'TMP:Advance Scout:advance scout').
card_rarity('advance scout'/'TMP', 'Common').
card_artist('advance scout'/'TMP', 'Heather Hudson').
card_flavor_text('advance scout'/'TMP', '\"The soldier\'s path is worn smooth by the tread of many feet—all in one direction, none returning.\"\n—Oracle en-Vec').
card_multiverse_id('advance scout'/'TMP', '4857').

card_in_set('aftershock', 'TMP').
card_original_type('aftershock'/'TMP', 'Sorcery').
card_original_text('aftershock'/'TMP', 'Destroy target artifact, creature, or land. Aftershock deals 3 damage to you.').
card_first_print('aftershock', 'TMP').
card_image_name('aftershock'/'TMP', 'aftershock').
card_uid('aftershock'/'TMP', 'TMP:Aftershock:aftershock').
card_rarity('aftershock'/'TMP', 'Common').
card_artist('aftershock'/'TMP', 'Hannibal King').
card_flavor_text('aftershock'/'TMP', '\"Every act of destruction has a repercussion.\"\n—Karn, silver golem').
card_multiverse_id('aftershock'/'TMP', '4800').

card_in_set('altar of dementia', 'TMP').
card_original_type('altar of dementia'/'TMP', 'Artifact').
card_original_text('altar of dementia'/'TMP', 'Sacrifice a creature: Target player puts a number of cards equal to that creature\'s power from the top of his or her library into his or her graveyard.').
card_first_print('altar of dementia', 'TMP').
card_image_name('altar of dementia'/'TMP', 'altar of dementia').
card_uid('altar of dementia'/'TMP', 'TMP:Altar of Dementia:altar of dementia').
card_rarity('altar of dementia'/'TMP', 'Rare').
card_artist('altar of dementia'/'TMP', 'Brom').
card_flavor_text('altar of dementia'/'TMP', '\"It is not that you will go mad. It is that you will beg for madness.\"\n—Volrath').
card_multiverse_id('altar of dementia'/'TMP', '4596').

card_in_set('aluren', 'TMP').
card_original_type('aluren'/'TMP', 'Enchantment').
card_original_text('aluren'/'TMP', 'Any player may play a creature card with total casting cost 3 or less whenever he or she could play an instant and without paying its casting cost.').
card_first_print('aluren', 'TMP').
card_image_name('aluren'/'TMP', 'aluren').
card_uid('aluren'/'TMP', 'TMP:Aluren:aluren').
card_rarity('aluren'/'TMP', 'Rare').
card_artist('aluren'/'TMP', 'April Lee').
card_flavor_text('aluren'/'TMP', 'Squee bounced up and down. \"I sees a horsey, an\' a piggy, an\' a—\"\n\"If you don\'t shut up,\" hissed Mirri, \"you\'ll see a kidney and a spleeny.\"').
card_multiverse_id('aluren'/'TMP', '4747').

card_in_set('ancient runes', 'TMP').
card_original_type('ancient runes'/'TMP', 'Enchantment').
card_original_text('ancient runes'/'TMP', 'During each player\'s upkeep, Ancient Runes deals 1 damage to that player for each artifact he or she controls.').
card_first_print('ancient runes', 'TMP').
card_image_name('ancient runes'/'TMP', 'ancient runes').
card_uid('ancient runes'/'TMP', 'TMP:Ancient Runes:ancient runes').
card_rarity('ancient runes'/'TMP', 'Uncommon').
card_artist('ancient runes'/'TMP', 'Susan Van Camp').
card_flavor_text('ancient runes'/'TMP', 'Ertai volunteered to open the portal. He figured he owed it one.').
card_multiverse_id('ancient runes'/'TMP', '4801').

card_in_set('ancient tomb', 'TMP').
card_original_type('ancient tomb'/'TMP', 'Land').
card_original_text('ancient tomb'/'TMP', '{T}: Add two colorless mana to your mana pool. Ancient Tomb deals 2 damage to you.').
card_first_print('ancient tomb', 'TMP').
card_image_name('ancient tomb'/'TMP', 'ancient tomb').
card_uid('ancient tomb'/'TMP', 'TMP:Ancient Tomb:ancient tomb').
card_rarity('ancient tomb'/'TMP', 'Uncommon').
card_artist('ancient tomb'/'TMP', 'Colin MacNeil').
card_flavor_text('ancient tomb'/'TMP', 'There is no glory to be gained in the kingdom of the dead.\n—Vec tomb inscription').
card_multiverse_id('ancient tomb'/'TMP', '4636').

card_in_set('angelic protector', 'TMP').
card_original_type('angelic protector'/'TMP', 'Summon — Angel').
card_original_text('angelic protector'/'TMP', 'Flying\nIf Angelic Protector is the target of a spell or ability, it gets +0/+3 until end of turn.').
card_first_print('angelic protector', 'TMP').
card_image_name('angelic protector'/'TMP', 'angelic protector').
card_uid('angelic protector'/'TMP', 'TMP:Angelic Protector:angelic protector').
card_rarity('angelic protector'/'TMP', 'Uncommon').
card_artist('angelic protector'/'TMP', 'DiTerlizzi').
card_flavor_text('angelic protector'/'TMP', '\"My family sheltered in her light, the dark was content to wait.\"\n—Crovax').
card_multiverse_id('angelic protector'/'TMP', '4858').

card_in_set('anoint', 'TMP').
card_original_type('anoint'/'TMP', 'Instant').
card_original_text('anoint'/'TMP', 'Buyback {3} (You may pay an additional {3} when you play this spell. If you do, put it into your hand instead of your graveyard as part of the spell\'s effect.) \nPrevent up to 3 damage to any creature.').
card_first_print('anoint', 'TMP').
card_image_name('anoint'/'TMP', 'anoint').
card_uid('anoint'/'TMP', 'TMP:Anoint:anoint').
card_rarity('anoint'/'TMP', 'Common').
card_artist('anoint'/'TMP', 'Eric David Anderson').
card_multiverse_id('anoint'/'TMP', '4859').

card_in_set('apes of rath', 'TMP').
card_original_type('apes of rath'/'TMP', 'Summon — Apes').
card_original_text('apes of rath'/'TMP', 'If Apes of Rath attacks, it does not untap during your next untap phase.').
card_first_print('apes of rath', 'TMP').
card_image_name('apes of rath'/'TMP', 'apes of rath').
card_uid('apes of rath'/'TMP', 'TMP:Apes of Rath:apes of rath').
card_rarity('apes of rath'/'TMP', 'Uncommon').
card_artist('apes of rath'/'TMP', 'Jeff Laubenstein').
card_flavor_text('apes of rath'/'TMP', 'Monkeys three, monkeys through.').
card_multiverse_id('apes of rath'/'TMP', '4748').

card_in_set('apocalypse', 'TMP').
card_original_type('apocalypse'/'TMP', 'Sorcery').
card_original_text('apocalypse'/'TMP', 'Remove all permanents from the game. Discard your hand.').
card_first_print('apocalypse', 'TMP').
card_image_name('apocalypse'/'TMP', 'apocalypse').
card_uid('apocalypse'/'TMP', 'TMP:Apocalypse:apocalypse').
card_rarity('apocalypse'/'TMP', 'Rare').
card_artist('apocalypse'/'TMP', 'L. A. Williams').
card_flavor_text('apocalypse'/'TMP', '\"There is a future in which I can see only mist and a single shadow.\"\n—Oracle en-Vec').
card_multiverse_id('apocalypse'/'TMP', '4802').

card_in_set('armor sliver', 'TMP').
card_original_type('armor sliver'/'TMP', 'Summon — Sliver').
card_original_text('armor sliver'/'TMP', 'Each Sliver gains \"{2}: This creature gets +0/+1 until end of turn.\"').
card_first_print('armor sliver', 'TMP').
card_image_name('armor sliver'/'TMP', 'armor sliver').
card_uid('armor sliver'/'TMP', 'TMP:Armor Sliver:armor sliver').
card_rarity('armor sliver'/'TMP', 'Uncommon').
card_artist('armor sliver'/'TMP', 'Scott Kirschner').
card_flavor_text('armor sliver'/'TMP', 'Hanna: \"We must learn how they protect each other.\"\nMirri: \"After they\'ve done trying to kill us, all right?\"').
card_multiverse_id('armor sliver'/'TMP', '4689').

card_in_set('armored pegasus', 'TMP').
card_original_type('armored pegasus'/'TMP', 'Summon — Pegasus').
card_original_text('armored pegasus'/'TMP', 'Flying').
card_image_name('armored pegasus'/'TMP', 'armored pegasus').
card_uid('armored pegasus'/'TMP', 'TMP:Armored Pegasus:armored pegasus').
card_rarity('armored pegasus'/'TMP', 'Common').
card_artist('armored pegasus'/'TMP', 'Una Fricker').
card_flavor_text('armored pegasus'/'TMP', '\"I always charge a little extra to take on a pegasus. They fly like eagles, kick like mules, and hide like hermits.\"\n—Tahli en-Dal, bounty hunter').
card_multiverse_id('armored pegasus'/'TMP', '4861').

card_in_set('auratog', 'TMP').
card_original_type('auratog'/'TMP', 'Summon — Atog').
card_original_text('auratog'/'TMP', 'Sacrifice an enchantment: Auratog gets +2/+2 until end of turn.').
card_first_print('auratog', 'TMP').
card_image_name('auratog'/'TMP', 'auratog').
card_uid('auratog'/'TMP', 'TMP:Auratog:auratog').
card_rarity('auratog'/'TMP', 'Rare').
card_artist('auratog'/'TMP', 'Jeff Miracola').
card_flavor_text('auratog'/'TMP', 'The auratog enjoys eating its wards.').
card_multiverse_id('auratog'/'TMP', '4862').

card_in_set('avenging angel', 'TMP').
card_original_type('avenging angel'/'TMP', 'Summon — Angel').
card_original_text('avenging angel'/'TMP', 'Flying\nIf Avenging Angel is put into any graveyard from play, you may put Avenging Angel on top of owner\'s library.').
card_first_print('avenging angel', 'TMP').
card_image_name('avenging angel'/'TMP', 'avenging angel').
card_uid('avenging angel'/'TMP', 'TMP:Avenging Angel:avenging angel').
card_rarity('avenging angel'/'TMP', 'Rare').
card_artist('avenging angel'/'TMP', 'Matthew D. Wilson').
card_multiverse_id('avenging angel'/'TMP', '4863').

card_in_set('barbed sliver', 'TMP').
card_original_type('barbed sliver'/'TMP', 'Summon — Sliver').
card_original_text('barbed sliver'/'TMP', 'Each Sliver gains \"{2}: This creature gets +1/+0 until end of turn.\"').
card_first_print('barbed sliver', 'TMP').
card_image_name('barbed sliver'/'TMP', 'barbed sliver').
card_uid('barbed sliver'/'TMP', 'TMP:Barbed Sliver:barbed sliver').
card_rarity('barbed sliver'/'TMP', 'Uncommon').
card_artist('barbed sliver'/'TMP', 'Scott Kirschner').
card_flavor_text('barbed sliver'/'TMP', 'Spans of spines leapt from one sliver to the next, forming a deadly hedge around the Weatherlight.').
card_multiverse_id('barbed sliver'/'TMP', '4803').

card_in_set('bayou dragonfly', 'TMP').
card_original_type('bayou dragonfly'/'TMP', 'Summon — Insect').
card_original_text('bayou dragonfly'/'TMP', 'Flying; swampwalk (If defending player controls any swamps, this creature is unblockable.)').
card_first_print('bayou dragonfly', 'TMP').
card_image_name('bayou dragonfly'/'TMP', 'bayou dragonfly').
card_uid('bayou dragonfly'/'TMP', 'TMP:Bayou Dragonfly:bayou dragonfly').
card_rarity('bayou dragonfly'/'TMP', 'Common').
card_artist('bayou dragonfly'/'TMP', 'DiTerlizzi').
card_flavor_text('bayou dragonfly'/'TMP', '\"Like a sugar stick with wings!\"\n—Squee, goblin cabin hand').
card_multiverse_id('bayou dragonfly'/'TMP', '4749').

card_in_set('bellowing fiend', 'TMP').
card_original_type('bellowing fiend'/'TMP', 'Summon — Spirit').
card_original_text('bellowing fiend'/'TMP', 'Flying\nWhenever Bellowing Fiend damages any creature, Bellowing Fiend deals 3 damage to that creature\'s controller and 3 damage to you.').
card_first_print('bellowing fiend', 'TMP').
card_image_name('bellowing fiend'/'TMP', 'bellowing fiend').
card_uid('bellowing fiend'/'TMP', 'TMP:Bellowing Fiend:bellowing fiend').
card_rarity('bellowing fiend'/'TMP', 'Rare').
card_artist('bellowing fiend'/'TMP', 'Jim Nelson').
card_multiverse_id('bellowing fiend'/'TMP', '4637').

card_in_set('benthic behemoth', 'TMP').
card_original_type('benthic behemoth'/'TMP', 'Summon — Serpent').
card_original_text('benthic behemoth'/'TMP', 'Islandwalk (If defending player controls any islands, this creature is unblockable.)').
card_first_print('benthic behemoth', 'TMP').
card_image_name('benthic behemoth'/'TMP', 'benthic behemoth').
card_uid('benthic behemoth'/'TMP', 'TMP:Benthic Behemoth:benthic behemoth').
card_rarity('benthic behemoth'/'TMP', 'Rare').
card_artist('benthic behemoth'/'TMP', 'Jim Nelson').
card_flavor_text('benthic behemoth'/'TMP', '\"Deep devourer   concealed in darkhome\nShrouded it seeks   all becomes fodder\nOnce we swam   alone and asea\nBut no more.\"\n—Rootwater Saga').
card_multiverse_id('benthic behemoth'/'TMP', '4690').

card_in_set('blood frenzy', 'TMP').
card_original_type('blood frenzy'/'TMP', 'Instant').
card_original_text('blood frenzy'/'TMP', 'Target attacking or blocking creature gets +4/+0 until end of turn. At end of turn, destroy that creature.').
card_first_print('blood frenzy', 'TMP').
card_image_name('blood frenzy'/'TMP', 'blood frenzy').
card_uid('blood frenzy'/'TMP', 'TMP:Blood Frenzy:blood frenzy').
card_rarity('blood frenzy'/'TMP', 'Common').
card_artist('blood frenzy'/'TMP', 'Paolo Parente').
card_flavor_text('blood frenzy'/'TMP', '\"When Torahn bellows, none survive—not even the warrior whose horns he rides.\"\n—Talruum adage').
card_multiverse_id('blood frenzy'/'TMP', '4804').

card_in_set('blood pet', 'TMP').
card_original_type('blood pet'/'TMP', 'Summon — Thrull').
card_original_text('blood pet'/'TMP', 'Sacrifice Blood Pet: Add {B} to your mana pool. Play this ability as a mana source.').
card_first_print('blood pet', 'TMP').
card_image_name('blood pet'/'TMP', 'blood pet').
card_uid('blood pet'/'TMP', 'TMP:Blood Pet:blood pet').
card_rarity('blood pet'/'TMP', 'Common').
card_artist('blood pet'/'TMP', 'Brom').
card_flavor_text('blood pet'/'TMP', '\"You are wrong,\" Volrath said. \"I do not hate the living. They often prove quite useful to me.\" And then he laughed.').
card_multiverse_id('blood pet'/'TMP', '12342').

card_in_set('boil', 'TMP').
card_original_type('boil'/'TMP', 'Instant').
card_original_text('boil'/'TMP', 'Destroy all islands.').
card_first_print('boil', 'TMP').
card_image_name('boil'/'TMP', 'boil').
card_uid('boil'/'TMP', 'TMP:Boil:boil').
card_rarity('boil'/'TMP', 'Uncommon').
card_artist('boil'/'TMP', 'Jason Alexander Behnke').
card_flavor_text('boil'/'TMP', '\"The fishers will throw out their nets and draw them back filled with dust.\"\n—Oracle en-Vec').
card_multiverse_id('boil'/'TMP', '4805').

card_in_set('booby trap', 'TMP').
card_original_type('booby trap'/'TMP', 'Artifact').
card_original_text('booby trap'/'TMP', 'When Booby Trap comes into play, name a card other than a basic land.\nWhenever target opponent draws any cards, he or she reveals those cards to all players. If any of those cards is the named card, Sacrifice Booby Trap and it deals 10 damage to that player.').
card_first_print('booby trap', 'TMP').
card_image_name('booby trap'/'TMP', 'booby trap').
card_uid('booby trap'/'TMP', 'TMP:Booby Trap:booby trap').
card_rarity('booby trap'/'TMP', 'Rare').
card_artist('booby trap'/'TMP', 'Doug Chaffee').
card_multiverse_id('booby trap'/'TMP', '4597').

card_in_set('bottle gnomes', 'TMP').
card_original_type('bottle gnomes'/'TMP', 'Artifact Creature').
card_original_text('bottle gnomes'/'TMP', 'Sacrifice Bottle Gnomes: Gain 3 life.').
card_first_print('bottle gnomes', 'TMP').
card_image_name('bottle gnomes'/'TMP', 'bottle gnomes').
card_uid('bottle gnomes'/'TMP', 'TMP:Bottle Gnomes:bottle gnomes').
card_rarity('bottle gnomes'/'TMP', 'Uncommon').
card_artist('bottle gnomes'/'TMP', 'Kaja Foglio').
card_flavor_text('bottle gnomes'/'TMP', '\"I am reminded of the fable of the silver egg. Its owner cracks it open to see what jewels it holds, only to discover a simple yellow yolk.\"\n—Karn, silver golem').
card_multiverse_id('bottle gnomes'/'TMP', '4598').

card_in_set('bounty hunter', 'TMP').
card_original_type('bounty hunter'/'TMP', 'Summon — Minion').
card_original_text('bounty hunter'/'TMP', '{T}: Put a bounty counter on target nonblack creature.\n{T}: Destroy target creature with any bounty counters on it.').
card_first_print('bounty hunter', 'TMP').
card_image_name('bounty hunter'/'TMP', 'bounty hunter').
card_uid('bounty hunter'/'TMP', 'TMP:Bounty Hunter:bounty hunter').
card_rarity('bounty hunter'/'TMP', 'Rare').
card_artist('bounty hunter'/'TMP', 'Brian Snõddy').
card_flavor_text('bounty hunter'/'TMP', '\"Once they\'ve marked you,\" Starke said, \"the world is made of glass.\"').
card_multiverse_id('bounty hunter'/'TMP', '4638').

card_in_set('broken fall', 'TMP').
card_original_type('broken fall'/'TMP', 'Enchantment').
card_original_text('broken fall'/'TMP', 'Return Broken Fall to owner\'s hand: Regenerate target creature.').
card_first_print('broken fall', 'TMP').
card_image_name('broken fall'/'TMP', 'broken fall').
card_uid('broken fall'/'TMP', 'TMP:Broken Fall:broken fall').
card_rarity('broken fall'/'TMP', 'Common').
card_artist('broken fall'/'TMP', 'Zina Saunders').
card_flavor_text('broken fall'/'TMP', '\"I think I used up all my good luck and all my bad luck in the same fall.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('broken fall'/'TMP', '4750').

card_in_set('caldera lake', 'TMP').
card_original_type('caldera lake'/'TMP', 'Land').
card_original_text('caldera lake'/'TMP', 'Caldera Lake comes into play tapped.\n{T}: Add one colorless mana to your mana pool.\n{T}: Add {U} or {R} to your mana pool. Caldera Lake deals 1 damage to you.').
card_first_print('caldera lake', 'TMP').
card_image_name('caldera lake'/'TMP', 'caldera lake').
card_uid('caldera lake'/'TMP', 'TMP:Caldera Lake:caldera lake').
card_rarity('caldera lake'/'TMP', 'Rare').
card_artist('caldera lake'/'TMP', 'L. A. Williams').
card_multiverse_id('caldera lake'/'TMP', '4930').

card_in_set('canopy spider', 'TMP').
card_original_type('canopy spider'/'TMP', 'Summon — Spider').
card_original_text('canopy spider'/'TMP', 'Canopy Spider can block creatures with flying.').
card_first_print('canopy spider', 'TMP').
card_image_name('canopy spider'/'TMP', 'canopy spider').
card_uid('canopy spider'/'TMP', 'TMP:Canopy Spider:canopy spider').
card_rarity('canopy spider'/'TMP', 'Common').
card_artist('canopy spider'/'TMP', 'Christopher Rush').
card_flavor_text('canopy spider'/'TMP', '\"We know our place in the cycle of life—and it is not as flies.\"\n—Eladamri, Lord of Leaves').
card_multiverse_id('canopy spider'/'TMP', '4751').

card_in_set('canyon drake', 'TMP').
card_original_type('canyon drake'/'TMP', 'Summon — Drake').
card_original_text('canyon drake'/'TMP', 'Flying\n{1}, Discard a card at random: Canyon Drake gets +2/+0 until end of turn.').
card_first_print('canyon drake', 'TMP').
card_image_name('canyon drake'/'TMP', 'canyon drake').
card_uid('canyon drake'/'TMP', 'TMP:Canyon Drake:canyon drake').
card_rarity('canyon drake'/'TMP', 'Rare').
card_artist('canyon drake'/'TMP', 'Quinton Hoover').
card_flavor_text('canyon drake'/'TMP', '\"These runes are tough enough without the distraction,\" Ertai muttered, one eye on the drake.').
card_multiverse_id('canyon drake'/'TMP', '4806').

card_in_set('canyon wildcat', 'TMP').
card_original_type('canyon wildcat'/'TMP', 'Summon — Cat').
card_original_text('canyon wildcat'/'TMP', 'Mountainwalk (If defending player controls any mountains, this creature is unblockable.)').
card_first_print('canyon wildcat', 'TMP').
card_image_name('canyon wildcat'/'TMP', 'canyon wildcat').
card_uid('canyon wildcat'/'TMP', 'TMP:Canyon Wildcat:canyon wildcat').
card_rarity('canyon wildcat'/'TMP', 'Common').
card_artist('canyon wildcat'/'TMP', 'Gary Leach').
card_flavor_text('canyon wildcat'/'TMP', '\"Relative of yours?\" Ertai teased. Mirri simply sneered.').
card_multiverse_id('canyon wildcat'/'TMP', '4807').

card_in_set('capsize', 'TMP').
card_original_type('capsize'/'TMP', 'Instant').
card_original_text('capsize'/'TMP', 'Buyback {3} (You may pay an additional {3} when you play this spell. If you do, put it into your hand instead of your graveyard as part of the spell\'s effect.)\nReturn target permanent to owner\'s hand.').
card_first_print('capsize', 'TMP').
card_image_name('capsize'/'TMP', 'capsize').
card_uid('capsize'/'TMP', 'TMP:Capsize:capsize').
card_rarity('capsize'/'TMP', 'Common').
card_artist('capsize'/'TMP', 'Tom Wänerstrand').
card_multiverse_id('capsize'/'TMP', '4691').

card_in_set('carrionette', 'TMP').
card_original_type('carrionette'/'TMP', 'Summon — Skeleton').
card_original_text('carrionette'/'TMP', '{2}{B}{B}: Remove Carrionette and target creature from the game. That creature\'s controller may pay {2} to counter this ability. Use this ability only if Carrionette is in your graveyard.').
card_first_print('carrionette', 'TMP').
card_image_name('carrionette'/'TMP', 'carrionette').
card_uid('carrionette'/'TMP', 'TMP:Carrionette:carrionette').
card_rarity('carrionette'/'TMP', 'Rare').
card_artist('carrionette'/'TMP', 'Pete Venters').
card_multiverse_id('carrionette'/'TMP', '4639').

card_in_set('chaotic goo', 'TMP').
card_original_type('chaotic goo'/'TMP', 'Summon — Ooze').
card_original_text('chaotic goo'/'TMP', 'Chaotic Goo comes into play with three +1/+1 counters on it.\nDuring your upkeep, you may flip a coin. If you win the flip, add a +1/+1 counter to Chaotic Goo. Otherwise, remove a +1/+1 counter from it.').
card_first_print('chaotic goo', 'TMP').
card_image_name('chaotic goo'/'TMP', 'chaotic goo').
card_uid('chaotic goo'/'TMP', 'TMP:Chaotic Goo:chaotic goo').
card_rarity('chaotic goo'/'TMP', 'Rare').
card_artist('chaotic goo'/'TMP', 'L. A. Williams').
card_multiverse_id('chaotic goo'/'TMP', '4808').

card_in_set('charging rhino', 'TMP').
card_original_type('charging rhino'/'TMP', 'Summon — Rhino').
card_original_text('charging rhino'/'TMP', 'Charging Rhino cannot be blocked by more than one creature.').
card_image_name('charging rhino'/'TMP', 'charging rhino').
card_uid('charging rhino'/'TMP', 'TMP:Charging Rhino:charging rhino').
card_rarity('charging rhino'/'TMP', 'Uncommon').
card_artist('charging rhino'/'TMP', 'Daren Bader').
card_flavor_text('charging rhino'/'TMP', '\"Is it wrong to go straight for what you want? Sneaking is for cats and goblins.\"\n—Tahngarth of the Weatherlight').
card_multiverse_id('charging rhino'/'TMP', '4752').

card_in_set('chill', 'TMP').
card_original_type('chill'/'TMP', 'Enchantment').
card_original_text('chill'/'TMP', 'Red spells cost an additional {2} to play.').
card_image_name('chill'/'TMP', 'chill').
card_uid('chill'/'TMP', 'TMP:Chill:chill').
card_rarity('chill'/'TMP', 'Uncommon').
card_artist('chill'/'TMP', 'Greg Simanson').
card_flavor_text('chill'/'TMP', '\"Temper, temper.\"\n—Ertai, wizard adept').
card_multiverse_id('chill'/'TMP', '4692').

card_in_set('choke', 'TMP').
card_original_type('choke'/'TMP', 'Enchantment').
card_original_text('choke'/'TMP', 'Islands do not untap during their controllers\' untap phases.').
card_first_print('choke', 'TMP').
card_image_name('choke'/'TMP', 'choke').
card_uid('choke'/'TMP', 'TMP:Choke:choke').
card_rarity('choke'/'TMP', 'Uncommon').
card_artist('choke'/'TMP', 'Terese Nielsen').
card_flavor_text('choke'/'TMP', '\"One day we shall walk where once was water.\"\n—Eladamri, Lord of Leaves').
card_multiverse_id('choke'/'TMP', '4753').

card_in_set('cinder marsh', 'TMP').
card_original_type('cinder marsh'/'TMP', 'Land').
card_original_text('cinder marsh'/'TMP', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {B} or {R} to your mana pool. Cinder Marsh does not untap during your next untap phase.').
card_first_print('cinder marsh', 'TMP').
card_image_name('cinder marsh'/'TMP', 'cinder marsh').
card_uid('cinder marsh'/'TMP', 'TMP:Cinder Marsh:cinder marsh').
card_rarity('cinder marsh'/'TMP', 'Uncommon').
card_artist('cinder marsh'/'TMP', 'John Matson').
card_multiverse_id('cinder marsh'/'TMP', '4931').

card_in_set('circle of protection: black', 'TMP').
card_original_type('circle of protection: black'/'TMP', 'Enchantment').
card_original_text('circle of protection: black'/'TMP', '{1}: Prevent all damage to you from a black source. (Treat further damage from that source normally.)').
card_image_name('circle of protection: black'/'TMP', 'circle of protection black').
card_uid('circle of protection: black'/'TMP', 'TMP:Circle of Protection: Black:circle of protection black').
card_rarity('circle of protection: black'/'TMP', 'Common').
card_artist('circle of protection: black'/'TMP', 'Harold McNeill').
card_multiverse_id('circle of protection: black'/'TMP', '4864').

card_in_set('circle of protection: blue', 'TMP').
card_original_type('circle of protection: blue'/'TMP', 'Enchantment').
card_original_text('circle of protection: blue'/'TMP', '{1}: Prevent all damage to you from a blue source. (Treat further damage from that source normally.)').
card_image_name('circle of protection: blue'/'TMP', 'circle of protection blue').
card_uid('circle of protection: blue'/'TMP', 'TMP:Circle of Protection: Blue:circle of protection blue').
card_rarity('circle of protection: blue'/'TMP', 'Common').
card_artist('circle of protection: blue'/'TMP', 'Harold McNeill').
card_multiverse_id('circle of protection: blue'/'TMP', '4865').

card_in_set('circle of protection: green', 'TMP').
card_original_type('circle of protection: green'/'TMP', 'Enchantment').
card_original_text('circle of protection: green'/'TMP', '{1}: Prevent all damage to you from a green source. (Treat further damage from that source normally.)').
card_image_name('circle of protection: green'/'TMP', 'circle of protection green').
card_uid('circle of protection: green'/'TMP', 'TMP:Circle of Protection: Green:circle of protection green').
card_rarity('circle of protection: green'/'TMP', 'Common').
card_artist('circle of protection: green'/'TMP', 'Harold McNeill').
card_multiverse_id('circle of protection: green'/'TMP', '4866').

card_in_set('circle of protection: red', 'TMP').
card_original_type('circle of protection: red'/'TMP', 'Enchantment').
card_original_text('circle of protection: red'/'TMP', '{1}: Prevent all damage to you from a red source. (Treat further damage from that source normally.)').
card_image_name('circle of protection: red'/'TMP', 'circle of protection red').
card_uid('circle of protection: red'/'TMP', 'TMP:Circle of Protection: Red:circle of protection red').
card_rarity('circle of protection: red'/'TMP', 'Common').
card_artist('circle of protection: red'/'TMP', 'Harold McNeill').
card_multiverse_id('circle of protection: red'/'TMP', '4867').

card_in_set('circle of protection: shadow', 'TMP').
card_original_type('circle of protection: shadow'/'TMP', 'Enchantment').
card_original_text('circle of protection: shadow'/'TMP', '{1}: Prevent all damage to you from a creature with shadow. (Treat further damage from that source normally.)').
card_first_print('circle of protection: shadow', 'TMP').
card_image_name('circle of protection: shadow'/'TMP', 'circle of protection shadow').
card_uid('circle of protection: shadow'/'TMP', 'TMP:Circle of Protection: Shadow:circle of protection shadow').
card_rarity('circle of protection: shadow'/'TMP', 'Common').
card_artist('circle of protection: shadow'/'TMP', 'Harold McNeill').
card_multiverse_id('circle of protection: shadow'/'TMP', '4868').

card_in_set('circle of protection: white', 'TMP').
card_original_type('circle of protection: white'/'TMP', 'Enchantment').
card_original_text('circle of protection: white'/'TMP', '{1}: Prevent all damage to you from a white source. (Treat further damage from that source normally.)').
card_image_name('circle of protection: white'/'TMP', 'circle of protection white').
card_uid('circle of protection: white'/'TMP', 'TMP:Circle of Protection: White:circle of protection white').
card_rarity('circle of protection: white'/'TMP', 'Common').
card_artist('circle of protection: white'/'TMP', 'Harold McNeill').
card_multiverse_id('circle of protection: white'/'TMP', '4869').

card_in_set('clergy en-vec', 'TMP').
card_original_type('clergy en-vec'/'TMP', 'Summon — Cleric').
card_original_text('clergy en-vec'/'TMP', '{T}: Prevent 1 damage to any creature or player.').
card_first_print('clergy en-vec', 'TMP').
card_image_name('clergy en-vec'/'TMP', 'clergy en-vec').
card_uid('clergy en-vec'/'TMP', 'TMP:Clergy en-Vec:clergy en-vec').
card_rarity('clergy en-vec'/'TMP', 'Common').
card_artist('clergy en-vec'/'TMP', 'Heather Hudson').
card_flavor_text('clergy en-vec'/'TMP', '\"Faith\'s shield is hammered out by the blows of unbelievers.\"\n—Oracle en-Vec').
card_multiverse_id('clergy en-vec'/'TMP', '4870').

card_in_set('clot sliver', 'TMP').
card_original_type('clot sliver'/'TMP', 'Summon — Sliver').
card_original_text('clot sliver'/'TMP', 'Each Sliver gains \"{2}: Regenerate this creature.\"').
card_first_print('clot sliver', 'TMP').
card_image_name('clot sliver'/'TMP', 'clot sliver').
card_uid('clot sliver'/'TMP', 'TMP:Clot Sliver:clot sliver').
card_rarity('clot sliver'/'TMP', 'Common').
card_artist('clot sliver'/'TMP', 'Jeff Laubenstein').
card_flavor_text('clot sliver'/'TMP', '\"One would think I would be accustomed to unexpected returns.\"\n—Hanna, Weatherlight navigator').
card_multiverse_id('clot sliver'/'TMP', '4640').

card_in_set('cloudchaser eagle', 'TMP').
card_original_type('cloudchaser eagle'/'TMP', 'Summon — Bird').
card_original_text('cloudchaser eagle'/'TMP', 'Flying\nWhen Cloudchaser Eagle comes into play, destroy target enchantment.').
card_first_print('cloudchaser eagle', 'TMP').
card_image_name('cloudchaser eagle'/'TMP', 'cloudchaser eagle').
card_uid('cloudchaser eagle'/'TMP', 'TMP:Cloudchaser Eagle:cloudchaser eagle').
card_rarity('cloudchaser eagle'/'TMP', 'Common').
card_artist('cloudchaser eagle'/'TMP', 'Una Fricker').
card_flavor_text('cloudchaser eagle'/'TMP', 'When the eagle catches a cloud, it tears it into strips that fall to earth.\n—Vec myth of the rains').
card_multiverse_id('cloudchaser eagle'/'TMP', '4871').

card_in_set('coercion', 'TMP').
card_original_type('coercion'/'TMP', 'Sorcery').
card_original_text('coercion'/'TMP', 'Look at target opponent\'s hand and choose one of those cards. That player discards that card.').
card_image_name('coercion'/'TMP', 'coercion').
card_uid('coercion'/'TMP', 'TMP:Coercion:coercion').
card_rarity('coercion'/'TMP', 'Common').
card_artist('coercion'/'TMP', 'Pete Venters').
card_flavor_text('coercion'/'TMP', '\"There\'s very little that escapes me, Greven. And you will escape very little if you fail.\"\n—Volrath').
card_multiverse_id('coercion'/'TMP', '4641').

card_in_set('coffin queen', 'TMP').
card_original_type('coffin queen'/'TMP', 'Summon — Wizard').
card_original_text('coffin queen'/'TMP', 'You may choose not to untap Coffin Queen during your untap phase.\n{2}{B}, {T}: Put target creature card from any graveyard into play under your control. Remove that creature from the game if Coffin Queen becomes untapped or if you lose control of Coffin Queen.').
card_first_print('coffin queen', 'TMP').
card_image_name('coffin queen'/'TMP', 'coffin queen').
card_uid('coffin queen'/'TMP', 'TMP:Coffin Queen:coffin queen').
card_rarity('coffin queen'/'TMP', 'Rare').
card_artist('coffin queen'/'TMP', 'Kaja Foglio').
card_multiverse_id('coffin queen'/'TMP', '4642').

card_in_set('coiled tinviper', 'TMP').
card_original_type('coiled tinviper'/'TMP', 'Artifact Creature').
card_original_text('coiled tinviper'/'TMP', 'First strike').
card_first_print('coiled tinviper', 'TMP').
card_image_name('coiled tinviper'/'TMP', 'coiled tinviper').
card_uid('coiled tinviper'/'TMP', 'TMP:Coiled Tinviper:coiled tinviper').
card_rarity('coiled tinviper'/'TMP', 'Common').
card_artist('coiled tinviper'/'TMP', 'John Matson').
card_flavor_text('coiled tinviper'/'TMP', 'The bite of the tinviper feels most like a razor drawn across the tongue.').
card_multiverse_id('coiled tinviper'/'TMP', '4599').

card_in_set('cold storage', 'TMP').
card_original_type('cold storage'/'TMP', 'Artifact').
card_original_text('cold storage'/'TMP', '{3}: Put target creature you control on Cold Storage.\nSacrifice Cold Storage: Put all creature cards on Cold Storage into play.').
card_first_print('cold storage', 'TMP').
card_image_name('cold storage'/'TMP', 'cold storage').
card_uid('cold storage'/'TMP', 'TMP:Cold Storage:cold storage').
card_rarity('cold storage'/'TMP', 'Rare').
card_artist('cold storage'/'TMP', 'Greg Simanson').
card_multiverse_id('cold storage'/'TMP', '4600').

card_in_set('commander greven il-vec', 'TMP').
card_original_type('commander greven il-vec'/'TMP', 'Summon — Legend').
card_original_text('commander greven il-vec'/'TMP', 'When Commander Greven il-Vec comes into play, sacrifice a creature.\nGreven cannot be blocked except by artifact creatures and black creatures.').
card_first_print('commander greven il-vec', 'TMP').
card_image_name('commander greven il-vec'/'TMP', 'commander greven il-vec').
card_uid('commander greven il-vec'/'TMP', 'TMP:Commander Greven il-Vec:commander greven il-vec').
card_rarity('commander greven il-vec'/'TMP', 'Rare').
card_artist('commander greven il-vec'/'TMP', 'Kev Walker').
card_flavor_text('commander greven il-vec'/'TMP', '\"Rage is the only freedom left me.\"\n—Greven il-Vec').
card_multiverse_id('commander greven il-vec'/'TMP', '4643').

card_in_set('corpse dance', 'TMP').
card_original_type('corpse dance'/'TMP', 'Instant').
card_original_text('corpse dance'/'TMP', 'Buyback {2} (You may pay an additional {2} when you play this spell. If you do, put it into your hand instead of your graveyard as part of the spell\'s effect.)\nPut the top creature card from your graveyard into play. That creature is unaffected by summoning sickness this turn. Remove the creature from the game at end of turn.').
card_first_print('corpse dance', 'TMP').
card_image_name('corpse dance'/'TMP', 'corpse dance').
card_uid('corpse dance'/'TMP', 'TMP:Corpse Dance:corpse dance').
card_rarity('corpse dance'/'TMP', 'Rare').
card_artist('corpse dance'/'TMP', 'Brian Snõddy').
card_multiverse_id('corpse dance'/'TMP', '4644').

card_in_set('counterspell', 'TMP').
card_original_type('counterspell'/'TMP', 'Interrupt').
card_original_text('counterspell'/'TMP', 'Counter target spell.').
card_image_name('counterspell'/'TMP', 'counterspell').
card_uid('counterspell'/'TMP', 'TMP:Counterspell:counterspell').
card_rarity('counterspell'/'TMP', 'Common').
card_artist('counterspell'/'TMP', 'Stephen Daniele').
card_flavor_text('counterspell'/'TMP', '\"It was probably a lousy spell in the first place.\"\n—Ertai, wizard adept').
card_multiverse_id('counterspell'/'TMP', '4693').

card_in_set('crazed armodon', 'TMP').
card_original_type('crazed armodon'/'TMP', 'Summon — Elephant').
card_original_text('crazed armodon'/'TMP', '{G}: Crazed Armodon gets +3/+0 and gains trample until end of turn. At end of turn, destroy Crazed Armodon. Use this ability only once each turn.').
card_first_print('crazed armodon', 'TMP').
card_image_name('crazed armodon'/'TMP', 'crazed armodon').
card_uid('crazed armodon'/'TMP', 'TMP:Crazed Armodon:crazed armodon').
card_rarity('crazed armodon'/'TMP', 'Rare').
card_artist('crazed armodon'/'TMP', 'Gary Leach').
card_flavor_text('crazed armodon'/'TMP', 'These are its last days. Mourning is over. Rage remains.').
card_multiverse_id('crazed armodon'/'TMP', '4754').

card_in_set('crown of flames', 'TMP').
card_original_type('crown of flames'/'TMP', 'Enchant Creature').
card_original_text('crown of flames'/'TMP', '{R}: Enchanted creature gets +1/+0 until end of turn.\n{R}: Return Crown of Flames to owner\'s hand.').
card_first_print('crown of flames', 'TMP').
card_image_name('crown of flames'/'TMP', 'crown of flames').
card_uid('crown of flames'/'TMP', 'TMP:Crown of Flames:crown of flames').
card_rarity('crown of flames'/'TMP', 'Common').
card_artist('crown of flames'/'TMP', 'William O\'Connor').
card_flavor_text('crown of flames'/'TMP', 'It is the forge of kings.').
card_multiverse_id('crown of flames'/'TMP', '4809').

card_in_set('cursed scroll', 'TMP').
card_original_type('cursed scroll'/'TMP', 'Artifact').
card_original_text('cursed scroll'/'TMP', '{3}, {T}: Name a card. Target opponent chooses a card at random from your hand. If he or she chooses the named card, Cursed Scroll deals 2 damage to target creature or player.').
card_first_print('cursed scroll', 'TMP').
card_image_name('cursed scroll'/'TMP', 'cursed scroll').
card_uid('cursed scroll'/'TMP', 'TMP:Cursed Scroll:cursed scroll').
card_rarity('cursed scroll'/'TMP', 'Rare').
card_artist('cursed scroll'/'TMP', 'D. Alexander Gregory').
card_multiverse_id('cursed scroll'/'TMP', '4601').

card_in_set('dark banishing', 'TMP').
card_original_type('dark banishing'/'TMP', 'Instant').
card_original_text('dark banishing'/'TMP', 'Destroy target nonblack creature. That creature cannot be regenerated this turn.').
card_image_name('dark banishing'/'TMP', 'dark banishing').
card_uid('dark banishing'/'TMP', 'TMP:Dark Banishing:dark banishing').
card_rarity('dark banishing'/'TMP', 'Common').
card_artist('dark banishing'/'TMP', 'John Matson').
card_flavor_text('dark banishing'/'TMP', '\"It is the way of most wizards to begin by exiling themselves and to end by exiling everyone else.\"\n—Hanna, Weatherlight navigator').
card_multiverse_id('dark banishing'/'TMP', '4645').

card_in_set('dark ritual', 'TMP').
card_original_type('dark ritual'/'TMP', 'Mana Source').
card_original_text('dark ritual'/'TMP', 'Add {B}{B}{B} to your mana pool.').
card_image_name('dark ritual'/'TMP', 'dark ritual').
card_uid('dark ritual'/'TMP', 'TMP:Dark Ritual:dark ritual').
card_rarity('dark ritual'/'TMP', 'Common').
card_artist('dark ritual'/'TMP', 'Ken Meyer, Jr.').
card_flavor_text('dark ritual'/'TMP', '\"If there is such a thing as too much power, I have not discovered it.\"\n—Volrath').
card_multiverse_id('dark ritual'/'TMP', '4646').

card_in_set('darkling stalker', 'TMP').
card_original_type('darkling stalker'/'TMP', 'Summon — Spirit').
card_original_text('darkling stalker'/'TMP', '{B}: Regenerate Darkling Stalker.\n{B}: Darkling Stalker gets +1/+1 until end of turn.').
card_first_print('darkling stalker', 'TMP').
card_image_name('darkling stalker'/'TMP', 'darkling stalker').
card_uid('darkling stalker'/'TMP', 'TMP:Darkling Stalker:darkling stalker').
card_rarity('darkling stalker'/'TMP', 'Common').
card_artist('darkling stalker'/'TMP', 'Susan Van Camp').
card_flavor_text('darkling stalker'/'TMP', '\"In this dark place, yes, I am afraid of my own shadow.\"\n—Mirri of the Weatherlight').
card_multiverse_id('darkling stalker'/'TMP', '4647').

card_in_set('dauthi embrace', 'TMP').
card_original_type('dauthi embrace'/'TMP', 'Enchantment').
card_original_text('dauthi embrace'/'TMP', '{B}{B}: Target creature gains shadow until end of turn. (This creature can block or be blocked by only creatures with shadow.)').
card_first_print('dauthi embrace', 'TMP').
card_image_name('dauthi embrace'/'TMP', 'dauthi embrace').
card_uid('dauthi embrace'/'TMP', 'TMP:Dauthi Embrace:dauthi embrace').
card_rarity('dauthi embrace'/'TMP', 'Uncommon').
card_artist('dauthi embrace'/'TMP', 'Andrew Robinson').
card_flavor_text('dauthi embrace'/'TMP', '\"The Dauthi army grows by screams and bounds.\"\n—Lyna, Soltari emissary').
card_multiverse_id('dauthi embrace'/'TMP', '4648').

card_in_set('dauthi ghoul', 'TMP').
card_original_type('dauthi ghoul'/'TMP', 'Summon — Zombie').
card_original_text('dauthi ghoul'/'TMP', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nWhenever any creature with shadow is put into any graveyard from play, put a +1/+1 counter on Dauthi Ghoul.').
card_first_print('dauthi ghoul', 'TMP').
card_image_name('dauthi ghoul'/'TMP', 'dauthi ghoul').
card_uid('dauthi ghoul'/'TMP', 'TMP:Dauthi Ghoul:dauthi ghoul').
card_rarity('dauthi ghoul'/'TMP', 'Uncommon').
card_artist('dauthi ghoul'/'TMP', 'Tom Kyffin').
card_multiverse_id('dauthi ghoul'/'TMP', '4649').

card_in_set('dauthi horror', 'TMP').
card_original_type('dauthi horror'/'TMP', 'Summon — Beast').
card_original_text('dauthi horror'/'TMP', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nDauthi Horror cannot be blocked by white creatures.').
card_first_print('dauthi horror', 'TMP').
card_image_name('dauthi horror'/'TMP', 'dauthi horror').
card_uid('dauthi horror'/'TMP', 'TMP:Dauthi Horror:dauthi horror').
card_rarity('dauthi horror'/'TMP', 'Common').
card_artist('dauthi horror'/'TMP', 'Jeff Laubenstein').
card_multiverse_id('dauthi horror'/'TMP', '4650').

card_in_set('dauthi marauder', 'TMP').
card_original_type('dauthi marauder'/'TMP', 'Summon — Minion').
card_original_text('dauthi marauder'/'TMP', 'Shadow (This creature can block or be blocked by only creatures with shadow.)').
card_first_print('dauthi marauder', 'TMP').
card_image_name('dauthi marauder'/'TMP', 'dauthi marauder').
card_uid('dauthi marauder'/'TMP', 'TMP:Dauthi Marauder:dauthi marauder').
card_rarity('dauthi marauder'/'TMP', 'Common').
card_artist('dauthi marauder'/'TMP', 'Andrew Robinson').
card_flavor_text('dauthi marauder'/'TMP', '\"The Dauthi came from beneath the Ruins one night, and the darkness cast them in the best possible light.\"\n—Soltari Tales of Life').
card_multiverse_id('dauthi marauder'/'TMP', '4651').

card_in_set('dauthi mercenary', 'TMP').
card_original_type('dauthi mercenary'/'TMP', 'Summon — Knight').
card_original_text('dauthi mercenary'/'TMP', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\n{1}{B}: Dauthi Mercenary gets +1/+0 until end of turn.').
card_first_print('dauthi mercenary', 'TMP').
card_image_name('dauthi mercenary'/'TMP', 'dauthi mercenary').
card_uid('dauthi mercenary'/'TMP', 'TMP:Dauthi Mercenary:dauthi mercenary').
card_rarity('dauthi mercenary'/'TMP', 'Uncommon').
card_artist('dauthi mercenary'/'TMP', 'Matthew D. Wilson').
card_flavor_text('dauthi mercenary'/'TMP', '\"The Dauthi believe they dignify murder by paying for it.\"\n—Lyna, Soltari emissary').
card_multiverse_id('dauthi mercenary'/'TMP', '4652').

card_in_set('dauthi mindripper', 'TMP').
card_original_type('dauthi mindripper'/'TMP', 'Summon — Minion').
card_original_text('dauthi mindripper'/'TMP', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nSacrifice Dauthi Mindripper: Defending player chooses and discards three cards. Use this ability only if Dauthi Mindripper is attacking and unblocked.').
card_first_print('dauthi mindripper', 'TMP').
card_image_name('dauthi mindripper'/'TMP', 'dauthi mindripper').
card_uid('dauthi mindripper'/'TMP', 'TMP:Dauthi Mindripper:dauthi mindripper').
card_rarity('dauthi mindripper'/'TMP', 'Uncommon').
card_artist('dauthi mindripper'/'TMP', 'L. A. Williams').
card_multiverse_id('dauthi mindripper'/'TMP', '4653').

card_in_set('dauthi slayer', 'TMP').
card_original_type('dauthi slayer'/'TMP', 'Summon — Soldier').
card_original_text('dauthi slayer'/'TMP', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nEach turn, Dauthi Slayer attacks if able.').
card_image_name('dauthi slayer'/'TMP', 'dauthi slayer').
card_uid('dauthi slayer'/'TMP', 'TMP:Dauthi Slayer:dauthi slayer').
card_rarity('dauthi slayer'/'TMP', 'Common').
card_artist('dauthi slayer'/'TMP', 'Dermot Power').
card_flavor_text('dauthi slayer'/'TMP', '\"They have knives for every soul.\"\n—Lyna, Soltari emissary').
card_multiverse_id('dauthi slayer'/'TMP', '4654').

card_in_set('deadshot', 'TMP').
card_original_type('deadshot'/'TMP', 'Sorcery').
card_original_text('deadshot'/'TMP', 'Tap target creature. That creature deals damage equal to its power to another target creature.').
card_first_print('deadshot', 'TMP').
card_image_name('deadshot'/'TMP', 'deadshot').
card_uid('deadshot'/'TMP', 'TMP:Deadshot:deadshot').
card_rarity('deadshot'/'TMP', 'Rare').
card_artist('deadshot'/'TMP', 'Heather Hudson').
card_flavor_text('deadshot'/'TMP', '\"Carrion! Keep your distance. My blade will come to you!\"\n—Crovax').
card_multiverse_id('deadshot'/'TMP', '4810').

card_in_set('death pits of rath', 'TMP').
card_original_type('death pits of rath'/'TMP', 'Enchantment').
card_original_text('death pits of rath'/'TMP', 'Whenever any creature is dealt damage, destroy it. That creature cannot be regenerated this turn.').
card_first_print('death pits of rath', 'TMP').
card_image_name('death pits of rath'/'TMP', 'death pits of rath').
card_uid('death pits of rath'/'TMP', 'TMP:Death Pits of Rath:death pits of rath').
card_rarity('death pits of rath'/'TMP', 'Rare').
card_artist('death pits of rath'/'TMP', 'Joel Biske').
card_flavor_text('death pits of rath'/'TMP', 'As the sludge below began to shift and take shapes, Gerrard turned from the railing to Orim. \"I suppose,\" he said, \"it\'s a little too late for prayer, isn\'t it?\"').
card_multiverse_id('death pits of rath'/'TMP', '4655').

card_in_set('diabolic edict', 'TMP').
card_original_type('diabolic edict'/'TMP', 'Instant').
card_original_text('diabolic edict'/'TMP', 'Target player sacrifices a creature.').
card_image_name('diabolic edict'/'TMP', 'diabolic edict').
card_uid('diabolic edict'/'TMP', 'TMP:Diabolic Edict:diabolic edict').
card_rarity('diabolic edict'/'TMP', 'Common').
card_artist('diabolic edict'/'TMP', 'Ron Spencer').
card_flavor_text('diabolic edict'/'TMP', 'Greven il-Vec lifted Vhati off his feet. \"The fall will give you time to think on your failure.\"').
card_multiverse_id('diabolic edict'/'TMP', '4656').

card_in_set('dirtcowl wurm', 'TMP').
card_original_type('dirtcowl wurm'/'TMP', 'Summon — Wurm').
card_original_text('dirtcowl wurm'/'TMP', 'Whenever any opponent plays a land, put a +1/+1 counter on Dirtcowl Wurm.').
card_image_name('dirtcowl wurm'/'TMP', 'dirtcowl wurm').
card_uid('dirtcowl wurm'/'TMP', 'TMP:Dirtcowl Wurm:dirtcowl wurm').
card_rarity('dirtcowl wurm'/'TMP', 'Rare').
card_artist('dirtcowl wurm'/'TMP', 'Dan Frazier').
card_flavor_text('dirtcowl wurm'/'TMP', 'Few of Rath\'s rivers follow a steady course, so many new channels are carved for them.').
card_multiverse_id('dirtcowl wurm'/'TMP', '4755').

card_in_set('disenchant', 'TMP').
card_original_type('disenchant'/'TMP', 'Instant').
card_original_text('disenchant'/'TMP', 'Destroy target artifact or enchantment.').
card_image_name('disenchant'/'TMP', 'disenchant').
card_uid('disenchant'/'TMP', 'TMP:Disenchant:disenchant').
card_rarity('disenchant'/'TMP', 'Common').
card_artist('disenchant'/'TMP', 'L. A. Williams').
card_flavor_text('disenchant'/'TMP', 'The swarm seemed unmoved by the artificial slivers\' destruction. \"They clearly were not the leaders,\" Hanna cursed. \"I guess we\'ll have to do this the hard way.\"').
card_multiverse_id('disenchant'/'TMP', '4872').

card_in_set('dismiss', 'TMP').
card_original_type('dismiss'/'TMP', 'Interrupt').
card_original_text('dismiss'/'TMP', 'Counter target spell. \nDraw a card.').
card_image_name('dismiss'/'TMP', 'dismiss').
card_uid('dismiss'/'TMP', 'TMP:Dismiss:dismiss').
card_rarity('dismiss'/'TMP', 'Uncommon').
card_artist('dismiss'/'TMP', 'Donato Giancola').
card_flavor_text('dismiss'/'TMP', '\"There is nothing you can do that I cannot simply deny.\"\n—Ertai, wizard adept').
card_multiverse_id('dismiss'/'TMP', '4695').

card_in_set('disturbed burial', 'TMP').
card_original_type('disturbed burial'/'TMP', 'Sorcery').
card_original_text('disturbed burial'/'TMP', 'Buyback {3} (You may pay an additional {3} when you play this spell. If you do, put it into your hand instead of your graveyard as part of the spell\'s effect.)\nReturn target creature card from your graveyard to your hand.').
card_first_print('disturbed burial', 'TMP').
card_image_name('disturbed burial'/'TMP', 'disturbed burial').
card_uid('disturbed burial'/'TMP', 'TMP:Disturbed Burial:disturbed burial').
card_rarity('disturbed burial'/'TMP', 'Common').
card_artist('disturbed burial'/'TMP', 'Heather Hudson').
card_multiverse_id('disturbed burial'/'TMP', '4657').

card_in_set('dracoplasm', 'TMP').
card_original_type('dracoplasm'/'TMP', 'Summon — Shapeshifter').
card_original_text('dracoplasm'/'TMP', 'Flying\nWhen you play Dracoplasm, sacrifice any number of creatures.\nDracoplasm comes into play with power equal to the total power of the sacrificed creatures and toughness equal to the total toughness of those creatures.\n{R}: Dracoplasm gets +1/+0 until end of turn.').
card_first_print('dracoplasm', 'TMP').
card_image_name('dracoplasm'/'TMP', 'dracoplasm').
card_uid('dracoplasm'/'TMP', 'TMP:Dracoplasm:dracoplasm').
card_rarity('dracoplasm'/'TMP', 'Rare').
card_artist('dracoplasm'/'TMP', 'Andrew Robinson').
card_multiverse_id('dracoplasm'/'TMP', '4912').

card_in_set('dread of night', 'TMP').
card_original_type('dread of night'/'TMP', 'Enchantment').
card_original_text('dread of night'/'TMP', 'All white creatures get -1/-1.').
card_first_print('dread of night', 'TMP').
card_image_name('dread of night'/'TMP', 'dread of night').
card_uid('dread of night'/'TMP', 'TMP:Dread of Night:dread of night').
card_rarity('dread of night'/'TMP', 'Uncommon').
card_artist('dread of night'/'TMP', 'Richard Thomas').
card_flavor_text('dread of night'/'TMP', '\"These moonless, foreign skies keep me in thrall. Dark whispers echo in the night, and I cannot resist.\"\n—Selenia, dark angel').
card_multiverse_id('dread of night'/'TMP', '4658').

card_in_set('dream cache', 'TMP').
card_original_type('dream cache'/'TMP', 'Sorcery').
card_original_text('dream cache'/'TMP', 'Draw three cards. Choose two cards from your hand and put both on either the top or the bottom of your library.').
card_image_name('dream cache'/'TMP', 'dream cache').
card_uid('dream cache'/'TMP', 'TMP:Dream Cache:dream cache').
card_rarity('dream cache'/'TMP', 'Common').
card_artist('dream cache'/'TMP', 'Phil Foglio').
card_flavor_text('dream cache'/'TMP', '\"The sanctity of poverty is an invention of the rich.\"\n—Starke').
card_multiverse_id('dream cache'/'TMP', '4696').

card_in_set('dregs of sorrow', 'TMP').
card_original_type('dregs of sorrow'/'TMP', 'Sorcery').
card_original_text('dregs of sorrow'/'TMP', 'Destroy X target nonblack creatures. Draw X cards.').
card_first_print('dregs of sorrow', 'TMP').
card_image_name('dregs of sorrow'/'TMP', 'dregs of sorrow').
card_uid('dregs of sorrow'/'TMP', 'TMP:Dregs of Sorrow:dregs of sorrow').
card_rarity('dregs of sorrow'/'TMP', 'Rare').
card_artist('dregs of sorrow'/'TMP', 'Thomas Gianni').
card_flavor_text('dregs of sorrow'/'TMP', 'Crovax gazed upon the dead, and for one dark moment he saw a banquet.').
card_multiverse_id('dregs of sorrow'/'TMP', '4659').

card_in_set('duplicity', 'TMP').
card_original_type('duplicity'/'TMP', 'Enchantment').
card_original_text('duplicity'/'TMP', 'When Duplicity comes into play, put the top five cards of your library face down on Duplicity.\nDuring your upkeep, you may exchange all the cards in your hand for the cards on Duplicity.\nAt the end of your turn, choose and discard a card.\nIf you lose control of Duplicity, put all cards on it into owner\'s graveyard.').
card_first_print('duplicity', 'TMP').
card_image_name('duplicity'/'TMP', 'duplicity').
card_uid('duplicity'/'TMP', 'TMP:Duplicity:duplicity').
card_rarity('duplicity'/'TMP', 'Rare').
card_artist('duplicity'/'TMP', 'Dan Frazier').
card_multiverse_id('duplicity'/'TMP', '4697').

card_in_set('earthcraft', 'TMP').
card_original_type('earthcraft'/'TMP', 'Enchantment').
card_original_text('earthcraft'/'TMP', 'Tap an untapped creature you control: Untap target basic land.').
card_first_print('earthcraft', 'TMP').
card_image_name('earthcraft'/'TMP', 'earthcraft').
card_uid('earthcraft'/'TMP', 'TMP:Earthcraft:earthcraft').
card_rarity('earthcraft'/'TMP', 'Rare').
card_artist('earthcraft'/'TMP', 'Randy Gallegos').
card_flavor_text('earthcraft'/'TMP', '\"The land gives up little, but we are masters of persuasion.\"\n—Eladamri, Lord of Leaves').
card_multiverse_id('earthcraft'/'TMP', '4756').

card_in_set('echo chamber', 'TMP').
card_original_type('echo chamber'/'TMP', 'Artifact').
card_original_text('echo chamber'/'TMP', '{4}, {T}: Target opponent chooses target creature he or she controls. Put a token creature into play and treat it as a copy of that creature. The token creature is unaffected by summoning sickness this turn. At end of turn, remove the token creature from the game. Play this ability as a sorcery.').
card_first_print('echo chamber', 'TMP').
card_image_name('echo chamber'/'TMP', 'echo chamber').
card_uid('echo chamber'/'TMP', 'TMP:Echo Chamber:echo chamber').
card_rarity('echo chamber'/'TMP', 'Rare').
card_artist('echo chamber'/'TMP', 'Donato Giancola').
card_multiverse_id('echo chamber'/'TMP', '4602').

card_in_set('eladamri\'s vineyard', 'TMP').
card_original_type('eladamri\'s vineyard'/'TMP', 'Enchantment').
card_original_text('eladamri\'s vineyard'/'TMP', 'At the beginning of each player\'s main phase, add {G}{G} to that player\'s mana pool.').
card_first_print('eladamri\'s vineyard', 'TMP').
card_image_name('eladamri\'s vineyard'/'TMP', 'eladamri\'s vineyard').
card_uid('eladamri\'s vineyard'/'TMP', 'TMP:Eladamri\'s Vineyard:eladamri\'s vineyard').
card_rarity('eladamri\'s vineyard'/'TMP', 'Rare').
card_artist('eladamri\'s vineyard'/'TMP', 'Ron Chironna').
card_flavor_text('eladamri\'s vineyard'/'TMP', 'The mushroom wine made Gerrard\'s head spin, and his stomach soon followed suit.').
card_multiverse_id('eladamri\'s vineyard'/'TMP', '4758').

card_in_set('eladamri, lord of leaves', 'TMP').
card_original_type('eladamri, lord of leaves'/'TMP', 'Summon — Legend').
card_original_text('eladamri, lord of leaves'/'TMP', 'All Elves gain forestwalk. (If defending player controls any forests, those creatures are unblockable.)\nElves cannot be the target of spells or abilities.').
card_first_print('eladamri, lord of leaves', 'TMP').
card_image_name('eladamri, lord of leaves'/'TMP', 'eladamri, lord of leaves').
card_uid('eladamri, lord of leaves'/'TMP', 'TMP:Eladamri, Lord of Leaves:eladamri, lord of leaves').
card_rarity('eladamri, lord of leaves'/'TMP', 'Rare').
card_artist('eladamri, lord of leaves'/'TMP', 'Ron Chironna').
card_flavor_text('eladamri, lord of leaves'/'TMP', '\"We have been patient. We have planned our attack. We are ready . . . now.\"\n—Eladamri, Lord of Leaves').
card_multiverse_id('eladamri, lord of leaves'/'TMP', '4757').

card_in_set('elite javelineer', 'TMP').
card_original_type('elite javelineer'/'TMP', 'Summon — Soldier').
card_original_text('elite javelineer'/'TMP', 'If Elite Javelineer blocks, it deals 1 damage to target attacking creature.').
card_first_print('elite javelineer', 'TMP').
card_image_name('elite javelineer'/'TMP', 'elite javelineer').
card_uid('elite javelineer'/'TMP', 'TMP:Elite Javelineer:elite javelineer').
card_rarity('elite javelineer'/'TMP', 'Common').
card_artist('elite javelineer'/'TMP', 'Mark Poole').
card_flavor_text('elite javelineer'/'TMP', '\"Precision is frequently more valuable than force.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('elite javelineer'/'TMP', '4873').

card_in_set('elven warhounds', 'TMP').
card_original_type('elven warhounds'/'TMP', 'Summon — Hounds').
card_original_text('elven warhounds'/'TMP', 'If Elven Warhounds is blocked by any creature, put that creature on top of owner\'s library.').
card_first_print('elven warhounds', 'TMP').
card_image_name('elven warhounds'/'TMP', 'elven warhounds').
card_uid('elven warhounds'/'TMP', 'TMP:Elven Warhounds:elven warhounds').
card_rarity('elven warhounds'/'TMP', 'Rare').
card_artist('elven warhounds'/'TMP', 'Kev Walker').
card_flavor_text('elven warhounds'/'TMP', '\"If a creature is dumb enough to love you for leashing it, why would you want to follow its lead?\"\n—Mirri of the Weatherlight').
card_multiverse_id('elven warhounds'/'TMP', '4759').

card_in_set('elvish fury', 'TMP').
card_original_type('elvish fury'/'TMP', 'Instant').
card_original_text('elvish fury'/'TMP', 'Buyback {4} (You may pay an additional {4} when you play this spell. If you do, put it into your hand instead of your graveyard as part of the spell\'s effect.) \nTarget creature gets +2/+2 until end of turn.').
card_first_print('elvish fury', 'TMP').
card_image_name('elvish fury'/'TMP', 'elvish fury').
card_uid('elvish fury'/'TMP', 'TMP:Elvish Fury:elvish fury').
card_rarity('elvish fury'/'TMP', 'Common').
card_artist('elvish fury'/'TMP', 'Quinton Hoover').
card_multiverse_id('elvish fury'/'TMP', '4760').

card_in_set('emerald medallion', 'TMP').
card_original_type('emerald medallion'/'TMP', 'Artifact').
card_original_text('emerald medallion'/'TMP', 'Your green spells cost {1} less to play.').
card_first_print('emerald medallion', 'TMP').
card_image_name('emerald medallion'/'TMP', 'emerald medallion').
card_uid('emerald medallion'/'TMP', 'TMP:Emerald Medallion:emerald medallion').
card_rarity('emerald medallion'/'TMP', 'Rare').
card_artist('emerald medallion'/'TMP', 'Sue Ellen Brown').
card_multiverse_id('emerald medallion'/'TMP', '4603').

card_in_set('emmessi tome', 'TMP').
card_original_type('emmessi tome'/'TMP', 'Artifact').
card_original_text('emmessi tome'/'TMP', '{5}, {T}: Draw two cards, then choose and discard a card.').
card_first_print('emmessi tome', 'TMP').
card_image_name('emmessi tome'/'TMP', 'emmessi tome').
card_uid('emmessi tome'/'TMP', 'TMP:Emmessi Tome:emmessi tome').
card_rarity('emmessi tome'/'TMP', 'Rare').
card_artist('emmessi tome'/'TMP', 'Tom Wänerstrand').
card_flavor_text('emmessi tome'/'TMP', 'It is like life and destiny: we think we know the story, but we have read only half the tale.').
card_multiverse_id('emmessi tome'/'TMP', '4604').

card_in_set('endless scream', 'TMP').
card_original_type('endless scream'/'TMP', 'Enchant Creature').
card_original_text('endless scream'/'TMP', 'Enchanted creature gets +X/+0.').
card_first_print('endless scream', 'TMP').
card_image_name('endless scream'/'TMP', 'endless scream').
card_uid('endless scream'/'TMP', 'TMP:Endless Scream:endless scream').
card_rarity('endless scream'/'TMP', 'Common').
card_artist('endless scream'/'TMP', 'Joel Biske').
card_flavor_text('endless scream'/'TMP', '\"I have made Crovax weep; now I will make him scream. The pain will not end, but will bind him to me.\"\n—Volrath').
card_multiverse_id('endless scream'/'TMP', '4660').

card_in_set('energizer', 'TMP').
card_original_type('energizer'/'TMP', 'Artifact Creature').
card_original_text('energizer'/'TMP', '{2}, {T}: Put a +1/+1 counter on Energizer.').
card_first_print('energizer', 'TMP').
card_image_name('energizer'/'TMP', 'energizer').
card_uid('energizer'/'TMP', 'TMP:Energizer:energizer').
card_rarity('energizer'/'TMP', 'Rare').
card_artist('energizer'/'TMP', 'Val Mayerik').
card_flavor_text('energizer'/'TMP', 'Like a kettle set to boil, it must eventually go off—on someone.').
card_multiverse_id('energizer'/'TMP', '4605').

card_in_set('enfeeblement', 'TMP').
card_original_type('enfeeblement'/'TMP', 'Enchant Creature').
card_original_text('enfeeblement'/'TMP', 'Enchanted creature gets -2/-2.').
card_image_name('enfeeblement'/'TMP', 'enfeeblement').
card_uid('enfeeblement'/'TMP', 'TMP:Enfeeblement:enfeeblement').
card_rarity('enfeeblement'/'TMP', 'Common').
card_artist('enfeeblement'/'TMP', 'D. Alexander Gregory').
card_flavor_text('enfeeblement'/'TMP', 'Crovax had witnessed Selenia lead the Predator to the Weatherlight. As the battle raged on deck, he felt his strength melt in the heat of her betrayal.').
card_multiverse_id('enfeeblement'/'TMP', '4661').

card_in_set('enraging licid', 'TMP').
card_original_type('enraging licid'/'TMP', 'Summon — Licid').
card_original_text('enraging licid'/'TMP', '{R}, {T}: Enraging Licid loses this ability and becomes a creature enchantment that reads \"Enchanted creature is unaffected by summoning sickness\" instead of a creature. Move Enraging Licid onto target creature. You may pay {R} to end this effect.').
card_first_print('enraging licid', 'TMP').
card_image_name('enraging licid'/'TMP', 'enraging licid').
card_uid('enraging licid'/'TMP', 'TMP:Enraging Licid:enraging licid').
card_rarity('enraging licid'/'TMP', 'Uncommon').
card_artist('enraging licid'/'TMP', 'Doug Chaffee').
card_multiverse_id('enraging licid'/'TMP', '4811').

card_in_set('ertai\'s meddling', 'TMP').
card_original_type('ertai\'s meddling'/'TMP', 'Interrupt').
card_original_text('ertai\'s meddling'/'TMP', 'When target spell is successfully cast, put X delay counters on it. X cannot be 0.\nDuring each upkeep of that spell\'s caster, remove a delay counter from the spell. If the spell has no delay counters on it, it resolves.').
card_first_print('ertai\'s meddling', 'TMP').
card_image_name('ertai\'s meddling'/'TMP', 'ertai\'s meddling').
card_uid('ertai\'s meddling'/'TMP', 'TMP:Ertai\'s Meddling:ertai\'s meddling').
card_rarity('ertai\'s meddling'/'TMP', 'Rare').
card_artist('ertai\'s meddling'/'TMP', 'Steve Luke').
card_multiverse_id('ertai\'s meddling'/'TMP', '4698').

card_in_set('escaped shapeshifter', 'TMP').
card_original_type('escaped shapeshifter'/'TMP', 'Summon — Shapeshifter').
card_original_text('escaped shapeshifter'/'TMP', 'As long as your opponent controls any creatures with flying, Escaped Shapeshifter gains flying. The same is true for first strike, trample, and protection from any color.').
card_first_print('escaped shapeshifter', 'TMP').
card_image_name('escaped shapeshifter'/'TMP', 'escaped shapeshifter').
card_uid('escaped shapeshifter'/'TMP', 'TMP:Escaped Shapeshifter:escaped shapeshifter').
card_rarity('escaped shapeshifter'/'TMP', 'Rare').
card_artist('escaped shapeshifter'/'TMP', 'Douglas Shuler').
card_multiverse_id('escaped shapeshifter'/'TMP', '4699').

card_in_set('essence bottle', 'TMP').
card_original_type('essence bottle'/'TMP', 'Artifact').
card_original_text('essence bottle'/'TMP', '{3}, {T}: Put an elixir counter on Essence Bottle.\n{T}, Remove all elixir counters from Essence Bottle: Gain 2 life for each elixir counter removed in this way.').
card_first_print('essence bottle', 'TMP').
card_image_name('essence bottle'/'TMP', 'essence bottle').
card_uid('essence bottle'/'TMP', 'TMP:Essence Bottle:essence bottle').
card_rarity('essence bottle'/'TMP', 'Uncommon').
card_artist('essence bottle'/'TMP', 'Donato Giancola').
card_multiverse_id('essence bottle'/'TMP', '4606').

card_in_set('evincar\'s justice', 'TMP').
card_original_type('evincar\'s justice'/'TMP', 'Sorcery').
card_original_text('evincar\'s justice'/'TMP', 'Buyback {3} (You may pay an additional {3} when you play this spell. If you do, put it into your hand instead of your graveyard as part of the spell\'s effect.)\nEvincar\'s Justice deals 2 damage to each creature and player.').
card_first_print('evincar\'s justice', 'TMP').
card_image_name('evincar\'s justice'/'TMP', 'evincar\'s justice').
card_uid('evincar\'s justice'/'TMP', 'TMP:Evincar\'s Justice:evincar\'s justice').
card_rarity('evincar\'s justice'/'TMP', 'Common').
card_artist('evincar\'s justice'/'TMP', 'Hannibal King').
card_multiverse_id('evincar\'s justice'/'TMP', '4662').

card_in_set('excavator', 'TMP').
card_original_type('excavator'/'TMP', 'Artifact').
card_original_text('excavator'/'TMP', '{T}, Sacrifice a basic land: Target creature gains that landwalk ability until end of turn. (If defending player controls any lands of that type, this creature is unblockable.)').
card_first_print('excavator', 'TMP').
card_image_name('excavator'/'TMP', 'excavator').
card_uid('excavator'/'TMP', 'TMP:Excavator:excavator').
card_rarity('excavator'/'TMP', 'Uncommon').
card_artist('excavator'/'TMP', 'Tom Kyffin').
card_multiverse_id('excavator'/'TMP', '4607').

card_in_set('extinction', 'TMP').
card_original_type('extinction'/'TMP', 'Sorcery').
card_original_text('extinction'/'TMP', 'Destroy all creatures of any creature type of your choice.').
card_first_print('extinction', 'TMP').
card_image_name('extinction'/'TMP', 'extinction').
card_uid('extinction'/'TMP', 'TMP:Extinction:extinction').
card_rarity('extinction'/'TMP', 'Rare').
card_artist('extinction'/'TMP', 'Una Fricker').
card_flavor_text('extinction'/'TMP', '\"I once had an entire race killed just to listen to the rattling of their dried bones as I waded through them.\"\n—Volrath').
card_multiverse_id('extinction'/'TMP', '4663').

card_in_set('fevered convulsions', 'TMP').
card_original_type('fevered convulsions'/'TMP', 'Enchantment').
card_original_text('fevered convulsions'/'TMP', '{2}{B}{B}: Put a -1/-1 counter on target creature.').
card_first_print('fevered convulsions', 'TMP').
card_image_name('fevered convulsions'/'TMP', 'fevered convulsions').
card_uid('fevered convulsions'/'TMP', 'TMP:Fevered Convulsions:fevered convulsions').
card_rarity('fevered convulsions'/'TMP', 'Rare').
card_artist('fevered convulsions'/'TMP', 'Jeff Miracola').
card_flavor_text('fevered convulsions'/'TMP', '\"Tell me again why you failed to capture Gerrard, you worthless pile of spine.\"\n—Volrath, to Greven').
card_multiverse_id('fevered convulsions'/'TMP', '4664').

card_in_set('field of souls', 'TMP').
card_original_type('field of souls'/'TMP', 'Enchantment').
card_original_text('field of souls'/'TMP', 'Whenever a nontoken creature is put into your graveyard from play, put an Essence token into play. Treat this token as a 1/1 white creature with flying.').
card_first_print('field of souls', 'TMP').
card_image_name('field of souls'/'TMP', 'field of souls').
card_uid('field of souls'/'TMP', 'TMP:Field of Souls:field of souls').
card_rarity('field of souls'/'TMP', 'Rare').
card_artist('field of souls'/'TMP', 'Richard Kane Ferguson').
card_multiverse_id('field of souls'/'TMP', '4874').

card_in_set('fighting drake', 'TMP').
card_original_type('fighting drake'/'TMP', 'Summon — Drake').
card_original_text('fighting drake'/'TMP', 'Flying').
card_first_print('fighting drake', 'TMP').
card_image_name('fighting drake'/'TMP', 'fighting drake').
card_uid('fighting drake'/'TMP', 'TMP:Fighting Drake:fighting drake').
card_rarity('fighting drake'/'TMP', 'Uncommon').
card_artist('fighting drake'/'TMP', 'DiTerlizzi').
card_flavor_text('fighting drake'/'TMP', 'Two things are needed to survive the harsh skies of Rath: a thick hide and a thicker skull.').
card_multiverse_id('fighting drake'/'TMP', '4700').

card_in_set('firefly', 'TMP').
card_original_type('firefly'/'TMP', 'Summon — Insect').
card_original_text('firefly'/'TMP', 'Flying\n{R}: Firefly gets +1/+0 until end of turn.').
card_first_print('firefly', 'TMP').
card_image_name('firefly'/'TMP', 'firefly').
card_uid('firefly'/'TMP', 'TMP:Firefly:firefly').
card_rarity('firefly'/'TMP', 'Uncommon').
card_artist('firefly'/'TMP', 'Stephen Daniele').
card_flavor_text('firefly'/'TMP', '\"If they don\'t pinch, they burn. Can\'t ya eat any of da bugs here?\"\n—Squee, goblin cabin hand').
card_multiverse_id('firefly'/'TMP', '4813').

card_in_set('fireslinger', 'TMP').
card_original_type('fireslinger'/'TMP', 'Summon — Wizard').
card_original_text('fireslinger'/'TMP', '{T}: Fireslinger deals 1 damage to target creature or player and 1 damage to you.').
card_first_print('fireslinger', 'TMP').
card_image_name('fireslinger'/'TMP', 'fireslinger').
card_uid('fireslinger'/'TMP', 'TMP:Fireslinger:fireslinger').
card_rarity('fireslinger'/'TMP', 'Common').
card_artist('fireslinger'/'TMP', 'Jeff Reitz').
card_flavor_text('fireslinger'/'TMP', '\"Remember the moral of the fireslinger fable: with power comes isolation.\"\n—Karn, silver golem').
card_multiverse_id('fireslinger'/'TMP', '4814').

card_in_set('flailing drake', 'TMP').
card_original_type('flailing drake'/'TMP', 'Summon — Drake').
card_original_text('flailing drake'/'TMP', 'Flying\nIf Flailing Drake blocks or is blocked by any creature, that creature gets +1/+1 until end of turn.').
card_first_print('flailing drake', 'TMP').
card_image_name('flailing drake'/'TMP', 'flailing drake').
card_uid('flailing drake'/'TMP', 'TMP:Flailing Drake:flailing drake').
card_rarity('flailing drake'/'TMP', 'Uncommon').
card_artist('flailing drake'/'TMP', 'Heather Hudson').
card_flavor_text('flailing drake'/'TMP', 'Too many choices will always distract a greedy beast.').
card_multiverse_id('flailing drake'/'TMP', '4761').

card_in_set('flickering ward', 'TMP').
card_original_type('flickering ward'/'TMP', 'Enchant Creature').
card_original_text('flickering ward'/'TMP', 'When you play Flickering Ward, choose a color.\nEnchanted creature gains protection from the chosen color.\n{W}: Return Flickering Ward to owner\'s hand.').
card_first_print('flickering ward', 'TMP').
card_image_name('flickering ward'/'TMP', 'flickering ward').
card_uid('flickering ward'/'TMP', 'TMP:Flickering Ward:flickering ward').
card_rarity('flickering ward'/'TMP', 'Uncommon').
card_artist('flickering ward'/'TMP', 'Greg Simanson').
card_multiverse_id('flickering ward'/'TMP', '4875').

card_in_set('flowstone giant', 'TMP').
card_original_type('flowstone giant'/'TMP', 'Summon — Giant').
card_original_text('flowstone giant'/'TMP', '{R}: Flowstone Giant gets +2/-2 until end of turn.').
card_first_print('flowstone giant', 'TMP').
card_image_name('flowstone giant'/'TMP', 'flowstone giant').
card_uid('flowstone giant'/'TMP', 'TMP:Flowstone Giant:flowstone giant').
card_rarity('flowstone giant'/'TMP', 'Common').
card_artist('flowstone giant'/'TMP', 'Joel Biske').
card_flavor_text('flowstone giant'/'TMP', 'When the first of these giants woke from the bedrock, he was still sleepy. He yawned and stretched until his legs grew so thin that they snapped like icicles in the sun.\n—Vec lore').
card_multiverse_id('flowstone giant'/'TMP', '4816').

card_in_set('flowstone salamander', 'TMP').
card_original_type('flowstone salamander'/'TMP', 'Summon — Salamander').
card_original_text('flowstone salamander'/'TMP', '{R}: Flowstone Salamander deals 1 damage to target creature blocking it.').
card_first_print('flowstone salamander', 'TMP').
card_image_name('flowstone salamander'/'TMP', 'flowstone salamander').
card_uid('flowstone salamander'/'TMP', 'TMP:Flowstone Salamander:flowstone salamander').
card_rarity('flowstone salamander'/'TMP', 'Uncommon').
card_artist('flowstone salamander'/'TMP', 'Daniel Gelon').
card_flavor_text('flowstone salamander'/'TMP', 'The Furnace of Rath is periodically illuminated by their sparkling yawns.').
card_multiverse_id('flowstone salamander'/'TMP', '4817').

card_in_set('flowstone sculpture', 'TMP').
card_original_type('flowstone sculpture'/'TMP', 'Artifact Creature').
card_original_text('flowstone sculpture'/'TMP', '{2}, Choose and discard a card: Flowstone Sculpture gains flying, first strike, or trample permanently, or put a +1/+1 counter on Flowstone Sculpture.').
card_first_print('flowstone sculpture', 'TMP').
card_image_name('flowstone sculpture'/'TMP', 'flowstone sculpture').
card_uid('flowstone sculpture'/'TMP', 'TMP:Flowstone Sculpture:flowstone sculpture').
card_rarity('flowstone sculpture'/'TMP', 'Rare').
card_artist('flowstone sculpture'/'TMP', 'Hannibal King').
card_flavor_text('flowstone sculpture'/'TMP', 'The sculptor, weary of his work, created art that would finish itself.').
card_multiverse_id('flowstone sculpture'/'TMP', '4608').

card_in_set('flowstone wyvern', 'TMP').
card_original_type('flowstone wyvern'/'TMP', 'Summon — Drake').
card_original_text('flowstone wyvern'/'TMP', 'Flying\n{R}: Flowstone Wyvern gets +2/-2 until end of turn.').
card_first_print('flowstone wyvern', 'TMP').
card_image_name('flowstone wyvern'/'TMP', 'flowstone wyvern').
card_uid('flowstone wyvern'/'TMP', 'TMP:Flowstone Wyvern:flowstone wyvern').
card_rarity('flowstone wyvern'/'TMP', 'Rare').
card_artist('flowstone wyvern'/'TMP', 'Stephen Daniele').
card_flavor_text('flowstone wyvern'/'TMP', '\"Where I come from, stone stays on the ground.\"\n—Tahngarth of the Weatherlight').
card_multiverse_id('flowstone wyvern'/'TMP', '4818').

card_in_set('fool\'s tome', 'TMP').
card_original_type('fool\'s tome'/'TMP', 'Artifact').
card_original_text('fool\'s tome'/'TMP', '{2}, {T}: Draw a card. Use this ability only if you have no cards in your hand.').
card_first_print('fool\'s tome', 'TMP').
card_image_name('fool\'s tome'/'TMP', 'fool\'s tome').
card_uid('fool\'s tome'/'TMP', 'TMP:Fool\'s Tome:fool\'s tome').
card_rarity('fool\'s tome'/'TMP', 'Rare').
card_artist('fool\'s tome'/'TMP', 'Julie Baroh').
card_flavor_text('fool\'s tome'/'TMP', 'Squee: \"What\'s that?\"\nErtai: \"It\'s a magical book.\"\nSquee: \"Am I smart enough ta use it?\"\nErtai: \"You could say that.\"').
card_multiverse_id('fool\'s tome'/'TMP', '4609').

card_in_set('forest', 'TMP').
card_original_type('forest'/'TMP', 'Land').
card_original_text('forest'/'TMP', '{T}: Add {G} to your mana pool.').
card_image_name('forest'/'TMP', 'forest1').
card_uid('forest'/'TMP', 'TMP:Forest:forest1').
card_rarity('forest'/'TMP', 'Basic Land').
card_artist('forest'/'TMP', 'Douglas Shuler').
card_multiverse_id('forest'/'TMP', '4928').

card_in_set('forest', 'TMP').
card_original_type('forest'/'TMP', 'Land').
card_original_text('forest'/'TMP', '{T}: Add {G} to your mana pool.').
card_image_name('forest'/'TMP', 'forest2').
card_uid('forest'/'TMP', 'TMP:Forest:forest2').
card_rarity('forest'/'TMP', 'Basic Land').
card_artist('forest'/'TMP', 'Douglas Shuler').
card_multiverse_id('forest'/'TMP', '4927').

card_in_set('forest', 'TMP').
card_original_type('forest'/'TMP', 'Land').
card_original_text('forest'/'TMP', '{T}: Add {G} to your mana pool.').
card_image_name('forest'/'TMP', 'forest3').
card_uid('forest'/'TMP', 'TMP:Forest:forest3').
card_rarity('forest'/'TMP', 'Basic Land').
card_artist('forest'/'TMP', 'Douglas Shuler').
card_multiverse_id('forest'/'TMP', '4926').

card_in_set('forest', 'TMP').
card_original_type('forest'/'TMP', 'Land').
card_original_text('forest'/'TMP', '{T}: Add {G} to your mana pool.').
card_image_name('forest'/'TMP', 'forest4').
card_uid('forest'/'TMP', 'TMP:Forest:forest4').
card_rarity('forest'/'TMP', 'Basic Land').
card_artist('forest'/'TMP', 'Douglas Shuler').
card_multiverse_id('forest'/'TMP', '4929').

card_in_set('frog tongue', 'TMP').
card_original_type('frog tongue'/'TMP', 'Enchant Creature').
card_original_text('frog tongue'/'TMP', 'When Frog Tongue comes into play, draw a card. \nEnchanted creature can block creatures with flying.').
card_first_print('frog tongue', 'TMP').
card_image_name('frog tongue'/'TMP', 'frog tongue').
card_uid('frog tongue'/'TMP', 'TMP:Frog Tongue:frog tongue').
card_rarity('frog tongue'/'TMP', 'Common').
card_artist('frog tongue'/'TMP', 'Phil Foglio').
card_flavor_text('frog tongue'/'TMP', '\"But why can\'t I get one?\" sniveled Squee. \"All da bugs here got wings.\"').
card_multiverse_id('frog tongue'/'TMP', '4762').

card_in_set('fugitive druid', 'TMP').
card_original_type('fugitive druid'/'TMP', 'Summon — Druid').
card_original_text('fugitive druid'/'TMP', 'Whenever any player successfully casts an enchantment spell that targets Fugitive Druid, draw a card.').
card_first_print('fugitive druid', 'TMP').
card_image_name('fugitive druid'/'TMP', 'fugitive druid').
card_uid('fugitive druid'/'TMP', 'TMP:Fugitive Druid:fugitive druid').
card_rarity('fugitive druid'/'TMP', 'Rare').
card_artist('fugitive druid'/'TMP', 'Quinton Hoover').
card_flavor_text('fugitive druid'/'TMP', '\"If the druids are so devoted to their trees, I will carve them coffins from their precious heartwood.\"\n—Volrath').
card_multiverse_id('fugitive druid'/'TMP', '4763').

card_in_set('furnace of rath', 'TMP').
card_original_type('furnace of rath'/'TMP', 'Enchantment').
card_original_text('furnace of rath'/'TMP', 'Double all damage assigned to any creature or player.').
card_first_print('furnace of rath', 'TMP').
card_image_name('furnace of rath'/'TMP', 'furnace of rath').
card_uid('furnace of rath'/'TMP', 'TMP:Furnace of Rath:furnace of rath').
card_rarity('furnace of rath'/'TMP', 'Rare').
card_artist('furnace of rath'/'TMP', 'John Matson').
card_multiverse_id('furnace of rath'/'TMP', '4819').

card_in_set('fylamarid', 'TMP').
card_original_type('fylamarid'/'TMP', 'Summon — Beast').
card_original_text('fylamarid'/'TMP', 'Flying\nFylamarid cannot be blocked by blue creatures.\n{U}: Target creature is blue until end of turn.').
card_first_print('fylamarid', 'TMP').
card_image_name('fylamarid'/'TMP', 'fylamarid').
card_uid('fylamarid'/'TMP', 'TMP:Fylamarid:fylamarid').
card_rarity('fylamarid'/'TMP', 'Uncommon').
card_artist('fylamarid'/'TMP', 'Una Fricker').
card_multiverse_id('fylamarid'/'TMP', '4701').

card_in_set('gallantry', 'TMP').
card_original_type('gallantry'/'TMP', 'Instant').
card_original_text('gallantry'/'TMP', 'Target blocking creature gets +4/+4 until end of turn.\nDraw a card.').
card_first_print('gallantry', 'TMP').
card_image_name('gallantry'/'TMP', 'gallantry').
card_uid('gallantry'/'TMP', 'TMP:Gallantry:gallantry').
card_rarity('gallantry'/'TMP', 'Uncommon').
card_artist('gallantry'/'TMP', 'Douglas Shuler').
card_flavor_text('gallantry'/'TMP', '\"Forgive me,\" Mirri whispered. \"I thought you a burden, and you saved my life.\" Hanna quietly accepted the apology.').
card_multiverse_id('gallantry'/'TMP', '4876').

card_in_set('gaseous form', 'TMP').
card_original_type('gaseous form'/'TMP', 'Enchant Creature').
card_original_text('gaseous form'/'TMP', 'Enchanted creature neither deals nor receives combat damage.').
card_image_name('gaseous form'/'TMP', 'gaseous form').
card_uid('gaseous form'/'TMP', 'TMP:Gaseous Form:gaseous form').
card_rarity('gaseous form'/'TMP', 'Common').
card_artist('gaseous form'/'TMP', 'Roger Raupp').
card_flavor_text('gaseous form'/'TMP', '\"It\'s hard to hurt anyone when you\'re as transparent as your motives.\"\n—Ertai, wizard adept').
card_multiverse_id('gaseous form'/'TMP', '4702').

card_in_set('gerrard\'s battle cry', 'TMP').
card_original_type('gerrard\'s battle cry'/'TMP', 'Enchantment').
card_original_text('gerrard\'s battle cry'/'TMP', '{2}{W}: All creatures you control get +1/+1 until end of turn.').
card_first_print('gerrard\'s battle cry', 'TMP').
card_image_name('gerrard\'s battle cry'/'TMP', 'gerrard\'s battle cry').
card_uid('gerrard\'s battle cry'/'TMP', 'TMP:Gerrard\'s Battle Cry:gerrard\'s battle cry').
card_rarity('gerrard\'s battle cry'/'TMP', 'Rare').
card_artist('gerrard\'s battle cry'/'TMP', 'Val Mayerik').
card_flavor_text('gerrard\'s battle cry'/'TMP', 'Gerrard grinned and drew his sword. \"This won\'t be a fair fight,\" he called to his crew. \"They should have brought a second ship!\"').
card_multiverse_id('gerrard\'s battle cry'/'TMP', '4877').

card_in_set('ghost town', 'TMP').
card_original_type('ghost town'/'TMP', 'Land').
card_original_text('ghost town'/'TMP', '{T}: Add one colorless mana to your mana pool.\n{0}: Return Ghost Town to owner\'s hand. Use this ability only during another player\'s turn.').
card_first_print('ghost town', 'TMP').
card_image_name('ghost town'/'TMP', 'ghost town').
card_uid('ghost town'/'TMP', 'TMP:Ghost Town:ghost town').
card_rarity('ghost town'/'TMP', 'Uncommon').
card_artist('ghost town'/'TMP', 'Tom Wänerstrand').
card_flavor_text('ghost town'/'TMP', '\"The air here smells like a grave.\"\n—Crovax').
card_multiverse_id('ghost town'/'TMP', '4932').

card_in_set('giant crab', 'TMP').
card_original_type('giant crab'/'TMP', 'Summon — Crab').
card_original_text('giant crab'/'TMP', '{U}: Until end of turn, Giant Crab cannot be the target of spells or abilities.').
card_first_print('giant crab', 'TMP').
card_image_name('giant crab'/'TMP', 'giant crab').
card_uid('giant crab'/'TMP', 'TMP:Giant Crab:giant crab').
card_rarity('giant crab'/'TMP', 'Common').
card_artist('giant crab'/'TMP', 'Tom Kyffin').
card_flavor_text('giant crab'/'TMP', 'During the giant crabs\' mating season, Skyshroud nights are filled with the clatter of their skirmishes.').
card_multiverse_id('giant crab'/'TMP', '4703').

card_in_set('giant strength', 'TMP').
card_original_type('giant strength'/'TMP', 'Enchant Creature').
card_original_text('giant strength'/'TMP', 'Enchanted creature gets +2/+2.').
card_image_name('giant strength'/'TMP', 'giant strength').
card_uid('giant strength'/'TMP', 'TMP:Giant Strength:giant strength').
card_rarity('giant strength'/'TMP', 'Common').
card_artist('giant strength'/'TMP', 'Pete Venters').
card_flavor_text('giant strength'/'TMP', '\"Crovax seems filled only with shadows,\" thought Orim. \"Where does his strength come from?\"').
card_multiverse_id('giant strength'/'TMP', '4820').

card_in_set('goblin bombardment', 'TMP').
card_original_type('goblin bombardment'/'TMP', 'Enchantment').
card_original_text('goblin bombardment'/'TMP', 'Sacrifice a creature: Goblin Bombardment deals 1 damage to target creature or player.').
card_first_print('goblin bombardment', 'TMP').
card_image_name('goblin bombardment'/'TMP', 'goblin bombardment').
card_uid('goblin bombardment'/'TMP', 'TMP:Goblin Bombardment:goblin bombardment').
card_rarity('goblin bombardment'/'TMP', 'Uncommon').
card_artist('goblin bombardment'/'TMP', 'Brian Snõddy').
card_flavor_text('goblin bombardment'/'TMP', 'One mogg to aim the catapult, one mogg to steer the rock.').
card_multiverse_id('goblin bombardment'/'TMP', '4821').

card_in_set('gravedigger', 'TMP').
card_original_type('gravedigger'/'TMP', 'Summon — Zombie').
card_original_text('gravedigger'/'TMP', 'When Gravedigger comes into play, you may return target creature card from your graveyard to your hand.').
card_image_name('gravedigger'/'TMP', 'gravedigger').
card_uid('gravedigger'/'TMP', 'TMP:Gravedigger:gravedigger').
card_rarity('gravedigger'/'TMP', 'Common').
card_artist('gravedigger'/'TMP', 'Dermot Power').
card_flavor_text('gravedigger'/'TMP', 'A full coffin is like a full coffer—both are attractive to thieves.').
card_multiverse_id('gravedigger'/'TMP', '4665').

card_in_set('grindstone', 'TMP').
card_original_type('grindstone'/'TMP', 'Artifact').
card_original_text('grindstone'/'TMP', '{3}, {T}: Put the top two cards of target player\'s library into that player\'s graveyard. If both cards share at least one color, repeat this process.').
card_first_print('grindstone', 'TMP').
card_image_name('grindstone'/'TMP', 'grindstone').
card_uid('grindstone'/'TMP', 'TMP:Grindstone:grindstone').
card_rarity('grindstone'/'TMP', 'Rare').
card_artist('grindstone'/'TMP', 'Greg Simanson').
card_flavor_text('grindstone'/'TMP', '\"Memory is a burden that wears at the soul as weather wears at stone.\"\n—Crovax').
card_multiverse_id('grindstone'/'TMP', '4610').

card_in_set('hand to hand', 'TMP').
card_original_type('hand to hand'/'TMP', 'Enchantment').
card_original_text('hand to hand'/'TMP', 'Instants and abilities requiring an activation cost cannot be played during combat.').
card_first_print('hand to hand', 'TMP').
card_image_name('hand to hand'/'TMP', 'hand to hand').
card_uid('hand to hand'/'TMP', 'TMP:Hand to Hand:hand to hand').
card_rarity('hand to hand'/'TMP', 'Rare').
card_artist('hand to hand'/'TMP', 'Carl Frank').
card_flavor_text('hand to hand'/'TMP', 'The match between Tahngarth and Greven was close only in the mind of the minotaur.').
card_multiverse_id('hand to hand'/'TMP', '4822').

card_in_set('hanna\'s custody', 'TMP').
card_original_type('hanna\'s custody'/'TMP', 'Enchantment').
card_original_text('hanna\'s custody'/'TMP', 'Artifacts cannot be the target of spells or abilities.').
card_first_print('hanna\'s custody', 'TMP').
card_image_name('hanna\'s custody'/'TMP', 'hanna\'s custody').
card_uid('hanna\'s custody'/'TMP', 'TMP:Hanna\'s Custody:hanna\'s custody').
card_rarity('hanna\'s custody'/'TMP', 'Rare').
card_artist('hanna\'s custody'/'TMP', 'DiTerlizzi').
card_flavor_text('hanna\'s custody'/'TMP', '\"I protect the Legacy with my life if necessary, for its purpose is far more important than my own.\"\n—Hanna, Weatherlight navigator').
card_multiverse_id('hanna\'s custody'/'TMP', '4878').

card_in_set('harrow', 'TMP').
card_original_type('harrow'/'TMP', 'Instant').
card_original_text('harrow'/'TMP', 'Sacrifice a land: Search your library for up to two basic land cards and put them into play. Shuffle your library afterwards.').
card_first_print('harrow', 'TMP').
card_image_name('harrow'/'TMP', 'harrow').
card_uid('harrow'/'TMP', 'TMP:Harrow:harrow').
card_rarity('harrow'/'TMP', 'Uncommon').
card_artist('harrow'/'TMP', 'Eric David Anderson').
card_flavor_text('harrow'/'TMP', 'Changing Rath\'s landscape is like tearing the scab from a wound.').
card_multiverse_id('harrow'/'TMP', '4764').

card_in_set('havoc', 'TMP').
card_original_type('havoc'/'TMP', 'Enchantment').
card_original_text('havoc'/'TMP', 'Whenever target opponent successfully casts a white spell, he or she loses 2 life.').
card_first_print('havoc', 'TMP').
card_image_name('havoc'/'TMP', 'havoc').
card_uid('havoc'/'TMP', 'TMP:Havoc:havoc').
card_rarity('havoc'/'TMP', 'Uncommon').
card_artist('havoc'/'TMP', 'Donato Giancola').
card_flavor_text('havoc'/'TMP', 'As Orim put the lightning rod into place on the bow, she cast a spell of protection to shield herself from the heat. That\'s when the Furnace got personal.').
card_multiverse_id('havoc'/'TMP', '4823').

card_in_set('heart sliver', 'TMP').
card_original_type('heart sliver'/'TMP', 'Summon — Sliver').
card_original_text('heart sliver'/'TMP', 'All Slivers are unaffected by summoning sickness.').
card_first_print('heart sliver', 'TMP').
card_image_name('heart sliver'/'TMP', 'heart sliver').
card_uid('heart sliver'/'TMP', 'TMP:Heart Sliver:heart sliver').
card_rarity('heart sliver'/'TMP', 'Common').
card_artist('heart sliver'/'TMP', 'Ron Spencer').
card_flavor_text('heart sliver'/'TMP', 'Gerrard looked all around for the source of the mysterious pulse, and at that moment the slivers boiled forth from the crevasses.').
card_multiverse_id('heart sliver'/'TMP', '4824').

card_in_set('heartwood dryad', 'TMP').
card_original_type('heartwood dryad'/'TMP', 'Summon — Dryad').
card_original_text('heartwood dryad'/'TMP', 'Heartwood Dryad can block creatures with shadow.').
card_first_print('heartwood dryad', 'TMP').
card_image_name('heartwood dryad'/'TMP', 'heartwood dryad').
card_uid('heartwood dryad'/'TMP', 'TMP:Heartwood Dryad:heartwood dryad').
card_rarity('heartwood dryad'/'TMP', 'Common').
card_artist('heartwood dryad'/'TMP', 'Rebecca Guay').
card_flavor_text('heartwood dryad'/'TMP', 'To snare what lies between worlds takes the patient reach of trees.').
card_multiverse_id('heartwood dryad'/'TMP', '4765').

card_in_set('heartwood giant', 'TMP').
card_original_type('heartwood giant'/'TMP', 'Summon — Giant').
card_original_text('heartwood giant'/'TMP', '{T}, Sacrifice a forest: Heartwood Giant deals 2 damage to target player.').
card_first_print('heartwood giant', 'TMP').
card_image_name('heartwood giant'/'TMP', 'heartwood giant').
card_uid('heartwood giant'/'TMP', 'TMP:Heartwood Giant:heartwood giant').
card_rarity('heartwood giant'/'TMP', 'Rare').
card_artist('heartwood giant'/'TMP', 'Randy Elliott').
card_flavor_text('heartwood giant'/'TMP', 'Wind in the trees is soothing, but the same can\'t be said for trees on the wind.').
card_multiverse_id('heartwood giant'/'TMP', '4766').

card_in_set('heartwood treefolk', 'TMP').
card_original_type('heartwood treefolk'/'TMP', 'Summon — Treefolk').
card_original_text('heartwood treefolk'/'TMP', 'Forestwalk (If defending player controls any forests, this creature is unblockable.)').
card_first_print('heartwood treefolk', 'TMP').
card_image_name('heartwood treefolk'/'TMP', 'heartwood treefolk').
card_uid('heartwood treefolk'/'TMP', 'TMP:Heartwood Treefolk:heartwood treefolk').
card_rarity('heartwood treefolk'/'TMP', 'Uncommon').
card_artist('heartwood treefolk'/'TMP', 'Daren Bader').
card_flavor_text('heartwood treefolk'/'TMP', '\"In all my years in Llanowar I never understood where trees fit in. They are revered by elves and watered on by dogs.\"\n—Mirri of the Weatherlight').
card_multiverse_id('heartwood treefolk'/'TMP', '4767').

card_in_set('helm of possession', 'TMP').
card_original_type('helm of possession'/'TMP', 'Artifact').
card_original_text('helm of possession'/'TMP', 'You may choose not to untap Helm of Possession during your untap phase.\n{2}, {T}, Sacrifice a creature: Gain control of target creature as long as you control Helm of Possession and Helm of Possession remains tapped.').
card_first_print('helm of possession', 'TMP').
card_image_name('helm of possession'/'TMP', 'helm of possession').
card_uid('helm of possession'/'TMP', 'TMP:Helm of Possession:helm of possession').
card_rarity('helm of possession'/'TMP', 'Rare').
card_artist('helm of possession'/'TMP', 'Janet Aulisio').
card_multiverse_id('helm of possession'/'TMP', '4611').

card_in_set('hero\'s resolve', 'TMP').
card_original_type('hero\'s resolve'/'TMP', 'Enchant Creature').
card_original_text('hero\'s resolve'/'TMP', 'Enchanted creature gets +1/+5.').
card_first_print('hero\'s resolve', 'TMP').
card_image_name('hero\'s resolve'/'TMP', 'hero\'s resolve').
card_uid('hero\'s resolve'/'TMP', 'TMP:Hero\'s Resolve:hero\'s resolve').
card_rarity('hero\'s resolve'/'TMP', 'Common').
card_artist('hero\'s resolve'/'TMP', 'Pete Venters').
card_flavor_text('hero\'s resolve'/'TMP', '\"Destiny, chance, fate, fortune—they\'re all just ways of claiming your successes without claiming your failures.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('hero\'s resolve'/'TMP', '4879').

card_in_set('horned sliver', 'TMP').
card_original_type('horned sliver'/'TMP', 'Summon — Sliver').
card_original_text('horned sliver'/'TMP', 'All Slivers gain trample.').
card_first_print('horned sliver', 'TMP').
card_image_name('horned sliver'/'TMP', 'horned sliver').
card_uid('horned sliver'/'TMP', 'TMP:Horned Sliver:horned sliver').
card_rarity('horned sliver'/'TMP', 'Uncommon').
card_artist('horned sliver'/'TMP', 'L. A. Williams').
card_flavor_text('horned sliver'/'TMP', 'A bristling wave of slivers broke against the Weatherlight\'s timbers.').
card_multiverse_id('horned sliver'/'TMP', '4768').

card_in_set('horned turtle', 'TMP').
card_original_type('horned turtle'/'TMP', 'Summon — Turtle').
card_original_text('horned turtle'/'TMP', '').
card_image_name('horned turtle'/'TMP', 'horned turtle').
card_uid('horned turtle'/'TMP', 'TMP:Horned Turtle:horned turtle').
card_rarity('horned turtle'/'TMP', 'Common').
card_artist('horned turtle'/'TMP', 'DiTerlizzi').
card_flavor_text('horned turtle'/'TMP', '\"So like men are they: a hardened shell with fragile flesh beneath.\"\n—Selenia, dark angel').
card_multiverse_id('horned turtle'/'TMP', '4704').

card_in_set('humility', 'TMP').
card_original_type('humility'/'TMP', 'Enchantment').
card_original_text('humility'/'TMP', 'Each creature loses all abilities and is a 1/1 creature.').
card_first_print('humility', 'TMP').
card_image_name('humility'/'TMP', 'humility').
card_uid('humility'/'TMP', 'TMP:Humility:humility').
card_rarity('humility'/'TMP', 'Rare').
card_artist('humility'/'TMP', 'Phil Foglio').
card_flavor_text('humility'/'TMP', '\"One cannot cleanse the wounds of failure.\"\n—Karn, silver golem').
card_multiverse_id('humility'/'TMP', '4881').

card_in_set('imps\' taunt', 'TMP').
card_original_type('imps\' taunt'/'TMP', 'Instant').
card_original_text('imps\' taunt'/'TMP', 'Buyback {3} (You may pay an additional {3} when you play this spell. If you do, put it into your hand instead of your graveyard as part of the spell\'s effect.)\nTarget creature attacks this turn if able.').
card_first_print('imps\' taunt', 'TMP').
card_image_name('imps\' taunt'/'TMP', 'imps\' taunt').
card_uid('imps\' taunt'/'TMP', 'TMP:Imps\' Taunt:imps\' taunt').
card_rarity('imps\' taunt'/'TMP', 'Uncommon').
card_artist('imps\' taunt'/'TMP', 'Colin MacNeil').
card_multiverse_id('imps\' taunt'/'TMP', '4666').

card_in_set('insight', 'TMP').
card_original_type('insight'/'TMP', 'Enchantment').
card_original_text('insight'/'TMP', 'Whenever target opponent successfully casts a green spell, draw a card.').
card_first_print('insight', 'TMP').
card_image_name('insight'/'TMP', 'insight').
card_uid('insight'/'TMP', 'TMP:Insight:insight').
card_rarity('insight'/'TMP', 'Uncommon').
card_artist('insight'/'TMP', 'Ron Chironna').
card_flavor_text('insight'/'TMP', '\"We can fight with you or against you. After my day, I\'d prefer the former.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('insight'/'TMP', '4705').

card_in_set('interdict', 'TMP').
card_original_type('interdict'/'TMP', 'Interrupt').
card_original_text('interdict'/'TMP', 'Counter target artifact, creature, enchantment, or land ability requiring an activation cost. Abilities of that permanent cannot be played again this turn. \nDraw a card.').
card_first_print('interdict', 'TMP').
card_image_name('interdict'/'TMP', 'interdict').
card_uid('interdict'/'TMP', 'TMP:Interdict:interdict').
card_rarity('interdict'/'TMP', 'Uncommon').
card_artist('interdict'/'TMP', 'Jeff Laubenstein').
card_multiverse_id('interdict'/'TMP', '4706').

card_in_set('intuition', 'TMP').
card_original_type('intuition'/'TMP', 'Instant').
card_original_text('intuition'/'TMP', 'Search your library for any three cards and reveal them to target opponent. He or she chooses one. Put that card into your hand and the rest into your graveyard. Shuffle your library afterwards.').
card_first_print('intuition', 'TMP').
card_image_name('intuition'/'TMP', 'intuition').
card_uid('intuition'/'TMP', 'TMP:Intuition:intuition').
card_rarity('intuition'/'TMP', 'Rare').
card_artist('intuition'/'TMP', 'April Lee').
card_multiverse_id('intuition'/'TMP', '4707').

card_in_set('invulnerability', 'TMP').
card_original_type('invulnerability'/'TMP', 'Instant').
card_original_text('invulnerability'/'TMP', 'Buyback {3} (You may pay an additional {3} when you play this spell. If you do, put it into your hand instead of your graveyard as part of the spell\'s effect.)\nPrevent all damage to you from one source. (Treat further damage from that source normally.)').
card_first_print('invulnerability', 'TMP').
card_image_name('invulnerability'/'TMP', 'invulnerability').
card_uid('invulnerability'/'TMP', 'TMP:Invulnerability:invulnerability').
card_rarity('invulnerability'/'TMP', 'Uncommon').
card_artist('invulnerability'/'TMP', 'Brian Snõddy').
card_multiverse_id('invulnerability'/'TMP', '4882').

card_in_set('island', 'TMP').
card_original_type('island'/'TMP', 'Land').
card_original_text('island'/'TMP', '{T}: Add {U} to your mana pool.').
card_image_name('island'/'TMP', 'island1').
card_uid('island'/'TMP', 'TMP:Island:island1').
card_rarity('island'/'TMP', 'Basic Land').
card_artist('island'/'TMP', 'Randy Gallegos').
card_multiverse_id('island'/'TMP', '4949').

card_in_set('island', 'TMP').
card_original_type('island'/'TMP', 'Land').
card_original_text('island'/'TMP', '{T}: Add {U} to your mana pool.').
card_image_name('island'/'TMP', 'island2').
card_uid('island'/'TMP', 'TMP:Island:island2').
card_rarity('island'/'TMP', 'Basic Land').
card_artist('island'/'TMP', 'Randy Gallegos').
card_multiverse_id('island'/'TMP', '4951').

card_in_set('island', 'TMP').
card_original_type('island'/'TMP', 'Land').
card_original_text('island'/'TMP', '{T}: Add {U} to your mana pool.').
card_image_name('island'/'TMP', 'island3').
card_uid('island'/'TMP', 'TMP:Island:island3').
card_rarity('island'/'TMP', 'Basic Land').
card_artist('island'/'TMP', 'Randy Gallegos').
card_multiverse_id('island'/'TMP', '4950').

card_in_set('island', 'TMP').
card_original_type('island'/'TMP', 'Land').
card_original_text('island'/'TMP', '{T}: Add {U} to your mana pool.').
card_image_name('island'/'TMP', 'island4').
card_uid('island'/'TMP', 'TMP:Island:island4').
card_rarity('island'/'TMP', 'Basic Land').
card_artist('island'/'TMP', 'Randy Gallegos').
card_multiverse_id('island'/'TMP', '4952').

card_in_set('jackal pup', 'TMP').
card_original_type('jackal pup'/'TMP', 'Summon — Hound').
card_original_text('jackal pup'/'TMP', 'For each 1 damage dealt to Jackal Pup, it deals 1 damage to you.').
card_first_print('jackal pup', 'TMP').
card_image_name('jackal pup'/'TMP', 'jackal pup').
card_uid('jackal pup'/'TMP', 'TMP:Jackal Pup:jackal pup').
card_rarity('jackal pup'/'TMP', 'Uncommon').
card_artist('jackal pup'/'TMP', 'Susan Van Camp').
card_flavor_text('jackal pup'/'TMP', 'The first morning after acquiring her familiar, the wizard awoke with fleabites and mange.').
card_multiverse_id('jackal pup'/'TMP', '4825').

card_in_set('jet medallion', 'TMP').
card_original_type('jet medallion'/'TMP', 'Artifact').
card_original_text('jet medallion'/'TMP', 'Your black spells cost {1} less to play.').
card_first_print('jet medallion', 'TMP').
card_image_name('jet medallion'/'TMP', 'jet medallion').
card_uid('jet medallion'/'TMP', 'TMP:Jet Medallion:jet medallion').
card_rarity('jet medallion'/'TMP', 'Rare').
card_artist('jet medallion'/'TMP', 'Sue Ellen Brown').
card_multiverse_id('jet medallion'/'TMP', '4612').

card_in_set('jinxed idol', 'TMP').
card_original_type('jinxed idol'/'TMP', 'Artifact').
card_original_text('jinxed idol'/'TMP', 'During your upkeep, Jinxed Idol deals 2 damage to you.\nSacrifice a creature: Target opponent gains control of Jinxed Idol permanently.').
card_first_print('jinxed idol', 'TMP').
card_image_name('jinxed idol'/'TMP', 'jinxed idol').
card_uid('jinxed idol'/'TMP', 'TMP:Jinxed Idol:jinxed idol').
card_rarity('jinxed idol'/'TMP', 'Rare').
card_artist('jinxed idol'/'TMP', 'John Matson').
card_flavor_text('jinxed idol'/'TMP', '\"Here.\"').
card_multiverse_id('jinxed idol'/'TMP', '4613').

card_in_set('kezzerdrix', 'TMP').
card_original_type('kezzerdrix'/'TMP', 'Summon — Beast').
card_original_text('kezzerdrix'/'TMP', 'First strike\nDuring your upkeep, if your opponents control no creatures, Kezzerdrix deals 4 damage to you.').
card_first_print('kezzerdrix', 'TMP').
card_image_name('kezzerdrix'/'TMP', 'kezzerdrix').
card_uid('kezzerdrix'/'TMP', 'TMP:Kezzerdrix:kezzerdrix').
card_rarity('kezzerdrix'/'TMP', 'Rare').
card_artist('kezzerdrix'/'TMP', 'Matthew D. Wilson').
card_multiverse_id('kezzerdrix'/'TMP', '4667').

card_in_set('kindle', 'TMP').
card_original_type('kindle'/'TMP', 'Instant').
card_original_text('kindle'/'TMP', 'Kindle deals to target creature or player an amount of damage equal to 2 plus the number of Kindle cards in all graveyards.').
card_first_print('kindle', 'TMP').
card_image_name('kindle'/'TMP', 'kindle').
card_uid('kindle'/'TMP', 'TMP:Kindle:kindle').
card_rarity('kindle'/'TMP', 'Common').
card_artist('kindle'/'TMP', 'Donato Giancola').
card_flavor_text('kindle'/'TMP', 'Hope of deliverance is scorched by the fire of futility.').
card_multiverse_id('kindle'/'TMP', '4826').

card_in_set('knight of dawn', 'TMP').
card_original_type('knight of dawn'/'TMP', 'Summon — Knight').
card_original_text('knight of dawn'/'TMP', 'First strike\n{W}{W}: Knight of Dawn gains protection from the color of your choice until end of turn.').
card_first_print('knight of dawn', 'TMP').
card_image_name('knight of dawn'/'TMP', 'knight of dawn').
card_uid('knight of dawn'/'TMP', 'TMP:Knight of Dawn:knight of dawn').
card_rarity('knight of dawn'/'TMP', 'Uncommon').
card_artist('knight of dawn'/'TMP', 'Ron Spencer').
card_flavor_text('knight of dawn'/'TMP', 'Flash like daybreak to the fray.\n—Motto of the Knights of Dawn').
card_multiverse_id('knight of dawn'/'TMP', '4883').

card_in_set('knight of dusk', 'TMP').
card_original_type('knight of dusk'/'TMP', 'Summon — Knight').
card_original_text('knight of dusk'/'TMP', '{B}{B}: Destroy target creature blocking Knight of Dusk.').
card_first_print('knight of dusk', 'TMP').
card_image_name('knight of dusk'/'TMP', 'knight of dusk').
card_uid('knight of dusk'/'TMP', 'TMP:Knight of Dusk:knight of dusk').
card_rarity('knight of dusk'/'TMP', 'Uncommon').
card_artist('knight of dusk'/'TMP', 'Ron Spencer').
card_flavor_text('knight of dusk'/'TMP', 'Fall like night upon the foe.\n—Motto of the Knights of Dusk').
card_multiverse_id('knight of dusk'/'TMP', '4668').

card_in_set('krakilin', 'TMP').
card_original_type('krakilin'/'TMP', 'Summon — Beast').
card_original_text('krakilin'/'TMP', 'Krakilin comes into play with X +1/+1 counters on it.\n{1}{G}: Regenerate Krakilin.').
card_first_print('krakilin', 'TMP').
card_image_name('krakilin'/'TMP', 'krakilin').
card_uid('krakilin'/'TMP', 'TMP:Krakilin:krakilin').
card_rarity('krakilin'/'TMP', 'Uncommon').
card_artist('krakilin'/'TMP', 'Richard Kane Ferguson').
card_flavor_text('krakilin'/'TMP', '\"A perfect reflection of its world: brutal, fetal, and lacking truth.\"\n—Oracle en-Vec').
card_multiverse_id('krakilin'/'TMP', '4769').

card_in_set('leeching licid', 'TMP').
card_original_type('leeching licid'/'TMP', 'Summon — Licid').
card_original_text('leeching licid'/'TMP', '{B}, {T}: Leeching Licid loses this ability and becomes a creature enchantment that reads \"During the upkeep of enchanted creature\'s controller, Leeching Licid deals 1 damage to that player\" instead of a creature. Move Leeching Licid onto target creature. You may pay {B} to end this effect.').
card_first_print('leeching licid', 'TMP').
card_image_name('leeching licid'/'TMP', 'leeching licid').
card_uid('leeching licid'/'TMP', 'TMP:Leeching Licid:leeching licid').
card_rarity('leeching licid'/'TMP', 'Uncommon').
card_artist('leeching licid'/'TMP', 'Joel Biske').
card_multiverse_id('leeching licid'/'TMP', '4670').

card_in_set('legacy\'s allure', 'TMP').
card_original_type('legacy\'s allure'/'TMP', 'Enchantment').
card_original_text('legacy\'s allure'/'TMP', 'During your upkeep, you may put a treasure counter on Legacy\'s Allure.\nSacrifice Legacy\'s Allure: Permanently gain control of target creature with power no greater than the number of treasure counters on Legacy\'s Allure.').
card_first_print('legacy\'s allure', 'TMP').
card_image_name('legacy\'s allure'/'TMP', 'legacy\'s allure').
card_uid('legacy\'s allure'/'TMP', 'TMP:Legacy\'s Allure:legacy\'s allure').
card_rarity('legacy\'s allure'/'TMP', 'Uncommon').
card_artist('legacy\'s allure'/'TMP', 'Daren Bader').
card_multiverse_id('legacy\'s allure'/'TMP', '4708').

card_in_set('legerdemain', 'TMP').
card_original_type('legerdemain'/'TMP', 'Sorcery').
card_original_text('legerdemain'/'TMP', 'Permanently exchange control of target artifact or creature for control of target permanent of the same type.').
card_first_print('legerdemain', 'TMP').
card_image_name('legerdemain'/'TMP', 'legerdemain').
card_uid('legerdemain'/'TMP', 'TMP:Legerdemain:legerdemain').
card_rarity('legerdemain'/'TMP', 'Uncommon').
card_artist('legerdemain'/'TMP', 'Daren Bader').
card_flavor_text('legerdemain'/'TMP', 'Squee tucked the warm ball in his pocket and slipped a pebble in its place. ‘Glok,\' he mumbled, and hid.').
card_multiverse_id('legerdemain'/'TMP', '4709').

card_in_set('light of day', 'TMP').
card_original_type('light of day'/'TMP', 'Enchantment').
card_original_text('light of day'/'TMP', 'Black creatures cannot attack or block').
card_first_print('light of day', 'TMP').
card_image_name('light of day'/'TMP', 'light of day').
card_uid('light of day'/'TMP', 'TMP:Light of Day:light of day').
card_rarity('light of day'/'TMP', 'Uncommon').
card_artist('light of day'/'TMP', 'Drew Tucker').
card_flavor_text('light of day'/'TMP', '\"I do not miss sunlight. The very memory of it burns my eyes.\"\n—Volrath').
card_multiverse_id('light of day'/'TMP', '4884').

card_in_set('lightning blast', 'TMP').
card_original_type('lightning blast'/'TMP', 'Instant').
card_original_text('lightning blast'/'TMP', 'Lightning Blast deals 4 damage to target creature or player.').
card_first_print('lightning blast', 'TMP').
card_image_name('lightning blast'/'TMP', 'lightning blast').
card_uid('lightning blast'/'TMP', 'TMP:Lightning Blast:lightning blast').
card_rarity('lightning blast'/'TMP', 'Common').
card_artist('lightning blast'/'TMP', 'Richard Thomas').
card_flavor_text('lightning blast'/'TMP', '\"Those who fear the darkness have never seen what the light can do.\"\n—Selenia, dark angel').
card_multiverse_id('lightning blast'/'TMP', '4827').

card_in_set('lightning elemental', 'TMP').
card_original_type('lightning elemental'/'TMP', 'Summon — Elemental').
card_original_text('lightning elemental'/'TMP', 'Lightning Elemental is unaffected by summoning sickness.').
card_first_print('lightning elemental', 'TMP').
card_image_name('lightning elemental'/'TMP', 'lightning elemental').
card_uid('lightning elemental'/'TMP', 'TMP:Lightning Elemental:lightning elemental').
card_rarity('lightning elemental'/'TMP', 'Common').
card_artist('lightning elemental'/'TMP', 'D. Alexander Gregory').
card_flavor_text('lightning elemental'/'TMP', 'A lightning elemental once killed an entire tribe of merfolk—simply by going for a swim.').
card_multiverse_id('lightning elemental'/'TMP', '4828').

card_in_set('living death', 'TMP').
card_original_type('living death'/'TMP', 'Sorcery').
card_original_text('living death'/'TMP', 'Set aside all creature cards in all graveyards. Then, put each creature that is in play into its owner\'s graveyard. Then, put each creature card set aside in this way into play under its owner\'s control.').
card_first_print('living death', 'TMP').
card_image_name('living death'/'TMP', 'living death').
card_uid('living death'/'TMP', 'TMP:Living Death:living death').
card_rarity('living death'/'TMP', 'Rare').
card_artist('living death'/'TMP', 'Charles Gillespie').
card_multiverse_id('living death'/'TMP', '4671').

card_in_set('lobotomy', 'TMP').
card_original_type('lobotomy'/'TMP', 'Sorcery').
card_original_text('lobotomy'/'TMP', 'Look at target player\'s hand and choose any of those cards other than a basic land. Search that player\'s graveyard, hand, and library for all copies of the chosen card and remove them from the game. That player shuffles his or her library afterwards.').
card_first_print('lobotomy', 'TMP').
card_image_name('lobotomy'/'TMP', 'lobotomy').
card_uid('lobotomy'/'TMP', 'TMP:Lobotomy:lobotomy').
card_rarity('lobotomy'/'TMP', 'Uncommon').
card_artist('lobotomy'/'TMP', 'Thomas M. Baxa').
card_multiverse_id('lobotomy'/'TMP', '4913').

card_in_set('lotus petal', 'TMP').
card_original_type('lotus petal'/'TMP', 'Artifact').
card_original_text('lotus petal'/'TMP', '{T}, Sacrifice Lotus Petal: Add one mana of any color to your mana pool. Play this ability as a mana source.').
card_first_print('lotus petal', 'TMP').
card_image_name('lotus petal'/'TMP', 'lotus petal').
card_uid('lotus petal'/'TMP', 'TMP:Lotus Petal:lotus petal').
card_rarity('lotus petal'/'TMP', 'Common').
card_artist('lotus petal'/'TMP', 'April Lee').
card_flavor_text('lotus petal'/'TMP', '\"Hard to imagine,\" mused Hanna, stroking the petal, \"such a lovely flower inspiring such greed.\"').
card_multiverse_id('lotus petal'/'TMP', '4614').

card_in_set('lowland giant', 'TMP').
card_original_type('lowland giant'/'TMP', 'Summon — Giant').
card_original_text('lowland giant'/'TMP', '').
card_first_print('lowland giant', 'TMP').
card_image_name('lowland giant'/'TMP', 'lowland giant').
card_uid('lowland giant'/'TMP', 'TMP:Lowland Giant:lowland giant').
card_rarity('lowland giant'/'TMP', 'Common').
card_artist('lowland giant'/'TMP', 'Paolo Parente').
card_flavor_text('lowland giant'/'TMP', '\"Faugh!\" snorted Tahngarth. \"Why would it make a meal of something like you?\" Squee looked relieved. \"No,\" he continued, \"you\'d make a much better toothpick.\"').
card_multiverse_id('lowland giant'/'TMP', '4829').

card_in_set('maddening imp', 'TMP').
card_original_type('maddening imp'/'TMP', 'Summon — Imp').
card_original_text('maddening imp'/'TMP', 'Flying\n{T}: All non-Wall creatures target opponent controls attack this turn if able. At end of turn, destroy each of those creatures that did not attack. Use this ability only during target opponent\'s turn and only before combat.').
card_first_print('maddening imp', 'TMP').
card_image_name('maddening imp'/'TMP', 'maddening imp').
card_uid('maddening imp'/'TMP', 'TMP:Maddening Imp:maddening imp').
card_rarity('maddening imp'/'TMP', 'Rare').
card_artist('maddening imp'/'TMP', 'Zina Saunders').
card_multiverse_id('maddening imp'/'TMP', '4672').

card_in_set('magmasaur', 'TMP').
card_original_type('magmasaur'/'TMP', 'Summon — Elemental').
card_original_text('magmasaur'/'TMP', 'Magmasaur comes into play with five +1/+1 counters on it.\nDuring your upkeep, remove a +1/+1 counter from Magmasaur, or sacrifice Magmasaur and it deals 1 damage for each +1/+1 counter on it to each creature without flying and each player.').
card_first_print('magmasaur', 'TMP').
card_image_name('magmasaur'/'TMP', 'magmasaur').
card_uid('magmasaur'/'TMP', 'TMP:Magmasaur:magmasaur').
card_rarity('magmasaur'/'TMP', 'Rare').
card_artist('magmasaur'/'TMP', 'Daniel Gelon').
card_multiverse_id('magmasaur'/'TMP', '4830').

card_in_set('magnetic web', 'TMP').
card_original_type('magnetic web'/'TMP', 'Artifact').
card_original_text('magnetic web'/'TMP', 'If any creature with any magnet counters on it attacks, all creatures with magnet counters on them that the attacking player controls attack if able.\nIf any creature with any magnet counters on it attacks, all creatures with magnet counters on them that the defending player controls block that creature if able.\n{1}, {T}: Put a magnet counter on target creature.').
card_first_print('magnetic web', 'TMP').
card_image_name('magnetic web'/'TMP', 'magnetic web').
card_uid('magnetic web'/'TMP', 'TMP:Magnetic Web:magnetic web').
card_rarity('magnetic web'/'TMP', 'Rare').
card_artist('magnetic web'/'TMP', 'Adam Rex').
card_multiverse_id('magnetic web'/'TMP', '4615').

card_in_set('mana severance', 'TMP').
card_original_type('mana severance'/'TMP', 'Sorcery').
card_original_text('mana severance'/'TMP', 'Search your library for any number of land cards and remove them from the game. Shuffle your library afterwards.').
card_first_print('mana severance', 'TMP').
card_image_name('mana severance'/'TMP', 'mana severance').
card_uid('mana severance'/'TMP', 'TMP:Mana Severance:mana severance').
card_rarity('mana severance'/'TMP', 'Rare').
card_artist('mana severance'/'TMP', 'Terese Nielsen').
card_flavor_text('mana severance'/'TMP', '\"And when this land is wasted, where will we go?\"\n—Thalakos lament').
card_multiverse_id('mana severance'/'TMP', '4710').

card_in_set('manakin', 'TMP').
card_original_type('manakin'/'TMP', 'Artifact Creature').
card_original_text('manakin'/'TMP', '{T}: Add one colorless mana to your mana pool. Play this ability as a mana source.').
card_first_print('manakin', 'TMP').
card_image_name('manakin'/'TMP', 'manakin').
card_uid('manakin'/'TMP', 'TMP:Manakin:manakin').
card_rarity('manakin'/'TMP', 'Common').
card_artist('manakin'/'TMP', 'Scott Kirschner').
card_flavor_text('manakin'/'TMP', 'Hanna regarded Squee sternly. \"Because it\'s not a toy, no matter how much it may look like one,\" she said, taking the manakin from him.').
card_multiverse_id('manakin'/'TMP', '4616').

card_in_set('manta riders', 'TMP').
card_original_type('manta riders'/'TMP', 'Summon — Merfolk').
card_original_text('manta riders'/'TMP', '{U}: Manta Riders gains flying until end of turn.').
card_first_print('manta riders', 'TMP').
card_image_name('manta riders'/'TMP', 'manta riders').
card_uid('manta riders'/'TMP', 'TMP:Manta Riders:manta riders').
card_rarity('manta riders'/'TMP', 'Common').
card_artist('manta riders'/'TMP', 'Kaja Foglio').
card_flavor_text('manta riders'/'TMP', '\"Water is firmament to the finned.\"\n—Oracle en-Vec').
card_multiverse_id('manta riders'/'TMP', '4711').

card_in_set('marble titan', 'TMP').
card_original_type('marble titan'/'TMP', 'Summon — Giant').
card_original_text('marble titan'/'TMP', 'Creatures with power 3 or greater do not untap during their controllers\' untap phases.').
card_first_print('marble titan', 'TMP').
card_image_name('marble titan'/'TMP', 'marble titan').
card_uid('marble titan'/'TMP', 'TMP:Marble Titan:marble titan').
card_rarity('marble titan'/'TMP', 'Rare').
card_artist('marble titan'/'TMP', 'Brom').
card_flavor_text('marble titan'/'TMP', '\"Strapping on nine hundred pounds of armor every morning would make any warrior cross.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('marble titan'/'TMP', '4886').

card_in_set('marsh lurker', 'TMP').
card_original_type('marsh lurker'/'TMP', 'Summon — Beast').
card_original_text('marsh lurker'/'TMP', 'Sacrifice a swamp: Marsh Lurker cannot be blocked this turn except by artifact creatures and black creatures.').
card_first_print('marsh lurker', 'TMP').
card_image_name('marsh lurker'/'TMP', 'marsh lurker').
card_uid('marsh lurker'/'TMP', 'TMP:Marsh Lurker:marsh lurker').
card_rarity('marsh lurker'/'TMP', 'Common').
card_artist('marsh lurker'/'TMP', 'Tom Kyffin').
card_flavor_text('marsh lurker'/'TMP', 'From the mists it rises, into the mists it retreats; through the mists it walks, fearless and unseen.').
card_multiverse_id('marsh lurker'/'TMP', '4673').

card_in_set('master decoy', 'TMP').
card_original_type('master decoy'/'TMP', 'Summon — Soldier').
card_original_text('master decoy'/'TMP', '{W}, {T}: Tap target creature.').
card_first_print('master decoy', 'TMP').
card_image_name('master decoy'/'TMP', 'master decoy').
card_uid('master decoy'/'TMP', 'TMP:Master Decoy:master decoy').
card_rarity('master decoy'/'TMP', 'Common').
card_artist('master decoy'/'TMP', 'Phil Foglio').
card_flavor_text('master decoy'/'TMP', '\"A skilled decoy can throw your enemies off your trail. A master decoy can survive to do it again.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('master decoy'/'TMP', '4887').

card_in_set('mawcor', 'TMP').
card_original_type('mawcor'/'TMP', 'Summon — Beast').
card_original_text('mawcor'/'TMP', 'Flying\n{T}: Mawcor deals 1 damage to target creature or player.').
card_first_print('mawcor', 'TMP').
card_image_name('mawcor'/'TMP', 'mawcor').
card_uid('mawcor'/'TMP', 'TMP:Mawcor:mawcor').
card_rarity('mawcor'/'TMP', 'Rare').
card_artist('mawcor'/'TMP', 'John Matson').
card_flavor_text('mawcor'/'TMP', 'From its maw comes neither word nor whisper—only wind.').
card_multiverse_id('mawcor'/'TMP', '4712').

card_in_set('maze of shadows', 'TMP').
card_original_type('maze of shadows'/'TMP', 'Land').
card_original_text('maze of shadows'/'TMP', '{T}: Add one colorless mana to your mana pool.\n{T}: Untap target attacking creature with shadow. That creature neither deals nor receives combat damage this turn.').
card_first_print('maze of shadows', 'TMP').
card_image_name('maze of shadows'/'TMP', 'maze of shadows').
card_uid('maze of shadows'/'TMP', 'TMP:Maze of Shadows:maze of shadows').
card_rarity('maze of shadows'/'TMP', 'Uncommon').
card_artist('maze of shadows'/'TMP', 'D. Alexander Gregory').
card_multiverse_id('maze of shadows'/'TMP', '4933').

card_in_set('meditate', 'TMP').
card_original_type('meditate'/'TMP', 'Instant').
card_original_text('meditate'/'TMP', 'Skip your next turn: Draw four cards.').
card_first_print('meditate', 'TMP').
card_image_name('meditate'/'TMP', 'meditate').
card_uid('meditate'/'TMP', 'TMP:Meditate:meditate').
card_rarity('meditate'/'TMP', 'Rare').
card_artist('meditate'/'TMP', 'Susan Van Camp').
card_flavor_text('meditate'/'TMP', '\"Part of me believes that Barrin taught me meditation simply to shut me up.\"\n—Ertai, wizard adept').
card_multiverse_id('meditate'/'TMP', '4713').

card_in_set('metallic sliver', 'TMP').
card_original_type('metallic sliver'/'TMP', 'Artifact Creature').
card_original_text('metallic sliver'/'TMP', 'Metallic Sliver counts as a Sliver.').
card_first_print('metallic sliver', 'TMP').
card_image_name('metallic sliver'/'TMP', 'metallic sliver').
card_uid('metallic sliver'/'TMP', 'TMP:Metallic Sliver:metallic sliver').
card_rarity('metallic sliver'/'TMP', 'Common').
card_artist('metallic sliver'/'TMP', 'L. A. Williams').
card_flavor_text('metallic sliver'/'TMP', 'When the clever counterfeit was accepted by the hive, Volrath\'s influence upon the slivers grew even stronger.').
card_multiverse_id('metallic sliver'/'TMP', '4617').

card_in_set('mindwhip sliver', 'TMP').
card_original_type('mindwhip sliver'/'TMP', 'Summon — Sliver').
card_original_text('mindwhip sliver'/'TMP', 'Each Sliver gains \"{2}, Sacrifice this creature: Target player discards a card at random. Play this ability as a sorcery.\"').
card_first_print('mindwhip sliver', 'TMP').
card_image_name('mindwhip sliver'/'TMP', 'mindwhip sliver').
card_uid('mindwhip sliver'/'TMP', 'TMP:Mindwhip Sliver:mindwhip sliver').
card_rarity('mindwhip sliver'/'TMP', 'Uncommon').
card_artist('mindwhip sliver'/'TMP', 'Jeff Miracola').
card_flavor_text('mindwhip sliver'/'TMP', '\"They share more than their thoughts. We must shatter their link quickly!\"\n—Hanna, to Orim').
card_multiverse_id('mindwhip sliver'/'TMP', '4674').

card_in_set('minion of the wastes', 'TMP').
card_original_type('minion of the wastes'/'TMP', 'Summon — Minion').
card_original_text('minion of the wastes'/'TMP', 'Trample\nWhen you play Minion of the Wastes, pay any amount of life.\nMinion of the Wastes has power and toughness each equal to that amount.').
card_first_print('minion of the wastes', 'TMP').
card_image_name('minion of the wastes'/'TMP', 'minion of the wastes').
card_uid('minion of the wastes'/'TMP', 'TMP:Minion of the Wastes:minion of the wastes').
card_rarity('minion of the wastes'/'TMP', 'Rare').
card_artist('minion of the wastes'/'TMP', 'Scott Kirschner').
card_multiverse_id('minion of the wastes'/'TMP', '4675').

card_in_set('mirri\'s guile', 'TMP').
card_original_type('mirri\'s guile'/'TMP', 'Enchantment').
card_original_text('mirri\'s guile'/'TMP', 'During your upkeep, you may look at the top three cards of your library and put them back in any order.').
card_first_print('mirri\'s guile', 'TMP').
card_image_name('mirri\'s guile'/'TMP', 'mirri\'s guile').
card_uid('mirri\'s guile'/'TMP', 'TMP:Mirri\'s Guile:mirri\'s guile').
card_rarity('mirri\'s guile'/'TMP', 'Rare').
card_artist('mirri\'s guile'/'TMP', 'Brom').
card_flavor_text('mirri\'s guile'/'TMP', 'Hanna was astounded. Mirri read every leaf and wisp of breeze like a book of ancient lore.').
card_multiverse_id('mirri\'s guile'/'TMP', '4770').

card_in_set('mnemonic sliver', 'TMP').
card_original_type('mnemonic sliver'/'TMP', 'Summon — Sliver').
card_original_text('mnemonic sliver'/'TMP', 'Each Sliver gains \"{2}, Sacrifice this creature: Draw a card.\"').
card_first_print('mnemonic sliver', 'TMP').
card_image_name('mnemonic sliver'/'TMP', 'mnemonic sliver').
card_uid('mnemonic sliver'/'TMP', 'TMP:Mnemonic Sliver:mnemonic sliver').
card_rarity('mnemonic sliver'/'TMP', 'Uncommon').
card_artist('mnemonic sliver'/'TMP', 'Randy Gallegos').
card_flavor_text('mnemonic sliver'/'TMP', '\"A jigsaw puzzle that lives, breeds, and thinks.\"\n—Hanna, Weatherlight navigator').
card_multiverse_id('mnemonic sliver'/'TMP', '4714').

card_in_set('mogg cannon', 'TMP').
card_original_type('mogg cannon'/'TMP', 'Artifact').
card_original_text('mogg cannon'/'TMP', '{T}: Target creature you control gets +1/+0 and gains flying until end of turn. At end of turn, destroy that creature.').
card_first_print('mogg cannon', 'TMP').
card_image_name('mogg cannon'/'TMP', 'mogg cannon').
card_uid('mogg cannon'/'TMP', 'TMP:Mogg Cannon:mogg cannon').
card_rarity('mogg cannon'/'TMP', 'Uncommon').
card_artist('mogg cannon'/'TMP', 'Mike Raabe').
card_flavor_text('mogg cannon'/'TMP', 'Moggs never volunteer for anything twice.').
card_multiverse_id('mogg cannon'/'TMP', '4618').

card_in_set('mogg conscripts', 'TMP').
card_original_type('mogg conscripts'/'TMP', 'Summon — Goblins').
card_original_text('mogg conscripts'/'TMP', 'Mogg Conscripts cannot attack unless you have successfully cast a creature spell this turn.').
card_first_print('mogg conscripts', 'TMP').
card_image_name('mogg conscripts'/'TMP', 'mogg conscripts').
card_uid('mogg conscripts'/'TMP', 'TMP:Mogg Conscripts:mogg conscripts').
card_rarity('mogg conscripts'/'TMP', 'Common').
card_artist('mogg conscripts'/'TMP', 'Pete Venters').
card_flavor_text('mogg conscripts'/'TMP', '\"Torahn\'s horns! They\'ve seen us. Now, statue, you must fight to save yourself!\"\n—Tahngarth, aboard the Predator').
card_multiverse_id('mogg conscripts'/'TMP', '4831').

card_in_set('mogg fanatic', 'TMP').
card_original_type('mogg fanatic'/'TMP', 'Summon — Goblin').
card_original_text('mogg fanatic'/'TMP', 'Sacrifice Mogg Fanatic: Mogg Fanatic deals 1 damage to target creature or player.').
card_first_print('mogg fanatic', 'TMP').
card_image_name('mogg fanatic'/'TMP', 'mogg fanatic').
card_uid('mogg fanatic'/'TMP', 'TMP:Mogg Fanatic:mogg fanatic').
card_rarity('mogg fanatic'/'TMP', 'Common').
card_artist('mogg fanatic'/'TMP', 'Brom').
card_flavor_text('mogg fanatic'/'TMP', '\"I got it! I got it! I—\"').
card_multiverse_id('mogg fanatic'/'TMP', '4832').

card_in_set('mogg hollows', 'TMP').
card_original_type('mogg hollows'/'TMP', 'Land').
card_original_text('mogg hollows'/'TMP', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {R} or {G} to your mana pool. Mogg Hollows does not untap during your next untap phase.').
card_first_print('mogg hollows', 'TMP').
card_image_name('mogg hollows'/'TMP', 'mogg hollows').
card_uid('mogg hollows'/'TMP', 'TMP:Mogg Hollows:mogg hollows').
card_rarity('mogg hollows'/'TMP', 'Uncommon').
card_artist('mogg hollows'/'TMP', 'Jeff Laubenstein').
card_multiverse_id('mogg hollows'/'TMP', '4934').

card_in_set('mogg raider', 'TMP').
card_original_type('mogg raider'/'TMP', 'Summon — Goblin').
card_original_text('mogg raider'/'TMP', 'Sacrifice a Goblin: Target creature gets +1/+1 until end of turn.').
card_first_print('mogg raider', 'TMP').
card_image_name('mogg raider'/'TMP', 'mogg raider').
card_uid('mogg raider'/'TMP', 'TMP:Mogg Raider:mogg raider').
card_rarity('mogg raider'/'TMP', 'Common').
card_artist('mogg raider'/'TMP', 'Brian Snõddy').
card_flavor_text('mogg raider'/'TMP', 'The evisceration of one mogg always cheers up the rest.').
card_multiverse_id('mogg raider'/'TMP', '4833').

card_in_set('mogg squad', 'TMP').
card_original_type('mogg squad'/'TMP', 'Summon — Goblins').
card_original_text('mogg squad'/'TMP', 'Mogg Squad gets -1/-1 for each other creature in play.').
card_first_print('mogg squad', 'TMP').
card_image_name('mogg squad'/'TMP', 'mogg squad').
card_uid('mogg squad'/'TMP', 'TMP:Mogg Squad:mogg squad').
card_rarity('mogg squad'/'TMP', 'Uncommon').
card_artist('mogg squad'/'TMP', 'Joel Biske').
card_flavor_text('mogg squad'/'TMP', 'As they crept into the Weatherlight\'s hold, the squad leader held up his hand and whispered, \"Stop, goblins—what\'s that sound?\"').
card_multiverse_id('mogg squad'/'TMP', '4834').

card_in_set('mongrel pack', 'TMP').
card_original_type('mongrel pack'/'TMP', 'Summon — Hounds').
card_original_text('mongrel pack'/'TMP', 'If Mongrel Pack is put into any graveyard from play during combat, put four Hound tokens into play. Treat these tokens as 1/1 green creatures.').
card_first_print('mongrel pack', 'TMP').
card_image_name('mongrel pack'/'TMP', 'mongrel pack').
card_uid('mongrel pack'/'TMP', 'TMP:Mongrel Pack:mongrel pack').
card_rarity('mongrel pack'/'TMP', 'Rare').
card_artist('mongrel pack'/'TMP', 'Jeff Miracola').
card_multiverse_id('mongrel pack'/'TMP', '4771').

card_in_set('mountain', 'TMP').
card_original_type('mountain'/'TMP', 'Land').
card_original_text('mountain'/'TMP', '{T}: Add {R} to your mana pool.').
card_image_name('mountain'/'TMP', 'mountain1').
card_uid('mountain'/'TMP', 'TMP:Mountain:mountain1').
card_rarity('mountain'/'TMP', 'Basic Land').
card_artist('mountain'/'TMP', 'Mark Poole').
card_multiverse_id('mountain'/'TMP', '4947').

card_in_set('mountain', 'TMP').
card_original_type('mountain'/'TMP', 'Land').
card_original_text('mountain'/'TMP', '{T}: Add {R} to your mana pool.').
card_image_name('mountain'/'TMP', 'mountain2').
card_uid('mountain'/'TMP', 'TMP:Mountain:mountain2').
card_rarity('mountain'/'TMP', 'Basic Land').
card_artist('mountain'/'TMP', 'Mark Poole').
card_multiverse_id('mountain'/'TMP', '4945').

card_in_set('mountain', 'TMP').
card_original_type('mountain'/'TMP', 'Land').
card_original_text('mountain'/'TMP', '{T}: Add {R} to your mana pool.').
card_image_name('mountain'/'TMP', 'mountain3').
card_uid('mountain'/'TMP', 'TMP:Mountain:mountain3').
card_rarity('mountain'/'TMP', 'Basic Land').
card_artist('mountain'/'TMP', 'Mark Poole').
card_multiverse_id('mountain'/'TMP', '4946').

card_in_set('mountain', 'TMP').
card_original_type('mountain'/'TMP', 'Land').
card_original_text('mountain'/'TMP', '{T}: Add {R} to your mana pool.').
card_image_name('mountain'/'TMP', 'mountain4').
card_uid('mountain'/'TMP', 'TMP:Mountain:mountain4').
card_rarity('mountain'/'TMP', 'Basic Land').
card_artist('mountain'/'TMP', 'Mark Poole').
card_multiverse_id('mountain'/'TMP', '4948').

card_in_set('mounted archers', 'TMP').
card_original_type('mounted archers'/'TMP', 'Summon — Soldiers').
card_original_text('mounted archers'/'TMP', 'Mounted Archers can block creatures with flying.\n{W}: Mounted Archers can block an additional creature this turn. (All blocking assignments must still be legal.)').
card_first_print('mounted archers', 'TMP').
card_image_name('mounted archers'/'TMP', 'mounted archers').
card_uid('mounted archers'/'TMP', 'TMP:Mounted Archers:mounted archers').
card_rarity('mounted archers'/'TMP', 'Common').
card_artist('mounted archers'/'TMP', 'Kev Walker').
card_multiverse_id('mounted archers'/'TMP', '4888').

card_in_set('muscle sliver', 'TMP').
card_original_type('muscle sliver'/'TMP', 'Summon — Sliver').
card_original_text('muscle sliver'/'TMP', 'All Slivers get +1/+1.').
card_first_print('muscle sliver', 'TMP').
card_image_name('muscle sliver'/'TMP', 'muscle sliver').
card_uid('muscle sliver'/'TMP', 'TMP:Muscle Sliver:muscle sliver').
card_rarity('muscle sliver'/'TMP', 'Common').
card_artist('muscle sliver'/'TMP', 'Richard Kane Ferguson').
card_flavor_text('muscle sliver'/'TMP', 'The air was filled with the cracks and snaps of flesh hardening as the new sliver joined the battle.').
card_multiverse_id('muscle sliver'/'TMP', '4772').

card_in_set('natural spring', 'TMP').
card_original_type('natural spring'/'TMP', 'Sorcery').
card_original_text('natural spring'/'TMP', 'Target player gains 8 life.').
card_image_name('natural spring'/'TMP', 'natural spring').
card_uid('natural spring'/'TMP', 'TMP:Natural Spring:natural spring').
card_rarity('natural spring'/'TMP', 'Common').
card_artist('natural spring'/'TMP', 'Susan Van Camp').
card_flavor_text('natural spring'/'TMP', 'Mirri listened intently, even as Hanna held the healing water to her lips. Would the sound of the spring drown out the elves\' approach?').
card_multiverse_id('natural spring'/'TMP', '4773').

card_in_set('nature\'s revolt', 'TMP').
card_original_type('nature\'s revolt'/'TMP', 'Enchantment').
card_original_text('nature\'s revolt'/'TMP', 'All lands are 2/2 creatures. (These creatures still count as lands.)').
card_first_print('nature\'s revolt', 'TMP').
card_image_name('nature\'s revolt'/'TMP', 'nature\'s revolt').
card_uid('nature\'s revolt'/'TMP', 'TMP:Nature\'s Revolt:nature\'s revolt').
card_rarity('nature\'s revolt'/'TMP', 'Rare').
card_artist('nature\'s revolt'/'TMP', 'Donato Giancola').
card_flavor_text('nature\'s revolt'/'TMP', '\"One day the land will don the semblance of flesh and challenge even the plowshare\'s blade.\"\n—Oracle en-Vec').
card_multiverse_id('nature\'s revolt'/'TMP', '4774').

card_in_set('needle storm', 'TMP').
card_original_type('needle storm'/'TMP', 'Sorcery').
card_original_text('needle storm'/'TMP', 'Needle Storm deals 4 damage to each creature with flying.').
card_image_name('needle storm'/'TMP', 'needle storm').
card_uid('needle storm'/'TMP', 'TMP:Needle Storm:needle storm').
card_rarity('needle storm'/'TMP', 'Uncommon').
card_artist('needle storm'/'TMP', 'Val Mayerik').
card_flavor_text('needle storm'/'TMP', 'The rain is driven like nails.').
card_multiverse_id('needle storm'/'TMP', '4775').

card_in_set('no quarter', 'TMP').
card_original_type('no quarter'/'TMP', 'Enchantment').
card_original_text('no quarter'/'TMP', 'Whenever any creature blocks or is blocked by a creature with lesser power, destroy the creature with the lesser power.').
card_first_print('no quarter', 'TMP').
card_image_name('no quarter'/'TMP', 'no quarter').
card_uid('no quarter'/'TMP', 'TMP:No Quarter:no quarter').
card_rarity('no quarter'/'TMP', 'Rare').
card_artist('no quarter'/'TMP', 'Doug Chaffee').
card_flavor_text('no quarter'/'TMP', 'As Gerrard and Greven neared each other, the moggs caught between them realized they should flee.').
card_multiverse_id('no quarter'/'TMP', '4835').

card_in_set('nurturing licid', 'TMP').
card_original_type('nurturing licid'/'TMP', 'Summon — Licid').
card_original_text('nurturing licid'/'TMP', '{G}, {T}: Nurturing Licid loses this ability and becomes a creature enchantment that reads \"{G}: Regenerate enchanted creature\" instead of a creature. Move Nurturing Licid onto target creature. You may pay {G} to end this effect.').
card_first_print('nurturing licid', 'TMP').
card_image_name('nurturing licid'/'TMP', 'nurturing licid').
card_uid('nurturing licid'/'TMP', 'TMP:Nurturing Licid:nurturing licid').
card_rarity('nurturing licid'/'TMP', 'Uncommon').
card_artist('nurturing licid'/'TMP', 'Mark Poole').
card_multiverse_id('nurturing licid'/'TMP', '4776').

card_in_set('opportunist', 'TMP').
card_original_type('opportunist'/'TMP', 'Summon — Soldier').
card_original_text('opportunist'/'TMP', '{T}: Opportunist deals 1 damage to target creature that was damaged this turn.').
card_first_print('opportunist', 'TMP').
card_image_name('opportunist'/'TMP', 'opportunist').
card_uid('opportunist'/'TMP', 'TMP:Opportunist:opportunist').
card_rarity('opportunist'/'TMP', 'Uncommon').
card_artist('opportunist'/'TMP', 'Dan Frazier').
card_flavor_text('opportunist'/'TMP', 'The third time the Oracle glanced suspiciously at him, Starke decided she had glanced twice too often.').
card_multiverse_id('opportunist'/'TMP', '4836').

card_in_set('oracle en-vec', 'TMP').
card_original_type('oracle en-vec'/'TMP', 'Summon — Wizard').
card_original_text('oracle en-vec'/'TMP', '{T}: Target opponent chooses any number of creatures he or she controls. During that player\'s next turn, those creatures attack if able, and no other creatures can attack. At the end of that turn, destroy each of those creatures that did not attack. Use this ability only during your turn.').
card_first_print('oracle en-vec', 'TMP').
card_image_name('oracle en-vec'/'TMP', 'oracle en-vec').
card_uid('oracle en-vec'/'TMP', 'TMP:Oracle en-Vec:oracle en-vec').
card_rarity('oracle en-vec'/'TMP', 'Rare').
card_artist('oracle en-vec'/'TMP', 'Dan Frazier').
card_multiverse_id('oracle en-vec'/'TMP', '4889').

card_in_set('orim\'s prayer', 'TMP').
card_original_type('orim\'s prayer'/'TMP', 'Enchantment').
card_original_text('orim\'s prayer'/'TMP', 'If any creatures attack you, gain 1 life for each attacking creature.').
card_first_print('orim\'s prayer', 'TMP').
card_image_name('orim\'s prayer'/'TMP', 'orim\'s prayer').
card_uid('orim\'s prayer'/'TMP', 'TMP:Orim\'s Prayer:orim\'s prayer').
card_rarity('orim\'s prayer'/'TMP', 'Uncommon').
card_artist('orim\'s prayer'/'TMP', 'Donato Giancola').
card_flavor_text('orim\'s prayer'/'TMP', '\"As usual, there will be time for prayer only after the worst happens.\"\n—Orim, Samite healer').
card_multiverse_id('orim\'s prayer'/'TMP', '4891').

card_in_set('orim, samite healer', 'TMP').
card_original_type('orim, samite healer'/'TMP', 'Summon — Legend').
card_original_text('orim, samite healer'/'TMP', 'Orim, Samite Healer counts as a Cleric.\n{T}: Prevent up to 3 damage to any creature or player.').
card_first_print('orim, samite healer', 'TMP').
card_image_name('orim, samite healer'/'TMP', 'orim, samite healer').
card_uid('orim, samite healer'/'TMP', 'TMP:Orim, Samite Healer:orim, samite healer').
card_rarity('orim, samite healer'/'TMP', 'Rare').
card_artist('orim, samite healer'/'TMP', 'Kaja Foglio').
card_flavor_text('orim, samite healer'/'TMP', '\"The silkworm spins itself a new existence. So the healer weaves the threads of life.\"\n—Orim, Samite healer').
card_multiverse_id('orim, samite healer'/'TMP', '4890').

card_in_set('overrun', 'TMP').
card_original_type('overrun'/'TMP', 'Sorcery').
card_original_text('overrun'/'TMP', 'All creatures you control get +3/+3 and gain trample until end of turn.').
card_first_print('overrun', 'TMP').
card_image_name('overrun'/'TMP', 'overrun').
card_uid('overrun'/'TMP', 'TMP:Overrun:overrun').
card_rarity('overrun'/'TMP', 'Uncommon').
card_artist('overrun'/'TMP', 'Jeff Miracola').
card_flavor_text('overrun'/'TMP', '\"You want to know your enemy? Look at your feet while you trample him.\"\n—Tahngarth of the Weatherlight').
card_multiverse_id('overrun'/'TMP', '4777').

card_in_set('pacifism', 'TMP').
card_original_type('pacifism'/'TMP', 'Enchant Creature').
card_original_text('pacifism'/'TMP', 'Enchanted creature cannot attack or block.').
card_image_name('pacifism'/'TMP', 'pacifism').
card_uid('pacifism'/'TMP', 'TMP:Pacifism:pacifism').
card_rarity('pacifism'/'TMP', 'Common').
card_artist('pacifism'/'TMP', 'Adam Rex').
card_flavor_text('pacifism'/'TMP', 'Frozen by conscience, Karn did not resist as the moggs carried him to the Predator.').
card_multiverse_id('pacifism'/'TMP', '4892').

card_in_set('pallimud', 'TMP').
card_original_type('pallimud'/'TMP', 'Summon — Beast').
card_original_text('pallimud'/'TMP', 'Pallimud has power equal to the number of tapped lands target opponent controls.').
card_first_print('pallimud', 'TMP').
card_image_name('pallimud'/'TMP', 'pallimud').
card_uid('pallimud'/'TMP', 'TMP:Pallimud:pallimud').
card_rarity('pallimud'/'TMP', 'Rare').
card_artist('pallimud'/'TMP', 'Quinton Hoover').
card_flavor_text('pallimud'/'TMP', 'The old Vec tell tales of a beast that fed on scraps but turned its nose up at the lavish banquet they set in its honor.').
card_multiverse_id('pallimud'/'TMP', '4837').

card_in_set('patchwork gnomes', 'TMP').
card_original_type('patchwork gnomes'/'TMP', 'Artifact Creature').
card_original_text('patchwork gnomes'/'TMP', 'Choose and discard a card: Regenerate Patchwork Gnomes.').
card_first_print('patchwork gnomes', 'TMP').
card_image_name('patchwork gnomes'/'TMP', 'patchwork gnomes').
card_uid('patchwork gnomes'/'TMP', 'TMP:Patchwork Gnomes:patchwork gnomes').
card_rarity('patchwork gnomes'/'TMP', 'Uncommon').
card_artist('patchwork gnomes'/'TMP', 'Mike Raabe').
card_flavor_text('patchwork gnomes'/'TMP', '\"Just when I get them working, they always start fighting and tearing at each other for spare parts.\"\n—Hanna, Weatherlight navigator').
card_multiverse_id('patchwork gnomes'/'TMP', '4619').

card_in_set('pearl medallion', 'TMP').
card_original_type('pearl medallion'/'TMP', 'Artifact').
card_original_text('pearl medallion'/'TMP', 'Your white spells cost {1} less to play.').
card_first_print('pearl medallion', 'TMP').
card_image_name('pearl medallion'/'TMP', 'pearl medallion').
card_uid('pearl medallion'/'TMP', 'TMP:Pearl Medallion:pearl medallion').
card_rarity('pearl medallion'/'TMP', 'Rare').
card_artist('pearl medallion'/'TMP', 'Sue Ellen Brown').
card_multiverse_id('pearl medallion'/'TMP', '4620').

card_in_set('pegasus refuge', 'TMP').
card_original_type('pegasus refuge'/'TMP', 'Enchantment').
card_original_text('pegasus refuge'/'TMP', '{2}, Choose and discard a card: Put a Pegasus token into play. Treat this token as a 1/1 white creature with flying.').
card_first_print('pegasus refuge', 'TMP').
card_image_name('pegasus refuge'/'TMP', 'pegasus refuge').
card_uid('pegasus refuge'/'TMP', 'TMP:Pegasus Refuge:pegasus refuge').
card_rarity('pegasus refuge'/'TMP', 'Rare').
card_artist('pegasus refuge'/'TMP', 'Kev Walker').
card_flavor_text('pegasus refuge'/'TMP', 'The first Rath-born pegasus was so offended by the sky that it hid its eyes in the earth.\n—Vec lore').
card_multiverse_id('pegasus refuge'/'TMP', '4893').

card_in_set('perish', 'TMP').
card_original_type('perish'/'TMP', 'Sorcery').
card_original_text('perish'/'TMP', 'Destroy all green creatures. Those creatures cannot be regenerated this turn.').
card_first_print('perish', 'TMP').
card_image_name('perish'/'TMP', 'perish').
card_uid('perish'/'TMP', 'TMP:Perish:perish').
card_rarity('perish'/'TMP', 'Uncommon').
card_artist('perish'/'TMP', 'Rebecca Guay').
card_flavor_text('perish'/'TMP', '\"There will come a time when the voices of soil and seedling will sing only laments.\"\n—Oracle en-Vec').
card_multiverse_id('perish'/'TMP', '4676').

card_in_set('phyrexian grimoire', 'TMP').
card_original_type('phyrexian grimoire'/'TMP', 'Artifact').
card_original_text('phyrexian grimoire'/'TMP', '{4}, {T}: Target opponent chooses one of the top two cards in your graveyard. Remove that card from the game and put the other into your hand.').
card_first_print('phyrexian grimoire', 'TMP').
card_image_name('phyrexian grimoire'/'TMP', 'phyrexian grimoire').
card_uid('phyrexian grimoire'/'TMP', 'TMP:Phyrexian Grimoire:phyrexian grimoire').
card_rarity('phyrexian grimoire'/'TMP', 'Rare').
card_artist('phyrexian grimoire'/'TMP', 'Doug Chaffee').
card_flavor_text('phyrexian grimoire'/'TMP', 'Every page cost a life; every secret cost a thousand more.').
card_multiverse_id('phyrexian grimoire'/'TMP', '4621').

card_in_set('phyrexian hulk', 'TMP').
card_original_type('phyrexian hulk'/'TMP', 'Artifact Creature').
card_original_text('phyrexian hulk'/'TMP', '').
card_first_print('phyrexian hulk', 'TMP').
card_image_name('phyrexian hulk'/'TMP', 'phyrexian hulk').
card_uid('phyrexian hulk'/'TMP', 'TMP:Phyrexian Hulk:phyrexian hulk').
card_rarity('phyrexian hulk'/'TMP', 'Uncommon').
card_artist('phyrexian hulk'/'TMP', 'Matthew D. Wilson').
card_flavor_text('phyrexian hulk'/'TMP', '\"They are convenient in having no souls, but less so in having no spirit.\"\n—Volrath').
card_multiverse_id('phyrexian hulk'/'TMP', '4622').

card_in_set('phyrexian splicer', 'TMP').
card_original_type('phyrexian splicer'/'TMP', 'Artifact').
card_original_text('phyrexian splicer'/'TMP', '{2}, {T}: Choose flying, first strike, trample, or shadow. Target creature with that ability loses it until end of turn. Another target creature gains that ability until end of turn.').
card_first_print('phyrexian splicer', 'TMP').
card_image_name('phyrexian splicer'/'TMP', 'phyrexian splicer').
card_uid('phyrexian splicer'/'TMP', 'TMP:Phyrexian Splicer:phyrexian splicer').
card_rarity('phyrexian splicer'/'TMP', 'Uncommon').
card_artist('phyrexian splicer'/'TMP', 'Brom').
card_multiverse_id('phyrexian splicer'/'TMP', '4623').

card_in_set('pincher beetles', 'TMP').
card_original_type('pincher beetles'/'TMP', 'Summon — Insects').
card_original_text('pincher beetles'/'TMP', 'Pincher Beetles cannot be the target of spells or abilities.').
card_first_print('pincher beetles', 'TMP').
card_image_name('pincher beetles'/'TMP', 'pincher beetles').
card_uid('pincher beetles'/'TMP', 'TMP:Pincher Beetles:pincher beetles').
card_rarity('pincher beetles'/'TMP', 'Common').
card_artist('pincher beetles'/'TMP', 'Stephen Daniele').
card_flavor_text('pincher beetles'/'TMP', '\"No fair! Since when does a bug get ta munch on me?\"\n—Squee, goblin cabin hand').
card_multiverse_id('pincher beetles'/'TMP', '4778').

card_in_set('pine barrens', 'TMP').
card_original_type('pine barrens'/'TMP', 'Land').
card_original_text('pine barrens'/'TMP', 'Pine Barrens comes into play tapped.\n{T}: Add one colorless mana to your mana pool.\n{T}: Add {B} or {G} to your mana pool. Pine Barrens deals 1 damage to you.').
card_first_print('pine barrens', 'TMP').
card_image_name('pine barrens'/'TMP', 'pine barrens').
card_uid('pine barrens'/'TMP', 'TMP:Pine Barrens:pine barrens').
card_rarity('pine barrens'/'TMP', 'Rare').
card_artist('pine barrens'/'TMP', 'Rebecca Guay').
card_multiverse_id('pine barrens'/'TMP', '4935').

card_in_set('pit imp', 'TMP').
card_original_type('pit imp'/'TMP', 'Summon — Imp').
card_original_text('pit imp'/'TMP', 'Flying\n{B}: Pit Imp gets +1/+0 until end of turn. You cannot spend more than {B}{B} in this way each turn.').
card_first_print('pit imp', 'TMP').
card_image_name('pit imp'/'TMP', 'pit imp').
card_uid('pit imp'/'TMP', 'TMP:Pit Imp:pit imp').
card_rarity('pit imp'/'TMP', 'Common').
card_artist('pit imp'/'TMP', 'Phil Foglio').
card_flavor_text('pit imp'/'TMP', 'The moans of the Death Pits were underscored by the chattering of imps, for whom the ship was cause for much discussion.').
card_multiverse_id('pit imp'/'TMP', '4677').

card_in_set('plains', 'TMP').
card_original_type('plains'/'TMP', 'Land').
card_original_text('plains'/'TMP', '{T}: Add {W} to your mana pool.').
card_image_name('plains'/'TMP', 'plains1').
card_uid('plains'/'TMP', 'TMP:Plains:plains1').
card_rarity('plains'/'TMP', 'Basic Land').
card_artist('plains'/'TMP', 'Terese Nielsen').
card_multiverse_id('plains'/'TMP', '4953').

card_in_set('plains', 'TMP').
card_original_type('plains'/'TMP', 'Land').
card_original_text('plains'/'TMP', '{T}: Add {W} to your mana pool.').
card_image_name('plains'/'TMP', 'plains2').
card_uid('plains'/'TMP', 'TMP:Plains:plains2').
card_rarity('plains'/'TMP', 'Basic Land').
card_artist('plains'/'TMP', 'Terese Nielsen').
card_multiverse_id('plains'/'TMP', '4954').

card_in_set('plains', 'TMP').
card_original_type('plains'/'TMP', 'Land').
card_original_text('plains'/'TMP', '{T}: Add {W} to your mana pool.').
card_image_name('plains'/'TMP', 'plains3').
card_uid('plains'/'TMP', 'TMP:Plains:plains3').
card_rarity('plains'/'TMP', 'Basic Land').
card_artist('plains'/'TMP', 'Terese Nielsen').
card_multiverse_id('plains'/'TMP', '4955').

card_in_set('plains', 'TMP').
card_original_type('plains'/'TMP', 'Land').
card_original_text('plains'/'TMP', '{T}: Add {W} to your mana pool.').
card_image_name('plains'/'TMP', 'plains4').
card_uid('plains'/'TMP', 'TMP:Plains:plains4').
card_rarity('plains'/'TMP', 'Basic Land').
card_artist('plains'/'TMP', 'Terese Nielsen').
card_multiverse_id('plains'/'TMP', '4956').

card_in_set('power sink', 'TMP').
card_original_type('power sink'/'TMP', 'Interrupt').
card_original_text('power sink'/'TMP', 'Counter target spell unless its caster pays an additional {X}. If he or she does not, tap all mana-producing lands that player controls and remove all mana from his or her mana pool.').
card_image_name('power sink'/'TMP', 'power sink').
card_uid('power sink'/'TMP', 'TMP:Power Sink:power sink').
card_rarity('power sink'/'TMP', 'Common').
card_artist('power sink'/'TMP', 'Jeff Miracola').
card_multiverse_id('power sink'/'TMP', '4716').

card_in_set('precognition', 'TMP').
card_original_type('precognition'/'TMP', 'Enchantment').
card_original_text('precognition'/'TMP', 'During your upkeep, you may look at the top card of target opponent\'s library. You may then put that card on the bottom of his or her library.').
card_first_print('precognition', 'TMP').
card_image_name('precognition'/'TMP', 'precognition').
card_uid('precognition'/'TMP', 'TMP:Precognition:precognition').
card_rarity('precognition'/'TMP', 'Rare').
card_artist('precognition'/'TMP', 'Jeff Miracola').
card_flavor_text('precognition'/'TMP', 'A gleam like struggling sunlight penetrated Selenia\'s dark thoughts.').
card_multiverse_id('precognition'/'TMP', '4717').

card_in_set('propaganda', 'TMP').
card_original_type('propaganda'/'TMP', 'Enchantment').
card_original_text('propaganda'/'TMP', 'Each turn, each creature cannot attack you unless its controller pays an additional {2} for that creature.').
card_first_print('propaganda', 'TMP').
card_image_name('propaganda'/'TMP', 'propaganda').
card_uid('propaganda'/'TMP', 'TMP:Propaganda:propaganda').
card_rarity('propaganda'/'TMP', 'Uncommon').
card_artist('propaganda'/'TMP', 'Jeff Miracola').
card_flavor_text('propaganda'/'TMP', '\"You\'ve failed Gerrard. You\'ve failed the Legacy. You\'ve failed yourself. I can do no more.\"\n—Volrath, to Karn').
card_multiverse_id('propaganda'/'TMP', '4718').

card_in_set('puppet strings', 'TMP').
card_original_type('puppet strings'/'TMP', 'Artifact').
card_original_text('puppet strings'/'TMP', '{2}, {T}: Tap or untap target creature.').
card_first_print('puppet strings', 'TMP').
card_image_name('puppet strings'/'TMP', 'puppet strings').
card_uid('puppet strings'/'TMP', 'TMP:Puppet Strings:puppet strings').
card_rarity('puppet strings'/'TMP', 'Uncommon').
card_artist('puppet strings'/'TMP', 'Scott Kirschner').
card_flavor_text('puppet strings'/'TMP', '\"Have no illusions,\" Volrath warned Greven, \"about free will.\"').
card_multiverse_id('puppet strings'/'TMP', '4624').

card_in_set('quickening licid', 'TMP').
card_original_type('quickening licid'/'TMP', 'Summon — Licid').
card_original_text('quickening licid'/'TMP', '{1}{W}, {T}: Quickening Licid loses this ability and becomes a creature enchantment that reads \"Enchanted creature gains first strike\" instead of a creature. Move Quickening Licid onto target creature. You may pay {W} to end this effect.').
card_first_print('quickening licid', 'TMP').
card_image_name('quickening licid'/'TMP', 'quickening licid').
card_uid('quickening licid'/'TMP', 'TMP:Quickening Licid:quickening licid').
card_rarity('quickening licid'/'TMP', 'Uncommon').
card_artist('quickening licid'/'TMP', 'Andrew Robinson').
card_multiverse_id('quickening licid'/'TMP', '4894').

card_in_set('rain of tears', 'TMP').
card_original_type('rain of tears'/'TMP', 'Sorcery').
card_original_text('rain of tears'/'TMP', 'Destroy target land.').
card_image_name('rain of tears'/'TMP', 'rain of tears').
card_uid('rain of tears'/'TMP', 'TMP:Rain of Tears:rain of tears').
card_rarity('rain of tears'/'TMP', 'Uncommon').
card_artist('rain of tears'/'TMP', 'Charles Gillespie').
card_flavor_text('rain of tears'/'TMP', '\"These rains cannot quench; they carve the land\'s face like a scalpel on flesh.\"\n—Crovax').
card_multiverse_id('rain of tears'/'TMP', '4678').

card_in_set('rampant growth', 'TMP').
card_original_type('rampant growth'/'TMP', 'Sorcery').
card_original_text('rampant growth'/'TMP', 'Search your library for a basic land card and put it into play, tapped. Shuffle your library afterwards.').
card_image_name('rampant growth'/'TMP', 'rampant growth').
card_uid('rampant growth'/'TMP', 'TMP:Rampant Growth:rampant growth').
card_rarity('rampant growth'/'TMP', 'Common').
card_artist('rampant growth'/'TMP', 'Tom Kyffin').
card_flavor_text('rampant growth'/'TMP', 'Rath forces plants from its surface as one presses whey from cheese.').
card_multiverse_id('rampant growth'/'TMP', '4779').

card_in_set('ranger en-vec', 'TMP').
card_original_type('ranger en-vec'/'TMP', 'Summon — Soldier').
card_original_text('ranger en-vec'/'TMP', 'First strike\n{G}: Regenerate Ranger en-Vec.').
card_first_print('ranger en-vec', 'TMP').
card_image_name('ranger en-vec'/'TMP', 'ranger en-vec').
card_uid('ranger en-vec'/'TMP', 'TMP:Ranger en-Vec:ranger en-vec').
card_rarity('ranger en-vec'/'TMP', 'Uncommon').
card_artist('ranger en-vec'/'TMP', 'Randy Elliott').
card_flavor_text('ranger en-vec'/'TMP', '\"The path of least resistance will seldom lead you beyond your doorstep.\"\n—Oracle en-Vec').
card_multiverse_id('ranger en-vec'/'TMP', '4914').

card_in_set('rathi dragon', 'TMP').
card_original_type('rathi dragon'/'TMP', 'Summon — Dragon').
card_original_text('rathi dragon'/'TMP', 'Flying \nWhen Rathi Dragon comes into play, sacrifice two mountains or sacrifice Rathi Dragon.').
card_first_print('rathi dragon', 'TMP').
card_image_name('rathi dragon'/'TMP', 'rathi dragon').
card_uid('rathi dragon'/'TMP', 'TMP:Rathi Dragon:rathi dragon').
card_rarity('rathi dragon'/'TMP', 'Rare').
card_artist('rathi dragon'/'TMP', 'Christopher Rush').
card_flavor_text('rathi dragon'/'TMP', 'Wrap the flame as twine\nKingdoms will be thine.').
card_multiverse_id('rathi dragon'/'TMP', '4838').

card_in_set('rats of rath', 'TMP').
card_original_type('rats of rath'/'TMP', 'Summon — Rats').
card_original_text('rats of rath'/'TMP', '{B}: Destroy target artifact, creature, or land you control.').
card_first_print('rats of rath', 'TMP').
card_image_name('rats of rath'/'TMP', 'rats of rath').
card_uid('rats of rath'/'TMP', 'TMP:Rats of Rath:rats of rath').
card_rarity('rats of rath'/'TMP', 'Common').
card_artist('rats of rath'/'TMP', 'John Matson').
card_flavor_text('rats of rath'/'TMP', '\"These creatures offer new challenges in the scrutiny of entrails.\"\n—Oracle en-Vec').
card_multiverse_id('rats of rath'/'TMP', '4679').

card_in_set('reality anchor', 'TMP').
card_original_type('reality anchor'/'TMP', 'Instant').
card_original_text('reality anchor'/'TMP', 'Target creature loses shadow until end of turn.\nDraw a card.').
card_first_print('reality anchor', 'TMP').
card_image_name('reality anchor'/'TMP', 'reality anchor').
card_uid('reality anchor'/'TMP', 'TMP:Reality Anchor:reality anchor').
card_rarity('reality anchor'/'TMP', 'Common').
card_artist('reality anchor'/'TMP', 'Randy Gallegos').
card_flavor_text('reality anchor'/'TMP', '\"See how you like it.\"\n—Ertai, wizard adept').
card_multiverse_id('reality anchor'/'TMP', '4780').

card_in_set('reanimate', 'TMP').
card_original_type('reanimate'/'TMP', 'Sorcery').
card_original_text('reanimate'/'TMP', 'Put target creature card from any graveyard into play under your control. Lose life equal to that creature\'s total casting cost.').
card_first_print('reanimate', 'TMP').
card_image_name('reanimate'/'TMP', 'reanimate').
card_uid('reanimate'/'TMP', 'TMP:Reanimate:reanimate').
card_rarity('reanimate'/'TMP', 'Uncommon').
card_artist('reanimate'/'TMP', 'Robert Bliss').
card_flavor_text('reanimate'/'TMP', '\"You will learn to earn death.\"\n—Volrath').
card_multiverse_id('reanimate'/'TMP', '4680').

card_in_set('reap', 'TMP').
card_original_type('reap'/'TMP', 'Instant').
card_original_text('reap'/'TMP', 'Return any number of target cards from your graveyard to your hand. You cannot choose more cards than the number of black permanents target opponent controls.').
card_first_print('reap', 'TMP').
card_image_name('reap'/'TMP', 'reap').
card_uid('reap'/'TMP', 'TMP:Reap:reap').
card_rarity('reap'/'TMP', 'Uncommon').
card_artist('reap'/'TMP', 'Ron Chironna').
card_flavor_text('reap'/'TMP', '\"Let Volrath choke on his crop of hatred.\"\n—Eladamri, Lord of Leaves').
card_multiverse_id('reap'/'TMP', '4781').

card_in_set('reckless spite', 'TMP').
card_original_type('reckless spite'/'TMP', 'Instant').
card_original_text('reckless spite'/'TMP', 'Destroy two target nonblack creatures. Lose 5 life.').
card_first_print('reckless spite', 'TMP').
card_image_name('reckless spite'/'TMP', 'reckless spite').
card_uid('reckless spite'/'TMP', 'TMP:Reckless Spite:reckless spite').
card_rarity('reckless spite'/'TMP', 'Uncommon').
card_artist('reckless spite'/'TMP', 'Pete Venters').
card_flavor_text('reckless spite'/'TMP', '\"My plan\'s elegance leads my enemies to believe it is spite that fuels me.\"\n—Volrath').
card_multiverse_id('reckless spite'/'TMP', '4681').

card_in_set('recycle', 'TMP').
card_original_type('recycle'/'TMP', 'Enchantment').
card_original_text('recycle'/'TMP', 'Skip your draw phase.\nWhenever you play a card, draw a card.\nDuring your discard phase, choose and discard all but two cards.').
card_first_print('recycle', 'TMP').
card_image_name('recycle'/'TMP', 'recycle').
card_uid('recycle'/'TMP', 'TMP:Recycle:recycle').
card_rarity('recycle'/'TMP', 'Rare').
card_artist('recycle'/'TMP', 'Phil Foglio').
card_multiverse_id('recycle'/'TMP', '4782').

card_in_set('reflecting pool', 'TMP').
card_original_type('reflecting pool'/'TMP', 'Land').
card_original_text('reflecting pool'/'TMP', '{T}: Add to your mana pool one mana of any type that any land you control can produce.').
card_first_print('reflecting pool', 'TMP').
card_image_name('reflecting pool'/'TMP', 'reflecting pool').
card_uid('reflecting pool'/'TMP', 'TMP:Reflecting Pool:reflecting pool').
card_rarity('reflecting pool'/'TMP', 'Rare').
card_artist('reflecting pool'/'TMP', 'Adam Rex').
card_flavor_text('reflecting pool'/'TMP', '\"The reflection of land interests me far more than its reality.\"\n—Volrath').
card_multiverse_id('reflecting pool'/'TMP', '4936').

card_in_set('renegade warlord', 'TMP').
card_original_type('renegade warlord'/'TMP', 'Summon — Soldier').
card_original_text('renegade warlord'/'TMP', 'First strike\nIf Renegade Warlord attacks, each other attacking creature gets +1/+0 until end of turn.').
card_first_print('renegade warlord', 'TMP').
card_image_name('renegade warlord'/'TMP', 'renegade warlord').
card_uid('renegade warlord'/'TMP', 'TMP:Renegade Warlord:renegade warlord').
card_rarity('renegade warlord'/'TMP', 'Uncommon').
card_artist('renegade warlord'/'TMP', 'Ron Spencer').
card_flavor_text('renegade warlord'/'TMP', '\"His tongue must be honey, the way he gathers ants to fight with him.\"\n—Volrath').
card_multiverse_id('renegade warlord'/'TMP', '4839').

card_in_set('repentance', 'TMP').
card_original_type('repentance'/'TMP', 'Sorcery').
card_original_text('repentance'/'TMP', 'Target creature deals to itself damage equal to its power.').
card_first_print('repentance', 'TMP').
card_image_name('repentance'/'TMP', 'repentance').
card_uid('repentance'/'TMP', 'TMP:Repentance:repentance').
card_rarity('repentance'/'TMP', 'Uncommon').
card_artist('repentance'/'TMP', 'Ron Spencer').
card_flavor_text('repentance'/'TMP', '\"The cannon wasn\'t aimed at you!\" pleaded Vhati.\n\"I\'m not sure which is more pathetic,\" replied Greven, \"your judgment or your aim.\"').
card_multiverse_id('repentance'/'TMP', '4895').

card_in_set('respite', 'TMP').
card_original_type('respite'/'TMP', 'Instant').
card_original_text('respite'/'TMP', 'Creatures deal no combat damage this turn. Gain 1 life for each attacking creature.').
card_first_print('respite', 'TMP').
card_image_name('respite'/'TMP', 'respite').
card_uid('respite'/'TMP', 'TMP:Respite:respite').
card_rarity('respite'/'TMP', 'Common').
card_artist('respite'/'TMP', 'Rebecca Guay').
card_flavor_text('respite'/'TMP', '\"If they board us we\'re finished,\" warned Orim. Crovax nodded. \"And if they don\'t . . . what then?\"').
card_multiverse_id('respite'/'TMP', '4783').

card_in_set('rolling thunder', 'TMP').
card_original_type('rolling thunder'/'TMP', 'Sorcery').
card_original_text('rolling thunder'/'TMP', 'Rolling Thunder deals X damage divided any way you choose among any number of target creatures and/or players.').
card_first_print('rolling thunder', 'TMP').
card_image_name('rolling thunder'/'TMP', 'rolling thunder').
card_uid('rolling thunder'/'TMP', 'TMP:Rolling Thunder:rolling thunder').
card_rarity('rolling thunder'/'TMP', 'Common').
card_artist('rolling thunder'/'TMP', 'Richard Thomas').
card_flavor_text('rolling thunder'/'TMP', '\"Such rage,\" thought Vhati, gazing up at the thunderhead from the Predator. \"It is Greven\'s mind manifest.\"').
card_multiverse_id('rolling thunder'/'TMP', '4840').

card_in_set('root maze', 'TMP').
card_original_type('root maze'/'TMP', 'Enchantment').
card_original_text('root maze'/'TMP', 'All artifacts and lands come into play tapped.').
card_first_print('root maze', 'TMP').
card_image_name('root maze'/'TMP', 'root maze').
card_uid('root maze'/'TMP', 'TMP:Root Maze:root maze').
card_rarity('root maze'/'TMP', 'Rare').
card_artist('root maze'/'TMP', 'Rebecca Guay').
card_flavor_text('root maze'/'TMP', '\"We should step up repairs. I think the forest has plans for us.\"\n—Crovax').
card_multiverse_id('root maze'/'TMP', '4784').

card_in_set('rootbreaker wurm', 'TMP').
card_original_type('rootbreaker wurm'/'TMP', 'Summon — Wurm').
card_original_text('rootbreaker wurm'/'TMP', 'Trample').
card_first_print('rootbreaker wurm', 'TMP').
card_image_name('rootbreaker wurm'/'TMP', 'rootbreaker wurm').
card_uid('rootbreaker wurm'/'TMP', 'TMP:Rootbreaker Wurm:rootbreaker wurm').
card_rarity('rootbreaker wurm'/'TMP', 'Common').
card_artist('rootbreaker wurm'/'TMP', 'Richard Kane Ferguson').
card_flavor_text('rootbreaker wurm'/'TMP', 'As Gerrard made his escape, the wurm covered his flight by helping itself to three great mouthfuls of merfolk.').
card_multiverse_id('rootbreaker wurm'/'TMP', '4785').

card_in_set('rootwalla', 'TMP').
card_original_type('rootwalla'/'TMP', 'Summon — Lizard').
card_original_text('rootwalla'/'TMP', '{1}{G}: Rootwalla gets +2/+2 until end of turn. Use this ability only once each turn.').
card_first_print('rootwalla', 'TMP').
card_image_name('rootwalla'/'TMP', 'rootwalla').
card_uid('rootwalla'/'TMP', 'TMP:Rootwalla:rootwalla').
card_rarity('rootwalla'/'TMP', 'Common').
card_artist('rootwalla'/'TMP', 'Roger Raupp').
card_flavor_text('rootwalla'/'TMP', 'If you try to sneak up on a rootwalla, you\'ll suddenly find yourself dealing with twice the lizard.').
card_multiverse_id('rootwalla'/'TMP', '4786').

card_in_set('rootwater depths', 'TMP').
card_original_type('rootwater depths'/'TMP', 'Land').
card_original_text('rootwater depths'/'TMP', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {U} or {B} to your mana pool. Rootwater Depths does not untap during your next untap phase.').
card_first_print('rootwater depths', 'TMP').
card_image_name('rootwater depths'/'TMP', 'rootwater depths').
card_uid('rootwater depths'/'TMP', 'TMP:Rootwater Depths:rootwater depths').
card_rarity('rootwater depths'/'TMP', 'Uncommon').
card_artist('rootwater depths'/'TMP', 'Roger Raupp').
card_multiverse_id('rootwater depths'/'TMP', '4937').

card_in_set('rootwater diver', 'TMP').
card_original_type('rootwater diver'/'TMP', 'Summon — Merfolk').
card_original_text('rootwater diver'/'TMP', '{T}, Sacrifice Rootwater Diver: Return target artifact card from your graveyard to your hand.').
card_first_print('rootwater diver', 'TMP').
card_image_name('rootwater diver'/'TMP', 'rootwater diver').
card_uid('rootwater diver'/'TMP', 'TMP:Rootwater Diver:rootwater diver').
card_rarity('rootwater diver'/'TMP', 'Uncommon').
card_artist('rootwater diver'/'TMP', 'Ron Spencer').
card_flavor_text('rootwater diver'/'TMP', '\"Drop a dagger into the murky deep and the merfolk will cut your throat with it.\"\n—Skyshroud saying').
card_multiverse_id('rootwater diver'/'TMP', '4720').

card_in_set('rootwater hunter', 'TMP').
card_original_type('rootwater hunter'/'TMP', 'Summon — Merfolk').
card_original_text('rootwater hunter'/'TMP', '{T}: Rootwater Hunter deals 1 damage to target creature or player.').
card_first_print('rootwater hunter', 'TMP').
card_image_name('rootwater hunter'/'TMP', 'rootwater hunter').
card_uid('rootwater hunter'/'TMP', 'TMP:Rootwater Hunter:rootwater hunter').
card_rarity('rootwater hunter'/'TMP', 'Common').
card_artist('rootwater hunter'/'TMP', 'Brom').
card_flavor_text('rootwater hunter'/'TMP', '\"Bitter water   vicious wave\nShadow-cold shallows   root-made maze\nHome\'s angry embrace.\"\n—Rootwater Saga').
card_multiverse_id('rootwater hunter'/'TMP', '4721').

card_in_set('rootwater matriarch', 'TMP').
card_original_type('rootwater matriarch'/'TMP', 'Summon — Merfolk').
card_original_text('rootwater matriarch'/'TMP', '{T}: Gain control of target creature as long as that creature has any enchantments on it.').
card_first_print('rootwater matriarch', 'TMP').
card_image_name('rootwater matriarch'/'TMP', 'rootwater matriarch').
card_uid('rootwater matriarch'/'TMP', 'TMP:Rootwater Matriarch:rootwater matriarch').
card_rarity('rootwater matriarch'/'TMP', 'Rare').
card_artist('rootwater matriarch'/'TMP', 'Randy Gallegos').
card_multiverse_id('rootwater matriarch'/'TMP', '4722').

card_in_set('rootwater shaman', 'TMP').
card_original_type('rootwater shaman'/'TMP', 'Summon — Merfolk').
card_original_text('rootwater shaman'/'TMP', 'You may play creature enchantments whenever you could play an instant.').
card_first_print('rootwater shaman', 'TMP').
card_image_name('rootwater shaman'/'TMP', 'rootwater shaman').
card_uid('rootwater shaman'/'TMP', 'TMP:Rootwater Shaman:rootwater shaman').
card_rarity('rootwater shaman'/'TMP', 'Rare').
card_artist('rootwater shaman'/'TMP', 'Paolo Parente').
card_flavor_text('rootwater shaman'/'TMP', '\"Ugh! When did orcs and fish start having kids?\"\n—Gerrard of the Weatherlight').
card_multiverse_id('rootwater shaman'/'TMP', '4723').

card_in_set('ruby medallion', 'TMP').
card_original_type('ruby medallion'/'TMP', 'Artifact').
card_original_text('ruby medallion'/'TMP', 'Your red spells cost {1} less to play.').
card_first_print('ruby medallion', 'TMP').
card_image_name('ruby medallion'/'TMP', 'ruby medallion').
card_uid('ruby medallion'/'TMP', 'TMP:Ruby Medallion:ruby medallion').
card_rarity('ruby medallion'/'TMP', 'Rare').
card_artist('ruby medallion'/'TMP', 'Sue Ellen Brown').
card_multiverse_id('ruby medallion'/'TMP', '4625').

card_in_set('sacred guide', 'TMP').
card_original_type('sacred guide'/'TMP', 'Summon — Cleric').
card_original_text('sacred guide'/'TMP', '{1}{W}, Sacrifice Sacred Guide: Reveal cards from your library until you reveal a white card. Put that card into your hand. Remove all other revealed cards from the game.').
card_first_print('sacred guide', 'TMP').
card_image_name('sacred guide'/'TMP', 'sacred guide').
card_uid('sacred guide'/'TMP', 'TMP:Sacred Guide:sacred guide').
card_rarity('sacred guide'/'TMP', 'Rare').
card_artist('sacred guide'/'TMP', 'Zina Saunders').
card_multiverse_id('sacred guide'/'TMP', '4896').

card_in_set('sadistic glee', 'TMP').
card_original_type('sadistic glee'/'TMP', 'Enchant Creature').
card_original_text('sadistic glee'/'TMP', 'Whenever any creature is put into any graveyard from play, put a +1/+1 counter on enchanted creature.').
card_first_print('sadistic glee', 'TMP').
card_image_name('sadistic glee'/'TMP', 'sadistic glee').
card_uid('sadistic glee'/'TMP', 'TMP:Sadistic Glee:sadistic glee').
card_rarity('sadistic glee'/'TMP', 'Common').
card_artist('sadistic glee'/'TMP', 'Pete Venters').
card_flavor_text('sadistic glee'/'TMP', '\"First blood isn\'t as important as last blood.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('sadistic glee'/'TMP', '4682').

card_in_set('safeguard', 'TMP').
card_original_type('safeguard'/'TMP', 'Enchantment').
card_original_text('safeguard'/'TMP', '{2}{W}: Target creature deals no combat damage this turn.').
card_first_print('safeguard', 'TMP').
card_image_name('safeguard'/'TMP', 'safeguard').
card_uid('safeguard'/'TMP', 'TMP:Safeguard:safeguard').
card_rarity('safeguard'/'TMP', 'Rare').
card_artist('safeguard'/'TMP', 'Thomas M. Baxa').
card_flavor_text('safeguard'/'TMP', '\"I\'m amused by wasted effort when it\'s not my own.\"\n—Hanna, Weatherlight navigator').
card_multiverse_id('safeguard'/'TMP', '4897').

card_in_set('salt flats', 'TMP').
card_original_type('salt flats'/'TMP', 'Land').
card_original_text('salt flats'/'TMP', 'Salt Flats comes into play tapped.\n{T}: Add one colorless mana to your mana pool.\n{T}: Add {W} or {B} to your mana pool. Salt Flats deals 1 damage to you.').
card_first_print('salt flats', 'TMP').
card_image_name('salt flats'/'TMP', 'salt flats').
card_uid('salt flats'/'TMP', 'TMP:Salt Flats:salt flats').
card_rarity('salt flats'/'TMP', 'Rare').
card_artist('salt flats'/'TMP', 'Scott Kirschner').
card_multiverse_id('salt flats'/'TMP', '4938').

card_in_set('sandstone warrior', 'TMP').
card_original_type('sandstone warrior'/'TMP', 'Summon — Soldier').
card_original_text('sandstone warrior'/'TMP', 'First strike\n{R}: Sandstone Warrior gets +1/+0 until end of turn.').
card_first_print('sandstone warrior', 'TMP').
card_image_name('sandstone warrior'/'TMP', 'sandstone warrior').
card_uid('sandstone warrior'/'TMP', 'TMP:Sandstone Warrior:sandstone warrior').
card_rarity('sandstone warrior'/'TMP', 'Common').
card_artist('sandstone warrior'/'TMP', 'Stephen Daniele').
card_flavor_text('sandstone warrior'/'TMP', '\"I used to describe something stable as ‘rock solid.\' So much for that expression.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('sandstone warrior'/'TMP', '4841').

card_in_set('sapphire medallion', 'TMP').
card_original_type('sapphire medallion'/'TMP', 'Artifact').
card_original_text('sapphire medallion'/'TMP', 'Your blue spells cost {1} less to play.').
card_first_print('sapphire medallion', 'TMP').
card_image_name('sapphire medallion'/'TMP', 'sapphire medallion').
card_uid('sapphire medallion'/'TMP', 'TMP:Sapphire Medallion:sapphire medallion').
card_rarity('sapphire medallion'/'TMP', 'Rare').
card_artist('sapphire medallion'/'TMP', 'Sue Ellen Brown').
card_multiverse_id('sapphire medallion'/'TMP', '4626').

card_in_set('sarcomancy', 'TMP').
card_original_type('sarcomancy'/'TMP', 'Enchantment').
card_original_text('sarcomancy'/'TMP', 'When Sarcomancy comes into play, put a Zombie token into play. Treat this token as a 2/2 black creature.\nDuring your upkeep, if there are no Zombies in play, Sarcomancy deals 1 damage to you.').
card_first_print('sarcomancy', 'TMP').
card_image_name('sarcomancy'/'TMP', 'sarcomancy').
card_uid('sarcomancy'/'TMP', 'TMP:Sarcomancy:sarcomancy').
card_rarity('sarcomancy'/'TMP', 'Rare').
card_artist('sarcomancy'/'TMP', 'Daren Bader').
card_multiverse_id('sarcomancy'/'TMP', '4683').

card_in_set('scabland', 'TMP').
card_original_type('scabland'/'TMP', 'Land').
card_original_text('scabland'/'TMP', 'Scabland comes into play tapped.\n{T}: Add one colorless mana to your mana pool.\n{T}: Add {R} or {W} to your mana pool. Scabland deals 1 damage to you.').
card_first_print('scabland', 'TMP').
card_image_name('scabland'/'TMP', 'scabland').
card_uid('scabland'/'TMP', 'TMP:Scabland:scabland').
card_rarity('scabland'/'TMP', 'Rare').
card_artist('scabland'/'TMP', 'Andrew Robinson').
card_multiverse_id('scabland'/'TMP', '4939').

card_in_set('scalding tongs', 'TMP').
card_original_type('scalding tongs'/'TMP', 'Artifact').
card_original_text('scalding tongs'/'TMP', 'During your upkeep, if you have three or fewer cards in your hand, Scalding Tongs deals 1 damage to target opponent.').
card_first_print('scalding tongs', 'TMP').
card_image_name('scalding tongs'/'TMP', 'scalding tongs').
card_uid('scalding tongs'/'TMP', 'TMP:Scalding Tongs:scalding tongs').
card_rarity('scalding tongs'/'TMP', 'Rare').
card_artist('scalding tongs'/'TMP', 'Randy Gallegos').
card_flavor_text('scalding tongs'/'TMP', '\"This has nothing to do with what you know and everything to do with what I wish.\"\n—Volrath').
card_multiverse_id('scalding tongs'/'TMP', '4627').

card_in_set('scorched earth', 'TMP').
card_original_type('scorched earth'/'TMP', 'Sorcery').
card_original_text('scorched earth'/'TMP', 'Choose and discard X land cards: Destroy X target lands.').
card_first_print('scorched earth', 'TMP').
card_image_name('scorched earth'/'TMP', 'scorched earth').
card_uid('scorched earth'/'TMP', 'TMP:Scorched Earth:scorched earth').
card_rarity('scorched earth'/'TMP', 'Rare').
card_artist('scorched earth'/'TMP', 'Nicola Leonard').
card_flavor_text('scorched earth'/'TMP', '\"Fire cleanses as well as destroys; that is the nature of change.\"\n—Oracle en-Vec').
card_multiverse_id('scorched earth'/'TMP', '4842').

card_in_set('scragnoth', 'TMP').
card_original_type('scragnoth'/'TMP', 'Summon — Beast').
card_original_text('scragnoth'/'TMP', 'Protection from blue\nWhile Scragnoth is being cast, it cannot be countered.').
card_first_print('scragnoth', 'TMP').
card_image_name('scragnoth'/'TMP', 'scragnoth').
card_uid('scragnoth'/'TMP', 'TMP:Scragnoth:scragnoth').
card_rarity('scragnoth'/'TMP', 'Uncommon').
card_artist('scragnoth'/'TMP', 'Jeff Laubenstein').
card_flavor_text('scragnoth'/'TMP', 'It possesses no intelligence, only counter-intelligence.').
card_multiverse_id('scragnoth'/'TMP', '4787').

card_in_set('screeching harpy', 'TMP').
card_original_type('screeching harpy'/'TMP', 'Summon — Beast').
card_original_text('screeching harpy'/'TMP', 'Flying\n{1}{B}: Regenerate Screeching Harpy.').
card_first_print('screeching harpy', 'TMP').
card_image_name('screeching harpy'/'TMP', 'screeching harpy').
card_uid('screeching harpy'/'TMP', 'TMP:Screeching Harpy:screeching harpy').
card_rarity('screeching harpy'/'TMP', 'Uncommon').
card_artist('screeching harpy'/'TMP', 'Una Fricker').
card_flavor_text('screeching harpy'/'TMP', '\"They are called ‘fowl\' for a reason.\"\n—Mirri of the Weatherlight').
card_multiverse_id('screeching harpy'/'TMP', '4684').

card_in_set('scroll rack', 'TMP').
card_original_type('scroll rack'/'TMP', 'Artifact').
card_original_text('scroll rack'/'TMP', '{1}, {T}: Choose any number of cards in your hand and set those cards aside. Put an equal number of cards from the top of your library into your hand. Then put the cards set aside in this way on top of your library in any order.').
card_first_print('scroll rack', 'TMP').
card_image_name('scroll rack'/'TMP', 'scroll rack').
card_uid('scroll rack'/'TMP', 'TMP:Scroll Rack:scroll rack').
card_rarity('scroll rack'/'TMP', 'Rare').
card_artist('scroll rack'/'TMP', 'Heather Hudson').
card_multiverse_id('scroll rack'/'TMP', '4628').

card_in_set('sea monster', 'TMP').
card_original_type('sea monster'/'TMP', 'Summon — Serpent').
card_original_text('sea monster'/'TMP', 'Sea Monster cannot attack unless defending player controls any islands.').
card_first_print('sea monster', 'TMP').
card_image_name('sea monster'/'TMP', 'sea monster').
card_uid('sea monster'/'TMP', 'TMP:Sea Monster:sea monster').
card_rarity('sea monster'/'TMP', 'Common').
card_artist('sea monster'/'TMP', 'Daniel Gelon').
card_flavor_text('sea monster'/'TMP', 'Water is life. Or perhaps it is just something to wash it down with.').
card_multiverse_id('sea monster'/'TMP', '4724').

card_in_set('searing touch', 'TMP').
card_original_type('searing touch'/'TMP', 'Instant').
card_original_text('searing touch'/'TMP', 'Buyback {4} (You may pay an additional {4} when you play this spell. If you do, put it into your hand instead of your graveyard as part of the spell\'s effect.)\nSearing Touch deals 1 damage to target creature or player.').
card_first_print('searing touch', 'TMP').
card_image_name('searing touch'/'TMP', 'searing touch').
card_uid('searing touch'/'TMP', 'TMP:Searing Touch:searing touch').
card_rarity('searing touch'/'TMP', 'Uncommon').
card_artist('searing touch'/'TMP', 'D. Alexander Gregory').
card_multiverse_id('searing touch'/'TMP', '4843').

card_in_set('seeker of skybreak', 'TMP').
card_original_type('seeker of skybreak'/'TMP', 'Summon — Elf').
card_original_text('seeker of skybreak'/'TMP', '{T}: Untap target creature.').
card_first_print('seeker of skybreak', 'TMP').
card_image_name('seeker of skybreak'/'TMP', 'seeker of skybreak').
card_uid('seeker of skybreak'/'TMP', 'TMP:Seeker of Skybreak:seeker of skybreak').
card_rarity('seeker of skybreak'/'TMP', 'Common').
card_artist('seeker of skybreak'/'TMP', 'Daren Bader').
card_flavor_text('seeker of skybreak'/'TMP', '\"We shun them not for their dream but for their refusal to let such a noble dream die a noble death.\"\n—Eladamri, Lord of Leaves').
card_multiverse_id('seeker of skybreak'/'TMP', '4788').

card_in_set('segmented wurm', 'TMP').
card_original_type('segmented wurm'/'TMP', 'Summon — Wurm').
card_original_text('segmented wurm'/'TMP', 'Whenever Segmented Wurm is the target of a spell or ability, put a -1/-1 counter on it.').
card_first_print('segmented wurm', 'TMP').
card_image_name('segmented wurm'/'TMP', 'segmented wurm').
card_uid('segmented wurm'/'TMP', 'TMP:Segmented Wurm:segmented wurm').
card_rarity('segmented wurm'/'TMP', 'Uncommon').
card_artist('segmented wurm'/'TMP', 'Jeff Miracola').
card_flavor_text('segmented wurm'/'TMP', '\"If only we could so easily leave behind those parts of ourselves that pain us.\"\n—Crovax').
card_multiverse_id('segmented wurm'/'TMP', '4915').

card_in_set('selenia, dark angel', 'TMP').
card_original_type('selenia, dark angel'/'TMP', 'Summon — Legend').
card_original_text('selenia, dark angel'/'TMP', 'Flying\nSelenia, Dark Angel counts as an Angel.\nPay 2 life: Return Selenia to owner\'s hand.').
card_first_print('selenia, dark angel', 'TMP').
card_image_name('selenia, dark angel'/'TMP', 'selenia, dark angel').
card_uid('selenia, dark angel'/'TMP', 'TMP:Selenia, Dark Angel:selenia, dark angel').
card_rarity('selenia, dark angel'/'TMP', 'Rare').
card_artist('selenia, dark angel'/'TMP', 'Matthew D. Wilson').
card_flavor_text('selenia, dark angel'/'TMP', '\"I am light. I am dark. I must give my life to serve; not even death can release me.\"\n—Selenia, dark angel').
card_multiverse_id('selenia, dark angel'/'TMP', '4916').

card_in_set('serene offering', 'TMP').
card_original_type('serene offering'/'TMP', 'Instant').
card_original_text('serene offering'/'TMP', 'Destroy target enchantment. Gain life equal to that enchantment\'s total casting cost.').
card_first_print('serene offering', 'TMP').
card_image_name('serene offering'/'TMP', 'serene offering').
card_uid('serene offering'/'TMP', 'TMP:Serene Offering:serene offering').
card_rarity('serene offering'/'TMP', 'Uncommon').
card_artist('serene offering'/'TMP', 'Paolo Parente').
card_flavor_text('serene offering'/'TMP', '\"A sacrifice made in peace is worth a dozen made in panic.\"\n—Orim, Samite healer').
card_multiverse_id('serene offering'/'TMP', '4898').

card_in_set('servant of volrath', 'TMP').
card_original_type('servant of volrath'/'TMP', 'Summon — Minion').
card_original_text('servant of volrath'/'TMP', 'If Servant of Volrath leaves play, sacrifice a creature.').
card_first_print('servant of volrath', 'TMP').
card_image_name('servant of volrath'/'TMP', 'servant of volrath').
card_uid('servant of volrath'/'TMP', 'TMP:Servant of Volrath:servant of volrath').
card_rarity('servant of volrath'/'TMP', 'Common').
card_artist('servant of volrath'/'TMP', 'Brian Snõddy').
card_flavor_text('servant of volrath'/'TMP', '\"Volrath is a harsh master. My reflection is a constant reminder of that.\"\n—Greven il-Vec').
card_multiverse_id('servant of volrath'/'TMP', '4685').

card_in_set('shadow rift', 'TMP').
card_original_type('shadow rift'/'TMP', 'Instant').
card_original_text('shadow rift'/'TMP', 'Target creature gains shadow until end of turn. (This creature can block or be blocked by only creatures with shadow.)\nDraw a card.').
card_first_print('shadow rift', 'TMP').
card_image_name('shadow rift'/'TMP', 'shadow rift').
card_uid('shadow rift'/'TMP', 'TMP:Shadow Rift:shadow rift').
card_rarity('shadow rift'/'TMP', 'Common').
card_artist('shadow rift'/'TMP', 'Adam Rex').
card_multiverse_id('shadow rift'/'TMP', '4725').

card_in_set('shadowstorm', 'TMP').
card_original_type('shadowstorm'/'TMP', 'Sorcery').
card_original_text('shadowstorm'/'TMP', 'Shadowstorm deals 2 damage to each creature with shadow.').
card_first_print('shadowstorm', 'TMP').
card_image_name('shadowstorm'/'TMP', 'shadowstorm').
card_uid('shadowstorm'/'TMP', 'TMP:Shadowstorm:shadowstorm').
card_rarity('shadowstorm'/'TMP', 'Uncommon').
card_artist('shadowstorm'/'TMP', 'Adam Rex').
card_flavor_text('shadowstorm'/'TMP', '\"You\'ve not seen a storm until you\'ve seen the kind that roars between worlds.\"\n—Lyna, Soltari emissary').
card_multiverse_id('shadowstorm'/'TMP', '4846').

card_in_set('shatter', 'TMP').
card_original_type('shatter'/'TMP', 'Instant').
card_original_text('shatter'/'TMP', 'Destroy target artifact.').
card_image_name('shatter'/'TMP', 'shatter').
card_uid('shatter'/'TMP', 'TMP:Shatter:shatter').
card_rarity('shatter'/'TMP', 'Common').
card_artist('shatter'/'TMP', 'Jason Alexander Behnke').
card_flavor_text('shatter'/'TMP', '\"Such fuss over machines, Gerrard. Remember that you are the prize of the Legacy. Throw away your toys!\"\n—Orim, Samite healer').
card_multiverse_id('shatter'/'TMP', '4847').

card_in_set('shimmering wings', 'TMP').
card_original_type('shimmering wings'/'TMP', 'Enchant Creature').
card_original_text('shimmering wings'/'TMP', 'Enchanted creature gains flying.\n{U}: Return Shimmering Wings to owner\'s hand.').
card_first_print('shimmering wings', 'TMP').
card_image_name('shimmering wings'/'TMP', 'shimmering wings').
card_uid('shimmering wings'/'TMP', 'TMP:Shimmering Wings:shimmering wings').
card_rarity('shimmering wings'/'TMP', 'Common').
card_artist('shimmering wings'/'TMP', 'Steve Luke').
card_flavor_text('shimmering wings'/'TMP', 'The brighter the wings, the deeper the impact crater.').
card_multiverse_id('shimmering wings'/'TMP', '4726').

card_in_set('shocker', 'TMP').
card_original_type('shocker'/'TMP', 'Summon — Insect').
card_original_text('shocker'/'TMP', 'If Shocker damages any player, that player discards his or her hand, then draws a new hand of as many cards as he or she had before.').
card_first_print('shocker', 'TMP').
card_image_name('shocker'/'TMP', 'shocker').
card_uid('shocker'/'TMP', 'TMP:Shocker:shocker').
card_rarity('shocker'/'TMP', 'Rare').
card_artist('shocker'/'TMP', 'Thomas M. Baxa').
card_multiverse_id('shocker'/'TMP', '4848').

card_in_set('sky spirit', 'TMP').
card_original_type('sky spirit'/'TMP', 'Summon — Spirit').
card_original_text('sky spirit'/'TMP', 'Flying, first strike').
card_first_print('sky spirit', 'TMP').
card_image_name('sky spirit'/'TMP', 'sky spirit').
card_uid('sky spirit'/'TMP', 'TMP:Sky Spirit:sky spirit').
card_rarity('sky spirit'/'TMP', 'Uncommon').
card_artist('sky spirit'/'TMP', 'Rebecca Guay').
card_flavor_text('sky spirit'/'TMP', '\"Like a strain of music: easy to remember but impossible to catch.\"\n—Mirri of the Weatherlight').
card_multiverse_id('sky spirit'/'TMP', '4917').

card_in_set('skyshroud condor', 'TMP').
card_original_type('skyshroud condor'/'TMP', 'Summon — Bird').
card_original_text('skyshroud condor'/'TMP', 'Flying\nYou cannot play Skyshroud Condor unless you have successfully cast another spell this turn.').
card_first_print('skyshroud condor', 'TMP').
card_image_name('skyshroud condor'/'TMP', 'skyshroud condor').
card_uid('skyshroud condor'/'TMP', 'TMP:Skyshroud Condor:skyshroud condor').
card_rarity('skyshroud condor'/'TMP', 'Uncommon').
card_artist('skyshroud condor'/'TMP', 'Doug Chaffee').
card_flavor_text('skyshroud condor'/'TMP', 'Condor\'s flight augurs sleepless night.\n—Skyshroud proverb').
card_multiverse_id('skyshroud condor'/'TMP', '4727').

card_in_set('skyshroud elf', 'TMP').
card_original_type('skyshroud elf'/'TMP', 'Summon — Elf').
card_original_text('skyshroud elf'/'TMP', '{T}: Add {G} to your mana pool. Play this ability as a mana source.\n{1}: Add {W} or {R} to your mana pool. Play this ability as a mana source.').
card_first_print('skyshroud elf', 'TMP').
card_image_name('skyshroud elf'/'TMP', 'skyshroud elf').
card_uid('skyshroud elf'/'TMP', 'TMP:Skyshroud Elf:skyshroud elf').
card_rarity('skyshroud elf'/'TMP', 'Common').
card_artist('skyshroud elf'/'TMP', 'Jeff Miracola').
card_flavor_text('skyshroud elf'/'TMP', '\"It is our duty to endure.\"\n—Eladamri, Lord of Leaves').
card_multiverse_id('skyshroud elf'/'TMP', '4789').

card_in_set('skyshroud forest', 'TMP').
card_original_type('skyshroud forest'/'TMP', 'Land').
card_original_text('skyshroud forest'/'TMP', 'Skyshroud Forest comes into play tapped.\n{T}: Add one colorless mana to your mana pool.\n{T}: Add {U} or {G} to your mana pool. Skyshroud Forest deals 1 damage to you.').
card_first_print('skyshroud forest', 'TMP').
card_image_name('skyshroud forest'/'TMP', 'skyshroud forest').
card_uid('skyshroud forest'/'TMP', 'TMP:Skyshroud Forest:skyshroud forest').
card_rarity('skyshroud forest'/'TMP', 'Rare').
card_artist('skyshroud forest'/'TMP', 'Roger Raupp').
card_multiverse_id('skyshroud forest'/'TMP', '4940').

card_in_set('skyshroud ranger', 'TMP').
card_original_type('skyshroud ranger'/'TMP', 'Summon — Elf').
card_original_text('skyshroud ranger'/'TMP', '{T}: Choose a land card in your hand and put it into play. Play this ability as a sorcery.').
card_first_print('skyshroud ranger', 'TMP').
card_image_name('skyshroud ranger'/'TMP', 'skyshroud ranger').
card_uid('skyshroud ranger'/'TMP', 'TMP:Skyshroud Ranger:skyshroud ranger').
card_rarity('skyshroud ranger'/'TMP', 'Common').
card_artist('skyshroud ranger'/'TMP', 'Steve Luke').
card_flavor_text('skyshroud ranger'/'TMP', '\"Just as you see the grave and remember the friend, I see this forest and remember Llanowar.\"\n—Mirri of the Weatherlight').
card_multiverse_id('skyshroud ranger'/'TMP', '4790').

card_in_set('skyshroud troll', 'TMP').
card_original_type('skyshroud troll'/'TMP', 'Summon — Giant').
card_original_text('skyshroud troll'/'TMP', '{1}{G}: Regenerate Skyshroud Troll.').
card_first_print('skyshroud troll', 'TMP').
card_image_name('skyshroud troll'/'TMP', 'skyshroud troll').
card_uid('skyshroud troll'/'TMP', 'TMP:Skyshroud Troll:skyshroud troll').
card_rarity('skyshroud troll'/'TMP', 'Common').
card_artist('skyshroud troll'/'TMP', 'Matthew D. Wilson').
card_flavor_text('skyshroud troll'/'TMP', 'The elves and merfolk have nothing but bitterness for each other. The trolls, however, find them both rather tasty.').
card_multiverse_id('skyshroud troll'/'TMP', '4791').

card_in_set('skyshroud vampire', 'TMP').
card_original_type('skyshroud vampire'/'TMP', 'Summon — Vampire').
card_original_text('skyshroud vampire'/'TMP', 'Flying\nChoose and discard a creature card: Skyshroud Vampire gets +2/+2 until end of turn.').
card_first_print('skyshroud vampire', 'TMP').
card_image_name('skyshroud vampire'/'TMP', 'skyshroud vampire').
card_uid('skyshroud vampire'/'TMP', 'TMP:Skyshroud Vampire:skyshroud vampire').
card_rarity('skyshroud vampire'/'TMP', 'Uncommon').
card_artist('skyshroud vampire'/'TMP', 'Gary Leach').
card_flavor_text('skyshroud vampire'/'TMP', '\"If it tastes one drop of elvish blood I will cast it from the shroud to see it burn.\"\n—Eladamri, Lord of Leaves').
card_multiverse_id('skyshroud vampire'/'TMP', '4686').

card_in_set('soltari crusader', 'TMP').
card_original_type('soltari crusader'/'TMP', 'Summon — Knight').
card_original_text('soltari crusader'/'TMP', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\n{1}{W}: Soltari Crusader gets +1/+0 until end of turn.').
card_first_print('soltari crusader', 'TMP').
card_image_name('soltari crusader'/'TMP', 'soltari crusader').
card_uid('soltari crusader'/'TMP', 'TMP:Soltari Crusader:soltari crusader').
card_rarity('soltari crusader'/'TMP', 'Uncommon').
card_artist('soltari crusader'/'TMP', 'Randy Gallegos').
card_flavor_text('soltari crusader'/'TMP', '\"Carry war to the Dauthi, no matter the way, no matter the world.\"\n—Soltari battle chant').
card_multiverse_id('soltari crusader'/'TMP', '4899').

card_in_set('soltari emissary', 'TMP').
card_original_type('soltari emissary'/'TMP', 'Summon — Soldier').
card_original_text('soltari emissary'/'TMP', '{W}: Soltari Emissary gains shadow until end of turn. (This creature can block or be blocked by only creatures with shadow.)').
card_first_print('soltari emissary', 'TMP').
card_image_name('soltari emissary'/'TMP', 'soltari emissary').
card_uid('soltari emissary'/'TMP', 'TMP:Soltari Emissary:soltari emissary').
card_rarity('soltari emissary'/'TMP', 'Rare').
card_artist('soltari emissary'/'TMP', 'Adam Rex').
card_flavor_text('soltari emissary'/'TMP', 'Alone at the portal, Ertai began his meditation. He realized immediately that he was not alone.').
card_multiverse_id('soltari emissary'/'TMP', '4900').

card_in_set('soltari foot soldier', 'TMP').
card_original_type('soltari foot soldier'/'TMP', 'Summon — Soldier').
card_original_text('soltari foot soldier'/'TMP', 'Shadow (This creature can block or be blocked by only creatures with shadow.)').
card_first_print('soltari foot soldier', 'TMP').
card_image_name('soltari foot soldier'/'TMP', 'soltari foot soldier').
card_uid('soltari foot soldier'/'TMP', 'TMP:Soltari Foot Soldier:soltari foot soldier').
card_rarity('soltari foot soldier'/'TMP', 'Common').
card_artist('soltari foot soldier'/'TMP', 'Janet Aulisio').
card_flavor_text('soltari foot soldier'/'TMP', '\"Children of the Ruins, raised to be warriors, know that life begins when another speaks their names.\"\n—Soltari Tales of Life').
card_multiverse_id('soltari foot soldier'/'TMP', '4901').

card_in_set('soltari guerrillas', 'TMP').
card_original_type('soltari guerrillas'/'TMP', 'Summon — Soldiers').
card_original_text('soltari guerrillas'/'TMP', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nIf Soltari Guerrillas assigns combat damage to any opponent, you may redirect that damage to target creature.').
card_first_print('soltari guerrillas', 'TMP').
card_image_name('soltari guerrillas'/'TMP', 'soltari guerrillas').
card_uid('soltari guerrillas'/'TMP', 'TMP:Soltari Guerrillas:soltari guerrillas').
card_rarity('soltari guerrillas'/'TMP', 'Rare').
card_artist('soltari guerrillas'/'TMP', 'Val Mayerik').
card_multiverse_id('soltari guerrillas'/'TMP', '4918').

card_in_set('soltari lancer', 'TMP').
card_original_type('soltari lancer'/'TMP', 'Summon — Knight').
card_original_text('soltari lancer'/'TMP', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nFirst strike when attacking').
card_first_print('soltari lancer', 'TMP').
card_image_name('soltari lancer'/'TMP', 'soltari lancer').
card_uid('soltari lancer'/'TMP', 'TMP:Soltari Lancer:soltari lancer').
card_rarity('soltari lancer'/'TMP', 'Common').
card_artist('soltari lancer'/'TMP', 'Matthew D. Wilson').
card_flavor_text('soltari lancer'/'TMP', '\"In times of war the victors rarely save their best for last.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('soltari lancer'/'TMP', '4902').

card_in_set('soltari monk', 'TMP').
card_original_type('soltari monk'/'TMP', 'Summon — Cleric').
card_original_text('soltari monk'/'TMP', 'Protection from black; shadow (This creature can block or be blocked by only creatures with shadow.)').
card_first_print('soltari monk', 'TMP').
card_image_name('soltari monk'/'TMP', 'soltari monk').
card_uid('soltari monk'/'TMP', 'TMP:Soltari Monk:soltari monk').
card_rarity('soltari monk'/'TMP', 'Uncommon').
card_artist('soltari monk'/'TMP', 'Janet Aulisio').
card_flavor_text('soltari monk'/'TMP', '\"Prayer rarely explains.\"\n—Orim, Samite healer').
card_multiverse_id('soltari monk'/'TMP', '4903').

card_in_set('soltari priest', 'TMP').
card_original_type('soltari priest'/'TMP', 'Summon — Cleric').
card_original_text('soltari priest'/'TMP', 'Protection from red; shadow (This creature can block or be blocked by only creatures with shadow.)').
card_first_print('soltari priest', 'TMP').
card_image_name('soltari priest'/'TMP', 'soltari priest').
card_uid('soltari priest'/'TMP', 'TMP:Soltari Priest:soltari priest').
card_rarity('soltari priest'/'TMP', 'Uncommon').
card_artist('soltari priest'/'TMP', 'Janet Aulisio').
card_flavor_text('soltari priest'/'TMP', '\"In Rath,\" the priest said, \"there is even greater need for prayer.\"').
card_multiverse_id('soltari priest'/'TMP', '4904').

card_in_set('soltari trooper', 'TMP').
card_original_type('soltari trooper'/'TMP', 'Summon — Soldier').
card_original_text('soltari trooper'/'TMP', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nIf Soltari Trooper attacks, it gets +1/+1 until end of turn.').
card_first_print('soltari trooper', 'TMP').
card_image_name('soltari trooper'/'TMP', 'soltari trooper').
card_uid('soltari trooper'/'TMP', 'TMP:Soltari Trooper:soltari trooper').
card_rarity('soltari trooper'/'TMP', 'Common').
card_artist('soltari trooper'/'TMP', 'Kev Walker').
card_flavor_text('soltari trooper'/'TMP', '\"Dauthi blood is Soltari wine.\"\n—Soltari Tales of Life').
card_multiverse_id('soltari trooper'/'TMP', '4905').

card_in_set('souldrinker', 'TMP').
card_original_type('souldrinker'/'TMP', 'Summon — Spirit').
card_original_text('souldrinker'/'TMP', 'Pay 3 life: Put a +1/+1 counter on Souldrinker.').
card_first_print('souldrinker', 'TMP').
card_image_name('souldrinker'/'TMP', 'souldrinker').
card_uid('souldrinker'/'TMP', 'TMP:Souldrinker:souldrinker').
card_rarity('souldrinker'/'TMP', 'Uncommon').
card_artist('souldrinker'/'TMP', 'Dermot Power').
card_flavor_text('souldrinker'/'TMP', 'Don\'t drink and thrive.').
card_multiverse_id('souldrinker'/'TMP', '4687').

card_in_set('spell blast', 'TMP').
card_original_type('spell blast'/'TMP', 'Interrupt').
card_original_text('spell blast'/'TMP', 'Counter target spell with total casting cost equal to X.').
card_image_name('spell blast'/'TMP', 'spell blast').
card_uid('spell blast'/'TMP', 'TMP:Spell Blast:spell blast').
card_rarity('spell blast'/'TMP', 'Common').
card_artist('spell blast'/'TMP', 'Steve Luke').
card_flavor_text('spell blast'/'TMP', 'Call it the thinking mage\'s version of brute force.').
card_multiverse_id('spell blast'/'TMP', '4728').

card_in_set('spike drone', 'TMP').
card_original_type('spike drone'/'TMP', 'Summon — Spike').
card_original_text('spike drone'/'TMP', 'Spike Drone comes into play with one +1/+1 counter on it.\n{2}, Remove a +1/+1 counter from Spike Drone: Put a +1/+1 counter on target creature.').
card_first_print('spike drone', 'TMP').
card_image_name('spike drone'/'TMP', 'spike drone').
card_uid('spike drone'/'TMP', 'TMP:Spike Drone:spike drone').
card_rarity('spike drone'/'TMP', 'Common').
card_artist('spike drone'/'TMP', 'Charles Gillespie').
card_multiverse_id('spike drone'/'TMP', '4792').

card_in_set('spinal graft', 'TMP').
card_original_type('spinal graft'/'TMP', 'Enchant Creature').
card_original_text('spinal graft'/'TMP', 'Enchanted creature gets +3/+3.\nIf enchanted creature is the target of a spell or ability, destroy that creature. The creature cannot be regenerated this turn.').
card_first_print('spinal graft', 'TMP').
card_image_name('spinal graft'/'TMP', 'spinal graft').
card_uid('spinal graft'/'TMP', 'TMP:Spinal Graft:spinal graft').
card_rarity('spinal graft'/'TMP', 'Common').
card_artist('spinal graft'/'TMP', 'Ron Spencer').
card_flavor_text('spinal graft'/'TMP', '\"This one has not screamed enough to show effective implantation. Kill it.\"\n—Volrath').
card_multiverse_id('spinal graft'/'TMP', '4688').

card_in_set('spirit mirror', 'TMP').
card_original_type('spirit mirror'/'TMP', 'Enchantment').
card_original_text('spirit mirror'/'TMP', 'During your upkeep, if there are no Reflection tokens in play, put a Reflection token into play. Treat this token as a 2/2 white creature.\n{0}: Destroy target Reflection.').
card_first_print('spirit mirror', 'TMP').
card_image_name('spirit mirror'/'TMP', 'spirit mirror').
card_uid('spirit mirror'/'TMP', 'TMP:Spirit Mirror:spirit mirror').
card_rarity('spirit mirror'/'TMP', 'Rare').
card_artist('spirit mirror'/'TMP', 'D. Alexander Gregory').
card_multiverse_id('spirit mirror'/'TMP', '4906').

card_in_set('spontaneous combustion', 'TMP').
card_original_type('spontaneous combustion'/'TMP', 'Instant').
card_original_text('spontaneous combustion'/'TMP', 'Sacrifice a creature: Spontaneous Combustion deals 3 damage to each creature.').
card_first_print('spontaneous combustion', 'TMP').
card_image_name('spontaneous combustion'/'TMP', 'spontaneous combustion').
card_uid('spontaneous combustion'/'TMP', 'TMP:Spontaneous Combustion:spontaneous combustion').
card_rarity('spontaneous combustion'/'TMP', 'Uncommon').
card_artist('spontaneous combustion'/'TMP', 'Doug Chaffee').
card_flavor_text('spontaneous combustion'/'TMP', '\"Heat of battle\" is usually a metaphor.').
card_multiverse_id('spontaneous combustion'/'TMP', '4919').

card_in_set('squee\'s toy', 'TMP').
card_original_type('squee\'s toy'/'TMP', 'Artifact').
card_original_text('squee\'s toy'/'TMP', '{T}: Prevent 1 damage to any creature.').
card_first_print('squee\'s toy', 'TMP').
card_image_name('squee\'s toy'/'TMP', 'squee\'s toy').
card_uid('squee\'s toy'/'TMP', 'TMP:Squee\'s Toy:squee\'s toy').
card_rarity('squee\'s toy'/'TMP', 'Common').
card_artist('squee\'s toy'/'TMP', 'Heather Hudson').
card_flavor_text('squee\'s toy'/'TMP', 'As the horrors closed in on Gerrard, Squee trembled and clutched his toy for comfort. He didn\'t know where it came from or why it was so warm, but he was glad he\'d kept it near.').
card_multiverse_id('squee\'s toy'/'TMP', '4629').

card_in_set('stalking stones', 'TMP').
card_original_type('stalking stones'/'TMP', 'Land').
card_original_text('stalking stones'/'TMP', '{T}: Add one colorless mana to your mana pool.\n{6}: Stalking Stones becomes a 3/3 artifact creature permanently. (This creature still counts as a land.)').
card_first_print('stalking stones', 'TMP').
card_image_name('stalking stones'/'TMP', 'stalking stones').
card_uid('stalking stones'/'TMP', 'TMP:Stalking Stones:stalking stones').
card_rarity('stalking stones'/'TMP', 'Uncommon').
card_artist('stalking stones'/'TMP', 'Stephen Daniele').
card_multiverse_id('stalking stones'/'TMP', '4941').

card_in_set('starke of rath', 'TMP').
card_original_type('starke of rath'/'TMP', 'Summon — Legend').
card_original_text('starke of rath'/'TMP', '{T}: Destroy target artifact or creature. That permanent\'s controller gains control of Starke of Rath permanently.').
card_first_print('starke of rath', 'TMP').
card_image_name('starke of rath'/'TMP', 'starke of rath').
card_uid('starke of rath'/'TMP', 'TMP:Starke of Rath:starke of rath').
card_rarity('starke of rath'/'TMP', 'Rare').
card_artist('starke of rath'/'TMP', 'Dan Frazier').
card_flavor_text('starke of rath'/'TMP', '\"I know to whom I owe the most loyalty, and I see him in the mirror every day.\"\n—Starke').
card_multiverse_id('starke of rath'/'TMP', '4849').

card_in_set('static orb', 'TMP').
card_original_type('static orb'/'TMP', 'Artifact').
card_original_text('static orb'/'TMP', 'Players cannot untap more than two permanents during their untap phases.').
card_first_print('static orb', 'TMP').
card_image_name('static orb'/'TMP', 'static orb').
card_uid('static orb'/'TMP', 'TMP:Static Orb:static orb').
card_rarity('static orb'/'TMP', 'Rare').
card_artist('static orb'/'TMP', 'Dermot Power').
card_flavor_text('static orb'/'TMP', 'Time passes, time crawls;\nTime doesn\'t move at all.').
card_multiverse_id('static orb'/'TMP', '4630').

card_in_set('staunch defenders', 'TMP').
card_original_type('staunch defenders'/'TMP', 'Summon — Soldiers').
card_original_text('staunch defenders'/'TMP', 'When Staunch Defenders comes into play, gain 4 life.').
card_first_print('staunch defenders', 'TMP').
card_image_name('staunch defenders'/'TMP', 'staunch defenders').
card_uid('staunch defenders'/'TMP', 'TMP:Staunch Defenders:staunch defenders').
card_rarity('staunch defenders'/'TMP', 'Uncommon').
card_artist('staunch defenders'/'TMP', 'Mark Poole').
card_flavor_text('staunch defenders'/'TMP', '\"Hold your position! Leave doubt for the dying!\"\n—Tahngarth of the Weatherlight').
card_multiverse_id('staunch defenders'/'TMP', '4907').

card_in_set('steal enchantment', 'TMP').
card_original_type('steal enchantment'/'TMP', 'Enchant Enchantment').
card_original_text('steal enchantment'/'TMP', 'Gain control of enchanted enchantment').
card_first_print('steal enchantment', 'TMP').
card_image_name('steal enchantment'/'TMP', 'steal enchantment').
card_uid('steal enchantment'/'TMP', 'TMP:Steal Enchantment:steal enchantment').
card_rarity('steal enchantment'/'TMP', 'Uncommon').
card_artist('steal enchantment'/'TMP', 'Hannibal King').
card_flavor_text('steal enchantment'/'TMP', '\"Why should I flatter another with imitation when I can satisfy myself through possession?\"\n—Ertai, wizard adept').
card_multiverse_id('steal enchantment'/'TMP', '4729').

card_in_set('stinging licid', 'TMP').
card_original_type('stinging licid'/'TMP', 'Summon — Licid').
card_original_text('stinging licid'/'TMP', '{1}{U}, {T}: Stinging Licid loses this ability and becomes a creature enchantment that reads \"Whenever enchanted creature becomes tapped, Stinging Licid deals 2 damage to that creature\'s controller\" instead of a creature. Move Stinging Licid onto target creature. You may pay {U} to end this effect.').
card_first_print('stinging licid', 'TMP').
card_image_name('stinging licid'/'TMP', 'stinging licid').
card_uid('stinging licid'/'TMP', 'TMP:Stinging Licid:stinging licid').
card_rarity('stinging licid'/'TMP', 'Uncommon').
card_artist('stinging licid'/'TMP', 'Paolo Parente').
card_multiverse_id('stinging licid'/'TMP', '4730').

card_in_set('stone rain', 'TMP').
card_original_type('stone rain'/'TMP', 'Sorcery').
card_original_text('stone rain'/'TMP', 'Destroy target land.').
card_image_name('stone rain'/'TMP', 'stone rain').
card_uid('stone rain'/'TMP', 'TMP:Stone Rain:stone rain').
card_rarity('stone rain'/'TMP', 'Common').
card_artist('stone rain'/'TMP', 'Christopher Rush').
card_flavor_text('stone rain'/'TMP', '\"The land shall beg for drink and curse the rain that follows.\"\n—Oracle en-Vec').
card_multiverse_id('stone rain'/'TMP', '4850').

card_in_set('storm front', 'TMP').
card_original_type('storm front'/'TMP', 'Enchantment').
card_original_text('storm front'/'TMP', '{G}{G}: Tap target creature with flying.').
card_first_print('storm front', 'TMP').
card_image_name('storm front'/'TMP', 'storm front').
card_uid('storm front'/'TMP', 'TMP:Storm Front:storm front').
card_rarity('storm front'/'TMP', 'Uncommon').
card_artist('storm front'/'TMP', 'William O\'Connor').
card_flavor_text('storm front'/'TMP', 'The calmest day on Rath would be thought a storm anywhere else.').
card_multiverse_id('storm front'/'TMP', '4793').

card_in_set('stun', 'TMP').
card_original_type('stun'/'TMP', 'Instant').
card_original_text('stun'/'TMP', 'Target creature cannot block this turn.\nDraw a card.').
card_first_print('stun', 'TMP').
card_image_name('stun'/'TMP', 'stun').
card_uid('stun'/'TMP', 'TMP:Stun:stun').
card_rarity('stun'/'TMP', 'Common').
card_artist('stun'/'TMP', 'Terese Nielsen').
card_flavor_text('stun'/'TMP', '\"I concede it was a cheap shot, but it was the only one I could afford.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('stun'/'TMP', '4851').

card_in_set('sudden impact', 'TMP').
card_original_type('sudden impact'/'TMP', 'Instant').
card_original_text('sudden impact'/'TMP', 'Sudden Impact deals 1 damage to target player for each card in his or her hand.').
card_first_print('sudden impact', 'TMP').
card_image_name('sudden impact'/'TMP', 'sudden impact').
card_uid('sudden impact'/'TMP', 'TMP:Sudden Impact:sudden impact').
card_rarity('sudden impact'/'TMP', 'Uncommon').
card_artist('sudden impact'/'TMP', 'Alan Pollack').
card_flavor_text('sudden impact'/'TMP', 'The Predator roared like an unruly beast, and the Weatherlight suddenly listed. \"No!\" screamed Hanna.').
card_multiverse_id('sudden impact'/'TMP', '4852').

card_in_set('swamp', 'TMP').
card_original_type('swamp'/'TMP', 'Land').
card_original_text('swamp'/'TMP', '{T}: Add {B} to your mana pool.').
card_image_name('swamp'/'TMP', 'swamp1').
card_uid('swamp'/'TMP', 'TMP:Swamp:swamp1').
card_rarity('swamp'/'TMP', 'Basic Land').
card_artist('swamp'/'TMP', 'Brom').
card_multiverse_id('swamp'/'TMP', '4923').

card_in_set('swamp', 'TMP').
card_original_type('swamp'/'TMP', 'Land').
card_original_text('swamp'/'TMP', '{T}: Add {B} to your mana pool.').
card_image_name('swamp'/'TMP', 'swamp2').
card_uid('swamp'/'TMP', 'TMP:Swamp:swamp2').
card_rarity('swamp'/'TMP', 'Basic Land').
card_artist('swamp'/'TMP', 'Brom').
card_multiverse_id('swamp'/'TMP', '4925').

card_in_set('swamp', 'TMP').
card_original_type('swamp'/'TMP', 'Land').
card_original_text('swamp'/'TMP', '{T}: Add {B} to your mana pool.').
card_image_name('swamp'/'TMP', 'swamp3').
card_uid('swamp'/'TMP', 'TMP:Swamp:swamp3').
card_rarity('swamp'/'TMP', 'Basic Land').
card_artist('swamp'/'TMP', 'Brom').
card_multiverse_id('swamp'/'TMP', '4922').

card_in_set('swamp', 'TMP').
card_original_type('swamp'/'TMP', 'Land').
card_original_text('swamp'/'TMP', '{T}: Add {B} to your mana pool.').
card_image_name('swamp'/'TMP', 'swamp4').
card_uid('swamp'/'TMP', 'TMP:Swamp:swamp4').
card_rarity('swamp'/'TMP', 'Basic Land').
card_artist('swamp'/'TMP', 'Brom').
card_multiverse_id('swamp'/'TMP', '4924').

card_in_set('tahngarth\'s rage', 'TMP').
card_original_type('tahngarth\'s rage'/'TMP', 'Enchant Creature').
card_original_text('tahngarth\'s rage'/'TMP', 'If enchanted creature is attacking, it gets +3/+0. Otherwise, it gets -2/-1.').
card_first_print('tahngarth\'s rage', 'TMP').
card_image_name('tahngarth\'s rage'/'TMP', 'tahngarth\'s rage').
card_uid('tahngarth\'s rage'/'TMP', 'TMP:Tahngarth\'s Rage:tahngarth\'s rage').
card_rarity('tahngarth\'s rage'/'TMP', 'Uncommon').
card_artist('tahngarth\'s rage'/'TMP', 'Hannibal King').
card_flavor_text('tahngarth\'s rage'/'TMP', '\"Taste my horns!\"\n—Tahngarth of the Weatherlight').
card_multiverse_id('tahngarth\'s rage'/'TMP', '4853').

card_in_set('talon sliver', 'TMP').
card_original_type('talon sliver'/'TMP', 'Summon — Sliver').
card_original_text('talon sliver'/'TMP', 'All Slivers gain first strike.').
card_first_print('talon sliver', 'TMP').
card_image_name('talon sliver'/'TMP', 'talon sliver').
card_uid('talon sliver'/'TMP', 'TMP:Talon Sliver:talon sliver').
card_rarity('talon sliver'/'TMP', 'Common').
card_artist('talon sliver'/'TMP', 'Mike Raabe').
card_flavor_text('talon sliver'/'TMP', '\"Keep them at sword\'s length!\" Gerrard\'s order fell flat as each sliver\'s talon suddenly grew longer. \"Hold on—break out the polearms!\"').
card_multiverse_id('talon sliver'/'TMP', '4908').

card_in_set('telethopter', 'TMP').
card_original_type('telethopter'/'TMP', 'Artifact Creature').
card_original_text('telethopter'/'TMP', 'Tap a creature you control: Telethopter gains flying until end of turn.').
card_first_print('telethopter', 'TMP').
card_image_name('telethopter'/'TMP', 'telethopter').
card_uid('telethopter'/'TMP', 'TMP:Telethopter:telethopter').
card_rarity('telethopter'/'TMP', 'Uncommon').
card_artist('telethopter'/'TMP', 'Thomas M. Baxa').
card_flavor_text('telethopter'/'TMP', 'After losing several of the devices to midair collisions, Greven forbade moggs from operating telethopters.').
card_multiverse_id('telethopter'/'TMP', '4631').

card_in_set('thalakos dreamsower', 'TMP').
card_original_type('thalakos dreamsower'/'TMP', 'Summon — Wizard').
card_original_text('thalakos dreamsower'/'TMP', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nYou may choose not to untap Thalakos Dreamsower during your untap phase.\nIf Thalakos Dreamsower damages any opponent, tap target creature. As long as Thalakos Dreamsower remains tapped, that creature does not untap during its controller\'s untap phase.').
card_first_print('thalakos dreamsower', 'TMP').
card_image_name('thalakos dreamsower'/'TMP', 'thalakos dreamsower').
card_uid('thalakos dreamsower'/'TMP', 'TMP:Thalakos Dreamsower:thalakos dreamsower').
card_rarity('thalakos dreamsower'/'TMP', 'Uncommon').
card_artist('thalakos dreamsower'/'TMP', 'Susan Van Camp').
card_multiverse_id('thalakos dreamsower'/'TMP', '4732').

card_in_set('thalakos lowlands', 'TMP').
card_original_type('thalakos lowlands'/'TMP', 'Land').
card_original_text('thalakos lowlands'/'TMP', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {W} or {U} to your mana pool. Thalakos Lowlands does not untap during your next untap phase.').
card_first_print('thalakos lowlands', 'TMP').
card_image_name('thalakos lowlands'/'TMP', 'thalakos lowlands').
card_uid('thalakos lowlands'/'TMP', 'TMP:Thalakos Lowlands:thalakos lowlands').
card_rarity('thalakos lowlands'/'TMP', 'Uncommon').
card_artist('thalakos lowlands'/'TMP', 'Jeff A. Menges').
card_multiverse_id('thalakos lowlands'/'TMP', '4942').

card_in_set('thalakos mistfolk', 'TMP').
card_original_type('thalakos mistfolk'/'TMP', 'Summon — Illusion').
card_original_text('thalakos mistfolk'/'TMP', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\n{U}: Put Thalakos Mistfolk on top of owner\'s library.').
card_first_print('thalakos mistfolk', 'TMP').
card_image_name('thalakos mistfolk'/'TMP', 'thalakos mistfolk').
card_uid('thalakos mistfolk'/'TMP', 'TMP:Thalakos Mistfolk:thalakos mistfolk').
card_rarity('thalakos mistfolk'/'TMP', 'Common').
card_artist('thalakos mistfolk'/'TMP', 'Richard Kane Ferguson').
card_flavor_text('thalakos mistfolk'/'TMP', '\"They are ‘between\' in every way.\"\n—Lyna, Soltari emissary').
card_multiverse_id('thalakos mistfolk'/'TMP', '4733').

card_in_set('thalakos seer', 'TMP').
card_original_type('thalakos seer'/'TMP', 'Summon — Wizard').
card_original_text('thalakos seer'/'TMP', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nIf Thalakos Seer leaves play, draw a card.').
card_first_print('thalakos seer', 'TMP').
card_image_name('thalakos seer'/'TMP', 'thalakos seer').
card_uid('thalakos seer'/'TMP', 'TMP:Thalakos Seer:thalakos seer').
card_rarity('thalakos seer'/'TMP', 'Common').
card_artist('thalakos seer'/'TMP', 'Ron Spencer').
card_flavor_text('thalakos seer'/'TMP', '\"You see our world when you shut your eyes so tightly that tiny shapes float before them.\"\n—Lyna, to Ertai').
card_multiverse_id('thalakos seer'/'TMP', '4734').

card_in_set('thalakos sentry', 'TMP').
card_original_type('thalakos sentry'/'TMP', 'Summon — Soldier').
card_original_text('thalakos sentry'/'TMP', 'Shadow (This creature can block or be blocked by only creatures with shadow)').
card_first_print('thalakos sentry', 'TMP').
card_image_name('thalakos sentry'/'TMP', 'thalakos sentry').
card_uid('thalakos sentry'/'TMP', 'TMP:Thalakos Sentry:thalakos sentry').
card_rarity('thalakos sentry'/'TMP', 'Common').
card_artist('thalakos sentry'/'TMP', 'Andrew Robinson').
card_flavor_text('thalakos sentry'/'TMP', '\"Ill luck and poor geography caught the Thalakos between us and the Dauthi.\"\n—Lyna, Soltari emissary').
card_multiverse_id('thalakos sentry'/'TMP', '4735').

card_in_set('thumbscrews', 'TMP').
card_original_type('thumbscrews'/'TMP', 'Artifact').
card_original_text('thumbscrews'/'TMP', 'During your upkeep, if you have five or more cards in your hand, Thumbscrews deals 1 damage to target opponent.').
card_first_print('thumbscrews', 'TMP').
card_image_name('thumbscrews'/'TMP', 'thumbscrews').
card_uid('thumbscrews'/'TMP', 'TMP:Thumbscrews:thumbscrews').
card_rarity('thumbscrews'/'TMP', 'Rare').
card_artist('thumbscrews'/'TMP', 'Charles Gillespie').
card_flavor_text('thumbscrews'/'TMP', '\"We will start small, I think.\"\n—Volrath').
card_multiverse_id('thumbscrews'/'TMP', '4632').

card_in_set('time ebb', 'TMP').
card_original_type('time ebb'/'TMP', 'Sorcery').
card_original_text('time ebb'/'TMP', 'Put target creature on top of owner\'s library.').
card_image_name('time ebb'/'TMP', 'time ebb').
card_uid('time ebb'/'TMP', 'TMP:Time Ebb:time ebb').
card_rarity('time ebb'/'TMP', 'Common').
card_artist('time ebb'/'TMP', 'Thomas M. Baxa').
card_flavor_text('time ebb'/'TMP', '\"Gone today, here tomorrow.\"\n—Ertai, wizard adept').
card_multiverse_id('time ebb'/'TMP', '4736').

card_in_set('time warp', 'TMP').
card_original_type('time warp'/'TMP', 'Sorcery').
card_original_text('time warp'/'TMP', 'Target player takes an extra turn after this one.').
card_first_print('time warp', 'TMP').
card_image_name('time warp'/'TMP', 'time warp').
card_uid('time warp'/'TMP', 'TMP:Time Warp:time warp').
card_rarity('time warp'/'TMP', 'Rare').
card_artist('time warp'/'TMP', 'Pete Venters').
card_flavor_text('time warp'/'TMP', '\"Let\'s do it again!\"\n—Squee, goblin cabin hand').
card_multiverse_id('time warp'/'TMP', '4737').

card_in_set('tooth and claw', 'TMP').
card_original_type('tooth and claw'/'TMP', 'Enchantment').
card_original_text('tooth and claw'/'TMP', 'Sacrifice two creatures: Put a Carnivore token into play. Treat this token as a 3/1 red creature.').
card_first_print('tooth and claw', 'TMP').
card_image_name('tooth and claw'/'TMP', 'tooth and claw').
card_uid('tooth and claw'/'TMP', 'TMP:Tooth and Claw:tooth and claw').
card_rarity('tooth and claw'/'TMP', 'Rare').
card_artist('tooth and claw'/'TMP', 'Val Mayerik').
card_flavor_text('tooth and claw'/'TMP', '\"You deify nature as female—kind and gentle—yet you cringe at what is natural.\"\n—Mirri of the Weatherlight').
card_multiverse_id('tooth and claw'/'TMP', '4854').

card_in_set('torture chamber', 'TMP').
card_original_type('torture chamber'/'TMP', 'Artifact').
card_original_text('torture chamber'/'TMP', 'During your upkeep, put a pain counter on Torture Chamber.\nAt the end of your turn, Torture Chamber deals 1 damage to you for each pain counter on it.\n{1}, {T}, Remove all pain counters from Torture Chamber: Torture Chamber deals 1 damage for each pain counter on it to target creature.').
card_first_print('torture chamber', 'TMP').
card_image_name('torture chamber'/'TMP', 'torture chamber').
card_uid('torture chamber'/'TMP', 'TMP:Torture Chamber:torture chamber').
card_rarity('torture chamber'/'TMP', 'Rare').
card_artist('torture chamber'/'TMP', 'Thomas Gianni').
card_multiverse_id('torture chamber'/'TMP', '4633').

card_in_set('tradewind rider', 'TMP').
card_original_type('tradewind rider'/'TMP', 'Summon — Spirit').
card_original_text('tradewind rider'/'TMP', 'Flying\n{T}, Tap two creatures you control: Return target permanent to owner\'s hand.').
card_first_print('tradewind rider', 'TMP').
card_image_name('tradewind rider'/'TMP', 'tradewind rider').
card_uid('tradewind rider'/'TMP', 'TMP:Tradewind Rider:tradewind rider').
card_rarity('tradewind rider'/'TMP', 'Rare').
card_artist('tradewind rider'/'TMP', 'John Matson').
card_flavor_text('tradewind rider'/'TMP', 'It is said that the wind will blow the world past if you wait long enough.').
card_multiverse_id('tradewind rider'/'TMP', '4738').

card_in_set('trained armodon', 'TMP').
card_original_type('trained armodon'/'TMP', 'Summon — Elephant').
card_original_text('trained armodon'/'TMP', '').
card_first_print('trained armodon', 'TMP').
card_image_name('trained armodon'/'TMP', 'trained armodon').
card_uid('trained armodon'/'TMP', 'TMP:Trained Armodon:trained armodon').
card_rarity('trained armodon'/'TMP', 'Common').
card_artist('trained armodon'/'TMP', 'Gary Leach').
card_flavor_text('trained armodon'/'TMP', 'These are its last days. Better to grow broad and heavy. Better that the enemy is crushed beneath its carcass.').
card_multiverse_id('trained armodon'/'TMP', '4794').

card_in_set('tranquility', 'TMP').
card_original_type('tranquility'/'TMP', 'Sorcery').
card_original_text('tranquility'/'TMP', 'Destroy all enchantments.').
card_image_name('tranquility'/'TMP', 'tranquility').
card_uid('tranquility'/'TMP', 'TMP:Tranquility:tranquility').
card_rarity('tranquility'/'TMP', 'Common').
card_artist('tranquility'/'TMP', 'Margaret Organ-Kean').
card_flavor_text('tranquility'/'TMP', '\"Peace will come, but whether born of harmony or entropy I cannot say.\"\n—Oracle en-Vec').
card_multiverse_id('tranquility'/'TMP', '4795').

card_in_set('trumpeting armodon', 'TMP').
card_original_type('trumpeting armodon'/'TMP', 'Summon — Elephant').
card_original_text('trumpeting armodon'/'TMP', '{1}{G}: Target creature blocks Trumpeting Armodon this turn if able.').
card_first_print('trumpeting armodon', 'TMP').
card_image_name('trumpeting armodon'/'TMP', 'trumpeting armodon').
card_uid('trumpeting armodon'/'TMP', 'TMP:Trumpeting Armodon:trumpeting armodon').
card_rarity('trumpeting armodon'/'TMP', 'Uncommon').
card_artist('trumpeting armodon'/'TMP', 'Gary Leach').
card_flavor_text('trumpeting armodon'/'TMP', 'These are its last days. But it will not go quietly.').
card_multiverse_id('trumpeting armodon'/'TMP', '4796').

card_in_set('twitch', 'TMP').
card_original_type('twitch'/'TMP', 'Instant').
card_original_text('twitch'/'TMP', 'Tap or untap target artifact, creature, or land.\nDraw a card.').
card_first_print('twitch', 'TMP').
card_image_name('twitch'/'TMP', 'twitch').
card_uid('twitch'/'TMP', 'TMP:Twitch:twitch').
card_rarity('twitch'/'TMP', 'Common').
card_artist('twitch'/'TMP', 'DiTerlizzi').
card_flavor_text('twitch'/'TMP', 'Battles are won in nuance.').
card_multiverse_id('twitch'/'TMP', '4739').

card_in_set('unstable shapeshifter', 'TMP').
card_original_type('unstable shapeshifter'/'TMP', 'Summon — Shapeshifter').
card_original_text('unstable shapeshifter'/'TMP', 'Whenever any creature comes into play, Unstable Shapeshifter permanently becomes a copy of that creature and retains this ability.').
card_first_print('unstable shapeshifter', 'TMP').
card_image_name('unstable shapeshifter'/'TMP', 'unstable shapeshifter').
card_uid('unstable shapeshifter'/'TMP', 'TMP:Unstable Shapeshifter:unstable shapeshifter').
card_rarity('unstable shapeshifter'/'TMP', 'Rare').
card_artist('unstable shapeshifter'/'TMP', 'Terese Nielsen').
card_flavor_text('unstable shapeshifter'/'TMP', '\"In a world of untruths, the living lie rules.\"\n—Oracle en-Vec').
card_multiverse_id('unstable shapeshifter'/'TMP', '4740').

card_in_set('vec townships', 'TMP').
card_original_type('vec townships'/'TMP', 'Land').
card_original_text('vec townships'/'TMP', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {G} or {W} to your mana pool. Vec Townships does not untap during your next untap phase.').
card_first_print('vec townships', 'TMP').
card_image_name('vec townships'/'TMP', 'vec townships').
card_uid('vec townships'/'TMP', 'TMP:Vec Townships:vec townships').
card_rarity('vec townships'/'TMP', 'Uncommon').
card_artist('vec townships'/'TMP', 'Eric David Anderson').
card_multiverse_id('vec townships'/'TMP', '4943').

card_in_set('verdant force', 'TMP').
card_original_type('verdant force'/'TMP', 'Summon — Elemental').
card_original_text('verdant force'/'TMP', 'During each player\'s upkeep, put a Saproling token into play. Treat this token as a 1/1 green creature.').
card_first_print('verdant force', 'TMP').
card_image_name('verdant force'/'TMP', 'verdant force').
card_uid('verdant force'/'TMP', 'TMP:Verdant Force:verdant force').
card_rarity('verdant force'/'TMP', 'Rare').
card_artist('verdant force'/'TMP', 'DiTerlizzi').
card_flavor_text('verdant force'/'TMP', 'Burl, scurf, and bower\nBirth fern and flower.').
card_multiverse_id('verdant force'/'TMP', '4797').

card_in_set('verdigris', 'TMP').
card_original_type('verdigris'/'TMP', 'Instant').
card_original_text('verdigris'/'TMP', 'Destroy target artifact.').
card_first_print('verdigris', 'TMP').
card_image_name('verdigris'/'TMP', 'verdigris').
card_uid('verdigris'/'TMP', 'TMP:Verdigris:verdigris').
card_rarity('verdigris'/'TMP', 'Uncommon').
card_artist('verdigris'/'TMP', 'Zina Saunders').
card_flavor_text('verdigris'/'TMP', '\"Only the most sophisticated inventions can survive nature\'s unsophisticated motivations.\"\n—Hanna, Weatherlight navigator').
card_multiverse_id('verdigris'/'TMP', '4798').

card_in_set('vhati il-dal', 'TMP').
card_original_type('vhati il-dal'/'TMP', 'Summon — Legend').
card_original_text('vhati il-dal'/'TMP', '{T}: Target creature\'s power or toughness is 1 until end of turn.').
card_first_print('vhati il-dal', 'TMP').
card_image_name('vhati il-dal'/'TMP', 'vhati il-dal').
card_uid('vhati il-dal'/'TMP', 'TMP:Vhati il-Dal:vhati il-dal').
card_rarity('vhati il-dal'/'TMP', 'Rare').
card_artist('vhati il-dal'/'TMP', 'Ron Spencer').
card_flavor_text('vhati il-dal'/'TMP', '\"Sir, I just thought . . . ,\" explained Vhati.\n\"Don\'t think,\" interrupted Greven. \"It doesn\'t suit you.\"').
card_multiverse_id('vhati il-dal'/'TMP', '4920').

card_in_set('volrath\'s curse', 'TMP').
card_original_type('volrath\'s curse'/'TMP', 'Enchant Creature').
card_original_text('volrath\'s curse'/'TMP', 'Enchanted creature cannot attack, block, or play any ability requiring an activation cost. That creature\'s controller may sacrifice a permanent to ignore this ability until end of turn.\n{1}{U}: Return Volrath\'s Curse to owner\'s hand.').
card_first_print('volrath\'s curse', 'TMP').
card_image_name('volrath\'s curse'/'TMP', 'volrath\'s curse').
card_uid('volrath\'s curse'/'TMP', 'TMP:Volrath\'s Curse:volrath\'s curse').
card_rarity('volrath\'s curse'/'TMP', 'Common').
card_artist('volrath\'s curse'/'TMP', 'Daren Bader').
card_multiverse_id('volrath\'s curse'/'TMP', '4741').

card_in_set('wall of diffusion', 'TMP').
card_original_type('wall of diffusion'/'TMP', 'Summon — Wall').
card_original_text('wall of diffusion'/'TMP', '(Walls cannot attack.)\nWall of Diffusion can block creatures with shadow.').
card_first_print('wall of diffusion', 'TMP').
card_image_name('wall of diffusion'/'TMP', 'wall of diffusion').
card_uid('wall of diffusion'/'TMP', 'TMP:Wall of Diffusion:wall of diffusion').
card_rarity('wall of diffusion'/'TMP', 'Common').
card_artist('wall of diffusion'/'TMP', 'DiTerlizzi').
card_flavor_text('wall of diffusion'/'TMP', '\"The injury was being caught between this world and our own; the insult was to find walls within.\"\n—Lyna, Soltari emissary').
card_multiverse_id('wall of diffusion'/'TMP', '4855').

card_in_set('warmth', 'TMP').
card_original_type('warmth'/'TMP', 'Enchantment').
card_original_text('warmth'/'TMP', 'Whenever target opponent successfully casts a red spell, gain 2 life.').
card_first_print('warmth', 'TMP').
card_image_name('warmth'/'TMP', 'warmth').
card_uid('warmth'/'TMP', 'TMP:Warmth:warmth').
card_rarity('warmth'/'TMP', 'Uncommon').
card_artist('warmth'/'TMP', 'Drew Tucker').
card_flavor_text('warmth'/'TMP', '\"Flame grows gentle with but a little distance.\"\n—Orim, Samite healer').
card_multiverse_id('warmth'/'TMP', '4909').

card_in_set('wasteland', 'TMP').
card_original_type('wasteland'/'TMP', 'Land').
card_original_text('wasteland'/'TMP', '{T}: Add one colorless mana to your mana pool.\n{T}, Sacrifice Wasteland: Destroy target nonbasic land.').
card_first_print('wasteland', 'TMP').
card_image_name('wasteland'/'TMP', 'wasteland').
card_uid('wasteland'/'TMP', 'TMP:Wasteland:wasteland').
card_rarity('wasteland'/'TMP', 'Uncommon').
card_artist('wasteland'/'TMP', 'Una Fricker').
card_flavor_text('wasteland'/'TMP', '\"The land promises nothing and keeps its promise.\"\n—Oracle en-Vec').
card_multiverse_id('wasteland'/'TMP', '4944').

card_in_set('watchdog', 'TMP').
card_original_type('watchdog'/'TMP', 'Artifact Creature').
card_original_text('watchdog'/'TMP', 'Watchdog blocks if able.\nAs long as Watchdog is untapped, all creatures attacking you get -1/-0.').
card_first_print('watchdog', 'TMP').
card_image_name('watchdog'/'TMP', 'watchdog').
card_uid('watchdog'/'TMP', 'TMP:Watchdog:watchdog').
card_rarity('watchdog'/'TMP', 'Uncommon').
card_artist('watchdog'/'TMP', 'Richard Kane Ferguson').
card_flavor_text('watchdog'/'TMP', 'Its growl is a long, rumbling grind of gears and clockwork, its bark a blast of hissing steam.').
card_multiverse_id('watchdog'/'TMP', '4634').

card_in_set('whim of volrath', 'TMP').
card_original_type('whim of volrath'/'TMP', 'Instant').
card_original_text('whim of volrath'/'TMP', 'Buyback {2} (You may pay an additional {2} when you play this spell. If you do, put it into your hand instead of your graveyard as part of the spell\'s effect.)\nChange the text of target permanent by replacing all instances of one color word or basic land type with another until end of turn.').
card_first_print('whim of volrath', 'TMP').
card_image_name('whim of volrath'/'TMP', 'whim of volrath').
card_uid('whim of volrath'/'TMP', 'TMP:Whim of Volrath:whim of volrath').
card_rarity('whim of volrath'/'TMP', 'Rare').
card_artist('whim of volrath'/'TMP', 'Anthony S. Waters').
card_multiverse_id('whim of volrath'/'TMP', '4742').

card_in_set('whispers of the muse', 'TMP').
card_original_type('whispers of the muse'/'TMP', 'Instant').
card_original_text('whispers of the muse'/'TMP', 'Buyback {5} (You may pay an additional {5} when you play this spell. If you do, put it into your hand instead of your graveyard as part of the spell\'s effect.) \nDraw a card.').
card_first_print('whispers of the muse', 'TMP').
card_image_name('whispers of the muse'/'TMP', 'whispers of the muse').
card_uid('whispers of the muse'/'TMP', 'TMP:Whispers of the Muse:whispers of the muse').
card_rarity('whispers of the muse'/'TMP', 'Uncommon').
card_artist('whispers of the muse'/'TMP', 'Quinton Hoover').
card_flavor_text('whispers of the muse'/'TMP', '\"I followed her song only to find it was a dirge.\"\n—Crovax').
card_multiverse_id('whispers of the muse'/'TMP', '4743').

card_in_set('wild wurm', 'TMP').
card_original_type('wild wurm'/'TMP', 'Summon — Wurm').
card_original_text('wild wurm'/'TMP', 'When Wild Wurm comes into play, flip a coin. If you lose the flip, return Wild Wurm to owner\'s hand.').
card_first_print('wild wurm', 'TMP').
card_image_name('wild wurm'/'TMP', 'wild wurm').
card_uid('wild wurm'/'TMP', 'TMP:Wild Wurm:wild wurm').
card_rarity('wild wurm'/'TMP', 'Uncommon').
card_artist('wild wurm'/'TMP', 'Randy Elliott').
card_flavor_text('wild wurm'/'TMP', 'Wurm heads appear\nWild wurm near\nWurm tails you see\nWild wurm flee').
card_multiverse_id('wild wurm'/'TMP', '4856').

card_in_set('wind dancer', 'TMP').
card_original_type('wind dancer'/'TMP', 'Summon — Faerie').
card_original_text('wind dancer'/'TMP', 'Flying\n{T}: Target creature gains flying until end of turn.').
card_first_print('wind dancer', 'TMP').
card_image_name('wind dancer'/'TMP', 'wind dancer').
card_uid('wind dancer'/'TMP', 'TMP:Wind Dancer:wind dancer').
card_rarity('wind dancer'/'TMP', 'Uncommon').
card_artist('wind dancer'/'TMP', 'Susan Van Camp').
card_flavor_text('wind dancer'/'TMP', '\"Flying like a bird does not make you as free as one.\"\n—Volrath').
card_multiverse_id('wind dancer'/'TMP', '4744').

card_in_set('wind drake', 'TMP').
card_original_type('wind drake'/'TMP', 'Summon — Drake').
card_original_text('wind drake'/'TMP', 'Flying').
card_image_name('wind drake'/'TMP', 'wind drake').
card_uid('wind drake'/'TMP', 'TMP:Wind Drake:wind drake').
card_rarity('wind drake'/'TMP', 'Common').
card_artist('wind drake'/'TMP', 'Greg Simanson').
card_flavor_text('wind drake'/'TMP', 'Orim regarded Gerrard coolly. \"You are like the drake,\" she said, her expression unchanging. \"It works hard to maintain its position, but to us it seems to glide along effortlessly. You must do the same.\"').
card_multiverse_id('wind drake'/'TMP', '4745').

card_in_set('winds of rath', 'TMP').
card_original_type('winds of rath'/'TMP', 'Sorcery').
card_original_text('winds of rath'/'TMP', 'Destroy all creatures with no enchantments on them. Those creatures cannot be regenerated this turn.').
card_first_print('winds of rath', 'TMP').
card_image_name('winds of rath'/'TMP', 'winds of rath').
card_uid('winds of rath'/'TMP', 'TMP:Winds of Rath:winds of rath').
card_rarity('winds of rath'/'TMP', 'Rare').
card_artist('winds of rath'/'TMP', 'Drew Tucker').
card_flavor_text('winds of rath'/'TMP', '\"There shall be a vast shout and then a vaster silence.\"\n—Oracle en-Vec').
card_multiverse_id('winds of rath'/'TMP', '4910').

card_in_set('winged sliver', 'TMP').
card_original_type('winged sliver'/'TMP', 'Summon — Sliver').
card_original_text('winged sliver'/'TMP', 'All Slivers gain flying.').
card_first_print('winged sliver', 'TMP').
card_image_name('winged sliver'/'TMP', 'winged sliver').
card_uid('winged sliver'/'TMP', 'TMP:Winged Sliver:winged sliver').
card_rarity('winged sliver'/'TMP', 'Common').
card_artist('winged sliver'/'TMP', 'Anthony S. Waters').
card_flavor_text('winged sliver'/'TMP', '\"Everything around here has cut a deal with gravity.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('winged sliver'/'TMP', '4746').

card_in_set('winter\'s grasp', 'TMP').
card_original_type('winter\'s grasp'/'TMP', 'Sorcery').
card_original_text('winter\'s grasp'/'TMP', 'Destroy target land.').
card_image_name('winter\'s grasp'/'TMP', 'winter\'s grasp').
card_uid('winter\'s grasp'/'TMP', 'TMP:Winter\'s Grasp:winter\'s grasp').
card_rarity('winter\'s grasp'/'TMP', 'Uncommon').
card_artist('winter\'s grasp'/'TMP', 'Tom Wänerstrand').
card_flavor_text('winter\'s grasp'/'TMP', 'Summer is buried there, centuries deep;\nA crypt of years where seasons sleep.').
card_multiverse_id('winter\'s grasp'/'TMP', '4799').

card_in_set('wood sage', 'TMP').
card_original_type('wood sage'/'TMP', 'Summon — Druid').
card_original_text('wood sage'/'TMP', '{T}: Name a creature card. Reveal the top four cards of your library to all players. If any of those cards are the named card, put them into your hand. Put the rest into your graveyard.').
card_first_print('wood sage', 'TMP').
card_image_name('wood sage'/'TMP', 'wood sage').
card_uid('wood sage'/'TMP', 'TMP:Wood Sage:wood sage').
card_rarity('wood sage'/'TMP', 'Rare').
card_artist('wood sage'/'TMP', 'Paolo Parente').
card_multiverse_id('wood sage'/'TMP', '4921').

card_in_set('worthy cause', 'TMP').
card_original_type('worthy cause'/'TMP', 'Instant').
card_original_text('worthy cause'/'TMP', 'Buyback {2} (You may pay an additional {2} when you play this spell. If you do, put it into your hand instead of your graveyard as part of the spell\'s effect.)\nSacrifice a creature: Gain life equal to the sacrificed creature\'s toughness.').
card_first_print('worthy cause', 'TMP').
card_image_name('worthy cause'/'TMP', 'worthy cause').
card_uid('worthy cause'/'TMP', 'TMP:Worthy Cause:worthy cause').
card_rarity('worthy cause'/'TMP', 'Uncommon').
card_artist('worthy cause'/'TMP', 'John Matson').
card_multiverse_id('worthy cause'/'TMP', '4911').
