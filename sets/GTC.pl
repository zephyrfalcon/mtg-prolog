% Gatecrash

set('GTC').
set_name('GTC', 'Gatecrash').
set_release_date('GTC', '2013-02-01').
set_border('GTC', 'black').
set_type('GTC', 'expansion').
set_block('GTC', 'Return to Ravnica').

card_in_set('act of treason', 'GTC').
card_original_type('act of treason'/'GTC', 'Sorcery').
card_original_text('act of treason'/'GTC', 'Gain control of target creature until end of turn. Untap that creature. It gains haste until end of turn.').
card_image_name('act of treason'/'GTC', 'act of treason').
card_uid('act of treason'/'GTC', 'GTC:Act of Treason:act of treason').
card_rarity('act of treason'/'GTC', 'Common').
card_artist('act of treason'/'GTC', 'Matt Stewart').
card_number('act of treason'/'GTC', '85').
card_flavor_text('act of treason'/'GTC', 'Each day uncertainty and rumors spread, and each night attendance at the Juri Revue increases.').
card_multiverse_id('act of treason'/'GTC', '366305').

card_in_set('adaptive snapjaw', 'GTC').
card_original_type('adaptive snapjaw'/'GTC', 'Creature — Lizard Beast').
card_original_text('adaptive snapjaw'/'GTC', 'Evolve (Whenever a creature enters the battlefield under your control, if that creature has greater power or toughness than this creature, put a +1/+1 counter on this creature.)').
card_first_print('adaptive snapjaw', 'GTC').
card_image_name('adaptive snapjaw'/'GTC', 'adaptive snapjaw').
card_uid('adaptive snapjaw'/'GTC', 'GTC:Adaptive Snapjaw:adaptive snapjaw').
card_rarity('adaptive snapjaw'/'GTC', 'Common').
card_artist('adaptive snapjaw'/'GTC', 'Tomasz Jedruszek').
card_number('adaptive snapjaw'/'GTC', '113').
card_flavor_text('adaptive snapjaw'/'GTC', '\"Eh, needs more toad.\"\n—Gulistan, Simic biomancer').
card_multiverse_id('adaptive snapjaw'/'GTC', '366290').
card_watermark('adaptive snapjaw'/'GTC', 'Simic').

card_in_set('aerial maneuver', 'GTC').
card_original_type('aerial maneuver'/'GTC', 'Instant').
card_original_text('aerial maneuver'/'GTC', 'Target creature gets +1/+1 and gains flying and first strike until end of turn.').
card_first_print('aerial maneuver', 'GTC').
card_image_name('aerial maneuver'/'GTC', 'aerial maneuver').
card_uid('aerial maneuver'/'GTC', 'GTC:Aerial Maneuver:aerial maneuver').
card_rarity('aerial maneuver'/'GTC', 'Common').
card_artist('aerial maneuver'/'GTC', 'Scott Chou').
card_number('aerial maneuver'/'GTC', '1').
card_flavor_text('aerial maneuver'/'GTC', '\"When you put all of yourself into an attack, without fear or hesitation, in that instant you may attain greatness.\"\n—Predak, Gateless freemage').
card_multiverse_id('aerial maneuver'/'GTC', '366294').

card_in_set('ætherize', 'GTC').
card_original_type('ætherize'/'GTC', 'Instant').
card_original_text('ætherize'/'GTC', 'Return all attacking creatures to their owner\'s hand.').
card_first_print('ætherize', 'GTC').
card_image_name('ætherize'/'GTC', 'aetherize').
card_uid('ætherize'/'GTC', 'GTC:Ætherize:aetherize').
card_rarity('ætherize'/'GTC', 'Uncommon').
card_artist('ætherize'/'GTC', 'Ryan Barger').
card_number('ætherize'/'GTC', '29').
card_flavor_text('ætherize'/'GTC', '\"You can come back once you\'ve learned some manners—and figured out how to reconstitute your physical forms.\"').
card_multiverse_id('ætherize'/'GTC', '366273').

card_in_set('agoraphobia', 'GTC').
card_original_type('agoraphobia'/'GTC', 'Enchantment — Aura').
card_original_text('agoraphobia'/'GTC', 'Enchant creature\nEnchanted creature gets -5/-0.\n{2}{U}: Return Agoraphobia to its owner\'s hand.').
card_first_print('agoraphobia', 'GTC').
card_image_name('agoraphobia'/'GTC', 'agoraphobia').
card_uid('agoraphobia'/'GTC', 'GTC:Agoraphobia:agoraphobia').
card_rarity('agoraphobia'/'GTC', 'Uncommon').
card_artist('agoraphobia'/'GTC', 'Jim Murray').
card_number('agoraphobia'/'GTC', '30').
card_flavor_text('agoraphobia'/'GTC', '\"Everyone in this city is choosing sides. I choose inside.\"').
card_multiverse_id('agoraphobia'/'GTC', '366258').

card_in_set('alms beast', 'GTC').
card_original_type('alms beast'/'GTC', 'Creature — Beast').
card_original_text('alms beast'/'GTC', 'Creatures blocking or blocked by Alms Beast have lifelink.').
card_first_print('alms beast', 'GTC').
card_image_name('alms beast'/'GTC', 'alms beast').
card_uid('alms beast'/'GTC', 'GTC:Alms Beast:alms beast').
card_rarity('alms beast'/'GTC', 'Rare').
card_artist('alms beast'/'GTC', 'Dan Scott').
card_number('alms beast'/'GTC', '141').
card_flavor_text('alms beast'/'GTC', 'Alms-coins are only redeemable at Orzhov businesses.').
card_multiverse_id('alms beast'/'GTC', '366402').
card_watermark('alms beast'/'GTC', 'Orzhov').

card_in_set('alpha authority', 'GTC').
card_original_type('alpha authority'/'GTC', 'Enchantment — Aura').
card_original_text('alpha authority'/'GTC', 'Enchant creature\nEnchanted creature has hexproof and can\'t be blocked by more than one creature.').
card_first_print('alpha authority', 'GTC').
card_image_name('alpha authority'/'GTC', 'alpha authority').
card_uid('alpha authority'/'GTC', 'GTC:Alpha Authority:alpha authority').
card_rarity('alpha authority'/'GTC', 'Uncommon').
card_artist('alpha authority'/'GTC', 'Dave Kendall').
card_number('alpha authority'/'GTC', '114').
card_flavor_text('alpha authority'/'GTC', 'Even the bitterest enemies of the Burning-Tree Clan respect the strength of Borborygmos\'s rule.').
card_multiverse_id('alpha authority'/'GTC', '366325').

card_in_set('angelic edict', 'GTC').
card_original_type('angelic edict'/'GTC', 'Sorcery').
card_original_text('angelic edict'/'GTC', 'Exile target creature or enchantment.').
card_first_print('angelic edict', 'GTC').
card_image_name('angelic edict'/'GTC', 'angelic edict').
card_uid('angelic edict'/'GTC', 'GTC:Angelic Edict:angelic edict').
card_rarity('angelic edict'/'GTC', 'Common').
card_artist('angelic edict'/'GTC', 'Trevor Claxton').
card_number('angelic edict'/'GTC', '2').
card_flavor_text('angelic edict'/'GTC', 'The Boros built a prison in the sky where Azorius statutes couldn\'t restrict their sense of justice.').
card_multiverse_id('angelic edict'/'GTC', '366400').

card_in_set('angelic skirmisher', 'GTC').
card_original_type('angelic skirmisher'/'GTC', 'Creature — Angel').
card_original_text('angelic skirmisher'/'GTC', 'Flying\nAt the beginning of each combat, choose first strike, vigilance, or lifelink. Creatures you control gain that ability until end of turn.').
card_image_name('angelic skirmisher'/'GTC', 'angelic skirmisher').
card_uid('angelic skirmisher'/'GTC', 'GTC:Angelic Skirmisher:angelic skirmisher').
card_rarity('angelic skirmisher'/'GTC', 'Rare').
card_artist('angelic skirmisher'/'GTC', 'David Rapoza').
card_number('angelic skirmisher'/'GTC', '3').
card_flavor_text('angelic skirmisher'/'GTC', '\"A keen mind wields an army as a deft hand wields a sword.\"').
card_multiverse_id('angelic skirmisher'/'GTC', '366395').

card_in_set('armored transport', 'GTC').
card_original_type('armored transport'/'GTC', 'Artifact Creature — Construct').
card_original_text('armored transport'/'GTC', 'Prevent all combat damage that would be dealt to Armored Transport by creatures blocking it.').
card_first_print('armored transport', 'GTC').
card_image_name('armored transport'/'GTC', 'armored transport').
card_uid('armored transport'/'GTC', 'GTC:Armored Transport:armored transport').
card_rarity('armored transport'/'GTC', 'Common').
card_artist('armored transport'/'GTC', 'Cliff Childs').
card_number('armored transport'/'GTC', '226').
card_flavor_text('armored transport'/'GTC', '\"If only our jails were as secure.\"\n—Arrester Lavinia, Tenth Precinct').
card_multiverse_id('armored transport'/'GTC', '366344').

card_in_set('arrows of justice', 'GTC').
card_original_type('arrows of justice'/'GTC', 'Instant').
card_original_text('arrows of justice'/'GTC', 'Arrows of Justice deals 4 damage to target attacking or blocking creature.').
card_first_print('arrows of justice', 'GTC').
card_image_name('arrows of justice'/'GTC', 'arrows of justice').
card_uid('arrows of justice'/'GTC', 'GTC:Arrows of Justice:arrows of justice').
card_rarity('arrows of justice'/'GTC', 'Uncommon').
card_artist('arrows of justice'/'GTC', 'James Ryman').
card_number('arrows of justice'/'GTC', '211').
card_flavor_text('arrows of justice'/'GTC', 'Archers from the Horizon Military Academy hone their skills by targeting pickpockets blocks away.').
card_multiverse_id('arrows of justice'/'GTC', '366381').
card_watermark('arrows of justice'/'GTC', 'Boros').

card_in_set('assault griffin', 'GTC').
card_original_type('assault griffin'/'GTC', 'Creature — Griffin').
card_original_text('assault griffin'/'GTC', 'Flying').
card_image_name('assault griffin'/'GTC', 'assault griffin').
card_uid('assault griffin'/'GTC', 'GTC:Assault Griffin:assault griffin').
card_rarity('assault griffin'/'GTC', 'Common').
card_artist('assault griffin'/'GTC', 'Eric Velhagen').
card_number('assault griffin'/'GTC', '4').
card_flavor_text('assault griffin'/'GTC', '\"The Simic offer a prize to any biomancer who can breed a krasis to match a griffin in the air. It\'s never been claimed.\"\n—Libuse, Boros sergeant').
card_multiverse_id('assault griffin'/'GTC', '366337').

card_in_set('assemble the legion', 'GTC').
card_original_type('assemble the legion'/'GTC', 'Enchantment').
card_original_text('assemble the legion'/'GTC', 'At the beginning of your upkeep, put a muster counter on Assemble the Legion. Then put a 1/1 red and white Soldier creature token with haste onto the battlefield for each muster counter on Assemble the Legion.').
card_first_print('assemble the legion', 'GTC').
card_image_name('assemble the legion'/'GTC', 'assemble the legion').
card_uid('assemble the legion'/'GTC', 'GTC:Assemble the Legion:assemble the legion').
card_rarity('assemble the legion'/'GTC', 'Rare').
card_artist('assemble the legion'/'GTC', 'Eric Deschamps').
card_number('assemble the legion'/'GTC', '142').
card_multiverse_id('assemble the legion'/'GTC', '366470').
card_watermark('assemble the legion'/'GTC', 'Boros').

card_in_set('aurelia\'s fury', 'GTC').
card_original_type('aurelia\'s fury'/'GTC', 'Instant').
card_original_text('aurelia\'s fury'/'GTC', 'Aurelia\'s Fury deals X damage divided as you choose among any number of target creatures and/or players. Tap each creature dealt damage this way. Players dealt damage this way can\'t cast noncreature spells this turn.').
card_first_print('aurelia\'s fury', 'GTC').
card_image_name('aurelia\'s fury'/'GTC', 'aurelia\'s fury').
card_uid('aurelia\'s fury'/'GTC', 'GTC:Aurelia\'s Fury:aurelia\'s fury').
card_rarity('aurelia\'s fury'/'GTC', 'Mythic Rare').
card_artist('aurelia\'s fury'/'GTC', 'Tyler Jacobson').
card_number('aurelia\'s fury'/'GTC', '144').
card_multiverse_id('aurelia\'s fury'/'GTC', '366384').
card_watermark('aurelia\'s fury'/'GTC', 'Boros').

card_in_set('aurelia, the warleader', 'GTC').
card_original_type('aurelia, the warleader'/'GTC', 'Legendary Creature — Angel').
card_original_text('aurelia, the warleader'/'GTC', 'Flying, vigilance, haste\nWhenever Aurelia, the Warleader attacks for the first time each turn, untap all creatures you control. After this phase, there is an additional combat phase.').
card_first_print('aurelia, the warleader', 'GTC').
card_image_name('aurelia, the warleader'/'GTC', 'aurelia, the warleader').
card_uid('aurelia, the warleader'/'GTC', 'GTC:Aurelia, the Warleader:aurelia, the warleader').
card_rarity('aurelia, the warleader'/'GTC', 'Mythic Rare').
card_artist('aurelia, the warleader'/'GTC', 'Slawomir Maniak').
card_number('aurelia, the warleader'/'GTC', '143').
card_flavor_text('aurelia, the warleader'/'GTC', 'Where Razia was aloof and untouchable, Aurelia is on the frontlines, calling for war.').
card_multiverse_id('aurelia, the warleader'/'GTC', '366448').
card_watermark('aurelia, the warleader'/'GTC', 'Boros').

card_in_set('balustrade spy', 'GTC').
card_original_type('balustrade spy'/'GTC', 'Creature — Vampire Rogue').
card_original_text('balustrade spy'/'GTC', 'Flying\nWhen Balustrade Spy enters the battlefield, target player reveals cards from the top of his or her library until he or she reveals a land card, then puts those cards into his or her graveyard.').
card_first_print('balustrade spy', 'GTC').
card_image_name('balustrade spy'/'GTC', 'balustrade spy').
card_uid('balustrade spy'/'GTC', 'GTC:Balustrade Spy:balustrade spy').
card_rarity('balustrade spy'/'GTC', 'Common').
card_artist('balustrade spy'/'GTC', 'Jaime Jones').
card_number('balustrade spy'/'GTC', '57').
card_multiverse_id('balustrade spy'/'GTC', '366464').

card_in_set('bane alley broker', 'GTC').
card_original_type('bane alley broker'/'GTC', 'Creature — Human Rogue').
card_original_text('bane alley broker'/'GTC', '{T}: Draw a card, then exile a card from your hand face down.\nYou may look at cards exiled with Bane Alley Broker.\n{U}{B}, {T}: Return a card exiled with Bane Alley Broker to its owner\'s hand.').
card_first_print('bane alley broker', 'GTC').
card_image_name('bane alley broker'/'GTC', 'bane alley broker').
card_uid('bane alley broker'/'GTC', 'GTC:Bane Alley Broker:bane alley broker').
card_rarity('bane alley broker'/'GTC', 'Uncommon').
card_artist('bane alley broker'/'GTC', 'Clint Cearley').
card_number('bane alley broker'/'GTC', '145').
card_multiverse_id('bane alley broker'/'GTC', '366475').
card_watermark('bane alley broker'/'GTC', 'Dimir').

card_in_set('basilica guards', 'GTC').
card_original_type('basilica guards'/'GTC', 'Creature — Human Soldier').
card_original_text('basilica guards'/'GTC', 'Defender\nExtort (Whenever you cast a spell, you may pay {W/B}. If you do, each opponent loses 1 life and you gain that much life.)').
card_first_print('basilica guards', 'GTC').
card_image_name('basilica guards'/'GTC', 'basilica guards').
card_uid('basilica guards'/'GTC', 'GTC:Basilica Guards:basilica guards').
card_rarity('basilica guards'/'GTC', 'Common').
card_artist('basilica guards'/'GTC', 'Dan Scott').
card_number('basilica guards'/'GTC', '5').
card_flavor_text('basilica guards'/'GTC', '\"None may pass . . . for free.\"').
card_multiverse_id('basilica guards'/'GTC', '366347').
card_watermark('basilica guards'/'GTC', 'Orzhov').

card_in_set('basilica screecher', 'GTC').
card_original_type('basilica screecher'/'GTC', 'Creature — Bat').
card_original_text('basilica screecher'/'GTC', 'Flying\nExtort (Whenever you cast a spell, you may pay {W/B}. If you do, each opponent loses 1 life and you gain that much life.)').
card_first_print('basilica screecher', 'GTC').
card_image_name('basilica screecher'/'GTC', 'basilica screecher').
card_uid('basilica screecher'/'GTC', 'GTC:Basilica Screecher:basilica screecher').
card_rarity('basilica screecher'/'GTC', 'Common').
card_artist('basilica screecher'/'GTC', 'Christine Choi').
card_number('basilica screecher'/'GTC', '58').
card_flavor_text('basilica screecher'/'GTC', 'A screech in the night is all it takes for Orzhov debtors to clutch at their coin purses.').
card_multiverse_id('basilica screecher'/'GTC', '366321').
card_watermark('basilica screecher'/'GTC', 'Orzhov').

card_in_set('beckon apparition', 'GTC').
card_original_type('beckon apparition'/'GTC', 'Instant').
card_original_text('beckon apparition'/'GTC', 'Exile target card from a graveyard. Put a 1/1 white and black Spirit creature token with flying onto the battlefield.').
card_image_name('beckon apparition'/'GTC', 'beckon apparition').
card_uid('beckon apparition'/'GTC', 'GTC:Beckon Apparition:beckon apparition').
card_rarity('beckon apparition'/'GTC', 'Common').
card_artist('beckon apparition'/'GTC', 'Cliff Childs').
card_number('beckon apparition'/'GTC', '212').
card_flavor_text('beckon apparition'/'GTC', 'The foyer is littered with the coins it demands but can no longer hold.').
card_multiverse_id('beckon apparition'/'GTC', '366461').
card_watermark('beckon apparition'/'GTC', 'Orzhov').

card_in_set('biomass mutation', 'GTC').
card_original_type('biomass mutation'/'GTC', 'Instant').
card_original_text('biomass mutation'/'GTC', 'Creatures you control become X/X until end of turn.').
card_first_print('biomass mutation', 'GTC').
card_image_name('biomass mutation'/'GTC', 'biomass mutation').
card_uid('biomass mutation'/'GTC', 'GTC:Biomass Mutation:biomass mutation').
card_rarity('biomass mutation'/'GTC', 'Rare').
card_artist('biomass mutation'/'GTC', 'Dan Scott').
card_number('biomass mutation'/'GTC', '213').
card_flavor_text('biomass mutation'/'GTC', '\"A regression to ancient forms? A glimpse of future evolutions? Or a fleeting alteration of the present?\"\n—Vorel of the Hull Clade').
card_multiverse_id('biomass mutation'/'GTC', '366312').
card_watermark('biomass mutation'/'GTC', 'Simic').

card_in_set('bioshift', 'GTC').
card_original_type('bioshift'/'GTC', 'Instant').
card_original_text('bioshift'/'GTC', 'Move any number of +1/+1 counters from target creature onto another target creature with the same controller.').
card_first_print('bioshift', 'GTC').
card_image_name('bioshift'/'GTC', 'bioshift').
card_uid('bioshift'/'GTC', 'GTC:Bioshift:bioshift').
card_rarity('bioshift'/'GTC', 'Common').
card_artist('bioshift'/'GTC', 'Scott Chou').
card_number('bioshift'/'GTC', '214').
card_flavor_text('bioshift'/'GTC', '\"It\'s all right if you change your mind. There\'s always a chance we can rearrange parts later.\"\n—Zija, Simic mutationist').
card_multiverse_id('bioshift'/'GTC', '366299').
card_watermark('bioshift'/'GTC', 'Simic').

card_in_set('biovisionary', 'GTC').
card_original_type('biovisionary'/'GTC', 'Creature — Human Wizard').
card_original_text('biovisionary'/'GTC', 'At the beginning of the end step, if you control four or more creatures named Biovisionary, you win the game.').
card_first_print('biovisionary', 'GTC').
card_image_name('biovisionary'/'GTC', 'biovisionary').
card_uid('biovisionary'/'GTC', 'GTC:Biovisionary:biovisionary').
card_rarity('biovisionary'/'GTC', 'Rare').
card_artist('biovisionary'/'GTC', 'Ryan Barger').
card_number('biovisionary'/'GTC', '146').
card_flavor_text('biovisionary'/'GTC', 'He tired of his fellow researchers and their useless prattling, so he decided to make a network of scholars with whom he could carry a conversation.').
card_multiverse_id('biovisionary'/'GTC', '366414').
card_watermark('biovisionary'/'GTC', 'Simic').

card_in_set('blind obedience', 'GTC').
card_original_type('blind obedience'/'GTC', 'Enchantment').
card_original_text('blind obedience'/'GTC', 'Extort (Whenever you cast a spell, you may pay {W/B}. If you do, each opponent loses 1 life and you gain that much life.)\nArtifacts and creatures your opponents control enter the battlefield tapped.').
card_first_print('blind obedience', 'GTC').
card_image_name('blind obedience'/'GTC', 'blind obedience').
card_uid('blind obedience'/'GTC', 'GTC:Blind Obedience:blind obedience').
card_rarity('blind obedience'/'GTC', 'Rare').
card_artist('blind obedience'/'GTC', 'Seb McKinnon').
card_number('blind obedience'/'GTC', '6').
card_flavor_text('blind obedience'/'GTC', '\"By the time your knees have worn through your robe, you may have begun to learn your place.\"').
card_multiverse_id('blind obedience'/'GTC', '366465').
card_watermark('blind obedience'/'GTC', 'Orzhov').

card_in_set('bomber corps', 'GTC').
card_original_type('bomber corps'/'GTC', 'Creature — Human Soldier').
card_original_text('bomber corps'/'GTC', 'Battalion — Whenever Bomber Corps and at least two other creatures attack, Bomber Corps deals 1 damage to target creature or player.').
card_first_print('bomber corps', 'GTC').
card_image_name('bomber corps'/'GTC', 'bomber corps').
card_uid('bomber corps'/'GTC', 'GTC:Bomber Corps:bomber corps').
card_rarity('bomber corps'/'GTC', 'Common').
card_artist('bomber corps'/'GTC', 'Chase Stone').
card_number('bomber corps'/'GTC', '86').
card_flavor_text('bomber corps'/'GTC', '\"Subtlety is for lawmages and spies.\"').
card_multiverse_id('bomber corps'/'GTC', '366370').
card_watermark('bomber corps'/'GTC', 'Boros').

card_in_set('borborygmos enraged', 'GTC').
card_original_type('borborygmos enraged'/'GTC', 'Legendary Creature — Cyclops').
card_original_text('borborygmos enraged'/'GTC', 'Trample\nWhenever Borborygmos Enraged deals combat damage to a player, reveal the top three cards of your library. Put all land cards revealed this way into your hand and the rest into your graveyard.\nDiscard a land card: Borborygmos Enraged deals 3 damage to target creature or player.').
card_first_print('borborygmos enraged', 'GTC').
card_image_name('borborygmos enraged'/'GTC', 'borborygmos enraged').
card_uid('borborygmos enraged'/'GTC', 'GTC:Borborygmos Enraged:borborygmos enraged').
card_rarity('borborygmos enraged'/'GTC', 'Mythic Rare').
card_artist('borborygmos enraged'/'GTC', 'Aleksi Briclot').
card_number('borborygmos enraged'/'GTC', '147').
card_multiverse_id('borborygmos enraged'/'GTC', '366283').
card_watermark('borborygmos enraged'/'GTC', 'Gruul').

card_in_set('boros charm', 'GTC').
card_original_type('boros charm'/'GTC', 'Instant').
card_original_text('boros charm'/'GTC', 'Choose one — Boros Charm deals 4 damage to target player; or permanents you control are indestructible this turn; or target creature gains double strike until end of turn.').
card_first_print('boros charm', 'GTC').
card_image_name('boros charm'/'GTC', 'boros charm').
card_uid('boros charm'/'GTC', 'GTC:Boros Charm:boros charm').
card_rarity('boros charm'/'GTC', 'Uncommon').
card_artist('boros charm'/'GTC', 'Zoltan Boros').
card_number('boros charm'/'GTC', '148').
card_flavor_text('boros charm'/'GTC', '\"Practice compassion and mercy. But know when they must end.\"\n—Aurelia').
card_multiverse_id('boros charm'/'GTC', '366435').
card_watermark('boros charm'/'GTC', 'Boros').

card_in_set('boros elite', 'GTC').
card_original_type('boros elite'/'GTC', 'Creature — Human Soldier').
card_original_text('boros elite'/'GTC', 'Battalion — Whenever Boros Elite and at least two other creatures attack, Boros Elite gets +2/+2 until end of turn.').
card_first_print('boros elite', 'GTC').
card_image_name('boros elite'/'GTC', 'boros elite').
card_uid('boros elite'/'GTC', 'GTC:Boros Elite:boros elite').
card_rarity('boros elite'/'GTC', 'Uncommon').
card_artist('boros elite'/'GTC', 'Willian Murai').
card_number('boros elite'/'GTC', '7').
card_flavor_text('boros elite'/'GTC', '\"First, you must fight among the rank and file. It is there I will discern your skills as a leader.\"\n—Aurelia, to Gideon Jura').
card_multiverse_id('boros elite'/'GTC', '366369').
card_watermark('boros elite'/'GTC', 'Boros').

card_in_set('boros guildgate', 'GTC').
card_original_type('boros guildgate'/'GTC', 'Land — Gate').
card_original_text('boros guildgate'/'GTC', 'Boros Guildgate enters the battlefield tapped.\n{T}: Add {R} or {W} to your mana pool.').
card_first_print('boros guildgate', 'GTC').
card_image_name('boros guildgate'/'GTC', 'boros guildgate').
card_uid('boros guildgate'/'GTC', 'GTC:Boros Guildgate:boros guildgate').
card_rarity('boros guildgate'/'GTC', 'Common').
card_artist('boros guildgate'/'GTC', 'Noah Bradley').
card_number('boros guildgate'/'GTC', '239').
card_flavor_text('boros guildgate'/'GTC', 'Enter with merciful hearts. Exit with battle cries.').
card_multiverse_id('boros guildgate'/'GTC', '366390').
card_watermark('boros guildgate'/'GTC', 'Boros').

card_in_set('boros keyrune', 'GTC').
card_original_type('boros keyrune'/'GTC', 'Artifact').
card_original_text('boros keyrune'/'GTC', '{T}: Add {R} or {W} to your mana pool.\n{R}{W}: Boros Keyrune becomes a 1/1 red and white Soldier artifact creature with double strike until end of turn. (It deals both first-strike and regular combat damage.)').
card_first_print('boros keyrune', 'GTC').
card_image_name('boros keyrune'/'GTC', 'boros keyrune').
card_uid('boros keyrune'/'GTC', 'GTC:Boros Keyrune:boros keyrune').
card_rarity('boros keyrune'/'GTC', 'Uncommon').
card_artist('boros keyrune'/'GTC', 'Daniel Ljunggren').
card_number('boros keyrune'/'GTC', '227').
card_multiverse_id('boros keyrune'/'GTC', '366413').
card_watermark('boros keyrune'/'GTC', 'Boros').

card_in_set('boros reckoner', 'GTC').
card_original_type('boros reckoner'/'GTC', 'Creature — Minotaur Wizard').
card_original_text('boros reckoner'/'GTC', 'Whenever Boros Reckoner is dealt damage, it deals that much damage to target creature or player.\n{R/W}: Boros Reckoner gains first strike until end of turn.').
card_first_print('boros reckoner', 'GTC').
card_image_name('boros reckoner'/'GTC', 'boros reckoner').
card_uid('boros reckoner'/'GTC', 'GTC:Boros Reckoner:boros reckoner').
card_rarity('boros reckoner'/'GTC', 'Rare').
card_artist('boros reckoner'/'GTC', 'Howard Lyon').
card_number('boros reckoner'/'GTC', '215').
card_flavor_text('boros reckoner'/'GTC', '\"Imagine a thunderstorm that\'s also a tactical genius. That\'s him.\"\n—Dars Gostok, Firefist captain').
card_multiverse_id('boros reckoner'/'GTC', '366385').
card_watermark('boros reckoner'/'GTC', 'Boros').

card_in_set('breeding pool', 'GTC').
card_original_type('breeding pool'/'GTC', 'Land — Forest Island').
card_original_text('breeding pool'/'GTC', '({T}: Add {G} or {U} to your mana pool.)\nAs Breeding Pool enters the battlefield, you may pay 2 life. If you don\'t, Breeding Pool enters the battlefield tapped.').
card_image_name('breeding pool'/'GTC', 'breeding pool').
card_uid('breeding pool'/'GTC', 'GTC:Breeding Pool:breeding pool').
card_rarity('breeding pool'/'GTC', 'Rare').
card_artist('breeding pool'/'GTC', 'Mike Bierek').
card_number('breeding pool'/'GTC', '240').
card_flavor_text('breeding pool'/'GTC', 'Uncharted depths. Unbounded hopes. Unfathomable mysteries.').
card_multiverse_id('breeding pool'/'GTC', '366291').
card_watermark('breeding pool'/'GTC', 'Simic').

card_in_set('burning-tree emissary', 'GTC').
card_original_type('burning-tree emissary'/'GTC', 'Creature — Human Shaman').
card_original_text('burning-tree emissary'/'GTC', 'When Burning-Tree Emissary enters the battlefield, add {R}{G} to your mana pool.').
card_first_print('burning-tree emissary', 'GTC').
card_image_name('burning-tree emissary'/'GTC', 'burning-tree emissary').
card_uid('burning-tree emissary'/'GTC', 'GTC:Burning-Tree Emissary:burning-tree emissary').
card_rarity('burning-tree emissary'/'GTC', 'Uncommon').
card_artist('burning-tree emissary'/'GTC', 'Izzy').
card_number('burning-tree emissary'/'GTC', '216').
card_flavor_text('burning-tree emissary'/'GTC', 'Those who regard the Gruul as savage simpletons underestimate the subtle power of their shamans.').
card_multiverse_id('burning-tree emissary'/'GTC', '366467').
card_watermark('burning-tree emissary'/'GTC', 'Gruul').

card_in_set('burst of strength', 'GTC').
card_original_type('burst of strength'/'GTC', 'Instant').
card_original_text('burst of strength'/'GTC', 'Put a +1/+1 counter on target creature and untap it.').
card_first_print('burst of strength', 'GTC').
card_image_name('burst of strength'/'GTC', 'burst of strength').
card_uid('burst of strength'/'GTC', 'GTC:Burst of Strength:burst of strength').
card_rarity('burst of strength'/'GTC', 'Common').
card_artist('burst of strength'/'GTC', 'Marco Nelor').
card_number('burst of strength'/'GTC', '115').
card_flavor_text('burst of strength'/'GTC', '\"Our fight is not about territory or revenge. It is about freedom, plain and simple.\"\n—Ruric Thar, Ghor-Clan chieftain').
card_multiverse_id('burst of strength'/'GTC', '366304').

card_in_set('call of the nightwing', 'GTC').
card_original_type('call of the nightwing'/'GTC', 'Sorcery').
card_original_text('call of the nightwing'/'GTC', 'Put a 1/1 blue and black Horror creature token with flying onto the battlefield.\nCipher (Then you may exile this spell card encoded on a creature you control. Whenever that creature deals combat damage to a player, its controller may cast a copy of the encoded card without paying its mana cost.)').
card_first_print('call of the nightwing', 'GTC').
card_image_name('call of the nightwing'/'GTC', 'call of the nightwing').
card_uid('call of the nightwing'/'GTC', 'GTC:Call of the Nightwing:call of the nightwing').
card_rarity('call of the nightwing'/'GTC', 'Uncommon').
card_artist('call of the nightwing'/'GTC', 'Adam Paquette').
card_number('call of the nightwing'/'GTC', '149').
card_multiverse_id('call of the nightwing'/'GTC', '366308').
card_watermark('call of the nightwing'/'GTC', 'Dimir').

card_in_set('cartel aristocrat', 'GTC').
card_original_type('cartel aristocrat'/'GTC', 'Creature — Human Advisor').
card_original_text('cartel aristocrat'/'GTC', 'Sacrifice another creature: Cartel Aristocrat gains protection from the color of your choice until end of turn.').
card_first_print('cartel aristocrat', 'GTC').
card_image_name('cartel aristocrat'/'GTC', 'cartel aristocrat').
card_uid('cartel aristocrat'/'GTC', 'GTC:Cartel Aristocrat:cartel aristocrat').
card_rarity('cartel aristocrat'/'GTC', 'Uncommon').
card_artist('cartel aristocrat'/'GTC', 'James Ryman').
card_number('cartel aristocrat'/'GTC', '150').
card_flavor_text('cartel aristocrat'/'GTC', '\"Your life serves a higher purpose now: ensuring the continuation of my own.\"').
card_multiverse_id('cartel aristocrat'/'GTC', '366364').
card_watermark('cartel aristocrat'/'GTC', 'Orzhov').

card_in_set('cinder elemental', 'GTC').
card_original_type('cinder elemental'/'GTC', 'Creature — Elemental').
card_original_text('cinder elemental'/'GTC', '{X}{R}, {T}, Sacrifice Cinder Elemental: Cinder Elemental deals X damage to target creature or player.').
card_image_name('cinder elemental'/'GTC', 'cinder elemental').
card_uid('cinder elemental'/'GTC', 'GTC:Cinder Elemental:cinder elemental').
card_rarity('cinder elemental'/'GTC', 'Uncommon').
card_artist('cinder elemental'/'GTC', 'Svetlin Velinov').
card_number('cinder elemental'/'GTC', '87').
card_flavor_text('cinder elemental'/'GTC', '\"The old gods awoke and sent fingers of fire to see what was left of the old ways under the endless city.\"\n—Daiva, Gruul storyteller').
card_multiverse_id('cinder elemental'/'GTC', '366314').

card_in_set('clan defiance', 'GTC').
card_original_type('clan defiance'/'GTC', 'Sorcery').
card_original_text('clan defiance'/'GTC', 'Choose one or more — Clan Defiance deals X damage to target creature with flying; Clan Defiance deals X damage to target creature without flying; and/or Clan Defiance deals X damage to target player.').
card_first_print('clan defiance', 'GTC').
card_image_name('clan defiance'/'GTC', 'clan defiance').
card_uid('clan defiance'/'GTC', 'GTC:Clan Defiance:clan defiance').
card_rarity('clan defiance'/'GTC', 'Rare').
card_artist('clan defiance'/'GTC', 'Daarken').
card_number('clan defiance'/'GTC', '151').
card_multiverse_id('clan defiance'/'GTC', '366468').
card_watermark('clan defiance'/'GTC', 'Gruul').

card_in_set('clinging anemones', 'GTC').
card_original_type('clinging anemones'/'GTC', 'Creature — Jellyfish').
card_original_text('clinging anemones'/'GTC', 'Defender\nEvolve (Whenever a creature enters the battlefield under your control, if that creature has greater power or toughness than this creature, put a +1/+1 counter on this creature.)').
card_first_print('clinging anemones', 'GTC').
card_image_name('clinging anemones'/'GTC', 'clinging anemones').
card_uid('clinging anemones'/'GTC', 'GTC:Clinging Anemones:clinging anemones').
card_rarity('clinging anemones'/'GTC', 'Common').
card_artist('clinging anemones'/'GTC', 'Mike Bierek').
card_number('clinging anemones'/'GTC', '31').
card_multiverse_id('clinging anemones'/'GTC', '366363').
card_watermark('clinging anemones'/'GTC', 'Simic').

card_in_set('cloudfin raptor', 'GTC').
card_original_type('cloudfin raptor'/'GTC', 'Creature — Bird Mutant').
card_original_text('cloudfin raptor'/'GTC', 'Flying\nEvolve (Whenever a creature enters the battlefield under your control, if that creature has greater power or toughness than this creature, put a +1/+1 counter on this creature.)').
card_first_print('cloudfin raptor', 'GTC').
card_image_name('cloudfin raptor'/'GTC', 'cloudfin raptor').
card_uid('cloudfin raptor'/'GTC', 'GTC:Cloudfin Raptor:cloudfin raptor').
card_rarity('cloudfin raptor'/'GTC', 'Common').
card_artist('cloudfin raptor'/'GTC', 'Peter Mohrbacher').
card_number('cloudfin raptor'/'GTC', '32').
card_multiverse_id('cloudfin raptor'/'GTC', '366403').
card_watermark('cloudfin raptor'/'GTC', 'Simic').

card_in_set('coerced confession', 'GTC').
card_original_type('coerced confession'/'GTC', 'Sorcery').
card_original_text('coerced confession'/'GTC', 'Target player puts the top four cards of his or her library into his or her graveyard. You draw a card for each creature card put into that graveyard this way.').
card_first_print('coerced confession', 'GTC').
card_image_name('coerced confession'/'GTC', 'coerced confession').
card_uid('coerced confession'/'GTC', 'GTC:Coerced Confession:coerced confession').
card_rarity('coerced confession'/'GTC', 'Uncommon').
card_artist('coerced confession'/'GTC', 'Mathias Kollros').
card_number('coerced confession'/'GTC', '217').
card_flavor_text('coerced confession'/'GTC', '\"Ask the right questions in the right way and truth is inevitable.\"\n—Lazav').
card_multiverse_id('coerced confession'/'GTC', '366408').
card_watermark('coerced confession'/'GTC', 'Dimir').

card_in_set('consuming aberration', 'GTC').
card_original_type('consuming aberration'/'GTC', 'Creature — Horror').
card_original_text('consuming aberration'/'GTC', 'Consuming Aberration\'s power and toughness are each equal to the number of cards in your opponents\' graveyards.\nWhenever you cast a spell, each opponent reveals cards from the top of his or her library until he or she reveals a land card, then puts those cards into his or her graveyard.').
card_image_name('consuming aberration'/'GTC', 'consuming aberration').
card_uid('consuming aberration'/'GTC', 'GTC:Consuming Aberration:consuming aberration').
card_rarity('consuming aberration'/'GTC', 'Rare').
card_artist('consuming aberration'/'GTC', 'Karl Kopinski').
card_number('consuming aberration'/'GTC', '152').
card_multiverse_id('consuming aberration'/'GTC', '366391').
card_watermark('consuming aberration'/'GTC', 'Dimir').

card_in_set('contaminated ground', 'GTC').
card_original_type('contaminated ground'/'GTC', 'Enchantment — Aura').
card_original_text('contaminated ground'/'GTC', 'Enchant land\nEnchanted land is a Swamp.\nWhenever enchanted land becomes tapped, its controller loses 2 life.').
card_image_name('contaminated ground'/'GTC', 'contaminated ground').
card_uid('contaminated ground'/'GTC', 'GTC:Contaminated Ground:contaminated ground').
card_rarity('contaminated ground'/'GTC', 'Common').
card_artist('contaminated ground'/'GTC', 'Christine Choi').
card_number('contaminated ground'/'GTC', '59').
card_flavor_text('contaminated ground'/'GTC', '\"No one cares about pollution until they can see it.\"\n—Gurras, Golgari rot farmer').
card_multiverse_id('contaminated ground'/'GTC', '366420').

card_in_set('corpse blockade', 'GTC').
card_original_type('corpse blockade'/'GTC', 'Creature — Zombie').
card_original_text('corpse blockade'/'GTC', 'Defender\nSacrifice another creature: Corpse Blockade gains deathtouch until end of turn.').
card_first_print('corpse blockade', 'GTC').
card_image_name('corpse blockade'/'GTC', 'corpse blockade').
card_uid('corpse blockade'/'GTC', 'GTC:Corpse Blockade:corpse blockade').
card_rarity('corpse blockade'/'GTC', 'Common').
card_artist('corpse blockade'/'GTC', 'Lucas Graciano').
card_number('corpse blockade'/'GTC', '60').
card_flavor_text('corpse blockade'/'GTC', 'There are laws against it, but the dead have no one to complain to and the living are too frightened to investigate.').
card_multiverse_id('corpse blockade'/'GTC', '366277').

card_in_set('court street denizen', 'GTC').
card_original_type('court street denizen'/'GTC', 'Creature — Human Soldier').
card_original_text('court street denizen'/'GTC', 'Whenever another white creature enters the battlefield under your control, tap target creature an opponent controls.').
card_first_print('court street denizen', 'GTC').
card_image_name('court street denizen'/'GTC', 'court street denizen').
card_uid('court street denizen'/'GTC', 'GTC:Court Street Denizen:court street denizen').
card_rarity('court street denizen'/'GTC', 'Common').
card_artist('court street denizen'/'GTC', 'Volkan Baga').
card_number('court street denizen'/'GTC', '8').
card_flavor_text('court street denizen'/'GTC', '\"The Boros fight for justice. The Azorius fight for law. I hold the line between, and make sure the people are given both.\"').
card_multiverse_id('court street denizen'/'GTC', '366389').

card_in_set('crackling perimeter', 'GTC').
card_original_type('crackling perimeter'/'GTC', 'Enchantment').
card_original_text('crackling perimeter'/'GTC', 'Tap an untapped Gate you control: Crackling Perimeter deals 1 damage to each opponent.').
card_first_print('crackling perimeter', 'GTC').
card_image_name('crackling perimeter'/'GTC', 'crackling perimeter').
card_uid('crackling perimeter'/'GTC', 'GTC:Crackling Perimeter:crackling perimeter').
card_rarity('crackling perimeter'/'GTC', 'Uncommon').
card_artist('crackling perimeter'/'GTC', 'Yeong-Hao Han').
card_number('crackling perimeter'/'GTC', '88').
card_flavor_text('crackling perimeter'/'GTC', 'As the path he sought came into focus, Niv-Mizzet took steps to prevent others from approaching it.').
card_multiverse_id('crackling perimeter'/'GTC', '366255').

card_in_set('crocanura', 'GTC').
card_original_type('crocanura'/'GTC', 'Creature — Crocodile Frog').
card_original_text('crocanura'/'GTC', 'Reach (This creature can block creatures with flying.)\nEvolve (Whenever a creature enters the battlefield under your control, if that creature has greater power or toughness than this creature, put a +1/+1 counter on this creature.)').
card_first_print('crocanura', 'GTC').
card_image_name('crocanura'/'GTC', 'crocanura').
card_uid('crocanura'/'GTC', 'GTC:Crocanura:crocanura').
card_rarity('crocanura'/'GTC', 'Common').
card_artist('crocanura'/'GTC', 'Jack Wang').
card_number('crocanura'/'GTC', '116').
card_multiverse_id('crocanura'/'GTC', '366319').
card_watermark('crocanura'/'GTC', 'Simic').

card_in_set('crowned ceratok', 'GTC').
card_original_type('crowned ceratok'/'GTC', 'Creature — Rhino').
card_original_text('crowned ceratok'/'GTC', 'Trample\nEach creature you control with a +1/+1 counter on it has trample.').
card_first_print('crowned ceratok', 'GTC').
card_image_name('crowned ceratok'/'GTC', 'crowned ceratok').
card_uid('crowned ceratok'/'GTC', 'GTC:Crowned Ceratok:crowned ceratok').
card_rarity('crowned ceratok'/'GTC', 'Uncommon').
card_artist('crowned ceratok'/'GTC', 'Steve Prescott').
card_number('crowned ceratok'/'GTC', '117').
card_flavor_text('crowned ceratok'/'GTC', 'The best way to calm a rampaging ceratok is to wait until it dies of natural causes.').
card_multiverse_id('crowned ceratok'/'GTC', '366256').

card_in_set('crypt ghast', 'GTC').
card_original_type('crypt ghast'/'GTC', 'Creature — Spirit').
card_original_text('crypt ghast'/'GTC', 'Extort (Whenever you cast a spell, you may pay {W/B}. If you do, each opponent loses 1 life and you gain that much life.)\nWhenever you tap a Swamp for mana, add {B} to your mana pool (in addition to the mana the land produces).').
card_first_print('crypt ghast', 'GTC').
card_image_name('crypt ghast'/'GTC', 'crypt ghast').
card_uid('crypt ghast'/'GTC', 'GTC:Crypt Ghast:crypt ghast').
card_rarity('crypt ghast'/'GTC', 'Rare').
card_artist('crypt ghast'/'GTC', 'Chris Rahn').
card_number('crypt ghast'/'GTC', '61').
card_multiverse_id('crypt ghast'/'GTC', '366452').
card_watermark('crypt ghast'/'GTC', 'Orzhov').

card_in_set('daring skyjek', 'GTC').
card_original_type('daring skyjek'/'GTC', 'Creature — Human Knight').
card_original_text('daring skyjek'/'GTC', 'Battalion — Whenever Daring Skyjek and at least two other creatures attack, Daring Skyjek gains flying until end of turn.').
card_first_print('daring skyjek', 'GTC').
card_image_name('daring skyjek'/'GTC', 'daring skyjek').
card_uid('daring skyjek'/'GTC', 'GTC:Daring Skyjek:daring skyjek').
card_rarity('daring skyjek'/'GTC', 'Common').
card_artist('daring skyjek'/'GTC', 'Jason Chan').
card_number('daring skyjek'/'GTC', '9').
card_flavor_text('daring skyjek'/'GTC', '\"The hard part isn\'t landing in the saddle. The hard part is leaping before you see it.\"').
card_multiverse_id('daring skyjek'/'GTC', '366251').
card_watermark('daring skyjek'/'GTC', 'Boros').

card_in_set('death\'s approach', 'GTC').
card_original_type('death\'s approach'/'GTC', 'Enchantment — Aura').
card_original_text('death\'s approach'/'GTC', 'Enchant creature\nEnchanted creature gets -X/-X, where X is the number of creature cards in its controller\'s graveyard.').
card_first_print('death\'s approach', 'GTC').
card_image_name('death\'s approach'/'GTC', 'death\'s approach').
card_uid('death\'s approach'/'GTC', 'GTC:Death\'s Approach:death\'s approach').
card_rarity('death\'s approach'/'GTC', 'Common').
card_artist('death\'s approach'/'GTC', 'Terese Nielsen').
card_number('death\'s approach'/'GTC', '62').
card_flavor_text('death\'s approach'/'GTC', '\"It\'s cold in the grave, and the dead crave your warmth. Who are you to deny them?\"\n—Strava, Dimir mage').
card_multiverse_id('death\'s approach'/'GTC', '366300').

card_in_set('deathcult rogue', 'GTC').
card_original_type('deathcult rogue'/'GTC', 'Creature — Human Rogue').
card_original_text('deathcult rogue'/'GTC', 'Deathcult Rogue can\'t be blocked except by Rogues.').
card_first_print('deathcult rogue', 'GTC').
card_image_name('deathcult rogue'/'GTC', 'deathcult rogue').
card_uid('deathcult rogue'/'GTC', 'GTC:Deathcult Rogue:deathcult rogue').
card_rarity('deathcult rogue'/'GTC', 'Common').
card_artist('deathcult rogue'/'GTC', 'David Palumbo').
card_number('deathcult rogue'/'GTC', '218').
card_flavor_text('deathcult rogue'/'GTC', '\"The hussars can\'t catch him, and he evades our traps with ease. I fear he walks unseen even in the halls of New Prahv.\"\n—Arrester Lavinia, Tenth Precinct').
card_multiverse_id('deathcult rogue'/'GTC', '366254').
card_watermark('deathcult rogue'/'GTC', 'Dimir').

card_in_set('deathpact angel', 'GTC').
card_original_type('deathpact angel'/'GTC', 'Creature — Angel').
card_original_text('deathpact angel'/'GTC', 'Flying\nWhen Deathpact Angel dies, put a 1/1 white and black Cleric creature token onto the battlefield. It has \"{3}{W}{B}{B}, {T}, Sacrifice this creature: Return a card named Deathpact Angel from your graveyard to the battlefield.\"').
card_first_print('deathpact angel', 'GTC').
card_image_name('deathpact angel'/'GTC', 'deathpact angel').
card_uid('deathpact angel'/'GTC', 'GTC:Deathpact Angel:deathpact angel').
card_rarity('deathpact angel'/'GTC', 'Mythic Rare').
card_artist('deathpact angel'/'GTC', 'Jason Chan').
card_number('deathpact angel'/'GTC', '153').
card_multiverse_id('deathpact angel'/'GTC', '366278').
card_watermark('deathpact angel'/'GTC', 'Orzhov').

card_in_set('debtor\'s pulpit', 'GTC').
card_original_type('debtor\'s pulpit'/'GTC', 'Enchantment — Aura').
card_original_text('debtor\'s pulpit'/'GTC', 'Enchant land\nEnchanted land has \"{T}: Tap target creature.\"').
card_first_print('debtor\'s pulpit', 'GTC').
card_image_name('debtor\'s pulpit'/'GTC', 'debtor\'s pulpit').
card_uid('debtor\'s pulpit'/'GTC', 'GTC:Debtor\'s Pulpit:debtor\'s pulpit').
card_rarity('debtor\'s pulpit'/'GTC', 'Uncommon').
card_artist('debtor\'s pulpit'/'GTC', 'James Paick').
card_number('debtor\'s pulpit'/'GTC', '10').
card_flavor_text('debtor\'s pulpit'/'GTC', '\"You may not be able to pay. But you can still kneel.\"\n—Milana, Orzhov prelate').
card_multiverse_id('debtor\'s pulpit'/'GTC', '366268').

card_in_set('devour flesh', 'GTC').
card_original_type('devour flesh'/'GTC', 'Instant').
card_original_text('devour flesh'/'GTC', 'Target player sacrifices a creature, then gains life equal to that creature\'s toughness.').
card_first_print('devour flesh', 'GTC').
card_image_name('devour flesh'/'GTC', 'devour flesh').
card_uid('devour flesh'/'GTC', 'GTC:Devour Flesh:devour flesh').
card_rarity('devour flesh'/'GTC', 'Common').
card_artist('devour flesh'/'GTC', 'Kev Walker').
card_number('devour flesh'/'GTC', '63').
card_flavor_text('devour flesh'/'GTC', 'His twisted mind concluded that if he was what he ate, and he wanted to stay human, . . .').
card_multiverse_id('devour flesh'/'GTC', '366379').

card_in_set('diluvian primordial', 'GTC').
card_original_type('diluvian primordial'/'GTC', 'Creature — Avatar').
card_original_text('diluvian primordial'/'GTC', 'Flying\nWhen Diluvian Primordial enters the battlefield, for each opponent, you may cast up to one target instant or sorcery card from that player\'s graveyard without paying its mana cost. If a card cast this way would be put into a graveyard this turn, exile it instead.').
card_first_print('diluvian primordial', 'GTC').
card_image_name('diluvian primordial'/'GTC', 'diluvian primordial').
card_uid('diluvian primordial'/'GTC', 'GTC:Diluvian Primordial:diluvian primordial').
card_rarity('diluvian primordial'/'GTC', 'Rare').
card_artist('diluvian primordial'/'GTC', 'Stephan Martiniere').
card_number('diluvian primordial'/'GTC', '33').
card_multiverse_id('diluvian primordial'/'GTC', '366326').

card_in_set('dimir charm', 'GTC').
card_original_type('dimir charm'/'GTC', 'Instant').
card_original_text('dimir charm'/'GTC', 'Choose one — Counter target sorcery spell; or destroy target creature with power 2 or less; or look at the top three cards of target player\'s library, then put one back and the rest into that player\'s graveyard.').
card_image_name('dimir charm'/'GTC', 'dimir charm').
card_uid('dimir charm'/'GTC', 'GTC:Dimir Charm:dimir charm').
card_rarity('dimir charm'/'GTC', 'Uncommon').
card_artist('dimir charm'/'GTC', 'Zoltan Boros').
card_number('dimir charm'/'GTC', '154').
card_flavor_text('dimir charm'/'GTC', '\"Dangerous to recognize. Deadly not to.\"\n—Lazav').
card_multiverse_id('dimir charm'/'GTC', '366473').
card_watermark('dimir charm'/'GTC', 'Dimir').

card_in_set('dimir guildgate', 'GTC').
card_original_type('dimir guildgate'/'GTC', 'Land — Gate').
card_original_text('dimir guildgate'/'GTC', 'Dimir Guildgate enters the battlefield tapped.\n{T}: Add {U} or {B} to your mana pool.').
card_first_print('dimir guildgate', 'GTC').
card_image_name('dimir guildgate'/'GTC', 'dimir guildgate').
card_uid('dimir guildgate'/'GTC', 'GTC:Dimir Guildgate:dimir guildgate').
card_rarity('dimir guildgate'/'GTC', 'Common').
card_artist('dimir guildgate'/'GTC', 'Cliff Childs').
card_number('dimir guildgate'/'GTC', '241').
card_flavor_text('dimir guildgate'/'GTC', 'Enter and become a mastermind of spies, lies, and deception.').
card_multiverse_id('dimir guildgate'/'GTC', '366313').
card_watermark('dimir guildgate'/'GTC', 'Dimir').

card_in_set('dimir keyrune', 'GTC').
card_original_type('dimir keyrune'/'GTC', 'Artifact').
card_original_text('dimir keyrune'/'GTC', '{T}: Add {U} or {B} to your mana pool.\n{U}{B}: Dimir Keyrune becomes a 2/2 blue and black Horror artifact creature until end of turn and is unblockable this turn.').
card_first_print('dimir keyrune', 'GTC').
card_image_name('dimir keyrune'/'GTC', 'dimir keyrune').
card_uid('dimir keyrune'/'GTC', 'GTC:Dimir Keyrune:dimir keyrune').
card_rarity('dimir keyrune'/'GTC', 'Uncommon').
card_artist('dimir keyrune'/'GTC', 'Daniel Ljunggren').
card_number('dimir keyrune'/'GTC', '228').
card_flavor_text('dimir keyrune'/'GTC', 'A key that slips through every lock.').
card_multiverse_id('dimir keyrune'/'GTC', '366270').
card_watermark('dimir keyrune'/'GTC', 'Dimir').

card_in_set('dinrova horror', 'GTC').
card_original_type('dinrova horror'/'GTC', 'Creature — Horror').
card_original_text('dinrova horror'/'GTC', 'When Dinrova Horror enters the battlefield, return target permanent to its owner\'s hand, then that player discards a card.').
card_first_print('dinrova horror', 'GTC').
card_image_name('dinrova horror'/'GTC', 'dinrova horror').
card_uid('dinrova horror'/'GTC', 'GTC:Dinrova Horror:dinrova horror').
card_rarity('dinrova horror'/'GTC', 'Uncommon').
card_artist('dinrova horror'/'GTC', 'Johann Bodin').
card_number('dinrova horror'/'GTC', '155').
card_flavor_text('dinrova horror'/'GTC', '\"Welcome to the Dimir Public Offices. Not responsible for death or loss of property. Basement off-limits.\"\n—Sign outside Dinrova Building').
card_multiverse_id('dinrova horror'/'GTC', '366453').
card_watermark('dinrova horror'/'GTC', 'Dimir').

card_in_set('disciple of the old ways', 'GTC').
card_original_type('disciple of the old ways'/'GTC', 'Creature — Human Warrior').
card_original_text('disciple of the old ways'/'GTC', '{R}: Disciple of the Old Ways gains first strike until end of turn.').
card_first_print('disciple of the old ways', 'GTC').
card_image_name('disciple of the old ways'/'GTC', 'disciple of the old ways').
card_uid('disciple of the old ways'/'GTC', 'GTC:Disciple of the Old Ways:disciple of the old ways').
card_rarity('disciple of the old ways'/'GTC', 'Common').
card_artist('disciple of the old ways'/'GTC', 'Anthony Palumbo').
card_number('disciple of the old ways'/'GTC', '118').
card_flavor_text('disciple of the old ways'/'GTC', '\"Selesnya thinks nature is a pretty plaything. True nature would rip their faces off and wear their skins as trophies.\"').
card_multiverse_id('disciple of the old ways'/'GTC', '366447').
card_watermark('disciple of the old ways'/'GTC', 'Gruul').

card_in_set('domri rade', 'GTC').
card_original_type('domri rade'/'GTC', 'Planeswalker — Domri').
card_original_text('domri rade'/'GTC', '+1: Look at the top card of your library. If it\'s a creature card, you may reveal it and put it into your hand.\n-2: Target creature you control fights another target creature.\n-7: You get an emblem with \"Creatures you control have double strike, trample, hexproof, and haste.\"').
card_first_print('domri rade', 'GTC').
card_image_name('domri rade'/'GTC', 'domri rade').
card_uid('domri rade'/'GTC', 'GTC:Domri Rade:domri rade').
card_rarity('domri rade'/'GTC', 'Mythic Rare').
card_artist('domri rade'/'GTC', 'Tyler Jacobson').
card_number('domri rade'/'GTC', '156').
card_multiverse_id('domri rade'/'GTC', '366367').

card_in_set('drakewing krasis', 'GTC').
card_original_type('drakewing krasis'/'GTC', 'Creature — Lizard Drake').
card_original_text('drakewing krasis'/'GTC', 'Flying, trample').
card_first_print('drakewing krasis', 'GTC').
card_image_name('drakewing krasis'/'GTC', 'drakewing krasis').
card_uid('drakewing krasis'/'GTC', 'GTC:Drakewing Krasis:drakewing krasis').
card_rarity('drakewing krasis'/'GTC', 'Common').
card_artist('drakewing krasis'/'GTC', 'Johann Bodin').
card_number('drakewing krasis'/'GTC', '157').
card_flavor_text('drakewing krasis'/'GTC', 'Cautious not to repeat Vig\'s mistakes, the Simic fuse life essences using magic alone. Each result is called a krasis.').
card_multiverse_id('drakewing krasis'/'GTC', '366292').
card_watermark('drakewing krasis'/'GTC', 'Simic').

card_in_set('duskmantle guildmage', 'GTC').
card_original_type('duskmantle guildmage'/'GTC', 'Creature — Human Wizard').
card_original_text('duskmantle guildmage'/'GTC', '{1}{U}{B}: Whenever a card is put into an opponent\'s graveyard from anywhere this turn, that player loses 1 life.\n{2}{U}{B}: Target player puts the top two cards of his or her library into his or her graveyard.').
card_first_print('duskmantle guildmage', 'GTC').
card_image_name('duskmantle guildmage'/'GTC', 'duskmantle guildmage').
card_uid('duskmantle guildmage'/'GTC', 'GTC:Duskmantle Guildmage:duskmantle guildmage').
card_rarity('duskmantle guildmage'/'GTC', 'Uncommon').
card_artist('duskmantle guildmage'/'GTC', 'Slawomir Maniak').
card_number('duskmantle guildmage'/'GTC', '158').
card_multiverse_id('duskmantle guildmage'/'GTC', '366328').
card_watermark('duskmantle guildmage'/'GTC', 'Dimir').

card_in_set('duskmantle seer', 'GTC').
card_original_type('duskmantle seer'/'GTC', 'Creature — Vampire Wizard').
card_original_text('duskmantle seer'/'GTC', 'Flying\nAt the beginning of your upkeep, each player reveals the top card of his or her library, loses life equal to that card\'s converted mana cost, then puts it into his or her hand.').
card_first_print('duskmantle seer', 'GTC').
card_image_name('duskmantle seer'/'GTC', 'duskmantle seer').
card_uid('duskmantle seer'/'GTC', 'GTC:Duskmantle Seer:duskmantle seer').
card_rarity('duskmantle seer'/'GTC', 'Mythic Rare').
card_artist('duskmantle seer'/'GTC', 'Kev Walker').
card_number('duskmantle seer'/'GTC', '159').
card_multiverse_id('duskmantle seer'/'GTC', '366310').
card_watermark('duskmantle seer'/'GTC', 'Dimir').

card_in_set('dutiful thrull', 'GTC').
card_original_type('dutiful thrull'/'GTC', 'Creature — Thrull').
card_original_text('dutiful thrull'/'GTC', '{B}: Regenerate Dutiful Thrull.').
card_first_print('dutiful thrull', 'GTC').
card_image_name('dutiful thrull'/'GTC', 'dutiful thrull').
card_uid('dutiful thrull'/'GTC', 'GTC:Dutiful Thrull:dutiful thrull').
card_rarity('dutiful thrull'/'GTC', 'Common').
card_artist('dutiful thrull'/'GTC', 'Daarken').
card_number('dutiful thrull'/'GTC', '11').
card_flavor_text('dutiful thrull'/'GTC', '\"They are useful, but there\'s little satisfaction in supplication that throws itself under your heel.\"\n—Jozica, Orzhov enforcer').
card_multiverse_id('dutiful thrull'/'GTC', '366457').
card_watermark('dutiful thrull'/'GTC', 'Orzhov').

card_in_set('dying wish', 'GTC').
card_original_type('dying wish'/'GTC', 'Enchantment — Aura').
card_original_text('dying wish'/'GTC', 'Enchant creature you control\nWhen enchanted creature dies, target player loses X life and you gain X life, where X is its power.').
card_first_print('dying wish', 'GTC').
card_image_name('dying wish'/'GTC', 'dying wish').
card_uid('dying wish'/'GTC', 'GTC:Dying Wish:dying wish').
card_rarity('dying wish'/'GTC', 'Uncommon').
card_artist('dying wish'/'GTC', 'Scott Chou').
card_number('dying wish'/'GTC', '64').
card_flavor_text('dying wish'/'GTC', '\"I wish I could rewrite my every deed and kill you with my first breath instead of my last.\"').
card_multiverse_id('dying wish'/'GTC', '366382').

card_in_set('elusive krasis', 'GTC').
card_original_type('elusive krasis'/'GTC', 'Creature — Fish Mutant').
card_original_text('elusive krasis'/'GTC', 'Elusive Krasis is unblockable.\nEvolve (Whenever a creature enters the battlefield under your control, if that creature has greater power or toughness than this creature, put a +1/+1 counter on this creature.)').
card_first_print('elusive krasis', 'GTC').
card_image_name('elusive krasis'/'GTC', 'elusive krasis').
card_uid('elusive krasis'/'GTC', 'GTC:Elusive Krasis:elusive krasis').
card_rarity('elusive krasis'/'GTC', 'Uncommon').
card_artist('elusive krasis'/'GTC', 'Wesley Burt').
card_number('elusive krasis'/'GTC', '160').
card_multiverse_id('elusive krasis'/'GTC', '366279').
card_watermark('elusive krasis'/'GTC', 'Simic').

card_in_set('ember beast', 'GTC').
card_original_type('ember beast'/'GTC', 'Creature — Beast').
card_original_text('ember beast'/'GTC', 'Ember Beast can\'t attack or block alone.').
card_image_name('ember beast'/'GTC', 'ember beast').
card_uid('ember beast'/'GTC', 'GTC:Ember Beast:ember beast').
card_rarity('ember beast'/'GTC', 'Common').
card_artist('ember beast'/'GTC', 'David Rapoza').
card_number('ember beast'/'GTC', '89').
card_flavor_text('ember beast'/'GTC', 'The only livestock that will survive for long in the Smelting District, ember beasts serve many functions—none of them willingly.').
card_multiverse_id('ember beast'/'GTC', '366306').

card_in_set('enter the infinite', 'GTC').
card_original_type('enter the infinite'/'GTC', 'Sorcery').
card_original_text('enter the infinite'/'GTC', 'Draw cards equal to the number of cards in your library, then put a card from your hand on top of your library. You have no maximum hand size until your next turn.').
card_first_print('enter the infinite', 'GTC').
card_image_name('enter the infinite'/'GTC', 'enter the infinite').
card_uid('enter the infinite'/'GTC', 'GTC:Enter the Infinite:enter the infinite').
card_rarity('enter the infinite'/'GTC', 'Mythic Rare').
card_artist('enter the infinite'/'GTC', 'Terese Nielsen').
card_number('enter the infinite'/'GTC', '34').
card_flavor_text('enter the infinite'/'GTC', '\"Don\'t just have an idea—have all of them.\"\n—Niv-Mizzet').
card_multiverse_id('enter the infinite'/'GTC', '366411').

card_in_set('executioner\'s swing', 'GTC').
card_original_type('executioner\'s swing'/'GTC', 'Instant').
card_original_text('executioner\'s swing'/'GTC', 'Target creature that dealt damage this turn gets -5/-5 until end of turn.').
card_first_print('executioner\'s swing', 'GTC').
card_image_name('executioner\'s swing'/'GTC', 'executioner\'s swing').
card_uid('executioner\'s swing'/'GTC', 'GTC:Executioner\'s Swing:executioner\'s swing').
card_rarity('executioner\'s swing'/'GTC', 'Common').
card_artist('executioner\'s swing'/'GTC', 'Karl Kopinski').
card_number('executioner\'s swing'/'GTC', '161').
card_flavor_text('executioner\'s swing'/'GTC', '\"The contract specified an appendage for a missed payment. Read the fine print: the head is an appendage.\"').
card_multiverse_id('executioner\'s swing'/'GTC', '366444').
card_watermark('executioner\'s swing'/'GTC', 'Orzhov').

card_in_set('experiment one', 'GTC').
card_original_type('experiment one'/'GTC', 'Creature — Human Ooze').
card_original_text('experiment one'/'GTC', 'Evolve (Whenever a creature enters the battlefield under your control, if that creature has greater power or toughness than this creature, put a +1/+1 counter on this creature.)\nRemove two +1/+1 counters from Experiment One: Regenerate Experiment One.').
card_image_name('experiment one'/'GTC', 'experiment one').
card_uid('experiment one'/'GTC', 'GTC:Experiment One:experiment one').
card_rarity('experiment one'/'GTC', 'Uncommon').
card_artist('experiment one'/'GTC', 'Chase Stone').
card_number('experiment one'/'GTC', '119').
card_multiverse_id('experiment one'/'GTC', '366441').
card_watermark('experiment one'/'GTC', 'Simic').

card_in_set('fathom mage', 'GTC').
card_original_type('fathom mage'/'GTC', 'Creature — Human Wizard').
card_original_text('fathom mage'/'GTC', 'Evolve (Whenever a creature enters the battlefield under your control, if that creature has greater power or toughness than this creature, put a +1/+1 counter on this creature.)\nWhenever a +1/+1 counter is placed on Fathom Mage, you may draw a card.').
card_image_name('fathom mage'/'GTC', 'fathom mage').
card_uid('fathom mage'/'GTC', 'GTC:Fathom Mage:fathom mage').
card_rarity('fathom mage'/'GTC', 'Rare').
card_artist('fathom mage'/'GTC', 'Ryan Pancoast').
card_number('fathom mage'/'GTC', '162').
card_multiverse_id('fathom mage'/'GTC', '366399').
card_watermark('fathom mage'/'GTC', 'Simic').

card_in_set('firefist striker', 'GTC').
card_original_type('firefist striker'/'GTC', 'Creature — Human Soldier').
card_original_text('firefist striker'/'GTC', 'Battalion — Whenever Firefist Striker and at least two other creatures attack, target creature can\'t block this turn.').
card_first_print('firefist striker', 'GTC').
card_image_name('firefist striker'/'GTC', 'firefist striker').
card_uid('firefist striker'/'GTC', 'GTC:Firefist Striker:firefist striker').
card_rarity('firefist striker'/'GTC', 'Uncommon').
card_artist('firefist striker'/'GTC', 'Tyler Jacobson').
card_number('firefist striker'/'GTC', '90').
card_flavor_text('firefist striker'/'GTC', 'The flaming fist of Boros holds the promise of both protection and destruction in its grasp.').
card_multiverse_id('firefist striker'/'GTC', '366311').
card_watermark('firefist striker'/'GTC', 'Boros').

card_in_set('firemane avenger', 'GTC').
card_original_type('firemane avenger'/'GTC', 'Creature — Angel').
card_original_text('firemane avenger'/'GTC', 'Flying\nBattalion — Whenever Firemane Avenger and at least two other creatures attack, Firemane Avenger deals 3 damage to target creature or player and you gain 3 life.').
card_image_name('firemane avenger'/'GTC', 'firemane avenger').
card_uid('firemane avenger'/'GTC', 'GTC:Firemane Avenger:firemane avenger').
card_rarity('firemane avenger'/'GTC', 'Rare').
card_artist('firemane avenger'/'GTC', 'Wayne Reynolds').
card_number('firemane avenger'/'GTC', '163').
card_flavor_text('firemane avenger'/'GTC', 'Firemane Nevena led the victorious assault on the Barbu Rooftop rebels. Now the Gateless have marked her for death.').
card_multiverse_id('firemane avenger'/'GTC', '366341').
card_watermark('firemane avenger'/'GTC', 'Boros').

card_in_set('five-alarm fire', 'GTC').
card_original_type('five-alarm fire'/'GTC', 'Enchantment').
card_original_text('five-alarm fire'/'GTC', 'Whenever a creature you control deals combat damage, put a blaze counter on Five-Alarm Fire.\nRemove five blaze counters from Five-Alarm Fire: Five-Alarm Fire deals 5 damage to target creature or player.').
card_first_print('five-alarm fire', 'GTC').
card_image_name('five-alarm fire'/'GTC', 'five-alarm fire').
card_uid('five-alarm fire'/'GTC', 'GTC:Five-Alarm Fire:five-alarm fire').
card_rarity('five-alarm fire'/'GTC', 'Rare').
card_artist('five-alarm fire'/'GTC', 'Karl Kopinski').
card_number('five-alarm fire'/'GTC', '91').
card_multiverse_id('five-alarm fire'/'GTC', '366386').

card_in_set('forced adaptation', 'GTC').
card_original_type('forced adaptation'/'GTC', 'Enchantment — Aura').
card_original_text('forced adaptation'/'GTC', 'Enchant creature\nAt the beginning of your upkeep, put a +1/+1 counter on enchanted creature.').
card_first_print('forced adaptation', 'GTC').
card_image_name('forced adaptation'/'GTC', 'forced adaptation').
card_uid('forced adaptation'/'GTC', 'GTC:Forced Adaptation:forced adaptation').
card_rarity('forced adaptation'/'GTC', 'Common').
card_artist('forced adaptation'/'GTC', 'Trevor Claxton').
card_number('forced adaptation'/'GTC', '120').
card_flavor_text('forced adaptation'/'GTC', '\"We\'re planting the seeds of change. Sometimes, we plant them in people.\"\n—Vorel of the Hull Clade').
card_multiverse_id('forced adaptation'/'GTC', '366336').

card_in_set('fortress cyclops', 'GTC').
card_original_type('fortress cyclops'/'GTC', 'Creature — Cyclops Soldier').
card_original_text('fortress cyclops'/'GTC', 'Whenever Fortress Cyclops attacks, it gets +3/+0 until end of turn.\nWhenever Fortress Cyclops blocks, it gets +0/+3 until end of turn.').
card_first_print('fortress cyclops', 'GTC').
card_image_name('fortress cyclops'/'GTC', 'fortress cyclops').
card_uid('fortress cyclops'/'GTC', 'GTC:Fortress Cyclops:fortress cyclops').
card_rarity('fortress cyclops'/'GTC', 'Uncommon').
card_artist('fortress cyclops'/'GTC', 'Maciej Kuciara').
card_number('fortress cyclops'/'GTC', '164').
card_flavor_text('fortress cyclops'/'GTC', '\"They think they tamed him, but he will always be a wild titan, a force of nature.\"\n—Nedja, Gruul shaman').
card_multiverse_id('fortress cyclops'/'GTC', '366280').
card_watermark('fortress cyclops'/'GTC', 'Boros').

card_in_set('foundry champion', 'GTC').
card_original_type('foundry champion'/'GTC', 'Creature — Elemental Soldier').
card_original_text('foundry champion'/'GTC', 'When Foundry Champion enters the battlefield, it deals damage to target creature or player equal to the number of creatures you control.\n{R}: Foundry Champion gets +1/+0 until end of turn.\n{W}: Foundry Champion gets +0/+1 until end of turn.').
card_image_name('foundry champion'/'GTC', 'foundry champion').
card_uid('foundry champion'/'GTC', 'GTC:Foundry Champion:foundry champion').
card_rarity('foundry champion'/'GTC', 'Rare').
card_artist('foundry champion'/'GTC', 'Todd Lockwood').
card_number('foundry champion'/'GTC', '165').
card_multiverse_id('foundry champion'/'GTC', '366431').
card_watermark('foundry champion'/'GTC', 'Boros').

card_in_set('foundry street denizen', 'GTC').
card_original_type('foundry street denizen'/'GTC', 'Creature — Goblin Warrior').
card_original_text('foundry street denizen'/'GTC', 'Whenever another red creature enters the battlefield under your control, Foundry Street Denizen gets +1/+0 until end of turn.').
card_first_print('foundry street denizen', 'GTC').
card_image_name('foundry street denizen'/'GTC', 'foundry street denizen').
card_uid('foundry street denizen'/'GTC', 'GTC:Foundry Street Denizen:foundry street denizen').
card_rarity('foundry street denizen'/'GTC', 'Common').
card_artist('foundry street denizen'/'GTC', 'Raoul Vitale').
card_number('foundry street denizen'/'GTC', '92').
card_flavor_text('foundry street denizen'/'GTC', 'After the Foundry Street riot, Arrester Hulbein tried to ban bludgeons. Which, inevitably, resulted in another riot.').
card_multiverse_id('foundry street denizen'/'GTC', '366472').

card_in_set('frenzied tilling', 'GTC').
card_original_type('frenzied tilling'/'GTC', 'Sorcery').
card_original_text('frenzied tilling'/'GTC', 'Destroy target land. Search your library for a basic land card and put that card onto the battlefield tapped. Then shuffle your library.').
card_image_name('frenzied tilling'/'GTC', 'frenzied tilling').
card_uid('frenzied tilling'/'GTC', 'GTC:Frenzied Tilling:frenzied tilling').
card_rarity('frenzied tilling'/'GTC', 'Uncommon').
card_artist('frenzied tilling'/'GTC', 'Noah Bradley').
card_number('frenzied tilling'/'GTC', '166').
card_flavor_text('frenzied tilling'/'GTC', '\"Izzet contraptions pollute our lands. Burn them down, kill the makers, cleanse the earth.\"\n—Nedja, Gruul shaman').
card_multiverse_id('frenzied tilling'/'GTC', '366295').
card_watermark('frenzied tilling'/'GTC', 'Gruul').

card_in_set('frilled oculus', 'GTC').
card_original_type('frilled oculus'/'GTC', 'Creature — Homunculus').
card_original_text('frilled oculus'/'GTC', '{1}{G}: Frilled Oculus gets +2/+2 until end of turn. Activate this ability only once each turn.').
card_first_print('frilled oculus', 'GTC').
card_image_name('frilled oculus'/'GTC', 'frilled oculus').
card_uid('frilled oculus'/'GTC', 'GTC:Frilled Oculus:frilled oculus').
card_rarity('frilled oculus'/'GTC', 'Common').
card_artist('frilled oculus'/'GTC', 'Marco Nelor').
card_number('frilled oculus'/'GTC', '35').
card_flavor_text('frilled oculus'/'GTC', '\"Understand the Simic? That\'s about as easy as winning a staring contest with a homunculus.\"\n—Dars Gostok, Firefist captain').
card_multiverse_id('frilled oculus'/'GTC', '366298').
card_watermark('frilled oculus'/'GTC', 'Simic').

card_in_set('frontline medic', 'GTC').
card_original_type('frontline medic'/'GTC', 'Creature — Human Cleric').
card_original_text('frontline medic'/'GTC', 'Battalion — Whenever Frontline Medic and at least two other creatures attack, creatures you control are indestructible this turn.\nSacrifice Frontline Medic: Counter target spell with {X} in its mana cost unless its controller pays {3}.').
card_first_print('frontline medic', 'GTC').
card_image_name('frontline medic'/'GTC', 'frontline medic').
card_uid('frontline medic'/'GTC', 'GTC:Frontline Medic:frontline medic').
card_rarity('frontline medic'/'GTC', 'Rare').
card_artist('frontline medic'/'GTC', 'Willian Murai').
card_number('frontline medic'/'GTC', '12').
card_multiverse_id('frontline medic'/'GTC', '366460').
card_watermark('frontline medic'/'GTC', 'Boros').

card_in_set('furious resistance', 'GTC').
card_original_type('furious resistance'/'GTC', 'Instant').
card_original_text('furious resistance'/'GTC', 'Target blocking creature gets +3/+0 and gains first strike until end of turn.').
card_first_print('furious resistance', 'GTC').
card_image_name('furious resistance'/'GTC', 'furious resistance').
card_uid('furious resistance'/'GTC', 'GTC:Furious Resistance:furious resistance').
card_rarity('furious resistance'/'GTC', 'Common').
card_artist('furious resistance'/'GTC', 'Slawomir Maniak').
card_number('furious resistance'/'GTC', '93').
card_flavor_text('furious resistance'/'GTC', '\"At least the Gateless rebels have ideals. Rakdos just mocks, defiles, and kills all we hold dear.\"\n—Libuse, Boros sergeant').
card_multiverse_id('furious resistance'/'GTC', '366309').

card_in_set('gateway shade', 'GTC').
card_original_type('gateway shade'/'GTC', 'Creature — Shade').
card_original_text('gateway shade'/'GTC', '{B}: Gateway Shade gets +1/+1 until end of turn.\nTap an untapped Gate you control: Gateway Shade gets +2/+2 until end of turn.').
card_first_print('gateway shade', 'GTC').
card_image_name('gateway shade'/'GTC', 'gateway shade').
card_uid('gateway shade'/'GTC', 'GTC:Gateway Shade:gateway shade').
card_rarity('gateway shade'/'GTC', 'Uncommon').
card_artist('gateway shade'/'GTC', 'Ryan Yee').
card_number('gateway shade'/'GTC', '65').
card_flavor_text('gateway shade'/'GTC', 'Tireless and merciless, shades are harnessed as spies by the Dimir.').
card_multiverse_id('gateway shade'/'GTC', '366438').

card_in_set('ghor-clan rampager', 'GTC').
card_original_type('ghor-clan rampager'/'GTC', 'Creature — Beast').
card_original_text('ghor-clan rampager'/'GTC', 'Trample\nBloodrush — {R}{G}, Discard Ghor-Clan Rampager: Target attacking creature gets +4/+4 and gains trample until end of turn.').
card_image_name('ghor-clan rampager'/'GTC', 'ghor-clan rampager').
card_uid('ghor-clan rampager'/'GTC', 'GTC:Ghor-Clan Rampager:ghor-clan rampager').
card_rarity('ghor-clan rampager'/'GTC', 'Uncommon').
card_artist('ghor-clan rampager'/'GTC', 'Charles Urbach').
card_number('ghor-clan rampager'/'GTC', '167').
card_flavor_text('ghor-clan rampager'/'GTC', '\"The Simic come from the cold slow depths. How could they understand the fire in a wild heart?\"\n—Kroshkar, Gruul shaman').
card_multiverse_id('ghor-clan rampager'/'GTC', '366287').
card_watermark('ghor-clan rampager'/'GTC', 'Gruul').

card_in_set('giant adephage', 'GTC').
card_original_type('giant adephage'/'GTC', 'Creature — Insect').
card_original_text('giant adephage'/'GTC', 'Trample\nWhenever Giant Adephage deals combat damage to a player, put a token onto the battlefield that\'s a copy of Giant Adephage.').
card_first_print('giant adephage', 'GTC').
card_image_name('giant adephage'/'GTC', 'giant adephage').
card_uid('giant adephage'/'GTC', 'GTC:Giant Adephage:giant adephage').
card_rarity('giant adephage'/'GTC', 'Mythic Rare').
card_artist('giant adephage'/'GTC', 'Christine Choi').
card_number('giant adephage'/'GTC', '121').
card_flavor_text('giant adephage'/'GTC', '\"To a creature like that, we must seem like, well, bugs.\"\n—Dars Gostok, Firefist captain').
card_multiverse_id('giant adephage'/'GTC', '366394').

card_in_set('gideon, champion of justice', 'GTC').
card_original_type('gideon, champion of justice'/'GTC', 'Planeswalker — Gideon').
card_original_text('gideon, champion of justice'/'GTC', '+1: Put a loyalty counter on Gideon, Champion of Justice for each creature target opponent controls.\n0: Until end of turn, Gideon, Champion of Justice becomes an indestructible Human Soldier creature with power and toughness each equal to the number of loyalty counters on him. He\'s still a planeswalker. Prevent all damage that would be dealt to him this turn.\n-15: Exile all other permanents.').
card_first_print('gideon, champion of justice', 'GTC').
card_image_name('gideon, champion of justice'/'GTC', 'gideon, champion of justice').
card_uid('gideon, champion of justice'/'GTC', 'GTC:Gideon, Champion of Justice:gideon, champion of justice').
card_rarity('gideon, champion of justice'/'GTC', 'Mythic Rare').
card_artist('gideon, champion of justice'/'GTC', 'David Rapoza').
card_number('gideon, champion of justice'/'GTC', '13').
card_multiverse_id('gideon, champion of justice'/'GTC', '366345').

card_in_set('gift of orzhova', 'GTC').
card_original_type('gift of orzhova'/'GTC', 'Enchantment — Aura').
card_original_text('gift of orzhova'/'GTC', 'Enchant creature\nEnchanted creature gets +1/+1 and has flying and lifelink.').
card_first_print('gift of orzhova', 'GTC').
card_image_name('gift of orzhova'/'GTC', 'gift of orzhova').
card_uid('gift of orzhova'/'GTC', 'GTC:Gift of Orzhova:gift of orzhova').
card_rarity('gift of orzhova'/'GTC', 'Uncommon').
card_artist('gift of orzhova'/'GTC', 'Johannes Voss').
card_number('gift of orzhova'/'GTC', '219').
card_flavor_text('gift of orzhova'/'GTC', '\"Remember by whose gift you ascend.\"\n—Milana, Orzhov prelate').
card_multiverse_id('gift of orzhova'/'GTC', '366339').
card_watermark('gift of orzhova'/'GTC', 'Orzhov').

card_in_set('glaring spotlight', 'GTC').
card_original_type('glaring spotlight'/'GTC', 'Artifact').
card_original_text('glaring spotlight'/'GTC', 'Creatures your opponents control with hexproof can be the targets of spells and abilities you control as though they didn\'t have hexproof.\n{3}, Sacrifice Glaring Spotlight: Creatures you control gain hexproof until end of turn and are unblockable this turn.').
card_first_print('glaring spotlight', 'GTC').
card_image_name('glaring spotlight'/'GTC', 'glaring spotlight').
card_uid('glaring spotlight'/'GTC', 'GTC:Glaring Spotlight:glaring spotlight').
card_rarity('glaring spotlight'/'GTC', 'Rare').
card_artist('glaring spotlight'/'GTC', 'Adam Paquette').
card_number('glaring spotlight'/'GTC', '229').
card_multiverse_id('glaring spotlight'/'GTC', '366334').

card_in_set('godless shrine', 'GTC').
card_original_type('godless shrine'/'GTC', 'Land — Plains Swamp').
card_original_text('godless shrine'/'GTC', '({T}: Add {W} or {B} to your mana pool.)\nAs Godless Shrine enters the battlefield, you may pay 2 life. If you don\'t, Godless Shrine enters the battlefield tapped.').
card_image_name('godless shrine'/'GTC', 'godless shrine').
card_uid('godless shrine'/'GTC', 'GTC:Godless Shrine:godless shrine').
card_rarity('godless shrine'/'GTC', 'Rare').
card_artist('godless shrine'/'GTC', 'Cliff Childs').
card_number('godless shrine'/'GTC', '242').
card_flavor_text('godless shrine'/'GTC', 'Judgment without hope.').
card_multiverse_id('godless shrine'/'GTC', '366302').
card_watermark('godless shrine'/'GTC', 'Orzhov').

card_in_set('greenside watcher', 'GTC').
card_original_type('greenside watcher'/'GTC', 'Creature — Elf Druid').
card_original_text('greenside watcher'/'GTC', '{T}: Untap target Gate.').
card_first_print('greenside watcher', 'GTC').
card_image_name('greenside watcher'/'GTC', 'greenside watcher').
card_uid('greenside watcher'/'GTC', 'GTC:Greenside Watcher:greenside watcher').
card_rarity('greenside watcher'/'GTC', 'Common').
card_artist('greenside watcher'/'GTC', 'Ryan Barger').
card_number('greenside watcher'/'GTC', '122').
card_flavor_text('greenside watcher'/'GTC', '\"The Izzet may incite war, but my duty is here. I will not abandon the wild.\"').
card_multiverse_id('greenside watcher'/'GTC', '366440').

card_in_set('gridlock', 'GTC').
card_original_type('gridlock'/'GTC', 'Instant').
card_original_text('gridlock'/'GTC', 'Tap X target nonland permanents.').
card_first_print('gridlock', 'GTC').
card_image_name('gridlock'/'GTC', 'gridlock').
card_uid('gridlock'/'GTC', 'GTC:Gridlock:gridlock').
card_rarity('gridlock'/'GTC', 'Uncommon').
card_artist('gridlock'/'GTC', 'Yeong-Hao Han').
card_number('gridlock'/'GTC', '36').
card_flavor_text('gridlock'/'GTC', '\"One tiny peg pulled from a single wagon wheel, and an entire district grinds to a halt.\"\n—Lazav').
card_multiverse_id('gridlock'/'GTC', '366330').

card_in_set('grisly spectacle', 'GTC').
card_original_type('grisly spectacle'/'GTC', 'Instant').
card_original_text('grisly spectacle'/'GTC', 'Destroy target nonartifact creature. Its controller puts a number of cards equal to that creature\'s power from the top of his or her library into his or her graveyard.').
card_first_print('grisly spectacle', 'GTC').
card_image_name('grisly spectacle'/'GTC', 'grisly spectacle').
card_uid('grisly spectacle'/'GTC', 'GTC:Grisly Spectacle:grisly spectacle').
card_rarity('grisly spectacle'/'GTC', 'Common').
card_artist('grisly spectacle'/'GTC', 'Zoltan Boros').
card_number('grisly spectacle'/'GTC', '66').
card_flavor_text('grisly spectacle'/'GTC', '\"Watch people flock to a murder scene. Then tell me we\'re not all a little sick in the head.\"\n—Juri, proprietor of the Juri Revue').
card_multiverse_id('grisly spectacle'/'GTC', '366396').

card_in_set('ground assault', 'GTC').
card_original_type('ground assault'/'GTC', 'Sorcery').
card_original_text('ground assault'/'GTC', 'Ground Assault deals damage to target creature equal to the number of lands you control.').
card_first_print('ground assault', 'GTC').
card_image_name('ground assault'/'GTC', 'ground assault').
card_uid('ground assault'/'GTC', 'GTC:Ground Assault:ground assault').
card_rarity('ground assault'/'GTC', 'Uncommon').
card_artist('ground assault'/'GTC', 'Karl Kopinski').
card_number('ground assault'/'GTC', '168').
card_flavor_text('ground assault'/'GTC', '\"Taking down a dragon is easy. All you need is enough space to get in a good swing.\"\n—Narbulg Nine Fingers').
card_multiverse_id('ground assault'/'GTC', '366428').
card_watermark('ground assault'/'GTC', 'Gruul').

card_in_set('gruul charm', 'GTC').
card_original_type('gruul charm'/'GTC', 'Instant').
card_original_text('gruul charm'/'GTC', 'Choose one — Creatures without flying can\'t block this turn; or gain control of all permanents you own; or Gruul Charm deals 3 damage to each creature with flying.').
card_first_print('gruul charm', 'GTC').
card_image_name('gruul charm'/'GTC', 'gruul charm').
card_uid('gruul charm'/'GTC', 'GTC:Gruul Charm:gruul charm').
card_rarity('gruul charm'/'GTC', 'Uncommon').
card_artist('gruul charm'/'GTC', 'Zoltan Boros').
card_number('gruul charm'/'GTC', '169').
card_flavor_text('gruul charm'/'GTC', '\"Not Gruul? Then die!\"\n—Borborygmos').
card_multiverse_id('gruul charm'/'GTC', '366360').
card_watermark('gruul charm'/'GTC', 'Gruul').

card_in_set('gruul guildgate', 'GTC').
card_original_type('gruul guildgate'/'GTC', 'Land — Gate').
card_original_text('gruul guildgate'/'GTC', 'Gruul Guildgate enters the battlefield tapped.\n{T}: Add {R} or {G} to your mana pool.').
card_first_print('gruul guildgate', 'GTC').
card_image_name('gruul guildgate'/'GTC', 'gruul guildgate').
card_uid('gruul guildgate'/'GTC', 'GTC:Gruul Guildgate:gruul guildgate').
card_rarity('gruul guildgate'/'GTC', 'Common').
card_artist('gruul guildgate'/'GTC', 'Randy Gallegos').
card_number('gruul guildgate'/'GTC', '243').
card_flavor_text('gruul guildgate'/'GTC', 'Enter and leave the shackles of society behind.').
card_multiverse_id('gruul guildgate'/'GTC', '366253').
card_watermark('gruul guildgate'/'GTC', 'Gruul').

card_in_set('gruul keyrune', 'GTC').
card_original_type('gruul keyrune'/'GTC', 'Artifact').
card_original_text('gruul keyrune'/'GTC', '{T}: Add {R} or {G} to your mana pool.\n{R}{G}: Gruul Keyrune becomes a 3/2 red and green Beast artifact creature with trample until end of turn.').
card_first_print('gruul keyrune', 'GTC').
card_image_name('gruul keyrune'/'GTC', 'gruul keyrune').
card_uid('gruul keyrune'/'GTC', 'GTC:Gruul Keyrune:gruul keyrune').
card_rarity('gruul keyrune'/'GTC', 'Uncommon').
card_artist('gruul keyrune'/'GTC', 'Daniel Ljunggren').
card_number('gruul keyrune'/'GTC', '230').
card_flavor_text('gruul keyrune'/'GTC', 'As perilous and unpredictable as the wilds themselves.').
card_multiverse_id('gruul keyrune'/'GTC', '366443').
card_watermark('gruul keyrune'/'GTC', 'Gruul').

card_in_set('gruul ragebeast', 'GTC').
card_original_type('gruul ragebeast'/'GTC', 'Creature — Beast').
card_original_text('gruul ragebeast'/'GTC', 'Whenever Gruul Ragebeast or another creature enters the battlefield under your control, that creature fights target creature an opponent controls.').
card_first_print('gruul ragebeast', 'GTC').
card_image_name('gruul ragebeast'/'GTC', 'gruul ragebeast').
card_uid('gruul ragebeast'/'GTC', 'GTC:Gruul Ragebeast:gruul ragebeast').
card_rarity('gruul ragebeast'/'GTC', 'Rare').
card_artist('gruul ragebeast'/'GTC', 'Dave Kendall').
card_number('gruul ragebeast'/'GTC', '170').
card_flavor_text('gruul ragebeast'/'GTC', '\"If the other guilds want a war, they should know we\'ve been ready the whole time.\"\n—Ruric Thar, Ghor-Clan chieftain').
card_multiverse_id('gruul ragebeast'/'GTC', '366398').
card_watermark('gruul ragebeast'/'GTC', 'Gruul').

card_in_set('guardian of the gateless', 'GTC').
card_original_type('guardian of the gateless'/'GTC', 'Creature — Angel').
card_original_text('guardian of the gateless'/'GTC', 'Flying\nGuardian of the Gateless can block any number of creatures.\nWhenever Guardian of the Gateless blocks, it gets +1/+1 until end of turn for each creature it\'s blocking.').
card_first_print('guardian of the gateless', 'GTC').
card_image_name('guardian of the gateless'/'GTC', 'guardian of the gateless').
card_uid('guardian of the gateless'/'GTC', 'GTC:Guardian of the Gateless:guardian of the gateless').
card_rarity('guardian of the gateless'/'GTC', 'Uncommon').
card_artist('guardian of the gateless'/'GTC', 'Wesley Burt').
card_number('guardian of the gateless'/'GTC', '14').
card_multiverse_id('guardian of the gateless'/'GTC', '366260').

card_in_set('guildscorn ward', 'GTC').
card_original_type('guildscorn ward'/'GTC', 'Enchantment — Aura').
card_original_text('guildscorn ward'/'GTC', 'Enchant creature\nEnchanted creature has protection from multicolored.').
card_first_print('guildscorn ward', 'GTC').
card_image_name('guildscorn ward'/'GTC', 'guildscorn ward').
card_uid('guildscorn ward'/'GTC', 'GTC:Guildscorn Ward:guildscorn ward').
card_rarity('guildscorn ward'/'GTC', 'Common').
card_artist('guildscorn ward'/'GTC', 'Ryan Barger').
card_number('guildscorn ward'/'GTC', '15').
card_flavor_text('guildscorn ward'/'GTC', '\"The Gateless have risen. The Ten shall fall.\"\n—Ninth District graffiti').
card_multiverse_id('guildscorn ward'/'GTC', '366392').

card_in_set('gutter skulk', 'GTC').
card_original_type('gutter skulk'/'GTC', 'Creature — Zombie Rat').
card_original_text('gutter skulk'/'GTC', '').
card_first_print('gutter skulk', 'GTC').
card_image_name('gutter skulk'/'GTC', 'gutter skulk').
card_uid('gutter skulk'/'GTC', 'GTC:Gutter Skulk:gutter skulk').
card_rarity('gutter skulk'/'GTC', 'Common').
card_artist('gutter skulk'/'GTC', 'Mark Winters').
card_number('gutter skulk'/'GTC', '67').
card_flavor_text('gutter skulk'/'GTC', 'Upon finding his warehouse infested, Gaven didn\'t know whether to get an exterminator or an exorcist.').
card_multiverse_id('gutter skulk'/'GTC', '366234').

card_in_set('gyre sage', 'GTC').
card_original_type('gyre sage'/'GTC', 'Creature — Elf Druid').
card_original_text('gyre sage'/'GTC', 'Evolve (Whenever a creature enters the battlefield under your control, if that creature has greater power or toughness than this creature, put a +1/+1 counter on this creature.)\n{T}: Add {G} to your mana pool for each +1/+1 counter on Gyre Sage.').
card_first_print('gyre sage', 'GTC').
card_image_name('gyre sage'/'GTC', 'gyre sage').
card_uid('gyre sage'/'GTC', 'GTC:Gyre Sage:gyre sage').
card_rarity('gyre sage'/'GTC', 'Rare').
card_artist('gyre sage'/'GTC', 'Tyler Jacobson').
card_number('gyre sage'/'GTC', '123').
card_multiverse_id('gyre sage'/'GTC', '366289').
card_watermark('gyre sage'/'GTC', 'Simic').

card_in_set('hands of binding', 'GTC').
card_original_type('hands of binding'/'GTC', 'Sorcery').
card_original_text('hands of binding'/'GTC', 'Tap target creature an opponent controls. That creature doesn\'t untap during its controller\'s next untap step.\nCipher (Then you may exile this spell card encoded on a creature you control. Whenever that creature deals combat damage to a player, its controller may cast a copy of the encoded card without paying its mana cost.)').
card_first_print('hands of binding', 'GTC').
card_image_name('hands of binding'/'GTC', 'hands of binding').
card_uid('hands of binding'/'GTC', 'GTC:Hands of Binding:hands of binding').
card_rarity('hands of binding'/'GTC', 'Common').
card_artist('hands of binding'/'GTC', 'Raymond Swanland').
card_number('hands of binding'/'GTC', '37').
card_multiverse_id('hands of binding'/'GTC', '366445').
card_watermark('hands of binding'/'GTC', 'Dimir').

card_in_set('hellkite tyrant', 'GTC').
card_original_type('hellkite tyrant'/'GTC', 'Creature — Dragon').
card_original_text('hellkite tyrant'/'GTC', 'Flying, trample\nWhenever Hellkite Tyrant deals combat damage to a player, gain control of all artifacts that player controls.\nAt the beginning of your upkeep, if you control twenty or more artifacts, you win the game.').
card_first_print('hellkite tyrant', 'GTC').
card_image_name('hellkite tyrant'/'GTC', 'hellkite tyrant').
card_uid('hellkite tyrant'/'GTC', 'GTC:Hellkite Tyrant:hellkite tyrant').
card_rarity('hellkite tyrant'/'GTC', 'Mythic Rare').
card_artist('hellkite tyrant'/'GTC', 'Aleksi Briclot').
card_number('hellkite tyrant'/'GTC', '94').
card_multiverse_id('hellkite tyrant'/'GTC', '366244').

card_in_set('hellraiser goblin', 'GTC').
card_original_type('hellraiser goblin'/'GTC', 'Creature — Goblin Berserker').
card_original_text('hellraiser goblin'/'GTC', 'Creatures you control have haste and attack each combat if able.').
card_first_print('hellraiser goblin', 'GTC').
card_image_name('hellraiser goblin'/'GTC', 'hellraiser goblin').
card_uid('hellraiser goblin'/'GTC', 'GTC:Hellraiser Goblin:hellraiser goblin').
card_rarity('hellraiser goblin'/'GTC', 'Uncommon').
card_artist('hellraiser goblin'/'GTC', 'Karl Kopinski').
card_number('hellraiser goblin'/'GTC', '95').
card_flavor_text('hellraiser goblin'/'GTC', 'Don\'t let him lead the soldiers, but by all means let him lead the way.').
card_multiverse_id('hellraiser goblin'/'GTC', '366342').

card_in_set('high priest of penance', 'GTC').
card_original_type('high priest of penance'/'GTC', 'Creature — Human Cleric').
card_original_text('high priest of penance'/'GTC', 'Whenever High Priest of Penance is dealt damage, you may destroy target nonland permanent.').
card_first_print('high priest of penance', 'GTC').
card_image_name('high priest of penance'/'GTC', 'high priest of penance').
card_uid('high priest of penance'/'GTC', 'GTC:High Priest of Penance:high priest of penance').
card_rarity('high priest of penance'/'GTC', 'Rare').
card_artist('high priest of penance'/'GTC', 'Mark Zug').
card_number('high priest of penance'/'GTC', '171').
card_flavor_text('high priest of penance'/'GTC', '\"All I require is faith, loyalty, obedience, trust, and complete and utter devotion.\"').
card_multiverse_id('high priest of penance'/'GTC', '366266').
card_watermark('high priest of penance'/'GTC', 'Orzhov').

card_in_set('hindervines', 'GTC').
card_original_type('hindervines'/'GTC', 'Instant').
card_original_text('hindervines'/'GTC', 'Prevent all combat damage that would be dealt this turn by creatures with no +1/+1 counters on them.').
card_first_print('hindervines', 'GTC').
card_image_name('hindervines'/'GTC', 'hindervines').
card_uid('hindervines'/'GTC', 'GTC:Hindervines:hindervines').
card_rarity('hindervines'/'GTC', 'Uncommon').
card_artist('hindervines'/'GTC', 'Svetlin Velinov').
card_number('hindervines'/'GTC', '124').
card_flavor_text('hindervines'/'GTC', '\"Woodlands and laws. Both grow faster than the Azorius\'s ability to manage them.\"\n—Suniel the Woodwise').
card_multiverse_id('hindervines'/'GTC', '366459').

card_in_set('hold the gates', 'GTC').
card_original_type('hold the gates'/'GTC', 'Enchantment').
card_original_text('hold the gates'/'GTC', 'Creatures you control get +0/+1 for each Gate you control and have vigilance.').
card_first_print('hold the gates', 'GTC').
card_image_name('hold the gates'/'GTC', 'hold the gates').
card_uid('hold the gates'/'GTC', 'GTC:Hold the Gates:hold the gates').
card_rarity('hold the gates'/'GTC', 'Uncommon').
card_artist('hold the gates'/'GTC', 'Zoltan Boros').
card_number('hold the gates'/'GTC', '16').
card_flavor_text('hold the gates'/'GTC', '\"We grew up on these streets. We know them better than any Boros grunt from Zelzo.\"').
card_multiverse_id('hold the gates'/'GTC', '366276').

card_in_set('holy mantle', 'GTC').
card_original_type('holy mantle'/'GTC', 'Enchantment — Aura').
card_original_text('holy mantle'/'GTC', 'Enchant creature\nEnchanted creature gets +2/+2 and has protection from creatures.').
card_first_print('holy mantle', 'GTC').
card_image_name('holy mantle'/'GTC', 'holy mantle').
card_uid('holy mantle'/'GTC', 'GTC:Holy Mantle:holy mantle').
card_rarity('holy mantle'/'GTC', 'Uncommon').
card_artist('holy mantle'/'GTC', 'Maciej Kuciara').
card_number('holy mantle'/'GTC', '17').
card_flavor_text('holy mantle'/'GTC', '\"The guilds promise to provide safety. I prefer to make my own.\"\n—Briska, Ninth District freemage').
card_multiverse_id('holy mantle'/'GTC', '366368').

card_in_set('homing lightning', 'GTC').
card_original_type('homing lightning'/'GTC', 'Instant').
card_original_text('homing lightning'/'GTC', 'Homing Lightning deals 4 damage to target creature and each other creature with the same name as that creature.').
card_first_print('homing lightning', 'GTC').
card_image_name('homing lightning'/'GTC', 'homing lightning').
card_uid('homing lightning'/'GTC', 'GTC:Homing Lightning:homing lightning').
card_rarity('homing lightning'/'GTC', 'Uncommon').
card_artist('homing lightning'/'GTC', 'Slawomir Maniak').
card_number('homing lightning'/'GTC', '96').
card_flavor_text('homing lightning'/'GTC', '\"I see countless worlds. What makes them think I can\'t find them in just one?\"\n—Ral Zarek').
card_multiverse_id('homing lightning'/'GTC', '366297').

card_in_set('horror of the dim', 'GTC').
card_original_type('horror of the dim'/'GTC', 'Creature — Horror').
card_original_text('horror of the dim'/'GTC', '{U}: Horror of the Dim gains hexproof until end of turn. (It can\'t be the target of spells or abilities your opponents control.)').
card_first_print('horror of the dim', 'GTC').
card_image_name('horror of the dim'/'GTC', 'horror of the dim').
card_uid('horror of the dim'/'GTC', 'GTC:Horror of the Dim:horror of the dim').
card_rarity('horror of the dim'/'GTC', 'Common').
card_artist('horror of the dim'/'GTC', 'Jack Wang').
card_number('horror of the dim'/'GTC', '68').
card_flavor_text('horror of the dim'/'GTC', '\"Lesser minds can scarcely imagine such a marriage of necromancy and mind magic.\"\n—Strava, Dimir mage').
card_multiverse_id('horror of the dim'/'GTC', '366397').
card_watermark('horror of the dim'/'GTC', 'Dimir').

card_in_set('hydroform', 'GTC').
card_original_type('hydroform'/'GTC', 'Instant').
card_original_text('hydroform'/'GTC', 'Target land becomes a 3/3 Elemental creature with flying until end of turn. It\'s still a land.').
card_first_print('hydroform', 'GTC').
card_image_name('hydroform'/'GTC', 'hydroform').
card_uid('hydroform'/'GTC', 'GTC:Hydroform:hydroform').
card_rarity('hydroform'/'GTC', 'Common').
card_artist('hydroform'/'GTC', 'Howard Lyon').
card_number('hydroform'/'GTC', '172').
card_flavor_text('hydroform'/'GTC', '\"While the sea may be our home, the sky is our destiny.\"\n—Prime Speaker Zegana').
card_multiverse_id('hydroform'/'GTC', '366423').
card_watermark('hydroform'/'GTC', 'Simic').

card_in_set('illness in the ranks', 'GTC').
card_original_type('illness in the ranks'/'GTC', 'Enchantment').
card_original_text('illness in the ranks'/'GTC', 'Creature tokens get -1/-1.').
card_first_print('illness in the ranks', 'GTC').
card_image_name('illness in the ranks'/'GTC', 'illness in the ranks').
card_uid('illness in the ranks'/'GTC', 'GTC:Illness in the Ranks:illness in the ranks').
card_rarity('illness in the ranks'/'GTC', 'Uncommon').
card_artist('illness in the ranks'/'GTC', 'Nils Hamm').
card_number('illness in the ranks'/'GTC', '69').
card_flavor_text('illness in the ranks'/'GTC', '\"Selesnya denied us tribute. Let\'s show them the error of their ways.\"\n—Milana, Orzhov prelate').
card_multiverse_id('illness in the ranks'/'GTC', '366354').

card_in_set('illusionist\'s bracers', 'GTC').
card_original_type('illusionist\'s bracers'/'GTC', 'Artifact — Equipment').
card_original_text('illusionist\'s bracers'/'GTC', 'Whenever an ability of equipped creature is activated, if it isn\'t a mana ability, copy that ability. You may choose new targets for the copy.\nEquip {3}').
card_first_print('illusionist\'s bracers', 'GTC').
card_image_name('illusionist\'s bracers'/'GTC', 'illusionist\'s bracers').
card_uid('illusionist\'s bracers'/'GTC', 'GTC:Illusionist\'s Bracers:illusionist\'s bracers').
card_rarity('illusionist\'s bracers'/'GTC', 'Rare').
card_artist('illusionist\'s bracers'/'GTC', 'Svetlin Velinov').
card_number('illusionist\'s bracers'/'GTC', '231').
card_flavor_text('illusionist\'s bracers'/'GTC', 'It\'s easy to believe you\'re a god when you\'re twice as powerful as everyone else.').
card_multiverse_id('illusionist\'s bracers'/'GTC', '366426').

card_in_set('immortal servitude', 'GTC').
card_original_type('immortal servitude'/'GTC', 'Sorcery').
card_original_text('immortal servitude'/'GTC', 'Return each creature card with converted mana cost X from your graveyard to the battlefield.').
card_first_print('immortal servitude', 'GTC').
card_image_name('immortal servitude'/'GTC', 'immortal servitude').
card_uid('immortal servitude'/'GTC', 'GTC:Immortal Servitude:immortal servitude').
card_rarity('immortal servitude'/'GTC', 'Rare').
card_artist('immortal servitude'/'GTC', 'Seb McKinnon').
card_number('immortal servitude'/'GTC', '220').
card_flavor_text('immortal servitude'/'GTC', '\"The fine print of countless contracts has ensured we are never defenseless.\"\n—Teysa Karlov, Grand Envoy of Orzhov').
card_multiverse_id('immortal servitude'/'GTC', '366247').
card_watermark('immortal servitude'/'GTC', 'Orzhov').

card_in_set('incursion specialist', 'GTC').
card_original_type('incursion specialist'/'GTC', 'Creature — Human Wizard').
card_original_text('incursion specialist'/'GTC', 'Whenever you cast your second spell each turn, Incursion Specialist gets +2/+0 until end of turn and is unblockable this turn.').
card_first_print('incursion specialist', 'GTC').
card_image_name('incursion specialist'/'GTC', 'incursion specialist').
card_uid('incursion specialist'/'GTC', 'GTC:Incursion Specialist:incursion specialist').
card_rarity('incursion specialist'/'GTC', 'Uncommon').
card_artist('incursion specialist'/'GTC', 'Svetlin Velinov').
card_number('incursion specialist'/'GTC', '38').
card_flavor_text('incursion specialist'/'GTC', '\"Work for the Dimir again? That depends. Tell me more about this maze.\"').
card_multiverse_id('incursion specialist'/'GTC', '366301').

card_in_set('ivy lane denizen', 'GTC').
card_original_type('ivy lane denizen'/'GTC', 'Creature — Elf Warrior').
card_original_text('ivy lane denizen'/'GTC', 'Whenever another green creature enters the battlefield under your control, put a +1/+1 counter on target creature.').
card_first_print('ivy lane denizen', 'GTC').
card_image_name('ivy lane denizen'/'GTC', 'ivy lane denizen').
card_uid('ivy lane denizen'/'GTC', 'GTC:Ivy Lane Denizen:ivy lane denizen').
card_rarity('ivy lane denizen'/'GTC', 'Common').
card_artist('ivy lane denizen'/'GTC', 'Winona Nelson').
card_number('ivy lane denizen'/'GTC', '125').
card_flavor_text('ivy lane denizen'/'GTC', '\"Skyknight gear? Well, I have drake-skin gauntlets, supple and fireproof. Or a ceratok chestplate that\'ll stop a crossbow bolt.\"').
card_multiverse_id('ivy lane denizen'/'GTC', '366317').

card_in_set('keymaster rogue', 'GTC').
card_original_type('keymaster rogue'/'GTC', 'Creature — Human Rogue').
card_original_text('keymaster rogue'/'GTC', 'Keymaster Rogue is unblockable.\nWhen Keymaster Rogue enters the battlefield, return a creature you control to its owner\'s hand.').
card_first_print('keymaster rogue', 'GTC').
card_image_name('keymaster rogue'/'GTC', 'keymaster rogue').
card_uid('keymaster rogue'/'GTC', 'GTC:Keymaster Rogue:keymaster rogue').
card_rarity('keymaster rogue'/'GTC', 'Common').
card_artist('keymaster rogue'/'GTC', 'Winona Nelson').
card_number('keymaster rogue'/'GTC', '39').
card_flavor_text('keymaster rogue'/'GTC', '\"This one opens the grate outside Zelzo Base. This one is for Ismeri Library. This one is for an invisible door . . . No, I\'ve said too much.\"').
card_multiverse_id('keymaster rogue'/'GTC', '366380').

card_in_set('killing glare', 'GTC').
card_original_type('killing glare'/'GTC', 'Instant').
card_original_text('killing glare'/'GTC', 'Destroy target creature with power X or less.').
card_first_print('killing glare', 'GTC').
card_image_name('killing glare'/'GTC', 'killing glare').
card_uid('killing glare'/'GTC', 'GTC:Killing Glare:killing glare').
card_rarity('killing glare'/'GTC', 'Uncommon').
card_artist('killing glare'/'GTC', 'Peter Mohrbacher').
card_number('killing glare'/'GTC', '70').
card_flavor_text('killing glare'/'GTC', 'A casual glance into a dark corner may be your last.').
card_multiverse_id('killing glare'/'GTC', '366466').

card_in_set('kingpin\'s pet', 'GTC').
card_original_type('kingpin\'s pet'/'GTC', 'Creature — Thrull').
card_original_text('kingpin\'s pet'/'GTC', 'Flying\nExtort (Whenever you cast a spell, you may pay {W/B}. If you do, each opponent loses 1 life and you gain that much life.)').
card_first_print('kingpin\'s pet', 'GTC').
card_image_name('kingpin\'s pet'/'GTC', 'kingpin\'s pet').
card_uid('kingpin\'s pet'/'GTC', 'GTC:Kingpin\'s Pet:kingpin\'s pet').
card_rarity('kingpin\'s pet'/'GTC', 'Common').
card_artist('kingpin\'s pet'/'GTC', 'Mark Zug').
card_number('kingpin\'s pet'/'GTC', '173').
card_flavor_text('kingpin\'s pet'/'GTC', '\"Debt and death sound so similar, and thrulls are so hard of hearing. Mistakes are inevitable.\"\n—Milana, Orzhov prelate').
card_multiverse_id('kingpin\'s pet'/'GTC', '366361').
card_watermark('kingpin\'s pet'/'GTC', 'Orzhov').

card_in_set('knight of obligation', 'GTC').
card_original_type('knight of obligation'/'GTC', 'Creature — Human Knight').
card_original_text('knight of obligation'/'GTC', 'Vigilance\nExtort (Whenever you cast a spell, you may pay {W/B}. If you do, each opponent loses 1 life and you gain that much life.)').
card_first_print('knight of obligation', 'GTC').
card_image_name('knight of obligation'/'GTC', 'knight of obligation').
card_uid('knight of obligation'/'GTC', 'GTC:Knight of Obligation:knight of obligation').
card_rarity('knight of obligation'/'GTC', 'Uncommon').
card_artist('knight of obligation'/'GTC', 'Ryan Barger').
card_number('knight of obligation'/'GTC', '18').
card_flavor_text('knight of obligation'/'GTC', 'He can calculate interest to six decimal places even in the midst of a deadly duel.').
card_multiverse_id('knight of obligation'/'GTC', '366237').
card_watermark('knight of obligation'/'GTC', 'Orzhov').

card_in_set('knight watch', 'GTC').
card_original_type('knight watch'/'GTC', 'Sorcery').
card_original_text('knight watch'/'GTC', 'Put two 2/2 white Knight creature tokens with vigilance onto the battlefield.').
card_first_print('knight watch', 'GTC').
card_image_name('knight watch'/'GTC', 'knight watch').
card_uid('knight watch'/'GTC', 'GTC:Knight Watch:knight watch').
card_rarity('knight watch'/'GTC', 'Common').
card_artist('knight watch'/'GTC', 'Matt Stewart').
card_number('knight watch'/'GTC', '19').
card_flavor_text('knight watch'/'GTC', '\"We lost respect after the battle for Barbu Rooftop. Send the troops to mingle respectfully with the citizens.\"\n—Commander Yaszen of Boros').
card_multiverse_id('knight watch'/'GTC', '366477').

card_in_set('last thoughts', 'GTC').
card_original_type('last thoughts'/'GTC', 'Sorcery').
card_original_text('last thoughts'/'GTC', 'Draw a card.\nCipher (Then you may exile this spell card encoded on a creature you control. Whenever that creature deals combat damage to a player, its controller may cast a copy of the encoded card without paying its mana cost.)').
card_first_print('last thoughts', 'GTC').
card_image_name('last thoughts'/'GTC', 'last thoughts').
card_uid('last thoughts'/'GTC', 'GTC:Last Thoughts:last thoughts').
card_rarity('last thoughts'/'GTC', 'Common').
card_artist('last thoughts'/'GTC', 'Peter Mohrbacher').
card_number('last thoughts'/'GTC', '40').
card_multiverse_id('last thoughts'/'GTC', '366407').
card_watermark('last thoughts'/'GTC', 'Dimir').

card_in_set('lazav, dimir mastermind', 'GTC').
card_original_type('lazav, dimir mastermind'/'GTC', 'Legendary Creature — Shapeshifter').
card_original_text('lazav, dimir mastermind'/'GTC', 'Hexproof\nWhenever a creature card is put into an opponent\'s graveyard from anywhere, you may have Lazav, Dimir Mastermind become a copy of that card except its name is still Lazav, Dimir Mastermind, it\'s legendary in addition to its other types, and it gains hexproof and this ability.').
card_first_print('lazav, dimir mastermind', 'GTC').
card_image_name('lazav, dimir mastermind'/'GTC', 'lazav, dimir mastermind').
card_uid('lazav, dimir mastermind'/'GTC', 'GTC:Lazav, Dimir Mastermind:lazav, dimir mastermind').
card_rarity('lazav, dimir mastermind'/'GTC', 'Mythic Rare').
card_artist('lazav, dimir mastermind'/'GTC', 'David Rapoza').
card_number('lazav, dimir mastermind'/'GTC', '174').
card_multiverse_id('lazav, dimir mastermind'/'GTC', '366469').
card_watermark('lazav, dimir mastermind'/'GTC', 'Dimir').

card_in_set('legion loyalist', 'GTC').
card_original_type('legion loyalist'/'GTC', 'Creature — Goblin Soldier').
card_original_text('legion loyalist'/'GTC', 'Haste\nBattalion — Whenever Legion Loyalist and at least two other creatures attack, creatures you control gain first strike and trample until end of turn and can\'t be blocked by creature tokens this turn.').
card_first_print('legion loyalist', 'GTC').
card_image_name('legion loyalist'/'GTC', 'legion loyalist').
card_uid('legion loyalist'/'GTC', 'GTC:Legion Loyalist:legion loyalist').
card_rarity('legion loyalist'/'GTC', 'Rare').
card_artist('legion loyalist'/'GTC', 'Eric Deschamps').
card_number('legion loyalist'/'GTC', '97').
card_multiverse_id('legion loyalist'/'GTC', '366348').
card_watermark('legion loyalist'/'GTC', 'Boros').

card_in_set('leyline phantom', 'GTC').
card_original_type('leyline phantom'/'GTC', 'Creature — Illusion').
card_original_text('leyline phantom'/'GTC', 'When Leyline Phantom deals combat damage, return it to its owner\'s hand. (Return it only if it survived combat.)').
card_first_print('leyline phantom', 'GTC').
card_image_name('leyline phantom'/'GTC', 'leyline phantom').
card_uid('leyline phantom'/'GTC', 'GTC:Leyline Phantom:leyline phantom').
card_rarity('leyline phantom'/'GTC', 'Common').
card_artist('leyline phantom'/'GTC', 'Ryan Yee').
card_number('leyline phantom'/'GTC', '41').
card_flavor_text('leyline phantom'/'GTC', '\"Is the maze itself a phantom? Or is one as real as the other? Perhaps I am as mad as the dragon.\"\n—Jace Beleren, journal').
card_multiverse_id('leyline phantom'/'GTC', '366329').

card_in_set('lord of the void', 'GTC').
card_original_type('lord of the void'/'GTC', 'Creature — Demon').
card_original_text('lord of the void'/'GTC', 'Flying\nWhenever Lord of the Void deals combat damage to a player, exile the top seven cards of that player\'s library, then put a creature card from among them onto the battlefield under your control.').
card_first_print('lord of the void', 'GTC').
card_image_name('lord of the void'/'GTC', 'lord of the void').
card_uid('lord of the void'/'GTC', 'GTC:Lord of the Void:lord of the void').
card_rarity('lord of the void'/'GTC', 'Mythic Rare').
card_artist('lord of the void'/'GTC', 'Chris Rahn').
card_number('lord of the void'/'GTC', '71').
card_multiverse_id('lord of the void'/'GTC', '366412').

card_in_set('luminate primordial', 'GTC').
card_original_type('luminate primordial'/'GTC', 'Creature — Avatar').
card_original_text('luminate primordial'/'GTC', 'Vigilance\nWhen Luminate Primordial enters the battlefield, for each opponent, exile up to one target creature that player controls and that player gains life equal to its power.').
card_first_print('luminate primordial', 'GTC').
card_image_name('luminate primordial'/'GTC', 'luminate primordial').
card_uid('luminate primordial'/'GTC', 'GTC:Luminate Primordial:luminate primordial').
card_rarity('luminate primordial'/'GTC', 'Rare').
card_artist('luminate primordial'/'GTC', 'Stephan Martiniere').
card_number('luminate primordial'/'GTC', '20').
card_multiverse_id('luminate primordial'/'GTC', '366240').

card_in_set('madcap skills', 'GTC').
card_original_type('madcap skills'/'GTC', 'Enchantment — Aura').
card_original_text('madcap skills'/'GTC', 'Enchant creature\nEnchanted creature gets +3/+0 and can\'t be blocked except by two or more creatures.').
card_first_print('madcap skills', 'GTC').
card_image_name('madcap skills'/'GTC', 'madcap skills').
card_uid('madcap skills'/'GTC', 'GTC:Madcap Skills:madcap skills').
card_rarity('madcap skills'/'GTC', 'Common').
card_artist('madcap skills'/'GTC', 'Anthony Palumbo').
card_number('madcap skills'/'GTC', '98').
card_flavor_text('madcap skills'/'GTC', 'The larger the crowd, the harder it is for them to run away.').
card_multiverse_id('madcap skills'/'GTC', '366377').

card_in_set('mark for death', 'GTC').
card_original_type('mark for death'/'GTC', 'Sorcery').
card_original_text('mark for death'/'GTC', 'Target creature an opponent controls blocks this turn if able. Untap that creature. Other creatures that player controls can\'t block this turn.').
card_first_print('mark for death', 'GTC').
card_image_name('mark for death'/'GTC', 'mark for death').
card_uid('mark for death'/'GTC', 'GTC:Mark for Death:mark for death').
card_rarity('mark for death'/'GTC', 'Uncommon').
card_artist('mark for death'/'GTC', 'Mathias Kollros').
card_number('mark for death'/'GTC', '99').
card_flavor_text('mark for death'/'GTC', 'Everyone roots for the little guy, but nobody bets on him.').
card_multiverse_id('mark for death'/'GTC', '366286').

card_in_set('martial glory', 'GTC').
card_original_type('martial glory'/'GTC', 'Instant').
card_original_text('martial glory'/'GTC', 'Target creature gets +3/+0 until end of turn.\nTarget creature gets +0/+3 until end of turn.').
card_first_print('martial glory', 'GTC').
card_image_name('martial glory'/'GTC', 'martial glory').
card_uid('martial glory'/'GTC', 'GTC:Martial Glory:martial glory').
card_rarity('martial glory'/'GTC', 'Common').
card_artist('martial glory'/'GTC', 'Raymond Swanland').
card_number('martial glory'/'GTC', '175').
card_flavor_text('martial glory'/'GTC', '\"Yes, Boros tactics are unparalleled. But when your comrades are dying around you, inner strength must carry the day.\"\n—Gideon Jura, to Aurelia').
card_multiverse_id('martial glory'/'GTC', '366358').
card_watermark('martial glory'/'GTC', 'Boros').

card_in_set('massive raid', 'GTC').
card_original_type('massive raid'/'GTC', 'Instant').
card_original_text('massive raid'/'GTC', 'Massive Raid deals damage to target creature or player equal to the number of creatures you control.').
card_first_print('massive raid', 'GTC').
card_image_name('massive raid'/'GTC', 'massive raid').
card_uid('massive raid'/'GTC', 'GTC:Massive Raid:massive raid').
card_rarity('massive raid'/'GTC', 'Common').
card_artist('massive raid'/'GTC', 'Zoltan Boros').
card_number('massive raid'/'GTC', '100').
card_flavor_text('massive raid'/'GTC', '\"The Boros lack vision. Give them a convenient scapegoat and they\'ll be blind to the true threats.\"\n—Lazav').
card_multiverse_id('massive raid'/'GTC', '366320').

card_in_set('master biomancer', 'GTC').
card_original_type('master biomancer'/'GTC', 'Creature — Elf Wizard').
card_original_text('master biomancer'/'GTC', 'Each other creature you control enters the battlefield with a number of additional +1/+1 counters on it equal to Master Biomancer\'s power and as a Mutant in addition to its other types.').
card_first_print('master biomancer', 'GTC').
card_image_name('master biomancer'/'GTC', 'master biomancer').
card_uid('master biomancer'/'GTC', 'GTC:Master Biomancer:master biomancer').
card_rarity('master biomancer'/'GTC', 'Mythic Rare').
card_artist('master biomancer'/'GTC', 'Willian Murai').
card_number('master biomancer'/'GTC', '176').
card_flavor_text('master biomancer'/'GTC', '\"Nature is a wonderful engineer but works far too slowly for my tastes.\"').
card_multiverse_id('master biomancer'/'GTC', '366352').
card_watermark('master biomancer'/'GTC', 'Simic').

card_in_set('mental vapors', 'GTC').
card_original_type('mental vapors'/'GTC', 'Sorcery').
card_original_text('mental vapors'/'GTC', 'Target player discards a card.\nCipher (Then you may exile this spell card encoded on a creature you control. Whenever that creature deals combat damage to a player, its controller may cast a copy of the encoded card without paying its mana cost.)').
card_first_print('mental vapors', 'GTC').
card_image_name('mental vapors'/'GTC', 'mental vapors').
card_uid('mental vapors'/'GTC', 'GTC:Mental Vapors:mental vapors').
card_rarity('mental vapors'/'GTC', 'Uncommon').
card_artist('mental vapors'/'GTC', 'Mark Winters').
card_number('mental vapors'/'GTC', '72').
card_multiverse_id('mental vapors'/'GTC', '366373').
card_watermark('mental vapors'/'GTC', 'Dimir').

card_in_set('merciless eviction', 'GTC').
card_original_type('merciless eviction'/'GTC', 'Sorcery').
card_original_text('merciless eviction'/'GTC', 'Choose one — Exile all artifacts; or exile all creatures; or exile all enchantments; or exile all planeswalkers.').
card_first_print('merciless eviction', 'GTC').
card_image_name('merciless eviction'/'GTC', 'merciless eviction').
card_uid('merciless eviction'/'GTC', 'GTC:Merciless Eviction:merciless eviction').
card_rarity('merciless eviction'/'GTC', 'Rare').
card_artist('merciless eviction'/'GTC', 'Richard Wright').
card_number('merciless eviction'/'GTC', '177').
card_flavor_text('merciless eviction'/'GTC', '\"I once saw the Obzedat moved to action. Since that day, I\'ve been thankful that they\'re mainly lazy, and dead.\"\n—Aurelia, to Gideon Jura').
card_multiverse_id('merciless eviction'/'GTC', '366316').
card_watermark('merciless eviction'/'GTC', 'Orzhov').

card_in_set('merfolk of the depths', 'GTC').
card_original_type('merfolk of the depths'/'GTC', 'Creature — Merfolk Soldier').
card_original_text('merfolk of the depths'/'GTC', 'Flash (You may cast this spell any time you could cast an instant.)').
card_first_print('merfolk of the depths', 'GTC').
card_image_name('merfolk of the depths'/'GTC', 'merfolk of the depths').
card_uid('merfolk of the depths'/'GTC', 'GTC:Merfolk of the Depths:merfolk of the depths').
card_rarity('merfolk of the depths'/'GTC', 'Uncommon').
card_artist('merfolk of the depths'/'GTC', 'Scott Chou').
card_number('merfolk of the depths'/'GTC', '221').
card_flavor_text('merfolk of the depths'/'GTC', 'When giant sinkholes opened across Ravnica, they revealed buried oceans and the merfolk, once thought to be extinct.').
card_multiverse_id('merfolk of the depths'/'GTC', '366442').
card_watermark('merfolk of the depths'/'GTC', 'Simic').

card_in_set('metropolis sprite', 'GTC').
card_original_type('metropolis sprite'/'GTC', 'Creature — Faerie Rogue').
card_original_text('metropolis sprite'/'GTC', 'Flying\n{U}: Metropolis Sprite gets +1/-1 until end of turn.').
card_first_print('metropolis sprite', 'GTC').
card_image_name('metropolis sprite'/'GTC', 'metropolis sprite').
card_uid('metropolis sprite'/'GTC', 'GTC:Metropolis Sprite:metropolis sprite').
card_rarity('metropolis sprite'/'GTC', 'Common').
card_artist('metropolis sprite'/'GTC', 'Scott Chou').
card_number('metropolis sprite'/'GTC', '42').
card_flavor_text('metropolis sprite'/'GTC', '\"Well, if that goblin didn\'t want his tongue pierced, he shouldn\'t have stuck it out at me.\"').
card_multiverse_id('metropolis sprite'/'GTC', '366293').

card_in_set('midnight recovery', 'GTC').
card_original_type('midnight recovery'/'GTC', 'Sorcery').
card_original_text('midnight recovery'/'GTC', 'Return target creature card from your graveyard to your hand.\nCipher (Then you may exile this spell card encoded on a creature you control. Whenever that creature deals combat damage to a player, its controller may cast a copy of the encoded card without paying its mana cost.)').
card_first_print('midnight recovery', 'GTC').
card_image_name('midnight recovery'/'GTC', 'midnight recovery').
card_uid('midnight recovery'/'GTC', 'GTC:Midnight Recovery:midnight recovery').
card_rarity('midnight recovery'/'GTC', 'Common').
card_artist('midnight recovery'/'GTC', 'Peter Mohrbacher').
card_number('midnight recovery'/'GTC', '73').
card_multiverse_id('midnight recovery'/'GTC', '366476').
card_watermark('midnight recovery'/'GTC', 'Dimir').

card_in_set('millennial gargoyle', 'GTC').
card_original_type('millennial gargoyle'/'GTC', 'Artifact Creature — Gargoyle').
card_original_text('millennial gargoyle'/'GTC', 'Flying').
card_first_print('millennial gargoyle', 'GTC').
card_image_name('millennial gargoyle'/'GTC', 'millennial gargoyle').
card_uid('millennial gargoyle'/'GTC', 'GTC:Millennial Gargoyle:millennial gargoyle').
card_rarity('millennial gargoyle'/'GTC', 'Common').
card_artist('millennial gargoyle'/'GTC', 'Seb McKinnon').
card_number('millennial gargoyle'/'GTC', '232').
card_flavor_text('millennial gargoyle'/'GTC', '\"You\'ve got to keep your wits about you on the roofs. Not all stones are compliant handholds.\"\n—Iveta, rooftop runner').
card_multiverse_id('millennial gargoyle'/'GTC', '366451').

card_in_set('miming slime', 'GTC').
card_original_type('miming slime'/'GTC', 'Sorcery').
card_original_text('miming slime'/'GTC', 'Put an X/X green Ooze creature token onto the battlefield, where X is the greatest power among creatures you control.').
card_first_print('miming slime', 'GTC').
card_image_name('miming slime'/'GTC', 'miming slime').
card_uid('miming slime'/'GTC', 'GTC:Miming Slime:miming slime').
card_rarity('miming slime'/'GTC', 'Uncommon').
card_artist('miming slime'/'GTC', 'Svetlin Velinov').
card_number('miming slime'/'GTC', '126').
card_flavor_text('miming slime'/'GTC', '\"We paid the Simic very well for this capability, but we should quickly recoup our expenses in saved wages.\"\n—Milana, Orzhov prelate').
card_multiverse_id('miming slime'/'GTC', '366340').

card_in_set('mind grind', 'GTC').
card_original_type('mind grind'/'GTC', 'Sorcery').
card_original_text('mind grind'/'GTC', 'Each opponent reveals cards from the top of his or her library until he or she reveals X land cards, then puts all cards revealed this way into his or her graveyard. X can\'t be 0.').
card_first_print('mind grind', 'GTC').
card_image_name('mind grind'/'GTC', 'mind grind').
card_uid('mind grind'/'GTC', 'GTC:Mind Grind:mind grind').
card_rarity('mind grind'/'GTC', 'Rare').
card_artist('mind grind'/'GTC', 'Daarken').
card_number('mind grind'/'GTC', '178').
card_multiverse_id('mind grind'/'GTC', '366418').
card_watermark('mind grind'/'GTC', 'Dimir').

card_in_set('mindeye drake', 'GTC').
card_original_type('mindeye drake'/'GTC', 'Creature — Drake').
card_original_text('mindeye drake'/'GTC', 'Flying\nWhen Mindeye Drake dies, target player puts the top five cards of his or her library into his or her graveyard.').
card_first_print('mindeye drake', 'GTC').
card_image_name('mindeye drake'/'GTC', 'mindeye drake').
card_uid('mindeye drake'/'GTC', 'GTC:Mindeye Drake:mindeye drake').
card_rarity('mindeye drake'/'GTC', 'Uncommon').
card_artist('mindeye drake'/'GTC', 'Lars Grant-West').
card_number('mindeye drake'/'GTC', '43').
card_flavor_text('mindeye drake'/'GTC', 'They can map both the cityscape and the minds of its architects.').
card_multiverse_id('mindeye drake'/'GTC', '366284').

card_in_set('molten primordial', 'GTC').
card_original_type('molten primordial'/'GTC', 'Creature — Avatar').
card_original_text('molten primordial'/'GTC', 'Haste\nWhen Molten Primordial enters the battlefield, for each opponent, gain control of up to one target creature that player controls until end of turn. Untap those creatures. They gain haste until end of turn.').
card_first_print('molten primordial', 'GTC').
card_image_name('molten primordial'/'GTC', 'molten primordial').
card_uid('molten primordial'/'GTC', 'GTC:Molten Primordial:molten primordial').
card_rarity('molten primordial'/'GTC', 'Rare').
card_artist('molten primordial'/'GTC', 'Stephan Martiniere').
card_number('molten primordial'/'GTC', '101').
card_multiverse_id('molten primordial'/'GTC', '366356').

card_in_set('mortus strider', 'GTC').
card_original_type('mortus strider'/'GTC', 'Creature — Skeleton').
card_original_text('mortus strider'/'GTC', 'When Mortus Strider dies, return it to its owner\'s hand.').
card_first_print('mortus strider', 'GTC').
card_image_name('mortus strider'/'GTC', 'mortus strider').
card_uid('mortus strider'/'GTC', 'GTC:Mortus Strider:mortus strider').
card_rarity('mortus strider'/'GTC', 'Common').
card_artist('mortus strider'/'GTC', 'Tomasz Jedruszek').
card_number('mortus strider'/'GTC', '179').
card_flavor_text('mortus strider'/'GTC', 'Dimir\'s best agents hide from death itself.').
card_multiverse_id('mortus strider'/'GTC', '366449').
card_watermark('mortus strider'/'GTC', 'Dimir').

card_in_set('mugging', 'GTC').
card_original_type('mugging'/'GTC', 'Sorcery').
card_original_text('mugging'/'GTC', 'Mugging deals 2 damage to target creature. That creature can\'t block this turn.').
card_first_print('mugging', 'GTC').
card_image_name('mugging'/'GTC', 'mugging').
card_uid('mugging'/'GTC', 'GTC:Mugging:mugging').
card_rarity('mugging'/'GTC', 'Common').
card_artist('mugging'/'GTC', 'Greg Staples').
card_number('mugging'/'GTC', '102').
card_flavor_text('mugging'/'GTC', 'Scream: muffled. Pockets: emptied. Neck function: never the same again.').
card_multiverse_id('mugging'/'GTC', '366430').

card_in_set('murder investigation', 'GTC').
card_original_type('murder investigation'/'GTC', 'Enchantment — Aura').
card_original_text('murder investigation'/'GTC', 'Enchant creature you control\nWhen enchanted creature dies, put X 1/1 white Soldier creature tokens onto the battlefield, where X is its power.').
card_first_print('murder investigation', 'GTC').
card_image_name('murder investigation'/'GTC', 'murder investigation').
card_uid('murder investigation'/'GTC', 'GTC:Murder Investigation:murder investigation').
card_rarity('murder investigation'/'GTC', 'Uncommon').
card_artist('murder investigation'/'GTC', 'Igor Kieryluk').
card_number('murder investigation'/'GTC', '21').
card_flavor_text('murder investigation'/'GTC', '\"Every death has a web of consequences. Our job is to find the spider.\"').
card_multiverse_id('murder investigation'/'GTC', '366327').

card_in_set('mystic genesis', 'GTC').
card_original_type('mystic genesis'/'GTC', 'Instant').
card_original_text('mystic genesis'/'GTC', 'Counter target spell. Put an X/X green Ooze creature token onto the battlefield, where X is that spell\'s converted mana cost.').
card_first_print('mystic genesis', 'GTC').
card_image_name('mystic genesis'/'GTC', 'mystic genesis').
card_uid('mystic genesis'/'GTC', 'GTC:Mystic Genesis:mystic genesis').
card_rarity('mystic genesis'/'GTC', 'Rare').
card_artist('mystic genesis'/'GTC', 'Mike Bierek').
card_number('mystic genesis'/'GTC', '180').
card_flavor_text('mystic genesis'/'GTC', '\"The Simic can grow anything out of a puddle of sludge. I just hope they never join forces with the Golgari.\"\n—Teysa Karlov, Grand Envoy of Orzhov').
card_multiverse_id('mystic genesis'/'GTC', '366401').
card_watermark('mystic genesis'/'GTC', 'Simic').

card_in_set('naturalize', 'GTC').
card_original_type('naturalize'/'GTC', 'Instant').
card_original_text('naturalize'/'GTC', 'Destroy target artifact or enchantment.').
card_image_name('naturalize'/'GTC', 'naturalize').
card_uid('naturalize'/'GTC', 'GTC:Naturalize:naturalize').
card_rarity('naturalize'/'GTC', 'Common').
card_artist('naturalize'/'GTC', 'Daniel Ljunggren').
card_number('naturalize'/'GTC', '127').
card_flavor_text('naturalize'/'GTC', '\"And if you threaten me again, your shiny steel trousers will be sprouting daggerthorn vines.\"\n—Iveta, rooftop runner').
card_multiverse_id('naturalize'/'GTC', '366303').

card_in_set('nav squad commandos', 'GTC').
card_original_type('nav squad commandos'/'GTC', 'Creature — Human Soldier').
card_original_text('nav squad commandos'/'GTC', 'Battalion — Whenever Nav Squad Commandos and at least two other creatures attack, Nav Squad Commandos gets +1/+1 until end of turn. Untap it.').
card_first_print('nav squad commandos', 'GTC').
card_image_name('nav squad commandos'/'GTC', 'nav squad commandos').
card_uid('nav squad commandos'/'GTC', 'GTC:Nav Squad Commandos:nav squad commandos').
card_rarity('nav squad commandos'/'GTC', 'Common').
card_artist('nav squad commandos'/'GTC', 'Steve Prescott').
card_number('nav squad commandos'/'GTC', '22').
card_flavor_text('nav squad commandos'/'GTC', 'Funded by the Azorius to keep current maps of the undercity, the Boros nav squads survey even the most unsavory areas.').
card_multiverse_id('nav squad commandos'/'GTC', '366378').
card_watermark('nav squad commandos'/'GTC', 'Boros').

card_in_set('nightveil specter', 'GTC').
card_original_type('nightveil specter'/'GTC', 'Creature — Specter').
card_original_text('nightveil specter'/'GTC', 'Flying\nWhenever Nightveil Specter deals combat damage to a player, that player exiles the top card of his or her library.\nYou may play cards exiled with Nightveil Specter.').
card_image_name('nightveil specter'/'GTC', 'nightveil specter').
card_uid('nightveil specter'/'GTC', 'GTC:Nightveil Specter:nightveil specter').
card_rarity('nightveil specter'/'GTC', 'Rare').
card_artist('nightveil specter'/'GTC', 'Min Yum').
card_number('nightveil specter'/'GTC', '222').
card_multiverse_id('nightveil specter'/'GTC', '366242').
card_watermark('nightveil specter'/'GTC', 'Dimir').

card_in_set('nimbus swimmer', 'GTC').
card_original_type('nimbus swimmer'/'GTC', 'Creature — Leviathan').
card_original_text('nimbus swimmer'/'GTC', 'Flying\nNimbus Swimmer enters the battlefield with X +1/+1 counters on it.').
card_first_print('nimbus swimmer', 'GTC').
card_image_name('nimbus swimmer'/'GTC', 'nimbus swimmer').
card_uid('nimbus swimmer'/'GTC', 'GTC:Nimbus Swimmer:nimbus swimmer').
card_rarity('nimbus swimmer'/'GTC', 'Uncommon').
card_artist('nimbus swimmer'/'GTC', 'Howard Lyon').
card_number('nimbus swimmer'/'GTC', '181').
card_flavor_text('nimbus swimmer'/'GTC', 'The Simic soon discovered that the sky offered as few constraints on size as the sea.').
card_multiverse_id('nimbus swimmer'/'GTC', '366275').
card_watermark('nimbus swimmer'/'GTC', 'Simic').

card_in_set('obzedat, ghost council', 'GTC').
card_original_type('obzedat, ghost council'/'GTC', 'Legendary Creature — Spirit Advisor').
card_original_text('obzedat, ghost council'/'GTC', 'When Obzedat, Ghost Council enters the battlefield, target opponent loses 2 life and you gain 2 life.\nAt the beginning of your end step, you may exile Obzedat. If you do, return it to the battlefield under its owner\'s control at the beginning of your next upkeep. It gains haste.').
card_first_print('obzedat, ghost council', 'GTC').
card_image_name('obzedat, ghost council'/'GTC', 'obzedat, ghost council').
card_uid('obzedat, ghost council'/'GTC', 'GTC:Obzedat, Ghost Council:obzedat, ghost council').
card_rarity('obzedat, ghost council'/'GTC', 'Mythic Rare').
card_artist('obzedat, ghost council'/'GTC', 'Svetlin Velinov').
card_number('obzedat, ghost council'/'GTC', '182').
card_multiverse_id('obzedat, ghost council'/'GTC', '366246').
card_watermark('obzedat, ghost council'/'GTC', 'Orzhov').

card_in_set('ogre slumlord', 'GTC').
card_original_type('ogre slumlord'/'GTC', 'Creature — Ogre Rogue').
card_original_text('ogre slumlord'/'GTC', 'Whenever another nontoken creature dies, you may put a 1/1 black Rat creature token onto the battlefield.\nRats you control have deathtouch.').
card_first_print('ogre slumlord', 'GTC').
card_image_name('ogre slumlord'/'GTC', 'ogre slumlord').
card_uid('ogre slumlord'/'GTC', 'GTC:Ogre Slumlord:ogre slumlord').
card_rarity('ogre slumlord'/'GTC', 'Rare').
card_artist('ogre slumlord'/'GTC', 'Trevor Claxton').
card_number('ogre slumlord'/'GTC', '74').
card_flavor_text('ogre slumlord'/'GTC', '\"His tenement is filled with the most vile, disgusting vermin. It\'s infested with rats, too.\"\n—Branko One-Ear').
card_multiverse_id('ogre slumlord'/'GTC', '366432').

card_in_set('one thousand lashes', 'GTC').
card_original_type('one thousand lashes'/'GTC', 'Enchantment — Aura').
card_original_text('one thousand lashes'/'GTC', 'Enchant creature\nEnchanted creature can\'t attack or block, and its activated abilities can\'t be activated.\nAt the beginning of the upkeep of enchanted creature\'s controller, that player loses 1 life.').
card_first_print('one thousand lashes', 'GTC').
card_image_name('one thousand lashes'/'GTC', 'one thousand lashes').
card_uid('one thousand lashes'/'GTC', 'GTC:One Thousand Lashes:one thousand lashes').
card_rarity('one thousand lashes'/'GTC', 'Uncommon').
card_artist('one thousand lashes'/'GTC', 'Daarken').
card_number('one thousand lashes'/'GTC', '183').
card_multiverse_id('one thousand lashes'/'GTC', '366261').
card_watermark('one thousand lashes'/'GTC', 'Orzhov').

card_in_set('ooze flux', 'GTC').
card_original_type('ooze flux'/'GTC', 'Enchantment').
card_original_text('ooze flux'/'GTC', '{1}{G}, Remove one or more +1/+1 counters from among creatures you control: Put an X/X green Ooze creature token onto the battlefield, where X is the number of +1/+1 counters removed this way.').
card_first_print('ooze flux', 'GTC').
card_image_name('ooze flux'/'GTC', 'ooze flux').
card_uid('ooze flux'/'GTC', 'GTC:Ooze Flux:ooze flux').
card_rarity('ooze flux'/'GTC', 'Rare').
card_artist('ooze flux'/'GTC', 'Zoltan Boros').
card_number('ooze flux'/'GTC', '128').
card_multiverse_id('ooze flux'/'GTC', '366375').

card_in_set('ordruun veteran', 'GTC').
card_original_type('ordruun veteran'/'GTC', 'Creature — Minotaur Soldier').
card_original_text('ordruun veteran'/'GTC', 'Battalion — Whenever Ordruun Veteran and at least two other creatures attack, Ordruun Veteran gains double strike until end of turn. (It deals both first-strike and regular combat damage.)').
card_first_print('ordruun veteran', 'GTC').
card_image_name('ordruun veteran'/'GTC', 'ordruun veteran').
card_uid('ordruun veteran'/'GTC', 'GTC:Ordruun Veteran:ordruun veteran').
card_rarity('ordruun veteran'/'GTC', 'Uncommon').
card_artist('ordruun veteran'/'GTC', 'Greg Staples').
card_number('ordruun veteran'/'GTC', '184').
card_flavor_text('ordruun veteran'/'GTC', '\"My father was Gruul, but I chose the precision of the legion over the fury of the pack.\"').
card_multiverse_id('ordruun veteran'/'GTC', '366259').
card_watermark('ordruun veteran'/'GTC', 'Boros').

card_in_set('orzhov charm', 'GTC').
card_original_type('orzhov charm'/'GTC', 'Instant').
card_original_text('orzhov charm'/'GTC', 'Choose one — Return target creature you control and all Auras you control attached to it to their owner\'s hand; or destroy target creature and you lose life equal to its toughness; or return target creature card with converted mana cost 1 or less from your graveyard to the battlefield.').
card_first_print('orzhov charm', 'GTC').
card_image_name('orzhov charm'/'GTC', 'orzhov charm').
card_uid('orzhov charm'/'GTC', 'GTC:Orzhov Charm:orzhov charm').
card_rarity('orzhov charm'/'GTC', 'Uncommon').
card_artist('orzhov charm'/'GTC', 'Zoltan Boros').
card_number('orzhov charm'/'GTC', '185').
card_multiverse_id('orzhov charm'/'GTC', '366436').
card_watermark('orzhov charm'/'GTC', 'Orzhov').

card_in_set('orzhov guildgate', 'GTC').
card_original_type('orzhov guildgate'/'GTC', 'Land — Gate').
card_original_text('orzhov guildgate'/'GTC', 'Orzhov Guildgate enters the battlefield tapped.\n{T}: Add {W} or {B} to your mana pool.').
card_first_print('orzhov guildgate', 'GTC').
card_image_name('orzhov guildgate'/'GTC', 'orzhov guildgate').
card_uid('orzhov guildgate'/'GTC', 'GTC:Orzhov Guildgate:orzhov guildgate').
card_rarity('orzhov guildgate'/'GTC', 'Common').
card_artist('orzhov guildgate'/'GTC', 'John Avon').
card_number('orzhov guildgate'/'GTC', '244').
card_flavor_text('orzhov guildgate'/'GTC', 'Enter to find wealth, security, and eternal life . . . for just a small price up front.').
card_multiverse_id('orzhov guildgate'/'GTC', '366374').
card_watermark('orzhov guildgate'/'GTC', 'Orzhov').

card_in_set('orzhov keyrune', 'GTC').
card_original_type('orzhov keyrune'/'GTC', 'Artifact').
card_original_text('orzhov keyrune'/'GTC', '{T}: Add {W} or {B} to your mana pool.\n{W}{B}: Orzhov Keyrune becomes a 1/4 white and black Thrull artifact creature with lifelink until end of turn.').
card_first_print('orzhov keyrune', 'GTC').
card_image_name('orzhov keyrune'/'GTC', 'orzhov keyrune').
card_uid('orzhov keyrune'/'GTC', 'GTC:Orzhov Keyrune:orzhov keyrune').
card_rarity('orzhov keyrune'/'GTC', 'Uncommon').
card_artist('orzhov keyrune'/'GTC', 'Daniel Ljunggren').
card_number('orzhov keyrune'/'GTC', '233').
card_flavor_text('orzhov keyrune'/'GTC', 'You only need to wield it once to appreciate the power of control.').
card_multiverse_id('orzhov keyrune'/'GTC', '366479').
card_watermark('orzhov keyrune'/'GTC', 'Orzhov').

card_in_set('paranoid delusions', 'GTC').
card_original_type('paranoid delusions'/'GTC', 'Sorcery').
card_original_text('paranoid delusions'/'GTC', 'Target player puts the top three cards of his or her library into his or her graveyard.\nCipher (Then you may exile this spell card encoded on a creature you control. Whenever that creature deals combat damage to a player, its controller may cast a copy of the encoded card without paying its mana cost.)').
card_first_print('paranoid delusions', 'GTC').
card_image_name('paranoid delusions'/'GTC', 'paranoid delusions').
card_uid('paranoid delusions'/'GTC', 'GTC:Paranoid Delusions:paranoid delusions').
card_rarity('paranoid delusions'/'GTC', 'Common').
card_artist('paranoid delusions'/'GTC', 'Christopher Moeller').
card_number('paranoid delusions'/'GTC', '186').
card_multiverse_id('paranoid delusions'/'GTC', '366324').
card_watermark('paranoid delusions'/'GTC', 'Dimir').

card_in_set('pit fight', 'GTC').
card_original_type('pit fight'/'GTC', 'Instant').
card_original_text('pit fight'/'GTC', 'Target creature you control fights another target creature. (Each deals damage equal to its power to the other.)').
card_first_print('pit fight', 'GTC').
card_image_name('pit fight'/'GTC', 'pit fight').
card_uid('pit fight'/'GTC', 'GTC:Pit Fight:pit fight').
card_rarity('pit fight'/'GTC', 'Common').
card_artist('pit fight'/'GTC', 'Matt Stewart').
card_number('pit fight'/'GTC', '223').
card_flavor_text('pit fight'/'GTC', '\"All the coins in Ravnica can\'t save his soul now.\"\n—Narbulg Nine Fingers').
card_multiverse_id('pit fight'/'GTC', '366450').
card_watermark('pit fight'/'GTC', 'Gruul').

card_in_set('predator\'s rapport', 'GTC').
card_original_type('predator\'s rapport'/'GTC', 'Instant').
card_original_text('predator\'s rapport'/'GTC', 'Choose target creature you control. You gain life equal to that creature\'s power plus its toughness.').
card_first_print('predator\'s rapport', 'GTC').
card_image_name('predator\'s rapport'/'GTC', 'predator\'s rapport').
card_uid('predator\'s rapport'/'GTC', 'GTC:Predator\'s Rapport:predator\'s rapport').
card_rarity('predator\'s rapport'/'GTC', 'Common').
card_artist('predator\'s rapport'/'GTC', 'Matt Stewart').
card_number('predator\'s rapport'/'GTC', '129').
card_flavor_text('predator\'s rapport'/'GTC', '\"Other guilds say the Gruul are savages, no better than the beasts we live with. I say we\'ve found friends who won\'t stab us in the back.\"\n—Domri Rade').
card_multiverse_id('predator\'s rapport'/'GTC', '366233').

card_in_set('primal visitation', 'GTC').
card_original_type('primal visitation'/'GTC', 'Enchantment — Aura').
card_original_text('primal visitation'/'GTC', 'Enchant creature\nEnchanted creature gets +3/+3 and has haste.').
card_first_print('primal visitation', 'GTC').
card_image_name('primal visitation'/'GTC', 'primal visitation').
card_uid('primal visitation'/'GTC', 'GTC:Primal Visitation:primal visitation').
card_rarity('primal visitation'/'GTC', 'Common').
card_artist('primal visitation'/'GTC', 'Christopher Moeller').
card_number('primal visitation'/'GTC', '187').
card_flavor_text('primal visitation'/'GTC', 'Gruul shamans believe all sentient beings have the soul of savagery inside them, an animal form clawing to escape.').
card_multiverse_id('primal visitation'/'GTC', '366250').
card_watermark('primal visitation'/'GTC', 'Gruul').

card_in_set('prime speaker zegana', 'GTC').
card_original_type('prime speaker zegana'/'GTC', 'Legendary Creature — Merfolk Wizard').
card_original_text('prime speaker zegana'/'GTC', 'Prime Speaker Zegana enters the battlefield with X +1/+1 counters on it, where X is the greatest power among other creatures you control.\nWhen Prime Speaker Zegana enters the battlefield, draw cards equal to its power.').
card_first_print('prime speaker zegana', 'GTC').
card_image_name('prime speaker zegana'/'GTC', 'prime speaker zegana').
card_uid('prime speaker zegana'/'GTC', 'GTC:Prime Speaker Zegana:prime speaker zegana').
card_rarity('prime speaker zegana'/'GTC', 'Mythic Rare').
card_artist('prime speaker zegana'/'GTC', 'Willian Murai').
card_number('prime speaker zegana'/'GTC', '188').
card_multiverse_id('prime speaker zegana'/'GTC', '366416').
card_watermark('prime speaker zegana'/'GTC', 'Simic').

card_in_set('prophetic prism', 'GTC').
card_original_type('prophetic prism'/'GTC', 'Artifact').
card_original_text('prophetic prism'/'GTC', 'When Prophetic Prism enters the battlefield, draw a card.\n{1}, {T}: Add one mana of any color to your mana pool.').
card_image_name('prophetic prism'/'GTC', 'prophetic prism').
card_uid('prophetic prism'/'GTC', 'GTC:Prophetic Prism:prophetic prism').
card_rarity('prophetic prism'/'GTC', 'Common').
card_artist('prophetic prism'/'GTC', 'Daniel Ljunggren').
card_number('prophetic prism'/'GTC', '234').
card_flavor_text('prophetic prism'/'GTC', 'To understand Ravnica, one must first understand how each guild contributes to its stability.').
card_multiverse_id('prophetic prism'/'GTC', '366262').

card_in_set('psychic strike', 'GTC').
card_original_type('psychic strike'/'GTC', 'Instant').
card_original_text('psychic strike'/'GTC', 'Counter target spell. Its controller puts the top two cards of his or her library into his or her graveyard.').
card_first_print('psychic strike', 'GTC').
card_image_name('psychic strike'/'GTC', 'psychic strike').
card_uid('psychic strike'/'GTC', 'GTC:Psychic Strike:psychic strike').
card_rarity('psychic strike'/'GTC', 'Common').
card_artist('psychic strike'/'GTC', 'Mathias Kollros').
card_number('psychic strike'/'GTC', '189').
card_flavor_text('psychic strike'/'GTC', 'Until more information could be gleaned about Niv-Mizzet\'s intentions, Lazav took every opportunity to stall his experiments.').
card_multiverse_id('psychic strike'/'GTC', '366296').
card_watermark('psychic strike'/'GTC', 'Dimir').

card_in_set('purge the profane', 'GTC').
card_original_type('purge the profane'/'GTC', 'Sorcery').
card_original_text('purge the profane'/'GTC', 'Target opponent discards two cards and you gain 2 life.').
card_first_print('purge the profane', 'GTC').
card_image_name('purge the profane'/'GTC', 'purge the profane').
card_uid('purge the profane'/'GTC', 'GTC:Purge the Profane:purge the profane').
card_rarity('purge the profane'/'GTC', 'Common').
card_artist('purge the profane'/'GTC', 'Michael C. Hayes').
card_number('purge the profane'/'GTC', '190').
card_flavor_text('purge the profane'/'GTC', 'The Orzhov hear the plans of the other guilds through the guilty thoughts of the disloyal.').
card_multiverse_id('purge the profane'/'GTC', '366393').
card_watermark('purge the profane'/'GTC', 'Orzhov').

card_in_set('rapid hybridization', 'GTC').
card_original_type('rapid hybridization'/'GTC', 'Instant').
card_original_text('rapid hybridization'/'GTC', 'Destroy target creature. It can\'t be regenerated. That creature\'s controller puts a 3/3 green Frog Lizard creature token onto the battlefield.').
card_first_print('rapid hybridization', 'GTC').
card_image_name('rapid hybridization'/'GTC', 'rapid hybridization').
card_uid('rapid hybridization'/'GTC', 'GTC:Rapid Hybridization:rapid hybridization').
card_rarity('rapid hybridization'/'GTC', 'Uncommon').
card_artist('rapid hybridization'/'GTC', 'Jack Wang').
card_number('rapid hybridization'/'GTC', '44').
card_flavor_text('rapid hybridization'/'GTC', '\"We\'ve merged your life essence with that of several creatures at once. You\'re welcome.\"\n—Speaker Trifon').
card_multiverse_id('rapid hybridization'/'GTC', '366388').

card_in_set('razortip whip', 'GTC').
card_original_type('razortip whip'/'GTC', 'Artifact').
card_original_text('razortip whip'/'GTC', '{1}, {T}: Razortip Whip deals 1 damage to target opponent.').
card_first_print('razortip whip', 'GTC').
card_image_name('razortip whip'/'GTC', 'razortip whip').
card_uid('razortip whip'/'GTC', 'GTC:Razortip Whip:razortip whip').
card_rarity('razortip whip'/'GTC', 'Common').
card_artist('razortip whip'/'GTC', 'James Paick').
card_number('razortip whip'/'GTC', '235').
card_flavor_text('razortip whip'/'GTC', '\"It no longer surprises me what shows up in our raid of a Rakdos revue.\"\n—Janik Bara, Wojek captain').
card_multiverse_id('razortip whip'/'GTC', '366405').

card_in_set('realmwright', 'GTC').
card_original_type('realmwright'/'GTC', 'Creature — Vedalken Wizard').
card_original_text('realmwright'/'GTC', 'As Realmwright enters the battlefield, choose a basic land type.\nLands you control are the chosen type in addition to their other types.').
card_first_print('realmwright', 'GTC').
card_image_name('realmwright'/'GTC', 'realmwright').
card_uid('realmwright'/'GTC', 'GTC:Realmwright:realmwright').
card_rarity('realmwright'/'GTC', 'Rare').
card_artist('realmwright'/'GTC', 'Slawomir Maniak').
card_number('realmwright'/'GTC', '45').
card_flavor_text('realmwright'/'GTC', 'There is not a single inch of Ravnica that hasn\'t been altered at one point or another to fit someone\'s whims.').
card_multiverse_id('realmwright'/'GTC', '366315').

card_in_set('righteous charge', 'GTC').
card_original_type('righteous charge'/'GTC', 'Sorcery').
card_original_text('righteous charge'/'GTC', 'Creatures you control get +2/+2 until end of turn.').
card_image_name('righteous charge'/'GTC', 'righteous charge').
card_uid('righteous charge'/'GTC', 'GTC:Righteous Charge:righteous charge').
card_rarity('righteous charge'/'GTC', 'Uncommon').
card_artist('righteous charge'/'GTC', 'Svetlin Velinov').
card_number('righteous charge'/'GTC', '23').
card_flavor_text('righteous charge'/'GTC', '\"In the name of peace, moderation, and decency—wipe them all out!\"').
card_multiverse_id('righteous charge'/'GTC', '366288').

card_in_set('riot gear', 'GTC').
card_original_type('riot gear'/'GTC', 'Artifact — Equipment').
card_original_text('riot gear'/'GTC', 'Equipped creature gets +1/+2.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('riot gear', 'GTC').
card_image_name('riot gear'/'GTC', 'riot gear').
card_uid('riot gear'/'GTC', 'GTC:Riot Gear:riot gear').
card_rarity('riot gear'/'GTC', 'Common').
card_artist('riot gear'/'GTC', 'Jack Wang').
card_number('riot gear'/'GTC', '236').
card_flavor_text('riot gear'/'GTC', 'Any meeting between guilds requires proper equipment.').
card_multiverse_id('riot gear'/'GTC', '366349').

card_in_set('ripscale predator', 'GTC').
card_original_type('ripscale predator'/'GTC', 'Creature — Lizard').
card_original_text('ripscale predator'/'GTC', 'Ripscale Predator can\'t be blocked except by two or more creatures.').
card_first_print('ripscale predator', 'GTC').
card_image_name('ripscale predator'/'GTC', 'ripscale predator').
card_uid('ripscale predator'/'GTC', 'GTC:Ripscale Predator:ripscale predator').
card_rarity('ripscale predator'/'GTC', 'Uncommon').
card_artist('ripscale predator'/'GTC', 'Volkan Baga').
card_number('ripscale predator'/'GTC', '103').
card_flavor_text('ripscale predator'/'GTC', 'As Ravnica\'s wilds expanded, even advocates for nature had to confront fiercer natural threats.').
card_multiverse_id('ripscale predator'/'GTC', '366362').

card_in_set('rubblebelt raiders', 'GTC').
card_original_type('rubblebelt raiders'/'GTC', 'Creature — Human Warrior').
card_original_text('rubblebelt raiders'/'GTC', 'Whenever Rubblebelt Raiders attacks, put a +1/+1 counter on it for each attacking creature you control.').
card_first_print('rubblebelt raiders', 'GTC').
card_image_name('rubblebelt raiders'/'GTC', 'rubblebelt raiders').
card_uid('rubblebelt raiders'/'GTC', 'GTC:Rubblebelt Raiders:rubblebelt raiders').
card_rarity('rubblebelt raiders'/'GTC', 'Rare').
card_artist('rubblebelt raiders'/'GTC', 'Chippy').
card_number('rubblebelt raiders'/'GTC', '224').
card_flavor_text('rubblebelt raiders'/'GTC', '\"This city will perish, and the Gruul will cheer as the boar-god crushes the last bricks into dust.\"\n—Nikya of the Old Ways').
card_multiverse_id('rubblebelt raiders'/'GTC', '366462').
card_watermark('rubblebelt raiders'/'GTC', 'Gruul').

card_in_set('rubblehulk', 'GTC').
card_original_type('rubblehulk'/'GTC', 'Creature — Elemental').
card_original_text('rubblehulk'/'GTC', 'Rubblehulk\'s power and toughness are each equal to the number of lands you control.\nBloodrush — {1}{R}{G}, Discard Rubblehulk: Target attacking creature gets +X/+X until end of turn, where X is the number of lands you control.').
card_image_name('rubblehulk'/'GTC', 'rubblehulk').
card_uid('rubblehulk'/'GTC', 'GTC:Rubblehulk:rubblehulk').
card_rarity('rubblehulk'/'GTC', 'Rare').
card_artist('rubblehulk'/'GTC', 'Raymond Swanland').
card_number('rubblehulk'/'GTC', '191').
card_multiverse_id('rubblehulk'/'GTC', '366424').
card_watermark('rubblehulk'/'GTC', 'Gruul').

card_in_set('ruination wurm', 'GTC').
card_original_type('ruination wurm'/'GTC', 'Creature — Wurm').
card_original_text('ruination wurm'/'GTC', '').
card_first_print('ruination wurm', 'GTC').
card_image_name('ruination wurm'/'GTC', 'ruination wurm').
card_uid('ruination wurm'/'GTC', 'GTC:Ruination Wurm:ruination wurm').
card_rarity('ruination wurm'/'GTC', 'Common').
card_artist('ruination wurm'/'GTC', 'Dave Kendall').
card_number('ruination wurm'/'GTC', '192').
card_flavor_text('ruination wurm'/'GTC', 'When the architects of Ravnica claim a structure will stand against a wurm, they never mention for how long.').
card_multiverse_id('ruination wurm'/'GTC', '366235').
card_watermark('ruination wurm'/'GTC', 'Gruul').

card_in_set('rust scarab', 'GTC').
card_original_type('rust scarab'/'GTC', 'Creature — Insect').
card_original_text('rust scarab'/'GTC', 'Whenever Rust Scarab becomes blocked, you may destroy target artifact or enchantment defending player controls.').
card_first_print('rust scarab', 'GTC').
card_image_name('rust scarab'/'GTC', 'rust scarab').
card_uid('rust scarab'/'GTC', 'GTC:Rust Scarab:rust scarab').
card_rarity('rust scarab'/'GTC', 'Uncommon').
card_artist('rust scarab'/'GTC', 'Adam Paquette').
card_number('rust scarab'/'GTC', '130').
card_flavor_text('rust scarab'/'GTC', '\"Behold nature\'s answer to the problem of society.\"\n—Nikya of the Old Ways').
card_multiverse_id('rust scarab'/'GTC', '366245').

card_in_set('sacred foundry', 'GTC').
card_original_type('sacred foundry'/'GTC', 'Land — Mountain Plains').
card_original_text('sacred foundry'/'GTC', '({T}: Add {R} or {W} to your mana pool.)\nAs Sacred Foundry enters the battlefield, you may pay 2 life. If you don\'t, Sacred Foundry enters the battlefield tapped.').
card_image_name('sacred foundry'/'GTC', 'sacred foundry').
card_uid('sacred foundry'/'GTC', 'GTC:Sacred Foundry:sacred foundry').
card_rarity('sacred foundry'/'GTC', 'Rare').
card_artist('sacred foundry'/'GTC', 'Sam Burley').
card_number('sacred foundry'/'GTC', '245').
card_flavor_text('sacred foundry'/'GTC', 'Burning fervor and fearsome skill create a lethal beauty terrible to behold.').
card_multiverse_id('sacred foundry'/'GTC', '366439').
card_watermark('sacred foundry'/'GTC', 'Boros').

card_in_set('sage\'s row denizen', 'GTC').
card_original_type('sage\'s row denizen'/'GTC', 'Creature — Vedalken Wizard').
card_original_text('sage\'s row denizen'/'GTC', 'Whenever another blue creature enters the battlefield under your control, target player puts the top two cards of his or her library into his or her graveyard.').
card_first_print('sage\'s row denizen', 'GTC').
card_image_name('sage\'s row denizen'/'GTC', 'sage\'s row denizen').
card_uid('sage\'s row denizen'/'GTC', 'GTC:Sage\'s Row Denizen:sage\'s row denizen').
card_rarity('sage\'s row denizen'/'GTC', 'Common').
card_artist('sage\'s row denizen'/'GTC', 'Svetlin Velinov').
card_number('sage\'s row denizen'/'GTC', '46').
card_flavor_text('sage\'s row denizen'/'GTC', '\"I offer you wisdom untainted by false loyalty, learning free of any guild\'s agenda.\"').
card_multiverse_id('sage\'s row denizen'/'GTC', '366406').

card_in_set('sapphire drake', 'GTC').
card_original_type('sapphire drake'/'GTC', 'Creature — Drake').
card_original_text('sapphire drake'/'GTC', 'Flying\nEach creature you control with a +1/+1 counter on it has flying.').
card_first_print('sapphire drake', 'GTC').
card_image_name('sapphire drake'/'GTC', 'sapphire drake').
card_uid('sapphire drake'/'GTC', 'GTC:Sapphire Drake:sapphire drake').
card_rarity('sapphire drake'/'GTC', 'Uncommon').
card_artist('sapphire drake'/'GTC', 'Steve Prescott').
card_number('sapphire drake'/'GTC', '47').
card_flavor_text('sapphire drake'/'GTC', 'Sapphire drakes can communicate over great distances, their calls resonating in the gems fixed in their skulls.').
card_multiverse_id('sapphire drake'/'GTC', '366421').

card_in_set('scab-clan charger', 'GTC').
card_original_type('scab-clan charger'/'GTC', 'Creature — Centaur Warrior').
card_original_text('scab-clan charger'/'GTC', 'Bloodrush — {1}{G}, Discard Scab-Clan Charger: Target attacking creature gets +2/+4 until end of turn.').
card_first_print('scab-clan charger', 'GTC').
card_image_name('scab-clan charger'/'GTC', 'scab-clan charger').
card_uid('scab-clan charger'/'GTC', 'GTC:Scab-Clan Charger:scab-clan charger').
card_rarity('scab-clan charger'/'GTC', 'Common').
card_artist('scab-clan charger'/'GTC', 'Nils Hamm').
card_number('scab-clan charger'/'GTC', '131').
card_flavor_text('scab-clan charger'/'GTC', '\"The only roads I wish to see are those beaten flat by wild hooves.\"').
card_multiverse_id('scab-clan charger'/'GTC', '366410').
card_watermark('scab-clan charger'/'GTC', 'Gruul').

card_in_set('scatter arc', 'GTC').
card_original_type('scatter arc'/'GTC', 'Instant').
card_original_text('scatter arc'/'GTC', 'Counter target noncreature spell.\nDraw a card.').
card_first_print('scatter arc', 'GTC').
card_image_name('scatter arc'/'GTC', 'scatter arc').
card_uid('scatter arc'/'GTC', 'GTC:Scatter Arc:scatter arc').
card_rarity('scatter arc'/'GTC', 'Common').
card_artist('scatter arc'/'GTC', 'Peter Mohrbacher').
card_number('scatter arc'/'GTC', '48').
card_flavor_text('scatter arc'/'GTC', '\"Should your chosen emissary be able to fluctuate triharmonic pentodes, however unlikely, I shall shunt excess negavolt radicals.\"').
card_multiverse_id('scatter arc'/'GTC', '366241').

card_in_set('scorchwalker', 'GTC').
card_original_type('scorchwalker'/'GTC', 'Creature — Elemental').
card_original_text('scorchwalker'/'GTC', 'Bloodrush — {1}{R}{R}, Discard Scorchwalker: Target attacking creature gets +5/+1 until end of turn.').
card_first_print('scorchwalker', 'GTC').
card_image_name('scorchwalker'/'GTC', 'scorchwalker').
card_uid('scorchwalker'/'GTC', 'GTC:Scorchwalker:scorchwalker').
card_rarity('scorchwalker'/'GTC', 'Common').
card_artist('scorchwalker'/'GTC', 'Anthony Palumbo').
card_number('scorchwalker'/'GTC', '104').
card_flavor_text('scorchwalker'/'GTC', '\"Borborygmos\'s elemental launched an assault, but the Izzet wards held and experiments continue.\"\n—Skyknight report on the Rubblebelt').
card_multiverse_id('scorchwalker'/'GTC', '366409').
card_watermark('scorchwalker'/'GTC', 'Gruul').

card_in_set('sepulchral primordial', 'GTC').
card_original_type('sepulchral primordial'/'GTC', 'Creature — Avatar').
card_original_text('sepulchral primordial'/'GTC', 'Intimidate\nWhen Sepulchral Primordial enters the battlefield, for each opponent, you may put up to one target creature card from that player\'s graveyard onto the battlefield under your control.').
card_first_print('sepulchral primordial', 'GTC').
card_image_name('sepulchral primordial'/'GTC', 'sepulchral primordial').
card_uid('sepulchral primordial'/'GTC', 'GTC:Sepulchral Primordial:sepulchral primordial').
card_rarity('sepulchral primordial'/'GTC', 'Rare').
card_artist('sepulchral primordial'/'GTC', 'Stephan Martiniere').
card_number('sepulchral primordial'/'GTC', '75').
card_multiverse_id('sepulchral primordial'/'GTC', '366285').

card_in_set('serene remembrance', 'GTC').
card_original_type('serene remembrance'/'GTC', 'Sorcery').
card_original_text('serene remembrance'/'GTC', 'Shuffle Serene Remembrance and up to three target cards from a single graveyard into their owners\' libraries.').
card_first_print('serene remembrance', 'GTC').
card_image_name('serene remembrance'/'GTC', 'serene remembrance').
card_uid('serene remembrance'/'GTC', 'GTC:Serene Remembrance:serene remembrance').
card_rarity('serene remembrance'/'GTC', 'Uncommon').
card_artist('serene remembrance'/'GTC', 'David Palumbo').
card_number('serene remembrance'/'GTC', '132').
card_flavor_text('serene remembrance'/'GTC', 'While Lazav schemed and Niv-Mizzet researched, Trostani sought the future in the whispers of the past.').
card_multiverse_id('serene remembrance'/'GTC', '366269').

card_in_set('shadow alley denizen', 'GTC').
card_original_type('shadow alley denizen'/'GTC', 'Creature — Vampire Rogue').
card_original_text('shadow alley denizen'/'GTC', 'Whenever another black creature enters the battlefield under your control, target creature gains intimidate until end of turn. (It can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)').
card_first_print('shadow alley denizen', 'GTC').
card_image_name('shadow alley denizen'/'GTC', 'shadow alley denizen').
card_uid('shadow alley denizen'/'GTC', 'GTC:Shadow Alley Denizen:shadow alley denizen').
card_rarity('shadow alley denizen'/'GTC', 'Common').
card_artist('shadow alley denizen'/'GTC', 'Cynthia Sheppard').
card_number('shadow alley denizen'/'GTC', '76').
card_multiverse_id('shadow alley denizen'/'GTC', '366307').

card_in_set('shadow slice', 'GTC').
card_original_type('shadow slice'/'GTC', 'Sorcery').
card_original_text('shadow slice'/'GTC', 'Target opponent loses 3 life.\nCipher (Then you may exile this spell card encoded on a creature you control. Whenever that creature deals combat damage to a player, its controller may cast a copy of the encoded card without paying its mana cost.)').
card_first_print('shadow slice', 'GTC').
card_image_name('shadow slice'/'GTC', 'shadow slice').
card_uid('shadow slice'/'GTC', 'GTC:Shadow Slice:shadow slice').
card_rarity('shadow slice'/'GTC', 'Common').
card_artist('shadow slice'/'GTC', 'Raymond Swanland').
card_number('shadow slice'/'GTC', '77').
card_multiverse_id('shadow slice'/'GTC', '366455').
card_watermark('shadow slice'/'GTC', 'Dimir').

card_in_set('shambleshark', 'GTC').
card_original_type('shambleshark'/'GTC', 'Creature — Fish Crab').
card_original_text('shambleshark'/'GTC', 'Flash (You may cast this spell any time you could cast an instant.)\nEvolve (Whenever a creature enters the battlefield under your control, if that creature has greater power or toughness than this creature, put a +1/+1 counter on this creature.)').
card_first_print('shambleshark', 'GTC').
card_image_name('shambleshark'/'GTC', 'shambleshark').
card_uid('shambleshark'/'GTC', 'GTC:Shambleshark:shambleshark').
card_rarity('shambleshark'/'GTC', 'Common').
card_artist('shambleshark'/'GTC', 'Wesley Burt').
card_number('shambleshark'/'GTC', '193').
card_multiverse_id('shambleshark'/'GTC', '366478').
card_watermark('shambleshark'/'GTC', 'Simic').

card_in_set('shattering blow', 'GTC').
card_original_type('shattering blow'/'GTC', 'Instant').
card_original_text('shattering blow'/'GTC', 'Exile target artifact.').
card_first_print('shattering blow', 'GTC').
card_image_name('shattering blow'/'GTC', 'shattering blow').
card_uid('shattering blow'/'GTC', 'GTC:Shattering Blow:shattering blow').
card_rarity('shattering blow'/'GTC', 'Common').
card_artist('shattering blow'/'GTC', 'Steve Prescott').
card_number('shattering blow'/'GTC', '225').
card_flavor_text('shattering blow'/'GTC', '\"Aurelia seems too eager to solve her problems by delivering them to oblivion.\"\n—Prime Speaker Zegana').
card_multiverse_id('shattering blow'/'GTC', '366331').
card_watermark('shattering blow'/'GTC', 'Boros').

card_in_set('shielded passage', 'GTC').
card_original_type('shielded passage'/'GTC', 'Instant').
card_original_text('shielded passage'/'GTC', 'Prevent all damage that would be dealt to target creature this turn.').
card_first_print('shielded passage', 'GTC').
card_image_name('shielded passage'/'GTC', 'shielded passage').
card_uid('shielded passage'/'GTC', 'GTC:Shielded Passage:shielded passage').
card_rarity('shielded passage'/'GTC', 'Common').
card_artist('shielded passage'/'GTC', 'Raymond Swanland').
card_number('shielded passage'/'GTC', '24').
card_flavor_text('shielded passage'/'GTC', 'Izzet delved into the depths of Ravnica\'s past, and Selesnya followed without fear.').
card_multiverse_id('shielded passage'/'GTC', '366332').

card_in_set('signal the clans', 'GTC').
card_original_type('signal the clans'/'GTC', 'Instant').
card_original_text('signal the clans'/'GTC', 'Search your library for three creature cards and reveal them. If you reveal three cards with different names, choose one of them at random and put that card into your hand. Shuffle the rest into your library.').
card_first_print('signal the clans', 'GTC').
card_image_name('signal the clans'/'GTC', 'signal the clans').
card_uid('signal the clans'/'GTC', 'GTC:Signal the Clans:signal the clans').
card_rarity('signal the clans'/'GTC', 'Rare').
card_artist('signal the clans'/'GTC', 'Dave Kendall').
card_number('signal the clans'/'GTC', '194').
card_flavor_text('signal the clans'/'GTC', 'Gruul clanfolk obey few rules, but all feel bound to answer the call of the horn.').
card_multiverse_id('signal the clans'/'GTC', '366365').
card_watermark('signal the clans'/'GTC', 'Gruul').

card_in_set('simic charm', 'GTC').
card_original_type('simic charm'/'GTC', 'Instant').
card_original_text('simic charm'/'GTC', 'Choose one — Target creature gets +3/+3 until end of turn; or permanents you control gain hexproof until end of turn; or return target creature to its owner\'s hand.').
card_first_print('simic charm', 'GTC').
card_image_name('simic charm'/'GTC', 'simic charm').
card_uid('simic charm'/'GTC', 'GTC:Simic Charm:simic charm').
card_rarity('simic charm'/'GTC', 'Uncommon').
card_artist('simic charm'/'GTC', 'Zoltan Boros').
card_number('simic charm'/'GTC', '195').
card_flavor_text('simic charm'/'GTC', '\"We measure our progress in heartbeats.\"\n—Prime Speaker Zegana').
card_multiverse_id('simic charm'/'GTC', '366463').
card_watermark('simic charm'/'GTC', 'Simic').

card_in_set('simic fluxmage', 'GTC').
card_original_type('simic fluxmage'/'GTC', 'Creature — Merfolk Wizard').
card_original_text('simic fluxmage'/'GTC', 'Evolve (Whenever a creature enters the battlefield under your control, if that creature has greater power or toughness than this creature, put a +1/+1 counter on this creature.)\n{1}{U}, {T}: Move a +1/+1 counter from Simic Fluxmage onto target creature.').
card_first_print('simic fluxmage', 'GTC').
card_image_name('simic fluxmage'/'GTC', 'simic fluxmage').
card_uid('simic fluxmage'/'GTC', 'GTC:Simic Fluxmage:simic fluxmage').
card_rarity('simic fluxmage'/'GTC', 'Uncommon').
card_artist('simic fluxmage'/'GTC', 'Karl Kopinski').
card_number('simic fluxmage'/'GTC', '49').
card_multiverse_id('simic fluxmage'/'GTC', '366274').
card_watermark('simic fluxmage'/'GTC', 'Simic').

card_in_set('simic guildgate', 'GTC').
card_original_type('simic guildgate'/'GTC', 'Land — Gate').
card_original_text('simic guildgate'/'GTC', 'Simic Guildgate enters the battlefield tapped.\n{T}: Add {G} or {U} to your mana pool.').
card_first_print('simic guildgate', 'GTC').
card_image_name('simic guildgate'/'GTC', 'simic guildgate').
card_uid('simic guildgate'/'GTC', 'GTC:Simic Guildgate:simic guildgate').
card_rarity('simic guildgate'/'GTC', 'Common').
card_artist('simic guildgate'/'GTC', 'Svetlin Velinov').
card_number('simic guildgate'/'GTC', '246').
card_flavor_text('simic guildgate'/'GTC', 'Enter and comprehend the perfection of orchestrated life.').
card_multiverse_id('simic guildgate'/'GTC', '366383').
card_watermark('simic guildgate'/'GTC', 'Simic').

card_in_set('simic keyrune', 'GTC').
card_original_type('simic keyrune'/'GTC', 'Artifact').
card_original_text('simic keyrune'/'GTC', '{T}: Add {G} or {U} to your mana pool.\n{G}{U}: Simic Keyrune becomes a 2/3 green and blue Crab artifact creature with hexproof until end of turn. (It can\'t be the target of spells or abilities your opponents control.)').
card_first_print('simic keyrune', 'GTC').
card_image_name('simic keyrune'/'GTC', 'simic keyrune').
card_uid('simic keyrune'/'GTC', 'GTC:Simic Keyrune:simic keyrune').
card_rarity('simic keyrune'/'GTC', 'Uncommon').
card_artist('simic keyrune'/'GTC', 'Daniel Ljunggren').
card_number('simic keyrune'/'GTC', '237').
card_multiverse_id('simic keyrune'/'GTC', '366357').
card_watermark('simic keyrune'/'GTC', 'Simic').

card_in_set('simic manipulator', 'GTC').
card_original_type('simic manipulator'/'GTC', 'Creature — Mutant Wizard').
card_original_text('simic manipulator'/'GTC', 'Evolve (Whenever a creature enters the battlefield under your control, if that creature has greater power or toughness than this creature, put a +1/+1 counter on this creature.)\n{T}, Remove one or more +1/+1 counters from Simic Manipulator: Gain control of target creature with power less than or equal to the number of +1/+1 counters removed this way.').
card_first_print('simic manipulator', 'GTC').
card_image_name('simic manipulator'/'GTC', 'simic manipulator').
card_uid('simic manipulator'/'GTC', 'GTC:Simic Manipulator:simic manipulator').
card_rarity('simic manipulator'/'GTC', 'Rare').
card_artist('simic manipulator'/'GTC', 'Maciej Kuciara').
card_number('simic manipulator'/'GTC', '50').
card_multiverse_id('simic manipulator'/'GTC', '366343').
card_watermark('simic manipulator'/'GTC', 'Simic').

card_in_set('skarrg goliath', 'GTC').
card_original_type('skarrg goliath'/'GTC', 'Creature — Beast').
card_original_text('skarrg goliath'/'GTC', 'Trample\nBloodrush — {5}{G}{G}, Discard Skarrg Goliath: Target attacking creature gets +9/+9 and gains trample until end of turn.').
card_image_name('skarrg goliath'/'GTC', 'skarrg goliath').
card_uid('skarrg goliath'/'GTC', 'GTC:Skarrg Goliath:skarrg goliath').
card_rarity('skarrg goliath'/'GTC', 'Rare').
card_artist('skarrg goliath'/'GTC', 'Scott Chou').
card_number('skarrg goliath'/'GTC', '133').
card_flavor_text('skarrg goliath'/'GTC', '\"They bind us with their laws. We free ourselves with nature\'s fist.\"\n—Nikya of the Old Ways').
card_multiverse_id('skarrg goliath'/'GTC', '366458').
card_watermark('skarrg goliath'/'GTC', 'Gruul').

card_in_set('skarrg guildmage', 'GTC').
card_original_type('skarrg guildmage'/'GTC', 'Creature — Human Shaman').
card_original_text('skarrg guildmage'/'GTC', '{R}{G}: Creatures you control gain trample until end of turn.\n{1}{R}{G}: Target land you control becomes a 4/4 Elemental creature until end of turn. It\'s still a land.').
card_first_print('skarrg guildmage', 'GTC').
card_image_name('skarrg guildmage'/'GTC', 'skarrg guildmage').
card_uid('skarrg guildmage'/'GTC', 'GTC:Skarrg Guildmage:skarrg guildmage').
card_rarity('skarrg guildmage'/'GTC', 'Uncommon').
card_artist('skarrg guildmage'/'GTC', 'Aleksi Briclot').
card_number('skarrg guildmage'/'GTC', '196').
card_multiverse_id('skarrg guildmage'/'GTC', '366350').
card_watermark('skarrg guildmage'/'GTC', 'Gruul').

card_in_set('skinbrand goblin', 'GTC').
card_original_type('skinbrand goblin'/'GTC', 'Creature — Goblin Warrior').
card_original_text('skinbrand goblin'/'GTC', 'Bloodrush — {R}, Discard Skinbrand Goblin: Target attacking creature gets +2/+1 until end of turn.').
card_first_print('skinbrand goblin', 'GTC').
card_image_name('skinbrand goblin'/'GTC', 'skinbrand goblin').
card_uid('skinbrand goblin'/'GTC', 'GTC:Skinbrand Goblin:skinbrand goblin').
card_rarity('skinbrand goblin'/'GTC', 'Common').
card_artist('skinbrand goblin'/'GTC', 'Marco Nelor').
card_number('skinbrand goblin'/'GTC', '105').
card_flavor_text('skinbrand goblin'/'GTC', '\"These Gruul skin-runes—discern whether their patterns evoke streets, leylines, or both. You have four days.\"\n—Niv-Mizzet, to his assistant').
card_multiverse_id('skinbrand goblin'/'GTC', '366434').
card_watermark('skinbrand goblin'/'GTC', 'Gruul').

card_in_set('skullcrack', 'GTC').
card_original_type('skullcrack'/'GTC', 'Instant').
card_original_text('skullcrack'/'GTC', 'Players can\'t gain life this turn. Damage can\'t be prevented this turn. Skullcrack deals 3 damage to target player.').
card_first_print('skullcrack', 'GTC').
card_image_name('skullcrack'/'GTC', 'skullcrack').
card_uid('skullcrack'/'GTC', 'GTC:Skullcrack:skullcrack').
card_rarity('skullcrack'/'GTC', 'Uncommon').
card_artist('skullcrack'/'GTC', 'Dave Kendall').
card_number('skullcrack'/'GTC', '106').
card_flavor_text('skullcrack'/'GTC', 'The lawmage\'s argument was clever and well reasoned, but Blunk\'s response proved irrefutable.').
card_multiverse_id('skullcrack'/'GTC', '366238').

card_in_set('skyblinder staff', 'GTC').
card_original_type('skyblinder staff'/'GTC', 'Artifact — Equipment').
card_original_text('skyblinder staff'/'GTC', 'Equipped creature gets +1/+0 and can\'t be blocked by creatures with flying.\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('skyblinder staff', 'GTC').
card_image_name('skyblinder staff'/'GTC', 'skyblinder staff').
card_uid('skyblinder staff'/'GTC', 'GTC:Skyblinder Staff:skyblinder staff').
card_rarity('skyblinder staff'/'GTC', 'Common').
card_artist('skyblinder staff'/'GTC', 'Mark Winters').
card_number('skyblinder staff'/'GTC', '238').
card_flavor_text('skyblinder staff'/'GTC', '\"If bats are the only thing between me and a mountain of Orzhov coins, then I\'m about to be a very rich thief.\"').
card_multiverse_id('skyblinder staff'/'GTC', '366346').

card_in_set('skygames', 'GTC').
card_original_type('skygames'/'GTC', 'Enchantment — Aura').
card_original_text('skygames'/'GTC', 'Enchant land\nEnchanted land has \"{T}: Target creature gains flying until end of turn. Activate this ability only any time you could cast a sorcery.\"').
card_first_print('skygames', 'GTC').
card_image_name('skygames'/'GTC', 'skygames').
card_uid('skygames'/'GTC', 'GTC:Skygames:skygames').
card_rarity('skygames'/'GTC', 'Common').
card_artist('skygames'/'GTC', 'Sam Burley').
card_number('skygames'/'GTC', '51').
card_flavor_text('skygames'/'GTC', 'Both Boros and Azorius scout the games in hopes of recruiting the finest athletes.').
card_multiverse_id('skygames'/'GTC', '366323').

card_in_set('skyknight legionnaire', 'GTC').
card_original_type('skyknight legionnaire'/'GTC', 'Creature — Human Knight').
card_original_text('skyknight legionnaire'/'GTC', 'Flying, haste').
card_image_name('skyknight legionnaire'/'GTC', 'skyknight legionnaire').
card_uid('skyknight legionnaire'/'GTC', 'GTC:Skyknight Legionnaire:skyknight legionnaire').
card_rarity('skyknight legionnaire'/'GTC', 'Common').
card_artist('skyknight legionnaire'/'GTC', 'Anthony Palumbo').
card_number('skyknight legionnaire'/'GTC', '197').
card_flavor_text('skyknight legionnaire'/'GTC', 'Millennium Platform, once a monument to peace, was quickly seized by the skyknights for its strategic aerial location.').
card_multiverse_id('skyknight legionnaire'/'GTC', '366338').
card_watermark('skyknight legionnaire'/'GTC', 'Boros').

card_in_set('slate street ruffian', 'GTC').
card_original_type('slate street ruffian'/'GTC', 'Creature — Human Warrior').
card_original_text('slate street ruffian'/'GTC', 'Whenever Slate Street Ruffian becomes blocked, defending player discards a card.').
card_first_print('slate street ruffian', 'GTC').
card_image_name('slate street ruffian'/'GTC', 'slate street ruffian').
card_uid('slate street ruffian'/'GTC', 'GTC:Slate Street Ruffian:slate street ruffian').
card_rarity('slate street ruffian'/'GTC', 'Common').
card_artist('slate street ruffian'/'GTC', 'Jim Murray').
card_number('slate street ruffian'/'GTC', '78').
card_flavor_text('slate street ruffian'/'GTC', '\"Merciless to the point of psychosis. Let\'s give him a job.\"\n—Zelinas, Orzhov recruiter').
card_multiverse_id('slate street ruffian'/'GTC', '366366').

card_in_set('slaughterhorn', 'GTC').
card_original_type('slaughterhorn'/'GTC', 'Creature — Beast').
card_original_text('slaughterhorn'/'GTC', 'Bloodrush — {G}, Discard Slaughterhorn: Target attacking creature gets +3/+2 until end of turn.').
card_first_print('slaughterhorn', 'GTC').
card_image_name('slaughterhorn'/'GTC', 'slaughterhorn').
card_uid('slaughterhorn'/'GTC', 'GTC:Slaughterhorn:slaughterhorn').
card_rarity('slaughterhorn'/'GTC', 'Common').
card_artist('slaughterhorn'/'GTC', 'Steve Prescott').
card_number('slaughterhorn'/'GTC', '134').
card_flavor_text('slaughterhorn'/'GTC', 'The Orzhov clergy would never cross its path, but sending thrull couriers to do so is cheap and entertaining.').
card_multiverse_id('slaughterhorn'/'GTC', '366281').
card_watermark('slaughterhorn'/'GTC', 'Gruul').

card_in_set('smite', 'GTC').
card_original_type('smite'/'GTC', 'Instant').
card_original_text('smite'/'GTC', 'Destroy target blocked creature.').
card_image_name('smite'/'GTC', 'smite').
card_uid('smite'/'GTC', 'GTC:Smite:smite').
card_rarity('smite'/'GTC', 'Common').
card_artist('smite'/'GTC', 'Zoltan Boros').
card_number('smite'/'GTC', '25').
card_flavor_text('smite'/'GTC', '\"Sure, today of all days, he asked to borrow my boots!\"\n—Darijo, Ivy Street ruffian').
card_multiverse_id('smite'/'GTC', '366456').

card_in_set('smog elemental', 'GTC').
card_original_type('smog elemental'/'GTC', 'Creature — Elemental').
card_original_text('smog elemental'/'GTC', 'Flying\nCreatures with flying your opponents control get -1/-1.').
card_first_print('smog elemental', 'GTC').
card_image_name('smog elemental'/'GTC', 'smog elemental').
card_uid('smog elemental'/'GTC', 'GTC:Smog Elemental:smog elemental').
card_rarity('smog elemental'/'GTC', 'Uncommon').
card_artist('smog elemental'/'GTC', 'Yeong-Hao Han').
card_number('smog elemental'/'GTC', '79').
card_flavor_text('smog elemental'/'GTC', '\"The skies used to be my refuge, a place of peace away from the chaos below. Now, with every flight, death seems to follow me like a shadow.\"\n—Writings of Cica, Skyjek scout').
card_multiverse_id('smog elemental'/'GTC', '366257').

card_in_set('soul ransom', 'GTC').
card_original_type('soul ransom'/'GTC', 'Enchantment — Aura').
card_original_text('soul ransom'/'GTC', 'Enchant creature\nYou control enchanted creature.\nDiscard two cards: Soul Ransom\'s controller sacrifices it, then draws two cards. Only any opponent may activate this ability.').
card_first_print('soul ransom', 'GTC').
card_image_name('soul ransom'/'GTC', 'soul ransom').
card_uid('soul ransom'/'GTC', 'GTC:Soul Ransom:soul ransom').
card_rarity('soul ransom'/'GTC', 'Rare').
card_artist('soul ransom'/'GTC', 'Steve Argyle').
card_number('soul ransom'/'GTC', '198').
card_multiverse_id('soul ransom'/'GTC', '366351').
card_watermark('soul ransom'/'GTC', 'Dimir').

card_in_set('spark trooper', 'GTC').
card_original_type('spark trooper'/'GTC', 'Creature — Elemental Soldier').
card_original_text('spark trooper'/'GTC', 'Trample, lifelink, haste\nAt the beginning of the end step, sacrifice Spark Trooper.').
card_first_print('spark trooper', 'GTC').
card_image_name('spark trooper'/'GTC', 'spark trooper').
card_uid('spark trooper'/'GTC', 'GTC:Spark Trooper:spark trooper').
card_rarity('spark trooper'/'GTC', 'Rare').
card_artist('spark trooper'/'GTC', 'James Ryman').
card_number('spark trooper'/'GTC', '199').
card_flavor_text('spark trooper'/'GTC', 'Izzet-designed conductors atop Sunhome charge empty sets of armor that give form to the fluctuating elementals.').
card_multiverse_id('spark trooper'/'GTC', '366422').
card_watermark('spark trooper'/'GTC', 'Boros').

card_in_set('spell rupture', 'GTC').
card_original_type('spell rupture'/'GTC', 'Instant').
card_original_text('spell rupture'/'GTC', 'Counter target spell unless its controller pays {X}, where X is the greatest power among creatures you control.').
card_first_print('spell rupture', 'GTC').
card_image_name('spell rupture'/'GTC', 'spell rupture').
card_uid('spell rupture'/'GTC', 'GTC:Spell Rupture:spell rupture').
card_rarity('spell rupture'/'GTC', 'Common').
card_artist('spell rupture'/'GTC', 'Kev Walker').
card_number('spell rupture'/'GTC', '52').
card_flavor_text('spell rupture'/'GTC', '\"Like water, mana will drain from an inferior vessel.\"\n—Jalan Tosk, Simic forcemage').
card_multiverse_id('spell rupture'/'GTC', '366372').

card_in_set('spire tracer', 'GTC').
card_original_type('spire tracer'/'GTC', 'Creature — Elf Scout').
card_original_text('spire tracer'/'GTC', 'Spire Tracer can\'t be blocked except by creatures with flying or reach.').
card_first_print('spire tracer', 'GTC').
card_image_name('spire tracer'/'GTC', 'spire tracer').
card_uid('spire tracer'/'GTC', 'GTC:Spire Tracer:spire tracer').
card_rarity('spire tracer'/'GTC', 'Common').
card_artist('spire tracer'/'GTC', 'Christopher Moeller').
card_number('spire tracer'/'GTC', '135').
card_flavor_text('spire tracer'/'GTC', 'Cartographers on Ravnica chart not just the horizontal, but also the vertical.').
card_multiverse_id('spire tracer'/'GTC', '366236').

card_in_set('stolen identity', 'GTC').
card_original_type('stolen identity'/'GTC', 'Sorcery').
card_original_text('stolen identity'/'GTC', 'Put a token onto the battlefield that\'s a copy of target artifact or creature.\nCipher (Then you may exile this spell card encoded on a creature you control. Whenever that creature deals combat damage to a player, its controller may cast a copy of the encoded card without paying its mana cost.)').
card_first_print('stolen identity', 'GTC').
card_image_name('stolen identity'/'GTC', 'stolen identity').
card_uid('stolen identity'/'GTC', 'GTC:Stolen Identity:stolen identity').
card_rarity('stolen identity'/'GTC', 'Rare').
card_artist('stolen identity'/'GTC', 'Clint Cearley').
card_number('stolen identity'/'GTC', '53').
card_multiverse_id('stolen identity'/'GTC', '366427').
card_watermark('stolen identity'/'GTC', 'Dimir').

card_in_set('stomping ground', 'GTC').
card_original_type('stomping ground'/'GTC', 'Land — Mountain Forest').
card_original_text('stomping ground'/'GTC', '({T}: Add {R} or {G} to your mana pool.)\nAs Stomping Ground enters the battlefield, you may pay 2 life. If you don\'t, Stomping Ground enters the battlefield tapped.').
card_image_name('stomping ground'/'GTC', 'stomping ground').
card_uid('stomping ground'/'GTC', 'GTC:Stomping Ground:stomping ground').
card_rarity('stomping ground'/'GTC', 'Rare').
card_artist('stomping ground'/'GTC', 'David Palumbo').
card_number('stomping ground'/'GTC', '247').
card_flavor_text('stomping ground'/'GTC', '\"Roots, ruins, and room to fight. All the comforts of home.\"\n—Domri Rade').
card_multiverse_id('stomping ground'/'GTC', '366232').
card_watermark('stomping ground'/'GTC', 'Gruul').

card_in_set('structural collapse', 'GTC').
card_original_type('structural collapse'/'GTC', 'Sorcery').
card_original_text('structural collapse'/'GTC', 'Target player sacrifices an artifact and a land. Structural Collapse deals 2 damage to that player.').
card_first_print('structural collapse', 'GTC').
card_image_name('structural collapse'/'GTC', 'structural collapse').
card_uid('structural collapse'/'GTC', 'GTC:Structural Collapse:structural collapse').
card_rarity('structural collapse'/'GTC', 'Common').
card_artist('structural collapse'/'GTC', 'Sam Burley').
card_number('structural collapse'/'GTC', '107').
card_flavor_text('structural collapse'/'GTC', 'The Gruul holiday of Rauck-Chauv always ends the same way.').
card_multiverse_id('structural collapse'/'GTC', '366376').

card_in_set('sunhome guildmage', 'GTC').
card_original_type('sunhome guildmage'/'GTC', 'Creature — Human Wizard').
card_original_text('sunhome guildmage'/'GTC', '{1}{R}{W}: Creatures you control get +1/+0 until end of turn.\n{2}{R}{W}: Put a 1/1 red and white Soldier creature token with haste onto the battlefield.').
card_first_print('sunhome guildmage', 'GTC').
card_image_name('sunhome guildmage'/'GTC', 'sunhome guildmage').
card_uid('sunhome guildmage'/'GTC', 'GTC:Sunhome Guildmage:sunhome guildmage').
card_rarity('sunhome guildmage'/'GTC', 'Uncommon').
card_artist('sunhome guildmage'/'GTC', 'Eric Deschamps').
card_number('sunhome guildmage'/'GTC', '200').
card_multiverse_id('sunhome guildmage'/'GTC', '366429').
card_watermark('sunhome guildmage'/'GTC', 'Boros').

card_in_set('sylvan primordial', 'GTC').
card_original_type('sylvan primordial'/'GTC', 'Creature — Avatar').
card_original_text('sylvan primordial'/'GTC', 'Reach\nWhen Sylvan Primordial enters the battlefield, for each opponent, destroy target noncreature permanent that player controls. For each permanent destroyed this way, search your library for a Forest card and put that card onto the battlefield tapped. Then shuffle your library.').
card_first_print('sylvan primordial', 'GTC').
card_image_name('sylvan primordial'/'GTC', 'sylvan primordial').
card_uid('sylvan primordial'/'GTC', 'GTC:Sylvan Primordial:sylvan primordial').
card_rarity('sylvan primordial'/'GTC', 'Rare').
card_artist('sylvan primordial'/'GTC', 'Stephan Martiniere').
card_number('sylvan primordial'/'GTC', '136').
card_multiverse_id('sylvan primordial'/'GTC', '366282').

card_in_set('syndic of tithes', 'GTC').
card_original_type('syndic of tithes'/'GTC', 'Creature — Human Cleric').
card_original_text('syndic of tithes'/'GTC', 'Extort (Whenever you cast a spell, you may pay {W/B}. If you do, each opponent loses 1 life and you gain that much life.)').
card_first_print('syndic of tithes', 'GTC').
card_image_name('syndic of tithes'/'GTC', 'syndic of tithes').
card_uid('syndic of tithes'/'GTC', 'GTC:Syndic of Tithes:syndic of tithes').
card_rarity('syndic of tithes'/'GTC', 'Common').
card_artist('syndic of tithes'/'GTC', 'Steve Prescott').
card_number('syndic of tithes'/'GTC', '26').
card_flavor_text('syndic of tithes'/'GTC', '\"Entrance is free. Donations are required.\"\n—Notice outside Vizkopa Bank').
card_multiverse_id('syndic of tithes'/'GTC', '366387').
card_watermark('syndic of tithes'/'GTC', 'Orzhov').

card_in_set('syndicate enforcer', 'GTC').
card_original_type('syndicate enforcer'/'GTC', 'Creature — Human Rogue').
card_original_text('syndicate enforcer'/'GTC', 'Extort (Whenever you cast a spell, you may pay {W/B}. If you do, each opponent loses 1 life and you gain that much life.)').
card_first_print('syndicate enforcer', 'GTC').
card_image_name('syndicate enforcer'/'GTC', 'syndicate enforcer').
card_uid('syndicate enforcer'/'GTC', 'GTC:Syndicate Enforcer:syndicate enforcer').
card_rarity('syndicate enforcer'/'GTC', 'Common').
card_artist('syndicate enforcer'/'GTC', 'Steven Belledin').
card_number('syndicate enforcer'/'GTC', '80').
card_flavor_text('syndicate enforcer'/'GTC', 'Even the darkest corners of Ravnica still lie in the shadow of the Church.').
card_multiverse_id('syndicate enforcer'/'GTC', '366243').
card_watermark('syndicate enforcer'/'GTC', 'Orzhov').

card_in_set('thespian\'s stage', 'GTC').
card_original_type('thespian\'s stage'/'GTC', 'Land').
card_original_text('thespian\'s stage'/'GTC', '{T}: Add {1} to your mana pool.\n{2}, {T}: Thespian\'s Stage becomes a copy of target land and gains this ability.').
card_first_print('thespian\'s stage', 'GTC').
card_image_name('thespian\'s stage'/'GTC', 'thespian\'s stage').
card_uid('thespian\'s stage'/'GTC', 'GTC:Thespian\'s Stage:thespian\'s stage').
card_rarity('thespian\'s stage'/'GTC', 'Rare').
card_artist('thespian\'s stage'/'GTC', 'John Avon').
card_number('thespian\'s stage'/'GTC', '248').
card_flavor_text('thespian\'s stage'/'GTC', 'Amid rumors of war, the third act of The Absolution of the Guildpact was quickly rewritten as a tragedy.').
card_multiverse_id('thespian\'s stage'/'GTC', '366353').

card_in_set('thrull parasite', 'GTC').
card_original_type('thrull parasite'/'GTC', 'Creature — Thrull').
card_original_text('thrull parasite'/'GTC', 'Extort (Whenever you cast a spell, you may pay {W/B}. If you do, each opponent loses 1 life and you gain that much life.)\n{T}, Pay 2 life: Remove a counter from target nonland permanent.').
card_first_print('thrull parasite', 'GTC').
card_image_name('thrull parasite'/'GTC', 'thrull parasite').
card_uid('thrull parasite'/'GTC', 'GTC:Thrull Parasite:thrull parasite').
card_rarity('thrull parasite'/'GTC', 'Uncommon').
card_artist('thrull parasite'/'GTC', 'Clint Cearley').
card_number('thrull parasite'/'GTC', '81').
card_flavor_text('thrull parasite'/'GTC', 'The hunger of a vampire and the subtlety of a tax collector.').
card_multiverse_id('thrull parasite'/'GTC', '366437').
card_watermark('thrull parasite'/'GTC', 'Orzhov').

card_in_set('tin street market', 'GTC').
card_original_type('tin street market'/'GTC', 'Enchantment — Aura').
card_original_text('tin street market'/'GTC', 'Enchant land\nEnchanted land has \"{T}, Discard a card: Draw a card.\"').
card_first_print('tin street market', 'GTC').
card_image_name('tin street market'/'GTC', 'tin street market').
card_uid('tin street market'/'GTC', 'GTC:Tin Street Market:tin street market').
card_rarity('tin street market'/'GTC', 'Common').
card_artist('tin street market'/'GTC', 'Noah Bradley').
card_number('tin street market'/'GTC', '108').
card_flavor_text('tin street market'/'GTC', '\"Sometimes I stroll along Tin Street and watch citizens busy with their daily lives. It reminds me why I do my job.\"\n—Arrester Lavinia, Tenth Precinct').
card_multiverse_id('tin street market'/'GTC', '366318').

card_in_set('totally lost', 'GTC').
card_original_type('totally lost'/'GTC', 'Instant').
card_original_text('totally lost'/'GTC', 'Put target nonland permanent on top of its owner\'s library.').
card_first_print('totally lost', 'GTC').
card_image_name('totally lost'/'GTC', 'totally lost').
card_uid('totally lost'/'GTC', 'GTC:Totally Lost:totally lost').
card_rarity('totally lost'/'GTC', 'Common').
card_artist('totally lost'/'GTC', 'David Palumbo').
card_number('totally lost'/'GTC', '54').
card_flavor_text('totally lost'/'GTC', 'Fblthp had always hated crowds.').
card_multiverse_id('totally lost'/'GTC', '366433').

card_in_set('tower defense', 'GTC').
card_original_type('tower defense'/'GTC', 'Instant').
card_original_text('tower defense'/'GTC', 'Creatures you control get +0/+5 and gain reach until end of turn.').
card_first_print('tower defense', 'GTC').
card_image_name('tower defense'/'GTC', 'tower defense').
card_uid('tower defense'/'GTC', 'GTC:Tower Defense:tower defense').
card_rarity('tower defense'/'GTC', 'Uncommon').
card_artist('tower defense'/'GTC', 'Seb McKinnon').
card_number('tower defense'/'GTC', '137').
card_flavor_text('tower defense'/'GTC', '\"The drakes are practice. We may one day need to bring down a sky swallower, or maybe even Rakdos himself.\"\n—Korun Nar, Rubblebelt hunter').
card_multiverse_id('tower defense'/'GTC', '366404').

card_in_set('towering thunderfist', 'GTC').
card_original_type('towering thunderfist'/'GTC', 'Creature — Giant Soldier').
card_original_text('towering thunderfist'/'GTC', '{W}: Towering Thunderfist gains vigilance until end of turn.').
card_first_print('towering thunderfist', 'GTC').
card_image_name('towering thunderfist'/'GTC', 'towering thunderfist').
card_uid('towering thunderfist'/'GTC', 'GTC:Towering Thunderfist:towering thunderfist').
card_rarity('towering thunderfist'/'GTC', 'Common').
card_artist('towering thunderfist'/'GTC', 'Zoltan Boros').
card_number('towering thunderfist'/'GTC', '109').
card_flavor_text('towering thunderfist'/'GTC', '\"Nothing ruins a party like one of those big killjoys bursting through the door and setting all the guests free.\"\n—Ruba, Rakdos cultist').
card_multiverse_id('towering thunderfist'/'GTC', '366355').
card_watermark('towering thunderfist'/'GTC', 'Boros').

card_in_set('treasury thrull', 'GTC').
card_original_type('treasury thrull'/'GTC', 'Creature — Thrull').
card_original_text('treasury thrull'/'GTC', 'Extort (Whenever you cast a spell, you may pay {W/B}. If you do, each opponent loses 1 life and you gain that much life.)\nWhenever Treasury Thrull attacks, you may return target artifact, creature, or enchantment card from your graveyard to your hand.').
card_image_name('treasury thrull'/'GTC', 'treasury thrull').
card_uid('treasury thrull'/'GTC', 'GTC:Treasury Thrull:treasury thrull').
card_rarity('treasury thrull'/'GTC', 'Rare').
card_artist('treasury thrull'/'GTC', 'Mark Zug').
card_number('treasury thrull'/'GTC', '201').
card_multiverse_id('treasury thrull'/'GTC', '366359').
card_watermark('treasury thrull'/'GTC', 'Orzhov').

card_in_set('truefire paladin', 'GTC').
card_original_type('truefire paladin'/'GTC', 'Creature — Human Knight').
card_original_text('truefire paladin'/'GTC', 'Vigilance\n{R}{W}: Truefire Paladin gets +2/+0 until end of turn.\n{R}{W}: Truefire Paladin gains first strike until end of turn.').
card_first_print('truefire paladin', 'GTC').
card_image_name('truefire paladin'/'GTC', 'truefire paladin').
card_uid('truefire paladin'/'GTC', 'GTC:Truefire Paladin:truefire paladin').
card_rarity('truefire paladin'/'GTC', 'Uncommon').
card_artist('truefire paladin'/'GTC', 'Michael C. Hayes').
card_number('truefire paladin'/'GTC', '202').
card_multiverse_id('truefire paladin'/'GTC', '366322').
card_watermark('truefire paladin'/'GTC', 'Boros').

card_in_set('undercity informer', 'GTC').
card_original_type('undercity informer'/'GTC', 'Creature — Human Rogue').
card_original_text('undercity informer'/'GTC', '{1}, Sacrifice a creature: Target player reveals cards from the top of his or her library until he or she reveals a land card, then puts those cards into his or her graveyard.').
card_first_print('undercity informer', 'GTC').
card_image_name('undercity informer'/'GTC', 'undercity informer').
card_uid('undercity informer'/'GTC', 'GTC:Undercity Informer:undercity informer').
card_rarity('undercity informer'/'GTC', 'Uncommon').
card_artist('undercity informer'/'GTC', 'Raymond Swanland').
card_number('undercity informer'/'GTC', '82').
card_flavor_text('undercity informer'/'GTC', '\"Scandal and slander are never welcome news. That\'s why I\'m never the messenger.\"').
card_multiverse_id('undercity informer'/'GTC', '366271').

card_in_set('undercity plague', 'GTC').
card_original_type('undercity plague'/'GTC', 'Sorcery').
card_original_text('undercity plague'/'GTC', 'Target player loses 1 life, discards a card, then sacrifices a permanent.\nCipher (Then you may exile this spell card encoded on a creature you control. Whenever that creature deals combat damage to a player, its controller may cast a copy of the encoded card without paying its mana cost.)').
card_first_print('undercity plague', 'GTC').
card_image_name('undercity plague'/'GTC', 'undercity plague').
card_uid('undercity plague'/'GTC', 'GTC:Undercity Plague:undercity plague').
card_rarity('undercity plague'/'GTC', 'Rare').
card_artist('undercity plague'/'GTC', 'Vincent Proce').
card_number('undercity plague'/'GTC', '83').
card_multiverse_id('undercity plague'/'GTC', '366231').
card_watermark('undercity plague'/'GTC', 'Dimir').

card_in_set('unexpected results', 'GTC').
card_original_type('unexpected results'/'GTC', 'Sorcery').
card_original_text('unexpected results'/'GTC', 'Shuffle your library, then reveal the top card. If it\'s a nonland card, you may cast it without paying its mana cost. If it\'s a land card, you may put it onto the battlefield and return Unexpected Results to its owner\'s hand.').
card_first_print('unexpected results', 'GTC').
card_image_name('unexpected results'/'GTC', 'unexpected results').
card_uid('unexpected results'/'GTC', 'GTC:Unexpected Results:unexpected results').
card_rarity('unexpected results'/'GTC', 'Rare').
card_artist('unexpected results'/'GTC', 'Mike Bierek').
card_number('unexpected results'/'GTC', '203').
card_multiverse_id('unexpected results'/'GTC', '366248').
card_watermark('unexpected results'/'GTC', 'Simic').

card_in_set('urban evolution', 'GTC').
card_original_type('urban evolution'/'GTC', 'Sorcery').
card_original_text('urban evolution'/'GTC', 'Draw three cards. You may play an additional land this turn.').
card_first_print('urban evolution', 'GTC').
card_image_name('urban evolution'/'GTC', 'urban evolution').
card_uid('urban evolution'/'GTC', 'GTC:Urban Evolution:urban evolution').
card_rarity('urban evolution'/'GTC', 'Uncommon').
card_artist('urban evolution'/'GTC', 'Eytan Zana').
card_number('urban evolution'/'GTC', '204').
card_flavor_text('urban evolution'/'GTC', 'As the Simic released more of their krasis experiments, they required new habitats, always at the expense of the locals.').
card_multiverse_id('urban evolution'/'GTC', '366419').
card_watermark('urban evolution'/'GTC', 'Simic').

card_in_set('urbis protector', 'GTC').
card_original_type('urbis protector'/'GTC', 'Creature — Human Cleric').
card_original_text('urbis protector'/'GTC', 'When Urbis Protector enters the battlefield, put a 4/4 white Angel creature token with flying onto the battlefield.').
card_first_print('urbis protector', 'GTC').
card_image_name('urbis protector'/'GTC', 'urbis protector').
card_uid('urbis protector'/'GTC', 'GTC:Urbis Protector:urbis protector').
card_rarity('urbis protector'/'GTC', 'Uncommon').
card_artist('urbis protector'/'GTC', 'Steve Argyle').
card_number('urbis protector'/'GTC', '27').
card_flavor_text('urbis protector'/'GTC', '\"I hear that Boros is losing its angels to the Gateless Movement. I\'m sure this comes as no surprise to you.\"\n—Teysa, to Lazav').
card_multiverse_id('urbis protector'/'GTC', '366415').

card_in_set('verdant haven', 'GTC').
card_original_type('verdant haven'/'GTC', 'Enchantment — Aura').
card_original_text('verdant haven'/'GTC', 'Enchant land\nWhen Verdant Haven enters the battlefield, you gain 2 life.\nWhenever enchanted land is tapped for mana, its controller adds one mana of any color to his or her mana pool (in addition to the mana the land produces).').
card_first_print('verdant haven', 'GTC').
card_image_name('verdant haven'/'GTC', 'verdant haven').
card_uid('verdant haven'/'GTC', 'GTC:Verdant Haven:verdant haven').
card_rarity('verdant haven'/'GTC', 'Common').
card_artist('verdant haven'/'GTC', 'Daniel Ljunggren').
card_number('verdant haven'/'GTC', '138').
card_multiverse_id('verdant haven'/'GTC', '366471').

card_in_set('viashino shanktail', 'GTC').
card_original_type('viashino shanktail'/'GTC', 'Creature — Viashino Warrior').
card_original_text('viashino shanktail'/'GTC', 'First strike\nBloodrush — {2}{R}, Discard Viashino Shanktail: Target attacking creature gets +3/+1 and gains first strike until end of turn.').
card_first_print('viashino shanktail', 'GTC').
card_image_name('viashino shanktail'/'GTC', 'viashino shanktail').
card_uid('viashino shanktail'/'GTC', 'GTC:Viashino Shanktail:viashino shanktail').
card_rarity('viashino shanktail'/'GTC', 'Uncommon').
card_artist('viashino shanktail'/'GTC', 'Kev Walker').
card_number('viashino shanktail'/'GTC', '110').
card_flavor_text('viashino shanktail'/'GTC', 'The Slizt Clan\'s battle cry is an impressive display of fury, defiance, and spit.').
card_multiverse_id('viashino shanktail'/'GTC', '366264').
card_watermark('viashino shanktail'/'GTC', 'Gruul').

card_in_set('vizkopa confessor', 'GTC').
card_original_type('vizkopa confessor'/'GTC', 'Creature — Human Cleric').
card_original_text('vizkopa confessor'/'GTC', 'Extort (Whenever you cast a spell, you may pay {W/B}. If you do, each opponent loses 1 life and you gain that much life.)\nWhen Vizkopa Confessor enters the battlefield, pay any amount of life. Target opponent reveals that many cards from his or her hand. You choose one of them and exile it.').
card_first_print('vizkopa confessor', 'GTC').
card_image_name('vizkopa confessor'/'GTC', 'vizkopa confessor').
card_uid('vizkopa confessor'/'GTC', 'GTC:Vizkopa Confessor:vizkopa confessor').
card_rarity('vizkopa confessor'/'GTC', 'Uncommon').
card_artist('vizkopa confessor'/'GTC', 'Ryan Pancoast').
card_number('vizkopa confessor'/'GTC', '205').
card_multiverse_id('vizkopa confessor'/'GTC', '366267').
card_watermark('vizkopa confessor'/'GTC', 'Orzhov').

card_in_set('vizkopa guildmage', 'GTC').
card_original_type('vizkopa guildmage'/'GTC', 'Creature — Human Wizard').
card_original_text('vizkopa guildmage'/'GTC', '{1}{W}{B}: Target creature gains lifelink until end of turn.\n{1}{W}{B}: Whenever you gain life this turn, each opponent loses that much life.').
card_first_print('vizkopa guildmage', 'GTC').
card_image_name('vizkopa guildmage'/'GTC', 'vizkopa guildmage').
card_uid('vizkopa guildmage'/'GTC', 'GTC:Vizkopa Guildmage:vizkopa guildmage').
card_rarity('vizkopa guildmage'/'GTC', 'Uncommon').
card_artist('vizkopa guildmage'/'GTC', 'Tyler Jacobson').
card_number('vizkopa guildmage'/'GTC', '206').
card_multiverse_id('vizkopa guildmage'/'GTC', '366371').
card_watermark('vizkopa guildmage'/'GTC', 'Orzhov').

card_in_set('voidwalk', 'GTC').
card_original_type('voidwalk'/'GTC', 'Sorcery').
card_original_text('voidwalk'/'GTC', 'Exile target creature. Return it to the battlefield under its owner\'s control at the beginning of the next end step.\nCipher (Then you may exile this spell card encoded on a creature you control. Whenever that creature deals combat damage to a player, its controller may cast a copy of the encoded card without paying its mana cost.)').
card_first_print('voidwalk', 'GTC').
card_image_name('voidwalk'/'GTC', 'voidwalk').
card_uid('voidwalk'/'GTC', 'GTC:Voidwalk:voidwalk').
card_rarity('voidwalk'/'GTC', 'Uncommon').
card_artist('voidwalk'/'GTC', 'James Ryman').
card_number('voidwalk'/'GTC', '55').
card_multiverse_id('voidwalk'/'GTC', '366474').
card_watermark('voidwalk'/'GTC', 'Dimir').

card_in_set('warmind infantry', 'GTC').
card_original_type('warmind infantry'/'GTC', 'Creature — Elemental Soldier').
card_original_text('warmind infantry'/'GTC', 'Battalion — Whenever Warmind Infantry and at least two other creatures attack, Warmind Infantry gets +2/+0 until end of turn.').
card_first_print('warmind infantry', 'GTC').
card_image_name('warmind infantry'/'GTC', 'warmind infantry').
card_uid('warmind infantry'/'GTC', 'GTC:Warmind Infantry:warmind infantry').
card_rarity('warmind infantry'/'GTC', 'Common').
card_artist('warmind infantry'/'GTC', 'Greg Staples').
card_number('warmind infantry'/'GTC', '111').
card_flavor_text('warmind infantry'/'GTC', 'Before Aurelia denounced Niv-Mizzet, the two guilds had collaborated on the Warmind Initiative in pursuit of the ultimate soldier.').
card_multiverse_id('warmind infantry'/'GTC', '366272').
card_watermark('warmind infantry'/'GTC', 'Boros').

card_in_set('wasteland viper', 'GTC').
card_original_type('wasteland viper'/'GTC', 'Creature — Snake').
card_original_text('wasteland viper'/'GTC', 'Deathtouch\nBloodrush — {G}, Discard Wasteland Viper: Target attacking creature gets +1/+2 and gains deathtouch until end of turn.').
card_first_print('wasteland viper', 'GTC').
card_image_name('wasteland viper'/'GTC', 'wasteland viper').
card_uid('wasteland viper'/'GTC', 'GTC:Wasteland Viper:wasteland viper').
card_rarity('wasteland viper'/'GTC', 'Uncommon').
card_artist('wasteland viper'/'GTC', 'Lucas Graciano').
card_number('wasteland viper'/'GTC', '139').
card_flavor_text('wasteland viper'/'GTC', '\"The Gruul embrace all things poisonous and living in the cracks of society.\"\n—Teysa Karlov, Grand Envoy of Orzhov').
card_multiverse_id('wasteland viper'/'GTC', '366417').
card_watermark('wasteland viper'/'GTC', 'Gruul').

card_in_set('watery grave', 'GTC').
card_original_type('watery grave'/'GTC', 'Land — Island Swamp').
card_original_text('watery grave'/'GTC', '({T}: Add {U} or {B} to your mana pool.)\nAs Watery Grave enters the battlefield, you may pay 2 life. If you don\'t, Watery Grave enters the battlefield tapped.').
card_image_name('watery grave'/'GTC', 'watery grave').
card_uid('watery grave'/'GTC', 'GTC:Watery Grave:watery grave').
card_rarity('watery grave'/'GTC', 'Rare').
card_artist('watery grave'/'GTC', 'Raymond Swanland').
card_number('watery grave'/'GTC', '249').
card_flavor_text('watery grave'/'GTC', '\"I fear that as we scurry after phantoms, the Dimir pull nine puppet strings.\"\n—Ral Zarek').
card_multiverse_id('watery grave'/'GTC', '366335').
card_watermark('watery grave'/'GTC', 'Dimir').

card_in_set('way of the thief', 'GTC').
card_original_type('way of the thief'/'GTC', 'Enchantment — Aura').
card_original_text('way of the thief'/'GTC', 'Enchant creature\nEnchanted creature gets +2/+2.\nEnchanted creature is unblockable as long as you control a Gate.').
card_first_print('way of the thief', 'GTC').
card_image_name('way of the thief'/'GTC', 'way of the thief').
card_uid('way of the thief'/'GTC', 'GTC:Way of the Thief:way of the thief').
card_rarity('way of the thief'/'GTC', 'Common').
card_artist('way of the thief'/'GTC', 'Igor Kieryluk').
card_number('way of the thief'/'GTC', '56').
card_flavor_text('way of the thief'/'GTC', 'A true shortcut isn\'t a way other people don\'t know; it\'s a way other people can\'t go.').
card_multiverse_id('way of the thief'/'GTC', '366333').

card_in_set('whispering madness', 'GTC').
card_original_type('whispering madness'/'GTC', 'Sorcery').
card_original_text('whispering madness'/'GTC', 'Each player discards his or her hand, then draws cards equal to the greatest number of cards a player discarded this way.\nCipher (Then you may exile this spell card encoded on a creature you control. Whenever that creature deals combat damage to a player, its controller may cast a copy of the encoded card without paying its mana cost.)').
card_first_print('whispering madness', 'GTC').
card_image_name('whispering madness'/'GTC', 'whispering madness').
card_uid('whispering madness'/'GTC', 'GTC:Whispering Madness:whispering madness').
card_rarity('whispering madness'/'GTC', 'Rare').
card_artist('whispering madness'/'GTC', 'Clint Cearley').
card_number('whispering madness'/'GTC', '207').
card_multiverse_id('whispering madness'/'GTC', '366263').
card_watermark('whispering madness'/'GTC', 'Dimir').

card_in_set('wight of precinct six', 'GTC').
card_original_type('wight of precinct six'/'GTC', 'Creature — Zombie').
card_original_text('wight of precinct six'/'GTC', 'Wight of Precinct Six gets +1/+1 for each creature card in your opponents\' graveyards.').
card_first_print('wight of precinct six', 'GTC').
card_image_name('wight of precinct six'/'GTC', 'wight of precinct six').
card_uid('wight of precinct six'/'GTC', 'GTC:Wight of Precinct Six:wight of precinct six').
card_rarity('wight of precinct six'/'GTC', 'Uncommon').
card_artist('wight of precinct six'/'GTC', 'Ryan Barger').
card_number('wight of precinct six'/'GTC', '84').
card_flavor_text('wight of precinct six'/'GTC', 'Even the lost and undead need a protector.').
card_multiverse_id('wight of precinct six'/'GTC', '366239').

card_in_set('wildwood rebirth', 'GTC').
card_original_type('wildwood rebirth'/'GTC', 'Instant').
card_original_text('wildwood rebirth'/'GTC', 'Return target creature card from your graveyard to your hand.').
card_first_print('wildwood rebirth', 'GTC').
card_image_name('wildwood rebirth'/'GTC', 'wildwood rebirth').
card_uid('wildwood rebirth'/'GTC', 'GTC:Wildwood Rebirth:wildwood rebirth').
card_rarity('wildwood rebirth'/'GTC', 'Common').
card_artist('wildwood rebirth'/'GTC', 'Dan Scott').
card_number('wildwood rebirth'/'GTC', '140').
card_flavor_text('wildwood rebirth'/'GTC', 'In Ravnica, there are maladies far more difficult to treat than death.').
card_multiverse_id('wildwood rebirth'/'GTC', '366425').

card_in_set('wojek halberdiers', 'GTC').
card_original_type('wojek halberdiers'/'GTC', 'Creature — Human Soldier').
card_original_text('wojek halberdiers'/'GTC', 'Battalion — Whenever Wojek Halberdiers and at least two other creatures attack, Wojek Halberdiers gains first strike until end of turn.').
card_first_print('wojek halberdiers', 'GTC').
card_image_name('wojek halberdiers'/'GTC', 'wojek halberdiers').
card_uid('wojek halberdiers'/'GTC', 'GTC:Wojek Halberdiers:wojek halberdiers').
card_rarity('wojek halberdiers'/'GTC', 'Common').
card_artist('wojek halberdiers'/'GTC', 'Nic Klein').
card_number('wojek halberdiers'/'GTC', '208').
card_flavor_text('wojek halberdiers'/'GTC', 'Tired of Boros interference, the Rakdos unleashed hellraiser goblins, hoping their corpses would weigh down the halberds.').
card_multiverse_id('wojek halberdiers'/'GTC', '366265').
card_watermark('wojek halberdiers'/'GTC', 'Boros').

card_in_set('wrecking ogre', 'GTC').
card_original_type('wrecking ogre'/'GTC', 'Creature — Ogre Warrior').
card_original_text('wrecking ogre'/'GTC', 'Double strike\nBloodrush — {3}{R}{R}, Discard Wrecking Ogre: Target attacking creature gets +3/+3 and gains double strike until end of turn.').
card_first_print('wrecking ogre', 'GTC').
card_image_name('wrecking ogre'/'GTC', 'wrecking ogre').
card_uid('wrecking ogre'/'GTC', 'GTC:Wrecking Ogre:wrecking ogre').
card_rarity('wrecking ogre'/'GTC', 'Rare').
card_artist('wrecking ogre'/'GTC', 'Nils Hamm').
card_number('wrecking ogre'/'GTC', '112').
card_flavor_text('wrecking ogre'/'GTC', 'If no opponent can be found, ogres of the Bolrac Clan will often pick fights with buildings.').
card_multiverse_id('wrecking ogre'/'GTC', '366446').
card_watermark('wrecking ogre'/'GTC', 'Gruul').

card_in_set('zameck guildmage', 'GTC').
card_original_type('zameck guildmage'/'GTC', 'Creature — Elf Wizard').
card_original_text('zameck guildmage'/'GTC', '{G}{U}: This turn, each creature you control enters the battlefield with an additional +1/+1 counter on it.\n{G}{U}, Remove a +1/+1 counter from a creature you control: Draw a card.').
card_image_name('zameck guildmage'/'GTC', 'zameck guildmage').
card_uid('zameck guildmage'/'GTC', 'GTC:Zameck Guildmage:zameck guildmage').
card_rarity('zameck guildmage'/'GTC', 'Uncommon').
card_artist('zameck guildmage'/'GTC', 'Chase Stone').
card_number('zameck guildmage'/'GTC', '209').
card_multiverse_id('zameck guildmage'/'GTC', '366454').
card_watermark('zameck guildmage'/'GTC', 'Simic').

card_in_set('zarichi tiger', 'GTC').
card_original_type('zarichi tiger'/'GTC', 'Creature — Cat').
card_original_text('zarichi tiger'/'GTC', '{1}{W}, {T}: You gain 2 life.').
card_first_print('zarichi tiger', 'GTC').
card_image_name('zarichi tiger'/'GTC', 'zarichi tiger').
card_uid('zarichi tiger'/'GTC', 'GTC:Zarichi Tiger:zarichi tiger').
card_rarity('zarichi tiger'/'GTC', 'Common').
card_artist('zarichi tiger'/'GTC', 'Nic Klein').
card_number('zarichi tiger'/'GTC', '28').
card_flavor_text('zarichi tiger'/'GTC', '\"The taming and consecration of these creatures is evidence of our faith.\"\n—Resimir, Zarichi Temple priest').
card_multiverse_id('zarichi tiger'/'GTC', '366249').

card_in_set('zhur-taa swine', 'GTC').
card_original_type('zhur-taa swine'/'GTC', 'Creature — Boar').
card_original_text('zhur-taa swine'/'GTC', 'Bloodrush — {1}{R}{G}, Discard Zhur-Taa Swine: Target attacking creature gets +5/+4 until end of turn.').
card_first_print('zhur-taa swine', 'GTC').
card_image_name('zhur-taa swine'/'GTC', 'zhur-taa swine').
card_uid('zhur-taa swine'/'GTC', 'GTC:Zhur-Taa Swine:zhur-taa swine').
card_rarity('zhur-taa swine'/'GTC', 'Common').
card_artist('zhur-taa swine'/'GTC', 'Yeong-Hao Han').
card_number('zhur-taa swine'/'GTC', '210').
card_flavor_text('zhur-taa swine'/'GTC', 'Gurley was the first to domesticate one, though his widow didn\'t take much solace in that accomplishment.').
card_multiverse_id('zhur-taa swine'/'GTC', '366252').
card_watermark('zhur-taa swine'/'GTC', 'Gruul').
