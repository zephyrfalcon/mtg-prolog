% Fifth Edition

set('5ED').
set_name('5ED', 'Fifth Edition').
set_release_date('5ED', '1997-03-24').
set_border('5ED', 'white').
set_type('5ED', 'core').

card_in_set('abbey gargoyles', '5ED').
card_original_type('abbey gargoyles'/'5ED', 'Summon — Gargoyles').
card_original_text('abbey gargoyles'/'5ED', 'Flying, protection from red').
card_image_name('abbey gargoyles'/'5ED', 'abbey gargoyles').
card_uid('abbey gargoyles'/'5ED', '5ED:Abbey Gargoyles:abbey gargoyles').
card_rarity('abbey gargoyles'/'5ED', 'Uncommon').
card_artist('abbey gargoyles'/'5ED', 'Christopher Rush').
card_flavor_text('abbey gargoyles'/'5ED', '\"They don\'t move, do they?\"\n\"Only for the wrong sort of people.\"\n\"Are we the wrong sort of people?\"').
card_multiverse_id('abbey gargoyles'/'5ED', '4098').

card_in_set('abyssal specter', '5ED').
card_original_type('abyssal specter'/'5ED', 'Summon — Specter').
card_original_text('abyssal specter'/'5ED', 'Flying\nIf Abyssal Specter damages any player, he or she chooses and discards a card.').
card_image_name('abyssal specter'/'5ED', 'abyssal specter').
card_uid('abyssal specter'/'5ED', '5ED:Abyssal Specter:abyssal specter').
card_rarity('abyssal specter'/'5ED', 'Uncommon').
card_artist('abyssal specter'/'5ED', 'George Pratt').
card_flavor_text('abyssal specter'/'5ED', '\"Mystic shadow, bending near me, /\nWho art thou? / Whence come ye?\"\n—Stephen Crane,\n\"Mystic shadow, bending near me\"').
card_multiverse_id('abyssal specter'/'5ED', '3822').

card_in_set('adarkar wastes', '5ED').
card_original_type('adarkar wastes'/'5ED', 'Land').
card_original_text('adarkar wastes'/'5ED', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {W} or {U} to your mana pool. Adarkar Wastes deals 1 damage to you.').
card_image_name('adarkar wastes'/'5ED', 'adarkar wastes').
card_uid('adarkar wastes'/'5ED', '5ED:Adarkar Wastes:adarkar wastes').
card_rarity('adarkar wastes'/'5ED', 'Rare').
card_artist('adarkar wastes'/'5ED', 'Gary Leach').
card_multiverse_id('adarkar wastes'/'5ED', '4175').

card_in_set('æther storm', '5ED').
card_original_type('æther storm'/'5ED', 'Enchantment').
card_original_text('æther storm'/'5ED', 'Summon spells cannot be played.\nAny player may pay 4 life to bury Æther Storm.').
card_image_name('æther storm'/'5ED', 'aether storm').
card_uid('æther storm'/'5ED', '5ED:Æther Storm:aether storm').
card_rarity('æther storm'/'5ED', 'Uncommon').
card_artist('æther storm'/'5ED', 'Mark Tedin').
card_flavor_text('æther storm'/'5ED', 'Thunder in the wind / no rain / peace mourns its passing.').
card_multiverse_id('æther storm'/'5ED', '3891').

card_in_set('air elemental', '5ED').
card_original_type('air elemental'/'5ED', 'Summon — Elemental').
card_original_text('air elemental'/'5ED', 'Flying').
card_image_name('air elemental'/'5ED', 'air elemental').
card_uid('air elemental'/'5ED', '5ED:Air Elemental:air elemental').
card_rarity('air elemental'/'5ED', 'Uncommon').
card_artist('air elemental'/'5ED', 'D. Alexander Gregory').
card_flavor_text('air elemental'/'5ED', 'These spirits of the air are winsome and wild and cannot be truly contained. Only marginally intelligent, they often substitute whimsy for strategy, delighting in mischief and mayhem.').
card_multiverse_id('air elemental'/'5ED', '3892').

card_in_set('akron legionnaire', '5ED').
card_original_type('akron legionnaire'/'5ED', 'Summon — Legionnaire').
card_original_text('akron legionnaire'/'5ED', 'Except for Legionnaires and artifact creatures, creatures you control cannot attack.').
card_image_name('akron legionnaire'/'5ED', 'akron legionnaire').
card_uid('akron legionnaire'/'5ED', '5ED:Akron Legionnaire:akron legionnaire').
card_rarity('akron legionnaire'/'5ED', 'Rare').
card_artist('akron legionnaire'/'5ED', 'Mark Poole').
card_multiverse_id('akron legionnaire'/'5ED', '4099').

card_in_set('alabaster potion', '5ED').
card_original_type('alabaster potion'/'5ED', 'Instant').
card_original_text('alabaster potion'/'5ED', 'Target player gains X life, or prevent X damage to any creature or player.').
card_image_name('alabaster potion'/'5ED', 'alabaster potion').
card_uid('alabaster potion'/'5ED', '5ED:Alabaster Potion:alabaster potion').
card_rarity('alabaster potion'/'5ED', 'Common').
card_artist('alabaster potion'/'5ED', 'Harold McNeill').
card_flavor_text('alabaster potion'/'5ED', 'Healing is a matter of time, but it is sometimes also a matter of opportunity.\n—D\'Avenant proverb').
card_multiverse_id('alabaster potion'/'5ED', '4100').

card_in_set('aladdin\'s ring', '5ED').
card_original_type('aladdin\'s ring'/'5ED', 'Artifact').
card_original_text('aladdin\'s ring'/'5ED', '{8}, {T}: Aladdin\'s Ring deals 4 damage to target creature or player.').
card_image_name('aladdin\'s ring'/'5ED', 'aladdin\'s ring').
card_uid('aladdin\'s ring'/'5ED', '5ED:Aladdin\'s Ring:aladdin\'s ring').
card_rarity('aladdin\'s ring'/'5ED', 'Rare').
card_artist('aladdin\'s ring'/'5ED', 'Stuart Griffin').
card_flavor_text('aladdin\'s ring'/'5ED', '\". . . [T]he magician drew a ring off his finger . . . , saying: \'It is a talisman against all evil, so long as you obey me.\'\"\n—The Arabian Nights,\nJunior Classics trans.').
card_multiverse_id('aladdin\'s ring'/'5ED', '3758').

card_in_set('ambush party', '5ED').
card_original_type('ambush party'/'5ED', 'Summon — Ambush Party').
card_original_text('ambush party'/'5ED', 'First strike\nAmbush Party is unaffected by summoning sickness.').
card_image_name('ambush party'/'5ED', 'ambush party').
card_uid('ambush party'/'5ED', '5ED:Ambush Party:ambush party').
card_rarity('ambush party'/'5ED', 'Common').
card_artist('ambush party'/'5ED', 'Charles Gillespie').
card_flavor_text('ambush party'/'5ED', '\"Fair fight? I\'ll take survival over chivalry any day!\"\n—Tor Wauki, Bandit King').
card_multiverse_id('ambush party'/'5ED', '4029').

card_in_set('amulet of kroog', '5ED').
card_original_type('amulet of kroog'/'5ED', 'Artifact').
card_original_text('amulet of kroog'/'5ED', '{2}, {T}: Prevent 1 damage to any creature or player.').
card_image_name('amulet of kroog'/'5ED', 'amulet of kroog').
card_uid('amulet of kroog'/'5ED', '5ED:Amulet of Kroog:amulet of kroog').
card_rarity('amulet of kroog'/'5ED', 'Common').
card_artist('amulet of kroog'/'5ED', 'Margaret Organ-Kean').
card_flavor_text('amulet of kroog'/'5ED', 'Among the first allies Urza gained were the people of Kroog. As a sign of friendship, Urza gave the healers of the city potent amulets; afterwards, thousands journeyed to Kroog in hope of healing.').
card_multiverse_id('amulet of kroog'/'5ED', '3759').

card_in_set('an-havva constable', '5ED').
card_original_type('an-havva constable'/'5ED', 'Summon — Constable').
card_original_text('an-havva constable'/'5ED', 'An-Havva Constable has toughness equal to 1 plus the number of green creatures in play.').
card_image_name('an-havva constable'/'5ED', 'an-havva constable').
card_uid('an-havva constable'/'5ED', '5ED:An-Havva Constable:an-havva constable').
card_rarity('an-havva constable'/'5ED', 'Rare').
card_artist('an-havva constable'/'5ED', 'Dan Frazier').
card_flavor_text('an-havva constable'/'5ED', '\"Joskun and the other constables serve with passion, if not with grace.\"\n—Devin, faerie noble').
card_multiverse_id('an-havva constable'/'5ED', '3960').

card_in_set('angry mob', '5ED').
card_original_type('angry mob'/'5ED', 'Summon — Mob').
card_original_text('angry mob'/'5ED', 'Trample\nDuring your turn, Angry Mob has power and toughness each equal to 2 plus the number of swamps all opponents control. During other turns, Angry Mob has power and toughness each equal to 2.').
card_image_name('angry mob'/'5ED', 'angry mob').
card_uid('angry mob'/'5ED', '5ED:Angry Mob:angry mob').
card_rarity('angry mob'/'5ED', 'Uncommon').
card_artist('angry mob'/'5ED', 'Drew Tucker').
card_multiverse_id('angry mob'/'5ED', '4101').

card_in_set('animate dead', '5ED').
card_original_type('animate dead'/'5ED', 'Enchantment').
card_original_text('animate dead'/'5ED', 'When you play Animate Dead, choose target creature card in any graveyard. When Animate Dead comes into play, put that creature into play and Animate Dead becomes a creature enchantment that targets the creature. Enchanted creature gets -1/-0. If Animate Dead leaves play, bury the creature.').
card_image_name('animate dead'/'5ED', 'animate dead').
card_uid('animate dead'/'5ED', '5ED:Animate Dead:animate dead').
card_rarity('animate dead'/'5ED', 'Uncommon').
card_artist('animate dead'/'5ED', 'Anson Maddocks').
card_multiverse_id('animate dead'/'5ED', '3823').

card_in_set('animate wall', '5ED').
card_original_type('animate wall'/'5ED', 'Enchant Creature').
card_original_text('animate wall'/'5ED', 'Play only on a Wall.\nEnchanted creature can attack as though it were not a Wall.').
card_image_name('animate wall'/'5ED', 'animate wall').
card_uid('animate wall'/'5ED', '5ED:Animate Wall:animate wall').
card_rarity('animate wall'/'5ED', 'Rare').
card_artist('animate wall'/'5ED', 'Richard Kane Ferguson').
card_flavor_text('animate wall'/'5ED', '\"When you have been bitten with fangs of granite, you start to long for the ivory sabers of tigers.\"\n—Norin the Wary').
card_multiverse_id('animate wall'/'5ED', '4102').

card_in_set('ankh of mishra', '5ED').
card_original_type('ankh of mishra'/'5ED', 'Artifact').
card_original_text('ankh of mishra'/'5ED', 'Whenever a land comes into play, Ankh of Mishra deals 2 damage to that land\'s controller.').
card_image_name('ankh of mishra'/'5ED', 'ankh of mishra').
card_uid('ankh of mishra'/'5ED', '5ED:Ankh of Mishra:ankh of mishra').
card_rarity('ankh of mishra'/'5ED', 'Rare').
card_artist('ankh of mishra'/'5ED', 'Ian Miller').
card_flavor_text('ankh of mishra'/'5ED', 'Tawnos finally cracked the puzzle: the strange structures did not house Mishra\'s malevolent creations but were themselves among his creations.').
card_multiverse_id('ankh of mishra'/'5ED', '3760').

card_in_set('anti-magic aura', '5ED').
card_original_type('anti-magic aura'/'5ED', 'Enchant Creature').
card_original_text('anti-magic aura'/'5ED', 'Enchanted creature cannot be the target of enchantments, instants, or sorceries. This effect does not bury Anti-Magic Aura. (Other enchantments on that creature are buried because their target is now illegal.)').
card_image_name('anti-magic aura'/'5ED', 'anti-magic aura').
card_uid('anti-magic aura'/'5ED', '5ED:Anti-Magic Aura:anti-magic aura').
card_rarity('anti-magic aura'/'5ED', 'Uncommon').
card_artist('anti-magic aura'/'5ED', 'Zak Plucinski').
card_multiverse_id('anti-magic aura'/'5ED', '3893').

card_in_set('arenson\'s aura', '5ED').
card_original_type('arenson\'s aura'/'5ED', 'Enchantment').
card_original_text('arenson\'s aura'/'5ED', '{W}, Sacrifice an enchantment: Destroy target enchantment.\n{3}{U}{U} Counter target enchantment spell. Play this ability as an interrupt.').
card_image_name('arenson\'s aura'/'5ED', 'arenson\'s aura').
card_uid('arenson\'s aura'/'5ED', '5ED:Arenson\'s Aura:arenson\'s aura').
card_rarity('arenson\'s aura'/'5ED', 'Uncommon').
card_artist('arenson\'s aura'/'5ED', 'D. Alexander Gregory').
card_flavor_text('arenson\'s aura'/'5ED', '\"Cloak me in mystery, hide my names from my enemies, and let me draw from the well of power.\"\n—Halvor Arenson, Kjeldoran priest').
card_multiverse_id('arenson\'s aura'/'5ED', '4103').

card_in_set('armageddon', '5ED').
card_original_type('armageddon'/'5ED', 'Sorcery').
card_original_text('armageddon'/'5ED', 'Destroy all lands.').
card_image_name('armageddon'/'5ED', 'armageddon').
card_uid('armageddon'/'5ED', '5ED:Armageddon:armageddon').
card_rarity('armageddon'/'5ED', 'Rare').
card_artist('armageddon'/'5ED', 'Jesper Myrfors').
card_multiverse_id('armageddon'/'5ED', '4104').

card_in_set('armor of faith', '5ED').
card_original_type('armor of faith'/'5ED', 'Enchant Creature').
card_original_text('armor of faith'/'5ED', 'Enchanted creature gets +1/+1.\n{W} Enchanted creature gets +0/+1 until end of turn.').
card_image_name('armor of faith'/'5ED', 'armor of faith').
card_uid('armor of faith'/'5ED', '5ED:Armor of Faith:armor of faith').
card_rarity('armor of faith'/'5ED', 'Common').
card_artist('armor of faith'/'5ED', 'Anson Maddocks').
card_flavor_text('armor of faith'/'5ED', '\"Keep your chain mail, warrior. I have my own form of protection.\"\n—Halvor Arenson, Kjeldoran priest').
card_multiverse_id('armor of faith'/'5ED', '4105').

card_in_set('ashes to ashes', '5ED').
card_original_type('ashes to ashes'/'5ED', 'Sorcery').
card_original_text('ashes to ashes'/'5ED', 'Remove two target nonartifact creatures from the game. Ashes to Ashes deals 5 damage to you.').
card_image_name('ashes to ashes'/'5ED', 'ashes to ashes').
card_uid('ashes to ashes'/'5ED', '5ED:Ashes to Ashes:ashes to ashes').
card_rarity('ashes to ashes'/'5ED', 'Uncommon').
card_artist('ashes to ashes'/'5ED', 'Doug Keith').
card_flavor_text('ashes to ashes'/'5ED', '\"Kings and beggars, they end the same—\nSurrounded by stench, rot, and blame.\"\n—Aline Corralurn, \"Legacy\"').
card_multiverse_id('ashes to ashes'/'5ED', '3824').

card_in_set('ashnod\'s altar', '5ED').
card_original_type('ashnod\'s altar'/'5ED', 'Artifact').
card_original_text('ashnod\'s altar'/'5ED', 'Sacrifice a creature: Add two colorless mana to your mana pool. Play this ability as a mana source.').
card_image_name('ashnod\'s altar'/'5ED', 'ashnod\'s altar').
card_uid('ashnod\'s altar'/'5ED', '5ED:Ashnod\'s Altar:ashnod\'s altar').
card_rarity('ashnod\'s altar'/'5ED', 'Uncommon').
card_artist('ashnod\'s altar'/'5ED', 'Anson Maddocks').
card_flavor_text('ashnod\'s altar'/'5ED', 'The Brothers\' War scarred the land, but Ashnod left her mark in the bloodlines of those she tortured.').
card_multiverse_id('ashnod\'s altar'/'5ED', '3761').

card_in_set('ashnod\'s transmogrant', '5ED').
card_original_type('ashnod\'s transmogrant'/'5ED', 'Artifact').
card_original_text('ashnod\'s transmogrant'/'5ED', '{T}, Sacrifice Ashnod\'s Transmogrant: Put a +1/+1 counter on target nonartifact creature. That creature becomes an artifact creature permanently.').
card_image_name('ashnod\'s transmogrant'/'5ED', 'ashnod\'s transmogrant').
card_uid('ashnod\'s transmogrant'/'5ED', '5ED:Ashnod\'s Transmogrant:ashnod\'s transmogrant').
card_rarity('ashnod\'s transmogrant'/'5ED', 'Common').
card_artist('ashnod\'s transmogrant'/'5ED', 'Mark Tedin').
card_flavor_text('ashnod\'s transmogrant'/'5ED', 'Ashnod found few willing to trade their humanity for the power she offered.').
card_multiverse_id('ashnod\'s transmogrant'/'5ED', '3762').

card_in_set('aspect of wolf', '5ED').
card_original_type('aspect of wolf'/'5ED', 'Enchant Creature').
card_original_text('aspect of wolf'/'5ED', 'Enchanted creature gets +*/+*, where * is equal to half the number of forests you control, rounded down for power and up for toughness.').
card_image_name('aspect of wolf'/'5ED', 'aspect of wolf').
card_uid('aspect of wolf'/'5ED', '5ED:Aspect of Wolf:aspect of wolf').
card_rarity('aspect of wolf'/'5ED', 'Rare').
card_artist('aspect of wolf'/'5ED', 'Janine Johnston').
card_multiverse_id('aspect of wolf'/'5ED', '3961').

card_in_set('atog', '5ED').
card_original_type('atog'/'5ED', 'Summon — Atog').
card_original_text('atog'/'5ED', 'Sacrifice an artifact: +2/+2 until end of turn').
card_image_name('atog'/'5ED', 'atog').
card_uid('atog'/'5ED', '5ED:Atog:atog').
card_rarity('atog'/'5ED', 'Uncommon').
card_artist('atog'/'5ED', 'Jesper Myrfors').
card_flavor_text('atog'/'5ED', 'The bane of all artificers, the legendary atogs devoured intricate tools to further their own twisted growth.').
card_multiverse_id('atog'/'5ED', '4030').

card_in_set('aurochs', '5ED').
card_original_type('aurochs'/'5ED', 'Summon — Aurochs').
card_original_text('aurochs'/'5ED', 'Trample\nIf Aurochs attacks, it gets +1/+0 until end of turn for each other Aurochs that attacks.').
card_image_name('aurochs'/'5ED', 'aurochs').
card_uid('aurochs'/'5ED', '5ED:Aurochs:aurochs').
card_rarity('aurochs'/'5ED', 'Common').
card_artist('aurochs'/'5ED', 'Steve White').
card_flavor_text('aurochs'/'5ED', 'One auroch may feed a village, but a herd will flatten it.').
card_multiverse_id('aurochs'/'5ED', '3962').

card_in_set('aysen bureaucrats', '5ED').
card_original_type('aysen bureaucrats'/'5ED', 'Summon — Bureaucrats').
card_original_text('aysen bureaucrats'/'5ED', '{T}: Tap target creature with power 2 or less.').
card_image_name('aysen bureaucrats'/'5ED', 'aysen bureaucrats').
card_uid('aysen bureaucrats'/'5ED', '5ED:Aysen Bureaucrats:aysen bureaucrats').
card_rarity('aysen bureaucrats'/'5ED', 'Common').
card_artist('aysen bureaucrats'/'5ED', 'Adrian Smith').
card_flavor_text('aysen bureaucrats'/'5ED', '\"When one\'s word is law, one\'s whim is equally binding.\"\n—Calmont, Aysen bureaucrat').
card_multiverse_id('aysen bureaucrats'/'5ED', '4106').

card_in_set('azure drake', '5ED').
card_original_type('azure drake'/'5ED', 'Summon — Drake').
card_original_text('azure drake'/'5ED', 'Flying').
card_image_name('azure drake'/'5ED', 'azure drake').
card_uid('azure drake'/'5ED', '5ED:Azure Drake:azure drake').
card_rarity('azure drake'/'5ED', 'Uncommon').
card_artist('azure drake'/'5ED', 'Janine Johnston').
card_flavor_text('azure drake'/'5ED', 'Little dreamt could seem so cruel\nAs waiting for the wings outspread,\nThe jagged teeth, the burning eyes,\nAnd dagger-claws that clench to nerves.').
card_multiverse_id('azure drake'/'5ED', '3894').

card_in_set('bad moon', '5ED').
card_original_type('bad moon'/'5ED', 'Enchantment').
card_original_text('bad moon'/'5ED', 'All black creatures get +1/+1.').
card_image_name('bad moon'/'5ED', 'bad moon').
card_uid('bad moon'/'5ED', '5ED:Bad Moon:bad moon').
card_rarity('bad moon'/'5ED', 'Rare').
card_artist('bad moon'/'5ED', 'Gary Leach').
card_multiverse_id('bad moon'/'5ED', '3825').

card_in_set('ball lightning', '5ED').
card_original_type('ball lightning'/'5ED', 'Summon — Ball Lightning').
card_original_text('ball lightning'/'5ED', 'Trample\nBall Lightning is unaffected by summoning sickness.\nAt the end of any turn, bury Ball Lightning.').
card_image_name('ball lightning'/'5ED', 'ball lightning').
card_uid('ball lightning'/'5ED', '5ED:Ball Lightning:ball lightning').
card_rarity('ball lightning'/'5ED', 'Rare').
card_artist('ball lightning'/'5ED', 'Quinton Hoover').
card_multiverse_id('ball lightning'/'5ED', '4031').

card_in_set('barbed sextant', '5ED').
card_original_type('barbed sextant'/'5ED', 'Artifact').
card_original_text('barbed sextant'/'5ED', '{1}, {T}, Sacrifice Barbed Sextant: Add one mana of any color to your mana pool. Play this ability as a mana source. Draw a card at the beginning of the next turn.').
card_image_name('barbed sextant'/'5ED', 'barbed sextant').
card_uid('barbed sextant'/'5ED', '5ED:Barbed Sextant:barbed sextant').
card_rarity('barbed sextant'/'5ED', 'Common').
card_artist('barbed sextant'/'5ED', 'Amy Weber').
card_multiverse_id('barbed sextant'/'5ED', '3763').

card_in_set('barl\'s cage', '5ED').
card_original_type('barl\'s cage'/'5ED', 'Artifact').
card_original_text('barl\'s cage'/'5ED', '{3}: Target creature does not untap during its controller\'s next untap phase.').
card_image_name('barl\'s cage'/'5ED', 'barl\'s cage').
card_uid('barl\'s cage'/'5ED', '5ED:Barl\'s Cage:barl\'s cage').
card_rarity('barl\'s cage'/'5ED', 'Rare').
card_artist('barl\'s cage'/'5ED', 'Tom Wänerstrand').
card_flavor_text('barl\'s cage'/'5ED', 'For a dozen years the cage had held Lord Ith, but as the Pretender Mairsil\'s power weakened, so did the bars.').
card_multiverse_id('barl\'s cage'/'5ED', '3764').

card_in_set('battering ram', '5ED').
card_original_type('battering ram'/'5ED', 'Artifact Creature').
card_original_text('battering ram'/'5ED', 'Banding when attacking\nIf Battering Ram is blocked by any Wall, destroy that Wall at end of combat.').
card_image_name('battering ram'/'5ED', 'battering ram').
card_uid('battering ram'/'5ED', '5ED:Battering Ram:battering ram').
card_rarity('battering ram'/'5ED', 'Common').
card_artist('battering ram'/'5ED', 'Jeff A. Menges').
card_flavor_text('battering ram'/'5ED', 'By the time Mishra was defeated, no mage was foolish enough to rely heavily on walls.').
card_multiverse_id('battering ram'/'5ED', '3765').

card_in_set('benalish hero', '5ED').
card_original_type('benalish hero'/'5ED', 'Summon — Hero').
card_original_text('benalish hero'/'5ED', 'Banding').
card_image_name('benalish hero'/'5ED', 'benalish hero').
card_uid('benalish hero'/'5ED', '5ED:Benalish Hero:benalish hero').
card_rarity('benalish hero'/'5ED', 'Common').
card_artist('benalish hero'/'5ED', 'Douglas Shuler').
card_flavor_text('benalish hero'/'5ED', 'Benalia has a complex caste system that changes with the lunar year. No matter what the season, the only caste that cannot be attained by either heredity or money is that of the Hero.').
card_multiverse_id('benalish hero'/'5ED', '4107').

card_in_set('binding grasp', '5ED').
card_original_type('binding grasp'/'5ED', 'Enchant Creature').
card_original_text('binding grasp'/'5ED', 'During your upkeep, pay {1}{U} or bury Binding Grasp.\nGain control of enchanted creature. That creature gets +0/+1.').
card_image_name('binding grasp'/'5ED', 'binding grasp').
card_uid('binding grasp'/'5ED', '5ED:Binding Grasp:binding grasp').
card_rarity('binding grasp'/'5ED', 'Uncommon').
card_artist('binding grasp'/'5ED', 'Jeff Miracola').
card_flavor_text('binding grasp'/'5ED', '\"What I want, I take.\"\n—Gustha Ebbasdotter,\nKjeldoran royal mage').
card_multiverse_id('binding grasp'/'5ED', '3895').

card_in_set('bird maiden', '5ED').
card_original_type('bird maiden'/'5ED', 'Summon — Bird Maiden').
card_original_text('bird maiden'/'5ED', 'Flying').
card_image_name('bird maiden'/'5ED', 'bird maiden').
card_uid('bird maiden'/'5ED', '5ED:Bird Maiden:bird maiden').
card_rarity('bird maiden'/'5ED', 'Common').
card_artist('bird maiden'/'5ED', 'Kaja Foglio').
card_flavor_text('bird maiden'/'5ED', '\"Four things that never meet do here unite To shed my blood and to ravage my heart,\nA radiant brow and tresses that beguile\nAnd rosy cheeks and a glittering smile.\"\n—The Arabian Nights, trans. Haddawy').
card_multiverse_id('bird maiden'/'5ED', '4032').

card_in_set('birds of paradise', '5ED').
card_original_type('birds of paradise'/'5ED', 'Summon — Mana Birds').
card_original_text('birds of paradise'/'5ED', 'Flying\n{T}: Add one mana of any color to your mana pool. Play this ability as a mana source.').
card_image_name('birds of paradise'/'5ED', 'birds of paradise').
card_uid('birds of paradise'/'5ED', '5ED:Birds of Paradise:birds of paradise').
card_rarity('birds of paradise'/'5ED', 'Rare').
card_artist('birds of paradise'/'5ED', 'Mark Poole').
card_multiverse_id('birds of paradise'/'5ED', '3963').

card_in_set('black knight', '5ED').
card_original_type('black knight'/'5ED', 'Summon — Knight').
card_original_text('black knight'/'5ED', 'First strike, protection from white').
card_image_name('black knight'/'5ED', 'black knight').
card_uid('black knight'/'5ED', '5ED:Black Knight:black knight').
card_rarity('black knight'/'5ED', 'Uncommon').
card_artist('black knight'/'5ED', 'Adrian Smith').
card_flavor_text('black knight'/'5ED', 'Battle doesn\'t need a purpose; the battle is its own purpose. You don\'t ask why a plague spreads or a field burns. Don\'t ask why I fight.').
card_multiverse_id('black knight'/'5ED', '3826').

card_in_set('blessed wine', '5ED').
card_original_type('blessed wine'/'5ED', 'Instant').
card_original_text('blessed wine'/'5ED', 'Gain 1 life.\nDraw a card at the beginning of the next turn.').
card_image_name('blessed wine'/'5ED', 'blessed wine').
card_uid('blessed wine'/'5ED', '5ED:Blessed Wine:blessed wine').
card_rarity('blessed wine'/'5ED', 'Common').
card_artist('blessed wine'/'5ED', 'Kaja Foglio').
card_flavor_text('blessed wine'/'5ED', '\"Out there, there are spirits, but here, there is wine.\"\n—Blessing for the bounty of Kjeld').
card_multiverse_id('blessed wine'/'5ED', '4108').

card_in_set('blight', '5ED').
card_original_type('blight'/'5ED', 'Enchant Land').
card_original_text('blight'/'5ED', 'If enchanted land becomes tapped, destroy it at end of turn.').
card_image_name('blight'/'5ED', 'blight').
card_uid('blight'/'5ED', '5ED:Blight:blight').
card_rarity('blight'/'5ED', 'Uncommon').
card_artist('blight'/'5ED', 'Ian Miller').
card_multiverse_id('blight'/'5ED', '3827').

card_in_set('blinking spirit', '5ED').
card_original_type('blinking spirit'/'5ED', 'Summon — Blinking Spirit').
card_original_text('blinking spirit'/'5ED', '{0}: Return Blinking Spirit to owner\'s hand.').
card_image_name('blinking spirit'/'5ED', 'blinking spirit').
card_uid('blinking spirit'/'5ED', '5ED:Blinking Spirit:blinking spirit').
card_rarity('blinking spirit'/'5ED', 'Rare').
card_artist('blinking spirit'/'5ED', 'L. A. Williams').
card_flavor_text('blinking spirit'/'5ED', '\"Don\'t look at it! Maybe it\'ll go away!\"\n—Ib Halfheart, goblin tactician').
card_multiverse_id('blinking spirit'/'5ED', '4109').

card_in_set('blood lust', '5ED').
card_original_type('blood lust'/'5ED', 'Instant').
card_original_text('blood lust'/'5ED', 'Target creature gets +4/-4 until end of turn. If this reduces that creature\'s toughness to less than 1, the creature\'s toughness is 1.').
card_image_name('blood lust'/'5ED', 'blood lust').
card_uid('blood lust'/'5ED', '5ED:Blood Lust:blood lust').
card_rarity('blood lust'/'5ED', 'Common').
card_artist('blood lust'/'5ED', 'Anson Maddocks').
card_multiverse_id('blood lust'/'5ED', '4033').

card_in_set('bog imp', '5ED').
card_original_type('bog imp'/'5ED', 'Summon — Imp').
card_original_text('bog imp'/'5ED', 'Flying').
card_image_name('bog imp'/'5ED', 'bog imp').
card_uid('bog imp'/'5ED', '5ED:Bog Imp:bog imp').
card_rarity('bog imp'/'5ED', 'Common').
card_artist('bog imp'/'5ED', 'Ron Spencer').
card_flavor_text('bog imp'/'5ED', 'On guard for larger dangers, we underestimated the power and speed of the imp\'s muck-crusted claws.').
card_multiverse_id('bog imp'/'5ED', '3828').

card_in_set('bog rats', '5ED').
card_original_type('bog rats'/'5ED', 'Summon — Rats').
card_original_text('bog rats'/'5ED', 'Bog Rats cannot be blocked by Walls.').
card_image_name('bog rats'/'5ED', 'bog rats').
card_uid('bog rats'/'5ED', '5ED:Bog Rats:bog rats').
card_rarity('bog rats'/'5ED', 'Common').
card_artist('bog rats'/'5ED', 'Ron Spencer').
card_flavor_text('bog rats'/'5ED', 'Their stench was vile and strong enough, but not nearly as powerful as their hunger.').
card_multiverse_id('bog rats'/'5ED', '3829').

card_in_set('bog wraith', '5ED').
card_original_type('bog wraith'/'5ED', 'Summon — Wraith').
card_original_text('bog wraith'/'5ED', 'Swampwalk (If defending player controls any swamps, this creature is unblockable.)').
card_image_name('bog wraith'/'5ED', 'bog wraith').
card_uid('bog wraith'/'5ED', '5ED:Bog Wraith:bog wraith').
card_rarity('bog wraith'/'5ED', 'Uncommon').
card_artist('bog wraith'/'5ED', 'Jeff A. Menges').
card_flavor_text('bog wraith'/'5ED', '\'Twas in the bogs of Cannelbrae\nMy mate did meet an early grave;\n\'Twas nothing left for us to save\nIn the peat-filled bogs of Cannelbrae.').
card_multiverse_id('bog wraith'/'5ED', '3830').

card_in_set('boomerang', '5ED').
card_original_type('boomerang'/'5ED', 'Instant').
card_original_text('boomerang'/'5ED', 'Return target permanent to owner\'s hand.').
card_image_name('boomerang'/'5ED', 'boomerang').
card_uid('boomerang'/'5ED', '5ED:Boomerang:boomerang').
card_rarity('boomerang'/'5ED', 'Common').
card_artist('boomerang'/'5ED', 'Alan Rabinowitz').
card_flavor_text('boomerang'/'5ED', '\"O! call back yesterday, bid time return.\" —William Shakespeare, King Richard the Second').
card_multiverse_id('boomerang'/'5ED', '3896').

card_in_set('bottle of suleiman', '5ED').
card_original_type('bottle of suleiman'/'5ED', 'Artifact').
card_original_text('bottle of suleiman'/'5ED', '{1}, Sacrifice Bottle of Suleiman: Flip a coin; target opponent calls heads or tails while coin is in the air. If the flip ends up in opponent\'s favor, Bottle of Suleiman deals 5 damage to you. Otherwise, put a Djinn token into play. Treat this token as a 5/5 artifact creature with flying.').
card_image_name('bottle of suleiman'/'5ED', 'bottle of suleiman').
card_uid('bottle of suleiman'/'5ED', '5ED:Bottle of Suleiman:bottle of suleiman').
card_rarity('bottle of suleiman'/'5ED', 'Rare').
card_artist('bottle of suleiman'/'5ED', 'DiTerlizzi').
card_multiverse_id('bottle of suleiman'/'5ED', '3766').

card_in_set('bottomless vault', '5ED').
card_original_type('bottomless vault'/'5ED', 'Land').
card_original_text('bottomless vault'/'5ED', 'Bottomless Vault comes into play tapped.\nYou may choose not to untap Bottomless Vault during your untap phase and put a storage counter on it instead.\n{T}, Remove X storage counters from Bottomless Vault: Add an amount of {B} equal to X to your mana pool.').
card_image_name('bottomless vault'/'5ED', 'bottomless vault').
card_uid('bottomless vault'/'5ED', '5ED:Bottomless Vault:bottomless vault').
card_rarity('bottomless vault'/'5ED', 'Rare').
card_artist('bottomless vault'/'5ED', 'David Seeley').
card_multiverse_id('bottomless vault'/'5ED', '4176').

card_in_set('brainstorm', '5ED').
card_original_type('brainstorm'/'5ED', 'Instant').
card_original_text('brainstorm'/'5ED', 'Draw three cards. Then, put any two cards from your hand on top of your library in any order.').
card_image_name('brainstorm'/'5ED', 'brainstorm').
card_uid('brainstorm'/'5ED', '5ED:Brainstorm:brainstorm').
card_rarity('brainstorm'/'5ED', 'Common').
card_artist('brainstorm'/'5ED', 'Christopher Rush').
card_flavor_text('brainstorm'/'5ED', '\"I reeled from the blow, and then suddenly, I knew exactly what to do. Within moments, victory was mine.\"\n—Gustha Ebbasdotter\nKjeldoran royal mage').
card_multiverse_id('brainstorm'/'5ED', '3897').

card_in_set('brainwash', '5ED').
card_original_type('brainwash'/'5ED', 'Enchant Creature').
card_original_text('brainwash'/'5ED', 'Enchanted creature cannot attack this turn unless its controller pays an additional {3}.').
card_image_name('brainwash'/'5ED', 'brainwash').
card_uid('brainwash'/'5ED', '5ED:Brainwash:brainwash').
card_rarity('brainwash'/'5ED', 'Common').
card_artist('brainwash'/'5ED', 'Terese Nielsen').
card_flavor_text('brainwash'/'5ED', '\"They\'re not your friends; they despise you. I\'m the only one you can count on. Trust me.\"').
card_multiverse_id('brainwash'/'5ED', '4110').

card_in_set('brassclaw orcs', '5ED').
card_original_type('brassclaw orcs'/'5ED', 'Summon — Orcs').
card_original_text('brassclaw orcs'/'5ED', 'Brassclaw Orcs cannot be assigned to block any creature with power 2 or greater.').
card_image_name('brassclaw orcs'/'5ED', 'brassclaw orcs').
card_uid('brassclaw orcs'/'5ED', '5ED:Brassclaw Orcs:brassclaw orcs').
card_rarity('brassclaw orcs'/'5ED', 'Common').
card_artist('brassclaw orcs'/'5ED', 'Rob Alexander').
card_flavor_text('brassclaw orcs'/'5ED', '\"Modern Sarpadia\'s swarming with thrulls. It\'s a testament to the orcs that they\'ve survived there so long.\"\n—Elbieta, Argivian scholar').
card_multiverse_id('brassclaw orcs'/'5ED', '4034').

card_in_set('breeding pit', '5ED').
card_original_type('breeding pit'/'5ED', 'Enchantment').
card_original_text('breeding pit'/'5ED', 'During your upkeep, pay {B}{B} or bury Breeding Pit.\nAt the end of your turn, put a Thrull token into play. Treat this token as a 0/1 black creature.').
card_image_name('breeding pit'/'5ED', 'breeding pit').
card_uid('breeding pit'/'5ED', '5ED:Breeding Pit:breeding pit').
card_rarity('breeding pit'/'5ED', 'Uncommon').
card_artist('breeding pit'/'5ED', 'Adrian Smith').
card_flavor_text('breeding pit'/'5ED', 'The thrulls bred at a terrifying pace. In the end, they overwhelmed the Order of the Ebon Hand.').
card_multiverse_id('breeding pit'/'5ED', '3831').

card_in_set('broken visage', '5ED').
card_original_type('broken visage'/'5ED', 'Instant').
card_original_text('broken visage'/'5ED', 'Bury target nonartifact attacking creature and put a Shadow token into play. Treat this token as a black creature with the same power and toughness as that attacking creature. At end of turn, bury the token.').
card_image_name('broken visage'/'5ED', 'broken visage').
card_uid('broken visage'/'5ED', '5ED:Broken Visage:broken visage').
card_rarity('broken visage'/'5ED', 'Rare').
card_artist('broken visage'/'5ED', 'Margaret Organ-Kean').
card_multiverse_id('broken visage'/'5ED', '3832').

card_in_set('brothers of fire', '5ED').
card_original_type('brothers of fire'/'5ED', 'Summon — Brothers').
card_original_text('brothers of fire'/'5ED', '{1}{R}{R} Brothers of Fire deals 1 damage to target creature or player and 1 damage to you.').
card_image_name('brothers of fire'/'5ED', 'brothers of fire').
card_uid('brothers of fire'/'5ED', '5ED:Brothers of Fire:brothers of fire').
card_rarity('brothers of fire'/'5ED', 'Common').
card_artist('brothers of fire'/'5ED', 'Mark Tedin').
card_flavor_text('brothers of fire'/'5ED', 'Fire is never a gentle master.').
card_multiverse_id('brothers of fire'/'5ED', '4035').

card_in_set('brushland', '5ED').
card_original_type('brushland'/'5ED', 'Land').
card_original_text('brushland'/'5ED', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {G} or {W} to your mana pool. Brushland deals 1 damage to you.').
card_image_name('brushland'/'5ED', 'brushland').
card_uid('brushland'/'5ED', '5ED:Brushland:brushland').
card_rarity('brushland'/'5ED', 'Rare').
card_artist('brushland'/'5ED', 'Tom Wänerstrand').
card_multiverse_id('brushland'/'5ED', '4177').

card_in_set('carapace', '5ED').
card_original_type('carapace'/'5ED', 'Enchant Creature').
card_original_text('carapace'/'5ED', 'Enchanted creature gets +0/+2.\nSacrifice Carapace: Regenerate enchanted creature.').
card_image_name('carapace'/'5ED', 'carapace').
card_uid('carapace'/'5ED', '5ED:Carapace:carapace').
card_rarity('carapace'/'5ED', 'Common').
card_artist('carapace'/'5ED', 'Anson Maddocks').
card_flavor_text('carapace'/'5ED', '\"Now that\'s a fashion statement.\"\n—Devin, faerie noble').
card_multiverse_id('carapace'/'5ED', '3964').

card_in_set('caribou range', '5ED').
card_original_type('caribou range'/'5ED', 'Enchant Land').
card_original_text('caribou range'/'5ED', 'Play only on a land you control.\n{W}{W}, Tap enchanted land: Put a Caribou token into play. Treat this token as a 0/1 white creature.\nSacrifice a Caribou token: Gain 1 life.').
card_image_name('caribou range'/'5ED', 'caribou range').
card_uid('caribou range'/'5ED', '5ED:Caribou Range:caribou range').
card_rarity('caribou range'/'5ED', 'Rare').
card_artist('caribou range'/'5ED', 'Una Fricker').
card_multiverse_id('caribou range'/'5ED', '4111').

card_in_set('carrion ants', '5ED').
card_original_type('carrion ants'/'5ED', 'Summon — Ants').
card_original_text('carrion ants'/'5ED', '{1}: +1/+1 until end of turn').
card_image_name('carrion ants'/'5ED', 'carrion ants').
card_uid('carrion ants'/'5ED', '5ED:Carrion Ants:carrion ants').
card_rarity('carrion ants'/'5ED', 'Uncommon').
card_artist('carrion ants'/'5ED', 'John Coulthart').
card_flavor_text('carrion ants'/'5ED', '\"War is no picnic,\" my father liked to say. But the ants seemed to disagree.').
card_multiverse_id('carrion ants'/'5ED', '3833').

card_in_set('castle', '5ED').
card_original_type('castle'/'5ED', 'Enchantment').
card_original_text('castle'/'5ED', 'Each untapped creature you control gets +0/+2 unless it is attacking.').
card_image_name('castle'/'5ED', 'castle').
card_uid('castle'/'5ED', '5ED:Castle:castle').
card_rarity('castle'/'5ED', 'Uncommon').
card_artist('castle'/'5ED', 'David O\'Connor').
card_flavor_text('castle'/'5ED', '\"Hang out our banners on the outward walls; / The cry is still, ‘They come\'; our castle\'s strength / Will laugh a siege to scorn.\"\n—William Shakespeare, Macbeth').
card_multiverse_id('castle'/'5ED', '4112').

card_in_set('cat warriors', '5ED').
card_original_type('cat warriors'/'5ED', 'Summon — Cat Warriors').
card_original_text('cat warriors'/'5ED', 'Forestwalk (If defending player controls any forests, this creature is unblockable.)').
card_image_name('cat warriors'/'5ED', 'cat warriors').
card_uid('cat warriors'/'5ED', '5ED:Cat Warriors:cat warriors').
card_rarity('cat warriors'/'5ED', 'Common').
card_artist('cat warriors'/'5ED', 'Melissa A. Benson').
card_flavor_text('cat warriors'/'5ED', 'These stealthy felines have survived so many battles that some believe they must possess many lives.').
card_multiverse_id('cat warriors'/'5ED', '3965').

card_in_set('cave people', '5ED').
card_original_type('cave people'/'5ED', 'Summon — Cave People').
card_original_text('cave people'/'5ED', 'If Cave People attacks, it gets +1/-2 until end of turn.\n{1}{R}{R}, {T}: Target creature gains mountainwalk until end of turn. (If defending player controls any mountains, that creature is unblockable.)').
card_image_name('cave people'/'5ED', 'cave people').
card_uid('cave people'/'5ED', '5ED:Cave People:cave people').
card_rarity('cave people'/'5ED', 'Uncommon').
card_artist('cave people'/'5ED', 'Steve Luke').
card_multiverse_id('cave people'/'5ED', '4036').

card_in_set('chub toad', '5ED').
card_original_type('chub toad'/'5ED', 'Summon — Toad').
card_original_text('chub toad'/'5ED', 'If Chub Toad blocks or is blocked, it gets +2/+2 until end of turn.').
card_image_name('chub toad'/'5ED', 'chub toad').
card_uid('chub toad'/'5ED', '5ED:Chub Toad:chub toad').
card_rarity('chub toad'/'5ED', 'Common').
card_artist('chub toad'/'5ED', 'Daniel Gelon').
card_flavor_text('chub toad'/'5ED', 'Chub toad, chub toad\nAt the door.\nRun away quick\nOr you\'ll run no more.\n—Traditional children\'s rhyme').
card_multiverse_id('chub toad'/'5ED', '3966').

card_in_set('circle of protection: artifacts', '5ED').
card_original_type('circle of protection: artifacts'/'5ED', 'Enchantment').
card_original_text('circle of protection: artifacts'/'5ED', '{2}: Prevent all damage to you from an artifact source. Treat further damage from that source normally.').
card_image_name('circle of protection: artifacts'/'5ED', 'circle of protection artifacts').
card_uid('circle of protection: artifacts'/'5ED', '5ED:Circle of Protection: Artifacts:circle of protection artifacts').
card_rarity('circle of protection: artifacts'/'5ED', 'Uncommon').
card_artist('circle of protection: artifacts'/'5ED', 'Pete Venters').
card_multiverse_id('circle of protection: artifacts'/'5ED', '4113').

card_in_set('circle of protection: black', '5ED').
card_original_type('circle of protection: black'/'5ED', 'Enchantment').
card_original_text('circle of protection: black'/'5ED', '{1}: Prevent all damage to you from a black source. Treat further damage from that source normally.').
card_image_name('circle of protection: black'/'5ED', 'circle of protection black').
card_uid('circle of protection: black'/'5ED', '5ED:Circle of Protection: Black:circle of protection black').
card_rarity('circle of protection: black'/'5ED', 'Common').
card_artist('circle of protection: black'/'5ED', 'Gerry Grace').
card_multiverse_id('circle of protection: black'/'5ED', '4114').

card_in_set('circle of protection: blue', '5ED').
card_original_type('circle of protection: blue'/'5ED', 'Enchantment').
card_original_text('circle of protection: blue'/'5ED', '{1}: Prevent all damage to you from a blue source. Treat further damage from that source normally.').
card_image_name('circle of protection: blue'/'5ED', 'circle of protection blue').
card_uid('circle of protection: blue'/'5ED', '5ED:Circle of Protection: Blue:circle of protection blue').
card_rarity('circle of protection: blue'/'5ED', 'Common').
card_artist('circle of protection: blue'/'5ED', 'Gerry Grace').
card_multiverse_id('circle of protection: blue'/'5ED', '4115').

card_in_set('circle of protection: green', '5ED').
card_original_type('circle of protection: green'/'5ED', 'Enchantment').
card_original_text('circle of protection: green'/'5ED', '{1}: Prevent all damage to you from a green source. Treat further damage from that source normally.').
card_image_name('circle of protection: green'/'5ED', 'circle of protection green').
card_uid('circle of protection: green'/'5ED', '5ED:Circle of Protection: Green:circle of protection green').
card_rarity('circle of protection: green'/'5ED', 'Common').
card_artist('circle of protection: green'/'5ED', 'Gerry Grace').
card_multiverse_id('circle of protection: green'/'5ED', '4116').

card_in_set('circle of protection: red', '5ED').
card_original_type('circle of protection: red'/'5ED', 'Enchantment').
card_original_text('circle of protection: red'/'5ED', '{1}: Prevent all damage to you from a red source. Treat further damage from that source normally.').
card_image_name('circle of protection: red'/'5ED', 'circle of protection red').
card_uid('circle of protection: red'/'5ED', '5ED:Circle of Protection: Red:circle of protection red').
card_rarity('circle of protection: red'/'5ED', 'Common').
card_artist('circle of protection: red'/'5ED', 'Gerry Grace').
card_multiverse_id('circle of protection: red'/'5ED', '4117').

card_in_set('circle of protection: white', '5ED').
card_original_type('circle of protection: white'/'5ED', 'Enchantment').
card_original_text('circle of protection: white'/'5ED', '{1}: Prevent all damage to you from a white source. Treat further damage from that source normally.').
card_image_name('circle of protection: white'/'5ED', 'circle of protection white').
card_uid('circle of protection: white'/'5ED', '5ED:Circle of Protection: White:circle of protection white').
card_rarity('circle of protection: white'/'5ED', 'Common').
card_artist('circle of protection: white'/'5ED', 'Gerry Grace').
card_multiverse_id('circle of protection: white'/'5ED', '4118').

card_in_set('city of brass', '5ED').
card_original_type('city of brass'/'5ED', 'Land').
card_original_text('city of brass'/'5ED', 'Whenever City of Brass becomes tapped, it deals 1 damage to you.\n{T}: Add one mana of any color to your mana pool.').
card_image_name('city of brass'/'5ED', 'city of brass').
card_uid('city of brass'/'5ED', '5ED:City of Brass:city of brass').
card_rarity('city of brass'/'5ED', 'Rare').
card_artist('city of brass'/'5ED', 'Tom Wänerstrand').
card_multiverse_id('city of brass'/'5ED', '4178').

card_in_set('clay statue', '5ED').
card_original_type('clay statue'/'5ED', 'Artifact Creature').
card_original_text('clay statue'/'5ED', '{2}: Regenerate').
card_image_name('clay statue'/'5ED', 'clay statue').
card_uid('clay statue'/'5ED', '5ED:Clay Statue:clay statue').
card_rarity('clay statue'/'5ED', 'Common').
card_artist('clay statue'/'5ED', 'Adam Rex').
card_flavor_text('clay statue'/'5ED', 'Tawnos won fame as Urza\'s greatest assistant. After he created these warriors, Urza ended his apprenticeship, promoting him directly to the rank of master.').
card_multiverse_id('clay statue'/'5ED', '3767').

card_in_set('cloak of confusion', '5ED').
card_original_type('cloak of confusion'/'5ED', 'Enchant Creature').
card_original_text('cloak of confusion'/'5ED', '{0}: Defending player discards a card at random. Enchanted creature deals no combat damage this turn. Use this ability only if enchanted creature is attacking and unblocked and only once each turn.').
card_image_name('cloak of confusion'/'5ED', 'cloak of confusion').
card_uid('cloak of confusion'/'5ED', '5ED:Cloak of Confusion:cloak of confusion').
card_rarity('cloak of confusion'/'5ED', 'Common').
card_artist('cloak of confusion'/'5ED', 'Margaret Organ-Kean').
card_multiverse_id('cloak of confusion'/'5ED', '3834').

card_in_set('clockwork beast', '5ED').
card_original_type('clockwork beast'/'5ED', 'Artifact Creature').
card_original_text('clockwork beast'/'5ED', 'When Clockwork Beast comes into play, put seven +1/+0 counters on it. At the end of any combat in which Clockwork Beast attacked or blocked, remove one of these counters.\n{X}, {T}: Put X +1/+0 counters on Clockwork Beast. You may have no more than seven of of these counters on Clockwork Beast. Use this ability only during your upkeep.').
card_image_name('clockwork beast'/'5ED', 'clockwork beast').
card_uid('clockwork beast'/'5ED', '5ED:Clockwork Beast:clockwork beast').
card_rarity('clockwork beast'/'5ED', 'Rare').
card_artist('clockwork beast'/'5ED', 'Drew Tucker').
card_multiverse_id('clockwork beast'/'5ED', '3768').

card_in_set('clockwork steed', '5ED').
card_original_type('clockwork steed'/'5ED', 'Artifact Creature').
card_original_text('clockwork steed'/'5ED', 'Clockwork Steed cannot be blocked by artifact creatures.\nWhen Clockwork Steed comes into play, put four +1/+0 counters on it. At the end of any combat in which Clockwork Steed attacked or blocked, remove one of these counters.\n{X}, {T}: Put X +1/+0 counters on Clockwork Steed. You may have no more than four of these counters on Clockwork Steed. Use this ability only during your upkeep.').
card_image_name('clockwork steed'/'5ED', 'clockwork steed').
card_uid('clockwork steed'/'5ED', '5ED:Clockwork Steed:clockwork steed').
card_rarity('clockwork steed'/'5ED', 'Uncommon').
card_artist('clockwork steed'/'5ED', 'Terese Nielsen').
card_multiverse_id('clockwork steed'/'5ED', '3769').

card_in_set('cockatrice', '5ED').
card_original_type('cockatrice'/'5ED', 'Summon — Cockatrice').
card_original_text('cockatrice'/'5ED', 'Flying\nIf Cockatrice blocks or is blocked by any non-Wall creature, destroy that creature at end of combat.').
card_image_name('cockatrice'/'5ED', 'cockatrice').
card_uid('cockatrice'/'5ED', '5ED:Cockatrice:cockatrice').
card_rarity('cockatrice'/'5ED', 'Rare').
card_artist('cockatrice'/'5ED', 'Dan Frazier').
card_multiverse_id('cockatrice'/'5ED', '3967').

card_in_set('colossus of sardia', '5ED').
card_original_type('colossus of sardia'/'5ED', 'Artifact Creature').
card_original_text('colossus of sardia'/'5ED', 'Trample\nColossus of Sardia does not untap during your untap phase.\n{9}: Untap Colossus of Sardia. Use this ability only during your upkeep.').
card_image_name('colossus of sardia'/'5ED', 'colossus of sardia').
card_uid('colossus of sardia'/'5ED', '5ED:Colossus of Sardia:colossus of sardia').
card_rarity('colossus of sardia'/'5ED', 'Rare').
card_artist('colossus of sardia'/'5ED', 'Hannibal King').
card_flavor_text('colossus of sardia'/'5ED', 'From the Sardian mountains wakes ancient doom: / Warrior born from a rocky womb.').
card_multiverse_id('colossus of sardia'/'5ED', '3770').

card_in_set('conquer', '5ED').
card_original_type('conquer'/'5ED', 'Enchant Land').
card_original_text('conquer'/'5ED', 'Gain control of enchanted land.').
card_image_name('conquer'/'5ED', 'conquer').
card_uid('conquer'/'5ED', '5ED:Conquer:conquer').
card_rarity('conquer'/'5ED', 'Uncommon').
card_artist('conquer'/'5ED', 'Gary Leach').
card_flavor_text('conquer'/'5ED', '\"Every field watered in warriors\' blood is sacred ground. Lay it not to waste, but claim it for our own—for glory, and for Stromgald!\"\n—Avram Garrisson,\nLeader of the Knights of Stromgald').
card_multiverse_id('conquer'/'5ED', '4037').

card_in_set('coral helm', '5ED').
card_original_type('coral helm'/'5ED', 'Artifact').
card_original_text('coral helm'/'5ED', '{3}, Discard a card at random: Target creature gets +2/+2 until end of turn.').
card_image_name('coral helm'/'5ED', 'coral helm').
card_uid('coral helm'/'5ED', '5ED:Coral Helm:coral helm').
card_rarity('coral helm'/'5ED', 'Rare').
card_artist('coral helm'/'5ED', 'Steve Luke').
card_multiverse_id('coral helm'/'5ED', '3771').

card_in_set('counterspell', '5ED').
card_original_type('counterspell'/'5ED', 'Interrupt').
card_original_text('counterspell'/'5ED', 'Counter target spell.').
card_image_name('counterspell'/'5ED', 'counterspell').
card_uid('counterspell'/'5ED', '5ED:Counterspell:counterspell').
card_rarity('counterspell'/'5ED', 'Common').
card_artist('counterspell'/'5ED', 'Hannibal King').
card_multiverse_id('counterspell'/'5ED', '3898').

card_in_set('craw giant', '5ED').
card_original_type('craw giant'/'5ED', 'Summon — Giant').
card_original_text('craw giant'/'5ED', 'Trample; rampage 2 (For each creature assigned to block it beyond the first, this creature gets +2/+2 until end of turn.)').
card_image_name('craw giant'/'5ED', 'craw giant').
card_uid('craw giant'/'5ED', '5ED:Craw Giant:craw giant').
card_rarity('craw giant'/'5ED', 'Uncommon').
card_artist('craw giant'/'5ED', 'Scott Kirschner').
card_flavor_text('craw giant'/'5ED', 'Harthag gave a jolly laugh as he surveyed the army before him. \"Ho ho ho! Midgets! You think you can stand in my way?\"').
card_multiverse_id('craw giant'/'5ED', '3968').

card_in_set('craw wurm', '5ED').
card_original_type('craw wurm'/'5ED', 'Summon — Wurm').
card_original_text('craw wurm'/'5ED', '').
card_image_name('craw wurm'/'5ED', 'craw wurm').
card_uid('craw wurm'/'5ED', '5ED:Craw Wurm:craw wurm').
card_rarity('craw wurm'/'5ED', 'Common').
card_artist('craw wurm'/'5ED', 'Daniel Gelon').
card_flavor_text('craw wurm'/'5ED', 'The most terrifying thing about the craw wurm is probably the horrible crashing sound it makes as it speeds through the forest. This noise is so loud it echoes through the trees and seems to come from all directions at once.').
card_multiverse_id('craw wurm'/'5ED', '3969').

card_in_set('crimson manticore', '5ED').
card_original_type('crimson manticore'/'5ED', 'Summon — Manticore').
card_original_text('crimson manticore'/'5ED', 'Flying\n{R}, {T}: Crimson Manticore deals 1 damage to target attacking or blocking creature.').
card_image_name('crimson manticore'/'5ED', 'crimson manticore').
card_uid('crimson manticore'/'5ED', '5ED:Crimson Manticore:crimson manticore').
card_rarity('crimson manticore'/'5ED', 'Rare').
card_artist('crimson manticore'/'5ED', 'Roger Raupp').
card_flavor_text('crimson manticore'/'5ED', 'Manticores divide all beings into two categories: other manticores and dinner.').
card_multiverse_id('crimson manticore'/'5ED', '4038').

card_in_set('crown of the ages', '5ED').
card_original_type('crown of the ages'/'5ED', 'Artifact').
card_original_text('crown of the ages'/'5ED', '{4}, {T}: Move target enchantment from one creature to another. The enchantment\'s new target must be legal.').
card_image_name('crown of the ages'/'5ED', 'crown of the ages').
card_uid('crown of the ages'/'5ED', '5ED:Crown of the Ages:crown of the ages').
card_rarity('crown of the ages'/'5ED', 'Rare').
card_artist('crown of the ages'/'5ED', 'Roger Raupp').
card_multiverse_id('crown of the ages'/'5ED', '3772').

card_in_set('crumble', '5ED').
card_original_type('crumble'/'5ED', 'Instant').
card_original_text('crumble'/'5ED', 'Bury target artifact. That artifact\'s controller gains an amount of life equal to its total casting cost.').
card_image_name('crumble'/'5ED', 'crumble').
card_uid('crumble'/'5ED', '5ED:Crumble:crumble').
card_rarity('crumble'/'5ED', 'Uncommon').
card_artist('crumble'/'5ED', 'Jesper Myrfors').
card_flavor_text('crumble'/'5ED', '\"Ashes and dust; dust and ashes.\nHope it works before it smashes.\"\n—Argivian children\'s rhyme').
card_multiverse_id('crumble'/'5ED', '3970').

card_in_set('crusade', '5ED').
card_original_type('crusade'/'5ED', 'Enchantment').
card_original_text('crusade'/'5ED', 'All white creatures get +1/+1.').
card_image_name('crusade'/'5ED', 'crusade').
card_uid('crusade'/'5ED', '5ED:Crusade:crusade').
card_rarity('crusade'/'5ED', 'Rare').
card_artist('crusade'/'5ED', 'D. Alexander Gregory').
card_multiverse_id('crusade'/'5ED', '4119').

card_in_set('crystal rod', '5ED').
card_original_type('crystal rod'/'5ED', 'Artifact').
card_original_text('crystal rod'/'5ED', '{1}: Gain 1 life. Use this ability only when a blue spell is successfully cast and only once for each such spell.').
card_image_name('crystal rod'/'5ED', 'crystal rod').
card_uid('crystal rod'/'5ED', '5ED:Crystal Rod:crystal rod').
card_rarity('crystal rod'/'5ED', 'Uncommon').
card_artist('crystal rod'/'5ED', 'Donato Giancola').
card_multiverse_id('crystal rod'/'5ED', '3773').

card_in_set('cursed land', '5ED').
card_original_type('cursed land'/'5ED', 'Enchant Land').
card_original_text('cursed land'/'5ED', 'During the upkeep of enchanted land\'s controller, Cursed Land deals 1 damage to him or her.').
card_image_name('cursed land'/'5ED', 'cursed land').
card_uid('cursed land'/'5ED', '5ED:Cursed Land:cursed land').
card_rarity('cursed land'/'5ED', 'Uncommon').
card_artist('cursed land'/'5ED', 'Jesper Myrfors').
card_multiverse_id('cursed land'/'5ED', '3835').

card_in_set('d\'avenant archer', '5ED').
card_original_type('d\'avenant archer'/'5ED', 'Summon — Archer').
card_original_text('d\'avenant archer'/'5ED', '{T}: D\'Avenant Archer deals 1 damage to target attacking or blocking creature.').
card_image_name('d\'avenant archer'/'5ED', 'd\'avenant archer').
card_uid('d\'avenant archer'/'5ED', '5ED:D\'Avenant Archer:d\'avenant archer').
card_rarity('d\'avenant archer'/'5ED', 'Common').
card_artist('d\'avenant archer'/'5ED', 'Douglas Shuler').
card_flavor_text('d\'avenant archer'/'5ED', 'Avenant\'s archers are also trained as poets, so that each arrow is guided by a fragment of verse.').
card_multiverse_id('d\'avenant archer'/'5ED', '4120').

card_in_set('dance of many', '5ED').
card_original_type('dance of many'/'5ED', 'Enchantment').
card_original_text('dance of many'/'5ED', 'During your upkeep, pay {U}{U} or bury Dance of Many.\nWhen you play Dance of Many, choose target summon card. When Dance of Many comes into play, put a token creature into play and treat it as an exact copy of that summon card. If either Dance of Many or the token creature leaves play, bury the other.').
card_image_name('dance of many'/'5ED', 'dance of many').
card_uid('dance of many'/'5ED', '5ED:Dance of Many:dance of many').
card_rarity('dance of many'/'5ED', 'Rare').
card_artist('dance of many'/'5ED', 'Sandra Everingham').
card_multiverse_id('dance of many'/'5ED', '3899').

card_in_set('dancing scimitar', '5ED').
card_original_type('dancing scimitar'/'5ED', 'Artifact Creature').
card_original_text('dancing scimitar'/'5ED', 'Flying').
card_image_name('dancing scimitar'/'5ED', 'dancing scimitar').
card_uid('dancing scimitar'/'5ED', '5ED:Dancing Scimitar:dancing scimitar').
card_rarity('dancing scimitar'/'5ED', 'Rare').
card_artist('dancing scimitar'/'5ED', 'Anson Maddocks').
card_flavor_text('dancing scimitar'/'5ED', 'Bobbing merrily from opponent to opponent, the scimitar began adding playful little flourishes to its strokes; it even turned a couple of somersaults.').
card_multiverse_id('dancing scimitar'/'5ED', '3774').

card_in_set('dandân', '5ED').
card_original_type('dandân'/'5ED', 'Summon — Dandân').
card_original_text('dandân'/'5ED', 'Islandhome (If defending player controls no islands, this creature cannot attack. If you control no islands, bury this creature.)').
card_image_name('dandân'/'5ED', 'dandan').
card_uid('dandân'/'5ED', '5ED:Dandân:dandan').
card_rarity('dandân'/'5ED', 'Common').
card_artist('dandân'/'5ED', 'Drew Tucker').
card_flavor_text('dandân'/'5ED', '\"Catch good today. Start replacing crew tomorrow.\"\n—Faysal al-Mousa, fisher captain, log').
card_multiverse_id('dandân'/'5ED', '3900').

card_in_set('dark maze', '5ED').
card_original_type('dark maze'/'5ED', 'Summon — Wall').
card_original_text('dark maze'/'5ED', '{0}: Dark Maze can attack this turn as though it were not a Wall. At end of turn, remove Dark Maze from the game.').
card_image_name('dark maze'/'5ED', 'dark maze').
card_uid('dark maze'/'5ED', '5ED:Dark Maze:dark maze').
card_rarity('dark maze'/'5ED', 'Common').
card_artist('dark maze'/'5ED', 'David Seeley').
card_flavor_text('dark maze'/'5ED', '\"Stray not onto the path of darkness, or be lost forever.\"\n—Baki, wizard attendant').
card_multiverse_id('dark maze'/'5ED', '3901').

card_in_set('dark ritual', '5ED').
card_original_type('dark ritual'/'5ED', 'Mana Source').
card_original_text('dark ritual'/'5ED', 'Add {B}{B}{B} to your mana pool.').
card_image_name('dark ritual'/'5ED', 'dark ritual').
card_uid('dark ritual'/'5ED', '5ED:Dark Ritual:dark ritual').
card_rarity('dark ritual'/'5ED', 'Common').
card_artist('dark ritual'/'5ED', 'Clint Langley').
card_multiverse_id('dark ritual'/'5ED', '3836').

card_in_set('death speakers', '5ED').
card_original_type('death speakers'/'5ED', 'Summon — Speakers').
card_original_text('death speakers'/'5ED', 'Protection from black').
card_image_name('death speakers'/'5ED', 'death speakers').
card_uid('death speakers'/'5ED', '5ED:Death Speakers:death speakers').
card_rarity('death speakers'/'5ED', 'Common').
card_artist('death speakers'/'5ED', 'Andrew Robinson').
card_flavor_text('death speakers'/'5ED', '\"The dead may be beyond the law, but speaking the wisdom of the dead will afford no protection!\"\n—Calmont, Aysen bureaucrat').
card_multiverse_id('death speakers'/'5ED', '4121').

card_in_set('death ward', '5ED').
card_original_type('death ward'/'5ED', 'Instant').
card_original_text('death ward'/'5ED', 'Regenerate target creature.').
card_image_name('death ward'/'5ED', 'death ward').
card_uid('death ward'/'5ED', '5ED:Death Ward:death ward').
card_rarity('death ward'/'5ED', 'Common').
card_artist('death ward'/'5ED', 'Mark Poole').
card_multiverse_id('death ward'/'5ED', '4122').

card_in_set('deathgrip', '5ED').
card_original_type('deathgrip'/'5ED', 'Enchantment').
card_original_text('deathgrip'/'5ED', '{B}{B} Counter target green spell. Play this ability as an interrupt.').
card_image_name('deathgrip'/'5ED', 'deathgrip').
card_uid('deathgrip'/'5ED', '5ED:Deathgrip:deathgrip').
card_rarity('deathgrip'/'5ED', 'Uncommon').
card_artist('deathgrip'/'5ED', 'Anson Maddocks').
card_flavor_text('deathgrip'/'5ED', '\"Every forest is carpeted with the corpses of a thousand trees.\"\n—Baron Sengir to Autumn Willow').
card_multiverse_id('deathgrip'/'5ED', '3837').

card_in_set('deflection', '5ED').
card_original_type('deflection'/'5ED', 'Interrupt').
card_original_text('deflection'/'5ED', 'Target spell with a single target now targets a new legal target of your choice.').
card_image_name('deflection'/'5ED', 'deflection').
card_uid('deflection'/'5ED', '5ED:Deflection:deflection').
card_rarity('deflection'/'5ED', 'Rare').
card_artist('deflection'/'5ED', 'Mike Raabe').
card_flavor_text('deflection'/'5ED', 'Up and down,\nover and through,\nback around—\nthe joke\'s on you.').
card_multiverse_id('deflection'/'5ED', '3902').

card_in_set('derelor', '5ED').
card_original_type('derelor'/'5ED', 'Summon — Thrull').
card_original_text('derelor'/'5ED', 'Your black spells cost an additional {B} to play.').
card_image_name('derelor'/'5ED', 'derelor').
card_uid('derelor'/'5ED', '5ED:Derelor:derelor').
card_rarity('derelor'/'5ED', 'Rare').
card_artist('derelor'/'5ED', 'Anson Maddocks').
card_flavor_text('derelor'/'5ED', '\"The derelor\'s greatest contribution to the Ebon Hand was the inspirational effect its creator\'s execution had upon the other thrull breeders.\"\n—Sarpadian Empires, vol. II').
card_multiverse_id('derelor'/'5ED', '3838').

card_in_set('desert twister', '5ED').
card_original_type('desert twister'/'5ED', 'Sorcery').
card_original_text('desert twister'/'5ED', 'Destroy target permanent.').
card_image_name('desert twister'/'5ED', 'desert twister').
card_uid('desert twister'/'5ED', '5ED:Desert Twister:desert twister').
card_rarity('desert twister'/'5ED', 'Uncommon').
card_artist('desert twister'/'5ED', 'Susan Van Camp').
card_multiverse_id('desert twister'/'5ED', '3971').

card_in_set('detonate', '5ED').
card_original_type('detonate'/'5ED', 'Sorcery').
card_original_text('detonate'/'5ED', 'Bury target artifact with total casting cost equal to X. Detonate deals X damage to that artifact\'s controller.').
card_image_name('detonate'/'5ED', 'detonate').
card_uid('detonate'/'5ED', '5ED:Detonate:detonate').
card_rarity('detonate'/'5ED', 'Uncommon').
card_artist('detonate'/'5ED', 'Randy Asplund-Faith').
card_multiverse_id('detonate'/'5ED', '4039').

card_in_set('diabolic machine', '5ED').
card_original_type('diabolic machine'/'5ED', 'Artifact Creature').
card_original_text('diabolic machine'/'5ED', '{3}: Regenerate').
card_image_name('diabolic machine'/'5ED', 'diabolic machine').
card_uid('diabolic machine'/'5ED', '5ED:Diabolic Machine:diabolic machine').
card_rarity('diabolic machine'/'5ED', 'Uncommon').
card_artist('diabolic machine'/'5ED', 'James Allen').
card_flavor_text('diabolic machine'/'5ED', '\"The bolts of our ballistae smashed into the monstrous thing, but our hopes died in our chests as its gears continued turning.\"\n—Sevti Mukul, The Fall of Alsoor').
card_multiverse_id('diabolic machine'/'5ED', '3775').

card_in_set('dingus egg', '5ED').
card_original_type('dingus egg'/'5ED', 'Artifact').
card_original_text('dingus egg'/'5ED', 'Whenever a land is put into any graveyard from play, Dingus Egg deals 2 damage to that land\'s controller.').
card_image_name('dingus egg'/'5ED', 'dingus egg').
card_uid('dingus egg'/'5ED', '5ED:Dingus Egg:dingus egg').
card_rarity('dingus egg'/'5ED', 'Rare').
card_artist('dingus egg'/'5ED', 'Randy Gallegos').
card_multiverse_id('dingus egg'/'5ED', '3776').

card_in_set('disenchant', '5ED').
card_original_type('disenchant'/'5ED', 'Instant').
card_original_text('disenchant'/'5ED', 'Destroy target artifact or enchantment.').
card_image_name('disenchant'/'5ED', 'disenchant').
card_uid('disenchant'/'5ED', '5ED:Disenchant:disenchant').
card_rarity('disenchant'/'5ED', 'Common').
card_artist('disenchant'/'5ED', 'Brian Snõddy').
card_multiverse_id('disenchant'/'5ED', '4123').

card_in_set('disintegrate', '5ED').
card_original_type('disintegrate'/'5ED', 'Sorcery').
card_original_text('disintegrate'/'5ED', 'Disintegrate deals X damage to target creature or player. That creature cannot regenerate this turn. If the creature is dealt lethal damage this turn, remove it from the game.').
card_image_name('disintegrate'/'5ED', 'disintegrate').
card_uid('disintegrate'/'5ED', '5ED:Disintegrate:disintegrate').
card_rarity('disintegrate'/'5ED', 'Common').
card_artist('disintegrate'/'5ED', 'Anson Maddocks').
card_multiverse_id('disintegrate'/'5ED', '4040').

card_in_set('disrupting scepter', '5ED').
card_original_type('disrupting scepter'/'5ED', 'Artifact').
card_original_text('disrupting scepter'/'5ED', '{3}, {T}: Target player chooses and discards a card. Use this ability only during your turn.').
card_image_name('disrupting scepter'/'5ED', 'disrupting scepter').
card_uid('disrupting scepter'/'5ED', '5ED:Disrupting Scepter:disrupting scepter').
card_rarity('disrupting scepter'/'5ED', 'Rare').
card_artist('disrupting scepter'/'5ED', 'Stuart Griffin').
card_multiverse_id('disrupting scepter'/'5ED', '3777').

card_in_set('divine offering', '5ED').
card_original_type('divine offering'/'5ED', 'Instant').
card_original_text('divine offering'/'5ED', 'Destroy target artifact. Gain an amount of life equal to that artifact\'s total casting cost.').
card_image_name('divine offering'/'5ED', 'divine offering').
card_uid('divine offering'/'5ED', '5ED:Divine Offering:divine offering').
card_rarity('divine offering'/'5ED', 'Common').
card_artist('divine offering'/'5ED', 'Jeff A. Menges').
card_flavor_text('divine offering'/'5ED', 'These tears of pain I weep to the skies, Not bitter drops, but water made divine. Each gem of sorrow slips, no longer mine, But proffered thus to echo other cries.').
card_multiverse_id('divine offering'/'5ED', '4124').

card_in_set('divine transformation', '5ED').
card_original_type('divine transformation'/'5ED', 'Enchant Creature').
card_original_text('divine transformation'/'5ED', 'Enchanted creature gets +3/+3.').
card_image_name('divine transformation'/'5ED', 'divine transformation').
card_uid('divine transformation'/'5ED', '5ED:Divine Transformation:divine transformation').
card_rarity('divine transformation'/'5ED', 'Uncommon').
card_artist('divine transformation'/'5ED', 'NéNé Thomas').
card_flavor_text('divine transformation'/'5ED', 'Glory surged through her and radiance surrounded her. All things were possible with the blessing of the Divine.').
card_multiverse_id('divine transformation'/'5ED', '4125').

card_in_set('dragon engine', '5ED').
card_original_type('dragon engine'/'5ED', 'Artifact Creature').
card_original_text('dragon engine'/'5ED', '{2}: +1/+0 until end of turn').
card_image_name('dragon engine'/'5ED', 'dragon engine').
card_uid('dragon engine'/'5ED', '5ED:Dragon Engine:dragon engine').
card_rarity('dragon engine'/'5ED', 'Rare').
card_artist('dragon engine'/'5ED', 'Anson Maddocks').
card_flavor_text('dragon engine'/'5ED', '\"How is it the legged engines seem so much more alive than their wheeled counterparts? Simply, they were born, not made.\"\n—Mishra').
card_multiverse_id('dragon engine'/'5ED', '3778').

card_in_set('drain life', '5ED').
card_original_type('drain life'/'5ED', 'Sorcery').
card_original_text('drain life'/'5ED', '{X} Drain Life deals X damage to target creature or player. Spend only black mana in this way. Gain 1 life for each 1 damage dealt, but not more than the toughness of the creature or the life total of the player Drain Life damages.').
card_image_name('drain life'/'5ED', 'drain life').
card_uid('drain life'/'5ED', '5ED:Drain Life:drain life').
card_rarity('drain life'/'5ED', 'Common').
card_artist('drain life'/'5ED', 'Andrew Robinson').
card_multiverse_id('drain life'/'5ED', '3839').

card_in_set('drain power', '5ED').
card_original_type('drain power'/'5ED', 'Sorcery').
card_original_text('drain power'/'5ED', 'Target player draws all mana from all lands he or she controls. Put all mana from that player\'s mana pool into yours.').
card_image_name('drain power'/'5ED', 'drain power').
card_uid('drain power'/'5ED', '5ED:Drain Power:drain power').
card_rarity('drain power'/'5ED', 'Rare').
card_artist('drain power'/'5ED', 'Jerry Tiritilli').
card_multiverse_id('drain power'/'5ED', '3903').

card_in_set('drudge skeletons', '5ED').
card_original_type('drudge skeletons'/'5ED', 'Summon — Skeletons').
card_original_text('drudge skeletons'/'5ED', '{B} Regenerate').
card_image_name('drudge skeletons'/'5ED', 'drudge skeletons').
card_uid('drudge skeletons'/'5ED', '5ED:Drudge Skeletons:drudge skeletons').
card_rarity('drudge skeletons'/'5ED', 'Common').
card_artist('drudge skeletons'/'5ED', 'Ian Miller').
card_flavor_text('drudge skeletons'/'5ED', '\"The dead make good soldiers. They can\'t disobey orders, they never surrender, and they don\'t stop fighting when a random body part falls off.\"\n—Nevinyrral,\nNecromancer\'s Handbook').
card_multiverse_id('drudge skeletons'/'5ED', '3840').

card_in_set('durkwood boars', '5ED').
card_original_type('durkwood boars'/'5ED', 'Summon — Boars').
card_original_text('durkwood boars'/'5ED', '').
card_image_name('durkwood boars'/'5ED', 'durkwood boars').
card_uid('durkwood boars'/'5ED', '5ED:Durkwood Boars:durkwood boars').
card_rarity('durkwood boars'/'5ED', 'Common').
card_artist('durkwood boars'/'5ED', 'Mike Kimble').
card_flavor_text('durkwood boars'/'5ED', '\"And the unclean spirits went out, and entered the swine: and the herd ran violently . . . .\"\n—The Bible, Mark 5:13').
card_multiverse_id('durkwood boars'/'5ED', '3972').

card_in_set('dust to dust', '5ED').
card_original_type('dust to dust'/'5ED', 'Sorcery').
card_original_text('dust to dust'/'5ED', 'Remove two target artifacts from the game.').
card_image_name('dust to dust'/'5ED', 'dust to dust').
card_uid('dust to dust'/'5ED', '5ED:Dust to Dust:dust to dust').
card_rarity('dust to dust'/'5ED', 'Uncommon').
card_artist('dust to dust'/'5ED', 'Doug Keith').
card_flavor_text('dust to dust'/'5ED', '\"All this nonsense made by mages\nRusts and crumbles through the ages.\"\n—Aline Corralurn, \"Inheritance\"').
card_multiverse_id('dust to dust'/'5ED', '4126').

card_in_set('dwarven catapult', '5ED').
card_original_type('dwarven catapult'/'5ED', 'Instant').
card_original_text('dwarven catapult'/'5ED', 'Dwarven Catapult deals X damage divided evenly, rounded down, among all creatures target opponent controls.').
card_image_name('dwarven catapult'/'5ED', 'dwarven catapult').
card_uid('dwarven catapult'/'5ED', '5ED:Dwarven Catapult:dwarven catapult').
card_rarity('dwarven catapult'/'5ED', 'Uncommon').
card_artist('dwarven catapult'/'5ED', 'Jeff A. Menges').
card_flavor_text('dwarven catapult'/'5ED', '\"Often greatly outnumbered in battle, dwarves relied on catapults as one means of damaging a large army.\"\n—Sarpadian Empires, vol. IV').
card_multiverse_id('dwarven catapult'/'5ED', '4041').

card_in_set('dwarven hold', '5ED').
card_original_type('dwarven hold'/'5ED', 'Land').
card_original_text('dwarven hold'/'5ED', 'Dwarven Hold comes into play tapped.\nYou may choose not to untap Dwarven Hold during your untap phase and put a storage counter on it instead.\n{T}, Remove X storage counters from Dwarven Hold: Add an amount of {R} equal to X to your mana pool.').
card_image_name('dwarven hold'/'5ED', 'dwarven hold').
card_uid('dwarven hold'/'5ED', '5ED:Dwarven Hold:dwarven hold').
card_rarity('dwarven hold'/'5ED', 'Rare').
card_artist('dwarven hold'/'5ED', 'David Seeley').
card_multiverse_id('dwarven hold'/'5ED', '4179').

card_in_set('dwarven ruins', '5ED').
card_original_type('dwarven ruins'/'5ED', 'Land').
card_original_text('dwarven ruins'/'5ED', 'Dwarven Ruins comes into play tapped.\n{T}: Add {R} to your mana pool.\n{T}, Sacrifice Dwarven Ruins: Add {R}{R} to your mana pool.').
card_image_name('dwarven ruins'/'5ED', 'dwarven ruins').
card_uid('dwarven ruins'/'5ED', '5ED:Dwarven Ruins:dwarven ruins').
card_rarity('dwarven ruins'/'5ED', 'Uncommon').
card_artist('dwarven ruins'/'5ED', 'Liz Danforth').
card_multiverse_id('dwarven ruins'/'5ED', '4180').

card_in_set('dwarven soldier', '5ED').
card_original_type('dwarven soldier'/'5ED', 'Summon — Dwarf').
card_original_text('dwarven soldier'/'5ED', 'If Dwarven Soldier blocks or is blocked by any Orcs, it gets +0/+2 until end of turn.').
card_image_name('dwarven soldier'/'5ED', 'dwarven soldier').
card_uid('dwarven soldier'/'5ED', '5ED:Dwarven Soldier:dwarven soldier').
card_rarity('dwarven soldier'/'5ED', 'Common').
card_artist('dwarven soldier'/'5ED', 'Randy Asplund-Faith').
card_flavor_text('dwarven soldier'/'5ED', '\"Orc must die / Orc must die / Not an orc? / March on by.\"\n—Dwarven marching song').
card_multiverse_id('dwarven soldier'/'5ED', '4042').

card_in_set('dwarven warriors', '5ED').
card_original_type('dwarven warriors'/'5ED', 'Summon — Dwarves').
card_original_text('dwarven warriors'/'5ED', '{T}: Target creature with power 2 or less is unblockable this turn.').
card_image_name('dwarven warriors'/'5ED', 'dwarven warriors').
card_uid('dwarven warriors'/'5ED', '5ED:Dwarven Warriors:dwarven warriors').
card_rarity('dwarven warriors'/'5ED', 'Common').
card_artist('dwarven warriors'/'5ED', 'Douglas Shuler').
card_multiverse_id('dwarven warriors'/'5ED', '4043').

card_in_set('earthquake', '5ED').
card_original_type('earthquake'/'5ED', 'Sorcery').
card_original_text('earthquake'/'5ED', 'Earthquake deals X damage to each creature without flying and each player.').
card_image_name('earthquake'/'5ED', 'earthquake').
card_uid('earthquake'/'5ED', '5ED:Earthquake:earthquake').
card_rarity('earthquake'/'5ED', 'Rare').
card_artist('earthquake'/'5ED', 'Richard Kane Ferguson').
card_multiverse_id('earthquake'/'5ED', '4044').

card_in_set('ebon stronghold', '5ED').
card_original_type('ebon stronghold'/'5ED', 'Land').
card_original_text('ebon stronghold'/'5ED', 'Ebon Stronghold comes into play tapped.\n{T}: Add {B} to your mana pool.\n{T}, Sacrifice Ebon Stronghold: Add {B}{B} to your mana pool.').
card_image_name('ebon stronghold'/'5ED', 'ebon stronghold').
card_uid('ebon stronghold'/'5ED', '5ED:Ebon Stronghold:ebon stronghold').
card_rarity('ebon stronghold'/'5ED', 'Uncommon').
card_artist('ebon stronghold'/'5ED', 'Liz Danforth').
card_multiverse_id('ebon stronghold'/'5ED', '4181').

card_in_set('elder druid', '5ED').
card_original_type('elder druid'/'5ED', 'Summon — Cleric').
card_original_text('elder druid'/'5ED', '{3}{G}, {T}: Tap or untap target artifact, creature, or land.').
card_image_name('elder druid'/'5ED', 'elder druid').
card_uid('elder druid'/'5ED', '5ED:Elder Druid:elder druid').
card_rarity('elder druid'/'5ED', 'Rare').
card_artist('elder druid'/'5ED', 'Richard Kane Ferguson').
card_flavor_text('elder druid'/'5ED', '\"Did clouds dance in his eyes,\ndid thorns play at his fingertips?\"\n—Shesul Fass, faerie band').
card_multiverse_id('elder druid'/'5ED', '3973').

card_in_set('elkin bottle', '5ED').
card_original_type('elkin bottle'/'5ED', 'Artifact').
card_original_text('elkin bottle'/'5ED', '{3}, {T}: Set the top card of your library aside face up. You may play that card as though it were in your hand. At the beginning of your next turn, bury the card if you have not played it.').
card_image_name('elkin bottle'/'5ED', 'elkin bottle').
card_uid('elkin bottle'/'5ED', '5ED:Elkin Bottle:elkin bottle').
card_rarity('elkin bottle'/'5ED', 'Rare').
card_artist('elkin bottle'/'5ED', 'Quinton Hoover').
card_multiverse_id('elkin bottle'/'5ED', '3779').

card_in_set('elven riders', '5ED').
card_original_type('elven riders'/'5ED', 'Summon — Riders').
card_original_text('elven riders'/'5ED', 'Elven Riders cannot be blocked except by Walls or creatures with flying.').
card_image_name('elven riders'/'5ED', 'elven riders').
card_uid('elven riders'/'5ED', '5ED:Elven Riders:elven riders').
card_rarity('elven riders'/'5ED', 'Uncommon').
card_artist('elven riders'/'5ED', 'Dan Frazier').
card_flavor_text('elven riders'/'5ED', 'Sometimes it is better to be swift of foot than strong of swordarm.\n—Elven proverb').
card_multiverse_id('elven riders'/'5ED', '3974').

card_in_set('elvish archers', '5ED').
card_original_type('elvish archers'/'5ED', 'Summon — Elves').
card_original_text('elvish archers'/'5ED', 'First strike').
card_image_name('elvish archers'/'5ED', 'elvish archers').
card_uid('elvish archers'/'5ED', '5ED:Elvish Archers:elvish archers').
card_rarity('elvish archers'/'5ED', 'Rare').
card_artist('elvish archers'/'5ED', 'Anson Maddocks').
card_flavor_text('elvish archers'/'5ED', 'I tell you, there was so many arrows flying about you couldn\'t hardly see the sun. So I says to young Angus, \"Well, at least now we\'re fighting in the shade!\"').
card_multiverse_id('elvish archers'/'5ED', '3975').

card_in_set('energy flux', '5ED').
card_original_type('energy flux'/'5ED', 'Enchantment').
card_original_text('energy flux'/'5ED', 'All artifacts gain \"During your upkeep, pay {2} or bury this artifact.\"').
card_image_name('energy flux'/'5ED', 'energy flux').
card_uid('energy flux'/'5ED', '5ED:Energy Flux:energy flux').
card_rarity('energy flux'/'5ED', 'Uncommon').
card_artist('energy flux'/'5ED', 'Kaja Foglio').
card_flavor_text('energy flux'/'5ED', '\"Nothing endures but change.\"\n—Heraclitus').
card_multiverse_id('energy flux'/'5ED', '3904').

card_in_set('enervate', '5ED').
card_original_type('enervate'/'5ED', 'Instant').
card_original_text('enervate'/'5ED', 'Tap target artifact, creature, or land. Draw a card at the beginning of the next turn.').
card_image_name('enervate'/'5ED', 'enervate').
card_uid('enervate'/'5ED', '5ED:Enervate:enervate').
card_rarity('enervate'/'5ED', 'Common').
card_artist('enervate'/'5ED', 'L. A. Williams').
card_flavor_text('enervate'/'5ED', '\"Worlds turn in crucial moments of decision. Make your choice.\"\n—Gustha Ebbasdotter,\nKjeldoran royal mage').
card_multiverse_id('enervate'/'5ED', '3905').

card_in_set('erg raiders', '5ED').
card_original_type('erg raiders'/'5ED', 'Summon — Raiders').
card_original_text('erg raiders'/'5ED', 'At the end of your turn, Erg Raiders deals 2 damage to you if it did not attack this turn. Ignore this effect if Erg Raiders has summoning sickness.').
card_image_name('erg raiders'/'5ED', 'erg raiders').
card_uid('erg raiders'/'5ED', '5ED:Erg Raiders:erg raiders').
card_rarity('erg raiders'/'5ED', 'Common').
card_artist('erg raiders'/'5ED', 'Stuart Griffin').
card_multiverse_id('erg raiders'/'5ED', '3841').

card_in_set('errantry', '5ED').
card_original_type('errantry'/'5ED', 'Enchant Creature').
card_original_text('errantry'/'5ED', 'Enchanted creature gets +3/+0 and cannot attack during any turn in which any other creatures attack.').
card_image_name('errantry'/'5ED', 'errantry').
card_uid('errantry'/'5ED', '5ED:Errantry:errantry').
card_rarity('errantry'/'5ED', 'Common').
card_artist('errantry'/'5ED', 'Scott Kirschner').
card_flavor_text('errantry'/'5ED', '\"There is no shame in solitude. The lone knight may succeed where a hundred founder.\"\n—Arna Kennerüd, skyknight').
card_multiverse_id('errantry'/'5ED', '4045').

card_in_set('eternal warrior', '5ED').
card_original_type('eternal warrior'/'5ED', 'Enchant Creature').
card_original_text('eternal warrior'/'5ED', 'Attacking does not cause enchanted creature to tap.').
card_image_name('eternal warrior'/'5ED', 'eternal warrior').
card_uid('eternal warrior'/'5ED', '5ED:Eternal Warrior:eternal warrior').
card_rarity('eternal warrior'/'5ED', 'Common').
card_artist('eternal warrior'/'5ED', 'Anson Maddocks').
card_flavor_text('eternal warrior'/'5ED', 'Warriors of the Tsunami-nito School spend years in training to master the way of effortless effort.').
card_multiverse_id('eternal warrior'/'5ED', '4046').

card_in_set('evil eye of orms-by-gore', '5ED').
card_original_type('evil eye of orms-by-gore'/'5ED', 'Summon — Evil Eye').
card_original_text('evil eye of orms-by-gore'/'5ED', 'Evil Eye of Orms-by-Gore cannot be blocked except by Walls.\nExcept for Evil Eyes, creatures you control cannot attack.').
card_image_name('evil eye of orms-by-gore'/'5ED', 'evil eye of orms-by-gore').
card_uid('evil eye of orms-by-gore'/'5ED', '5ED:Evil Eye of Orms-by-Gore:evil eye of orms-by-gore').
card_rarity('evil eye of orms-by-gore'/'5ED', 'Uncommon').
card_artist('evil eye of orms-by-gore'/'5ED', 'George Pratt').
card_flavor_text('evil eye of orms-by-gore'/'5ED', 'The highway of fear is the shortest route to defeat.').
card_multiverse_id('evil eye of orms-by-gore'/'5ED', '3842').

card_in_set('evil presence', '5ED').
card_original_type('evil presence'/'5ED', 'Enchant Land').
card_original_text('evil presence'/'5ED', 'Enchanted land is a swamp.').
card_image_name('evil presence'/'5ED', 'evil presence').
card_uid('evil presence'/'5ED', '5ED:Evil Presence:evil presence').
card_rarity('evil presence'/'5ED', 'Uncommon').
card_artist('evil presence'/'5ED', 'Bob Eggleton').
card_multiverse_id('evil presence'/'5ED', '3843').

card_in_set('eye for an eye', '5ED').
card_original_type('eye for an eye'/'5ED', 'Instant').
card_original_text('eye for an eye'/'5ED', 'Play only when a creature, spell, or effect assigns damage to you. Eye for an Eye deals an equal amount of damage to that source\'s controller.').
card_image_name('eye for an eye'/'5ED', 'eye for an eye').
card_uid('eye for an eye'/'5ED', '5ED:Eye for an Eye:eye for an eye').
card_rarity('eye for an eye'/'5ED', 'Rare').
card_artist('eye for an eye'/'5ED', 'Mark Poole').
card_multiverse_id('eye for an eye'/'5ED', '4127').

card_in_set('fallen angel', '5ED').
card_original_type('fallen angel'/'5ED', 'Summon — Angel').
card_original_text('fallen angel'/'5ED', 'Flying\nSacrifice a creature: +2/+1 until end of turn').
card_image_name('fallen angel'/'5ED', 'fallen angel').
card_uid('fallen angel'/'5ED', '5ED:Fallen Angel:fallen angel').
card_rarity('fallen angel'/'5ED', 'Uncommon').
card_artist('fallen angel'/'5ED', 'Anson Maddocks').
card_flavor_text('fallen angel'/'5ED', '\"Angels are simply extensions of truth upon the fabric of life—and there is far more dark than light.\"\n—Baron Sengir').
card_multiverse_id('fallen angel'/'5ED', '3844').

card_in_set('fear', '5ED').
card_original_type('fear'/'5ED', 'Enchant Creature').
card_original_text('fear'/'5ED', 'Enchanted creature cannot be blocked except by artifact creatures and black creatures.').
card_image_name('fear'/'5ED', 'fear').
card_uid('fear'/'5ED', '5ED:Fear:fear').
card_rarity('fear'/'5ED', 'Common').
card_artist('fear'/'5ED', 'Doug Keith').
card_multiverse_id('fear'/'5ED', '3845').

card_in_set('feedback', '5ED').
card_original_type('feedback'/'5ED', 'Enchant Enchantment').
card_original_text('feedback'/'5ED', 'During the upkeep of enchanted enchantment\'s controller, Feedback deals 1 damage to him or her.').
card_image_name('feedback'/'5ED', 'feedback').
card_uid('feedback'/'5ED', '5ED:Feedback:feedback').
card_rarity('feedback'/'5ED', 'Uncommon').
card_artist('feedback'/'5ED', 'Quinton Hoover').
card_multiverse_id('feedback'/'5ED', '3906').

card_in_set('feldon\'s cane', '5ED').
card_original_type('feldon\'s cane'/'5ED', 'Artifact').
card_original_text('feldon\'s cane'/'5ED', '{T}, Remove Feldon\'s Cane from the game: Shuffle your graveyard into your library.').
card_image_name('feldon\'s cane'/'5ED', 'feldon\'s cane').
card_uid('feldon\'s cane'/'5ED', '5ED:Feldon\'s Cane:feldon\'s cane').
card_rarity('feldon\'s cane'/'5ED', 'Uncommon').
card_artist('feldon\'s cane'/'5ED', 'Mark Tedin').
card_flavor_text('feldon\'s cane'/'5ED', 'Feldon found the first of these canes frozen in the Ronom Glacier.').
card_multiverse_id('feldon\'s cane'/'5ED', '3780').

card_in_set('fellwar stone', '5ED').
card_original_type('fellwar stone'/'5ED', 'Artifact').
card_original_text('fellwar stone'/'5ED', '{T}: Add to your mana pool one mana of any type that any opponent\'s lands can produce. Play this ability as a mana source.').
card_image_name('fellwar stone'/'5ED', 'fellwar stone').
card_uid('fellwar stone'/'5ED', '5ED:Fellwar Stone:fellwar stone').
card_rarity('fellwar stone'/'5ED', 'Uncommon').
card_artist('fellwar stone'/'5ED', 'Quinton Hoover').
card_flavor_text('fellwar stone'/'5ED', 'Throw stones, and throw away the world.\n—Dwarven proverb').
card_multiverse_id('fellwar stone'/'5ED', '3781').

card_in_set('feroz\'s ban', '5ED').
card_original_type('feroz\'s ban'/'5ED', 'Artifact').
card_original_text('feroz\'s ban'/'5ED', 'Summon spells cost an additional {2} to play.').
card_image_name('feroz\'s ban'/'5ED', 'feroz\'s ban').
card_uid('feroz\'s ban'/'5ED', '5ED:Feroz\'s Ban:feroz\'s ban').
card_rarity('feroz\'s ban'/'5ED', 'Rare').
card_artist('feroz\'s ban'/'5ED', 'Heather Hudson').
card_flavor_text('feroz\'s ban'/'5ED', '\"Without the protection of Feroz\'s Ban, I fear the Homelands are lost.\"\n—Daria').
card_multiverse_id('feroz\'s ban'/'5ED', '3782').

card_in_set('fire drake', '5ED').
card_original_type('fire drake'/'5ED', 'Summon — Drake').
card_original_text('fire drake'/'5ED', 'Flying\n{R} +1/+0 until end of turn. You cannot spend more than {R} in this way each turn.').
card_image_name('fire drake'/'5ED', 'fire drake').
card_uid('fire drake'/'5ED', '5ED:Fire Drake:fire drake').
card_rarity('fire drake'/'5ED', 'Uncommon').
card_artist('fire drake'/'5ED', 'Christopher Rush').
card_flavor_text('fire drake'/'5ED', 'Brimstone marks this drake\'s territory. Unfortunately for travelers, all of the Burning Isles smell likewise.').
card_multiverse_id('fire drake'/'5ED', '4047').

card_in_set('fireball', '5ED').
card_original_type('fireball'/'5ED', 'Sorcery').
card_original_text('fireball'/'5ED', 'Pay {1} for each target beyond the first: Fireball deals X damage divided evenly, rounded down, among any number of target creatures and/or players.').
card_image_name('fireball'/'5ED', 'fireball').
card_uid('fireball'/'5ED', '5ED:Fireball:fireball').
card_rarity('fireball'/'5ED', 'Common').
card_artist('fireball'/'5ED', 'Mark Tedin').
card_multiverse_id('fireball'/'5ED', '4048').

card_in_set('firebreathing', '5ED').
card_original_type('firebreathing'/'5ED', 'Enchant Creature').
card_original_text('firebreathing'/'5ED', '{R} Enchanted creature gets +1/+0 until end of turn.').
card_image_name('firebreathing'/'5ED', 'firebreathing').
card_uid('firebreathing'/'5ED', '5ED:Firebreathing:firebreathing').
card_rarity('firebreathing'/'5ED', 'Common').
card_artist('firebreathing'/'5ED', 'Dan Frazier').
card_flavor_text('firebreathing'/'5ED', '\"And topples round the dreary west\nA looming bastion fringed with fire.\"\n—Alfred, Lord Tennyson,\n\"In Memoriam\"').
card_multiverse_id('firebreathing'/'5ED', '4049').

card_in_set('flame spirit', '5ED').
card_original_type('flame spirit'/'5ED', 'Summon — Spirit').
card_original_text('flame spirit'/'5ED', '{R} +1/+0 until end of turn').
card_image_name('flame spirit'/'5ED', 'flame spirit').
card_uid('flame spirit'/'5ED', '5ED:Flame Spirit:flame spirit').
card_rarity('flame spirit'/'5ED', 'Uncommon').
card_artist('flame spirit'/'5ED', 'Justin Hampton').
card_flavor_text('flame spirit'/'5ED', '\"The spirit of the flame is the spirit of change.\"\n—Lovisa Coldeyes,\nBalduvian chieftain').
card_multiverse_id('flame spirit'/'5ED', '4050').

card_in_set('flare', '5ED').
card_original_type('flare'/'5ED', 'Instant').
card_original_text('flare'/'5ED', 'Flare deals 1 damage to target creature or player.\nDraw a card at the beginning of the next turn.').
card_image_name('flare'/'5ED', 'flare').
card_uid('flare'/'5ED', '5ED:Flare:flare').
card_rarity('flare'/'5ED', 'Common').
card_artist('flare'/'5ED', 'Andrew Robinson').
card_flavor_text('flare'/'5ED', 'The secret of destruction is simple: everything burns.').
card_multiverse_id('flare'/'5ED', '4051').

card_in_set('flashfires', '5ED').
card_original_type('flashfires'/'5ED', 'Sorcery').
card_original_text('flashfires'/'5ED', 'Destroy all plains.').
card_image_name('flashfires'/'5ED', 'flashfires').
card_uid('flashfires'/'5ED', '5ED:Flashfires:flashfires').
card_rarity('flashfires'/'5ED', 'Uncommon').
card_artist('flashfires'/'5ED', 'Dameon Willich').
card_multiverse_id('flashfires'/'5ED', '4052').

card_in_set('flight', '5ED').
card_original_type('flight'/'5ED', 'Enchant Creature').
card_original_text('flight'/'5ED', 'Enchanted creature gains flying.').
card_image_name('flight'/'5ED', 'flight').
card_uid('flight'/'5ED', '5ED:Flight:flight').
card_rarity('flight'/'5ED', 'Common').
card_artist('flight'/'5ED', 'Jerry Tiritilli').
card_multiverse_id('flight'/'5ED', '3907').

card_in_set('flood', '5ED').
card_original_type('flood'/'5ED', 'Enchantment').
card_original_text('flood'/'5ED', '{U}{U} Tap target creature without flying.').
card_image_name('flood'/'5ED', 'flood').
card_uid('flood'/'5ED', '5ED:Flood:flood').
card_rarity('flood'/'5ED', 'Common').
card_artist('flood'/'5ED', 'Dennis Detwiller').
card_flavor_text('flood'/'5ED', '\"A dash of cool water does wonders to clear a cluttered battlefield.\"\n—Vibekke Ragnild, Witches and War').
card_multiverse_id('flood'/'5ED', '3908').

card_in_set('flying carpet', '5ED').
card_original_type('flying carpet'/'5ED', 'Artifact').
card_original_text('flying carpet'/'5ED', '{2}, {T}: Target creature gains flying until end of turn. If that creature is put into any graveyard this turn, bury Flying Carpet.').
card_image_name('flying carpet'/'5ED', 'flying carpet').
card_uid('flying carpet'/'5ED', '5ED:Flying Carpet:flying carpet').
card_rarity('flying carpet'/'5ED', 'Rare').
card_artist('flying carpet'/'5ED', 'Mark Tedin').
card_multiverse_id('flying carpet'/'5ED', '3783').

card_in_set('fog', '5ED').
card_original_type('fog'/'5ED', 'Instant').
card_original_text('fog'/'5ED', 'Creatures deal no combat damage this turn.').
card_image_name('fog'/'5ED', 'fog').
card_uid('fog'/'5ED', '5ED:Fog:fog').
card_rarity('fog'/'5ED', 'Common').
card_artist('fog'/'5ED', 'John Avon').
card_multiverse_id('fog'/'5ED', '3976').

card_in_set('force of nature', '5ED').
card_original_type('force of nature'/'5ED', 'Summon — Force').
card_original_text('force of nature'/'5ED', 'Trample\nDuring your upkeep, pay {G}{G}{G}{G} or Force of Nature deals 8 damage to you.').
card_image_name('force of nature'/'5ED', 'force of nature').
card_uid('force of nature'/'5ED', '5ED:Force of Nature:force of nature').
card_rarity('force of nature'/'5ED', 'Rare').
card_artist('force of nature'/'5ED', 'Pete Venters').
card_flavor_text('force of nature'/'5ED', '\"It blinked, and flower petals fell like tears. Harthar, sensing weakness, attacked—and his blood fell like petals.\"\n—Shesul Fass, faerie bard').
card_multiverse_id('force of nature'/'5ED', '3977').

card_in_set('force spike', '5ED').
card_original_type('force spike'/'5ED', 'Interrupt').
card_original_text('force spike'/'5ED', 'Counter target spell unless its caster pays an additional {1}.').
card_image_name('force spike'/'5ED', 'force spike').
card_uid('force spike'/'5ED', '5ED:Force Spike:force spike').
card_rarity('force spike'/'5ED', 'Common').
card_artist('force spike'/'5ED', 'John Matson').
card_flavor_text('force spike'/'5ED', '\"Unknown spears / Suddenly hurtle before my dream-awakened eyes . . . .\"\n—William Butler Yeats,\n\"The Valley of the Black Pig\"').
card_multiverse_id('force spike'/'5ED', '3909').

card_in_set('forest', '5ED').
card_original_type('forest'/'5ED', 'Land').
card_original_text('forest'/'5ED', '{T}: Add {G} to your mana pool.').
card_image_name('forest'/'5ED', 'forest1').
card_uid('forest'/'5ED', '5ED:Forest:forest1').
card_rarity('forest'/'5ED', 'Basic Land').
card_artist('forest'/'5ED', 'David O\'Connor').
card_multiverse_id('forest'/'5ED', '4171').

card_in_set('forest', '5ED').
card_original_type('forest'/'5ED', 'Land').
card_original_text('forest'/'5ED', '{T}: Add {G} to your mana pool.').
card_image_name('forest'/'5ED', 'forest2').
card_uid('forest'/'5ED', '5ED:Forest:forest2').
card_rarity('forest'/'5ED', 'Basic Land').
card_artist('forest'/'5ED', 'David O\'Connor').
card_multiverse_id('forest'/'5ED', '4172').

card_in_set('forest', '5ED').
card_original_type('forest'/'5ED', 'Land').
card_original_text('forest'/'5ED', '{T}: Add {G} to your mana pool.').
card_image_name('forest'/'5ED', 'forest3').
card_uid('forest'/'5ED', '5ED:Forest:forest3').
card_rarity('forest'/'5ED', 'Basic Land').
card_artist('forest'/'5ED', 'David O\'Connor').
card_multiverse_id('forest'/'5ED', '4173').

card_in_set('forest', '5ED').
card_original_type('forest'/'5ED', 'Land').
card_original_text('forest'/'5ED', '{T}: Add {G} to your mana pool.').
card_image_name('forest'/'5ED', 'forest4').
card_uid('forest'/'5ED', '5ED:Forest:forest4').
card_rarity('forest'/'5ED', 'Basic Land').
card_artist('forest'/'5ED', 'David O\'Connor').
card_multiverse_id('forest'/'5ED', '4174').

card_in_set('forget', '5ED').
card_original_type('forget'/'5ED', 'Sorcery').
card_original_text('forget'/'5ED', 'Target player chooses and discards two cards, then draws as many cards as he or she discarded in this way.').
card_image_name('forget'/'5ED', 'forget').
card_uid('forget'/'5ED', '5ED:Forget:forget').
card_rarity('forget'/'5ED', 'Rare').
card_artist('forget'/'5ED', 'Mike Kimble').
card_multiverse_id('forget'/'5ED', '3910').

card_in_set('fountain of youth', '5ED').
card_original_type('fountain of youth'/'5ED', 'Artifact').
card_original_text('fountain of youth'/'5ED', '{2}, {T}: Gain 1 life.').
card_image_name('fountain of youth'/'5ED', 'fountain of youth').
card_uid('fountain of youth'/'5ED', '5ED:Fountain of Youth:fountain of youth').
card_rarity('fountain of youth'/'5ED', 'Uncommon').
card_artist('fountain of youth'/'5ED', 'Daniel Gelon').
card_flavor_text('fountain of youth'/'5ED', 'The fountain had stood in the town square for centuries, but only the pigeons knew its secret.').
card_multiverse_id('fountain of youth'/'5ED', '3784').

card_in_set('foxfire', '5ED').
card_original_type('foxfire'/'5ED', 'Instant').
card_original_text('foxfire'/'5ED', 'Untap target attacking creature. That creature neither deals nor receives combat damage this turn.\nDraw a card at the beginning of the next turn.').
card_image_name('foxfire'/'5ED', 'foxfire').
card_uid('foxfire'/'5ED', '5ED:Foxfire:foxfire').
card_rarity('foxfire'/'5ED', 'Common').
card_artist('foxfire'/'5ED', 'Margaret Organ-Kean').
card_flavor_text('foxfire'/'5ED', '\"Only the foolish fear foxfire.\"\n—Kolbjörn,\nElder Druid of the Juniper Order').
card_multiverse_id('foxfire'/'5ED', '3978').

card_in_set('frozen shade', '5ED').
card_original_type('frozen shade'/'5ED', 'Summon — Shade').
card_original_text('frozen shade'/'5ED', '{B} +1/+1 until end of turn').
card_image_name('frozen shade'/'5ED', 'frozen shade').
card_uid('frozen shade'/'5ED', '5ED:Frozen Shade:frozen shade').
card_rarity('frozen shade'/'5ED', 'Common').
card_artist('frozen shade'/'5ED', 'DiTerlizzi').
card_flavor_text('frozen shade'/'5ED', '\"There are some qualities, some incorporate things, / That have a double life, which thus is made / A type of twin entity which springs / From matter and light, evinced in solid and shade.\"\n—Edgar Allan Poe, \"Silence\"').
card_multiverse_id('frozen shade'/'5ED', '3846').

card_in_set('funeral march', '5ED').
card_original_type('funeral march'/'5ED', 'Enchant Creature').
card_original_text('funeral march'/'5ED', 'If enchanted creature leaves play, its controller sacrifices a creature.').
card_image_name('funeral march'/'5ED', 'funeral march').
card_uid('funeral march'/'5ED', '5ED:Funeral March:funeral march').
card_rarity('funeral march'/'5ED', 'Common').
card_artist('funeral march'/'5ED', 'John Coulthart').
card_flavor_text('funeral march'/'5ED', '\"Turn the key deftly in the oiled wards,\nAnd seal the hushed casket of my soul.\"\n—John Keats, \"To Sleep\"').
card_multiverse_id('funeral march'/'5ED', '3847').

card_in_set('fungusaur', '5ED').
card_original_type('fungusaur'/'5ED', 'Summon — Fungusaur').
card_original_text('fungusaur'/'5ED', 'At the end of any turn in which Fungusaur was damaged, put a +1/+1 counter on it.').
card_image_name('fungusaur'/'5ED', 'fungusaur').
card_uid('fungusaur'/'5ED', '5ED:Fungusaur:fungusaur').
card_rarity('fungusaur'/'5ED', 'Rare').
card_artist('fungusaur'/'5ED', 'Scott M. Fischer').
card_flavor_text('fungusaur'/'5ED', 'Rather than sheltering her young, the female fungusaur often injures her own offspring, thereby ensuring their rapid growth.').
card_multiverse_id('fungusaur'/'5ED', '3979').

card_in_set('fyndhorn elder', '5ED').
card_original_type('fyndhorn elder'/'5ED', 'Summon — Elf').
card_original_text('fyndhorn elder'/'5ED', '{T}: Add {G}{G} to your mana pool. Play this ability as a mana source.').
card_image_name('fyndhorn elder'/'5ED', 'fyndhorn elder').
card_uid('fyndhorn elder'/'5ED', '5ED:Fyndhorn Elder:fyndhorn elder').
card_rarity('fyndhorn elder'/'5ED', 'Uncommon').
card_artist('fyndhorn elder'/'5ED', 'Donato Giancola').
card_flavor_text('fyndhorn elder'/'5ED', '\"Do we know what we\'re doing? Yes—the will of Freyalise.\"\n—Laina of the Elvish Council').
card_multiverse_id('fyndhorn elder'/'5ED', '3980').

card_in_set('game of chaos', '5ED').
card_original_type('game of chaos'/'5ED', 'Sorcery').
card_original_text('game of chaos'/'5ED', 'Flip a coin; target opponent calls heads or tails while coin is in the air. The loser of the flip loses 1 life. The winner of the flip gains 1 life and may choose to repeat the process. Double the stakes each time.').
card_image_name('game of chaos'/'5ED', 'game of chaos').
card_uid('game of chaos'/'5ED', '5ED:Game of Chaos:game of chaos').
card_rarity('game of chaos'/'5ED', 'Rare').
card_artist('game of chaos'/'5ED', 'Thomas Gianni').
card_multiverse_id('game of chaos'/'5ED', '4053').

card_in_set('gaseous form', '5ED').
card_original_type('gaseous form'/'5ED', 'Enchant Creature').
card_original_text('gaseous form'/'5ED', 'Enchanted creature neither deals nor receives combat damage.').
card_image_name('gaseous form'/'5ED', 'gaseous form').
card_uid('gaseous form'/'5ED', '5ED:Gaseous Form:gaseous form').
card_rarity('gaseous form'/'5ED', 'Common').
card_artist('gaseous form'/'5ED', 'Doug Keith').
card_flavor_text('gaseous form'/'5ED', '\". . . [A]nd gives to airy nothing\nA local habitation and a name.\"\n—William Shakespeare, A Midsummer-Night\'s Dream').
card_multiverse_id('gaseous form'/'5ED', '3911').

card_in_set('gauntlets of chaos', '5ED').
card_original_type('gauntlets of chaos'/'5ED', 'Artifact').
card_original_text('gauntlets of chaos'/'5ED', '{5}, Sacrifice Gauntlets of Chaos: Exchange control of target artifact, creature, or land you control for control of target permanent of the same type that an opponent controls. Bury all enchantments played on those permanents.').
card_image_name('gauntlets of chaos'/'5ED', 'gauntlets of chaos').
card_uid('gauntlets of chaos'/'5ED', '5ED:Gauntlets of Chaos:gauntlets of chaos').
card_rarity('gauntlets of chaos'/'5ED', 'Rare').
card_artist('gauntlets of chaos'/'5ED', 'Alan Rabinowitz').
card_multiverse_id('gauntlets of chaos'/'5ED', '3785').

card_in_set('ghazbán ogre', '5ED').
card_original_type('ghazbán ogre'/'5ED', 'Summon — Ogre').
card_original_text('ghazbán ogre'/'5ED', 'During your upkeep, if a player has more life than any other, he or she gains control of Ghazbán Ogre.').
card_image_name('ghazbán ogre'/'5ED', 'ghazban ogre').
card_uid('ghazbán ogre'/'5ED', '5ED:Ghazbán Ogre:ghazban ogre').
card_rarity('ghazbán ogre'/'5ED', 'Common').
card_artist('ghazbán ogre'/'5ED', 'Mike Raabe').
card_flavor_text('ghazbán ogre'/'5ED', '\"We were holding our own—until the ogre guiding us suddenly grinned and turned on our commander.\"\n—Ivra Jursdotter, Sarpadian mercenary').
card_multiverse_id('ghazbán ogre'/'5ED', '3981').

card_in_set('giant growth', '5ED').
card_original_type('giant growth'/'5ED', 'Instant').
card_original_text('giant growth'/'5ED', 'Target creature gets +3/+3 until end of turn.').
card_image_name('giant growth'/'5ED', 'giant growth').
card_uid('giant growth'/'5ED', '5ED:Giant Growth:giant growth').
card_rarity('giant growth'/'5ED', 'Common').
card_artist('giant growth'/'5ED', 'DiTerlizzi').
card_multiverse_id('giant growth'/'5ED', '3982').

card_in_set('giant spider', '5ED').
card_original_type('giant spider'/'5ED', 'Summon — Spider').
card_original_text('giant spider'/'5ED', 'Giant Spider can block creatures with flying.').
card_image_name('giant spider'/'5ED', 'giant spider').
card_uid('giant spider'/'5ED', '5ED:Giant Spider:giant spider').
card_rarity('giant spider'/'5ED', 'Common').
card_artist('giant spider'/'5ED', 'Brian Snõddy').
card_flavor_text('giant spider'/'5ED', 'While it possesses potent venom, the giant spider often chooses not to paralyze its victims. Perhaps the creature enjoys the gentle rocking motion caused by its captives\' struggles to escape its web.').
card_multiverse_id('giant spider'/'5ED', '3983').

card_in_set('giant strength', '5ED').
card_original_type('giant strength'/'5ED', 'Enchant Creature').
card_original_text('giant strength'/'5ED', 'Enchanted creature gets +2/+2.').
card_image_name('giant strength'/'5ED', 'giant strength').
card_uid('giant strength'/'5ED', '5ED:Giant Strength:giant strength').
card_rarity('giant strength'/'5ED', 'Common').
card_artist('giant strength'/'5ED', 'Kev Walker').
card_flavor_text('giant strength'/'5ED', '\"O! it is excellent / To have a giant\'s strength, but it is tyrannous / To use it like a giant.\"\n—William Shakespeare,\nMeasure for Measure').
card_multiverse_id('giant strength'/'5ED', '4054').

card_in_set('glacial wall', '5ED').
card_original_type('glacial wall'/'5ED', 'Summon — Wall').
card_original_text('glacial wall'/'5ED', '').
card_image_name('glacial wall'/'5ED', 'glacial wall').
card_uid('glacial wall'/'5ED', '5ED:Glacial Wall:glacial wall').
card_rarity('glacial wall'/'5ED', 'Uncommon').
card_artist('glacial wall'/'5ED', 'Greg Simanson').
card_flavor_text('glacial wall'/'5ED', '\"As we neared, Belfar proclaimed the wall to be made of glass and went forward to prove it. We had no choice but to pull his hands free from where he had touched it, leaving flesh palmprints on the icy wall.\"\n—Tross, Vainus Expedition leader').
card_multiverse_id('glacial wall'/'5ED', '3912').

card_in_set('glasses of urza', '5ED').
card_original_type('glasses of urza'/'5ED', 'Artifact').
card_original_text('glasses of urza'/'5ED', '{T}: Look at target player\'s hand.').
card_image_name('glasses of urza'/'5ED', 'glasses of urza').
card_uid('glasses of urza'/'5ED', '5ED:Glasses of Urza:glasses of urza').
card_rarity('glasses of urza'/'5ED', 'Uncommon').
card_artist('glasses of urza'/'5ED', 'Douglas Shuler').
card_multiverse_id('glasses of urza'/'5ED', '3786').

card_in_set('gloom', '5ED').
card_original_type('gloom'/'5ED', 'Enchantment').
card_original_text('gloom'/'5ED', 'White spells cost an additional {3} to play. Activated abilities of white enchantments cost an additional {3} to play.').
card_image_name('gloom'/'5ED', 'gloom').
card_uid('gloom'/'5ED', '5ED:Gloom:gloom').
card_rarity('gloom'/'5ED', 'Uncommon').
card_artist('gloom'/'5ED', 'Douglas Shuler').
card_multiverse_id('gloom'/'5ED', '3848').

card_in_set('goblin digging team', '5ED').
card_original_type('goblin digging team'/'5ED', 'Summon — Goblins').
card_original_text('goblin digging team'/'5ED', '{T}, Sacrifice Goblin Digging Team: Destroy target Wall.').
card_image_name('goblin digging team'/'5ED', 'goblin digging team').
card_uid('goblin digging team'/'5ED', '5ED:Goblin Digging Team:goblin digging team').
card_rarity('goblin digging team'/'5ED', 'Common').
card_artist('goblin digging team'/'5ED', 'Phil Foglio').
card_flavor_text('goblin digging team'/'5ED', '\"From down here we can make the whole wall collapse!\"\n\"Uh, yeah, boss, but how do we get out?\"').
card_multiverse_id('goblin digging team'/'5ED', '4055').

card_in_set('goblin hero', '5ED').
card_original_type('goblin hero'/'5ED', 'Summon — Goblin').
card_original_text('goblin hero'/'5ED', '').
card_image_name('goblin hero'/'5ED', 'goblin hero').
card_uid('goblin hero'/'5ED', '5ED:Goblin Hero:goblin hero').
card_rarity('goblin hero'/'5ED', 'Common').
card_artist('goblin hero'/'5ED', 'Pete Venters').
card_flavor_text('goblin hero'/'5ED', '\"When you\'re a goblin, you don\'t have to step forward to be a hero—everyone else just has to step back!\"\n—Biggum Flodrot, goblin veteran').
card_multiverse_id('goblin hero'/'5ED', '4056').

card_in_set('goblin king', '5ED').
card_original_type('goblin king'/'5ED', 'Summon — Lord').
card_original_text('goblin king'/'5ED', 'All Goblins get +1/+1 and gain mountainwalk. (If defending player controls any mountains, these creatures are unblockable.)').
card_image_name('goblin king'/'5ED', 'goblin king').
card_uid('goblin king'/'5ED', '5ED:Goblin King:goblin king').
card_rarity('goblin king'/'5ED', 'Rare').
card_artist('goblin king'/'5ED', 'Phil Foglio').
card_flavor_text('goblin king'/'5ED', 'To be king, Numsgil did in Blog, who did in Unkful, who did in Viddle, who did in Loll, who did in Alrok . . . .').
card_multiverse_id('goblin king'/'5ED', '4057').

card_in_set('goblin war drums', '5ED').
card_original_type('goblin war drums'/'5ED', 'Enchantment').
card_original_text('goblin war drums'/'5ED', 'Each creature you control cannot be blocked by only one creature.').
card_image_name('goblin war drums'/'5ED', 'goblin war drums').
card_uid('goblin war drums'/'5ED', '5ED:Goblin War Drums:goblin war drums').
card_rarity('goblin war drums'/'5ED', 'Common').
card_artist('goblin war drums'/'5ED', 'Dan Frazier').
card_flavor_text('goblin war drums'/'5ED', '\"Goblins charge with a deafening war cry. The cry doesn\'t mean anything—it just drowns out the drums!\"\n—Reod Dai, mercenary').
card_multiverse_id('goblin war drums'/'5ED', '4058').

card_in_set('goblin warrens', '5ED').
card_original_type('goblin warrens'/'5ED', 'Enchantment').
card_original_text('goblin warrens'/'5ED', '{2}{R}, Sacrifice two Goblins: Put three Goblin tokens into play. Treat these tokens as 1/1 red creatures.').
card_image_name('goblin warrens'/'5ED', 'goblin warrens').
card_uid('goblin warrens'/'5ED', '5ED:Goblin Warrens:goblin warrens').
card_rarity('goblin warrens'/'5ED', 'Rare').
card_artist('goblin warrens'/'5ED', 'Dan Frazier').
card_flavor_text('goblin warrens'/'5ED', '\"Goblins bred underground, their numbers hidden from the enemy until it was too late.\"\n—Sarpadian Empires, vol. IV').
card_multiverse_id('goblin warrens'/'5ED', '4059').

card_in_set('grapeshot catapult', '5ED').
card_original_type('grapeshot catapult'/'5ED', 'Artifact Creature').
card_original_text('grapeshot catapult'/'5ED', '{T}: Grapeshot Catapult deals 1 damage to target creature with flying.').
card_image_name('grapeshot catapult'/'5ED', 'grapeshot catapult').
card_uid('grapeshot catapult'/'5ED', '5ED:Grapeshot Catapult:grapeshot catapult').
card_rarity('grapeshot catapult'/'5ED', 'Common').
card_artist('grapeshot catapult'/'5ED', 'Dan Frazier').
card_flavor_text('grapeshot catapult'/'5ED', 'Recent research suggests these creatures were invented by Urza\'s and Mishra\'s original master, Tocasia, and that both brothers used them.').
card_multiverse_id('grapeshot catapult'/'5ED', '3787').

card_in_set('greater realm of preservation', '5ED').
card_original_type('greater realm of preservation'/'5ED', 'Enchantment').
card_original_text('greater realm of preservation'/'5ED', '{1}{W} Prevent all damage to you from a black or red source. Treat further damage from that source normally.').
card_image_name('greater realm of preservation'/'5ED', 'greater realm of preservation').
card_uid('greater realm of preservation'/'5ED', '5ED:Greater Realm of Preservation:greater realm of preservation').
card_rarity('greater realm of preservation'/'5ED', 'Uncommon').
card_artist('greater realm of preservation'/'5ED', 'Steve Luke').
card_multiverse_id('greater realm of preservation'/'5ED', '4128').

card_in_set('greater werewolf', '5ED').
card_original_type('greater werewolf'/'5ED', 'Summon — Lycanthrope').
card_original_text('greater werewolf'/'5ED', 'At end of combat, put a -0/-2 counter on each creature blocking or blocked by Greater Werewolf.').
card_image_name('greater werewolf'/'5ED', 'greater werewolf').
card_uid('greater werewolf'/'5ED', '5ED:Greater Werewolf:greater werewolf').
card_rarity('greater werewolf'/'5ED', 'Uncommon').
card_artist('greater werewolf'/'5ED', 'Dennis Detwiller').
card_flavor_text('greater werewolf'/'5ED', '\"Two legs by day, four legs by night, eyes raging in storm-gotten might.\"\n—Ancient Northland riddle').
card_multiverse_id('greater werewolf'/'5ED', '3849').

card_in_set('grizzly bears', '5ED').
card_original_type('grizzly bears'/'5ED', 'Summon — Bears').
card_original_text('grizzly bears'/'5ED', '').
card_image_name('grizzly bears'/'5ED', 'grizzly bears').
card_uid('grizzly bears'/'5ED', '5ED:Grizzly Bears:grizzly bears').
card_rarity('grizzly bears'/'5ED', 'Common').
card_artist('grizzly bears'/'5ED', 'Una Fricker').
card_flavor_text('grizzly bears'/'5ED', 'Don\'t try to outrun one of Dominaria\'s grizzlies; it\'ll catch you, knock you down, and eat you. Of course, you could run up a tree. In that case you\'ll get a nice view before it knocks the tree down and eats you.').
card_multiverse_id('grizzly bears'/'5ED', '3984').

card_in_set('havenwood battleground', '5ED').
card_original_type('havenwood battleground'/'5ED', 'Land').
card_original_text('havenwood battleground'/'5ED', 'Havenwood Battleground comes into play tapped.\n{T}: Add {G} to your mana pool.\n{T}, Sacrifice Havenwood Battleground: Add {G}{G} to your mana pool.').
card_image_name('havenwood battleground'/'5ED', 'havenwood battleground').
card_uid('havenwood battleground'/'5ED', '5ED:Havenwood Battleground:havenwood battleground').
card_rarity('havenwood battleground'/'5ED', 'Uncommon').
card_artist('havenwood battleground'/'5ED', 'Liz Danforth').
card_multiverse_id('havenwood battleground'/'5ED', '4182').

card_in_set('heal', '5ED').
card_original_type('heal'/'5ED', 'Instant').
card_original_text('heal'/'5ED', 'Prevent 1 damage to any creature or player.\nDraw a card at the beginning of the next turn.').
card_image_name('heal'/'5ED', 'heal').
card_uid('heal'/'5ED', '5ED:Heal:heal').
card_rarity('heal'/'5ED', 'Common').
card_artist('heal'/'5ED', 'Mark Tedin').
card_flavor_text('heal'/'5ED', '\"Sometimes even the smallest boon can save a life.\"\n—Halvor Arenson, Kjeldoran priest').
card_multiverse_id('heal'/'5ED', '4129').

card_in_set('healing salve', '5ED').
card_original_type('healing salve'/'5ED', 'Instant').
card_original_text('healing salve'/'5ED', 'Target player gains 3 life, or prevent up to 3 damage to any creature or player.').
card_image_name('healing salve'/'5ED', 'healing salve').
card_uid('healing salve'/'5ED', '5ED:Healing Salve:healing salve').
card_rarity('healing salve'/'5ED', 'Common').
card_artist('healing salve'/'5ED', 'Zina Saunders').
card_multiverse_id('healing salve'/'5ED', '4130').

card_in_set('hecatomb', '5ED').
card_original_type('hecatomb'/'5ED', 'Enchantment').
card_original_text('hecatomb'/'5ED', 'When Hecatomb comes into play, sacrifice four creatures or bury Hecatomb.\nTap a swamp you control: Hecatomb deals 1 damage to target creature or player.').
card_image_name('hecatomb'/'5ED', 'hecatomb').
card_uid('hecatomb'/'5ED', '5ED:Hecatomb:hecatomb').
card_rarity('hecatomb'/'5ED', 'Rare').
card_artist('hecatomb'/'5ED', 'George Pratt').
card_multiverse_id('hecatomb'/'5ED', '3850').

card_in_set('helm of chatzuk', '5ED').
card_original_type('helm of chatzuk'/'5ED', 'Artifact').
card_original_text('helm of chatzuk'/'5ED', '{1}, {T}: Target creature gains banding until end of turn.').
card_image_name('helm of chatzuk'/'5ED', 'helm of chatzuk').
card_uid('helm of chatzuk'/'5ED', '5ED:Helm of Chatzuk:helm of chatzuk').
card_rarity('helm of chatzuk'/'5ED', 'Rare').
card_artist('helm of chatzuk'/'5ED', 'Mark Tedin').
card_flavor_text('helm of chatzuk'/'5ED', 'Whether inspired by fear or by honor, all loyalty lends might.').
card_multiverse_id('helm of chatzuk'/'5ED', '3788').

card_in_set('hill giant', '5ED').
card_original_type('hill giant'/'5ED', 'Summon — Giant').
card_original_text('hill giant'/'5ED', '').
card_image_name('hill giant'/'5ED', 'hill giant').
card_uid('hill giant'/'5ED', '5ED:Hill Giant:hill giant').
card_rarity('hill giant'/'5ED', 'Common').
card_artist('hill giant'/'5ED', 'Charles Gillespie').
card_flavor_text('hill giant'/'5ED', 'Fortunately, hill giants have large blind spots in which a human can easily hide. Unfortunately, these blind spots are beneath the bottoms of their feet.').
card_multiverse_id('hill giant'/'5ED', '4060').

card_in_set('hipparion', '5ED').
card_original_type('hipparion'/'5ED', 'Summon — Hipparion').
card_original_text('hipparion'/'5ED', 'Hipparion cannot be assigned to block any creature with power 3 or greater unless you pay an additional {1}.').
card_image_name('hipparion'/'5ED', 'hipparion').
card_uid('hipparion'/'5ED', '5ED:Hipparion:hipparion').
card_rarity('hipparion'/'5ED', 'Common').
card_artist('hipparion'/'5ED', 'Margaret Organ-Kean').
card_flavor_text('hipparion'/'5ED', '\"Someone once said that hipparions are to warriors what aesthir are to skyknights. Don\'t believe it.\"\n—General Jarkeld, the Arctic Fox').
card_multiverse_id('hipparion'/'5ED', '4131').

card_in_set('hollow trees', '5ED').
card_original_type('hollow trees'/'5ED', 'Land').
card_original_text('hollow trees'/'5ED', 'Hollow Trees comes into play tapped.\nYou may choose not to untap Hollow Trees during your untap phase and put a storage counter on it instead.\n{T}, Remove X storage counters from Hollow Trees: Add an amount of {G} equal to X to your mana pool.').
card_image_name('hollow trees'/'5ED', 'hollow trees').
card_uid('hollow trees'/'5ED', '5ED:Hollow Trees:hollow trees').
card_rarity('hollow trees'/'5ED', 'Rare').
card_artist('hollow trees'/'5ED', 'David Seeley').
card_multiverse_id('hollow trees'/'5ED', '4183').

card_in_set('holy strength', '5ED').
card_original_type('holy strength'/'5ED', 'Enchant Creature').
card_original_text('holy strength'/'5ED', 'Enchanted creature gets +1/+2.').
card_image_name('holy strength'/'5ED', 'holy strength').
card_uid('holy strength'/'5ED', '5ED:Holy Strength:holy strength').
card_rarity('holy strength'/'5ED', 'Common').
card_artist('holy strength'/'5ED', 'Anson Maddocks').
card_multiverse_id('holy strength'/'5ED', '4132').

card_in_set('homarid warrior', '5ED').
card_original_type('homarid warrior'/'5ED', 'Summon — Homarid').
card_original_text('homarid warrior'/'5ED', '{U} Homarid Warrior cannot be the target of spells or effects until end of turn and does not untap during your next untap phase. Tap Homarid Warrior.').
card_image_name('homarid warrior'/'5ED', 'homarid warrior').
card_uid('homarid warrior'/'5ED', '5ED:Homarid Warrior:homarid warrior').
card_rarity('homarid warrior'/'5ED', 'Common').
card_artist('homarid warrior'/'5ED', 'Pete Venters').
card_multiverse_id('homarid warrior'/'5ED', '3913').

card_in_set('howl from beyond', '5ED').
card_original_type('howl from beyond'/'5ED', 'Instant').
card_original_text('howl from beyond'/'5ED', 'Target creature gets +X/+0 until end of turn.').
card_image_name('howl from beyond'/'5ED', 'howl from beyond').
card_uid('howl from beyond'/'5ED', '5ED:Howl from Beyond:howl from beyond').
card_rarity('howl from beyond'/'5ED', 'Common').
card_artist('howl from beyond'/'5ED', 'John Coulthart').
card_multiverse_id('howl from beyond'/'5ED', '3851').

card_in_set('howling mine', '5ED').
card_original_type('howling mine'/'5ED', 'Artifact').
card_original_text('howling mine'/'5ED', 'During each player\'s draw phase, that player draws an additional card.').
card_image_name('howling mine'/'5ED', 'howling mine').
card_uid('howling mine'/'5ED', '5ED:Howling Mine:howling mine').
card_rarity('howling mine'/'5ED', 'Rare').
card_artist('howling mine'/'5ED', 'Mark Poole').
card_multiverse_id('howling mine'/'5ED', '3789').

card_in_set('hungry mist', '5ED').
card_original_type('hungry mist'/'5ED', 'Summon — Mist').
card_original_text('hungry mist'/'5ED', 'During your upkeep, pay {G}{G} or bury Hungry Mist.').
card_image_name('hungry mist'/'5ED', 'hungry mist').
card_uid('hungry mist'/'5ED', '5ED:Hungry Mist:hungry mist').
card_rarity('hungry mist'/'5ED', 'Common').
card_artist('hungry mist'/'5ED', 'Heather Hudson').
card_flavor_text('hungry mist'/'5ED', '\"All things must eat, after all. Even the air can hunger.\"\n—Gemma, Willow priestess').
card_multiverse_id('hungry mist'/'5ED', '3985').

card_in_set('hurkyl\'s recall', '5ED').
card_original_type('hurkyl\'s recall'/'5ED', 'Instant').
card_original_text('hurkyl\'s recall'/'5ED', 'Return to target player\'s hand all artifacts in play he or she owns.').
card_image_name('hurkyl\'s recall'/'5ED', 'hurkyl\'s recall').
card_uid('hurkyl\'s recall'/'5ED', '5ED:Hurkyl\'s Recall:hurkyl\'s recall').
card_rarity('hurkyl\'s recall'/'5ED', 'Rare').
card_artist('hurkyl\'s recall'/'5ED', 'NéNé Thomas').
card_flavor_text('hurkyl\'s recall'/'5ED', 'This spell, like many attributed to Drafna, was actually the work of his wife and former student, Hurkyl.').
card_multiverse_id('hurkyl\'s recall'/'5ED', '3914').

card_in_set('hurloon minotaur', '5ED').
card_original_type('hurloon minotaur'/'5ED', 'Summon — Minotaur').
card_original_text('hurloon minotaur'/'5ED', '').
card_image_name('hurloon minotaur'/'5ED', 'hurloon minotaur').
card_uid('hurloon minotaur'/'5ED', '5ED:Hurloon Minotaur:hurloon minotaur').
card_rarity('hurloon minotaur'/'5ED', 'Common').
card_artist('hurloon minotaur'/'5ED', 'Anson Maddocks').
card_flavor_text('hurloon minotaur'/'5ED', 'The minotaurs of the Hurloon Mountains are known for their love of battle. They are also known for their hymns to the dead, sung for friend and foe alike. These hymns can last for days, filling the mountain valleys with their low, haunting sounds.').
card_multiverse_id('hurloon minotaur'/'5ED', '4061').

card_in_set('hurricane', '5ED').
card_original_type('hurricane'/'5ED', 'Sorcery').
card_original_text('hurricane'/'5ED', 'Hurricane deals X damage to each creature with flying and each player.').
card_image_name('hurricane'/'5ED', 'hurricane').
card_uid('hurricane'/'5ED', '5ED:Hurricane:hurricane').
card_rarity('hurricane'/'5ED', 'Uncommon').
card_artist('hurricane'/'5ED', 'Cornelius Brudi').
card_flavor_text('hurricane'/'5ED', '\"The raging winds . . . , settling on the sea, the surges sweep, / Raise liquid mountains, and disclose the deep.\"\n—Virgil, Aeneid, Book I, trans. Dryden').
card_multiverse_id('hurricane'/'5ED', '3986').

card_in_set('hydroblast', '5ED').
card_original_type('hydroblast'/'5ED', 'Interrupt').
card_original_text('hydroblast'/'5ED', 'Counter target spell if it is red, or destroy target permanent if it is red. (If this spell targets a permanent, play it as an instant.)').
card_image_name('hydroblast'/'5ED', 'hydroblast').
card_uid('hydroblast'/'5ED', '5ED:Hydroblast:hydroblast').
card_rarity('hydroblast'/'5ED', 'Uncommon').
card_artist('hydroblast'/'5ED', 'Kaja Foglio').
card_flavor_text('hydroblast'/'5ED', '\"Heed the lessons of our time: the forms of water may move the land itself and hold captive the fires within.\"\n—Gustha Ebbasdotter,\nKjeldoran royal mage').
card_multiverse_id('hydroblast'/'5ED', '3915').

card_in_set('icatian phalanx', '5ED').
card_original_type('icatian phalanx'/'5ED', 'Summon — Soldiers').
card_original_text('icatian phalanx'/'5ED', 'Banding').
card_image_name('icatian phalanx'/'5ED', 'icatian phalanx').
card_uid('icatian phalanx'/'5ED', '5ED:Icatian Phalanx:icatian phalanx').
card_rarity('icatian phalanx'/'5ED', 'Uncommon').
card_artist('icatian phalanx'/'5ED', 'Kaja Foglio').
card_flavor_text('icatian phalanx'/'5ED', 'Even after the wall was breached in half a dozen places, the phalanxes fought on, standing solidly against the onrushing raiders. Disciplined and dedicated, they held their ranks to the end, even in the face of tremendous losses.').
card_multiverse_id('icatian phalanx'/'5ED', '4133').

card_in_set('icatian scout', '5ED').
card_original_type('icatian scout'/'5ED', 'Summon — Soldier').
card_original_text('icatian scout'/'5ED', '{1}, {T}: Target creature gains first strike until end of turn.').
card_image_name('icatian scout'/'5ED', 'icatian scout').
card_uid('icatian scout'/'5ED', '5ED:Icatian Scout:icatian scout').
card_rarity('icatian scout'/'5ED', 'Common').
card_artist('icatian scout'/'5ED', 'Douglas Shuler').
card_flavor_text('icatian scout'/'5ED', '\"In these, our final days, I offer this. Though we could not save Icatia, we gave our children time to grow, and love, before it fell.\"\n—Ailis Connaut, diary').
card_multiverse_id('icatian scout'/'5ED', '4134').

card_in_set('icatian store', '5ED').
card_original_type('icatian store'/'5ED', 'Land').
card_original_text('icatian store'/'5ED', 'Icatian Store comes into play tapped.\nYou may choose not to untap Icatian Store during your untap phase and put a storage counter on it instead.\n{T}, Remove X storage counters from Icatian Store: Add an amount of {W} equal to X to your mana pool.').
card_image_name('icatian store'/'5ED', 'icatian store').
card_uid('icatian store'/'5ED', '5ED:Icatian Store:icatian store').
card_rarity('icatian store'/'5ED', 'Rare').
card_artist('icatian store'/'5ED', 'David Seeley').
card_multiverse_id('icatian store'/'5ED', '4184').

card_in_set('icatian town', '5ED').
card_original_type('icatian town'/'5ED', 'Sorcery').
card_original_text('icatian town'/'5ED', 'Put four Citizen tokens into play. Treat these tokens as 1/1 white creatures.').
card_image_name('icatian town'/'5ED', 'icatian town').
card_uid('icatian town'/'5ED', '5ED:Icatian Town:icatian town').
card_rarity('icatian town'/'5ED', 'Rare').
card_artist('icatian town'/'5ED', 'Tom Wänerstrand').
card_flavor_text('icatian town'/'5ED', 'Icatia\'s once-peaceful towns faced increasing attacks from orcs and goblins as the climate cooled. By the time the empire fell, they were little more than armed camps.').
card_multiverse_id('icatian town'/'5ED', '4135').

card_in_set('ice floe', '5ED').
card_original_type('ice floe'/'5ED', 'Land').
card_original_text('ice floe'/'5ED', 'You may choose not to untap Ice Floe during your untap phase.\n{T}: Tap target creature without flying that is attacking you. As long as Ice Floe remains tapped, that creature does not untap during its controller\'s untap phase.').
card_image_name('ice floe'/'5ED', 'ice floe').
card_uid('ice floe'/'5ED', '5ED:Ice Floe:ice floe').
card_rarity('ice floe'/'5ED', 'Uncommon').
card_artist('ice floe'/'5ED', 'John Avon').
card_multiverse_id('ice floe'/'5ED', '4185').

card_in_set('imposing visage', '5ED').
card_original_type('imposing visage'/'5ED', 'Enchant Creature').
card_original_text('imposing visage'/'5ED', 'Enchanted creature cannot be blocked by only one creature.').
card_image_name('imposing visage'/'5ED', 'imposing visage').
card_uid('imposing visage'/'5ED', '5ED:Imposing Visage:imposing visage').
card_rarity('imposing visage'/'5ED', 'Common').
card_artist('imposing visage'/'5ED', 'Brian Snõddy').
card_flavor_text('imposing visage'/'5ED', 'With the horrible mask, Lagfrak the Low terrorized the countryside. But then he met Il-Chunt, the mask\'s model . . . .').
card_multiverse_id('imposing visage'/'5ED', '4062').

card_in_set('incinerate', '5ED').
card_original_type('incinerate'/'5ED', 'Instant').
card_original_text('incinerate'/'5ED', 'Incinerate deals 3 damage to target creature or player. Creatures damaged by Incinerate cannot regenerate this turn.').
card_image_name('incinerate'/'5ED', 'incinerate').
card_uid('incinerate'/'5ED', '5ED:Incinerate:incinerate').
card_rarity('incinerate'/'5ED', 'Common').
card_artist('incinerate'/'5ED', 'Scott M. Fischer').
card_flavor_text('incinerate'/'5ED', '\"Yes, I think ‘toast\' is an appropriate description.\"\n—Jaya Ballard, task mage').
card_multiverse_id('incinerate'/'5ED', '4063').

card_in_set('inferno', '5ED').
card_original_type('inferno'/'5ED', 'Instant').
card_original_text('inferno'/'5ED', 'Inferno deals 6 damage to each creature and player.').
card_image_name('inferno'/'5ED', 'inferno').
card_uid('inferno'/'5ED', '5ED:Inferno:inferno').
card_rarity('inferno'/'5ED', 'Rare').
card_artist('inferno'/'5ED', 'Mike Kerr').
card_flavor_text('inferno'/'5ED', '\"Some have said there is no subtlety to destruction. You know what? They\'re dead.\"\n—Jaya Ballard, task mage').
card_multiverse_id('inferno'/'5ED', '4064').

card_in_set('infinite hourglass', '5ED').
card_original_type('infinite hourglass'/'5ED', 'Artifact').
card_original_text('infinite hourglass'/'5ED', 'During your upkeep, put a time counter on Infinite Hourglass.\nAll creatures get +X/+0, where X is equal to the number of time counters on Infinite Hourglass.\nAny player may pay {3} during any upkeep to remove a time counter from Infinite Hourglass.').
card_image_name('infinite hourglass'/'5ED', 'infinite hourglass').
card_uid('infinite hourglass'/'5ED', '5ED:Infinite Hourglass:infinite hourglass').
card_rarity('infinite hourglass'/'5ED', 'Rare').
card_artist('infinite hourglass'/'5ED', 'Adam Rex').
card_multiverse_id('infinite hourglass'/'5ED', '3790').

card_in_set('initiates of the ebon hand', '5ED').
card_original_type('initiates of the ebon hand'/'5ED', 'Summon — Clerics').
card_original_text('initiates of the ebon hand'/'5ED', '{1}: Add {B} to your mana pool. If {4} or more is spent in this way during one turn, bury Initiates of the Ebon Hand at end of turn. Play this ability as a mana source.').
card_image_name('initiates of the ebon hand'/'5ED', 'initiates of the ebon hand').
card_uid('initiates of the ebon hand'/'5ED', '5ED:Initiates of the Ebon Hand:initiates of the ebon hand').
card_rarity('initiates of the ebon hand'/'5ED', 'Common').
card_artist('initiates of the ebon hand'/'5ED', 'Heather Hudson').
card_flavor_text('initiates of the ebon hand'/'5ED', '\"We are no longer Nature\'s children, but her masters . . . .\"\n—Oath of the Ebon Hand').
card_multiverse_id('initiates of the ebon hand'/'5ED', '3852').

card_in_set('instill energy', '5ED').
card_original_type('instill energy'/'5ED', 'Enchant Creature').
card_original_text('instill energy'/'5ED', 'Enchanted creature is unaffected by summoning sickness.\n{0}: Untap enchanted creature. Use this ability only during your turn and only once each turn.').
card_image_name('instill energy'/'5ED', 'instill energy').
card_uid('instill energy'/'5ED', '5ED:Instill Energy:instill energy').
card_rarity('instill energy'/'5ED', 'Uncommon').
card_artist('instill energy'/'5ED', 'Ron Spencer').
card_multiverse_id('instill energy'/'5ED', '3987').

card_in_set('iron star', '5ED').
card_original_type('iron star'/'5ED', 'Artifact').
card_original_text('iron star'/'5ED', '{1}: Gain 1 life. Use this ability only when a red spell is successfully cast and only once for each such spell.').
card_image_name('iron star'/'5ED', 'iron star').
card_uid('iron star'/'5ED', '5ED:Iron Star:iron star').
card_rarity('iron star'/'5ED', 'Uncommon').
card_artist('iron star'/'5ED', 'Donato Giancola').
card_multiverse_id('iron star'/'5ED', '3791').

card_in_set('ironclaw curse', '5ED').
card_original_type('ironclaw curse'/'5ED', 'Enchant Creature').
card_original_text('ironclaw curse'/'5ED', 'Enchanted creature gets -0/-1 and cannot be assigned to block any creature with power greater than or equal to enchanted creature\'s toughness.').
card_image_name('ironclaw curse'/'5ED', 'ironclaw curse').
card_uid('ironclaw curse'/'5ED', '5ED:Ironclaw Curse:ironclaw curse').
card_rarity('ironclaw curse'/'5ED', 'Rare').
card_artist('ironclaw curse'/'5ED', 'Dennis Detwiller').
card_multiverse_id('ironclaw curse'/'5ED', '4065').

card_in_set('ironclaw orcs', '5ED').
card_original_type('ironclaw orcs'/'5ED', 'Summon — Orcs').
card_original_text('ironclaw orcs'/'5ED', 'Ironclaw Orcs cannot be assigned to block any creature with power 2 or greater.').
card_image_name('ironclaw orcs'/'5ED', 'ironclaw orcs').
card_uid('ironclaw orcs'/'5ED', '5ED:Ironclaw Orcs:ironclaw orcs').
card_rarity('ironclaw orcs'/'5ED', 'Common').
card_artist('ironclaw orcs'/'5ED', 'Anson Maddocks').
card_flavor_text('ironclaw orcs'/'5ED', 'Generations of genetic weeding have given rise to the deviously cowardly Ironclaw clan.').
card_multiverse_id('ironclaw orcs'/'5ED', '4066').

card_in_set('ironroot treefolk', '5ED').
card_original_type('ironroot treefolk'/'5ED', 'Summon — Treefolk').
card_original_text('ironroot treefolk'/'5ED', '').
card_image_name('ironroot treefolk'/'5ED', 'ironroot treefolk').
card_uid('ironroot treefolk'/'5ED', '5ED:Ironroot Treefolk:ironroot treefolk').
card_rarity('ironroot treefolk'/'5ED', 'Common').
card_artist('ironroot treefolk'/'5ED', 'Jerry Tiritilli').
card_flavor_text('ironroot treefolk'/'5ED', '\"‘I\'ll show you how to handle the treefolk!\' the giant bragged, and he strode off into the forest. Two days later he returned, his face masked in sap and a nest behind his ear. None dared ask who won.\"\n—Azeworai, \"The Treeling\"').
card_multiverse_id('ironroot treefolk'/'5ED', '3988').

card_in_set('island', '5ED').
card_original_type('island'/'5ED', 'Land').
card_original_text('island'/'5ED', '{T}: Add {U} to your mana pool.').
card_image_name('island'/'5ED', 'island1').
card_uid('island'/'5ED', '5ED:Island:island1').
card_rarity('island'/'5ED', 'Basic Land').
card_artist('island'/'5ED', 'J. W. Frost').
card_multiverse_id('island'/'5ED', '4199').

card_in_set('island', '5ED').
card_original_type('island'/'5ED', 'Land').
card_original_text('island'/'5ED', '{T}: Add {U} to your mana pool.').
card_image_name('island'/'5ED', 'island2').
card_uid('island'/'5ED', '5ED:Island:island2').
card_rarity('island'/'5ED', 'Basic Land').
card_artist('island'/'5ED', 'J. W. Frost').
card_multiverse_id('island'/'5ED', '4200').

card_in_set('island', '5ED').
card_original_type('island'/'5ED', 'Land').
card_original_text('island'/'5ED', '{T}: Add {U} to your mana pool.').
card_image_name('island'/'5ED', 'island3').
card_uid('island'/'5ED', '5ED:Island:island3').
card_rarity('island'/'5ED', 'Basic Land').
card_artist('island'/'5ED', 'J. W. Frost').
card_multiverse_id('island'/'5ED', '4201').

card_in_set('island', '5ED').
card_original_type('island'/'5ED', 'Land').
card_original_text('island'/'5ED', '{T}: Add {U} to your mana pool.').
card_image_name('island'/'5ED', 'island4').
card_uid('island'/'5ED', '5ED:Island:island4').
card_rarity('island'/'5ED', 'Basic Land').
card_artist('island'/'5ED', 'J. W. Frost').
card_multiverse_id('island'/'5ED', '4202').

card_in_set('island sanctuary', '5ED').
card_original_type('island sanctuary'/'5ED', 'Enchantment').
card_original_text('island sanctuary'/'5ED', 'Skip drawing a card: Until the beginning of your next turn, only creatures with flying or islandwalk can attack you. Use this ability only during your draw phase and only once each turn.').
card_image_name('island sanctuary'/'5ED', 'island sanctuary').
card_uid('island sanctuary'/'5ED', '5ED:Island Sanctuary:island sanctuary').
card_rarity('island sanctuary'/'5ED', 'Rare').
card_artist('island sanctuary'/'5ED', 'Mark Poole').
card_multiverse_id('island sanctuary'/'5ED', '4136').

card_in_set('ivory cup', '5ED').
card_original_type('ivory cup'/'5ED', 'Artifact').
card_original_text('ivory cup'/'5ED', '{1}: Gain 1 life. Use this ability only when a white spell is successfully cast and only once for each such spell.').
card_image_name('ivory cup'/'5ED', 'ivory cup').
card_uid('ivory cup'/'5ED', '5ED:Ivory Cup:ivory cup').
card_rarity('ivory cup'/'5ED', 'Uncommon').
card_artist('ivory cup'/'5ED', 'Donato Giancola').
card_multiverse_id('ivory cup'/'5ED', '3792').

card_in_set('ivory guardians', '5ED').
card_original_type('ivory guardians'/'5ED', 'Summon — Guardians').
card_original_text('ivory guardians'/'5ED', 'Protection from red\nAs long as any opponent controls any red cards in play, all Guardians get +1/+1.').
card_image_name('ivory guardians'/'5ED', 'ivory guardians').
card_uid('ivory guardians'/'5ED', '5ED:Ivory Guardians:ivory guardians').
card_rarity('ivory guardians'/'5ED', 'Uncommon').
card_artist('ivory guardians'/'5ED', 'Adam Rex').
card_flavor_text('ivory guardians'/'5ED', '\"But who is to guard the guards themselves?\"\n—Juvenal, Satires').
card_multiverse_id('ivory guardians'/'5ED', '4137').

card_in_set('jade monolith', '5ED').
card_original_type('jade monolith'/'5ED', 'Artifact').
card_original_text('jade monolith'/'5ED', '{1}: Redirect all damage from any creature to yourself.').
card_image_name('jade monolith'/'5ED', 'jade monolith').
card_uid('jade monolith'/'5ED', '5ED:Jade Monolith:jade monolith').
card_rarity('jade monolith'/'5ED', 'Rare').
card_artist('jade monolith'/'5ED', 'Richard Kane Ferguson').
card_multiverse_id('jade monolith'/'5ED', '3793').

card_in_set('jalum tome', '5ED').
card_original_type('jalum tome'/'5ED', 'Artifact').
card_original_text('jalum tome'/'5ED', '{2}, {T}: Draw a card, then choose and discard a card.').
card_image_name('jalum tome'/'5ED', 'jalum tome').
card_uid('jalum tome'/'5ED', '5ED:Jalum Tome:jalum tome').
card_rarity('jalum tome'/'5ED', 'Rare').
card_artist('jalum tome'/'5ED', 'Tom Wänerstrand').
card_flavor_text('jalum tome'/'5ED', 'This timeworn relic was responsible for many of Urza\'s victories, though he never fully comprehended its mystic runes.').
card_multiverse_id('jalum tome'/'5ED', '3794').

card_in_set('jandor\'s saddlebags', '5ED').
card_original_type('jandor\'s saddlebags'/'5ED', 'Artifact').
card_original_text('jandor\'s saddlebags'/'5ED', '{3}, {T}: Untap target creature.').
card_image_name('jandor\'s saddlebags'/'5ED', 'jandor\'s saddlebags').
card_uid('jandor\'s saddlebags'/'5ED', '5ED:Jandor\'s Saddlebags:jandor\'s saddlebags').
card_rarity('jandor\'s saddlebags'/'5ED', 'Rare').
card_artist('jandor\'s saddlebags'/'5ED', 'Roger Raupp').
card_flavor_text('jandor\'s saddlebags'/'5ED', 'Each day of their journey, Jandor opened the saddlebags and found them full of mutton, quinces, cheese, date rolls, wine, and all manner of delicious and satisfying foods.').
card_multiverse_id('jandor\'s saddlebags'/'5ED', '3795').

card_in_set('jayemdae tome', '5ED').
card_original_type('jayemdae tome'/'5ED', 'Artifact').
card_original_text('jayemdae tome'/'5ED', '{4}, {T}: Draw a card.').
card_image_name('jayemdae tome'/'5ED', 'jayemdae tome').
card_uid('jayemdae tome'/'5ED', '5ED:Jayemdae Tome:jayemdae tome').
card_rarity('jayemdae tome'/'5ED', 'Rare').
card_artist('jayemdae tome'/'5ED', 'Mark Tedin').
card_multiverse_id('jayemdae tome'/'5ED', '3796').

card_in_set('jester\'s cap', '5ED').
card_original_type('jester\'s cap'/'5ED', 'Artifact').
card_original_text('jester\'s cap'/'5ED', '{2}, {T}, Sacrifice Jester\'s Cap: Look through target player\'s library and remove any three of those cards from the game. Shuffle that library afterwards.').
card_image_name('jester\'s cap'/'5ED', 'jester\'s cap').
card_uid('jester\'s cap'/'5ED', '5ED:Jester\'s Cap:jester\'s cap').
card_rarity('jester\'s cap'/'5ED', 'Rare').
card_artist('jester\'s cap'/'5ED', 'Dan Frazier').
card_flavor_text('jester\'s cap'/'5ED', '\"Know your foes\' strengths as well as their weaknesses.\"\n—Arcum Dagsson, Soldevi machinist').
card_multiverse_id('jester\'s cap'/'5ED', '3797').

card_in_set('johtull wurm', '5ED').
card_original_type('johtull wurm'/'5ED', 'Summon — Wurm').
card_original_text('johtull wurm'/'5ED', 'For each creature assigned to block it beyond the first, Johtull Wurm gets -2/-1 until end of turn.').
card_image_name('johtull wurm'/'5ED', 'johtull wurm').
card_uid('johtull wurm'/'5ED', '5ED:Johtull Wurm:johtull wurm').
card_rarity('johtull wurm'/'5ED', 'Uncommon').
card_artist('johtull wurm'/'5ED', 'Ian Miller').
card_flavor_text('johtull wurm'/'5ED', '\"To bring her down we must be on all sides at once—leave one avenue open and we\'ll all be dead.\"\n—Taaveti of Kelsinko, elvish hunter').
card_multiverse_id('johtull wurm'/'5ED', '3989').

card_in_set('jokulhaups', '5ED').
card_original_type('jokulhaups'/'5ED', 'Sorcery').
card_original_text('jokulhaups'/'5ED', 'Bury all artifacts, creatures, and lands.').
card_image_name('jokulhaups'/'5ED', 'jokulhaups').
card_uid('jokulhaups'/'5ED', '5ED:Jokulhaups:jokulhaups').
card_rarity('jokulhaups'/'5ED', 'Rare').
card_artist('jokulhaups'/'5ED', 'Mike Kerr').
card_flavor_text('jokulhaups'/'5ED', '\"The raging waters had swept away trees, bridges, and even houses. My healers had much work to do.\"\n—Halvor Arenson, Kjeldoran priest').
card_multiverse_id('jokulhaups'/'5ED', '4067').

card_in_set('joven\'s tools', '5ED').
card_original_type('joven\'s tools'/'5ED', 'Artifact').
card_original_text('joven\'s tools'/'5ED', '{4}, {T}: Target creature cannot be blocked this turn except by Walls.').
card_image_name('joven\'s tools'/'5ED', 'joven\'s tools').
card_uid('joven\'s tools'/'5ED', '5ED:Joven\'s Tools:joven\'s tools').
card_rarity('joven\'s tools'/'5ED', 'Uncommon').
card_artist('joven\'s tools'/'5ED', 'Zina Saunders').
card_flavor_text('joven\'s tools'/'5ED', '\"If that thief Joven ever shows his head around here again, make sure he leaves without it.\"\n—Eron the Relentless').
card_multiverse_id('joven\'s tools'/'5ED', '3798').

card_in_set('justice', '5ED').
card_original_type('justice'/'5ED', 'Enchantment').
card_original_text('justice'/'5ED', 'During your upkeep, pay {W}{W} or bury Justice.\nWhenever any red creature or spell assigns damage, Justice deals an equal amount of damage to that creature\'s or spell\'s controller.').
card_image_name('justice'/'5ED', 'justice').
card_uid('justice'/'5ED', '5ED:Justice:justice').
card_rarity('justice'/'5ED', 'Uncommon').
card_artist('justice'/'5ED', 'Ruth Thompson').
card_multiverse_id('justice'/'5ED', '4138').

card_in_set('juxtapose', '5ED').
card_original_type('juxtapose'/'5ED', 'Sorcery').
card_original_text('juxtapose'/'5ED', 'Exchange with target player control of the creature with the highest total casting cost that you each control. If two or more creatures are tied for highest total casting cost creature a player controls, he or she chooses between them. Exchange control of artifacts in the same way.').
card_image_name('juxtapose'/'5ED', 'juxtapose').
card_uid('juxtapose'/'5ED', '5ED:Juxtapose:juxtapose').
card_rarity('juxtapose'/'5ED', 'Rare').
card_artist('juxtapose'/'5ED', 'Justin Hampton').
card_multiverse_id('juxtapose'/'5ED', '3916').

card_in_set('karma', '5ED').
card_original_type('karma'/'5ED', 'Enchantment').
card_original_text('karma'/'5ED', 'During each player\'s upkeep, Karma deals to that player an amount of damage equal to the number of swamps he or she controls.').
card_image_name('karma'/'5ED', 'karma').
card_uid('karma'/'5ED', '5ED:Karma:karma').
card_rarity('karma'/'5ED', 'Uncommon').
card_artist('karma'/'5ED', 'Bob Eggleton').
card_multiverse_id('karma'/'5ED', '4139').

card_in_set('karplusan forest', '5ED').
card_original_type('karplusan forest'/'5ED', 'Land').
card_original_text('karplusan forest'/'5ED', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {R} or {G} to your mana pool. Karplusan Forest deals 1 damage to you.').
card_image_name('karplusan forest'/'5ED', 'karplusan forest').
card_uid('karplusan forest'/'5ED', '5ED:Karplusan Forest:karplusan forest').
card_rarity('karplusan forest'/'5ED', 'Rare').
card_artist('karplusan forest'/'5ED', 'Randy Gallegos').
card_multiverse_id('karplusan forest'/'5ED', '4186').

card_in_set('keldon warlord', '5ED').
card_original_type('keldon warlord'/'5ED', 'Summon — Lord').
card_original_text('keldon warlord'/'5ED', 'Keldon Warlord has power and toughness each equal to the number of non-Wall creatures you control.').
card_image_name('keldon warlord'/'5ED', 'keldon warlord').
card_uid('keldon warlord'/'5ED', '5ED:Keldon Warlord:keldon warlord').
card_rarity('keldon warlord'/'5ED', 'Uncommon').
card_artist('keldon warlord'/'5ED', 'Kev Brockschmidt').
card_flavor_text('keldon warlord'/'5ED', 'What if they threw a war, and everybody came?').
card_multiverse_id('keldon warlord'/'5ED', '4068').

card_in_set('killer bees', '5ED').
card_original_type('killer bees'/'5ED', 'Summon — Bees').
card_original_text('killer bees'/'5ED', 'Flying\n{G} +1/+1 until end of turn').
card_image_name('killer bees'/'5ED', 'killer bees').
card_uid('killer bees'/'5ED', '5ED:Killer Bees:killer bees').
card_rarity('killer bees'/'5ED', 'Uncommon').
card_artist('killer bees'/'5ED', 'Phil Foglio').
card_flavor_text('killer bees'/'5ED', 'The communal mind produces a savage strategy, yet no one could predict that this vicious crossbreed would unravel the secret of steel.').
card_multiverse_id('killer bees'/'5ED', '3990').

card_in_set('kismet', '5ED').
card_original_type('kismet'/'5ED', 'Enchantment').
card_original_text('kismet'/'5ED', 'Artifacts, creatures, and lands target player controls come into play tapped.').
card_image_name('kismet'/'5ED', 'kismet').
card_uid('kismet'/'5ED', '5ED:Kismet:kismet').
card_rarity('kismet'/'5ED', 'Uncommon').
card_artist('kismet'/'5ED', 'Kaja Foglio').
card_flavor_text('kismet'/'5ED', '\"Make people wait for what they want, and you have power over them. This is as true for merchants and militia as it is for cooks and couples.\"\n—Gwendlyn Di Corci').
card_multiverse_id('kismet'/'5ED', '4140').

card_in_set('kjeldoran dead', '5ED').
card_original_type('kjeldoran dead'/'5ED', 'Summon — Dead').
card_original_text('kjeldoran dead'/'5ED', 'When Kjeldoran Dead comes into play, sacrifice a creature.\n{B} Regenerate').
card_image_name('kjeldoran dead'/'5ED', 'kjeldoran dead').
card_uid('kjeldoran dead'/'5ED', '5ED:Kjeldoran Dead:kjeldoran dead').
card_rarity('kjeldoran dead'/'5ED', 'Common').
card_artist('kjeldoran dead'/'5ED', 'Melissa A. Benson').
card_flavor_text('kjeldoran dead'/'5ED', '\"They shall drink sorrow\'s dregs. They shall kill those whom once they loved.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('kjeldoran dead'/'5ED', '3853').

card_in_set('kjeldoran royal guard', '5ED').
card_original_type('kjeldoran royal guard'/'5ED', 'Summon — Soldiers').
card_original_text('kjeldoran royal guard'/'5ED', '{T}: Redirect from you to Kjeldoran Royal Guard all combat damage dealt by unblocked creatures this turn.').
card_image_name('kjeldoran royal guard'/'5ED', 'kjeldoran royal guard').
card_uid('kjeldoran royal guard'/'5ED', '5ED:Kjeldoran Royal Guard:kjeldoran royal guard').
card_rarity('kjeldoran royal guard'/'5ED', 'Rare').
card_artist('kjeldoran royal guard'/'5ED', 'L. A. Williams').
card_flavor_text('kjeldoran royal guard'/'5ED', 'Honorable in battle, generous in death.\n—Motto of the Kjeldoran Royal Guard').
card_multiverse_id('kjeldoran royal guard'/'5ED', '4141').

card_in_set('kjeldoran skycaptain', '5ED').
card_original_type('kjeldoran skycaptain'/'5ED', 'Summon — Soldier').
card_original_text('kjeldoran skycaptain'/'5ED', 'Banding, flying, first strike').
card_image_name('kjeldoran skycaptain'/'5ED', 'kjeldoran skycaptain').
card_uid('kjeldoran skycaptain'/'5ED', '5ED:Kjeldoran Skycaptain:kjeldoran skycaptain').
card_rarity('kjeldoran skycaptain'/'5ED', 'Uncommon').
card_artist('kjeldoran skycaptain'/'5ED', 'Mark Poole').
card_flavor_text('kjeldoran skycaptain'/'5ED', 'The heartlink between soldier and steed is hatched with the aesthir struggling from its shell.').
card_multiverse_id('kjeldoran skycaptain'/'5ED', '4142').

card_in_set('knight of stromgald', '5ED').
card_original_type('knight of stromgald'/'5ED', 'Summon — Knight').
card_original_text('knight of stromgald'/'5ED', 'Protection from white\n{B}: First strike until end of turn\n{B}{B}: +1/+0 until end of turn').
card_image_name('knight of stromgald'/'5ED', 'knight of stromgald').
card_uid('knight of stromgald'/'5ED', '5ED:Knight of Stromgald:knight of stromgald').
card_rarity('knight of stromgald'/'5ED', 'Uncommon').
card_artist('knight of stromgald'/'5ED', 'Mark Poole').
card_flavor_text('knight of stromgald'/'5ED', '\"Kjeldorans should rule supreme, and to the rest, death!\"\n—Avram Garrisson,\nLeader of the Knights of Stromgald').
card_multiverse_id('knight of stromgald'/'5ED', '3854').

card_in_set('krovikan fetish', '5ED').
card_original_type('krovikan fetish'/'5ED', 'Enchant Creature').
card_original_text('krovikan fetish'/'5ED', 'Draw a card at the beginning of the turn after Krovikan Fetish comes into play.\nEnchanted creature gets +1/+1.').
card_image_name('krovikan fetish'/'5ED', 'krovikan fetish').
card_uid('krovikan fetish'/'5ED', '5ED:Krovikan Fetish:krovikan fetish').
card_rarity('krovikan fetish'/'5ED', 'Common').
card_artist('krovikan fetish'/'5ED', 'Heather Hudson').
card_flavor_text('krovikan fetish'/'5ED', 'Some Krovikans find strength in the ears and eyes of their victims and wear such fetishes into battle.').
card_multiverse_id('krovikan fetish'/'5ED', '3855').

card_in_set('krovikan sorcerer', '5ED').
card_original_type('krovikan sorcerer'/'5ED', 'Summon — Wizard').
card_original_text('krovikan sorcerer'/'5ED', '{T}, Choose and discard a nonblack card: Draw a card.\n{T}, Choose and discard a black card: Draw two cards, then choose and discard one of them.').
card_image_name('krovikan sorcerer'/'5ED', 'krovikan sorcerer').
card_uid('krovikan sorcerer'/'5ED', '5ED:Krovikan Sorcerer:krovikan sorcerer').
card_rarity('krovikan sorcerer'/'5ED', 'Common').
card_artist('krovikan sorcerer'/'5ED', 'Pat Morrissey').
card_multiverse_id('krovikan sorcerer'/'5ED', '3917').

card_in_set('labyrinth minotaur', '5ED').
card_original_type('labyrinth minotaur'/'5ED', 'Summon — Minotaur').
card_original_text('labyrinth minotaur'/'5ED', 'If Labyrinth Minotaur blocks any creature, that creature does not untap during its controller\'s next untap phase.').
card_image_name('labyrinth minotaur'/'5ED', 'labyrinth minotaur').
card_uid('labyrinth minotaur'/'5ED', '5ED:Labyrinth Minotaur:labyrinth minotaur').
card_rarity('labyrinth minotaur'/'5ED', 'Common').
card_artist('labyrinth minotaur'/'5ED', 'Anson Maddocks').
card_flavor_text('labyrinth minotaur'/'5ED', '\"I doubt any labyrinth minotaurs still live—but then we minotaurs are stubborn beings.\"\n—Onatah, Anaba shaman').
card_multiverse_id('labyrinth minotaur'/'5ED', '3918').

card_in_set('leshrac\'s rite', '5ED').
card_original_type('leshrac\'s rite'/'5ED', 'Enchant Creature').
card_original_text('leshrac\'s rite'/'5ED', 'Enchanted creature gains swampwalk. (If defending player controls any swamps, this creature is unblockable.)').
card_image_name('leshrac\'s rite'/'5ED', 'leshrac\'s rite').
card_uid('leshrac\'s rite'/'5ED', '5ED:Leshrac\'s Rite:leshrac\'s rite').
card_rarity('leshrac\'s rite'/'5ED', 'Uncommon').
card_artist('leshrac\'s rite'/'5ED', 'Mike Raabe').
card_flavor_text('leshrac\'s rite'/'5ED', '\"Bind me to thee, my soul to thine. I am your servant and your slave. Blood for blood, flesh for flesh, Leshrac, my lord.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('leshrac\'s rite'/'5ED', '3856').

card_in_set('leviathan', '5ED').
card_original_type('leviathan'/'5ED', 'Summon — Leviathan').
card_original_text('leviathan'/'5ED', 'Trample\nLeviathan comes into play tapped and does not untap during your untap phase.\nLeviathan cannot attack this turn unless you sacrifice two islands.\nSacrifice two islands: Untap Leviathan. Use this ability only during your upkeep.').
card_image_name('leviathan'/'5ED', 'leviathan').
card_uid('leviathan'/'5ED', '5ED:Leviathan:leviathan').
card_rarity('leviathan'/'5ED', 'Rare').
card_artist('leviathan'/'5ED', 'Mark Tedin').
card_multiverse_id('leviathan'/'5ED', '3919').

card_in_set('ley druid', '5ED').
card_original_type('ley druid'/'5ED', 'Summon — Cleric').
card_original_text('ley druid'/'5ED', '{T}: Untap target land.').
card_image_name('ley druid'/'5ED', 'ley druid').
card_uid('ley druid'/'5ED', '5ED:Ley Druid:ley druid').
card_rarity('ley druid'/'5ED', 'Common').
card_artist('ley druid'/'5ED', 'Sandra Everingham').
card_flavor_text('ley druid'/'5ED', 'After years of training, the druid becomes one with nature, drawing power from the land and returning it when needed.').
card_multiverse_id('ley druid'/'5ED', '3991').

card_in_set('lhurgoyf', '5ED').
card_original_type('lhurgoyf'/'5ED', 'Summon — Lhurgoyf').
card_original_text('lhurgoyf'/'5ED', 'Lhurgoyf has power equal to the number of creature cards in all graveyards and toughness equal to 1 plus the number of creature cards in all graveyards.').
card_image_name('lhurgoyf'/'5ED', 'lhurgoyf').
card_uid('lhurgoyf'/'5ED', '5ED:Lhurgoyf:lhurgoyf').
card_rarity('lhurgoyf'/'5ED', 'Rare').
card_artist('lhurgoyf'/'5ED', 'Pete Venters').
card_flavor_text('lhurgoyf'/'5ED', '\"Ach! Hans, run! It\'s the lhurgoyf!\"\n—Saffi Eriksdotter, last words').
card_multiverse_id('lhurgoyf'/'5ED', '3992').

card_in_set('library of leng', '5ED').
card_original_type('library of leng'/'5ED', 'Artifact').
card_original_text('library of leng'/'5ED', 'Skip your discard phase.\nWhenever a spell or effect forces you to discard a card, you may instead discard that card to the top of your library.').
card_image_name('library of leng'/'5ED', 'library of leng').
card_uid('library of leng'/'5ED', '5ED:Library of Leng:library of leng').
card_rarity('library of leng'/'5ED', 'Uncommon').
card_artist('library of leng'/'5ED', 'Daniel Gelon').
card_multiverse_id('library of leng'/'5ED', '3799').

card_in_set('lifeforce', '5ED').
card_original_type('lifeforce'/'5ED', 'Enchantment').
card_original_text('lifeforce'/'5ED', '{G}{G}: Counter target black spell. Play this ability as an interrupt.').
card_image_name('lifeforce'/'5ED', 'lifeforce').
card_uid('lifeforce'/'5ED', '5ED:Lifeforce:lifeforce').
card_rarity('lifeforce'/'5ED', 'Uncommon').
card_artist('lifeforce'/'5ED', 'Ron Spencer').
card_flavor_text('lifeforce'/'5ED', '\"Every fallen tree nourishes a thousand seedlings.\"\n—Autumn Willow to Baron Sengir').
card_multiverse_id('lifeforce'/'5ED', '3993').

card_in_set('lifetap', '5ED').
card_original_type('lifetap'/'5ED', 'Enchantment').
card_original_text('lifetap'/'5ED', 'Whenever any forest target opponent controls becomes tapped, gain 1 life.').
card_image_name('lifetap'/'5ED', 'lifetap').
card_uid('lifetap'/'5ED', '5ED:Lifetap:lifetap').
card_rarity('lifetap'/'5ED', 'Uncommon').
card_artist('lifetap'/'5ED', 'Mike Dringenberg').
card_flavor_text('lifetap'/'5ED', '\"Keep a green tree in your heart and perhaps the singing bird will come.\"\n—Chinese proverb').
card_multiverse_id('lifetap'/'5ED', '3920').

card_in_set('living artifact', '5ED').
card_original_type('living artifact'/'5ED', 'Enchant Artifact').
card_original_text('living artifact'/'5ED', 'For each 1 damage dealt to you, put a vitality counter on Living Artifact.\nRemove a vitality counter from Living Artifact: Gain 1 life. Use this ability only during your upkeep and only once each turn.').
card_image_name('living artifact'/'5ED', 'living artifact').
card_uid('living artifact'/'5ED', '5ED:Living Artifact:living artifact').
card_rarity('living artifact'/'5ED', 'Rare').
card_artist('living artifact'/'5ED', 'Anson Maddocks').
card_multiverse_id('living artifact'/'5ED', '3994').

card_in_set('living lands', '5ED').
card_original_type('living lands'/'5ED', 'Enchantment').
card_original_text('living lands'/'5ED', 'All forests are 1/1 creatures. (These creatures still count as lands.)').
card_image_name('living lands'/'5ED', 'living lands').
card_uid('living lands'/'5ED', '5ED:Living Lands:living lands').
card_rarity('living lands'/'5ED', 'Rare').
card_artist('living lands'/'5ED', 'John Matson').
card_multiverse_id('living lands'/'5ED', '3995').

card_in_set('llanowar elves', '5ED').
card_original_type('llanowar elves'/'5ED', 'Summon — Elves').
card_original_text('llanowar elves'/'5ED', '{T}: Add {G} to your mana pool. Play this ability as a mana source.').
card_image_name('llanowar elves'/'5ED', 'llanowar elves').
card_uid('llanowar elves'/'5ED', '5ED:Llanowar Elves:llanowar elves').
card_rarity('llanowar elves'/'5ED', 'Common').
card_artist('llanowar elves'/'5ED', 'Anson Maddocks').
card_flavor_text('llanowar elves'/'5ED', 'One bone broken for every twig snapped underfoot.\n—Llanowar penalty for trespassing').
card_multiverse_id('llanowar elves'/'5ED', '3996').

card_in_set('lord of atlantis', '5ED').
card_original_type('lord of atlantis'/'5ED', 'Summon — Lord').
card_original_text('lord of atlantis'/'5ED', 'All Merfolk get +1/+1 and gain islandwalk. (If defending player controls any islands, these creatures are unblockable.)').
card_image_name('lord of atlantis'/'5ED', 'lord of atlantis').
card_uid('lord of atlantis'/'5ED', '5ED:Lord of Atlantis:lord of atlantis').
card_rarity('lord of atlantis'/'5ED', 'Rare').
card_artist('lord of atlantis'/'5ED', 'Melissa A. Benson').
card_flavor_text('lord of atlantis'/'5ED', '\"‘We have no legs,\' the warrior spat, ‘for we have finished running.\'\"\n—Vodalian folktale').
card_multiverse_id('lord of atlantis'/'5ED', '3921').

card_in_set('lord of the pit', '5ED').
card_original_type('lord of the pit'/'5ED', 'Summon — Demon').
card_original_text('lord of the pit'/'5ED', 'Flying, trample\nDuring your upkeep, sacrifice a creature other than Lord of the Pit. If you cannot, Lord of the Pit deals 7 damage to you.').
card_image_name('lord of the pit'/'5ED', 'lord of the pit').
card_uid('lord of the pit'/'5ED', '5ED:Lord of the Pit:lord of the pit').
card_rarity('lord of the pit'/'5ED', 'Rare').
card_artist('lord of the pit'/'5ED', 'Mark Tedin').
card_multiverse_id('lord of the pit'/'5ED', '3857').

card_in_set('lost soul', '5ED').
card_original_type('lost soul'/'5ED', 'Summon — Lost Soul').
card_original_text('lost soul'/'5ED', 'Swampwalk (If defending player controls any swamps, this creature is unblockable.)').
card_image_name('lost soul'/'5ED', 'lost soul').
card_uid('lost soul'/'5ED', '5ED:Lost Soul:lost soul').
card_rarity('lost soul'/'5ED', 'Common').
card_artist('lost soul'/'5ED', 'Randy Asplund-Faith').
card_flavor_text('lost soul'/'5ED', 'Her hand gently beckons, she whispers your name—but those who go with her are never the same.').
card_multiverse_id('lost soul'/'5ED', '3858').

card_in_set('lure', '5ED').
card_original_type('lure'/'5ED', 'Enchant Creature').
card_original_text('lure'/'5ED', 'All creatures able to block enchanted creature do so.').
card_image_name('lure'/'5ED', 'lure').
card_uid('lure'/'5ED', '5ED:Lure:lure').
card_rarity('lure'/'5ED', 'Uncommon').
card_artist('lure'/'5ED', 'Anson Maddocks').
card_flavor_text('lure'/'5ED', '\"When setting a snare, success depends not on the quality of the trap, but the quality of the bait.\"\n—Geyadrone Dihada').
card_multiverse_id('lure'/'5ED', '3997').

card_in_set('magical hack', '5ED').
card_original_type('magical hack'/'5ED', 'Interrupt').
card_original_text('magical hack'/'5ED', 'Change the text of target permanent or spell by replacing all instances of one basic land type with another. (For example, you may change \"swampwalk\" to \"plainswalk.\" If this spell targets a permanent, play it as an instant.)').
card_image_name('magical hack'/'5ED', 'magical hack').
card_uid('magical hack'/'5ED', '5ED:Magical Hack:magical hack').
card_rarity('magical hack'/'5ED', 'Rare').
card_artist('magical hack'/'5ED', 'Julie Baroh').
card_multiverse_id('magical hack'/'5ED', '3922').

card_in_set('magus of the unseen', '5ED').
card_original_type('magus of the unseen'/'5ED', 'Summon — Wizard').
card_original_text('magus of the unseen'/'5ED', '{1}{U}, {T}: Untap target artifact an opponent controls and gain control of it until end of turn. That artifact is unaffected by summoning sickness this turn. Tap the artifact if you lose control of it at end of this turn.').
card_image_name('magus of the unseen'/'5ED', 'magus of the unseen').
card_uid('magus of the unseen'/'5ED', '5ED:Magus of the Unseen:magus of the unseen').
card_rarity('magus of the unseen'/'5ED', 'Rare').
card_artist('magus of the unseen'/'5ED', 'Kaja Foglio').
card_multiverse_id('magus of the unseen'/'5ED', '3923').

card_in_set('mana clash', '5ED').
card_original_type('mana clash'/'5ED', 'Sorcery').
card_original_text('mana clash'/'5ED', 'You and target opponent each flip a coin. Mana Clash deals 1 damage to each player whose coin comes up tails. Repeat this process until both players\' coins come up heads at the same time.').
card_image_name('mana clash'/'5ED', 'mana clash').
card_uid('mana clash'/'5ED', '5ED:Mana Clash:mana clash').
card_rarity('mana clash'/'5ED', 'Rare').
card_artist('mana clash'/'5ED', 'Mark Tedin').
card_multiverse_id('mana clash'/'5ED', '4069').

card_in_set('mana flare', '5ED').
card_original_type('mana flare'/'5ED', 'Enchantment').
card_original_text('mana flare'/'5ED', 'Whenever any player taps a land for mana, it produces one additional mana of the same type.').
card_image_name('mana flare'/'5ED', 'mana flare').
card_uid('mana flare'/'5ED', '5ED:Mana Flare:mana flare').
card_rarity('mana flare'/'5ED', 'Rare').
card_artist('mana flare'/'5ED', 'Christopher Rush').
card_multiverse_id('mana flare'/'5ED', '4070').

card_in_set('mana vault', '5ED').
card_original_type('mana vault'/'5ED', 'Artifact').
card_original_text('mana vault'/'5ED', 'Mana Vault does not untap during your untap phase.\nAt the end of your upkeep, if Mana Vault is tapped, it deals 1 damage to you.\n{4}: Untap Mana Vault at end of upkeep. Use this ability only during your upkeep.\n{T}: Add three colorless mana to your mana pool. Play this ability as a mana source.').
card_image_name('mana vault'/'5ED', 'mana vault').
card_uid('mana vault'/'5ED', '5ED:Mana Vault:mana vault').
card_rarity('mana vault'/'5ED', 'Rare').
card_artist('mana vault'/'5ED', 'Mark Tedin').
card_multiverse_id('mana vault'/'5ED', '3800').

card_in_set('manabarbs', '5ED').
card_original_type('manabarbs'/'5ED', 'Enchantment').
card_original_text('manabarbs'/'5ED', 'Whenever any player taps a land for mana, Manabarbs deals 1 damage to him or her.').
card_image_name('manabarbs'/'5ED', 'manabarbs').
card_uid('manabarbs'/'5ED', '5ED:Manabarbs:manabarbs').
card_rarity('manabarbs'/'5ED', 'Rare').
card_artist('manabarbs'/'5ED', 'Greg Simanson').
card_multiverse_id('manabarbs'/'5ED', '4071').

card_in_set('marsh viper', '5ED').
card_original_type('marsh viper'/'5ED', 'Summon — Viper').
card_original_text('marsh viper'/'5ED', 'If Marsh Viper damages any player, he or she gets two poison counters. If any player has ten or more poison counters, he or she loses the game.').
card_image_name('marsh viper'/'5ED', 'marsh viper').
card_uid('marsh viper'/'5ED', '5ED:Marsh Viper:marsh viper').
card_rarity('marsh viper'/'5ED', 'Common').
card_artist('marsh viper'/'5ED', 'Ron Spencer').
card_flavor_text('marsh viper'/'5ED', '\"And the seraphs sob at vermin fangs\nIn human gore imbued.\"\n—Edgar Allan Poe, \"The Conqueror Wurm\"').
card_multiverse_id('marsh viper'/'5ED', '3998').

card_in_set('meekstone', '5ED').
card_original_type('meekstone'/'5ED', 'Artifact').
card_original_text('meekstone'/'5ED', 'Creatures with power 3 or greater do not untap during their controllers\' untap phases.').
card_image_name('meekstone'/'5ED', 'meekstone').
card_uid('meekstone'/'5ED', '5ED:Meekstone:meekstone').
card_rarity('meekstone'/'5ED', 'Rare').
card_artist('meekstone'/'5ED', 'Quinton Hoover').
card_flavor_text('meekstone'/'5ED', 'When the mighty sleep, the lowly prowl.').
card_multiverse_id('meekstone'/'5ED', '3801').

card_in_set('memory lapse', '5ED').
card_original_type('memory lapse'/'5ED', 'Interrupt').
card_original_text('memory lapse'/'5ED', 'Counter target spell. Put that spell on top of owner\'s library.').
card_image_name('memory lapse'/'5ED', 'memory lapse').
card_uid('memory lapse'/'5ED', '5ED:Memory Lapse:memory lapse').
card_rarity('memory lapse'/'5ED', 'Common').
card_artist('memory lapse'/'5ED', 'Mark Tedin').
card_flavor_text('memory lapse'/'5ED', '\"Oh, I had a conscience once. But alas, I seem to have forgotten where I put it.\"\n—Chandler').
card_multiverse_id('memory lapse'/'5ED', '3924').

card_in_set('merfolk of the pearl trident', '5ED').
card_original_type('merfolk of the pearl trident'/'5ED', 'Summon — Merfolk').
card_original_text('merfolk of the pearl trident'/'5ED', '').
card_image_name('merfolk of the pearl trident'/'5ED', 'merfolk of the pearl trident').
card_uid('merfolk of the pearl trident'/'5ED', '5ED:Merfolk of the Pearl Trident:merfolk of the pearl trident').
card_rarity('merfolk of the pearl trident'/'5ED', 'Common').
card_artist('merfolk of the pearl trident'/'5ED', 'John Matson').
card_flavor_text('merfolk of the pearl trident'/'5ED', 'Most human scholars believe that merfolk are the survivors of sunken Atlantis, humans adapted to the water. Merfolk, however, believe that humans sprang forth from merfolk who adapted themselves in order to explore their last frontier.').
card_multiverse_id('merfolk of the pearl trident'/'5ED', '3925').

card_in_set('mesa falcon', '5ED').
card_original_type('mesa falcon'/'5ED', 'Summon — Falcon').
card_original_text('mesa falcon'/'5ED', 'Flying\n{1}{W} +0/+1 until end of turn').
card_image_name('mesa falcon'/'5ED', 'mesa falcon').
card_uid('mesa falcon'/'5ED', '5ED:Mesa Falcon:mesa falcon').
card_rarity('mesa falcon'/'5ED', 'Common').
card_artist('mesa falcon'/'5ED', 'Mark Poole').
card_flavor_text('mesa falcon'/'5ED', '\"The faith of Serra is borne on wings of hope.\"\n—Gulsen, abbey matron').
card_multiverse_id('mesa falcon'/'5ED', '4143').

card_in_set('mesa pegasus', '5ED').
card_original_type('mesa pegasus'/'5ED', 'Summon — Pegasus').
card_original_text('mesa pegasus'/'5ED', 'Banding, flying').
card_image_name('mesa pegasus'/'5ED', 'mesa pegasus').
card_uid('mesa pegasus'/'5ED', '5ED:Mesa Pegasus:mesa pegasus').
card_rarity('mesa pegasus'/'5ED', 'Common').
card_artist('mesa pegasus'/'5ED', 'Melissa A. Benson').
card_flavor_text('mesa pegasus'/'5ED', 'Before a woman marries in the village of Sursi, she must visit the land of the mesa pegasus. Legend has it that if the woman is pure of heart and her love is true, a mesa pegasus will appear, blessing her family with long life and good fortune.').
card_multiverse_id('mesa pegasus'/'5ED', '4144').

card_in_set('millstone', '5ED').
card_original_type('millstone'/'5ED', 'Artifact').
card_original_text('millstone'/'5ED', '{2}, {T}: Put the top two cards of target player\'s library into his or her graveyard.').
card_image_name('millstone'/'5ED', 'millstone').
card_uid('millstone'/'5ED', '5ED:Millstone:millstone').
card_rarity('millstone'/'5ED', 'Rare').
card_artist('millstone'/'5ED', 'Kaja Foglio').
card_flavor_text('millstone'/'5ED', 'More than one mage has been driven insane by the sound of the millstone relentlessly grinding away.').
card_multiverse_id('millstone'/'5ED', '3802').

card_in_set('mind bomb', '5ED').
card_original_type('mind bomb'/'5ED', 'Sorcery').
card_original_text('mind bomb'/'5ED', 'Mind Bomb deals 3 damage to each player. Each player may choose and discard up to three cards to prevent that amount of damage to him or her from Mind Bomb.').
card_image_name('mind bomb'/'5ED', 'mind bomb').
card_uid('mind bomb'/'5ED', '5ED:Mind Bomb:mind bomb').
card_rarity('mind bomb'/'5ED', 'Uncommon').
card_artist('mind bomb'/'5ED', 'Mark Tedin').
card_multiverse_id('mind bomb'/'5ED', '3926').

card_in_set('mind ravel', '5ED').
card_original_type('mind ravel'/'5ED', 'Sorcery').
card_original_text('mind ravel'/'5ED', 'Target player chooses and discards a card.\nDraw a card at the beginning of the next turn.').
card_image_name('mind ravel'/'5ED', 'mind ravel').
card_uid('mind ravel'/'5ED', '5ED:Mind Ravel:mind ravel').
card_rarity('mind ravel'/'5ED', 'Common').
card_artist('mind ravel'/'5ED', 'Mark Tedin').
card_flavor_text('mind ravel'/'5ED', 'An end to reason, an end to order. Forget all that has been.').
card_multiverse_id('mind ravel'/'5ED', '3859').

card_in_set('mind warp', '5ED').
card_original_type('mind warp'/'5ED', 'Sorcery').
card_original_text('mind warp'/'5ED', 'Look at target player\'s hand. He or she discards X cards of your choice.').
card_image_name('mind warp'/'5ED', 'mind warp').
card_uid('mind warp'/'5ED', '5ED:Mind Warp:mind warp').
card_rarity('mind warp'/'5ED', 'Uncommon').
card_artist('mind warp'/'5ED', 'Liz Danforth').
card_flavor_text('mind warp'/'5ED', 'The hardest of wizards can have the softest of minds.').
card_multiverse_id('mind warp'/'5ED', '3860').

card_in_set('mindstab thrull', '5ED').
card_original_type('mindstab thrull'/'5ED', 'Summon — Thrull').
card_original_text('mindstab thrull'/'5ED', 'Sacrifice Mindstab Thrull: Defending player chooses and discards three cards. Use this ability only if Mindstab Thrull is attacking and unblocked.').
card_image_name('mindstab thrull'/'5ED', 'mindstab thrull').
card_uid('mindstab thrull'/'5ED', '5ED:Mindstab Thrull:mindstab thrull').
card_rarity('mindstab thrull'/'5ED', 'Common').
card_artist('mindstab thrull'/'5ED', 'Mark Tedin').
card_multiverse_id('mindstab thrull'/'5ED', '3861').

card_in_set('mole worms', '5ED').
card_original_type('mole worms'/'5ED', 'Summon — Worms').
card_original_text('mole worms'/'5ED', 'You may choose not to untap Mole Worms during your untap phase.\n{T}: Tap target land. As long as Mole Worms remains tapped, that land does not untap during its controller\'s untap phase.').
card_image_name('mole worms'/'5ED', 'mole worms').
card_uid('mole worms'/'5ED', '5ED:Mole Worms:mole worms').
card_rarity('mole worms'/'5ED', 'Uncommon').
card_artist('mole worms'/'5ED', 'Adrian Smith').
card_multiverse_id('mole worms'/'5ED', '3862').

card_in_set('mons\'s goblin raiders', '5ED').
card_original_type('mons\'s goblin raiders'/'5ED', 'Summon — Goblins').
card_original_text('mons\'s goblin raiders'/'5ED', '').
card_image_name('mons\'s goblin raiders'/'5ED', 'mons\'s goblin raiders').
card_uid('mons\'s goblin raiders'/'5ED', '5ED:Mons\'s Goblin Raiders:mons\'s goblin raiders').
card_rarity('mons\'s goblin raiders'/'5ED', 'Common').
card_artist('mons\'s goblin raiders'/'5ED', 'Pete Venters').
card_flavor_text('mons\'s goblin raiders'/'5ED', 'The intricate dynamics of Rundvelt goblin affairs are often confused with anarchy. The chaos, however, is the chaos of a thundercloud, and direction will sporadically and violently appear. Pashalik Mons and his raiders are the thunderhead that leads in the storm.').
card_multiverse_id('mons\'s goblin raiders'/'5ED', '4072').

card_in_set('mountain', '5ED').
card_original_type('mountain'/'5ED', 'Land').
card_original_text('mountain'/'5ED', '{T}: Add {R} to your mana pool.').
card_image_name('mountain'/'5ED', 'mountain1').
card_uid('mountain'/'5ED', '5ED:Mountain:mountain1').
card_rarity('mountain'/'5ED', 'Basic Land').
card_artist('mountain'/'5ED', 'John Avon').
card_multiverse_id('mountain'/'5ED', '4195').

card_in_set('mountain', '5ED').
card_original_type('mountain'/'5ED', 'Land').
card_original_text('mountain'/'5ED', '{T}: Add {R} to your mana pool.').
card_image_name('mountain'/'5ED', 'mountain2').
card_uid('mountain'/'5ED', '5ED:Mountain:mountain2').
card_rarity('mountain'/'5ED', 'Basic Land').
card_artist('mountain'/'5ED', 'John Avon').
card_multiverse_id('mountain'/'5ED', '4196').

card_in_set('mountain', '5ED').
card_original_type('mountain'/'5ED', 'Land').
card_original_text('mountain'/'5ED', '{T}: Add {R} to your mana pool.').
card_image_name('mountain'/'5ED', 'mountain3').
card_uid('mountain'/'5ED', '5ED:Mountain:mountain3').
card_rarity('mountain'/'5ED', 'Basic Land').
card_artist('mountain'/'5ED', 'John Avon').
card_multiverse_id('mountain'/'5ED', '4197').

card_in_set('mountain', '5ED').
card_original_type('mountain'/'5ED', 'Land').
card_original_text('mountain'/'5ED', '{T}: Add {R} to your mana pool.').
card_image_name('mountain'/'5ED', 'mountain4').
card_uid('mountain'/'5ED', '5ED:Mountain:mountain4').
card_rarity('mountain'/'5ED', 'Basic Land').
card_artist('mountain'/'5ED', 'John Avon').
card_multiverse_id('mountain'/'5ED', '4198').

card_in_set('mountain goat', '5ED').
card_original_type('mountain goat'/'5ED', 'Summon — Goat').
card_original_text('mountain goat'/'5ED', 'Mountainwalk (If defending player controls any mountains, this creature is unblockable.)').
card_image_name('mountain goat'/'5ED', 'mountain goat').
card_uid('mountain goat'/'5ED', '5ED:Mountain Goat:mountain goat').
card_rarity('mountain goat'/'5ED', 'Common').
card_artist('mountain goat'/'5ED', 'Cornelius Brudi').
card_flavor_text('mountain goat'/'5ED', '\"Folklore has it that to capture a mountain goat is a sign of divine blessing. I just know it\'s a sign that dinner is on the way.\"\n—Klazina Jansdotter,\nLeader of the Order of the Sacred Torch').
card_multiverse_id('mountain goat'/'5ED', '4073').

card_in_set('murk dwellers', '5ED').
card_original_type('murk dwellers'/'5ED', 'Summon — Murk Dwellers').
card_original_text('murk dwellers'/'5ED', 'If Murk Dwellers attacks and is not blocked, it gets +2/+0 until end of turn.').
card_image_name('murk dwellers'/'5ED', 'murk dwellers').
card_uid('murk dwellers'/'5ED', '5ED:Murk Dwellers:murk dwellers').
card_rarity('murk dwellers'/'5ED', 'Common').
card_artist('murk dwellers'/'5ED', 'Drew Tucker').
card_flavor_text('murk dwellers'/'5ED', 'When Raganorn unsealed the catacombs, he found more than the dead and their treasures.').
card_multiverse_id('murk dwellers'/'5ED', '3863').

card_in_set('nature\'s lore', '5ED').
card_original_type('nature\'s lore'/'5ED', 'Sorcery').
card_original_text('nature\'s lore'/'5ED', 'Search your library for a forest card and put that card into play. Shuffle your library afterwards.').
card_image_name('nature\'s lore'/'5ED', 'nature\'s lore').
card_uid('nature\'s lore'/'5ED', '5ED:Nature\'s Lore:nature\'s lore').
card_rarity('nature\'s lore'/'5ED', 'Common').
card_artist('nature\'s lore'/'5ED', 'Alan Rabinowitz').
card_flavor_text('nature\'s lore'/'5ED', '\"It is in our nature to recognize beauty, but to repeat it—ah, that is another feat altogether.\"\n—Hannah of Verdura').
card_multiverse_id('nature\'s lore'/'5ED', '3999').

card_in_set('necrite', '5ED').
card_original_type('necrite'/'5ED', 'Summon — Thrull').
card_original_text('necrite'/'5ED', 'Sacrifice Necrite: Bury target creature defending player controls. Use this ability only if Necrite is attacking and unblocked.').
card_image_name('necrite'/'5ED', 'necrite').
card_uid('necrite'/'5ED', '5ED:Necrite:necrite').
card_rarity('necrite'/'5ED', 'Common').
card_artist('necrite'/'5ED', 'Ron Spencer').
card_flavor_text('necrite'/'5ED', 'Necrites killed Jherana Rure, ending the counterinsurgency.').
card_multiverse_id('necrite'/'5ED', '3864').

card_in_set('necropotence', '5ED').
card_original_type('necropotence'/'5ED', 'Enchantment').
card_original_text('necropotence'/'5ED', 'Skip your draw phase.\nWhenever you discard a card, remove that card from the game.\nPay 1 life: Set aside the top card of your library. Put that card into your hand at the beginning of your discard phase.').
card_image_name('necropotence'/'5ED', 'necropotence').
card_uid('necropotence'/'5ED', '5ED:Necropotence:necropotence').
card_rarity('necropotence'/'5ED', 'Rare').
card_artist('necropotence'/'5ED', 'Mark Tedin').
card_multiverse_id('necropotence'/'5ED', '3865').

card_in_set('nether shadow', '5ED').
card_original_type('nether shadow'/'5ED', 'Summon — Shadow').
card_original_text('nether shadow'/'5ED', 'Nether Shadow is unaffected by summoning sickness.\nAt the end of your upkeep, if Nether Shadow is in your graveyard with at least three creature cards above it, you may put Nether Shadow into play.').
card_image_name('nether shadow'/'5ED', 'nether shadow').
card_uid('nether shadow'/'5ED', '5ED:Nether Shadow:nether shadow').
card_rarity('nether shadow'/'5ED', 'Rare').
card_artist('nether shadow'/'5ED', 'DiTerlizzi').
card_multiverse_id('nether shadow'/'5ED', '3866').

card_in_set('nevinyrral\'s disk', '5ED').
card_original_type('nevinyrral\'s disk'/'5ED', 'Artifact').
card_original_text('nevinyrral\'s disk'/'5ED', 'Nevinyrral\'s Disk comes into play tapped.\n{1}, {T}: Destroy all artifacts, creatures, and enchantments.').
card_image_name('nevinyrral\'s disk'/'5ED', 'nevinyrral\'s disk').
card_uid('nevinyrral\'s disk'/'5ED', '5ED:Nevinyrral\'s Disk:nevinyrral\'s disk').
card_rarity('nevinyrral\'s disk'/'5ED', 'Rare').
card_artist('nevinyrral\'s disk'/'5ED', 'Mark Tedin').
card_multiverse_id('nevinyrral\'s disk'/'5ED', '3803').

card_in_set('nightmare', '5ED').
card_original_type('nightmare'/'5ED', 'Summon — Nightmare').
card_original_text('nightmare'/'5ED', 'Flying\nNightmare has power and toughness each equal to the number of swamps you control.').
card_image_name('nightmare'/'5ED', 'nightmare').
card_uid('nightmare'/'5ED', '5ED:Nightmare:nightmare').
card_rarity('nightmare'/'5ED', 'Rare').
card_artist('nightmare'/'5ED', 'Melissa A. Benson').
card_flavor_text('nightmare'/'5ED', 'The nightmare arises from its lair in the swamps. As the poisoned land spreads, so does the nightmare\'s rage and terrifying strength.').
card_multiverse_id('nightmare'/'5ED', '3867').

card_in_set('obelisk of undoing', '5ED').
card_original_type('obelisk of undoing'/'5ED', 'Artifact').
card_original_text('obelisk of undoing'/'5ED', '{6}, {T}: Return target permanent you control and own to your hand.').
card_image_name('obelisk of undoing'/'5ED', 'obelisk of undoing').
card_uid('obelisk of undoing'/'5ED', '5ED:Obelisk of Undoing:obelisk of undoing').
card_rarity('obelisk of undoing'/'5ED', 'Rare').
card_artist('obelisk of undoing'/'5ED', 'Tom Wänerstrand').
card_flavor_text('obelisk of undoing'/'5ED', 'The Battle of Tomakul taught Urza not to rely on fickle reinforcements.').
card_multiverse_id('obelisk of undoing'/'5ED', '3804').

card_in_set('orcish artillery', '5ED').
card_original_type('orcish artillery'/'5ED', 'Summon — Orcs').
card_original_text('orcish artillery'/'5ED', '{T}: Orcish Artillery deals 2 damage to target creature or player and 3 damage to you.').
card_image_name('orcish artillery'/'5ED', 'orcish artillery').
card_uid('orcish artillery'/'5ED', '5ED:Orcish Artillery:orcish artillery').
card_rarity('orcish artillery'/'5ED', 'Uncommon').
card_artist('orcish artillery'/'5ED', 'Dan Frazier').
card_flavor_text('orcish artillery'/'5ED', 'In a rare display of ingenuity, the orcs invented an incredibly destructive weapon. Most orcish artillerists are those who dared criticize its effectiveness.').
card_multiverse_id('orcish artillery'/'5ED', '4074').

card_in_set('orcish captain', '5ED').
card_original_type('orcish captain'/'5ED', 'Summon — Orc').
card_original_text('orcish captain'/'5ED', '{1}: Flip a coin; target opponent calls heads or tails while coin is in the air. If the flip ends up in your favor, target Orc gets +2/+0 until end of turn. Otherwise, that Orc gets -0/-2 until end of turn.').
card_image_name('orcish captain'/'5ED', 'orcish captain').
card_uid('orcish captain'/'5ED', '5ED:Orcish Captain:orcish captain').
card_rarity('orcish captain'/'5ED', 'Uncommon').
card_artist('orcish captain'/'5ED', 'Charles Gillespie').
card_flavor_text('orcish captain'/'5ED', 'There\'s a chance to win every battle.').
card_multiverse_id('orcish captain'/'5ED', '4075').

card_in_set('orcish conscripts', '5ED').
card_original_type('orcish conscripts'/'5ED', 'Summon — Orcs').
card_original_text('orcish conscripts'/'5ED', 'Orcish Conscripts cannot attack this turn unless at least two other creatures are attacking.\nOrcish Conscripts cannot be assigned to block this turn unless at least two other creatures are blocking.').
card_image_name('orcish conscripts'/'5ED', 'orcish conscripts').
card_uid('orcish conscripts'/'5ED', '5ED:Orcish Conscripts:orcish conscripts').
card_rarity('orcish conscripts'/'5ED', 'Common').
card_artist('orcish conscripts'/'5ED', 'Douglas Shuler').
card_multiverse_id('orcish conscripts'/'5ED', '4076').

card_in_set('orcish farmer', '5ED').
card_original_type('orcish farmer'/'5ED', 'Summon — Orc').
card_original_text('orcish farmer'/'5ED', '{T}: Target land is a swamp until its controller\'s next untap phase.').
card_image_name('orcish farmer'/'5ED', 'orcish farmer').
card_uid('orcish farmer'/'5ED', '5ED:Orcish Farmer:orcish farmer').
card_rarity('orcish farmer'/'5ED', 'Common').
card_artist('orcish farmer'/'5ED', 'Dan Frazier').
card_flavor_text('orcish farmer'/'5ED', 'There is no natural disaster quite like orcish farming.').
card_multiverse_id('orcish farmer'/'5ED', '4077').

card_in_set('orcish oriflamme', '5ED').
card_original_type('orcish oriflamme'/'5ED', 'Enchantment').
card_original_text('orcish oriflamme'/'5ED', 'Attacking creatures you control get +1/+0.').
card_image_name('orcish oriflamme'/'5ED', 'orcish oriflamme').
card_uid('orcish oriflamme'/'5ED', '5ED:Orcish Oriflamme:orcish oriflamme').
card_rarity('orcish oriflamme'/'5ED', 'Uncommon').
card_artist('orcish oriflamme'/'5ED', 'Dan Frazier').
card_multiverse_id('orcish oriflamme'/'5ED', '4078').

card_in_set('orcish squatters', '5ED').
card_original_type('orcish squatters'/'5ED', 'Summon — Orcs').
card_original_text('orcish squatters'/'5ED', '{0}: Gain control of target land defending player controls as long as you control Orcish Squatters. Orcish Squatters deals no combat damage this turn. Use this ability only if Orcish Squatters is attacking and unblocked and only once each turn.').
card_image_name('orcish squatters'/'5ED', 'orcish squatters').
card_uid('orcish squatters'/'5ED', '5ED:Orcish Squatters:orcish squatters').
card_rarity('orcish squatters'/'5ED', 'Rare').
card_artist('orcish squatters'/'5ED', 'Richard Kane Ferguson').
card_multiverse_id('orcish squatters'/'5ED', '4079').

card_in_set('order of the sacred torch', '5ED').
card_original_type('order of the sacred torch'/'5ED', 'Summon — Paladin').
card_original_text('order of the sacred torch'/'5ED', '{T}, Pay 1 life: Counter target black spell. Play this ability as an interrupt.').
card_image_name('order of the sacred torch'/'5ED', 'order of the sacred torch').
card_uid('order of the sacred torch'/'5ED', '5ED:Order of the Sacred Torch:order of the sacred torch').
card_rarity('order of the sacred torch'/'5ED', 'Rare').
card_artist('order of the sacred torch'/'5ED', 'Ruth Thompson').
card_flavor_text('order of the sacred torch'/'5ED', 'A blazing light to drive out the shadows.').
card_multiverse_id('order of the sacred torch'/'5ED', '4145').

card_in_set('order of the white shield', '5ED').
card_original_type('order of the white shield'/'5ED', 'Summon — Knights').
card_original_text('order of the white shield'/'5ED', 'Protection from black\n{W} First strike until end of turn\n{W}{W} +1/+0 until end of turn').
card_image_name('order of the white shield'/'5ED', 'order of the white shield').
card_uid('order of the white shield'/'5ED', '5ED:Order of the White Shield:order of the white shield').
card_rarity('order of the white shield'/'5ED', 'Uncommon').
card_artist('order of the white shield'/'5ED', 'Ruth Thompson').
card_flavor_text('order of the white shield'/'5ED', 'A shield of light admits no shadow.').
card_multiverse_id('order of the white shield'/'5ED', '4146').

card_in_set('orgg', '5ED').
card_original_type('orgg'/'5ED', 'Summon — Orgg').
card_original_text('orgg'/'5ED', 'Trample\nOrgg cannot attack if defending player controls an untapped creature with power 3 or greater.\nOrgg cannot be assigned to block any creature with power 3 or greater.').
card_image_name('orgg'/'5ED', 'orgg').
card_uid('orgg'/'5ED', '5ED:Orgg:orgg').
card_rarity('orgg'/'5ED', 'Rare').
card_artist('orgg'/'5ED', 'Daniel Gelon').
card_flavor_text('orgg'/'5ED', 'It\'s bigger than it thinks.').
card_multiverse_id('orgg'/'5ED', '4080').

card_in_set('ornithopter', '5ED').
card_original_type('ornithopter'/'5ED', 'Artifact Creature').
card_original_text('ornithopter'/'5ED', 'Flying').
card_image_name('ornithopter'/'5ED', 'ornithopter').
card_uid('ornithopter'/'5ED', '5ED:Ornithopter:ornithopter').
card_rarity('ornithopter'/'5ED', 'Uncommon').
card_artist('ornithopter'/'5ED', 'Amy Weber').
card_flavor_text('ornithopter'/'5ED', '\"It has been my honor to improve on Thran\'s original design. Perhaps history will remember me in some small part for my work.\"\n—Urza, in his apprenticeship').
card_multiverse_id('ornithopter'/'5ED', '3805').

card_in_set('panic', '5ED').
card_original_type('panic'/'5ED', 'Instant').
card_original_text('panic'/'5ED', 'Play only during combat before blockers are declared.\nTarget creature cannot be assigned to block this turn.\nDraw a card at the beginning of the next turn.').
card_image_name('panic'/'5ED', 'panic').
card_uid('panic'/'5ED', '5ED:Panic:panic').
card_rarity('panic'/'5ED', 'Common').
card_artist('panic'/'5ED', 'Greg Simanson').
card_multiverse_id('panic'/'5ED', '4081').

card_in_set('paralyze', '5ED').
card_original_type('paralyze'/'5ED', 'Enchant Creature').
card_original_text('paralyze'/'5ED', 'When Paralyze comes into play, tap enchanted creature.\nEnchanted creature does not untap during its controller\'s untap phase. That player may pay an additional {4} during his or her upkeep to untap it.').
card_image_name('paralyze'/'5ED', 'paralyze').
card_uid('paralyze'/'5ED', '5ED:Paralyze:paralyze').
card_rarity('paralyze'/'5ED', 'Common').
card_artist('paralyze'/'5ED', 'Ron Spencer').
card_multiverse_id('paralyze'/'5ED', '3868').

card_in_set('pearled unicorn', '5ED').
card_original_type('pearled unicorn'/'5ED', 'Summon — Unicorn').
card_original_text('pearled unicorn'/'5ED', '').
card_image_name('pearled unicorn'/'5ED', 'pearled unicorn').
card_uid('pearled unicorn'/'5ED', '5ED:Pearled Unicorn:pearled unicorn').
card_rarity('pearled unicorn'/'5ED', 'Common').
card_artist('pearled unicorn'/'5ED', 'David A. Cherry').
card_flavor_text('pearled unicorn'/'5ED', '\"‘Do you know, I always thought Unicorns were fabulous monsters, too? I never saw one alive before!\' ‘Well, now that we have seen each other,\' said the Unicorn, ‘if you\'ll believe in me, I\'ll believe in you.\'\"\n—Lewis Carroll, Through the Looking-Glass').
card_multiverse_id('pearled unicorn'/'5ED', '4147').

card_in_set('pentagram of the ages', '5ED').
card_original_type('pentagram of the ages'/'5ED', 'Artifact').
card_original_text('pentagram of the ages'/'5ED', '{4}, {T}: Prevent all damage to you from one source. Treat further damage from that source normally.').
card_image_name('pentagram of the ages'/'5ED', 'pentagram of the ages').
card_uid('pentagram of the ages'/'5ED', '5ED:Pentagram of the Ages:pentagram of the ages').
card_rarity('pentagram of the ages'/'5ED', 'Rare').
card_artist('pentagram of the ages'/'5ED', 'Douglas Shuler').
card_flavor_text('pentagram of the ages'/'5ED', 'A mirror, a shield, a promise, a great distance, and a kind word—five ways to avoid harm.').
card_multiverse_id('pentagram of the ages'/'5ED', '3806').

card_in_set('personal incarnation', '5ED').
card_original_type('personal incarnation'/'5ED', 'Summon — Avatar').
card_original_text('personal incarnation'/'5ED', 'Personal Incarnation\'s owner may redirect any amount of damage from it to himself or herself.\nIf Personal Incarnation is put into any graveyard from play, its owner loses half his or her life, rounded up.').
card_image_name('personal incarnation'/'5ED', 'personal incarnation').
card_uid('personal incarnation'/'5ED', '5ED:Personal Incarnation:personal incarnation').
card_rarity('personal incarnation'/'5ED', 'Rare').
card_artist('personal incarnation'/'5ED', 'Kev Walker').
card_multiverse_id('personal incarnation'/'5ED', '4148').

card_in_set('pestilence', '5ED').
card_original_type('pestilence'/'5ED', 'Enchantment').
card_original_text('pestilence'/'5ED', 'At the end of any turn, if there are no creatures in play, bury Pestilence.\n{B} Pestilence deals 1 damage to each creature and player.').
card_image_name('pestilence'/'5ED', 'pestilence').
card_uid('pestilence'/'5ED', '5ED:Pestilence:pestilence').
card_rarity('pestilence'/'5ED', 'Common').
card_artist('pestilence'/'5ED', 'Kev Walker').
card_multiverse_id('pestilence'/'5ED', '3869').

card_in_set('phantasmal forces', '5ED').
card_original_type('phantasmal forces'/'5ED', 'Summon — Phantasm').
card_original_text('phantasmal forces'/'5ED', 'Flying\nDuring your upkeep, pay {U} or bury Phantasmal Forces.').
card_image_name('phantasmal forces'/'5ED', 'phantasmal forces').
card_uid('phantasmal forces'/'5ED', '5ED:Phantasmal Forces:phantasmal forces').
card_rarity('phantasmal forces'/'5ED', 'Uncommon').
card_artist('phantasmal forces'/'5ED', 'Mark Poole').
card_flavor_text('phantasmal forces'/'5ED', 'These beings embody the essence of true heroes long dead. Summoned from the dreamrealms, they rise to meet their enemies.').
card_multiverse_id('phantasmal forces'/'5ED', '3927').

card_in_set('phantasmal terrain', '5ED').
card_original_type('phantasmal terrain'/'5ED', 'Enchant Land').
card_original_text('phantasmal terrain'/'5ED', 'Enchanted land is a basic land type of your choice.').
card_image_name('phantasmal terrain'/'5ED', 'phantasmal terrain').
card_uid('phantasmal terrain'/'5ED', '5ED:Phantasmal Terrain:phantasmal terrain').
card_rarity('phantasmal terrain'/'5ED', 'Common').
card_artist('phantasmal terrain'/'5ED', 'David A. Cherry').
card_multiverse_id('phantasmal terrain'/'5ED', '3928').

card_in_set('phantom monster', '5ED').
card_original_type('phantom monster'/'5ED', 'Summon — Phantasm').
card_original_text('phantom monster'/'5ED', 'Flying').
card_image_name('phantom monster'/'5ED', 'phantom monster').
card_uid('phantom monster'/'5ED', '5ED:Phantom Monster:phantom monster').
card_rarity('phantom monster'/'5ED', 'Uncommon').
card_artist('phantom monster'/'5ED', 'Rebecca Guay').
card_flavor_text('phantom monster'/'5ED', '\"While, like a ghastly rapid river,\nThrough the pale door,\nA hideous throng rush out forever,\nAnd laugh—but smile no more.\"\n—Edgar Allan Poe,\n\"The Haunted Palace\"').
card_multiverse_id('phantom monster'/'5ED', '3929').

card_in_set('pikemen', '5ED').
card_original_type('pikemen'/'5ED', 'Summon — Pikemen').
card_original_text('pikemen'/'5ED', 'Banding, first strike').
card_image_name('pikemen'/'5ED', 'pikemen').
card_uid('pikemen'/'5ED', '5ED:Pikemen:pikemen').
card_rarity('pikemen'/'5ED', 'Common').
card_artist('pikemen'/'5ED', 'Dan Frazier').
card_flavor_text('pikemen'/'5ED', '\"As the cavalry bore down, we faced them with swords drawn and pikes hidden in the grass at our feet. ‘Don\'t lift your pikes \'til I give the word,\' I said.\"\n—Maeveen O\'Donagh,\nMemoirs of a Soldier').
card_multiverse_id('pikemen'/'5ED', '4149').

card_in_set('pirate ship', '5ED').
card_original_type('pirate ship'/'5ED', 'Summon — Ship').
card_original_text('pirate ship'/'5ED', 'Islandhome (If defending player controls no islands, this creature cannot attack. If you control no islands, bury this creature.)\n{T}: Pirate Ship deals 1 damage to target creature or player.').
card_image_name('pirate ship'/'5ED', 'pirate ship').
card_uid('pirate ship'/'5ED', '5ED:Pirate Ship:pirate ship').
card_rarity('pirate ship'/'5ED', 'Rare').
card_artist('pirate ship'/'5ED', 'Tom Wänerstrand').
card_multiverse_id('pirate ship'/'5ED', '3930').

card_in_set('pit scorpion', '5ED').
card_original_type('pit scorpion'/'5ED', 'Summon — Scorpion').
card_original_text('pit scorpion'/'5ED', 'If Pit Scorpion damages any player, he or she gets a poison counter. If any player has ten or more poison counters, he or she loses the game.').
card_image_name('pit scorpion'/'5ED', 'pit scorpion').
card_uid('pit scorpion'/'5ED', '5ED:Pit Scorpion:pit scorpion').
card_rarity('pit scorpion'/'5ED', 'Common').
card_artist('pit scorpion'/'5ED', 'Ian Miller').
card_flavor_text('pit scorpion'/'5ED', 'Sometimes the smallest nuisance can be the greatest pain.').
card_multiverse_id('pit scorpion'/'5ED', '3870').

card_in_set('plague rats', '5ED').
card_original_type('plague rats'/'5ED', 'Summon — Rats').
card_original_text('plague rats'/'5ED', 'Plague Rats has power and toughness each equal to the number of Plague Rats in play.').
card_image_name('plague rats'/'5ED', 'plague rats').
card_uid('plague rats'/'5ED', '5ED:Plague Rats:plague rats').
card_rarity('plague rats'/'5ED', 'Common').
card_artist('plague rats'/'5ED', 'Anson Maddocks').
card_flavor_text('plague rats'/'5ED', '\"Should you a Rat to madness tease\nWhy ev\'n a Rat may plague you . . . .\"\n—Samuel Coleridge, \"Recantation\"').
card_multiverse_id('plague rats'/'5ED', '3871').

card_in_set('plains', '5ED').
card_original_type('plains'/'5ED', 'Land').
card_original_text('plains'/'5ED', '{T}: Add {W} to your mana pool.').
card_image_name('plains'/'5ED', 'plains1').
card_uid('plains'/'5ED', '5ED:Plains:plains1').
card_rarity('plains'/'5ED', 'Basic Land').
card_artist('plains'/'5ED', 'Pat Morrissey').
card_multiverse_id('plains'/'5ED', '4204').

card_in_set('plains', '5ED').
card_original_type('plains'/'5ED', 'Land').
card_original_text('plains'/'5ED', '{T}: Add {W} to your mana pool.').
card_image_name('plains'/'5ED', 'plains2').
card_uid('plains'/'5ED', '5ED:Plains:plains2').
card_rarity('plains'/'5ED', 'Basic Land').
card_artist('plains'/'5ED', 'Pat Morrissey').
card_multiverse_id('plains'/'5ED', '4206').

card_in_set('plains', '5ED').
card_original_type('plains'/'5ED', 'Land').
card_original_text('plains'/'5ED', '{T}: Add {W} to your mana pool.').
card_image_name('plains'/'5ED', 'plains3').
card_uid('plains'/'5ED', '5ED:Plains:plains3').
card_rarity('plains'/'5ED', 'Basic Land').
card_artist('plains'/'5ED', 'Pat Morrissey').
card_multiverse_id('plains'/'5ED', '4205').

card_in_set('plains', '5ED').
card_original_type('plains'/'5ED', 'Land').
card_original_text('plains'/'5ED', '{T}: Add {W} to your mana pool.').
card_image_name('plains'/'5ED', 'plains4').
card_uid('plains'/'5ED', '5ED:Plains:plains4').
card_rarity('plains'/'5ED', 'Basic Land').
card_artist('plains'/'5ED', 'Pat Morrissey').
card_multiverse_id('plains'/'5ED', '4203').

card_in_set('portent', '5ED').
card_original_type('portent'/'5ED', 'Sorcery').
card_original_text('portent'/'5ED', 'Look at the top three cards of target player\'s library. Shuffle that library or put those three cards back on top of it in any order. Draw a card at the beginning of the next turn.').
card_image_name('portent'/'5ED', 'portent').
card_uid('portent'/'5ED', '5ED:Portent:portent').
card_rarity('portent'/'5ED', 'Common').
card_artist('portent'/'5ED', 'Liz Danforth').
card_multiverse_id('portent'/'5ED', '3931').

card_in_set('power sink', '5ED').
card_original_type('power sink'/'5ED', 'Interrupt').
card_original_text('power sink'/'5ED', 'Counter target spell unless that spell\'s caster pays an additional {X}. That player draws and pays all mana from lands and mana pool until {X} is paid; he or she may also draw and pay mana from other sources if desired.').
card_image_name('power sink'/'5ED', 'power sink').
card_uid('power sink'/'5ED', '5ED:Power Sink:power sink').
card_rarity('power sink'/'5ED', 'Uncommon').
card_artist('power sink'/'5ED', 'Richard Thomas').
card_multiverse_id('power sink'/'5ED', '3932').

card_in_set('pox', '5ED').
card_original_type('pox'/'5ED', 'Sorcery').
card_original_text('pox'/'5ED', 'Each player loses 1/3 of his or her life; then chooses and discards 1/3 of his or her hand; then sacrifices 1/3 of the creatures he or she controls; and then sacrifices 1/3 of the lands he or she controls. Round each loss up.').
card_image_name('pox'/'5ED', 'pox').
card_uid('pox'/'5ED', '5ED:Pox:pox').
card_rarity('pox'/'5ED', 'Rare').
card_artist('pox'/'5ED', 'Scott M. Fischer').
card_multiverse_id('pox'/'5ED', '3872').

card_in_set('pradesh gypsies', '5ED').
card_original_type('pradesh gypsies'/'5ED', 'Summon — Gypsies').
card_original_text('pradesh gypsies'/'5ED', '{1}{G}, {T}: Target creature gets -2/-0 until end of turn.').
card_image_name('pradesh gypsies'/'5ED', 'pradesh gypsies').
card_uid('pradesh gypsies'/'5ED', '5ED:Pradesh Gypsies:pradesh gypsies').
card_rarity('pradesh gypsies'/'5ED', 'Common').
card_artist('pradesh gypsies'/'5ED', 'Quinton Hoover').
card_flavor_text('pradesh gypsies'/'5ED', '\"A mysterious people indeed. Their origins are as secret as their traditions.\"\n—Lord Magnus of Llanowar').
card_multiverse_id('pradesh gypsies'/'5ED', '4000').

card_in_set('primal clay', '5ED').
card_original_type('primal clay'/'5ED', 'Artifact Creature').
card_original_text('primal clay'/'5ED', 'When you play Primal Clay, choose one—Primal Clay is a 2/2 artifact creature with flying; or Primal Clay is a 3/3 artifact creature; or Primal Clay is a 1/6 artifact creature that counts as a Wall.').
card_image_name('primal clay'/'5ED', 'primal clay').
card_uid('primal clay'/'5ED', '5ED:Primal Clay:primal clay').
card_rarity('primal clay'/'5ED', 'Rare').
card_artist('primal clay'/'5ED', 'Adam Rex').
card_multiverse_id('primal clay'/'5ED', '3807').

card_in_set('primal order', '5ED').
card_original_type('primal order'/'5ED', 'Enchantment').
card_original_text('primal order'/'5ED', 'During each player\'s upkeep, Primal Order deals to that player an amount of damage equal to the number of nonbasic lands he or she controls.').
card_image_name('primal order'/'5ED', 'primal order').
card_uid('primal order'/'5ED', '5ED:Primal Order:primal order').
card_rarity('primal order'/'5ED', 'Rare').
card_artist('primal order'/'5ED', 'David A. Cherry').
card_flavor_text('primal order'/'5ED', '\"My sorrow: to dream of simple times, and wake in mine . . . .\"\n—Ola DaRiol, \"Regrets\"').
card_multiverse_id('primal order'/'5ED', '4001').

card_in_set('primordial ooze', '5ED').
card_original_type('primordial ooze'/'5ED', 'Summon — Ooze').
card_original_text('primordial ooze'/'5ED', 'Primordial Ooze attacks each turn if able.\nDuring your upkeep, put a +1/+1 counter on Primordial Ooze. Then pay {X}, where X is equal to the number of these counters on Primordial Ooze, or tap Primordial Ooze and it deals X damage to you.').
card_image_name('primordial ooze'/'5ED', 'primordial ooze').
card_uid('primordial ooze'/'5ED', '5ED:Primordial Ooze:primordial ooze').
card_rarity('primordial ooze'/'5ED', 'Uncommon').
card_artist('primordial ooze'/'5ED', 'Randy Gallegos').
card_multiverse_id('primordial ooze'/'5ED', '4082').

card_in_set('prismatic ward', '5ED').
card_original_type('prismatic ward'/'5ED', 'Enchant Creature').
card_original_text('prismatic ward'/'5ED', 'When you play Prismatic Ward, choose a color.\nAll damage dealt to enchanted creature by sources of the chosen color is reduced to 0.').
card_image_name('prismatic ward'/'5ED', 'prismatic ward').
card_uid('prismatic ward'/'5ED', '5ED:Prismatic Ward:prismatic ward').
card_rarity('prismatic ward'/'5ED', 'Common').
card_artist('prismatic ward'/'5ED', 'Zina Saunders').
card_multiverse_id('prismatic ward'/'5ED', '4150').

card_in_set('prodigal sorcerer', '5ED').
card_original_type('prodigal sorcerer'/'5ED', 'Summon — Wizard').
card_original_text('prodigal sorcerer'/'5ED', '{T}: Prodigal Sorcerer deals 1 damage to target creature or player.').
card_image_name('prodigal sorcerer'/'5ED', 'prodigal sorcerer').
card_uid('prodigal sorcerer'/'5ED', '5ED:Prodigal Sorcerer:prodigal sorcerer').
card_rarity('prodigal sorcerer'/'5ED', 'Common').
card_artist('prodigal sorcerer'/'5ED', 'Douglas Shuler').
card_flavor_text('prodigal sorcerer'/'5ED', 'Occasionally members of the Institute of Arcane Study acquire a taste for worldly pleasures. Seldom do they have trouble finding employment.').
card_multiverse_id('prodigal sorcerer'/'5ED', '3933').

card_in_set('psychic venom', '5ED').
card_original_type('psychic venom'/'5ED', 'Enchant Land').
card_original_text('psychic venom'/'5ED', 'Whenever enchanted land becomes tapped, Psychic Venom deals 2 damage to that land\'s controller.').
card_image_name('psychic venom'/'5ED', 'psychic venom').
card_uid('psychic venom'/'5ED', '5ED:Psychic Venom:psychic venom').
card_rarity('psychic venom'/'5ED', 'Common').
card_artist('psychic venom'/'5ED', 'Brian Snõddy').
card_multiverse_id('psychic venom'/'5ED', '3934').

card_in_set('pyroblast', '5ED').
card_original_type('pyroblast'/'5ED', 'Interrupt').
card_original_text('pyroblast'/'5ED', 'Counter target spell if it is blue, or destroy target permanent if it is blue. (If this spell targets a permanent, play it as an instant.)').
card_image_name('pyroblast'/'5ED', 'pyroblast').
card_uid('pyroblast'/'5ED', '5ED:Pyroblast:pyroblast').
card_rarity('pyroblast'/'5ED', 'Uncommon').
card_artist('pyroblast'/'5ED', 'Kaja Foglio').
card_flavor_text('pyroblast'/'5ED', '\"Just the thing for those pesky water mages.\"\n—Jaya Ballard, task mage').
card_multiverse_id('pyroblast'/'5ED', '4083').

card_in_set('pyrotechnics', '5ED').
card_original_type('pyrotechnics'/'5ED', 'Sorcery').
card_original_text('pyrotechnics'/'5ED', 'Pyrotechnics deals 4 damage divided any way you choose among any number of target creatures and/or players.').
card_image_name('pyrotechnics'/'5ED', 'pyrotechnics').
card_uid('pyrotechnics'/'5ED', '5ED:Pyrotechnics:pyrotechnics').
card_rarity('pyrotechnics'/'5ED', 'Uncommon').
card_artist('pyrotechnics'/'5ED', 'Anson Maddocks').
card_flavor_text('pyrotechnics'/'5ED', '\"Hi! ni! ya! Behold the man of flint, that\'s me! / Four lightnings zigzag from me, strike and return.\"\n—Navajo war chant').
card_multiverse_id('pyrotechnics'/'5ED', '4084').

card_in_set('rabid wombat', '5ED').
card_original_type('rabid wombat'/'5ED', 'Summon — Wombat').
card_original_text('rabid wombat'/'5ED', 'Attacking does not cause Rabid Wombat to tap.\nRabid Wombat gets +2/+2 for each creature enchantment on it.').
card_image_name('rabid wombat'/'5ED', 'rabid wombat').
card_uid('rabid wombat'/'5ED', '5ED:Rabid Wombat:rabid wombat').
card_rarity('rabid wombat'/'5ED', 'Uncommon').
card_artist('rabid wombat'/'5ED', 'Kaja Foglio').
card_multiverse_id('rabid wombat'/'5ED', '4002').

card_in_set('radjan spirit', '5ED').
card_original_type('radjan spirit'/'5ED', 'Summon — Spirit').
card_original_text('radjan spirit'/'5ED', '{T}: Target creature loses flying until end of turn.').
card_image_name('radjan spirit'/'5ED', 'radjan spirit').
card_uid('radjan spirit'/'5ED', '5ED:Radjan Spirit:radjan spirit').
card_rarity('radjan spirit'/'5ED', 'Uncommon').
card_artist('radjan spirit'/'5ED', 'Christopher Rush').
card_flavor_text('radjan spirit'/'5ED', '\"Crawing, crawing,\nFor my crowse crawing,\nI lost the best feather i\' my wing\nFor my crowse crawing.\"\n—Anonymous Scottish ballad').
card_multiverse_id('radjan spirit'/'5ED', '4003').

card_in_set('rag man', '5ED').
card_original_type('rag man'/'5ED', 'Summon — Rag Man').
card_original_text('rag man'/'5ED', '{B}{B}{B}, {T}: Look at target opponent\'s hand. That player discards a creature card at random. Use this ability only during your turn.').
card_image_name('rag man'/'5ED', 'rag man').
card_uid('rag man'/'5ED', '5ED:Rag Man:rag man').
card_rarity('rag man'/'5ED', 'Rare').
card_artist('rag man'/'5ED', 'Daniel Gelon').
card_flavor_text('rag man'/'5ED', '\"Aw, he\'s just a silly, dirty little man. What\'s to be afraid of?\"').
card_multiverse_id('rag man'/'5ED', '3873').

card_in_set('raise dead', '5ED').
card_original_type('raise dead'/'5ED', 'Sorcery').
card_original_text('raise dead'/'5ED', 'Return target creature card in your graveyard to your hand.').
card_image_name('raise dead'/'5ED', 'raise dead').
card_uid('raise dead'/'5ED', '5ED:Raise Dead:raise dead').
card_rarity('raise dead'/'5ED', 'Common').
card_artist('raise dead'/'5ED', 'David Seeley').
card_multiverse_id('raise dead'/'5ED', '3874').

card_in_set('ray of command', '5ED').
card_original_type('ray of command'/'5ED', 'Instant').
card_original_text('ray of command'/'5ED', 'Untap target creature an opponent controls and gain control of it until end of turn. That creature is unaffected by summoning sickness this turn. Tap the creature if you lose control of it at end of this turn.').
card_image_name('ray of command'/'5ED', 'ray of command').
card_uid('ray of command'/'5ED', '5ED:Ray of Command:ray of command').
card_rarity('ray of command'/'5ED', 'Common').
card_artist('ray of command'/'5ED', 'Harold McNeill').
card_multiverse_id('ray of command'/'5ED', '3935').

card_in_set('recall', '5ED').
card_original_type('recall'/'5ED', 'Sorcery').
card_original_text('recall'/'5ED', 'Choose and discard X cards: Return X target cards in your graveyard to your hand. Remove Recall from the game.').
card_image_name('recall'/'5ED', 'recall').
card_uid('recall'/'5ED', '5ED:Recall:recall').
card_rarity('recall'/'5ED', 'Rare').
card_artist('recall'/'5ED', 'Richard Kane Ferguson').
card_multiverse_id('recall'/'5ED', '3936').

card_in_set('reef pirates', '5ED').
card_original_type('reef pirates'/'5ED', 'Summon — Ships').
card_original_text('reef pirates'/'5ED', 'If Reef Pirates damages any opponent, put the top card of that player\'s library into his or her graveyard.').
card_image_name('reef pirates'/'5ED', 'reef pirates').
card_uid('reef pirates'/'5ED', '5ED:Reef Pirates:reef pirates').
card_rarity('reef pirates'/'5ED', 'Common').
card_artist('reef pirates'/'5ED', 'Tom Wänerstrand').
card_flavor_text('reef pirates'/'5ED', 'Relentless as the tides, souls dark as a moonless night. Bloodshed on their minds, and greed burning in their hearts like molten gold.').
card_multiverse_id('reef pirates'/'5ED', '3937').

card_in_set('regeneration', '5ED').
card_original_type('regeneration'/'5ED', 'Enchant Creature').
card_original_text('regeneration'/'5ED', '{G} Regenerate enchanted creature.').
card_image_name('regeneration'/'5ED', 'regeneration').
card_uid('regeneration'/'5ED', '5ED:Regeneration:regeneration').
card_rarity('regeneration'/'5ED', 'Common').
card_artist('regeneration'/'5ED', 'Quinton Hoover').
card_multiverse_id('regeneration'/'5ED', '4004').

card_in_set('remove soul', '5ED').
card_original_type('remove soul'/'5ED', 'Interrupt').
card_original_text('remove soul'/'5ED', 'Counter target summon spell.').
card_image_name('remove soul'/'5ED', 'remove soul').
card_uid('remove soul'/'5ED', '5ED:Remove Soul:remove soul').
card_rarity('remove soul'/'5ED', 'Common').
card_artist('remove soul'/'5ED', 'Mike Dringenberg').
card_flavor_text('remove soul'/'5ED', 'Nethya stiffened suddenly, head cocked as if straining to hear some distant sound, then fell lifeless to the ground.').
card_multiverse_id('remove soul'/'5ED', '3938').

card_in_set('repentant blacksmith', '5ED').
card_original_type('repentant blacksmith'/'5ED', 'Summon — Smith').
card_original_text('repentant blacksmith'/'5ED', 'Protection from red').
card_image_name('repentant blacksmith'/'5ED', 'repentant blacksmith').
card_uid('repentant blacksmith'/'5ED', '5ED:Repentant Blacksmith:repentant blacksmith').
card_rarity('repentant blacksmith'/'5ED', 'Common').
card_artist('repentant blacksmith'/'5ED', 'Drew Tucker').
card_flavor_text('repentant blacksmith'/'5ED', '\"For my confession they burned me with fire / And found that I was for endurance made.\"\n—The Arabian Nights,\ntrans. Haddawy').
card_multiverse_id('repentant blacksmith'/'5ED', '4151').

card_in_set('reverse damage', '5ED').
card_original_type('reverse damage'/'5ED', 'Instant').
card_original_text('reverse damage'/'5ED', 'All damage dealt to you so far this turn from one source is retroactively added to your life total instead of subtracted. Treat further damage from that source normally.').
card_image_name('reverse damage'/'5ED', 'reverse damage').
card_uid('reverse damage'/'5ED', '5ED:Reverse Damage:reverse damage').
card_rarity('reverse damage'/'5ED', 'Rare').
card_artist('reverse damage'/'5ED', 'Thomas Gianni').
card_multiverse_id('reverse damage'/'5ED', '4152').

card_in_set('righteousness', '5ED').
card_original_type('righteousness'/'5ED', 'Instant').
card_original_text('righteousness'/'5ED', 'Target blocking creature gets +7/+7 until end of turn.').
card_image_name('righteousness'/'5ED', 'righteousness').
card_uid('righteousness'/'5ED', '5ED:Righteousness:righteousness').
card_rarity('righteousness'/'5ED', 'Rare').
card_artist('righteousness'/'5ED', 'Mike Dringenberg').
card_flavor_text('righteousness'/'5ED', '\"I too shall be brought low by death, but until then let me win glory.\"\n—Homer, Iliad, Book XVIII').
card_multiverse_id('righteousness'/'5ED', '4153').

card_in_set('rod of ruin', '5ED').
card_original_type('rod of ruin'/'5ED', 'Artifact').
card_original_text('rod of ruin'/'5ED', '{3}, {T}: Rod of Ruin deals 1 damage to target creature or player.').
card_image_name('rod of ruin'/'5ED', 'rod of ruin').
card_uid('rod of ruin'/'5ED', '5ED:Rod of Ruin:rod of ruin').
card_rarity('rod of ruin'/'5ED', 'Uncommon').
card_artist('rod of ruin'/'5ED', 'Christopher Rush').
card_multiverse_id('rod of ruin'/'5ED', '3808').

card_in_set('ruins of trokair', '5ED').
card_original_type('ruins of trokair'/'5ED', 'Land').
card_original_text('ruins of trokair'/'5ED', 'Ruins of Trokair comes into play tapped.\n{T}: Add {W} to your mana pool.\n{T}, Sacrifice Ruins of Trokair: Add {W}{W} to your mana pool.').
card_image_name('ruins of trokair'/'5ED', 'ruins of trokair').
card_uid('ruins of trokair'/'5ED', '5ED:Ruins of Trokair:ruins of trokair').
card_rarity('ruins of trokair'/'5ED', 'Uncommon').
card_artist('ruins of trokair'/'5ED', 'Liz Danforth').
card_multiverse_id('ruins of trokair'/'5ED', '4187').

card_in_set('sabretooth tiger', '5ED').
card_original_type('sabretooth tiger'/'5ED', 'Summon — Tiger').
card_original_text('sabretooth tiger'/'5ED', 'First strike').
card_image_name('sabretooth tiger'/'5ED', 'sabretooth tiger').
card_uid('sabretooth tiger'/'5ED', '5ED:Sabretooth Tiger:sabretooth tiger').
card_rarity('sabretooth tiger'/'5ED', 'Common').
card_artist('sabretooth tiger'/'5ED', 'Melissa A. Benson').
card_flavor_text('sabretooth tiger'/'5ED', '\"I fear anything with teeth measured in handspans!\"\n—Norin the Wary').
card_multiverse_id('sabretooth tiger'/'5ED', '4085').

card_in_set('sacred boon', '5ED').
card_original_type('sacred boon'/'5ED', 'Instant').
card_original_text('sacred boon'/'5ED', 'Prevent up to 3 damage to target creature. At end of turn, put a +0/+1 counter on that creature for each 1 damage prevented in this way.').
card_image_name('sacred boon'/'5ED', 'sacred boon').
card_uid('sacred boon'/'5ED', '5ED:Sacred Boon:sacred boon').
card_rarity('sacred boon'/'5ED', 'Uncommon').
card_artist('sacred boon'/'5ED', 'Mike Raabe').
card_flavor_text('sacred boon'/'5ED', '\"Divine gifts are granted to those who are worthy.\"\n—Halvor Arenson, Kjeldoran priest').
card_multiverse_id('sacred boon'/'5ED', '4154').

card_in_set('samite healer', '5ED').
card_original_type('samite healer'/'5ED', 'Summon — Cleric').
card_original_text('samite healer'/'5ED', '{T}: Prevent 1 damage to any creature or player.').
card_image_name('samite healer'/'5ED', 'samite healer').
card_uid('samite healer'/'5ED', '5ED:Samite Healer:samite healer').
card_rarity('samite healer'/'5ED', 'Common').
card_artist('samite healer'/'5ED', 'Tom Wänerstrand').
card_flavor_text('samite healer'/'5ED', 'Healers ultimately acquire the divine gifts of spiritual and physical wholeness. The most devout are also granted the ability to pass physical wholeness on to others.').
card_multiverse_id('samite healer'/'5ED', '4155').

card_in_set('sand silos', '5ED').
card_original_type('sand silos'/'5ED', 'Land').
card_original_text('sand silos'/'5ED', 'Sand Silos comes into play tapped.\nYou may choose not to untap Sand Silos during your untap phase and put a storage counter on it instead.\n{T}, Remove X storage counters from Sand Silos: Add an amount of {U} equal to X to your mana pool.').
card_image_name('sand silos'/'5ED', 'sand silos').
card_uid('sand silos'/'5ED', '5ED:Sand Silos:sand silos').
card_rarity('sand silos'/'5ED', 'Rare').
card_artist('sand silos'/'5ED', 'David Seeley').
card_multiverse_id('sand silos'/'5ED', '4188').

card_in_set('scaled wurm', '5ED').
card_original_type('scaled wurm'/'5ED', 'Summon — Wurm').
card_original_text('scaled wurm'/'5ED', '').
card_image_name('scaled wurm'/'5ED', 'scaled wurm').
card_uid('scaled wurm'/'5ED', '5ED:Scaled Wurm:scaled wurm').
card_rarity('scaled wurm'/'5ED', 'Common').
card_artist('scaled wurm'/'5ED', 'Daniel Gelon').
card_flavor_text('scaled wurm'/'5ED', '\"Flourishing during the Ice Age, these\nwurms were the bane of all Kjeldorans. Their great size and ferocity made them the subject of countless nightmares—they embodied the worst of the Ice Age.\"\n—Kjeldor: Ice Civilization').
card_multiverse_id('scaled wurm'/'5ED', '4005').

card_in_set('scathe zombies', '5ED').
card_original_type('scathe zombies'/'5ED', 'Summon — Zombies').
card_original_text('scathe zombies'/'5ED', '').
card_image_name('scathe zombies'/'5ED', 'scathe zombies').
card_uid('scathe zombies'/'5ED', '5ED:Scathe Zombies:scathe zombies').
card_rarity('scathe zombies'/'5ED', 'Common').
card_artist('scathe zombies'/'5ED', 'Tom Kyffin').
card_flavor_text('scathe zombies'/'5ED', '\"They groaned, they stirred, they all uprose,\nNor Spake, nor moved their eyes;\nIt had been strange, even in a dream,\nTo have seen those dead men rise.\"\n—Samuel Coleridge,\n\"The Rime of the Ancient Mariner\"').
card_multiverse_id('scathe zombies'/'5ED', '3875').

card_in_set('scavenger folk', '5ED').
card_original_type('scavenger folk'/'5ED', 'Summon — Scavenger Folk').
card_original_text('scavenger folk'/'5ED', '{G}, {T}, Sacrifice Scavenger Folk: Destroy target artifact.').
card_image_name('scavenger folk'/'5ED', 'scavenger folk').
card_uid('scavenger folk'/'5ED', '5ED:Scavenger Folk:scavenger folk').
card_rarity('scavenger folk'/'5ED', 'Common').
card_artist('scavenger folk'/'5ED', 'Jeff Miracola').
card_flavor_text('scavenger folk'/'5ED', '\"Stoop, pick, turn, toss.\nThis is the way we mourn our loss.\"\n—Scavenger dirge').
card_multiverse_id('scavenger folk'/'5ED', '4006').

card_in_set('scryb sprites', '5ED').
card_original_type('scryb sprites'/'5ED', 'Summon — Faeries').
card_original_text('scryb sprites'/'5ED', 'Flying').
card_image_name('scryb sprites'/'5ED', 'scryb sprites').
card_uid('scryb sprites'/'5ED', '5ED:Scryb Sprites:scryb sprites').
card_rarity('scryb sprites'/'5ED', 'Common').
card_artist('scryb sprites'/'5ED', 'Amy Weber').
card_flavor_text('scryb sprites'/'5ED', 'The only sound was the gentle clicking of the faeries\' wings. Then those intruders who were still standing turned and fled. One thing was certain: they didn\'t think the Scryb were very funny anymore.').
card_multiverse_id('scryb sprites'/'5ED', '4007').

card_in_set('sea serpent', '5ED').
card_original_type('sea serpent'/'5ED', 'Summon — Serpent').
card_original_text('sea serpent'/'5ED', 'Islandhome (If defending player controls no islands, this creature cannot attack. If you control no islands, bury this creature.)').
card_image_name('sea serpent'/'5ED', 'sea serpent').
card_uid('sea serpent'/'5ED', '5ED:Sea Serpent:sea serpent').
card_rarity('sea serpent'/'5ED', 'Common').
card_artist('sea serpent'/'5ED', 'Ian Miller').
card_flavor_text('sea serpent'/'5ED', 'Legend has it that serpents used to be bigger, but how could that be?').
card_multiverse_id('sea serpent'/'5ED', '3939').

card_in_set('sea spirit', '5ED').
card_original_type('sea spirit'/'5ED', 'Summon — Spirit').
card_original_text('sea spirit'/'5ED', '{U} +1/+0 until end of turn').
card_image_name('sea spirit'/'5ED', 'sea spirit').
card_uid('sea spirit'/'5ED', '5ED:Sea Spirit:sea spirit').
card_rarity('sea spirit'/'5ED', 'Uncommon').
card_artist('sea spirit'/'5ED', 'DiTerlizzi').
card_flavor_text('sea spirit'/'5ED', '\"It rose above our heads, above the ship, and still higher yet. No foggy, ice-laden sea in the world could frighten me more.\"\n—General Jarkeld, the Arctic Fox').
card_multiverse_id('sea spirit'/'5ED', '3940').

card_in_set('sea sprite', '5ED').
card_original_type('sea sprite'/'5ED', 'Summon — Faerie').
card_original_text('sea sprite'/'5ED', 'Flying, protection from red').
card_image_name('sea sprite'/'5ED', 'sea sprite').
card_uid('sea sprite'/'5ED', '5ED:Sea Sprite:sea sprite').
card_rarity('sea sprite'/'5ED', 'Uncommon').
card_artist('sea sprite'/'5ED', 'Rebecca Guay').
card_flavor_text('sea sprite'/'5ED', '\"No one can catch what won\'t be caught.\"\n—Kakra, sea troll').
card_multiverse_id('sea sprite'/'5ED', '3941').

card_in_set('seasinger', '5ED').
card_original_type('seasinger'/'5ED', 'Summon — Merfolk').
card_original_text('seasinger'/'5ED', 'If you control no islands, bury Seasinger.\nYou may choose not to untap Seasinger during your untap phase.\n{T}: Gain control of target creature whose controller controls any islands as long as you control Seasinger and Seasinger remains tapped.').
card_image_name('seasinger'/'5ED', 'seasinger').
card_uid('seasinger'/'5ED', '5ED:Seasinger:seasinger').
card_rarity('seasinger'/'5ED', 'Uncommon').
card_artist('seasinger'/'5ED', 'John Matson').
card_multiverse_id('seasinger'/'5ED', '3942').

card_in_set('segovian leviathan', '5ED').
card_original_type('segovian leviathan'/'5ED', 'Summon — Leviathan').
card_original_text('segovian leviathan'/'5ED', 'Islandwalk (If defending player controls any islands, this creature is unblockable.)').
card_image_name('segovian leviathan'/'5ED', 'segovian leviathan').
card_uid('segovian leviathan'/'5ED', '5ED:Segovian Leviathan:segovian leviathan').
card_rarity('segovian leviathan'/'5ED', 'Uncommon').
card_artist('segovian leviathan'/'5ED', 'Melissa A. Benson').
card_flavor_text('segovian leviathan'/'5ED', '\"Leviathan, too! Can you catch him with a fish-hook or run a line round his tongue?\" —The Bible, Job 41:1').
card_multiverse_id('segovian leviathan'/'5ED', '3943').

card_in_set('sengir autocrat', '5ED').
card_original_type('sengir autocrat'/'5ED', 'Summon — Autocrat').
card_original_text('sengir autocrat'/'5ED', 'When Sengir Autocrat comes into play, put three Serf tokens into play. Treat these tokens as 0/1 black creatures.\nIf Sengir Autocrat leaves play, bury all Serf tokens.').
card_image_name('sengir autocrat'/'5ED', 'sengir autocrat').
card_uid('sengir autocrat'/'5ED', '5ED:Sengir Autocrat:sengir autocrat').
card_rarity('sengir autocrat'/'5ED', 'Rare').
card_artist('sengir autocrat'/'5ED', 'David A. Cherry').
card_flavor_text('sengir autocrat'/'5ED', '\"Evil is relative. Do sheep think any better of the shepherd?\"\n—Baron Sengir').
card_multiverse_id('sengir autocrat'/'5ED', '3876').

card_in_set('seraph', '5ED').
card_original_type('seraph'/'5ED', 'Summon — Angel').
card_original_text('seraph'/'5ED', 'Flying\nWhenever any creature Seraph damaged this turn is put into any graveyard, put that creature into play under your control at end of turn. Bury the creature if you lose control of Seraph.').
card_image_name('seraph'/'5ED', 'seraph').
card_uid('seraph'/'5ED', '5ED:Seraph:seraph').
card_rarity('seraph'/'5ED', 'Rare').
card_artist('seraph'/'5ED', 'D. Alexander Gregory').
card_multiverse_id('seraph'/'5ED', '4156').

card_in_set('serpent generator', '5ED').
card_original_type('serpent generator'/'5ED', 'Artifact').
card_original_text('serpent generator'/'5ED', '{4}, {T}: Put a Poison Snake token into play. Treat this token as a 1/1 artifact creature. If any Poison Snake damages any player, he or she gets a poison counter. If any player has ten or more poison counters, he or she loses the game.').
card_image_name('serpent generator'/'5ED', 'serpent generator').
card_uid('serpent generator'/'5ED', '5ED:Serpent Generator:serpent generator').
card_rarity('serpent generator'/'5ED', 'Rare').
card_artist('serpent generator'/'5ED', 'Mark Tedin').
card_multiverse_id('serpent generator'/'5ED', '3809').

card_in_set('serra bestiary', '5ED').
card_original_type('serra bestiary'/'5ED', 'Enchant Creature').
card_original_text('serra bestiary'/'5ED', 'During your upkeep, pay {W}{W} or bury Serra Bestiary.\nEnchanted creature cannot attack, block, or play any ability that includes {T} in its activation cost.').
card_image_name('serra bestiary'/'5ED', 'serra bestiary').
card_uid('serra bestiary'/'5ED', '5ED:Serra Bestiary:serra bestiary').
card_rarity('serra bestiary'/'5ED', 'Uncommon').
card_artist('serra bestiary'/'5ED', 'Steve Luke').
card_multiverse_id('serra bestiary'/'5ED', '4157').

card_in_set('serra paladin', '5ED').
card_original_type('serra paladin'/'5ED', 'Summon — Paladin').
card_original_text('serra paladin'/'5ED', '{T}: Prevent 1 damage to any creature or player.\n{1}{W}{W}, {T}: Attacking this turn does not cause target creature to tap.').
card_image_name('serra paladin'/'5ED', 'serra paladin').
card_uid('serra paladin'/'5ED', '5ED:Serra Paladin:serra paladin').
card_rarity('serra paladin'/'5ED', 'Uncommon').
card_artist('serra paladin'/'5ED', 'Pete Venters').
card_flavor_text('serra paladin'/'5ED', 'One light, one blade, one purpose.\n—Vow of the Serra Paladins').
card_multiverse_id('serra paladin'/'5ED', '4158').

card_in_set('shanodin dryads', '5ED').
card_original_type('shanodin dryads'/'5ED', 'Summon — Nymphs').
card_original_text('shanodin dryads'/'5ED', 'Forestwalk (If defending player controls any forests, this creature is unblockable.)').
card_image_name('shanodin dryads'/'5ED', 'shanodin dryads').
card_uid('shanodin dryads'/'5ED', '5ED:Shanodin Dryads:shanodin dryads').
card_rarity('shanodin dryads'/'5ED', 'Common').
card_artist('shanodin dryads'/'5ED', 'Gary Leach').
card_flavor_text('shanodin dryads'/'5ED', 'Moving without sound, swift figures pass through branches and undergrowth completely unhindered. One with the trees around them, the dryads of Shanodin Forest are seen only when they wish to be.').
card_multiverse_id('shanodin dryads'/'5ED', '4008').

card_in_set('shapeshifter', '5ED').
card_original_type('shapeshifter'/'5ED', 'Artifact Creature').
card_original_text('shapeshifter'/'5ED', 'Shapeshifter has total power and toughness of 7, divided any way you choose, though neither can be more than 7.\nWhen you play Shapeshifter, choose its power and toughness.\nDuring your upkeep, choose Shapeshifter\'s power and toughness.').
card_image_name('shapeshifter'/'5ED', 'shapeshifter').
card_uid('shapeshifter'/'5ED', '5ED:Shapeshifter:shapeshifter').
card_rarity('shapeshifter'/'5ED', 'Uncommon').
card_artist('shapeshifter'/'5ED', 'Adrian Smith').
card_multiverse_id('shapeshifter'/'5ED', '3810').

card_in_set('shatter', '5ED').
card_original_type('shatter'/'5ED', 'Instant').
card_original_text('shatter'/'5ED', 'Destroy target artifact.').
card_image_name('shatter'/'5ED', 'shatter').
card_uid('shatter'/'5ED', '5ED:Shatter:shatter').
card_rarity('shatter'/'5ED', 'Common').
card_artist('shatter'/'5ED', 'Hannibal King').
card_multiverse_id('shatter'/'5ED', '4086').

card_in_set('shatterstorm', '5ED').
card_original_type('shatterstorm'/'5ED', 'Sorcery').
card_original_text('shatterstorm'/'5ED', 'Bury all artifacts.').
card_image_name('shatterstorm'/'5ED', 'shatterstorm').
card_uid('shatterstorm'/'5ED', '5ED:Shatterstorm:shatterstorm').
card_rarity('shatterstorm'/'5ED', 'Uncommon').
card_artist('shatterstorm'/'5ED', 'James Allen').
card_flavor_text('shatterstorm'/'5ED', 'Chains of leaping fire and sizzling lightning laid waste the artificers\' handiwork, sparing not a single device.').
card_multiverse_id('shatterstorm'/'5ED', '4087').

card_in_set('shield bearer', '5ED').
card_original_type('shield bearer'/'5ED', 'Summon — Soldier').
card_original_text('shield bearer'/'5ED', 'Banding').
card_image_name('shield bearer'/'5ED', 'shield bearer').
card_uid('shield bearer'/'5ED', '5ED:Shield Bearer:shield bearer').
card_rarity('shield bearer'/'5ED', 'Common').
card_artist('shield bearer'/'5ED', 'Dan Frazier').
card_flavor_text('shield bearer'/'5ED', '\"You have almost completed your four years, my son. Soon you shall be a skyknight.\"\n—Arna Kennerüd, skyknight').
card_multiverse_id('shield bearer'/'5ED', '4159').

card_in_set('shield wall', '5ED').
card_original_type('shield wall'/'5ED', 'Instant').
card_original_text('shield wall'/'5ED', 'All creatures you control get +0/+2 until end of turn.').
card_image_name('shield wall'/'5ED', 'shield wall').
card_uid('shield wall'/'5ED', '5ED:Shield Wall:shield wall').
card_rarity('shield wall'/'5ED', 'Common').
card_artist('shield wall'/'5ED', 'Scott Kirschner').
card_flavor_text('shield wall'/'5ED', 'The strongest walls are made not of stone or steel but of the focused strength of warriors.').
card_multiverse_id('shield wall'/'5ED', '4160').

card_in_set('shivan dragon', '5ED').
card_original_type('shivan dragon'/'5ED', 'Summon — Dragon').
card_original_text('shivan dragon'/'5ED', 'Flying\n{R} +1/+0 until end of turn').
card_image_name('shivan dragon'/'5ED', 'shivan dragon').
card_uid('shivan dragon'/'5ED', '5ED:Shivan Dragon:shivan dragon').
card_rarity('shivan dragon'/'5ED', 'Rare').
card_artist('shivan dragon'/'5ED', 'Melissa A. Benson').
card_flavor_text('shivan dragon'/'5ED', 'While it\'s true most dragons are cruel, the Shivan dragon seems to take particular glee in the misery of others, often tormenting its victims much like a cat plays with a mouse before delivering the final blow.').
card_multiverse_id('shivan dragon'/'5ED', '4088').

card_in_set('shrink', '5ED').
card_original_type('shrink'/'5ED', 'Instant').
card_original_text('shrink'/'5ED', 'Target creature gets -5/-0 until end of turn.').
card_image_name('shrink'/'5ED', 'shrink').
card_uid('shrink'/'5ED', '5ED:Shrink:shrink').
card_rarity('shrink'/'5ED', 'Common').
card_artist('shrink'/'5ED', 'Liz Danforth').
card_flavor_text('shrink'/'5ED', '\"May you decrease like coal upon the hearth . . . Evaporate like water in a pail, Become as little as a linseed-grain . . . And so diminish that you come to nothing.\"\n—Anglo-Saxon curse').
card_multiverse_id('shrink'/'5ED', '4009').

card_in_set('sibilant spirit', '5ED').
card_original_type('sibilant spirit'/'5ED', 'Summon — Spirit').
card_original_text('sibilant spirit'/'5ED', 'Flying\nIf Sibilant Spirit attacks, defending player may draw a card.').
card_image_name('sibilant spirit'/'5ED', 'sibilant spirit').
card_uid('sibilant spirit'/'5ED', '5ED:Sibilant Spirit:sibilant spirit').
card_rarity('sibilant spirit'/'5ED', 'Rare').
card_artist('sibilant spirit'/'5ED', 'Ron Spencer').
card_flavor_text('sibilant spirit'/'5ED', 'She had expected death to roar, to thunder, to growl. She did not recognize it when it came hissing to her side.').
card_multiverse_id('sibilant spirit'/'5ED', '3944').

card_in_set('skull catapult', '5ED').
card_original_type('skull catapult'/'5ED', 'Artifact').
card_original_text('skull catapult'/'5ED', '{1}, {T}, Sacrifice a creature: Skull Catapult deals 2 damage to target creature or player.').
card_image_name('skull catapult'/'5ED', 'skull catapult').
card_uid('skull catapult'/'5ED', '5ED:Skull Catapult:skull catapult').
card_rarity('skull catapult'/'5ED', 'Uncommon').
card_artist('skull catapult'/'5ED', 'Ian Miller').
card_flavor_text('skull catapult'/'5ED', '\"Let any who doubt the evil of using the ancient devices look at this infernal machine. What manner of fiend would design such a sadistic device?\"\n—Sorine Relicbane, Soldevi heretic').
card_multiverse_id('skull catapult'/'5ED', '3811').

card_in_set('sleight of mind', '5ED').
card_original_type('sleight of mind'/'5ED', 'Interrupt').
card_original_text('sleight of mind'/'5ED', 'Change the text of target permanent or spell by replacing all instances of one color word with another. (For example, you may change \"nongreen creature\" to \"nonred creature.\" If this spell targets a permanent, play it as an instant.)').
card_image_name('sleight of mind'/'5ED', 'sleight of mind').
card_uid('sleight of mind'/'5ED', '5ED:Sleight of Mind:sleight of mind').
card_rarity('sleight of mind'/'5ED', 'Rare').
card_artist('sleight of mind'/'5ED', 'Mark Poole').
card_multiverse_id('sleight of mind'/'5ED', '3945').

card_in_set('smoke', '5ED').
card_original_type('smoke'/'5ED', 'Enchantment').
card_original_text('smoke'/'5ED', 'Players cannot untap more than one creature during their untap phases.').
card_image_name('smoke'/'5ED', 'smoke').
card_uid('smoke'/'5ED', '5ED:Smoke:smoke').
card_rarity('smoke'/'5ED', 'Rare').
card_artist('smoke'/'5ED', 'Tom Kyffin').
card_multiverse_id('smoke'/'5ED', '4089').

card_in_set('sorceress queen', '5ED').
card_original_type('sorceress queen'/'5ED', 'Summon — Sorceress').
card_original_text('sorceress queen'/'5ED', '{T}: Target creature other than Sorceress Queen is 0/2 until end of turn.').
card_image_name('sorceress queen'/'5ED', 'sorceress queen').
card_uid('sorceress queen'/'5ED', '5ED:Sorceress Queen:sorceress queen').
card_rarity('sorceress queen'/'5ED', 'Rare').
card_artist('sorceress queen'/'5ED', 'Kaja Foglio').
card_flavor_text('sorceress queen'/'5ED', '\"We have always counted time with sand; this pinch is your meaningless life.\"\n—Nailah, self-proclaimed Queen of Rabiah').
card_multiverse_id('sorceress queen'/'5ED', '3877').

card_in_set('soul barrier', '5ED').
card_original_type('soul barrier'/'5ED', 'Enchantment').
card_original_text('soul barrier'/'5ED', 'Whenever target opponent successfully casts a summon spell, Soul Barrier deals 2 damage to him or her. That player may pay {2} to prevent this damage.').
card_image_name('soul barrier'/'5ED', 'soul barrier').
card_uid('soul barrier'/'5ED', '5ED:Soul Barrier:soul barrier').
card_rarity('soul barrier'/'5ED', 'Common').
card_artist('soul barrier'/'5ED', 'Harold McNeill').
card_flavor_text('soul barrier'/'5ED', '\"The Soul selects her own Society—\nThen—shuts the Door—\"\n—Emily Dickinson,\n\"The Soul selects her own Society\"').
card_multiverse_id('soul barrier'/'5ED', '3946').

card_in_set('soul net', '5ED').
card_original_type('soul net'/'5ED', 'Artifact').
card_original_text('soul net'/'5ED', '{1}: Gain 1 life. Use this ability only when a creature is put into any graveyard from play and only once for each such creature.').
card_image_name('soul net'/'5ED', 'soul net').
card_uid('soul net'/'5ED', '5ED:Soul Net:soul net').
card_rarity('soul net'/'5ED', 'Uncommon').
card_artist('soul net'/'5ED', 'Andrew Robinson').
card_multiverse_id('soul net'/'5ED', '3812').

card_in_set('spell blast', '5ED').
card_original_type('spell blast'/'5ED', 'Interrupt').
card_original_text('spell blast'/'5ED', 'Counter target spell with total casting cost equal to X.').
card_image_name('spell blast'/'5ED', 'spell blast').
card_uid('spell blast'/'5ED', '5ED:Spell Blast:spell blast').
card_rarity('spell blast'/'5ED', 'Common').
card_artist('spell blast'/'5ED', 'Greg Simanson').
card_multiverse_id('spell blast'/'5ED', '3947').

card_in_set('spirit link', '5ED').
card_original_type('spirit link'/'5ED', 'Enchant Creature').
card_original_text('spirit link'/'5ED', 'For each 1 damage enchanted creature deals, gain 1 life.').
card_image_name('spirit link'/'5ED', 'spirit link').
card_uid('spirit link'/'5ED', '5ED:Spirit Link:spirit link').
card_rarity('spirit link'/'5ED', 'Uncommon').
card_artist('spirit link'/'5ED', 'Kaja Foglio').
card_flavor_text('spirit link'/'5ED', '\"I regret feeding Kaervek\'s hunger for power—far better to live with animals innocent of such ambitions.\"\n—Jolrael').
card_multiverse_id('spirit link'/'5ED', '4161').

card_in_set('stampede', '5ED').
card_original_type('stampede'/'5ED', 'Instant').
card_original_text('stampede'/'5ED', 'All attacking creatures get +1/+0 and gain trample until end of turn.').
card_image_name('stampede'/'5ED', 'stampede').
card_uid('stampede'/'5ED', '5ED:Stampede:stampede').
card_rarity('stampede'/'5ED', 'Rare').
card_artist('stampede'/'5ED', 'Jeff A. Menges').
card_flavor_text('stampede'/'5ED', '\"When are beasts like a river?\"\n—The One Thousand Questions').
card_multiverse_id('stampede'/'5ED', '4010').

card_in_set('stasis', '5ED').
card_original_type('stasis'/'5ED', 'Enchantment').
card_original_text('stasis'/'5ED', 'Each player skips his or her untap phase.\nDuring your upkeep, pay {U} or bury Stasis.').
card_image_name('stasis'/'5ED', 'stasis').
card_uid('stasis'/'5ED', '5ED:Stasis:stasis').
card_rarity('stasis'/'5ED', 'Rare').
card_artist('stasis'/'5ED', 'Fay Jones').
card_multiverse_id('stasis'/'5ED', '3948').

card_in_set('steal artifact', '5ED').
card_original_type('steal artifact'/'5ED', 'Enchant Artifact').
card_original_text('steal artifact'/'5ED', 'Gain control of enchanted artifact.').
card_image_name('steal artifact'/'5ED', 'steal artifact').
card_uid('steal artifact'/'5ED', '5ED:Steal Artifact:steal artifact').
card_rarity('steal artifact'/'5ED', 'Uncommon').
card_artist('steal artifact'/'5ED', 'John Coulthart').
card_multiverse_id('steal artifact'/'5ED', '3949').

card_in_set('stone giant', '5ED').
card_original_type('stone giant'/'5ED', 'Summon — Giant').
card_original_text('stone giant'/'5ED', '{T}: Target creature you control with toughness less than Stone Giant\'s power gains flying until end of turn. At end of turn, destroy that creature.').
card_image_name('stone giant'/'5ED', 'stone giant').
card_uid('stone giant'/'5ED', '5ED:Stone Giant:stone giant').
card_rarity('stone giant'/'5ED', 'Uncommon').
card_artist('stone giant'/'5ED', 'James Allen').
card_multiverse_id('stone giant'/'5ED', '4090').

card_in_set('stone rain', '5ED').
card_original_type('stone rain'/'5ED', 'Sorcery').
card_original_text('stone rain'/'5ED', 'Destroy target land.').
card_image_name('stone rain'/'5ED', 'stone rain').
card_uid('stone rain'/'5ED', '5ED:Stone Rain:stone rain').
card_rarity('stone rain'/'5ED', 'Common').
card_artist('stone rain'/'5ED', 'Tony Roberts').
card_multiverse_id('stone rain'/'5ED', '4091').

card_in_set('stone spirit', '5ED').
card_original_type('stone spirit'/'5ED', 'Summon — Spirit').
card_original_text('stone spirit'/'5ED', 'Stone Spirit cannot be blocked by creatures with flying.').
card_image_name('stone spirit'/'5ED', 'stone spirit').
card_uid('stone spirit'/'5ED', '5ED:Stone Spirit:stone spirit').
card_rarity('stone spirit'/'5ED', 'Uncommon').
card_artist('stone spirit'/'5ED', 'James Allen').
card_flavor_text('stone spirit'/'5ED', '\"The spirit of the stone is the spirit of strength.\"\n—Lovisa Coldeyes,\nBalduvian chieftain').
card_multiverse_id('stone spirit'/'5ED', '4092').

card_in_set('stream of life', '5ED').
card_original_type('stream of life'/'5ED', 'Sorcery').
card_original_text('stream of life'/'5ED', 'Target player gains X life.').
card_image_name('stream of life'/'5ED', 'stream of life').
card_uid('stream of life'/'5ED', '5ED:Stream of Life:stream of life').
card_rarity('stream of life'/'5ED', 'Common').
card_artist('stream of life'/'5ED', 'Terese Nielsen').
card_multiverse_id('stream of life'/'5ED', '4011').

card_in_set('stromgald cabal', '5ED').
card_original_type('stromgald cabal'/'5ED', 'Summon — Knights').
card_original_text('stromgald cabal'/'5ED', '{T}, Pay 1 life: Counter target white spell. Play this ability as an interrupt.').
card_image_name('stromgald cabal'/'5ED', 'stromgald cabal').
card_uid('stromgald cabal'/'5ED', '5ED:Stromgald Cabal:stromgald cabal').
card_rarity('stromgald cabal'/'5ED', 'Rare').
card_artist('stromgald cabal'/'5ED', 'Anson Maddocks').
card_flavor_text('stromgald cabal'/'5ED', 'When the alliance between Balduvia and Kjeldor deprived Stromgald of easy victories, its agents were forced to fuel their rituals with their own flesh.').
card_multiverse_id('stromgald cabal'/'5ED', '3878').

card_in_set('sulfurous springs', '5ED').
card_original_type('sulfurous springs'/'5ED', 'Land').
card_original_text('sulfurous springs'/'5ED', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {B} or {R} to your mana pool. Sulfurous Springs deals 1 damage to you.').
card_image_name('sulfurous springs'/'5ED', 'sulfurous springs').
card_uid('sulfurous springs'/'5ED', '5ED:Sulfurous Springs:sulfurous springs').
card_rarity('sulfurous springs'/'5ED', 'Rare').
card_artist('sulfurous springs'/'5ED', 'Jeff Miracola').
card_multiverse_id('sulfurous springs'/'5ED', '4189').

card_in_set('svyelunite temple', '5ED').
card_original_type('svyelunite temple'/'5ED', 'Land').
card_original_text('svyelunite temple'/'5ED', 'Svyelunite Temple comes into play tapped.\n{T}: Add {U} to your mana pool.\n{T}, Sacrifice Svyelunite Temple: Add {U}{U} to your mana pool.').
card_image_name('svyelunite temple'/'5ED', 'svyelunite temple').
card_uid('svyelunite temple'/'5ED', '5ED:Svyelunite Temple:svyelunite temple').
card_rarity('svyelunite temple'/'5ED', 'Uncommon').
card_artist('svyelunite temple'/'5ED', 'Liz Danforth').
card_multiverse_id('svyelunite temple'/'5ED', '4190').

card_in_set('swamp', '5ED').
card_original_type('swamp'/'5ED', 'Land').
card_original_text('swamp'/'5ED', '{T}: Add {B} to your mana pool.').
card_image_name('swamp'/'5ED', 'swamp1').
card_uid('swamp'/'5ED', '5ED:Swamp:swamp1').
card_rarity('swamp'/'5ED', 'Basic Land').
card_artist('swamp'/'5ED', 'Andrew Robinson').
card_multiverse_id('swamp'/'5ED', '4169').

card_in_set('swamp', '5ED').
card_original_type('swamp'/'5ED', 'Land').
card_original_text('swamp'/'5ED', '{T}: Add {B} to your mana pool.').
card_image_name('swamp'/'5ED', 'swamp2').
card_uid('swamp'/'5ED', '5ED:Swamp:swamp2').
card_rarity('swamp'/'5ED', 'Basic Land').
card_artist('swamp'/'5ED', 'Andrew Robinson').
card_multiverse_id('swamp'/'5ED', '4168').

card_in_set('swamp', '5ED').
card_original_type('swamp'/'5ED', 'Land').
card_original_text('swamp'/'5ED', '{T}: Add {B} to your mana pool.').
card_image_name('swamp'/'5ED', 'swamp3').
card_uid('swamp'/'5ED', '5ED:Swamp:swamp3').
card_rarity('swamp'/'5ED', 'Basic Land').
card_artist('swamp'/'5ED', 'Andrew Robinson').
card_multiverse_id('swamp'/'5ED', '4167').

card_in_set('swamp', '5ED').
card_original_type('swamp'/'5ED', 'Land').
card_original_text('swamp'/'5ED', '{T}: Add {B} to your mana pool.').
card_image_name('swamp'/'5ED', 'swamp4').
card_uid('swamp'/'5ED', '5ED:Swamp:swamp4').
card_rarity('swamp'/'5ED', 'Basic Land').
card_artist('swamp'/'5ED', 'Andrew Robinson').
card_multiverse_id('swamp'/'5ED', '4170').

card_in_set('sylvan library', '5ED').
card_original_type('sylvan library'/'5ED', 'Enchantment').
card_original_text('sylvan library'/'5ED', '{0}: Draw two cards, then choose any two cards in your hand drawn this turn. For each of those cards, pay 4 life or put that card back on top of your library. Use this ability only during your draw phase and only once each turn.').
card_image_name('sylvan library'/'5ED', 'sylvan library').
card_uid('sylvan library'/'5ED', '5ED:Sylvan Library:sylvan library').
card_rarity('sylvan library'/'5ED', 'Rare').
card_artist('sylvan library'/'5ED', 'Harold McNeill').
card_multiverse_id('sylvan library'/'5ED', '4012').

card_in_set('tarpan', '5ED').
card_original_type('tarpan'/'5ED', 'Summon — Tarpan').
card_original_text('tarpan'/'5ED', 'If Tarpan is put into any graveyard from play, gain 1 life.').
card_image_name('tarpan'/'5ED', 'tarpan').
card_uid('tarpan'/'5ED', '5ED:Tarpan:tarpan').
card_rarity('tarpan'/'5ED', 'Common').
card_artist('tarpan'/'5ED', 'Margaret Organ-Kean').
card_flavor_text('tarpan'/'5ED', '\"A good tarpan will serve you, faithful and true. A bad one will kick you in the head.\"\n—General Jarkeld, the Arctic Fox').
card_multiverse_id('tarpan'/'5ED', '4013').

card_in_set('tawnos\'s weaponry', '5ED').
card_original_type('tawnos\'s weaponry'/'5ED', 'Artifact').
card_original_text('tawnos\'s weaponry'/'5ED', 'You may choose not to untap Tawnos\'s Weaponry during your untap phase.\n{2}, {T}: Target creature gets +1/+1 as long as Tawnos\'s Weaponry remains tapped.').
card_image_name('tawnos\'s weaponry'/'5ED', 'tawnos\'s weaponry').
card_uid('tawnos\'s weaponry'/'5ED', '5ED:Tawnos\'s Weaponry:tawnos\'s weaponry').
card_rarity('tawnos\'s weaponry'/'5ED', 'Uncommon').
card_artist('tawnos\'s weaponry'/'5ED', 'John Coulthart').
card_flavor_text('tawnos\'s weaponry'/'5ED', 'When war machines became too costly, Tawnos\'s weaponry replaced them.').
card_multiverse_id('tawnos\'s weaponry'/'5ED', '3813').

card_in_set('terror', '5ED').
card_original_type('terror'/'5ED', 'Instant').
card_original_text('terror'/'5ED', 'Bury target nonartifact, nonblack creature.').
card_image_name('terror'/'5ED', 'terror').
card_uid('terror'/'5ED', '5ED:Terror:terror').
card_rarity('terror'/'5ED', 'Common').
card_artist('terror'/'5ED', 'Ron Spencer').
card_multiverse_id('terror'/'5ED', '3879').

card_in_set('the brute', '5ED').
card_original_type('the brute'/'5ED', 'Enchant Creature').
card_original_text('the brute'/'5ED', 'Enchanted creature gets +1/+0.\n{R}{R}{R} Regenerate enchanted creature.').
card_image_name('the brute'/'5ED', 'the brute').
card_uid('the brute'/'5ED', '5ED:The Brute:the brute').
card_rarity('the brute'/'5ED', 'Common').
card_artist('the brute'/'5ED', 'Douglas Shuler').
card_flavor_text('the brute'/'5ED', 'After years of being bullied, Fergo reaped the benefits of his newfound strength: he never met a man he didn\'t want to fight.').
card_multiverse_id('the brute'/'5ED', '4093').

card_in_set('the hive', '5ED').
card_original_type('the hive'/'5ED', 'Artifact').
card_original_text('the hive'/'5ED', '{5}, {T}: Put a Wasp token into play. Treat this token as a 1/1 artifact creature with flying.').
card_image_name('the hive'/'5ED', 'the hive').
card_uid('the hive'/'5ED', '5ED:The Hive:the hive').
card_rarity('the hive'/'5ED', 'Rare').
card_artist('the hive'/'5ED', 'Sandra Everingham').
card_flavor_text('the hive'/'5ED', 'Mouths and wings beyond numbering, but a single mind to guide them.').
card_multiverse_id('the hive'/'5ED', '3814').

card_in_set('the wretched', '5ED').
card_original_type('the wretched'/'5ED', 'Summon — Wretched').
card_original_text('the wretched'/'5ED', 'At end of combat, gain control of all creatures blocking The Wretched as long as you control The Wretched.').
card_image_name('the wretched'/'5ED', 'the wretched').
card_uid('the wretched'/'5ED', '5ED:The Wretched:the wretched').
card_rarity('the wretched'/'5ED', 'Rare').
card_artist('the wretched'/'5ED', 'Christopher Rush').
card_flavor_text('the wretched'/'5ED', 'Once I had the strength but no wisdom; now I have the wisdom but no strength.\n—Persian proverb').
card_multiverse_id('the wretched'/'5ED', '3880').

card_in_set('thicket basilisk', '5ED').
card_original_type('thicket basilisk'/'5ED', 'Summon — Basilisk').
card_original_text('thicket basilisk'/'5ED', 'If Thicket Basilisk blocks or is blocked by any non-Wall creature, destroy that creature at end of combat.').
card_image_name('thicket basilisk'/'5ED', 'thicket basilisk').
card_uid('thicket basilisk'/'5ED', '5ED:Thicket Basilisk:thicket basilisk').
card_rarity('thicket basilisk'/'5ED', 'Uncommon').
card_artist('thicket basilisk'/'5ED', 'Dan Frazier').
card_flavor_text('thicket basilisk'/'5ED', 'Moss-covered statues littered the area, a macabre monument to the basilisk\'s terrible power.').
card_multiverse_id('thicket basilisk'/'5ED', '4014').

card_in_set('throne of bone', '5ED').
card_original_type('throne of bone'/'5ED', 'Artifact').
card_original_text('throne of bone'/'5ED', '{1}: Gain 1 life. Use this ability only when a black spell is successfully cast and only once for each such spell.').
card_image_name('throne of bone'/'5ED', 'throne of bone').
card_uid('throne of bone'/'5ED', '5ED:Throne of Bone:throne of bone').
card_rarity('throne of bone'/'5ED', 'Uncommon').
card_artist('throne of bone'/'5ED', 'Donato Giancola').
card_multiverse_id('throne of bone'/'5ED', '3815').

card_in_set('thrull retainer', '5ED').
card_original_type('thrull retainer'/'5ED', 'Enchant Creature').
card_original_text('thrull retainer'/'5ED', 'Enchanted creature gets +1/+1.\nSacrifice Thrull Retainer: Regenerate enchanted creature.').
card_image_name('thrull retainer'/'5ED', 'thrull retainer').
card_uid('thrull retainer'/'5ED', '5ED:Thrull Retainer:thrull retainer').
card_rarity('thrull retainer'/'5ED', 'Uncommon').
card_artist('thrull retainer'/'5ED', 'Ron Spencer').
card_flavor_text('thrull retainer'/'5ED', '\"Until the Rebellion, thrulls served their masters faithfully—even at the cost of their own lives.\"\n—Sarpadian Empires, vol. II').
card_multiverse_id('thrull retainer'/'5ED', '3881').

card_in_set('time bomb', '5ED').
card_original_type('time bomb'/'5ED', 'Artifact').
card_original_text('time bomb'/'5ED', 'During your upkeep, put a time counter on Time Bomb.\n{1}, {T}, Sacrifice Time Bomb: Time Bomb deals to each creature and player an amount of damage equal to the number of time counters on Time Bomb.').
card_image_name('time bomb'/'5ED', 'time bomb').
card_uid('time bomb'/'5ED', '5ED:Time Bomb:time bomb').
card_rarity('time bomb'/'5ED', 'Rare').
card_artist('time bomb'/'5ED', 'George Pratt').
card_multiverse_id('time bomb'/'5ED', '3816').

card_in_set('time elemental', '5ED').
card_original_type('time elemental'/'5ED', 'Summon — Elemental').
card_original_text('time elemental'/'5ED', 'If Time Elemental attacks or blocks, it deals 5 damage to you and is buried at end of combat.\n{2}{U}{U}, {T}: Return target permanent with no enchantments on it to owner\'s hand.').
card_image_name('time elemental'/'5ED', 'time elemental').
card_uid('time elemental'/'5ED', '5ED:Time Elemental:time elemental').
card_rarity('time elemental'/'5ED', 'Rare').
card_artist('time elemental'/'5ED', 'Amy Weber').
card_multiverse_id('time elemental'/'5ED', '3950').

card_in_set('titania\'s song', '5ED').
card_original_type('titania\'s song'/'5ED', 'Enchantment').
card_original_text('titania\'s song'/'5ED', 'Each noncreature artifact loses its abilities and is an artifact creature with power and toughness each equal to its total casting cost. If Titania\'s Song leaves play, this effect continues until end of turn.').
card_image_name('titania\'s song'/'5ED', 'titania\'s song').
card_uid('titania\'s song'/'5ED', '5ED:Titania\'s Song:titania\'s song').
card_rarity('titania\'s song'/'5ED', 'Rare').
card_artist('titania\'s song'/'5ED', 'D. Alexander Gregory').
card_multiverse_id('titania\'s song'/'5ED', '4015').

card_in_set('torture', '5ED').
card_original_type('torture'/'5ED', 'Enchant Creature').
card_original_text('torture'/'5ED', '{1}{B} Put a -1/-1 counter on enchanted creature.').
card_image_name('torture'/'5ED', 'torture').
card_uid('torture'/'5ED', '5ED:Torture:torture').
card_rarity('torture'/'5ED', 'Common').
card_artist('torture'/'5ED', 'Mark Tedin').
card_flavor_text('torture'/'5ED', '\"It helps pass the time until you die.\"\n—Grandmother Sengir').
card_multiverse_id('torture'/'5ED', '3882').

card_in_set('touch of death', '5ED').
card_original_type('touch of death'/'5ED', 'Sorcery').
card_original_text('touch of death'/'5ED', 'Touch of Death deals 1 damage to target player and you gain 1 life.\nDraw a card at the beginning of the next turn.').
card_image_name('touch of death'/'5ED', 'touch of death').
card_uid('touch of death'/'5ED', '5ED:Touch of Death:touch of death').
card_rarity('touch of death'/'5ED', 'Common').
card_artist('touch of death'/'5ED', 'Melissa A. Benson').
card_flavor_text('touch of death'/'5ED', '\"What was yours is mine. Your land, your people, and now your life.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('touch of death'/'5ED', '3883').

card_in_set('tranquility', '5ED').
card_original_type('tranquility'/'5ED', 'Sorcery').
card_original_text('tranquility'/'5ED', 'Destroy all enchantments.').
card_image_name('tranquility'/'5ED', 'tranquility').
card_uid('tranquility'/'5ED', '5ED:Tranquility:tranquility').
card_rarity('tranquility'/'5ED', 'Common').
card_artist('tranquility'/'5ED', 'Douglas Shuler').
card_multiverse_id('tranquility'/'5ED', '4016').

card_in_set('truce', '5ED').
card_original_type('truce'/'5ED', 'Instant').
card_original_text('truce'/'5ED', 'Each player may draw up to two cards. For each card less than two any player draws, that player gains 2 life.').
card_image_name('truce'/'5ED', 'truce').
card_uid('truce'/'5ED', '5ED:Truce:truce').
card_rarity('truce'/'5ED', 'Rare').
card_artist('truce'/'5ED', 'Donato Giancola').
card_flavor_text('truce'/'5ED', 'Not all victories require defeat.\n—Onean idiom').
card_multiverse_id('truce'/'5ED', '4162').

card_in_set('tsunami', '5ED').
card_original_type('tsunami'/'5ED', 'Sorcery').
card_original_text('tsunami'/'5ED', 'Destroy all islands.').
card_image_name('tsunami'/'5ED', 'tsunami').
card_uid('tsunami'/'5ED', '5ED:Tsunami:tsunami').
card_rarity('tsunami'/'5ED', 'Uncommon').
card_artist('tsunami'/'5ED', 'Richard Thomas').
card_multiverse_id('tsunami'/'5ED', '4017').

card_in_set('tundra wolves', '5ED').
card_original_type('tundra wolves'/'5ED', 'Summon — Wolves').
card_original_text('tundra wolves'/'5ED', 'First strike').
card_image_name('tundra wolves'/'5ED', 'tundra wolves').
card_uid('tundra wolves'/'5ED', '5ED:Tundra Wolves:tundra wolves').
card_rarity('tundra wolves'/'5ED', 'Common').
card_artist('tundra wolves'/'5ED', 'Quinton Hoover').
card_flavor_text('tundra wolves'/'5ED', 'I heard their eerie howling, the wolves calling their kindred across the frozen plains.').
card_multiverse_id('tundra wolves'/'5ED', '4163').

card_in_set('twiddle', '5ED').
card_original_type('twiddle'/'5ED', 'Instant').
card_original_text('twiddle'/'5ED', 'Tap or untap target artifact, creature, or land.').
card_image_name('twiddle'/'5ED', 'twiddle').
card_uid('twiddle'/'5ED', '5ED:Twiddle:twiddle').
card_rarity('twiddle'/'5ED', 'Common').
card_artist('twiddle'/'5ED', 'Rob Alexander').
card_multiverse_id('twiddle'/'5ED', '3951').

card_in_set('underground river', '5ED').
card_original_type('underground river'/'5ED', 'Land').
card_original_text('underground river'/'5ED', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {U} or {B} to your mana pool. Underground River deals 1 damage to you.').
card_image_name('underground river'/'5ED', 'underground river').
card_uid('underground river'/'5ED', '5ED:Underground River:underground river').
card_rarity('underground river'/'5ED', 'Rare').
card_artist('underground river'/'5ED', 'Jeff Miracola').
card_multiverse_id('underground river'/'5ED', '4191').

card_in_set('unholy strength', '5ED').
card_original_type('unholy strength'/'5ED', 'Enchant Creature').
card_original_text('unholy strength'/'5ED', 'Enchanted creature gets +2/+1.').
card_image_name('unholy strength'/'5ED', 'unholy strength').
card_uid('unholy strength'/'5ED', '5ED:Unholy Strength:unholy strength').
card_rarity('unholy strength'/'5ED', 'Common').
card_artist('unholy strength'/'5ED', 'Tom Kyffin').
card_multiverse_id('unholy strength'/'5ED', '3884').

card_in_set('unstable mutation', '5ED').
card_original_type('unstable mutation'/'5ED', 'Enchant Creature').
card_original_text('unstable mutation'/'5ED', 'Enchanted creature gets +3/+3.\nDuring its controller\'s upkeep, put a -1/-1 counter on enchanted creature.').
card_image_name('unstable mutation'/'5ED', 'unstable mutation').
card_uid('unstable mutation'/'5ED', '5ED:Unstable Mutation:unstable mutation').
card_rarity('unstable mutation'/'5ED', 'Common').
card_artist('unstable mutation'/'5ED', 'Charles Gillespie').
card_multiverse_id('unstable mutation'/'5ED', '3952').

card_in_set('unsummon', '5ED').
card_original_type('unsummon'/'5ED', 'Instant').
card_original_text('unsummon'/'5ED', 'Return target creature to owner\'s hand.').
card_image_name('unsummon'/'5ED', 'unsummon').
card_uid('unsummon'/'5ED', '5ED:Unsummon:unsummon').
card_rarity('unsummon'/'5ED', 'Common').
card_artist('unsummon'/'5ED', 'Douglas Shuler').
card_multiverse_id('unsummon'/'5ED', '3953').

card_in_set('untamed wilds', '5ED').
card_original_type('untamed wilds'/'5ED', 'Sorcery').
card_original_text('untamed wilds'/'5ED', 'Search your library for a basic land card and put that card into play. Shuffle your library afterwards.').
card_image_name('untamed wilds'/'5ED', 'untamed wilds').
card_uid('untamed wilds'/'5ED', '5ED:Untamed Wilds:untamed wilds').
card_rarity('untamed wilds'/'5ED', 'Uncommon').
card_artist('untamed wilds'/'5ED', 'NéNé Thomas').
card_multiverse_id('untamed wilds'/'5ED', '4018').

card_in_set('updraft', '5ED').
card_original_type('updraft'/'5ED', 'Instant').
card_original_text('updraft'/'5ED', 'Target creature gains flying until end of turn.\nDraw a card at the beginning of the next turn.').
card_image_name('updraft'/'5ED', 'updraft').
card_uid('updraft'/'5ED', '5ED:Updraft:updraft').
card_rarity('updraft'/'5ED', 'Common').
card_artist('updraft'/'5ED', 'John Matson').
card_flavor_text('updraft'/'5ED', '\"Come one, come all! this rock shall fly From its firm base as soon as I.\"\n—Sir Walter Scott, \"The Lady of the Lake\"').
card_multiverse_id('updraft'/'5ED', '3954').

card_in_set('urza\'s avenger', '5ED').
card_original_type('urza\'s avenger'/'5ED', 'Artifact Creature').
card_original_text('urza\'s avenger'/'5ED', '{0}: -1/-1 and your choice of banding, flying, first strike, or trample until end of turn').
card_image_name('urza\'s avenger'/'5ED', 'urza\'s avenger').
card_uid('urza\'s avenger'/'5ED', '5ED:Urza\'s Avenger:urza\'s avenger').
card_rarity('urza\'s avenger'/'5ED', 'Rare').
card_artist('urza\'s avenger'/'5ED', 'Amy Weber').
card_flavor_text('urza\'s avenger'/'5ED', 'Unable to settle on just one design, Urza decided to create one versatile being.').
card_multiverse_id('urza\'s avenger'/'5ED', '3817').

card_in_set('urza\'s bauble', '5ED').
card_original_type('urza\'s bauble'/'5ED', 'Artifact').
card_original_text('urza\'s bauble'/'5ED', '{T}, Sacrifice Urza\'s Bauble: Choose a card at random from target player\'s hand and look at that card. Draw a card at the beginning of the next turn.').
card_image_name('urza\'s bauble'/'5ED', 'urza\'s bauble').
card_uid('urza\'s bauble'/'5ED', '5ED:Urza\'s Bauble:urza\'s bauble').
card_rarity('urza\'s bauble'/'5ED', 'Uncommon').
card_artist('urza\'s bauble'/'5ED', 'Christopher Rush').
card_multiverse_id('urza\'s bauble'/'5ED', '3818').

card_in_set('urza\'s mine', '5ED').
card_original_type('urza\'s mine'/'5ED', 'Land').
card_original_text('urza\'s mine'/'5ED', '{T}: Add one colorless mana to your mana pool. If you control Urza\'s Mine, Urza\'s Power Plant, and Urza\'s Tower, add two colorless mana to your mana pool instead of one.').
card_image_name('urza\'s mine'/'5ED', 'urza\'s mine').
card_uid('urza\'s mine'/'5ED', '5ED:Urza\'s Mine:urza\'s mine').
card_rarity('urza\'s mine'/'5ED', 'Common').
card_artist('urza\'s mine'/'5ED', 'Anson Maddocks').
card_flavor_text('urza\'s mine'/'5ED', 'Mines became common as cities during the days of the artificers.').
card_multiverse_id('urza\'s mine'/'5ED', '4192').

card_in_set('urza\'s power plant', '5ED').
card_original_type('urza\'s power plant'/'5ED', 'Land').
card_original_text('urza\'s power plant'/'5ED', '{T}: Add one colorless mana to your mana pool. If you control Urza\'s Mine, Urza\'s Power Plant, and Urza\'s Tower, add two colorless mana to your mana pool instead of one.').
card_image_name('urza\'s power plant'/'5ED', 'urza\'s power plant').
card_uid('urza\'s power plant'/'5ED', '5ED:Urza\'s Power Plant:urza\'s power plant').
card_rarity('urza\'s power plant'/'5ED', 'Common').
card_artist('urza\'s power plant'/'5ED', 'Mark Tedin').
card_flavor_text('urza\'s power plant'/'5ED', 'Artifact construction required immense resources.').
card_multiverse_id('urza\'s power plant'/'5ED', '4193').

card_in_set('urza\'s tower', '5ED').
card_original_type('urza\'s tower'/'5ED', 'Land').
card_original_text('urza\'s tower'/'5ED', '{T}: Add one colorless mana to your mana pool. If you control Urza\'s Mine, Urza\'s Power Plant, and Urza\'s Tower, add three colorless mana to your mana pool instead of one.').
card_image_name('urza\'s tower'/'5ED', 'urza\'s tower').
card_uid('urza\'s tower'/'5ED', '5ED:Urza\'s Tower:urza\'s tower').
card_rarity('urza\'s tower'/'5ED', 'Common').
card_artist('urza\'s tower'/'5ED', 'Mark Poole').
card_flavor_text('urza\'s tower'/'5ED', 'Urza always put Tocasia\'s lessons on resource-gathering to effective use.').
card_multiverse_id('urza\'s tower'/'5ED', '4194').

card_in_set('vampire bats', '5ED').
card_original_type('vampire bats'/'5ED', 'Summon — Bats').
card_original_text('vampire bats'/'5ED', 'Flying\n{B} +1/+0 until end of turn. You cannot spend more than {B}{B} in this way each turn.').
card_image_name('vampire bats'/'5ED', 'vampire bats').
card_uid('vampire bats'/'5ED', '5ED:Vampire Bats:vampire bats').
card_rarity('vampire bats'/'5ED', 'Common').
card_artist('vampire bats'/'5ED', 'Anson Maddocks').
card_flavor_text('vampire bats'/'5ED', '\"For something is amiss or out of place / When mice with wings can wear a human face.\"\n—Theodore Roethke, \"The Bat\"').
card_multiverse_id('vampire bats'/'5ED', '3885').

card_in_set('venom', '5ED').
card_original_type('venom'/'5ED', 'Enchant Creature').
card_original_text('venom'/'5ED', 'If enchanted creature blocks or is blocked by any non-Wall creature, destroy that creature at end of combat.').
card_image_name('venom'/'5ED', 'venom').
card_uid('venom'/'5ED', '5ED:Venom:venom').
card_rarity('venom'/'5ED', 'Common').
card_artist('venom'/'5ED', 'Tom Wänerstrand').
card_flavor_text('venom'/'5ED', '\"I told him it was just a flesh wound, but the next time I looked at him, poor Tadhg was dead and gone.\"\n—Maeveen O\'Donagh,\nMemoirs of a Soldier').
card_multiverse_id('venom'/'5ED', '4019').

card_in_set('verduran enchantress', '5ED').
card_original_type('verduran enchantress'/'5ED', 'Summon — Enchantress').
card_original_text('verduran enchantress'/'5ED', '{0}: Draw a card. Use this ability only when you successfully cast an enchantment spell and only once for each such spell.').
card_image_name('verduran enchantress'/'5ED', 'verduran enchantress').
card_uid('verduran enchantress'/'5ED', '5ED:Verduran Enchantress:verduran enchantress').
card_rarity('verduran enchantress'/'5ED', 'Rare').
card_artist('verduran enchantress'/'5ED', 'Kev Brockschmidt').
card_flavor_text('verduran enchantress'/'5ED', '\"Weave the magic so that it sings to you, and will always fly home . . . .\"\n—Verduran teaching').
card_multiverse_id('verduran enchantress'/'5ED', '4020').

card_in_set('vodalian soldiers', '5ED').
card_original_type('vodalian soldiers'/'5ED', 'Summon — Merfolk').
card_original_text('vodalian soldiers'/'5ED', '').
card_image_name('vodalian soldiers'/'5ED', 'vodalian soldiers').
card_uid('vodalian soldiers'/'5ED', '5ED:Vodalian Soldiers:vodalian soldiers').
card_rarity('vodalian soldiers'/'5ED', 'Common').
card_artist('vodalian soldiers'/'5ED', 'Melissa A. Benson').
card_flavor_text('vodalian soldiers'/'5ED', '\"Vodalian rank is displayed by the colors and patterns of their skin. Beware the color red; that is the badge of the empress\'s favor.\"\n—Corbio, pearl diver').
card_multiverse_id('vodalian soldiers'/'5ED', '3955').

card_in_set('wall of air', '5ED').
card_original_type('wall of air'/'5ED', 'Summon — Wall').
card_original_text('wall of air'/'5ED', 'Flying').
card_image_name('wall of air'/'5ED', 'wall of air').
card_uid('wall of air'/'5ED', '5ED:Wall of Air:wall of air').
card_rarity('wall of air'/'5ED', 'Uncommon').
card_artist('wall of air'/'5ED', 'Richard Kane Ferguson').
card_flavor_text('wall of air'/'5ED', 'When no falcons fly, beware the sky.\n—Femeref aphorism').
card_multiverse_id('wall of air'/'5ED', '3956').

card_in_set('wall of bone', '5ED').
card_original_type('wall of bone'/'5ED', 'Summon — Wall').
card_original_text('wall of bone'/'5ED', '{B} Regenerate').
card_image_name('wall of bone'/'5ED', 'wall of bone').
card_uid('wall of bone'/'5ED', '5ED:Wall of Bone:wall of bone').
card_rarity('wall of bone'/'5ED', 'Uncommon').
card_artist('wall of bone'/'5ED', 'Anson Maddocks').
card_flavor_text('wall of bone'/'5ED', '\"Good neighbors make good walls.\"\n—Jakkarak the Maimer').
card_multiverse_id('wall of bone'/'5ED', '3886').

card_in_set('wall of brambles', '5ED').
card_original_type('wall of brambles'/'5ED', 'Summon — Wall').
card_original_text('wall of brambles'/'5ED', '{G} Regenerate').
card_image_name('wall of brambles'/'5ED', 'wall of brambles').
card_uid('wall of brambles'/'5ED', '5ED:Wall of Brambles:wall of brambles').
card_rarity('wall of brambles'/'5ED', 'Uncommon').
card_artist('wall of brambles'/'5ED', 'Tony Roberts').
card_flavor_text('wall of brambles'/'5ED', 'Sleeping in brambles\n—Elven phrase meaning\n\"plagued by guilt\"').
card_multiverse_id('wall of brambles'/'5ED', '4021').

card_in_set('wall of fire', '5ED').
card_original_type('wall of fire'/'5ED', 'Summon — Wall').
card_original_text('wall of fire'/'5ED', '{R} +1/+0 until end of turn').
card_image_name('wall of fire'/'5ED', 'wall of fire').
card_uid('wall of fire'/'5ED', '5ED:Wall of Fire:wall of fire').
card_rarity('wall of fire'/'5ED', 'Uncommon').
card_artist('wall of fire'/'5ED', 'Tony Roberts').
card_flavor_text('wall of fire'/'5ED', '\"Struggle, and you will only fan the flames.\"\n—Talibah, embermage').
card_multiverse_id('wall of fire'/'5ED', '4094').

card_in_set('wall of spears', '5ED').
card_original_type('wall of spears'/'5ED', 'Artifact Creature').
card_original_text('wall of spears'/'5ED', 'First strike\nWall of Spears counts as a Wall.').
card_image_name('wall of spears'/'5ED', 'wall of spears').
card_uid('wall of spears'/'5ED', '5ED:Wall of Spears:wall of spears').
card_rarity('wall of spears'/'5ED', 'Common').
card_artist('wall of spears'/'5ED', 'Zak Plucinski').
card_flavor_text('wall of spears'/'5ED', 'Even the most conservative generals revised their tactics after the Battle of Sarinth, during which a handful of peasant-pikemen held off a trio of rampaging craw wurms.').
card_multiverse_id('wall of spears'/'5ED', '3819').

card_in_set('wall of stone', '5ED').
card_original_type('wall of stone'/'5ED', 'Summon — Wall').
card_original_text('wall of stone'/'5ED', '').
card_image_name('wall of stone'/'5ED', 'wall of stone').
card_uid('wall of stone'/'5ED', '5ED:Wall of Stone:wall of stone').
card_rarity('wall of stone'/'5ED', 'Uncommon').
card_artist('wall of stone'/'5ED', 'Thomas Gianni').
card_flavor_text('wall of stone'/'5ED', 'The ground itself lends its strength to these walls of living stone, which possess the stability of ancient mountains. These mighty bulwarks thwart ground-based troops, providing welcome relief for weary warriors who defend the land.').
card_multiverse_id('wall of stone'/'5ED', '4095').

card_in_set('wall of swords', '5ED').
card_original_type('wall of swords'/'5ED', 'Summon — Wall').
card_original_text('wall of swords'/'5ED', 'Flying').
card_image_name('wall of swords'/'5ED', 'wall of swords').
card_uid('wall of swords'/'5ED', '5ED:Wall of Swords:wall of swords').
card_rarity('wall of swords'/'5ED', 'Uncommon').
card_artist('wall of swords'/'5ED', 'Brian Snõddy').
card_flavor_text('wall of swords'/'5ED', 'Just as the evil ones approached to slay Justina, she cast a great spell, imbuing her weapons with her own life force. Thus she fulfilled the prophecy: \"In the death of your savior will you find salvation.\"').
card_multiverse_id('wall of swords'/'5ED', '4164').

card_in_set('wanderlust', '5ED').
card_original_type('wanderlust'/'5ED', 'Enchant Creature').
card_original_text('wanderlust'/'5ED', 'During the upkeep of enchanted creature\'s controller, Wanderlust deals 1 damage to him or her.').
card_image_name('wanderlust'/'5ED', 'wanderlust').
card_uid('wanderlust'/'5ED', '5ED:Wanderlust:wanderlust').
card_rarity('wanderlust'/'5ED', 'Uncommon').
card_artist('wanderlust'/'5ED', 'Rebecca Guay').
card_flavor_text('wanderlust'/'5ED', '\"How terrible to wander wishing only to escape oneself.\"\n—Taysir').
card_multiverse_id('wanderlust'/'5ED', '4022').

card_in_set('war mammoth', '5ED').
card_original_type('war mammoth'/'5ED', 'Summon — Mammoth').
card_original_text('war mammoth'/'5ED', 'Trample').
card_image_name('war mammoth'/'5ED', 'war mammoth').
card_uid('war mammoth'/'5ED', '5ED:War Mammoth:war mammoth').
card_rarity('war mammoth'/'5ED', 'Common').
card_artist('war mammoth'/'5ED', 'Jeff A. Menges').
card_flavor_text('war mammoth'/'5ED', 'I didn\'t think mammoths could ever hold a candle to a well-trained battle horse. Then one day I turned my back on a drunken soldier. His blow never landed; Mi\'cha flung the brute over ten meters.').
card_multiverse_id('war mammoth'/'5ED', '4023').

card_in_set('warp artifact', '5ED').
card_original_type('warp artifact'/'5ED', 'Enchant Artifact').
card_original_text('warp artifact'/'5ED', 'During the upkeep of enchanted artifact\'s controller, Warp Artifact deals 1 damage to him or her.').
card_image_name('warp artifact'/'5ED', 'warp artifact').
card_uid('warp artifact'/'5ED', '5ED:Warp Artifact:warp artifact').
card_rarity('warp artifact'/'5ED', 'Rare').
card_artist('warp artifact'/'5ED', 'Amy Weber').
card_multiverse_id('warp artifact'/'5ED', '3887').

card_in_set('weakness', '5ED').
card_original_type('weakness'/'5ED', 'Enchant Creature').
card_original_text('weakness'/'5ED', 'Enchanted creature gets -2/-1.').
card_image_name('weakness'/'5ED', 'weakness').
card_uid('weakness'/'5ED', '5ED:Weakness:weakness').
card_rarity('weakness'/'5ED', 'Common').
card_artist('weakness'/'5ED', 'Kev Walker').
card_multiverse_id('weakness'/'5ED', '3888').

card_in_set('whirling dervish', '5ED').
card_original_type('whirling dervish'/'5ED', 'Summon — Dervish').
card_original_text('whirling dervish'/'5ED', 'Protection from black\nIf Whirling Dervish damages any opponent, put a +1/+1 counter on it at end of turn.').
card_image_name('whirling dervish'/'5ED', 'whirling dervish').
card_uid('whirling dervish'/'5ED', '5ED:Whirling Dervish:whirling dervish').
card_rarity('whirling dervish'/'5ED', 'Uncommon').
card_artist('whirling dervish'/'5ED', 'Susan Van Camp').
card_multiverse_id('whirling dervish'/'5ED', '4024').

card_in_set('white knight', '5ED').
card_original_type('white knight'/'5ED', 'Summon — Knight').
card_original_text('white knight'/'5ED', 'First strike, protection from black').
card_image_name('white knight'/'5ED', 'white knight').
card_uid('white knight'/'5ED', '5ED:White Knight:white knight').
card_rarity('white knight'/'5ED', 'Uncommon').
card_artist('white knight'/'5ED', 'Daniel Gelon').
card_flavor_text('white knight'/'5ED', 'Out of the blackness and stench of the engulfing swamp emerged a shimmering figure. Only the splattered armor and ichor-stained sword hinted at the unfathomable evil the knight had just laid waste.').
card_multiverse_id('white knight'/'5ED', '4165').

card_in_set('wild growth', '5ED').
card_original_type('wild growth'/'5ED', 'Enchant Land').
card_original_text('wild growth'/'5ED', 'Whenever enchanted land is tapped for mana, it produces an additional {G}.').
card_image_name('wild growth'/'5ED', 'wild growth').
card_uid('wild growth'/'5ED', '5ED:Wild Growth:wild growth').
card_rarity('wild growth'/'5ED', 'Common').
card_artist('wild growth'/'5ED', 'Pat Morrissey').
card_multiverse_id('wild growth'/'5ED', '4025').

card_in_set('wind spirit', '5ED').
card_original_type('wind spirit'/'5ED', 'Summon — Spirit').
card_original_text('wind spirit'/'5ED', 'Flying\nWind Spirit cannot be blocked by only one creature.').
card_image_name('wind spirit'/'5ED', 'wind spirit').
card_uid('wind spirit'/'5ED', '5ED:Wind Spirit:wind spirit').
card_rarity('wind spirit'/'5ED', 'Uncommon').
card_artist('wind spirit'/'5ED', 'Kaja Foglio').
card_flavor_text('wind spirit'/'5ED', '\" . . . When the trees bow down their heads, / The wind is passing by.\"\n—Christina Rossetti,\n\"Who Has Seen the Wind?\"').
card_multiverse_id('wind spirit'/'5ED', '3957').

card_in_set('winds of change', '5ED').
card_original_type('winds of change'/'5ED', 'Sorcery').
card_original_text('winds of change'/'5ED', 'Each player shuffles his or her hand into his or her library, then draws a new hand of as many cards as he or she had before.').
card_image_name('winds of change'/'5ED', 'winds of change').
card_uid('winds of change'/'5ED', '5ED:Winds of Change:winds of change').
card_rarity('winds of change'/'5ED', 'Rare').
card_artist('winds of change'/'5ED', 'Blackie del Rio').
card_flavor_text('winds of change'/'5ED', '\"\'Tis the set of sails, and not the gales, Which tell us the way to go.\"\n—Ella Wheeler Wilcox, \"Winds of Fate\"').
card_multiverse_id('winds of change'/'5ED', '4096').

card_in_set('winter blast', '5ED').
card_original_type('winter blast'/'5ED', 'Sorcery').
card_original_text('winter blast'/'5ED', 'Tap X target creatures. Winter Blast deals 2 damage to each of those creatures with flying.').
card_image_name('winter blast'/'5ED', 'winter blast').
card_uid('winter blast'/'5ED', '5ED:Winter Blast:winter blast').
card_rarity('winter blast'/'5ED', 'Uncommon').
card_artist('winter blast'/'5ED', 'Kaja Foglio').
card_flavor_text('winter blast'/'5ED', '\"Blow, winds, and crack your cheeks! rage! blow!\" —William Shakespeare, King Lear').
card_multiverse_id('winter blast'/'5ED', '4026').

card_in_set('winter orb', '5ED').
card_original_type('winter orb'/'5ED', 'Artifact').
card_original_text('winter orb'/'5ED', 'Players cannot untap more than one land during their untap phases.').
card_image_name('winter orb'/'5ED', 'winter orb').
card_uid('winter orb'/'5ED', '5ED:Winter Orb:winter orb').
card_rarity('winter orb'/'5ED', 'Rare').
card_artist('winter orb'/'5ED', 'Mark Tedin').
card_flavor_text('winter orb'/'5ED', '\"When once winter has ahold,\nNot willingly does she withdraw\nher icy touch, wistfully caressing—\nThe deepened air sleeps its cold.\"\n—Aline Corralurn, \"Late Thaw\"').
card_multiverse_id('winter orb'/'5ED', '3820').

card_in_set('wolverine pack', '5ED').
card_original_type('wolverine pack'/'5ED', 'Summon — Wolverine Pack').
card_original_text('wolverine pack'/'5ED', 'Rampage 2 (For each creature assigned to block it beyond the first, this creature gets +2/+2 until end of turn.)').
card_image_name('wolverine pack'/'5ED', 'wolverine pack').
card_uid('wolverine pack'/'5ED', '5ED:Wolverine Pack:wolverine pack').
card_rarity('wolverine pack'/'5ED', 'Uncommon').
card_artist('wolverine pack'/'5ED', 'Steve White').
card_flavor_text('wolverine pack'/'5ED', '\"Give them great meals of beef and iron and steel, they will eat like wolves and fight like devils.\"\n—William Shakespeare, King Henry V').
card_multiverse_id('wolverine pack'/'5ED', '4027').

card_in_set('wooden sphere', '5ED').
card_original_type('wooden sphere'/'5ED', 'Artifact').
card_original_text('wooden sphere'/'5ED', '{1}: Gain 1 life. Use this ability only when a green spell is successfully cast and only once for each such spell.').
card_image_name('wooden sphere'/'5ED', 'wooden sphere').
card_uid('wooden sphere'/'5ED', '5ED:Wooden Sphere:wooden sphere').
card_rarity('wooden sphere'/'5ED', 'Uncommon').
card_artist('wooden sphere'/'5ED', 'Donato Giancola').
card_multiverse_id('wooden sphere'/'5ED', '3821').

card_in_set('word of blasting', '5ED').
card_original_type('word of blasting'/'5ED', 'Instant').
card_original_text('word of blasting'/'5ED', 'Bury target Wall. Word of Blasting deals to that Wall\'s controller an amount of damage equal to the Wall\'s total casting cost.').
card_image_name('word of blasting'/'5ED', 'word of blasting').
card_uid('word of blasting'/'5ED', '5ED:Word of Blasting:word of blasting').
card_rarity('word of blasting'/'5ED', 'Uncommon').
card_artist('word of blasting'/'5ED', 'Ken Meyer, Jr.').
card_flavor_text('word of blasting'/'5ED', '\"Walls? What walls?\"\n—Jaya Ballard, task mage').
card_multiverse_id('word of blasting'/'5ED', '4097').

card_in_set('wrath of god', '5ED').
card_original_type('wrath of god'/'5ED', 'Sorcery').
card_original_text('wrath of god'/'5ED', 'Bury all creatures.').
card_image_name('wrath of god'/'5ED', 'wrath of god').
card_uid('wrath of god'/'5ED', '5ED:Wrath of God:wrath of god').
card_rarity('wrath of god'/'5ED', 'Rare').
card_artist('wrath of god'/'5ED', 'Quinton Hoover').
card_multiverse_id('wrath of god'/'5ED', '4166').

card_in_set('wyluli wolf', '5ED').
card_original_type('wyluli wolf'/'5ED', 'Summon — Wolf').
card_original_text('wyluli wolf'/'5ED', '{T}: Target creature gets +1/+1 until end of turn.').
card_image_name('wyluli wolf'/'5ED', 'wyluli wolf').
card_uid('wyluli wolf'/'5ED', '5ED:Wyluli Wolf:wyluli wolf').
card_rarity('wyluli wolf'/'5ED', 'Rare').
card_artist('wyluli wolf'/'5ED', 'Susan Van Camp').
card_flavor_text('wyluli wolf'/'5ED', '\"When one wolf calls, others follow.\nWho wants to fight creatures that eat scorpions?\"\n—Maimun al-Wyluli, diary').
card_multiverse_id('wyluli wolf'/'5ED', '4028').

card_in_set('xenic poltergeist', '5ED').
card_original_type('xenic poltergeist'/'5ED', 'Summon — Poltergeist').
card_original_text('xenic poltergeist'/'5ED', '{T}: Until your next upkeep, target noncreature artifact is an artifact creature with power and toughness each equal to its total casting cost. (That artifact retains all of its original abilities.)').
card_image_name('xenic poltergeist'/'5ED', 'xenic poltergeist').
card_uid('xenic poltergeist'/'5ED', '5ED:Xenic Poltergeist:xenic poltergeist').
card_rarity('xenic poltergeist'/'5ED', 'Rare').
card_artist('xenic poltergeist'/'5ED', 'Mike Kerr').
card_multiverse_id('xenic poltergeist'/'5ED', '3889').

card_in_set('zephyr falcon', '5ED').
card_original_type('zephyr falcon'/'5ED', 'Summon — Falcon').
card_original_text('zephyr falcon'/'5ED', 'Flying\nAttacking does not cause Zephyr Falcon to tap.').
card_image_name('zephyr falcon'/'5ED', 'zephyr falcon').
card_uid('zephyr falcon'/'5ED', '5ED:Zephyr Falcon:zephyr falcon').
card_rarity('zephyr falcon'/'5ED', 'Common').
card_artist('zephyr falcon'/'5ED', 'Heather Hudson').
card_flavor_text('zephyr falcon'/'5ED', 'Although greatly prized among falconers, the zephyr falcon is capricious and not easily tamed.').
card_multiverse_id('zephyr falcon'/'5ED', '3958').

card_in_set('zombie master', '5ED').
card_original_type('zombie master'/'5ED', 'Summon — Lord').
card_original_text('zombie master'/'5ED', 'All Zombies gain \"{B} Regenerate\" and swampwalk. (If defending player controls any swamps, these creatures are unblockable.)').
card_image_name('zombie master'/'5ED', 'zombie master').
card_uid('zombie master'/'5ED', '5ED:Zombie Master:zombie master').
card_rarity('zombie master'/'5ED', 'Rare').
card_artist('zombie master'/'5ED', 'Stuart Griffin').
card_flavor_text('zombie master'/'5ED', 'He controlled the zombies even before his own death; now nothing can make them betray him.').
card_multiverse_id('zombie master'/'5ED', '3890').

card_in_set('zur\'s weirding', '5ED').
card_original_type('zur\'s weirding'/'5ED', 'Enchantment').
card_original_text('zur\'s weirding'/'5ED', 'Players play with their hands face up.\nWhenever any player draws a card, any other player may pay 2 life to force the drawing player to discard that card.').
card_image_name('zur\'s weirding'/'5ED', 'zur\'s weirding').
card_uid('zur\'s weirding'/'5ED', '5ED:Zur\'s Weirding:zur\'s weirding').
card_rarity('zur\'s weirding'/'5ED', 'Rare').
card_artist('zur\'s weirding'/'5ED', 'Liz Danforth').
card_multiverse_id('zur\'s weirding'/'5ED', '3959').
