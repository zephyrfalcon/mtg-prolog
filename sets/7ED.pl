% Seventh Edition

set('7ED').
set_name('7ED', 'Seventh Edition').
set_release_date('7ED', '2001-04-11').
set_border('7ED', 'white').
set_type('7ED', 'core').

card_in_set('abyssal horror', '7ED').
card_original_type('abyssal horror'/'7ED', 'Creature — Horror').
card_original_text('abyssal horror'/'7ED', 'Flying\nWhen Abyssal Horror comes into play, target player discards two cards from his or her hand.').
card_image_name('abyssal horror'/'7ED', 'abyssal horror').
card_uid('abyssal horror'/'7ED', '7ED:Abyssal Horror:abyssal horror').
card_rarity('abyssal horror'/'7ED', 'Rare').
card_artist('abyssal horror'/'7ED', 'Daren Bader').
card_number('abyssal horror'/'7ED', '115').
card_flavor_text('abyssal horror'/'7ED', '\"I dreamt of it once. Now I fear it dreams of me.\"\n—Tolarian scholar').
card_multiverse_id('abyssal horror'/'7ED', '13111').

card_in_set('abyssal specter', '7ED').
card_original_type('abyssal specter'/'7ED', 'Creature — Specter').
card_original_text('abyssal specter'/'7ED', 'Flying\nWhenever Abyssal Specter deals damage to a player, that player discards a card from his or her hand.').
card_image_name('abyssal specter'/'7ED', 'abyssal specter').
card_uid('abyssal specter'/'7ED', '7ED:Abyssal Specter:abyssal specter').
card_rarity('abyssal specter'/'7ED', 'Uncommon').
card_artist('abyssal specter'/'7ED', 'Michael Sutfin').
card_number('abyssal specter'/'7ED', '116').
card_flavor_text('abyssal specter'/'7ED', 'To gaze under its hood is to invite death.').
card_multiverse_id('abyssal specter'/'7ED', '25636').

card_in_set('adarkar wastes', '7ED').
card_original_type('adarkar wastes'/'7ED', 'Land').
card_original_text('adarkar wastes'/'7ED', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {W} or {U} to your mana pool. Adarkar Wastes deals 1 damage to you.').
card_image_name('adarkar wastes'/'7ED', 'adarkar wastes').
card_uid('adarkar wastes'/'7ED', '7ED:Adarkar Wastes:adarkar wastes').
card_rarity('adarkar wastes'/'7ED', 'Rare').
card_artist('adarkar wastes'/'7ED', 'John Avon').
card_number('adarkar wastes'/'7ED', '325').
card_multiverse_id('adarkar wastes'/'7ED', '11140').

card_in_set('æther flash', '7ED').
card_original_type('æther flash'/'7ED', 'Enchantment').
card_original_text('æther flash'/'7ED', 'Whenever a creature comes into play, Æther Flash deals 2 damage to it.').
card_image_name('æther flash'/'7ED', 'aether flash').
card_uid('æther flash'/'7ED', '7ED:Æther Flash:aether flash').
card_rarity('æther flash'/'7ED', 'Uncommon').
card_artist('æther flash'/'7ED', 'Wayne England').
card_number('æther flash'/'7ED', '172').
card_flavor_text('æther flash'/'7ED', 'As the foul beast appeared, arcane lightning crackled around it.').
card_multiverse_id('æther flash'/'7ED', '25678').

card_in_set('agonizing memories', '7ED').
card_original_type('agonizing memories'/'7ED', 'Sorcery').
card_original_text('agonizing memories'/'7ED', 'Look at target player\'s hand and choose two cards from it. Put them on top of that player\'s library in any order.').
card_image_name('agonizing memories'/'7ED', 'agonizing memories').
card_uid('agonizing memories'/'7ED', '7ED:Agonizing Memories:agonizing memories').
card_rarity('agonizing memories'/'7ED', 'Uncommon').
card_artist('agonizing memories'/'7ED', 'Adam Rex').
card_number('agonizing memories'/'7ED', '117').
card_flavor_text('agonizing memories'/'7ED', '\"I think little of the foes I\'ve slain. But I\'m haunted by the friends I\'ve sent to die.\"\n—The Southern Paladin').
card_multiverse_id('agonizing memories'/'7ED', '12994').

card_in_set('air elemental', '7ED').
card_original_type('air elemental'/'7ED', 'Creature — Elemental').
card_original_text('air elemental'/'7ED', 'Flying').
card_image_name('air elemental'/'7ED', 'air elemental').
card_uid('air elemental'/'7ED', '7ED:Air Elemental:air elemental').
card_rarity('air elemental'/'7ED', 'Uncommon').
card_artist('air elemental'/'7ED', 'Wayne England').
card_number('air elemental'/'7ED', '58').
card_flavor_text('air elemental'/'7ED', 'Where psycho meets cyclone.').
card_multiverse_id('air elemental'/'7ED', '15802').

card_in_set('aladdin\'s ring', '7ED').
card_original_type('aladdin\'s ring'/'7ED', 'Artifact').
card_original_text('aladdin\'s ring'/'7ED', '{8}, {T}: Aladdin\'s Ring deals 4 damage to target creature or player.').
card_image_name('aladdin\'s ring'/'7ED', 'aladdin\'s ring').
card_uid('aladdin\'s ring'/'7ED', '7ED:Aladdin\'s Ring:aladdin\'s ring').
card_rarity('aladdin\'s ring'/'7ED', 'Rare').
card_artist('aladdin\'s ring'/'7ED', 'Dave Dorman').
card_number('aladdin\'s ring'/'7ED', '286').
card_flavor_text('aladdin\'s ring'/'7ED', 'Some say Aladdin\'s third wish was for this ring.').
card_multiverse_id('aladdin\'s ring'/'7ED', '11145').

card_in_set('anaconda', '7ED').
card_original_type('anaconda'/'7ED', 'Creature — Snake').
card_original_text('anaconda'/'7ED', 'Swampwalk (This creature is unblockable as long as defending player controls a swamp.)').
card_image_name('anaconda'/'7ED', 'anaconda').
card_uid('anaconda'/'7ED', '7ED:Anaconda:anaconda').
card_rarity('anaconda'/'7ED', 'Uncommon').
card_artist('anaconda'/'7ED', 'John Gallagher').
card_number('anaconda'/'7ED', '229').
card_flavor_text('anaconda'/'7ED', 'You\'ll never find an anaconda in a swamp. It will always find you first.').
card_multiverse_id('anaconda'/'7ED', '13106').

card_in_set('ancestral memories', '7ED').
card_original_type('ancestral memories'/'7ED', 'Sorcery').
card_original_text('ancestral memories'/'7ED', 'Look at the top seven cards of your library. Put two of them into your hand and the rest into your graveyard.').
card_image_name('ancestral memories'/'7ED', 'ancestral memories').
card_uid('ancestral memories'/'7ED', '7ED:Ancestral Memories:ancestral memories').
card_rarity('ancestral memories'/'7ED', 'Rare').
card_artist('ancestral memories'/'7ED', 'Rebecca Guay').
card_number('ancestral memories'/'7ED', '59').
card_flavor_text('ancestral memories'/'7ED', '\"It\'s a poor sort of memory that only works backwards.\"\n—Lewis Carroll,\nThrough the Looking Glass').
card_multiverse_id('ancestral memories'/'7ED', '27115').

card_in_set('ancient silverback', '7ED').
card_original_type('ancient silverback'/'7ED', 'Creature — Ape').
card_original_text('ancient silverback'/'7ED', '{G} Regenerate Ancient Silverback.').
card_image_name('ancient silverback'/'7ED', 'ancient silverback').
card_uid('ancient silverback'/'7ED', '7ED:Ancient Silverback:ancient silverback').
card_rarity('ancient silverback'/'7ED', 'Rare').
card_artist('ancient silverback'/'7ED', 'Scott M. Fischer').
card_number('ancient silverback'/'7ED', '230').
card_flavor_text('ancient silverback'/'7ED', 'The Phyrexian killing machines couldn\'t have known the seriousness of their mistake in wounding the ape—they\'d never seen it angry.').
card_multiverse_id('ancient silverback'/'7ED', '25595').

card_in_set('angelic page', '7ED').
card_original_type('angelic page'/'7ED', 'Creature — Spirit').
card_original_text('angelic page'/'7ED', 'Flying\n{T}: Target attacking or blocking creature gets +1/+1 until end of turn.').
card_image_name('angelic page'/'7ED', 'angelic page').
card_uid('angelic page'/'7ED', '7ED:Angelic Page:angelic page').
card_rarity('angelic page'/'7ED', 'Common').
card_artist('angelic page'/'7ED', 'Marc Fishman').
card_number('angelic page'/'7ED', '1').
card_flavor_text('angelic page'/'7ED', 'If only every message were as perfect as its messenger.').
card_multiverse_id('angelic page'/'7ED', '25546').

card_in_set('arcane laboratory', '7ED').
card_original_type('arcane laboratory'/'7ED', 'Enchantment').
card_original_text('arcane laboratory'/'7ED', 'Each player can\'t play more than one spell each turn.').
card_image_name('arcane laboratory'/'7ED', 'arcane laboratory').
card_uid('arcane laboratory'/'7ED', '7ED:Arcane Laboratory:arcane laboratory').
card_rarity('arcane laboratory'/'7ED', 'Uncommon').
card_artist('arcane laboratory'/'7ED', 'Brian Snõddy').
card_number('arcane laboratory'/'7ED', '60').
card_flavor_text('arcane laboratory'/'7ED', 'Too many wizards spoil the spell, but too many spells spoil the wizard.\n—Sign on entry to arcane lab').
card_multiverse_id('arcane laboratory'/'7ED', '25562').

card_in_set('archivist', '7ED').
card_original_type('archivist'/'7ED', 'Creature — Wizard').
card_original_text('archivist'/'7ED', '{T}: Draw a card.').
card_image_name('archivist'/'7ED', 'archivist').
card_uid('archivist'/'7ED', '7ED:Archivist:archivist').
card_rarity('archivist'/'7ED', 'Rare').
card_artist('archivist'/'7ED', 'Donato Giancola').
card_number('archivist'/'7ED', '61').
card_flavor_text('archivist'/'7ED', '\"Knowledge is a feast for the mind. Savor every swallow.\"').
card_multiverse_id('archivist'/'7ED', '13157').

card_in_set('ardent militia', '7ED').
card_original_type('ardent militia'/'7ED', 'Creature — Soldier').
card_original_text('ardent militia'/'7ED', 'Attacking doesn\'t cause Ardent Militia to tap.').
card_image_name('ardent militia'/'7ED', 'ardent militia').
card_uid('ardent militia'/'7ED', '7ED:Ardent Militia:ardent militia').
card_rarity('ardent militia'/'7ED', 'Uncommon').
card_artist('ardent militia'/'7ED', 'Paolo Parente').
card_number('ardent militia'/'7ED', '2').
card_flavor_text('ardent militia'/'7ED', 'Knights fight for honor and mercenaries fight for gold. The militia fights for hearth and home.').
card_multiverse_id('ardent militia'/'7ED', '13016').

card_in_set('balduvian barbarians', '7ED').
card_original_type('balduvian barbarians'/'7ED', 'Creature — Barbarian').
card_original_text('balduvian barbarians'/'7ED', '').
card_image_name('balduvian barbarians'/'7ED', 'balduvian barbarians').
card_uid('balduvian barbarians'/'7ED', '7ED:Balduvian Barbarians:balduvian barbarians').
card_rarity('balduvian barbarians'/'7ED', 'Common').
card_artist('balduvian barbarians'/'7ED', 'Jim Nelson').
card_number('balduvian barbarians'/'7ED', '173').
card_flavor_text('balduvian barbarians'/'7ED', '\"From the snowy slopes of Kaelor,\nTo the canyons of Bandu,\nWe drink and fight and feast and die\nAs we were born to do.\"\n—Balduvian tavern song').
card_multiverse_id('balduvian barbarians'/'7ED', '25673').

card_in_set('baleful stare', '7ED').
card_original_type('baleful stare'/'7ED', 'Sorcery').
card_original_text('baleful stare'/'7ED', 'Target opponent reveals his or her hand. You draw a card for each mountain and red card in it.').
card_image_name('baleful stare'/'7ED', 'baleful stare').
card_uid('baleful stare'/'7ED', '7ED:Baleful Stare:baleful stare').
card_rarity('baleful stare'/'7ED', 'Uncommon').
card_artist('baleful stare'/'7ED', 'Eric Peterson').
card_number('baleful stare'/'7ED', '62').
card_flavor_text('baleful stare'/'7ED', 'He can drown you in his eyes.').
card_multiverse_id('baleful stare'/'7ED', '13124').

card_in_set('beast of burden', '7ED').
card_original_type('beast of burden'/'7ED', 'Artifact Creature').
card_original_text('beast of burden'/'7ED', 'Beast of Burden\'s power and toughness are each equal to the number of creatures in play.').
card_image_name('beast of burden'/'7ED', 'beast of burden').
card_uid('beast of burden'/'7ED', '7ED:Beast of Burden:beast of burden').
card_rarity('beast of burden'/'7ED', 'Rare').
card_artist('beast of burden'/'7ED', 'Chippy').
card_number('beast of burden'/'7ED', '287').
card_flavor_text('beast of burden'/'7ED', 'The heavier the load, the harder it works.').
card_multiverse_id('beast of burden'/'7ED', '15861').

card_in_set('bedlam', '7ED').
card_original_type('bedlam'/'7ED', 'Enchantment').
card_original_text('bedlam'/'7ED', 'Creatures can\'t block.').
card_image_name('bedlam'/'7ED', 'bedlam').
card_uid('bedlam'/'7ED', '7ED:Bedlam:bedlam').
card_rarity('bedlam'/'7ED', 'Rare').
card_artist('bedlam'/'7ED', 'Ron Spencer').
card_number('bedlam'/'7ED', '174').
card_flavor_text('bedlam'/'7ED', 'The generals were dead. The battle plans were forgotten. It was everyone for themselves.').
card_multiverse_id('bedlam'/'7ED', '25689').

card_in_set('befoul', '7ED').
card_original_type('befoul'/'7ED', 'Sorcery').
card_original_text('befoul'/'7ED', 'Destroy target land or nonblack creature. It can\'t be regenerated.').
card_image_name('befoul'/'7ED', 'befoul').
card_uid('befoul'/'7ED', '7ED:Befoul:befoul').
card_rarity('befoul'/'7ED', 'Uncommon').
card_artist('befoul'/'7ED', 'Ciruelo').
card_number('befoul'/'7ED', '118').
card_flavor_text('befoul'/'7ED', 'The bog\'s touch means death to all but the darkest of hearts.').
card_multiverse_id('befoul'/'7ED', '25632').

card_in_set('bellowing fiend', '7ED').
card_original_type('bellowing fiend'/'7ED', 'Creature — Spirit').
card_original_text('bellowing fiend'/'7ED', 'Flying\nWhenever Bellowing Fiend deals damage to a creature, Bellowing Fiend deals 3 damage to that creature\'s controller and 3 damage to you.').
card_image_name('bellowing fiend'/'7ED', 'bellowing fiend').
card_uid('bellowing fiend'/'7ED', '7ED:Bellowing Fiend:bellowing fiend').
card_rarity('bellowing fiend'/'7ED', 'Rare').
card_artist('bellowing fiend'/'7ED', 'Chippy').
card_number('bellowing fiend'/'7ED', '119').
card_multiverse_id('bellowing fiend'/'7ED', '25647').

card_in_set('benthic behemoth', '7ED').
card_original_type('benthic behemoth'/'7ED', 'Creature — Serpent').
card_original_text('benthic behemoth'/'7ED', 'Islandwalk (This creature is unblockable as long as defending player controls an island.)').
card_image_name('benthic behemoth'/'7ED', 'benthic behemoth').
card_uid('benthic behemoth'/'7ED', '7ED:Benthic Behemoth:benthic behemoth').
card_rarity('benthic behemoth'/'7ED', 'Rare').
card_artist('benthic behemoth'/'7ED', 'Heather Hudson').
card_number('benthic behemoth'/'7ED', '63').
card_flavor_text('benthic behemoth'/'7ED', 'The sea has many secrets, but some are too big to be kept.').
card_multiverse_id('benthic behemoth'/'7ED', '13030').

card_in_set('bereavement', '7ED').
card_original_type('bereavement'/'7ED', 'Enchantment').
card_original_text('bereavement'/'7ED', 'Whenever a green creature is put into a graveyard from play, its controller discards a card from his or her hand.').
card_image_name('bereavement'/'7ED', 'bereavement').
card_uid('bereavement'/'7ED', '7ED:Bereavement:bereavement').
card_rarity('bereavement'/'7ED', 'Uncommon').
card_artist('bereavement'/'7ED', 'Marc Fishman').
card_number('bereavement'/'7ED', '120').
card_flavor_text('bereavement'/'7ED', 'Grief is a razor that slices the soul.').
card_multiverse_id('bereavement'/'7ED', '25629').

card_in_set('birds of paradise', '7ED').
card_original_type('birds of paradise'/'7ED', 'Creature — Bird').
card_original_text('birds of paradise'/'7ED', 'Flying\n{T}: Add one mana of any color to your mana pool.').
card_image_name('birds of paradise'/'7ED', 'birds of paradise').
card_uid('birds of paradise'/'7ED', '7ED:Birds of Paradise:birds of paradise').
card_rarity('birds of paradise'/'7ED', 'Rare').
card_artist('birds of paradise'/'7ED', 'Edward P. Beard, Jr.').
card_number('birds of paradise'/'7ED', '231').
card_multiverse_id('birds of paradise'/'7ED', '11173').

card_in_set('blanchwood armor', '7ED').
card_original_type('blanchwood armor'/'7ED', 'Enchant Creature').
card_original_text('blanchwood armor'/'7ED', 'Enchanted creature gets +1/+1 for each forest you control.').
card_image_name('blanchwood armor'/'7ED', 'blanchwood armor').
card_uid('blanchwood armor'/'7ED', '7ED:Blanchwood Armor:blanchwood armor').
card_rarity('blanchwood armor'/'7ED', 'Uncommon').
card_artist('blanchwood armor'/'7ED', 'Paolo Parente').
card_number('blanchwood armor'/'7ED', '232').
card_flavor_text('blanchwood armor'/'7ED', '\"There\'s never been a dwarven smith who could match the marvels that Gaea herself forges.\"\n—The Elvish Champion').
card_multiverse_id('blanchwood armor'/'7ED', '25588').

card_in_set('blaze', '7ED').
card_original_type('blaze'/'7ED', 'Sorcery').
card_original_text('blaze'/'7ED', 'Blaze deals X damage to target creature or player.').
card_image_name('blaze'/'7ED', 'blaze').
card_uid('blaze'/'7ED', '7ED:Blaze:blaze').
card_rarity('blaze'/'7ED', 'Uncommon').
card_artist('blaze'/'7ED', 'Alex Horley-Orlandelli').
card_number('blaze'/'7ED', '175').
card_flavor_text('blaze'/'7ED', 'Fire never dies alone.').
card_multiverse_id('blaze'/'7ED', '13187').

card_in_set('blessed reversal', '7ED').
card_original_type('blessed reversal'/'7ED', 'Instant').
card_original_text('blessed reversal'/'7ED', 'You gain 3 life for each creature attacking you.').
card_image_name('blessed reversal'/'7ED', 'blessed reversal').
card_uid('blessed reversal'/'7ED', '7ED:Blessed Reversal:blessed reversal').
card_rarity('blessed reversal'/'7ED', 'Rare').
card_artist('blessed reversal'/'7ED', 'Christopher Moeller').
card_number('blessed reversal'/'7ED', '3').
card_flavor_text('blessed reversal'/'7ED', '\"A battle\'s outcome is never certain.\"\n—The Southern Paladin').
card_multiverse_id('blessed reversal'/'7ED', '13153').

card_in_set('blood pet', '7ED').
card_original_type('blood pet'/'7ED', 'Creature — Thrull').
card_original_text('blood pet'/'7ED', 'Sacrifice Blood Pet: Add {B} to your mana pool.').
card_image_name('blood pet'/'7ED', 'blood pet').
card_uid('blood pet'/'7ED', '7ED:Blood Pet:blood pet').
card_rarity('blood pet'/'7ED', 'Common').
card_artist('blood pet'/'7ED', 'Heather Hudson').
card_number('blood pet'/'7ED', '121').
card_flavor_text('blood pet'/'7ED', 'No thrull has ever been burdened with a long life.').
card_multiverse_id('blood pet'/'7ED', '25625').

card_in_set('bloodshot cyclops', '7ED').
card_original_type('bloodshot cyclops'/'7ED', 'Creature — Giant').
card_original_text('bloodshot cyclops'/'7ED', '{T}, Sacrifice a creature: Bloodshot Cyclops deals damage equal to the sacrificed creature\'s power to target creature or player.').
card_image_name('bloodshot cyclops'/'7ED', 'bloodshot cyclops').
card_uid('bloodshot cyclops'/'7ED', '7ED:Bloodshot Cyclops:bloodshot cyclops').
card_rarity('bloodshot cyclops'/'7ED', 'Rare').
card_artist('bloodshot cyclops'/'7ED', 'Daren Bader').
card_number('bloodshot cyclops'/'7ED', '176').
card_flavor_text('bloodshot cyclops'/'7ED', 'After their first encounter, the goblins named him Chuck.').
card_multiverse_id('bloodshot cyclops'/'7ED', '15792').

card_in_set('bog imp', '7ED').
card_original_type('bog imp'/'7ED', 'Creature — Imp').
card_original_text('bog imp'/'7ED', 'Flying').
card_image_name('bog imp'/'7ED', 'bog imp').
card_uid('bog imp'/'7ED', '7ED:Bog Imp:bog imp').
card_rarity('bog imp'/'7ED', 'Common').
card_artist('bog imp'/'7ED', 'Carl Critchlow').
card_number('bog imp'/'7ED', '122').
card_flavor_text('bog imp'/'7ED', 'Think of it as a butcher knife with wings.').
card_multiverse_id('bog imp'/'7ED', '11179').

card_in_set('bog wraith', '7ED').
card_original_type('bog wraith'/'7ED', 'Creature — Wraith').
card_original_text('bog wraith'/'7ED', 'Swampwalk (This creature is unblockable as long as defending player controls a swamp.)').
card_image_name('bog wraith'/'7ED', 'bog wraith').
card_uid('bog wraith'/'7ED', '7ED:Bog Wraith:bog wraith').
card_rarity('bog wraith'/'7ED', 'Uncommon').
card_artist('bog wraith'/'7ED', 'Dave Dorman').
card_number('bog wraith'/'7ED', '123').
card_flavor_text('bog wraith'/'7ED', 'The bodies of its victims are the only tracks it leaves.').
card_multiverse_id('bog wraith'/'7ED', '11181').

card_in_set('boil', '7ED').
card_original_type('boil'/'7ED', 'Instant').
card_original_text('boil'/'7ED', 'Destroy all islands.').
card_image_name('boil'/'7ED', 'boil').
card_uid('boil'/'7ED', '7ED:Boil:boil').
card_rarity('boil'/'7ED', 'Uncommon').
card_artist('boil'/'7ED', 'Christopher Moeller').
card_number('boil'/'7ED', '177').
card_flavor_text('boil'/'7ED', '\"It is a slaughter, the death of a sea. Never speak of it again.\"\n—The Lord of Atlantis').
card_multiverse_id('boil'/'7ED', '25675').

card_in_set('boomerang', '7ED').
card_original_type('boomerang'/'7ED', 'Instant').
card_original_text('boomerang'/'7ED', 'Return target permanent to its owner\'s hand.').
card_image_name('boomerang'/'7ED', 'boomerang').
card_uid('boomerang'/'7ED', '7ED:Boomerang:boomerang').
card_rarity('boomerang'/'7ED', 'Common').
card_artist('boomerang'/'7ED', 'Rebecca Guay').
card_number('boomerang'/'7ED', '64').
card_flavor_text('boomerang'/'7ED', '\"Returne from whence ye came. . . .\"\n—Edmund Spenser,\nThe Faerie Queene').
card_multiverse_id('boomerang'/'7ED', '11182').

card_in_set('breath of life', '7ED').
card_original_type('breath of life'/'7ED', 'Sorcery').
card_original_text('breath of life'/'7ED', 'Return target creature card from your graveyard to play.').
card_image_name('breath of life'/'7ED', 'breath of life').
card_uid('breath of life'/'7ED', '7ED:Breath of Life:breath of life').
card_rarity('breath of life'/'7ED', 'Uncommon').
card_artist('breath of life'/'7ED', 'Roger Raupp').
card_number('breath of life'/'7ED', '4').
card_flavor_text('breath of life'/'7ED', '\"Nothing can stop the power of life—not even death.\"\n—Onean cleric').
card_multiverse_id('breath of life'/'7ED', '13112').

card_in_set('brushland', '7ED').
card_original_type('brushland'/'7ED', 'Land').
card_original_text('brushland'/'7ED', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {G} or {W} to your mana pool. Brushland deals 1 damage to you.').
card_image_name('brushland'/'7ED', 'brushland').
card_uid('brushland'/'7ED', '7ED:Brushland:brushland').
card_rarity('brushland'/'7ED', 'Rare').
card_artist('brushland'/'7ED', 'Scott Bailey').
card_number('brushland'/'7ED', '326').
card_multiverse_id('brushland'/'7ED', '11191').

card_in_set('bull hippo', '7ED').
card_original_type('bull hippo'/'7ED', 'Creature — Hippo').
card_original_text('bull hippo'/'7ED', 'Islandwalk (This creature is unblockable as long as defending player controls an island.)').
card_image_name('bull hippo'/'7ED', 'bull hippo').
card_uid('bull hippo'/'7ED', '7ED:Bull Hippo:bull hippo').
card_rarity('bull hippo'/'7ED', 'Uncommon').
card_artist('bull hippo'/'7ED', 'Heather Hudson').
card_number('bull hippo'/'7ED', '233').
card_flavor_text('bull hippo'/'7ED', 'The merfolk may rule the waters, but hippos have never had much respect for authority.').
card_multiverse_id('bull hippo'/'7ED', '13103').

card_in_set('caltrops', '7ED').
card_original_type('caltrops'/'7ED', 'Artifact').
card_original_text('caltrops'/'7ED', 'Whenever a creature attacks, Caltrops deals 1 damage to it.').
card_image_name('caltrops'/'7ED', 'caltrops').
card_uid('caltrops'/'7ED', '7ED:Caltrops:caltrops').
card_rarity('caltrops'/'7ED', 'Uncommon').
card_artist('caltrops'/'7ED', 'Eric Peterson').
card_number('caltrops'/'7ED', '288').
card_flavor_text('caltrops'/'7ED', 'The Eastern Paladin knew how highly his southern rival valued horses, so he gleefully prepared a suitable welcome.').
card_multiverse_id('caltrops'/'7ED', '25655').

card_in_set('canopy spider', '7ED').
card_original_type('canopy spider'/'7ED', 'Creature — Spider').
card_original_text('canopy spider'/'7ED', 'Canopy Spider may block as though it had flying.').
card_image_name('canopy spider'/'7ED', 'canopy spider').
card_uid('canopy spider'/'7ED', '7ED:Canopy Spider:canopy spider').
card_rarity('canopy spider'/'7ED', 'Common').
card_artist('canopy spider'/'7ED', 'Mike Raabe').
card_number('canopy spider'/'7ED', '234').
card_flavor_text('canopy spider'/'7ED', 'It keeps the upper reaches of the forest free of every menace . . . except for the spider itself.').
card_multiverse_id('canopy spider'/'7ED', '13040').

card_in_set('castle', '7ED').
card_original_type('castle'/'7ED', 'Enchantment').
card_original_text('castle'/'7ED', 'Untapped creatures you control get +0/+2.').
card_image_name('castle'/'7ED', 'castle').
card_uid('castle'/'7ED', '7ED:Castle:castle').
card_rarity('castle'/'7ED', 'Uncommon').
card_artist('castle'/'7ED', 'Edward P. Beard, Jr.').
card_number('castle'/'7ED', '5').
card_flavor_text('castle'/'7ED', '\". . . Our castle\'s strength\nWill laugh a siege to scorn.\"\n—William Shakespeare,\nMacbeth').
card_multiverse_id('castle'/'7ED', '11195').

card_in_set('charcoal diamond', '7ED').
card_original_type('charcoal diamond'/'7ED', 'Artifact').
card_original_text('charcoal diamond'/'7ED', 'Charcoal Diamond comes into play tapped.\n{T}: Add {B} to your mana pool.').
card_image_name('charcoal diamond'/'7ED', 'charcoal diamond').
card_uid('charcoal diamond'/'7ED', '7ED:Charcoal Diamond:charcoal diamond').
card_rarity('charcoal diamond'/'7ED', 'Uncommon').
card_artist('charcoal diamond'/'7ED', 'David Martin').
card_number('charcoal diamond'/'7ED', '289').
card_multiverse_id('charcoal diamond'/'7ED', '15849').

card_in_set('circle of protection: black', '7ED').
card_original_type('circle of protection: black'/'7ED', 'Enchantment').
card_original_text('circle of protection: black'/'7ED', '{1}: The next time a black source of your choice would deal damage to you this turn, prevent that damage.').
card_image_name('circle of protection: black'/'7ED', 'circle of protection black').
card_uid('circle of protection: black'/'7ED', '7ED:Circle of Protection: Black:circle of protection black').
card_rarity('circle of protection: black'/'7ED', 'Common').
card_artist('circle of protection: black'/'7ED', 'Mark Romanoski').
card_number('circle of protection: black'/'7ED', '6').
card_multiverse_id('circle of protection: black'/'7ED', '11200').

card_in_set('circle of protection: blue', '7ED').
card_original_type('circle of protection: blue'/'7ED', 'Enchantment').
card_original_text('circle of protection: blue'/'7ED', '{1}: The next time a blue source of your choice would deal damage to you this turn, prevent that damage.').
card_image_name('circle of protection: blue'/'7ED', 'circle of protection blue').
card_uid('circle of protection: blue'/'7ED', '7ED:Circle of Protection: Blue:circle of protection blue').
card_rarity('circle of protection: blue'/'7ED', 'Common').
card_artist('circle of protection: blue'/'7ED', 'Greg & Tim Hildebrandt').
card_number('circle of protection: blue'/'7ED', '7').
card_multiverse_id('circle of protection: blue'/'7ED', '11201').

card_in_set('circle of protection: green', '7ED').
card_original_type('circle of protection: green'/'7ED', 'Enchantment').
card_original_text('circle of protection: green'/'7ED', '{1}: The next time a green source of your choice would deal damage to you this turn, prevent that damage.').
card_image_name('circle of protection: green'/'7ED', 'circle of protection green').
card_uid('circle of protection: green'/'7ED', '7ED:Circle of Protection: Green:circle of protection green').
card_rarity('circle of protection: green'/'7ED', 'Common').
card_artist('circle of protection: green'/'7ED', 'Alan Pollack').
card_number('circle of protection: green'/'7ED', '8').
card_multiverse_id('circle of protection: green'/'7ED', '11202').

card_in_set('circle of protection: red', '7ED').
card_original_type('circle of protection: red'/'7ED', 'Enchantment').
card_original_text('circle of protection: red'/'7ED', '{1}: The next time a red source of your choice would deal damage to you this turn, prevent that damage.').
card_image_name('circle of protection: red'/'7ED', 'circle of protection red').
card_uid('circle of protection: red'/'7ED', '7ED:Circle of Protection: Red:circle of protection red').
card_rarity('circle of protection: red'/'7ED', 'Common').
card_artist('circle of protection: red'/'7ED', 'Gary Ruddell').
card_number('circle of protection: red'/'7ED', '9').
card_multiverse_id('circle of protection: red'/'7ED', '11203').

card_in_set('circle of protection: white', '7ED').
card_original_type('circle of protection: white'/'7ED', 'Enchantment').
card_original_text('circle of protection: white'/'7ED', '{1}: The next time a white source of your choice would deal damage to you this turn, prevent that damage.').
card_image_name('circle of protection: white'/'7ED', 'circle of protection white').
card_uid('circle of protection: white'/'7ED', '7ED:Circle of Protection: White:circle of protection white').
card_rarity('circle of protection: white'/'7ED', 'Common').
card_artist('circle of protection: white'/'7ED', 'Darrell Riche').
card_number('circle of protection: white'/'7ED', '10').
card_multiverse_id('circle of protection: white'/'7ED', '11204').

card_in_set('city of brass', '7ED').
card_original_type('city of brass'/'7ED', 'Land').
card_original_text('city of brass'/'7ED', 'Whenever City of Brass becomes tapped, it deals 1 damage to you.\n{T}: Add one mana of any color to your mana pool.').
card_image_name('city of brass'/'7ED', 'city of brass').
card_uid('city of brass'/'7ED', '7ED:City of Brass:city of brass').
card_rarity('city of brass'/'7ED', 'Rare').
card_artist('city of brass'/'7ED', 'Ron Walotsky').
card_number('city of brass'/'7ED', '327').
card_multiverse_id('city of brass'/'7ED', '11205').

card_in_set('cloudchaser eagle', '7ED').
card_original_type('cloudchaser eagle'/'7ED', 'Creature — Bird').
card_original_text('cloudchaser eagle'/'7ED', 'Flying\nWhen Cloudchaser Eagle comes into play, destroy target enchantment.').
card_image_name('cloudchaser eagle'/'7ED', 'cloudchaser eagle').
card_uid('cloudchaser eagle'/'7ED', '7ED:Cloudchaser Eagle:cloudchaser eagle').
card_rarity('cloudchaser eagle'/'7ED', 'Common').
card_artist('cloudchaser eagle'/'7ED', 'Aaron Boyd').
card_number('cloudchaser eagle'/'7ED', '11').
card_flavor_text('cloudchaser eagle'/'7ED', 'Most eagles hunt small animals. The cloudchasers hunt magic.').
card_multiverse_id('cloudchaser eagle'/'7ED', '15778').

card_in_set('coat of arms', '7ED').
card_original_type('coat of arms'/'7ED', 'Artifact').
card_original_text('coat of arms'/'7ED', 'Each creature gets +1/+1 for each other creature in play that shares a creature type with it. (For example, if there are three Goblins in play, each gets +2/+2.)').
card_image_name('coat of arms'/'7ED', 'coat of arms').
card_uid('coat of arms'/'7ED', '7ED:Coat of Arms:coat of arms').
card_rarity('coat of arms'/'7ED', 'Rare').
card_artist('coat of arms'/'7ED', 'D. Alexander Gregory').
card_number('coat of arms'/'7ED', '290').
card_multiverse_id('coat of arms'/'7ED', '15863').

card_in_set('compost', '7ED').
card_original_type('compost'/'7ED', 'Enchantment').
card_original_text('compost'/'7ED', 'Whenever a black card is put into an opponent\'s graveyard, you may draw a card.').
card_image_name('compost'/'7ED', 'compost').
card_uid('compost'/'7ED', '7ED:Compost:compost').
card_rarity('compost'/'7ED', 'Uncommon').
card_artist('compost'/'7ED', 'Marc Fishman').
card_number('compost'/'7ED', '235').
card_flavor_text('compost'/'7ED', 'A fruit must rot before its seed can sprout.\n—Yavimaya saying').
card_multiverse_id('compost'/'7ED', '15881').

card_in_set('confiscate', '7ED').
card_original_type('confiscate'/'7ED', 'Enchant Permanent').
card_original_text('confiscate'/'7ED', 'You control enchanted permanent.').
card_image_name('confiscate'/'7ED', 'confiscate').
card_uid('confiscate'/'7ED', '7ED:Confiscate:confiscate').
card_rarity('confiscate'/'7ED', 'Uncommon').
card_artist('confiscate'/'7ED', 'Christopher Moeller').
card_number('confiscate'/'7ED', '65').
card_flavor_text('confiscate'/'7ED', '\"The surface world contains nothing I cannot take.\"\n—The Lord of Atlantis').
card_multiverse_id('confiscate'/'7ED', '13165').

card_in_set('coral merfolk', '7ED').
card_original_type('coral merfolk'/'7ED', 'Creature — Merfolk').
card_original_text('coral merfolk'/'7ED', '').
card_image_name('coral merfolk'/'7ED', 'coral merfolk').
card_uid('coral merfolk'/'7ED', '7ED:Coral Merfolk:coral merfolk').
card_rarity('coral merfolk'/'7ED', 'Common').
card_artist('coral merfolk'/'7ED', 'Rebecca Guay').
card_number('coral merfolk'/'7ED', '66').
card_flavor_text('coral merfolk'/'7ED', 'The coral merfolk delight in wrestling with their foes, skinning them alive on the razor-sharp edges of their reef.').
card_multiverse_id('coral merfolk'/'7ED', '26844').

card_in_set('corrupt', '7ED').
card_original_type('corrupt'/'7ED', 'Sorcery').
card_original_text('corrupt'/'7ED', 'Corrupt deals damage equal to the number of swamps you control to target creature or player. You gain life equal to the damage dealt this way.').
card_image_name('corrupt'/'7ED', 'corrupt').
card_uid('corrupt'/'7ED', '7ED:Corrupt:corrupt').
card_rarity('corrupt'/'7ED', 'Common').
card_artist('corrupt'/'7ED', 'Scott M. Fischer').
card_number('corrupt'/'7ED', '124').
card_multiverse_id('corrupt'/'7ED', '25624').

card_in_set('counterspell', '7ED').
card_original_type('counterspell'/'7ED', 'Instant').
card_original_text('counterspell'/'7ED', 'Counter target spell.').
card_image_name('counterspell'/'7ED', 'counterspell').
card_uid('counterspell'/'7ED', '7ED:Counterspell:counterspell').
card_rarity('counterspell'/'7ED', 'Common').
card_artist('counterspell'/'7ED', 'Mark Romanoski').
card_number('counterspell'/'7ED', '67').
card_multiverse_id('counterspell'/'7ED', '11214').

card_in_set('creeping mold', '7ED').
card_original_type('creeping mold'/'7ED', 'Sorcery').
card_original_text('creeping mold'/'7ED', 'Destroy target artifact, enchantment, or land.').
card_image_name('creeping mold'/'7ED', 'creeping mold').
card_uid('creeping mold'/'7ED', '7ED:Creeping Mold:creeping mold').
card_rarity('creeping mold'/'7ED', 'Uncommon').
card_artist('creeping mold'/'7ED', 'Gary Ruddell').
card_number('creeping mold'/'7ED', '236').
card_flavor_text('creeping mold'/'7ED', 'Mold crept over the walls and into every crevice until the gleaming white stone strained and burst.').
card_multiverse_id('creeping mold'/'7ED', '25666').

card_in_set('crimson hellkite', '7ED').
card_original_type('crimson hellkite'/'7ED', 'Creature — Dragon').
card_original_text('crimson hellkite'/'7ED', 'Flying\n{X}, {T}: Crimson Hellkite deals X damage to target creature. Spend only red mana this way.').
card_image_name('crimson hellkite'/'7ED', 'crimson hellkite').
card_uid('crimson hellkite'/'7ED', '7ED:Crimson Hellkite:crimson hellkite').
card_rarity('crimson hellkite'/'7ED', 'Rare').
card_artist('crimson hellkite'/'7ED', 'Carl Critchlow').
card_number('crimson hellkite'/'7ED', '178').
card_flavor_text('crimson hellkite'/'7ED', 'The hellkites are artists with flames, painting the world with fire.').
card_multiverse_id('crimson hellkite'/'7ED', '15814').

card_in_set('crossbow infantry', '7ED').
card_original_type('crossbow infantry'/'7ED', 'Creature — Soldier').
card_original_text('crossbow infantry'/'7ED', '{T}: Crossbow Infantry deals 1 damage to target attacking or blocking creature.').
card_image_name('crossbow infantry'/'7ED', 'crossbow infantry').
card_uid('crossbow infantry'/'7ED', '7ED:Crossbow Infantry:crossbow infantry').
card_rarity('crossbow infantry'/'7ED', 'Common').
card_artist('crossbow infantry'/'7ED', 'James Bernardin').
card_number('crossbow infantry'/'7ED', '12').
card_flavor_text('crossbow infantry'/'7ED', 'Twang. You\'re dead.').
card_multiverse_id('crossbow infantry'/'7ED', '26841').

card_in_set('crypt rats', '7ED').
card_original_type('crypt rats'/'7ED', 'Creature — Rat').
card_original_text('crypt rats'/'7ED', '{X} Crypt Rats deals X damage to each creature and each player. Spend only black mana this way.').
card_image_name('crypt rats'/'7ED', 'crypt rats').
card_uid('crypt rats'/'7ED', '7ED:Crypt Rats:crypt rats').
card_rarity('crypt rats'/'7ED', 'Uncommon').
card_artist('crypt rats'/'7ED', 'Matt Cavotta').
card_number('crypt rats'/'7ED', '125').
card_flavor_text('crypt rats'/'7ED', 'Beware anything born among the dead.\n—Tolarian saying').
card_multiverse_id('crypt rats'/'7ED', '25637').

card_in_set('crystal rod', '7ED').
card_original_type('crystal rod'/'7ED', 'Artifact').
card_original_text('crystal rod'/'7ED', 'Whenever a player plays a blue spell, you may pay {1}. If you do, you gain 1 life.').
card_image_name('crystal rod'/'7ED', 'crystal rod').
card_uid('crystal rod'/'7ED', '7ED:Crystal Rod:crystal rod').
card_rarity('crystal rod'/'7ED', 'Uncommon').
card_artist('crystal rod'/'7ED', 'Ciruelo').
card_number('crystal rod'/'7ED', '291').
card_flavor_text('crystal rod'/'7ED', 'Polished by a thousand waves.').
card_multiverse_id('crystal rod'/'7ED', '25648').

card_in_set('dakmor lancer', '7ED').
card_original_type('dakmor lancer'/'7ED', 'Creature — Knight').
card_original_text('dakmor lancer'/'7ED', 'When Dakmor Lancer comes into play, destroy target nonblack creature.').
card_image_name('dakmor lancer'/'7ED', 'dakmor lancer').
card_uid('dakmor lancer'/'7ED', '7ED:Dakmor Lancer:dakmor lancer').
card_rarity('dakmor lancer'/'7ED', 'Uncommon').
card_artist('dakmor lancer'/'7ED', 'Luca Zontini').
card_number('dakmor lancer'/'7ED', '126').
card_flavor_text('dakmor lancer'/'7ED', 'The darkness of his shield reflects the inky blackness of his soul.').
card_multiverse_id('dakmor lancer'/'7ED', '25640').

card_in_set('daring apprentice', '7ED').
card_original_type('daring apprentice'/'7ED', 'Creature — Wizard').
card_original_text('daring apprentice'/'7ED', '{T}, Sacrifice Daring Apprentice: Counter target spell.').
card_image_name('daring apprentice'/'7ED', 'daring apprentice').
card_uid('daring apprentice'/'7ED', '7ED:Daring Apprentice:daring apprentice').
card_rarity('daring apprentice'/'7ED', 'Rare').
card_artist('daring apprentice'/'7ED', 'Orizio Daniele').
card_number('daring apprentice'/'7ED', '68').
card_flavor_text('daring apprentice'/'7ED', '\"I\'ve seen my master do this a hundred times. How hard can it be?\"').
card_multiverse_id('daring apprentice'/'7ED', '12960').

card_in_set('dark banishing', '7ED').
card_original_type('dark banishing'/'7ED', 'Instant').
card_original_text('dark banishing'/'7ED', 'Destroy target nonblack creature. It can\'t be regenerated.').
card_image_name('dark banishing'/'7ED', 'dark banishing').
card_uid('dark banishing'/'7ED', '7ED:Dark Banishing:dark banishing').
card_rarity('dark banishing'/'7ED', 'Common').
card_artist('dark banishing'/'7ED', 'Rebecca Guay').
card_number('dark banishing'/'7ED', '127').
card_flavor_text('dark banishing'/'7ED', '\"Hence ‘banishèd\' is banished from the world, / And the world\'s exile is death.\"\n—William Shakespeare,\nRomeo and Juliet').
card_multiverse_id('dark banishing'/'7ED', '14754').

card_in_set('darkest hour', '7ED').
card_original_type('darkest hour'/'7ED', 'Enchantment').
card_original_text('darkest hour'/'7ED', 'All creatures are black.').
card_image_name('darkest hour'/'7ED', 'darkest hour').
card_uid('darkest hour'/'7ED', '7ED:Darkest Hour:darkest hour').
card_rarity('darkest hour'/'7ED', 'Rare').
card_artist('darkest hour'/'7ED', 'Ciruelo').
card_number('darkest hour'/'7ED', '128').
card_flavor_text('darkest hour'/'7ED', 'The eclipse made it clear whose side the sky was on.').
card_multiverse_id('darkest hour'/'7ED', '15825').

card_in_set('deflection', '7ED').
card_original_type('deflection'/'7ED', 'Instant').
card_original_text('deflection'/'7ED', 'Change the target of target spell with a single target.').
card_image_name('deflection'/'7ED', 'deflection').
card_uid('deflection'/'7ED', '7ED:Deflection:deflection').
card_rarity('deflection'/'7ED', 'Rare').
card_artist('deflection'/'7ED', 'Jeff Easley').
card_number('deflection'/'7ED', '69').
card_flavor_text('deflection'/'7ED', '\"You\'ve thrown your net at the wrong fish.\"\n—The Lord of Atlantis').
card_multiverse_id('deflection'/'7ED', '11232').

card_in_set('delusions of mediocrity', '7ED').
card_original_type('delusions of mediocrity'/'7ED', 'Enchantment').
card_original_text('delusions of mediocrity'/'7ED', 'When Delusions of Mediocrity comes into play, you gain 10 life.\nWhen Delusions of Mediocrity leaves play, you lose 10 life.').
card_image_name('delusions of mediocrity'/'7ED', 'delusions of mediocrity').
card_uid('delusions of mediocrity'/'7ED', '7ED:Delusions of Mediocrity:delusions of mediocrity').
card_rarity('delusions of mediocrity'/'7ED', 'Rare').
card_artist('delusions of mediocrity'/'7ED', 'Terese Nielsen').
card_number('delusions of mediocrity'/'7ED', '70').
card_flavor_text('delusions of mediocrity'/'7ED', 'When nothing but second best will do.').
card_multiverse_id('delusions of mediocrity'/'7ED', '13152').

card_in_set('dingus egg', '7ED').
card_original_type('dingus egg'/'7ED', 'Artifact').
card_original_text('dingus egg'/'7ED', 'Whenever a land is put into a graveyard from play, Dingus Egg deals 2 damage to that land\'s controller.').
card_image_name('dingus egg'/'7ED', 'dingus egg').
card_uid('dingus egg'/'7ED', '7ED:Dingus Egg:dingus egg').
card_rarity('dingus egg'/'7ED', 'Rare').
card_artist('dingus egg'/'7ED', 'Tony Szczudlo').
card_number('dingus egg'/'7ED', '292').
card_flavor_text('dingus egg'/'7ED', 'Legend has it that the world was hatched from a dingus egg.').
card_multiverse_id('dingus egg'/'7ED', '15864').

card_in_set('disenchant', '7ED').
card_original_type('disenchant'/'7ED', 'Instant').
card_original_text('disenchant'/'7ED', 'Destroy target artifact or enchantment.').
card_image_name('disenchant'/'7ED', 'disenchant').
card_uid('disenchant'/'7ED', '7ED:Disenchant:disenchant').
card_rarity('disenchant'/'7ED', 'Common').
card_artist('disenchant'/'7ED', 'Andrew Goldhawk').
card_number('disenchant'/'7ED', '13').
card_multiverse_id('disenchant'/'7ED', '11238').

card_in_set('disorder', '7ED').
card_original_type('disorder'/'7ED', 'Sorcery').
card_original_text('disorder'/'7ED', 'Disorder deals 2 damage to each white creature and each player who controls a white creature.').
card_image_name('disorder'/'7ED', 'disorder').
card_uid('disorder'/'7ED', '7ED:Disorder:disorder').
card_rarity('disorder'/'7ED', 'Uncommon').
card_artist('disorder'/'7ED', 'Glen Angus').
card_number('disorder'/'7ED', '179').
card_flavor_text('disorder'/'7ED', '\"Fire hurts them almost as much as the chaos that follows. Almost.\"\n—The Western Paladin').
card_multiverse_id('disorder'/'7ED', '13136').

card_in_set('disrupting scepter', '7ED').
card_original_type('disrupting scepter'/'7ED', 'Artifact').
card_original_text('disrupting scepter'/'7ED', '{3}, {T}: Target player discards a card from his or her hand. Play this ability only during your turn.').
card_image_name('disrupting scepter'/'7ED', 'disrupting scepter').
card_uid('disrupting scepter'/'7ED', '7ED:Disrupting Scepter:disrupting scepter').
card_rarity('disrupting scepter'/'7ED', 'Rare').
card_artist('disrupting scepter'/'7ED', 'Darrell Riche').
card_number('disrupting scepter'/'7ED', '293').
card_multiverse_id('disrupting scepter'/'7ED', '15868').

card_in_set('dregs of sorrow', '7ED').
card_original_type('dregs of sorrow'/'7ED', 'Sorcery').
card_original_text('dregs of sorrow'/'7ED', 'Destroy X target nonblack creatures. Draw X cards.').
card_image_name('dregs of sorrow'/'7ED', 'dregs of sorrow').
card_uid('dregs of sorrow'/'7ED', '7ED:Dregs of Sorrow:dregs of sorrow').
card_rarity('dregs of sorrow'/'7ED', 'Rare').
card_artist('dregs of sorrow'/'7ED', 'Massimilano Frezzato').
card_number('dregs of sorrow'/'7ED', '129').
card_flavor_text('dregs of sorrow'/'7ED', '\"We live for a thousand years. The death of even one brings untold sorrow.\"\n—Elvish elder').
card_multiverse_id('dregs of sorrow'/'7ED', '13021').

card_in_set('drudge skeletons', '7ED').
card_original_type('drudge skeletons'/'7ED', 'Creature — Skeleton').
card_original_text('drudge skeletons'/'7ED', '{B} Regenerate Drudge Skeletons.').
card_image_name('drudge skeletons'/'7ED', 'drudge skeletons').
card_uid('drudge skeletons'/'7ED', '7ED:Drudge Skeletons:drudge skeletons').
card_rarity('drudge skeletons'/'7ED', 'Common').
card_artist('drudge skeletons'/'7ED', 'Jim Nelson').
card_number('drudge skeletons'/'7ED', '130').
card_flavor_text('drudge skeletons'/'7ED', '\"The dead make good soldiers. They can\'t disobey orders, they never surrender, and they don\'t stop fighting when a random body part falls off.\"\n—Nevinyrral,\nNecromancer\'s Handbook').
card_multiverse_id('drudge skeletons'/'7ED', '11246').

card_in_set('duress', '7ED').
card_original_type('duress'/'7ED', 'Sorcery').
card_original_text('duress'/'7ED', 'Target opponent reveals his or her hand. Choose a noncreature, nonland card from it. That player discards that card.').
card_image_name('duress'/'7ED', 'duress').
card_uid('duress'/'7ED', '7ED:Duress:duress').
card_rarity('duress'/'7ED', 'Common').
card_artist('duress'/'7ED', 'Pete Venters').
card_number('duress'/'7ED', '131').
card_flavor_text('duress'/'7ED', '\"Change your mind, or I\'ll change it for you.\"').
card_multiverse_id('duress'/'7ED', '14557').

card_in_set('eager cadet', '7ED').
card_original_type('eager cadet'/'7ED', 'Creature — Soldier').
card_original_text('eager cadet'/'7ED', '').
card_image_name('eager cadet'/'7ED', 'eager cadet').
card_uid('eager cadet'/'7ED', '7ED:Eager Cadet:eager cadet').
card_rarity('eager cadet'/'7ED', 'Common').
card_artist('eager cadet'/'7ED', 'Greg & Tim Hildebrandt').
card_number('eager cadet'/'7ED', '14').
card_flavor_text('eager cadet'/'7ED', '\"Training? Seeing my crops burnt to cinders was all the ‘training\' I needed.\"').
card_multiverse_id('eager cadet'/'7ED', '15780').

card_in_set('early harvest', '7ED').
card_original_type('early harvest'/'7ED', 'Instant').
card_original_text('early harvest'/'7ED', 'Target player untaps all basic lands he or she controls.').
card_image_name('early harvest'/'7ED', 'early harvest').
card_uid('early harvest'/'7ED', '7ED:Early Harvest:early harvest').
card_rarity('early harvest'/'7ED', 'Rare').
card_artist('early harvest'/'7ED', 'Heather Hudson').
card_number('early harvest'/'7ED', '237').
card_flavor_text('early harvest'/'7ED', '\"Give it a week and it will feed an army.\"').
card_multiverse_id('early harvest'/'7ED', '12962').

card_in_set('earthquake', '7ED').
card_original_type('earthquake'/'7ED', 'Sorcery').
card_original_text('earthquake'/'7ED', 'Earthquake deals X damage to each creature without flying and each player.').
card_image_name('earthquake'/'7ED', 'earthquake').
card_uid('earthquake'/'7ED', '7ED:Earthquake:earthquake').
card_rarity('earthquake'/'7ED', 'Rare').
card_artist('earthquake'/'7ED', 'Franz Vohwinkel').
card_number('earthquake'/'7ED', '180').
card_multiverse_id('earthquake'/'7ED', '11254').

card_in_set('eastern paladin', '7ED').
card_original_type('eastern paladin'/'7ED', 'Creature — Knight').
card_original_text('eastern paladin'/'7ED', '{B}{B}, {T}: Destroy target green creature.').
card_image_name('eastern paladin'/'7ED', 'eastern paladin').
card_uid('eastern paladin'/'7ED', '7ED:Eastern Paladin:eastern paladin').
card_rarity('eastern paladin'/'7ED', 'Rare').
card_artist('eastern paladin'/'7ED', 'Kev Walker').
card_number('eastern paladin'/'7ED', '132').
card_flavor_text('eastern paladin'/'7ED', '\"There is no worse enemy. His passion for death doesn\'t just border on insanity, it wallows in it.\"\n—The Southern Paladin').
card_multiverse_id('eastern paladin'/'7ED', '13137').

card_in_set('elder druid', '7ED').
card_original_type('elder druid'/'7ED', 'Creature — Cleric').
card_original_text('elder druid'/'7ED', '{3}{G}, {T}: Tap or untap target artifact, creature, or land.').
card_image_name('elder druid'/'7ED', 'elder druid').
card_uid('elder druid'/'7ED', '7ED:Elder Druid:elder druid').
card_rarity('elder druid'/'7ED', 'Rare').
card_artist('elder druid'/'7ED', 'Alan Pollack').
card_number('elder druid'/'7ED', '238').
card_flavor_text('elder druid'/'7ED', '\"I may seem wise to you, but I\'m merely a sapling among mighty oaks.\"').
card_multiverse_id('elder druid'/'7ED', '11256').

card_in_set('elite archers', '7ED').
card_original_type('elite archers'/'7ED', 'Creature — Soldier').
card_original_text('elite archers'/'7ED', '{T}: Elite Archers deals 3 damage to target attacking or blocking creature.').
card_image_name('elite archers'/'7ED', 'elite archers').
card_uid('elite archers'/'7ED', '7ED:Elite Archers:elite archers').
card_rarity('elite archers'/'7ED', 'Rare').
card_artist('elite archers'/'7ED', 'Dan Frazier').
card_number('elite archers'/'7ED', '15').
card_flavor_text('elite archers'/'7ED', 'They fletch their arrows with the feathers of angels.').
card_multiverse_id('elite archers'/'7ED', '13129').

card_in_set('elvish archers', '7ED').
card_original_type('elvish archers'/'7ED', 'Creature — Elf').
card_original_text('elvish archers'/'7ED', 'First strike').
card_image_name('elvish archers'/'7ED', 'elvish archers').
card_uid('elvish archers'/'7ED', '7ED:Elvish Archers:elvish archers').
card_rarity('elvish archers'/'7ED', 'Rare').
card_artist('elvish archers'/'7ED', 'Doug Chaffee').
card_number('elvish archers'/'7ED', '239').
card_flavor_text('elvish archers'/'7ED', 'The archers have a reputation for attacking without warning. They claim that the first arrow is the warning.').
card_multiverse_id('elvish archers'/'7ED', '25593').

card_in_set('elvish champion', '7ED').
card_original_type('elvish champion'/'7ED', 'Creature — Lord').
card_original_text('elvish champion'/'7ED', 'All Elves get +1/+1 and have forestwalk. (They\'re unblockable as long as defending player controls a forest.)').
card_image_name('elvish champion'/'7ED', 'elvish champion').
card_uid('elvish champion'/'7ED', '7ED:Elvish Champion:elvish champion').
card_rarity('elvish champion'/'7ED', 'Rare').
card_artist('elvish champion'/'7ED', 'Paolo Parente').
card_number('elvish champion'/'7ED', '240').
card_flavor_text('elvish champion'/'7ED', '\"For what are leaves but countless blades\nTo fight a countless foe on high.\"\n—Elvish hymn').
card_multiverse_id('elvish champion'/'7ED', '11582').

card_in_set('elvish lyrist', '7ED').
card_original_type('elvish lyrist'/'7ED', 'Creature — Elf').
card_original_text('elvish lyrist'/'7ED', '{G}, {T}, Sacrifice Elvish Lyrist: Destroy target enchantment.').
card_image_name('elvish lyrist'/'7ED', 'elvish lyrist').
card_uid('elvish lyrist'/'7ED', '7ED:Elvish Lyrist:elvish lyrist').
card_rarity('elvish lyrist'/'7ED', 'Uncommon').
card_artist('elvish lyrist'/'7ED', 'Michael Koelsch').
card_number('elvish lyrist'/'7ED', '241').
card_flavor_text('elvish lyrist'/'7ED', 'To an elf, magic and music are the same.').
card_multiverse_id('elvish lyrist'/'7ED', '11482').

card_in_set('elvish piper', '7ED').
card_original_type('elvish piper'/'7ED', 'Creature — Elf').
card_original_text('elvish piper'/'7ED', '{G}, {T}: Put a creature card from your hand into play.').
card_image_name('elvish piper'/'7ED', 'elvish piper').
card_uid('elvish piper'/'7ED', '7ED:Elvish Piper:elvish piper').
card_rarity('elvish piper'/'7ED', 'Rare').
card_artist('elvish piper'/'7ED', 'Tristan Elwell').
card_number('elvish piper'/'7ED', '242').
card_flavor_text('elvish piper'/'7ED', 'From Gaea grew the world, and the world was silent. From Gaea grew the world\'s elves, and the world was silent no more.\n—Elvish teaching').
card_multiverse_id('elvish piper'/'7ED', '13197').

card_in_set('engineered plague', '7ED').
card_original_type('engineered plague'/'7ED', 'Enchantment').
card_original_text('engineered plague'/'7ED', 'As Engineered Plague comes into play, choose a creature type.\nAll creatures of the chosen type get -1/-1.').
card_image_name('engineered plague'/'7ED', 'engineered plague').
card_uid('engineered plague'/'7ED', '7ED:Engineered Plague:engineered plague').
card_rarity('engineered plague'/'7ED', 'Uncommon').
card_artist('engineered plague'/'7ED', 'Andrew Goldhawk').
card_number('engineered plague'/'7ED', '133').
card_flavor_text('engineered plague'/'7ED', 'A single germ can kill more soldiers than ten thousand blades.').
card_multiverse_id('engineered plague'/'7ED', '13097').

card_in_set('ensnaring bridge', '7ED').
card_original_type('ensnaring bridge'/'7ED', 'Artifact').
card_original_text('ensnaring bridge'/'7ED', 'Creatures with power greater than the number of cards in your hand can\'t attack.').
card_image_name('ensnaring bridge'/'7ED', 'ensnaring bridge').
card_uid('ensnaring bridge'/'7ED', '7ED:Ensnaring Bridge:ensnaring bridge').
card_rarity('ensnaring bridge'/'7ED', 'Rare').
card_artist('ensnaring bridge'/'7ED', 'Ron Spencer').
card_number('ensnaring bridge'/'7ED', '294').
card_flavor_text('ensnaring bridge'/'7ED', 'Bridges usually get you from one place to another. This one just gets you.').
card_multiverse_id('ensnaring bridge'/'7ED', '15866').

card_in_set('equilibrium', '7ED').
card_original_type('equilibrium'/'7ED', 'Enchantment').
card_original_text('equilibrium'/'7ED', 'Whenever you play a creature spell, you may pay {1}. If you do, return target creature to its owner\'s hand.').
card_image_name('equilibrium'/'7ED', 'equilibrium').
card_uid('equilibrium'/'7ED', '7ED:Equilibrium:equilibrium').
card_rarity('equilibrium'/'7ED', 'Rare').
card_artist('equilibrium'/'7ED', 'Don Hazeltine').
card_number('equilibrium'/'7ED', '71').
card_flavor_text('equilibrium'/'7ED', 'Seek balance in all things—as long as the scales are weighted in your favor.\n—Vodalian saying').
card_multiverse_id('equilibrium'/'7ED', '25568').

card_in_set('evacuation', '7ED').
card_original_type('evacuation'/'7ED', 'Instant').
card_original_text('evacuation'/'7ED', 'Return all creatures to their owners\' hands.').
card_image_name('evacuation'/'7ED', 'evacuation').
card_uid('evacuation'/'7ED', '7ED:Evacuation:evacuation').
card_rarity('evacuation'/'7ED', 'Rare').
card_artist('evacuation'/'7ED', 'Darrell Riche').
card_number('evacuation'/'7ED', '72').
card_flavor_text('evacuation'/'7ED', '\"How many people can you save in that thing?\"\n\"All of them,\" replied the mage with a smile.').
card_multiverse_id('evacuation'/'7ED', '13075').

card_in_set('fallen angel', '7ED').
card_original_type('fallen angel'/'7ED', 'Creature — Angel').
card_original_text('fallen angel'/'7ED', 'Flying\nSacrifice a creature: Fallen Angel gets +2/+1 until end of turn.').
card_image_name('fallen angel'/'7ED', 'fallen angel').
card_uid('fallen angel'/'7ED', '7ED:Fallen Angel:fallen angel').
card_rarity('fallen angel'/'7ED', 'Rare').
card_artist('fallen angel'/'7ED', 'Arnie Swekel').
card_number('fallen angel'/'7ED', '134').
card_flavor_text('fallen angel'/'7ED', '\"Why do you weep for the dead? I rejoice, for they have died for me.\"\n—Fallen angel, to Serra').
card_multiverse_id('fallen angel'/'7ED', '11268').

card_in_set('familiar ground', '7ED').
card_original_type('familiar ground'/'7ED', 'Enchantment').
card_original_text('familiar ground'/'7ED', 'Each creature you control can\'t be blocked by more than one creature.').
card_image_name('familiar ground'/'7ED', 'familiar ground').
card_uid('familiar ground'/'7ED', '7ED:Familiar Ground:familiar ground').
card_rarity('familiar ground'/'7ED', 'Uncommon').
card_artist('familiar ground'/'7ED', 'Thomas Gianni').
card_number('familiar ground'/'7ED', '243').
card_flavor_text('familiar ground'/'7ED', 'More than any other battlefield, the forest gives the advantage to those who know it best.').
card_multiverse_id('familiar ground'/'7ED', '13191').

card_in_set('fear', '7ED').
card_original_type('fear'/'7ED', 'Enchant Creature').
card_original_text('fear'/'7ED', 'Enchanted creature can\'t be blocked except by artifact creatures and/or black creatures.').
card_image_name('fear'/'7ED', 'fear').
card_uid('fear'/'7ED', '7ED:Fear:fear').
card_rarity('fear'/'7ED', 'Common').
card_artist('fear'/'7ED', 'Adam Rex').
card_number('fear'/'7ED', '135').
card_flavor_text('fear'/'7ED', '\"Booga booga booga!\"').
card_multiverse_id('fear'/'7ED', '11269').

card_in_set('femeref archers', '7ED').
card_original_type('femeref archers'/'7ED', 'Creature — Soldier').
card_original_text('femeref archers'/'7ED', '{T}: Femeref Archers deals 4 damage to target attacking creature with flying.').
card_image_name('femeref archers'/'7ED', 'femeref archers').
card_uid('femeref archers'/'7ED', '7ED:Femeref Archers:femeref archers').
card_rarity('femeref archers'/'7ED', 'Uncommon').
card_artist('femeref archers'/'7ED', 'Gary Ruddell').
card_number('femeref archers'/'7ED', '244').
card_flavor_text('femeref archers'/'7ED', '\"Bet you can\'t put it through the eye.\"\n\"Left or right?\"').
card_multiverse_id('femeref archers'/'7ED', '12963').

card_in_set('feroz\'s ban', '7ED').
card_original_type('feroz\'s ban'/'7ED', 'Artifact').
card_original_text('feroz\'s ban'/'7ED', 'Creature spells cost {2} more to play.').
card_image_name('feroz\'s ban'/'7ED', 'feroz\'s ban').
card_uid('feroz\'s ban'/'7ED', '7ED:Feroz\'s Ban:feroz\'s ban').
card_rarity('feroz\'s ban'/'7ED', 'Rare').
card_artist('feroz\'s ban'/'7ED', 'Donato Giancola').
card_number('feroz\'s ban'/'7ED', '295').
card_flavor_text('feroz\'s ban'/'7ED', 'Feroz is gone, but what remains is his final gift to his people: the right to be left alone.').
card_multiverse_id('feroz\'s ban'/'7ED', '25664').

card_in_set('fervor', '7ED').
card_original_type('fervor'/'7ED', 'Enchantment').
card_original_text('fervor'/'7ED', 'Creatures you control have haste. (They may attack and {T} the turn they come under your control.)').
card_image_name('fervor'/'7ED', 'fervor').
card_uid('fervor'/'7ED', '7ED:Fervor:fervor').
card_rarity('fervor'/'7ED', 'Rare').
card_artist('fervor'/'7ED', 'Wayne England').
card_number('fervor'/'7ED', '181').
card_flavor_text('fervor'/'7ED', 'The intensity of their glare was soon surpassed by the intensity of their attack.').
card_multiverse_id('fervor'/'7ED', '13015').

card_in_set('fighting drake', '7ED').
card_original_type('fighting drake'/'7ED', 'Creature — Drake').
card_original_text('fighting drake'/'7ED', 'Flying').
card_image_name('fighting drake'/'7ED', 'fighting drake').
card_uid('fighting drake'/'7ED', '7ED:Fighting Drake:fighting drake').
card_rarity('fighting drake'/'7ED', 'Uncommon').
card_artist('fighting drake'/'7ED', 'Matt Cavotta').
card_number('fighting drake'/'7ED', '73').
card_flavor_text('fighting drake'/'7ED', 'Scholars in their ivory towers call them \"sharks of the sky.\" Scholars on the road don\'t call them at all.').
card_multiverse_id('fighting drake'/'7ED', '13031').

card_in_set('final fortune', '7ED').
card_original_type('final fortune'/'7ED', 'Instant').
card_original_text('final fortune'/'7ED', 'Take an extra turn after this one. At the end of that turn, you lose the game.').
card_image_name('final fortune'/'7ED', 'final fortune').
card_uid('final fortune'/'7ED', '7ED:Final Fortune:final fortune').
card_rarity('final fortune'/'7ED', 'Rare').
card_artist('final fortune'/'7ED', 'Greg & Tim Hildebrandt').
card_number('final fortune'/'7ED', '182').
card_flavor_text('final fortune'/'7ED', 'A reward is worthless if you aren\'t around to enjoy it.').
card_multiverse_id('final fortune'/'7ED', '12970').

card_in_set('fire diamond', '7ED').
card_original_type('fire diamond'/'7ED', 'Artifact').
card_original_text('fire diamond'/'7ED', 'Fire Diamond comes into play tapped.\n{T}: Add {R} to your mana pool.').
card_image_name('fire diamond'/'7ED', 'fire diamond').
card_uid('fire diamond'/'7ED', '7ED:Fire Diamond:fire diamond').
card_rarity('fire diamond'/'7ED', 'Uncommon').
card_artist('fire diamond'/'7ED', 'David Martin').
card_number('fire diamond'/'7ED', '296').
card_multiverse_id('fire diamond'/'7ED', '15853').

card_in_set('fire elemental', '7ED').
card_original_type('fire elemental'/'7ED', 'Creature — Elemental').
card_original_text('fire elemental'/'7ED', '').
card_image_name('fire elemental'/'7ED', 'fire elemental').
card_uid('fire elemental'/'7ED', '7ED:Fire Elemental:fire elemental').
card_rarity('fire elemental'/'7ED', 'Uncommon').
card_artist('fire elemental'/'7ED', 'Douglas Shuler').
card_number('fire elemental'/'7ED', '183').
card_flavor_text('fire elemental'/'7ED', 'It treads on feet of coals and strikes with the force of a volcano.').
card_multiverse_id('fire elemental'/'7ED', '15813').

card_in_set('fleeting image', '7ED').
card_original_type('fleeting image'/'7ED', 'Creature — Illusion').
card_original_text('fleeting image'/'7ED', 'Flying\n{1}{U} Return Fleeting Image to its owner\'s hand.').
card_image_name('fleeting image'/'7ED', 'fleeting image').
card_uid('fleeting image'/'7ED', '7ED:Fleeting Image:fleeting image').
card_rarity('fleeting image'/'7ED', 'Rare').
card_artist('fleeting image'/'7ED', 'Dave Dorman').
card_number('fleeting image'/'7ED', '74').
card_flavor_text('fleeting image'/'7ED', '\"It was as if we fought a shadow.\"\n—Onean soldier').
card_multiverse_id('fleeting image'/'7ED', '13156').

card_in_set('flight', '7ED').
card_original_type('flight'/'7ED', 'Enchant Creature').
card_original_text('flight'/'7ED', 'Enchanted creature has flying.').
card_image_name('flight'/'7ED', 'flight').
card_uid('flight'/'7ED', '7ED:Flight:flight').
card_rarity('flight'/'7ED', 'Common').
card_artist('flight'/'7ED', 'Bradley Williams').
card_number('flight'/'7ED', '75').
card_multiverse_id('flight'/'7ED', '11280').

card_in_set('flying carpet', '7ED').
card_original_type('flying carpet'/'7ED', 'Artifact').
card_original_text('flying carpet'/'7ED', '{2}, {T}: Target creature gains flying until end of turn.').
card_image_name('flying carpet'/'7ED', 'flying carpet').
card_uid('flying carpet'/'7ED', '7ED:Flying Carpet:flying carpet').
card_rarity('flying carpet'/'7ED', 'Rare').
card_artist('flying carpet'/'7ED', 'Scott M. Fischer').
card_number('flying carpet'/'7ED', '297').
card_flavor_text('flying carpet'/'7ED', '\"Wheeeeeeeeeeeeeee!\"').
card_multiverse_id('flying carpet'/'7ED', '11282').

card_in_set('fog', '7ED').
card_original_type('fog'/'7ED', 'Instant').
card_original_text('fog'/'7ED', 'Prevent all combat damage that would be dealt this turn.').
card_image_name('fog'/'7ED', 'fog').
card_uid('fog'/'7ED', '7ED:Fog:fog').
card_rarity('fog'/'7ED', 'Common').
card_artist('fog'/'7ED', 'Arnie Swekel').
card_number('fog'/'7ED', '245').
card_multiverse_id('fog'/'7ED', '11283').

card_in_set('force spike', '7ED').
card_original_type('force spike'/'7ED', 'Instant').
card_original_text('force spike'/'7ED', 'Counter target spell unless its controller pays {1}.').
card_image_name('force spike'/'7ED', 'force spike').
card_uid('force spike'/'7ED', '7ED:Force Spike:force spike').
card_rarity('force spike'/'7ED', 'Common').
card_artist('force spike'/'7ED', 'Nelson DeCastro').
card_number('force spike'/'7ED', '76').
card_flavor_text('force spike'/'7ED', '\"I don\'t think so.\"').
card_multiverse_id('force spike'/'7ED', '11285').

card_in_set('forest', '7ED').
card_original_type('forest'/'7ED', 'Land').
card_original_text('forest'/'7ED', 'G').
card_image_name('forest'/'7ED', 'forest1').
card_uid('forest'/'7ED', '7ED:Forest:forest1').
card_rarity('forest'/'7ED', 'Basic Land').
card_artist('forest'/'7ED', 'D. J. Cleland-Hura').
card_number('forest'/'7ED', '328').
card_multiverse_id('forest'/'7ED', '11286').

card_in_set('forest', '7ED').
card_original_type('forest'/'7ED', 'Land').
card_original_text('forest'/'7ED', 'G').
card_image_name('forest'/'7ED', 'forest2').
card_uid('forest'/'7ED', '7ED:Forest:forest2').
card_rarity('forest'/'7ED', 'Basic Land').
card_artist('forest'/'7ED', 'Rob Alexander').
card_number('forest'/'7ED', '329').
card_multiverse_id('forest'/'7ED', '11287').

card_in_set('forest', '7ED').
card_original_type('forest'/'7ED', 'Land').
card_original_text('forest'/'7ED', 'G').
card_image_name('forest'/'7ED', 'forest3').
card_uid('forest'/'7ED', '7ED:Forest:forest3').
card_rarity('forest'/'7ED', 'Basic Land').
card_artist('forest'/'7ED', 'John Avon').
card_number('forest'/'7ED', '330').
card_multiverse_id('forest'/'7ED', '11288').

card_in_set('forest', '7ED').
card_original_type('forest'/'7ED', 'Land').
card_original_text('forest'/'7ED', 'G').
card_image_name('forest'/'7ED', 'forest4').
card_uid('forest'/'7ED', '7ED:Forest:forest4').
card_rarity('forest'/'7ED', 'Basic Land').
card_artist('forest'/'7ED', 'John Avon').
card_number('forest'/'7ED', '331').
card_multiverse_id('forest'/'7ED', '11289').

card_in_set('foul imp', '7ED').
card_original_type('foul imp'/'7ED', 'Creature — Imp').
card_original_text('foul imp'/'7ED', 'Flying\nWhen Foul Imp comes into play, you lose 2 life.').
card_image_name('foul imp'/'7ED', 'foul imp').
card_uid('foul imp'/'7ED', '7ED:Foul Imp:foul imp').
card_rarity('foul imp'/'7ED', 'Uncommon').
card_artist('foul imp'/'7ED', 'Kev Walker').
card_number('foul imp'/'7ED', '136').
card_flavor_text('foul imp'/'7ED', 'The villagers call it \"winged stench.\"').
card_multiverse_id('foul imp'/'7ED', '25638').

card_in_set('fugue', '7ED').
card_original_type('fugue'/'7ED', 'Sorcery').
card_original_text('fugue'/'7ED', 'Target player discards three cards from his or her hand.').
card_image_name('fugue'/'7ED', 'fugue').
card_uid('fugue'/'7ED', '7ED:Fugue:fugue').
card_rarity('fugue'/'7ED', 'Uncommon').
card_artist('fugue'/'7ED', 'Alan Pollack').
card_number('fugue'/'7ED', '137').
card_flavor_text('fugue'/'7ED', '\"It\'s hard to keep our own troops from fleeing when he\'s like this. How can our foes possibly stand against him?\"\n—Field marshal for the Eastern Paladin').
card_multiverse_id('fugue'/'7ED', '25634').

card_in_set('fyndhorn elder', '7ED').
card_original_type('fyndhorn elder'/'7ED', 'Creature — Elf').
card_original_text('fyndhorn elder'/'7ED', '{T}: Add {G}{G} to your mana pool.').
card_image_name('fyndhorn elder'/'7ED', 'fyndhorn elder').
card_uid('fyndhorn elder'/'7ED', '7ED:Fyndhorn Elder:fyndhorn elder').
card_rarity('fyndhorn elder'/'7ED', 'Uncommon').
card_artist('fyndhorn elder'/'7ED', 'Greg Staples').
card_number('fyndhorn elder'/'7ED', '246').
card_flavor_text('fyndhorn elder'/'7ED', '\"It is useful to be able to speak to the trees. But it is truly wondrous to be able to listen to them.\"').
card_multiverse_id('fyndhorn elder'/'7ED', '11296').

card_in_set('gang of elk', '7ED').
card_original_type('gang of elk'/'7ED', 'Creature — Beast').
card_original_text('gang of elk'/'7ED', 'Whenever Gang of Elk becomes blocked, it gets +2/+2 until end of turn for each creature blocking it.').
card_image_name('gang of elk'/'7ED', 'gang of elk').
card_uid('gang of elk'/'7ED', '7ED:Gang of Elk:gang of elk').
card_rarity('gang of elk'/'7ED', 'Uncommon').
card_artist('gang of elk'/'7ED', 'Thomas Gianni').
card_number('gang of elk'/'7ED', '247').
card_flavor_text('gang of elk'/'7ED', '\"I felt the ground shake, so I hid in my tent until the earthquake passed. It passed all right, and it left hoofprints.\"\n—Femeref explorer').
card_multiverse_id('gang of elk'/'7ED', '25668').

card_in_set('gerrard\'s wisdom', '7ED').
card_original_type('gerrard\'s wisdom'/'7ED', 'Sorcery').
card_original_text('gerrard\'s wisdom'/'7ED', 'You gain 2 life for each card in your hand.').
card_image_name('gerrard\'s wisdom'/'7ED', 'gerrard\'s wisdom').
card_uid('gerrard\'s wisdom'/'7ED', '7ED:Gerrard\'s Wisdom:gerrard\'s wisdom').
card_rarity('gerrard\'s wisdom'/'7ED', 'Uncommon').
card_artist('gerrard\'s wisdom'/'7ED', 'Donato Giancola').
card_number('gerrard\'s wisdom'/'7ED', '16').
card_flavor_text('gerrard\'s wisdom'/'7ED', 'A picture is worth a thousand swords.').
card_multiverse_id('gerrard\'s wisdom'/'7ED', '13168').

card_in_set('ghitu fire-eater', '7ED').
card_original_type('ghitu fire-eater'/'7ED', 'Creature — Nomad').
card_original_text('ghitu fire-eater'/'7ED', '{T}, Sacrifice Ghitu Fire-Eater: Ghitu Fire-Eater deals damage equal to its power to target creature or player.').
card_image_name('ghitu fire-eater'/'7ED', 'ghitu fire-eater').
card_uid('ghitu fire-eater'/'7ED', '7ED:Ghitu Fire-Eater:ghitu fire-eater').
card_rarity('ghitu fire-eater'/'7ED', 'Uncommon').
card_artist('ghitu fire-eater'/'7ED', 'Eric Peterson').
card_number('ghitu fire-eater'/'7ED', '184').
card_flavor_text('ghitu fire-eater'/'7ED', 'The Ghitu believe their race was born of fire, and it\'s an honor to return to it.').
card_multiverse_id('ghitu fire-eater'/'7ED', '15809').

card_in_set('giant cockroach', '7ED').
card_original_type('giant cockroach'/'7ED', 'Creature — Insect').
card_original_text('giant cockroach'/'7ED', '').
card_image_name('giant cockroach'/'7ED', 'giant cockroach').
card_uid('giant cockroach'/'7ED', '7ED:Giant Cockroach:giant cockroach').
card_rarity('giant cockroach'/'7ED', 'Common').
card_artist('giant cockroach'/'7ED', 'John Matson').
card_number('giant cockroach'/'7ED', '138').
card_flavor_text('giant cockroach'/'7ED', 'Toren had stepped on a lot of bugs during his life, so he couldn\'t help feeling embarrassed when a bug stepped on him.').
card_multiverse_id('giant cockroach'/'7ED', '25628').

card_in_set('giant growth', '7ED').
card_original_type('giant growth'/'7ED', 'Instant').
card_original_text('giant growth'/'7ED', 'Target creature gets +3/+3 until end of turn.').
card_image_name('giant growth'/'7ED', 'giant growth').
card_uid('giant growth'/'7ED', '7ED:Giant Growth:giant growth').
card_rarity('giant growth'/'7ED', 'Common').
card_artist('giant growth'/'7ED', 'Terese Nielsen').
card_number('giant growth'/'7ED', '248').
card_multiverse_id('giant growth'/'7ED', '11301').

card_in_set('giant octopus', '7ED').
card_original_type('giant octopus'/'7ED', 'Creature — Octopus').
card_original_text('giant octopus'/'7ED', '').
card_image_name('giant octopus'/'7ED', 'giant octopus').
card_uid('giant octopus'/'7ED', '7ED:Giant Octopus:giant octopus').
card_rarity('giant octopus'/'7ED', 'Common').
card_artist('giant octopus'/'7ED', 'Heather Hudson').
card_number('giant octopus'/'7ED', '77').
card_flavor_text('giant octopus'/'7ED', 'The only thing that flows faster than water is fear.').
card_multiverse_id('giant octopus'/'7ED', '30170').

card_in_set('giant spider', '7ED').
card_original_type('giant spider'/'7ED', 'Creature — Spider').
card_original_text('giant spider'/'7ED', 'Giant Spider may block as though it had flying.').
card_image_name('giant spider'/'7ED', 'giant spider').
card_uid('giant spider'/'7ED', '7ED:Giant Spider:giant spider').
card_rarity('giant spider'/'7ED', 'Common').
card_artist('giant spider'/'7ED', 'Ray Lago').
card_number('giant spider'/'7ED', '249').
card_flavor_text('giant spider'/'7ED', 'Watching the spider\'s web\n—Llanowar expression meaning\n\"focusing on the wrong thing\"').
card_multiverse_id('giant spider'/'7ED', '11302').

card_in_set('glacial wall', '7ED').
card_original_type('glacial wall'/'7ED', 'Creature — Wall').
card_original_text('glacial wall'/'7ED', '(Walls can\'t attack.)').
card_image_name('glacial wall'/'7ED', 'glacial wall').
card_uid('glacial wall'/'7ED', '7ED:Glacial Wall:glacial wall').
card_rarity('glacial wall'/'7ED', 'Uncommon').
card_artist('glacial wall'/'7ED', 'Heather Hudson').
card_number('glacial wall'/'7ED', '78').
card_flavor_text('glacial wall'/'7ED', 'Like arguing with ice\n—Vodalian expression meaning\n\"wasting your time\"').
card_multiverse_id('glacial wall'/'7ED', '11304').

card_in_set('glorious anthem', '7ED').
card_original_type('glorious anthem'/'7ED', 'Enchantment').
card_original_text('glorious anthem'/'7ED', 'Creatures you control get +1/+1.').
card_image_name('glorious anthem'/'7ED', 'glorious anthem').
card_uid('glorious anthem'/'7ED', '7ED:Glorious Anthem:glorious anthem').
card_rarity('glorious anthem'/'7ED', 'Rare').
card_artist('glorious anthem'/'7ED', 'Terese Nielsen').
card_number('glorious anthem'/'7ED', '17').
card_flavor_text('glorious anthem'/'7ED', '\"North and South meet and angels sing.\nVoices resound with a righteous ring.\"\n—\"The Ballad of the Paladins\"').
card_multiverse_id('glorious anthem'/'7ED', '26843').

card_in_set('goblin chariot', '7ED').
card_original_type('goblin chariot'/'7ED', 'Creature — Goblin').
card_original_text('goblin chariot'/'7ED', 'Haste (This creature may attack and {T} the turn it comes under your control.)').
card_image_name('goblin chariot'/'7ED', 'goblin chariot').
card_uid('goblin chariot'/'7ED', '7ED:Goblin Chariot:goblin chariot').
card_rarity('goblin chariot'/'7ED', 'Common').
card_artist('goblin chariot'/'7ED', 'John Howe').
card_number('goblin chariot'/'7ED', '185').
card_flavor_text('goblin chariot'/'7ED', 'The hardest part of his training is learning not to eat the boar.').
card_multiverse_id('goblin chariot'/'7ED', '25672').

card_in_set('goblin digging team', '7ED').
card_original_type('goblin digging team'/'7ED', 'Creature — Goblin').
card_original_text('goblin digging team'/'7ED', '{T}, Sacrifice Goblin Digging Team: Destroy target Wall.').
card_image_name('goblin digging team'/'7ED', 'goblin digging team').
card_uid('goblin digging team'/'7ED', '7ED:Goblin Digging Team:goblin digging team').
card_rarity('goblin digging team'/'7ED', 'Common').
card_artist('goblin digging team'/'7ED', 'Ben Thompson').
card_number('goblin digging team'/'7ED', '186').
card_flavor_text('goblin digging team'/'7ED', '\"Dig. Dig. Dig. Dig. Dig. Dig. Die\"\n—Goblin chant').
card_multiverse_id('goblin digging team'/'7ED', '11307').

card_in_set('goblin elite infantry', '7ED').
card_original_type('goblin elite infantry'/'7ED', 'Creature — Goblin').
card_original_text('goblin elite infantry'/'7ED', 'Whenever Goblin Elite Infantry blocks or becomes blocked, it gets -1/-1 until end of turn.').
card_image_name('goblin elite infantry'/'7ED', 'goblin elite infantry').
card_uid('goblin elite infantry'/'7ED', '7ED:Goblin Elite Infantry:goblin elite infantry').
card_rarity('goblin elite infantry'/'7ED', 'Common').
card_artist('goblin elite infantry'/'7ED', 'Daren Bader').
card_number('goblin elite infantry'/'7ED', '187').
card_flavor_text('goblin elite infantry'/'7ED', 'We\'re elite. You\'re not.\n—Infantry motto').
card_multiverse_id('goblin elite infantry'/'7ED', '15805').

card_in_set('goblin gardener', '7ED').
card_original_type('goblin gardener'/'7ED', 'Creature — Goblin').
card_original_text('goblin gardener'/'7ED', 'When Goblin Gardener is put into a graveyard from play, destroy target land.').
card_image_name('goblin gardener'/'7ED', 'goblin gardener').
card_uid('goblin gardener'/'7ED', '7ED:Goblin Gardener:goblin gardener').
card_rarity('goblin gardener'/'7ED', 'Common').
card_artist('goblin gardener'/'7ED', 'Jerry Tiritilli').
card_number('goblin gardener'/'7ED', '188').
card_flavor_text('goblin gardener'/'7ED', 'Years of attempts have brought the goblins no closer to growing a sausage tree.').
card_multiverse_id('goblin gardener'/'7ED', '25674').

card_in_set('goblin glider', '7ED').
card_original_type('goblin glider'/'7ED', 'Creature — Goblin').
card_original_text('goblin glider'/'7ED', 'Flying\nGoblin Glider can\'t block.').
card_image_name('goblin glider'/'7ED', 'goblin glider').
card_uid('goblin glider'/'7ED', '7ED:Goblin Glider:goblin glider').
card_rarity('goblin glider'/'7ED', 'Uncommon').
card_artist('goblin glider'/'7ED', 'Patrick Faricy').
card_number('goblin glider'/'7ED', '189').
card_flavor_text('goblin glider'/'7ED', '\"What\'s got two arms, one wing, and no brains?\"\n—Ghitu riddle').
card_multiverse_id('goblin glider'/'7ED', '25679').

card_in_set('goblin king', '7ED').
card_original_type('goblin king'/'7ED', 'Creature — Lord').
card_original_text('goblin king'/'7ED', 'All Goblins get +1/+1 and have mountainwalk. (They\'re unblockable as long as defending player controls a mountain.)').
card_image_name('goblin king'/'7ED', 'goblin king').
card_uid('goblin king'/'7ED', '7ED:Goblin King:goblin king').
card_rarity('goblin king'/'7ED', 'Rare').
card_artist('goblin king'/'7ED', 'Ron Spears').
card_number('goblin king'/'7ED', '190').
card_flavor_text('goblin king'/'7ED', 'To be king, Numsgil did in Blog, who did in Unkful, who did in Viddle, who did in Loll, who did in Alrok. . . .').
card_multiverse_id('goblin king'/'7ED', '11309').

card_in_set('goblin matron', '7ED').
card_original_type('goblin matron'/'7ED', 'Creature — Goblin').
card_original_text('goblin matron'/'7ED', 'When Goblin Matron comes into play, you may search your library for a Goblin card. If you do, reveal that card and put it into your hand. Then shuffle your library.').
card_image_name('goblin matron'/'7ED', 'goblin matron').
card_uid('goblin matron'/'7ED', '7ED:Goblin Matron:goblin matron').
card_rarity('goblin matron'/'7ED', 'Uncommon').
card_artist('goblin matron'/'7ED', 'Dan Frazier').
card_number('goblin matron'/'7ED', '191').
card_multiverse_id('goblin matron'/'7ED', '15810').

card_in_set('goblin raider', '7ED').
card_original_type('goblin raider'/'7ED', 'Creature — Goblin').
card_original_text('goblin raider'/'7ED', 'Goblin Raider can\'t block.').
card_image_name('goblin raider'/'7ED', 'goblin raider').
card_uid('goblin raider'/'7ED', '7ED:Goblin Raider:goblin raider').
card_rarity('goblin raider'/'7ED', 'Common').
card_artist('goblin raider'/'7ED', 'Arnie Swekel').
card_number('goblin raider'/'7ED', '192').
card_flavor_text('goblin raider'/'7ED', 'He\'s smart for a goblin. He can do two things: hit and run.').
card_multiverse_id('goblin raider'/'7ED', '13122').

card_in_set('goblin spelunkers', '7ED').
card_original_type('goblin spelunkers'/'7ED', 'Creature — Goblin').
card_original_text('goblin spelunkers'/'7ED', 'Mountainwalk (This creature is unblockable as long as defending player controls a mountain.)').
card_image_name('goblin spelunkers'/'7ED', 'goblin spelunkers').
card_uid('goblin spelunkers'/'7ED', '7ED:Goblin Spelunkers:goblin spelunkers').
card_rarity('goblin spelunkers'/'7ED', 'Common').
card_artist('goblin spelunkers'/'7ED', 'Matt Cavotta').
card_number('goblin spelunkers'/'7ED', '193').
card_flavor_text('goblin spelunkers'/'7ED', 'Goblins prefer caves to just about anything else. Caves prefer just about anything else to goblins.').
card_multiverse_id('goblin spelunkers'/'7ED', '13100').

card_in_set('goblin war drums', '7ED').
card_original_type('goblin war drums'/'7ED', 'Enchantment').
card_original_text('goblin war drums'/'7ED', 'Each creature you control can\'t be blocked except by two or more creatures.').
card_image_name('goblin war drums'/'7ED', 'goblin war drums').
card_uid('goblin war drums'/'7ED', '7ED:Goblin War Drums:goblin war drums').
card_rarity('goblin war drums'/'7ED', 'Uncommon').
card_artist('goblin war drums'/'7ED', 'Peter Bollinger').
card_number('goblin war drums'/'7ED', '194').
card_flavor_text('goblin war drums'/'7ED', 'The goblin idea of music is to hit something with a stick.').
card_multiverse_id('goblin war drums'/'7ED', '25676').

card_in_set('gorilla chieftain', '7ED').
card_original_type('gorilla chieftain'/'7ED', 'Creature — Ape').
card_original_text('gorilla chieftain'/'7ED', '{1}{G} Regenerate Gorilla Chieftain.').
card_image_name('gorilla chieftain'/'7ED', 'gorilla chieftain').
card_uid('gorilla chieftain'/'7ED', '7ED:Gorilla Chieftain:gorilla chieftain').
card_rarity('gorilla chieftain'/'7ED', 'Common').
card_artist('gorilla chieftain'/'7ED', 'Carl Critchlow').
card_number('gorilla chieftain'/'7ED', '250').
card_flavor_text('gorilla chieftain'/'7ED', 'A gorilla chief must always be prepared to defend his crown. This leads to some pretty tough apes.').
card_multiverse_id('gorilla chieftain'/'7ED', '13047').

card_in_set('grafted skullcap', '7ED').
card_original_type('grafted skullcap'/'7ED', 'Artifact').
card_original_text('grafted skullcap'/'7ED', 'At the beginning of your draw step, draw a card.\nAt the end of your turn, discard your hand.').
card_image_name('grafted skullcap'/'7ED', 'grafted skullcap').
card_uid('grafted skullcap'/'7ED', '7ED:Grafted Skullcap:grafted skullcap').
card_rarity('grafted skullcap'/'7ED', 'Rare').
card_artist('grafted skullcap'/'7ED', 'Bradley Williams').
card_number('grafted skullcap'/'7ED', '298').
card_flavor_text('grafted skullcap'/'7ED', '\"Every day, I fight for my life and win. Every night, I fight to remember my name and lose.\"').
card_multiverse_id('grafted skullcap'/'7ED', '25661').

card_in_set('granite grip', '7ED').
card_original_type('granite grip'/'7ED', 'Enchant Creature').
card_original_text('granite grip'/'7ED', 'Enchanted creature gets +1/+0 for each mountain you control.').
card_image_name('granite grip'/'7ED', 'granite grip').
card_uid('granite grip'/'7ED', '7ED:Granite Grip:granite grip').
card_rarity('granite grip'/'7ED', 'Common').
card_artist('granite grip'/'7ED', 'Ray Lago').
card_number('granite grip'/'7ED', '195').
card_flavor_text('granite grip'/'7ED', '\"Let me introduce you to Rocky.\"').
card_multiverse_id('granite grip'/'7ED', '25671').

card_in_set('grapeshot catapult', '7ED').
card_original_type('grapeshot catapult'/'7ED', 'Artifact Creature').
card_original_text('grapeshot catapult'/'7ED', '{T}: Grapeshot Catapult deals 1 damage to target creature with flying.').
card_image_name('grapeshot catapult'/'7ED', 'grapeshot catapult').
card_uid('grapeshot catapult'/'7ED', '7ED:Grapeshot Catapult:grapeshot catapult').
card_rarity('grapeshot catapult'/'7ED', 'Uncommon').
card_artist('grapeshot catapult'/'7ED', 'Dave Dorman').
card_number('grapeshot catapult'/'7ED', '299').
card_flavor_text('grapeshot catapult'/'7ED', 'The Southern Paladin taught the airborne vermin to fear the storm that rains upward.').
card_multiverse_id('grapeshot catapult'/'7ED', '25659').

card_in_set('gravedigger', '7ED').
card_original_type('gravedigger'/'7ED', 'Creature — Zombie').
card_original_text('gravedigger'/'7ED', 'When Gravedigger comes into play, you may return target creature card from your graveyard to your hand.').
card_image_name('gravedigger'/'7ED', 'gravedigger').
card_uid('gravedigger'/'7ED', '7ED:Gravedigger:gravedigger').
card_rarity('gravedigger'/'7ED', 'Common').
card_artist('gravedigger'/'7ED', 'James Bernardin').
card_number('gravedigger'/'7ED', '139').
card_flavor_text('gravedigger'/'7ED', 'A full coffin is like a full coffer—both are attractive to thieves.').
card_multiverse_id('gravedigger'/'7ED', '13025').

card_in_set('greed', '7ED').
card_original_type('greed'/'7ED', 'Enchantment').
card_original_text('greed'/'7ED', '{B}, Pay 2 life: Draw a card.').
card_image_name('greed'/'7ED', 'greed').
card_uid('greed'/'7ED', '7ED:Greed:greed').
card_rarity('greed'/'7ED', 'Rare').
card_artist('greed'/'7ED', 'Peter Bollinger').
card_number('greed'/'7ED', '140').
card_flavor_text('greed'/'7ED', 'An advisor once asked the Western Paladin how much gold would be enough. \"I have no need of fools who can imagine ‘enough,\'\" he told the advisor\'s corpse.').
card_multiverse_id('greed'/'7ED', '25642').

card_in_set('grizzly bears', '7ED').
card_original_type('grizzly bears'/'7ED', 'Creature — Bear').
card_original_text('grizzly bears'/'7ED', '').
card_image_name('grizzly bears'/'7ED', 'grizzly bears').
card_uid('grizzly bears'/'7ED', '7ED:Grizzly Bears:grizzly bears').
card_rarity('grizzly bears'/'7ED', 'Common').
card_artist('grizzly bears'/'7ED', 'D. J. Cleland-Hura').
card_number('grizzly bears'/'7ED', '251').
card_flavor_text('grizzly bears'/'7ED', 'They\'ve got claws as long as your arm. And they\'re grouchy. Really, really grouchy.').
card_multiverse_id('grizzly bears'/'7ED', '11315').

card_in_set('healing salve', '7ED').
card_original_type('healing salve'/'7ED', 'Instant').
card_original_text('healing salve'/'7ED', 'Choose one Target player gains 3 life; or prevent the next 3 damage that would be dealt to target creature or player this turn.').
card_image_name('healing salve'/'7ED', 'healing salve').
card_uid('healing salve'/'7ED', '7ED:Healing Salve:healing salve').
card_rarity('healing salve'/'7ED', 'Common').
card_artist('healing salve'/'7ED', 'Greg & Tim Hildebrandt').
card_number('healing salve'/'7ED', '18').
card_multiverse_id('healing salve'/'7ED', '25545').

card_in_set('heavy ballista', '7ED').
card_original_type('heavy ballista'/'7ED', 'Creature — Soldier').
card_original_text('heavy ballista'/'7ED', '{T}: Heavy Ballista deals 2 damage to target attacking or blocking creature.').
card_image_name('heavy ballista'/'7ED', 'heavy ballista').
card_uid('heavy ballista'/'7ED', '7ED:Heavy Ballista:heavy ballista').
card_rarity('heavy ballista'/'7ED', 'Uncommon').
card_artist('heavy ballista'/'7ED', 'Brian Snõddy').
card_number('heavy ballista'/'7ED', '19').
card_flavor_text('heavy ballista'/'7ED', '\"Soldiers wear down and enemies collapse—but the ballista never tires.\"\n—Ballista captain').
card_multiverse_id('heavy ballista'/'7ED', '13017').

card_in_set('hibernation', '7ED').
card_original_type('hibernation'/'7ED', 'Instant').
card_original_text('hibernation'/'7ED', 'Return all green permanents to their owners\' hands.').
card_image_name('hibernation'/'7ED', 'hibernation').
card_uid('hibernation'/'7ED', '7ED:Hibernation:hibernation').
card_rarity('hibernation'/'7ED', 'Uncommon').
card_artist('hibernation'/'7ED', 'Matt Cavotta').
card_number('hibernation'/'7ED', '79').
card_flavor_text('hibernation'/'7ED', 'The deep sleep of winter is not always restful.').
card_multiverse_id('hibernation'/'7ED', '13116').

card_in_set('hill giant', '7ED').
card_original_type('hill giant'/'7ED', 'Creature — Giant').
card_original_text('hill giant'/'7ED', '').
card_image_name('hill giant'/'7ED', 'hill giant').
card_uid('hill giant'/'7ED', '7ED:Hill Giant:hill giant').
card_rarity('hill giant'/'7ED', 'Common').
card_artist('hill giant'/'7ED', 'Orizio Daniele').
card_number('hill giant'/'7ED', '196').
card_flavor_text('hill giant'/'7ED', 'Hill giants are mostly just big. Of course, that does count for a lot.').
card_multiverse_id('hill giant'/'7ED', '25680').

card_in_set('hollow dogs', '7ED').
card_original_type('hollow dogs'/'7ED', 'Creature — Hound').
card_original_text('hollow dogs'/'7ED', 'Whenever Hollow Dogs attacks, it gets +2/+0 until end of turn.').
card_image_name('hollow dogs'/'7ED', 'hollow dogs').
card_uid('hollow dogs'/'7ED', '7ED:Hollow Dogs:hollow dogs').
card_rarity('hollow dogs'/'7ED', 'Common').
card_artist('hollow dogs'/'7ED', 'Arnie Swekel').
card_number('hollow dogs'/'7ED', '141').
card_flavor_text('hollow dogs'/'7ED', 'A hollow dog is never empty. It is filled with thirst for the hunt.').
card_multiverse_id('hollow dogs'/'7ED', '13107').

card_in_set('holy strength', '7ED').
card_original_type('holy strength'/'7ED', 'Enchant Creature').
card_original_text('holy strength'/'7ED', 'Enchanted creature gets +1/+2.').
card_image_name('holy strength'/'7ED', 'holy strength').
card_uid('holy strength'/'7ED', '7ED:Holy Strength:holy strength').
card_rarity('holy strength'/'7ED', 'Common').
card_artist('holy strength'/'7ED', 'Scott M. Fischer').
card_number('holy strength'/'7ED', '20').
card_multiverse_id('holy strength'/'7ED', '15779').

card_in_set('honor guard', '7ED').
card_original_type('honor guard'/'7ED', 'Creature — Soldier').
card_original_text('honor guard'/'7ED', '{W} Honor Guard gets +0/+1 until end of turn.').
card_image_name('honor guard'/'7ED', 'honor guard').
card_uid('honor guard'/'7ED', '7ED:Honor Guard:honor guard').
card_rarity('honor guard'/'7ED', 'Common').
card_artist('honor guard'/'7ED', 'Mark Zug').
card_number('honor guard'/'7ED', '21').
card_flavor_text('honor guard'/'7ED', 'The strength of one. The courage of ten.').
card_multiverse_id('honor guard'/'7ED', '13082').

card_in_set('horned turtle', '7ED').
card_original_type('horned turtle'/'7ED', 'Creature — Turtle').
card_original_text('horned turtle'/'7ED', '').
card_image_name('horned turtle'/'7ED', 'horned turtle').
card_uid('horned turtle'/'7ED', '7ED:Horned Turtle:horned turtle').
card_rarity('horned turtle'/'7ED', 'Common').
card_artist('horned turtle'/'7ED', 'Edward P. Beard, Jr.').
card_number('horned turtle'/'7ED', '80').
card_flavor_text('horned turtle'/'7ED', 'It doesn\'t like having visitors . . . unless it\'s having them for lunch.').
card_multiverse_id('horned turtle'/'7ED', '13080').

card_in_set('howl from beyond', '7ED').
card_original_type('howl from beyond'/'7ED', 'Instant').
card_original_text('howl from beyond'/'7ED', 'Target creature gets +X/+0 until end of turn.').
card_image_name('howl from beyond'/'7ED', 'howl from beyond').
card_uid('howl from beyond'/'7ED', '7ED:Howl from Beyond:howl from beyond').
card_rarity('howl from beyond'/'7ED', 'Common').
card_artist('howl from beyond'/'7ED', 'Dave Dorman').
card_number('howl from beyond'/'7ED', '142').
card_flavor_text('howl from beyond'/'7ED', 'Whether it howls for hunger or for joy, you\'re in trouble just the same.').
card_multiverse_id('howl from beyond'/'7ED', '11326').

card_in_set('howling mine', '7ED').
card_original_type('howling mine'/'7ED', 'Artifact').
card_original_text('howling mine'/'7ED', 'At the beginning of each player\'s draw step, if Howling Mine is untapped, that player draws a card.').
card_image_name('howling mine'/'7ED', 'howling mine').
card_uid('howling mine'/'7ED', '7ED:Howling Mine:howling mine').
card_rarity('howling mine'/'7ED', 'Rare').
card_artist('howling mine'/'7ED', 'Dana Knutson').
card_number('howling mine'/'7ED', '300').
card_flavor_text('howling mine'/'7ED', 'Legend has it that the mine howls out the last words of those who died inside.').
card_multiverse_id('howling mine'/'7ED', '11327').

card_in_set('hurricane', '7ED').
card_original_type('hurricane'/'7ED', 'Sorcery').
card_original_text('hurricane'/'7ED', 'Hurricane deals X damage to each creature with flying and each player.').
card_image_name('hurricane'/'7ED', 'hurricane').
card_uid('hurricane'/'7ED', '7ED:Hurricane:hurricane').
card_rarity('hurricane'/'7ED', 'Rare').
card_artist('hurricane'/'7ED', 'John Howe').
card_number('hurricane'/'7ED', '252').
card_multiverse_id('hurricane'/'7ED', '10844').

card_in_set('impatience', '7ED').
card_original_type('impatience'/'7ED', 'Enchantment').
card_original_text('impatience'/'7ED', 'At the end of each player\'s turn, if that player didn\'t play a spell that turn, Impatience deals 2 damage to him or her.').
card_image_name('impatience'/'7ED', 'impatience').
card_uid('impatience'/'7ED', '7ED:Impatience:impatience').
card_rarity('impatience'/'7ED', 'Rare').
card_artist('impatience'/'7ED', 'Kunio Hagio').
card_number('impatience'/'7ED', '197').
card_flavor_text('impatience'/'7ED', '\"I\'m going to count to three. Three.\"').
card_multiverse_id('impatience'/'7ED', '25681').

card_in_set('infernal contract', '7ED').
card_original_type('infernal contract'/'7ED', 'Sorcery').
card_original_text('infernal contract'/'7ED', 'Draw four cards. You lose half your life, rounded up.').
card_image_name('infernal contract'/'7ED', 'infernal contract').
card_uid('infernal contract'/'7ED', '7ED:Infernal Contract:infernal contract').
card_rarity('infernal contract'/'7ED', 'Rare').
card_artist('infernal contract'/'7ED', 'Pete Venters').
card_number('infernal contract'/'7ED', '143').
card_flavor_text('infernal contract'/'7ED', '\"East and West meet and angels cry.\nUnholy power darkens the sky.\"\n—\"The Ballad of the Paladins\"').
card_multiverse_id('infernal contract'/'7ED', '12957').

card_in_set('inferno', '7ED').
card_original_type('inferno'/'7ED', 'Instant').
card_original_text('inferno'/'7ED', 'Inferno deals 6 damage to each creature and each player.').
card_image_name('inferno'/'7ED', 'inferno').
card_uid('inferno'/'7ED', '7ED:Inferno:inferno').
card_rarity('inferno'/'7ED', 'Rare').
card_artist('inferno'/'7ED', 'Don Hazeltine').
card_number('inferno'/'7ED', '198').
card_flavor_text('inferno'/'7ED', '\"Some have said there is no subtlety to destruction. You know what? They\'re dead.\"\n—Wandering mage').
card_multiverse_id('inferno'/'7ED', '15815').

card_in_set('inspiration', '7ED').
card_original_type('inspiration'/'7ED', 'Instant').
card_original_text('inspiration'/'7ED', 'Target player draws two cards.').
card_image_name('inspiration'/'7ED', 'inspiration').
card_uid('inspiration'/'7ED', '7ED:Inspiration:inspiration').
card_rarity('inspiration'/'7ED', 'Common').
card_artist('inspiration'/'7ED', 'Matt Cavotta').
card_number('inspiration'/'7ED', '81').
card_multiverse_id('inspiration'/'7ED', '13169').

card_in_set('intrepid hero', '7ED').
card_original_type('intrepid hero'/'7ED', 'Creature — Soldier').
card_original_text('intrepid hero'/'7ED', '{T}: Destroy target creature with power 4 or greater.').
card_image_name('intrepid hero'/'7ED', 'intrepid hero').
card_uid('intrepid hero'/'7ED', '7ED:Intrepid Hero:intrepid hero').
card_rarity('intrepid hero'/'7ED', 'Rare').
card_artist('intrepid hero'/'7ED', 'Mike Ploog').
card_number('intrepid hero'/'7ED', '22').
card_flavor_text('intrepid hero'/'7ED', 'A fool knows no fear. A hero shows no fear.').
card_multiverse_id('intrepid hero'/'7ED', '13117').

card_in_set('iron star', '7ED').
card_original_type('iron star'/'7ED', 'Artifact').
card_original_text('iron star'/'7ED', 'Whenever a player plays a red spell, you may pay {1}. If you do, you gain 1 life.').
card_image_name('iron star'/'7ED', 'iron star').
card_uid('iron star'/'7ED', '7ED:Iron Star:iron star').
card_rarity('iron star'/'7ED', 'Uncommon').
card_artist('iron star'/'7ED', 'Pete Venters').
card_number('iron star'/'7ED', '301').
card_flavor_text('iron star'/'7ED', 'Forged by a thousand flames.').
card_multiverse_id('iron star'/'7ED', '25649').

card_in_set('island', '7ED').
card_original_type('island'/'7ED', 'Land').
card_original_text('island'/'7ED', 'U').
card_image_name('island'/'7ED', 'island1').
card_uid('island'/'7ED', '7ED:Island:island1').
card_rarity('island'/'7ED', 'Basic Land').
card_artist('island'/'7ED', 'Scott Bailey').
card_number('island'/'7ED', '332').
card_multiverse_id('island'/'7ED', '11348').

card_in_set('island', '7ED').
card_original_type('island'/'7ED', 'Land').
card_original_text('island'/'7ED', 'U').
card_image_name('island'/'7ED', 'island2').
card_uid('island'/'7ED', '7ED:Island:island2').
card_rarity('island'/'7ED', 'Basic Land').
card_artist('island'/'7ED', 'Rob Alexander').
card_number('island'/'7ED', '333').
card_multiverse_id('island'/'7ED', '11349').

card_in_set('island', '7ED').
card_original_type('island'/'7ED', 'Land').
card_original_text('island'/'7ED', 'U').
card_image_name('island'/'7ED', 'island3').
card_uid('island'/'7ED', '7ED:Island:island3').
card_rarity('island'/'7ED', 'Basic Land').
card_artist('island'/'7ED', 'John Avon').
card_number('island'/'7ED', '334').
card_multiverse_id('island'/'7ED', '11350').

card_in_set('island', '7ED').
card_original_type('island'/'7ED', 'Land').
card_original_text('island'/'7ED', 'U').
card_image_name('island'/'7ED', 'island4').
card_uid('island'/'7ED', '7ED:Island:island4').
card_rarity('island'/'7ED', 'Basic Land').
card_artist('island'/'7ED', 'Tony Szczudlo').
card_number('island'/'7ED', '335').
card_multiverse_id('island'/'7ED', '11351').

card_in_set('ivory cup', '7ED').
card_original_type('ivory cup'/'7ED', 'Artifact').
card_original_text('ivory cup'/'7ED', 'Whenever a player plays a white spell, you may pay {1}. If you do, you gain 1 life.').
card_image_name('ivory cup'/'7ED', 'ivory cup').
card_uid('ivory cup'/'7ED', '7ED:Ivory Cup:ivory cup').
card_rarity('ivory cup'/'7ED', 'Uncommon').
card_artist('ivory cup'/'7ED', 'Alan Pollack').
card_number('ivory cup'/'7ED', '302').
card_flavor_text('ivory cup'/'7ED', 'Blessed by a thousand prayers.').
card_multiverse_id('ivory cup'/'7ED', '25650').

card_in_set('jalum tome', '7ED').
card_original_type('jalum tome'/'7ED', 'Artifact').
card_original_text('jalum tome'/'7ED', '{2}, {T}: Draw a card, then discard a card from your hand.').
card_image_name('jalum tome'/'7ED', 'jalum tome').
card_uid('jalum tome'/'7ED', '7ED:Jalum Tome:jalum tome').
card_rarity('jalum tome'/'7ED', 'Rare').
card_artist('jalum tome'/'7ED', 'Jerry Tiritilli').
card_number('jalum tome'/'7ED', '303').
card_flavor_text('jalum tome'/'7ED', 'Most books simply deliver knowledge. This one expects some in return.').
card_multiverse_id('jalum tome'/'7ED', '11356').

card_in_set('jandor\'s saddlebags', '7ED').
card_original_type('jandor\'s saddlebags'/'7ED', 'Artifact').
card_original_text('jandor\'s saddlebags'/'7ED', '{3}, {T}: Untap target creature.').
card_image_name('jandor\'s saddlebags'/'7ED', 'jandor\'s saddlebags').
card_uid('jandor\'s saddlebags'/'7ED', '7ED:Jandor\'s Saddlebags:jandor\'s saddlebags').
card_rarity('jandor\'s saddlebags'/'7ED', 'Rare').
card_artist('jandor\'s saddlebags'/'7ED', 'Brian Despain').
card_number('jandor\'s saddlebags'/'7ED', '304').
card_flavor_text('jandor\'s saddlebags'/'7ED', 'Your heart\'s desire is found within.\n—Saddlebag inscription').
card_multiverse_id('jandor\'s saddlebags'/'7ED', '11357').

card_in_set('jayemdae tome', '7ED').
card_original_type('jayemdae tome'/'7ED', 'Artifact').
card_original_text('jayemdae tome'/'7ED', '{4}, {T}: Draw a card.').
card_image_name('jayemdae tome'/'7ED', 'jayemdae tome').
card_uid('jayemdae tome'/'7ED', '7ED:Jayemdae Tome:jayemdae tome').
card_rarity('jayemdae tome'/'7ED', 'Rare').
card_artist('jayemdae tome'/'7ED', 'Donato Giancola').
card_number('jayemdae tome'/'7ED', '305').
card_flavor_text('jayemdae tome'/'7ED', '\"Knowledge is power.\"\n—Sir Francis Bacon,\nMeditationes Sacrae').
card_multiverse_id('jayemdae tome'/'7ED', '25660').

card_in_set('karplusan forest', '7ED').
card_original_type('karplusan forest'/'7ED', 'Land').
card_original_text('karplusan forest'/'7ED', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {R} or {G} to your mana pool. Karplusan Forest deals 1 damage to you.').
card_image_name('karplusan forest'/'7ED', 'karplusan forest').
card_uid('karplusan forest'/'7ED', '7ED:Karplusan Forest:karplusan forest').
card_rarity('karplusan forest'/'7ED', 'Rare').
card_artist('karplusan forest'/'7ED', 'Scott Bailey').
card_number('karplusan forest'/'7ED', '336').
card_multiverse_id('karplusan forest'/'7ED', '11366').

card_in_set('kjeldoran royal guard', '7ED').
card_original_type('kjeldoran royal guard'/'7ED', 'Creature — Soldier').
card_original_text('kjeldoran royal guard'/'7ED', '{T}: All combat damage that unblocked creatures would deal to you this turn is dealt to Kjeldoran Royal Guard instead.').
card_image_name('kjeldoran royal guard'/'7ED', 'kjeldoran royal guard').
card_uid('kjeldoran royal guard'/'7ED', '7ED:Kjeldoran Royal Guard:kjeldoran royal guard').
card_rarity('kjeldoran royal guard'/'7ED', 'Rare').
card_artist('kjeldoran royal guard'/'7ED', 'Carl Critchlow').
card_number('kjeldoran royal guard'/'7ED', '23').
card_flavor_text('kjeldoran royal guard'/'7ED', 'Honorable in battle, generous in death.\n—Motto of the Kjeldoran Royal Guard').
card_multiverse_id('kjeldoran royal guard'/'7ED', '25556').

card_in_set('knight errant', '7ED').
card_original_type('knight errant'/'7ED', 'Creature — Knight').
card_original_text('knight errant'/'7ED', '').
card_image_name('knight errant'/'7ED', 'knight errant').
card_uid('knight errant'/'7ED', '7ED:Knight Errant:knight errant').
card_rarity('knight errant'/'7ED', 'Common').
card_artist('knight errant'/'7ED', 'Matthew D. Wilson').
card_number('knight errant'/'7ED', '24').
card_flavor_text('knight errant'/'7ED', 'Knights are quick to pledge their loyalty and even quicker to charge into battle.').
card_multiverse_id('knight errant'/'7ED', '13099').

card_in_set('knighthood', '7ED').
card_original_type('knighthood'/'7ED', 'Enchantment').
card_original_text('knighthood'/'7ED', 'Creatures you control have first strike.').
card_image_name('knighthood'/'7ED', 'knighthood').
card_uid('knighthood'/'7ED', '7ED:Knighthood:knighthood').
card_rarity('knighthood'/'7ED', 'Uncommon').
card_artist('knighthood'/'7ED', 'Greg & Tim Hildebrandt').
card_number('knighthood'/'7ED', '25').
card_flavor_text('knighthood'/'7ED', 'Kings don\'t make knights. Actions do.').
card_multiverse_id('knighthood'/'7ED', '13171').

card_in_set('lava axe', '7ED').
card_original_type('lava axe'/'7ED', 'Sorcery').
card_original_text('lava axe'/'7ED', 'Lava Axe deals 5 damage to target player.').
card_image_name('lava axe'/'7ED', 'lava axe').
card_uid('lava axe'/'7ED', '7ED:Lava Axe:lava axe').
card_rarity('lava axe'/'7ED', 'Common').
card_artist('lava axe'/'7ED', 'Ray Lago').
card_number('lava axe'/'7ED', '199').
card_flavor_text('lava axe'/'7ED', '\"Catch!\"').
card_multiverse_id('lava axe'/'7ED', '13185').

card_in_set('leshrac\'s rite', '7ED').
card_original_type('leshrac\'s rite'/'7ED', 'Enchant Creature').
card_original_text('leshrac\'s rite'/'7ED', 'Enchanted creature has swampwalk. (It\'s unblockable as long as defending player controls a swamp.)').
card_image_name('leshrac\'s rite'/'7ED', 'leshrac\'s rite').
card_uid('leshrac\'s rite'/'7ED', '7ED:Leshrac\'s Rite:leshrac\'s rite').
card_rarity('leshrac\'s rite'/'7ED', 'Uncommon').
card_artist('leshrac\'s rite'/'7ED', 'rk post').
card_number('leshrac\'s rite'/'7ED', '144').
card_flavor_text('leshrac\'s rite'/'7ED', '\"Brackish water, dark and blind\nHeed my will, to you I bind.\"\n—Opening passage of Leshrac\'s Rite').
card_multiverse_id('leshrac\'s rite'/'7ED', '11377').

card_in_set('levitation', '7ED').
card_original_type('levitation'/'7ED', 'Enchantment').
card_original_text('levitation'/'7ED', 'Creatures you control have flying.').
card_image_name('levitation'/'7ED', 'levitation').
card_uid('levitation'/'7ED', '7ED:Levitation:levitation').
card_rarity('levitation'/'7ED', 'Uncommon').
card_artist('levitation'/'7ED', 'John Matson').
card_number('levitation'/'7ED', '82').
card_flavor_text('levitation'/'7ED', '\"We\'ll be easy targets if we remain on the ground. Let\'s take the fight to them.\"').
card_multiverse_id('levitation'/'7ED', '13155').

card_in_set('lightning blast', '7ED').
card_original_type('lightning blast'/'7ED', 'Instant').
card_original_text('lightning blast'/'7ED', 'Lightning Blast deals 4 damage to target creature or player.').
card_image_name('lightning blast'/'7ED', 'lightning blast').
card_uid('lightning blast'/'7ED', '7ED:Lightning Blast:lightning blast').
card_rarity('lightning blast'/'7ED', 'Common').
card_artist('lightning blast'/'7ED', 'Ron Spencer').
card_number('lightning blast'/'7ED', '200').
card_flavor_text('lightning blast'/'7ED', '\"I\'ll climb down soon. Don\'t worry, it\'s just a thundersto—\"\n—Former elvish scout').
card_multiverse_id('lightning blast'/'7ED', '13051').

card_in_set('lightning elemental', '7ED').
card_original_type('lightning elemental'/'7ED', 'Creature — Elemental').
card_original_text('lightning elemental'/'7ED', 'Haste (This creature may attack and {T} the turn it comes under your control.)').
card_image_name('lightning elemental'/'7ED', 'lightning elemental').
card_uid('lightning elemental'/'7ED', '7ED:Lightning Elemental:lightning elemental').
card_rarity('lightning elemental'/'7ED', 'Common').
card_artist('lightning elemental'/'7ED', 'Kev Walker').
card_number('lightning elemental'/'7ED', '201').
card_flavor_text('lightning elemental'/'7ED', '\"A flash of the lightning, a break of the wave,\nHe passes from life to his rest in the grave.\"\n—William Knox, \"Mortality\"').
card_multiverse_id('lightning elemental'/'7ED', '13053').

card_in_set('llanowar elves', '7ED').
card_original_type('llanowar elves'/'7ED', 'Creature — Elf').
card_original_text('llanowar elves'/'7ED', '{T}: Add {G} to your mana pool.').
card_image_name('llanowar elves'/'7ED', 'llanowar elves').
card_uid('llanowar elves'/'7ED', '7ED:Llanowar Elves:llanowar elves').
card_rarity('llanowar elves'/'7ED', 'Common').
card_artist('llanowar elves'/'7ED', 'Jerry Tiritilli').
card_number('llanowar elves'/'7ED', '253').
card_flavor_text('llanowar elves'/'7ED', 'Llanowar covers a million square miles, yet nobody enters the forest without the elves knowing it.').
card_multiverse_id('llanowar elves'/'7ED', '11386').

card_in_set('lone wolf', '7ED').
card_original_type('lone wolf'/'7ED', 'Creature — Wolf').
card_original_text('lone wolf'/'7ED', 'You may have Lone Wolf deal its combat damage to defending player as though it weren\'t blocked.').
card_image_name('lone wolf'/'7ED', 'lone wolf').
card_uid('lone wolf'/'7ED', '7ED:Lone Wolf:lone wolf').
card_rarity('lone wolf'/'7ED', 'Common').
card_artist('lone wolf'/'7ED', 'Douglas Shuler').
card_number('lone wolf'/'7ED', '254').
card_flavor_text('lone wolf'/'7ED', 'A wolf without a pack is either a survivor or a brute.').
card_multiverse_id('lone wolf'/'7ED', '13161').

card_in_set('longbow archer', '7ED').
card_original_type('longbow archer'/'7ED', 'Creature — Soldier').
card_original_text('longbow archer'/'7ED', 'First strike\nLongbow Archer may block as though it had flying.').
card_image_name('longbow archer'/'7ED', 'longbow archer').
card_uid('longbow archer'/'7ED', '7ED:Longbow Archer:longbow archer').
card_rarity('longbow archer'/'7ED', 'Uncommon').
card_artist('longbow archer'/'7ED', 'Ray Lago').
card_number('longbow archer'/'7ED', '26').
card_flavor_text('longbow archer'/'7ED', 'Some say they have the blood of hawks running through their veins.').
card_multiverse_id('longbow archer'/'7ED', '25549').

card_in_set('looming shade', '7ED').
card_original_type('looming shade'/'7ED', 'Creature — Shade').
card_original_text('looming shade'/'7ED', '{B} Looming Shade gets +1/+1 until end of turn.').
card_image_name('looming shade'/'7ED', 'looming shade').
card_uid('looming shade'/'7ED', '7ED:Looming Shade:looming shade').
card_rarity('looming shade'/'7ED', 'Common').
card_artist('looming shade'/'7ED', 'Kev Walker').
card_number('looming shade'/'7ED', '145').
card_flavor_text('looming shade'/'7ED', 'On darkness feeding, in nightmares breeding.').
card_multiverse_id('looming shade'/'7ED', '13104').

card_in_set('lord of atlantis', '7ED').
card_original_type('lord of atlantis'/'7ED', 'Creature — Lord').
card_original_text('lord of atlantis'/'7ED', 'All Merfolk get +1/+1 and have islandwalk. (They\'re unblockable as long as defending player controls an island.)').
card_image_name('lord of atlantis'/'7ED', 'lord of atlantis').
card_uid('lord of atlantis'/'7ED', '7ED:Lord of Atlantis:lord of atlantis').
card_rarity('lord of atlantis'/'7ED', 'Rare').
card_artist('lord of atlantis'/'7ED', 'Greg Staples').
card_number('lord of atlantis'/'7ED', '83').
card_flavor_text('lord of atlantis'/'7ED', 'A master of tactics, the Lord of Atlantis makes his people bold in battle merely by arriving to lead them.').
card_multiverse_id('lord of atlantis'/'7ED', '11387').

card_in_set('lure', '7ED').
card_original_type('lure'/'7ED', 'Enchant Creature').
card_original_text('lure'/'7ED', 'All creatures able to block enchanted creature do so.').
card_image_name('lure'/'7ED', 'lure').
card_uid('lure'/'7ED', '7ED:Lure:lure').
card_rarity('lure'/'7ED', 'Uncommon').
card_artist('lure'/'7ED', 'Larry Elmore').
card_number('lure'/'7ED', '255').
card_flavor_text('lure'/'7ED', '\"When the forest wants something, no mortal mind can resist her.\"\n—Maro').
card_multiverse_id('lure'/'7ED', '11390').

card_in_set('mahamoti djinn', '7ED').
card_original_type('mahamoti djinn'/'7ED', 'Creature — Djinn').
card_original_text('mahamoti djinn'/'7ED', 'Flying').
card_image_name('mahamoti djinn'/'7ED', 'mahamoti djinn').
card_uid('mahamoti djinn'/'7ED', '7ED:Mahamoti Djinn:mahamoti djinn').
card_rarity('mahamoti djinn'/'7ED', 'Rare').
card_artist('mahamoti djinn'/'7ED', 'Eric Peterson').
card_number('mahamoti djinn'/'7ED', '84').
card_flavor_text('mahamoti djinn'/'7ED', 'Of royal blood among the spirits of the air, the Mahamoti djinn rides on the wings of the winds. As dangerous in the gambling hall as he is in battle, he is a master of trickery and misdirection.').
card_multiverse_id('mahamoti djinn'/'7ED', '25566').

card_in_set('mana breach', '7ED').
card_original_type('mana breach'/'7ED', 'Enchantment').
card_original_text('mana breach'/'7ED', 'Whenever a player plays a spell, that player returns a land he or she controls to its owner\'s hand.').
card_image_name('mana breach'/'7ED', 'mana breach').
card_uid('mana breach'/'7ED', '7ED:Mana Breach:mana breach').
card_rarity('mana breach'/'7ED', 'Uncommon').
card_artist('mana breach'/'7ED', 'Gary Ruddell').
card_number('mana breach'/'7ED', '85').
card_flavor_text('mana breach'/'7ED', 'The mana that binds the world together can also tear it apart.').
card_multiverse_id('mana breach'/'7ED', '25563').

card_in_set('mana clash', '7ED').
card_original_type('mana clash'/'7ED', 'Sorcery').
card_original_text('mana clash'/'7ED', 'You and target opponent each flip a coin. Mana Clash deals 1 damage to each player whose coin comes up tails. Repeat this process until both players\' coins come up heads on the same flip.').
card_image_name('mana clash'/'7ED', 'mana clash').
card_uid('mana clash'/'7ED', '7ED:Mana Clash:mana clash').
card_rarity('mana clash'/'7ED', 'Rare').
card_artist('mana clash'/'7ED', 'Larry Elmore').
card_number('mana clash'/'7ED', '202').
card_multiverse_id('mana clash'/'7ED', '25682').

card_in_set('mana short', '7ED').
card_original_type('mana short'/'7ED', 'Instant').
card_original_text('mana short'/'7ED', 'Tap all lands target player controls and empty his or her mana pool.').
card_image_name('mana short'/'7ED', 'mana short').
card_uid('mana short'/'7ED', '7ED:Mana Short:mana short').
card_rarity('mana short'/'7ED', 'Rare').
card_artist('mana short'/'7ED', 'Greg Staples').
card_number('mana short'/'7ED', '86').
card_flavor_text('mana short'/'7ED', '\"Remove their power, and their will to fight will follow.\"\n—Tolarian wizard').
card_multiverse_id('mana short'/'7ED', '25567').

card_in_set('marble diamond', '7ED').
card_original_type('marble diamond'/'7ED', 'Artifact').
card_original_text('marble diamond'/'7ED', 'Marble Diamond comes into play tapped.\n{T}: Add {W} to your mana pool.').
card_image_name('marble diamond'/'7ED', 'marble diamond').
card_uid('marble diamond'/'7ED', '7ED:Marble Diamond:marble diamond').
card_rarity('marble diamond'/'7ED', 'Uncommon').
card_artist('marble diamond'/'7ED', 'David Martin').
card_number('marble diamond'/'7ED', '306').
card_multiverse_id('marble diamond'/'7ED', '15850').

card_in_set('maro', '7ED').
card_original_type('maro'/'7ED', 'Creature — Elemental').
card_original_text('maro'/'7ED', 'Maro\'s power and toughness are each equal to the number of cards in your hand.').
card_image_name('maro'/'7ED', 'maro').
card_uid('maro'/'7ED', '7ED:Maro:maro').
card_rarity('maro'/'7ED', 'Rare').
card_artist('maro'/'7ED', 'Pete Venters').
card_number('maro'/'7ED', '256').
card_flavor_text('maro'/'7ED', 'No two see the same Maro.').
card_multiverse_id('maro'/'7ED', '12965').

card_in_set('master healer', '7ED').
card_original_type('master healer'/'7ED', 'Creature — Cleric').
card_original_text('master healer'/'7ED', '{T}: Prevent the next 4 damage that would be dealt to target creature or player this turn.').
card_image_name('master healer'/'7ED', 'master healer').
card_uid('master healer'/'7ED', '7ED:Master Healer:master healer').
card_rarity('master healer'/'7ED', 'Rare').
card_artist('master healer'/'7ED', 'Greg & Tim Hildebrandt').
card_number('master healer'/'7ED', '27').
card_flavor_text('master healer'/'7ED', 'Her heart holds the pain of every soldier she\'s ever healed.').
card_multiverse_id('master healer'/'7ED', '15790').

card_in_set('mawcor', '7ED').
card_original_type('mawcor'/'7ED', 'Creature — Beast').
card_original_text('mawcor'/'7ED', 'Flying\n{T}: Mawcor deals 1 damage to target creature or player.').
card_image_name('mawcor'/'7ED', 'mawcor').
card_uid('mawcor'/'7ED', '7ED:Mawcor:mawcor').
card_rarity('mawcor'/'7ED', 'Rare').
card_artist('mawcor'/'7ED', 'Kev Walker').
card_number('mawcor'/'7ED', '87').
card_flavor_text('mawcor'/'7ED', 'Neither head holds a brain—that\'s located somewhere in the chest—but four eyes aim much better than two.').
card_multiverse_id('mawcor'/'7ED', '13035').

card_in_set('meekstone', '7ED').
card_original_type('meekstone'/'7ED', 'Artifact').
card_original_text('meekstone'/'7ED', 'Creatures with power 3 or greater don\'t untap during their controllers\' untap steps.').
card_image_name('meekstone'/'7ED', 'meekstone').
card_uid('meekstone'/'7ED', '7ED:Meekstone:meekstone').
card_rarity('meekstone'/'7ED', 'Rare').
card_artist('meekstone'/'7ED', 'David Martin').
card_number('meekstone'/'7ED', '307').
card_flavor_text('meekstone'/'7ED', 'The bigger they are, the stronger its pull.').
card_multiverse_id('meekstone'/'7ED', '11398').

card_in_set('megrim', '7ED').
card_original_type('megrim'/'7ED', 'Enchantment').
card_original_text('megrim'/'7ED', 'Whenever an opponent discards a card, Megrim deals 2 damage to that player.').
card_image_name('megrim'/'7ED', 'megrim').
card_uid('megrim'/'7ED', '7ED:Megrim:megrim').
card_rarity('megrim'/'7ED', 'Uncommon').
card_artist('megrim'/'7ED', 'Peter Bollinger').
card_number('megrim'/'7ED', '146').
card_flavor_text('megrim'/'7ED', 'Memories cut from the mind can cut right back.').
card_multiverse_id('megrim'/'7ED', '25631').

card_in_set('memory lapse', '7ED').
card_original_type('memory lapse'/'7ED', 'Instant').
card_original_text('memory lapse'/'7ED', 'Counter target spell. Put it on top of its owner\'s library instead of into that player\'s graveyard.').
card_image_name('memory lapse'/'7ED', 'memory lapse').
card_uid('memory lapse'/'7ED', '7ED:Memory Lapse:memory lapse').
card_rarity('memory lapse'/'7ED', 'Common').
card_artist('memory lapse'/'7ED', 'Tristan Elwell').
card_number('memory lapse'/'7ED', '88').
card_flavor_text('memory lapse'/'7ED', '\"Eri phar phesta . . . um, phistor . . . er, phistara . . .\"').
card_multiverse_id('memory lapse'/'7ED', '27080').

card_in_set('merfolk looter', '7ED').
card_original_type('merfolk looter'/'7ED', 'Creature — Merfolk').
card_original_text('merfolk looter'/'7ED', '{T}: Draw a card, then discard a card from your hand.').
card_image_name('merfolk looter'/'7ED', 'merfolk looter').
card_uid('merfolk looter'/'7ED', '7ED:Merfolk Looter:merfolk looter').
card_rarity('merfolk looter'/'7ED', 'Uncommon').
card_artist('merfolk looter'/'7ED', 'Tristan Elwell').
card_number('merfolk looter'/'7ED', '89').
card_flavor_text('merfolk looter'/'7ED', 'When you sink a lot of ships, it\'s easy to find sunken treasure.').
card_multiverse_id('merfolk looter'/'7ED', '13140').

card_in_set('merfolk of the pearl trident', '7ED').
card_original_type('merfolk of the pearl trident'/'7ED', 'Creature — Merfolk').
card_original_text('merfolk of the pearl trident'/'7ED', '').
card_image_name('merfolk of the pearl trident'/'7ED', 'merfolk of the pearl trident').
card_uid('merfolk of the pearl trident'/'7ED', '7ED:Merfolk of the Pearl Trident:merfolk of the pearl trident').
card_rarity('merfolk of the pearl trident'/'7ED', 'Common').
card_artist('merfolk of the pearl trident'/'7ED', 'Ray Lago').
card_number('merfolk of the pearl trident'/'7ED', '90').
card_flavor_text('merfolk of the pearl trident'/'7ED', '\"The paladins would be wise not to forget about the Pearl Trident. The merfolk warriors have turned the tide of more than one human war . . . and I have no doubt they shall do so again.\"\n—Onean scholar').
card_multiverse_id('merfolk of the pearl trident'/'7ED', '10810').

card_in_set('might of oaks', '7ED').
card_original_type('might of oaks'/'7ED', 'Instant').
card_original_text('might of oaks'/'7ED', 'Target creature gets +7/+7 until end of turn.').
card_image_name('might of oaks'/'7ED', 'might of oaks').
card_uid('might of oaks'/'7ED', '7ED:Might of Oaks:might of oaks').
card_rarity('might of oaks'/'7ED', 'Rare').
card_artist('might of oaks'/'7ED', 'Greg Staples').
card_number('might of oaks'/'7ED', '257').
card_flavor_text('might of oaks'/'7ED', '\"Guess where I\'m gonna plant this!\"').
card_multiverse_id('might of oaks'/'7ED', '13148').

card_in_set('millstone', '7ED').
card_original_type('millstone'/'7ED', 'Artifact').
card_original_text('millstone'/'7ED', '{2}, {T}: Target player puts the top two cards of his or her library into his or her graveyard.').
card_image_name('millstone'/'7ED', 'millstone').
card_uid('millstone'/'7ED', '7ED:Millstone:millstone').
card_rarity('millstone'/'7ED', 'Rare').
card_artist('millstone'/'7ED', 'John Avon').
card_number('millstone'/'7ED', '308').
card_flavor_text('millstone'/'7ED', 'More than one mage has been driven insane by the sound of the millstone relentlessly grinding away.').
card_multiverse_id('millstone'/'7ED', '11403').

card_in_set('mind rot', '7ED').
card_original_type('mind rot'/'7ED', 'Sorcery').
card_original_text('mind rot'/'7ED', 'Target player discards two cards from his or her hand.').
card_image_name('mind rot'/'7ED', 'mind rot').
card_uid('mind rot'/'7ED', '7ED:Mind Rot:mind rot').
card_rarity('mind rot'/'7ED', 'Common').
card_artist('mind rot'/'7ED', 'Adam Rex').
card_number('mind rot'/'7ED', '147').
card_flavor_text('mind rot'/'7ED', 'What wizards fear most is losing their minds.').
card_multiverse_id('mind rot'/'7ED', '15828').

card_in_set('monstrous growth', '7ED').
card_original_type('monstrous growth'/'7ED', 'Sorcery').
card_original_text('monstrous growth'/'7ED', 'Target creature gets +4/+4 until end of turn.').
card_image_name('monstrous growth'/'7ED', 'monstrous growth').
card_uid('monstrous growth'/'7ED', '7ED:Monstrous Growth:monstrous growth').
card_rarity('monstrous growth'/'7ED', 'Common').
card_artist('monstrous growth'/'7ED', 'Ron Spencer').
card_number('monstrous growth'/'7ED', '258').
card_flavor_text('monstrous growth'/'7ED', 'Get in touch with your inner monster.').
card_multiverse_id('monstrous growth'/'7ED', '30169').

card_in_set('moss diamond', '7ED').
card_original_type('moss diamond'/'7ED', 'Artifact').
card_original_text('moss diamond'/'7ED', 'Moss Diamond comes into play tapped.\n{T}: Add {G} to your mana pool.').
card_image_name('moss diamond'/'7ED', 'moss diamond').
card_uid('moss diamond'/'7ED', '7ED:Moss Diamond:moss diamond').
card_rarity('moss diamond'/'7ED', 'Uncommon').
card_artist('moss diamond'/'7ED', 'David Martin').
card_number('moss diamond'/'7ED', '309').
card_multiverse_id('moss diamond'/'7ED', '15854').

card_in_set('mountain', '7ED').
card_original_type('mountain'/'7ED', 'Land').
card_original_text('mountain'/'7ED', 'R').
card_image_name('mountain'/'7ED', 'mountain1').
card_uid('mountain'/'7ED', '7ED:Mountain:mountain1').
card_rarity('mountain'/'7ED', 'Basic Land').
card_artist('mountain'/'7ED', 'D. J. Cleland-Hura').
card_number('mountain'/'7ED', '337').
card_multiverse_id('mountain'/'7ED', '11410').

card_in_set('mountain', '7ED').
card_original_type('mountain'/'7ED', 'Land').
card_original_text('mountain'/'7ED', 'R').
card_image_name('mountain'/'7ED', 'mountain2').
card_uid('mountain'/'7ED', '7ED:Mountain:mountain2').
card_rarity('mountain'/'7ED', 'Basic Land').
card_artist('mountain'/'7ED', 'Rob Alexander').
card_number('mountain'/'7ED', '338').
card_multiverse_id('mountain'/'7ED', '11411').

card_in_set('mountain', '7ED').
card_original_type('mountain'/'7ED', 'Land').
card_original_text('mountain'/'7ED', 'R').
card_image_name('mountain'/'7ED', 'mountain3').
card_uid('mountain'/'7ED', '7ED:Mountain:mountain3').
card_rarity('mountain'/'7ED', 'Basic Land').
card_artist('mountain'/'7ED', 'Rob Alexander').
card_number('mountain'/'7ED', '339').
card_multiverse_id('mountain'/'7ED', '11412').

card_in_set('mountain', '7ED').
card_original_type('mountain'/'7ED', 'Land').
card_original_text('mountain'/'7ED', 'R').
card_image_name('mountain'/'7ED', 'mountain4').
card_uid('mountain'/'7ED', '7ED:Mountain:mountain4').
card_rarity('mountain'/'7ED', 'Basic Land').
card_artist('mountain'/'7ED', 'John Avon').
card_number('mountain'/'7ED', '340').
card_multiverse_id('mountain'/'7ED', '11413').

card_in_set('nature\'s resurgence', '7ED').
card_original_type('nature\'s resurgence'/'7ED', 'Sorcery').
card_original_text('nature\'s resurgence'/'7ED', 'Each player draws a card for each creature card in his or her graveyard.').
card_image_name('nature\'s resurgence'/'7ED', 'nature\'s resurgence').
card_uid('nature\'s resurgence'/'7ED', '7ED:Nature\'s Resurgence:nature\'s resurgence').
card_rarity('nature\'s resurgence'/'7ED', 'Rare').
card_artist('nature\'s resurgence'/'7ED', 'Gary Ruddell').
card_number('nature\'s resurgence'/'7ED', '259').
card_flavor_text('nature\'s resurgence'/'7ED', '\"The dead whisper to the living. If you listen, you will learn.\"\n—Maro').
card_multiverse_id('nature\'s resurgence'/'7ED', '12998').

card_in_set('nature\'s revolt', '7ED').
card_original_type('nature\'s revolt'/'7ED', 'Enchantment').
card_original_text('nature\'s revolt'/'7ED', 'All lands are 2/2 creatures that are still lands.').
card_image_name('nature\'s revolt'/'7ED', 'nature\'s revolt').
card_uid('nature\'s revolt'/'7ED', '7ED:Nature\'s Revolt:nature\'s revolt').
card_rarity('nature\'s revolt'/'7ED', 'Rare').
card_artist('nature\'s revolt'/'7ED', 'Dave Dorman').
card_number('nature\'s revolt'/'7ED', '260').
card_flavor_text('nature\'s revolt'/'7ED', 'The land rises up to defend against those who would spoil its beauty.').
card_multiverse_id('nature\'s revolt'/'7ED', '25592').

card_in_set('nausea', '7ED').
card_original_type('nausea'/'7ED', 'Sorcery').
card_original_text('nausea'/'7ED', 'All creatures get -1/-1 until end of turn.').
card_image_name('nausea'/'7ED', 'nausea').
card_uid('nausea'/'7ED', '7ED:Nausea:nausea').
card_rarity('nausea'/'7ED', 'Common').
card_artist('nausea'/'7ED', 'James Bernardin').
card_number('nausea'/'7ED', '148').
card_flavor_text('nausea'/'7ED', 'No fear. No sense. And soon, no lunch.').
card_multiverse_id('nausea'/'7ED', '25623').

card_in_set('necrologia', '7ED').
card_original_type('necrologia'/'7ED', 'Instant').
card_original_text('necrologia'/'7ED', 'Play Necrologia only during your end of turn step.\nAs an additional cost to play Necrologia, pay any amount of life.\nDraw cards equal to the life paid this way.').
card_image_name('necrologia'/'7ED', 'necrologia').
card_uid('necrologia'/'7ED', '7ED:Necrologia:necrologia').
card_rarity('necrologia'/'7ED', 'Uncommon').
card_artist('necrologia'/'7ED', 'Scott M. Fischer').
card_number('necrologia'/'7ED', '149').
card_multiverse_id('necrologia'/'7ED', '25635').

card_in_set('nightmare', '7ED').
card_original_type('nightmare'/'7ED', 'Creature — Nightmare').
card_original_text('nightmare'/'7ED', 'Flying\nNightmare\'s power and toughness are each equal to the number of swamps you control.').
card_image_name('nightmare'/'7ED', 'nightmare').
card_uid('nightmare'/'7ED', '7ED:Nightmare:nightmare').
card_rarity('nightmare'/'7ED', 'Rare').
card_artist('nightmare'/'7ED', 'Carl Critchlow').
card_number('nightmare'/'7ED', '150').
card_flavor_text('nightmare'/'7ED', 'The thunder of its hooves beats dreams into despair.').
card_multiverse_id('nightmare'/'7ED', '15826').

card_in_set('nocturnal raid', '7ED').
card_original_type('nocturnal raid'/'7ED', 'Instant').
card_original_text('nocturnal raid'/'7ED', 'Black creatures get +2/+0 until end of turn.').
card_image_name('nocturnal raid'/'7ED', 'nocturnal raid').
card_uid('nocturnal raid'/'7ED', '7ED:Nocturnal Raid:nocturnal raid').
card_rarity('nocturnal raid'/'7ED', 'Uncommon').
card_artist('nocturnal raid'/'7ED', 'Pete Venters').
card_number('nocturnal raid'/'7ED', '151').
card_flavor_text('nocturnal raid'/'7ED', 'The Eastern Paladin doesn\'t attack at night for the tactical advantage. He does it for the sheer joy of fighting beneath the moon.').
card_multiverse_id('nocturnal raid'/'7ED', '25633').

card_in_set('northern paladin', '7ED').
card_original_type('northern paladin'/'7ED', 'Creature — Knight').
card_original_text('northern paladin'/'7ED', '{W}{W}, {T}: Destroy target black permanent.').
card_image_name('northern paladin'/'7ED', 'northern paladin').
card_uid('northern paladin'/'7ED', '7ED:Northern Paladin:northern paladin').
card_rarity('northern paladin'/'7ED', 'Rare').
card_artist('northern paladin'/'7ED', 'Carl Critchlow').
card_number('northern paladin'/'7ED', '28').
card_flavor_text('northern paladin'/'7ED', '\"How smooth his face, how sure his thoughts. I look forward to breaking his mighty will.\"\n—The Eastern Paladin').
card_multiverse_id('northern paladin'/'7ED', '13166').

card_in_set('ogre taskmaster', '7ED').
card_original_type('ogre taskmaster'/'7ED', 'Creature — Ogre').
card_original_text('ogre taskmaster'/'7ED', 'Ogre Taskmaster can\'t block.').
card_image_name('ogre taskmaster'/'7ED', 'ogre taskmaster').
card_uid('ogre taskmaster'/'7ED', '7ED:Ogre Taskmaster:ogre taskmaster').
card_rarity('ogre taskmaster'/'7ED', 'Uncommon').
card_artist('ogre taskmaster'/'7ED', 'Jeff Easley').
card_number('ogre taskmaster'/'7ED', '203').
card_flavor_text('ogre taskmaster'/'7ED', 'Ogres learn how to smash stuff even before they learn how to walk.').
card_multiverse_id('ogre taskmaster'/'7ED', '15817').

card_in_set('okk', '7ED').
card_original_type('okk'/'7ED', 'Creature — Goblin').
card_original_text('okk'/'7ED', 'Okk can\'t attack unless a creature with greater power also attacks.\nOkk can\'t block unless a creature with greater power also blocks.').
card_image_name('okk'/'7ED', 'okk').
card_uid('okk'/'7ED', '7ED:Okk:okk').
card_rarity('okk'/'7ED', 'Rare').
card_artist('okk'/'7ED', 'Peter Bollinger').
card_number('okk'/'7ED', '204').
card_multiverse_id('okk'/'7ED', '25686').

card_in_set('opportunity', '7ED').
card_original_type('opportunity'/'7ED', 'Instant').
card_original_text('opportunity'/'7ED', 'Target player draws four cards.').
card_image_name('opportunity'/'7ED', 'opportunity').
card_uid('opportunity'/'7ED', '7ED:Opportunity:opportunity').
card_rarity('opportunity'/'7ED', 'Uncommon').
card_artist('opportunity'/'7ED', 'Patrick Faricy').
card_number('opportunity'/'7ED', '91').
card_flavor_text('opportunity'/'7ED', 'Little things like morals shouldn\'t hinder one\'s ambition.\n—Pirate creed').
card_multiverse_id('opportunity'/'7ED', '25564').

card_in_set('opposition', '7ED').
card_original_type('opposition'/'7ED', 'Enchantment').
card_original_text('opposition'/'7ED', 'Tap an untapped creature you control: Tap target artifact, creature, or land.').
card_image_name('opposition'/'7ED', 'opposition').
card_uid('opposition'/'7ED', '7ED:Opposition:opposition').
card_rarity('opposition'/'7ED', 'Rare').
card_artist('opposition'/'7ED', 'Mark Brill').
card_number('opposition'/'7ED', '92').
card_flavor_text('opposition'/'7ED', 'Exhausting a wizard can be exhausting work.').
card_multiverse_id('opposition'/'7ED', '15796').

card_in_set('oppression', '7ED').
card_original_type('oppression'/'7ED', 'Enchantment').
card_original_text('oppression'/'7ED', 'Whenever a player plays a spell, that player discards a card from his or her hand.').
card_image_name('oppression'/'7ED', 'oppression').
card_uid('oppression'/'7ED', '7ED:Oppression:oppression').
card_rarity('oppression'/'7ED', 'Rare').
card_artist('oppression'/'7ED', 'Alex Horley-Orlandelli').
card_number('oppression'/'7ED', '152').
card_flavor_text('oppression'/'7ED', '\"Defeating an enemy is not nearly as satisfying as watching one suffer.\"\n—The Western Paladin').
card_multiverse_id('oppression'/'7ED', '25641').

card_in_set('orcish artillery', '7ED').
card_original_type('orcish artillery'/'7ED', 'Creature — Orc').
card_original_text('orcish artillery'/'7ED', '{T}: Orcish Artillery deals 2 damage to target creature or player and 3 damage to you.').
card_image_name('orcish artillery'/'7ED', 'orcish artillery').
card_uid('orcish artillery'/'7ED', '7ED:Orcish Artillery:orcish artillery').
card_rarity('orcish artillery'/'7ED', 'Uncommon').
card_artist('orcish artillery'/'7ED', 'Dan Frazier').
card_number('orcish artillery'/'7ED', '205').
card_flavor_text('orcish artillery'/'7ED', '\"Ready . . . aim . . . fire\"\n\"Ow\"').
card_multiverse_id('orcish artillery'/'7ED', '15812').

card_in_set('orcish oriflamme', '7ED').
card_original_type('orcish oriflamme'/'7ED', 'Enchantment').
card_original_text('orcish oriflamme'/'7ED', 'Attacking creatures you control get +1/+0.').
card_image_name('orcish oriflamme'/'7ED', 'orcish oriflamme').
card_uid('orcish oriflamme'/'7ED', '7ED:Orcish Oriflamme:orcish oriflamme').
card_rarity('orcish oriflamme'/'7ED', 'Uncommon').
card_artist('orcish oriflamme'/'7ED', 'Ben Thompson').
card_number('orcish oriflamme'/'7ED', '206').
card_multiverse_id('orcish oriflamme'/'7ED', '11427').

card_in_set('ostracize', '7ED').
card_original_type('ostracize'/'7ED', 'Sorcery').
card_original_text('ostracize'/'7ED', 'Target opponent reveals his or her hand. Choose a creature card from it. That player discards that card.').
card_image_name('ostracize'/'7ED', 'ostracize').
card_uid('ostracize'/'7ED', '7ED:Ostracize:ostracize').
card_rarity('ostracize'/'7ED', 'Common').
card_artist('ostracize'/'7ED', 'Hannibal King').
card_number('ostracize'/'7ED', '153').
card_flavor_text('ostracize'/'7ED', '\"You won\'t be needing this.\"').
card_multiverse_id('ostracize'/'7ED', '12980').

card_in_set('pacifism', '7ED').
card_original_type('pacifism'/'7ED', 'Enchant Creature').
card_original_text('pacifism'/'7ED', 'Enchanted creature can\'t attack or block.').
card_image_name('pacifism'/'7ED', 'pacifism').
card_uid('pacifism'/'7ED', '7ED:Pacifism:pacifism').
card_rarity('pacifism'/'7ED', 'Common').
card_artist('pacifism'/'7ED', 'Eric Peterson').
card_number('pacifism'/'7ED', '29').
card_flavor_text('pacifism'/'7ED', 'Without warriors there can be no war.').
card_multiverse_id('pacifism'/'7ED', '12978').

card_in_set('pariah', '7ED').
card_original_type('pariah'/'7ED', 'Enchant Creature').
card_original_text('pariah'/'7ED', 'All damage that would be dealt to you is dealt to enchanted creature instead.').
card_image_name('pariah'/'7ED', 'pariah').
card_uid('pariah'/'7ED', '7ED:Pariah:pariah').
card_rarity('pariah'/'7ED', 'Rare').
card_artist('pariah'/'7ED', 'Scott M. Fischer').
card_number('pariah'/'7ED', '30').
card_flavor_text('pariah'/'7ED', 'A valiant knight\'s life has no meaning without honor.').
card_multiverse_id('pariah'/'7ED', '11155').

card_in_set('patagia golem', '7ED').
card_original_type('patagia golem'/'7ED', 'Artifact Creature — Golem').
card_original_text('patagia golem'/'7ED', '{3}: Patagia Golem gains flying until end of turn.').
card_image_name('patagia golem'/'7ED', 'patagia golem').
card_uid('patagia golem'/'7ED', '7ED:Patagia Golem:patagia golem').
card_rarity('patagia golem'/'7ED', 'Uncommon').
card_artist('patagia golem'/'7ED', 'Kev Walker').
card_number('patagia golem'/'7ED', '310').
card_flavor_text('patagia golem'/'7ED', 'Its wings were only designed to be ornamental, but it learned to use them on its own.').
card_multiverse_id('patagia golem'/'7ED', '15856').

card_in_set('persecute', '7ED').
card_original_type('persecute'/'7ED', 'Sorcery').
card_original_text('persecute'/'7ED', 'Choose a color. Target player reveals his or her hand and discards all cards of that color from it.').
card_image_name('persecute'/'7ED', 'persecute').
card_uid('persecute'/'7ED', '7ED:Persecute:persecute').
card_rarity('persecute'/'7ED', 'Rare').
card_artist('persecute'/'7ED', 'Luca Zontini').
card_number('persecute'/'7ED', '154').
card_flavor_text('persecute'/'7ED', '\"I want every last one of these tree-hugging, earth-loving, pointy-eared weaklings out of here. Now!\"').
card_multiverse_id('persecute'/'7ED', '25644').

card_in_set('phantom warrior', '7ED').
card_original_type('phantom warrior'/'7ED', 'Creature — Illusion').
card_original_text('phantom warrior'/'7ED', 'Phantom Warrior is unblockable.').
card_image_name('phantom warrior'/'7ED', 'phantom warrior').
card_uid('phantom warrior'/'7ED', '7ED:Phantom Warrior:phantom warrior').
card_rarity('phantom warrior'/'7ED', 'Uncommon').
card_artist('phantom warrior'/'7ED', 'Greg Staples').
card_number('phantom warrior'/'7ED', '93').
card_flavor_text('phantom warrior'/'7ED', 'It can pass through solid matter—but that doesn\'t mean it\'s harmless.').
card_multiverse_id('phantom warrior'/'7ED', '12997').

card_in_set('phyrexian colossus', '7ED').
card_original_type('phyrexian colossus'/'7ED', 'Artifact Creature').
card_original_text('phyrexian colossus'/'7ED', 'Phyrexian Colossus doesn\'t untap during your untap step.\nPay 8 life: Untap Phyrexian Colossus.\nPhyrexian Colossus can\'t be blocked except by three or more creatures.').
card_image_name('phyrexian colossus'/'7ED', 'phyrexian colossus').
card_uid('phyrexian colossus'/'7ED', '7ED:Phyrexian Colossus:phyrexian colossus').
card_rarity('phyrexian colossus'/'7ED', 'Rare').
card_artist('phyrexian colossus'/'7ED', 'Mark Tedin').
card_number('phyrexian colossus'/'7ED', '311').
card_multiverse_id('phyrexian colossus'/'7ED', '15860').

card_in_set('phyrexian hulk', '7ED').
card_original_type('phyrexian hulk'/'7ED', 'Artifact Creature').
card_original_text('phyrexian hulk'/'7ED', '').
card_image_name('phyrexian hulk'/'7ED', 'phyrexian hulk').
card_uid('phyrexian hulk'/'7ED', '7ED:Phyrexian Hulk:phyrexian hulk').
card_rarity('phyrexian hulk'/'7ED', 'Uncommon').
card_artist('phyrexian hulk'/'7ED', 'Brian Snõddy').
card_number('phyrexian hulk'/'7ED', '312').
card_flavor_text('phyrexian hulk'/'7ED', 'It doesn\'t think. It doesn\'t feel.\nIt doesn\'t laugh or cry.\nAll it does from dusk till dawn\nIs make the soldiers die.\n—Onean children\'s rhyme').
card_multiverse_id('phyrexian hulk'/'7ED', '25656').

card_in_set('pillage', '7ED').
card_original_type('pillage'/'7ED', 'Sorcery').
card_original_text('pillage'/'7ED', 'Destroy target artifact or land. It can\'t be regenerated.').
card_image_name('pillage'/'7ED', 'pillage').
card_uid('pillage'/'7ED', '7ED:Pillage:pillage').
card_rarity('pillage'/'7ED', 'Uncommon').
card_artist('pillage'/'7ED', 'Bradley Williams').
card_number('pillage'/'7ED', '207').
card_flavor_text('pillage'/'7ED', '\"Burn their homes. Salt their lands. Nothing survives.\"\n—The Western Paladin').
card_multiverse_id('pillage'/'7ED', '13182').

card_in_set('pit trap', '7ED').
card_original_type('pit trap'/'7ED', 'Artifact').
card_original_text('pit trap'/'7ED', '{2}, {T}, Sacrifice Pit Trap: Destroy target attacking creature without flying. It can\'t be regenerated.').
card_image_name('pit trap'/'7ED', 'pit trap').
card_uid('pit trap'/'7ED', '7ED:Pit Trap:pit trap').
card_rarity('pit trap'/'7ED', 'Uncommon').
card_artist('pit trap'/'7ED', 'Nelson DeCastro').
card_number('pit trap'/'7ED', '313').
card_flavor_text('pit trap'/'7ED', 'Never forget your feet\n—Elvish expression meaning\n\"watch your step\"').
card_multiverse_id('pit trap'/'7ED', '25657').

card_in_set('plague beetle', '7ED').
card_original_type('plague beetle'/'7ED', 'Creature — Insect').
card_original_text('plague beetle'/'7ED', 'Swampwalk (This creature is unblockable as long as defending player controls a swamp.)').
card_image_name('plague beetle'/'7ED', 'plague beetle').
card_uid('plague beetle'/'7ED', '7ED:Plague Beetle:plague beetle').
card_rarity('plague beetle'/'7ED', 'Common').
card_artist('plague beetle'/'7ED', 'Matt Cavotta').
card_number('plague beetle'/'7ED', '155').
card_flavor_text('plague beetle'/'7ED', 'No one knows whether they were named for the diseases they carry or for the speed at which they multiply.').
card_multiverse_id('plague beetle'/'7ED', '13158').

card_in_set('plains', '7ED').
card_original_type('plains'/'7ED', 'Land').
card_original_text('plains'/'7ED', 'W').
card_image_name('plains'/'7ED', 'plains1').
card_uid('plains'/'7ED', '7ED:Plains:plains1').
card_rarity('plains'/'7ED', 'Basic Land').
card_artist('plains'/'7ED', 'Scott Bailey').
card_number('plains'/'7ED', '341').
card_multiverse_id('plains'/'7ED', '11446').

card_in_set('plains', '7ED').
card_original_type('plains'/'7ED', 'Land').
card_original_text('plains'/'7ED', 'W').
card_image_name('plains'/'7ED', 'plains2').
card_uid('plains'/'7ED', '7ED:Plains:plains2').
card_rarity('plains'/'7ED', 'Basic Land').
card_artist('plains'/'7ED', 'Rob Alexander').
card_number('plains'/'7ED', '342').
card_multiverse_id('plains'/'7ED', '11447').

card_in_set('plains', '7ED').
card_original_type('plains'/'7ED', 'Land').
card_original_text('plains'/'7ED', 'W').
card_image_name('plains'/'7ED', 'plains3').
card_uid('plains'/'7ED', '7ED:Plains:plains3').
card_rarity('plains'/'7ED', 'Basic Land').
card_artist('plains'/'7ED', 'Rob Alexander').
card_number('plains'/'7ED', '343').
card_multiverse_id('plains'/'7ED', '11448').

card_in_set('plains', '7ED').
card_original_type('plains'/'7ED', 'Land').
card_original_text('plains'/'7ED', 'W').
card_image_name('plains'/'7ED', 'plains4').
card_uid('plains'/'7ED', '7ED:Plains:plains4').
card_rarity('plains'/'7ED', 'Basic Land').
card_artist('plains'/'7ED', 'John Avon').
card_number('plains'/'7ED', '344').
card_multiverse_id('plains'/'7ED', '11449').

card_in_set('pride of lions', '7ED').
card_original_type('pride of lions'/'7ED', 'Creature — Cat').
card_original_text('pride of lions'/'7ED', 'You may have Pride of Lions deal its combat damage to defending player as though it weren\'t blocked.').
card_image_name('pride of lions'/'7ED', 'pride of lions').
card_uid('pride of lions'/'7ED', '7ED:Pride of Lions:pride of lions').
card_rarity('pride of lions'/'7ED', 'Uncommon').
card_artist('pride of lions'/'7ED', 'Gary Ruddell').
card_number('pride of lions'/'7ED', '261').
card_flavor_text('pride of lions'/'7ED', 'There\'s only one thing worse than a hungry lion—many hungry lions.').
card_multiverse_id('pride of lions'/'7ED', '25667').

card_in_set('prodigal sorcerer', '7ED').
card_original_type('prodigal sorcerer'/'7ED', 'Creature — Wizard').
card_original_text('prodigal sorcerer'/'7ED', '{T}: Prodigal Sorcerer deals 1 damage to target creature or player.').
card_image_name('prodigal sorcerer'/'7ED', 'prodigal sorcerer').
card_uid('prodigal sorcerer'/'7ED', '7ED:Prodigal Sorcerer:prodigal sorcerer').
card_rarity('prodigal sorcerer'/'7ED', 'Common').
card_artist('prodigal sorcerer'/'7ED', 'Tony Szczudlo').
card_number('prodigal sorcerer'/'7ED', '94').
card_flavor_text('prodigal sorcerer'/'7ED', 'He wastes his amazing talents on proving how amazing he really is.').
card_multiverse_id('prodigal sorcerer'/'7ED', '11458').

card_in_set('purify', '7ED').
card_original_type('purify'/'7ED', 'Sorcery').
card_original_text('purify'/'7ED', 'Destroy all artifacts and enchantments.').
card_image_name('purify'/'7ED', 'purify').
card_uid('purify'/'7ED', '7ED:Purify:purify').
card_rarity('purify'/'7ED', 'Rare').
card_artist('purify'/'7ED', 'Doug Chaffee').
card_number('purify'/'7ED', '31').
card_flavor_text('purify'/'7ED', '\"We have no need for these trinkets. We need only the strength of our swords and the virtue of our hearts.\"\n—The Northern Paladin').
card_multiverse_id('purify'/'7ED', '25554').

card_in_set('pygmy pyrosaur', '7ED').
card_original_type('pygmy pyrosaur'/'7ED', 'Creature — Lizard').
card_original_text('pygmy pyrosaur'/'7ED', 'Pygmy Pyrosaur can\'t block.\n{R} Pygmy Pyrosaur gets +1/+0 until end of turn.').
card_image_name('pygmy pyrosaur'/'7ED', 'pygmy pyrosaur').
card_uid('pygmy pyrosaur'/'7ED', '7ED:Pygmy Pyrosaur:pygmy pyrosaur').
card_rarity('pygmy pyrosaur'/'7ED', 'Common').
card_artist('pygmy pyrosaur'/'7ED', 'Dan Frazier').
card_number('pygmy pyrosaur'/'7ED', '208').
card_flavor_text('pygmy pyrosaur'/'7ED', 'Do not judge a lizard by its size.\n—Ghitu proverb').
card_multiverse_id('pygmy pyrosaur'/'7ED', '15879').

card_in_set('pyroclasm', '7ED').
card_original_type('pyroclasm'/'7ED', 'Sorcery').
card_original_text('pyroclasm'/'7ED', 'Pyroclasm deals 2 damage to each creature.').
card_image_name('pyroclasm'/'7ED', 'pyroclasm').
card_uid('pyroclasm'/'7ED', '7ED:Pyroclasm:pyroclasm').
card_rarity('pyroclasm'/'7ED', 'Uncommon').
card_artist('pyroclasm'/'7ED', 'Monte Michael Moore').
card_number('pyroclasm'/'7ED', '209').
card_flavor_text('pyroclasm'/'7ED', '\"When the air burns, only death breathes deep.\"\n—Bogardan mage').
card_multiverse_id('pyroclasm'/'7ED', '25677').

card_in_set('pyrotechnics', '7ED').
card_original_type('pyrotechnics'/'7ED', 'Sorcery').
card_original_text('pyrotechnics'/'7ED', 'Pyrotechnics deals 4 damage divided as you choose among any number of target creatures and/or players.').
card_image_name('pyrotechnics'/'7ED', 'pyrotechnics').
card_uid('pyrotechnics'/'7ED', '7ED:Pyrotechnics:pyrotechnics').
card_rarity('pyrotechnics'/'7ED', 'Uncommon').
card_artist('pyrotechnics'/'7ED', 'John Avon').
card_number('pyrotechnics'/'7ED', '210').
card_flavor_text('pyrotechnics'/'7ED', '\"Who says lightning never strikes twice?\"\n—Storm shaman').
card_multiverse_id('pyrotechnics'/'7ED', '25670').

card_in_set('rag man', '7ED').
card_original_type('rag man'/'7ED', 'Creature — Minion').
card_original_text('rag man'/'7ED', '{B}{B}{B}, {T}: Target opponent reveals his or her hand and discards a creature card at random from it. Play this ability only during your turn.').
card_image_name('rag man'/'7ED', 'rag man').
card_uid('rag man'/'7ED', '7ED:Rag Man:rag man').
card_rarity('rag man'/'7ED', 'Rare').
card_artist('rag man'/'7ED', 'Scott M. Fischer').
card_number('rag man'/'7ED', '156').
card_flavor_text('rag man'/'7ED', 'He hunts what might have been.').
card_multiverse_id('rag man'/'7ED', '11464').

card_in_set('raging goblin', '7ED').
card_original_type('raging goblin'/'7ED', 'Creature — Goblin').
card_original_text('raging goblin'/'7ED', 'Haste (This creature may attack and {T} the turn it comes under your control.)').
card_image_name('raging goblin'/'7ED', 'raging goblin').
card_uid('raging goblin'/'7ED', '7ED:Raging Goblin:raging goblin').
card_rarity('raging goblin'/'7ED', 'Common').
card_artist('raging goblin'/'7ED', 'Peter Bollinger').
card_number('raging goblin'/'7ED', '211').
card_flavor_text('raging goblin'/'7ED', 'He raged at the world, at his family, at his life. But mostly he just raged.').
card_multiverse_id('raging goblin'/'7ED', '13146').

card_in_set('raise dead', '7ED').
card_original_type('raise dead'/'7ED', 'Sorcery').
card_original_text('raise dead'/'7ED', 'Return target creature card from your graveyard to your hand.').
card_image_name('raise dead'/'7ED', 'raise dead').
card_uid('raise dead'/'7ED', '7ED:Raise Dead:raise dead').
card_rarity('raise dead'/'7ED', 'Common').
card_artist('raise dead'/'7ED', 'Carl Critchlow').
card_number('raise dead'/'7ED', '157').
card_flavor_text('raise dead'/'7ED', 'The earth cannot hold that which magic commands.').
card_multiverse_id('raise dead'/'7ED', '11465').

card_in_set('rampant growth', '7ED').
card_original_type('rampant growth'/'7ED', 'Sorcery').
card_original_text('rampant growth'/'7ED', 'Search your library for a basic land card and put that card into play tapped. Then shuffle your library.').
card_image_name('rampant growth'/'7ED', 'rampant growth').
card_uid('rampant growth'/'7ED', '7ED:Rampant Growth:rampant growth').
card_rarity('rampant growth'/'7ED', 'Common').
card_artist('rampant growth'/'7ED', 'Scott M. Fischer').
card_number('rampant growth'/'7ED', '262').
card_flavor_text('rampant growth'/'7ED', 'Nature grows solutions to her problems.').
card_multiverse_id('rampant growth'/'7ED', '11501').

card_in_set('razorfoot griffin', '7ED').
card_original_type('razorfoot griffin'/'7ED', 'Creature — Griffin').
card_original_text('razorfoot griffin'/'7ED', 'Flying, first strike').
card_image_name('razorfoot griffin'/'7ED', 'razorfoot griffin').
card_uid('razorfoot griffin'/'7ED', '7ED:Razorfoot Griffin:razorfoot griffin').
card_rarity('razorfoot griffin'/'7ED', 'Common').
card_artist('razorfoot griffin'/'7ED', 'Pete Venters').
card_number('razorfoot griffin'/'7ED', '32').
card_flavor_text('razorfoot griffin'/'7ED', 'Like a meteor, it strikes from above without warning. Unlike a meteor, it then carries you off and eats you.').
card_multiverse_id('razorfoot griffin'/'7ED', '12977').

card_in_set('razortooth rats', '7ED').
card_original_type('razortooth rats'/'7ED', 'Creature — Rat').
card_original_text('razortooth rats'/'7ED', 'Razortooth Rats can\'t be blocked except by artifact creatures and/or black creatures.').
card_image_name('razortooth rats'/'7ED', 'razortooth rats').
card_uid('razortooth rats'/'7ED', '7ED:Razortooth Rats:razortooth rats').
card_rarity('razortooth rats'/'7ED', 'Common').
card_artist('razortooth rats'/'7ED', 'Carl Critchlow').
card_number('razortooth rats'/'7ED', '158').
card_flavor_text('razortooth rats'/'7ED', 'They\'ll chew through armor, flesh, and bone. They\'ll even chew through each other if they\'re hungry enough.').
card_multiverse_id('razortooth rats'/'7ED', '12996').

card_in_set('reckless embermage', '7ED').
card_original_type('reckless embermage'/'7ED', 'Creature — Wizard').
card_original_text('reckless embermage'/'7ED', '{1}{R} Reckless Embermage deals 1 damage to target creature or player and 1 damage to itself.').
card_image_name('reckless embermage'/'7ED', 'reckless embermage').
card_uid('reckless embermage'/'7ED', '7ED:Reckless Embermage:reckless embermage').
card_rarity('reckless embermage'/'7ED', 'Rare').
card_artist('reckless embermage'/'7ED', 'Bob Petillo').
card_number('reckless embermage'/'7ED', '212').
card_flavor_text('reckless embermage'/'7ED', 'Those who play with fire are bound to get burned.').
card_multiverse_id('reckless embermage'/'7ED', '25687').

card_in_set('reclaim', '7ED').
card_original_type('reclaim'/'7ED', 'Instant').
card_original_text('reclaim'/'7ED', 'Put target card from your graveyard on top of your library.').
card_image_name('reclaim'/'7ED', 'reclaim').
card_uid('reclaim'/'7ED', '7ED:Reclaim:reclaim').
card_rarity('reclaim'/'7ED', 'Common').
card_artist('reclaim'/'7ED', 'Greg & Tim Hildebrandt').
card_number('reclaim'/'7ED', '263').
card_flavor_text('reclaim'/'7ED', 'The wise pay as much attention to what they throw away as to what they keep.').
card_multiverse_id('reclaim'/'7ED', '15837').

card_in_set('redwood treefolk', '7ED').
card_original_type('redwood treefolk'/'7ED', 'Creature — Treefolk').
card_original_text('redwood treefolk'/'7ED', '').
card_image_name('redwood treefolk'/'7ED', 'redwood treefolk').
card_uid('redwood treefolk'/'7ED', '7ED:Redwood Treefolk:redwood treefolk').
card_rarity('redwood treefolk'/'7ED', 'Common').
card_artist('redwood treefolk'/'7ED', 'D. J. Cleland-Hura').
card_number('redwood treefolk'/'7ED', '264').
card_flavor_text('redwood treefolk'/'7ED', 'And would you strip the bark from a treefolk?\n—Elvish expression meaning\n\"Are you crazy?\"').
card_multiverse_id('redwood treefolk'/'7ED', '12999').

card_in_set('reflexes', '7ED').
card_original_type('reflexes'/'7ED', 'Enchant Creature').
card_original_text('reflexes'/'7ED', 'Enchanted creature has first strike.').
card_image_name('reflexes'/'7ED', 'reflexes').
card_uid('reflexes'/'7ED', '7ED:Reflexes:reflexes').
card_rarity('reflexes'/'7ED', 'Common').
card_artist('reflexes'/'7ED', 'Donato Giancola').
card_number('reflexes'/'7ED', '213').
card_flavor_text('reflexes'/'7ED', '\"Be as fast as lightning, and you will be just as deadly.\"\n—Combat trainer').
card_multiverse_id('reflexes'/'7ED', '15833').

card_in_set('regeneration', '7ED').
card_original_type('regeneration'/'7ED', 'Enchant Creature').
card_original_text('regeneration'/'7ED', '{G} Regenerate enchanted creature.').
card_image_name('regeneration'/'7ED', 'regeneration').
card_uid('regeneration'/'7ED', '7ED:Regeneration:regeneration').
card_rarity('regeneration'/'7ED', 'Common').
card_artist('regeneration'/'7ED', 'Adam Rex').
card_number('regeneration'/'7ED', '265').
card_flavor_text('regeneration'/'7ED', '\"Why should death always have the last word?\"\n—Llanowar druid').
card_multiverse_id('regeneration'/'7ED', '11469').

card_in_set('relentless assault', '7ED').
card_original_type('relentless assault'/'7ED', 'Sorcery').
card_original_text('relentless assault'/'7ED', 'Untap all creatures that attacked this turn. You get an additional combat phase followed by an additional main phase this turn.').
card_image_name('relentless assault'/'7ED', 'relentless assault').
card_uid('relentless assault'/'7ED', '7ED:Relentless Assault:relentless assault').
card_rarity('relentless assault'/'7ED', 'Rare').
card_artist('relentless assault'/'7ED', 'Greg & Tim Hildebrandt').
card_number('relentless assault'/'7ED', '214').
card_flavor_text('relentless assault'/'7ED', '\"Mercy? Mercy is for the playground, not the battleground.\"').
card_multiverse_id('relentless assault'/'7ED', '25684').

card_in_set('remove soul', '7ED').
card_original_type('remove soul'/'7ED', 'Instant').
card_original_text('remove soul'/'7ED', 'Counter target creature spell.').
card_image_name('remove soul'/'7ED', 'remove soul').
card_uid('remove soul'/'7ED', '7ED:Remove Soul:remove soul').
card_rarity('remove soul'/'7ED', 'Common').
card_artist('remove soul'/'7ED', 'Adam Rex').
card_number('remove soul'/'7ED', '95').
card_multiverse_id('remove soul'/'7ED', '11470').

card_in_set('reprisal', '7ED').
card_original_type('reprisal'/'7ED', 'Instant').
card_original_text('reprisal'/'7ED', 'Destroy target creature with power 4 or greater. It can\'t be regenerated.').
card_image_name('reprisal'/'7ED', 'reprisal').
card_uid('reprisal'/'7ED', '7ED:Reprisal:reprisal').
card_rarity('reprisal'/'7ED', 'Uncommon').
card_artist('reprisal'/'7ED', 'Ciruelo').
card_number('reprisal'/'7ED', '33').
card_flavor_text('reprisal'/'7ED', '\"Fear this no more!\"').
card_multiverse_id('reprisal'/'7ED', '15782').

card_in_set('reprocess', '7ED').
card_original_type('reprocess'/'7ED', 'Sorcery').
card_original_text('reprocess'/'7ED', 'Sacrifice any number of artifacts, creatures, and/or lands. Draw a card for each permanent sacrificed this way.').
card_image_name('reprocess'/'7ED', 'reprocess').
card_uid('reprocess'/'7ED', '7ED:Reprocess:reprocess').
card_rarity('reprocess'/'7ED', 'Rare').
card_artist('reprocess'/'7ED', 'John Howe').
card_number('reprocess'/'7ED', '159').
card_flavor_text('reprocess'/'7ED', 'The battle\'s end is the meal\'s beginning.').
card_multiverse_id('reprocess'/'7ED', '13101').

card_in_set('revenant', '7ED').
card_original_type('revenant'/'7ED', 'Creature — Spirit').
card_original_text('revenant'/'7ED', 'Flying\nRevenant\'s power and toughness are each equal to the number of creature cards in your graveyard.').
card_image_name('revenant'/'7ED', 'revenant').
card_uid('revenant'/'7ED', '7ED:Revenant:revenant').
card_rarity('revenant'/'7ED', 'Rare').
card_artist('revenant'/'7ED', 'Andrew Goldhawk').
card_number('revenant'/'7ED', '160').
card_flavor_text('revenant'/'7ED', 'It eats death and drinks pain.').
card_multiverse_id('revenant'/'7ED', '13088').

card_in_set('reverse damage', '7ED').
card_original_type('reverse damage'/'7ED', 'Instant').
card_original_text('reverse damage'/'7ED', 'The next time a source of your choice would deal damage to you this turn, prevent that damage. You gain that much life.').
card_image_name('reverse damage'/'7ED', 'reverse damage').
card_uid('reverse damage'/'7ED', '7ED:Reverse Damage:reverse damage').
card_rarity('reverse damage'/'7ED', 'Rare').
card_artist('reverse damage'/'7ED', 'Eric Peterson').
card_number('reverse damage'/'7ED', '34').
card_flavor_text('reverse damage'/'7ED', '\"Weapons usually bring death. Today, mine will bring life.\"').
card_multiverse_id('reverse damage'/'7ED', '15799').

card_in_set('rod of ruin', '7ED').
card_original_type('rod of ruin'/'7ED', 'Artifact').
card_original_text('rod of ruin'/'7ED', '{3}, {T}: Rod of Ruin deals 1 damage to target creature or player.').
card_image_name('rod of ruin'/'7ED', 'rod of ruin').
card_uid('rod of ruin'/'7ED', '7ED:Rod of Ruin:rod of ruin').
card_rarity('rod of ruin'/'7ED', 'Uncommon').
card_artist('rod of ruin'/'7ED', 'Ciruelo').
card_number('rod of ruin'/'7ED', '314').
card_flavor_text('rod of ruin'/'7ED', 'The rod is a relic from ancient times . . . cruel, vicious, mean-spirited times.').
card_multiverse_id('rod of ruin'/'7ED', '11474').

card_in_set('rolling stones', '7ED').
card_original_type('rolling stones'/'7ED', 'Enchantment').
card_original_text('rolling stones'/'7ED', 'Walls may attack as though they weren\'t Walls.').
card_image_name('rolling stones'/'7ED', 'rolling stones').
card_uid('rolling stones'/'7ED', '7ED:Rolling Stones:rolling stones').
card_rarity('rolling stones'/'7ED', 'Rare').
card_artist('rolling stones'/'7ED', 'Don Hazeltine').
card_number('rolling stones'/'7ED', '35').
card_flavor_text('rolling stones'/'7ED', '\"We spent three weeks trying to knock that wall down. Now it\'s knocking us down.\"\n—Onean sergeant').
card_multiverse_id('rolling stones'/'7ED', '25552').

card_in_set('rowen', '7ED').
card_original_type('rowen'/'7ED', 'Enchantment').
card_original_text('rowen'/'7ED', 'Reveal the first card you draw each turn. Whenever you reveal a basic land card this way, draw a card.').
card_image_name('rowen'/'7ED', 'rowen').
card_uid('rowen'/'7ED', '7ED:Rowen:rowen').
card_rarity('rowen'/'7ED', 'Rare').
card_artist('rowen'/'7ED', 'Franz Vohwinkel').
card_number('rowen'/'7ED', '266').
card_flavor_text('rowen'/'7ED', 'Those who understand the forest are free to harvest its bounty.').
card_multiverse_id('rowen'/'7ED', '25591').

card_in_set('sabretooth tiger', '7ED').
card_original_type('sabretooth tiger'/'7ED', 'Creature — Cat').
card_original_text('sabretooth tiger'/'7ED', 'First strike').
card_image_name('sabretooth tiger'/'7ED', 'sabretooth tiger').
card_uid('sabretooth tiger'/'7ED', '7ED:Sabretooth Tiger:sabretooth tiger').
card_rarity('sabretooth tiger'/'7ED', 'Common').
card_artist('sabretooth tiger'/'7ED', 'Monte Michael Moore').
card_number('sabretooth tiger'/'7ED', '215').
card_flavor_text('sabretooth tiger'/'7ED', '\"Here, kitty kitty. Here, kitty kitty. Aaaaugh!\"').
card_multiverse_id('sabretooth tiger'/'7ED', '15807').

card_in_set('sacred ground', '7ED').
card_original_type('sacred ground'/'7ED', 'Enchantment').
card_original_text('sacred ground'/'7ED', 'Whenever a spell or ability an opponent controls puts a land into your graveyard from play, return that land to play.').
card_image_name('sacred ground'/'7ED', 'sacred ground').
card_uid('sacred ground'/'7ED', '7ED:Sacred Ground:sacred ground').
card_rarity('sacred ground'/'7ED', 'Rare').
card_artist('sacred ground'/'7ED', 'Gary Ruddell').
card_number('sacred ground'/'7ED', '36').
card_flavor_text('sacred ground'/'7ED', 'The land will never betray those who gave their lives for it.').
card_multiverse_id('sacred ground'/'7ED', '25551').

card_in_set('sacred nectar', '7ED').
card_original_type('sacred nectar'/'7ED', 'Sorcery').
card_original_text('sacred nectar'/'7ED', 'You gain 4 life.').
card_image_name('sacred nectar'/'7ED', 'sacred nectar').
card_uid('sacred nectar'/'7ED', '7ED:Sacred Nectar:sacred nectar').
card_rarity('sacred nectar'/'7ED', 'Common').
card_artist('sacred nectar'/'7ED', 'Dana Knutson').
card_number('sacred nectar'/'7ED', '37').
card_flavor_text('sacred nectar'/'7ED', '\"For he on honey-dew hath fed,\nAnd drunk the milk of Paradise.\"\n—Samuel Taylor Coleridge,\n\"Kubla Khan\"').
card_multiverse_id('sacred nectar'/'7ED', '30168').

card_in_set('sage owl', '7ED').
card_original_type('sage owl'/'7ED', 'Creature — Bird').
card_original_text('sage owl'/'7ED', 'Flying\nWhen Sage Owl comes into play, look at the top four cards of your library, then put them back in any order.').
card_image_name('sage owl'/'7ED', 'sage owl').
card_uid('sage owl'/'7ED', '7ED:Sage Owl:sage owl').
card_rarity('sage owl'/'7ED', 'Common').
card_artist('sage owl'/'7ED', 'Mark Brill').
card_number('sage owl'/'7ED', '96').
card_multiverse_id('sage owl'/'7ED', '15794').

card_in_set('samite healer', '7ED').
card_original_type('samite healer'/'7ED', 'Creature — Cleric').
card_original_text('samite healer'/'7ED', '{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.').
card_image_name('samite healer'/'7ED', 'samite healer').
card_uid('samite healer'/'7ED', '7ED:Samite Healer:samite healer').
card_rarity('samite healer'/'7ED', 'Common').
card_artist('samite healer'/'7ED', 'Anson Maddocks').
card_number('samite healer'/'7ED', '38').
card_flavor_text('samite healer'/'7ED', 'Why does a Samite heal? You might as well ask why a bird flies.').
card_multiverse_id('samite healer'/'7ED', '11478').

card_in_set('sanctimony', '7ED').
card_original_type('sanctimony'/'7ED', 'Enchantment').
card_original_text('sanctimony'/'7ED', 'Whenever an opponent taps a mountain for mana, you may gain 1 life.').
card_image_name('sanctimony'/'7ED', 'sanctimony').
card_uid('sanctimony'/'7ED', '7ED:Sanctimony:sanctimony').
card_rarity('sanctimony'/'7ED', 'Uncommon').
card_artist('sanctimony'/'7ED', 'Patrick Faricy').
card_number('sanctimony'/'7ED', '39').
card_flavor_text('sanctimony'/'7ED', '\"The peasants labor for my profit. I approve.\"').
card_multiverse_id('sanctimony'/'7ED', '26842').

card_in_set('scathe zombies', '7ED').
card_original_type('scathe zombies'/'7ED', 'Creature — Zombie').
card_original_text('scathe zombies'/'7ED', '').
card_image_name('scathe zombies'/'7ED', 'scathe zombies').
card_uid('scathe zombies'/'7ED', '7ED:Scathe Zombies:scathe zombies').
card_rarity('scathe zombies'/'7ED', 'Common').
card_artist('scathe zombies'/'7ED', 'Kev Walker').
card_number('scathe zombies'/'7ED', '161').
card_flavor_text('scathe zombies'/'7ED', '\"They groaned, they stirred, they all uprose,\nNor spake, nor moved their eyes;\nIt had been strange, even in a dream,\nTo have seen those dead men rise.\"\n—Samuel Taylor Coleridge,\n\"The Rime of the Ancient Mariner\"').
card_multiverse_id('scathe zombies'/'7ED', '27588').

card_in_set('scavenger folk', '7ED').
card_original_type('scavenger folk'/'7ED', 'Creature — Scavenger').
card_original_text('scavenger folk'/'7ED', '{G}, {T}, Sacrifice Scavenger Folk: Destroy target artifact.').
card_image_name('scavenger folk'/'7ED', 'scavenger folk').
card_uid('scavenger folk'/'7ED', '7ED:Scavenger Folk:scavenger folk').
card_rarity('scavenger folk'/'7ED', 'Uncommon').
card_artist('scavenger folk'/'7ED', 'Matt Cavotta').
card_number('scavenger folk'/'7ED', '267').
card_flavor_text('scavenger folk'/'7ED', '\"One piece? Now there\'s a hundred!\"').
card_multiverse_id('scavenger folk'/'7ED', '25590').

card_in_set('sea monster', '7ED').
card_original_type('sea monster'/'7ED', 'Creature — Serpent').
card_original_text('sea monster'/'7ED', 'Sea Monster can\'t attack unless defending player controls an island.').
card_image_name('sea monster'/'7ED', 'sea monster').
card_uid('sea monster'/'7ED', '7ED:Sea Monster:sea monster').
card_rarity('sea monster'/'7ED', 'Common').
card_artist('sea monster'/'7ED', 'John Howe').
card_number('sea monster'/'7ED', '97').
card_flavor_text('sea monster'/'7ED', '\"Abandon ship!\"').
card_multiverse_id('sea monster'/'7ED', '13036').

card_in_set('seasoned marshal', '7ED').
card_original_type('seasoned marshal'/'7ED', 'Creature — Soldier').
card_original_text('seasoned marshal'/'7ED', 'Whenever Seasoned Marshal attacks, you may tap target creature.').
card_image_name('seasoned marshal'/'7ED', 'seasoned marshal').
card_uid('seasoned marshal'/'7ED', '7ED:Seasoned Marshal:seasoned marshal').
card_rarity('seasoned marshal'/'7ED', 'Uncommon').
card_artist('seasoned marshal'/'7ED', 'Edward P. Beard, Jr.').
card_number('seasoned marshal'/'7ED', '40').
card_flavor_text('seasoned marshal'/'7ED', 'He reads the battlefield like a map, always seeking the shortest route to victory.').
card_multiverse_id('seasoned marshal'/'7ED', '13130').

card_in_set('seeker of skybreak', '7ED').
card_original_type('seeker of skybreak'/'7ED', 'Creature — Elf').
card_original_text('seeker of skybreak'/'7ED', '{T}: Untap target creature.').
card_image_name('seeker of skybreak'/'7ED', 'seeker of skybreak').
card_uid('seeker of skybreak'/'7ED', '7ED:Seeker of Skybreak:seeker of skybreak').
card_rarity('seeker of skybreak'/'7ED', 'Common').
card_artist('seeker of skybreak'/'7ED', 'Alex Horley-Orlandelli').
card_number('seeker of skybreak'/'7ED', '268').
card_flavor_text('seeker of skybreak'/'7ED', 'Seekers spend their lives protecting a home they rarely visit.').
card_multiverse_id('seeker of skybreak'/'7ED', '13046').

card_in_set('seismic assault', '7ED').
card_original_type('seismic assault'/'7ED', 'Enchantment').
card_original_text('seismic assault'/'7ED', 'Discard a land card from your hand: Seismic Assault deals 2 damage to target creature or player.').
card_image_name('seismic assault'/'7ED', 'seismic assault').
card_uid('seismic assault'/'7ED', '7ED:Seismic Assault:seismic assault').
card_rarity('seismic assault'/'7ED', 'Rare').
card_artist('seismic assault'/'7ED', 'Greg Staples').
card_number('seismic assault'/'7ED', '216').
card_flavor_text('seismic assault'/'7ED', 'Earth arms itself with fire.').
card_multiverse_id('seismic assault'/'7ED', '11361').

card_in_set('serpent warrior', '7ED').
card_original_type('serpent warrior'/'7ED', 'Creature — Soldier').
card_original_text('serpent warrior'/'7ED', 'When Serpent Warrior comes into play, you lose 3 life.').
card_image_name('serpent warrior'/'7ED', 'serpent warrior').
card_uid('serpent warrior'/'7ED', '7ED:Serpent Warrior:serpent warrior').
card_rarity('serpent warrior'/'7ED', 'Common').
card_artist('serpent warrior'/'7ED', 'Eric Peterson').
card_number('serpent warrior'/'7ED', '162').
card_flavor_text('serpent warrior'/'7ED', 'They\'re the fiercest of all the Western Paladin\'s soldiers. They\'re also the most fiercely loyal—but that loyalty comes at a price.').
card_multiverse_id('serpent warrior'/'7ED', '25627').

card_in_set('serra advocate', '7ED').
card_original_type('serra advocate'/'7ED', 'Creature — Angel').
card_original_text('serra advocate'/'7ED', 'Flying\n{T}: Target attacking or blocking creature gets +2/+2 until end of turn.').
card_image_name('serra advocate'/'7ED', 'serra advocate').
card_uid('serra advocate'/'7ED', '7ED:Serra Advocate:serra advocate').
card_rarity('serra advocate'/'7ED', 'Uncommon').
card_artist('serra advocate'/'7ED', 'Matthew D. Wilson').
card_number('serra advocate'/'7ED', '41').
card_flavor_text('serra advocate'/'7ED', 'An angel\'s blessing prepares a soldier for war better than a thousand military drills.').
card_multiverse_id('serra advocate'/'7ED', '13162').

card_in_set('serra angel', '7ED').
card_original_type('serra angel'/'7ED', 'Creature — Angel').
card_original_text('serra angel'/'7ED', 'Flying\nAttacking doesn\'t cause Serra Angel to tap.').
card_image_name('serra angel'/'7ED', 'serra angel').
card_uid('serra angel'/'7ED', '7ED:Serra Angel:serra angel').
card_rarity('serra angel'/'7ED', 'Rare').
card_artist('serra angel'/'7ED', 'Mark Zug').
card_number('serra angel'/'7ED', '42').
card_flavor_text('serra angel'/'7ED', 'Her sword sings more beautifully than any choir.').
card_multiverse_id('serra angel'/'7ED', '25555').

card_in_set('serra\'s embrace', '7ED').
card_original_type('serra\'s embrace'/'7ED', 'Enchant Creature').
card_original_text('serra\'s embrace'/'7ED', 'Enchanted creature gets +2/+2, has flying, and attacking doesn\'t cause it to tap.').
card_image_name('serra\'s embrace'/'7ED', 'serra\'s embrace').
card_uid('serra\'s embrace'/'7ED', '7ED:Serra\'s Embrace:serra\'s embrace').
card_rarity('serra\'s embrace'/'7ED', 'Uncommon').
card_artist('serra\'s embrace'/'7ED', 'Ciruelo').
card_number('serra\'s embrace'/'7ED', '43').
card_flavor_text('serra\'s embrace'/'7ED', '\"With one breath, the newborn angel took to the air . . . and the world sighed.\"\n—The Book of Serra').
card_multiverse_id('serra\'s embrace'/'7ED', '25658').

card_in_set('shanodin dryads', '7ED').
card_original_type('shanodin dryads'/'7ED', 'Creature — Dryad').
card_original_text('shanodin dryads'/'7ED', 'Forestwalk (This creature is unblockable as long as defending player controls a forest.)').
card_image_name('shanodin dryads'/'7ED', 'shanodin dryads').
card_uid('shanodin dryads'/'7ED', '7ED:Shanodin Dryads:shanodin dryads').
card_rarity('shanodin dryads'/'7ED', 'Common').
card_artist('shanodin dryads'/'7ED', 'Eric Peterson').
card_number('shanodin dryads'/'7ED', '269').
card_flavor_text('shanodin dryads'/'7ED', 'Where dryads dance, seeds sprout and flowers bloom.').
card_multiverse_id('shanodin dryads'/'7ED', '11494').

card_in_set('shatter', '7ED').
card_original_type('shatter'/'7ED', 'Instant').
card_original_text('shatter'/'7ED', 'Destroy target artifact.').
card_image_name('shatter'/'7ED', 'shatter').
card_uid('shatter'/'7ED', '7ED:Shatter:shatter').
card_rarity('shatter'/'7ED', 'Common').
card_artist('shatter'/'7ED', 'Michael Koelsch').
card_number('shatter'/'7ED', '217').
card_multiverse_id('shatter'/'7ED', '11496').

card_in_set('shield wall', '7ED').
card_original_type('shield wall'/'7ED', 'Instant').
card_original_text('shield wall'/'7ED', 'Creatures you control get +0/+2 until end of turn.').
card_image_name('shield wall'/'7ED', 'shield wall').
card_uid('shield wall'/'7ED', '7ED:Shield Wall:shield wall').
card_rarity('shield wall'/'7ED', 'Common').
card_artist('shield wall'/'7ED', 'Adam Rex').
card_number('shield wall'/'7ED', '44').
card_flavor_text('shield wall'/'7ED', '\"If not for the wizard, we\'d all be dead now. \'Course, it was him that started all this in the first place.\"\n—Onean veteran').
card_multiverse_id('shield wall'/'7ED', '11499').

card_in_set('shivan dragon', '7ED').
card_original_type('shivan dragon'/'7ED', 'Creature — Dragon').
card_original_text('shivan dragon'/'7ED', 'Flying\n{R} Shivan Dragon gets +1/+0 until end of turn.').
card_image_name('shivan dragon'/'7ED', 'shivan dragon').
card_uid('shivan dragon'/'7ED', '7ED:Shivan Dragon:shivan dragon').
card_rarity('shivan dragon'/'7ED', 'Rare').
card_artist('shivan dragon'/'7ED', 'Donato Giancola').
card_number('shivan dragon'/'7ED', '218').
card_flavor_text('shivan dragon'/'7ED', 'The undisputed master of the mountains of Shiv.').
card_multiverse_id('shivan dragon'/'7ED', '25688').

card_in_set('shock', '7ED').
card_original_type('shock'/'7ED', 'Instant').
card_original_text('shock'/'7ED', 'Shock deals 2 damage to target creature or player.').
card_image_name('shock'/'7ED', 'shock').
card_uid('shock'/'7ED', '7ED:Shock:shock').
card_rarity('shock'/'7ED', 'Common').
card_artist('shock'/'7ED', 'Mike Sass').
card_number('shock'/'7ED', '219').
card_multiverse_id('shock'/'7ED', '13074').

card_in_set('sisay\'s ring', '7ED').
card_original_type('sisay\'s ring'/'7ED', 'Artifact').
card_original_text('sisay\'s ring'/'7ED', '{T}: Add two colorless mana to your mana pool.').
card_image_name('sisay\'s ring'/'7ED', 'sisay\'s ring').
card_uid('sisay\'s ring'/'7ED', '7ED:Sisay\'s Ring:sisay\'s ring').
card_rarity('sisay\'s ring'/'7ED', 'Uncommon').
card_artist('sisay\'s ring'/'7ED', 'David Martin').
card_number('sisay\'s ring'/'7ED', '315').
card_flavor_text('sisay\'s ring'/'7ED', '\"I\'m not one for pretty things,\" said Sisay. \"Unless they\'re pretty useful as well.\"').
card_multiverse_id('sisay\'s ring'/'7ED', '15846').

card_in_set('sky diamond', '7ED').
card_original_type('sky diamond'/'7ED', 'Artifact').
card_original_text('sky diamond'/'7ED', 'Sky Diamond comes into play tapped.\n{T}: Add {U} to your mana pool.').
card_image_name('sky diamond'/'7ED', 'sky diamond').
card_uid('sky diamond'/'7ED', '7ED:Sky Diamond:sky diamond').
card_rarity('sky diamond'/'7ED', 'Uncommon').
card_artist('sky diamond'/'7ED', 'Tony Szczudlo').
card_number('sky diamond'/'7ED', '316').
card_multiverse_id('sky diamond'/'7ED', '15851').

card_in_set('skyshroud falcon', '7ED').
card_original_type('skyshroud falcon'/'7ED', 'Creature — Bird').
card_original_text('skyshroud falcon'/'7ED', 'Flying\nAttacking doesn\'t cause Skyshroud Falcon to tap.').
card_image_name('skyshroud falcon'/'7ED', 'skyshroud falcon').
card_uid('skyshroud falcon'/'7ED', '7ED:Skyshroud Falcon:skyshroud falcon').
card_rarity('skyshroud falcon'/'7ED', 'Common').
card_artist('skyshroud falcon'/'7ED', 'Anson Maddocks').
card_number('skyshroud falcon'/'7ED', '45').
card_flavor_text('skyshroud falcon'/'7ED', 'The earliest mystics believed that the falcon slit the sky to let the light break through.').
card_multiverse_id('skyshroud falcon'/'7ED', '15781').

card_in_set('sleight of hand', '7ED').
card_original_type('sleight of hand'/'7ED', 'Sorcery').
card_original_text('sleight of hand'/'7ED', 'Look at the top two cards of your library. Put one of them into your hand and the other on the bottom of your library.').
card_image_name('sleight of hand'/'7ED', 'sleight of hand').
card_uid('sleight of hand'/'7ED', '7ED:Sleight of Hand:sleight of hand').
card_rarity('sleight of hand'/'7ED', 'Common').
card_artist('sleight of hand'/'7ED', 'Christopher Moeller').
card_number('sleight of hand'/'7ED', '98').
card_flavor_text('sleight of hand'/'7ED', '\"Most people believe magic is only a trick. Why change their minds?\"').
card_multiverse_id('sleight of hand'/'7ED', '25557').

card_in_set('soul feast', '7ED').
card_original_type('soul feast'/'7ED', 'Sorcery').
card_original_text('soul feast'/'7ED', 'Target player loses 4 life and you gain 4 life.').
card_image_name('soul feast'/'7ED', 'soul feast').
card_uid('soul feast'/'7ED', '7ED:Soul Feast:soul feast').
card_rarity('soul feast'/'7ED', 'Uncommon').
card_artist('soul feast'/'7ED', 'Adam Rex').
card_number('soul feast'/'7ED', '163').
card_flavor_text('soul feast'/'7ED', 'You are who you eat.').
card_multiverse_id('soul feast'/'7ED', '15818').

card_in_set('soul net', '7ED').
card_original_type('soul net'/'7ED', 'Artifact').
card_original_text('soul net'/'7ED', 'Whenever a creature is put into a graveyard from play, you may pay {1}. If you do, you gain 1 life.').
card_image_name('soul net'/'7ED', 'soul net').
card_uid('soul net'/'7ED', '7ED:Soul Net:soul net').
card_rarity('soul net'/'7ED', 'Uncommon').
card_artist('soul net'/'7ED', 'Ron Spencer').
card_number('soul net'/'7ED', '317').
card_multiverse_id('soul net'/'7ED', '25651').

card_in_set('southern paladin', '7ED').
card_original_type('southern paladin'/'7ED', 'Creature — Knight').
card_original_text('southern paladin'/'7ED', '{W}{W}, {T}: Destroy target red permanent.').
card_image_name('southern paladin'/'7ED', 'southern paladin').
card_uid('southern paladin'/'7ED', '7ED:Southern Paladin:southern paladin').
card_rarity('southern paladin'/'7ED', 'Rare').
card_artist('southern paladin'/'7ED', 'Greg Staples').
card_number('southern paladin'/'7ED', '46').
card_flavor_text('southern paladin'/'7ED', '\"How can anyone fight so much yet enjoy it so little?\"\n—The Western Paladin').
card_multiverse_id('southern paladin'/'7ED', '13018').

card_in_set('spellbook', '7ED').
card_original_type('spellbook'/'7ED', 'Artifact').
card_original_text('spellbook'/'7ED', 'You have no maximum hand size.').
card_image_name('spellbook'/'7ED', 'spellbook').
card_uid('spellbook'/'7ED', '7ED:Spellbook:spellbook').
card_rarity('spellbook'/'7ED', 'Uncommon').
card_artist('spellbook'/'7ED', 'Andrew Goldhawk').
card_number('spellbook'/'7ED', '318').
card_flavor_text('spellbook'/'7ED', 'Knowledge knows no bounds.').
card_multiverse_id('spellbook'/'7ED', '25654').

card_in_set('spined wurm', '7ED').
card_original_type('spined wurm'/'7ED', 'Creature — Wurm').
card_original_text('spined wurm'/'7ED', '').
card_image_name('spined wurm'/'7ED', 'spined wurm').
card_uid('spined wurm'/'7ED', '7ED:Spined Wurm:spined wurm').
card_rarity('spined wurm'/'7ED', 'Common').
card_artist('spined wurm'/'7ED', 'Carl Critchlow').
card_number('spined wurm'/'7ED', '270').
card_flavor_text('spined wurm'/'7ED', '\"I wouldn\'t stand in front of that wurm, son. \'Course, I wouldn\'t stand behind it neither. In fact, standing anywhere near that wurm\'s not much of a plan. Running, now that\'s a plan\"\n—Wandering mage').
card_multiverse_id('spined wurm'/'7ED', '13096').

card_in_set('spineless thug', '7ED').
card_original_type('spineless thug'/'7ED', 'Creature — Mercenary').
card_original_text('spineless thug'/'7ED', 'Spineless Thug can\'t block.').
card_image_name('spineless thug'/'7ED', 'spineless thug').
card_uid('spineless thug'/'7ED', '7ED:Spineless Thug:spineless thug').
card_rarity('spineless thug'/'7ED', 'Common').
card_artist('spineless thug'/'7ED', 'Alan Pollack').
card_number('spineless thug'/'7ED', '164').
card_flavor_text('spineless thug'/'7ED', 'What it lacks in backbone, it makes up for in cruelty.').
card_multiverse_id('spineless thug'/'7ED', '15883').

card_in_set('spirit link', '7ED').
card_original_type('spirit link'/'7ED', 'Enchant Creature').
card_original_text('spirit link'/'7ED', 'Whenever enchanted creature deals damage, you gain that much life.').
card_image_name('spirit link'/'7ED', 'spirit link').
card_uid('spirit link'/'7ED', '7ED:Spirit Link:spirit link').
card_rarity('spirit link'/'7ED', 'Uncommon').
card_artist('spirit link'/'7ED', 'Daren Bader').
card_number('spirit link'/'7ED', '47').
card_flavor_text('spirit link'/'7ED', 'The noble paladins draw strength from the courage of others.').
card_multiverse_id('spirit link'/'7ED', '25547').

card_in_set('spitting earth', '7ED').
card_original_type('spitting earth'/'7ED', 'Sorcery').
card_original_text('spitting earth'/'7ED', 'Spitting Earth deals damage equal to the number of mountains you control to target creature.').
card_image_name('spitting earth'/'7ED', 'spitting earth').
card_uid('spitting earth'/'7ED', '7ED:Spitting Earth:spitting earth').
card_rarity('spitting earth'/'7ED', 'Common').
card_artist('spitting earth'/'7ED', 'Michael Koelsch').
card_number('spitting earth'/'7ED', '220').
card_flavor_text('spitting earth'/'7ED', '\"We were ready for an attack from the front, sides, or back—but the mage hit us from below.\"\n—Veteran soldier').
card_multiverse_id('spitting earth'/'7ED', '12972').

card_in_set('squall', '7ED').
card_original_type('squall'/'7ED', 'Sorcery').
card_original_text('squall'/'7ED', 'Squall deals 2 damage to each creature with flying.').
card_image_name('squall'/'7ED', 'squall').
card_uid('squall'/'7ED', '7ED:Squall:squall').
card_rarity('squall'/'7ED', 'Common').
card_artist('squall'/'7ED', 'Greg Staples').
card_number('squall'/'7ED', '271').
card_flavor_text('squall'/'7ED', '\"May the winds blow till they have wakened death. . . .\"\n—William Shakespeare,\nOthello').
card_multiverse_id('squall'/'7ED', '25586').

card_in_set('standing troops', '7ED').
card_original_type('standing troops'/'7ED', 'Creature — Soldier').
card_original_text('standing troops'/'7ED', 'Attacking doesn\'t cause Standing Troops to tap.').
card_image_name('standing troops'/'7ED', 'standing troops').
card_uid('standing troops'/'7ED', '7ED:Standing Troops:standing troops').
card_rarity('standing troops'/'7ED', 'Common').
card_artist('standing troops'/'7ED', 'Gary Ruddell').
card_number('standing troops'/'7ED', '48').
card_flavor_text('standing troops'/'7ED', 'They fight so closely together a mouse couldn\'t slip past their guard.').
card_multiverse_id('standing troops'/'7ED', '13139').

card_in_set('starlight', '7ED').
card_original_type('starlight'/'7ED', 'Sorcery').
card_original_text('starlight'/'7ED', 'You gain 3 life for each black creature target opponent controls.').
card_image_name('starlight'/'7ED', 'starlight').
card_uid('starlight'/'7ED', '7ED:Starlight:starlight').
card_rarity('starlight'/'7ED', 'Uncommon').
card_artist('starlight'/'7ED', 'Brian Despain').
card_number('starlight'/'7ED', '49').
card_flavor_text('starlight'/'7ED', '\"Even in the darkest night\nStars and angels still shine bright.\"\n—Onean children\'s rhyme').
card_multiverse_id('starlight'/'7ED', '13065').

card_in_set('static orb', '7ED').
card_original_type('static orb'/'7ED', 'Artifact').
card_original_text('static orb'/'7ED', 'If Static Orb is untapped, players can\'t untap more than two permanents during their untap steps.').
card_image_name('static orb'/'7ED', 'static orb').
card_uid('static orb'/'7ED', '7ED:Static Orb:static orb').
card_rarity('static orb'/'7ED', 'Rare').
card_artist('static orb'/'7ED', 'Terese Nielsen').
card_number('static orb'/'7ED', '319').
card_flavor_text('static orb'/'7ED', 'The warriors fought against the paralyzing waves until even their thoughts froze in place.').
card_multiverse_id('static orb'/'7ED', '15862').

card_in_set('staunch defenders', '7ED').
card_original_type('staunch defenders'/'7ED', 'Creature — Soldier').
card_original_text('staunch defenders'/'7ED', 'When Staunch Defenders comes into play, you gain 4 life.').
card_image_name('staunch defenders'/'7ED', 'staunch defenders').
card_uid('staunch defenders'/'7ED', '7ED:Staunch Defenders:staunch defenders').
card_rarity('staunch defenders'/'7ED', 'Uncommon').
card_artist('staunch defenders'/'7ED', 'Tristan Elwell').
card_number('staunch defenders'/'7ED', '50').
card_flavor_text('staunch defenders'/'7ED', '\"The key to winning any fight is simply staying alive.\"\n—The Southern Paladin').
card_multiverse_id('staunch defenders'/'7ED', '13068').

card_in_set('steal artifact', '7ED').
card_original_type('steal artifact'/'7ED', 'Enchant Artifact').
card_original_text('steal artifact'/'7ED', 'You control enchanted artifact.').
card_image_name('steal artifact'/'7ED', 'steal artifact').
card_uid('steal artifact'/'7ED', '7ED:Steal Artifact:steal artifact').
card_rarity('steal artifact'/'7ED', 'Uncommon').
card_artist('steal artifact'/'7ED', 'Peter Bollinger').
card_number('steal artifact'/'7ED', '99').
card_multiverse_id('steal artifact'/'7ED', '15801').

card_in_set('stone rain', '7ED').
card_original_type('stone rain'/'7ED', 'Sorcery').
card_original_text('stone rain'/'7ED', 'Destroy target land.').
card_image_name('stone rain'/'7ED', 'stone rain').
card_uid('stone rain'/'7ED', '7ED:Stone Rain:stone rain').
card_rarity('stone rain'/'7ED', 'Common').
card_artist('stone rain'/'7ED', 'Tony Szczudlo').
card_number('stone rain'/'7ED', '221').
card_multiverse_id('stone rain'/'7ED', '11515').

card_in_set('storm cauldron', '7ED').
card_original_type('storm cauldron'/'7ED', 'Artifact').
card_original_text('storm cauldron'/'7ED', 'Each player may play an additional land during each of his or her turns.\nWhenever a land is tapped for mana, return it to its owner\'s hand.').
card_image_name('storm cauldron'/'7ED', 'storm cauldron').
card_uid('storm cauldron'/'7ED', '7ED:Storm Cauldron:storm cauldron').
card_rarity('storm cauldron'/'7ED', 'Rare').
card_artist('storm cauldron'/'7ED', 'Doug Chaffee').
card_number('storm cauldron'/'7ED', '320').
card_multiverse_id('storm cauldron'/'7ED', '25663').

card_in_set('storm crow', '7ED').
card_original_type('storm crow'/'7ED', 'Creature — Bird').
card_original_text('storm crow'/'7ED', 'Flying').
card_image_name('storm crow'/'7ED', 'storm crow').
card_uid('storm crow'/'7ED', '7ED:Storm Crow:storm crow').
card_rarity('storm crow'/'7ED', 'Common').
card_artist('storm crow'/'7ED', 'John Matson').
card_number('storm crow'/'7ED', '100').
card_flavor_text('storm crow'/'7ED', 'Storm crow descending, winter unending.\nStorm crow departing, summer is starting.').
card_multiverse_id('storm crow'/'7ED', '13172').

card_in_set('storm shaman', '7ED').
card_original_type('storm shaman'/'7ED', 'Creature — Cleric').
card_original_text('storm shaman'/'7ED', '{R} Storm Shaman gets +1/+0 until end of turn.').
card_image_name('storm shaman'/'7ED', 'storm shaman').
card_uid('storm shaman'/'7ED', '7ED:Storm Shaman:storm shaman').
card_rarity('storm shaman'/'7ED', 'Uncommon').
card_artist('storm shaman'/'7ED', 'D. J. Cleland-Hura').
card_number('storm shaman'/'7ED', '222').
card_flavor_text('storm shaman'/'7ED', 'She speaks with the voice of the winds, and the earth trembles.').
card_multiverse_id('storm shaman'/'7ED', '15834').

card_in_set('strands of night', '7ED').
card_original_type('strands of night'/'7ED', 'Enchantment').
card_original_text('strands of night'/'7ED', '{B}{B}, Pay 2 life, Sacrifice a swamp: Return target creature card from your graveyard to play.').
card_image_name('strands of night'/'7ED', 'strands of night').
card_uid('strands of night'/'7ED', '7ED:Strands of Night:strands of night').
card_rarity('strands of night'/'7ED', 'Uncommon').
card_artist('strands of night'/'7ED', 'Glen Angus').
card_number('strands of night'/'7ED', '165').
card_flavor_text('strands of night'/'7ED', 'When the strands call, only the dead answer.').
card_multiverse_id('strands of night'/'7ED', '13175').

card_in_set('stream of life', '7ED').
card_original_type('stream of life'/'7ED', 'Sorcery').
card_original_text('stream of life'/'7ED', 'Target player gains X life.').
card_image_name('stream of life'/'7ED', 'stream of life').
card_uid('stream of life'/'7ED', '7ED:Stream of Life:stream of life').
card_rarity('stream of life'/'7ED', 'Common').
card_artist('stream of life'/'7ED', 'Andrew Goldhawk').
card_number('stream of life'/'7ED', '272').
card_multiverse_id('stream of life'/'7ED', '11517').

card_in_set('stronghold assassin', '7ED').
card_original_type('stronghold assassin'/'7ED', 'Creature — Assassin').
card_original_text('stronghold assassin'/'7ED', '{T}, Sacrifice a creature: Destroy target nonblack creature.').
card_image_name('stronghold assassin'/'7ED', 'stronghold assassin').
card_uid('stronghold assassin'/'7ED', '7ED:Stronghold Assassin:stronghold assassin').
card_rarity('stronghold assassin'/'7ED', 'Rare').
card_artist('stronghold assassin'/'7ED', 'Ron Walotsky').
card_number('stronghold assassin'/'7ED', '166').
card_flavor_text('stronghold assassin'/'7ED', 'The assassin sees only throats and hears only heartbeats.').
card_multiverse_id('stronghold assassin'/'7ED', '25645').

card_in_set('sudden impact', '7ED').
card_original_type('sudden impact'/'7ED', 'Instant').
card_original_text('sudden impact'/'7ED', 'Sudden Impact deals damage equal to the number of cards in target player\'s hand to that player.').
card_image_name('sudden impact'/'7ED', 'sudden impact').
card_uid('sudden impact'/'7ED', '7ED:Sudden Impact:sudden impact').
card_rarity('sudden impact'/'7ED', 'Uncommon').
card_artist('sudden impact'/'7ED', 'Greg & Tim Hildebrandt').
card_number('sudden impact'/'7ED', '223').
card_flavor_text('sudden impact'/'7ED', 'Caught red-handed.').
card_multiverse_id('sudden impact'/'7ED', '13059').

card_in_set('sulfurous springs', '7ED').
card_original_type('sulfurous springs'/'7ED', 'Land').
card_original_text('sulfurous springs'/'7ED', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {B} or {R} to your mana pool. Sulfurous Springs deals 1 damage to you.').
card_image_name('sulfurous springs'/'7ED', 'sulfurous springs').
card_uid('sulfurous springs'/'7ED', '7ED:Sulfurous Springs:sulfurous springs').
card_rarity('sulfurous springs'/'7ED', 'Rare').
card_artist('sulfurous springs'/'7ED', 'Rob Alexander').
card_number('sulfurous springs'/'7ED', '345').
card_multiverse_id('sulfurous springs'/'7ED', '11519').

card_in_set('sunweb', '7ED').
card_original_type('sunweb'/'7ED', 'Creature — Wall').
card_original_text('sunweb'/'7ED', '(Walls can\'t attack.)\nFlying\nSunweb can\'t block creatures with power 2 or less.').
card_image_name('sunweb'/'7ED', 'sunweb').
card_uid('sunweb'/'7ED', '7ED:Sunweb:sunweb').
card_rarity('sunweb'/'7ED', 'Rare').
card_artist('sunweb'/'7ED', 'Greg Staples').
card_number('sunweb'/'7ED', '51').
card_flavor_text('sunweb'/'7ED', 'Sometimes the wings of fallen dragons refuse to give up the sky.').
card_multiverse_id('sunweb'/'7ED', '12979').

card_in_set('sustainer of the realm', '7ED').
card_original_type('sustainer of the realm'/'7ED', 'Creature — Angel').
card_original_text('sustainer of the realm'/'7ED', 'Flying\nWhenever Sustainer of the Realm blocks, it gets +0/+2 until end of turn.').
card_image_name('sustainer of the realm'/'7ED', 'sustainer of the realm').
card_uid('sustainer of the realm'/'7ED', '7ED:Sustainer of the Realm:sustainer of the realm').
card_rarity('sustainer of the realm'/'7ED', 'Uncommon').
card_artist('sustainer of the realm'/'7ED', 'Mark Zug').
card_number('sustainer of the realm'/'7ED', '52').
card_flavor_text('sustainer of the realm'/'7ED', 'You may break her shield, but you\'ll never break her spirit.').
card_multiverse_id('sustainer of the realm'/'7ED', '25550').

card_in_set('swamp', '7ED').
card_original_type('swamp'/'7ED', 'Land').
card_original_text('swamp'/'7ED', 'B').
card_image_name('swamp'/'7ED', 'swamp1').
card_uid('swamp'/'7ED', '7ED:Swamp:swamp1').
card_rarity('swamp'/'7ED', 'Basic Land').
card_artist('swamp'/'7ED', 'D. J. Cleland-Hura').
card_number('swamp'/'7ED', '346').
card_multiverse_id('swamp'/'7ED', '11521').

card_in_set('swamp', '7ED').
card_original_type('swamp'/'7ED', 'Land').
card_original_text('swamp'/'7ED', 'B').
card_image_name('swamp'/'7ED', 'swamp2').
card_uid('swamp'/'7ED', '7ED:Swamp:swamp2').
card_rarity('swamp'/'7ED', 'Basic Land').
card_artist('swamp'/'7ED', 'Rob Alexander').
card_number('swamp'/'7ED', '347').
card_multiverse_id('swamp'/'7ED', '11522').

card_in_set('swamp', '7ED').
card_original_type('swamp'/'7ED', 'Land').
card_original_text('swamp'/'7ED', 'B').
card_image_name('swamp'/'7ED', 'swamp3').
card_uid('swamp'/'7ED', '7ED:Swamp:swamp3').
card_rarity('swamp'/'7ED', 'Basic Land').
card_artist('swamp'/'7ED', 'Larry Elmore').
card_number('swamp'/'7ED', '348').
card_multiverse_id('swamp'/'7ED', '11523').

card_in_set('swamp', '7ED').
card_original_type('swamp'/'7ED', 'Land').
card_original_text('swamp'/'7ED', 'B').
card_image_name('swamp'/'7ED', 'swamp4').
card_uid('swamp'/'7ED', '7ED:Swamp:swamp4').
card_rarity('swamp'/'7ED', 'Basic Land').
card_artist('swamp'/'7ED', 'Tony Szczudlo').
card_number('swamp'/'7ED', '349').
card_multiverse_id('swamp'/'7ED', '11524').

card_in_set('tainted æther', '7ED').
card_original_type('tainted æther'/'7ED', 'Enchantment').
card_original_text('tainted æther'/'7ED', 'Whenever a creature comes into play, its controller sacrifices a creature or land.').
card_image_name('tainted æther'/'7ED', 'tainted aether').
card_uid('tainted æther'/'7ED', '7ED:Tainted Æther:tainted aether').
card_rarity('tainted æther'/'7ED', 'Rare').
card_artist('tainted æther'/'7ED', 'Ciruelo').
card_number('tainted æther'/'7ED', '167').
card_flavor_text('tainted æther'/'7ED', 'Pockets of Æther can be so foul they melt flesh from bone.').
card_multiverse_id('tainted æther'/'7ED', '25643').

card_in_set('teferi\'s puzzle box', '7ED').
card_original_type('teferi\'s puzzle box'/'7ED', 'Artifact').
card_original_text('teferi\'s puzzle box'/'7ED', 'At the beginning of each player\'s draw step, that player puts the cards in his or her hand on the bottom of his or her library in any order, then draws that many cards.').
card_image_name('teferi\'s puzzle box'/'7ED', 'teferi\'s puzzle box').
card_uid('teferi\'s puzzle box'/'7ED', '7ED:Teferi\'s Puzzle Box:teferi\'s puzzle box').
card_rarity('teferi\'s puzzle box'/'7ED', 'Rare').
card_artist('teferi\'s puzzle box'/'7ED', 'Donato Giancola').
card_number('teferi\'s puzzle box'/'7ED', '321').
card_multiverse_id('teferi\'s puzzle box'/'7ED', '25662').

card_in_set('telepathic spies', '7ED').
card_original_type('telepathic spies'/'7ED', 'Creature — Wizard').
card_original_text('telepathic spies'/'7ED', 'When Telepathic Spies comes into play, look at target opponent\'s hand.').
card_image_name('telepathic spies'/'7ED', 'telepathic spies').
card_uid('telepathic spies'/'7ED', '7ED:Telepathic Spies:telepathic spies').
card_rarity('telepathic spies'/'7ED', 'Common').
card_artist('telepathic spies'/'7ED', 'Jim Nelson').
card_number('telepathic spies'/'7ED', '101').
card_flavor_text('telepathic spies'/'7ED', 'Most spies sneak into enemy camps. These spies sneak into enemy minds.').
card_multiverse_id('telepathic spies'/'7ED', '25561').

card_in_set('telepathy', '7ED').
card_original_type('telepathy'/'7ED', 'Enchantment').
card_original_text('telepathy'/'7ED', 'Your opponents play with their hands revealed.').
card_image_name('telepathy'/'7ED', 'telepathy').
card_uid('telepathy'/'7ED', '7ED:Telepathy:telepathy').
card_rarity('telepathy'/'7ED', 'Uncommon').
card_artist('telepathy'/'7ED', 'Mark Tedin').
card_number('telepathy'/'7ED', '102').
card_flavor_text('telepathy'/'7ED', '\"Secrets? What secrets?\"').
card_multiverse_id('telepathy'/'7ED', '15793').

card_in_set('temporal adept', '7ED').
card_original_type('temporal adept'/'7ED', 'Creature — Wizard').
card_original_text('temporal adept'/'7ED', '{U}{U}{U}, {T}: Return target permanent to its owner\'s hand.').
card_image_name('temporal adept'/'7ED', 'temporal adept').
card_uid('temporal adept'/'7ED', '7ED:Temporal Adept:temporal adept').
card_rarity('temporal adept'/'7ED', 'Rare').
card_artist('temporal adept'/'7ED', 'Roger Raupp').
card_number('temporal adept'/'7ED', '103').
card_flavor_text('temporal adept'/'7ED', '\"Of course she\'s at the head of her class. All of her classmates have disappeared.\"\n—Tolarian renegade').
card_multiverse_id('temporal adept'/'7ED', '15791').

card_in_set('thieving magpie', '7ED').
card_original_type('thieving magpie'/'7ED', 'Creature — Bird').
card_original_text('thieving magpie'/'7ED', 'Flying\nWhenever Thieving Magpie deals damage to an opponent, you draw a card.').
card_image_name('thieving magpie'/'7ED', 'thieving magpie').
card_uid('thieving magpie'/'7ED', '7ED:Thieving Magpie:thieving magpie').
card_rarity('thieving magpie'/'7ED', 'Uncommon').
card_artist('thieving magpie'/'7ED', 'Christopher Moeller').
card_number('thieving magpie'/'7ED', '104').
card_flavor_text('thieving magpie'/'7ED', 'Other birds collect twigs for their nests. Magpies steal jewels for theirs.').
card_multiverse_id('thieving magpie'/'7ED', '25565').

card_in_set('thorn elemental', '7ED').
card_original_type('thorn elemental'/'7ED', 'Creature — Elemental').
card_original_text('thorn elemental'/'7ED', 'You may have Thorn Elemental deal its combat damage to defending player as though it weren\'t blocked.').
card_image_name('thorn elemental'/'7ED', 'thorn elemental').
card_uid('thorn elemental'/'7ED', '7ED:Thorn Elemental:thorn elemental').
card_rarity('thorn elemental'/'7ED', 'Rare').
card_artist('thorn elemental'/'7ED', 'rk post').
card_number('thorn elemental'/'7ED', '273').
card_flavor_text('thorn elemental'/'7ED', 'Rain from this storm leaves you pinned to the ground like an insect.').
card_multiverse_id('thorn elemental'/'7ED', '25596').

card_in_set('thoughtleech', '7ED').
card_original_type('thoughtleech'/'7ED', 'Enchantment').
card_original_text('thoughtleech'/'7ED', 'Whenever an island an opponent controls becomes tapped, you may gain 1 life.').
card_image_name('thoughtleech'/'7ED', 'thoughtleech').
card_uid('thoughtleech'/'7ED', '7ED:Thoughtleech:thoughtleech').
card_rarity('thoughtleech'/'7ED', 'Uncommon').
card_artist('thoughtleech'/'7ED', 'Rebecca Guay').
card_number('thoughtleech'/'7ED', '274').
card_flavor_text('thoughtleech'/'7ED', '\"Their thoughts flow through my mind, and I am healed.\"\n—Fyndhorn elder').
card_multiverse_id('thoughtleech'/'7ED', '25587').

card_in_set('throne of bone', '7ED').
card_original_type('throne of bone'/'7ED', 'Artifact').
card_original_text('throne of bone'/'7ED', 'Whenever a player plays a black spell, you may pay {1}. If you do, you gain 1 life.').
card_image_name('throne of bone'/'7ED', 'throne of bone').
card_uid('throne of bone'/'7ED', '7ED:Throne of Bone:throne of bone').
card_rarity('throne of bone'/'7ED', 'Uncommon').
card_artist('throne of bone'/'7ED', 'Ron Spears').
card_number('throne of bone'/'7ED', '322').
card_flavor_text('throne of bone'/'7ED', 'Tainted by a thousand fears.').
card_multiverse_id('throne of bone'/'7ED', '25652').

card_in_set('tolarian winds', '7ED').
card_original_type('tolarian winds'/'7ED', 'Instant').
card_original_text('tolarian winds'/'7ED', 'Discard your hand, then draw that many cards.').
card_image_name('tolarian winds'/'7ED', 'tolarian winds').
card_uid('tolarian winds'/'7ED', '7ED:Tolarian Winds:tolarian winds').
card_rarity('tolarian winds'/'7ED', 'Common').
card_artist('tolarian winds'/'7ED', 'Bob Petillo').
card_number('tolarian winds'/'7ED', '105').
card_flavor_text('tolarian winds'/'7ED', 'Why not ask the winds?\n—Tolarian expression meaning\n\"Who knows?\"').
card_multiverse_id('tolarian winds'/'7ED', '25558').

card_in_set('trained armodon', '7ED').
card_original_type('trained armodon'/'7ED', 'Creature — Elephant').
card_original_text('trained armodon'/'7ED', '').
card_image_name('trained armodon'/'7ED', 'trained armodon').
card_uid('trained armodon'/'7ED', '7ED:Trained Armodon:trained armodon').
card_rarity('trained armodon'/'7ED', 'Common').
card_artist('trained armodon'/'7ED', 'John Matson').
card_number('trained armodon'/'7ED', '275').
card_flavor_text('trained armodon'/'7ED', 'Armodons are trained to step on things. Enemy things.').
card_multiverse_id('trained armodon'/'7ED', '13048').

card_in_set('trained orgg', '7ED').
card_original_type('trained orgg'/'7ED', 'Creature — Beast').
card_original_text('trained orgg'/'7ED', '').
card_image_name('trained orgg'/'7ED', 'trained orgg').
card_uid('trained orgg'/'7ED', '7ED:Trained Orgg:trained orgg').
card_rarity('trained orgg'/'7ED', 'Rare').
card_artist('trained orgg'/'7ED', 'Alex Horley-Orlandelli').
card_number('trained orgg'/'7ED', '224').
card_flavor_text('trained orgg'/'7ED', '\"That arm\'s for bashin\'. That one\'s for smashin\'. That one\'s for scratchin\'. And that one . . . I don\'t know what that one\'s for.\"\n—Orgg trainer').
card_multiverse_id('trained orgg'/'7ED', '12975').

card_in_set('tranquility', '7ED').
card_original_type('tranquility'/'7ED', 'Sorcery').
card_original_text('tranquility'/'7ED', 'Destroy all enchantments.').
card_image_name('tranquility'/'7ED', 'tranquility').
card_uid('tranquility'/'7ED', '7ED:Tranquility:tranquility').
card_rarity('tranquility'/'7ED', 'Common').
card_artist('tranquility'/'7ED', 'John Matson').
card_number('tranquility'/'7ED', '276').
card_multiverse_id('tranquility'/'7ED', '11540').

card_in_set('treasure trove', '7ED').
card_original_type('treasure trove'/'7ED', 'Enchantment').
card_original_text('treasure trove'/'7ED', '{2}{U}{U} Draw a card.').
card_image_name('treasure trove'/'7ED', 'treasure trove').
card_uid('treasure trove'/'7ED', '7ED:Treasure Trove:treasure trove').
card_rarity('treasure trove'/'7ED', 'Uncommon').
card_artist('treasure trove'/'7ED', 'Brian Despain').
card_number('treasure trove'/'7ED', '106').
card_flavor_text('treasure trove'/'7ED', '\"Nope, there\'s nothing here either. You guys go on ahead, I\'ll catch up in a minute. . . . Whoo hoo!\"').
card_multiverse_id('treasure trove'/'7ED', '13141').

card_in_set('treefolk seedlings', '7ED').
card_original_type('treefolk seedlings'/'7ED', 'Creature — Treefolk').
card_original_text('treefolk seedlings'/'7ED', 'Treefolk Seedlings\'s toughness is equal to the number of forests you control.').
card_image_name('treefolk seedlings'/'7ED', 'treefolk seedlings').
card_uid('treefolk seedlings'/'7ED', '7ED:Treefolk Seedlings:treefolk seedlings').
card_rarity('treefolk seedlings'/'7ED', 'Uncommon').
card_artist('treefolk seedlings'/'7ED', 'Don Hazeltine').
card_number('treefolk seedlings'/'7ED', '277').
card_flavor_text('treefolk seedlings'/'7ED', 'Even the grandest forest begins with a single seedling.\n—Llanowar saying').
card_multiverse_id('treefolk seedlings'/'7ED', '25589').

card_in_set('tremor', '7ED').
card_original_type('tremor'/'7ED', 'Sorcery').
card_original_text('tremor'/'7ED', 'Tremor deals 1 damage to each creature without flying.').
card_image_name('tremor'/'7ED', 'tremor').
card_uid('tremor'/'7ED', '7ED:Tremor:tremor').
card_rarity('tremor'/'7ED', 'Common').
card_artist('tremor'/'7ED', 'Michael Koelsch').
card_number('tremor'/'7ED', '225').
card_flavor_text('tremor'/'7ED', 'Where do you run when the earth becomes your enemy?').
card_multiverse_id('tremor'/'7ED', '12988').

card_in_set('twiddle', '7ED').
card_original_type('twiddle'/'7ED', 'Instant').
card_original_text('twiddle'/'7ED', 'Tap or untap target artifact, creature, or land.').
card_image_name('twiddle'/'7ED', 'twiddle').
card_uid('twiddle'/'7ED', '7ED:Twiddle:twiddle').
card_rarity('twiddle'/'7ED', 'Common').
card_artist('twiddle'/'7ED', 'Rebecca Guay').
card_number('twiddle'/'7ED', '107').
card_multiverse_id('twiddle'/'7ED', '11544').

card_in_set('uktabi wildcats', '7ED').
card_original_type('uktabi wildcats'/'7ED', 'Creature — Cat').
card_original_text('uktabi wildcats'/'7ED', 'Uktabi Wildcats\'s power and toughness are each equal to the number of forests you control.\n{G}, Sacrifice a forest: Regenerate Uktabi Wildcats.').
card_image_name('uktabi wildcats'/'7ED', 'uktabi wildcats').
card_uid('uktabi wildcats'/'7ED', '7ED:Uktabi Wildcats:uktabi wildcats').
card_rarity('uktabi wildcats'/'7ED', 'Rare').
card_artist('uktabi wildcats'/'7ED', 'Thomas Gianni').
card_number('uktabi wildcats'/'7ED', '278').
card_multiverse_id('uktabi wildcats'/'7ED', '12968').

card_in_set('underground river', '7ED').
card_original_type('underground river'/'7ED', 'Land').
card_original_text('underground river'/'7ED', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {U} or {B} to your mana pool. Underground River deals 1 damage to you.').
card_image_name('underground river'/'7ED', 'underground river').
card_uid('underground river'/'7ED', '7ED:Underground River:underground river').
card_rarity('underground river'/'7ED', 'Rare').
card_artist('underground river'/'7ED', 'Andrew Goldhawk').
card_number('underground river'/'7ED', '350').
card_multiverse_id('underground river'/'7ED', '11545').

card_in_set('unholy strength', '7ED').
card_original_type('unholy strength'/'7ED', 'Enchant Creature').
card_original_text('unholy strength'/'7ED', 'Enchanted creature gets +2/+1.').
card_image_name('unholy strength'/'7ED', 'unholy strength').
card_uid('unholy strength'/'7ED', '7ED:Unholy Strength:unholy strength').
card_rarity('unholy strength'/'7ED', 'Common').
card_artist('unholy strength'/'7ED', 'Gary Ruddell').
card_number('unholy strength'/'7ED', '168').
card_flavor_text('unholy strength'/'7ED', '\"I don\'t feel any different.\"').
card_multiverse_id('unholy strength'/'7ED', '15821').

card_in_set('unsummon', '7ED').
card_original_type('unsummon'/'7ED', 'Instant').
card_original_text('unsummon'/'7ED', 'Return target creature to its owner\'s hand.').
card_image_name('unsummon'/'7ED', 'unsummon').
card_uid('unsummon'/'7ED', '7ED:Unsummon:unsummon').
card_rarity('unsummon'/'7ED', 'Common').
card_artist('unsummon'/'7ED', 'Ron Spencer').
card_number('unsummon'/'7ED', '108').
card_multiverse_id('unsummon'/'7ED', '11548').

card_in_set('untamed wilds', '7ED').
card_original_type('untamed wilds'/'7ED', 'Sorcery').
card_original_text('untamed wilds'/'7ED', 'Search your library for a basic land card and put that card into play. Then shuffle your library.').
card_image_name('untamed wilds'/'7ED', 'untamed wilds').
card_uid('untamed wilds'/'7ED', '7ED:Untamed Wilds:untamed wilds').
card_rarity('untamed wilds'/'7ED', 'Uncommon').
card_artist('untamed wilds'/'7ED', 'Thomas Gianni').
card_number('untamed wilds'/'7ED', '279').
card_flavor_text('untamed wilds'/'7ED', 'No forest is so remote that the elves don\'t know its secrets.').
card_multiverse_id('untamed wilds'/'7ED', '11549').

card_in_set('venerable monk', '7ED').
card_original_type('venerable monk'/'7ED', 'Creature — Cleric').
card_original_text('venerable monk'/'7ED', 'When Venerable Monk comes into play, you gain 2 life.').
card_image_name('venerable monk'/'7ED', 'venerable monk').
card_uid('venerable monk'/'7ED', '7ED:Venerable Monk:venerable monk').
card_rarity('venerable monk'/'7ED', 'Common').
card_artist('venerable monk'/'7ED', 'Mark Brill').
card_number('venerable monk'/'7ED', '53').
card_flavor_text('venerable monk'/'7ED', '\"I rely not so much on the wisdom of age as on the overconfidence of youth.\"').
card_multiverse_id('venerable monk'/'7ED', '13167').

card_in_set('vengeance', '7ED').
card_original_type('vengeance'/'7ED', 'Sorcery').
card_original_text('vengeance'/'7ED', 'Destroy target tapped creature.').
card_image_name('vengeance'/'7ED', 'vengeance').
card_uid('vengeance'/'7ED', '7ED:Vengeance:vengeance').
card_rarity('vengeance'/'7ED', 'Uncommon').
card_artist('vengeance'/'7ED', 'Paolo Parente').
card_number('vengeance'/'7ED', '54').
card_flavor_text('vengeance'/'7ED', '\"It is the duty of the strong to oppose any who threaten the weak.\"\n—The Southern Paladin').
card_multiverse_id('vengeance'/'7ED', '14462').

card_in_set('verduran enchantress', '7ED').
card_original_type('verduran enchantress'/'7ED', 'Creature — Druid').
card_original_text('verduran enchantress'/'7ED', 'Whenever you play an enchantment spell, you may draw a card.').
card_image_name('verduran enchantress'/'7ED', 'verduran enchantress').
card_uid('verduran enchantress'/'7ED', '7ED:Verduran Enchantress:verduran enchantress').
card_rarity('verduran enchantress'/'7ED', 'Rare').
card_artist('verduran enchantress'/'7ED', 'Rob Alexander').
card_number('verduran enchantress'/'7ED', '280').
card_flavor_text('verduran enchantress'/'7ED', 'Each spell has its own song, and the enchantress knows the dance for each.').
card_multiverse_id('verduran enchantress'/'7ED', '25594').

card_in_set('vernal bloom', '7ED').
card_original_type('vernal bloom'/'7ED', 'Enchantment').
card_original_text('vernal bloom'/'7ED', 'Whenever a forest is tapped for mana, its controller adds {G} to his or her mana pool.').
card_image_name('vernal bloom'/'7ED', 'vernal bloom').
card_uid('vernal bloom'/'7ED', '7ED:Vernal Bloom:vernal bloom').
card_rarity('vernal bloom'/'7ED', 'Rare').
card_artist('vernal bloom'/'7ED', 'Scott Bailey').
card_number('vernal bloom'/'7ED', '281').
card_flavor_text('vernal bloom'/'7ED', 'Spring breathes the magic of new life.').
card_multiverse_id('vernal bloom'/'7ED', '13119').

card_in_set('vigilant drake', '7ED').
card_original_type('vigilant drake'/'7ED', 'Creature — Drake').
card_original_text('vigilant drake'/'7ED', 'Flying\n{2}{U} Untap Vigilant Drake').
card_image_name('vigilant drake'/'7ED', 'vigilant drake').
card_uid('vigilant drake'/'7ED', '7ED:Vigilant Drake:vigilant drake').
card_rarity('vigilant drake'/'7ED', 'Common').
card_artist('vigilant drake'/'7ED', 'Dave Dorman').
card_number('vigilant drake'/'7ED', '109').
card_flavor_text('vigilant drake'/'7ED', 'It can stay aloft for weeks with little or no sleep.').
card_multiverse_id('vigilant drake'/'7ED', '26845').

card_in_set('vizzerdrix', '7ED').
card_original_type('vizzerdrix'/'7ED', 'Creature — Beast').
card_original_text('vizzerdrix'/'7ED', '').
card_image_name('vizzerdrix'/'7ED', 'vizzerdrix').
card_uid('vizzerdrix'/'7ED', '7ED:Vizzerdrix:vizzerdrix').
card_rarity('vizzerdrix'/'7ED', 'Rare').
card_artist('vizzerdrix'/'7ED', 'Dave Dorman').
card_number('vizzerdrix'/'7ED', '110').
card_flavor_text('vizzerdrix'/'7ED', 'A bored wizard once created a vizzerdrix out of a bunny and a piranha. He never made that mistake again.').
card_multiverse_id('vizzerdrix'/'7ED', '29322').

card_in_set('volcanic hammer', '7ED').
card_original_type('volcanic hammer'/'7ED', 'Sorcery').
card_original_text('volcanic hammer'/'7ED', 'Volcanic Hammer deals 3 damage to target creature or player.').
card_image_name('volcanic hammer'/'7ED', 'volcanic hammer').
card_uid('volcanic hammer'/'7ED', '7ED:Volcanic Hammer:volcanic hammer').
card_rarity('volcanic hammer'/'7ED', 'Common').
card_artist('volcanic hammer'/'7ED', 'Ben Thompson').
card_number('volcanic hammer'/'7ED', '226').
card_flavor_text('volcanic hammer'/'7ED', 'Fire finds its form in the heat of the forge.').
card_multiverse_id('volcanic hammer'/'7ED', '30171').

card_in_set('wall of air', '7ED').
card_original_type('wall of air'/'7ED', 'Creature — Wall').
card_original_text('wall of air'/'7ED', '(Walls can\'t attack.)\nFlying').
card_image_name('wall of air'/'7ED', 'wall of air').
card_uid('wall of air'/'7ED', '7ED:Wall of Air:wall of air').
card_rarity('wall of air'/'7ED', 'Uncommon').
card_artist('wall of air'/'7ED', 'John Avon').
card_number('wall of air'/'7ED', '111').
card_flavor_text('wall of air'/'7ED', '\"Let the air itself protect you, for it is everywhere.\"\n—Master wizard').
card_multiverse_id('wall of air'/'7ED', '11560').

card_in_set('wall of bone', '7ED').
card_original_type('wall of bone'/'7ED', 'Creature — Wall').
card_original_text('wall of bone'/'7ED', '(Walls can\'t attack.)\n{B} Regenerate Wall of Bone.').
card_image_name('wall of bone'/'7ED', 'wall of bone').
card_uid('wall of bone'/'7ED', '7ED:Wall of Bone:wall of bone').
card_rarity('wall of bone'/'7ED', 'Uncommon').
card_artist('wall of bone'/'7ED', 'Alan Pollack').
card_number('wall of bone'/'7ED', '169').
card_flavor_text('wall of bone'/'7ED', 'A skeletal wall, strengthened by the souls of traitors, rings the city of the Eastern Paladin.').
card_multiverse_id('wall of bone'/'7ED', '25639').

card_in_set('wall of fire', '7ED').
card_original_type('wall of fire'/'7ED', 'Creature — Wall').
card_original_text('wall of fire'/'7ED', '(Walls can\'t attack.)\n{R} Wall of Fire gets +1/+0 until end of turn.').
card_image_name('wall of fire'/'7ED', 'wall of fire').
card_uid('wall of fire'/'7ED', '7ED:Wall of Fire:wall of fire').
card_rarity('wall of fire'/'7ED', 'Uncommon').
card_artist('wall of fire'/'7ED', 'Ron Spencer').
card_number('wall of fire'/'7ED', '227').
card_flavor_text('wall of fire'/'7ED', 'The erupting flames work as both barrier and weapon.').
card_multiverse_id('wall of fire'/'7ED', '11563').

card_in_set('wall of spears', '7ED').
card_original_type('wall of spears'/'7ED', 'Artifact Creature — Wall').
card_original_text('wall of spears'/'7ED', '(Walls can\'t attack.)\nFirst strike').
card_image_name('wall of spears'/'7ED', 'wall of spears').
card_uid('wall of spears'/'7ED', '7ED:Wall of Spears:wall of spears').
card_rarity('wall of spears'/'7ED', 'Uncommon').
card_artist('wall of spears'/'7ED', 'Christopher Moeller').
card_number('wall of spears'/'7ED', '323').
card_flavor_text('wall of spears'/'7ED', '\"A stick is your friend. A pointed stick is your good friend. An army of pointed sticks is your best friend.\"\n—Onean sergeant').
card_multiverse_id('wall of spears'/'7ED', '11564').

card_in_set('wall of swords', '7ED').
card_original_type('wall of swords'/'7ED', 'Creature — Wall').
card_original_text('wall of swords'/'7ED', '(Walls can\'t attack.)\nFlying').
card_image_name('wall of swords'/'7ED', 'wall of swords').
card_uid('wall of swords'/'7ED', '7ED:Wall of Swords:wall of swords').
card_rarity('wall of swords'/'7ED', 'Uncommon').
card_artist('wall of swords'/'7ED', 'Hannibal King').
card_number('wall of swords'/'7ED', '55').
card_flavor_text('wall of swords'/'7ED', 'Sharper than wind, lighter than air.').
card_multiverse_id('wall of swords'/'7ED', '11566').

card_in_set('wall of wonder', '7ED').
card_original_type('wall of wonder'/'7ED', 'Creature — Wall').
card_original_text('wall of wonder'/'7ED', '(Walls can\'t attack.)\n{2}{U}{U} Wall of Wonder gets +4/-4 until end of turn and may attack this turn as though it weren\'t a Wall.').
card_image_name('wall of wonder'/'7ED', 'wall of wonder').
card_uid('wall of wonder'/'7ED', '7ED:Wall of Wonder:wall of wonder').
card_rarity('wall of wonder'/'7ED', 'Rare').
card_artist('wall of wonder'/'7ED', 'Carl Critchlow').
card_number('wall of wonder'/'7ED', '112').
card_flavor_text('wall of wonder'/'7ED', 'They wondered at the size of it. It wondered which to eat first.').
card_multiverse_id('wall of wonder'/'7ED', '15797').

card_in_set('western paladin', '7ED').
card_original_type('western paladin'/'7ED', 'Creature — Knight').
card_original_text('western paladin'/'7ED', '{B}{B}, {T}: Destroy target white creature.').
card_image_name('western paladin'/'7ED', 'western paladin').
card_uid('western paladin'/'7ED', '7ED:Western Paladin:western paladin').
card_rarity('western paladin'/'7ED', 'Rare').
card_artist('western paladin'/'7ED', 'Paolo Parente').
card_number('western paladin'/'7ED', '170').
card_flavor_text('western paladin'/'7ED', '\"Expect nothing but scorn, flattery, and lies. And never turn your back on him.\"\n—The Northern Paladin').
card_multiverse_id('western paladin'/'7ED', '13138').

card_in_set('wild growth', '7ED').
card_original_type('wild growth'/'7ED', 'Enchant Land').
card_original_text('wild growth'/'7ED', 'Whenever enchanted land is tapped for mana, its controller adds {G} to his or her mana pool.').
card_image_name('wild growth'/'7ED', 'wild growth').
card_uid('wild growth'/'7ED', '7ED:Wild Growth:wild growth').
card_rarity('wild growth'/'7ED', 'Common').
card_artist('wild growth'/'7ED', 'Tony Szczudlo').
card_number('wild growth'/'7ED', '282').
card_flavor_text('wild growth'/'7ED', 'If you stop to listen, you can hear it grow.').
card_multiverse_id('wild growth'/'7ED', '11573').

card_in_set('wildfire', '7ED').
card_original_type('wildfire'/'7ED', 'Sorcery').
card_original_text('wildfire'/'7ED', 'Each player sacrifices four lands. Wildfire deals 4 damage to each creature.').
card_image_name('wildfire'/'7ED', 'wildfire').
card_uid('wildfire'/'7ED', '7ED:Wildfire:wildfire').
card_rarity('wildfire'/'7ED', 'Rare').
card_artist('wildfire'/'7ED', 'Ron Spencer').
card_number('wildfire'/'7ED', '228').
card_flavor_text('wildfire'/'7ED', 'Fire is always at the top of the food chain, and it has a big appetite.').
card_multiverse_id('wildfire'/'7ED', '25685').

card_in_set('wind dancer', '7ED').
card_original_type('wind dancer'/'7ED', 'Creature — Faerie').
card_original_text('wind dancer'/'7ED', 'Flying\n{T}: Target creature gains flying until end of turn.').
card_image_name('wind dancer'/'7ED', 'wind dancer').
card_uid('wind dancer'/'7ED', '7ED:Wind Dancer:wind dancer').
card_rarity('wind dancer'/'7ED', 'Uncommon').
card_artist('wind dancer'/'7ED', 'Rob Alexander').
card_number('wind dancer'/'7ED', '113').
card_flavor_text('wind dancer'/'7ED', 'The sky is her stage.').
card_multiverse_id('wind dancer'/'7ED', '13038').

card_in_set('wind drake', '7ED').
card_original_type('wind drake'/'7ED', 'Creature — Drake').
card_original_text('wind drake'/'7ED', 'Flying').
card_image_name('wind drake'/'7ED', 'wind drake').
card_uid('wind drake'/'7ED', '7ED:Wind Drake:wind drake').
card_rarity('wind drake'/'7ED', 'Common').
card_artist('wind drake'/'7ED', 'Tom Wänerstrand').
card_number('wind drake'/'7ED', '114').
card_flavor_text('wind drake'/'7ED', '\"But high she shoots through air and light,\nAbove all low delay,\nWhere nothing earthly bounds her flight,\nNor shadow dims her way.\"\n—Thomas Moore,\n\"Oh that I had Wings\"').
card_multiverse_id('wind drake'/'7ED', '13039').

card_in_set('wing snare', '7ED').
card_original_type('wing snare'/'7ED', 'Sorcery').
card_original_text('wing snare'/'7ED', 'Destroy target creature with flying.').
card_image_name('wing snare'/'7ED', 'wing snare').
card_uid('wing snare'/'7ED', '7ED:Wing Snare:wing snare').
card_rarity('wing snare'/'7ED', 'Uncommon').
card_artist('wing snare'/'7ED', 'Daren Bader').
card_number('wing snare'/'7ED', '283').
card_flavor_text('wing snare'/'7ED', 'The elves long ago mastered the art of hunting with nets. They call it \"fishing the sky.\"').
card_multiverse_id('wing snare'/'7ED', '13195').

card_in_set('wood elves', '7ED').
card_original_type('wood elves'/'7ED', 'Creature — Elf').
card_original_text('wood elves'/'7ED', 'When Wood Elves comes into play, search your library for a forest card and put that card into play. Then shuffle your library.').
card_image_name('wood elves'/'7ED', 'wood elves').
card_uid('wood elves'/'7ED', '7ED:Wood Elves:wood elves').
card_rarity('wood elves'/'7ED', 'Common').
card_artist('wood elves'/'7ED', 'Christopher Moeller').
card_number('wood elves'/'7ED', '284').
card_flavor_text('wood elves'/'7ED', 'Every branch a crossroads, every vine a swift steed.').
card_multiverse_id('wood elves'/'7ED', '13147').

card_in_set('wooden sphere', '7ED').
card_original_type('wooden sphere'/'7ED', 'Artifact').
card_original_text('wooden sphere'/'7ED', 'Whenever a player plays a green spell, you may pay {1}. If you do, you gain 1 life.').
card_image_name('wooden sphere'/'7ED', 'wooden sphere').
card_uid('wooden sphere'/'7ED', '7ED:Wooden Sphere:wooden sphere').
card_rarity('wooden sphere'/'7ED', 'Uncommon').
card_artist('wooden sphere'/'7ED', 'Terese Nielsen').
card_number('wooden sphere'/'7ED', '324').
card_flavor_text('wooden sphere'/'7ED', 'Strengthened by a thousand limbs.').
card_multiverse_id('wooden sphere'/'7ED', '25653').

card_in_set('worship', '7ED').
card_original_type('worship'/'7ED', 'Enchantment').
card_original_text('worship'/'7ED', 'If you control a creature, damage that would reduce your life total to less than 1 reduces it to 1 instead.').
card_image_name('worship'/'7ED', 'worship').
card_uid('worship'/'7ED', '7ED:Worship:worship').
card_rarity('worship'/'7ED', 'Rare').
card_artist('worship'/'7ED', 'rk post').
card_number('worship'/'7ED', '56').
card_flavor_text('worship'/'7ED', '\"Believe in the ideal, not the idol.\"\n—Serra').
card_multiverse_id('worship'/'7ED', '25553').

card_in_set('wrath of god', '7ED').
card_original_type('wrath of god'/'7ED', 'Sorcery').
card_original_text('wrath of god'/'7ED', 'Destroy all creatures. They can\'t be regenerated.').
card_image_name('wrath of god'/'7ED', 'wrath of god').
card_uid('wrath of god'/'7ED', '7ED:Wrath of God:wrath of god').
card_rarity('wrath of god'/'7ED', 'Rare').
card_artist('wrath of god'/'7ED', 'Kev Walker').
card_number('wrath of god'/'7ED', '57').
card_multiverse_id('wrath of god'/'7ED', '11581').

card_in_set('yavimaya enchantress', '7ED').
card_original_type('yavimaya enchantress'/'7ED', 'Creature — Druid').
card_original_text('yavimaya enchantress'/'7ED', 'Yavimaya Enchantress gets +1/+1 for each enchantment in play.').
card_image_name('yavimaya enchantress'/'7ED', 'yavimaya enchantress').
card_uid('yavimaya enchantress'/'7ED', '7ED:Yavimaya Enchantress:yavimaya enchantress').
card_rarity('yavimaya enchantress'/'7ED', 'Uncommon').
card_artist('yavimaya enchantress'/'7ED', 'Terese Nielsen').
card_number('yavimaya enchantress'/'7ED', '285').
card_flavor_text('yavimaya enchantress'/'7ED', 'Her roots connect her to the forest\'s wishes.').
card_multiverse_id('yavimaya enchantress'/'7ED', '15882').

card_in_set('yawgmoth\'s edict', '7ED').
card_original_type('yawgmoth\'s edict'/'7ED', 'Enchantment').
card_original_text('yawgmoth\'s edict'/'7ED', 'Whenever an opponent plays a white spell, that player loses 1 life and you gain 1 life.').
card_image_name('yawgmoth\'s edict'/'7ED', 'yawgmoth\'s edict').
card_uid('yawgmoth\'s edict'/'7ED', '7ED:Yawgmoth\'s Edict:yawgmoth\'s edict').
card_rarity('yawgmoth\'s edict'/'7ED', 'Uncommon').
card_artist('yawgmoth\'s edict'/'7ED', 'Donato Giancola').
card_number('yawgmoth\'s edict'/'7ED', '171').
card_flavor_text('yawgmoth\'s edict'/'7ED', 'The warping of bodies and souls has become the heart of Yawgmoth\'s existence.').
card_multiverse_id('yawgmoth\'s edict'/'7ED', '25630').
