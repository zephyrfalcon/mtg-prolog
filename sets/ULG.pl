% Urza's Legacy

set('ULG').
set_name('ULG', 'Urza\'s Legacy').
set_release_date('ULG', '1999-02-15').
set_border('ULG', 'black').
set_type('ULG', 'expansion').
set_block('ULG', 'Urza\'s').

card_in_set('about face', 'ULG').
card_original_type('about face'/'ULG', 'Instant').
card_original_text('about face'/'ULG', 'Switch target creature\'s power and toughness until end of turn. Effects that alter the creature\'s power alter its toughness instead, and vice versa, this turn.').
card_first_print('about face', 'ULG').
card_image_name('about face'/'ULG', 'about face').
card_uid('about face'/'ULG', 'ULG:About Face:about face').
card_rarity('about face'/'ULG', 'Common').
card_artist('about face'/'ULG', 'Melissa A. Benson').
card_number('about face'/'ULG', '73').
card_flavor_text('about face'/'ULG', 'The overconfident are the most vulnerable.').
card_multiverse_id('about face'/'ULG', '12414').

card_in_set('angel\'s trumpet', 'ULG').
card_original_type('angel\'s trumpet'/'ULG', 'Artifact').
card_original_text('angel\'s trumpet'/'ULG', 'Attacking does not cause creatures to tap.\nAt the end of each player\'s turn, tap all untapped creatures he or she controls that did not attack this turn. Angel\'s Trumpet deals 1 damage to that player for each creature tapped this way.').
card_first_print('angel\'s trumpet', 'ULG').
card_image_name('angel\'s trumpet'/'ULG', 'angel\'s trumpet').
card_uid('angel\'s trumpet'/'ULG', 'ULG:Angel\'s Trumpet:angel\'s trumpet').
card_rarity('angel\'s trumpet'/'ULG', 'Uncommon').
card_artist('angel\'s trumpet'/'ULG', 'Kev Walker').
card_number('angel\'s trumpet'/'ULG', '121').
card_multiverse_id('angel\'s trumpet'/'ULG', '8832').

card_in_set('angelic curator', 'ULG').
card_original_type('angelic curator'/'ULG', 'Summon — Spirit').
card_original_text('angelic curator'/'ULG', 'Flying, protection from artifacts').
card_first_print('angelic curator', 'ULG').
card_image_name('angelic curator'/'ULG', 'angelic curator').
card_uid('angelic curator'/'ULG', 'ULG:Angelic Curator:angelic curator').
card_rarity('angelic curator'/'ULG', 'Common').
card_artist('angelic curator'/'ULG', 'Greg Staples').
card_number('angelic curator'/'ULG', '1').
card_flavor_text('angelic curator'/'ULG', '\"Do not treat your people as you treat your artifacts. Let them go, and they will live; seal them here, and they will die.\"\n—Urza, to Radiant').
card_multiverse_id('angelic curator'/'ULG', '12354').

card_in_set('anthroplasm', 'ULG').
card_original_type('anthroplasm'/'ULG', 'Summon — Shapeshifter').
card_original_text('anthroplasm'/'ULG', 'Anthroplasm comes into play with two +1/+1 counters on it.\n{X}, {T}: Remove all +1/+1 counters from Anthroplasm and put X +1/+1 counters on it.').
card_first_print('anthroplasm', 'ULG').
card_image_name('anthroplasm'/'ULG', 'anthroplasm').
card_uid('anthroplasm'/'ULG', 'ULG:Anthroplasm:anthroplasm').
card_rarity('anthroplasm'/'ULG', 'Rare').
card_artist('anthroplasm'/'ULG', 'Ron Spencer').
card_number('anthroplasm'/'ULG', '25').
card_flavor_text('anthroplasm'/'ULG', 'I never metamorphosis I didn\'t like.').
card_multiverse_id('anthroplasm'/'ULG', '12387').

card_in_set('archivist', 'ULG').
card_original_type('archivist'/'ULG', 'Summon — Wizard').
card_original_text('archivist'/'ULG', '{T}: Draw a card.').
card_first_print('archivist', 'ULG').
card_image_name('archivist'/'ULG', 'archivist').
card_uid('archivist'/'ULG', 'ULG:Archivist:archivist').
card_rarity('archivist'/'ULG', 'Rare').
card_artist('archivist'/'ULG', 'Pete Venters').
card_number('archivist'/'ULG', '26').
card_flavor_text('archivist'/'ULG', 'Some do. Some teach. The rest look it up.').
card_multiverse_id('archivist'/'ULG', '12390').

card_in_set('aura flux', 'ULG').
card_original_type('aura flux'/'ULG', 'Enchantment').
card_original_text('aura flux'/'ULG', 'Each other enchantment gains \"During your upkeep, pay {2} or sacrifice this enchantment.\"').
card_first_print('aura flux', 'ULG').
card_image_name('aura flux'/'ULG', 'aura flux').
card_uid('aura flux'/'ULG', 'ULG:Aura Flux:aura flux').
card_rarity('aura flux'/'ULG', 'Common').
card_artist('aura flux'/'ULG', 'John Avon').
card_number('aura flux'/'ULG', '27').
card_flavor_text('aura flux'/'ULG', 'To some, the Tolarian sunrise was a blinding flash; to others, a lingering glow.').
card_multiverse_id('aura flux'/'ULG', '12943').

card_in_set('avalanche riders', 'ULG').
card_original_type('avalanche riders'/'ULG', 'Summon — Nomads').
card_original_text('avalanche riders'/'ULG', 'Echo (During your next upkeep after this permanent comes under your control, pay its casting cost or sacrifice it.)\nAvalanche Riders is unaffected by summoning sickness.\nWhen Avalanche Riders comes into play, destroy target land.').
card_first_print('avalanche riders', 'ULG').
card_image_name('avalanche riders'/'ULG', 'avalanche riders').
card_uid('avalanche riders'/'ULG', 'ULG:Avalanche Riders:avalanche riders').
card_rarity('avalanche riders'/'ULG', 'Uncommon').
card_artist('avalanche riders'/'ULG', 'Edward P. Beard, Jr.').
card_number('avalanche riders'/'ULG', '74').
card_multiverse_id('avalanche riders'/'ULG', '12418').

card_in_set('beast of burden', 'ULG').
card_original_type('beast of burden'/'ULG', 'Artifact Creature').
card_original_text('beast of burden'/'ULG', 'Beast of Burden has power and toughness each equal to the total number of creatures in play.').
card_image_name('beast of burden'/'ULG', 'beast of burden').
card_uid('beast of burden'/'ULG', 'ULG:Beast of Burden:beast of burden').
card_rarity('beast of burden'/'ULG', 'Rare').
card_artist('beast of burden'/'ULG', 'Ron Spears').
card_number('beast of burden'/'ULG', '122').
card_flavor_text('beast of burden'/'ULG', '\"If it is meant to be nothing but a machine,\" Karn finally asked Jhoira, \"why did Urza build it to be like me?\"').
card_multiverse_id('beast of burden'/'ULG', '12504').

card_in_set('blessed reversal', 'ULG').
card_original_type('blessed reversal'/'ULG', 'Instant').
card_original_text('blessed reversal'/'ULG', 'Gain 3 life for each creature attacking you.').
card_image_name('blessed reversal'/'ULG', 'blessed reversal').
card_uid('blessed reversal'/'ULG', 'ULG:Blessed Reversal:blessed reversal').
card_rarity('blessed reversal'/'ULG', 'Rare').
card_artist('blessed reversal'/'ULG', 'Pete Venters').
card_number('blessed reversal'/'ULG', '2').
card_flavor_text('blessed reversal'/'ULG', 'Even an enemy is a valuable resource.').
card_multiverse_id('blessed reversal'/'ULG', '12366').

card_in_set('bloated toad', 'ULG').
card_original_type('bloated toad'/'ULG', 'Summon — Toad').
card_original_text('bloated toad'/'ULG', 'Protection from blue\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('bloated toad', 'ULG').
card_image_name('bloated toad'/'ULG', 'bloated toad').
card_uid('bloated toad'/'ULG', 'ULG:Bloated Toad:bloated toad').
card_rarity('bloated toad'/'ULG', 'Uncommon').
card_artist('bloated toad'/'ULG', 'Una Fricker').
card_number('bloated toad'/'ULG', '97').
card_multiverse_id('bloated toad'/'ULG', '12462').

card_in_set('bone shredder', 'ULG').
card_original_type('bone shredder'/'ULG', 'Summon — Minion').
card_original_text('bone shredder'/'ULG', 'Flying; echo (During your next upkeep after this permanent comes under your control, pay its casting cost or sacrifice it.) When Bone Shredder comes into play, destroy target nonartifact, nonblack creature.').
card_first_print('bone shredder', 'ULG').
card_image_name('bone shredder'/'ULG', 'bone shredder').
card_uid('bone shredder'/'ULG', 'ULG:Bone Shredder:bone shredder').
card_rarity('bone shredder'/'ULG', 'Uncommon').
card_artist('bone shredder'/'ULG', 'Ron Spencer').
card_number('bone shredder'/'ULG', '49').
card_multiverse_id('bone shredder'/'ULG', '12400').

card_in_set('bouncing beebles', 'ULG').
card_original_type('bouncing beebles'/'ULG', 'Summon — Beebles').
card_original_text('bouncing beebles'/'ULG', 'Bouncing Beebles is unblockable if defending player controls an artifact.').
card_first_print('bouncing beebles', 'ULG').
card_image_name('bouncing beebles'/'ULG', 'bouncing beebles').
card_uid('bouncing beebles'/'ULG', 'ULG:Bouncing Beebles:bouncing beebles').
card_rarity('bouncing beebles'/'ULG', 'Common').
card_artist('bouncing beebles'/'ULG', 'Jeff Miracola').
card_number('bouncing beebles'/'ULG', '28').
card_flavor_text('bouncing beebles'/'ULG', 'Beebles are frequently hurled against stone surfaces at high speed but always zing back into the air with a giggle.').
card_multiverse_id('bouncing beebles'/'ULG', '12372').

card_in_set('brink of madness', 'ULG').
card_original_type('brink of madness'/'ULG', 'Enchantment').
card_original_text('brink of madness'/'ULG', 'During your upkeep, if you have no cards in hand, sacrifice Brink of Madness and target opponent discards his or her hand.').
card_first_print('brink of madness', 'ULG').
card_image_name('brink of madness'/'ULG', 'brink of madness').
card_uid('brink of madness'/'ULG', 'ULG:Brink of Madness:brink of madness').
card_rarity('brink of madness'/'ULG', 'Rare').
card_artist('brink of madness'/'ULG', 'Donato Giancola').
card_number('brink of madness'/'ULG', '50').
card_flavor_text('brink of madness'/'ULG', '\"The fools thought me dead. But I built an empire inside my tomb.\"\n—Kerrick, sleeper agent').
card_multiverse_id('brink of madness'/'ULG', '12406').

card_in_set('burst of energy', 'ULG').
card_original_type('burst of energy'/'ULG', 'Instant').
card_original_text('burst of energy'/'ULG', 'Untap target permanent.').
card_first_print('burst of energy', 'ULG').
card_image_name('burst of energy'/'ULG', 'burst of energy').
card_uid('burst of energy'/'ULG', 'ULG:Burst of Energy:burst of energy').
card_rarity('burst of energy'/'ULG', 'Common').
card_artist('burst of energy'/'ULG', 'Mark Brill').
card_number('burst of energy'/'ULG', '3').
card_flavor_text('burst of energy'/'ULG', '\"I stand ready to die for our world. Who will stand with me?\"\n—Radiant, archangel').
card_multiverse_id('burst of energy'/'ULG', '13806').

card_in_set('cessation', 'ULG').
card_original_type('cessation'/'ULG', 'Enchant Creature').
card_original_text('cessation'/'ULG', 'Enchanted creature cannot attack.\nWhen Cessation is put into a graveyard from play, return Cessation to owner\'s hand.').
card_first_print('cessation', 'ULG').
card_image_name('cessation'/'ULG', 'cessation').
card_uid('cessation'/'ULG', 'ULG:Cessation:cessation').
card_rarity('cessation'/'ULG', 'Common').
card_artist('cessation'/'ULG', 'Mark Zug').
card_number('cessation'/'ULG', '4').
card_flavor_text('cessation'/'ULG', '\"The face of Light will shine upon them, and they will know war no more.\"\n—Song of All, canto 918').
card_multiverse_id('cessation'/'ULG', '12359').

card_in_set('cloud of faeries', 'ULG').
card_original_type('cloud of faeries'/'ULG', 'Summon — Faeries').
card_original_text('cloud of faeries'/'ULG', 'Flying\nWhen Cloud of Faeries comes into play, untap up to two lands.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('cloud of faeries', 'ULG').
card_image_name('cloud of faeries'/'ULG', 'cloud of faeries').
card_uid('cloud of faeries'/'ULG', 'ULG:Cloud of Faeries:cloud of faeries').
card_rarity('cloud of faeries'/'ULG', 'Common').
card_artist('cloud of faeries'/'ULG', 'Melissa A. Benson').
card_number('cloud of faeries'/'ULG', '29').
card_multiverse_id('cloud of faeries'/'ULG', '11588').

card_in_set('crawlspace', 'ULG').
card_original_type('crawlspace'/'ULG', 'Artifact').
card_original_text('crawlspace'/'ULG', 'No more than two creatures can attack you each combat.').
card_first_print('crawlspace', 'ULG').
card_image_name('crawlspace'/'ULG', 'crawlspace').
card_uid('crawlspace'/'ULG', 'ULG:Crawlspace:crawlspace').
card_rarity('crawlspace'/'ULG', 'Rare').
card_artist('crawlspace'/'ULG', 'Douglas Shuler').
card_number('crawlspace'/'ULG', '123').
card_flavor_text('crawlspace'/'ULG', 'The goblins did not understand their attraction to the mana rig. They simply knew it was where they belonged.').
card_multiverse_id('crawlspace'/'ULG', '12505').

card_in_set('crop rotation', 'ULG').
card_original_type('crop rotation'/'ULG', 'Instant').
card_original_text('crop rotation'/'ULG', 'At the time you play Crop Rotation, sacrifice a land.\nSearch your library for a land card and put that land into play. Shuffle your library afterward.').
card_first_print('crop rotation', 'ULG').
card_image_name('crop rotation'/'ULG', 'crop rotation').
card_uid('crop rotation'/'ULG', 'ULG:Crop Rotation:crop rotation').
card_rarity('crop rotation'/'ULG', 'Common').
card_artist('crop rotation'/'ULG', 'DiTerlizzi').
card_number('crop rotation'/'ULG', '98').
card_flavor_text('crop rotation'/'ULG', '\"Hmm . . . maybe lotuses this year.\"').
card_multiverse_id('crop rotation'/'ULG', '12432').

card_in_set('damping engine', 'ULG').
card_original_type('damping engine'/'ULG', 'Artifact').
card_original_text('damping engine'/'ULG', 'A player who controls more permanents than any other cannot play lands or artifact, creature, or enchantment spells. That player may sacrifice a permanent to ignore this effect until end of turn.').
card_first_print('damping engine', 'ULG').
card_image_name('damping engine'/'ULG', 'damping engine').
card_uid('damping engine'/'ULG', 'ULG:Damping Engine:damping engine').
card_rarity('damping engine'/'ULG', 'Rare').
card_artist('damping engine'/'ULG', 'rk post').
card_number('damping engine'/'ULG', '124').
card_multiverse_id('damping engine'/'ULG', '12503').

card_in_set('darkwatch elves', 'ULG').
card_original_type('darkwatch elves'/'ULG', 'Summon — Elves').
card_original_text('darkwatch elves'/'ULG', 'Protection from black\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('darkwatch elves', 'ULG').
card_image_name('darkwatch elves'/'ULG', 'darkwatch elves').
card_uid('darkwatch elves'/'ULG', 'ULG:Darkwatch Elves:darkwatch elves').
card_rarity('darkwatch elves'/'ULG', 'Uncommon').
card_artist('darkwatch elves'/'ULG', 'Hazeltine').
card_number('darkwatch elves'/'ULG', '99').
card_multiverse_id('darkwatch elves'/'ULG', '12435').

card_in_set('defender of chaos', 'ULG').
card_original_type('defender of chaos'/'ULG', 'Summon — Knight').
card_original_text('defender of chaos'/'ULG', 'Protection from white\nYou may play Defender of Chaos any time you could play an instant.').
card_first_print('defender of chaos', 'ULG').
card_image_name('defender of chaos'/'ULG', 'defender of chaos').
card_uid('defender of chaos'/'ULG', 'ULG:Defender of Chaos:defender of chaos').
card_rarity('defender of chaos'/'ULG', 'Common').
card_artist('defender of chaos'/'ULG', 'Carl Critchlow').
card_number('defender of chaos'/'ULG', '75').
card_flavor_text('defender of chaos'/'ULG', 'Some knights will not follow orders—only disorder.').
card_multiverse_id('defender of chaos'/'ULG', '12411').

card_in_set('defender of law', 'ULG').
card_original_type('defender of law'/'ULG', 'Summon — Knight').
card_original_text('defender of law'/'ULG', 'Protection from red\nYou may play Defender of Law any time you could play an instant.').
card_first_print('defender of law', 'ULG').
card_image_name('defender of law'/'ULG', 'defender of law').
card_uid('defender of law'/'ULG', 'ULG:Defender of Law:defender of law').
card_rarity('defender of law'/'ULG', 'Common').
card_artist('defender of law'/'ULG', 'Carl Critchlow').
card_number('defender of law'/'ULG', '5').
card_flavor_text('defender of law'/'ULG', '\"It is not my place to question Radiant\'s rule. I exist to enforce her will.\"').
card_multiverse_id('defender of law'/'ULG', '12353').

card_in_set('defense grid', 'ULG').
card_original_type('defense grid'/'ULG', 'Artifact').
card_original_text('defense grid'/'ULG', 'During each player\'s turn, spells played by another player cost an additional {3}.').
card_first_print('defense grid', 'ULG').
card_image_name('defense grid'/'ULG', 'defense grid').
card_uid('defense grid'/'ULG', 'ULG:Defense Grid:defense grid').
card_rarity('defense grid'/'ULG', 'Rare').
card_artist('defense grid'/'ULG', 'Mark Tedin').
card_number('defense grid'/'ULG', '125').
card_flavor_text('defense grid'/'ULG', '\"We\'ve managed to keep the goblins in their assigned areas, but there\'s little we can do about the smell.\"\n—Whip Tongue, viashino technician').
card_multiverse_id('defense grid'/'ULG', '8897').

card_in_set('defense of the heart', 'ULG').
card_original_type('defense of the heart'/'ULG', 'Enchantment').
card_original_text('defense of the heart'/'ULG', 'During your upkeep, if one of your opponents controls three or more creatures, sacrifice Defense of the Heart, search your library for up to two creature cards, and put those creatures into play. Shuffle your library afterward.').
card_first_print('defense of the heart', 'ULG').
card_image_name('defense of the heart'/'ULG', 'defense of the heart').
card_uid('defense of the heart'/'ULG', 'ULG:Defense of the Heart:defense of the heart').
card_rarity('defense of the heart'/'ULG', 'Rare').
card_artist('defense of the heart'/'ULG', 'Rebecca Guay').
card_number('defense of the heart'/'ULG', '100').
card_multiverse_id('defense of the heart'/'ULG', '12442').

card_in_set('delusions of mediocrity', 'ULG').
card_original_type('delusions of mediocrity'/'ULG', 'Enchantment').
card_original_text('delusions of mediocrity'/'ULG', 'When Delusions of Mediocrity comes into play, gain 10 life.\nWhen Delusions of Mediocrity leaves play, lose 10 life.').
card_first_print('delusions of mediocrity', 'ULG').
card_image_name('delusions of mediocrity'/'ULG', 'delusions of mediocrity').
card_uid('delusions of mediocrity'/'ULG', 'ULG:Delusions of Mediocrity:delusions of mediocrity').
card_rarity('delusions of mediocrity'/'ULG', 'Rare').
card_artist('delusions of mediocrity'/'ULG', 'Jeff Laubenstein').
card_number('delusions of mediocrity'/'ULG', '30').
card_flavor_text('delusions of mediocrity'/'ULG', 'When nothing but second best will do.').
card_multiverse_id('delusions of mediocrity'/'ULG', '12349').

card_in_set('deranged hermit', 'ULG').
card_original_type('deranged hermit'/'ULG', 'Summon — Elf').
card_original_text('deranged hermit'/'ULG', 'Echo (During your next upkeep after this permanent comes under your control, pay its casting cost or sacrifice it.)\nWhen Deranged Hermit comes into play, put four Squirrel tokens into play. Treat these tokens as 1/1 green creatures.\nAll Squirrels get +1/+1.').
card_image_name('deranged hermit'/'ULG', 'deranged hermit').
card_uid('deranged hermit'/'ULG', 'ULG:Deranged Hermit:deranged hermit').
card_rarity('deranged hermit'/'ULG', 'Rare').
card_artist('deranged hermit'/'ULG', 'Kev Walker').
card_number('deranged hermit'/'ULG', '101').
card_multiverse_id('deranged hermit'/'ULG', '12458').

card_in_set('devout harpist', 'ULG').
card_original_type('devout harpist'/'ULG', 'Summon — Townsfolk').
card_original_text('devout harpist'/'ULG', '{T}: Destroy target creature enchantment.').
card_first_print('devout harpist', 'ULG').
card_image_name('devout harpist'/'ULG', 'devout harpist').
card_uid('devout harpist'/'ULG', 'ULG:Devout Harpist:devout harpist').
card_rarity('devout harpist'/'ULG', 'Common').
card_artist('devout harpist'/'ULG', 'Rebecca Guay').
card_number('devout harpist'/'ULG', '6').
card_flavor_text('devout harpist'/'ULG', 'The notes themselves are irrelevant. The music\'s effect, however, is worth ten armies.').
card_multiverse_id('devout harpist'/'ULG', '12813').

card_in_set('engineered plague', 'ULG').
card_original_type('engineered plague'/'ULG', 'Enchantment').
card_original_text('engineered plague'/'ULG', 'When Engineered Plague comes into play, choose a creature type.\nAll creatures of the chosen type get -1/-1.').
card_first_print('engineered plague', 'ULG').
card_image_name('engineered plague'/'ULG', 'engineered plague').
card_uid('engineered plague'/'ULG', 'ULG:Engineered Plague:engineered plague').
card_rarity('engineered plague'/'ULG', 'Uncommon').
card_artist('engineered plague'/'ULG', 'Michael Sutfin').
card_number('engineered plague'/'ULG', '51').
card_flavor_text('engineered plague'/'ULG', '\"The admixture of bitterwort in the viral brew has produced most favorable results.\"\n—Phyrexian progress notes').
card_multiverse_id('engineered plague'/'ULG', '12944').

card_in_set('erase', 'ULG').
card_original_type('erase'/'ULG', 'Instant').
card_original_text('erase'/'ULG', 'Remove target enchantment from the game.').
card_first_print('erase', 'ULG').
card_image_name('erase'/'ULG', 'erase').
card_uid('erase'/'ULG', 'ULG:Erase:erase').
card_rarity('erase'/'ULG', 'Common').
card_artist('erase'/'ULG', 'Ron Spears').
card_number('erase'/'ULG', '7').
card_flavor_text('erase'/'ULG', 'Perception is more pleasing than truth.').
card_multiverse_id('erase'/'ULG', '12941').

card_in_set('eviscerator', 'ULG').
card_original_type('eviscerator'/'ULG', 'Summon — Horror').
card_original_text('eviscerator'/'ULG', 'Protection from white\nWhen Eviscerator comes into play, lose 5 life.').
card_first_print('eviscerator', 'ULG').
card_image_name('eviscerator'/'ULG', 'eviscerator').
card_uid('eviscerator'/'ULG', 'ULG:Eviscerator:eviscerator').
card_rarity('eviscerator'/'ULG', 'Rare').
card_artist('eviscerator'/'ULG', 'Michael Sutfin').
card_number('eviscerator'/'ULG', '52').
card_flavor_text('eviscerator'/'ULG', 'It roamed the time bubble like a captured animal. Kerrick knew he must soon find a way to unleash it on Tolaria or destroy it.').
card_multiverse_id('eviscerator'/'ULG', '12404').

card_in_set('expendable troops', 'ULG').
card_original_type('expendable troops'/'ULG', 'Summon — Soldiers').
card_original_text('expendable troops'/'ULG', '{T}, Sacrifice Expendable Troops: Expendable Troops deals 2 damage to target attacking or blocking creature.').
card_first_print('expendable troops', 'ULG').
card_image_name('expendable troops'/'ULG', 'expendable troops').
card_uid('expendable troops'/'ULG', 'ULG:Expendable Troops:expendable troops').
card_rarity('expendable troops'/'ULG', 'Common').
card_artist('expendable troops'/'ULG', 'Carl Critchlow').
card_number('expendable troops'/'ULG', '8').
card_flavor_text('expendable troops'/'ULG', 'No doubt in their minds, no fear in their hearts.').
card_multiverse_id('expendable troops'/'ULG', '12356').

card_in_set('faerie conclave', 'ULG').
card_original_type('faerie conclave'/'ULG', 'Land').
card_original_text('faerie conclave'/'ULG', 'Faerie Conclave comes into play tapped.\n{T}: Add one blue mana to your mana pool.\n{1}{U} Faerie Conclave becomes a 2/1 blue creature with flying until end of turn. This creature still counts as a land.').
card_first_print('faerie conclave', 'ULG').
card_image_name('faerie conclave'/'ULG', 'faerie conclave').
card_uid('faerie conclave'/'ULG', 'ULG:Faerie Conclave:faerie conclave').
card_rarity('faerie conclave'/'ULG', 'Uncommon').
card_artist('faerie conclave'/'ULG', 'Val Mayerik').
card_number('faerie conclave'/'ULG', '139').
card_multiverse_id('faerie conclave'/'ULG', '12495').

card_in_set('fleeting image', 'ULG').
card_original_type('fleeting image'/'ULG', 'Summon — Illusion').
card_original_text('fleeting image'/'ULG', 'Flying\n{1}{U} Return Fleeting Image to owner\'s hand.').
card_first_print('fleeting image', 'ULG').
card_image_name('fleeting image'/'ULG', 'fleeting image').
card_uid('fleeting image'/'ULG', 'ULG:Fleeting Image:fleeting image').
card_rarity('fleeting image'/'ULG', 'Rare').
card_artist('fleeting image'/'ULG', 'Scott M. Fischer').
card_number('fleeting image'/'ULG', '31').
card_flavor_text('fleeting image'/'ULG', 'Horas swore off drinking, but after meeting the creature a second time he decided he\'d better start again.').
card_multiverse_id('fleeting image'/'ULG', '12389').

card_in_set('fog of gnats', 'ULG').
card_original_type('fog of gnats'/'ULG', 'Summon — Insects').
card_original_text('fog of gnats'/'ULG', 'Flying\n{B} Regenerate Fog of Gnats.').
card_first_print('fog of gnats', 'ULG').
card_image_name('fog of gnats'/'ULG', 'fog of gnats').
card_uid('fog of gnats'/'ULG', 'ULG:Fog of Gnats:fog of gnats').
card_rarity('fog of gnats'/'ULG', 'Common').
card_artist('fog of gnats'/'ULG', 'Jeff Miracola').
card_number('fog of gnats'/'ULG', '53').
card_flavor_text('fog of gnats'/'ULG', 'You can swat a thousand gnats, but a thousand more will assail you.').
card_multiverse_id('fog of gnats'/'ULG', '9699').

card_in_set('forbidding watchtower', 'ULG').
card_original_type('forbidding watchtower'/'ULG', 'Land').
card_original_text('forbidding watchtower'/'ULG', 'Forbidding Watchtower comes into play tapped.\n{T}: Add one white mana to your mana pool.\n{1}{W} Forbidding Watchtower becomes a 1/5 white creature until end of turn. This creature still counts as a land.').
card_first_print('forbidding watchtower', 'ULG').
card_image_name('forbidding watchtower'/'ULG', 'forbidding watchtower').
card_uid('forbidding watchtower'/'ULG', 'ULG:Forbidding Watchtower:forbidding watchtower').
card_rarity('forbidding watchtower'/'ULG', 'Uncommon').
card_artist('forbidding watchtower'/'ULG', 'Mark Brill').
card_number('forbidding watchtower'/'ULG', '140').
card_multiverse_id('forbidding watchtower'/'ULG', '12494').

card_in_set('frantic search', 'ULG').
card_original_type('frantic search'/'ULG', 'Instant').
card_original_text('frantic search'/'ULG', 'Draw two cards, then choose and discard two cards. Untap up to three lands.').
card_first_print('frantic search', 'ULG').
card_image_name('frantic search'/'ULG', 'frantic search').
card_uid('frantic search'/'ULG', 'ULG:Frantic Search:frantic search').
card_rarity('frantic search'/'ULG', 'Common').
card_artist('frantic search'/'ULG', 'Jeff Miracola').
card_number('frantic search'/'ULG', '32').
card_flavor_text('frantic search'/'ULG', 'Motivation was high in the academy once students realized flunking their exams could kill them.').
card_multiverse_id('frantic search'/'ULG', '12375').

card_in_set('gang of elk', 'ULG').
card_original_type('gang of elk'/'ULG', 'Summon — Beasts').
card_original_text('gang of elk'/'ULG', 'Whenever a creature blocks it, Gang of Elk gets +2/+2 until end of turn.').
card_first_print('gang of elk', 'ULG').
card_image_name('gang of elk'/'ULG', 'gang of elk').
card_uid('gang of elk'/'ULG', 'ULG:Gang of Elk:gang of elk').
card_rarity('gang of elk'/'ULG', 'Uncommon').
card_artist('gang of elk'/'ULG', 'Una Fricker').
card_number('gang of elk'/'ULG', '102').
card_flavor_text('gang of elk'/'ULG', 'The elk is Gaea\'s favorite, who wears the forest on its brow.').
card_multiverse_id('gang of elk'/'ULG', '12950').

card_in_set('ghitu encampment', 'ULG').
card_original_type('ghitu encampment'/'ULG', 'Land').
card_original_text('ghitu encampment'/'ULG', 'Ghitu Encampment comes into play tapped.\n{T}: Add one red mana to your mana pool.\n{1}{R} Ghitu Encampment becomes a 2/1 red creature with first strike until end of turn. This creature still counts as a land.').
card_first_print('ghitu encampment', 'ULG').
card_image_name('ghitu encampment'/'ULG', 'ghitu encampment').
card_uid('ghitu encampment'/'ULG', 'ULG:Ghitu Encampment:ghitu encampment').
card_rarity('ghitu encampment'/'ULG', 'Uncommon').
card_artist('ghitu encampment'/'ULG', 'Hazeltine').
card_number('ghitu encampment'/'ULG', '141').
card_multiverse_id('ghitu encampment'/'ULG', '12497').

card_in_set('ghitu fire-eater', 'ULG').
card_original_type('ghitu fire-eater'/'ULG', 'Summon — Nomad').
card_original_text('ghitu fire-eater'/'ULG', '{T}, Sacrifice Ghitu Fire-Eater: Ghitu Fire-Eater deals damage equal to its power to target creature or player.').
card_first_print('ghitu fire-eater', 'ULG').
card_image_name('ghitu fire-eater'/'ULG', 'ghitu fire-eater').
card_uid('ghitu fire-eater'/'ULG', 'ULG:Ghitu Fire-Eater:ghitu fire-eater').
card_rarity('ghitu fire-eater'/'ULG', 'Uncommon').
card_artist('ghitu fire-eater'/'ULG', 'Melissa A. Benson').
card_number('ghitu fire-eater'/'ULG', '76').
card_flavor_text('ghitu fire-eater'/'ULG', '\"They are called ‘Mi\'uto,\' which means ‘one use.\'\"\n—Jhoira, artificer').
card_multiverse_id('ghitu fire-eater'/'ULG', '12815').

card_in_set('ghitu slinger', 'ULG').
card_original_type('ghitu slinger'/'ULG', 'Summon — Nomad').
card_original_text('ghitu slinger'/'ULG', 'Echo (During your next upkeep after this permanent comes under your control, pay its casting cost or sacrifice it.)\nWhen Ghitu Slinger comes into play, it deals 2 damage to target creature or player.').
card_first_print('ghitu slinger', 'ULG').
card_image_name('ghitu slinger'/'ULG', 'ghitu slinger').
card_uid('ghitu slinger'/'ULG', 'ULG:Ghitu Slinger:ghitu slinger').
card_rarity('ghitu slinger'/'ULG', 'Common').
card_artist('ghitu slinger'/'ULG', 'Melissa A. Benson').
card_number('ghitu slinger'/'ULG', '77').
card_flavor_text('ghitu slinger'/'ULG', 'If you survive a slinger attack, the slinger wished you to live.').
card_multiverse_id('ghitu slinger'/'ULG', '12475').

card_in_set('ghitu war cry', 'ULG').
card_original_type('ghitu war cry'/'ULG', 'Enchantment').
card_original_text('ghitu war cry'/'ULG', '{R} Target creature gets +1/+0 until end of turn.').
card_first_print('ghitu war cry', 'ULG').
card_image_name('ghitu war cry'/'ULG', 'ghitu war cry').
card_uid('ghitu war cry'/'ULG', 'ULG:Ghitu War Cry:ghitu war cry').
card_rarity('ghitu war cry'/'ULG', 'Uncommon').
card_artist('ghitu war cry'/'ULG', 'Douglas Shuler').
card_number('ghitu war cry'/'ULG', '78').
card_flavor_text('ghitu war cry'/'ULG', 'The war cry is not simply a shout but a sacrament.').
card_multiverse_id('ghitu war cry'/'ULG', '13810').

card_in_set('giant cockroach', 'ULG').
card_original_type('giant cockroach'/'ULG', 'Summon — Insect').
card_original_text('giant cockroach'/'ULG', '').
card_first_print('giant cockroach', 'ULG').
card_image_name('giant cockroach'/'ULG', 'giant cockroach').
card_uid('giant cockroach'/'ULG', 'ULG:Giant Cockroach:giant cockroach').
card_rarity('giant cockroach'/'ULG', 'Common').
card_artist('giant cockroach'/'ULG', 'Heather Hudson').
card_number('giant cockroach'/'ULG', '54').
card_flavor_text('giant cockroach'/'ULG', 'If the sun ever hit the swamp, where would these scurry to?').
card_multiverse_id('giant cockroach'/'ULG', '12946').

card_in_set('goblin medics', 'ULG').
card_original_type('goblin medics'/'ULG', 'Summon — Goblins').
card_original_text('goblin medics'/'ULG', 'Whenever Goblin Medics becomes tapped, it deals 1 damage to target creature or player.').
card_first_print('goblin medics', 'ULG').
card_image_name('goblin medics'/'ULG', 'goblin medics').
card_uid('goblin medics'/'ULG', 'ULG:Goblin Medics:goblin medics').
card_rarity('goblin medics'/'ULG', 'Common').
card_artist('goblin medics'/'ULG', 'Jeff Laubenstein').
card_number('goblin medics'/'ULG', '79').
card_flavor_text('goblin medics'/'ULG', '\"First, do some harm.\"\n—Oath of Goblins').
card_multiverse_id('goblin medics'/'ULG', '12643').

card_in_set('goblin welder', 'ULG').
card_original_type('goblin welder'/'ULG', 'Summon — Goblin').
card_original_text('goblin welder'/'ULG', '{T}: Exchange target artifact a player controls for target artifact card in that player\'s graveyard.').
card_image_name('goblin welder'/'ULG', 'goblin welder').
card_uid('goblin welder'/'ULG', 'ULG:Goblin Welder:goblin welder').
card_rarity('goblin welder'/'ULG', 'Rare').
card_artist('goblin welder'/'ULG', 'Scott M. Fischer').
card_number('goblin welder'/'ULG', '80').
card_flavor_text('goblin welder'/'ULG', '\"I wrecked your metal guy, boss. But look! I made you an ashtray.\"').
card_multiverse_id('goblin welder'/'ULG', '13001').

card_in_set('granite grip', 'ULG').
card_original_type('granite grip'/'ULG', 'Enchant Creature').
card_original_text('granite grip'/'ULG', 'Enchanted creature gets +1/+0 for each mountain you control.').
card_first_print('granite grip', 'ULG').
card_image_name('granite grip'/'ULG', 'granite grip').
card_uid('granite grip'/'ULG', 'ULG:Granite Grip:granite grip').
card_rarity('granite grip'/'ULG', 'Common').
card_artist('granite grip'/'ULG', 'Mike Raabe').
card_number('granite grip'/'ULG', '81').
card_flavor_text('granite grip'/'ULG', 'There\'s beauty in the desert—but it\'s best to view it from afar.').
card_multiverse_id('granite grip'/'ULG', '5682').

card_in_set('grim monolith', 'ULG').
card_original_type('grim monolith'/'ULG', 'Artifact').
card_original_text('grim monolith'/'ULG', 'Grim Monolith does not untap during your untap phase.\n{T}: Add three colorless mana to your mana pool. Play this ability as a mana source.\n{4}: Untap Grim Monolith.').
card_first_print('grim monolith', 'ULG').
card_image_name('grim monolith'/'ULG', 'grim monolith').
card_uid('grim monolith'/'ULG', 'ULG:Grim Monolith:grim monolith').
card_rarity('grim monolith'/'ULG', 'Rare').
card_artist('grim monolith'/'ULG', 'Chippy').
card_number('grim monolith'/'ULG', '126').
card_flavor_text('grim monolith'/'ULG', 'Part prison, part home.').
card_multiverse_id('grim monolith'/'ULG', '12626').

card_in_set('harmonic convergence', 'ULG').
card_original_type('harmonic convergence'/'ULG', 'Instant').
card_original_text('harmonic convergence'/'ULG', 'Return all enchantments to top of owners\' libraries.').
card_first_print('harmonic convergence', 'ULG').
card_image_name('harmonic convergence'/'ULG', 'harmonic convergence').
card_uid('harmonic convergence'/'ULG', 'ULG:Harmonic Convergence:harmonic convergence').
card_rarity('harmonic convergence'/'ULG', 'Uncommon').
card_artist('harmonic convergence'/'ULG', 'John Avon').
card_number('harmonic convergence'/'ULG', '103').
card_flavor_text('harmonic convergence'/'ULG', 'When the eternal stars align, can mere mortals resist?').
card_multiverse_id('harmonic convergence'/'ULG', '12437').

card_in_set('hidden gibbons', 'ULG').
card_original_type('hidden gibbons'/'ULG', 'Enchantment').
card_original_text('hidden gibbons'/'ULG', 'When one of your opponents successfully casts an instant or interrupt spell, if Hidden Gibbons is an enchantment, Hidden Gibbons becomes a 4/4 creature that counts as an Ape.').
card_first_print('hidden gibbons', 'ULG').
card_image_name('hidden gibbons'/'ULG', 'hidden gibbons').
card_uid('hidden gibbons'/'ULG', 'ULG:Hidden Gibbons:hidden gibbons').
card_rarity('hidden gibbons'/'ULG', 'Rare').
card_artist('hidden gibbons'/'ULG', 'Una Fricker').
card_number('hidden gibbons'/'ULG', '104').
card_flavor_text('hidden gibbons'/'ULG', 'When these apes want something, it\'s a matter of gibbon take.').
card_multiverse_id('hidden gibbons'/'ULG', '12441').

card_in_set('hope and glory', 'ULG').
card_original_type('hope and glory'/'ULG', 'Instant').
card_original_text('hope and glory'/'ULG', 'Untap two target creatures. Each of them gets +1/+1 until end of turn.').
card_first_print('hope and glory', 'ULG').
card_image_name('hope and glory'/'ULG', 'hope and glory').
card_uid('hope and glory'/'ULG', 'ULG:Hope and Glory:hope and glory').
card_rarity('hope and glory'/'ULG', 'Uncommon').
card_artist('hope and glory'/'ULG', 'Heather Hudson').
card_number('hope and glory'/'ULG', '9').
card_flavor_text('hope and glory'/'ULG', '\"Serra ruled by faith. I cannot afford that luxury.\"\n—Radiant, archangel').
card_multiverse_id('hope and glory'/'ULG', '12942').

card_in_set('impending disaster', 'ULG').
card_original_type('impending disaster'/'ULG', 'Enchantment').
card_original_text('impending disaster'/'ULG', 'During your upkeep, if there are seven or more lands in play, sacrifice Impending Disaster and destroy all lands.').
card_first_print('impending disaster', 'ULG').
card_image_name('impending disaster'/'ULG', 'impending disaster').
card_uid('impending disaster'/'ULG', 'ULG:Impending Disaster:impending disaster').
card_rarity('impending disaster'/'ULG', 'Rare').
card_artist('impending disaster'/'ULG', 'Pete Venters').
card_number('impending disaster'/'ULG', '82').
card_flavor_text('impending disaster'/'ULG', '\"The goblins are in charge of maintenance? Why not just set it on fire now and call it a day?\"\n—Whip Tongue, viashino technician').
card_multiverse_id('impending disaster'/'ULG', '12426').

card_in_set('intervene', 'ULG').
card_original_type('intervene'/'ULG', 'Interrupt').
card_original_text('intervene'/'ULG', 'Counter target spell that targets a creature.').
card_first_print('intervene', 'ULG').
card_image_name('intervene'/'ULG', 'intervene').
card_uid('intervene'/'ULG', 'ULG:Intervene:intervene').
card_rarity('intervene'/'ULG', 'Common').
card_artist('intervene'/'ULG', 'Heather Hudson').
card_number('intervene'/'ULG', '33').
card_flavor_text('intervene'/'ULG', '\"At first I simply observed. But I found that without investment in others, life serves no purpose.\"\n—Karn, silver golem').
card_multiverse_id('intervene'/'ULG', '12378').

card_in_set('iron maiden', 'ULG').
card_original_type('iron maiden'/'ULG', 'Artifact').
card_original_text('iron maiden'/'ULG', 'During each of your opponent\'s upkeeps, Iron Maiden deals 1 damage to that player for each card more than four in his or her hand.').
card_first_print('iron maiden', 'ULG').
card_image_name('iron maiden'/'ULG', 'iron maiden').
card_uid('iron maiden'/'ULG', 'ULG:Iron Maiden:iron maiden').
card_rarity('iron maiden'/'ULG', 'Rare').
card_artist('iron maiden'/'ULG', 'Tom Wänerstrand').
card_number('iron maiden'/'ULG', '127').
card_flavor_text('iron maiden'/'ULG', 'The maiden is a jealous lover.').
card_multiverse_id('iron maiden'/'ULG', '9705').

card_in_set('iron will', 'ULG').
card_original_type('iron will'/'ULG', 'Instant').
card_original_text('iron will'/'ULG', 'Target creature gets +0/+4 until end of turn.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('iron will', 'ULG').
card_image_name('iron will'/'ULG', 'iron will').
card_uid('iron will'/'ULG', 'ULG:Iron Will:iron will').
card_rarity('iron will'/'ULG', 'Common').
card_artist('iron will'/'ULG', 'Val Mayerik').
card_number('iron will'/'ULG', '10').
card_multiverse_id('iron will'/'ULG', '12358').

card_in_set('jhoira\'s toolbox', 'ULG').
card_original_type('jhoira\'s toolbox'/'ULG', 'Artifact Creature').
card_original_text('jhoira\'s toolbox'/'ULG', '{2}: Regenerate target artifact creature.').
card_first_print('jhoira\'s toolbox', 'ULG').
card_image_name('jhoira\'s toolbox'/'ULG', 'jhoira\'s toolbox').
card_uid('jhoira\'s toolbox'/'ULG', 'ULG:Jhoira\'s Toolbox:jhoira\'s toolbox').
card_rarity('jhoira\'s toolbox'/'ULG', 'Uncommon').
card_artist('jhoira\'s toolbox'/'ULG', 'Mike Raabe').
card_number('jhoira\'s toolbox'/'ULG', '128').
card_flavor_text('jhoira\'s toolbox'/'ULG', 'It entertained Jhoira to craft a kit that could bring her the tools.').
card_multiverse_id('jhoira\'s toolbox'/'ULG', '8896').

card_in_set('karmic guide', 'ULG').
card_original_type('karmic guide'/'ULG', 'Summon — Spirit').
card_original_text('karmic guide'/'ULG', 'Flying, protection from black; echo (During your next upkeep after this permanent comes under your control, pay its casting cost or sacrifice it.)\nWhen Karmic Guide comes into play, choose target creature card in your graveyard and put that creature into play.').
card_image_name('karmic guide'/'ULG', 'karmic guide').
card_uid('karmic guide'/'ULG', 'ULG:Karmic Guide:karmic guide').
card_rarity('karmic guide'/'ULG', 'Rare').
card_artist('karmic guide'/'ULG', 'Heather Hudson').
card_number('karmic guide'/'ULG', '11').
card_multiverse_id('karmic guide'/'ULG', '12955').

card_in_set('king crab', 'ULG').
card_original_type('king crab'/'ULG', 'Summon — Crab').
card_original_text('king crab'/'ULG', '{1}{U}, {T}: Put target green creature on top of owner\'s library.').
card_first_print('king crab', 'ULG').
card_image_name('king crab'/'ULG', 'king crab').
card_uid('king crab'/'ULG', 'ULG:King Crab:king crab').
card_rarity('king crab'/'ULG', 'Uncommon').
card_artist('king crab'/'ULG', 'Daniel Gelon').
card_number('king crab'/'ULG', '34').
card_flavor_text('king crab'/'ULG', '\"I\'m allergic to shellfish—not only did it kill my entire village, it also gave me this nasty rash.\"\n—Olivia, fisher').
card_multiverse_id('king crab'/'ULG', '12382').

card_in_set('knighthood', 'ULG').
card_original_type('knighthood'/'ULG', 'Enchantment').
card_original_text('knighthood'/'ULG', 'All creatures you control gain first strike.').
card_first_print('knighthood', 'ULG').
card_image_name('knighthood'/'ULG', 'knighthood').
card_uid('knighthood'/'ULG', 'ULG:Knighthood:knighthood').
card_rarity('knighthood'/'ULG', 'Uncommon').
card_artist('knighthood'/'ULG', 'Kev Walker').
card_number('knighthood'/'ULG', '12').
card_flavor_text('knighthood'/'ULG', '\"He has returned. He who brought the dark ones. He who poisoned our paradise. How shall we greet him? With swift and certain death!\"\n—Radiant, archangel').
card_multiverse_id('knighthood'/'ULG', '5709').

card_in_set('last-ditch effort', 'ULG').
card_original_type('last-ditch effort'/'ULG', 'Instant').
card_original_text('last-ditch effort'/'ULG', 'Sacrifice X creatures. Last-Ditch Effort deals X damage to target creature or player.').
card_first_print('last-ditch effort', 'ULG').
card_image_name('last-ditch effort'/'ULG', 'last-ditch effort').
card_uid('last-ditch effort'/'ULG', 'ULG:Last-Ditch Effort:last-ditch effort').
card_rarity('last-ditch effort'/'ULG', 'Uncommon').
card_artist('last-ditch effort'/'ULG', 'Dan Frazier').
card_number('last-ditch effort'/'ULG', '83').
card_flavor_text('last-ditch effort'/'ULG', '\"If you\'re gonna lose, at least make sure they don\'t win as much.\"\n—Jula, goblin raider').
card_multiverse_id('last-ditch effort'/'ULG', '13812').

card_in_set('lava axe', 'ULG').
card_original_type('lava axe'/'ULG', 'Sorcery').
card_original_text('lava axe'/'ULG', 'Lava Axe deals 5 damage to target player.').
card_image_name('lava axe'/'ULG', 'lava axe').
card_uid('lava axe'/'ULG', 'ULG:Lava Axe:lava axe').
card_rarity('lava axe'/'ULG', 'Common').
card_artist('lava axe'/'ULG', 'Brian Snõddy').
card_number('lava axe'/'ULG', '84').
card_flavor_text('lava axe'/'ULG', '\"Catch!\"').
card_multiverse_id('lava axe'/'ULG', '12947').

card_in_set('levitation', 'ULG').
card_original_type('levitation'/'ULG', 'Enchantment').
card_original_text('levitation'/'ULG', 'All creatures you control gain flying.').
card_first_print('levitation', 'ULG').
card_image_name('levitation'/'ULG', 'levitation').
card_uid('levitation'/'ULG', 'ULG:Levitation:levitation').
card_rarity('levitation'/'ULG', 'Uncommon').
card_artist('levitation'/'ULG', 'Heather Hudson').
card_number('levitation'/'ULG', '35').
card_flavor_text('levitation'/'ULG', 'Barrin\'s pride in his apprentice was diminished somewhat when he had to get the others back down.').
card_multiverse_id('levitation'/'ULG', '12388').

card_in_set('lone wolf', 'ULG').
card_original_type('lone wolf'/'ULG', 'Summon — Wolf').
card_original_text('lone wolf'/'ULG', 'You may have Lone Wolf deal combat damage to defending player instead of to creatures blocking it.').
card_image_name('lone wolf'/'ULG', 'lone wolf').
card_uid('lone wolf'/'ULG', 'ULG:Lone Wolf:lone wolf').
card_rarity('lone wolf'/'ULG', 'Uncommon').
card_artist('lone wolf'/'ULG', 'Una Fricker').
card_number('lone wolf'/'ULG', '105').
card_flavor_text('lone wolf'/'ULG', 'A wolf without a pack is either a survivor or a brute.').
card_multiverse_id('lone wolf'/'ULG', '12436').

card_in_set('lurking skirge', 'ULG').
card_original_type('lurking skirge'/'ULG', 'Enchantment').
card_original_text('lurking skirge'/'ULG', 'When a creature is put into one of your opponents\' graveyards, if Lurking Skirge is an enchantment, Lurking Skirge becomes a 3/2 creature with flying that counts as an Imp.').
card_first_print('lurking skirge', 'ULG').
card_image_name('lurking skirge'/'ULG', 'lurking skirge').
card_uid('lurking skirge'/'ULG', 'ULG:Lurking Skirge:lurking skirge').
card_rarity('lurking skirge'/'ULG', 'Rare').
card_artist('lurking skirge'/'ULG', 'Daren Bader').
card_number('lurking skirge'/'ULG', '55').
card_flavor_text('lurking skirge'/'ULG', 'They never miss a funeral.').
card_multiverse_id('lurking skirge'/'ULG', '12386').

card_in_set('martyr\'s cause', 'ULG').
card_original_type('martyr\'s cause'/'ULG', 'Enchantment').
card_original_text('martyr\'s cause'/'ULG', 'Sacrifice a creature: Prevent all damage to a creature or player from one source. (Treat further damage from that source normally.)').
card_first_print('martyr\'s cause', 'ULG').
card_image_name('martyr\'s cause'/'ULG', 'martyr\'s cause').
card_uid('martyr\'s cause'/'ULG', 'ULG:Martyr\'s Cause:martyr\'s cause').
card_rarity('martyr\'s cause'/'ULG', 'Uncommon').
card_artist('martyr\'s cause'/'ULG', 'Jeff Laubenstein').
card_number('martyr\'s cause'/'ULG', '13').
card_flavor_text('martyr\'s cause'/'ULG', 'Dying is a soldier\'s talent.').
card_multiverse_id('martyr\'s cause'/'ULG', '9712').

card_in_set('memory jar', 'ULG').
card_original_type('memory jar'/'ULG', 'Artifact').
card_original_text('memory jar'/'ULG', '{T}, Sacrifice Memory Jar: Each player sets aside his or her hand, face down, and draws seven cards. At end of turn, each player discards his or her hand and returns to his or her hand each card he or she set aside this way.').
card_first_print('memory jar', 'ULG').
card_image_name('memory jar'/'ULG', 'memory jar').
card_uid('memory jar'/'ULG', 'ULG:Memory Jar:memory jar').
card_rarity('memory jar'/'ULG', 'Rare').
card_artist('memory jar'/'ULG', 'Donato Giancola').
card_number('memory jar'/'ULG', '129').
card_multiverse_id('memory jar'/'ULG', '8841').

card_in_set('might of oaks', 'ULG').
card_original_type('might of oaks'/'ULG', 'Instant').
card_original_text('might of oaks'/'ULG', 'Target creature gets +7/+7 until end of turn.').
card_first_print('might of oaks', 'ULG').
card_image_name('might of oaks'/'ULG', 'might of oaks').
card_uid('might of oaks'/'ULG', 'ULG:Might of Oaks:might of oaks').
card_rarity('might of oaks'/'ULG', 'Rare').
card_artist('might of oaks'/'ULG', 'Ron Spencer').
card_number('might of oaks'/'ULG', '106').
card_flavor_text('might of oaks'/'ULG', 'Suddenly, she couldn\'t see the acorns for the trees.').
card_multiverse_id('might of oaks'/'ULG', '8822').

card_in_set('miscalculation', 'ULG').
card_original_type('miscalculation'/'ULG', 'Interrupt').
card_original_text('miscalculation'/'ULG', 'Counter target spell unless its caster pays an additional {2}.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('miscalculation', 'ULG').
card_image_name('miscalculation'/'ULG', 'miscalculation').
card_uid('miscalculation'/'ULG', 'ULG:Miscalculation:miscalculation').
card_rarity('miscalculation'/'ULG', 'Common').
card_artist('miscalculation'/'ULG', 'Jeff Laubenstein').
card_number('miscalculation'/'ULG', '36').
card_multiverse_id('miscalculation'/'ULG', '12376').

card_in_set('molten hydra', 'ULG').
card_original_type('molten hydra'/'ULG', 'Summon — Hydra').
card_original_text('molten hydra'/'ULG', '{1}{R}{R} Put a +1/+1 counter on Molten Hydra.\n{T}, Remove all +1/+1 counters from Molten Hydra: Molten Hydra deals 1 damage to target creature or player for each +1/+1 counter removed this way.').
card_first_print('molten hydra', 'ULG').
card_image_name('molten hydra'/'ULG', 'molten hydra').
card_uid('molten hydra'/'ULG', 'ULG:Molten Hydra:molten hydra').
card_rarity('molten hydra'/'ULG', 'Rare').
card_artist('molten hydra'/'ULG', 'Greg Staples').
card_number('molten hydra'/'ULG', '85').
card_flavor_text('molten hydra'/'ULG', 'Keep off the glass. —Hydra warning sign').
card_multiverse_id('molten hydra'/'ULG', '9689').

card_in_set('mother of runes', 'ULG').
card_original_type('mother of runes'/'ULG', 'Summon — Cleric').
card_original_text('mother of runes'/'ULG', '{T}: Target creature you control gains protection from a color of your choice until end of turn.').
card_first_print('mother of runes', 'ULG').
card_image_name('mother of runes'/'ULG', 'mother of runes').
card_uid('mother of runes'/'ULG', 'ULG:Mother of Runes:mother of runes').
card_rarity('mother of runes'/'ULG', 'Uncommon').
card_artist('mother of runes'/'ULG', 'Scott M. Fischer').
card_number('mother of runes'/'ULG', '14').
card_flavor_text('mother of runes'/'ULG', '\"My family protects all families.\"').
card_multiverse_id('mother of runes'/'ULG', '5704').

card_in_set('multani\'s acolyte', 'ULG').
card_original_type('multani\'s acolyte'/'ULG', 'Summon — Elf').
card_original_text('multani\'s acolyte'/'ULG', 'Echo (During your next upkeep after this permanent comes under your control, pay its casting cost or sacrifice it.)\nWhen Multani\'s Acolyte comes into play, draw a card.').
card_first_print('multani\'s acolyte', 'ULG').
card_image_name('multani\'s acolyte'/'ULG', 'multani\'s acolyte').
card_uid('multani\'s acolyte'/'ULG', 'ULG:Multani\'s Acolyte:multani\'s acolyte').
card_rarity('multani\'s acolyte'/'ULG', 'Common').
card_artist('multani\'s acolyte'/'ULG', 'Edward P. Beard, Jr.').
card_number('multani\'s acolyte'/'ULG', '108').
card_multiverse_id('multani\'s acolyte'/'ULG', '12427').

card_in_set('multani\'s presence', 'ULG').
card_original_type('multani\'s presence'/'ULG', 'Enchantment').
card_original_text('multani\'s presence'/'ULG', 'Whenever a spell you play is countered, draw a card.').
card_first_print('multani\'s presence', 'ULG').
card_image_name('multani\'s presence'/'ULG', 'multani\'s presence').
card_uid('multani\'s presence'/'ULG', 'ULG:Multani\'s Presence:multani\'s presence').
card_rarity('multani\'s presence'/'ULG', 'Uncommon').
card_artist('multani\'s presence'/'ULG', 'Scott M. Fischer').
card_number('multani\'s presence'/'ULG', '109').
card_flavor_text('multani\'s presence'/'ULG', 'When a tree falls in the forest, Multani hears it.').
card_multiverse_id('multani\'s presence'/'ULG', '12490').

card_in_set('multani, maro-sorcerer', 'ULG').
card_original_type('multani, maro-sorcerer'/'ULG', 'Summon — Legend').
card_original_text('multani, maro-sorcerer'/'ULG', 'Multani, Maro-Sorcerer has power and toughness each equal to the total number of cards in all players\' hands.\nMultani cannot be the target of spells or abilities.').
card_first_print('multani, maro-sorcerer', 'ULG').
card_image_name('multani, maro-sorcerer'/'ULG', 'multani, maro-sorcerer').
card_uid('multani, maro-sorcerer'/'ULG', 'ULG:Multani, Maro-Sorcerer:multani, maro-sorcerer').
card_rarity('multani, maro-sorcerer'/'ULG', 'Rare').
card_artist('multani, maro-sorcerer'/'ULG', 'Daren Bader').
card_number('multani, maro-sorcerer'/'ULG', '107').
card_flavor_text('multani, maro-sorcerer'/'ULG', '\"To make peace with the forest, make peace with me.\"\n—Multani, to Urza').
card_multiverse_id('multani, maro-sorcerer'/'ULG', '12440').

card_in_set('no mercy', 'ULG').
card_original_type('no mercy'/'ULG', 'Enchantment').
card_original_text('no mercy'/'ULG', 'Whenever a creature successfully deals damage to you, destroy it.').
card_first_print('no mercy', 'ULG').
card_image_name('no mercy'/'ULG', 'no mercy').
card_uid('no mercy'/'ULG', 'ULG:No Mercy:no mercy').
card_rarity('no mercy'/'ULG', 'Rare').
card_artist('no mercy'/'ULG', 'Mark Tedin').
card_number('no mercy'/'ULG', '56').
card_flavor_text('no mercy'/'ULG', '\"We had years to prepare, while they had mere minutes.\"\n—Kerrick, sleeper agent').
card_multiverse_id('no mercy'/'ULG', '12408').

card_in_set('opal avenger', 'ULG').
card_original_type('opal avenger'/'ULG', 'Enchantment').
card_original_text('opal avenger'/'ULG', 'When you have 10 life or less, if Opal Avenger is an enchantment, Opal Avenger becomes a 3/5 creature that counts as a Guardian.').
card_first_print('opal avenger', 'ULG').
card_image_name('opal avenger'/'ULG', 'opal avenger').
card_uid('opal avenger'/'ULG', 'ULG:Opal Avenger:opal avenger').
card_rarity('opal avenger'/'ULG', 'Rare').
card_artist('opal avenger'/'ULG', 'Edward P. Beard, Jr.').
card_number('opal avenger'/'ULG', '15').
card_flavor_text('opal avenger'/'ULG', 'As the sun grew cold in the realm, the statue grew warm.').
card_multiverse_id('opal avenger'/'ULG', '8814').

card_in_set('opal champion', 'ULG').
card_original_type('opal champion'/'ULG', 'Enchantment').
card_original_text('opal champion'/'ULG', 'When one of your opponents successfully casts a creature spell, if Opal Champion is an enchantment, Opal Champion becomes a 3/3 creature with first strike that counts as a Knight.').
card_first_print('opal champion', 'ULG').
card_image_name('opal champion'/'ULG', 'opal champion').
card_uid('opal champion'/'ULG', 'ULG:Opal Champion:opal champion').
card_rarity('opal champion'/'ULG', 'Common').
card_artist('opal champion'/'ULG', 'Edward P. Beard, Jr.').
card_number('opal champion'/'ULG', '16').
card_multiverse_id('opal champion'/'ULG', '12352').

card_in_set('opportunity', 'ULG').
card_original_type('opportunity'/'ULG', 'Instant').
card_original_text('opportunity'/'ULG', 'Target player draws four cards.').
card_first_print('opportunity', 'ULG').
card_image_name('opportunity'/'ULG', 'opportunity').
card_uid('opportunity'/'ULG', 'ULG:Opportunity:opportunity').
card_rarity('opportunity'/'ULG', 'Uncommon').
card_artist('opportunity'/'ULG', 'Ron Spears').
card_number('opportunity'/'ULG', '37').
card_flavor_text('opportunity'/'ULG', '\"He cocooned himself alone in his workshop for months. When he finally emerged, all broad grins and excited chatter, I knew he\'d found his answer.\"\n—Barrin, master wizard').
card_multiverse_id('opportunity'/'ULG', '12822').

card_in_set('ostracize', 'ULG').
card_original_type('ostracize'/'ULG', 'Sorcery').
card_original_text('ostracize'/'ULG', 'Look at target opponent\'s hand and choose a creature card there. That player discards that card.').
card_first_print('ostracize', 'ULG').
card_image_name('ostracize'/'ULG', 'ostracize').
card_uid('ostracize'/'ULG', 'ULG:Ostracize:ostracize').
card_rarity('ostracize'/'ULG', 'Common').
card_artist('ostracize'/'ULG', 'Chippy').
card_number('ostracize'/'ULG', '57').
card_flavor_text('ostracize'/'ULG', 'Kerrick was to find that some borders can never be crossed.').
card_multiverse_id('ostracize'/'ULG', '8786').

card_in_set('palinchron', 'ULG').
card_original_type('palinchron'/'ULG', 'Summon — Illusion').
card_original_text('palinchron'/'ULG', 'Flying\nWhen Palinchron comes into play, untap up to seven lands.\n{2}{U}{U} Return Palinchron to owner\'s hand.').
card_first_print('palinchron', 'ULG').
card_image_name('palinchron'/'ULG', 'palinchron').
card_uid('palinchron'/'ULG', 'ULG:Palinchron:palinchron').
card_rarity('palinchron'/'ULG', 'Rare').
card_artist('palinchron'/'ULG', 'Matthew D. Wilson').
card_number('palinchron'/'ULG', '38').
card_multiverse_id('palinchron'/'ULG', '12580').

card_in_set('parch', 'ULG').
card_original_type('parch'/'ULG', 'Instant').
card_original_text('parch'/'ULG', 'Choose one Parch deals 2 damage to target creature or player; or Parch deals 4 damage to target blue creature.').
card_first_print('parch', 'ULG').
card_image_name('parch'/'ULG', 'parch').
card_uid('parch'/'ULG', 'ULG:Parch:parch').
card_rarity('parch'/'ULG', 'Common').
card_artist('parch'/'ULG', 'Ron Spencer').
card_number('parch'/'ULG', '86').
card_flavor_text('parch'/'ULG', '\"Your porous flesh betrays you.\"\n—Fire Eye, viashino bey').
card_multiverse_id('parch'/'ULG', '12415').

card_in_set('peace and quiet', 'ULG').
card_original_type('peace and quiet'/'ULG', 'Instant').
card_original_text('peace and quiet'/'ULG', 'Destroy two target enchantments.').
card_first_print('peace and quiet', 'ULG').
card_image_name('peace and quiet'/'ULG', 'peace and quiet').
card_uid('peace and quiet'/'ULG', 'ULG:Peace and Quiet:peace and quiet').
card_rarity('peace and quiet'/'ULG', 'Uncommon').
card_artist('peace and quiet'/'ULG', 'Hazeltine').
card_number('peace and quiet'/'ULG', '17').
card_flavor_text('peace and quiet'/'ULG', '\"In time our realm will shine again. But it will gleam only when we scour away the taint of doubt.\"\n—Radiant, archangel').
card_multiverse_id('peace and quiet'/'ULG', '9709').

card_in_set('phyrexian broodlings', 'ULG').
card_original_type('phyrexian broodlings'/'ULG', 'Summon — Minions').
card_original_text('phyrexian broodlings'/'ULG', '{1}, Sacrifice a creature: Put a +1/+1 counter on Phyrexian Broodlings.').
card_first_print('phyrexian broodlings', 'ULG').
card_image_name('phyrexian broodlings'/'ULG', 'phyrexian broodlings').
card_uid('phyrexian broodlings'/'ULG', 'ULG:Phyrexian Broodlings:phyrexian broodlings').
card_rarity('phyrexian broodlings'/'ULG', 'Common').
card_artist('phyrexian broodlings'/'ULG', 'Daren Bader').
card_number('phyrexian broodlings'/'ULG', '58').
card_flavor_text('phyrexian broodlings'/'ULG', 'With limited resources and near unlimited time, Kerrick used parts from one beast to build another.').
card_multiverse_id('phyrexian broodlings'/'ULG', '12392').

card_in_set('phyrexian debaser', 'ULG').
card_original_type('phyrexian debaser'/'ULG', 'Summon — Carrier').
card_original_text('phyrexian debaser'/'ULG', 'Flying\n{T}, Sacrifice Phyrexian Debaser: Target creature gets -2/-2 until end of turn.').
card_first_print('phyrexian debaser', 'ULG').
card_image_name('phyrexian debaser'/'ULG', 'phyrexian debaser').
card_uid('phyrexian debaser'/'ULG', 'ULG:Phyrexian Debaser:phyrexian debaser').
card_rarity('phyrexian debaser'/'ULG', 'Common').
card_artist('phyrexian debaser'/'ULG', 'Mark Tedin').
card_number('phyrexian debaser'/'ULG', '59').
card_flavor_text('phyrexian debaser'/'ULG', '\"The second stage of the illness: high fever and severe infectiousness.\"\n—Phyrexian progress notes').
card_multiverse_id('phyrexian debaser'/'ULG', '12697').

card_in_set('phyrexian defiler', 'ULG').
card_original_type('phyrexian defiler'/'ULG', 'Summon — Carrier').
card_original_text('phyrexian defiler'/'ULG', '{T}, Sacrifice Phyrexian Defiler: Target creature gets -3/-3 until end of turn.').
card_first_print('phyrexian defiler', 'ULG').
card_image_name('phyrexian defiler'/'ULG', 'phyrexian defiler').
card_uid('phyrexian defiler'/'ULG', 'ULG:Phyrexian Defiler:phyrexian defiler').
card_rarity('phyrexian defiler'/'ULG', 'Uncommon').
card_artist('phyrexian defiler'/'ULG', 'DiTerlizzi').
card_number('phyrexian defiler'/'ULG', '60').
card_flavor_text('phyrexian defiler'/'ULG', '\"The third stage of the illness: muscle aches and persistent cough.\"\n—Phyrexian progress notes').
card_multiverse_id('phyrexian defiler'/'ULG', '12696').

card_in_set('phyrexian denouncer', 'ULG').
card_original_type('phyrexian denouncer'/'ULG', 'Summon — Carrier').
card_original_text('phyrexian denouncer'/'ULG', '{T}, Sacrifice Phyrexian Denouncer: Target creature gets -1/-1 until end of turn.').
card_first_print('phyrexian denouncer', 'ULG').
card_image_name('phyrexian denouncer'/'ULG', 'phyrexian denouncer').
card_uid('phyrexian denouncer'/'ULG', 'ULG:Phyrexian Denouncer:phyrexian denouncer').
card_rarity('phyrexian denouncer'/'ULG', 'Common').
card_artist('phyrexian denouncer'/'ULG', 'Brian Snõddy').
card_number('phyrexian denouncer'/'ULG', '61').
card_flavor_text('phyrexian denouncer'/'ULG', '\"The first stage of the illness: rash and nausea.\"\n—Phyrexian progress notes').
card_multiverse_id('phyrexian denouncer'/'ULG', '12614').

card_in_set('phyrexian plaguelord', 'ULG').
card_original_type('phyrexian plaguelord'/'ULG', 'Summon — Carrier').
card_original_text('phyrexian plaguelord'/'ULG', '{T}, Sacrifice Phyrexian Plaguelord: Target creature gets -4/-4 until end of turn.\nSacrifice a creature: Target creature gets -1/-1 until end of turn.').
card_first_print('phyrexian plaguelord', 'ULG').
card_image_name('phyrexian plaguelord'/'ULG', 'phyrexian plaguelord').
card_uid('phyrexian plaguelord'/'ULG', 'ULG:Phyrexian Plaguelord:phyrexian plaguelord').
card_rarity('phyrexian plaguelord'/'ULG', 'Rare').
card_artist('phyrexian plaguelord'/'ULG', 'Kev Walker').
card_number('phyrexian plaguelord'/'ULG', '62').
card_flavor_text('phyrexian plaguelord'/'ULG', '\"The final stage of the illness: delirium, convulsions, and death.\"\n—Phyrexian progress notes').
card_multiverse_id('phyrexian plaguelord'/'ULG', '12940').

card_in_set('phyrexian reclamation', 'ULG').
card_original_type('phyrexian reclamation'/'ULG', 'Enchantment').
card_original_text('phyrexian reclamation'/'ULG', '{1}{B}, Pay 2 life: Return target creature card from your graveyard to your hand.').
card_first_print('phyrexian reclamation', 'ULG').
card_image_name('phyrexian reclamation'/'ULG', 'phyrexian reclamation').
card_uid('phyrexian reclamation'/'ULG', 'ULG:Phyrexian Reclamation:phyrexian reclamation').
card_rarity('phyrexian reclamation'/'ULG', 'Uncommon').
card_artist('phyrexian reclamation'/'ULG', 'rk post').
card_number('phyrexian reclamation'/'ULG', '63').
card_flavor_text('phyrexian reclamation'/'ULG', 'Death is no excuse to stop working.').
card_multiverse_id('phyrexian reclamation'/'ULG', '12945').

card_in_set('plague beetle', 'ULG').
card_original_type('plague beetle'/'ULG', 'Summon — Insect').
card_original_text('plague beetle'/'ULG', 'Swampwalk (If defending player controls a swamp, this creature is unblockable.)').
card_first_print('plague beetle', 'ULG').
card_image_name('plague beetle'/'ULG', 'plague beetle').
card_uid('plague beetle'/'ULG', 'ULG:Plague Beetle:plague beetle').
card_rarity('plague beetle'/'ULG', 'Common').
card_artist('plague beetle'/'ULG', 'Tom Fleming').
card_number('plague beetle'/'ULG', '64').
card_flavor_text('plague beetle'/'ULG', 'It is the harbinger of disease, not the carrier.').
card_multiverse_id('plague beetle'/'ULG', '12393').

card_in_set('planar collapse', 'ULG').
card_original_type('planar collapse'/'ULG', 'Enchantment').
card_original_text('planar collapse'/'ULG', 'During your upkeep, if there are four or more creatures in play, sacrifice Planar Collapse and destroy all creatures. Those creatures cannot be regenerated this turn.').
card_first_print('planar collapse', 'ULG').
card_image_name('planar collapse'/'ULG', 'planar collapse').
card_uid('planar collapse'/'ULG', 'ULG:Planar Collapse:planar collapse').
card_rarity('planar collapse'/'ULG', 'Rare').
card_artist('planar collapse'/'ULG', 'Mark Zug').
card_number('planar collapse'/'ULG', '18').
card_flavor_text('planar collapse'/'ULG', 'With heavy heart, Urza doused one world\'s light to rekindle another\'s.').
card_multiverse_id('planar collapse'/'ULG', '12365').

card_in_set('purify', 'ULG').
card_original_type('purify'/'ULG', 'Sorcery').
card_original_text('purify'/'ULG', 'Destroy all artifacts and enchantments.').
card_first_print('purify', 'ULG').
card_image_name('purify'/'ULG', 'purify').
card_uid('purify'/'ULG', 'ULG:Purify:purify').
card_rarity('purify'/'ULG', 'Rare').
card_artist('purify'/'ULG', 'John Avon').
card_number('purify'/'ULG', '19').
card_flavor_text('purify'/'ULG', '\"Our Mother! The sky was Her hair; the sun, Her face. She danced on the grass and in the hills.\"\n—Song of All, canto 23').
card_multiverse_id('purify'/'ULG', '12367').

card_in_set('pygmy pyrosaur', 'ULG').
card_original_type('pygmy pyrosaur'/'ULG', 'Summon — Lizard').
card_original_text('pygmy pyrosaur'/'ULG', 'Pygmy Pyrosaur cannot block.\n{R} Pygmy Pyrosaur gets +1/+0 until end of turn.').
card_first_print('pygmy pyrosaur', 'ULG').
card_image_name('pygmy pyrosaur'/'ULG', 'pygmy pyrosaur').
card_uid('pygmy pyrosaur'/'ULG', 'ULG:Pygmy Pyrosaur:pygmy pyrosaur').
card_rarity('pygmy pyrosaur'/'ULG', 'Common').
card_artist('pygmy pyrosaur'/'ULG', 'Dan Frazier').
card_number('pygmy pyrosaur'/'ULG', '87').
card_flavor_text('pygmy pyrosaur'/'ULG', 'Do not judge a lizard by its size.\n—Ghitu proverb').
card_multiverse_id('pygmy pyrosaur'/'ULG', '12412').

card_in_set('pyromancy', 'ULG').
card_original_type('pyromancy'/'ULG', 'Enchantment').
card_original_text('pyromancy'/'ULG', '{3}, Discard a card at random: Pyromancy deals to target creature or player damage equal to the total casting cost of the discarded card.').
card_first_print('pyromancy', 'ULG').
card_image_name('pyromancy'/'ULG', 'pyromancy').
card_uid('pyromancy'/'ULG', 'ULG:Pyromancy:pyromancy').
card_rarity('pyromancy'/'ULG', 'Rare').
card_artist('pyromancy'/'ULG', 'Quinton Hoover').
card_number('pyromancy'/'ULG', '88').
card_flavor_text('pyromancy'/'ULG', 'Who harnesses fire controls the world.\n—Ghitu proverb').
card_multiverse_id('pyromancy'/'ULG', '12668').

card_in_set('quicksilver amulet', 'ULG').
card_original_type('quicksilver amulet'/'ULG', 'Artifact').
card_original_text('quicksilver amulet'/'ULG', '{4}, {T}: Choose a creature card in your hand and put that creature into play.').
card_first_print('quicksilver amulet', 'ULG').
card_image_name('quicksilver amulet'/'ULG', 'quicksilver amulet').
card_uid('quicksilver amulet'/'ULG', 'ULG:Quicksilver Amulet:quicksilver amulet').
card_rarity('quicksilver amulet'/'ULG', 'Rare').
card_artist('quicksilver amulet'/'ULG', 'Douglas Shuler').
card_number('quicksilver amulet'/'ULG', '130').
card_flavor_text('quicksilver amulet'/'ULG', '\"Wonderful! You got a lion on your first try. Now put it back.\"').
card_multiverse_id('quicksilver amulet'/'ULG', '8831').

card_in_set('rack and ruin', 'ULG').
card_original_type('rack and ruin'/'ULG', 'Instant').
card_original_text('rack and ruin'/'ULG', 'Destroy two target artifacts.').
card_first_print('rack and ruin', 'ULG').
card_image_name('rack and ruin'/'ULG', 'rack and ruin').
card_uid('rack and ruin'/'ULG', 'ULG:Rack and Ruin:rack and ruin').
card_rarity('rack and ruin'/'ULG', 'Uncommon').
card_artist('rack and ruin'/'ULG', 'Donato Giancola').
card_number('rack and ruin'/'ULG', '89').
card_flavor_text('rack and ruin'/'ULG', '\"My people are bound by masters centuries dead. Each artifact we destroy is another link broken in that chain.\"\n—Barb Tail, viashino heretic').
card_multiverse_id('rack and ruin'/'ULG', '12422').

card_in_set('radiant\'s dragoons', 'ULG').
card_original_type('radiant\'s dragoons'/'ULG', 'Summon — Soldiers').
card_original_text('radiant\'s dragoons'/'ULG', 'Echo (During your next upkeep after this permanent comes under your control, pay its casting cost or sacrifice it.)\nWhen Radiant\'s Dragoons comes into play, gain 5 life.').
card_first_print('radiant\'s dragoons', 'ULG').
card_image_name('radiant\'s dragoons'/'ULG', 'radiant\'s dragoons').
card_uid('radiant\'s dragoons'/'ULG', 'ULG:Radiant\'s Dragoons:radiant\'s dragoons').
card_rarity('radiant\'s dragoons'/'ULG', 'Uncommon').
card_artist('radiant\'s dragoons'/'ULG', 'Pete Venters').
card_number('radiant\'s dragoons'/'ULG', '21').
card_multiverse_id('radiant\'s dragoons'/'ULG', '12364').

card_in_set('radiant\'s judgment', 'ULG').
card_original_type('radiant\'s judgment'/'ULG', 'Instant').
card_original_text('radiant\'s judgment'/'ULG', 'Destroy target creature with power 4 or greater.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('radiant\'s judgment', 'ULG').
card_image_name('radiant\'s judgment'/'ULG', 'radiant\'s judgment').
card_uid('radiant\'s judgment'/'ULG', 'ULG:Radiant\'s Judgment:radiant\'s judgment').
card_rarity('radiant\'s judgment'/'ULG', 'Common').
card_artist('radiant\'s judgment'/'ULG', 'Greg Staples').
card_number('radiant\'s judgment'/'ULG', '22').
card_multiverse_id('radiant\'s judgment'/'ULG', '12360').

card_in_set('radiant, archangel', 'ULG').
card_original_type('radiant, archangel'/'ULG', 'Summon — Legend').
card_original_text('radiant, archangel'/'ULG', 'Flying\nRadiant, Archangel counts as an Angel.\nAttacking does not cause Radiant to tap.\nRadiant gets +1/+1 for each other creature with flying in play.').
card_first_print('radiant, archangel', 'ULG').
card_image_name('radiant, archangel'/'ULG', 'radiant, archangel').
card_uid('radiant, archangel'/'ULG', 'ULG:Radiant, Archangel:radiant, archangel').
card_rarity('radiant, archangel'/'ULG', 'Rare').
card_artist('radiant, archangel'/'ULG', 'Michael Sutfin').
card_number('radiant, archangel'/'ULG', '20').
card_multiverse_id('radiant, archangel'/'ULG', '12368').

card_in_set('rancor', 'ULG').
card_original_type('rancor'/'ULG', 'Enchant Creature').
card_original_text('rancor'/'ULG', 'Enchanted creature gains +2/+0 and trample.\nWhen Rancor is put into a graveyard from play, return Rancor to owner\'s hand.').
card_first_print('rancor', 'ULG').
card_image_name('rancor'/'ULG', 'rancor').
card_uid('rancor'/'ULG', 'ULG:Rancor:rancor').
card_rarity('rancor'/'ULG', 'Common').
card_artist('rancor'/'ULG', 'Kev Walker').
card_number('rancor'/'ULG', '110').
card_flavor_text('rancor'/'ULG', 'Hatred outlives the hateful.').
card_multiverse_id('rancor'/'ULG', '12433').

card_in_set('rank and file', 'ULG').
card_original_type('rank and file'/'ULG', 'Summon — Zombies').
card_original_text('rank and file'/'ULG', 'When Rank and File comes into play, all green creatures get -1/-1 until end of turn.').
card_first_print('rank and file', 'ULG').
card_image_name('rank and file'/'ULG', 'rank and file').
card_uid('rank and file'/'ULG', 'ULG:Rank and File:rank and file').
card_rarity('rank and file'/'ULG', 'Uncommon').
card_artist('rank and file'/'ULG', 'Donato Giancola').
card_number('rank and file'/'ULG', '65').
card_flavor_text('rank and file'/'ULG', '\"Left, right, left, right . . . hmm. Okay—left, left, left, left, . . .\"').
card_multiverse_id('rank and file'/'ULG', '5703').

card_in_set('raven familiar', 'ULG').
card_original_type('raven familiar'/'ULG', 'Summon — Bird').
card_original_text('raven familiar'/'ULG', 'Flying; echo (During your next upkeep after this permanent comes under your control, pay its casting cost or sacrifice it.)\nWhen Raven Familiar comes into play, look at the top three cards of your library. Put one of them into your hand and the rest on the bottom of your library in any order.').
card_first_print('raven familiar', 'ULG').
card_image_name('raven familiar'/'ULG', 'raven familiar').
card_uid('raven familiar'/'ULG', 'ULG:Raven Familiar:raven familiar').
card_rarity('raven familiar'/'ULG', 'Uncommon').
card_artist('raven familiar'/'ULG', 'Edward P. Beard, Jr.').
card_number('raven familiar'/'ULG', '39').
card_multiverse_id('raven familiar'/'ULG', '12381').

card_in_set('rebuild', 'ULG').
card_original_type('rebuild'/'ULG', 'Instant').
card_original_text('rebuild'/'ULG', 'Return all artifacts to owners\' hands.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('rebuild', 'ULG').
card_image_name('rebuild'/'ULG', 'rebuild').
card_uid('rebuild'/'ULG', 'ULG:Rebuild:rebuild').
card_rarity('rebuild'/'ULG', 'Uncommon').
card_artist('rebuild'/'ULG', 'L. A. Williams').
card_number('rebuild'/'ULG', '40').
card_multiverse_id('rebuild'/'ULG', '12385').

card_in_set('repopulate', 'ULG').
card_original_type('repopulate'/'ULG', 'Instant').
card_original_text('repopulate'/'ULG', 'Shuffle all creature cards from target player\'s graveyard into that player\'s library.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('repopulate', 'ULG').
card_image_name('repopulate'/'ULG', 'repopulate').
card_uid('repopulate'/'ULG', 'ULG:Repopulate:repopulate').
card_rarity('repopulate'/'ULG', 'Common').
card_artist('repopulate'/'ULG', 'Una Fricker').
card_number('repopulate'/'ULG', '111').
card_multiverse_id('repopulate'/'ULG', '12672').

card_in_set('ring of gix', 'ULG').
card_original_type('ring of gix'/'ULG', 'Artifact').
card_original_text('ring of gix'/'ULG', 'Echo (During your next upkeep after this permanent comes under your control, pay its casting cost or sacrifice it.)\n{1}, {T}: Tap target artifact, creature, or land.').
card_first_print('ring of gix', 'ULG').
card_image_name('ring of gix'/'ULG', 'ring of gix').
card_uid('ring of gix'/'ULG', 'ULG:Ring of Gix:ring of gix').
card_rarity('ring of gix'/'ULG', 'Rare').
card_artist('ring of gix'/'ULG', 'Mark Tedin').
card_number('ring of gix'/'ULG', '131').
card_flavor_text('ring of gix'/'ULG', 'Not every cage is made of bars.').
card_multiverse_id('ring of gix'/'ULG', '12501').

card_in_set('rivalry', 'ULG').
card_original_type('rivalry'/'ULG', 'Enchantment').
card_original_text('rivalry'/'ULG', 'During each player\'s upkeep, if that player controls more lands than any other, Rivalry deals 2 damage to him or her.').
card_first_print('rivalry', 'ULG').
card_image_name('rivalry'/'ULG', 'rivalry').
card_uid('rivalry'/'ULG', 'ULG:Rivalry:rivalry').
card_rarity('rivalry'/'ULG', 'Rare').
card_artist('rivalry'/'ULG', 'Brian Snõddy').
card_number('rivalry'/'ULG', '90').
card_flavor_text('rivalry'/'ULG', 'The goblins revered it; the viashino defended it. Only Urza understood it.').
card_multiverse_id('rivalry'/'ULG', '8903').

card_in_set('scrapheap', 'ULG').
card_original_type('scrapheap'/'ULG', 'Artifact').
card_original_text('scrapheap'/'ULG', 'Whenever an artifact or enchantment is put into your graveyard from play, gain 1 life.').
card_first_print('scrapheap', 'ULG').
card_image_name('scrapheap'/'ULG', 'scrapheap').
card_uid('scrapheap'/'ULG', 'ULG:Scrapheap:scrapheap').
card_rarity('scrapheap'/'ULG', 'Rare').
card_artist('scrapheap'/'ULG', 'Bradley Williams').
card_number('scrapheap'/'ULG', '132').
card_flavor_text('scrapheap'/'ULG', 'Junk heaps have rats, but scrapheaps have goblins.').
card_multiverse_id('scrapheap'/'ULG', '8830').

card_in_set('second chance', 'ULG').
card_original_type('second chance'/'ULG', 'Enchantment').
card_original_text('second chance'/'ULG', 'During your upkeep, if you have 5 life or less, sacrifice Second Chance and take an extra turn after this one.').
card_first_print('second chance', 'ULG').
card_image_name('second chance'/'ULG', 'second chance').
card_uid('second chance'/'ULG', 'ULG:Second Chance:second chance').
card_rarity('second chance'/'ULG', 'Rare').
card_artist('second chance'/'ULG', 'Mark Tedin').
card_number('second chance'/'ULG', '41').
card_flavor_text('second chance'/'ULG', 'The greatest gift is the opportunity to right one\'s wrongs.').
card_multiverse_id('second chance'/'ULG', '12391').

card_in_set('shivan phoenix', 'ULG').
card_original_type('shivan phoenix'/'ULG', 'Summon — Phoenix').
card_original_text('shivan phoenix'/'ULG', 'Flying\nWhen Shivan Phoenix is put into a graveyard from play, return Shivan Phoenix to owner\'s hand.').
card_first_print('shivan phoenix', 'ULG').
card_image_name('shivan phoenix'/'ULG', 'shivan phoenix').
card_uid('shivan phoenix'/'ULG', 'ULG:Shivan Phoenix:shivan phoenix').
card_rarity('shivan phoenix'/'ULG', 'Rare').
card_artist('shivan phoenix'/'ULG', 'Daren Bader').
card_number('shivan phoenix'/'ULG', '91').
card_flavor_text('shivan phoenix'/'ULG', 'To strike down the phoenix is to slash at the flame.').
card_multiverse_id('shivan phoenix'/'ULG', '12423').

card_in_set('sick and tired', 'ULG').
card_original_type('sick and tired'/'ULG', 'Instant').
card_original_text('sick and tired'/'ULG', 'Two target creatures each get -1/-1 until end of turn.').
card_first_print('sick and tired', 'ULG').
card_image_name('sick and tired'/'ULG', 'sick and tired').
card_uid('sick and tired'/'ULG', 'ULG:Sick and Tired:sick and tired').
card_rarity('sick and tired'/'ULG', 'Common').
card_artist('sick and tired'/'ULG', 'Val Mayerik').
card_number('sick and tired'/'ULG', '66').
card_flavor_text('sick and tired'/'ULG', 'The Phyrexians\' only interest in organic life is discerning its weakness.').
card_multiverse_id('sick and tired'/'ULG', '9691').

card_in_set('silk net', 'ULG').
card_original_type('silk net'/'ULG', 'Instant').
card_original_text('silk net'/'ULG', 'Target creature gets +1/+1 and can block creatures with flying until end of turn.').
card_first_print('silk net', 'ULG').
card_image_name('silk net'/'ULG', 'silk net').
card_uid('silk net'/'ULG', 'ULG:Silk Net:silk net').
card_rarity('silk net'/'ULG', 'Common').
card_artist('silk net'/'ULG', 'Rob Alexander').
card_number('silk net'/'ULG', '112').
card_flavor_text('silk net'/'ULG', 'Sometimes it\'s possible to pull a meal out of thin air.').
card_multiverse_id('silk net'/'ULG', '12949').

card_in_set('simian grunts', 'ULG').
card_original_type('simian grunts'/'ULG', 'Summon — Apes').
card_original_text('simian grunts'/'ULG', 'Echo (During your next upkeep after this permanent comes under your control, pay its casting cost or sacrifice it.)\nYou may play Simian Grunts any time you could play an instant.').
card_first_print('simian grunts', 'ULG').
card_image_name('simian grunts'/'ULG', 'simian grunts').
card_uid('simian grunts'/'ULG', 'ULG:Simian Grunts:simian grunts').
card_rarity('simian grunts'/'ULG', 'Common').
card_artist('simian grunts'/'ULG', 'Pete Venters').
card_number('simian grunts'/'ULG', '113').
card_flavor_text('simian grunts'/'ULG', 'These monkeys mean business.').
card_multiverse_id('simian grunts'/'ULG', '12428').

card_in_set('sleeper\'s guile', 'ULG').
card_original_type('sleeper\'s guile'/'ULG', 'Enchant Creature').
card_original_text('sleeper\'s guile'/'ULG', 'Enchanted creature cannot be blocked except by artifact creatures and black creatures.\nWhen Sleeper\'s Guile is put into a graveyard from play, return Sleeper\'s Guile to owner\'s hand.').
card_first_print('sleeper\'s guile', 'ULG').
card_image_name('sleeper\'s guile'/'ULG', 'sleeper\'s guile').
card_uid('sleeper\'s guile'/'ULG', 'ULG:Sleeper\'s Guile:sleeper\'s guile').
card_rarity('sleeper\'s guile'/'ULG', 'Common').
card_artist('sleeper\'s guile'/'ULG', 'Daren Bader').
card_number('sleeper\'s guile'/'ULG', '67').
card_multiverse_id('sleeper\'s guile'/'ULG', '12397').

card_in_set('slow motion', 'ULG').
card_original_type('slow motion'/'ULG', 'Enchant Creature').
card_original_text('slow motion'/'ULG', 'During the upkeep of enchanted creature\'s controller, that player pays {2} or sacrifices that creature.\nWhen Slow Motion is put into a graveyard from play, return Slow Motion to owner\'s hand.').
card_first_print('slow motion', 'ULG').
card_image_name('slow motion'/'ULG', 'slow motion').
card_uid('slow motion'/'ULG', 'ULG:Slow Motion:slow motion').
card_rarity('slow motion'/'ULG', 'Common').
card_artist('slow motion'/'ULG', 'Todd Lockwood').
card_number('slow motion'/'ULG', '42').
card_multiverse_id('slow motion'/'ULG', '12384').

card_in_set('sluggishness', 'ULG').
card_original_type('sluggishness'/'ULG', 'Enchant Creature').
card_original_text('sluggishness'/'ULG', 'Enchanted creature cannot block.\nWhen Sluggishness is put into a graveyard from play, return Sluggishness to owner\'s hand.').
card_first_print('sluggishness', 'ULG').
card_image_name('sluggishness'/'ULG', 'sluggishness').
card_uid('sluggishness'/'ULG', 'ULG:Sluggishness:sluggishness').
card_rarity('sluggishness'/'ULG', 'Common').
card_artist('sluggishness'/'ULG', 'Pete Venters').
card_number('sluggishness'/'ULG', '92').
card_flavor_text('sluggishness'/'ULG', 'Vark decided to lie down and think of a good excuse to quit working.').
card_multiverse_id('sluggishness'/'ULG', '12416').

card_in_set('snap', 'ULG').
card_original_type('snap'/'ULG', 'Instant').
card_original_text('snap'/'ULG', 'Return target creature to owner\'s hand. Untap up to two lands.').
card_first_print('snap', 'ULG').
card_image_name('snap'/'ULG', 'snap').
card_uid('snap'/'ULG', 'ULG:Snap:snap').
card_rarity('snap'/'ULG', 'Common').
card_artist('snap'/'ULG', 'Mike Raabe').
card_number('snap'/'ULG', '43').
card_flavor_text('snap'/'ULG', 'Good riddance.').
card_multiverse_id('snap'/'ULG', '12374').

card_in_set('spawning pool', 'ULG').
card_original_type('spawning pool'/'ULG', 'Land').
card_original_text('spawning pool'/'ULG', 'Spawning Pool comes into play tapped.\n{T}: Add one black mana to your mana pool.\n{1}{B} Spawning Pool becomes a 1/1 black creature with \"{B} Regenerate this creature\" until end of turn. This creature still counts as a land.').
card_first_print('spawning pool', 'ULG').
card_image_name('spawning pool'/'ULG', 'spawning pool').
card_uid('spawning pool'/'ULG', 'ULG:Spawning Pool:spawning pool').
card_rarity('spawning pool'/'ULG', 'Uncommon').
card_artist('spawning pool'/'ULG', 'Rob Alexander').
card_number('spawning pool'/'ULG', '142').
card_multiverse_id('spawning pool'/'ULG', '12496').

card_in_set('subversion', 'ULG').
card_original_type('subversion'/'ULG', 'Enchantment').
card_original_text('subversion'/'ULG', 'During your upkeep, each of your opponents loses 1 life. Gain 1 life for each 1 life lost this way.').
card_first_print('subversion', 'ULG').
card_image_name('subversion'/'ULG', 'subversion').
card_uid('subversion'/'ULG', 'ULG:Subversion:subversion').
card_rarity('subversion'/'ULG', 'Rare').
card_artist('subversion'/'ULG', 'Rob Alexander').
card_number('subversion'/'ULG', '68').
card_flavor_text('subversion'/'ULG', 'Kerrick\'s corrupt domain swelled like a blister on Tolaria\'s skin.').
card_multiverse_id('subversion'/'ULG', '12407').

card_in_set('sustainer of the realm', 'ULG').
card_original_type('sustainer of the realm'/'ULG', 'Summon — Angel').
card_original_text('sustainer of the realm'/'ULG', 'Flying\nWhenever Sustainer of the Realm blocks, it gets +0/+2 until end of turn.').
card_first_print('sustainer of the realm', 'ULG').
card_image_name('sustainer of the realm'/'ULG', 'sustainer of the realm').
card_uid('sustainer of the realm'/'ULG', 'ULG:Sustainer of the Realm:sustainer of the realm').
card_rarity('sustainer of the realm'/'ULG', 'Uncommon').
card_artist('sustainer of the realm'/'ULG', 'Greg Staples').
card_number('sustainer of the realm'/'ULG', '23').
card_flavor_text('sustainer of the realm'/'ULG', '\"The harder you push, the stronger we become.\"\n—Radiant, to Urza').
card_multiverse_id('sustainer of the realm'/'ULG', '12814').

card_in_set('swat', 'ULG').
card_original_type('swat'/'ULG', 'Instant').
card_original_text('swat'/'ULG', 'Destroy target creature with power 2 or less.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('swat', 'ULG').
card_image_name('swat'/'ULG', 'swat').
card_uid('swat'/'ULG', 'ULG:Swat:swat').
card_rarity('swat'/'ULG', 'Common').
card_artist('swat'/'ULG', 'Daren Bader').
card_number('swat'/'ULG', '69').
card_multiverse_id('swat'/'ULG', '9694').

card_in_set('tethered skirge', 'ULG').
card_original_type('tethered skirge'/'ULG', 'Summon — Imp').
card_original_text('tethered skirge'/'ULG', 'Flying\nWhenever Tethered Skirge becomes the target of a spell or ability, lose 1 life.').
card_first_print('tethered skirge', 'ULG').
card_image_name('tethered skirge'/'ULG', 'tethered skirge').
card_uid('tethered skirge'/'ULG', 'ULG:Tethered Skirge:tethered skirge').
card_rarity('tethered skirge'/'ULG', 'Uncommon').
card_artist('tethered skirge'/'ULG', 'Brian Snõddy').
card_number('tethered skirge'/'ULG', '70').
card_flavor_text('tethered skirge'/'ULG', 'It bites the hand that leads it.').
card_multiverse_id('tethered skirge'/'ULG', '9840').

card_in_set('thornwind faeries', 'ULG').
card_original_type('thornwind faeries'/'ULG', 'Summon — Faeries').
card_original_text('thornwind faeries'/'ULG', 'Flying\n{T}: Thornwind Faeries deals 1 damage to target creature or player.').
card_first_print('thornwind faeries', 'ULG').
card_image_name('thornwind faeries'/'ULG', 'thornwind faeries').
card_uid('thornwind faeries'/'ULG', 'ULG:Thornwind Faeries:thornwind faeries').
card_rarity('thornwind faeries'/'ULG', 'Common').
card_artist('thornwind faeries'/'ULG', 'Rebecca Guay').
card_number('thornwind faeries'/'ULG', '44').
card_flavor_text('thornwind faeries'/'ULG', 'Guarding the ship is the Thornwinds\' first concern. Getting along with the locals ranks fourth or fifth at best.').
card_multiverse_id('thornwind faeries'/'ULG', '12369').

card_in_set('thran lens', 'ULG').
card_original_type('thran lens'/'ULG', 'Artifact').
card_original_text('thran lens'/'ULG', 'All permanents are colorless.').
card_first_print('thran lens', 'ULG').
card_image_name('thran lens'/'ULG', 'thran lens').
card_uid('thran lens'/'ULG', 'ULG:Thran Lens:thran lens').
card_rarity('thran lens'/'ULG', 'Rare').
card_artist('thran lens'/'ULG', 'L. A. Williams').
card_number('thran lens'/'ULG', '133').
card_flavor_text('thran lens'/'ULG', '\"Every device in the rig is evidence of Thran enlightenment. All mana was the same to them, whether from rock or water, growth or decay. Can you imagine such unity of vision?\"\n—Urza, journal').
card_multiverse_id('thran lens'/'ULG', '8852').

card_in_set('thran war machine', 'ULG').
card_original_type('thran war machine'/'ULG', 'Artifact Creature').
card_original_text('thran war machine'/'ULG', 'Echo (During your next upkeep after this permanent comes under your control, pay its casting cost or sacrifice it.)\nThran War Machine attacks each turn if able.').
card_first_print('thran war machine', 'ULG').
card_image_name('thran war machine'/'ULG', 'thran war machine').
card_uid('thran war machine'/'ULG', 'ULG:Thran War Machine:thran war machine').
card_rarity('thran war machine'/'ULG', 'Uncommon').
card_artist('thran war machine'/'ULG', 'Pete Venters').
card_number('thran war machine'/'ULG', '134').
card_multiverse_id('thran war machine'/'ULG', '12625').

card_in_set('thran weaponry', 'ULG').
card_original_type('thran weaponry'/'ULG', 'Artifact').
card_original_text('thran weaponry'/'ULG', 'Echo (During your next upkeep after this permanent comes under your control, pay its casting cost or sacrifice it.)\nYou may choose not to untap Thran Weaponry during your untap phase.\n{2}, {T}: All creatures get +2/+2 as long as Thran Weaponry remains tapped.').
card_first_print('thran weaponry', 'ULG').
card_image_name('thran weaponry'/'ULG', 'thran weaponry').
card_uid('thran weaponry'/'ULG', 'ULG:Thran Weaponry:thran weaponry').
card_rarity('thran weaponry'/'ULG', 'Rare').
card_artist('thran weaponry'/'ULG', 'Anthony S. Waters').
card_number('thran weaponry'/'ULG', '135').
card_multiverse_id('thran weaponry'/'ULG', '12646').

card_in_set('ticking gnomes', 'ULG').
card_original_type('ticking gnomes'/'ULG', 'Artifact Creature').
card_original_text('ticking gnomes'/'ULG', 'Echo (During your next upkeep after this permanent comes under your control, pay its casting cost or sacrifice it.)\nSacrifice Ticking Gnomes: Ticking Gnomes deals 1 damage to target creature or player.').
card_first_print('ticking gnomes', 'ULG').
card_image_name('ticking gnomes'/'ULG', 'ticking gnomes').
card_uid('ticking gnomes'/'ULG', 'ULG:Ticking Gnomes:ticking gnomes').
card_rarity('ticking gnomes'/'ULG', 'Uncommon').
card_artist('ticking gnomes'/'ULG', 'Henry Van Der Linde').
card_number('ticking gnomes'/'ULG', '136').
card_multiverse_id('ticking gnomes'/'ULG', '12647').

card_in_set('tinker', 'ULG').
card_original_type('tinker'/'ULG', 'Sorcery').
card_original_text('tinker'/'ULG', 'At the time you play Tinker, sacrifice an artifact.\nSearch your library for an artifact card and put that artifact into play. Shuffle your library afterward.').
card_first_print('tinker', 'ULG').
card_image_name('tinker'/'ULG', 'tinker').
card_uid('tinker'/'ULG', 'ULG:Tinker:tinker').
card_rarity('tinker'/'ULG', 'Uncommon').
card_artist('tinker'/'ULG', 'Mike Raabe').
card_number('tinker'/'ULG', '45').
card_flavor_text('tinker'/'ULG', '\"I wonder how it feels to be bored.\"\n—Jhoira, artificer').
card_multiverse_id('tinker'/'ULG', '12383').

card_in_set('tragic poet', 'ULG').
card_original_type('tragic poet'/'ULG', 'Summon — Townsfolk').
card_original_text('tragic poet'/'ULG', '{T}, Sacrifice Tragic Poet: Return target enchantment card from your graveyard to your hand.').
card_first_print('tragic poet', 'ULG').
card_image_name('tragic poet'/'ULG', 'tragic poet').
card_uid('tragic poet'/'ULG', 'ULG:Tragic Poet:tragic poet').
card_rarity('tragic poet'/'ULG', 'Common').
card_artist('tragic poet'/'ULG', 'Quinton Hoover').
card_number('tragic poet'/'ULG', '24').
card_flavor_text('tragic poet'/'ULG', '\"I would weep, but my tears have been stolen; I would shout, but my voice has been taken. Thus, I write.\"').
card_multiverse_id('tragic poet'/'ULG', '12355').

card_in_set('treacherous link', 'ULG').
card_original_type('treacherous link'/'ULG', 'Enchant Creature').
card_original_text('treacherous link'/'ULG', 'Redirect to its controller all damage dealt to enchanted creature.').
card_first_print('treacherous link', 'ULG').
card_image_name('treacherous link'/'ULG', 'treacherous link').
card_uid('treacherous link'/'ULG', 'ULG:Treacherous Link:treacherous link').
card_rarity('treacherous link'/'ULG', 'Uncommon').
card_artist('treacherous link'/'ULG', 'Carl Critchlow').
card_number('treacherous link'/'ULG', '71').
card_flavor_text('treacherous link'/'ULG', '\"You cannot possibly know the toll your alliances will exact from you.\"\n—Barrin, to Urza').
card_multiverse_id('treacherous link'/'ULG', '12403').

card_in_set('treefolk mystic', 'ULG').
card_original_type('treefolk mystic'/'ULG', 'Summon — Treefolk').
card_original_text('treefolk mystic'/'ULG', 'Whenever a creature blocks or is blocked by Treefolk Mystic, destroy all enchantments on that creature.').
card_first_print('treefolk mystic', 'ULG').
card_image_name('treefolk mystic'/'ULG', 'treefolk mystic').
card_uid('treefolk mystic'/'ULG', 'ULG:Treefolk Mystic:treefolk mystic').
card_rarity('treefolk mystic'/'ULG', 'Common').
card_artist('treefolk mystic'/'ULG', 'DiTerlizzi').
card_number('treefolk mystic'/'ULG', '114').
card_flavor_text('treefolk mystic'/'ULG', 'Urza\'s wards fell from him like autumn leaves as he entered the dreaming grove. He awoke imprisoned in living wood.').
card_multiverse_id('treefolk mystic'/'ULG', '9723').

card_in_set('treetop village', 'ULG').
card_original_type('treetop village'/'ULG', 'Land').
card_original_text('treetop village'/'ULG', 'Treetop Village comes into play tapped.\n{T}: Add one green mana to your mana pool.\n{1}{G} Treetop Village becomes a 3/3 green creature with trample until end of turn. This creature still counts as a land.').
card_first_print('treetop village', 'ULG').
card_image_name('treetop village'/'ULG', 'treetop village').
card_uid('treetop village'/'ULG', 'ULG:Treetop Village:treetop village').
card_rarity('treetop village'/'ULG', 'Uncommon').
card_artist('treetop village'/'ULG', 'Anthony S. Waters').
card_number('treetop village'/'ULG', '143').
card_multiverse_id('treetop village'/'ULG', '12498').

card_in_set('unearth', 'ULG').
card_original_type('unearth'/'ULG', 'Sorcery').
card_original_text('unearth'/'ULG', 'Choose target creature card in your graveyard with total casting cost 3 or less and put that creature into play.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('unearth', 'ULG').
card_image_name('unearth'/'ULG', 'unearth').
card_uid('unearth'/'ULG', 'ULG:Unearth:unearth').
card_rarity('unearth'/'ULG', 'Common').
card_artist('unearth'/'ULG', 'Hazeltine').
card_number('unearth'/'ULG', '72').
card_multiverse_id('unearth'/'ULG', '12398').

card_in_set('urza\'s blueprints', 'ULG').
card_original_type('urza\'s blueprints'/'ULG', 'Artifact').
card_original_text('urza\'s blueprints'/'ULG', 'Echo (During your next upkeep after this permanent comes under your control, pay its casting cost or sacrifice it.)\n{T}: Draw a card.').
card_first_print('urza\'s blueprints', 'ULG').
card_image_name('urza\'s blueprints'/'ULG', 'urza\'s blueprints').
card_uid('urza\'s blueprints'/'ULG', 'ULG:Urza\'s Blueprints:urza\'s blueprints').
card_rarity('urza\'s blueprints'/'ULG', 'Rare').
card_artist('urza\'s blueprints'/'ULG', 'Tom Wänerstrand').
card_number('urza\'s blueprints'/'ULG', '137').
card_flavor_text('urza\'s blueprints'/'ULG', 'From concept to paper to reality.').
card_multiverse_id('urza\'s blueprints'/'ULG', '12633').

card_in_set('viashino bey', 'ULG').
card_original_type('viashino bey'/'ULG', 'Summon — Viashino').
card_original_text('viashino bey'/'ULG', 'When Viashino Bey attacks, all creatures you control attack if able.').
card_first_print('viashino bey', 'ULG').
card_image_name('viashino bey'/'ULG', 'viashino bey').
card_uid('viashino bey'/'ULG', 'ULG:Viashino Bey:viashino bey').
card_rarity('viashino bey'/'ULG', 'Common').
card_artist('viashino bey'/'ULG', 'Bradley Williams').
card_number('viashino bey'/'ULG', '93').
card_flavor_text('viashino bey'/'ULG', 'When the bey runs, match his stride.\n—Viashino saying').
card_multiverse_id('viashino bey'/'ULG', '12948').

card_in_set('viashino cutthroat', 'ULG').
card_original_type('viashino cutthroat'/'ULG', 'Summon — Viashino').
card_original_text('viashino cutthroat'/'ULG', 'Viashino Cutthroat is unaffected by summoning sickness.\nAt end of turn, return Viashino Cutthroat to owner\'s hand.').
card_first_print('viashino cutthroat', 'ULG').
card_image_name('viashino cutthroat'/'ULG', 'viashino cutthroat').
card_uid('viashino cutthroat'/'ULG', 'ULG:Viashino Cutthroat:viashino cutthroat').
card_rarity('viashino cutthroat'/'ULG', 'Uncommon').
card_artist('viashino cutthroat'/'ULG', 'Edward P. Beard, Jr.').
card_number('viashino cutthroat'/'ULG', '94').
card_flavor_text('viashino cutthroat'/'ULG', '\"You guys go on ahead. I\'ll catch up with ya.\"\n—Vark, goblin scout, last words').
card_multiverse_id('viashino cutthroat'/'ULG', '12419').

card_in_set('viashino heretic', 'ULG').
card_original_type('viashino heretic'/'ULG', 'Summon — Viashino').
card_original_text('viashino heretic'/'ULG', '{1}{R}, {T}: Destroy target artifact. Viashino Heretic deals to that artifact\'s controller damage equal to the artifact\'s total casting cost.').
card_first_print('viashino heretic', 'ULG').
card_image_name('viashino heretic'/'ULG', 'viashino heretic').
card_uid('viashino heretic'/'ULG', 'ULG:Viashino Heretic:viashino heretic').
card_rarity('viashino heretic'/'ULG', 'Uncommon').
card_artist('viashino heretic'/'ULG', 'Douglas Shuler').
card_number('viashino heretic'/'ULG', '95').
card_flavor_text('viashino heretic'/'ULG', '\"The past is buried for good reason.\"').
card_multiverse_id('viashino heretic'/'ULG', '12424').

card_in_set('viashino sandscout', 'ULG').
card_original_type('viashino sandscout'/'ULG', 'Summon — Viashino').
card_original_text('viashino sandscout'/'ULG', 'Viashino Sandscout is unaffected by summoning sickness.\nAt end of turn, return Viashino Sandscout to owner\'s hand.').
card_first_print('viashino sandscout', 'ULG').
card_image_name('viashino sandscout'/'ULG', 'viashino sandscout').
card_uid('viashino sandscout'/'ULG', 'ULG:Viashino Sandscout:viashino sandscout').
card_rarity('viashino sandscout'/'ULG', 'Common').
card_artist('viashino sandscout'/'ULG', 'Scott M. Fischer').
card_number('viashino sandscout'/'ULG', '96').
card_multiverse_id('viashino sandscout'/'ULG', '12410').

card_in_set('vigilant drake', 'ULG').
card_original_type('vigilant drake'/'ULG', 'Summon — Drake').
card_original_text('vigilant drake'/'ULG', 'Flying\n{2}{U} Untap Vigilant Drake.').
card_first_print('vigilant drake', 'ULG').
card_image_name('vigilant drake'/'ULG', 'vigilant drake').
card_uid('vigilant drake'/'ULG', 'ULG:Vigilant Drake:vigilant drake').
card_rarity('vigilant drake'/'ULG', 'Common').
card_artist('vigilant drake'/'ULG', 'Greg Staples').
card_number('vigilant drake'/'ULG', '46').
card_flavor_text('vigilant drake'/'ULG', 'Awake and awing in the blink of an eye.').
card_multiverse_id('vigilant drake'/'ULG', '12371').

card_in_set('walking sponge', 'ULG').
card_original_type('walking sponge'/'ULG', 'Summon — Sponge').
card_original_text('walking sponge'/'ULG', '{T}: Target creature loses flying, first strike, or trample until end of turn.').
card_first_print('walking sponge', 'ULG').
card_image_name('walking sponge'/'ULG', 'walking sponge').
card_uid('walking sponge'/'ULG', 'ULG:Walking Sponge:walking sponge').
card_rarity('walking sponge'/'ULG', 'Uncommon').
card_artist('walking sponge'/'ULG', 'Ron Spencer').
card_number('walking sponge'/'ULG', '47').
card_flavor_text('walking sponge'/'ULG', 'Not only does it catch fish, it cleans them too.').
card_multiverse_id('walking sponge'/'ULG', '12380').

card_in_set('weatherseed elf', 'ULG').
card_original_type('weatherseed elf'/'ULG', 'Summon — Elf').
card_original_text('weatherseed elf'/'ULG', '{T}: Target creature gains forestwalk until end of turn. (If defending player controls a forest, this creature is unblockable.)').
card_first_print('weatherseed elf', 'ULG').
card_image_name('weatherseed elf'/'ULG', 'weatherseed elf').
card_uid('weatherseed elf'/'ULG', 'ULG:Weatherseed Elf:weatherseed elf').
card_rarity('weatherseed elf'/'ULG', 'Common').
card_artist('weatherseed elf'/'ULG', 'Heather Hudson').
card_number('weatherseed elf'/'ULG', '115').
card_flavor_text('weatherseed elf'/'ULG', '\"My grandmother once told me the future of our world was inside the Weatherseed. When I touched it, I knew she was right.\"').
card_multiverse_id('weatherseed elf'/'ULG', '12430').

card_in_set('weatherseed faeries', 'ULG').
card_original_type('weatherseed faeries'/'ULG', 'Summon — Faeries').
card_original_text('weatherseed faeries'/'ULG', 'Flying, protection from red').
card_first_print('weatherseed faeries', 'ULG').
card_image_name('weatherseed faeries'/'ULG', 'weatherseed faeries').
card_uid('weatherseed faeries'/'ULG', 'ULG:Weatherseed Faeries:weatherseed faeries').
card_rarity('weatherseed faeries'/'ULG', 'Common').
card_artist('weatherseed faeries'/'ULG', 'Hazeltine').
card_number('weatherseed faeries'/'ULG', '48').
card_flavor_text('weatherseed faeries'/'ULG', 'Two days after the forge was completed, the faeries were immune to its flames.').
card_multiverse_id('weatherseed faeries'/'ULG', '12370').

card_in_set('weatherseed treefolk', 'ULG').
card_original_type('weatherseed treefolk'/'ULG', 'Summon — Treefolk').
card_original_text('weatherseed treefolk'/'ULG', 'Trample\nWhen Weatherseed Treefolk is put into a graveyard from play, return Weatherseed Treefolk to owner\'s hand.').
card_first_print('weatherseed treefolk', 'ULG').
card_image_name('weatherseed treefolk'/'ULG', 'weatherseed treefolk').
card_uid('weatherseed treefolk'/'ULG', 'ULG:Weatherseed Treefolk:weatherseed treefolk').
card_rarity('weatherseed treefolk'/'ULG', 'Rare').
card_artist('weatherseed treefolk'/'ULG', 'Heather Hudson').
card_number('weatherseed treefolk'/'ULG', '116').
card_multiverse_id('weatherseed treefolk'/'ULG', '12438').

card_in_set('wheel of torture', 'ULG').
card_original_type('wheel of torture'/'ULG', 'Artifact').
card_original_text('wheel of torture'/'ULG', 'During each of your opponents\' upkeeps, Wheel of Torture deals 1 damage to that player for each card fewer than three in his or her hand.').
card_first_print('wheel of torture', 'ULG').
card_image_name('wheel of torture'/'ULG', 'wheel of torture').
card_uid('wheel of torture'/'ULG', 'ULG:Wheel of Torture:wheel of torture').
card_rarity('wheel of torture'/'ULG', 'Rare').
card_artist('wheel of torture'/'ULG', 'Henry Van Der Linde').
card_number('wheel of torture'/'ULG', '138').
card_flavor_text('wheel of torture'/'ULG', '\"I\'d like to buy a bowel.\"').
card_multiverse_id('wheel of torture'/'ULG', '9706').

card_in_set('wing snare', 'ULG').
card_original_type('wing snare'/'ULG', 'Sorcery').
card_original_text('wing snare'/'ULG', 'Destroy target creature with flying.').
card_first_print('wing snare', 'ULG').
card_image_name('wing snare'/'ULG', 'wing snare').
card_uid('wing snare'/'ULG', 'ULG:Wing Snare:wing snare').
card_rarity('wing snare'/'ULG', 'Uncommon').
card_artist('wing snare'/'ULG', 'Henry Van Der Linde').
card_number('wing snare'/'ULG', '117').
card_flavor_text('wing snare'/'ULG', '\"Argoth\'s doom rained from a clear sky. Yavimaya will not share that fate.\"\n—Multani, maro-sorcerer').
card_multiverse_id('wing snare'/'ULG', '5675').

card_in_set('yavimaya granger', 'ULG').
card_original_type('yavimaya granger'/'ULG', 'Summon — Elf').
card_original_text('yavimaya granger'/'ULG', 'Echo (During your next upkeep after this permanent comes under your control, pay its casting cost or sacrifice it.)\nWhen Yavimaya Granger comes into play, you may search your library for a basic land card and put that land into play tapped. Shuffle your library afterward.').
card_first_print('yavimaya granger', 'ULG').
card_image_name('yavimaya granger'/'ULG', 'yavimaya granger').
card_uid('yavimaya granger'/'ULG', 'ULG:Yavimaya Granger:yavimaya granger').
card_rarity('yavimaya granger'/'ULG', 'Common').
card_artist('yavimaya granger'/'ULG', 'Henry Van Der Linde').
card_number('yavimaya granger'/'ULG', '118').
card_multiverse_id('yavimaya granger'/'ULG', '12656').

card_in_set('yavimaya scion', 'ULG').
card_original_type('yavimaya scion'/'ULG', 'Summon — Treefolk').
card_original_text('yavimaya scion'/'ULG', 'Protection from artifacts').
card_first_print('yavimaya scion', 'ULG').
card_image_name('yavimaya scion'/'ULG', 'yavimaya scion').
card_uid('yavimaya scion'/'ULG', 'ULG:Yavimaya Scion:yavimaya scion').
card_rarity('yavimaya scion'/'ULG', 'Common').
card_artist('yavimaya scion'/'ULG', 'DiTerlizzi').
card_number('yavimaya scion'/'ULG', '119').
card_flavor_text('yavimaya scion'/'ULG', 'Each time the saw blade bit, the tree spat it out.').
card_multiverse_id('yavimaya scion'/'ULG', '12429').

card_in_set('yavimaya wurm', 'ULG').
card_original_type('yavimaya wurm'/'ULG', 'Summon — Wurm').
card_original_text('yavimaya wurm'/'ULG', 'Trample').
card_first_print('yavimaya wurm', 'ULG').
card_image_name('yavimaya wurm'/'ULG', 'yavimaya wurm').
card_uid('yavimaya wurm'/'ULG', 'ULG:Yavimaya Wurm:yavimaya wurm').
card_rarity('yavimaya wurm'/'ULG', 'Common').
card_artist('yavimaya wurm'/'ULG', 'Melissa A. Benson').
card_number('yavimaya wurm'/'ULG', '120').
card_flavor_text('yavimaya wurm'/'ULG', 'When the gorilla playfully grabbed the wurm\'s tail, the wurm doubled back and playfully ate the gorilla\'s head.').
card_multiverse_id('yavimaya wurm'/'ULG', '5571').
