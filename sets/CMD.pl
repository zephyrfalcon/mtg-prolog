% Magic: The Gathering-Commander

set('CMD').
set_name('CMD', 'Magic: The Gathering-Commander').
set_release_date('CMD', '2011-06-17').
set_border('CMD', 'black').
set_type('CMD', 'commander').

card_in_set('acidic slime', 'CMD').
card_original_type('acidic slime'/'CMD', 'Creature — Ooze').
card_original_text('acidic slime'/'CMD', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)\nWhen Acidic Slime enters the battlefield, destroy target artifact, enchantment, or land.').
card_image_name('acidic slime'/'CMD', 'acidic slime').
card_uid('acidic slime'/'CMD', 'CMD:Acidic Slime:acidic slime').
card_rarity('acidic slime'/'CMD', 'Uncommon').
card_artist('acidic slime'/'CMD', 'Karl Kopinski').
card_number('acidic slime'/'CMD', '140').
card_multiverse_id('acidic slime'/'CMD', '247317').

card_in_set('acorn catapult', 'CMD').
card_original_type('acorn catapult'/'CMD', 'Artifact').
card_original_text('acorn catapult'/'CMD', '{1}, {T}: Acorn Catapult deals 1 damage to target creature or player. That creature\'s controller or that player puts a 1/1 green Squirrel creature token onto the battlefield.').
card_first_print('acorn catapult', 'CMD').
card_image_name('acorn catapult'/'CMD', 'acorn catapult').
card_uid('acorn catapult'/'CMD', 'CMD:Acorn Catapult:acorn catapult').
card_rarity('acorn catapult'/'CMD', 'Rare').
card_artist('acorn catapult'/'CMD', 'Jesper Ejsing').
card_number('acorn catapult'/'CMD', '241').
card_flavor_text('acorn catapult'/'CMD', 'The Kalonian word for \"ammunition\" is the same as the word for \"bait.\"').
card_multiverse_id('acorn catapult'/'CMD', '233197').

card_in_set('æthersnipe', 'CMD').
card_original_type('æthersnipe'/'CMD', 'Creature — Elemental').
card_original_text('æthersnipe'/'CMD', 'When Æthersnipe enters the battlefield, return target nonland permanent to its owner\'s hand.\nEvoke {1}{U}{U} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_image_name('æthersnipe'/'CMD', 'aethersnipe').
card_uid('æthersnipe'/'CMD', 'CMD:Æthersnipe:aethersnipe').
card_rarity('æthersnipe'/'CMD', 'Common').
card_artist('æthersnipe'/'CMD', 'Zoltan Boros & Gabor Szikszai').
card_number('æthersnipe'/'CMD', '39').
card_multiverse_id('æthersnipe'/'CMD', '247295').

card_in_set('afterlife', 'CMD').
card_original_type('afterlife'/'CMD', 'Instant').
card_original_text('afterlife'/'CMD', 'Destroy target creature. It can\'t be regenerated. Its controller puts a 1/1 white Spirit creature token with flying onto the battlefield.').
card_image_name('afterlife'/'CMD', 'afterlife').
card_uid('afterlife'/'CMD', 'CMD:Afterlife:afterlife').
card_rarity('afterlife'/'CMD', 'Uncommon').
card_artist('afterlife'/'CMD', 'Brian Snõddy').
card_number('afterlife'/'CMD', '2').
card_multiverse_id('afterlife'/'CMD', '247330').

card_in_set('akoum refuge', 'CMD').
card_original_type('akoum refuge'/'CMD', 'Land').
card_original_text('akoum refuge'/'CMD', 'Akoum Refuge enters the battlefield tapped.\nWhen Akoum Refuge enters the battlefield, you gain 1 life.\n{T}: Add {B} or {R} to your mana pool.').
card_image_name('akoum refuge'/'CMD', 'akoum refuge').
card_uid('akoum refuge'/'CMD', 'CMD:Akoum Refuge:akoum refuge').
card_rarity('akoum refuge'/'CMD', 'Uncommon').
card_artist('akoum refuge'/'CMD', 'Fred Fields').
card_number('akoum refuge'/'CMD', '264').
card_multiverse_id('akoum refuge'/'CMD', '247543').

card_in_set('akroma\'s vengeance', 'CMD').
card_original_type('akroma\'s vengeance'/'CMD', 'Sorcery').
card_original_text('akroma\'s vengeance'/'CMD', 'Destroy all artifacts, creatures, and enchantments.\nCycling {3} ({3}, Discard this card: Draw a card.)').
card_image_name('akroma\'s vengeance'/'CMD', 'akroma\'s vengeance').
card_uid('akroma\'s vengeance'/'CMD', 'CMD:Akroma\'s Vengeance:akroma\'s vengeance').
card_rarity('akroma\'s vengeance'/'CMD', 'Rare').
card_artist('akroma\'s vengeance'/'CMD', 'Greg & Tim Hildebrandt').
card_number('akroma\'s vengeance'/'CMD', '3').
card_flavor_text('akroma\'s vengeance'/'CMD', 'Ixidor had only to imagine their ruin and Akroma made it so.').
card_multiverse_id('akroma\'s vengeance'/'CMD', '247343').

card_in_set('akroma, angel of fury', 'CMD').
card_original_type('akroma, angel of fury'/'CMD', 'Legendary Creature — Angel').
card_original_text('akroma, angel of fury'/'CMD', 'Akroma, Angel of Fury can\'t be countered.\nFlying, trample, protection from white and from blue\n{R}: Akroma, Angel of Fury gets +1/+0 until end of turn.\nMorph {3}{R}{R}{R} (You may cast this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_image_name('akroma, angel of fury'/'CMD', 'akroma, angel of fury').
card_uid('akroma, angel of fury'/'CMD', 'CMD:Akroma, Angel of Fury:akroma, angel of fury').
card_rarity('akroma, angel of fury'/'CMD', 'Rare').
card_artist('akroma, angel of fury'/'CMD', 'Daren Bader').
card_number('akroma, angel of fury'/'CMD', '108').
card_multiverse_id('akroma, angel of fury'/'CMD', '247358').

card_in_set('alliance of arms', 'CMD').
card_original_type('alliance of arms'/'CMD', 'Sorcery').
card_original_text('alliance of arms'/'CMD', 'Join forces — Starting with you, each player may pay any amount of mana. Each player puts X 1/1 white Soldier creature tokens onto the battlefield, where X is the total amount of mana paid this way.').
card_first_print('alliance of arms', 'CMD').
card_image_name('alliance of arms'/'CMD', 'alliance of arms').
card_uid('alliance of arms'/'CMD', 'CMD:Alliance of Arms:alliance of arms').
card_rarity('alliance of arms'/'CMD', 'Rare').
card_artist('alliance of arms'/'CMD', 'Johann Bodin').
card_number('alliance of arms'/'CMD', '4').
card_multiverse_id('alliance of arms'/'CMD', '228218').

card_in_set('angel of despair', 'CMD').
card_original_type('angel of despair'/'CMD', 'Creature — Angel').
card_original_text('angel of despair'/'CMD', 'Flying\nWhen Angel of Despair enters the battlefield, destroy target permanent.').
card_image_name('angel of despair'/'CMD', 'angel of despair').
card_uid('angel of despair'/'CMD', 'CMD:Angel of Despair:angel of despair').
card_rarity('angel of despair'/'CMD', 'Rare').
card_artist('angel of despair'/'CMD', 'Todd Lockwood').
card_number('angel of despair'/'CMD', '180').
card_flavor_text('angel of despair'/'CMD', '\"I feel in them a sense of duty and commitment, yet I can feel nothing else. It is as if their duty is to an empty void.\"\n—Razia, Boros archangel').
card_multiverse_id('angel of despair'/'CMD', '247275').

card_in_set('angelic arbiter', 'CMD').
card_original_type('angelic arbiter'/'CMD', 'Creature — Angel').
card_original_text('angelic arbiter'/'CMD', 'Flying\nEach opponent who cast a spell this turn can\'t attack with creatures.\nEach opponent who attacked with a creature this turn can\'t cast spells.').
card_image_name('angelic arbiter'/'CMD', 'angelic arbiter').
card_uid('angelic arbiter'/'CMD', 'CMD:Angelic Arbiter:angelic arbiter').
card_rarity('angelic arbiter'/'CMD', 'Rare').
card_artist('angelic arbiter'/'CMD', 'Steve Argyle').
card_number('angelic arbiter'/'CMD', '5').
card_multiverse_id('angelic arbiter'/'CMD', '247318').

card_in_set('anger', 'CMD').
card_original_type('anger'/'CMD', 'Creature — Incarnation').
card_original_text('anger'/'CMD', 'Haste\nAs long as Anger is in your graveyard and you control a Mountain, creatures you control have haste.').
card_image_name('anger'/'CMD', 'anger').
card_uid('anger'/'CMD', 'CMD:Anger:anger').
card_rarity('anger'/'CMD', 'Uncommon').
card_artist('anger'/'CMD', 'John Avon').
card_number('anger'/'CMD', '109').
card_flavor_text('anger'/'CMD', '\"For its time as a mortal, Anger chose a shell of boiling rock.\"\n—Scroll of Beginnings').
card_multiverse_id('anger'/'CMD', '247289').

card_in_set('animar, soul of elements', 'CMD').
card_original_type('animar, soul of elements'/'CMD', 'Legendary Creature — Elemental').
card_original_text('animar, soul of elements'/'CMD', 'Protection from white and from black\nWhenever you cast a creature spell, put a +1/+1 counter on Animar, Soul of Elements.\nCreature spells you cast cost {1} less to cast for each +1/+1 counter on Animar.').
card_first_print('animar, soul of elements', 'CMD').
card_image_name('animar, soul of elements'/'CMD', 'animar, soul of elements').
card_uid('animar, soul of elements'/'CMD', 'CMD:Animar, Soul of Elements:animar, soul of elements').
card_rarity('animar, soul of elements'/'CMD', 'Mythic Rare').
card_artist('animar, soul of elements'/'CMD', 'Peter Mohrbacher').
card_number('animar, soul of elements'/'CMD', '181').
card_multiverse_id('animar, soul of elements'/'CMD', '236504').

card_in_set('aquastrand spider', 'CMD').
card_original_type('aquastrand spider'/'CMD', 'Creature — Spider Mutant').
card_original_text('aquastrand spider'/'CMD', 'Graft 2 (This creature enters the battlefield with two +1/+1 counters on it. Whenever another creature enters the battlefield, you may move a +1/+1 counter from this creature onto it.)\n{G}: Target creature with a +1/+1 counter on it gains reach until end of turn. (It can block creatures with flying.)').
card_image_name('aquastrand spider'/'CMD', 'aquastrand spider').
card_uid('aquastrand spider'/'CMD', 'CMD:Aquastrand Spider:aquastrand spider').
card_rarity('aquastrand spider'/'CMD', 'Common').
card_artist('aquastrand spider'/'CMD', 'Dany Orizio').
card_number('aquastrand spider'/'CMD', '141').
card_multiverse_id('aquastrand spider'/'CMD', '247188').

card_in_set('arbiter of knollridge', 'CMD').
card_original_type('arbiter of knollridge'/'CMD', 'Creature — Giant Wizard').
card_original_text('arbiter of knollridge'/'CMD', 'Vigilance\nWhen Arbiter of Knollridge enters the battlefield, each player\'s life total becomes the highest life total among all players.').
card_image_name('arbiter of knollridge'/'CMD', 'arbiter of knollridge').
card_uid('arbiter of knollridge'/'CMD', 'CMD:Arbiter of Knollridge:arbiter of knollridge').
card_rarity('arbiter of knollridge'/'CMD', 'Rare').
card_artist('arbiter of knollridge'/'CMD', 'Brandon Dorman').
card_number('arbiter of knollridge'/'CMD', '6').
card_flavor_text('arbiter of knollridge'/'CMD', 'Though giants are mortal, they live so long and on such a grand scale that many small folk don\'t believe they ever truly die.').
card_multiverse_id('arbiter of knollridge'/'CMD', '247296').

card_in_set('archangel of strife', 'CMD').
card_original_type('archangel of strife'/'CMD', 'Creature — Angel').
card_original_text('archangel of strife'/'CMD', 'Flying\nAs Archangel of Strife enters the battlefield, each player chooses war or peace.\nCreatures controlled by players who chose war get +3/+0.\nCreatures controlled by players who chose peace get +0/+3.').
card_first_print('archangel of strife', 'CMD').
card_image_name('archangel of strife'/'CMD', 'archangel of strife').
card_uid('archangel of strife'/'CMD', 'CMD:Archangel of Strife:archangel of strife').
card_rarity('archangel of strife'/'CMD', 'Rare').
card_artist('archangel of strife'/'CMD', 'Greg Staples').
card_number('archangel of strife'/'CMD', '7').
card_multiverse_id('archangel of strife'/'CMD', '228246').

card_in_set('armillary sphere', 'CMD').
card_original_type('armillary sphere'/'CMD', 'Artifact').
card_original_text('armillary sphere'/'CMD', '{2}, {T}, Sacrifice Armillary Sphere: Search your library for up to two basic land cards, reveal them, and put them into your hand. Then shuffle your library.').
card_image_name('armillary sphere'/'CMD', 'armillary sphere').
card_uid('armillary sphere'/'CMD', 'CMD:Armillary Sphere:armillary sphere').
card_rarity('armillary sphere'/'CMD', 'Common').
card_artist('armillary sphere'/'CMD', 'Franz Vohwinkel').
card_number('armillary sphere'/'CMD', '242').
card_flavor_text('armillary sphere'/'CMD', 'The mysterious purpose of two of the rings had eluded Esper mages—until now.').
card_multiverse_id('armillary sphere'/'CMD', '247177').

card_in_set('artisan of kozilek', 'CMD').
card_original_type('artisan of kozilek'/'CMD', 'Creature — Eldrazi').
card_original_text('artisan of kozilek'/'CMD', 'When you cast Artisan of Kozilek, you may return target creature card from your graveyard to the battlefield.\nAnnihilator 2 (Whenever this creature attacks, defending player sacrifices two permanents.)').
card_image_name('artisan of kozilek'/'CMD', 'artisan of kozilek').
card_uid('artisan of kozilek'/'CMD', 'CMD:Artisan of Kozilek:artisan of kozilek').
card_rarity('artisan of kozilek'/'CMD', 'Uncommon').
card_artist('artisan of kozilek'/'CMD', 'Jason Felix').
card_number('artisan of kozilek'/'CMD', '1').
card_multiverse_id('artisan of kozilek'/'CMD', '247392').

card_in_set('attrition', 'CMD').
card_original_type('attrition'/'CMD', 'Enchantment').
card_original_text('attrition'/'CMD', '{B}, Sacrifice a creature: Destroy target nonblack creature.').
card_image_name('attrition'/'CMD', 'attrition').
card_uid('attrition'/'CMD', 'CMD:Attrition:attrition').
card_rarity('attrition'/'CMD', 'Rare').
card_artist('attrition'/'CMD', 'Scott M. Fischer').
card_number('attrition'/'CMD', '72').
card_flavor_text('attrition'/'CMD', '\"I will trade life for life with the insurgents. Our resources, unlike theirs, are limitless.\"\n—Davvol, Evincar of Rath').
card_multiverse_id('attrition'/'CMD', '247522').

card_in_set('aura shards', 'CMD').
card_original_type('aura shards'/'CMD', 'Enchantment').
card_original_text('aura shards'/'CMD', 'Whenever a creature enters the battlefield under your control, you may destroy target artifact or enchantment.').
card_image_name('aura shards'/'CMD', 'aura shards').
card_uid('aura shards'/'CMD', 'CMD:Aura Shards:aura shards').
card_rarity('aura shards'/'CMD', 'Uncommon').
card_artist('aura shards'/'CMD', 'Ron Spencer').
card_number('aura shards'/'CMD', '182').
card_flavor_text('aura shards'/'CMD', 'Gaea forged her soldiers into self-wielding weapons that struck down all impurities.').
card_multiverse_id('aura shards'/'CMD', '247184').

card_in_set('austere command', 'CMD').
card_original_type('austere command'/'CMD', 'Sorcery').
card_original_text('austere command'/'CMD', 'Choose two — Destroy all artifacts; or destroy all enchantments; or destroy all creatures with converted mana cost 3 or less; or destroy all creatures with converted mana cost 4 or greater.').
card_image_name('austere command'/'CMD', 'austere command').
card_uid('austere command'/'CMD', 'CMD:Austere Command:austere command').
card_rarity('austere command'/'CMD', 'Rare').
card_artist('austere command'/'CMD', 'Wayne England').
card_number('austere command'/'CMD', '8').
card_multiverse_id('austere command'/'CMD', '247297').

card_in_set('avatar of fury', 'CMD').
card_original_type('avatar of fury'/'CMD', 'Creature — Avatar').
card_original_text('avatar of fury'/'CMD', 'If an opponent controls seven or more lands, Avatar of Fury costs {6} less to cast.\nFlying\n{R}: Avatar of Fury gets +1/+0 until end of turn.').
card_image_name('avatar of fury'/'CMD', 'avatar of fury').
card_uid('avatar of fury'/'CMD', 'CMD:Avatar of Fury:avatar of fury').
card_rarity('avatar of fury'/'CMD', 'Rare').
card_artist('avatar of fury'/'CMD', 'rk post').
card_number('avatar of fury'/'CMD', '110').
card_multiverse_id('avatar of fury'/'CMD', '247367').

card_in_set('avatar of slaughter', 'CMD').
card_original_type('avatar of slaughter'/'CMD', 'Creature — Avatar').
card_original_text('avatar of slaughter'/'CMD', 'All creatures have double strike and attack each turn if able.').
card_first_print('avatar of slaughter', 'CMD').
card_image_name('avatar of slaughter'/'CMD', 'avatar of slaughter').
card_uid('avatar of slaughter'/'CMD', 'CMD:Avatar of Slaughter:avatar of slaughter').
card_rarity('avatar of slaughter'/'CMD', 'Rare').
card_artist('avatar of slaughter'/'CMD', 'Jason A. Engle').
card_number('avatar of slaughter'/'CMD', '111').
card_flavor_text('avatar of slaughter'/'CMD', '\"Diplomacy has solved nothing. Only bloodspill can end this now. Call forth the warbringer.\"\n—Basandra, Battle Seraph').
card_multiverse_id('avatar of slaughter'/'CMD', '236497').

card_in_set('avatar of woe', 'CMD').
card_original_type('avatar of woe'/'CMD', 'Creature — Avatar').
card_original_text('avatar of woe'/'CMD', 'If there are ten or more creature cards total in all graveyards, Avatar of Woe costs {6} less to cast.\nFear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\n{T}: Destroy target creature. It can\'t be regenerated.').
card_image_name('avatar of woe'/'CMD', 'avatar of woe').
card_uid('avatar of woe'/'CMD', 'CMD:Avatar of Woe:avatar of woe').
card_rarity('avatar of woe'/'CMD', 'Rare').
card_artist('avatar of woe'/'CMD', 'rk post').
card_number('avatar of woe'/'CMD', '73').
card_multiverse_id('avatar of woe'/'CMD', '247520').

card_in_set('awakening zone', 'CMD').
card_original_type('awakening zone'/'CMD', 'Enchantment').
card_original_text('awakening zone'/'CMD', 'At the beginning of your upkeep, you may put a 0/1 colorless Eldrazi Spawn creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_image_name('awakening zone'/'CMD', 'awakening zone').
card_uid('awakening zone'/'CMD', 'CMD:Awakening Zone:awakening zone').
card_rarity('awakening zone'/'CMD', 'Rare').
card_artist('awakening zone'/'CMD', 'Johann Bodin').
card_number('awakening zone'/'CMD', '142').
card_flavor_text('awakening zone'/'CMD', 'The ground erupted in time with the hedron\'s thrum, a dirge of the last days.').
card_multiverse_id('awakening zone'/'CMD', '247393').

card_in_set('azorius chancery', 'CMD').
card_original_type('azorius chancery'/'CMD', 'Land').
card_original_text('azorius chancery'/'CMD', 'Azorius Chancery enters the battlefield tapped.\nWhen Azorius Chancery enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {W}{U} to your mana pool.').
card_image_name('azorius chancery'/'CMD', 'azorius chancery').
card_uid('azorius chancery'/'CMD', 'CMD:Azorius Chancery:azorius chancery').
card_rarity('azorius chancery'/'CMD', 'Common').
card_artist('azorius chancery'/'CMD', 'John Avon').
card_number('azorius chancery'/'CMD', '265').
card_multiverse_id('azorius chancery'/'CMD', '247189').

card_in_set('azorius guildmage', 'CMD').
card_original_type('azorius guildmage'/'CMD', 'Creature — Vedalken Wizard').
card_original_text('azorius guildmage'/'CMD', '{2}{W}: Tap target creature.\n{2}{U}: Counter target activated ability. (Mana abilities can\'t be targeted.)').
card_image_name('azorius guildmage'/'CMD', 'azorius guildmage').
card_uid('azorius guildmage'/'CMD', 'CMD:Azorius Guildmage:azorius guildmage').
card_rarity('azorius guildmage'/'CMD', 'Uncommon').
card_artist('azorius guildmage'/'CMD', 'Christopher Moeller').
card_number('azorius guildmage'/'CMD', '183').
card_multiverse_id('azorius guildmage'/'CMD', '247190').

card_in_set('baloth woodcrasher', 'CMD').
card_original_type('baloth woodcrasher'/'CMD', 'Creature — Beast').
card_original_text('baloth woodcrasher'/'CMD', 'Landfall — Whenever a land enters the battlefield under your control, Baloth Woodcrasher gets +4/+4 and gains trample until end of turn.').
card_image_name('baloth woodcrasher'/'CMD', 'baloth woodcrasher').
card_uid('baloth woodcrasher'/'CMD', 'CMD:Baloth Woodcrasher:baloth woodcrasher').
card_rarity('baloth woodcrasher'/'CMD', 'Uncommon').
card_artist('baloth woodcrasher'/'CMD', 'Zoltan Boros & Gabor Szikszai').
card_number('baloth woodcrasher'/'CMD', '143').
card_flavor_text('baloth woodcrasher'/'CMD', 'Its insatiable hunger quickly depletes a region of prey. It must migrate from place to place to feed its massive bulk.').
card_multiverse_id('baloth woodcrasher'/'CMD', '247544').

card_in_set('barren moor', 'CMD').
card_original_type('barren moor'/'CMD', 'Land').
card_original_text('barren moor'/'CMD', 'Barren Moor enters the battlefield tapped.\n{T}: Add {B} to your mana pool.\nCycling {B} ({B}, Discard this card: Draw a card.)').
card_image_name('barren moor'/'CMD', 'barren moor').
card_uid('barren moor'/'CMD', 'CMD:Barren Moor:barren moor').
card_rarity('barren moor'/'CMD', 'Common').
card_artist('barren moor'/'CMD', 'Heather Hudson').
card_number('barren moor'/'CMD', '266').
card_multiverse_id('barren moor'/'CMD', '247344').

card_in_set('basandra, battle seraph', 'CMD').
card_original_type('basandra, battle seraph'/'CMD', 'Legendary Creature — Angel').
card_original_text('basandra, battle seraph'/'CMD', 'Flying\nPlayers can\'t cast spells during combat.\n{R}: Target creature attacks this turn if able.').
card_first_print('basandra, battle seraph', 'CMD').
card_image_name('basandra, battle seraph'/'CMD', 'basandra, battle seraph').
card_uid('basandra, battle seraph'/'CMD', 'CMD:Basandra, Battle Seraph:basandra, battle seraph').
card_rarity('basandra, battle seraph'/'CMD', 'Rare').
card_artist('basandra, battle seraph'/'CMD', 'Terese Nielsen').
card_number('basandra, battle seraph'/'CMD', '184').
card_flavor_text('basandra, battle seraph'/'CMD', '\"Why listen to the mumblings of wizards when the lash speaks true?\"').
card_multiverse_id('basandra, battle seraph'/'CMD', '236490').

card_in_set('bathe in light', 'CMD').
card_original_type('bathe in light'/'CMD', 'Instant').
card_original_text('bathe in light'/'CMD', 'Radiance — Choose a color. Target creature and each other creature that shares a color with it gain protection from the chosen color until end of turn.').
card_image_name('bathe in light'/'CMD', 'bathe in light').
card_uid('bathe in light'/'CMD', 'CMD:Bathe in Light:bathe in light').
card_rarity('bathe in light'/'CMD', 'Uncommon').
card_artist('bathe in light'/'CMD', 'Alex Horley-Orlandelli').
card_number('bathe in light'/'CMD', '9').
card_flavor_text('bathe in light'/'CMD', '\"Truth shines even in darkness. Those who march on the side of truth walk always in righteous light.\"').
card_multiverse_id('bathe in light'/'CMD', '247370').

card_in_set('bestial menace', 'CMD').
card_original_type('bestial menace'/'CMD', 'Sorcery').
card_original_text('bestial menace'/'CMD', 'Put a 1/1 green Snake creature token, a 2/2 green Wolf creature token, and a 3/3 green Elephant creature token onto the battlefield.').
card_image_name('bestial menace'/'CMD', 'bestial menace').
card_uid('bestial menace'/'CMD', 'CMD:Bestial Menace:bestial menace').
card_rarity('bestial menace'/'CMD', 'Uncommon').
card_artist('bestial menace'/'CMD', 'Andrew Robinson').
card_number('bestial menace'/'CMD', '144').
card_flavor_text('bestial menace'/'CMD', '\"My battle cry reaches ears far keener than yours.\"\n—Saidah, Joraga hunter').
card_multiverse_id('bestial menace'/'CMD', '247535').

card_in_set('bladewing the risen', 'CMD').
card_original_type('bladewing the risen'/'CMD', 'Legendary Creature — Zombie Dragon').
card_original_text('bladewing the risen'/'CMD', 'Flying\nWhen Bladewing the Risen enters the battlefield, you may return target Dragon permanent card from your graveyard to the battlefield.\n{B}{R}: Dragon creatures get +1/+1 until end of turn.').
card_image_name('bladewing the risen'/'CMD', 'bladewing the risen').
card_uid('bladewing the risen'/'CMD', 'CMD:Bladewing the Risen:bladewing the risen').
card_rarity('bladewing the risen'/'CMD', 'Rare').
card_artist('bladewing the risen'/'CMD', 'Kev Walker').
card_number('bladewing the risen'/'CMD', '185').
card_multiverse_id('bladewing the risen'/'CMD', '247401').

card_in_set('bojuka bog', 'CMD').
card_original_type('bojuka bog'/'CMD', 'Land').
card_original_text('bojuka bog'/'CMD', 'Bojuka Bog enters the battlefield tapped.\nWhen Bojuka Bog enters the battlefield, exile all cards from target player\'s graveyard.\n{T}: Add {B} to your mana pool.').
card_image_name('bojuka bog'/'CMD', 'bojuka bog').
card_uid('bojuka bog'/'CMD', 'CMD:Bojuka Bog:bojuka bog').
card_rarity('bojuka bog'/'CMD', 'Common').
card_artist('bojuka bog'/'CMD', 'Howard Lyon').
card_number('bojuka bog'/'CMD', '267').
card_multiverse_id('bojuka bog'/'CMD', '247536').

card_in_set('boros garrison', 'CMD').
card_original_type('boros garrison'/'CMD', 'Land').
card_original_text('boros garrison'/'CMD', 'Boros Garrison enters the battlefield tapped.\nWhen Boros Garrison enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {R}{W} to your mana pool.').
card_image_name('boros garrison'/'CMD', 'boros garrison').
card_uid('boros garrison'/'CMD', 'CMD:Boros Garrison:boros garrison').
card_rarity('boros garrison'/'CMD', 'Common').
card_artist('boros garrison'/'CMD', 'John Avon').
card_number('boros garrison'/'CMD', '268').
card_multiverse_id('boros garrison'/'CMD', '247371').

card_in_set('boros guildmage', 'CMD').
card_original_type('boros guildmage'/'CMD', 'Creature — Human Wizard').
card_original_text('boros guildmage'/'CMD', '{1}{R}: Target creature gains haste until end of turn.\n{1}{W}: Target creature gains first strike until end of turn.').
card_image_name('boros guildmage'/'CMD', 'boros guildmage').
card_uid('boros guildmage'/'CMD', 'CMD:Boros Guildmage:boros guildmage').
card_rarity('boros guildmage'/'CMD', 'Uncommon').
card_artist('boros guildmage'/'CMD', 'Paolo Parente').
card_number('boros guildmage'/'CMD', '186').
card_multiverse_id('boros guildmage'/'CMD', '247372').

card_in_set('boros signet', 'CMD').
card_original_type('boros signet'/'CMD', 'Artifact').
card_original_text('boros signet'/'CMD', '{1}, {T}: Add {R}{W} to your mana pool.').
card_image_name('boros signet'/'CMD', 'boros signet').
card_uid('boros signet'/'CMD', 'CMD:Boros Signet:boros signet').
card_rarity('boros signet'/'CMD', 'Common').
card_artist('boros signet'/'CMD', 'Greg Hildebrandt').
card_number('boros signet'/'CMD', '243').
card_flavor_text('boros signet'/'CMD', '\"Have you ever held a Boros signet? There\'s a weight to it that belies its size—a weight of strength and of pride.\"\n—Agrus Kos').
card_multiverse_id('boros signet'/'CMD', '247373').

card_in_set('brainstorm', 'CMD').
card_original_type('brainstorm'/'CMD', 'Instant').
card_original_text('brainstorm'/'CMD', 'Draw three cards, then put two cards from your hand on top of your library in any order.').
card_image_name('brainstorm'/'CMD', 'brainstorm').
card_uid('brainstorm'/'CMD', 'CMD:Brainstorm:brainstorm').
card_rarity('brainstorm'/'CMD', 'Common').
card_artist('brainstorm'/'CMD', 'DiTerlizzi').
card_number('brainstorm'/'CMD', '40').
card_flavor_text('brainstorm'/'CMD', '\"I see more than others do because I know where to look.\"\n—Saprazzan vizier').
card_multiverse_id('brainstorm'/'CMD', '247331').

card_in_set('brawn', 'CMD').
card_original_type('brawn'/'CMD', 'Creature — Incarnation').
card_original_text('brawn'/'CMD', 'Trample\nAs long as Brawn is in your graveyard and you control a Forest, creatures you control have trample.').
card_image_name('brawn'/'CMD', 'brawn').
card_uid('brawn'/'CMD', 'CMD:Brawn:brawn').
card_rarity('brawn'/'CMD', 'Uncommon').
card_artist('brawn'/'CMD', 'Matt Cavotta').
card_number('brawn'/'CMD', '145').
card_flavor_text('brawn'/'CMD', '\"‘I have arrived,\' bellowed Brawn, and the plane shuddered.\"\n—Scroll of Beginnings').
card_multiverse_id('brawn'/'CMD', '247290').

card_in_set('breath of darigaaz', 'CMD').
card_original_type('breath of darigaaz'/'CMD', 'Sorcery').
card_original_text('breath of darigaaz'/'CMD', 'Kicker {2} (You may pay an additional {2} as you cast this spell.)\nBreath of Darigaaz deals 1 damage to each creature without flying and each player. If Breath of Darigaaz was kicked, it deals 4 damage to each creature without flying and each player instead.').
card_image_name('breath of darigaaz'/'CMD', 'breath of darigaaz').
card_uid('breath of darigaaz'/'CMD', 'CMD:Breath of Darigaaz:breath of darigaaz').
card_rarity('breath of darigaaz'/'CMD', 'Uncommon').
card_artist('breath of darigaaz'/'CMD', 'Greg & Tim Hildebrandt').
card_number('breath of darigaaz'/'CMD', '112').
card_multiverse_id('breath of darigaaz'/'CMD', '247185').

card_in_set('brion stoutarm', 'CMD').
card_original_type('brion stoutarm'/'CMD', 'Legendary Creature — Giant Warrior').
card_original_text('brion stoutarm'/'CMD', 'Lifelink\n{R}, {T}, Sacrifice a creature other than Brion Stoutarm: Brion Stoutarm deals damage equal to the sacrificed creature\'s power to target player.').
card_image_name('brion stoutarm'/'CMD', 'brion stoutarm').
card_uid('brion stoutarm'/'CMD', 'CMD:Brion Stoutarm:brion stoutarm').
card_rarity('brion stoutarm'/'CMD', 'Rare').
card_artist('brion stoutarm'/'CMD', 'Zoltan Boros & Gabor Szikszai').
card_number('brion stoutarm'/'CMD', '187').
card_multiverse_id('brion stoutarm'/'CMD', '247298').

card_in_set('buried alive', 'CMD').
card_original_type('buried alive'/'CMD', 'Sorcery').
card_original_text('buried alive'/'CMD', 'Search your library for up to three creature cards and put them into your graveyard. Then shuffle your library.').
card_image_name('buried alive'/'CMD', 'buried alive').
card_uid('buried alive'/'CMD', 'CMD:Buried Alive:buried alive').
card_rarity('buried alive'/'CMD', 'Uncommon').
card_artist('buried alive'/'CMD', 'Greg Staples').
card_number('buried alive'/'CMD', '74').
card_flavor_text('buried alive'/'CMD', 'The scrape of shovels and the tumble of cold dirt soon muffled their pleas.').
card_multiverse_id('buried alive'/'CMD', '247342').

card_in_set('butcher of malakir', 'CMD').
card_original_type('butcher of malakir'/'CMD', 'Creature — Vampire Warrior').
card_original_text('butcher of malakir'/'CMD', 'Flying\nWhenever Butcher of Malakir or another creature you control is put into a graveyard from the battlefield, each opponent sacrifices a creature.').
card_image_name('butcher of malakir'/'CMD', 'butcher of malakir').
card_uid('butcher of malakir'/'CMD', 'CMD:Butcher of Malakir:butcher of malakir').
card_rarity('butcher of malakir'/'CMD', 'Rare').
card_artist('butcher of malakir'/'CMD', 'Jason Chan').
card_number('butcher of malakir'/'CMD', '75').
card_flavor_text('butcher of malakir'/'CMD', 'His verdict is always guilty. His sentence is always death.').
card_multiverse_id('butcher of malakir'/'CMD', '247537').

card_in_set('call the skybreaker', 'CMD').
card_original_type('call the skybreaker'/'CMD', 'Sorcery').
card_original_text('call the skybreaker'/'CMD', 'Put a 5/5 blue and red Elemental creature token with flying onto the battlefield.\nRetrace (You may cast this card from your graveyard by discarding a land card in addition to paying its other costs.)').
card_image_name('call the skybreaker'/'CMD', 'call the skybreaker').
card_uid('call the skybreaker'/'CMD', 'CMD:Call the Skybreaker:call the skybreaker').
card_rarity('call the skybreaker'/'CMD', 'Rare').
card_artist('call the skybreaker'/'CMD', 'Randy Gallegos').
card_number('call the skybreaker'/'CMD', '188').
card_flavor_text('call the skybreaker'/'CMD', 'It hears and answers every orison across Shadowmoor.').
card_multiverse_id('call the skybreaker'/'CMD', '247202').

card_in_set('celestial force', 'CMD').
card_original_type('celestial force'/'CMD', 'Creature — Elemental').
card_original_text('celestial force'/'CMD', 'At the beginning of each upkeep, you gain 3 life.').
card_first_print('celestial force', 'CMD').
card_image_name('celestial force'/'CMD', 'celestial force').
card_uid('celestial force'/'CMD', 'CMD:Celestial Force:celestial force').
card_rarity('celestial force'/'CMD', 'Rare').
card_artist('celestial force'/'CMD', 'Véronique Meignaud').
card_number('celestial force'/'CMD', '10').
card_flavor_text('celestial force'/'CMD', '\"Wishing upon only one star seems like a failure of imagination.\"\n—Vish Kal, Blood Arbiter').
card_multiverse_id('celestial force'/'CMD', '228257').

card_in_set('chain reaction', 'CMD').
card_original_type('chain reaction'/'CMD', 'Sorcery').
card_original_text('chain reaction'/'CMD', 'Chain Reaction deals X damage to each creature, where X is the number of creatures on the battlefield.').
card_image_name('chain reaction'/'CMD', 'chain reaction').
card_uid('chain reaction'/'CMD', 'CMD:Chain Reaction:chain reaction').
card_rarity('chain reaction'/'CMD', 'Rare').
card_artist('chain reaction'/'CMD', 'Trevor Claxton').
card_number('chain reaction'/'CMD', '113').
card_flavor_text('chain reaction'/'CMD', '\"We train for the improbable, like lightning striking the same place twice, or striking everywhere at once.\"\n—Nundari, Sea Gate militia captain').
card_multiverse_id('chain reaction'/'CMD', '247538').

card_in_set('champion\'s helm', 'CMD').
card_original_type('champion\'s helm'/'CMD', 'Artifact — Equipment').
card_original_text('champion\'s helm'/'CMD', 'Equipped creature gets +2/+2.\nAs long as equipped creature is legendary, it has hexproof. (It can\'t be the target of spells or abilities your opponents control.)\nEquip {1}').
card_first_print('champion\'s helm', 'CMD').
card_image_name('champion\'s helm'/'CMD', 'champion\'s helm').
card_uid('champion\'s helm'/'CMD', 'CMD:Champion\'s Helm:champion\'s helm').
card_rarity('champion\'s helm'/'CMD', 'Rare').
card_artist('champion\'s helm'/'CMD', 'Alan Pollack').
card_number('champion\'s helm'/'CMD', '244').
card_multiverse_id('champion\'s helm'/'CMD', '233200').

card_in_set('chaos warp', 'CMD').
card_original_type('chaos warp'/'CMD', 'Instant').
card_original_text('chaos warp'/'CMD', 'The owner of target permanent shuffles it into his or her library, then reveals the top card of his or her library. If it\'s a permanent card, he or she puts it onto the battlefield.').
card_first_print('chaos warp', 'CMD').
card_image_name('chaos warp'/'CMD', 'chaos warp').
card_uid('chaos warp'/'CMD', 'CMD:Chaos Warp:chaos warp').
card_rarity('chaos warp'/'CMD', 'Rare').
card_artist('chaos warp'/'CMD', 'Trevor Claxton').
card_number('chaos warp'/'CMD', '114').
card_multiverse_id('chaos warp'/'CMD', '236466').

card_in_set('chartooth cougar', 'CMD').
card_original_type('chartooth cougar'/'CMD', 'Creature — Cat Beast').
card_original_text('chartooth cougar'/'CMD', '{R}: Chartooth Cougar gets +1/+0 until end of turn.\nMountaincycling {2} ({2}, Discard this card: Search your library for a Mountain card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('chartooth cougar'/'CMD', 'chartooth cougar').
card_uid('chartooth cougar'/'CMD', 'CMD:Chartooth Cougar:chartooth cougar').
card_rarity('chartooth cougar'/'CMD', 'Common').
card_artist('chartooth cougar'/'CMD', 'Tony Szczudlo').
card_number('chartooth cougar'/'CMD', '115').
card_multiverse_id('chartooth cougar'/'CMD', '247402').

card_in_set('chorus of the conclave', 'CMD').
card_original_type('chorus of the conclave'/'CMD', 'Legendary Creature — Dryad').
card_original_text('chorus of the conclave'/'CMD', 'Forestwalk\nAs an additional cost to cast creature spells, you may pay any amount of mana. If you do, that creature enters the battlefield with that many additional +1/+1 counters on it.').
card_image_name('chorus of the conclave'/'CMD', 'chorus of the conclave').
card_uid('chorus of the conclave'/'CMD', 'CMD:Chorus of the Conclave:chorus of the conclave').
card_rarity('chorus of the conclave'/'CMD', 'Rare').
card_artist('chorus of the conclave'/'CMD', 'Brian Despain').
card_number('chorus of the conclave'/'CMD', '189').
card_flavor_text('chorus of the conclave'/'CMD', '\"We are many, yet one. We are separate in body, yet speak with a single voice. Join us in our chorus.\"').
card_multiverse_id('chorus of the conclave'/'CMD', '247374').

card_in_set('chromeshell crab', 'CMD').
card_original_type('chromeshell crab'/'CMD', 'Creature — Crab Beast').
card_original_text('chromeshell crab'/'CMD', 'Morph {4}{U} (You may cast this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Chromeshell Crab is turned face up, you may exchange control of target creature you control and target creature an opponent controls.').
card_image_name('chromeshell crab'/'CMD', 'chromeshell crab').
card_uid('chromeshell crab'/'CMD', 'CMD:Chromeshell Crab:chromeshell crab').
card_rarity('chromeshell crab'/'CMD', 'Rare').
card_artist('chromeshell crab'/'CMD', 'Ron Spencer').
card_number('chromeshell crab'/'CMD', '41').
card_multiverse_id('chromeshell crab'/'CMD', '247294').

card_in_set('cleansing beam', 'CMD').
card_original_type('cleansing beam'/'CMD', 'Instant').
card_original_text('cleansing beam'/'CMD', 'Radiance — Cleansing Beam deals 2 damage to target creature and each other creature that shares a color with it.').
card_image_name('cleansing beam'/'CMD', 'cleansing beam').
card_uid('cleansing beam'/'CMD', 'CMD:Cleansing Beam:cleansing beam').
card_rarity('cleansing beam'/'CMD', 'Uncommon').
card_artist('cleansing beam'/'CMD', 'Pat Lee').
card_number('cleansing beam'/'CMD', '116').
card_flavor_text('cleansing beam'/'CMD', '\"Justice is toothless without punishment. Righteousness cannot succeed without the suffering of the guilty.\"\n—Razia, Boros archangel').
card_multiverse_id('cleansing beam'/'CMD', '247375').

card_in_set('cobra trap', 'CMD').
card_original_type('cobra trap'/'CMD', 'Instant — Trap').
card_original_text('cobra trap'/'CMD', 'If a noncreature permanent under your control was destroyed this turn by a spell or ability an opponent controlled, you may pay {G} rather than pay Cobra Trap\'s mana cost.\nPut four 1/1 green Snake creature tokens onto the battlefield.').
card_image_name('cobra trap'/'CMD', 'cobra trap').
card_uid('cobra trap'/'CMD', 'CMD:Cobra Trap:cobra trap').
card_rarity('cobra trap'/'CMD', 'Uncommon').
card_artist('cobra trap'/'CMD', 'Scott Chou').
card_number('cobra trap'/'CMD', '146').
card_multiverse_id('cobra trap'/'CMD', '247545').

card_in_set('collective voyage', 'CMD').
card_original_type('collective voyage'/'CMD', 'Sorcery').
card_original_text('collective voyage'/'CMD', 'Join forces — Starting with you, each player may pay any amount of mana. Each player searches his or her library for up to X basic land cards, where X is the total amount of mana paid this way, puts them onto the battlefield tapped, then shuffles his or her library.').
card_first_print('collective voyage', 'CMD').
card_image_name('collective voyage'/'CMD', 'collective voyage').
card_uid('collective voyage'/'CMD', 'CMD:Collective Voyage:collective voyage').
card_rarity('collective voyage'/'CMD', 'Rare').
card_artist('collective voyage'/'CMD', 'Charles Urbach').
card_number('collective voyage'/'CMD', '147').
card_multiverse_id('collective voyage'/'CMD', '241854').

card_in_set('colossal might', 'CMD').
card_original_type('colossal might'/'CMD', 'Instant').
card_original_text('colossal might'/'CMD', 'Target creature gets +4/+2 and gains trample until end of turn.').
card_image_name('colossal might'/'CMD', 'colossal might').
card_uid('colossal might'/'CMD', 'CMD:Colossal Might:colossal might').
card_rarity('colossal might'/'CMD', 'Common').
card_artist('colossal might'/'CMD', 'Justin Sweet').
card_number('colossal might'/'CMD', '190').
card_flavor_text('colossal might'/'CMD', '\"Never corner a barbarian. You become the path of least resistance.\"\n—Sarkhan Vol').
card_multiverse_id('colossal might'/'CMD', '247164').

card_in_set('comet storm', 'CMD').
card_original_type('comet storm'/'CMD', 'Instant').
card_original_text('comet storm'/'CMD', 'Multikicker {1} (You may pay an additional {1} any number of times as you cast this spell.)\nChoose target creature or player, then choose another target creature or player for each time Comet Storm was kicked. Comet Storm deals X damage to each of them.').
card_image_name('comet storm'/'CMD', 'comet storm').
card_uid('comet storm'/'CMD', 'CMD:Comet Storm:comet storm').
card_rarity('comet storm'/'CMD', 'Mythic Rare').
card_artist('comet storm'/'CMD', 'Jung Park').
card_number('comet storm'/'CMD', '117').
card_multiverse_id('comet storm'/'CMD', '247539').

card_in_set('command tower', 'CMD').
card_original_type('command tower'/'CMD', 'Land').
card_original_text('command tower'/'CMD', '{T}: Add to your mana pool one mana of any color in your commander\'s color identity.').
card_image_name('command tower'/'CMD', 'command tower').
card_uid('command tower'/'CMD', 'CMD:Command Tower:command tower').
card_rarity('command tower'/'CMD', 'Common').
card_artist('command tower'/'CMD', 'Ryan Yee').
card_number('command tower'/'CMD', '269').
card_flavor_text('command tower'/'CMD', 'When defeat is near and guidance is scarce, all eyes look in one direction.').
card_multiverse_id('command tower'/'CMD', '237004').

card_in_set('congregate', 'CMD').
card_original_type('congregate'/'CMD', 'Instant').
card_original_text('congregate'/'CMD', 'Target player gains 2 life for each creature on the battlefield.').
card_image_name('congregate'/'CMD', 'congregate').
card_uid('congregate'/'CMD', 'CMD:Congregate:congregate').
card_rarity('congregate'/'CMD', 'Common').
card_artist('congregate'/'CMD', 'Mark Zug').
card_number('congregate'/'CMD', '11').
card_flavor_text('congregate'/'CMD', '\"In the gathering there is strength for all who founder, renewal for all who languish, love for all who sing.\"\n—Song of All, canto 642').
card_multiverse_id('congregate'/'CMD', '247526').

card_in_set('conundrum sphinx', 'CMD').
card_original_type('conundrum sphinx'/'CMD', 'Creature — Sphinx').
card_original_text('conundrum sphinx'/'CMD', 'Flying\nWhenever Conundrum Sphinx attacks, each player names a card. Then each player reveals the top card of his or her library. If the card a player revealed is the card he or she named, that player puts it into his or her hand. If it\'s not, that player puts it on the bottom of his or her library.').
card_image_name('conundrum sphinx'/'CMD', 'conundrum sphinx').
card_uid('conundrum sphinx'/'CMD', 'CMD:Conundrum Sphinx:conundrum sphinx').
card_rarity('conundrum sphinx'/'CMD', 'Rare').
card_artist('conundrum sphinx'/'CMD', 'Michael Komarck').
card_number('conundrum sphinx'/'CMD', '42').
card_multiverse_id('conundrum sphinx'/'CMD', '247319').

card_in_set('court hussar', 'CMD').
card_original_type('court hussar'/'CMD', 'Creature — Vedalken Knight').
card_original_text('court hussar'/'CMD', 'Vigilance\nWhen Court Hussar enters the battlefield, look at the top three cards of your library, then put one of them into your hand and the rest on the bottom of your library in any order.\nWhen Court Hussar enters the battlefield, sacrifice it unless {W} was spent to cast it.').
card_image_name('court hussar'/'CMD', 'court hussar').
card_uid('court hussar'/'CMD', 'CMD:Court Hussar:court hussar').
card_rarity('court hussar'/'CMD', 'Uncommon').
card_artist('court hussar'/'CMD', 'Ron Spears').
card_number('court hussar'/'CMD', '43').
card_multiverse_id('court hussar'/'CMD', '247191').

card_in_set('crescendo of war', 'CMD').
card_original_type('crescendo of war'/'CMD', 'Enchantment').
card_original_text('crescendo of war'/'CMD', 'At the beginning of each upkeep, put a strife counter on Crescendo of War.\nAttacking creatures get +1/+0 for each strife counter on Crescendo of War.\nBlocking creatures you control get +1/+0 for each strife counter on Crescendo of War.').
card_first_print('crescendo of war', 'CMD').
card_image_name('crescendo of war'/'CMD', 'crescendo of war').
card_uid('crescendo of war'/'CMD', 'CMD:Crescendo of War:crescendo of war').
card_rarity('crescendo of war'/'CMD', 'Rare').
card_artist('crescendo of war'/'CMD', 'Daarken').
card_number('crescendo of war'/'CMD', '12').
card_multiverse_id('crescendo of war'/'CMD', '228253').

card_in_set('cultivate', 'CMD').
card_original_type('cultivate'/'CMD', 'Sorcery').
card_original_text('cultivate'/'CMD', 'Search your library for up to two basic land cards, reveal those cards, and put one onto the battlefield tapped and the other into your hand. Then shuffle your library.').
card_image_name('cultivate'/'CMD', 'cultivate').
card_uid('cultivate'/'CMD', 'CMD:Cultivate:cultivate').
card_rarity('cultivate'/'CMD', 'Common').
card_artist('cultivate'/'CMD', 'Anthony Palumbo').
card_number('cultivate'/'CMD', '148').
card_flavor_text('cultivate'/'CMD', 'All seeds share a common bond, calling to each other across infinity.').
card_multiverse_id('cultivate'/'CMD', '247320').

card_in_set('damia, sage of stone', 'CMD').
card_original_type('damia, sage of stone'/'CMD', 'Legendary Creature — Gorgon Wizard').
card_original_text('damia, sage of stone'/'CMD', 'Deathtouch\nSkip your draw step.\nAt the beginning of your upkeep, if you have fewer than seven cards in hand, draw cards equal to the difference.').
card_first_print('damia, sage of stone', 'CMD').
card_image_name('damia, sage of stone'/'CMD', 'damia, sage of stone').
card_uid('damia, sage of stone'/'CMD', 'CMD:Damia, Sage of Stone:damia, sage of stone').
card_rarity('damia, sage of stone'/'CMD', 'Mythic Rare').
card_artist('damia, sage of stone'/'CMD', 'Steve Argyle').
card_number('damia, sage of stone'/'CMD', '191').
card_flavor_text('damia, sage of stone'/'CMD', 'Ask your query and be on your way. Just don\'t look her in the eye.').
card_multiverse_id('damia, sage of stone'/'CMD', '236479').

card_in_set('dark hatchling', 'CMD').
card_original_type('dark hatchling'/'CMD', 'Creature — Horror').
card_original_text('dark hatchling'/'CMD', 'Flying\nWhen Dark Hatchling enters the battlefield, destroy target nonblack creature. It can\'t be regenerated.').
card_image_name('dark hatchling'/'CMD', 'dark hatchling').
card_uid('dark hatchling'/'CMD', 'CMD:Dark Hatchling:dark hatchling').
card_rarity('dark hatchling'/'CMD', 'Rare').
card_artist('dark hatchling'/'CMD', 'Mark A. Nelson').
card_number('dark hatchling'/'CMD', '76').
card_multiverse_id('dark hatchling'/'CMD', '247527').

card_in_set('darksteel ingot', 'CMD').
card_original_type('darksteel ingot'/'CMD', 'Artifact').
card_original_text('darksteel ingot'/'CMD', 'Darksteel Ingot is indestructible. (\"Destroy\" effects and lethal damage don\'t destroy it.)\n{T}: Add one mana of any color to your mana pool.').
card_image_name('darksteel ingot'/'CMD', 'darksteel ingot').
card_uid('darksteel ingot'/'CMD', 'CMD:Darksteel Ingot:darksteel ingot').
card_rarity('darksteel ingot'/'CMD', 'Common').
card_artist('darksteel ingot'/'CMD', 'Martina Pilcerova').
card_number('darksteel ingot'/'CMD', '245').
card_multiverse_id('darksteel ingot'/'CMD', '247200').

card_in_set('deadly recluse', 'CMD').
card_original_type('deadly recluse'/'CMD', 'Creature — Spider').
card_original_text('deadly recluse'/'CMD', 'Reach (This creature can block creatures with flying.)\nDeathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_image_name('deadly recluse'/'CMD', 'deadly recluse').
card_uid('deadly recluse'/'CMD', 'CMD:Deadly Recluse:deadly recluse').
card_rarity('deadly recluse'/'CMD', 'Common').
card_artist('deadly recluse'/'CMD', 'Warren Mahy').
card_number('deadly recluse'/'CMD', '149').
card_multiverse_id('deadly recluse'/'CMD', '247313').

card_in_set('deadwood treefolk', 'CMD').
card_original_type('deadwood treefolk'/'CMD', 'Creature — Treefolk').
card_original_text('deadwood treefolk'/'CMD', 'Vanishing 3 (This permanent enters the battlefield with three time counters on it. At the beginning of your upkeep, remove a time counter from it. When the last is removed, sacrifice it.)\nWhen Deadwood Treefolk enters the battlefield or leaves the battlefield, return another target creature card from your graveyard to your hand.').
card_image_name('deadwood treefolk'/'CMD', 'deadwood treefolk').
card_uid('deadwood treefolk'/'CMD', 'CMD:Deadwood Treefolk:deadwood treefolk').
card_rarity('deadwood treefolk'/'CMD', 'Uncommon').
card_artist('deadwood treefolk'/'CMD', 'Don Hazeltine').
card_number('deadwood treefolk'/'CMD', '150').
card_multiverse_id('deadwood treefolk'/'CMD', '247359').

card_in_set('death by dragons', 'CMD').
card_original_type('death by dragons'/'CMD', 'Sorcery').
card_original_text('death by dragons'/'CMD', 'Each player other than target player puts a 5/5 red Dragon creature token with flying onto the battlefield.').
card_first_print('death by dragons', 'CMD').
card_image_name('death by dragons'/'CMD', 'death by dragons').
card_uid('death by dragons'/'CMD', 'CMD:Death by Dragons:death by dragons').
card_rarity('death by dragons'/'CMD', 'Uncommon').
card_artist('death by dragons'/'CMD', 'Austin Hsu').
card_number('death by dragons'/'CMD', '118').
card_flavor_text('death by dragons'/'CMD', 'Suddenly, Eldo felt very alone.').
card_multiverse_id('death by dragons'/'CMD', '229988').

card_in_set('death mutation', 'CMD').
card_original_type('death mutation'/'CMD', 'Sorcery').
card_original_text('death mutation'/'CMD', 'Destroy target nonblack creature. It can\'t be regenerated. Put X 1/1 green Saproling creature tokens onto the battlefield, where X is that creature\'s converted mana cost.').
card_image_name('death mutation'/'CMD', 'death mutation').
card_uid('death mutation'/'CMD', 'CMD:Death Mutation:death mutation').
card_rarity('death mutation'/'CMD', 'Uncommon').
card_artist('death mutation'/'CMD', 'Carl Critchlow').
card_number('death mutation'/'CMD', '192').
card_multiverse_id('death mutation'/'CMD', '247158').

card_in_set('desecrator hag', 'CMD').
card_original_type('desecrator hag'/'CMD', 'Creature — Hag').
card_original_text('desecrator hag'/'CMD', 'When Desecrator Hag enters the battlefield, return to your hand the creature card in your graveyard with the greatest power. If two or more cards are tied for greatest power, you choose one of them.').
card_image_name('desecrator hag'/'CMD', 'desecrator hag').
card_uid('desecrator hag'/'CMD', 'CMD:Desecrator Hag:desecrator hag').
card_rarity('desecrator hag'/'CMD', 'Common').
card_artist('desecrator hag'/'CMD', 'Fred Harper').
card_number('desecrator hag'/'CMD', '193').
card_flavor_text('desecrator hag'/'CMD', 'Loose earth is not a burial but an appetizer.').
card_multiverse_id('desecrator hag'/'CMD', '247203').

card_in_set('diabolic tutor', 'CMD').
card_original_type('diabolic tutor'/'CMD', 'Sorcery').
card_original_text('diabolic tutor'/'CMD', 'Search your library for a card and put that card into your hand. Then shuffle your library.').
card_image_name('diabolic tutor'/'CMD', 'diabolic tutor').
card_uid('diabolic tutor'/'CMD', 'CMD:Diabolic Tutor:diabolic tutor').
card_rarity('diabolic tutor'/'CMD', 'Uncommon').
card_artist('diabolic tutor'/'CMD', 'Greg Staples').
card_number('diabolic tutor'/'CMD', '77').
card_flavor_text('diabolic tutor'/'CMD', 'The wise always keep an ear open to the whispers of power.').
card_multiverse_id('diabolic tutor'/'CMD', '247321').

card_in_set('dimir aqueduct', 'CMD').
card_original_type('dimir aqueduct'/'CMD', 'Land').
card_original_text('dimir aqueduct'/'CMD', 'Dimir Aqueduct enters the battlefield tapped.\nWhen Dimir Aqueduct enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {U}{B} to your mana pool.').
card_image_name('dimir aqueduct'/'CMD', 'dimir aqueduct').
card_uid('dimir aqueduct'/'CMD', 'CMD:Dimir Aqueduct:dimir aqueduct').
card_rarity('dimir aqueduct'/'CMD', 'Common').
card_artist('dimir aqueduct'/'CMD', 'John Avon').
card_number('dimir aqueduct'/'CMD', '270').
card_multiverse_id('dimir aqueduct'/'CMD', '247376').

card_in_set('dimir signet', 'CMD').
card_original_type('dimir signet'/'CMD', 'Artifact').
card_original_text('dimir signet'/'CMD', '{1}, {T}: Add {U}{B} to your mana pool.').
card_image_name('dimir signet'/'CMD', 'dimir signet').
card_uid('dimir signet'/'CMD', 'CMD:Dimir Signet:dimir signet').
card_rarity('dimir signet'/'CMD', 'Common').
card_artist('dimir signet'/'CMD', 'Greg Hildebrandt').
card_number('dimir signet'/'CMD', '246').
card_flavor_text('dimir signet'/'CMD', 'An emblem of a secret guild, the Dimir insignia is seen only by its own members—and the doomed.').
card_multiverse_id('dimir signet'/'CMD', '247377').

card_in_set('disaster radius', 'CMD').
card_original_type('disaster radius'/'CMD', 'Sorcery').
card_original_text('disaster radius'/'CMD', 'As an additional cost to cast Disaster Radius, reveal a creature card from your hand.\nDisaster Radius deals X damage to each creature your opponents control, where X is the revealed card\'s converted mana cost.').
card_image_name('disaster radius'/'CMD', 'disaster radius').
card_uid('disaster radius'/'CMD', 'CMD:Disaster Radius:disaster radius').
card_rarity('disaster radius'/'CMD', 'Rare').
card_artist('disaster radius'/'CMD', 'James Paick').
card_number('disaster radius'/'CMD', '119').
card_multiverse_id('disaster radius'/'CMD', '247394').

card_in_set('dominus of fealty', 'CMD').
card_original_type('dominus of fealty'/'CMD', 'Creature — Spirit Avatar').
card_original_text('dominus of fealty'/'CMD', 'Flying\nAt the beginning of your upkeep, you may gain control of target permanent until end of turn. If you do, untap it and it gains haste until end of turn.').
card_image_name('dominus of fealty'/'CMD', 'dominus of fealty').
card_uid('dominus of fealty'/'CMD', 'CMD:Dominus of Fealty:dominus of fealty').
card_rarity('dominus of fealty'/'CMD', 'Rare').
card_artist('dominus of fealty'/'CMD', 'Kev Walker').
card_number('dominus of fealty'/'CMD', '194').
card_flavor_text('dominus of fealty'/'CMD', '\"Nothing is truly your own. It is his, whether you know it or not.\"\n—The Seer\'s Parables').
card_multiverse_id('dominus of fealty'/'CMD', '247204').

card_in_set('doom blade', 'CMD').
card_original_type('doom blade'/'CMD', 'Instant').
card_original_text('doom blade'/'CMD', 'Destroy target nonblack creature.').
card_image_name('doom blade'/'CMD', 'doom blade').
card_uid('doom blade'/'CMD', 'CMD:Doom Blade:doom blade').
card_rarity('doom blade'/'CMD', 'Common').
card_artist('doom blade'/'CMD', 'Chippy').
card_number('doom blade'/'CMD', '78').
card_flavor_text('doom blade'/'CMD', 'The void is without substance but cuts like steel.').
card_multiverse_id('doom blade'/'CMD', '247322').

card_in_set('dragon whelp', 'CMD').
card_original_type('dragon whelp'/'CMD', 'Creature — Dragon').
card_original_text('dragon whelp'/'CMD', 'Flying\n{R}: Dragon Whelp gets +1/+0 until end of turn. If this ability has been activated four or more times this turn, sacrifice Dragon Whelp at the beginning of the next end step.').
card_image_name('dragon whelp'/'CMD', 'dragon whelp').
card_uid('dragon whelp'/'CMD', 'CMD:Dragon Whelp:dragon whelp').
card_rarity('dragon whelp'/'CMD', 'Uncommon').
card_artist('dragon whelp'/'CMD', 'Steven Belledin').
card_number('dragon whelp'/'CMD', '120').
card_multiverse_id('dragon whelp'/'CMD', '247314').

card_in_set('dread cacodemon', 'CMD').
card_original_type('dread cacodemon'/'CMD', 'Creature — Demon').
card_original_text('dread cacodemon'/'CMD', 'When Dread Cacodemon enters the battlefield, if you cast it from your hand, destroy all creatures your opponents control, then tap all other creatures you control.').
card_first_print('dread cacodemon', 'CMD').
card_image_name('dread cacodemon'/'CMD', 'dread cacodemon').
card_uid('dread cacodemon'/'CMD', 'CMD:Dread Cacodemon:dread cacodemon').
card_rarity('dread cacodemon'/'CMD', 'Rare').
card_artist('dread cacodemon'/'CMD', 'Izzy').
card_number('dread cacodemon'/'CMD', '79').
card_flavor_text('dread cacodemon'/'CMD', 'Those who hear its roar perish. The lucky ones only feel its fetid breath.').
card_multiverse_id('dread cacodemon'/'CMD', '228228').

card_in_set('dreadship reef', 'CMD').
card_original_type('dreadship reef'/'CMD', 'Land').
card_original_text('dreadship reef'/'CMD', '{T}: Add {1} to your mana pool.\n{1}, {T}: Put a storage counter on Dreadship Reef.\n{1}, Remove X storage counters from Dreadship Reef: Add X mana in any combination of {U} and/or {B} to your mana pool.').
card_image_name('dreadship reef'/'CMD', 'dreadship reef').
card_uid('dreadship reef'/'CMD', 'CMD:Dreadship Reef:dreadship reef').
card_rarity('dreadship reef'/'CMD', 'Uncommon').
card_artist('dreadship reef'/'CMD', 'Lars Grant-West').
card_number('dreadship reef'/'CMD', '271').
card_multiverse_id('dreadship reef'/'CMD', '247513').

card_in_set('dreamborn muse', 'CMD').
card_original_type('dreamborn muse'/'CMD', 'Creature — Spirit').
card_original_text('dreamborn muse'/'CMD', 'At the beginning of each player\'s upkeep, that player puts the top X cards of his or her library into his or her graveyard, where X is the number of cards in his or her hand.').
card_image_name('dreamborn muse'/'CMD', 'dreamborn muse').
card_uid('dreamborn muse'/'CMD', 'CMD:Dreamborn Muse:dreamborn muse').
card_rarity('dreamborn muse'/'CMD', 'Rare').
card_artist('dreamborn muse'/'CMD', 'Kev Walker').
card_number('dreamborn muse'/'CMD', '44').
card_flavor_text('dreamborn muse'/'CMD', '\"Her voice is insight, piercing and true.\"\n—Ixidor, reality sculptor').
card_multiverse_id('dreamborn muse'/'CMD', '247140').

card_in_set('dreamstone hedron', 'CMD').
card_original_type('dreamstone hedron'/'CMD', 'Artifact').
card_original_text('dreamstone hedron'/'CMD', '{T}: Add {3} to your mana pool.\n{3}, {T}, Sacrifice Dreamstone Hedron: Draw three cards.').
card_image_name('dreamstone hedron'/'CMD', 'dreamstone hedron').
card_uid('dreamstone hedron'/'CMD', 'CMD:Dreamstone Hedron:dreamstone hedron').
card_rarity('dreamstone hedron'/'CMD', 'Uncommon').
card_artist('dreamstone hedron'/'CMD', 'Eric Deschamps').
card_number('dreamstone hedron'/'CMD', '247').
card_flavor_text('dreamstone hedron'/'CMD', 'Only the Eldrazi mind thinks in the warped paths required to open the hedrons and tap the power within.').
card_multiverse_id('dreamstone hedron'/'CMD', '247395').

card_in_set('duergar hedge-mage', 'CMD').
card_original_type('duergar hedge-mage'/'CMD', 'Creature — Dwarf Shaman').
card_original_text('duergar hedge-mage'/'CMD', 'When Duergar Hedge-Mage enters the battlefield, if you control two or more Mountains, you may destroy target artifact.\nWhen Duergar Hedge-Mage enters the battlefield, if you control two or more Plains, you may destroy target enchantment.').
card_image_name('duergar hedge-mage'/'CMD', 'duergar hedge-mage').
card_uid('duergar hedge-mage'/'CMD', 'CMD:Duergar Hedge-Mage:duergar hedge-mage').
card_rarity('duergar hedge-mage'/'CMD', 'Uncommon').
card_artist('duergar hedge-mage'/'CMD', 'Dave Allsop').
card_number('duergar hedge-mage'/'CMD', '195').
card_multiverse_id('duergar hedge-mage'/'CMD', '247205').

card_in_set('earthquake', 'CMD').
card_original_type('earthquake'/'CMD', 'Sorcery').
card_original_text('earthquake'/'CMD', 'Earthquake deals X damage to each creature without flying and each player.').
card_image_name('earthquake'/'CMD', 'earthquake').
card_uid('earthquake'/'CMD', 'CMD:Earthquake:earthquake').
card_rarity('earthquake'/'CMD', 'Rare').
card_artist('earthquake'/'CMD', 'Adrian Smith').
card_number('earthquake'/'CMD', '121').
card_flavor_text('earthquake'/'CMD', 'They fell screaming into the depths of the chasm like so many pebbles tossed down a well.').
card_multiverse_id('earthquake'/'CMD', '247315').

card_in_set('edric, spymaster of trest', 'CMD').
card_original_type('edric, spymaster of trest'/'CMD', 'Legendary Creature — Elf Rogue').
card_original_text('edric, spymaster of trest'/'CMD', 'Whenever a creature deals combat damage to one of your opponents, its controller may draw a card.').
card_first_print('edric, spymaster of trest', 'CMD').
card_image_name('edric, spymaster of trest'/'CMD', 'edric, spymaster of trest').
card_uid('edric, spymaster of trest'/'CMD', 'CMD:Edric, Spymaster of Trest:edric, spymaster of trest').
card_rarity('edric, spymaster of trest'/'CMD', 'Rare').
card_artist('edric, spymaster of trest'/'CMD', 'Volkan Baga').
card_number('edric, spymaster of trest'/'CMD', '196').
card_flavor_text('edric, spymaster of trest'/'CMD', '\"I am not at liberty to reveal my sources, but I can assure you, the price on your head is high.\"').
card_multiverse_id('edric, spymaster of trest'/'CMD', '230000').

card_in_set('electrolyze', 'CMD').
card_original_type('electrolyze'/'CMD', 'Instant').
card_original_text('electrolyze'/'CMD', 'Electrolyze deals 2 damage divided as you choose among one or two target creatures and/or players.\nDraw a card.').
card_image_name('electrolyze'/'CMD', 'electrolyze').
card_uid('electrolyze'/'CMD', 'CMD:Electrolyze:electrolyze').
card_rarity('electrolyze'/'CMD', 'Uncommon').
card_artist('electrolyze'/'CMD', 'Zoltan Boros & Gabor Szikszai').
card_number('electrolyze'/'CMD', '197').
card_flavor_text('electrolyze'/'CMD', 'The Izzet learn something from every lesson they teach.').
card_multiverse_id('electrolyze'/'CMD', '247276').

card_in_set('elvish aberration', 'CMD').
card_original_type('elvish aberration'/'CMD', 'Creature — Elf Mutant').
card_original_text('elvish aberration'/'CMD', '{T}: Add {G}{G}{G} to your mana pool.\nForestcycling {2} ({2}, Discard this card: Search your library for a Forest card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('elvish aberration'/'CMD', 'elvish aberration').
card_uid('elvish aberration'/'CMD', 'CMD:Elvish Aberration:elvish aberration').
card_rarity('elvish aberration'/'CMD', 'Uncommon').
card_artist('elvish aberration'/'CMD', 'Matt Cavotta').
card_number('elvish aberration'/'CMD', '151').
card_multiverse_id('elvish aberration'/'CMD', '247403').

card_in_set('eternal witness', 'CMD').
card_original_type('eternal witness'/'CMD', 'Creature — Human Shaman').
card_original_text('eternal witness'/'CMD', 'When Eternal Witness enters the battlefield, you may return target card from your graveyard to your hand.').
card_image_name('eternal witness'/'CMD', 'eternal witness').
card_uid('eternal witness'/'CMD', 'CMD:Eternal Witness:eternal witness').
card_rarity('eternal witness'/'CMD', 'Uncommon').
card_artist('eternal witness'/'CMD', 'Terese Nielsen').
card_number('eternal witness'/'CMD', '152').
card_flavor_text('eternal witness'/'CMD', 'She remembers every word spoken, from the hero\'s oath to the baby\'s cry.').
card_multiverse_id('eternal witness'/'CMD', '247148').

card_in_set('evincar\'s justice', 'CMD').
card_original_type('evincar\'s justice'/'CMD', 'Sorcery').
card_original_text('evincar\'s justice'/'CMD', 'Buyback {3} (You may pay an additional {3} as you cast this spell. If you do, put this card into your hand as it resolves.)\nEvincar\'s Justice deals 2 damage to each creature and each player.').
card_image_name('evincar\'s justice'/'CMD', 'evincar\'s justice').
card_uid('evincar\'s justice'/'CMD', 'CMD:Evincar\'s Justice:evincar\'s justice').
card_rarity('evincar\'s justice'/'CMD', 'Common').
card_artist('evincar\'s justice'/'CMD', 'Hannibal King').
card_number('evincar\'s justice'/'CMD', '80').
card_multiverse_id('evincar\'s justice'/'CMD', '247415').

card_in_set('evolving wilds', 'CMD').
card_original_type('evolving wilds'/'CMD', 'Land').
card_original_text('evolving wilds'/'CMD', '{T}, Sacrifice Evolving Wilds: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('evolving wilds'/'CMD', 'evolving wilds').
card_uid('evolving wilds'/'CMD', 'CMD:Evolving Wilds:evolving wilds').
card_rarity('evolving wilds'/'CMD', 'Common').
card_artist('evolving wilds'/'CMD', 'Steven Belledin').
card_number('evolving wilds'/'CMD', '272').
card_flavor_text('evolving wilds'/'CMD', 'Every world is an organism, able to grow new lands. Some just do it faster than others.').
card_multiverse_id('evolving wilds'/'CMD', '247396').

card_in_set('explosive vegetation', 'CMD').
card_original_type('explosive vegetation'/'CMD', 'Sorcery').
card_original_text('explosive vegetation'/'CMD', 'Search your library for up to two basic land cards and put them onto the battlefield tapped. Then shuffle your library.').
card_image_name('explosive vegetation'/'CMD', 'explosive vegetation').
card_uid('explosive vegetation'/'CMD', 'CMD:Explosive Vegetation:explosive vegetation').
card_rarity('explosive vegetation'/'CMD', 'Uncommon').
card_artist('explosive vegetation'/'CMD', 'John Avon').
card_number('explosive vegetation'/'CMD', '153').
card_flavor_text('explosive vegetation'/'CMD', 'Torching Krosa would be pointless. It grows faster than it burns.').
card_multiverse_id('explosive vegetation'/'CMD', '247345').

card_in_set('extractor demon', 'CMD').
card_original_type('extractor demon'/'CMD', 'Creature — Demon').
card_original_text('extractor demon'/'CMD', 'Flying\nWhenever another creature leaves the battlefield, you may have target player put the top two cards of his or her library into his or her graveyard.\nUnearth {2}{B} ({2}{B}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_image_name('extractor demon'/'CMD', 'extractor demon').
card_uid('extractor demon'/'CMD', 'CMD:Extractor Demon:extractor demon').
card_rarity('extractor demon'/'CMD', 'Rare').
card_artist('extractor demon'/'CMD', 'Carl Critchlow').
card_number('extractor demon'/'CMD', '81').
card_multiverse_id('extractor demon'/'CMD', '247178').

card_in_set('fact or fiction', 'CMD').
card_original_type('fact or fiction'/'CMD', 'Instant').
card_original_text('fact or fiction'/'CMD', 'Reveal the top five cards of your library. An opponent separates those cards into two piles. Put one pile into your hand and the other into your graveyard.').
card_image_name('fact or fiction'/'CMD', 'fact or fiction').
card_uid('fact or fiction'/'CMD', 'CMD:Fact or Fiction:fact or fiction').
card_rarity('fact or fiction'/'CMD', 'Uncommon').
card_artist('fact or fiction'/'CMD', 'Matt Cavotta').
card_number('fact or fiction'/'CMD', '45').
card_flavor_text('fact or fiction'/'CMD', '\"Try to pretend like you understand what\'s important.\"').
card_multiverse_id('fact or fiction'/'CMD', '247186').

card_in_set('fallen angel', 'CMD').
card_original_type('fallen angel'/'CMD', 'Creature — Angel').
card_original_text('fallen angel'/'CMD', 'Flying\nSacrifice a creature: Fallen Angel gets +2/+1 until end of turn.').
card_image_name('fallen angel'/'CMD', 'fallen angel').
card_uid('fallen angel'/'CMD', 'CMD:Fallen Angel:fallen angel').
card_rarity('fallen angel'/'CMD', 'Rare').
card_artist('fallen angel'/'CMD', 'Matthew D. Wilson').
card_number('fallen angel'/'CMD', '82').
card_flavor_text('fallen angel'/'CMD', '\"Why do you weep for the dead? I rejoice, for they have died for me.\"').
card_multiverse_id('fallen angel'/'CMD', '247150').

card_in_set('false prophet', 'CMD').
card_original_type('false prophet'/'CMD', 'Creature — Human Cleric').
card_original_text('false prophet'/'CMD', 'When False Prophet is put into a graveyard from the battlefield, exile all creatures.').
card_image_name('false prophet'/'CMD', 'false prophet').
card_uid('false prophet'/'CMD', 'CMD:False Prophet:false prophet').
card_rarity('false prophet'/'CMD', 'Rare').
card_artist('false prophet'/'CMD', 'Eric Peterson').
card_number('false prophet'/'CMD', '13').
card_flavor_text('false prophet'/'CMD', '\"You lived for Serra\'s love. Will you not die for it?\"').
card_multiverse_id('false prophet'/'CMD', '247523').

card_in_set('faultgrinder', 'CMD').
card_original_type('faultgrinder'/'CMD', 'Creature — Elemental').
card_original_text('faultgrinder'/'CMD', 'Trample\nWhen Faultgrinder enters the battlefield, destroy target land.\nEvoke {4}{R} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_image_name('faultgrinder'/'CMD', 'faultgrinder').
card_uid('faultgrinder'/'CMD', 'CMD:Faultgrinder:faultgrinder').
card_rarity('faultgrinder'/'CMD', 'Common').
card_artist('faultgrinder'/'CMD', 'Anthony S. Waters').
card_number('faultgrinder'/'CMD', '122').
card_multiverse_id('faultgrinder'/'CMD', '247299').

card_in_set('fellwar stone', 'CMD').
card_original_type('fellwar stone'/'CMD', 'Artifact').
card_original_text('fellwar stone'/'CMD', '{T}: Add to your mana pool one mana of any color that a land an opponent controls could produce.').
card_image_name('fellwar stone'/'CMD', 'fellwar stone').
card_uid('fellwar stone'/'CMD', 'CMD:Fellwar Stone:fellwar stone').
card_rarity('fellwar stone'/'CMD', 'Uncommon').
card_artist('fellwar stone'/'CMD', 'John Avon').
card_number('fellwar stone'/'CMD', '248').
card_flavor_text('fellwar stone'/'CMD', '\"What do you have that I cannot obtain?\"\n—Mairsil, the Pretender').
card_multiverse_id('fellwar stone'/'CMD', '247152').

card_in_set('fertilid', 'CMD').
card_original_type('fertilid'/'CMD', 'Creature — Elemental').
card_original_text('fertilid'/'CMD', 'Fertilid enters the battlefield with two +1/+1 counters on it.\n{1}{G}, Remove a +1/+1 counter from Fertilid: Target player searches his or her library for a basic land card and puts it onto the battlefield tapped. Then that player shuffles his or her library.').
card_image_name('fertilid'/'CMD', 'fertilid').
card_uid('fertilid'/'CMD', 'CMD:Fertilid:fertilid').
card_rarity('fertilid'/'CMD', 'Common').
card_artist('fertilid'/'CMD', 'Wayne Reynolds').
card_number('fertilid'/'CMD', '154').
card_multiverse_id('fertilid'/'CMD', '247335').

card_in_set('fierce empath', 'CMD').
card_original_type('fierce empath'/'CMD', 'Creature — Elf').
card_original_text('fierce empath'/'CMD', 'When Fierce Empath enters the battlefield, you may search your library for a creature card with converted mana cost 6 or greater, reveal it, put it into your hand, then shuffle your library.').
card_image_name('fierce empath'/'CMD', 'fierce empath').
card_uid('fierce empath'/'CMD', 'CMD:Fierce Empath:fierce empath').
card_rarity('fierce empath'/'CMD', 'Common').
card_artist('fierce empath'/'CMD', 'Alan Pollack').
card_number('fierce empath'/'CMD', '155').
card_multiverse_id('fierce empath'/'CMD', '247404').

card_in_set('fire', 'CMD').
card_original_type('fire'/'CMD', 'Instant').
card_original_text('fire'/'CMD', 'Fire deals 2 damage divided as you choose among one or two target creatures and/or players.\n//\nIce\n{1}{U}\nTap target permanent.\nDraw a card.\nFranz Vohwinkel').
card_image_name('fire'/'CMD', 'fireice').
card_uid('fire'/'CMD', 'CMD:Fire:fireice').
card_rarity('fire'/'CMD', 'Uncommon').
card_artist('fire'/'CMD', 'David Martin').
card_number('fire'/'CMD', '198a').
card_multiverse_id('fire'/'CMD', '247159').

card_in_set('firespout', 'CMD').
card_original_type('firespout'/'CMD', 'Sorcery').
card_original_text('firespout'/'CMD', 'Firespout deals 3 damage to each creature without flying if {R} was spent to cast Firespout and 3 damage to each creature with flying if {G} was spent to cast it. (Do both if {R}{G} was spent.)').
card_image_name('firespout'/'CMD', 'firespout').
card_uid('firespout'/'CMD', 'CMD:Firespout:firespout').
card_rarity('firespout'/'CMD', 'Uncommon').
card_artist('firespout'/'CMD', 'Jeff Miracola').
card_number('firespout'/'CMD', '199').
card_multiverse_id('firespout'/'CMD', '247407').

card_in_set('fists of ironwood', 'CMD').
card_original_type('fists of ironwood'/'CMD', 'Enchantment — Aura').
card_original_text('fists of ironwood'/'CMD', 'Enchant creature\nWhen Fists of Ironwood enters the battlefield, put two 1/1 green Saproling creature tokens onto the battlefield.\nEnchanted creature has trample.').
card_image_name('fists of ironwood'/'CMD', 'fists of ironwood').
card_uid('fists of ironwood'/'CMD', 'CMD:Fists of Ironwood:fists of ironwood').
card_rarity('fists of ironwood'/'CMD', 'Common').
card_artist('fists of ironwood'/'CMD', 'Glen Angus').
card_number('fists of ironwood'/'CMD', '156').
card_flavor_text('fists of ironwood'/'CMD', 'Saprolings add the three and the four to the \"one-two punch.\"').
card_multiverse_id('fists of ironwood'/'CMD', '247378').

card_in_set('flametongue kavu', 'CMD').
card_original_type('flametongue kavu'/'CMD', 'Creature — Kavu').
card_original_text('flametongue kavu'/'CMD', 'When Flametongue Kavu enters the battlefield, it deals 4 damage to target creature.').
card_image_name('flametongue kavu'/'CMD', 'flametongue kavu').
card_uid('flametongue kavu'/'CMD', 'CMD:Flametongue Kavu:flametongue kavu').
card_rarity('flametongue kavu'/'CMD', 'Uncommon').
card_artist('flametongue kavu'/'CMD', 'Slawomir Maniak').
card_number('flametongue kavu'/'CMD', '123').
card_flavor_text('flametongue kavu'/'CMD', '\"For dim-witted, thick-skulled genetic mutants, they have pretty good aim.\"\n—Sisay, captain of the Weatherlight').
card_multiverse_id('flametongue kavu'/'CMD', '247368').

card_in_set('fleshbag marauder', 'CMD').
card_original_type('fleshbag marauder'/'CMD', 'Creature — Zombie Warrior').
card_original_text('fleshbag marauder'/'CMD', 'When Fleshbag Marauder enters the battlefield, each player sacrifices a creature.').
card_image_name('fleshbag marauder'/'CMD', 'fleshbag marauder').
card_uid('fleshbag marauder'/'CMD', 'CMD:Fleshbag Marauder:fleshbag marauder').
card_rarity('fleshbag marauder'/'CMD', 'Uncommon').
card_artist('fleshbag marauder'/'CMD', 'Pete Venters').
card_number('fleshbag marauder'/'CMD', '83').
card_flavor_text('fleshbag marauder'/'CMD', 'Grixis is a world where the only things found in abundance are death and decay. Corpses, whole or in part, are the standard currency among necromancers and demons.').
card_multiverse_id('fleshbag marauder'/'CMD', '247153').

card_in_set('flusterstorm', 'CMD').
card_original_type('flusterstorm'/'CMD', 'Instant').
card_original_text('flusterstorm'/'CMD', 'Counter target instant or sorcery spell unless its controller pays {1}.\nStorm (When you cast this spell, copy it for each spell cast before it this turn. You may choose new targets for the copies.)').
card_image_name('flusterstorm'/'CMD', 'flusterstorm').
card_uid('flusterstorm'/'CMD', 'CMD:Flusterstorm:flusterstorm').
card_rarity('flusterstorm'/'CMD', 'Rare').
card_artist('flusterstorm'/'CMD', 'Erica Yang').
card_number('flusterstorm'/'CMD', '46').
card_multiverse_id('flusterstorm'/'CMD', '228255').

card_in_set('fog bank', 'CMD').
card_original_type('fog bank'/'CMD', 'Creature — Wall').
card_original_text('fog bank'/'CMD', 'Defender, flying\nPrevent all combat damage that would be dealt to and dealt by Fog Bank.').
card_image_name('fog bank'/'CMD', 'fog bank').
card_uid('fog bank'/'CMD', 'CMD:Fog Bank:fog bank').
card_rarity('fog bank'/'CMD', 'Uncommon').
card_artist('fog bank'/'CMD', 'Scott Kirschner').
card_number('fog bank'/'CMD', '47').
card_multiverse_id('fog bank'/'CMD', '247528').

card_in_set('footbottom feast', 'CMD').
card_original_type('footbottom feast'/'CMD', 'Instant').
card_original_text('footbottom feast'/'CMD', 'Put any number of target creature cards from your graveyard on top of your library.\nDraw a card.').
card_image_name('footbottom feast'/'CMD', 'footbottom feast').
card_uid('footbottom feast'/'CMD', 'CMD:Footbottom Feast:footbottom feast').
card_rarity('footbottom feast'/'CMD', 'Common').
card_artist('footbottom feast'/'CMD', 'Jim Nelson').
card_number('footbottom feast'/'CMD', '84').
card_flavor_text('footbottom feast'/'CMD', 'The scent of rot and vinegar fills the marsh, inviting boggarts from every warren to reunite and feast.').
card_multiverse_id('footbottom feast'/'CMD', '247300').

card_in_set('forest', 'CMD').
card_original_type('forest'/'CMD', 'Basic Land — Forest').
card_original_text('forest'/'CMD', 'G').
card_image_name('forest'/'CMD', 'forest1').
card_uid('forest'/'CMD', 'CMD:Forest:forest1').
card_rarity('forest'/'CMD', 'Basic Land').
card_artist('forest'/'CMD', 'Rob Alexander').
card_number('forest'/'CMD', '315').
card_multiverse_id('forest'/'CMD', '249803').

card_in_set('forest', 'CMD').
card_original_type('forest'/'CMD', 'Basic Land — Forest').
card_original_text('forest'/'CMD', 'G').
card_image_name('forest'/'CMD', 'forest2').
card_uid('forest'/'CMD', 'CMD:Forest:forest2').
card_rarity('forest'/'CMD', 'Basic Land').
card_artist('forest'/'CMD', 'Steven Belledin').
card_number('forest'/'CMD', '316').
card_multiverse_id('forest'/'CMD', '249801').

card_in_set('forest', 'CMD').
card_original_type('forest'/'CMD', 'Basic Land — Forest').
card_original_text('forest'/'CMD', 'G').
card_image_name('forest'/'CMD', 'forest3').
card_uid('forest'/'CMD', 'CMD:Forest:forest3').
card_rarity('forest'/'CMD', 'Basic Land').
card_artist('forest'/'CMD', 'Zoltan Boros & Gabor Szikszai').
card_number('forest'/'CMD', '317').
card_multiverse_id('forest'/'CMD', '249800').

card_in_set('forest', 'CMD').
card_original_type('forest'/'CMD', 'Basic Land — Forest').
card_original_text('forest'/'CMD', 'G').
card_image_name('forest'/'CMD', 'forest4').
card_uid('forest'/'CMD', 'CMD:Forest:forest4').
card_rarity('forest'/'CMD', 'Basic Land').
card_artist('forest'/'CMD', 'Anthony S. Waters').
card_number('forest'/'CMD', '318').
card_multiverse_id('forest'/'CMD', '249802').

card_in_set('forgotten cave', 'CMD').
card_original_type('forgotten cave'/'CMD', 'Land').
card_original_text('forgotten cave'/'CMD', 'Forgotten Cave enters the battlefield tapped.\n{T}: Add {R} to your mana pool.\nCycling {R} ({R}, Discard this card: Draw a card.)').
card_image_name('forgotten cave'/'CMD', 'forgotten cave').
card_uid('forgotten cave'/'CMD', 'CMD:Forgotten Cave:forgotten cave').
card_rarity('forgotten cave'/'CMD', 'Common').
card_artist('forgotten cave'/'CMD', 'Tony Szczudlo').
card_number('forgotten cave'/'CMD', '273').
card_multiverse_id('forgotten cave'/'CMD', '247346').

card_in_set('fungal reaches', 'CMD').
card_original_type('fungal reaches'/'CMD', 'Land').
card_original_text('fungal reaches'/'CMD', '{T}: Add {1} to your mana pool.\n{1}, {T}: Put a storage counter on Fungal Reaches.\n{1}, Remove X storage counters from Fungal Reaches: Add X mana in any combination of {R} and/or {G} to your mana pool.').
card_image_name('fungal reaches'/'CMD', 'fungal reaches').
card_uid('fungal reaches'/'CMD', 'CMD:Fungal Reaches:fungal reaches').
card_rarity('fungal reaches'/'CMD', 'Uncommon').
card_artist('fungal reaches'/'CMD', 'Martina Pilcerova').
card_number('fungal reaches'/'CMD', '274').
card_multiverse_id('fungal reaches'/'CMD', '247514').

card_in_set('furnace whelp', 'CMD').
card_original_type('furnace whelp'/'CMD', 'Creature — Dragon').
card_original_text('furnace whelp'/'CMD', 'Flying\n{R}: Furnace Whelp gets +1/+0 until end of turn.').
card_image_name('furnace whelp'/'CMD', 'furnace whelp').
card_uid('furnace whelp'/'CMD', 'CMD:Furnace Whelp:furnace whelp').
card_rarity('furnace whelp'/'CMD', 'Uncommon').
card_artist('furnace whelp'/'CMD', 'Matt Cavotta').
card_number('furnace whelp'/'CMD', '124').
card_flavor_text('furnace whelp'/'CMD', 'Baby dragons can\'t figure out humans—if they didn\'t want to be killed, why were they made of meat and treasure?').
card_multiverse_id('furnace whelp'/'CMD', '247141').

card_in_set('garruk wildspeaker', 'CMD').
card_original_type('garruk wildspeaker'/'CMD', 'Planeswalker — Garruk').
card_original_text('garruk wildspeaker'/'CMD', '+1: Untap two target lands.\n-1: Put a 3/3 green Beast creature token onto the battlefield.\n-4: Creatures you control get +3/+3 and gain trample until end of turn.').
card_image_name('garruk wildspeaker'/'CMD', 'garruk wildspeaker').
card_uid('garruk wildspeaker'/'CMD', 'CMD:Garruk Wildspeaker:garruk wildspeaker').
card_rarity('garruk wildspeaker'/'CMD', 'Mythic Rare').
card_artist('garruk wildspeaker'/'CMD', 'Aleksi Briclot').
card_number('garruk wildspeaker'/'CMD', '157').
card_multiverse_id('garruk wildspeaker'/'CMD', '247323').

card_in_set('ghave, guru of spores', 'CMD').
card_original_type('ghave, guru of spores'/'CMD', 'Legendary Creature — Fungus Shaman').
card_original_text('ghave, guru of spores'/'CMD', 'Ghave, Guru of Spores enters the battlefield with five +1/+1 counters on it.\n{1}, Remove a +1/+1 counter from a creature you control: Put a 1/1 green Saproling creature token onto the battlefield.\n{1}, Sacrifice a creature: Put a +1/+1 counter on target creature.').
card_first_print('ghave, guru of spores', 'CMD').
card_image_name('ghave, guru of spores'/'CMD', 'ghave, guru of spores').
card_uid('ghave, guru of spores'/'CMD', 'CMD:Ghave, Guru of Spores:ghave, guru of spores').
card_rarity('ghave, guru of spores'/'CMD', 'Mythic Rare').
card_artist('ghave, guru of spores'/'CMD', 'James Paick').
card_number('ghave, guru of spores'/'CMD', '200').
card_multiverse_id('ghave, guru of spores'/'CMD', '236501').

card_in_set('ghostly prison', 'CMD').
card_original_type('ghostly prison'/'CMD', 'Enchantment').
card_original_text('ghostly prison'/'CMD', 'Creatures can\'t attack you unless their controller pays {2} for each creature he or she controls that\'s attacking you.').
card_image_name('ghostly prison'/'CMD', 'ghostly prison').
card_uid('ghostly prison'/'CMD', 'CMD:Ghostly Prison:ghostly prison').
card_rarity('ghostly prison'/'CMD', 'Uncommon').
card_artist('ghostly prison'/'CMD', 'Wayne England').
card_number('ghostly prison'/'CMD', '14').
card_flavor_text('ghostly prison'/'CMD', '\"May the memory of our fallen heroes ensnare the violent hearts of lesser men.\"\n—Great Threshold of Monserkat inscription').
card_multiverse_id('ghostly prison'/'CMD', '247173').

card_in_set('goblin cadets', 'CMD').
card_original_type('goblin cadets'/'CMD', 'Creature — Goblin').
card_original_text('goblin cadets'/'CMD', 'Whenever Goblin Cadets blocks or becomes blocked, target opponent gains control of it. (This removes Goblin Cadets from combat.)').
card_image_name('goblin cadets'/'CMD', 'goblin cadets').
card_uid('goblin cadets'/'CMD', 'CMD:Goblin Cadets:goblin cadets').
card_rarity('goblin cadets'/'CMD', 'Uncommon').
card_artist('goblin cadets'/'CMD', 'Jerry Tiritilli').
card_number('goblin cadets'/'CMD', '125').
card_flavor_text('goblin cadets'/'CMD', '\"If you kids don\'t stop that racket, I\'m turning this expedition around right now!\"').
card_multiverse_id('goblin cadets'/'CMD', '247529').

card_in_set('golgari guildmage', 'CMD').
card_original_type('golgari guildmage'/'CMD', 'Creature — Elf Shaman').
card_original_text('golgari guildmage'/'CMD', '{4}{B}, Sacrifice a creature: Return target creature card from your graveyard to your hand.\n{4}{G}: Put a +1/+1 counter on target creature.').
card_image_name('golgari guildmage'/'CMD', 'golgari guildmage').
card_uid('golgari guildmage'/'CMD', 'CMD:Golgari Guildmage:golgari guildmage').
card_rarity('golgari guildmage'/'CMD', 'Uncommon').
card_artist('golgari guildmage'/'CMD', 'Zoltan Boros & Gabor Szikszai').
card_number('golgari guildmage'/'CMD', '201').
card_multiverse_id('golgari guildmage'/'CMD', '247379').

card_in_set('golgari rot farm', 'CMD').
card_original_type('golgari rot farm'/'CMD', 'Land').
card_original_text('golgari rot farm'/'CMD', 'Golgari Rot Farm enters the battlefield tapped.\nWhen Golgari Rot Farm enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {B}{G} to your mana pool.').
card_image_name('golgari rot farm'/'CMD', 'golgari rot farm').
card_uid('golgari rot farm'/'CMD', 'CMD:Golgari Rot Farm:golgari rot farm').
card_rarity('golgari rot farm'/'CMD', 'Common').
card_artist('golgari rot farm'/'CMD', 'John Avon').
card_number('golgari rot farm'/'CMD', '275').
card_multiverse_id('golgari rot farm'/'CMD', '247380').

card_in_set('golgari signet', 'CMD').
card_original_type('golgari signet'/'CMD', 'Artifact').
card_original_text('golgari signet'/'CMD', '{1}, {T}: Add {B}{G} to your mana pool.').
card_image_name('golgari signet'/'CMD', 'golgari signet').
card_uid('golgari signet'/'CMD', 'CMD:Golgari Signet:golgari signet').
card_rarity('golgari signet'/'CMD', 'Common').
card_artist('golgari signet'/'CMD', 'Greg Hildebrandt').
card_number('golgari signet'/'CMD', '249').
card_flavor_text('golgari signet'/'CMD', 'Depending on your point of view, the seal represents a proud guardian of the natural cycle or one who has sold her soul to darkness for eternal life.').
card_multiverse_id('golgari signet'/'CMD', '247381').

card_in_set('gomazoa', 'CMD').
card_original_type('gomazoa'/'CMD', 'Creature — Jellyfish').
card_original_text('gomazoa'/'CMD', 'Defender, flying\n{T}: Put Gomazoa and each creature it\'s blocking on top of their owners\' libraries, then those players shuffle their libraries.').
card_image_name('gomazoa'/'CMD', 'gomazoa').
card_uid('gomazoa'/'CMD', 'CMD:Gomazoa:gomazoa').
card_rarity('gomazoa'/'CMD', 'Uncommon').
card_artist('gomazoa'/'CMD', 'Chippy').
card_number('gomazoa'/'CMD', '48').
card_flavor_text('gomazoa'/'CMD', 'To explorers, \"point man\" is a polite way of saying \"gomazoa fodder.\"').
card_multiverse_id('gomazoa'/'CMD', '247546').

card_in_set('grave pact', 'CMD').
card_original_type('grave pact'/'CMD', 'Enchantment').
card_original_text('grave pact'/'CMD', 'Whenever a creature you control is put into a graveyard from the battlefield, each other player sacrifices a creature.').
card_image_name('grave pact'/'CMD', 'grave pact').
card_uid('grave pact'/'CMD', 'CMD:Grave Pact:grave pact').
card_rarity('grave pact'/'CMD', 'Rare').
card_artist('grave pact'/'CMD', 'Puddnhead').
card_number('grave pact'/'CMD', '85').
card_flavor_text('grave pact'/'CMD', '\"The bonds of loyalty can tie one to the grave.\"\n—Crovax, ascendant evincar').
card_multiverse_id('grave pact'/'CMD', '247142').

card_in_set('gravedigger', 'CMD').
card_original_type('gravedigger'/'CMD', 'Creature — Zombie').
card_original_text('gravedigger'/'CMD', 'When Gravedigger enters the battlefield, you may return target creature card from your graveyard to your hand.').
card_image_name('gravedigger'/'CMD', 'gravedigger').
card_uid('gravedigger'/'CMD', 'CMD:Gravedigger:gravedigger').
card_rarity('gravedigger'/'CMD', 'Common').
card_artist('gravedigger'/'CMD', 'Dermot Power').
card_number('gravedigger'/'CMD', '86').
card_flavor_text('gravedigger'/'CMD', 'Only a thin layer of dirt separates civilization from an undead apocalypse.').
card_multiverse_id('gravedigger'/'CMD', '247324').

card_in_set('gruul signet', 'CMD').
card_original_type('gruul signet'/'CMD', 'Artifact').
card_original_text('gruul signet'/'CMD', '{1}, {T}: Add {R}{G} to your mana pool.').
card_image_name('gruul signet'/'CMD', 'gruul signet').
card_uid('gruul signet'/'CMD', 'CMD:Gruul Signet:gruul signet').
card_rarity('gruul signet'/'CMD', 'Common').
card_artist('gruul signet'/'CMD', 'Greg Hildebrandt').
card_number('gruul signet'/'CMD', '250').
card_flavor_text('gruul signet'/'CMD', 'Gruul territorial markings need not be legible. The blood, snot, and muck used to smear them are unmistakably Gruul.').
card_multiverse_id('gruul signet'/'CMD', '247277').

card_in_set('gruul turf', 'CMD').
card_original_type('gruul turf'/'CMD', 'Land').
card_original_text('gruul turf'/'CMD', 'Gruul Turf enters the battlefield tapped.\nWhen Gruul Turf enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {R}{G} to your mana pool.').
card_image_name('gruul turf'/'CMD', 'gruul turf').
card_uid('gruul turf'/'CMD', 'CMD:Gruul Turf:gruul turf').
card_rarity('gruul turf'/'CMD', 'Common').
card_artist('gruul turf'/'CMD', 'John Avon').
card_number('gruul turf'/'CMD', '276').
card_multiverse_id('gruul turf'/'CMD', '247278').

card_in_set('guard gomazoa', 'CMD').
card_original_type('guard gomazoa'/'CMD', 'Creature — Jellyfish').
card_original_text('guard gomazoa'/'CMD', 'Defender, flying\nPrevent all combat damage that would be dealt to Guard Gomazoa.').
card_image_name('guard gomazoa'/'CMD', 'guard gomazoa').
card_uid('guard gomazoa'/'CMD', 'CMD:Guard Gomazoa:guard gomazoa').
card_rarity('guard gomazoa'/'CMD', 'Uncommon').
card_artist('guard gomazoa'/'CMD', 'Rob Alexander').
card_number('guard gomazoa'/'CMD', '49').
card_flavor_text('guard gomazoa'/'CMD', 'It lingers near the outposts of the Makindi Trenches, inadvertently granting another layer of defense.').
card_multiverse_id('guard gomazoa'/'CMD', '247397').

card_in_set('gwyllion hedge-mage', 'CMD').
card_original_type('gwyllion hedge-mage'/'CMD', 'Creature — Hag Wizard').
card_original_text('gwyllion hedge-mage'/'CMD', 'When Gwyllion Hedge-Mage enters the battlefield, if you control two or more Plains, you may put a 1/1 white Kithkin Soldier creature token onto the battlefield.\nWhen Gwyllion Hedge-Mage enters the battlefield, if you control two or more Swamps, you may put a -1/-1 counter on target creature.').
card_image_name('gwyllion hedge-mage'/'CMD', 'gwyllion hedge-mage').
card_uid('gwyllion hedge-mage'/'CMD', 'CMD:Gwyllion Hedge-Mage:gwyllion hedge-mage').
card_rarity('gwyllion hedge-mage'/'CMD', 'Uncommon').
card_artist('gwyllion hedge-mage'/'CMD', 'Todd Lockwood').
card_number('gwyllion hedge-mage'/'CMD', '202').
card_multiverse_id('gwyllion hedge-mage'/'CMD', '247206').

card_in_set('harmonize', 'CMD').
card_original_type('harmonize'/'CMD', 'Sorcery').
card_original_text('harmonize'/'CMD', 'Draw three cards.').
card_image_name('harmonize'/'CMD', 'harmonize').
card_uid('harmonize'/'CMD', 'CMD:Harmonize:harmonize').
card_rarity('harmonize'/'CMD', 'Uncommon').
card_artist('harmonize'/'CMD', 'Paul Lee').
card_number('harmonize'/'CMD', '158').
card_flavor_text('harmonize'/'CMD', '\"Words lie. People lie. The land tells the truth.\"').
card_multiverse_id('harmonize'/'CMD', '247360').

card_in_set('hex', 'CMD').
card_original_type('hex'/'CMD', 'Sorcery').
card_original_text('hex'/'CMD', 'Destroy six target creatures.').
card_image_name('hex'/'CMD', 'hex').
card_uid('hex'/'CMD', 'CMD:Hex:hex').
card_rarity('hex'/'CMD', 'Rare').
card_artist('hex'/'CMD', 'Michael Sutfin').
card_number('hex'/'CMD', '87').
card_flavor_text('hex'/'CMD', 'When killing five just isn\'t enough.').
card_multiverse_id('hex'/'CMD', '247382').

card_in_set('homeward path', 'CMD').
card_original_type('homeward path'/'CMD', 'Land').
card_original_text('homeward path'/'CMD', '{T}: Add {1} to your mana pool.\n{T}: Each player gains control of all creatures he or she owns.').
card_first_print('homeward path', 'CMD').
card_image_name('homeward path'/'CMD', 'homeward path').
card_uid('homeward path'/'CMD', 'CMD:Homeward Path:homeward path').
card_rarity('homeward path'/'CMD', 'Rare').
card_artist('homeward path'/'CMD', 'Tomasz Jedruszek').
card_number('homeward path'/'CMD', '277').
card_flavor_text('homeward path'/'CMD', '\"Let your heels point you home.\"\n—Ancient blessing').
card_multiverse_id('homeward path'/'CMD', '233201').

card_in_set('hornet queen', 'CMD').
card_original_type('hornet queen'/'CMD', 'Creature — Insect').
card_original_text('hornet queen'/'CMD', 'Flying, deathtouch\nWhen Hornet Queen enters the battlefield, put four 1/1 green Insect creature tokens with flying and deathtouch onto the battlefield.').
card_first_print('hornet queen', 'CMD').
card_image_name('hornet queen'/'CMD', 'hornet queen').
card_uid('hornet queen'/'CMD', 'CMD:Hornet Queen:hornet queen').
card_rarity('hornet queen'/'CMD', 'Rare').
card_artist('hornet queen'/'CMD', 'Martina Pilcerova').
card_number('hornet queen'/'CMD', '159').
card_multiverse_id('hornet queen'/'CMD', '238141').

card_in_set('hour of reckoning', 'CMD').
card_original_type('hour of reckoning'/'CMD', 'Sorcery').
card_original_text('hour of reckoning'/'CMD', 'Convoke (Each creature you tap while casting this spell reduces its cost by {1} or by one mana of that creature\'s color.)\nDestroy all nontoken creatures.').
card_image_name('hour of reckoning'/'CMD', 'hour of reckoning').
card_uid('hour of reckoning'/'CMD', 'CMD:Hour of Reckoning:hour of reckoning').
card_rarity('hour of reckoning'/'CMD', 'Rare').
card_artist('hour of reckoning'/'CMD', 'Randy Gallegos').
card_number('hour of reckoning'/'CMD', '15').
card_flavor_text('hour of reckoning'/'CMD', '\"Ravnica, like a hedge, must be pruned, leaving only leaves of verdant uniformity.\"\n—Niszka, Selesnya evangel').
card_multiverse_id('hour of reckoning'/'CMD', '247383').

card_in_set('howling mine', 'CMD').
card_original_type('howling mine'/'CMD', 'Artifact').
card_original_text('howling mine'/'CMD', 'At the beginning of each player\'s draw step, if Howling Mine is untapped, that player draws an additional card.').
card_image_name('howling mine'/'CMD', 'howling mine').
card_uid('howling mine'/'CMD', 'CMD:Howling Mine:howling mine').
card_rarity('howling mine'/'CMD', 'Rare').
card_artist('howling mine'/'CMD', 'Ralph Horsley').
card_number('howling mine'/'CMD', '251').
card_flavor_text('howling mine'/'CMD', 'The mine\'s riches never end, nor do the moans of the spirits doomed to haunt them.').
card_multiverse_id('howling mine'/'CMD', '247316').

card_in_set('hull breach', 'CMD').
card_original_type('hull breach'/'CMD', 'Sorcery').
card_original_text('hull breach'/'CMD', 'Choose one — Destroy target artifact; or destroy target enchantment; or destroy target artifact and target enchantment.').
card_image_name('hull breach'/'CMD', 'hull breach').
card_uid('hull breach'/'CMD', 'CMD:Hull Breach:hull breach').
card_rarity('hull breach'/'CMD', 'Common').
card_artist('hull breach'/'CMD', 'Brian Snõddy').
card_number('hull breach'/'CMD', '203').
card_flavor_text('hull breach'/'CMD', '\"Crovax knows we\'re coming now,\" said a grinning Sisay. \"I just sent the Predator crashing into his stronghold.\"').
card_multiverse_id('hull breach'/'CMD', '247369').

card_in_set('hunting pack', 'CMD').
card_original_type('hunting pack'/'CMD', 'Instant').
card_original_text('hunting pack'/'CMD', 'Put a 4/4 green Beast creature token onto the battlefield.\nStorm (When you cast this spell, copy it for each spell cast before it this turn.)').
card_image_name('hunting pack'/'CMD', 'hunting pack').
card_uid('hunting pack'/'CMD', 'CMD:Hunting Pack:hunting pack').
card_rarity('hunting pack'/'CMD', 'Uncommon').
card_artist('hunting pack'/'CMD', 'Jim Nelson').
card_number('hunting pack'/'CMD', '160').
card_multiverse_id('hunting pack'/'CMD', '247405').

card_in_set('hydra omnivore', 'CMD').
card_original_type('hydra omnivore'/'CMD', 'Creature — Hydra').
card_original_text('hydra omnivore'/'CMD', 'Whenever Hydra Omnivore deals combat damage to an opponent, it deals that much damage to each other opponent.').
card_first_print('hydra omnivore', 'CMD').
card_image_name('hydra omnivore'/'CMD', 'hydra omnivore').
card_uid('hydra omnivore'/'CMD', 'CMD:Hydra Omnivore:hydra omnivore').
card_rarity('hydra omnivore'/'CMD', 'Rare').
card_artist('hydra omnivore'/'CMD', 'Svetlin Velinov').
card_number('hydra omnivore'/'CMD', '161').
card_flavor_text('hydra omnivore'/'CMD', '\"We should learn from the hydra. It never plays favorites; it devours everyone equally.\"\n—Thrass, elder druid').
card_multiverse_id('hydra omnivore'/'CMD', '228247').

card_in_set('ice', 'CMD').
card_original_type('ice'/'CMD', 'Instant').
card_original_text('ice'/'CMD', 'Fire deals 2 damage divided as you choose among one or two target creatures and/or players.\n//\nIce\n{1}{U}\nTap target permanent.\nDraw a card.\nFranz Vohwinkel').
card_image_name('ice'/'CMD', 'fireice').
card_uid('ice'/'CMD', 'CMD:Ice:fireice').
card_rarity('ice'/'CMD', 'Uncommon').
card_artist('ice'/'CMD', 'David Martin').
card_number('ice'/'CMD', '198b').
card_multiverse_id('ice'/'CMD', '247159').

card_in_set('insurrection', 'CMD').
card_original_type('insurrection'/'CMD', 'Sorcery').
card_original_text('insurrection'/'CMD', 'Untap all creatures and gain control of them until end of turn. They gain haste until end of turn.').
card_image_name('insurrection'/'CMD', 'insurrection').
card_uid('insurrection'/'CMD', 'CMD:Insurrection:insurrection').
card_rarity('insurrection'/'CMD', 'Rare').
card_artist('insurrection'/'CMD', 'Mark Zug').
card_number('insurrection'/'CMD', '126').
card_flavor_text('insurrection'/'CMD', '\"Maybe they wanted to be on the winning side for once.\"\n—Matoc, lavamancer').
card_multiverse_id('insurrection'/'CMD', '247347').

card_in_set('intet, the dreamer', 'CMD').
card_original_type('intet, the dreamer'/'CMD', 'Legendary Creature — Dragon').
card_original_text('intet, the dreamer'/'CMD', 'Flying\nWhenever Intet, the Dreamer deals combat damage to a player, you may pay {2}{U}. If you do, exile the top card of your library face down. You may look at that card for as long as it remains exiled. You may play that card without paying its mana cost for as long as Intet remains on the battlefield.').
card_image_name('intet, the dreamer'/'CMD', 'intet, the dreamer').
card_uid('intet, the dreamer'/'CMD', 'CMD:Intet, the Dreamer:intet, the dreamer').
card_rarity('intet, the dreamer'/'CMD', 'Rare').
card_artist('intet, the dreamer'/'CMD', 'Dan Scott').
card_number('intet, the dreamer'/'CMD', '204').
card_multiverse_id('intet, the dreamer'/'CMD', '247361').

card_in_set('invigorate', 'CMD').
card_original_type('invigorate'/'CMD', 'Instant').
card_original_text('invigorate'/'CMD', 'If you control a Forest, rather than pay Invigorate\'s mana cost, you may have an opponent gain 3 life.\nTarget creature gets +4/+4 until end of turn.').
card_image_name('invigorate'/'CMD', 'invigorate').
card_uid('invigorate'/'CMD', 'CMD:Invigorate:invigorate').
card_rarity('invigorate'/'CMD', 'Common').
card_artist('invigorate'/'CMD', 'Dan Frazier').
card_number('invigorate'/'CMD', '162').
card_multiverse_id('invigorate'/'CMD', '247332').

card_in_set('island', 'CMD').
card_original_type('island'/'CMD', 'Basic Land — Island').
card_original_text('island'/'CMD', 'U').
card_image_name('island'/'CMD', 'island1').
card_uid('island'/'CMD', 'CMD:Island:island1').
card_rarity('island'/'CMD', 'Basic Land').
card_artist('island'/'CMD', 'Rob Alexander').
card_number('island'/'CMD', '303').
card_multiverse_id('island'/'CMD', '249804').

card_in_set('island', 'CMD').
card_original_type('island'/'CMD', 'Basic Land — Island').
card_original_text('island'/'CMD', 'U').
card_image_name('island'/'CMD', 'island2').
card_uid('island'/'CMD', 'CMD:Island:island2').
card_rarity('island'/'CMD', 'Basic Land').
card_artist('island'/'CMD', 'John Avon').
card_number('island'/'CMD', '304').
card_multiverse_id('island'/'CMD', '249807').

card_in_set('island', 'CMD').
card_original_type('island'/'CMD', 'Basic Land — Island').
card_original_text('island'/'CMD', 'U').
card_image_name('island'/'CMD', 'island3').
card_uid('island'/'CMD', 'CMD:Island:island3').
card_rarity('island'/'CMD', 'Basic Land').
card_artist('island'/'CMD', 'John Avon').
card_number('island'/'CMD', '305').
card_multiverse_id('island'/'CMD', '249806').

card_in_set('island', 'CMD').
card_original_type('island'/'CMD', 'Basic Land — Island').
card_original_text('island'/'CMD', 'U').
card_image_name('island'/'CMD', 'island4').
card_uid('island'/'CMD', 'CMD:Island:island4').
card_rarity('island'/'CMD', 'Basic Land').
card_artist('island'/'CMD', 'Richard Wright').
card_number('island'/'CMD', '306').
card_multiverse_id('island'/'CMD', '249805').

card_in_set('izzet boilerworks', 'CMD').
card_original_type('izzet boilerworks'/'CMD', 'Land').
card_original_text('izzet boilerworks'/'CMD', 'Izzet Boilerworks enters the battlefield tapped.\nWhen Izzet Boilerworks enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {U}{R} to your mana pool.').
card_image_name('izzet boilerworks'/'CMD', 'izzet boilerworks').
card_uid('izzet boilerworks'/'CMD', 'CMD:Izzet Boilerworks:izzet boilerworks').
card_rarity('izzet boilerworks'/'CMD', 'Common').
card_artist('izzet boilerworks'/'CMD', 'John Avon').
card_number('izzet boilerworks'/'CMD', '278').
card_multiverse_id('izzet boilerworks'/'CMD', '247279').

card_in_set('izzet chronarch', 'CMD').
card_original_type('izzet chronarch'/'CMD', 'Creature — Human Wizard').
card_original_text('izzet chronarch'/'CMD', 'When Izzet Chronarch enters the battlefield, return target instant or sorcery card from your graveyard to your hand.').
card_image_name('izzet chronarch'/'CMD', 'izzet chronarch').
card_uid('izzet chronarch'/'CMD', 'CMD:Izzet Chronarch:izzet chronarch').
card_rarity('izzet chronarch'/'CMD', 'Common').
card_artist('izzet chronarch'/'CMD', 'Nick Percival').
card_number('izzet chronarch'/'CMD', '205').
card_flavor_text('izzet chronarch'/'CMD', 'He ensures not only whether but also when and where the lightning strikes twice.').
card_multiverse_id('izzet chronarch'/'CMD', '247280').

card_in_set('izzet signet', 'CMD').
card_original_type('izzet signet'/'CMD', 'Artifact').
card_original_text('izzet signet'/'CMD', '{1}, {T}: Add {U}{R} to your mana pool.').
card_image_name('izzet signet'/'CMD', 'izzet signet').
card_uid('izzet signet'/'CMD', 'CMD:Izzet Signet:izzet signet').
card_rarity('izzet signet'/'CMD', 'Common').
card_artist('izzet signet'/'CMD', 'Greg Hildebrandt').
card_number('izzet signet'/'CMD', '252').
card_flavor_text('izzet signet'/'CMD', 'The Izzet signet is redesigned often, each time becoming closer to a vanity portrait of Niv-Mizzet.').
card_multiverse_id('izzet signet'/'CMD', '247281').

card_in_set('jötun grunt', 'CMD').
card_original_type('jötun grunt'/'CMD', 'Creature — Giant Soldier').
card_original_text('jötun grunt'/'CMD', 'Cumulative upkeep—Put two cards from a single graveyard on the bottom of their owner\'s library. (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)').
card_image_name('jötun grunt'/'CMD', 'jotun grunt').
card_uid('jötun grunt'/'CMD', 'CMD:Jötun Grunt:jotun grunt').
card_rarity('jötun grunt'/'CMD', 'Uncommon').
card_artist('jötun grunt'/'CMD', 'Franz Vohwinkel').
card_number('jötun grunt'/'CMD', '16').
card_multiverse_id('jötun grunt'/'CMD', '247182').

card_in_set('journey to nowhere', 'CMD').
card_original_type('journey to nowhere'/'CMD', 'Enchantment').
card_original_text('journey to nowhere'/'CMD', 'When Journey to Nowhere enters the battlefield, exile target creature.\nWhen Journey to Nowhere leaves the battlefield, return the exiled card to the battlefield under its owner\'s control.').
card_image_name('journey to nowhere'/'CMD', 'journey to nowhere').
card_uid('journey to nowhere'/'CMD', 'CMD:Journey to Nowhere:journey to nowhere').
card_rarity('journey to nowhere'/'CMD', 'Common').
card_artist('journey to nowhere'/'CMD', 'Warren Mahy').
card_number('journey to nowhere'/'CMD', '17').
card_multiverse_id('journey to nowhere'/'CMD', '247547').

card_in_set('jwar isle refuge', 'CMD').
card_original_type('jwar isle refuge'/'CMD', 'Land').
card_original_text('jwar isle refuge'/'CMD', 'Jwar Isle Refuge enters the battlefield tapped.\nWhen Jwar Isle Refuge enters the battlefield, you gain 1 life.\n{T}: Add {U} or {B} to your mana pool.').
card_image_name('jwar isle refuge'/'CMD', 'jwar isle refuge').
card_uid('jwar isle refuge'/'CMD', 'CMD:Jwar Isle Refuge:jwar isle refuge').
card_rarity('jwar isle refuge'/'CMD', 'Uncommon').
card_artist('jwar isle refuge'/'CMD', 'Cyril Van Der Haegen').
card_number('jwar isle refuge'/'CMD', '279').
card_multiverse_id('jwar isle refuge'/'CMD', '247548').

card_in_set('kaalia of the vast', 'CMD').
card_original_type('kaalia of the vast'/'CMD', 'Legendary Creature — Human Cleric').
card_original_text('kaalia of the vast'/'CMD', 'Flying\nWhenever Kaalia of the Vast attacks an opponent, you may put an Angel, Demon, or Dragon creature card from your hand onto the battlefield tapped and attacking that opponent.').
card_first_print('kaalia of the vast', 'CMD').
card_image_name('kaalia of the vast'/'CMD', 'kaalia of the vast').
card_uid('kaalia of the vast'/'CMD', 'CMD:Kaalia of the Vast:kaalia of the vast').
card_rarity('kaalia of the vast'/'CMD', 'Mythic Rare').
card_artist('kaalia of the vast'/'CMD', 'Michael Komarck').
card_number('kaalia of the vast'/'CMD', '206').
card_flavor_text('kaalia of the vast'/'CMD', '\"I\'ll have my revenge if I have to call on every force from above and below.\"').
card_multiverse_id('kaalia of the vast'/'CMD', '236473').

card_in_set('karador, ghost chieftain', 'CMD').
card_original_type('karador, ghost chieftain'/'CMD', 'Legendary Creature — Centaur Spirit').
card_original_text('karador, ghost chieftain'/'CMD', 'Karador, Ghost Chieftain costs {1} less to cast for each creature card in your graveyard.\nDuring each of your turns, you may cast one creature card from your graveyard.').
card_image_name('karador, ghost chieftain'/'CMD', 'karador, ghost chieftain').
card_uid('karador, ghost chieftain'/'CMD', 'CMD:Karador, Ghost Chieftain:karador, ghost chieftain').
card_rarity('karador, ghost chieftain'/'CMD', 'Mythic Rare').
card_artist('karador, ghost chieftain'/'CMD', 'Todd Lockwood').
card_number('karador, ghost chieftain'/'CMD', '207').
card_flavor_text('karador, ghost chieftain'/'CMD', '\"Death tried to uncrown me. But now I return, king of a greater realm.\"').
card_multiverse_id('karador, ghost chieftain'/'CMD', '236468').

card_in_set('kazandu refuge', 'CMD').
card_original_type('kazandu refuge'/'CMD', 'Land').
card_original_text('kazandu refuge'/'CMD', 'Kazandu Refuge enters the battlefield tapped.\nWhen Kazandu Refuge enters the battlefield, you gain 1 life.\n{T}: Add {R} or {G} to your mana pool.').
card_image_name('kazandu refuge'/'CMD', 'kazandu refuge').
card_uid('kazandu refuge'/'CMD', 'CMD:Kazandu Refuge:kazandu refuge').
card_rarity('kazandu refuge'/'CMD', 'Uncommon').
card_artist('kazandu refuge'/'CMD', 'Franz Vohwinkel').
card_number('kazandu refuge'/'CMD', '280').
card_multiverse_id('kazandu refuge'/'CMD', '247549').

card_in_set('kodama\'s reach', 'CMD').
card_original_type('kodama\'s reach'/'CMD', 'Sorcery — Arcane').
card_original_text('kodama\'s reach'/'CMD', 'Search your library for two basic land cards, reveal those cards, and put one onto the battlefield tapped and the other into your hand. Then shuffle your library.').
card_image_name('kodama\'s reach'/'CMD', 'kodama\'s reach').
card_uid('kodama\'s reach'/'CMD', 'CMD:Kodama\'s Reach:kodama\'s reach').
card_rarity('kodama\'s reach'/'CMD', 'Common').
card_artist('kodama\'s reach'/'CMD', 'Heather Hudson').
card_number('kodama\'s reach'/'CMD', '163').
card_flavor_text('kodama\'s reach'/'CMD', '\"The land grows only where the kami will it.\"\n—Dosan the Falling Leaf').
card_multiverse_id('kodama\'s reach'/'CMD', '247174').

card_in_set('krosan tusker', 'CMD').
card_original_type('krosan tusker'/'CMD', 'Creature — Boar Beast').
card_original_text('krosan tusker'/'CMD', 'Cycling {2}{G} ({2}{G}, Discard this card: Draw a card.)\nWhen you cycle Krosan Tusker, you may search your library for a basic land card, reveal that card, put it into your hand, then shuffle your library.').
card_image_name('krosan tusker'/'CMD', 'krosan tusker').
card_uid('krosan tusker'/'CMD', 'CMD:Krosan Tusker:krosan tusker').
card_rarity('krosan tusker'/'CMD', 'Common').
card_artist('krosan tusker'/'CMD', 'Kev Walker').
card_number('krosan tusker'/'CMD', '164').
card_multiverse_id('krosan tusker'/'CMD', '247348').

card_in_set('lash out', 'CMD').
card_original_type('lash out'/'CMD', 'Instant').
card_original_text('lash out'/'CMD', 'Lash Out deals 3 damage to target creature. Clash with an opponent. If you win, Lash Out deals 3 damage to that creature\'s controller. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_image_name('lash out'/'CMD', 'lash out').
card_uid('lash out'/'CMD', 'CMD:Lash Out:lash out').
card_rarity('lash out'/'CMD', 'Common').
card_artist('lash out'/'CMD', 'Scott Hampton').
card_number('lash out'/'CMD', '127').
card_multiverse_id('lash out'/'CMD', '247301').

card_in_set('lhurgoyf', 'CMD').
card_original_type('lhurgoyf'/'CMD', 'Creature — Lhurgoyf').
card_original_text('lhurgoyf'/'CMD', 'Lhurgoyf\'s power is equal to the number of creature cards in all graveyards and its toughness is equal to that number plus 1.').
card_image_name('lhurgoyf'/'CMD', 'lhurgoyf').
card_uid('lhurgoyf'/'CMD', 'CMD:Lhurgoyf:lhurgoyf').
card_rarity('lhurgoyf'/'CMD', 'Rare').
card_artist('lhurgoyf'/'CMD', 'Pete Venters').
card_number('lhurgoyf'/'CMD', '165').
card_flavor_text('lhurgoyf'/'CMD', '\"Ach! Hans, run! It\'s the lhurgoyf!\"\n—Saffi Eriksdotter, last words').
card_multiverse_id('lhurgoyf'/'CMD', '247151').

card_in_set('lightkeeper of emeria', 'CMD').
card_original_type('lightkeeper of emeria'/'CMD', 'Creature — Angel').
card_original_text('lightkeeper of emeria'/'CMD', 'Multikicker {W} (You may pay an additional {W} any number of times as you cast this spell.)\nFlying\nWhen Lightkeeper of Emeria enters the battlefield, you gain 2 life for each time it was kicked.').
card_image_name('lightkeeper of emeria'/'CMD', 'lightkeeper of emeria').
card_uid('lightkeeper of emeria'/'CMD', 'CMD:Lightkeeper of Emeria:lightkeeper of emeria').
card_rarity('lightkeeper of emeria'/'CMD', 'Uncommon').
card_artist('lightkeeper of emeria'/'CMD', 'James Ryman').
card_number('lightkeeper of emeria'/'CMD', '18').
card_multiverse_id('lightkeeper of emeria'/'CMD', '247540').

card_in_set('lightning greaves', 'CMD').
card_original_type('lightning greaves'/'CMD', 'Artifact — Equipment').
card_original_text('lightning greaves'/'CMD', 'Equipped creature has haste and shroud. (It can\'t be the target of spells or abilities.)\nEquip {0}').
card_image_name('lightning greaves'/'CMD', 'lightning greaves').
card_uid('lightning greaves'/'CMD', 'CMD:Lightning Greaves:lightning greaves').
card_rarity('lightning greaves'/'CMD', 'Uncommon').
card_artist('lightning greaves'/'CMD', 'Jeremy Jarvis').
card_number('lightning greaves'/'CMD', '253').
card_flavor_text('lightning greaves'/'CMD', 'After lightning struck the cliffs, the ore became iron, the iron became steel, and the steel became greaves. The lightning never left.').
card_multiverse_id('lightning greaves'/'CMD', '247337').

card_in_set('living death', 'CMD').
card_original_type('living death'/'CMD', 'Sorcery').
card_original_text('living death'/'CMD', 'Each player exiles all creature cards from his or her graveyard, then sacrifices all creatures he or she controls, then puts all cards he or she exiled this way onto the battlefield.').
card_image_name('living death'/'CMD', 'living death').
card_uid('living death'/'CMD', 'CMD:Living Death:living death').
card_rarity('living death'/'CMD', 'Rare').
card_artist('living death'/'CMD', 'Charles Gillespie').
card_number('living death'/'CMD', '88').
card_multiverse_id('living death'/'CMD', '247416').

card_in_set('lonely sandbar', 'CMD').
card_original_type('lonely sandbar'/'CMD', 'Land').
card_original_text('lonely sandbar'/'CMD', 'Lonely Sandbar enters the battlefield tapped.\n{T}: Add {U} to your mana pool.\nCycling {U} ({U}, Discard this card: Draw a card.)').
card_image_name('lonely sandbar'/'CMD', 'lonely sandbar').
card_uid('lonely sandbar'/'CMD', 'CMD:Lonely Sandbar:lonely sandbar').
card_rarity('lonely sandbar'/'CMD', 'Common').
card_artist('lonely sandbar'/'CMD', 'Heather Hudson').
card_number('lonely sandbar'/'CMD', '281').
card_multiverse_id('lonely sandbar'/'CMD', '247349').

card_in_set('magmatic force', 'CMD').
card_original_type('magmatic force'/'CMD', 'Creature — Elemental').
card_original_text('magmatic force'/'CMD', 'At the beginning of each upkeep, Magmatic Force deals 3 damage to target creature or player.').
card_first_print('magmatic force', 'CMD').
card_image_name('magmatic force'/'CMD', 'magmatic force').
card_uid('magmatic force'/'CMD', 'CMD:Magmatic Force:magmatic force').
card_rarity('magmatic force'/'CMD', 'Rare').
card_artist('magmatic force'/'CMD', 'Jung Park').
card_number('magmatic force'/'CMD', '128').
card_flavor_text('magmatic force'/'CMD', '\"Granted, it\'s the pinnacle of utter destructiveness. So what would you call two of them?\"\n—Riku of Two Reflections').
card_multiverse_id('magmatic force'/'CMD', '228241').

card_in_set('magus of the vineyard', 'CMD').
card_original_type('magus of the vineyard'/'CMD', 'Creature — Human Wizard').
card_original_text('magus of the vineyard'/'CMD', 'At the beginning of each player\'s precombat main phase, add {G}{G} to that player\'s mana pool.').
card_image_name('magus of the vineyard'/'CMD', 'magus of the vineyard').
card_uid('magus of the vineyard'/'CMD', 'CMD:Magus of the Vineyard:magus of the vineyard').
card_rarity('magus of the vineyard'/'CMD', 'Rare').
card_artist('magus of the vineyard'/'CMD', 'Jim Murray').
card_number('magus of the vineyard'/'CMD', '166').
card_flavor_text('magus of the vineyard'/'CMD', 'Battered and beaten by years of salt and claw, he never ceased to walk, and to seed. Only now, in this time of rebirth, do his seeds take root.').
card_multiverse_id('magus of the vineyard'/'CMD', '247273').

card_in_set('malfegor', 'CMD').
card_original_type('malfegor'/'CMD', 'Legendary Creature — Demon Dragon').
card_original_text('malfegor'/'CMD', 'Flying\nWhen Malfegor enters the battlefield, discard your hand. Each opponent sacrifices a creature for each card discarded this way.').
card_image_name('malfegor'/'CMD', 'malfegor').
card_uid('malfegor'/'CMD', 'CMD:Malfegor:malfegor').
card_rarity('malfegor'/'CMD', 'Mythic Rare').
card_artist('malfegor'/'CMD', 'Jason Chan').
card_number('malfegor'/'CMD', '208').
card_flavor_text('malfegor'/'CMD', 'A demon cannot be trusted, and a dragon will not be ruled.').
card_multiverse_id('malfegor'/'CMD', '247179').

card_in_set('mana-charged dragon', 'CMD').
card_original_type('mana-charged dragon'/'CMD', 'Creature — Dragon').
card_original_text('mana-charged dragon'/'CMD', 'Flying, trample\nJoin forces — Whenever Mana-Charged Dragon attacks or blocks, each player starting with you may pay any amount of mana. Mana-Charged Dragon gets +X/+0 until end of turn, where X is the total amount of mana paid this way.').
card_first_print('mana-charged dragon', 'CMD').
card_image_name('mana-charged dragon'/'CMD', 'mana-charged dragon').
card_uid('mana-charged dragon'/'CMD', 'CMD:Mana-Charged Dragon:mana-charged dragon').
card_rarity('mana-charged dragon'/'CMD', 'Rare').
card_artist('mana-charged dragon'/'CMD', 'Mike Bierek').
card_number('mana-charged dragon'/'CMD', '129').
card_multiverse_id('mana-charged dragon'/'CMD', '236465').

card_in_set('martyr\'s bond', 'CMD').
card_original_type('martyr\'s bond'/'CMD', 'Enchantment').
card_original_text('martyr\'s bond'/'CMD', 'Whenever Martyr\'s Bond or another nonland permanent you control is put into a graveyard from the battlefield, each opponent sacrifices a permanent that shares a card type with it.').
card_first_print('martyr\'s bond', 'CMD').
card_image_name('martyr\'s bond'/'CMD', 'martyr\'s bond').
card_uid('martyr\'s bond'/'CMD', 'CMD:Martyr\'s Bond:martyr\'s bond').
card_rarity('martyr\'s bond'/'CMD', 'Rare').
card_artist('martyr\'s bond'/'CMD', 'Ryan Pancoast').
card_number('martyr\'s bond'/'CMD', '19').
card_flavor_text('martyr\'s bond'/'CMD', '\"Our lives are connected by divine law. The truth of this will be made evident in time.\"\n—Zedruu the Greathearted').
card_multiverse_id('martyr\'s bond'/'CMD', '236481').

card_in_set('master warcraft', 'CMD').
card_original_type('master warcraft'/'CMD', 'Instant').
card_original_text('master warcraft'/'CMD', 'Cast Master Warcraft only before attackers are declared.\nYou choose which creatures attack this turn. You choose how each creature blocks this turn.').
card_image_name('master warcraft'/'CMD', 'master warcraft').
card_uid('master warcraft'/'CMD', 'CMD:Master Warcraft:master warcraft').
card_rarity('master warcraft'/'CMD', 'Rare').
card_artist('master warcraft'/'CMD', 'Zoltan Boros & Gabor Szikszai').
card_number('master warcraft'/'CMD', '209').
card_multiverse_id('master warcraft'/'CMD', '247384').

card_in_set('memory erosion', 'CMD').
card_original_type('memory erosion'/'CMD', 'Enchantment').
card_original_text('memory erosion'/'CMD', 'Whenever an opponent casts a spell, that player puts the top two cards of his or her library into his or her graveyard.').
card_image_name('memory erosion'/'CMD', 'memory erosion').
card_uid('memory erosion'/'CMD', 'CMD:Memory Erosion:memory erosion').
card_rarity('memory erosion'/'CMD', 'Rare').
card_artist('memory erosion'/'CMD', 'Howard Lyon').
card_number('memory erosion'/'CMD', '50').
card_flavor_text('memory erosion'/'CMD', '\"The Filigree Texts do not compel you to act in accordance with their precepts. They only specify the consequences should you fail.\"\n—Lodus of the Ethersworn').
card_multiverse_id('memory erosion'/'CMD', '247154').

card_in_set('minds aglow', 'CMD').
card_original_type('minds aglow'/'CMD', 'Sorcery').
card_original_text('minds aglow'/'CMD', 'Join forces — Starting with you, each player may pay any amount of mana. Each player draws X cards, where X is the total amount of mana paid this way.').
card_first_print('minds aglow', 'CMD').
card_image_name('minds aglow'/'CMD', 'minds aglow').
card_uid('minds aglow'/'CMD', 'CMD:Minds Aglow:minds aglow').
card_rarity('minds aglow'/'CMD', 'Rare').
card_artist('minds aglow'/'CMD', 'Yeong-Hao Han').
card_number('minds aglow'/'CMD', '51').
card_multiverse_id('minds aglow'/'CMD', '241853').

card_in_set('molten slagheap', 'CMD').
card_original_type('molten slagheap'/'CMD', 'Land').
card_original_text('molten slagheap'/'CMD', '{T}: Add {1} to your mana pool.\n{1}, {T}: Put a storage counter on Molten Slagheap.\n{1}, Remove X storage counters from Molten Slagheap: Add X mana in any combination of {B} and/or {R} to your mana pool.').
card_image_name('molten slagheap'/'CMD', 'molten slagheap').
card_uid('molten slagheap'/'CMD', 'CMD:Molten Slagheap:molten slagheap').
card_rarity('molten slagheap'/'CMD', 'Uncommon').
card_artist('molten slagheap'/'CMD', 'Daren Bader').
card_number('molten slagheap'/'CMD', '282').
card_multiverse_id('molten slagheap'/'CMD', '247515').

card_in_set('monk realist', 'CMD').
card_original_type('monk realist'/'CMD', 'Creature — Human Monk Cleric').
card_original_text('monk realist'/'CMD', 'When Monk Realist enters the battlefield, destroy target enchantment.').
card_image_name('monk realist'/'CMD', 'monk realist').
card_uid('monk realist'/'CMD', 'CMD:Monk Realist:monk realist').
card_rarity('monk realist'/'CMD', 'Common').
card_artist('monk realist'/'CMD', 'Daren Bader').
card_number('monk realist'/'CMD', '20').
card_flavor_text('monk realist'/'CMD', '\"We plant the seeds of doubt to harvest the crop of wisdom.\"').
card_multiverse_id('monk realist'/'CMD', '247530').

card_in_set('mortify', 'CMD').
card_original_type('mortify'/'CMD', 'Instant').
card_original_text('mortify'/'CMD', 'Destroy target creature or enchantment.').
card_image_name('mortify'/'CMD', 'mortify').
card_uid('mortify'/'CMD', 'CMD:Mortify:mortify').
card_rarity('mortify'/'CMD', 'Uncommon').
card_artist('mortify'/'CMD', 'Glen Angus').
card_number('mortify'/'CMD', '211').
card_flavor_text('mortify'/'CMD', 'The eyes let flow with tears, then blood, then the very soul—the whole wrung inside out, dripping down into the blackened puddle of the past.').
card_multiverse_id('mortify'/'CMD', '247282').

card_in_set('mortivore', 'CMD').
card_original_type('mortivore'/'CMD', 'Creature — Lhurgoyf').
card_original_text('mortivore'/'CMD', 'Mortivore\'s power and toughness are each equal to the number of creature cards in all graveyards.\n{B}: Regenerate Mortivore.').
card_image_name('mortivore'/'CMD', 'mortivore').
card_uid('mortivore'/'CMD', 'CMD:Mortivore:mortivore').
card_rarity('mortivore'/'CMD', 'Rare').
card_artist('mortivore'/'CMD', 'Anthony S. Waters').
card_number('mortivore'/'CMD', '89').
card_flavor_text('mortivore'/'CMD', 'The light sigh of its breath whistles through its bones.').
card_multiverse_id('mortivore'/'CMD', '247143').

card_in_set('mother of runes', 'CMD').
card_original_type('mother of runes'/'CMD', 'Creature — Human Cleric').
card_original_text('mother of runes'/'CMD', '{T}: Target creature you control gains protection from the color of your choice until end of turn.').
card_image_name('mother of runes'/'CMD', 'mother of runes').
card_uid('mother of runes'/'CMD', 'CMD:Mother of Runes:mother of runes').
card_rarity('mother of runes'/'CMD', 'Uncommon').
card_artist('mother of runes'/'CMD', 'Scott M. Fischer').
card_number('mother of runes'/'CMD', '21').
card_flavor_text('mother of runes'/'CMD', '\"My family protects all families.\"').
card_multiverse_id('mother of runes'/'CMD', '247525').

card_in_set('mountain', 'CMD').
card_original_type('mountain'/'CMD', 'Basic Land — Mountain').
card_original_text('mountain'/'CMD', 'R').
card_image_name('mountain'/'CMD', 'mountain1').
card_uid('mountain'/'CMD', 'CMD:Mountain:mountain1').
card_rarity('mountain'/'CMD', 'Basic Land').
card_artist('mountain'/'CMD', 'John Avon').
card_number('mountain'/'CMD', '311').
card_multiverse_id('mountain'/'CMD', '249810').

card_in_set('mountain', 'CMD').
card_original_type('mountain'/'CMD', 'Basic Land — Mountain').
card_original_text('mountain'/'CMD', 'R').
card_image_name('mountain'/'CMD', 'mountain2').
card_uid('mountain'/'CMD', 'CMD:Mountain:mountain2').
card_rarity('mountain'/'CMD', 'Basic Land').
card_artist('mountain'/'CMD', 'John Avon').
card_number('mountain'/'CMD', '312').
card_multiverse_id('mountain'/'CMD', '249809').

card_in_set('mountain', 'CMD').
card_original_type('mountain'/'CMD', 'Basic Land — Mountain').
card_original_text('mountain'/'CMD', 'R').
card_image_name('mountain'/'CMD', 'mountain3').
card_uid('mountain'/'CMD', 'CMD:Mountain:mountain3').
card_rarity('mountain'/'CMD', 'Basic Land').
card_artist('mountain'/'CMD', 'Karl Kopinski').
card_number('mountain'/'CMD', '313').
card_multiverse_id('mountain'/'CMD', '249808').

card_in_set('mountain', 'CMD').
card_original_type('mountain'/'CMD', 'Basic Land — Mountain').
card_original_text('mountain'/'CMD', 'R').
card_image_name('mountain'/'CMD', 'mountain4').
card_uid('mountain'/'CMD', 'CMD:Mountain:mountain4').
card_rarity('mountain'/'CMD', 'Basic Land').
card_artist('mountain'/'CMD', 'Martina Pilcerova').
card_number('mountain'/'CMD', '314').
card_multiverse_id('mountain'/'CMD', '249811').

card_in_set('mulldrifter', 'CMD').
card_original_type('mulldrifter'/'CMD', 'Creature — Elemental').
card_original_text('mulldrifter'/'CMD', 'Flying\nWhen Mulldrifter enters the battlefield, draw two cards.\nEvoke {2}{U} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_image_name('mulldrifter'/'CMD', 'mulldrifter').
card_uid('mulldrifter'/'CMD', 'CMD:Mulldrifter:mulldrifter').
card_rarity('mulldrifter'/'CMD', 'Common').
card_artist('mulldrifter'/'CMD', 'Eric Fortune').
card_number('mulldrifter'/'CMD', '52').
card_multiverse_id('mulldrifter'/'CMD', '247302').

card_in_set('murmurs from beyond', 'CMD').
card_original_type('murmurs from beyond'/'CMD', 'Instant — Arcane').
card_original_text('murmurs from beyond'/'CMD', 'Reveal the top three cards of your library. An opponent chooses one of them. Put that card into your graveyard and the rest into your hand.').
card_image_name('murmurs from beyond'/'CMD', 'murmurs from beyond').
card_uid('murmurs from beyond'/'CMD', 'CMD:Murmurs from Beyond:murmurs from beyond').
card_rarity('murmurs from beyond'/'CMD', 'Common').
card_artist('murmurs from beyond'/'CMD', 'Paolo Parente').
card_number('murmurs from beyond'/'CMD', '53').
card_flavor_text('murmurs from beyond'/'CMD', 'Like pools of fog that hid treacherous rocks from sailors\' eyes, so too were the hands of the kami.').
card_multiverse_id('murmurs from beyond'/'CMD', '247410').

card_in_set('nantuko husk', 'CMD').
card_original_type('nantuko husk'/'CMD', 'Creature — Zombie Insect').
card_original_text('nantuko husk'/'CMD', 'Sacrifice a creature: Nantuko Husk gets +2/+2 until end of turn.').
card_image_name('nantuko husk'/'CMD', 'nantuko husk').
card_uid('nantuko husk'/'CMD', 'CMD:Nantuko Husk:nantuko husk').
card_rarity('nantuko husk'/'CMD', 'Uncommon').
card_artist('nantuko husk'/'CMD', 'Carl Critchlow').
card_number('nantuko husk'/'CMD', '90').
card_flavor_text('nantuko husk'/'CMD', 'The soul sheds light, and death is its shadow. When the light dims, life and death embrace.\n—Nantuko teaching').
card_multiverse_id('nantuko husk'/'CMD', '247144').

card_in_set('necrogenesis', 'CMD').
card_original_type('necrogenesis'/'CMD', 'Enchantment').
card_original_text('necrogenesis'/'CMD', '{2}: Exile target creature card from a graveyard. Put a 1/1 green Saproling creature token onto the battlefield.').
card_image_name('necrogenesis'/'CMD', 'necrogenesis').
card_uid('necrogenesis'/'CMD', 'CMD:Necrogenesis:necrogenesis').
card_rarity('necrogenesis'/'CMD', 'Uncommon').
card_artist('necrogenesis'/'CMD', 'Trevor Claxton').
card_number('necrogenesis'/'CMD', '212').
card_flavor_text('necrogenesis'/'CMD', '\"Those may be the squirms of one life ending or of another beginning. Either way, I\'d leave it alone.\"\n—Rakka Mar').
card_multiverse_id('necrogenesis'/'CMD', '247155').

card_in_set('nemesis trap', 'CMD').
card_original_type('nemesis trap'/'CMD', 'Instant — Trap').
card_original_text('nemesis trap'/'CMD', 'If a white creature is attacking, you may pay {B}{B} rather than pay Nemesis Trap\'s mana cost.\nExile target attacking creature. Put a token that\'s a copy of that creature onto the battlefield. Exile it at the beginning of the next end step.').
card_image_name('nemesis trap'/'CMD', 'nemesis trap').
card_uid('nemesis trap'/'CMD', 'CMD:Nemesis Trap:nemesis trap').
card_rarity('nemesis trap'/'CMD', 'Uncommon').
card_artist('nemesis trap'/'CMD', 'Ryan Pancoast').
card_number('nemesis trap'/'CMD', '91').
card_multiverse_id('nemesis trap'/'CMD', '247541').

card_in_set('nezumi graverobber', 'CMD').
card_original_type('nezumi graverobber'/'CMD', 'Creature — Rat Rogue').
card_original_text('nezumi graverobber'/'CMD', '{1}{B}: Exile target card from an opponent\'s graveyard. If no cards are in that graveyard, flip Nezumi Graverobber.\n-----\nNighteyes the Desecrator\nLegendary Creature — Rat Wizard\n4/2\n{4}{B}: Put target creature card from a graveyard onto the battlefield under your control.').
card_image_name('nezumi graverobber'/'CMD', 'nezumi graverobber').
card_uid('nezumi graverobber'/'CMD', 'CMD:Nezumi Graverobber:nezumi graverobber').
card_rarity('nezumi graverobber'/'CMD', 'Uncommon').
card_artist('nezumi graverobber'/'CMD', 'Jim Nelson').
card_number('nezumi graverobber'/'CMD', '92a').
card_multiverse_id('nezumi graverobber'/'CMD', '247175').

card_in_set('nighteyes the desecrator', 'CMD').
card_original_type('nighteyes the desecrator'/'CMD', 'Creature — Rat Rogue').
card_original_text('nighteyes the desecrator'/'CMD', '{1}{B}: Exile target card from an opponent\'s graveyard. If no cards are in that graveyard, flip Nezumi Graverobber.\n-----\nNighteyes the Desecrator\nLegendary Creature — Rat Wizard\n4/2\n{4}{B}: Put target creature card from a graveyard onto the battlefield under your control.').
card_image_name('nighteyes the desecrator'/'CMD', 'nighteyes the desecrator').
card_uid('nighteyes the desecrator'/'CMD', 'CMD:Nighteyes the Desecrator:nighteyes the desecrator').
card_rarity('nighteyes the desecrator'/'CMD', 'Uncommon').
card_artist('nighteyes the desecrator'/'CMD', 'Jim Nelson').
card_number('nighteyes the desecrator'/'CMD', '92b').
card_multiverse_id('nighteyes the desecrator'/'CMD', '247175').

card_in_set('nin, the pain artist', 'CMD').
card_original_type('nin, the pain artist'/'CMD', 'Legendary Creature — Vedalken Wizard').
card_original_text('nin, the pain artist'/'CMD', '{X}{U}{R}, {T}: Nin, the Pain Artist deals X damage to target creature. That creature\'s controller draws X cards.').
card_first_print('nin, the pain artist', 'CMD').
card_image_name('nin, the pain artist'/'CMD', 'nin, the pain artist').
card_uid('nin, the pain artist'/'CMD', 'CMD:Nin, the Pain Artist:nin, the pain artist').
card_rarity('nin, the pain artist'/'CMD', 'Rare').
card_artist('nin, the pain artist'/'CMD', 'Brad Rigney').
card_number('nin, the pain artist'/'CMD', '213').
card_flavor_text('nin, the pain artist'/'CMD', '\"Your body is a delicate instrument that tells me truths. These devices help me ‘tune\' that instrument.\"').
card_multiverse_id('nin, the pain artist'/'CMD', '236471').

card_in_set('nucklavee', 'CMD').
card_original_type('nucklavee'/'CMD', 'Creature — Beast').
card_original_text('nucklavee'/'CMD', 'When Nucklavee enters the battlefield, you may return target red sorcery card from your graveyard to your hand.\nWhen Nucklavee enters the battlefield, you may return target blue instant card from your graveyard to your hand.').
card_image_name('nucklavee'/'CMD', 'nucklavee').
card_uid('nucklavee'/'CMD', 'CMD:Nucklavee:nucklavee').
card_rarity('nucklavee'/'CMD', 'Uncommon').
card_artist('nucklavee'/'CMD', 'Trevor Hairsine').
card_number('nucklavee'/'CMD', '214').
card_flavor_text('nucklavee'/'CMD', 'It loathes all tastes but two: spells and flesh.').
card_multiverse_id('nucklavee'/'CMD', '247207').

card_in_set('numot, the devastator', 'CMD').
card_original_type('numot, the devastator'/'CMD', 'Legendary Creature — Dragon').
card_original_text('numot, the devastator'/'CMD', 'Flying\nWhenever Numot, the Devastator deals combat damage to a player, you may pay {2}{R}. If you do, destroy up to two target lands.').
card_image_name('numot, the devastator'/'CMD', 'numot, the devastator').
card_uid('numot, the devastator'/'CMD', 'CMD:Numot, the Devastator:numot, the devastator').
card_rarity('numot, the devastator'/'CMD', 'Rare').
card_artist('numot, the devastator'/'CMD', 'Dan Dos Santos').
card_number('numot, the devastator'/'CMD', '215').
card_multiverse_id('numot, the devastator'/'CMD', '247362').

card_in_set('oblation', 'CMD').
card_original_type('oblation'/'CMD', 'Instant').
card_original_text('oblation'/'CMD', 'The owner of target nonland permanent shuffles it into his or her library, then draws two cards.').
card_image_name('oblation'/'CMD', 'oblation').
card_uid('oblation'/'CMD', 'CMD:Oblation:oblation').
card_rarity('oblation'/'CMD', 'Rare').
card_artist('oblation'/'CMD', 'Doug Chaffee').
card_number('oblation'/'CMD', '22').
card_flavor_text('oblation'/'CMD', '\"A richer people could give more but they could never give as much.\"').
card_multiverse_id('oblation'/'CMD', '247350').

card_in_set('oblivion ring', 'CMD').
card_original_type('oblivion ring'/'CMD', 'Enchantment').
card_original_text('oblivion ring'/'CMD', 'When Oblivion Ring enters the battlefield, exile another target nonland permanent.\nWhen Oblivion Ring leaves the battlefield, return the exiled card to the battlefield under its owner\'s control.').
card_image_name('oblivion ring'/'CMD', 'oblivion ring').
card_uid('oblivion ring'/'CMD', 'CMD:Oblivion Ring:oblivion ring').
card_rarity('oblivion ring'/'CMD', 'Common').
card_artist('oblivion ring'/'CMD', 'Franz Vohwinkel').
card_number('oblivion ring'/'CMD', '23').
card_multiverse_id('oblivion ring'/'CMD', '247156').

card_in_set('oblivion stone', 'CMD').
card_original_type('oblivion stone'/'CMD', 'Artifact').
card_original_text('oblivion stone'/'CMD', '{4}, {T}: Put a fate counter on target permanent.\n{5}, {T}, Sacrifice Oblivion Stone: Destroy each nonland permanent without a fate counter on it, then remove all fate counters from all permanents.').
card_image_name('oblivion stone'/'CMD', 'oblivion stone').
card_uid('oblivion stone'/'CMD', 'CMD:Oblivion Stone:oblivion stone').
card_rarity('oblivion stone'/'CMD', 'Rare').
card_artist('oblivion stone'/'CMD', 'Sam Wood').
card_number('oblivion stone'/'CMD', '254').
card_multiverse_id('oblivion stone'/'CMD', '247338').

card_in_set('oni of wild places', 'CMD').
card_original_type('oni of wild places'/'CMD', 'Creature — Demon Spirit').
card_original_text('oni of wild places'/'CMD', 'Haste\nAt the beginning of your upkeep, return a red creature you control to its owner\'s hand.').
card_image_name('oni of wild places'/'CMD', 'oni of wild places').
card_uid('oni of wild places'/'CMD', 'CMD:Oni of Wild Places:oni of wild places').
card_rarity('oni of wild places'/'CMD', 'Uncommon').
card_artist('oni of wild places'/'CMD', 'Mark Tedin').
card_number('oni of wild places'/'CMD', '130').
card_flavor_text('oni of wild places'/'CMD', 'The oni leapt easily from peak to peak, toying with its victims, its voice a purr from the rumbling depths of nightmare.').
card_multiverse_id('oni of wild places'/'CMD', '247411').

card_in_set('orim\'s thunder', 'CMD').
card_original_type('orim\'s thunder'/'CMD', 'Instant').
card_original_text('orim\'s thunder'/'CMD', 'Kicker {R} (You may pay an additional {R} as you cast this spell.)\nDestroy target artifact or enchantment. If Orim\'s Thunder was kicked, it deals damage equal to that permanent\'s converted mana cost to target creature.').
card_image_name('orim\'s thunder'/'CMD', 'orim\'s thunder').
card_uid('orim\'s thunder'/'CMD', 'CMD:Orim\'s Thunder:orim\'s thunder').
card_rarity('orim\'s thunder'/'CMD', 'Common').
card_artist('orim\'s thunder'/'CMD', 'Carl Critchlow').
card_number('orim\'s thunder'/'CMD', '24').
card_multiverse_id('orim\'s thunder'/'CMD', '247161').

card_in_set('oros, the avenger', 'CMD').
card_original_type('oros, the avenger'/'CMD', 'Legendary Creature — Dragon').
card_original_text('oros, the avenger'/'CMD', 'Flying\nWhenever Oros, the Avenger deals combat damage to a player, you may pay {2}{W}. If you do, Oros deals 3 damage to each nonwhite creature.').
card_image_name('oros, the avenger'/'CMD', 'oros, the avenger').
card_uid('oros, the avenger'/'CMD', 'CMD:Oros, the Avenger:oros, the avenger').
card_rarity('oros, the avenger'/'CMD', 'Rare').
card_artist('oros, the avenger'/'CMD', 'Daren Bader').
card_number('oros, the avenger'/'CMD', '216').
card_multiverse_id('oros, the avenger'/'CMD', '247363').

card_in_set('orzhov basilica', 'CMD').
card_original_type('orzhov basilica'/'CMD', 'Land').
card_original_text('orzhov basilica'/'CMD', 'Orzhov Basilica enters the battlefield tapped.\nWhen Orzhov Basilica enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {W}{B} to your mana pool.').
card_image_name('orzhov basilica'/'CMD', 'orzhov basilica').
card_uid('orzhov basilica'/'CMD', 'CMD:Orzhov Basilica:orzhov basilica').
card_rarity('orzhov basilica'/'CMD', 'Common').
card_artist('orzhov basilica'/'CMD', 'John Avon').
card_number('orzhov basilica'/'CMD', '283').
card_multiverse_id('orzhov basilica'/'CMD', '247283').

card_in_set('orzhov guildmage', 'CMD').
card_original_type('orzhov guildmage'/'CMD', 'Creature — Human Wizard').
card_original_text('orzhov guildmage'/'CMD', '{2}{W}: Target player gains 1 life.\n{2}{B}: Each player loses 1 life.').
card_image_name('orzhov guildmage'/'CMD', 'orzhov guildmage').
card_uid('orzhov guildmage'/'CMD', 'CMD:Orzhov Guildmage:orzhov guildmage').
card_rarity('orzhov guildmage'/'CMD', 'Uncommon').
card_artist('orzhov guildmage'/'CMD', 'Greg Staples').
card_number('orzhov guildmage'/'CMD', '217').
card_multiverse_id('orzhov guildmage'/'CMD', '247284').

card_in_set('orzhov signet', 'CMD').
card_original_type('orzhov signet'/'CMD', 'Artifact').
card_original_text('orzhov signet'/'CMD', '{1}, {T}: Add {W}{B} to your mana pool.').
card_image_name('orzhov signet'/'CMD', 'orzhov signet').
card_uid('orzhov signet'/'CMD', 'CMD:Orzhov Signet:orzhov signet').
card_rarity('orzhov signet'/'CMD', 'Common').
card_artist('orzhov signet'/'CMD', 'Greg Hildebrandt').
card_number('orzhov signet'/'CMD', '255').
card_flavor_text('orzhov signet'/'CMD', 'The form of the sigil is just as important as the sigil itself. If it\'s carried on a medallion, its bearer is a master. If it\'s tattooed on the body, its bearer is a slave.').
card_multiverse_id('orzhov signet'/'CMD', '247285').

card_in_set('path to exile', 'CMD').
card_original_type('path to exile'/'CMD', 'Instant').
card_original_text('path to exile'/'CMD', 'Exile target creature. Its controller may search his or her library for a basic land card, put that card onto the battlefield tapped, then shuffle his or her library.').
card_image_name('path to exile'/'CMD', 'path to exile').
card_uid('path to exile'/'CMD', 'CMD:Path to Exile:path to exile').
card_rarity('path to exile'/'CMD', 'Uncommon').
card_artist('path to exile'/'CMD', 'Todd Lockwood').
card_number('path to exile'/'CMD', '25').
card_multiverse_id('path to exile'/'CMD', '247180').

card_in_set('patron of the nezumi', 'CMD').
card_original_type('patron of the nezumi'/'CMD', 'Legendary Creature — Spirit').
card_original_text('patron of the nezumi'/'CMD', 'Rat offering (You may cast this card any time you could cast an instant by sacrificing a Rat and paying the difference in mana costs between this and the sacrificed Rat. Mana cost includes color.)\nWhenever a permanent is put into an opponent\'s graveyard, that player loses 1 life.').
card_image_name('patron of the nezumi'/'CMD', 'patron of the nezumi').
card_uid('patron of the nezumi'/'CMD', 'CMD:Patron of the Nezumi:patron of the nezumi').
card_rarity('patron of the nezumi'/'CMD', 'Rare').
card_artist('patron of the nezumi'/'CMD', 'Kev Walker').
card_number('patron of the nezumi'/'CMD', '93').
card_multiverse_id('patron of the nezumi'/'CMD', '247171').

card_in_set('penumbra spider', 'CMD').
card_original_type('penumbra spider'/'CMD', 'Creature — Spider').
card_original_text('penumbra spider'/'CMD', 'Reach (This creature can block creatures with flying.)\nWhen Penumbra Spider is put into a graveyard from the battlefield, put a 2/4 black Spider creature token with reach onto the battlefield.').
card_image_name('penumbra spider'/'CMD', 'penumbra spider').
card_uid('penumbra spider'/'CMD', 'CMD:Penumbra Spider:penumbra spider').
card_rarity('penumbra spider'/'CMD', 'Common').
card_artist('penumbra spider'/'CMD', 'Jeff Easley').
card_number('penumbra spider'/'CMD', '167').
card_multiverse_id('penumbra spider'/'CMD', '247516').

card_in_set('perilous research', 'CMD').
card_original_type('perilous research'/'CMD', 'Instant').
card_original_text('perilous research'/'CMD', 'Draw two cards, then sacrifice a permanent.').
card_image_name('perilous research'/'CMD', 'perilous research').
card_uid('perilous research'/'CMD', 'CMD:Perilous Research:perilous research').
card_rarity('perilous research'/'CMD', 'Uncommon').
card_artist('perilous research'/'CMD', 'Dany Orizio').
card_number('perilous research'/'CMD', '54').
card_flavor_text('perilous research'/'CMD', 'When the School of the Unseen fell, so many magical treasures lay abandoned that no amount of death could deter the stream of thieves and desperate scholars.').
card_multiverse_id('perilous research'/'CMD', '247183').

card_in_set('plains', 'CMD').
card_original_type('plains'/'CMD', 'Basic Land — Plains').
card_original_text('plains'/'CMD', 'W').
card_image_name('plains'/'CMD', 'plains1').
card_uid('plains'/'CMD', 'CMD:Plains:plains1').
card_rarity('plains'/'CMD', 'Basic Land').
card_artist('plains'/'CMD', 'John Avon').
card_number('plains'/'CMD', '299').
card_multiverse_id('plains'/'CMD', '249814').

card_in_set('plains', 'CMD').
card_original_type('plains'/'CMD', 'Basic Land — Plains').
card_original_text('plains'/'CMD', 'W').
card_image_name('plains'/'CMD', 'plains2').
card_uid('plains'/'CMD', 'CMD:Plains:plains2').
card_rarity('plains'/'CMD', 'Basic Land').
card_artist('plains'/'CMD', 'John Avon').
card_number('plains'/'CMD', '300').
card_multiverse_id('plains'/'CMD', '249813').

card_in_set('plains', 'CMD').
card_original_type('plains'/'CMD', 'Basic Land — Plains').
card_original_text('plains'/'CMD', 'W').
card_image_name('plains'/'CMD', 'plains3').
card_uid('plains'/'CMD', 'CMD:Plains:plains3').
card_rarity('plains'/'CMD', 'Basic Land').
card_artist('plains'/'CMD', 'Michael Komarck').
card_number('plains'/'CMD', '301').
card_multiverse_id('plains'/'CMD', '249815').

card_in_set('plains', 'CMD').
card_original_type('plains'/'CMD', 'Basic Land — Plains').
card_original_text('plains'/'CMD', 'W').
card_image_name('plains'/'CMD', 'plains4').
card_uid('plains'/'CMD', 'CMD:Plains:plains4').
card_rarity('plains'/'CMD', 'Basic Land').
card_artist('plains'/'CMD', 'Matthew Mitchell').
card_number('plains'/'CMD', '302').
card_multiverse_id('plains'/'CMD', '249812').

card_in_set('plumeveil', 'CMD').
card_original_type('plumeveil'/'CMD', 'Creature — Elemental').
card_original_text('plumeveil'/'CMD', 'Flash (You may cast this spell any time you could cast an instant.)\nDefender, flying').
card_image_name('plumeveil'/'CMD', 'plumeveil').
card_uid('plumeveil'/'CMD', 'CMD:Plumeveil:plumeveil').
card_rarity('plumeveil'/'CMD', 'Uncommon').
card_artist('plumeveil'/'CMD', 'Nils Hamm').
card_number('plumeveil'/'CMD', '218').
card_flavor_text('plumeveil'/'CMD', '\"It was vast, a great sheet of soaring wings, and equally silent. It caught us unawares and blocked our view of the kithkin stronghold.\"\n—Grensch, merrow cutthroat').
card_multiverse_id('plumeveil'/'CMD', '247408').

card_in_set('pollen lullaby', 'CMD').
card_original_type('pollen lullaby'/'CMD', 'Instant').
card_original_text('pollen lullaby'/'CMD', 'Prevent all combat damage that would be dealt this turn. Clash with an opponent. If you win, creatures that player controls don\'t untap during the player\'s next untap step. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_image_name('pollen lullaby'/'CMD', 'pollen lullaby').
card_uid('pollen lullaby'/'CMD', 'CMD:Pollen Lullaby:pollen lullaby').
card_rarity('pollen lullaby'/'CMD', 'Uncommon').
card_artist('pollen lullaby'/'CMD', 'Warren Mahy').
card_number('pollen lullaby'/'CMD', '26').
card_multiverse_id('pollen lullaby'/'CMD', '247303').

card_in_set('prison term', 'CMD').
card_original_type('prison term'/'CMD', 'Enchantment — Aura').
card_original_text('prison term'/'CMD', 'Enchant creature\nEnchanted creature can\'t attack or block, and its activated abilities can\'t be activated.\nWhenever a creature enters the battlefield under an opponent\'s control, you may attach Prison Term to that creature.').
card_image_name('prison term'/'CMD', 'prison term').
card_uid('prison term'/'CMD', 'CMD:Prison Term:prison term').
card_rarity('prison term'/'CMD', 'Uncommon').
card_artist('prison term'/'CMD', 'Zoltan Boros & Gabor Szikszai').
card_number('prison term'/'CMD', '27').
card_multiverse_id('prison term'/'CMD', '247409').

card_in_set('propaganda', 'CMD').
card_original_type('propaganda'/'CMD', 'Enchantment').
card_original_text('propaganda'/'CMD', 'Creatures can\'t attack you unless their controller pays {2} for each creature he or she controls that\'s attacking you.').
card_image_name('propaganda'/'CMD', 'propaganda').
card_uid('propaganda'/'CMD', 'CMD:Propaganda:propaganda').
card_rarity('propaganda'/'CMD', 'Uncommon').
card_artist('propaganda'/'CMD', 'Jeff Miracola').
card_number('propaganda'/'CMD', '55').
card_flavor_text('propaganda'/'CMD', '\"You\'ve failed Gerrard. You\'ve failed the Legacy. You\'ve failed yourself. I can do no more.\"\n—Volrath, to Karn').
card_multiverse_id('propaganda'/'CMD', '247417').

card_in_set('prophetic bolt', 'CMD').
card_original_type('prophetic bolt'/'CMD', 'Instant').
card_original_text('prophetic bolt'/'CMD', 'Prophetic Bolt deals 4 damage to target creature or player. Look at the top four cards of your library. Put one of those cards into your hand and the rest on the bottom of your library in any order.').
card_image_name('prophetic bolt'/'CMD', 'prophetic bolt').
card_uid('prophetic bolt'/'CMD', 'CMD:Prophetic Bolt:prophetic bolt').
card_rarity('prophetic bolt'/'CMD', 'Rare').
card_artist('prophetic bolt'/'CMD', 'Dave Dorman').
card_number('prophetic bolt'/'CMD', '219').
card_multiverse_id('prophetic bolt'/'CMD', '247162').

card_in_set('prophetic prism', 'CMD').
card_original_type('prophetic prism'/'CMD', 'Artifact').
card_original_text('prophetic prism'/'CMD', 'When Prophetic Prism enters the battlefield, draw a card.\n{1}, {T}: Add one mana of any color to your mana pool.').
card_image_name('prophetic prism'/'CMD', 'prophetic prism').
card_uid('prophetic prism'/'CMD', 'CMD:Prophetic Prism:prophetic prism').
card_rarity('prophetic prism'/'CMD', 'Common').
card_artist('prophetic prism'/'CMD', 'John Avon').
card_number('prophetic prism'/'CMD', '256').
card_flavor_text('prophetic prism'/'CMD', '\"The explorer who found it got a bad deal when he sold it. He merely got rich.\"').
card_multiverse_id('prophetic prism'/'CMD', '247398').

card_in_set('punishing fire', 'CMD').
card_original_type('punishing fire'/'CMD', 'Instant').
card_original_text('punishing fire'/'CMD', 'Punishing Fire deals 2 damage to target creature or player.\nWhenever an opponent gains life, you may pay {R}. If you do, return Punishing Fire from your graveyard to your hand.').
card_image_name('punishing fire'/'CMD', 'punishing fire').
card_uid('punishing fire'/'CMD', 'CMD:Punishing Fire:punishing fire').
card_rarity('punishing fire'/'CMD', 'Uncommon').
card_artist('punishing fire'/'CMD', 'Christopher Moeller').
card_number('punishing fire'/'CMD', '131').
card_multiverse_id('punishing fire'/'CMD', '247550').

card_in_set('pyrohemia', 'CMD').
card_original_type('pyrohemia'/'CMD', 'Enchantment').
card_original_text('pyrohemia'/'CMD', 'At the beginning of the end step, if no creatures are on the battlefield, sacrifice Pyrohemia.\n{R}: Pyrohemia deals 1 damage to each creature and each player.').
card_image_name('pyrohemia'/'CMD', 'pyrohemia').
card_uid('pyrohemia'/'CMD', 'CMD:Pyrohemia:pyrohemia').
card_rarity('pyrohemia'/'CMD', 'Uncommon').
card_artist('pyrohemia'/'CMD', 'Stephen Tappin').
card_number('pyrohemia'/'CMD', '132').
card_multiverse_id('pyrohemia'/'CMD', '247364').

card_in_set('rakdos carnarium', 'CMD').
card_original_type('rakdos carnarium'/'CMD', 'Land').
card_original_text('rakdos carnarium'/'CMD', 'Rakdos Carnarium enters the battlefield tapped.\nWhen Rakdos Carnarium enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {B}{R} to your mana pool.').
card_image_name('rakdos carnarium'/'CMD', 'rakdos carnarium').
card_uid('rakdos carnarium'/'CMD', 'CMD:Rakdos Carnarium:rakdos carnarium').
card_rarity('rakdos carnarium'/'CMD', 'Common').
card_artist('rakdos carnarium'/'CMD', 'John Avon').
card_number('rakdos carnarium'/'CMD', '284').
card_multiverse_id('rakdos carnarium'/'CMD', '247192').

card_in_set('rakdos signet', 'CMD').
card_original_type('rakdos signet'/'CMD', 'Artifact').
card_original_text('rakdos signet'/'CMD', '{1}, {T}: Add {B}{R} to your mana pool.').
card_image_name('rakdos signet'/'CMD', 'rakdos signet').
card_uid('rakdos signet'/'CMD', 'CMD:Rakdos Signet:rakdos signet').
card_rarity('rakdos signet'/'CMD', 'Common').
card_artist('rakdos signet'/'CMD', 'Greg Hildebrandt').
card_number('rakdos signet'/'CMD', '257').
card_flavor_text('rakdos signet'/'CMD', 'Made of bone and boiled in blood, a Rakdos signet is not considered finished until it has been used as a murder weapon.').
card_multiverse_id('rakdos signet'/'CMD', '247193').

card_in_set('rapacious one', 'CMD').
card_original_type('rapacious one'/'CMD', 'Creature — Eldrazi Drone').
card_original_text('rapacious one'/'CMD', 'Trample\nWhenever Rapacious One deals combat damage to a player, put that many 0/1 colorless Eldrazi Spawn creature tokens onto the battlefield. They have \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_image_name('rapacious one'/'CMD', 'rapacious one').
card_uid('rapacious one'/'CMD', 'CMD:Rapacious One:rapacious one').
card_rarity('rapacious one'/'CMD', 'Uncommon').
card_artist('rapacious one'/'CMD', 'Jason A. Engle').
card_number('rapacious one'/'CMD', '133').
card_multiverse_id('rapacious one'/'CMD', '247399').

card_in_set('ray of command', 'CMD').
card_original_type('ray of command'/'CMD', 'Instant').
card_original_text('ray of command'/'CMD', 'Untap target creature an opponent controls and gain control of it until end of turn. That creature gains haste until end of turn. When you lose control of the creature, tap it.').
card_image_name('ray of command'/'CMD', 'ray of command').
card_uid('ray of command'/'CMD', 'CMD:Ray of Command:ray of command').
card_rarity('ray of command'/'CMD', 'Common').
card_artist('ray of command'/'CMD', 'Andrew Robinson').
card_number('ray of command'/'CMD', '56').
card_flavor_text('ray of command'/'CMD', '\"Heel.\"').
card_multiverse_id('ray of command'/'CMD', '247329').

card_in_set('razorjaw oni', 'CMD').
card_original_type('razorjaw oni'/'CMD', 'Creature — Demon Spirit').
card_original_text('razorjaw oni'/'CMD', 'Black creatures can\'t block.').
card_image_name('razorjaw oni'/'CMD', 'razorjaw oni').
card_uid('razorjaw oni'/'CMD', 'CMD:Razorjaw Oni:razorjaw oni').
card_rarity('razorjaw oni'/'CMD', 'Uncommon').
card_artist('razorjaw oni'/'CMD', 'Carl Critchlow').
card_number('razorjaw oni'/'CMD', '94').
card_flavor_text('razorjaw oni'/'CMD', 'It gnashed its teeth, and the battlefield filled with the screech of blade striking blade.').
card_multiverse_id('razorjaw oni'/'CMD', '247412').

card_in_set('reins of power', 'CMD').
card_original_type('reins of power'/'CMD', 'Instant').
card_original_text('reins of power'/'CMD', 'Untap all creatures you control and all creatures target opponent controls. You and that opponent each gain control of all creatures the other controls until end of turn. Those creatures gain haste until end of turn.').
card_image_name('reins of power'/'CMD', 'reins of power').
card_uid('reins of power'/'CMD', 'CMD:Reins of Power:reins of power').
card_rarity('reins of power'/'CMD', 'Rare').
card_artist('reins of power'/'CMD', 'Colin MacNeil').
card_number('reins of power'/'CMD', '57').
card_multiverse_id('reins of power'/'CMD', '247413').

card_in_set('reiver demon', 'CMD').
card_original_type('reiver demon'/'CMD', 'Creature — Demon').
card_original_text('reiver demon'/'CMD', 'Flying\nWhen Reiver Demon enters the battlefield, if you cast it from your hand, destroy all nonartifact, nonblack creatures. They can\'t be regenerated.').
card_image_name('reiver demon'/'CMD', 'reiver demon').
card_uid('reiver demon'/'CMD', 'CMD:Reiver Demon:reiver demon').
card_rarity('reiver demon'/'CMD', 'Rare').
card_artist('reiver demon'/'CMD', 'Brom').
card_number('reiver demon'/'CMD', '95').
card_multiverse_id('reiver demon'/'CMD', '247339').

card_in_set('relic crush', 'CMD').
card_original_type('relic crush'/'CMD', 'Instant').
card_original_text('relic crush'/'CMD', 'Destroy target artifact or enchantment and up to one other target artifact or enchantment.').
card_image_name('relic crush'/'CMD', 'relic crush').
card_uid('relic crush'/'CMD', 'CMD:Relic Crush:relic crush').
card_rarity('relic crush'/'CMD', 'Common').
card_artist('relic crush'/'CMD', 'Steven Belledin').
card_number('relic crush'/'CMD', '168').
card_flavor_text('relic crush'/'CMD', 'There are many ruins, but there used to be many more.').
card_multiverse_id('relic crush'/'CMD', '247551').

card_in_set('repulse', 'CMD').
card_original_type('repulse'/'CMD', 'Instant').
card_original_text('repulse'/'CMD', 'Return target creature to its owner\'s hand.\nDraw a card.').
card_image_name('repulse'/'CMD', 'repulse').
card_uid('repulse'/'CMD', 'CMD:Repulse:repulse').
card_rarity('repulse'/'CMD', 'Common').
card_artist('repulse'/'CMD', 'Aaron Boyd').
card_number('repulse'/'CMD', '58').
card_flavor_text('repulse'/'CMD', '\"You aren\'t invited.\"').
card_multiverse_id('repulse'/'CMD', '247187').

card_in_set('return to dust', 'CMD').
card_original_type('return to dust'/'CMD', 'Instant').
card_original_text('return to dust'/'CMD', 'Exile target artifact or enchantment. If you cast this spell during your main phase, you may exile up to one other target artifact or enchantment.').
card_image_name('return to dust'/'CMD', 'return to dust').
card_uid('return to dust'/'CMD', 'CMD:Return to Dust:return to dust').
card_rarity('return to dust'/'CMD', 'Uncommon').
card_artist('return to dust'/'CMD', 'Wayne Reynolds').
card_number('return to dust'/'CMD', '28').
card_flavor_text('return to dust'/'CMD', 'Some timelines forever fray, branch, and intermingle. Others end abruptly.').
card_multiverse_id('return to dust'/'CMD', '247517').

card_in_set('riddlekeeper', 'CMD').
card_original_type('riddlekeeper'/'CMD', 'Creature — Homunculus').
card_original_text('riddlekeeper'/'CMD', 'Whenever a creature attacks you or a planeswalker you control, that creature\'s controller puts the top two cards of his or her library into his or her graveyard.').
card_first_print('riddlekeeper', 'CMD').
card_image_name('riddlekeeper'/'CMD', 'riddlekeeper').
card_uid('riddlekeeper'/'CMD', 'CMD:Riddlekeeper:riddlekeeper').
card_rarity('riddlekeeper'/'CMD', 'Rare').
card_artist('riddlekeeper'/'CMD', 'Steve Prescott').
card_number('riddlekeeper'/'CMD', '59').
card_flavor_text('riddlekeeper'/'CMD', 'Fools surrender their sanity to his riddles. Even greater fools try to solve them.').
card_multiverse_id('riddlekeeper'/'CMD', '238138').

card_in_set('righteous cause', 'CMD').
card_original_type('righteous cause'/'CMD', 'Enchantment').
card_original_text('righteous cause'/'CMD', 'Whenever a creature attacks, you gain 1 life.').
card_image_name('righteous cause'/'CMD', 'righteous cause').
card_uid('righteous cause'/'CMD', 'CMD:Righteous Cause:righteous cause').
card_rarity('righteous cause'/'CMD', 'Uncommon').
card_artist('righteous cause'/'CMD', 'Scott M. Fischer').
card_number('righteous cause'/'CMD', '29').
card_flavor_text('righteous cause'/'CMD', '\"Until the world unites in vengeful fury and Phage is destroyed, I will not stay my hand.\"\n—Akroma, angelic avenger').
card_multiverse_id('righteous cause'/'CMD', '247351').

card_in_set('riku of two reflections', 'CMD').
card_original_type('riku of two reflections'/'CMD', 'Legendary Creature — Human Wizard').
card_original_text('riku of two reflections'/'CMD', 'Whenever you cast an instant or sorcery spell, you may pay {U}{R}. If you do, copy that spell. You may choose new targets for the copy.\nWhenever another nontoken creature enters the battlefield under your control, you may pay {G}{U}. If you do, put a token that\'s a copy of that creature onto the battlefield.').
card_image_name('riku of two reflections'/'CMD', 'riku of two reflections').
card_uid('riku of two reflections'/'CMD', 'CMD:Riku of Two Reflections:riku of two reflections').
card_rarity('riku of two reflections'/'CMD', 'Mythic Rare').
card_artist('riku of two reflections'/'CMD', 'Izzy').
card_number('riku of two reflections'/'CMD', '220').
card_multiverse_id('riku of two reflections'/'CMD', '236469').

card_in_set('rise from the grave', 'CMD').
card_original_type('rise from the grave'/'CMD', 'Sorcery').
card_original_text('rise from the grave'/'CMD', 'Put target creature card in a graveyard onto the battlefield under your control. That creature is a black Zombie in addition to its other colors and types.').
card_image_name('rise from the grave'/'CMD', 'rise from the grave').
card_uid('rise from the grave'/'CMD', 'CMD:Rise from the Grave:rise from the grave').
card_rarity('rise from the grave'/'CMD', 'Uncommon').
card_artist('rise from the grave'/'CMD', 'Vance Kovacs').
card_number('rise from the grave'/'CMD', '96').
card_flavor_text('rise from the grave'/'CMD', '\"Death is no excuse for disobedience.\"\n—Liliana Vess').
card_multiverse_id('rise from the grave'/'CMD', '247325').

card_in_set('ruhan of the fomori', 'CMD').
card_original_type('ruhan of the fomori'/'CMD', 'Legendary Creature — Giant Warrior').
card_original_text('ruhan of the fomori'/'CMD', 'At the beginning of combat on your turn, choose an opponent at random. Ruhan of the Fomori attacks that player this combat if able.').
card_first_print('ruhan of the fomori', 'CMD').
card_image_name('ruhan of the fomori'/'CMD', 'ruhan of the fomori').
card_uid('ruhan of the fomori'/'CMD', 'CMD:Ruhan of the Fomori:ruhan of the fomori').
card_rarity('ruhan of the fomori'/'CMD', 'Mythic Rare').
card_artist('ruhan of the fomori'/'CMD', 'Raymond Swanland').
card_number('ruhan of the fomori'/'CMD', '221').
card_flavor_text('ruhan of the fomori'/'CMD', 'Unfettered by allegiance, driven by war.').
card_multiverse_id('ruhan of the fomori'/'CMD', '236477').

card_in_set('ruination', 'CMD').
card_original_type('ruination'/'CMD', 'Sorcery').
card_original_text('ruination'/'CMD', 'Destroy all nonbasic lands.').
card_image_name('ruination'/'CMD', 'ruination').
card_uid('ruination'/'CMD', 'CMD:Ruination:ruination').
card_rarity('ruination'/'CMD', 'Rare').
card_artist('ruination'/'CMD', 'Dermot Power').
card_number('ruination'/'CMD', '134').
card_flavor_text('ruination'/'CMD', '\"We have built a wall upon sand. The wall will vanish. The sand will remain.\"\n—Oracle en-Vec').
card_multiverse_id('ruination'/'CMD', '247414').

card_in_set('rupture spire', 'CMD').
card_original_type('rupture spire'/'CMD', 'Land').
card_original_text('rupture spire'/'CMD', 'Rupture Spire enters the battlefield tapped.\nWhen Rupture Spire enters the battlefield, sacrifice it unless you pay {1}.\n{T}: Add one mana of any color to your mana pool.').
card_image_name('rupture spire'/'CMD', 'rupture spire').
card_uid('rupture spire'/'CMD', 'CMD:Rupture Spire:rupture spire').
card_rarity('rupture spire'/'CMD', 'Common').
card_artist('rupture spire'/'CMD', 'Jaime Jones').
card_number('rupture spire'/'CMD', '285').
card_multiverse_id('rupture spire'/'CMD', '247181').

card_in_set('sakura-tribe elder', 'CMD').
card_original_type('sakura-tribe elder'/'CMD', 'Creature — Snake Shaman').
card_original_text('sakura-tribe elder'/'CMD', 'Sacrifice Sakura-Tribe Elder: Search your library for a basic land card, put that card onto the battlefield tapped, then shuffle your library.').
card_image_name('sakura-tribe elder'/'CMD', 'sakura-tribe elder').
card_uid('sakura-tribe elder'/'CMD', 'CMD:Sakura-Tribe Elder:sakura-tribe elder').
card_rarity('sakura-tribe elder'/'CMD', 'Common').
card_artist('sakura-tribe elder'/'CMD', 'Carl Critchlow').
card_number('sakura-tribe elder'/'CMD', '169').
card_flavor_text('sakura-tribe elder'/'CMD', 'Slain warriors were buried with a tree sapling, so they would become a part of the forest after death.').
card_multiverse_id('sakura-tribe elder'/'CMD', '247176').

card_in_set('savage twister', 'CMD').
card_original_type('savage twister'/'CMD', 'Sorcery').
card_original_text('savage twister'/'CMD', 'Savage Twister deals X damage to each creature.').
card_image_name('savage twister'/'CMD', 'savage twister').
card_uid('savage twister'/'CMD', 'CMD:Savage Twister:savage twister').
card_rarity('savage twister'/'CMD', 'Uncommon').
card_artist('savage twister'/'CMD', 'Bob Eggleton').
card_number('savage twister'/'CMD', '222').
card_flavor_text('savage twister'/'CMD', '\"Frozen, we watched the funnel pluck up three of the goats—pook! pook! pook!—before we ran for the wadi.\"\n—Travelogue of Najat').
card_multiverse_id('savage twister'/'CMD', '247286').

card_in_set('scattering stroke', 'CMD').
card_original_type('scattering stroke'/'CMD', 'Instant').
card_original_text('scattering stroke'/'CMD', 'Counter target spell. Clash with an opponent. If you win, at the beginning of your next main phase, you may add {X} to your mana pool, where X is that spell\'s converted mana cost. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_image_name('scattering stroke'/'CMD', 'scattering stroke').
card_uid('scattering stroke'/'CMD', 'CMD:Scattering Stroke:scattering stroke').
card_rarity('scattering stroke'/'CMD', 'Uncommon').
card_artist('scattering stroke'/'CMD', 'Franz Vohwinkel').
card_number('scattering stroke'/'CMD', '60').
card_multiverse_id('scattering stroke'/'CMD', '247304').

card_in_set('scavenging ooze', 'CMD').
card_original_type('scavenging ooze'/'CMD', 'Creature — Ooze').
card_original_text('scavenging ooze'/'CMD', '{G}: Exile target card from a graveyard. If it was a creature card, put a +1/+1 counter on Scavenging Ooze and you gain 1 life.').
card_image_name('scavenging ooze'/'CMD', 'scavenging ooze').
card_uid('scavenging ooze'/'CMD', 'CMD:Scavenging Ooze:scavenging ooze').
card_rarity('scavenging ooze'/'CMD', 'Rare').
card_artist('scavenging ooze'/'CMD', 'Austin Hsu').
card_number('scavenging ooze'/'CMD', '170').
card_flavor_text('scavenging ooze'/'CMD', 'In nature, not a single bone or scrap of flesh goes to waste.').
card_multiverse_id('scavenging ooze'/'CMD', '233181').

card_in_set('scythe specter', 'CMD').
card_original_type('scythe specter'/'CMD', 'Creature — Specter').
card_original_text('scythe specter'/'CMD', 'Flying\nWhenever Scythe Specter deals combat damage to a player, each opponent discards a card. Each player who discarded a card with the highest converted mana cost among cards discarded this way loses life equal to that converted mana cost.').
card_first_print('scythe specter', 'CMD').
card_image_name('scythe specter'/'CMD', 'scythe specter').
card_uid('scythe specter'/'CMD', 'CMD:Scythe Specter:scythe specter').
card_rarity('scythe specter'/'CMD', 'Rare').
card_artist('scythe specter'/'CMD', 'Vincent Proce').
card_number('scythe specter'/'CMD', '97').
card_multiverse_id('scythe specter'/'CMD', '236500').

card_in_set('secluded steppe', 'CMD').
card_original_type('secluded steppe'/'CMD', 'Land').
card_original_text('secluded steppe'/'CMD', 'Secluded Steppe enters the battlefield tapped.\n{T}: Add {W} to your mana pool.\nCycling {W} ({W}, Discard this card: Draw a card.)').
card_image_name('secluded steppe'/'CMD', 'secluded steppe').
card_uid('secluded steppe'/'CMD', 'CMD:Secluded Steppe:secluded steppe').
card_rarity('secluded steppe'/'CMD', 'Common').
card_artist('secluded steppe'/'CMD', 'Heather Hudson').
card_number('secluded steppe'/'CMD', '286').
card_multiverse_id('secluded steppe'/'CMD', '247352').

card_in_set('selesnya evangel', 'CMD').
card_original_type('selesnya evangel'/'CMD', 'Creature — Elf Shaman').
card_original_text('selesnya evangel'/'CMD', '{1}, {T}, Tap an untapped creature you control: Put a 1/1 green Saproling creature token onto the battlefield.').
card_image_name('selesnya evangel'/'CMD', 'selesnya evangel').
card_uid('selesnya evangel'/'CMD', 'CMD:Selesnya Evangel:selesnya evangel').
card_rarity('selesnya evangel'/'CMD', 'Common').
card_artist('selesnya evangel'/'CMD', 'Rob Alexander').
card_number('selesnya evangel'/'CMD', '223').
card_flavor_text('selesnya evangel'/'CMD', '\"The clamor of the city drowns all voices. But together we can sing a harmony that will resonate from Ravnica\'s tallest spires to her deepest wells.\"').
card_multiverse_id('selesnya evangel'/'CMD', '247385').

card_in_set('selesnya guildmage', 'CMD').
card_original_type('selesnya guildmage'/'CMD', 'Creature — Elf Wizard').
card_original_text('selesnya guildmage'/'CMD', '{3}{G}: Put a 1/1 green Saproling creature token onto the battlefield.\n{3}{W}: Creatures you control get +1/+1 until end of turn.').
card_image_name('selesnya guildmage'/'CMD', 'selesnya guildmage').
card_uid('selesnya guildmage'/'CMD', 'CMD:Selesnya Guildmage:selesnya guildmage').
card_rarity('selesnya guildmage'/'CMD', 'Uncommon').
card_artist('selesnya guildmage'/'CMD', 'Mark Zug').
card_number('selesnya guildmage'/'CMD', '224').
card_multiverse_id('selesnya guildmage'/'CMD', '247386').

card_in_set('selesnya sanctuary', 'CMD').
card_original_type('selesnya sanctuary'/'CMD', 'Land').
card_original_text('selesnya sanctuary'/'CMD', 'Selesnya Sanctuary enters the battlefield tapped.\nWhen Selesnya Sanctuary enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {G}{W} to your mana pool.').
card_image_name('selesnya sanctuary'/'CMD', 'selesnya sanctuary').
card_uid('selesnya sanctuary'/'CMD', 'CMD:Selesnya Sanctuary:selesnya sanctuary').
card_rarity('selesnya sanctuary'/'CMD', 'Common').
card_artist('selesnya sanctuary'/'CMD', 'John Avon').
card_number('selesnya sanctuary'/'CMD', '287').
card_multiverse_id('selesnya sanctuary'/'CMD', '247387').

card_in_set('selesnya signet', 'CMD').
card_original_type('selesnya signet'/'CMD', 'Artifact').
card_original_text('selesnya signet'/'CMD', '{1}, {T}: Add {G}{W} to your mana pool.').
card_image_name('selesnya signet'/'CMD', 'selesnya signet').
card_uid('selesnya signet'/'CMD', 'CMD:Selesnya Signet:selesnya signet').
card_rarity('selesnya signet'/'CMD', 'Common').
card_artist('selesnya signet'/'CMD', 'Greg Hildebrandt').
card_number('selesnya signet'/'CMD', '258').
card_flavor_text('selesnya signet'/'CMD', 'The symbol of the Conclave is one of unity, with tree supporting sun and sun feeding tree.').
card_multiverse_id('selesnya signet'/'CMD', '247388').

card_in_set('serra angel', 'CMD').
card_original_type('serra angel'/'CMD', 'Creature — Angel').
card_original_text('serra angel'/'CMD', 'Flying, vigilance').
card_image_name('serra angel'/'CMD', 'serra angel').
card_uid('serra angel'/'CMD', 'CMD:Serra Angel:serra angel').
card_rarity('serra angel'/'CMD', 'Uncommon').
card_artist('serra angel'/'CMD', 'Greg Staples').
card_number('serra angel'/'CMD', '30').
card_flavor_text('serra angel'/'CMD', 'Perfect justice in service of a perfect will.').
card_multiverse_id('serra angel'/'CMD', '247326').

card_in_set('sewer nemesis', 'CMD').
card_original_type('sewer nemesis'/'CMD', 'Creature — Horror').
card_original_text('sewer nemesis'/'CMD', 'As Sewer Nemesis enters the battlefield, choose a player.\nSewer Nemesis\'s power and toughness are each equal to the number of cards in the chosen player\'s graveyard.\nWhenever the chosen player casts a spell, that player puts the top card of his or her library into his or her graveyard.').
card_first_print('sewer nemesis', 'CMD').
card_image_name('sewer nemesis'/'CMD', 'sewer nemesis').
card_uid('sewer nemesis'/'CMD', 'CMD:Sewer Nemesis:sewer nemesis').
card_rarity('sewer nemesis'/'CMD', 'Rare').
card_artist('sewer nemesis'/'CMD', 'Nils Hamm').
card_number('sewer nemesis'/'CMD', '98').
card_multiverse_id('sewer nemesis'/'CMD', '236482').

card_in_set('shared trauma', 'CMD').
card_original_type('shared trauma'/'CMD', 'Sorcery').
card_original_text('shared trauma'/'CMD', 'Join forces — Starting with you, each player may pay any amount of mana. Each player puts the top X cards of his or her library into his or her graveyard, where X is the total amount of mana paid this way.').
card_first_print('shared trauma', 'CMD').
card_image_name('shared trauma'/'CMD', 'shared trauma').
card_uid('shared trauma'/'CMD', 'CMD:Shared Trauma:shared trauma').
card_rarity('shared trauma'/'CMD', 'Rare').
card_artist('shared trauma'/'CMD', 'Clint Cearley').
card_number('shared trauma'/'CMD', '99').
card_multiverse_id('shared trauma'/'CMD', '228259').

card_in_set('shattered angel', 'CMD').
card_original_type('shattered angel'/'CMD', 'Creature — Angel').
card_original_text('shattered angel'/'CMD', 'Flying\nWhenever a land enters the battlefield under an opponent\'s control, you may gain 3 life.').
card_image_name('shattered angel'/'CMD', 'shattered angel').
card_uid('shattered angel'/'CMD', 'CMD:Shattered Angel:shattered angel').
card_rarity('shattered angel'/'CMD', 'Uncommon').
card_artist('shattered angel'/'CMD', 'Kev Walker').
card_number('shattered angel'/'CMD', '31').
card_flavor_text('shattered angel'/'CMD', 'She preaches the blessings of the Machine Orthodoxy.').
card_multiverse_id('shattered angel'/'CMD', '247341').

card_in_set('shriekmaw', 'CMD').
card_original_type('shriekmaw'/'CMD', 'Creature — Elemental').
card_original_text('shriekmaw'/'CMD', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\nWhen Shriekmaw enters the battlefield, destroy target nonartifact, nonblack creature.\nEvoke {1}{B} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_image_name('shriekmaw'/'CMD', 'shriekmaw').
card_uid('shriekmaw'/'CMD', 'CMD:Shriekmaw:shriekmaw').
card_rarity('shriekmaw'/'CMD', 'Uncommon').
card_artist('shriekmaw'/'CMD', 'Steve Prescott').
card_number('shriekmaw'/'CMD', '100').
card_multiverse_id('shriekmaw'/'CMD', '247305').

card_in_set('sigil captain', 'CMD').
card_original_type('sigil captain'/'CMD', 'Creature — Rhino Soldier').
card_original_text('sigil captain'/'CMD', 'Whenever a creature enters the battlefield under your control, if that creature is 1/1, put two +1/+1 counters on it.').
card_image_name('sigil captain'/'CMD', 'sigil captain').
card_uid('sigil captain'/'CMD', 'CMD:Sigil Captain:sigil captain').
card_rarity('sigil captain'/'CMD', 'Uncommon').
card_artist('sigil captain'/'CMD', 'Howard Lyon').
card_number('sigil captain'/'CMD', '225').
card_flavor_text('sigil captain'/'CMD', '\"One sigil awarded for valor, the other awarded for surviving Hellkite\'s Pass.\"').
card_multiverse_id('sigil captain'/'CMD', '247165').

card_in_set('sign in blood', 'CMD').
card_original_type('sign in blood'/'CMD', 'Sorcery').
card_original_text('sign in blood'/'CMD', 'Target player draws two cards and loses 2 life.').
card_image_name('sign in blood'/'CMD', 'sign in blood').
card_uid('sign in blood'/'CMD', 'CMD:Sign in Blood:sign in blood').
card_rarity('sign in blood'/'CMD', 'Common').
card_artist('sign in blood'/'CMD', 'Howard Lyon').
card_number('sign in blood'/'CMD', '101').
card_flavor_text('sign in blood'/'CMD', 'Little agonies pave the way to greater power.').
card_multiverse_id('sign in blood'/'CMD', '247327').

card_in_set('simic growth chamber', 'CMD').
card_original_type('simic growth chamber'/'CMD', 'Land').
card_original_text('simic growth chamber'/'CMD', 'Simic Growth Chamber enters the battlefield tapped.\nWhen Simic Growth Chamber enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {G}{U} to your mana pool.').
card_image_name('simic growth chamber'/'CMD', 'simic growth chamber').
card_uid('simic growth chamber'/'CMD', 'CMD:Simic Growth Chamber:simic growth chamber').
card_rarity('simic growth chamber'/'CMD', 'Common').
card_artist('simic growth chamber'/'CMD', 'John Avon').
card_number('simic growth chamber'/'CMD', '288').
card_multiverse_id('simic growth chamber'/'CMD', '247194').

card_in_set('simic signet', 'CMD').
card_original_type('simic signet'/'CMD', 'Artifact').
card_original_text('simic signet'/'CMD', '{1}, {T}: Add {G}{U} to your mana pool.').
card_image_name('simic signet'/'CMD', 'simic signet').
card_uid('simic signet'/'CMD', 'CMD:Simic Signet:simic signet').
card_rarity('simic signet'/'CMD', 'Common').
card_artist('simic signet'/'CMD', 'Greg Hildebrandt').
card_number('simic signet'/'CMD', '259').
card_flavor_text('simic signet'/'CMD', 'For the Simic Combine, its sigil serves not as an emblem of honor but as a trademark. Its familiar image on any biological commodity attests to superb craftsmanship, ingenious innovation, and higher cost.').
card_multiverse_id('simic signet'/'CMD', '247195').

card_in_set('simic sky swallower', 'CMD').
card_original_type('simic sky swallower'/'CMD', 'Creature — Leviathan').
card_original_text('simic sky swallower'/'CMD', 'Flying, trample\nShroud (This creature can\'t be the target of spells or abilities.)').
card_image_name('simic sky swallower'/'CMD', 'simic sky swallower').
card_uid('simic sky swallower'/'CMD', 'CMD:Simic Sky Swallower:simic sky swallower').
card_rarity('simic sky swallower'/'CMD', 'Rare').
card_artist('simic sky swallower'/'CMD', 'rk post').
card_number('simic sky swallower'/'CMD', '226').
card_flavor_text('simic sky swallower'/'CMD', '\"We\'ve bred out the shortcomings of the species\' natural form and replaced them with assets of our own design.\"\n—Momir Vig').
card_multiverse_id('simic sky swallower'/'CMD', '247196').

card_in_set('skullbriar, the walking grave', 'CMD').
card_original_type('skullbriar, the walking grave'/'CMD', 'Legendary Creature — Zombie Elemental').
card_original_text('skullbriar, the walking grave'/'CMD', 'Haste\nWhenever Skullbriar, the Walking Grave deals combat damage to a player, put a +1/+1 counter on it.\nCounters remain on Skullbriar as it moves to any zone other than a player\'s hand or library.').
card_first_print('skullbriar, the walking grave', 'CMD').
card_image_name('skullbriar, the walking grave'/'CMD', 'skullbriar, the walking grave').
card_uid('skullbriar, the walking grave'/'CMD', 'CMD:Skullbriar, the Walking Grave:skullbriar, the walking grave').
card_rarity('skullbriar, the walking grave'/'CMD', 'Rare').
card_artist('skullbriar, the walking grave'/'CMD', 'Nils Hamm').
card_number('skullbriar, the walking grave'/'CMD', '227').
card_multiverse_id('skullbriar, the walking grave'/'CMD', '236485').

card_in_set('skullclamp', 'CMD').
card_original_type('skullclamp'/'CMD', 'Artifact — Equipment').
card_original_text('skullclamp'/'CMD', 'Equipped creature gets +1/-1.\nWhen equipped creature is put into a graveyard, draw two cards.\nEquip {1}').
card_image_name('skullclamp'/'CMD', 'skullclamp').
card_uid('skullclamp'/'CMD', 'CMD:Skullclamp:skullclamp').
card_rarity('skullclamp'/'CMD', 'Uncommon').
card_artist('skullclamp'/'CMD', 'Luca Zontini').
card_number('skullclamp'/'CMD', '260').
card_flavor_text('skullclamp'/'CMD', 'The mind is a beautiful bounty encased in an annoying bone container.').
card_multiverse_id('skullclamp'/'CMD', '247201').

card_in_set('skyscribing', 'CMD').
card_original_type('skyscribing'/'CMD', 'Sorcery').
card_original_text('skyscribing'/'CMD', 'Each player draws X cards.\nForecast — {2}{U}, Reveal Skyscribing from your hand: Each player draws a card. (Activate this ability only during your upkeep and only once each turn.)').
card_image_name('skyscribing'/'CMD', 'skyscribing').
card_uid('skyscribing'/'CMD', 'CMD:Skyscribing:skyscribing').
card_rarity('skyscribing'/'CMD', 'Uncommon').
card_artist('skyscribing'/'CMD', 'Luca Zontini').
card_number('skyscribing'/'CMD', '61').
card_multiverse_id('skyscribing'/'CMD', '247197').

card_in_set('slipstream eel', 'CMD').
card_original_type('slipstream eel'/'CMD', 'Creature — Fish Beast').
card_original_text('slipstream eel'/'CMD', 'Slipstream Eel can\'t attack unless defending player controls an Island.\nCycling {1}{U} ({1}{U}, Discard this card: Draw a card.)').
card_image_name('slipstream eel'/'CMD', 'slipstream eel').
card_uid('slipstream eel'/'CMD', 'CMD:Slipstream Eel:slipstream eel').
card_rarity('slipstream eel'/'CMD', 'Common').
card_artist('slipstream eel'/'CMD', 'Mark Tedin').
card_number('slipstream eel'/'CMD', '62').
card_flavor_text('slipstream eel'/'CMD', '\"It\'s a fine way to travel, if you don\'t mind the smell.\"').
card_multiverse_id('slipstream eel'/'CMD', '247353').

card_in_set('sol ring', 'CMD').
card_original_type('sol ring'/'CMD', 'Artifact').
card_original_text('sol ring'/'CMD', '{T}: Add {2} to your mana pool.').
card_image_name('sol ring'/'CMD', 'sol ring').
card_uid('sol ring'/'CMD', 'CMD:Sol Ring:sol ring').
card_rarity('sol ring'/'CMD', 'Uncommon').
card_artist('sol ring'/'CMD', 'Mike Bierek').
card_number('sol ring'/'CMD', '261').
card_flavor_text('sol ring'/'CMD', 'Lost to time is the artificer\'s art of trapping light from a distant star in a ring of purest gold.').
card_multiverse_id('sol ring'/'CMD', '247533').

card_in_set('solemn simulacrum', 'CMD').
card_original_type('solemn simulacrum'/'CMD', 'Artifact Creature — Golem').
card_original_text('solemn simulacrum'/'CMD', 'When Solemn Simulacrum enters the battlefield, you may search your library for a basic land card and put that card onto the battlefield tapped. If you do, shuffle your library.\nWhen Solemn Simulacrum is put into a graveyard from the battlefield, you may draw a card.').
card_image_name('solemn simulacrum'/'CMD', 'solemn simulacrum').
card_uid('solemn simulacrum'/'CMD', 'CMD:Solemn Simulacrum:solemn simulacrum').
card_rarity('solemn simulacrum'/'CMD', 'Rare').
card_artist('solemn simulacrum'/'CMD', 'Greg Staples').
card_number('solemn simulacrum'/'CMD', '262').
card_multiverse_id('solemn simulacrum'/'CMD', '247340').

card_in_set('soul snare', 'CMD').
card_original_type('soul snare'/'CMD', 'Enchantment').
card_original_text('soul snare'/'CMD', '{W}, Sacrifice Soul Snare: Exile target creature that\'s attacking you or a planeswalker you control.').
card_first_print('soul snare', 'CMD').
card_image_name('soul snare'/'CMD', 'soul snare').
card_uid('soul snare'/'CMD', 'CMD:Soul Snare:soul snare').
card_rarity('soul snare'/'CMD', 'Uncommon').
card_artist('soul snare'/'CMD', 'Carl Critchlow').
card_number('soul snare'/'CMD', '32').
card_flavor_text('soul snare'/'CMD', '\"Body and spirit are sworn enemies. They will take any excuse to be rid of each other.\"\n—Tariel, Reckoner of Souls').
card_multiverse_id('soul snare'/'CMD', '238136').

card_in_set('spawnwrithe', 'CMD').
card_original_type('spawnwrithe'/'CMD', 'Creature — Elemental').
card_original_text('spawnwrithe'/'CMD', 'Trample\nWhenever Spawnwrithe deals combat damage to a player, put a token that\'s a copy of Spawnwrithe onto the battlefield.').
card_image_name('spawnwrithe'/'CMD', 'spawnwrithe').
card_uid('spawnwrithe'/'CMD', 'CMD:Spawnwrithe:spawnwrithe').
card_rarity('spawnwrithe'/'CMD', 'Rare').
card_artist('spawnwrithe'/'CMD', 'Daarken').
card_number('spawnwrithe'/'CMD', '171').
card_flavor_text('spawnwrithe'/'CMD', 'Its victims feel only an itchy, wriggling feeling just under their skin. By then, it\'s far too late.').
card_multiverse_id('spawnwrithe'/'CMD', '259016').

card_in_set('spell crumple', 'CMD').
card_original_type('spell crumple'/'CMD', 'Instant').
card_original_text('spell crumple'/'CMD', 'Counter target spell. If that spell is countered this way, put it on the bottom of its owner\'s library instead of into that player\'s graveyard. Put Spell Crumple on the bottom of its owner\'s library.').
card_first_print('spell crumple', 'CMD').
card_image_name('spell crumple'/'CMD', 'spell crumple').
card_uid('spell crumple'/'CMD', 'CMD:Spell Crumple:spell crumple').
card_rarity('spell crumple'/'CMD', 'Uncommon').
card_artist('spell crumple'/'CMD', 'Dan Scott').
card_number('spell crumple'/'CMD', '63').
card_multiverse_id('spell crumple'/'CMD', '237005').

card_in_set('spike feeder', 'CMD').
card_original_type('spike feeder'/'CMD', 'Creature — Spike').
card_original_text('spike feeder'/'CMD', 'Spike Feeder enters the battlefield with two +1/+1 counters on it.\n{2}, Remove a +1/+1 counter from Spike Feeder: Put a +1/+1 counter on target creature.\nRemove a +1/+1 counter from Spike Feeder: You gain 2 life.').
card_image_name('spike feeder'/'CMD', 'spike feeder').
card_uid('spike feeder'/'CMD', 'CMD:Spike Feeder:spike feeder').
card_rarity('spike feeder'/'CMD', 'Uncommon').
card_artist('spike feeder'/'CMD', 'Heather Hudson').
card_number('spike feeder'/'CMD', '172').
card_multiverse_id('spike feeder'/'CMD', '247521').

card_in_set('spitebellows', 'CMD').
card_original_type('spitebellows'/'CMD', 'Creature — Elemental').
card_original_text('spitebellows'/'CMD', 'When Spitebellows leaves the battlefield, it deals 6 damage to target creature.\nEvoke {1}{R}{R} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_image_name('spitebellows'/'CMD', 'spitebellows').
card_uid('spitebellows'/'CMD', 'CMD:Spitebellows:spitebellows').
card_rarity('spitebellows'/'CMD', 'Uncommon').
card_artist('spitebellows'/'CMD', 'Larry MacDougall').
card_number('spitebellows'/'CMD', '135').
card_flavor_text('spitebellows'/'CMD', 'Disaster stalks with gaping jaws across unready lands.').
card_multiverse_id('spitebellows'/'CMD', '247336').

card_in_set('spurnmage advocate', 'CMD').
card_original_type('spurnmage advocate'/'CMD', 'Creature — Human Nomad').
card_original_text('spurnmage advocate'/'CMD', '{T}: Return two target cards from an opponent\'s graveyard to his or her hand. Destroy target attacking creature.').
card_image_name('spurnmage advocate'/'CMD', 'spurnmage advocate').
card_uid('spurnmage advocate'/'CMD', 'CMD:Spurnmage Advocate:spurnmage advocate').
card_rarity('spurnmage advocate'/'CMD', 'Uncommon').
card_artist('spurnmage advocate'/'CMD', 'Ron Spears').
card_number('spurnmage advocate'/'CMD', '33').
card_flavor_text('spurnmage advocate'/'CMD', '\"Our unity humbles our foes.\"').
card_multiverse_id('spurnmage advocate'/'CMD', '247291').

card_in_set('squallmonger', 'CMD').
card_original_type('squallmonger'/'CMD', 'Creature — Monger').
card_original_text('squallmonger'/'CMD', '{2}: Squallmonger deals 1 damage to each creature with flying and each player. Any player may activate this ability.').
card_image_name('squallmonger'/'CMD', 'squallmonger').
card_uid('squallmonger'/'CMD', 'CMD:Squallmonger:squallmonger').
card_rarity('squallmonger'/'CMD', 'Uncommon').
card_artist('squallmonger'/'CMD', 'Heather Hudson').
card_number('squallmonger'/'CMD', '173').
card_flavor_text('squallmonger'/'CMD', '\"Squall, gale, hurricane—it\'s all a matter of size . . . and price.\"').
card_multiverse_id('squallmonger'/'CMD', '247333').

card_in_set('stitch together', 'CMD').
card_original_type('stitch together'/'CMD', 'Sorcery').
card_original_text('stitch together'/'CMD', 'Return target creature card from your graveyard to your hand.\nThreshold — Return that card from your graveyard to the battlefield instead if seven or more cards are in your graveyard.').
card_image_name('stitch together'/'CMD', 'stitch together').
card_uid('stitch together'/'CMD', 'CMD:Stitch Together:stitch together').
card_rarity('stitch together'/'CMD', 'Uncommon').
card_artist('stitch together'/'CMD', 'Arnie Swekel').
card_number('stitch together'/'CMD', '102').
card_multiverse_id('stitch together'/'CMD', '247292').

card_in_set('storm herd', 'CMD').
card_original_type('storm herd'/'CMD', 'Sorcery').
card_original_text('storm herd'/'CMD', 'Put X 1/1 white Pegasus creature tokens with flying onto the battlefield, where X is your life total.').
card_image_name('storm herd'/'CMD', 'storm herd').
card_uid('storm herd'/'CMD', 'CMD:Storm Herd:storm herd').
card_rarity('storm herd'/'CMD', 'Rare').
card_artist('storm herd'/'CMD', 'Jim Nelson').
card_number('storm herd'/'CMD', '34').
card_flavor_text('storm herd'/'CMD', '\"When you hear thunder on a cloudless day, take cover and brace for the coming of the storm herd.\"\n—Skotov, Tin Street basket vendor').
card_multiverse_id('storm herd'/'CMD', '247287').

card_in_set('stranglehold', 'CMD').
card_original_type('stranglehold'/'CMD', 'Enchantment').
card_original_text('stranglehold'/'CMD', 'Your opponents can\'t search libraries.\nIf an opponent would begin an extra turn, that player skips that turn instead.').
card_first_print('stranglehold', 'CMD').
card_image_name('stranglehold'/'CMD', 'stranglehold').
card_uid('stranglehold'/'CMD', 'CMD:Stranglehold:stranglehold').
card_rarity('stranglehold'/'CMD', 'Rare').
card_artist('stranglehold'/'CMD', 'John Stanko').
card_number('stranglehold'/'CMD', '136').
card_flavor_text('stranglehold'/'CMD', 'The correct answer to a barbarian\'s riddle is to choke on your cleverness and die.').
card_multiverse_id('stranglehold'/'CMD', '233183').

card_in_set('sulfurous blast', 'CMD').
card_original_type('sulfurous blast'/'CMD', 'Instant').
card_original_text('sulfurous blast'/'CMD', 'Sulfurous Blast deals 2 damage to each creature and each player. If you cast this spell during your main phase, Sulfurous Blast deals 3 damage to each creature and each player instead.').
card_image_name('sulfurous blast'/'CMD', 'sulfurous blast').
card_uid('sulfurous blast'/'CMD', 'CMD:Sulfurous Blast:sulfurous blast').
card_rarity('sulfurous blast'/'CMD', 'Uncommon').
card_artist('sulfurous blast'/'CMD', 'Jeff Miracola').
card_number('sulfurous blast'/'CMD', '137').
card_flavor_text('sulfurous blast'/'CMD', 'The Keldons used the toxic vents in the cracked earth to bolster their home\'s defenses.').
card_multiverse_id('sulfurous blast'/'CMD', '247518').

card_in_set('svogthos, the restless tomb', 'CMD').
card_original_type('svogthos, the restless tomb'/'CMD', 'Land').
card_original_text('svogthos, the restless tomb'/'CMD', '{T}: Add {1} to your mana pool.\n{3}{B}{G}: Until end of turn, Svogthos, the Restless Tomb becomes a black and green Plant Zombie creature with \"This creature\'s power and toughness are each equal to the number of creature cards in your graveyard.\" It\'s still a land.').
card_image_name('svogthos, the restless tomb'/'CMD', 'svogthos, the restless tomb').
card_uid('svogthos, the restless tomb'/'CMD', 'CMD:Svogthos, the Restless Tomb:svogthos, the restless tomb').
card_rarity('svogthos, the restless tomb'/'CMD', 'Uncommon').
card_artist('svogthos, the restless tomb'/'CMD', 'Martina Pilcerova').
card_number('svogthos, the restless tomb'/'CMD', '289').
card_multiverse_id('svogthos, the restless tomb'/'CMD', '247389').

card_in_set('swamp', 'CMD').
card_original_type('swamp'/'CMD', 'Basic Land — Swamp').
card_original_text('swamp'/'CMD', 'B').
card_image_name('swamp'/'CMD', 'swamp1').
card_uid('swamp'/'CMD', 'CMD:Swamp:swamp1').
card_rarity('swamp'/'CMD', 'Basic Land').
card_artist('swamp'/'CMD', 'Dan Frazier').
card_number('swamp'/'CMD', '307').
card_multiverse_id('swamp'/'CMD', '249816').

card_in_set('swamp', 'CMD').
card_original_type('swamp'/'CMD', 'Basic Land — Swamp').
card_original_text('swamp'/'CMD', 'B').
card_image_name('swamp'/'CMD', 'swamp2').
card_uid('swamp'/'CMD', 'CMD:Swamp:swamp2').
card_rarity('swamp'/'CMD', 'Basic Land').
card_artist('swamp'/'CMD', 'Jung Park').
card_number('swamp'/'CMD', '308').
card_multiverse_id('swamp'/'CMD', '249819').

card_in_set('swamp', 'CMD').
card_original_type('swamp'/'CMD', 'Basic Land — Swamp').
card_original_text('swamp'/'CMD', 'B').
card_image_name('swamp'/'CMD', 'swamp3').
card_uid('swamp'/'CMD', 'CMD:Swamp:swamp3').
card_rarity('swamp'/'CMD', 'Basic Land').
card_artist('swamp'/'CMD', 'Jim Pavelec').
card_number('swamp'/'CMD', '309').
card_multiverse_id('swamp'/'CMD', '249818').

card_in_set('swamp', 'CMD').
card_original_type('swamp'/'CMD', 'Basic Land — Swamp').
card_original_text('swamp'/'CMD', 'B').
card_image_name('swamp'/'CMD', 'swamp4').
card_uid('swamp'/'CMD', 'CMD:Swamp:swamp4').
card_rarity('swamp'/'CMD', 'Basic Land').
card_artist('swamp'/'CMD', 'Richard Wright').
card_number('swamp'/'CMD', '310').
card_multiverse_id('swamp'/'CMD', '249817').

card_in_set('symbiotic wurm', 'CMD').
card_original_type('symbiotic wurm'/'CMD', 'Creature — Wurm').
card_original_text('symbiotic wurm'/'CMD', 'When Symbiotic Wurm is put into a graveyard from the battlefield, put seven 1/1 green Insect creature tokens onto the battlefield.').
card_image_name('symbiotic wurm'/'CMD', 'symbiotic wurm').
card_uid('symbiotic wurm'/'CMD', 'CMD:Symbiotic Wurm:symbiotic wurm').
card_rarity('symbiotic wurm'/'CMD', 'Rare').
card_artist('symbiotic wurm'/'CMD', 'Matt Cavotta').
card_number('symbiotic wurm'/'CMD', '174').
card_flavor_text('symbiotic wurm'/'CMD', 'The insects keep the wurm\'s hide free from parasites. In return, the wurm doesn\'t eat the insects.').
card_multiverse_id('symbiotic wurm'/'CMD', '247354').

card_in_set('syphon flesh', 'CMD').
card_original_type('syphon flesh'/'CMD', 'Sorcery').
card_original_text('syphon flesh'/'CMD', 'Each other player sacrifices a creature. You put a 2/2 black Zombie creature token onto the battlefield for each creature sacrificed this way.').
card_first_print('syphon flesh', 'CMD').
card_image_name('syphon flesh'/'CMD', 'syphon flesh').
card_uid('syphon flesh'/'CMD', 'CMD:Syphon Flesh:syphon flesh').
card_rarity('syphon flesh'/'CMD', 'Uncommon').
card_artist('syphon flesh'/'CMD', 'Ironbrush').
card_number('syphon flesh'/'CMD', '103').
card_flavor_text('syphon flesh'/'CMD', 'Necromancers deal in commodities far more precious than gold and grain.').
card_multiverse_id('syphon flesh'/'CMD', '233218').

card_in_set('syphon mind', 'CMD').
card_original_type('syphon mind'/'CMD', 'Sorcery').
card_original_text('syphon mind'/'CMD', 'Each other player discards a card. You draw a card for each card discarded this way.').
card_image_name('syphon mind'/'CMD', 'syphon mind').
card_uid('syphon mind'/'CMD', 'CMD:Syphon Mind:syphon mind').
card_rarity('syphon mind'/'CMD', 'Common').
card_artist('syphon mind'/'CMD', 'Jeff Easley').
card_number('syphon mind'/'CMD', '104').
card_flavor_text('syphon mind'/'CMD', 'When tempers run high, it\'s easy to lose your head.').
card_multiverse_id('syphon mind'/'CMD', '247355').

card_in_set('szadek, lord of secrets', 'CMD').
card_original_type('szadek, lord of secrets'/'CMD', 'Legendary Creature — Vampire').
card_original_text('szadek, lord of secrets'/'CMD', 'Flying\nIf Szadek, Lord of Secrets would deal combat damage to a player, instead put that many +1/+1 counters on Szadek and that player puts that many cards from the top of his or her library into his or her graveyard.').
card_image_name('szadek, lord of secrets'/'CMD', 'szadek, lord of secrets').
card_uid('szadek, lord of secrets'/'CMD', 'CMD:Szadek, Lord of Secrets:szadek, lord of secrets').
card_rarity('szadek, lord of secrets'/'CMD', 'Rare').
card_artist('szadek, lord of secrets'/'CMD', 'Donato Giancola').
card_number('szadek, lord of secrets'/'CMD', '228').
card_multiverse_id('szadek, lord of secrets'/'CMD', '247390').

card_in_set('tariel, reckoner of souls', 'CMD').
card_original_type('tariel, reckoner of souls'/'CMD', 'Legendary Creature — Angel').
card_original_text('tariel, reckoner of souls'/'CMD', 'Flying, vigilance\n{T}: Choose a creature card at random from target opponent\'s graveyard. Put that card onto the battlefield under your control.').
card_first_print('tariel, reckoner of souls', 'CMD').
card_image_name('tariel, reckoner of souls'/'CMD', 'tariel, reckoner of souls').
card_uid('tariel, reckoner of souls'/'CMD', 'CMD:Tariel, Reckoner of Souls:tariel, reckoner of souls').
card_rarity('tariel, reckoner of souls'/'CMD', 'Mythic Rare').
card_artist('tariel, reckoner of souls'/'CMD', 'Wayne Reynolds').
card_number('tariel, reckoner of souls'/'CMD', '229').
card_flavor_text('tariel, reckoner of souls'/'CMD', '\"After death you face paradise, damnation, or Tariel.\"\n—Priest\'s teaching').
card_multiverse_id('tariel, reckoner of souls'/'CMD', '236995').

card_in_set('temple of the false god', 'CMD').
card_original_type('temple of the false god'/'CMD', 'Land').
card_original_text('temple of the false god'/'CMD', '{T}: Add {2} to your mana pool. Activate this ability only if you control five or more lands.').
card_image_name('temple of the false god'/'CMD', 'temple of the false god').
card_uid('temple of the false god'/'CMD', 'CMD:Temple of the False God:temple of the false god').
card_rarity('temple of the false god'/'CMD', 'Uncommon').
card_artist('temple of the false god'/'CMD', 'Brian Snõddy').
card_number('temple of the false god'/'CMD', '290').
card_flavor_text('temple of the false god'/'CMD', 'Those who bring nothing to the temple take nothing away.').
card_multiverse_id('temple of the false god'/'CMD', '247406').

card_in_set('teneb, the harvester', 'CMD').
card_original_type('teneb, the harvester'/'CMD', 'Legendary Creature — Dragon').
card_original_text('teneb, the harvester'/'CMD', 'Flying\nWhenever Teneb, the Harvester deals combat damage to a player, you may pay {2}{B}. If you do, put target creature card from a graveyard onto the battlefield under your control.').
card_image_name('teneb, the harvester'/'CMD', 'teneb, the harvester').
card_uid('teneb, the harvester'/'CMD', 'CMD:Teneb, the Harvester:teneb, the harvester').
card_rarity('teneb, the harvester'/'CMD', 'Rare').
card_artist('teneb, the harvester'/'CMD', 'Zoltan Boros & Gabor Szikszai').
card_number('teneb, the harvester'/'CMD', '230').
card_multiverse_id('teneb, the harvester'/'CMD', '247365').

card_in_set('terminate', 'CMD').
card_original_type('terminate'/'CMD', 'Instant').
card_original_text('terminate'/'CMD', 'Destroy target creature. It can\'t be regenerated.').
card_image_name('terminate'/'CMD', 'terminate').
card_uid('terminate'/'CMD', 'CMD:Terminate:terminate').
card_rarity('terminate'/'CMD', 'Common').
card_artist('terminate'/'CMD', 'Wayne Reynolds').
card_number('terminate'/'CMD', '231').
card_flavor_text('terminate'/'CMD', '\"I\'ve seen death before. My mother succumbing to illness, my comrades bleeding on the battlefield . . . But I\'d never seen anything as dreadful as that.\"\n—Taani, berserker of Etlan').
card_multiverse_id('terminate'/'CMD', '247166').

card_in_set('terramorphic expanse', 'CMD').
card_original_type('terramorphic expanse'/'CMD', 'Land').
card_original_text('terramorphic expanse'/'CMD', '{T}, Sacrifice Terramorphic Expanse: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('terramorphic expanse'/'CMD', 'terramorphic expanse').
card_uid('terramorphic expanse'/'CMD', 'CMD:Terramorphic Expanse:terramorphic expanse').
card_rarity('terramorphic expanse'/'CMD', 'Common').
card_artist('terramorphic expanse'/'CMD', 'Dan Scott').
card_number('terramorphic expanse'/'CMD', '291').
card_flavor_text('terramorphic expanse'/'CMD', 'Take two steps north into the unsettled future, south into the unquiet past, east into the present day, or west into the great unknown.').
card_multiverse_id('terramorphic expanse'/'CMD', '247328').

card_in_set('the mimeoplasm', 'CMD').
card_original_type('the mimeoplasm'/'CMD', 'Legendary Creature — Ooze').
card_original_text('the mimeoplasm'/'CMD', 'As The Mimeoplasm enters the battlefield, you may exile two creature cards from graveyards. If you do, it enters the battlefield as a copy of one of those cards with a number of additional +1/+1 counters on it equal to the power of the other card.').
card_first_print('the mimeoplasm', 'CMD').
card_image_name('the mimeoplasm'/'CMD', 'the mimeoplasm').
card_uid('the mimeoplasm'/'CMD', 'CMD:The Mimeoplasm:the mimeoplasm').
card_rarity('the mimeoplasm'/'CMD', 'Mythic Rare').
card_artist('the mimeoplasm'/'CMD', 'Svetlin Velinov').
card_number('the mimeoplasm'/'CMD', '210').
card_multiverse_id('the mimeoplasm'/'CMD', '228232').

card_in_set('trade secrets', 'CMD').
card_original_type('trade secrets'/'CMD', 'Sorcery').
card_original_text('trade secrets'/'CMD', 'Target opponent draws two cards, then you draw up to four cards. That opponent may repeat this process as many times as he or she chooses.').
card_image_name('trade secrets'/'CMD', 'trade secrets').
card_uid('trade secrets'/'CMD', 'CMD:Trade Secrets:trade secrets').
card_rarity('trade secrets'/'CMD', 'Rare').
card_artist('trade secrets'/'CMD', 'Ron Spears').
card_number('trade secrets'/'CMD', '64').
card_multiverse_id('trade secrets'/'CMD', '247356').

card_in_set('tranquil thicket', 'CMD').
card_original_type('tranquil thicket'/'CMD', 'Land').
card_original_text('tranquil thicket'/'CMD', 'Tranquil Thicket enters the battlefield tapped.\n{T}: Add {G} to your mana pool.\nCycling {G} ({G}, Discard this card: Draw a card.)').
card_image_name('tranquil thicket'/'CMD', 'tranquil thicket').
card_uid('tranquil thicket'/'CMD', 'CMD:Tranquil Thicket:tranquil thicket').
card_rarity('tranquil thicket'/'CMD', 'Common').
card_artist('tranquil thicket'/'CMD', 'Heather Hudson').
card_number('tranquil thicket'/'CMD', '292').
card_multiverse_id('tranquil thicket'/'CMD', '247357').

card_in_set('trench gorger', 'CMD').
card_original_type('trench gorger'/'CMD', 'Creature — Leviathan').
card_original_text('trench gorger'/'CMD', 'Trample\nWhen Trench Gorger enters the battlefield, you may search your library for any number of land cards, exile them, then shuffle your library. If you do, Trench Gorger\'s power and toughness each become equal to the number of cards exiled this way.').
card_first_print('trench gorger', 'CMD').
card_image_name('trench gorger'/'CMD', 'trench gorger').
card_uid('trench gorger'/'CMD', 'CMD:Trench Gorger:trench gorger').
card_rarity('trench gorger'/'CMD', 'Rare').
card_artist('trench gorger'/'CMD', 'Hideaki Takamura').
card_number('trench gorger'/'CMD', '65').
card_multiverse_id('trench gorger'/'CMD', '228242').

card_in_set('tribute to the wild', 'CMD').
card_original_type('tribute to the wild'/'CMD', 'Instant').
card_original_text('tribute to the wild'/'CMD', 'Each opponent sacrifices an artifact or enchantment.').
card_first_print('tribute to the wild', 'CMD').
card_image_name('tribute to the wild'/'CMD', 'tribute to the wild').
card_uid('tribute to the wild'/'CMD', 'CMD:Tribute to the Wild:tribute to the wild').
card_rarity('tribute to the wild'/'CMD', 'Uncommon').
card_artist('tribute to the wild'/'CMD', 'Hideaki Takamura').
card_number('tribute to the wild'/'CMD', '175').
card_flavor_text('tribute to the wild'/'CMD', '\"You may enter, but leave those lifeless things of your world behind.\"\n—Modruni, maro-sorcerer').
card_multiverse_id('tribute to the wild'/'CMD', '236998').

card_in_set('triskelavus', 'CMD').
card_original_type('triskelavus'/'CMD', 'Artifact Creature — Construct').
card_original_text('triskelavus'/'CMD', 'Flying\nTriskelavus enters the battlefield with three +1/+1 counters on it.\n{1}, Remove a +1/+1 counter from Triskelavus: Put a 1/1 colorless Triskelavite artifact creature token with flying onto the battlefield. It has \"Sacrifice this creature: This creature deals 1 damage to target creature or player.\"').
card_image_name('triskelavus'/'CMD', 'triskelavus').
card_uid('triskelavus'/'CMD', 'CMD:Triskelavus:triskelavus').
card_rarity('triskelavus'/'CMD', 'Rare').
card_artist('triskelavus'/'CMD', 'Mark Zug').
card_number('triskelavus'/'CMD', '263').
card_multiverse_id('triskelavus'/'CMD', '247519').

card_in_set('troll ascetic', 'CMD').
card_original_type('troll ascetic'/'CMD', 'Creature — Troll Shaman').
card_original_text('troll ascetic'/'CMD', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)\n{1}{G}: Regenerate Troll Ascetic.').
card_image_name('troll ascetic'/'CMD', 'troll ascetic').
card_uid('troll ascetic'/'CMD', 'CMD:Troll Ascetic:troll ascetic').
card_rarity('troll ascetic'/'CMD', 'Rare').
card_artist('troll ascetic'/'CMD', 'Puddnhead').
card_number('troll ascetic'/'CMD', '176').
card_flavor_text('troll ascetic'/'CMD', 'It\'s no coincidence that the oldest trolls are also the angriest.').
card_multiverse_id('troll ascetic'/'CMD', '247145').

card_in_set('unnerve', 'CMD').
card_original_type('unnerve'/'CMD', 'Sorcery').
card_original_text('unnerve'/'CMD', 'Each opponent discards two cards.').
card_image_name('unnerve'/'CMD', 'unnerve').
card_uid('unnerve'/'CMD', 'CMD:Unnerve:unnerve').
card_rarity('unnerve'/'CMD', 'Common').
card_artist('unnerve'/'CMD', 'Terese Nielsen').
card_number('unnerve'/'CMD', '105').
card_flavor_text('unnerve'/'CMD', '\"If fear is the only tool you have left, then you\'ll never control me.\"\n—Xantcha, to Gix').
card_multiverse_id('unnerve'/'CMD', '247531').

card_in_set('valley rannet', 'CMD').
card_original_type('valley rannet'/'CMD', 'Creature — Beast').
card_original_text('valley rannet'/'CMD', 'Mountaincycling {2}, forestcycling {2} ({2}, Discard this card: Search your library for a Mountain or Forest card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('valley rannet'/'CMD', 'valley rannet').
card_uid('valley rannet'/'CMD', 'CMD:Valley Rannet:valley rannet').
card_rarity('valley rannet'/'CMD', 'Common').
card_artist('valley rannet'/'CMD', 'Dave Allsop').
card_number('valley rannet'/'CMD', '232').
card_multiverse_id('valley rannet'/'CMD', '247167').

card_in_set('vampire nighthawk', 'CMD').
card_original_type('vampire nighthawk'/'CMD', 'Creature — Vampire Shaman').
card_original_text('vampire nighthawk'/'CMD', 'Flying\nDeathtouch (Any amount of damage this deals to a creature is enough to destroy it.)\nLifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_image_name('vampire nighthawk'/'CMD', 'vampire nighthawk').
card_uid('vampire nighthawk'/'CMD', 'CMD:Vampire Nighthawk:vampire nighthawk').
card_rarity('vampire nighthawk'/'CMD', 'Uncommon').
card_artist('vampire nighthawk'/'CMD', 'Jason Chan').
card_number('vampire nighthawk'/'CMD', '106').
card_multiverse_id('vampire nighthawk'/'CMD', '247552').

card_in_set('vedalken plotter', 'CMD').
card_original_type('vedalken plotter'/'CMD', 'Creature — Vedalken Wizard').
card_original_text('vedalken plotter'/'CMD', 'When Vedalken Plotter enters the battlefield, exchange control of target land you control and target land an opponent controls.').
card_image_name('vedalken plotter'/'CMD', 'vedalken plotter').
card_uid('vedalken plotter'/'CMD', 'CMD:Vedalken Plotter:vedalken plotter').
card_rarity('vedalken plotter'/'CMD', 'Uncommon').
card_artist('vedalken plotter'/'CMD', 'Greg Staples').
card_number('vedalken plotter'/'CMD', '66').
card_flavor_text('vedalken plotter'/'CMD', '\"Fair? At what point in our negotiations did you convince yourself my goal was to be fair?\"').
card_multiverse_id('vedalken plotter'/'CMD', '247288').

card_in_set('vengeful rebirth', 'CMD').
card_original_type('vengeful rebirth'/'CMD', 'Sorcery').
card_original_text('vengeful rebirth'/'CMD', 'Return target card from your graveyard to your hand. If you return a nonland card to your hand this way, Vengeful Rebirth deals damage equal to that card\'s converted mana cost to target creature or player.\nExile Vengeful Rebirth.').
card_image_name('vengeful rebirth'/'CMD', 'vengeful rebirth').
card_uid('vengeful rebirth'/'CMD', 'CMD:Vengeful Rebirth:vengeful rebirth').
card_rarity('vengeful rebirth'/'CMD', 'Uncommon').
card_artist('vengeful rebirth'/'CMD', 'Vance Kovacs').
card_number('vengeful rebirth'/'CMD', '233').
card_multiverse_id('vengeful rebirth'/'CMD', '247168').

card_in_set('veteran explorer', 'CMD').
card_original_type('veteran explorer'/'CMD', 'Creature — Human Soldier Scout').
card_original_text('veteran explorer'/'CMD', 'When Veteran Explorer is put into a graveyard from the battlefield, each player may search his or her library for up to two basic land cards and put them onto the battlefield. Then each player who searched his or her library this way shuffles it.').
card_image_name('veteran explorer'/'CMD', 'veteran explorer').
card_uid('veteran explorer'/'CMD', 'CMD:Veteran Explorer:veteran explorer').
card_rarity('veteran explorer'/'CMD', 'Uncommon').
card_artist('veteran explorer'/'CMD', 'David A. Cherry').
card_number('veteran explorer'/'CMD', '177').
card_multiverse_id('veteran explorer'/'CMD', '247534').

card_in_set('vish kal, blood arbiter', 'CMD').
card_original_type('vish kal, blood arbiter'/'CMD', 'Legendary Creature — Vampire').
card_original_text('vish kal, blood arbiter'/'CMD', 'Flying, lifelink\nSacrifice a creature: Put X +1/+1 counters on Vish Kal, Blood Arbiter, where X is the sacrificed creature\'s power.\nRemove all +1/+1 counters from Vish Kal: Target creature gets -1/-1 until end of turn for each +1/+1 counter removed this way.').
card_first_print('vish kal, blood arbiter', 'CMD').
card_image_name('vish kal, blood arbiter'/'CMD', 'vish kal, blood arbiter').
card_uid('vish kal, blood arbiter'/'CMD', 'CMD:Vish Kal, Blood Arbiter:vish kal, blood arbiter').
card_rarity('vish kal, blood arbiter'/'CMD', 'Rare').
card_artist('vish kal, blood arbiter'/'CMD', 'Michael C. Hayes').
card_number('vish kal, blood arbiter'/'CMD', '234').
card_multiverse_id('vish kal, blood arbiter'/'CMD', '236487').

card_in_set('vision skeins', 'CMD').
card_original_type('vision skeins'/'CMD', 'Instant').
card_original_text('vision skeins'/'CMD', 'Each player draws two cards.').
card_image_name('vision skeins'/'CMD', 'vision skeins').
card_uid('vision skeins'/'CMD', 'CMD:Vision Skeins:vision skeins').
card_rarity('vision skeins'/'CMD', 'Common').
card_artist('vision skeins'/'CMD', 'Aleksi Briclot').
card_number('vision skeins'/'CMD', '67').
card_flavor_text('vision skeins'/'CMD', '\"I could see in the other mage\'s eyes that he\'d thought of it too. Then it became a race to exploit the knowledge first.\"').
card_multiverse_id('vision skeins'/'CMD', '247198').

card_in_set('vivid crag', 'CMD').
card_original_type('vivid crag'/'CMD', 'Land').
card_original_text('vivid crag'/'CMD', 'Vivid Crag enters the battlefield tapped with two charge counters on it.\n{T}: Add {R} to your mana pool.\n{T}, Remove a charge counter from Vivid Crag: Add one mana of any color to your mana pool.').
card_image_name('vivid crag'/'CMD', 'vivid crag').
card_uid('vivid crag'/'CMD', 'CMD:Vivid Crag:vivid crag').
card_rarity('vivid crag'/'CMD', 'Uncommon').
card_artist('vivid crag'/'CMD', 'Martina Pilcerova').
card_number('vivid crag'/'CMD', '293').
card_multiverse_id('vivid crag'/'CMD', '247306').

card_in_set('vivid creek', 'CMD').
card_original_type('vivid creek'/'CMD', 'Land').
card_original_text('vivid creek'/'CMD', 'Vivid Creek enters the battlefield tapped with two charge counters on it.\n{T}: Add {U} to your mana pool.\n{T}, Remove a charge counter from Vivid Creek: Add one mana of any color to your mana pool.').
card_image_name('vivid creek'/'CMD', 'vivid creek').
card_uid('vivid creek'/'CMD', 'CMD:Vivid Creek:vivid creek').
card_rarity('vivid creek'/'CMD', 'Uncommon').
card_artist('vivid creek'/'CMD', 'Fred Fields').
card_number('vivid creek'/'CMD', '294').
card_multiverse_id('vivid creek'/'CMD', '247307').

card_in_set('vivid grove', 'CMD').
card_original_type('vivid grove'/'CMD', 'Land').
card_original_text('vivid grove'/'CMD', 'Vivid Grove enters the battlefield tapped with two charge counters on it.\n{T}: Add {G} to your mana pool.\n{T}, Remove a charge counter from Vivid Grove: Add one mana of any color to your mana pool.').
card_image_name('vivid grove'/'CMD', 'vivid grove').
card_uid('vivid grove'/'CMD', 'CMD:Vivid Grove:vivid grove').
card_rarity('vivid grove'/'CMD', 'Uncommon').
card_artist('vivid grove'/'CMD', 'Howard Lyon').
card_number('vivid grove'/'CMD', '295').
card_multiverse_id('vivid grove'/'CMD', '247308').

card_in_set('vivid marsh', 'CMD').
card_original_type('vivid marsh'/'CMD', 'Land').
card_original_text('vivid marsh'/'CMD', 'Vivid Marsh enters the battlefield tapped with two charge counters on it.\n{T}: Add {B} to your mana pool.\n{T}, Remove a charge counter from Vivid Marsh: Add one mana of any color to your mana pool.').
card_image_name('vivid marsh'/'CMD', 'vivid marsh').
card_uid('vivid marsh'/'CMD', 'CMD:Vivid Marsh:vivid marsh').
card_rarity('vivid marsh'/'CMD', 'Uncommon').
card_artist('vivid marsh'/'CMD', 'John Avon').
card_number('vivid marsh'/'CMD', '296').
card_multiverse_id('vivid marsh'/'CMD', '247309').

card_in_set('vivid meadow', 'CMD').
card_original_type('vivid meadow'/'CMD', 'Land').
card_original_text('vivid meadow'/'CMD', 'Vivid Meadow enters the battlefield tapped with two charge counters on it.\n{T}: Add {W} to your mana pool.\n{T}, Remove a charge counter from Vivid Meadow: Add one mana of any color to your mana pool.').
card_image_name('vivid meadow'/'CMD', 'vivid meadow').
card_uid('vivid meadow'/'CMD', 'CMD:Vivid Meadow:vivid meadow').
card_rarity('vivid meadow'/'CMD', 'Uncommon').
card_artist('vivid meadow'/'CMD', 'Rob Alexander').
card_number('vivid meadow'/'CMD', '297').
card_multiverse_id('vivid meadow'/'CMD', '247310').

card_in_set('voice of all', 'CMD').
card_original_type('voice of all'/'CMD', 'Creature — Angel').
card_original_text('voice of all'/'CMD', 'Flying\nAs Voice of All enters the battlefield, choose a color. \nVoice of All has protection from the chosen color.').
card_image_name('voice of all'/'CMD', 'voice of all').
card_uid('voice of all'/'CMD', 'CMD:Voice of All:voice of all').
card_rarity('voice of all'/'CMD', 'Rare').
card_artist('voice of all'/'CMD', 'rk post').
card_number('voice of all'/'CMD', '35').
card_multiverse_id('voice of all'/'CMD', '247146').

card_in_set('vorosh, the hunter', 'CMD').
card_original_type('vorosh, the hunter'/'CMD', 'Legendary Creature — Dragon').
card_original_text('vorosh, the hunter'/'CMD', 'Flying\nWhenever Vorosh, the Hunter deals combat damage to a player, you may pay {2}{G}. If you do, put six +1/+1 counters on Vorosh.').
card_image_name('vorosh, the hunter'/'CMD', 'vorosh, the hunter').
card_uid('vorosh, the hunter'/'CMD', 'CMD:Vorosh, the Hunter:vorosh, the hunter').
card_rarity('vorosh, the hunter'/'CMD', 'Rare').
card_artist('vorosh, the hunter'/'CMD', 'Mark Zug').
card_number('vorosh, the hunter'/'CMD', '235').
card_multiverse_id('vorosh, the hunter'/'CMD', '247366').

card_in_set('vow of duty', 'CMD').
card_original_type('vow of duty'/'CMD', 'Enchantment — Aura').
card_original_text('vow of duty'/'CMD', 'Enchant creature\nEnchanted creature gets +2/+2, has vigilance, and can\'t attack you or a planeswalker you control.').
card_first_print('vow of duty', 'CMD').
card_image_name('vow of duty'/'CMD', 'vow of duty').
card_uid('vow of duty'/'CMD', 'CMD:Vow of Duty:vow of duty').
card_rarity('vow of duty'/'CMD', 'Uncommon').
card_artist('vow of duty'/'CMD', 'Wayne Reynolds').
card_number('vow of duty'/'CMD', '36').
card_flavor_text('vow of duty'/'CMD', '\"I prefer loyalty to be a matter of respect, not of magic. But one does what one must.\"\n—Kaalia of the Vast').
card_multiverse_id('vow of duty'/'CMD', '236986').

card_in_set('vow of flight', 'CMD').
card_original_type('vow of flight'/'CMD', 'Enchantment — Aura').
card_original_text('vow of flight'/'CMD', 'Enchant creature\nEnchanted creature gets +2/+2, has flying, and can\'t attack you or a planeswalker you control.').
card_first_print('vow of flight', 'CMD').
card_image_name('vow of flight'/'CMD', 'vow of flight').
card_uid('vow of flight'/'CMD', 'CMD:Vow of Flight:vow of flight').
card_rarity('vow of flight'/'CMD', 'Uncommon').
card_artist('vow of flight'/'CMD', 'Erica Yang').
card_number('vow of flight'/'CMD', '68').
card_flavor_text('vow of flight'/'CMD', '\"Soaring is wasted on the winged.\"\n—Edric, Spymaster of Trest').
card_multiverse_id('vow of flight'/'CMD', '238144').

card_in_set('vow of lightning', 'CMD').
card_original_type('vow of lightning'/'CMD', 'Enchantment — Aura').
card_original_text('vow of lightning'/'CMD', 'Enchant creature\nEnchanted creature gets +2/+2, has first strike, and can\'t attack you or a planeswalker you control.').
card_first_print('vow of lightning', 'CMD').
card_image_name('vow of lightning'/'CMD', 'vow of lightning').
card_uid('vow of lightning'/'CMD', 'CMD:Vow of Lightning:vow of lightning').
card_rarity('vow of lightning'/'CMD', 'Uncommon').
card_artist('vow of lightning'/'CMD', 'Svetlin Velinov').
card_number('vow of lightning'/'CMD', '138').
card_flavor_text('vow of lightning'/'CMD', '\"Ruhan would never agree to such terms.\"\n—Ruhan of the Fomori').
card_multiverse_id('vow of lightning'/'CMD', '236999').

card_in_set('vow of malice', 'CMD').
card_original_type('vow of malice'/'CMD', 'Enchantment — Aura').
card_original_text('vow of malice'/'CMD', 'Enchant creature\nEnchanted creature gets +2/+2, has intimidate, and can\'t attack you or a planeswalker you control.').
card_first_print('vow of malice', 'CMD').
card_image_name('vow of malice'/'CMD', 'vow of malice').
card_uid('vow of malice'/'CMD', 'CMD:Vow of Malice:vow of malice').
card_rarity('vow of malice'/'CMD', 'Uncommon').
card_artist('vow of malice'/'CMD', 'Jesper Ejsing').
card_number('vow of malice'/'CMD', '107').
card_flavor_text('vow of malice'/'CMD', '\"I grant you blades—on the condition that they are not pointed at me.\"\n—Damia, Sage of Stone').
card_multiverse_id('vow of malice'/'CMD', '238145').

card_in_set('vow of wildness', 'CMD').
card_original_type('vow of wildness'/'CMD', 'Enchantment — Aura').
card_original_text('vow of wildness'/'CMD', 'Enchant creature\nEnchanted creature gets +3/+3, has trample, and can\'t attack you or a planeswalker you control.').
card_first_print('vow of wildness', 'CMD').
card_image_name('vow of wildness'/'CMD', 'vow of wildness').
card_uid('vow of wildness'/'CMD', 'CMD:Vow of Wildness:vow of wildness').
card_rarity('vow of wildness'/'CMD', 'Uncommon').
card_artist('vow of wildness'/'CMD', 'Jim Pavelec').
card_number('vow of wildness'/'CMD', '178').
card_flavor_text('vow of wildness'/'CMD', '\"Savagery is the birthright of every creature that draws breath—or wishes it could.\"\n—Karador, Ghost Chieftain').
card_multiverse_id('vow of wildness'/'CMD', '238135').

card_in_set('vulturous zombie', 'CMD').
card_original_type('vulturous zombie'/'CMD', 'Creature — Plant Zombie').
card_original_text('vulturous zombie'/'CMD', 'Flying\nWhenever a card is put into an opponent\'s graveyard from anywhere, put a +1/+1 counter on Vulturous Zombie.').
card_image_name('vulturous zombie'/'CMD', 'vulturous zombie').
card_uid('vulturous zombie'/'CMD', 'CMD:Vulturous Zombie:vulturous zombie').
card_rarity('vulturous zombie'/'CMD', 'Rare').
card_artist('vulturous zombie'/'CMD', 'Greg Staples').
card_number('vulturous zombie'/'CMD', '236').
card_flavor_text('vulturous zombie'/'CMD', '\"When something dies, all things benefit. Well okay, just our things.\"\n—Ezoc, Golgari rot farmer').
card_multiverse_id('vulturous zombie'/'CMD', '247391').

card_in_set('wall of denial', 'CMD').
card_original_type('wall of denial'/'CMD', 'Creature — Wall').
card_original_text('wall of denial'/'CMD', 'Defender, flying\nShroud (This creature can\'t be the target of spells or abilities.)').
card_image_name('wall of denial'/'CMD', 'wall of denial').
card_uid('wall of denial'/'CMD', 'CMD:Wall of Denial:wall of denial').
card_rarity('wall of denial'/'CMD', 'Uncommon').
card_artist('wall of denial'/'CMD', 'Howard Lyon').
card_number('wall of denial'/'CMD', '237').
card_flavor_text('wall of denial'/'CMD', 'It provides what every discerning mage requires—time to think.').
card_multiverse_id('wall of denial'/'CMD', '247169').

card_in_set('wall of omens', 'CMD').
card_original_type('wall of omens'/'CMD', 'Creature — Wall').
card_original_text('wall of omens'/'CMD', 'Defender\nWhen Wall of Omens enters the battlefield, draw a card.').
card_image_name('wall of omens'/'CMD', 'wall of omens').
card_uid('wall of omens'/'CMD', 'CMD:Wall of Omens:wall of omens').
card_rarity('wall of omens'/'CMD', 'Uncommon').
card_artist('wall of omens'/'CMD', 'James Paick').
card_number('wall of omens'/'CMD', '37').
card_flavor_text('wall of omens'/'CMD', '\"I search for a vision of Zendikar that does not include the Eldrazi.\"\n—Expedition journal entry').
card_multiverse_id('wall of omens'/'CMD', '247400').

card_in_set('whirlpool whelm', 'CMD').
card_original_type('whirlpool whelm'/'CMD', 'Instant').
card_original_text('whirlpool whelm'/'CMD', 'Clash with an opponent, then return target creature to its owner\'s hand. If you win, you may put that creature on top of its owner\'s library instead. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_image_name('whirlpool whelm'/'CMD', 'whirlpool whelm').
card_uid('whirlpool whelm'/'CMD', 'CMD:Whirlpool Whelm:whirlpool whelm').
card_rarity('whirlpool whelm'/'CMD', 'Common').
card_artist('whirlpool whelm'/'CMD', 'Cyril Van Der Haegen').
card_number('whirlpool whelm'/'CMD', '69').
card_multiverse_id('whirlpool whelm'/'CMD', '247311').

card_in_set('wild ricochet', 'CMD').
card_original_type('wild ricochet'/'CMD', 'Instant').
card_original_text('wild ricochet'/'CMD', 'You may choose new targets for target instant or sorcery spell. Then copy that spell. You may choose new targets for the copy.').
card_image_name('wild ricochet'/'CMD', 'wild ricochet').
card_uid('wild ricochet'/'CMD', 'CMD:Wild Ricochet:wild ricochet').
card_rarity('wild ricochet'/'CMD', 'Rare').
card_artist('wild ricochet'/'CMD', 'Dan Scott').
card_number('wild ricochet'/'CMD', '139').
card_flavor_text('wild ricochet'/'CMD', '\"I knew that trick long before your great-grandmother\'s great-grandmother was born.\"').
card_multiverse_id('wild ricochet'/'CMD', '247312').

card_in_set('windborn muse', 'CMD').
card_original_type('windborn muse'/'CMD', 'Creature — Spirit').
card_original_text('windborn muse'/'CMD', 'Flying\nCreatures can\'t attack you unless their controller pays {2} for each creature he or she controls that\'s attacking you.').
card_image_name('windborn muse'/'CMD', 'windborn muse').
card_uid('windborn muse'/'CMD', 'CMD:Windborn Muse:windborn muse').
card_rarity('windborn muse'/'CMD', 'Rare').
card_artist('windborn muse'/'CMD', 'Adam Rex').
card_number('windborn muse'/'CMD', '38').
card_flavor_text('windborn muse'/'CMD', '\"Her voice is justice, clear and relentless.\"\n—Akroma, angelic avenger').
card_multiverse_id('windborn muse'/'CMD', '247147').

card_in_set('windfall', 'CMD').
card_original_type('windfall'/'CMD', 'Sorcery').
card_original_text('windfall'/'CMD', 'Each player discards his or her hand, then draws cards equal to the greatest number of cards a player discarded this way.').
card_image_name('windfall'/'CMD', 'windfall').
card_uid('windfall'/'CMD', 'CMD:Windfall:windfall').
card_rarity('windfall'/'CMD', 'Uncommon').
card_artist('windfall'/'CMD', 'Pete Venters').
card_number('windfall'/'CMD', '70').
card_flavor_text('windfall'/'CMD', '\"To fill your mind with knowledge, we must start by emptying it.\"\n—Barrin, master wizard').
card_multiverse_id('windfall'/'CMD', '247532').

card_in_set('wonder', 'CMD').
card_original_type('wonder'/'CMD', 'Creature — Incarnation').
card_original_text('wonder'/'CMD', 'Flying\nAs long as Wonder is in your graveyard and you control an Island, creatures you control have flying.').
card_image_name('wonder'/'CMD', 'wonder').
card_uid('wonder'/'CMD', 'CMD:Wonder:wonder').
card_rarity('wonder'/'CMD', 'Uncommon').
card_artist('wonder'/'CMD', 'Rebecca Guay').
card_number('wonder'/'CMD', '71').
card_flavor_text('wonder'/'CMD', '\"The awestruck birds gazed at Wonder. Slowly, timidly, they rose into the air.\"\n—Scroll of Beginnings').
card_multiverse_id('wonder'/'CMD', '247293').

card_in_set('wrecking ball', 'CMD').
card_original_type('wrecking ball'/'CMD', 'Instant').
card_original_text('wrecking ball'/'CMD', 'Destroy target creature or land.').
card_image_name('wrecking ball'/'CMD', 'wrecking ball').
card_uid('wrecking ball'/'CMD', 'CMD:Wrecking Ball:wrecking ball').
card_rarity('wrecking ball'/'CMD', 'Common').
card_artist('wrecking ball'/'CMD', 'Ron Spears').
card_number('wrecking ball'/'CMD', '238').
card_flavor_text('wrecking ball'/'CMD', 'Rakdos festivals almost leave enough rubble in their wake to hide the bodies.').
card_multiverse_id('wrecking ball'/'CMD', '247199').

card_in_set('wrexial, the risen deep', 'CMD').
card_original_type('wrexial, the risen deep'/'CMD', 'Legendary Creature — Kraken').
card_original_text('wrexial, the risen deep'/'CMD', 'Islandwalk, swampwalk\nWhenever Wrexial, the Risen Deep deals combat damage to a player, you may cast target instant or sorcery card from that player\'s graveyard without paying its mana cost. If that card would be put into a graveyard this turn, exile it instead.').
card_image_name('wrexial, the risen deep'/'CMD', 'wrexial, the risen deep').
card_uid('wrexial, the risen deep'/'CMD', 'CMD:Wrexial, the Risen Deep:wrexial, the risen deep').
card_rarity('wrexial, the risen deep'/'CMD', 'Mythic Rare').
card_artist('wrexial, the risen deep'/'CMD', 'Eric Deschamps').
card_number('wrexial, the risen deep'/'CMD', '239').
card_multiverse_id('wrexial, the risen deep'/'CMD', '247542').

card_in_set('yavimaya elder', 'CMD').
card_original_type('yavimaya elder'/'CMD', 'Creature — Human Druid').
card_original_text('yavimaya elder'/'CMD', 'When Yavimaya Elder is put into a graveyard from the battlefield, you may search your library for up to two basic land cards, reveal them, and put them into your hand. If you do, shuffle your library.\n{2}, Sacrifice Yavimaya Elder: Draw a card.').
card_image_name('yavimaya elder'/'CMD', 'yavimaya elder').
card_uid('yavimaya elder'/'CMD', 'CMD:Yavimaya Elder:yavimaya elder').
card_rarity('yavimaya elder'/'CMD', 'Common').
card_artist('yavimaya elder'/'CMD', 'Matt Cavotta').
card_number('yavimaya elder'/'CMD', '179').
card_multiverse_id('yavimaya elder'/'CMD', '247524').

card_in_set('zedruu the greathearted', 'CMD').
card_original_type('zedruu the greathearted'/'CMD', 'Legendary Creature — Minotaur Monk').
card_original_text('zedruu the greathearted'/'CMD', 'At the beginning of your upkeep, you gain X life and draw X cards, where X is the number of permanents you own that your opponents control.\n{R}{W}{U}: Target opponent gains control of target permanent you control.').
card_first_print('zedruu the greathearted', 'CMD').
card_image_name('zedruu the greathearted'/'CMD', 'zedruu the greathearted').
card_uid('zedruu the greathearted'/'CMD', 'CMD:Zedruu the Greathearted:zedruu the greathearted').
card_rarity('zedruu the greathearted'/'CMD', 'Mythic Rare').
card_artist('zedruu the greathearted'/'CMD', 'Mark Zug').
card_number('zedruu the greathearted'/'CMD', '240').
card_multiverse_id('zedruu the greathearted'/'CMD', '236499').

card_in_set('zoetic cavern', 'CMD').
card_original_type('zoetic cavern'/'CMD', 'Land').
card_original_text('zoetic cavern'/'CMD', '{T}: Add {1} to your mana pool.\nMorph {2} (You may cast this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_image_name('zoetic cavern'/'CMD', 'zoetic cavern').
card_uid('zoetic cavern'/'CMD', 'CMD:Zoetic Cavern:zoetic cavern').
card_rarity('zoetic cavern'/'CMD', 'Uncommon').
card_artist('zoetic cavern'/'CMD', 'Lars Grant-West').
card_number('zoetic cavern'/'CMD', '298').
card_flavor_text('zoetic cavern'/'CMD', 'Nothing lives within it, yet there is life.').
card_multiverse_id('zoetic cavern'/'CMD', '247274').
