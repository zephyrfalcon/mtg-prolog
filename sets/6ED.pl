% Classic Sixth Edition

set('6ED').
set_name('6ED', 'Classic Sixth Edition').
set_release_date('6ED', '1999-04-21').
set_border('6ED', 'white').
set_type('6ED', 'core').

card_in_set('abduction', '6ED').
card_original_type('abduction'/'6ED', 'Enchant Creature').
card_original_text('abduction'/'6ED', 'When Abduction comes into play, untap enchanted creature.\nYou control enchanted creature.\nWhen enchanted creature is put into a graveyard, return that creature to play under its owner\'s control.').
card_image_name('abduction'/'6ED', 'abduction').
card_uid('abduction'/'6ED', '6ED:Abduction:abduction').
card_rarity('abduction'/'6ED', 'Uncommon').
card_artist('abduction'/'6ED', 'Colin MacNeil').
card_number('abduction'/'6ED', '55').
card_multiverse_id('abduction'/'6ED', '14526').

card_in_set('abyssal hunter', '6ED').
card_original_type('abyssal hunter'/'6ED', 'Creature — Minion').
card_original_text('abyssal hunter'/'6ED', '{B}, {T}: Tap target creature. Abyssal Hunter deals damage equal to its power to that creature.').
card_image_name('abyssal hunter'/'6ED', 'abyssal hunter').
card_uid('abyssal hunter'/'6ED', '6ED:Abyssal Hunter:abyssal hunter').
card_rarity('abyssal hunter'/'6ED', 'Rare').
card_artist('abyssal hunter'/'6ED', 'Steve Luke').
card_number('abyssal hunter'/'6ED', '109').
card_flavor_text('abyssal hunter'/'6ED', 'Some smiles show cheer; some merely show teeth.').
card_multiverse_id('abyssal hunter'/'6ED', '15383').

card_in_set('abyssal specter', '6ED').
card_original_type('abyssal specter'/'6ED', 'Creature — Specter').
card_original_text('abyssal specter'/'6ED', 'Flying\nWhenever Abyssal Specter deals damage to a player, that player chooses and discards a card from his or her hand.').
card_image_name('abyssal specter'/'6ED', 'abyssal specter').
card_uid('abyssal specter'/'6ED', '6ED:Abyssal Specter:abyssal specter').
card_rarity('abyssal specter'/'6ED', 'Uncommon').
card_artist('abyssal specter'/'6ED', 'George Pratt').
card_number('abyssal specter'/'6ED', '110').
card_multiverse_id('abyssal specter'/'6ED', '15381').

card_in_set('adarkar wastes', '6ED').
card_original_type('adarkar wastes'/'6ED', 'Land').
card_original_text('adarkar wastes'/'6ED', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {W} or {U} to your mana pool. Adarkar Wastes deals 1 damage to you.').
card_image_name('adarkar wastes'/'6ED', 'adarkar wastes').
card_uid('adarkar wastes'/'6ED', '6ED:Adarkar Wastes:adarkar wastes').
card_rarity('adarkar wastes'/'6ED', 'Rare').
card_artist('adarkar wastes'/'6ED', 'Gary Leach').
card_number('adarkar wastes'/'6ED', '319').
card_multiverse_id('adarkar wastes'/'6ED', '14727').

card_in_set('æther flash', '6ED').
card_original_type('æther flash'/'6ED', 'Enchantment').
card_original_text('æther flash'/'6ED', 'Whenever a creature comes into play, Æther Flash deals 2 damage to it.').
card_image_name('æther flash'/'6ED', 'aether flash').
card_uid('æther flash'/'6ED', '6ED:Æther Flash:aether flash').
card_rarity('æther flash'/'6ED', 'Uncommon').
card_artist('æther flash'/'6ED', 'Ron Spencer').
card_number('æther flash'/'6ED', '163').
card_flavor_text('æther flash'/'6ED', '\"You can\'t get there from here. Well, you can—it just won\'t be fun.\"\n—Ertai, wizard adept').
card_multiverse_id('æther flash'/'6ED', '15447').

card_in_set('agonizing memories', '6ED').
card_original_type('agonizing memories'/'6ED', 'Sorcery').
card_original_text('agonizing memories'/'6ED', 'Look at target player\'s hand and choose two cards from it. Put those cards on top of that player\'s library in any order you choose.').
card_image_name('agonizing memories'/'6ED', 'agonizing memories').
card_uid('agonizing memories'/'6ED', '6ED:Agonizing Memories:agonizing memories').
card_rarity('agonizing memories'/'6ED', 'Uncommon').
card_artist('agonizing memories'/'6ED', 'Mike Dringenberg').
card_number('agonizing memories'/'6ED', '111').
card_flavor_text('agonizing memories'/'6ED', '\"An innocent man died because of my anger. That knowledge will haunt me for all eternity.\"\n—Karn, silver golem').
card_multiverse_id('agonizing memories'/'6ED', '14586').

card_in_set('air elemental', '6ED').
card_original_type('air elemental'/'6ED', 'Creature — Elemental').
card_original_text('air elemental'/'6ED', 'Flying').
card_image_name('air elemental'/'6ED', 'air elemental').
card_uid('air elemental'/'6ED', '6ED:Air Elemental:air elemental').
card_rarity('air elemental'/'6ED', 'Uncommon').
card_artist('air elemental'/'6ED', 'Doug Chaffee').
card_number('air elemental'/'6ED', '56').
card_flavor_text('air elemental'/'6ED', 'As insubstantial, and as powerful, as the wind that carries it.').
card_multiverse_id('air elemental'/'6ED', '15371').

card_in_set('aladdin\'s ring', '6ED').
card_original_type('aladdin\'s ring'/'6ED', 'Artifact').
card_original_text('aladdin\'s ring'/'6ED', '{8}, {T}: Aladdin\'s Ring deals 4 damage to target creature or player.').
card_image_name('aladdin\'s ring'/'6ED', 'aladdin\'s ring').
card_uid('aladdin\'s ring'/'6ED', '6ED:Aladdin\'s Ring:aladdin\'s ring').
card_rarity('aladdin\'s ring'/'6ED', 'Rare').
card_artist('aladdin\'s ring'/'6ED', 'Stuart Griffin').
card_number('aladdin\'s ring'/'6ED', '271').
card_flavor_text('aladdin\'s ring'/'6ED', '\". . . The magician drew a ring off his finger . . . , saying: \'It is a talisman against all evil, so long as you obey me.\'\"\n—The Arabian Nights,\nJunior Classics trans.').
card_multiverse_id('aladdin\'s ring'/'6ED', '14770').

card_in_set('amber prison', '6ED').
card_original_type('amber prison'/'6ED', 'Artifact').
card_original_text('amber prison'/'6ED', 'You may choose not to untap Amber Prison during your untap step.\n{4}, {T}: Tap target artifact, creature, or land. As long as Amber Prison is tapped, that permanent doesn\'t untap during its controller\'s untap step.').
card_image_name('amber prison'/'6ED', 'amber prison').
card_uid('amber prison'/'6ED', '6ED:Amber Prison:amber prison').
card_rarity('amber prison'/'6ED', 'Rare').
card_artist('amber prison'/'6ED', 'Donato Giancola').
card_number('amber prison'/'6ED', '272').
card_multiverse_id('amber prison'/'6ED', '15403').

card_in_set('anaba bodyguard', '6ED').
card_original_type('anaba bodyguard'/'6ED', 'Creature — Minotaur').
card_original_text('anaba bodyguard'/'6ED', 'First strike').
card_image_name('anaba bodyguard'/'6ED', 'anaba bodyguard').
card_uid('anaba bodyguard'/'6ED', '6ED:Anaba Bodyguard:anaba bodyguard').
card_rarity('anaba bodyguard'/'6ED', 'Common').
card_artist('anaba bodyguard'/'6ED', 'Anson Maddocks').
card_number('anaba bodyguard'/'6ED', '164').
card_flavor_text('anaba bodyguard'/'6ED', '\"Not all minotaurs are tribal. Some are freelance.\"\n—Eron the Relentless').
card_multiverse_id('anaba bodyguard'/'6ED', '16444').

card_in_set('anaba shaman', '6ED').
card_original_type('anaba shaman'/'6ED', 'Creature — Minotaur').
card_original_text('anaba shaman'/'6ED', '{R}, {T}: Anaba Shaman deals 1 damage to target creature or player.').
card_image_name('anaba shaman'/'6ED', 'anaba shaman').
card_uid('anaba shaman'/'6ED', '6ED:Anaba Shaman:anaba shaman').
card_rarity('anaba shaman'/'6ED', 'Common').
card_artist('anaba shaman'/'6ED', 'Anson Maddocks').
card_number('anaba shaman'/'6ED', '165').
card_flavor_text('anaba shaman'/'6ED', '\"The shamans? Ha! They are craven cows not capable of true magic.\"\n—Irini Sengir').
card_multiverse_id('anaba shaman'/'6ED', '16441').

card_in_set('ancestral memories', '6ED').
card_original_type('ancestral memories'/'6ED', 'Sorcery').
card_original_text('ancestral memories'/'6ED', 'Look at the top seven cards of your library and put two of them into your hand. Put the rest into your graveyard.').
card_image_name('ancestral memories'/'6ED', 'ancestral memories').
card_uid('ancestral memories'/'6ED', '6ED:Ancestral Memories:ancestral memories').
card_rarity('ancestral memories'/'6ED', 'Rare').
card_artist('ancestral memories'/'6ED', 'William Donohoe').
card_number('ancestral memories'/'6ED', '57').
card_multiverse_id('ancestral memories'/'6ED', '14550').

card_in_set('animate wall', '6ED').
card_original_type('animate wall'/'6ED', 'Enchant Creature').
card_original_text('animate wall'/'6ED', 'Enchanted creature can attack as though it weren\'t a Wall.').
card_image_name('animate wall'/'6ED', 'animate wall').
card_uid('animate wall'/'6ED', '6ED:Animate Wall:animate wall').
card_rarity('animate wall'/'6ED', 'Rare').
card_artist('animate wall'/'6ED', 'Richard Kane Ferguson').
card_number('animate wall'/'6ED', '1').
card_flavor_text('animate wall'/'6ED', '\"When you have been bitten with fangs of granite, you start to long for the ivory sabers of tigers.\"\n—Norin the Wary').
card_multiverse_id('animate wall'/'6ED', '16428').

card_in_set('ankh of mishra', '6ED').
card_original_type('ankh of mishra'/'6ED', 'Artifact').
card_original_text('ankh of mishra'/'6ED', 'Whenever a land comes into play, Ankh of Mishra deals 2 damage to that land\'s controller.').
card_image_name('ankh of mishra'/'6ED', 'ankh of mishra').
card_uid('ankh of mishra'/'6ED', '6ED:Ankh of Mishra:ankh of mishra').
card_rarity('ankh of mishra'/'6ED', 'Rare').
card_artist('ankh of mishra'/'6ED', 'Ian Miller').
card_number('ankh of mishra'/'6ED', '273').
card_flavor_text('ankh of mishra'/'6ED', 'Tawnos finally cracked the puzzle: the strange structures did not house Mishra\'s malevolent creations but were themselves among his creations.').
card_multiverse_id('ankh of mishra'/'6ED', '14771').

card_in_set('archangel', '6ED').
card_original_type('archangel'/'6ED', 'Creature — Angel').
card_original_text('archangel'/'6ED', 'Flying\nAttacking doesn\'t cause Archangel to tap.').
card_image_name('archangel'/'6ED', 'archangel').
card_uid('archangel'/'6ED', '6ED:Archangel:archangel').
card_rarity('archangel'/'6ED', 'Rare').
card_artist('archangel'/'6ED', 'Quinton Hoover').
card_number('archangel'/'6ED', '2').
card_flavor_text('archangel'/'6ED', '\"My mother once told me angels sing their swords\' names with each strike.\"\n—Sisay, Captain of the Weatherlight').
card_multiverse_id('archangel'/'6ED', '14493').

card_in_set('ardent militia', '6ED').
card_original_type('ardent militia'/'6ED', 'Creature — Soldier').
card_original_text('ardent militia'/'6ED', 'Attacking doesn\'t cause Ardent Militia to tap.').
card_image_name('ardent militia'/'6ED', 'ardent militia').
card_uid('ardent militia'/'6ED', '6ED:Ardent Militia:ardent militia').
card_rarity('ardent militia'/'6ED', 'Uncommon').
card_artist('ardent militia'/'6ED', 'Zina Saunders').
card_number('ardent militia'/'6ED', '3').
card_flavor_text('ardent militia'/'6ED', 'Some fight for honor and some for gold, but the militia fight for hearth and home.').
card_multiverse_id('ardent militia'/'6ED', '14489').

card_in_set('armageddon', '6ED').
card_original_type('armageddon'/'6ED', 'Sorcery').
card_original_text('armageddon'/'6ED', 'Destroy all lands.').
card_image_name('armageddon'/'6ED', 'armageddon').
card_uid('armageddon'/'6ED', '6ED:Armageddon:armageddon').
card_rarity('armageddon'/'6ED', 'Rare').
card_artist('armageddon'/'6ED', 'Rob Alexander').
card_number('armageddon'/'6ED', '4').
card_multiverse_id('armageddon'/'6ED', '14500').

card_in_set('armored pegasus', '6ED').
card_original_type('armored pegasus'/'6ED', 'Creature — Pegasus').
card_original_text('armored pegasus'/'6ED', 'Flying').
card_image_name('armored pegasus'/'6ED', 'armored pegasus').
card_uid('armored pegasus'/'6ED', '6ED:Armored Pegasus:armored pegasus').
card_rarity('armored pegasus'/'6ED', 'Common').
card_artist('armored pegasus'/'6ED', 'Andrew Robinson').
card_number('armored pegasus'/'6ED', '5').
card_flavor_text('armored pegasus'/'6ED', 'Asked how it survived a run-in with a bog imp, the pegasus just shook its mane and burped.').
card_multiverse_id('armored pegasus'/'6ED', '14469').

card_in_set('ashen powder', '6ED').
card_original_type('ashen powder'/'6ED', 'Sorcery').
card_original_text('ashen powder'/'6ED', 'Put target creature card from one of your opponents\' graveyards into play under your control.').
card_image_name('ashen powder'/'6ED', 'ashen powder').
card_uid('ashen powder'/'6ED', '6ED:Ashen Powder:ashen powder').
card_rarity('ashen powder'/'6ED', 'Rare').
card_artist('ashen powder'/'6ED', 'Geofrey Darrow').
card_number('ashen powder'/'6ED', '112').
card_flavor_text('ashen powder'/'6ED', '\"What you call decay, I call ripening. You\'ll gain no sustenance from this harvest.\"\n—Kaervek').
card_multiverse_id('ashen powder'/'6ED', '15384').

card_in_set('ashnod\'s altar', '6ED').
card_original_type('ashnod\'s altar'/'6ED', 'Artifact').
card_original_text('ashnod\'s altar'/'6ED', 'Sacrifice a creature: Add two colorless mana to your mana pool.').
card_image_name('ashnod\'s altar'/'6ED', 'ashnod\'s altar').
card_uid('ashnod\'s altar'/'6ED', '6ED:Ashnod\'s Altar:ashnod\'s altar').
card_rarity('ashnod\'s altar'/'6ED', 'Uncommon').
card_artist('ashnod\'s altar'/'6ED', 'Anson Maddocks').
card_number('ashnod\'s altar'/'6ED', '274').
card_flavor_text('ashnod\'s altar'/'6ED', 'The Brothers\' War scarred the land, but Ashnod left her mark in the bloodlines of those she tortured.').
card_multiverse_id('ashnod\'s altar'/'6ED', '11158').

card_in_set('balduvian barbarians', '6ED').
card_original_type('balduvian barbarians'/'6ED', 'Creature — Barbarian').
card_original_text('balduvian barbarians'/'6ED', '').
card_image_name('balduvian barbarians'/'6ED', 'balduvian barbarians').
card_uid('balduvian barbarians'/'6ED', '6ED:Balduvian Barbarians:balduvian barbarians').
card_rarity('balduvian barbarians'/'6ED', 'Common').
card_artist('balduvian barbarians'/'6ED', 'Mark Poole').
card_number('balduvian barbarians'/'6ED', '166').
card_flavor_text('balduvian barbarians'/'6ED', '\"Barbarian raids were a concern to those living in the northwest provinces, but the skyknights never dealt with the problem in a systematic way. They thought of the Balduvians as an ‘amusing model\' of their forebears\' culture.\"\n—Kjeldor: Ice Civilization').
card_multiverse_id('balduvian barbarians'/'6ED', '16445').

card_in_set('balduvian horde', '6ED').
card_original_type('balduvian horde'/'6ED', 'Creature — Barbarian').
card_original_text('balduvian horde'/'6ED', 'When Balduvian Horde comes into play, discard a card at random from your hand. If you don\'t, sacrifice Balduvian Horde.').
card_image_name('balduvian horde'/'6ED', 'balduvian horde').
card_uid('balduvian horde'/'6ED', '6ED:Balduvian Horde:balduvian horde').
card_rarity('balduvian horde'/'6ED', 'Rare').
card_artist('balduvian horde'/'6ED', 'Brian Snõddy').
card_number('balduvian horde'/'6ED', '167').
card_multiverse_id('balduvian horde'/'6ED', '15423').

card_in_set('birds of paradise', '6ED').
card_original_type('birds of paradise'/'6ED', 'Creature — Bird').
card_original_text('birds of paradise'/'6ED', 'Flying\n{T}: Add one mana of a color of your choice to your mana pool.').
card_image_name('birds of paradise'/'6ED', 'birds of paradise').
card_uid('birds of paradise'/'6ED', '6ED:Birds of Paradise:birds of paradise').
card_rarity('birds of paradise'/'6ED', 'Rare').
card_artist('birds of paradise'/'6ED', 'Mark Poole').
card_number('birds of paradise'/'6ED', '217').
card_multiverse_id('birds of paradise'/'6ED', '14719').

card_in_set('blaze', '6ED').
card_original_type('blaze'/'6ED', 'Sorcery').
card_original_text('blaze'/'6ED', 'Blaze deals X damage to target creature or player.').
card_image_name('blaze'/'6ED', 'blaze').
card_uid('blaze'/'6ED', '6ED:Blaze:blaze').
card_rarity('blaze'/'6ED', 'Uncommon').
card_artist('blaze'/'6ED', 'Gerry Grace').
card_number('blaze'/'6ED', '168').
card_flavor_text('blaze'/'6ED', 'Fire never dies alone.').
card_multiverse_id('blaze'/'6ED', '14628').

card_in_set('blight', '6ED').
card_original_type('blight'/'6ED', 'Enchant Land').
card_original_text('blight'/'6ED', 'When enchanted land is tapped, destroy it.').
card_image_name('blight'/'6ED', 'blight').
card_uid('blight'/'6ED', '6ED:Blight:blight').
card_rarity('blight'/'6ED', 'Uncommon').
card_artist('blight'/'6ED', 'Ian Miller').
card_number('blight'/'6ED', '113').
card_multiverse_id('blight'/'6ED', '15399').

card_in_set('blighted shaman', '6ED').
card_original_type('blighted shaman'/'6ED', 'Creature — Wizard').
card_original_text('blighted shaman'/'6ED', '{T}, Sacrifice a swamp: Target creature gets +1/+1 until end of turn.\n{T}, Sacrifice a creature: Target creature gets +2/+2 until end of turn.').
card_image_name('blighted shaman'/'6ED', 'blighted shaman').
card_uid('blighted shaman'/'6ED', '6ED:Blighted Shaman:blighted shaman').
card_rarity('blighted shaman'/'6ED', 'Uncommon').
card_artist('blighted shaman'/'6ED', 'Ian Miller').
card_number('blighted shaman'/'6ED', '114').
card_multiverse_id('blighted shaman'/'6ED', '15385').

card_in_set('blood pet', '6ED').
card_original_type('blood pet'/'6ED', 'Creature — Thrull').
card_original_text('blood pet'/'6ED', 'Sacrifice Blood Pet: Add {B} to your mana pool.').
card_image_name('blood pet'/'6ED', 'blood pet').
card_uid('blood pet'/'6ED', '6ED:Blood Pet:blood pet').
card_rarity('blood pet'/'6ED', 'Common').
card_artist('blood pet'/'6ED', 'Brom').
card_number('blood pet'/'6ED', '115').
card_flavor_text('blood pet'/'6ED', '\"You are wrong,\" Volrath said. \"I do not hate the living. They often prove quite useful to me.\" And then he laughed.').
card_multiverse_id('blood pet'/'6ED', '15400').

card_in_set('bog imp', '6ED').
card_original_type('bog imp'/'6ED', 'Creature — Imp').
card_original_text('bog imp'/'6ED', 'Flying').
card_image_name('bog imp'/'6ED', 'bog imp').
card_uid('bog imp'/'6ED', '6ED:Bog Imp:bog imp').
card_rarity('bog imp'/'6ED', 'Common').
card_artist('bog imp'/'6ED', 'Christopher Rush').
card_number('bog imp'/'6ED', '116').
card_flavor_text('bog imp'/'6ED', 'Don\'t be fooled by their looks. Think of them as little knives with wings.').
card_multiverse_id('bog imp'/'6ED', '16626').

card_in_set('bog rats', '6ED').
card_original_type('bog rats'/'6ED', 'Creature — Rat').
card_original_text('bog rats'/'6ED', 'Bog Rats can\'t be blocked by Walls.').
card_image_name('bog rats'/'6ED', 'bog rats').
card_uid('bog rats'/'6ED', '6ED:Bog Rats:bog rats').
card_rarity('bog rats'/'6ED', 'Common').
card_artist('bog rats'/'6ED', 'Ron Spencer').
card_number('bog rats'/'6ED', '117').
card_flavor_text('bog rats'/'6ED', 'Their stench was vile and strong enough, but not nearly as powerful as their hunger.').
card_multiverse_id('bog rats'/'6ED', '11180').

card_in_set('bog wraith', '6ED').
card_original_type('bog wraith'/'6ED', 'Creature — Wraith').
card_original_text('bog wraith'/'6ED', 'Swampwalk (This creature is unblockable if defending player controls a swamp.)').
card_image_name('bog wraith'/'6ED', 'bog wraith').
card_uid('bog wraith'/'6ED', '6ED:Bog Wraith:bog wraith').
card_rarity('bog wraith'/'6ED', 'Uncommon').
card_artist('bog wraith'/'6ED', 'Jeff A. Menges').
card_number('bog wraith'/'6ED', '118').
card_flavor_text('bog wraith'/'6ED', 'It moved through the troops leaving no footprints, save on their souls.').
card_multiverse_id('bog wraith'/'6ED', '14589').

card_in_set('boil', '6ED').
card_original_type('boil'/'6ED', 'Instant').
card_original_text('boil'/'6ED', 'Destroy all islands.').
card_image_name('boil'/'6ED', 'boil').
card_uid('boil'/'6ED', '6ED:Boil:boil').
card_rarity('boil'/'6ED', 'Uncommon').
card_artist('boil'/'6ED', 'Jason Alexander Behnke').
card_number('boil'/'6ED', '169').
card_flavor_text('boil'/'6ED', '\"The fishers will throw out their nets and draw them back filled with dust.\"\n—Oracle en-Vec').
card_multiverse_id('boil'/'6ED', '14630').

card_in_set('boomerang', '6ED').
card_original_type('boomerang'/'6ED', 'Instant').
card_original_text('boomerang'/'6ED', 'Return target permanent to its owner\'s hand.').
card_image_name('boomerang'/'6ED', 'boomerang').
card_uid('boomerang'/'6ED', '6ED:Boomerang:boomerang').
card_rarity('boomerang'/'6ED', 'Common').
card_artist('boomerang'/'6ED', 'Richard Kane Ferguson').
card_number('boomerang'/'6ED', '58').
card_flavor_text('boomerang'/'6ED', 'A lie always returns; be careful how you catch it.').
card_multiverse_id('boomerang'/'6ED', '14513').

card_in_set('bottle of suleiman', '6ED').
card_original_type('bottle of suleiman'/'6ED', 'Artifact').
card_original_text('bottle of suleiman'/'6ED', '{1}, Sacrifice Bottle of Suleiman: Flip a coin. If you lose the flip, Bottle of Suleiman deals 5 damage to you. If you win the flip, put a 5/5 Djinn artifact creature token into play. That creature has flying.').
card_image_name('bottle of suleiman'/'6ED', 'bottle of suleiman').
card_uid('bottle of suleiman'/'6ED', '6ED:Bottle of Suleiman:bottle of suleiman').
card_rarity('bottle of suleiman'/'6ED', 'Rare').
card_artist('bottle of suleiman'/'6ED', 'DiTerlizzi').
card_number('bottle of suleiman'/'6ED', '275').
card_multiverse_id('bottle of suleiman'/'6ED', '11183').

card_in_set('browse', '6ED').
card_original_type('browse'/'6ED', 'Enchantment').
card_original_text('browse'/'6ED', '{2}{U}{U} Look at the top five cards of your library and put one of them into your hand. Remove the rest from the game.').
card_image_name('browse'/'6ED', 'browse').
card_uid('browse'/'6ED', '6ED:Browse:browse').
card_rarity('browse'/'6ED', 'Uncommon').
card_artist('browse'/'6ED', 'Phil Foglio').
card_number('browse'/'6ED', '59').
card_flavor_text('browse'/'6ED', '\"Once great literature—now great litter.\"\n—Jaya Ballard, task mage').
card_multiverse_id('browse'/'6ED', '16434').

card_in_set('brushland', '6ED').
card_original_type('brushland'/'6ED', 'Land').
card_original_text('brushland'/'6ED', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {G} or {W} to your mana pool. Brushland deals 1 damage to you.').
card_image_name('brushland'/'6ED', 'brushland').
card_uid('brushland'/'6ED', '6ED:Brushland:brushland').
card_rarity('brushland'/'6ED', 'Rare').
card_artist('brushland'/'6ED', 'Tom Wänerstrand').
card_number('brushland'/'6ED', '320').
card_multiverse_id('brushland'/'6ED', '14731').

card_in_set('burrowing', '6ED').
card_original_type('burrowing'/'6ED', 'Enchant Creature').
card_original_text('burrowing'/'6ED', 'Enchanted creature gains mountainwalk. (It\'s unblockable if defending player controls a mountain.)').
card_image_name('burrowing'/'6ED', 'burrowing').
card_uid('burrowing'/'6ED', '6ED:Burrowing:burrowing').
card_rarity('burrowing'/'6ED', 'Uncommon').
card_artist('burrowing'/'6ED', 'Mark Poole').
card_number('burrowing'/'6ED', '170').
card_multiverse_id('burrowing'/'6ED', '16448').

card_in_set('call of the wild', '6ED').
card_original_type('call of the wild'/'6ED', 'Enchantment').
card_original_text('call of the wild'/'6ED', '{2}{G}{G}: Reveal the top card of your library. If it\'s a creature card, put it into play. Otherwise, put it into your graveyard.').
card_image_name('call of the wild'/'6ED', 'call of the wild').
card_uid('call of the wild'/'6ED', '6ED:Call of the Wild:call of the wild').
card_rarity('call of the wild'/'6ED', 'Rare').
card_artist('call of the wild'/'6ED', 'Brom').
card_number('call of the wild'/'6ED', '218').
card_flavor_text('call of the wild'/'6ED', 'Thinking of dinner made Squee realize that the forest was probably thinking the same thing.').
card_multiverse_id('call of the wild'/'6ED', '15433').

card_in_set('castle', '6ED').
card_original_type('castle'/'6ED', 'Enchantment').
card_original_text('castle'/'6ED', 'Untapped creatures you control get +0/+2.').
card_image_name('castle'/'6ED', 'castle').
card_uid('castle'/'6ED', '6ED:Castle:castle').
card_rarity('castle'/'6ED', 'Uncommon').
card_artist('castle'/'6ED', 'Dameon Willich').
card_number('castle'/'6ED', '6').
card_flavor_text('castle'/'6ED', '\"Hang out our banners on the outward walls; / The cry is still, ‘They come\'; our castle\'s strength / Will laugh a siege to scorn.\"\n—William Shakespeare, Macbeth').
card_multiverse_id('castle'/'6ED', '14478').

card_in_set('cat warriors', '6ED').
card_original_type('cat warriors'/'6ED', 'Creature — Cat Warrior').
card_original_text('cat warriors'/'6ED', 'Forestwalk (This creature is unblockable if defending player controls a forest.)').
card_image_name('cat warriors'/'6ED', 'cat warriors').
card_uid('cat warriors'/'6ED', '6ED:Cat Warriors:cat warriors').
card_rarity('cat warriors'/'6ED', 'Common').
card_artist('cat warriors'/'6ED', 'Melissa A. Benson').
card_number('cat warriors'/'6ED', '219').
card_flavor_text('cat warriors'/'6ED', 'These stealthy felines have survived so many battles that some believe they must possess many lives.').
card_multiverse_id('cat warriors'/'6ED', '16450').

card_in_set('celestial dawn', '6ED').
card_original_type('celestial dawn'/'6ED', 'Enchantment').
card_original_text('celestial dawn'/'6ED', 'Nonland cards you own that aren\'t in play are white. Nonland permanents you control are white. Lands you control are plains. Colored mana symbols in the costs on all those cards and permanents are {W}.').
card_image_name('celestial dawn'/'6ED', 'celestial dawn').
card_uid('celestial dawn'/'6ED', '6ED:Celestial Dawn:celestial dawn').
card_rarity('celestial dawn'/'6ED', 'Rare').
card_artist('celestial dawn'/'6ED', 'Liz Danforth').
card_number('celestial dawn'/'6ED', '7').
card_multiverse_id('celestial dawn'/'6ED', '15354').

card_in_set('charcoal diamond', '6ED').
card_original_type('charcoal diamond'/'6ED', 'Artifact').
card_original_text('charcoal diamond'/'6ED', 'Charcoal Diamond comes into play tapped.\n{T}: Add {B} to your mana pool.').
card_image_name('charcoal diamond'/'6ED', 'charcoal diamond').
card_uid('charcoal diamond'/'6ED', '6ED:Charcoal Diamond:charcoal diamond').
card_rarity('charcoal diamond'/'6ED', 'Uncommon').
card_artist('charcoal diamond'/'6ED', 'Drew Tucker').
card_number('charcoal diamond'/'6ED', '276').
card_multiverse_id('charcoal diamond'/'6ED', '15440').

card_in_set('chill', '6ED').
card_original_type('chill'/'6ED', 'Enchantment').
card_original_text('chill'/'6ED', 'Red spells cost {2} more to play.').
card_image_name('chill'/'6ED', 'chill').
card_uid('chill'/'6ED', '6ED:Chill:chill').
card_rarity('chill'/'6ED', 'Uncommon').
card_artist('chill'/'6ED', 'Greg Simanson').
card_number('chill'/'6ED', '60').
card_flavor_text('chill'/'6ED', '\"Temper, temper.\"\n—Ertai, wizard adept').
card_multiverse_id('chill'/'6ED', '15444').

card_in_set('circle of protection: black', '6ED').
card_original_type('circle of protection: black'/'6ED', 'Enchantment').
card_original_text('circle of protection: black'/'6ED', '{1}: The next time a black source of your choice would deal damage to you this turn, prevent that damage.').
card_image_name('circle of protection: black'/'6ED', 'circle of protection black').
card_uid('circle of protection: black'/'6ED', '6ED:Circle of Protection: Black:circle of protection black').
card_rarity('circle of protection: black'/'6ED', 'Common').
card_artist('circle of protection: black'/'6ED', 'Gerry Grace').
card_number('circle of protection: black'/'6ED', '8').
card_multiverse_id('circle of protection: black'/'6ED', '14457').

card_in_set('circle of protection: blue', '6ED').
card_original_type('circle of protection: blue'/'6ED', 'Enchantment').
card_original_text('circle of protection: blue'/'6ED', '{1}: The next time a blue source of your choice would deal damage to you this turn, prevent that damage.').
card_image_name('circle of protection: blue'/'6ED', 'circle of protection blue').
card_uid('circle of protection: blue'/'6ED', '6ED:Circle of Protection: Blue:circle of protection blue').
card_rarity('circle of protection: blue'/'6ED', 'Common').
card_artist('circle of protection: blue'/'6ED', 'Gerry Grace').
card_number('circle of protection: blue'/'6ED', '9').
card_multiverse_id('circle of protection: blue'/'6ED', '14456').

card_in_set('circle of protection: green', '6ED').
card_original_type('circle of protection: green'/'6ED', 'Enchantment').
card_original_text('circle of protection: green'/'6ED', '{1}: The next time a green source of your choice would deal damage to you this turn, prevent that damage.').
card_image_name('circle of protection: green'/'6ED', 'circle of protection green').
card_uid('circle of protection: green'/'6ED', '6ED:Circle of Protection: Green:circle of protection green').
card_rarity('circle of protection: green'/'6ED', 'Common').
card_artist('circle of protection: green'/'6ED', 'Gerry Grace').
card_number('circle of protection: green'/'6ED', '10').
card_multiverse_id('circle of protection: green'/'6ED', '14459').

card_in_set('circle of protection: red', '6ED').
card_original_type('circle of protection: red'/'6ED', 'Enchantment').
card_original_text('circle of protection: red'/'6ED', '{1}: The next time a red source of your choice would deal damage to you this turn, prevent that damage.').
card_image_name('circle of protection: red'/'6ED', 'circle of protection red').
card_uid('circle of protection: red'/'6ED', '6ED:Circle of Protection: Red:circle of protection red').
card_rarity('circle of protection: red'/'6ED', 'Common').
card_artist('circle of protection: red'/'6ED', 'Gerry Grace').
card_number('circle of protection: red'/'6ED', '11').
card_multiverse_id('circle of protection: red'/'6ED', '14458').

card_in_set('circle of protection: white', '6ED').
card_original_type('circle of protection: white'/'6ED', 'Enchantment').
card_original_text('circle of protection: white'/'6ED', '{1}: The next time a white source of your choice would deal damage to you this turn, prevent that damage.').
card_image_name('circle of protection: white'/'6ED', 'circle of protection white').
card_uid('circle of protection: white'/'6ED', '6ED:Circle of Protection: White:circle of protection white').
card_rarity('circle of protection: white'/'6ED', 'Common').
card_artist('circle of protection: white'/'6ED', 'Gerry Grace').
card_number('circle of protection: white'/'6ED', '12').
card_multiverse_id('circle of protection: white'/'6ED', '14455').

card_in_set('city of brass', '6ED').
card_original_type('city of brass'/'6ED', 'Land').
card_original_text('city of brass'/'6ED', 'Whenever City of Brass is tapped, it deals 1 damage to you.\n{T}: Add one mana of a color of your choice to your mana pool.').
card_image_name('city of brass'/'6ED', 'city of brass').
card_uid('city of brass'/'6ED', '6ED:City of Brass:city of brass').
card_rarity('city of brass'/'6ED', 'Rare').
card_artist('city of brass'/'6ED', 'Tom Wänerstrand').
card_number('city of brass'/'6ED', '321').
card_multiverse_id('city of brass'/'6ED', '14732').

card_in_set('coercion', '6ED').
card_original_type('coercion'/'6ED', 'Sorcery').
card_original_text('coercion'/'6ED', 'Look at target opponent\'s hand and choose a card from it. That player discards that card.').
card_image_name('coercion'/'6ED', 'coercion').
card_uid('coercion'/'6ED', '6ED:Coercion:coercion').
card_rarity('coercion'/'6ED', 'Common').
card_artist('coercion'/'6ED', 'DiTerlizzi').
card_number('coercion'/'6ED', '119').
card_flavor_text('coercion'/'6ED', 'A rhino\'s bargain\n—Femeref expression meaning\n\"a situation with no choices\"').
card_multiverse_id('coercion'/'6ED', '14561').

card_in_set('conquer', '6ED').
card_original_type('conquer'/'6ED', 'Enchant Land').
card_original_text('conquer'/'6ED', 'You control enchanted land.').
card_image_name('conquer'/'6ED', 'conquer').
card_uid('conquer'/'6ED', '6ED:Conquer:conquer').
card_rarity('conquer'/'6ED', 'Uncommon').
card_artist('conquer'/'6ED', 'Randy Gallegos').
card_number('conquer'/'6ED', '171').
card_flavor_text('conquer'/'6ED', '\"Why do we trade with those despicable elves? You don\'t live in forests, you burn them!\"\n—Avram Garrisson,\nLeader of the Knights of Stromgald').
card_multiverse_id('conquer'/'6ED', '11212').

card_in_set('counterspell', '6ED').
card_original_type('counterspell'/'6ED', 'Instant').
card_original_text('counterspell'/'6ED', 'Counter target spell.').
card_image_name('counterspell'/'6ED', 'counterspell').
card_uid('counterspell'/'6ED', '6ED:Counterspell:counterspell').
card_rarity('counterspell'/'6ED', 'Common').
card_artist('counterspell'/'6ED', 'Hannibal King').
card_number('counterspell'/'6ED', '61').
card_multiverse_id('counterspell'/'6ED', '14511').

card_in_set('creeping mold', '6ED').
card_original_type('creeping mold'/'6ED', 'Sorcery').
card_original_text('creeping mold'/'6ED', 'Destroy target artifact, land, or enchantment.').
card_image_name('creeping mold'/'6ED', 'creeping mold').
card_uid('creeping mold'/'6ED', '6ED:Creeping Mold:creeping mold').
card_rarity('creeping mold'/'6ED', 'Uncommon').
card_artist('creeping mold'/'6ED', 'David Seeley').
card_number('creeping mold'/'6ED', '220').
card_flavor_text('creeping mold'/'6ED', '\"Mold could catch you.\"\n—Suq\'Ata insult').
card_multiverse_id('creeping mold'/'6ED', '15428').

card_in_set('crimson hellkite', '6ED').
card_original_type('crimson hellkite'/'6ED', 'Creature — Dragon').
card_original_text('crimson hellkite'/'6ED', 'Flying\n{X}, {T}: Crimson Hellkite deals X damage to target creature. Spend only red mana this way.').
card_image_name('crimson hellkite'/'6ED', 'crimson hellkite').
card_uid('crimson hellkite'/'6ED', '6ED:Crimson Hellkite:crimson hellkite').
card_rarity('crimson hellkite'/'6ED', 'Rare').
card_artist('crimson hellkite'/'6ED', 'Gerry Grace').
card_number('crimson hellkite'/'6ED', '172').
card_flavor_text('crimson hellkite'/'6ED', '\"Dragonfire forged me a warrior.\"\n—Rashida Scalebane').
card_multiverse_id('crimson hellkite'/'6ED', '15424').

card_in_set('crusade', '6ED').
card_original_type('crusade'/'6ED', 'Enchantment').
card_original_text('crusade'/'6ED', 'White creatures get +1/+1.').
card_image_name('crusade'/'6ED', 'crusade').
card_uid('crusade'/'6ED', '6ED:Crusade:crusade').
card_rarity('crusade'/'6ED', 'Rare').
card_artist('crusade'/'6ED', 'D. Alexander Gregory').
card_number('crusade'/'6ED', '13').
card_multiverse_id('crusade'/'6ED', '14503').

card_in_set('crystal rod', '6ED').
card_original_type('crystal rod'/'6ED', 'Artifact').
card_original_text('crystal rod'/'6ED', 'Whenever a player plays a blue spell, you may pay {1}. If you do, you gain 1 life.').
card_image_name('crystal rod'/'6ED', 'crystal rod').
card_uid('crystal rod'/'6ED', '6ED:Crystal Rod:crystal rod').
card_rarity('crystal rod'/'6ED', 'Uncommon').
card_artist('crystal rod'/'6ED', 'Donato Giancola').
card_number('crystal rod'/'6ED', '277').
card_multiverse_id('crystal rod'/'6ED', '14758').

card_in_set('crystal vein', '6ED').
card_original_type('crystal vein'/'6ED', 'Land').
card_original_text('crystal vein'/'6ED', '{T}: Add one colorless mana to your mana pool.\n{T}, Sacrifice Crystal Vein: Add two colorless mana to your mana pool.').
card_image_name('crystal vein'/'6ED', 'crystal vein').
card_uid('crystal vein'/'6ED', '6ED:Crystal Vein:crystal vein').
card_rarity('crystal vein'/'6ED', 'Uncommon').
card_artist('crystal vein'/'6ED', 'Pat Morrissey').
card_number('crystal vein'/'6ED', '322').
card_multiverse_id('crystal vein'/'6ED', '15413').

card_in_set('cursed totem', '6ED').
card_original_type('cursed totem'/'6ED', 'Artifact').
card_original_text('cursed totem'/'6ED', 'Players can\'t play activated abilities of creatures.').
card_image_name('cursed totem'/'6ED', 'cursed totem').
card_uid('cursed totem'/'6ED', '6ED:Cursed Totem:cursed totem').
card_rarity('cursed totem'/'6ED', 'Rare').
card_artist('cursed totem'/'6ED', 'D. Alexander Gregory').
card_number('cursed totem'/'6ED', '278').
card_flavor_text('cursed totem'/'6ED', 'Pass me from soul to soul, / soldier to herder, herder to beast, beast to soil / until I am everywhere. / Then pass me those souls.\n—Totem inscription (translated)').
card_multiverse_id('cursed totem'/'6ED', '15404').

card_in_set('d\'avenant archer', '6ED').
card_original_type('d\'avenant archer'/'6ED', 'Creature — Soldier').
card_original_text('d\'avenant archer'/'6ED', '{T}: D\'Avenant Archer deals 1 damage to target attacking or blocking creature.').
card_image_name('d\'avenant archer'/'6ED', 'd\'avenant archer').
card_uid('d\'avenant archer'/'6ED', '6ED:D\'Avenant Archer:d\'avenant archer').
card_rarity('d\'avenant archer'/'6ED', 'Common').
card_artist('d\'avenant archer'/'6ED', 'Douglas Shuler').
card_number('d\'avenant archer'/'6ED', '14').
card_flavor_text('d\'avenant archer'/'6ED', 'Avenant\'s archers are also trained as poets, so that each arrow is guided by a fragment of verse.').
card_multiverse_id('d\'avenant archer'/'6ED', '14472').

card_in_set('dancing scimitar', '6ED').
card_original_type('dancing scimitar'/'6ED', 'Artifact Creature').
card_original_text('dancing scimitar'/'6ED', 'Flying').
card_image_name('dancing scimitar'/'6ED', 'dancing scimitar').
card_uid('dancing scimitar'/'6ED', '6ED:Dancing Scimitar:dancing scimitar').
card_rarity('dancing scimitar'/'6ED', 'Rare').
card_artist('dancing scimitar'/'6ED', 'Anson Maddocks').
card_number('dancing scimitar'/'6ED', '279').
card_flavor_text('dancing scimitar'/'6ED', 'Bobbing merrily from opponent to opponent, the scimitar began adding playful little flourishes to its strokes; it even turned a couple of somersaults.').
card_multiverse_id('dancing scimitar'/'6ED', '14773').

card_in_set('daraja griffin', '6ED').
card_original_type('daraja griffin'/'6ED', 'Creature — Griffin').
card_original_text('daraja griffin'/'6ED', 'Flying\nSacrifice Daraja Griffin: Destroy target black creature.').
card_image_name('daraja griffin'/'6ED', 'daraja griffin').
card_uid('daraja griffin'/'6ED', '6ED:Daraja Griffin:daraja griffin').
card_rarity('daraja griffin'/'6ED', 'Uncommon').
card_artist('daraja griffin'/'6ED', 'Stuart Griffin').
card_number('daraja griffin'/'6ED', '15').
card_flavor_text('daraja griffin'/'6ED', '\"And the flamingos said, ‘Get out of our nest—we can\'t be seen with the likes of you!\' So, the griffin ate them.\"\n—Azeworai, \"The Ugly Bird\"').
card_multiverse_id('daraja griffin'/'6ED', '15358').

card_in_set('daring apprentice', '6ED').
card_original_type('daring apprentice'/'6ED', 'Creature — Wizard').
card_original_text('daring apprentice'/'6ED', '{T}, Sacrifice Daring Apprentice: Counter target spell.').
card_image_name('daring apprentice'/'6ED', 'daring apprentice').
card_uid('daring apprentice'/'6ED', '6ED:Daring Apprentice:daring apprentice').
card_rarity('daring apprentice'/'6ED', 'Rare').
card_artist('daring apprentice'/'6ED', 'Kaja Foglio').
card_number('daring apprentice'/'6ED', '62').
card_flavor_text('daring apprentice'/'6ED', 'In front of every great wizard is a doomed apprentice.').
card_multiverse_id('daring apprentice'/'6ED', '14552').

card_in_set('deflection', '6ED').
card_original_type('deflection'/'6ED', 'Instant').
card_original_text('deflection'/'6ED', 'Choose a new target for target spell with a single target.').
card_image_name('deflection'/'6ED', 'deflection').
card_uid('deflection'/'6ED', '6ED:Deflection:deflection').
card_rarity('deflection'/'6ED', 'Rare').
card_artist('deflection'/'6ED', 'Mike Raabe').
card_number('deflection'/'6ED', '63').
card_flavor_text('deflection'/'6ED', 'Up and down,\nover and through,\nback around—\nthe joke\'s on you.').
card_multiverse_id('deflection'/'6ED', '16438').

card_in_set('dense foliage', '6ED').
card_original_type('dense foliage'/'6ED', 'Enchantment').
card_original_text('dense foliage'/'6ED', 'Creatures can\'t be the targets of spells.').
card_image_name('dense foliage'/'6ED', 'dense foliage').
card_uid('dense foliage'/'6ED', '6ED:Dense Foliage:dense foliage').
card_rarity('dense foliage'/'6ED', 'Rare').
card_artist('dense foliage'/'6ED', 'Alan Rabinowitz').
card_number('dense foliage'/'6ED', '221').
card_flavor_text('dense foliage'/'6ED', '\"Big plants not only good for hidin\', but full o\' tasty bugs, too.\"\n—Squee, goblin cabin hand').
card_multiverse_id('dense foliage'/'6ED', '15431').

card_in_set('derelor', '6ED').
card_original_type('derelor'/'6ED', 'Creature — Thrull').
card_original_text('derelor'/'6ED', 'Your black spells cost {B} more to play.').
card_image_name('derelor'/'6ED', 'derelor').
card_uid('derelor'/'6ED', '6ED:Derelor:derelor').
card_rarity('derelor'/'6ED', 'Rare').
card_artist('derelor'/'6ED', 'Anson Maddocks').
card_number('derelor'/'6ED', '120').
card_flavor_text('derelor'/'6ED', '\"Strength it has, but at the cost of a continuous supply of energy. Such failure can bear only one result.\"\n—Execution order of Endrek Sahr,\nmaster breeder').
card_multiverse_id('derelor'/'6ED', '11233').

card_in_set('desertion', '6ED').
card_original_type('desertion'/'6ED', 'Instant').
card_original_text('desertion'/'6ED', 'Counter target spell. If it\'s an artifact or creature card, put it into play under your control instead of into its owner\'s graveyard.').
card_image_name('desertion'/'6ED', 'desertion').
card_uid('desertion'/'6ED', '6ED:Desertion:desertion').
card_rarity('desertion'/'6ED', 'Rare').
card_artist('desertion'/'6ED', 'Richard Kane Ferguson').
card_number('desertion'/'6ED', '64').
card_flavor_text('desertion'/'6ED', 'First the insult, then the injury.').
card_multiverse_id('desertion'/'6ED', '15370').

card_in_set('diminishing returns', '6ED').
card_original_type('diminishing returns'/'6ED', 'Sorcery').
card_original_text('diminishing returns'/'6ED', 'Each player shuffles his or her hand and graveyard into his or her library. You remove the top ten cards of your library from the game. Then each player draws up to seven cards.').
card_image_name('diminishing returns'/'6ED', 'diminishing returns').
card_uid('diminishing returns'/'6ED', '6ED:Diminishing Returns:diminishing returns').
card_rarity('diminishing returns'/'6ED', 'Rare').
card_artist('diminishing returns'/'6ED', 'L. A. Williams').
card_number('diminishing returns'/'6ED', '65').
card_multiverse_id('diminishing returns'/'6ED', '15363').

card_in_set('dingus egg', '6ED').
card_original_type('dingus egg'/'6ED', 'Artifact').
card_original_text('dingus egg'/'6ED', 'Whenever a land is put into a graveyard from play, Dingus Egg deals 2 damage to that land\'s controller.').
card_image_name('dingus egg'/'6ED', 'dingus egg').
card_uid('dingus egg'/'6ED', '6ED:Dingus Egg:dingus egg').
card_rarity('dingus egg'/'6ED', 'Rare').
card_artist('dingus egg'/'6ED', 'Randy Gallegos').
card_number('dingus egg'/'6ED', '280').
card_multiverse_id('dingus egg'/'6ED', '14774').

card_in_set('disenchant', '6ED').
card_original_type('disenchant'/'6ED', 'Instant').
card_original_text('disenchant'/'6ED', 'Destroy target artifact or enchantment.').
card_image_name('disenchant'/'6ED', 'disenchant').
card_uid('disenchant'/'6ED', '6ED:Disenchant:disenchant').
card_rarity('disenchant'/'6ED', 'Common').
card_artist('disenchant'/'6ED', 'Brian Snõddy').
card_number('disenchant'/'6ED', '16').
card_multiverse_id('disenchant'/'6ED', '14463').

card_in_set('disrupting scepter', '6ED').
card_original_type('disrupting scepter'/'6ED', 'Artifact').
card_original_text('disrupting scepter'/'6ED', '{3}, {T}: Target player chooses and discards a card from his or her hand. Play this ability only during your turn.').
card_image_name('disrupting scepter'/'6ED', 'disrupting scepter').
card_uid('disrupting scepter'/'6ED', '6ED:Disrupting Scepter:disrupting scepter').
card_rarity('disrupting scepter'/'6ED', 'Rare').
card_artist('disrupting scepter'/'6ED', 'Stuart Griffin').
card_number('disrupting scepter'/'6ED', '281').
card_multiverse_id('disrupting scepter'/'6ED', '11240').

card_in_set('divine transformation', '6ED').
card_original_type('divine transformation'/'6ED', 'Enchant Creature').
card_original_text('divine transformation'/'6ED', 'Enchanted creature gets +3/+3.').
card_image_name('divine transformation'/'6ED', 'divine transformation').
card_uid('divine transformation'/'6ED', '6ED:Divine Transformation:divine transformation').
card_rarity('divine transformation'/'6ED', 'Uncommon').
card_artist('divine transformation'/'6ED', 'NéNé Thomas').
card_number('divine transformation'/'6ED', '17').
card_flavor_text('divine transformation'/'6ED', 'Glory surged through her and radiance surrounded her. All things were possible with the blessing of the Divine.').
card_multiverse_id('divine transformation'/'6ED', '11242').

card_in_set('doomsday', '6ED').
card_original_type('doomsday'/'6ED', 'Sorcery').
card_original_text('doomsday'/'6ED', 'Search your library and graveyard for five cards of your choice and remove the rest from the game. Put the chosen cards on top of your library in any order you choose. You lose half your life, rounded up.').
card_image_name('doomsday'/'6ED', 'doomsday').
card_uid('doomsday'/'6ED', '6ED:Doomsday:doomsday').
card_rarity('doomsday'/'6ED', 'Rare').
card_artist('doomsday'/'6ED', 'Adrian Smith').
card_number('doomsday'/'6ED', '121').
card_multiverse_id('doomsday'/'6ED', '15397').

card_in_set('dragon engine', '6ED').
card_original_type('dragon engine'/'6ED', 'Artifact Creature').
card_original_text('dragon engine'/'6ED', '{2}: Dragon Engine gets +1/+0 until end of turn.').
card_image_name('dragon engine'/'6ED', 'dragon engine').
card_uid('dragon engine'/'6ED', '6ED:Dragon Engine:dragon engine').
card_rarity('dragon engine'/'6ED', 'Rare').
card_artist('dragon engine'/'6ED', 'Anson Maddocks').
card_number('dragon engine'/'6ED', '282').
card_flavor_text('dragon engine'/'6ED', '\"How is it the legged engines seem so much more alive than their wheeled counterparts? Simply, they were born, not made.\"\n—Mishra').
card_multiverse_id('dragon engine'/'6ED', '15412').

card_in_set('dragon mask', '6ED').
card_original_type('dragon mask'/'6ED', 'Artifact').
card_original_text('dragon mask'/'6ED', '{3}, {T}: Target creature you control gets +2/+2 until end of turn. At end of turn, return that creature to its owner\'s hand.').
card_image_name('dragon mask'/'6ED', 'dragon mask').
card_uid('dragon mask'/'6ED', '6ED:Dragon Mask:dragon mask').
card_rarity('dragon mask'/'6ED', 'Uncommon').
card_artist('dragon mask'/'6ED', 'Craig Hooper').
card_number('dragon mask'/'6ED', '283').
card_flavor_text('dragon mask'/'6ED', '\"With no further options, I was forced to don the mask.\"\n—Rashida Scalebane').
card_multiverse_id('dragon mask'/'6ED', '15437').

card_in_set('dread of night', '6ED').
card_original_type('dread of night'/'6ED', 'Enchantment').
card_original_text('dread of night'/'6ED', 'White creatures get -1/-1.').
card_image_name('dread of night'/'6ED', 'dread of night').
card_uid('dread of night'/'6ED', '6ED:Dread of Night:dread of night').
card_rarity('dread of night'/'6ED', 'Uncommon').
card_artist('dread of night'/'6ED', 'Richard Thomas').
card_number('dread of night'/'6ED', '122').
card_flavor_text('dread of night'/'6ED', '\"These moonless, foreign skies keep me in thrall. Dark whispers echo in the night, and I cannot resist.\"\n—Selenia, dark angel').
card_multiverse_id('dread of night'/'6ED', '14580').

card_in_set('dream cache', '6ED').
card_original_type('dream cache'/'6ED', 'Sorcery').
card_original_text('dream cache'/'6ED', 'Draw three cards. Choose two cards from your hand and put both on either the top or the bottom of your library.').
card_image_name('dream cache'/'6ED', 'dream cache').
card_uid('dream cache'/'6ED', '6ED:Dream Cache:dream cache').
card_rarity('dream cache'/'6ED', 'Common').
card_artist('dream cache'/'6ED', 'D. Alexander Gregory').
card_number('dream cache'/'6ED', '66').
card_flavor_text('dream cache'/'6ED', 'Dreams\' riches are easily spent.\n—Suq\'Ata adage').
card_multiverse_id('dream cache'/'6ED', '15365').

card_in_set('drudge skeletons', '6ED').
card_original_type('drudge skeletons'/'6ED', 'Creature — Skeleton').
card_original_text('drudge skeletons'/'6ED', '{B} Regenerate Drudge Skeletons.').
card_image_name('drudge skeletons'/'6ED', 'drudge skeletons').
card_uid('drudge skeletons'/'6ED', '6ED:Drudge Skeletons:drudge skeletons').
card_rarity('drudge skeletons'/'6ED', 'Common').
card_artist('drudge skeletons'/'6ED', 'Ian Miller').
card_number('drudge skeletons'/'6ED', '123').
card_flavor_text('drudge skeletons'/'6ED', '\"The dead make good soldiers. They can\'t disobey orders, they never surrender, and they don\'t stop fighting when a random body part falls off.\"\n—Nevinyrral, Necromancer\'s Handbook').
card_multiverse_id('drudge skeletons'/'6ED', '14569').

card_in_set('dry spell', '6ED').
card_original_type('dry spell'/'6ED', 'Sorcery').
card_original_text('dry spell'/'6ED', 'Dry Spell deals 1 damage to each creature and each player.').
card_image_name('dry spell'/'6ED', 'dry spell').
card_uid('dry spell'/'6ED', '6ED:Dry Spell:dry spell').
card_rarity('dry spell'/'6ED', 'Common').
card_artist('dry spell'/'6ED', 'Brian Snõddy').
card_number('dry spell'/'6ED', '124').
card_flavor_text('dry spell'/'6ED', '\"Wherever water is lacking, all things suffer.\"\n—Autumn Willow').
card_multiverse_id('dry spell'/'6ED', '15382').

card_in_set('dwarven ruins', '6ED').
card_original_type('dwarven ruins'/'6ED', 'Land').
card_original_text('dwarven ruins'/'6ED', 'Dwarven Ruins comes into play tapped.\n{T}: Add {R} to your mana pool.\n{T}, Sacrifice Dwarven Ruins: Add {R}{R} to your mana pool.').
card_image_name('dwarven ruins'/'6ED', 'dwarven ruins').
card_uid('dwarven ruins'/'6ED', '6ED:Dwarven Ruins:dwarven ruins').
card_rarity('dwarven ruins'/'6ED', 'Uncommon').
card_artist('dwarven ruins'/'6ED', 'Liz Danforth').
card_number('dwarven ruins'/'6ED', '323').
card_multiverse_id('dwarven ruins'/'6ED', '14725').

card_in_set('early harvest', '6ED').
card_original_type('early harvest'/'6ED', 'Instant').
card_original_text('early harvest'/'6ED', 'Target player untaps all basic lands he or she controls.').
card_image_name('early harvest'/'6ED', 'early harvest').
card_uid('early harvest'/'6ED', '6ED:Early Harvest:early harvest').
card_rarity('early harvest'/'6ED', 'Rare').
card_artist('early harvest'/'6ED', 'Janine Johnston').
card_number('early harvest'/'6ED', '222').
card_flavor_text('early harvest'/'6ED', '\"Tonight we\'ll eat a farewell feast. Cold corn porridge is not enough. / Let\'s peel papayas, pineapples, and mangoes, drink coconut milk, / and bake bananas.\"\n—\"Love Song of Night and Day\"').
card_multiverse_id('early harvest'/'6ED', '14714').

card_in_set('earthquake', '6ED').
card_original_type('earthquake'/'6ED', 'Sorcery').
card_original_text('earthquake'/'6ED', 'Earthquake deals X damage to each creature without flying and each player.').
card_image_name('earthquake'/'6ED', 'earthquake').
card_uid('earthquake'/'6ED', '6ED:Earthquake:earthquake').
card_rarity('earthquake'/'6ED', 'Rare').
card_artist('earthquake'/'6ED', 'Richard Kane Ferguson').
card_number('earthquake'/'6ED', '173').
card_multiverse_id('earthquake'/'6ED', '14651').

card_in_set('ebon stronghold', '6ED').
card_original_type('ebon stronghold'/'6ED', 'Land').
card_original_text('ebon stronghold'/'6ED', 'Ebon Stronghold comes into play tapped.\n{T}: Add {B} to your mana pool.\n{T}, Sacrifice Ebon Stronghold: Add {B}{B} to your mana pool.').
card_image_name('ebon stronghold'/'6ED', 'ebon stronghold').
card_uid('ebon stronghold'/'6ED', '6ED:Ebon Stronghold:ebon stronghold').
card_rarity('ebon stronghold'/'6ED', 'Uncommon').
card_artist('ebon stronghold'/'6ED', 'Liz Danforth').
card_number('ebon stronghold'/'6ED', '324').
card_multiverse_id('ebon stronghold'/'6ED', '14724').

card_in_set('ekundu griffin', '6ED').
card_original_type('ekundu griffin'/'6ED', 'Creature — Griffin').
card_original_text('ekundu griffin'/'6ED', 'Flying, first strike').
card_image_name('ekundu griffin'/'6ED', 'ekundu griffin').
card_uid('ekundu griffin'/'6ED', '6ED:Ekundu Griffin:ekundu griffin').
card_rarity('ekundu griffin'/'6ED', 'Common').
card_artist('ekundu griffin'/'6ED', 'David A. Cherry').
card_number('ekundu griffin'/'6ED', '18').
card_flavor_text('ekundu griffin'/'6ED', '\"My goat was a small price to pay for the chance to see the hunting griffin dive and seize it.\"\n—Suq\'Ata trader').
card_multiverse_id('ekundu griffin'/'6ED', '14474').

card_in_set('elder druid', '6ED').
card_original_type('elder druid'/'6ED', 'Creature — Cleric').
card_original_text('elder druid'/'6ED', '{3}{G}, {T}: Tap or untap target artifact, creature, or land.').
card_image_name('elder druid'/'6ED', 'elder druid').
card_uid('elder druid'/'6ED', '6ED:Elder Druid:elder druid').
card_rarity('elder druid'/'6ED', 'Rare').
card_artist('elder druid'/'6ED', 'Richard Kane Ferguson').
card_number('elder druid'/'6ED', '223').
card_flavor_text('elder druid'/'6ED', '\"Did clouds dance in his eyes,\ndid thorns play at his fingertips?\"\n—Shesul Fass, faerie bard').
card_multiverse_id('elder druid'/'6ED', '14717').

card_in_set('elven cache', '6ED').
card_original_type('elven cache'/'6ED', 'Sorcery').
card_original_text('elven cache'/'6ED', 'Return target card from your graveyard to your hand.').
card_image_name('elven cache'/'6ED', 'elven cache').
card_uid('elven cache'/'6ED', '6ED:Elven Cache:elven cache').
card_rarity('elven cache'/'6ED', 'Common').
card_artist('elven cache'/'6ED', 'Rebecca Guay').
card_number('elven cache'/'6ED', '224').
card_flavor_text('elven cache'/'6ED', 'Elves know where to harvest the best of the forest, for they planted it there themselves.').
card_multiverse_id('elven cache'/'6ED', '14686').

card_in_set('elven riders', '6ED').
card_original_type('elven riders'/'6ED', 'Creature — Elf').
card_original_text('elven riders'/'6ED', 'Elven Riders can\'t be blocked except by creatures with flying or Walls.').
card_image_name('elven riders'/'6ED', 'elven riders').
card_uid('elven riders'/'6ED', '6ED:Elven Riders:elven riders').
card_rarity('elven riders'/'6ED', 'Uncommon').
card_artist('elven riders'/'6ED', 'Dan Frazier').
card_number('elven riders'/'6ED', '225').
card_flavor_text('elven riders'/'6ED', 'Sometimes it is better to be swift of foot than strong of swordarm.\n—Elven proverb').
card_multiverse_id('elven riders'/'6ED', '14705').

card_in_set('elvish archers', '6ED').
card_original_type('elvish archers'/'6ED', 'Creature — Elf').
card_original_text('elvish archers'/'6ED', 'First strike').
card_image_name('elvish archers'/'6ED', 'elvish archers').
card_uid('elvish archers'/'6ED', '6ED:Elvish Archers:elvish archers').
card_rarity('elvish archers'/'6ED', 'Rare').
card_artist('elvish archers'/'6ED', 'Anson Maddocks').
card_number('elvish archers'/'6ED', '226').
card_flavor_text('elvish archers'/'6ED', '\"I tell you, there was so many arrows flying about you couldn\'t hardly see the sun. So I says to young Angus, ‘Well, at least now we\'re fighting in the shade!\'\"').
card_multiverse_id('elvish archers'/'6ED', '14716').

card_in_set('enfeeblement', '6ED').
card_original_type('enfeeblement'/'6ED', 'Enchant Creature').
card_original_text('enfeeblement'/'6ED', 'Enchanted creature gets -2/-2.').
card_image_name('enfeeblement'/'6ED', 'enfeeblement').
card_uid('enfeeblement'/'6ED', '6ED:Enfeeblement:enfeeblement').
card_rarity('enfeeblement'/'6ED', 'Common').
card_artist('enfeeblement'/'6ED', 'John Bolton').
card_number('enfeeblement'/'6ED', '125').
card_flavor_text('enfeeblement'/'6ED', '\"If it is weak, either kill it or ignore it. Anything else honors it.\"\n—Kaervek').
card_multiverse_id('enfeeblement'/'6ED', '15386').

card_in_set('enlightened tutor', '6ED').
card_original_type('enlightened tutor'/'6ED', 'Instant').
card_original_text('enlightened tutor'/'6ED', 'Search your library for an artifact or enchantment card and reveal that card. Shuffle your library, then put the card on top of it.').
card_image_name('enlightened tutor'/'6ED', 'enlightened tutor').
card_uid('enlightened tutor'/'6ED', '6ED:Enlightened Tutor:enlightened tutor').
card_rarity('enlightened tutor'/'6ED', 'Uncommon').
card_artist('enlightened tutor'/'6ED', 'Dan Frazier').
card_number('enlightened tutor'/'6ED', '19').
card_flavor_text('enlightened tutor'/'6ED', '\"I do not teach. I simply reveal.\"\n—Daudi, Femeref tutor').
card_multiverse_id('enlightened tutor'/'6ED', '15355').

card_in_set('ethereal champion', '6ED').
card_original_type('ethereal champion'/'6ED', 'Creature — Avatar').
card_original_text('ethereal champion'/'6ED', 'Pay 1 life: Prevent the next 1 damage to Ethereal Champion this turn.').
card_image_name('ethereal champion'/'6ED', 'ethereal champion').
card_uid('ethereal champion'/'6ED', '6ED:Ethereal Champion:ethereal champion').
card_rarity('ethereal champion'/'6ED', 'Rare').
card_artist('ethereal champion'/'6ED', 'Terese Nielsen').
card_number('ethereal champion'/'6ED', '20').
card_flavor_text('ethereal champion'/'6ED', '\"If I have learned anything as a wizard it is this: never fight your own battles.\"\n—Waffa, sorcerer of Nyomba').
card_multiverse_id('ethereal champion'/'6ED', '15356').

card_in_set('evil eye of orms-by-gore', '6ED').
card_original_type('evil eye of orms-by-gore'/'6ED', 'Creature — Horror').
card_original_text('evil eye of orms-by-gore'/'6ED', 'Evil Eye of Orms-by-Gore can\'t be blocked except by Walls.\nExcept for Evil Eye of Orms-by-Gore, creatures you control can\'t attack.').
card_image_name('evil eye of orms-by-gore'/'6ED', 'evil eye of orms-by-gore').
card_uid('evil eye of orms-by-gore'/'6ED', '6ED:Evil Eye of Orms-by-Gore:evil eye of orms-by-gore').
card_rarity('evil eye of orms-by-gore'/'6ED', 'Uncommon').
card_artist('evil eye of orms-by-gore'/'6ED', 'George Pratt').
card_number('evil eye of orms-by-gore'/'6ED', '126').
card_flavor_text('evil eye of orms-by-gore'/'6ED', 'The highway of fear is the shortest route to defeat.').
card_multiverse_id('evil eye of orms-by-gore'/'6ED', '14593').

card_in_set('exile', '6ED').
card_original_type('exile'/'6ED', 'Instant').
card_original_text('exile'/'6ED', 'Remove target nonwhite attacking creature from the game. You gain life equal to its toughness.').
card_image_name('exile'/'6ED', 'exile').
card_uid('exile'/'6ED', '6ED:Exile:exile').
card_rarity('exile'/'6ED', 'Rare').
card_artist('exile'/'6ED', 'Rob Alexander').
card_number('exile'/'6ED', '21').
card_multiverse_id('exile'/'6ED', '15350').

card_in_set('fallen angel', '6ED').
card_original_type('fallen angel'/'6ED', 'Creature — Angel').
card_original_text('fallen angel'/'6ED', 'Flying\nSacrifice a creature: Fallen Angel gets +2/+1 until end of turn.').
card_image_name('fallen angel'/'6ED', 'fallen angel').
card_uid('fallen angel'/'6ED', '6ED:Fallen Angel:fallen angel').
card_rarity('fallen angel'/'6ED', 'Rare').
card_artist('fallen angel'/'6ED', 'Anson Maddocks').
card_number('fallen angel'/'6ED', '127').
card_flavor_text('fallen angel'/'6ED', '\"Angels are simply extensions of truth upon the fabric of life—and there is far more dark than light.\" —Baron Sengir').
card_multiverse_id('fallen angel'/'6ED', '16629').

card_in_set('fallow earth', '6ED').
card_original_type('fallow earth'/'6ED', 'Sorcery').
card_original_text('fallow earth'/'6ED', 'Put target land on top of its owner\'s library.').
card_image_name('fallow earth'/'6ED', 'fallow earth').
card_uid('fallow earth'/'6ED', '6ED:Fallow Earth:fallow earth').
card_rarity('fallow earth'/'6ED', 'Uncommon').
card_artist('fallow earth'/'6ED', 'Janine Johnston').
card_number('fallow earth'/'6ED', '227').
card_flavor_text('fallow earth'/'6ED', '\". . . and when the farmer awoke the next morning, all the seeds from his field were once again in their sacks.\"\n—Afari, Tales').
card_multiverse_id('fallow earth'/'6ED', '16454').

card_in_set('familiar ground', '6ED').
card_original_type('familiar ground'/'6ED', 'Enchantment').
card_original_text('familiar ground'/'6ED', 'Each creature you control can\'t be blocked by more than one creature each combat.').
card_image_name('familiar ground'/'6ED', 'familiar ground').
card_uid('familiar ground'/'6ED', '6ED:Familiar Ground:familiar ground').
card_rarity('familiar ground'/'6ED', 'Uncommon').
card_artist('familiar ground'/'6ED', 'Jeff Miracola').
card_number('familiar ground'/'6ED', '228').
card_flavor_text('familiar ground'/'6ED', '\"I\'d rather fight on foreign soil. That way, if I lose I can blame it on the terrain.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('familiar ground'/'6ED', '14691').

card_in_set('fatal blow', '6ED').
card_original_type('fatal blow'/'6ED', 'Instant').
card_original_text('fatal blow'/'6ED', 'Destroy target creature that was dealt damage this turn. It can\'t be regenerated.').
card_image_name('fatal blow'/'6ED', 'fatal blow').
card_uid('fatal blow'/'6ED', '6ED:Fatal Blow:fatal blow').
card_rarity('fatal blow'/'6ED', 'Common').
card_artist('fatal blow'/'6ED', 'George Pratt').
card_number('fatal blow'/'6ED', '128').
card_flavor_text('fatal blow'/'6ED', '\"What is crueler? To let a wound of the heart fester, or to simply cut it out?\"\n—Crovax').
card_multiverse_id('fatal blow'/'6ED', '16623').

card_in_set('fear', '6ED').
card_original_type('fear'/'6ED', 'Enchant Creature').
card_original_text('fear'/'6ED', 'Enchanted creature can\'t be blocked except by artifact creatures and black creatures.').
card_image_name('fear'/'6ED', 'fear').
card_uid('fear'/'6ED', '6ED:Fear:fear').
card_rarity('fear'/'6ED', 'Common').
card_artist('fear'/'6ED', 'Doug Keith').
card_number('fear'/'6ED', '129').
card_multiverse_id('fear'/'6ED', '14560').

card_in_set('feast of the unicorn', '6ED').
card_original_type('feast of the unicorn'/'6ED', 'Enchant Creature').
card_original_text('feast of the unicorn'/'6ED', 'Enchanted creature gets +4/+0.').
card_image_name('feast of the unicorn'/'6ED', 'feast of the unicorn').
card_uid('feast of the unicorn'/'6ED', '6ED:Feast of the Unicorn:feast of the unicorn').
card_rarity('feast of the unicorn'/'6ED', 'Common').
card_artist('feast of the unicorn'/'6ED', 'Dennis Detwiller').
card_number('feast of the unicorn'/'6ED', '130').
card_flavor_text('feast of the unicorn'/'6ED', '\"Could there be a fouler act? No doubt the baron knows of one.\"\n—Autumn Willow').
card_multiverse_id('feast of the unicorn'/'6ED', '16625').

card_in_set('femeref archers', '6ED').
card_original_type('femeref archers'/'6ED', 'Creature — Soldier').
card_original_text('femeref archers'/'6ED', '{T}: Femeref Archers deals 4 damage to target attacking creature with flying.').
card_image_name('femeref archers'/'6ED', 'femeref archers').
card_uid('femeref archers'/'6ED', '6ED:Femeref Archers:femeref archers').
card_rarity('femeref archers'/'6ED', 'Uncommon').
card_artist('femeref archers'/'6ED', 'William Donohoe').
card_number('femeref archers'/'6ED', '229').
card_flavor_text('femeref archers'/'6ED', '\"They say a Zhalfirin archer can split the eye of a griffin. Nonsense, of course; they have no faith to guide their darts.\"\n—Nuru, Femeref archer').
card_multiverse_id('femeref archers'/'6ED', '14698').

card_in_set('feral shadow', '6ED').
card_original_type('feral shadow'/'6ED', 'Creature — Night Stalker').
card_original_text('feral shadow'/'6ED', 'Flying').
card_image_name('feral shadow'/'6ED', 'feral shadow').
card_uid('feral shadow'/'6ED', '6ED:Feral Shadow:feral shadow').
card_rarity('feral shadow'/'6ED', 'Common').
card_artist('feral shadow'/'6ED', 'Cliff Nielsen').
card_number('feral shadow'/'6ED', '131').
card_flavor_text('feral shadow'/'6ED', 'Not all shadows are cast by light—some are cast by darkness.').
card_multiverse_id('feral shadow'/'6ED', '15387').

card_in_set('fervor', '6ED').
card_original_type('fervor'/'6ED', 'Enchantment').
card_original_text('fervor'/'6ED', 'Creatures you control gain haste. (They may attack and {T} the turn they come under your control.)').
card_image_name('fervor'/'6ED', 'fervor').
card_uid('fervor'/'6ED', '6ED:Fervor:fervor').
card_rarity('fervor'/'6ED', 'Rare').
card_artist('fervor'/'6ED', 'Franz Vohwinkel').
card_number('fervor'/'6ED', '174').
card_flavor_text('fervor'/'6ED', '\"If your blood doesn\'t run hot, I will make it run in the sands!\"\n—Maraxus of Keld').
card_multiverse_id('fervor'/'6ED', '14648').

card_in_set('final fortune', '6ED').
card_original_type('final fortune'/'6ED', 'Instant').
card_original_text('final fortune'/'6ED', 'Take another turn after this one. At the end of that turn, you lose the game.').
card_image_name('final fortune'/'6ED', 'final fortune').
card_uid('final fortune'/'6ED', '6ED:Final Fortune:final fortune').
card_rarity('final fortune'/'6ED', 'Rare').
card_artist('final fortune'/'6ED', 'D. Alexander Gregory').
card_number('final fortune'/'6ED', '175').
card_flavor_text('final fortune'/'6ED', 'Want all, lose all.\n—Zhalfirin aphorism').
card_multiverse_id('final fortune'/'6ED', '14655').

card_in_set('fire diamond', '6ED').
card_original_type('fire diamond'/'6ED', 'Artifact').
card_original_text('fire diamond'/'6ED', 'Fire Diamond comes into play tapped.\n{T}: Add {R} to your mana pool.').
card_image_name('fire diamond'/'6ED', 'fire diamond').
card_uid('fire diamond'/'6ED', '6ED:Fire Diamond:fire diamond').
card_rarity('fire diamond'/'6ED', 'Uncommon').
card_artist('fire diamond'/'6ED', 'Richard Thomas').
card_number('fire diamond'/'6ED', '284').
card_multiverse_id('fire diamond'/'6ED', '15405').

card_in_set('fire elemental', '6ED').
card_original_type('fire elemental'/'6ED', 'Creature — Elemental').
card_original_text('fire elemental'/'6ED', '').
card_image_name('fire elemental'/'6ED', 'fire elemental').
card_uid('fire elemental'/'6ED', '6ED:Fire Elemental:fire elemental').
card_rarity('fire elemental'/'6ED', 'Uncommon').
card_artist('fire elemental'/'6ED', 'Melissa A. Benson').
card_number('fire elemental'/'6ED', '176').
card_flavor_text('fire elemental'/'6ED', 'Fire elementals are ruthless infernos, annihilating and consuming their foes in a frenzied holocaust. Crackling and blazing, they sear swift, terrible paths, leaving the land charred and scorched in their wake.').
card_multiverse_id('fire elemental'/'6ED', '15419').

card_in_set('firebreathing', '6ED').
card_original_type('firebreathing'/'6ED', 'Enchant Creature').
card_original_text('firebreathing'/'6ED', '{R} Enchanted creature gets +1/+0 until end of turn.').
card_image_name('firebreathing'/'6ED', 'firebreathing').
card_uid('firebreathing'/'6ED', '6ED:Firebreathing:firebreathing').
card_rarity('firebreathing'/'6ED', 'Common').
card_artist('firebreathing'/'6ED', 'Mike Kerr').
card_number('firebreathing'/'6ED', '177').
card_flavor_text('firebreathing'/'6ED', '\"Magic\'s only part of it, my friend. Diet does the rest.\"\n—Suq\'Ata trader').
card_multiverse_id('firebreathing'/'6ED', '14616').

card_in_set('fit of rage', '6ED').
card_original_type('fit of rage'/'6ED', 'Sorcery').
card_original_text('fit of rage'/'6ED', 'Target creature gets +3/+3 and gains first strike until end of turn.').
card_image_name('fit of rage'/'6ED', 'fit of rage').
card_uid('fit of rage'/'6ED', '6ED:Fit of Rage:fit of rage').
card_rarity('fit of rage'/'6ED', 'Common').
card_artist('fit of rage'/'6ED', 'Douglas Shuler').
card_number('fit of rage'/'6ED', '178').
card_flavor_text('fit of rage'/'6ED', '\"Anger is fleeting; remorse is eternal.\"\n—Karn, silver golem').
card_multiverse_id('fit of rage'/'6ED', '16622').

card_in_set('flame spirit', '6ED').
card_original_type('flame spirit'/'6ED', 'Creature — Spirit').
card_original_text('flame spirit'/'6ED', '{R} Flame Spirit gets +1/+0 until end of turn.').
card_image_name('flame spirit'/'6ED', 'flame spirit').
card_uid('flame spirit'/'6ED', '6ED:Flame Spirit:flame spirit').
card_rarity('flame spirit'/'6ED', 'Common').
card_artist('flame spirit'/'6ED', 'Justin Hampton').
card_number('flame spirit'/'6ED', '179').
card_flavor_text('flame spirit'/'6ED', '\"The spirit of the flame is the spirit of change.\"\n—Lovisa Coldeyes,\nBalduvian chieftain').
card_multiverse_id('flame spirit'/'6ED', '14626').

card_in_set('flash', '6ED').
card_original_type('flash'/'6ED', 'Instant').
card_original_text('flash'/'6ED', 'Put a creature card from your hand into play. You may pay its mana cost reduced by up to {2}. If you don\'t, sacrifice it.').
card_image_name('flash'/'6ED', 'flash').
card_uid('flash'/'6ED', '6ED:Flash:flash').
card_rarity('flash'/'6ED', 'Rare').
card_artist('flash'/'6ED', 'David Ho').
card_number('flash'/'6ED', '67').
card_multiverse_id('flash'/'6ED', '16440').

card_in_set('flashfires', '6ED').
card_original_type('flashfires'/'6ED', 'Sorcery').
card_original_text('flashfires'/'6ED', 'Destroy all plains.').
card_image_name('flashfires'/'6ED', 'flashfires').
card_uid('flashfires'/'6ED', '6ED:Flashfires:flashfires').
card_rarity('flashfires'/'6ED', 'Uncommon').
card_artist('flashfires'/'6ED', 'Randy Gallegos').
card_number('flashfires'/'6ED', '180').
card_multiverse_id('flashfires'/'6ED', '16446').

card_in_set('flight', '6ED').
card_original_type('flight'/'6ED', 'Enchant Creature').
card_original_text('flight'/'6ED', 'Enchanted creature gains flying.').
card_image_name('flight'/'6ED', 'flight').
card_uid('flight'/'6ED', '6ED:Flight:flight').
card_rarity('flight'/'6ED', 'Common').
card_artist('flight'/'6ED', 'Jerry Tiritilli').
card_number('flight'/'6ED', '68').
card_multiverse_id('flight'/'6ED', '14515').

card_in_set('flying carpet', '6ED').
card_original_type('flying carpet'/'6ED', 'Artifact').
card_original_text('flying carpet'/'6ED', '{2}, {T}: Target creature gains flying until end of turn.').
card_image_name('flying carpet'/'6ED', 'flying carpet').
card_uid('flying carpet'/'6ED', '6ED:Flying Carpet:flying carpet').
card_rarity('flying carpet'/'6ED', 'Rare').
card_artist('flying carpet'/'6ED', 'Mark Tedin').
card_number('flying carpet'/'6ED', '285').
card_multiverse_id('flying carpet'/'6ED', '14776').

card_in_set('fog', '6ED').
card_original_type('fog'/'6ED', 'Instant').
card_original_text('fog'/'6ED', 'Creatures deal no combat damage this turn.').
card_image_name('fog'/'6ED', 'fog').
card_uid('fog'/'6ED', '6ED:Fog:fog').
card_rarity('fog'/'6ED', 'Common').
card_artist('fog'/'6ED', 'Harold McNeill').
card_number('fog'/'6ED', '230').
card_multiverse_id('fog'/'6ED', '14682').

card_in_set('fog elemental', '6ED').
card_original_type('fog elemental'/'6ED', 'Creature — Elemental').
card_original_text('fog elemental'/'6ED', 'Flying\nWhen Fog Elemental attacks or blocks, sacrifice it at end of combat.').
card_image_name('fog elemental'/'6ED', 'fog elemental').
card_uid('fog elemental'/'6ED', '6ED:Fog Elemental:fog elemental').
card_rarity('fog elemental'/'6ED', 'Common').
card_artist('fog elemental'/'6ED', 'Jon J. Muth').
card_number('fog elemental'/'6ED', '69').
card_flavor_text('fog elemental'/'6ED', '\"I\'ve seen fog so thick you could cut it, but none that could cut me.\"\n—Tahngarth of the Weatherlight').
card_multiverse_id('fog elemental'/'6ED', '16433').

card_in_set('forbidden crypt', '6ED').
card_original_type('forbidden crypt'/'6ED', 'Enchantment').
card_original_text('forbidden crypt'/'6ED', 'Whenever you would draw a card, instead return target card from your graveyard to your hand. If you can\'t, you lose the game.\nWhenever a card would be put into your graveyard, instead remove that card from the game.').
card_image_name('forbidden crypt'/'6ED', 'forbidden crypt').
card_uid('forbidden crypt'/'6ED', '6ED:Forbidden Crypt:forbidden crypt').
card_rarity('forbidden crypt'/'6ED', 'Rare').
card_artist('forbidden crypt'/'6ED', 'D. Alexander Gregory').
card_number('forbidden crypt'/'6ED', '132').
card_multiverse_id('forbidden crypt'/'6ED', '15388').

card_in_set('forest', '6ED').
card_original_type('forest'/'6ED', 'Land').
card_original_text('forest'/'6ED', 'G').
card_image_name('forest'/'6ED', 'forest1').
card_uid('forest'/'6ED', '6ED:Forest:forest1').
card_rarity('forest'/'6ED', 'Basic Land').
card_artist('forest'/'6ED', 'Quinton Hoover').
card_number('forest'/'6ED', '347').
card_multiverse_id('forest'/'6ED', '14749').

card_in_set('forest', '6ED').
card_original_type('forest'/'6ED', 'Land').
card_original_text('forest'/'6ED', 'G').
card_image_name('forest'/'6ED', 'forest2').
card_uid('forest'/'6ED', '6ED:Forest:forest2').
card_rarity('forest'/'6ED', 'Basic Land').
card_artist('forest'/'6ED', 'Quinton Hoover').
card_number('forest'/'6ED', '348').
card_multiverse_id('forest'/'6ED', '14750').

card_in_set('forest', '6ED').
card_original_type('forest'/'6ED', 'Land').
card_original_text('forest'/'6ED', 'G').
card_image_name('forest'/'6ED', 'forest3').
card_uid('forest'/'6ED', '6ED:Forest:forest3').
card_rarity('forest'/'6ED', 'Basic Land').
card_artist('forest'/'6ED', 'John Avon').
card_number('forest'/'6ED', '349').
card_multiverse_id('forest'/'6ED', '14751').

card_in_set('forest', '6ED').
card_original_type('forest'/'6ED', 'Land').
card_original_text('forest'/'6ED', 'G').
card_image_name('forest'/'6ED', 'forest4').
card_uid('forest'/'6ED', '6ED:Forest:forest4').
card_rarity('forest'/'6ED', 'Basic Land').
card_artist('forest'/'6ED', 'John Avon').
card_number('forest'/'6ED', '350').
card_multiverse_id('forest'/'6ED', '14752').

card_in_set('forget', '6ED').
card_original_type('forget'/'6ED', 'Sorcery').
card_original_text('forget'/'6ED', 'Target player chooses and discards two cards from his or her hand, then draws as many cards as he or she discarded this way.').
card_image_name('forget'/'6ED', 'forget').
card_uid('forget'/'6ED', '6ED:Forget:forget').
card_rarity('forget'/'6ED', 'Rare').
card_artist('forget'/'6ED', 'Mike Kimble').
card_number('forget'/'6ED', '70').
card_multiverse_id('forget'/'6ED', '14547').

card_in_set('fountain of youth', '6ED').
card_original_type('fountain of youth'/'6ED', 'Artifact').
card_original_text('fountain of youth'/'6ED', '{2}, {T}: You gain 1 life.').
card_image_name('fountain of youth'/'6ED', 'fountain of youth').
card_uid('fountain of youth'/'6ED', '6ED:Fountain of Youth:fountain of youth').
card_rarity('fountain of youth'/'6ED', 'Uncommon').
card_artist('fountain of youth'/'6ED', 'Daniel Gelon').
card_number('fountain of youth'/'6ED', '286').
card_flavor_text('fountain of youth'/'6ED', 'The fountain had stood in the town square for centuries, but only the pigeons knew its secret.').
card_multiverse_id('fountain of youth'/'6ED', '14765').

card_in_set('fyndhorn brownie', '6ED').
card_original_type('fyndhorn brownie'/'6ED', 'Creature — Brownie').
card_original_text('fyndhorn brownie'/'6ED', '{2}{G}, {T}: Untap target creature.').
card_image_name('fyndhorn brownie'/'6ED', 'fyndhorn brownie').
card_uid('fyndhorn brownie'/'6ED', '6ED:Fyndhorn Brownie:fyndhorn brownie').
card_rarity('fyndhorn brownie'/'6ED', 'Common').
card_artist('fyndhorn brownie'/'6ED', 'Richard Thomas').
card_number('fyndhorn brownie'/'6ED', '231').
card_flavor_text('fyndhorn brownie'/'6ED', '\"I\'ve been insulted by drunks in a hundred inns, but never as skillfully or annoyingly as by those blasted brownies.\"\n—General Jarkeld, the Arctic Fox').
card_multiverse_id('fyndhorn brownie'/'6ED', '16451').

card_in_set('fyndhorn elder', '6ED').
card_original_type('fyndhorn elder'/'6ED', 'Creature — Elf').
card_original_text('fyndhorn elder'/'6ED', '{T}: Add {G}{G} to your mana pool.').
card_image_name('fyndhorn elder'/'6ED', 'fyndhorn elder').
card_uid('fyndhorn elder'/'6ED', '6ED:Fyndhorn Elder:fyndhorn elder').
card_rarity('fyndhorn elder'/'6ED', 'Uncommon').
card_artist('fyndhorn elder'/'6ED', 'Donato Giancola').
card_number('fyndhorn elder'/'6ED', '232').
card_flavor_text('fyndhorn elder'/'6ED', '\"Do we know what we\'re doing? Yes—the will of Freyalise.\"\n—Laina of the Elvish Council').
card_multiverse_id('fyndhorn elder'/'6ED', '14699').

card_in_set('gaseous form', '6ED').
card_original_type('gaseous form'/'6ED', 'Enchant Creature').
card_original_text('gaseous form'/'6ED', 'Enchanted creature deals no combat damage.\nPrevent all combat damage that would be dealt to enchanted creature.').
card_image_name('gaseous form'/'6ED', 'gaseous form').
card_uid('gaseous form'/'6ED', '6ED:Gaseous Form:gaseous form').
card_rarity('gaseous form'/'6ED', 'Common').
card_artist('gaseous form'/'6ED', 'Roger Raupp').
card_number('gaseous form'/'6ED', '71').
card_multiverse_id('gaseous form'/'6ED', '11298').

card_in_set('giant growth', '6ED').
card_original_type('giant growth'/'6ED', 'Instant').
card_original_text('giant growth'/'6ED', 'Target creature gets +3/+3 until end of turn.').
card_image_name('giant growth'/'6ED', 'giant growth').
card_uid('giant growth'/'6ED', '6ED:Giant Growth:giant growth').
card_rarity('giant growth'/'6ED', 'Common').
card_artist('giant growth'/'6ED', 'DiTerlizzi').
card_number('giant growth'/'6ED', '233').
card_multiverse_id('giant growth'/'6ED', '14683').

card_in_set('giant spider', '6ED').
card_original_type('giant spider'/'6ED', 'Creature — Spider').
card_original_text('giant spider'/'6ED', 'Giant Spider can block as though it had flying.').
card_image_name('giant spider'/'6ED', 'giant spider').
card_uid('giant spider'/'6ED', '6ED:Giant Spider:giant spider').
card_rarity('giant spider'/'6ED', 'Common').
card_artist('giant spider'/'6ED', 'Randy Gallegos').
card_number('giant spider'/'6ED', '234').
card_flavor_text('giant spider'/'6ED', 'It\'s not far into the trap, as the crow flies.').
card_multiverse_id('giant spider'/'6ED', '14676').

card_in_set('giant strength', '6ED').
card_original_type('giant strength'/'6ED', 'Enchant Creature').
card_original_text('giant strength'/'6ED', 'Enchanted creature gets +2/+2.').
card_image_name('giant strength'/'6ED', 'giant strength').
card_uid('giant strength'/'6ED', '6ED:Giant Strength:giant strength').
card_rarity('giant strength'/'6ED', 'Common').
card_artist('giant strength'/'6ED', 'Kev Walker').
card_number('giant strength'/'6ED', '181').
card_flavor_text('giant strength'/'6ED', '\"O! it is excellent / To have a giant\'s strength, but it is tyrannous / To use it like a giant.\"\n—William Shakespeare,\nMeasure for Measure').
card_multiverse_id('giant strength'/'6ED', '11303').

card_in_set('glacial wall', '6ED').
card_original_type('glacial wall'/'6ED', 'Creature — Wall').
card_original_text('glacial wall'/'6ED', '(Walls can\'t attack.)').
card_image_name('glacial wall'/'6ED', 'glacial wall').
card_uid('glacial wall'/'6ED', '6ED:Glacial Wall:glacial wall').
card_rarity('glacial wall'/'6ED', 'Uncommon').
card_artist('glacial wall'/'6ED', 'Dameon Willich').
card_number('glacial wall'/'6ED', '72').
card_flavor_text('glacial wall'/'6ED', '\"Huge walls of ice block further travel. We can\'t believe they are natural.\"\n—Disa the Restless, journal entry').
card_multiverse_id('glacial wall'/'6ED', '14535').

card_in_set('glasses of urza', '6ED').
card_original_type('glasses of urza'/'6ED', 'Artifact').
card_original_text('glasses of urza'/'6ED', '{T}: Look at target player\'s hand.').
card_image_name('glasses of urza'/'6ED', 'glasses of urza').
card_uid('glasses of urza'/'6ED', '6ED:Glasses of Urza:glasses of urza').
card_rarity('glasses of urza'/'6ED', 'Uncommon').
card_artist('glasses of urza'/'6ED', 'Douglas Shuler').
card_number('glasses of urza'/'6ED', '287').
card_multiverse_id('glasses of urza'/'6ED', '11305').

card_in_set('goblin digging team', '6ED').
card_original_type('goblin digging team'/'6ED', 'Creature — Goblin').
card_original_text('goblin digging team'/'6ED', '{T}, Sacrifice Goblin Digging Team: Destroy target Wall.').
card_image_name('goblin digging team'/'6ED', 'goblin digging team').
card_uid('goblin digging team'/'6ED', '6ED:Goblin Digging Team:goblin digging team').
card_rarity('goblin digging team'/'6ED', 'Common').
card_artist('goblin digging team'/'6ED', 'Phil Foglio').
card_number('goblin digging team'/'6ED', '182').
card_flavor_text('goblin digging team'/'6ED', '\"From down here we can make the whole wall collapse!\"\n\"Uh, yeah, boss, but how do we get out?\"').
card_multiverse_id('goblin digging team'/'6ED', '14618').

card_in_set('goblin elite infantry', '6ED').
card_original_type('goblin elite infantry'/'6ED', 'Creature — Goblin').
card_original_text('goblin elite infantry'/'6ED', 'Whenever Goblin Elite Infantry blocks or is blocked, it gets -1/-1 until end of turn.').
card_image_name('goblin elite infantry'/'6ED', 'goblin elite infantry').
card_uid('goblin elite infantry'/'6ED', '6ED:Goblin Elite Infantry:goblin elite infantry').
card_rarity('goblin elite infantry'/'6ED', 'Common').
card_artist('goblin elite infantry'/'6ED', 'Robert Bliss').
card_number('goblin elite infantry'/'6ED', '183').
card_flavor_text('goblin elite infantry'/'6ED', 'They talk a good fight.').
card_multiverse_id('goblin elite infantry'/'6ED', '16443').

card_in_set('goblin hero', '6ED').
card_original_type('goblin hero'/'6ED', 'Creature — Goblin').
card_original_text('goblin hero'/'6ED', '').
card_image_name('goblin hero'/'6ED', 'goblin hero').
card_uid('goblin hero'/'6ED', '6ED:Goblin Hero:goblin hero').
card_rarity('goblin hero'/'6ED', 'Common').
card_artist('goblin hero'/'6ED', 'Pete Venters').
card_number('goblin hero'/'6ED', '184').
card_flavor_text('goblin hero'/'6ED', '\"When you\'re a goblin, you don\'t have to step forward to be a hero—everyone else just has to step back!\"\n—Biggum Flodrot, goblin veteran').
card_multiverse_id('goblin hero'/'6ED', '15415').

card_in_set('goblin king', '6ED').
card_original_type('goblin king'/'6ED', 'Creature — Lord').
card_original_text('goblin king'/'6ED', 'All Goblins get +1/+1 and gain mountainwalk. (They\'re unblockable if defending player controls a mountain.)').
card_image_name('goblin king'/'6ED', 'goblin king').
card_uid('goblin king'/'6ED', '6ED:Goblin King:goblin king').
card_rarity('goblin king'/'6ED', 'Rare').
card_artist('goblin king'/'6ED', 'Phil Foglio').
card_number('goblin king'/'6ED', '185').
card_flavor_text('goblin king'/'6ED', 'To be king, Numsgil did in Blog, who did in Unkful, who did in Viddle, who did in Loll, who did in Alrok . . . .\"').
card_multiverse_id('goblin king'/'6ED', '14657').

card_in_set('goblin recruiter', '6ED').
card_original_type('goblin recruiter'/'6ED', 'Creature — Goblin').
card_original_text('goblin recruiter'/'6ED', 'When Goblin Recruiter comes into play, search your library for any number of Goblin cards you choose. Reveal those cards, then shuffle your library and put them on top of it in any order you choose.').
card_image_name('goblin recruiter'/'6ED', 'goblin recruiter').
card_uid('goblin recruiter'/'6ED', '6ED:Goblin Recruiter:goblin recruiter').
card_rarity('goblin recruiter'/'6ED', 'Uncommon').
card_artist('goblin recruiter'/'6ED', 'Scott Kirschner').
card_number('goblin recruiter'/'6ED', '186').
card_flavor_text('goblin recruiter'/'6ED', '\"Next!\"').
card_multiverse_id('goblin recruiter'/'6ED', '15417').

card_in_set('goblin warrens', '6ED').
card_original_type('goblin warrens'/'6ED', 'Enchantment').
card_original_text('goblin warrens'/'6ED', '{2}{R}, Sacrifice two Goblins: Put three 1/1 red Goblin creature tokens into play.').
card_image_name('goblin warrens'/'6ED', 'goblin warrens').
card_uid('goblin warrens'/'6ED', '6ED:Goblin Warrens:goblin warrens').
card_rarity('goblin warrens'/'6ED', 'Rare').
card_artist('goblin warrens'/'6ED', 'Dan Frazier').
card_number('goblin warrens'/'6ED', '187').
card_flavor_text('goblin warrens'/'6ED', '\"Goblins bred underground, their numbers hidden from the enemy until it was too late.\" —Sarpadian Empires, vol. IV').
card_multiverse_id('goblin warrens'/'6ED', '15421').

card_in_set('gorilla chieftain', '6ED').
card_original_type('gorilla chieftain'/'6ED', 'Creature — Ape').
card_original_text('gorilla chieftain'/'6ED', '{1}{G} Regenerate Gorilla Chieftain.').
card_image_name('gorilla chieftain'/'6ED', 'gorilla chieftain').
card_uid('gorilla chieftain'/'6ED', '6ED:Gorilla Chieftain:gorilla chieftain').
card_rarity('gorilla chieftain'/'6ED', 'Common').
card_artist('gorilla chieftain'/'6ED', 'Quinton Hoover').
card_number('gorilla chieftain'/'6ED', '235').
card_flavor_text('gorilla chieftain'/'6ED', '\"In the soil of leadership sprout the seeds of immortality.\"\n—Kaysa, Elder Druid of the Juniper').
card_multiverse_id('gorilla chieftain'/'6ED', '15427').

card_in_set('gravebane zombie', '6ED').
card_original_type('gravebane zombie'/'6ED', 'Creature — Zombie').
card_original_text('gravebane zombie'/'6ED', 'When Gravebane Zombie would be put into a graveyard from play, instead put Gravebane Zombie on top of its owner\'s library.').
card_image_name('gravebane zombie'/'6ED', 'gravebane zombie').
card_uid('gravebane zombie'/'6ED', '6ED:Gravebane Zombie:gravebane zombie').
card_rarity('gravebane zombie'/'6ED', 'Uncommon').
card_artist('gravebane zombie'/'6ED', 'Gary Leach').
card_number('gravebane zombie'/'6ED', '133').
card_multiverse_id('gravebane zombie'/'6ED', '15389').

card_in_set('gravedigger', '6ED').
card_original_type('gravedigger'/'6ED', 'Creature — Zombie').
card_original_text('gravedigger'/'6ED', 'When Gravedigger comes into play, you may return target creature card from your graveyard to your hand.').
card_image_name('gravedigger'/'6ED', 'gravedigger').
card_uid('gravedigger'/'6ED', '6ED:Gravedigger:gravedigger').
card_rarity('gravedigger'/'6ED', 'Common').
card_artist('gravedigger'/'6ED', 'Dermot Power').
card_number('gravedigger'/'6ED', '134').
card_flavor_text('gravedigger'/'6ED', 'A full coffin is like a full coffer—both are attractive to thieves.').
card_multiverse_id('gravedigger'/'6ED', '14575').

card_in_set('greed', '6ED').
card_original_type('greed'/'6ED', 'Enchantment').
card_original_text('greed'/'6ED', '{B}, Pay 2 life: Draw a card.').
card_image_name('greed'/'6ED', 'greed').
card_uid('greed'/'6ED', '6ED:Greed:greed').
card_rarity('greed'/'6ED', 'Rare').
card_artist('greed'/'6ED', 'Phil Foglio').
card_number('greed'/'6ED', '135').
card_flavor_text('greed'/'6ED', '\"There is no calamity greater than lavish desires. There is no greater guilt than discontentment. And there is no greater disaster than greed.\"\n—Lao Tsu, Tao Te Ching, (trans. Chan)').
card_multiverse_id('greed'/'6ED', '15379').

card_in_set('grinning totem', '6ED').
card_original_type('grinning totem'/'6ED', 'Artifact').
card_original_text('grinning totem'/'6ED', '{2}, {T}, Sacrifice Grinning Totem: Search target opponent\'s library for a card and set that card aside. That player then shuffles his or her library. You may play the card as though it were in your hand. At the beginning of your next upkeep, if you haven\'t played the card, put it into its owner\'s graveyard.').
card_image_name('grinning totem'/'6ED', 'grinning totem').
card_uid('grinning totem'/'6ED', '6ED:Grinning Totem:grinning totem').
card_rarity('grinning totem'/'6ED', 'Rare').
card_artist('grinning totem'/'6ED', 'Donato Giancola').
card_number('grinning totem'/'6ED', '288').
card_multiverse_id('grinning totem'/'6ED', '15406').

card_in_set('grizzly bears', '6ED').
card_original_type('grizzly bears'/'6ED', 'Creature — Bear').
card_original_text('grizzly bears'/'6ED', '').
card_image_name('grizzly bears'/'6ED', 'grizzly bears').
card_uid('grizzly bears'/'6ED', '6ED:Grizzly Bears:grizzly bears').
card_rarity('grizzly bears'/'6ED', 'Common').
card_artist('grizzly bears'/'6ED', 'Una Fricker').
card_number('grizzly bears'/'6ED', '236').
card_flavor_text('grizzly bears'/'6ED', 'Don\'t try to outrun one of Dominaria\'s grizzlies; it\'ll catch you, knock you down, and eat you. Of course, you could run up a tree. In that case you\'ll get a nice view before it knocks the tree down and eats you.').
card_multiverse_id('grizzly bears'/'6ED', '14671').

card_in_set('hammer of bogardan', '6ED').
card_original_type('hammer of bogardan'/'6ED', 'Sorcery').
card_original_text('hammer of bogardan'/'6ED', 'Hammer of Bogardan deals 3 damage to target creature or player.\n{2}{R}{R}{R} Return Hammer of Bogardan to your hand. Play this ability only during your upkeep and only if Hammer of Bogardan is in your graveyard.').
card_image_name('hammer of bogardan'/'6ED', 'hammer of bogardan').
card_uid('hammer of bogardan'/'6ED', '6ED:Hammer of Bogardan:hammer of bogardan').
card_rarity('hammer of bogardan'/'6ED', 'Rare').
card_artist('hammer of bogardan'/'6ED', 'Ron Spencer').
card_number('hammer of bogardan'/'6ED', '188').
card_multiverse_id('hammer of bogardan'/'6ED', '15420').

card_in_set('harmattan efreet', '6ED').
card_original_type('harmattan efreet'/'6ED', 'Creature — Efreet').
card_original_text('harmattan efreet'/'6ED', 'Flying \n{1}{U}{U} Target creature gains flying until end of turn.').
card_image_name('harmattan efreet'/'6ED', 'harmattan efreet').
card_uid('harmattan efreet'/'6ED', '6ED:Harmattan Efreet:harmattan efreet').
card_rarity('harmattan efreet'/'6ED', 'Uncommon').
card_artist('harmattan efreet'/'6ED', 'Drew Tucker').
card_number('harmattan efreet'/'6ED', '73').
card_flavor_text('harmattan efreet'/'6ED', '\"One moment I was walking along the beach, and the next I was high in the air, staring into a hideous smiling face.\"\n—Tarub, Suq\'Ata sailor').
card_multiverse_id('harmattan efreet'/'6ED', '15366').

card_in_set('havenwood battleground', '6ED').
card_original_type('havenwood battleground'/'6ED', 'Land').
card_original_text('havenwood battleground'/'6ED', 'Havenwood Battleground comes into play tapped.\n{T}: Add {G} to your mana pool.\n{T}, Sacrifice Havenwood Battleground: Add {G}{G} to your mana pool.').
card_image_name('havenwood battleground'/'6ED', 'havenwood battleground').
card_uid('havenwood battleground'/'6ED', '6ED:Havenwood Battleground:havenwood battleground').
card_rarity('havenwood battleground'/'6ED', 'Uncommon').
card_artist('havenwood battleground'/'6ED', 'Liz Danforth').
card_number('havenwood battleground'/'6ED', '325').
card_multiverse_id('havenwood battleground'/'6ED', '14726').

card_in_set('healing salve', '6ED').
card_original_type('healing salve'/'6ED', 'Instant').
card_original_text('healing salve'/'6ED', 'Choose one Target player gains 3 life; or prevent the next 3 damage to target creature or player this turn.').
card_image_name('healing salve'/'6ED', 'healing salve').
card_uid('healing salve'/'6ED', '6ED:Healing Salve:healing salve').
card_rarity('healing salve'/'6ED', 'Common').
card_artist('healing salve'/'6ED', 'Dan Frazier').
card_number('healing salve'/'6ED', '22').
card_multiverse_id('healing salve'/'6ED', '16425').

card_in_set('heavy ballista', '6ED').
card_original_type('heavy ballista'/'6ED', 'Creature — Soldier').
card_original_text('heavy ballista'/'6ED', '{T}: Heavy Ballista deals 2 damage to target attacking or blocking creature.').
card_image_name('heavy ballista'/'6ED', 'heavy ballista').
card_uid('heavy ballista'/'6ED', '6ED:Heavy Ballista:heavy ballista').
card_rarity('heavy ballista'/'6ED', 'Uncommon').
card_artist('heavy ballista'/'6ED', 'Ron Spencer').
card_number('heavy ballista'/'6ED', '23').
card_flavor_text('heavy ballista'/'6ED', '\"Archers, ballistae—you can\'t even get near the island of Avenant.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('heavy ballista'/'6ED', '14486').

card_in_set('hecatomb', '6ED').
card_original_type('hecatomb'/'6ED', 'Enchantment').
card_original_text('hecatomb'/'6ED', 'When Hecatomb comes into play, you may sacrifice four creatures. If you don\'t, sacrifice Hecatomb.\nTap an untapped swamp you control: Hecatomb deals 1 damage to target creature or player.').
card_image_name('hecatomb'/'6ED', 'hecatomb').
card_uid('hecatomb'/'6ED', '6ED:Hecatomb:hecatomb').
card_rarity('hecatomb'/'6ED', 'Rare').
card_artist('hecatomb'/'6ED', 'George Pratt').
card_number('hecatomb'/'6ED', '136').
card_multiverse_id('hecatomb'/'6ED', '11319').

card_in_set('hero\'s resolve', '6ED').
card_original_type('hero\'s resolve'/'6ED', 'Enchant Creature').
card_original_text('hero\'s resolve'/'6ED', 'Enchanted creature gets +1/+5.').
card_image_name('hero\'s resolve'/'6ED', 'hero\'s resolve').
card_uid('hero\'s resolve'/'6ED', '6ED:Hero\'s Resolve:hero\'s resolve').
card_rarity('hero\'s resolve'/'6ED', 'Common').
card_artist('hero\'s resolve'/'6ED', 'Pete Venters').
card_number('hero\'s resolve'/'6ED', '24').
card_flavor_text('hero\'s resolve'/'6ED', '\"Destiny, chance, fate, fortune—they\'re all just ways of claiming your successes without claiming your failures.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('hero\'s resolve'/'6ED', '16426').

card_in_set('hidden horror', '6ED').
card_original_type('hidden horror'/'6ED', 'Creature — Horror').
card_original_text('hidden horror'/'6ED', 'When Hidden Horror comes into play, choose and discard a creature card from your hand. If you don\'t, sacrifice Hidden Horror.').
card_image_name('hidden horror'/'6ED', 'hidden horror').
card_uid('hidden horror'/'6ED', '6ED:Hidden Horror:hidden horror').
card_rarity('hidden horror'/'6ED', 'Uncommon').
card_artist('hidden horror'/'6ED', 'Clint Langley').
card_number('hidden horror'/'6ED', '137').
card_multiverse_id('hidden horror'/'6ED', '14578').

card_in_set('horned turtle', '6ED').
card_original_type('horned turtle'/'6ED', 'Creature — Turtle').
card_original_text('horned turtle'/'6ED', '').
card_image_name('horned turtle'/'6ED', 'horned turtle').
card_uid('horned turtle'/'6ED', '6ED:Horned Turtle:horned turtle').
card_rarity('horned turtle'/'6ED', 'Common').
card_artist('horned turtle'/'6ED', 'DiTerlizzi').
card_number('horned turtle'/'6ED', '74').
card_flavor_text('horned turtle'/'6ED', '\"So like men are they: a hardened shell with fragile flesh beneath.\"\n—Selenia, dark angel').
card_multiverse_id('horned turtle'/'6ED', '14524').

card_in_set('howl from beyond', '6ED').
card_original_type('howl from beyond'/'6ED', 'Instant').
card_original_text('howl from beyond'/'6ED', 'Target creature gets +X/+0 until end of turn.').
card_image_name('howl from beyond'/'6ED', 'howl from beyond').
card_uid('howl from beyond'/'6ED', '6ED:Howl from Beyond:howl from beyond').
card_rarity('howl from beyond'/'6ED', 'Common').
card_artist('howl from beyond'/'6ED', 'John Coulthart').
card_number('howl from beyond'/'6ED', '138').
card_multiverse_id('howl from beyond'/'6ED', '14562').

card_in_set('howling mine', '6ED').
card_original_type('howling mine'/'6ED', 'Artifact').
card_original_text('howling mine'/'6ED', 'At the beginning of each player\'s draw step, if Howling Mine is untapped, that player draws an additional card.').
card_image_name('howling mine'/'6ED', 'howling mine').
card_uid('howling mine'/'6ED', '6ED:Howling Mine:howling mine').
card_rarity('howling mine'/'6ED', 'Rare').
card_artist('howling mine'/'6ED', 'Mark Poole').
card_number('howling mine'/'6ED', '289').
card_multiverse_id('howling mine'/'6ED', '14777').

card_in_set('hulking cyclops', '6ED').
card_original_type('hulking cyclops'/'6ED', 'Creature — Giant').
card_original_text('hulking cyclops'/'6ED', 'Hulking Cyclops can\'t block.').
card_image_name('hulking cyclops'/'6ED', 'hulking cyclops').
card_uid('hulking cyclops'/'6ED', '6ED:Hulking Cyclops:hulking cyclops').
card_rarity('hulking cyclops'/'6ED', 'Uncommon').
card_artist('hulking cyclops'/'6ED', 'Paolo Parente').
card_number('hulking cyclops'/'6ED', '189').
card_flavor_text('hulking cyclops'/'6ED', 'Anyone can get around a cyclops, but few can stand in its way.').
card_multiverse_id('hulking cyclops'/'6ED', '14644').

card_in_set('hurricane', '6ED').
card_original_type('hurricane'/'6ED', 'Sorcery').
card_original_text('hurricane'/'6ED', 'Hurricane deals X damage to each creature with flying and each player.').
card_image_name('hurricane'/'6ED', 'hurricane').
card_uid('hurricane'/'6ED', '6ED:Hurricane:hurricane').
card_rarity('hurricane'/'6ED', 'Rare').
card_artist('hurricane'/'6ED', 'Andrew Robinson').
card_number('hurricane'/'6ED', '237').
card_multiverse_id('hurricane'/'6ED', '11117').

card_in_set('icatian town', '6ED').
card_original_type('icatian town'/'6ED', 'Sorcery').
card_original_text('icatian town'/'6ED', 'Put four 1/1 white Citizen creature tokens into play.').
card_image_name('icatian town'/'6ED', 'icatian town').
card_uid('icatian town'/'6ED', '6ED:Icatian Town:icatian town').
card_rarity('icatian town'/'6ED', 'Rare').
card_artist('icatian town'/'6ED', 'Tom Wänerstrand').
card_number('icatian town'/'6ED', '25').
card_flavor_text('icatian town'/'6ED', 'Icatia\'s once-peaceful towns faced increasing attacks from orcs and goblins as the climate cooled. By the time the empire fell, they were little more than armed camps.').
card_multiverse_id('icatian town'/'6ED', '11336').

card_in_set('illicit auction', '6ED').
card_original_type('illicit auction'/'6ED', 'Sorcery').
card_original_text('illicit auction'/'6ED', 'Choose target creature. Each player may bid life for control of that creature. You begin the bidding at 0. Proceeding in turn order, each player may top the high bid. The auction ends when the high bid stands. The high bidder loses life equal to the high bid and gains control of the creature.').
card_image_name('illicit auction'/'6ED', 'illicit auction').
card_uid('illicit auction'/'6ED', '6ED:Illicit Auction:illicit auction').
card_rarity('illicit auction'/'6ED', 'Rare').
card_artist('illicit auction'/'6ED', 'Scott Kirschner').
card_number('illicit auction'/'6ED', '190').
card_multiverse_id('illicit auction'/'6ED', '16449').

card_in_set('infantry veteran', '6ED').
card_original_type('infantry veteran'/'6ED', 'Creature — Soldier').
card_original_text('infantry veteran'/'6ED', '{T}: Target attacking creature gets +1/+1 until end of turn.').
card_image_name('infantry veteran'/'6ED', 'infantry veteran').
card_uid('infantry veteran'/'6ED', '6ED:Infantry Veteran:infantry veteran').
card_rarity('infantry veteran'/'6ED', 'Common').
card_artist('infantry veteran'/'6ED', 'Christopher Rush').
card_number('infantry veteran'/'6ED', '26').
card_flavor_text('infantry veteran'/'6ED', '\"The true dishonor for a soldier is surviving the war.\"\n—Telim\'Tor').
card_multiverse_id('infantry veteran'/'6ED', '15359').

card_in_set('infernal contract', '6ED').
card_original_type('infernal contract'/'6ED', 'Sorcery').
card_original_text('infernal contract'/'6ED', 'Draw four cards. You lose half your life, rounded up.').
card_image_name('infernal contract'/'6ED', 'infernal contract').
card_uid('infernal contract'/'6ED', '6ED:Infernal Contract:infernal contract').
card_rarity('infernal contract'/'6ED', 'Rare').
card_artist('infernal contract'/'6ED', 'Roger Raupp').
card_number('infernal contract'/'6ED', '139').
card_flavor_text('infernal contract'/'6ED', '\"But I signed nothing!\"\n—Taraneh, Suq\'Ata mage').
card_multiverse_id('infernal contract'/'6ED', '14604').

card_in_set('inferno', '6ED').
card_original_type('inferno'/'6ED', 'Instant').
card_original_text('inferno'/'6ED', 'Inferno deals 6 damage to each creature and each player.').
card_image_name('inferno'/'6ED', 'inferno').
card_uid('inferno'/'6ED', '6ED:Inferno:inferno').
card_rarity('inferno'/'6ED', 'Rare').
card_artist('inferno'/'6ED', 'Mike Kerr').
card_number('inferno'/'6ED', '191').
card_flavor_text('inferno'/'6ED', '\"Some have said there is no subtlety to destruction. You know what? They\'re dead.\" —Jaya Ballard, task mage').
card_multiverse_id('inferno'/'6ED', '11340').

card_in_set('insight', '6ED').
card_original_type('insight'/'6ED', 'Enchantment').
card_original_text('insight'/'6ED', 'Whenever one of your opponents plays a green spell, you draw a card.').
card_image_name('insight'/'6ED', 'insight').
card_uid('insight'/'6ED', '6ED:Insight:insight').
card_rarity('insight'/'6ED', 'Uncommon').
card_artist('insight'/'6ED', 'Ron Chironna').
card_number('insight'/'6ED', '75').
card_flavor_text('insight'/'6ED', '\"We can fight with you or against you. After my day, I\'d prefer the former.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('insight'/'6ED', '15376').

card_in_set('inspiration', '6ED').
card_original_type('inspiration'/'6ED', 'Instant').
card_original_text('inspiration'/'6ED', 'Target player draws two cards.').
card_image_name('inspiration'/'6ED', 'inspiration').
card_uid('inspiration'/'6ED', '6ED:Inspiration:inspiration').
card_rarity('inspiration'/'6ED', 'Common').
card_artist('inspiration'/'6ED', 'Zina Saunders').
card_number('inspiration'/'6ED', '76').
card_flavor_text('inspiration'/'6ED', '\"Madness and genius are separated only by degrees of success.\"\n—Sidar Jabari').
card_multiverse_id('inspiration'/'6ED', '14507').

card_in_set('iron star', '6ED').
card_original_type('iron star'/'6ED', 'Artifact').
card_original_text('iron star'/'6ED', 'Whenever a player plays a red spell, you may pay {1}. If you do, you gain 1 life.').
card_image_name('iron star'/'6ED', 'iron star').
card_uid('iron star'/'6ED', '6ED:Iron Star:iron star').
card_rarity('iron star'/'6ED', 'Uncommon').
card_artist('iron star'/'6ED', 'Donato Giancola').
card_number('iron star'/'6ED', '290').
card_multiverse_id('iron star'/'6ED', '14761').

card_in_set('island', '6ED').
card_original_type('island'/'6ED', 'Land').
card_original_text('island'/'6ED', 'U').
card_image_name('island'/'6ED', 'island1').
card_uid('island'/'6ED', '6ED:Island:island1').
card_rarity('island'/'6ED', 'Basic Land').
card_artist('island'/'6ED', 'Douglas Shuler').
card_number('island'/'6ED', '335').
card_multiverse_id('island'/'6ED', '14737').

card_in_set('island', '6ED').
card_original_type('island'/'6ED', 'Land').
card_original_text('island'/'6ED', 'U').
card_image_name('island'/'6ED', 'island2').
card_uid('island'/'6ED', '6ED:Island:island2').
card_rarity('island'/'6ED', 'Basic Land').
card_artist('island'/'6ED', 'J. W. Frost').
card_number('island'/'6ED', '336').
card_multiverse_id('island'/'6ED', '14738').

card_in_set('island', '6ED').
card_original_type('island'/'6ED', 'Land').
card_original_text('island'/'6ED', 'U').
card_image_name('island'/'6ED', 'island3').
card_uid('island'/'6ED', '6ED:Island:island3').
card_rarity('island'/'6ED', 'Basic Land').
card_artist('island'/'6ED', 'John Avon').
card_number('island'/'6ED', '337').
card_multiverse_id('island'/'6ED', '14739').

card_in_set('island', '6ED').
card_original_type('island'/'6ED', 'Land').
card_original_text('island'/'6ED', 'U').
card_image_name('island'/'6ED', 'island4').
card_uid('island'/'6ED', '6ED:Island:island4').
card_rarity('island'/'6ED', 'Basic Land').
card_artist('island'/'6ED', 'Eric Peterson').
card_number('island'/'6ED', '338').
card_multiverse_id('island'/'6ED', '14740').

card_in_set('ivory cup', '6ED').
card_original_type('ivory cup'/'6ED', 'Artifact').
card_original_text('ivory cup'/'6ED', 'Whenever a player plays a white spell, you may pay {1}. If you do, you gain 1 life.').
card_image_name('ivory cup'/'6ED', 'ivory cup').
card_uid('ivory cup'/'6ED', '6ED:Ivory Cup:ivory cup').
card_rarity('ivory cup'/'6ED', 'Uncommon').
card_artist('ivory cup'/'6ED', 'Donato Giancola').
card_number('ivory cup'/'6ED', '291').
card_multiverse_id('ivory cup'/'6ED', '14759').

card_in_set('jade monolith', '6ED').
card_original_type('jade monolith'/'6ED', 'Artifact').
card_original_text('jade monolith'/'6ED', '{1}: The next time a source of your choice would deal damage to target creature this turn, that damage is dealt to you instead.').
card_image_name('jade monolith'/'6ED', 'jade monolith').
card_uid('jade monolith'/'6ED', '6ED:Jade Monolith:jade monolith').
card_rarity('jade monolith'/'6ED', 'Rare').
card_artist('jade monolith'/'6ED', 'Richard Kane Ferguson').
card_number('jade monolith'/'6ED', '292').
card_multiverse_id('jade monolith'/'6ED', '11355').

card_in_set('jalum tome', '6ED').
card_original_type('jalum tome'/'6ED', 'Artifact').
card_original_text('jalum tome'/'6ED', '{2}, {T}: Draw a card, then choose and discard a card from your hand.').
card_image_name('jalum tome'/'6ED', 'jalum tome').
card_uid('jalum tome'/'6ED', '6ED:Jalum Tome:jalum tome').
card_rarity('jalum tome'/'6ED', 'Rare').
card_artist('jalum tome'/'6ED', 'Tom Wänerstrand').
card_number('jalum tome'/'6ED', '293').
card_flavor_text('jalum tome'/'6ED', 'This timeworn relic was responsible for many of Urza\'s victories, though he never fully comprehended its mystic runes.').
card_multiverse_id('jalum tome'/'6ED', '14778').

card_in_set('jayemdae tome', '6ED').
card_original_type('jayemdae tome'/'6ED', 'Artifact').
card_original_text('jayemdae tome'/'6ED', '{4}, {T}: Draw a card.').
card_image_name('jayemdae tome'/'6ED', 'jayemdae tome').
card_uid('jayemdae tome'/'6ED', '6ED:Jayemdae Tome:jayemdae tome').
card_rarity('jayemdae tome'/'6ED', 'Rare').
card_artist('jayemdae tome'/'6ED', 'Mark Tedin').
card_number('jayemdae tome'/'6ED', '294').
card_multiverse_id('jayemdae tome'/'6ED', '14780').

card_in_set('jokulhaups', '6ED').
card_original_type('jokulhaups'/'6ED', 'Sorcery').
card_original_text('jokulhaups'/'6ED', 'Destroy all artifacts, creatures, and lands. They can\'t be regenerated.').
card_image_name('jokulhaups'/'6ED', 'jokulhaups').
card_uid('jokulhaups'/'6ED', '6ED:Jokulhaups:jokulhaups').
card_rarity('jokulhaups'/'6ED', 'Rare').
card_artist('jokulhaups'/'6ED', 'Mike Kerr').
card_number('jokulhaups'/'6ED', '192').
card_flavor_text('jokulhaups'/'6ED', '\"The raging waters had swept away trees, bridges, and even houses. My healers had much work to do.\"\n—Halvor Arenson, Kjeldoran priest').
card_multiverse_id('jokulhaups'/'6ED', '14653').

card_in_set('juxtapose', '6ED').
card_original_type('juxtapose'/'6ED', 'Sorcery').
card_original_text('juxtapose'/'6ED', 'You and target player exchange control of the creature you each control with the highest converted mana cost. Then exchange control of artifacts the same way. (If two or more permanents a player controls are tied for highest cost, that player chooses between them.)').
card_image_name('juxtapose'/'6ED', 'juxtapose').
card_uid('juxtapose'/'6ED', '6ED:Juxtapose:juxtapose').
card_rarity('juxtapose'/'6ED', 'Rare').
card_artist('juxtapose'/'6ED', 'Justin Hampton').
card_number('juxtapose'/'6ED', '77').
card_multiverse_id('juxtapose'/'6ED', '11364').

card_in_set('karplusan forest', '6ED').
card_original_type('karplusan forest'/'6ED', 'Land').
card_original_text('karplusan forest'/'6ED', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {R} or {G} to your mana pool. Karplusan Forest deals 1 damage to you.').
card_image_name('karplusan forest'/'6ED', 'karplusan forest').
card_uid('karplusan forest'/'6ED', '6ED:Karplusan Forest:karplusan forest').
card_rarity('karplusan forest'/'6ED', 'Rare').
card_artist('karplusan forest'/'6ED', 'Randy Gallegos').
card_number('karplusan forest'/'6ED', '326').
card_multiverse_id('karplusan forest'/'6ED', '14730').

card_in_set('kismet', '6ED').
card_original_type('kismet'/'6ED', 'Enchantment').
card_original_text('kismet'/'6ED', 'Artifacts, creatures, and lands your opponents play come into play tapped.').
card_image_name('kismet'/'6ED', 'kismet').
card_uid('kismet'/'6ED', '6ED:Kismet:kismet').
card_rarity('kismet'/'6ED', 'Uncommon').
card_artist('kismet'/'6ED', 'Kaja Foglio').
card_number('kismet'/'6ED', '27').
card_flavor_text('kismet'/'6ED', '\"Make people wait for what they want, and you have power over them. This is as true for merchants and militia as it is for cooks and couples.\"\n—Gwendlyn Di Corci').
card_multiverse_id('kismet'/'6ED', '14483').

card_in_set('kjeldoran dead', '6ED').
card_original_type('kjeldoran dead'/'6ED', 'Creature — Skeleton').
card_original_text('kjeldoran dead'/'6ED', 'When Kjeldoran Dead comes into play, sacrifice a creature.\n{B} Regenerate Kjeldoran Dead.').
card_image_name('kjeldoran dead'/'6ED', 'kjeldoran dead').
card_uid('kjeldoran dead'/'6ED', '6ED:Kjeldoran Dead:kjeldoran dead').
card_rarity('kjeldoran dead'/'6ED', 'Common').
card_artist('kjeldoran dead'/'6ED', 'Melissa A. Benson').
card_number('kjeldoran dead'/'6ED', '140').
card_flavor_text('kjeldoran dead'/'6ED', '\"They shall drink sorrow\'s dregs. They shall kill those whom once they loved.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('kjeldoran dead'/'6ED', '20179').

card_in_set('kjeldoran royal guard', '6ED').
card_original_type('kjeldoran royal guard'/'6ED', 'Creature — Soldier').
card_original_text('kjeldoran royal guard'/'6ED', '{T}: All combat damage that would be dealt to you by unblocked creatures this turn is dealt to Kjeldoran Royal Guard instead.').
card_image_name('kjeldoran royal guard'/'6ED', 'kjeldoran royal guard').
card_uid('kjeldoran royal guard'/'6ED', '6ED:Kjeldoran Royal Guard:kjeldoran royal guard').
card_rarity('kjeldoran royal guard'/'6ED', 'Rare').
card_artist('kjeldoran royal guard'/'6ED', 'L. A. Williams').
card_number('kjeldoran royal guard'/'6ED', '28').
card_flavor_text('kjeldoran royal guard'/'6ED', 'Honorable in battle, generous in death.\n—Motto of the Kjeldoran Royal Guard').
card_multiverse_id('kjeldoran royal guard'/'6ED', '11371').

card_in_set('lead golem', '6ED').
card_original_type('lead golem'/'6ED', 'Artifact Creature — Golem').
card_original_text('lead golem'/'6ED', 'Whenever Lead Golem attacks, it doesn\'t untap during its controller\'s next untap step.').
card_image_name('lead golem'/'6ED', 'lead golem').
card_uid('lead golem'/'6ED', '6ED:Lead Golem:lead golem').
card_rarity('lead golem'/'6ED', 'Uncommon').
card_artist('lead golem'/'6ED', 'Hannibal King').
card_number('lead golem'/'6ED', '295').
card_flavor_text('lead golem'/'6ED', 'Slow and heavy swings the club.').
card_multiverse_id('lead golem'/'6ED', '15407').

card_in_set('leshrac\'s rite', '6ED').
card_original_type('leshrac\'s rite'/'6ED', 'Enchant Creature').
card_original_text('leshrac\'s rite'/'6ED', 'Enchanted creature gains swampwalk. (It\'s unblockable if defending player controls a swamp.)').
card_image_name('leshrac\'s rite'/'6ED', 'leshrac\'s rite').
card_uid('leshrac\'s rite'/'6ED', '6ED:Leshrac\'s Rite:leshrac\'s rite').
card_rarity('leshrac\'s rite'/'6ED', 'Uncommon').
card_artist('leshrac\'s rite'/'6ED', 'Mike Raabe').
card_number('leshrac\'s rite'/'6ED', '141').
card_flavor_text('leshrac\'s rite'/'6ED', '\"Bind me to thee, my soul to thine. I am your servant and your slave. Blood for blood, flesh for flesh, Leshrac, my lord.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('leshrac\'s rite'/'6ED', '14585').

card_in_set('library of lat-nam', '6ED').
card_original_type('library of lat-nam'/'6ED', 'Sorcery').
card_original_text('library of lat-nam'/'6ED', 'Target opponent chooses one You draw three cards at the beginning of the next turn\'s upkeep; or you search your library for a card, put that card into your hand, and then shuffle your library.').
card_image_name('library of lat-nam'/'6ED', 'library of lat-nam').
card_uid('library of lat-nam'/'6ED', '6ED:Library of Lat-Nam:library of lat-nam').
card_rarity('library of lat-nam'/'6ED', 'Rare').
card_artist('library of lat-nam'/'6ED', 'Alan Rabinowitz').
card_number('library of lat-nam'/'6ED', '78').
card_multiverse_id('library of lat-nam'/'6ED', '15364').

card_in_set('light of day', '6ED').
card_original_type('light of day'/'6ED', 'Enchantment').
card_original_text('light of day'/'6ED', 'Black creatures can\'t attack or block.').
card_image_name('light of day'/'6ED', 'light of day').
card_uid('light of day'/'6ED', '6ED:Light of Day:light of day').
card_rarity('light of day'/'6ED', 'Uncommon').
card_artist('light of day'/'6ED', 'Drew Tucker').
card_number('light of day'/'6ED', '29').
card_flavor_text('light of day'/'6ED', '\"I do not miss sunlight. The very memory of it burns my eyes.\"\n—Volrath').
card_multiverse_id('light of day'/'6ED', '14481').

card_in_set('lightning blast', '6ED').
card_original_type('lightning blast'/'6ED', 'Instant').
card_original_text('lightning blast'/'6ED', 'Lightning Blast deals 4 damage to target creature or player.').
card_image_name('lightning blast'/'6ED', 'lightning blast').
card_uid('lightning blast'/'6ED', '6ED:Lightning Blast:lightning blast').
card_rarity('lightning blast'/'6ED', 'Common').
card_artist('lightning blast'/'6ED', 'Richard Thomas').
card_number('lightning blast'/'6ED', '193').
card_flavor_text('lightning blast'/'6ED', '\"Those who fear the darkness have never seen what the light can do.\"\n—Selenia, dark angel').
card_multiverse_id('lightning blast'/'6ED', '14610').

card_in_set('living lands', '6ED').
card_original_type('living lands'/'6ED', 'Enchantment').
card_original_text('living lands'/'6ED', 'All forests are 1/1 creatures that are still lands.').
card_image_name('living lands'/'6ED', 'living lands').
card_uid('living lands'/'6ED', '6ED:Living Lands:living lands').
card_rarity('living lands'/'6ED', 'Rare').
card_artist('living lands'/'6ED', 'John Matson').
card_number('living lands'/'6ED', '238').
card_multiverse_id('living lands'/'6ED', '11385').

card_in_set('llanowar elves', '6ED').
card_original_type('llanowar elves'/'6ED', 'Creature — Elf').
card_original_text('llanowar elves'/'6ED', '{T}: Add {G} to your mana pool.').
card_image_name('llanowar elves'/'6ED', 'llanowar elves').
card_uid('llanowar elves'/'6ED', '6ED:Llanowar Elves:llanowar elves').
card_rarity('llanowar elves'/'6ED', 'Common').
card_artist('llanowar elves'/'6ED', 'Anson Maddocks').
card_number('llanowar elves'/'6ED', '239').
card_flavor_text('llanowar elves'/'6ED', 'One bone broken for every twig snapped underfoot.\n—Llanowar penalty for trespassing').
card_multiverse_id('llanowar elves'/'6ED', '14668').

card_in_set('longbow archer', '6ED').
card_original_type('longbow archer'/'6ED', 'Creature — Soldier').
card_original_text('longbow archer'/'6ED', 'First strike\nLongbow Archer can block as though it had flying.').
card_image_name('longbow archer'/'6ED', 'longbow archer').
card_uid('longbow archer'/'6ED', '6ED:Longbow Archer:longbow archer').
card_rarity('longbow archer'/'6ED', 'Uncommon').
card_artist('longbow archer'/'6ED', 'Eric Peterson').
card_number('longbow archer'/'6ED', '30').
card_flavor_text('longbow archer'/'6ED', '\"If it bears wings, I will pin it to the skies over Tefemburu.\"\n—Roya, Zhalfirin archer').
card_multiverse_id('longbow archer'/'6ED', '14475').

card_in_set('lord of atlantis', '6ED').
card_original_type('lord of atlantis'/'6ED', 'Creature — Lord').
card_original_text('lord of atlantis'/'6ED', 'All Merfolk get +1/+1 and gain islandwalk. (They\'re unblockable if defending player controls an island.)').
card_image_name('lord of atlantis'/'6ED', 'lord of atlantis').
card_uid('lord of atlantis'/'6ED', '6ED:Lord of Atlantis:lord of atlantis').
card_rarity('lord of atlantis'/'6ED', 'Rare').
card_artist('lord of atlantis'/'6ED', 'Melissa A. Benson').
card_number('lord of atlantis'/'6ED', '79').
card_flavor_text('lord of atlantis'/'6ED', 'A master of tactics, the lord of Atlantis makes his people bold in battle merely by arriving to lead them.').
card_multiverse_id('lord of atlantis'/'6ED', '14551').

card_in_set('lost soul', '6ED').
card_original_type('lost soul'/'6ED', 'Creature — Minion').
card_original_text('lost soul'/'6ED', 'Swampwalk (This creature is unblockable if defending player controls a swamp.)').
card_image_name('lost soul'/'6ED', 'lost soul').
card_uid('lost soul'/'6ED', '6ED:Lost Soul:lost soul').
card_rarity('lost soul'/'6ED', 'Common').
card_artist('lost soul'/'6ED', 'Randy Asplund-Faith').
card_number('lost soul'/'6ED', '142').
card_flavor_text('lost soul'/'6ED', 'Her hand gently beckons, she whispers your name—but those who go with her are never the same.').
card_multiverse_id('lost soul'/'6ED', '20178').

card_in_set('lure', '6ED').
card_original_type('lure'/'6ED', 'Enchant Creature').
card_original_text('lure'/'6ED', 'All creatures able to block enchanted creature do so.').
card_image_name('lure'/'6ED', 'lure').
card_uid('lure'/'6ED', '6ED:Lure:lure').
card_rarity('lure'/'6ED', 'Uncommon').
card_artist('lure'/'6ED', 'Anson Maddocks').
card_number('lure'/'6ED', '240').
card_flavor_text('lure'/'6ED', 'When setting a snare, success depends not on the quality of the trap, but the quality of the bait.').
card_multiverse_id('lure'/'6ED', '14697').

card_in_set('mana prism', '6ED').
card_original_type('mana prism'/'6ED', 'Artifact').
card_original_text('mana prism'/'6ED', '{T}: Add one colorless mana to your mana pool.\n{1}, {T}: Add one mana of a color of your choice to your mana pool.').
card_image_name('mana prism'/'6ED', 'mana prism').
card_uid('mana prism'/'6ED', '6ED:Mana Prism:mana prism').
card_rarity('mana prism'/'6ED', 'Uncommon').
card_artist('mana prism'/'6ED', 'Margaret Organ-Kean').
card_number('mana prism'/'6ED', '296').
card_multiverse_id('mana prism'/'6ED', '15439').

card_in_set('mana short', '6ED').
card_original_type('mana short'/'6ED', 'Instant').
card_original_text('mana short'/'6ED', 'Tap all lands target player controls and empty his or her mana pool.').
card_image_name('mana short'/'6ED', 'mana short').
card_uid('mana short'/'6ED', '6ED:Mana Short:mana short').
card_rarity('mana short'/'6ED', 'Rare').
card_artist('mana short'/'6ED', 'Dameon Willich').
card_number('mana short'/'6ED', '80').
card_multiverse_id('mana short'/'6ED', '16439').

card_in_set('manabarbs', '6ED').
card_original_type('manabarbs'/'6ED', 'Enchantment').
card_original_text('manabarbs'/'6ED', 'Whenever a player taps a land for mana, Manabarbs deals 1 damage to him or her.').
card_image_name('manabarbs'/'6ED', 'manabarbs').
card_uid('manabarbs'/'6ED', '6ED:Manabarbs:manabarbs').
card_rarity('manabarbs'/'6ED', 'Rare').
card_artist('manabarbs'/'6ED', 'Christopher Rush').
card_number('manabarbs'/'6ED', '194').
card_multiverse_id('manabarbs'/'6ED', '14654').

card_in_set('marble diamond', '6ED').
card_original_type('marble diamond'/'6ED', 'Artifact').
card_original_text('marble diamond'/'6ED', 'Marble Diamond comes into play tapped.\n{T}: Add {W} to your mana pool.').
card_image_name('marble diamond'/'6ED', 'marble diamond').
card_uid('marble diamond'/'6ED', '6ED:Marble Diamond:marble diamond').
card_rarity('marble diamond'/'6ED', 'Uncommon').
card_artist('marble diamond'/'6ED', 'Jeff Miracola').
card_number('marble diamond'/'6ED', '297').
card_multiverse_id('marble diamond'/'6ED', '15441').

card_in_set('maro', '6ED').
card_original_type('maro'/'6ED', 'Creature — Elemental').
card_original_text('maro'/'6ED', 'Maro\'s power and toughness are each equal to the number of cards in your hand.').
card_image_name('maro'/'6ED', 'maro').
card_uid('maro'/'6ED', '6ED:Maro:maro').
card_rarity('maro'/'6ED', 'Rare').
card_artist('maro'/'6ED', 'Stuart Griffin').
card_number('maro'/'6ED', '241').
card_flavor_text('maro'/'6ED', 'No two see the same Maro.').
card_multiverse_id('maro'/'6ED', '14718').

card_in_set('meekstone', '6ED').
card_original_type('meekstone'/'6ED', 'Artifact').
card_original_text('meekstone'/'6ED', 'Each creature with power 3 or greater doesn\'t untap during its controller\'s untap step.').
card_image_name('meekstone'/'6ED', 'meekstone').
card_uid('meekstone'/'6ED', '6ED:Meekstone:meekstone').
card_rarity('meekstone'/'6ED', 'Rare').
card_artist('meekstone'/'6ED', 'Quinton Hoover').
card_number('meekstone'/'6ED', '298').
card_flavor_text('meekstone'/'6ED', 'When the mighty sleep, the lowly prowl.').
card_multiverse_id('meekstone'/'6ED', '14781').

card_in_set('memory lapse', '6ED').
card_original_type('memory lapse'/'6ED', 'Instant').
card_original_text('memory lapse'/'6ED', 'Counter target spell. Put it on top of its owner\'s library instead of into that player\'s graveyard.').
card_image_name('memory lapse'/'6ED', 'memory lapse').
card_uid('memory lapse'/'6ED', '6ED:Memory Lapse:memory lapse').
card_rarity('memory lapse'/'6ED', 'Common').
card_artist('memory lapse'/'6ED', 'Mark Tedin').
card_number('memory lapse'/'6ED', '81').
card_flavor_text('memory lapse'/'6ED', '\"Um . . . oh . . . what was I saying?\"\n—Reveka, wizard savant').
card_multiverse_id('memory lapse'/'6ED', '11399').

card_in_set('merfolk of the pearl trident', '6ED').
card_original_type('merfolk of the pearl trident'/'6ED', 'Creature — Merfolk').
card_original_text('merfolk of the pearl trident'/'6ED', '').
card_image_name('merfolk of the pearl trident'/'6ED', 'merfolk of the pearl trident').
card_uid('merfolk of the pearl trident'/'6ED', '6ED:Merfolk of the Pearl Trident:merfolk of the pearl trident').
card_rarity('merfolk of the pearl trident'/'6ED', 'Common').
card_artist('merfolk of the pearl trident'/'6ED', 'DiTerlizzi').
card_number('merfolk of the pearl trident'/'6ED', '82').
card_flavor_text('merfolk of the pearl trident'/'6ED', 'Are merfolk humans with fins, or are humans merfolk with feet?').
card_multiverse_id('merfolk of the pearl trident'/'6ED', '11400').

card_in_set('mesa falcon', '6ED').
card_original_type('mesa falcon'/'6ED', 'Creature — Bird').
card_original_text('mesa falcon'/'6ED', 'Flying\n{1}{W} Mesa Falcon gets +0/+1 until end of turn.').
card_image_name('mesa falcon'/'6ED', 'mesa falcon').
card_uid('mesa falcon'/'6ED', '6ED:Mesa Falcon:mesa falcon').
card_rarity('mesa falcon'/'6ED', 'Common').
card_artist('mesa falcon'/'6ED', 'Mark Poole').
card_number('mesa falcon'/'6ED', '31').
card_flavor_text('mesa falcon'/'6ED', '\"The faith of Serra is borne on wings of hope.\"\n—Gulsen, abbey matron').
card_multiverse_id('mesa falcon'/'6ED', '16427').

card_in_set('millstone', '6ED').
card_original_type('millstone'/'6ED', 'Artifact').
card_original_text('millstone'/'6ED', '{2}, {T}: Put the top two cards of target player\'s library into his or her graveyard.').
card_image_name('millstone'/'6ED', 'millstone').
card_uid('millstone'/'6ED', '6ED:Millstone:millstone').
card_rarity('millstone'/'6ED', 'Rare').
card_artist('millstone'/'6ED', 'Kaja Foglio').
card_number('millstone'/'6ED', '299').
card_flavor_text('millstone'/'6ED', 'More than one mage has been driven insane by the sound of the millstone relentlessly grinding away.').
card_multiverse_id('millstone'/'6ED', '14782').

card_in_set('mind warp', '6ED').
card_original_type('mind warp'/'6ED', 'Sorcery').
card_original_text('mind warp'/'6ED', 'Look at target player\'s hand and choose X cards from it. That player discards them.').
card_image_name('mind warp'/'6ED', 'mind warp').
card_uid('mind warp'/'6ED', '6ED:Mind Warp:mind warp').
card_rarity('mind warp'/'6ED', 'Uncommon').
card_artist('mind warp'/'6ED', 'Liz Danforth').
card_number('mind warp'/'6ED', '143').
card_flavor_text('mind warp'/'6ED', 'The hardest of wizards can have the softest of minds.').
card_multiverse_id('mind warp'/'6ED', '14584').

card_in_set('mischievous poltergeist', '6ED').
card_original_type('mischievous poltergeist'/'6ED', 'Creature — Ghost').
card_original_text('mischievous poltergeist'/'6ED', 'Flying\nPay 1 life: Regenerate Mischievous Poltergeist.').
card_image_name('mischievous poltergeist'/'6ED', 'mischievous poltergeist').
card_uid('mischievous poltergeist'/'6ED', '6ED:Mischievous Poltergeist:mischievous poltergeist').
card_rarity('mischievous poltergeist'/'6ED', 'Uncommon').
card_artist('mischievous poltergeist'/'6ED', 'DiTerlizzi').
card_number('mischievous poltergeist'/'6ED', '144').
card_flavor_text('mischievous poltergeist'/'6ED', '\"The past is a ghost that haunts you from the moment it exists until the moment you don\'t.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('mischievous poltergeist'/'6ED', '15398').

card_in_set('moss diamond', '6ED').
card_original_type('moss diamond'/'6ED', 'Artifact').
card_original_text('moss diamond'/'6ED', 'Moss Diamond comes into play tapped.\n{T}: Add {G} to your mana pool.').
card_image_name('moss diamond'/'6ED', 'moss diamond').
card_uid('moss diamond'/'6ED', '6ED:Moss Diamond:moss diamond').
card_rarity('moss diamond'/'6ED', 'Uncommon').
card_artist('moss diamond'/'6ED', 'Donato Giancola').
card_number('moss diamond'/'6ED', '300').
card_multiverse_id('moss diamond'/'6ED', '15442').

card_in_set('mountain', '6ED').
card_original_type('mountain'/'6ED', 'Land').
card_original_text('mountain'/'6ED', 'R').
card_image_name('mountain'/'6ED', 'mountain1').
card_uid('mountain'/'6ED', '6ED:Mountain:mountain1').
card_rarity('mountain'/'6ED', 'Basic Land').
card_artist('mountain'/'6ED', 'John Avon').
card_number('mountain'/'6ED', '343').
card_multiverse_id('mountain'/'6ED', '14745').

card_in_set('mountain', '6ED').
card_original_type('mountain'/'6ED', 'Land').
card_original_text('mountain'/'6ED', 'R').
card_image_name('mountain'/'6ED', 'mountain2').
card_uid('mountain'/'6ED', '6ED:Mountain:mountain2').
card_rarity('mountain'/'6ED', 'Basic Land').
card_artist('mountain'/'6ED', 'John Avon').
card_number('mountain'/'6ED', '344').
card_multiverse_id('mountain'/'6ED', '14746').

card_in_set('mountain', '6ED').
card_original_type('mountain'/'6ED', 'Land').
card_original_text('mountain'/'6ED', 'R').
card_image_name('mountain'/'6ED', 'mountain3').
card_uid('mountain'/'6ED', '6ED:Mountain:mountain3').
card_rarity('mountain'/'6ED', 'Basic Land').
card_artist('mountain'/'6ED', 'John Avon').
card_number('mountain'/'6ED', '345').
card_multiverse_id('mountain'/'6ED', '14747').

card_in_set('mountain', '6ED').
card_original_type('mountain'/'6ED', 'Land').
card_original_text('mountain'/'6ED', 'R').
card_image_name('mountain'/'6ED', 'mountain4').
card_uid('mountain'/'6ED', '6ED:Mountain:mountain4').
card_rarity('mountain'/'6ED', 'Basic Land').
card_artist('mountain'/'6ED', 'Brian Durfee').
card_number('mountain'/'6ED', '346').
card_multiverse_id('mountain'/'6ED', '14748').

card_in_set('mountain goat', '6ED').
card_original_type('mountain goat'/'6ED', 'Creature — Goat').
card_original_text('mountain goat'/'6ED', 'Mountainwalk (This creature is unblockable if defending player controls a mountain.)').
card_image_name('mountain goat'/'6ED', 'mountain goat').
card_uid('mountain goat'/'6ED', '6ED:Mountain Goat:mountain goat').
card_rarity('mountain goat'/'6ED', 'Common').
card_artist('mountain goat'/'6ED', 'Cornelius Brudi').
card_number('mountain goat'/'6ED', '195').
card_flavor_text('mountain goat'/'6ED', 'Only the heroic and the mad follow mountain goat trails.').
card_multiverse_id('mountain goat'/'6ED', '16442').

card_in_set('mystic compass', '6ED').
card_original_type('mystic compass'/'6ED', 'Artifact').
card_original_text('mystic compass'/'6ED', '{1}, {T}: Target land is a basic land type of your choice until end of turn.').
card_image_name('mystic compass'/'6ED', 'mystic compass').
card_uid('mystic compass'/'6ED', '6ED:Mystic Compass:mystic compass').
card_rarity('mystic compass'/'6ED', 'Uncommon').
card_artist('mystic compass'/'6ED', 'Amy Weber').
card_number('mystic compass'/'6ED', '301').
card_multiverse_id('mystic compass'/'6ED', '15435').

card_in_set('mystical tutor', '6ED').
card_original_type('mystical tutor'/'6ED', 'Instant').
card_original_text('mystical tutor'/'6ED', 'Search your library for an instant or sorcery card and reveal that card. Shuffle your library, then put the card on top of it.').
card_image_name('mystical tutor'/'6ED', 'mystical tutor').
card_uid('mystical tutor'/'6ED', '6ED:Mystical Tutor:mystical tutor').
card_rarity('mystical tutor'/'6ED', 'Uncommon').
card_artist('mystical tutor'/'6ED', 'David O\'Connor').
card_number('mystical tutor'/'6ED', '83').
card_flavor_text('mystical tutor'/'6ED', '\"To the tutors, a ‘poem of sand\' was of little account, a ‘poem of ivory,\' priceless.\"\n—Afari, Tales').
card_multiverse_id('mystical tutor'/'6ED', '15367').

card_in_set('nature\'s resurgence', '6ED').
card_original_type('nature\'s resurgence'/'6ED', 'Sorcery').
card_original_text('nature\'s resurgence'/'6ED', 'Each player draws as many cards as there are creature cards in his or her graveyard.').
card_image_name('nature\'s resurgence'/'6ED', 'nature\'s resurgence').
card_uid('nature\'s resurgence'/'6ED', '6ED:Nature\'s Resurgence:nature\'s resurgence').
card_rarity('nature\'s resurgence'/'6ED', 'Rare').
card_artist('nature\'s resurgence'/'6ED', 'Scott M. Fischer').
card_number('nature\'s resurgence'/'6ED', '242').
card_flavor_text('nature\'s resurgence'/'6ED', 'Spring follows winter\n—Elvish expression meaning\n\"all things pass\"').
card_multiverse_id('nature\'s resurgence'/'6ED', '14715').

card_in_set('necrosavant', '6ED').
card_original_type('necrosavant'/'6ED', 'Creature — Necrosavant').
card_original_text('necrosavant'/'6ED', '{3}{B}{B}, Sacrifice a creature: Return Necrosavant from your graveyard to play.\nPlay this ability only during your upkeep.').
card_image_name('necrosavant'/'6ED', 'necrosavant').
card_uid('necrosavant'/'6ED', '6ED:Necrosavant:necrosavant').
card_rarity('necrosavant'/'6ED', 'Rare').
card_artist('necrosavant'/'6ED', 'John Coulthart').
card_number('necrosavant'/'6ED', '145').
card_multiverse_id('necrosavant'/'6ED', '15390').

card_in_set('nightmare', '6ED').
card_original_type('nightmare'/'6ED', 'Creature — Nightmare').
card_original_text('nightmare'/'6ED', 'Flying\nNightmare\'s power and toughness are each equal to the number of swamps you control.').
card_image_name('nightmare'/'6ED', 'nightmare').
card_uid('nightmare'/'6ED', '6ED:Nightmare:nightmare').
card_rarity('nightmare'/'6ED', 'Rare').
card_artist('nightmare'/'6ED', 'Melissa A. Benson').
card_number('nightmare'/'6ED', '146').
card_flavor_text('nightmare'/'6ED', 'As the poisoned land spreads, so does the nightmare\'s rage and terrifying strength.').
card_multiverse_id('nightmare'/'6ED', '15394').

card_in_set('obsianus golem', '6ED').
card_original_type('obsianus golem'/'6ED', 'Artifact Creature — Golem').
card_original_text('obsianus golem'/'6ED', '').
card_image_name('obsianus golem'/'6ED', 'obsianus golem').
card_uid('obsianus golem'/'6ED', '6ED:Obsianus Golem:obsianus golem').
card_rarity('obsianus golem'/'6ED', 'Uncommon').
card_artist('obsianus golem'/'6ED', 'Jesper Myrfors').
card_number('obsianus golem'/'6ED', '302').
card_flavor_text('obsianus golem'/'6ED', '\"The foot stone is connected to the ankle stone, the ankle stone is connected to the leg stone . . . .\"\n—\"Song of the Artificer\"').
card_multiverse_id('obsianus golem'/'6ED', '15401').

card_in_set('orcish artillery', '6ED').
card_original_type('orcish artillery'/'6ED', 'Creature — Orc').
card_original_text('orcish artillery'/'6ED', '{T}: Orcish Artillery deals 2 damage to target creature or player and 3 damage to you.').
card_image_name('orcish artillery'/'6ED', 'orcish artillery').
card_uid('orcish artillery'/'6ED', '6ED:Orcish Artillery:orcish artillery').
card_rarity('orcish artillery'/'6ED', 'Uncommon').
card_artist('orcish artillery'/'6ED', 'Dan Frazier').
card_number('orcish artillery'/'6ED', '196').
card_flavor_text('orcish artillery'/'6ED', 'In a rare display of ingenuity, the orcs invented an incredibly destructive weapon. Most orcish artillerists are those who dared criticize its effectiveness.').
card_multiverse_id('orcish artillery'/'6ED', '11423').

card_in_set('orcish oriflamme', '6ED').
card_original_type('orcish oriflamme'/'6ED', 'Enchantment').
card_original_text('orcish oriflamme'/'6ED', 'Attacking creatures you control get +1/+0.').
card_image_name('orcish oriflamme'/'6ED', 'orcish oriflamme').
card_uid('orcish oriflamme'/'6ED', '6ED:Orcish Oriflamme:orcish oriflamme').
card_rarity('orcish oriflamme'/'6ED', 'Uncommon').
card_artist('orcish oriflamme'/'6ED', 'Dan Frazier').
card_number('orcish oriflamme'/'6ED', '197').
card_multiverse_id('orcish oriflamme'/'6ED', '14635').

card_in_set('order of the sacred torch', '6ED').
card_original_type('order of the sacred torch'/'6ED', 'Creature — Paladin').
card_original_text('order of the sacred torch'/'6ED', '{T}, Pay 1 life: Counter target black spell.').
card_image_name('order of the sacred torch'/'6ED', 'order of the sacred torch').
card_uid('order of the sacred torch'/'6ED', '6ED:Order of the Sacred Torch:order of the sacred torch').
card_rarity('order of the sacred torch'/'6ED', 'Rare').
card_artist('order of the sacred torch'/'6ED', 'Ruth Thompson').
card_number('order of the sacred torch'/'6ED', '32').
card_flavor_text('order of the sacred torch'/'6ED', 'A blazing light to drive out the shadows.').
card_multiverse_id('order of the sacred torch'/'6ED', '11429').

card_in_set('ornithopter', '6ED').
card_original_type('ornithopter'/'6ED', 'Artifact Creature').
card_original_text('ornithopter'/'6ED', 'Flying').
card_image_name('ornithopter'/'6ED', 'ornithopter').
card_uid('ornithopter'/'6ED', '6ED:Ornithopter:ornithopter').
card_rarity('ornithopter'/'6ED', 'Uncommon').
card_artist('ornithopter'/'6ED', 'Amy Weber').
card_number('ornithopter'/'6ED', '303').
card_flavor_text('ornithopter'/'6ED', '\"It has been my honor to improve on the Thran\'s original design. Perhaps history will remember me in some small part for my work.\"\n—Urza, in his apprenticeship').
card_multiverse_id('ornithopter'/'6ED', '14767').

card_in_set('pacifism', '6ED').
card_original_type('pacifism'/'6ED', 'Enchant Creature').
card_original_text('pacifism'/'6ED', 'Enchanted creature can\'t attack or block.').
card_image_name('pacifism'/'6ED', 'pacifism').
card_uid('pacifism'/'6ED', '6ED:Pacifism:pacifism').
card_rarity('pacifism'/'6ED', 'Common').
card_artist('pacifism'/'6ED', 'Robert Bliss').
card_number('pacifism'/'6ED', '33').
card_flavor_text('pacifism'/'6ED', 'For the first time in his life, Grakk felt a little warm and fuzzy inside.').
card_multiverse_id('pacifism'/'6ED', '14460').

card_in_set('painful memories', '6ED').
card_original_type('painful memories'/'6ED', 'Sorcery').
card_original_text('painful memories'/'6ED', 'Look at target opponent\'s hand and choose a card from it. Put that card on top of that player\'s library.').
card_image_name('painful memories'/'6ED', 'painful memories').
card_uid('painful memories'/'6ED', '6ED:Painful Memories:painful memories').
card_rarity('painful memories'/'6ED', 'Common').
card_artist('painful memories'/'6ED', 'John Coulthart').
card_number('painful memories'/'6ED', '147').
card_flavor_text('painful memories'/'6ED', '\"It is terrible how one brief action can live forever in memory.\"\n—Jolrael').
card_multiverse_id('painful memories'/'6ED', '14564').

card_in_set('panther warriors', '6ED').
card_original_type('panther warriors'/'6ED', 'Creature — Cat Warrior').
card_original_text('panther warriors'/'6ED', '').
card_image_name('panther warriors'/'6ED', 'panther warriors').
card_uid('panther warriors'/'6ED', '6ED:Panther Warriors:panther warriors').
card_rarity('panther warriors'/'6ED', 'Common').
card_artist('panther warriors'/'6ED', 'Eric Peterson').
card_number('panther warriors'/'6ED', '243').
card_flavor_text('panther warriors'/'6ED', 'The dogs of war are nothing compared to the cats.').
card_multiverse_id('panther warriors'/'6ED', '16453').

card_in_set('patagia golem', '6ED').
card_original_type('patagia golem'/'6ED', 'Artifact Creature — Golem').
card_original_text('patagia golem'/'6ED', '{3}: Patagia Golem gains flying until end of turn.').
card_image_name('patagia golem'/'6ED', 'patagia golem').
card_uid('patagia golem'/'6ED', '6ED:Patagia Golem:patagia golem').
card_rarity('patagia golem'/'6ED', 'Uncommon').
card_artist('patagia golem'/'6ED', 'Scott Kirschner').
card_number('patagia golem'/'6ED', '304').
card_flavor_text('patagia golem'/'6ED', 'It scattered falcons like a lion hunting among jackals.').
card_multiverse_id('patagia golem'/'6ED', '15408').

card_in_set('pearl dragon', '6ED').
card_original_type('pearl dragon'/'6ED', 'Creature — Dragon').
card_original_text('pearl dragon'/'6ED', 'Flying\n{1}{W} Pearl Dragon gets +0/+1 until end of turn.').
card_image_name('pearl dragon'/'6ED', 'pearl dragon').
card_uid('pearl dragon'/'6ED', '6ED:Pearl Dragon:pearl dragon').
card_rarity('pearl dragon'/'6ED', 'Rare').
card_artist('pearl dragon'/'6ED', 'Ian Miller').
card_number('pearl dragon'/'6ED', '34').
card_flavor_text('pearl dragon'/'6ED', '\"They knew Bantau was lost when, to hide the pearl he had found on the beach, he swallowed it—and soon grew wings.\"\n—Hakim, Loreweaver').
card_multiverse_id('pearl dragon'/'6ED', '15446').

card_in_set('pentagram of the ages', '6ED').
card_original_type('pentagram of the ages'/'6ED', 'Artifact').
card_original_text('pentagram of the ages'/'6ED', '{4}, {T}: The next time a source of your choice would deal damage to you this turn, prevent that damage.').
card_image_name('pentagram of the ages'/'6ED', 'pentagram of the ages').
card_uid('pentagram of the ages'/'6ED', '6ED:Pentagram of the Ages:pentagram of the ages').
card_rarity('pentagram of the ages'/'6ED', 'Rare').
card_artist('pentagram of the ages'/'6ED', 'Douglas Shuler').
card_number('pentagram of the ages'/'6ED', '305').
card_flavor_text('pentagram of the ages'/'6ED', 'A mirror, a shield, a promise, a great distance, and a kind word—five ways to avoid harm.').
card_multiverse_id('pentagram of the ages'/'6ED', '14784').

card_in_set('perish', '6ED').
card_original_type('perish'/'6ED', 'Sorcery').
card_original_text('perish'/'6ED', 'Destroy all green creatures. They can\'t be regenerated.').
card_image_name('perish'/'6ED', 'perish').
card_uid('perish'/'6ED', '6ED:Perish:perish').
card_rarity('perish'/'6ED', 'Uncommon').
card_artist('perish'/'6ED', 'Rebecca Guay').
card_number('perish'/'6ED', '148').
card_flavor_text('perish'/'6ED', '\"There will come a time when the voices of soil and seedling will sing only laments.\"\n—Oracle en-Vec').
card_multiverse_id('perish'/'6ED', '14581').

card_in_set('pestilence', '6ED').
card_original_type('pestilence'/'6ED', 'Enchantment').
card_original_text('pestilence'/'6ED', 'At end of turn, if there are no creatures in play, sacrifice Pestilence.\n{B} Pestilence deals 1 damage to each creature and each player.').
card_image_name('pestilence'/'6ED', 'pestilence').
card_uid('pestilence'/'6ED', '6ED:Pestilence:pestilence').
card_rarity('pestilence'/'6ED', 'Uncommon').
card_artist('pestilence'/'6ED', 'Kev Walker').
card_number('pestilence'/'6ED', '149').
card_multiverse_id('pestilence'/'6ED', '11438').

card_in_set('phantasmal terrain', '6ED').
card_original_type('phantasmal terrain'/'6ED', 'Enchant Land').
card_original_text('phantasmal terrain'/'6ED', 'Enchanted land is a basic land type of your choice.').
card_image_name('phantasmal terrain'/'6ED', 'phantasmal terrain').
card_uid('phantasmal terrain'/'6ED', '6ED:Phantasmal Terrain:phantasmal terrain').
card_rarity('phantasmal terrain'/'6ED', 'Common').
card_artist('phantasmal terrain'/'6ED', 'David A. Cherry').
card_number('phantasmal terrain'/'6ED', '84').
card_multiverse_id('phantasmal terrain'/'6ED', '16431').

card_in_set('phantom warrior', '6ED').
card_original_type('phantom warrior'/'6ED', 'Creature — Illusion').
card_original_text('phantom warrior'/'6ED', 'Phantom Warrior is unblockable.').
card_image_name('phantom warrior'/'6ED', 'phantom warrior').
card_uid('phantom warrior'/'6ED', '6ED:Phantom Warrior:phantom warrior').
card_rarity('phantom warrior'/'6ED', 'Uncommon').
card_artist('phantom warrior'/'6ED', 'John Matson').
card_number('phantom warrior'/'6ED', '85').
card_flavor_text('phantom warrior'/'6ED', 'The phantom warrior comes from nowhere and returns there just as quickly.').
card_multiverse_id('phantom warrior'/'6ED', '14540').

card_in_set('phyrexian vault', '6ED').
card_original_type('phyrexian vault'/'6ED', 'Artifact').
card_original_text('phyrexian vault'/'6ED', '{2}, {T}, Sacrifice a creature: Draw a card.').
card_image_name('phyrexian vault'/'6ED', 'phyrexian vault').
card_uid('phyrexian vault'/'6ED', '6ED:Phyrexian Vault:phyrexian vault').
card_rarity('phyrexian vault'/'6ED', 'Uncommon').
card_artist('phyrexian vault'/'6ED', 'Hannibal King').
card_number('phyrexian vault'/'6ED', '306').
card_flavor_text('phyrexian vault'/'6ED', '\"The secrets of Phyrexia are expensive. You will pay in brass and bone, steel and sinew.\"\n—Kaervek').
card_multiverse_id('phyrexian vault'/'6ED', '15436').

card_in_set('pillage', '6ED').
card_original_type('pillage'/'6ED', 'Sorcery').
card_original_text('pillage'/'6ED', 'Destroy target artifact or land. It can\'t be regenerated.').
card_image_name('pillage'/'6ED', 'pillage').
card_uid('pillage'/'6ED', '6ED:Pillage:pillage').
card_rarity('pillage'/'6ED', 'Uncommon').
card_artist('pillage'/'6ED', 'Richard Kane Ferguson').
card_number('pillage'/'6ED', '198').
card_multiverse_id('pillage'/'6ED', '14755').

card_in_set('plains', '6ED').
card_original_type('plains'/'6ED', 'Land').
card_original_text('plains'/'6ED', 'W').
card_image_name('plains'/'6ED', 'plains1').
card_uid('plains'/'6ED', '6ED:Plains:plains1').
card_rarity('plains'/'6ED', 'Basic Land').
card_artist('plains'/'6ED', 'Tom Wänerstrand').
card_number('plains'/'6ED', '331').
card_multiverse_id('plains'/'6ED', '14733').

card_in_set('plains', '6ED').
card_original_type('plains'/'6ED', 'Land').
card_original_text('plains'/'6ED', 'W').
card_image_name('plains'/'6ED', 'plains2').
card_uid('plains'/'6ED', '6ED:Plains:plains2').
card_rarity('plains'/'6ED', 'Basic Land').
card_artist('plains'/'6ED', 'Tom Wänerstrand').
card_number('plains'/'6ED', '332').
card_multiverse_id('plains'/'6ED', '14734').

card_in_set('plains', '6ED').
card_original_type('plains'/'6ED', 'Land').
card_original_text('plains'/'6ED', 'W').
card_image_name('plains'/'6ED', 'plains3').
card_uid('plains'/'6ED', '6ED:Plains:plains3').
card_rarity('plains'/'6ED', 'Basic Land').
card_artist('plains'/'6ED', 'Douglas Shuler').
card_number('plains'/'6ED', '333').
card_multiverse_id('plains'/'6ED', '14735').

card_in_set('plains', '6ED').
card_original_type('plains'/'6ED', 'Land').
card_original_text('plains'/'6ED', 'W').
card_image_name('plains'/'6ED', 'plains4').
card_uid('plains'/'6ED', '6ED:Plains:plains4').
card_rarity('plains'/'6ED', 'Basic Land').
card_artist('plains'/'6ED', 'Fred Fields').
card_number('plains'/'6ED', '334').
card_multiverse_id('plains'/'6ED', '14736').

card_in_set('polymorph', '6ED').
card_original_type('polymorph'/'6ED', 'Sorcery').
card_original_text('polymorph'/'6ED', 'Destroy target creature. It can\'t be regenerated. Its controller reveals cards from the top of his or her library until a creature card is revealed. The player puts that card into play and shuffles all other revealed cards into his or her library.').
card_image_name('polymorph'/'6ED', 'polymorph').
card_uid('polymorph'/'6ED', '6ED:Polymorph:polymorph').
card_rarity('polymorph'/'6ED', 'Rare').
card_artist('polymorph'/'6ED', 'Robert Bliss').
card_number('polymorph'/'6ED', '86').
card_flavor_text('polymorph'/'6ED', '\"Ahh! Opposable digits!\"').
card_multiverse_id('polymorph'/'6ED', '15445').

card_in_set('power sink', '6ED').
card_original_type('power sink'/'6ED', 'Instant').
card_original_text('power sink'/'6ED', 'Counter target spell unless its controller pays {X} more. If he or she doesn\'t, tap all mana-producing lands that player controls and empty his or her mana pool.').
card_image_name('power sink'/'6ED', 'power sink').
card_uid('power sink'/'6ED', '6ED:Power Sink:power sink').
card_rarity('power sink'/'6ED', 'Uncommon').
card_artist('power sink'/'6ED', 'Mark Poole').
card_number('power sink'/'6ED', '87').
card_multiverse_id('power sink'/'6ED', '16435').

card_in_set('pradesh gypsies', '6ED').
card_original_type('pradesh gypsies'/'6ED', 'Creature — Gypsy').
card_original_text('pradesh gypsies'/'6ED', '{1}{G}, {T}: Target creature gets -2/-0 until end of turn.').
card_image_name('pradesh gypsies'/'6ED', 'pradesh gypsies').
card_uid('pradesh gypsies'/'6ED', '6ED:Pradesh Gypsies:pradesh gypsies').
card_rarity('pradesh gypsies'/'6ED', 'Common').
card_artist('pradesh gypsies'/'6ED', 'Quinton Hoover').
card_number('pradesh gypsies'/'6ED', '244').
card_flavor_text('pradesh gypsies'/'6ED', '\"A mysterious people indeed. Their origins are as secret as their traditions.\"\n—Lord Magnus of Llanowar').
card_multiverse_id('pradesh gypsies'/'6ED', '11453').

card_in_set('primal clay', '6ED').
card_original_type('primal clay'/'6ED', 'Artifact Creature').
card_original_text('primal clay'/'6ED', 'Primal Clay comes into play as your choice of a 3/3 artifact creature; a 2/2 artifact creature with flying; or a 1/6 Wall artifact creature. (Walls can\'t attack.)').
card_image_name('primal clay'/'6ED', 'primal clay').
card_uid('primal clay'/'6ED', '6ED:Primal Clay:primal clay').
card_rarity('primal clay'/'6ED', 'Rare').
card_artist('primal clay'/'6ED', 'Adam Rex').
card_number('primal clay'/'6ED', '307').
card_multiverse_id('primal clay'/'6ED', '11454').

card_in_set('prodigal sorcerer', '6ED').
card_original_type('prodigal sorcerer'/'6ED', 'Creature — Wizard').
card_original_text('prodigal sorcerer'/'6ED', '{T}: Prodigal Sorcerer deals 1 damage to target creature or player.').
card_image_name('prodigal sorcerer'/'6ED', 'prodigal sorcerer').
card_uid('prodigal sorcerer'/'6ED', '6ED:Prodigal Sorcerer:prodigal sorcerer').
card_rarity('prodigal sorcerer'/'6ED', 'Common').
card_artist('prodigal sorcerer'/'6ED', 'Douglas Shuler').
card_number('prodigal sorcerer'/'6ED', '88').
card_flavor_text('prodigal sorcerer'/'6ED', 'Occasionally members of the Institute of Arcane Study acquire a taste for worldly pleasures. Seldom do they have trouble finding employment.').
card_multiverse_id('prodigal sorcerer'/'6ED', '14523').

card_in_set('prosperity', '6ED').
card_original_type('prosperity'/'6ED', 'Sorcery').
card_original_text('prosperity'/'6ED', 'Each player draws X cards.').
card_image_name('prosperity'/'6ED', 'prosperity').
card_uid('prosperity'/'6ED', '6ED:Prosperity:prosperity').
card_rarity('prosperity'/'6ED', 'Uncommon').
card_artist('prosperity'/'6ED', 'Dan Frazier').
card_number('prosperity'/'6ED', '89').
card_flavor_text('prosperity'/'6ED', '\"Wealth is a good thing, compared to poverty—your food is better, your robes are softer, and your companions have bathed more recently.\"\n—Kukemssa pirate').
card_multiverse_id('prosperity'/'6ED', '10816').

card_in_set('psychic transfer', '6ED').
card_original_type('psychic transfer'/'6ED', 'Sorcery').
card_original_text('psychic transfer'/'6ED', 'If the difference between your life total and target player\'s life total is 5 or less, exchange life totals with that player.').
card_image_name('psychic transfer'/'6ED', 'psychic transfer').
card_uid('psychic transfer'/'6ED', '6ED:Psychic Transfer:psychic transfer').
card_rarity('psychic transfer'/'6ED', 'Rare').
card_artist('psychic transfer'/'6ED', 'Dom!').
card_number('psychic transfer'/'6ED', '90').
card_flavor_text('psychic transfer'/'6ED', '\"The memory of your existence will fade like the final stars of morning.\"\n—Kaervek').
card_multiverse_id('psychic transfer'/'6ED', '15368').

card_in_set('psychic venom', '6ED').
card_original_type('psychic venom'/'6ED', 'Enchant Land').
card_original_text('psychic venom'/'6ED', 'Whenever enchanted land is tapped, Psychic Venom deals 2 damage to that land\'s controller.').
card_image_name('psychic venom'/'6ED', 'psychic venom').
card_uid('psychic venom'/'6ED', '6ED:Psychic Venom:psychic venom').
card_rarity('psychic venom'/'6ED', 'Common').
card_artist('psychic venom'/'6ED', 'Brian Snõddy').
card_number('psychic venom'/'6ED', '91').
card_multiverse_id('psychic venom'/'6ED', '16430').

card_in_set('pyrotechnics', '6ED').
card_original_type('pyrotechnics'/'6ED', 'Sorcery').
card_original_text('pyrotechnics'/'6ED', 'Pyrotechnics deals 4 damage divided any way you choose among any number of target creatures and/or players.').
card_image_name('pyrotechnics'/'6ED', 'pyrotechnics').
card_uid('pyrotechnics'/'6ED', '6ED:Pyrotechnics:pyrotechnics').
card_rarity('pyrotechnics'/'6ED', 'Common').
card_artist('pyrotechnics'/'6ED', 'Anson Maddocks').
card_number('pyrotechnics'/'6ED', '199').
card_flavor_text('pyrotechnics'/'6ED', '\"Hi!ni!ya! Behold the man of flint, that\'s me! / Four lightnings zigzag from me, strike and return.\"\n—Navajo war chant').
card_multiverse_id('pyrotechnics'/'6ED', '14611').

card_in_set('python', '6ED').
card_original_type('python'/'6ED', 'Creature — Snake').
card_original_text('python'/'6ED', '').
card_image_name('python'/'6ED', 'python').
card_uid('python'/'6ED', '6ED:Python:python').
card_rarity('python'/'6ED', 'Common').
card_artist('python'/'6ED', 'Steve White').
card_number('python'/'6ED', '150').
card_flavor_text('python'/'6ED', '\"How can you claim the gods are merciless when they robbed the snake of its limbs to give the other creatures a sporting chance?\"\n—Hakim, Loreweaver').
card_multiverse_id('python'/'6ED', '15392').

card_in_set('radjan spirit', '6ED').
card_original_type('radjan spirit'/'6ED', 'Creature — Spirit').
card_original_text('radjan spirit'/'6ED', '{T}: Target creature loses flying until end of turn.').
card_image_name('radjan spirit'/'6ED', 'radjan spirit').
card_uid('radjan spirit'/'6ED', '6ED:Radjan Spirit:radjan spirit').
card_rarity('radjan spirit'/'6ED', 'Uncommon').
card_artist('radjan spirit'/'6ED', 'Christopher Rush').
card_number('radjan spirit'/'6ED', '245').
card_multiverse_id('radjan spirit'/'6ED', '14700').

card_in_set('rag man', '6ED').
card_original_type('rag man'/'6ED', 'Creature — Rag Man').
card_original_text('rag man'/'6ED', '{B}{B}{B}, {T}: Look at target opponent\'s hand. That player discards a creature card at random from it. Play this ability only during your turn.').
card_image_name('rag man'/'6ED', 'rag man').
card_uid('rag man'/'6ED', '6ED:Rag Man:rag man').
card_rarity('rag man'/'6ED', 'Rare').
card_artist('rag man'/'6ED', 'Daniel Gelon').
card_number('rag man'/'6ED', '151').
card_flavor_text('rag man'/'6ED', '\"Aw, he\'s just a silly, dirty little man. What\'s to be afraid of?\"').
card_multiverse_id('rag man'/'6ED', '11506').

card_in_set('raging goblin', '6ED').
card_original_type('raging goblin'/'6ED', 'Creature — Goblin').
card_original_text('raging goblin'/'6ED', 'Haste (This creature may attack and {T} the turn it comes under your control.)').
card_image_name('raging goblin'/'6ED', 'raging goblin').
card_uid('raging goblin'/'6ED', '6ED:Raging Goblin:raging goblin').
card_rarity('raging goblin'/'6ED', 'Common').
card_artist('raging goblin'/'6ED', 'Jeff Miracola').
card_number('raging goblin'/'6ED', '200').
card_flavor_text('raging goblin'/'6ED', 'He raged at the world, at his family, at his life. But mostly he just raged.').
card_multiverse_id('raging goblin'/'6ED', '14617').

card_in_set('raise dead', '6ED').
card_original_type('raise dead'/'6ED', 'Sorcery').
card_original_text('raise dead'/'6ED', 'Return target creature card from your graveyard to your hand.').
card_image_name('raise dead'/'6ED', 'raise dead').
card_uid('raise dead'/'6ED', '6ED:Raise Dead:raise dead').
card_rarity('raise dead'/'6ED', 'Common').
card_artist('raise dead'/'6ED', 'Charles Gillespie').
card_number('raise dead'/'6ED', '152').
card_flavor_text('raise dead'/'6ED', 'The earth cannot hold that which magic commands.').
card_multiverse_id('raise dead'/'6ED', '14563').

card_in_set('rampant growth', '6ED').
card_original_type('rampant growth'/'6ED', 'Sorcery').
card_original_text('rampant growth'/'6ED', 'Search your library for a basic land card and put that card into play tapped. Then shuffle your library.').
card_image_name('rampant growth'/'6ED', 'rampant growth').
card_uid('rampant growth'/'6ED', '6ED:Rampant Growth:rampant growth').
card_rarity('rampant growth'/'6ED', 'Common').
card_artist('rampant growth'/'6ED', 'Tom Kyffin').
card_number('rampant growth'/'6ED', '246').
card_flavor_text('rampant growth'/'6ED', 'Rath forces plants from its surface as one presses whey from cheese.').
card_multiverse_id('rampant growth'/'6ED', '12966').

card_in_set('razortooth rats', '6ED').
card_original_type('razortooth rats'/'6ED', 'Creature — Rat').
card_original_text('razortooth rats'/'6ED', 'Razortooth Rats can\'t be blocked except by artifact creatures and black creatures.').
card_image_name('razortooth rats'/'6ED', 'razortooth rats').
card_uid('razortooth rats'/'6ED', '6ED:Razortooth Rats:razortooth rats').
card_rarity('razortooth rats'/'6ED', 'Common').
card_artist('razortooth rats'/'6ED', 'Brian Horton').
card_number('razortooth rats'/'6ED', '153').
card_flavor_text('razortooth rats'/'6ED', '\"Men and rats both hunger: we for our playthings; they, for us.\"\n—Crovax').
card_multiverse_id('razortooth rats'/'6ED', '14572').

card_in_set('recall', '6ED').
card_original_type('recall'/'6ED', 'Sorcery').
card_original_text('recall'/'6ED', 'Choose and discard X cards from your hand, then return that many cards from your graveyard to your hand. Remove Recall from the game.').
card_image_name('recall'/'6ED', 'recall').
card_uid('recall'/'6ED', '6ED:Recall:recall').
card_rarity('recall'/'6ED', 'Rare').
card_artist('recall'/'6ED', 'Brian Snõddy').
card_number('recall'/'6ED', '92').
card_multiverse_id('recall'/'6ED', '11467').

card_in_set('reckless embermage', '6ED').
card_original_type('reckless embermage'/'6ED', 'Creature — Wizard').
card_original_text('reckless embermage'/'6ED', '{1}{R} Reckless Embermage deals 1 damage to target creature or player and 1 damage to itself.').
card_image_name('reckless embermage'/'6ED', 'reckless embermage').
card_uid('reckless embermage'/'6ED', '6ED:Reckless Embermage:reckless embermage').
card_rarity('reckless embermage'/'6ED', 'Rare').
card_artist('reckless embermage'/'6ED', 'Tom Kyffin').
card_number('reckless embermage'/'6ED', '201').
card_flavor_text('reckless embermage'/'6ED', 'Many mages have been consumed by their work.').
card_multiverse_id('reckless embermage'/'6ED', '15425').

card_in_set('redwood treefolk', '6ED').
card_original_type('redwood treefolk'/'6ED', 'Creature — Treefolk').
card_original_text('redwood treefolk'/'6ED', '').
card_image_name('redwood treefolk'/'6ED', 'redwood treefolk').
card_uid('redwood treefolk'/'6ED', '6ED:Redwood Treefolk:redwood treefolk').
card_rarity('redwood treefolk'/'6ED', 'Common').
card_artist('redwood treefolk'/'6ED', 'Steve Luke').
card_number('redwood treefolk'/'6ED', '247').
card_flavor_text('redwood treefolk'/'6ED', 'The soldiers chopped and chopped, and still the great tree stood, frowning down at them.').
card_multiverse_id('redwood treefolk'/'6ED', '14681').

card_in_set('regal unicorn', '6ED').
card_original_type('regal unicorn'/'6ED', 'Creature — Unicorn').
card_original_text('regal unicorn'/'6ED', '').
card_image_name('regal unicorn'/'6ED', 'regal unicorn').
card_uid('regal unicorn'/'6ED', '6ED:Regal Unicorn:regal unicorn').
card_rarity('regal unicorn'/'6ED', 'Common').
card_artist('regal unicorn'/'6ED', 'Zina Saunders').
card_number('regal unicorn'/'6ED', '35').
card_flavor_text('regal unicorn'/'6ED', 'Unicorns don\'t care if you believe in them any more than you care if they believe in you.').
card_multiverse_id('regal unicorn'/'6ED', '14473').

card_in_set('regeneration', '6ED').
card_original_type('regeneration'/'6ED', 'Enchant Creature').
card_original_text('regeneration'/'6ED', '{G} Regenerate enchanted creature.').
card_image_name('regeneration'/'6ED', 'regeneration').
card_uid('regeneration'/'6ED', '6ED:Regeneration:regeneration').
card_rarity('regeneration'/'6ED', 'Common').
card_artist('regeneration'/'6ED', 'Quinton Hoover').
card_number('regeneration'/'6ED', '248').
card_multiverse_id('regeneration'/'6ED', '14684').

card_in_set('relearn', '6ED').
card_original_type('relearn'/'6ED', 'Sorcery').
card_original_text('relearn'/'6ED', 'Return target instant or sorcery card from your graveyard to your hand.').
card_image_name('relearn'/'6ED', 'relearn').
card_uid('relearn'/'6ED', '6ED:Relearn:relearn').
card_rarity('relearn'/'6ED', 'Uncommon').
card_artist('relearn'/'6ED', 'Zina Saunders').
card_number('relearn'/'6ED', '93').
card_flavor_text('relearn'/'6ED', '\"Barrin taught me that the hardest lessons to grasp are the ones you\'ve already learned.\"\n—Ertai, wizard adept').
card_multiverse_id('relearn'/'6ED', '15375').

card_in_set('relentless assault', '6ED').
card_original_type('relentless assault'/'6ED', 'Sorcery').
card_original_text('relentless assault'/'6ED', 'Untap all creatures that attacked this turn. You get an additional combat phase followed by an additional main phase this turn.').
card_image_name('relentless assault'/'6ED', 'relentless assault').
card_uid('relentless assault'/'6ED', '6ED:Relentless Assault:relentless assault').
card_rarity('relentless assault'/'6ED', 'Rare').
card_artist('relentless assault'/'6ED', 'Geofrey Darrow').
card_number('relentless assault'/'6ED', '202').
card_flavor_text('relentless assault'/'6ED', '\"Flog and Squee / Up the tree / See the army / Flee, flee, flee.\"\n—Goblin nursery rhyme/war cry').
card_multiverse_id('relentless assault'/'6ED', '14650').

card_in_set('remedy', '6ED').
card_original_type('remedy'/'6ED', 'Instant').
card_original_text('remedy'/'6ED', 'Prevent the next 5 damage this turn divided any way you choose among any number of target creatures and/or players.').
card_image_name('remedy'/'6ED', 'remedy').
card_uid('remedy'/'6ED', '6ED:Remedy:remedy').
card_rarity('remedy'/'6ED', 'Common').
card_artist('remedy'/'6ED', 'Zina Saunders').
card_number('remedy'/'6ED', '36').
card_flavor_text('remedy'/'6ED', '\"These things will protect you while I\'m gone, remind you of my love for you.\"\n—\"Love Song of Night and Day\"').
card_multiverse_id('remedy'/'6ED', '14464').

card_in_set('remove soul', '6ED').
card_original_type('remove soul'/'6ED', 'Instant').
card_original_text('remove soul'/'6ED', 'Counter target creature spell.').
card_image_name('remove soul'/'6ED', 'remove soul').
card_uid('remove soul'/'6ED', '6ED:Remove Soul:remove soul').
card_rarity('remove soul'/'6ED', 'Common').
card_artist('remove soul'/'6ED', 'Mike Dringenberg').
card_number('remove soul'/'6ED', '94').
card_flavor_text('remove soul'/'6ED', 'Nethya stiffened suddenly, head cocked as if straining to hear some distant sound, then fell lifeless to the ground.').
card_multiverse_id('remove soul'/'6ED', '14512').

card_in_set('reprisal', '6ED').
card_original_type('reprisal'/'6ED', 'Instant').
card_original_text('reprisal'/'6ED', 'Destroy target creature with power 4 or greater. It can\'t be regenerated.').
card_image_name('reprisal'/'6ED', 'reprisal').
card_uid('reprisal'/'6ED', '6ED:Reprisal:reprisal').
card_rarity('reprisal'/'6ED', 'Uncommon').
card_artist('reprisal'/'6ED', 'Randy Asplund-Faith').
card_number('reprisal'/'6ED', '37').
card_flavor_text('reprisal'/'6ED', '\"The meek shall fight as one, and they shall overcome even the greatest of foes.\"\n—Halvor Arenson, Kjeldoran priest').
card_multiverse_id('reprisal'/'6ED', '15352').

card_in_set('resistance fighter', '6ED').
card_original_type('resistance fighter'/'6ED', 'Creature — Soldier').
card_original_text('resistance fighter'/'6ED', 'Sacrifice Resistance Fighter: Target creature deals no combat damage this turn.').
card_image_name('resistance fighter'/'6ED', 'resistance fighter').
card_uid('resistance fighter'/'6ED', '6ED:Resistance Fighter:resistance fighter').
card_rarity('resistance fighter'/'6ED', 'Common').
card_artist('resistance fighter'/'6ED', 'Cecil Fernando').
card_number('resistance fighter'/'6ED', '38').
card_flavor_text('resistance fighter'/'6ED', '\"My soldiers fought without hesitation, died without doubt.\"\n—Sidar Jabari').
card_multiverse_id('resistance fighter'/'6ED', '15362').

card_in_set('reverse damage', '6ED').
card_original_type('reverse damage'/'6ED', 'Instant').
card_original_text('reverse damage'/'6ED', 'The next time a source of your choice would deal damage to you this turn, prevent that damage. You gain life equal to the damage prevented this way.').
card_image_name('reverse damage'/'6ED', 'reverse damage').
card_uid('reverse damage'/'6ED', '6ED:Reverse Damage:reverse damage').
card_rarity('reverse damage'/'6ED', 'Rare').
card_artist('reverse damage'/'6ED', 'Thomas Gianni').
card_number('reverse damage'/'6ED', '39').
card_multiverse_id('reverse damage'/'6ED', '11472').

card_in_set('river boa', '6ED').
card_original_type('river boa'/'6ED', 'Creature — Snake').
card_original_text('river boa'/'6ED', 'Islandwalk (This creature is unblockable if defending player controls an island.)\n{G} Regenerate River Boa.').
card_image_name('river boa'/'6ED', 'river boa').
card_uid('river boa'/'6ED', '6ED:River Boa:river boa').
card_rarity('river boa'/'6ED', 'Uncommon').
card_artist('river boa'/'6ED', 'Steve White').
card_number('river boa'/'6ED', '249').
card_flavor_text('river boa'/'6ED', '\"But no one heard the snake\'s gentle hiss for peace over the elephant\'s trumpeting of war.\"\n—Afari, Tales').
card_multiverse_id('river boa'/'6ED', '16457').

card_in_set('rod of ruin', '6ED').
card_original_type('rod of ruin'/'6ED', 'Artifact').
card_original_text('rod of ruin'/'6ED', '{3}, {T}: Rod of Ruin deals 1 damage to target creature or player.').
card_image_name('rod of ruin'/'6ED', 'rod of ruin').
card_uid('rod of ruin'/'6ED', '6ED:Rod of Ruin:rod of ruin').
card_rarity('rod of ruin'/'6ED', 'Uncommon').
card_artist('rod of ruin'/'6ED', 'Christopher Rush').
card_number('rod of ruin'/'6ED', '308').
card_multiverse_id('rod of ruin'/'6ED', '14768').

card_in_set('rowen', '6ED').
card_original_type('rowen'/'6ED', 'Enchantment').
card_original_text('rowen'/'6ED', 'Reveal the first card you draw each turn. Whenever you reveal a basic land card this way, draw a card.').
card_image_name('rowen'/'6ED', 'rowen').
card_uid('rowen'/'6ED', '6ED:Rowen:rowen').
card_rarity('rowen'/'6ED', 'Rare').
card_artist('rowen'/'6ED', 'Jon J. Muth').
card_number('rowen'/'6ED', '250').
card_flavor_text('rowen'/'6ED', '\"I\'ve dreamt of a second harvest but fear I will not see it.\"\n—Asmira, holy avenger').
card_multiverse_id('rowen'/'6ED', '14711').

card_in_set('ruins of trokair', '6ED').
card_original_type('ruins of trokair'/'6ED', 'Land').
card_original_text('ruins of trokair'/'6ED', 'Ruins of Trokair comes into play tapped.\n{T}: Add {W} to your mana pool.\n{T}, Sacrifice Ruins of Trokair: Add {W}{W} to your mana pool.').
card_image_name('ruins of trokair'/'6ED', 'ruins of trokair').
card_uid('ruins of trokair'/'6ED', '6ED:Ruins of Trokair:ruins of trokair').
card_rarity('ruins of trokair'/'6ED', 'Uncommon').
card_artist('ruins of trokair'/'6ED', 'Liz Danforth').
card_number('ruins of trokair'/'6ED', '327').
card_multiverse_id('ruins of trokair'/'6ED', '14722').

card_in_set('sabretooth tiger', '6ED').
card_original_type('sabretooth tiger'/'6ED', 'Creature — Tiger').
card_original_text('sabretooth tiger'/'6ED', 'First strike').
card_image_name('sabretooth tiger'/'6ED', 'sabretooth tiger').
card_uid('sabretooth tiger'/'6ED', '6ED:Sabretooth Tiger:sabretooth tiger').
card_rarity('sabretooth tiger'/'6ED', 'Common').
card_artist('sabretooth tiger'/'6ED', 'Melissa A. Benson').
card_number('sabretooth tiger'/'6ED', '203').
card_flavor_text('sabretooth tiger'/'6ED', '\"I fear anything with teeth measured in handspans!\"\n—Norin the Wary').
card_multiverse_id('sabretooth tiger'/'6ED', '11476').

card_in_set('sage owl', '6ED').
card_original_type('sage owl'/'6ED', 'Creature — Bird').
card_original_text('sage owl'/'6ED', 'Flying\nWhen Sage Owl comes into play, look at the top four cards of your library and put them back in any order you choose.').
card_image_name('sage owl'/'6ED', 'sage owl').
card_uid('sage owl'/'6ED', '6ED:Sage Owl:sage owl').
card_rarity('sage owl'/'6ED', 'Common').
card_artist('sage owl'/'6ED', 'Mark Poole').
card_number('sage owl'/'6ED', '95').
card_flavor_text('sage owl'/'6ED', 'The owl asks but never answers.').
card_multiverse_id('sage owl'/'6ED', '14520').

card_in_set('samite healer', '6ED').
card_original_type('samite healer'/'6ED', 'Creature — Cleric').
card_original_text('samite healer'/'6ED', '{T}: Prevent the next 1 damage to target creature or player this turn.').
card_image_name('samite healer'/'6ED', 'samite healer').
card_uid('samite healer'/'6ED', '6ED:Samite Healer:samite healer').
card_rarity('samite healer'/'6ED', 'Common').
card_artist('samite healer'/'6ED', 'Tom Wänerstrand').
card_number('samite healer'/'6ED', '40').
card_flavor_text('samite healer'/'6ED', 'Healers ultimately acquire the divine gifts of spiritual and physical wholeness. The most devout are also granted the ability to pass physical wholeness on to others.').
card_multiverse_id('samite healer'/'6ED', '14467').

card_in_set('scaled wurm', '6ED').
card_original_type('scaled wurm'/'6ED', 'Creature — Wurm').
card_original_text('scaled wurm'/'6ED', '').
card_image_name('scaled wurm'/'6ED', 'scaled wurm').
card_uid('scaled wurm'/'6ED', '6ED:Scaled Wurm:scaled wurm').
card_rarity('scaled wurm'/'6ED', 'Common').
card_artist('scaled wurm'/'6ED', 'Daniel Gelon').
card_number('scaled wurm'/'6ED', '251').
card_flavor_text('scaled wurm'/'6ED', '\"Flourishing during the Ice Age, these wurms were the bane of all Kjeldorans. Their great size and ferocity made them the subject of countless nightmares—they embodied the worst of the Ice Age.\"\n—Kjeldor: Ice Civilization').
card_multiverse_id('scaled wurm'/'6ED', '16452').

card_in_set('scathe zombies', '6ED').
card_original_type('scathe zombies'/'6ED', 'Creature — Zombie').
card_original_text('scathe zombies'/'6ED', '').
card_image_name('scathe zombies'/'6ED', 'scathe zombies').
card_uid('scathe zombies'/'6ED', '6ED:Scathe Zombies:scathe zombies').
card_rarity('scathe zombies'/'6ED', 'Common').
card_artist('scathe zombies'/'6ED', 'Jesper Myrfors').
card_number('scathe zombies'/'6ED', '154').
card_flavor_text('scathe zombies'/'6ED', '\"They groaned, they stirred, they all uprose,\nNor spake, nor moved their eyes;\nIt had been strange, even in a dream,\nTo have seen those dead men rise.\"\n—Samuel Coleridge,\n\"The Rime of the Ancient Mariner\"').
card_multiverse_id('scathe zombies'/'6ED', '16627').

card_in_set('sea monster', '6ED').
card_original_type('sea monster'/'6ED', 'Creature — Serpent').
card_original_text('sea monster'/'6ED', 'Sea Monster can\'t attack unless defending player controls an island.').
card_image_name('sea monster'/'6ED', 'sea monster').
card_uid('sea monster'/'6ED', '6ED:Sea Monster:sea monster').
card_rarity('sea monster'/'6ED', 'Common').
card_artist('sea monster'/'6ED', 'Daniel Gelon').
card_number('sea monster'/'6ED', '96').
card_flavor_text('sea monster'/'6ED', 'Water is life. Or perhaps it is just something to wash it down with.').
card_multiverse_id('sea monster'/'6ED', '14525').

card_in_set('segovian leviathan', '6ED').
card_original_type('segovian leviathan'/'6ED', 'Creature — Serpent').
card_original_text('segovian leviathan'/'6ED', 'Islandwalk (This creature is unblockable if defending player controls an island.)').
card_image_name('segovian leviathan'/'6ED', 'segovian leviathan').
card_uid('segovian leviathan'/'6ED', '6ED:Segovian Leviathan:segovian leviathan').
card_rarity('segovian leviathan'/'6ED', 'Uncommon').
card_artist('segovian leviathan'/'6ED', 'Melissa A. Benson').
card_number('segovian leviathan'/'6ED', '97').
card_flavor_text('segovian leviathan'/'6ED', '\"Leviathan, too! Can you catch him with a fish-hook or run a line round his tongue?\"\n—The Bible, Job 41:1').
card_multiverse_id('segovian leviathan'/'6ED', '14538').

card_in_set('sengir autocrat', '6ED').
card_original_type('sengir autocrat'/'6ED', 'Creature — Minion').
card_original_text('sengir autocrat'/'6ED', 'When Sengir Autocrat comes into play, put three 0/1 black Serf creature tokens into play.\nWhen Sengir Autocrat leaves play, remove all Serf tokens from play.').
card_image_name('sengir autocrat'/'6ED', 'sengir autocrat').
card_uid('sengir autocrat'/'6ED', '6ED:Sengir Autocrat:sengir autocrat').
card_rarity('sengir autocrat'/'6ED', 'Rare').
card_artist('sengir autocrat'/'6ED', 'David A. Cherry').
card_number('sengir autocrat'/'6ED', '155').
card_multiverse_id('sengir autocrat'/'6ED', '11489').

card_in_set('serenity', '6ED').
card_original_type('serenity'/'6ED', 'Enchantment').
card_original_text('serenity'/'6ED', 'At the beginning of your upkeep, destroy all artifacts and enchantments. They can\'t be regenerated.').
card_image_name('serenity'/'6ED', 'serenity').
card_uid('serenity'/'6ED', '6ED:Serenity:serenity').
card_rarity('serenity'/'6ED', 'Rare').
card_artist('serenity'/'6ED', 'Cliff Nielsen').
card_number('serenity'/'6ED', '41').
card_flavor_text('serenity'/'6ED', '\"Just think of me as the storm before the calm.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('serenity'/'6ED', '15360').

card_in_set('serra\'s blessing', '6ED').
card_original_type('serra\'s blessing'/'6ED', 'Enchantment').
card_original_text('serra\'s blessing'/'6ED', 'Attacking doesn\'t cause creatures you control to tap.').
card_image_name('serra\'s blessing'/'6ED', 'serra\'s blessing').
card_uid('serra\'s blessing'/'6ED', '6ED:Serra\'s Blessing:serra\'s blessing').
card_rarity('serra\'s blessing'/'6ED', 'Uncommon').
card_artist('serra\'s blessing'/'6ED', 'Rebecca Guay').
card_number('serra\'s blessing'/'6ED', '42').
card_flavor_text('serra\'s blessing'/'6ED', '\"I have seen your strength imbued in angels\' wings, and I have felt your sorrow rain down on the ruins brought by the Lord of the Wastes.\"\n—Hanna, Weatherlight navigator').
card_multiverse_id('serra\'s blessing'/'6ED', '14476').

card_in_set('shanodin dryads', '6ED').
card_original_type('shanodin dryads'/'6ED', 'Creature — Dryad').
card_original_text('shanodin dryads'/'6ED', 'Forestwalk (This creature is unblockable if defending player controls a forest.)').
card_image_name('shanodin dryads'/'6ED', 'shanodin dryads').
card_uid('shanodin dryads'/'6ED', '6ED:Shanodin Dryads:shanodin dryads').
card_rarity('shanodin dryads'/'6ED', 'Common').
card_artist('shanodin dryads'/'6ED', 'Gary Leach').
card_number('shanodin dryads'/'6ED', '252').
card_flavor_text('shanodin dryads'/'6ED', 'Moving without sound, swift figures pass through branches and undergrowth completely unhindered. One with the trees around them, the dryads of Shanodin Forest are seen only when they wish to be.').
card_multiverse_id('shanodin dryads'/'6ED', '14670').

card_in_set('shatter', '6ED').
card_original_type('shatter'/'6ED', 'Instant').
card_original_text('shatter'/'6ED', 'Destroy target artifact.').
card_image_name('shatter'/'6ED', 'shatter').
card_uid('shatter'/'6ED', '6ED:Shatter:shatter').
card_rarity('shatter'/'6ED', 'Common').
card_artist('shatter'/'6ED', 'Jason Alexander Behnke').
card_number('shatter'/'6ED', '204').
card_multiverse_id('shatter'/'6ED', '14614').

card_in_set('shatterstorm', '6ED').
card_original_type('shatterstorm'/'6ED', 'Sorcery').
card_original_text('shatterstorm'/'6ED', 'Destroy all artifacts. They can\'t be regenerated.').
card_image_name('shatterstorm'/'6ED', 'shatterstorm').
card_uid('shatterstorm'/'6ED', '6ED:Shatterstorm:shatterstorm').
card_rarity('shatterstorm'/'6ED', 'Rare').
card_artist('shatterstorm'/'6ED', 'James Allen').
card_number('shatterstorm'/'6ED', '205').
card_flavor_text('shatterstorm'/'6ED', 'Chains of leaping fire and sizzling lightning laid waste the artificers\' handiwork, sparing not a single device.').
card_multiverse_id('shatterstorm'/'6ED', '11497').

card_in_set('shock', '6ED').
card_original_type('shock'/'6ED', 'Instant').
card_original_text('shock'/'6ED', 'Shock deals 2 damage to target creature or player.').
card_image_name('shock'/'6ED', 'shock').
card_uid('shock'/'6ED', '6ED:Shock:shock').
card_rarity('shock'/'6ED', 'Common').
card_artist('shock'/'6ED', 'Randy Gallegos').
card_number('shock'/'6ED', '206').
card_flavor_text('shock'/'6ED', 'Lightning tethers souls to the world.\n—Kor saying').
card_multiverse_id('shock'/'6ED', '14609').

card_in_set('sibilant spirit', '6ED').
card_original_type('sibilant spirit'/'6ED', 'Creature — Spirit').
card_original_text('sibilant spirit'/'6ED', 'Flying\nWhenever Sibilant Spirit attacks, defending player may draw a card.').
card_image_name('sibilant spirit'/'6ED', 'sibilant spirit').
card_uid('sibilant spirit'/'6ED', '6ED:Sibilant Spirit:sibilant spirit').
card_rarity('sibilant spirit'/'6ED', 'Rare').
card_artist('sibilant spirit'/'6ED', 'Ron Spencer').
card_number('sibilant spirit'/'6ED', '98').
card_flavor_text('sibilant spirit'/'6ED', 'She had expected death to roar, to thunder, to growl. She did not recognize it when it came hissing to her side.').
card_multiverse_id('sibilant spirit'/'6ED', '11502').

card_in_set('skull catapult', '6ED').
card_original_type('skull catapult'/'6ED', 'Artifact').
card_original_text('skull catapult'/'6ED', '{1}, {T}, Sacrifice a creature: Skull Catapult deals 2 damage to target creature or player.').
card_image_name('skull catapult'/'6ED', 'skull catapult').
card_uid('skull catapult'/'6ED', '6ED:Skull Catapult:skull catapult').
card_rarity('skull catapult'/'6ED', 'Uncommon').
card_artist('skull catapult'/'6ED', 'Ian Miller').
card_number('skull catapult'/'6ED', '309').
card_flavor_text('skull catapult'/'6ED', '\"What manner of fiend would design such a sadistic device?\"\n—Sorine Relicbane, Soldevi heretic').
card_multiverse_id('skull catapult'/'6ED', '11503').

card_in_set('sky diamond', '6ED').
card_original_type('sky diamond'/'6ED', 'Artifact').
card_original_text('sky diamond'/'6ED', 'Sky Diamond comes into play tapped.\n{T}: Add {U} to your mana pool.').
card_image_name('sky diamond'/'6ED', 'sky diamond').
card_uid('sky diamond'/'6ED', '6ED:Sky Diamond:sky diamond').
card_rarity('sky diamond'/'6ED', 'Uncommon').
card_artist('sky diamond'/'6ED', 'D. Alexander Gregory').
card_number('sky diamond'/'6ED', '310').
card_multiverse_id('sky diamond'/'6ED', '15443').

card_in_set('snake basket', '6ED').
card_original_type('snake basket'/'6ED', 'Artifact').
card_original_text('snake basket'/'6ED', '{X}, Sacrifice Snake Basket: Put X 1/1 green Cobra creature tokens into play. Play this ability only if you\'re allowed to play a sorcery.').
card_image_name('snake basket'/'6ED', 'snake basket').
card_uid('snake basket'/'6ED', '6ED:Snake Basket:snake basket').
card_rarity('snake basket'/'6ED', 'Rare').
card_artist('snake basket'/'6ED', 'Roger Raupp').
card_number('snake basket'/'6ED', '311').
card_flavor_text('snake basket'/'6ED', '\"Uh, does anyone have a flute?\"\n—Rana, Suq\'Ata market fool').
card_multiverse_id('snake basket'/'6ED', '15409').

card_in_set('soldevi sage', '6ED').
card_original_type('soldevi sage'/'6ED', 'Creature — Wizard').
card_original_text('soldevi sage'/'6ED', '{T}, Sacrifice two lands: Draw three cards, then choose and discard one of them.').
card_image_name('soldevi sage'/'6ED', 'soldevi sage').
card_uid('soldevi sage'/'6ED', '6ED:Soldevi Sage:soldevi sage').
card_rarity('soldevi sage'/'6ED', 'Uncommon').
card_artist('soldevi sage'/'6ED', 'Carol Heyer').
card_number('soldevi sage'/'6ED', '99').
card_flavor_text('soldevi sage'/'6ED', '\"To hide the truth is more than folly—it is fatal.\"\n—Sorine Relicbane, Soldevi heretic').
card_multiverse_id('soldevi sage'/'6ED', '16436').

card_in_set('soul net', '6ED').
card_original_type('soul net'/'6ED', 'Artifact').
card_original_text('soul net'/'6ED', 'Whenever a creature is put into a graveyard from play, you may pay {1}. If you do, you gain 1 life.').
card_image_name('soul net'/'6ED', 'soul net').
card_uid('soul net'/'6ED', '6ED:Soul Net:soul net').
card_rarity('soul net'/'6ED', 'Uncommon').
card_artist('soul net'/'6ED', 'Andrew Robinson').
card_number('soul net'/'6ED', '312').
card_multiverse_id('soul net'/'6ED', '14769').

card_in_set('spell blast', '6ED').
card_original_type('spell blast'/'6ED', 'Instant').
card_original_text('spell blast'/'6ED', 'Counter target spell with converted mana cost equal to X.').
card_image_name('spell blast'/'6ED', 'spell blast').
card_uid('spell blast'/'6ED', '6ED:Spell Blast:spell blast').
card_rarity('spell blast'/'6ED', 'Common').
card_artist('spell blast'/'6ED', 'Greg Simanson').
card_number('spell blast'/'6ED', '100').
card_flavor_text('spell blast'/'6ED', 'Call it the thinking mage\'s version of brute force.').
card_multiverse_id('spell blast'/'6ED', '15377').

card_in_set('spirit link', '6ED').
card_original_type('spirit link'/'6ED', 'Enchant Creature').
card_original_text('spirit link'/'6ED', 'Whenever enchanted creature deals damage, you gain life equal to the damage dealt this way.').
card_image_name('spirit link'/'6ED', 'spirit link').
card_uid('spirit link'/'6ED', '6ED:Spirit Link:spirit link').
card_rarity('spirit link'/'6ED', 'Uncommon').
card_artist('spirit link'/'6ED', 'Kaja Foglio').
card_number('spirit link'/'6ED', '43').
card_multiverse_id('spirit link'/'6ED', '11510').

card_in_set('spitting drake', '6ED').
card_original_type('spitting drake'/'6ED', 'Creature — Drake').
card_original_text('spitting drake'/'6ED', 'Flying\n{R} Spitting Drake gets +1/+0 until end of turn. Spend no more than {R} this way each turn.').
card_image_name('spitting drake'/'6ED', 'spitting drake').
card_uid('spitting drake'/'6ED', '6ED:Spitting Drake:spitting drake').
card_rarity('spitting drake'/'6ED', 'Uncommon').
card_artist('spitting drake'/'6ED', 'Geofrey Darrow').
card_number('spitting drake'/'6ED', '207').
card_flavor_text('spitting drake'/'6ED', 'It prefers its meals cooked.').
card_multiverse_id('spitting drake'/'6ED', '15418').

card_in_set('spitting earth', '6ED').
card_original_type('spitting earth'/'6ED', 'Sorcery').
card_original_text('spitting earth'/'6ED', 'Spitting Earth deals to target creature damage equal to the number of mountains you control.').
card_image_name('spitting earth'/'6ED', 'spitting earth').
card_uid('spitting earth'/'6ED', '6ED:Spitting Earth:spitting earth').
card_rarity('spitting earth'/'6ED', 'Common').
card_artist('spitting earth'/'6ED', 'Brian Snõddy').
card_number('spitting earth'/'6ED', '208').
card_flavor_text('spitting earth'/'6ED', '\"There are times solid ground gives precious little comfort.\"\n—Talibah, embermage').
card_multiverse_id('spitting earth'/'6ED', '14612').

card_in_set('stalking tiger', '6ED').
card_original_type('stalking tiger'/'6ED', 'Creature — Tiger').
card_original_text('stalking tiger'/'6ED', 'Stalking Tiger can\'t be blocked by more than one creature each combat.').
card_image_name('stalking tiger'/'6ED', 'stalking tiger').
card_uid('stalking tiger'/'6ED', '6ED:Stalking Tiger:stalking tiger').
card_rarity('stalking tiger'/'6ED', 'Common').
card_artist('stalking tiger'/'6ED', 'Terese Nielsen').
card_number('stalking tiger'/'6ED', '253').
card_flavor_text('stalking tiger'/'6ED', 'In the Jamuraan jungles, there is often no separating beauty from danger.').
card_multiverse_id('stalking tiger'/'6ED', '14677').

card_in_set('standing troops', '6ED').
card_original_type('standing troops'/'6ED', 'Creature — Soldier').
card_original_text('standing troops'/'6ED', 'Attacking doesn\'t cause Standing Troops to tap.').
card_image_name('standing troops'/'6ED', 'standing troops').
card_uid('standing troops'/'6ED', '6ED:Standing Troops:standing troops').
card_rarity('standing troops'/'6ED', 'Common').
card_artist('standing troops'/'6ED', 'Daren Bader').
card_number('standing troops'/'6ED', '44').
card_flavor_text('standing troops'/'6ED', 'The less you have, the harder you fight for it.').
card_multiverse_id('standing troops'/'6ED', '14471').

card_in_set('staunch defenders', '6ED').
card_original_type('staunch defenders'/'6ED', 'Creature — Soldier').
card_original_text('staunch defenders'/'6ED', 'When Staunch Defenders comes into play, you gain 4 life.').
card_image_name('staunch defenders'/'6ED', 'staunch defenders').
card_uid('staunch defenders'/'6ED', '6ED:Staunch Defenders:staunch defenders').
card_rarity('staunch defenders'/'6ED', 'Uncommon').
card_artist('staunch defenders'/'6ED', 'Mark Poole').
card_number('staunch defenders'/'6ED', '45').
card_flavor_text('staunch defenders'/'6ED', '\"Hold your position! Leave doubt for the dying!\"\n—Tahngarth of the Weatherlight').
card_multiverse_id('staunch defenders'/'6ED', '14490').

card_in_set('stone rain', '6ED').
card_original_type('stone rain'/'6ED', 'Sorcery').
card_original_text('stone rain'/'6ED', 'Destroy target land.').
card_image_name('stone rain'/'6ED', 'stone rain').
card_uid('stone rain'/'6ED', '6ED:Stone Rain:stone rain').
card_rarity('stone rain'/'6ED', 'Common').
card_artist('stone rain'/'6ED', 'John Matson').
card_number('stone rain'/'6ED', '209').
card_multiverse_id('stone rain'/'6ED', '14615').

card_in_set('storm cauldron', '6ED').
card_original_type('storm cauldron'/'6ED', 'Artifact').
card_original_text('storm cauldron'/'6ED', 'Each player may play an additional land during each of his or her turns.\nWhenever a land is tapped for mana, return it to its owner\'s hand.').
card_image_name('storm cauldron'/'6ED', 'storm cauldron').
card_uid('storm cauldron'/'6ED', '6ED:Storm Cauldron:storm cauldron').
card_rarity('storm cauldron'/'6ED', 'Rare').
card_artist('storm cauldron'/'6ED', 'Dan Frazier').
card_number('storm cauldron'/'6ED', '313').
card_multiverse_id('storm cauldron'/'6ED', '15402').

card_in_set('storm crow', '6ED').
card_original_type('storm crow'/'6ED', 'Creature — Bird').
card_original_text('storm crow'/'6ED', 'Flying').
card_image_name('storm crow'/'6ED', 'storm crow').
card_uid('storm crow'/'6ED', '6ED:Storm Crow:storm crow').
card_rarity('storm crow'/'6ED', 'Common').
card_artist('storm crow'/'6ED', 'Una Fricker').
card_number('storm crow'/'6ED', '101').
card_flavor_text('storm crow'/'6ED', 'Storm crow descending, winter unending.\nStorm crow departing, summer is starting.').
card_multiverse_id('storm crow'/'6ED', '14753').

card_in_set('strands of night', '6ED').
card_original_type('strands of night'/'6ED', 'Enchantment').
card_original_text('strands of night'/'6ED', '{B}{B}, Pay 2 life, Sacrifice a swamp: Return target creature card from your graveyard to play.').
card_image_name('strands of night'/'6ED', 'strands of night').
card_uid('strands of night'/'6ED', '6ED:Strands of Night:strands of night').
card_rarity('strands of night'/'6ED', 'Uncommon').
card_artist('strands of night'/'6ED', 'Patrick Kochakji').
card_number('strands of night'/'6ED', '156').
card_flavor_text('strands of night'/'6ED', '\"I have seen the night torn into thin darkling strips and woven into shapes too bleak for dreams.\"\n—Crovax').
card_multiverse_id('strands of night'/'6ED', '14577').

card_in_set('stream of life', '6ED').
card_original_type('stream of life'/'6ED', 'Sorcery').
card_original_text('stream of life'/'6ED', 'Target player gains X life.').
card_image_name('stream of life'/'6ED', 'stream of life').
card_uid('stream of life'/'6ED', '6ED:Stream of Life:stream of life').
card_rarity('stream of life'/'6ED', 'Common').
card_artist('stream of life'/'6ED', 'Terese Nielsen').
card_number('stream of life'/'6ED', '254').
card_multiverse_id('stream of life'/'6ED', '14687').

card_in_set('stromgald cabal', '6ED').
card_original_type('stromgald cabal'/'6ED', 'Creature — Knight').
card_original_text('stromgald cabal'/'6ED', '{T}, Pay 1 life: Counter target white spell.').
card_image_name('stromgald cabal'/'6ED', 'stromgald cabal').
card_uid('stromgald cabal'/'6ED', '6ED:Stromgald Cabal:stromgald cabal').
card_rarity('stromgald cabal'/'6ED', 'Rare').
card_artist('stromgald cabal'/'6ED', 'Anson Maddocks').
card_number('stromgald cabal'/'6ED', '157').
card_flavor_text('stromgald cabal'/'6ED', 'When the alliance between Balduvia and Kjeldor deprived Stromgald of easy victories, its agents were forced to fuel their rituals with their own flesh.').
card_multiverse_id('stromgald cabal'/'6ED', '11518').

card_in_set('stupor', '6ED').
card_original_type('stupor'/'6ED', 'Sorcery').
card_original_text('stupor'/'6ED', 'Target opponent discards a card at random from his or her hand, then chooses and discards a card from his or her hand.').
card_image_name('stupor'/'6ED', 'stupor').
card_uid('stupor'/'6ED', '6ED:Stupor:stupor').
card_rarity('stupor'/'6ED', 'Uncommon').
card_artist('stupor'/'6ED', 'Mike Kimble').
card_number('stupor'/'6ED', '158').
card_flavor_text('stupor'/'6ED', 'There are medicines for all afflictions but idleness.\n—Suq\'Ata saying').
card_multiverse_id('stupor'/'6ED', '16628').

card_in_set('sulfurous springs', '6ED').
card_original_type('sulfurous springs'/'6ED', 'Land').
card_original_text('sulfurous springs'/'6ED', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {B} or {R} to your mana pool. Sulfurous Springs deals 1 damage to you.').
card_image_name('sulfurous springs'/'6ED', 'sulfurous springs').
card_uid('sulfurous springs'/'6ED', '6ED:Sulfurous Springs:sulfurous springs').
card_rarity('sulfurous springs'/'6ED', 'Rare').
card_artist('sulfurous springs'/'6ED', 'Jeff Miracola').
card_number('sulfurous springs'/'6ED', '328').
card_multiverse_id('sulfurous springs'/'6ED', '14729').

card_in_set('summer bloom', '6ED').
card_original_type('summer bloom'/'6ED', 'Sorcery').
card_original_text('summer bloom'/'6ED', 'Play up to three additional lands this turn.').
card_image_name('summer bloom'/'6ED', 'summer bloom').
card_uid('summer bloom'/'6ED', '6ED:Summer Bloom:summer bloom').
card_rarity('summer bloom'/'6ED', 'Uncommon').
card_artist('summer bloom'/'6ED', 'Kaja Foglio').
card_number('summer bloom'/'6ED', '255').
card_flavor_text('summer bloom'/'6ED', 'Summer sends its kiss with warmth and blooming life.').
card_multiverse_id('summer bloom'/'6ED', '14696').

card_in_set('sunweb', '6ED').
card_original_type('sunweb'/'6ED', 'Creature — Wall').
card_original_text('sunweb'/'6ED', '(Walls can\'t attack.)\nFlying\nSunweb can\'t block creatures with power 2 or less.').
card_image_name('sunweb'/'6ED', 'sunweb').
card_uid('sunweb'/'6ED', '6ED:Sunweb:sunweb').
card_rarity('sunweb'/'6ED', 'Rare').
card_artist('sunweb'/'6ED', 'Dan Frazier').
card_number('sunweb'/'6ED', '46').
card_flavor_text('sunweb'/'6ED', '\"There is no sweeter music than the wails of a dying dragon.\"\n—Rashida Scalebane').
card_multiverse_id('sunweb'/'6ED', '14494').

card_in_set('svyelunite temple', '6ED').
card_original_type('svyelunite temple'/'6ED', 'Land').
card_original_text('svyelunite temple'/'6ED', 'Svyelunite Temple comes into play tapped.\n{T}: Add {U} to your mana pool.\n{T}, Sacrifice Svyelunite Temple: Add {U}{U} to your mana pool.').
card_image_name('svyelunite temple'/'6ED', 'svyelunite temple').
card_uid('svyelunite temple'/'6ED', '6ED:Svyelunite Temple:svyelunite temple').
card_rarity('svyelunite temple'/'6ED', 'Uncommon').
card_artist('svyelunite temple'/'6ED', 'Liz Danforth').
card_number('svyelunite temple'/'6ED', '329').
card_multiverse_id('svyelunite temple'/'6ED', '14723').

card_in_set('swamp', '6ED').
card_original_type('swamp'/'6ED', 'Land').
card_original_text('swamp'/'6ED', 'B').
card_image_name('swamp'/'6ED', 'swamp1').
card_uid('swamp'/'6ED', '6ED:Swamp:swamp1').
card_rarity('swamp'/'6ED', 'Basic Land').
card_artist('swamp'/'6ED', 'Romas Kukalis').
card_number('swamp'/'6ED', '339').
card_multiverse_id('swamp'/'6ED', '14741').

card_in_set('swamp', '6ED').
card_original_type('swamp'/'6ED', 'Land').
card_original_text('swamp'/'6ED', 'B').
card_image_name('swamp'/'6ED', 'swamp2').
card_uid('swamp'/'6ED', '6ED:Swamp:swamp2').
card_rarity('swamp'/'6ED', 'Basic Land').
card_artist('swamp'/'6ED', 'Dan Frazier').
card_number('swamp'/'6ED', '340').
card_multiverse_id('swamp'/'6ED', '14742').

card_in_set('swamp', '6ED').
card_original_type('swamp'/'6ED', 'Land').
card_original_text('swamp'/'6ED', 'B').
card_image_name('swamp'/'6ED', 'swamp3').
card_uid('swamp'/'6ED', '6ED:Swamp:swamp3').
card_rarity('swamp'/'6ED', 'Basic Land').
card_artist('swamp'/'6ED', 'Douglas Shuler').
card_number('swamp'/'6ED', '341').
card_multiverse_id('swamp'/'6ED', '14743').

card_in_set('swamp', '6ED').
card_original_type('swamp'/'6ED', 'Land').
card_original_text('swamp'/'6ED', 'B').
card_image_name('swamp'/'6ED', 'swamp4').
card_uid('swamp'/'6ED', '6ED:Swamp:swamp4').
card_rarity('swamp'/'6ED', 'Basic Land').
card_artist('swamp'/'6ED', 'Romas Kukalis').
card_number('swamp'/'6ED', '342').
card_multiverse_id('swamp'/'6ED', '14744').

card_in_set('syphon soul', '6ED').
card_original_type('syphon soul'/'6ED', 'Sorcery').
card_original_text('syphon soul'/'6ED', 'Syphon Soul deals 2 damage to each other player. You gain life equal to the damage dealt this way.').
card_image_name('syphon soul'/'6ED', 'syphon soul').
card_uid('syphon soul'/'6ED', '6ED:Syphon Soul:syphon soul').
card_rarity('syphon soul'/'6ED', 'Common').
card_artist('syphon soul'/'6ED', 'Melissa A. Benson').
card_number('syphon soul'/'6ED', '159').
card_multiverse_id('syphon soul'/'6ED', '15380').

card_in_set('talruum minotaur', '6ED').
card_original_type('talruum minotaur'/'6ED', 'Creature — Minotaur').
card_original_text('talruum minotaur'/'6ED', 'Haste (This creature may attack and {T} the turn it comes under your control.)').
card_image_name('talruum minotaur'/'6ED', 'talruum minotaur').
card_uid('talruum minotaur'/'6ED', '6ED:Talruum Minotaur:talruum minotaur').
card_rarity('talruum minotaur'/'6ED', 'Common').
card_artist('talruum minotaur'/'6ED', 'Pete Venters').
card_number('talruum minotaur'/'6ED', '210').
card_flavor_text('talruum minotaur'/'6ED', 'Don\'t insult a Talruum unless your mount is swift.\n—Suq\'Ata saying').
card_multiverse_id('talruum minotaur'/'6ED', '14624').

card_in_set('tariff', '6ED').
card_original_type('tariff'/'6ED', 'Sorcery').
card_original_text('tariff'/'6ED', 'Each player chooses a creature with the highest converted mana cost he or she controls, then pays mana equal to that cost or sacrifices that creature.').
card_image_name('tariff'/'6ED', 'tariff').
card_uid('tariff'/'6ED', '6ED:Tariff:tariff').
card_rarity('tariff'/'6ED', 'Rare').
card_artist('tariff'/'6ED', 'Kev Walker').
card_number('tariff'/'6ED', '47').
card_multiverse_id('tariff'/'6ED', '15361').

card_in_set('teferi\'s puzzle box', '6ED').
card_original_type('teferi\'s puzzle box'/'6ED', 'Artifact').
card_original_text('teferi\'s puzzle box'/'6ED', 'At the beginning of each player\'s draw step, that player counts the cards in his or her hand, puts them on the bottom of his or her library, and then draws that many cards.').
card_image_name('teferi\'s puzzle box'/'6ED', 'teferi\'s puzzle box').
card_uid('teferi\'s puzzle box'/'6ED', '6ED:Teferi\'s Puzzle Box:teferi\'s puzzle box').
card_rarity('teferi\'s puzzle box'/'6ED', 'Rare').
card_artist('teferi\'s puzzle box'/'6ED', 'Kaja Foglio').
card_number('teferi\'s puzzle box'/'6ED', '314').
card_multiverse_id('teferi\'s puzzle box'/'6ED', '15410').

card_in_set('terror', '6ED').
card_original_type('terror'/'6ED', 'Instant').
card_original_text('terror'/'6ED', 'Destroy target nonartifact, nonblack creature. It can\'t be regenerated.').
card_image_name('terror'/'6ED', 'terror').
card_uid('terror'/'6ED', '6ED:Terror:terror').
card_rarity('terror'/'6ED', 'Common').
card_artist('terror'/'6ED', 'Ron Spencer').
card_number('terror'/'6ED', '160').
card_multiverse_id('terror'/'6ED', '16624').

card_in_set('the hive', '6ED').
card_original_type('the hive'/'6ED', 'Artifact').
card_original_text('the hive'/'6ED', '{5}, {T}: Put a 1/1 Wasp artifact creature token into play. That creature has flying.').
card_image_name('the hive'/'6ED', 'the hive').
card_uid('the hive'/'6ED', '6ED:The Hive:the hive').
card_rarity('the hive'/'6ED', 'Rare').
card_artist('the hive'/'6ED', 'Sandra Everingham').
card_number('the hive'/'6ED', '315').
card_flavor_text('the hive'/'6ED', 'Mouths and wings beyond numbering, but a single mind to guide them.').
card_multiverse_id('the hive'/'6ED', '11530').

card_in_set('thicket basilisk', '6ED').
card_original_type('thicket basilisk'/'6ED', 'Creature — Basilisk').
card_original_text('thicket basilisk'/'6ED', 'Whenever Thicket Basilisk blocks or becomes blocked by a non-Wall creature, destroy that creature at end of combat.').
card_image_name('thicket basilisk'/'6ED', 'thicket basilisk').
card_uid('thicket basilisk'/'6ED', '6ED:Thicket Basilisk:thicket basilisk').
card_rarity('thicket basilisk'/'6ED', 'Uncommon').
card_artist('thicket basilisk'/'6ED', 'Dan Frazier').
card_number('thicket basilisk'/'6ED', '256').
card_flavor_text('thicket basilisk'/'6ED', 'Moss-covered statues littered the area, a macabre monument to the basilisk\'s terrible power.').
card_multiverse_id('thicket basilisk'/'6ED', '16455').

card_in_set('throne of bone', '6ED').
card_original_type('throne of bone'/'6ED', 'Artifact').
card_original_text('throne of bone'/'6ED', 'Whenever a player plays a black spell, you may pay {1}. If you do, you gain 1 life.').
card_image_name('throne of bone'/'6ED', 'throne of bone').
card_uid('throne of bone'/'6ED', '6ED:Throne of Bone:throne of bone').
card_rarity('throne of bone'/'6ED', 'Uncommon').
card_artist('throne of bone'/'6ED', 'Donato Giancola').
card_number('throne of bone'/'6ED', '316').
card_multiverse_id('throne of bone'/'6ED', '14760').

card_in_set('tidal surge', '6ED').
card_original_type('tidal surge'/'6ED', 'Sorcery').
card_original_text('tidal surge'/'6ED', 'Tap up to three target creatures without flying.').
card_image_name('tidal surge'/'6ED', 'tidal surge').
card_uid('tidal surge'/'6ED', '6ED:Tidal Surge:tidal surge').
card_rarity('tidal surge'/'6ED', 'Common').
card_artist('tidal surge'/'6ED', 'Doug Chaffee').
card_number('tidal surge'/'6ED', '102').
card_flavor_text('tidal surge'/'6ED', '\"It is nature that gives us our boundaries and nature that enforces them.\"\n—Eladamri, Lord of Leaves').
card_multiverse_id('tidal surge'/'6ED', '16429').

card_in_set('trained armodon', '6ED').
card_original_type('trained armodon'/'6ED', 'Creature — Elephant').
card_original_text('trained armodon'/'6ED', '').
card_image_name('trained armodon'/'6ED', 'trained armodon').
card_uid('trained armodon'/'6ED', '6ED:Trained Armodon:trained armodon').
card_rarity('trained armodon'/'6ED', 'Common').
card_artist('trained armodon'/'6ED', 'Gary Leach').
card_number('trained armodon'/'6ED', '257').
card_flavor_text('trained armodon'/'6ED', 'These are its last days. Better to grow broad and heavy. Better that the enemy is crushed beneath its carcass.').
card_multiverse_id('trained armodon'/'6ED', '14675').

card_in_set('tranquil grove', '6ED').
card_original_type('tranquil grove'/'6ED', 'Enchantment').
card_original_text('tranquil grove'/'6ED', '{1}{G}{G} Destroy all other enchantments.').
card_image_name('tranquil grove'/'6ED', 'tranquil grove').
card_uid('tranquil grove'/'6ED', '6ED:Tranquil Grove:tranquil grove').
card_rarity('tranquil grove'/'6ED', 'Rare').
card_artist('tranquil grove'/'6ED', 'Dylan Martens').
card_number('tranquil grove'/'6ED', '258').
card_multiverse_id('tranquil grove'/'6ED', '11295').

card_in_set('tranquility', '6ED').
card_original_type('tranquility'/'6ED', 'Sorcery').
card_original_text('tranquility'/'6ED', 'Destroy all enchantments.').
card_image_name('tranquility'/'6ED', 'tranquility').
card_uid('tranquility'/'6ED', '6ED:Tranquility:tranquility').
card_rarity('tranquility'/'6ED', 'Common').
card_artist('tranquility'/'6ED', 'Douglas Shuler').
card_number('tranquility'/'6ED', '259').
card_multiverse_id('tranquility'/'6ED', '14685').

card_in_set('tremor', '6ED').
card_original_type('tremor'/'6ED', 'Sorcery').
card_original_text('tremor'/'6ED', 'Tremor deals 1 damage to each creature without flying.').
card_image_name('tremor'/'6ED', 'tremor').
card_uid('tremor'/'6ED', '6ED:Tremor:tremor').
card_rarity('tremor'/'6ED', 'Common').
card_artist('tremor'/'6ED', 'Pete Venters').
card_number('tremor'/'6ED', '211').
card_flavor_text('tremor'/'6ED', '\"Where do you run when the earth becomes your enemy?\"\n—Naimag, Femeref philosopher').
card_multiverse_id('tremor'/'6ED', '14613').

card_in_set('tundra wolves', '6ED').
card_original_type('tundra wolves'/'6ED', 'Creature — Wolf').
card_original_text('tundra wolves'/'6ED', 'First strike').
card_image_name('tundra wolves'/'6ED', 'tundra wolves').
card_uid('tundra wolves'/'6ED', '6ED:Tundra Wolves:tundra wolves').
card_rarity('tundra wolves'/'6ED', 'Common').
card_artist('tundra wolves'/'6ED', 'Quinton Hoover').
card_number('tundra wolves'/'6ED', '48').
card_flavor_text('tundra wolves'/'6ED', 'I heard their eerie howling, the wolves calling their kindred across the frozen plains.').
card_multiverse_id('tundra wolves'/'6ED', '11543').

card_in_set('uktabi orangutan', '6ED').
card_original_type('uktabi orangutan'/'6ED', 'Creature — Ape').
card_original_text('uktabi orangutan'/'6ED', 'When Uktabi Orangutan comes into play, destroy target artifact.').
card_image_name('uktabi orangutan'/'6ED', 'uktabi orangutan').
card_uid('uktabi orangutan'/'6ED', '6ED:Uktabi Orangutan:uktabi orangutan').
card_rarity('uktabi orangutan'/'6ED', 'Uncommon').
card_artist('uktabi orangutan'/'6ED', 'Una Fricker').
card_number('uktabi orangutan'/'6ED', '260').
card_flavor_text('uktabi orangutan'/'6ED', '\"Is it true that the apes wear furs of gold when they marry?\"\n—Rana, Suq\'Ata market fool').
card_multiverse_id('uktabi orangutan'/'6ED', '14690').

card_in_set('uktabi wildcats', '6ED').
card_original_type('uktabi wildcats'/'6ED', 'Creature — Cat Warrior').
card_original_text('uktabi wildcats'/'6ED', 'Uktabi Wildcats\'s power and toughness are each equal to the number of forests you control.\n{G}, Sacrifice a forest: Regenerate Uktabi Wildcats.').
card_image_name('uktabi wildcats'/'6ED', 'uktabi wildcats').
card_uid('uktabi wildcats'/'6ED', '6ED:Uktabi Wildcats:uktabi wildcats').
card_rarity('uktabi wildcats'/'6ED', 'Rare').
card_artist('uktabi wildcats'/'6ED', 'John Matson').
card_number('uktabi wildcats'/'6ED', '261').
card_multiverse_id('uktabi wildcats'/'6ED', '14721').

card_in_set('underground river', '6ED').
card_original_type('underground river'/'6ED', 'Land').
card_original_text('underground river'/'6ED', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {U} or {B} to your mana pool. Underground River deals 1 damage to you.').
card_image_name('underground river'/'6ED', 'underground river').
card_uid('underground river'/'6ED', '6ED:Underground River:underground river').
card_rarity('underground river'/'6ED', 'Rare').
card_artist('underground river'/'6ED', 'Jeff Miracola').
card_number('underground river'/'6ED', '330').
card_multiverse_id('underground river'/'6ED', '14728').

card_in_set('unseen walker', '6ED').
card_original_type('unseen walker'/'6ED', 'Creature — Dryad').
card_original_text('unseen walker'/'6ED', 'Forestwalk (This creature is unblockable if defending player controls a forest.)\n{1}{G}{G} Target creature gains forestwalk until end of turn.').
card_image_name('unseen walker'/'6ED', 'unseen walker').
card_uid('unseen walker'/'6ED', '6ED:Unseen Walker:unseen walker').
card_rarity('unseen walker'/'6ED', 'Uncommon').
card_artist('unseen walker'/'6ED', 'Alan Rabinowitz').
card_number('unseen walker'/'6ED', '262').
card_flavor_text('unseen walker'/'6ED', '\"To pass through the jungle: silence, courtesy, ferocity, as the occasion demands.\"\n—Kamau, \"Proper Passage\"').
card_multiverse_id('unseen walker'/'6ED', '16456').

card_in_set('unsummon', '6ED').
card_original_type('unsummon'/'6ED', 'Instant').
card_original_text('unsummon'/'6ED', 'Return target creature to its owner\'s hand.').
card_image_name('unsummon'/'6ED', 'unsummon').
card_uid('unsummon'/'6ED', '6ED:Unsummon:unsummon').
card_rarity('unsummon'/'6ED', 'Common').
card_artist('unsummon'/'6ED', 'Douglas Shuler').
card_number('unsummon'/'6ED', '103').
card_multiverse_id('unsummon'/'6ED', '14514').

card_in_set('untamed wilds', '6ED').
card_original_type('untamed wilds'/'6ED', 'Sorcery').
card_original_text('untamed wilds'/'6ED', 'Search your library for a basic land card and put that card into play. Then shuffle your library.').
card_image_name('untamed wilds'/'6ED', 'untamed wilds').
card_uid('untamed wilds'/'6ED', '6ED:Untamed Wilds:untamed wilds').
card_rarity('untamed wilds'/'6ED', 'Uncommon').
card_artist('untamed wilds'/'6ED', 'NéNé Thomas').
card_number('untamed wilds'/'6ED', '263').
card_multiverse_id('untamed wilds'/'6ED', '14695').

card_in_set('unyaro griffin', '6ED').
card_original_type('unyaro griffin'/'6ED', 'Creature — Griffin').
card_original_text('unyaro griffin'/'6ED', 'Flying\nSacrifice Unyaro Griffin: Counter target red instant or sorcery spell.').
card_image_name('unyaro griffin'/'6ED', 'unyaro griffin').
card_uid('unyaro griffin'/'6ED', '6ED:Unyaro Griffin:unyaro griffin').
card_rarity('unyaro griffin'/'6ED', 'Uncommon').
card_artist('unyaro griffin'/'6ED', 'Al Davidson').
card_number('unyaro griffin'/'6ED', '49').
card_flavor_text('unyaro griffin'/'6ED', 'The griffin\'s shadow gives courage to the righteous and dread to the idle.').
card_multiverse_id('unyaro griffin'/'6ED', '15357').

card_in_set('vampiric tutor', '6ED').
card_original_type('vampiric tutor'/'6ED', 'Instant').
card_original_text('vampiric tutor'/'6ED', 'Search your library for a card, then shuffle your library and put that card on top of it. You lose 2 life.').
card_image_name('vampiric tutor'/'6ED', 'vampiric tutor').
card_uid('vampiric tutor'/'6ED', '6ED:Vampiric Tutor:vampiric tutor').
card_rarity('vampiric tutor'/'6ED', 'Rare').
card_artist('vampiric tutor'/'6ED', 'Gary Leach').
card_number('vampiric tutor'/'6ED', '161').
card_flavor_text('vampiric tutor'/'6ED', '\"I write upon clean white parchment with a sharp quill and the blood of my students, divining their secrets.\"\n—Shauku, Endbringer').
card_multiverse_id('vampiric tutor'/'6ED', '15393').

card_in_set('venerable monk', '6ED').
card_original_type('venerable monk'/'6ED', 'Creature — Cleric').
card_original_text('venerable monk'/'6ED', 'When Venerable Monk comes into play, you gain 2 life.').
card_image_name('venerable monk'/'6ED', 'venerable monk').
card_uid('venerable monk'/'6ED', '6ED:Venerable Monk:venerable monk').
card_rarity('venerable monk'/'6ED', 'Common').
card_artist('venerable monk'/'6ED', 'D. Alexander Gregory').
card_number('venerable monk'/'6ED', '50').
card_flavor_text('venerable monk'/'6ED', 'His presence brings not only a strong arm but also renewed hope.').
card_multiverse_id('venerable monk'/'6ED', '14454').

card_in_set('verduran enchantress', '6ED').
card_original_type('verduran enchantress'/'6ED', 'Creature — Wizard').
card_original_text('verduran enchantress'/'6ED', 'Whenever you play an enchantment spell, you may draw a card.').
card_image_name('verduran enchantress'/'6ED', 'verduran enchantress').
card_uid('verduran enchantress'/'6ED', '6ED:Verduran Enchantress:verduran enchantress').
card_rarity('verduran enchantress'/'6ED', 'Rare').
card_artist('verduran enchantress'/'6ED', 'Kev Brockschmidt').
card_number('verduran enchantress'/'6ED', '264').
card_flavor_text('verduran enchantress'/'6ED', 'Some say magic was first practiced by women, who have always felt strong ties to the land.').
card_multiverse_id('verduran enchantress'/'6ED', '11558').

card_in_set('vertigo', '6ED').
card_original_type('vertigo'/'6ED', 'Instant').
card_original_text('vertigo'/'6ED', 'Vertigo deals 2 damage to target creature with flying. That creature loses flying until end of turn.').
card_image_name('vertigo'/'6ED', 'vertigo').
card_uid('vertigo'/'6ED', '6ED:Vertigo:vertigo').
card_rarity('vertigo'/'6ED', 'Uncommon').
card_artist('vertigo'/'6ED', 'Drew Tucker').
card_number('vertigo'/'6ED', '212').
card_flavor_text('vertigo'/'6ED', '\"I saw the wizard gesturing, but I didn\'t guess his plan until my aesthir cried out and went into a dive.\"\n—Arna Kennerüd, skyknight').
card_multiverse_id('vertigo'/'6ED', '16447').

card_in_set('viashino warrior', '6ED').
card_original_type('viashino warrior'/'6ED', 'Creature — Viashino').
card_original_text('viashino warrior'/'6ED', '').
card_image_name('viashino warrior'/'6ED', 'viashino warrior').
card_uid('viashino warrior'/'6ED', '6ED:Viashino Warrior:viashino warrior').
card_rarity('viashino warrior'/'6ED', 'Common').
card_artist('viashino warrior'/'6ED', 'Roger Raupp').
card_number('viashino warrior'/'6ED', '213').
card_flavor_text('viashino warrior'/'6ED', '\"When traveling the Great Desert avoid wearing the scales of lizards, for the Viashino rule the sands and look poorly on the skinning of their cousins.\"\n—Zhalfirin Guide to the Desert').
card_multiverse_id('viashino warrior'/'6ED', '12974').

card_in_set('vitalize', '6ED').
card_original_type('vitalize'/'6ED', 'Instant').
card_original_text('vitalize'/'6ED', 'Untap all creatures you control.').
card_image_name('vitalize'/'6ED', 'vitalize').
card_uid('vitalize'/'6ED', '6ED:Vitalize:vitalize').
card_rarity('vitalize'/'6ED', 'Common').
card_artist('vitalize'/'6ED', 'Pete Venters').
card_number('vitalize'/'6ED', '265').
card_flavor_text('vitalize'/'6ED', '\"After waking to the magic of Llanowar, no dreams can compare.\"\n—Mirri of the Weatherlight').
card_multiverse_id('vitalize'/'6ED', '14667').

card_in_set('vodalian soldiers', '6ED').
card_original_type('vodalian soldiers'/'6ED', 'Creature — Merfolk').
card_original_text('vodalian soldiers'/'6ED', '').
card_image_name('vodalian soldiers'/'6ED', 'vodalian soldiers').
card_uid('vodalian soldiers'/'6ED', '6ED:Vodalian Soldiers:vodalian soldiers').
card_rarity('vodalian soldiers'/'6ED', 'Common').
card_artist('vodalian soldiers'/'6ED', 'Melissa A. Benson').
card_number('vodalian soldiers'/'6ED', '104').
card_flavor_text('vodalian soldiers'/'6ED', '\"Vodalian rank is displayed by the colors and patterns of their skin. Beware the color red; that is the badge of the empress\'s favor.\"\n—Corbio, pearl diver').
card_multiverse_id('vodalian soldiers'/'6ED', '16432').

card_in_set('volcanic dragon', '6ED').
card_original_type('volcanic dragon'/'6ED', 'Creature — Dragon').
card_original_text('volcanic dragon'/'6ED', 'Flying; haste (This creature may attack and {T} the turn it comes under your control.)').
card_image_name('volcanic dragon'/'6ED', 'volcanic dragon').
card_uid('volcanic dragon'/'6ED', '6ED:Volcanic Dragon:volcanic dragon').
card_rarity('volcanic dragon'/'6ED', 'Rare').
card_artist('volcanic dragon'/'6ED', 'Janine Johnston').
card_number('volcanic dragon'/'6ED', '214').
card_flavor_text('volcanic dragon'/'6ED', 'Speed and fire are always a deadly combination.').
card_multiverse_id('volcanic dragon'/'6ED', '14660').

card_in_set('volcanic geyser', '6ED').
card_original_type('volcanic geyser'/'6ED', 'Instant').
card_original_text('volcanic geyser'/'6ED', 'Volcanic Geyser deals X damage to target creature or player.').
card_image_name('volcanic geyser'/'6ED', 'volcanic geyser').
card_uid('volcanic geyser'/'6ED', '6ED:Volcanic Geyser:volcanic geyser').
card_rarity('volcanic geyser'/'6ED', 'Uncommon').
card_artist('volcanic geyser'/'6ED', 'David O\'Connor').
card_number('volcanic geyser'/'6ED', '215').
card_flavor_text('volcanic geyser'/'6ED', 'My thunder comes before the lightning; my lightning comes before the clouds; my rain dries all the land it touches. What am I?\n—Femeref riddle').
card_multiverse_id('volcanic geyser'/'6ED', '14632').

card_in_set('waiting in the weeds', '6ED').
card_original_type('waiting in the weeds'/'6ED', 'Sorcery').
card_original_text('waiting in the weeds'/'6ED', 'Each player counts the untapped forests he or she controls and puts that many 1/1 green Cat creature tokens into play.').
card_image_name('waiting in the weeds'/'6ED', 'waiting in the weeds').
card_uid('waiting in the weeds'/'6ED', '6ED:Waiting in the Weeds:waiting in the weeds').
card_rarity('waiting in the weeds'/'6ED', 'Rare').
card_artist('waiting in the weeds'/'6ED', 'Susan Van Camp').
card_number('waiting in the weeds'/'6ED', '266').
card_flavor_text('waiting in the weeds'/'6ED', 'The trees have ears, eyes, and teeth.').
card_multiverse_id('waiting in the weeds'/'6ED', '15434').

card_in_set('wall of air', '6ED').
card_original_type('wall of air'/'6ED', 'Creature — Wall').
card_original_text('wall of air'/'6ED', '(Walls can\'t attack.)\nFlying').
card_image_name('wall of air'/'6ED', 'wall of air').
card_uid('wall of air'/'6ED', '6ED:Wall of Air:wall of air').
card_rarity('wall of air'/'6ED', 'Uncommon').
card_artist('wall of air'/'6ED', 'Richard Thomas').
card_number('wall of air'/'6ED', '105').
card_flavor_text('wall of air'/'6ED', 'When no falcons fly, beware the sky.\n—Femeref aphorism').
card_multiverse_id('wall of air'/'6ED', '14536').

card_in_set('wall of fire', '6ED').
card_original_type('wall of fire'/'6ED', 'Creature — Wall').
card_original_text('wall of fire'/'6ED', '(Walls can\'t attack.)\n{R} Wall of Fire gets +1/+0 until end of turn.').
card_image_name('wall of fire'/'6ED', 'wall of fire').
card_uid('wall of fire'/'6ED', '6ED:Wall of Fire:wall of fire').
card_rarity('wall of fire'/'6ED', 'Uncommon').
card_artist('wall of fire'/'6ED', 'Richard Thomas').
card_number('wall of fire'/'6ED', '216').
card_flavor_text('wall of fire'/'6ED', '\"Struggle, and you will only fan the flames.\"\n—Talibah, embermage').
card_multiverse_id('wall of fire'/'6ED', '14639').

card_in_set('wall of swords', '6ED').
card_original_type('wall of swords'/'6ED', 'Creature — Wall').
card_original_text('wall of swords'/'6ED', '(Walls can\'t attack.)\nFlying').
card_image_name('wall of swords'/'6ED', 'wall of swords').
card_uid('wall of swords'/'6ED', '6ED:Wall of Swords:wall of swords').
card_rarity('wall of swords'/'6ED', 'Uncommon').
card_artist('wall of swords'/'6ED', 'Brian Snõddy').
card_number('wall of swords'/'6ED', '51').
card_flavor_text('wall of swords'/'6ED', 'Sharper than wind, lighter than air.').
card_multiverse_id('wall of swords'/'6ED', '14487').

card_in_set('wand of denial', '6ED').
card_original_type('wand of denial'/'6ED', 'Artifact').
card_original_text('wand of denial'/'6ED', '{T}: Look at the top card of target player\'s library. If it\'s a nonland card, you may pay 2 life. If you do, put it into that player\'s graveyard.').
card_image_name('wand of denial'/'6ED', 'wand of denial').
card_uid('wand of denial'/'6ED', '6ED:Wand of Denial:wand of denial').
card_rarity('wand of denial'/'6ED', 'Rare').
card_artist('wand of denial'/'6ED', 'Steve Luke').
card_number('wand of denial'/'6ED', '317').
card_flavor_text('wand of denial'/'6ED', 'You\'ll never miss what you never had.').
card_multiverse_id('wand of denial'/'6ED', '15411').

card_in_set('warmth', '6ED').
card_original_type('warmth'/'6ED', 'Enchantment').
card_original_text('warmth'/'6ED', 'Whenever one of your opponents plays a red spell, you gain 2 life.').
card_image_name('warmth'/'6ED', 'warmth').
card_uid('warmth'/'6ED', '6ED:Warmth:warmth').
card_rarity('warmth'/'6ED', 'Uncommon').
card_artist('warmth'/'6ED', 'Drew Tucker').
card_number('warmth'/'6ED', '52').
card_flavor_text('warmth'/'6ED', '\"Flame grows gentle with but a little distance.\"\n—Orim, Samite healer').
card_multiverse_id('warmth'/'6ED', '14482').

card_in_set('warrior\'s honor', '6ED').
card_original_type('warrior\'s honor'/'6ED', 'Instant').
card_original_text('warrior\'s honor'/'6ED', 'Creatures you control get +1/+1 until end of turn.').
card_image_name('warrior\'s honor'/'6ED', 'warrior\'s honor').
card_uid('warrior\'s honor'/'6ED', '6ED:Warrior\'s Honor:warrior\'s honor').
card_rarity('warrior\'s honor'/'6ED', 'Common').
card_artist('warrior\'s honor'/'6ED', 'D. Alexander Gregory').
card_number('warrior\'s honor'/'6ED', '53').
card_multiverse_id('warrior\'s honor'/'6ED', '14465').

card_in_set('warthog', '6ED').
card_original_type('warthog'/'6ED', 'Creature — Warthog').
card_original_text('warthog'/'6ED', 'Swampwalk (This creature is unblockable if defending player controls a swamp.)').
card_image_name('warthog'/'6ED', 'warthog').
card_uid('warthog'/'6ED', '6ED:Warthog:warthog').
card_rarity('warthog'/'6ED', 'Uncommon').
card_artist('warthog'/'6ED', 'Steve White').
card_number('warthog'/'6ED', '267').
card_flavor_text('warthog'/'6ED', '\"Too much work—it takes a long time to break them in, and more than a few recruits.\"\n—Grebog, goblin swine-rider').
card_multiverse_id('warthog'/'6ED', '16458').

card_in_set('wild growth', '6ED').
card_original_type('wild growth'/'6ED', 'Enchant Land').
card_original_text('wild growth'/'6ED', 'Whenever enchanted land is tapped for mana, it produces an additional {G}.').
card_image_name('wild growth'/'6ED', 'wild growth').
card_uid('wild growth'/'6ED', '6ED:Wild Growth:wild growth').
card_rarity('wild growth'/'6ED', 'Common').
card_artist('wild growth'/'6ED', 'Pat Morrissey').
card_number('wild growth'/'6ED', '268').
card_multiverse_id('wild growth'/'6ED', '14663').

card_in_set('wind drake', '6ED').
card_original_type('wind drake'/'6ED', 'Creature — Drake').
card_original_text('wind drake'/'6ED', 'Flying').
card_image_name('wind drake'/'6ED', 'wind drake').
card_uid('wind drake'/'6ED', '6ED:Wind Drake:wind drake').
card_rarity('wind drake'/'6ED', 'Common').
card_artist('wind drake'/'6ED', 'Zina Saunders').
card_number('wind drake'/'6ED', '106').
card_flavor_text('wind drake'/'6ED', '\"No bird soars too high, if he soars with his own wings.\"\n—William Blake,\nThe Marriage of Heaven and Hell').
card_multiverse_id('wind drake'/'6ED', '14521').

card_in_set('wind spirit', '6ED').
card_original_type('wind spirit'/'6ED', 'Creature — Spirit').
card_original_text('wind spirit'/'6ED', 'Flying\nWind Spirit can\'t be blocked by only one creature each combat.').
card_image_name('wind spirit'/'6ED', 'wind spirit').
card_uid('wind spirit'/'6ED', '6ED:Wind Spirit:wind spirit').
card_rarity('wind spirit'/'6ED', 'Uncommon').
card_artist('wind spirit'/'6ED', 'Kaja Foglio').
card_number('wind spirit'/'6ED', '107').
card_flavor_text('wind spirit'/'6ED', '\" . . . When the trees bow down their heads,\nThe wind is passing by.\"\n—Christina Rossetti,\n\"Who Has Seen the Wind?\"').
card_multiverse_id('wind spirit'/'6ED', '16437').

card_in_set('wooden sphere', '6ED').
card_original_type('wooden sphere'/'6ED', 'Artifact').
card_original_text('wooden sphere'/'6ED', 'Whenever a player plays a green spell, you may pay {1}. If you do, you gain 1 life.').
card_image_name('wooden sphere'/'6ED', 'wooden sphere').
card_uid('wooden sphere'/'6ED', '6ED:Wooden Sphere:wooden sphere').
card_rarity('wooden sphere'/'6ED', 'Uncommon').
card_artist('wooden sphere'/'6ED', 'Donato Giancola').
card_number('wooden sphere'/'6ED', '318').
card_multiverse_id('wooden sphere'/'6ED', '14763').

card_in_set('worldly tutor', '6ED').
card_original_type('worldly tutor'/'6ED', 'Instant').
card_original_text('worldly tutor'/'6ED', 'Search your library for a creature card and reveal that card. Shuffle your library, then put the card on top of it.').
card_image_name('worldly tutor'/'6ED', 'worldly tutor').
card_uid('worldly tutor'/'6ED', '6ED:Worldly Tutor:worldly tutor').
card_rarity('worldly tutor'/'6ED', 'Uncommon').
card_artist('worldly tutor'/'6ED', 'David O\'Connor').
card_number('worldly tutor'/'6ED', '269').
card_flavor_text('worldly tutor'/'6ED', '\"Aselbo soon had the rhino eating from his palm and the snake waiting at his heels.\"\n—Afari, Tales').
card_multiverse_id('worldly tutor'/'6ED', '15429').

card_in_set('wrath of god', '6ED').
card_original_type('wrath of god'/'6ED', 'Sorcery').
card_original_text('wrath of god'/'6ED', 'Destroy all creatures. They can\'t be regenerated.').
card_image_name('wrath of god'/'6ED', 'wrath of god').
card_uid('wrath of god'/'6ED', '6ED:Wrath of God:wrath of god').
card_rarity('wrath of god'/'6ED', 'Rare').
card_artist('wrath of god'/'6ED', 'Quinton Hoover').
card_number('wrath of god'/'6ED', '54').
card_multiverse_id('wrath of god'/'6ED', '14498').

card_in_set('wyluli wolf', '6ED').
card_original_type('wyluli wolf'/'6ED', 'Creature — Wolf').
card_original_text('wyluli wolf'/'6ED', '{T}: Target creature gets +1/+1 until end of turn.').
card_image_name('wyluli wolf'/'6ED', 'wyluli wolf').
card_uid('wyluli wolf'/'6ED', '6ED:Wyluli Wolf:wyluli wolf').
card_rarity('wyluli wolf'/'6ED', 'Rare').
card_artist('wyluli wolf'/'6ED', 'Susan Van Camp').
card_number('wyluli wolf'/'6ED', '270').
card_flavor_text('wyluli wolf'/'6ED', '\"When one wolf calls, others follow. Who wants to fight creatures that eat scorpions?\"\n—Maimun al-Wyluli, diary').
card_multiverse_id('wyluli wolf'/'6ED', '14720').

card_in_set('zombie master', '6ED').
card_original_type('zombie master'/'6ED', 'Creature — Lord').
card_original_text('zombie master'/'6ED', 'All Zombies gain \"{B} Regenerate this creature\" and swampwalk. (They\'re unblockable if defending player controls a swamp.)').
card_image_name('zombie master'/'6ED', 'zombie master').
card_uid('zombie master'/'6ED', '6ED:Zombie Master:zombie master').
card_rarity('zombie master'/'6ED', 'Rare').
card_artist('zombie master'/'6ED', 'Jeff A. Menges').
card_number('zombie master'/'6ED', '162').
card_flavor_text('zombie master'/'6ED', 'He controlled the zombies even before his own death; now nothing can make them betray him.').
card_multiverse_id('zombie master'/'6ED', '14599').

card_in_set('zur\'s weirding', '6ED').
card_original_type('zur\'s weirding'/'6ED', 'Enchantment').
card_original_text('zur\'s weirding'/'6ED', 'Players play with their hands revealed.\nWhenever a player would draw a card, instead reveal it. Any other player may pay 2 life to put that card into its owner\'s graveyard. If no one does, that player then draws the card.').
card_image_name('zur\'s weirding'/'6ED', 'zur\'s weirding').
card_uid('zur\'s weirding'/'6ED', '6ED:Zur\'s Weirding:zur\'s weirding').
card_rarity('zur\'s weirding'/'6ED', 'Rare').
card_artist('zur\'s weirding'/'6ED', 'Liz Danforth').
card_number('zur\'s weirding'/'6ED', '108').
card_multiverse_id('zur\'s weirding'/'6ED', '15372').
