% Wizards Play Network

set('pWPN').
set_name('pWPN', 'Wizards Play Network').
set_release_date('pWPN', '2008-10-01').
set_border('pWPN', 'black').
set_type('pWPN', 'promo').

card_in_set('auramancer', 'pWPN').
card_original_type('auramancer'/'pWPN', 'Creature — Human Wizard').
card_original_text('auramancer'/'pWPN', '').
card_image_name('auramancer'/'pWPN', 'auramancer').
card_uid('auramancer'/'pWPN', 'pWPN:Auramancer:auramancer').
card_rarity('auramancer'/'pWPN', 'Special').
card_artist('auramancer'/'pWPN', 'Wayne Reynolds').
card_number('auramancer'/'pWPN', '77').
card_flavor_text('auramancer'/'pWPN', '\"In memories, we can find our deepest reserves of strength.\"').

card_in_set('bloodcrazed neonate', 'pWPN').
card_original_type('bloodcrazed neonate'/'pWPN', 'Creature — Vampire').
card_original_text('bloodcrazed neonate'/'pWPN', '').
card_first_print('bloodcrazed neonate', 'pWPN').
card_image_name('bloodcrazed neonate'/'pWPN', 'bloodcrazed neonate').
card_uid('bloodcrazed neonate'/'pWPN', 'pWPN:Bloodcrazed Neonate:bloodcrazed neonate').
card_rarity('bloodcrazed neonate'/'pWPN', 'Special').
card_artist('bloodcrazed neonate'/'pWPN', 'Dave Kendall').
card_number('bloodcrazed neonate'/'pWPN', '83').
card_flavor_text('bloodcrazed neonate'/'pWPN', 'While elder vampires select their meals with care, the newly sired frenzy at the first whiff.').

card_in_set('boneyard wurm', 'pWPN').
card_original_type('boneyard wurm'/'pWPN', 'Creature — Wurm').
card_original_text('boneyard wurm'/'pWPN', '').
card_first_print('boneyard wurm', 'pWPN').
card_image_name('boneyard wurm'/'pWPN', 'boneyard wurm').
card_uid('boneyard wurm'/'pWPN', 'pWPN:Boneyard Wurm:boneyard wurm').
card_rarity('boneyard wurm'/'pWPN', 'Special').
card_artist('boneyard wurm'/'pWPN', 'Austin Hsu').
card_number('boneyard wurm'/'pWPN', '84').
card_flavor_text('boneyard wurm'/'pWPN', 'The only thing it likes more than discovering a pit of bones is adding to it.').

card_in_set('circle of flame', 'pWPN').
card_original_type('circle of flame'/'pWPN', 'Enchantment').
card_original_text('circle of flame'/'pWPN', '').
card_first_print('circle of flame', 'pWPN').
card_image_name('circle of flame'/'pWPN', 'circle of flame').
card_uid('circle of flame'/'pWPN', 'pWPN:Circle of Flame:circle of flame').
card_rarity('circle of flame'/'pWPN', 'Special').
card_artist('circle of flame'/'pWPN', 'James Paick').
card_number('circle of flame'/'pWPN', '78').
card_flavor_text('circle of flame'/'pWPN', '\"Which do you think is a better deterrent: a moat of water or one of fire?\"\n—Chandra Nalaar').

card_in_set('curse of the bloody tome', 'pWPN').
card_original_type('curse of the bloody tome'/'pWPN', 'Enchantment — Aura Curse').
card_original_text('curse of the bloody tome'/'pWPN', '').
card_first_print('curse of the bloody tome', 'pWPN').
card_image_name('curse of the bloody tome'/'pWPN', 'curse of the bloody tome').
card_uid('curse of the bloody tome'/'pWPN', 'pWPN:Curse of the Bloody Tome:curse of the bloody tome').
card_rarity('curse of the bloody tome'/'pWPN', 'Special').
card_artist('curse of the bloody tome'/'pWPN', 'John Stanko').
card_number('curse of the bloody tome'/'pWPN', '80').
card_flavor_text('curse of the bloody tome'/'pWPN', 'After seeing his life\'s work drip away, the mage decided it was a good time to go crazy.').

card_in_set('curse of thirst', 'pWPN').
card_original_type('curse of thirst'/'pWPN', 'Enchantment — Aura Curse').
card_original_text('curse of thirst'/'pWPN', '').
card_first_print('curse of thirst', 'pWPN').
card_image_name('curse of thirst'/'pWPN', 'curse of thirst').
card_uid('curse of thirst'/'pWPN', 'pWPN:Curse of Thirst:curse of thirst').
card_rarity('curse of thirst'/'pWPN', 'Special').
card_artist('curse of thirst'/'pWPN', 'Dave Kendall').
card_number('curse of thirst'/'pWPN', '81').
card_flavor_text('curse of thirst'/'pWPN', '\"May you drink until you drown but never be sated.\"\n—Odila, witch of Morkrut').

card_in_set('curse of wizardry', 'pWPN').
card_original_type('curse of wizardry'/'pWPN', 'Enchantment').
card_original_text('curse of wizardry'/'pWPN', '').
card_first_print('curse of wizardry', 'pWPN').
card_image_name('curse of wizardry'/'pWPN', 'curse of wizardry').
card_uid('curse of wizardry'/'pWPN', 'pWPN:Curse of Wizardry:curse of wizardry').
card_rarity('curse of wizardry'/'pWPN', 'Special').
card_artist('curse of wizardry'/'pWPN', 'Karl Kopinski').
card_number('curse of wizardry'/'pWPN', '47').
card_flavor_text('curse of wizardry'/'pWPN', '\"We must all push through the pain to heal our world.\"\n—Ayli, Kamsa cleric').

card_in_set('deathless angel', 'pWPN').
card_original_type('deathless angel'/'pWPN', 'Creature — Angel').
card_original_text('deathless angel'/'pWPN', '').
card_first_print('deathless angel', 'pWPN').
card_image_name('deathless angel'/'pWPN', 'deathless angel').
card_uid('deathless angel'/'pWPN', 'pWPN:Deathless Angel:deathless angel').
card_rarity('deathless angel'/'pWPN', 'Special').
card_artist('deathless angel'/'pWPN', 'Johann Bodin').
card_number('deathless angel'/'pWPN', '49').

card_in_set('emeria angel', 'pWPN').
card_original_type('emeria angel'/'pWPN', 'Creature — Angel').
card_original_text('emeria angel'/'pWPN', '').
card_first_print('emeria angel', 'pWPN').
card_image_name('emeria angel'/'pWPN', 'emeria angel').
card_uid('emeria angel'/'pWPN', 'pWPN:Emeria Angel:emeria angel').
card_rarity('emeria angel'/'pWPN', 'Special').
card_artist('emeria angel'/'pWPN', 'Steve Argyle').
card_number('emeria angel'/'pWPN', '35').

card_in_set('fling', 'pWPN').
card_original_type('fling'/'pWPN', 'Instant').
card_original_text('fling'/'pWPN', '').
card_image_name('fling'/'pWPN', 'fling1').
card_uid('fling'/'pWPN', 'pWPN:Fling:fling1').
card_rarity('fling'/'pWPN', 'Special').
card_artist('fling'/'pWPN', 'Daren Bader').
card_number('fling'/'pWPN', '50').
card_flavor_text('fling'/'pWPN', 'Sometimes the cannoneer must become the cannonball.').

card_in_set('fling', 'pWPN').
card_original_type('fling'/'pWPN', 'Instant').
card_original_text('fling'/'pWPN', '').
card_image_name('fling'/'pWPN', 'fling2').
card_uid('fling'/'pWPN', 'pWPN:Fling:fling2').
card_rarity('fling'/'pWPN', 'Special').
card_artist('fling'/'pWPN', 'Daren Bader').
card_number('fling'/'pWPN', '69').
card_flavor_text('fling'/'pWPN', 'Sometimes the cannoneer must become the cannonball.').

card_in_set('gather the townsfolk', 'pWPN').
card_original_type('gather the townsfolk'/'pWPN', 'Sorcery').
card_original_text('gather the townsfolk'/'pWPN', '').
card_first_print('gather the townsfolk', 'pWPN').
card_image_name('gather the townsfolk'/'pWPN', 'gather the townsfolk').
card_uid('gather the townsfolk'/'pWPN', 'pWPN:Gather the Townsfolk:gather the townsfolk').
card_rarity('gather the townsfolk'/'pWPN', 'Special').
card_artist('gather the townsfolk'/'pWPN', 'Bud Cook').
card_number('gather the townsfolk'/'pWPN', '79').
card_flavor_text('gather the townsfolk'/'pWPN', 'In the memories of those they lost lies the strength needed to defend their city.').

card_in_set('golem\'s heart', 'pWPN').
card_original_type('golem\'s heart'/'pWPN', 'Artifact').
card_original_text('golem\'s heart'/'pWPN', '').
card_first_print('golem\'s heart', 'pWPN').
card_image_name('golem\'s heart'/'pWPN', 'golem\'s heart').
card_uid('golem\'s heart'/'pWPN', 'pWPN:Golem\'s Heart:golem\'s heart').
card_rarity('golem\'s heart'/'pWPN', 'Special').
card_artist('golem\'s heart'/'pWPN', 'Alan Pollack').
card_number('golem\'s heart'/'pWPN', '60').
card_flavor_text('golem\'s heart'/'pWPN', 'The heart of a golem gives life to more than just the iron husk around it.').

card_in_set('hada freeblade', 'pWPN').
card_original_type('hada freeblade'/'pWPN', 'Creature — Human Soldier Ally').
card_original_text('hada freeblade'/'pWPN', '').
card_first_print('hada freeblade', 'pWPN').
card_image_name('hada freeblade'/'pWPN', 'hada freeblade').
card_uid('hada freeblade'/'pWPN', 'pWPN:Hada Freeblade:hada freeblade').
card_rarity('hada freeblade'/'pWPN', 'Special').
card_artist('hada freeblade'/'pWPN', 'Cyril Van Der Haegen').
card_number('hada freeblade'/'pWPN', '38').

card_in_set('hellspark elemental', 'pWPN').
card_original_type('hellspark elemental'/'pWPN', 'Creature — Elemental').
card_original_text('hellspark elemental'/'pWPN', '').
card_first_print('hellspark elemental', 'pWPN').
card_image_name('hellspark elemental'/'pWPN', 'hellspark elemental').
card_uid('hellspark elemental'/'pWPN', 'pWPN:Hellspark Elemental:hellspark elemental').
card_rarity('hellspark elemental'/'pWPN', 'Special').
card_artist('hellspark elemental'/'pWPN', 'Nils Hamm').
card_number('hellspark elemental'/'pWPN', '25').

card_in_set('kalastria highborn', 'pWPN').
card_original_type('kalastria highborn'/'pWPN', 'Creature — Vampire Shaman').
card_original_text('kalastria highborn'/'pWPN', '').
card_first_print('kalastria highborn', 'pWPN').
card_image_name('kalastria highborn'/'pWPN', 'kalastria highborn').
card_uid('kalastria highborn'/'pWPN', 'pWPN:Kalastria Highborn:kalastria highborn').
card_rarity('kalastria highborn'/'pWPN', 'Special').
card_artist('kalastria highborn'/'pWPN', 'D. Alexander Gregory').
card_number('kalastria highborn'/'pWPN', '39').

card_in_set('kor duelist', 'pWPN').
card_original_type('kor duelist'/'pWPN', 'Creature — Kor Soldier').
card_original_text('kor duelist'/'pWPN', '').
card_first_print('kor duelist', 'pWPN').
card_image_name('kor duelist'/'pWPN', 'kor duelist').
card_uid('kor duelist'/'pWPN', 'pWPN:Kor Duelist:kor duelist').
card_rarity('kor duelist'/'pWPN', 'Special').
card_artist('kor duelist'/'pWPN', 'Kekai Kotaki').
card_number('kor duelist'/'pWPN', '32').
card_flavor_text('kor duelist'/'pWPN', '\"Swords cannot reach far enough. Chains cannot strike hard enough. An eternal dilemma, but a simple one.\"').

card_in_set('kor firewalker', 'pWPN').
card_original_type('kor firewalker'/'pWPN', 'Creature — Kor Soldier').
card_original_text('kor firewalker'/'pWPN', '').
card_first_print('kor firewalker', 'pWPN').
card_image_name('kor firewalker'/'pWPN', 'kor firewalker').
card_uid('kor firewalker'/'pWPN', 'pWPN:Kor Firewalker:kor firewalker').
card_rarity('kor firewalker'/'pWPN', 'Special').
card_artist('kor firewalker'/'pWPN', 'Alex Horley-Orlandelli').
card_number('kor firewalker'/'pWPN', '36').
card_flavor_text('kor firewalker'/'pWPN', '\"A river of lava is just another river to cross.\"').

card_in_set('leatherback baloth', 'pWPN').
card_original_type('leatherback baloth'/'pWPN', 'Creature — Beast').
card_original_text('leatherback baloth'/'pWPN', '').
card_first_print('leatherback baloth', 'pWPN').
card_image_name('leatherback baloth'/'pWPN', 'leatherback baloth').
card_uid('leatherback baloth'/'pWPN', 'pWPN:Leatherback Baloth:leatherback baloth').
card_rarity('leatherback baloth'/'pWPN', 'Special').
card_artist('leatherback baloth'/'pWPN', 'Chris Rahn').
card_number('leatherback baloth'/'pWPN', '37').
card_flavor_text('leatherback baloth'/'pWPN', 'Heavy enough to withstand the Roil, leatherback skeletons are havens for travelers in storms and landshifts.').

card_in_set('marisi\'s twinclaws', 'pWPN').
card_original_type('marisi\'s twinclaws'/'pWPN', 'Creature — Cat Warrior').
card_original_text('marisi\'s twinclaws'/'pWPN', '').
card_first_print('marisi\'s twinclaws', 'pWPN').
card_image_name('marisi\'s twinclaws'/'pWPN', 'marisi\'s twinclaws').
card_uid('marisi\'s twinclaws'/'pWPN', 'pWPN:Marisi\'s Twinclaws:marisi\'s twinclaws').
card_rarity('marisi\'s twinclaws'/'pWPN', 'Special').
card_artist('marisi\'s twinclaws'/'pWPN', 'Dominick Domingo').
card_number('marisi\'s twinclaws'/'pWPN', '26').
card_flavor_text('marisi\'s twinclaws'/'pWPN', '\"Looks like we\'re completely outnumbered.\"\n\"Lucky for us. I thought we\'d be bored.\"').

card_in_set('master\'s call', 'pWPN').
card_original_type('master\'s call'/'pWPN', 'Instant').
card_original_text('master\'s call'/'pWPN', '').
card_first_print('master\'s call', 'pWPN').
card_image_name('master\'s call'/'pWPN', 'master\'s call').
card_uid('master\'s call'/'pWPN', 'pWPN:Master\'s Call:master\'s call').
card_rarity('master\'s call'/'pWPN', 'Special').
card_artist('master\'s call'/'pWPN', 'Erica Yang').
card_number('master\'s call'/'pWPN', '64').
card_flavor_text('master\'s call'/'pWPN', 'The need to obey was inscribed on every plate in their bodies.').

card_in_set('maul splicer', 'pWPN').
card_original_type('maul splicer'/'pWPN', 'Creature — Human Artificer').
card_original_text('maul splicer'/'pWPN', '').
card_first_print('maul splicer', 'pWPN').
card_image_name('maul splicer'/'pWPN', 'maul splicer').
card_uid('maul splicer'/'pWPN', 'pWPN:Maul Splicer:maul splicer').
card_rarity('maul splicer'/'pWPN', 'Special').
card_artist('maul splicer'/'pWPN', 'Erica Yang').
card_number('maul splicer'/'pWPN', '72').

card_in_set('mind control', 'pWPN').
card_original_type('mind control'/'pWPN', 'Enchantment — Aura').
card_original_text('mind control'/'pWPN', '').
card_first_print('mind control', 'pWPN').
card_image_name('mind control'/'pWPN', 'mind control').
card_uid('mind control'/'pWPN', 'pWPN:Mind Control:mind control').
card_rarity('mind control'/'pWPN', 'Special').
card_artist('mind control'/'pWPN', 'Keith Garletts').
card_number('mind control'/'pWPN', '30').
card_flavor_text('mind control'/'pWPN', '\"Why fight the body when you can dominate the mind that rules it?\"\n—Jace Beleren').

card_in_set('mycoid shepherd', 'pWPN').
card_original_type('mycoid shepherd'/'pWPN', 'Creature — Fungus').
card_original_text('mycoid shepherd'/'pWPN', '').
card_first_print('mycoid shepherd', 'pWPN').
card_image_name('mycoid shepherd'/'pWPN', 'mycoid shepherd').
card_uid('mycoid shepherd'/'pWPN', 'pWPN:Mycoid Shepherd:mycoid shepherd').
card_rarity('mycoid shepherd'/'pWPN', 'Special').
card_artist('mycoid shepherd'/'pWPN', 'Jarreau Wimberly').
card_number('mycoid shepherd'/'pWPN', '28').

card_in_set('naya sojourners', 'pWPN').
card_original_type('naya sojourners'/'pWPN', 'Creature — Elf Shaman').
card_original_text('naya sojourners'/'pWPN', '').
card_first_print('naya sojourners', 'pWPN').
card_image_name('naya sojourners'/'pWPN', 'naya sojourners').
card_uid('naya sojourners'/'pWPN', 'pWPN:Naya Sojourners:naya sojourners').
card_rarity('naya sojourners'/'pWPN', 'Special').
card_artist('naya sojourners'/'pWPN', 'Michael Bruinsma').
card_number('naya sojourners'/'pWPN', '29').
card_flavor_text('naya sojourners'/'pWPN', 'The elves had entered a land like an open grave-an act of fortitude or folly?').

card_in_set('nearheath stalker', 'pWPN').
card_original_type('nearheath stalker'/'pWPN', 'Creature — Vampire Rogue').
card_original_text('nearheath stalker'/'pWPN', '').
card_first_print('nearheath stalker', 'pWPN').
card_image_name('nearheath stalker'/'pWPN', 'nearheath stalker').
card_uid('nearheath stalker'/'pWPN', 'pWPN:Nearheath Stalker:nearheath stalker').
card_rarity('nearheath stalker'/'pWPN', 'Special').
card_artist('nearheath stalker'/'pWPN', 'Steve Prescott').
card_number('nearheath stalker'/'pWPN', '82').
card_flavor_text('nearheath stalker'/'pWPN', '\"Do not speak of this to the elders. They look unfavorably upon my indulgences.\"').

card_in_set('nissa\'s chosen', 'pWPN').
card_original_type('nissa\'s chosen'/'pWPN', 'Creature — Elf Warrior').
card_original_text('nissa\'s chosen'/'pWPN', '').
card_first_print('nissa\'s chosen', 'pWPN').
card_image_name('nissa\'s chosen'/'pWPN', 'nissa\'s chosen').
card_uid('nissa\'s chosen'/'pWPN', 'pWPN:Nissa\'s Chosen:nissa\'s chosen').
card_rarity('nissa\'s chosen'/'pWPN', 'Special').
card_artist('nissa\'s chosen'/'pWPN', 'Sam Wood').
card_number('nissa\'s chosen'/'pWPN', '34').

card_in_set('path to exile', 'pWPN').
card_original_type('path to exile'/'pWPN', 'Instant').
card_original_text('path to exile'/'pWPN', '').
card_first_print('path to exile', 'pWPN').
card_image_name('path to exile'/'pWPN', 'path to exile').
card_uid('path to exile'/'pWPN', 'pWPN:Path to Exile:path to exile').
card_rarity('path to exile'/'pWPN', 'Special').
card_artist('path to exile'/'pWPN', 'Rebecca Guay').
card_number('path to exile'/'pWPN', '24').

card_in_set('pathrazer of ulamog', 'pWPN').
card_original_type('pathrazer of ulamog'/'pWPN', 'Creature — Eldrazi').
card_original_text('pathrazer of ulamog'/'pWPN', '').
card_first_print('pathrazer of ulamog', 'pWPN').
card_image_name('pathrazer of ulamog'/'pWPN', 'pathrazer of ulamog').
card_uid('pathrazer of ulamog'/'pWPN', 'pWPN:Pathrazer of Ulamog:pathrazer of ulamog').
card_rarity('pathrazer of ulamog'/'pWPN', 'Special').
card_artist('pathrazer of ulamog'/'pWPN', 'Goran Josic').
card_number('pathrazer of ulamog'/'pWPN', '46').
card_flavor_text('pathrazer of ulamog'/'pWPN', 'No thought but hunger. No strategy but destruction.').

card_in_set('plague myr', 'pWPN').
card_original_type('plague myr'/'pWPN', 'Artifact Creature — Myr').
card_original_text('plague myr'/'pWPN', '').
card_first_print('plague myr', 'pWPN').
card_image_name('plague myr'/'pWPN', 'plague myr').
card_uid('plague myr'/'pWPN', 'pWPN:Plague Myr:plague myr').
card_rarity('plague myr'/'pWPN', 'Special').
card_artist('plague myr'/'pWPN', 'Ryan Pancoast').
card_number('plague myr'/'pWPN', '65').
card_flavor_text('plague myr'/'pWPN', 'They watch for a new master, one more sinister than the last.').

card_in_set('plague stinger', 'pWPN').
card_original_type('plague stinger'/'pWPN', 'Creature — Insect Horror').
card_original_text('plague stinger'/'pWPN', '').
card_first_print('plague stinger', 'pWPN').
card_image_name('plague stinger'/'pWPN', 'plague stinger').
card_uid('plague stinger'/'pWPN', 'pWPN:Plague Stinger:plague stinger').
card_rarity('plague stinger'/'pWPN', 'Special').
card_artist('plague stinger'/'pWPN', 'Tomasz Jedruszek').
card_number('plague stinger'/'pWPN', '59').
card_flavor_text('plague stinger'/'pWPN', 'It leaves its victims one sting closer to phyresis.').

card_in_set('rise from the grave', 'pWPN').
card_original_type('rise from the grave'/'pWPN', 'Sorcery').
card_original_text('rise from the grave'/'pWPN', '').
card_first_print('rise from the grave', 'pWPN').
card_image_name('rise from the grave'/'pWPN', 'rise from the grave').
card_uid('rise from the grave'/'pWPN', 'pWPN:Rise from the Grave:rise from the grave').
card_rarity('rise from the grave'/'pWPN', 'Special').
card_artist('rise from the grave'/'pWPN', 'Thomas M. Baxa').
card_number('rise from the grave'/'pWPN', '31').
card_flavor_text('rise from the grave'/'pWPN', '\"Death is no excuse for disobedience.\"\n—Liliana Vess').

card_in_set('shrine of burning rage', 'pWPN').
card_original_type('shrine of burning rage'/'pWPN', 'Artifact').
card_original_text('shrine of burning rage'/'pWPN', '').
card_first_print('shrine of burning rage', 'pWPN').
card_image_name('shrine of burning rage'/'pWPN', 'shrine of burning rage').
card_uid('shrine of burning rage'/'pWPN', 'pWPN:Shrine of Burning Rage:shrine of burning rage').
card_rarity('shrine of burning rage'/'pWPN', 'Special').
card_artist('shrine of burning rage'/'pWPN', 'Nils Hamm').
card_number('shrine of burning rage'/'pWPN', '73').

card_in_set('signal pest', 'pWPN').
card_original_type('signal pest'/'pWPN', 'Artifact Creature — Pest').
card_original_text('signal pest'/'pWPN', '').
card_first_print('signal pest', 'pWPN').
card_image_name('signal pest'/'pWPN', 'signal pest').
card_uid('signal pest'/'pWPN', 'pWPN:Signal Pest:signal pest').
card_rarity('signal pest'/'pWPN', 'Special').
card_artist('signal pest'/'pWPN', 'Chippy').
card_number('signal pest'/'pWPN', '66').
card_flavor_text('signal pest'/'pWPN', 'It leaps from tree to tree, revealing the enemy\'s positions.').

card_in_set('skinrender', 'pWPN').
card_original_type('skinrender'/'pWPN', 'Creature — Zombie').
card_original_text('skinrender'/'pWPN', '').
card_first_print('skinrender', 'pWPN').
card_image_name('skinrender'/'pWPN', 'skinrender').
card_uid('skinrender'/'pWPN', 'pWPN:Skinrender:skinrender').
card_rarity('skinrender'/'pWPN', 'Special').
card_artist('skinrender'/'pWPN', 'Adrian Smith').
card_number('skinrender'/'pWPN', '63').
card_flavor_text('skinrender'/'pWPN', '\"Your creations are effective, Sheoldred, but we must unite the flesh, not merely flay it.\"\n—Elesh Norn, Grand Cenobite').

card_in_set('slave of bolas', 'pWPN').
card_original_type('slave of bolas'/'pWPN', 'Sorcery').
card_original_text('slave of bolas'/'pWPN', '').
card_first_print('slave of bolas', 'pWPN').
card_image_name('slave of bolas'/'pWPN', 'slave of bolas').
card_uid('slave of bolas'/'pWPN', 'pWPN:Slave of Bolas:slave of bolas').
card_rarity('slave of bolas'/'pWPN', 'Special').
card_artist('slave of bolas'/'pWPN', 'Karl Kopinski').
card_number('slave of bolas'/'pWPN', '27').
card_flavor_text('slave of bolas'/'pWPN', 'Nicol Bolas doesn\'t distinguish between servants and victims.').

card_in_set('sprouting thrinax', 'pWPN').
card_original_type('sprouting thrinax'/'pWPN', 'Creature — Lizard').
card_original_text('sprouting thrinax'/'pWPN', '').
card_first_print('sprouting thrinax', 'pWPN').
card_image_name('sprouting thrinax'/'pWPN', 'sprouting thrinax').
card_uid('sprouting thrinax'/'pWPN', 'pWPN:Sprouting Thrinax:sprouting thrinax').
card_rarity('sprouting thrinax'/'pWPN', 'Special').
card_artist('sprouting thrinax'/'pWPN', 'Jaime Jones').
card_number('sprouting thrinax'/'pWPN', '21').
card_flavor_text('sprouting thrinax'/'pWPN', 'The vast network of predation on Jund has actually caused some strange creatures to adapt to being eaten.').

card_in_set('staggershock', 'pWPN').
card_original_type('staggershock'/'pWPN', 'Instant').
card_original_text('staggershock'/'pWPN', '').
card_first_print('staggershock', 'pWPN').
card_image_name('staggershock'/'pWPN', 'staggershock').
card_uid('staggershock'/'pWPN', 'pWPN:Staggershock:staggershock').
card_rarity('staggershock'/'pWPN', 'Special').
card_artist('staggershock'/'pWPN', 'Raymond Swanland').
card_number('staggershock'/'pWPN', '48').

card_in_set('sylvan ranger', 'pWPN').
card_original_type('sylvan ranger'/'pWPN', 'Creature — Elf Scout').
card_original_text('sylvan ranger'/'pWPN', '').
card_first_print('sylvan ranger', 'pWPN').
card_image_name('sylvan ranger'/'pWPN', 'sylvan ranger1').
card_uid('sylvan ranger'/'pWPN', 'pWPN:Sylvan Ranger:sylvan ranger1').
card_rarity('sylvan ranger'/'pWPN', 'Special').
card_artist('sylvan ranger'/'pWPN', 'Ciruelo').
card_number('sylvan ranger'/'pWPN', '51').
card_flavor_text('sylvan ranger'/'pWPN', '\"Not all paths are found on the forest floor.\"').

card_in_set('sylvan ranger', 'pWPN').
card_original_type('sylvan ranger'/'pWPN', 'Creature — Elf Scout').
card_original_text('sylvan ranger'/'pWPN', '').
card_image_name('sylvan ranger'/'pWPN', 'sylvan ranger2').
card_uid('sylvan ranger'/'pWPN', 'pWPN:Sylvan Ranger:sylvan ranger2').
card_rarity('sylvan ranger'/'pWPN', 'Special').
card_artist('sylvan ranger'/'pWPN', 'Ciruelo').
card_number('sylvan ranger'/'pWPN', '70').
card_flavor_text('sylvan ranger'/'pWPN', '\"Not all paths are found on the forest floor.\"').

card_in_set('syphon mind', 'pWPN').
card_original_type('syphon mind'/'pWPN', 'Sorcery').
card_original_text('syphon mind'/'pWPN', '').
card_image_name('syphon mind'/'pWPN', 'syphon mind').
card_uid('syphon mind'/'pWPN', 'pWPN:Syphon Mind:syphon mind').
card_rarity('syphon mind'/'pWPN', 'Special').
card_artist('syphon mind'/'pWPN', 'Jeff Easley').
card_number('syphon mind'/'pWPN', '40').
card_flavor_text('syphon mind'/'pWPN', 'When tempers run high, it\'s easy to lose your head.').

card_in_set('tormented soul', 'pWPN').
card_original_type('tormented soul'/'pWPN', 'Creature — Spirit').
card_original_text('tormented soul'/'pWPN', '').
card_first_print('tormented soul', 'pWPN').
card_image_name('tormented soul'/'pWPN', 'tormented soul').
card_uid('tormented soul'/'pWPN', 'pWPN:Tormented Soul:tormented soul').
card_rarity('tormented soul'/'pWPN', 'Special').
card_artist('tormented soul'/'pWPN', 'John Stanko').
card_number('tormented soul'/'pWPN', '76').
card_flavor_text('tormented soul'/'pWPN', 'Those who raged most bitterly at the world in life are cursed to roam the nether realms in death.').

card_in_set('vampire nighthawk', 'pWPN').
card_original_type('vampire nighthawk'/'pWPN', 'Creature — Vampire Shaman').
card_original_text('vampire nighthawk'/'pWPN', '').
card_first_print('vampire nighthawk', 'pWPN').
card_image_name('vampire nighthawk'/'pWPN', 'vampire nighthawk').
card_uid('vampire nighthawk'/'pWPN', 'pWPN:Vampire Nighthawk:vampire nighthawk').
card_rarity('vampire nighthawk'/'pWPN', 'Special').
card_artist('vampire nighthawk'/'pWPN', 'Karl Kopinski').
card_number('vampire nighthawk'/'pWPN', '33').

card_in_set('vault skirge', 'pWPN').
card_original_type('vault skirge'/'pWPN', 'Artifact Creature — Imp').
card_original_text('vault skirge'/'pWPN', '').
card_first_print('vault skirge', 'pWPN').
card_image_name('vault skirge'/'pWPN', 'vault skirge').
card_uid('vault skirge'/'pWPN', 'pWPN:Vault Skirge:vault skirge').
card_rarity('vault skirge'/'pWPN', 'Special').
card_artist('vault skirge'/'pWPN', 'Lars Grant-West').
card_number('vault skirge'/'pWPN', '71').
card_flavor_text('vault skirge'/'pWPN', 'From the remnants of the dead, Geth forged a swarm to safeguard his throne.').

card_in_set('woolly thoctar', 'pWPN').
card_original_type('woolly thoctar'/'pWPN', 'Creature — Beast').
card_original_text('woolly thoctar'/'pWPN', '').
card_first_print('woolly thoctar', 'pWPN').
card_image_name('woolly thoctar'/'pWPN', 'woolly thoctar').
card_uid('woolly thoctar'/'pWPN', 'pWPN:Woolly Thoctar:woolly thoctar').
card_rarity('woolly thoctar'/'pWPN', 'Special').
card_artist('woolly thoctar'/'pWPN', 'Steve Argyle').
card_number('woolly thoctar'/'pWPN', '22').
card_flavor_text('woolly thoctar'/'pWPN', 'One of the most ferocious and deadly gargantuans, the thoctar never sees its worshippers, but it often awakens surrounded by gifts and sacrifices.').
