% Innistrad

set('ISD').
set_name('ISD', 'Innistrad').
set_release_date('ISD', '2011-09-30').
set_border('ISD', 'black').
set_type('ISD', 'expansion').
set_block('ISD', 'Innistrad').

card_in_set('abattoir ghoul', 'ISD').
card_original_type('abattoir ghoul'/'ISD', 'Creature — Zombie').
card_original_text('abattoir ghoul'/'ISD', 'First strike\nWhenever a creature dealt damage by Abattoir Ghoul this turn dies, you gain life equal to that creature\'s toughness.').
card_first_print('abattoir ghoul', 'ISD').
card_image_name('abattoir ghoul'/'ISD', 'abattoir ghoul').
card_uid('abattoir ghoul'/'ISD', 'ISD:Abattoir Ghoul:abattoir ghoul').
card_rarity('abattoir ghoul'/'ISD', 'Uncommon').
card_artist('abattoir ghoul'/'ISD', 'Volkan Baga').
card_number('abattoir ghoul'/'ISD', '85').
card_flavor_text('abattoir ghoul'/'ISD', 'Death took his humanity but not his skill with the knife.').
card_multiverse_id('abattoir ghoul'/'ISD', '222911').

card_in_set('abbey griffin', 'ISD').
card_original_type('abbey griffin'/'ISD', 'Creature — Griffin').
card_original_text('abbey griffin'/'ISD', 'Flying, vigilance').
card_first_print('abbey griffin', 'ISD').
card_image_name('abbey griffin'/'ISD', 'abbey griffin').
card_uid('abbey griffin'/'ISD', 'ISD:Abbey Griffin:abbey griffin').
card_rarity('abbey griffin'/'ISD', 'Common').
card_artist('abbey griffin'/'ISD', 'Jaime Jones').
card_number('abbey griffin'/'ISD', '1').
card_flavor_text('abbey griffin'/'ISD', '\"The darkness crawls with vampires and ghouls, but we are not without allies.\"\n—Mikaeus, the Lunarch').
card_multiverse_id('abbey griffin'/'ISD', '235595').

card_in_set('altar\'s reap', 'ISD').
card_original_type('altar\'s reap'/'ISD', 'Instant').
card_original_text('altar\'s reap'/'ISD', 'As an additional cost to cast Altar\'s Reap, sacrifice a creature.\nDraw two cards.').
card_first_print('altar\'s reap', 'ISD').
card_image_name('altar\'s reap'/'ISD', 'altar\'s reap').
card_uid('altar\'s reap'/'ISD', 'ISD:Altar\'s Reap:altar\'s reap').
card_rarity('altar\'s reap'/'ISD', 'Common').
card_artist('altar\'s reap'/'ISD', 'Donato Giancola').
card_number('altar\'s reap'/'ISD', '86').
card_flavor_text('altar\'s reap'/'ISD', '\"The demons don\'t care if we mutter niceties and act out ceremonies. Just kill him.\"\n—Bishop Volpaig, servant of Griselbrand').
card_multiverse_id('altar\'s reap'/'ISD', '226890').

card_in_set('ambush viper', 'ISD').
card_original_type('ambush viper'/'ISD', 'Creature — Snake').
card_original_text('ambush viper'/'ISD', 'Flash (You may cast this spell any time you could cast an instant.)\nDeathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_first_print('ambush viper', 'ISD').
card_image_name('ambush viper'/'ISD', 'ambush viper').
card_uid('ambush viper'/'ISD', 'ISD:Ambush Viper:ambush viper').
card_rarity('ambush viper'/'ISD', 'Common').
card_artist('ambush viper'/'ISD', 'Alan Pollack').
card_number('ambush viper'/'ISD', '169').
card_flavor_text('ambush viper'/'ISD', '\"Living creatures seldom impress me. But I like this one.\"\n—Uta Falkenrath').
card_multiverse_id('ambush viper'/'ISD', '220644').

card_in_set('ancient grudge', 'ISD').
card_original_type('ancient grudge'/'ISD', 'Instant').
card_original_text('ancient grudge'/'ISD', 'Destroy target artifact.\nFlashback {G} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('ancient grudge'/'ISD', 'ancient grudge').
card_uid('ancient grudge'/'ISD', 'ISD:Ancient Grudge:ancient grudge').
card_rarity('ancient grudge'/'ISD', 'Common').
card_artist('ancient grudge'/'ISD', 'Ryan Yee').
card_number('ancient grudge'/'ISD', '127').
card_flavor_text('ancient grudge'/'ISD', 'If there\'s anything a werewolf hates, it\'s a collar—especially Avacyn\'s Collar, the symbol of her church.').
card_multiverse_id('ancient grudge'/'ISD', '235600').

card_in_set('angel of flight alabaster', 'ISD').
card_original_type('angel of flight alabaster'/'ISD', 'Creature — Angel').
card_original_text('angel of flight alabaster'/'ISD', 'Flying\nAt the beginning of your upkeep, return target Spirit card from your graveyard to your hand.').
card_first_print('angel of flight alabaster', 'ISD').
card_image_name('angel of flight alabaster'/'ISD', 'angel of flight alabaster').
card_uid('angel of flight alabaster'/'ISD', 'ISD:Angel of Flight Alabaster:angel of flight alabaster').
card_rarity('angel of flight alabaster'/'ISD', 'Rare').
card_artist('angel of flight alabaster'/'ISD', 'Howard Lyon').
card_number('angel of flight alabaster'/'ISD', '2').
card_flavor_text('angel of flight alabaster'/'ISD', 'She endures without Avacyn but secretly asks each soul she guides if it has seen her.').
card_multiverse_id('angel of flight alabaster'/'ISD', '230620').

card_in_set('angelic overseer', 'ISD').
card_original_type('angelic overseer'/'ISD', 'Creature — Angel').
card_original_text('angelic overseer'/'ISD', 'Flying\nAs long as you control a Human, Angelic Overseer has hexproof and is indestructible.').
card_first_print('angelic overseer', 'ISD').
card_image_name('angelic overseer'/'ISD', 'angelic overseer').
card_uid('angelic overseer'/'ISD', 'ISD:Angelic Overseer:angelic overseer').
card_rarity('angelic overseer'/'ISD', 'Mythic Rare').
card_artist('angelic overseer'/'ISD', 'Jason Chan').
card_number('angelic overseer'/'ISD', '3').
card_flavor_text('angelic overseer'/'ISD', 'From mortal hope immortal power springs.').
card_multiverse_id('angelic overseer'/'ISD', '220370').

card_in_set('armored skaab', 'ISD').
card_original_type('armored skaab'/'ISD', 'Creature — Zombie Warrior').
card_original_text('armored skaab'/'ISD', 'When Armored Skaab enters the battlefield, put the top four cards of your library into your graveyard.').
card_first_print('armored skaab', 'ISD').
card_image_name('armored skaab'/'ISD', 'armored skaab').
card_uid('armored skaab'/'ISD', 'ISD:Armored Skaab:armored skaab').
card_rarity('armored skaab'/'ISD', 'Common').
card_artist('armored skaab'/'ISD', 'Volkan Baga').
card_number('armored skaab'/'ISD', '43').
card_flavor_text('armored skaab'/'ISD', 'Literally made for battle.').
card_multiverse_id('armored skaab'/'ISD', '244685').

card_in_set('army of the damned', 'ISD').
card_original_type('army of the damned'/'ISD', 'Sorcery').
card_original_text('army of the damned'/'ISD', 'Put thirteen 2/2 black Zombie creature tokens onto the battlefield tapped.\nFlashback {7}{B}{B}{B} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('army of the damned', 'ISD').
card_image_name('army of the damned'/'ISD', 'army of the damned').
card_uid('army of the damned'/'ISD', 'ISD:Army of the Damned:army of the damned').
card_rarity('army of the damned'/'ISD', 'Mythic Rare').
card_artist('army of the damned'/'ISD', 'Ryan Pancoast').
card_number('army of the damned'/'ISD', '87').
card_flavor_text('army of the damned'/'ISD', 'Sometimes death comes knocking. Sometimes it tears down the walls.').
card_multiverse_id('army of the damned'/'ISD', '229968').

card_in_set('ashmouth hound', 'ISD').
card_original_type('ashmouth hound'/'ISD', 'Creature — Elemental Hound').
card_original_text('ashmouth hound'/'ISD', 'Whenever Ashmouth Hound blocks or becomes blocked by a creature, Ashmouth Hound deals 1 damage to that creature.').
card_first_print('ashmouth hound', 'ISD').
card_image_name('ashmouth hound'/'ISD', 'ashmouth hound').
card_uid('ashmouth hound'/'ISD', 'ISD:Ashmouth Hound:ashmouth hound').
card_rarity('ashmouth hound'/'ISD', 'Common').
card_artist('ashmouth hound'/'ISD', 'Daarken').
card_number('ashmouth hound'/'ISD', '128').
card_flavor_text('ashmouth hound'/'ISD', 'Its fiery paws make it easy to track, which is very useful when you want to go in exactly the opposite direction.').
card_multiverse_id('ashmouth hound'/'ISD', '233255').

card_in_set('avacyn\'s pilgrim', 'ISD').
card_original_type('avacyn\'s pilgrim'/'ISD', 'Creature — Human Monk').
card_original_text('avacyn\'s pilgrim'/'ISD', '{T}: Add {W} to your mana pool.').
card_image_name('avacyn\'s pilgrim'/'ISD', 'avacyn\'s pilgrim').
card_uid('avacyn\'s pilgrim'/'ISD', 'ISD:Avacyn\'s Pilgrim:avacyn\'s pilgrim').
card_rarity('avacyn\'s pilgrim'/'ISD', 'Common').
card_artist('avacyn\'s pilgrim'/'ISD', 'Jana Schirmer & Johannes Voss').
card_number('avacyn\'s pilgrim'/'ISD', '170').
card_flavor_text('avacyn\'s pilgrim'/'ISD', '\"Avacyn\'s protection is everywhere. From the holy church to the sacred glade, all that we see is under her blessed watch.\"').
card_multiverse_id('avacyn\'s pilgrim'/'ISD', '243212').

card_in_set('avacynian priest', 'ISD').
card_original_type('avacynian priest'/'ISD', 'Creature — Human Cleric').
card_original_text('avacynian priest'/'ISD', '{1}, {T}: Tap target non-Human creature.').
card_first_print('avacynian priest', 'ISD').
card_image_name('avacynian priest'/'ISD', 'avacynian priest').
card_uid('avacynian priest'/'ISD', 'ISD:Avacynian Priest:avacynian priest').
card_rarity('avacynian priest'/'ISD', 'Common').
card_artist('avacynian priest'/'ISD', 'Greg Staples').
card_number('avacynian priest'/'ISD', '4').
card_flavor_text('avacynian priest'/'ISD', 'Moorlanders speak in awe of the priests\' talent for arriving just in time to drive off the wicked things that prowl the ravished parish.').
card_multiverse_id('avacynian priest'/'ISD', '220380').

card_in_set('back from the brink', 'ISD').
card_original_type('back from the brink'/'ISD', 'Enchantment').
card_original_text('back from the brink'/'ISD', 'Exile a creature card from your graveyard and pay its mana cost: Put a token onto the battlefield that\'s a copy of that card. Activate this ability only any time you could cast a sorcery.').
card_first_print('back from the brink', 'ISD').
card_image_name('back from the brink'/'ISD', 'back from the brink').
card_uid('back from the brink'/'ISD', 'ISD:Back from the Brink:back from the brink').
card_rarity('back from the brink'/'ISD', 'Rare').
card_artist('back from the brink'/'ISD', 'Anthony Palumbo').
card_number('back from the brink'/'ISD', '44').
card_flavor_text('back from the brink'/'ISD', 'On Innistrad, death is just a career change.').
card_multiverse_id('back from the brink'/'ISD', '249663').

card_in_set('balefire dragon', 'ISD').
card_original_type('balefire dragon'/'ISD', 'Creature — Dragon').
card_original_text('balefire dragon'/'ISD', 'Flying\nWhenever Balefire Dragon deals combat damage to a player, it deals that much damage to each creature that player controls.').
card_first_print('balefire dragon', 'ISD').
card_image_name('balefire dragon'/'ISD', 'balefire dragon').
card_uid('balefire dragon'/'ISD', 'ISD:Balefire Dragon:balefire dragon').
card_rarity('balefire dragon'/'ISD', 'Mythic Rare').
card_artist('balefire dragon'/'ISD', 'Eric Deschamps').
card_number('balefire dragon'/'ISD', '129').
card_flavor_text('balefire dragon'/'ISD', 'If it comes for you, die boldly or die swiftly—for die you will.').
card_multiverse_id('balefire dragon'/'ISD', '230774').

card_in_set('bane of hanweir', 'ISD').
card_original_type('bane of hanweir'/'ISD', 'Creature — Werewolf').
card_original_text('bane of hanweir'/'ISD', 'Bane of Hanweir attacks each turn if able.\nAt the beginning of each upkeep, if a player cast two or more spells last turn, transform Bane of Hanweir.').
card_first_print('bane of hanweir', 'ISD').
card_image_name('bane of hanweir'/'ISD', 'bane of hanweir').
card_uid('bane of hanweir'/'ISD', 'ISD:Bane of Hanweir:bane of hanweir').
card_rarity('bane of hanweir'/'ISD', 'Uncommon').
card_artist('bane of hanweir'/'ISD', 'Wayne Reynolds').
card_number('bane of hanweir'/'ISD', '145b').
card_flavor_text('bane of hanweir'/'ISD', 'Technically he never left his post. He looks after the wolf wherever it goes.').
card_multiverse_id('bane of hanweir'/'ISD', '244687').

card_in_set('battleground geist', 'ISD').
card_original_type('battleground geist'/'ISD', 'Creature — Spirit').
card_original_text('battleground geist'/'ISD', 'Flying\nOther Spirit creatures you control get +1/+0.').
card_first_print('battleground geist', 'ISD').
card_image_name('battleground geist'/'ISD', 'battleground geist').
card_uid('battleground geist'/'ISD', 'ISD:Battleground Geist:battleground geist').
card_rarity('battleground geist'/'ISD', 'Uncommon').
card_artist('battleground geist'/'ISD', 'Clint Cearley').
card_number('battleground geist'/'ISD', '45').
card_flavor_text('battleground geist'/'ISD', 'Not content to nudge vases and chill drawing rooms, some geists muster spectral armies to claw at the hearts of the living.').
card_multiverse_id('battleground geist'/'ISD', '237355').

card_in_set('bitterheart witch', 'ISD').
card_original_type('bitterheart witch'/'ISD', 'Creature — Human Shaman').
card_original_text('bitterheart witch'/'ISD', 'Deathtouch\nWhen Bitterheart Witch dies, you may search your library for a Curse card, put it onto the battlefield attached to target player, then shuffle your library.').
card_first_print('bitterheart witch', 'ISD').
card_image_name('bitterheart witch'/'ISD', 'bitterheart witch').
card_uid('bitterheart witch'/'ISD', 'ISD:Bitterheart Witch:bitterheart witch').
card_rarity('bitterheart witch'/'ISD', 'Uncommon').
card_artist('bitterheart witch'/'ISD', 'Karl Kopinski').
card_number('bitterheart witch'/'ISD', '88').
card_multiverse_id('bitterheart witch'/'ISD', '222205').

card_in_set('blasphemous act', 'ISD').
card_original_type('blasphemous act'/'ISD', 'Sorcery').
card_original_text('blasphemous act'/'ISD', 'Blasphemous Act costs {1} less to cast for each creature on the battlefield.\nBlasphemous Act deals 13 damage to each creature.').
card_first_print('blasphemous act', 'ISD').
card_image_name('blasphemous act'/'ISD', 'blasphemous act').
card_uid('blasphemous act'/'ISD', 'ISD:Blasphemous Act:blasphemous act').
card_rarity('blasphemous act'/'ISD', 'Rare').
card_artist('blasphemous act'/'ISD', 'Daarken').
card_number('blasphemous act'/'ISD', '130').
card_flavor_text('blasphemous act'/'ISD', '\"Holy places are no longer sanctuary from death, and death is no longer sanctuary from anything.\"\n—Thalia, Knight-Cathar').
card_multiverse_id('blasphemous act'/'ISD', '222206').

card_in_set('blazing torch', 'ISD').
card_original_type('blazing torch'/'ISD', 'Artifact — Equipment').
card_original_text('blazing torch'/'ISD', 'Equipped creature can\'t be blocked by Vampires or Zombies.\nEquipped creature has \"{T}, Sacrifice Blazing Torch: Blazing Torch deals 2 damage to target creature or player.\"\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_image_name('blazing torch'/'ISD', 'blazing torch').
card_uid('blazing torch'/'ISD', 'ISD:Blazing Torch:blazing torch').
card_rarity('blazing torch'/'ISD', 'Common').
card_artist('blazing torch'/'ISD', 'Scott Chou').
card_number('blazing torch'/'ISD', '216').
card_multiverse_id('blazing torch'/'ISD', '221284').

card_in_set('bloodcrazed neonate', 'ISD').
card_original_type('bloodcrazed neonate'/'ISD', 'Creature — Vampire').
card_original_text('bloodcrazed neonate'/'ISD', 'Bloodcrazed Neonate attacks each turn if able.\nWhenever Bloodcrazed Neonate deals combat damage to a player, put a +1/+1 counter on it.').
card_image_name('bloodcrazed neonate'/'ISD', 'bloodcrazed neonate').
card_uid('bloodcrazed neonate'/'ISD', 'ISD:Bloodcrazed Neonate:bloodcrazed neonate').
card_rarity('bloodcrazed neonate'/'ISD', 'Common').
card_artist('bloodcrazed neonate'/'ISD', 'Cynthia Sheppard').
card_number('bloodcrazed neonate'/'ISD', '131').
card_flavor_text('bloodcrazed neonate'/'ISD', 'While elder vampires select their meals with care, the newly sired frenzy at the first whiff.').
card_multiverse_id('bloodcrazed neonate'/'ISD', '220373').

card_in_set('bloodgift demon', 'ISD').
card_original_type('bloodgift demon'/'ISD', 'Creature — Demon').
card_original_text('bloodgift demon'/'ISD', 'Flying\nAt the beginning of your upkeep, target player draws a card and loses 1 life.').
card_first_print('bloodgift demon', 'ISD').
card_image_name('bloodgift demon'/'ISD', 'bloodgift demon').
card_uid('bloodgift demon'/'ISD', 'ISD:Bloodgift Demon:bloodgift demon').
card_rarity('bloodgift demon'/'ISD', 'Rare').
card_artist('bloodgift demon'/'ISD', 'Peter Mohrbacher').
card_number('bloodgift demon'/'ISD', '89').
card_flavor_text('bloodgift demon'/'ISD', 'He relishes the devotion of his Skirsdag puppets and their belief that it will earn them immortality.').
card_multiverse_id('bloodgift demon'/'ISD', '226885').

card_in_set('bloodline keeper', 'ISD').
card_original_type('bloodline keeper'/'ISD', 'Creature — Vampire').
card_original_text('bloodline keeper'/'ISD', 'Flying\n{T}: Put a 2/2 black Vampire creature token with flying onto the battlefield.\n{B}: Transform Bloodline Keeper. Activate this ability only if you control five or more Vampires.').
card_first_print('bloodline keeper', 'ISD').
card_image_name('bloodline keeper'/'ISD', 'bloodline keeper').
card_uid('bloodline keeper'/'ISD', 'ISD:Bloodline Keeper:bloodline keeper').
card_rarity('bloodline keeper'/'ISD', 'Rare').
card_artist('bloodline keeper'/'ISD', 'Jason Chan').
card_number('bloodline keeper'/'ISD', '90a').
card_multiverse_id('bloodline keeper'/'ISD', '227061').

card_in_set('bonds of faith', 'ISD').
card_original_type('bonds of faith'/'ISD', 'Enchantment — Aura').
card_original_text('bonds of faith'/'ISD', 'Enchant creature\nEnchanted creature gets +2/+2 as long as it\'s a Human. Otherwise, it can\'t attack or block.').
card_first_print('bonds of faith', 'ISD').
card_image_name('bonds of faith'/'ISD', 'bonds of faith').
card_uid('bonds of faith'/'ISD', 'ISD:Bonds of Faith:bonds of faith').
card_rarity('bonds of faith'/'ISD', 'Common').
card_artist('bonds of faith'/'ISD', 'Steve Argyle').
card_number('bonds of faith'/'ISD', '5').
card_flavor_text('bonds of faith'/'ISD', '\"What cannot be destroyed will be bound.\"\n—Oath of Avacyn').
card_multiverse_id('bonds of faith'/'ISD', '221216').

card_in_set('boneyard wurm', 'ISD').
card_original_type('boneyard wurm'/'ISD', 'Creature — Wurm').
card_original_text('boneyard wurm'/'ISD', 'Boneyard Wurm\'s power and toughness are each equal to the number of creature cards in your graveyard.').
card_image_name('boneyard wurm'/'ISD', 'boneyard wurm').
card_uid('boneyard wurm'/'ISD', 'ISD:Boneyard Wurm:boneyard wurm').
card_rarity('boneyard wurm'/'ISD', 'Uncommon').
card_artist('boneyard wurm'/'ISD', 'Jaime Jones').
card_number('boneyard wurm'/'ISD', '171').
card_flavor_text('boneyard wurm'/'ISD', 'The only thing it likes more than discovering a pit of bones is adding to it.').
card_multiverse_id('boneyard wurm'/'ISD', '220050').

card_in_set('brain weevil', 'ISD').
card_original_type('brain weevil'/'ISD', 'Creature — Insect').
card_original_text('brain weevil'/'ISD', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)\nSacrifice Brain Weevil: Target player discards two cards. Activate this ability only any time you could cast a sorcery.').
card_first_print('brain weevil', 'ISD').
card_image_name('brain weevil'/'ISD', 'brain weevil').
card_uid('brain weevil'/'ISD', 'ISD:Brain Weevil:brain weevil').
card_rarity('brain weevil'/'ISD', 'Common').
card_artist('brain weevil'/'ISD', 'Anthony Jones').
card_number('brain weevil'/'ISD', '91').
card_multiverse_id('brain weevil'/'ISD', '235596').

card_in_set('bramblecrush', 'ISD').
card_original_type('bramblecrush'/'ISD', 'Sorcery').
card_original_text('bramblecrush'/'ISD', 'Destroy target noncreature permanent.').
card_first_print('bramblecrush', 'ISD').
card_image_name('bramblecrush'/'ISD', 'bramblecrush').
card_uid('bramblecrush'/'ISD', 'ISD:Bramblecrush:bramblecrush').
card_rarity('bramblecrush'/'ISD', 'Uncommon').
card_artist('bramblecrush'/'ISD', 'Drew Baker').
card_number('bramblecrush'/'ISD', '172').
card_flavor_text('bramblecrush'/'ISD', '\"Civilization is fertilizer.\"\n—Garruk Wildspeaker').
card_multiverse_id('bramblecrush'/'ISD', '227095').

card_in_set('brimstone volley', 'ISD').
card_original_type('brimstone volley'/'ISD', 'Instant').
card_original_text('brimstone volley'/'ISD', 'Brimstone Volley deals 3 damage to target creature or player.\nMorbid — Brimstone Volley deals 5 damage to that creature or player instead if a creature died this turn.').
card_first_print('brimstone volley', 'ISD').
card_image_name('brimstone volley'/'ISD', 'brimstone volley').
card_uid('brimstone volley'/'ISD', 'ISD:Brimstone Volley:brimstone volley').
card_rarity('brimstone volley'/'ISD', 'Common').
card_artist('brimstone volley'/'ISD', 'Eytan Zana').
card_number('brimstone volley'/'ISD', '132').
card_multiverse_id('brimstone volley'/'ISD', '233253').

card_in_set('bump in the night', 'ISD').
card_original_type('bump in the night'/'ISD', 'Sorcery').
card_original_text('bump in the night'/'ISD', 'Target opponent loses 3 life.\nFlashback {5}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('bump in the night', 'ISD').
card_image_name('bump in the night'/'ISD', 'bump in the night').
card_uid('bump in the night'/'ISD', 'ISD:Bump in the Night:bump in the night').
card_rarity('bump in the night'/'ISD', 'Common').
card_artist('bump in the night'/'ISD', 'Kev Walker').
card_number('bump in the night'/'ISD', '92').
card_flavor_text('bump in the night'/'ISD', 'It\'s not just the wind. It\'s not all in your head. And it\'s definitely something to worry about.').
card_multiverse_id('bump in the night'/'ISD', '222203').

card_in_set('burning vengeance', 'ISD').
card_original_type('burning vengeance'/'ISD', 'Enchantment').
card_original_text('burning vengeance'/'ISD', 'Whenever you cast a spell from your graveyard, Burning Vengeance deals 2 damage to target creature or player.').
card_first_print('burning vengeance', 'ISD').
card_image_name('burning vengeance'/'ISD', 'burning vengeance').
card_uid('burning vengeance'/'ISD', 'ISD:Burning Vengeance:burning vengeance').
card_rarity('burning vengeance'/'ISD', 'Uncommon').
card_artist('burning vengeance'/'ISD', 'Raymond Swanland').
card_number('burning vengeance'/'ISD', '133').
card_flavor_text('burning vengeance'/'ISD', 'Mist is the geists\' sorrow. Wind is their pain. Fire is their vengeance.').
card_multiverse_id('burning vengeance'/'ISD', '243211').

card_in_set('butcher\'s cleaver', 'ISD').
card_original_type('butcher\'s cleaver'/'ISD', 'Artifact — Equipment').
card_original_text('butcher\'s cleaver'/'ISD', 'Equipped creature gets +3/+0.\nAs long as equipped creature is a Human, it has lifelink.\nEquip {3}').
card_first_print('butcher\'s cleaver', 'ISD').
card_image_name('butcher\'s cleaver'/'ISD', 'butcher\'s cleaver').
card_uid('butcher\'s cleaver'/'ISD', 'ISD:Butcher\'s Cleaver:butcher\'s cleaver').
card_rarity('butcher\'s cleaver'/'ISD', 'Uncommon').
card_artist('butcher\'s cleaver'/'ISD', 'Jason Felix').
card_number('butcher\'s cleaver'/'ISD', '217').
card_flavor_text('butcher\'s cleaver'/'ISD', 'Outside the safety of Thraben, there is little distinction between tool and weapon.').
card_multiverse_id('butcher\'s cleaver'/'ISD', '220049').

card_in_set('cackling counterpart', 'ISD').
card_original_type('cackling counterpart'/'ISD', 'Instant').
card_original_text('cackling counterpart'/'ISD', 'Put a token onto the battlefield that\'s a copy of target creature you control.\nFlashback {5}{U}{U} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('cackling counterpart', 'ISD').
card_image_name('cackling counterpart'/'ISD', 'cackling counterpart').
card_uid('cackling counterpart'/'ISD', 'ISD:Cackling Counterpart:cackling counterpart').
card_rarity('cackling counterpart'/'ISD', 'Rare').
card_artist('cackling counterpart'/'ISD', 'David Rapoza').
card_number('cackling counterpart'/'ISD', '46').
card_multiverse_id('cackling counterpart'/'ISD', '245195').

card_in_set('caravan vigil', 'ISD').
card_original_type('caravan vigil'/'ISD', 'Sorcery').
card_original_text('caravan vigil'/'ISD', 'Search your library for a basic land card, reveal it, put it into your hand, then shuffle your library.\nMorbid — You may put that card onto the battlefield instead of putting it into your hand if a creature died this turn.').
card_first_print('caravan vigil', 'ISD').
card_image_name('caravan vigil'/'ISD', 'caravan vigil').
card_uid('caravan vigil'/'ISD', 'ISD:Caravan Vigil:caravan vigil').
card_rarity('caravan vigil'/'ISD', 'Common').
card_artist('caravan vigil'/'ISD', 'Drew Baker').
card_number('caravan vigil'/'ISD', '173').
card_multiverse_id('caravan vigil'/'ISD', '234444').

card_in_set('cellar door', 'ISD').
card_original_type('cellar door'/'ISD', 'Artifact').
card_original_text('cellar door'/'ISD', '{3}, {T}: Target player puts the bottom card of his or her library into his or her graveyard. If it\'s a creature card, you put a 2/2 black Zombie creature token onto the battlefield.').
card_first_print('cellar door', 'ISD').
card_image_name('cellar door'/'ISD', 'cellar door').
card_uid('cellar door'/'ISD', 'ISD:Cellar Door:cellar door').
card_rarity('cellar door'/'ISD', 'Uncommon').
card_artist('cellar door'/'ISD', 'Rob Alexander').
card_number('cellar door'/'ISD', '218').
card_flavor_text('cellar door'/'ISD', 'As if anyone needed another reason to avoid dark, dank cellars.').
card_multiverse_id('cellar door'/'ISD', '220390').

card_in_set('champion of the parish', 'ISD').
card_original_type('champion of the parish'/'ISD', 'Creature — Human Soldier').
card_original_text('champion of the parish'/'ISD', 'Whenever another Human enters the battlefield under your control, put a +1/+1 counter on Champion of the Parish.').
card_first_print('champion of the parish', 'ISD').
card_image_name('champion of the parish'/'ISD', 'champion of the parish').
card_uid('champion of the parish'/'ISD', 'ISD:Champion of the Parish:champion of the parish').
card_rarity('champion of the parish'/'ISD', 'Rare').
card_artist('champion of the parish'/'ISD', 'Svetlin Velinov').
card_number('champion of the parish'/'ISD', '6').
card_flavor_text('champion of the parish'/'ISD', '\"I stand for every cobbler, tanner, and fool in this town—and they stand for me.\"').
card_multiverse_id('champion of the parish'/'ISD', '262861').

card_in_set('chapel geist', 'ISD').
card_original_type('chapel geist'/'ISD', 'Creature — Spirit').
card_original_text('chapel geist'/'ISD', 'Flying').
card_first_print('chapel geist', 'ISD').
card_image_name('chapel geist'/'ISD', 'chapel geist').
card_uid('chapel geist'/'ISD', 'ISD:Chapel Geist:chapel geist').
card_rarity('chapel geist'/'ISD', 'Common').
card_artist('chapel geist'/'ISD', 'Peter Mohrbacher').
card_number('chapel geist'/'ISD', '7').
card_flavor_text('chapel geist'/'ISD', 'Death has bound it with chains of past regrets. Forever it searches for atonement, each kindness removing a single link.').
card_multiverse_id('chapel geist'/'ISD', '230622').

card_in_set('charmbreaker devils', 'ISD').
card_original_type('charmbreaker devils'/'ISD', 'Creature — Devil').
card_original_text('charmbreaker devils'/'ISD', 'At the beginning of your upkeep, return an instant or sorcery card at random from your graveyard to your hand.\nWhenever you cast an instant or sorcery spell, Charmbreaker Devils gets +4/+0 until end of turn.').
card_first_print('charmbreaker devils', 'ISD').
card_image_name('charmbreaker devils'/'ISD', 'charmbreaker devils').
card_uid('charmbreaker devils'/'ISD', 'ISD:Charmbreaker Devils:charmbreaker devils').
card_rarity('charmbreaker devils'/'ISD', 'Rare').
card_artist('charmbreaker devils'/'ISD', 'Dan Scott').
card_number('charmbreaker devils'/'ISD', '134').
card_multiverse_id('charmbreaker devils'/'ISD', '227668').

card_in_set('civilized scholar', 'ISD').
card_original_type('civilized scholar'/'ISD', 'Creature — Human Advisor').
card_original_text('civilized scholar'/'ISD', '{T}: Draw a card, then discard a card. If a creature card is discarded this way, untap Civilized Scholar, then transform it.').
card_first_print('civilized scholar', 'ISD').
card_image_name('civilized scholar'/'ISD', 'civilized scholar').
card_uid('civilized scholar'/'ISD', 'ISD:Civilized Scholar:civilized scholar').
card_rarity('civilized scholar'/'ISD', 'Uncommon').
card_artist('civilized scholar'/'ISD', 'Michael C. Hayes').
card_number('civilized scholar'/'ISD', '47a').
card_flavor_text('civilized scholar'/'ISD', '\"Me, angry? Of course not. Thanks to my research, I\'m above such petty emotions now.\"').
card_multiverse_id('civilized scholar'/'ISD', '221209').

card_in_set('claustrophobia', 'ISD').
card_original_type('claustrophobia'/'ISD', 'Enchantment — Aura').
card_original_text('claustrophobia'/'ISD', 'Enchant creature\nWhen Claustrophobia enters the battlefield, tap enchanted creature.\nEnchanted creature doesn\'t untap during its controller\'s untap step.').
card_first_print('claustrophobia', 'ISD').
card_image_name('claustrophobia'/'ISD', 'claustrophobia').
card_uid('claustrophobia'/'ISD', 'ISD:Claustrophobia:claustrophobia').
card_rarity('claustrophobia'/'ISD', 'Common').
card_artist('claustrophobia'/'ISD', 'Ryan Pancoast').
card_number('claustrophobia'/'ISD', '48').
card_flavor_text('claustrophobia'/'ISD', 'Six feet of earth muffled his cries.').
card_multiverse_id('claustrophobia'/'ISD', '235601').

card_in_set('clifftop retreat', 'ISD').
card_original_type('clifftop retreat'/'ISD', 'Land').
card_original_text('clifftop retreat'/'ISD', 'Clifftop Retreat enters the battlefield tapped unless you control a Mountain or a Plains.\n{T}: Add {R} or {W} to your mana pool.').
card_first_print('clifftop retreat', 'ISD').
card_image_name('clifftop retreat'/'ISD', 'clifftop retreat').
card_uid('clifftop retreat'/'ISD', 'ISD:Clifftop Retreat:clifftop retreat').
card_rarity('clifftop retreat'/'ISD', 'Rare').
card_artist('clifftop retreat'/'ISD', 'John Avon').
card_number('clifftop retreat'/'ISD', '238').
card_flavor_text('clifftop retreat'/'ISD', 'Where cathars learn to fight not only demons and vampires, but ignorance as well.').
card_multiverse_id('clifftop retreat'/'ISD', '241980').

card_in_set('cloistered youth', 'ISD').
card_original_type('cloistered youth'/'ISD', 'Creature — Human').
card_original_text('cloistered youth'/'ISD', 'At the beginning of your upkeep, you may transform Cloistered Youth.').
card_first_print('cloistered youth', 'ISD').
card_image_name('cloistered youth'/'ISD', 'cloistered youth').
card_uid('cloistered youth'/'ISD', 'ISD:Cloistered Youth:cloistered youth').
card_rarity('cloistered youth'/'ISD', 'Uncommon').
card_artist('cloistered youth'/'ISD', 'Igor Kieryluk').
card_number('cloistered youth'/'ISD', '8a').
card_flavor_text('cloistered youth'/'ISD', '\"I heard her talking in her sleep—pleading, shrieking, snarling. It was not my daughter\'s voice. That is not my daughter.\"\n—Ekatrin, elder of Hanweir').
card_multiverse_id('cloistered youth'/'ISD', '221212').

card_in_set('cobbled wings', 'ISD').
card_original_type('cobbled wings'/'ISD', 'Artifact — Equipment').
card_original_text('cobbled wings'/'ISD', 'Equipped creature has flying.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('cobbled wings', 'ISD').
card_image_name('cobbled wings'/'ISD', 'cobbled wings').
card_uid('cobbled wings'/'ISD', 'ISD:Cobbled Wings:cobbled wings').
card_rarity('cobbled wings'/'ISD', 'Common').
card_artist('cobbled wings'/'ISD', 'Matt Stewart').
card_number('cobbled wings'/'ISD', '219').
card_flavor_text('cobbled wings'/'ISD', 'For the necro-alchemist, any puzzle can be solved with intelligence and harvested body parts.').
card_multiverse_id('cobbled wings'/'ISD', '220649').

card_in_set('corpse lunge', 'ISD').
card_original_type('corpse lunge'/'ISD', 'Instant').
card_original_text('corpse lunge'/'ISD', 'As an additional cost to cast Corpse Lunge, exile a creature card from your graveyard.\nCorpse Lunge deals damage equal to the exiled card\'s power to target creature.').
card_first_print('corpse lunge', 'ISD').
card_image_name('corpse lunge'/'ISD', 'corpse lunge').
card_uid('corpse lunge'/'ISD', 'ISD:Corpse Lunge:corpse lunge').
card_rarity('corpse lunge'/'ISD', 'Common').
card_artist('corpse lunge'/'ISD', 'Christopher Moeller').
card_number('corpse lunge'/'ISD', '93').
card_multiverse_id('corpse lunge'/'ISD', '244680').

card_in_set('creeping renaissance', 'ISD').
card_original_type('creeping renaissance'/'ISD', 'Sorcery').
card_original_text('creeping renaissance'/'ISD', 'Choose a permanent type. Return all cards of the chosen type from your graveyard to your hand.\nFlashback {5}{G}{G} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('creeping renaissance', 'ISD').
card_image_name('creeping renaissance'/'ISD', 'creeping renaissance').
card_uid('creeping renaissance'/'ISD', 'ISD:Creeping Renaissance:creeping renaissance').
card_rarity('creeping renaissance'/'ISD', 'Rare').
card_artist('creeping renaissance'/'ISD', 'Tomasz Jedruszek').
card_number('creeping renaissance'/'ISD', '174').
card_multiverse_id('creeping renaissance'/'ISD', '245198').

card_in_set('creepy doll', 'ISD').
card_original_type('creepy doll'/'ISD', 'Artifact Creature — Construct').
card_original_text('creepy doll'/'ISD', 'Creepy Doll is indestructible.\nWhenever Creepy Doll deals combat damage to a creature, flip a coin. If you win the flip, destroy that creature.').
card_first_print('creepy doll', 'ISD').
card_image_name('creepy doll'/'ISD', 'creepy doll').
card_uid('creepy doll'/'ISD', 'ISD:Creepy Doll:creepy doll').
card_rarity('creepy doll'/'ISD', 'Rare').
card_artist('creepy doll'/'ISD', 'Matt Stewart').
card_number('creepy doll'/'ISD', '220').
card_flavor_text('creepy doll'/'ISD', 'A child\'s porcelain doll went missing one night, as did a pair of kitchen shears and the town magistrate.').
card_multiverse_id('creepy doll'/'ISD', '220378').

card_in_set('crossway vampire', 'ISD').
card_original_type('crossway vampire'/'ISD', 'Creature — Vampire').
card_original_text('crossway vampire'/'ISD', 'When Crossway Vampire enters the battlefield, target creature can\'t block this turn.').
card_first_print('crossway vampire', 'ISD').
card_image_name('crossway vampire'/'ISD', 'crossway vampire').
card_uid('crossway vampire'/'ISD', 'ISD:Crossway Vampire:crossway vampire').
card_rarity('crossway vampire'/'ISD', 'Common').
card_artist('crossway vampire'/'ISD', 'Mark Evans').
card_number('crossway vampire'/'ISD', '135').
card_flavor_text('crossway vampire'/'ISD', '\"Do not flatter Fate. It did not guide you into my clutches tonight. That was all my doing.\"').
card_multiverse_id('crossway vampire'/'ISD', '230779').

card_in_set('curiosity', 'ISD').
card_original_type('curiosity'/'ISD', 'Enchantment — Aura').
card_original_text('curiosity'/'ISD', 'Enchant creature\nWhenever enchanted creature deals damage to an opponent, you may draw a card.').
card_image_name('curiosity'/'ISD', 'curiosity').
card_uid('curiosity'/'ISD', 'ISD:Curiosity:curiosity').
card_rarity('curiosity'/'ISD', 'Uncommon').
card_artist('curiosity'/'ISD', 'Igor Kieryluk').
card_number('curiosity'/'ISD', '49').
card_flavor_text('curiosity'/'ISD', '\"Surely you know the joy of the hunt, my furry friend. My kind of hunt just takes a different form than yours.\"').
card_multiverse_id('curiosity'/'ISD', '230767').

card_in_set('curse of death\'s hold', 'ISD').
card_original_type('curse of death\'s hold'/'ISD', 'Enchantment — Aura Curse').
card_original_text('curse of death\'s hold'/'ISD', 'Enchant player\nCreatures enchanted player controls get -1/-1.').
card_first_print('curse of death\'s hold', 'ISD').
card_image_name('curse of death\'s hold'/'ISD', 'curse of death\'s hold').
card_uid('curse of death\'s hold'/'ISD', 'ISD:Curse of Death\'s Hold:curse of death\'s hold').
card_rarity('curse of death\'s hold'/'ISD', 'Rare').
card_artist('curse of death\'s hold'/'ISD', 'Clint Cearley').
card_number('curse of death\'s hold'/'ISD', '94').
card_flavor_text('curse of death\'s hold'/'ISD', '\"May you and all your kin waste and wither until your clan is no more!\"').
card_multiverse_id('curse of death\'s hold'/'ISD', '227075').

card_in_set('curse of oblivion', 'ISD').
card_original_type('curse of oblivion'/'ISD', 'Enchantment — Aura Curse').
card_original_text('curse of oblivion'/'ISD', 'Enchant player\nAt the beginning of enchanted player\'s upkeep, that player exiles two cards from his or her graveyard.').
card_first_print('curse of oblivion', 'ISD').
card_image_name('curse of oblivion'/'ISD', 'curse of oblivion').
card_uid('curse of oblivion'/'ISD', 'ISD:Curse of Oblivion:curse of oblivion').
card_rarity('curse of oblivion'/'ISD', 'Common').
card_artist('curse of oblivion'/'ISD', 'Jana Schirmer & Johannes Voss').
card_number('curse of oblivion'/'ISD', '95').
card_flavor_text('curse of oblivion'/'ISD', 'The first step to peace is to learn how to forget.').
card_multiverse_id('curse of oblivion'/'ISD', '237020').

card_in_set('curse of stalked prey', 'ISD').
card_original_type('curse of stalked prey'/'ISD', 'Enchantment — Aura Curse').
card_original_text('curse of stalked prey'/'ISD', 'Enchant player\nWhenever a creature deals combat damage to enchanted player, put a +1/+1 counter on that creature.').
card_first_print('curse of stalked prey', 'ISD').
card_image_name('curse of stalked prey'/'ISD', 'curse of stalked prey').
card_uid('curse of stalked prey'/'ISD', 'ISD:Curse of Stalked Prey:curse of stalked prey').
card_rarity('curse of stalked prey'/'ISD', 'Rare').
card_artist('curse of stalked prey'/'ISD', 'Christopher Moeller').
card_number('curse of stalked prey'/'ISD', '136').
card_flavor_text('curse of stalked prey'/'ISD', '\"Innocent thorns can fill the air with your bloodscent. Don\'t stray from the path.\"\n—Elmut, crossway watcher').
card_multiverse_id('curse of stalked prey'/'ISD', '226889').

card_in_set('curse of the bloody tome', 'ISD').
card_original_type('curse of the bloody tome'/'ISD', 'Enchantment — Aura Curse').
card_original_text('curse of the bloody tome'/'ISD', 'Enchant player\nAt the beginning of enchanted player\'s upkeep, that player puts the top two cards of his or her library into his or her graveyard.').
card_image_name('curse of the bloody tome'/'ISD', 'curse of the bloody tome').
card_uid('curse of the bloody tome'/'ISD', 'ISD:Curse of the Bloody Tome:curse of the bloody tome').
card_rarity('curse of the bloody tome'/'ISD', 'Common').
card_artist('curse of the bloody tome'/'ISD', 'Jaime Jones').
card_number('curse of the bloody tome'/'ISD', '50').
card_flavor_text('curse of the bloody tome'/'ISD', 'After seeing his life\'s work drip away, the mage decided it was a good time to go crazy.').
card_multiverse_id('curse of the bloody tome'/'ISD', '230627').

card_in_set('curse of the nightly hunt', 'ISD').
card_original_type('curse of the nightly hunt'/'ISD', 'Enchantment — Aura Curse').
card_original_text('curse of the nightly hunt'/'ISD', 'Enchant player\nCreatures enchanted player controls attack each turn if able.').
card_first_print('curse of the nightly hunt', 'ISD').
card_image_name('curse of the nightly hunt'/'ISD', 'curse of the nightly hunt').
card_uid('curse of the nightly hunt'/'ISD', 'ISD:Curse of the Nightly Hunt:curse of the nightly hunt').
card_rarity('curse of the nightly hunt'/'ISD', 'Uncommon').
card_artist('curse of the nightly hunt'/'ISD', 'Daarken').
card_number('curse of the nightly hunt'/'ISD', '137').
card_flavor_text('curse of the nightly hunt'/'ISD', 'When the moon rises and the bloodlust takes hold, nothing matters but the kill.').
card_multiverse_id('curse of the nightly hunt'/'ISD', '226881').

card_in_set('curse of the pierced heart', 'ISD').
card_original_type('curse of the pierced heart'/'ISD', 'Enchantment — Aura Curse').
card_original_text('curse of the pierced heart'/'ISD', 'Enchant player\nAt the beginning of enchanted player\'s upkeep, Curse of the Pierced Heart deals 1 damage to that player.').
card_first_print('curse of the pierced heart', 'ISD').
card_image_name('curse of the pierced heart'/'ISD', 'curse of the pierced heart').
card_uid('curse of the pierced heart'/'ISD', 'ISD:Curse of the Pierced Heart:curse of the pierced heart').
card_rarity('curse of the pierced heart'/'ISD', 'Common').
card_artist('curse of the pierced heart'/'ISD', 'E. M. Gist').
card_number('curse of the pierced heart'/'ISD', '138').
card_flavor_text('curse of the pierced heart'/'ISD', '\"May your veins weep, and may every vampire take notice.\"\n—Stensian curse').
card_multiverse_id('curse of the pierced heart'/'ISD', '227071').

card_in_set('darkthicket wolf', 'ISD').
card_original_type('darkthicket wolf'/'ISD', 'Creature — Wolf').
card_original_text('darkthicket wolf'/'ISD', '{2}{G}: Darkthicket Wolf gets +2/+2 until end of turn. Activate this ability only once each turn.').
card_first_print('darkthicket wolf', 'ISD').
card_image_name('darkthicket wolf'/'ISD', 'darkthicket wolf').
card_uid('darkthicket wolf'/'ISD', 'ISD:Darkthicket Wolf:darkthicket wolf').
card_rarity('darkthicket wolf'/'ISD', 'Common').
card_artist('darkthicket wolf'/'ISD', 'Wayne England').
card_number('darkthicket wolf'/'ISD', '175').
card_flavor_text('darkthicket wolf'/'ISD', 'Werewolf slayers don\'t know for certain how werewolves are created. So they kill all the wolves, just to be safe.').
card_multiverse_id('darkthicket wolf'/'ISD', '241986').

card_in_set('daybreak ranger', 'ISD').
card_original_type('daybreak ranger'/'ISD', 'Creature — Human Archer Werewolf').
card_original_text('daybreak ranger'/'ISD', '{T}: Daybreak Ranger deals 2 damage to target creature with flying.\nAt the beginning of each upkeep, if no spells were cast last turn, transform Daybreak Ranger.').
card_first_print('daybreak ranger', 'ISD').
card_image_name('daybreak ranger'/'ISD', 'daybreak ranger').
card_uid('daybreak ranger'/'ISD', 'ISD:Daybreak Ranger:daybreak ranger').
card_rarity('daybreak ranger'/'ISD', 'Rare').
card_artist('daybreak ranger'/'ISD', 'Steve Prescott').
card_number('daybreak ranger'/'ISD', '176a').
card_multiverse_id('daybreak ranger'/'ISD', '222118').

card_in_set('dead weight', 'ISD').
card_original_type('dead weight'/'ISD', 'Enchantment — Aura').
card_original_text('dead weight'/'ISD', 'Enchant creature\nEnchanted creature gets -2/-2.').
card_first_print('dead weight', 'ISD').
card_image_name('dead weight'/'ISD', 'dead weight').
card_uid('dead weight'/'ISD', 'ISD:Dead Weight:dead weight').
card_rarity('dead weight'/'ISD', 'Common').
card_artist('dead weight'/'ISD', 'Randy Gallegos').
card_number('dead weight'/'ISD', '96').
card_flavor_text('dead weight'/'ISD', 'The crueler vampires\' idea of free-range farming.').
card_multiverse_id('dead weight'/'ISD', '220393').

card_in_set('dearly departed', 'ISD').
card_original_type('dearly departed'/'ISD', 'Creature — Spirit').
card_original_text('dearly departed'/'ISD', 'Flying\nAs long as Dearly Departed is in your graveyard, each Human creature you control enters the battlefield with an additional +1/+1 counter on it.').
card_first_print('dearly departed', 'ISD').
card_image_name('dearly departed'/'ISD', 'dearly departed').
card_uid('dearly departed'/'ISD', 'ISD:Dearly Departed:dearly departed').
card_rarity('dearly departed'/'ISD', 'Rare').
card_artist('dearly departed'/'ISD', 'Daniel Ljunggren').
card_number('dearly departed'/'ISD', '9').
card_flavor_text('dearly departed'/'ISD', '\"Never forget our ancestors. They have not forgotten us.\"\n—Mikaeus, the Lunarch').
card_multiverse_id('dearly departed'/'ISD', '222194').

card_in_set('delver of secrets', 'ISD').
card_original_type('delver of secrets'/'ISD', 'Creature — Human Wizard').
card_original_text('delver of secrets'/'ISD', 'At the beginning of your upkeep, look at the top card of your library. You may reveal that card. If an instant or sorcery card is revealed this way, transform Delver of Secrets.').
card_first_print('delver of secrets', 'ISD').
card_image_name('delver of secrets'/'ISD', 'delver of secrets').
card_uid('delver of secrets'/'ISD', 'ISD:Delver of Secrets:delver of secrets').
card_rarity('delver of secrets'/'ISD', 'Common').
card_artist('delver of secrets'/'ISD', 'Nils Hamm').
card_number('delver of secrets'/'ISD', '51a').
card_multiverse_id('delver of secrets'/'ISD', '226749').

card_in_set('demonmail hauberk', 'ISD').
card_original_type('demonmail hauberk'/'ISD', 'Artifact — Equipment').
card_original_text('demonmail hauberk'/'ISD', 'Equipped creature gets +4/+2.\nEquip—Sacrifice a creature.').
card_first_print('demonmail hauberk', 'ISD').
card_image_name('demonmail hauberk'/'ISD', 'demonmail hauberk').
card_uid('demonmail hauberk'/'ISD', 'ISD:Demonmail Hauberk:demonmail hauberk').
card_rarity('demonmail hauberk'/'ISD', 'Uncommon').
card_artist('demonmail hauberk'/'ISD', 'Jason Felix').
card_number('demonmail hauberk'/'ISD', '221').
card_flavor_text('demonmail hauberk'/'ISD', 'It comes off as easily as your own skin.').
card_multiverse_id('demonmail hauberk'/'ISD', '227291').

card_in_set('deranged assistant', 'ISD').
card_original_type('deranged assistant'/'ISD', 'Creature — Human Wizard').
card_original_text('deranged assistant'/'ISD', '{T}, Put the top card of your library into your graveyard: Add {1} to your mana pool.').
card_first_print('deranged assistant', 'ISD').
card_image_name('deranged assistant'/'ISD', 'deranged assistant').
card_uid('deranged assistant'/'ISD', 'ISD:Deranged Assistant:deranged assistant').
card_rarity('deranged assistant'/'ISD', 'Common').
card_artist('deranged assistant'/'ISD', 'Nils Hamm').
card_number('deranged assistant'/'ISD', '52').
card_flavor_text('deranged assistant'/'ISD', '\"Garl, adjust the slurry dispensers. Garl, fetch more corpses. Garl, quit crying and give me your brain tissue. If he doesn\'t stop being so rude, I\'m quitting.\"').
card_multiverse_id('deranged assistant'/'ISD', '221171').

card_in_set('desperate ravings', 'ISD').
card_original_type('desperate ravings'/'ISD', 'Instant').
card_original_text('desperate ravings'/'ISD', 'Draw two cards, then discard a card at random.\nFlashback {2}{U} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('desperate ravings', 'ISD').
card_image_name('desperate ravings'/'ISD', 'desperate ravings').
card_uid('desperate ravings'/'ISD', 'ISD:Desperate Ravings:desperate ravings').
card_rarity('desperate ravings'/'ISD', 'Uncommon').
card_artist('desperate ravings'/'ISD', 'John Stanko').
card_number('desperate ravings'/'ISD', '139').
card_flavor_text('desperate ravings'/'ISD', 'Her mind was quite gone, yet she spoke nothing but truth.').
card_multiverse_id('desperate ravings'/'ISD', '237359').

card_in_set('devil\'s play', 'ISD').
card_original_type('devil\'s play'/'ISD', 'Sorcery').
card_original_text('devil\'s play'/'ISD', 'Devil\'s Play deals X damage to target creature or player.\nFlashback {X}{R}{R}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('devil\'s play'/'ISD', 'devil\'s play').
card_uid('devil\'s play'/'ISD', 'ISD:Devil\'s Play:devil\'s play').
card_rarity('devil\'s play'/'ISD', 'Rare').
card_artist('devil\'s play'/'ISD', 'Austin Hsu').
card_number('devil\'s play'/'ISD', '140').
card_flavor_text('devil\'s play'/'ISD', 'A devil\'s hands are never idle.').
card_multiverse_id('devil\'s play'/'ISD', '247419').

card_in_set('diregraf ghoul', 'ISD').
card_original_type('diregraf ghoul'/'ISD', 'Creature — Zombie').
card_original_text('diregraf ghoul'/'ISD', 'Diregraf Ghoul enters the battlefield tapped.').
card_image_name('diregraf ghoul'/'ISD', 'diregraf ghoul').
card_uid('diregraf ghoul'/'ISD', 'ISD:Diregraf Ghoul:diregraf ghoul').
card_rarity('diregraf ghoul'/'ISD', 'Uncommon').
card_artist('diregraf ghoul'/'ISD', 'Dave Kendall').
card_number('diregraf ghoul'/'ISD', '97').
card_flavor_text('diregraf ghoul'/'ISD', '\"At least this one still has arms and legs. Well, most of its legs.\"\n—Enslow, ghoulcaller of Nephalia').
card_multiverse_id('diregraf ghoul'/'ISD', '226747').

card_in_set('disciple of griselbrand', 'ISD').
card_original_type('disciple of griselbrand'/'ISD', 'Creature — Human Cleric').
card_original_text('disciple of griselbrand'/'ISD', '{1}, Sacrifice a creature: You gain life equal to the sacrificed creature\'s toughness.').
card_first_print('disciple of griselbrand', 'ISD').
card_image_name('disciple of griselbrand'/'ISD', 'disciple of griselbrand').
card_uid('disciple of griselbrand'/'ISD', 'ISD:Disciple of Griselbrand:disciple of griselbrand').
card_rarity('disciple of griselbrand'/'ISD', 'Uncommon').
card_artist('disciple of griselbrand'/'ISD', 'Clint Cearley').
card_number('disciple of griselbrand'/'ISD', '98').
card_flavor_text('disciple of griselbrand'/'ISD', 'The demon Griselbrand dared to stand against Avacyn, earning him the loathing of thousands and the admiration of a few.').
card_multiverse_id('disciple of griselbrand'/'ISD', '244679').

card_in_set('dissipate', 'ISD').
card_original_type('dissipate'/'ISD', 'Instant').
card_original_text('dissipate'/'ISD', 'Counter target spell. If that spell is countered this way, exile it instead of putting it into its owner\'s graveyard.').
card_image_name('dissipate'/'ISD', 'dissipate').
card_uid('dissipate'/'ISD', 'ISD:Dissipate:dissipate').
card_rarity('dissipate'/'ISD', 'Uncommon').
card_artist('dissipate'/'ISD', 'Tomasz Jedruszek').
card_number('dissipate'/'ISD', '53').
card_flavor_text('dissipate'/'ISD', '\"This abomination never belonged in our world. I\'m merely setting it free.\"\n—Dierk, geistmage').
card_multiverse_id('dissipate'/'ISD', '241985').

card_in_set('divine reckoning', 'ISD').
card_original_type('divine reckoning'/'ISD', 'Sorcery').
card_original_text('divine reckoning'/'ISD', 'Each player chooses a creature he or she controls. Destroy the rest.\nFlashback {5}{W}{W} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('divine reckoning', 'ISD').
card_image_name('divine reckoning'/'ISD', 'divine reckoning').
card_uid('divine reckoning'/'ISD', 'ISD:Divine Reckoning:divine reckoning').
card_rarity('divine reckoning'/'ISD', 'Rare').
card_artist('divine reckoning'/'ISD', 'Greg Staples').
card_number('divine reckoning'/'ISD', '10').
card_flavor_text('divine reckoning'/'ISD', 'Survival of the purest.').
card_multiverse_id('divine reckoning'/'ISD', '245196').

card_in_set('doomed traveler', 'ISD').
card_original_type('doomed traveler'/'ISD', 'Creature — Human Soldier').
card_original_text('doomed traveler'/'ISD', 'When Doomed Traveler dies, put a 1/1 white Spirit creature token with flying onto the battlefield.').
card_first_print('doomed traveler', 'ISD').
card_image_name('doomed traveler'/'ISD', 'doomed traveler').
card_uid('doomed traveler'/'ISD', 'ISD:Doomed Traveler:doomed traveler').
card_rarity('doomed traveler'/'ISD', 'Common').
card_artist('doomed traveler'/'ISD', 'Lars Grant-West').
card_number('doomed traveler'/'ISD', '11').
card_flavor_text('doomed traveler'/'ISD', 'He vowed he would never rest until he reached his destination. He doesn\'t know how right he was.').
card_multiverse_id('doomed traveler'/'ISD', '237364').

card_in_set('dream twist', 'ISD').
card_original_type('dream twist'/'ISD', 'Instant').
card_original_text('dream twist'/'ISD', 'Target player puts the top three cards of his or her library into his or her graveyard.\nFlashback {1}{U} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('dream twist', 'ISD').
card_image_name('dream twist'/'ISD', 'dream twist').
card_uid('dream twist'/'ISD', 'ISD:Dream Twist:dream twist').
card_rarity('dream twist'/'ISD', 'Common').
card_artist('dream twist'/'ISD', 'Dan Scott').
card_number('dream twist'/'ISD', '54').
card_multiverse_id('dream twist'/'ISD', '243213').

card_in_set('elder cathar', 'ISD').
card_original_type('elder cathar'/'ISD', 'Creature — Human Soldier').
card_original_text('elder cathar'/'ISD', 'When Elder Cathar dies, put a +1/+1 counter on target creature you control. If that creature is a Human, put two +1/+1 counters on it instead.').
card_first_print('elder cathar', 'ISD').
card_image_name('elder cathar'/'ISD', 'elder cathar').
card_uid('elder cathar'/'ISD', 'ISD:Elder Cathar:elder cathar').
card_rarity('elder cathar'/'ISD', 'Common').
card_artist('elder cathar'/'ISD', 'Chris Rahn').
card_number('elder cathar'/'ISD', '12').
card_flavor_text('elder cathar'/'ISD', '\"My greatest hope is that you will surpass me in every way, consigning my name to some forgotten corner of history.\"').
card_multiverse_id('elder cathar'/'ISD', '222910').

card_in_set('elder of laurels', 'ISD').
card_original_type('elder of laurels'/'ISD', 'Creature — Human Advisor').
card_original_text('elder of laurels'/'ISD', '{3}{G}: Target creature gets +X/+X until end of turn, where X is the number of creatures you control.').
card_first_print('elder of laurels', 'ISD').
card_image_name('elder of laurels'/'ISD', 'elder of laurels').
card_uid('elder of laurels'/'ISD', 'ISD:Elder of Laurels:elder of laurels').
card_rarity('elder of laurels'/'ISD', 'Rare').
card_artist('elder of laurels'/'ISD', 'Terese Nielsen').
card_number('elder of laurels'/'ISD', '177').
card_flavor_text('elder of laurels'/'ISD', 'Hopes and prayers can carry the same force as swords and torches.').
card_multiverse_id('elder of laurels'/'ISD', '247418').

card_in_set('elite inquisitor', 'ISD').
card_original_type('elite inquisitor'/'ISD', 'Creature — Human Soldier').
card_original_text('elite inquisitor'/'ISD', 'First strike, vigilance\nProtection from Vampires, from Werewolves, and from Zombies').
card_image_name('elite inquisitor'/'ISD', 'elite inquisitor').
card_uid('elite inquisitor'/'ISD', 'ISD:Elite Inquisitor:elite inquisitor').
card_rarity('elite inquisitor'/'ISD', 'Rare').
card_artist('elite inquisitor'/'ISD', 'Jana Schirmer & Johannes Voss').
card_number('elite inquisitor'/'ISD', '13').
card_flavor_text('elite inquisitor'/'ISD', '\"No matter how many I kill, there are always more to hunt. It makes me smile just to think about it.\"').
card_multiverse_id('elite inquisitor'/'ISD', '246949').

card_in_set('endless ranks of the dead', 'ISD').
card_original_type('endless ranks of the dead'/'ISD', 'Enchantment').
card_original_text('endless ranks of the dead'/'ISD', 'At the beginning of your upkeep, put X 2/2 black Zombie creature tokens onto the battlefield, where X is half the number of Zombies you control, rounded down.').
card_first_print('endless ranks of the dead', 'ISD').
card_image_name('endless ranks of the dead'/'ISD', 'endless ranks of the dead').
card_uid('endless ranks of the dead'/'ISD', 'ISD:Endless Ranks of the Dead:endless ranks of the dead').
card_rarity('endless ranks of the dead'/'ISD', 'Rare').
card_artist('endless ranks of the dead'/'ISD', 'Ryan Yee').
card_number('endless ranks of the dead'/'ISD', '99').
card_flavor_text('endless ranks of the dead'/'ISD', 'With Thraben\'s army recalled to its city walls, outlying villages were left to fend for themselves.').
card_multiverse_id('endless ranks of the dead'/'ISD', '234430').

card_in_set('essence of the wild', 'ISD').
card_original_type('essence of the wild'/'ISD', 'Creature — Avatar').
card_original_text('essence of the wild'/'ISD', 'Creatures you control enter the battlefield as a copy of Essence of the Wild.').
card_first_print('essence of the wild', 'ISD').
card_image_name('essence of the wild'/'ISD', 'essence of the wild').
card_uid('essence of the wild'/'ISD', 'ISD:Essence of the Wild:essence of the wild').
card_rarity('essence of the wild'/'ISD', 'Mythic Rare').
card_artist('essence of the wild'/'ISD', 'Terese Nielsen').
card_number('essence of the wild'/'ISD', '178').
card_flavor_text('essence of the wild'/'ISD', 'Wander too far into the wild and it may take you for its own.').
card_multiverse_id('essence of the wild'/'ISD', '254133').

card_in_set('evil twin', 'ISD').
card_original_type('evil twin'/'ISD', 'Creature — Shapeshifter').
card_original_text('evil twin'/'ISD', 'You may have Evil Twin enter the battlefield as a copy of any creature on the battlefield except it gains \"{U}{B}, {T}: Destroy target creature with the same name as this creature.\"').
card_first_print('evil twin', 'ISD').
card_image_name('evil twin'/'ISD', 'evil twin').
card_uid('evil twin'/'ISD', 'ISD:Evil Twin:evil twin').
card_rarity('evil twin'/'ISD', 'Rare').
card_artist('evil twin'/'ISD', 'Greg Staples').
card_number('evil twin'/'ISD', '212').
card_flavor_text('evil twin'/'ISD', 'You can always tell the evil one by the dagger he\'s sticking in you.').
card_multiverse_id('evil twin'/'ISD', '229965').

card_in_set('falkenrath marauders', 'ISD').
card_original_type('falkenrath marauders'/'ISD', 'Creature — Vampire Warrior').
card_original_text('falkenrath marauders'/'ISD', 'Flying, haste\nWhenever Falkenrath Marauders deals combat damage to a player, put two +1/+1 counters on it.').
card_first_print('falkenrath marauders', 'ISD').
card_image_name('falkenrath marauders'/'ISD', 'falkenrath marauders').
card_uid('falkenrath marauders'/'ISD', 'ISD:Falkenrath Marauders:falkenrath marauders').
card_rarity('falkenrath marauders'/'ISD', 'Rare').
card_artist('falkenrath marauders'/'ISD', 'James Ryman').
card_number('falkenrath marauders'/'ISD', '141').
card_flavor_text('falkenrath marauders'/'ISD', 'No slinking in the dark for them, no luring whispers. They claim their prey in the speed and glory of the hunt.').
card_multiverse_id('falkenrath marauders'/'ISD', '246954').

card_in_set('falkenrath noble', 'ISD').
card_original_type('falkenrath noble'/'ISD', 'Creature — Vampire').
card_original_text('falkenrath noble'/'ISD', 'Flying\nWhenever Falkenrath Noble or another creature dies, target player loses 1 life and you gain 1 life.').
card_first_print('falkenrath noble', 'ISD').
card_image_name('falkenrath noble'/'ISD', 'falkenrath noble').
card_uid('falkenrath noble'/'ISD', 'ISD:Falkenrath Noble:falkenrath noble').
card_rarity('falkenrath noble'/'ISD', 'Uncommon').
card_artist('falkenrath noble'/'ISD', 'Slawomir Maniak').
card_number('falkenrath noble'/'ISD', '100').
card_flavor_text('falkenrath noble'/'ISD', '\"I often think on how excited they must feel to be chosen as my prey.\"').
card_multiverse_id('falkenrath noble'/'ISD', '226751').

card_in_set('feeling of dread', 'ISD').
card_original_type('feeling of dread'/'ISD', 'Instant').
card_original_text('feeling of dread'/'ISD', 'Tap up to two target creatures.\nFlashback {1}{U} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('feeling of dread', 'ISD').
card_image_name('feeling of dread'/'ISD', 'feeling of dread').
card_uid('feeling of dread'/'ISD', 'ISD:Feeling of Dread:feeling of dread').
card_rarity('feeling of dread'/'ISD', 'Common').
card_artist('feeling of dread'/'ISD', 'John Stanko').
card_number('feeling of dread'/'ISD', '14').
card_flavor_text('feeling of dread'/'ISD', 'People only say \"I\'m sure it was nothing\" when they\'re sure it was something.').
card_multiverse_id('feeling of dread'/'ISD', '237360').

card_in_set('feral ridgewolf', 'ISD').
card_original_type('feral ridgewolf'/'ISD', 'Creature — Wolf').
card_original_text('feral ridgewolf'/'ISD', 'Trample\n{1}{R}: Feral Ridgewolf gets +2/+0 until end of turn.').
card_first_print('feral ridgewolf', 'ISD').
card_image_name('feral ridgewolf'/'ISD', 'feral ridgewolf').
card_uid('feral ridgewolf'/'ISD', 'ISD:Feral Ridgewolf:feral ridgewolf').
card_rarity('feral ridgewolf'/'ISD', 'Common').
card_artist('feral ridgewolf'/'ISD', 'Martina Pilcerova').
card_number('feral ridgewolf'/'ISD', '142').
card_flavor_text('feral ridgewolf'/'ISD', '\"The wolves no longer hunt just to feed. They kill for sport, or for some madness behind their eyes.\"\n—Elmut, crossway watcher').
card_multiverse_id('feral ridgewolf'/'ISD', '220022').

card_in_set('festerhide boar', 'ISD').
card_original_type('festerhide boar'/'ISD', 'Creature — Boar').
card_original_text('festerhide boar'/'ISD', 'Trample\nMorbid — Festerhide Boar enters the battlefield with two +1/+1 counters on it if a creature died this turn.').
card_first_print('festerhide boar', 'ISD').
card_image_name('festerhide boar'/'ISD', 'festerhide boar').
card_uid('festerhide boar'/'ISD', 'ISD:Festerhide Boar:festerhide boar').
card_rarity('festerhide boar'/'ISD', 'Common').
card_artist('festerhide boar'/'ISD', 'Nils Hamm').
card_number('festerhide boar'/'ISD', '179').
card_flavor_text('festerhide boar'/'ISD', '\"Bury your dead deep. The boars are hungriest while the corpse is still warm.\"\n—Paulin, trapper of Somberwald').
card_multiverse_id('festerhide boar'/'ISD', '233250').

card_in_set('fiend hunter', 'ISD').
card_original_type('fiend hunter'/'ISD', 'Creature — Human Cleric').
card_original_text('fiend hunter'/'ISD', 'When Fiend Hunter enters the battlefield, you may exile another target creature.\nWhen Fiend Hunter leaves the battlefield, return the exiled card to the battlefield under its owner\'s control.').
card_first_print('fiend hunter', 'ISD').
card_image_name('fiend hunter'/'ISD', 'fiend hunter').
card_uid('fiend hunter'/'ISD', 'ISD:Fiend Hunter:fiend hunter').
card_rarity('fiend hunter'/'ISD', 'Uncommon').
card_artist('fiend hunter'/'ISD', 'Wayne Reynolds').
card_number('fiend hunter'/'ISD', '15').
card_multiverse_id('fiend hunter'/'ISD', '222007').

card_in_set('forbidden alchemy', 'ISD').
card_original_type('forbidden alchemy'/'ISD', 'Instant').
card_original_text('forbidden alchemy'/'ISD', 'Look at the top four cards of your library. Put one of them into your hand and the rest into your graveyard.\nFlashback {6}{B} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('forbidden alchemy'/'ISD', 'forbidden alchemy').
card_uid('forbidden alchemy'/'ISD', 'ISD:Forbidden Alchemy:forbidden alchemy').
card_rarity('forbidden alchemy'/'ISD', 'Common').
card_artist('forbidden alchemy'/'ISD', 'David Rapoza').
card_number('forbidden alchemy'/'ISD', '55').
card_multiverse_id('forbidden alchemy'/'ISD', '226758').

card_in_set('forest', 'ISD').
card_original_type('forest'/'ISD', 'Basic Land — Forest').
card_original_text('forest'/'ISD', 'G').
card_image_name('forest'/'ISD', 'forest1').
card_uid('forest'/'ISD', 'ISD:Forest:forest1').
card_rarity('forest'/'ISD', 'Basic Land').
card_artist('forest'/'ISD', 'James Paick').
card_number('forest'/'ISD', '262').
card_multiverse_id('forest'/'ISD', '245247').

card_in_set('forest', 'ISD').
card_original_type('forest'/'ISD', 'Basic Land — Forest').
card_original_text('forest'/'ISD', 'G').
card_image_name('forest'/'ISD', 'forest2').
card_uid('forest'/'ISD', 'ISD:Forest:forest2').
card_rarity('forest'/'ISD', 'Basic Land').
card_artist('forest'/'ISD', 'Jung Park').
card_number('forest'/'ISD', '263').
card_multiverse_id('forest'/'ISD', '245248').

card_in_set('forest', 'ISD').
card_original_type('forest'/'ISD', 'Basic Land — Forest').
card_original_text('forest'/'ISD', 'G').
card_image_name('forest'/'ISD', 'forest3').
card_uid('forest'/'ISD', 'ISD:Forest:forest3').
card_rarity('forest'/'ISD', 'Basic Land').
card_artist('forest'/'ISD', 'Eytan Zana').
card_number('forest'/'ISD', '264').
card_multiverse_id('forest'/'ISD', '245246').

card_in_set('fortress crab', 'ISD').
card_original_type('fortress crab'/'ISD', 'Creature — Crab').
card_original_text('fortress crab'/'ISD', '').
card_first_print('fortress crab', 'ISD').
card_image_name('fortress crab'/'ISD', 'fortress crab').
card_uid('fortress crab'/'ISD', 'ISD:Fortress Crab:fortress crab').
card_rarity('fortress crab'/'ISD', 'Common').
card_artist('fortress crab'/'ISD', 'Vincent Proce').
card_number('fortress crab'/'ISD', '56').
card_flavor_text('fortress crab'/'ISD', 'Unbreakable and unappetizing, the crab grows uninterrupted, sometimes to the size of a cottage and beyond.').
card_multiverse_id('fortress crab'/'ISD', '234429').

card_in_set('frightful delusion', 'ISD').
card_original_type('frightful delusion'/'ISD', 'Instant').
card_original_text('frightful delusion'/'ISD', 'Counter target spell unless its controller pays {1}. That player discards a card.').
card_first_print('frightful delusion', 'ISD').
card_image_name('frightful delusion'/'ISD', 'frightful delusion').
card_uid('frightful delusion'/'ISD', 'ISD:Frightful Delusion:frightful delusion').
card_rarity('frightful delusion'/'ISD', 'Common').
card_artist('frightful delusion'/'ISD', 'Anthony Palumbo').
card_number('frightful delusion'/'ISD', '57').
card_flavor_text('frightful delusion'/'ISD', 'Whether he actually exists is in question, but the terror she feels is excruciatingly real.').
card_multiverse_id('frightful delusion'/'ISD', '220031').

card_in_set('full moon\'s rise', 'ISD').
card_original_type('full moon\'s rise'/'ISD', 'Enchantment').
card_original_text('full moon\'s rise'/'ISD', 'Werewolf creatures you control get +1/+0 and have trample.\nSacrifice Full Moon\'s Rise: Regenerate all Werewolf creatures you control.').
card_first_print('full moon\'s rise', 'ISD').
card_image_name('full moon\'s rise'/'ISD', 'full moon\'s rise').
card_uid('full moon\'s rise'/'ISD', 'ISD:Full Moon\'s Rise:full moon\'s rise').
card_rarity('full moon\'s rise'/'ISD', 'Uncommon').
card_artist('full moon\'s rise'/'ISD', 'Terese Nielsen').
card_number('full moon\'s rise'/'ISD', '180').
card_flavor_text('full moon\'s rise'/'ISD', 'Some lycanthropes wish they could abandon their weak human shapes and revel in the power of the wolf.').
card_multiverse_id('full moon\'s rise'/'ISD', '241981').

card_in_set('furor of the bitten', 'ISD').
card_original_type('furor of the bitten'/'ISD', 'Enchantment — Aura').
card_original_text('furor of the bitten'/'ISD', 'Enchant creature\nEnchanted creature gets +2/+2 and attacks each turn if able.').
card_first_print('furor of the bitten', 'ISD').
card_image_name('furor of the bitten'/'ISD', 'furor of the bitten').
card_uid('furor of the bitten'/'ISD', 'ISD:Furor of the Bitten:furor of the bitten').
card_rarity('furor of the bitten'/'ISD', 'Common').
card_artist('furor of the bitten'/'ISD', 'Randy Gallegos').
card_number('furor of the bitten'/'ISD', '143').
card_flavor_text('furor of the bitten'/'ISD', 'Those who only believe themselves infected with lycanthropy can be as dangerous as those who really are.').
card_multiverse_id('furor of the bitten'/'ISD', '234431').

card_in_set('gallows warden', 'ISD').
card_original_type('gallows warden'/'ISD', 'Creature — Spirit').
card_original_text('gallows warden'/'ISD', 'Flying\nOther Spirit creatures you control get +0/+1.').
card_first_print('gallows warden', 'ISD').
card_image_name('gallows warden'/'ISD', 'gallows warden').
card_uid('gallows warden'/'ISD', 'ISD:Gallows Warden:gallows warden').
card_rarity('gallows warden'/'ISD', 'Uncommon').
card_artist('gallows warden'/'ISD', 'Dan Scott').
card_number('gallows warden'/'ISD', '16').
card_flavor_text('gallows warden'/'ISD', 'The spirit cares nothing for the crimes or triumphs of the slain. It shelters all beneath its stormy cloak.').
card_multiverse_id('gallows warden'/'ISD', '233248').

card_in_set('galvanic juggernaut', 'ISD').
card_original_type('galvanic juggernaut'/'ISD', 'Artifact Creature — Juggernaut').
card_original_text('galvanic juggernaut'/'ISD', 'Galvanic Juggernaut attacks each turn if able.\nGalvanic Juggernaut doesn\'t untap during your untap step.\nWhenever another creature dies, untap Galvanic Juggernaut.').
card_first_print('galvanic juggernaut', 'ISD').
card_image_name('galvanic juggernaut'/'ISD', 'galvanic juggernaut').
card_uid('galvanic juggernaut'/'ISD', 'ISD:Galvanic Juggernaut:galvanic juggernaut').
card_rarity('galvanic juggernaut'/'ISD', 'Uncommon').
card_artist('galvanic juggernaut'/'ISD', 'Lucas Graciano').
card_number('galvanic juggernaut'/'ISD', '222').
card_multiverse_id('galvanic juggernaut'/'ISD', '230772').

card_in_set('garruk relentless', 'ISD').
card_original_type('garruk relentless'/'ISD', 'Planeswalker — Garruk').
card_original_text('garruk relentless'/'ISD', 'When Garruk Relentless has two or fewer loyalty counters on him, transform him.\n0: Garruk Relentless deals 3 damage to target creature. That creature deals damage equal to its power to him.\n0: Put a 2/2 green Wolf creature token onto the battlefield.').
card_first_print('garruk relentless', 'ISD').
card_image_name('garruk relentless'/'ISD', 'garruk relentless').
card_uid('garruk relentless'/'ISD', 'ISD:Garruk Relentless:garruk relentless').
card_rarity('garruk relentless'/'ISD', 'Mythic Rare').
card_artist('garruk relentless'/'ISD', 'Eric Deschamps').
card_number('garruk relentless'/'ISD', '181a').
card_multiverse_id('garruk relentless'/'ISD', '245250').

card_in_set('garruk, the veil-cursed', 'ISD').
card_original_type('garruk, the veil-cursed'/'ISD', 'Planeswalker — Garruk').
card_original_text('garruk, the veil-cursed'/'ISD', '+1: Put a 1/1 black Wolf creature token with deathtouch onto the battlefield.\n-1: Sacrifice a creature. If you do, search your library for a creature card, reveal it, put it into your hand, then shuffle your library.\n-3: Creatures you control gain trample and get +X/+X until end of turn, where X is the number of creature cards in your graveyard.').
card_first_print('garruk, the veil-cursed', 'ISD').
card_image_name('garruk, the veil-cursed'/'ISD', 'garruk, the veil-cursed').
card_uid('garruk, the veil-cursed'/'ISD', 'ISD:Garruk, the Veil-Cursed:garruk, the veil-cursed').
card_rarity('garruk, the veil-cursed'/'ISD', 'Mythic Rare').
card_artist('garruk, the veil-cursed'/'ISD', 'Eric Deschamps').
card_number('garruk, the veil-cursed'/'ISD', '181b').
card_multiverse_id('garruk, the veil-cursed'/'ISD', '245251').

card_in_set('gatstaf howler', 'ISD').
card_original_type('gatstaf howler'/'ISD', 'Creature — Werewolf').
card_original_text('gatstaf howler'/'ISD', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)\nAt the beginning of each upkeep, if a player cast two or more spells last turn, transform Gatstaf Howler.').
card_first_print('gatstaf howler', 'ISD').
card_image_name('gatstaf howler'/'ISD', 'gatstaf howler').
card_uid('gatstaf howler'/'ISD', 'ISD:Gatstaf Howler:gatstaf howler').
card_rarity('gatstaf howler'/'ISD', 'Uncommon').
card_artist('gatstaf howler'/'ISD', 'Mark Evans').
card_number('gatstaf howler'/'ISD', '182b').
card_multiverse_id('gatstaf howler'/'ISD', '227290').

card_in_set('gatstaf shepherd', 'ISD').
card_original_type('gatstaf shepherd'/'ISD', 'Creature — Human Werewolf').
card_original_text('gatstaf shepherd'/'ISD', 'At the beginning of each upkeep, if no spells were cast last turn, transform Gatstaf Shepherd.').
card_first_print('gatstaf shepherd', 'ISD').
card_image_name('gatstaf shepherd'/'ISD', 'gatstaf shepherd').
card_uid('gatstaf shepherd'/'ISD', 'ISD:Gatstaf Shepherd:gatstaf shepherd').
card_rarity('gatstaf shepherd'/'ISD', 'Uncommon').
card_artist('gatstaf shepherd'/'ISD', 'Mark Evans').
card_number('gatstaf shepherd'/'ISD', '182a').
card_flavor_text('gatstaf shepherd'/'ISD', '\"What is worse than a wolf in sheep\'s clothing?\"\n—Wolfhunter\'s riddle').
card_multiverse_id('gatstaf shepherd'/'ISD', '227409').

card_in_set('gavony township', 'ISD').
card_original_type('gavony township'/'ISD', 'Land').
card_original_text('gavony township'/'ISD', '{T}: Add {1} to your mana pool.\n{2}{G}{W}, {T}: Put a +1/+1 counter on each creature you control.').
card_first_print('gavony township', 'ISD').
card_image_name('gavony township'/'ISD', 'gavony township').
card_uid('gavony township'/'ISD', 'ISD:Gavony Township:gavony township').
card_rarity('gavony township'/'ISD', 'Rare').
card_artist('gavony township'/'ISD', 'Peter Mohrbacher').
card_number('gavony township'/'ISD', '239').
card_flavor_text('gavony township'/'ISD', '\"The protective wards of the church have weakened, and no one can tell us why. It\'s time to look to our own defenses.\"\n—Gregel, militia leader').
card_multiverse_id('gavony township'/'ISD', '233242').

card_in_set('geist of saint traft', 'ISD').
card_original_type('geist of saint traft'/'ISD', 'Legendary Creature — Spirit Cleric').
card_original_text('geist of saint traft'/'ISD', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)\nWhenever Geist of Saint Traft attacks, put a 4/4 white Angel creature token with flying onto the battlefield tapped and attacking. Exile that token at end of combat.').
card_first_print('geist of saint traft', 'ISD').
card_image_name('geist of saint traft'/'ISD', 'geist of saint traft').
card_uid('geist of saint traft'/'ISD', 'ISD:Geist of Saint Traft:geist of saint traft').
card_rarity('geist of saint traft'/'ISD', 'Mythic Rare').
card_artist('geist of saint traft'/'ISD', 'Igor Kieryluk').
card_number('geist of saint traft'/'ISD', '213').
card_multiverse_id('geist of saint traft'/'ISD', '247236').

card_in_set('geist-honored monk', 'ISD').
card_original_type('geist-honored monk'/'ISD', 'Creature — Human Monk').
card_original_text('geist-honored monk'/'ISD', 'Vigilance\nGeist-Honored Monk\'s power and toughness are each equal to the number of creatures you control.\nWhen Geist-Honored Monk enters the battlefield, put two 1/1 white Spirit creature tokens with flying onto the battlefield.').
card_first_print('geist-honored monk', 'ISD').
card_image_name('geist-honored monk'/'ISD', 'geist-honored monk').
card_uid('geist-honored monk'/'ISD', 'ISD:Geist-Honored Monk:geist-honored monk').
card_rarity('geist-honored monk'/'ISD', 'Rare').
card_artist('geist-honored monk'/'ISD', 'Clint Cearley').
card_number('geist-honored monk'/'ISD', '17').
card_multiverse_id('geist-honored monk'/'ISD', '230787').

card_in_set('geistcatcher\'s rig', 'ISD').
card_original_type('geistcatcher\'s rig'/'ISD', 'Artifact Creature — Construct').
card_original_text('geistcatcher\'s rig'/'ISD', 'When Geistcatcher\'s Rig enters the battlefield, you may have it deal 4 damage to target creature with flying.').
card_first_print('geistcatcher\'s rig', 'ISD').
card_image_name('geistcatcher\'s rig'/'ISD', 'geistcatcher\'s rig').
card_uid('geistcatcher\'s rig'/'ISD', 'ISD:Geistcatcher\'s Rig:geistcatcher\'s rig').
card_rarity('geistcatcher\'s rig'/'ISD', 'Uncommon').
card_artist('geistcatcher\'s rig'/'ISD', 'Vincent Proce').
card_number('geistcatcher\'s rig'/'ISD', '223').
card_flavor_text('geistcatcher\'s rig'/'ISD', 'The rigs were outlawed except in Nephalia, where malign spirits are as plentiful as crazed inventors.').
card_multiverse_id('geistcatcher\'s rig'/'ISD', '234445').

card_in_set('geistflame', 'ISD').
card_original_type('geistflame'/'ISD', 'Instant').
card_original_text('geistflame'/'ISD', 'Geistflame deals 1 damage to target creature or player.\nFlashback {3}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('geistflame', 'ISD').
card_image_name('geistflame'/'ISD', 'geistflame').
card_uid('geistflame'/'ISD', 'ISD:Geistflame:geistflame').
card_rarity('geistflame'/'ISD', 'Common').
card_artist('geistflame'/'ISD', 'Scott Chou').
card_number('geistflame'/'ISD', '144').
card_flavor_text('geistflame'/'ISD', 'Innistrad pyromancers find their best raw materials in the fury of the dead.').
card_multiverse_id('geistflame'/'ISD', '247427').

card_in_set('ghost quarter', 'ISD').
card_original_type('ghost quarter'/'ISD', 'Land').
card_original_text('ghost quarter'/'ISD', '{T}: Add {1} to your mana pool.\n{T}, Sacrifice Ghost Quarter: Destroy target land. Its controller may search his or her library for a basic land card, put it onto the battlefield, then shuffle his or her library.').
card_image_name('ghost quarter'/'ISD', 'ghost quarter').
card_uid('ghost quarter'/'ISD', 'ISD:Ghost Quarter:ghost quarter').
card_rarity('ghost quarter'/'ISD', 'Uncommon').
card_artist('ghost quarter'/'ISD', 'Peter Mohrbacher').
card_number('ghost quarter'/'ISD', '240').
card_flavor_text('ghost quarter'/'ISD', 'Deserted, but not uninhabited.').
card_multiverse_id('ghost quarter'/'ISD', '220371').

card_in_set('ghostly possession', 'ISD').
card_original_type('ghostly possession'/'ISD', 'Enchantment — Aura').
card_original_text('ghostly possession'/'ISD', 'Enchant creature\nEnchanted creature has flying.\nPrevent all combat damage that would be dealt to and dealt by enchanted creature.').
card_first_print('ghostly possession', 'ISD').
card_image_name('ghostly possession'/'ISD', 'ghostly possession').
card_uid('ghostly possession'/'ISD', 'ISD:Ghostly Possession:ghostly possession').
card_rarity('ghostly possession'/'ISD', 'Common').
card_artist('ghostly possession'/'ISD', 'Howard Lyon').
card_number('ghostly possession'/'ISD', '18').
card_multiverse_id('ghostly possession'/'ISD', '220052').

card_in_set('ghoulcaller\'s bell', 'ISD').
card_original_type('ghoulcaller\'s bell'/'ISD', 'Artifact').
card_original_text('ghoulcaller\'s bell'/'ISD', '{T}: Each player puts the top card of his or her library into his or her graveyard.').
card_first_print('ghoulcaller\'s bell', 'ISD').
card_image_name('ghoulcaller\'s bell'/'ISD', 'ghoulcaller\'s bell').
card_uid('ghoulcaller\'s bell'/'ISD', 'ISD:Ghoulcaller\'s Bell:ghoulcaller\'s bell').
card_rarity('ghoulcaller\'s bell'/'ISD', 'Common').
card_artist('ghoulcaller\'s bell'/'ISD', 'Lars Grant-West').
card_number('ghoulcaller\'s bell'/'ISD', '224').
card_flavor_text('ghoulcaller\'s bell'/'ISD', 'The bell\'s chime heralds the rise of the unhallowed from the grave and the fall of their victims into the earth.').
card_multiverse_id('ghoulcaller\'s bell'/'ISD', '237362').

card_in_set('ghoulcaller\'s chant', 'ISD').
card_original_type('ghoulcaller\'s chant'/'ISD', 'Sorcery').
card_original_text('ghoulcaller\'s chant'/'ISD', 'Choose one — Return target creature card from your graveyard to your hand; or return two target Zombie cards from your graveyard to your hand.').
card_first_print('ghoulcaller\'s chant', 'ISD').
card_image_name('ghoulcaller\'s chant'/'ISD', 'ghoulcaller\'s chant').
card_uid('ghoulcaller\'s chant'/'ISD', 'ISD:Ghoulcaller\'s Chant:ghoulcaller\'s chant').
card_rarity('ghoulcaller\'s chant'/'ISD', 'Common').
card_artist('ghoulcaller\'s chant'/'ISD', 'Randy Gallegos').
card_number('ghoulcaller\'s chant'/'ISD', '101').
card_flavor_text('ghoulcaller\'s chant'/'ISD', '\"The living are born small and weak. The dead rise ready to serve. The choice is obvious.\"\n—Gisa, ghoulcaller of Gavony').
card_multiverse_id('ghoulcaller\'s chant'/'ISD', '220021').

card_in_set('ghoulraiser', 'ISD').
card_original_type('ghoulraiser'/'ISD', 'Creature — Zombie').
card_original_text('ghoulraiser'/'ISD', 'When Ghoulraiser enters the battlefield, return a Zombie card at random from your graveyard to your hand.').
card_first_print('ghoulraiser', 'ISD').
card_image_name('ghoulraiser'/'ISD', 'ghoulraiser').
card_uid('ghoulraiser'/'ISD', 'ISD:Ghoulraiser:ghoulraiser').
card_rarity('ghoulraiser'/'ISD', 'Common').
card_artist('ghoulraiser'/'ISD', 'Steve Prescott').
card_number('ghoulraiser'/'ISD', '102').
card_flavor_text('ghoulraiser'/'ISD', '\"Come. Bring your brothers. Tonight, you feast on living flesh.\"\n—Jadar, ghoulcaller of Nephalia').
card_multiverse_id('ghoulraiser'/'ISD', '220019').

card_in_set('gnaw to the bone', 'ISD').
card_original_type('gnaw to the bone'/'ISD', 'Instant').
card_original_text('gnaw to the bone'/'ISD', 'You gain 2 life for each creature card in your graveyard.\nFlashback {2}{G} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('gnaw to the bone', 'ISD').
card_image_name('gnaw to the bone'/'ISD', 'gnaw to the bone').
card_uid('gnaw to the bone'/'ISD', 'ISD:Gnaw to the Bone:gnaw to the bone').
card_rarity('gnaw to the bone'/'ISD', 'Common').
card_artist('gnaw to the bone'/'ISD', 'Scott Chou').
card_number('gnaw to the bone'/'ISD', '183').
card_multiverse_id('gnaw to the bone'/'ISD', '247420').

card_in_set('grasp of phantoms', 'ISD').
card_original_type('grasp of phantoms'/'ISD', 'Sorcery').
card_original_text('grasp of phantoms'/'ISD', 'Put target creature on top of its owner\'s library.\nFlashback {7}{U} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('grasp of phantoms', 'ISD').
card_image_name('grasp of phantoms'/'ISD', 'grasp of phantoms').
card_uid('grasp of phantoms'/'ISD', 'ISD:Grasp of Phantoms:grasp of phantoms').
card_rarity('grasp of phantoms'/'ISD', 'Uncommon').
card_artist('grasp of phantoms'/'ISD', 'Izzy').
card_number('grasp of phantoms'/'ISD', '58').
card_multiverse_id('grasp of phantoms'/'ISD', '243208').

card_in_set('grave bramble', 'ISD').
card_original_type('grave bramble'/'ISD', 'Creature — Plant').
card_original_text('grave bramble'/'ISD', 'Defender, protection from Zombies').
card_first_print('grave bramble', 'ISD').
card_image_name('grave bramble'/'ISD', 'grave bramble').
card_uid('grave bramble'/'ISD', 'ISD:Grave Bramble:grave bramble').
card_rarity('grave bramble'/'ISD', 'Common').
card_artist('grave bramble'/'ISD', 'Anthony Jones').
card_number('grave bramble'/'ISD', '184').
card_flavor_text('grave bramble'/'ISD', 'Nature abhors a zombie.').
card_multiverse_id('grave bramble'/'ISD', '229964').

card_in_set('graveyard shovel', 'ISD').
card_original_type('graveyard shovel'/'ISD', 'Artifact').
card_original_text('graveyard shovel'/'ISD', '{2}, {T}: Target player exiles a card from his or her graveyard. If it\'s a creature card, you gain 2 life.').
card_first_print('graveyard shovel', 'ISD').
card_image_name('graveyard shovel'/'ISD', 'graveyard shovel').
card_uid('graveyard shovel'/'ISD', 'ISD:Graveyard Shovel:graveyard shovel').
card_rarity('graveyard shovel'/'ISD', 'Uncommon').
card_artist('graveyard shovel'/'ISD', 'Martina Pilcerova').
card_number('graveyard shovel'/'ISD', '225').
card_flavor_text('graveyard shovel'/'ISD', 'Thanks to Havengul\'s thriving illegal market for corpses, the shovel that interred a body during the day is usually the same one digging it out at night.').
card_multiverse_id('graveyard shovel'/'ISD', '221288').

card_in_set('grimgrin, corpse-born', 'ISD').
card_original_type('grimgrin, corpse-born'/'ISD', 'Legendary Creature — Zombie Warrior').
card_original_text('grimgrin, corpse-born'/'ISD', 'Grimgrin, Corpse-Born enters the battlefield tapped and doesn\'t untap during your untap step.\nSacrifice another creature: Untap Grimgrin and put a +1/+1 counter on it.\nWhenever Grimgrin attacks, destroy target creature defending player controls, then put a +1/+1 counter on Grimgrin.').
card_first_print('grimgrin, corpse-born', 'ISD').
card_image_name('grimgrin, corpse-born'/'ISD', 'grimgrin, corpse-born').
card_uid('grimgrin, corpse-born'/'ISD', 'ISD:Grimgrin, Corpse-Born:grimgrin, corpse-born').
card_rarity('grimgrin, corpse-born'/'ISD', 'Mythic Rare').
card_artist('grimgrin, corpse-born'/'ISD', 'Peter Mohrbacher').
card_number('grimgrin, corpse-born'/'ISD', '214').
card_multiverse_id('grimgrin, corpse-born'/'ISD', '247237').

card_in_set('grimoire of the dead', 'ISD').
card_original_type('grimoire of the dead'/'ISD', 'Legendary Artifact').
card_original_text('grimoire of the dead'/'ISD', '{1}, {T}, Discard a card: Put a study counter on Grimoire of the Dead.\n{T}, Remove three study counters from Grimoire of the Dead and sacrifice it: Put all creature cards from all graveyards onto the battlefield under your control. They\'re black Zombies in addition to their other colors and types.').
card_first_print('grimoire of the dead', 'ISD').
card_image_name('grimoire of the dead'/'ISD', 'grimoire of the dead').
card_uid('grimoire of the dead'/'ISD', 'ISD:Grimoire of the Dead:grimoire of the dead').
card_rarity('grimoire of the dead'/'ISD', 'Mythic Rare').
card_artist('grimoire of the dead'/'ISD', 'Steven Belledin').
card_number('grimoire of the dead'/'ISD', '226').
card_multiverse_id('grimoire of the dead'/'ISD', '230792').

card_in_set('grizzled outcasts', 'ISD').
card_original_type('grizzled outcasts'/'ISD', 'Creature — Human Werewolf').
card_original_text('grizzled outcasts'/'ISD', 'At the beginning of each upkeep, if no spells were cast last turn, transform Grizzled Outcasts.').
card_first_print('grizzled outcasts', 'ISD').
card_image_name('grizzled outcasts'/'ISD', 'grizzled outcasts').
card_uid('grizzled outcasts'/'ISD', 'ISD:Grizzled Outcasts:grizzled outcasts').
card_rarity('grizzled outcasts'/'ISD', 'Common').
card_artist('grizzled outcasts'/'ISD', 'Randy Gallegos').
card_number('grizzled outcasts'/'ISD', '185a').
card_flavor_text('grizzled outcasts'/'ISD', 'Seen as unsavory, the hunters were never allowed in town—until one night they vanished.').
card_multiverse_id('grizzled outcasts'/'ISD', '222124').

card_in_set('gruesome deformity', 'ISD').
card_original_type('gruesome deformity'/'ISD', 'Enchantment — Aura').
card_original_text('gruesome deformity'/'ISD', 'Enchant creature\nEnchanted creature has intimidate. (It can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)').
card_first_print('gruesome deformity', 'ISD').
card_image_name('gruesome deformity'/'ISD', 'gruesome deformity').
card_uid('gruesome deformity'/'ISD', 'ISD:Gruesome Deformity:gruesome deformity').
card_rarity('gruesome deformity'/'ISD', 'Common').
card_artist('gruesome deformity'/'ISD', 'Matt Stewart').
card_number('gruesome deformity'/'ISD', '103').
card_flavor_text('gruesome deformity'/'ISD', 'She wanted to inspire terror wherever she went. The demons happily granted her wish.').
card_multiverse_id('gruesome deformity'/'ISD', '246945').

card_in_set('gutter grime', 'ISD').
card_original_type('gutter grime'/'ISD', 'Enchantment').
card_original_text('gutter grime'/'ISD', 'Whenever a nontoken creature you control dies, put a slime counter on Gutter Grime, then put a green Ooze creature token onto the battlefield with \"This creature\'s power and toughness are each equal to the number of slime counters on Gutter Grime.\"').
card_first_print('gutter grime', 'ISD').
card_image_name('gutter grime'/'ISD', 'gutter grime').
card_uid('gutter grime'/'ISD', 'ISD:Gutter Grime:gutter grime').
card_rarity('gutter grime'/'ISD', 'Rare').
card_artist('gutter grime'/'ISD', 'Erica Yang').
card_number('gutter grime'/'ISD', '186').
card_multiverse_id('gutter grime'/'ISD', '234849').

card_in_set('hamlet captain', 'ISD').
card_original_type('hamlet captain'/'ISD', 'Creature — Human Warrior').
card_original_text('hamlet captain'/'ISD', 'Whenever Hamlet Captain attacks or blocks, other Human creatures you control get +1/+1 until end of turn.').
card_first_print('hamlet captain', 'ISD').
card_image_name('hamlet captain'/'ISD', 'hamlet captain').
card_uid('hamlet captain'/'ISD', 'ISD:Hamlet Captain:hamlet captain').
card_rarity('hamlet captain'/'ISD', 'Uncommon').
card_artist('hamlet captain'/'ISD', 'Wayne Reynolds').
card_number('hamlet captain'/'ISD', '187').
card_flavor_text('hamlet captain'/'ISD', '\"I would fear this fight, bloodsucker, were I alone.\"').
card_multiverse_id('hamlet captain'/'ISD', '234446').

card_in_set('hanweir watchkeep', 'ISD').
card_original_type('hanweir watchkeep'/'ISD', 'Creature — Human Warrior Werewolf').
card_original_text('hanweir watchkeep'/'ISD', 'Defender\nAt the beginning of each upkeep, if no spells were cast last turn, transform Hanweir Watchkeep.').
card_first_print('hanweir watchkeep', 'ISD').
card_image_name('hanweir watchkeep'/'ISD', 'hanweir watchkeep').
card_uid('hanweir watchkeep'/'ISD', 'ISD:Hanweir Watchkeep:hanweir watchkeep').
card_rarity('hanweir watchkeep'/'ISD', 'Uncommon').
card_artist('hanweir watchkeep'/'ISD', 'Wayne Reynolds').
card_number('hanweir watchkeep'/'ISD', '145a').
card_flavor_text('hanweir watchkeep'/'ISD', 'He scans for wolves, knowing there\'s one he can never anticipate.').
card_multiverse_id('hanweir watchkeep'/'ISD', '244683').

card_in_set('harvest pyre', 'ISD').
card_original_type('harvest pyre'/'ISD', 'Instant').
card_original_text('harvest pyre'/'ISD', 'As an additional cost to cast Harvest Pyre, exile X cards from your graveyard.\nHarvest Pyre deals X damage to target creature.').
card_first_print('harvest pyre', 'ISD').
card_image_name('harvest pyre'/'ISD', 'harvest pyre').
card_uid('harvest pyre'/'ISD', 'ISD:Harvest Pyre:harvest pyre').
card_rarity('harvest pyre'/'ISD', 'Common').
card_artist('harvest pyre'/'ISD', 'Ryan Yee').
card_number('harvest pyre'/'ISD', '146').
card_flavor_text('harvest pyre'/'ISD', 'Corpses destroyed by fire form the most destructive geists.').
card_multiverse_id('harvest pyre'/'ISD', '220010').

card_in_set('heartless summoning', 'ISD').
card_original_type('heartless summoning'/'ISD', 'Enchantment').
card_original_text('heartless summoning'/'ISD', 'Creature spells you cast cost {2} less to cast.\nCreatures you control get -1/-1.').
card_first_print('heartless summoning', 'ISD').
card_image_name('heartless summoning'/'ISD', 'heartless summoning').
card_uid('heartless summoning'/'ISD', 'ISD:Heartless Summoning:heartless summoning').
card_rarity('heartless summoning'/'ISD', 'Rare').
card_artist('heartless summoning'/'ISD', 'Anthony Palumbo').
card_number('heartless summoning'/'ISD', '104').
card_flavor_text('heartless summoning'/'ISD', '\"They won\'t be winning any beauty pageants, but they\'ll do the trick.\"\n—Enslow, ghoulcaller of Nephalia').
card_multiverse_id('heartless summoning'/'ISD', '244678').

card_in_set('heretic\'s punishment', 'ISD').
card_original_type('heretic\'s punishment'/'ISD', 'Enchantment').
card_original_text('heretic\'s punishment'/'ISD', '{3}{R}: Choose target creature or player, then put the top three cards of your library into your graveyard.  Heretic\'s Punishment deals damage to that creature or player equal to the highest converted mana cost among those cards.').
card_first_print('heretic\'s punishment', 'ISD').
card_image_name('heretic\'s punishment'/'ISD', 'heretic\'s punishment').
card_uid('heretic\'s punishment'/'ISD', 'ISD:Heretic\'s Punishment:heretic\'s punishment').
card_rarity('heretic\'s punishment'/'ISD', 'Rare').
card_artist('heretic\'s punishment'/'ISD', 'Vincent Proce').
card_number('heretic\'s punishment'/'ISD', '147').
card_multiverse_id('heretic\'s punishment'/'ISD', '249974').

card_in_set('hinterland harbor', 'ISD').
card_original_type('hinterland harbor'/'ISD', 'Land').
card_original_text('hinterland harbor'/'ISD', 'Hinterland Harbor enters the battlefield tapped unless you control a Forest or an Island.\n{T}: Add {G} or {U} to your mana pool.').
card_first_print('hinterland harbor', 'ISD').
card_image_name('hinterland harbor'/'ISD', 'hinterland harbor').
card_uid('hinterland harbor'/'ISD', 'ISD:Hinterland Harbor:hinterland harbor').
card_rarity('hinterland harbor'/'ISD', 'Rare').
card_artist('hinterland harbor'/'ISD', 'Karl Kopinski').
card_number('hinterland harbor'/'ISD', '241').
card_flavor_text('hinterland harbor'/'ISD', 'In places where the sea meets the trees, it\'s easy to forget the darkness that rules most of Innistrad.').
card_multiverse_id('hinterland harbor'/'ISD', '241988').

card_in_set('hollowhenge scavenger', 'ISD').
card_original_type('hollowhenge scavenger'/'ISD', 'Creature — Elemental').
card_original_text('hollowhenge scavenger'/'ISD', 'Morbid — When Hollowhenge Scavenger enters the battlefield, if a creature died this turn, you gain 5 life.').
card_first_print('hollowhenge scavenger', 'ISD').
card_image_name('hollowhenge scavenger'/'ISD', 'hollowhenge scavenger').
card_uid('hollowhenge scavenger'/'ISD', 'ISD:Hollowhenge Scavenger:hollowhenge scavenger').
card_rarity('hollowhenge scavenger'/'ISD', 'Uncommon').
card_artist('hollowhenge scavenger'/'ISD', 'Slawomir Maniak').
card_number('hollowhenge scavenger'/'ISD', '188').
card_flavor_text('hollowhenge scavenger'/'ISD', 'After werewolves slaughtered the citizenry of Hollowhenge, other creatures moved in.').
card_multiverse_id('hollowhenge scavenger'/'ISD', '226883').

card_in_set('homicidal brute', 'ISD').
card_original_type('homicidal brute'/'ISD', 'Creature — Human Mutant').
card_original_text('homicidal brute'/'ISD', 'At the beginning of your end step, if Homicidal Brute didn\'t attack this turn, tap Homicidal Brute, then transform it.').
card_first_print('homicidal brute', 'ISD').
card_image_name('homicidal brute'/'ISD', 'homicidal brute').
card_uid('homicidal brute'/'ISD', 'ISD:Homicidal Brute:homicidal brute').
card_rarity('homicidal brute'/'ISD', 'Uncommon').
card_artist('homicidal brute'/'ISD', 'Michael C. Hayes').
card_number('homicidal brute'/'ISD', '47b').
card_flavor_text('homicidal brute'/'ISD', 'For a moment his basest desires run free, unshackled by reason or conscience.').
card_multiverse_id('homicidal brute'/'ISD', '221185').

card_in_set('howlpack alpha', 'ISD').
card_original_type('howlpack alpha'/'ISD', 'Creature — Werewolf').
card_original_text('howlpack alpha'/'ISD', 'Other Werewolf and Wolf creatures you control get +1/+1.\nAt the beginning of your end step, put a 2/2 green Wolf creature token onto the battlefield.\nAt the beginning of each upkeep, if a player cast two or more spells last turn, transform Howlpack Alpha.').
card_image_name('howlpack alpha'/'ISD', 'howlpack alpha').
card_uid('howlpack alpha'/'ISD', 'ISD:Howlpack Alpha:howlpack alpha').
card_rarity('howlpack alpha'/'ISD', 'Rare').
card_artist('howlpack alpha'/'ISD', 'Svetlin Velinov').
card_number('howlpack alpha'/'ISD', '193b').
card_multiverse_id('howlpack alpha'/'ISD', '222183').

card_in_set('howlpack of estwald', 'ISD').
card_original_type('howlpack of estwald'/'ISD', 'Creature — Werewolf').
card_original_text('howlpack of estwald'/'ISD', 'At the beginning of each upkeep, if a player cast two or more spells last turn, transform Howlpack of Estwald.').
card_first_print('howlpack of estwald', 'ISD').
card_image_name('howlpack of estwald'/'ISD', 'howlpack of estwald').
card_uid('howlpack of estwald'/'ISD', 'ISD:Howlpack of Estwald:howlpack of estwald').
card_rarity('howlpack of estwald'/'ISD', 'Common').
card_artist('howlpack of estwald'/'ISD', 'Kev Walker').
card_number('howlpack of estwald'/'ISD', '209b').
card_flavor_text('howlpack of estwald'/'ISD', 'Estwald\'s citizens don\'t dislike outsiders—they taste just fine.').
card_multiverse_id('howlpack of estwald'/'ISD', '222906').

card_in_set('hysterical blindness', 'ISD').
card_original_type('hysterical blindness'/'ISD', 'Instant').
card_original_text('hysterical blindness'/'ISD', 'Creatures your opponents control get -4/-0 until end of turn.').
card_first_print('hysterical blindness', 'ISD').
card_image_name('hysterical blindness'/'ISD', 'hysterical blindness').
card_uid('hysterical blindness'/'ISD', 'ISD:Hysterical Blindness:hysterical blindness').
card_rarity('hysterical blindness'/'ISD', 'Common').
card_artist('hysterical blindness'/'ISD', 'Wayne England').
card_number('hysterical blindness'/'ISD', '59').
card_flavor_text('hysterical blindness'/'ISD', '\"The panic is amusing, but the real fun comes when the spell passes and the villagers start looking for a scapegoat.\"\n—Ludevic, necro-alchemist').
card_multiverse_id('hysterical blindness'/'ISD', '234433').

card_in_set('infernal plunge', 'ISD').
card_original_type('infernal plunge'/'ISD', 'Sorcery').
card_original_text('infernal plunge'/'ISD', 'As an additional cost to cast Infernal Plunge, sacrifice a creature.\nAdd {R}{R}{R} to your mana pool.').
card_first_print('infernal plunge', 'ISD').
card_image_name('infernal plunge'/'ISD', 'infernal plunge').
card_uid('infernal plunge'/'ISD', 'ISD:Infernal Plunge:infernal plunge').
card_rarity('infernal plunge'/'ISD', 'Common').
card_artist('infernal plunge'/'ISD', 'Daarken').
card_number('infernal plunge'/'ISD', '148').
card_flavor_text('infernal plunge'/'ISD', '\"Oh mighty Griselbrand, Scourge of the Heedless World, gladly I consume myself for thee!\"').
card_multiverse_id('infernal plunge'/'ISD', '235603').

card_in_set('inquisitor\'s flail', 'ISD').
card_original_type('inquisitor\'s flail'/'ISD', 'Artifact — Equipment').
card_original_text('inquisitor\'s flail'/'ISD', 'If equipped creature would deal combat damage, it deals double that damage instead.\nIf another creature would deal combat damage to equipped creature, it deals double that damage to equipped creature instead.\nEquip {2}').
card_first_print('inquisitor\'s flail', 'ISD').
card_image_name('inquisitor\'s flail'/'ISD', 'inquisitor\'s flail').
card_uid('inquisitor\'s flail'/'ISD', 'ISD:Inquisitor\'s Flail:inquisitor\'s flail').
card_rarity('inquisitor\'s flail'/'ISD', 'Uncommon').
card_artist('inquisitor\'s flail'/'ISD', 'Rob Alexander').
card_number('inquisitor\'s flail'/'ISD', '227').
card_multiverse_id('inquisitor\'s flail'/'ISD', '222196').

card_in_set('insectile aberration', 'ISD').
card_original_type('insectile aberration'/'ISD', 'Creature — Human Insect').
card_original_text('insectile aberration'/'ISD', 'Flying').
card_first_print('insectile aberration', 'ISD').
card_image_name('insectile aberration'/'ISD', 'insectile aberration').
card_uid('insectile aberration'/'ISD', 'ISD:Insectile Aberration:insectile aberration').
card_rarity('insectile aberration'/'ISD', 'Common').
card_artist('insectile aberration'/'ISD', 'Nils Hamm').
card_number('insectile aberration'/'ISD', '51b').
card_flavor_text('insectile aberration'/'ISD', '\"Unfortunately, all my test animals have died or escaped, so I shall be the final subject. I feel no fear. This is a momentous night.\"\n—Laboratory notes, final entry').
card_multiverse_id('insectile aberration'/'ISD', '226755').

card_in_set('instigator gang', 'ISD').
card_original_type('instigator gang'/'ISD', 'Creature — Human Werewolf').
card_original_text('instigator gang'/'ISD', 'Attacking creatures you control get +1/+0.\nAt the beginning of each upkeep, if no spells were cast last turn, transform Instigator Gang.').
card_first_print('instigator gang', 'ISD').
card_image_name('instigator gang'/'ISD', 'instigator gang').
card_uid('instigator gang'/'ISD', 'ISD:Instigator Gang:instigator gang').
card_rarity('instigator gang'/'ISD', 'Rare').
card_artist('instigator gang'/'ISD', 'Greg Staples').
card_number('instigator gang'/'ISD', '149a').
card_multiverse_id('instigator gang'/'ISD', '227415').

card_in_set('intangible virtue', 'ISD').
card_original_type('intangible virtue'/'ISD', 'Enchantment').
card_original_text('intangible virtue'/'ISD', 'Creature tokens you control get +1/+1 and have vigilance.').
card_first_print('intangible virtue', 'ISD').
card_image_name('intangible virtue'/'ISD', 'intangible virtue').
card_uid('intangible virtue'/'ISD', 'ISD:Intangible Virtue:intangible virtue').
card_rarity('intangible virtue'/'ISD', 'Uncommon').
card_artist('intangible virtue'/'ISD', 'Clint Cearley').
card_number('intangible virtue'/'ISD', '19').
card_flavor_text('intangible virtue'/'ISD', 'In life, they were a motley crew: farmers, lords, cutpurses, priests. In death, they are united in singular, benevolent purpose.').
card_multiverse_id('intangible virtue'/'ISD', '243217').

card_in_set('into the maw of hell', 'ISD').
card_original_type('into the maw of hell'/'ISD', 'Sorcery').
card_original_text('into the maw of hell'/'ISD', 'Destroy target land. Into the Maw of Hell deals 13 damage to target creature.').
card_first_print('into the maw of hell', 'ISD').
card_image_name('into the maw of hell'/'ISD', 'into the maw of hell').
card_uid('into the maw of hell'/'ISD', 'ISD:Into the Maw of Hell:into the maw of hell').
card_rarity('into the maw of hell'/'ISD', 'Uncommon').
card_artist('into the maw of hell'/'ISD', 'Raymond Swanland').
card_number('into the maw of hell'/'ISD', '150').
card_flavor_text('into the maw of hell'/'ISD', 'Few entrances lead to where demons and devils lurk. Unfortunately, they occasionally open new ones.').
card_multiverse_id('into the maw of hell'/'ISD', '220035').

card_in_set('invisible stalker', 'ISD').
card_original_type('invisible stalker'/'ISD', 'Creature — Human Rogue').
card_original_text('invisible stalker'/'ISD', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)\nInvisible Stalker is unblockable.').
card_first_print('invisible stalker', 'ISD').
card_image_name('invisible stalker'/'ISD', 'invisible stalker').
card_uid('invisible stalker'/'ISD', 'ISD:Invisible Stalker:invisible stalker').
card_rarity('invisible stalker'/'ISD', 'Uncommon').
card_artist('invisible stalker'/'ISD', 'Bud Cook').
card_number('invisible stalker'/'ISD', '60').
card_flavor_text('invisible stalker'/'ISD', '\"All that concerns me is the vampires\' sense of smell and those freezing Nephalian nights.\"').
card_multiverse_id('invisible stalker'/'ISD', '220041').

card_in_set('ironfang', 'ISD').
card_original_type('ironfang'/'ISD', 'Creature — Werewolf').
card_original_text('ironfang'/'ISD', 'First strike\nAt the beginning of each upkeep, if a player cast two or more spells last turn, transform Ironfang.').
card_first_print('ironfang', 'ISD').
card_image_name('ironfang'/'ISD', 'ironfang').
card_uid('ironfang'/'ISD', 'ISD:Ironfang:ironfang').
card_rarity('ironfang'/'ISD', 'Common').
card_artist('ironfang'/'ISD', 'Christopher Moeller').
card_number('ironfang'/'ISD', '168b').
card_flavor_text('ironfang'/'ISD', 'Each morning, he repairs the front door.').
card_multiverse_id('ironfang'/'ISD', '222107').

card_in_set('island', 'ISD').
card_original_type('island'/'ISD', 'Basic Land — Island').
card_original_text('island'/'ISD', 'U').
card_image_name('island'/'ISD', 'island1').
card_uid('island'/'ISD', 'ISD:Island:island1').
card_rarity('island'/'ISD', 'Basic Land').
card_artist('island'/'ISD', 'James Paick').
card_number('island'/'ISD', '253').
card_multiverse_id('island'/'ISD', '245235').

card_in_set('island', 'ISD').
card_original_type('island'/'ISD', 'Basic Land — Island').
card_original_text('island'/'ISD', 'U').
card_image_name('island'/'ISD', 'island2').
card_uid('island'/'ISD', 'ISD:Island:island2').
card_rarity('island'/'ISD', 'Basic Land').
card_artist('island'/'ISD', 'Adam Paquette').
card_number('island'/'ISD', '254').
card_multiverse_id('island'/'ISD', '245237').

card_in_set('island', 'ISD').
card_original_type('island'/'ISD', 'Basic Land — Island').
card_original_text('island'/'ISD', 'U').
card_image_name('island'/'ISD', 'island3').
card_uid('island'/'ISD', 'ISD:Island:island3').
card_rarity('island'/'ISD', 'Basic Land').
card_artist('island'/'ISD', 'Jung Park').
card_number('island'/'ISD', '255').
card_multiverse_id('island'/'ISD', '245236').

card_in_set('isolated chapel', 'ISD').
card_original_type('isolated chapel'/'ISD', 'Land').
card_original_text('isolated chapel'/'ISD', 'Isolated Chapel enters the battlefield tapped unless you control a Plains or a Swamp.\n{T}: Add {W} or {B} to your mana pool.').
card_first_print('isolated chapel', 'ISD').
card_image_name('isolated chapel'/'ISD', 'isolated chapel').
card_uid('isolated chapel'/'ISD', 'ISD:Isolated Chapel:isolated chapel').
card_rarity('isolated chapel'/'ISD', 'Rare').
card_artist('isolated chapel'/'ISD', 'Cliff Childs').
card_number('isolated chapel'/'ISD', '242').
card_flavor_text('isolated chapel'/'ISD', 'Not every church is a place of faith.').
card_multiverse_id('isolated chapel'/'ISD', '241979').

card_in_set('kessig cagebreakers', 'ISD').
card_original_type('kessig cagebreakers'/'ISD', 'Creature — Human Rogue').
card_original_text('kessig cagebreakers'/'ISD', 'Whenever Kessig Cagebreakers attacks, put a 2/2 green Wolf creature token onto the battlefield tapped and attacking for each creature card in your graveyard.').
card_first_print('kessig cagebreakers', 'ISD').
card_image_name('kessig cagebreakers'/'ISD', 'kessig cagebreakers').
card_uid('kessig cagebreakers'/'ISD', 'ISD:Kessig Cagebreakers:kessig cagebreakers').
card_rarity('kessig cagebreakers'/'ISD', 'Rare').
card_artist('kessig cagebreakers'/'ISD', 'Wayne England').
card_number('kessig cagebreakers'/'ISD', '189').
card_flavor_text('kessig cagebreakers'/'ISD', '\"They put bars on these noble beasts and then wonder why werewolves target our towns.\"').
card_multiverse_id('kessig cagebreakers'/'ISD', '249868').

card_in_set('kessig wolf', 'ISD').
card_original_type('kessig wolf'/'ISD', 'Creature — Wolf').
card_original_text('kessig wolf'/'ISD', '{1}{R}: Kessig Wolf gains first strike until end of turn.').
card_first_print('kessig wolf', 'ISD').
card_image_name('kessig wolf'/'ISD', 'kessig wolf').
card_uid('kessig wolf'/'ISD', 'ISD:Kessig Wolf:kessig wolf').
card_rarity('kessig wolf'/'ISD', 'Common').
card_artist('kessig wolf'/'ISD', 'Wayne England').
card_number('kessig wolf'/'ISD', '151').
card_flavor_text('kessig wolf'/'ISD', '\"Don\'t be afraid when you hear the wolves howl. Be afraid when you don\'t hear them at all.\"\n—Saint Trogen, the Slayer').
card_multiverse_id('kessig wolf'/'ISD', '220040').

card_in_set('kessig wolf run', 'ISD').
card_original_type('kessig wolf run'/'ISD', 'Land').
card_original_text('kessig wolf run'/'ISD', '{T}: Add {1} to your mana pool.\n{X}{R}{G}, {T}: Target creature gets +X/+0 and gains trample until end of turn.').
card_first_print('kessig wolf run', 'ISD').
card_image_name('kessig wolf run'/'ISD', 'kessig wolf run').
card_uid('kessig wolf run'/'ISD', 'ISD:Kessig Wolf Run:kessig wolf run').
card_rarity('kessig wolf run'/'ISD', 'Rare').
card_artist('kessig wolf run'/'ISD', 'Eytan Zana').
card_number('kessig wolf run'/'ISD', '243').
card_flavor_text('kessig wolf run'/'ISD', 'When a werewolf changes for the first time, that first howl is said to echo through the wilds till moonset.').
card_multiverse_id('kessig wolf run'/'ISD', '233256').

card_in_set('kindercatch', 'ISD').
card_original_type('kindercatch'/'ISD', 'Creature — Spirit').
card_original_text('kindercatch'/'ISD', '').
card_first_print('kindercatch', 'ISD').
card_image_name('kindercatch'/'ISD', 'kindercatch').
card_uid('kindercatch'/'ISD', 'ISD:Kindercatch:kindercatch').
card_rarity('kindercatch'/'ISD', 'Common').
card_artist('kindercatch'/'ISD', 'Terese Nielsen').
card_number('kindercatch'/'ISD', '190').
card_flavor_text('kindercatch'/'ISD', 'In the forest villages of Kessig, parents feed their children wormwood and other bitter herbs, hoping to make them less palatable to things that roam the night.').
card_multiverse_id('kindercatch'/'ISD', '227088').

card_in_set('krallenhorde wantons', 'ISD').
card_original_type('krallenhorde wantons'/'ISD', 'Creature — Werewolf').
card_original_text('krallenhorde wantons'/'ISD', 'At the beginning of each upkeep, if a player cast two or more spells last turn, transform Krallenhorde Wantons.').
card_first_print('krallenhorde wantons', 'ISD').
card_image_name('krallenhorde wantons'/'ISD', 'krallenhorde wantons').
card_uid('krallenhorde wantons'/'ISD', 'ISD:Krallenhorde Wantons:krallenhorde wantons').
card_rarity('krallenhorde wantons'/'ISD', 'Common').
card_artist('krallenhorde wantons'/'ISD', 'Randy Gallegos').
card_number('krallenhorde wantons'/'ISD', '185b').
card_flavor_text('krallenhorde wantons'/'ISD', 'The boot prints became paw prints, and they led right back into town.').
card_multiverse_id('krallenhorde wantons'/'ISD', '222123').

card_in_set('kruin outlaw', 'ISD').
card_original_type('kruin outlaw'/'ISD', 'Creature — Human Rogue Werewolf').
card_original_text('kruin outlaw'/'ISD', 'First strike\nAt the beginning of each upkeep, if no spells were cast last turn, transform Kruin Outlaw.').
card_first_print('kruin outlaw', 'ISD').
card_image_name('kruin outlaw'/'ISD', 'kruin outlaw').
card_uid('kruin outlaw'/'ISD', 'ISD:Kruin Outlaw:kruin outlaw').
card_rarity('kruin outlaw'/'ISD', 'Rare').
card_artist('kruin outlaw'/'ISD', 'David Rapoza').
card_number('kruin outlaw'/'ISD', '152a').
card_flavor_text('kruin outlaw'/'ISD', '\"Hold tight. I\'ve got a surprise for them.\"').
card_multiverse_id('kruin outlaw'/'ISD', '227084').

card_in_set('laboratory maniac', 'ISD').
card_original_type('laboratory maniac'/'ISD', 'Creature — Human Wizard').
card_original_text('laboratory maniac'/'ISD', 'If you would draw a card while your library has no cards in it, you win the game instead.').
card_first_print('laboratory maniac', 'ISD').
card_image_name('laboratory maniac'/'ISD', 'laboratory maniac').
card_uid('laboratory maniac'/'ISD', 'ISD:Laboratory Maniac:laboratory maniac').
card_rarity('laboratory maniac'/'ISD', 'Rare').
card_artist('laboratory maniac'/'ISD', 'Jason Felix').
card_number('laboratory maniac'/'ISD', '61').
card_flavor_text('laboratory maniac'/'ISD', 'His mind whirled with grand plans, never thinking of what might happen if he were to succeed.').
card_multiverse_id('laboratory maniac'/'ISD', '230788').

card_in_set('lantern spirit', 'ISD').
card_original_type('lantern spirit'/'ISD', 'Creature — Spirit').
card_original_text('lantern spirit'/'ISD', 'Flying\n{U}: Return Lantern Spirit to its owner\'s hand.').
card_first_print('lantern spirit', 'ISD').
card_image_name('lantern spirit'/'ISD', 'lantern spirit').
card_uid('lantern spirit'/'ISD', 'ISD:Lantern Spirit:lantern spirit').
card_rarity('lantern spirit'/'ISD', 'Uncommon').
card_artist('lantern spirit'/'ISD', 'Johann Bodin').
card_number('lantern spirit'/'ISD', '62').
card_flavor_text('lantern spirit'/'ISD', 'Bound to the square where she died, she calls out to those in danger—usually scaring them straight into the peril she warns of.').
card_multiverse_id('lantern spirit'/'ISD', '241984').

card_in_set('liliana of the veil', 'ISD').
card_original_type('liliana of the veil'/'ISD', 'Planeswalker — Liliana').
card_original_text('liliana of the veil'/'ISD', '+1: Each player discards a card.\n-2: Target player sacrifices a creature.\n-6: Separate all permanents target player controls into two piles. That player sacrifices all permanents in the pile of his or her choice.').
card_first_print('liliana of the veil', 'ISD').
card_image_name('liliana of the veil'/'ISD', 'liliana of the veil').
card_uid('liliana of the veil'/'ISD', 'ISD:Liliana of the Veil:liliana of the veil').
card_rarity('liliana of the veil'/'ISD', 'Mythic Rare').
card_artist('liliana of the veil'/'ISD', 'Steve Argyle').
card_number('liliana of the veil'/'ISD', '105').
card_multiverse_id('liliana of the veil'/'ISD', '235597').

card_in_set('lord of lineage', 'ISD').
card_original_type('lord of lineage'/'ISD', 'Creature — Vampire').
card_original_text('lord of lineage'/'ISD', 'Flying\nOther Vampire creatures you control get +2/+2.\n{T}: Put a 2/2 black Vampire creature token with flying onto the battlefield.').
card_first_print('lord of lineage', 'ISD').
card_image_name('lord of lineage'/'ISD', 'lord of lineage').
card_uid('lord of lineage'/'ISD', 'ISD:Lord of Lineage:lord of lineage').
card_rarity('lord of lineage'/'ISD', 'Rare').
card_artist('lord of lineage'/'ISD', 'Jason Chan').
card_number('lord of lineage'/'ISD', '90b').
card_multiverse_id('lord of lineage'/'ISD', '227072').

card_in_set('lost in the mist', 'ISD').
card_original_type('lost in the mist'/'ISD', 'Instant').
card_original_text('lost in the mist'/'ISD', 'Counter target spell. Return target permanent to its owner\'s hand.').
card_first_print('lost in the mist', 'ISD').
card_image_name('lost in the mist'/'ISD', 'lost in the mist').
card_uid('lost in the mist'/'ISD', 'ISD:Lost in the Mist:lost in the mist').
card_rarity('lost in the mist'/'ISD', 'Common').
card_artist('lost in the mist'/'ISD', 'David Palumbo').
card_number('lost in the mist'/'ISD', '63').
card_flavor_text('lost in the mist'/'ISD', 'The secrets she carried could have saved many lives, but they quietly drowned with her.').
card_multiverse_id('lost in the mist'/'ISD', '241982').

card_in_set('ludevic\'s abomination', 'ISD').
card_original_type('ludevic\'s abomination'/'ISD', 'Creature — Lizard Horror').
card_original_text('ludevic\'s abomination'/'ISD', 'Trample').
card_image_name('ludevic\'s abomination'/'ISD', 'ludevic\'s abomination').
card_uid('ludevic\'s abomination'/'ISD', 'ISD:Ludevic\'s Abomination:ludevic\'s abomination').
card_rarity('ludevic\'s abomination'/'ISD', 'Rare').
card_artist('ludevic\'s abomination'/'ISD', 'Nils Hamm').
card_number('ludevic\'s abomination'/'ISD', '64b').
card_flavor_text('ludevic\'s abomination'/'ISD', 'After several frustrating experiments, the visionary Ludevic realized he needed to create a monster that fed on torch-wielding mobs.').
card_multiverse_id('ludevic\'s abomination'/'ISD', '221173').

card_in_set('ludevic\'s test subject', 'ISD').
card_original_type('ludevic\'s test subject'/'ISD', 'Creature — Lizard').
card_original_text('ludevic\'s test subject'/'ISD', 'Defender\n{1}{U}: Put a hatchling counter on Ludevic\'s Test Subject. Then if there are five or more hatchling counters on it, remove all of them and transform it.').
card_image_name('ludevic\'s test subject'/'ISD', 'ludevic\'s test subject').
card_uid('ludevic\'s test subject'/'ISD', 'ISD:Ludevic\'s Test Subject:ludevic\'s test subject').
card_rarity('ludevic\'s test subject'/'ISD', 'Rare').
card_artist('ludevic\'s test subject'/'ISD', 'Nils Hamm').
card_number('ludevic\'s test subject'/'ISD', '64a').
card_multiverse_id('ludevic\'s test subject'/'ISD', '221179').

card_in_set('lumberknot', 'ISD').
card_original_type('lumberknot'/'ISD', 'Creature — Treefolk').
card_original_text('lumberknot'/'ISD', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)\nWhenever a creature dies, put a +1/+1 counter on Lumberknot.').
card_first_print('lumberknot', 'ISD').
card_image_name('lumberknot'/'ISD', 'lumberknot').
card_uid('lumberknot'/'ISD', 'ISD:Lumberknot:lumberknot').
card_rarity('lumberknot'/'ISD', 'Uncommon').
card_artist('lumberknot'/'ISD', 'Jason A. Engle').
card_number('lumberknot'/'ISD', '191').
card_flavor_text('lumberknot'/'ISD', 'Animated by geists fused in its oak, it hungers for more life to add to its core.').
card_multiverse_id('lumberknot'/'ISD', '230624').

card_in_set('make a wish', 'ISD').
card_original_type('make a wish'/'ISD', 'Sorcery').
card_original_text('make a wish'/'ISD', 'Return two cards at random from your graveyard to your hand.').
card_first_print('make a wish', 'ISD').
card_image_name('make a wish'/'ISD', 'make a wish').
card_uid('make a wish'/'ISD', 'ISD:Make a Wish:make a wish').
card_rarity('make a wish'/'ISD', 'Uncommon').
card_artist('make a wish'/'ISD', 'Howard Lyon').
card_number('make a wish'/'ISD', '192').
card_flavor_text('make a wish'/'ISD', 'In Gavony wishing is taken as a sign of weakness, yet the wells usually brim with silver.').
card_multiverse_id('make a wish'/'ISD', '222193').

card_in_set('makeshift mauler', 'ISD').
card_original_type('makeshift mauler'/'ISD', 'Creature — Zombie Horror').
card_original_text('makeshift mauler'/'ISD', 'As an additional cost to cast Makeshift Mauler, exile a creature card from your graveyard.').
card_first_print('makeshift mauler', 'ISD').
card_image_name('makeshift mauler'/'ISD', 'makeshift mauler').
card_uid('makeshift mauler'/'ISD', 'ISD:Makeshift Mauler:makeshift mauler').
card_rarity('makeshift mauler'/'ISD', 'Common').
card_artist('makeshift mauler'/'ISD', 'James Ryman').
card_number('makeshift mauler'/'ISD', '65').
card_flavor_text('makeshift mauler'/'ISD', '\"It always amazes me what perfectly good things people throw away.\"\n—Ludevic, necro-alchemist').
card_multiverse_id('makeshift mauler'/'ISD', '222916').

card_in_set('manor gargoyle', 'ISD').
card_original_type('manor gargoyle'/'ISD', 'Artifact Creature — Gargoyle').
card_original_text('manor gargoyle'/'ISD', 'Defender\nManor Gargoyle is indestructible as long as it has defender.\n{1}: Until end of turn, Manor Gargoyle loses defender and gains flying.').
card_first_print('manor gargoyle', 'ISD').
card_image_name('manor gargoyle'/'ISD', 'manor gargoyle').
card_uid('manor gargoyle'/'ISD', 'ISD:Manor Gargoyle:manor gargoyle').
card_rarity('manor gargoyle'/'ISD', 'Rare').
card_artist('manor gargoyle'/'ISD', 'Matt Stewart').
card_number('manor gargoyle'/'ISD', '228').
card_flavor_text('manor gargoyle'/'ISD', 'Nothing could break it but the fall.').
card_multiverse_id('manor gargoyle'/'ISD', '227083').

card_in_set('manor skeleton', 'ISD').
card_original_type('manor skeleton'/'ISD', 'Creature — Skeleton').
card_original_text('manor skeleton'/'ISD', 'Haste\n{1}{B}: Regenerate Manor Skeleton.').
card_first_print('manor skeleton', 'ISD').
card_image_name('manor skeleton'/'ISD', 'manor skeleton').
card_uid('manor skeleton'/'ISD', 'ISD:Manor Skeleton:manor skeleton').
card_rarity('manor skeleton'/'ISD', 'Common').
card_artist('manor skeleton'/'ISD', 'Eric Deschamps').
card_number('manor skeleton'/'ISD', '106').
card_flavor_text('manor skeleton'/'ISD', 'Lungs dried to parchment wheeze blasphemies within a cage of bleached bone.').
card_multiverse_id('manor skeleton'/'ISD', '249976').

card_in_set('markov patrician', 'ISD').
card_original_type('markov patrician'/'ISD', 'Creature — Vampire').
card_original_text('markov patrician'/'ISD', 'Lifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_first_print('markov patrician', 'ISD').
card_image_name('markov patrician'/'ISD', 'markov patrician').
card_uid('markov patrician'/'ISD', 'ISD:Markov Patrician:markov patrician').
card_rarity('markov patrician'/'ISD', 'Common').
card_artist('markov patrician'/'ISD', 'Jana Schirmer & Johannes Voss').
card_number('markov patrician'/'ISD', '107').
card_flavor_text('markov patrician'/'ISD', 'Vampires of the Markov bloodline pride themselves on their exquisite taste in fashion, art, and \"wine of the vein.\"').
card_multiverse_id('markov patrician'/'ISD', '226754').

card_in_set('mask of avacyn', 'ISD').
card_original_type('mask of avacyn'/'ISD', 'Artifact — Equipment').
card_original_text('mask of avacyn'/'ISD', 'Equipped creature gets +1/+2 and has hexproof. (It can\'t be the target of spells or abilities your opponents control.)\nEquip {3}').
card_first_print('mask of avacyn', 'ISD').
card_image_name('mask of avacyn'/'ISD', 'mask of avacyn').
card_uid('mask of avacyn'/'ISD', 'ISD:Mask of Avacyn:mask of avacyn').
card_rarity('mask of avacyn'/'ISD', 'Uncommon').
card_artist('mask of avacyn'/'ISD', 'James Paick').
card_number('mask of avacyn'/'ISD', '229').
card_flavor_text('mask of avacyn'/'ISD', 'It hides the face and protects the soul.').
card_multiverse_id('mask of avacyn'/'ISD', '221193').

card_in_set('mausoleum guard', 'ISD').
card_original_type('mausoleum guard'/'ISD', 'Creature — Human Scout').
card_original_text('mausoleum guard'/'ISD', 'When Mausoleum Guard dies, put two 1/1 white Spirit creature tokens with flying onto the battlefield.').
card_first_print('mausoleum guard', 'ISD').
card_image_name('mausoleum guard'/'ISD', 'mausoleum guard').
card_uid('mausoleum guard'/'ISD', 'ISD:Mausoleum Guard:mausoleum guard').
card_rarity('mausoleum guard'/'ISD', 'Uncommon').
card_artist('mausoleum guard'/'ISD', 'David Palumbo').
card_number('mausoleum guard'/'ISD', '20').
card_flavor_text('mausoleum guard'/'ISD', '\"Ghoulcallers trying to get in, geists trying to get out . . . . This duty is never dull.\"').
card_multiverse_id('mausoleum guard'/'ISD', '237356').

card_in_set('maw of the mire', 'ISD').
card_original_type('maw of the mire'/'ISD', 'Sorcery').
card_original_text('maw of the mire'/'ISD', 'Destroy target land. You gain 4 life.').
card_first_print('maw of the mire', 'ISD').
card_image_name('maw of the mire'/'ISD', 'maw of the mire').
card_uid('maw of the mire'/'ISD', 'ISD:Maw of the Mire:maw of the mire').
card_rarity('maw of the mire'/'ISD', 'Common').
card_artist('maw of the mire'/'ISD', 'Vincent Proce').
card_number('maw of the mire'/'ISD', '108').
card_flavor_text('maw of the mire'/'ISD', 'As the chapel was dragged under, the bell in the tower rang until the murky depths forever silenced its toll.').
card_multiverse_id('maw of the mire'/'ISD', '246952').

card_in_set('mayor of avabruck', 'ISD').
card_original_type('mayor of avabruck'/'ISD', 'Creature — Human Advisor Werewolf').
card_original_text('mayor of avabruck'/'ISD', 'Other Human creatures you control get +1/+1.\nAt the beginning of each upkeep, if no spells were cast last turn, transform Mayor of Avabruck.').
card_image_name('mayor of avabruck'/'ISD', 'mayor of avabruck').
card_uid('mayor of avabruck'/'ISD', 'ISD:Mayor of Avabruck:mayor of avabruck').
card_rarity('mayor of avabruck'/'ISD', 'Rare').
card_artist('mayor of avabruck'/'ISD', 'Svetlin Velinov').
card_number('mayor of avabruck'/'ISD', '193a').
card_flavor_text('mayor of avabruck'/'ISD', 'He can deny his true nature for only so long.').
card_multiverse_id('mayor of avabruck'/'ISD', '222189').

card_in_set('memory\'s journey', 'ISD').
card_original_type('memory\'s journey'/'ISD', 'Instant').
card_original_text('memory\'s journey'/'ISD', 'Target player shuffles up to three target cards from his or her graveyard into his or her library.\nFlashback {G} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('memory\'s journey', 'ISD').
card_image_name('memory\'s journey'/'ISD', 'memory\'s journey').
card_uid('memory\'s journey'/'ISD', 'ISD:Memory\'s Journey:memory\'s journey').
card_rarity('memory\'s journey'/'ISD', 'Uncommon').
card_artist('memory\'s journey'/'ISD', 'Slawomir Maniak').
card_number('memory\'s journey'/'ISD', '66').
card_multiverse_id('memory\'s journey'/'ISD', '254134').

card_in_set('mentor of the meek', 'ISD').
card_original_type('mentor of the meek'/'ISD', 'Creature — Human Soldier').
card_original_text('mentor of the meek'/'ISD', 'Whenever another creature with power 2 or less enters the battlefield under your control, you may pay {1}. If you do, draw a card.').
card_first_print('mentor of the meek', 'ISD').
card_image_name('mentor of the meek'/'ISD', 'mentor of the meek').
card_uid('mentor of the meek'/'ISD', 'ISD:Mentor of the Meek:mentor of the meek').
card_rarity('mentor of the meek'/'ISD', 'Rare').
card_artist('mentor of the meek'/'ISD', 'Jana Schirmer & Johannes Voss').
card_number('mentor of the meek'/'ISD', '21').
card_flavor_text('mentor of the meek'/'ISD', '\"In these halls there is no pass or fail. Your true test comes with the first full moon.\"').
card_multiverse_id('mentor of the meek'/'ISD', '244682').

card_in_set('merciless predator', 'ISD').
card_original_type('merciless predator'/'ISD', 'Creature — Werewolf').
card_original_text('merciless predator'/'ISD', 'At the beginning of each upkeep, if a player cast two or more spells last turn, transform Merciless Predator.').
card_first_print('merciless predator', 'ISD').
card_image_name('merciless predator'/'ISD', 'merciless predator').
card_uid('merciless predator'/'ISD', 'ISD:Merciless Predator:merciless predator').
card_rarity('merciless predator'/'ISD', 'Uncommon').
card_artist('merciless predator'/'ISD', 'Michael C. Hayes').
card_number('merciless predator'/'ISD', '159b').
card_flavor_text('merciless predator'/'ISD', 'Before she just wanted to snatch your purse; now she\'ll take the whole arm.').
card_multiverse_id('merciless predator'/'ISD', '222115').

card_in_set('midnight haunting', 'ISD').
card_original_type('midnight haunting'/'ISD', 'Instant').
card_original_text('midnight haunting'/'ISD', 'Put two 1/1 white Spirit creature tokens with flying onto the battlefield.').
card_first_print('midnight haunting', 'ISD').
card_image_name('midnight haunting'/'ISD', 'midnight haunting').
card_uid('midnight haunting'/'ISD', 'ISD:Midnight Haunting:midnight haunting').
card_rarity('midnight haunting'/'ISD', 'Uncommon').
card_artist('midnight haunting'/'ISD', 'Matt Stewart').
card_number('midnight haunting'/'ISD', '22').
card_flavor_text('midnight haunting'/'ISD', 'The path back to the world of the living is murky and bewildering. A geist may not even realize that it\'s terrifying its own loved ones.').
card_multiverse_id('midnight haunting'/'ISD', '221214').

card_in_set('mikaeus, the lunarch', 'ISD').
card_original_type('mikaeus, the lunarch'/'ISD', 'Legendary Creature — Human Cleric').
card_original_text('mikaeus, the lunarch'/'ISD', 'Mikaeus, the Lunarch enters the battlefield with X +1/+1 counters on it.\n{T}: Put a +1/+1 counter on Mikaeus.\n{T}, Remove a +1/+1 counter from Mikaeus: Put a +1/+1 counter on each other creature you control.').
card_image_name('mikaeus, the lunarch'/'ISD', 'mikaeus, the lunarch').
card_uid('mikaeus, the lunarch'/'ISD', 'ISD:Mikaeus, the Lunarch:mikaeus, the lunarch').
card_rarity('mikaeus, the lunarch'/'ISD', 'Mythic Rare').
card_artist('mikaeus, the lunarch'/'ISD', 'Steven Belledin').
card_number('mikaeus, the lunarch'/'ISD', '23').
card_multiverse_id('mikaeus, the lunarch'/'ISD', '247234').

card_in_set('mindshrieker', 'ISD').
card_original_type('mindshrieker'/'ISD', 'Creature — Spirit Bird').
card_original_text('mindshrieker'/'ISD', 'Flying\n{2}: Target player puts the top card of his or her library into his or her graveyard. Mindshrieker gets +X/+X until end of turn, where X is that card\'s converted mana cost.').
card_first_print('mindshrieker', 'ISD').
card_image_name('mindshrieker'/'ISD', 'mindshrieker').
card_uid('mindshrieker'/'ISD', 'ISD:Mindshrieker:mindshrieker').
card_rarity('mindshrieker'/'ISD', 'Rare').
card_artist('mindshrieker'/'ISD', 'Dave Kendall').
card_number('mindshrieker'/'ISD', '67').
card_multiverse_id('mindshrieker'/'ISD', '229960').

card_in_set('mirror-mad phantasm', 'ISD').
card_original_type('mirror-mad phantasm'/'ISD', 'Creature — Spirit').
card_original_text('mirror-mad phantasm'/'ISD', 'Flying\n{1}{U}: Mirror-Mad Phantasm\'s owner shuffles it into his or her library. If that player does, he or she reveals cards from the top of that library until a card named Mirror-Mad Phantasm is revealed. The player puts that card onto the battlefield and all other cards revealed this way into his or her graveyard.').
card_first_print('mirror-mad phantasm', 'ISD').
card_image_name('mirror-mad phantasm'/'ISD', 'mirror-mad phantasm').
card_uid('mirror-mad phantasm'/'ISD', 'ISD:Mirror-Mad Phantasm:mirror-mad phantasm').
card_rarity('mirror-mad phantasm'/'ISD', 'Mythic Rare').
card_artist('mirror-mad phantasm'/'ISD', 'Howard Lyon').
card_number('mirror-mad phantasm'/'ISD', '68').
card_multiverse_id('mirror-mad phantasm'/'ISD', '227297').

card_in_set('moan of the unhallowed', 'ISD').
card_original_type('moan of the unhallowed'/'ISD', 'Sorcery').
card_original_text('moan of the unhallowed'/'ISD', 'Put two 2/2 black Zombie creature tokens onto the battlefield.\nFlashback {5}{B}{B} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('moan of the unhallowed', 'ISD').
card_image_name('moan of the unhallowed'/'ISD', 'moan of the unhallowed').
card_uid('moan of the unhallowed'/'ISD', 'ISD:Moan of the Unhallowed:moan of the unhallowed').
card_rarity('moan of the unhallowed'/'ISD', 'Uncommon').
card_artist('moan of the unhallowed'/'ISD', 'Nils Hamm').
card_number('moan of the unhallowed'/'ISD', '109').
card_flavor_text('moan of the unhallowed'/'ISD', 'For a ghoul, every village is a buffet and every disaster is a reunion.').
card_multiverse_id('moan of the unhallowed'/'ISD', '227286').

card_in_set('moldgraf monstrosity', 'ISD').
card_original_type('moldgraf monstrosity'/'ISD', 'Creature — Insect').
card_original_text('moldgraf monstrosity'/'ISD', 'Trample\nWhen Moldgraf Monstrosity dies, exile it, then return two creature cards at random from your graveyard to the battlefield.').
card_first_print('moldgraf monstrosity', 'ISD').
card_image_name('moldgraf monstrosity'/'ISD', 'moldgraf monstrosity').
card_uid('moldgraf monstrosity'/'ISD', 'ISD:Moldgraf Monstrosity:moldgraf monstrosity').
card_rarity('moldgraf monstrosity'/'ISD', 'Rare').
card_artist('moldgraf monstrosity'/'ISD', 'Tomasz Jedruszek').
card_number('moldgraf monstrosity'/'ISD', '194').
card_flavor_text('moldgraf monstrosity'/'ISD', 'The border between life and death is as thin as a layer of topsoil.').
card_multiverse_id('moldgraf monstrosity'/'ISD', '249664').

card_in_set('moment of heroism', 'ISD').
card_original_type('moment of heroism'/'ISD', 'Instant').
card_original_text('moment of heroism'/'ISD', 'Target creature gets +2/+2 and gains lifelink until end of turn. (Damage dealt by the creature also causes its controller to gain that much life.)').
card_first_print('moment of heroism', 'ISD').
card_image_name('moment of heroism'/'ISD', 'moment of heroism').
card_uid('moment of heroism'/'ISD', 'ISD:Moment of Heroism:moment of heroism').
card_rarity('moment of heroism'/'ISD', 'Common').
card_artist('moment of heroism'/'ISD', 'Christopher Moeller').
card_number('moment of heroism'/'ISD', '24').
card_flavor_text('moment of heroism'/'ISD', '\"My faith is stronger than fang, claw, or mindless hunger.\"').
card_multiverse_id('moment of heroism'/'ISD', '226882').

card_in_set('moon heron', 'ISD').
card_original_type('moon heron'/'ISD', 'Creature — Spirit Bird').
card_original_text('moon heron'/'ISD', 'Flying').
card_first_print('moon heron', 'ISD').
card_image_name('moon heron'/'ISD', 'moon heron').
card_uid('moon heron'/'ISD', 'ISD:Moon Heron:moon heron').
card_rarity('moon heron'/'ISD', 'Common').
card_artist('moon heron'/'ISD', 'Charles Urbach').
card_number('moon heron'/'ISD', '69').
card_flavor_text('moon heron'/'ISD', 'The heron is the symbol of the archangel Avacyn. The fact that such spirits still fly gives the church hope that its founder may one day return.').
card_multiverse_id('moon heron'/'ISD', '221187').

card_in_set('moonmist', 'ISD').
card_original_type('moonmist'/'ISD', 'Instant').
card_original_text('moonmist'/'ISD', 'Transform all Humans. Prevent all combat damage that would be dealt this turn by creatures other than Werewolves and Wolves. (Only double-faced cards can be transformed.)').
card_first_print('moonmist', 'ISD').
card_image_name('moonmist'/'ISD', 'moonmist').
card_uid('moonmist'/'ISD', 'ISD:Moonmist:moonmist').
card_rarity('moonmist'/'ISD', 'Common').
card_artist('moonmist'/'ISD', 'Ryan Yee').
card_number('moonmist'/'ISD', '195').
card_multiverse_id('moonmist'/'ISD', '222933').

card_in_set('moorland haunt', 'ISD').
card_original_type('moorland haunt'/'ISD', 'Land').
card_original_text('moorland haunt'/'ISD', '{T}: Add {1} to your mana pool.\n{W}{U}, {T}, Exile a creature card from your graveyard: Put a 1/1 white Spirit creature token with flying onto the battlefield.').
card_first_print('moorland haunt', 'ISD').
card_image_name('moorland haunt'/'ISD', 'moorland haunt').
card_uid('moorland haunt'/'ISD', 'ISD:Moorland Haunt:moorland haunt').
card_rarity('moorland haunt'/'ISD', 'Rare').
card_artist('moorland haunt'/'ISD', 'James Paick').
card_number('moorland haunt'/'ISD', '244').
card_multiverse_id('moorland haunt'/'ISD', '233239').

card_in_set('morkrut banshee', 'ISD').
card_original_type('morkrut banshee'/'ISD', 'Creature — Spirit').
card_original_text('morkrut banshee'/'ISD', 'Morbid — When Morkrut Banshee enters the battlefield, if a creature died this turn, target creature gets -4/-4 until end of turn.').
card_first_print('morkrut banshee', 'ISD').
card_image_name('morkrut banshee'/'ISD', 'morkrut banshee').
card_uid('morkrut banshee'/'ISD', 'ISD:Morkrut Banshee:morkrut banshee').
card_rarity('morkrut banshee'/'ISD', 'Uncommon').
card_artist('morkrut banshee'/'ISD', 'Svetlin Velinov').
card_number('morkrut banshee'/'ISD', '110').
card_flavor_text('morkrut banshee'/'ISD', '\"Let go your grudges, or risk wandering the bogs forever in a murderous rage.\"\n—Hildin, priest of Avacyn').
card_multiverse_id('morkrut banshee'/'ISD', '230777').

card_in_set('mountain', 'ISD').
card_original_type('mountain'/'ISD', 'Basic Land — Mountain').
card_original_text('mountain'/'ISD', 'R').
card_image_name('mountain'/'ISD', 'mountain1').
card_uid('mountain'/'ISD', 'ISD:Mountain:mountain1').
card_rarity('mountain'/'ISD', 'Basic Land').
card_artist('mountain'/'ISD', 'James Paick').
card_number('mountain'/'ISD', '259').
card_multiverse_id('mountain'/'ISD', '245242').

card_in_set('mountain', 'ISD').
card_original_type('mountain'/'ISD', 'Basic Land — Mountain').
card_original_text('mountain'/'ISD', 'R').
card_image_name('mountain'/'ISD', 'mountain2').
card_uid('mountain'/'ISD', 'ISD:Mountain:mountain2').
card_rarity('mountain'/'ISD', 'Basic Land').
card_artist('mountain'/'ISD', 'Adam Paquette').
card_number('mountain'/'ISD', '260').
card_multiverse_id('mountain'/'ISD', '245245').

card_in_set('mountain', 'ISD').
card_original_type('mountain'/'ISD', 'Basic Land — Mountain').
card_original_text('mountain'/'ISD', 'R').
card_image_name('mountain'/'ISD', 'mountain3').
card_uid('mountain'/'ISD', 'ISD:Mountain:mountain3').
card_rarity('mountain'/'ISD', 'Basic Land').
card_artist('mountain'/'ISD', 'Eytan Zana').
card_number('mountain'/'ISD', '261').
card_multiverse_id('mountain'/'ISD', '245243').

card_in_set('mulch', 'ISD').
card_original_type('mulch'/'ISD', 'Sorcery').
card_original_text('mulch'/'ISD', 'Reveal the top four cards of your library. Put all land cards revealed this way into your hand and the rest into your graveyard.').
card_image_name('mulch'/'ISD', 'mulch').
card_uid('mulch'/'ISD', 'ISD:Mulch:mulch').
card_rarity('mulch'/'ISD', 'Common').
card_artist('mulch'/'ISD', 'Christopher Moeller').
card_number('mulch'/'ISD', '196').
card_flavor_text('mulch'/'ISD', 'The land knows no difference between the graves of commoners and nobles.').
card_multiverse_id('mulch'/'ISD', '237012').

card_in_set('murder of crows', 'ISD').
card_original_type('murder of crows'/'ISD', 'Creature — Bird').
card_original_text('murder of crows'/'ISD', 'Flying\nWhenever another creature dies, you may draw a card. If you do, discard a card.').
card_first_print('murder of crows', 'ISD').
card_image_name('murder of crows'/'ISD', 'murder of crows').
card_uid('murder of crows'/'ISD', 'ISD:Murder of Crows:murder of crows').
card_rarity('murder of crows'/'ISD', 'Uncommon').
card_artist('murder of crows'/'ISD', 'Drew Baker').
card_number('murder of crows'/'ISD', '70').
card_flavor_text('murder of crows'/'ISD', 'Even more than carrion, they crave the last words of the dying.').
card_multiverse_id('murder of crows'/'ISD', '230616').

card_in_set('naturalize', 'ISD').
card_original_type('naturalize'/'ISD', 'Instant').
card_original_text('naturalize'/'ISD', 'Destroy target artifact or enchantment.').
card_image_name('naturalize'/'ISD', 'naturalize').
card_uid('naturalize'/'ISD', 'ISD:Naturalize:naturalize').
card_rarity('naturalize'/'ISD', 'Common').
card_artist('naturalize'/'ISD', 'Scott Chou').
card_number('naturalize'/'ISD', '197').
card_flavor_text('naturalize'/'ISD', '\"Stone and thought fade into ruin. Only the wind in the leaves will endure.\"\n—Tomb inscription').
card_multiverse_id('naturalize'/'ISD', '237015').

card_in_set('nephalia drownyard', 'ISD').
card_original_type('nephalia drownyard'/'ISD', 'Land').
card_original_text('nephalia drownyard'/'ISD', '{T}: Add {1} to your mana pool.\n{1}{U}{B}, {T}: Target player puts the top three cards of his or her library into his or her graveyard.').
card_first_print('nephalia drownyard', 'ISD').
card_image_name('nephalia drownyard'/'ISD', 'nephalia drownyard').
card_uid('nephalia drownyard'/'ISD', 'ISD:Nephalia Drownyard:nephalia drownyard').
card_rarity('nephalia drownyard'/'ISD', 'Rare').
card_artist('nephalia drownyard'/'ISD', 'Cliff Childs').
card_number('nephalia drownyard'/'ISD', '245').
card_flavor_text('nephalia drownyard'/'ISD', '\"Some see a coastal disaster. I see a fully-stocked seagraf.\"\n—Jadar, ghoulcaller of Nephalia').
card_multiverse_id('nephalia drownyard'/'ISD', '230789').

card_in_set('nevermore', 'ISD').
card_original_type('nevermore'/'ISD', 'Enchantment').
card_original_text('nevermore'/'ISD', 'As Nevermore enters the battlefield, name a nonland card.\nThe named card can\'t be cast.').
card_first_print('nevermore', 'ISD').
card_image_name('nevermore'/'ISD', 'nevermore').
card_uid('nevermore'/'ISD', 'ISD:Nevermore:nevermore').
card_rarity('nevermore'/'ISD', 'Rare').
card_artist('nevermore'/'ISD', 'Jason A. Engle').
card_number('nevermore'/'ISD', '25').
card_flavor_text('nevermore'/'ISD', '\"By the law of Avacyn, the following thoughts, words, and deeds are henceforth disallowed.\"').
card_multiverse_id('nevermore'/'ISD', '226878').

card_in_set('night revelers', 'ISD').
card_original_type('night revelers'/'ISD', 'Creature — Vampire').
card_original_text('night revelers'/'ISD', 'Night Revelers has haste as long as an opponent controls a Human.').
card_first_print('night revelers', 'ISD').
card_image_name('night revelers'/'ISD', 'night revelers').
card_uid('night revelers'/'ISD', 'ISD:Night Revelers:night revelers').
card_rarity('night revelers'/'ISD', 'Common').
card_artist('night revelers'/'ISD', 'Steve Argyle').
card_number('night revelers'/'ISD', '153').
card_flavor_text('night revelers'/'ISD', 'Vampires think of themselves as the indisputable nobility of Innistrad, able to do as they please. There are few who can prove them wrong.').
card_multiverse_id('night revelers'/'ISD', '226879').

card_in_set('night terrors', 'ISD').
card_original_type('night terrors'/'ISD', 'Sorcery').
card_original_text('night terrors'/'ISD', 'Target player reveals his or her hand. You choose a nonland card from it. Exile that card.').
card_first_print('night terrors', 'ISD').
card_image_name('night terrors'/'ISD', 'night terrors').
card_uid('night terrors'/'ISD', 'ISD:Night Terrors:night terrors').
card_rarity('night terrors'/'ISD', 'Common').
card_artist('night terrors'/'ISD', 'Christopher Moeller').
card_number('night terrors'/'ISD', '111').
card_flavor_text('night terrors'/'ISD', 'Innistrad is a place where dreams invade the mind and mere shadows exact a terrible toll.').
card_multiverse_id('night terrors'/'ISD', '246947').

card_in_set('nightbird\'s clutches', 'ISD').
card_original_type('nightbird\'s clutches'/'ISD', 'Sorcery').
card_original_text('nightbird\'s clutches'/'ISD', 'Up to two target creatures can\'t block this turn.\nFlashback {3}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('nightbird\'s clutches', 'ISD').
card_image_name('nightbird\'s clutches'/'ISD', 'nightbird\'s clutches').
card_uid('nightbird\'s clutches'/'ISD', 'ISD:Nightbird\'s Clutches:nightbird\'s clutches').
card_rarity('nightbird\'s clutches'/'ISD', 'Common').
card_artist('nightbird\'s clutches'/'ISD', 'Jason A. Engle').
card_number('nightbird\'s clutches'/'ISD', '154').
card_flavor_text('nightbird\'s clutches'/'ISD', 'Shiny polished silver can ward off many creatures, but it attracts others.').
card_multiverse_id('nightbird\'s clutches'/'ISD', '220386').

card_in_set('nightfall predator', 'ISD').
card_original_type('nightfall predator'/'ISD', 'Creature — Werewolf').
card_original_text('nightfall predator'/'ISD', '{R}, {T}: Nightfall Predator fights target creature. (Each deals damage equal to its power to the other.)\nAt the beginning of each upkeep, if a player cast two or more spells last turn, transform Nightfall Predator.').
card_first_print('nightfall predator', 'ISD').
card_image_name('nightfall predator'/'ISD', 'nightfall predator').
card_uid('nightfall predator'/'ISD', 'ISD:Nightfall Predator:nightfall predator').
card_rarity('nightfall predator'/'ISD', 'Rare').
card_artist('nightfall predator'/'ISD', 'Steve Prescott').
card_number('nightfall predator'/'ISD', '176b').
card_multiverse_id('nightfall predator'/'ISD', '222114').

card_in_set('olivia voldaren', 'ISD').
card_original_type('olivia voldaren'/'ISD', 'Legendary Creature — Vampire').
card_original_text('olivia voldaren'/'ISD', 'Flying\n{1}{R}: Olivia Voldaren deals 1 damage to another target creature. That creature becomes a Vampire in addition to its other types. Put a +1/+1 counter on Olivia Voldaren.\n{3}{B}{B}: Gain control of target Vampire for as long as you control Olivia Voldaren.').
card_first_print('olivia voldaren', 'ISD').
card_image_name('olivia voldaren'/'ISD', 'olivia voldaren').
card_uid('olivia voldaren'/'ISD', 'ISD:Olivia Voldaren:olivia voldaren').
card_rarity('olivia voldaren'/'ISD', 'Mythic Rare').
card_artist('olivia voldaren'/'ISD', 'Eric Deschamps').
card_number('olivia voldaren'/'ISD', '215').
card_multiverse_id('olivia voldaren'/'ISD', '247235').

card_in_set('one-eyed scarecrow', 'ISD').
card_original_type('one-eyed scarecrow'/'ISD', 'Artifact Creature — Scarecrow').
card_original_text('one-eyed scarecrow'/'ISD', 'Defender\nCreatures with flying your opponents control get -1/-0.').
card_first_print('one-eyed scarecrow', 'ISD').
card_image_name('one-eyed scarecrow'/'ISD', 'one-eyed scarecrow').
card_uid('one-eyed scarecrow'/'ISD', 'ISD:One-Eyed Scarecrow:one-eyed scarecrow').
card_rarity('one-eyed scarecrow'/'ISD', 'Common').
card_artist('one-eyed scarecrow'/'ISD', 'Dave Kendall').
card_number('one-eyed scarecrow'/'ISD', '230').
card_flavor_text('one-eyed scarecrow'/'ISD', 'Farmhands and priests mutter curses at the ragged thing; it unnerves more than just the crows.').
card_multiverse_id('one-eyed scarecrow'/'ISD', '220045').

card_in_set('orchard spirit', 'ISD').
card_original_type('orchard spirit'/'ISD', 'Creature — Spirit').
card_original_text('orchard spirit'/'ISD', 'Orchard Spirit can\'t be blocked except by creatures with flying or reach.').
card_first_print('orchard spirit', 'ISD').
card_image_name('orchard spirit'/'ISD', 'orchard spirit').
card_uid('orchard spirit'/'ISD', 'ISD:Orchard Spirit:orchard spirit').
card_rarity('orchard spirit'/'ISD', 'Common').
card_artist('orchard spirit'/'ISD', 'Howard Lyon').
card_number('orchard spirit'/'ISD', '198').
card_flavor_text('orchard spirit'/'ISD', '\"Pick not the rotten fruit, but neither touch the best. Leave those as an offering for our unseen guests.\"\n—Radwick, farmer of Gatstaf').
card_multiverse_id('orchard spirit'/'ISD', '234855').

card_in_set('parallel lives', 'ISD').
card_original_type('parallel lives'/'ISD', 'Enchantment').
card_original_text('parallel lives'/'ISD', 'If an effect would put one or more tokens onto the battlefield under your control, it puts twice that many of those tokens onto the battlefield instead.').
card_first_print('parallel lives', 'ISD').
card_image_name('parallel lives'/'ISD', 'parallel lives').
card_uid('parallel lives'/'ISD', 'ISD:Parallel Lives:parallel lives').
card_rarity('parallel lives'/'ISD', 'Rare').
card_artist('parallel lives'/'ISD', 'Steve Prescott').
card_number('parallel lives'/'ISD', '199').
card_flavor_text('parallel lives'/'ISD', '\"There will come a time when the only prey left will be each other.\"\n—Ulrich of Krallenhorde Pack').
card_multiverse_id('parallel lives'/'ISD', '249662').

card_in_set('paraselene', 'ISD').
card_original_type('paraselene'/'ISD', 'Sorcery').
card_original_text('paraselene'/'ISD', 'Destroy all enchantments. You gain 1 life for each enchantment destroyed this way.').
card_first_print('paraselene', 'ISD').
card_image_name('paraselene'/'ISD', 'paraselene').
card_uid('paraselene'/'ISD', 'ISD:Paraselene:paraselene').
card_rarity('paraselene'/'ISD', 'Uncommon').
card_artist('paraselene'/'ISD', 'Ryan Yee').
card_number('paraselene'/'ISD', '26').
card_flavor_text('paraselene'/'ISD', '\"Moonlight has a way of showing all things as they truly are—for better or for worse.\"\n—Zilla of Lambholt').
card_multiverse_id('paraselene'/'ISD', '249975').

card_in_set('past in flames', 'ISD').
card_original_type('past in flames'/'ISD', 'Sorcery').
card_original_text('past in flames'/'ISD', 'Each instant and sorcery card in your graveyard gains flashback until end of turn. The flashback cost is equal to its mana cost.\nFlashback {4}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('past in flames', 'ISD').
card_image_name('past in flames'/'ISD', 'past in flames').
card_uid('past in flames'/'ISD', 'ISD:Past in Flames:past in flames').
card_rarity('past in flames'/'ISD', 'Mythic Rare').
card_artist('past in flames'/'ISD', 'Anthony Jones').
card_number('past in flames'/'ISD', '155').
card_multiverse_id('past in flames'/'ISD', '245197').

card_in_set('pitchburn devils', 'ISD').
card_original_type('pitchburn devils'/'ISD', 'Creature — Devil').
card_original_text('pitchburn devils'/'ISD', 'When Pitchburn Devils dies, it deals 3 damage to target creature or player.').
card_first_print('pitchburn devils', 'ISD').
card_image_name('pitchburn devils'/'ISD', 'pitchburn devils').
card_uid('pitchburn devils'/'ISD', 'ISD:Pitchburn Devils:pitchburn devils').
card_rarity('pitchburn devils'/'ISD', 'Common').
card_artist('pitchburn devils'/'ISD', 'Johann Bodin').
card_number('pitchburn devils'/'ISD', '156').
card_flavor_text('pitchburn devils'/'ISD', 'The ingenuity of goblins, the depravity of demons, and the smarts of sheep.').
card_multiverse_id('pitchburn devils'/'ISD', '222904').

card_in_set('plains', 'ISD').
card_original_type('plains'/'ISD', 'Basic Land — Plains').
card_original_text('plains'/'ISD', 'W').
card_image_name('plains'/'ISD', 'plains1').
card_uid('plains'/'ISD', 'ISD:Plains:plains1').
card_rarity('plains'/'ISD', 'Basic Land').
card_artist('plains'/'ISD', 'Adam Paquette').
card_number('plains'/'ISD', '250').
card_multiverse_id('plains'/'ISD', '245230').

card_in_set('plains', 'ISD').
card_original_type('plains'/'ISD', 'Basic Land — Plains').
card_original_text('plains'/'ISD', 'W').
card_image_name('plains'/'ISD', 'plains2').
card_uid('plains'/'ISD', 'ISD:Plains:plains2').
card_rarity('plains'/'ISD', 'Basic Land').
card_artist('plains'/'ISD', 'Jung Park').
card_number('plains'/'ISD', '251').
card_multiverse_id('plains'/'ISD', '245233').

card_in_set('plains', 'ISD').
card_original_type('plains'/'ISD', 'Basic Land — Plains').
card_original_text('plains'/'ISD', 'W').
card_image_name('plains'/'ISD', 'plains3').
card_uid('plains'/'ISD', 'ISD:Plains:plains3').
card_rarity('plains'/'ISD', 'Basic Land').
card_artist('plains'/'ISD', 'Eytan Zana').
card_number('plains'/'ISD', '252').
card_multiverse_id('plains'/'ISD', '245231').

card_in_set('prey upon', 'ISD').
card_original_type('prey upon'/'ISD', 'Sorcery').
card_original_text('prey upon'/'ISD', 'Target creature you control fights target creature you don\'t control. (Each deals damage equal to its power to the other.)').
card_first_print('prey upon', 'ISD').
card_image_name('prey upon'/'ISD', 'prey upon').
card_uid('prey upon'/'ISD', 'ISD:Prey Upon:prey upon').
card_rarity('prey upon'/'ISD', 'Common').
card_artist('prey upon'/'ISD', 'Dave Kendall').
card_number('prey upon'/'ISD', '200').
card_flavor_text('prey upon'/'ISD', '\"You don\'t find many old werewolf hunters.\"\n—Paulin, trapper of Somberwald').
card_multiverse_id('prey upon'/'ISD', '220383').

card_in_set('purify the grave', 'ISD').
card_original_type('purify the grave'/'ISD', 'Instant').
card_original_text('purify the grave'/'ISD', 'Exile target card from a graveyard.\nFlashback {W} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('purify the grave', 'ISD').
card_image_name('purify the grave'/'ISD', 'purify the grave').
card_uid('purify the grave'/'ISD', 'ISD:Purify the Grave:purify the grave').
card_rarity('purify the grave'/'ISD', 'Uncommon').
card_artist('purify the grave'/'ISD', 'Drew Baker').
card_number('purify the grave'/'ISD', '27').
card_flavor_text('purify the grave'/'ISD', 'Some priests speak in whispers of the Helvault, a silver prison of unpurged evils.').
card_multiverse_id('purify the grave'/'ISD', '226884').

card_in_set('rage thrower', 'ISD').
card_original_type('rage thrower'/'ISD', 'Creature — Human Shaman').
card_original_text('rage thrower'/'ISD', 'Whenever another creature dies, Rage Thrower deals 2 damage to target player.').
card_first_print('rage thrower', 'ISD').
card_image_name('rage thrower'/'ISD', 'rage thrower').
card_uid('rage thrower'/'ISD', 'ISD:Rage Thrower:rage thrower').
card_rarity('rage thrower'/'ISD', 'Uncommon').
card_artist('rage thrower'/'ISD', 'Peter Mohrbacher').
card_number('rage thrower'/'ISD', '157').
card_flavor_text('rage thrower'/'ISD', '\"Some lament these haunted times, but I\'m a geistflame-tank-half-full kind of person.\"').
card_multiverse_id('rage thrower'/'ISD', '222921').

card_in_set('rakish heir', 'ISD').
card_original_type('rakish heir'/'ISD', 'Creature — Vampire').
card_original_text('rakish heir'/'ISD', 'Whenever a Vampire you control deals combat damage to a player, put a +1/+1 counter on it.').
card_first_print('rakish heir', 'ISD').
card_image_name('rakish heir'/'ISD', 'rakish heir').
card_uid('rakish heir'/'ISD', 'ISD:Rakish Heir:rakish heir').
card_rarity('rakish heir'/'ISD', 'Uncommon').
card_artist('rakish heir'/'ISD', 'Winona Nelson').
card_number('rakish heir'/'ISD', '158').
card_flavor_text('rakish heir'/'ISD', '\"If you\'re not having fun, what\'s the point of living forever?\"').
card_multiverse_id('rakish heir'/'ISD', '234441').

card_in_set('rally the peasants', 'ISD').
card_original_type('rally the peasants'/'ISD', 'Instant').
card_original_text('rally the peasants'/'ISD', 'Creatures you control get +2/+0 until end of turn.\nFlashback {2}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('rally the peasants', 'ISD').
card_image_name('rally the peasants'/'ISD', 'rally the peasants').
card_uid('rally the peasants'/'ISD', 'ISD:Rally the Peasants:rally the peasants').
card_rarity('rally the peasants'/'ISD', 'Uncommon').
card_artist('rally the peasants'/'ISD', 'Jaime Jones').
card_number('rally the peasants'/'ISD', '28').
card_flavor_text('rally the peasants'/'ISD', '\"If you must go out at night, bring a mob.\"\n—Master of the Elgaud Cathars').
card_multiverse_id('rally the peasants'/'ISD', '237361').

card_in_set('rampaging werewolf', 'ISD').
card_original_type('rampaging werewolf'/'ISD', 'Creature — Werewolf').
card_original_text('rampaging werewolf'/'ISD', 'At the beginning of each upkeep, if a player cast two or more spells last turn, transform Rampaging Werewolf.').
card_first_print('rampaging werewolf', 'ISD').
card_image_name('rampaging werewolf'/'ISD', 'rampaging werewolf').
card_uid('rampaging werewolf'/'ISD', 'ISD:Rampaging Werewolf:rampaging werewolf').
card_rarity('rampaging werewolf'/'ISD', 'Common').
card_artist('rampaging werewolf'/'ISD', 'Bud Cook').
card_number('rampaging werewolf'/'ISD', '165b').
card_flavor_text('rampaging werewolf'/'ISD', '\"Just look at him, groveling on all fours! What a pathetic—uh-oh.\"').
card_multiverse_id('rampaging werewolf'/'ISD', '222117').

card_in_set('ranger\'s guile', 'ISD').
card_original_type('ranger\'s guile'/'ISD', 'Instant').
card_original_text('ranger\'s guile'/'ISD', 'Target creature you control gets +1/+1 and gains hexproof until end of turn. (It can\'t be the target of spells or abilities your opponents control.)').
card_first_print('ranger\'s guile', 'ISD').
card_image_name('ranger\'s guile'/'ISD', 'ranger\'s guile').
card_uid('ranger\'s guile'/'ISD', 'ISD:Ranger\'s Guile:ranger\'s guile').
card_rarity('ranger\'s guile'/'ISD', 'Common').
card_artist('ranger\'s guile'/'ISD', 'Steve Prescott').
card_number('ranger\'s guile'/'ISD', '201').
card_flavor_text('ranger\'s guile'/'ISD', 'In the remote corners of Kessig, protection is found in secret powers unknown to the Church of Avacyn.').
card_multiverse_id('ranger\'s guile'/'ISD', '249973').

card_in_set('reaper from the abyss', 'ISD').
card_original_type('reaper from the abyss'/'ISD', 'Creature — Demon').
card_original_text('reaper from the abyss'/'ISD', 'Flying\nMorbid — At the beginning of each end step, if a creature died this turn, destroy target non-Demon creature.').
card_first_print('reaper from the abyss', 'ISD').
card_image_name('reaper from the abyss'/'ISD', 'reaper from the abyss').
card_uid('reaper from the abyss'/'ISD', 'ISD:Reaper from the Abyss:reaper from the abyss').
card_rarity('reaper from the abyss'/'ISD', 'Mythic Rare').
card_artist('reaper from the abyss'/'ISD', 'Matt Stewart').
card_number('reaper from the abyss'/'ISD', '112').
card_flavor_text('reaper from the abyss'/'ISD', '\"Avacyn has deserted you. I welcome your devotion in her stead.\"').
card_multiverse_id('reaper from the abyss'/'ISD', '230768').

card_in_set('rebuke', 'ISD').
card_original_type('rebuke'/'ISD', 'Instant').
card_original_text('rebuke'/'ISD', 'Destroy target attacking creature.').
card_first_print('rebuke', 'ISD').
card_image_name('rebuke'/'ISD', 'rebuke').
card_uid('rebuke'/'ISD', 'ISD:Rebuke:rebuke').
card_rarity('rebuke'/'ISD', 'Common').
card_artist('rebuke'/'ISD', 'Igor Kieryluk').
card_number('rebuke'/'ISD', '29').
card_flavor_text('rebuke'/'ISD', '\"No one chooses to be stitched together into a skaab. If it could speak, it would beg for its own destruction.\"\n—Thalia, Knight-Cathar').
card_multiverse_id('rebuke'/'ISD', '247423').

card_in_set('reckless waif', 'ISD').
card_original_type('reckless waif'/'ISD', 'Creature — Human Rogue Werewolf').
card_original_text('reckless waif'/'ISD', 'At the beginning of each upkeep, if no spells were cast last turn, transform Reckless Waif.').
card_first_print('reckless waif', 'ISD').
card_image_name('reckless waif'/'ISD', 'reckless waif').
card_uid('reckless waif'/'ISD', 'ISD:Reckless Waif:reckless waif').
card_rarity('reckless waif'/'ISD', 'Uncommon').
card_artist('reckless waif'/'ISD', 'Michael C. Hayes').
card_number('reckless waif'/'ISD', '159a').
card_flavor_text('reckless waif'/'ISD', '\"Yes, I\'m alone. No, I\'m not worried.\"').
card_multiverse_id('reckless waif'/'ISD', '222111').

card_in_set('riot devils', 'ISD').
card_original_type('riot devils'/'ISD', 'Creature — Devil').
card_original_text('riot devils'/'ISD', '').
card_first_print('riot devils', 'ISD').
card_image_name('riot devils'/'ISD', 'riot devils').
card_uid('riot devils'/'ISD', 'ISD:Riot Devils:riot devils').
card_rarity('riot devils'/'ISD', 'Common').
card_artist('riot devils'/'ISD', 'Svetlin Velinov').
card_number('riot devils'/'ISD', '160').
card_flavor_text('riot devils'/'ISD', 'Devils are demons\' unearthly desires made flesh. Let loose on the world, they satisfy their mad craving for destruction in a whirlwind of chaos.').
card_multiverse_id('riot devils'/'ISD', '220648').

card_in_set('rolling temblor', 'ISD').
card_original_type('rolling temblor'/'ISD', 'Sorcery').
card_original_text('rolling temblor'/'ISD', 'Rolling Temblor deals 2 damage to each creature without flying.\nFlashback {4}{R}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('rolling temblor', 'ISD').
card_image_name('rolling temblor'/'ISD', 'rolling temblor').
card_uid('rolling temblor'/'ISD', 'ISD:Rolling Temblor:rolling temblor').
card_rarity('rolling temblor'/'ISD', 'Uncommon').
card_artist('rolling temblor'/'ISD', 'Cliff Childs').
card_number('rolling temblor'/'ISD', '161').
card_flavor_text('rolling temblor'/'ISD', 'When demons open a path to your front door, it\'s time to move.').
card_multiverse_id('rolling temblor'/'ISD', '249660').

card_in_set('rooftop storm', 'ISD').
card_original_type('rooftop storm'/'ISD', 'Enchantment').
card_original_text('rooftop storm'/'ISD', 'You may pay {0} rather than pay the mana cost for Zombie creature spells you cast.').
card_first_print('rooftop storm', 'ISD').
card_image_name('rooftop storm'/'ISD', 'rooftop storm').
card_uid('rooftop storm'/'ISD', 'ISD:Rooftop Storm:rooftop storm').
card_rarity('rooftop storm'/'ISD', 'Rare').
card_artist('rooftop storm'/'ISD', 'John Stanko').
card_number('rooftop storm'/'ISD', '71').
card_flavor_text('rooftop storm'/'ISD', '\"Let those idiot priests tremble! A new era in unlife begins here and now. Oglor, raise the lightning vane!\"\n—Stitcher Geralf').
card_multiverse_id('rooftop storm'/'ISD', '227287').

card_in_set('rotting fensnake', 'ISD').
card_original_type('rotting fensnake'/'ISD', 'Creature — Zombie Snake').
card_original_text('rotting fensnake'/'ISD', '').
card_first_print('rotting fensnake', 'ISD').
card_image_name('rotting fensnake'/'ISD', 'rotting fensnake').
card_uid('rotting fensnake'/'ISD', 'ISD:Rotting Fensnake:rotting fensnake').
card_rarity('rotting fensnake'/'ISD', 'Common').
card_artist('rotting fensnake'/'ISD', 'Tomasz Jedruszek').
card_number('rotting fensnake'/'ISD', '113').
card_flavor_text('rotting fensnake'/'ISD', 'Its venom sacs are long gone, but its crushing strength is somehow greater than ever.').
card_multiverse_id('rotting fensnake'/'ISD', '237011').

card_in_set('runechanter\'s pike', 'ISD').
card_original_type('runechanter\'s pike'/'ISD', 'Artifact — Equipment').
card_original_text('runechanter\'s pike'/'ISD', 'Equipped creature has first strike and gets +X/+0, where X is the number of instant and sorcery cards in your graveyard.\nEquip {2}').
card_first_print('runechanter\'s pike', 'ISD').
card_image_name('runechanter\'s pike'/'ISD', 'runechanter\'s pike').
card_uid('runechanter\'s pike'/'ISD', 'ISD:Runechanter\'s Pike:runechanter\'s pike').
card_rarity('runechanter\'s pike'/'ISD', 'Rare').
card_artist('runechanter\'s pike'/'ISD', 'John Avon').
card_number('runechanter\'s pike'/'ISD', '231').
card_flavor_text('runechanter\'s pike'/'ISD', 'As sharp as its bearer\'s words of faith.').
card_multiverse_id('runechanter\'s pike'/'ISD', '244681').

card_in_set('runic repetition', 'ISD').
card_original_type('runic repetition'/'ISD', 'Sorcery').
card_original_text('runic repetition'/'ISD', 'Return target exiled card with flashback you own to your hand.').
card_first_print('runic repetition', 'ISD').
card_image_name('runic repetition'/'ISD', 'runic repetition').
card_uid('runic repetition'/'ISD', 'ISD:Runic Repetition:runic repetition').
card_rarity('runic repetition'/'ISD', 'Uncommon').
card_artist('runic repetition'/'ISD', 'Svetlin Velinov').
card_number('runic repetition'/'ISD', '72').
card_flavor_text('runic repetition'/'ISD', '\"What some call obsession, I call the quest for perfection.\"').
card_multiverse_id('runic repetition'/'ISD', '227298').

card_in_set('scourge of geier reach', 'ISD').
card_original_type('scourge of geier reach'/'ISD', 'Creature — Elemental').
card_original_text('scourge of geier reach'/'ISD', 'Scourge of Geier Reach gets +1/+1 for each creature your opponents control.').
card_first_print('scourge of geier reach', 'ISD').
card_image_name('scourge of geier reach'/'ISD', 'scourge of geier reach').
card_uid('scourge of geier reach'/'ISD', 'ISD:Scourge of Geier Reach:scourge of geier reach').
card_rarity('scourge of geier reach'/'ISD', 'Uncommon').
card_artist('scourge of geier reach'/'ISD', 'Jung Park').
card_number('scourge of geier reach'/'ISD', '162').
card_flavor_text('scourge of geier reach'/'ISD', 'Stensian villagers mourned the loss of human life. Thraben vampire slayers mourned the loss of living wood.').
card_multiverse_id('scourge of geier reach'/'ISD', '227077').

card_in_set('screeching bat', 'ISD').
card_original_type('screeching bat'/'ISD', 'Creature — Bat').
card_original_text('screeching bat'/'ISD', 'Flying\nAt the beginning of your upkeep, you may pay {2}{B}{B}. If you do, transform Screeching Bat.').
card_first_print('screeching bat', 'ISD').
card_image_name('screeching bat'/'ISD', 'screeching bat').
card_uid('screeching bat'/'ISD', 'ISD:Screeching Bat:screeching bat').
card_rarity('screeching bat'/'ISD', 'Uncommon').
card_artist('screeching bat'/'ISD', 'Slawomir Maniak').
card_number('screeching bat'/'ISD', '114a').
card_flavor_text('screeching bat'/'ISD', 'The bat has such clarity of hearing that simple sounds become symphonies.').
card_multiverse_id('screeching bat'/'ISD', '221211').

card_in_set('selfless cathar', 'ISD').
card_original_type('selfless cathar'/'ISD', 'Creature — Human Cleric').
card_original_text('selfless cathar'/'ISD', '{1}{W}, Sacrifice Selfless Cathar: Creatures you control get +1/+1 until end of turn.').
card_first_print('selfless cathar', 'ISD').
card_image_name('selfless cathar'/'ISD', 'selfless cathar').
card_uid('selfless cathar'/'ISD', 'ISD:Selfless Cathar:selfless cathar').
card_rarity('selfless cathar'/'ISD', 'Common').
card_artist('selfless cathar'/'ISD', 'Slawomir Maniak').
card_number('selfless cathar'/'ISD', '30').
card_flavor_text('selfless cathar'/'ISD', '\"If I fail to offer myself, we will surely be overrun. My fate would be the same.\"').
card_multiverse_id('selfless cathar'/'ISD', '221219').

card_in_set('selhoff occultist', 'ISD').
card_original_type('selhoff occultist'/'ISD', 'Creature — Human Rogue').
card_original_text('selhoff occultist'/'ISD', 'Whenever Selhoff Occultist or another creature dies, target player puts the top card of his or her library into his or her graveyard.').
card_first_print('selhoff occultist', 'ISD').
card_image_name('selhoff occultist'/'ISD', 'selhoff occultist').
card_uid('selhoff occultist'/'ISD', 'ISD:Selhoff Occultist:selhoff occultist').
card_rarity('selhoff occultist'/'ISD', 'Common').
card_artist('selhoff occultist'/'ISD', 'Igor Kieryluk').
card_number('selhoff occultist'/'ISD', '73').
card_flavor_text('selhoff occultist'/'ISD', '\"The mist whispers to those who know it to be more than a mundane fog.\"').
card_multiverse_id('selhoff occultist'/'ISD', '233254').

card_in_set('sensory deprivation', 'ISD').
card_original_type('sensory deprivation'/'ISD', 'Enchantment — Aura').
card_original_text('sensory deprivation'/'ISD', 'Enchant creature\nEnchanted creature gets -3/-0.').
card_first_print('sensory deprivation', 'ISD').
card_image_name('sensory deprivation'/'ISD', 'sensory deprivation').
card_uid('sensory deprivation'/'ISD', 'ISD:Sensory Deprivation:sensory deprivation').
card_rarity('sensory deprivation'/'ISD', 'Common').
card_artist('sensory deprivation'/'ISD', 'Steven Belledin').
card_number('sensory deprivation'/'ISD', '74').
card_flavor_text('sensory deprivation'/'ISD', 'They call it \"stitcher\'s anesthesia,\" a spell to deaden the senses while the mad doctors begin their grisly work.').
card_multiverse_id('sensory deprivation'/'ISD', '222014').

card_in_set('sever the bloodline', 'ISD').
card_original_type('sever the bloodline'/'ISD', 'Sorcery').
card_original_text('sever the bloodline'/'ISD', 'Exile target creature and all other creatures with the same name as that creature.\nFlashback {5}{B}{B} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('sever the bloodline', 'ISD').
card_image_name('sever the bloodline'/'ISD', 'sever the bloodline').
card_uid('sever the bloodline'/'ISD', 'ISD:Sever the Bloodline:sever the bloodline').
card_rarity('sever the bloodline'/'ISD', 'Rare').
card_artist('sever the bloodline'/'ISD', 'Clint Cearley').
card_number('sever the bloodline'/'ISD', '115').
card_multiverse_id('sever the bloodline'/'ISD', '245194').

card_in_set('sharpened pitchfork', 'ISD').
card_original_type('sharpened pitchfork'/'ISD', 'Artifact — Equipment').
card_original_text('sharpened pitchfork'/'ISD', 'Equipped creature has first strike.\nAs long as equipped creature is a Human, it gets +1/+1.\nEquip {1}').
card_first_print('sharpened pitchfork', 'ISD').
card_image_name('sharpened pitchfork'/'ISD', 'sharpened pitchfork').
card_uid('sharpened pitchfork'/'ISD', 'ISD:Sharpened Pitchfork:sharpened pitchfork').
card_rarity('sharpened pitchfork'/'ISD', 'Uncommon').
card_artist('sharpened pitchfork'/'ISD', 'Winona Nelson').
card_number('sharpened pitchfork'/'ISD', '232').
card_flavor_text('sharpened pitchfork'/'ISD', 'Not everyone can have a sword of blessed silver. Not everyone needs one, either.').
card_multiverse_id('sharpened pitchfork'/'ISD', '220642').

card_in_set('shimmering grotto', 'ISD').
card_original_type('shimmering grotto'/'ISD', 'Land').
card_original_text('shimmering grotto'/'ISD', '{T}: Add {1} to your mana pool.\n{1}, {T}: Add one mana of any color to your mana pool.').
card_image_name('shimmering grotto'/'ISD', 'shimmering grotto').
card_uid('shimmering grotto'/'ISD', 'ISD:Shimmering Grotto:shimmering grotto').
card_rarity('shimmering grotto'/'ISD', 'Common').
card_artist('shimmering grotto'/'ISD', 'Cliff Childs').
card_number('shimmering grotto'/'ISD', '246').
card_flavor_text('shimmering grotto'/'ISD', '\"This is the price of being last in the food chain. We must keep our places of beauty hidden away instead of displayed for all to see.\"\n—Hildin, priest of Avacyn').
card_multiverse_id('shimmering grotto'/'ISD', '243216').

card_in_set('silent departure', 'ISD').
card_original_type('silent departure'/'ISD', 'Sorcery').
card_original_text('silent departure'/'ISD', 'Return target creature to its owner\'s hand.\nFlashback {4}{U} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('silent departure', 'ISD').
card_image_name('silent departure'/'ISD', 'silent departure').
card_uid('silent departure'/'ISD', 'ISD:Silent Departure:silent departure').
card_rarity('silent departure'/'ISD', 'Common').
card_artist('silent departure'/'ISD', 'John Avon').
card_number('silent departure'/'ISD', '75').
card_flavor_text('silent departure'/'ISD', 'The same magic that lets geists pass through our realm can force them out of it.').
card_multiverse_id('silent departure'/'ISD', '220047').

card_in_set('silver-inlaid dagger', 'ISD').
card_original_type('silver-inlaid dagger'/'ISD', 'Artifact — Equipment').
card_original_text('silver-inlaid dagger'/'ISD', 'Equipped creature gets +2/+0.\nAs long as equipped creature is a Human, it gets an additional +1/+0.\nEquip {2}').
card_first_print('silver-inlaid dagger', 'ISD').
card_image_name('silver-inlaid dagger'/'ISD', 'silver-inlaid dagger').
card_uid('silver-inlaid dagger'/'ISD', 'ISD:Silver-Inlaid Dagger:silver-inlaid dagger').
card_rarity('silver-inlaid dagger'/'ISD', 'Uncommon').
card_artist('silver-inlaid dagger'/'ISD', 'Austin Hsu').
card_number('silver-inlaid dagger'/'ISD', '233').
card_flavor_text('silver-inlaid dagger'/'ISD', 'Magical etchings turned a whittler\'s tool into a wolfhunter\'s weapon.').
card_multiverse_id('silver-inlaid dagger'/'ISD', '222190').

card_in_set('silverchase fox', 'ISD').
card_original_type('silverchase fox'/'ISD', 'Creature — Fox').
card_original_text('silverchase fox'/'ISD', '{1}{W}, Sacrifice Silverchase Fox: Exile target enchantment.').
card_first_print('silverchase fox', 'ISD').
card_image_name('silverchase fox'/'ISD', 'silverchase fox').
card_uid('silverchase fox'/'ISD', 'ISD:Silverchase Fox:silverchase fox').
card_rarity('silverchase fox'/'ISD', 'Common').
card_artist('silverchase fox'/'ISD', 'Howard Lyon').
card_number('silverchase fox'/'ISD', '31').
card_flavor_text('silverchase fox'/'ISD', '\"Be careful in your hunt. The clever little creature can steal your wards and lead you right into the werewolf\'s den.\"\n—Britta, midwife of Gatstaf').
card_multiverse_id('silverchase fox'/'ISD', '234848').

card_in_set('skaab goliath', 'ISD').
card_original_type('skaab goliath'/'ISD', 'Creature — Zombie Giant').
card_original_text('skaab goliath'/'ISD', 'As an additional cost to cast Skaab Goliath, exile two creature cards from your graveyard.\nTrample').
card_first_print('skaab goliath', 'ISD').
card_image_name('skaab goliath'/'ISD', 'skaab goliath').
card_uid('skaab goliath'/'ISD', 'ISD:Skaab Goliath:skaab goliath').
card_rarity('skaab goliath'/'ISD', 'Uncommon').
card_artist('skaab goliath'/'ISD', 'Volkan Baga').
card_number('skaab goliath'/'ISD', '76').
card_flavor_text('skaab goliath'/'ISD', '\"Three heads, six arms, and some armor grafts are better than . . . the normal numbers of those things.\"\n—Stitcher Geralf').
card_multiverse_id('skaab goliath'/'ISD', '222913').

card_in_set('skaab ruinator', 'ISD').
card_original_type('skaab ruinator'/'ISD', 'Creature — Zombie Horror').
card_original_text('skaab ruinator'/'ISD', 'As an additional cost to cast Skaab Ruinator, exile three creature cards from your graveyard.\nFlying\nYou may cast Skaab Ruinator from your graveyard.').
card_first_print('skaab ruinator', 'ISD').
card_image_name('skaab ruinator'/'ISD', 'skaab ruinator').
card_uid('skaab ruinator'/'ISD', 'ISD:Skaab Ruinator:skaab ruinator').
card_rarity('skaab ruinator'/'ISD', 'Mythic Rare').
card_artist('skaab ruinator'/'ISD', 'Chris Rahn').
card_number('skaab ruinator'/'ISD', '77').
card_multiverse_id('skaab ruinator'/'ISD', '230780').

card_in_set('skeletal grimace', 'ISD').
card_original_type('skeletal grimace'/'ISD', 'Enchantment — Aura').
card_original_text('skeletal grimace'/'ISD', 'Enchant creature\nEnchanted creature gets +1/+1 and has \"{B}: Regenerate this creature.\"').
card_first_print('skeletal grimace', 'ISD').
card_image_name('skeletal grimace'/'ISD', 'skeletal grimace').
card_uid('skeletal grimace'/'ISD', 'ISD:Skeletal Grimace:skeletal grimace').
card_rarity('skeletal grimace'/'ISD', 'Common').
card_artist('skeletal grimace'/'ISD', 'Eric Deschamps').
card_number('skeletal grimace'/'ISD', '116').
card_flavor_text('skeletal grimace'/'ISD', '\"And then I realized we all have skeletons on the inside. Why wait until the bodies are dead to control them?\"\n—Jadar, ghoulcaller of Nephalia').
card_multiverse_id('skeletal grimace'/'ISD', '220387').

card_in_set('skirsdag cultist', 'ISD').
card_original_type('skirsdag cultist'/'ISD', 'Creature — Human Shaman').
card_original_text('skirsdag cultist'/'ISD', '{R}, {T}, Sacrifice a creature: Skirsdag Cultist deals 2 damage to target creature or player.').
card_first_print('skirsdag cultist', 'ISD').
card_image_name('skirsdag cultist'/'ISD', 'skirsdag cultist').
card_uid('skirsdag cultist'/'ISD', 'ISD:Skirsdag Cultist:skirsdag cultist').
card_rarity('skirsdag cultist'/'ISD', 'Uncommon').
card_artist('skirsdag cultist'/'ISD', 'Slawomir Maniak').
card_number('skirsdag cultist'/'ISD', '163').
card_flavor_text('skirsdag cultist'/'ISD', '\"Within blood is life. Within life is fire. Within fire is the path to our masters\' glory!\"').
card_multiverse_id('skirsdag cultist'/'ISD', '220638').

card_in_set('skirsdag high priest', 'ISD').
card_original_type('skirsdag high priest'/'ISD', 'Creature — Human Cleric').
card_original_text('skirsdag high priest'/'ISD', 'Morbid — {T}, Tap two untapped creatures you control: Put a 5/5 black Demon creature token with flying onto the battlefield. Activate this ability only if a creature died this turn.').
card_first_print('skirsdag high priest', 'ISD').
card_image_name('skirsdag high priest'/'ISD', 'skirsdag high priest').
card_uid('skirsdag high priest'/'ISD', 'ISD:Skirsdag High Priest:skirsdag high priest').
card_rarity('skirsdag high priest'/'ISD', 'Rare').
card_artist('skirsdag high priest'/'ISD', 'Jason A. Engle').
card_number('skirsdag high priest'/'ISD', '117').
card_flavor_text('skirsdag high priest'/'ISD', '\"Thraben\'s pleas fall on deaf ears. Ours do not.\"').
card_multiverse_id('skirsdag high priest'/'ISD', '230794').

card_in_set('slayer of the wicked', 'ISD').
card_original_type('slayer of the wicked'/'ISD', 'Creature — Human Soldier').
card_original_text('slayer of the wicked'/'ISD', 'When Slayer of the Wicked enters the battlefield, you may destroy target Vampire, Werewolf, or Zombie.').
card_first_print('slayer of the wicked', 'ISD').
card_image_name('slayer of the wicked'/'ISD', 'slayer of the wicked').
card_uid('slayer of the wicked'/'ISD', 'ISD:Slayer of the Wicked:slayer of the wicked').
card_rarity('slayer of the wicked'/'ISD', 'Uncommon').
card_artist('slayer of the wicked'/'ISD', 'Anthony Palumbo').
card_number('slayer of the wicked'/'ISD', '32').
card_flavor_text('slayer of the wicked'/'ISD', 'You don\'t want to see his trophy room.').
card_multiverse_id('slayer of the wicked'/'ISD', '220062').

card_in_set('smite the monstrous', 'ISD').
card_original_type('smite the monstrous'/'ISD', 'Instant').
card_original_text('smite the monstrous'/'ISD', 'Destroy target creature with power 4 or greater.').
card_first_print('smite the monstrous', 'ISD').
card_image_name('smite the monstrous'/'ISD', 'smite the monstrous').
card_uid('smite the monstrous'/'ISD', 'ISD:Smite the Monstrous:smite the monstrous').
card_rarity('smite the monstrous'/'ISD', 'Common').
card_artist('smite the monstrous'/'ISD', 'Jason Felix').
card_number('smite the monstrous'/'ISD', '33').
card_flavor_text('smite the monstrous'/'ISD', 'Though the old holy wards and roadside shrines have begun to fail, faith in Avacyn still holds true power.').
card_multiverse_id('smite the monstrous'/'ISD', '220377').

card_in_set('snapcaster mage', 'ISD').
card_original_type('snapcaster mage'/'ISD', 'Creature — Human Wizard').
card_original_text('snapcaster mage'/'ISD', 'Flash\nWhen Snapcaster Mage enters the battlefield, target instant or sorcery card in your graveyard gains flashback until end of turn. The flashback cost is equal to its mana cost. (You may cast that card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('snapcaster mage', 'ISD').
card_image_name('snapcaster mage'/'ISD', 'snapcaster mage').
card_uid('snapcaster mage'/'ISD', 'ISD:Snapcaster Mage:snapcaster mage').
card_rarity('snapcaster mage'/'ISD', 'Rare').
card_artist('snapcaster mage'/'ISD', 'Volkan Baga').
card_number('snapcaster mage'/'ISD', '78').
card_multiverse_id('snapcaster mage'/'ISD', '227676').

card_in_set('somberwald spider', 'ISD').
card_original_type('somberwald spider'/'ISD', 'Creature — Spider').
card_original_text('somberwald spider'/'ISD', 'Reach (This creature can block creatures with flying.)\nMorbid — Somberwald Spider enters the battlefield with two +1/+1 counters on it if a creature died this turn.').
card_first_print('somberwald spider', 'ISD').
card_image_name('somberwald spider'/'ISD', 'somberwald spider').
card_uid('somberwald spider'/'ISD', 'ISD:Somberwald Spider:somberwald spider').
card_rarity('somberwald spider'/'ISD', 'Common').
card_artist('somberwald spider'/'ISD', 'Volkan Baga').
card_number('somberwald spider'/'ISD', '202').
card_flavor_text('somberwald spider'/'ISD', 'Stensian vampires encourage spiders near their manors so they can rob the webs.').
card_multiverse_id('somberwald spider'/'ISD', '237013').

card_in_set('spare from evil', 'ISD').
card_original_type('spare from evil'/'ISD', 'Instant').
card_original_text('spare from evil'/'ISD', 'Creatures you control gain protection from non-Human creatures until end of turn.').
card_first_print('spare from evil', 'ISD').
card_image_name('spare from evil'/'ISD', 'spare from evil').
card_uid('spare from evil'/'ISD', 'ISD:Spare from Evil:spare from evil').
card_rarity('spare from evil'/'ISD', 'Common').
card_artist('spare from evil'/'ISD', 'Jason Felix').
card_number('spare from evil'/'ISD', '34').
card_flavor_text('spare from evil'/'ISD', '\"As long as we can utter a prayer or lift a weapon, we have the power to fight back.\"\n—Thalia, Knight-Cathar').
card_multiverse_id('spare from evil'/'ISD', '230618').

card_in_set('spectral flight', 'ISD').
card_original_type('spectral flight'/'ISD', 'Enchantment — Aura').
card_original_text('spectral flight'/'ISD', 'Enchant creature\nEnchanted creature gets +2/+2 and has flying.').
card_first_print('spectral flight', 'ISD').
card_image_name('spectral flight'/'ISD', 'spectral flight').
card_uid('spectral flight'/'ISD', 'ISD:Spectral Flight:spectral flight').
card_rarity('spectral flight'/'ISD', 'Common').
card_artist('spectral flight'/'ISD', 'Johann Bodin').
card_number('spectral flight'/'ISD', '79').
card_flavor_text('spectral flight'/'ISD', '\"The church looks down on binding spirits into armor, but now I can look down on them.\"\n—Dierk, geistmage').
card_multiverse_id('spectral flight'/'ISD', '230775').

card_in_set('spectral rider', 'ISD').
card_original_type('spectral rider'/'ISD', 'Creature — Spirit Knight').
card_original_text('spectral rider'/'ISD', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)').
card_first_print('spectral rider', 'ISD').
card_image_name('spectral rider'/'ISD', 'spectral rider').
card_uid('spectral rider'/'ISD', 'ISD:Spectral Rider:spectral rider').
card_rarity('spectral rider'/'ISD', 'Uncommon').
card_artist('spectral rider'/'ISD', 'Igor Kieryluk').
card_number('spectral rider'/'ISD', '35').
card_flavor_text('spectral rider'/'ISD', 'Never a word. Never a warning. Only the sound of hoofbeats along the crossways, and the screams of the avenged.').
card_multiverse_id('spectral rider'/'ISD', '220372').

card_in_set('spider spawning', 'ISD').
card_original_type('spider spawning'/'ISD', 'Sorcery').
card_original_text('spider spawning'/'ISD', 'Put a 1/2 green Spider creature token with reach onto the battlefield for each creature card in your graveyard.\nFlashback {6}{B} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('spider spawning', 'ISD').
card_image_name('spider spawning'/'ISD', 'spider spawning').
card_uid('spider spawning'/'ISD', 'ISD:Spider Spawning:spider spawning').
card_rarity('spider spawning'/'ISD', 'Uncommon').
card_artist('spider spawning'/'ISD', 'Daniel Ljunggren').
card_number('spider spawning'/'ISD', '203').
card_multiverse_id('spider spawning'/'ISD', '222175').

card_in_set('spidery grasp', 'ISD').
card_original_type('spidery grasp'/'ISD', 'Instant').
card_original_text('spidery grasp'/'ISD', 'Untap target creature. It gets +2/+4 and gains reach until end of turn. (It can block creatures with flying.)').
card_first_print('spidery grasp', 'ISD').
card_image_name('spidery grasp'/'ISD', 'spidery grasp').
card_uid('spidery grasp'/'ISD', 'ISD:Spidery Grasp:spidery grasp').
card_rarity('spidery grasp'/'ISD', 'Common').
card_artist('spidery grasp'/'ISD', 'James Ryman').
card_number('spidery grasp'/'ISD', '204').
card_flavor_text('spidery grasp'/'ISD', '\"Nobody\'s seen the Lady of Videns for years. Poor dear, no companions at all in that old castle.\"\n—Yonda of Gavony').
card_multiverse_id('spidery grasp'/'ISD', '244686').

card_in_set('splinterfright', 'ISD').
card_original_type('splinterfright'/'ISD', 'Creature — Elemental').
card_original_text('splinterfright'/'ISD', 'Trample\nSplinterfright\'s power and toughness are each equal to the number of creature cards in your graveyard.\nAt the beginning of your upkeep, put the top two cards of your library into your graveyard.').
card_first_print('splinterfright', 'ISD').
card_image_name('splinterfright'/'ISD', 'splinterfright').
card_uid('splinterfright'/'ISD', 'ISD:Splinterfright:splinterfright').
card_rarity('splinterfright'/'ISD', 'Rare').
card_artist('splinterfright'/'ISD', 'Eric Deschamps').
card_number('splinterfright'/'ISD', '205').
card_multiverse_id('splinterfright'/'ISD', '227301').

card_in_set('stalking vampire', 'ISD').
card_original_type('stalking vampire'/'ISD', 'Creature — Vampire').
card_original_text('stalking vampire'/'ISD', 'At the beginning of your upkeep, you may pay {2}{B}{B}. If you do, transform Stalking Vampire.').
card_first_print('stalking vampire', 'ISD').
card_image_name('stalking vampire'/'ISD', 'stalking vampire').
card_uid('stalking vampire'/'ISD', 'ISD:Stalking Vampire:stalking vampire').
card_rarity('stalking vampire'/'ISD', 'Uncommon').
card_artist('stalking vampire'/'ISD', 'Slawomir Maniak').
card_number('stalking vampire'/'ISD', '114b').
card_flavor_text('stalking vampire'/'ISD', 'Each form she takes is an exploration of the senses.').
card_multiverse_id('stalking vampire'/'ISD', '221215').

card_in_set('stensia bloodhall', 'ISD').
card_original_type('stensia bloodhall'/'ISD', 'Land').
card_original_text('stensia bloodhall'/'ISD', '{T}: Add {1} to your mana pool.\n{3}{B}{R}, {T}: Stensia Bloodhall deals 2 damage to target player.').
card_first_print('stensia bloodhall', 'ISD').
card_image_name('stensia bloodhall'/'ISD', 'stensia bloodhall').
card_uid('stensia bloodhall'/'ISD', 'ISD:Stensia Bloodhall:stensia bloodhall').
card_rarity('stensia bloodhall'/'ISD', 'Rare').
card_artist('stensia bloodhall'/'ISD', 'John Avon').
card_number('stensia bloodhall'/'ISD', '247').
card_flavor_text('stensia bloodhall'/'ISD', 'The revelry ends when the supply of fresh ingenues runs dry.').
card_multiverse_id('stensia bloodhall'/'ISD', '233244').

card_in_set('stitched drake', 'ISD').
card_original_type('stitched drake'/'ISD', 'Creature — Zombie Drake').
card_original_text('stitched drake'/'ISD', 'As an additional cost to cast Stitched Drake, exile a creature card from your graveyard.\nFlying').
card_first_print('stitched drake', 'ISD').
card_image_name('stitched drake'/'ISD', 'stitched drake').
card_uid('stitched drake'/'ISD', 'ISD:Stitched Drake:stitched drake').
card_rarity('stitched drake'/'ISD', 'Common').
card_artist('stitched drake'/'ISD', 'Chris Rahn').
card_number('stitched drake'/'ISD', '80').
card_flavor_text('stitched drake'/'ISD', '\"The best skaab are more powerful and more beautiful than the sum of their parts.\"\n—Stitcher Geralf').
card_multiverse_id('stitched drake'/'ISD', '220652').

card_in_set('stitcher\'s apprentice', 'ISD').
card_original_type('stitcher\'s apprentice'/'ISD', 'Creature — Homunculus').
card_original_text('stitcher\'s apprentice'/'ISD', '{1}{U}, {T}: Put a 2/2 blue Homunculus creature token onto the battlefield, then sacrifice a creature.').
card_first_print('stitcher\'s apprentice', 'ISD').
card_image_name('stitcher\'s apprentice'/'ISD', 'stitcher\'s apprentice').
card_uid('stitcher\'s apprentice'/'ISD', 'ISD:Stitcher\'s Apprentice:stitcher\'s apprentice').
card_rarity('stitcher\'s apprentice'/'ISD', 'Common').
card_artist('stitcher\'s apprentice'/'ISD', 'Johann Bodin').
card_number('stitcher\'s apprentice'/'ISD', '81').
card_flavor_text('stitcher\'s apprentice'/'ISD', '\"Oglor create life, Oglor take it away. Oglor god! . . . until Master return.\"').
card_multiverse_id('stitcher\'s apprentice'/'ISD', '220631').

card_in_set('stony silence', 'ISD').
card_original_type('stony silence'/'ISD', 'Enchantment').
card_original_text('stony silence'/'ISD', 'Activated abilities of artifacts can\'t be activated.').
card_first_print('stony silence', 'ISD').
card_image_name('stony silence'/'ISD', 'stony silence').
card_uid('stony silence'/'ISD', 'ISD:Stony Silence:stony silence').
card_rarity('stony silence'/'ISD', 'Rare').
card_artist('stony silence'/'ISD', 'Wayne England').
card_number('stony silence'/'ISD', '36').
card_flavor_text('stony silence'/'ISD', 'Let moss grow over gargoyles\n—Gavony saying meaning\n\"forget painful memories\"').
card_multiverse_id('stony silence'/'ISD', '247425').

card_in_set('stromkirk noble', 'ISD').
card_original_type('stromkirk noble'/'ISD', 'Creature — Vampire').
card_original_text('stromkirk noble'/'ISD', 'Stromkirk Noble can\'t be blocked by Humans.\nWhenever Stromkirk Noble deals combat damage to a player, put a +1/+1 counter on it.').
card_first_print('stromkirk noble', 'ISD').
card_image_name('stromkirk noble'/'ISD', 'stromkirk noble').
card_uid('stromkirk noble'/'ISD', 'ISD:Stromkirk Noble:stromkirk noble').
card_rarity('stromkirk noble'/'ISD', 'Rare').
card_artist('stromkirk noble'/'ISD', 'James Ryman').
card_number('stromkirk noble'/'ISD', '164').
card_flavor_text('stromkirk noble'/'ISD', '\"A responsible king walks among his subjects.\"').
card_multiverse_id('stromkirk noble'/'ISD', '230783').

card_in_set('stromkirk patrol', 'ISD').
card_original_type('stromkirk patrol'/'ISD', 'Creature — Vampire Soldier').
card_original_text('stromkirk patrol'/'ISD', 'Whenever Stromkirk Patrol deals combat damage to a player, put a +1/+1 counter on it.').
card_first_print('stromkirk patrol', 'ISD').
card_image_name('stromkirk patrol'/'ISD', 'stromkirk patrol').
card_uid('stromkirk patrol'/'ISD', 'ISD:Stromkirk Patrol:stromkirk patrol').
card_rarity('stromkirk patrol'/'ISD', 'Common').
card_artist('stromkirk patrol'/'ISD', 'Karl Kopinski').
card_number('stromkirk patrol'/'ISD', '118').
card_flavor_text('stromkirk patrol'/'ISD', '\"You\'d best move along, human. Our palates are far less discerning than our master\'s.\"').
card_multiverse_id('stromkirk patrol'/'ISD', '226876').

card_in_set('sturmgeist', 'ISD').
card_original_type('sturmgeist'/'ISD', 'Creature — Spirit').
card_original_text('sturmgeist'/'ISD', 'Flying\nSturmgeist\'s power and toughness are each equal to the number of cards in your hand.\nWhenever Sturmgeist deals combat damage to a player, draw a card.').
card_first_print('sturmgeist', 'ISD').
card_image_name('sturmgeist'/'ISD', 'sturmgeist').
card_uid('sturmgeist'/'ISD', 'ISD:Sturmgeist:sturmgeist').
card_rarity('sturmgeist'/'ISD', 'Rare').
card_artist('sturmgeist'/'ISD', 'Terese Nielsen').
card_number('sturmgeist'/'ISD', '82').
card_multiverse_id('sturmgeist'/'ISD', '246950').

card_in_set('sulfur falls', 'ISD').
card_original_type('sulfur falls'/'ISD', 'Land').
card_original_text('sulfur falls'/'ISD', 'Sulfur Falls enters the battlefield tapped unless you control an Island or a Mountain.\n{T}: Add {U} or {R} to your mana pool.').
card_first_print('sulfur falls', 'ISD').
card_image_name('sulfur falls'/'ISD', 'sulfur falls').
card_uid('sulfur falls'/'ISD', 'ISD:Sulfur Falls:sulfur falls').
card_rarity('sulfur falls'/'ISD', 'Rare').
card_artist('sulfur falls'/'ISD', 'Cliff Childs').
card_number('sulfur falls'/'ISD', '248').
card_flavor_text('sulfur falls'/'ISD', 'Skirsdag cultists meet behind a waterfall\'s curtain and plot with demonic forces.').
card_multiverse_id('sulfur falls'/'ISD', '241987').

card_in_set('swamp', 'ISD').
card_original_type('swamp'/'ISD', 'Basic Land — Swamp').
card_original_text('swamp'/'ISD', 'B').
card_image_name('swamp'/'ISD', 'swamp1').
card_uid('swamp'/'ISD', 'ISD:Swamp:swamp1').
card_rarity('swamp'/'ISD', 'Basic Land').
card_artist('swamp'/'ISD', 'James Paick').
card_number('swamp'/'ISD', '256').
card_multiverse_id('swamp'/'ISD', '245240').

card_in_set('swamp', 'ISD').
card_original_type('swamp'/'ISD', 'Basic Land — Swamp').
card_original_text('swamp'/'ISD', 'B').
card_image_name('swamp'/'ISD', 'swamp2').
card_uid('swamp'/'ISD', 'ISD:Swamp:swamp2').
card_rarity('swamp'/'ISD', 'Basic Land').
card_artist('swamp'/'ISD', 'Adam Paquette').
card_number('swamp'/'ISD', '257').
card_multiverse_id('swamp'/'ISD', '245239').

card_in_set('swamp', 'ISD').
card_original_type('swamp'/'ISD', 'Basic Land — Swamp').
card_original_text('swamp'/'ISD', 'B').
card_image_name('swamp'/'ISD', 'swamp3').
card_uid('swamp'/'ISD', 'ISD:Swamp:swamp3').
card_rarity('swamp'/'ISD', 'Basic Land').
card_artist('swamp'/'ISD', 'Jung Park').
card_number('swamp'/'ISD', '258').
card_multiverse_id('swamp'/'ISD', '245241').

card_in_set('terror of kruin pass', 'ISD').
card_original_type('terror of kruin pass'/'ISD', 'Creature — Werewolf').
card_original_text('terror of kruin pass'/'ISD', 'Double strike\nEach Werewolf you control can\'t be blocked except by two or more creatures.\nAt the beginning of each upkeep, if a player cast two or more spells last turn, transform Terror of Kruin Pass.').
card_first_print('terror of kruin pass', 'ISD').
card_image_name('terror of kruin pass'/'ISD', 'terror of kruin pass').
card_uid('terror of kruin pass'/'ISD', 'ISD:Terror of Kruin Pass:terror of kruin pass').
card_rarity('terror of kruin pass'/'ISD', 'Rare').
card_artist('terror of kruin pass'/'ISD', 'David Rapoza').
card_number('terror of kruin pass'/'ISD', '152b').
card_multiverse_id('terror of kruin pass'/'ISD', '227090').

card_in_set('think twice', 'ISD').
card_original_type('think twice'/'ISD', 'Instant').
card_original_text('think twice'/'ISD', 'Draw a card.\nFlashback {2}{U} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('think twice'/'ISD', 'think twice').
card_uid('think twice'/'ISD', 'ISD:Think Twice:think twice').
card_rarity('think twice'/'ISD', 'Common').
card_artist('think twice'/'ISD', 'Anthony Francisco').
card_number('think twice'/'ISD', '83').
card_flavor_text('think twice'/'ISD', '\"Either I know just the spell I need, or I\'m about to.\"').
card_multiverse_id('think twice'/'ISD', '230626').

card_in_set('thraben militia', 'ISD').
card_original_type('thraben militia'/'ISD', 'Creature — Human Soldier').
card_original_text('thraben militia'/'ISD', 'Trample').
card_first_print('thraben militia', 'ISD').
card_image_name('thraben militia'/'ISD', 'thraben militia').
card_uid('thraben militia'/'ISD', 'ISD:Thraben Militia:thraben militia').
card_rarity('thraben militia'/'ISD', 'Common').
card_artist('thraben militia'/'ISD', 'David Rapoza').
card_number('thraben militia'/'ISD', '38b').
card_flavor_text('thraben militia'/'ISD', '\"The time for grief is over. The time for retribution has begun.\"').
card_multiverse_id('thraben militia'/'ISD', '221190').

card_in_set('thraben purebloods', 'ISD').
card_original_type('thraben purebloods'/'ISD', 'Creature — Hound').
card_original_text('thraben purebloods'/'ISD', '').
card_first_print('thraben purebloods', 'ISD').
card_image_name('thraben purebloods'/'ISD', 'thraben purebloods').
card_uid('thraben purebloods'/'ISD', 'ISD:Thraben Purebloods:thraben purebloods').
card_rarity('thraben purebloods'/'ISD', 'Common').
card_artist('thraben purebloods'/'ISD', 'Martina Pilcerova').
card_number('thraben purebloods'/'ISD', '37').
card_flavor_text('thraben purebloods'/'ISD', '\"I\'ve learned to trust my dogs\' instincts. When they\'re calm, I\'m calm. And when they get nervous, I get my crossbow.\"\n—Hinrik of House Cecani').
card_multiverse_id('thraben purebloods'/'ISD', '230625').

card_in_set('thraben sentry', 'ISD').
card_original_type('thraben sentry'/'ISD', 'Creature — Human Soldier').
card_original_text('thraben sentry'/'ISD', 'Vigilance\nWhenever another creature you control dies, you may transform Thraben Sentry.').
card_first_print('thraben sentry', 'ISD').
card_image_name('thraben sentry'/'ISD', 'thraben sentry').
card_uid('thraben sentry'/'ISD', 'ISD:Thraben Sentry:thraben sentry').
card_rarity('thraben sentry'/'ISD', 'Common').
card_artist('thraben sentry'/'ISD', 'David Rapoza').
card_number('thraben sentry'/'ISD', '38a').
card_flavor_text('thraben sentry'/'ISD', '\"Looks like it\'s going to be a quiet night.\"').
card_multiverse_id('thraben sentry'/'ISD', '222016').

card_in_set('tormented pariah', 'ISD').
card_original_type('tormented pariah'/'ISD', 'Creature — Human Warrior Werewolf').
card_original_text('tormented pariah'/'ISD', 'At the beginning of each upkeep, if no spells were cast last turn, transform Tormented Pariah.').
card_first_print('tormented pariah', 'ISD').
card_image_name('tormented pariah'/'ISD', 'tormented pariah').
card_uid('tormented pariah'/'ISD', 'ISD:Tormented Pariah:tormented pariah').
card_rarity('tormented pariah'/'ISD', 'Common').
card_artist('tormented pariah'/'ISD', 'Bud Cook').
card_number('tormented pariah'/'ISD', '165a').
card_flavor_text('tormented pariah'/'ISD', '\"Hey lads, the moon\'s rising. All the better to watch him beg for mercy.\"').
card_multiverse_id('tormented pariah'/'ISD', '222186').

card_in_set('traitorous blood', 'ISD').
card_original_type('traitorous blood'/'ISD', 'Sorcery').
card_original_text('traitorous blood'/'ISD', 'Gain control of target creature until end of turn. Untap it. It gains trample and haste until end of turn.').
card_first_print('traitorous blood', 'ISD').
card_image_name('traitorous blood'/'ISD', 'traitorous blood').
card_uid('traitorous blood'/'ISD', 'ISD:Traitorous Blood:traitorous blood').
card_rarity('traitorous blood'/'ISD', 'Common').
card_artist('traitorous blood'/'ISD', 'Raymond Swanland').
card_number('traitorous blood'/'ISD', '166').
card_flavor_text('traitorous blood'/'ISD', '\"I can make you dance to my tune like a puppet, your very blood eager to obey.\"\n—Olivia Voldaren').
card_multiverse_id('traitorous blood'/'ISD', '234438').

card_in_set('travel preparations', 'ISD').
card_original_type('travel preparations'/'ISD', 'Sorcery').
card_original_text('travel preparations'/'ISD', 'Put a +1/+1 counter on each of up to two target creatures.\nFlashback {1}{W} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('travel preparations', 'ISD').
card_image_name('travel preparations'/'ISD', 'travel preparations').
card_uid('travel preparations'/'ISD', 'ISD:Travel Preparations:travel preparations').
card_rarity('travel preparations'/'ISD', 'Common').
card_artist('travel preparations'/'ISD', 'Vincent Proce').
card_number('travel preparations'/'ISD', '206').
card_flavor_text('travel preparations'/'ISD', 'Visiting a shrine at the start of a journey makes the traveler more likely to finish it.').
card_multiverse_id('travel preparations'/'ISD', '220026').

card_in_set('traveler\'s amulet', 'ISD').
card_original_type('traveler\'s amulet'/'ISD', 'Artifact').
card_original_text('traveler\'s amulet'/'ISD', '{1}, Sacrifice Traveler\'s Amulet: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.').
card_first_print('traveler\'s amulet', 'ISD').
card_image_name('traveler\'s amulet'/'ISD', 'traveler\'s amulet').
card_uid('traveler\'s amulet'/'ISD', 'ISD:Traveler\'s Amulet:traveler\'s amulet').
card_rarity('traveler\'s amulet'/'ISD', 'Common').
card_artist('traveler\'s amulet'/'ISD', 'Alan Pollack').
card_number('traveler\'s amulet'/'ISD', '234').
card_flavor_text('traveler\'s amulet'/'ISD', 'The rider set off into the eerie mist, swaddled in armor and laden with amulets.').
card_multiverse_id('traveler\'s amulet'/'ISD', '237357').

card_in_set('tree of redemption', 'ISD').
card_original_type('tree of redemption'/'ISD', 'Creature — Plant').
card_original_text('tree of redemption'/'ISD', 'Defender\n{T}: Exchange your life total with Tree of Redemption\'s toughness.').
card_first_print('tree of redemption', 'ISD').
card_image_name('tree of redemption'/'ISD', 'tree of redemption').
card_uid('tree of redemption'/'ISD', 'ISD:Tree of Redemption:tree of redemption').
card_rarity('tree of redemption'/'ISD', 'Mythic Rare').
card_artist('tree of redemption'/'ISD', 'Vincent Proce').
card_number('tree of redemption'/'ISD', '207').
card_flavor_text('tree of redemption'/'ISD', 'The executioner\'s tree in Thraben Cathedral holds the power to absolve more than the souls of the hanged.').
card_multiverse_id('tree of redemption'/'ISD', '247421').

card_in_set('trepanation blade', 'ISD').
card_original_type('trepanation blade'/'ISD', 'Artifact — Equipment').
card_original_text('trepanation blade'/'ISD', 'Whenever equipped creature attacks, defending player reveals cards from the top of his or her library until he or she reveals a land card. The creature gets +1/+0 until end of turn for each card revealed this way. That player puts the revealed cards into his or her graveyard.\nEquip {2}').
card_first_print('trepanation blade', 'ISD').
card_image_name('trepanation blade'/'ISD', 'trepanation blade').
card_uid('trepanation blade'/'ISD', 'ISD:Trepanation Blade:trepanation blade').
card_rarity('trepanation blade'/'ISD', 'Uncommon').
card_artist('trepanation blade'/'ISD', 'Daniel Ljunggren').
card_number('trepanation blade'/'ISD', '235').
card_multiverse_id('trepanation blade'/'ISD', '221186').

card_in_set('tribute to hunger', 'ISD').
card_original_type('tribute to hunger'/'ISD', 'Instant').
card_original_text('tribute to hunger'/'ISD', 'Target opponent sacrifices a creature. You gain life equal to that creature\'s toughness.').
card_first_print('tribute to hunger', 'ISD').
card_image_name('tribute to hunger'/'ISD', 'tribute to hunger').
card_uid('tribute to hunger'/'ISD', 'ISD:Tribute to Hunger:tribute to hunger').
card_rarity('tribute to hunger'/'ISD', 'Uncommon').
card_artist('tribute to hunger'/'ISD', 'Dave Kendall').
card_number('tribute to hunger'/'ISD', '119').
card_flavor_text('tribute to hunger'/'ISD', 'Marella was delighted. The ball had attracted so many suitors for her daughter, including that handsome stranger. She wondered where the two were now . . . .').
card_multiverse_id('tribute to hunger'/'ISD', '227064').

card_in_set('typhoid rats', 'ISD').
card_original_type('typhoid rats'/'ISD', 'Creature — Rat').
card_original_text('typhoid rats'/'ISD', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_first_print('typhoid rats', 'ISD').
card_image_name('typhoid rats'/'ISD', 'typhoid rats').
card_uid('typhoid rats'/'ISD', 'ISD:Typhoid Rats:typhoid rats').
card_rarity('typhoid rats'/'ISD', 'Common').
card_artist('typhoid rats'/'ISD', 'Kev Walker').
card_number('typhoid rats'/'ISD', '120').
card_flavor_text('typhoid rats'/'ISD', 'Kidnappers caught in Havengul are given two choices: languish in prison or become rat catchers. The smart ones go to prison.').
card_multiverse_id('typhoid rats'/'ISD', '220634').

card_in_set('ulvenwald mystics', 'ISD').
card_original_type('ulvenwald mystics'/'ISD', 'Creature — Human Shaman Werewolf').
card_original_text('ulvenwald mystics'/'ISD', 'At the beginning of each upkeep, if no spells were cast last turn, transform Ulvenwald Mystics.').
card_first_print('ulvenwald mystics', 'ISD').
card_image_name('ulvenwald mystics'/'ISD', 'ulvenwald mystics').
card_uid('ulvenwald mystics'/'ISD', 'ISD:Ulvenwald Mystics:ulvenwald mystics').
card_rarity('ulvenwald mystics'/'ISD', 'Uncommon').
card_artist('ulvenwald mystics'/'ISD', 'Dan Scott').
card_number('ulvenwald mystics'/'ISD', '208a').
card_flavor_text('ulvenwald mystics'/'ISD', '\"With this ritual, we cast aside our fragile humanity . . .\"').
card_multiverse_id('ulvenwald mystics'/'ISD', '222105').

card_in_set('ulvenwald primordials', 'ISD').
card_original_type('ulvenwald primordials'/'ISD', 'Creature — Werewolf').
card_original_text('ulvenwald primordials'/'ISD', '{G}: Regenerate Ulvenwald Primordials.\nAt the beginning of each upkeep, if a player cast two or more spells last turn, transform Ulvenwald Primordials.').
card_first_print('ulvenwald primordials', 'ISD').
card_image_name('ulvenwald primordials'/'ISD', 'ulvenwald primordials').
card_uid('ulvenwald primordials'/'ISD', 'ISD:Ulvenwald Primordials:ulvenwald primordials').
card_rarity('ulvenwald primordials'/'ISD', 'Uncommon').
card_artist('ulvenwald primordials'/'ISD', 'Dan Scott').
card_number('ulvenwald primordials'/'ISD', '208b').
card_flavor_text('ulvenwald primordials'/'ISD', '\". . . and embrace the ancient ferocity of the undying Wild.\"').
card_multiverse_id('ulvenwald primordials'/'ISD', '222108').

card_in_set('unbreathing horde', 'ISD').
card_original_type('unbreathing horde'/'ISD', 'Creature — Zombie').
card_original_text('unbreathing horde'/'ISD', 'Unbreathing Horde enters the battlefield with a +1/+1 counter on it for each other Zombie you control and each Zombie card in your graveyard.\nIf Unbreathing Horde would be dealt damage, prevent that damage and remove a +1/+1 counter from it.').
card_first_print('unbreathing horde', 'ISD').
card_image_name('unbreathing horde'/'ISD', 'unbreathing horde').
card_uid('unbreathing horde'/'ISD', 'ISD:Unbreathing Horde:unbreathing horde').
card_rarity('unbreathing horde'/'ISD', 'Rare').
card_artist('unbreathing horde'/'ISD', 'Dave Kendall').
card_number('unbreathing horde'/'ISD', '121').
card_multiverse_id('unbreathing horde'/'ISD', '230786').

card_in_set('unburial rites', 'ISD').
card_original_type('unburial rites'/'ISD', 'Sorcery').
card_original_text('unburial rites'/'ISD', 'Return target creature card from your graveyard to the battlefield.\nFlashback {3}{W} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('unburial rites', 'ISD').
card_image_name('unburial rites'/'ISD', 'unburial rites').
card_uid('unburial rites'/'ISD', 'ISD:Unburial Rites:unburial rites').
card_rarity('unburial rites'/'ISD', 'Uncommon').
card_artist('unburial rites'/'ISD', 'Ryan Pancoast').
card_number('unburial rites'/'ISD', '122').
card_flavor_text('unburial rites'/'ISD', 'All crave the Blessed Sleep. Few receive it.').
card_multiverse_id('unburial rites'/'ISD', '227087').

card_in_set('undead alchemist', 'ISD').
card_original_type('undead alchemist'/'ISD', 'Creature — Zombie').
card_original_text('undead alchemist'/'ISD', 'If a Zombie you control would deal combat damage to a player, instead that player puts that many cards from the top of his or her library into his or her graveyard.\nWhenever a creature card is put into an opponent\'s graveyard from his or her library, exile that card and put a 2/2 black Zombie creature token onto the battlefield.').
card_first_print('undead alchemist', 'ISD').
card_image_name('undead alchemist'/'ISD', 'undead alchemist').
card_uid('undead alchemist'/'ISD', 'ISD:Undead Alchemist:undead alchemist').
card_rarity('undead alchemist'/'ISD', 'Rare').
card_artist('undead alchemist'/'ISD', 'Michael C. Hayes').
card_number('undead alchemist'/'ISD', '84').
card_multiverse_id('undead alchemist'/'ISD', '244684').

card_in_set('unholy fiend', 'ISD').
card_original_type('unholy fiend'/'ISD', 'Creature — Horror').
card_original_text('unholy fiend'/'ISD', 'At the beginning of your end step, you lose 1 life.').
card_first_print('unholy fiend', 'ISD').
card_image_name('unholy fiend'/'ISD', 'unholy fiend').
card_uid('unholy fiend'/'ISD', 'ISD:Unholy Fiend:unholy fiend').
card_rarity('unholy fiend'/'ISD', 'Uncommon').
card_artist('unholy fiend'/'ISD', 'Igor Kieryluk').
card_number('unholy fiend'/'ISD', '8b').
card_flavor_text('unholy fiend'/'ISD', 'The fiend tormented them by recounting the girl\'s memories, as if some part of her remained inside that twisted shell.').
card_multiverse_id('unholy fiend'/'ISD', '221222').

card_in_set('unruly mob', 'ISD').
card_original_type('unruly mob'/'ISD', 'Creature — Human').
card_original_text('unruly mob'/'ISD', 'Whenever another creature you control dies, put a +1/+1 counter on Unruly Mob.').
card_first_print('unruly mob', 'ISD').
card_image_name('unruly mob'/'ISD', 'unruly mob').
card_uid('unruly mob'/'ISD', 'ISD:Unruly Mob:unruly mob').
card_rarity('unruly mob'/'ISD', 'Common').
card_artist('unruly mob'/'ISD', 'Ryan Pancoast').
card_number('unruly mob'/'ISD', '39').
card_flavor_text('unruly mob'/'ISD', '\"Who says werewolves are the only ones who can hunt as a pack?\"\n—Kolman, elder of Gatstaf').
card_multiverse_id('unruly mob'/'ISD', '220389').

card_in_set('urgent exorcism', 'ISD').
card_original_type('urgent exorcism'/'ISD', 'Instant').
card_original_text('urgent exorcism'/'ISD', 'Destroy target Spirit or enchantment.').
card_first_print('urgent exorcism', 'ISD').
card_image_name('urgent exorcism'/'ISD', 'urgent exorcism').
card_uid('urgent exorcism'/'ISD', 'ISD:Urgent Exorcism:urgent exorcism').
card_rarity('urgent exorcism'/'ISD', 'Common').
card_artist('urgent exorcism'/'ISD', 'Svetlin Velinov').
card_number('urgent exorcism'/'ISD', '40').
card_flavor_text('urgent exorcism'/'ISD', '\"To show geists pity is to prolong their agonies. Whether they know it or not, they seek the Blessed Sleep, and we are to give it to them.\"\n—Mikaeus, the Lunarch').
card_multiverse_id('urgent exorcism'/'ISD', '227066').

card_in_set('vampire interloper', 'ISD').
card_original_type('vampire interloper'/'ISD', 'Creature — Vampire Scout').
card_original_text('vampire interloper'/'ISD', 'Flying\nVampire Interloper can\'t block.').
card_first_print('vampire interloper', 'ISD').
card_image_name('vampire interloper'/'ISD', 'vampire interloper').
card_uid('vampire interloper'/'ISD', 'ISD:Vampire Interloper:vampire interloper').
card_rarity('vampire interloper'/'ISD', 'Common').
card_artist('vampire interloper'/'ISD', 'James Ryman').
card_number('vampire interloper'/'ISD', '123').
card_flavor_text('vampire interloper'/'ISD', 'He listens to every heartbeat, deciding which one of hundreds he will stop.').
card_multiverse_id('vampire interloper'/'ISD', '226748').

card_in_set('vampiric fury', 'ISD').
card_original_type('vampiric fury'/'ISD', 'Instant').
card_original_text('vampiric fury'/'ISD', 'Vampire creatures you control get +2/+0 and gain first strike until end of turn.').
card_first_print('vampiric fury', 'ISD').
card_image_name('vampiric fury'/'ISD', 'vampiric fury').
card_uid('vampiric fury'/'ISD', 'ISD:Vampiric Fury:vampiric fury').
card_rarity('vampiric fury'/'ISD', 'Common').
card_artist('vampiric fury'/'ISD', 'Matt Stewart').
card_number('vampiric fury'/'ISD', '167').
card_flavor_text('vampiric fury'/'ISD', 'Many peasants secretly admire vampires\' glamour and elegance—until they witness a moment of pure bloodlust.').
card_multiverse_id('vampiric fury'/'ISD', '247424').

card_in_set('victim of night', 'ISD').
card_original_type('victim of night'/'ISD', 'Instant').
card_original_text('victim of night'/'ISD', 'Destroy target non-Vampire, non-Werewolf, non-Zombie creature.').
card_first_print('victim of night', 'ISD').
card_image_name('victim of night'/'ISD', 'victim of night').
card_uid('victim of night'/'ISD', 'ISD:Victim of Night:victim of night').
card_rarity('victim of night'/'ISD', 'Common').
card_artist('victim of night'/'ISD', 'Winona Nelson').
card_number('victim of night'/'ISD', '124').
card_flavor_text('victim of night'/'ISD', '\"Do not touch a drop. Not yet. I want to watch this so-called slayer\'s last crisis of faith.\"\n—Olivia Voldaren').
card_multiverse_id('victim of night'/'ISD', '220023').

card_in_set('village bell-ringer', 'ISD').
card_original_type('village bell-ringer'/'ISD', 'Creature — Human Scout').
card_original_text('village bell-ringer'/'ISD', 'Flash (You may cast this spell any time you could cast an instant.)\nWhen Village Bell-Ringer enters the battlefield, untap all creatures you control.').
card_first_print('village bell-ringer', 'ISD').
card_image_name('village bell-ringer'/'ISD', 'village bell-ringer').
card_uid('village bell-ringer'/'ISD', 'ISD:Village Bell-Ringer:village bell-ringer').
card_rarity('village bell-ringer'/'ISD', 'Common').
card_artist('village bell-ringer'/'ISD', 'David Palumbo').
card_number('village bell-ringer'/'ISD', '41').
card_flavor_text('village bell-ringer'/'ISD', '\"Priests, hunters, slayers—to arms! The enemy approaches!\"').
card_multiverse_id('village bell-ringer'/'ISD', '222923').

card_in_set('village cannibals', 'ISD').
card_original_type('village cannibals'/'ISD', 'Creature — Human').
card_original_text('village cannibals'/'ISD', 'Whenever another Human creature dies, put a +1/+1 counter on Village Cannibals.').
card_first_print('village cannibals', 'ISD').
card_image_name('village cannibals'/'ISD', 'village cannibals').
card_uid('village cannibals'/'ISD', 'ISD:Village Cannibals:village cannibals').
card_rarity('village cannibals'/'ISD', 'Uncommon').
card_artist('village cannibals'/'ISD', 'Bud Cook').
card_number('village cannibals'/'ISD', '125').
card_flavor_text('village cannibals'/'ISD', 'Some have endured the horrors of Innistrad by becoming the worst monsters of all.').
card_multiverse_id('village cannibals'/'ISD', '222903').

card_in_set('village ironsmith', 'ISD').
card_original_type('village ironsmith'/'ISD', 'Creature — Human Werewolf').
card_original_text('village ironsmith'/'ISD', 'First strike\nAt the beginning of each upkeep, if no spells were cast last turn, transform Village Ironsmith.').
card_first_print('village ironsmith', 'ISD').
card_image_name('village ironsmith'/'ISD', 'village ironsmith').
card_uid('village ironsmith'/'ISD', 'ISD:Village Ironsmith:village ironsmith').
card_rarity('village ironsmith'/'ISD', 'Common').
card_artist('village ironsmith'/'ISD', 'Christopher Moeller').
card_number('village ironsmith'/'ISD', '168a').
card_flavor_text('village ironsmith'/'ISD', 'Each night, he abandons the trappings of civilization.').
card_multiverse_id('village ironsmith'/'ISD', '222112').

card_in_set('villagers of estwald', 'ISD').
card_original_type('villagers of estwald'/'ISD', 'Creature — Human Werewolf').
card_original_text('villagers of estwald'/'ISD', 'At the beginning of each upkeep, if no spells were cast last turn, transform Villagers of Estwald.').
card_first_print('villagers of estwald', 'ISD').
card_image_name('villagers of estwald'/'ISD', 'villagers of estwald').
card_uid('villagers of estwald'/'ISD', 'ISD:Villagers of Estwald:villagers of estwald').
card_rarity('villagers of estwald'/'ISD', 'Common').
card_artist('villagers of estwald'/'ISD', 'Kev Walker').
card_number('villagers of estwald'/'ISD', '209a').
card_flavor_text('villagers of estwald'/'ISD', 'You can spot a werewolf-infested town by its lack of butcher shops.').
card_multiverse_id('villagers of estwald'/'ISD', '222915').

card_in_set('voiceless spirit', 'ISD').
card_original_type('voiceless spirit'/'ISD', 'Creature — Spirit').
card_original_text('voiceless spirit'/'ISD', 'Flying, first strike').
card_first_print('voiceless spirit', 'ISD').
card_image_name('voiceless spirit'/'ISD', 'voiceless spirit').
card_uid('voiceless spirit'/'ISD', 'ISD:Voiceless Spirit:voiceless spirit').
card_rarity('voiceless spirit'/'ISD', 'Common').
card_artist('voiceless spirit'/'ISD', 'Daarken').
card_number('voiceless spirit'/'ISD', '42').
card_flavor_text('voiceless spirit'/'ISD', '\"I\'d rather take on a neckbiter or moonhowler any day. Oh well, looks like I\'m going to end up parrying with empty air half the night.\"\n—Saint Trogen, the Slayer').
card_multiverse_id('voiceless spirit'/'ISD', '243215').

card_in_set('walking corpse', 'ISD').
card_original_type('walking corpse'/'ISD', 'Creature — Zombie').
card_original_text('walking corpse'/'ISD', '').
card_first_print('walking corpse', 'ISD').
card_image_name('walking corpse'/'ISD', 'walking corpse').
card_uid('walking corpse'/'ISD', 'ISD:Walking Corpse:walking corpse').
card_rarity('walking corpse'/'ISD', 'Common').
card_artist('walking corpse'/'ISD', 'Igor Kieryluk').
card_number('walking corpse'/'ISD', '126').
card_flavor_text('walking corpse'/'ISD', '\"Is it hopeless? When wave upon wave of our dead return as mindless flesh-eaters to claw upon Thraben\'s walls? We\'re too busy cutting them down to indulge ourselves in such philosophical foppery.\"\n—Mikaeus, the Lunarch').
card_multiverse_id('walking corpse'/'ISD', '243210').

card_in_set('wildblood pack', 'ISD').
card_original_type('wildblood pack'/'ISD', 'Creature — Werewolf').
card_original_text('wildblood pack'/'ISD', 'Trample\nAttacking creatures you control get +3/+0.\nAt the beginning of each upkeep, if a player cast two or more spells last turn, transform Wildblood Pack.').
card_first_print('wildblood pack', 'ISD').
card_image_name('wildblood pack'/'ISD', 'wildblood pack').
card_uid('wildblood pack'/'ISD', 'ISD:Wildblood Pack:wildblood pack').
card_rarity('wildblood pack'/'ISD', 'Rare').
card_artist('wildblood pack'/'ISD', 'Greg Staples').
card_number('wildblood pack'/'ISD', '149b').
card_multiverse_id('wildblood pack'/'ISD', '227419').

card_in_set('witchbane orb', 'ISD').
card_original_type('witchbane orb'/'ISD', 'Artifact').
card_original_text('witchbane orb'/'ISD', 'When Witchbane Orb enters the battlefield, destroy all Curses attached to you.\nYou have hexproof. (You can\'t be the target of spells or abilities your opponents control, including Aura spells.)').
card_first_print('witchbane orb', 'ISD').
card_image_name('witchbane orb'/'ISD', 'witchbane orb').
card_uid('witchbane orb'/'ISD', 'ISD:Witchbane Orb:witchbane orb').
card_rarity('witchbane orb'/'ISD', 'Rare').
card_artist('witchbane orb'/'ISD', 'John Avon').
card_number('witchbane orb'/'ISD', '236').
card_multiverse_id('witchbane orb'/'ISD', '233240').

card_in_set('wooden stake', 'ISD').
card_original_type('wooden stake'/'ISD', 'Artifact — Equipment').
card_original_text('wooden stake'/'ISD', 'Equipped creature gets +1/+0.\nWhenever equipped creature blocks or becomes blocked by a Vampire, destroy that creature. It can\'t be regenerated.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('wooden stake', 'ISD').
card_image_name('wooden stake'/'ISD', 'wooden stake').
card_uid('wooden stake'/'ISD', 'ISD:Wooden Stake:wooden stake').
card_rarity('wooden stake'/'ISD', 'Common').
card_artist('wooden stake'/'ISD', 'David Palumbo').
card_number('wooden stake'/'ISD', '237').
card_multiverse_id('wooden stake'/'ISD', '226880').

card_in_set('woodland cemetery', 'ISD').
card_original_type('woodland cemetery'/'ISD', 'Land').
card_original_text('woodland cemetery'/'ISD', 'Woodland Cemetery enters the battlefield tapped unless you control a Swamp or a Forest.\n{T}: Add {B} or {G} to your mana pool.').
card_first_print('woodland cemetery', 'ISD').
card_image_name('woodland cemetery'/'ISD', 'woodland cemetery').
card_uid('woodland cemetery'/'ISD', 'ISD:Woodland Cemetery:woodland cemetery').
card_rarity('woodland cemetery'/'ISD', 'Rare').
card_artist('woodland cemetery'/'ISD', 'Lars Grant-West').
card_number('woodland cemetery'/'ISD', '249').
card_flavor_text('woodland cemetery'/'ISD', 'Farmers cut the trees and buried their dead. The dead rose up and killed the farmers. The trees grew back and strangled the dead.').
card_multiverse_id('woodland cemetery'/'ISD', '241983').

card_in_set('woodland sleuth', 'ISD').
card_original_type('woodland sleuth'/'ISD', 'Creature — Human Scout').
card_original_text('woodland sleuth'/'ISD', 'Morbid — When Woodland Sleuth enters the battlefield, if a creature died this turn, return a creature card at random from your graveyard to your hand.').
card_first_print('woodland sleuth', 'ISD').
card_image_name('woodland sleuth'/'ISD', 'woodland sleuth').
card_uid('woodland sleuth'/'ISD', 'ISD:Woodland Sleuth:woodland sleuth').
card_rarity('woodland sleuth'/'ISD', 'Common').
card_artist('woodland sleuth'/'ISD', 'Tomasz Jedruszek').
card_number('woodland sleuth'/'ISD', '210').
card_flavor_text('woodland sleuth'/'ISD', '\"Whatever did this is near. Death reversion has not yet occurred. Be alert!\"').
card_multiverse_id('woodland sleuth'/'ISD', '220654').

card_in_set('wreath of geists', 'ISD').
card_original_type('wreath of geists'/'ISD', 'Enchantment — Aura').
card_original_text('wreath of geists'/'ISD', 'Enchant creature\nEnchanted creature gets +X/+X, where X is the number of creature cards in your graveyard.').
card_first_print('wreath of geists', 'ISD').
card_image_name('wreath of geists'/'ISD', 'wreath of geists').
card_uid('wreath of geists'/'ISD', 'ISD:Wreath of Geists:wreath of geists').
card_rarity('wreath of geists'/'ISD', 'Uncommon').
card_artist('wreath of geists'/'ISD', 'Jason A. Engle').
card_number('wreath of geists'/'ISD', '211').
card_flavor_text('wreath of geists'/'ISD', 'Before you travel the woods of Kessig, first get permission from those who died trying.').
card_multiverse_id('wreath of geists'/'ISD', '237017').
