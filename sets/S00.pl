% Starter 2000

set('S00').
set_name('S00', 'Starter 2000').
set_release_date('S00', '2000-04-01').
set_border('S00', 'white').
set_type('S00', 'starter').

card_in_set('angelic blessing', 'S00').
card_original_type('angelic blessing'/'S00', 'Sorcery').
card_original_text('angelic blessing'/'S00', 'Target creature gets +3/+3 and gains flying until end of turn.').
card_image_name('angelic blessing'/'S00', 'angelic blessing').
card_uid('angelic blessing'/'S00', 'S00:Angelic Blessing:angelic blessing').
card_rarity('angelic blessing'/'S00', 'Common').
card_artist('angelic blessing'/'S00', 'Mark Zug').
card_flavor_text('angelic blessing'/'S00', 'Only the warrior who can admit mortal weakness will be bolstered by immortal strength.').
card_multiverse_id('angelic blessing'/'S00', '25456').

card_in_set('armored pegasus', 'S00').
card_original_type('armored pegasus'/'S00', 'Creature — Pegasus').
card_original_text('armored pegasus'/'S00', 'Flying').
card_image_name('armored pegasus'/'S00', 'armored pegasus').
card_uid('armored pegasus'/'S00', 'S00:Armored Pegasus:armored pegasus').
card_rarity('armored pegasus'/'S00', 'Common').
card_artist('armored pegasus'/'S00', 'Andrew Robinson').
card_flavor_text('armored pegasus'/'S00', 'Asked how it survived a run-in with a bog imp, the pegasus just shook its mane and burped.').
card_multiverse_id('armored pegasus'/'S00', '25498').

card_in_set('bog imp', 'S00').
card_original_type('bog imp'/'S00', 'Creature — Imp').
card_original_text('bog imp'/'S00', 'Flying').
card_image_name('bog imp'/'S00', 'bog imp').
card_uid('bog imp'/'S00', 'S00:Bog Imp:bog imp').
card_rarity('bog imp'/'S00', 'Common').
card_artist('bog imp'/'S00', 'Christopher Rush').
card_flavor_text('bog imp'/'S00', 'Don\'t be fooled by their looks. Think of them as little knives with wings.').
card_multiverse_id('bog imp'/'S00', '25492').

card_in_set('breath of life', 'S00').
card_original_type('breath of life'/'S00', 'Sorcery').
card_original_text('breath of life'/'S00', 'Return target creature card from your graveyard to play.').
card_image_name('breath of life'/'S00', 'breath of life').
card_uid('breath of life'/'S00', 'S00:Breath of Life:breath of life').
card_rarity('breath of life'/'S00', 'Uncommon').
card_artist('breath of life'/'S00', 'DiTerlizzi').
card_multiverse_id('breath of life'/'S00', '25447').

card_in_set('coercion', 'S00').
card_original_type('coercion'/'S00', 'Sorcery').
card_original_text('coercion'/'S00', 'Look at target player\'s hand and choose a card from it. That player discards that card.').
card_image_name('coercion'/'S00', 'coercion').
card_uid('coercion'/'S00', 'S00:Coercion:coercion').
card_rarity('coercion'/'S00', 'Common').
card_artist('coercion'/'S00', 'DiTerlizzi').
card_flavor_text('coercion'/'S00', 'A rhino\'s bargain\n—Femeref expression meaning\n\"a situation with no choices\"').
card_multiverse_id('coercion'/'S00', '25477').

card_in_set('counterspell', 'S00').
card_original_type('counterspell'/'S00', 'Instant').
card_original_text('counterspell'/'S00', 'Counter target spell.').
card_image_name('counterspell'/'S00', 'counterspell').
card_uid('counterspell'/'S00', 'S00:Counterspell:counterspell').
card_rarity('counterspell'/'S00', 'Common').
card_artist('counterspell'/'S00', 'Hannibal King').
card_multiverse_id('counterspell'/'S00', '25503').

card_in_set('disenchant', 'S00').
card_original_type('disenchant'/'S00', 'Instant').
card_original_text('disenchant'/'S00', 'Destroy target artifact or enchantment.').
card_image_name('disenchant'/'S00', 'disenchant').
card_uid('disenchant'/'S00', 'S00:Disenchant:disenchant').
card_rarity('disenchant'/'S00', 'Common').
card_artist('disenchant'/'S00', 'Brian Snõddy').
card_multiverse_id('disenchant'/'S00', '25500').

card_in_set('drudge skeletons', 'S00').
card_original_type('drudge skeletons'/'S00', 'Creature — Skeleton').
card_original_text('drudge skeletons'/'S00', '{B} Regenerate Drudge Skeletons.').
card_image_name('drudge skeletons'/'S00', 'drudge skeletons').
card_uid('drudge skeletons'/'S00', 'S00:Drudge Skeletons:drudge skeletons').
card_rarity('drudge skeletons'/'S00', 'Common').
card_artist('drudge skeletons'/'S00', 'Ian Miller').
card_flavor_text('drudge skeletons'/'S00', '\"The dead make good soldiers. They can\'t disobey orders, they never surrender, and they don\'t stop fighting when a random body part falls off.\"\n—Nevinyrral, Necromancer\'s Handbook').
card_multiverse_id('drudge skeletons'/'S00', '25507').

card_in_set('durkwood boars', 'S00').
card_original_type('durkwood boars'/'S00', 'Creature — Boar').
card_original_text('durkwood boars'/'S00', '').
card_image_name('durkwood boars'/'S00', 'durkwood boars').
card_uid('durkwood boars'/'S00', 'S00:Durkwood Boars:durkwood boars').
card_rarity('durkwood boars'/'S00', 'Common').
card_artist('durkwood boars'/'S00', 'Mike Kimble').
card_flavor_text('durkwood boars'/'S00', '\"And the unclean spirits went out, and entered the swine: and the herd ran violently . . . .\"\n—The Bible, Mark 5:13').
card_multiverse_id('durkwood boars'/'S00', '25493').

card_in_set('eager cadet', 'S00').
card_original_type('eager cadet'/'S00', 'Creature — Soldier').
card_original_text('eager cadet'/'S00', '').
card_image_name('eager cadet'/'S00', 'eager cadet').
card_uid('eager cadet'/'S00', 'S00:Eager Cadet:eager cadet').
card_rarity('eager cadet'/'S00', 'Common').
card_artist('eager cadet'/'S00', 'Scott M. Fischer').
card_flavor_text('eager cadet'/'S00', 'The enthusiasm of the young, sharpened by the discipline of a soldier.').
card_multiverse_id('eager cadet'/'S00', '25455').

card_in_set('flame spirit', 'S00').
card_original_type('flame spirit'/'S00', 'Creature — Spirit').
card_original_text('flame spirit'/'S00', '{R} Flame Spirit gets +1/+0 until end of turn.').
card_image_name('flame spirit'/'S00', 'flame spirit').
card_uid('flame spirit'/'S00', 'S00:Flame Spirit:flame spirit').
card_rarity('flame spirit'/'S00', 'Common').
card_artist('flame spirit'/'S00', 'Justin Hampton').
card_flavor_text('flame spirit'/'S00', '\"The spirit of the flame is the spirit of change.\"\n—Lovisa Coldeyes,\nBalduvian chieftain').
card_multiverse_id('flame spirit'/'S00', '25514').

card_in_set('flight', 'S00').
card_original_type('flight'/'S00', 'Enchant Creature').
card_original_text('flight'/'S00', 'Enchanted creature gains flying.').
card_image_name('flight'/'S00', 'flight').
card_uid('flight'/'S00', 'S00:Flight:flight').
card_rarity('flight'/'S00', 'Common').
card_artist('flight'/'S00', 'Jerry Tiritilli').
card_multiverse_id('flight'/'S00', '25510').

card_in_set('forest', 'S00').
card_original_type('forest'/'S00', 'Land').
card_original_text('forest'/'S00', 'G').
card_image_name('forest'/'S00', 'forest1').
card_uid('forest'/'S00', 'S00:Forest:forest1').
card_rarity('forest'/'S00', 'Basic Land').
card_artist('forest'/'S00', 'John Avon').
card_multiverse_id('forest'/'S00', '25483').

card_in_set('forest', 'S00').
card_original_type('forest'/'S00', 'Land').
card_original_text('forest'/'S00', 'G').
card_image_name('forest'/'S00', 'forest2').
card_uid('forest'/'S00', 'S00:Forest:forest2').
card_rarity('forest'/'S00', 'Basic Land').
card_artist('forest'/'S00', 'John Avon').
card_multiverse_id('forest'/'S00', '25482').

card_in_set('giant growth', 'S00').
card_original_type('giant growth'/'S00', 'Instant').
card_original_text('giant growth'/'S00', 'Target creature gets +3/+3 until end of turn.').
card_image_name('giant growth'/'S00', 'giant growth').
card_uid('giant growth'/'S00', 'S00:Giant Growth:giant growth').
card_rarity('giant growth'/'S00', 'Common').
card_artist('giant growth'/'S00', 'DiTerlizzi').
card_multiverse_id('giant growth'/'S00', '25502').

card_in_set('giant octopus', 'S00').
card_original_type('giant octopus'/'S00', 'Creature — Octopus').
card_original_text('giant octopus'/'S00', '').
card_image_name('giant octopus'/'S00', 'giant octopus').
card_uid('giant octopus'/'S00', 'S00:Giant Octopus:giant octopus').
card_rarity('giant octopus'/'S00', 'Common').
card_artist('giant octopus'/'S00', 'John Matson').
card_flavor_text('giant octopus'/'S00', 'At the sight of the thing the calamari vendor\'s eyes went wide, but from fear or avarice none could tell.').
card_multiverse_id('giant octopus'/'S00', '25457').

card_in_set('goblin hero', 'S00').
card_original_type('goblin hero'/'S00', 'Creature — Goblin').
card_original_text('goblin hero'/'S00', '').
card_image_name('goblin hero'/'S00', 'goblin hero').
card_uid('goblin hero'/'S00', 'S00:Goblin Hero:goblin hero').
card_rarity('goblin hero'/'S00', 'Common').
card_artist('goblin hero'/'S00', 'Pete Venters').
card_flavor_text('goblin hero'/'S00', '\"When you\'re a goblin, you don\'t have to step forward to be a hero—everyone else just has to step back\"\n—Biggum Flodrot, goblin veteran').
card_multiverse_id('goblin hero'/'S00', '25486').

card_in_set('hand of death', 'S00').
card_original_type('hand of death'/'S00', 'Sorcery').
card_original_text('hand of death'/'S00', 'Destroy target nonblack creature.').
card_image_name('hand of death'/'S00', 'hand of death').
card_uid('hand of death'/'S00', 'S00:Hand of Death:hand of death').
card_rarity('hand of death'/'S00', 'Common').
card_artist('hand of death'/'S00', 'Heather Hudson').
card_flavor_text('hand of death'/'S00', 'The touch of death is never gentle.').
card_multiverse_id('hand of death'/'S00', '25490').

card_in_set('hero\'s resolve', 'S00').
card_original_type('hero\'s resolve'/'S00', 'Enchant Creature').
card_original_text('hero\'s resolve'/'S00', 'Enchanted creature gets +1/+5.').
card_image_name('hero\'s resolve'/'S00', 'hero\'s resolve').
card_uid('hero\'s resolve'/'S00', 'S00:Hero\'s Resolve:hero\'s resolve').
card_rarity('hero\'s resolve'/'S00', 'Common').
card_artist('hero\'s resolve'/'S00', 'Pete Venters').
card_flavor_text('hero\'s resolve'/'S00', '\"Destiny, chance, fate, fortune—they\'re all just ways of claiming your successes without claiming your failures.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('hero\'s resolve'/'S00', '25536').

card_in_set('inspiration', 'S00').
card_original_type('inspiration'/'S00', 'Instant').
card_original_text('inspiration'/'S00', 'Target player draws two cards.').
card_image_name('inspiration'/'S00', 'inspiration').
card_uid('inspiration'/'S00', 'S00:Inspiration:inspiration').
card_rarity('inspiration'/'S00', 'Common').
card_artist('inspiration'/'S00', 'Zina Saunders').
card_flavor_text('inspiration'/'S00', '\"Madness and genius are separated only by degrees of success.\"\n—Sidar Jabari').
card_multiverse_id('inspiration'/'S00', '25526').

card_in_set('island', 'S00').
card_original_type('island'/'S00', 'Land').
card_original_text('island'/'S00', 'U').
card_image_name('island'/'S00', 'island1').
card_uid('island'/'S00', 'S00:Island:island1').
card_rarity('island'/'S00', 'Basic Land').
card_artist('island'/'S00', 'Douglas Shuler').
card_multiverse_id('island'/'S00', '25451').

card_in_set('island', 'S00').
card_original_type('island'/'S00', 'Land').
card_original_text('island'/'S00', 'U').
card_image_name('island'/'S00', 'island2').
card_uid('island'/'S00', 'S00:Island:island2').
card_rarity('island'/'S00', 'Basic Land').
card_artist('island'/'S00', 'Eric Peterson').
card_multiverse_id('island'/'S00', '25452').

card_in_set('knight errant', 'S00').
card_original_type('knight errant'/'S00', 'Creature — Knight').
card_original_text('knight errant'/'S00', '').
card_image_name('knight errant'/'S00', 'knight errant').
card_uid('knight errant'/'S00', 'S00:Knight Errant:knight errant').
card_rarity('knight errant'/'S00', 'Common').
card_artist('knight errant'/'S00', 'Dan Frazier').
card_flavor_text('knight errant'/'S00', '\". . . Before honor is humility.\"\n—The Bible, Proverbs 15:33').
card_multiverse_id('knight errant'/'S00', '25450').

card_in_set('lava axe', 'S00').
card_original_type('lava axe'/'S00', 'Sorcery').
card_original_text('lava axe'/'S00', 'Lava Axe deals 5 damage to target opponent.').
card_image_name('lava axe'/'S00', 'lava axe').
card_uid('lava axe'/'S00', 'S00:Lava Axe:lava axe').
card_rarity('lava axe'/'S00', 'Common').
card_artist('lava axe'/'S00', 'Brian Snõddy').
card_flavor_text('lava axe'/'S00', 'Meant to cut through the body and burn straight to the soul.').
card_multiverse_id('lava axe'/'S00', '25488').

card_in_set('llanowar elves', 'S00').
card_original_type('llanowar elves'/'S00', 'Creature — Elf').
card_original_text('llanowar elves'/'S00', '{T}: Add {G} to your mana pool.').
card_image_name('llanowar elves'/'S00', 'llanowar elves').
card_uid('llanowar elves'/'S00', 'S00:Llanowar Elves:llanowar elves').
card_rarity('llanowar elves'/'S00', 'Common').
card_artist('llanowar elves'/'S00', 'Anson Maddocks').
card_flavor_text('llanowar elves'/'S00', 'One bone broken for every twig snapped underfoot.\n—Llanowar penalty for trespassing').
card_multiverse_id('llanowar elves'/'S00', '25508').

card_in_set('merfolk of the pearl trident', 'S00').
card_original_type('merfolk of the pearl trident'/'S00', 'Creature — Merfolk').
card_original_text('merfolk of the pearl trident'/'S00', '').
card_image_name('merfolk of the pearl trident'/'S00', 'merfolk of the pearl trident').
card_uid('merfolk of the pearl trident'/'S00', 'S00:Merfolk of the Pearl Trident:merfolk of the pearl trident').
card_rarity('merfolk of the pearl trident'/'S00', 'Common').
card_artist('merfolk of the pearl trident'/'S00', 'DiTerlizzi').
card_flavor_text('merfolk of the pearl trident'/'S00', 'Are merfolk humans with fins, or are humans merfolk with feet?').
card_multiverse_id('merfolk of the pearl trident'/'S00', '25458').

card_in_set('mons\'s goblin raiders', 'S00').
card_original_type('mons\'s goblin raiders'/'S00', 'Creature — Goblin').
card_original_text('mons\'s goblin raiders'/'S00', '').
card_image_name('mons\'s goblin raiders'/'S00', 'mons\'s goblin raiders').
card_uid('mons\'s goblin raiders'/'S00', 'S00:Mons\'s Goblin Raiders:mons\'s goblin raiders').
card_rarity('mons\'s goblin raiders'/'S00', 'Common').
card_artist('mons\'s goblin raiders'/'S00', 'Pete Venters').
card_flavor_text('mons\'s goblin raiders'/'S00', 'Just because they have a club doesn\'t mean they\'re organized.').
card_multiverse_id('mons\'s goblin raiders'/'S00', '25491').

card_in_set('monstrous growth', 'S00').
card_original_type('monstrous growth'/'S00', 'Sorcery').
card_original_text('monstrous growth'/'S00', 'Target creature gets +4/+4 until end of turn.').
card_image_name('monstrous growth'/'S00', 'monstrous growth').
card_uid('monstrous growth'/'S00', 'S00:Monstrous Growth:monstrous growth').
card_rarity('monstrous growth'/'S00', 'Common').
card_artist('monstrous growth'/'S00', 'Una Fricker').
card_flavor_text('monstrous growth'/'S00', 'The nightstalkers\' little game of tease-the-squirrel suddenly took an unexpected turn.').
card_multiverse_id('monstrous growth'/'S00', '25475').

card_in_set('moon sprite', 'S00').
card_original_type('moon sprite'/'S00', 'Creature — Faerie').
card_original_text('moon sprite'/'S00', 'Flying').
card_image_name('moon sprite'/'S00', 'moon sprite').
card_uid('moon sprite'/'S00', 'S00:Moon Sprite:moon sprite').
card_rarity('moon sprite'/'S00', 'Uncommon').
card_artist('moon sprite'/'S00', 'Terese Nielsen').
card_flavor_text('moon sprite'/'S00', '\"I am that merry wanderer of the night.\"\n—William Shakespeare,\nA Midsummer Night\'s Dream').
card_multiverse_id('moon sprite'/'S00', '25485').

card_in_set('mountain', 'S00').
card_original_type('mountain'/'S00', 'Land').
card_original_text('mountain'/'S00', 'R').
card_image_name('mountain'/'S00', 'mountain1').
card_uid('mountain'/'S00', 'S00:Mountain:mountain1').
card_rarity('mountain'/'S00', 'Basic Land').
card_artist('mountain'/'S00', 'John Avon').
card_multiverse_id('mountain'/'S00', '25480').

card_in_set('mountain', 'S00').
card_original_type('mountain'/'S00', 'Land').
card_original_text('mountain'/'S00', 'R').
card_image_name('mountain'/'S00', 'mountain2').
card_uid('mountain'/'S00', 'S00:Mountain:mountain2').
card_rarity('mountain'/'S00', 'Basic Land').
card_artist('mountain'/'S00', 'Brian Durfee').
card_multiverse_id('mountain'/'S00', '25481').

card_in_set('obsianus golem', 'S00').
card_original_type('obsianus golem'/'S00', 'Artifact Creature — Golem').
card_original_text('obsianus golem'/'S00', '').
card_image_name('obsianus golem'/'S00', 'obsianus golem').
card_uid('obsianus golem'/'S00', 'S00:Obsianus Golem:obsianus golem').
card_rarity('obsianus golem'/'S00', 'Uncommon').
card_artist('obsianus golem'/'S00', 'Jesper Myrfors').
card_flavor_text('obsianus golem'/'S00', '\"The foot stone is connected to the ankle stone, the ankle stone is connected to the leg stone . . . .\"\n—\"Song of the Artificer\"').
card_multiverse_id('obsianus golem'/'S00', '25528').

card_in_set('ogre warrior', 'S00').
card_original_type('ogre warrior'/'S00', 'Creature — Ogre').
card_original_text('ogre warrior'/'S00', '').
card_image_name('ogre warrior'/'S00', 'ogre warrior').
card_uid('ogre warrior'/'S00', 'S00:Ogre Warrior:ogre warrior').
card_rarity('ogre warrior'/'S00', 'Common').
card_artist('ogre warrior'/'S00', 'Jeff Miracola').
card_flavor_text('ogre warrior'/'S00', 'Assault and battery included.').
card_multiverse_id('ogre warrior'/'S00', '25476').

card_in_set('orcish oriflamme', 'S00').
card_original_type('orcish oriflamme'/'S00', 'Enchantment').
card_original_text('orcish oriflamme'/'S00', 'Attacking creatures you control get +1/+0.').
card_image_name('orcish oriflamme'/'S00', 'orcish oriflamme').
card_uid('orcish oriflamme'/'S00', 'S00:Orcish Oriflamme:orcish oriflamme').
card_rarity('orcish oriflamme'/'S00', 'Uncommon').
card_artist('orcish oriflamme'/'S00', 'Dan Frazier').
card_multiverse_id('orcish oriflamme'/'S00', '25540').

card_in_set('plains', 'S00').
card_original_type('plains'/'S00', 'Land').
card_original_text('plains'/'S00', 'W').
card_image_name('plains'/'S00', 'plains1').
card_uid('plains'/'S00', 'S00:Plains:plains1').
card_rarity('plains'/'S00', 'Basic Land').
card_artist('plains'/'S00', 'Douglas Shuler').
card_multiverse_id('plains'/'S00', '25453').

card_in_set('plains', 'S00').
card_original_type('plains'/'S00', 'Land').
card_original_text('plains'/'S00', 'W').
card_image_name('plains'/'S00', 'plains2').
card_uid('plains'/'S00', 'S00:Plains:plains2').
card_rarity('plains'/'S00', 'Basic Land').
card_artist('plains'/'S00', 'Fred Fields').
card_multiverse_id('plains'/'S00', '25454').

card_in_set('prodigal sorcerer', 'S00').
card_original_type('prodigal sorcerer'/'S00', 'Creature — Wizard').
card_original_text('prodigal sorcerer'/'S00', '{T}: Prodigal Sorcerer deals 1 damage to target creature or player.').
card_image_name('prodigal sorcerer'/'S00', 'prodigal sorcerer').
card_uid('prodigal sorcerer'/'S00', 'S00:Prodigal Sorcerer:prodigal sorcerer').
card_rarity('prodigal sorcerer'/'S00', 'Common').
card_artist('prodigal sorcerer'/'S00', 'Douglas Shuler').
card_flavor_text('prodigal sorcerer'/'S00', 'Occasionally members of the Institute of Arcane Study acquire a taste for worldly pleasures. Seldom do they have trouble finding employment.').
card_multiverse_id('prodigal sorcerer'/'S00', '25506').

card_in_set('python', 'S00').
card_original_type('python'/'S00', 'Creature — Snake').
card_original_text('python'/'S00', '').
card_image_name('python'/'S00', 'python').
card_uid('python'/'S00', 'S00:Python:python').
card_rarity('python'/'S00', 'Common').
card_artist('python'/'S00', 'Steve White').
card_flavor_text('python'/'S00', '\"How can you claim the gods are merciless when they robbed the snake of its limbs to give the other creatures a sporting chance?\"\n—Hakim, Loreweaver').
card_multiverse_id('python'/'S00', '25499').

card_in_set('rhox', 'S00').
card_original_type('rhox'/'S00', 'Creature — Beast').
card_original_text('rhox'/'S00', 'You may have Rhox deal combat damage to defending player as though it weren\'t blocked.\n{2}{G}: Regenerate Rhox.').
card_image_name('rhox'/'S00', 'rhox').
card_uid('rhox'/'S00', 'S00:Rhox:rhox').
card_rarity('rhox'/'S00', 'Rare').
card_artist('rhox'/'S00', 'Mark Zug').

card_in_set('rod of ruin', 'S00').
card_original_type('rod of ruin'/'S00', 'Artifact').
card_original_text('rod of ruin'/'S00', '{3}, {T}: Rod of Ruin deals 1 damage to target creature or player.').
card_image_name('rod of ruin'/'S00', 'rod of ruin').
card_uid('rod of ruin'/'S00', 'S00:Rod of Ruin:rod of ruin').
card_rarity('rod of ruin'/'S00', 'Uncommon').
card_artist('rod of ruin'/'S00', 'Christopher Rush').
card_multiverse_id('rod of ruin'/'S00', '25529').

card_in_set('royal falcon', 'S00').
card_original_type('royal falcon'/'S00', 'Creature — Bird').
card_original_text('royal falcon'/'S00', 'Flying').
card_image_name('royal falcon'/'S00', 'royal falcon').
card_uid('royal falcon'/'S00', 'S00:Royal Falcon:royal falcon').
card_rarity('royal falcon'/'S00', 'Common').
card_artist('royal falcon'/'S00', 'Carl Critchlow').
card_flavor_text('royal falcon'/'S00', 'Hunter by instinct, weapon by training.').
card_multiverse_id('royal falcon'/'S00', '25459').

card_in_set('samite healer', 'S00').
card_original_type('samite healer'/'S00', 'Creature — Cleric').
card_original_text('samite healer'/'S00', '{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.').
card_image_name('samite healer'/'S00', 'samite healer').
card_uid('samite healer'/'S00', 'S00:Samite Healer:samite healer').
card_rarity('samite healer'/'S00', 'Common').
card_artist('samite healer'/'S00', 'Tom Wänerstrand').
card_flavor_text('samite healer'/'S00', 'Healers ultimately acquire the divine gifts of spiritual and physical wholeness. The most devout are also granted the ability to pass physical wholeness on to others.').
card_multiverse_id('samite healer'/'S00', '25533').

card_in_set('scathe zombies', 'S00').
card_original_type('scathe zombies'/'S00', 'Creature — Zombie').
card_original_text('scathe zombies'/'S00', '').
card_image_name('scathe zombies'/'S00', 'scathe zombies').
card_uid('scathe zombies'/'S00', 'S00:Scathe Zombies:scathe zombies').
card_rarity('scathe zombies'/'S00', 'Common').
card_artist('scathe zombies'/'S00', 'Jesper Myrfors').
card_flavor_text('scathe zombies'/'S00', '\"They groaned, they stirred, they all uprose,\nNor spake, nor moved their eyes;\nIt had been strange, even in a dream,\nTo have seen those dead men rise.\"\n—Samuel Coleridge,\n\"The Rime of the Ancient Mariner\"').
card_multiverse_id('scathe zombies'/'S00', '25487').

card_in_set('sea eagle', 'S00').
card_original_type('sea eagle'/'S00', 'Creature — Bird').
card_original_text('sea eagle'/'S00', 'Flying').
card_image_name('sea eagle'/'S00', 'sea eagle').
card_uid('sea eagle'/'S00', 'S00:Sea Eagle:sea eagle').
card_rarity('sea eagle'/'S00', 'Common').
card_artist('sea eagle'/'S00', 'Anthony S. Waters').
card_flavor_text('sea eagle'/'S00', 'Where air meets water, fish meets talon.').
card_multiverse_id('sea eagle'/'S00', '25460').

card_in_set('shock', 'S00').
card_original_type('shock'/'S00', 'Instant').
card_original_text('shock'/'S00', 'Shock deals 2 damage to target creature or player.').
card_image_name('shock'/'S00', 'shock').
card_uid('shock'/'S00', 'S00:Shock:shock').
card_rarity('shock'/'S00', 'Common').
card_artist('shock'/'S00', 'Randy Gallegos').
card_flavor_text('shock'/'S00', 'Lightning tethers souls to the world.\n—Kor saying').
card_multiverse_id('shock'/'S00', '25501').

card_in_set('soul net', 'S00').
card_original_type('soul net'/'S00', 'Artifact').
card_original_text('soul net'/'S00', 'Whenever a creature is put into a graveyard from play, you may pay {1}. If you do, you gain 1 life.').
card_image_name('soul net'/'S00', 'soul net').
card_uid('soul net'/'S00', 'S00:Soul Net:soul net').
card_rarity('soul net'/'S00', 'Uncommon').
card_artist('soul net'/'S00', 'Andrew Robinson').
card_multiverse_id('soul net'/'S00', '25543').

card_in_set('spined wurm', 'S00').
card_original_type('spined wurm'/'S00', 'Creature — Wurm').
card_original_text('spined wurm'/'S00', '').
card_image_name('spined wurm'/'S00', 'spined wurm').
card_uid('spined wurm'/'S00', 'S00:Spined Wurm:spined wurm').
card_rarity('spined wurm'/'S00', 'Common').
card_artist('spined wurm'/'S00', 'Keith Parkinson').
card_flavor_text('spined wurm'/'S00', '\"As it moved, the wurm\'s spines gathered up bits of flowstone, which took the shapes of dead villagers\' heads. Each head spoke a single sound, but if taken together, they said, ‘Alas for the living.\'\"\n—Dal myth of the wurm').
card_multiverse_id('spined wurm'/'S00', '25496').

card_in_set('stone rain', 'S00').
card_original_type('stone rain'/'S00', 'Sorcery').
card_original_text('stone rain'/'S00', 'Destroy target land.').
card_image_name('stone rain'/'S00', 'stone rain').
card_uid('stone rain'/'S00', 'S00:Stone Rain:stone rain').
card_rarity('stone rain'/'S00', 'Common').
card_artist('stone rain'/'S00', 'John Matson').
card_multiverse_id('stone rain'/'S00', '25497').

card_in_set('swamp', 'S00').
card_original_type('swamp'/'S00', 'Land').
card_original_text('swamp'/'S00', 'B').
card_image_name('swamp'/'S00', 'swamp1').
card_uid('swamp'/'S00', 'S00:Swamp:swamp1').
card_rarity('swamp'/'S00', 'Basic Land').
card_artist('swamp'/'S00', 'Dan Frazier').
card_multiverse_id('swamp'/'S00', '25478').

card_in_set('swamp', 'S00').
card_original_type('swamp'/'S00', 'Land').
card_original_text('swamp'/'S00', 'B').
card_image_name('swamp'/'S00', 'swamp2').
card_uid('swamp'/'S00', 'S00:Swamp:swamp2').
card_rarity('swamp'/'S00', 'Basic Land').
card_artist('swamp'/'S00', 'Douglas Shuler').
card_multiverse_id('swamp'/'S00', '25479').

card_in_set('terror', 'S00').
card_original_type('terror'/'S00', 'Instant').
card_original_text('terror'/'S00', 'Destroy target nonartifact, nonblack creature. It can\'t be regenerated.').
card_image_name('terror'/'S00', 'terror').
card_uid('terror'/'S00', 'S00:Terror:terror').
card_rarity('terror'/'S00', 'Common').
card_artist('terror'/'S00', 'Ron Spencer').
card_multiverse_id('terror'/'S00', '25504').

card_in_set('time ebb', 'S00').
card_original_type('time ebb'/'S00', 'Sorcery').
card_original_text('time ebb'/'S00', 'Put target creature on top of its owner\'s library.').
card_image_name('time ebb'/'S00', 'time ebb').
card_uid('time ebb'/'S00', 'S00:Time Ebb:time ebb').
card_rarity('time ebb'/'S00', 'Common').
card_artist('time ebb'/'S00', 'Alan Rabinowitz').
card_flavor_text('time ebb'/'S00', 'Like the tide, time both ebbs and flows.').
card_multiverse_id('time ebb'/'S00', '25462').

card_in_set('trained orgg', 'S00').
card_original_type('trained orgg'/'S00', 'Creature — Beast').
card_original_text('trained orgg'/'S00', '').
card_image_name('trained orgg'/'S00', 'trained orgg').
card_uid('trained orgg'/'S00', 'S00:Trained Orgg:trained orgg').
card_rarity('trained orgg'/'S00', 'Rare').
card_artist('trained orgg'/'S00', 'Eric Peterson').
card_flavor_text('trained orgg'/'S00', 'All orggs know how to kill; training teaches them what to kill.').
card_multiverse_id('trained orgg'/'S00', '25494').

card_in_set('venerable monk', 'S00').
card_original_type('venerable monk'/'S00', 'Creature — Cleric').
card_original_text('venerable monk'/'S00', 'When Venerable Monk comes into play, you gain 2 life.').
card_image_name('venerable monk'/'S00', 'venerable monk').
card_uid('venerable monk'/'S00', 'S00:Venerable Monk:venerable monk').
card_rarity('venerable monk'/'S00', 'Common').
card_artist('venerable monk'/'S00', 'D. Alexander Gregory').
card_flavor_text('venerable monk'/'S00', 'His presence brings not only a strong arm but also renewed hope.').
card_multiverse_id('venerable monk'/'S00', '25515').

card_in_set('vizzerdrix', 'S00').
card_original_type('vizzerdrix'/'S00', 'Creature — Beast').
card_original_text('vizzerdrix'/'S00', '').
card_image_name('vizzerdrix'/'S00', 'vizzerdrix').
card_uid('vizzerdrix'/'S00', 'S00:Vizzerdrix:vizzerdrix').
card_rarity('vizzerdrix'/'S00', 'Rare').
card_artist('vizzerdrix'/'S00', 'Eric Peterson').
card_flavor_text('vizzerdrix'/'S00', 'Its swamp cousin may be meaner, but the vizzerdrix is much bigger.').
card_multiverse_id('vizzerdrix'/'S00', '25463').

card_in_set('wild griffin', 'S00').
card_original_type('wild griffin'/'S00', 'Creature — Griffin').
card_original_text('wild griffin'/'S00', 'Flying').
card_image_name('wild griffin'/'S00', 'wild griffin').
card_uid('wild griffin'/'S00', 'S00:Wild Griffin:wild griffin').
card_rarity('wild griffin'/'S00', 'Common').
card_artist('wild griffin'/'S00', 'Jeff Miracola').
card_flavor_text('wild griffin'/'S00', 'Two little goblins, out in the sun. Down came a griffin, and then there was one.').
card_multiverse_id('wild griffin'/'S00', '25449').

card_in_set('willow elf', 'S00').
card_original_type('willow elf'/'S00', 'Creature — Elf').
card_original_text('willow elf'/'S00', '').
card_image_name('willow elf'/'S00', 'willow elf').
card_uid('willow elf'/'S00', 'S00:Willow Elf:willow elf').
card_rarity('willow elf'/'S00', 'Common').
card_artist('willow elf'/'S00', 'DiTerlizzi').
card_flavor_text('willow elf'/'S00', 'The forest lives in the elf as the elf lives in the forest.').
card_multiverse_id('willow elf'/'S00', '25484').

card_in_set('wind drake', 'S00').
card_original_type('wind drake'/'S00', 'Creature — Drake').
card_original_text('wind drake'/'S00', 'Flying').
card_image_name('wind drake'/'S00', 'wind drake').
card_uid('wind drake'/'S00', 'S00:Wind Drake:wind drake').
card_rarity('wind drake'/'S00', 'Common').
card_artist('wind drake'/'S00', 'Zina Saunders').
card_flavor_text('wind drake'/'S00', '\"No bird soars too high, if he soars with his own wings.\"\n—William Blake,\nThe Marriage of Heaven and Hell').
card_multiverse_id('wind drake'/'S00', '25448').
