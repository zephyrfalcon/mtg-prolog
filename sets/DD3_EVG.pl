% Duel Decks Anthology, Elves vs. Goblins

set('DD3_EVG').
set_name('DD3_EVG', 'Duel Decks Anthology, Elves vs. Goblins').
set_release_date('DD3_EVG', '2014-12-05').
set_border('DD3_EVG', 'black').
set_type('DD3_EVG', 'duel deck').

card_in_set('akki coalflinger', 'DD3_EVG').
card_original_type('akki coalflinger'/'DD3_EVG', 'Creature — Goblin Shaman').
card_original_text('akki coalflinger'/'DD3_EVG', 'First strike\n{R}, {T}: Attacking creatures gain first strike until end of turn.').
card_image_name('akki coalflinger'/'DD3_EVG', 'akki coalflinger').
card_uid('akki coalflinger'/'DD3_EVG', 'DD3_EVG:Akki Coalflinger:akki coalflinger').
card_rarity('akki coalflinger'/'DD3_EVG', 'Uncommon').
card_artist('akki coalflinger'/'DD3_EVG', 'Nottsuo').
card_number('akki coalflinger'/'DD3_EVG', '33').
card_flavor_text('akki coalflinger'/'DD3_EVG', 'No matter where you find them, goblins love rocks.').
card_multiverse_id('akki coalflinger'/'DD3_EVG', '393941').

card_in_set('allosaurus rider', 'DD3_EVG').
card_original_type('allosaurus rider'/'DD3_EVG', 'Creature — Elf Warrior').
card_original_text('allosaurus rider'/'DD3_EVG', 'You may exile two green cards from your hand rather than pay Allosaurus Rider\'s mana cost.\nAllosaurus Rider\'s power and toughness are each equal to 1 plus the number of lands you control.').
card_image_name('allosaurus rider'/'DD3_EVG', 'allosaurus rider').
card_uid('allosaurus rider'/'DD3_EVG', 'DD3_EVG:Allosaurus Rider:allosaurus rider').
card_rarity('allosaurus rider'/'DD3_EVG', 'Rare').
card_artist('allosaurus rider'/'DD3_EVG', 'Daren Bader').
card_number('allosaurus rider'/'DD3_EVG', '2').
card_multiverse_id('allosaurus rider'/'DD3_EVG', '393942').

card_in_set('ambush commander', 'DD3_EVG').
card_original_type('ambush commander'/'DD3_EVG', 'Creature — Elf').
card_original_text('ambush commander'/'DD3_EVG', 'Forests you control are 1/1 green Elf creatures that are still lands.\n{1}{G}, Sacrifice an Elf: Target creature gets +3/+3 until end of turn.').
card_image_name('ambush commander'/'DD3_EVG', 'ambush commander').
card_uid('ambush commander'/'DD3_EVG', 'DD3_EVG:Ambush Commander:ambush commander').
card_rarity('ambush commander'/'DD3_EVG', 'Rare').
card_artist('ambush commander'/'DD3_EVG', 'Lucio Parrillo').
card_number('ambush commander'/'DD3_EVG', '1').
card_multiverse_id('ambush commander'/'DD3_EVG', '393943').

card_in_set('boggart shenanigans', 'DD3_EVG').
card_original_type('boggart shenanigans'/'DD3_EVG', 'Tribal Enchantment — Goblin').
card_original_text('boggart shenanigans'/'DD3_EVG', 'Whenever another Goblin you control is put into a graveyard from the battlefield, you may have Boggart Shenanigans deal 1 damage to target player.').
card_image_name('boggart shenanigans'/'DD3_EVG', 'boggart shenanigans').
card_uid('boggart shenanigans'/'DD3_EVG', 'DD3_EVG:Boggart Shenanigans:boggart shenanigans').
card_rarity('boggart shenanigans'/'DD3_EVG', 'Uncommon').
card_artist('boggart shenanigans'/'DD3_EVG', 'Warren Mahy').
card_number('boggart shenanigans'/'DD3_EVG', '54').
card_flavor_text('boggart shenanigans'/'DD3_EVG', 'Boggarts revel in discovering new sensations, from the texture of an otter pellet to the squeak of a dying warren mate.').
card_multiverse_id('boggart shenanigans'/'DD3_EVG', '393944').

card_in_set('clickslither', 'DD3_EVG').
card_original_type('clickslither'/'DD3_EVG', 'Creature — Insect').
card_original_text('clickslither'/'DD3_EVG', 'Haste\nSacrifice a Goblin: Clickslither gets +2/+2 and gains trample until end of turn.').
card_image_name('clickslither'/'DD3_EVG', 'clickslither').
card_uid('clickslither'/'DD3_EVG', 'DD3_EVG:Clickslither:clickslither').
card_rarity('clickslither'/'DD3_EVG', 'Rare').
card_artist('clickslither'/'DD3_EVG', 'Kev Walker').
card_number('clickslither'/'DD3_EVG', '34').
card_flavor_text('clickslither'/'DD3_EVG', 'The least popular goblins get the outer caves.').
card_multiverse_id('clickslither'/'DD3_EVG', '393945').

card_in_set('elvish eulogist', 'DD3_EVG').
card_original_type('elvish eulogist'/'DD3_EVG', 'Creature — Elf Shaman').
card_original_text('elvish eulogist'/'DD3_EVG', 'Sacrifice Elvish Eulogist: You gain 1 life for each Elf card in your graveyard.').
card_image_name('elvish eulogist'/'DD3_EVG', 'elvish eulogist').
card_uid('elvish eulogist'/'DD3_EVG', 'DD3_EVG:Elvish Eulogist:elvish eulogist').
card_rarity('elvish eulogist'/'DD3_EVG', 'Common').
card_artist('elvish eulogist'/'DD3_EVG', 'Ben Thompson').
card_number('elvish eulogist'/'DD3_EVG', '3').
card_flavor_text('elvish eulogist'/'DD3_EVG', '\"No matter how adept our artistic skill, our effigies can never hope to capture the vibrant beauty of a living elf. Perhaps that is truly why we mourn.\"').
card_multiverse_id('elvish eulogist'/'DD3_EVG', '393946').

card_in_set('elvish harbinger', 'DD3_EVG').
card_original_type('elvish harbinger'/'DD3_EVG', 'Creature — Elf Druid').
card_original_text('elvish harbinger'/'DD3_EVG', 'When Elvish Harbinger enters the battlefield, you may search your library for an Elf card, reveal it, then shuffle your library and put that card on top of it.\n{T}: Add one mana of any color to your mana pool.').
card_image_name('elvish harbinger'/'DD3_EVG', 'elvish harbinger').
card_uid('elvish harbinger'/'DD3_EVG', 'DD3_EVG:Elvish Harbinger:elvish harbinger').
card_rarity('elvish harbinger'/'DD3_EVG', 'Uncommon').
card_artist('elvish harbinger'/'DD3_EVG', 'Larry MacDougall').
card_number('elvish harbinger'/'DD3_EVG', '4').
card_multiverse_id('elvish harbinger'/'DD3_EVG', '393947').

card_in_set('elvish promenade', 'DD3_EVG').
card_original_type('elvish promenade'/'DD3_EVG', 'Tribal Sorcery — Elf').
card_original_text('elvish promenade'/'DD3_EVG', 'Put a 1/1 green Elf Warrior creature token onto the battlefield for each Elf you control.').
card_image_name('elvish promenade'/'DD3_EVG', 'elvish promenade').
card_uid('elvish promenade'/'DD3_EVG', 'DD3_EVG:Elvish Promenade:elvish promenade').
card_rarity('elvish promenade'/'DD3_EVG', 'Uncommon').
card_artist('elvish promenade'/'DD3_EVG', 'Steve Ellis').
card_number('elvish promenade'/'DD3_EVG', '20').
card_flavor_text('elvish promenade'/'DD3_EVG', 'The faultless and immaculate castes form the lower tiers of elvish society, with the exquisite caste above them. At the pinnacle is the perfect, a consummate blend of aristocrat and predator.').
card_multiverse_id('elvish promenade'/'DD3_EVG', '393948').

card_in_set('elvish warrior', 'DD3_EVG').
card_original_type('elvish warrior'/'DD3_EVG', 'Creature — Elf Warrior').
card_original_text('elvish warrior'/'DD3_EVG', '').
card_image_name('elvish warrior'/'DD3_EVG', 'elvish warrior').
card_uid('elvish warrior'/'DD3_EVG', 'DD3_EVG:Elvish Warrior:elvish warrior').
card_rarity('elvish warrior'/'DD3_EVG', 'Common').
card_artist('elvish warrior'/'DD3_EVG', 'Christopher Moeller').
card_number('elvish warrior'/'DD3_EVG', '5').
card_flavor_text('elvish warrior'/'DD3_EVG', '\"My tales of war are the stories most asked for around the fires at night, but they\'re the ones I care least to tell.\"').
card_multiverse_id('elvish warrior'/'DD3_EVG', '393949').

card_in_set('emberwilde augur', 'DD3_EVG').
card_original_type('emberwilde augur'/'DD3_EVG', 'Creature — Goblin Shaman').
card_original_text('emberwilde augur'/'DD3_EVG', 'Sacrifice Emberwilde Augur: Emberwilde Augur deals 3 damage to target player. Activate this ability only during your upkeep.').
card_image_name('emberwilde augur'/'DD3_EVG', 'emberwilde augur').
card_uid('emberwilde augur'/'DD3_EVG', 'DD3_EVG:Emberwilde Augur:emberwilde augur').
card_rarity('emberwilde augur'/'DD3_EVG', 'Common').
card_artist('emberwilde augur'/'DD3_EVG', 'Brandon Kitkouski').
card_number('emberwilde augur'/'DD3_EVG', '35').
card_flavor_text('emberwilde augur'/'DD3_EVG', 'Legends say a djinn gave the goblins a gift they could never hope to master.').
card_multiverse_id('emberwilde augur'/'DD3_EVG', '393950').

card_in_set('flamewave invoker', 'DD3_EVG').
card_original_type('flamewave invoker'/'DD3_EVG', 'Creature — Goblin Mutant').
card_original_text('flamewave invoker'/'DD3_EVG', '{7}{R}: Flamewave Invoker deals 5 damage to target player.').
card_image_name('flamewave invoker'/'DD3_EVG', 'flamewave invoker').
card_uid('flamewave invoker'/'DD3_EVG', 'DD3_EVG:Flamewave Invoker:flamewave invoker').
card_rarity('flamewave invoker'/'DD3_EVG', 'Uncommon').
card_artist('flamewave invoker'/'DD3_EVG', 'Dave Dorman').
card_number('flamewave invoker'/'DD3_EVG', '36').
card_flavor_text('flamewave invoker'/'DD3_EVG', 'Inside even the humblest goblin lurks the potential for far greater things—and far worse.').
card_multiverse_id('flamewave invoker'/'DD3_EVG', '393951').

card_in_set('forest', 'DD3_EVG').
card_original_type('forest'/'DD3_EVG', 'Basic Land — Forest').
card_original_text('forest'/'DD3_EVG', 'G').
card_image_name('forest'/'DD3_EVG', 'forest1').
card_uid('forest'/'DD3_EVG', 'DD3_EVG:Forest:forest1').
card_rarity('forest'/'DD3_EVG', 'Basic Land').
card_artist('forest'/'DD3_EVG', 'Glen Angus').
card_number('forest'/'DD3_EVG', '28').
card_multiverse_id('forest'/'DD3_EVG', '393955').

card_in_set('forest', 'DD3_EVG').
card_original_type('forest'/'DD3_EVG', 'Basic Land — Forest').
card_original_text('forest'/'DD3_EVG', 'G').
card_image_name('forest'/'DD3_EVG', 'forest2').
card_uid('forest'/'DD3_EVG', 'DD3_EVG:Forest:forest2').
card_rarity('forest'/'DD3_EVG', 'Basic Land').
card_artist('forest'/'DD3_EVG', 'John Avon').
card_number('forest'/'DD3_EVG', '29').
card_multiverse_id('forest'/'DD3_EVG', '393953').

card_in_set('forest', 'DD3_EVG').
card_original_type('forest'/'DD3_EVG', 'Basic Land — Forest').
card_original_text('forest'/'DD3_EVG', 'G').
card_image_name('forest'/'DD3_EVG', 'forest3').
card_uid('forest'/'DD3_EVG', 'DD3_EVG:Forest:forest3').
card_rarity('forest'/'DD3_EVG', 'Basic Land').
card_artist('forest'/'DD3_EVG', 'John Avon').
card_number('forest'/'DD3_EVG', '30').
card_multiverse_id('forest'/'DD3_EVG', '393952').

card_in_set('forest', 'DD3_EVG').
card_original_type('forest'/'DD3_EVG', 'Basic Land — Forest').
card_original_text('forest'/'DD3_EVG', 'G').
card_image_name('forest'/'DD3_EVG', 'forest4').
card_uid('forest'/'DD3_EVG', 'DD3_EVG:Forest:forest4').
card_rarity('forest'/'DD3_EVG', 'Basic Land').
card_artist('forest'/'DD3_EVG', 'Rob Alexander').
card_number('forest'/'DD3_EVG', '31').
card_multiverse_id('forest'/'DD3_EVG', '393954').

card_in_set('forgotten cave', 'DD3_EVG').
card_original_type('forgotten cave'/'DD3_EVG', 'Land').
card_original_text('forgotten cave'/'DD3_EVG', 'Forgotten Cave enters the battlefield tapped.\n{T}: Add {R} to your mana pool.\nCycling {R} ({R}, Discard this card: Draw a card.)').
card_image_name('forgotten cave'/'DD3_EVG', 'forgotten cave').
card_uid('forgotten cave'/'DD3_EVG', 'DD3_EVG:Forgotten Cave:forgotten cave').
card_rarity('forgotten cave'/'DD3_EVG', 'Common').
card_artist('forgotten cave'/'DD3_EVG', 'Tony Szczudlo').
card_number('forgotten cave'/'DD3_EVG', '57').
card_multiverse_id('forgotten cave'/'DD3_EVG', '393956').

card_in_set('gempalm incinerator', 'DD3_EVG').
card_original_type('gempalm incinerator'/'DD3_EVG', 'Creature — Goblin').
card_original_text('gempalm incinerator'/'DD3_EVG', 'Cycling {1}{R} ({1}{R}, Discard this card: Draw a card.)\nWhen you cycle Gempalm Incinerator, you may have it deal X damage to target creature, where X is the number of Goblins on the battlefield.').
card_image_name('gempalm incinerator'/'DD3_EVG', 'gempalm incinerator').
card_uid('gempalm incinerator'/'DD3_EVG', 'DD3_EVG:Gempalm Incinerator:gempalm incinerator').
card_rarity('gempalm incinerator'/'DD3_EVG', 'Uncommon').
card_artist('gempalm incinerator'/'DD3_EVG', 'Luca Zontini').
card_number('gempalm incinerator'/'DD3_EVG', '37').
card_multiverse_id('gempalm incinerator'/'DD3_EVG', '393957').

card_in_set('gempalm strider', 'DD3_EVG').
card_original_type('gempalm strider'/'DD3_EVG', 'Creature — Elf').
card_original_text('gempalm strider'/'DD3_EVG', 'Cycling {2}{G}{G} ({2}{G}{G}, Discard this card: Draw a card.)\nWhen you cycle Gempalm Strider, Elf creatures get +2/+2 until end of turn.').
card_image_name('gempalm strider'/'DD3_EVG', 'gempalm strider').
card_uid('gempalm strider'/'DD3_EVG', 'DD3_EVG:Gempalm Strider:gempalm strider').
card_rarity('gempalm strider'/'DD3_EVG', 'Uncommon').
card_artist('gempalm strider'/'DD3_EVG', 'Tim Hildebrandt').
card_number('gempalm strider'/'DD3_EVG', '6').
card_multiverse_id('gempalm strider'/'DD3_EVG', '393958').

card_in_set('giant growth', 'DD3_EVG').
card_original_type('giant growth'/'DD3_EVG', 'Instant').
card_original_text('giant growth'/'DD3_EVG', 'Target creature gets +3/+3 until end of turn.').
card_image_name('giant growth'/'DD3_EVG', 'giant growth').
card_uid('giant growth'/'DD3_EVG', 'DD3_EVG:Giant Growth:giant growth').
card_rarity('giant growth'/'DD3_EVG', 'Common').
card_artist('giant growth'/'DD3_EVG', 'Matt Cavotta').
card_number('giant growth'/'DD3_EVG', '21').
card_multiverse_id('giant growth'/'DD3_EVG', '393959').

card_in_set('goblin burrows', 'DD3_EVG').
card_original_type('goblin burrows'/'DD3_EVG', 'Land').
card_original_text('goblin burrows'/'DD3_EVG', '{T}: Add {1} to your mana pool.\n{1}{R}, {T}: Target Goblin creature gets +2/+0 until end of turn.').
card_image_name('goblin burrows'/'DD3_EVG', 'goblin burrows').
card_uid('goblin burrows'/'DD3_EVG', 'DD3_EVG:Goblin Burrows:goblin burrows').
card_rarity('goblin burrows'/'DD3_EVG', 'Uncommon').
card_artist('goblin burrows'/'DD3_EVG', 'David Martin').
card_number('goblin burrows'/'DD3_EVG', '58').
card_multiverse_id('goblin burrows'/'DD3_EVG', '393960').

card_in_set('goblin cohort', 'DD3_EVG').
card_original_type('goblin cohort'/'DD3_EVG', 'Creature — Goblin Warrior').
card_original_text('goblin cohort'/'DD3_EVG', 'Goblin Cohort can\'t attack unless you\'ve cast a creature spell this turn.').
card_image_name('goblin cohort'/'DD3_EVG', 'goblin cohort').
card_uid('goblin cohort'/'DD3_EVG', 'DD3_EVG:Goblin Cohort:goblin cohort').
card_rarity('goblin cohort'/'DD3_EVG', 'Common').
card_artist('goblin cohort'/'DD3_EVG', 'Darrell Riche').
card_number('goblin cohort'/'DD3_EVG', '38').
card_flavor_text('goblin cohort'/'DD3_EVG', 'Akki shells provided good protection when downhill charging became headlong tumbling.').
card_multiverse_id('goblin cohort'/'DD3_EVG', '393961').

card_in_set('goblin matron', 'DD3_EVG').
card_original_type('goblin matron'/'DD3_EVG', 'Creature — Goblin').
card_original_text('goblin matron'/'DD3_EVG', 'When Goblin Matron enters the battlefield, you may search your library for a Goblin card, reveal that card, and put it into your hand. If you do, shuffle your library.').
card_image_name('goblin matron'/'DD3_EVG', 'goblin matron').
card_uid('goblin matron'/'DD3_EVG', 'DD3_EVG:Goblin Matron:goblin matron').
card_rarity('goblin matron'/'DD3_EVG', 'Uncommon').
card_artist('goblin matron'/'DD3_EVG', 'Daniel Gelon').
card_number('goblin matron'/'DD3_EVG', '39').
card_flavor_text('goblin matron'/'DD3_EVG', 'There\'s always room for one more.').
card_multiverse_id('goblin matron'/'DD3_EVG', '393962').

card_in_set('goblin ringleader', 'DD3_EVG').
card_original_type('goblin ringleader'/'DD3_EVG', 'Creature — Goblin').
card_original_text('goblin ringleader'/'DD3_EVG', 'Haste\nWhen Goblin Ringleader enters the battlefield, reveal the top four cards of your library. Put all Goblin cards revealed this way into your hand and the rest on the bottom of your library in any order.').
card_image_name('goblin ringleader'/'DD3_EVG', 'goblin ringleader').
card_uid('goblin ringleader'/'DD3_EVG', 'DD3_EVG:Goblin Ringleader:goblin ringleader').
card_rarity('goblin ringleader'/'DD3_EVG', 'Uncommon').
card_artist('goblin ringleader'/'DD3_EVG', 'Mark Romanoski').
card_number('goblin ringleader'/'DD3_EVG', '40').
card_multiverse_id('goblin ringleader'/'DD3_EVG', '393963').

card_in_set('goblin sledder', 'DD3_EVG').
card_original_type('goblin sledder'/'DD3_EVG', 'Creature — Goblin').
card_original_text('goblin sledder'/'DD3_EVG', 'Sacrifice a Goblin: Target creature gets +1/+1 until end of turn.').
card_image_name('goblin sledder'/'DD3_EVG', 'goblin sledder').
card_uid('goblin sledder'/'DD3_EVG', 'DD3_EVG:Goblin Sledder:goblin sledder').
card_rarity('goblin sledder'/'DD3_EVG', 'Common').
card_artist('goblin sledder'/'DD3_EVG', 'Ron Spencer').
card_number('goblin sledder'/'DD3_EVG', '41').
card_flavor_text('goblin sledder'/'DD3_EVG', '\"Let\'s play ‘sled.\' Here\'s how it works: you\'re the sled.\"').
card_multiverse_id('goblin sledder'/'DD3_EVG', '393964').

card_in_set('goblin warchief', 'DD3_EVG').
card_original_type('goblin warchief'/'DD3_EVG', 'Creature — Goblin Warrior').
card_original_text('goblin warchief'/'DD3_EVG', 'Goblin spells you cast cost {1} less to cast.\nGoblin creatures you control have haste.').
card_image_name('goblin warchief'/'DD3_EVG', 'goblin warchief').
card_uid('goblin warchief'/'DD3_EVG', 'DD3_EVG:Goblin Warchief:goblin warchief').
card_rarity('goblin warchief'/'DD3_EVG', 'Uncommon').
card_artist('goblin warchief'/'DD3_EVG', 'Greg & Tim Hildebrandt').
card_number('goblin warchief'/'DD3_EVG', '42').
card_flavor_text('goblin warchief'/'DD3_EVG', 'They poured from the Skirk Ridge like lava, burning and devouring everything in their path.').
card_multiverse_id('goblin warchief'/'DD3_EVG', '393965').

card_in_set('harmonize', 'DD3_EVG').
card_original_type('harmonize'/'DD3_EVG', 'Sorcery').
card_original_text('harmonize'/'DD3_EVG', 'Draw three cards.').
card_image_name('harmonize'/'DD3_EVG', 'harmonize').
card_uid('harmonize'/'DD3_EVG', 'DD3_EVG:Harmonize:harmonize').
card_rarity('harmonize'/'DD3_EVG', 'Uncommon').
card_artist('harmonize'/'DD3_EVG', 'Rob Alexander').
card_number('harmonize'/'DD3_EVG', '22').
card_flavor_text('harmonize'/'DD3_EVG', '\"Life\'s greatest lessons don\'t come from focus or concentration. They come from breathing and simply noticing.\"\n—Seton, centaur druid').
card_multiverse_id('harmonize'/'DD3_EVG', '393966').

card_in_set('heedless one', 'DD3_EVG').
card_original_type('heedless one'/'DD3_EVG', 'Creature — Elf Avatar').
card_original_text('heedless one'/'DD3_EVG', 'Trample\nHeedless One\'s power and toughness are each equal to the number of Elves on the battlefield.').
card_image_name('heedless one'/'DD3_EVG', 'heedless one').
card_uid('heedless one'/'DD3_EVG', 'DD3_EVG:Heedless One:heedless one').
card_rarity('heedless one'/'DD3_EVG', 'Uncommon').
card_artist('heedless one'/'DD3_EVG', 'Mark Zug').
card_number('heedless one'/'DD3_EVG', '7').
card_flavor_text('heedless one'/'DD3_EVG', '\"Channel your vitality through me.\"').
card_multiverse_id('heedless one'/'DD3_EVG', '393967').

card_in_set('ib halfheart, goblin tactician', 'DD3_EVG').
card_original_type('ib halfheart, goblin tactician'/'DD3_EVG', 'Legendary Creature — Goblin Advisor').
card_original_text('ib halfheart, goblin tactician'/'DD3_EVG', 'Whenever another Goblin you control becomes blocked, sacrifice it. If you do, it deals 4 damage to each creature blocking it.\nSacrifice two Mountains: Put two 1/1 red Goblin creature tokens onto the battlefield.').
card_image_name('ib halfheart, goblin tactician'/'DD3_EVG', 'ib halfheart, goblin tactician').
card_uid('ib halfheart, goblin tactician'/'DD3_EVG', 'DD3_EVG:Ib Halfheart, Goblin Tactician:ib halfheart, goblin tactician').
card_rarity('ib halfheart, goblin tactician'/'DD3_EVG', 'Rare').
card_artist('ib halfheart, goblin tactician'/'DD3_EVG', 'Wayne Reynolds').
card_number('ib halfheart, goblin tactician'/'DD3_EVG', '43').
card_flavor_text('ib halfheart, goblin tactician'/'DD3_EVG', '\"Everybody but me—CHARGE!\"').
card_multiverse_id('ib halfheart, goblin tactician'/'DD3_EVG', '393968').

card_in_set('imperious perfect', 'DD3_EVG').
card_original_type('imperious perfect'/'DD3_EVG', 'Creature — Elf Warrior').
card_original_text('imperious perfect'/'DD3_EVG', 'Other Elf creatures you control get +1/+1.\n{G}, {T}: Put a 1/1 green Elf Warrior creature token onto the battlefield.').
card_image_name('imperious perfect'/'DD3_EVG', 'imperious perfect').
card_uid('imperious perfect'/'DD3_EVG', 'DD3_EVG:Imperious Perfect:imperious perfect').
card_rarity('imperious perfect'/'DD3_EVG', 'Uncommon').
card_artist('imperious perfect'/'DD3_EVG', 'Scott M. Fischer').
card_number('imperious perfect'/'DD3_EVG', '8').
card_flavor_text('imperious perfect'/'DD3_EVG', 'In a culture of beauty, the most beautiful are worshipped as gods.').
card_multiverse_id('imperious perfect'/'DD3_EVG', '393969').

card_in_set('llanowar elves', 'DD3_EVG').
card_original_type('llanowar elves'/'DD3_EVG', 'Creature — Elf Druid').
card_original_text('llanowar elves'/'DD3_EVG', '{T}: Add {G} to your mana pool.').
card_image_name('llanowar elves'/'DD3_EVG', 'llanowar elves').
card_uid('llanowar elves'/'DD3_EVG', 'DD3_EVG:Llanowar Elves:llanowar elves').
card_rarity('llanowar elves'/'DD3_EVG', 'Common').
card_artist('llanowar elves'/'DD3_EVG', 'Kev Walker').
card_number('llanowar elves'/'DD3_EVG', '9').
card_flavor_text('llanowar elves'/'DD3_EVG', 'One bone broken for every twig snapped underfoot.\n—Llanowar penalty for trespassing').
card_multiverse_id('llanowar elves'/'DD3_EVG', '393970').

card_in_set('lys alana huntmaster', 'DD3_EVG').
card_original_type('lys alana huntmaster'/'DD3_EVG', 'Creature — Elf Warrior').
card_original_text('lys alana huntmaster'/'DD3_EVG', 'Whenever you cast an Elf spell, you may put a 1/1 green Elf Warrior creature token onto the battlefield.').
card_image_name('lys alana huntmaster'/'DD3_EVG', 'lys alana huntmaster').
card_uid('lys alana huntmaster'/'DD3_EVG', 'DD3_EVG:Lys Alana Huntmaster:lys alana huntmaster').
card_rarity('lys alana huntmaster'/'DD3_EVG', 'Common').
card_artist('lys alana huntmaster'/'DD3_EVG', 'Pete Venters').
card_number('lys alana huntmaster'/'DD3_EVG', '10').
card_flavor_text('lys alana huntmaster'/'DD3_EVG', 'From the highest tiers of Dawn\'s Light Palace to the deepest shade of Wren\'s Run, the silver notes of the horn shimmer through the air, and all who hear it feel its pull.').
card_multiverse_id('lys alana huntmaster'/'DD3_EVG', '393971').

card_in_set('mogg fanatic', 'DD3_EVG').
card_original_type('mogg fanatic'/'DD3_EVG', 'Creature — Goblin').
card_original_text('mogg fanatic'/'DD3_EVG', 'Sacrifice Mogg Fanatic: Mogg Fanatic deals 1 damage to target creature or player.').
card_image_name('mogg fanatic'/'DD3_EVG', 'mogg fanatic').
card_uid('mogg fanatic'/'DD3_EVG', 'DD3_EVG:Mogg Fanatic:mogg fanatic').
card_rarity('mogg fanatic'/'DD3_EVG', 'Uncommon').
card_artist('mogg fanatic'/'DD3_EVG', 'Brom').
card_number('mogg fanatic'/'DD3_EVG', '44').
card_flavor_text('mogg fanatic'/'DD3_EVG', '\"I got it! I got it! I—\"').
card_multiverse_id('mogg fanatic'/'DD3_EVG', '393972').

card_in_set('mogg war marshal', 'DD3_EVG').
card_original_type('mogg war marshal'/'DD3_EVG', 'Creature — Goblin Warrior').
card_original_text('mogg war marshal'/'DD3_EVG', 'Echo {1}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Mogg War Marshal enters the battlefield or dies, put a 1/1 red Goblin creature token onto the battlefield.').
card_image_name('mogg war marshal'/'DD3_EVG', 'mogg war marshal').
card_uid('mogg war marshal'/'DD3_EVG', 'DD3_EVG:Mogg War Marshal:mogg war marshal').
card_rarity('mogg war marshal'/'DD3_EVG', 'Common').
card_artist('mogg war marshal'/'DD3_EVG', 'Wayne England').
card_number('mogg war marshal'/'DD3_EVG', '45').
card_multiverse_id('mogg war marshal'/'DD3_EVG', '393973').

card_in_set('moonglove extract', 'DD3_EVG').
card_original_type('moonglove extract'/'DD3_EVG', 'Artifact').
card_original_text('moonglove extract'/'DD3_EVG', 'Sacrifice Moonglove Extract: Moonglove Extract deals 2 damage to target creature or player.').
card_image_name('moonglove extract'/'DD3_EVG', 'moonglove extract').
card_uid('moonglove extract'/'DD3_EVG', 'DD3_EVG:Moonglove Extract:moonglove extract').
card_rarity('moonglove extract'/'DD3_EVG', 'Common').
card_artist('moonglove extract'/'DD3_EVG', 'Terese Nielsen').
card_number('moonglove extract'/'DD3_EVG', '24').
card_flavor_text('moonglove extract'/'DD3_EVG', 'Diluted, moonglove can etch living tissue. Concentrated, a drop will kill a giant.').
card_multiverse_id('moonglove extract'/'DD3_EVG', '393974').

card_in_set('mountain', 'DD3_EVG').
card_original_type('mountain'/'DD3_EVG', 'Basic Land — Mountain').
card_original_text('mountain'/'DD3_EVG', 'R').
card_image_name('mountain'/'DD3_EVG', 'mountain1').
card_uid('mountain'/'DD3_EVG', 'DD3_EVG:Mountain:mountain1').
card_rarity('mountain'/'DD3_EVG', 'Basic Land').
card_artist('mountain'/'DD3_EVG', 'David Day').
card_number('mountain'/'DD3_EVG', '59').
card_multiverse_id('mountain'/'DD3_EVG', '393978').

card_in_set('mountain', 'DD3_EVG').
card_original_type('mountain'/'DD3_EVG', 'Basic Land — Mountain').
card_original_text('mountain'/'DD3_EVG', 'R').
card_image_name('mountain'/'DD3_EVG', 'mountain2').
card_uid('mountain'/'DD3_EVG', 'DD3_EVG:Mountain:mountain2').
card_rarity('mountain'/'DD3_EVG', 'Basic Land').
card_artist('mountain'/'DD3_EVG', 'Jeff Miracola').
card_number('mountain'/'DD3_EVG', '60').
card_multiverse_id('mountain'/'DD3_EVG', '393976').

card_in_set('mountain', 'DD3_EVG').
card_original_type('mountain'/'DD3_EVG', 'Basic Land — Mountain').
card_original_text('mountain'/'DD3_EVG', 'R').
card_image_name('mountain'/'DD3_EVG', 'mountain3').
card_uid('mountain'/'DD3_EVG', 'DD3_EVG:Mountain:mountain3').
card_rarity('mountain'/'DD3_EVG', 'Basic Land').
card_artist('mountain'/'DD3_EVG', 'Rob Alexander').
card_number('mountain'/'DD3_EVG', '61').
card_multiverse_id('mountain'/'DD3_EVG', '393977').

card_in_set('mountain', 'DD3_EVG').
card_original_type('mountain'/'DD3_EVG', 'Basic Land — Mountain').
card_original_text('mountain'/'DD3_EVG', 'R').
card_image_name('mountain'/'DD3_EVG', 'mountain4').
card_uid('mountain'/'DD3_EVG', 'DD3_EVG:Mountain:mountain4').
card_rarity('mountain'/'DD3_EVG', 'Basic Land').
card_artist('mountain'/'DD3_EVG', 'Sam Wood').
card_number('mountain'/'DD3_EVG', '62').
card_multiverse_id('mountain'/'DD3_EVG', '393975').

card_in_set('mudbutton torchrunner', 'DD3_EVG').
card_original_type('mudbutton torchrunner'/'DD3_EVG', 'Creature — Goblin Warrior').
card_original_text('mudbutton torchrunner'/'DD3_EVG', 'When Mudbutton Torchrunner dies, it deals 3 damage to target creature or player.').
card_image_name('mudbutton torchrunner'/'DD3_EVG', 'mudbutton torchrunner').
card_uid('mudbutton torchrunner'/'DD3_EVG', 'DD3_EVG:Mudbutton Torchrunner:mudbutton torchrunner').
card_rarity('mudbutton torchrunner'/'DD3_EVG', 'Common').
card_artist('mudbutton torchrunner'/'DD3_EVG', 'Steve Ellis').
card_number('mudbutton torchrunner'/'DD3_EVG', '46').
card_flavor_text('mudbutton torchrunner'/'DD3_EVG', 'The oil sloshes against his skull as he nears his destination: the Frogtosser Games and the lighting of the Flaming Boggart.').
card_multiverse_id('mudbutton torchrunner'/'DD3_EVG', '393979').

card_in_set('raging goblin', 'DD3_EVG').
card_original_type('raging goblin'/'DD3_EVG', 'Creature — Goblin Berserker').
card_original_text('raging goblin'/'DD3_EVG', 'Haste').
card_image_name('raging goblin'/'DD3_EVG', 'raging goblin').
card_uid('raging goblin'/'DD3_EVG', 'DD3_EVG:Raging Goblin:raging goblin').
card_rarity('raging goblin'/'DD3_EVG', 'Common').
card_artist('raging goblin'/'DD3_EVG', 'Jeff Miracola').
card_number('raging goblin'/'DD3_EVG', '47').
card_flavor_text('raging goblin'/'DD3_EVG', 'He raged at the world, at his family, at his life. But mostly he just raged.').
card_multiverse_id('raging goblin'/'DD3_EVG', '393980').

card_in_set('reckless one', 'DD3_EVG').
card_original_type('reckless one'/'DD3_EVG', 'Creature — Goblin Avatar').
card_original_text('reckless one'/'DD3_EVG', 'Haste\nReckless One\'s power and toughness are each equal to the number of Goblins on the battlefield.').
card_image_name('reckless one'/'DD3_EVG', 'reckless one').
card_uid('reckless one'/'DD3_EVG', 'DD3_EVG:Reckless One:reckless one').
card_rarity('reckless one'/'DD3_EVG', 'Uncommon').
card_artist('reckless one'/'DD3_EVG', 'Ron Spencer').
card_number('reckless one'/'DD3_EVG', '48').
card_flavor_text('reckless one'/'DD3_EVG', '\"Release chaos with me!\"').
card_multiverse_id('reckless one'/'DD3_EVG', '393981').

card_in_set('siege-gang commander', 'DD3_EVG').
card_original_type('siege-gang commander'/'DD3_EVG', 'Creature — Goblin').
card_original_text('siege-gang commander'/'DD3_EVG', 'When Siege-Gang Commander enters the battlefield, put three 1/1 red Goblin creature tokens onto the battlefield.\n{1}{R}, Sacrifice a Goblin: Siege-Gang Commander deals 2 damage to target creature or player.').
card_image_name('siege-gang commander'/'DD3_EVG', 'siege-gang commander').
card_uid('siege-gang commander'/'DD3_EVG', 'DD3_EVG:Siege-Gang Commander:siege-gang commander').
card_rarity('siege-gang commander'/'DD3_EVG', 'Rare').
card_artist('siege-gang commander'/'DD3_EVG', 'Lucio Parrillo').
card_number('siege-gang commander'/'DD3_EVG', '32').
card_multiverse_id('siege-gang commander'/'DD3_EVG', '393982').

card_in_set('skirk drill sergeant', 'DD3_EVG').
card_original_type('skirk drill sergeant'/'DD3_EVG', 'Creature — Goblin').
card_original_text('skirk drill sergeant'/'DD3_EVG', 'Whenever Skirk Drill Sergeant or another Goblin dies, you may pay {2}{R}. If you do, reveal the top card of your library. If it\'s a Goblin permanent card, put it onto the battlefield. Otherwise, put it into your graveyard.').
card_image_name('skirk drill sergeant'/'DD3_EVG', 'skirk drill sergeant').
card_uid('skirk drill sergeant'/'DD3_EVG', 'DD3_EVG:Skirk Drill Sergeant:skirk drill sergeant').
card_rarity('skirk drill sergeant'/'DD3_EVG', 'Uncommon').
card_artist('skirk drill sergeant'/'DD3_EVG', 'Alex Horley-Orlandelli').
card_number('skirk drill sergeant'/'DD3_EVG', '49').
card_flavor_text('skirk drill sergeant'/'DD3_EVG', '\"I order you to volunteer.\"').
card_multiverse_id('skirk drill sergeant'/'DD3_EVG', '393983').

card_in_set('skirk fire marshal', 'DD3_EVG').
card_original_type('skirk fire marshal'/'DD3_EVG', 'Creature — Goblin').
card_original_text('skirk fire marshal'/'DD3_EVG', 'Protection from red\nTap five untapped Goblins you control: Skirk Fire Marshal deals 10 damage to each creature and each player.').
card_image_name('skirk fire marshal'/'DD3_EVG', 'skirk fire marshal').
card_uid('skirk fire marshal'/'DD3_EVG', 'DD3_EVG:Skirk Fire Marshal:skirk fire marshal').
card_rarity('skirk fire marshal'/'DD3_EVG', 'Rare').
card_artist('skirk fire marshal'/'DD3_EVG', 'Greg & Tim Hildebrandt').
card_number('skirk fire marshal'/'DD3_EVG', '50').
card_flavor_text('skirk fire marshal'/'DD3_EVG', 'He\'s boss because he\'s smart enough to get out of the way.').
card_multiverse_id('skirk fire marshal'/'DD3_EVG', '393984').

card_in_set('skirk prospector', 'DD3_EVG').
card_original_type('skirk prospector'/'DD3_EVG', 'Creature — Goblin').
card_original_text('skirk prospector'/'DD3_EVG', 'Sacrifice a Goblin: Add {R} to your mana pool.').
card_image_name('skirk prospector'/'DD3_EVG', 'skirk prospector').
card_uid('skirk prospector'/'DD3_EVG', 'DD3_EVG:Skirk Prospector:skirk prospector').
card_rarity('skirk prospector'/'DD3_EVG', 'Common').
card_artist('skirk prospector'/'DD3_EVG', 'Doug Chaffee').
card_number('skirk prospector'/'DD3_EVG', '51').
card_flavor_text('skirk prospector'/'DD3_EVG', '\"I like goblins. They make funny little popping sounds when they die.\"\n—Braids, dementia summoner').
card_multiverse_id('skirk prospector'/'DD3_EVG', '393985').

card_in_set('skirk shaman', 'DD3_EVG').
card_original_type('skirk shaman'/'DD3_EVG', 'Creature — Goblin Shaman').
card_original_text('skirk shaman'/'DD3_EVG', 'Skirk Shaman can\'t be blocked except by artifact creatures and/or red creatures.').
card_image_name('skirk shaman'/'DD3_EVG', 'skirk shaman').
card_uid('skirk shaman'/'DD3_EVG', 'DD3_EVG:Skirk Shaman:skirk shaman').
card_rarity('skirk shaman'/'DD3_EVG', 'Common').
card_artist('skirk shaman'/'DD3_EVG', 'Chippy').
card_number('skirk shaman'/'DD3_EVG', '52').
card_flavor_text('skirk shaman'/'DD3_EVG', 'In a shaman\'s grimy hands, a concoction of dried Skirk Ridge herbs can become the face of panic.').
card_multiverse_id('skirk shaman'/'DD3_EVG', '393986').

card_in_set('slate of ancestry', 'DD3_EVG').
card_original_type('slate of ancestry'/'DD3_EVG', 'Artifact').
card_original_text('slate of ancestry'/'DD3_EVG', '{4}, {T}, Discard your hand: Draw a card for each creature you control.').
card_image_name('slate of ancestry'/'DD3_EVG', 'slate of ancestry').
card_uid('slate of ancestry'/'DD3_EVG', 'DD3_EVG:Slate of Ancestry:slate of ancestry').
card_rarity('slate of ancestry'/'DD3_EVG', 'Rare').
card_artist('slate of ancestry'/'DD3_EVG', 'Corey D. Macourek').
card_number('slate of ancestry'/'DD3_EVG', '25').
card_flavor_text('slate of ancestry'/'DD3_EVG', 'The pattern of life can be studied like a book, if you know how to read it.').
card_multiverse_id('slate of ancestry'/'DD3_EVG', '393987').

card_in_set('spitting earth', 'DD3_EVG').
card_original_type('spitting earth'/'DD3_EVG', 'Sorcery').
card_original_text('spitting earth'/'DD3_EVG', 'Spitting Earth deals damage to target creature equal to the number of Mountains you control.').
card_image_name('spitting earth'/'DD3_EVG', 'spitting earth').
card_uid('spitting earth'/'DD3_EVG', 'DD3_EVG:Spitting Earth:spitting earth').
card_rarity('spitting earth'/'DD3_EVG', 'Common').
card_artist('spitting earth'/'DD3_EVG', 'Michael Koelsch').
card_number('spitting earth'/'DD3_EVG', '55').
card_flavor_text('spitting earth'/'DD3_EVG', '\"Within each of you is the strength of a landslide.\"\n—Kumano, to his pupils').
card_multiverse_id('spitting earth'/'DD3_EVG', '393988').

card_in_set('stonewood invoker', 'DD3_EVG').
card_original_type('stonewood invoker'/'DD3_EVG', 'Creature — Elf Mutant').
card_original_text('stonewood invoker'/'DD3_EVG', '{7}{G}: Stonewood Invoker gets +5/+5 until end of turn.').
card_image_name('stonewood invoker'/'DD3_EVG', 'stonewood invoker').
card_uid('stonewood invoker'/'DD3_EVG', 'DD3_EVG:Stonewood Invoker:stonewood invoker').
card_rarity('stonewood invoker'/'DD3_EVG', 'Common').
card_artist('stonewood invoker'/'DD3_EVG', 'Eric Peterson').
card_number('stonewood invoker'/'DD3_EVG', '11').
card_flavor_text('stonewood invoker'/'DD3_EVG', 'The Mirari pulses in his veins.').
card_multiverse_id('stonewood invoker'/'DD3_EVG', '393989').

card_in_set('sylvan messenger', 'DD3_EVG').
card_original_type('sylvan messenger'/'DD3_EVG', 'Creature — Elf').
card_original_text('sylvan messenger'/'DD3_EVG', 'Trample\nWhen Sylvan Messenger enters the battlefield, reveal the top four cards of your library. Put all Elf cards revealed this way into your hand and the rest on the bottom of your library in any order.').
card_image_name('sylvan messenger'/'DD3_EVG', 'sylvan messenger').
card_uid('sylvan messenger'/'DD3_EVG', 'DD3_EVG:Sylvan Messenger:sylvan messenger').
card_rarity('sylvan messenger'/'DD3_EVG', 'Uncommon').
card_artist('sylvan messenger'/'DD3_EVG', 'Heather Hudson').
card_number('sylvan messenger'/'DD3_EVG', '12').
card_multiverse_id('sylvan messenger'/'DD3_EVG', '393990').

card_in_set('tar pitcher', 'DD3_EVG').
card_original_type('tar pitcher'/'DD3_EVG', 'Creature — Goblin Shaman').
card_original_text('tar pitcher'/'DD3_EVG', '{T}, Sacrifice a Goblin: Tar Pitcher deals 2 damage to target creature or player.').
card_image_name('tar pitcher'/'DD3_EVG', 'tar pitcher').
card_uid('tar pitcher'/'DD3_EVG', 'DD3_EVG:Tar Pitcher:tar pitcher').
card_rarity('tar pitcher'/'DD3_EVG', 'Uncommon').
card_artist('tar pitcher'/'DD3_EVG', 'Omar Rayyan').
card_number('tar pitcher'/'DD3_EVG', '53').
card_flavor_text('tar pitcher'/'DD3_EVG', '\"Auntie Grub had caught a goat, a bleating doe that still squirmed in her arms. At the same time, her warren decided to share with her a sensory greeting of hot tar . . . .\"\n—A tale of Auntie Grub').
card_multiverse_id('tar pitcher'/'DD3_EVG', '393991').

card_in_set('tarfire', 'DD3_EVG').
card_original_type('tarfire'/'DD3_EVG', 'Tribal Instant — Goblin').
card_original_text('tarfire'/'DD3_EVG', 'Tarfire deals 2 damage to target creature or player.').
card_image_name('tarfire'/'DD3_EVG', 'tarfire').
card_uid('tarfire'/'DD3_EVG', 'DD3_EVG:Tarfire:tarfire').
card_rarity('tarfire'/'DD3_EVG', 'Common').
card_artist('tarfire'/'DD3_EVG', 'Omar Rayyan').
card_number('tarfire'/'DD3_EVG', '56').
card_flavor_text('tarfire'/'DD3_EVG', '\"After Auntie brushed the soot from her eyes, she discovered something wonderful: the fire had turned the goat into something that smelled delicious.\"\n—A tale of Auntie Grub').
card_multiverse_id('tarfire'/'DD3_EVG', '393992').

card_in_set('timberwatch elf', 'DD3_EVG').
card_original_type('timberwatch elf'/'DD3_EVG', 'Creature — Elf').
card_original_text('timberwatch elf'/'DD3_EVG', '{T}: Target creature gets +X/+X until end of turn, where X is the number of Elves on the battlefield.').
card_image_name('timberwatch elf'/'DD3_EVG', 'timberwatch elf').
card_uid('timberwatch elf'/'DD3_EVG', 'DD3_EVG:Timberwatch Elf:timberwatch elf').
card_rarity('timberwatch elf'/'DD3_EVG', 'Common').
card_artist('timberwatch elf'/'DD3_EVG', 'Dave Dorman').
card_number('timberwatch elf'/'DD3_EVG', '13').
card_flavor_text('timberwatch elf'/'DD3_EVG', 'Even through the Mirari\'s voice, the elves still hear the call of their kinship.').
card_multiverse_id('timberwatch elf'/'DD3_EVG', '393993').

card_in_set('tranquil thicket', 'DD3_EVG').
card_original_type('tranquil thicket'/'DD3_EVG', 'Land').
card_original_text('tranquil thicket'/'DD3_EVG', 'Tranquil Thicket enters the battlefield tapped.\n{T}: Add {G} to your mana pool.\nCycling {G} ({G}, Discard this card: Draw a card.)').
card_image_name('tranquil thicket'/'DD3_EVG', 'tranquil thicket').
card_uid('tranquil thicket'/'DD3_EVG', 'DD3_EVG:Tranquil Thicket:tranquil thicket').
card_rarity('tranquil thicket'/'DD3_EVG', 'Common').
card_artist('tranquil thicket'/'DD3_EVG', 'Heather Hudson').
card_number('tranquil thicket'/'DD3_EVG', '26').
card_multiverse_id('tranquil thicket'/'DD3_EVG', '393994').

card_in_set('voice of the woods', 'DD3_EVG').
card_original_type('voice of the woods'/'DD3_EVG', 'Creature — Elf').
card_original_text('voice of the woods'/'DD3_EVG', 'Tap five untapped Elves you control: Put a 7/7 green Elemental creature token with trample onto the battlefield.').
card_image_name('voice of the woods'/'DD3_EVG', 'voice of the woods').
card_uid('voice of the woods'/'DD3_EVG', 'DD3_EVG:Voice of the Woods:voice of the woods').
card_rarity('voice of the woods'/'DD3_EVG', 'Rare').
card_artist('voice of the woods'/'DD3_EVG', 'Pete Venters').
card_number('voice of the woods'/'DD3_EVG', '14').
card_flavor_text('voice of the woods'/'DD3_EVG', 'The ritual of making draws upon the elves\' memories and pasts. And elves have long memories and ancient pasts.').
card_multiverse_id('voice of the woods'/'DD3_EVG', '393995').

card_in_set('wellwisher', 'DD3_EVG').
card_original_type('wellwisher'/'DD3_EVG', 'Creature — Elf').
card_original_text('wellwisher'/'DD3_EVG', '{T}: You gain 1 life for each Elf on the battlefield.').
card_image_name('wellwisher'/'DD3_EVG', 'wellwisher').
card_uid('wellwisher'/'DD3_EVG', 'DD3_EVG:Wellwisher:wellwisher').
card_rarity('wellwisher'/'DD3_EVG', 'Common').
card_artist('wellwisher'/'DD3_EVG', 'Christopher Rush').
card_number('wellwisher'/'DD3_EVG', '15').
card_flavor_text('wellwisher'/'DD3_EVG', '\"Close your ears to the voice of greed, and you can turn a gift for one into a gift for many.\"').
card_multiverse_id('wellwisher'/'DD3_EVG', '393996').

card_in_set('wildsize', 'DD3_EVG').
card_original_type('wildsize'/'DD3_EVG', 'Instant').
card_original_text('wildsize'/'DD3_EVG', 'Target creature gets +2/+2 and gains trample until end of turn.\nDraw a card.').
card_image_name('wildsize'/'DD3_EVG', 'wildsize').
card_uid('wildsize'/'DD3_EVG', 'DD3_EVG:Wildsize:wildsize').
card_rarity('wildsize'/'DD3_EVG', 'Common').
card_artist('wildsize'/'DD3_EVG', 'Jim Murray').
card_number('wildsize'/'DD3_EVG', '23').
card_flavor_text('wildsize'/'DD3_EVG', 'Two times the size, four times the smell.').
card_multiverse_id('wildsize'/'DD3_EVG', '393997').

card_in_set('wirewood herald', 'DD3_EVG').
card_original_type('wirewood herald'/'DD3_EVG', 'Creature — Elf').
card_original_text('wirewood herald'/'DD3_EVG', 'When Wirewood Herald dies, you may search your library for an Elf card, reveal that card, put it into your hand, then shuffle your library.').
card_image_name('wirewood herald'/'DD3_EVG', 'wirewood herald').
card_uid('wirewood herald'/'DD3_EVG', 'DD3_EVG:Wirewood Herald:wirewood herald').
card_rarity('wirewood herald'/'DD3_EVG', 'Common').
card_artist('wirewood herald'/'DD3_EVG', 'Alex Horley-Orlandelli').
card_number('wirewood herald'/'DD3_EVG', '16').
card_flavor_text('wirewood herald'/'DD3_EVG', 'The goblins laughed as the elf ran away, until more came back.').
card_multiverse_id('wirewood herald'/'DD3_EVG', '393998').

card_in_set('wirewood lodge', 'DD3_EVG').
card_original_type('wirewood lodge'/'DD3_EVG', 'Land').
card_original_text('wirewood lodge'/'DD3_EVG', '{T}: Add {1} to your mana pool.\n{G}, {T}: Untap target Elf.').
card_image_name('wirewood lodge'/'DD3_EVG', 'wirewood lodge').
card_uid('wirewood lodge'/'DD3_EVG', 'DD3_EVG:Wirewood Lodge:wirewood lodge').
card_rarity('wirewood lodge'/'DD3_EVG', 'Uncommon').
card_artist('wirewood lodge'/'DD3_EVG', 'Anthony S. Waters').
card_number('wirewood lodge'/'DD3_EVG', '27').
card_multiverse_id('wirewood lodge'/'DD3_EVG', '393999').

card_in_set('wirewood symbiote', 'DD3_EVG').
card_original_type('wirewood symbiote'/'DD3_EVG', 'Creature — Insect').
card_original_text('wirewood symbiote'/'DD3_EVG', 'Return an Elf you control to its owner\'s hand: Untap target creature. Activate this ability only once each turn.').
card_image_name('wirewood symbiote'/'DD3_EVG', 'wirewood symbiote').
card_uid('wirewood symbiote'/'DD3_EVG', 'DD3_EVG:Wirewood Symbiote:wirewood symbiote').
card_rarity('wirewood symbiote'/'DD3_EVG', 'Uncommon').
card_artist('wirewood symbiote'/'DD3_EVG', 'Thomas M. Baxa').
card_number('wirewood symbiote'/'DD3_EVG', '17').
card_flavor_text('wirewood symbiote'/'DD3_EVG', 'It drinks fatigue.').
card_multiverse_id('wirewood symbiote'/'DD3_EVG', '394000').

card_in_set('wood elves', 'DD3_EVG').
card_original_type('wood elves'/'DD3_EVG', 'Creature — Elf Scout').
card_original_text('wood elves'/'DD3_EVG', 'When Wood Elves enters the battlefield, search your library for a Forest card and put that card onto the battlefield. Then shuffle your library.').
card_image_name('wood elves'/'DD3_EVG', 'wood elves').
card_uid('wood elves'/'DD3_EVG', 'DD3_EVG:Wood Elves:wood elves').
card_rarity('wood elves'/'DD3_EVG', 'Common').
card_artist('wood elves'/'DD3_EVG', 'Christopher Moeller').
card_number('wood elves'/'DD3_EVG', '18').
card_flavor_text('wood elves'/'DD3_EVG', 'Every branch a crossroads, every vine a swift steed.').
card_multiverse_id('wood elves'/'DD3_EVG', '394001').

card_in_set('wren\'s run vanquisher', 'DD3_EVG').
card_original_type('wren\'s run vanquisher'/'DD3_EVG', 'Creature — Elf Warrior').
card_original_text('wren\'s run vanquisher'/'DD3_EVG', 'As an additional cost to cast Wren\'s Run Vanquisher, reveal an Elf card from your hand or pay {3}.\nDeathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_image_name('wren\'s run vanquisher'/'DD3_EVG', 'wren\'s run vanquisher').
card_uid('wren\'s run vanquisher'/'DD3_EVG', 'DD3_EVG:Wren\'s Run Vanquisher:wren\'s run vanquisher').
card_rarity('wren\'s run vanquisher'/'DD3_EVG', 'Uncommon').
card_artist('wren\'s run vanquisher'/'DD3_EVG', 'Paolo Parente').
card_number('wren\'s run vanquisher'/'DD3_EVG', '19').
card_multiverse_id('wren\'s run vanquisher'/'DD3_EVG', '394002').
