% Legends

set('LEG').
set_name('LEG', 'Legends').
set_release_date('LEG', '1994-06-01').
set_border('LEG', 'black').
set_type('LEG', 'expansion').

card_in_set('abomination', 'LEG').
card_original_type('abomination'/'LEG', 'Summon — Abomination').
card_original_text('abomination'/'LEG', 'All green or white creatures blocking or blocked by Abomination are destroyed at the end of combat.').
card_first_print('abomination', 'LEG').
card_image_name('abomination'/'LEG', 'abomination').
card_uid('abomination'/'LEG', 'LEG:Abomination:abomination').
card_rarity('abomination'/'LEG', 'Uncommon').
card_artist('abomination'/'LEG', 'Mark Tedin').
card_multiverse_id('abomination'/'LEG', '1427').

card_in_set('acid rain', 'LEG').
card_original_type('acid rain'/'LEG', 'Sorcery').
card_original_text('acid rain'/'LEG', 'Destroys all forests in play.').
card_first_print('acid rain', 'LEG').
card_image_name('acid rain'/'LEG', 'acid rain').
card_uid('acid rain'/'LEG', 'LEG:Acid Rain:acid rain').
card_rarity('acid rain'/'LEG', 'Rare').
card_artist('acid rain'/'LEG', 'NéNé Thomas').
card_multiverse_id('acid rain'/'LEG', '1470').

card_in_set('active volcano', 'LEG').
card_original_type('active volcano'/'LEG', 'Instant').
card_original_text('active volcano'/'LEG', 'Destroy target blue permanent or return target island to owner\'s hand. Enchantments on target land are destroyed.').
card_first_print('active volcano', 'LEG').
card_image_name('active volcano'/'LEG', 'active volcano').
card_uid('active volcano'/'LEG', 'LEG:Active Volcano:active volcano').
card_rarity('active volcano'/'LEG', 'Common').
card_artist('active volcano'/'LEG', 'Justin Hampton').
card_multiverse_id('active volcano'/'LEG', '1556').

card_in_set('adun oakenshield', 'LEG').
card_original_type('adun oakenshield'/'LEG', 'Summon — Legend').
card_original_text('adun oakenshield'/'LEG', '{G}{R}{B}{T}: Select one creature from your graveyard and place it in your hand.').
card_first_print('adun oakenshield', 'LEG').
card_image_name('adun oakenshield'/'LEG', 'adun oakenshield').
card_uid('adun oakenshield'/'LEG', 'LEG:Adun Oakenshield:adun oakenshield').
card_rarity('adun oakenshield'/'LEG', 'Rare').
card_artist('adun oakenshield'/'LEG', 'Jeff A. Menges').
card_flavor_text('adun oakenshield'/'LEG', '\". . . And at his passing, the bodies of the world\'s great warriors shall rise from their graves and follow him to battle.\" —The Anvilonian Grimoire').
card_multiverse_id('adun oakenshield'/'LEG', '1642').

card_in_set('adventurers\' guildhouse', 'LEG').
card_original_type('adventurers\' guildhouse'/'LEG', 'Land').
card_original_text('adventurers\' guildhouse'/'LEG', 'All your green legends gain bands with other legends.').
card_first_print('adventurers\' guildhouse', 'LEG').
card_image_name('adventurers\' guildhouse'/'LEG', 'adventurers\' guildhouse').
card_uid('adventurers\' guildhouse'/'LEG', 'LEG:Adventurers\' Guildhouse:adventurers\' guildhouse').
card_rarity('adventurers\' guildhouse'/'LEG', 'Uncommon').
card_artist('adventurers\' guildhouse'/'LEG', 'Tom Wänerstrand').
card_multiverse_id('adventurers\' guildhouse'/'LEG', '1698').

card_in_set('ærathi berserker', 'LEG').
card_original_type('ærathi berserker'/'LEG', 'Summon — Berserker').
card_original_text('ærathi berserker'/'LEG', 'Rampage: 3').
card_first_print('ærathi berserker', 'LEG').
card_image_name('ærathi berserker'/'LEG', 'aerathi berserker').
card_uid('ærathi berserker'/'LEG', 'LEG:Ærathi Berserker:aerathi berserker').
card_rarity('ærathi berserker'/'LEG', 'Uncommon').
card_artist('ærathi berserker'/'LEG', 'Melissa A. Benson').
card_flavor_text('ærathi berserker'/'LEG', 'Ærathi children who show promise are left to survive for a year in the wilderness. Those who return are shown the way of the Berserker.').
card_multiverse_id('ærathi berserker'/'LEG', '1557').

card_in_set('aisling leprechaun', 'LEG').
card_original_type('aisling leprechaun'/'LEG', 'Summon — Faerie').
card_original_text('aisling leprechaun'/'LEG', 'All creatures that block or are blocked by Leprechaun become green creatures. Use counters to indicate changed creatures. Cost to tap, maintain, or use a special ability of target creature remains entirely unchanged.').
card_first_print('aisling leprechaun', 'LEG').
card_image_name('aisling leprechaun'/'LEG', 'aisling leprechaun').
card_uid('aisling leprechaun'/'LEG', 'LEG:Aisling Leprechaun:aisling leprechaun').
card_rarity('aisling leprechaun'/'LEG', 'Common').
card_artist('aisling leprechaun'/'LEG', 'Quinton Hoover').
card_multiverse_id('aisling leprechaun'/'LEG', '1513').

card_in_set('akron legionnaire', 'LEG').
card_original_type('akron legionnaire'/'LEG', 'Summon — Legionnaire').
card_original_text('akron legionnaire'/'LEG', 'None of your non-artifact creatures may attack except Akron Legionnaire.').
card_first_print('akron legionnaire', 'LEG').
card_image_name('akron legionnaire'/'LEG', 'akron legionnaire').
card_uid('akron legionnaire'/'LEG', 'LEG:Akron Legionnaire:akron legionnaire').
card_rarity('akron legionnaire'/'LEG', 'Rare').
card_artist('akron legionnaire'/'LEG', 'Mark Poole').
card_multiverse_id('akron legionnaire'/'LEG', '1599').

card_in_set('al-abara\'s carpet', 'LEG').
card_original_type('al-abara\'s carpet'/'LEG', 'Artifact').
card_original_text('al-abara\'s carpet'/'LEG', '{5}{T}: Prevents all damage done to you by attacking non-flying creatures.').
card_first_print('al-abara\'s carpet', 'LEG').
card_image_name('al-abara\'s carpet'/'LEG', 'al-abara\'s carpet').
card_uid('al-abara\'s carpet'/'LEG', 'LEG:Al-abara\'s Carpet:al-abara\'s carpet').
card_rarity('al-abara\'s carpet'/'LEG', 'Rare').
card_artist('al-abara\'s carpet'/'LEG', 'Kaja Foglio').
card_flavor_text('al-abara\'s carpet'/'LEG', 'Al-abara simply laughed and lifted one finger, and the carpet carried her high out of our reach.').
card_multiverse_id('al-abara\'s carpet'/'LEG', '1398').

card_in_set('alabaster potion', 'LEG').
card_original_type('alabaster potion'/'LEG', 'Instant').
card_original_text('alabaster potion'/'LEG', 'Target player gains X life or prevents X damage to any one creature or player.').
card_first_print('alabaster potion', 'LEG').
card_image_name('alabaster potion'/'LEG', 'alabaster potion').
card_uid('alabaster potion'/'LEG', 'LEG:Alabaster Potion:alabaster potion').
card_rarity('alabaster potion'/'LEG', 'Common').
card_artist('alabaster potion'/'LEG', 'Harold McNeill').
card_flavor_text('alabaster potion'/'LEG', '\"Healing is a matter of time, but it is sometimes also a matter of opportunity.\" —D\'Avenant proverb').
card_multiverse_id('alabaster potion'/'LEG', '1600').

card_in_set('alchor\'s tomb', 'LEG').
card_original_type('alchor\'s tomb'/'LEG', 'Artifact').
card_original_text('alchor\'s tomb'/'LEG', '{2}{T}: Change the color of target permanent you control to a color of your choice. Use counters. Cost to cast, tap, maintain, or use a special ability of card remains unchanged.').
card_first_print('alchor\'s tomb', 'LEG').
card_image_name('alchor\'s tomb'/'LEG', 'alchor\'s tomb').
card_uid('alchor\'s tomb'/'LEG', 'LEG:Alchor\'s Tomb:alchor\'s tomb').
card_rarity('alchor\'s tomb'/'LEG', 'Rare').
card_artist('alchor\'s tomb'/'LEG', 'Jesper Myrfors').
card_multiverse_id('alchor\'s tomb'/'LEG', '1399').

card_in_set('all hallow\'s eve', 'LEG').
card_original_type('all hallow\'s eve'/'LEG', 'Sorcery').
card_original_text('all hallow\'s eve'/'LEG', 'Put two counters on this card. Remove a counter during your upkeep. When you remove the last counter from All Hallow\'s Eve, all players take all creatures from their graveyards and put them directly into play. Treat these creatures as though they were just summoned. You choose what order they come into play.').
card_first_print('all hallow\'s eve', 'LEG').
card_image_name('all hallow\'s eve'/'LEG', 'all hallow\'s eve').
card_uid('all hallow\'s eve'/'LEG', 'LEG:All Hallow\'s Eve:all hallow\'s eve').
card_rarity('all hallow\'s eve'/'LEG', 'Rare').
card_artist('all hallow\'s eve'/'LEG', 'Christopher Rush').
card_multiverse_id('all hallow\'s eve'/'LEG', '1428').

card_in_set('amrou kithkin', 'LEG').
card_original_type('amrou kithkin'/'LEG', 'Summon — Kithkin').
card_original_text('amrou kithkin'/'LEG', 'Creatures with power greater than 2 may not be assigned to block Kithkin. Blocker\'s power may be increased after blocking has been assigned.').
card_first_print('amrou kithkin', 'LEG').
card_image_name('amrou kithkin'/'LEG', 'amrou kithkin').
card_uid('amrou kithkin'/'LEG', 'LEG:Amrou Kithkin:amrou kithkin').
card_rarity('amrou kithkin'/'LEG', 'Common').
card_artist('amrou kithkin'/'LEG', 'Quinton Hoover').
card_flavor_text('amrou kithkin'/'LEG', 'Quick and agile, Amrou Kithkin can usually escape from even the most fearsome opponents.').
card_multiverse_id('amrou kithkin'/'LEG', '1601').

card_in_set('angelic voices', 'LEG').
card_original_type('angelic voices'/'LEG', 'Enchantment').
card_original_text('angelic voices'/'LEG', 'As long as the only creatures you control are white or artifact creatures all your creatures gain +1/+1.').
card_first_print('angelic voices', 'LEG').
card_image_name('angelic voices'/'LEG', 'angelic voices').
card_uid('angelic voices'/'LEG', 'LEG:Angelic Voices:angelic voices').
card_rarity('angelic voices'/'LEG', 'Rare').
card_artist('angelic voices'/'LEG', 'Julie Baroh').
card_multiverse_id('angelic voices'/'LEG', '1602').

card_in_set('angus mackenzie', 'LEG').
card_original_type('angus mackenzie'/'LEG', 'Summon — Legend').
card_original_text('angus mackenzie'/'LEG', '{W}{U}{G}{T}: Creatures attack and block as normal, but none deal any damage during combat. All attacking creatures are still tapped. Use this ability any time before attack damage is dealt.').
card_first_print('angus mackenzie', 'LEG').
card_image_name('angus mackenzie'/'LEG', 'angus mackenzie').
card_uid('angus mackenzie'/'LEG', 'LEG:Angus Mackenzie:angus mackenzie').
card_rarity('angus mackenzie'/'LEG', 'Rare').
card_artist('angus mackenzie'/'LEG', 'Bryon Wackwitz').
card_flavor_text('angus mackenzie'/'LEG', '\"Battles no longer served a purpose in Karakas.\" —Angus Mackenzie, Diary').
card_multiverse_id('angus mackenzie'/'LEG', '1643').

card_in_set('anti-magic aura', 'LEG').
card_original_type('anti-magic aura'/'LEG', 'Enchant Creature').
card_original_text('anti-magic aura'/'LEG', 'All enchantments on target creature are destroyed. Target creature cannot be further targeted by instants, sorceries, or enchantments.').
card_first_print('anti-magic aura', 'LEG').
card_image_name('anti-magic aura'/'LEG', 'anti-magic aura').
card_uid('anti-magic aura'/'LEG', 'LEG:Anti-Magic Aura:anti-magic aura').
card_rarity('anti-magic aura'/'LEG', 'Common').
card_artist('anti-magic aura'/'LEG', 'Douglas Shuler').
card_multiverse_id('anti-magic aura'/'LEG', '1471').

card_in_set('arboria', 'LEG').
card_original_type('arboria'/'LEG', 'Enchant World').
card_original_text('arboria'/'LEG', 'If a player does not cast a spell or put a card into play on his or her turn, no creatures may attack that player until after his or her next turn.').
card_first_print('arboria', 'LEG').
card_image_name('arboria'/'LEG', 'arboria').
card_uid('arboria'/'LEG', 'LEG:Arboria:arboria').
card_rarity('arboria'/'LEG', 'Uncommon').
card_artist('arboria'/'LEG', 'Daniel Gelon').
card_multiverse_id('arboria'/'LEG', '1514').

card_in_set('arcades sabboth', 'LEG').
card_original_type('arcades sabboth'/'LEG', 'Summon — Elder Dragon Legend').
card_original_text('arcades sabboth'/'LEG', 'Flying\n{W} +0/+1 until end of turn.\nYour untapped creatures gain +0/+2. Attacking creatures do not get this bonus. Pay {W}{G}{U} during your upkeep or Arcades Sabboth is buried.').
card_first_print('arcades sabboth', 'LEG').
card_image_name('arcades sabboth'/'LEG', 'arcades sabboth').
card_uid('arcades sabboth'/'LEG', 'LEG:Arcades Sabboth:arcades sabboth').
card_rarity('arcades sabboth'/'LEG', 'Rare').
card_artist('arcades sabboth'/'LEG', 'Edward P. Beard, Jr.').
card_multiverse_id('arcades sabboth'/'LEG', '1644').

card_in_set('arena of the ancients', 'LEG').
card_original_type('arena of the ancients'/'LEG', 'Artifact').
card_original_text('arena of the ancients'/'LEG', 'All legends become tapped when Arena comes into play. Legends do not untap as normal during the untap phase.').
card_first_print('arena of the ancients', 'LEG').
card_image_name('arena of the ancients'/'LEG', 'arena of the ancients').
card_uid('arena of the ancients'/'LEG', 'LEG:Arena of the Ancients:arena of the ancients').
card_rarity('arena of the ancients'/'LEG', 'Rare').
card_artist('arena of the ancients'/'LEG', 'Tom Wänerstrand').
card_multiverse_id('arena of the ancients'/'LEG', '1400').

card_in_set('avoid fate', 'LEG').
card_original_type('avoid fate'/'LEG', 'Interrupt').
card_original_text('avoid fate'/'LEG', 'Counters target interrupt or enchantment. Can only counter spells that target a permanent under your control.').
card_first_print('avoid fate', 'LEG').
card_image_name('avoid fate'/'LEG', 'avoid fate').
card_uid('avoid fate'/'LEG', 'LEG:Avoid Fate:avoid fate').
card_rarity('avoid fate'/'LEG', 'Common').
card_artist('avoid fate'/'LEG', 'Phil Foglio').
card_multiverse_id('avoid fate'/'LEG', '1515').

card_in_set('axelrod gunnarson', 'LEG').
card_original_type('axelrod gunnarson'/'LEG', 'Summon — Legend').
card_original_text('axelrod gunnarson'/'LEG', 'Trample\nEach time a creature is placed in the graveyard during a turn in which Axelrod damaged it, you gain 1 life and Axelrod does 1 damage to target player.').
card_first_print('axelrod gunnarson', 'LEG').
card_image_name('axelrod gunnarson'/'LEG', 'axelrod gunnarson').
card_uid('axelrod gunnarson'/'LEG', 'LEG:Axelrod Gunnarson:axelrod gunnarson').
card_rarity('axelrod gunnarson'/'LEG', 'Rare').
card_artist('axelrod gunnarson'/'LEG', 'Scott Kirschner').
card_multiverse_id('axelrod gunnarson'/'LEG', '1645').

card_in_set('ayesha tanaka', 'LEG').
card_original_type('ayesha tanaka'/'LEG', 'Summon — Legend').
card_original_text('ayesha tanaka'/'LEG', 'Banding\n{T}: Artifact effect which requires an activation cost is countered unless its controller spends {W}. This ability is played as an interrupt.').
card_first_print('ayesha tanaka', 'LEG').
card_image_name('ayesha tanaka'/'LEG', 'ayesha tanaka').
card_uid('ayesha tanaka'/'LEG', 'LEG:Ayesha Tanaka:ayesha tanaka').
card_rarity('ayesha tanaka'/'LEG', 'Rare').
card_artist('ayesha tanaka'/'LEG', 'Bryon Wackwitz').
card_multiverse_id('ayesha tanaka'/'LEG', '1646').

card_in_set('azure drake', 'LEG').
card_original_type('azure drake'/'LEG', 'Summon — Drake').
card_original_text('azure drake'/'LEG', 'Flying').
card_first_print('azure drake', 'LEG').
card_image_name('azure drake'/'LEG', 'azure drake').
card_uid('azure drake'/'LEG', 'LEG:Azure Drake:azure drake').
card_rarity('azure drake'/'LEG', 'Uncommon').
card_artist('azure drake'/'LEG', 'Dan Frazier').
card_flavor_text('azure drake'/'LEG', 'The Azure Drake would be more powerful were it not so easily distracted.').
card_multiverse_id('azure drake'/'LEG', '1472').

card_in_set('backdraft', 'LEG').
card_original_type('backdraft'/'LEG', 'Instant').
card_original_text('backdraft'/'LEG', 'Backdraft does half of the damage (rounded down) done by one sorcery cast this turn to the caster of the sorcery.').
card_first_print('backdraft', 'LEG').
card_image_name('backdraft'/'LEG', 'backdraft').
card_uid('backdraft'/'LEG', 'LEG:Backdraft:backdraft').
card_rarity('backdraft'/'LEG', 'Uncommon').
card_artist('backdraft'/'LEG', 'Brian Snõddy').
card_multiverse_id('backdraft'/'LEG', '1558').

card_in_set('backfire', 'LEG').
card_original_type('backfire'/'LEG', 'Enchant Creature').
card_original_text('backfire'/'LEG', 'For each point of damage done to you from target creature Backfire does one point of damage to target creature\'s controller.').
card_first_print('backfire', 'LEG').
card_image_name('backfire'/'LEG', 'backfire').
card_uid('backfire'/'LEG', 'LEG:Backfire:backfire').
card_rarity('backfire'/'LEG', 'Uncommon').
card_artist('backfire'/'LEG', 'Brian Snõddy').
card_multiverse_id('backfire'/'LEG', '1473').

card_in_set('barbary apes', 'LEG').
card_original_type('barbary apes'/'LEG', 'Summon — Apes').
card_original_text('barbary apes'/'LEG', '').
card_first_print('barbary apes', 'LEG').
card_image_name('barbary apes'/'LEG', 'barbary apes').
card_uid('barbary apes'/'LEG', 'LEG:Barbary Apes:barbary apes').
card_rarity('barbary apes'/'LEG', 'Common').
card_artist('barbary apes'/'LEG', 'Bryon Wackwitz').
card_flavor_text('barbary apes'/'LEG', 'Unpredictable in the extreme, these carnivorous apes will prey even upon their own kind.').
card_multiverse_id('barbary apes'/'LEG', '1516').

card_in_set('barktooth warbeard', 'LEG').
card_original_type('barktooth warbeard'/'LEG', 'Summon — Legend').
card_original_text('barktooth warbeard'/'LEG', '').
card_first_print('barktooth warbeard', 'LEG').
card_image_name('barktooth warbeard'/'LEG', 'barktooth warbeard').
card_uid('barktooth warbeard'/'LEG', 'LEG:Barktooth Warbeard:barktooth warbeard').
card_rarity('barktooth warbeard'/'LEG', 'Uncommon').
card_artist('barktooth warbeard'/'LEG', 'Andi Rusu').
card_flavor_text('barktooth warbeard'/'LEG', 'He is devious and cunning, in both appearance and deed. Beware the Warbeard, for this brute bites as well as he barks!').
card_multiverse_id('barktooth warbeard'/'LEG', '1647').

card_in_set('bartel runeaxe', 'LEG').
card_original_type('bartel runeaxe'/'LEG', 'Summon — Legend').
card_original_text('bartel runeaxe'/'LEG', 'Bartel Runeaxe cannot be the target of enchant creature spells. Attacking does not cause Bartel Runeaxe to tap.').
card_first_print('bartel runeaxe', 'LEG').
card_image_name('bartel runeaxe'/'LEG', 'bartel runeaxe').
card_uid('bartel runeaxe'/'LEG', 'LEG:Bartel Runeaxe:bartel runeaxe').
card_rarity('bartel runeaxe'/'LEG', 'Rare').
card_artist('bartel runeaxe'/'LEG', 'Andi Rusu').
card_flavor_text('bartel runeaxe'/'LEG', 'Thundering down from Hammerheim, no foe could slow Bartel\'s charge.').
card_multiverse_id('bartel runeaxe'/'LEG', '1648').

card_in_set('beasts of bogardan', 'LEG').
card_original_type('beasts of bogardan'/'LEG', 'Summon — Beasts').
card_original_text('beasts of bogardan'/'LEG', 'Protection from red\nGains +1/+1 if an opponent controls any white cards.').
card_first_print('beasts of bogardan', 'LEG').
card_image_name('beasts of bogardan'/'LEG', 'beasts of bogardan').
card_uid('beasts of bogardan'/'LEG', 'LEG:Beasts of Bogardan:beasts of bogardan').
card_rarity('beasts of bogardan'/'LEG', 'Uncommon').
card_artist('beasts of bogardan'/'LEG', 'Daniel Gelon').
card_flavor_text('beasts of bogardan'/'LEG', 'Bogardan is a land as volatile as the creatures who live there.').
card_multiverse_id('beasts of bogardan'/'LEG', '1559').

card_in_set('black mana battery', 'LEG').
card_original_type('black mana battery'/'LEG', 'Artifact').
card_original_text('black mana battery'/'LEG', '{2}{T}: Put one counter on Black Mana Battery.\n{T}: Add {B} to your mana pool. Remove as many counters as you wish. For each counter removed add {B} to your mana pool. This ability is played as an interrupt.').
card_first_print('black mana battery', 'LEG').
card_image_name('black mana battery'/'LEG', 'black mana battery').
card_uid('black mana battery'/'LEG', 'LEG:Black Mana Battery:black mana battery').
card_rarity('black mana battery'/'LEG', 'Uncommon').
card_artist('black mana battery'/'LEG', 'Anson Maddocks').
card_multiverse_id('black mana battery'/'LEG', '1401').

card_in_set('blazing effigy', 'LEG').
card_original_type('blazing effigy'/'LEG', 'Summon — Effigy').
card_original_text('blazing effigy'/'LEG', 'When placed in the graveyard from play, Effigy does 3 damage to target creature. If an Effigy is damaged by another Effigy in this manner and is placed in the graveyard that turn, it deals the amount of damage received from the other Effigy in addition to its normal 3.').
card_first_print('blazing effigy', 'LEG').
card_image_name('blazing effigy'/'LEG', 'blazing effigy').
card_uid('blazing effigy'/'LEG', 'LEG:Blazing Effigy:blazing effigy').
card_rarity('blazing effigy'/'LEG', 'Common').
card_artist('blazing effigy'/'LEG', 'Susan Van Camp').
card_multiverse_id('blazing effigy'/'LEG', '1560').

card_in_set('blight', 'LEG').
card_original_type('blight'/'LEG', 'Enchant Land').
card_original_text('blight'/'LEG', 'If target land becomes tapped, it is destroyed at the end of the turn.').
card_first_print('blight', 'LEG').
card_image_name('blight'/'LEG', 'blight').
card_uid('blight'/'LEG', 'LEG:Blight:blight').
card_rarity('blight'/'LEG', 'Uncommon').
card_artist('blight'/'LEG', 'Pete Venters').
card_multiverse_id('blight'/'LEG', '1429').

card_in_set('blood lust', 'LEG').
card_original_type('blood lust'/'LEG', 'Instant').
card_original_text('blood lust'/'LEG', 'Target creatures gain +4/-4 until end of turn. If this reduces creature\'s toughness below 1, creature\'s toughness is 1.').
card_first_print('blood lust', 'LEG').
card_image_name('blood lust'/'LEG', 'blood lust').
card_uid('blood lust'/'LEG', 'LEG:Blood Lust:blood lust').
card_rarity('blood lust'/'LEG', 'Uncommon').
card_artist('blood lust'/'LEG', 'Anson Maddocks').
card_multiverse_id('blood lust'/'LEG', '1561').

card_in_set('blue mana battery', 'LEG').
card_original_type('blue mana battery'/'LEG', 'Artifact').
card_original_text('blue mana battery'/'LEG', '{2}{T}: Put one counter on Blue Mana Battery.\n{T}: Add {U} to your mana pool. Remove as many counters as you wish. For each counter removed add {U} to your mana pool. This ability is played as an interrupt.').
card_first_print('blue mana battery', 'LEG').
card_image_name('blue mana battery'/'LEG', 'blue mana battery').
card_uid('blue mana battery'/'LEG', 'LEG:Blue Mana Battery:blue mana battery').
card_rarity('blue mana battery'/'LEG', 'Uncommon').
card_artist('blue mana battery'/'LEG', 'Amy Weber').
card_multiverse_id('blue mana battery'/'LEG', '1402').

card_in_set('boomerang', 'LEG').
card_original_type('boomerang'/'LEG', 'Instant').
card_original_text('boomerang'/'LEG', 'Return target permanent to owner\'s hand; enchantments on target permanent are destroyed.').
card_first_print('boomerang', 'LEG').
card_image_name('boomerang'/'LEG', 'boomerang').
card_uid('boomerang'/'LEG', 'LEG:Boomerang:boomerang').
card_rarity('boomerang'/'LEG', 'Common').
card_artist('boomerang'/'LEG', 'Brian Snõddy').
card_flavor_text('boomerang'/'LEG', '\"O! call back yesterday, bid time return.\" —William Shakespeare, King Richard the Second').
card_multiverse_id('boomerang'/'LEG', '1474').

card_in_set('boris devilboon', 'LEG').
card_original_type('boris devilboon'/'LEG', 'Summon — Legend').
card_original_text('boris devilboon'/'LEG', '{2}{B}{R}{T}: Put a minor demon token into play. Treat this token as a 1/1 red and black creature.').
card_first_print('boris devilboon', 'LEG').
card_image_name('boris devilboon'/'LEG', 'boris devilboon').
card_uid('boris devilboon'/'LEG', 'LEG:Boris Devilboon:boris devilboon').
card_rarity('boris devilboon'/'LEG', 'Rare').
card_artist('boris devilboon'/'LEG', 'Jesper Myrfors').
card_multiverse_id('boris devilboon'/'LEG', '1649').

card_in_set('brine hag', 'LEG').
card_original_type('brine hag'/'LEG', 'Summon — Hag').
card_original_text('brine hag'/'LEG', 'On the turn during which Hag is placed in the graveyard, all creatures who dealt damage to Hag that turn become 0/2 creatures. Use counters to mark these creatures.').
card_first_print('brine hag', 'LEG').
card_image_name('brine hag'/'LEG', 'brine hag').
card_uid('brine hag'/'LEG', 'LEG:Brine Hag:brine hag').
card_rarity('brine hag'/'LEG', 'Uncommon').
card_artist('brine hag'/'LEG', 'Quinton Hoover').
card_multiverse_id('brine hag'/'LEG', '1475').

card_in_set('bronze horse', 'LEG').
card_original_type('bronze horse'/'LEG', 'Artifact Creature').
card_original_text('bronze horse'/'LEG', 'Trample\nDamage done to Bronze Horse by spells which target it is reduced to zero as long as you control another creature.').
card_first_print('bronze horse', 'LEG').
card_image_name('bronze horse'/'LEG', 'bronze horse').
card_uid('bronze horse'/'LEG', 'LEG:Bronze Horse:bronze horse').
card_rarity('bronze horse'/'LEG', 'Rare').
card_artist('bronze horse'/'LEG', 'Mark Poole').
card_multiverse_id('bronze horse'/'LEG', '1403').

card_in_set('carrion ants', 'LEG').
card_original_type('carrion ants'/'LEG', 'Summon — Ants').
card_original_text('carrion ants'/'LEG', '{1}: +1/+1 until end of turn.').
card_first_print('carrion ants', 'LEG').
card_image_name('carrion ants'/'LEG', 'carrion ants').
card_uid('carrion ants'/'LEG', 'LEG:Carrion Ants:carrion ants').
card_rarity('carrion ants'/'LEG', 'Rare').
card_artist('carrion ants'/'LEG', 'Richard Thomas').
card_flavor_text('carrion ants'/'LEG', '\"‘War is no picnic,\' my father liked to say. But the Ants seemed to disagree.\" —General Chanek Valteroth').
card_multiverse_id('carrion ants'/'LEG', '1430').

card_in_set('cat warriors', 'LEG').
card_original_type('cat warriors'/'LEG', 'Summon — Cat Warriors').
card_original_text('cat warriors'/'LEG', 'Forestwalk').
card_first_print('cat warriors', 'LEG').
card_image_name('cat warriors'/'LEG', 'cat warriors').
card_uid('cat warriors'/'LEG', 'LEG:Cat Warriors:cat warriors').
card_rarity('cat warriors'/'LEG', 'Common').
card_artist('cat warriors'/'LEG', 'Melissa A. Benson').
card_flavor_text('cat warriors'/'LEG', 'These stealthy felines have survived so many battles that some believe they must possess many lives.').
card_multiverse_id('cat warriors'/'LEG', '1517').

card_in_set('cathedral of serra', 'LEG').
card_original_type('cathedral of serra'/'LEG', 'Land').
card_original_text('cathedral of serra'/'LEG', 'All your white legends gain bands with other legends.').
card_first_print('cathedral of serra', 'LEG').
card_image_name('cathedral of serra'/'LEG', 'cathedral of serra').
card_uid('cathedral of serra'/'LEG', 'LEG:Cathedral of Serra:cathedral of serra').
card_rarity('cathedral of serra'/'LEG', 'Uncommon').
card_artist('cathedral of serra'/'LEG', 'Mark Poole').
card_multiverse_id('cathedral of serra'/'LEG', '1699').

card_in_set('caverns of despair', 'LEG').
card_original_type('caverns of despair'/'LEG', 'Enchant World').
card_original_text('caverns of despair'/'LEG', 'All players may attack with no more than two creatures each turn and block with no more than two creatures each turn.').
card_first_print('caverns of despair', 'LEG').
card_image_name('caverns of despair'/'LEG', 'caverns of despair').
card_uid('caverns of despair'/'LEG', 'LEG:Caverns of Despair:caverns of despair').
card_rarity('caverns of despair'/'LEG', 'Rare').
card_artist('caverns of despair'/'LEG', 'Harold McNeill').
card_multiverse_id('caverns of despair'/'LEG', '1562').

card_in_set('chain lightning', 'LEG').
card_original_type('chain lightning'/'LEG', 'Sorcery').
card_original_text('chain lightning'/'LEG', 'Chain Lightning does 3 damage to one target. Each time Chain Lightning does damage, the target or target\'s controller may then pay {R}{R} to have Chain Lightning do 3 damage to any target of that player\'s choice.').
card_first_print('chain lightning', 'LEG').
card_image_name('chain lightning'/'LEG', 'chain lightning').
card_uid('chain lightning'/'LEG', 'LEG:Chain Lightning:chain lightning').
card_rarity('chain lightning'/'LEG', 'Common').
card_artist('chain lightning'/'LEG', 'Sandra Everingham').
card_multiverse_id('chain lightning'/'LEG', '1563').

card_in_set('chains of mephistopheles', 'LEG').
card_original_type('chains of mephistopheles'/'LEG', 'Enchantment').
card_original_text('chains of mephistopheles'/'LEG', 'Every time a player draws a card, that player must first discard a card from his or her hand. If there are no cards in player\'s hand, take top card from library and place it in the graveyard instead of drawing. This enchantment does not apply to the first card drawn by a player during the draw phase.').
card_first_print('chains of mephistopheles', 'LEG').
card_image_name('chains of mephistopheles'/'LEG', 'chains of mephistopheles').
card_uid('chains of mephistopheles'/'LEG', 'LEG:Chains of Mephistopheles:chains of mephistopheles').
card_rarity('chains of mephistopheles'/'LEG', 'Rare').
card_artist('chains of mephistopheles'/'LEG', 'Heather Hudson').
card_multiverse_id('chains of mephistopheles'/'LEG', '1431').

card_in_set('chromium', 'LEG').
card_original_type('chromium'/'LEG', 'Summon — Elder Dragon Legend').
card_original_text('chromium'/'LEG', 'Flying, Rampage: 2\nPay {B}{U}{W} during your upkeep or Chromium is buried.').
card_first_print('chromium', 'LEG').
card_image_name('chromium'/'LEG', 'chromium').
card_uid('chromium'/'LEG', 'LEG:Chromium:chromium').
card_rarity('chromium'/'LEG', 'Rare').
card_artist('chromium'/'LEG', 'Edward P. Beard, Jr.').
card_multiverse_id('chromium'/'LEG', '1650').

card_in_set('cleanse', 'LEG').
card_original_type('cleanse'/'LEG', 'Sorcery').
card_original_text('cleanse'/'LEG', 'All black creatures in play are destroyed.').
card_first_print('cleanse', 'LEG').
card_image_name('cleanse'/'LEG', 'cleanse').
card_uid('cleanse'/'LEG', 'LEG:Cleanse:cleanse').
card_rarity('cleanse'/'LEG', 'Rare').
card_artist('cleanse'/'LEG', 'Phil Foglio').
card_flavor_text('cleanse'/'LEG', 'The clouds broke and the sun\'s rays burst forth; each foul beast in its turn faltered, and was gone.').
card_multiverse_id('cleanse'/'LEG', '1603').

card_in_set('clergy of the holy nimbus', 'LEG').
card_original_type('clergy of the holy nimbus'/'LEG', 'Summon — Priest').
card_original_text('clergy of the holy nimbus'/'LEG', 'When Clergy are destroyed or take lethal damage, unless opponent pays {1} Clergy are regenerated.').
card_first_print('clergy of the holy nimbus', 'LEG').
card_image_name('clergy of the holy nimbus'/'LEG', 'clergy of the holy nimbus').
card_uid('clergy of the holy nimbus'/'LEG', 'LEG:Clergy of the Holy Nimbus:clergy of the holy nimbus').
card_rarity('clergy of the holy nimbus'/'LEG', 'Common').
card_artist('clergy of the holy nimbus'/'LEG', 'Daniel Gelon').
card_multiverse_id('clergy of the holy nimbus'/'LEG', '1604').

card_in_set('cocoon', 'LEG').
card_original_type('cocoon'/'LEG', 'Enchant Creature').
card_original_text('cocoon'/'LEG', 'Tap target creature you control and put three counters on it. Target creature does not untap as normal while it has one or more of these counters on it. Remove one counter during your upkeep. During the upkeep phase after the one in which the last counter was removed, Cocoon is destroyed and target creature gains a +1/+1 counter and flying ability.').
card_first_print('cocoon', 'LEG').
card_image_name('cocoon'/'LEG', 'cocoon').
card_uid('cocoon'/'LEG', 'LEG:Cocoon:cocoon').
card_rarity('cocoon'/'LEG', 'Uncommon').
card_artist('cocoon'/'LEG', 'Mark Tedin').
card_multiverse_id('cocoon'/'LEG', '1518').

card_in_set('concordant crossroads', 'LEG').
card_original_type('concordant crossroads'/'LEG', 'Enchant World').
card_original_text('concordant crossroads'/'LEG', 'Creatures may attack or use abilities that include the {T} symbol during the turn they are brought into play.').
card_first_print('concordant crossroads', 'LEG').
card_image_name('concordant crossroads'/'LEG', 'concordant crossroads').
card_uid('concordant crossroads'/'LEG', 'LEG:Concordant Crossroads:concordant crossroads').
card_rarity('concordant crossroads'/'LEG', 'Rare').
card_artist('concordant crossroads'/'LEG', 'Amy Weber').
card_multiverse_id('concordant crossroads'/'LEG', '1519').

card_in_set('cosmic horror', 'LEG').
card_original_type('cosmic horror'/'LEG', 'Summon — Horror').
card_original_text('cosmic horror'/'LEG', 'First strike\nPay {3}{B}{B}{B} during your upkeep or Cosmic Horror does 7 damage to you and is destroyed.').
card_first_print('cosmic horror', 'LEG').
card_image_name('cosmic horror'/'LEG', 'cosmic horror').
card_uid('cosmic horror'/'LEG', 'LEG:Cosmic Horror:cosmic horror').
card_rarity('cosmic horror'/'LEG', 'Rare').
card_artist('cosmic horror'/'LEG', 'Jesper Myrfors').
card_flavor_text('cosmic horror'/'LEG', '\"Then flashed the living lightning from her eyes,/ And screams of horror rend th\' affrighted skies.\"\n—Alexander Pope, The Rape of the Lock').
card_multiverse_id('cosmic horror'/'LEG', '1432').

card_in_set('craw giant', 'LEG').
card_original_type('craw giant'/'LEG', 'Summon — Giant').
card_original_text('craw giant'/'LEG', 'Trample\nRampage: 2').
card_first_print('craw giant', 'LEG').
card_image_name('craw giant'/'LEG', 'craw giant').
card_uid('craw giant'/'LEG', 'LEG:Craw Giant:craw giant').
card_rarity('craw giant'/'LEG', 'Uncommon').
card_artist('craw giant'/'LEG', 'Christopher Rush').
card_flavor_text('craw giant'/'LEG', 'Harthag gave a jolly laugh as he surveyed the army before him. \"Ho ho ho! Midgets! You think you can stand in my way?\"').
card_multiverse_id('craw giant'/'LEG', '1520').

card_in_set('crevasse', 'LEG').
card_original_type('crevasse'/'LEG', 'Enchantment').
card_original_text('crevasse'/'LEG', 'Creatures with mountainwalk may be blocked as if they did not have this ability.').
card_first_print('crevasse', 'LEG').
card_image_name('crevasse'/'LEG', 'crevasse').
card_uid('crevasse'/'LEG', 'LEG:Crevasse:crevasse').
card_rarity('crevasse'/'LEG', 'Uncommon').
card_artist('crevasse'/'LEG', 'Rob Alexander').
card_multiverse_id('crevasse'/'LEG', '1564').

card_in_set('crimson kobolds', 'LEG').
card_original_type('crimson kobolds'/'LEG', 'Summon — Kobolds').
card_original_text('crimson kobolds'/'LEG', 'This card is a red spell when cast and Kobolds are a red creature.').
card_first_print('crimson kobolds', 'LEG').
card_image_name('crimson kobolds'/'LEG', 'crimson kobolds').
card_uid('crimson kobolds'/'LEG', 'LEG:Crimson Kobolds:crimson kobolds').
card_rarity('crimson kobolds'/'LEG', 'Common').
card_artist('crimson kobolds'/'LEG', 'Anson Maddocks').
card_flavor_text('crimson kobolds'/'LEG', '\"Kobolds are harmless.\"\n—Bearand the Bold, epitaph').
card_multiverse_id('crimson kobolds'/'LEG', '1565').

card_in_set('crimson manticore', 'LEG').
card_original_type('crimson manticore'/'LEG', 'Summon — Manticore').
card_original_text('crimson manticore'/'LEG', 'Flying\n{R}{T}: Manticore does 1 damage to target attacking or blocking creature.').
card_first_print('crimson manticore', 'LEG').
card_image_name('crimson manticore'/'LEG', 'crimson manticore').
card_uid('crimson manticore'/'LEG', 'LEG:Crimson Manticore:crimson manticore').
card_rarity('crimson manticore'/'LEG', 'Rare').
card_artist('crimson manticore'/'LEG', 'Daniel Gelon').
card_flavor_text('crimson manticore'/'LEG', 'Noted neither for their good looks nor their charm, Crimson Manticores can be fearsome allies. As dinner companions, however, they are best left alone.').
card_multiverse_id('crimson manticore'/'LEG', '1566').

card_in_set('crookshank kobolds', 'LEG').
card_original_type('crookshank kobolds'/'LEG', 'Summon — Kobolds').
card_original_text('crookshank kobolds'/'LEG', 'This card is a red spell when cast and Kobolds are a red creature.').
card_first_print('crookshank kobolds', 'LEG').
card_image_name('crookshank kobolds'/'LEG', 'crookshank kobolds').
card_uid('crookshank kobolds'/'LEG', 'LEG:Crookshank Kobolds:crookshank kobolds').
card_rarity('crookshank kobolds'/'LEG', 'Common').
card_artist('crookshank kobolds'/'LEG', 'Christopher Rush').
card_flavor_text('crookshank kobolds'/'LEG', 'The Crookshank military boasts a standing army of nearly twenty-four million, give or take twenty-two million.').
card_multiverse_id('crookshank kobolds'/'LEG', '1567').

card_in_set('cyclopean mummy', 'LEG').
card_original_type('cyclopean mummy'/'LEG', 'Summon — Mummy').
card_original_text('cyclopean mummy'/'LEG', 'If Mummy is placed in the graveyard from play, remove it from the game.').
card_first_print('cyclopean mummy', 'LEG').
card_image_name('cyclopean mummy'/'LEG', 'cyclopean mummy').
card_uid('cyclopean mummy'/'LEG', 'LEG:Cyclopean Mummy:cyclopean mummy').
card_rarity('cyclopean mummy'/'LEG', 'Common').
card_artist('cyclopean mummy'/'LEG', 'Edward P. Beard, Jr.').
card_flavor_text('cyclopean mummy'/'LEG', 'The ritual of plucking out an eye to gain future sight is but a curse that enables the living to see their own deaths.').
card_multiverse_id('cyclopean mummy'/'LEG', '1433').

card_in_set('d\'avenant archer', 'LEG').
card_original_type('d\'avenant archer'/'LEG', 'Summon — Archer').
card_original_text('d\'avenant archer'/'LEG', '{T}: Archer does 1 damage to target attacking or blocking creature.').
card_first_print('d\'avenant archer', 'LEG').
card_image_name('d\'avenant archer'/'LEG', 'd\'avenant archer').
card_uid('d\'avenant archer'/'LEG', 'LEG:D\'Avenant Archer:d\'avenant archer').
card_rarity('d\'avenant archer'/'LEG', 'Common').
card_artist('d\'avenant archer'/'LEG', 'Douglas Shuler').
card_multiverse_id('d\'avenant archer'/'LEG', '1605').

card_in_set('dakkon blackblade', 'LEG').
card_original_type('dakkon blackblade'/'LEG', 'Summon — Legend').
card_original_text('dakkon blackblade'/'LEG', 'The *s below equal the number of lands you control.').
card_first_print('dakkon blackblade', 'LEG').
card_image_name('dakkon blackblade'/'LEG', 'dakkon blackblade').
card_uid('dakkon blackblade'/'LEG', 'LEG:Dakkon Blackblade:dakkon blackblade').
card_rarity('dakkon blackblade'/'LEG', 'Rare').
card_artist('dakkon blackblade'/'LEG', 'Richard Kane Ferguson').
card_flavor_text('dakkon blackblade'/'LEG', '\"My power is as vast as the plains, my strength is that of mountains. Each wave that crashes upon the shore thunders like blood in my veins.\" —Memoirs').
card_multiverse_id('dakkon blackblade'/'LEG', '1651').

card_in_set('darkness', 'LEG').
card_original_type('darkness'/'LEG', 'Instant').
card_original_text('darkness'/'LEG', 'Creatures attack and block as normal, but none deal any damage. All attacking creatures are still tapped. Play any time before attack damage is assigned.').
card_first_print('darkness', 'LEG').
card_image_name('darkness'/'LEG', 'darkness').
card_uid('darkness'/'LEG', 'LEG:Darkness:darkness').
card_rarity('darkness'/'LEG', 'Common').
card_artist('darkness'/'LEG', 'Harold McNeill').
card_flavor_text('darkness'/'LEG', '\"If I must die, I will encounter darkness as a bride,/ And hug it in my arms.\" —William Shakespeare, Measure for Measure').
card_multiverse_id('darkness'/'LEG', '1434').

card_in_set('deadfall', 'LEG').
card_original_type('deadfall'/'LEG', 'Enchantment').
card_original_text('deadfall'/'LEG', 'Creatures with forestwalk may be blocked as if they did not have this ability.').
card_first_print('deadfall', 'LEG').
card_image_name('deadfall'/'LEG', 'deadfall').
card_uid('deadfall'/'LEG', 'LEG:Deadfall:deadfall').
card_rarity('deadfall'/'LEG', 'Uncommon').
card_artist('deadfall'/'LEG', 'NéNé Thomas').
card_multiverse_id('deadfall'/'LEG', '1521').

card_in_set('demonic torment', 'LEG').
card_original_type('demonic torment'/'LEG', 'Enchant Creature').
card_original_text('demonic torment'/'LEG', 'Target creature deals no damage during combat. Creature cannot attack.').
card_first_print('demonic torment', 'LEG').
card_image_name('demonic torment'/'LEG', 'demonic torment').
card_uid('demonic torment'/'LEG', 'LEG:Demonic Torment:demonic torment').
card_rarity('demonic torment'/'LEG', 'Uncommon').
card_artist('demonic torment'/'LEG', 'Anson Maddocks').
card_multiverse_id('demonic torment'/'LEG', '1435').

card_in_set('devouring deep', 'LEG').
card_original_type('devouring deep'/'LEG', 'Summon — Devouring Deep').
card_original_text('devouring deep'/'LEG', 'Islandwalk').
card_first_print('devouring deep', 'LEG').
card_image_name('devouring deep'/'LEG', 'devouring deep').
card_uid('devouring deep'/'LEG', 'LEG:Devouring Deep:devouring deep').
card_rarity('devouring deep'/'LEG', 'Common').
card_artist('devouring deep'/'LEG', 'Liz Danforth').
card_flavor_text('devouring deep'/'LEG', '\"Full fathom five thy father lies;/ Of his bones are coral made;/ Those are pearls that were his eyes;/ Nothing of him that doth fade,/ But doth suffer a sea-change/ Into something rich and strange.\" —William Shakespeare, The Tempest').
card_multiverse_id('devouring deep'/'LEG', '1476').

card_in_set('disharmony', 'LEG').
card_original_type('disharmony'/'LEG', 'Instant').
card_original_text('disharmony'/'LEG', 'Target attacking creature comes under your control untapped. Return to former controller at end of turn. This creature is no longer considered to have attacked. Play before defense is chosen.').
card_first_print('disharmony', 'LEG').
card_image_name('disharmony'/'LEG', 'disharmony').
card_uid('disharmony'/'LEG', 'LEG:Disharmony:disharmony').
card_rarity('disharmony'/'LEG', 'Rare').
card_artist('disharmony'/'LEG', 'Bryon Wackwitz').
card_multiverse_id('disharmony'/'LEG', '1568').

card_in_set('divine intervention', 'LEG').
card_original_type('divine intervention'/'LEG', 'Enchantment').
card_original_text('divine intervention'/'LEG', 'Put two counters on this card. Remove a counter during your upkeep. When you remove the last counter from Divine Intervention, the game is over and considered a draw.').
card_first_print('divine intervention', 'LEG').
card_image_name('divine intervention'/'LEG', 'divine intervention').
card_uid('divine intervention'/'LEG', 'LEG:Divine Intervention:divine intervention').
card_rarity('divine intervention'/'LEG', 'Rare').
card_artist('divine intervention'/'LEG', 'Amy Weber').
card_multiverse_id('divine intervention'/'LEG', '1606').

card_in_set('divine offering', 'LEG').
card_original_type('divine offering'/'LEG', 'Instant').
card_original_text('divine offering'/'LEG', 'Destroy target artifact. You gain life points equal to casting cost of artifact.').
card_first_print('divine offering', 'LEG').
card_image_name('divine offering'/'LEG', 'divine offering').
card_uid('divine offering'/'LEG', 'LEG:Divine Offering:divine offering').
card_rarity('divine offering'/'LEG', 'Common').
card_artist('divine offering'/'LEG', 'Jeff A. Menges').
card_flavor_text('divine offering'/'LEG', 'D\'Haren stared at the twisted lump of metal that had been a prized artifact. The fight was getting ugly.').
card_multiverse_id('divine offering'/'LEG', '1607').

card_in_set('divine transformation', 'LEG').
card_original_type('divine transformation'/'LEG', 'Enchant Creature').
card_original_text('divine transformation'/'LEG', 'Target creature gains +3/+3.').
card_first_print('divine transformation', 'LEG').
card_image_name('divine transformation'/'LEG', 'divine transformation').
card_uid('divine transformation'/'LEG', 'LEG:Divine Transformation:divine transformation').
card_rarity('divine transformation'/'LEG', 'Rare').
card_artist('divine transformation'/'LEG', 'NéNé Thomas').
card_flavor_text('divine transformation'/'LEG', 'Glory surged through her and radiance surrounded her. All things were possible with the blessing of the Divine.').
card_multiverse_id('divine transformation'/'LEG', '1608').

card_in_set('dream coat', 'LEG').
card_original_type('dream coat'/'LEG', 'Enchant Creature').
card_original_text('dream coat'/'LEG', 'Caster may change target creature\'s color to any other color. This ability is played as an interrupt. Limit of one change per turn. Cost to tap, maintain, or use a special ability of target creature remains entirely unchanged.').
card_first_print('dream coat', 'LEG').
card_image_name('dream coat'/'LEG', 'dream coat').
card_uid('dream coat'/'LEG', 'LEG:Dream Coat:dream coat').
card_rarity('dream coat'/'LEG', 'Uncommon').
card_artist('dream coat'/'LEG', 'Anthony Waters').
card_flavor_text('dream coat'/'LEG', '\"Adopt the character of the twisting octopus, which takes on the appearance of the nearby rock. Now follow in this direction, now turn a different hue.\" —Theognis, Elegies 1, 215').
card_multiverse_id('dream coat'/'LEG', '1477').

card_in_set('durkwood boars', 'LEG').
card_original_type('durkwood boars'/'LEG', 'Summon — Boars').
card_original_text('durkwood boars'/'LEG', '').
card_first_print('durkwood boars', 'LEG').
card_image_name('durkwood boars'/'LEG', 'durkwood boars').
card_uid('durkwood boars'/'LEG', 'LEG:Durkwood Boars:durkwood boars').
card_rarity('durkwood boars'/'LEG', 'Common').
card_artist('durkwood boars'/'LEG', 'Mike Kimble').
card_flavor_text('durkwood boars'/'LEG', '\"And the unclean spirits went out, and entered the swine; and the herd ran violently . . .\" —Mark 5:13').
card_multiverse_id('durkwood boars'/'LEG', '1522').

card_in_set('dwarven song', 'LEG').
card_original_type('dwarven song'/'LEG', 'Instant').
card_original_text('dwarven song'/'LEG', 'Changes the color of one or more target creatures to red until end of turn. You choose which and how many creatures are affected. Cost to tap, maintain, or use a special ability of target creatures remains entirely unchanged.').
card_first_print('dwarven song', 'LEG').
card_image_name('dwarven song'/'LEG', 'dwarven song').
card_uid('dwarven song'/'LEG', 'LEG:Dwarven Song:dwarven song').
card_rarity('dwarven song'/'LEG', 'Uncommon').
card_artist('dwarven song'/'LEG', 'Dan Frazier').
card_multiverse_id('dwarven song'/'LEG', '1569').

card_in_set('elder land wurm', 'LEG').
card_original_type('elder land wurm'/'LEG', 'Summon — Wurm').
card_original_text('elder land wurm'/'LEG', 'Trample\nWurm cannot attack until it has been assigned as a blocker.').
card_first_print('elder land wurm', 'LEG').
card_image_name('elder land wurm'/'LEG', 'elder land wurm').
card_uid('elder land wurm'/'LEG', 'LEG:Elder Land Wurm:elder land wurm').
card_rarity('elder land wurm'/'LEG', 'Rare').
card_artist('elder land wurm'/'LEG', 'Quinton Hoover').
card_flavor_text('elder land wurm'/'LEG', 'Sometimes it\'s best to let sleeping dragons lie.').
card_multiverse_id('elder land wurm'/'LEG', '1609').

card_in_set('elder spawn', 'LEG').
card_original_type('elder spawn'/'LEG', 'Summon — Spawn').
card_original_text('elder spawn'/'LEG', 'Elder Spawn cannot be blocked by red creatures. Sacrifice one of your islands during your upkeep or Elder Spawn does 6 damage to you and is buried.').
card_first_print('elder spawn', 'LEG').
card_image_name('elder spawn'/'LEG', 'elder spawn').
card_uid('elder spawn'/'LEG', 'LEG:Elder Spawn:elder spawn').
card_rarity('elder spawn'/'LEG', 'Rare').
card_artist('elder spawn'/'LEG', 'Jesper Myrfors').
card_multiverse_id('elder spawn'/'LEG', '1478').

card_in_set('elven riders', 'LEG').
card_original_type('elven riders'/'LEG', 'Summon — Riders').
card_original_text('elven riders'/'LEG', 'Cannot be blocked by any creatures except walls and flying creatures.').
card_first_print('elven riders', 'LEG').
card_image_name('elven riders'/'LEG', 'elven riders').
card_uid('elven riders'/'LEG', 'LEG:Elven Riders:elven riders').
card_rarity('elven riders'/'LEG', 'Rare').
card_artist('elven riders'/'LEG', 'Melissa A. Benson').
card_flavor_text('elven riders'/'LEG', '\"Sometimes it is better to be swift of foot than strong of swordarm.\" —Eleven Proverb').
card_multiverse_id('elven riders'/'LEG', '1523').

card_in_set('emerald dragonfly', 'LEG').
card_original_type('emerald dragonfly'/'LEG', 'Summon — Dragonfly').
card_original_text('emerald dragonfly'/'LEG', 'Flying, {G}{G} First strike until end of turn.').
card_first_print('emerald dragonfly', 'LEG').
card_image_name('emerald dragonfly'/'LEG', 'emerald dragonfly').
card_uid('emerald dragonfly'/'LEG', 'LEG:Emerald Dragonfly:emerald dragonfly').
card_rarity('emerald dragonfly'/'LEG', 'Common').
card_artist('emerald dragonfly'/'LEG', 'Quinton Hoover').
card_flavor_text('emerald dragonfly'/'LEG', '\"Flittering, wheeling,/ darting in to strike, and then/ gone just as you blink.\"\n—\"Dragonfly Haiku,\" poet unkown').
card_multiverse_id('emerald dragonfly'/'LEG', '1524').

card_in_set('enchanted being', 'LEG').
card_original_type('enchanted being'/'LEG', 'Summon — Being').
card_original_text('enchanted being'/'LEG', 'Any damage dealt to Enchanted Being during combat by creatures with one or more enchantment cards played on them is reduced to 0.').
card_first_print('enchanted being', 'LEG').
card_image_name('enchanted being'/'LEG', 'enchanted being').
card_uid('enchanted being'/'LEG', 'LEG:Enchanted Being:enchanted being').
card_rarity('enchanted being'/'LEG', 'Common').
card_artist('enchanted being'/'LEG', 'Douglas Shuler').
card_multiverse_id('enchanted being'/'LEG', '1610').

card_in_set('enchantment alteration', 'LEG').
card_original_type('enchantment alteration'/'LEG', 'Instant').
card_original_text('enchantment alteration'/'LEG', 'Switch target enchantment from one creature to another or from one land to another. The controller of the enchantment does not change. New target of enchantment must be valid or this spell has no effect. Treat this as if the enchantment had just been cast on the new target.').
card_first_print('enchantment alteration', 'LEG').
card_image_name('enchantment alteration'/'LEG', 'enchantment alteration').
card_uid('enchantment alteration'/'LEG', 'LEG:Enchantment Alteration:enchantment alteration').
card_rarity('enchantment alteration'/'LEG', 'Common').
card_artist('enchantment alteration'/'LEG', 'Brian Snõddy').
card_multiverse_id('enchantment alteration'/'LEG', '1479').

card_in_set('energy tap', 'LEG').
card_original_type('energy tap'/'LEG', 'Sorcery').
card_original_text('energy tap'/'LEG', 'Target untapped creature you control becomes tapped. Add an amount of colorless mana equal to target creature\'s casting cost to your mana pool.').
card_first_print('energy tap', 'LEG').
card_image_name('energy tap'/'LEG', 'energy tap').
card_uid('energy tap'/'LEG', 'LEG:Energy Tap:energy tap').
card_rarity('energy tap'/'LEG', 'Common').
card_artist('energy tap'/'LEG', 'Daniel Gelon').
card_multiverse_id('energy tap'/'LEG', '1480').

card_in_set('equinox', 'LEG').
card_original_type('equinox'/'LEG', 'Enchant Land').
card_original_text('equinox'/'LEG', 'Tap land enchanted with Equinox to counter a spell that destroys one or more of your lands. This ability is played as an interrupt.').
card_first_print('equinox', 'LEG').
card_image_name('equinox'/'LEG', 'equinox').
card_uid('equinox'/'LEG', 'LEG:Equinox:equinox').
card_rarity('equinox'/'LEG', 'Common').
card_artist('equinox'/'LEG', 'Susan Van Camp').
card_multiverse_id('equinox'/'LEG', '1611').

card_in_set('eternal warrior', 'LEG').
card_original_type('eternal warrior'/'LEG', 'Enchant Creature').
card_original_text('eternal warrior'/'LEG', 'Attacking does not tap target creature.').
card_first_print('eternal warrior', 'LEG').
card_image_name('eternal warrior'/'LEG', 'eternal warrior').
card_uid('eternal warrior'/'LEG', 'LEG:Eternal Warrior:eternal warrior').
card_rarity('eternal warrior'/'LEG', 'Uncommon').
card_artist('eternal warrior'/'LEG', 'Anson Maddocks').
card_flavor_text('eternal warrior'/'LEG', 'Warriors of the Tsunami-nito School spend years in training to master the way of effortless effort.').
card_multiverse_id('eternal warrior'/'LEG', '1570').

card_in_set('eureka', 'LEG').
card_original_type('eureka'/'LEG', 'Sorcery').
card_original_text('eureka'/'LEG', 'Both players may take any permanent in their hand and put it directly into play. Players take turns playing one card from their hand until neither wants to play more permanents. No other spells or effects of any kind may be used while Eureka is in effect. If a spell has an X in its casting cost, X is 0.').
card_first_print('eureka', 'LEG').
card_image_name('eureka'/'LEG', 'eureka').
card_uid('eureka'/'LEG', 'LEG:Eureka:eureka').
card_rarity('eureka'/'LEG', 'Rare').
card_artist('eureka'/'LEG', 'Kaja Foglio').
card_multiverse_id('eureka'/'LEG', '1525').

card_in_set('evil eye of orms-by-gore', 'LEG').
card_original_type('evil eye of orms-by-gore'/'LEG', 'Summon — Evil Eye').
card_original_text('evil eye of orms-by-gore'/'LEG', 'None of your creatures can attack except for Evil Eyes. Evil Eye can only be blocked by walls.').
card_first_print('evil eye of orms-by-gore', 'LEG').
card_image_name('evil eye of orms-by-gore'/'LEG', 'evil eye of orms-by-gore').
card_uid('evil eye of orms-by-gore'/'LEG', 'LEG:Evil Eye of Orms-by-Gore:evil eye of orms-by-gore').
card_rarity('evil eye of orms-by-gore'/'LEG', 'Uncommon').
card_artist('evil eye of orms-by-gore'/'LEG', 'Jesper Myrfors').
card_flavor_text('evil eye of orms-by-gore'/'LEG', '\"The highway of fear is the shortest route to defeat.\"').
card_multiverse_id('evil eye of orms-by-gore'/'LEG', '1436').

card_in_set('fallen angel', 'LEG').
card_original_type('fallen angel'/'LEG', 'Summon — Angel').
card_original_text('fallen angel'/'LEG', 'Flying\nSacrifice a creature to give Fallen Angel +2/+1 until end of turn.').
card_first_print('fallen angel', 'LEG').
card_image_name('fallen angel'/'LEG', 'fallen angel').
card_uid('fallen angel'/'LEG', 'LEG:Fallen Angel:fallen angel').
card_rarity('fallen angel'/'LEG', 'Uncommon').
card_artist('fallen angel'/'LEG', 'Anson Maddocks').
card_multiverse_id('fallen angel'/'LEG', '1437').

card_in_set('falling star', 'LEG').
card_original_type('falling star'/'LEG', 'Sorcery').
card_original_text('falling star'/'LEG', 'Flip Star onto the playing area from a height of at least one foot. Star must turn at least 360 degrees or it has no effect. When Falling Star lands, Falling Star does 3 damage to each creature that it touches. Any creatures damaged by Falling Star that are not destroyed become tapped.').
card_first_print('falling star', 'LEG').
card_image_name('falling star'/'LEG', 'falling star').
card_uid('falling star'/'LEG', 'LEG:Falling Star:falling star').
card_rarity('falling star'/'LEG', 'Rare').
card_artist('falling star'/'LEG', 'Douglas Shuler').
card_multiverse_id('falling star'/'LEG', '1571').

card_in_set('feint', 'LEG').
card_original_type('feint'/'LEG', 'Instant').
card_original_text('feint'/'LEG', 'All creatures blocking target attacking creature become tapped. Target attacking creature and all creatures blocking it deal no damage during combat.').
card_first_print('feint', 'LEG').
card_image_name('feint'/'LEG', 'feint').
card_uid('feint'/'LEG', 'LEG:Feint:feint').
card_rarity('feint'/'LEG', 'Common').
card_artist('feint'/'LEG', 'Brian Snõddy').
card_multiverse_id('feint'/'LEG', '1572').

card_in_set('field of dreams', 'LEG').
card_original_type('field of dreams'/'LEG', 'Enchant World').
card_original_text('field of dreams'/'LEG', 'The top card of each player\'s library is always face up.').
card_first_print('field of dreams', 'LEG').
card_image_name('field of dreams'/'LEG', 'field of dreams').
card_uid('field of dreams'/'LEG', 'LEG:Field of Dreams:field of dreams').
card_rarity('field of dreams'/'LEG', 'Rare').
card_artist('field of dreams'/'LEG', 'Kaja Foglio').
card_flavor_text('field of dreams'/'LEG', 'Some people say that the world is round, and if you travel far enough you\'ll come to the other side, where everything is upside down.').
card_multiverse_id('field of dreams'/'LEG', '1481').

card_in_set('fire sprites', 'LEG').
card_original_type('fire sprites'/'LEG', 'Summon — Faeries').
card_original_text('fire sprites'/'LEG', 'Flying\n{G}{T}: Add {R} to your mana pool. This ability is played as an interrupt.').
card_first_print('fire sprites', 'LEG').
card_image_name('fire sprites'/'LEG', 'fire sprites').
card_uid('fire sprites'/'LEG', 'LEG:Fire Sprites:fire sprites').
card_rarity('fire sprites'/'LEG', 'Common').
card_artist('fire sprites'/'LEG', 'Julie Baroh').
card_multiverse_id('fire sprites'/'LEG', '1526').

card_in_set('firestorm phoenix', 'LEG').
card_original_type('firestorm phoenix'/'LEG', 'Summon — Phoenix').
card_original_text('firestorm phoenix'/'LEG', 'Flying\nIf Phoenix is placed in the graveyard from play, return it to owner\'s hand instead. It may not be summoned again until owner\'s next turn.').
card_first_print('firestorm phoenix', 'LEG').
card_image_name('firestorm phoenix'/'LEG', 'firestorm phoenix').
card_uid('firestorm phoenix'/'LEG', 'LEG:Firestorm Phoenix:firestorm phoenix').
card_rarity('firestorm phoenix'/'LEG', 'Rare').
card_artist('firestorm phoenix'/'LEG', 'Jeff A. Menges').
card_flavor_text('firestorm phoenix'/'LEG', '\"The bird of wonder dies, the maiden phoenix,/ Her ashes new-create another heir/ As great in admiration as herself.\" —William Shakespeare, King Henry the Eighth.').
card_multiverse_id('firestorm phoenix'/'LEG', '1573').

card_in_set('flash counter', 'LEG').
card_original_type('flash counter'/'LEG', 'Interrupt').
card_original_text('flash counter'/'LEG', 'Counters target interrupt or instant spell.').
card_first_print('flash counter', 'LEG').
card_image_name('flash counter'/'LEG', 'flash counter').
card_uid('flash counter'/'LEG', 'LEG:Flash Counter:flash counter').
card_rarity('flash counter'/'LEG', 'Common').
card_artist('flash counter'/'LEG', 'Harold McNeill').
card_flavor_text('flash counter'/'LEG', '\"She grinned at me—a wicked grin. ‘I hope you weren\'t relying too heavily on that, my dear.\'\" —Medryn Silverwand, Diary').
card_multiverse_id('flash counter'/'LEG', '1482').

card_in_set('flash flood', 'LEG').
card_original_type('flash flood'/'LEG', 'Instant').
card_original_text('flash flood'/'LEG', 'Destroy target red permanent, or return target mountain to owner\'s hand. Enchantments on target land are destroyed.').
card_first_print('flash flood', 'LEG').
card_image_name('flash flood'/'LEG', 'flash flood').
card_uid('flash flood'/'LEG', 'LEG:Flash Flood:flash flood').
card_rarity('flash flood'/'LEG', 'Common').
card_artist('flash flood'/'LEG', 'Tom Wänerstrand').
card_flavor_text('flash flood'/'LEG', 'Many people say that no power can bring the mountains low. Many people are fools.').
card_multiverse_id('flash flood'/'LEG', '1483').

card_in_set('floral spuzzem', 'LEG').
card_original_type('floral spuzzem'/'LEG', 'Summon — Spuzzem').
card_original_text('floral spuzzem'/'LEG', 'If Floral Spuzzem attacks an opponent and is not blocked, then Floral Spuzzem may choose to destroy a target artifact under that opponent\'s control and deal no damage.').
card_first_print('floral spuzzem', 'LEG').
card_image_name('floral spuzzem'/'LEG', 'floral spuzzem').
card_uid('floral spuzzem'/'LEG', 'LEG:Floral Spuzzem:floral spuzzem').
card_rarity('floral spuzzem'/'LEG', 'Uncommon').
card_artist('floral spuzzem'/'LEG', 'Rob Alexander').
card_multiverse_id('floral spuzzem'/'LEG', '1527').

card_in_set('force spike', 'LEG').
card_original_type('force spike'/'LEG', 'Interrupt').
card_original_text('force spike'/'LEG', 'Target spell is countered unless its caster spends an additional {1}.').
card_first_print('force spike', 'LEG').
card_image_name('force spike'/'LEG', 'force spike').
card_uid('force spike'/'LEG', 'LEG:Force Spike:force spike').
card_rarity('force spike'/'LEG', 'Common').
card_artist('force spike'/'LEG', 'Bryon Wackwitz').
card_multiverse_id('force spike'/'LEG', '1484').

card_in_set('forethought amulet', 'LEG').
card_original_type('forethought amulet'/'LEG', 'Artifact').
card_original_text('forethought amulet'/'LEG', 'Pay {3} during your upkeep or Forethought Amulet is destroyed. If you receive more than 2 damage from a sorcery or instant source, that damage is reduced to 2.').
card_first_print('forethought amulet', 'LEG').
card_image_name('forethought amulet'/'LEG', 'forethought amulet').
card_uid('forethought amulet'/'LEG', 'LEG:Forethought Amulet:forethought amulet').
card_rarity('forethought amulet'/'LEG', 'Rare').
card_artist('forethought amulet'/'LEG', 'Melissa A. Benson').
card_multiverse_id('forethought amulet'/'LEG', '1404').

card_in_set('fortified area', 'LEG').
card_original_type('fortified area'/'LEG', 'Enchantment').
card_original_text('fortified area'/'LEG', 'All your walls gain +1/+0 and banding.').
card_first_print('fortified area', 'LEG').
card_image_name('fortified area'/'LEG', 'fortified area').
card_uid('fortified area'/'LEG', 'LEG:Fortified Area:fortified area').
card_rarity('fortified area'/'LEG', 'Uncommon').
card_artist('fortified area'/'LEG', 'Randy Asplund-Faith').
card_multiverse_id('fortified area'/'LEG', '1612').

card_in_set('frost giant', 'LEG').
card_original_type('frost giant'/'LEG', 'Summon — Giant').
card_original_text('frost giant'/'LEG', 'Rampage: 2').
card_first_print('frost giant', 'LEG').
card_image_name('frost giant'/'LEG', 'frost giant').
card_uid('frost giant'/'LEG', 'LEG:Frost Giant:frost giant').
card_rarity('frost giant'/'LEG', 'Uncommon').
card_artist('frost giant'/'LEG', 'Daniel Gelon').
card_flavor_text('frost giant'/'LEG', 'The Frost Giants have been out in the cold a long, long time, but they have their rage to keep them warm.').
card_multiverse_id('frost giant'/'LEG', '1574').

card_in_set('gabriel angelfire', 'LEG').
card_original_type('gabriel angelfire'/'LEG', 'Summon — Legend').
card_original_text('gabriel angelfire'/'LEG', 'During your upkeep, Gabriel gains one of the following abilities until your next upkeep: flying, first strike, trample, or rampage: 3.').
card_first_print('gabriel angelfire', 'LEG').
card_image_name('gabriel angelfire'/'LEG', 'gabriel angelfire').
card_uid('gabriel angelfire'/'LEG', 'LEG:Gabriel Angelfire:gabriel angelfire').
card_rarity('gabriel angelfire'/'LEG', 'Rare').
card_artist('gabriel angelfire'/'LEG', 'Daniel Gelon').
card_multiverse_id('gabriel angelfire'/'LEG', '1652').

card_in_set('gaseous form', 'LEG').
card_original_type('gaseous form'/'LEG', 'Enchant Creature').
card_original_text('gaseous form'/'LEG', 'Damage done to target creature by creatures it blocks, or that block it, is reduced to 0. Creature deals no damage during combat.').
card_first_print('gaseous form', 'LEG').
card_image_name('gaseous form'/'LEG', 'gaseous form').
card_uid('gaseous form'/'LEG', 'LEG:Gaseous Form:gaseous form').
card_rarity('gaseous form'/'LEG', 'Common').
card_artist('gaseous form'/'LEG', 'Phil Foglio').
card_flavor_text('gaseous form'/'LEG', '\". . . [A]nd gives to airy nothing/ A local habitation and a name.\" —William Shakespeare, A Midsummer-Night\'s Dream').
card_multiverse_id('gaseous form'/'LEG', '1485').

card_in_set('gauntlets of chaos', 'LEG').
card_original_type('gauntlets of chaos'/'LEG', 'Artifact').
card_original_text('gauntlets of chaos'/'LEG', '{5}: Sacrifice Gauntlets of Chaos. Take control of target land, creature, or artifact. Then give former controller of that permanent control of a target permanent of the same type under your control. You each control these permanents until game ends. Gauntlets of Chaos does not tap or untap these permanents. Enchantments on traded permanents are destroyed.').
card_first_print('gauntlets of chaos', 'LEG').
card_image_name('gauntlets of chaos'/'LEG', 'gauntlets of chaos').
card_uid('gauntlets of chaos'/'LEG', 'LEG:Gauntlets of Chaos:gauntlets of chaos').
card_rarity('gauntlets of chaos'/'LEG', 'Rare').
card_artist('gauntlets of chaos'/'LEG', 'Dan Frazier').
card_multiverse_id('gauntlets of chaos'/'LEG', '1405').

card_in_set('ghosts of the damned', 'LEG').
card_original_type('ghosts of the damned'/'LEG', 'Summon — Ghosts').
card_original_text('ghosts of the damned'/'LEG', '{T}: Target creature gets -1/-0 until end of turn.').
card_first_print('ghosts of the damned', 'LEG').
card_image_name('ghosts of the damned'/'LEG', 'ghosts of the damned').
card_uid('ghosts of the damned'/'LEG', 'LEG:Ghosts of the Damned:ghosts of the damned').
card_rarity('ghosts of the damned'/'LEG', 'Common').
card_artist('ghosts of the damned'/'LEG', 'Edward P. Beard, Jr.').
card_flavor_text('ghosts of the damned'/'LEG', 'The voices of the dead ring in the heart long after they have faded from the ears.').
card_multiverse_id('ghosts of the damned'/'LEG', '1438').

card_in_set('giant slug', 'LEG').
card_original_type('giant slug'/'LEG', 'Summon — Slug').
card_original_text('giant slug'/'LEG', '{5}: During controller\'s next upkeep Giant Slug gains landwalk ability of controller\'s choice until end of turn. The type of landwalk chosen must correspond with one of the five basic land types.').
card_first_print('giant slug', 'LEG').
card_image_name('giant slug'/'LEG', 'giant slug').
card_uid('giant slug'/'LEG', 'LEG:Giant Slug:giant slug').
card_rarity('giant slug'/'LEG', 'Common').
card_artist('giant slug'/'LEG', 'Anson Maddocks').
card_multiverse_id('giant slug'/'LEG', '1439').

card_in_set('giant strength', 'LEG').
card_original_type('giant strength'/'LEG', 'Enchant Creature').
card_original_text('giant strength'/'LEG', 'Target creature gains +2/+2').
card_first_print('giant strength', 'LEG').
card_image_name('giant strength'/'LEG', 'giant strength').
card_uid('giant strength'/'LEG', 'LEG:Giant Strength:giant strength').
card_rarity('giant strength'/'LEG', 'Common').
card_artist('giant strength'/'LEG', 'Justin Hampton').
card_flavor_text('giant strength'/'LEG', '\"O! it is excellent/ To have a giant\'s strength, but it is tyrannous/ To use it like a giant.\" —William Shakespeare, Measure for Measure').
card_multiverse_id('giant strength'/'LEG', '1575').

card_in_set('giant turtle', 'LEG').
card_original_type('giant turtle'/'LEG', 'Summon — Turtle').
card_original_text('giant turtle'/'LEG', 'Giant Turtle may not attack if it attacked during your last turn.').
card_first_print('giant turtle', 'LEG').
card_image_name('giant turtle'/'LEG', 'giant turtle').
card_uid('giant turtle'/'LEG', 'LEG:Giant Turtle:giant turtle').
card_rarity('giant turtle'/'LEG', 'Common').
card_artist('giant turtle'/'LEG', 'Jeff A. Menges').
card_flavor_text('giant turtle'/'LEG', '\"The turtle lives \'twixt plated decks/ Which practically conceal its sex./ I think it clever of the turtle/ In such a fix to be so fertile.\" —Ogden Nash, \"The Turtle\"').
card_multiverse_id('giant turtle'/'LEG', '1528').

card_in_set('glyph of delusion', 'LEG').
card_original_type('glyph of delusion'/'LEG', 'Instant').
card_original_text('glyph of delusion'/'LEG', 'Put X counters on one target creature that target wall blocked during this turn; X is the power of the blocked creature. Creature does not untap as normal while it has one or more of these counters on it. Remove one counter during creature\'s controller\'s upkeep.').
card_first_print('glyph of delusion', 'LEG').
card_image_name('glyph of delusion'/'LEG', 'glyph of delusion').
card_uid('glyph of delusion'/'LEG', 'LEG:Glyph of Delusion:glyph of delusion').
card_rarity('glyph of delusion'/'LEG', 'Common').
card_artist('glyph of delusion'/'LEG', 'Susan Van Camp').
card_multiverse_id('glyph of delusion'/'LEG', '1486').

card_in_set('glyph of destruction', 'LEG').
card_original_type('glyph of destruction'/'LEG', 'Instant').
card_original_text('glyph of destruction'/'LEG', 'Target wall you control gains +10/+0 when blocking. Any damage dealt to target wall is reduced to zero. Target wall is destroyed at end of turn.').
card_first_print('glyph of destruction', 'LEG').
card_image_name('glyph of destruction'/'LEG', 'glyph of destruction').
card_uid('glyph of destruction'/'LEG', 'LEG:Glyph of Destruction:glyph of destruction').
card_rarity('glyph of destruction'/'LEG', 'Common').
card_artist('glyph of destruction'/'LEG', 'Susan Van Camp').
card_multiverse_id('glyph of destruction'/'LEG', '1576').

card_in_set('glyph of doom', 'LEG').
card_original_type('glyph of doom'/'LEG', 'Instant').
card_original_text('glyph of doom'/'LEG', 'All creatures blocked by target wall are destroyed at the end of combat.').
card_first_print('glyph of doom', 'LEG').
card_image_name('glyph of doom'/'LEG', 'glyph of doom').
card_uid('glyph of doom'/'LEG', 'LEG:Glyph of Doom:glyph of doom').
card_rarity('glyph of doom'/'LEG', 'Common').
card_artist('glyph of doom'/'LEG', 'Susan Van Camp').
card_flavor_text('glyph of doom'/'LEG', '\"He knows he has a short span of life, that the day will come when he must pass through the wall of oblivion . . .\" —William Faulkner').
card_multiverse_id('glyph of doom'/'LEG', '1440').

card_in_set('glyph of life', 'LEG').
card_original_type('glyph of life'/'LEG', 'Instant').
card_original_text('glyph of life'/'LEG', 'Damage done to target wall by attacking creatures is added to your life point total.').
card_first_print('glyph of life', 'LEG').
card_image_name('glyph of life'/'LEG', 'glyph of life').
card_uid('glyph of life'/'LEG', 'LEG:Glyph of Life:glyph of life').
card_rarity('glyph of life'/'LEG', 'Common').
card_artist('glyph of life'/'LEG', 'Susan Van Camp').
card_flavor_text('glyph of life'/'LEG', 'Any wall can be battered down, but at what cost?').
card_multiverse_id('glyph of life'/'LEG', '1613').

card_in_set('glyph of reincarnation', 'LEG').
card_original_type('glyph of reincarnation'/'LEG', 'Instant').
card_original_text('glyph of reincarnation'/'LEG', 'Play after combat is over. All surviving creatures blocked by target wall this turn are buried. For each creature buried in this manner, choose one creature from attacker\'s graveyard and return it to play under attacker\'s control. Treat these creatures as if they were just summoned. If there are not enough creatures in attacker\'s graveyard, all creatures in attacker\'s graveyard are returned to play.').
card_first_print('glyph of reincarnation', 'LEG').
card_image_name('glyph of reincarnation'/'LEG', 'glyph of reincarnation').
card_uid('glyph of reincarnation'/'LEG', 'LEG:Glyph of Reincarnation:glyph of reincarnation').
card_rarity('glyph of reincarnation'/'LEG', 'Common').
card_artist('glyph of reincarnation'/'LEG', 'Susan Van Camp').
card_multiverse_id('glyph of reincarnation'/'LEG', '1529').

card_in_set('gosta dirk', 'LEG').
card_original_type('gosta dirk'/'LEG', 'Summon — Legend').
card_original_text('gosta dirk'/'LEG', 'First strike\nCreatures with islandwalk may be blocked as if they did not have this ability.').
card_first_print('gosta dirk', 'LEG').
card_image_name('gosta dirk'/'LEG', 'gosta dirk').
card_uid('gosta dirk'/'LEG', 'LEG:Gosta Dirk:gosta dirk').
card_rarity('gosta dirk'/'LEG', 'Rare').
card_artist('gosta dirk'/'LEG', 'Richard Thomas').
card_multiverse_id('gosta dirk'/'LEG', '1653').

card_in_set('gravity sphere', 'LEG').
card_original_type('gravity sphere'/'LEG', 'Enchant World').
card_original_text('gravity sphere'/'LEG', 'All creatures lose flying ability.').
card_first_print('gravity sphere', 'LEG').
card_image_name('gravity sphere'/'LEG', 'gravity sphere').
card_uid('gravity sphere'/'LEG', 'LEG:Gravity Sphere:gravity sphere').
card_rarity('gravity sphere'/'LEG', 'Rare').
card_artist('gravity sphere'/'LEG', 'Brian Snõddy').
card_flavor_text('gravity sphere'/'LEG', 'On the morning of the Battle of Gal-Shan, the Sorcerer\'s cry rang out: \"Let no bird fly, let no creature take wing, let all the battle join as one.\"').
card_multiverse_id('gravity sphere'/'LEG', '1577').

card_in_set('great defender', 'LEG').
card_original_type('great defender'/'LEG', 'Instant').
card_original_text('great defender'/'LEG', 'Target creature gains +0/+X until end of turn where X is the creature\'s casting cost.').
card_first_print('great defender', 'LEG').
card_image_name('great defender'/'LEG', 'great defender').
card_uid('great defender'/'LEG', 'LEG:Great Defender:great defender').
card_rarity('great defender'/'LEG', 'Uncommon').
card_artist('great defender'/'LEG', 'Mark Poole').
card_multiverse_id('great defender'/'LEG', '1614').

card_in_set('great wall', 'LEG').
card_original_type('great wall'/'LEG', 'Enchantment').
card_original_text('great wall'/'LEG', 'Creatures with plainswalk may be blocked as if they did not have this ability.').
card_first_print('great wall', 'LEG').
card_image_name('great wall'/'LEG', 'great wall').
card_uid('great wall'/'LEG', 'LEG:Great Wall:great wall').
card_rarity('great wall'/'LEG', 'Uncommon').
card_artist('great wall'/'LEG', 'Sandra Everingham').
card_multiverse_id('great wall'/'LEG', '1615').

card_in_set('greater realm of preservation', 'LEG').
card_original_type('greater realm of preservation'/'LEG', 'Enchantment').
card_original_text('greater realm of preservation'/'LEG', '{1}{W} Prevents all damage against you from one red or black source. If a source does damage to you more than once in a turn, you must pay {1}{W} each time you want to prevent the damage.').
card_first_print('greater realm of preservation', 'LEG').
card_image_name('greater realm of preservation'/'LEG', 'greater realm of preservation').
card_uid('greater realm of preservation'/'LEG', 'LEG:Greater Realm of Preservation:greater realm of preservation').
card_rarity('greater realm of preservation'/'LEG', 'Uncommon').
card_artist('greater realm of preservation'/'LEG', 'NéNé Thomas').
card_multiverse_id('greater realm of preservation'/'LEG', '1616').

card_in_set('greed', 'LEG').
card_original_type('greed'/'LEG', 'Enchantment').
card_original_text('greed'/'LEG', '{B} Draw an extra card and lose 2 life. Effects that prevent or redirect damage may not be used to counter this loss of life.').
card_first_print('greed', 'LEG').
card_image_name('greed'/'LEG', 'greed').
card_uid('greed'/'LEG', 'LEG:Greed:greed').
card_rarity('greed'/'LEG', 'Rare').
card_artist('greed'/'LEG', 'Phil Foglio').
card_flavor_text('greed'/'LEG', '\"There is no calamity greater than lavish desires./ There is no greater guilt than discontentment./ And there is no greater disaster than greed.\" —Tao Tê Ching 46').
card_multiverse_id('greed'/'LEG', '1441').

card_in_set('green mana battery', 'LEG').
card_original_type('green mana battery'/'LEG', 'Artifact').
card_original_text('green mana battery'/'LEG', '{2}{T}: Put one counter on Green Mana Battery.\n{T}: Add {G} to your mana pool. Remove as many counters as you wish. For each counter removed add {G} to your mana pool. This ability is played as an interrupt.').
card_first_print('green mana battery', 'LEG').
card_image_name('green mana battery'/'LEG', 'green mana battery').
card_uid('green mana battery'/'LEG', 'LEG:Green Mana Battery:green mana battery').
card_rarity('green mana battery'/'LEG', 'Uncommon').
card_artist('green mana battery'/'LEG', 'Christopher Rush').
card_multiverse_id('green mana battery'/'LEG', '1406').

card_in_set('gwendlyn di corci', 'LEG').
card_original_type('gwendlyn di corci'/'LEG', 'Summon — Legend').
card_original_text('gwendlyn di corci'/'LEG', '{T}: Target player discards one card from his or her hand at random. This power may only be used during your turn.').
card_first_print('gwendlyn di corci', 'LEG').
card_image_name('gwendlyn di corci'/'LEG', 'gwendlyn di corci').
card_uid('gwendlyn di corci'/'LEG', 'LEG:Gwendlyn Di Corci:gwendlyn di corci').
card_rarity('gwendlyn di corci'/'LEG', 'Rare').
card_artist('gwendlyn di corci'/'LEG', 'Julie Baroh').
card_flavor_text('gwendlyn di corci'/'LEG', 'Urborg became the final resting place of many swordsmen of renown, their steel outmatched by the wiles of Gwendlyn.').
card_multiverse_id('gwendlyn di corci'/'LEG', '1654').

card_in_set('halfdane', 'LEG').
card_original_type('halfdane'/'LEG', 'Summon — Legend').
card_original_text('halfdane'/'LEG', 'When Halfdane comes into play he is 3/3. During your upkeep, Halfdane acquires the current power and toughness of target creature other than Halfdane. If there are no legal targets, Halfdane becomes 3/3.').
card_first_print('halfdane', 'LEG').
card_image_name('halfdane'/'LEG', 'halfdane').
card_uid('halfdane'/'LEG', 'LEG:Halfdane:halfdane').
card_rarity('halfdane'/'LEG', 'Rare').
card_artist('halfdane'/'LEG', 'Melissa A. Benson').
card_flavor_text('halfdane'/'LEG', 'Hail from Tolaria the ever changing \'Dane.').
card_multiverse_id('halfdane'/'LEG', '1655').

card_in_set('hammerheim', 'LEG').
card_original_type('hammerheim'/'LEG', 'Legendary Land').
card_original_text('hammerheim'/'LEG', '{T}: Add {R} to your mana pool\n{T}: Remove all landwalking ability from target creature until end of turn.').
card_first_print('hammerheim', 'LEG').
card_image_name('hammerheim'/'LEG', 'hammerheim').
card_uid('hammerheim'/'LEG', 'LEG:Hammerheim:hammerheim').
card_rarity('hammerheim'/'LEG', 'Uncommon').
card_artist('hammerheim'/'LEG', 'Bryon Wackwitz').
card_flavor_text('hammerheim'/'LEG', '\"‘Tis distance lends enchantment to the view,/ And robes the mountain in its azure hue.\" —Thomas Campbell, \"Pleasures of Hope\"').
card_multiverse_id('hammerheim'/'LEG', '1700').

card_in_set('hazezon tamar', 'LEG').
card_original_type('hazezon tamar'/'LEG', 'Summon — Legend').
card_original_text('hazezon tamar'/'LEG', 'On your next upkeep after Hazezon is put into play, put * token Sand Warriors into play, where * is the number of lands under your control. Treat the Warriors as 1/1 white, green, and red creatures. If Hazezon leaves play, all Sand Warriors are also removed from game.').
card_first_print('hazezon tamar', 'LEG').
card_image_name('hazezon tamar'/'LEG', 'hazezon tamar').
card_uid('hazezon tamar'/'LEG', 'LEG:Hazezon Tamar:hazezon tamar').
card_rarity('hazezon tamar'/'LEG', 'Rare').
card_artist('hazezon tamar'/'LEG', 'Richard Kane Ferguson').
card_multiverse_id('hazezon tamar'/'LEG', '1656').

card_in_set('headless horseman', 'LEG').
card_original_type('headless horseman'/'LEG', 'Summon — Horseman').
card_original_text('headless horseman'/'LEG', '').
card_first_print('headless horseman', 'LEG').
card_image_name('headless horseman'/'LEG', 'headless horseman').
card_uid('headless horseman'/'LEG', 'LEG:Headless Horseman:headless horseman').
card_rarity('headless horseman'/'LEG', 'Common').
card_artist('headless horseman'/'LEG', 'Quinton Hoover').
card_flavor_text('headless horseman'/'LEG', '\". . . The ghost rides forth to the scene of battle in nightly quest of his head . . . he sometimes passes along the Hollow, like a midnight blast . . .\" —Washington Irving, The Legend of Sleepy Hollow').
card_multiverse_id('headless horseman'/'LEG', '1442').

card_in_set('heaven\'s gate', 'LEG').
card_original_type('heaven\'s gate'/'LEG', 'Instant').
card_original_text('heaven\'s gate'/'LEG', 'Changes the color of one or more target creatures to white until end of turn. You choose which and how many creatures are affected. Costs to tap, maintain, or use a special ability of target creatures remain entirely unchanged.').
card_first_print('heaven\'s gate', 'LEG').
card_image_name('heaven\'s gate'/'LEG', 'heaven\'s gate').
card_uid('heaven\'s gate'/'LEG', 'LEG:Heaven\'s Gate:heaven\'s gate').
card_rarity('heaven\'s gate'/'LEG', 'Uncommon').
card_artist('heaven\'s gate'/'LEG', 'Douglas Shuler').
card_multiverse_id('heaven\'s gate'/'LEG', '1617').

card_in_set('hell swarm', 'LEG').
card_original_type('hell swarm'/'LEG', 'Instant').
card_original_text('hell swarm'/'LEG', 'All creatures get -1/-0 until end of turn.').
card_first_print('hell swarm', 'LEG').
card_image_name('hell swarm'/'LEG', 'hell swarm').
card_uid('hell swarm'/'LEG', 'LEG:Hell Swarm:hell swarm').
card_rarity('hell swarm'/'LEG', 'Common').
card_artist('hell swarm'/'LEG', 'Christopher Rush').
card_flavor_text('hell swarm'/'LEG', 'The brightness of day turned in an instant to dusk as the swarm descended upon the battlefield.').
card_multiverse_id('hell swarm'/'LEG', '1443').

card_in_set('hell\'s caretaker', 'LEG').
card_original_type('hell\'s caretaker'/'LEG', 'Summon — Hell\'s Caretaker').
card_original_text('hell\'s caretaker'/'LEG', '{T}: During your upkeep sacrifice a creature and take a creature from your graveyard and put it directly into play. Treat this creature as though it were just summoned.').
card_first_print('hell\'s caretaker', 'LEG').
card_image_name('hell\'s caretaker'/'LEG', 'hell\'s caretaker').
card_uid('hell\'s caretaker'/'LEG', 'LEG:Hell\'s Caretaker:hell\'s caretaker').
card_rarity('hell\'s caretaker'/'LEG', 'Rare').
card_artist('hell\'s caretaker'/'LEG', 'Sandra Everingham').
card_flavor_text('hell\'s caretaker'/'LEG', 'You might leave here, Chenndra, should another take your place . . . .').
card_multiverse_id('hell\'s caretaker'/'LEG', '1444').

card_in_set('hellfire', 'LEG').
card_original_type('hellfire'/'LEG', 'Sorcery').
card_original_text('hellfire'/'LEG', 'All non-black creatures are destroyed. Hellfire does X + 3 damage to you; X is the number of creatures placed in the graveyard.').
card_first_print('hellfire', 'LEG').
card_image_name('hellfire'/'LEG', 'hellfire').
card_uid('hellfire'/'LEG', 'LEG:Hellfire:hellfire').
card_rarity('hellfire'/'LEG', 'Rare').
card_artist('hellfire'/'LEG', 'Pete Venters').
card_flavor_text('hellfire'/'LEG', '\"High on a throne of royal state . . . insatiate to pursue vain war with heav\'n.\" —John Milton, Paradise Lost').
card_multiverse_id('hellfire'/'LEG', '1445').

card_in_set('holy day', 'LEG').
card_original_type('holy day'/'LEG', 'Instant').
card_original_text('holy day'/'LEG', 'Creatures attack and block as normal, but none deal any damage. All attacking creatures are still tapped. Play any time before attack damage is assigned.').
card_first_print('holy day', 'LEG').
card_image_name('holy day'/'LEG', 'holy day').
card_uid('holy day'/'LEG', 'LEG:Holy Day:holy day').
card_rarity('holy day'/'LEG', 'Common').
card_artist('holy day'/'LEG', 'Justin Hampton').
card_flavor_text('holy day'/'LEG', '\"The day of spirits; my soul\'s clam retreat/ Which none disturb!\" —Henry Vaughan, \"Silex Scintillans\"').
card_multiverse_id('holy day'/'LEG', '1618').

card_in_set('horn of deafening', 'LEG').
card_original_type('horn of deafening'/'LEG', 'Artifact').
card_original_text('horn of deafening'/'LEG', '{2}{T}: Target creature deals no damage during combat this turn.').
card_first_print('horn of deafening', 'LEG').
card_image_name('horn of deafening'/'LEG', 'horn of deafening').
card_uid('horn of deafening'/'LEG', 'LEG:Horn of Deafening:horn of deafening').
card_rarity('horn of deafening'/'LEG', 'Rare').
card_artist('horn of deafening'/'LEG', 'Dan Frazier').
card_flavor_text('horn of deafening'/'LEG', '\"A blast, an echo . . . then silence.\"').
card_multiverse_id('horn of deafening'/'LEG', '1407').

card_in_set('hornet cobra', 'LEG').
card_original_type('hornet cobra'/'LEG', 'Summon — Cobra').
card_original_text('hornet cobra'/'LEG', 'First strike').
card_first_print('hornet cobra', 'LEG').
card_image_name('hornet cobra'/'LEG', 'hornet cobra').
card_uid('hornet cobra'/'LEG', 'LEG:Hornet Cobra:hornet cobra').
card_rarity('hornet cobra'/'LEG', 'Common').
card_artist('hornet cobra'/'LEG', 'Sandra Everingham').
card_flavor_text('hornet cobra'/'LEG', '\"Then inch by inch out of the grass rose up the head and spread hood of Nag, the big black cobra, and he was five feet long from tongue to tail.\" —Rudyard Kipling, The Jungle Books').
card_multiverse_id('hornet cobra'/'LEG', '1530').

card_in_set('horror of horrors', 'LEG').
card_original_type('horror of horrors'/'LEG', 'Enchantment').
card_original_text('horror of horrors'/'LEG', 'Allows caster to sacrifice a swamp to regenerate a target black creature.').
card_first_print('horror of horrors', 'LEG').
card_image_name('horror of horrors'/'LEG', 'horror of horrors').
card_uid('horror of horrors'/'LEG', 'LEG:Horror of Horrors:horror of horrors').
card_rarity('horror of horrors'/'LEG', 'Uncommon').
card_artist('horror of horrors'/'LEG', 'Mark Tedin').
card_flavor_text('horror of horrors'/'LEG', '\"And a horror of outer darkness after,/\nAnd dust returneth to dust again.\" —Adam Lindsay Gordon, The Swimmer').
card_multiverse_id('horror of horrors'/'LEG', '1446').

card_in_set('hunding gjornersen', 'LEG').
card_original_type('hunding gjornersen'/'LEG', 'Summon — Legend').
card_original_text('hunding gjornersen'/'LEG', 'Rampage: 1').
card_first_print('hunding gjornersen', 'LEG').
card_image_name('hunding gjornersen'/'LEG', 'hunding gjornersen').
card_uid('hunding gjornersen'/'LEG', 'LEG:Hunding Gjornersen:hunding gjornersen').
card_rarity('hunding gjornersen'/'LEG', 'Uncommon').
card_artist('hunding gjornersen'/'LEG', 'Richard Thomas').
card_flavor_text('hunding gjornersen'/'LEG', '\"You would never guess, at the terrifying sight of the man, that Hunding was as charming a companion as one could wish for.\"').
card_multiverse_id('hunding gjornersen'/'LEG', '1657').

card_in_set('hyperion blacksmith', 'LEG').
card_original_type('hyperion blacksmith'/'LEG', 'Summon — Smith').
card_original_text('hyperion blacksmith'/'LEG', '{T}: Target artifact controlled by opponent becomes tapped or untapped.').
card_first_print('hyperion blacksmith', 'LEG').
card_image_name('hyperion blacksmith'/'LEG', 'hyperion blacksmith').
card_uid('hyperion blacksmith'/'LEG', 'LEG:Hyperion Blacksmith:hyperion blacksmith').
card_rarity('hyperion blacksmith'/'LEG', 'Uncommon').
card_artist('hyperion blacksmith'/'LEG', 'Dan Frazier').
card_flavor_text('hyperion blacksmith'/'LEG', '\"The smith a mighty man is he/ With large and sinewy hands./ And the muscles of his brawny arms/ Are strong as iron bands.\" —Henry Wadsworth Longfellow, The Village Blacksmith').
card_multiverse_id('hyperion blacksmith'/'LEG', '1578').

card_in_set('ichneumon druid', 'LEG').
card_original_type('ichneumon druid'/'LEG', 'Summon — Druid').
card_original_text('ichneumon druid'/'LEG', 'Ichneumon Druid does 4 damage to any opponent casting an instant. This does not apply to the first instant cast by that opponent in each turn.').
card_first_print('ichneumon druid', 'LEG').
card_image_name('ichneumon druid'/'LEG', 'ichneumon druid').
card_uid('ichneumon druid'/'LEG', 'LEG:Ichneumon Druid:ichneumon druid').
card_rarity('ichneumon druid'/'LEG', 'Uncommon').
card_artist('ichneumon druid'/'LEG', 'Melissa A. Benson').
card_multiverse_id('ichneumon druid'/'LEG', '1531').

card_in_set('immolation', 'LEG').
card_original_type('immolation'/'LEG', 'Enchant Creature').
card_original_text('immolation'/'LEG', 'Target creature gains +2/-2.').
card_first_print('immolation', 'LEG').
card_image_name('immolation'/'LEG', 'immolation').
card_uid('immolation'/'LEG', 'LEG:Immolation:immolation').
card_rarity('immolation'/'LEG', 'Common').
card_artist('immolation'/'LEG', 'Scott Kirschner').
card_multiverse_id('immolation'/'LEG', '1579').

card_in_set('imprison', 'LEG').
card_original_type('imprison'/'LEG', 'Enchant Creature').
card_original_text('imprison'/'LEG', 'Pay 1 each time target creature attempts to attack, block, or tap. That action is prevented and the creature becomes tapped. Destroy enchantment if mana is not paid.').
card_first_print('imprison', 'LEG').
card_image_name('imprison'/'LEG', 'imprison').
card_uid('imprison'/'LEG', 'LEG:Imprison:imprison').
card_rarity('imprison'/'LEG', 'Rare').
card_artist('imprison'/'LEG', 'Christopher Rush').
card_multiverse_id('imprison'/'LEG', '1447').

card_in_set('in the eye of chaos', 'LEG').
card_original_type('in the eye of chaos'/'LEG', 'Enchant World').
card_original_text('in the eye of chaos'/'LEG', 'All instants and interrupts are countered unless their caster pays an additional {X}, where X is the casting cost of the spell being cast.').
card_first_print('in the eye of chaos', 'LEG').
card_image_name('in the eye of chaos'/'LEG', 'in the eye of chaos').
card_uid('in the eye of chaos'/'LEG', 'LEG:In the Eye of Chaos:in the eye of chaos').
card_rarity('in the eye of chaos'/'LEG', 'Rare').
card_artist('in the eye of chaos'/'LEG', 'Brian Snõddy').
card_multiverse_id('in the eye of chaos'/'LEG', '1487').

card_in_set('indestructible aura', 'LEG').
card_original_type('indestructible aura'/'LEG', 'Instant').
card_original_text('indestructible aura'/'LEG', 'Any damage dealt to target creature for remainder of turn is reduced to 0.').
card_first_print('indestructible aura', 'LEG').
card_image_name('indestructible aura'/'LEG', 'indestructible aura').
card_uid('indestructible aura'/'LEG', 'LEG:Indestructible Aura:indestructible aura').
card_rarity('indestructible aura'/'LEG', 'Common').
card_artist('indestructible aura'/'LEG', 'Mark Poole').
card_flavor_text('indestructible aura'/'LEG', 'Theodar strode the battle lines, snatching swords with his bare hands and casting them aside until all cowered before him.').
card_multiverse_id('indestructible aura'/'LEG', '1619').

card_in_set('infernal medusa', 'LEG').
card_original_type('infernal medusa'/'LEG', 'Summon — Medusa').
card_original_text('infernal medusa'/'LEG', 'All non-wall creatures blocking Medusa are destroyed at the end of combat, as are all creatures blocked by Medusa.').
card_first_print('infernal medusa', 'LEG').
card_image_name('infernal medusa'/'LEG', 'infernal medusa').
card_uid('infernal medusa'/'LEG', 'LEG:Infernal Medusa:infernal medusa').
card_rarity('infernal medusa'/'LEG', 'Uncommon').
card_artist('infernal medusa'/'LEG', 'Anson Maddocks').
card_multiverse_id('infernal medusa'/'LEG', '1448').

card_in_set('infinite authority', 'LEG').
card_original_type('infinite authority'/'LEG', 'Enchant Creature').
card_original_text('infinite authority'/'LEG', 'All creatures with toughness 3 or less blocking target creature or blocked by target creature are destroyed at end of combat. At the end of the turn put a +1/+1 counter on the target creature for each creature destroyed in this manner during the turn. Counters remain on creature even if enchantment leaves play.').
card_first_print('infinite authority', 'LEG').
card_image_name('infinite authority'/'LEG', 'infinite authority').
card_uid('infinite authority'/'LEG', 'LEG:Infinite Authority:infinite authority').
card_rarity('infinite authority'/'LEG', 'Rare').
card_artist('infinite authority'/'LEG', 'Douglas Shuler').
card_multiverse_id('infinite authority'/'LEG', '1620').

card_in_set('invoke prejudice', 'LEG').
card_original_type('invoke prejudice'/'LEG', 'Enchantment').
card_original_text('invoke prejudice'/'LEG', 'If opponent casts a Summon spell that does not match the color of one of the creatures under your control, that spell is countered unless the caster pays an additional {X} where X is the casting cost of the Summon spell.').
card_first_print('invoke prejudice', 'LEG').
card_image_name('invoke prejudice'/'LEG', 'invoke prejudice').
card_uid('invoke prejudice'/'LEG', 'LEG:Invoke Prejudice:invoke prejudice').
card_rarity('invoke prejudice'/'LEG', 'Rare').
card_artist('invoke prejudice'/'LEG', 'Harold McNeill').
card_multiverse_id('invoke prejudice'/'LEG', '1488').

card_in_set('ivory guardians', 'LEG').
card_original_type('ivory guardians'/'LEG', 'Summon — Guardians').
card_original_text('ivory guardians'/'LEG', 'Protection from red\nAll guardians gain +1/+1 if an opponent controls any red cards.').
card_first_print('ivory guardians', 'LEG').
card_image_name('ivory guardians'/'LEG', 'ivory guardians').
card_uid('ivory guardians'/'LEG', 'LEG:Ivory Guardians:ivory guardians').
card_rarity('ivory guardians'/'LEG', 'Uncommon').
card_artist('ivory guardians'/'LEG', 'Melissa A. Benson').
card_flavor_text('ivory guardians'/'LEG', 'The elite guard of the Mesa High Priests, the Ivory Guardians, were created to protect the innocent and faithful. Some say their actions are above the law.').
card_multiverse_id('ivory guardians'/'LEG', '1621').

card_in_set('jacques le vert', 'LEG').
card_original_type('jacques le vert'/'LEG', 'Summon — Legend').
card_original_text('jacques le vert'/'LEG', 'All your green creatures gain +0/+2.').
card_first_print('jacques le vert', 'LEG').
card_image_name('jacques le vert'/'LEG', 'jacques le vert').
card_uid('jacques le vert'/'LEG', 'LEG:Jacques le Vert:jacques le vert').
card_rarity('jacques le vert'/'LEG', 'Rare').
card_artist('jacques le vert'/'LEG', 'Andi Rusu').
card_flavor_text('jacques le vert'/'LEG', 'Abandoning his sword to return to the lush forest of Pendelhaven, Jacques le Vert devoted his life to protecting the creatures of his homeland.').
card_multiverse_id('jacques le vert'/'LEG', '1658').

card_in_set('jasmine boreal', 'LEG').
card_original_type('jasmine boreal'/'LEG', 'Summon — Legend').
card_original_text('jasmine boreal'/'LEG', '').
card_first_print('jasmine boreal', 'LEG').
card_image_name('jasmine boreal'/'LEG', 'jasmine boreal').
card_uid('jasmine boreal'/'LEG', 'LEG:Jasmine Boreal:jasmine boreal').
card_rarity('jasmine boreal'/'LEG', 'Uncommon').
card_artist('jasmine boreal'/'LEG', 'Richard Kane Ferguson').
card_flavor_text('jasmine boreal'/'LEG', '\"Peace must prevail, even if the wicked must die.\"').
card_multiverse_id('jasmine boreal'/'LEG', '1659').

card_in_set('jedit ojanen', 'LEG').
card_original_type('jedit ojanen'/'LEG', 'Summon — Legend').
card_original_text('jedit ojanen'/'LEG', '').
card_first_print('jedit ojanen', 'LEG').
card_image_name('jedit ojanen'/'LEG', 'jedit ojanen').
card_uid('jedit ojanen'/'LEG', 'LEG:Jedit Ojanen:jedit ojanen').
card_rarity('jedit ojanen'/'LEG', 'Uncommon').
card_artist('jedit ojanen'/'LEG', 'Mark Poole').
card_flavor_text('jedit ojanen'/'LEG', 'Mightiest of the Cat Warriors, Jedit Ojanen forsook the forests of his tribe and took service with the Robaran Mercenaries, rising through their ranks to lead them in the battle for Efrava.').
card_multiverse_id('jedit ojanen'/'LEG', '1660').

card_in_set('jerrard of the closed fist', 'LEG').
card_original_type('jerrard of the closed fist'/'LEG', 'Summon — Legend').
card_original_text('jerrard of the closed fist'/'LEG', '').
card_first_print('jerrard of the closed fist', 'LEG').
card_image_name('jerrard of the closed fist'/'LEG', 'jerrard of the closed fist').
card_uid('jerrard of the closed fist'/'LEG', 'LEG:Jerrard of the Closed Fist:jerrard of the closed fist').
card_rarity('jerrard of the closed fist'/'LEG', 'Uncommon').
card_artist('jerrard of the closed fist'/'LEG', 'Andi Rusu').
card_flavor_text('jerrard of the closed fist'/'LEG', 'Once, the order of the Closed Fist reached throughout the Kb\'Briann Highlands. Now, Jerrard alone remains to uphold their standard.').
card_multiverse_id('jerrard of the closed fist'/'LEG', '1661').

card_in_set('johan', 'LEG').
card_original_type('johan'/'LEG', 'Summon — Legend').
card_original_text('johan'/'LEG', 'If Johan does not attack and is not tapped, any of your creatures may attack without tapping.').
card_first_print('johan', 'LEG').
card_image_name('johan'/'LEG', 'johan').
card_uid('johan'/'LEG', 'LEG:Johan:johan').
card_rarity('johan'/'LEG', 'Rare').
card_artist('johan'/'LEG', 'Mark Tedin').
card_multiverse_id('johan'/'LEG', '1662').

card_in_set('jovial evil', 'LEG').
card_original_type('jovial evil'/'LEG', 'Sorcery').
card_original_text('jovial evil'/'LEG', 'Jovial Evil does 2 damage to opponent for each white creature he or she controls.').
card_first_print('jovial evil', 'LEG').
card_image_name('jovial evil'/'LEG', 'jovial evil').
card_uid('jovial evil'/'LEG', 'LEG:Jovial Evil:jovial evil').
card_rarity('jovial evil'/'LEG', 'Rare').
card_artist('jovial evil'/'LEG', 'Christopher Rush').
card_flavor_text('jovial evil'/'LEG', '\"Today, for a lark, let\'s visit the plains. I\'m sure we\'ll find something to entertain us.\"').
card_multiverse_id('jovial evil'/'LEG', '1449').

card_in_set('juxtapose', 'LEG').
card_original_type('juxtapose'/'LEG', 'Sorcery').
card_original_text('juxtapose'/'LEG', 'Target player and caster each choose one of the creatures they control with the highest casting cost. Exchange control of these creatures. Then do the same for artifacts. Juxtapose does not tap or untap these cards. The control of any enchantment cards played on these permanents is unchanged. If one player does not have an artifact or creature do not trade that type of card.').
card_first_print('juxtapose', 'LEG').
card_image_name('juxtapose'/'LEG', 'juxtapose').
card_uid('juxtapose'/'LEG', 'LEG:Juxtapose:juxtapose').
card_rarity('juxtapose'/'LEG', 'Rare').
card_artist('juxtapose'/'LEG', 'Justin Hampton').
card_multiverse_id('juxtapose'/'LEG', '1489').

card_in_set('karakas', 'LEG').
card_original_type('karakas'/'LEG', 'Legendary Land').
card_original_text('karakas'/'LEG', '{T}: Add {W} to your mana pool\n{T}: Return target legend to owner\'s hand; enchantments on target legend are destroyed.').
card_first_print('karakas', 'LEG').
card_image_name('karakas'/'LEG', 'karakas').
card_uid('karakas'/'LEG', 'LEG:Karakas:karakas').
card_rarity('karakas'/'LEG', 'Uncommon').
card_artist('karakas'/'LEG', 'Nicola Leonard').
card_flavor_text('karakas'/'LEG', '\"To make a prairie it takes a clover and one bee,/ One clover, and a bee,/ And revery.\" —Emily Dickinson').
card_multiverse_id('karakas'/'LEG', '1701').

card_in_set('kasimir the lone wolf', 'LEG').
card_original_type('kasimir the lone wolf'/'LEG', 'Summon — Legend').
card_original_text('kasimir the lone wolf'/'LEG', '').
card_first_print('kasimir the lone wolf', 'LEG').
card_image_name('kasimir the lone wolf'/'LEG', 'kasimir the lone wolf').
card_uid('kasimir the lone wolf'/'LEG', 'LEG:Kasimir the Lone Wolf:kasimir the lone wolf').
card_rarity('kasimir the lone wolf'/'LEG', 'Uncommon').
card_artist('kasimir the lone wolf'/'LEG', 'Richard Kane Ferguson').
card_flavor_text('kasimir the lone wolf'/'LEG', 'Popular indeed is the tale of how Kasimir was once a Holy man who renounced his order to take up the sword. But this tale is no more likely than any other.').
card_multiverse_id('kasimir the lone wolf'/'LEG', '1663').

card_in_set('keepers of the faith', 'LEG').
card_original_type('keepers of the faith'/'LEG', 'Summon — Keepers').
card_original_text('keepers of the faith'/'LEG', '').
card_first_print('keepers of the faith', 'LEG').
card_image_name('keepers of the faith'/'LEG', 'keepers of the faith').
card_uid('keepers of the faith'/'LEG', 'LEG:Keepers of the Faith:keepers of the faith').
card_rarity('keepers of the faith'/'LEG', 'Common').
card_artist('keepers of the faith'/'LEG', 'Daniel Gelon').
card_flavor_text('keepers of the faith'/'LEG', 'And then the Archangel Anthius spoke to them, saying, \"Fear shall be vanquished by the Sword of Faith.\"').
card_multiverse_id('keepers of the faith'/'LEG', '1622').

card_in_set('kei takahashi', 'LEG').
card_original_type('kei takahashi'/'LEG', 'Summon — Legend').
card_original_text('kei takahashi'/'LEG', '{T}: Prevent up to 2 damage to one creature.').
card_first_print('kei takahashi', 'LEG').
card_image_name('kei takahashi'/'LEG', 'kei takahashi').
card_uid('kei takahashi'/'LEG', 'LEG:Kei Takahashi:kei takahashi').
card_rarity('kei takahashi'/'LEG', 'Rare').
card_artist('kei takahashi'/'LEG', 'Scott Kirschner').
card_multiverse_id('kei takahashi'/'LEG', '1664').

card_in_set('killer bees', 'LEG').
card_original_type('killer bees'/'LEG', 'Summon — Bees').
card_original_text('killer bees'/'LEG', 'Flying\n{G} +1/+1 until end of turn.').
card_first_print('killer bees', 'LEG').
card_image_name('killer bees'/'LEG', 'killer bees').
card_uid('killer bees'/'LEG', 'LEG:Killer Bees:killer bees').
card_rarity('killer bees'/'LEG', 'Rare').
card_artist('killer bees'/'LEG', 'Phil Foglio').
card_flavor_text('killer bees'/'LEG', 'The communal mind produces a savage strategy, yet no one could predict that this vicious crossbreed would unravel the secret of steel.').
card_multiverse_id('killer bees'/'LEG', '1532').

card_in_set('kismet', 'LEG').
card_original_type('kismet'/'LEG', 'Enchantment').
card_original_text('kismet'/'LEG', 'All creatures, lands, and artifacts played by opponent come into play tapped.').
card_first_print('kismet', 'LEG').
card_image_name('kismet'/'LEG', 'kismet').
card_uid('kismet'/'LEG', 'LEG:Kismet:kismet').
card_rarity('kismet'/'LEG', 'Uncommon').
card_artist('kismet'/'LEG', 'Kaja Foglio').
card_multiverse_id('kismet'/'LEG', '1623').

card_in_set('knowledge vault', 'LEG').
card_original_type('knowledge vault'/'LEG', 'Artifact').
card_original_text('knowledge vault'/'LEG', '{2}{T}: Take a card from your library without looking at it and place it face down under Knowledge Vault. Sacrifice Knowledge Vault to discard entire hand and take the cards under the vault into your hand. If Knowledge Vault leaves play, put all cards under it in your graveyard.').
card_first_print('knowledge vault', 'LEG').
card_image_name('knowledge vault'/'LEG', 'knowledge vault').
card_uid('knowledge vault'/'LEG', 'LEG:Knowledge Vault:knowledge vault').
card_rarity('knowledge vault'/'LEG', 'Rare').
card_artist('knowledge vault'/'LEG', 'Amy Weber').
card_multiverse_id('knowledge vault'/'LEG', '1408').

card_in_set('kobold drill sergeant', 'LEG').
card_original_type('kobold drill sergeant'/'LEG', 'Summon — Drill Sergeant').
card_original_text('kobold drill sergeant'/'LEG', 'All your Kobolds gain +0/+1 and Trample.').
card_first_print('kobold drill sergeant', 'LEG').
card_image_name('kobold drill sergeant'/'LEG', 'kobold drill sergeant').
card_uid('kobold drill sergeant'/'LEG', 'LEG:Kobold Drill Sergeant:kobold drill sergeant').
card_rarity('kobold drill sergeant'/'LEG', 'Uncommon').
card_artist('kobold drill sergeant'/'LEG', 'Julie Baroh').
card_flavor_text('kobold drill sergeant'/'LEG', '\"Joining this army is easy, boy. Just survive your first battle.\"').
card_multiverse_id('kobold drill sergeant'/'LEG', '1580').

card_in_set('kobold overlord', 'LEG').
card_original_type('kobold overlord'/'LEG', 'Summon — Lord').
card_original_text('kobold overlord'/'LEG', 'First strike\nAll your Kobolds gain first strike.').
card_first_print('kobold overlord', 'LEG').
card_image_name('kobold overlord'/'LEG', 'kobold overlord').
card_uid('kobold overlord'/'LEG', 'LEG:Kobold Overlord:kobold overlord').
card_rarity('kobold overlord'/'LEG', 'Rare').
card_artist('kobold overlord'/'LEG', 'Julie Baroh').
card_flavor_text('kobold overlord'/'LEG', '\"One for all, all for one; we strike first, and then you\'re done!\" —Oath of the Kobold Musketeers').
card_multiverse_id('kobold overlord'/'LEG', '1581').

card_in_set('kobold taskmaster', 'LEG').
card_original_type('kobold taskmaster'/'LEG', 'Summon — Taskmaster').
card_original_text('kobold taskmaster'/'LEG', 'All your Kobolds gain +1/+0.').
card_first_print('kobold taskmaster', 'LEG').
card_image_name('kobold taskmaster'/'LEG', 'kobold taskmaster').
card_uid('kobold taskmaster'/'LEG', 'LEG:Kobold Taskmaster:kobold taskmaster').
card_rarity('kobold taskmaster'/'LEG', 'Uncommon').
card_artist('kobold taskmaster'/'LEG', 'Randy Asplund-Faith').
card_flavor_text('kobold taskmaster'/'LEG', 'The Taskmaster knows that there is no cure for the common Kobold.').
card_multiverse_id('kobold taskmaster'/'LEG', '1582').

card_in_set('kobolds of kher keep', 'LEG').
card_original_type('kobolds of kher keep'/'LEG', 'Summon — Kobolds').
card_original_text('kobolds of kher keep'/'LEG', 'This card is a red spell when cast and Kobolds are a red creature.').
card_first_print('kobolds of kher keep', 'LEG').
card_image_name('kobolds of kher keep'/'LEG', 'kobolds of kher keep').
card_uid('kobolds of kher keep'/'LEG', 'LEG:Kobolds of Kher Keep:kobolds of kher keep').
card_rarity('kobolds of kher keep'/'LEG', 'Common').
card_artist('kobolds of kher keep'/'LEG', 'Julie Baroh').
card_flavor_text('kobolds of kher keep'/'LEG', 'Kher Keep is unique among fortresses: impervious to aerial assault but defenseless from the ground.').
card_multiverse_id('kobolds of kher keep'/'LEG', '1583').

card_in_set('kry shield', 'LEG').
card_original_type('kry shield'/'LEG', 'Artifact').
card_original_text('kry shield'/'LEG', '{2}{T}: Target creature you control deals no damage this turn, but gains +0/+X until end of turn, where X is the casting cost of target creature.').
card_first_print('kry shield', 'LEG').
card_image_name('kry shield'/'LEG', 'kry shield').
card_uid('kry shield'/'LEG', 'LEG:Kry Shield:kry shield').
card_rarity('kry shield'/'LEG', 'Uncommon').
card_artist('kry shield'/'LEG', 'Richard Thomas').
card_multiverse_id('kry shield'/'LEG', '1409').

card_in_set('lady caleria', 'LEG').
card_original_type('lady caleria'/'LEG', 'Summon — Legend').
card_original_text('lady caleria'/'LEG', '{T}: Lady Caleria does 3 damage to target attacking or blocking creature.').
card_first_print('lady caleria', 'LEG').
card_image_name('lady caleria'/'LEG', 'lady caleria').
card_uid('lady caleria'/'LEG', 'LEG:Lady Caleria:lady caleria').
card_rarity('lady caleria'/'LEG', 'Rare').
card_artist('lady caleria'/'LEG', 'Bryon Wackwitz').
card_multiverse_id('lady caleria'/'LEG', '1665').

card_in_set('lady evangela', 'LEG').
card_original_type('lady evangela'/'LEG', 'Summon — Legend').
card_original_text('lady evangela'/'LEG', '{B}{W}{T}: Target creature does no damage during combat this turn.').
card_first_print('lady evangela', 'LEG').
card_image_name('lady evangela'/'LEG', 'lady evangela').
card_uid('lady evangela'/'LEG', 'LEG:Lady Evangela:lady evangela').
card_rarity('lady evangela'/'LEG', 'Rare').
card_artist('lady evangela'/'LEG', 'Mark Poole').
card_flavor_text('lady evangela'/'LEG', '\"When milady was young, the sight of a rainbow would fill her soul with peace. As she grew, she learned to share her rapture with others.\" —Lady Gabriella').
card_multiverse_id('lady evangela'/'LEG', '1666').

card_in_set('lady orca', 'LEG').
card_original_type('lady orca'/'LEG', 'Summon — Legend').
card_original_text('lady orca'/'LEG', '').
card_first_print('lady orca', 'LEG').
card_image_name('lady orca'/'LEG', 'lady orca').
card_uid('lady orca'/'LEG', 'LEG:Lady Orca:lady orca').
card_rarity('lady orca'/'LEG', 'Uncommon').
card_artist('lady orca'/'LEG', 'Sandra Everingham').
card_flavor_text('lady orca'/'LEG', '\"I do not remember what he said to her. I remember her fiery eyes, fixed upon him for an instant. I remember a flash, and the hot breath of sudden flames made me turn away. When I looked again, Angus was gone.\" —A Wayfarer, on meeting Lady Orca').
card_multiverse_id('lady orca'/'LEG', '1667').

card_in_set('land equilibrium', 'LEG').
card_original_type('land equilibrium'/'LEG', 'Enchantment').
card_original_text('land equilibrium'/'LEG', 'If your opponent controls at least as much land as you do, he or she must sacrifice a land for each land he or she puts into play.').
card_first_print('land equilibrium', 'LEG').
card_image_name('land equilibrium'/'LEG', 'land equilibrium').
card_uid('land equilibrium'/'LEG', 'LEG:Land Equilibrium:land equilibrium').
card_rarity('land equilibrium'/'LEG', 'Rare').
card_artist('land equilibrium'/'LEG', 'Jesper Myrfors').
card_multiverse_id('land equilibrium'/'LEG', '1490').

card_in_set('land tax', 'LEG').
card_original_type('land tax'/'LEG', 'Enchantment').
card_original_text('land tax'/'LEG', 'During your upkeep, if an opponent controls more land than you, you may search your library and remove up to three basic land cards and put them into your hand. Reshuffle your library afterwards.').
card_first_print('land tax', 'LEG').
card_image_name('land tax'/'LEG', 'land tax').
card_uid('land tax'/'LEG', 'LEG:Land Tax:land tax').
card_rarity('land tax'/'LEG', 'Uncommon').
card_artist('land tax'/'LEG', 'Brian Snõddy').
card_multiverse_id('land tax'/'LEG', '1624').

card_in_set('land\'s edge', 'LEG').
card_original_type('land\'s edge'/'LEG', 'Enchant World').
card_original_text('land\'s edge'/'LEG', 'Any player may discard a card from hand at any time. If that player discards a land, Land\'s Edge does 2 damage to target player of the discarding player\'s choice.').
card_first_print('land\'s edge', 'LEG').
card_image_name('land\'s edge'/'LEG', 'land\'s edge').
card_uid('land\'s edge'/'LEG', 'LEG:Land\'s Edge:land\'s edge').
card_rarity('land\'s edge'/'LEG', 'Rare').
card_artist('land\'s edge'/'LEG', 'Brian Snõddy').
card_multiverse_id('land\'s edge'/'LEG', '1584').

card_in_set('lesser werewolf', 'LEG').
card_original_type('lesser werewolf'/'LEG', 'Summon — Lycanthrope').
card_original_text('lesser werewolf'/'LEG', '{B} Lesser Werewolf gets -1/-0 until end of turn. Put a -0/-1 counter on target creature that blocks or is blocked by the Werewolf. Use this ability after defense is chosen but before damage is dealt. You may not use this ability to reduce the Lesser Werewolf\'s power below 0.').
card_first_print('lesser werewolf', 'LEG').
card_image_name('lesser werewolf'/'LEG', 'lesser werewolf').
card_uid('lesser werewolf'/'LEG', 'LEG:Lesser Werewolf:lesser werewolf').
card_rarity('lesser werewolf'/'LEG', 'Uncommon').
card_artist('lesser werewolf'/'LEG', 'Quinton Hoover').
card_multiverse_id('lesser werewolf'/'LEG', '1450').

card_in_set('life chisel', 'LEG').
card_original_type('life chisel'/'LEG', 'Artifact').
card_original_text('life chisel'/'LEG', 'Sacrifice a creature during your upkeep to gain life equal to creature\'s toughness.').
card_first_print('life chisel', 'LEG').
card_image_name('life chisel'/'LEG', 'life chisel').
card_uid('life chisel'/'LEG', 'LEG:Life Chisel:life chisel').
card_rarity('life chisel'/'LEG', 'Uncommon').
card_artist('life chisel'/'LEG', 'Anthony Waters').
card_multiverse_id('life chisel'/'LEG', '1410').

card_in_set('life matrix', 'LEG').
card_original_type('life matrix'/'LEG', 'Artifact').
card_original_text('life matrix'/'LEG', '{4}{T}: During your upkeep, put one counter on target creature. You may remove this counter at any time to regenerate that creature.').
card_first_print('life matrix', 'LEG').
card_image_name('life matrix'/'LEG', 'life matrix').
card_uid('life matrix'/'LEG', 'LEG:Life Matrix:life matrix').
card_rarity('life matrix'/'LEG', 'Rare').
card_artist('life matrix'/'LEG', 'Amy Weber').
card_multiverse_id('life matrix'/'LEG', '1411').

card_in_set('lifeblood', 'LEG').
card_original_type('lifeblood'/'LEG', 'Enchantment').
card_original_text('lifeblood'/'LEG', 'You gain 1 life point each time one of opponent\'s mountains becomes tapped.').
card_first_print('lifeblood', 'LEG').
card_image_name('lifeblood'/'LEG', 'lifeblood').
card_uid('lifeblood'/'LEG', 'LEG:Lifeblood:lifeblood').
card_rarity('lifeblood'/'LEG', 'Rare').
card_artist('lifeblood'/'LEG', 'Mark Tedin').
card_flavor_text('lifeblood'/'LEG', '\"Foolish wizard! As you tap the power of your lofty keep, so grows my strength.\" —Malvern Xelionos, Letters').
card_multiverse_id('lifeblood'/'LEG', '1625').

card_in_set('living plane', 'LEG').
card_original_type('living plane'/'LEG', 'Enchant World').
card_original_text('living plane'/'LEG', 'Treat all land in play as both lands and 1/1 creatures. They may not be tapped for mana the first turn they are brought into play.').
card_first_print('living plane', 'LEG').
card_image_name('living plane'/'LEG', 'living plane').
card_uid('living plane'/'LEG', 'LEG:Living Plane:living plane').
card_rarity('living plane'/'LEG', 'Rare').
card_artist('living plane'/'LEG', 'Bryon Wackwitz').
card_multiverse_id('living plane'/'LEG', '1533').

card_in_set('livonya silone', 'LEG').
card_original_type('livonya silone'/'LEG', 'Summon — Legend').
card_original_text('livonya silone'/'LEG', 'First strike, Legendary Land-walk.').
card_first_print('livonya silone', 'LEG').
card_image_name('livonya silone'/'LEG', 'livonya silone').
card_uid('livonya silone'/'LEG', 'LEG:Livonya Silone:livonya silone').
card_rarity('livonya silone'/'LEG', 'Rare').
card_artist('livonya silone'/'LEG', 'Richard Kane Ferguson').
card_flavor_text('livonya silone'/'LEG', 'Livonya\'s own nature is a matter of mystery. Nothing is known for sure, merely rumors of unearthly stealth, and unholy alliances.').
card_multiverse_id('livonya silone'/'LEG', '1668').

card_in_set('lord magnus', 'LEG').
card_original_type('lord magnus'/'LEG', 'Summon — Legend').
card_original_text('lord magnus'/'LEG', 'First strike\nCreatures with plainswalk or forestwalk may be blocked as if they did not have either ability.').
card_first_print('lord magnus', 'LEG').
card_image_name('lord magnus'/'LEG', 'lord magnus').
card_uid('lord magnus'/'LEG', 'LEG:Lord Magnus:lord magnus').
card_rarity('lord magnus'/'LEG', 'Uncommon').
card_artist('lord magnus'/'LEG', 'Mark Tedin').
card_multiverse_id('lord magnus'/'LEG', '1669').

card_in_set('lost soul', 'LEG').
card_original_type('lost soul'/'LEG', 'Summon — Lost Soul').
card_original_text('lost soul'/'LEG', 'Swampwalk').
card_first_print('lost soul', 'LEG').
card_image_name('lost soul'/'LEG', 'lost soul').
card_uid('lost soul'/'LEG', 'LEG:Lost Soul:lost soul').
card_rarity('lost soul'/'LEG', 'Common').
card_artist('lost soul'/'LEG', 'Randy Asplund-Faith').
card_flavor_text('lost soul'/'LEG', 'She walks in the twilight, her steps make no sound,/ Her feet leave no tracks on the dew-covered ground./ Her hand gently beckons, she whispers your name—/ But those who go with her are never the same.').
card_multiverse_id('lost soul'/'LEG', '1451').

card_in_set('mana drain', 'LEG').
card_original_type('mana drain'/'LEG', 'Interrupt').
card_original_text('mana drain'/'LEG', 'Counters target spell. At the beginning of your next main phase, add {X} to your mana pool, where X is the casting cost of target spell.').
card_first_print('mana drain', 'LEG').
card_image_name('mana drain'/'LEG', 'mana drain').
card_uid('mana drain'/'LEG', 'LEG:Mana Drain:mana drain').
card_rarity('mana drain'/'LEG', 'Uncommon').
card_artist('mana drain'/'LEG', 'Mark Tedin').
card_multiverse_id('mana drain'/'LEG', '1491').

card_in_set('mana matrix', 'LEG').
card_original_type('mana matrix'/'LEG', 'Artifact').
card_original_text('mana matrix'/'LEG', 'Pay up to {2} less than required whenever casting an instant, interrupt, or enchantment spell.').
card_first_print('mana matrix', 'LEG').
card_image_name('mana matrix'/'LEG', 'mana matrix').
card_uid('mana matrix'/'LEG', 'LEG:Mana Matrix:mana matrix').
card_rarity('mana matrix'/'LEG', 'Rare').
card_artist('mana matrix'/'LEG', 'Mark Tedin').
card_multiverse_id('mana matrix'/'LEG', '1412').

card_in_set('marble priest', 'LEG').
card_original_type('marble priest'/'LEG', 'Artifact Creature').
card_original_text('marble priest'/'LEG', 'All walls able to block Marble Priest must do so. Walls able to block more than one creature can still do so. If blocking wall is compelled to block more creatures than it is legally able to, defender chooses which of these attacking creatures to block, but must block as many creatures as it legally can. Damage dealt to Marble Priest from walls during combat is reduced to 0.').
card_first_print('marble priest', 'LEG').
card_image_name('marble priest'/'LEG', 'marble priest').
card_uid('marble priest'/'LEG', 'LEG:Marble Priest:marble priest').
card_rarity('marble priest'/'LEG', 'Uncommon').
card_artist('marble priest'/'LEG', 'Melissa A. Benson').
card_multiverse_id('marble priest'/'LEG', '1413').

card_in_set('marhault elsdragon', 'LEG').
card_original_type('marhault elsdragon'/'LEG', 'Summon — Legend').
card_original_text('marhault elsdragon'/'LEG', 'Rampage: 1').
card_first_print('marhault elsdragon', 'LEG').
card_image_name('marhault elsdragon'/'LEG', 'marhault elsdragon').
card_uid('marhault elsdragon'/'LEG', 'LEG:Marhault Elsdragon:marhault elsdragon').
card_rarity('marhault elsdragon'/'LEG', 'Uncommon').
card_artist('marhault elsdragon'/'LEG', 'Mark Poole').
card_flavor_text('marhault elsdragon'/'LEG', 'Marhault follows a strict philosophy, never letting emotions cloud his thoughts. No chance observer could imagine the rage in his heart.').
card_multiverse_id('marhault elsdragon'/'LEG', '1670').

card_in_set('master of the hunt', 'LEG').
card_original_type('master of the hunt'/'LEG', 'Summon — Master').
card_original_text('master of the hunt'/'LEG', '{2}{G}{G} Put a Wolves of the Hunt token into play. Treat this token as a 1/1 green creature with the ability bands with other Wolves of the Hunt.').
card_first_print('master of the hunt', 'LEG').
card_image_name('master of the hunt'/'LEG', 'master of the hunt').
card_uid('master of the hunt'/'LEG', 'LEG:Master of the Hunt:master of the hunt').
card_rarity('master of the hunt'/'LEG', 'Rare').
card_artist('master of the hunt'/'LEG', 'Jeff A. Menges').
card_multiverse_id('master of the hunt'/'LEG', '1534').

card_in_set('mirror universe', 'LEG').
card_original_type('mirror universe'/'LEG', 'Artifact').
card_original_text('mirror universe'/'LEG', '{T}: Sacrifice Mirror Universe during your upkeep, and trade your number of live points with opponent. For example, if you had 2 life points and your opponent had 10, you would now have 10 life points and your opponent would have 2. Effects that prevent or redirect damage may not be used to counter this change of life.').
card_first_print('mirror universe', 'LEG').
card_image_name('mirror universe'/'LEG', 'mirror universe').
card_uid('mirror universe'/'LEG', 'LEG:Mirror Universe:mirror universe').
card_rarity('mirror universe'/'LEG', 'Rare').
card_artist('mirror universe'/'LEG', 'Phil Foglio').
card_multiverse_id('mirror universe'/'LEG', '1414').

card_in_set('moat', 'LEG').
card_original_type('moat'/'LEG', 'Enchantment').
card_original_text('moat'/'LEG', 'Non-flying creatures cannot attack.').
card_first_print('moat', 'LEG').
card_image_name('moat'/'LEG', 'moat').
card_uid('moat'/'LEG', 'LEG:Moat:moat').
card_rarity('moat'/'LEG', 'Rare').
card_artist('moat'/'LEG', 'Jeff A. Menges').
card_flavor_text('moat'/'LEG', 'The purpose of any moat is to impede attack. Some are filled with water, some with thistles. Some are filled with things best left unseen.').
card_multiverse_id('moat'/'LEG', '1626').

card_in_set('mold demon', 'LEG').
card_original_type('mold demon'/'LEG', 'Summon — Mold Demon').
card_original_text('mold demon'/'LEG', 'When Mold Demon is brought into play, controller must sacrifice two swamps or Mold Demon is buried.').
card_first_print('mold demon', 'LEG').
card_image_name('mold demon'/'LEG', 'mold demon').
card_uid('mold demon'/'LEG', 'LEG:Mold Demon:mold demon').
card_rarity('mold demon'/'LEG', 'Rare').
card_artist('mold demon'/'LEG', 'Jesper Myrfors').
card_multiverse_id('mold demon'/'LEG', '1452').

card_in_set('moss monster', 'LEG').
card_original_type('moss monster'/'LEG', 'Summon — Monster').
card_original_text('moss monster'/'LEG', '').
card_first_print('moss monster', 'LEG').
card_image_name('moss monster'/'LEG', 'moss monster').
card_uid('moss monster'/'LEG', 'LEG:Moss Monster:moss monster').
card_rarity('moss monster'/'LEG', 'Common').
card_artist('moss monster'/'LEG', 'Jesper Myrfors').
card_flavor_text('moss monster'/'LEG', 'After the battle, an eerie silence gripped the forest. The losers\' remains were lightly dusted with green.').
card_multiverse_id('moss monster'/'LEG', '1535').

card_in_set('mountain stronghold', 'LEG').
card_original_type('mountain stronghold'/'LEG', 'Land').
card_original_text('mountain stronghold'/'LEG', 'All your red legends may band with other legends.').
card_first_print('mountain stronghold', 'LEG').
card_image_name('mountain stronghold'/'LEG', 'mountain stronghold').
card_uid('mountain stronghold'/'LEG', 'LEG:Mountain Stronghold:mountain stronghold').
card_rarity('mountain stronghold'/'LEG', 'Uncommon').
card_artist('mountain stronghold'/'LEG', 'Tom Wänerstrand').
card_multiverse_id('mountain stronghold'/'LEG', '1702').

card_in_set('mountain yeti', 'LEG').
card_original_type('mountain yeti'/'LEG', 'Summon — Yeti').
card_original_text('mountain yeti'/'LEG', 'Mountainwalk, protection from white.').
card_first_print('mountain yeti', 'LEG').
card_image_name('mountain yeti'/'LEG', 'mountain yeti').
card_uid('mountain yeti'/'LEG', 'LEG:Mountain Yeti:mountain yeti').
card_rarity('mountain yeti'/'LEG', 'Uncommon').
card_artist('mountain yeti'/'LEG', 'Dan Frazier').
card_flavor_text('mountain yeti'/'LEG', 'The Yeti\'s single greatest asset is its unnerving ability to blend in with its surroundings.').
card_multiverse_id('mountain yeti'/'LEG', '1585').

card_in_set('nebuchadnezzar', 'LEG').
card_original_type('nebuchadnezzar'/'LEG', 'Summon — Legend').
card_original_text('nebuchadnezzar'/'LEG', '{X}{T}: Name a card. Opponent reveals X cards from his or her hand at random, or entire hand if he or she does not have enough cards. Opponent then discards any of those cards that match the one you named. May only use this power during your turn.').
card_first_print('nebuchadnezzar', 'LEG').
card_image_name('nebuchadnezzar'/'LEG', 'nebuchadnezzar').
card_uid('nebuchadnezzar'/'LEG', 'LEG:Nebuchadnezzar:nebuchadnezzar').
card_rarity('nebuchadnezzar'/'LEG', 'Rare').
card_artist('nebuchadnezzar'/'LEG', 'Richard Kane Ferguson').
card_multiverse_id('nebuchadnezzar'/'LEG', '1671').

card_in_set('nether void', 'LEG').
card_original_type('nether void'/'LEG', 'Enchant World').
card_original_text('nether void'/'LEG', 'All spells cast are countered unless their casters pay an additional {3}.').
card_first_print('nether void', 'LEG').
card_image_name('nether void'/'LEG', 'nether void').
card_uid('nether void'/'LEG', 'LEG:Nether Void:nether void').
card_rarity('nether void'/'LEG', 'Rare').
card_artist('nether void'/'LEG', 'Harold McNeill').
card_flavor_text('nether void'/'LEG', 'These days, some wizards are finding that they have a little too much spell left at the end of their mana.').
card_multiverse_id('nether void'/'LEG', '1453').

card_in_set('nicol bolas', 'LEG').
card_original_type('nicol bolas'/'LEG', 'Summon — Elder Dragon Legend').
card_original_text('nicol bolas'/'LEG', 'Flying\nAn opponent damaged by Nicol Bolas must discard entire hand. Ignore this effect if opponent has no cards left in hand. Pay {U}{B}{R} during your upkeep or Nicol Bolas is buried.').
card_first_print('nicol bolas', 'LEG').
card_image_name('nicol bolas'/'LEG', 'nicol bolas').
card_uid('nicol bolas'/'LEG', 'LEG:Nicol Bolas:nicol bolas').
card_rarity('nicol bolas'/'LEG', 'Rare').
card_artist('nicol bolas'/'LEG', 'Edward P. Beard, Jr.').
card_multiverse_id('nicol bolas'/'LEG', '1672').

card_in_set('north star', 'LEG').
card_original_type('north star'/'LEG', 'Artifact').
card_original_text('north star'/'LEG', '{4}{T}: You may cast one spell this turn by paying its casting cost with any type of mana. For example, {2}{G}{G} becomes {4}. However, the card still retains its original color. This ability is played as an interrupt.').
card_first_print('north star', 'LEG').
card_image_name('north star'/'LEG', 'north star').
card_uid('north star'/'LEG', 'LEG:North Star:north star').
card_rarity('north star'/'LEG', 'Rare').
card_artist('north star'/'LEG', 'Kaja Foglio').
card_multiverse_id('north star'/'LEG', '1415').

card_in_set('nova pentacle', 'LEG').
card_original_type('nova pentacle'/'LEG', 'Artifact').
card_original_text('nova pentacle'/'LEG', '{3}{T}: Redirect damage done to you from one source to target creature of opponent\'s choice.').
card_first_print('nova pentacle', 'LEG').
card_image_name('nova pentacle'/'LEG', 'nova pentacle').
card_uid('nova pentacle'/'LEG', 'LEG:Nova Pentacle:nova pentacle').
card_rarity('nova pentacle'/'LEG', 'Rare').
card_artist('nova pentacle'/'LEG', 'Richard Thomas').
card_multiverse_id('nova pentacle'/'LEG', '1416').

card_in_set('osai vultures', 'LEG').
card_original_type('osai vultures'/'LEG', 'Summon — Vultures').
card_original_text('osai vultures'/'LEG', 'Flying\nAt the end of any turn in which a creature is placed in the graveyard from play, put a counter on the Vultures. Remove two counters to give Vultures +1/+1 until end of turn.').
card_first_print('osai vultures', 'LEG').
card_image_name('osai vultures'/'LEG', 'osai vultures').
card_uid('osai vultures'/'LEG', 'LEG:Osai Vultures:osai vultures').
card_rarity('osai vultures'/'LEG', 'Common').
card_artist('osai vultures'/'LEG', 'Dan Frazier').
card_flavor_text('osai vultures'/'LEG', 'A sign of battle, the Vultures circle and wait for the victorious to depart.').
card_multiverse_id('osai vultures'/'LEG', '1627').

card_in_set('palladia-mors', 'LEG').
card_original_type('palladia-mors'/'LEG', 'Summon — Elder Dragon Legend').
card_original_text('palladia-mors'/'LEG', 'Flying, Trample\nPay {W}{G}{R} during your upkeep or Palladia-Mors is buried.').
card_first_print('palladia-mors', 'LEG').
card_image_name('palladia-mors'/'LEG', 'palladia-mors').
card_uid('palladia-mors'/'LEG', 'LEG:Palladia-Mors:palladia-mors').
card_rarity('palladia-mors'/'LEG', 'Rare').
card_artist('palladia-mors'/'LEG', 'Edward P. Beard, Jr.').
card_multiverse_id('palladia-mors'/'LEG', '1673').

card_in_set('part water', 'LEG').
card_original_type('part water'/'LEG', 'Sorcery').
card_original_text('part water'/'LEG', 'X target creatures gain islandwalk until end of turn.').
card_first_print('part water', 'LEG').
card_image_name('part water'/'LEG', 'part water').
card_uid('part water'/'LEG', 'LEG:Part Water:part water').
card_rarity('part water'/'LEG', 'Uncommon').
card_artist('part water'/'LEG', 'NéNé Thomas').
card_flavor_text('part water'/'LEG', '\". . . and the waters were a wall unto them on their right hand, and on their left.\" —Exodus, 14:22').
card_multiverse_id('part water'/'LEG', '1492').

card_in_set('pavel maliki', 'LEG').
card_original_type('pavel maliki'/'LEG', 'Summon — Legend').
card_original_text('pavel maliki'/'LEG', '{B}{R} +1/+0 until end of turn.').
card_first_print('pavel maliki', 'LEG').
card_image_name('pavel maliki'/'LEG', 'pavel maliki').
card_uid('pavel maliki'/'LEG', 'LEG:Pavel Maliki:pavel maliki').
card_rarity('pavel maliki'/'LEG', 'Uncommon').
card_artist('pavel maliki'/'LEG', 'Andi Rusu').
card_flavor_text('pavel maliki'/'LEG', 'We all know the legend: Pavel wanders the realms, helping those in greatest need. But is this a measure of his generosity, or of his obligation to atone?').
card_multiverse_id('pavel maliki'/'LEG', '1674').

card_in_set('pendelhaven', 'LEG').
card_original_type('pendelhaven'/'LEG', 'Legendary Land').
card_original_text('pendelhaven'/'LEG', '{T}: Add {G} to your mana pool\n{T}: Target 1/1 creature gains +1/+2 until end of turn.').
card_first_print('pendelhaven', 'LEG').
card_image_name('pendelhaven'/'LEG', 'pendelhaven').
card_uid('pendelhaven'/'LEG', 'LEG:Pendelhaven:pendelhaven').
card_rarity('pendelhaven'/'LEG', 'Uncommon').
card_artist('pendelhaven'/'LEG', 'Bryon Wackwitz').
card_flavor_text('pendelhaven'/'LEG', '\"This is the forest primeval. The murmuring pines and the hemlocks . . . / Stand like Druids of old.\" —Henry Wadsworth Longfellow, \"Evangeline\"').
card_multiverse_id('pendelhaven'/'LEG', '1703').

card_in_set('petra sphinx', 'LEG').
card_original_type('petra sphinx'/'LEG', 'Summon — Sphinx').
card_original_text('petra sphinx'/'LEG', '{T}: Target player names a card and then turns over the top card of his or her library. If it matches the named card, the card is put in the player\'s hand; otherwise it is put into the graveyard.').
card_first_print('petra sphinx', 'LEG').
card_image_name('petra sphinx'/'LEG', 'petra sphinx').
card_uid('petra sphinx'/'LEG', 'LEG:Petra Sphinx:petra sphinx').
card_rarity('petra sphinx'/'LEG', 'Rare').
card_artist('petra sphinx'/'LEG', 'Sandra Everingham').
card_flavor_text('petra sphinx'/'LEG', 'What walks on four legs in the morning, two legs in the afternoon, and three legs in the evening?').
card_multiverse_id('petra sphinx'/'LEG', '1628').

card_in_set('pit scorpion', 'LEG').
card_original_type('pit scorpion'/'LEG', 'Summon — Scorpion').
card_original_text('pit scorpion'/'LEG', 'If Scorpion damages opponent, opponent gets a poison counter. If opponent ever has ten or more poison counters, opponent loses game.').
card_first_print('pit scorpion', 'LEG').
card_image_name('pit scorpion'/'LEG', 'pit scorpion').
card_uid('pit scorpion'/'LEG', 'LEG:Pit Scorpion:pit scorpion').
card_rarity('pit scorpion'/'LEG', 'Common').
card_artist('pit scorpion'/'LEG', 'Scott Kirschner').
card_flavor_text('pit scorpion'/'LEG', 'Sometimes the smallest nuisance can be the greatest pain.').
card_multiverse_id('pit scorpion'/'LEG', '1454').

card_in_set('pixie queen', 'LEG').
card_original_type('pixie queen'/'LEG', 'Summon — Pixie Queen').
card_original_text('pixie queen'/'LEG', 'Flying\n{G}{G}{G}{T}: Target creature gains flying until end of turn.').
card_first_print('pixie queen', 'LEG').
card_image_name('pixie queen'/'LEG', 'pixie queen').
card_uid('pixie queen'/'LEG', 'LEG:Pixie Queen:pixie queen').
card_rarity('pixie queen'/'LEG', 'Rare').
card_artist('pixie queen'/'LEG', 'Quinton Hoover').
card_multiverse_id('pixie queen'/'LEG', '1536').

card_in_set('planar gate', 'LEG').
card_original_type('planar gate'/'LEG', 'Artifact').
card_original_text('planar gate'/'LEG', 'Pay up to {2} less than required whenever casting a summon spell.').
card_first_print('planar gate', 'LEG').
card_image_name('planar gate'/'LEG', 'planar gate').
card_uid('planar gate'/'LEG', 'LEG:Planar Gate:planar gate').
card_rarity('planar gate'/'LEG', 'Rare').
card_artist('planar gate'/'LEG', 'Melissa A. Benson').
card_flavor_text('planar gate'/'LEG', 'Nireya reached through the Gate, sensing the energies trapped beyond.').
card_multiverse_id('planar gate'/'LEG', '1417').

card_in_set('pradesh gypsies', 'LEG').
card_original_type('pradesh gypsies'/'LEG', 'Summon — Gypsies').
card_original_text('pradesh gypsies'/'LEG', '{1}{G}{T}: Target creature gets -2/-0 until end of turn.').
card_first_print('pradesh gypsies', 'LEG').
card_image_name('pradesh gypsies'/'LEG', 'pradesh gypsies').
card_uid('pradesh gypsies'/'LEG', 'LEG:Pradesh Gypsies:pradesh gypsies').
card_rarity('pradesh gypsies'/'LEG', 'Uncommon').
card_artist('pradesh gypsies'/'LEG', 'Quinton Hoover').
card_multiverse_id('pradesh gypsies'/'LEG', '1537').

card_in_set('presence of the master', 'LEG').
card_original_type('presence of the master'/'LEG', 'Enchantment').
card_original_text('presence of the master'/'LEG', 'While Presence of the Master is in play, any new enchantments cast are countered.').
card_first_print('presence of the master', 'LEG').
card_image_name('presence of the master'/'LEG', 'presence of the master').
card_uid('presence of the master'/'LEG', 'LEG:Presence of the Master:presence of the master').
card_rarity('presence of the master'/'LEG', 'Uncommon').
card_artist('presence of the master'/'LEG', 'Phil Foglio').
card_multiverse_id('presence of the master'/'LEG', '1629').

card_in_set('primordial ooze', 'LEG').
card_original_type('primordial ooze'/'LEG', 'Summon — Ooze').
card_original_text('primordial ooze'/'LEG', 'Must attack each turn if possible. Gains +1/+1 at end of your upkeep. Use counters. Then pay {1} per counter or Ooze deals 1 damage to you for each counter and becomes tapped.').
card_first_print('primordial ooze', 'LEG').
card_image_name('primordial ooze'/'LEG', 'primordial ooze').
card_uid('primordial ooze'/'LEG', 'LEG:Primordial Ooze:primordial ooze').
card_rarity('primordial ooze'/'LEG', 'Uncommon').
card_artist('primordial ooze'/'LEG', 'Sandra Everingham').
card_flavor_text('primordial ooze'/'LEG', 'The thick, moving mass from the beginning of evolution swallows and digests the animate and inanimate as it unthinkingly strives to continue its existence.').
card_multiverse_id('primordial ooze'/'LEG', '1586').

card_in_set('princess lucrezia', 'LEG').
card_original_type('princess lucrezia'/'LEG', 'Summon — Legend').
card_original_text('princess lucrezia'/'LEG', '{T}: Add {U} to your mana pool.\nThis ability is played as an interrupt.').
card_first_print('princess lucrezia', 'LEG').
card_image_name('princess lucrezia'/'LEG', 'princess lucrezia').
card_uid('princess lucrezia'/'LEG', 'LEG:Princess Lucrezia:princess lucrezia').
card_rarity('princess lucrezia'/'LEG', 'Uncommon').
card_artist('princess lucrezia'/'LEG', 'Edward P. Beard, Jr.').
card_multiverse_id('princess lucrezia'/'LEG', '1675').

card_in_set('psionic entity', 'LEG').
card_original_type('psionic entity'/'LEG', 'Summon — Entity').
card_original_text('psionic entity'/'LEG', '{T}: Psionic Entity does 2 damage to any target but does 3 damage to itself.').
card_first_print('psionic entity', 'LEG').
card_image_name('psionic entity'/'LEG', 'psionic entity').
card_uid('psionic entity'/'LEG', 'LEG:Psionic Entity:psionic entity').
card_rarity('psionic entity'/'LEG', 'Rare').
card_artist('psionic entity'/'LEG', 'Justin Hampton').
card_flavor_text('psionic entity'/'LEG', 'Creatures of the Æther are notorious for neglecting their own well-being.').
card_multiverse_id('psionic entity'/'LEG', '1493').

card_in_set('psychic purge', 'LEG').
card_original_type('psychic purge'/'LEG', 'Sorcery').
card_original_text('psychic purge'/'LEG', 'Psychic Purge does 1 damage to any target. If a spell cast by opponent or a permanent under opponent\'s control causes you to discard this card, opponent loses 5 life. Effects that prevent or redirect damage may not be used to counter this loss of life.').
card_first_print('psychic purge', 'LEG').
card_image_name('psychic purge'/'LEG', 'psychic purge').
card_uid('psychic purge'/'LEG', 'LEG:Psychic Purge:psychic purge').
card_rarity('psychic purge'/'LEG', 'Common').
card_artist('psychic purge'/'LEG', 'Susan Van Camp').
card_multiverse_id('psychic purge'/'LEG', '1494').

card_in_set('puppet master', 'LEG').
card_original_type('puppet master'/'LEG', 'Enchant Creature').
card_original_text('puppet master'/'LEG', 'If target creature is placed in the graveyard, return creature to owner\'s hand. All enchantments on target creature are destroyed. You may pay {U}{U}{U} to return Puppet Master to its owner\'s hand if target creature returns to its owner\'s hand.').
card_first_print('puppet master', 'LEG').
card_image_name('puppet master'/'LEG', 'puppet master').
card_uid('puppet master'/'LEG', 'LEG:Puppet Master:puppet master').
card_rarity('puppet master'/'LEG', 'Uncommon').
card_artist('puppet master'/'LEG', 'Sandra Everingham').
card_multiverse_id('puppet master'/'LEG', '1495').

card_in_set('pyrotechnics', 'LEG').
card_original_type('pyrotechnics'/'LEG', 'Sorcery').
card_original_text('pyrotechnics'/'LEG', 'Pyrotechnics does 4 damage divided any way you choose among any number of targets.').
card_first_print('pyrotechnics', 'LEG').
card_image_name('pyrotechnics'/'LEG', 'pyrotechnics').
card_uid('pyrotechnics'/'LEG', 'LEG:Pyrotechnics:pyrotechnics').
card_rarity('pyrotechnics'/'LEG', 'Common').
card_artist('pyrotechnics'/'LEG', 'Anson Maddocks').
card_flavor_text('pyrotechnics'/'LEG', '\"Hi! ni! ya! Behold the man of flint, that\'s me!/ Four lightnings zigzag from me, strike and return.\"\n—Navajo war chant').
card_multiverse_id('pyrotechnics'/'LEG', '1587').

card_in_set('quagmire', 'LEG').
card_original_type('quagmire'/'LEG', 'Enchantment').
card_original_text('quagmire'/'LEG', 'Creatures with swampwalk may be blocked as if they did not have this ability.').
card_first_print('quagmire', 'LEG').
card_image_name('quagmire'/'LEG', 'quagmire').
card_uid('quagmire'/'LEG', 'LEG:Quagmire:quagmire').
card_rarity('quagmire'/'LEG', 'Uncommon').
card_artist('quagmire'/'LEG', 'Dan Frazier').
card_multiverse_id('quagmire'/'LEG', '1455').

card_in_set('quarum trench gnomes', 'LEG').
card_original_type('quarum trench gnomes'/'LEG', 'Summon — Gnomes').
card_original_text('quarum trench gnomes'/'LEG', '{T}: Target plains produces {1} instead of {W} until end of game. Use counters.').
card_first_print('quarum trench gnomes', 'LEG').
card_image_name('quarum trench gnomes'/'LEG', 'quarum trench gnomes').
card_uid('quarum trench gnomes'/'LEG', 'LEG:Quarum Trench Gnomes:quarum trench gnomes').
card_rarity('quarum trench gnomes'/'LEG', 'Rare').
card_artist('quarum trench gnomes'/'LEG', 'Dan Frazier').
card_flavor_text('quarum trench gnomes'/'LEG', '\"O Great Captain, all is lost. They tunneled, they burrowed, they trenched. They sapped the strength of our defenses.\" —Sorgus, Chronicles of the Quarum Plains').
card_multiverse_id('quarum trench gnomes'/'LEG', '1588').

card_in_set('rabid wombat', 'LEG').
card_original_type('rabid wombat'/'LEG', 'Summon — Wombat').
card_original_text('rabid wombat'/'LEG', 'Wombat gains +2/+2 for each creature enchantment on it. Attacking does not cause Rabid Wombat to tap.').
card_first_print('rabid wombat', 'LEG').
card_image_name('rabid wombat'/'LEG', 'rabid wombat').
card_uid('rabid wombat'/'LEG', 'LEG:Rabid Wombat:rabid wombat').
card_rarity('rabid wombat'/'LEG', 'Uncommon').
card_artist('rabid wombat'/'LEG', 'Kaja Foglio').
card_multiverse_id('rabid wombat'/'LEG', '1538').

card_in_set('radjan spirit', 'LEG').
card_original_type('radjan spirit'/'LEG', 'Summon — Spirit').
card_original_text('radjan spirit'/'LEG', '{T}: Target creature loses flying ability until end of turn.').
card_first_print('radjan spirit', 'LEG').
card_image_name('radjan spirit'/'LEG', 'radjan spirit').
card_uid('radjan spirit'/'LEG', 'LEG:Radjan Spirit:radjan spirit').
card_rarity('radjan spirit'/'LEG', 'Uncommon').
card_artist('radjan spirit'/'LEG', 'Christopher Rush').
card_multiverse_id('radjan spirit'/'LEG', '1539').

card_in_set('raging bull', 'LEG').
card_original_type('raging bull'/'LEG', 'Summon — Bull').
card_original_text('raging bull'/'LEG', '').
card_first_print('raging bull', 'LEG').
card_image_name('raging bull'/'LEG', 'raging bull').
card_uid('raging bull'/'LEG', 'LEG:Raging Bull:raging bull').
card_rarity('raging bull'/'LEG', 'Common').
card_artist('raging bull'/'LEG', 'Randy Asplund-Faith').
card_flavor_text('raging bull'/'LEG', '\"Sometimes the bulls win, and sometimes the bears win. But the bulls have more fun.\" —Anonymous').
card_multiverse_id('raging bull'/'LEG', '1589').

card_in_set('ragnar', 'LEG').
card_original_type('ragnar'/'LEG', 'Summon — Legend').
card_original_text('ragnar'/'LEG', '{W}{U}{G}{T}: Regenerate target creature.').
card_first_print('ragnar', 'LEG').
card_image_name('ragnar'/'LEG', 'ragnar').
card_uid('ragnar'/'LEG', 'LEG:Ragnar:ragnar').
card_rarity('ragnar'/'LEG', 'Rare').
card_artist('ragnar'/'LEG', 'Melissa A. Benson').
card_flavor_text('ragnar'/'LEG', '\"On the field of honor, a soldier need have no fear.\"').
card_multiverse_id('ragnar'/'LEG', '1676').

card_in_set('ramirez depietro', 'LEG').
card_original_type('ramirez depietro'/'LEG', 'Summon — Legend').
card_original_text('ramirez depietro'/'LEG', 'First strike').
card_first_print('ramirez depietro', 'LEG').
card_image_name('ramirez depietro'/'LEG', 'ramirez depietro').
card_uid('ramirez depietro'/'LEG', 'LEG:Ramirez DePietro:ramirez depietro').
card_rarity('ramirez depietro'/'LEG', 'Uncommon').
card_artist('ramirez depietro'/'LEG', 'Phil Foglio').
card_flavor_text('ramirez depietro'/'LEG', 'Ramirez DePietro is a most flamboyant pirate. Be careful not to believe his tall tales, especially when you ask his age.').
card_multiverse_id('ramirez depietro'/'LEG', '1677').

card_in_set('ramses overdark', 'LEG').
card_original_type('ramses overdark'/'LEG', 'Summon — Legend').
card_original_text('ramses overdark'/'LEG', '{T}: Destroys a target creature which has an enchantment card played on it.').
card_first_print('ramses overdark', 'LEG').
card_image_name('ramses overdark'/'LEG', 'ramses overdark').
card_uid('ramses overdark'/'LEG', 'LEG:Ramses Overdark:ramses overdark').
card_rarity('ramses overdark'/'LEG', 'Rare').
card_artist('ramses overdark'/'LEG', 'Richard Kane Ferguson').
card_multiverse_id('ramses overdark'/'LEG', '1678').

card_in_set('rapid fire', 'LEG').
card_original_type('rapid fire'/'LEG', 'Instant').
card_original_text('rapid fire'/'LEG', 'Play before defense is chosen. Target creature gains first strike until end of turn. If the creature does not already have rampage, then it also gains rampage: 2 until end of turn.').
card_first_print('rapid fire', 'LEG').
card_image_name('rapid fire'/'LEG', 'rapid fire').
card_uid('rapid fire'/'LEG', 'LEG:Rapid Fire:rapid fire').
card_rarity('rapid fire'/'LEG', 'Rare').
card_artist('rapid fire'/'LEG', 'Justin Hampton').
card_multiverse_id('rapid fire'/'LEG', '1630').

card_in_set('rasputin dreamweaver', 'LEG').
card_original_type('rasputin dreamweaver'/'LEG', 'Summon — Legend').
card_original_text('rasputin dreamweaver'/'LEG', 'Put seven counters on Rasputin when brought into play. You may remove a counter to prevent one damage to Rasputin or add {1} to your mana pool. This ability is played as an interrupt. Put one counter on Rasputin during your upkeep if he started the turn untapped. You may not have more than seven of these counters on Rasputin at any time.').
card_first_print('rasputin dreamweaver', 'LEG').
card_image_name('rasputin dreamweaver'/'LEG', 'rasputin dreamweaver').
card_uid('rasputin dreamweaver'/'LEG', 'LEG:Rasputin Dreamweaver:rasputin dreamweaver').
card_rarity('rasputin dreamweaver'/'LEG', 'Rare').
card_artist('rasputin dreamweaver'/'LEG', 'Andi Rusu').
card_multiverse_id('rasputin dreamweaver'/'LEG', '1679').

card_in_set('rebirth', 'LEG').
card_original_type('rebirth'/'LEG', 'Sorcery').
card_original_text('rebirth'/'LEG', 'Each player may choose to be healed to 20 life. Any player choosing to be healed antes an additional card from the top of his or her library. Remove this card from your deck before playing if you are not playing for ante.').
card_first_print('rebirth', 'LEG').
card_image_name('rebirth'/'LEG', 'rebirth').
card_uid('rebirth'/'LEG', 'LEG:Rebirth:rebirth').
card_rarity('rebirth'/'LEG', 'Rare').
card_artist('rebirth'/'LEG', 'Mark Tedin').
card_multiverse_id('rebirth'/'LEG', '1540').

card_in_set('recall', 'LEG').
card_original_type('recall'/'LEG', 'Sorcery').
card_original_text('recall'/'LEG', 'Sacrifice X cards from your hand and then bring X cards from your graveyard to your hand. Then remove Recall from the game.').
card_first_print('recall', 'LEG').
card_image_name('recall'/'LEG', 'recall').
card_uid('recall'/'LEG', 'LEG:Recall:recall').
card_rarity('recall'/'LEG', 'Rare').
card_artist('recall'/'LEG', 'Brian Snõddy').
card_multiverse_id('recall'/'LEG', '1496').

card_in_set('red mana battery', 'LEG').
card_original_type('red mana battery'/'LEG', 'Artifact').
card_original_text('red mana battery'/'LEG', '{2}{T}: Put one counter on Red Mana Battery.\n{T}: Add {R} to your mana pool. Remove as many counters as you wish. For each counter removed add {R} to your mana pool. This ability is played as an interrupt.').
card_first_print('red mana battery', 'LEG').
card_image_name('red mana battery'/'LEG', 'red mana battery').
card_uid('red mana battery'/'LEG', 'LEG:Red Mana Battery:red mana battery').
card_rarity('red mana battery'/'LEG', 'Uncommon').
card_artist('red mana battery'/'LEG', 'Mark Tedin').
card_multiverse_id('red mana battery'/'LEG', '1418').

card_in_set('reincarnation', 'LEG').
card_original_type('reincarnation'/'LEG', 'Instant').
card_original_text('reincarnation'/'LEG', 'If target creature is placed in graveyard this turn, bring a creature from that graveyard directly into play under the control of the owner of the target creature. Treat this creature as though it were just summoned.').
card_first_print('reincarnation', 'LEG').
card_image_name('reincarnation'/'LEG', 'reincarnation').
card_uid('reincarnation'/'LEG', 'LEG:Reincarnation:reincarnation').
card_rarity('reincarnation'/'LEG', 'Uncommon').
card_artist('reincarnation'/'LEG', 'Edward P. Beard, Jr.').
card_multiverse_id('reincarnation'/'LEG', '1541').

card_in_set('relic barrier', 'LEG').
card_original_type('relic barrier'/'LEG', 'Artifact').
card_original_text('relic barrier'/'LEG', '{T}: Target artifact becomes tapped.').
card_first_print('relic barrier', 'LEG').
card_image_name('relic barrier'/'LEG', 'relic barrier').
card_uid('relic barrier'/'LEG', 'LEG:Relic Barrier:relic barrier').
card_rarity('relic barrier'/'LEG', 'Uncommon').
card_artist('relic barrier'/'LEG', 'Harold McNeill').
card_multiverse_id('relic barrier'/'LEG', '1419').

card_in_set('relic bind', 'LEG').
card_original_type('relic bind'/'LEG', 'Enchant Artifact').
card_original_text('relic bind'/'LEG', 'When target artifact is tapped, the controller of Relic Bind can choose to do 1 damage to any player or give 1 life to any player.').
card_first_print('relic bind', 'LEG').
card_image_name('relic bind'/'LEG', 'relic bind').
card_uid('relic bind'/'LEG', 'LEG:Relic Bind:relic bind').
card_rarity('relic bind'/'LEG', 'Uncommon').
card_artist('relic bind'/'LEG', 'Christopher Rush').
card_multiverse_id('relic bind'/'LEG', '1497').

card_in_set('remove enchantments', 'LEG').
card_original_type('remove enchantments'/'LEG', 'Instant').
card_original_text('remove enchantments'/'LEG', 'Remove all enchantments you control and remove all enchantment cards played on all permanents you control. If this spell is cast during opponent\'s attack, also remove all enchantment cards played on attacking creatures. All enchantments you own are returned to your hand; all other enchantments are destroyed.').
card_first_print('remove enchantments', 'LEG').
card_image_name('remove enchantments'/'LEG', 'remove enchantments').
card_uid('remove enchantments'/'LEG', 'LEG:Remove Enchantments:remove enchantments').
card_rarity('remove enchantments'/'LEG', 'Common').
card_artist('remove enchantments'/'LEG', 'Brian Snõddy').
card_multiverse_id('remove enchantments'/'LEG', '1631').

card_in_set('remove soul', 'LEG').
card_original_type('remove soul'/'LEG', 'Interrupt').
card_original_text('remove soul'/'LEG', 'Counter target summon spell.').
card_first_print('remove soul', 'LEG').
card_image_name('remove soul'/'LEG', 'remove soul').
card_uid('remove soul'/'LEG', 'LEG:Remove Soul:remove soul').
card_rarity('remove soul'/'LEG', 'Common').
card_artist('remove soul'/'LEG', 'Brian Snõddy').
card_flavor_text('remove soul'/'LEG', 'Nethya stiffened suddenly, head cocked as if straining to hear some distant sound, then fell lifeless to the ground.').
card_multiverse_id('remove soul'/'LEG', '1498').

card_in_set('reset', 'LEG').
card_original_type('reset'/'LEG', 'Interrupt').
card_original_text('reset'/'LEG', 'All your lands untap. Reset can only be played on an opponent\'s turn after his or her upkeep phase.').
card_first_print('reset', 'LEG').
card_image_name('reset'/'LEG', 'reset').
card_uid('reset'/'LEG', 'LEG:Reset:reset').
card_rarity('reset'/'LEG', 'Uncommon').
card_artist('reset'/'LEG', 'Nicola Leonard').
card_multiverse_id('reset'/'LEG', '1499').

card_in_set('revelation', 'LEG').
card_original_type('revelation'/'LEG', 'Enchant World').
card_original_text('revelation'/'LEG', 'All players play with the cards in their hands face up on the table.').
card_first_print('revelation', 'LEG').
card_image_name('revelation'/'LEG', 'revelation').
card_uid('revelation'/'LEG', 'LEG:Revelation:revelation').
card_rarity('revelation'/'LEG', 'Rare').
card_artist('revelation'/'LEG', 'Kaja Foglio').
card_flavor_text('revelation'/'LEG', '\"Many are in high place, and of renown: but mysteries are revealed unto the meek.\" —Ecclesiastes 3:19').
card_multiverse_id('revelation'/'LEG', '1542').

card_in_set('reverberation', 'LEG').
card_original_type('reverberation'/'LEG', 'Instant').
card_original_text('reverberation'/'LEG', 'Damage from one sorcery spell is redirected to its caster.').
card_first_print('reverberation', 'LEG').
card_image_name('reverberation'/'LEG', 'reverberation').
card_uid('reverberation'/'LEG', 'LEG:Reverberation:reverberation').
card_rarity('reverberation'/'LEG', 'Rare').
card_artist('reverberation'/'LEG', 'Justin Hampton').
card_multiverse_id('reverberation'/'LEG', '1500').

card_in_set('righteous avengers', 'LEG').
card_original_type('righteous avengers'/'LEG', 'Summon — Avengers').
card_original_text('righteous avengers'/'LEG', 'Plainswalk').
card_first_print('righteous avengers', 'LEG').
card_image_name('righteous avengers'/'LEG', 'righteous avengers').
card_uid('righteous avengers'/'LEG', 'LEG:Righteous Avengers:righteous avengers').
card_rarity('righteous avengers'/'LEG', 'Uncommon').
card_artist('righteous avengers'/'LEG', 'Heather Hudson').
card_flavor_text('righteous avengers'/'LEG', 'Few can withstand the wrath of the righteous.').
card_multiverse_id('righteous avengers'/'LEG', '1632').

card_in_set('ring of immortals', 'LEG').
card_original_type('ring of immortals'/'LEG', 'Artifact').
card_original_text('ring of immortals'/'LEG', '{3}{T}: Counter target interrupt or enchantment. Can only counter spells which target a permanent under your control. This ability is played as an interrupt.').
card_first_print('ring of immortals', 'LEG').
card_image_name('ring of immortals'/'LEG', 'ring of immortals').
card_uid('ring of immortals'/'LEG', 'LEG:Ring of Immortals:ring of immortals').
card_rarity('ring of immortals'/'LEG', 'Rare').
card_artist('ring of immortals'/'LEG', 'Melissa A. Benson').
card_multiverse_id('ring of immortals'/'LEG', '1420').

card_in_set('riven turnbull', 'LEG').
card_original_type('riven turnbull'/'LEG', 'Summon — Legend').
card_original_text('riven turnbull'/'LEG', '{T}: Add {B} to your mana pool. This ability is played as an interrupt.').
card_first_print('riven turnbull', 'LEG').
card_image_name('riven turnbull'/'LEG', 'riven turnbull').
card_uid('riven turnbull'/'LEG', 'LEG:Riven Turnbull:riven turnbull').
card_rarity('riven turnbull'/'LEG', 'Uncommon').
card_artist('riven turnbull'/'LEG', 'Richard Kane Ferguson').
card_flavor_text('riven turnbull'/'LEG', '\"Political violence is a perfectly legitimate answer to the persecution handed down by dignitaries of the state.\"').
card_multiverse_id('riven turnbull'/'LEG', '1680').

card_in_set('rohgahh of kher keep', 'LEG').
card_original_type('rohgahh of kher keep'/'LEG', 'Summon — Legend').
card_original_text('rohgahh of kher keep'/'LEG', 'All your Kobolds of Kher Keep gain +2/+2. Pay {R}{R}{R} during your upkeep, or Rohgahh and all Kobolds of Kher Keep become tapped and come under opponent\'s control.').
card_first_print('rohgahh of kher keep', 'LEG').
card_image_name('rohgahh of kher keep'/'LEG', 'rohgahh of kher keep').
card_uid('rohgahh of kher keep'/'LEG', 'LEG:Rohgahh of Kher Keep:rohgahh of kher keep').
card_rarity('rohgahh of kher keep'/'LEG', 'Rare').
card_artist('rohgahh of kher keep'/'LEG', 'Edward P. Beard, Jr.').
card_multiverse_id('rohgahh of kher keep'/'LEG', '1681').

card_in_set('rubinia soulsinger', 'LEG').
card_original_type('rubinia soulsinger'/'LEG', 'Summon — Legend').
card_original_text('rubinia soulsinger'/'LEG', '{T}: Gain control of target creature. Rubinia does not tap or untap this creature. If Rubinia becomes untapped you lose control of this creature; you may choose not to untap Rubinia as normal during your untap phase. You also lose control of target creature if either Rubinia leaves play or you lose control of Rubinia.').
card_first_print('rubinia soulsinger', 'LEG').
card_image_name('rubinia soulsinger'/'LEG', 'rubinia soulsinger').
card_uid('rubinia soulsinger'/'LEG', 'LEG:Rubinia Soulsinger:rubinia soulsinger').
card_rarity('rubinia soulsinger'/'LEG', 'Rare').
card_artist('rubinia soulsinger'/'LEG', 'Rob Alexander').
card_multiverse_id('rubinia soulsinger'/'LEG', '1682').

card_in_set('rust', 'LEG').
card_original_type('rust'/'LEG', 'Interrupt').
card_original_text('rust'/'LEG', 'Counter target artifact effect, which must require an activation cost.').
card_first_print('rust', 'LEG').
card_image_name('rust'/'LEG', 'rust').
card_uid('rust'/'LEG', 'LEG:Rust:rust').
card_rarity('rust'/'LEG', 'Common').
card_artist('rust'/'LEG', 'Liz Danforth').
card_flavor_text('rust'/'LEG', '\"How dull it is to pause, to make an end,/ To rust unburnished, not to shine in use,/ As though to breathe were life!\" —Alfred, Lord Tennyson, \"Ulysses\"').
card_multiverse_id('rust'/'LEG', '1543').

card_in_set('sea kings\' blessing', 'LEG').
card_original_type('sea kings\' blessing'/'LEG', 'Instant').
card_original_text('sea kings\' blessing'/'LEG', 'Changes the color of one or more target creatures to blue until end of turn. You choose which and how many creatures are affected. Cost to tap, maintain, or use a special ability of target creature(s) remains entirely unchanged.').
card_first_print('sea kings\' blessing', 'LEG').
card_image_name('sea kings\' blessing'/'LEG', 'sea kings\' blessing').
card_uid('sea kings\' blessing'/'LEG', 'LEG:Sea Kings\' Blessing:sea kings\' blessing').
card_rarity('sea kings\' blessing'/'LEG', 'Uncommon').
card_artist('sea kings\' blessing'/'LEG', 'Randy Asplund-Faith').
card_multiverse_id('sea kings\' blessing'/'LEG', '1501').

card_in_set('seafarer\'s quay', 'LEG').
card_original_type('seafarer\'s quay'/'LEG', 'Land').
card_original_text('seafarer\'s quay'/'LEG', 'All your blue legends gain bands with other legends.').
card_first_print('seafarer\'s quay', 'LEG').
card_image_name('seafarer\'s quay'/'LEG', 'seafarer\'s quay').
card_uid('seafarer\'s quay'/'LEG', 'LEG:Seafarer\'s Quay:seafarer\'s quay').
card_rarity('seafarer\'s quay'/'LEG', 'Uncommon').
card_artist('seafarer\'s quay'/'LEG', 'Tom Wänerstrand').
card_multiverse_id('seafarer\'s quay'/'LEG', '1704').

card_in_set('seeker', 'LEG').
card_original_type('seeker'/'LEG', 'Enchant Creature').
card_original_text('seeker'/'LEG', 'Target creature cannot be blocked by any creatures except white creatures and artifact creatures.').
card_first_print('seeker', 'LEG').
card_image_name('seeker'/'LEG', 'seeker').
card_uid('seeker'/'LEG', 'LEG:Seeker:seeker').
card_rarity('seeker'/'LEG', 'Uncommon').
card_artist('seeker'/'LEG', 'Mark Poole').
card_multiverse_id('seeker'/'LEG', '1633').

card_in_set('segovian leviathan', 'LEG').
card_original_type('segovian leviathan'/'LEG', 'Summon — Leviathan').
card_original_text('segovian leviathan'/'LEG', 'Islandwalk').
card_first_print('segovian leviathan', 'LEG').
card_image_name('segovian leviathan'/'LEG', 'segovian leviathan').
card_uid('segovian leviathan'/'LEG', 'LEG:Segovian Leviathan:segovian leviathan').
card_rarity('segovian leviathan'/'LEG', 'Uncommon').
card_artist('segovian leviathan'/'LEG', 'Melissa A. Benson').
card_flavor_text('segovian leviathan'/'LEG', '\"Leviathan, too! Can you catch him with a fish-hook/ or run a line round his tongue?\" —Job 40:25').
card_multiverse_id('segovian leviathan'/'LEG', '1502').

card_in_set('sentinel', 'LEG').
card_original_type('sentinel'/'LEG', 'Artifact Creature').
card_original_text('sentinel'/'LEG', 'The * in the lower right hand corner is 1 when cast. While blocking, you may choose to change * to equal one plus the power of target creature sentinel blocks this turn. While attacking, you may choose to change * to equal one plus the power of target creature that blocks Sentinel this turn.').
card_first_print('sentinel', 'LEG').
card_image_name('sentinel'/'LEG', 'sentinel').
card_uid('sentinel'/'LEG', 'LEG:Sentinel:sentinel').
card_rarity('sentinel'/'LEG', 'Rare').
card_artist('sentinel'/'LEG', 'Randy Asplund-Faith').
card_multiverse_id('sentinel'/'LEG', '1421').

card_in_set('serpent generator', 'LEG').
card_original_type('serpent generator'/'LEG', 'Artifact').
card_original_text('serpent generator'/'LEG', '{4}{T}: Put a Poison Snake token into play. Treat this token as a 1/1 artifact creature. If this creature damages opponent, opponent gets a poison counter. If opponent ever has ten or more poison counters, opponent loses game.').
card_first_print('serpent generator', 'LEG').
card_image_name('serpent generator'/'LEG', 'serpent generator').
card_uid('serpent generator'/'LEG', 'LEG:Serpent Generator:serpent generator').
card_rarity('serpent generator'/'LEG', 'Rare').
card_artist('serpent generator'/'LEG', 'Mark Tedin').
card_multiverse_id('serpent generator'/'LEG', '1422').

card_in_set('shelkin brownie', 'LEG').
card_original_type('shelkin brownie'/'LEG', 'Summon — Faerie').
card_original_text('shelkin brownie'/'LEG', '{T}: Remove the bands with other ability from target creature until end of turn.').
card_first_print('shelkin brownie', 'LEG').
card_image_name('shelkin brownie'/'LEG', 'shelkin brownie').
card_uid('shelkin brownie'/'LEG', 'LEG:Shelkin Brownie:shelkin brownie').
card_rarity('shelkin brownie'/'LEG', 'Common').
card_artist('shelkin brownie'/'LEG', 'Douglas Shuler').
card_flavor_text('shelkin brownie'/'LEG', 'Leave a bowl of milk on your doorstep each night for the brownies, or they\'ll cause you no end of mischief.').
card_multiverse_id('shelkin brownie'/'LEG', '1544').

card_in_set('shield wall', 'LEG').
card_original_type('shield wall'/'LEG', 'Instant').
card_original_text('shield wall'/'LEG', 'All your creatures gain +0/+2 until end of turn.').
card_first_print('shield wall', 'LEG').
card_image_name('shield wall'/'LEG', 'shield wall').
card_uid('shield wall'/'LEG', 'LEG:Shield Wall:shield wall').
card_rarity('shield wall'/'LEG', 'Uncommon').
card_artist('shield wall'/'LEG', 'Douglas Shuler').
card_multiverse_id('shield wall'/'LEG', '1634').

card_in_set('shimian night stalker', 'LEG').
card_original_type('shimian night stalker'/'LEG', 'Summon — Night Stalker').
card_original_text('shimian night stalker'/'LEG', '{B}{T}: Redirect all damage done to you from any one attacking creature to the Shimian Night Stalker.').
card_first_print('shimian night stalker', 'LEG').
card_image_name('shimian night stalker'/'LEG', 'shimian night stalker').
card_uid('shimian night stalker'/'LEG', 'LEG:Shimian Night Stalker:shimian night stalker').
card_rarity('shimian night stalker'/'LEG', 'Uncommon').
card_artist('shimian night stalker'/'LEG', 'Jesper Myrfors').
card_flavor_text('shimian night stalker'/'LEG', '\"‘Tis now the very witching time of night,/ When churchyards yawn and hell itself breathes out/ Contagion to this world.\" —William Shakespeare, Hamlet').
card_multiverse_id('shimian night stalker'/'LEG', '1456').

card_in_set('silhouette', 'LEG').
card_original_type('silhouette'/'LEG', 'Instant').
card_original_text('silhouette'/'LEG', 'Until end of turn, damage done to target creature by spells or effects that target it is reduced to 0.').
card_first_print('silhouette', 'LEG').
card_image_name('silhouette'/'LEG', 'silhouette').
card_uid('silhouette'/'LEG', 'LEG:Silhouette:silhouette').
card_rarity('silhouette'/'LEG', 'Uncommon').
card_artist('silhouette'/'LEG', 'Kaja Foglio').
card_multiverse_id('silhouette'/'LEG', '1503').

card_in_set('sir shandlar of eberyn', 'LEG').
card_original_type('sir shandlar of eberyn'/'LEG', 'Summon — Legend').
card_original_text('sir shandlar of eberyn'/'LEG', '').
card_first_print('sir shandlar of eberyn', 'LEG').
card_image_name('sir shandlar of eberyn'/'LEG', 'sir shandlar of eberyn').
card_uid('sir shandlar of eberyn'/'LEG', 'LEG:Sir Shandlar of Eberyn:sir shandlar of eberyn').
card_rarity('sir shandlar of eberyn'/'LEG', 'Uncommon').
card_artist('sir shandlar of eberyn'/'LEG', 'Andi Rusu').
card_flavor_text('sir shandlar of eberyn'/'LEG', '\"Remember Sir Shandlar! Remember and stand firm!\" —rallying cry of the Eberyn militia').
card_multiverse_id('sir shandlar of eberyn'/'LEG', '1683').

card_in_set('sivitri scarzam', 'LEG').
card_original_type('sivitri scarzam'/'LEG', 'Summon — Legend').
card_original_text('sivitri scarzam'/'LEG', '').
card_first_print('sivitri scarzam', 'LEG').
card_image_name('sivitri scarzam'/'LEG', 'sivitri scarzam').
card_uid('sivitri scarzam'/'LEG', 'LEG:Sivitri Scarzam:sivitri scarzam').
card_rarity('sivitri scarzam'/'LEG', 'Uncommon').
card_artist('sivitri scarzam'/'LEG', 'NéNé Thomas').
card_flavor_text('sivitri scarzam'/'LEG', 'Even the brave have cause to tremble at the sight of Sivitri Scarzam. Who else has tamed Scarzam\'s Dragon?').
card_multiverse_id('sivitri scarzam'/'LEG', '1684').

card_in_set('sol\'kanar the swamp king', 'LEG').
card_original_type('sol\'kanar the swamp king'/'LEG', 'Summon — Legend').
card_original_text('sol\'kanar the swamp king'/'LEG', 'Swampwalk\nSol\'kanar\'s controller gains 1 life each time a black spell is cast.').
card_first_print('sol\'kanar the swamp king', 'LEG').
card_image_name('sol\'kanar the swamp king'/'LEG', 'sol\'kanar the swamp king').
card_uid('sol\'kanar the swamp king'/'LEG', 'LEG:Sol\'kanar the Swamp King:sol\'kanar the swamp king').
card_rarity('sol\'kanar the swamp king'/'LEG', 'Rare').
card_artist('sol\'kanar the swamp king'/'LEG', 'Richard Kane Ferguson').
card_multiverse_id('sol\'kanar the swamp king'/'LEG', '1685').

card_in_set('spectral cloak', 'LEG').
card_original_type('spectral cloak'/'LEG', 'Enchant Creature').
card_original_text('spectral cloak'/'LEG', 'Target creature cannot be the target of instants, sorceries, fast effects, or enchantments unless creature is tapped.').
card_first_print('spectral cloak', 'LEG').
card_image_name('spectral cloak'/'LEG', 'spectral cloak').
card_uid('spectral cloak'/'LEG', 'LEG:Spectral Cloak:spectral cloak').
card_rarity('spectral cloak'/'LEG', 'Uncommon').
card_artist('spectral cloak'/'LEG', 'Rob Alexander').
card_multiverse_id('spectral cloak'/'LEG', '1504').

card_in_set('spinal villain', 'LEG').
card_original_type('spinal villain'/'LEG', 'Summon — Villain').
card_original_text('spinal villain'/'LEG', '{T}: Destroy target blue creature.').
card_first_print('spinal villain', 'LEG').
card_image_name('spinal villain'/'LEG', 'spinal villain').
card_uid('spinal villain'/'LEG', 'LEG:Spinal Villain:spinal villain').
card_rarity('spinal villain'/'LEG', 'Rare').
card_artist('spinal villain'/'LEG', 'Anson Maddocks').
card_flavor_text('spinal villain'/'LEG', '\"Striking silent as a dream,/ Cutting short the strangled scream . . .\" —Tobrian, \"Watchdragon\"').
card_multiverse_id('spinal villain'/'LEG', '1590').

card_in_set('spirit link', 'LEG').
card_original_type('spirit link'/'LEG', 'Enchant Creature').
card_original_text('spirit link'/'LEG', 'For every point of damage target creature does, you gain 1 life.').
card_first_print('spirit link', 'LEG').
card_image_name('spirit link'/'LEG', 'spirit link').
card_uid('spirit link'/'LEG', 'LEG:Spirit Link:spirit link').
card_rarity('spirit link'/'LEG', 'Uncommon').
card_artist('spirit link'/'LEG', 'Kaja Foglio').
card_multiverse_id('spirit link'/'LEG', '1635').

card_in_set('spirit shackle', 'LEG').
card_original_type('spirit shackle'/'LEG', 'Enchant Creature').
card_original_text('spirit shackle'/'LEG', 'Put a -0/-2 counter on target creature every time it becomes tapped. Counters remain even if enchantment is removed.').
card_first_print('spirit shackle', 'LEG').
card_image_name('spirit shackle'/'LEG', 'spirit shackle').
card_uid('spirit shackle'/'LEG', 'LEG:Spirit Shackle:spirit shackle').
card_rarity('spirit shackle'/'LEG', 'Common').
card_artist('spirit shackle'/'LEG', 'Edward P. Beard, Jr.').
card_multiverse_id('spirit shackle'/'LEG', '1457').

card_in_set('spiritual sanctuary', 'LEG').
card_original_type('spiritual sanctuary'/'LEG', 'Enchantment').
card_original_text('spiritual sanctuary'/'LEG', 'All players with plains under his or her control gains 1 life point during upkeep.').
card_first_print('spiritual sanctuary', 'LEG').
card_image_name('spiritual sanctuary'/'LEG', 'spiritual sanctuary').
card_uid('spiritual sanctuary'/'LEG', 'LEG:Spiritual Sanctuary:spiritual sanctuary').
card_rarity('spiritual sanctuary'/'LEG', 'Rare').
card_artist('spiritual sanctuary'/'LEG', 'Amy Weber').
card_multiverse_id('spiritual sanctuary'/'LEG', '1636').

card_in_set('stangg', 'LEG').
card_original_type('stangg'/'LEG', 'Summon — Legend').
card_original_text('stangg'/'LEG', 'When Stangg is brought into play, also put a Stangg Twin token into play. Stangg Twin token is a 3/4 green and red legend. If Stangg leaves play, remove Stangg Twin token from game. If Stangg Twin leaves play, bury Stangg.').
card_first_print('stangg', 'LEG').
card_image_name('stangg'/'LEG', 'stangg').
card_uid('stangg'/'LEG', 'LEG:Stangg:stangg').
card_rarity('stangg'/'LEG', 'Rare').
card_artist('stangg'/'LEG', 'Mark Poole').
card_multiverse_id('stangg'/'LEG', '1686').

card_in_set('storm seeker', 'LEG').
card_original_type('storm seeker'/'LEG', 'Instant').
card_original_text('storm seeker'/'LEG', 'Storm Seeker does 1 damage to opponent for every card in his or her hand.').
card_first_print('storm seeker', 'LEG').
card_image_name('storm seeker'/'LEG', 'storm seeker').
card_uid('storm seeker'/'LEG', 'LEG:Storm Seeker:storm seeker').
card_rarity('storm seeker'/'LEG', 'Uncommon').
card_artist('storm seeker'/'LEG', 'Mark Poole').
card_multiverse_id('storm seeker'/'LEG', '1545').

card_in_set('storm world', 'LEG').
card_original_type('storm world'/'LEG', 'Enchant World').
card_original_text('storm world'/'LEG', 'If any player has less than four cards in hand at the end of his or her upkeep, Storm World does one damage to that player for each card less than four.').
card_first_print('storm world', 'LEG').
card_image_name('storm world'/'LEG', 'storm world').
card_uid('storm world'/'LEG', 'LEG:Storm World:storm world').
card_rarity('storm world'/'LEG', 'Rare').
card_artist('storm world'/'LEG', 'Christopher Rush').
card_multiverse_id('storm world'/'LEG', '1591').

card_in_set('subdue', 'LEG').
card_original_type('subdue'/'LEG', 'Instant').
card_original_text('subdue'/'LEG', 'Target creature deals no damage during combat but gains X toughness until end of turn; X is target creature\'s casting cost.').
card_first_print('subdue', 'LEG').
card_image_name('subdue'/'LEG', 'subdue').
card_uid('subdue'/'LEG', 'LEG:Subdue:subdue').
card_rarity('subdue'/'LEG', 'Common').
card_artist('subdue'/'LEG', 'Brian Snõddy').
card_multiverse_id('subdue'/'LEG', '1546').

card_in_set('sunastian falconer', 'LEG').
card_original_type('sunastian falconer'/'LEG', 'Summon — Legend').
card_original_text('sunastian falconer'/'LEG', '{T}: Add {2} to your mana pool. This ability is played as an interrupt.').
card_first_print('sunastian falconer', 'LEG').
card_image_name('sunastian falconer'/'LEG', 'sunastian falconer').
card_uid('sunastian falconer'/'LEG', 'LEG:Sunastian Falconer:sunastian falconer').
card_rarity('sunastian falconer'/'LEG', 'Uncommon').
card_artist('sunastian falconer'/'LEG', 'Christopher Rush').
card_flavor_text('sunastian falconer'/'LEG', 'Sunastian has roots in both sorcery and swordplay; he has learned never to depend too heavily on the latter.').
card_multiverse_id('sunastian falconer'/'LEG', '1687').

card_in_set('sword of the ages', 'LEG').
card_original_type('sword of the ages'/'LEG', 'Artifact').
card_original_text('sword of the ages'/'LEG', 'Sword of the Ages comes into play tapped.\n{T}: Sacrifice Sword of the Ages and as many creatures as you choose. Sword does the combined power of these creatures in damage to one target. Sacrificed creatures and Sword are then removed from the game entirely.').
card_first_print('sword of the ages', 'LEG').
card_image_name('sword of the ages'/'LEG', 'sword of the ages').
card_uid('sword of the ages'/'LEG', 'LEG:Sword of the Ages:sword of the ages').
card_rarity('sword of the ages'/'LEG', 'Rare').
card_artist('sword of the ages'/'LEG', 'Dan Frazier').
card_multiverse_id('sword of the ages'/'LEG', '1423').

card_in_set('sylvan library', 'LEG').
card_original_type('sylvan library'/'LEG', 'Enchantment').
card_original_text('sylvan library'/'LEG', 'You may draw two extra cards during your draw phase, then either put two of the cards drawn this turn back on top of your library (in any order), or lose 4 lives per card not replaced. Effects that prevent or redirect damage may not be used to counter this loss of life.').
card_first_print('sylvan library', 'LEG').
card_image_name('sylvan library'/'LEG', 'sylvan library').
card_uid('sylvan library'/'LEG', 'LEG:Sylvan Library:sylvan library').
card_rarity('sylvan library'/'LEG', 'Uncommon').
card_artist('sylvan library'/'LEG', 'Harold McNeill').
card_multiverse_id('sylvan library'/'LEG', '1547').

card_in_set('sylvan paradise', 'LEG').
card_original_type('sylvan paradise'/'LEG', 'Instant').
card_original_text('sylvan paradise'/'LEG', 'Changes the color of one or more target creatures to green until end of turn. You choose which and how many creatures are affected. Cost to tap, maintain, or use a special ability of target creatures remains entirely unchanged.').
card_first_print('sylvan paradise', 'LEG').
card_image_name('sylvan paradise'/'LEG', 'sylvan paradise').
card_uid('sylvan paradise'/'LEG', 'LEG:Sylvan Paradise:sylvan paradise').
card_rarity('sylvan paradise'/'LEG', 'Uncommon').
card_artist('sylvan paradise'/'LEG', 'Randy Asplund-Faith').
card_multiverse_id('sylvan paradise'/'LEG', '1548').

card_in_set('syphon soul', 'LEG').
card_original_type('syphon soul'/'LEG', 'Sorcery').
card_original_text('syphon soul'/'LEG', 'Syphon Soul does 2 damage to all players except caster. Caster gains life points equal to the amount of damage done by Syphon Soul.').
card_first_print('syphon soul', 'LEG').
card_image_name('syphon soul'/'LEG', 'syphon soul').
card_uid('syphon soul'/'LEG', 'LEG:Syphon Soul:syphon soul').
card_rarity('syphon soul'/'LEG', 'Common').
card_artist('syphon soul'/'LEG', 'Melissa A. Benson').
card_flavor_text('syphon soul'/'LEG', '\"Her lips suck forth; see, where it flies!\"\n—Christopher Marlowe, The Tragical History of Doctor Faustus').
card_multiverse_id('syphon soul'/'LEG', '1458').

card_in_set('takklemaggot', 'LEG').
card_original_type('takklemaggot'/'LEG', 'Enchant Creature').
card_original_text('takklemaggot'/'LEG', 'Put a 0/-1 counter on target creature during its controller\'s upkeep. If the creature is placed in the graveyard, its controller chooses a new target for Takklemaggot. If there are no legal targets, Takklemaggot becomes an enchantment and does 1 damage to the controller of the last creature Takklemaggot was on, during his or her upkeep. Takklemaggot does not revert to a creature enchantment even if other creatures are afterwards brought into play.').
card_first_print('takklemaggot', 'LEG').
card_image_name('takklemaggot'/'LEG', 'takklemaggot').
card_uid('takklemaggot'/'LEG', 'LEG:Takklemaggot:takklemaggot').
card_rarity('takklemaggot'/'LEG', 'Uncommon').
card_artist('takklemaggot'/'LEG', 'Daniel Gelon').
card_multiverse_id('takklemaggot'/'LEG', '1459').

card_in_set('telekinesis', 'LEG').
card_original_type('telekinesis'/'LEG', 'Instant').
card_original_text('telekinesis'/'LEG', 'Target creature deals no damage during combat this turn. Creature becomes tapped and may not untap as normal during its controller\'s next two untap phases.').
card_first_print('telekinesis', 'LEG').
card_image_name('telekinesis'/'LEG', 'telekinesis').
card_uid('telekinesis'/'LEG', 'LEG:Telekinesis:telekinesis').
card_rarity('telekinesis'/'LEG', 'Rare').
card_artist('telekinesis'/'LEG', 'Daniel Gelon').
card_multiverse_id('telekinesis'/'LEG', '1505').

card_in_set('teleport', 'LEG').
card_original_type('teleport'/'LEG', 'Instant').
card_original_text('teleport'/'LEG', 'Target creature cannot be blocked until end of turn. Play after attack is declared and before defense is chosen.').
card_first_print('teleport', 'LEG').
card_image_name('teleport'/'LEG', 'teleport').
card_uid('teleport'/'LEG', 'LEG:Teleport:teleport').
card_rarity('teleport'/'LEG', 'Rare').
card_artist('teleport'/'LEG', 'Douglas Shuler').
card_multiverse_id('teleport'/'LEG', '1506').

card_in_set('tempest efreet', 'LEG').
card_original_type('tempest efreet'/'LEG', 'Summon — Efreet').
card_original_text('tempest efreet'/'LEG', '{T}: Pick a card at random from opponent\'s hand and place it in yours. Bury Tempest Efreet in opponent\'s graveyard. The change in ownership is permanent. Play as an interrupt, but opponent may prevent effect by paying 10 life or conceding game before the card to be switched is chosen—if this is done, Tempest Efreet is buried. Effects that prevent or redirect damage may not be used to counter this loss of life. Remove this card from deck if not playing for ante.').
card_first_print('tempest efreet', 'LEG').
card_image_name('tempest efreet'/'LEG', 'tempest efreet').
card_uid('tempest efreet'/'LEG', 'LEG:Tempest Efreet:tempest efreet').
card_rarity('tempest efreet'/'LEG', 'Rare').
card_artist('tempest efreet'/'LEG', 'NéNé Thomas').
card_multiverse_id('tempest efreet'/'LEG', '1592').

card_in_set('tetsuo umezawa', 'LEG').
card_original_type('tetsuo umezawa'/'LEG', 'Summon — Legend').
card_original_text('tetsuo umezawa'/'LEG', '{R}{B}{B}{U}{T}: Destroy target tapped creature or target blocking creature. Tetsuo may not be a target of an enchant creature spell.').
card_first_print('tetsuo umezawa', 'LEG').
card_image_name('tetsuo umezawa'/'LEG', 'tetsuo umezawa').
card_uid('tetsuo umezawa'/'LEG', 'LEG:Tetsuo Umezawa:tetsuo umezawa').
card_rarity('tetsuo umezawa'/'LEG', 'Rare').
card_artist('tetsuo umezawa'/'LEG', 'Julie Baroh').
card_multiverse_id('tetsuo umezawa'/'LEG', '1688').

card_in_set('the abyss', 'LEG').
card_original_type('the abyss'/'LEG', 'Enchant World').
card_original_text('the abyss'/'LEG', 'All players bury one target non-artifact creature under their control, if they have any, during their upkeep.').
card_first_print('the abyss', 'LEG').
card_image_name('the abyss'/'LEG', 'the abyss').
card_uid('the abyss'/'LEG', 'LEG:The Abyss:the abyss').
card_rarity('the abyss'/'LEG', 'Rare').
card_artist('the abyss'/'LEG', 'Pete Venters').
card_flavor_text('the abyss'/'LEG', '\"An immense river of oblivion is sweeping us away into a nameless abyss.\" —Ernest Renan, Souvenirs d\'Enfance et de Jeunesse').
card_multiverse_id('the abyss'/'LEG', '1460').

card_in_set('the brute', 'LEG').
card_original_type('the brute'/'LEG', 'Enchant Creature').
card_original_text('the brute'/'LEG', 'Target creature gains +1/+0\n{R}{R}{R} Regenerates').
card_first_print('the brute', 'LEG').
card_image_name('the brute'/'LEG', 'the brute').
card_uid('the brute'/'LEG', 'LEG:The Brute:the brute').
card_rarity('the brute'/'LEG', 'Common').
card_artist('the brute'/'LEG', 'Mark Poole').
card_flavor_text('the brute'/'LEG', '\"Union may be strength, but it is mere blind brute strength unless wisely directed.\" —Samuel Butler').
card_multiverse_id('the brute'/'LEG', '1593').

card_in_set('the lady of the mountain', 'LEG').
card_original_type('the lady of the mountain'/'LEG', 'Summon — Legend').
card_original_text('the lady of the mountain'/'LEG', '').
card_first_print('the lady of the mountain', 'LEG').
card_image_name('the lady of the mountain'/'LEG', 'the lady of the mountain').
card_uid('the lady of the mountain'/'LEG', 'LEG:The Lady of the Mountain:the lady of the mountain').
card_rarity('the lady of the mountain'/'LEG', 'Uncommon').
card_artist('the lady of the mountain'/'LEG', 'Richard Kane Ferguson').
card_flavor_text('the lady of the mountain'/'LEG', 'Her given name has been lost in the mists of time. Legend says that her silent vigil will one day be ended by the one who, pure of heart and spirit, calls out that name again.').
card_multiverse_id('the lady of the mountain'/'LEG', '1689').

card_in_set('the tabernacle at pendrell vale', 'LEG').
card_original_type('the tabernacle at pendrell vale'/'LEG', 'Legendary Land').
card_original_text('the tabernacle at pendrell vale'/'LEG', 'All creatures now require an upkeep cost of {1} in addition to any other upkeep costs they may have. If the upkeep cost for a creature is not paid, the creature is destroyed.').
card_first_print('the tabernacle at pendrell vale', 'LEG').
card_image_name('the tabernacle at pendrell vale'/'LEG', 'the tabernacle at pendrell vale').
card_uid('the tabernacle at pendrell vale'/'LEG', 'LEG:The Tabernacle at Pendrell Vale:the tabernacle at pendrell vale').
card_rarity('the tabernacle at pendrell vale'/'LEG', 'Rare').
card_artist('the tabernacle at pendrell vale'/'LEG', 'Nicola Leonard').
card_multiverse_id('the tabernacle at pendrell vale'/'LEG', '1690').

card_in_set('the wretched', 'LEG').
card_original_type('the wretched'/'LEG', 'Summon — Wretched').
card_original_text('the wretched'/'LEG', 'At the end of combat take control of all creatures that blocked The Wretched. The Wretched does not tap or untap these creatures. You lose control of these creatures if The Wretched leaves play or if you lose control of The Wretched.').
card_first_print('the wretched', 'LEG').
card_image_name('the wretched'/'LEG', 'the wretched').
card_uid('the wretched'/'LEG', 'LEG:The Wretched:the wretched').
card_rarity('the wretched'/'LEG', 'Rare').
card_artist('the wretched'/'LEG', 'Christopher Rush').
card_multiverse_id('the wretched'/'LEG', '1461').

card_in_set('thunder spirit', 'LEG').
card_original_type('thunder spirit'/'LEG', 'Summon — Spirit').
card_original_text('thunder spirit'/'LEG', 'First strike, flying.').
card_first_print('thunder spirit', 'LEG').
card_image_name('thunder spirit'/'LEG', 'thunder spirit').
card_uid('thunder spirit'/'LEG', 'LEG:Thunder Spirit:thunder spirit').
card_rarity('thunder spirit'/'LEG', 'Rare').
card_artist('thunder spirit'/'LEG', 'Randy Asplund-Faith').
card_flavor_text('thunder spirit'/'LEG', '\"It was full of fire and smoke and light and . . . it drove between us and the Efrafans like a thousand thunderstorms with lightning.\" —Richard Adams, Watership Down').
card_multiverse_id('thunder spirit'/'LEG', '1637').

card_in_set('time elemental', 'LEG').
card_original_type('time elemental'/'LEG', 'Summon — Elemental').
card_original_text('time elemental'/'LEG', '{2}{U}{U}{T}: Return target permanent to owner\'s hand. Cannot target permanents with enchantment cards played on them.\nIf Time Elemental blocks or attacks it is destroyed and does 5 damage to controller.').
card_first_print('time elemental', 'LEG').
card_image_name('time elemental'/'LEG', 'time elemental').
card_uid('time elemental'/'LEG', 'LEG:Time Elemental:time elemental').
card_rarity('time elemental'/'LEG', 'Rare').
card_artist('time elemental'/'LEG', 'Amy Weber').
card_multiverse_id('time elemental'/'LEG', '1507').

card_in_set('tobias andrion', 'LEG').
card_original_type('tobias andrion'/'LEG', 'Summon — Legend').
card_original_text('tobias andrion'/'LEG', '').
card_first_print('tobias andrion', 'LEG').
card_image_name('tobias andrion'/'LEG', 'tobias andrion').
card_uid('tobias andrion'/'LEG', 'LEG:Tobias Andrion:tobias andrion').
card_rarity('tobias andrion'/'LEG', 'Uncommon').
card_artist('tobias andrion'/'LEG', 'Andi Rusu').
card_flavor_text('tobias andrion'/'LEG', 'Administrator of the military state of Sheoltun, Tobias is the military right arm of the empire and the figurehead of its freedom.').
card_multiverse_id('tobias andrion'/'LEG', '1691').

card_in_set('tolaria', 'LEG').
card_original_type('tolaria'/'LEG', 'Legendary Land').
card_original_text('tolaria'/'LEG', '{T}: Add {U} to your mana pool\n{T}: During upkeep remove the banding or bands with other ability from target creature until end of turn.').
card_first_print('tolaria', 'LEG').
card_image_name('tolaria'/'LEG', 'tolaria').
card_uid('tolaria'/'LEG', 'LEG:Tolaria:tolaria').
card_rarity('tolaria'/'LEG', 'Uncommon').
card_artist('tolaria'/'LEG', 'Nicola Leonard').
card_flavor_text('tolaria'/'LEG', '\"Fairest Isle, all isles excelling,/ Seat of pleasures, and of loves . . .\" —John Dryden').
card_multiverse_id('tolaria'/'LEG', '1705').

card_in_set('tor wauki', 'LEG').
card_original_type('tor wauki'/'LEG', 'Summon — Legend').
card_original_text('tor wauki'/'LEG', '{T}: Tor Wauki does 2 damage to target attacking or blocking creature.').
card_first_print('tor wauki', 'LEG').
card_image_name('tor wauki'/'LEG', 'tor wauki').
card_uid('tor wauki'/'LEG', 'LEG:Tor Wauki:tor wauki').
card_rarity('tor wauki'/'LEG', 'Uncommon').
card_artist('tor wauki'/'LEG', 'Randy Asplund-Faith').
card_multiverse_id('tor wauki'/'LEG', '1692').

card_in_set('torsten von ursus', 'LEG').
card_original_type('torsten von ursus'/'LEG', 'Summon — Legend').
card_original_text('torsten von ursus'/'LEG', '').
card_first_print('torsten von ursus', 'LEG').
card_image_name('torsten von ursus'/'LEG', 'torsten von ursus').
card_uid('torsten von ursus'/'LEG', 'LEG:Torsten Von Ursus:torsten von ursus').
card_rarity('torsten von ursus'/'LEG', 'Uncommon').
card_artist('torsten von ursus'/'LEG', 'Mark Poole').
card_flavor_text('torsten von ursus'/'LEG', '\"How can you accuse me of evil? Though these deeds be unsavory, no one will argue: good shall follow from them.\"').
card_multiverse_id('torsten von ursus'/'LEG', '1693').

card_in_set('touch of darkness', 'LEG').
card_original_type('touch of darkness'/'LEG', 'Instant').
card_original_text('touch of darkness'/'LEG', 'Changes the color of one or more target creatures to black until end of turn. You choose which and how many creatures are affected. Cost to tap, maintain, or to use a special ability of target creatures remains entirely unchanged.').
card_first_print('touch of darkness', 'LEG').
card_image_name('touch of darkness'/'LEG', 'touch of darkness').
card_uid('touch of darkness'/'LEG', 'LEG:Touch of Darkness:touch of darkness').
card_rarity('touch of darkness'/'LEG', 'Uncommon').
card_artist('touch of darkness'/'LEG', 'Pete Venters').
card_flavor_text('touch of darkness'/'LEG', '\"Black spirits and white, red spirits and gray,/ Mingle, mingle, mingle, you that mingle may.\" —Thomas Middleton, The Witch').
card_multiverse_id('touch of darkness'/'LEG', '1462').

card_in_set('transmutation', 'LEG').
card_original_type('transmutation'/'LEG', 'Instant').
card_original_text('transmutation'/'LEG', 'Until end of turn, target creature\'s power and toughness are switched. Effects that alter power alter toughness instead, and vice versa.').
card_first_print('transmutation', 'LEG').
card_image_name('transmutation'/'LEG', 'transmutation').
card_uid('transmutation'/'LEG', 'LEG:Transmutation:transmutation').
card_rarity('transmutation'/'LEG', 'Common').
card_artist('transmutation'/'LEG', 'Susan Van Camp').
card_flavor_text('transmutation'/'LEG', '\"You know what I was,/ You see what I am: change me, change me!\"\n—Randall').
card_multiverse_id('transmutation'/'LEG', '1463').

card_in_set('triassic egg', 'LEG').
card_original_type('triassic egg'/'LEG', 'Artifact').
card_original_text('triassic egg'/'LEG', '{3}{T}: Put one counter on Triassic Egg. If there are at least two such counters, you may sacrifice Triassic Egg to take any creature from your hand or graveyard and put it directly into play. Treat this creature as though it were just summoned.').
card_first_print('triassic egg', 'LEG').
card_image_name('triassic egg'/'LEG', 'triassic egg').
card_uid('triassic egg'/'LEG', 'LEG:Triassic Egg:triassic egg').
card_rarity('triassic egg'/'LEG', 'Rare').
card_artist('triassic egg'/'LEG', 'Dan Frazier').
card_multiverse_id('triassic egg'/'LEG', '1424').

card_in_set('tuknir deathlock', 'LEG').
card_original_type('tuknir deathlock'/'LEG', 'Summon — Legend').
card_original_text('tuknir deathlock'/'LEG', 'Flying\n{G}{R}{T}: Target creature gains +2/+2 until end of turn.').
card_first_print('tuknir deathlock', 'LEG').
card_image_name('tuknir deathlock'/'LEG', 'tuknir deathlock').
card_uid('tuknir deathlock'/'LEG', 'LEG:Tuknir Deathlock:tuknir deathlock').
card_rarity('tuknir deathlock'/'LEG', 'Rare').
card_artist('tuknir deathlock'/'LEG', 'Liz Danforth').
card_flavor_text('tuknir deathlock'/'LEG', 'An explorer of the Æther, Tuknir often discovers himself in the most unusual physical realms.').
card_multiverse_id('tuknir deathlock'/'LEG', '1694').

card_in_set('tundra wolves', 'LEG').
card_original_type('tundra wolves'/'LEG', 'Summon — Wolves').
card_original_text('tundra wolves'/'LEG', 'First strike').
card_first_print('tundra wolves', 'LEG').
card_image_name('tundra wolves'/'LEG', 'tundra wolves').
card_uid('tundra wolves'/'LEG', 'LEG:Tundra Wolves:tundra wolves').
card_rarity('tundra wolves'/'LEG', 'Common').
card_artist('tundra wolves'/'LEG', 'Quinton Hoover').
card_flavor_text('tundra wolves'/'LEG', 'I heard their eerie howling, the wolves calling their kindred across the frozen plains.').
card_multiverse_id('tundra wolves'/'LEG', '1638').

card_in_set('typhoon', 'LEG').
card_original_type('typhoon'/'LEG', 'Sorcery').
card_original_text('typhoon'/'LEG', 'Typhoon does 1 damage to each opponent for each island he or she controls.').
card_first_print('typhoon', 'LEG').
card_image_name('typhoon'/'LEG', 'typhoon').
card_uid('typhoon'/'LEG', 'LEG:Typhoon:typhoon').
card_rarity('typhoon'/'LEG', 'Rare').
card_artist('typhoon'/'LEG', 'Anson Maddocks').
card_flavor_text('typhoon'/'LEG', 'Fierce winds ripped across the tropical landscape. What they did not destroy with their fiery breath was washed away by torrential rain.').
card_multiverse_id('typhoon'/'LEG', '1549').

card_in_set('undertow', 'LEG').
card_original_type('undertow'/'LEG', 'Enchantment').
card_original_text('undertow'/'LEG', 'Creatures with islandwalk may be blocked as if they did not have this ability.').
card_first_print('undertow', 'LEG').
card_image_name('undertow'/'LEG', 'undertow').
card_uid('undertow'/'LEG', 'LEG:Undertow:undertow').
card_rarity('undertow'/'LEG', 'Uncommon').
card_artist('undertow'/'LEG', 'Randy Asplund-Faith').
card_multiverse_id('undertow'/'LEG', '1508').

card_in_set('underworld dreams', 'LEG').
card_original_type('underworld dreams'/'LEG', 'Enchantment').
card_original_text('underworld dreams'/'LEG', 'Underworld Dreams does one damage to opponent for each card he or she draws.').
card_first_print('underworld dreams', 'LEG').
card_image_name('underworld dreams'/'LEG', 'underworld dreams').
card_uid('underworld dreams'/'LEG', 'LEG:Underworld Dreams:underworld dreams').
card_rarity('underworld dreams'/'LEG', 'Uncommon').
card_artist('underworld dreams'/'LEG', 'Julie Baroh').
card_flavor_text('underworld dreams'/'LEG', '\"In the drowsy dark cave of the mind dreams build their nest with fragments dropped from day\'s caravan.\" —Rabindranath Tagore').
card_multiverse_id('underworld dreams'/'LEG', '1464').

card_in_set('unholy citadel', 'LEG').
card_original_type('unholy citadel'/'LEG', 'Land').
card_original_text('unholy citadel'/'LEG', 'All your black legends gain bands with other legends.').
card_first_print('unholy citadel', 'LEG').
card_image_name('unholy citadel'/'LEG', 'unholy citadel').
card_uid('unholy citadel'/'LEG', 'LEG:Unholy Citadel:unholy citadel').
card_rarity('unholy citadel'/'LEG', 'Uncommon').
card_artist('unholy citadel'/'LEG', 'Mark Poole').
card_multiverse_id('unholy citadel'/'LEG', '1706').

card_in_set('untamed wilds', 'LEG').
card_original_type('untamed wilds'/'LEG', 'Sorcery').
card_original_text('untamed wilds'/'LEG', 'Search your library for any one basic land and put it into play. This does not count towards your one land per turn limit. Reshuffle your library afterwards.').
card_first_print('untamed wilds', 'LEG').
card_image_name('untamed wilds'/'LEG', 'untamed wilds').
card_uid('untamed wilds'/'LEG', 'LEG:Untamed Wilds:untamed wilds').
card_rarity('untamed wilds'/'LEG', 'Uncommon').
card_artist('untamed wilds'/'LEG', 'NéNé Thomas').
card_multiverse_id('untamed wilds'/'LEG', '1550').

card_in_set('ur-drago', 'LEG').
card_original_type('ur-drago'/'LEG', 'Summon — Legend').
card_original_text('ur-drago'/'LEG', 'First strike\nCreatures with swampwalk may be blocked as if they did not have this ability.').
card_first_print('ur-drago', 'LEG').
card_image_name('ur-drago'/'LEG', 'ur-drago').
card_uid('ur-drago'/'LEG', 'LEG:Ur-Drago:ur-drago').
card_rarity('ur-drago'/'LEG', 'Rare').
card_artist('ur-drago'/'LEG', 'Christopher Rush').
card_multiverse_id('ur-drago'/'LEG', '1695').

card_in_set('urborg', 'LEG').
card_original_type('urborg'/'LEG', 'Legendary Land').
card_original_text('urborg'/'LEG', '{T}: Add {B} to your mana pool\n{T}: Remove first strike ability or swampwalk ability from target creature until end of turn.').
card_first_print('urborg', 'LEG').
card_image_name('urborg'/'LEG', 'urborg').
card_uid('urborg'/'LEG', 'LEG:Urborg:urborg').
card_rarity('urborg'/'LEG', 'Uncommon').
card_artist('urborg'/'LEG', 'Bryon Wackwitz').
card_flavor_text('urborg'/'LEG', '\"Resignedly beneath the sky/The melancholy waters lie./So blend the turrets and shadows there/That all seem pendulous in air,/While from a proud tower in town/Death looks gigantically down.\" —Edgar Allan Poe, \"The City in the Sea\"').
card_multiverse_id('urborg'/'LEG', '1707').

card_in_set('vaevictis asmadi', 'LEG').
card_original_type('vaevictis asmadi'/'LEG', 'Summon — Elder Dragon Legend').
card_original_text('vaevictis asmadi'/'LEG', 'Flying\n{B} Gain +1/+0 until end of turn.\n{R} Gain +1/+0 until end of turn.\n{G} Gain +1/+0 until end of turn.\nPay {B}{R}{G} during your upkeep or Vaevictis Asmadi is buried.').
card_first_print('vaevictis asmadi', 'LEG').
card_image_name('vaevictis asmadi'/'LEG', 'vaevictis asmadi').
card_uid('vaevictis asmadi'/'LEG', 'LEG:Vaevictis Asmadi:vaevictis asmadi').
card_rarity('vaevictis asmadi'/'LEG', 'Rare').
card_artist('vaevictis asmadi'/'LEG', 'Andi Rusu').
card_multiverse_id('vaevictis asmadi'/'LEG', '1696').

card_in_set('vampire bats', 'LEG').
card_original_type('vampire bats'/'LEG', 'Summon — Bats').
card_original_text('vampire bats'/'LEG', 'Flying, {B} +1/+0 until end of turn. No more than {B}{B} may be spent in this way per turn.').
card_first_print('vampire bats', 'LEG').
card_image_name('vampire bats'/'LEG', 'vampire bats').
card_uid('vampire bats'/'LEG', 'LEG:Vampire Bats:vampire bats').
card_rarity('vampire bats'/'LEG', 'Common').
card_artist('vampire bats'/'LEG', 'Anson Maddocks').
card_flavor_text('vampire bats'/'LEG', '\"For something is amiss or out of place/ When mice with wings can wear a human face.\" —Theodore Roethke, \"The Bat\"').
card_multiverse_id('vampire bats'/'LEG', '1465').

card_in_set('venarian gold', 'LEG').
card_original_type('venarian gold'/'LEG', 'Enchant Creature').
card_original_text('venarian gold'/'LEG', 'Put X counters on target creature. Target creature becomes tapped when Venarian Gold is cast. Creature does not untap as normal if it has any of these counters on it. Remove one counter during creature\'s controller\'s upkeep phase.').
card_first_print('venarian gold', 'LEG').
card_image_name('venarian gold'/'LEG', 'venarian gold').
card_uid('venarian gold'/'LEG', 'LEG:Venarian Gold:venarian gold').
card_rarity('venarian gold'/'LEG', 'Common').
card_artist('venarian gold'/'LEG', 'Daniel Gelon').
card_multiverse_id('venarian gold'/'LEG', '1509').

card_in_set('visions', 'LEG').
card_original_type('visions'/'LEG', 'Sorcery').
card_original_text('visions'/'LEG', 'You may look at the top five cards of any library. You may then choose to shuffle that library.').
card_first_print('visions', 'LEG').
card_image_name('visions'/'LEG', 'visions').
card_uid('visions'/'LEG', 'LEG:Visions:visions').
card_rarity('visions'/'LEG', 'Uncommon').
card_artist('visions'/'LEG', 'NéNé Thomas').
card_flavor_text('visions'/'LEG', '\"Visions of glory, spare my aching sight,/ Ye unborn ages, crowd not on my soul!\"\n—Thomas Gray, The Bard').
card_multiverse_id('visions'/'LEG', '1639').

card_in_set('voodoo doll', 'LEG').
card_original_type('voodoo doll'/'LEG', 'Artifact').
card_original_text('voodoo doll'/'LEG', 'Put one counter on Voodoo Doll during your upkeep. If Voodoo Doll is not tapped at end of your turn, it does X damage to you and is destroyed. X equals the number of counters on Voodoo Doll.\n{X}{X}{T}: Voodoo Doll does X damage to any one target.').
card_first_print('voodoo doll', 'LEG').
card_image_name('voodoo doll'/'LEG', 'voodoo doll').
card_uid('voodoo doll'/'LEG', 'LEG:Voodoo Doll:voodoo doll').
card_rarity('voodoo doll'/'LEG', 'Rare').
card_artist('voodoo doll'/'LEG', 'Sandra Everingham').
card_multiverse_id('voodoo doll'/'LEG', '1425').

card_in_set('walking dead', 'LEG').
card_original_type('walking dead'/'LEG', 'Summon — Walking Dead').
card_original_text('walking dead'/'LEG', '{B} Regenerates').
card_first_print('walking dead', 'LEG').
card_image_name('walking dead'/'LEG', 'walking dead').
card_uid('walking dead'/'LEG', 'LEG:Walking Dead:walking dead').
card_rarity('walking dead'/'LEG', 'Common').
card_artist('walking dead'/'LEG', 'Dan Frazier').
card_flavor_text('walking dead'/'LEG', 'The Walking Dead are the remains of freakish experiments by the Necromantic Lords.').
card_multiverse_id('walking dead'/'LEG', '1466').

card_in_set('wall of caltrops', 'LEG').
card_original_type('wall of caltrops'/'LEG', 'Summon — Wall').
card_original_text('wall of caltrops'/'LEG', 'If Wall of Caltrops and one or more other walls join to block an attacker and no other creatures besides walls block that attacker, Wall of Caltrops gains banding ability until end of turn.').
card_first_print('wall of caltrops', 'LEG').
card_image_name('wall of caltrops'/'LEG', 'wall of caltrops').
card_uid('wall of caltrops'/'LEG', 'LEG:Wall of Caltrops:wall of caltrops').
card_rarity('wall of caltrops'/'LEG', 'Common').
card_artist('wall of caltrops'/'LEG', 'Brian Snõddy').
card_flavor_text('wall of caltrops'/'LEG', '\"Ow! Ow ow ow! Oooh, ow, OW!\"').
card_multiverse_id('wall of caltrops'/'LEG', '1640').

card_in_set('wall of dust', 'LEG').
card_original_type('wall of dust'/'LEG', 'Summon — Wall').
card_original_text('wall of dust'/'LEG', 'Creatures blocked by Wall of Dust cannot attack during your opponent\'s next turn. Use counters to mark these creatures.').
card_first_print('wall of dust', 'LEG').
card_image_name('wall of dust'/'LEG', 'wall of dust').
card_uid('wall of dust'/'LEG', 'LEG:Wall of Dust:wall of dust').
card_rarity('wall of dust'/'LEG', 'Uncommon').
card_artist('wall of dust'/'LEG', 'Richard Thomas').
card_flavor_text('wall of dust'/'LEG', 'An ever-moving swarm of dust engulfs and disorients anything that comes near.').
card_multiverse_id('wall of dust'/'LEG', '1594').

card_in_set('wall of earth', 'LEG').
card_original_type('wall of earth'/'LEG', 'Summon — Wall').
card_original_text('wall of earth'/'LEG', '').
card_first_print('wall of earth', 'LEG').
card_image_name('wall of earth'/'LEG', 'wall of earth').
card_uid('wall of earth'/'LEG', 'LEG:Wall of Earth:wall of earth').
card_rarity('wall of earth'/'LEG', 'Common').
card_artist('wall of earth'/'LEG', 'Richard Thomas').
card_flavor_text('wall of earth'/'LEG', 'The ground shuddered violently and the earth seemed to come to life. The elemental force contained in the vast wall of earth was trapped, bent to its controller\'s will.').
card_multiverse_id('wall of earth'/'LEG', '1595').

card_in_set('wall of heat', 'LEG').
card_original_type('wall of heat'/'LEG', 'Summon — Wall').
card_original_text('wall of heat'/'LEG', '').
card_first_print('wall of heat', 'LEG').
card_image_name('wall of heat'/'LEG', 'wall of heat').
card_uid('wall of heat'/'LEG', 'LEG:Wall of Heat:wall of heat').
card_rarity('wall of heat'/'LEG', 'Common').
card_artist('wall of heat'/'LEG', 'Richard Thomas').
card_flavor_text('wall of heat'/'LEG', 'At a distance, we mistook the sound for a waterfall . . .').
card_multiverse_id('wall of heat'/'LEG', '1596').

card_in_set('wall of light', 'LEG').
card_original_type('wall of light'/'LEG', 'Summon — Wall').
card_original_text('wall of light'/'LEG', 'Protection from black').
card_first_print('wall of light', 'LEG').
card_image_name('wall of light'/'LEG', 'wall of light').
card_uid('wall of light'/'LEG', 'LEG:Wall of Light:wall of light').
card_rarity('wall of light'/'LEG', 'Uncommon').
card_artist('wall of light'/'LEG', 'Richard Thomas').
card_flavor_text('wall of light'/'LEG', 'As many attackers were dazzled by the wall\'s beauty as were halted by its force.').
card_multiverse_id('wall of light'/'LEG', '1641').

card_in_set('wall of opposition', 'LEG').
card_original_type('wall of opposition'/'LEG', 'Summon — Wall').
card_original_text('wall of opposition'/'LEG', '{1}: +1/+0 until end of turn.').
card_first_print('wall of opposition', 'LEG').
card_image_name('wall of opposition'/'LEG', 'wall of opposition').
card_uid('wall of opposition'/'LEG', 'LEG:Wall of Opposition:wall of opposition').
card_rarity('wall of opposition'/'LEG', 'Rare').
card_artist('wall of opposition'/'LEG', 'Harold McNeill').
card_flavor_text('wall of opposition'/'LEG', 'Like so many obstacles in life, the Wall of Opposition is but an illusion, held fast by the focus and belief of the one who creates it.').
card_multiverse_id('wall of opposition'/'LEG', '1597').

card_in_set('wall of putrid flesh', 'LEG').
card_original_type('wall of putrid flesh'/'LEG', 'Summon — Wall').
card_original_text('wall of putrid flesh'/'LEG', 'Protection from white\nDamage done to wall by creatures with enchantment cards played on them is reduced to 0.').
card_first_print('wall of putrid flesh', 'LEG').
card_image_name('wall of putrid flesh'/'LEG', 'wall of putrid flesh').
card_uid('wall of putrid flesh'/'LEG', 'LEG:Wall of Putrid Flesh:wall of putrid flesh').
card_rarity('wall of putrid flesh'/'LEG', 'Uncommon').
card_artist('wall of putrid flesh'/'LEG', 'Richard Thomas').
card_multiverse_id('wall of putrid flesh'/'LEG', '1467').

card_in_set('wall of shadows', 'LEG').
card_original_type('wall of shadows'/'LEG', 'Summon — Wall').
card_original_text('wall of shadows'/'LEG', 'Damage Wall of Shadows receives from creatures it blocks is reduced to 0. Effects that target only walls may not target Wall of Shadows.').
card_first_print('wall of shadows', 'LEG').
card_image_name('wall of shadows'/'LEG', 'wall of shadows').
card_uid('wall of shadows'/'LEG', 'LEG:Wall of Shadows:wall of shadows').
card_rarity('wall of shadows'/'LEG', 'Common').
card_artist('wall of shadows'/'LEG', 'Pete Venters').
card_multiverse_id('wall of shadows'/'LEG', '1468').

card_in_set('wall of tombstones', 'LEG').
card_original_type('wall of tombstones'/'LEG', 'Summon — Wall').
card_original_text('wall of tombstones'/'LEG', 'At the end of your upkeep, the * below is set to the number of creatures in your graveyard.').
card_first_print('wall of tombstones', 'LEG').
card_image_name('wall of tombstones'/'LEG', 'wall of tombstones').
card_uid('wall of tombstones'/'LEG', 'LEG:Wall of Tombstones:wall of tombstones').
card_rarity('wall of tombstones'/'LEG', 'Uncommon').
card_artist('wall of tombstones'/'LEG', 'Dan Frazier').
card_multiverse_id('wall of tombstones'/'LEG', '1469').

card_in_set('wall of vapor', 'LEG').
card_original_type('wall of vapor'/'LEG', 'Summon — Wall').
card_original_text('wall of vapor'/'LEG', 'Damage done to Wall of Vapor by creatures it blocks is reduced to 0.').
card_first_print('wall of vapor', 'LEG').
card_image_name('wall of vapor'/'LEG', 'wall of vapor').
card_uid('wall of vapor'/'LEG', 'LEG:Wall of Vapor:wall of vapor').
card_rarity('wall of vapor'/'LEG', 'Common').
card_artist('wall of vapor'/'LEG', 'Richard Thomas').
card_flavor_text('wall of vapor'/'LEG', '\"Walls of a castle are made out of stone,/ Walls of a house out of bricks or of wood./ My walls are made out of magic alone,/ Stronger than any that ever have stood.\" —Chrysoberyl Earthsdaughter, Incantations').
card_multiverse_id('wall of vapor'/'LEG', '1510').

card_in_set('wall of wonder', 'LEG').
card_original_type('wall of wonder'/'LEG', 'Summon — Wall').
card_original_text('wall of wonder'/'LEG', '{2}{U}{U} Gain +4/-4 and allow Wall of Wonder to attack this turn.').
card_first_print('wall of wonder', 'LEG').
card_image_name('wall of wonder'/'LEG', 'wall of wonder').
card_uid('wall of wonder'/'LEG', 'LEG:Wall of Wonder:wall of wonder').
card_rarity('wall of wonder'/'LEG', 'Uncommon').
card_artist('wall of wonder'/'LEG', 'Richard Thomas').
card_flavor_text('wall of wonder'/'LEG', 'So confusing is the Wall\'s appearance that few of its victims even see it move.').
card_multiverse_id('wall of wonder'/'LEG', '1511').

card_in_set('whirling dervish', 'LEG').
card_original_type('whirling dervish'/'LEG', 'Summon — Dervish').
card_original_text('whirling dervish'/'LEG', 'Protection from black\nGains +1/+1 (use counters) at the end of each turn in which it does damage to opponent.').
card_first_print('whirling dervish', 'LEG').
card_image_name('whirling dervish'/'LEG', 'whirling dervish').
card_uid('whirling dervish'/'LEG', 'LEG:Whirling Dervish:whirling dervish').
card_rarity('whirling dervish'/'LEG', 'Uncommon').
card_artist('whirling dervish'/'LEG', 'Susan Van Camp').
card_multiverse_id('whirling dervish'/'LEG', '1551').

card_in_set('white mana battery', 'LEG').
card_original_type('white mana battery'/'LEG', 'Artifact').
card_original_text('white mana battery'/'LEG', '{2}{T}: Put one counter on White Mana Battery.\n{T}: Add {W} to your mana pool. Remove as many counters as you wish. For each counter removed add {W} to your mana pool. This ability is played as an interrupt.').
card_first_print('white mana battery', 'LEG').
card_image_name('white mana battery'/'LEG', 'white mana battery').
card_uid('white mana battery'/'LEG', 'LEG:White Mana Battery:white mana battery').
card_rarity('white mana battery'/'LEG', 'Uncommon').
card_artist('white mana battery'/'LEG', 'Anthony Waters').
card_multiverse_id('white mana battery'/'LEG', '1426').

card_in_set('willow satyr', 'LEG').
card_original_type('willow satyr'/'LEG', 'Summon — Satyr').
card_original_text('willow satyr'/'LEG', '{T}: Gain control of target legend. If Willow Satyr becomes untapped, you lose control of this legend; you may choose not to untap Willow Satyr as normal. You also lose control of legend if Willow Satyr leaves play, if you lose control of Willow Satyr, or if the game ends.').
card_first_print('willow satyr', 'LEG').
card_image_name('willow satyr'/'LEG', 'willow satyr').
card_uid('willow satyr'/'LEG', 'LEG:Willow Satyr:willow satyr').
card_rarity('willow satyr'/'LEG', 'Rare').
card_artist('willow satyr'/'LEG', 'Jeff A. Menges').
card_multiverse_id('willow satyr'/'LEG', '1552').

card_in_set('winds of change', 'LEG').
card_original_type('winds of change'/'LEG', 'Sorcery').
card_original_text('winds of change'/'LEG', 'All players shuffle their hands into their libraries, and then draw the same number of cards they originally held.').
card_first_print('winds of change', 'LEG').
card_image_name('winds of change'/'LEG', 'winds of change').
card_uid('winds of change'/'LEG', 'LEG:Winds of Change:winds of change').
card_rarity('winds of change'/'LEG', 'Uncommon').
card_artist('winds of change'/'LEG', 'Justin Hampton').
card_flavor_text('winds of change'/'LEG', '\"‘Tis the set of sails, and not the gales,/ Which tells us the way to go.\" —Ella Wheeler Wilcox').
card_multiverse_id('winds of change'/'LEG', '1598').

card_in_set('winter blast', 'LEG').
card_original_type('winter blast'/'LEG', 'Sorcery').
card_original_text('winter blast'/'LEG', 'X target creatures become tapped. Winter Blast does 2 damage to each target creature that has flying.').
card_first_print('winter blast', 'LEG').
card_image_name('winter blast'/'LEG', 'winter blast').
card_uid('winter blast'/'LEG', 'LEG:Winter Blast:winter blast').
card_rarity('winter blast'/'LEG', 'Rare').
card_artist('winter blast'/'LEG', 'Kaja Foglio').
card_flavor_text('winter blast'/'LEG', '\"Blow, winds, and crack your cheeks! rage! blow!\" —William Shakespeare, King Lear').
card_multiverse_id('winter blast'/'LEG', '1553').

card_in_set('wolverine pack', 'LEG').
card_original_type('wolverine pack'/'LEG', 'Summon — Wolverine Pack').
card_original_text('wolverine pack'/'LEG', 'Rampage: 2').
card_first_print('wolverine pack', 'LEG').
card_image_name('wolverine pack'/'LEG', 'wolverine pack').
card_uid('wolverine pack'/'LEG', 'LEG:Wolverine Pack:wolverine pack').
card_rarity('wolverine pack'/'LEG', 'Common').
card_artist('wolverine pack'/'LEG', 'Jeff A. Menges').
card_flavor_text('wolverine pack'/'LEG', '\"Give them great meals of beef and iron and steel, they will eat like wolves and fight like devils.\" —William Shakespeare, King Henry V').
card_multiverse_id('wolverine pack'/'LEG', '1554').

card_in_set('wood elemental', 'LEG').
card_original_type('wood elemental'/'LEG', 'Summon — Elemental').
card_original_text('wood elemental'/'LEG', '*s in the lower right-hand corner are set to the number of untapped forests you sacrifice when Wood Elemental is brought into play.').
card_first_print('wood elemental', 'LEG').
card_image_name('wood elemental'/'LEG', 'wood elemental').
card_uid('wood elemental'/'LEG', 'LEG:Wood Elemental:wood elemental').
card_rarity('wood elemental'/'LEG', 'Rare').
card_artist('wood elemental'/'LEG', 'Brian Snõddy').
card_multiverse_id('wood elemental'/'LEG', '1555').

card_in_set('xira arien', 'LEG').
card_original_type('xira arien'/'LEG', 'Summon — Legend').
card_original_text('xira arien'/'LEG', 'Flying\n{G}{R}{B}{T}: Target player draws one card.').
card_first_print('xira arien', 'LEG').
card_image_name('xira arien'/'LEG', 'xira arien').
card_uid('xira arien'/'LEG', 'LEG:Xira Arien:xira arien').
card_rarity('xira arien'/'LEG', 'Rare').
card_artist('xira arien'/'LEG', 'Melissa A. Benson').
card_flavor_text('xira arien'/'LEG', 'A regular guest at the Royal Masquerade, Arien is the envy of the Court. She appears in a new costume every hour.').
card_multiverse_id('xira arien'/'LEG', '1697').

card_in_set('zephyr falcon', 'LEG').
card_original_type('zephyr falcon'/'LEG', 'Summon — Falcon').
card_original_text('zephyr falcon'/'LEG', 'Flying\nAttacking does not cause Zephyr Falcon to tap.').
card_first_print('zephyr falcon', 'LEG').
card_image_name('zephyr falcon'/'LEG', 'zephyr falcon').
card_uid('zephyr falcon'/'LEG', 'LEG:Zephyr Falcon:zephyr falcon').
card_rarity('zephyr falcon'/'LEG', 'Common').
card_artist('zephyr falcon'/'LEG', 'Heather Hudson').
card_flavor_text('zephyr falcon'/'LEG', 'Although greatly prized among falconers, the Zephyr Falcon is capricious and not easily tamed.').
card_multiverse_id('zephyr falcon'/'LEG', '1512').
