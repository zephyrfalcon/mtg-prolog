% Happy Holidays

set('pHHO').
set_name('pHHO', 'Happy Holidays').
set_release_date('pHHO', '2006-12-31').
set_border('pHHO', 'silver').
set_type('pHHO', 'promo').

card_in_set('evil presents', 'pHHO').
card_original_type('evil presents'/'pHHO', 'Sorcery').
card_original_text('evil presents'/'pHHO', '').
card_first_print('evil presents', 'pHHO').
card_image_name('evil presents'/'pHHO', 'evil presents').
card_uid('evil presents'/'pHHO', 'pHHO:Evil Presents:evil presents').
card_rarity('evil presents'/'pHHO', 'Special').
card_artist('evil presents'/'pHHO', 'Paul Bonner').
card_number('evil presents'/'pHHO', '3').
card_flavor_text('evil presents'/'pHHO', '\'Tis better to give than to receive.').

card_in_set('fruitcake elemental', 'pHHO').
card_original_type('fruitcake elemental'/'pHHO', 'Creature — Elemental').
card_original_text('fruitcake elemental'/'pHHO', '').
card_first_print('fruitcake elemental', 'pHHO').
card_image_name('fruitcake elemental'/'pHHO', 'fruitcake elemental').
card_uid('fruitcake elemental'/'pHHO', 'pHHO:Fruitcake Elemental:fruitcake elemental').
card_rarity('fruitcake elemental'/'pHHO', 'Special').
card_artist('fruitcake elemental'/'pHHO', 'Darrell Riche').
card_number('fruitcake elemental'/'pHHO', '1').
card_flavor_text('fruitcake elemental'/'pHHO', 'Experiments intended to create the perfect holiday emissary resulted in a substance harder than darksteel and a fraction as merry.').

card_in_set('gifts given', 'pHHO').
card_original_type('gifts given'/'pHHO', 'Instant').
card_original_text('gifts given'/'pHHO', '').
card_first_print('gifts given', 'pHHO').
card_image_name('gifts given'/'pHHO', 'gifts given').
card_uid('gifts given'/'pHHO', 'pHHO:Gifts Given:gifts given').
card_rarity('gifts given'/'pHHO', 'Special').
card_artist('gifts given'/'pHHO', 'Jason Chan').
card_number('gifts given'/'pHHO', '2').
card_flavor_text('gifts given'/'pHHO', '\"Thanks! You shouldn\'t have.\"').

card_in_set('mishra\'s toy workshop', 'pHHO').
card_original_type('mishra\'s toy workshop'/'pHHO', 'Land').
card_original_text('mishra\'s toy workshop'/'pHHO', '').
card_first_print('mishra\'s toy workshop', 'pHHO').
card_image_name('mishra\'s toy workshop'/'pHHO', 'mishra\'s toy workshop').
card_uid('mishra\'s toy workshop'/'pHHO', 'pHHO:Mishra\'s Toy Workshop:mishra\'s toy workshop').
card_rarity('mishra\'s toy workshop'/'pHHO', 'Special').
card_artist('mishra\'s toy workshop'/'pHHO', 'Jung Park').
card_number('mishra\'s toy workshop'/'pHHO', '9').
card_flavor_text('mishra\'s toy workshop'/'pHHO', 'Even though Urza tried to apologize, Mishra continued to bear a grudge.').

card_in_set('naughty', 'pHHO').
card_original_type('naughty'/'pHHO', 'Sorcery').
card_original_text('naughty'/'pHHO', '').
card_first_print('naughty', 'pHHO').
card_image_name('naughty'/'pHHO', 'naughtynice').
card_uid('naughty'/'pHHO', 'pHHO:Naughty:naughtynice').
card_rarity('naughty'/'pHHO', 'Special').
card_artist('naughty'/'pHHO', 'Greg Staples').
card_number('naughty'/'pHHO', '7a').

card_in_set('nice', 'pHHO').
card_original_type('nice'/'pHHO', 'Sorcery').
card_original_text('nice'/'pHHO', '').
card_first_print('nice', 'pHHO').
card_image_name('nice'/'pHHO', 'naughtynice').
card_uid('nice'/'pHHO', 'pHHO:Nice:naughtynice').
card_rarity('nice'/'pHHO', 'Special').
card_artist('nice'/'pHHO', 'Greg Staples').
card_number('nice'/'pHHO', '7b').

card_in_set('season\'s beatings', 'pHHO').
card_original_type('season\'s beatings'/'pHHO', 'Sorcery').
card_original_text('season\'s beatings'/'pHHO', '').
card_first_print('season\'s beatings', 'pHHO').
card_image_name('season\'s beatings'/'pHHO', 'season\'s beatings').
card_uid('season\'s beatings'/'pHHO', 'pHHO:Season\'s Beatings:season\'s beatings').
card_rarity('season\'s beatings'/'pHHO', 'Special').
card_artist('season\'s beatings'/'pHHO', 'Kev Walker').
card_number('season\'s beatings'/'pHHO', '4').
card_flavor_text('season\'s beatings'/'pHHO', 'Arriving home, he suddenly longed for the bloodsoaked battlefields behind him.').

card_in_set('snow mercy', 'pHHO').
card_original_type('snow mercy'/'pHHO', 'Snow Enchantment').
card_original_text('snow mercy'/'pHHO', '').
card_first_print('snow mercy', 'pHHO').
card_image_name('snow mercy'/'pHHO', 'snow mercy').
card_uid('snow mercy'/'pHHO', 'pHHO:Snow Mercy:snow mercy').
card_rarity('snow mercy'/'pHHO', 'Special').
card_artist('snow mercy'/'pHHO', 'rk post').
card_number('snow mercy'/'pHHO', '5').
card_flavor_text('snow mercy'/'pHHO', 'WARNING: Shake gently. Contents may cause choking, hypothermia, and/or planar invasion.').

card_in_set('stocking tiger', 'pHHO').
card_original_type('stocking tiger'/'pHHO', 'Artifact Creature — Cat Construct').
card_original_text('stocking tiger'/'pHHO', '').
card_first_print('stocking tiger', 'pHHO').
card_image_name('stocking tiger'/'pHHO', 'stocking tiger').
card_uid('stocking tiger'/'pHHO', 'pHHO:Stocking Tiger:stocking tiger').
card_rarity('stocking tiger'/'pHHO', 'Special').
card_artist('stocking tiger'/'pHHO', 'Terese Nielsen').
card_number('stocking tiger'/'pHHO', '8').
card_flavor_text('stocking tiger'/'pHHO', 'Just hope yours isn\'t full of coal golems.').

card_in_set('yule ooze', 'pHHO').
card_original_type('yule ooze'/'pHHO', 'Creature — Ooze').
card_original_text('yule ooze'/'pHHO', '').
card_first_print('yule ooze', 'pHHO').
card_image_name('yule ooze'/'pHHO', 'yule ooze').
card_uid('yule ooze'/'pHHO', 'pHHO:Yule Ooze:yule ooze').
card_rarity('yule ooze'/'pHHO', 'Special').
card_artist('yule ooze'/'pHHO', 'Steve Prescott').
card_number('yule ooze'/'pHHO', '6').
card_flavor_text('yule ooze'/'pHHO', 'It loves having family for dinner.').
