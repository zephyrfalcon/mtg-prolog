% Fourth Edition

set('4ED').
set_name('4ED', 'Fourth Edition').
set_release_date('4ED', '1995-04-01').
set_border('4ED', 'white').
set_type('4ED', 'core').

card_in_set('abomination', '4ED').
card_original_type('abomination'/'4ED', 'Summon — Abomination').
card_original_text('abomination'/'4ED', 'At the end of combat, destroy all green and white creatures blocking or blocked by Abomination.').
card_image_name('abomination'/'4ED', 'abomination').
card_uid('abomination'/'4ED', '4ED:Abomination:abomination').
card_rarity('abomination'/'4ED', 'Uncommon').
card_artist('abomination'/'4ED', 'Mark Tedin').
card_multiverse_id('abomination'/'4ED', '2084').

card_in_set('air elemental', '4ED').
card_original_type('air elemental'/'4ED', 'Summon — Elemental').
card_original_text('air elemental'/'4ED', 'Flying').
card_image_name('air elemental'/'4ED', 'air elemental').
card_uid('air elemental'/'4ED', '4ED:Air Elemental:air elemental').
card_rarity('air elemental'/'4ED', 'Uncommon').
card_artist('air elemental'/'4ED', 'Richard Thomas').
card_flavor_text('air elemental'/'4ED', 'These spirits of the air are winsome and wild and cannot be truly contained. Only marginally intelligent, they often substitute whimsy for strategy, delighting in mischief and mayhem.').
card_multiverse_id('air elemental'/'4ED', '2142').

card_in_set('alabaster potion', '4ED').
card_original_type('alabaster potion'/'4ED', 'Instant').
card_original_text('alabaster potion'/'4ED', 'Give target player X life, or prevent X damage to any creature or player.').
card_image_name('alabaster potion'/'4ED', 'alabaster potion').
card_uid('alabaster potion'/'4ED', '4ED:Alabaster Potion:alabaster potion').
card_rarity('alabaster potion'/'4ED', 'Common').
card_artist('alabaster potion'/'4ED', 'Harold McNeill').
card_flavor_text('alabaster potion'/'4ED', '\"Healing is a matter of time, but it is sometimes also a matter of opportunity.\"\n—D\'Avenant proverb').
card_multiverse_id('alabaster potion'/'4ED', '2316').

card_in_set('aladdin\'s lamp', '4ED').
card_original_type('aladdin\'s lamp'/'4ED', 'Artifact').
card_original_text('aladdin\'s lamp'/'4ED', '{X}, {T}: Instead of drawing a card from the top of your library, draw X cards but choose only one to put into your hand. Shuffle the leftover cards and put them at the bottom of your library. X cannot be 0.').
card_image_name('aladdin\'s lamp'/'4ED', 'aladdin\'s lamp').
card_uid('aladdin\'s lamp'/'4ED', '4ED:Aladdin\'s Lamp:aladdin\'s lamp').
card_rarity('aladdin\'s lamp'/'4ED', 'Rare').
card_artist('aladdin\'s lamp'/'4ED', 'Mark Tedin').
card_multiverse_id('aladdin\'s lamp'/'4ED', '2014').

card_in_set('aladdin\'s ring', '4ED').
card_original_type('aladdin\'s ring'/'4ED', 'Artifact').
card_original_text('aladdin\'s ring'/'4ED', '{8}, {T}: Aladdin\'s Ring deals 4 damage to target creature or player.').
card_image_name('aladdin\'s ring'/'4ED', 'aladdin\'s ring').
card_uid('aladdin\'s ring'/'4ED', '4ED:Aladdin\'s Ring:aladdin\'s ring').
card_rarity('aladdin\'s ring'/'4ED', 'Rare').
card_artist('aladdin\'s ring'/'4ED', 'Dan Frazier').
card_flavor_text('aladdin\'s ring'/'4ED', '\"After these words the magician drew a ring off his finger, and put it on one of Aladdin\'s, saying: \'It is a talisman against all evil, so long as you obey me.\'\" —The Arabian Nights, Junior Classics trans.').
card_multiverse_id('aladdin\'s ring'/'4ED', '2015').

card_in_set('ali baba', '4ED').
card_original_type('ali baba'/'4ED', 'Summon — Ali Baba').
card_original_text('ali baba'/'4ED', '{R} Tap target wall.').
card_image_name('ali baba'/'4ED', 'ali baba').
card_uid('ali baba'/'4ED', '4ED:Ali Baba:ali baba').
card_rarity('ali baba'/'4ED', 'Uncommon').
card_artist('ali baba'/'4ED', 'Julie Baroh').
card_flavor_text('ali baba'/'4ED', '\"When he reached the entrance of the cavern, he pronounced the words, ‘Open, Sesame!\'\"\n—The Arabian Nights, Junior Classics trans.').
card_multiverse_id('ali baba'/'4ED', '2258').

card_in_set('amrou kithkin', '4ED').
card_original_type('amrou kithkin'/'4ED', 'Summon — Kithkin').
card_original_text('amrou kithkin'/'4ED', 'No creature with power greater than 2 may be assigned to block Kithkin.').
card_image_name('amrou kithkin'/'4ED', 'amrou kithkin').
card_uid('amrou kithkin'/'4ED', '4ED:Amrou Kithkin:amrou kithkin').
card_rarity('amrou kithkin'/'4ED', 'Common').
card_artist('amrou kithkin'/'4ED', 'Quinton Hoover').
card_flavor_text('amrou kithkin'/'4ED', 'Quick and agile, Amrou Kithkin can usually escape from even the most fearsome opponents.').
card_multiverse_id('amrou kithkin'/'4ED', '2317').

card_in_set('amulet of kroog', '4ED').
card_original_type('amulet of kroog'/'4ED', 'Artifact').
card_original_text('amulet of kroog'/'4ED', '{2}, {T}: Prevent 1 damage to any creature or player.').
card_image_name('amulet of kroog'/'4ED', 'amulet of kroog').
card_uid('amulet of kroog'/'4ED', '4ED:Amulet of Kroog:amulet of kroog').
card_rarity('amulet of kroog'/'4ED', 'Common').
card_artist('amulet of kroog'/'4ED', 'Margaret Organ-Kean').
card_flavor_text('amulet of kroog'/'4ED', 'Among the first allies Urza gained were the people of Kroog. As a sign of friendship, Urza gave the healers of the city potent amulets; afterwards, thousands journeyed to Kroog in hope of healing.').
card_multiverse_id('amulet of kroog'/'4ED', '2016').

card_in_set('angry mob', '4ED').
card_original_type('angry mob'/'4ED', 'Summon — Mob').
card_original_text('angry mob'/'4ED', 'Trample\nDuring your turn, Angry Mob has power and toughness each equal to 2 plus the total number of swamps opponents control. During other turns, Angry Mob has power and toughness 2/2.').
card_image_name('angry mob'/'4ED', 'angry mob').
card_uid('angry mob'/'4ED', '4ED:Angry Mob:angry mob').
card_rarity('angry mob'/'4ED', 'Uncommon').
card_artist('angry mob'/'4ED', 'Drew Tucker').
card_multiverse_id('angry mob'/'4ED', '2318').

card_in_set('animate artifact', '4ED').
card_original_type('animate artifact'/'4ED', 'Enchant Artifact').
card_original_text('animate artifact'/'4ED', 'Target artifact becomes an artifact creature with power and toughness each equal to its casting cost; target retains all its original abilities. Animate Artifact does not affect artifact creatures.').
card_image_name('animate artifact'/'4ED', 'animate artifact').
card_uid('animate artifact'/'4ED', '4ED:Animate Artifact:animate artifact').
card_rarity('animate artifact'/'4ED', 'Uncommon').
card_artist('animate artifact'/'4ED', 'Douglas Shuler').
card_multiverse_id('animate artifact'/'4ED', '2143').

card_in_set('animate dead', '4ED').
card_original_type('animate dead'/'4ED', 'Enchant Dead Creature').
card_original_text('animate dead'/'4ED', 'Take target creature from any graveyard and put it directly into play under your control with -1/-0. Treat this creature as though it were just summoned. If Animate Dead is removed, bury the creature in its owner\'s graveyard.').
card_image_name('animate dead'/'4ED', 'animate dead').
card_uid('animate dead'/'4ED', '4ED:Animate Dead:animate dead').
card_rarity('animate dead'/'4ED', 'Uncommon').
card_artist('animate dead'/'4ED', 'Anson Maddocks').
card_multiverse_id('animate dead'/'4ED', '2085').

card_in_set('animate wall', '4ED').
card_original_type('animate wall'/'4ED', 'Enchant Wall').
card_original_text('animate wall'/'4ED', 'Target wall can now attack.').
card_image_name('animate wall'/'4ED', 'animate wall').
card_uid('animate wall'/'4ED', '4ED:Animate Wall:animate wall').
card_rarity('animate wall'/'4ED', 'Rare').
card_artist('animate wall'/'4ED', 'Dan Frazier').
card_multiverse_id('animate wall'/'4ED', '2319').

card_in_set('ankh of mishra', '4ED').
card_original_type('ankh of mishra'/'4ED', 'Artifact').
card_original_text('ankh of mishra'/'4ED', 'Each time a player puts a land into play, Ankh of Mishra deals 2 damage to that player.').
card_image_name('ankh of mishra'/'4ED', 'ankh of mishra').
card_uid('ankh of mishra'/'4ED', '4ED:Ankh of Mishra:ankh of mishra').
card_rarity('ankh of mishra'/'4ED', 'Rare').
card_artist('ankh of mishra'/'4ED', 'Amy Weber').
card_multiverse_id('ankh of mishra'/'4ED', '2017').

card_in_set('apprentice wizard', '4ED').
card_original_type('apprentice wizard'/'4ED', 'Summon — Wizard').
card_original_text('apprentice wizard'/'4ED', '{U}, {T}: Add {3} to your mana pool. Play this ability as an interrupt.').
card_image_name('apprentice wizard'/'4ED', 'apprentice wizard').
card_uid('apprentice wizard'/'4ED', '4ED:Apprentice Wizard:apprentice wizard').
card_rarity('apprentice wizard'/'4ED', 'Common').
card_artist('apprentice wizard'/'4ED', 'Dan Frazier').
card_multiverse_id('apprentice wizard'/'4ED', '2144').

card_in_set('armageddon', '4ED').
card_original_type('armageddon'/'4ED', 'Sorcery').
card_original_text('armageddon'/'4ED', 'Destroy all lands.').
card_image_name('armageddon'/'4ED', 'armageddon').
card_uid('armageddon'/'4ED', '4ED:Armageddon:armageddon').
card_rarity('armageddon'/'4ED', 'Rare').
card_artist('armageddon'/'4ED', 'Jesper Myrfors').
card_multiverse_id('armageddon'/'4ED', '2320').

card_in_set('armageddon clock', '4ED').
card_original_type('armageddon clock'/'4ED', 'Artifact').
card_original_text('armageddon clock'/'4ED', 'During your upkeep, put one doom counter on Armageddon Clock. At the end of your upkeep, Armageddon Clock deals X damage to each player, where X is the number of doom counters on Armageddon Clock. During any upkeep, any player may pay {4} to remove a doom counter from Armageddon Clock.').
card_image_name('armageddon clock'/'4ED', 'armageddon clock').
card_uid('armageddon clock'/'4ED', '4ED:Armageddon Clock:armageddon clock').
card_rarity('armageddon clock'/'4ED', 'Rare').
card_artist('armageddon clock'/'4ED', 'Amy Weber').
card_multiverse_id('armageddon clock'/'4ED', '2018').

card_in_set('ashes to ashes', '4ED').
card_original_type('ashes to ashes'/'4ED', 'Sorcery').
card_original_text('ashes to ashes'/'4ED', 'Ashes to Ashes removes two target non-artifact creatures from the game and deals 5 damage to you.').
card_image_name('ashes to ashes'/'4ED', 'ashes to ashes').
card_uid('ashes to ashes'/'4ED', '4ED:Ashes to Ashes:ashes to ashes').
card_rarity('ashes to ashes'/'4ED', 'Uncommon').
card_artist('ashes to ashes'/'4ED', 'Drew Tucker').
card_flavor_text('ashes to ashes'/'4ED', '\"All rivers eventually run to the sea. My job is to sort out who goes first.\"\n—Maeveen O\'Donagh, Memoirs of a Soldier').
card_multiverse_id('ashes to ashes'/'4ED', '2086').

card_in_set('ashnod\'s battle gear', '4ED').
card_original_type('ashnod\'s battle gear'/'4ED', 'Artifact').
card_original_text('ashnod\'s battle gear'/'4ED', '{2}, {T}: Target creature you control gets +2/-2 as long as Ashnod\'s Battle Gear remains tapped. You may choose not to untap Ashnod\'s Battle Gear during your untap phase.').
card_image_name('ashnod\'s battle gear'/'4ED', 'ashnod\'s battle gear').
card_uid('ashnod\'s battle gear'/'4ED', '4ED:Ashnod\'s Battle Gear:ashnod\'s battle gear').
card_rarity('ashnod\'s battle gear'/'4ED', 'Uncommon').
card_artist('ashnod\'s battle gear'/'4ED', 'Mark Poole').
card_flavor_text('ashnod\'s battle gear'/'4ED', 'This invention shows why Ashnod was feared by her troops as well as her foes.').
card_multiverse_id('ashnod\'s battle gear'/'4ED', '2019').

card_in_set('aspect of wolf', '4ED').
card_original_type('aspect of wolf'/'4ED', 'Enchant Creature').
card_original_text('aspect of wolf'/'4ED', 'Increase target creature\'s power and toughness by half the number of forests you control, rounding down for power and up for toughness.').
card_image_name('aspect of wolf'/'4ED', 'aspect of wolf').
card_uid('aspect of wolf'/'4ED', '4ED:Aspect of Wolf:aspect of wolf').
card_rarity('aspect of wolf'/'4ED', 'Rare').
card_artist('aspect of wolf'/'4ED', 'Jeff A. Menges').
card_multiverse_id('aspect of wolf'/'4ED', '2200').

card_in_set('backfire', '4ED').
card_original_type('backfire'/'4ED', 'Enchant Creature').
card_original_text('backfire'/'4ED', 'Backfire deals 1 damage to target creature\'s controller for each 1 damage dealt to you by that creature.').
card_image_name('backfire'/'4ED', 'backfire').
card_uid('backfire'/'4ED', '4ED:Backfire:backfire').
card_rarity('backfire'/'4ED', 'Uncommon').
card_artist('backfire'/'4ED', 'Brian Snõddy').
card_multiverse_id('backfire'/'4ED', '2145').

card_in_set('bad moon', '4ED').
card_original_type('bad moon'/'4ED', 'Enchantment').
card_original_text('bad moon'/'4ED', 'All black creatures get +1/+1.').
card_image_name('bad moon'/'4ED', 'bad moon').
card_uid('bad moon'/'4ED', '4ED:Bad Moon:bad moon').
card_rarity('bad moon'/'4ED', 'Rare').
card_artist('bad moon'/'4ED', 'Jesper Myrfors').
card_multiverse_id('bad moon'/'4ED', '2087').

card_in_set('balance', '4ED').
card_original_type('balance'/'4ED', 'Sorcery').
card_original_text('balance'/'4ED', 'Each player sacrifices enough lands to equalize the number of lands all players control. The player who controls the fewest lands cannot sacrifice any in this way. All players then equalize cards in hand and then creatures in play in the same way.').
card_image_name('balance'/'4ED', 'balance').
card_uid('balance'/'4ED', '4ED:Balance:balance').
card_rarity('balance'/'4ED', 'Rare').
card_artist('balance'/'4ED', 'Mark Poole').
card_multiverse_id('balance'/'4ED', '2321').

card_in_set('ball lightning', '4ED').
card_original_type('ball lightning'/'4ED', 'Summon — Ball Lightning').
card_original_text('ball lightning'/'4ED', 'Trample\nBall Lightning can attack the turn it comes into play. At the end of any turn, bury Ball Lightning.').
card_image_name('ball lightning'/'4ED', 'ball lightning').
card_uid('ball lightning'/'4ED', '4ED:Ball Lightning:ball lightning').
card_rarity('ball lightning'/'4ED', 'Rare').
card_artist('ball lightning'/'4ED', 'Quinton Hoover').
card_multiverse_id('ball lightning'/'4ED', '2259').

card_in_set('battering ram', '4ED').
card_original_type('battering ram'/'4ED', 'Artifact Creature').
card_original_text('battering ram'/'4ED', 'Banding when attacking\nAt the end of combat, destroy all walls blocking Battering Ram.').
card_image_name('battering ram'/'4ED', 'battering ram').
card_uid('battering ram'/'4ED', '4ED:Battering Ram:battering ram').
card_rarity('battering ram'/'4ED', 'Common').
card_artist('battering ram'/'4ED', 'Jeff A. Menges').
card_flavor_text('battering ram'/'4ED', 'By the time Mishra was defeated, no mage was foolish enough to rely heavily on walls.').
card_multiverse_id('battering ram'/'4ED', '2020').

card_in_set('benalish hero', '4ED').
card_original_type('benalish hero'/'4ED', 'Summon — Hero').
card_original_text('benalish hero'/'4ED', 'Banding').
card_image_name('benalish hero'/'4ED', 'benalish hero').
card_uid('benalish hero'/'4ED', '4ED:Benalish Hero:benalish hero').
card_rarity('benalish hero'/'4ED', 'Common').
card_artist('benalish hero'/'4ED', 'Douglas Shuler').
card_flavor_text('benalish hero'/'4ED', 'Benalia has a complex caste system that changes with the lunar year. No matter what the season, the only caste that cannot be attained by either heredity or money is that of the Hero.').
card_multiverse_id('benalish hero'/'4ED', '2322').

card_in_set('bird maiden', '4ED').
card_original_type('bird maiden'/'4ED', 'Summon — Bird Maiden').
card_original_text('bird maiden'/'4ED', 'Flying').
card_image_name('bird maiden'/'4ED', 'bird maiden').
card_uid('bird maiden'/'4ED', '4ED:Bird Maiden:bird maiden').
card_rarity('bird maiden'/'4ED', 'Common').
card_artist('bird maiden'/'4ED', 'Kaja Foglio').
card_flavor_text('bird maiden'/'4ED', 'Four things that never meet do here unite\nTo shed my blood and to ravage my heart,\nA radiant brow and tresses that beguile\nAnd rosy cheeks and a glittering smile.\"\n—The Arabian Nights, trans. Haddawy').
card_multiverse_id('bird maiden'/'4ED', '2260').

card_in_set('birds of paradise', '4ED').
card_original_type('birds of paradise'/'4ED', 'Summon — Mana Birds').
card_original_text('birds of paradise'/'4ED', 'Flying\n{T}: Add one mana of any color to your mana pool. Play this ability as an interrupt.').
card_image_name('birds of paradise'/'4ED', 'birds of paradise').
card_uid('birds of paradise'/'4ED', '4ED:Birds of Paradise:birds of paradise').
card_rarity('birds of paradise'/'4ED', 'Rare').
card_artist('birds of paradise'/'4ED', 'Mark Poole').
card_multiverse_id('birds of paradise'/'4ED', '2201').

card_in_set('black knight', '4ED').
card_original_type('black knight'/'4ED', 'Summon — Knight').
card_original_text('black knight'/'4ED', 'Protection from white, first strike').
card_image_name('black knight'/'4ED', 'black knight').
card_uid('black knight'/'4ED', '4ED:Black Knight:black knight').
card_rarity('black knight'/'4ED', 'Uncommon').
card_artist('black knight'/'4ED', 'Jeff A. Menges').
card_flavor_text('black knight'/'4ED', 'Battle doesn\'t need a purpose; the battle is its own purpose. You don\'t ask why a plague spreads or a field burns. Don\'t ask why I fight.').
card_multiverse_id('black knight'/'4ED', '2088').

card_in_set('black mana battery', '4ED').
card_original_type('black mana battery'/'4ED', 'Artifact').
card_original_text('black mana battery'/'4ED', '{2}, {T}: Put one charge counter on Black Mana Battery.\n{T}: Add {B} to your mana pool and remove as many charge counters as you wish. For each charge counter removed from Black Mana Battery, add {B} to your mana pool. Play this ability as an interrupt.').
card_image_name('black mana battery'/'4ED', 'black mana battery').
card_uid('black mana battery'/'4ED', '4ED:Black Mana Battery:black mana battery').
card_rarity('black mana battery'/'4ED', 'Rare').
card_artist('black mana battery'/'4ED', 'Anson Maddocks').
card_multiverse_id('black mana battery'/'4ED', '2021').

card_in_set('black vise', '4ED').
card_original_type('black vise'/'4ED', 'Artifact').
card_original_text('black vise'/'4ED', 'At the end of target opponent\'s upkeep, Black Vise deals that player 1 damage for each card in his or her hand in excess of four.').
card_image_name('black vise'/'4ED', 'black vise').
card_uid('black vise'/'4ED', '4ED:Black Vise:black vise').
card_rarity('black vise'/'4ED', 'Uncommon').
card_artist('black vise'/'4ED', 'Richard Thomas').
card_multiverse_id('black vise'/'4ED', '2022').

card_in_set('black ward', '4ED').
card_original_type('black ward'/'4ED', 'Enchant Creature').
card_original_text('black ward'/'4ED', 'Target creature gains protection from black. The protection granted by Black Ward does not destroy Black Ward.').
card_image_name('black ward'/'4ED', 'black ward').
card_uid('black ward'/'4ED', '4ED:Black Ward:black ward').
card_rarity('black ward'/'4ED', 'Uncommon').
card_artist('black ward'/'4ED', 'Dan Frazier').
card_multiverse_id('black ward'/'4ED', '2323').

card_in_set('blessing', '4ED').
card_original_type('blessing'/'4ED', 'Enchant Creature').
card_original_text('blessing'/'4ED', '{W} Target creature Blessing enchants gets +1/+1 until end of turn.').
card_image_name('blessing'/'4ED', 'blessing').
card_uid('blessing'/'4ED', '4ED:Blessing:blessing').
card_rarity('blessing'/'4ED', 'Rare').
card_artist('blessing'/'4ED', 'Julie Baroh').
card_multiverse_id('blessing'/'4ED', '2324').

card_in_set('blight', '4ED').
card_original_type('blight'/'4ED', 'Enchant Land').
card_original_text('blight'/'4ED', 'If target land becomes tapped, destroy it at end of turn.').
card_image_name('blight'/'4ED', 'blight').
card_uid('blight'/'4ED', '4ED:Blight:blight').
card_rarity('blight'/'4ED', 'Uncommon').
card_artist('blight'/'4ED', 'Pete Venters').
card_multiverse_id('blight'/'4ED', '2089').

card_in_set('blood lust', '4ED').
card_original_type('blood lust'/'4ED', 'Instant').
card_original_text('blood lust'/'4ED', 'Target creature gets +4/-4 until end of turn. If this reduces creature\'s toughness to less than 1, creature\'s toughness becomes 1.').
card_image_name('blood lust'/'4ED', 'blood lust').
card_uid('blood lust'/'4ED', '4ED:Blood Lust:blood lust').
card_rarity('blood lust'/'4ED', 'Common').
card_artist('blood lust'/'4ED', 'Anson Maddocks').
card_multiverse_id('blood lust'/'4ED', '2261').

card_in_set('blue elemental blast', '4ED').
card_original_type('blue elemental blast'/'4ED', 'Interrupt').
card_original_text('blue elemental blast'/'4ED', 'Counter target red spell or destroy target red permanent.').
card_image_name('blue elemental blast'/'4ED', 'blue elemental blast').
card_uid('blue elemental blast'/'4ED', '4ED:Blue Elemental Blast:blue elemental blast').
card_rarity('blue elemental blast'/'4ED', 'Common').
card_artist('blue elemental blast'/'4ED', 'Richard Thomas').
card_multiverse_id('blue elemental blast'/'4ED', '2146').

card_in_set('blue mana battery', '4ED').
card_original_type('blue mana battery'/'4ED', 'Artifact').
card_original_text('blue mana battery'/'4ED', '{2}, {T}: Put one charge counter on Blue Mana Battery.\n{T}: Add {U} to your mana pool and remove as many charge counters as you wish. For each charge counter removed from Blue Mana Battery, add {U} to your mana pool. Play this ability as an interrupt.').
card_image_name('blue mana battery'/'4ED', 'blue mana battery').
card_uid('blue mana battery'/'4ED', '4ED:Blue Mana Battery:blue mana battery').
card_rarity('blue mana battery'/'4ED', 'Rare').
card_artist('blue mana battery'/'4ED', 'Amy Weber').
card_multiverse_id('blue mana battery'/'4ED', '2023').

card_in_set('blue ward', '4ED').
card_original_type('blue ward'/'4ED', 'Enchant Creature').
card_original_text('blue ward'/'4ED', 'Target creature gains protection from blue. The protection granted by Blue Ward does not destroy Blue Ward.').
card_image_name('blue ward'/'4ED', 'blue ward').
card_uid('blue ward'/'4ED', '4ED:Blue Ward:blue ward').
card_rarity('blue ward'/'4ED', 'Uncommon').
card_artist('blue ward'/'4ED', 'Dan Frazier').
card_multiverse_id('blue ward'/'4ED', '2325').

card_in_set('bog imp', '4ED').
card_original_type('bog imp'/'4ED', 'Summon — Imp').
card_original_text('bog imp'/'4ED', 'Flying').
card_image_name('bog imp'/'4ED', 'bog imp').
card_uid('bog imp'/'4ED', '4ED:Bog Imp:bog imp').
card_rarity('bog imp'/'4ED', 'Common').
card_artist('bog imp'/'4ED', 'Ron Spencer').
card_flavor_text('bog imp'/'4ED', 'On guard for larger dangers, we underestimated the power and speed of the Imp\'s muck-crusted claws.').
card_multiverse_id('bog imp'/'4ED', '2090').

card_in_set('bog wraith', '4ED').
card_original_type('bog wraith'/'4ED', 'Summon — Wraith').
card_original_text('bog wraith'/'4ED', 'Swampwalk').
card_image_name('bog wraith'/'4ED', 'bog wraith').
card_uid('bog wraith'/'4ED', '4ED:Bog Wraith:bog wraith').
card_rarity('bog wraith'/'4ED', 'Uncommon').
card_artist('bog wraith'/'4ED', 'Jeff A. Menges').
card_flavor_text('bog wraith'/'4ED', '\'Twas in the bogs of Cannelbrae\nMy mate did meet an early grave\n\'Twas nothing left for us to save\nIn the peat-filled bogs of Cannelbrae.').
card_multiverse_id('bog wraith'/'4ED', '2091').

card_in_set('bottle of suleiman', '4ED').
card_original_type('bottle of suleiman'/'4ED', 'Artifact').
card_original_text('bottle of suleiman'/'4ED', '{1}: Sacrifice Bottle of Suleiman. Flip a coin; target opponent calls heads or tails while coin is in the air. If the flip ends up in opponent\'s favor, Bottle of Suleiman deals 5 damage to you. Otherwise, put a Djinn token into play. Treat this token as a 5/5 artifact creature with flying.').
card_image_name('bottle of suleiman'/'4ED', 'bottle of suleiman').
card_uid('bottle of suleiman'/'4ED', '4ED:Bottle of Suleiman:bottle of suleiman').
card_rarity('bottle of suleiman'/'4ED', 'Rare').
card_artist('bottle of suleiman'/'4ED', 'Jesper Myrfors').
card_multiverse_id('bottle of suleiman'/'4ED', '2024').

card_in_set('brainwash', '4ED').
card_original_type('brainwash'/'4ED', 'Enchant Creature').
card_original_text('brainwash'/'4ED', 'Target creature cannot attack unless its controller pays an additional {3}.').
card_image_name('brainwash'/'4ED', 'brainwash').
card_uid('brainwash'/'4ED', '4ED:Brainwash:brainwash').
card_rarity('brainwash'/'4ED', 'Common').
card_artist('brainwash'/'4ED', 'Pete Venters').
card_flavor_text('brainwash'/'4ED', '\"They\'re not your friends; they despise you. I\'m the only one you can count on. Trust me.\"').
card_multiverse_id('brainwash'/'4ED', '2326').

card_in_set('brass man', '4ED').
card_original_type('brass man'/'4ED', 'Artifact Creature').
card_original_text('brass man'/'4ED', 'Brass Man does not untap during your untap phase.\n{1}: Untap Brass Man. Use this ability only during your upkeep.').
card_image_name('brass man'/'4ED', 'brass man').
card_uid('brass man'/'4ED', '4ED:Brass Man:brass man').
card_rarity('brass man'/'4ED', 'Uncommon').
card_artist('brass man'/'4ED', 'Christopher Rush').
card_multiverse_id('brass man'/'4ED', '2025').

card_in_set('bronze tablet', '4ED').
card_original_type('bronze tablet'/'4ED', 'Artifact').
card_original_text('bronze tablet'/'4ED', 'Comes into play tapped.\n{4}, {T}: Remove Bronze Tablet and target card opponent owns from the game. You become owner of opponent\'s card, and opponent becomes owner of Bronze Tablet. Opponent may prevent this exchange by paying 10 life; if he or she does so, destroy Bronze Tablet. Effects that prevent or redirect damage cannot be used to counter this loss of life. Play this ability as an interrupt.\nRemove Bronze Tablet from your deck before playing if not playing for ante.').
card_image_name('bronze tablet'/'4ED', 'bronze tablet').
card_uid('bronze tablet'/'4ED', '4ED:Bronze Tablet:bronze tablet').
card_rarity('bronze tablet'/'4ED', 'Rare').
card_artist('bronze tablet'/'4ED', 'Tom Wänerstrand').
card_multiverse_id('bronze tablet'/'4ED', '2026').

card_in_set('brothers of fire', '4ED').
card_original_type('brothers of fire'/'4ED', 'Summon — Brothers').
card_original_text('brothers of fire'/'4ED', '1{R}{R} Brothers of Fire deals 1 damage to target creature or player and 1 damage to you.').
card_image_name('brothers of fire'/'4ED', 'brothers of fire').
card_uid('brothers of fire'/'4ED', '4ED:Brothers of Fire:brothers of fire').
card_rarity('brothers of fire'/'4ED', 'Common').
card_artist('brothers of fire'/'4ED', 'Mark Tedin').
card_flavor_text('brothers of fire'/'4ED', 'Fire is never a gentle master.').
card_multiverse_id('brothers of fire'/'4ED', '2262').

card_in_set('burrowing', '4ED').
card_original_type('burrowing'/'4ED', 'Enchant Creature').
card_original_text('burrowing'/'4ED', 'Target creature gains mountainwalk.').
card_image_name('burrowing'/'4ED', 'burrowing').
card_uid('burrowing'/'4ED', '4ED:Burrowing:burrowing').
card_rarity('burrowing'/'4ED', 'Uncommon').
card_artist('burrowing'/'4ED', 'Mark Poole').
card_multiverse_id('burrowing'/'4ED', '2263').

card_in_set('carnivorous plant', '4ED').
card_original_type('carnivorous plant'/'4ED', 'Summon — Wall').
card_original_text('carnivorous plant'/'4ED', '').
card_image_name('carnivorous plant'/'4ED', 'carnivorous plant').
card_uid('carnivorous plant'/'4ED', '4ED:Carnivorous Plant:carnivorous plant').
card_rarity('carnivorous plant'/'4ED', 'Common').
card_artist('carnivorous plant'/'4ED', 'Quinton Hoover').
card_flavor_text('carnivorous plant'/'4ED', '\"It had a mouth like that of a great beast, and gnashed its teeth as it strained to reach us. I am thankful it possessed no means of locomotion.\"\n—Vervamon the Elder').
card_multiverse_id('carnivorous plant'/'4ED', '2202').

card_in_set('carrion ants', '4ED').
card_original_type('carrion ants'/'4ED', 'Summon — Ants').
card_original_text('carrion ants'/'4ED', '{1}: +1/+1 until end of turn').
card_image_name('carrion ants'/'4ED', 'carrion ants').
card_uid('carrion ants'/'4ED', '4ED:Carrion Ants:carrion ants').
card_rarity('carrion ants'/'4ED', 'Uncommon').
card_artist('carrion ants'/'4ED', 'Richard Thomas').
card_flavor_text('carrion ants'/'4ED', '\"War is no picnic,\" my father liked to say. But the Ants seemed to disagree.').
card_multiverse_id('carrion ants'/'4ED', '2092').

card_in_set('castle', '4ED').
card_original_type('castle'/'4ED', 'Enchantment').
card_original_text('castle'/'4ED', 'Untapped creatures you control get +0/+2 when not attacking.').
card_image_name('castle'/'4ED', 'castle').
card_uid('castle'/'4ED', '4ED:Castle:castle').
card_rarity('castle'/'4ED', 'Uncommon').
card_artist('castle'/'4ED', 'Dameon Willich').
card_multiverse_id('castle'/'4ED', '2327').

card_in_set('cave people', '4ED').
card_original_type('cave people'/'4ED', 'Summon — Cave People').
card_original_text('cave people'/'4ED', 'When attacking, Cave People gets +1/-2 until end of turn.\n1{R}{R}, {T}: Target creature gains mountainwalk until end of turn.').
card_image_name('cave people'/'4ED', 'cave people').
card_uid('cave people'/'4ED', '4ED:Cave People:cave people').
card_rarity('cave people'/'4ED', 'Uncommon').
card_artist('cave people'/'4ED', 'Drew Tucker').
card_multiverse_id('cave people'/'4ED', '2264').

card_in_set('celestial prism', '4ED').
card_original_type('celestial prism'/'4ED', 'Artifact').
card_original_text('celestial prism'/'4ED', '{2}, {T}: Add one mana of any color to your mana pool. Play this ability as an interrupt.').
card_image_name('celestial prism'/'4ED', 'celestial prism').
card_uid('celestial prism'/'4ED', '4ED:Celestial Prism:celestial prism').
card_rarity('celestial prism'/'4ED', 'Uncommon').
card_artist('celestial prism'/'4ED', 'Amy Weber').
card_multiverse_id('celestial prism'/'4ED', '2027').

card_in_set('channel', '4ED').
card_original_type('channel'/'4ED', 'Sorcery').
card_original_text('channel'/'4ED', 'Until end of turn, you may add colorless mana to your mana pool at a cost of 1 life per one mana. Play these additions as interrupts. Effects that prevent or redirect damage cannot be used to counter this loss of life.').
card_image_name('channel'/'4ED', 'channel').
card_uid('channel'/'4ED', '4ED:Channel:channel').
card_rarity('channel'/'4ED', 'Uncommon').
card_artist('channel'/'4ED', 'Richard Thomas').
card_multiverse_id('channel'/'4ED', '2203').

card_in_set('chaoslace', '4ED').
card_original_type('chaoslace'/'4ED', 'Interrupt').
card_original_text('chaoslace'/'4ED', 'Change the color of target spell or target permanent to red. Costs to cast, tap, maintain, or use a special ability of target remain unchanged.').
card_image_name('chaoslace'/'4ED', 'chaoslace').
card_uid('chaoslace'/'4ED', '4ED:Chaoslace:chaoslace').
card_rarity('chaoslace'/'4ED', 'Rare').
card_artist('chaoslace'/'4ED', 'Dameon Willich').
card_multiverse_id('chaoslace'/'4ED', '2265').

card_in_set('circle of protection: artifacts', '4ED').
card_original_type('circle of protection: artifacts'/'4ED', 'Enchantment').
card_original_text('circle of protection: artifacts'/'4ED', '{2}: Prevent all damage against you from one artifact source. If a source deals damage to you more than once in a turn, you may pay {2} each time to prevent the damage.').
card_image_name('circle of protection: artifacts'/'4ED', 'circle of protection artifacts').
card_uid('circle of protection: artifacts'/'4ED', '4ED:Circle of Protection: Artifacts:circle of protection artifacts').
card_rarity('circle of protection: artifacts'/'4ED', 'Uncommon').
card_artist('circle of protection: artifacts'/'4ED', 'Pete Venters').
card_multiverse_id('circle of protection: artifacts'/'4ED', '2328').

card_in_set('circle of protection: black', '4ED').
card_original_type('circle of protection: black'/'4ED', 'Enchantment').
card_original_text('circle of protection: black'/'4ED', '{1}: Prevent all damage against you from one black source. If a source deals damage to you more than once in a turn, you may pay {1} each time to prevent the damage.').
card_image_name('circle of protection: black'/'4ED', 'circle of protection black').
card_uid('circle of protection: black'/'4ED', '4ED:Circle of Protection: Black:circle of protection black').
card_rarity('circle of protection: black'/'4ED', 'Common').
card_artist('circle of protection: black'/'4ED', 'Jesper Myrfors').
card_multiverse_id('circle of protection: black'/'4ED', '2329').

card_in_set('circle of protection: blue', '4ED').
card_original_type('circle of protection: blue'/'4ED', 'Enchantment').
card_original_text('circle of protection: blue'/'4ED', '{1}: Prevent all damage against you from one blue source. If a source deals damage to you more than once in a turn, you may pay {1} each time to prevent the damage.').
card_image_name('circle of protection: blue'/'4ED', 'circle of protection blue').
card_uid('circle of protection: blue'/'4ED', '4ED:Circle of Protection: Blue:circle of protection blue').
card_rarity('circle of protection: blue'/'4ED', 'Common').
card_artist('circle of protection: blue'/'4ED', 'Dameon Willich').
card_multiverse_id('circle of protection: blue'/'4ED', '2330').

card_in_set('circle of protection: green', '4ED').
card_original_type('circle of protection: green'/'4ED', 'Enchantment').
card_original_text('circle of protection: green'/'4ED', '{1}: Prevent all damage against you from one green source. If a source deals damage to you more than once in a turn, you may pay {1} each time to prevent the damage.').
card_image_name('circle of protection: green'/'4ED', 'circle of protection green').
card_uid('circle of protection: green'/'4ED', '4ED:Circle of Protection: Green:circle of protection green').
card_rarity('circle of protection: green'/'4ED', 'Common').
card_artist('circle of protection: green'/'4ED', 'Sandra Everingham').
card_multiverse_id('circle of protection: green'/'4ED', '2331').

card_in_set('circle of protection: red', '4ED').
card_original_type('circle of protection: red'/'4ED', 'Enchantment').
card_original_text('circle of protection: red'/'4ED', '{1}: Prevent all damage against you from one red source. If a source deals damage to you more than once in a turn, you may pay {1} each time to prevent the damage.').
card_image_name('circle of protection: red'/'4ED', 'circle of protection red').
card_uid('circle of protection: red'/'4ED', '4ED:Circle of Protection: Red:circle of protection red').
card_rarity('circle of protection: red'/'4ED', 'Common').
card_artist('circle of protection: red'/'4ED', 'Mark Tedin').
card_multiverse_id('circle of protection: red'/'4ED', '2332').

card_in_set('circle of protection: white', '4ED').
card_original_type('circle of protection: white'/'4ED', 'Enchantment').
card_original_text('circle of protection: white'/'4ED', '{1}: Prevent all damage against you from one white source. If a source deals damage to you more than once in a turn, you may pay {1} each time to prevent the damage.').
card_image_name('circle of protection: white'/'4ED', 'circle of protection white').
card_uid('circle of protection: white'/'4ED', '4ED:Circle of Protection: White:circle of protection white').
card_rarity('circle of protection: white'/'4ED', 'Common').
card_artist('circle of protection: white'/'4ED', 'Douglas Shuler').
card_multiverse_id('circle of protection: white'/'4ED', '2333').

card_in_set('clay statue', '4ED').
card_original_type('clay statue'/'4ED', 'Artifact Creature').
card_original_text('clay statue'/'4ED', '{2}: Regenerate').
card_image_name('clay statue'/'4ED', 'clay statue').
card_uid('clay statue'/'4ED', '4ED:Clay Statue:clay statue').
card_rarity('clay statue'/'4ED', 'Common').
card_artist('clay statue'/'4ED', 'Jesper Myrfors').
card_flavor_text('clay statue'/'4ED', 'Tawnos won fame as Urza\'s greatest assistant. After he created these warriors, Urza ended his apprenticeship, promoting him directly to the rank of master.').
card_multiverse_id('clay statue'/'4ED', '2028').

card_in_set('clockwork avian', '4ED').
card_original_type('clockwork avian'/'4ED', 'Artifact Creature').
card_original_text('clockwork avian'/'4ED', 'Flying\nWhen Clockwork Avian comes into play, put four +1/+0 counters on it. At the end of any combat in which Clockwork Avian is assigned to attack or block, remove a counter.\n{X}, {T}: Put X +1/+0 counters on Clockwork Avian. You may have no more than four of these counters on Clockwork Avian. Use this ability only during your upkeep.').
card_image_name('clockwork avian'/'4ED', 'clockwork avian').
card_uid('clockwork avian'/'4ED', '4ED:Clockwork Avian:clockwork avian').
card_rarity('clockwork avian'/'4ED', 'Rare').
card_artist('clockwork avian'/'4ED', 'Randy Asplund-Faith').
card_multiverse_id('clockwork avian'/'4ED', '2029').

card_in_set('clockwork beast', '4ED').
card_original_type('clockwork beast'/'4ED', 'Artifact Creature').
card_original_text('clockwork beast'/'4ED', 'When Clockwork Beast comes into play, put seven +1/+0 counters on it. At the end of any combat in which Clockwork Beast is assigned to attack or block, remove a counter.\n{X}, {T}: Put X +1/+0 counters on Clockwork Beast. You may have no more than seven of these counters on Clockwork Beast. Use this ability only during your upkeep.').
card_image_name('clockwork beast'/'4ED', 'clockwork beast').
card_uid('clockwork beast'/'4ED', '4ED:Clockwork Beast:clockwork beast').
card_rarity('clockwork beast'/'4ED', 'Rare').
card_artist('clockwork beast'/'4ED', 'Drew Tucker').
card_multiverse_id('clockwork beast'/'4ED', '2030').

card_in_set('cockatrice', '4ED').
card_original_type('cockatrice'/'4ED', 'Summon — Cockatrice').
card_original_text('cockatrice'/'4ED', 'Flying\nAt the end of combat, destroy all non-wall creatures blocking or blocked by Cockatrice.').
card_image_name('cockatrice'/'4ED', 'cockatrice').
card_uid('cockatrice'/'4ED', '4ED:Cockatrice:cockatrice').
card_rarity('cockatrice'/'4ED', 'Rare').
card_artist('cockatrice'/'4ED', 'Dan Frazier').
card_multiverse_id('cockatrice'/'4ED', '2204').

card_in_set('colossus of sardia', '4ED').
card_original_type('colossus of sardia'/'4ED', 'Artifact Creature').
card_original_text('colossus of sardia'/'4ED', 'Trample\nColossus does not untap during your untap phase.\n{9}: Untap Colossus. Use this ability only during your upkeep.').
card_image_name('colossus of sardia'/'4ED', 'colossus of sardia').
card_uid('colossus of sardia'/'4ED', '4ED:Colossus of Sardia:colossus of sardia').
card_rarity('colossus of sardia'/'4ED', 'Rare').
card_artist('colossus of sardia'/'4ED', 'Jesper Myrfors').
card_flavor_text('colossus of sardia'/'4ED', 'From the Sardian mountains wakes ancient doom:\nWarrior born from a rocky womb.').
card_multiverse_id('colossus of sardia'/'4ED', '2031').

card_in_set('conservator', '4ED').
card_original_type('conservator'/'4ED', 'Artifact').
card_original_text('conservator'/'4ED', '{3}, {T}: Prevent up to 2 damage to you.').
card_image_name('conservator'/'4ED', 'conservator').
card_uid('conservator'/'4ED', '4ED:Conservator:conservator').
card_rarity('conservator'/'4ED', 'Uncommon').
card_artist('conservator'/'4ED', 'Amy Weber').
card_multiverse_id('conservator'/'4ED', '2032').

card_in_set('control magic', '4ED').
card_original_type('control magic'/'4ED', 'Enchant Creature').
card_original_text('control magic'/'4ED', 'Gain control of target creature.').
card_image_name('control magic'/'4ED', 'control magic').
card_uid('control magic'/'4ED', '4ED:Control Magic:control magic').
card_rarity('control magic'/'4ED', 'Uncommon').
card_artist('control magic'/'4ED', 'Dameon Willich').
card_multiverse_id('control magic'/'4ED', '2147').

card_in_set('conversion', '4ED').
card_original_type('conversion'/'4ED', 'Enchantment').
card_original_text('conversion'/'4ED', 'All mountains become basic plains. During your upkeep, pay {W}{W} or destroy Conversion.').
card_image_name('conversion'/'4ED', 'conversion').
card_uid('conversion'/'4ED', '4ED:Conversion:conversion').
card_rarity('conversion'/'4ED', 'Uncommon').
card_artist('conversion'/'4ED', 'Jesper Myrfors').
card_multiverse_id('conversion'/'4ED', '2334').

card_in_set('coral helm', '4ED').
card_original_type('coral helm'/'4ED', 'Artifact').
card_original_text('coral helm'/'4ED', '{3}: Discard a card at random from your hand to give target creature +2/+2 until end of turn.').
card_image_name('coral helm'/'4ED', 'coral helm').
card_uid('coral helm'/'4ED', '4ED:Coral Helm:coral helm').
card_rarity('coral helm'/'4ED', 'Rare').
card_artist('coral helm'/'4ED', 'Amy Weber').
card_multiverse_id('coral helm'/'4ED', '2033').

card_in_set('cosmic horror', '4ED').
card_original_type('cosmic horror'/'4ED', 'Summon — Horror').
card_original_text('cosmic horror'/'4ED', 'First strike\nDuring your upkeep, pay {3}{B}{B}{B} or destroy Cosmic Horror. If you destroy Cosmic Horror in this way, it deals 7 damage to you.').
card_image_name('cosmic horror'/'4ED', 'cosmic horror').
card_uid('cosmic horror'/'4ED', '4ED:Cosmic Horror:cosmic horror').
card_rarity('cosmic horror'/'4ED', 'Rare').
card_artist('cosmic horror'/'4ED', 'Jesper Myrfors').
card_flavor_text('cosmic horror'/'4ED', '\". . . [S]creams of horror rend th\' affrighted skies.\" —Alexander Pope, The Rape of the Lock').
card_multiverse_id('cosmic horror'/'4ED', '2093').

card_in_set('counterspell', '4ED').
card_original_type('counterspell'/'4ED', 'Interrupt').
card_original_text('counterspell'/'4ED', 'Counter target spell.').
card_image_name('counterspell'/'4ED', 'counterspell').
card_uid('counterspell'/'4ED', '4ED:Counterspell:counterspell').
card_rarity('counterspell'/'4ED', 'Uncommon').
card_artist('counterspell'/'4ED', 'Mark Poole').
card_multiverse_id('counterspell'/'4ED', '2148').

card_in_set('craw wurm', '4ED').
card_original_type('craw wurm'/'4ED', 'Summon — Wurm').
card_original_text('craw wurm'/'4ED', '').
card_image_name('craw wurm'/'4ED', 'craw wurm').
card_uid('craw wurm'/'4ED', '4ED:Craw Wurm:craw wurm').
card_rarity('craw wurm'/'4ED', 'Common').
card_artist('craw wurm'/'4ED', 'Daniel Gelon').
card_flavor_text('craw wurm'/'4ED', 'The most terrifying thing about the Craw Wurm is probably the horrible crashing sound it makes as it speeds through the forest. This noise is so loud it echoes through the trees and seems to come from all directions at once.').
card_multiverse_id('craw wurm'/'4ED', '2205').

card_in_set('creature bond', '4ED').
card_original_type('creature bond'/'4ED', 'Enchant Creature').
card_original_text('creature bond'/'4ED', 'If target creature is put into the graveyard, Creature Bond deals damage equal to the creature\'s toughness to that creature\'s controller.').
card_image_name('creature bond'/'4ED', 'creature bond').
card_uid('creature bond'/'4ED', '4ED:Creature Bond:creature bond').
card_rarity('creature bond'/'4ED', 'Common').
card_artist('creature bond'/'4ED', 'Anson Maddocks').
card_multiverse_id('creature bond'/'4ED', '2149').

card_in_set('crimson manticore', '4ED').
card_original_type('crimson manticore'/'4ED', 'Summon — Manticore').
card_original_text('crimson manticore'/'4ED', 'Flying\n{R}, {T}: Manticore deals 1 damage to target attacking or blocking creature.').
card_image_name('crimson manticore'/'4ED', 'crimson manticore').
card_uid('crimson manticore'/'4ED', '4ED:Crimson Manticore:crimson manticore').
card_rarity('crimson manticore'/'4ED', 'Rare').
card_artist('crimson manticore'/'4ED', 'Daniel Gelon').
card_flavor_text('crimson manticore'/'4ED', 'Noted neither for their good looks nor their charm, Manticores can be fearsome allies.  As dinner companions, however, they are best left alone.').
card_multiverse_id('crimson manticore'/'4ED', '2266').

card_in_set('crumble', '4ED').
card_original_type('crumble'/'4ED', 'Instant').
card_original_text('crumble'/'4ED', 'Bury target artifact. Artifact\'s controller gains life equal to the artifact\'s casting cost.').
card_image_name('crumble'/'4ED', 'crumble').
card_uid('crumble'/'4ED', '4ED:Crumble:crumble').
card_rarity('crumble'/'4ED', 'Uncommon').
card_artist('crumble'/'4ED', 'Jesper Myrfors').
card_flavor_text('crumble'/'4ED', 'The spirits of Argoth grant new life to those who repent the folly of enslaving their labors to devices.').
card_multiverse_id('crumble'/'4ED', '2206').

card_in_set('crusade', '4ED').
card_original_type('crusade'/'4ED', 'Enchantment').
card_original_text('crusade'/'4ED', 'All white creatures get +1/+1.').
card_image_name('crusade'/'4ED', 'crusade').
card_uid('crusade'/'4ED', '4ED:Crusade:crusade').
card_rarity('crusade'/'4ED', 'Rare').
card_artist('crusade'/'4ED', 'Mark Poole').
card_multiverse_id('crusade'/'4ED', '2335').

card_in_set('crystal rod', '4ED').
card_original_type('crystal rod'/'4ED', 'Artifact').
card_original_text('crystal rod'/'4ED', '{1}: Gain 1 life for a successfully cast blue spell. Use this effect either when the spell is cast or later in the turn but only once for each blue spell cast.').
card_image_name('crystal rod'/'4ED', 'crystal rod').
card_uid('crystal rod'/'4ED', '4ED:Crystal Rod:crystal rod').
card_rarity('crystal rod'/'4ED', 'Uncommon').
card_artist('crystal rod'/'4ED', 'Amy Weber').
card_multiverse_id('crystal rod'/'4ED', '2034').

card_in_set('cursed land', '4ED').
card_original_type('cursed land'/'4ED', 'Enchant Land').
card_original_text('cursed land'/'4ED', 'Cursed Land deals 1 damage to target land\'s controller during his or her upkeep.').
card_image_name('cursed land'/'4ED', 'cursed land').
card_uid('cursed land'/'4ED', '4ED:Cursed Land:cursed land').
card_rarity('cursed land'/'4ED', 'Uncommon').
card_artist('cursed land'/'4ED', 'Jesper Myrfors').
card_multiverse_id('cursed land'/'4ED', '2094').

card_in_set('cursed rack', '4ED').
card_original_type('cursed rack'/'4ED', 'Artifact').
card_original_text('cursed rack'/'4ED', 'Target opponent discards down to four cards during his or her discard phase.').
card_image_name('cursed rack'/'4ED', 'cursed rack').
card_uid('cursed rack'/'4ED', '4ED:Cursed Rack:cursed rack').
card_rarity('cursed rack'/'4ED', 'Uncommon').
card_artist('cursed rack'/'4ED', 'Richard Thomas').
card_flavor_text('cursed rack'/'4ED', 'Ashnod invented several torture techniques that could make victims even miles away beg for mercy as if the End had come.').
card_multiverse_id('cursed rack'/'4ED', '2035').

card_in_set('cyclopean mummy', '4ED').
card_original_type('cyclopean mummy'/'4ED', 'Summon — Mummy').
card_original_text('cyclopean mummy'/'4ED', 'If Mummy is put into the graveyard from play, remove it from the game.').
card_image_name('cyclopean mummy'/'4ED', 'cyclopean mummy').
card_uid('cyclopean mummy'/'4ED', '4ED:Cyclopean Mummy:cyclopean mummy').
card_rarity('cyclopean mummy'/'4ED', 'Common').
card_artist('cyclopean mummy'/'4ED', 'Edward P. Beard, Jr.').
card_flavor_text('cyclopean mummy'/'4ED', 'The ritual of plucking out an eye to gain future sight is but a curse that enables the living to see their own deaths.').
card_multiverse_id('cyclopean mummy'/'4ED', '2095').

card_in_set('dancing scimitar', '4ED').
card_original_type('dancing scimitar'/'4ED', 'Artifact Creature').
card_original_text('dancing scimitar'/'4ED', 'Flying').
card_image_name('dancing scimitar'/'4ED', 'dancing scimitar').
card_uid('dancing scimitar'/'4ED', '4ED:Dancing Scimitar:dancing scimitar').
card_rarity('dancing scimitar'/'4ED', 'Rare').
card_artist('dancing scimitar'/'4ED', 'Anson Maddocks').
card_flavor_text('dancing scimitar'/'4ED', 'Bobbing merrily from opponent to opponent, the scimitar began adding playful little flourishes to its strokes; it even turned a couple of somersaults.').
card_multiverse_id('dancing scimitar'/'4ED', '2036').

card_in_set('dark ritual', '4ED').
card_original_type('dark ritual'/'4ED', 'Interrupt').
card_original_text('dark ritual'/'4ED', 'Add {B}{B}{B} to your mana pool.').
card_image_name('dark ritual'/'4ED', 'dark ritual').
card_uid('dark ritual'/'4ED', '4ED:Dark Ritual:dark ritual').
card_rarity('dark ritual'/'4ED', 'Common').
card_artist('dark ritual'/'4ED', 'Sandra Everingham').
card_multiverse_id('dark ritual'/'4ED', '2096').

card_in_set('death ward', '4ED').
card_original_type('death ward'/'4ED', 'Instant').
card_original_text('death ward'/'4ED', 'Regenerate target creature.').
card_image_name('death ward'/'4ED', 'death ward').
card_uid('death ward'/'4ED', '4ED:Death Ward:death ward').
card_rarity('death ward'/'4ED', 'Common').
card_artist('death ward'/'4ED', 'Mark Poole').
card_multiverse_id('death ward'/'4ED', '2336').

card_in_set('deathgrip', '4ED').
card_original_type('deathgrip'/'4ED', 'Enchantment').
card_original_text('deathgrip'/'4ED', '{B}{B} Counter target green spell. Play this ability as an interrupt.').
card_image_name('deathgrip'/'4ED', 'deathgrip').
card_uid('deathgrip'/'4ED', '4ED:Deathgrip:deathgrip').
card_rarity('deathgrip'/'4ED', 'Uncommon').
card_artist('deathgrip'/'4ED', 'Anson Maddocks').
card_multiverse_id('deathgrip'/'4ED', '2097').

card_in_set('deathlace', '4ED').
card_original_type('deathlace'/'4ED', 'Interrupt').
card_original_text('deathlace'/'4ED', 'Change the color of target spell or target permanent to black. Costs to cast, tap, maintain, or use a special ability of target remain unchanged.').
card_image_name('deathlace'/'4ED', 'deathlace').
card_uid('deathlace'/'4ED', '4ED:Deathlace:deathlace').
card_rarity('deathlace'/'4ED', 'Rare').
card_artist('deathlace'/'4ED', 'Sandra Everingham').
card_multiverse_id('deathlace'/'4ED', '2098').

card_in_set('desert twister', '4ED').
card_original_type('desert twister'/'4ED', 'Sorcery').
card_original_text('desert twister'/'4ED', 'Destroy target permanent.').
card_image_name('desert twister'/'4ED', 'desert twister').
card_uid('desert twister'/'4ED', '4ED:Desert Twister:desert twister').
card_rarity('desert twister'/'4ED', 'Uncommon').
card_artist('desert twister'/'4ED', 'Susan Van Camp').
card_multiverse_id('desert twister'/'4ED', '2207').

card_in_set('detonate', '4ED').
card_original_type('detonate'/'4ED', 'Sorcery').
card_original_text('detonate'/'4ED', 'Bury target artifact. Detonate deals X damage to the artifact\'s controller, where X is the casting cost of the artifact.').
card_image_name('detonate'/'4ED', 'detonate').
card_uid('detonate'/'4ED', '4ED:Detonate:detonate').
card_rarity('detonate'/'4ED', 'Uncommon').
card_artist('detonate'/'4ED', 'Randy Asplund-Faith').
card_multiverse_id('detonate'/'4ED', '2267').

card_in_set('diabolic machine', '4ED').
card_original_type('diabolic machine'/'4ED', 'Artifact Creature').
card_original_text('diabolic machine'/'4ED', '{3}: Regenerate').
card_image_name('diabolic machine'/'4ED', 'diabolic machine').
card_uid('diabolic machine'/'4ED', '4ED:Diabolic Machine:diabolic machine').
card_rarity('diabolic machine'/'4ED', 'Uncommon').
card_artist('diabolic machine'/'4ED', 'Anson Maddocks').
card_flavor_text('diabolic machine'/'4ED', '\"The bolts of our ballistae smashed into the monstrous thing, but our hopes died in our chests as its gears continued turning.\"\n—Sevti Mukul, The Fall of Alsoor').
card_multiverse_id('diabolic machine'/'4ED', '2037').

card_in_set('dingus egg', '4ED').
card_original_type('dingus egg'/'4ED', 'Artifact').
card_original_text('dingus egg'/'4ED', 'Each time a player puts a land into the graveyard from play, Dingus Egg deals 2 damage to that land\'s controller.').
card_image_name('dingus egg'/'4ED', 'dingus egg').
card_uid('dingus egg'/'4ED', '4ED:Dingus Egg:dingus egg').
card_rarity('dingus egg'/'4ED', 'Rare').
card_artist('dingus egg'/'4ED', 'Dan Frazier').
card_multiverse_id('dingus egg'/'4ED', '2038').

card_in_set('disenchant', '4ED').
card_original_type('disenchant'/'4ED', 'Instant').
card_original_text('disenchant'/'4ED', 'Destroy target enchantment or artifact.').
card_image_name('disenchant'/'4ED', 'disenchant').
card_uid('disenchant'/'4ED', '4ED:Disenchant:disenchant').
card_rarity('disenchant'/'4ED', 'Common').
card_artist('disenchant'/'4ED', 'Amy Weber').
card_multiverse_id('disenchant'/'4ED', '2337').

card_in_set('disintegrate', '4ED').
card_original_type('disintegrate'/'4ED', 'Sorcery').
card_original_text('disintegrate'/'4ED', 'Disintegrate deals X damage to target creature or player. The target cannot regenerate until end of turn. If the target receives lethal damage this turn, remove it from the game entirely.').
card_image_name('disintegrate'/'4ED', 'disintegrate').
card_uid('disintegrate'/'4ED', '4ED:Disintegrate:disintegrate').
card_rarity('disintegrate'/'4ED', 'Common').
card_artist('disintegrate'/'4ED', 'Anson Maddocks').
card_multiverse_id('disintegrate'/'4ED', '2268').

card_in_set('disrupting scepter', '4ED').
card_original_type('disrupting scepter'/'4ED', 'Artifact').
card_original_text('disrupting scepter'/'4ED', '{3}, {T}: Target player chooses and discards one card from his or her hand. Use this ability only during your turn.').
card_image_name('disrupting scepter'/'4ED', 'disrupting scepter').
card_uid('disrupting scepter'/'4ED', '4ED:Disrupting Scepter:disrupting scepter').
card_rarity('disrupting scepter'/'4ED', 'Rare').
card_artist('disrupting scepter'/'4ED', 'Dan Frazier').
card_multiverse_id('disrupting scepter'/'4ED', '2039').

card_in_set('divine transformation', '4ED').
card_original_type('divine transformation'/'4ED', 'Enchant Creature').
card_original_text('divine transformation'/'4ED', 'Target creature gets +3/+3.').
card_image_name('divine transformation'/'4ED', 'divine transformation').
card_uid('divine transformation'/'4ED', '4ED:Divine Transformation:divine transformation').
card_rarity('divine transformation'/'4ED', 'Uncommon').
card_artist('divine transformation'/'4ED', 'NéNé Thomas').
card_flavor_text('divine transformation'/'4ED', 'Glory surged through her and radiance surrounded her. All things were possible with the blessing of the Divine.').
card_multiverse_id('divine transformation'/'4ED', '2338').

card_in_set('dragon engine', '4ED').
card_original_type('dragon engine'/'4ED', 'Artifact Creature').
card_original_text('dragon engine'/'4ED', '{2}: +1/+0 until end of turn').
card_image_name('dragon engine'/'4ED', 'dragon engine').
card_uid('dragon engine'/'4ED', '4ED:Dragon Engine:dragon engine').
card_rarity('dragon engine'/'4ED', 'Rare').
card_artist('dragon engine'/'4ED', 'Anson Maddocks').
card_flavor_text('dragon engine'/'4ED', 'Those who believed the city of Kroog would never fall to Mishra\'s forces severely underestimated the might of his war machines.').
card_multiverse_id('dragon engine'/'4ED', '2040').

card_in_set('dragon whelp', '4ED').
card_original_type('dragon whelp'/'4ED', 'Summon — Dragon').
card_original_text('dragon whelp'/'4ED', 'Flying\n{R} +1/+0 until end of turn. If you spend more than {R}{R}{R} in this way during one turn, destroy Dragon Whelp at end of turn.').
card_image_name('dragon whelp'/'4ED', 'dragon whelp').
card_uid('dragon whelp'/'4ED', '4ED:Dragon Whelp:dragon whelp').
card_rarity('dragon whelp'/'4ED', 'Uncommon').
card_artist('dragon whelp'/'4ED', 'Amy Weber').
card_flavor_text('dragon whelp'/'4ED', '\"O to be a dragon . . . of silkworm size or immense . . .\" —Marianne Moore, \"O to Be a Dragon\"').
card_multiverse_id('dragon whelp'/'4ED', '2269').

card_in_set('drain life', '4ED').
card_original_type('drain life'/'4ED', 'Sorcery').
card_original_text('drain life'/'4ED', 'Drain Life deals 1 damage to a target creature or player for each {B} you pay in addition to the casting cost. You then gain 1 life for each 1 damage dealt. You cannot gain more life than the toughness of the creature or the total life of the player Drain Life damages.').
card_image_name('drain life'/'4ED', 'drain life').
card_uid('drain life'/'4ED', '4ED:Drain Life:drain life').
card_rarity('drain life'/'4ED', 'Common').
card_artist('drain life'/'4ED', 'Douglas Shuler').
card_multiverse_id('drain life'/'4ED', '2099').

card_in_set('drain power', '4ED').
card_original_type('drain power'/'4ED', 'Sorcery').
card_original_text('drain power'/'4ED', 'Target player must draw all mana from his or her available lands; then, all mana in target player\'s mana pool drains into your mana pool.').
card_image_name('drain power'/'4ED', 'drain power').
card_uid('drain power'/'4ED', '4ED:Drain Power:drain power').
card_rarity('drain power'/'4ED', 'Rare').
card_artist('drain power'/'4ED', 'Douglas Shuler').
card_multiverse_id('drain power'/'4ED', '2150').

card_in_set('drudge skeletons', '4ED').
card_original_type('drudge skeletons'/'4ED', 'Summon — Skeletons').
card_original_text('drudge skeletons'/'4ED', '{B} Regenerate').
card_image_name('drudge skeletons'/'4ED', 'drudge skeletons').
card_uid('drudge skeletons'/'4ED', '4ED:Drudge Skeletons:drudge skeletons').
card_rarity('drudge skeletons'/'4ED', 'Common').
card_artist('drudge skeletons'/'4ED', 'Sandra Everingham').
card_flavor_text('drudge skeletons'/'4ED', 'Bones scattered around us joined to form misshapen bodies. We struck at them repeatedly—they fell, but soon formed again, with the same mocking look on their faceless skulls.').
card_multiverse_id('drudge skeletons'/'4ED', '2100').

card_in_set('durkwood boars', '4ED').
card_original_type('durkwood boars'/'4ED', 'Summon — Boars').
card_original_text('durkwood boars'/'4ED', '').
card_image_name('durkwood boars'/'4ED', 'durkwood boars').
card_uid('durkwood boars'/'4ED', '4ED:Durkwood Boars:durkwood boars').
card_rarity('durkwood boars'/'4ED', 'Common').
card_artist('durkwood boars'/'4ED', 'Mike Kimble').
card_flavor_text('durkwood boars'/'4ED', '\"And the unclean spirits went out, and entered the swine: and the herd ran violently . . . .\"\n—Mark 5:13').
card_multiverse_id('durkwood boars'/'4ED', '2208').

card_in_set('dwarven warriors', '4ED').
card_original_type('dwarven warriors'/'4ED', 'Summon — Dwarves').
card_original_text('dwarven warriors'/'4ED', '{T}: Target creature with power no greater than 2 becomes unblockable until end of turn. Other effects may later be used to increase the creature\'s power beyond 2.').
card_image_name('dwarven warriors'/'4ED', 'dwarven warriors').
card_uid('dwarven warriors'/'4ED', '4ED:Dwarven Warriors:dwarven warriors').
card_rarity('dwarven warriors'/'4ED', 'Common').
card_artist('dwarven warriors'/'4ED', 'Douglas Shuler').
card_multiverse_id('dwarven warriors'/'4ED', '2270').

card_in_set('earth elemental', '4ED').
card_original_type('earth elemental'/'4ED', 'Summon — Elemental').
card_original_text('earth elemental'/'4ED', '').
card_image_name('earth elemental'/'4ED', 'earth elemental').
card_uid('earth elemental'/'4ED', '4ED:Earth Elemental:earth elemental').
card_rarity('earth elemental'/'4ED', 'Uncommon').
card_artist('earth elemental'/'4ED', 'Dan Frazier').
card_flavor_text('earth elemental'/'4ED', 'Earth Elementals have the eternal strength of stone and the endurance of mountains. Primordially connected to the land they inhabit, they take a long-term view of things, scorning the impetuous haste of short-lived mortal creatures.').
card_multiverse_id('earth elemental'/'4ED', '2271').

card_in_set('earthquake', '4ED').
card_original_type('earthquake'/'4ED', 'Sorcery').
card_original_text('earthquake'/'4ED', 'Earthquake deals X damage to each player and each creature without flying.').
card_image_name('earthquake'/'4ED', 'earthquake').
card_uid('earthquake'/'4ED', '4ED:Earthquake:earthquake').
card_rarity('earthquake'/'4ED', 'Rare').
card_artist('earthquake'/'4ED', 'Dan Frazier').
card_multiverse_id('earthquake'/'4ED', '2272').

card_in_set('ebony horse', '4ED').
card_original_type('ebony horse'/'4ED', 'Artifact').
card_original_text('ebony horse'/'4ED', '{2}, {T}: Untap target attacking creature you control. That creature neither receives nor deals damage during combat this turn.').
card_image_name('ebony horse'/'4ED', 'ebony horse').
card_uid('ebony horse'/'4ED', '4ED:Ebony Horse:ebony horse').
card_rarity('ebony horse'/'4ED', 'Rare').
card_artist('ebony horse'/'4ED', 'Dameon Willich').
card_multiverse_id('ebony horse'/'4ED', '2041').

card_in_set('el-hajjâj', '4ED').
card_original_type('el-hajjâj'/'4ED', 'Summon — El-Hajjâj').
card_original_text('el-hajjâj'/'4ED', 'Gain 1 life for every 1 damage El-Hajjâj deals. You cannot gain more life in this way than the toughness of the creature or the total life of the player that El-Hajjâj damages.').
card_image_name('el-hajjâj'/'4ED', 'el-hajjaj').
card_uid('el-hajjâj'/'4ED', '4ED:El-Hajjâj:el-hajjaj').
card_rarity('el-hajjâj'/'4ED', 'Rare').
card_artist('el-hajjâj'/'4ED', 'Dameon Willich').
card_multiverse_id('el-hajjâj'/'4ED', '2101').

card_in_set('elder land wurm', '4ED').
card_original_type('elder land wurm'/'4ED', 'Summon — Wurm').
card_original_text('elder land wurm'/'4ED', 'Trample\nCannot attack until assigned as a blocker.').
card_image_name('elder land wurm'/'4ED', 'elder land wurm').
card_uid('elder land wurm'/'4ED', '4ED:Elder Land Wurm:elder land wurm').
card_rarity('elder land wurm'/'4ED', 'Rare').
card_artist('elder land wurm'/'4ED', 'Quinton Hoover').
card_flavor_text('elder land wurm'/'4ED', 'Sometimes it\'s best to let sleeping dragons lie.').
card_multiverse_id('elder land wurm'/'4ED', '2339').

card_in_set('elven riders', '4ED').
card_original_type('elven riders'/'4ED', 'Summon — Riders').
card_original_text('elven riders'/'4ED', 'Cannot be blocked except by walls and by creatures with flying.').
card_image_name('elven riders'/'4ED', 'elven riders').
card_uid('elven riders'/'4ED', '4ED:Elven Riders:elven riders').
card_rarity('elven riders'/'4ED', 'Uncommon').
card_artist('elven riders'/'4ED', 'Melissa A. Benson').
card_flavor_text('elven riders'/'4ED', '\"Sometimes it is better to be swift of foot than strong of swordarm.\"\n—Elven proverb').
card_multiverse_id('elven riders'/'4ED', '2209').

card_in_set('elvish archers', '4ED').
card_original_type('elvish archers'/'4ED', 'Summon — Elves').
card_original_text('elvish archers'/'4ED', 'First strike').
card_image_name('elvish archers'/'4ED', 'elvish archers').
card_uid('elvish archers'/'4ED', '4ED:Elvish Archers:elvish archers').
card_rarity('elvish archers'/'4ED', 'Rare').
card_artist('elvish archers'/'4ED', 'Anson Maddocks').
card_flavor_text('elvish archers'/'4ED', 'I tell you, there was so many arrows flying about you couldn\'t hardly see the sun. So I says to young Angus, \"Well, at least now we\'re fighting in the shade!\"').
card_multiverse_id('elvish archers'/'4ED', '2210').

card_in_set('energy flux', '4ED').
card_original_type('energy flux'/'4ED', 'Enchantment').
card_original_text('energy flux'/'4ED', 'During each player\'s upkeep, destroy all artifacts that player controls. The player may pay an additional {2} for each artifact he or she wishes to prevent Energy Flux from destroying.').
card_image_name('energy flux'/'4ED', 'energy flux').
card_uid('energy flux'/'4ED', '4ED:Energy Flux:energy flux').
card_rarity('energy flux'/'4ED', 'Uncommon').
card_artist('energy flux'/'4ED', 'Kaja Foglio').
card_multiverse_id('energy flux'/'4ED', '2151').

card_in_set('energy tap', '4ED').
card_original_type('energy tap'/'4ED', 'Sorcery').
card_original_text('energy tap'/'4ED', 'Tap target creature you control. Add an amount of colorless mana equal to that creature\'s casting cost to your mana pool.').
card_image_name('energy tap'/'4ED', 'energy tap').
card_uid('energy tap'/'4ED', '4ED:Energy Tap:energy tap').
card_rarity('energy tap'/'4ED', 'Common').
card_artist('energy tap'/'4ED', 'Daniel Gelon').
card_multiverse_id('energy tap'/'4ED', '2152').

card_in_set('erg raiders', '4ED').
card_original_type('erg raiders'/'4ED', 'Summon — Raiders').
card_original_text('erg raiders'/'4ED', 'If you do not attack with Erg Raiders during your turn, it deals 2 damage to you at end of turn. Erg Raiders deals no damage to you the turn it comes into play on your side.').
card_image_name('erg raiders'/'4ED', 'erg raiders').
card_uid('erg raiders'/'4ED', '4ED:Erg Raiders:erg raiders').
card_rarity('erg raiders'/'4ED', 'Common').
card_artist('erg raiders'/'4ED', 'Dameon Willich').
card_multiverse_id('erg raiders'/'4ED', '2102').

card_in_set('erosion', '4ED').
card_original_type('erosion'/'4ED', 'Enchant Land').
card_original_text('erosion'/'4ED', 'During his or her upkeep, target land\'s controller pays {1} or 1 life, or target land is destroyed. Effects that prevent or redirect damage cannot be used to counter this loss of life.').
card_image_name('erosion'/'4ED', 'erosion').
card_uid('erosion'/'4ED', '4ED:Erosion:erosion').
card_rarity('erosion'/'4ED', 'Common').
card_artist('erosion'/'4ED', 'Pete Venters').
card_multiverse_id('erosion'/'4ED', '2153').

card_in_set('eternal warrior', '4ED').
card_original_type('eternal warrior'/'4ED', 'Enchant Creature').
card_original_text('eternal warrior'/'4ED', 'Attacking does not cause target creature to tap.').
card_image_name('eternal warrior'/'4ED', 'eternal warrior').
card_uid('eternal warrior'/'4ED', '4ED:Eternal Warrior:eternal warrior').
card_rarity('eternal warrior'/'4ED', 'Common').
card_artist('eternal warrior'/'4ED', 'Anson Maddocks').
card_flavor_text('eternal warrior'/'4ED', 'Warriors of the Tsunami-nito School spend years in training to master the way of effortless effort.').
card_multiverse_id('eternal warrior'/'4ED', '2273').

card_in_set('evil presence', '4ED').
card_original_type('evil presence'/'4ED', 'Enchant Land').
card_original_text('evil presence'/'4ED', 'Target land becomes a basic swamp.').
card_image_name('evil presence'/'4ED', 'evil presence').
card_uid('evil presence'/'4ED', '4ED:Evil Presence:evil presence').
card_rarity('evil presence'/'4ED', 'Uncommon').
card_artist('evil presence'/'4ED', 'Sandra Everingham').
card_multiverse_id('evil presence'/'4ED', '2103').

card_in_set('eye for an eye', '4ED').
card_original_type('eye for an eye'/'4ED', 'Instant').
card_original_text('eye for an eye'/'4ED', 'You may cast Eye for an Eye only when a creature, spell, or effect deals damage to you. Eye for an Eye deals an equal amount of damage to the controller of that creature, spell, or effect. If another spell or effect reduces the amount of damage you receive, it does not reduce the damage dealt by Eye for an Eye.').
card_image_name('eye for an eye'/'4ED', 'eye for an eye').
card_uid('eye for an eye'/'4ED', '4ED:Eye for an Eye:eye for an eye').
card_rarity('eye for an eye'/'4ED', 'Rare').
card_artist('eye for an eye'/'4ED', 'Mark Poole').
card_multiverse_id('eye for an eye'/'4ED', '2340').

card_in_set('fear', '4ED').
card_original_type('fear'/'4ED', 'Enchant Creature').
card_original_text('fear'/'4ED', 'Target creature cannot be blocked except by black creatures and artifact creatures.').
card_image_name('fear'/'4ED', 'fear').
card_uid('fear'/'4ED', '4ED:Fear:fear').
card_rarity('fear'/'4ED', 'Common').
card_artist('fear'/'4ED', 'Mark Poole').
card_multiverse_id('fear'/'4ED', '2104').

card_in_set('feedback', '4ED').
card_original_type('feedback'/'4ED', 'Enchant Enchantment').
card_original_text('feedback'/'4ED', 'Feedback deals 1 damage to controller of target enchantment during that player\'s upkeep.').
card_image_name('feedback'/'4ED', 'feedback').
card_uid('feedback'/'4ED', '4ED:Feedback:feedback').
card_rarity('feedback'/'4ED', 'Uncommon').
card_artist('feedback'/'4ED', 'Quinton Hoover').
card_multiverse_id('feedback'/'4ED', '2154').

card_in_set('fellwar stone', '4ED').
card_original_type('fellwar stone'/'4ED', 'Artifact').
card_original_text('fellwar stone'/'4ED', '{T}: Add one mana to your mana pool. This mana may be of any type that any land opponent controls can produce. Play this ability as an interrupt.').
card_image_name('fellwar stone'/'4ED', 'fellwar stone').
card_uid('fellwar stone'/'4ED', '4ED:Fellwar Stone:fellwar stone').
card_rarity('fellwar stone'/'4ED', 'Uncommon').
card_artist('fellwar stone'/'4ED', 'Quinton Hoover').
card_flavor_text('fellwar stone'/'4ED', '\"What do you have that I cannot obtain?\"\n—Mairsil, called the Pretender').
card_multiverse_id('fellwar stone'/'4ED', '2042').

card_in_set('fire elemental', '4ED').
card_original_type('fire elemental'/'4ED', 'Summon — Elemental').
card_original_text('fire elemental'/'4ED', '').
card_image_name('fire elemental'/'4ED', 'fire elemental').
card_uid('fire elemental'/'4ED', '4ED:Fire Elemental:fire elemental').
card_rarity('fire elemental'/'4ED', 'Uncommon').
card_artist('fire elemental'/'4ED', 'Melissa A. Benson').
card_flavor_text('fire elemental'/'4ED', 'Fire Elementals are ruthless infernos, annihilating and consuming their foes in a frenzied holocaust. Crackling and blazing, they sear swift, terrible paths, leaving the land charred and scorched in their wake.').
card_multiverse_id('fire elemental'/'4ED', '2274').

card_in_set('fireball', '4ED').
card_original_type('fireball'/'4ED', 'Sorcery').
card_original_text('fireball'/'4ED', 'Fireball deals X damage, divided evenly (round down) among any number of target creatures and/or players. Pay an additional {1} for each target beyond the first.').
card_image_name('fireball'/'4ED', 'fireball').
card_uid('fireball'/'4ED', '4ED:Fireball:fireball').
card_rarity('fireball'/'4ED', 'Common').
card_artist('fireball'/'4ED', 'Mark Tedin').
card_multiverse_id('fireball'/'4ED', '2275').

card_in_set('firebreathing', '4ED').
card_original_type('firebreathing'/'4ED', 'Enchant Creature').
card_original_text('firebreathing'/'4ED', '{R} Target creature Firebreathing enchants gets +1/+0 until end of turn.').
card_image_name('firebreathing'/'4ED', 'firebreathing').
card_uid('firebreathing'/'4ED', '4ED:Firebreathing:firebreathing').
card_rarity('firebreathing'/'4ED', 'Common').
card_artist('firebreathing'/'4ED', 'Dan Frazier').
card_flavor_text('firebreathing'/'4ED', '\"And topples round the dreary west\nA looming bastion fringed with fire.\"\n—Alfred, Lord Tennyson, \"In Memoriam\"').
card_multiverse_id('firebreathing'/'4ED', '2276').

card_in_set('fissure', '4ED').
card_original_type('fissure'/'4ED', 'Instant').
card_original_text('fissure'/'4ED', 'Bury target land or creature.').
card_image_name('fissure'/'4ED', 'fissure').
card_uid('fissure'/'4ED', '4ED:Fissure:fissure').
card_rarity('fissure'/'4ED', 'Common').
card_artist('fissure'/'4ED', 'Douglas Shuler').
card_flavor_text('fissure'/'4ED', '\"Must not all things at the last be swallowed up in death?\"\n—Plato').
card_multiverse_id('fissure'/'4ED', '2277').

card_in_set('flashfires', '4ED').
card_original_type('flashfires'/'4ED', 'Sorcery').
card_original_text('flashfires'/'4ED', 'Destroy all plains.').
card_image_name('flashfires'/'4ED', 'flashfires').
card_uid('flashfires'/'4ED', '4ED:Flashfires:flashfires').
card_rarity('flashfires'/'4ED', 'Uncommon').
card_artist('flashfires'/'4ED', 'Dameon Willich').
card_multiverse_id('flashfires'/'4ED', '2278').

card_in_set('flight', '4ED').
card_original_type('flight'/'4ED', 'Enchant Creature').
card_original_text('flight'/'4ED', 'Target creature gains flying.').
card_image_name('flight'/'4ED', 'flight').
card_uid('flight'/'4ED', '4ED:Flight:flight').
card_rarity('flight'/'4ED', 'Common').
card_artist('flight'/'4ED', 'Anson Maddocks').
card_multiverse_id('flight'/'4ED', '2155').

card_in_set('flood', '4ED').
card_original_type('flood'/'4ED', 'Enchantment').
card_original_text('flood'/'4ED', '{U}{U} Tap target creature without flying.').
card_image_name('flood'/'4ED', 'flood').
card_uid('flood'/'4ED', '4ED:Flood:flood').
card_rarity('flood'/'4ED', 'Common').
card_artist('flood'/'4ED', 'Dennis Detwiller').
card_flavor_text('flood'/'4ED', '\"A dash of cool water does wonders to clear a cluttered battlefield.\"\n—Vibekke Ragnild, Witches and War').
card_multiverse_id('flood'/'4ED', '2156').

card_in_set('flying carpet', '4ED').
card_original_type('flying carpet'/'4ED', 'Artifact').
card_original_text('flying carpet'/'4ED', '{2}, {T}: Target creature gains flying until end of turn. If that creature is put into the graveyard before end of turn, destroy Flying Carpet.').
card_image_name('flying carpet'/'4ED', 'flying carpet').
card_uid('flying carpet'/'4ED', '4ED:Flying Carpet:flying carpet').
card_rarity('flying carpet'/'4ED', 'Rare').
card_artist('flying carpet'/'4ED', 'Mark Tedin').
card_multiverse_id('flying carpet'/'4ED', '2043').

card_in_set('fog', '4ED').
card_original_type('fog'/'4ED', 'Instant').
card_original_text('fog'/'4ED', 'No creatures deal damage in combat this turn.').
card_image_name('fog'/'4ED', 'fog').
card_uid('fog'/'4ED', '4ED:Fog:fog').
card_rarity('fog'/'4ED', 'Common').
card_artist('fog'/'4ED', 'Jesper Myrfors').
card_multiverse_id('fog'/'4ED', '2211').

card_in_set('force of nature', '4ED').
card_original_type('force of nature'/'4ED', 'Summon — Force').
card_original_text('force of nature'/'4ED', 'Trample\nDuring your upkeep, pay {G}{G}{G}{G} or Force of Nature deals 8 damage to you.').
card_image_name('force of nature'/'4ED', 'force of nature').
card_uid('force of nature'/'4ED', '4ED:Force of Nature:force of nature').
card_rarity('force of nature'/'4ED', 'Rare').
card_artist('force of nature'/'4ED', 'Douglas Shuler').
card_multiverse_id('force of nature'/'4ED', '2212').

card_in_set('forest', '4ED').
card_original_type('forest'/'4ED', 'Land').
card_original_text('forest'/'4ED', '{T}: Add {G} to your mana pool.').
card_image_name('forest'/'4ED', 'forest1').
card_uid('forest'/'4ED', '4ED:Forest:forest1').
card_rarity('forest'/'4ED', 'Basic Land').
card_artist('forest'/'4ED', 'Christopher Rush').
card_multiverse_id('forest'/'4ED', '2377').

card_in_set('forest', '4ED').
card_original_type('forest'/'4ED', 'Land').
card_original_text('forest'/'4ED', '{T}: Add {G} to your mana pool.').
card_image_name('forest'/'4ED', 'forest2').
card_uid('forest'/'4ED', '4ED:Forest:forest2').
card_rarity('forest'/'4ED', 'Basic Land').
card_artist('forest'/'4ED', 'Christopher Rush').
card_multiverse_id('forest'/'4ED', '2378').

card_in_set('forest', '4ED').
card_original_type('forest'/'4ED', 'Land').
card_original_text('forest'/'4ED', '{T}: Add {G} to your mana pool.').
card_image_name('forest'/'4ED', 'forest3').
card_uid('forest'/'4ED', '4ED:Forest:forest3').
card_rarity('forest'/'4ED', 'Basic Land').
card_artist('forest'/'4ED', 'Christopher Rush').
card_multiverse_id('forest'/'4ED', '2379').

card_in_set('fortified area', '4ED').
card_original_type('fortified area'/'4ED', 'Enchantment').
card_original_text('fortified area'/'4ED', 'All walls you control gain banding and get +1/+0.').
card_image_name('fortified area'/'4ED', 'fortified area').
card_uid('fortified area'/'4ED', '4ED:Fortified Area:fortified area').
card_rarity('fortified area'/'4ED', 'Common').
card_artist('fortified area'/'4ED', 'Randy Asplund-Faith').
card_multiverse_id('fortified area'/'4ED', '2341').

card_in_set('frozen shade', '4ED').
card_original_type('frozen shade'/'4ED', 'Summon — Shade').
card_original_text('frozen shade'/'4ED', '{B} +1/+1 until end of turn').
card_image_name('frozen shade'/'4ED', 'frozen shade').
card_uid('frozen shade'/'4ED', '4ED:Frozen Shade:frozen shade').
card_rarity('frozen shade'/'4ED', 'Common').
card_artist('frozen shade'/'4ED', 'Douglas Shuler').
card_flavor_text('frozen shade'/'4ED', '\"There are some qualities, some incorporate things, / That have a double life, which thus is made / A type of twin entity which springs / From matter and light, evinced in solid and shade.\"\n—Edgar Allan Poe, \"Silence\"').
card_multiverse_id('frozen shade'/'4ED', '2105').

card_in_set('fungusaur', '4ED').
card_original_type('fungusaur'/'4ED', 'Summon — Fungusaur').
card_original_text('fungusaur'/'4ED', 'At the end of any turn in which Fungusaur receives damage but does not leave play, put a +1/+1 counter on it.').
card_image_name('fungusaur'/'4ED', 'fungusaur').
card_uid('fungusaur'/'4ED', '4ED:Fungusaur:fungusaur').
card_rarity('fungusaur'/'4ED', 'Rare').
card_artist('fungusaur'/'4ED', 'Daniel Gelon').
card_flavor_text('fungusaur'/'4ED', 'Rather than sheltering her young, the female Fungusaur often injures her own offspring, thereby ensuring their rapid growth.').
card_multiverse_id('fungusaur'/'4ED', '2213').

card_in_set('gaea\'s liege', '4ED').
card_original_type('gaea\'s liege'/'4ED', 'Summon — Gaea\'s Liege').
card_original_text('gaea\'s liege'/'4ED', 'Gaea\'s Liege has power and toughness each equal to the number of forests you control; when Gaea\'s Liege attacks, these are instead equal to the number of forests defending player controls.\n{T}: Target land becomes a basic forest until Gaea\'s Liege leaves play.').
card_image_name('gaea\'s liege'/'4ED', 'gaea\'s liege').
card_uid('gaea\'s liege'/'4ED', '4ED:Gaea\'s Liege:gaea\'s liege').
card_rarity('gaea\'s liege'/'4ED', 'Rare').
card_artist('gaea\'s liege'/'4ED', 'Dameon Willich').
card_multiverse_id('gaea\'s liege'/'4ED', '2214').

card_in_set('gaseous form', '4ED').
card_original_type('gaseous form'/'4ED', 'Enchant Creature').
card_original_text('gaseous form'/'4ED', 'Target creature neither deals nor receives damage during combat.').
card_image_name('gaseous form'/'4ED', 'gaseous form').
card_uid('gaseous form'/'4ED', '4ED:Gaseous Form:gaseous form').
card_rarity('gaseous form'/'4ED', 'Common').
card_artist('gaseous form'/'4ED', 'Phil Foglio').
card_flavor_text('gaseous form'/'4ED', '\". . . [A]nd gives to airy nothing\nA local habitation and a name.\"\n—William Shakespeare, A Midsummer-Night\'s Dream').
card_multiverse_id('gaseous form'/'4ED', '2157').

card_in_set('ghost ship', '4ED').
card_original_type('ghost ship'/'4ED', 'Summon — Ship').
card_original_text('ghost ship'/'4ED', 'Flying\n{U}{U}{U} Regenerate').
card_image_name('ghost ship'/'4ED', 'ghost ship').
card_uid('ghost ship'/'4ED', '4ED:Ghost Ship:ghost ship').
card_rarity('ghost ship'/'4ED', 'Uncommon').
card_artist('ghost ship'/'4ED', 'Tom Wänerstrand').
card_flavor_text('ghost ship'/'4ED', '\"That phantom prow split the storm as lightning cast its long shadow on the battlefield below.\"\n—Mireille Gaetane, The Valeriad').
card_multiverse_id('ghost ship'/'4ED', '2158').

card_in_set('giant growth', '4ED').
card_original_type('giant growth'/'4ED', 'Instant').
card_original_text('giant growth'/'4ED', 'Target creature gets +3/+3 until end of turn.').
card_image_name('giant growth'/'4ED', 'giant growth').
card_uid('giant growth'/'4ED', '4ED:Giant Growth:giant growth').
card_rarity('giant growth'/'4ED', 'Common').
card_artist('giant growth'/'4ED', 'Sandra Everingham').
card_multiverse_id('giant growth'/'4ED', '2215').

card_in_set('giant spider', '4ED').
card_original_type('giant spider'/'4ED', 'Summon — Spider').
card_original_text('giant spider'/'4ED', 'Can block creatures with flying.').
card_image_name('giant spider'/'4ED', 'giant spider').
card_uid('giant spider'/'4ED', '4ED:Giant Spider:giant spider').
card_rarity('giant spider'/'4ED', 'Common').
card_artist('giant spider'/'4ED', 'Sandra Everingham').
card_flavor_text('giant spider'/'4ED', 'While it possesses potent venom, the Giant Spider often chooses not to paralyze its victims. Perhaps the creature enjoys the gentle rocking motion caused by its captives\' struggles to escape its web.').
card_multiverse_id('giant spider'/'4ED', '2216').

card_in_set('giant strength', '4ED').
card_original_type('giant strength'/'4ED', 'Enchant Creature').
card_original_text('giant strength'/'4ED', 'Target creature gets +2/+2.').
card_image_name('giant strength'/'4ED', 'giant strength').
card_uid('giant strength'/'4ED', '4ED:Giant Strength:giant strength').
card_rarity('giant strength'/'4ED', 'Common').
card_artist('giant strength'/'4ED', 'Justin Hampton').
card_flavor_text('giant strength'/'4ED', '\"O! it is excellent\nTo have a giant\'s strength, but it is tyrannous\nTo use it like a giant.\"\n—William Shakespeare, Measure for Measure').
card_multiverse_id('giant strength'/'4ED', '2279').

card_in_set('giant tortoise', '4ED').
card_original_type('giant tortoise'/'4ED', 'Summon — Tortoise').
card_original_text('giant tortoise'/'4ED', 'Giant Tortoise gets +0/+3 while untapped.').
card_image_name('giant tortoise'/'4ED', 'giant tortoise').
card_uid('giant tortoise'/'4ED', '4ED:Giant Tortoise:giant tortoise').
card_rarity('giant tortoise'/'4ED', 'Common').
card_artist('giant tortoise'/'4ED', 'Kaja Foglio').
card_multiverse_id('giant tortoise'/'4ED', '2159').

card_in_set('glasses of urza', '4ED').
card_original_type('glasses of urza'/'4ED', 'Artifact').
card_original_text('glasses of urza'/'4ED', '{T}: Look at target player\'s hand.').
card_image_name('glasses of urza'/'4ED', 'glasses of urza').
card_uid('glasses of urza'/'4ED', '4ED:Glasses of Urza:glasses of urza').
card_rarity('glasses of urza'/'4ED', 'Uncommon').
card_artist('glasses of urza'/'4ED', 'Douglas Shuler').
card_multiverse_id('glasses of urza'/'4ED', '2044').

card_in_set('gloom', '4ED').
card_original_type('gloom'/'4ED', 'Enchantment').
card_original_text('gloom'/'4ED', 'White spells cost an additional {3} to cast. White enchantments with activation costs require an additional {3} to use.').
card_image_name('gloom'/'4ED', 'gloom').
card_uid('gloom'/'4ED', '4ED:Gloom:gloom').
card_rarity('gloom'/'4ED', 'Uncommon').
card_artist('gloom'/'4ED', 'Dan Frazier').
card_multiverse_id('gloom'/'4ED', '2106').

card_in_set('goblin balloon brigade', '4ED').
card_original_type('goblin balloon brigade'/'4ED', 'Summon — Goblins').
card_original_text('goblin balloon brigade'/'4ED', '{R} Flying until end of turn').
card_image_name('goblin balloon brigade'/'4ED', 'goblin balloon brigade').
card_uid('goblin balloon brigade'/'4ED', '4ED:Goblin Balloon Brigade:goblin balloon brigade').
card_rarity('goblin balloon brigade'/'4ED', 'Uncommon').
card_artist('goblin balloon brigade'/'4ED', 'Andi Rusu').
card_flavor_text('goblin balloon brigade'/'4ED', '\"From up here we can drop rocks and arrows and more rocks!\"\n\"Uh, yeah boss, but how do we get down?\"').
card_multiverse_id('goblin balloon brigade'/'4ED', '2280').

card_in_set('goblin king', '4ED').
card_original_type('goblin king'/'4ED', 'Summon — Lord').
card_original_text('goblin king'/'4ED', 'All Goblins gain mountainwalk and get +1/+1.').
card_image_name('goblin king'/'4ED', 'goblin king').
card_uid('goblin king'/'4ED', '4ED:Goblin King:goblin king').
card_rarity('goblin king'/'4ED', 'Rare').
card_artist('goblin king'/'4ED', 'Jesper Myrfors').
card_flavor_text('goblin king'/'4ED', 'To become king of the Goblins, one must assassinate the previous king. Thus, only the most foolish seek positions of leadership.').
card_multiverse_id('goblin king'/'4ED', '2281').

card_in_set('goblin rock sled', '4ED').
card_original_type('goblin rock sled'/'4ED', 'Summon — Rock Sled').
card_original_text('goblin rock sled'/'4ED', 'Trample\nCannot attack if defending player controls no mountains. Rock Sled does not untap during your untap phase if it attacked during your last turn.').
card_image_name('goblin rock sled'/'4ED', 'goblin rock sled').
card_uid('goblin rock sled'/'4ED', '4ED:Goblin Rock Sled:goblin rock sled').
card_rarity('goblin rock sled'/'4ED', 'Common').
card_artist('goblin rock sled'/'4ED', 'Dennis Detwiller').
card_multiverse_id('goblin rock sled'/'4ED', '2282').

card_in_set('grapeshot catapult', '4ED').
card_original_type('grapeshot catapult'/'4ED', 'Artifact Creature').
card_original_text('grapeshot catapult'/'4ED', '{T}: Grapeshot Catapult deals 1 damage to target creature with flying.').
card_image_name('grapeshot catapult'/'4ED', 'grapeshot catapult').
card_uid('grapeshot catapult'/'4ED', '4ED:Grapeshot Catapult:grapeshot catapult').
card_rarity('grapeshot catapult'/'4ED', 'Common').
card_artist('grapeshot catapult'/'4ED', 'Dan Frazier').
card_flavor_text('grapeshot catapult'/'4ED', 'Recent research suggests these creatures were invented by Urza\'s and Mishra\'s original master, Tocasia, and that both brothers used them.').
card_multiverse_id('grapeshot catapult'/'4ED', '2045').

card_in_set('gray ogre', '4ED').
card_original_type('gray ogre'/'4ED', 'Summon — Ogre').
card_original_text('gray ogre'/'4ED', '').
card_image_name('gray ogre'/'4ED', 'gray ogre').
card_uid('gray ogre'/'4ED', '4ED:Gray Ogre:gray ogre').
card_rarity('gray ogre'/'4ED', 'Common').
card_artist('gray ogre'/'4ED', 'Dan Frazier').
card_flavor_text('gray ogre'/'4ED', 'The Ogre philosopher Gnerdel believed the purpose of life was to live as high on the food chain as possible. She refused to eat vegetarians, preferring to live entirely on creatures that preyed on sentient beings.').
card_multiverse_id('gray ogre'/'4ED', '2283').

card_in_set('greed', '4ED').
card_original_type('greed'/'4ED', 'Enchantment').
card_original_text('greed'/'4ED', '{B} Pay 2 life to draw a card. Effects that prevent or redirect damage cannot be used to counter this loss of life.').
card_image_name('greed'/'4ED', 'greed').
card_uid('greed'/'4ED', '4ED:Greed:greed').
card_rarity('greed'/'4ED', 'Rare').
card_artist('greed'/'4ED', 'Phil Foglio').
card_flavor_text('greed'/'4ED', '\"There is no calamity greater than lavish desires. . . . And there is no greater disaster than greed.\"\n—Tao Tê Ching 46').
card_multiverse_id('greed'/'4ED', '2107').

card_in_set('green mana battery', '4ED').
card_original_type('green mana battery'/'4ED', 'Artifact').
card_original_text('green mana battery'/'4ED', '{2}, {T}: Put one charge counter on Green Mana Battery.\n{T}: Add {G} to your mana pool and remove as many charge counters as you wish. For each charge counter removed from Green Mana Battery, add {G} to your mana pool. Play this ability as an interrupt.').
card_image_name('green mana battery'/'4ED', 'green mana battery').
card_uid('green mana battery'/'4ED', '4ED:Green Mana Battery:green mana battery').
card_rarity('green mana battery'/'4ED', 'Rare').
card_artist('green mana battery'/'4ED', 'Christopher Rush').
card_multiverse_id('green mana battery'/'4ED', '2046').

card_in_set('green ward', '4ED').
card_original_type('green ward'/'4ED', 'Enchant Creature').
card_original_text('green ward'/'4ED', 'Target creature gains protection from green. The protection granted by Green Ward does not destroy Green Ward.').
card_image_name('green ward'/'4ED', 'green ward').
card_uid('green ward'/'4ED', '4ED:Green Ward:green ward').
card_rarity('green ward'/'4ED', 'Uncommon').
card_artist('green ward'/'4ED', 'Dan Frazier').
card_multiverse_id('green ward'/'4ED', '2342').

card_in_set('grizzly bears', '4ED').
card_original_type('grizzly bears'/'4ED', 'Summon — Bears').
card_original_text('grizzly bears'/'4ED', '').
card_image_name('grizzly bears'/'4ED', 'grizzly bears').
card_uid('grizzly bears'/'4ED', '4ED:Grizzly Bears:grizzly bears').
card_rarity('grizzly bears'/'4ED', 'Common').
card_artist('grizzly bears'/'4ED', 'Jeff A. Menges').
card_flavor_text('grizzly bears'/'4ED', 'Don\'t try to outrun one of Dominia\'s Grizzlies; it\'ll catch you, knock you down, and eat you. Of course, you could run up a tree. In that case you\'ll get a nice view before it knocks the tree down and eats you.').
card_multiverse_id('grizzly bears'/'4ED', '2217').

card_in_set('healing salve', '4ED').
card_original_type('healing salve'/'4ED', 'Instant').
card_original_text('healing salve'/'4ED', 'Give target player 3 life, or prevent up to 3 damage to any creature or player.').
card_image_name('healing salve'/'4ED', 'healing salve').
card_uid('healing salve'/'4ED', '4ED:Healing Salve:healing salve').
card_rarity('healing salve'/'4ED', 'Common').
card_artist('healing salve'/'4ED', 'Dan Frazier').
card_multiverse_id('healing salve'/'4ED', '2343').

card_in_set('helm of chatzuk', '4ED').
card_original_type('helm of chatzuk'/'4ED', 'Artifact').
card_original_text('helm of chatzuk'/'4ED', '{1}, {T}: Target creature gains banding until end of turn.').
card_image_name('helm of chatzuk'/'4ED', 'helm of chatzuk').
card_uid('helm of chatzuk'/'4ED', '4ED:Helm of Chatzuk:helm of chatzuk').
card_rarity('helm of chatzuk'/'4ED', 'Rare').
card_artist('helm of chatzuk'/'4ED', 'Mark Tedin').
card_multiverse_id('helm of chatzuk'/'4ED', '2047').

card_in_set('hill giant', '4ED').
card_original_type('hill giant'/'4ED', 'Summon — Giant').
card_original_text('hill giant'/'4ED', '').
card_image_name('hill giant'/'4ED', 'hill giant').
card_uid('hill giant'/'4ED', '4ED:Hill Giant:hill giant').
card_rarity('hill giant'/'4ED', 'Common').
card_artist('hill giant'/'4ED', 'Dan Frazier').
card_flavor_text('hill giant'/'4ED', 'Fortunately, Hill Giants have large blind spots in which a human can easily hide. Unfortunately, these blind spots are beneath the bottoms of their feet.').
card_multiverse_id('hill giant'/'4ED', '2284').

card_in_set('holy armor', '4ED').
card_original_type('holy armor'/'4ED', 'Enchant Creature').
card_original_text('holy armor'/'4ED', 'Target creature gets +0/+2.\n{W} Target creature Holy Armor enchants gets +0/+1 until end of turn.').
card_image_name('holy armor'/'4ED', 'holy armor').
card_uid('holy armor'/'4ED', '4ED:Holy Armor:holy armor').
card_rarity('holy armor'/'4ED', 'Common').
card_artist('holy armor'/'4ED', 'Melissa A. Benson').
card_multiverse_id('holy armor'/'4ED', '2344').

card_in_set('holy strength', '4ED').
card_original_type('holy strength'/'4ED', 'Enchant Creature').
card_original_text('holy strength'/'4ED', 'Target creature gets +1/+2.').
card_image_name('holy strength'/'4ED', 'holy strength').
card_uid('holy strength'/'4ED', '4ED:Holy Strength:holy strength').
card_rarity('holy strength'/'4ED', 'Common').
card_artist('holy strength'/'4ED', 'Anson Maddocks').
card_multiverse_id('holy strength'/'4ED', '2345').

card_in_set('howl from beyond', '4ED').
card_original_type('howl from beyond'/'4ED', 'Instant').
card_original_text('howl from beyond'/'4ED', 'Target creature gets +X/+0 until end of turn.').
card_image_name('howl from beyond'/'4ED', 'howl from beyond').
card_uid('howl from beyond'/'4ED', '4ED:Howl from Beyond:howl from beyond').
card_rarity('howl from beyond'/'4ED', 'Common').
card_artist('howl from beyond'/'4ED', 'Mark Poole').
card_multiverse_id('howl from beyond'/'4ED', '2108').

card_in_set('howling mine', '4ED').
card_original_type('howling mine'/'4ED', 'Artifact').
card_original_text('howling mine'/'4ED', 'Each player draws one extra card during his or her draw phase.').
card_image_name('howling mine'/'4ED', 'howling mine').
card_uid('howling mine'/'4ED', '4ED:Howling Mine:howling mine').
card_rarity('howling mine'/'4ED', 'Rare').
card_artist('howling mine'/'4ED', 'Mark Poole').
card_multiverse_id('howling mine'/'4ED', '2048').

card_in_set('hurkyl\'s recall', '4ED').
card_original_type('hurkyl\'s recall'/'4ED', 'Instant').
card_original_text('hurkyl\'s recall'/'4ED', 'All artifacts in play owned by target player are returned to that player\'s hand.').
card_image_name('hurkyl\'s recall'/'4ED', 'hurkyl\'s recall').
card_uid('hurkyl\'s recall'/'4ED', '4ED:Hurkyl\'s Recall:hurkyl\'s recall').
card_rarity('hurkyl\'s recall'/'4ED', 'Rare').
card_artist('hurkyl\'s recall'/'4ED', 'NéNé Thomas').
card_flavor_text('hurkyl\'s recall'/'4ED', 'This spell, like many attributed to Drafna, was actually the work of his wife and former student, Hurkyl.').
card_multiverse_id('hurkyl\'s recall'/'4ED', '2160').

card_in_set('hurloon minotaur', '4ED').
card_original_type('hurloon minotaur'/'4ED', 'Summon — Minotaur').
card_original_text('hurloon minotaur'/'4ED', '').
card_image_name('hurloon minotaur'/'4ED', 'hurloon minotaur').
card_uid('hurloon minotaur'/'4ED', '4ED:Hurloon Minotaur:hurloon minotaur').
card_rarity('hurloon minotaur'/'4ED', 'Common').
card_artist('hurloon minotaur'/'4ED', 'Anson Maddocks').
card_flavor_text('hurloon minotaur'/'4ED', 'The Minotaurs of the Hurloon Mountains are known for their love of battle. They are also known for their hymns to the dead, sung for friend and foe alike. These hymns can last for days, filling the mountain valleys with their low, haunting sounds.').
card_multiverse_id('hurloon minotaur'/'4ED', '2285').

card_in_set('hurr jackal', '4ED').
card_original_type('hurr jackal'/'4ED', 'Summon — Jackal').
card_original_text('hurr jackal'/'4ED', '{T}: Target creature cannot regenerate this turn.').
card_image_name('hurr jackal'/'4ED', 'hurr jackal').
card_uid('hurr jackal'/'4ED', '4ED:Hurr Jackal:hurr jackal').
card_rarity('hurr jackal'/'4ED', 'Rare').
card_artist('hurr jackal'/'4ED', 'Drew Tucker').
card_multiverse_id('hurr jackal'/'4ED', '2286').

card_in_set('hurricane', '4ED').
card_original_type('hurricane'/'4ED', 'Sorcery').
card_original_text('hurricane'/'4ED', 'Hurricane deals X damage to each player and each creature with flying.').
card_image_name('hurricane'/'4ED', 'hurricane').
card_uid('hurricane'/'4ED', '4ED:Hurricane:hurricane').
card_rarity('hurricane'/'4ED', 'Uncommon').
card_artist('hurricane'/'4ED', 'Dameon Willich').
card_multiverse_id('hurricane'/'4ED', '2218').

card_in_set('hypnotic specter', '4ED').
card_original_type('hypnotic specter'/'4ED', 'Summon — Specter').
card_original_text('hypnotic specter'/'4ED', 'Flying\nAn opponent damaged by Specter discards a card at random from his or her hand. Ignore this effect if opponent has no cards in hand.').
card_image_name('hypnotic specter'/'4ED', 'hypnotic specter').
card_uid('hypnotic specter'/'4ED', '4ED:Hypnotic Specter:hypnotic specter').
card_rarity('hypnotic specter'/'4ED', 'Uncommon').
card_artist('hypnotic specter'/'4ED', 'Douglas Shuler').
card_flavor_text('hypnotic specter'/'4ED', '\"...There was no trace\nOf aught on that illumined face....\"\n—Samuel Coleridge, \"Phantom\"').
card_multiverse_id('hypnotic specter'/'4ED', '2109').

card_in_set('immolation', '4ED').
card_original_type('immolation'/'4ED', 'Enchant Creature').
card_original_text('immolation'/'4ED', 'Target creature gets +2/-2.').
card_image_name('immolation'/'4ED', 'immolation').
card_uid('immolation'/'4ED', '4ED:Immolation:immolation').
card_rarity('immolation'/'4ED', 'Common').
card_artist('immolation'/'4ED', 'Scott Kirschner').
card_multiverse_id('immolation'/'4ED', '2287').

card_in_set('inferno', '4ED').
card_original_type('inferno'/'4ED', 'Instant').
card_original_text('inferno'/'4ED', 'Inferno deals 6 damage to all players and all creatures.').
card_image_name('inferno'/'4ED', 'inferno').
card_uid('inferno'/'4ED', '4ED:Inferno:inferno').
card_rarity('inferno'/'4ED', 'Rare').
card_artist('inferno'/'4ED', 'Randy Asplund-Faith').
card_flavor_text('inferno'/'4ED', '\"Any scrap of compassion that still existed in my soul was permanently snuffed out when they cast me out into the flames.\"\n—Mairsil, called the Pretender').
card_multiverse_id('inferno'/'4ED', '2288').

card_in_set('instill energy', '4ED').
card_original_type('instill energy'/'4ED', 'Enchant Creature').
card_original_text('instill energy'/'4ED', 'Target creature can attack the turn it comes into play on your side.\n{0}: During your turn, untap target creature Instill Energy enchants. Use this ability only once each turn.').
card_image_name('instill energy'/'4ED', 'instill energy').
card_uid('instill energy'/'4ED', '4ED:Instill Energy:instill energy').
card_rarity('instill energy'/'4ED', 'Uncommon').
card_artist('instill energy'/'4ED', 'Dameon Willich').
card_multiverse_id('instill energy'/'4ED', '2219').

card_in_set('iron star', '4ED').
card_original_type('iron star'/'4ED', 'Artifact').
card_original_text('iron star'/'4ED', '{1}: Gain 1 life for a successfully cast red spell. Use this effect either when the spell is cast or later in the turn but only once for each red spell cast.').
card_image_name('iron star'/'4ED', 'iron star').
card_uid('iron star'/'4ED', '4ED:Iron Star:iron star').
card_rarity('iron star'/'4ED', 'Uncommon').
card_artist('iron star'/'4ED', 'Dan Frazier').
card_multiverse_id('iron star'/'4ED', '2049').

card_in_set('ironclaw orcs', '4ED').
card_original_type('ironclaw orcs'/'4ED', 'Summon — Orcs').
card_original_text('ironclaw orcs'/'4ED', 'Cannot be assigned to block any creature with power greater than 1.').
card_image_name('ironclaw orcs'/'4ED', 'ironclaw orcs').
card_uid('ironclaw orcs'/'4ED', '4ED:Ironclaw Orcs:ironclaw orcs').
card_rarity('ironclaw orcs'/'4ED', 'Common').
card_artist('ironclaw orcs'/'4ED', 'Anson Maddocks').
card_flavor_text('ironclaw orcs'/'4ED', 'Generations of genetic weeding have given rise to the deviously cowardly Ironclaw clan. To say that Orcs in general are vicious, depraved, and ignoble does not do justice to the Ironclaw.').
card_multiverse_id('ironclaw orcs'/'4ED', '2289').

card_in_set('ironroot treefolk', '4ED').
card_original_type('ironroot treefolk'/'4ED', 'Summon — Treefolk').
card_original_text('ironroot treefolk'/'4ED', '').
card_image_name('ironroot treefolk'/'4ED', 'ironroot treefolk').
card_uid('ironroot treefolk'/'4ED', '4ED:Ironroot Treefolk:ironroot treefolk').
card_rarity('ironroot treefolk'/'4ED', 'Common').
card_artist('ironroot treefolk'/'4ED', 'Jesper Myrfors').
card_flavor_text('ironroot treefolk'/'4ED', 'The mating habits of Treefolk, particularly the stalwart Ironroot Treefolk, are truly absurd. Molasses comes to mind. It\'s amazing the species can survive at all given such protracted periods of mate selection, conjugation, and gestation.').
card_multiverse_id('ironroot treefolk'/'4ED', '2220').

card_in_set('island', '4ED').
card_original_type('island'/'4ED', 'Land').
card_original_text('island'/'4ED', '{T}: Add {U} to your mana pool.').
card_image_name('island'/'4ED', 'island1').
card_uid('island'/'4ED', '4ED:Island:island1').
card_rarity('island'/'4ED', 'Basic Land').
card_artist('island'/'4ED', 'Mark Poole').
card_multiverse_id('island'/'4ED', '2390').

card_in_set('island', '4ED').
card_original_type('island'/'4ED', 'Land').
card_original_text('island'/'4ED', '{T}: Add {U} to your mana pool.').
card_image_name('island'/'4ED', 'island2').
card_uid('island'/'4ED', '4ED:Island:island2').
card_rarity('island'/'4ED', 'Basic Land').
card_artist('island'/'4ED', 'Mark Poole').
card_multiverse_id('island'/'4ED', '2389').

card_in_set('island', '4ED').
card_original_type('island'/'4ED', 'Land').
card_original_text('island'/'4ED', '{T}: Add {U} to your mana pool.').
card_image_name('island'/'4ED', 'island3').
card_uid('island'/'4ED', '4ED:Island:island3').
card_rarity('island'/'4ED', 'Basic Land').
card_artist('island'/'4ED', 'Mark Poole').
card_multiverse_id('island'/'4ED', '2391').

card_in_set('island fish jasconius', '4ED').
card_original_type('island fish jasconius'/'4ED', 'Summon — Island Fish').
card_original_text('island fish jasconius'/'4ED', 'Does not untap during your untap phase.\nCannot attack if defending player controls no islands. If at any time you control no islands, bury Island Fish Jasconius.\n{U}{U}{U} Untap Island Fish. Use this ability only during your upkeep.').
card_image_name('island fish jasconius'/'4ED', 'island fish jasconius').
card_uid('island fish jasconius'/'4ED', '4ED:Island Fish Jasconius:island fish jasconius').
card_rarity('island fish jasconius'/'4ED', 'Rare').
card_artist('island fish jasconius'/'4ED', 'Jesper Myrfors').
card_multiverse_id('island fish jasconius'/'4ED', '2161').

card_in_set('island sanctuary', '4ED').
card_original_type('island sanctuary'/'4ED', 'Enchantment').
card_original_text('island sanctuary'/'4ED', 'During your draw phase, you may draw one less card from your library. If you do so, until start of your next turn the only creatures that can attack you are those with flying or islandwalk.').
card_image_name('island sanctuary'/'4ED', 'island sanctuary').
card_uid('island sanctuary'/'4ED', '4ED:Island Sanctuary:island sanctuary').
card_rarity('island sanctuary'/'4ED', 'Rare').
card_artist('island sanctuary'/'4ED', 'Mark Poole').
card_multiverse_id('island sanctuary'/'4ED', '2346').

card_in_set('ivory cup', '4ED').
card_original_type('ivory cup'/'4ED', 'Artifact').
card_original_text('ivory cup'/'4ED', '{1}: Gain 1 life for a successfully cast white spell. Use this effect either when the spell is cast or later in the turn but only once for each white spell cast.').
card_image_name('ivory cup'/'4ED', 'ivory cup').
card_uid('ivory cup'/'4ED', '4ED:Ivory Cup:ivory cup').
card_rarity('ivory cup'/'4ED', 'Uncommon').
card_artist('ivory cup'/'4ED', 'Anson Maddocks').
card_multiverse_id('ivory cup'/'4ED', '2050').

card_in_set('ivory tower', '4ED').
card_original_type('ivory tower'/'4ED', 'Artifact').
card_original_text('ivory tower'/'4ED', 'At the beginning of your upkeep, gain 1 life for each card in your hand in excess of four.').
card_image_name('ivory tower'/'4ED', 'ivory tower').
card_uid('ivory tower'/'4ED', '4ED:Ivory Tower:ivory tower').
card_rarity('ivory tower'/'4ED', 'Rare').
card_artist('ivory tower'/'4ED', 'Margaret Organ-Kean').
card_flavor_text('ivory tower'/'4ED', 'Valuing scholarship above all else, the inhabitants of the Ivory Tower reward those who sacrifice power for knowledge.').
card_multiverse_id('ivory tower'/'4ED', '2051').

card_in_set('jade monolith', '4ED').
card_original_type('jade monolith'/'4ED', 'Artifact').
card_original_text('jade monolith'/'4ED', '{1}: Redirect to yourself all damage done to any creature. The source of the damage does not change.').
card_image_name('jade monolith'/'4ED', 'jade monolith').
card_uid('jade monolith'/'4ED', '4ED:Jade Monolith:jade monolith').
card_rarity('jade monolith'/'4ED', 'Rare').
card_artist('jade monolith'/'4ED', 'Anson Maddocks').
card_multiverse_id('jade monolith'/'4ED', '2052').

card_in_set('jandor\'s saddlebags', '4ED').
card_original_type('jandor\'s saddlebags'/'4ED', 'Artifact').
card_original_text('jandor\'s saddlebags'/'4ED', '{3}, {T}: Untap target creature.').
card_image_name('jandor\'s saddlebags'/'4ED', 'jandor\'s saddlebags').
card_uid('jandor\'s saddlebags'/'4ED', '4ED:Jandor\'s Saddlebags:jandor\'s saddlebags').
card_rarity('jandor\'s saddlebags'/'4ED', 'Rare').
card_artist('jandor\'s saddlebags'/'4ED', 'Dameon Willich').
card_flavor_text('jandor\'s saddlebags'/'4ED', 'Each day of their journey, Jandor opened the saddlebags and found them full of mutton, quinces, cheese, date rolls, wine, and all manner of delicious and satisfying foods.').
card_multiverse_id('jandor\'s saddlebags'/'4ED', '2053').

card_in_set('jayemdae tome', '4ED').
card_original_type('jayemdae tome'/'4ED', 'Artifact').
card_original_text('jayemdae tome'/'4ED', '{4}, {T}: Draw one card.').
card_image_name('jayemdae tome'/'4ED', 'jayemdae tome').
card_uid('jayemdae tome'/'4ED', '4ED:Jayemdae Tome:jayemdae tome').
card_rarity('jayemdae tome'/'4ED', 'Rare').
card_artist('jayemdae tome'/'4ED', 'Mark Tedin').
card_multiverse_id('jayemdae tome'/'4ED', '2054').

card_in_set('jump', '4ED').
card_original_type('jump'/'4ED', 'Instant').
card_original_text('jump'/'4ED', 'Target creature gains flying until end of turn.').
card_image_name('jump'/'4ED', 'jump').
card_uid('jump'/'4ED', '4ED:Jump:jump').
card_rarity('jump'/'4ED', 'Common').
card_artist('jump'/'4ED', 'Mark Poole').
card_multiverse_id('jump'/'4ED', '2162').

card_in_set('junún efreet', '4ED').
card_original_type('junún efreet'/'4ED', 'Summon — Efreet').
card_original_text('junún efreet'/'4ED', 'Flying\nDuring your upkeep, pay {B}{B} or bury Junún Efreet.').
card_image_name('junún efreet'/'4ED', 'junun efreet').
card_uid('junún efreet'/'4ED', '4ED:Junún Efreet:junun efreet').
card_rarity('junún efreet'/'4ED', 'Uncommon').
card_artist('junún efreet'/'4ED', 'Christopher Rush').
card_multiverse_id('junún efreet'/'4ED', '2110').

card_in_set('karma', '4ED').
card_original_type('karma'/'4ED', 'Enchantment').
card_original_text('karma'/'4ED', 'During each player\'s upkeep, Karma deals 1 damage to that player for each swamp he or she controls.').
card_image_name('karma'/'4ED', 'karma').
card_uid('karma'/'4ED', '4ED:Karma:karma').
card_rarity('karma'/'4ED', 'Uncommon').
card_artist('karma'/'4ED', 'Richard Thomas').
card_multiverse_id('karma'/'4ED', '2347').

card_in_set('keldon warlord', '4ED').
card_original_type('keldon warlord'/'4ED', 'Summon — Lord').
card_original_text('keldon warlord'/'4ED', 'Keldon Warlord has power and toughness each equal to the number of non-wall creatures you control, including Warlord. For example, if you control two other non-wall creatures, Warlord is 3/3. If one of those creatures leaves play, Warlord immediately becomes 2/2.').
card_image_name('keldon warlord'/'4ED', 'keldon warlord').
card_uid('keldon warlord'/'4ED', '4ED:Keldon Warlord:keldon warlord').
card_rarity('keldon warlord'/'4ED', 'Uncommon').
card_artist('keldon warlord'/'4ED', 'Kev Brockschmidt').
card_multiverse_id('keldon warlord'/'4ED', '2290').

card_in_set('killer bees', '4ED').
card_original_type('killer bees'/'4ED', 'Summon — Bees').
card_original_text('killer bees'/'4ED', 'Flying\n{G} +1/+1 until end of turn').
card_image_name('killer bees'/'4ED', 'killer bees').
card_uid('killer bees'/'4ED', '4ED:Killer Bees:killer bees').
card_rarity('killer bees'/'4ED', 'Uncommon').
card_artist('killer bees'/'4ED', 'Phil Foglio').
card_flavor_text('killer bees'/'4ED', 'The communal mind produces a savage strategy, yet no one could predict that this vicious crossbreed would unravel the secret of steel.').
card_multiverse_id('killer bees'/'4ED', '2221').

card_in_set('kismet', '4ED').
card_original_type('kismet'/'4ED', 'Enchantment').
card_original_text('kismet'/'4ED', 'All of target player\'s creatures, lands, and artifacts come into play tapped.').
card_image_name('kismet'/'4ED', 'kismet').
card_uid('kismet'/'4ED', '4ED:Kismet:kismet').
card_rarity('kismet'/'4ED', 'Uncommon').
card_artist('kismet'/'4ED', 'Kaja Foglio').
card_multiverse_id('kismet'/'4ED', '2348').

card_in_set('kormus bell', '4ED').
card_original_type('kormus bell'/'4ED', 'Artifact').
card_original_text('kormus bell'/'4ED', 'All swamps become 1/1 black creatures. The swamps still count as lands but cannot be tapped for mana the turn they come into play.').
card_image_name('kormus bell'/'4ED', 'kormus bell').
card_uid('kormus bell'/'4ED', '4ED:Kormus Bell:kormus bell').
card_rarity('kormus bell'/'4ED', 'Rare').
card_artist('kormus bell'/'4ED', 'Christopher Rush').
card_multiverse_id('kormus bell'/'4ED', '2055').

card_in_set('land leeches', '4ED').
card_original_type('land leeches'/'4ED', 'Summon — Leeches').
card_original_text('land leeches'/'4ED', 'First strike').
card_image_name('land leeches'/'4ED', 'land leeches').
card_uid('land leeches'/'4ED', '4ED:Land Leeches:land leeches').
card_rarity('land leeches'/'4ED', 'Common').
card_artist('land leeches'/'4ED', 'Quinton Hoover').
card_flavor_text('land leeches'/'4ED', '\"The standard cure for leeches requires the application of burning embers. Alternative methods must be devised should an ember of sufficient size prove more harmful than the leech.\"\n—Vervamon the Elder').
card_multiverse_id('land leeches'/'4ED', '2222').

card_in_set('land tax', '4ED').
card_original_type('land tax'/'4ED', 'Enchantment').
card_original_text('land tax'/'4ED', 'During your upkeep, if an opponent controls more land than you, you may search your library and remove up to three basic land cards and put them into your hand. Reshuffle your library afterwards.').
card_image_name('land tax'/'4ED', 'land tax').
card_uid('land tax'/'4ED', '4ED:Land Tax:land tax').
card_rarity('land tax'/'4ED', 'Rare').
card_artist('land tax'/'4ED', 'Brian Snõddy').
card_multiverse_id('land tax'/'4ED', '2349').

card_in_set('leviathan', '4ED').
card_original_type('leviathan'/'4ED', 'Summon — Leviathan').
card_original_text('leviathan'/'4ED', 'Trample\nComes into play tapped and does not untap during your untap phase.\nDuring your upkeep, you may sacrifice two islands to untap Leviathan.\nLeviathan cannot attack unless you sacrifice two islands during your attack.').
card_image_name('leviathan'/'4ED', 'leviathan').
card_uid('leviathan'/'4ED', '4ED:Leviathan:leviathan').
card_rarity('leviathan'/'4ED', 'Rare').
card_artist('leviathan'/'4ED', 'Mark Tedin').
card_multiverse_id('leviathan'/'4ED', '2163').

card_in_set('ley druid', '4ED').
card_original_type('ley druid'/'4ED', 'Summon — Cleric').
card_original_text('ley druid'/'4ED', '{T}: Untap target land. Play this ability as an interrupt.').
card_image_name('ley druid'/'4ED', 'ley druid').
card_uid('ley druid'/'4ED', '4ED:Ley Druid:ley druid').
card_rarity('ley druid'/'4ED', 'Uncommon').
card_artist('ley druid'/'4ED', 'Sandra Everingham').
card_flavor_text('ley druid'/'4ED', 'After years of training, the Druid becomes one with nature, drawing power from the land and returning it when needed.').
card_multiverse_id('ley druid'/'4ED', '2223').

card_in_set('library of leng', '4ED').
card_original_type('library of leng'/'4ED', 'Artifact').
card_original_text('library of leng'/'4ED', 'Skip the discard phase of your turn. If a spell or effect forces you to discard, you may discard to the top of your library rather than to your graveyard. If the discard is random, you may look at the card before choosing where to discard it.').
card_image_name('library of leng'/'4ED', 'library of leng').
card_uid('library of leng'/'4ED', '4ED:Library of Leng:library of leng').
card_rarity('library of leng'/'4ED', 'Uncommon').
card_artist('library of leng'/'4ED', 'Daniel Gelon').
card_multiverse_id('library of leng'/'4ED', '2056').

card_in_set('lifeforce', '4ED').
card_original_type('lifeforce'/'4ED', 'Enchantment').
card_original_text('lifeforce'/'4ED', '{G}{G}: Counter target black spell. Play this ability as an interrupt.').
card_image_name('lifeforce'/'4ED', 'lifeforce').
card_uid('lifeforce'/'4ED', '4ED:Lifeforce:lifeforce').
card_rarity('lifeforce'/'4ED', 'Uncommon').
card_artist('lifeforce'/'4ED', 'Dameon Willich').
card_multiverse_id('lifeforce'/'4ED', '2224').

card_in_set('lifelace', '4ED').
card_original_type('lifelace'/'4ED', 'Interrupt').
card_original_text('lifelace'/'4ED', 'Change the color of target spell or target permanent to green. Costs to cast, tap, maintain or use a special ability of target remain unchanged.').
card_image_name('lifelace'/'4ED', 'lifelace').
card_uid('lifelace'/'4ED', '4ED:Lifelace:lifelace').
card_rarity('lifelace'/'4ED', 'Rare').
card_artist('lifelace'/'4ED', 'Amy Weber').
card_multiverse_id('lifelace'/'4ED', '2225').

card_in_set('lifetap', '4ED').
card_original_type('lifetap'/'4ED', 'Enchantment').
card_original_text('lifetap'/'4ED', 'Gain 1 life each time a forest controlled by target opponent becomes tapped.').
card_image_name('lifetap'/'4ED', 'lifetap').
card_uid('lifetap'/'4ED', '4ED:Lifetap:lifetap').
card_rarity('lifetap'/'4ED', 'Uncommon').
card_artist('lifetap'/'4ED', 'Anson Maddocks').
card_multiverse_id('lifetap'/'4ED', '2164').

card_in_set('lightning bolt', '4ED').
card_original_type('lightning bolt'/'4ED', 'Instant').
card_original_text('lightning bolt'/'4ED', 'Lightning Bolt deals 3 damage to target creature or player.').
card_image_name('lightning bolt'/'4ED', 'lightning bolt').
card_uid('lightning bolt'/'4ED', '4ED:Lightning Bolt:lightning bolt').
card_rarity('lightning bolt'/'4ED', 'Common').
card_artist('lightning bolt'/'4ED', 'Christopher Rush').
card_multiverse_id('lightning bolt'/'4ED', '2291').

card_in_set('living artifact', '4ED').
card_original_type('living artifact'/'4ED', 'Enchant Artifact').
card_original_text('living artifact'/'4ED', 'Put a vitality counter on Living Artifact for each damage dealt to you.\n{0}: During your upkeep, remove a vitality counter to gain 1 life. Remove only one vitality counter during each of your upkeeps.').
card_image_name('living artifact'/'4ED', 'living artifact').
card_uid('living artifact'/'4ED', '4ED:Living Artifact:living artifact').
card_rarity('living artifact'/'4ED', 'Rare').
card_artist('living artifact'/'4ED', 'Anson Maddocks').
card_multiverse_id('living artifact'/'4ED', '2226').

card_in_set('living lands', '4ED').
card_original_type('living lands'/'4ED', 'Enchantment').
card_original_text('living lands'/'4ED', 'All forests become 1/1 creatures. The forests still count as lands but cannot be tapped for mana the turn they come into play.').
card_image_name('living lands'/'4ED', 'living lands').
card_uid('living lands'/'4ED', '4ED:Living Lands:living lands').
card_rarity('living lands'/'4ED', 'Rare').
card_artist('living lands'/'4ED', 'Jesper Myrfors').
card_multiverse_id('living lands'/'4ED', '2227').

card_in_set('llanowar elves', '4ED').
card_original_type('llanowar elves'/'4ED', 'Summon — Elves').
card_original_text('llanowar elves'/'4ED', '{T}: Add {G} to your mana pool. Play this ability as an interrupt.').
card_image_name('llanowar elves'/'4ED', 'llanowar elves').
card_uid('llanowar elves'/'4ED', '4ED:Llanowar Elves:llanowar elves').
card_rarity('llanowar elves'/'4ED', 'Common').
card_artist('llanowar elves'/'4ED', 'Anson Maddocks').
card_flavor_text('llanowar elves'/'4ED', 'Hardened by their life in the haunted Llanowar Forest, these fierce beings are outcasts among elvenkind.').
card_multiverse_id('llanowar elves'/'4ED', '2228').

card_in_set('lord of atlantis', '4ED').
card_original_type('lord of atlantis'/'4ED', 'Summon — Lord').
card_original_text('lord of atlantis'/'4ED', 'All Merfolk gain islandwalk and get +1/+1.').
card_image_name('lord of atlantis'/'4ED', 'lord of atlantis').
card_uid('lord of atlantis'/'4ED', '4ED:Lord of Atlantis:lord of atlantis').
card_rarity('lord of atlantis'/'4ED', 'Rare').
card_artist('lord of atlantis'/'4ED', 'Melissa A. Benson').
card_flavor_text('lord of atlantis'/'4ED', 'A master of tactics, the Lord of Atlantis makes his people bold in battle merely by arriving to lead them.').
card_multiverse_id('lord of atlantis'/'4ED', '2165').

card_in_set('lord of the pit', '4ED').
card_original_type('lord of the pit'/'4ED', 'Summon — Demon').
card_original_text('lord of the pit'/'4ED', 'Flying, trample\nDuring your upkeep, sacrifice a creature. If you cannot sacrifice a creature, Lord of the Pit deals 7 damage to you. You cannot sacrifice Lord of the Pit to itself.').
card_image_name('lord of the pit'/'4ED', 'lord of the pit').
card_uid('lord of the pit'/'4ED', '4ED:Lord of the Pit:lord of the pit').
card_rarity('lord of the pit'/'4ED', 'Rare').
card_artist('lord of the pit'/'4ED', 'Mark Tedin').
card_multiverse_id('lord of the pit'/'4ED', '2111').

card_in_set('lost soul', '4ED').
card_original_type('lost soul'/'4ED', 'Summon — Lost Soul').
card_original_text('lost soul'/'4ED', 'Swampwalk').
card_image_name('lost soul'/'4ED', 'lost soul').
card_uid('lost soul'/'4ED', '4ED:Lost Soul:lost soul').
card_rarity('lost soul'/'4ED', 'Common').
card_artist('lost soul'/'4ED', 'Randy Asplund-Faith').
card_flavor_text('lost soul'/'4ED', 'Her hand gently beckons,\nshe whispers your name—\nBut those who go with her\nare never the same.').
card_multiverse_id('lost soul'/'4ED', '2112').

card_in_set('lure', '4ED').
card_original_type('lure'/'4ED', 'Enchant Creature').
card_original_text('lure'/'4ED', 'All creatures able to block target creature must do so. Lure does not prevent a creature from blocking more than one creature if blocker has that ability. If blocker is forced to block more creatures than it is allowed to, defender chooses which of these creatures to block, but must block as many creatures as allowed.').
card_image_name('lure'/'4ED', 'lure').
card_uid('lure'/'4ED', '4ED:Lure:lure').
card_rarity('lure'/'4ED', 'Uncommon').
card_artist('lure'/'4ED', 'Anson Maddocks').
card_multiverse_id('lure'/'4ED', '2229').

card_in_set('magical hack', '4ED').
card_original_type('magical hack'/'4ED', 'Interrupt').
card_original_text('magical hack'/'4ED', 'Change the text of target spell or target permanent by replacing all occurrences of one basic land type with another. For example, you may change \"swampwalk\" to \"plainswalk.\"').
card_image_name('magical hack'/'4ED', 'magical hack').
card_uid('magical hack'/'4ED', '4ED:Magical Hack:magical hack').
card_rarity('magical hack'/'4ED', 'Rare').
card_artist('magical hack'/'4ED', 'Julie Baroh').
card_multiverse_id('magical hack'/'4ED', '2166').

card_in_set('magnetic mountain', '4ED').
card_original_type('magnetic mountain'/'4ED', 'Enchantment').
card_original_text('magnetic mountain'/'4ED', 'Blue creatures do not untap during their controllers\' untap phase. During his or her upkeep, a player may pay an additional {4} to untap a blue creature he or she controls.').
card_image_name('magnetic mountain'/'4ED', 'magnetic mountain').
card_uid('magnetic mountain'/'4ED', '4ED:Magnetic Mountain:magnetic mountain').
card_rarity('magnetic mountain'/'4ED', 'Rare').
card_artist('magnetic mountain'/'4ED', 'Susan Van Camp').
card_multiverse_id('magnetic mountain'/'4ED', '2292').

card_in_set('mahamoti djinn', '4ED').
card_original_type('mahamoti djinn'/'4ED', 'Summon — Djinn').
card_original_text('mahamoti djinn'/'4ED', 'Flying').
card_image_name('mahamoti djinn'/'4ED', 'mahamoti djinn').
card_uid('mahamoti djinn'/'4ED', '4ED:Mahamoti Djinn:mahamoti djinn').
card_rarity('mahamoti djinn'/'4ED', 'Rare').
card_artist('mahamoti djinn'/'4ED', 'Dan Frazier').
card_flavor_text('mahamoti djinn'/'4ED', 'Of royal blood among the spirits of the air, the Mahamoti Djinn rides on the wings of the winds. As dangerous in the gambling hall as he is in battle, he is a master of trickery and misdirection.').
card_multiverse_id('mahamoti djinn'/'4ED', '2167').

card_in_set('mana clash', '4ED').
card_original_type('mana clash'/'4ED', 'Sorcery').
card_original_text('mana clash'/'4ED', 'You and target opponent each flip a coin. Mana Clash deals 1 damage to any player whose coin comes up tails. Repeat this process until both players\' coins come up heads at the same time.').
card_image_name('mana clash'/'4ED', 'mana clash').
card_uid('mana clash'/'4ED', '4ED:Mana Clash:mana clash').
card_rarity('mana clash'/'4ED', 'Rare').
card_artist('mana clash'/'4ED', 'Mark Tedin').
card_multiverse_id('mana clash'/'4ED', '2293').

card_in_set('mana flare', '4ED').
card_original_type('mana flare'/'4ED', 'Enchantment').
card_original_text('mana flare'/'4ED', 'Whenever a player taps a land for mana, it produces one additional mana of the same type.').
card_image_name('mana flare'/'4ED', 'mana flare').
card_uid('mana flare'/'4ED', '4ED:Mana Flare:mana flare').
card_rarity('mana flare'/'4ED', 'Rare').
card_artist('mana flare'/'4ED', 'Christopher Rush').
card_multiverse_id('mana flare'/'4ED', '2294').

card_in_set('mana short', '4ED').
card_original_type('mana short'/'4ED', 'Instant').
card_original_text('mana short'/'4ED', 'Mana Short empties target player\'s mana pool and taps that player\'s lands.').
card_image_name('mana short'/'4ED', 'mana short').
card_uid('mana short'/'4ED', '4ED:Mana Short:mana short').
card_rarity('mana short'/'4ED', 'Rare').
card_artist('mana short'/'4ED', 'Dameon Willich').
card_multiverse_id('mana short'/'4ED', '2168').

card_in_set('mana vault', '4ED').
card_original_type('mana vault'/'4ED', 'Artifact').
card_original_text('mana vault'/'4ED', 'Mana Vault does not untap during your untap phase. If it remains tapped during your upkeep, Mana Vault deals 1 damage to you.\n{4}: Untap Mana Vault. Use this ability only during your upkeep.\n{T}: Add three colorless mana to your mana pool. Play these additions as interrupts.').
card_image_name('mana vault'/'4ED', 'mana vault').
card_uid('mana vault'/'4ED', '4ED:Mana Vault:mana vault').
card_rarity('mana vault'/'4ED', 'Rare').
card_artist('mana vault'/'4ED', 'Mark Tedin').
card_multiverse_id('mana vault'/'4ED', '2057').

card_in_set('manabarbs', '4ED').
card_original_type('manabarbs'/'4ED', 'Enchantment').
card_original_text('manabarbs'/'4ED', 'Each time any land is tapped for mana, Manabarbs deals 1 damage to that land\'s controller.').
card_image_name('manabarbs'/'4ED', 'manabarbs').
card_uid('manabarbs'/'4ED', '4ED:Manabarbs:manabarbs').
card_rarity('manabarbs'/'4ED', 'Rare').
card_artist('manabarbs'/'4ED', 'Christopher Rush').
card_multiverse_id('manabarbs'/'4ED', '2295').

card_in_set('marsh gas', '4ED').
card_original_type('marsh gas'/'4ED', 'Instant').
card_original_text('marsh gas'/'4ED', 'All creatures get -2/-0 until end of turn.').
card_image_name('marsh gas'/'4ED', 'marsh gas').
card_uid('marsh gas'/'4ED', '4ED:Marsh Gas:marsh gas').
card_rarity('marsh gas'/'4ED', 'Common').
card_artist('marsh gas'/'4ED', 'Douglas Shuler').
card_flavor_text('marsh gas'/'4ED', '\"Comes right outta th\' ground. If ya can smell it, it\'s too late.\"\n—Keevy Bogsbury').
card_multiverse_id('marsh gas'/'4ED', '2113').

card_in_set('marsh viper', '4ED').
card_original_type('marsh viper'/'4ED', 'Summon — Viper').
card_original_text('marsh viper'/'4ED', 'If Marsh Viper damages a player, he or she gets two poison counters. If a player has ten or more poison counters, he or she loses the game.').
card_image_name('marsh viper'/'4ED', 'marsh viper').
card_uid('marsh viper'/'4ED', '4ED:Marsh Viper:marsh viper').
card_rarity('marsh viper'/'4ED', 'Common').
card_artist('marsh viper'/'4ED', 'Ron Spencer').
card_flavor_text('marsh viper'/'4ED', '\"All we had left were their black and bloated bodies.\" —Maeveen O\'Donagh, Memoirs of a Soldier').
card_multiverse_id('marsh viper'/'4ED', '2230').

card_in_set('meekstone', '4ED').
card_original_type('meekstone'/'4ED', 'Artifact').
card_original_text('meekstone'/'4ED', 'No creatures with power greater than 2 untap during their controllers\' untap phase.').
card_image_name('meekstone'/'4ED', 'meekstone').
card_uid('meekstone'/'4ED', '4ED:Meekstone:meekstone').
card_rarity('meekstone'/'4ED', 'Rare').
card_artist('meekstone'/'4ED', 'Quinton Hoover').
card_multiverse_id('meekstone'/'4ED', '2058').

card_in_set('merfolk of the pearl trident', '4ED').
card_original_type('merfolk of the pearl trident'/'4ED', 'Summon — Merfolk').
card_original_text('merfolk of the pearl trident'/'4ED', '').
card_image_name('merfolk of the pearl trident'/'4ED', 'merfolk of the pearl trident').
card_uid('merfolk of the pearl trident'/'4ED', '4ED:Merfolk of the Pearl Trident:merfolk of the pearl trident').
card_rarity('merfolk of the pearl trident'/'4ED', 'Common').
card_artist('merfolk of the pearl trident'/'4ED', 'Jeff A. Menges').
card_flavor_text('merfolk of the pearl trident'/'4ED', 'Most human scholars believe that Merfolk are the survivors of sunken Atlantis, humans adapted to the water. Merfolk, however, believe that humans sprang forth from Merfolk who adapted themselves in order to explore their last frontier.').
card_multiverse_id('merfolk of the pearl trident'/'4ED', '2169').

card_in_set('mesa pegasus', '4ED').
card_original_type('mesa pegasus'/'4ED', 'Summon — Pegasus').
card_original_text('mesa pegasus'/'4ED', 'Flying, banding').
card_image_name('mesa pegasus'/'4ED', 'mesa pegasus').
card_uid('mesa pegasus'/'4ED', '4ED:Mesa Pegasus:mesa pegasus').
card_rarity('mesa pegasus'/'4ED', 'Common').
card_artist('mesa pegasus'/'4ED', 'Melissa A. Benson').
card_flavor_text('mesa pegasus'/'4ED', 'Before a woman marries in the village of Sursi, she must visit the land of the Mesa Pegasus. Legend has it that if the woman is pure of heart and her love is true, a Mesa Pegasus will appear, blessing her family with long life and good fortune.').
card_multiverse_id('mesa pegasus'/'4ED', '2350').

card_in_set('millstone', '4ED').
card_original_type('millstone'/'4ED', 'Artifact').
card_original_text('millstone'/'4ED', '{2}, {T}: Take the top two cards from target player\'s library and put them into that player\'s graveyard.').
card_image_name('millstone'/'4ED', 'millstone').
card_uid('millstone'/'4ED', '4ED:Millstone:millstone').
card_rarity('millstone'/'4ED', 'Rare').
card_artist('millstone'/'4ED', 'Kaja Foglio').
card_flavor_text('millstone'/'4ED', 'More than one mage was driven insane by the sound of the Millstone relentlessly grinding away.').
card_multiverse_id('millstone'/'4ED', '2059').

card_in_set('mind bomb', '4ED').
card_original_type('mind bomb'/'4ED', 'Sorcery').
card_original_text('mind bomb'/'4ED', 'Mind Bomb deals 3 damage to each player. All players may discard up to three cards of their choice from their hands. Each card a player discards in this manner prevents 1 damage to that player from Mind Bomb.').
card_image_name('mind bomb'/'4ED', 'mind bomb').
card_uid('mind bomb'/'4ED', '4ED:Mind Bomb:mind bomb').
card_rarity('mind bomb'/'4ED', 'Uncommon').
card_artist('mind bomb'/'4ED', 'Mark Tedin').
card_multiverse_id('mind bomb'/'4ED', '2170').

card_in_set('mind twist', '4ED').
card_original_type('mind twist'/'4ED', 'Sorcery').
card_original_text('mind twist'/'4ED', 'Target player discards X cards at random from his or her hand. If that player does not have enough cards, his or her entire hand is discarded.').
card_image_name('mind twist'/'4ED', 'mind twist').
card_uid('mind twist'/'4ED', '4ED:Mind Twist:mind twist').
card_rarity('mind twist'/'4ED', 'Rare').
card_artist('mind twist'/'4ED', 'Julie Baroh').
card_multiverse_id('mind twist'/'4ED', '2114').

card_in_set('mishra\'s factory', '4ED').
card_original_type('mishra\'s factory'/'4ED', 'Land').
card_original_text('mishra\'s factory'/'4ED', '{T}: Add 1 colorless mana to your mana pool.\n{1}: Mishra\'s Factory becomes an Assembly Worker, a 2/2 artifact creature, until end of turn. Assembly Worker still counts as a land but cannot be tapped for mana the turn it comes into play.\n{T}: Target Assembly Worker gets +1/+1 until end of turn.').
card_image_name('mishra\'s factory'/'4ED', 'mishra\'s factory').
card_uid('mishra\'s factory'/'4ED', '4ED:Mishra\'s Factory:mishra\'s factory').
card_rarity('mishra\'s factory'/'4ED', 'Uncommon').
card_artist('mishra\'s factory'/'4ED', 'Kaja & Phil Foglio').
card_multiverse_id('mishra\'s factory'/'4ED', '2387').

card_in_set('mishra\'s war machine', '4ED').
card_original_type('mishra\'s war machine'/'4ED', 'Artifact Creature').
card_original_text('mishra\'s war machine'/'4ED', 'Banding\nDuring your upkeep, choose and discard one card from your hand, or Mishra\'s War Machine deals 3 damage to you. If Mishra\'s War Machine deals damage to you in this way, tap it.').
card_image_name('mishra\'s war machine'/'4ED', 'mishra\'s war machine').
card_uid('mishra\'s war machine'/'4ED', '4ED:Mishra\'s War Machine:mishra\'s war machine').
card_rarity('mishra\'s war machine'/'4ED', 'Rare').
card_artist('mishra\'s war machine'/'4ED', 'Amy Weber').
card_multiverse_id('mishra\'s war machine'/'4ED', '2060').

card_in_set('mons\'s goblin raiders', '4ED').
card_original_type('mons\'s goblin raiders'/'4ED', 'Summon — Goblins').
card_original_text('mons\'s goblin raiders'/'4ED', '').
card_image_name('mons\'s goblin raiders'/'4ED', 'mons\'s goblin raiders').
card_uid('mons\'s goblin raiders'/'4ED', '4ED:Mons\'s Goblin Raiders:mons\'s goblin raiders').
card_rarity('mons\'s goblin raiders'/'4ED', 'Common').
card_artist('mons\'s goblin raiders'/'4ED', 'Jeff A. Menges').
card_flavor_text('mons\'s goblin raiders'/'4ED', 'The intricate dynamics of Rundvelt Goblin affairs are often confused with anarchy. The chaos, however, is the chaos of a thundercloud, and direction will sporadically and violently appear. Pashalik Mons and his Raiders are the thunderhead that leads in the storm.').
card_multiverse_id('mons\'s goblin raiders'/'4ED', '2296').

card_in_set('morale', '4ED').
card_original_type('morale'/'4ED', 'Instant').
card_original_text('morale'/'4ED', 'All attacking creatures get +1/+1 until end of turn.').
card_image_name('morale'/'4ED', 'morale').
card_uid('morale'/'4ED', '4ED:Morale:morale').
card_rarity('morale'/'4ED', 'Common').
card_artist('morale'/'4ED', 'Mark Poole').
card_flavor_text('morale'/'4ED', '\"After Lacjsi\'s speech, the Knights grew determined to crush their ancient enemies clan by clan.\"\n—Tivadar of Thorn, History of the Goblin Wars').
card_multiverse_id('morale'/'4ED', '2351').

card_in_set('mountain', '4ED').
card_original_type('mountain'/'4ED', 'Land').
card_original_text('mountain'/'4ED', '{T}: Add {R} to your mana pool.').
card_image_name('mountain'/'4ED', 'mountain1').
card_uid('mountain'/'4ED', '4ED:Mountain:mountain1').
card_rarity('mountain'/'4ED', 'Basic Land').
card_artist('mountain'/'4ED', 'Douglas Shuler').
card_multiverse_id('mountain'/'4ED', '2383').

card_in_set('mountain', '4ED').
card_original_type('mountain'/'4ED', 'Land').
card_original_text('mountain'/'4ED', '{T}: Add {R} to your mana pool.').
card_image_name('mountain'/'4ED', 'mountain2').
card_uid('mountain'/'4ED', '4ED:Mountain:mountain2').
card_rarity('mountain'/'4ED', 'Basic Land').
card_artist('mountain'/'4ED', 'Douglas Shuler').
card_multiverse_id('mountain'/'4ED', '2381').

card_in_set('mountain', '4ED').
card_original_type('mountain'/'4ED', 'Land').
card_original_text('mountain'/'4ED', '{T}: Add {R} to your mana pool.').
card_image_name('mountain'/'4ED', 'mountain3').
card_uid('mountain'/'4ED', '4ED:Mountain:mountain3').
card_rarity('mountain'/'4ED', 'Basic Land').
card_artist('mountain'/'4ED', 'Douglas Shuler').
card_multiverse_id('mountain'/'4ED', '2382').

card_in_set('murk dwellers', '4ED').
card_original_type('murk dwellers'/'4ED', 'Summon — Murk Dwellers').
card_original_text('murk dwellers'/'4ED', 'When attacking and not blocked, Murk Dwellers gets +2/+0 until end of turn.').
card_image_name('murk dwellers'/'4ED', 'murk dwellers').
card_uid('murk dwellers'/'4ED', '4ED:Murk Dwellers:murk dwellers').
card_rarity('murk dwellers'/'4ED', 'Common').
card_artist('murk dwellers'/'4ED', 'Drew Tucker').
card_flavor_text('murk dwellers'/'4ED', 'When Raganorn unsealed the catacombs, he found more than the dead and their treasures.').
card_multiverse_id('murk dwellers'/'4ED', '2115').

card_in_set('nafs asp', '4ED').
card_original_type('nafs asp'/'4ED', 'Summon — Asp').
card_original_text('nafs asp'/'4ED', 'If Nafs Asp damages a player, it also deals 1 damage to that player during his or her next draw phase. Before then, the player may pay {1} to prevent this damage.').
card_image_name('nafs asp'/'4ED', 'nafs asp').
card_uid('nafs asp'/'4ED', '4ED:Nafs Asp:nafs asp').
card_rarity('nafs asp'/'4ED', 'Common').
card_artist('nafs asp'/'4ED', 'Christopher Rush').
card_multiverse_id('nafs asp'/'4ED', '2231').

card_in_set('nether shadow', '4ED').
card_original_type('nether shadow'/'4ED', 'Summon — Shadow').
card_original_text('nether shadow'/'4ED', 'At the end of your upkeep, if Shadow is in your graveyard with at least three creature cards above it, you may return it to play. Shadow can attack the turn it comes into play.').
card_image_name('nether shadow'/'4ED', 'nether shadow').
card_uid('nether shadow'/'4ED', '4ED:Nether Shadow:nether shadow').
card_rarity('nether shadow'/'4ED', 'Rare').
card_artist('nether shadow'/'4ED', 'Christopher Rush').
card_multiverse_id('nether shadow'/'4ED', '2116').

card_in_set('nevinyrral\'s disk', '4ED').
card_original_type('nevinyrral\'s disk'/'4ED', 'Artifact').
card_original_text('nevinyrral\'s disk'/'4ED', 'Comes into play tapped.\n{1}, {T}: Destroy all creatures, enchantments, and artifacts, including Nevinyrral\'s Disk itself.').
card_image_name('nevinyrral\'s disk'/'4ED', 'nevinyrral\'s disk').
card_uid('nevinyrral\'s disk'/'4ED', '4ED:Nevinyrral\'s Disk:nevinyrral\'s disk').
card_rarity('nevinyrral\'s disk'/'4ED', 'Rare').
card_artist('nevinyrral\'s disk'/'4ED', 'Mark Tedin').
card_multiverse_id('nevinyrral\'s disk'/'4ED', '2061').

card_in_set('nightmare', '4ED').
card_original_type('nightmare'/'4ED', 'Summon — Nightmare').
card_original_text('nightmare'/'4ED', 'Flying\nNightmare has power and toughness each equal to the number of swamps its controller controls.').
card_image_name('nightmare'/'4ED', 'nightmare').
card_uid('nightmare'/'4ED', '4ED:Nightmare:nightmare').
card_rarity('nightmare'/'4ED', 'Rare').
card_artist('nightmare'/'4ED', 'Melissa A. Benson').
card_flavor_text('nightmare'/'4ED', 'The Nightmare arises from its lair in the swamps. As the poisoned land spreads, so does the Nightmare\'s rage and terrifying strength.').
card_multiverse_id('nightmare'/'4ED', '2117').

card_in_set('northern paladin', '4ED').
card_original_type('northern paladin'/'4ED', 'Summon — Paladin').
card_original_text('northern paladin'/'4ED', '{W}{W}, {T}: Destroy target black permanent.').
card_image_name('northern paladin'/'4ED', 'northern paladin').
card_uid('northern paladin'/'4ED', '4ED:Northern Paladin:northern paladin').
card_rarity('northern paladin'/'4ED', 'Rare').
card_artist('northern paladin'/'4ED', 'Douglas Shuler').
card_flavor_text('northern paladin'/'4ED', '\"Look to the north; there you will find aid and comfort.\" —The Book of Tal').
card_multiverse_id('northern paladin'/'4ED', '2352').

card_in_set('oasis', '4ED').
card_original_type('oasis'/'4ED', 'Land').
card_original_text('oasis'/'4ED', '{T}: Prevent 1 damage to any creature.').
card_image_name('oasis'/'4ED', 'oasis').
card_uid('oasis'/'4ED', '4ED:Oasis:oasis').
card_rarity('oasis'/'4ED', 'Uncommon').
card_artist('oasis'/'4ED', 'Brian Snõddy').
card_multiverse_id('oasis'/'4ED', '2388').

card_in_set('obsianus golem', '4ED').
card_original_type('obsianus golem'/'4ED', 'Artifact Creature').
card_original_text('obsianus golem'/'4ED', '').
card_image_name('obsianus golem'/'4ED', 'obsianus golem').
card_uid('obsianus golem'/'4ED', '4ED:Obsianus Golem:obsianus golem').
card_rarity('obsianus golem'/'4ED', 'Uncommon').
card_artist('obsianus golem'/'4ED', 'Jesper Myrfors').
card_flavor_text('obsianus golem'/'4ED', '\"The foot stone is connected to the ankle stone, the ankle stone is connected to the leg stone . . . .\"\n—Song of the Artificer').
card_multiverse_id('obsianus golem'/'4ED', '2062').

card_in_set('onulet', '4ED').
card_original_type('onulet'/'4ED', 'Artifact Creature').
card_original_text('onulet'/'4ED', 'If Onulet is put into the graveyard from play, you gain 2 life.').
card_image_name('onulet'/'4ED', 'onulet').
card_uid('onulet'/'4ED', '4ED:Onulet:onulet').
card_rarity('onulet'/'4ED', 'Rare').
card_artist('onulet'/'4ED', 'Anson Maddocks').
card_flavor_text('onulet'/'4ED', 'An early inspiration for Urza, Tocasia\'s Onulets contained magical essences that could be cannibalized after they stopped functioning.').
card_multiverse_id('onulet'/'4ED', '2063').

card_in_set('orcish artillery', '4ED').
card_original_type('orcish artillery'/'4ED', 'Summon — Orcs').
card_original_text('orcish artillery'/'4ED', '{T}: Orcish Artillery deals 2 damage to target creature or player and 3 damage to you.').
card_image_name('orcish artillery'/'4ED', 'orcish artillery').
card_uid('orcish artillery'/'4ED', '4ED:Orcish Artillery:orcish artillery').
card_rarity('orcish artillery'/'4ED', 'Uncommon').
card_artist('orcish artillery'/'4ED', 'Anson Maddocks').
card_flavor_text('orcish artillery'/'4ED', 'In a rare display of ingenuity, the Orcs invented an incredibly destructive weapon. Most Orcish artillerists are those who dared criticize its effectiveness.').
card_multiverse_id('orcish artillery'/'4ED', '2297').

card_in_set('orcish oriflamme', '4ED').
card_original_type('orcish oriflamme'/'4ED', 'Enchantment').
card_original_text('orcish oriflamme'/'4ED', 'All attacking creatures you control get +1/+0.').
card_image_name('orcish oriflamme'/'4ED', 'orcish oriflamme').
card_uid('orcish oriflamme'/'4ED', '4ED:Orcish Oriflamme:orcish oriflamme').
card_rarity('orcish oriflamme'/'4ED', 'Uncommon').
card_artist('orcish oriflamme'/'4ED', 'Dan Frazier').
card_multiverse_id('orcish oriflamme'/'4ED', '2298').

card_in_set('ornithopter', '4ED').
card_original_type('ornithopter'/'4ED', 'Artifact Creature').
card_original_text('ornithopter'/'4ED', 'Flying').
card_image_name('ornithopter'/'4ED', 'ornithopter').
card_uid('ornithopter'/'4ED', '4ED:Ornithopter:ornithopter').
card_rarity('ornithopter'/'4ED', 'Uncommon').
card_artist('ornithopter'/'4ED', 'Amy Weber').
card_flavor_text('ornithopter'/'4ED', 'Many scholars believe that these creatures were the result of Urza\'s first attempt at mechanical life, perhaps created in his early days as an apprentice to Tocasia.').
card_multiverse_id('ornithopter'/'4ED', '2064').

card_in_set('osai vultures', '4ED').
card_original_type('osai vultures'/'4ED', 'Summon — Vultures').
card_original_text('osai vultures'/'4ED', 'Flying\nAt the end of any turn in which a creature is put into the graveyard from play, put a carrion counter on Vultures.\n{0}: Remove two carrion counters to give Vultures +1/+1 until end of turn.').
card_image_name('osai vultures'/'4ED', 'osai vultures').
card_uid('osai vultures'/'4ED', '4ED:Osai Vultures:osai vultures').
card_rarity('osai vultures'/'4ED', 'Uncommon').
card_artist('osai vultures'/'4ED', 'Dan Frazier').
card_multiverse_id('osai vultures'/'4ED', '2353').

card_in_set('paralyze', '4ED').
card_original_type('paralyze'/'4ED', 'Enchant Creature').
card_original_text('paralyze'/'4ED', 'Target creature does not untap during its controller\'s untap phase. That player may pay an additional {4} during his or her upkeep to untap it. Tap target creatures when Paralyze comes into play.').
card_image_name('paralyze'/'4ED', 'paralyze').
card_uid('paralyze'/'4ED', '4ED:Paralyze:paralyze').
card_rarity('paralyze'/'4ED', 'Common').
card_artist('paralyze'/'4ED', 'Anson Maddocks').
card_multiverse_id('paralyze'/'4ED', '2118').

card_in_set('pearled unicorn', '4ED').
card_original_type('pearled unicorn'/'4ED', 'Summon — Unicorn').
card_original_text('pearled unicorn'/'4ED', '').
card_image_name('pearled unicorn'/'4ED', 'pearled unicorn').
card_uid('pearled unicorn'/'4ED', '4ED:Pearled Unicorn:pearled unicorn').
card_rarity('pearled unicorn'/'4ED', 'Common').
card_artist('pearled unicorn'/'4ED', 'Cornelius Brudi').
card_flavor_text('pearled unicorn'/'4ED', '\"‘Do you know, I always thought Unicorns were fabulous monsters, too? I never saw one alive before!\' ‘Well, now that we have seen each other,\' said the Unicorn, ‘if you\'ll believe in me, I\'ll believe in you.\'\" —Lewis Carroll').
card_multiverse_id('pearled unicorn'/'4ED', '2354').

card_in_set('personal incarnation', '4ED').
card_original_type('personal incarnation'/'4ED', 'Summon — Avatar').
card_original_text('personal incarnation'/'4ED', 'Owner may redirect any or all damage done to Personal Incarnation to self instead. If Personal Incarnation is put into the graveyard from play, owner loses half his or her remaining life, rounding up the loss. Effects that redirect or prevent damage cannot be used to counter this loss of life.').
card_image_name('personal incarnation'/'4ED', 'personal incarnation').
card_uid('personal incarnation'/'4ED', '4ED:Personal Incarnation:personal incarnation').
card_rarity('personal incarnation'/'4ED', 'Rare').
card_artist('personal incarnation'/'4ED', 'Kev Brockschmidt').
card_multiverse_id('personal incarnation'/'4ED', '2355').

card_in_set('pestilence', '4ED').
card_original_type('pestilence'/'4ED', 'Enchantment').
card_original_text('pestilence'/'4ED', 'At the end of any turn, if there are no creatures in play, bury Pestilence.\n{B} Pestilence deals 1 damage to all creatures and players.').
card_image_name('pestilence'/'4ED', 'pestilence').
card_uid('pestilence'/'4ED', '4ED:Pestilence:pestilence').
card_rarity('pestilence'/'4ED', 'Common').
card_artist('pestilence'/'4ED', 'Jesper Myrfors').
card_multiverse_id('pestilence'/'4ED', '2119').

card_in_set('phantasmal forces', '4ED').
card_original_type('phantasmal forces'/'4ED', 'Summon — Phantasm').
card_original_text('phantasmal forces'/'4ED', 'Flying\nDuring your upkeep, pay {U} or destroy Phantasmal Forces.').
card_image_name('phantasmal forces'/'4ED', 'phantasmal forces').
card_uid('phantasmal forces'/'4ED', '4ED:Phantasmal Forces:phantasmal forces').
card_rarity('phantasmal forces'/'4ED', 'Uncommon').
card_artist('phantasmal forces'/'4ED', 'Mark Poole').
card_flavor_text('phantasmal forces'/'4ED', 'These beings embody the essence of true heroes long dead. Summoned from the dreamrealms, they rise to meet their enemies.').
card_multiverse_id('phantasmal forces'/'4ED', '2171').

card_in_set('phantasmal terrain', '4ED').
card_original_type('phantasmal terrain'/'4ED', 'Enchant Land').
card_original_text('phantasmal terrain'/'4ED', 'Target land becomes any basic land type of your choice.').
card_image_name('phantasmal terrain'/'4ED', 'phantasmal terrain').
card_uid('phantasmal terrain'/'4ED', '4ED:Phantasmal Terrain:phantasmal terrain').
card_rarity('phantasmal terrain'/'4ED', 'Common').
card_artist('phantasmal terrain'/'4ED', 'Dameon Willich').
card_multiverse_id('phantasmal terrain'/'4ED', '2172').

card_in_set('phantom monster', '4ED').
card_original_type('phantom monster'/'4ED', 'Summon — Phantasm').
card_original_text('phantom monster'/'4ED', 'Flying').
card_image_name('phantom monster'/'4ED', 'phantom monster').
card_uid('phantom monster'/'4ED', '4ED:Phantom Monster:phantom monster').
card_rarity('phantom monster'/'4ED', 'Uncommon').
card_artist('phantom monster'/'4ED', 'Jesper Myrfors').
card_flavor_text('phantom monster'/'4ED', '\"While, like a ghastly rapid river,\nThrough the pale door,\nA hideous throng rush out forever,\nAnd laugh—but smile no more.\"\n—Edgar Allan Poe, \"The Haunted Palace\"').
card_multiverse_id('phantom monster'/'4ED', '2173').

card_in_set('piety', '4ED').
card_original_type('piety'/'4ED', 'Instant').
card_original_text('piety'/'4ED', 'All blocking creatures get +0/+3 until end of turn.').
card_image_name('piety'/'4ED', 'piety').
card_uid('piety'/'4ED', '4ED:Piety:piety').
card_rarity('piety'/'4ED', 'Common').
card_artist('piety'/'4ED', 'Mark Poole').
card_flavor_text('piety'/'4ED', '\"Whoever obeys God and His Prophet, fears God and does his duty to Him, will surely find success.\"\n—The Qur\'an, 24:52').
card_multiverse_id('piety'/'4ED', '2356').

card_in_set('pikemen', '4ED').
card_original_type('pikemen'/'4ED', 'Summon — Pikemen').
card_original_text('pikemen'/'4ED', 'Banding, first strike').
card_image_name('pikemen'/'4ED', 'pikemen').
card_uid('pikemen'/'4ED', '4ED:Pikemen:pikemen').
card_rarity('pikemen'/'4ED', 'Common').
card_artist('pikemen'/'4ED', 'Dennis Detwiller').
card_flavor_text('pikemen'/'4ED', '\"As the cavalry bore down, we faced them with swords drawn and pikes hidden in the grass at our feet. ‘Don\'t lift your pikes \'til I give the word,\' I said.\"\n—Maeveen O\'Donagh, Memoirs of a Soldier').
card_multiverse_id('pikemen'/'4ED', '2357').

card_in_set('pirate ship', '4ED').
card_original_type('pirate ship'/'4ED', 'Summon — Ship').
card_original_text('pirate ship'/'4ED', 'Cannot attack if defending player controls no islands. If at any time you control no islands, bury Pirate Ship.\n{T}: Pirate Ship deals 1 damage to target creature or player.').
card_image_name('pirate ship'/'4ED', 'pirate ship').
card_uid('pirate ship'/'4ED', '4ED:Pirate Ship:pirate ship').
card_rarity('pirate ship'/'4ED', 'Rare').
card_artist('pirate ship'/'4ED', 'Tom Wänerstrand').
card_multiverse_id('pirate ship'/'4ED', '2174').

card_in_set('pit scorpion', '4ED').
card_original_type('pit scorpion'/'4ED', 'Summon — Scorpion').
card_original_text('pit scorpion'/'4ED', 'If Pit Scorpion damages a player, he or she gets a poison counter. If a player has ten or more poison counters, he or she loses the game.').
card_image_name('pit scorpion'/'4ED', 'pit scorpion').
card_uid('pit scorpion'/'4ED', '4ED:Pit Scorpion:pit scorpion').
card_rarity('pit scorpion'/'4ED', 'Common').
card_artist('pit scorpion'/'4ED', 'Scott Kirschner').
card_flavor_text('pit scorpion'/'4ED', 'Sometimes the smallest nuisance can be the greatest pain.').
card_multiverse_id('pit scorpion'/'4ED', '2120').

card_in_set('plague rats', '4ED').
card_original_type('plague rats'/'4ED', 'Summon — Rats').
card_original_text('plague rats'/'4ED', 'Plague Rats has power and toughness each equal to the number of Plague Rats in play, no matter who controls them. For example, if there are two Plague Rats in play, each has power and toughness 2/2.').
card_image_name('plague rats'/'4ED', 'plague rats').
card_uid('plague rats'/'4ED', '4ED:Plague Rats:plague rats').
card_rarity('plague rats'/'4ED', 'Common').
card_artist('plague rats'/'4ED', 'Anson Maddocks').
card_flavor_text('plague rats'/'4ED', 'During the dark times, the rats seemed to grow ever stronger.').
card_multiverse_id('plague rats'/'4ED', '2121').

card_in_set('plains', '4ED').
card_original_type('plains'/'4ED', 'Land').
card_original_text('plains'/'4ED', '{T}: Add {W} to your mana pool.').
card_image_name('plains'/'4ED', 'plains1').
card_uid('plains'/'4ED', '4ED:Plains:plains1').
card_rarity('plains'/'4ED', 'Basic Land').
card_artist('plains'/'4ED', 'Jesper Myrfors').
card_multiverse_id('plains'/'4ED', '2386').

card_in_set('plains', '4ED').
card_original_type('plains'/'4ED', 'Land').
card_original_text('plains'/'4ED', '{T}: Add {W} to your mana pool.').
card_image_name('plains'/'4ED', 'plains2').
card_uid('plains'/'4ED', '4ED:Plains:plains2').
card_rarity('plains'/'4ED', 'Basic Land').
card_artist('plains'/'4ED', 'Jesper Myrfors').
card_multiverse_id('plains'/'4ED', '2385').

card_in_set('plains', '4ED').
card_original_type('plains'/'4ED', 'Land').
card_original_text('plains'/'4ED', '{T}: Add {W} to your mana pool.').
card_image_name('plains'/'4ED', 'plains3').
card_uid('plains'/'4ED', '4ED:Plains:plains3').
card_rarity('plains'/'4ED', 'Basic Land').
card_artist('plains'/'4ED', 'Jesper Myrfors').
card_multiverse_id('plains'/'4ED', '2384').

card_in_set('power leak', '4ED').
card_original_type('power leak'/'4ED', 'Enchant Enchantment').
card_original_text('power leak'/'4ED', 'During the upkeep of target enchantment\'s controller, Power Leak deals 2 damage to him or her. That player may pay {1} for each damage he or she wishes to prevent from Power Leak.').
card_image_name('power leak'/'4ED', 'power leak').
card_uid('power leak'/'4ED', '4ED:Power Leak:power leak').
card_rarity('power leak'/'4ED', 'Common').
card_artist('power leak'/'4ED', 'Drew Tucker').
card_multiverse_id('power leak'/'4ED', '2175').

card_in_set('power sink', '4ED').
card_original_type('power sink'/'4ED', 'Interrupt').
card_original_text('power sink'/'4ED', 'Counter a target spell if its caster does not pay {X}. Target spell\'s caster must draw and pay all available mana from lands and mana pool until {X} is paid; he or she may also pay mana from other sources if desired.').
card_image_name('power sink'/'4ED', 'power sink').
card_uid('power sink'/'4ED', '4ED:Power Sink:power sink').
card_rarity('power sink'/'4ED', 'Common').
card_artist('power sink'/'4ED', 'Richard Thomas').
card_multiverse_id('power sink'/'4ED', '2176').

card_in_set('power surge', '4ED').
card_original_type('power surge'/'4ED', 'Enchantment').
card_original_text('power surge'/'4ED', 'During each player\'s upkeep, Power Surge deals that player 1 damage for each land he or she controls that was untapped at the beginning of the turn, before the untap phase.').
card_image_name('power surge'/'4ED', 'power surge').
card_uid('power surge'/'4ED', '4ED:Power Surge:power surge').
card_rarity('power surge'/'4ED', 'Rare').
card_artist('power surge'/'4ED', 'Douglas Shuler').
card_multiverse_id('power surge'/'4ED', '2299').

card_in_set('pradesh gypsies', '4ED').
card_original_type('pradesh gypsies'/'4ED', 'Summon — Gypsies').
card_original_text('pradesh gypsies'/'4ED', '{1}{G}, {T}: Target creature gets -2/-0 until end of turn.').
card_image_name('pradesh gypsies'/'4ED', 'pradesh gypsies').
card_uid('pradesh gypsies'/'4ED', '4ED:Pradesh Gypsies:pradesh gypsies').
card_rarity('pradesh gypsies'/'4ED', 'Common').
card_artist('pradesh gypsies'/'4ED', 'Quinton Hoover').
card_multiverse_id('pradesh gypsies'/'4ED', '2232').

card_in_set('primal clay', '4ED').
card_original_type('primal clay'/'4ED', 'Artifact Creature').
card_original_text('primal clay'/'4ED', 'When Primal Clay comes into play, choose whether to make it a 1/6 wall, a 2/2 creature with flying, or a 3/3 creature.').
card_image_name('primal clay'/'4ED', 'primal clay').
card_uid('primal clay'/'4ED', '4ED:Primal Clay:primal clay').
card_rarity('primal clay'/'4ED', 'Rare').
card_artist('primal clay'/'4ED', 'Kaja Foglio').
card_multiverse_id('primal clay'/'4ED', '2065').

card_in_set('prodigal sorcerer', '4ED').
card_original_type('prodigal sorcerer'/'4ED', 'Summon — Wizard').
card_original_text('prodigal sorcerer'/'4ED', '{T}: Prodigal Sorcerer deals 1 damage to target creature or player.').
card_image_name('prodigal sorcerer'/'4ED', 'prodigal sorcerer').
card_uid('prodigal sorcerer'/'4ED', '4ED:Prodigal Sorcerer:prodigal sorcerer').
card_rarity('prodigal sorcerer'/'4ED', 'Common').
card_artist('prodigal sorcerer'/'4ED', 'Douglas Shuler').
card_flavor_text('prodigal sorcerer'/'4ED', 'Occasionally a member of the Institute of Arcane Study acquires a taste for worldly pleasures. Seldom do they have trouble finding employment.').
card_multiverse_id('prodigal sorcerer'/'4ED', '2177').

card_in_set('psionic entity', '4ED').
card_original_type('psionic entity'/'4ED', 'Summon — Entity').
card_original_text('psionic entity'/'4ED', '{T}: Psionic Entity deals 2 damage to target creature or player and 3 damage to itself.').
card_image_name('psionic entity'/'4ED', 'psionic entity').
card_uid('psionic entity'/'4ED', '4ED:Psionic Entity:psionic entity').
card_rarity('psionic entity'/'4ED', 'Rare').
card_artist('psionic entity'/'4ED', 'Justin Hampton').
card_flavor_text('psionic entity'/'4ED', 'Creatures of the Æther are notorious for neglecting their own well-being.').
card_multiverse_id('psionic entity'/'4ED', '2178').

card_in_set('psychic venom', '4ED').
card_original_type('psychic venom'/'4ED', 'Enchant Land').
card_original_text('psychic venom'/'4ED', 'Whenever target land becomes tapped, Psychic Venom deals 2 damage to that land\'s controller.').
card_image_name('psychic venom'/'4ED', 'psychic venom').
card_uid('psychic venom'/'4ED', '4ED:Psychic Venom:psychic venom').
card_rarity('psychic venom'/'4ED', 'Common').
card_artist('psychic venom'/'4ED', 'Brian Snõddy').
card_multiverse_id('psychic venom'/'4ED', '2179').

card_in_set('purelace', '4ED').
card_original_type('purelace'/'4ED', 'Interrupt').
card_original_text('purelace'/'4ED', 'Change the color of target spell or target permanent to white. Costs to cast, tap, maintain, or use a special ability of target remain unchanged.').
card_image_name('purelace'/'4ED', 'purelace').
card_uid('purelace'/'4ED', '4ED:Purelace:purelace').
card_rarity('purelace'/'4ED', 'Rare').
card_artist('purelace'/'4ED', 'Sandra Everingham').
card_multiverse_id('purelace'/'4ED', '2358').

card_in_set('pyrotechnics', '4ED').
card_original_type('pyrotechnics'/'4ED', 'Sorcery').
card_original_text('pyrotechnics'/'4ED', 'Pyrotechnics deals 4 damage divided any way you choose among any number of target creatures and/or players.').
card_image_name('pyrotechnics'/'4ED', 'pyrotechnics').
card_uid('pyrotechnics'/'4ED', '4ED:Pyrotechnics:pyrotechnics').
card_rarity('pyrotechnics'/'4ED', 'Uncommon').
card_artist('pyrotechnics'/'4ED', 'Anson Maddocks').
card_flavor_text('pyrotechnics'/'4ED', '\"Hi! ni! ya! Behold the man of flint, that\'s me!\nFour lightnings zigzag from me, strike and return.\" —Navajo war chant').
card_multiverse_id('pyrotechnics'/'4ED', '2300').

card_in_set('radjan spirit', '4ED').
card_original_type('radjan spirit'/'4ED', 'Summon — Spirit').
card_original_text('radjan spirit'/'4ED', '{T}: Target creature loses flying until end of turn.').
card_image_name('radjan spirit'/'4ED', 'radjan spirit').
card_uid('radjan spirit'/'4ED', '4ED:Radjan Spirit:radjan spirit').
card_rarity('radjan spirit'/'4ED', 'Uncommon').
card_artist('radjan spirit'/'4ED', 'Christopher Rush').
card_multiverse_id('radjan spirit'/'4ED', '2233').

card_in_set('rag man', '4ED').
card_original_type('rag man'/'4ED', 'Summon — Rag Man').
card_original_text('rag man'/'4ED', '{B}{B}{B}, {T}: Look at target opponent\'s hand. If that player has any creature cards in hand, he or she discards one of them at random. Use this ability only during your turn.').
card_image_name('rag man'/'4ED', 'rag man').
card_uid('rag man'/'4ED', '4ED:Rag Man:rag man').
card_rarity('rag man'/'4ED', 'Rare').
card_artist('rag man'/'4ED', 'Daniel Gelon').
card_flavor_text('rag man'/'4ED', '\"Aw, he\'s just a silly, dirty little man. What\'s to be afraid of?\"').
card_multiverse_id('rag man'/'4ED', '2122').

card_in_set('raise dead', '4ED').
card_original_type('raise dead'/'4ED', 'Sorcery').
card_original_text('raise dead'/'4ED', 'Take target creature from your graveyard and put it into your hand.').
card_image_name('raise dead'/'4ED', 'raise dead').
card_uid('raise dead'/'4ED', '4ED:Raise Dead:raise dead').
card_rarity('raise dead'/'4ED', 'Common').
card_artist('raise dead'/'4ED', 'Jeff A. Menges').
card_multiverse_id('raise dead'/'4ED', '2123').

card_in_set('rebirth', '4ED').
card_original_type('rebirth'/'4ED', 'Sorcery').
card_original_text('rebirth'/'4ED', 'Each player may be healed to 20 life. Any player choosing to be healed antes an additional card from the top of his or her library. Remove Rebirth from your deck before playing if not playing for ante.').
card_image_name('rebirth'/'4ED', 'rebirth').
card_uid('rebirth'/'4ED', '4ED:Rebirth:rebirth').
card_rarity('rebirth'/'4ED', 'Rare').
card_artist('rebirth'/'4ED', 'Mark Tedin').
card_multiverse_id('rebirth'/'4ED', '2234').

card_in_set('red elemental blast', '4ED').
card_original_type('red elemental blast'/'4ED', 'Interrupt').
card_original_text('red elemental blast'/'4ED', 'Counter target blue spell or destroy target blue permanent.').
card_image_name('red elemental blast'/'4ED', 'red elemental blast').
card_uid('red elemental blast'/'4ED', '4ED:Red Elemental Blast:red elemental blast').
card_rarity('red elemental blast'/'4ED', 'Common').
card_artist('red elemental blast'/'4ED', 'Richard Thomas').
card_multiverse_id('red elemental blast'/'4ED', '2301').

card_in_set('red mana battery', '4ED').
card_original_type('red mana battery'/'4ED', 'Artifact').
card_original_text('red mana battery'/'4ED', '{2}, {T}: Put one charge counter on Red Mana Battery.\n{T}: Add {R} to your mana pool and remove as many charge counters as you wish. For each charge counter removed from Red Mana Battery, add {R} to your mana pool. Play this ability as an interrupt.').
card_image_name('red mana battery'/'4ED', 'red mana battery').
card_uid('red mana battery'/'4ED', '4ED:Red Mana Battery:red mana battery').
card_rarity('red mana battery'/'4ED', 'Rare').
card_artist('red mana battery'/'4ED', 'Mark Tedin').
card_multiverse_id('red mana battery'/'4ED', '2066').

card_in_set('red ward', '4ED').
card_original_type('red ward'/'4ED', 'Enchant Creature').
card_original_text('red ward'/'4ED', 'Target creature gains protection from red. The protection granted by Red Ward does not destroy Red Ward.').
card_image_name('red ward'/'4ED', 'red ward').
card_uid('red ward'/'4ED', '4ED:Red Ward:red ward').
card_rarity('red ward'/'4ED', 'Uncommon').
card_artist('red ward'/'4ED', 'Dan Frazier').
card_multiverse_id('red ward'/'4ED', '2359').

card_in_set('regeneration', '4ED').
card_original_type('regeneration'/'4ED', 'Enchant Creature').
card_original_text('regeneration'/'4ED', '{G} Regenerate target creature Regeneration enchants.').
card_image_name('regeneration'/'4ED', 'regeneration').
card_uid('regeneration'/'4ED', '4ED:Regeneration:regeneration').
card_rarity('regeneration'/'4ED', 'Common').
card_artist('regeneration'/'4ED', 'Quinton Hoover').
card_multiverse_id('regeneration'/'4ED', '2235').

card_in_set('relic bind', '4ED').
card_original_type('relic bind'/'4ED', 'Enchant Artifact').
card_original_text('relic bind'/'4ED', 'When target artifact opponent controls becomes tapped, you may give 1 life or have Relic Bind deal 1 damage to target player.').
card_image_name('relic bind'/'4ED', 'relic bind').
card_uid('relic bind'/'4ED', '4ED:Relic Bind:relic bind').
card_rarity('relic bind'/'4ED', 'Rare').
card_artist('relic bind'/'4ED', 'Christopher Rush').
card_multiverse_id('relic bind'/'4ED', '2180').

card_in_set('reverse damage', '4ED').
card_original_type('reverse damage'/'4ED', 'Instant').
card_original_text('reverse damage'/'4ED', 'All damage dealt to you so far this turn by one source is retroactively added to your life total instead of subtracted. Further damage this turn is treated normally.').
card_image_name('reverse damage'/'4ED', 'reverse damage').
card_uid('reverse damage'/'4ED', '4ED:Reverse Damage:reverse damage').
card_rarity('reverse damage'/'4ED', 'Rare').
card_artist('reverse damage'/'4ED', 'Dameon Willich').
card_multiverse_id('reverse damage'/'4ED', '2360').

card_in_set('righteousness', '4ED').
card_original_type('righteousness'/'4ED', 'Instant').
card_original_text('righteousness'/'4ED', 'Target blocking creature gets +7/+7 until end of turn.').
card_image_name('righteousness'/'4ED', 'righteousness').
card_uid('righteousness'/'4ED', '4ED:Righteousness:righteousness').
card_rarity('righteousness'/'4ED', 'Rare').
card_artist('righteousness'/'4ED', 'Douglas Shuler').
card_multiverse_id('righteousness'/'4ED', '2361').

card_in_set('rod of ruin', '4ED').
card_original_type('rod of ruin'/'4ED', 'Artifact').
card_original_text('rod of ruin'/'4ED', '{3}, {T}: Rod of Ruin deals 1 damage to target creature or player.').
card_image_name('rod of ruin'/'4ED', 'rod of ruin').
card_uid('rod of ruin'/'4ED', '4ED:Rod of Ruin:rod of ruin').
card_rarity('rod of ruin'/'4ED', 'Uncommon').
card_artist('rod of ruin'/'4ED', 'Christopher Rush').
card_multiverse_id('rod of ruin'/'4ED', '2067').

card_in_set('royal assassin', '4ED').
card_original_type('royal assassin'/'4ED', 'Summon — Assassin').
card_original_text('royal assassin'/'4ED', '{T}: Destroy target tapped creature.').
card_image_name('royal assassin'/'4ED', 'royal assassin').
card_uid('royal assassin'/'4ED', '4ED:Royal Assassin:royal assassin').
card_rarity('royal assassin'/'4ED', 'Rare').
card_artist('royal assassin'/'4ED', 'Tom Wänerstrand').
card_flavor_text('royal assassin'/'4ED', 'Trained in the arts of stealth, the Royal Assassins choose their victims carefully, relying on timing and precision rather than brute force.').
card_multiverse_id('royal assassin'/'4ED', '2124').

card_in_set('samite healer', '4ED').
card_original_type('samite healer'/'4ED', 'Summon — Cleric').
card_original_text('samite healer'/'4ED', '{T}: Prevent 1 damage to any creature or player.').
card_image_name('samite healer'/'4ED', 'samite healer').
card_uid('samite healer'/'4ED', '4ED:Samite Healer:samite healer').
card_rarity('samite healer'/'4ED', 'Common').
card_artist('samite healer'/'4ED', 'Tom Wänerstrand').
card_flavor_text('samite healer'/'4ED', 'Healers ultimately acquire the divine gifts of spiritual and physical wholeness. The most devout are also granted the ability to pass physical wholeness on to others.').
card_multiverse_id('samite healer'/'4ED', '2362').

card_in_set('sandstorm', '4ED').
card_original_type('sandstorm'/'4ED', 'Instant').
card_original_text('sandstorm'/'4ED', 'Sandstorm deals 1 damage to all attacking creatures.').
card_image_name('sandstorm'/'4ED', 'sandstorm').
card_uid('sandstorm'/'4ED', '4ED:Sandstorm:sandstorm').
card_rarity('sandstorm'/'4ED', 'Common').
card_artist('sandstorm'/'4ED', 'Brian Snõddy').
card_flavor_text('sandstorm'/'4ED', 'Even the landscape turned against Sarsour, first rising up and pelting him, then rearranging itself so he could no longer find his way.').
card_multiverse_id('sandstorm'/'4ED', '2236').

card_in_set('savannah lions', '4ED').
card_original_type('savannah lions'/'4ED', 'Summon — Lions').
card_original_text('savannah lions'/'4ED', '').
card_image_name('savannah lions'/'4ED', 'savannah lions').
card_uid('savannah lions'/'4ED', '4ED:Savannah Lions:savannah lions').
card_rarity('savannah lions'/'4ED', 'Rare').
card_artist('savannah lions'/'4ED', 'Daniel Gelon').
card_flavor_text('savannah lions'/'4ED', 'The traditional kings of the jungle command a healthy respect in other climates as well. Relying mainly on their stealth and speed, Savannah Lions can take a victim by surprise, even in the endless, flat plains of their homeland.').
card_multiverse_id('savannah lions'/'4ED', '2363').

card_in_set('scathe zombies', '4ED').
card_original_type('scathe zombies'/'4ED', 'Summon — Zombies').
card_original_text('scathe zombies'/'4ED', '').
card_image_name('scathe zombies'/'4ED', 'scathe zombies').
card_uid('scathe zombies'/'4ED', '4ED:Scathe Zombies:scathe zombies').
card_rarity('scathe zombies'/'4ED', 'Common').
card_artist('scathe zombies'/'4ED', 'Jesper Myrfors').
card_flavor_text('scathe zombies'/'4ED', '\"They groaned, they stirred, they all uprose, Nor spake, nor moved their eyes; It had been strange, even in a dream, To have seen those dead men rise.\" —Samuel Coleridge, \"The Rime of the Ancient Mariner\"').
card_multiverse_id('scathe zombies'/'4ED', '2125').

card_in_set('scavenging ghoul', '4ED').
card_original_type('scavenging ghoul'/'4ED', 'Summon — Ghoul').
card_original_text('scavenging ghoul'/'4ED', 'At the end of each turn, put a corpse counter on Scavenging Ghoul for each creature put into the graveyard during that turn.\n{0}: Remove a corpse counter from Scavenging Ghoul to regenerate it.').
card_image_name('scavenging ghoul'/'4ED', 'scavenging ghoul').
card_uid('scavenging ghoul'/'4ED', '4ED:Scavenging Ghoul:scavenging ghoul').
card_rarity('scavenging ghoul'/'4ED', 'Uncommon').
card_artist('scavenging ghoul'/'4ED', 'Jeff A. Menges').
card_multiverse_id('scavenging ghoul'/'4ED', '2126').

card_in_set('scryb sprites', '4ED').
card_original_type('scryb sprites'/'4ED', 'Summon — Faeries').
card_original_text('scryb sprites'/'4ED', 'Flying').
card_image_name('scryb sprites'/'4ED', 'scryb sprites').
card_uid('scryb sprites'/'4ED', '4ED:Scryb Sprites:scryb sprites').
card_rarity('scryb sprites'/'4ED', 'Common').
card_artist('scryb sprites'/'4ED', 'Amy Weber').
card_flavor_text('scryb sprites'/'4ED', 'The only sound was the gentle clicking of the Faeries\' wings. Then those intruders who were still standing turned and fled. One thing was certain: they didn\'t think the Scryb were very funny anymore.').
card_multiverse_id('scryb sprites'/'4ED', '2237').

card_in_set('sea serpent', '4ED').
card_original_type('sea serpent'/'4ED', 'Summon — Serpent').
card_original_text('sea serpent'/'4ED', 'Cannot attack if defending player controls no islands. If at any time you control no islands, bury Sea Serpent.').
card_image_name('sea serpent'/'4ED', 'sea serpent').
card_uid('sea serpent'/'4ED', '4ED:Sea Serpent:sea serpent').
card_rarity('sea serpent'/'4ED', 'Common').
card_artist('sea serpent'/'4ED', 'Jeff A. Menges').
card_flavor_text('sea serpent'/'4ED', 'Legend has it that Serpents used to be bigger, but how could that be?').
card_multiverse_id('sea serpent'/'4ED', '2181').

card_in_set('seeker', '4ED').
card_original_type('seeker'/'4ED', 'Enchant Creature').
card_original_text('seeker'/'4ED', 'Target creature cannot be blocked except by white creatures and artifact creatures.').
card_image_name('seeker'/'4ED', 'seeker').
card_uid('seeker'/'4ED', '4ED:Seeker:seeker').
card_rarity('seeker'/'4ED', 'Common').
card_artist('seeker'/'4ED', 'Mark Poole').
card_multiverse_id('seeker'/'4ED', '2364').

card_in_set('segovian leviathan', '4ED').
card_original_type('segovian leviathan'/'4ED', 'Summon — Leviathan').
card_original_text('segovian leviathan'/'4ED', 'Islandwalk').
card_image_name('segovian leviathan'/'4ED', 'segovian leviathan').
card_uid('segovian leviathan'/'4ED', '4ED:Segovian Leviathan:segovian leviathan').
card_rarity('segovian leviathan'/'4ED', 'Uncommon').
card_artist('segovian leviathan'/'4ED', 'Melissa A. Benson').
card_flavor_text('segovian leviathan'/'4ED', '\"Leviathan, too! Can you catch him with a fish-hook or run a line round his tongue?\" —Job 40:25').
card_multiverse_id('segovian leviathan'/'4ED', '2182').

card_in_set('sengir vampire', '4ED').
card_original_type('sengir vampire'/'4ED', 'Summon — Vampire').
card_original_text('sengir vampire'/'4ED', 'Flying\nPut a +1/+1 counter on Sengir Vampire each time a creature is put into the graveyard the same turn Sengir Vampire damaged it.').
card_image_name('sengir vampire'/'4ED', 'sengir vampire').
card_uid('sengir vampire'/'4ED', '4ED:Sengir Vampire:sengir vampire').
card_rarity('sengir vampire'/'4ED', 'Uncommon').
card_artist('sengir vampire'/'4ED', 'Anson Maddocks').
card_multiverse_id('sengir vampire'/'4ED', '2127').

card_in_set('serra angel', '4ED').
card_original_type('serra angel'/'4ED', 'Summon — Angel').
card_original_text('serra angel'/'4ED', 'Flying\nAttacking does not cause Serra Angel to tap.').
card_image_name('serra angel'/'4ED', 'serra angel').
card_uid('serra angel'/'4ED', '4ED:Serra Angel:serra angel').
card_rarity('serra angel'/'4ED', 'Uncommon').
card_artist('serra angel'/'4ED', 'Douglas Shuler').
card_flavor_text('serra angel'/'4ED', 'Born with wings of light and a sword of faith, this heavenly incarnation embodies both fury and purity.').
card_multiverse_id('serra angel'/'4ED', '2365').

card_in_set('shanodin dryads', '4ED').
card_original_type('shanodin dryads'/'4ED', 'Summon — Nymphs').
card_original_text('shanodin dryads'/'4ED', 'Forestwalk').
card_image_name('shanodin dryads'/'4ED', 'shanodin dryads').
card_uid('shanodin dryads'/'4ED', '4ED:Shanodin Dryads:shanodin dryads').
card_rarity('shanodin dryads'/'4ED', 'Common').
card_artist('shanodin dryads'/'4ED', 'Anson Maddocks').
card_flavor_text('shanodin dryads'/'4ED', 'Moving without sound, swift figures pass through branches and undergrowth completely unhindered. One with the trees around them, the Dryads of Shanodin Forest are seen only when they wish to be.').
card_multiverse_id('shanodin dryads'/'4ED', '2238').

card_in_set('shapeshifter', '4ED').
card_original_type('shapeshifter'/'4ED', 'Artifact Creature').
card_original_text('shapeshifter'/'4ED', 'Shapeshifter has power and toughness that add up to seven, but neither may be more than seven. Set them when Shapeshifter comes into play; you may change them during your upkeep.').
card_image_name('shapeshifter'/'4ED', 'shapeshifter').
card_uid('shapeshifter'/'4ED', '4ED:Shapeshifter:shapeshifter').
card_rarity('shapeshifter'/'4ED', 'Uncommon').
card_artist('shapeshifter'/'4ED', 'Dan Frazier').
card_multiverse_id('shapeshifter'/'4ED', '2068').

card_in_set('shatter', '4ED').
card_original_type('shatter'/'4ED', 'Instant').
card_original_text('shatter'/'4ED', 'Destroy target artifact.').
card_image_name('shatter'/'4ED', 'shatter').
card_uid('shatter'/'4ED', '4ED:Shatter:shatter').
card_rarity('shatter'/'4ED', 'Common').
card_artist('shatter'/'4ED', 'Amy Weber').
card_multiverse_id('shatter'/'4ED', '2302').

card_in_set('shivan dragon', '4ED').
card_original_type('shivan dragon'/'4ED', 'Summon — Dragon').
card_original_text('shivan dragon'/'4ED', 'Flying\n{R} +1/+0 until end of turn').
card_image_name('shivan dragon'/'4ED', 'shivan dragon').
card_uid('shivan dragon'/'4ED', '4ED:Shivan Dragon:shivan dragon').
card_rarity('shivan dragon'/'4ED', 'Rare').
card_artist('shivan dragon'/'4ED', 'Melissa A. Benson').
card_flavor_text('shivan dragon'/'4ED', 'While it\'s true most Dragons are cruel, the Shivan Dragon seems to take particular glee in the misery of others, often tormenting its victims much like a cat plays with a mouse before delivering the final blow.').
card_multiverse_id('shivan dragon'/'4ED', '2303').

card_in_set('simulacrum', '4ED').
card_original_type('simulacrum'/'4ED', 'Instant').
card_original_text('simulacrum'/'4ED', 'All damage done to you so far this turn is instead retroactively applied to a target creature you control. Further damage this turn is treated normally.').
card_image_name('simulacrum'/'4ED', 'simulacrum').
card_uid('simulacrum'/'4ED', '4ED:Simulacrum:simulacrum').
card_rarity('simulacrum'/'4ED', 'Uncommon').
card_artist('simulacrum'/'4ED', 'Mark Poole').
card_multiverse_id('simulacrum'/'4ED', '2128').

card_in_set('sindbad', '4ED').
card_original_type('sindbad'/'4ED', 'Summon — Sindbad').
card_original_text('sindbad'/'4ED', '{T}: Draw a card. If it is not a land, discard it.').
card_image_name('sindbad'/'4ED', 'sindbad').
card_uid('sindbad'/'4ED', '4ED:Sindbad:sindbad').
card_rarity('sindbad'/'4ED', 'Uncommon').
card_artist('sindbad'/'4ED', 'Julie Baroh').
card_multiverse_id('sindbad'/'4ED', '2183').

card_in_set('siren\'s call', '4ED').
card_original_type('siren\'s call'/'4ED', 'Instant').
card_original_text('siren\'s call'/'4ED', 'All of target opponent\'s creatures that can attack must do so. At end of turn, destroy any non-wall creatures that did not attack. Play only during opponent\'s turn, before opponent\'s attack. Siren\'s Call does not affect creatures brought under opponent\'s control this turn.').
card_image_name('siren\'s call'/'4ED', 'siren\'s call').
card_uid('siren\'s call'/'4ED', '4ED:Siren\'s Call:siren\'s call').
card_rarity('siren\'s call'/'4ED', 'Uncommon').
card_artist('siren\'s call'/'4ED', 'Anson Maddocks').
card_multiverse_id('siren\'s call'/'4ED', '2184').

card_in_set('sisters of the flame', '4ED').
card_original_type('sisters of the flame'/'4ED', 'Summon — Sisters').
card_original_text('sisters of the flame'/'4ED', '{T}: Add {R} to your mana pool. Play this ability as an interrupt.').
card_image_name('sisters of the flame'/'4ED', 'sisters of the flame').
card_uid('sisters of the flame'/'4ED', '4ED:Sisters of the Flame:sisters of the flame').
card_rarity('sisters of the flame'/'4ED', 'Common').
card_artist('sisters of the flame'/'4ED', 'Jesper Myrfors').
card_flavor_text('sisters of the flame'/'4ED', 'We are many wicks sharing a common tallow; we feed the skies with the ashes of our prey.').
card_multiverse_id('sisters of the flame'/'4ED', '2304').

card_in_set('sleight of mind', '4ED').
card_original_type('sleight of mind'/'4ED', 'Interrupt').
card_original_text('sleight of mind'/'4ED', 'Change the text of target spell or target permanent by replacing all occurrences of one color word with another. For example, you may change \"Counters black spells\" to \"Counters blue spells.\" Sleight of Mind cannot change mana symbols.').
card_image_name('sleight of mind'/'4ED', 'sleight of mind').
card_uid('sleight of mind'/'4ED', '4ED:Sleight of Mind:sleight of mind').
card_rarity('sleight of mind'/'4ED', 'Rare').
card_artist('sleight of mind'/'4ED', 'Mark Poole').
card_multiverse_id('sleight of mind'/'4ED', '2185').

card_in_set('smoke', '4ED').
card_original_type('smoke'/'4ED', 'Enchantment').
card_original_text('smoke'/'4ED', 'No player may untap more than one creature during his or her untap phase.').
card_image_name('smoke'/'4ED', 'smoke').
card_uid('smoke'/'4ED', '4ED:Smoke:smoke').
card_rarity('smoke'/'4ED', 'Rare').
card_artist('smoke'/'4ED', 'Jesper Myrfors').
card_multiverse_id('smoke'/'4ED', '2305').

card_in_set('sorceress queen', '4ED').
card_original_type('sorceress queen'/'4ED', 'Summon — Sorceress').
card_original_text('sorceress queen'/'4ED', '{T}: Target creature other than Sorceress Queen becomes 0/2 until end of turn.').
card_image_name('sorceress queen'/'4ED', 'sorceress queen').
card_uid('sorceress queen'/'4ED', '4ED:Sorceress Queen:sorceress queen').
card_rarity('sorceress queen'/'4ED', 'Rare').
card_artist('sorceress queen'/'4ED', 'Kaja Foglio').
card_multiverse_id('sorceress queen'/'4ED', '2129').

card_in_set('soul net', '4ED').
card_original_type('soul net'/'4ED', 'Artifact').
card_original_text('soul net'/'4ED', '{1}: Gain 1 life when a creature is put into the graveyard from play. Use this effect only once each time a creature is put into the graveyard.').
card_image_name('soul net'/'4ED', 'soul net').
card_uid('soul net'/'4ED', '4ED:Soul Net:soul net').
card_rarity('soul net'/'4ED', 'Uncommon').
card_artist('soul net'/'4ED', 'Dameon Willich').
card_multiverse_id('soul net'/'4ED', '2069').

card_in_set('spell blast', '4ED').
card_original_type('spell blast'/'4ED', 'Interrupt').
card_original_text('spell blast'/'4ED', 'Counter target spell. X is the casting cost of the target spell.').
card_image_name('spell blast'/'4ED', 'spell blast').
card_uid('spell blast'/'4ED', '4ED:Spell Blast:spell blast').
card_rarity('spell blast'/'4ED', 'Common').
card_artist('spell blast'/'4ED', 'Brian Snõddy').
card_multiverse_id('spell blast'/'4ED', '2186').

card_in_set('spirit link', '4ED').
card_original_type('spirit link'/'4ED', 'Enchant Creature').
card_original_text('spirit link'/'4ED', 'Gain 1 life for every 1 damage target creature deals. You may gain more life than the toughness or the total life of the creature or player damaged by the creature Spirit Link enchants.').
card_image_name('spirit link'/'4ED', 'spirit link').
card_uid('spirit link'/'4ED', '4ED:Spirit Link:spirit link').
card_rarity('spirit link'/'4ED', 'Uncommon').
card_artist('spirit link'/'4ED', 'Kaja Foglio').
card_multiverse_id('spirit link'/'4ED', '2366').

card_in_set('spirit shackle', '4ED').
card_original_type('spirit shackle'/'4ED', 'Enchant Creature').
card_original_text('spirit shackle'/'4ED', 'Put a -0/-2 counter on target creature every time it becomes tapped. These counters remain even if Spirit Shackle is removed.').
card_image_name('spirit shackle'/'4ED', 'spirit shackle').
card_uid('spirit shackle'/'4ED', '4ED:Spirit Shackle:spirit shackle').
card_rarity('spirit shackle'/'4ED', 'Uncommon').
card_artist('spirit shackle'/'4ED', 'Edward P. Beard, Jr.').
card_multiverse_id('spirit shackle'/'4ED', '2130').

card_in_set('stasis', '4ED').
card_original_type('stasis'/'4ED', 'Enchantment').
card_original_text('stasis'/'4ED', 'Players do not get an untap phase. During your upkeep, pay {U} or destroy Stasis.').
card_image_name('stasis'/'4ED', 'stasis').
card_uid('stasis'/'4ED', '4ED:Stasis:stasis').
card_rarity('stasis'/'4ED', 'Rare').
card_artist('stasis'/'4ED', 'Fay Jones').
card_multiverse_id('stasis'/'4ED', '2187').

card_in_set('steal artifact', '4ED').
card_original_type('steal artifact'/'4ED', 'Enchant Artifact').
card_original_text('steal artifact'/'4ED', 'Gain control of target artifact.').
card_image_name('steal artifact'/'4ED', 'steal artifact').
card_uid('steal artifact'/'4ED', '4ED:Steal Artifact:steal artifact').
card_rarity('steal artifact'/'4ED', 'Uncommon').
card_artist('steal artifact'/'4ED', 'Amy Weber').
card_multiverse_id('steal artifact'/'4ED', '2188').

card_in_set('stone giant', '4ED').
card_original_type('stone giant'/'4ED', 'Summon — Giant').
card_original_text('stone giant'/'4ED', '{T}: Target creature you control, which must have toughness less than Stone Giant\'s power, gains flying until end of turn. Destroy that creature at end of turn. Other effects may later be used to increase the creature\'s toughness beyond Stone Giant\'s power.').
card_image_name('stone giant'/'4ED', 'stone giant').
card_uid('stone giant'/'4ED', '4ED:Stone Giant:stone giant').
card_rarity('stone giant'/'4ED', 'Uncommon').
card_artist('stone giant'/'4ED', 'Dameon Willich').
card_multiverse_id('stone giant'/'4ED', '2306').

card_in_set('stone rain', '4ED').
card_original_type('stone rain'/'4ED', 'Sorcery').
card_original_text('stone rain'/'4ED', 'Destroy target land.').
card_image_name('stone rain'/'4ED', 'stone rain').
card_uid('stone rain'/'4ED', '4ED:Stone Rain:stone rain').
card_rarity('stone rain'/'4ED', 'Common').
card_artist('stone rain'/'4ED', 'Daniel Gelon').
card_multiverse_id('stone rain'/'4ED', '2307').

card_in_set('stream of life', '4ED').
card_original_type('stream of life'/'4ED', 'Sorcery').
card_original_text('stream of life'/'4ED', 'Target player gains X life.').
card_image_name('stream of life'/'4ED', 'stream of life').
card_uid('stream of life'/'4ED', '4ED:Stream of Life:stream of life').
card_rarity('stream of life'/'4ED', 'Common').
card_artist('stream of life'/'4ED', 'Mark Poole').
card_multiverse_id('stream of life'/'4ED', '2239').

card_in_set('strip mine', '4ED').
card_original_type('strip mine'/'4ED', 'Land').
card_original_text('strip mine'/'4ED', '{T}: Add one colorless mana to your mana pool.\n{T}: Sacrifice Strip Mine to destroy target land.').
card_image_name('strip mine'/'4ED', 'strip mine').
card_uid('strip mine'/'4ED', '4ED:Strip Mine:strip mine').
card_rarity('strip mine'/'4ED', 'Uncommon').
card_artist('strip mine'/'4ED', 'Daniel Gelon').
card_flavor_text('strip mine'/'4ED', 'Unlike previous conflicts, the war between Urza and Mishra made Dominia itself a casualty of war.').
card_multiverse_id('strip mine'/'4ED', '2380').

card_in_set('sunglasses of urza', '4ED').
card_original_type('sunglasses of urza'/'4ED', 'Artifact').
card_original_text('sunglasses of urza'/'4ED', 'You may use white mana in your mana pool as either white or red mana.').
card_image_name('sunglasses of urza'/'4ED', 'sunglasses of urza').
card_uid('sunglasses of urza'/'4ED', '4ED:Sunglasses of Urza:sunglasses of urza').
card_rarity('sunglasses of urza'/'4ED', 'Rare').
card_artist('sunglasses of urza'/'4ED', 'Dan Frazier').
card_multiverse_id('sunglasses of urza'/'4ED', '2070').

card_in_set('sunken city', '4ED').
card_original_type('sunken city'/'4ED', 'Enchantment').
card_original_text('sunken city'/'4ED', 'All blue creatures get +1/+1. During your upkeep, pay {U}{U} or destroy Sunken City.').
card_image_name('sunken city'/'4ED', 'sunken city').
card_uid('sunken city'/'4ED', '4ED:Sunken City:sunken city').
card_rarity('sunken city'/'4ED', 'Common').
card_artist('sunken city'/'4ED', 'Jesper Myrfors').
card_multiverse_id('sunken city'/'4ED', '2189').

card_in_set('swamp', '4ED').
card_original_type('swamp'/'4ED', 'Land').
card_original_text('swamp'/'4ED', '{T}: Add {B} to your mana pool.').
card_image_name('swamp'/'4ED', 'swamp1').
card_uid('swamp'/'4ED', '4ED:Swamp:swamp1').
card_rarity('swamp'/'4ED', 'Basic Land').
card_artist('swamp'/'4ED', 'Dan Frazier').
card_multiverse_id('swamp'/'4ED', '2375').

card_in_set('swamp', '4ED').
card_original_type('swamp'/'4ED', 'Land').
card_original_text('swamp'/'4ED', '{T}: Add {B} to your mana pool.').
card_image_name('swamp'/'4ED', 'swamp2').
card_uid('swamp'/'4ED', '4ED:Swamp:swamp2').
card_rarity('swamp'/'4ED', 'Basic Land').
card_artist('swamp'/'4ED', 'Dan Frazier').
card_multiverse_id('swamp'/'4ED', '2376').

card_in_set('swamp', '4ED').
card_original_type('swamp'/'4ED', 'Land').
card_original_text('swamp'/'4ED', '{T}: Add {B} to your mana pool.').
card_image_name('swamp'/'4ED', 'swamp3').
card_uid('swamp'/'4ED', '4ED:Swamp:swamp3').
card_rarity('swamp'/'4ED', 'Basic Land').
card_artist('swamp'/'4ED', 'Dan Frazier').
card_multiverse_id('swamp'/'4ED', '2374').

card_in_set('swords to plowshares', '4ED').
card_original_type('swords to plowshares'/'4ED', 'Instant').
card_original_text('swords to plowshares'/'4ED', 'Remove target creature from the game. The creature\'s controller gains life equal to its power.').
card_image_name('swords to plowshares'/'4ED', 'swords to plowshares').
card_uid('swords to plowshares'/'4ED', '4ED:Swords to Plowshares:swords to plowshares').
card_rarity('swords to plowshares'/'4ED', 'Uncommon').
card_artist('swords to plowshares'/'4ED', 'Jeff A. Menges').
card_multiverse_id('swords to plowshares'/'4ED', '2367').

card_in_set('sylvan library', '4ED').
card_original_type('sylvan library'/'4ED', 'Enchantment').
card_original_text('sylvan library'/'4ED', 'You may draw two extra cards during your draw phase. If you do so, put two of the cards drawn this turn back on top of your library (in any order) or pay 4 life per card not replaced. Effects that prevent or redirect damage cannot be used to counter this loss of life.').
card_image_name('sylvan library'/'4ED', 'sylvan library').
card_uid('sylvan library'/'4ED', '4ED:Sylvan Library:sylvan library').
card_rarity('sylvan library'/'4ED', 'Rare').
card_artist('sylvan library'/'4ED', 'Harold McNeill').
card_multiverse_id('sylvan library'/'4ED', '2240').

card_in_set('tawnos\'s wand', '4ED').
card_original_type('tawnos\'s wand'/'4ED', 'Artifact').
card_original_text('tawnos\'s wand'/'4ED', '{2}, {T}: Target creature with power no greater than 2 becomes unblockable until end of turn. Other effects may later be used to increase the creature\'s power beyond 2.').
card_image_name('tawnos\'s wand'/'4ED', 'tawnos\'s wand').
card_uid('tawnos\'s wand'/'4ED', '4ED:Tawnos\'s Wand:tawnos\'s wand').
card_rarity('tawnos\'s wand'/'4ED', 'Uncommon').
card_artist('tawnos\'s wand'/'4ED', 'Douglas Shuler').
card_multiverse_id('tawnos\'s wand'/'4ED', '2071').

card_in_set('tawnos\'s weaponry', '4ED').
card_original_type('tawnos\'s weaponry'/'4ED', 'Artifact').
card_original_text('tawnos\'s weaponry'/'4ED', '{2}, {T}: Target creature gets +1/+1 as long as Tawnos\'s Weaponry remains tapped.\nYou may choose not to untap Tawnos\'s Weaponry during your untap phase.').
card_image_name('tawnos\'s weaponry'/'4ED', 'tawnos\'s weaponry').
card_uid('tawnos\'s weaponry'/'4ED', '4ED:Tawnos\'s Weaponry:tawnos\'s weaponry').
card_rarity('tawnos\'s weaponry'/'4ED', 'Uncommon').
card_artist('tawnos\'s weaponry'/'4ED', 'Dan Frazier').
card_flavor_text('tawnos\'s weaponry'/'4ED', 'When war machines became too costly, Tawnos\'s Weaponry replaced them.').
card_multiverse_id('tawnos\'s weaponry'/'4ED', '2072').

card_in_set('tempest efreet', '4ED').
card_original_type('tempest efreet'/'4ED', 'Summon — Efreet').
card_original_text('tempest efreet'/'4ED', '{T}: Choose a card at random from target opponent\'s hand and put it in yours. Bury Tempest Efreet in opponent\'s graveyard. The change in ownership is permanent. Play this ability as an interrupt. Before you choose the card to be switched, the opponent may prevent effect by paying 10 life or conceding game; if this is done, bury Tempest Efreet. Effects that prevent or redirect damage cannot be used to counter this loss of life. Remove Tempest Efreet from your deck before playing if not playing for ante.').
card_image_name('tempest efreet'/'4ED', 'tempest efreet').
card_uid('tempest efreet'/'4ED', '4ED:Tempest Efreet:tempest efreet').
card_rarity('tempest efreet'/'4ED', 'Rare').
card_artist('tempest efreet'/'4ED', 'NéNé Thomas').
card_multiverse_id('tempest efreet'/'4ED', '2308').

card_in_set('terror', '4ED').
card_original_type('terror'/'4ED', 'Instant').
card_original_text('terror'/'4ED', 'Bury target non-black, non-artifact creature.').
card_image_name('terror'/'4ED', 'terror').
card_uid('terror'/'4ED', '4ED:Terror:terror').
card_rarity('terror'/'4ED', 'Common').
card_artist('terror'/'4ED', 'Ron Spencer').
card_multiverse_id('terror'/'4ED', '2131').

card_in_set('tetravus', '4ED').
card_original_type('tetravus'/'4ED', 'Artifact Creature').
card_original_text('tetravus'/'4ED', 'Flying\nWhen Tetravus comes into play, put three +1/+1 counters on it.\nDuring your upkeep, you may move each of these counters on or off of Tetravus, regardless of who controls them. Counters that are removed become Tetravite tokens. Treat these tokens as 1/1 artifact creatures with flying. These creatures cannot have enchantment played on them and do not share any enchantments on Tetravus.').
card_image_name('tetravus'/'4ED', 'tetravus').
card_uid('tetravus'/'4ED', '4ED:Tetravus:tetravus').
card_rarity('tetravus'/'4ED', 'Rare').
card_artist('tetravus'/'4ED', 'Mark Tedin').
card_multiverse_id('tetravus'/'4ED', '2073').

card_in_set('the brute', '4ED').
card_original_type('the brute'/'4ED', 'Enchant Creature').
card_original_text('the brute'/'4ED', 'Target creature gets +1/+0.\n{R}{R}{R} Regenerate target creature The Brute enchants.').
card_image_name('the brute'/'4ED', 'the brute').
card_uid('the brute'/'4ED', '4ED:The Brute:the brute').
card_rarity('the brute'/'4ED', 'Common').
card_artist('the brute'/'4ED', 'Mark Poole').
card_flavor_text('the brute'/'4ED', '\"Union may be strength, but it is mere blind brute strength unless wisely directed.\"\n—Samuel Butler').
card_multiverse_id('the brute'/'4ED', '2309').

card_in_set('the hive', '4ED').
card_original_type('the hive'/'4ED', 'Artifact').
card_original_text('the hive'/'4ED', '5, {T}: Put a Wasp token into play. Treat this token as a 1/1 artifact creature with flying.').
card_image_name('the hive'/'4ED', 'the hive').
card_uid('the hive'/'4ED', '4ED:The Hive:the hive').
card_rarity('the hive'/'4ED', 'Rare').
card_artist('the hive'/'4ED', 'Sandra Everingham').
card_multiverse_id('the hive'/'4ED', '2074').

card_in_set('the rack', '4ED').
card_original_type('the rack'/'4ED', 'Artifact').
card_original_text('the rack'/'4ED', 'At the end of target opponent\'s upkeep, The Rack deals that player 1 damage for each card in his or her hand fewer than three.').
card_image_name('the rack'/'4ED', 'the rack').
card_uid('the rack'/'4ED', '4ED:The Rack:the rack').
card_rarity('the rack'/'4ED', 'Uncommon').
card_artist('the rack'/'4ED', 'Richard Thomas').
card_flavor_text('the rack'/'4ED', 'Invented in Mishra\'s earlier days, the Rack was once his most feared creation.').
card_multiverse_id('the rack'/'4ED', '2075').

card_in_set('thicket basilisk', '4ED').
card_original_type('thicket basilisk'/'4ED', 'Summon — Basilisk').
card_original_text('thicket basilisk'/'4ED', 'At the end of combat, destroy all non-wall creatures blocking or blocked by Basilisk.').
card_image_name('thicket basilisk'/'4ED', 'thicket basilisk').
card_uid('thicket basilisk'/'4ED', '4ED:Thicket Basilisk:thicket basilisk').
card_rarity('thicket basilisk'/'4ED', 'Uncommon').
card_artist('thicket basilisk'/'4ED', 'Dan Frazier').
card_flavor_text('thicket basilisk'/'4ED', 'Moss-covered statues littered the area, a macabre monument to the Basilisk\'s power.').
card_multiverse_id('thicket basilisk'/'4ED', '2241').

card_in_set('thoughtlace', '4ED').
card_original_type('thoughtlace'/'4ED', 'Interrupt').
card_original_text('thoughtlace'/'4ED', 'Change the color of target spell or target permanent to blue. Costs to cast, tap, maintain, or use a special ability of target remain unchanged.').
card_image_name('thoughtlace'/'4ED', 'thoughtlace').
card_uid('thoughtlace'/'4ED', '4ED:Thoughtlace:thoughtlace').
card_rarity('thoughtlace'/'4ED', 'Rare').
card_artist('thoughtlace'/'4ED', 'Mark Poole').
card_multiverse_id('thoughtlace'/'4ED', '2190').

card_in_set('throne of bone', '4ED').
card_original_type('throne of bone'/'4ED', 'Artifact').
card_original_text('throne of bone'/'4ED', '{1}: Gain 1 life for a successfully cast black spell. Use this effect either when the spell is cast or later in the turn but only once for each black spell cast.').
card_image_name('throne of bone'/'4ED', 'throne of bone').
card_uid('throne of bone'/'4ED', '4ED:Throne of Bone:throne of bone').
card_rarity('throne of bone'/'4ED', 'Uncommon').
card_artist('throne of bone'/'4ED', 'Anson Maddocks').
card_multiverse_id('throne of bone'/'4ED', '2076').

card_in_set('timber wolves', '4ED').
card_original_type('timber wolves'/'4ED', 'Summon — Wolves').
card_original_text('timber wolves'/'4ED', 'Banding').
card_image_name('timber wolves'/'4ED', 'timber wolves').
card_uid('timber wolves'/'4ED', '4ED:Timber Wolves:timber wolves').
card_rarity('timber wolves'/'4ED', 'Rare').
card_artist('timber wolves'/'4ED', 'Melissa A. Benson').
card_flavor_text('timber wolves'/'4ED', 'Though many think of Wolves as solitary predators, they are actually extremely social animals. During a hunt they often call to each other, which can be quite unsettling for their prey.').
card_multiverse_id('timber wolves'/'4ED', '2242').

card_in_set('time elemental', '4ED').
card_original_type('time elemental'/'4ED', 'Summon — Elemental').
card_original_text('time elemental'/'4ED', '{2}{U}{U}, {T}: Return target permanent to owner\'s hand. You cannot use this ability on permanents with enchantment cards played on them. If Time Elemental blocks or attacks, destroy it at end of combat. In this case, Time Elemental deals 5 damage to its controller.').
card_image_name('time elemental'/'4ED', 'time elemental').
card_uid('time elemental'/'4ED', '4ED:Time Elemental:time elemental').
card_rarity('time elemental'/'4ED', 'Rare').
card_artist('time elemental'/'4ED', 'Amy Weber').
card_multiverse_id('time elemental'/'4ED', '2191').

card_in_set('titania\'s song', '4ED').
card_original_type('titania\'s song'/'4ED', 'Enchantment').
card_original_text('titania\'s song'/'4ED', 'All non-creature artifacts lose all their usual abilities and become artifact creatures with toughness and power each equal to their casting costs. If Titania\'s Song leaves play, artifacts return to normal just before the untap phase of the next turn.').
card_image_name('titania\'s song'/'4ED', 'titania\'s song').
card_uid('titania\'s song'/'4ED', '4ED:Titania\'s Song:titania\'s song').
card_rarity('titania\'s song'/'4ED', 'Rare').
card_artist('titania\'s song'/'4ED', 'Kerstin Kaman').
card_multiverse_id('titania\'s song'/'4ED', '2243').

card_in_set('tranquility', '4ED').
card_original_type('tranquility'/'4ED', 'Sorcery').
card_original_text('tranquility'/'4ED', 'Destroy all enchantments.').
card_image_name('tranquility'/'4ED', 'tranquility').
card_uid('tranquility'/'4ED', '4ED:Tranquility:tranquility').
card_rarity('tranquility'/'4ED', 'Common').
card_artist('tranquility'/'4ED', 'Douglas Shuler').
card_multiverse_id('tranquility'/'4ED', '2244').

card_in_set('triskelion', '4ED').
card_original_type('triskelion'/'4ED', 'Artifact Creature').
card_original_text('triskelion'/'4ED', 'When Triskelion comes into play, put three +1/+1 counters on it.\n{0}: Remove one of these counters from Triskelion to have Triskelion deal 1 damage to target creature or player.').
card_image_name('triskelion'/'4ED', 'triskelion').
card_uid('triskelion'/'4ED', '4ED:Triskelion:triskelion').
card_rarity('triskelion'/'4ED', 'Rare').
card_artist('triskelion'/'4ED', 'Douglas Shuler').
card_flavor_text('triskelion'/'4ED', 'A brainchild of Tawnos, the Triskelion later proved both versatile and useful.').
card_multiverse_id('triskelion'/'4ED', '2077').

card_in_set('tsunami', '4ED').
card_original_type('tsunami'/'4ED', 'Sorcery').
card_original_text('tsunami'/'4ED', 'Destroy all islands.').
card_image_name('tsunami'/'4ED', 'tsunami').
card_uid('tsunami'/'4ED', '4ED:Tsunami:tsunami').
card_rarity('tsunami'/'4ED', 'Uncommon').
card_artist('tsunami'/'4ED', 'Richard Thomas').
card_multiverse_id('tsunami'/'4ED', '2245').

card_in_set('tundra wolves', '4ED').
card_original_type('tundra wolves'/'4ED', 'Summon — Wolves').
card_original_text('tundra wolves'/'4ED', 'First strike').
card_image_name('tundra wolves'/'4ED', 'tundra wolves').
card_uid('tundra wolves'/'4ED', '4ED:Tundra Wolves:tundra wolves').
card_rarity('tundra wolves'/'4ED', 'Common').
card_artist('tundra wolves'/'4ED', 'Quinton Hoover').
card_flavor_text('tundra wolves'/'4ED', 'I heard their eerie howling, the wolves calling their kindred across the frozen plains.').
card_multiverse_id('tundra wolves'/'4ED', '2368').

card_in_set('tunnel', '4ED').
card_original_type('tunnel'/'4ED', 'Instant').
card_original_text('tunnel'/'4ED', 'Bury target wall.').
card_image_name('tunnel'/'4ED', 'tunnel').
card_uid('tunnel'/'4ED', '4ED:Tunnel:tunnel').
card_rarity('tunnel'/'4ED', 'Uncommon').
card_artist('tunnel'/'4ED', 'Dan Frazier').
card_multiverse_id('tunnel'/'4ED', '2310').

card_in_set('twiddle', '4ED').
card_original_type('twiddle'/'4ED', 'Instant').
card_original_text('twiddle'/'4ED', 'Tap or untap target land, artifact, or creature.').
card_image_name('twiddle'/'4ED', 'twiddle').
card_uid('twiddle'/'4ED', '4ED:Twiddle:twiddle').
card_rarity('twiddle'/'4ED', 'Common').
card_artist('twiddle'/'4ED', 'Rob Alexander').
card_multiverse_id('twiddle'/'4ED', '2192').

card_in_set('uncle istvan', '4ED').
card_original_type('uncle istvan'/'4ED', 'Summon — Uncle Istvan').
card_original_text('uncle istvan'/'4ED', 'All damage done to Uncle Istvan by creatures is reduced to 0.').
card_image_name('uncle istvan'/'4ED', 'uncle istvan').
card_uid('uncle istvan'/'4ED', '4ED:Uncle Istvan:uncle istvan').
card_rarity('uncle istvan'/'4ED', 'Uncommon').
card_artist('uncle istvan'/'4ED', 'Daniel Gelon').
card_flavor_text('uncle istvan'/'4ED', 'Solitude drove the old hermit insane. Now he only keeps company with those he can catch.').
card_multiverse_id('uncle istvan'/'4ED', '2132').

card_in_set('unholy strength', '4ED').
card_original_type('unholy strength'/'4ED', 'Enchant Creature').
card_original_text('unholy strength'/'4ED', 'Target creature gets +2/+1.').
card_image_name('unholy strength'/'4ED', 'unholy strength').
card_uid('unholy strength'/'4ED', '4ED:Unholy Strength:unholy strength').
card_rarity('unholy strength'/'4ED', 'Common').
card_artist('unholy strength'/'4ED', 'Douglas Shuler').
card_multiverse_id('unholy strength'/'4ED', '2133').

card_in_set('unstable mutation', '4ED').
card_original_type('unstable mutation'/'4ED', 'Enchant Creature').
card_original_text('unstable mutation'/'4ED', 'Target creature gets +3/+3. During each of its controller\'s upkeeps, put a -1/-1 counter on the creature. These counters remain even if Unstable Mutation is removed.').
card_image_name('unstable mutation'/'4ED', 'unstable mutation').
card_uid('unstable mutation'/'4ED', '4ED:Unstable Mutation:unstable mutation').
card_rarity('unstable mutation'/'4ED', 'Common').
card_artist('unstable mutation'/'4ED', 'Douglas Shuler').
card_multiverse_id('unstable mutation'/'4ED', '2193').

card_in_set('unsummon', '4ED').
card_original_type('unsummon'/'4ED', 'Instant').
card_original_text('unsummon'/'4ED', 'Return target creature to owner\'s hand.').
card_image_name('unsummon'/'4ED', 'unsummon').
card_uid('unsummon'/'4ED', '4ED:Unsummon:unsummon').
card_rarity('unsummon'/'4ED', 'Common').
card_artist('unsummon'/'4ED', 'Douglas Shuler').
card_multiverse_id('unsummon'/'4ED', '2194').

card_in_set('untamed wilds', '4ED').
card_original_type('untamed wilds'/'4ED', 'Sorcery').
card_original_text('untamed wilds'/'4ED', 'Search your library for any one basic land and put it directly into play. This does not count towards your one land per turn limit. Reshuffle your library afterwards.').
card_image_name('untamed wilds'/'4ED', 'untamed wilds').
card_uid('untamed wilds'/'4ED', '4ED:Untamed Wilds:untamed wilds').
card_rarity('untamed wilds'/'4ED', 'Uncommon').
card_artist('untamed wilds'/'4ED', 'NéNé Thomas').
card_multiverse_id('untamed wilds'/'4ED', '2246').

card_in_set('urza\'s avenger', '4ED').
card_original_type('urza\'s avenger'/'4ED', 'Artifact Creature').
card_original_text('urza\'s avenger'/'4ED', '{0}: Urza\'s Avenger gets -1/-1 until end of turn and your choice of flying, banding, first strike, or trample until end of turn.').
card_image_name('urza\'s avenger'/'4ED', 'urza\'s avenger').
card_uid('urza\'s avenger'/'4ED', '4ED:Urza\'s Avenger:urza\'s avenger').
card_rarity('urza\'s avenger'/'4ED', 'Rare').
card_artist('urza\'s avenger'/'4ED', 'Amy Weber').
card_flavor_text('urza\'s avenger'/'4ED', 'Unable to settle on just one design, Urza decided to create one versatile being.').
card_multiverse_id('urza\'s avenger'/'4ED', '2078').

card_in_set('uthden troll', '4ED').
card_original_type('uthden troll'/'4ED', 'Summon — Troll').
card_original_text('uthden troll'/'4ED', '{R} Regenerate').
card_image_name('uthden troll'/'4ED', 'uthden troll').
card_uid('uthden troll'/'4ED', '4ED:Uthden Troll:uthden troll').
card_rarity('uthden troll'/'4ED', 'Uncommon').
card_artist('uthden troll'/'4ED', 'Douglas Shuler').
card_flavor_text('uthden troll'/'4ED', '\"Oi oi oi, me gotta hurt in \'ere,\nOi oi oi, me smell a ting is near,\nGonna bosh \'n gonna nosh\n\'n da hurt\'ll disappear.\"\n—Troll chant').
card_multiverse_id('uthden troll'/'4ED', '2311').

card_in_set('vampire bats', '4ED').
card_original_type('vampire bats'/'4ED', 'Summon — Bats').
card_original_text('vampire bats'/'4ED', 'Flying\n{B} +1/+0 until end of turn. You cannot spend more than {B}{B} in this way each turn.').
card_image_name('vampire bats'/'4ED', 'vampire bats').
card_uid('vampire bats'/'4ED', '4ED:Vampire Bats:vampire bats').
card_rarity('vampire bats'/'4ED', 'Common').
card_artist('vampire bats'/'4ED', 'Anson Maddocks').
card_flavor_text('vampire bats'/'4ED', '\"For something is amiss or out of place\nWhen mice with wings can wear a human face.\"\n—Theodore Roethke, \"The Bat\"').
card_multiverse_id('vampire bats'/'4ED', '2134').

card_in_set('venom', '4ED').
card_original_type('venom'/'4ED', 'Enchant Creature').
card_original_text('venom'/'4ED', 'At the end of combat, destroy all non-wall creatures blocking or blocked by target creature.').
card_image_name('venom'/'4ED', 'venom').
card_uid('venom'/'4ED', '4ED:Venom:venom').
card_rarity('venom'/'4ED', 'Common').
card_artist('venom'/'4ED', 'Tom Wänerstrand').
card_flavor_text('venom'/'4ED', '\"I told him it was just a flesh wound, but the next time I looked at him, poor Tadhg was dead and gone.\"\n—Maeveen O\'Donagh, Memoirs of a Soldier').
card_multiverse_id('venom'/'4ED', '2247').

card_in_set('verduran enchantress', '4ED').
card_original_type('verduran enchantress'/'4ED', 'Summon — Enchantress').
card_original_text('verduran enchantress'/'4ED', '{0}: Draw a card when you successfully cast an enchantment. Use this effect only once for each enchantment cast.').
card_image_name('verduran enchantress'/'4ED', 'verduran enchantress').
card_uid('verduran enchantress'/'4ED', '4ED:Verduran Enchantress:verduran enchantress').
card_rarity('verduran enchantress'/'4ED', 'Rare').
card_artist('verduran enchantress'/'4ED', 'Kev Brockschmidt').
card_flavor_text('verduran enchantress'/'4ED', 'Some say magic was first practiced by women, who have always felt strong ties to the land.').
card_multiverse_id('verduran enchantress'/'4ED', '2248').

card_in_set('visions', '4ED').
card_original_type('visions'/'4ED', 'Sorcery').
card_original_text('visions'/'4ED', 'Look at the top five cards of any library. You may then shuffle that library.').
card_image_name('visions'/'4ED', 'visions').
card_uid('visions'/'4ED', '4ED:Visions:visions').
card_rarity('visions'/'4ED', 'Uncommon').
card_artist('visions'/'4ED', 'NéNé Thomas').
card_flavor_text('visions'/'4ED', '\"Visions of glory, spare my aching sight,\nYe unborn ages, crowd not on my soul!\"\n—Thomas Gray, \"The Bard\"').
card_multiverse_id('visions'/'4ED', '2369').

card_in_set('volcanic eruption', '4ED').
card_original_type('volcanic eruption'/'4ED', 'Sorcery').
card_original_text('volcanic eruption'/'4ED', 'Destroy X target mountains. Volcanic Eruption deals 1 damage to each creature and player for each mountain put into the graveyard in this way.').
card_image_name('volcanic eruption'/'4ED', 'volcanic eruption').
card_uid('volcanic eruption'/'4ED', '4ED:Volcanic Eruption:volcanic eruption').
card_rarity('volcanic eruption'/'4ED', 'Rare').
card_artist('volcanic eruption'/'4ED', 'Douglas Shuler').
card_multiverse_id('volcanic eruption'/'4ED', '2195').

card_in_set('wall of air', '4ED').
card_original_type('wall of air'/'4ED', 'Summon — Wall').
card_original_text('wall of air'/'4ED', 'Flying').
card_image_name('wall of air'/'4ED', 'wall of air').
card_uid('wall of air'/'4ED', '4ED:Wall of Air:wall of air').
card_rarity('wall of air'/'4ED', 'Uncommon').
card_artist('wall of air'/'4ED', 'Richard Thomas').
card_flavor_text('wall of air'/'4ED', '\"This ‘standing windstorm\' can hold us off indefinitely? Ridiculous!\" Saying nothing, she put a pinch of salt on the table. With a bang she clapped her hands, and the salt disappeared, blown away.').
card_multiverse_id('wall of air'/'4ED', '2196').

card_in_set('wall of bone', '4ED').
card_original_type('wall of bone'/'4ED', 'Summon — Wall').
card_original_text('wall of bone'/'4ED', '{B} Regenerate').
card_image_name('wall of bone'/'4ED', 'wall of bone').
card_uid('wall of bone'/'4ED', '4ED:Wall of Bone:wall of bone').
card_rarity('wall of bone'/'4ED', 'Uncommon').
card_artist('wall of bone'/'4ED', 'Anson Maddocks').
card_flavor_text('wall of bone'/'4ED', 'The Wall of Bone is said to be an aspect of the Great Wall in Hel, where the bones of all sinners wait for Ragnarok, when Hela will call them forth for the final battle.').
card_multiverse_id('wall of bone'/'4ED', '2135').

card_in_set('wall of brambles', '4ED').
card_original_type('wall of brambles'/'4ED', 'Summon — Wall').
card_original_text('wall of brambles'/'4ED', '{G} Regenerate').
card_image_name('wall of brambles'/'4ED', 'wall of brambles').
card_uid('wall of brambles'/'4ED', '4ED:Wall of Brambles:wall of brambles').
card_rarity('wall of brambles'/'4ED', 'Uncommon').
card_artist('wall of brambles'/'4ED', 'Anson Maddocks').
card_flavor_text('wall of brambles'/'4ED', '\"What else, when chaos\ndraws all forces inward\nto shape a single leaf.\"\n—Conrad Aiken').
card_multiverse_id('wall of brambles'/'4ED', '2249').

card_in_set('wall of dust', '4ED').
card_original_type('wall of dust'/'4ED', 'Summon — Wall').
card_original_text('wall of dust'/'4ED', 'No creature blocked by Wall of Dust may attack during its controller\'s next turn.').
card_image_name('wall of dust'/'4ED', 'wall of dust').
card_uid('wall of dust'/'4ED', '4ED:Wall of Dust:wall of dust').
card_rarity('wall of dust'/'4ED', 'Uncommon').
card_artist('wall of dust'/'4ED', 'Richard Thomas').
card_flavor_text('wall of dust'/'4ED', 'An ever-moving swarm of dust engulfs and disorients anything that comes near.').
card_multiverse_id('wall of dust'/'4ED', '2312').

card_in_set('wall of fire', '4ED').
card_original_type('wall of fire'/'4ED', 'Summon — Wall').
card_original_text('wall of fire'/'4ED', '{R} +1/+0 until end of turn').
card_image_name('wall of fire'/'4ED', 'wall of fire').
card_uid('wall of fire'/'4ED', '4ED:Wall of Fire:wall of fire').
card_rarity('wall of fire'/'4ED', 'Uncommon').
card_artist('wall of fire'/'4ED', 'Richard Thomas').
card_flavor_text('wall of fire'/'4ED', 'Conjured from the bowels of hell, the fiery wall forms an impassable barrier, searing the soul of any creature attempting to pass through its terrible bursts of flame.').
card_multiverse_id('wall of fire'/'4ED', '2313').

card_in_set('wall of ice', '4ED').
card_original_type('wall of ice'/'4ED', 'Summon — Wall').
card_original_text('wall of ice'/'4ED', '').
card_image_name('wall of ice'/'4ED', 'wall of ice').
card_uid('wall of ice'/'4ED', '4ED:Wall of Ice:wall of ice').
card_rarity('wall of ice'/'4ED', 'Uncommon').
card_artist('wall of ice'/'4ED', 'Richard Thomas').
card_flavor_text('wall of ice'/'4ED', '\"And through the drifts the snowy cliffs\nDid send a dismal sheen:\nNor shapes of men nor beasts we ken—\nThe ice was all between.\"\n—Samuel Coleridge, \"The Rime of the Ancient Mariner\"').
card_multiverse_id('wall of ice'/'4ED', '2250').

card_in_set('wall of spears', '4ED').
card_original_type('wall of spears'/'4ED', 'Artifact Creature').
card_original_text('wall of spears'/'4ED', 'First strike, counts as a wall').
card_image_name('wall of spears'/'4ED', 'wall of spears').
card_uid('wall of spears'/'4ED', '4ED:Wall of Spears:wall of spears').
card_rarity('wall of spears'/'4ED', 'Common').
card_artist('wall of spears'/'4ED', 'Sandra Everingham').
card_flavor_text('wall of spears'/'4ED', 'Even the most conservative generals revised their tactics after the Battle of Sarinth, during which a handful of peasant-pikemen held off a trio of rampaging Craw Wurms.').
card_multiverse_id('wall of spears'/'4ED', '2079').

card_in_set('wall of stone', '4ED').
card_original_type('wall of stone'/'4ED', 'Summon — Wall').
card_original_text('wall of stone'/'4ED', '').
card_image_name('wall of stone'/'4ED', 'wall of stone').
card_uid('wall of stone'/'4ED', '4ED:Wall of Stone:wall of stone').
card_rarity('wall of stone'/'4ED', 'Uncommon').
card_artist('wall of stone'/'4ED', 'Dan Frazier').
card_flavor_text('wall of stone'/'4ED', 'The Earth herself lends her strength to these walls of living stone, which possess the stability of ancient mountains. These mighty bulwarks thwart ground-based troops, providing welcome relief for weary warriors who defend the land.').
card_multiverse_id('wall of stone'/'4ED', '2314').

card_in_set('wall of swords', '4ED').
card_original_type('wall of swords'/'4ED', 'Summon — Wall').
card_original_text('wall of swords'/'4ED', 'Flying').
card_image_name('wall of swords'/'4ED', 'wall of swords').
card_uid('wall of swords'/'4ED', '4ED:Wall of Swords:wall of swords').
card_rarity('wall of swords'/'4ED', 'Uncommon').
card_artist('wall of swords'/'4ED', 'Mark Tedin').
card_flavor_text('wall of swords'/'4ED', 'Just as the evil ones approached to slay Justina, she cast a great spell, imbuing her weapons with her own life force. Thus she fulfilled the prophecy: \"In the death of your savior will you find salvation.\"').
card_multiverse_id('wall of swords'/'4ED', '2370').

card_in_set('wall of water', '4ED').
card_original_type('wall of water'/'4ED', 'Summon — Wall').
card_original_text('wall of water'/'4ED', '{U} +1/+0 until end of turn').
card_image_name('wall of water'/'4ED', 'wall of water').
card_uid('wall of water'/'4ED', '4ED:Wall of Water:wall of water').
card_rarity('wall of water'/'4ED', 'Uncommon').
card_artist('wall of water'/'4ED', 'Richard Thomas').
card_flavor_text('wall of water'/'4ED', 'A deafening roar arose as the fury of an enormous vertical river supplanted our serenity. Eddies turned into whirling geysers, leveling everything in their path.').
card_multiverse_id('wall of water'/'4ED', '2197').

card_in_set('wall of wood', '4ED').
card_original_type('wall of wood'/'4ED', 'Summon — Wall').
card_original_text('wall of wood'/'4ED', '').
card_image_name('wall of wood'/'4ED', 'wall of wood').
card_uid('wall of wood'/'4ED', '4ED:Wall of Wood:wall of wood').
card_rarity('wall of wood'/'4ED', 'Common').
card_artist('wall of wood'/'4ED', 'Mark Tedin').
card_flavor_text('wall of wood'/'4ED', 'Everybody knows that to ward off trouble, you knock on wood. But usually it\'s better to make a wall out of the wood and let trouble do the knocking.').
card_multiverse_id('wall of wood'/'4ED', '2251').

card_in_set('wanderlust', '4ED').
card_original_type('wanderlust'/'4ED', 'Enchant Creature').
card_original_text('wanderlust'/'4ED', 'Wanderlust deals 1 damage to target creature\'s controller during that player\'s upkeep.').
card_image_name('wanderlust'/'4ED', 'wanderlust').
card_uid('wanderlust'/'4ED', '4ED:Wanderlust:wanderlust').
card_rarity('wanderlust'/'4ED', 'Uncommon').
card_artist('wanderlust'/'4ED', 'Cornelius Brudi').
card_multiverse_id('wanderlust'/'4ED', '2252').

card_in_set('war mammoth', '4ED').
card_original_type('war mammoth'/'4ED', 'Summon — Mammoth').
card_original_text('war mammoth'/'4ED', 'Trample').
card_image_name('war mammoth'/'4ED', 'war mammoth').
card_uid('war mammoth'/'4ED', '4ED:War Mammoth:war mammoth').
card_rarity('war mammoth'/'4ED', 'Common').
card_artist('war mammoth'/'4ED', 'Jeff A. Menges').
card_flavor_text('war mammoth'/'4ED', 'I didn\'t think Mammoths could ever hold a candle to a well-trained battle horse. Then one day I turned my back on a drunken soldier. His blow never landed; Mi\'cha flung the brute over ten meters.').
card_multiverse_id('war mammoth'/'4ED', '2253').

card_in_set('warp artifact', '4ED').
card_original_type('warp artifact'/'4ED', 'Enchant Artifact').
card_original_text('warp artifact'/'4ED', 'Warp Artifact deals 1 damage to target artifact\'s controller during his or her upkeep.').
card_image_name('warp artifact'/'4ED', 'warp artifact').
card_uid('warp artifact'/'4ED', '4ED:Warp Artifact:warp artifact').
card_rarity('warp artifact'/'4ED', 'Rare').
card_artist('warp artifact'/'4ED', 'Amy Weber').
card_multiverse_id('warp artifact'/'4ED', '2136').

card_in_set('water elemental', '4ED').
card_original_type('water elemental'/'4ED', 'Summon — Elemental').
card_original_text('water elemental'/'4ED', '').
card_image_name('water elemental'/'4ED', 'water elemental').
card_uid('water elemental'/'4ED', '4ED:Water Elemental:water elemental').
card_rarity('water elemental'/'4ED', 'Uncommon').
card_artist('water elemental'/'4ED', 'Jeff A. Menges').
card_flavor_text('water elemental'/'4ED', 'Unpredictable as the sea itself, Water Elementals shift without warning from tranquility to tempest. Capricious and fickle, they flow restlessly from one shape to another, expressing their moods with their physical forms.').
card_multiverse_id('water elemental'/'4ED', '2198').

card_in_set('weakness', '4ED').
card_original_type('weakness'/'4ED', 'Enchant Creature').
card_original_text('weakness'/'4ED', 'Target creature gets -2/-1.').
card_image_name('weakness'/'4ED', 'weakness').
card_uid('weakness'/'4ED', '4ED:Weakness:weakness').
card_rarity('weakness'/'4ED', 'Common').
card_artist('weakness'/'4ED', 'Douglas Shuler').
card_multiverse_id('weakness'/'4ED', '2137').

card_in_set('web', '4ED').
card_original_type('web'/'4ED', 'Enchant Creature').
card_original_text('web'/'4ED', 'Target creature gets +0/+2 and can block creatures with flying.').
card_image_name('web'/'4ED', 'web').
card_uid('web'/'4ED', '4ED:Web:web').
card_rarity('web'/'4ED', 'Rare').
card_artist('web'/'4ED', 'Rob Alexander').
card_multiverse_id('web'/'4ED', '2254').

card_in_set('whirling dervish', '4ED').
card_original_type('whirling dervish'/'4ED', 'Summon — Dervish').
card_original_text('whirling dervish'/'4ED', 'Protection from black\nPut a +1/+1 counter on Whirling Dervish at the end of each turn in which it damages opponent.').
card_image_name('whirling dervish'/'4ED', 'whirling dervish').
card_uid('whirling dervish'/'4ED', '4ED:Whirling Dervish:whirling dervish').
card_rarity('whirling dervish'/'4ED', 'Uncommon').
card_artist('whirling dervish'/'4ED', 'Susan Van Camp').
card_multiverse_id('whirling dervish'/'4ED', '2255').

card_in_set('white knight', '4ED').
card_original_type('white knight'/'4ED', 'Summon — Knight').
card_original_text('white knight'/'4ED', 'Protection from black, first strike').
card_image_name('white knight'/'4ED', 'white knight').
card_uid('white knight'/'4ED', '4ED:White Knight:white knight').
card_rarity('white knight'/'4ED', 'Uncommon').
card_artist('white knight'/'4ED', 'Daniel Gelon').
card_flavor_text('white knight'/'4ED', 'Out of the blackness and stench of the engulfing swamp emerged a shimmering figure. Only the splattered armor and ichor-stained sword hinted at the unfathomable evil the Knight had just laid waste.').
card_multiverse_id('white knight'/'4ED', '2371').

card_in_set('white mana battery', '4ED').
card_original_type('white mana battery'/'4ED', 'Artifact').
card_original_text('white mana battery'/'4ED', '{2}, {T}: Put one charge counter on White Mana Battery.\n{T}: Add {W} to your mana pool and remove as many charge counters as you wish. For each charge counter removed from White Mana Battery, add {W} to your mana pool. Play this ability as an interrupt.').
card_image_name('white mana battery'/'4ED', 'white mana battery').
card_uid('white mana battery'/'4ED', '4ED:White Mana Battery:white mana battery').
card_rarity('white mana battery'/'4ED', 'Rare').
card_artist('white mana battery'/'4ED', 'Anthony S. Waters').
card_multiverse_id('white mana battery'/'4ED', '2080').

card_in_set('white ward', '4ED').
card_original_type('white ward'/'4ED', 'Enchant Creature').
card_original_text('white ward'/'4ED', 'Target creature gains protection from white. The protection granted by White Ward does not destroy White Ward.').
card_image_name('white ward'/'4ED', 'white ward').
card_uid('white ward'/'4ED', '4ED:White Ward:white ward').
card_rarity('white ward'/'4ED', 'Uncommon').
card_artist('white ward'/'4ED', 'Dan Frazier').
card_multiverse_id('white ward'/'4ED', '2372').

card_in_set('wild growth', '4ED').
card_original_type('wild growth'/'4ED', 'Enchant Land').
card_original_text('wild growth'/'4ED', 'Wild Growth adds {G} to your mana pool each time target land is tapped for mana.').
card_image_name('wild growth'/'4ED', 'wild growth').
card_uid('wild growth'/'4ED', '4ED:Wild Growth:wild growth').
card_rarity('wild growth'/'4ED', 'Common').
card_artist('wild growth'/'4ED', 'Mark Poole').
card_multiverse_id('wild growth'/'4ED', '2256').

card_in_set('will-o\'-the-wisp', '4ED').
card_original_type('will-o\'-the-wisp'/'4ED', 'Summon — Will-O\'-The-Wisp').
card_original_text('will-o\'-the-wisp'/'4ED', 'Flying\n{B} Regenerate').
card_image_name('will-o\'-the-wisp'/'4ED', 'will-o\'-the-wisp').
card_uid('will-o\'-the-wisp'/'4ED', '4ED:Will-o\'-the-Wisp:will-o\'-the-wisp').
card_rarity('will-o\'-the-wisp'/'4ED', 'Rare').
card_artist('will-o\'-the-wisp'/'4ED', 'Jesper Myrfors').
card_flavor_text('will-o\'-the-wisp'/'4ED', '\"About, about in reel and rout\nThe death-fires danced at night;\nThe water, like a witch\'s oils,\nBurnt green, and blue and white.\"\n—Samuel Coleridge, \"The Rime of the Ancient Mariner\"').
card_multiverse_id('will-o\'-the-wisp'/'4ED', '2138').

card_in_set('winds of change', '4ED').
card_original_type('winds of change'/'4ED', 'Sorcery').
card_original_text('winds of change'/'4ED', 'All players shuffle their hands into their libraries and then draw the same number of cards they originally held.').
card_image_name('winds of change'/'4ED', 'winds of change').
card_uid('winds of change'/'4ED', '4ED:Winds of Change:winds of change').
card_rarity('winds of change'/'4ED', 'Rare').
card_artist('winds of change'/'4ED', 'Justin Hampton').
card_flavor_text('winds of change'/'4ED', '\"\'Tis the set of sails, and not the gales, Which tells us the way to go.\"\n—Ella Wheeler Wilcox').
card_multiverse_id('winds of change'/'4ED', '2315').

card_in_set('winter blast', '4ED').
card_original_type('winter blast'/'4ED', 'Sorcery').
card_original_text('winter blast'/'4ED', 'Tap X target creatures. Winter Blast deals 2 damage to each of these target creatures with flying.').
card_image_name('winter blast'/'4ED', 'winter blast').
card_uid('winter blast'/'4ED', '4ED:Winter Blast:winter blast').
card_rarity('winter blast'/'4ED', 'Uncommon').
card_artist('winter blast'/'4ED', 'Kaja Foglio').
card_flavor_text('winter blast'/'4ED', '\"Blow, winds, and crack your cheeks! rage! blow!\" —William Shakespeare, King Lear').
card_multiverse_id('winter blast'/'4ED', '2257').

card_in_set('winter orb', '4ED').
card_original_type('winter orb'/'4ED', 'Artifact').
card_original_text('winter orb'/'4ED', 'No player may untap more than one land during his or her untap phase.').
card_image_name('winter orb'/'4ED', 'winter orb').
card_uid('winter orb'/'4ED', '4ED:Winter Orb:winter orb').
card_rarity('winter orb'/'4ED', 'Rare').
card_artist('winter orb'/'4ED', 'Mark Tedin').
card_multiverse_id('winter orb'/'4ED', '2081').

card_in_set('wooden sphere', '4ED').
card_original_type('wooden sphere'/'4ED', 'Artifact').
card_original_text('wooden sphere'/'4ED', '{1}: Gain 1 life for a successfully cast green spell. Use this effect either when the spell is cast or later in the turn but only once for each green spell cast.').
card_image_name('wooden sphere'/'4ED', 'wooden sphere').
card_uid('wooden sphere'/'4ED', '4ED:Wooden Sphere:wooden sphere').
card_rarity('wooden sphere'/'4ED', 'Uncommon').
card_artist('wooden sphere'/'4ED', 'Mark Tedin').
card_multiverse_id('wooden sphere'/'4ED', '2082').

card_in_set('word of binding', '4ED').
card_original_type('word of binding'/'4ED', 'Sorcery').
card_original_text('word of binding'/'4ED', 'Tap X target creatures.').
card_image_name('word of binding'/'4ED', 'word of binding').
card_uid('word of binding'/'4ED', '4ED:Word of Binding:word of binding').
card_rarity('word of binding'/'4ED', 'Common').
card_artist('word of binding'/'4ED', 'Ron Spencer').
card_flavor_text('word of binding'/'4ED', '\"That was the worst experience of my days: standing there helpless as they killed my whole troop.\"\n—Maeveen O\'Donagh, Memoirs of a Soldier').
card_multiverse_id('word of binding'/'4ED', '2139').

card_in_set('wrath of god', '4ED').
card_original_type('wrath of god'/'4ED', 'Sorcery').
card_original_text('wrath of god'/'4ED', 'Bury all creatures.').
card_image_name('wrath of god'/'4ED', 'wrath of god').
card_uid('wrath of god'/'4ED', '4ED:Wrath of God:wrath of god').
card_rarity('wrath of god'/'4ED', 'Rare').
card_artist('wrath of god'/'4ED', 'Quinton Hoover').
card_multiverse_id('wrath of god'/'4ED', '2373').

card_in_set('xenic poltergeist', '4ED').
card_original_type('xenic poltergeist'/'4ED', 'Summon — Poltergeist').
card_original_text('xenic poltergeist'/'4ED', '{T}: Target non-creature artifact becomes an artifact creature with power and toughness each equal to its casting cost. Target retains all original abilities. This change lasts until your next upkeep.').
card_image_name('xenic poltergeist'/'4ED', 'xenic poltergeist').
card_uid('xenic poltergeist'/'4ED', '4ED:Xenic Poltergeist:xenic poltergeist').
card_rarity('xenic poltergeist'/'4ED', 'Rare').
card_artist('xenic poltergeist'/'4ED', 'Dan Frazier').
card_multiverse_id('xenic poltergeist'/'4ED', '2140').

card_in_set('yotian soldier', '4ED').
card_original_type('yotian soldier'/'4ED', 'Artifact Creature').
card_original_text('yotian soldier'/'4ED', 'Attacking does not cause Yotian Soldier to tap.').
card_image_name('yotian soldier'/'4ED', 'yotian soldier').
card_uid('yotian soldier'/'4ED', '4ED:Yotian Soldier:yotian soldier').
card_rarity('yotian soldier'/'4ED', 'Common').
card_artist('yotian soldier'/'4ED', 'Christopher Rush').
card_flavor_text('yotian soldier'/'4ED', 'After Kroog was destroyed while most of its defenders were at his side, Urza vowed that none of his allies would ever need to fear for their own defense again, even while laying siege to a city far from their homes.').
card_multiverse_id('yotian soldier'/'4ED', '2083').

card_in_set('zephyr falcon', '4ED').
card_original_type('zephyr falcon'/'4ED', 'Summon — Falcon').
card_original_text('zephyr falcon'/'4ED', 'Flying\nAttacking does not cause Zephyr Falcon to tap.').
card_image_name('zephyr falcon'/'4ED', 'zephyr falcon').
card_uid('zephyr falcon'/'4ED', '4ED:Zephyr Falcon:zephyr falcon').
card_rarity('zephyr falcon'/'4ED', 'Common').
card_artist('zephyr falcon'/'4ED', 'Heather Hudson').
card_flavor_text('zephyr falcon'/'4ED', 'Although greatly prized among falconers, the Zephyr Falcon is capricious and not easily tamed.').
card_multiverse_id('zephyr falcon'/'4ED', '2199').

card_in_set('zombie master', '4ED').
card_original_type('zombie master'/'4ED', 'Summon — Lord').
card_original_text('zombie master'/'4ED', 'All zombies gain swampwalk and \n\"{B} Regenerate.\"').
card_image_name('zombie master'/'4ED', 'zombie master').
card_uid('zombie master'/'4ED', '4ED:Zombie Master:zombie master').
card_rarity('zombie master'/'4ED', 'Rare').
card_artist('zombie master'/'4ED', 'Jeff A. Menges').
card_flavor_text('zombie master'/'4ED', 'They say the Zombie Master controlled these foul creatures even before his own death, but now that he is one of them, nothing can make them betray him.').
card_multiverse_id('zombie master'/'4ED', '2141').
