% Two-Headed Giant Tournament

set('p2HG').
set_name('p2HG', 'Two-Headed Giant Tournament').
set_release_date('p2HG', '2005-12-09').
set_border('p2HG', 'black').
set_type('p2HG', 'promo').

card_in_set('underworld dreams', 'p2HG').
card_original_type('underworld dreams'/'p2HG', 'Enchantment').
card_original_text('underworld dreams'/'p2HG', '').
card_image_name('underworld dreams'/'p2HG', 'underworld dreams').
card_uid('underworld dreams'/'p2HG', 'p2HG:Underworld Dreams:underworld dreams').
card_rarity('underworld dreams'/'p2HG', 'Special').
card_artist('underworld dreams'/'p2HG', 'Julie Baroh').
card_number('underworld dreams'/'p2HG', '1').
card_flavor_text('underworld dreams'/'p2HG', '\"In the drowsy dark cave of the mind, dreams build their nest with fragments dropped from day\'s caravan.\"\n—Rabindranath Tagore').
