% Masters Edition IV

set('ME4').
set_name('ME4', 'Masters Edition IV').
set_release_date('ME4', '2011-01-10').
set_border('ME4', 'black').
set_type('ME4', 'masters').

card_in_set('acid rain', 'ME4').
card_original_type('acid rain'/'ME4', 'Sorcery').
card_original_text('acid rain'/'ME4', 'Destroy all Forests.').
card_image_name('acid rain'/'ME4', 'acid rain').
card_uid('acid rain'/'ME4', 'ME4:Acid Rain:acid rain').
card_rarity('acid rain'/'ME4', 'Rare').
card_artist('acid rain'/'ME4', 'NéNé Thomas').
card_number('acid rain'/'ME4', '36').
card_multiverse_id('acid rain'/'ME4', '233299').

card_in_set('aesthir glider', 'ME4').
card_original_type('aesthir glider'/'ME4', 'Artifact Creature — Bird').
card_original_text('aesthir glider'/'ME4', 'Flying\nAesthir Glider can\'t block.').
card_image_name('aesthir glider'/'ME4', 'aesthir glider').
card_uid('aesthir glider'/'ME4', 'ME4:Aesthir Glider:aesthir glider').
card_rarity('aesthir glider'/'ME4', 'Common').
card_artist('aesthir glider'/'ME4', 'Ruth Thompson').
card_number('aesthir glider'/'ME4', '176').
card_flavor_text('aesthir glider'/'ME4', '\"Sacrilege! A noble ally in life, made nothing more than a glorified kite in death!\"\n—Arna Kennerüd, skycaptain').
card_multiverse_id('aesthir glider'/'ME4', '202510').

card_in_set('air elemental', 'ME4').
card_original_type('air elemental'/'ME4', 'Creature — Elemental').
card_original_text('air elemental'/'ME4', 'Flying').
card_image_name('air elemental'/'ME4', 'air elemental').
card_uid('air elemental'/'ME4', 'ME4:Air Elemental:air elemental').
card_rarity('air elemental'/'ME4', 'Uncommon').
card_artist('air elemental'/'ME4', 'Brian Snõddy').
card_number('air elemental'/'ME4', '37').
card_flavor_text('air elemental'/'ME4', 'These spirits of the air are winsome and wild and cannot be truly contained. Only marginally intelligent, they often substitute whimsy for strategy, delighting in mischief and mayhem.').
card_multiverse_id('air elemental'/'ME4', '221985').

card_in_set('al-abara\'s carpet', 'ME4').
card_original_type('al-abara\'s carpet'/'ME4', 'Artifact').
card_original_text('al-abara\'s carpet'/'ME4', '{5}, {T}: Prevent all damage that would be dealt to you this turn by attacking creatures without flying.').
card_image_name('al-abara\'s carpet'/'ME4', 'al-abara\'s carpet').
card_uid('al-abara\'s carpet'/'ME4', 'ME4:Al-abara\'s Carpet:al-abara\'s carpet').
card_rarity('al-abara\'s carpet'/'ME4', 'Rare').
card_artist('al-abara\'s carpet'/'ME4', 'Kaja Foglio').
card_number('al-abara\'s carpet'/'ME4', '177').
card_flavor_text('al-abara\'s carpet'/'ME4', 'Al-abara simply laughed and lifted one finger, and the carpet carried her high out of our reach.').
card_multiverse_id('al-abara\'s carpet'/'ME4', '228269').

card_in_set('alaborn musketeer', 'ME4').
card_original_type('alaborn musketeer'/'ME4', 'Creature — Human Soldier').
card_original_text('alaborn musketeer'/'ME4', 'Reach').
card_image_name('alaborn musketeer'/'ME4', 'alaborn musketeer').
card_uid('alaborn musketeer'/'ME4', 'ME4:Alaborn Musketeer:alaborn musketeer').
card_rarity('alaborn musketeer'/'ME4', 'Common').
card_artist('alaborn musketeer'/'ME4', 'Heather Hudson').
card_number('alaborn musketeer'/'ME4', '1').
card_flavor_text('alaborn musketeer'/'ME4', 'Muskets gave the Alaborn army an advantage it had long lacked—air defense.').
card_multiverse_id('alaborn musketeer'/'ME4', '221493').

card_in_set('alaborn trooper', 'ME4').
card_original_type('alaborn trooper'/'ME4', 'Creature — Human Soldier').
card_original_text('alaborn trooper'/'ME4', '').
card_image_name('alaborn trooper'/'ME4', 'alaborn trooper').
card_uid('alaborn trooper'/'ME4', 'ME4:Alaborn Trooper:alaborn trooper').
card_rarity('alaborn trooper'/'ME4', 'Common').
card_artist('alaborn trooper'/'ME4', 'Lubov').
card_number('alaborn trooper'/'ME4', '2').
card_flavor_text('alaborn trooper'/'ME4', '\"I dedicate my body to my country\nAnd my life to my King.\"\n—Alaborn Soldier\'s Oath').
card_multiverse_id('alaborn trooper'/'ME4', '221987').

card_in_set('aladdin', 'ME4').
card_original_type('aladdin'/'ME4', 'Creature — Human Rogue').
card_original_text('aladdin'/'ME4', '{1}{R}{R}, {T}: Gain control of target artifact for as long as you control Aladdin.').
card_image_name('aladdin'/'ME4', 'aladdin').
card_uid('aladdin'/'ME4', 'ME4:Aladdin:aladdin').
card_rarity('aladdin'/'ME4', 'Rare').
card_artist('aladdin'/'ME4', 'Julie Baroh').
card_number('aladdin'/'ME4', '106').
card_multiverse_id('aladdin'/'ME4', '202452').

card_in_set('alchor\'s tomb', 'ME4').
card_original_type('alchor\'s tomb'/'ME4', 'Artifact').
card_original_text('alchor\'s tomb'/'ME4', '{2}, {T}: Target permanent you control becomes the color of your choice.').
card_image_name('alchor\'s tomb'/'ME4', 'alchor\'s tomb').
card_uid('alchor\'s tomb'/'ME4', 'ME4:Alchor\'s Tomb:alchor\'s tomb').
card_rarity('alchor\'s tomb'/'ME4', 'Rare').
card_artist('alchor\'s tomb'/'ME4', 'Jesper Myrfors').
card_number('alchor\'s tomb'/'ME4', '178').
card_multiverse_id('alchor\'s tomb'/'ME4', '228270').

card_in_set('ali from cairo', 'ME4').
card_original_type('ali from cairo'/'ME4', 'Creature — Human').
card_original_text('ali from cairo'/'ME4', 'Damage that would reduce your life total to less than 1 reduces it to 1 instead.').
card_image_name('ali from cairo'/'ME4', 'ali from cairo').
card_uid('ali from cairo'/'ME4', 'ME4:Ali from Cairo:ali from cairo').
card_rarity('ali from cairo'/'ME4', 'Rare').
card_artist('ali from cairo'/'ME4', 'Mark Poole').
card_number('ali from cairo'/'ME4', '107').
card_multiverse_id('ali from cairo'/'ME4', '202619').

card_in_set('alluring scent', 'ME4').
card_original_type('alluring scent'/'ME4', 'Sorcery').
card_original_text('alluring scent'/'ME4', 'All creatures able to block target creature this turn do so.').
card_image_name('alluring scent'/'ME4', 'alluring scent').
card_uid('alluring scent'/'ME4', 'ME4:Alluring Scent:alluring scent').
card_rarity('alluring scent'/'ME4', 'Common').
card_artist('alluring scent'/'ME4', 'Melissa A. Benson').
card_number('alluring scent'/'ME4', '141').
card_flavor_text('alluring scent'/'ME4', 'No earthly armor is protection against this sweetness.').
card_multiverse_id('alluring scent'/'ME4', '221494').

card_in_set('amulet of kroog', 'ME4').
card_original_type('amulet of kroog'/'ME4', 'Artifact').
card_original_text('amulet of kroog'/'ME4', '{2}, {T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.').
card_image_name('amulet of kroog'/'ME4', 'amulet of kroog').
card_uid('amulet of kroog'/'ME4', 'ME4:Amulet of Kroog:amulet of kroog').
card_rarity('amulet of kroog'/'ME4', 'Common').
card_artist('amulet of kroog'/'ME4', 'Margaret Organ-Kean').
card_number('amulet of kroog'/'ME4', '179').
card_flavor_text('amulet of kroog'/'ME4', 'Among the first allies Urza gained were the people of Kroog. As a sign of friendship, Urza gave the healers of the city potent amulets; afterwards, thousands journeyed to Kroog in hope of healing, greatly adding to the city\'s glory.').
card_multiverse_id('amulet of kroog'/'ME4', '202567').

card_in_set('angelic voices', 'ME4').
card_original_type('angelic voices'/'ME4', 'Enchantment').
card_original_text('angelic voices'/'ME4', 'Creatures you control get +1/+1 as long as you control no nonartifact, nonwhite creatures.').
card_image_name('angelic voices'/'ME4', 'angelic voices').
card_uid('angelic voices'/'ME4', 'ME4:Angelic Voices:angelic voices').
card_rarity('angelic voices'/'ME4', 'Uncommon').
card_artist('angelic voices'/'ME4', 'Julie Baroh').
card_number('angelic voices'/'ME4', '3').
card_multiverse_id('angelic voices'/'ME4', '202477').

card_in_set('animate artifact', 'ME4').
card_original_type('animate artifact'/'ME4', 'Enchantment — Aura').
card_original_text('animate artifact'/'ME4', 'Enchant artifact\nAs long as enchanted artifact isn\'t a creature, it\'s an artifact creature with power and toughness each equal to its converted mana cost.').
card_image_name('animate artifact'/'ME4', 'animate artifact').
card_uid('animate artifact'/'ME4', 'ME4:Animate Artifact:animate artifact').
card_rarity('animate artifact'/'ME4', 'Uncommon').
card_artist('animate artifact'/'ME4', 'Douglas Shuler').
card_number('animate artifact'/'ME4', '38').
card_multiverse_id('animate artifact'/'ME4', '202417').

card_in_set('argivian blacksmith', 'ME4').
card_original_type('argivian blacksmith'/'ME4', 'Creature — Human Artificer').
card_original_text('argivian blacksmith'/'ME4', '{T}: Prevent the next 2 damage that would be dealt to target artifact creature this turn.').
card_image_name('argivian blacksmith'/'ME4', 'argivian blacksmith').
card_uid('argivian blacksmith'/'ME4', 'ME4:Argivian Blacksmith:argivian blacksmith').
card_rarity('argivian blacksmith'/'ME4', 'Uncommon').
card_artist('argivian blacksmith'/'ME4', 'Kerstin Kaman').
card_number('argivian blacksmith'/'ME4', '4').
card_flavor_text('argivian blacksmith'/'ME4', 'Through years of study and training, the Blacksmiths of Argive became adept at reassembling the mangled remains of the strange, mechanical creatures abounding in their native land.').
card_multiverse_id('argivian blacksmith'/'ME4', '221112').

card_in_set('argothian pixies', 'ME4').
card_original_type('argothian pixies'/'ME4', 'Creature — Faerie').
card_original_text('argothian pixies'/'ME4', 'Argothian Pixies can\'t be blocked by artifact creatures.\nPrevent all damage that would be dealt to Argothian Pixies by artifact creatures.').
card_image_name('argothian pixies'/'ME4', 'argothian pixies').
card_uid('argothian pixies'/'ME4', 'ME4:Argothian Pixies:argothian pixies').
card_rarity('argothian pixies'/'ME4', 'Common').
card_artist('argothian pixies'/'ME4', 'Amy Weber').
card_number('argothian pixies'/'ME4', '142').
card_multiverse_id('argothian pixies'/'ME4', '202589').

card_in_set('argothian treefolk', 'ME4').
card_original_type('argothian treefolk'/'ME4', 'Creature — Treefolk').
card_original_text('argothian treefolk'/'ME4', 'Prevent all damage that would be dealt to Argothian Treefolk by artifacts.').
card_image_name('argothian treefolk'/'ME4', 'argothian treefolk').
card_uid('argothian treefolk'/'ME4', 'ME4:Argothian Treefolk:argothian treefolk').
card_rarity('argothian treefolk'/'ME4', 'Uncommon').
card_artist('argothian treefolk'/'ME4', 'Amy Weber').
card_number('argothian treefolk'/'ME4', '143').
card_multiverse_id('argothian treefolk'/'ME4', '202428').

card_in_set('armageddon', 'ME4').
card_original_type('armageddon'/'ME4', 'Sorcery').
card_original_text('armageddon'/'ME4', 'Destroy all lands.').
card_image_name('armageddon'/'ME4', 'armageddon').
card_uid('armageddon'/'ME4', 'ME4:Armageddon:armageddon').
card_rarity('armageddon'/'ME4', 'Rare').
card_artist('armageddon'/'ME4', 'John Avon').
card_number('armageddon'/'ME4', '5').
card_flavor_text('armageddon'/'ME4', '\"‘O miserable of happy! Is this the end\nOf this new glorious world . . . ?\'\"\n—John Milton, Paradise Lost').
card_multiverse_id('armageddon'/'ME4', '228262').

card_in_set('armageddon clock', 'ME4').
card_original_type('armageddon clock'/'ME4', 'Artifact').
card_original_text('armageddon clock'/'ME4', 'At the beginning of your upkeep, put a doom counter on Armageddon Clock.\nAt the beginning of your draw step, Armageddon Clock deals damage to each player equal to the number of doom counters on it.\n{4}: Remove a doom counter from Armageddon Clock. Any player may activate this ability but only during any upkeep step.').
card_image_name('armageddon clock'/'ME4', 'armageddon clock').
card_uid('armageddon clock'/'ME4', 'ME4:Armageddon Clock:armageddon clock').
card_rarity('armageddon clock'/'ME4', 'Rare').
card_artist('armageddon clock'/'ME4', 'Amy Weber').
card_number('armageddon clock'/'ME4', '180').
card_multiverse_id('armageddon clock'/'ME4', '202414').

card_in_set('artifact blast', 'ME4').
card_original_type('artifact blast'/'ME4', 'Instant').
card_original_text('artifact blast'/'ME4', 'Counter target artifact spell.').
card_image_name('artifact blast'/'ME4', 'artifact blast').
card_uid('artifact blast'/'ME4', 'ME4:Artifact Blast:artifact blast').
card_rarity('artifact blast'/'ME4', 'Common').
card_artist('artifact blast'/'ME4', 'Mark Poole').
card_number('artifact blast'/'ME4', '108').
card_flavor_text('artifact blast'/'ME4', 'The first line of defense against Urza and Mishra, the Artifact Blast achieved widespread fame until an unlucky mage discovered it was useless on the devices the brothers had already created.').
card_multiverse_id('artifact blast'/'ME4', '221551').

card_in_set('ashnod\'s altar', 'ME4').
card_original_type('ashnod\'s altar'/'ME4', 'Artifact').
card_original_text('ashnod\'s altar'/'ME4', 'Sacrifice a creature: Add {2} to your mana pool.').
card_image_name('ashnod\'s altar'/'ME4', 'ashnod\'s altar').
card_uid('ashnod\'s altar'/'ME4', 'ME4:Ashnod\'s Altar:ashnod\'s altar').
card_rarity('ashnod\'s altar'/'ME4', 'Rare').
card_artist('ashnod\'s altar'/'ME4', 'Anson Maddocks').
card_number('ashnod\'s altar'/'ME4', '181').
card_multiverse_id('ashnod\'s altar'/'ME4', '202622').

card_in_set('atog', 'ME4').
card_original_type('atog'/'ME4', 'Creature — Atog').
card_original_text('atog'/'ME4', 'Sacrifice an artifact: Atog gets +2/+2 until end of turn.').
card_image_name('atog'/'ME4', 'atog').
card_uid('atog'/'ME4', 'ME4:Atog:atog').
card_rarity('atog'/'ME4', 'Common').
card_artist('atog'/'ME4', 'Jesper Myrfors').
card_number('atog'/'ME4', '109').
card_multiverse_id('atog'/'ME4', '202463').

card_in_set('badlands', 'ME4').
card_original_type('badlands'/'ME4', 'Land — Swamp Mountain').
card_original_text('badlands'/'ME4', '').
card_image_name('badlands'/'ME4', 'badlands').
card_uid('badlands'/'ME4', 'ME4:Badlands:badlands').
card_rarity('badlands'/'ME4', 'Rare').
card_artist('badlands'/'ME4', 'Rob Alexander').
card_number('badlands'/'ME4', '241').
card_multiverse_id('badlands'/'ME4', '202626').

card_in_set('balance', 'ME4').
card_original_type('balance'/'ME4', 'Sorcery').
card_original_text('balance'/'ME4', 'Each player chooses a number of lands he or she controls equal to the number of lands controlled by the player who controls the fewest, then sacrifices the rest. Players discard cards and sacrifice creatures the same way.').
card_image_name('balance'/'ME4', 'balance').
card_uid('balance'/'ME4', 'ME4:Balance:balance').
card_rarity('balance'/'ME4', 'Rare').
card_artist('balance'/'ME4', 'Mark Poole').
card_number('balance'/'ME4', '6').
card_multiverse_id('balance'/'ME4', '202501').

card_in_set('basalt monolith', 'ME4').
card_original_type('basalt monolith'/'ME4', 'Artifact').
card_original_text('basalt monolith'/'ME4', 'Basalt Monolith doesn\'t untap during your untap step.\n{3}: Untap Basalt Monolith.\n{T}: Add {3} to your mana pool.').
card_image_name('basalt monolith'/'ME4', 'basalt monolith').
card_uid('basalt monolith'/'ME4', 'ME4:Basalt Monolith:basalt monolith').
card_rarity('basalt monolith'/'ME4', 'Uncommon').
card_artist('basalt monolith'/'ME4', 'Jesper Myrfors').
card_number('basalt monolith'/'ME4', '182').
card_multiverse_id('basalt monolith'/'ME4', '202565').

card_in_set('bayou', 'ME4').
card_original_type('bayou'/'ME4', 'Land — Swamp Forest').
card_original_text('bayou'/'ME4', '').
card_image_name('bayou'/'ME4', 'bayou').
card_uid('bayou'/'ME4', 'ME4:Bayou:bayou').
card_rarity('bayou'/'ME4', 'Rare').
card_artist('bayou'/'ME4', 'Jesper Myrfors').
card_number('bayou'/'ME4', '242').
card_multiverse_id('bayou'/'ME4', '202604').

card_in_set('bee sting', 'ME4').
card_original_type('bee sting'/'ME4', 'Sorcery').
card_original_text('bee sting'/'ME4', 'Bee Sting deals 2 damage to target creature or player.').
card_image_name('bee sting'/'ME4', 'bee sting').
card_uid('bee sting'/'ME4', 'ME4:Bee Sting:bee sting').
card_rarity('bee sting'/'ME4', 'Uncommon').
card_artist('bee sting'/'ME4', 'Cris Dornaus').
card_number('bee sting'/'ME4', '144').
card_flavor_text('bee sting'/'ME4', 'Busy as a bumblebee, blistered as a goblin.\n—Alaborn saying').
card_multiverse_id('bee sting'/'ME4', '221570').

card_in_set('bird maiden', 'ME4').
card_original_type('bird maiden'/'ME4', 'Creature — Human Bird').
card_original_text('bird maiden'/'ME4', 'Flying').
card_image_name('bird maiden'/'ME4', 'bird maiden').
card_uid('bird maiden'/'ME4', 'ME4:Bird Maiden:bird maiden').
card_rarity('bird maiden'/'ME4', 'Common').
card_artist('bird maiden'/'ME4', 'Kaja Foglio').
card_number('bird maiden'/'ME4', '110').
card_flavor_text('bird maiden'/'ME4', '\"Four things that never meet do here unite To shed my blood and to ravage my heart, A radiant brow and tresses that beguile And rosy cheeks and a glittering smile.\"\n—The Arabian Nights, trans. Haddawy').
card_multiverse_id('bird maiden'/'ME4', '202553').

card_in_set('black knight', 'ME4').
card_original_type('black knight'/'ME4', 'Creature — Human Knight').
card_original_text('black knight'/'ME4', 'First strike, protection from white').
card_image_name('black knight'/'ME4', 'black knight').
card_uid('black knight'/'ME4', 'ME4:Black Knight:black knight').
card_rarity('black knight'/'ME4', 'Uncommon').
card_artist('black knight'/'ME4', 'Jeff A. Menges').
card_number('black knight'/'ME4', '71').
card_flavor_text('black knight'/'ME4', 'Battle doesn\'t need a purpose; the battle is its own purpose. You don\'t ask why a plague spreads or a field burns. Don\'t ask why I fight.').
card_multiverse_id('black knight'/'ME4', '221568').

card_in_set('blaze of glory', 'ME4').
card_original_type('blaze of glory'/'ME4', 'Instant').
card_original_text('blaze of glory'/'ME4', 'Target creature can block any number of creatures this turn. It must block each attacking creature this turn if able.').
card_image_name('blaze of glory'/'ME4', 'blaze of glory').
card_uid('blaze of glory'/'ME4', 'ME4:Blaze of Glory:blaze of glory').
card_rarity('blaze of glory'/'ME4', 'Uncommon').
card_artist('blaze of glory'/'ME4', 'Richard Thomas').
card_number('blaze of glory'/'ME4', '7').
card_multiverse_id('blaze of glory'/'ME4', '202450').

card_in_set('blue elemental blast', 'ME4').
card_original_type('blue elemental blast'/'ME4', 'Instant').
card_original_text('blue elemental blast'/'ME4', 'Choose one — Counter target red spell; or destroy target red permanent.').
card_image_name('blue elemental blast'/'ME4', 'blue elemental blast').
card_uid('blue elemental blast'/'ME4', 'ME4:Blue Elemental Blast:blue elemental blast').
card_rarity('blue elemental blast'/'ME4', 'Uncommon').
card_artist('blue elemental blast'/'ME4', 'Richard Thomas').
card_number('blue elemental blast'/'ME4', '39').
card_multiverse_id('blue elemental blast'/'ME4', '202520').

card_in_set('book of rass', 'ME4').
card_original_type('book of rass'/'ME4', 'Artifact').
card_original_text('book of rass'/'ME4', '{2}, Pay 2 life: Draw a card.').
card_image_name('book of rass'/'ME4', 'book of rass').
card_uid('book of rass'/'ME4', 'ME4:Book of Rass:book of rass').
card_rarity('book of rass'/'ME4', 'Uncommon').
card_artist('book of rass'/'ME4', 'Sandra Everingham').
card_number('book of rass'/'ME4', '183').
card_multiverse_id('book of rass'/'ME4', '202573').

card_in_set('bottle of suleiman', 'ME4').
card_original_type('bottle of suleiman'/'ME4', 'Artifact').
card_original_text('bottle of suleiman'/'ME4', '{1}, Sacrifice Bottle of Suleiman: Flip a coin. If you lose the flip, Bottle of Suleiman deals 5 damage to you. If you win the flip, put a 5/5 colorless Djinn artifact creature token with flying onto the battlefield.').
card_image_name('bottle of suleiman'/'ME4', 'bottle of suleiman').
card_uid('bottle of suleiman'/'ME4', 'ME4:Bottle of Suleiman:bottle of suleiman').
card_rarity('bottle of suleiman'/'ME4', 'Rare').
card_artist('bottle of suleiman'/'ME4', 'Jesper Myrfors').
card_number('bottle of suleiman'/'ME4', '184').
card_multiverse_id('bottle of suleiman'/'ME4', '202474').

card_in_set('braingeyser', 'ME4').
card_original_type('braingeyser'/'ME4', 'Sorcery').
card_original_text('braingeyser'/'ME4', 'Target player draws X cards.').
card_image_name('braingeyser'/'ME4', 'braingeyser').
card_uid('braingeyser'/'ME4', 'ME4:Braingeyser:braingeyser').
card_rarity('braingeyser'/'ME4', 'Rare').
card_artist('braingeyser'/'ME4', 'Mark Tedin').
card_number('braingeyser'/'ME4', '40').
card_multiverse_id('braingeyser'/'ME4', '202409').

card_in_set('brass man', 'ME4').
card_original_type('brass man'/'ME4', 'Artifact Creature — Construct').
card_original_text('brass man'/'ME4', 'Brass Man doesn\'t untap during your untap step.\nAt the beginning of your upkeep, you may pay {1}. If you do, untap Brass Man.').
card_image_name('brass man'/'ME4', 'brass man').
card_uid('brass man'/'ME4', 'ME4:Brass Man:brass man').
card_rarity('brass man'/'ME4', 'Common').
card_artist('brass man'/'ME4', 'Christopher Rush').
card_number('brass man'/'ME4', '185').
card_multiverse_id('brass man'/'ME4', '202445').

card_in_set('bronze horse', 'ME4').
card_original_type('bronze horse'/'ME4', 'Artifact Creature — Horse').
card_original_text('bronze horse'/'ME4', 'Trample\nIf you control another creature, prevent all damage that would be dealt to Bronze Horse by spells that target it.').
card_image_name('bronze horse'/'ME4', 'bronze horse').
card_uid('bronze horse'/'ME4', 'ME4:Bronze Horse:bronze horse').
card_rarity('bronze horse'/'ME4', 'Uncommon').
card_artist('bronze horse'/'ME4', 'Mark Poole').
card_number('bronze horse'/'ME4', '186').
card_multiverse_id('bronze horse'/'ME4', '202610').

card_in_set('candelabra of tawnos', 'ME4').
card_original_type('candelabra of tawnos'/'ME4', 'Artifact').
card_original_text('candelabra of tawnos'/'ME4', '{X}, {T}: Untap X target lands.').
card_image_name('candelabra of tawnos'/'ME4', 'candelabra of tawnos').
card_uid('candelabra of tawnos'/'ME4', 'ME4:Candelabra of Tawnos:candelabra of tawnos').
card_rarity('candelabra of tawnos'/'ME4', 'Rare').
card_artist('candelabra of tawnos'/'ME4', 'Douglas Shuler').
card_number('candelabra of tawnos'/'ME4', '187').
card_flavor_text('candelabra of tawnos'/'ME4', 'Tawnos learned quickly from Urza that utter simplicity often led to wondrous, yet subtle utility.').
card_multiverse_id('candelabra of tawnos'/'ME4', '202627').

card_in_set('celestial sword', 'ME4').
card_original_type('celestial sword'/'ME4', 'Artifact').
card_original_text('celestial sword'/'ME4', '{3}, {T}: Target creature you control gets +3/+3 until end of turn. Its controller sacrifices it at the beginning of the next end step.').
card_image_name('celestial sword'/'ME4', 'celestial sword').
card_uid('celestial sword'/'ME4', 'ME4:Celestial Sword:celestial sword').
card_rarity('celestial sword'/'ME4', 'Uncommon').
card_artist('celestial sword'/'ME4', 'Amy Weber').
card_number('celestial sword'/'ME4', '188').
card_flavor_text('celestial sword'/'ME4', '\"So great is its power, only the chosen can wield it and live.\"\n—Avram Garrisson, Leader of the Knights of Stromgald').
card_multiverse_id('celestial sword'/'ME4', '202600').

card_in_set('champion lancer', 'ME4').
card_original_type('champion lancer'/'ME4', 'Creature — Human Knight').
card_original_text('champion lancer'/'ME4', 'Prevent all damage that would be dealt to Champion Lancer by creatures.').
card_image_name('champion lancer'/'ME4', 'champion lancer').
card_uid('champion lancer'/'ME4', 'ME4:Champion Lancer:champion lancer').
card_rarity('champion lancer'/'ME4', 'Rare').
card_artist('champion lancer'/'ME4', 'Chippy').
card_number('champion lancer'/'ME4', '8').
card_flavor_text('champion lancer'/'ME4', 'The flash of his lance projects the pure radiance of his honor.').
card_multiverse_id('champion lancer'/'ME4', '202435').

card_in_set('channel', 'ME4').
card_original_type('channel'/'ME4', 'Sorcery').
card_original_text('channel'/'ME4', 'Until end of turn, any time you could activate a mana ability, you may pay 1 life. If you do, add {1} to your mana pool.').
card_image_name('channel'/'ME4', 'channel').
card_uid('channel'/'ME4', 'ME4:Channel:channel').
card_rarity('channel'/'ME4', 'Rare').
card_artist('channel'/'ME4', 'Richard Thomas').
card_number('channel'/'ME4', '145').
card_multiverse_id('channel'/'ME4', '202596').

card_in_set('citanul druid', 'ME4').
card_original_type('citanul druid'/'ME4', 'Creature — Human Druid').
card_original_text('citanul druid'/'ME4', 'Whenever an opponent casts an artifact spell, put a +1/+1 counter on Citanul Druid.').
card_image_name('citanul druid'/'ME4', 'citanul druid').
card_uid('citanul druid'/'ME4', 'ME4:Citanul Druid:citanul druid').
card_rarity('citanul druid'/'ME4', 'Common').
card_artist('citanul druid'/'ME4', 'Jeff A. Menges').
card_number('citanul druid'/'ME4', '146').
card_multiverse_id('citanul druid'/'ME4', '202625').

card_in_set('city of brass', 'ME4').
card_original_type('city of brass'/'ME4', 'Land').
card_original_text('city of brass'/'ME4', 'Whenever City of Brass becomes tapped, it deals 1 damage to you.\n{T}: Add one mana of any color to your mana pool.').
card_image_name('city of brass'/'ME4', 'city of brass').
card_uid('city of brass'/'ME4', 'ME4:City of Brass:city of brass').
card_rarity('city of brass'/'ME4', 'Rare').
card_artist('city of brass'/'ME4', 'Mark Tedin').
card_number('city of brass'/'ME4', '243').
card_multiverse_id('city of brass'/'ME4', '220963').

card_in_set('clay statue', 'ME4').
card_original_type('clay statue'/'ME4', 'Artifact Creature — Golem').
card_original_text('clay statue'/'ME4', '{2}: Regenerate Clay Statue.').
card_image_name('clay statue'/'ME4', 'clay statue').
card_uid('clay statue'/'ME4', 'ME4:Clay Statue:clay statue').
card_rarity('clay statue'/'ME4', 'Uncommon').
card_artist('clay statue'/'ME4', 'Jesper Myrfors').
card_number('clay statue'/'ME4', '189').
card_flavor_text('clay statue'/'ME4', 'Tawnos won fame as Urza\'s greatest assistant. After he created these warriors, Urza ended his apprenticeship, promoting him directly to the rank of master.').
card_multiverse_id('clay statue'/'ME4', '202513').

card_in_set('clockwork avian', 'ME4').
card_original_type('clockwork avian'/'ME4', 'Artifact Creature — Bird').
card_original_text('clockwork avian'/'ME4', 'Flying\nClockwork Avian enters the battlefield with four +1/+0 counters on it.\nAt end of combat, if Clockwork Avian attacked or blocked this combat, remove a +1/+0 counter from it.\n{X}, {T}: Put up to X +1/+0 counters on Clockwork Avian. This ability can\'t cause the total number of +1/+0 counters on Clockwork Avian to be greater than four. Activate this ability only during your upkeep.').
card_image_name('clockwork avian'/'ME4', 'clockwork avian').
card_uid('clockwork avian'/'ME4', 'ME4:Clockwork Avian:clockwork avian').
card_rarity('clockwork avian'/'ME4', 'Uncommon').
card_artist('clockwork avian'/'ME4', 'Randy Asplund-Faith').
card_number('clockwork avian'/'ME4', '190').
card_multiverse_id('clockwork avian'/'ME4', '202456').

card_in_set('clockwork gnomes', 'ME4').
card_original_type('clockwork gnomes'/'ME4', 'Artifact Creature — Gnome').
card_original_text('clockwork gnomes'/'ME4', '{3}, {T}: Regenerate target artifact creature.').
card_image_name('clockwork gnomes'/'ME4', 'clockwork gnomes').
card_uid('clockwork gnomes'/'ME4', 'ME4:Clockwork Gnomes:clockwork gnomes').
card_rarity('clockwork gnomes'/'ME4', 'Uncommon').
card_artist('clockwork gnomes'/'ME4', 'Douglas Shuler').
card_number('clockwork gnomes'/'ME4', '191').
card_flavor_text('clockwork gnomes'/'ME4', '\"Until I came to the Floating Isle, I never had to oil the servants.\"\n—Baki, wizard attendant').
card_multiverse_id('clockwork gnomes'/'ME4', '202431').

card_in_set('clockwork swarm', 'ME4').
card_original_type('clockwork swarm'/'ME4', 'Artifact Creature — Insect').
card_original_text('clockwork swarm'/'ME4', 'Clockwork Swarm enters the battlefield with four +1/+0 counters on it.\nClockwork Swarm can\'t be blocked by Walls.\nAt end of combat, if Clockwork Swarm attacked or blocked this combat, remove a +1/+0 counter from it.\n{X}, {T}: Put up to X +1/+0 counters on Clockwork Swarm. This ability can\'t cause the total number of +1/+0 counters on Clockwork Swarm to be greater than four. Activate this ability only during your upkeep.').
card_image_name('clockwork swarm'/'ME4', 'clockwork swarm').
card_uid('clockwork swarm'/'ME4', 'ME4:Clockwork Swarm:clockwork swarm').
card_rarity('clockwork swarm'/'ME4', 'Common').
card_artist('clockwork swarm'/'ME4', 'Amy Weber').
card_number('clockwork swarm'/'ME4', '192').
card_multiverse_id('clockwork swarm'/'ME4', '202618').

card_in_set('cloud dragon', 'ME4').
card_original_type('cloud dragon'/'ME4', 'Creature — Illusion Dragon').
card_original_text('cloud dragon'/'ME4', 'Flying\nCloud Dragon can block only creatures with flying.').
card_image_name('cloud dragon'/'ME4', 'cloud dragon').
card_uid('cloud dragon'/'ME4', 'ME4:Cloud Dragon:cloud dragon').
card_rarity('cloud dragon'/'ME4', 'Rare').
card_artist('cloud dragon'/'ME4', 'John Avon').
card_number('cloud dragon'/'ME4', '41').
card_multiverse_id('cloud dragon'/'ME4', '233300').

card_in_set('cloud spirit', 'ME4').
card_original_type('cloud spirit'/'ME4', 'Creature — Spirit').
card_original_text('cloud spirit'/'ME4', 'Flying\nCloud Spirit can block only creatures with flying.').
card_image_name('cloud spirit'/'ME4', 'cloud spirit').
card_uid('cloud spirit'/'ME4', 'ME4:Cloud Spirit:cloud spirit').
card_rarity('cloud spirit'/'ME4', 'Common').
card_artist('cloud spirit'/'ME4', 'DiTerlizzi').
card_number('cloud spirit'/'ME4', '42').
card_multiverse_id('cloud spirit'/'ME4', '221988').

card_in_set('colossus of sardia', 'ME4').
card_original_type('colossus of sardia'/'ME4', 'Artifact Creature — Golem').
card_original_text('colossus of sardia'/'ME4', 'Trample (If this creature would deal enough damage to its blockers to destroy them, you may have it deal the rest of its damage to defending player or planeswalker.)\nColossus of Sardia doesn\'t untap during your untap step.\n{9}: Untap Colossus of Sardia. Activate this ability only during your upkeep.').
card_image_name('colossus of sardia'/'ME4', 'colossus of sardia').
card_uid('colossus of sardia'/'ME4', 'ME4:Colossus of Sardia:colossus of sardia').
card_rarity('colossus of sardia'/'ME4', 'Rare').
card_artist('colossus of sardia'/'ME4', 'Jesper Myrfors').
card_number('colossus of sardia'/'ME4', '193').
card_flavor_text('colossus of sardia'/'ME4', 'Buried under a thin layer of dirt, it was known for centuries as Mount Sardia.').
card_multiverse_id('colossus of sardia'/'ME4', '221291').

card_in_set('control magic', 'ME4').
card_original_type('control magic'/'ME4', 'Enchantment — Aura').
card_original_text('control magic'/'ME4', 'Enchant creature\nYou control enchanted creature.').
card_image_name('control magic'/'ME4', 'control magic').
card_uid('control magic'/'ME4', 'ME4:Control Magic:control magic').
card_rarity('control magic'/'ME4', 'Rare').
card_artist('control magic'/'ME4', 'Dameon Willich').
card_number('control magic'/'ME4', '43').
card_multiverse_id('control magic'/'ME4', '202411').

card_in_set('conversion', 'ME4').
card_original_type('conversion'/'ME4', 'Enchantment').
card_original_text('conversion'/'ME4', 'At the beginning of your upkeep, sacrifice Conversion unless you pay {W}{W}.\nAll Mountains are Plains.').
card_image_name('conversion'/'ME4', 'conversion').
card_uid('conversion'/'ME4', 'ME4:Conversion:conversion').
card_rarity('conversion'/'ME4', 'Rare').
card_artist('conversion'/'ME4', 'Jesper Myrfors').
card_number('conversion'/'ME4', '9').
card_multiverse_id('conversion'/'ME4', '202543').

card_in_set('copy artifact', 'ME4').
card_original_type('copy artifact'/'ME4', 'Enchantment').
card_original_text('copy artifact'/'ME4', 'You may have Copy Artifact enter the battlefield as a copy of any artifact on the battlefield, except it\'s an enchantment in addition to its other types.').
card_image_name('copy artifact'/'ME4', 'copy artifact').
card_uid('copy artifact'/'ME4', 'ME4:Copy Artifact:copy artifact').
card_rarity('copy artifact'/'ME4', 'Rare').
card_artist('copy artifact'/'ME4', 'Amy Weber').
card_number('copy artifact'/'ME4', '44').
card_multiverse_id('copy artifact'/'ME4', '202611').

card_in_set('coral helm', 'ME4').
card_original_type('coral helm'/'ME4', 'Artifact').
card_original_text('coral helm'/'ME4', '{3}, Discard a card at random: Target creature gets +2/+2 until end of turn.').
card_image_name('coral helm'/'ME4', 'coral helm').
card_uid('coral helm'/'ME4', 'ME4:Coral Helm:coral helm').
card_rarity('coral helm'/'ME4', 'Uncommon').
card_artist('coral helm'/'ME4', 'Amy Weber').
card_number('coral helm'/'ME4', '194').
card_multiverse_id('coral helm'/'ME4', '202497').

card_in_set('counterspell', 'ME4').
card_original_type('counterspell'/'ME4', 'Instant').
card_original_text('counterspell'/'ME4', 'Counter target spell.').
card_image_name('counterspell'/'ME4', 'counterspell').
card_uid('counterspell'/'ME4', 'ME4:Counterspell:counterspell').
card_rarity('counterspell'/'ME4', 'Common').
card_artist('counterspell'/'ME4', 'Mark Poole').
card_number('counterspell'/'ME4', '45').
card_multiverse_id('counterspell'/'ME4', '202437').

card_in_set('crumble', 'ME4').
card_original_type('crumble'/'ME4', 'Instant').
card_original_text('crumble'/'ME4', 'Destroy target artifact. It can\'t be regenerated. That artifact\'s controller gains life equal to its converted mana cost.').
card_image_name('crumble'/'ME4', 'crumble').
card_uid('crumble'/'ME4', 'ME4:Crumble:crumble').
card_rarity('crumble'/'ME4', 'Common').
card_artist('crumble'/'ME4', 'Jesper Myrfors').
card_number('crumble'/'ME4', '147').
card_multiverse_id('crumble'/'ME4', '202582').

card_in_set('cyclone', 'ME4').
card_original_type('cyclone'/'ME4', 'Enchantment').
card_original_text('cyclone'/'ME4', 'At the beginning of your upkeep, put a wind counter on Cyclone, then sacrifice Cyclone unless you pay {G} for each wind counter on it. If you pay, Cyclone deals damage equal to the number of wind counters on it to each creature and each player.').
card_image_name('cyclone'/'ME4', 'cyclone').
card_uid('cyclone'/'ME4', 'ME4:Cyclone:cyclone').
card_rarity('cyclone'/'ME4', 'Rare').
card_artist('cyclone'/'ME4', 'Mark Tedin').
card_number('cyclone'/'ME4', '148').
card_multiverse_id('cyclone'/'ME4', '202533').

card_in_set('cyclopean mummy', 'ME4').
card_original_type('cyclopean mummy'/'ME4', 'Creature — Zombie').
card_original_text('cyclopean mummy'/'ME4', 'When Cyclopean Mummy is put into a graveyard from the battlefield, exile Cyclopean Mummy.').
card_image_name('cyclopean mummy'/'ME4', 'cyclopean mummy').
card_uid('cyclopean mummy'/'ME4', 'ME4:Cyclopean Mummy:cyclopean mummy').
card_rarity('cyclopean mummy'/'ME4', 'Common').
card_artist('cyclopean mummy'/'ME4', 'Edward P. Beard, Jr.').
card_number('cyclopean mummy'/'ME4', '72').
card_multiverse_id('cyclopean mummy'/'ME4', '202550').

card_in_set('cyclopean tomb', 'ME4').
card_original_type('cyclopean tomb'/'ME4', 'Artifact').
card_original_text('cyclopean tomb'/'ME4', '{2}, {T}: Put a mire counter on target non-Swamp land. That land is a Swamp for as long as it has a mire counter on it. Activate this ability only during your upkeep.\nWhen Cyclopean Tomb is put into a graveyard from the battlefield, at the beginning of each of your upkeeps for the rest of the game, remove all mire counters from a land that a mire counter was put onto with Cyclopean Tomb but that a mire counter has not been removed from with Cyclopean Tomb.').
card_image_name('cyclopean tomb'/'ME4', 'cyclopean tomb').
card_uid('cyclopean tomb'/'ME4', 'ME4:Cyclopean Tomb:cyclopean tomb').
card_rarity('cyclopean tomb'/'ME4', 'Rare').
card_artist('cyclopean tomb'/'ME4', 'Anson Maddocks').
card_number('cyclopean tomb'/'ME4', '195').
card_multiverse_id('cyclopean tomb'/'ME4', '202517').

card_in_set('dakmor plague', 'ME4').
card_original_type('dakmor plague'/'ME4', 'Sorcery').
card_original_text('dakmor plague'/'ME4', 'Dakmor Plague deals 3 damage to each creature and each player.').
card_image_name('dakmor plague'/'ME4', 'dakmor plague').
card_uid('dakmor plague'/'ME4', 'ME4:Dakmor Plague:dakmor plague').
card_rarity('dakmor plague'/'ME4', 'Uncommon').
card_artist('dakmor plague'/'ME4', 'Jeff Miracola').
card_number('dakmor plague'/'ME4', '73').
card_flavor_text('dakmor plague'/'ME4', 'The tiniest cough can be deadlier than the fiercest dragon.').
card_multiverse_id('dakmor plague'/'ME4', '221552').

card_in_set('dark ritual', 'ME4').
card_original_type('dark ritual'/'ME4', 'Instant').
card_original_text('dark ritual'/'ME4', 'Add {B}{B}{B} to your mana pool.').
card_image_name('dark ritual'/'ME4', 'dark ritual').
card_uid('dark ritual'/'ME4', 'ME4:Dark Ritual:dark ritual').
card_rarity('dark ritual'/'ME4', 'Common').
card_artist('dark ritual'/'ME4', 'Sandra Everingham').
card_number('dark ritual'/'ME4', '74').
card_multiverse_id('dark ritual'/'ME4', '221510').

card_in_set('deathcoil wurm', 'ME4').
card_original_type('deathcoil wurm'/'ME4', 'Creature — Wurm').
card_original_text('deathcoil wurm'/'ME4', 'You may have Deathcoil Wurm assign its combat damage as though it weren\'t blocked.').
card_image_name('deathcoil wurm'/'ME4', 'deathcoil wurm').
card_uid('deathcoil wurm'/'ME4', 'ME4:Deathcoil Wurm:deathcoil wurm').
card_rarity('deathcoil wurm'/'ME4', 'Rare').
card_artist('deathcoil wurm'/'ME4', 'Rebecca Guay').
card_number('deathcoil wurm'/'ME4', '149').
card_multiverse_id('deathcoil wurm'/'ME4', '221518').

card_in_set('deathgrip', 'ME4').
card_original_type('deathgrip'/'ME4', 'Enchantment').
card_original_text('deathgrip'/'ME4', '{B}{B}: Counter target green spell.').
card_image_name('deathgrip'/'ME4', 'deathgrip').
card_uid('deathgrip'/'ME4', 'ME4:Deathgrip:deathgrip').
card_rarity('deathgrip'/'ME4', 'Rare').
card_artist('deathgrip'/'ME4', 'Anson Maddocks').
card_number('deathgrip'/'ME4', '75').
card_multiverse_id('deathgrip'/'ME4', '202478').

card_in_set('demonic hordes', 'ME4').
card_original_type('demonic hordes'/'ME4', 'Creature — Demon').
card_original_text('demonic hordes'/'ME4', '{T}: Destroy target land.\nAt the beginning of your upkeep, unless you pay {B}{B}{B}, tap Demonic Hordes and sacrifice a land of an opponent\'s choice.').
card_image_name('demonic hordes'/'ME4', 'demonic hordes').
card_uid('demonic hordes'/'ME4', 'ME4:Demonic Hordes:demonic hordes').
card_rarity('demonic hordes'/'ME4', 'Rare').
card_artist('demonic hordes'/'ME4', 'Jesper Myrfors').
card_number('demonic hordes'/'ME4', '76').
card_flavor_text('demonic hordes'/'ME4', 'Created to destroy Dominiaria, demons can sometimes be bent to a more focused purpose.').
card_multiverse_id('demonic hordes'/'ME4', '202507').

card_in_set('demonic tutor', 'ME4').
card_original_type('demonic tutor'/'ME4', 'Sorcery').
card_original_text('demonic tutor'/'ME4', 'Search your library for a card and put that card into your hand. Then shuffle your library.').
card_image_name('demonic tutor'/'ME4', 'demonic tutor').
card_uid('demonic tutor'/'ME4', 'ME4:Demonic Tutor:demonic tutor').
card_rarity('demonic tutor'/'ME4', 'Rare').
card_artist('demonic tutor'/'ME4', 'Douglas Shuler').
card_number('demonic tutor'/'ME4', '77').
card_multiverse_id('demonic tutor'/'ME4', '202628').

card_in_set('detonate', 'ME4').
card_original_type('detonate'/'ME4', 'Sorcery').
card_original_text('detonate'/'ME4', 'Destroy target artifact with converted mana cost X. It can\'t be regenerated. Detonate deals X damage to that artifact\'s controller.').
card_image_name('detonate'/'ME4', 'detonate').
card_uid('detonate'/'ME4', 'ME4:Detonate:detonate').
card_rarity('detonate'/'ME4', 'Uncommon').
card_artist('detonate'/'ME4', 'Randy Asplund-Faith').
card_number('detonate'/'ME4', '111').
card_multiverse_id('detonate'/'ME4', '202511').

card_in_set('devastation', 'ME4').
card_original_type('devastation'/'ME4', 'Sorcery').
card_original_text('devastation'/'ME4', 'Destroy all creatures and lands.').
card_image_name('devastation'/'ME4', 'devastation').
card_uid('devastation'/'ME4', 'ME4:Devastation:devastation').
card_rarity('devastation'/'ME4', 'Rare').
card_artist('devastation'/'ME4', 'Steve Luke').
card_number('devastation'/'ME4', '112').
card_flavor_text('devastation'/'ME4', 'There is much talk about the art of creation. What about the art of destruction?').
card_multiverse_id('devastation'/'ME4', '228265').

card_in_set('diabolic machine', 'ME4').
card_original_type('diabolic machine'/'ME4', 'Artifact Creature — Construct').
card_original_text('diabolic machine'/'ME4', '{3}: Regenerate Diabolic Machine.').
card_image_name('diabolic machine'/'ME4', 'diabolic machine').
card_uid('diabolic machine'/'ME4', 'ME4:Diabolic Machine:diabolic machine').
card_rarity('diabolic machine'/'ME4', 'Uncommon').
card_artist('diabolic machine'/'ME4', 'Anson Maddocks').
card_number('diabolic machine'/'ME4', '196').
card_flavor_text('diabolic machine'/'ME4', '\"The bolts of our ballistae smashed into the monstrous thing, but our hopes died in our chests as its gears continued turning.\"\n—Sevti Mukul, The Fall of Alsoor').
card_multiverse_id('diabolic machine'/'ME4', '202623').

card_in_set('divine offering', 'ME4').
card_original_type('divine offering'/'ME4', 'Instant').
card_original_text('divine offering'/'ME4', 'Destroy target artifact. You gain life equal to its converted mana cost.').
card_image_name('divine offering'/'ME4', 'divine offering').
card_uid('divine offering'/'ME4', 'ME4:Divine Offering:divine offering').
card_rarity('divine offering'/'ME4', 'Common').
card_artist('divine offering'/'ME4', 'Jeff A. Menges').
card_number('divine offering'/'ME4', '10').
card_multiverse_id('divine offering'/'ME4', '202485').

card_in_set('dragon engine', 'ME4').
card_original_type('dragon engine'/'ME4', 'Artifact Creature — Construct').
card_original_text('dragon engine'/'ME4', '{2}: Dragon Engine gets +1/+0 until end of turn.').
card_image_name('dragon engine'/'ME4', 'dragon engine').
card_uid('dragon engine'/'ME4', 'ME4:Dragon Engine:dragon engine').
card_rarity('dragon engine'/'ME4', 'Common').
card_artist('dragon engine'/'ME4', 'Anson Maddocks').
card_number('dragon engine'/'ME4', '197').
card_flavor_text('dragon engine'/'ME4', 'Those who believed the city of Kroog would never fall to Mishra\'s forces severely underestimated the might of his war machines.').
card_multiverse_id('dragon engine'/'ME4', '221103').

card_in_set('drain power', 'ME4').
card_original_type('drain power'/'ME4', 'Sorcery').
card_original_text('drain power'/'ME4', 'Target player activates a mana ability of each land he or she controls, then empties his or her mana pool. Add mana to your mana pool equal to the type and amount emptied from that player\'s mana pool this way.').
card_image_name('drain power'/'ME4', 'drain power').
card_uid('drain power'/'ME4', 'ME4:Drain Power:drain power').
card_rarity('drain power'/'ME4', 'Rare').
card_artist('drain power'/'ME4', 'Douglas Shuler').
card_number('drain power'/'ME4', '46').
card_multiverse_id('drain power'/'ME4', '202532').

card_in_set('dread reaper', 'ME4').
card_original_type('dread reaper'/'ME4', 'Creature — Horror').
card_original_text('dread reaper'/'ME4', 'Flying\nWhen Dread Reaper enters the battlefield, you lose 5 life.').
card_image_name('dread reaper'/'ME4', 'dread reaper').
card_uid('dread reaper'/'ME4', 'ME4:Dread Reaper:dread reaper').
card_rarity('dread reaper'/'ME4', 'Rare').
card_artist('dread reaper'/'ME4', 'Christopher Rush').
card_number('dread reaper'/'ME4', '78').
card_multiverse_id('dread reaper'/'ME4', '228266').

card_in_set('dread wight', 'ME4').
card_original_type('dread wight'/'ME4', 'Creature — Zombie').
card_original_text('dread wight'/'ME4', 'At end of combat, put a paralyzation counter on all creatures blocking or blocked by Dread Wight and tap those creatures. They gain \"If this creature has a paralyzation counter on it, it doesn\'t untap during its controller\'s untap step\" and \"{4}: Remove a paralyzation counter from this creature.\"').
card_image_name('dread wight'/'ME4', 'dread wight').
card_uid('dread wight'/'ME4', 'ME4:Dread Wight:dread wight').
card_rarity('dread wight'/'ME4', 'Uncommon').
card_artist('dread wight'/'ME4', 'Daniel Gelon').
card_number('dread wight'/'ME4', '79').
card_multiverse_id('dread wight'/'ME4', '202467').

card_in_set('drop of honey', 'ME4').
card_original_type('drop of honey'/'ME4', 'Enchantment').
card_original_text('drop of honey'/'ME4', 'At the beginning of your upkeep, destroy the creature with the least power. It can\'t be regenerated. If two or more creatures are tied for least power, you choose one of them.\nWhen there are no creatures on the battlefield, sacrifice Drop of Honey.').
card_image_name('drop of honey'/'ME4', 'drop of honey').
card_uid('drop of honey'/'ME4', 'ME4:Drop of Honey:drop of honey').
card_rarity('drop of honey'/'ME4', 'Rare').
card_artist('drop of honey'/'ME4', 'Anson Maddocks').
card_number('drop of honey'/'ME4', '150').
card_multiverse_id('drop of honey'/'ME4', '202482').

card_in_set('drowned', 'ME4').
card_original_type('drowned'/'ME4', 'Creature — Zombie').
card_original_text('drowned'/'ME4', '{B}: Regenerate Drowned.').
card_image_name('drowned'/'ME4', 'drowned').
card_uid('drowned'/'ME4', 'ME4:Drowned:drowned').
card_rarity('drowned'/'ME4', 'Common').
card_artist('drowned'/'ME4', 'Quinton Hoover').
card_number('drowned'/'ME4', '47').
card_flavor_text('drowned'/'ME4', 'We asked Captain Soll what became of the Serafina, but all he said was, \"Ships that go down shouldn\'t come back up.\"').
card_multiverse_id('drowned'/'ME4', '202554').

card_in_set('dust to dust', 'ME4').
card_original_type('dust to dust'/'ME4', 'Sorcery').
card_original_text('dust to dust'/'ME4', 'Exile two target artifacts.').
card_image_name('dust to dust'/'ME4', 'dust to dust').
card_uid('dust to dust'/'ME4', 'ME4:Dust to Dust:dust to dust').
card_rarity('dust to dust'/'ME4', 'Uncommon').
card_artist('dust to dust'/'ME4', 'Drew Tucker').
card_number('dust to dust'/'ME4', '11').
card_flavor_text('dust to dust'/'ME4', '\"All this nonsense made by mages Rusts and crumbles through the ages.\"\n—Aline Corralurn, \"Inheritance\"').
card_multiverse_id('dust to dust'/'ME4', '202542').

card_in_set('ebon dragon', 'ME4').
card_original_type('ebon dragon'/'ME4', 'Creature — Dragon').
card_original_text('ebon dragon'/'ME4', 'Flying\nWhen Ebon Dragon enters the battlefield, you may have target opponent discard a card.').
card_image_name('ebon dragon'/'ME4', 'ebon dragon').
card_uid('ebon dragon'/'ME4', 'ME4:Ebon Dragon:ebon dragon').
card_rarity('ebon dragon'/'ME4', 'Rare').
card_artist('ebon dragon'/'ME4', 'Donato Giancola').
card_number('ebon dragon'/'ME4', '80').
card_multiverse_id('ebon dragon'/'ME4', '221547').

card_in_set('ebony horse', 'ME4').
card_original_type('ebony horse'/'ME4', 'Artifact').
card_original_text('ebony horse'/'ME4', '{2}, {T}: Untap target attacking creature you control. Prevent all combat damage that would be dealt to and dealt by that creature this turn.').
card_image_name('ebony horse'/'ME4', 'ebony horse').
card_uid('ebony horse'/'ME4', 'ME4:Ebony Horse:ebony horse').
card_rarity('ebony horse'/'ME4', 'Common').
card_artist('ebony horse'/'ME4', 'Dameon Willich').
card_number('ebony horse'/'ME4', '198').
card_multiverse_id('ebony horse'/'ME4', '202524').

card_in_set('ebony rhino', 'ME4').
card_original_type('ebony rhino'/'ME4', 'Artifact Creature — Rhino').
card_original_text('ebony rhino'/'ME4', 'Trample').
card_image_name('ebony rhino'/'ME4', 'ebony rhino').
card_uid('ebony rhino'/'ME4', 'ME4:Ebony Rhino:ebony rhino').
card_rarity('ebony rhino'/'ME4', 'Common').
card_artist('ebony rhino'/'ME4', 'Amy Weber').
card_number('ebony rhino'/'ME4', '199').
card_flavor_text('ebony rhino'/'ME4', '\"That Rhino would fetch us a tidy sum, Joven. Perhaps it\'s time to make it ours.\"\n—Chandler').
card_multiverse_id('ebony rhino'/'ME4', '202523').

card_in_set('elephant graveyard', 'ME4').
card_original_type('elephant graveyard'/'ME4', 'Land').
card_original_text('elephant graveyard'/'ME4', '{T}: Add {1} to your mana pool.\n{T}: Regenerate target Elephant.').
card_image_name('elephant graveyard'/'ME4', 'elephant graveyard').
card_uid('elephant graveyard'/'ME4', 'ME4:Elephant Graveyard:elephant graveyard').
card_rarity('elephant graveyard'/'ME4', 'Uncommon').
card_artist('elephant graveyard'/'ME4', 'Rob Alexander').
card_number('elephant graveyard'/'ME4', '244').
card_multiverse_id('elephant graveyard'/'ME4', '202432').

card_in_set('elite cat warrior', 'ME4').
card_original_type('elite cat warrior'/'ME4', 'Creature — Cat Warrior').
card_original_text('elite cat warrior'/'ME4', 'Forestwalk').
card_image_name('elite cat warrior'/'ME4', 'elite cat warrior').
card_uid('elite cat warrior'/'ME4', 'ME4:Elite Cat Warrior:elite cat warrior').
card_rarity('elite cat warrior'/'ME4', 'Common').
card_artist('elite cat warrior'/'ME4', 'Eric Peterson').
card_number('elite cat warrior'/'ME4', '151').
card_flavor_text('elite cat warrior'/'ME4', '\"Hear that? No? That\'s a cat warrior.\"').
card_multiverse_id('elite cat warrior'/'ME4', '221917').

card_in_set('energy flux', 'ME4').
card_original_type('energy flux'/'ME4', 'Enchantment').
card_original_text('energy flux'/'ME4', 'All artifacts have \"At the beginning of your upkeep, sacrifice this artifact unless you pay {2}.\"').
card_image_name('energy flux'/'ME4', 'energy flux').
card_uid('energy flux'/'ME4', 'ME4:Energy Flux:energy flux').
card_rarity('energy flux'/'ME4', 'Uncommon').
card_artist('energy flux'/'ME4', 'Kaja Foglio').
card_number('energy flux'/'ME4', '48').
card_multiverse_id('energy flux'/'ME4', '202480').

card_in_set('eye for an eye', 'ME4').
card_original_type('eye for an eye'/'ME4', 'Instant').
card_original_text('eye for an eye'/'ME4', 'The next time a source of your choice would deal damage to you this turn, instead that source deals that much damage to you and Eye for an Eye deals that much damage to that source\'s controller.').
card_image_name('eye for an eye'/'ME4', 'eye for an eye').
card_uid('eye for an eye'/'ME4', 'ME4:Eye for an Eye:eye for an eye').
card_rarity('eye for an eye'/'ME4', 'Rare').
card_artist('eye for an eye'/'ME4', 'Mark Poole').
card_number('eye for an eye'/'ME4', '12').
card_multiverse_id('eye for an eye'/'ME4', '202492').

card_in_set('false summoning', 'ME4').
card_original_type('false summoning'/'ME4', 'Instant').
card_original_text('false summoning'/'ME4', 'Counter target creature spell.').
card_image_name('false summoning'/'ME4', 'false summoning').
card_uid('false summoning'/'ME4', 'ME4:False Summoning:false summoning').
card_rarity('false summoning'/'ME4', 'Common').
card_artist('false summoning'/'ME4', 'DiTerlizzi').
card_number('false summoning'/'ME4', '49').
card_multiverse_id('false summoning'/'ME4', '202549').

card_in_set('fastbond', 'ME4').
card_original_type('fastbond'/'ME4', 'Enchantment').
card_original_text('fastbond'/'ME4', 'You may play any number of additional lands on each of your turns.\nWhenever you play a land, if it wasn\'t the first land you played this turn, Fastbond deals 1 damage to you.').
card_image_name('fastbond'/'ME4', 'fastbond').
card_uid('fastbond'/'ME4', 'ME4:Fastbond:fastbond').
card_rarity('fastbond'/'ME4', 'Rare').
card_artist('fastbond'/'ME4', 'Mark Poole').
card_number('fastbond'/'ME4', '152').
card_multiverse_id('fastbond'/'ME4', '202529').

card_in_set('fire imp', 'ME4').
card_original_type('fire imp'/'ME4', 'Creature — Imp').
card_original_text('fire imp'/'ME4', 'When Fire Imp enters the battlefield, it deals 2 damage to target creature.').
card_image_name('fire imp'/'ME4', 'fire imp').
card_uid('fire imp'/'ME4', 'ME4:Fire Imp:fire imp').
card_rarity('fire imp'/'ME4', 'Uncommon').
card_artist('fire imp'/'ME4', 'DiTerlizzi').
card_number('fire imp'/'ME4', '113').
card_multiverse_id('fire imp'/'ME4', '202575').

card_in_set('fire tempest', 'ME4').
card_original_type('fire tempest'/'ME4', 'Sorcery').
card_original_text('fire tempest'/'ME4', 'Fire Tempest deals 6 damage to each creature and each player.').
card_image_name('fire tempest'/'ME4', 'fire tempest').
card_uid('fire tempest'/'ME4', 'ME4:Fire Tempest:fire tempest').
card_rarity('fire tempest'/'ME4', 'Rare').
card_artist('fire tempest'/'ME4', 'Mike Dringenberg').
card_number('fire tempest'/'ME4', '114').
card_multiverse_id('fire tempest'/'ME4', '202537').

card_in_set('fireball', 'ME4').
card_original_type('fireball'/'ME4', 'Sorcery').
card_original_text('fireball'/'ME4', 'Fireball deals X damage divided evenly, rounded down, among any number of target creatures and/or players.\nFireball costs {1} more to cast for each target beyond the first.').
card_image_name('fireball'/'ME4', 'fireball').
card_uid('fireball'/'ME4', 'ME4:Fireball:fireball').
card_rarity('fireball'/'ME4', 'Uncommon').
card_artist('fireball'/'ME4', 'Mark Tedin').
card_number('fireball'/'ME4', '115').
card_multiverse_id('fireball'/'ME4', '221511').

card_in_set('floodwater dam', 'ME4').
card_original_type('floodwater dam'/'ME4', 'Artifact').
card_original_text('floodwater dam'/'ME4', '{X}{X}{1}, {T}: Tap X target lands.').
card_image_name('floodwater dam'/'ME4', 'floodwater dam').
card_uid('floodwater dam'/'ME4', 'ME4:Floodwater Dam:floodwater dam').
card_rarity('floodwater dam'/'ME4', 'Rare').
card_artist('floodwater dam'/'ME4', 'Randy Gallegos').
card_number('floodwater dam'/'ME4', '200').
card_flavor_text('floodwater dam'/'ME4', 'Viscerid dams may be of living creatures as well as bones and mud.').
card_multiverse_id('floodwater dam'/'ME4', '202464').

card_in_set('flying carpet', 'ME4').
card_original_type('flying carpet'/'ME4', 'Artifact').
card_original_text('flying carpet'/'ME4', '{2}, {T}: Target creature gains flying until end of turn.').
card_image_name('flying carpet'/'ME4', 'flying carpet').
card_uid('flying carpet'/'ME4', 'ME4:Flying Carpet:flying carpet').
card_rarity('flying carpet'/'ME4', 'Common').
card_artist('flying carpet'/'ME4', 'Mark Tedin').
card_number('flying carpet'/'ME4', '201').
card_multiverse_id('flying carpet'/'ME4', '221106').

card_in_set('fog', 'ME4').
card_original_type('fog'/'ME4', 'Instant').
card_original_text('fog'/'ME4', 'Prevent all combat damage that would be dealt this turn.').
card_image_name('fog'/'ME4', 'fog').
card_uid('fog'/'ME4', 'ME4:Fog:fog').
card_rarity('fog'/'ME4', 'Common').
card_artist('fog'/'ME4', 'Jesper Myrfors').
card_number('fog'/'ME4', '153').
card_multiverse_id('fog'/'ME4', '202617').

card_in_set('force of nature', 'ME4').
card_original_type('force of nature'/'ME4', 'Creature — Elemental').
card_original_text('force of nature'/'ME4', 'Trample (If this creature would deal enough damage to its blockers to destroy them, you may have it deal the rest of its damage to defending player or planeswalker.)\nAt the beginning of your upkeep, Force of Nature deals 8 damage to you unless you pay {G}{G}{G}{G}.').
card_image_name('force of nature'/'ME4', 'force of nature').
card_uid('force of nature'/'ME4', 'ME4:Force of Nature:force of nature').
card_rarity('force of nature'/'ME4', 'Rare').
card_artist('force of nature'/'ME4', 'Douglas Shuler').
card_number('force of nature'/'ME4', '154').
card_multiverse_id('force of nature'/'ME4', '233304').

card_in_set('fork', 'ME4').
card_original_type('fork'/'ME4', 'Instant').
card_original_text('fork'/'ME4', 'Copy target instant or sorcery spell, except that the copy is red. You may choose new targets for the copy.').
card_image_name('fork'/'ME4', 'fork').
card_uid('fork'/'ME4', 'ME4:Fork:fork').
card_rarity('fork'/'ME4', 'Rare').
card_artist('fork'/'ME4', 'Amy Weber').
card_number('fork'/'ME4', '116').
card_multiverse_id('fork'/'ME4', '202493').

card_in_set('foul spirit', 'ME4').
card_original_type('foul spirit'/'ME4', 'Creature — Spirit').
card_original_text('foul spirit'/'ME4', 'Flying\nWhen Foul Spirit enters the battlefield, sacrifice a land.').
card_image_name('foul spirit'/'ME4', 'foul spirit').
card_uid('foul spirit'/'ME4', 'ME4:Foul Spirit:foul spirit').
card_rarity('foul spirit'/'ME4', 'Common').
card_artist('foul spirit'/'ME4', 'rk post').
card_number('foul spirit'/'ME4', '81').
card_multiverse_id('foul spirit'/'ME4', '221495').

card_in_set('gaea\'s avenger', 'ME4').
card_original_type('gaea\'s avenger'/'ME4', 'Creature — Treefolk').
card_original_text('gaea\'s avenger'/'ME4', 'Gaea\'s Avenger\'s power and toughness are each equal to 1 plus the number of artifacts your opponents control.').
card_image_name('gaea\'s avenger'/'ME4', 'gaea\'s avenger').
card_uid('gaea\'s avenger'/'ME4', 'ME4:Gaea\'s Avenger:gaea\'s avenger').
card_rarity('gaea\'s avenger'/'ME4', 'Uncommon').
card_artist('gaea\'s avenger'/'ME4', 'Pete Venters').
card_number('gaea\'s avenger'/'ME4', '155').
card_multiverse_id('gaea\'s avenger'/'ME4', '202448').

card_in_set('gate to phyrexia', 'ME4').
card_original_type('gate to phyrexia'/'ME4', 'Enchantment').
card_original_text('gate to phyrexia'/'ME4', 'Sacrifice a creature: Destroy target artifact. Activate this ability only during your upkeep and only once each turn.').
card_image_name('gate to phyrexia'/'ME4', 'gate to phyrexia').
card_uid('gate to phyrexia'/'ME4', 'ME4:Gate to Phyrexia:gate to phyrexia').
card_rarity('gate to phyrexia'/'ME4', 'Uncommon').
card_artist('gate to phyrexia'/'ME4', 'Sandra Everingham').
card_number('gate to phyrexia'/'ME4', '82').
card_flavor_text('gate to phyrexia'/'ME4', '\"The warm rain of grease on my face immediately made it clear I had entered Phyrexia.\"\n—Jarsyl, Diary').
card_multiverse_id('gate to phyrexia'/'ME4', '202534').

card_in_set('gauntlet of might', 'ME4').
card_original_type('gauntlet of might'/'ME4', 'Artifact').
card_original_text('gauntlet of might'/'ME4', 'Red creatures get +1/+1.\nWhenever a Mountain is tapped for mana, its controller adds {R} to his or her mana pool (in addition to the mana the land produces).').
card_image_name('gauntlet of might'/'ME4', 'gauntlet of might').
card_uid('gauntlet of might'/'ME4', 'ME4:Gauntlet of Might:gauntlet of might').
card_rarity('gauntlet of might'/'ME4', 'Rare').
card_artist('gauntlet of might'/'ME4', 'Christopher Rush').
card_number('gauntlet of might'/'ME4', '202').
card_multiverse_id('gauntlet of might'/'ME4', '202629').

card_in_set('giant growth', 'ME4').
card_original_type('giant growth'/'ME4', 'Instant').
card_original_text('giant growth'/'ME4', 'Target creature gets +3/+3 until end of turn.').
card_image_name('giant growth'/'ME4', 'giant growth').
card_uid('giant growth'/'ME4', 'ME4:Giant Growth:giant growth').
card_rarity('giant growth'/'ME4', 'Common').
card_artist('giant growth'/'ME4', 'Sandra Everingham').
card_number('giant growth'/'ME4', '156').
card_multiverse_id('giant growth'/'ME4', '202528').

card_in_set('giant tortoise', 'ME4').
card_original_type('giant tortoise'/'ME4', 'Creature — Turtle').
card_original_text('giant tortoise'/'ME4', 'Giant Tortoise gets +0/+3 as long as it\'s untapped.').
card_image_name('giant tortoise'/'ME4', 'giant tortoise').
card_uid('giant tortoise'/'ME4', 'ME4:Giant Tortoise:giant tortoise').
card_rarity('giant tortoise'/'ME4', 'Common').
card_artist('giant tortoise'/'ME4', 'Kaja Foglio').
card_number('giant tortoise'/'ME4', '50').
card_multiverse_id('giant tortoise'/'ME4', '221565').

card_in_set('glasses of urza', 'ME4').
card_original_type('glasses of urza'/'ME4', 'Artifact').
card_original_text('glasses of urza'/'ME4', '{T}: Look at target player\'s hand.').
card_image_name('glasses of urza'/'ME4', 'glasses of urza').
card_uid('glasses of urza'/'ME4', 'ME4:Glasses of Urza:glasses of urza').
card_rarity('glasses of urza'/'ME4', 'Common').
card_artist('glasses of urza'/'ME4', 'Douglas Shuler').
card_number('glasses of urza'/'ME4', '203').
card_multiverse_id('glasses of urza'/'ME4', '202530').

card_in_set('gloom', 'ME4').
card_original_type('gloom'/'ME4', 'Enchantment').
card_original_text('gloom'/'ME4', 'White spells cost {3} more to cast.\nActivated abilities of white enchantments cost {3} more to activate.').
card_image_name('gloom'/'ME4', 'gloom').
card_uid('gloom'/'ME4', 'ME4:Gloom:gloom').
card_rarity('gloom'/'ME4', 'Rare').
card_artist('gloom'/'ME4', 'Dan Frazier').
card_number('gloom'/'ME4', '83').
card_multiverse_id('gloom'/'ME4', '202516').

card_in_set('goblin bully', 'ME4').
card_original_type('goblin bully'/'ME4', 'Creature — Goblin').
card_original_text('goblin bully'/'ME4', '').
card_image_name('goblin bully'/'ME4', 'goblin bully').
card_uid('goblin bully'/'ME4', 'ME4:Goblin Bully:goblin bully').
card_rarity('goblin bully'/'ME4', 'Common').
card_artist('goblin bully'/'ME4', 'Pete Venters').
card_number('goblin bully'/'ME4', '117').
card_flavor_text('goblin bully'/'ME4', 'It\'s easy to stand head and shoulders over those with no backbone.').
card_multiverse_id('goblin bully'/'ME4', '221507').

card_in_set('goblin cavaliers', 'ME4').
card_original_type('goblin cavaliers'/'ME4', 'Creature — Goblin').
card_original_text('goblin cavaliers'/'ME4', '').
card_image_name('goblin cavaliers'/'ME4', 'goblin cavaliers').
card_uid('goblin cavaliers'/'ME4', 'ME4:Goblin Cavaliers:goblin cavaliers').
card_rarity('goblin cavaliers'/'ME4', 'Common').
card_artist('goblin cavaliers'/'ME4', 'DiTerlizzi').
card_number('goblin cavaliers'/'ME4', '118').
card_flavor_text('goblin cavaliers'/'ME4', 'They get along so well with their goats because they\'re practically goats themselves.').
card_multiverse_id('goblin cavaliers'/'ME4', '202608').

card_in_set('goblin caves', 'ME4').
card_original_type('goblin caves'/'ME4', 'Enchantment — Aura').
card_original_text('goblin caves'/'ME4', 'Enchant land\nIf enchanted land is a basic Mountain, Goblin creatures get +0/+2.').
card_image_name('goblin caves'/'ME4', 'goblin caves').
card_uid('goblin caves'/'ME4', 'ME4:Goblin Caves:goblin caves').
card_rarity('goblin caves'/'ME4', 'Common').
card_artist('goblin caves'/'ME4', 'Drew Tucker').
card_number('goblin caves'/'ME4', '119').
card_multiverse_id('goblin caves'/'ME4', '202426').

card_in_set('goblin firestarter', 'ME4').
card_original_type('goblin firestarter'/'ME4', 'Creature — Goblin').
card_original_text('goblin firestarter'/'ME4', 'Sacrifice Goblin Firestarter: Goblin Firestarter deals 1 damage to target creature or player. Activate this ability only during your turn, before attackers are declared.').
card_image_name('goblin firestarter'/'ME4', 'goblin firestarter').
card_uid('goblin firestarter'/'ME4', 'ME4:Goblin Firestarter:goblin firestarter').
card_rarity('goblin firestarter'/'ME4', 'Common').
card_artist('goblin firestarter'/'ME4', 'Keith Parkinson').
card_number('goblin firestarter'/'ME4', '120').
card_multiverse_id('goblin firestarter'/'ME4', '202471').

card_in_set('goblin general', 'ME4').
card_original_type('goblin general'/'ME4', 'Creature — Goblin Warrior').
card_original_text('goblin general'/'ME4', 'Whenever Goblin General attacks, Goblin creatures you control get +1/+1 until end of turn.').
card_image_name('goblin general'/'ME4', 'goblin general').
card_uid('goblin general'/'ME4', 'ME4:Goblin General:goblin general').
card_rarity('goblin general'/'ME4', 'Uncommon').
card_artist('goblin general'/'ME4', 'Keith Parkinson').
card_number('goblin general'/'ME4', '121').
card_flavor_text('goblin general'/'ME4', 'Lead, follow, or run like crazy.').
card_multiverse_id('goblin general'/'ME4', '202475').

card_in_set('goblin shrine', 'ME4').
card_original_type('goblin shrine'/'ME4', 'Enchantment — Aura').
card_original_text('goblin shrine'/'ME4', 'Enchant land\nIf enchanted land is a Mountain, Goblin creatures get +1/+0.\nWhen Goblin Shrine leaves the battlefield, it deals 1 damage to each Goblin creature.').
card_image_name('goblin shrine'/'ME4', 'goblin shrine').
card_uid('goblin shrine'/'ME4', 'ME4:Goblin Shrine:goblin shrine').
card_rarity('goblin shrine'/'ME4', 'Common').
card_artist('goblin shrine'/'ME4', 'Ron Spencer').
card_number('goblin shrine'/'ME4', '122').
card_multiverse_id('goblin shrine'/'ME4', '202584').

card_in_set('goblin warrens', 'ME4').
card_original_type('goblin warrens'/'ME4', 'Enchantment').
card_original_text('goblin warrens'/'ME4', '{2}{R}, Sacrifice two Goblins: Put three 1/1 red Goblin creature tokens onto the battlefield.').
card_image_name('goblin warrens'/'ME4', 'goblin warrens').
card_uid('goblin warrens'/'ME4', 'ME4:Goblin Warrens:goblin warrens').
card_rarity('goblin warrens'/'ME4', 'Uncommon').
card_artist('goblin warrens'/'ME4', 'Dan Frazier').
card_number('goblin warrens'/'ME4', '123').
card_flavor_text('goblin warrens'/'ME4', '\"Goblins bred underground, their numbers hidden from the enemy until it was too late.\"\n—Sarpadian Empires, vol. IV').
card_multiverse_id('goblin warrens'/'ME4', '202479').

card_in_set('gorilla war cry', 'ME4').
card_original_type('gorilla war cry'/'ME4', 'Instant').
card_original_text('gorilla war cry'/'ME4', 'Cast Gorilla War Cry only during combat before blockers are declared.\nAttacking creatures can\'t be blocked this turn except by two or more creatures.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_image_name('gorilla war cry'/'ME4', 'gorilla war cry').
card_uid('gorilla war cry'/'ME4', 'ME4:Gorilla War Cry:gorilla war cry').
card_rarity('gorilla war cry'/'ME4', 'Common').
card_artist('gorilla war cry'/'ME4', 'Bryon Wackwitz').
card_number('gorilla war cry'/'ME4', '124').
card_multiverse_id('gorilla war cry'/'ME4', '202562').

card_in_set('grapeshot catapult', 'ME4').
card_original_type('grapeshot catapult'/'ME4', 'Artifact Creature — Construct').
card_original_text('grapeshot catapult'/'ME4', '{T}: Grapeshot Catapult deals 1 damage to target creature with flying.').
card_image_name('grapeshot catapult'/'ME4', 'grapeshot catapult').
card_uid('grapeshot catapult'/'ME4', 'ME4:Grapeshot Catapult:grapeshot catapult').
card_rarity('grapeshot catapult'/'ME4', 'Uncommon').
card_artist('grapeshot catapult'/'ME4', 'Dan Frazier').
card_number('grapeshot catapult'/'ME4', '204').
card_flavor_text('grapeshot catapult'/'ME4', 'For years scholars debated whether these were Urza\'s or Mishra\'s creations. Recent research suggests they were invented by the brothers\' original master, Tocasia, and that both used these devices.').
card_multiverse_id('grapeshot catapult'/'ME4', '221101').

card_in_set('gravebind', 'ME4').
card_original_type('gravebind'/'ME4', 'Instant').
card_original_text('gravebind'/'ME4', 'Target creature can\'t be regenerated this turn.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_image_name('gravebind'/'ME4', 'gravebind').
card_uid('gravebind'/'ME4', 'ME4:Gravebind:gravebind').
card_rarity('gravebind'/'ME4', 'Common').
card_artist('gravebind'/'ME4', 'Drew Tucker').
card_number('gravebind'/'ME4', '84').
card_multiverse_id('gravebind'/'ME4', '202597').

card_in_set('guardian beast', 'ME4').
card_original_type('guardian beast'/'ME4', 'Creature — Beast').
card_original_text('guardian beast'/'ME4', 'As long as Guardian Beast is untapped, noncreature artifacts you control can\'t be enchanted, they\'re indestructible, and other players can\'t gain control of them. This effect doesn\'t remove Auras already attached to those artifacts.').
card_image_name('guardian beast'/'ME4', 'guardian beast').
card_uid('guardian beast'/'ME4', 'ME4:Guardian Beast:guardian beast').
card_rarity('guardian beast'/'ME4', 'Rare').
card_artist('guardian beast'/'ME4', 'Ken Meyer, Jr.').
card_number('guardian beast'/'ME4', '85').
card_multiverse_id('guardian beast'/'ME4', '202539').

card_in_set('harsh justice', 'ME4').
card_original_type('harsh justice'/'ME4', 'Instant').
card_original_text('harsh justice'/'ME4', 'Cast Harsh Justice only during the declare attackers step and only if you\'ve been attacked this step.\nThis turn, whenever an attacking creature deals combat damage to you, it deals that much damage to its controller.').
card_image_name('harsh justice'/'ME4', 'harsh justice').
card_uid('harsh justice'/'ME4', 'ME4:Harsh Justice:harsh justice').
card_rarity('harsh justice'/'ME4', 'Rare').
card_artist('harsh justice'/'ME4', 'John Coulthart').
card_number('harsh justice'/'ME4', '13').
card_multiverse_id('harsh justice'/'ME4', '228267').

card_in_set('hasran ogress', 'ME4').
card_original_type('hasran ogress'/'ME4', 'Creature — Ogre').
card_original_text('hasran ogress'/'ME4', 'Whenever Hasran Ogress attacks, it deals 3 damage to you unless you pay {2}.').
card_image_name('hasran ogress'/'ME4', 'hasran ogress').
card_uid('hasran ogress'/'ME4', 'ME4:Hasran Ogress:hasran ogress').
card_rarity('hasran ogress'/'ME4', 'Common').
card_artist('hasran ogress'/'ME4', 'Dan Frazier').
card_number('hasran ogress'/'ME4', '86').
card_multiverse_id('hasran ogress'/'ME4', '202427').

card_in_set('healing salve', 'ME4').
card_original_type('healing salve'/'ME4', 'Instant').
card_original_text('healing salve'/'ME4', 'Choose one — Target player gains 3 life; or prevent the next 3 damage that would be dealt to target creature or player this turn.').
card_image_name('healing salve'/'ME4', 'healing salve').
card_uid('healing salve'/'ME4', 'ME4:Healing Salve:healing salve').
card_rarity('healing salve'/'ME4', 'Common').
card_artist('healing salve'/'ME4', 'Dan Frazier').
card_number('healing salve'/'ME4', '14').
card_multiverse_id('healing salve'/'ME4', '220959').

card_in_set('horn of deafening', 'ME4').
card_original_type('horn of deafening'/'ME4', 'Artifact').
card_original_text('horn of deafening'/'ME4', '{2}, {T}: Prevent all combat damage that would be dealt by target creature this turn.').
card_image_name('horn of deafening'/'ME4', 'horn of deafening').
card_uid('horn of deafening'/'ME4', 'ME4:Horn of Deafening:horn of deafening').
card_rarity('horn of deafening'/'ME4', 'Uncommon').
card_artist('horn of deafening'/'ME4', 'Dan Frazier').
card_number('horn of deafening'/'ME4', '205').
card_flavor_text('horn of deafening'/'ME4', '\"A blast, an echo . . . then silence.\"').
card_multiverse_id('horn of deafening'/'ME4', '202547').

card_in_set('howl from beyond', 'ME4').
card_original_type('howl from beyond'/'ME4', 'Instant').
card_original_text('howl from beyond'/'ME4', 'Target creature gets +X/+0 until end of turn.').
card_image_name('howl from beyond'/'ME4', 'howl from beyond').
card_uid('howl from beyond'/'ME4', 'ME4:Howl from Beyond:howl from beyond').
card_rarity('howl from beyond'/'ME4', 'Common').
card_artist('howl from beyond'/'ME4', 'Mark Poole').
card_number('howl from beyond'/'ME4', '87').
card_multiverse_id('howl from beyond'/'ME4', '202588').

card_in_set('ice cauldron', 'ME4').
card_original_type('ice cauldron'/'ME4', 'Artifact').
card_original_text('ice cauldron'/'ME4', '{X}, {T}: Put a charge counter on Ice Cauldron and exile a nonland card from your hand. You may cast that card for as long as it remains exiled. Note the type and amount of mana spent to pay this activation cost. Activate this ability only if there are no charge counters on Ice Cauldron.\n{T}, Remove a charge counter from Ice Cauldron: Add Ice Cauldron\'s last noted type and amount of mana to your mana pool. Spend this mana only to cast the last card exiled with Ice Cauldron.').
card_image_name('ice cauldron'/'ME4', 'ice cauldron').
card_uid('ice cauldron'/'ME4', 'ME4:Ice Cauldron:ice cauldron').
card_rarity('ice cauldron'/'ME4', 'Rare').
card_artist('ice cauldron'/'ME4', 'Dan Frazier').
card_number('ice cauldron'/'ME4', '206').
card_multiverse_id('ice cauldron'/'ME4', '202494').

card_in_set('icy manipulator', 'ME4').
card_original_type('icy manipulator'/'ME4', 'Artifact').
card_original_text('icy manipulator'/'ME4', '{1}, {T}: Tap target artifact, creature, or land.').
card_image_name('icy manipulator'/'ME4', 'icy manipulator').
card_uid('icy manipulator'/'ME4', 'ME4:Icy Manipulator:icy manipulator').
card_rarity('icy manipulator'/'ME4', 'Uncommon').
card_artist('icy manipulator'/'ME4', 'Douglas Shuler').
card_number('icy manipulator'/'ME4', '207').
card_multiverse_id('icy manipulator'/'ME4', '221108').

card_in_set('in the eye of chaos', 'ME4').
card_original_type('in the eye of chaos'/'ME4', 'World Enchantment').
card_original_text('in the eye of chaos'/'ME4', 'Whenever a player casts an instant spell, counter it unless that player pays {X}, where X is its converted mana cost.').
card_image_name('in the eye of chaos'/'ME4', 'in the eye of chaos').
card_uid('in the eye of chaos'/'ME4', 'ME4:In the Eye of Chaos:in the eye of chaos').
card_rarity('in the eye of chaos'/'ME4', 'Rare').
card_artist('in the eye of chaos'/'ME4', 'Brian Snõddy').
card_number('in the eye of chaos'/'ME4', '51').
card_multiverse_id('in the eye of chaos'/'ME4', '202410').

card_in_set('instill energy', 'ME4').
card_original_type('instill energy'/'ME4', 'Enchantment — Aura').
card_original_text('instill energy'/'ME4', 'Enchant creature\nEnchanted creature has haste.\n{0}: Untap enchanted creature. Activate this ability only during your turn and only once each turn.').
card_image_name('instill energy'/'ME4', 'instill energy').
card_uid('instill energy'/'ME4', 'ME4:Instill Energy:instill energy').
card_rarity('instill energy'/'ME4', 'Uncommon').
card_artist('instill energy'/'ME4', 'Dameon Willich').
card_number('instill energy'/'ME4', '157').
card_multiverse_id('instill energy'/'ME4', '202572').

card_in_set('ironhoof ox', 'ME4').
card_original_type('ironhoof ox'/'ME4', 'Creature — Ox').
card_original_text('ironhoof ox'/'ME4', 'Ironhoof Ox can\'t be blocked by more than one creature.').
card_image_name('ironhoof ox'/'ME4', 'ironhoof ox').
card_uid('ironhoof ox'/'ME4', 'ME4:Ironhoof Ox:ironhoof ox').
card_rarity('ironhoof ox'/'ME4', 'Common').
card_artist('ironhoof ox'/'ME4', 'Una Fricker').
card_number('ironhoof ox'/'ME4', '158').
card_flavor_text('ironhoof ox'/'ME4', 'The good news is it\'s vegetarian. The bad news is it just doesn\'t like you.').
card_multiverse_id('ironhoof ox'/'ME4', '221571').

card_in_set('island sanctuary', 'ME4').
card_original_type('island sanctuary'/'ME4', 'Enchantment').
card_original_text('island sanctuary'/'ME4', 'If you would draw a card during your draw step, instead you may skip that draw. If you do, until your next turn, you can\'t be attacked except by creatures with flying and/or islandwalk.').
card_image_name('island sanctuary'/'ME4', 'island sanctuary').
card_uid('island sanctuary'/'ME4', 'ME4:Island Sanctuary:island sanctuary').
card_rarity('island sanctuary'/'ME4', 'Rare').
card_artist('island sanctuary'/'ME4', 'Mark Poole').
card_number('island sanctuary'/'ME4', '15').
card_multiverse_id('island sanctuary'/'ME4', '202556').

card_in_set('jade monolith', 'ME4').
card_original_type('jade monolith'/'ME4', 'Artifact').
card_original_text('jade monolith'/'ME4', '{1}: The next time a source of your choice would deal damage to target creature this turn, that source deals that damage to you instead.').
card_image_name('jade monolith'/'ME4', 'jade monolith').
card_uid('jade monolith'/'ME4', 'ME4:Jade Monolith:jade monolith').
card_rarity('jade monolith'/'ME4', 'Rare').
card_artist('jade monolith'/'ME4', 'Anson Maddocks').
card_number('jade monolith'/'ME4', '208').
card_multiverse_id('jade monolith'/'ME4', '202564').

card_in_set('juggernaut', 'ME4').
card_original_type('juggernaut'/'ME4', 'Artifact Creature — Juggernaut').
card_original_text('juggernaut'/'ME4', 'Juggernaut attacks each turn if able.\nJuggernaut can\'t be blocked by Walls.').
card_image_name('juggernaut'/'ME4', 'juggernaut').
card_uid('juggernaut'/'ME4', 'ME4:Juggernaut:juggernaut').
card_rarity('juggernaut'/'ME4', 'Uncommon').
card_artist('juggernaut'/'ME4', 'Dan Frazier').
card_number('juggernaut'/'ME4', '209').
card_flavor_text('juggernaut'/'ME4', 'We had taken refuge in a small cave, thinking the entrance was too narrow for it to follow. To our horror, its gigantic head smashed into the mountainside, ripping itself a new entrance.').
card_multiverse_id('juggernaut'/'ME4', '221109').

card_in_set('junún efreet', 'ME4').
card_original_type('junún efreet'/'ME4', 'Creature — Efreet').
card_original_text('junún efreet'/'ME4', 'Flying\nAt the beginning of your upkeep, sacrifice Junún Efreet unless you pay {B}{B}.').
card_image_name('junún efreet'/'ME4', 'junun efreet').
card_uid('junún efreet'/'ME4', 'ME4:Junún Efreet:junun efreet').
card_rarity('junún efreet'/'ME4', 'Uncommon').
card_artist('junún efreet'/'ME4', 'Christopher Rush').
card_number('junún efreet'/'ME4', '88').
card_multiverse_id('junún efreet'/'ME4', '202605').

card_in_set('just fate', 'ME4').
card_original_type('just fate'/'ME4', 'Instant').
card_original_text('just fate'/'ME4', 'Cast Just Fate only during the declare attackers step and only if you\'ve been attacked this step.\nDestroy target attacking creature.').
card_image_name('just fate'/'ME4', 'just fate').
card_uid('just fate'/'ME4', 'ME4:Just Fate:just fate').
card_rarity('just fate'/'ME4', 'Common').
card_artist('just fate'/'ME4', 'Bradley Williams').
card_number('just fate'/'ME4', '16').
card_multiverse_id('just fate'/'ME4', '202545').

card_in_set('kismet', 'ME4').
card_original_type('kismet'/'ME4', 'Enchantment').
card_original_text('kismet'/'ME4', 'Artifacts, creatures, and lands played by your opponents enter the battlefield tapped.').
card_image_name('kismet'/'ME4', 'kismet').
card_uid('kismet'/'ME4', 'ME4:Kismet:kismet').
card_rarity('kismet'/'ME4', 'Rare').
card_artist('kismet'/'ME4', 'Kaja Foglio').
card_number('kismet'/'ME4', '17').
card_flavor_text('kismet'/'ME4', '\"Make people wait for what they want, and you have power over them. This is as true for merchants and militia as it is for cooks and couples.\"\n—Gwendlyn Di Corci').
card_multiverse_id('kismet'/'ME4', '202434').

card_in_set('kormus bell', 'ME4').
card_original_type('kormus bell'/'ME4', 'Artifact').
card_original_text('kormus bell'/'ME4', 'All Swamps are 1/1 creatures that are still lands.').
card_image_name('kormus bell'/'ME4', 'kormus bell').
card_uid('kormus bell'/'ME4', 'ME4:Kormus Bell:kormus bell').
card_rarity('kormus bell'/'ME4', 'Rare').
card_artist('kormus bell'/'ME4', 'Christopher Rush').
card_number('kormus bell'/'ME4', '210').
card_multiverse_id('kormus bell'/'ME4', '202499').

card_in_set('kudzu', 'ME4').
card_original_type('kudzu'/'ME4', 'Enchantment — Aura').
card_original_text('kudzu'/'ME4', 'Enchant land\nWhen enchanted land becomes tapped, destroy it. That land\'s controller attaches Kudzu to a land of his or her choice.').
card_image_name('kudzu'/'ME4', 'kudzu').
card_uid('kudzu'/'ME4', 'ME4:Kudzu:kudzu').
card_rarity('kudzu'/'ME4', 'Uncommon').
card_artist('kudzu'/'ME4', 'Mark Poole').
card_number('kudzu'/'ME4', '159').
card_multiverse_id('kudzu'/'ME4', '202601').

card_in_set('last chance', 'ME4').
card_original_type('last chance'/'ME4', 'Sorcery').
card_original_text('last chance'/'ME4', 'Take an extra turn after this one. At the beginning of that turn\'s end step, you lose the game.').
card_image_name('last chance'/'ME4', 'last chance').
card_uid('last chance'/'ME4', 'ME4:Last Chance:last chance').
card_rarity('last chance'/'ME4', 'Rare').
card_artist('last chance'/'ME4', 'Hannibal King').
card_number('last chance'/'ME4', '125').
card_multiverse_id('last chance'/'ME4', '233303').

card_in_set('lava flow', 'ME4').
card_original_type('lava flow'/'ME4', 'Sorcery').
card_original_text('lava flow'/'ME4', 'Destroy target creature or land.').
card_image_name('lava flow'/'ME4', 'lava flow').
card_uid('lava flow'/'ME4', 'ME4:Lava Flow:lava flow').
card_rarity('lava flow'/'ME4', 'Common').
card_artist('lava flow'/'ME4', 'Mike Dringenberg').
card_number('lava flow'/'ME4', '126').
card_multiverse_id('lava flow'/'ME4', '202614').

card_in_set('leeches', 'ME4').
card_original_type('leeches'/'ME4', 'Sorcery').
card_original_text('leeches'/'ME4', 'Target player loses all poison counters. Leeches deals that much damage to that player.').
card_image_name('leeches'/'ME4', 'leeches').
card_uid('leeches'/'ME4', 'ME4:Leeches:leeches').
card_rarity('leeches'/'ME4', 'Rare').
card_artist('leeches'/'ME4', 'Alan Rabinowitz').
card_number('leeches'/'ME4', '18').
card_flavor_text('leeches'/'ME4', '\"Where our potions and powders fail, perhaps nature will succeed.\"\n—Reyhan, Samite Alchemist').
card_multiverse_id('leeches'/'ME4', '233298').

card_in_set('library of alexandria', 'ME4').
card_original_type('library of alexandria'/'ME4', 'Land').
card_original_text('library of alexandria'/'ME4', '{T}: Add {1} to your mana pool.\n{T}: Draw a card. Activate this ability only if you have exactly seven cards in hand.').
card_image_name('library of alexandria'/'ME4', 'library of alexandria').
card_uid('library of alexandria'/'ME4', 'ME4:Library of Alexandria:library of alexandria').
card_rarity('library of alexandria'/'ME4', 'Rare').
card_artist('library of alexandria'/'ME4', 'Mark Poole').
card_number('library of alexandria'/'ME4', '245').
card_multiverse_id('library of alexandria'/'ME4', '202429').

card_in_set('library of leng', 'ME4').
card_original_type('library of leng'/'ME4', 'Artifact').
card_original_text('library of leng'/'ME4', 'You have no maximum hand size.\nIf an effect causes you to discard a card, discard it, but you may put it on top of your library instead of into your graveyard.').
card_image_name('library of leng'/'ME4', 'library of leng').
card_uid('library of leng'/'ME4', 'ME4:Library of Leng:library of leng').
card_rarity('library of leng'/'ME4', 'Common').
card_artist('library of leng'/'ME4', 'Daniel Gelon').
card_number('library of leng'/'ME4', '211').
card_multiverse_id('library of leng'/'ME4', '202455').

card_in_set('lich', 'ME4').
card_original_type('lich'/'ME4', 'Enchantment').
card_original_text('lich'/'ME4', 'As Lich enters the battlefield, you lose life equal to your life total.\nYou don\'t lose the game for having 0 or less life.\nIf you would gain life, draw that many cards instead.\nWhenever you\'re dealt damage, sacrifice that many nontoken permanents. If you can\'t, you lose the game.\nWhen Lich is put into a graveyard from the battlefield, you lose the game.').
card_image_name('lich'/'ME4', 'lich').
card_uid('lich'/'ME4', 'ME4:Lich:lich').
card_rarity('lich'/'ME4', 'Rare').
card_artist('lich'/'ME4', 'Daniel Gelon').
card_number('lich'/'ME4', '89').
card_multiverse_id('lich'/'ME4', '202483').

card_in_set('lifeforce', 'ME4').
card_original_type('lifeforce'/'ME4', 'Enchantment').
card_original_text('lifeforce'/'ME4', '{G}{G}: Counter target black spell.').
card_image_name('lifeforce'/'ME4', 'lifeforce').
card_uid('lifeforce'/'ME4', 'ME4:Lifeforce:lifeforce').
card_rarity('lifeforce'/'ME4', 'Rare').
card_artist('lifeforce'/'ME4', 'Dameon Willich').
card_number('lifeforce'/'ME4', '160').
card_multiverse_id('lifeforce'/'ME4', '202425').

card_in_set('lim-dûl\'s cohort', 'ME4').
card_original_type('lim-dûl\'s cohort'/'ME4', 'Creature — Zombie').
card_original_text('lim-dûl\'s cohort'/'ME4', 'Whenever Lim-Dûl\'s Cohort blocks or becomes blocked by a creature, that creature can\'t be regenerated this turn.').
card_image_name('lim-dûl\'s cohort'/'ME4', 'lim-dul\'s cohort').
card_uid('lim-dûl\'s cohort'/'ME4', 'ME4:Lim-Dûl\'s Cohort:lim-dul\'s cohort').
card_rarity('lim-dûl\'s cohort'/'ME4', 'Common').
card_artist('lim-dûl\'s cohort'/'ME4', 'Douglas Shuler').
card_number('lim-dûl\'s cohort'/'ME4', '90').
card_multiverse_id('lim-dûl\'s cohort'/'ME4', '202443').

card_in_set('living lands', 'ME4').
card_original_type('living lands'/'ME4', 'Enchantment').
card_original_text('living lands'/'ME4', 'All Forests are 1/1 creatures that are still lands.').
card_image_name('living lands'/'ME4', 'living lands').
card_uid('living lands'/'ME4', 'ME4:Living Lands:living lands').
card_rarity('living lands'/'ME4', 'Rare').
card_artist('living lands'/'ME4', 'Jesper Myrfors').
card_number('living lands'/'ME4', '161').
card_multiverse_id('living lands'/'ME4', '233305').

card_in_set('living wall', 'ME4').
card_original_type('living wall'/'ME4', 'Artifact Creature — Wall').
card_original_text('living wall'/'ME4', 'Defender (This creature can\'t attack.)\n{1}: Regenerate Living Wall.').
card_image_name('living wall'/'ME4', 'living wall').
card_uid('living wall'/'ME4', 'ME4:Living Wall:living wall').
card_rarity('living wall'/'ME4', 'Uncommon').
card_artist('living wall'/'ME4', 'Anson Maddocks').
card_number('living wall'/'ME4', '212').
card_flavor_text('living wall'/'ME4', 'Some fiendish mage had created a horrifying wall of living flesh, patched together from a jumble of still-recognizable body parts. As we sought to hew our way through it, some unknown power healed the gaping wounds we cut, denying us passage.').
card_multiverse_id('living wall'/'ME4', '202418').

card_in_set('mahamoti djinn', 'ME4').
card_original_type('mahamoti djinn'/'ME4', 'Creature — Djinn').
card_original_text('mahamoti djinn'/'ME4', 'Flying').
card_image_name('mahamoti djinn'/'ME4', 'mahamoti djinn').
card_uid('mahamoti djinn'/'ME4', 'ME4:Mahamoti Djinn:mahamoti djinn').
card_rarity('mahamoti djinn'/'ME4', 'Rare').
card_artist('mahamoti djinn'/'ME4', 'Dan Frazier').
card_number('mahamoti djinn'/'ME4', '52').
card_flavor_text('mahamoti djinn'/'ME4', 'Of royal blood amongst the spirits of the air, the Mahamoti Djinn rides on the wings of the winds. As dangerous in the gambling hall as he is in battle, he is a master of trickery and misdirection.').
card_multiverse_id('mahamoti djinn'/'ME4', '228263').

card_in_set('mana matrix', 'ME4').
card_original_type('mana matrix'/'ME4', 'Artifact').
card_original_text('mana matrix'/'ME4', 'Instant and enchantment spells you cast cost up to {2} less to cast.').
card_image_name('mana matrix'/'ME4', 'mana matrix').
card_uid('mana matrix'/'ME4', 'ME4:Mana Matrix:mana matrix').
card_rarity('mana matrix'/'ME4', 'Rare').
card_artist('mana matrix'/'ME4', 'Mark Tedin').
card_number('mana matrix'/'ME4', '213').
card_multiverse_id('mana matrix'/'ME4', '228271').

card_in_set('mana vault', 'ME4').
card_original_type('mana vault'/'ME4', 'Artifact').
card_original_text('mana vault'/'ME4', 'Mana Vault doesn\'t untap during your untap step.\nAt the beginning of your upkeep, you may pay {4}. If you do, untap Mana Vault.\nAt the beginning of your draw step, if Mana Vault is tapped, it deals 1 damage to you.\n{T}: Add {3} to your mana pool.').
card_image_name('mana vault'/'ME4', 'mana vault').
card_uid('mana vault'/'ME4', 'ME4:Mana Vault:mana vault').
card_rarity('mana vault'/'ME4', 'Rare').
card_artist('mana vault'/'ME4', 'Mark Tedin').
card_number('mana vault'/'ME4', '214').
card_multiverse_id('mana vault'/'ME4', '202538').

card_in_set('martyr\'s cry', 'ME4').
card_original_type('martyr\'s cry'/'ME4', 'Sorcery').
card_original_text('martyr\'s cry'/'ME4', 'Exile all white creatures. For each creature exiled this way, its controller draws a card.').
card_image_name('martyr\'s cry'/'ME4', 'martyr\'s cry').
card_uid('martyr\'s cry'/'ME4', 'ME4:Martyr\'s Cry:martyr\'s cry').
card_rarity('martyr\'s cry'/'ME4', 'Rare').
card_artist('martyr\'s cry'/'ME4', 'Jeff A. Menges').
card_number('martyr\'s cry'/'ME4', '19').
card_flavor_text('martyr\'s cry'/'ME4', '\"It is only fitting that one such as I should die in pursuit of knowledge.\"\n—Vervamon the Elder').
card_multiverse_id('martyr\'s cry'/'ME4', '233297').

card_in_set('martyrs of korlis', 'ME4').
card_original_type('martyrs of korlis'/'ME4', 'Creature — Human').
card_original_text('martyrs of korlis'/'ME4', 'As long as Martyrs of Korlis is untapped, all damage that would be dealt to you by artifacts is dealt to Martyrs of Korlis instead.').
card_image_name('martyrs of korlis'/'ME4', 'martyrs of korlis').
card_uid('martyrs of korlis'/'ME4', 'ME4:Martyrs of Korlis:martyrs of korlis').
card_rarity('martyrs of korlis'/'ME4', 'Uncommon').
card_artist('martyrs of korlis'/'ME4', 'Margaret Organ-Kean').
card_number('martyrs of korlis'/'ME4', '20').
card_multiverse_id('martyrs of korlis'/'ME4', '220946').

card_in_set('maze of ith', 'ME4').
card_original_type('maze of ith'/'ME4', 'Land').
card_original_text('maze of ith'/'ME4', '{T}: Untap target attacking creature. Prevent all combat damage that would be dealt to and dealt by that creature this turn.').
card_image_name('maze of ith'/'ME4', 'maze of ith').
card_uid('maze of ith'/'ME4', 'ME4:Maze of Ith:maze of ith').
card_rarity('maze of ith'/'ME4', 'Rare').
card_artist('maze of ith'/'ME4', 'Anson Maddocks').
card_number('maze of ith'/'ME4', '246').
card_multiverse_id('maze of ith'/'ME4', '201263').

card_in_set('mightstone', 'ME4').
card_original_type('mightstone'/'ME4', 'Artifact').
card_original_text('mightstone'/'ME4', 'Attacking creatures get +1/+0.').
card_image_name('mightstone'/'ME4', 'mightstone').
card_uid('mightstone'/'ME4', 'ME4:Mightstone:mightstone').
card_rarity('mightstone'/'ME4', 'Common').
card_artist('mightstone'/'ME4', 'Pete Venters').
card_number('mightstone'/'ME4', '215').
card_flavor_text('mightstone'/'ME4', 'While exploring the sacred cave of Koilos with his brother Mishra and their master Tocasia, Urza fell behind in the Hall of Tagsin, where he discovered the remarkable Mightstone.').
card_multiverse_id('mightstone'/'ME4', '202453').

card_in_set('mijae djinn', 'ME4').
card_original_type('mijae djinn'/'ME4', 'Creature — Djinn').
card_original_text('mijae djinn'/'ME4', 'Whenever Mijae Djinn attacks, flip a coin. If you lose the flip, remove Mijae Djinn from combat and tap it.').
card_image_name('mijae djinn'/'ME4', 'mijae djinn').
card_uid('mijae djinn'/'ME4', 'ME4:Mijae Djinn:mijae djinn').
card_rarity('mijae djinn'/'ME4', 'Rare').
card_artist('mijae djinn'/'ME4', 'Susan Van Camp').
card_number('mijae djinn'/'ME4', '127').
card_multiverse_id('mijae djinn'/'ME4', '202503').

card_in_set('minion of tevesh szat', 'ME4').
card_original_type('minion of tevesh szat'/'ME4', 'Creature — Demon Minion').
card_original_text('minion of tevesh szat'/'ME4', 'At the beginning of your upkeep, Minion of Tevesh Szat deals 2 damage to you unless you pay {B}{B}.\n{T}: Target creature gets +3/-2 until end of turn.').
card_image_name('minion of tevesh szat'/'ME4', 'minion of tevesh szat').
card_uid('minion of tevesh szat'/'ME4', 'ME4:Minion of Tevesh Szat:minion of tevesh szat').
card_rarity('minion of tevesh szat'/'ME4', 'Rare').
card_artist('minion of tevesh szat'/'ME4', 'Julie Baroh').
card_number('minion of tevesh szat'/'ME4', '91').
card_flavor_text('minion of tevesh szat'/'ME4', '\"A minion given over to Tevesh Szat is a stronger minion gained.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('minion of tevesh szat'/'ME4', '233302').

card_in_set('mishra\'s workshop', 'ME4').
card_original_type('mishra\'s workshop'/'ME4', 'Land').
card_original_text('mishra\'s workshop'/'ME4', '{T}: Add {3} to your mana pool. Spend this mana only to cast artifact spells.').
card_image_name('mishra\'s workshop'/'ME4', 'mishra\'s workshop').
card_uid('mishra\'s workshop'/'ME4', 'ME4:Mishra\'s Workshop:mishra\'s workshop').
card_rarity('mishra\'s workshop'/'ME4', 'Rare').
card_artist('mishra\'s workshop'/'ME4', 'Kaja Foglio').
card_number('mishra\'s workshop'/'ME4', '247').
card_flavor_text('mishra\'s workshop'/'ME4', 'Though he eventually came to despise Tocasia, Mishra listened well to her lessons on clarity of purpose. Unlike his brother, he focused his mind on a single goal.').
card_multiverse_id('mishra\'s workshop'/'ME4', '202578').

card_in_set('mystic decree', 'ME4').
card_original_type('mystic decree'/'ME4', 'World Enchantment').
card_original_text('mystic decree'/'ME4', 'All creatures lose flying and islandwalk.').
card_image_name('mystic decree'/'ME4', 'mystic decree').
card_uid('mystic decree'/'ME4', 'ME4:Mystic Decree:mystic decree').
card_rarity('mystic decree'/'ME4', 'Uncommon').
card_artist('mystic decree'/'ME4', 'Liz Danforth').
card_number('mystic decree'/'ME4', '53').
card_flavor_text('mystic decree'/'ME4', '\"Curse Reveka, and curse her coddled conjurers. Their sorcerers\' school shall yet be ours.\"\n—Irini Sengir').
card_multiverse_id('mystic decree'/'ME4', '202518').

card_in_set('naked singularity', 'ME4').
card_original_type('naked singularity'/'ME4', 'Artifact').
card_original_text('naked singularity'/'ME4', 'Cumulative upkeep {3} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nIf tapped for mana, Plains produce {R}, Islands produce {G}, Swamps produce {W}, Mountains produce {U}, and Forests produce {B} instead of any other type.').
card_image_name('naked singularity'/'ME4', 'naked singularity').
card_uid('naked singularity'/'ME4', 'ME4:Naked Singularity:naked singularity').
card_rarity('naked singularity'/'ME4', 'Rare').
card_artist('naked singularity'/'ME4', 'Mark Tedin').
card_number('naked singularity'/'ME4', '216').
card_multiverse_id('naked singularity'/'ME4', '202412').

card_in_set('oasis', 'ME4').
card_original_type('oasis'/'ME4', 'Land').
card_original_text('oasis'/'ME4', '{T}: Prevent the next 1 damage that would be dealt to target creature this turn.').
card_image_name('oasis'/'ME4', 'oasis').
card_uid('oasis'/'ME4', 'ME4:Oasis:oasis').
card_rarity('oasis'/'ME4', 'Common').
card_artist('oasis'/'ME4', 'Brian Snõddy').
card_number('oasis'/'ME4', '248').
card_multiverse_id('oasis'/'ME4', '202521').

card_in_set('obelisk of undoing', 'ME4').
card_original_type('obelisk of undoing'/'ME4', 'Artifact').
card_original_text('obelisk of undoing'/'ME4', '{6}, {T}: Return target permanent you own and control to your hand.').
card_image_name('obelisk of undoing'/'ME4', 'obelisk of undoing').
card_uid('obelisk of undoing'/'ME4', 'ME4:Obelisk of Undoing:obelisk of undoing').
card_rarity('obelisk of undoing'/'ME4', 'Rare').
card_artist('obelisk of undoing'/'ME4', 'Tom Wänerstrand').
card_number('obelisk of undoing'/'ME4', '217').
card_flavor_text('obelisk of undoing'/'ME4', 'The Battle of Tomakul taught Urza not to rely on fickle reinforcements.').
card_multiverse_id('obelisk of undoing'/'ME4', '202603').

card_in_set('obsianus golem', 'ME4').
card_original_type('obsianus golem'/'ME4', 'Artifact Creature — Golem').
card_original_text('obsianus golem'/'ME4', '').
card_image_name('obsianus golem'/'ME4', 'obsianus golem').
card_uid('obsianus golem'/'ME4', 'ME4:Obsianus Golem:obsianus golem').
card_rarity('obsianus golem'/'ME4', 'Common').
card_artist('obsianus golem'/'ME4', 'Jesper Myrfors').
card_number('obsianus golem'/'ME4', '218').
card_flavor_text('obsianus golem'/'ME4', '\"The foot stone is connected to the ankle stone, the ankle stone is connected to the leg stone . . .\"\n—\"Song of the Artificer\"').
card_multiverse_id('obsianus golem'/'ME4', '202624').

card_in_set('ogre taskmaster', 'ME4').
card_original_type('ogre taskmaster'/'ME4', 'Creature — Ogre').
card_original_text('ogre taskmaster'/'ME4', 'Ogre Taskmaster can\'t block.').
card_image_name('ogre taskmaster'/'ME4', 'ogre taskmaster').
card_uid('ogre taskmaster'/'ME4', 'ME4:Ogre Taskmaster:ogre taskmaster').
card_rarity('ogre taskmaster'/'ME4', 'Common').
card_artist('ogre taskmaster'/'ME4', 'Dan Frazier').
card_number('ogre taskmaster'/'ME4', '128').
card_flavor_text('ogre taskmaster'/'ME4', 'Three little goblins, enjoying their brew. One bumped an ogre, and then there were two.').
card_multiverse_id('ogre taskmaster'/'ME4', '220965').

card_in_set('onulet', 'ME4').
card_original_type('onulet'/'ME4', 'Artifact Creature — Construct').
card_original_text('onulet'/'ME4', 'When Onulet is put into a graveyard from the battlefield, you gain 2 life.').
card_image_name('onulet'/'ME4', 'onulet').
card_uid('onulet'/'ME4', 'ME4:Onulet:onulet').
card_rarity('onulet'/'ME4', 'Common').
card_artist('onulet'/'ME4', 'Anson Maddocks').
card_number('onulet'/'ME4', '219').
card_flavor_text('onulet'/'ME4', 'An early inspiration for Urza, Tocasia\'s Onulets contained magical essences that could be cannibalized after they stopped functioning.').
card_multiverse_id('onulet'/'ME4', '221104').

card_in_set('orcish mechanics', 'ME4').
card_original_type('orcish mechanics'/'ME4', 'Creature — Orc').
card_original_text('orcish mechanics'/'ME4', '{T}, Sacrifice an artifact: Orcish Mechanics deals 2 damage to target creature or player.').
card_image_name('orcish mechanics'/'ME4', 'orcish mechanics').
card_uid('orcish mechanics'/'ME4', 'ME4:Orcish Mechanics:orcish mechanics').
card_rarity('orcish mechanics'/'ME4', 'Uncommon').
card_artist('orcish mechanics'/'ME4', 'Pete Venters').
card_number('orcish mechanics'/'ME4', '129').
card_multiverse_id('orcish mechanics'/'ME4', '221110').

card_in_set('osai vultures', 'ME4').
card_original_type('osai vultures'/'ME4', 'Creature — Bird').
card_original_text('osai vultures'/'ME4', 'Flying\nAt the beginning of each end step, if a creature was put into a graveyard from the battlefield this turn, put a carrion counter on Osai Vultures.\nRemove two carrion counters from Osai Vultures: Osai Vultures gets +1/+1 until end of turn.').
card_image_name('osai vultures'/'ME4', 'osai vultures').
card_uid('osai vultures'/'ME4', 'ME4:Osai Vultures:osai vultures').
card_rarity('osai vultures'/'ME4', 'Common').
card_artist('osai vultures'/'ME4', 'Dan Frazier').
card_number('osai vultures'/'ME4', '21').
card_multiverse_id('osai vultures'/'ME4', '202436').

card_in_set('overwhelming forces', 'ME4').
card_original_type('overwhelming forces'/'ME4', 'Sorcery').
card_original_text('overwhelming forces'/'ME4', 'Destroy all creatures target opponent controls. Draw a card for each creature destroyed this way.').
card_image_name('overwhelming forces'/'ME4', 'overwhelming forces').
card_uid('overwhelming forces'/'ME4', 'ME4:Overwhelming Forces:overwhelming forces').
card_rarity('overwhelming forces'/'ME4', 'Rare').
card_artist('overwhelming forces'/'ME4', 'Gao Yan').
card_number('overwhelming forces'/'ME4', '92').
card_multiverse_id('overwhelming forces'/'ME4', '202488').

card_in_set('owl familiar', 'ME4').
card_original_type('owl familiar'/'ME4', 'Creature — Bird').
card_original_text('owl familiar'/'ME4', 'Flying\nWhen Owl Familiar enters the battlefield, draw a card, then discard a card.').
card_image_name('owl familiar'/'ME4', 'owl familiar').
card_uid('owl familiar'/'ME4', 'ME4:Owl Familiar:owl familiar').
card_rarity('owl familiar'/'ME4', 'Common').
card_artist('owl familiar'/'ME4', 'Janine Johnston').
card_number('owl familiar'/'ME4', '54').
card_multiverse_id('owl familiar'/'ME4', '202498').

card_in_set('pentagram of the ages', 'ME4').
card_original_type('pentagram of the ages'/'ME4', 'Artifact').
card_original_text('pentagram of the ages'/'ME4', '{4}, {T}: The next time a source of your choice would deal damage to you this turn, prevent that damage.').
card_image_name('pentagram of the ages'/'ME4', 'pentagram of the ages').
card_uid('pentagram of the ages'/'ME4', 'ME4:Pentagram of the Ages:pentagram of the ages').
card_rarity('pentagram of the ages'/'ME4', 'Uncommon').
card_artist('pentagram of the ages'/'ME4', 'Douglas Shuler').
card_number('pentagram of the ages'/'ME4', '220').
card_flavor_text('pentagram of the ages'/'ME4', '\"Take this item, for instance. How would it destroy us, Relicbane?\" —Arcum Dagsson, Soldevi Machinist').
card_multiverse_id('pentagram of the ages'/'ME4', '202509').

card_in_set('personal incarnation', 'ME4').
card_original_type('personal incarnation'/'ME4', 'Creature — Avatar Incarnation').
card_original_text('personal incarnation'/'ME4', '{0}: The next 1 damage that would be dealt to Personal Incarnation this turn is dealt to its owner instead. Any player may activate this ability, but only if he or she owns Personal Incarnation.\nWhen Personal Incarnation is put into a graveyard from the battlefield, its owner loses half his or her life, rounded up.').
card_image_name('personal incarnation'/'ME4', 'personal incarnation').
card_uid('personal incarnation'/'ME4', 'ME4:Personal Incarnation:personal incarnation').
card_rarity('personal incarnation'/'ME4', 'Rare').
card_artist('personal incarnation'/'ME4', 'Kev Brockschmidt').
card_number('personal incarnation'/'ME4', '22').
card_multiverse_id('personal incarnation'/'ME4', '202489').

card_in_set('phantasmal forces', 'ME4').
card_original_type('phantasmal forces'/'ME4', 'Creature — Illusion').
card_original_text('phantasmal forces'/'ME4', 'Flying\nAt the beginning of your upkeep, sacrifice Phantasmal Forces unless you pay {U}.').
card_image_name('phantasmal forces'/'ME4', 'phantasmal forces').
card_uid('phantasmal forces'/'ME4', 'ME4:Phantasmal Forces:phantasmal forces').
card_rarity('phantasmal forces'/'ME4', 'Common').
card_artist('phantasmal forces'/'ME4', 'Mark Poole').
card_number('phantasmal forces'/'ME4', '55').
card_flavor_text('phantasmal forces'/'ME4', 'These beings embody the essence of true heroes long dead. Summoned from the dreamrealms, they rise to meet their enemies.').
card_multiverse_id('phantasmal forces'/'ME4', '202458').

card_in_set('phantasmal terrain', 'ME4').
card_original_type('phantasmal terrain'/'ME4', 'Enchantment — Aura').
card_original_text('phantasmal terrain'/'ME4', 'Enchant land\nAs Phantasmal Terrain enters the battlefield, choose a basic land type.\nEnchanted land is the chosen type.').
card_image_name('phantasmal terrain'/'ME4', 'phantasmal terrain').
card_uid('phantasmal terrain'/'ME4', 'ME4:Phantasmal Terrain:phantasmal terrain').
card_rarity('phantasmal terrain'/'ME4', 'Common').
card_artist('phantasmal terrain'/'ME4', 'Dameon Willich').
card_number('phantasmal terrain'/'ME4', '56').
card_multiverse_id('phantasmal terrain'/'ME4', '221553').

card_in_set('planar gate', 'ME4').
card_original_type('planar gate'/'ME4', 'Artifact').
card_original_text('planar gate'/'ME4', 'Creature spells you cast cost up to {2} less to cast.').
card_image_name('planar gate'/'ME4', 'planar gate').
card_uid('planar gate'/'ME4', 'ME4:Planar Gate:planar gate').
card_rarity('planar gate'/'ME4', 'Uncommon').
card_artist('planar gate'/'ME4', 'Melissa A. Benson').
card_number('planar gate'/'ME4', '221').
card_flavor_text('planar gate'/'ME4', 'Nireya reached through the Gate, sensing the energies trapped beyond.').
card_multiverse_id('planar gate'/'ME4', '202613').

card_in_set('plateau', 'ME4').
card_original_type('plateau'/'ME4', 'Land — Mountain Plains').
card_original_text('plateau'/'ME4', '').
card_image_name('plateau'/'ME4', 'plateau').
card_uid('plateau'/'ME4', 'ME4:Plateau:plateau').
card_rarity('plateau'/'ME4', 'Rare').
card_artist('plateau'/'ME4', 'Cornelius Brudi').
card_number('plateau'/'ME4', '249').
card_multiverse_id('plateau'/'ME4', '202612').

card_in_set('power artifact', 'ME4').
card_original_type('power artifact'/'ME4', 'Enchantment — Aura').
card_original_text('power artifact'/'ME4', 'Enchant artifact\nEnchanted artifact\'s activated abilities cost {2} less to activate. This effect can\'t reduce the amount of mana an ability costs to activate to less than one mana.').
card_image_name('power artifact'/'ME4', 'power artifact').
card_uid('power artifact'/'ME4', 'ME4:Power Artifact:power artifact').
card_rarity('power artifact'/'ME4', 'Rare').
card_artist('power artifact'/'ME4', 'Douglas Shuler').
card_number('power artifact'/'ME4', '57').
card_multiverse_id('power artifact'/'ME4', '202566').

card_in_set('primal clay', 'ME4').
card_original_type('primal clay'/'ME4', 'Artifact Creature — Shapeshifter').
card_original_text('primal clay'/'ME4', 'As Primal Clay enters the battlefield, it becomes your choice of a 3/3 artifact creature, a 2/2 artifact creature with flying, or a 1/6 Shapeshifter Wall artifact creature with defender.').
card_image_name('primal clay'/'ME4', 'primal clay').
card_uid('primal clay'/'ME4', 'ME4:Primal Clay:primal clay').
card_rarity('primal clay'/'ME4', 'Common').
card_artist('primal clay'/'ME4', 'Kaja Foglio').
card_number('primal clay'/'ME4', '222').
card_multiverse_id('primal clay'/'ME4', '202576').

card_in_set('primitive justice', 'ME4').
card_original_type('primitive justice'/'ME4', 'Sorcery').
card_original_text('primitive justice'/'ME4', 'As an additional cost to cast Primitive Justice, you may pay {1}{R} and/or {1}{G} any number of times.\nDestroy target artifact. For each additional {1}{R} you paid, destroy another target artifact. For each additional {1}{G} you paid, destroy another target artifact, and you gain 1 life.').
card_image_name('primitive justice'/'ME4', 'primitive justice').
card_uid('primitive justice'/'ME4', 'ME4:Primitive Justice:primitive justice').
card_rarity('primitive justice'/'ME4', 'Uncommon').
card_artist('primitive justice'/'ME4', 'Anthony S. Waters').
card_number('primitive justice'/'ME4', '130').
card_multiverse_id('primitive justice'/'ME4', '202491').

card_in_set('prodigal sorcerer', 'ME4').
card_original_type('prodigal sorcerer'/'ME4', 'Creature — Human Wizard').
card_original_text('prodigal sorcerer'/'ME4', '{T}: Prodigal Sorcerer deals 1 damage to target creature or player.').
card_image_name('prodigal sorcerer'/'ME4', 'prodigal sorcerer').
card_uid('prodigal sorcerer'/'ME4', 'ME4:Prodigal Sorcerer:prodigal sorcerer').
card_rarity('prodigal sorcerer'/'ME4', 'Uncommon').
card_artist('prodigal sorcerer'/'ME4', 'Douglas Shuler').
card_number('prodigal sorcerer'/'ME4', '58').
card_flavor_text('prodigal sorcerer'/'ME4', 'Occasionally a member of the Institute of Arcane Study acquires a taste for worldly pleasures. Seldom do they have trouble finding employment.').
card_multiverse_id('prodigal sorcerer'/'ME4', '221111').

card_in_set('prowling nightstalker', 'ME4').
card_original_type('prowling nightstalker'/'ME4', 'Creature — Nightstalker').
card_original_text('prowling nightstalker'/'ME4', 'Prowling Nightstalker can\'t be blocked except by black creatures.').
card_image_name('prowling nightstalker'/'ME4', 'prowling nightstalker').
card_uid('prowling nightstalker'/'ME4', 'ME4:Prowling Nightstalker:prowling nightstalker').
card_rarity('prowling nightstalker'/'ME4', 'Common').
card_artist('prowling nightstalker'/'ME4', 'Keith Parkinson').
card_number('prowling nightstalker'/'ME4', '93').
card_flavor_text('prowling nightstalker'/'ME4', 'Silent as a serpent, twisted as a lone bog tree, evil as Tojira\'s heart.').
card_multiverse_id('prowling nightstalker'/'ME4', '221915').

card_in_set('radjan spirit', 'ME4').
card_original_type('radjan spirit'/'ME4', 'Creature — Spirit').
card_original_text('radjan spirit'/'ME4', '{T}: Target creature loses flying until end of turn.').
card_image_name('radjan spirit'/'ME4', 'radjan spirit').
card_uid('radjan spirit'/'ME4', 'ME4:Radjan Spirit:radjan spirit').
card_rarity('radjan spirit'/'ME4', 'Uncommon').
card_artist('radjan spirit'/'ME4', 'Christopher Rush').
card_number('radjan spirit'/'ME4', '162').
card_multiverse_id('radjan spirit'/'ME4', '202438').

card_in_set('rain of daggers', 'ME4').
card_original_type('rain of daggers'/'ME4', 'Sorcery').
card_original_text('rain of daggers'/'ME4', 'Destroy all creatures target opponent controls. You lose 2 life for each creature destroyed this way.').
card_image_name('rain of daggers'/'ME4', 'rain of daggers').
card_uid('rain of daggers'/'ME4', 'ME4:Rain of Daggers:rain of daggers').
card_rarity('rain of daggers'/'ME4', 'Rare').
card_artist('rain of daggers'/'ME4', 'Melissa A. Benson').
card_number('rain of daggers'/'ME4', '94').
card_flavor_text('rain of daggers'/'ME4', 'Knives in the sky, cries in the air, and blood on the ground.').
card_multiverse_id('rain of daggers'/'ME4', '233301').

card_in_set('rakalite', 'ME4').
card_original_type('rakalite'/'ME4', 'Artifact').
card_original_text('rakalite'/'ME4', '{2}: Prevent the next 1 damage that would be dealt to target creature or player this turn. Return Rakalite to its owner\'s hand at the beginning of the next end step.').
card_image_name('rakalite'/'ME4', 'rakalite').
card_uid('rakalite'/'ME4', 'ME4:Rakalite:rakalite').
card_rarity('rakalite'/'ME4', 'Rare').
card_artist('rakalite'/'ME4', 'Christopher Rush').
card_number('rakalite'/'ME4', '223').
card_flavor_text('rakalite'/'ME4', 'Urza was the first to understand that the war would not be lost for lack of power, but for lack of troops.').
card_multiverse_id('rakalite'/'ME4', '238128').

card_in_set('reconstruction', 'ME4').
card_original_type('reconstruction'/'ME4', 'Sorcery').
card_original_text('reconstruction'/'ME4', 'Return target artifact card from your graveyard to your hand.').
card_image_name('reconstruction'/'ME4', 'reconstruction').
card_uid('reconstruction'/'ME4', 'ME4:Reconstruction:reconstruction').
card_rarity('reconstruction'/'ME4', 'Common').
card_artist('reconstruction'/'ME4', 'Anson Maddocks').
card_number('reconstruction'/'ME4', '59').
card_flavor_text('reconstruction'/'ME4', 'Tedious research made the Sages of the College of Lat-Nam adept in repairing broken artifacts.').
card_multiverse_id('reconstruction'/'ME4', '202580').

card_in_set('red elemental blast', 'ME4').
card_original_type('red elemental blast'/'ME4', 'Instant').
card_original_text('red elemental blast'/'ME4', 'Choose one — Counter target blue spell; or destroy target blue permanent.').
card_image_name('red elemental blast'/'ME4', 'red elemental blast').
card_uid('red elemental blast'/'ME4', 'ME4:Red Elemental Blast:red elemental blast').
card_rarity('red elemental blast'/'ME4', 'Uncommon').
card_artist('red elemental blast'/'ME4', 'Richard Thomas').
card_number('red elemental blast'/'ME4', '131').
card_multiverse_id('red elemental blast'/'ME4', '202447').

card_in_set('regrowth', 'ME4').
card_original_type('regrowth'/'ME4', 'Sorcery').
card_original_text('regrowth'/'ME4', 'Return target card from your graveyard to your hand.').
card_image_name('regrowth'/'ME4', 'regrowth').
card_uid('regrowth'/'ME4', 'ME4:Regrowth:regrowth').
card_rarity('regrowth'/'ME4', 'Rare').
card_artist('regrowth'/'ME4', 'Dameon Willich').
card_number('regrowth'/'ME4', '163').
card_multiverse_id('regrowth'/'ME4', '202461').

card_in_set('righteous charge', 'ME4').
card_original_type('righteous charge'/'ME4', 'Sorcery').
card_original_text('righteous charge'/'ME4', 'Creatures you control get +2/+2 until end of turn.').
card_image_name('righteous charge'/'ME4', 'righteous charge').
card_uid('righteous charge'/'ME4', 'ME4:Righteous Charge:righteous charge').
card_rarity('righteous charge'/'ME4', 'Common').
card_artist('righteous charge'/'ME4', 'Jeffrey R. Busch').
card_number('righteous charge'/'ME4', '23').
card_flavor_text('righteous charge'/'ME4', 'Bravery shines brightest in a pure soul.').
card_multiverse_id('righteous charge'/'ME4', '202525').

card_in_set('ring of renewal', 'ME4').
card_original_type('ring of renewal'/'ME4', 'Artifact').
card_original_text('ring of renewal'/'ME4', '{5}, {T}: Discard a card at random, then draw two cards.').
card_image_name('ring of renewal'/'ME4', 'ring of renewal').
card_uid('ring of renewal'/'ME4', 'ME4:Ring of Renewal:ring of renewal').
card_rarity('ring of renewal'/'ME4', 'Rare').
card_artist('ring of renewal'/'ME4', 'Douglas Shuler').
card_number('ring of renewal'/'ME4', '224').
card_flavor_text('ring of renewal'/'ME4', 'To the uninitiated, the Ring of Renewal is merely an oddity. For those fluent in the wielding of magic, however, it is a source of great knowledge.').
card_multiverse_id('ring of renewal'/'ME4', '202484').

card_in_set('roc of kher ridges', 'ME4').
card_original_type('roc of kher ridges'/'ME4', 'Creature — Bird').
card_original_text('roc of kher ridges'/'ME4', 'Flying').
card_image_name('roc of kher ridges'/'ME4', 'roc of kher ridges').
card_uid('roc of kher ridges'/'ME4', 'ME4:Roc of Kher Ridges:roc of kher ridges').
card_rarity('roc of kher ridges'/'ME4', 'Uncommon').
card_artist('roc of kher ridges'/'ME4', 'Andi Rusu').
card_number('roc of kher ridges'/'ME4', '132').
card_multiverse_id('roc of kher ridges'/'ME4', '202490').

card_in_set('rock hydra', 'ME4').
card_original_type('rock hydra'/'ME4', 'Creature — Hydra').
card_original_text('rock hydra'/'ME4', 'Rock Hydra enters the battlefield with X +1/+1 counters on it.\nFor each 1 damage that would be dealt to Rock Hydra, if it has a +1/+1 counter on it, remove a +1/+1 counter from it and prevent that 1 damage.\n{R}: Prevent the next 1 damage that would be dealt to Rock Hydra this turn.\n{R}{R}{R}: Put a +1/+1 counter on Rock Hydra. Activate this ability only during your upkeep.').
card_image_name('rock hydra'/'ME4', 'rock hydra').
card_uid('rock hydra'/'ME4', 'ME4:Rock Hydra:rock hydra').
card_rarity('rock hydra'/'ME4', 'Rare').
card_artist('rock hydra'/'ME4', 'Jeff A. Menges').
card_number('rock hydra'/'ME4', '133').
card_multiverse_id('rock hydra'/'ME4', '202586').

card_in_set('rockslide ambush', 'ME4').
card_original_type('rockslide ambush'/'ME4', 'Sorcery').
card_original_text('rockslide ambush'/'ME4', 'Rockslide Ambush deals damage equal to the number of Mountains you control to target creature.').
card_image_name('rockslide ambush'/'ME4', 'rockslide ambush').
card_uid('rockslide ambush'/'ME4', 'ME4:Rockslide Ambush:rockslide ambush').
card_rarity('rockslide ambush'/'ME4', 'Common').
card_artist('rockslide ambush'/'ME4', 'Inoue Junichi').
card_number('rockslide ambush'/'ME4', '134').
card_multiverse_id('rockslide ambush'/'ME4', '202592').

card_in_set('sandstorm', 'ME4').
card_original_type('sandstorm'/'ME4', 'Instant').
card_original_text('sandstorm'/'ME4', 'Sandstorm deals 1 damage to each attacking creature.').
card_image_name('sandstorm'/'ME4', 'sandstorm').
card_uid('sandstorm'/'ME4', 'ME4:Sandstorm:sandstorm').
card_rarity('sandstorm'/'ME4', 'Common').
card_artist('sandstorm'/'ME4', 'Brian Snõddy').
card_number('sandstorm'/'ME4', '164').
card_flavor_text('sandstorm'/'ME4', 'Even the landscape turned against Sarsour, first rising up and pelting him, then rearranging itself so he could no longer find his way.').
card_multiverse_id('sandstorm'/'ME4', '220964').

card_in_set('savannah', 'ME4').
card_original_type('savannah'/'ME4', 'Land — Forest Plains').
card_original_text('savannah'/'ME4', '').
card_image_name('savannah'/'ME4', 'savannah').
card_uid('savannah'/'ME4', 'ME4:Savannah:savannah').
card_rarity('savannah'/'ME4', 'Rare').
card_artist('savannah'/'ME4', 'Rob Alexander').
card_number('savannah'/'ME4', '250').
card_multiverse_id('savannah'/'ME4', '202571').

card_in_set('savannah lions', 'ME4').
card_original_type('savannah lions'/'ME4', 'Creature — Cat').
card_original_text('savannah lions'/'ME4', '').
card_image_name('savannah lions'/'ME4', 'savannah lions').
card_uid('savannah lions'/'ME4', 'ME4:Savannah Lions:savannah lions').
card_rarity('savannah lions'/'ME4', 'Uncommon').
card_artist('savannah lions'/'ME4', 'Daniel Gelon').
card_number('savannah lions'/'ME4', '24').
card_flavor_text('savannah lions'/'ME4', 'The traditional kings of the jungle command a healthy respect in other climates as well. Relying mainly on their stealth and speed, Savannah Lions can take a victim by surprise, even in the endless flat plains of their homeland.').
card_multiverse_id('savannah lions'/'ME4', '221577').

card_in_set('scarecrow', 'ME4').
card_original_type('scarecrow'/'ME4', 'Artifact Creature — Scarecrow').
card_original_text('scarecrow'/'ME4', '{6}, {T}: Prevent all damage that would be dealt to you this turn by creatures with flying.').
card_image_name('scarecrow'/'ME4', 'scarecrow').
card_uid('scarecrow'/'ME4', 'ME4:Scarecrow:scarecrow').
card_rarity('scarecrow'/'ME4', 'Uncommon').
card_artist('scarecrow'/'ME4', 'Anson Maddocks').
card_number('scarecrow'/'ME4', '225').
card_flavor_text('scarecrow'/'ME4', 'There was more malice in its button eyes than should have been possible in something that had never known life.').
card_multiverse_id('scarecrow'/'ME4', '202615').

card_in_set('scarwood bandits', 'ME4').
card_original_type('scarwood bandits'/'ME4', 'Creature — Human Rogue').
card_original_text('scarwood bandits'/'ME4', 'Forestwalk\n{2}{G}, {T}: Unless target artifact\'s controller pays {2}, gain control of that artifact for as long as Scarwood Bandits remains on the battlefield.').
card_image_name('scarwood bandits'/'ME4', 'scarwood bandits').
card_uid('scarwood bandits'/'ME4', 'ME4:Scarwood Bandits:scarwood bandits').
card_rarity('scarwood bandits'/'ME4', 'Rare').
card_artist('scarwood bandits'/'ME4', 'Mark Poole').
card_number('scarwood bandits'/'ME4', '165').
card_multiverse_id('scarwood bandits'/'ME4', '202459').

card_in_set('scavenger folk', 'ME4').
card_original_type('scavenger folk'/'ME4', 'Creature — Human').
card_original_text('scavenger folk'/'ME4', '{G}, {T}, Sacrifice Scavenger Folk: Destroy target artifact.').
card_image_name('scavenger folk'/'ME4', 'scavenger folk').
card_uid('scavenger folk'/'ME4', 'ME4:Scavenger Folk:scavenger folk').
card_rarity('scavenger folk'/'ME4', 'Common').
card_artist('scavenger folk'/'ME4', 'Dennis Detwiller').
card_number('scavenger folk'/'ME4', '166').
card_multiverse_id('scavenger folk'/'ME4', '202416').

card_in_set('scavenging ghoul', 'ME4').
card_original_type('scavenging ghoul'/'ME4', 'Creature — Zombie').
card_original_text('scavenging ghoul'/'ME4', 'At the beginning of each end step, put a corpse counter on Scavenging Ghoul for each creature put into a graveyard from the battlefield this turn.\nRemove a corpse counter from Scavenging Ghoul: Regenerate Scavenging Ghoul.').
card_image_name('scavenging ghoul'/'ME4', 'scavenging ghoul').
card_uid('scavenging ghoul'/'ME4', 'ME4:Scavenging Ghoul:scavenging ghoul').
card_rarity('scavenging ghoul'/'ME4', 'Uncommon').
card_artist('scavenging ghoul'/'ME4', 'Jeff A. Menges').
card_number('scavenging ghoul'/'ME4', '95').
card_multiverse_id('scavenging ghoul'/'ME4', '202441').

card_in_set('scrubland', 'ME4').
card_original_type('scrubland'/'ME4', 'Land — Plains Swamp').
card_original_text('scrubland'/'ME4', '').
card_image_name('scrubland'/'ME4', 'scrubland').
card_uid('scrubland'/'ME4', 'ME4:Scrubland:scrubland').
card_rarity('scrubland'/'ME4', 'Rare').
card_artist('scrubland'/'ME4', 'Jesper Myrfors').
card_number('scrubland'/'ME4', '251').
card_multiverse_id('scrubland'/'ME4', '202515').

card_in_set('sea serpent', 'ME4').
card_original_type('sea serpent'/'ME4', 'Creature — Serpent').
card_original_text('sea serpent'/'ME4', 'Sea Serpent can\'t attack unless defending player controls an Island.\nWhen you control no Islands, sacrifice Sea Serpent.').
card_image_name('sea serpent'/'ME4', 'sea serpent').
card_uid('sea serpent'/'ME4', 'ME4:Sea Serpent:sea serpent').
card_rarity('sea serpent'/'ME4', 'Common').
card_artist('sea serpent'/'ME4', 'Jeff A. Menges').
card_number('sea serpent'/'ME4', '60').
card_flavor_text('sea serpent'/'ME4', 'Legend has it that Serpents used to be bigger, but how could that be?').
card_multiverse_id('sea serpent'/'ME4', '221566').

card_in_set('sedge troll', 'ME4').
card_original_type('sedge troll'/'ME4', 'Creature — Troll').
card_original_text('sedge troll'/'ME4', 'Sedge Troll gets +1/+1 as long as you control a Swamp.\n{B}: Regenerate Sedge Troll.').
card_image_name('sedge troll'/'ME4', 'sedge troll').
card_uid('sedge troll'/'ME4', 'ME4:Sedge Troll:sedge troll').
card_rarity('sedge troll'/'ME4', 'Rare').
card_artist('sedge troll'/'ME4', 'Dan Frazier').
card_number('sedge troll'/'ME4', '135').
card_flavor_text('sedge troll'/'ME4', 'The stench in the hovel was overpowering; something loathsome was cooking. Occasionally something surfaced in the thick paste, but my host would casually push it down before I could make out what it was.').
card_multiverse_id('sedge troll'/'ME4', '202454').

card_in_set('sengir vampire', 'ME4').
card_original_type('sengir vampire'/'ME4', 'Creature — Vampire').
card_original_text('sengir vampire'/'ME4', 'Flying\nWhenever a creature dealt damage by Sengir Vampire this turn is put into a graveyard, put a +1/+1 counter on Sengir Vampire.').
card_image_name('sengir vampire'/'ME4', 'sengir vampire').
card_uid('sengir vampire'/'ME4', 'ME4:Sengir Vampire:sengir vampire').
card_rarity('sengir vampire'/'ME4', 'Uncommon').
card_artist('sengir vampire'/'ME4', 'Anson Maddocks').
card_number('sengir vampire'/'ME4', '96').
card_multiverse_id('sengir vampire'/'ME4', '220960').

card_in_set('serendib djinn', 'ME4').
card_original_type('serendib djinn'/'ME4', 'Creature — Djinn').
card_original_text('serendib djinn'/'ME4', 'Flying\nAt the beginning of your upkeep, sacrifice a land. If you sacrifice an Island this way, Serendib Djinn deals 3 damage to you.\nWhen you control no lands, sacrifice Serendib Djinn.').
card_image_name('serendib djinn'/'ME4', 'serendib djinn').
card_uid('serendib djinn'/'ME4', 'ME4:Serendib Djinn:serendib djinn').
card_rarity('serendib djinn'/'ME4', 'Rare').
card_artist('serendib djinn'/'ME4', 'Anson Maddocks').
card_number('serendib djinn'/'ME4', '61').
card_multiverse_id('serendib djinn'/'ME4', '202546').

card_in_set('serra angel', 'ME4').
card_original_type('serra angel'/'ME4', 'Creature — Angel').
card_original_text('serra angel'/'ME4', 'Flying, vigilance').
card_image_name('serra angel'/'ME4', 'serra angel').
card_uid('serra angel'/'ME4', 'ME4:Serra Angel:serra angel').
card_rarity('serra angel'/'ME4', 'Uncommon').
card_artist('serra angel'/'ME4', 'Douglas Shuler').
card_number('serra angel'/'ME4', '25').
card_flavor_text('serra angel'/'ME4', 'Born with wings of light and a sword of faith, this heavenly incarnation embodies both fury and purity.').
card_multiverse_id('serra angel'/'ME4', '202561').

card_in_set('serra aviary', 'ME4').
card_original_type('serra aviary'/'ME4', 'World Enchantment').
card_original_text('serra aviary'/'ME4', 'Creatures with flying get +1/+1.').
card_image_name('serra aviary'/'ME4', 'serra aviary').
card_uid('serra aviary'/'ME4', 'ME4:Serra Aviary:serra aviary').
card_rarity('serra aviary'/'ME4', 'Uncommon').
card_artist('serra aviary'/'ME4', 'Nicola Leonard').
card_number('serra aviary'/'ME4', '26').
card_flavor_text('serra aviary'/'ME4', '\"Serra, like Feroz, is long since dead. But remember, Daria: her spirit shall survive so long as the Homelands do.\"\n—Taysir').
card_multiverse_id('serra aviary'/'ME4', '202527').

card_in_set('serra bestiary', 'ME4').
card_original_type('serra bestiary'/'ME4', 'Enchantment — Aura').
card_original_text('serra bestiary'/'ME4', 'Enchant creature\nAt the beginning of your upkeep, sacrifice Serra Bestiary unless you pay {W}{W}.\nEnchanted creature can\'t attack or block and its activated abilities with {T} in their costs can\'t be activated.').
card_image_name('serra bestiary'/'ME4', 'serra bestiary').
card_uid('serra bestiary'/'ME4', 'ME4:Serra Bestiary:serra bestiary').
card_rarity('serra bestiary'/'ME4', 'Common').
card_artist('serra bestiary'/'ME4', 'Steve Luke').
card_number('serra bestiary'/'ME4', '27').
card_multiverse_id('serra bestiary'/'ME4', '202481').

card_in_set('shapeshifter', 'ME4').
card_original_type('shapeshifter'/'ME4', 'Artifact Creature — Shapeshifter').
card_original_text('shapeshifter'/'ME4', 'As Shapeshifter enters the battlefield, choose a number between 0 and 7.\nAt the beginning of your upkeep, you may choose a number between 0 and 7.\nShapeshifter\'s power is equal to the last chosen number and its toughness is equal to 7 minus that number.').
card_image_name('shapeshifter'/'ME4', 'shapeshifter').
card_uid('shapeshifter'/'ME4', 'ME4:Shapeshifter:shapeshifter').
card_rarity('shapeshifter'/'ME4', 'Uncommon').
card_artist('shapeshifter'/'ME4', 'Dan Frazier').
card_number('shapeshifter'/'ME4', '226').
card_multiverse_id('shapeshifter'/'ME4', '202581').

card_in_set('shivan dragon', 'ME4').
card_original_type('shivan dragon'/'ME4', 'Creature — Dragon').
card_original_text('shivan dragon'/'ME4', 'Flying\n{R}: Shivan Dragon gets +1/+0 until end of turn.').
card_image_name('shivan dragon'/'ME4', 'shivan dragon').
card_uid('shivan dragon'/'ME4', 'ME4:Shivan Dragon:shivan dragon').
card_rarity('shivan dragon'/'ME4', 'Rare').
card_artist('shivan dragon'/'ME4', 'Melissa A. Benson').
card_number('shivan dragon'/'ME4', '136').
card_flavor_text('shivan dragon'/'ME4', 'While it\'s true most Dragons are cruel, the Shivan Dragon seems to take particular glee in the misery of others, often tormenting its victims much like a cat plays with a mouse before delivering the final blow.').
card_multiverse_id('shivan dragon'/'ME4', '228264').

card_in_set('sinkhole', 'ME4').
card_original_type('sinkhole'/'ME4', 'Sorcery').
card_original_text('sinkhole'/'ME4', 'Destroy target land.').
card_image_name('sinkhole'/'ME4', 'sinkhole').
card_uid('sinkhole'/'ME4', 'ME4:Sinkhole:sinkhole').
card_rarity('sinkhole'/'ME4', 'Rare').
card_artist('sinkhole'/'ME4', 'Sandra Everingham').
card_number('sinkhole'/'ME4', '97').
card_multiverse_id('sinkhole'/'ME4', '202439').

card_in_set('sleight of hand', 'ME4').
card_original_type('sleight of hand'/'ME4', 'Sorcery').
card_original_text('sleight of hand'/'ME4', 'Look at the top two cards of your library. Put one of them into your hand and the other on the bottom of your library.').
card_image_name('sleight of hand'/'ME4', 'sleight of hand').
card_uid('sleight of hand'/'ME4', 'ME4:Sleight of Hand:sleight of hand').
card_rarity('sleight of hand'/'ME4', 'Common').
card_artist('sleight of hand'/'ME4', 'Phil Foglio').
card_number('sleight of hand'/'ME4', '62').
card_multiverse_id('sleight of hand'/'ME4', '221496').

card_in_set('smoke', 'ME4').
card_original_type('smoke'/'ME4', 'Enchantment').
card_original_text('smoke'/'ME4', 'Players can\'t untap more than one creature during their untap steps.').
card_image_name('smoke'/'ME4', 'smoke').
card_uid('smoke'/'ME4', 'ME4:Smoke:smoke').
card_rarity('smoke'/'ME4', 'Rare').
card_artist('smoke'/'ME4', 'Jesper Myrfors').
card_number('smoke'/'ME4', '137').
card_multiverse_id('smoke'/'ME4', '233306').

card_in_set('sol ring', 'ME4').
card_original_type('sol ring'/'ME4', 'Artifact').
card_original_text('sol ring'/'ME4', '{T}: Add {2} to your mana pool.').
card_image_name('sol ring'/'ME4', 'sol ring').
card_uid('sol ring'/'ME4', 'ME4:Sol Ring:sol ring').
card_rarity('sol ring'/'ME4', 'Rare').
card_artist('sol ring'/'ME4', 'Mark Tedin').
card_number('sol ring'/'ME4', '227').
card_multiverse_id('sol ring'/'ME4', '202500').

card_in_set('soldevi golem', 'ME4').
card_original_type('soldevi golem'/'ME4', 'Artifact Creature — Golem').
card_original_text('soldevi golem'/'ME4', 'Soldevi Golem doesn\'t untap during your untap step.\nAt the beginning of your upkeep, you may untap target tapped creature an opponent controls. If you do, untap Soldevi Golem.').
card_image_name('soldevi golem'/'ME4', 'soldevi golem').
card_uid('soldevi golem'/'ME4', 'ME4:Soldevi Golem:soldevi golem').
card_rarity('soldevi golem'/'ME4', 'Uncommon').
card_artist('soldevi golem'/'ME4', 'Anson Maddocks').
card_number('soldevi golem'/'ME4', '228').
card_flavor_text('soldevi golem'/'ME4', 'Slow and steady wins the race.').
card_multiverse_id('soldevi golem'/'ME4', '202569').

card_in_set('soldevi machinist', 'ME4').
card_original_type('soldevi machinist'/'ME4', 'Creature — Human Wizard Artificer').
card_original_text('soldevi machinist'/'ME4', '{T}: Add {2} to your mana pool. Spend this mana only to activate abilities of artifacts.').
card_image_name('soldevi machinist'/'ME4', 'soldevi machinist').
card_uid('soldevi machinist'/'ME4', 'ME4:Soldevi Machinist:soldevi machinist').
card_rarity('soldevi machinist'/'ME4', 'Uncommon').
card_artist('soldevi machinist'/'ME4', 'Jeff A. Menges').
card_number('soldevi machinist'/'ME4', '63').
card_multiverse_id('soldevi machinist'/'ME4', '202522').

card_in_set('soul shred', 'ME4').
card_original_type('soul shred'/'ME4', 'Sorcery').
card_original_text('soul shred'/'ME4', 'Soul Shred deals 3 damage to target nonblack creature. You gain 3 life.').
card_image_name('soul shred'/'ME4', 'soul shred').
card_uid('soul shred'/'ME4', 'ME4:Soul Shred:soul shred').
card_rarity('soul shred'/'ME4', 'Common').
card_artist('soul shred'/'ME4', 'Alan Rabinowitz').
card_number('soul shred'/'ME4', '98').
card_multiverse_id('soul shred'/'ME4', '202466').

card_in_set('southern elephant', 'ME4').
card_original_type('southern elephant'/'ME4', 'Creature — Elephant').
card_original_text('southern elephant'/'ME4', '').
card_image_name('southern elephant'/'ME4', 'southern elephant').
card_uid('southern elephant'/'ME4', 'ME4:Southern Elephant:southern elephant').
card_rarity('southern elephant'/'ME4', 'Common').
card_artist('southern elephant'/'ME4', 'Wang Yuqun').
card_number('southern elephant'/'ME4', '167').
card_flavor_text('southern elephant'/'ME4', 'While defending their southern borders, both the Wu and Shu kingdoms fought against the barbarians\' trained elephants.').
card_multiverse_id('southern elephant'/'ME4', '202557').

card_in_set('spotted griffin', 'ME4').
card_original_type('spotted griffin'/'ME4', 'Creature — Griffin').
card_original_text('spotted griffin'/'ME4', 'Flying').
card_image_name('spotted griffin'/'ME4', 'spotted griffin').
card_uid('spotted griffin'/'ME4', 'ME4:Spotted Griffin:spotted griffin').
card_rarity('spotted griffin'/'ME4', 'Common').
card_artist('spotted griffin'/'ME4', 'William Simpson').
card_number('spotted griffin'/'ME4', '28').
card_flavor_text('spotted griffin'/'ME4', 'When the cat flies and the bird stalks, guard your horses carefully.').
card_multiverse_id('spotted griffin'/'ME4', '202473').

card_in_set('squall', 'ME4').
card_original_type('squall'/'ME4', 'Sorcery').
card_original_text('squall'/'ME4', 'Squall deals 2 damage to each creature with flying.').
card_image_name('squall'/'ME4', 'squall').
card_uid('squall'/'ME4', 'ME4:Squall:squall').
card_rarity('squall'/'ME4', 'Uncommon').
card_artist('squall'/'ME4', 'Carl Critchlow').
card_number('squall'/'ME4', '168').
card_flavor_text('squall'/'ME4', '\"To-night the winds begin to rise . . .\nThe rooks are blown about the skies . . . .\"\n—Alfred, Lord Tennyson, In Memoriam').
card_multiverse_id('squall'/'ME4', '221513').

card_in_set('staff of zegon', 'ME4').
card_original_type('staff of zegon'/'ME4', 'Artifact').
card_original_text('staff of zegon'/'ME4', '{3}, {T}: Target creature gets -2/-0 until end of turn.').
card_image_name('staff of zegon'/'ME4', 'staff of zegon').
card_uid('staff of zegon'/'ME4', 'ME4:Staff of Zegon:staff of zegon').
card_rarity('staff of zegon'/'ME4', 'Common').
card_artist('staff of zegon'/'ME4', 'Mark Poole').
card_number('staff of zegon'/'ME4', '229').
card_flavor_text('staff of zegon'/'ME4', 'Though Mishra was impressed by the staves Ashnod had created for Zegon\'s defense, he understood they only hinted at her full potential.').
card_multiverse_id('staff of zegon'/'ME4', '202607').

card_in_set('stasis', 'ME4').
card_original_type('stasis'/'ME4', 'Enchantment').
card_original_text('stasis'/'ME4', 'Players skip their untap steps.\nAt the beginning of your upkeep, sacrifice Stasis unless you pay {U}.').
card_image_name('stasis'/'ME4', 'stasis').
card_uid('stasis'/'ME4', 'ME4:Stasis:stasis').
card_rarity('stasis'/'ME4', 'Rare').
card_artist('stasis'/'ME4', 'Fay Jones').
card_number('stasis'/'ME4', '64').
card_multiverse_id('stasis'/'ME4', '202472').

card_in_set('steam catapult', 'ME4').
card_original_type('steam catapult'/'ME4', 'Creature — Human Soldier').
card_original_text('steam catapult'/'ME4', '{T}: Destroy target tapped creature. Activate this ability only during your turn, before attackers are declared.').
card_image_name('steam catapult'/'ME4', 'steam catapult').
card_uid('steam catapult'/'ME4', 'ME4:Steam Catapult:steam catapult').
card_rarity('steam catapult'/'ME4', 'Rare').
card_artist('steam catapult'/'ME4', 'Mark Tedin').
card_number('steam catapult'/'ME4', '29').
card_flavor_text('steam catapult'/'ME4', '\"You idiots! Turn it around! Turn it around!\"').
card_multiverse_id('steam catapult'/'ME4', '221567').

card_in_set('strip mine', 'ME4').
card_original_type('strip mine'/'ME4', 'Land').
card_original_text('strip mine'/'ME4', '{T}: Add {1} to your mana pool.\n{T}, Sacrifice Strip Mine: Destroy target land.').
card_image_name('strip mine'/'ME4', 'strip mine').
card_uid('strip mine'/'ME4', 'ME4:Strip Mine:strip mine').
card_rarity('strip mine'/'ME4', 'Rare').
card_artist('strip mine'/'ME4', 'Daniel Gelon').
card_number('strip mine'/'ME4', '252').
card_flavor_text('strip mine'/'ME4', 'Unlike previous conflicts, the war between Urza and Mishra made Dominia itself a casualty of war.').
card_multiverse_id('strip mine'/'ME4', '202433').

card_in_set('swords to plowshares', 'ME4').
card_original_type('swords to plowshares'/'ME4', 'Instant').
card_original_text('swords to plowshares'/'ME4', 'Exile target creature. Its controller gains life equal to its power.').
card_image_name('swords to plowshares'/'ME4', 'swords to plowshares').
card_uid('swords to plowshares'/'ME4', 'ME4:Swords to Plowshares:swords to plowshares').
card_rarity('swords to plowshares'/'ME4', 'Uncommon').
card_artist('swords to plowshares'/'ME4', 'Jeff A. Menges').
card_number('swords to plowshares'/'ME4', '30').
card_multiverse_id('swords to plowshares'/'ME4', '202462').

card_in_set('sylvan tutor', 'ME4').
card_original_type('sylvan tutor'/'ME4', 'Sorcery').
card_original_text('sylvan tutor'/'ME4', 'Search your library for a creature card and reveal that card. Shuffle your library, then put the card on top of it.').
card_image_name('sylvan tutor'/'ME4', 'sylvan tutor').
card_uid('sylvan tutor'/'ME4', 'ME4:Sylvan Tutor:sylvan tutor').
card_rarity('sylvan tutor'/'ME4', 'Uncommon').
card_artist('sylvan tutor'/'ME4', 'Kaja Foglio').
card_number('sylvan tutor'/'ME4', '169').
card_multiverse_id('sylvan tutor'/'ME4', '221548').

card_in_set('symbol of unsummoning', 'ME4').
card_original_type('symbol of unsummoning'/'ME4', 'Sorcery').
card_original_text('symbol of unsummoning'/'ME4', 'Return target creature to its owner\'s hand.\nDraw a card.').
card_image_name('symbol of unsummoning'/'ME4', 'symbol of unsummoning').
card_uid('symbol of unsummoning'/'ME4', 'ME4:Symbol of Unsummoning:symbol of unsummoning').
card_rarity('symbol of unsummoning'/'ME4', 'Common').
card_artist('symbol of unsummoning'/'ME4', 'Adam Rex').
card_number('symbol of unsummoning'/'ME4', '65').
card_flavor_text('symbol of unsummoning'/'ME4', '\". . . inviting the soul to wander for a spell in abysses of solitude . . . .\" —Kate Chopin, The Awakening').
card_multiverse_id('symbol of unsummoning'/'ME4', '202579').

card_in_set('tablet of epityr', 'ME4').
card_original_type('tablet of epityr'/'ME4', 'Artifact').
card_original_text('tablet of epityr'/'ME4', 'Whenever an artifact you control is put into a graveyard from the battlefield, you may pay {1}. If you do, you gain 1 life.').
card_image_name('tablet of epityr'/'ME4', 'tablet of epityr').
card_uid('tablet of epityr'/'ME4', 'ME4:Tablet of Epityr:tablet of epityr').
card_rarity('tablet of epityr'/'ME4', 'Common').
card_artist('tablet of epityr'/'ME4', 'Christopher Rush').
card_number('tablet of epityr'/'ME4', '230').
card_flavor_text('tablet of epityr'/'ME4', 'Originally considered the work of Urza, this tablet was created by forgers seeking to imitate Urza\'s masterpieces.').
card_multiverse_id('tablet of epityr'/'ME4', '221576').

card_in_set('taiga', 'ME4').
card_original_type('taiga'/'ME4', 'Land — Mountain Forest').
card_original_text('taiga'/'ME4', '').
card_image_name('taiga'/'ME4', 'taiga').
card_uid('taiga'/'ME4', 'ME4:Taiga:taiga').
card_rarity('taiga'/'ME4', 'Rare').
card_artist('taiga'/'ME4', 'Rob Alexander').
card_number('taiga'/'ME4', '253').
card_multiverse_id('taiga'/'ME4', '202421').

card_in_set('talas researcher', 'ME4').
card_original_type('talas researcher'/'ME4', 'Creature — Human Pirate Wizard').
card_original_text('talas researcher'/'ME4', '{T}: Draw a card. Activate this ability only during your turn, before attackers are declared.').
card_image_name('talas researcher'/'ME4', 'talas researcher').
card_uid('talas researcher'/'ME4', 'ME4:Talas Researcher:talas researcher').
card_rarity('talas researcher'/'ME4', 'Uncommon').
card_artist('talas researcher'/'ME4', 'Kaja Foglio').
card_number('talas researcher'/'ME4', '66').
card_flavor_text('talas researcher'/'ME4', 'From time, knowledge.\nFrom knowledge, power.').
card_multiverse_id('talas researcher'/'ME4', '221498').

card_in_set('tawnos\'s wand', 'ME4').
card_original_type('tawnos\'s wand'/'ME4', 'Artifact').
card_original_text('tawnos\'s wand'/'ME4', '{2}, {T}: Target creature with power 2 or less is unblockable this turn.').
card_image_name('tawnos\'s wand'/'ME4', 'tawnos\'s wand').
card_uid('tawnos\'s wand'/'ME4', 'ME4:Tawnos\'s Wand:tawnos\'s wand').
card_rarity('tawnos\'s wand'/'ME4', 'Common').
card_artist('tawnos\'s wand'/'ME4', 'Douglas Shuler').
card_number('tawnos\'s wand'/'ME4', '231').
card_multiverse_id('tawnos\'s wand'/'ME4', '221102').

card_in_set('tawnos\'s weaponry', 'ME4').
card_original_type('tawnos\'s weaponry'/'ME4', 'Artifact').
card_original_text('tawnos\'s weaponry'/'ME4', 'You may choose not to untap Tawnos\'s Weaponry during your untap step.\n{2}, {T}: Target creature gets +1/+1 for as long as Tawnos\'s Weaponry remains tapped.').
card_image_name('tawnos\'s weaponry'/'ME4', 'tawnos\'s weaponry').
card_uid('tawnos\'s weaponry'/'ME4', 'ME4:Tawnos\'s Weaponry:tawnos\'s weaponry').
card_rarity('tawnos\'s weaponry'/'ME4', 'Uncommon').
card_artist('tawnos\'s weaponry'/'ME4', 'Dan Frazier').
card_number('tawnos\'s weaponry'/'ME4', '232').
card_flavor_text('tawnos\'s weaponry'/'ME4', 'When Urza\'s war machines became too costly, Tawnos\'s weaponry replaced them.').
card_multiverse_id('tawnos\'s weaponry'/'ME4', '221107').

card_in_set('temple acolyte', 'ME4').
card_original_type('temple acolyte'/'ME4', 'Creature — Human Cleric').
card_original_text('temple acolyte'/'ME4', 'When Temple Acolyte enters the battlefield, you gain 3 life.').
card_image_name('temple acolyte'/'ME4', 'temple acolyte').
card_uid('temple acolyte'/'ME4', 'ME4:Temple Acolyte:temple acolyte').
card_rarity('temple acolyte'/'ME4', 'Common').
card_artist('temple acolyte'/'ME4', 'Lubov').
card_number('temple acolyte'/'ME4', '31').
card_flavor_text('temple acolyte'/'ME4', 'Young, yes. Inexperienced, yes. Weak? Don\'t count on it.').
card_multiverse_id('temple acolyte'/'ME4', '202568').

card_in_set('terror', 'ME4').
card_original_type('terror'/'ME4', 'Instant').
card_original_text('terror'/'ME4', 'Destroy target nonartifact, nonblack creature. It can\'t be regenerated.').
card_image_name('terror'/'ME4', 'terror').
card_uid('terror'/'ME4', 'ME4:Terror:terror').
card_rarity('terror'/'ME4', 'Common').
card_artist('terror'/'ME4', 'Ron Spencer').
card_number('terror'/'ME4', '99').
card_multiverse_id('terror'/'ME4', '202486').

card_in_set('tetravus', 'ME4').
card_original_type('tetravus'/'ME4', 'Artifact Creature — Construct').
card_original_text('tetravus'/'ME4', 'Flying\nTetravus enters the battlefield with three +1/+1 counters on it. \nAt the beginning of your upkeep, you may remove any number of +1/+1 counters from Tetravus. If you do, put that many 1/1 colorless Tetravite artifact creature tokens onto the battlefield. They each have flying and \"This creature can\'t be enchanted.\"\nAt the beginning of your upkeep, you may exile any number of tokens put onto the battlefield with Tetravus. If you do, put that many +1/+1 counters on Tetravus.').
card_image_name('tetravus'/'ME4', 'tetravus').
card_uid('tetravus'/'ME4', 'ME4:Tetravus:tetravus').
card_rarity('tetravus'/'ME4', 'Rare').
card_artist('tetravus'/'ME4', 'Mark Tedin').
card_number('tetravus'/'ME4', '233').
card_multiverse_id('tetravus'/'ME4', '202590').

card_in_set('theft of dreams', 'ME4').
card_original_type('theft of dreams'/'ME4', 'Sorcery').
card_original_text('theft of dreams'/'ME4', 'Draw a card for each tapped creature target opponent controls.').
card_image_name('theft of dreams'/'ME4', 'theft of dreams').
card_uid('theft of dreams'/'ME4', 'ME4:Theft of Dreams:theft of dreams').
card_rarity('theft of dreams'/'ME4', 'Uncommon').
card_artist('theft of dreams'/'ME4', 'Adam Rex').
card_number('theft of dreams'/'ME4', '67').
card_flavor_text('theft of dreams'/'ME4', 'Energy is never lost, only transformed.').
card_multiverse_id('theft of dreams'/'ME4', '221500').

card_in_set('thing from the deep', 'ME4').
card_original_type('thing from the deep'/'ME4', 'Creature — Leviathan').
card_original_text('thing from the deep'/'ME4', 'Whenever Thing from the Deep attacks, sacrifice it unless you sacrifice an Island.').
card_image_name('thing from the deep'/'ME4', 'thing from the deep').
card_uid('thing from the deep'/'ME4', 'ME4:Thing from the Deep:thing from the deep').
card_rarity('thing from the deep'/'ME4', 'Rare').
card_artist('thing from the deep'/'ME4', 'Paolo Parente').
card_number('thing from the deep'/'ME4', '68').
card_flavor_text('thing from the deep'/'ME4', 'Seafarers fear sailing off the world\'s edge not so much as down its gullet.').
card_multiverse_id('thing from the deep'/'ME4', '221517').

card_in_set('thunder dragon', 'ME4').
card_original_type('thunder dragon'/'ME4', 'Creature — Dragon').
card_original_text('thunder dragon'/'ME4', 'Flying\nWhen Thunder Dragon enters the battlefield, it deals 3 damage to each creature without flying.').
card_image_name('thunder dragon'/'ME4', 'thunder dragon').
card_uid('thunder dragon'/'ME4', 'ME4:Thunder Dragon:thunder dragon').
card_rarity('thunder dragon'/'ME4', 'Rare').
card_artist('thunder dragon'/'ME4', 'Dana Knutson').
card_number('thunder dragon'/'ME4', '138').
card_multiverse_id('thunder dragon'/'ME4', '202544').

card_in_set('time vault', 'ME4').
card_original_type('time vault'/'ME4', 'Artifact').
card_original_text('time vault'/'ME4', 'Time Vault enters the battlefield tapped.\nTime Vault doesn\'t untap during your untap step.\nIf you would begin your turn while Time Vault is tapped, you may skip that turn instead. If you do, untap Time Vault.\n{T}: Take an extra turn after this one.').
card_image_name('time vault'/'ME4', 'time vault').
card_uid('time vault'/'ME4', 'ME4:Time Vault:time vault').
card_rarity('time vault'/'ME4', 'Rare').
card_artist('time vault'/'ME4', 'Mark Tedin').
card_number('time vault'/'ME4', '234').
card_multiverse_id('time vault'/'ME4', '202470').

card_in_set('titania\'s song', 'ME4').
card_original_type('titania\'s song'/'ME4', 'Enchantment').
card_original_text('titania\'s song'/'ME4', 'Each noncreature artifact loses its abilities and becomes an artifact creature with power and toughness each equal to its converted mana cost. If Titania\'s Song leaves the battlefield, this effect continues until end of turn.').
card_image_name('titania\'s song'/'ME4', 'titania\'s song').
card_uid('titania\'s song'/'ME4', 'ME4:Titania\'s Song:titania\'s song').
card_rarity('titania\'s song'/'ME4', 'Rare').
card_artist('titania\'s song'/'ME4', 'Kerstin Kaman').
card_number('titania\'s song'/'ME4', '170').
card_multiverse_id('titania\'s song'/'ME4', '202570').

card_in_set('transmute artifact', 'ME4').
card_original_type('transmute artifact'/'ME4', 'Sorcery').
card_original_text('transmute artifact'/'ME4', 'Sacrifice an artifact. If you do, search your library for an artifact card. If that card\'s converted mana cost is less than or equal to the sacrificed artifact\'s converted mana cost, put it onto the battlefield. It it\'s greater, you may pay {X}, where X is the difference. If you do, put it onto the battlefield. If you don\'t, put it into its owner\'s graveyard. Then shuffle your library.').
card_image_name('transmute artifact'/'ME4', 'transmute artifact').
card_uid('transmute artifact'/'ME4', 'ME4:Transmute Artifact:transmute artifact').
card_rarity('transmute artifact'/'ME4', 'Rare').
card_artist('transmute artifact'/'ME4', 'Anson Maddocks').
card_number('transmute artifact'/'ME4', '69').
card_multiverse_id('transmute artifact'/'ME4', '202616').

card_in_set('triassic egg', 'ME4').
card_original_type('triassic egg'/'ME4', 'Artifact').
card_original_text('triassic egg'/'ME4', '{3}, {T}: Put a hatchling counter on Triassic Egg. \nRemove two hatchling counters from Triassic Egg, Sacrifice Triassic Egg: You may put a creature card from your hand or graveyard onto the battlefield.').
card_image_name('triassic egg'/'ME4', 'triassic egg').
card_uid('triassic egg'/'ME4', 'ME4:Triassic Egg:triassic egg').
card_rarity('triassic egg'/'ME4', 'Rare').
card_artist('triassic egg'/'ME4', 'Dan Frazier').
card_number('triassic egg'/'ME4', '235').
card_multiverse_id('triassic egg'/'ME4', '202469').

card_in_set('tropical island', 'ME4').
card_original_type('tropical island'/'ME4', 'Land — Forest Island').
card_original_text('tropical island'/'ME4', '').
card_image_name('tropical island'/'ME4', 'tropical island').
card_uid('tropical island'/'ME4', 'ME4:Tropical Island:tropical island').
card_rarity('tropical island'/'ME4', 'Rare').
card_artist('tropical island'/'ME4', 'Jesper Myrfors').
card_number('tropical island'/'ME4', '254').
card_multiverse_id('tropical island'/'ME4', '202446').

card_in_set('tsunami', 'ME4').
card_original_type('tsunami'/'ME4', 'Sorcery').
card_original_text('tsunami'/'ME4', 'Destroy all Islands.').
card_image_name('tsunami'/'ME4', 'tsunami').
card_uid('tsunami'/'ME4', 'ME4:Tsunami:tsunami').
card_rarity('tsunami'/'ME4', 'Rare').
card_artist('tsunami'/'ME4', 'Richard Thomas').
card_number('tsunami'/'ME4', '171').
card_multiverse_id('tsunami'/'ME4', '202457').

card_in_set('tundra', 'ME4').
card_original_type('tundra'/'ME4', 'Land — Plains Island').
card_original_text('tundra'/'ME4', '').
card_image_name('tundra'/'ME4', 'tundra').
card_uid('tundra'/'ME4', 'ME4:Tundra:tundra').
card_rarity('tundra'/'ME4', 'Rare').
card_artist('tundra'/'ME4', 'Jesper Myrfors').
card_number('tundra'/'ME4', '255').
card_multiverse_id('tundra'/'ME4', '202424').

card_in_set('two-headed giant of foriys', 'ME4').
card_original_type('two-headed giant of foriys'/'ME4', 'Creature — Giant').
card_original_text('two-headed giant of foriys'/'ME4', 'Trample\nTwo-Headed Giant of Foriys can block an additional creature.').
card_image_name('two-headed giant of foriys'/'ME4', 'two-headed giant of foriys').
card_uid('two-headed giant of foriys'/'ME4', 'ME4:Two-Headed Giant of Foriys:two-headed giant of foriys').
card_rarity('two-headed giant of foriys'/'ME4', 'Uncommon').
card_artist('two-headed giant of foriys'/'ME4', 'Anson Maddocks').
card_number('two-headed giant of foriys'/'ME4', '139').
card_multiverse_id('two-headed giant of foriys'/'ME4', '202551').

card_in_set('underground sea', 'ME4').
card_original_type('underground sea'/'ME4', 'Land — Island Swamp').
card_original_text('underground sea'/'ME4', '').
card_image_name('underground sea'/'ME4', 'underground sea').
card_uid('underground sea'/'ME4', 'ME4:Underground Sea:underground sea').
card_rarity('underground sea'/'ME4', 'Rare').
card_artist('underground sea'/'ME4', 'Rob Alexander').
card_number('underground sea'/'ME4', '256').
card_multiverse_id('underground sea'/'ME4', '202536').

card_in_set('urza\'s chalice', 'ME4').
card_original_type('urza\'s chalice'/'ME4', 'Artifact').
card_original_text('urza\'s chalice'/'ME4', 'Whenever a player casts an artifact spell, you may pay {1}. If you do, you gain 1 life.').
card_image_name('urza\'s chalice'/'ME4', 'urza\'s chalice').
card_uid('urza\'s chalice'/'ME4', 'ME4:Urza\'s Chalice:urza\'s chalice').
card_rarity('urza\'s chalice'/'ME4', 'Common').
card_artist('urza\'s chalice'/'ME4', 'Jeff A. Menges').
card_number('urza\'s chalice'/'ME4', '236').
card_flavor_text('urza\'s chalice'/'ME4', 'When sorely wounded or tired, Urza would often retreat to the workshops of his apprentices. They were greatly amazed at how much better he looked each time he took a sip of water.').
card_multiverse_id('urza\'s chalice'/'ME4', '221105').

card_in_set('urza\'s mine', 'ME4').
card_original_type('urza\'s mine'/'ME4', 'Land — Urza\'s Mine').
card_original_text('urza\'s mine'/'ME4', '{T}: Add {1} to your mana pool. If you control an Urza\'s Power-Plant and an Urza\'s Tower, add {2} to your mana pool instead.').
card_image_name('urza\'s mine'/'ME4', 'urza\'s mine1').
card_uid('urza\'s mine'/'ME4', 'ME4:Urza\'s Mine:urza\'s mine1').
card_rarity('urza\'s mine'/'ME4', 'Basic Land').
card_artist('urza\'s mine'/'ME4', 'Anson Maddocks').
card_number('urza\'s mine'/'ME4', '257').
card_flavor_text('urza\'s mine'/'ME4', 'Mines became common as cities during the days of the artificers.').
card_multiverse_id('urza\'s mine'/'ME4', '220947').

card_in_set('urza\'s mine', 'ME4').
card_original_type('urza\'s mine'/'ME4', 'Land — Urza\'s Mine').
card_original_text('urza\'s mine'/'ME4', '{T}: Add {1} to your mana pool. If you control an Urza\'s Power-Plant and an Urza\'s Tower, add {2} to your mana pool instead.').
card_image_name('urza\'s mine'/'ME4', 'urza\'s mine2').
card_uid('urza\'s mine'/'ME4', 'ME4:Urza\'s Mine:urza\'s mine2').
card_rarity('urza\'s mine'/'ME4', 'Basic Land').
card_artist('urza\'s mine'/'ME4', 'Anson Maddocks').
card_number('urza\'s mine'/'ME4', '257').
card_flavor_text('urza\'s mine'/'ME4', 'Mines became common as cities during the days of the artificers.').
card_multiverse_id('urza\'s mine'/'ME4', '220948').

card_in_set('urza\'s mine', 'ME4').
card_original_type('urza\'s mine'/'ME4', 'Land — Urza\'s Mine').
card_original_text('urza\'s mine'/'ME4', '{T}: Add {1} to your mana pool. If you control an Urza\'s Power-Plant and an Urza\'s Tower, add {2} to your mana pool instead.').
card_image_name('urza\'s mine'/'ME4', 'urza\'s mine3').
card_uid('urza\'s mine'/'ME4', 'ME4:Urza\'s Mine:urza\'s mine3').
card_rarity('urza\'s mine'/'ME4', 'Basic Land').
card_artist('urza\'s mine'/'ME4', 'Anson Maddocks').
card_number('urza\'s mine'/'ME4', '257').
card_flavor_text('urza\'s mine'/'ME4', 'Mines became common as cities during the days of the artificers.').
card_multiverse_id('urza\'s mine'/'ME4', '220950').

card_in_set('urza\'s mine', 'ME4').
card_original_type('urza\'s mine'/'ME4', 'Land — Urza\'s Mine').
card_original_text('urza\'s mine'/'ME4', '{T}: Add {1} to your mana pool. If you control an Urza\'s Power-Plant and an Urza\'s Tower, add {2} to your mana pool instead.').
card_image_name('urza\'s mine'/'ME4', 'urza\'s mine4').
card_uid('urza\'s mine'/'ME4', 'ME4:Urza\'s Mine:urza\'s mine4').
card_rarity('urza\'s mine'/'ME4', 'Basic Land').
card_artist('urza\'s mine'/'ME4', 'Anson Maddocks').
card_number('urza\'s mine'/'ME4', '257').
card_flavor_text('urza\'s mine'/'ME4', 'Mines became common as cities during the days of the artificers.').
card_multiverse_id('urza\'s mine'/'ME4', '220949').

card_in_set('urza\'s miter', 'ME4').
card_original_type('urza\'s miter'/'ME4', 'Artifact').
card_original_text('urza\'s miter'/'ME4', 'Whenever an artifact you control is put into a graveyard from the battlefield, if it wasn\'t sacrificed, you may pay {3}. If you do, draw a card.').
card_image_name('urza\'s miter'/'ME4', 'urza\'s miter').
card_uid('urza\'s miter'/'ME4', 'ME4:Urza\'s Miter:urza\'s miter').
card_rarity('urza\'s miter'/'ME4', 'Rare').
card_artist('urza\'s miter'/'ME4', 'Randy Asplund-Faith').
card_number('urza\'s miter'/'ME4', '237').
card_multiverse_id('urza\'s miter'/'ME4', '228268').

card_in_set('urza\'s power plant', 'ME4').
card_original_type('urza\'s power plant'/'ME4', 'Land — Urza\'s Power-Plant').
card_original_text('urza\'s power plant'/'ME4', '{T}: Add {1} to your mana pool. If you control an Urza\'s Mine and an Urza\'s Tower, add {2} to your mana pool instead.').
card_image_name('urza\'s power plant'/'ME4', 'urza\'s power plant1').
card_uid('urza\'s power plant'/'ME4', 'ME4:Urza\'s Power Plant:urza\'s power plant1').
card_rarity('urza\'s power plant'/'ME4', 'Basic Land').
card_artist('urza\'s power plant'/'ME4', 'Mark Tedin').
card_number('urza\'s power plant'/'ME4', '258').
card_flavor_text('urza\'s power plant'/'ME4', 'Artifact construction requires immense resources.').
card_multiverse_id('urza\'s power plant'/'ME4', '220953').

card_in_set('urza\'s power plant', 'ME4').
card_original_type('urza\'s power plant'/'ME4', 'Land — Urza\'s Power-Plant').
card_original_text('urza\'s power plant'/'ME4', '{T}: Add {1} to your mana pool. If you control an Urza\'s Mine and an Urza\'s Tower, add {2} to your mana pool instead.').
card_image_name('urza\'s power plant'/'ME4', 'urza\'s power plant2').
card_uid('urza\'s power plant'/'ME4', 'ME4:Urza\'s Power Plant:urza\'s power plant2').
card_rarity('urza\'s power plant'/'ME4', 'Basic Land').
card_artist('urza\'s power plant'/'ME4', 'Mark Tedin').
card_number('urza\'s power plant'/'ME4', '258').
card_flavor_text('urza\'s power plant'/'ME4', 'Artifact construction requires immense resources.').
card_multiverse_id('urza\'s power plant'/'ME4', '220954').

card_in_set('urza\'s power plant', 'ME4').
card_original_type('urza\'s power plant'/'ME4', 'Land — Urza\'s Power-Plant').
card_original_text('urza\'s power plant'/'ME4', '{T}: Add {1} to your mana pool. If you control an Urza\'s Mine and an Urza\'s Tower, add {2} to your mana pool instead.').
card_image_name('urza\'s power plant'/'ME4', 'urza\'s power plant3').
card_uid('urza\'s power plant'/'ME4', 'ME4:Urza\'s Power Plant:urza\'s power plant3').
card_rarity('urza\'s power plant'/'ME4', 'Basic Land').
card_artist('urza\'s power plant'/'ME4', 'Mark Tedin').
card_number('urza\'s power plant'/'ME4', '258').
card_flavor_text('urza\'s power plant'/'ME4', 'Artifact construction requires immense resources.').
card_multiverse_id('urza\'s power plant'/'ME4', '220951').

card_in_set('urza\'s power plant', 'ME4').
card_original_type('urza\'s power plant'/'ME4', 'Land — Urza\'s Power-Plant').
card_original_text('urza\'s power plant'/'ME4', '{T}: Add {1} to your mana pool. If you control an Urza\'s Mine and an Urza\'s Tower, add {2} to your mana pool instead.').
card_image_name('urza\'s power plant'/'ME4', 'urza\'s power plant4').
card_uid('urza\'s power plant'/'ME4', 'ME4:Urza\'s Power Plant:urza\'s power plant4').
card_rarity('urza\'s power plant'/'ME4', 'Basic Land').
card_artist('urza\'s power plant'/'ME4', 'Mark Tedin').
card_number('urza\'s power plant'/'ME4', '258').
card_flavor_text('urza\'s power plant'/'ME4', 'Artifact construction requires immense resources.').
card_multiverse_id('urza\'s power plant'/'ME4', '220952').

card_in_set('urza\'s tower', 'ME4').
card_original_type('urza\'s tower'/'ME4', 'Land — Urza\'s Tower').
card_original_text('urza\'s tower'/'ME4', '{T}: Add {1} to your mana pool. If you control an Urza\'s Mine and an Urza\'s Power-Plant, add {3} to your mana pool instead.').
card_image_name('urza\'s tower'/'ME4', 'urza\'s tower1').
card_uid('urza\'s tower'/'ME4', 'ME4:Urza\'s Tower:urza\'s tower1').
card_rarity('urza\'s tower'/'ME4', 'Basic Land').
card_artist('urza\'s tower'/'ME4', 'Mark Poole').
card_number('urza\'s tower'/'ME4', '259').
card_flavor_text('urza\'s tower'/'ME4', 'Urza always put Tocasia\'s lessons on resource-gathering to effective use.').
card_multiverse_id('urza\'s tower'/'ME4', '220957').

card_in_set('urza\'s tower', 'ME4').
card_original_type('urza\'s tower'/'ME4', 'Land — Urza\'s Tower').
card_original_text('urza\'s tower'/'ME4', '{T}: Add {1} to your mana pool. If you control an Urza\'s Mine and an Urza\'s Power-Plant, add {3} to your mana pool instead.').
card_image_name('urza\'s tower'/'ME4', 'urza\'s tower2').
card_uid('urza\'s tower'/'ME4', 'ME4:Urza\'s Tower:urza\'s tower2').
card_rarity('urza\'s tower'/'ME4', 'Basic Land').
card_artist('urza\'s tower'/'ME4', 'Mark Poole').
card_number('urza\'s tower'/'ME4', '259').
card_flavor_text('urza\'s tower'/'ME4', 'Urza always put Tocasia\'s lessons on resource-gathering to effective use.').
card_multiverse_id('urza\'s tower'/'ME4', '220956').

card_in_set('urza\'s tower', 'ME4').
card_original_type('urza\'s tower'/'ME4', 'Land — Urza\'s Tower').
card_original_text('urza\'s tower'/'ME4', '{T}: Add {1} to your mana pool. If you control an Urza\'s Mine and an Urza\'s Power-Plant, add {3} to your mana pool instead.').
card_image_name('urza\'s tower'/'ME4', 'urza\'s tower3').
card_uid('urza\'s tower'/'ME4', 'ME4:Urza\'s Tower:urza\'s tower3').
card_rarity('urza\'s tower'/'ME4', 'Basic Land').
card_artist('urza\'s tower'/'ME4', 'Mark Poole').
card_number('urza\'s tower'/'ME4', '259').
card_flavor_text('urza\'s tower'/'ME4', 'Urza always put Tocasia\'s lessons on resource-gathering to effective use.').
card_multiverse_id('urza\'s tower'/'ME4', '220955').

card_in_set('urza\'s tower', 'ME4').
card_original_type('urza\'s tower'/'ME4', 'Land — Urza\'s Tower').
card_original_text('urza\'s tower'/'ME4', '{T}: Add {1} to your mana pool. If you control an Urza\'s Mine and an Urza\'s Power-Plant, add {3} to your mana pool instead.').
card_image_name('urza\'s tower'/'ME4', 'urza\'s tower4').
card_uid('urza\'s tower'/'ME4', 'ME4:Urza\'s Tower:urza\'s tower4').
card_rarity('urza\'s tower'/'ME4', 'Basic Land').
card_artist('urza\'s tower'/'ME4', 'Mark Poole').
card_number('urza\'s tower'/'ME4', '259').
card_flavor_text('urza\'s tower'/'ME4', 'Urza always put Tocasia\'s lessons on resource-gathering to effective use.').
card_multiverse_id('urza\'s tower'/'ME4', '220958').

card_in_set('veteran bodyguard', 'ME4').
card_original_type('veteran bodyguard'/'ME4', 'Creature — Human').
card_original_text('veteran bodyguard'/'ME4', 'As long as Veteran Bodyguard is untapped, all damage that would be dealt to you by unblocked creatures is dealt to Veteran Bodyguard instead.').
card_image_name('veteran bodyguard'/'ME4', 'veteran bodyguard').
card_uid('veteran bodyguard'/'ME4', 'ME4:Veteran Bodyguard:veteran bodyguard').
card_rarity('veteran bodyguard'/'ME4', 'Rare').
card_artist('veteran bodyguard'/'ME4', 'Douglas Shuler').
card_number('veteran bodyguard'/'ME4', '32').
card_flavor_text('veteran bodyguard'/'ME4', 'Good bodyguards are hard to find, mainly because they don\'t live long.').
card_multiverse_id('veteran bodyguard'/'ME4', '202583').

card_in_set('vibrating sphere', 'ME4').
card_original_type('vibrating sphere'/'ME4', 'Artifact').
card_original_text('vibrating sphere'/'ME4', 'As long as it\'s your turn, creatures you control get +2/+0.\nAs long as it\'s not your turn, creatures you control get -0/-2.').
card_image_name('vibrating sphere'/'ME4', 'vibrating sphere').
card_uid('vibrating sphere'/'ME4', 'ME4:Vibrating Sphere:vibrating sphere').
card_rarity('vibrating sphere'/'ME4', 'Rare').
card_artist('vibrating sphere'/'ME4', 'Richard Thomas').
card_number('vibrating sphere'/'ME4', '238').
card_flavor_text('vibrating sphere'/'ME4', '\"Unearthly and invisible fibers emanate from this sphere, entangling all who draw near.\"\n—Arcum Dagsson, Soldevi Machinist').
card_multiverse_id('vibrating sphere'/'ME4', '228272').

card_in_set('volcanic island', 'ME4').
card_original_type('volcanic island'/'ME4', 'Land — Island Mountain').
card_original_text('volcanic island'/'ME4', '').
card_image_name('volcanic island'/'ME4', 'volcanic island').
card_uid('volcanic island'/'ME4', 'ME4:Volcanic Island:volcanic island').
card_rarity('volcanic island'/'ME4', 'Rare').
card_artist('volcanic island'/'ME4', 'Brian Snõddy').
card_number('volcanic island'/'ME4', '260').
card_multiverse_id('volcanic island'/'ME4', '202442').

card_in_set('war mammoth', 'ME4').
card_original_type('war mammoth'/'ME4', 'Creature — Elephant').
card_original_text('war mammoth'/'ME4', 'Trample').
card_image_name('war mammoth'/'ME4', 'war mammoth').
card_uid('war mammoth'/'ME4', 'ME4:War Mammoth:war mammoth').
card_rarity('war mammoth'/'ME4', 'Common').
card_artist('war mammoth'/'ME4', 'Jeff A. Menges').
card_number('war mammoth'/'ME4', '172').
card_multiverse_id('war mammoth'/'ME4', '202465').

card_in_set('warp artifact', 'ME4').
card_original_type('warp artifact'/'ME4', 'Enchantment — Aura').
card_original_text('warp artifact'/'ME4', 'Enchant artifact\nAt the beginning of the upkeep of enchanted artifact\'s controller, Warp Artifact deals 1 damage to that player.').
card_image_name('warp artifact'/'ME4', 'warp artifact').
card_uid('warp artifact'/'ME4', 'ME4:Warp Artifact:warp artifact').
card_rarity('warp artifact'/'ME4', 'Common').
card_artist('warp artifact'/'ME4', 'Amy Weber').
card_number('warp artifact'/'ME4', '100').
card_multiverse_id('warp artifact'/'ME4', '221514').

card_in_set('water elemental', 'ME4').
card_original_type('water elemental'/'ME4', 'Creature — Elemental').
card_original_text('water elemental'/'ME4', '').
card_image_name('water elemental'/'ME4', 'water elemental').
card_uid('water elemental'/'ME4', 'ME4:Water Elemental:water elemental').
card_rarity('water elemental'/'ME4', 'Uncommon').
card_artist('water elemental'/'ME4', 'Jeff A. Menges').
card_number('water elemental'/'ME4', '70').
card_flavor_text('water elemental'/'ME4', 'Unpredictable as the sea itself, water elementals shift without warning from tranquility to tempest. Capricious and fickle, they flow restlessly from one shape to another, expressing their moods with their physical forms.').
card_multiverse_id('water elemental'/'ME4', '202415').

card_in_set('weakness', 'ME4').
card_original_type('weakness'/'ME4', 'Enchantment — Aura').
card_original_text('weakness'/'ME4', 'Enchant creature\nEnchanted creature gets -2/-1.').
card_image_name('weakness'/'ME4', 'weakness').
card_uid('weakness'/'ME4', 'ME4:Weakness:weakness').
card_rarity('weakness'/'ME4', 'Common').
card_artist('weakness'/'ME4', 'Douglas Shuler').
card_number('weakness'/'ME4', '101').
card_multiverse_id('weakness'/'ME4', '202413').

card_in_set('weakstone', 'ME4').
card_original_type('weakstone'/'ME4', 'Artifact').
card_original_text('weakstone'/'ME4', 'Attacking creatures get -1/-0.').
card_image_name('weakstone'/'ME4', 'weakstone').
card_uid('weakstone'/'ME4', 'ME4:Weakstone:weakstone').
card_rarity('weakstone'/'ME4', 'Uncommon').
card_artist('weakstone'/'ME4', 'Justin Hampton').
card_number('weakstone'/'ME4', '239').
card_flavor_text('weakstone'/'ME4', 'During the brothers\' childhood, Tocasia took them to explore the sacred cave of Koilos. There, in the Hall of Tagsin, Mishra discovered the mysterious weakstone.').
card_multiverse_id('weakstone'/'ME4', '202419').

card_in_set('wheel of fortune', 'ME4').
card_original_type('wheel of fortune'/'ME4', 'Sorcery').
card_original_text('wheel of fortune'/'ME4', 'Each player discards his or her hand and draws seven cards.').
card_image_name('wheel of fortune'/'ME4', 'wheel of fortune').
card_uid('wheel of fortune'/'ME4', 'ME4:Wheel of Fortune:wheel of fortune').
card_rarity('wheel of fortune'/'ME4', 'Rare').
card_artist('wheel of fortune'/'ME4', 'Daniel Gelon').
card_number('wheel of fortune'/'ME4', '140').
card_multiverse_id('wheel of fortune'/'ME4', '202558').

card_in_set('whiptail wurm', 'ME4').
card_original_type('whiptail wurm'/'ME4', 'Creature — Wurm').
card_original_text('whiptail wurm'/'ME4', '').
card_image_name('whiptail wurm'/'ME4', 'whiptail wurm').
card_uid('whiptail wurm'/'ME4', 'ME4:Whiptail Wurm:whiptail wurm').
card_rarity('whiptail wurm'/'ME4', 'Uncommon').
card_artist('whiptail wurm'/'ME4', 'Una Fricker').
card_number('whiptail wurm'/'ME4', '173').
card_flavor_text('whiptail wurm'/'ME4', 'It\'s hard to say for certain which end is more dangerous.').
card_multiverse_id('whiptail wurm'/'ME4', '202449').

card_in_set('white knight', 'ME4').
card_original_type('white knight'/'ME4', 'Creature — Human Knight').
card_original_text('white knight'/'ME4', 'First strike, protection from black').
card_image_name('white knight'/'ME4', 'white knight').
card_uid('white knight'/'ME4', 'ME4:White Knight:white knight').
card_rarity('white knight'/'ME4', 'Uncommon').
card_artist('white knight'/'ME4', 'Daniel Gelon').
card_number('white knight'/'ME4', '33').
card_flavor_text('white knight'/'ME4', 'Out of the blackness and stench of the engulfing swamp emerged a shimmering figure. Only the splattered armor and ichor-stained sword hinted at the unfathomable evil the knight had just laid waste.').
card_multiverse_id('white knight'/'ME4', '221569').

card_in_set('wicked pact', 'ME4').
card_original_type('wicked pact'/'ME4', 'Sorcery').
card_original_text('wicked pact'/'ME4', 'Destroy two target nonblack creatures. You lose 5 life.').
card_image_name('wicked pact'/'ME4', 'wicked pact').
card_uid('wicked pact'/'ME4', 'ME4:Wicked Pact:wicked pact').
card_rarity('wicked pact'/'ME4', 'Uncommon').
card_artist('wicked pact'/'ME4', 'Adam Rex').
card_number('wicked pact'/'ME4', '102').
card_multiverse_id('wicked pact'/'ME4', '202420').

card_in_set('wild aesthir', 'ME4').
card_original_type('wild aesthir'/'ME4', 'Creature — Bird').
card_original_text('wild aesthir'/'ME4', 'Flying, first strike\n{W}{W}: Wild Aesthir gets +2/+0 until end of turn. Activate this ability only once each turn.').
card_image_name('wild aesthir'/'ME4', 'wild aesthir').
card_uid('wild aesthir'/'ME4', 'ME4:Wild Aesthir:wild aesthir').
card_rarity('wild aesthir'/'ME4', 'Common').
card_artist('wild aesthir'/'ME4', 'Greg Simanson').
card_number('wild aesthir'/'ME4', '34').
card_flavor_text('wild aesthir'/'ME4', '\"High in the Karplusans, death is swift and razor-clawed.\"\n—Arna Kennerüd, skycaptain').
card_multiverse_id('wild aesthir'/'ME4', '202577').

card_in_set('wild griffin', 'ME4').
card_original_type('wild griffin'/'ME4', 'Creature — Griffin').
card_original_text('wild griffin'/'ME4', 'Flying').
card_image_name('wild griffin'/'ME4', 'wild griffin').
card_uid('wild griffin'/'ME4', 'ME4:Wild Griffin:wild griffin').
card_rarity('wild griffin'/'ME4', 'Common').
card_artist('wild griffin'/'ME4', 'Jeff Miracola').
card_number('wild griffin'/'ME4', '35').
card_flavor_text('wild griffin'/'ME4', 'Two little goblins, out in the sun. Down came a griffin, and then there was one.').
card_multiverse_id('wild griffin'/'ME4', '221639').

card_in_set('wild ox', 'ME4').
card_original_type('wild ox'/'ME4', 'Creature — Ox').
card_original_text('wild ox'/'ME4', 'Swampwalk').
card_image_name('wild ox'/'ME4', 'wild ox').
card_uid('wild ox'/'ME4', 'ME4:Wild Ox:wild ox').
card_rarity('wild ox'/'ME4', 'Uncommon').
card_artist('wild ox'/'ME4', 'Jeffrey R. Busch').
card_number('wild ox'/'ME4', '174').
card_flavor_text('wild ox'/'ME4', 'It has the run of the swamps, and it knows it.').
card_multiverse_id('wild ox'/'ME4', '221918').

card_in_set('wood elemental', 'ME4').
card_original_type('wood elemental'/'ME4', 'Creature — Elemental').
card_original_text('wood elemental'/'ME4', 'As Wood Elemental enters the battlefield, sacrifice any number of untapped Forests.\nWood Elemental\'s power and toughness are each equal to the number of Forests sacrificed as it entered the battlefield.').
card_image_name('wood elemental'/'ME4', 'wood elemental').
card_uid('wood elemental'/'ME4', 'ME4:Wood Elemental:wood elemental').
card_rarity('wood elemental'/'ME4', 'Rare').
card_artist('wood elemental'/'ME4', 'Brian Snõddy').
card_number('wood elemental'/'ME4', '175').
card_multiverse_id('wood elemental'/'ME4', '202555').

card_in_set('word of command', 'ME4').
card_original_type('word of command'/'ME4', 'Instant').
card_original_text('word of command'/'ME4', 'Look at target opponent\'s hand and choose a card from it. You control that player until Word of Command finishes resolving. The player plays that card if able. While doing so, the player can activate mana abilities only if they\'re from lands he or she controls and only if mana they produce is spent to activate other mana abilities of lands he or she controls and/or play that card. If the chosen card is cast as a spell, you control the player while that spell is resolving.').
card_image_name('word of command'/'ME4', 'word of command').
card_uid('word of command'/'ME4', 'ME4:Word of Command:word of command').
card_rarity('word of command'/'ME4', 'Rare').
card_artist('word of command'/'ME4', 'Jesper Myrfors').
card_number('word of command'/'ME4', '103').
card_multiverse_id('word of command'/'ME4', '202591').

card_in_set('xenic poltergeist', 'ME4').
card_original_type('xenic poltergeist'/'ME4', 'Creature — Spirit').
card_original_text('xenic poltergeist'/'ME4', '{T}: Until your next upkeep, target noncreature artifact becomes an artifact creature with power and toughness each equal to its converted mana cost.').
card_image_name('xenic poltergeist'/'ME4', 'xenic poltergeist').
card_uid('xenic poltergeist'/'ME4', 'ME4:Xenic Poltergeist:xenic poltergeist').
card_rarity('xenic poltergeist'/'ME4', 'Uncommon').
card_artist('xenic poltergeist'/'ME4', 'Dan Frazier').
card_number('xenic poltergeist'/'ME4', '104').
card_multiverse_id('xenic poltergeist'/'ME4', '202563').

card_in_set('yotian soldier', 'ME4').
card_original_type('yotian soldier'/'ME4', 'Artifact Creature — Soldier').
card_original_text('yotian soldier'/'ME4', 'Vigilance').
card_image_name('yotian soldier'/'ME4', 'yotian soldier').
card_uid('yotian soldier'/'ME4', 'ME4:Yotian Soldier:yotian soldier').
card_rarity('yotian soldier'/'ME4', 'Common').
card_artist('yotian soldier'/'ME4', 'Christopher Rush').
card_number('yotian soldier'/'ME4', '240').
card_flavor_text('yotian soldier'/'ME4', 'After Kroog was destroyed while most of its defenders were at his side, Urza vowed that none of his allies would ever need to fear for their own defense again, even while laying siege to a city far from their homes.').
card_multiverse_id('yotian soldier'/'ME4', '202548').

card_in_set('zombie master', 'ME4').
card_original_type('zombie master'/'ME4', 'Creature — Zombie').
card_original_text('zombie master'/'ME4', 'Other Zombie creatures have swampwalk.\nOther Zombies have \"{B}: Regenerate this permanent.\"').
card_image_name('zombie master'/'ME4', 'zombie master').
card_uid('zombie master'/'ME4', 'ME4:Zombie Master:zombie master').
card_rarity('zombie master'/'ME4', 'Uncommon').
card_artist('zombie master'/'ME4', 'Jeff A. Menges').
card_number('zombie master'/'ME4', '105').
card_flavor_text('zombie master'/'ME4', 'They say the Zombie Master controlled these foul creatures even before his own death, but now that he is one of them, nothing can make them betray him.').
card_multiverse_id('zombie master'/'ME4', '202440').
