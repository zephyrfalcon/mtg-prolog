% Duel Decks: Ajani vs. Nicol Bolas

set('DDH').
set_name('DDH', 'Duel Decks: Ajani vs. Nicol Bolas').
set_release_date('DDH', '2011-09-02').
set_border('DDH', 'black').
set_type('DDH', 'duel deck').

card_in_set('ageless entity', 'DDH').
card_original_type('ageless entity'/'DDH', 'Creature — Elemental').
card_original_text('ageless entity'/'DDH', 'Whenever you gain life, put that many +1/+1 counters on Ageless Entity.').
card_image_name('ageless entity'/'DDH', 'ageless entity').
card_uid('ageless entity'/'DDH', 'DDH:Ageless Entity:ageless entity').
card_rarity('ageless entity'/'DDH', 'Rare').
card_artist('ageless entity'/'DDH', 'Jeff Miracola').
card_number('ageless entity'/'DDH', '18').
card_flavor_text('ageless entity'/'DDH', 'Tel-Jilad\'s sworn protectors are the trolls, yet more fearsome protectors were created by the Tangle itself.').
card_multiverse_id('ageless entity'/'DDH', '249411').

card_in_set('agonizing demise', 'DDH').
card_original_type('agonizing demise'/'DDH', 'Instant').
card_original_text('agonizing demise'/'DDH', 'Kicker {1}{R} (You may pay an additional {1}{R} as you cast this spell.)\nDestroy target nonblack creature. It can\'t be regenerated. If Agonizing Demise was kicked, it deals damage equal to that creature\'s power to the creature\'s controller.').
card_image_name('agonizing demise'/'DDH', 'agonizing demise').
card_uid('agonizing demise'/'DDH', 'DDH:Agonizing Demise:agonizing demise').
card_rarity('agonizing demise'/'DDH', 'Common').
card_artist('agonizing demise'/'DDH', 'Mark Brill').
card_number('agonizing demise'/'DDH', '66').
card_multiverse_id('agonizing demise'/'DDH', '259268').

card_in_set('ajani vengeant', 'DDH').
card_original_type('ajani vengeant'/'DDH', 'Planeswalker — Ajani').
card_original_text('ajani vengeant'/'DDH', '+1: Target permanent doesn\'t untap during its controller\'s next untap step.\n-2: Ajani Vengeant deals 3 damage to target creature or player and you gain 3 life.\n-7: Destroy all lands target player controls.').
card_image_name('ajani vengeant'/'DDH', 'ajani vengeant').
card_uid('ajani vengeant'/'DDH', 'DDH:Ajani Vengeant:ajani vengeant').
card_rarity('ajani vengeant'/'DDH', 'Mythic Rare').
card_artist('ajani vengeant'/'DDH', 'Izzy').
card_number('ajani vengeant'/'DDH', '1').
card_multiverse_id('ajani vengeant'/'DDH', '266299').

card_in_set('ajani\'s mantra', 'DDH').
card_original_type('ajani\'s mantra'/'DDH', 'Enchantment').
card_original_text('ajani\'s mantra'/'DDH', 'At the beginning of your upkeep, you may gain 1 life.').
card_image_name('ajani\'s mantra'/'DDH', 'ajani\'s mantra').
card_uid('ajani\'s mantra'/'DDH', 'DDH:Ajani\'s Mantra:ajani\'s mantra').
card_rarity('ajani\'s mantra'/'DDH', 'Common').
card_artist('ajani\'s mantra'/'DDH', 'James Paick').
card_number('ajani\'s mantra'/'DDH', '22').
card_flavor_text('ajani\'s mantra'/'DDH', '\"He hasn\'t returned to the Cloud Forest. But I can still sense his calming presence.\"\n—Zaliki of Naya').
card_multiverse_id('ajani\'s mantra'/'DDH', '249398').

card_in_set('ajani\'s pridemate', 'DDH').
card_original_type('ajani\'s pridemate'/'DDH', 'Creature — Cat Soldier').
card_original_text('ajani\'s pridemate'/'DDH', 'Whenever you gain life, you may put a +1/+1 counter on Ajani\'s Pridemate. (For example, if an effect causes you to gain 3 life, you may put one +1/+1 counter on this creature.)').
card_image_name('ajani\'s pridemate'/'DDH', 'ajani\'s pridemate').
card_uid('ajani\'s pridemate'/'DDH', 'DDH:Ajani\'s Pridemate:ajani\'s pridemate').
card_rarity('ajani\'s pridemate'/'DDH', 'Uncommon').
card_artist('ajani\'s pridemate'/'DDH', 'Svetlin Velinov').
card_number('ajani\'s pridemate'/'DDH', '9').
card_flavor_text('ajani\'s pridemate'/'DDH', '\"When one of us prospers, the pride prospers.\"\n—Jazal Goldmane').
card_multiverse_id('ajani\'s pridemate'/'DDH', '249413').

card_in_set('behemoth sledge', 'DDH').
card_original_type('behemoth sledge'/'DDH', 'Artifact — Equipment').
card_original_text('behemoth sledge'/'DDH', 'Equipped creature gets +2/+2 and has lifelink and trample.\nEquip {3}').
card_image_name('behemoth sledge'/'DDH', 'behemoth sledge').
card_uid('behemoth sledge'/'DDH', 'DDH:Behemoth Sledge:behemoth sledge').
card_rarity('behemoth sledge'/'DDH', 'Uncommon').
card_artist('behemoth sledge'/'DDH', 'Dave Allsop').
card_number('behemoth sledge'/'DDH', '28').
card_flavor_text('behemoth sledge'/'DDH', '\"May this grant you all the strength of the behemoths and none of their serenity.\"\n—Ajani').
card_multiverse_id('behemoth sledge'/'DDH', '249396').

card_in_set('blazing specter', 'DDH').
card_original_type('blazing specter'/'DDH', 'Creature — Specter').
card_original_text('blazing specter'/'DDH', 'Flying, haste\nWhenever Blazing Specter deals combat damage to a player, that player discards a card.').
card_image_name('blazing specter'/'DDH', 'blazing specter').
card_uid('blazing specter'/'DDH', 'DDH:Blazing Specter:blazing specter').
card_rarity('blazing specter'/'DDH', 'Rare').
card_artist('blazing specter'/'DDH', 'Marc Fishman').
card_number('blazing specter'/'DDH', '52').
card_multiverse_id('blazing specter'/'DDH', '259274').

card_in_set('brackwater elemental', 'DDH').
card_original_type('brackwater elemental'/'DDH', 'Creature — Elemental').
card_original_text('brackwater elemental'/'DDH', 'When Brackwater Elemental attacks or blocks, sacrifice it at the beginning of the next end step.\nUnearth {2}{U} ({2}{U}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_image_name('brackwater elemental'/'DDH', 'brackwater elemental').
card_uid('brackwater elemental'/'DDH', 'DDH:Brackwater Elemental:brackwater elemental').
card_rarity('brackwater elemental'/'DDH', 'Common').
card_artist('brackwater elemental'/'DDH', 'Thomas M. Baxa').
card_number('brackwater elemental'/'DDH', '46').
card_multiverse_id('brackwater elemental'/'DDH', '249390').

card_in_set('briarhorn', 'DDH').
card_original_type('briarhorn'/'DDH', 'Creature — Elemental').
card_original_text('briarhorn'/'DDH', 'Flash (You may cast this spell any time you could cast an instant.)\nWhen Briarhorn enters the battlefield, target creature gets +3/+3 until end of turn.\nEvoke {1}{G} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_image_name('briarhorn'/'DDH', 'briarhorn').
card_uid('briarhorn'/'DDH', 'DDH:Briarhorn:briarhorn').
card_rarity('briarhorn'/'DDH', 'Uncommon').
card_artist('briarhorn'/'DDH', 'Nils Hamm').
card_number('briarhorn'/'DDH', '14').
card_multiverse_id('briarhorn'/'DDH', '249383').

card_in_set('canyon wildcat', 'DDH').
card_original_type('canyon wildcat'/'DDH', 'Creature — Cat').
card_original_text('canyon wildcat'/'DDH', 'Mountainwalk').
card_image_name('canyon wildcat'/'DDH', 'canyon wildcat').
card_uid('canyon wildcat'/'DDH', 'DDH:Canyon Wildcat:canyon wildcat').
card_rarity('canyon wildcat'/'DDH', 'Common').
card_artist('canyon wildcat'/'DDH', 'Gary Leach').
card_number('canyon wildcat'/'DDH', '6').
card_flavor_text('canyon wildcat'/'DDH', '\"Relative of yours?\" Ertai teased. Mirri simply sneered.').
card_multiverse_id('canyon wildcat'/'DDH', '249412').

card_in_set('countersquall', 'DDH').
card_original_type('countersquall'/'DDH', 'Instant').
card_original_text('countersquall'/'DDH', 'Counter target noncreature spell. Its controller loses 2 life.').
card_image_name('countersquall'/'DDH', 'countersquall').
card_uid('countersquall'/'DDH', 'DDH:Countersquall:countersquall').
card_rarity('countersquall'/'DDH', 'Uncommon').
card_artist('countersquall'/'DDH', 'Anthony Francisco').
card_number('countersquall'/'DDH', '59').
card_flavor_text('countersquall'/'DDH', '\"The essence of every world, every spell, and every thought is power. Nothing else matters, because nothing else exists.\"\n—Nicol Bolas').
card_multiverse_id('countersquall'/'DDH', '249406').

card_in_set('cruel ultimatum', 'DDH').
card_original_type('cruel ultimatum'/'DDH', 'Sorcery').
card_original_text('cruel ultimatum'/'DDH', 'Target opponent sacrifices a creature, discards three cards, then loses 5 life. You return a creature card from your graveyard to your hand, draw three cards, then gain 5 life.').
card_image_name('cruel ultimatum'/'DDH', 'cruel ultimatum').
card_uid('cruel ultimatum'/'DDH', 'DDH:Cruel Ultimatum:cruel ultimatum').
card_rarity('cruel ultimatum'/'DDH', 'Rare').
card_artist('cruel ultimatum'/'DDH', 'Ralph Horsley').
card_number('cruel ultimatum'/'DDH', '69').
card_flavor_text('cruel ultimatum'/'DDH', 'There is always a greater power.').
card_multiverse_id('cruel ultimatum'/'DDH', '259262').

card_in_set('crumbling necropolis', 'DDH').
card_original_type('crumbling necropolis'/'DDH', 'Land').
card_original_text('crumbling necropolis'/'DDH', 'Crumbling Necropolis enters the battlefield tapped.\n{T}: Add {U}, {B}, or {R} to your mana pool.').
card_image_name('crumbling necropolis'/'DDH', 'crumbling necropolis').
card_uid('crumbling necropolis'/'DDH', 'DDH:Crumbling Necropolis:crumbling necropolis').
card_rarity('crumbling necropolis'/'DDH', 'Uncommon').
card_artist('crumbling necropolis'/'DDH', 'Dave Kendall').
card_number('crumbling necropolis'/'DDH', '74').
card_flavor_text('crumbling necropolis'/'DDH', '\"They say the ruins of Sedraxis were once a shining capital in Vithia. Now it is a blight, a place to be avoided by the living.\"\n—Olcot, Rider of Joffik').
card_multiverse_id('crumbling necropolis'/'DDH', '259265').

card_in_set('deep analysis', 'DDH').
card_original_type('deep analysis'/'DDH', 'Sorcery').
card_original_text('deep analysis'/'DDH', 'Target player draws two cards.\nFlashback—{1}{U}, Pay 3 life. (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('deep analysis'/'DDH', 'deep analysis').
card_uid('deep analysis'/'DDH', 'DDH:Deep Analysis:deep analysis').
card_rarity('deep analysis'/'DDH', 'Uncommon').
card_artist('deep analysis'/'DDH', 'Jesper Ejsing').
card_number('deep analysis'/'DDH', '65').
card_flavor_text('deep analysis'/'DDH', '\"No fact escapes me. Why do you think you can?\"').
card_multiverse_id('deep analysis'/'DDH', '249365').

card_in_set('dimir cutpurse', 'DDH').
card_original_type('dimir cutpurse'/'DDH', 'Creature — Spirit').
card_original_text('dimir cutpurse'/'DDH', 'Whenever Dimir Cutpurse deals combat damage to a player, that player discards a card and you draw a card.').
card_image_name('dimir cutpurse'/'DDH', 'dimir cutpurse').
card_uid('dimir cutpurse'/'DDH', 'DDH:Dimir Cutpurse:dimir cutpurse').
card_rarity('dimir cutpurse'/'DDH', 'Rare').
card_artist('dimir cutpurse'/'DDH', 'Kev Walker').
card_number('dimir cutpurse'/'DDH', '49').
card_flavor_text('dimir cutpurse'/'DDH', 'Other guilds demand tolls from those who travel their territories, but not House Dimir. It takes its share secretly, one coin purse at a time.').
card_multiverse_id('dimir cutpurse'/'DDH', '259269').

card_in_set('elder mastery', 'DDH').
card_original_type('elder mastery'/'DDH', 'Enchantment — Aura').
card_original_text('elder mastery'/'DDH', 'Enchant creature\nEnchanted creature gets +3/+3 and has flying.\nWhenever enchanted creature deals damage to a player, that player discards two cards.').
card_image_name('elder mastery'/'DDH', 'elder mastery').
card_uid('elder mastery'/'DDH', 'DDH:Elder Mastery:elder mastery').
card_rarity('elder mastery'/'DDH', 'Uncommon').
card_artist('elder mastery'/'DDH', 'Dave Allsop').
card_number('elder mastery'/'DDH', '68').
card_flavor_text('elder mastery'/'DDH', 'Taste his power, hunger for his command.').
card_multiverse_id('elder mastery'/'DDH', '249407').

card_in_set('essence warden', 'DDH').
card_original_type('essence warden'/'DDH', 'Creature — Elf Shaman').
card_original_text('essence warden'/'DDH', 'Whenever another creature enters the battlefield, you gain 1 life.').
card_image_name('essence warden'/'DDH', 'essence warden').
card_uid('essence warden'/'DDH', 'DDH:Essence Warden:essence warden').
card_rarity('essence warden'/'DDH', 'Common').
card_artist('essence warden'/'DDH', 'Terese Nielsen').
card_number('essence warden'/'DDH', '3').
card_flavor_text('essence warden'/'DDH', '\"The more our numbers grow, the more I gain hope that Volrath and his cursed stronghold will one day fall.\"\n—Eladamri, Lord of Leaves').
card_multiverse_id('essence warden'/'DDH', '249374').

card_in_set('evolving wilds', 'DDH').
card_original_type('evolving wilds'/'DDH', 'Land').
card_original_text('evolving wilds'/'DDH', '{T}, Sacrifice Evolving Wilds: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('evolving wilds'/'DDH', 'evolving wilds').
card_uid('evolving wilds'/'DDH', 'DDH:Evolving Wilds:evolving wilds').
card_rarity('evolving wilds'/'DDH', 'Common').
card_artist('evolving wilds'/'DDH', 'Steven Belledin').
card_number('evolving wilds'/'DDH', '32').
card_flavor_text('evolving wilds'/'DDH', 'Every world is an organism, able to grow new lands. Some just do it faster than others.').
card_multiverse_id('evolving wilds'/'DDH', '249395').

card_in_set('fall', 'DDH').
card_original_type('fall'/'DDH', 'Sorcery').
card_original_text('fall'/'DDH', 'Return target creature card from a graveyard and target creature on the battlefield to their owners\' hands.\n//\nFall\n{B}{R}\nSorcery\nTarget player reveals two cards at random from his or her hand, then discards each nonland card revealed this way.').
card_image_name('fall'/'DDH', 'risefall').
card_uid('fall'/'DDH', 'DDH:Fall:risefall').
card_rarity('fall'/'DDH', 'Uncommon').
card_artist('fall'/'DDH', 'Pete Venters').
card_number('fall'/'DDH', '73b').
card_multiverse_id('fall'/'DDH', '259266').

card_in_set('fire-field ogre', 'DDH').
card_original_type('fire-field ogre'/'DDH', 'Creature — Ogre Mutant').
card_original_text('fire-field ogre'/'DDH', 'First strike\nUnearth {U}{B}{R} ({U}{B}{R}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_image_name('fire-field ogre'/'DDH', 'fire-field ogre').
card_uid('fire-field ogre'/'DDH', 'DDH:Fire-Field Ogre:fire-field ogre').
card_rarity('fire-field ogre'/'DDH', 'Uncommon').
card_artist('fire-field ogre'/'DDH', 'Mitch Cotie').
card_number('fire-field ogre'/'DDH', '53').
card_multiverse_id('fire-field ogre'/'DDH', '259270').

card_in_set('firemane angel', 'DDH').
card_original_type('firemane angel'/'DDH', 'Creature — Angel').
card_original_text('firemane angel'/'DDH', 'Flying, first strike\nAt the beginning of your upkeep, if Firemane Angel is in your graveyard or on the battlefield, you may gain 1 life.\n{6}{R}{R}{W}{W}: Return Firemane Angel from your graveyard to the battlefield. Activate this ability only during your upkeep.').
card_image_name('firemane angel'/'DDH', 'firemane angel').
card_uid('firemane angel'/'DDH', 'DDH:Firemane Angel:firemane angel').
card_rarity('firemane angel'/'DDH', 'Rare').
card_artist('firemane angel'/'DDH', 'Matt Cavotta').
card_number('firemane angel'/'DDH', '21').
card_multiverse_id('firemane angel'/'DDH', '249366').

card_in_set('fleetfoot panther', 'DDH').
card_original_type('fleetfoot panther'/'DDH', 'Creature — Cat').
card_original_text('fleetfoot panther'/'DDH', 'Flash (You may cast this spell any time you could cast an instant.)\nWhen Fleetfoot Panther enters the battlefield, return a green or white creature you control to its owner\'s hand.').
card_image_name('fleetfoot panther'/'DDH', 'fleetfoot panther').
card_uid('fleetfoot panther'/'DDH', 'DDH:Fleetfoot Panther:fleetfoot panther').
card_rarity('fleetfoot panther'/'DDH', 'Uncommon').
card_artist('fleetfoot panther'/'DDH', 'Mark Brill').
card_number('fleetfoot panther'/'DDH', '12').
card_multiverse_id('fleetfoot panther'/'DDH', '249391').

card_in_set('forest', 'DDH').
card_original_type('forest'/'DDH', 'Basic Land — Forest').
card_original_text('forest'/'DDH', 'G').
card_image_name('forest'/'DDH', 'forest1').
card_uid('forest'/'DDH', 'DDH:Forest:forest1').
card_rarity('forest'/'DDH', 'Basic Land').
card_artist('forest'/'DDH', 'Zoltan Boros & Gabor Szikszai').
card_number('forest'/'DDH', '38').
card_multiverse_id('forest'/'DDH', '262651').

card_in_set('forest', 'DDH').
card_original_type('forest'/'DDH', 'Basic Land — Forest').
card_original_text('forest'/'DDH', 'G').
card_image_name('forest'/'DDH', 'forest2').
card_uid('forest'/'DDH', 'DDH:Forest:forest2').
card_rarity('forest'/'DDH', 'Basic Land').
card_artist('forest'/'DDH', 'Zoltan Boros & Gabor Szikszai').
card_number('forest'/'DDH', '39').
card_multiverse_id('forest'/'DDH', '259285').

card_in_set('graypelt refuge', 'DDH').
card_original_type('graypelt refuge'/'DDH', 'Land').
card_original_text('graypelt refuge'/'DDH', 'Graypelt Refuge enters the battlefield tapped.\nWhen Graypelt Refuge enters the battlefield, you gain 1 life.\n{T}: Add {G} or {W} to your mana pool.').
card_image_name('graypelt refuge'/'DDH', 'graypelt refuge').
card_uid('graypelt refuge'/'DDH', 'DDH:Graypelt Refuge:graypelt refuge').
card_rarity('graypelt refuge'/'DDH', 'Uncommon').
card_artist('graypelt refuge'/'DDH', 'Philip Straub').
card_number('graypelt refuge'/'DDH', '33').
card_multiverse_id('graypelt refuge'/'DDH', '249379').

card_in_set('grazing gladehart', 'DDH').
card_original_type('grazing gladehart'/'DDH', 'Creature — Antelope').
card_original_text('grazing gladehart'/'DDH', 'Landfall — Whenever a land enters the battlefield under your control, you may gain 2 life.').
card_image_name('grazing gladehart'/'DDH', 'grazing gladehart').
card_uid('grazing gladehart'/'DDH', 'DDH:Grazing Gladehart:grazing gladehart').
card_rarity('grazing gladehart'/'DDH', 'Common').
card_artist('grazing gladehart'/'DDH', 'Ryan Pancoast').
card_number('grazing gladehart'/'DDH', '11').
card_flavor_text('grazing gladehart'/'DDH', '\"Don\'t be fooled. If it were as docile as it looks, it would\'ve died off long ago.\"\n—Yon Basrel, Oran-Rief survivalist').
card_multiverse_id('grazing gladehart'/'DDH', '249378').

card_in_set('griffin guide', 'DDH').
card_original_type('griffin guide'/'DDH', 'Enchantment — Aura').
card_original_text('griffin guide'/'DDH', 'Enchant creature\nEnchanted creature gets +2/+2 and has flying.\nWhen enchanted creature dies, put a 2/2 white Griffin creature token with flying onto the battlefield.').
card_image_name('griffin guide'/'DDH', 'griffin guide').
card_uid('griffin guide'/'DDH', 'DDH:Griffin Guide:griffin guide').
card_rarity('griffin guide'/'DDH', 'Uncommon').
card_artist('griffin guide'/'DDH', 'Jim Nelson').
card_number('griffin guide'/'DDH', '25').
card_multiverse_id('griffin guide'/'DDH', '249372').

card_in_set('grixis charm', 'DDH').
card_original_type('grixis charm'/'DDH', 'Instant').
card_original_text('grixis charm'/'DDH', 'Choose one — Return target permanent to its owner\'s hand; or target creature gets -4/-4 until end of turn; or creatures you control get +2/+0 until end of turn.').
card_image_name('grixis charm'/'DDH', 'grixis charm').
card_uid('grixis charm'/'DDH', 'DDH:Grixis Charm:grixis charm').
card_rarity('grixis charm'/'DDH', 'Uncommon').
card_artist('grixis charm'/'DDH', 'Lars Grant-West').
card_number('grixis charm'/'DDH', '63').
card_flavor_text('grixis charm'/'DDH', '\"So many choices. Shall I choose loathing, hate, or malice today?\"\n—Eliza of the Keep').
card_multiverse_id('grixis charm'/'DDH', '259271').

card_in_set('hellfire mongrel', 'DDH').
card_original_type('hellfire mongrel'/'DDH', 'Creature — Elemental Hound').
card_original_text('hellfire mongrel'/'DDH', 'At the beginning of each opponent\'s upkeep, if that player has two or fewer cards in hand, Hellfire Mongrel deals 2 damage to him or her.').
card_image_name('hellfire mongrel'/'DDH', 'hellfire mongrel').
card_uid('hellfire mongrel'/'DDH', 'DDH:Hellfire Mongrel:hellfire mongrel').
card_rarity('hellfire mongrel'/'DDH', 'Uncommon').
card_artist('hellfire mongrel'/'DDH', 'Dan Scott').
card_number('hellfire mongrel'/'DDH', '48').
card_flavor_text('hellfire mongrel'/'DDH', 'There is no fondness between the hound and its master. There is only a common appreciation of the hunt.').
card_multiverse_id('hellfire mongrel'/'DDH', '249408').

card_in_set('icy manipulator', 'DDH').
card_original_type('icy manipulator'/'DDH', 'Artifact').
card_original_text('icy manipulator'/'DDH', '{1}, {T}: Tap target artifact, creature, or land.').
card_image_name('icy manipulator'/'DDH', 'icy manipulator').
card_uid('icy manipulator'/'DDH', 'DDH:Icy Manipulator:icy manipulator').
card_rarity('icy manipulator'/'DDH', 'Uncommon').
card_artist('icy manipulator'/'DDH', 'Matt Cavotta').
card_number('icy manipulator'/'DDH', '64').
card_flavor_text('icy manipulator'/'DDH', 'In fire there is the spark of chaos and destruction, the seed of life. In ice there is perfect tranquility, perfect order, and the silence of death.').
card_multiverse_id('icy manipulator'/'DDH', '249384').

card_in_set('igneous pouncer', 'DDH').
card_original_type('igneous pouncer'/'DDH', 'Creature — Elemental').
card_original_text('igneous pouncer'/'DDH', 'Haste\nSwampcycling {2}, mountaincycling {2} ({2}, Discard this card: Search your library for a Swamp or Mountain card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('igneous pouncer'/'DDH', 'igneous pouncer').
card_uid('igneous pouncer'/'DDH', 'DDH:Igneous Pouncer:igneous pouncer').
card_rarity('igneous pouncer'/'DDH', 'Common').
card_artist('igneous pouncer'/'DDH', 'Chippy').
card_number('igneous pouncer'/'DDH', '57').
card_multiverse_id('igneous pouncer'/'DDH', '249410').

card_in_set('island', 'DDH').
card_original_type('island'/'DDH', 'Basic Land — Island').
card_original_text('island'/'DDH', 'U').
card_image_name('island'/'DDH', 'island').
card_uid('island'/'DDH', 'DDH:Island:island').
card_rarity('island'/'DDH', 'Basic Land').
card_artist('island'/'DDH', 'Mark Tedin').
card_number('island'/'DDH', '79').
card_multiverse_id('island'/'DDH', '259287').

card_in_set('jade mage', 'DDH').
card_original_type('jade mage'/'DDH', 'Creature — Human Shaman').
card_original_text('jade mage'/'DDH', '{2}{G}: Put a 1/1 green Saproling creature token onto the battlefield.').
card_image_name('jade mage'/'DDH', 'jade mage').
card_uid('jade mage'/'DDH', 'DDH:Jade Mage:jade mage').
card_rarity('jade mage'/'DDH', 'Uncommon').
card_artist('jade mage'/'DDH', 'Izzy').
card_number('jade mage'/'DDH', '7').
card_flavor_text('jade mage'/'DDH', '\"We are one with the wild things. Life blooms from our fingertips and nature responds to our summons.\"\n—Jade creed').
card_multiverse_id('jade mage'/'DDH', '249397').

card_in_set('jhessian zombies', 'DDH').
card_original_type('jhessian zombies'/'DDH', 'Creature — Zombie').
card_original_text('jhessian zombies'/'DDH', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\nIslandcycling {2}, swampcycling {2} ({2}, Discard this card: Search your library for an Island or Swamp card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('jhessian zombies'/'DDH', 'jhessian zombies').
card_uid('jhessian zombies'/'DDH', 'DDH:Jhessian Zombies:jhessian zombies').
card_rarity('jhessian zombies'/'DDH', 'Common').
card_artist('jhessian zombies'/'DDH', 'Ash Wood').
card_number('jhessian zombies'/'DDH', '56').
card_multiverse_id('jhessian zombies'/'DDH', '259277').

card_in_set('jungle shrine', 'DDH').
card_original_type('jungle shrine'/'DDH', 'Land').
card_original_text('jungle shrine'/'DDH', 'Jungle Shrine enters the battlefield tapped.\n{T}: Add {R}, {G}, or {W} to your mana pool.').
card_image_name('jungle shrine'/'DDH', 'jungle shrine').
card_uid('jungle shrine'/'DDH', 'DDH:Jungle Shrine:jungle shrine').
card_rarity('jungle shrine'/'DDH', 'Uncommon').
card_artist('jungle shrine'/'DDH', 'Wayne Reynolds').
card_number('jungle shrine'/'DDH', '34').
card_flavor_text('jungle shrine'/'DDH', 'On Naya, ambition and treachery are scarce, hunted nearly to extinction by the awe owed to terrestrial gods.').
card_multiverse_id('jungle shrine'/'DDH', '249403').

card_in_set('kazandu refuge', 'DDH').
card_original_type('kazandu refuge'/'DDH', 'Land').
card_original_text('kazandu refuge'/'DDH', 'Kazandu Refuge enters the battlefield tapped.\nWhen Kazandu Refuge enters the battlefield, you gain 1 life.\n{T}: Add {R} or {G} to your mana pool.').
card_image_name('kazandu refuge'/'DDH', 'kazandu refuge').
card_uid('kazandu refuge'/'DDH', 'DDH:Kazandu Refuge:kazandu refuge').
card_rarity('kazandu refuge'/'DDH', 'Uncommon').
card_artist('kazandu refuge'/'DDH', 'Franz Vohwinkel').
card_number('kazandu refuge'/'DDH', '35').
card_multiverse_id('kazandu refuge'/'DDH', '249402').

card_in_set('kird ape', 'DDH').
card_original_type('kird ape'/'DDH', 'Creature — Ape').
card_original_text('kird ape'/'DDH', 'Kird Ape gets +1/+2 as long as you control a Forest.').
card_image_name('kird ape'/'DDH', 'kird ape').
card_uid('kird ape'/'DDH', 'DDH:Kird Ape:kird ape').
card_rarity('kird ape'/'DDH', 'Uncommon').
card_artist('kird ape'/'DDH', 'Terese Nielsen').
card_number('kird ape'/'DDH', '2').
card_flavor_text('kird ape'/'DDH', 'Apes can be gentle, contemplative beasts—but context is everything.').
card_multiverse_id('kird ape'/'DDH', '249382').

card_in_set('lead the stampede', 'DDH').
card_original_type('lead the stampede'/'DDH', 'Sorcery').
card_original_text('lead the stampede'/'DDH', 'Look at the top five cards of your library. You may reveal any number of creature cards from among them and put the revealed cards into your hand. Put the rest on the bottom of your library in any order.').
card_image_name('lead the stampede'/'DDH', 'lead the stampede').
card_uid('lead the stampede'/'DDH', 'DDH:Lead the Stampede:lead the stampede').
card_rarity('lead the stampede'/'DDH', 'Uncommon').
card_artist('lead the stampede'/'DDH', 'Efrem Palacios').
card_number('lead the stampede'/'DDH', '24').
card_multiverse_id('lead the stampede'/'DDH', '249369').

card_in_set('lightning helix', 'DDH').
card_original_type('lightning helix'/'DDH', 'Instant').
card_original_text('lightning helix'/'DDH', 'Lightning Helix deals 3 damage to target creature or player and you gain 3 life.').
card_image_name('lightning helix'/'DDH', 'lightning helix').
card_uid('lightning helix'/'DDH', 'DDH:Lightning Helix:lightning helix').
card_rarity('lightning helix'/'DDH', 'Uncommon').
card_artist('lightning helix'/'DDH', 'Raymond Swanland').
card_number('lightning helix'/'DDH', '23').
card_flavor_text('lightning helix'/'DDH', '\"Rage is not the answer. Rage followed by fitting vengeance is the answer.\"\n—Ajani').
card_multiverse_id('lightning helix'/'DDH', '249386').

card_in_set('loam lion', 'DDH').
card_original_type('loam lion'/'DDH', 'Creature — Cat').
card_original_text('loam lion'/'DDH', 'Loam Lion gets +1/+2 as long as you control a Forest.').
card_image_name('loam lion'/'DDH', 'loam lion').
card_uid('loam lion'/'DDH', 'DDH:Loam Lion:loam lion').
card_rarity('loam lion'/'DDH', 'Uncommon').
card_artist('loam lion'/'DDH', 'Daniel Ljunggren').
card_number('loam lion'/'DDH', '5').
card_flavor_text('loam lion'/'DDH', 'In Zendikar, today\'s grassland could be tomorrow\'s jungle, and hunting grounds change as quickly as the weather.').
card_multiverse_id('loam lion'/'DDH', '249377').

card_in_set('loxodon hierarch', 'DDH').
card_original_type('loxodon hierarch'/'DDH', 'Creature — Elephant Cleric').
card_original_text('loxodon hierarch'/'DDH', 'When Loxodon Hierarch enters the battlefield, you gain 4 life.\n{G}{W}, Sacrifice Loxodon Hierarch: Regenerate each creature you control.').
card_image_name('loxodon hierarch'/'DDH', 'loxodon hierarch').
card_uid('loxodon hierarch'/'DDH', 'DDH:Loxodon Hierarch:loxodon hierarch').
card_rarity('loxodon hierarch'/'DDH', 'Rare').
card_artist('loxodon hierarch'/'DDH', 'Kev Walker').
card_number('loxodon hierarch'/'DDH', '15').
card_flavor_text('loxodon hierarch'/'DDH', '\"I have lived long, and I remember how this city once was. If my death serves to bring back the Ravnica in my memory, then so be it.\"').
card_multiverse_id('loxodon hierarch'/'DDH', '249414').

card_in_set('malice', 'DDH').
card_original_type('malice'/'DDH', 'Instant').
card_original_text('malice'/'DDH', 'Counter target noncreature spell.\n//\nMalice\n{3}{B}\nInstant\nDestroy target nonblack creature. It can\'t be regenerated.').
card_image_name('malice'/'DDH', 'spitemalice').
card_uid('malice'/'DDH', 'DDH:Malice:spitemalice').
card_rarity('malice'/'DDH', 'Uncommon').
card_artist('malice'/'DDH', 'David Martin').
card_number('malice'/'DDH', '71b').
card_multiverse_id('malice'/'DDH', '249394').

card_in_set('marisi\'s twinclaws', 'DDH').
card_original_type('marisi\'s twinclaws'/'DDH', 'Creature — Cat Warrior').
card_original_text('marisi\'s twinclaws'/'DDH', 'Double strike').
card_image_name('marisi\'s twinclaws'/'DDH', 'marisi\'s twinclaws').
card_uid('marisi\'s twinclaws'/'DDH', 'DDH:Marisi\'s Twinclaws:marisi\'s twinclaws').
card_rarity('marisi\'s twinclaws'/'DDH', 'Uncommon').
card_artist('marisi\'s twinclaws'/'DDH', 'Izzy').
card_number('marisi\'s twinclaws'/'DDH', '17').
card_flavor_text('marisi\'s twinclaws'/'DDH', '\"Looks like we\'re completely outnumbered.\"\n\"Lucky for us. I thought we\'d be bored.\"').
card_multiverse_id('marisi\'s twinclaws'/'DDH', '249375').

card_in_set('morgue toad', 'DDH').
card_original_type('morgue toad'/'DDH', 'Creature — Frog').
card_original_text('morgue toad'/'DDH', 'Sacrifice Morgue Toad: Add {U}{R} to your mana pool.').
card_image_name('morgue toad'/'DDH', 'morgue toad').
card_uid('morgue toad'/'DDH', 'DDH:Morgue Toad:morgue toad').
card_rarity('morgue toad'/'DDH', 'Common').
card_artist('morgue toad'/'DDH', 'Franz Vohwinkel').
card_number('morgue toad'/'DDH', '47').
card_flavor_text('morgue toad'/'DDH', '\"The toads of Urborg aren\'t fast, powerful, or pleasant, but they have their uses.\"\n—Ertai').
card_multiverse_id('morgue toad'/'DDH', '259264').

card_in_set('moroii', 'DDH').
card_original_type('moroii'/'DDH', 'Creature — Vampire').
card_original_text('moroii'/'DDH', 'Flying\nAt the beginning of your upkeep, you lose 1 life.').
card_image_name('moroii'/'DDH', 'moroii').
card_uid('moroii'/'DDH', 'DDH:Moroii:moroii').
card_rarity('moroii'/'DDH', 'Uncommon').
card_artist('moroii'/'DDH', 'Dan Scott').
card_number('moroii'/'DDH', '51').
card_flavor_text('moroii'/'DDH', '\"Touched by moroii\"\n—Undercity slang meaning \"to grow old\"').
card_multiverse_id('moroii'/'DDH', '249367').

card_in_set('mountain', 'DDH').
card_original_type('mountain'/'DDH', 'Basic Land — Mountain').
card_original_text('mountain'/'DDH', 'R').
card_image_name('mountain'/'DDH', 'mountain1').
card_uid('mountain'/'DDH', 'DDH:Mountain:mountain1').
card_rarity('mountain'/'DDH', 'Basic Land').
card_artist('mountain'/'DDH', 'Zoltan Boros & Gabor Szikszai').
card_number('mountain'/'DDH', '41').
card_multiverse_id('mountain'/'DDH', '259284').

card_in_set('mountain', 'DDH').
card_original_type('mountain'/'DDH', 'Basic Land — Mountain').
card_original_text('mountain'/'DDH', 'R').
card_image_name('mountain'/'DDH', 'mountain2').
card_uid('mountain'/'DDH', 'DDH:Mountain:mountain2').
card_rarity('mountain'/'DDH', 'Basic Land').
card_artist('mountain'/'DDH', 'Mark Tedin').
card_number('mountain'/'DDH', '80').
card_multiverse_id('mountain'/'DDH', '259286').

card_in_set('nacatl hunt-pride', 'DDH').
card_original_type('nacatl hunt-pride'/'DDH', 'Creature — Cat Warrior').
card_original_text('nacatl hunt-pride'/'DDH', 'Vigilance\n{R}, {T}: Target creature can\'t block this turn.\n{G}, {T}: Target creature blocks this turn if able.').
card_image_name('nacatl hunt-pride'/'DDH', 'nacatl hunt-pride').
card_uid('nacatl hunt-pride'/'DDH', 'DDH:Nacatl Hunt-Pride:nacatl hunt-pride').
card_rarity('nacatl hunt-pride'/'DDH', 'Uncommon').
card_artist('nacatl hunt-pride'/'DDH', 'Steve Prescott').
card_number('nacatl hunt-pride'/'DDH', '20').
card_flavor_text('nacatl hunt-pride'/'DDH', '\"We must hunt and kill the dragon before it can return to raze our ancient city.\"').
card_multiverse_id('nacatl hunt-pride'/'DDH', '249409').

card_in_set('naya charm', 'DDH').
card_original_type('naya charm'/'DDH', 'Instant').
card_original_text('naya charm'/'DDH', 'Choose one — Naya Charm deals 3 damage to target creature; or return target card from a graveyard to its owner\'s hand; or tap all creatures target player controls.').
card_image_name('naya charm'/'DDH', 'naya charm').
card_uid('naya charm'/'DDH', 'DDH:Naya Charm:naya charm').
card_rarity('naya charm'/'DDH', 'Uncommon').
card_artist('naya charm'/'DDH', 'Jesper Ejsing').
card_number('naya charm'/'DDH', '29').
card_flavor_text('naya charm'/'DDH', 'Deep in nature\'s core lies a potential unsullied by greed or civilization.').
card_multiverse_id('naya charm'/'DDH', '249385').

card_in_set('nicol bolas, planeswalker', 'DDH').
card_original_type('nicol bolas, planeswalker'/'DDH', 'Planeswalker — Bolas').
card_original_text('nicol bolas, planeswalker'/'DDH', '+3: Destroy target noncreature permanent.\n-2: Gain control of target creature.\n-9: Nicol Bolas, Planeswalker deals 7 damage to target player. That player discards seven cards, then sacrifices seven permanents.').
card_image_name('nicol bolas, planeswalker'/'DDH', 'nicol bolas, planeswalker').
card_uid('nicol bolas, planeswalker'/'DDH', 'DDH:Nicol Bolas, Planeswalker:nicol bolas, planeswalker').
card_rarity('nicol bolas, planeswalker'/'DDH', 'Mythic Rare').
card_artist('nicol bolas, planeswalker'/'DDH', 'Izzy').
card_number('nicol bolas, planeswalker'/'DDH', '42').
card_multiverse_id('nicol bolas, planeswalker'/'DDH', '266154').

card_in_set('nightscape familiar', 'DDH').
card_original_type('nightscape familiar'/'DDH', 'Creature — Zombie').
card_original_text('nightscape familiar'/'DDH', 'Blue spells and red spells you cast cost {1} less to cast.\n{1}{B}: Regenerate Nightscape Familiar.').
card_image_name('nightscape familiar'/'DDH', 'nightscape familiar').
card_uid('nightscape familiar'/'DDH', 'DDH:Nightscape Familiar:nightscape familiar').
card_rarity('nightscape familiar'/'DDH', 'Common').
card_artist('nightscape familiar'/'DDH', 'Jeff Easley').
card_number('nightscape familiar'/'DDH', '44').
card_flavor_text('nightscape familiar'/'DDH', 'Nightscape masters don\'t stop at raising the spirit of a fallen battlemage. They raise the flesh along with it.').
card_multiverse_id('nightscape familiar'/'DDH', '259263').

card_in_set('obelisk of grixis', 'DDH').
card_original_type('obelisk of grixis'/'DDH', 'Artifact').
card_original_text('obelisk of grixis'/'DDH', '{T}: Add {U}, {B}, or {R} to your mana pool.').
card_image_name('obelisk of grixis'/'DDH', 'obelisk of grixis').
card_uid('obelisk of grixis'/'DDH', 'DDH:Obelisk of Grixis:obelisk of grixis').
card_rarity('obelisk of grixis'/'DDH', 'Common').
card_artist('obelisk of grixis'/'DDH', 'Nils Hamm').
card_number('obelisk of grixis'/'DDH', '60').
card_flavor_text('obelisk of grixis'/'DDH', 'Like most features of Grixis, the obelisks that remain from the time of Alara now exist only for dark exploitation.').
card_multiverse_id('obelisk of grixis'/'DDH', '259279').

card_in_set('ogre savant', 'DDH').
card_original_type('ogre savant'/'DDH', 'Creature — Ogre Wizard').
card_original_text('ogre savant'/'DDH', 'When Ogre Savant enters the battlefield, if {U} was spent to cast Ogre Savant, return target creature to its owner\'s hand.').
card_image_name('ogre savant'/'DDH', 'ogre savant').
card_uid('ogre savant'/'DDH', 'DDH:Ogre Savant:ogre savant').
card_rarity('ogre savant'/'DDH', 'Common').
card_artist('ogre savant'/'DDH', 'Paolo Parente').
card_number('ogre savant'/'DDH', '55').
card_flavor_text('ogre savant'/'DDH', 'He\'s an oxymoron.').
card_multiverse_id('ogre savant'/'DDH', '249387').

card_in_set('pain', 'DDH').
card_original_type('pain'/'DDH', 'Sorcery').
card_original_text('pain'/'DDH', 'Target player discards a card.\n//\nSuffering\n{3}{R}\nSorcery\nDestroy target land.').
card_image_name('pain'/'DDH', 'painsuffering').
card_uid('pain'/'DDH', 'DDH:Pain:painsuffering').
card_rarity('pain'/'DDH', 'Uncommon').
card_artist('pain'/'DDH', 'David Martin').
card_number('pain'/'DDH', '72a').
card_multiverse_id('pain'/'DDH', '259281').

card_in_set('plains', 'DDH').
card_original_type('plains'/'DDH', 'Basic Land — Plains').
card_original_text('plains'/'DDH', 'W').
card_image_name('plains'/'DDH', 'plains').
card_uid('plains'/'DDH', 'DDH:Plains:plains').
card_rarity('plains'/'DDH', 'Basic Land').
card_artist('plains'/'DDH', 'Zoltan Boros & Gabor Szikszai').
card_number('plains'/'DDH', '40').
card_multiverse_id('plains'/'DDH', '249376').

card_in_set('pride of lions', 'DDH').
card_original_type('pride of lions'/'DDH', 'Creature — Cat').
card_original_text('pride of lions'/'DDH', 'You may have Pride of Lions assign its combat damage as though it weren\'t blocked.').
card_image_name('pride of lions'/'DDH', 'pride of lions').
card_uid('pride of lions'/'DDH', 'DDH:Pride of Lions:pride of lions').
card_rarity('pride of lions'/'DDH', 'Uncommon').
card_artist('pride of lions'/'DDH', 'Carl Critchlow').
card_number('pride of lions'/'DDH', '19').
card_flavor_text('pride of lions'/'DDH', 'There\'s only one thing worse than a hungry lion—many hungry lions.').
card_multiverse_id('pride of lions'/'DDH', '249371').

card_in_set('profane command', 'DDH').
card_original_type('profane command'/'DDH', 'Sorcery').
card_original_text('profane command'/'DDH', 'Choose two — Target player loses X life; or return target creature card with converted mana cost X or less from your graveyard to the battlefield; or target creature gets -X/-X until end of turn; or up to X target creatures gain fear until end of turn. (They can\'t be blocked except by artifact creatures and/or black creatures.)').
card_image_name('profane command'/'DDH', 'profane command').
card_uid('profane command'/'DDH', 'DDH:Profane Command:profane command').
card_rarity('profane command'/'DDH', 'Rare').
card_artist('profane command'/'DDH', 'Wayne England').
card_number('profane command'/'DDH', '70').
card_multiverse_id('profane command'/'DDH', '259275').

card_in_set('qasali pridemage', 'DDH').
card_original_type('qasali pridemage'/'DDH', 'Creature — Cat Wizard').
card_original_text('qasali pridemage'/'DDH', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\n{1}, Sacrifice Qasali Pridemage: Destroy target artifact or enchantment.').
card_image_name('qasali pridemage'/'DDH', 'qasali pridemage').
card_uid('qasali pridemage'/'DDH', 'DDH:Qasali Pridemage:qasali pridemage').
card_rarity('qasali pridemage'/'DDH', 'Common').
card_artist('qasali pridemage'/'DDH', 'Chris Rahn').
card_number('qasali pridemage'/'DDH', '10').
card_flavor_text('qasali pridemage'/'DDH', 'An elder in one pride, of the Sigiled caste in another.').
card_multiverse_id('qasali pridemage'/'DDH', '249405').

card_in_set('recoil', 'DDH').
card_original_type('recoil'/'DDH', 'Instant').
card_original_text('recoil'/'DDH', 'Return target permanent to its owner\'s hand. Then that player discards a card.').
card_image_name('recoil'/'DDH', 'recoil').
card_uid('recoil'/'DDH', 'DDH:Recoil:recoil').
card_rarity('recoil'/'DDH', 'Common').
card_artist('recoil'/'DDH', 'Alan Pollack').
card_number('recoil'/'DDH', '61').
card_flavor_text('recoil'/'DDH', 'Anything sent into a plagued world is bound to come back infected.').
card_multiverse_id('recoil'/'DDH', '266305').

card_in_set('recumbent bliss', 'DDH').
card_original_type('recumbent bliss'/'DDH', 'Enchantment — Aura').
card_original_text('recumbent bliss'/'DDH', 'Enchant creature\nEnchanted creature can\'t attack or block.\nAt the beginning of your upkeep, you may gain 1 life.').
card_image_name('recumbent bliss'/'DDH', 'recumbent bliss').
card_uid('recumbent bliss'/'DDH', 'DDH:Recumbent Bliss:recumbent bliss').
card_rarity('recumbent bliss'/'DDH', 'Common').
card_artist('recumbent bliss'/'DDH', 'Todd Lockwood').
card_number('recumbent bliss'/'DDH', '26').
card_flavor_text('recumbent bliss'/'DDH', 'Kithkin somnomancers enjoy the peaceful dreams wafting from their victims.').
card_multiverse_id('recumbent bliss'/'DDH', '249400').

card_in_set('rise', 'DDH').
card_original_type('rise'/'DDH', 'Sorcery').
card_original_text('rise'/'DDH', 'Return target creature card from a graveyard and target creature on the battlefield to their owners\' hands.\n//\nFall\n{B}{R}\nSorcery\nTarget player reveals two cards at random from his or her hand, then discards each nonland card revealed this way.').
card_image_name('rise'/'DDH', 'risefall').
card_uid('rise'/'DDH', 'DDH:Rise:risefall').
card_rarity('rise'/'DDH', 'Uncommon').
card_artist('rise'/'DDH', 'Pete Venters').
card_number('rise'/'DDH', '73a').
card_multiverse_id('rise'/'DDH', '259266').

card_in_set('rupture spire', 'DDH').
card_original_type('rupture spire'/'DDH', 'Land').
card_original_text('rupture spire'/'DDH', 'Rupture Spire enters the battlefield tapped.\nWhen Rupture Spire enters the battlefield, sacrifice it unless you pay {1}.\n{T}: Add one mana of any color to your mana pool.').
card_image_name('rupture spire'/'DDH', 'rupture spire').
card_uid('rupture spire'/'DDH', 'DDH:Rupture Spire:rupture spire').
card_rarity('rupture spire'/'DDH', 'Common').
card_artist('rupture spire'/'DDH', 'Jaime Jones').
card_number('rupture spire'/'DDH', '75').
card_multiverse_id('rupture spire'/'DDH', '259282').

card_in_set('sapseep forest', 'DDH').
card_original_type('sapseep forest'/'DDH', 'Land — Forest').
card_original_text('sapseep forest'/'DDH', '({T}: Add {G} to your mana pool.)\nSapseep Forest enters the battlefield tapped.\n{G}, {T}: You gain 1 life. Activate this ability only if you control two or more green permanents.').
card_image_name('sapseep forest'/'DDH', 'sapseep forest').
card_uid('sapseep forest'/'DDH', 'DDH:Sapseep Forest:sapseep forest').
card_rarity('sapseep forest'/'DDH', 'Uncommon').
card_artist('sapseep forest'/'DDH', 'Aleksi Briclot').
card_number('sapseep forest'/'DDH', '36').
card_multiverse_id('sapseep forest'/'DDH', '249389').

card_in_set('searing meditation', 'DDH').
card_original_type('searing meditation'/'DDH', 'Enchantment').
card_original_text('searing meditation'/'DDH', 'Whenever you gain life, you may pay {2}. If you do, Searing Meditation deals 2 damage to target creature or player.').
card_image_name('searing meditation'/'DDH', 'searing meditation').
card_uid('searing meditation'/'DDH', 'DDH:Searing Meditation:searing meditation').
card_rarity('searing meditation'/'DDH', 'Rare').
card_artist('searing meditation'/'DDH', 'Dave Dorman').
card_number('searing meditation'/'DDH', '27').
card_flavor_text('searing meditation'/'DDH', '\"When I meditate I see the world as it should be. All that does not fit, I remove.\"\n—Alovnek, Boros guildmage').
card_multiverse_id('searing meditation'/'DDH', '249380').

card_in_set('shriekmaw', 'DDH').
card_original_type('shriekmaw'/'DDH', 'Creature — Elemental').
card_original_text('shriekmaw'/'DDH', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\nWhen Shriekmaw enters the battlefield, destroy target nonartifact, nonblack creature.\nEvoke {1}{B} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_image_name('shriekmaw'/'DDH', 'shriekmaw').
card_uid('shriekmaw'/'DDH', 'DDH:Shriekmaw:shriekmaw').
card_rarity('shriekmaw'/'DDH', 'Uncommon').
card_artist('shriekmaw'/'DDH', 'Steve Prescott').
card_number('shriekmaw'/'DDH', '54').
card_multiverse_id('shriekmaw'/'DDH', '259272').

card_in_set('slave of bolas', 'DDH').
card_original_type('slave of bolas'/'DDH', 'Sorcery').
card_original_text('slave of bolas'/'DDH', 'Gain control of target creature. Untap that creature. It gains haste until end of turn. Sacrifice it at the beginning of the next end step.').
card_image_name('slave of bolas'/'DDH', 'slave of bolas').
card_uid('slave of bolas'/'DDH', 'DDH:Slave of Bolas:slave of bolas').
card_rarity('slave of bolas'/'DDH', 'Uncommon').
card_artist('slave of bolas'/'DDH', 'Steve Argyle').
card_number('slave of bolas'/'DDH', '67').
card_flavor_text('slave of bolas'/'DDH', 'Nicol Bolas doesn\'t distinguish between servants and victims.').
card_multiverse_id('slave of bolas'/'DDH', '259276').

card_in_set('slavering nulls', 'DDH').
card_original_type('slavering nulls'/'DDH', 'Creature — Goblin Zombie').
card_original_text('slavering nulls'/'DDH', 'Whenever Slavering Nulls deals combat damage to a player, if you control a Swamp, you may have that player discard a card.').
card_image_name('slavering nulls'/'DDH', 'slavering nulls').
card_uid('slavering nulls'/'DDH', 'DDH:Slavering Nulls:slavering nulls').
card_rarity('slavering nulls'/'DDH', 'Uncommon').
card_artist('slavering nulls'/'DDH', 'Dave Kendall').
card_number('slavering nulls'/'DDH', '45').
card_flavor_text('slavering nulls'/'DDH', 'Having lost their minds, they now want yours.').
card_multiverse_id('slavering nulls'/'DDH', '249404').

card_in_set('spite', 'DDH').
card_original_type('spite'/'DDH', 'Instant').
card_original_text('spite'/'DDH', 'Counter target noncreature spell.\n//\nMalice\n{3}{B}\nInstant\nDestroy target nonblack creature. It can\'t be regenerated.').
card_image_name('spite'/'DDH', 'spitemalice').
card_uid('spite'/'DDH', 'DDH:Spite:spitemalice').
card_rarity('spite'/'DDH', 'Uncommon').
card_artist('spite'/'DDH', 'David Martin').
card_number('spite'/'DDH', '71a').
card_multiverse_id('spite'/'DDH', '249394').

card_in_set('spitemare', 'DDH').
card_original_type('spitemare'/'DDH', 'Creature — Elemental').
card_original_text('spitemare'/'DDH', 'Whenever Spitemare is dealt damage, it deals that much damage to target creature or player.').
card_image_name('spitemare'/'DDH', 'spitemare').
card_uid('spitemare'/'DDH', 'DDH:Spitemare:spitemare').
card_rarity('spitemare'/'DDH', 'Uncommon').
card_artist('spitemare'/'DDH', 'Matt Cavotta').
card_number('spitemare'/'DDH', '16').
card_flavor_text('spitemare'/'DDH', '\"I knew a creature carved from a dream of wild freedom. Any attempt to leash or exploit it failed, and at terrible cost.\"\n—Ashling').
card_multiverse_id('spitemare'/'DDH', '249388').

card_in_set('steamcore weird', 'DDH').
card_original_type('steamcore weird'/'DDH', 'Creature — Weird').
card_original_text('steamcore weird'/'DDH', 'When Steamcore Weird enters the battlefield, if {R} was spent to cast Steamcore Weird, it deals 2 damage to target creature or player.').
card_image_name('steamcore weird'/'DDH', 'steamcore weird').
card_uid('steamcore weird'/'DDH', 'DDH:Steamcore Weird:steamcore weird').
card_rarity('steamcore weird'/'DDH', 'Common').
card_artist('steamcore weird'/'DDH', 'Justin Norman').
card_number('steamcore weird'/'DDH', '50').
card_flavor_text('steamcore weird'/'DDH', 'Like many Izzet creations, weirds are based on wild contradictions yet somehow manage to work.').
card_multiverse_id('steamcore weird'/'DDH', '259273').

card_in_set('suffering', 'DDH').
card_original_type('suffering'/'DDH', 'Sorcery').
card_original_text('suffering'/'DDH', 'Target player discards a card.\n//\nSuffering\n{3}{R}\nSorcery\nDestroy target land.').
card_image_name('suffering'/'DDH', 'painsuffering').
card_uid('suffering'/'DDH', 'DDH:Suffering:painsuffering').
card_rarity('suffering'/'DDH', 'Uncommon').
card_artist('suffering'/'DDH', 'David Martin').
card_number('suffering'/'DDH', '72b').
card_multiverse_id('suffering'/'DDH', '259281').

card_in_set('surveilling sprite', 'DDH').
card_original_type('surveilling sprite'/'DDH', 'Creature — Faerie Rogue').
card_original_text('surveilling sprite'/'DDH', 'Flying\nWhen Surveilling Sprite dies, you may draw a card.').
card_image_name('surveilling sprite'/'DDH', 'surveilling sprite').
card_uid('surveilling sprite'/'DDH', 'DDH:Surveilling Sprite:surveilling sprite').
card_rarity('surveilling sprite'/'DDH', 'Common').
card_artist('surveilling sprite'/'DDH', 'Terese Nielsen').
card_number('surveilling sprite'/'DDH', '43').
card_flavor_text('surveilling sprite'/'DDH', 'Their natural curiosity, combined with a knack for trespassing, makes sprites excellent spies.').
card_multiverse_id('surveilling sprite'/'DDH', '259267').

card_in_set('swamp', 'DDH').
card_original_type('swamp'/'DDH', 'Basic Land — Swamp').
card_original_text('swamp'/'DDH', 'B').
card_image_name('swamp'/'DDH', 'swamp1').
card_uid('swamp'/'DDH', 'DDH:Swamp:swamp1').
card_rarity('swamp'/'DDH', 'Basic Land').
card_artist('swamp'/'DDH', 'Mark Tedin').
card_number('swamp'/'DDH', '77').
card_multiverse_id('swamp'/'DDH', '262652').

card_in_set('swamp', 'DDH').
card_original_type('swamp'/'DDH', 'Basic Land — Swamp').
card_original_text('swamp'/'DDH', 'B').
card_image_name('swamp'/'DDH', 'swamp2').
card_uid('swamp'/'DDH', 'DDH:Swamp:swamp2').
card_rarity('swamp'/'DDH', 'Basic Land').
card_artist('swamp'/'DDH', 'Mark Tedin').
card_number('swamp'/'DDH', '78').
card_multiverse_id('swamp'/'DDH', '259283').

card_in_set('sylvan bounty', 'DDH').
card_original_type('sylvan bounty'/'DDH', 'Instant').
card_original_text('sylvan bounty'/'DDH', 'Target player gains 8 life.\nBasic landcycling {1}{G} ({1}{G}, Discard this card: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('sylvan bounty'/'DDH', 'sylvan bounty').
card_uid('sylvan bounty'/'DDH', 'DDH:Sylvan Bounty:sylvan bounty').
card_rarity('sylvan bounty'/'DDH', 'Common').
card_artist('sylvan bounty'/'DDH', 'Chris Rahn').
card_number('sylvan bounty'/'DDH', '30').
card_flavor_text('sylvan bounty'/'DDH', 'Some who scouted new lands chose to stay.').
card_multiverse_id('sylvan bounty'/'DDH', '249392').

card_in_set('sylvan ranger', 'DDH').
card_original_type('sylvan ranger'/'DDH', 'Creature — Elf Scout').
card_original_text('sylvan ranger'/'DDH', 'When Sylvan Ranger enters the battlefield, you may search your library for a basic land card, reveal it, put it into your hand, then shuffle your library.').
card_image_name('sylvan ranger'/'DDH', 'sylvan ranger').
card_uid('sylvan ranger'/'DDH', 'DDH:Sylvan Ranger:sylvan ranger').
card_rarity('sylvan ranger'/'DDH', 'Common').
card_artist('sylvan ranger'/'DDH', 'Christopher Moeller').
card_number('sylvan ranger'/'DDH', '8').
card_flavor_text('sylvan ranger'/'DDH', '\"Not all paths are found on the forest floor.\"').
card_multiverse_id('sylvan ranger'/'DDH', '249399').

card_in_set('terramorphic expanse', 'DDH').
card_original_type('terramorphic expanse'/'DDH', 'Land').
card_original_text('terramorphic expanse'/'DDH', '{T}, Sacrifice Terramorphic Expanse: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('terramorphic expanse'/'DDH', 'terramorphic expanse').
card_uid('terramorphic expanse'/'DDH', 'DDH:Terramorphic Expanse:terramorphic expanse').
card_rarity('terramorphic expanse'/'DDH', 'Common').
card_artist('terramorphic expanse'/'DDH', 'Dan Scott').
card_number('terramorphic expanse'/'DDH', '76').
card_flavor_text('terramorphic expanse'/'DDH', 'Take two steps north into the unsettled future, south into the unquiet past, east into the present day, or west into the great unknown.').
card_multiverse_id('terramorphic expanse'/'DDH', '249381').

card_in_set('titanic ultimatum', 'DDH').
card_original_type('titanic ultimatum'/'DDH', 'Sorcery').
card_original_text('titanic ultimatum'/'DDH', 'Until end of turn, creatures you control get +5/+5 and gain first strike, lifelink, and trample.').
card_image_name('titanic ultimatum'/'DDH', 'titanic ultimatum').
card_uid('titanic ultimatum'/'DDH', 'DDH:Titanic Ultimatum:titanic ultimatum').
card_rarity('titanic ultimatum'/'DDH', 'Rare').
card_artist('titanic ultimatum'/'DDH', 'Steve Prescott').
card_number('titanic ultimatum'/'DDH', '31').
card_flavor_text('titanic ultimatum'/'DDH', '\"Retribution is best delivered by claws and rage, with both magnified.\"\n—Ajani').
card_multiverse_id('titanic ultimatum'/'DDH', '249370').

card_in_set('undermine', 'DDH').
card_original_type('undermine'/'DDH', 'Instant').
card_original_text('undermine'/'DDH', 'Counter target spell. Its controller loses 3 life.').
card_image_name('undermine'/'DDH', 'undermine').
card_uid('undermine'/'DDH', 'DDH:Undermine:undermine').
card_rarity('undermine'/'DDH', 'Rare').
card_artist('undermine'/'DDH', 'Massimilano Frezzato').
card_number('undermine'/'DDH', '62').
card_flavor_text('undermine'/'DDH', '\"Which would you like first, the insult or the injury?\"').
card_multiverse_id('undermine'/'DDH', '259278').

card_in_set('vapor snag', 'DDH').
card_original_type('vapor snag'/'DDH', 'Instant').
card_original_text('vapor snag'/'DDH', 'Return target creature to its owner\'s hand. Its controller loses 1 life.').
card_image_name('vapor snag'/'DDH', 'vapor snag').
card_uid('vapor snag'/'DDH', 'DDH:Vapor Snag:vapor snag').
card_rarity('vapor snag'/'DDH', 'Common').
card_artist('vapor snag'/'DDH', 'Raymond Swanland').
card_number('vapor snag'/'DDH', '58').
card_flavor_text('vapor snag'/'DDH', '\"This creature is inadequate. Send it to the splicers for innovation.\"\n—Malcator, Executor of Synthesis').
card_multiverse_id('vapor snag'/'DDH', '249373').

card_in_set('vitu-ghazi, the city-tree', 'DDH').
card_original_type('vitu-ghazi, the city-tree'/'DDH', 'Land').
card_original_text('vitu-ghazi, the city-tree'/'DDH', '{T}: Add {1} to your mana pool.\n{2}{G}{W}, {T}: Put a 1/1 green Saproling creature token onto the battlefield.').
card_image_name('vitu-ghazi, the city-tree'/'DDH', 'vitu-ghazi, the city-tree').
card_uid('vitu-ghazi, the city-tree'/'DDH', 'DDH:Vitu-Ghazi, the City-Tree:vitu-ghazi, the city-tree').
card_rarity('vitu-ghazi, the city-tree'/'DDH', 'Uncommon').
card_artist('vitu-ghazi, the city-tree'/'DDH', 'Martina Pilcerova').
card_number('vitu-ghazi, the city-tree'/'DDH', '37').
card_flavor_text('vitu-ghazi, the city-tree'/'DDH', 'In the autumn, she casts her seeds across the streets below, and come spring, her children rise in service to the Conclave.').
card_multiverse_id('vitu-ghazi, the city-tree'/'DDH', '249393').

card_in_set('wild nacatl', 'DDH').
card_original_type('wild nacatl'/'DDH', 'Creature — Cat Warrior').
card_original_text('wild nacatl'/'DDH', 'Wild Nacatl gets +1/+1 as long as you control a Mountain.\nWild Nacatl gets +1/+1 as long as you control a Plains.').
card_image_name('wild nacatl'/'DDH', 'wild nacatl').
card_uid('wild nacatl'/'DDH', 'DDH:Wild Nacatl:wild nacatl').
card_rarity('wild nacatl'/'DDH', 'Common').
card_artist('wild nacatl'/'DDH', 'Wayne Reynolds').
card_number('wild nacatl'/'DDH', '4').
card_flavor_text('wild nacatl'/'DDH', '\"The Cloud Nacatl sit and think, a bunch of soft paws. We are the Claws of Marisi, stalking, pouncing, drawing blood.\"').
card_multiverse_id('wild nacatl'/'DDH', '249401').

card_in_set('woolly thoctar', 'DDH').
card_original_type('woolly thoctar'/'DDH', 'Creature — Beast').
card_original_text('woolly thoctar'/'DDH', '').
card_image_name('woolly thoctar'/'DDH', 'woolly thoctar').
card_uid('woolly thoctar'/'DDH', 'DDH:Woolly Thoctar:woolly thoctar').
card_rarity('woolly thoctar'/'DDH', 'Uncommon').
card_artist('woolly thoctar'/'DDH', 'Wayne Reynolds').
card_number('woolly thoctar'/'DDH', '13').
card_flavor_text('woolly thoctar'/'DDH', 'One of the most ferocious and deadly gargantuans, the thoctar never sees its worshippers, but it often awakens surrounded by gifts and sacrifices.').
card_multiverse_id('woolly thoctar'/'DDH', '249368').
