% Premium Deck Series: Slivers

set('H09').
set_name('H09', 'Premium Deck Series: Slivers').
set_release_date('H09', '2009-11-20').
set_border('H09', 'black').
set_type('H09', 'premium deck').

card_in_set('acidic sliver', 'H09').
card_original_type('acidic sliver'/'H09', 'Creature — Sliver').
card_original_text('acidic sliver'/'H09', 'All Slivers have \"{2}, Sacrifice this permanent: This permanent deals 2 damage to target creature or player.\"').
card_image_name('acidic sliver'/'H09', 'acidic sliver').
card_uid('acidic sliver'/'H09', 'H09:Acidic Sliver:acidic sliver').
card_rarity('acidic sliver'/'H09', 'Uncommon').
card_artist('acidic sliver'/'H09', 'Jeff Miracola').
card_number('acidic sliver'/'H09', '13').
card_flavor_text('acidic sliver'/'H09', 'The first sliver burst against the cave wall, and others piled in behind to deepen the new tunnel.').
card_multiverse_id('acidic sliver'/'H09', '207897').

card_in_set('amoeboid changeling', 'H09').
card_original_type('amoeboid changeling'/'H09', 'Creature — Shapeshifter').
card_original_text('amoeboid changeling'/'H09', 'Changeling (This card is every creature type at all times.)\n{T}: Target creature gains all creature types until end of turn.\n{T}: Target creature loses all creature types until end of turn.').
card_image_name('amoeboid changeling'/'H09', 'amoeboid changeling').
card_uid('amoeboid changeling'/'H09', 'H09:Amoeboid Changeling:amoeboid changeling').
card_rarity('amoeboid changeling'/'H09', 'Common').
card_artist('amoeboid changeling'/'H09', 'Nils Hamm').
card_number('amoeboid changeling'/'H09', '3').
card_multiverse_id('amoeboid changeling'/'H09', '207921').

card_in_set('ancient ziggurat', 'H09').
card_original_type('ancient ziggurat'/'H09', 'Land').
card_original_text('ancient ziggurat'/'H09', '{T}: Add one mana of any color to your mana pool. Spend this mana only to cast creature spells.').
card_image_name('ancient ziggurat'/'H09', 'ancient ziggurat').
card_uid('ancient ziggurat'/'H09', 'H09:Ancient Ziggurat:ancient ziggurat').
card_rarity('ancient ziggurat'/'H09', 'Uncommon').
card_artist('ancient ziggurat'/'H09', 'John Avon').
card_number('ancient ziggurat'/'H09', '31').
card_flavor_text('ancient ziggurat'/'H09', 'Built in honor of Alara\'s creatures, the ziggurat vanished long ago. When Progenitus awakened, the temple emerged again.').
card_multiverse_id('ancient ziggurat'/'H09', '207930').

card_in_set('aphetto dredging', 'H09').
card_original_type('aphetto dredging'/'H09', 'Sorcery').
card_original_text('aphetto dredging'/'H09', 'Return up to three target creature cards of the creature type of your choice from your graveyard to your hand.').
card_image_name('aphetto dredging'/'H09', 'aphetto dredging').
card_uid('aphetto dredging'/'H09', 'H09:Aphetto Dredging:aphetto dredging').
card_rarity('aphetto dredging'/'H09', 'Common').
card_artist('aphetto dredging'/'H09', 'Monte Michael Moore').
card_number('aphetto dredging'/'H09', '28').
card_flavor_text('aphetto dredging'/'H09', 'Phage became both executioner and savior, helping others to the same rebirth she had found.').
card_multiverse_id('aphetto dredging'/'H09', '207925').

card_in_set('armor sliver', 'H09').
card_original_type('armor sliver'/'H09', 'Creature — Sliver').
card_original_text('armor sliver'/'H09', 'All Sliver creatures have \"{2}: This creature gets +0/+1 until end of turn.\"').
card_image_name('armor sliver'/'H09', 'armor sliver').
card_uid('armor sliver'/'H09', 'H09:Armor Sliver:armor sliver').
card_rarity('armor sliver'/'H09', 'Uncommon').
card_artist('armor sliver'/'H09', 'Scott Kirschner').
card_number('armor sliver'/'H09', '16').
card_flavor_text('armor sliver'/'H09', 'Hanna: \"We must learn how they protect each other.\"\nMirri: \"After they\'re done trying to kill us, all right?\"').
card_multiverse_id('armor sliver'/'H09', '207898').

card_in_set('barbed sliver', 'H09').
card_original_type('barbed sliver'/'H09', 'Creature — Sliver').
card_original_text('barbed sliver'/'H09', 'All Sliver creatures have \"{2}: This creature gets +1/+0 until end of turn.\"').
card_image_name('barbed sliver'/'H09', 'barbed sliver').
card_uid('barbed sliver'/'H09', 'H09:Barbed Sliver:barbed sliver').
card_rarity('barbed sliver'/'H09', 'Uncommon').
card_artist('barbed sliver'/'H09', 'Scott Kirschner').
card_number('barbed sliver'/'H09', '18').
card_flavor_text('barbed sliver'/'H09', 'Spans of spines leapt from one sliver to the next, forming a deadly hedge around the Weatherlight.').
card_multiverse_id('barbed sliver'/'H09', '207899').

card_in_set('brood sliver', 'H09').
card_original_type('brood sliver'/'H09', 'Creature — Sliver').
card_original_text('brood sliver'/'H09', 'Whenever a Sliver deals combat damage to a player, its controller may put a 1/1 colorless Sliver creature token onto the battlefield.').
card_image_name('brood sliver'/'H09', 'brood sliver').
card_uid('brood sliver'/'H09', 'H09:Brood Sliver:brood sliver').
card_rarity('brood sliver'/'H09', 'Rare').
card_artist('brood sliver'/'H09', 'Ron Spears').
card_number('brood sliver'/'H09', '22').
card_flavor_text('brood sliver'/'H09', 'Within weeks, more slivers nested in Otaria than ever existed on Rath.').
card_multiverse_id('brood sliver'/'H09', '208039').

card_in_set('clot sliver', 'H09').
card_original_type('clot sliver'/'H09', 'Creature — Sliver').
card_original_text('clot sliver'/'H09', 'All Slivers have \"{2}: Regenerate this permanent.\"').
card_image_name('clot sliver'/'H09', 'clot sliver').
card_uid('clot sliver'/'H09', 'H09:Clot Sliver:clot sliver').
card_rarity('clot sliver'/'H09', 'Common').
card_artist('clot sliver'/'H09', 'Jeff Laubenstein').
card_number('clot sliver'/'H09', '5').
card_flavor_text('clot sliver'/'H09', '\"One would think I would be accustomed to unexpected returns.\"\n—Hanna, Weatherlight navigator').
card_multiverse_id('clot sliver'/'H09', '207900').

card_in_set('coat of arms', 'H09').
card_original_type('coat of arms'/'H09', 'Artifact').
card_original_text('coat of arms'/'H09', 'Each creature gets +1/+1 for each other creature on the battlefield that shares at least one creature type with it.').
card_image_name('coat of arms'/'H09', 'coat of arms').
card_uid('coat of arms'/'H09', 'H09:Coat of Arms:coat of arms').
card_rarity('coat of arms'/'H09', 'Rare').
card_artist('coat of arms'/'H09', 'Scott M. Fischer').
card_number('coat of arms'/'H09', '29').
card_flavor_text('coat of arms'/'H09', '\"Hup, two, three, four,\nDunno how to count no more.\"\n—Mogg march').
card_multiverse_id('coat of arms'/'H09', '207927').

card_in_set('crystalline sliver', 'H09').
card_original_type('crystalline sliver'/'H09', 'Creature — Sliver').
card_original_text('crystalline sliver'/'H09', 'All Slivers have shroud.').
card_image_name('crystalline sliver'/'H09', 'crystalline sliver').
card_uid('crystalline sliver'/'H09', 'H09:Crystalline Sliver:crystalline sliver').
card_rarity('crystalline sliver'/'H09', 'Uncommon').
card_artist('crystalline sliver'/'H09', 'L. A. Williams').
card_number('crystalline sliver'/'H09', '11').
card_flavor_text('crystalline sliver'/'H09', 'Bred as living shields, these slivers have proven unruly—they know they cannot be caught.').
card_multiverse_id('crystalline sliver'/'H09', '207901').

card_in_set('distant melody', 'H09').
card_original_type('distant melody'/'H09', 'Sorcery').
card_original_text('distant melody'/'H09', 'Choose a creature type. Draw a card for each permanent you control of that type.').
card_image_name('distant melody'/'H09', 'distant melody').
card_uid('distant melody'/'H09', 'H09:Distant Melody:distant melody').
card_rarity('distant melody'/'H09', 'Common').
card_artist('distant melody'/'H09', 'Omar Rayyan').
card_number('distant melody'/'H09', '27').
card_flavor_text('distant melody'/'H09', 'Oona\'s song is like a twisted dinner chime. All the faeries return home, but it is Oona who feasts—on the stolen dreams and rumors they serve her.').
card_multiverse_id('distant melody'/'H09', '207926').

card_in_set('forest', 'H09').
card_original_type('forest'/'H09', 'Basic Land — Forest').
card_original_text('forest'/'H09', 'G').
card_image_name('forest'/'H09', 'forest').
card_uid('forest'/'H09', 'H09:Forest:forest').
card_rarity('forest'/'H09', 'Basic Land').
card_artist('forest'/'H09', 'Douglas Shuler').
card_number('forest'/'H09', '41').
card_multiverse_id('forest'/'H09', '207934').

card_in_set('frenzy sliver', 'H09').
card_original_type('frenzy sliver'/'H09', 'Creature — Sliver').
card_original_text('frenzy sliver'/'H09', 'All Sliver creatures have frenzy 1. (Whenever a Sliver attacks and isn\'t blocked, it gets +1/+0 until end of turn.)').
card_image_name('frenzy sliver'/'H09', 'frenzy sliver').
card_uid('frenzy sliver'/'H09', 'H09:Frenzy Sliver:frenzy sliver').
card_rarity('frenzy sliver'/'H09', 'Common').
card_artist('frenzy sliver'/'H09', 'Glen Angus').
card_number('frenzy sliver'/'H09', '6').
card_flavor_text('frenzy sliver'/'H09', 'The Arturan conjurers took the strange, alien fossil and patterned their own twisted creations in its image.').
card_multiverse_id('frenzy sliver'/'H09', '207902').

card_in_set('fungus sliver', 'H09').
card_original_type('fungus sliver'/'H09', 'Creature — Fungus Sliver').
card_original_text('fungus sliver'/'H09', 'All Sliver creatures have \"Whenever this creature is dealt damage, put a +1/+1 counter on it.\"').
card_image_name('fungus sliver'/'H09', 'fungus sliver').
card_uid('fungus sliver'/'H09', 'H09:Fungus Sliver:fungus sliver').
card_rarity('fungus sliver'/'H09', 'Rare').
card_artist('fungus sliver'/'H09', 'Daniel Gelon').
card_number('fungus sliver'/'H09', '21').
card_flavor_text('fungus sliver'/'H09', '\"When a sliver of this breed enters the hive, the others claw each other in frenzied fits, thereby ensuring their rapid growth.\"\n—Rukarumel, field journal').
card_multiverse_id('fungus sliver'/'H09', '207903').

card_in_set('fury sliver', 'H09').
card_original_type('fury sliver'/'H09', 'Creature — Sliver').
card_original_text('fury sliver'/'H09', 'All Sliver creatures have double strike.').
card_image_name('fury sliver'/'H09', 'fury sliver').
card_uid('fury sliver'/'H09', 'H09:Fury Sliver:fury sliver').
card_rarity('fury sliver'/'H09', 'Uncommon').
card_artist('fury sliver'/'H09', 'Paolo Parente').
card_number('fury sliver'/'H09', '25').
card_flavor_text('fury sliver'/'H09', '\"A rift opened, and our arrows were abruptly stilled. To move was to push the world. But the sliver\'s claw still twitched, red wounds appeared in Thed\'s chest, and ribbons of blood hung in the air.\"\n—Adom Capashen, Benalish hero').
card_multiverse_id('fury sliver'/'H09', '207904').

card_in_set('gemhide sliver', 'H09').
card_original_type('gemhide sliver'/'H09', 'Creature — Sliver').
card_original_text('gemhide sliver'/'H09', 'All Slivers have \"{T}: Add one mana of any color to your mana pool.\"').
card_image_name('gemhide sliver'/'H09', 'gemhide sliver').
card_uid('gemhide sliver'/'H09', 'H09:Gemhide Sliver:gemhide sliver').
card_rarity('gemhide sliver'/'H09', 'Common').
card_artist('gemhide sliver'/'H09', 'John Matson').
card_number('gemhide sliver'/'H09', '8').
card_flavor_text('gemhide sliver'/'H09', '\"The land is weary. Even Skyshroud is depleted. We must find another source of mana—one that is growing despite our withering world.\"\n—Freyalise').
card_multiverse_id('gemhide sliver'/'H09', '207905').

card_in_set('heart sliver', 'H09').
card_original_type('heart sliver'/'H09', 'Creature — Sliver').
card_original_text('heart sliver'/'H09', 'All Sliver creatures have haste.').
card_image_name('heart sliver'/'H09', 'heart sliver').
card_uid('heart sliver'/'H09', 'H09:Heart Sliver:heart sliver').
card_rarity('heart sliver'/'H09', 'Common').
card_artist('heart sliver'/'H09', 'Ron Spencer').
card_number('heart sliver'/'H09', '7').
card_flavor_text('heart sliver'/'H09', 'Gerrard looked all around for the source of the mysterious pulse, and at that moment the slivers boiled forth from the crevasses.').
card_multiverse_id('heart sliver'/'H09', '207906').

card_in_set('heartstone', 'H09').
card_original_type('heartstone'/'H09', 'Artifact').
card_original_text('heartstone'/'H09', 'Activated abilities of creatures cost {1} less to activate. This effect can\'t reduce the amount of mana an ability costs to activate to less than one mana.').
card_image_name('heartstone'/'H09', 'heartstone').
card_uid('heartstone'/'H09', 'H09:Heartstone:heartstone').
card_rarity('heartstone'/'H09', 'Uncommon').
card_artist('heartstone'/'H09', 'John Matson').
card_number('heartstone'/'H09', '26').
card_flavor_text('heartstone'/'H09', '\"Finding a true heartstone is even harder than finding a true heart.\"\n—Hanna, Weatherlight navigator').
card_multiverse_id('heartstone'/'H09', '207924').

card_in_set('hibernation sliver', 'H09').
card_original_type('hibernation sliver'/'H09', 'Creature — Sliver').
card_original_text('hibernation sliver'/'H09', 'All Slivers have \"Pay 2 life: Return this permanent to its owner\'s hand.\"').
card_image_name('hibernation sliver'/'H09', 'hibernation sliver').
card_uid('hibernation sliver'/'H09', 'H09:Hibernation Sliver:hibernation sliver').
card_rarity('hibernation sliver'/'H09', 'Uncommon').
card_artist('hibernation sliver'/'H09', 'Scott Kirschner').
card_number('hibernation sliver'/'H09', '12').
card_flavor_text('hibernation sliver'/'H09', 'Mogglings have been known to play ball with hibernating slivers, completely unaware of their true nature.').
card_multiverse_id('hibernation sliver'/'H09', '207907').

card_in_set('homing sliver', 'H09').
card_original_type('homing sliver'/'H09', 'Creature — Sliver').
card_original_text('homing sliver'/'H09', 'Each Sliver card in each player\'s hand has slivercycling {3}.\nSlivercycling {3} ({3}, Discard this card: Search your library for a Sliver card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('homing sliver'/'H09', 'homing sliver').
card_uid('homing sliver'/'H09', 'H09:Homing Sliver:homing sliver').
card_rarity('homing sliver'/'H09', 'Common').
card_artist('homing sliver'/'H09', 'Trevor Hairsine').
card_number('homing sliver'/'H09', '19').
card_multiverse_id('homing sliver'/'H09', '207908').

card_in_set('island', 'H09').
card_original_type('island'/'H09', 'Basic Land — Island').
card_original_text('island'/'H09', 'U').
card_image_name('island'/'H09', 'island').
card_uid('island'/'H09', 'H09:Island:island').
card_rarity('island'/'H09', 'Basic Land').
card_artist('island'/'H09', 'Randy Gallegos').
card_number('island'/'H09', '38').
card_multiverse_id('island'/'H09', '207935').

card_in_set('metallic sliver', 'H09').
card_original_type('metallic sliver'/'H09', 'Artifact Creature — Sliver').
card_original_text('metallic sliver'/'H09', '').
card_image_name('metallic sliver'/'H09', 'metallic sliver').
card_uid('metallic sliver'/'H09', 'H09:Metallic Sliver:metallic sliver').
card_rarity('metallic sliver'/'H09', 'Common').
card_artist('metallic sliver'/'H09', 'L. A. Williams').
card_number('metallic sliver'/'H09', '1').
card_flavor_text('metallic sliver'/'H09', 'When the clever counterfeit was accepted by the hive, Volrath\'s influence upon the slivers grew even stronger.').
card_multiverse_id('metallic sliver'/'H09', '208040').

card_in_set('might sliver', 'H09').
card_original_type('might sliver'/'H09', 'Creature — Sliver').
card_original_text('might sliver'/'H09', 'All Sliver creatures get +2/+2.').
card_image_name('might sliver'/'H09', 'might sliver').
card_uid('might sliver'/'H09', 'H09:Might Sliver:might sliver').
card_rarity('might sliver'/'H09', 'Uncommon').
card_artist('might sliver'/'H09', 'Jeff Miracola').
card_number('might sliver'/'H09', '23').
card_flavor_text('might sliver'/'H09', '\"The colossal thing rumbled over the ridge, tree husks crumbling before it. The ones we were already fighting howled as it came, their muscles suddenly surging, and we knew it was time to flee.\"\n—Llanach, Skyshroud ranger').
card_multiverse_id('might sliver'/'H09', '207910').

card_in_set('mountain', 'H09').
card_original_type('mountain'/'H09', 'Basic Land — Mountain').
card_original_text('mountain'/'H09', 'R').
card_image_name('mountain'/'H09', 'mountain').
card_uid('mountain'/'H09', 'H09:Mountain:mountain').
card_rarity('mountain'/'H09', 'Basic Land').
card_artist('mountain'/'H09', 'Mark Poole').
card_number('mountain'/'H09', '40').
card_multiverse_id('mountain'/'H09', '207936').

card_in_set('muscle sliver', 'H09').
card_original_type('muscle sliver'/'H09', 'Creature — Sliver').
card_original_text('muscle sliver'/'H09', 'All Sliver creatures get +1/+1.').
card_image_name('muscle sliver'/'H09', 'muscle sliver').
card_uid('muscle sliver'/'H09', 'H09:Muscle Sliver:muscle sliver').
card_rarity('muscle sliver'/'H09', 'Common').
card_artist('muscle sliver'/'H09', 'Richard Kane Ferguson').
card_number('muscle sliver'/'H09', '9').
card_flavor_text('muscle sliver'/'H09', 'The air was filled with the cracks and snaps of flesh hardening as the new sliver joined the battle.').
card_multiverse_id('muscle sliver'/'H09', '207911').

card_in_set('necrotic sliver', 'H09').
card_original_type('necrotic sliver'/'H09', 'Creature — Sliver').
card_original_text('necrotic sliver'/'H09', 'All Slivers have \"{3}, Sacrifice this permanent: Destroy target permanent.\"').
card_image_name('necrotic sliver'/'H09', 'necrotic sliver').
card_uid('necrotic sliver'/'H09', 'H09:Necrotic Sliver:necrotic sliver').
card_rarity('necrotic sliver'/'H09', 'Uncommon').
card_artist('necrotic sliver'/'H09', 'Dave Allsop').
card_number('necrotic sliver'/'H09', '20').
card_flavor_text('necrotic sliver'/'H09', 'Though Volrath is long dead, the slivers have become everything he wanted them to be: mindless instruments of destruction and despair.').
card_multiverse_id('necrotic sliver'/'H09', '207912').

card_in_set('plains', 'H09').
card_original_type('plains'/'H09', 'Basic Land — Plains').
card_original_text('plains'/'H09', 'W').
card_image_name('plains'/'H09', 'plains').
card_uid('plains'/'H09', 'H09:Plains:plains').
card_rarity('plains'/'H09', 'Basic Land').
card_artist('plains'/'H09', 'Terese Nielsen').
card_number('plains'/'H09', '37').
card_multiverse_id('plains'/'H09', '207937').

card_in_set('quick sliver', 'H09').
card_original_type('quick sliver'/'H09', 'Creature — Sliver').
card_original_text('quick sliver'/'H09', 'Flash\nAny player may cast Sliver cards as though they had flash.').
card_image_name('quick sliver'/'H09', 'quick sliver').
card_uid('quick sliver'/'H09', 'H09:Quick Sliver:quick sliver').
card_rarity('quick sliver'/'H09', 'Common').
card_artist('quick sliver'/'H09', 'John Avon').
card_number('quick sliver'/'H09', '10').
card_flavor_text('quick sliver'/'H09', 'The directors of the Riptide Project wanted instant results on the sliver experiments. They got their wish.').
card_multiverse_id('quick sliver'/'H09', '207913').

card_in_set('rootbound crag', 'H09').
card_original_type('rootbound crag'/'H09', 'Land').
card_original_text('rootbound crag'/'H09', 'Rootbound Crag enters the battlefield tapped unless you control a Mountain or a Forest.\n{T}: Add {R} or {G} to your mana pool.').
card_image_name('rootbound crag'/'H09', 'rootbound crag').
card_uid('rootbound crag'/'H09', 'H09:Rootbound Crag:rootbound crag').
card_rarity('rootbound crag'/'H09', 'Rare').
card_artist('rootbound crag'/'H09', 'Matt Stewart').
card_number('rootbound crag'/'H09', '32').
card_multiverse_id('rootbound crag'/'H09', '208042').

card_in_set('rupture spire', 'H09').
card_original_type('rupture spire'/'H09', 'Land').
card_original_text('rupture spire'/'H09', 'Rupture Spire enters the battlefield tapped.\nWhen Rupture Spire enters the battlefield, sacrifice it unless you pay {1}.\n{T}: Add one mana of any color to your mana pool.').
card_image_name('rupture spire'/'H09', 'rupture spire').
card_uid('rupture spire'/'H09', 'H09:Rupture Spire:rupture spire').
card_rarity('rupture spire'/'H09', 'Common').
card_artist('rupture spire'/'H09', 'Jaime Jones').
card_number('rupture spire'/'H09', '33').
card_multiverse_id('rupture spire'/'H09', '207933').

card_in_set('sliver overlord', 'H09').
card_original_type('sliver overlord'/'H09', 'Legendary Creature — Sliver Mutant').
card_original_text('sliver overlord'/'H09', '{3}: Search your library for a Sliver card, reveal that card, and put it into your hand. Then shuffle your library.\n{3}: Gain control of target Sliver.').
card_image_name('sliver overlord'/'H09', 'sliver overlord').
card_uid('sliver overlord'/'H09', 'H09:Sliver Overlord:sliver overlord').
card_rarity('sliver overlord'/'H09', 'Mythic Rare').
card_artist('sliver overlord'/'H09', 'Tony Szczudlo').
card_number('sliver overlord'/'H09', '24').
card_flavor_text('sliver overlord'/'H09', 'The end of evolution.').
card_multiverse_id('sliver overlord'/'H09', '207915').

card_in_set('spectral sliver', 'H09').
card_original_type('spectral sliver'/'H09', 'Creature — Sliver Spirit').
card_original_text('spectral sliver'/'H09', 'All Sliver creatures have \"{2}: This creature gets +1/+1 until end of turn.\"').
card_image_name('spectral sliver'/'H09', 'spectral sliver').
card_uid('spectral sliver'/'H09', 'H09:Spectral Sliver:spectral sliver').
card_rarity('spectral sliver'/'H09', 'Uncommon').
card_artist('spectral sliver'/'H09', 'Pete Venters').
card_number('spectral sliver'/'H09', '17').
card_flavor_text('spectral sliver'/'H09', '\"Sure, I\'ve seen agents of the Cabal here and there. Well, not here, and certainly not in any of the sliver labs. Oh dear, I\'ve said too much.\"\n—Apprentice researcher').
card_multiverse_id('spectral sliver'/'H09', '207916').

card_in_set('spined sliver', 'H09').
card_original_type('spined sliver'/'H09', 'Creature — Sliver').
card_original_text('spined sliver'/'H09', 'Whenever a Sliver becomes blocked, that Sliver gets +1/+1 until end of turn for each creature blocking it.').
card_image_name('spined sliver'/'H09', 'spined sliver').
card_uid('spined sliver'/'H09', 'H09:Spined Sliver:spined sliver').
card_rarity('spined sliver'/'H09', 'Uncommon').
card_artist('spined sliver'/'H09', 'Ron Spencer').
card_number('spined sliver'/'H09', '14').
card_flavor_text('spined sliver'/'H09', '\"Slivers are evil and slivers are sly;\nAnd if you get eaten, then no one will cry.\"\n—Mogg children\'s rhyme').
card_multiverse_id('spined sliver'/'H09', '208041').

card_in_set('swamp', 'H09').
card_original_type('swamp'/'H09', 'Basic Land — Swamp').
card_original_text('swamp'/'H09', 'B').
card_image_name('swamp'/'H09', 'swamp').
card_uid('swamp'/'H09', 'H09:Swamp:swamp').
card_rarity('swamp'/'H09', 'Basic Land').
card_artist('swamp'/'H09', 'Brom').
card_number('swamp'/'H09', '39').
card_multiverse_id('swamp'/'H09', '207938').

card_in_set('terramorphic expanse', 'H09').
card_original_type('terramorphic expanse'/'H09', 'Land').
card_original_text('terramorphic expanse'/'H09', '{T}, Sacrifice Terramorphic Expanse: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('terramorphic expanse'/'H09', 'terramorphic expanse').
card_uid('terramorphic expanse'/'H09', 'H09:Terramorphic Expanse:terramorphic expanse').
card_rarity('terramorphic expanse'/'H09', 'Common').
card_artist('terramorphic expanse'/'H09', 'Dan Scott').
card_number('terramorphic expanse'/'H09', '34').
card_multiverse_id('terramorphic expanse'/'H09', '207928').

card_in_set('victual sliver', 'H09').
card_original_type('victual sliver'/'H09', 'Creature — Sliver').
card_original_text('victual sliver'/'H09', 'All Slivers have \"{2}, Sacrifice this permanent: You gain 4 life.\"').
card_image_name('victual sliver'/'H09', 'victual sliver').
card_uid('victual sliver'/'H09', 'H09:Victual Sliver:victual sliver').
card_rarity('victual sliver'/'H09', 'Uncommon').
card_artist('victual sliver'/'H09', 'Terese Nielsen').
card_number('victual sliver'/'H09', '15').
card_flavor_text('victual sliver'/'H09', '\"We are kinfolk,\" explained Karn to the sliver queen. \"Just as you need your progeny to complete you, so do I need the pieces of the Legacy to make me whole.\"').
card_multiverse_id('victual sliver'/'H09', '207918').

card_in_set('virulent sliver', 'H09').
card_original_type('virulent sliver'/'H09', 'Creature — Sliver').
card_original_text('virulent sliver'/'H09', 'All Sliver creatures have poisonous 1. (Whenever a Sliver deals combat damage to a player, that player gets a poison counter. A player with ten or more poison counters loses the game.)').
card_image_name('virulent sliver'/'H09', 'virulent sliver').
card_uid('virulent sliver'/'H09', 'H09:Virulent Sliver:virulent sliver').
card_rarity('virulent sliver'/'H09', 'Common').
card_artist('virulent sliver'/'H09', 'Franz Vohwinkel').
card_number('virulent sliver'/'H09', '2').
card_multiverse_id('virulent sliver'/'H09', '207919').

card_in_set('vivid creek', 'H09').
card_original_type('vivid creek'/'H09', 'Land').
card_original_text('vivid creek'/'H09', 'Vivid Creek enters the battlefield tapped with two charge counters on it.\n{T}: Add {U} to your mana pool.\n{T}, Remove a charge counter from Vivid Creek: Add one mana of any color to your mana pool.').
card_image_name('vivid creek'/'H09', 'vivid creek').
card_uid('vivid creek'/'H09', 'H09:Vivid Creek:vivid creek').
card_rarity('vivid creek'/'H09', 'Uncommon').
card_artist('vivid creek'/'H09', 'Fred Fields').
card_number('vivid creek'/'H09', '35').
card_multiverse_id('vivid creek'/'H09', '208043').

card_in_set('vivid grove', 'H09').
card_original_type('vivid grove'/'H09', 'Land').
card_original_text('vivid grove'/'H09', 'Vivid Grove enters the battlefield tapped with two charge counters on it.\n{T}: Add {G} to your mana pool.\n{T}, Remove a charge counter from Vivid Grove: Add one mana of any color to your mana pool.').
card_image_name('vivid grove'/'H09', 'vivid grove').
card_uid('vivid grove'/'H09', 'H09:Vivid Grove:vivid grove').
card_rarity('vivid grove'/'H09', 'Uncommon').
card_artist('vivid grove'/'H09', 'Howard Lyon').
card_number('vivid grove'/'H09', '36').
card_multiverse_id('vivid grove'/'H09', '207922').

card_in_set('wild pair', 'H09').
card_original_type('wild pair'/'H09', 'Enchantment').
card_original_text('wild pair'/'H09', 'Whenever a creature enters the battlefield, if you cast it from your hand, you may search your library for a creature card with the same total power and toughness, put it onto the battlefield, then shuffle your library.').
card_image_name('wild pair'/'H09', 'wild pair').
card_uid('wild pair'/'H09', 'H09:Wild Pair:wild pair').
card_rarity('wild pair'/'H09', 'Rare').
card_artist('wild pair'/'H09', 'Mark Brill').
card_number('wild pair'/'H09', '30').
card_multiverse_id('wild pair'/'H09', '207929').

card_in_set('winged sliver', 'H09').
card_original_type('winged sliver'/'H09', 'Creature — Sliver').
card_original_text('winged sliver'/'H09', 'All Sliver creatures have flying.').
card_image_name('winged sliver'/'H09', 'winged sliver').
card_uid('winged sliver'/'H09', 'H09:Winged Sliver:winged sliver').
card_rarity('winged sliver'/'H09', 'Common').
card_artist('winged sliver'/'H09', 'Anthony S. Waters').
card_number('winged sliver'/'H09', '4').
card_flavor_text('winged sliver'/'H09', '\"Everything around here has cut a deal with gravity.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('winged sliver'/'H09', '207920').
