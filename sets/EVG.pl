% Duel Decks: Elves vs. Goblins

set('EVG').
set_name('EVG', 'Duel Decks: Elves vs. Goblins').
set_release_date('EVG', '2007-11-16').
set_border('EVG', 'black').
set_type('EVG', 'duel deck').

card_in_set('akki coalflinger', 'EVG').
card_original_type('akki coalflinger'/'EVG', 'Creature — Goblin Shaman').
card_original_text('akki coalflinger'/'EVG', 'First strike\n{R}, {T}: Attacking creatures gain first strike until end of turn.').
card_image_name('akki coalflinger'/'EVG', 'akki coalflinger').
card_uid('akki coalflinger'/'EVG', 'EVG:Akki Coalflinger:akki coalflinger').
card_rarity('akki coalflinger'/'EVG', 'Uncommon').
card_artist('akki coalflinger'/'EVG', 'Nottsuo').
card_number('akki coalflinger'/'EVG', '33').
card_flavor_text('akki coalflinger'/'EVG', 'No matter where you find them, goblins love rocks.').
card_multiverse_id('akki coalflinger'/'EVG', '159328').

card_in_set('allosaurus rider', 'EVG').
card_original_type('allosaurus rider'/'EVG', 'Creature — Elf Warrior').
card_original_text('allosaurus rider'/'EVG', 'You may remove two green cards in your hand from the game rather than pay Allosaurus Rider\'s mana cost.\nAllosaurus Rider\'s power and toughness are each equal to 1 plus the number of lands you control.').
card_image_name('allosaurus rider'/'EVG', 'allosaurus rider').
card_uid('allosaurus rider'/'EVG', 'EVG:Allosaurus Rider:allosaurus rider').
card_rarity('allosaurus rider'/'EVG', 'Rare').
card_artist('allosaurus rider'/'EVG', 'Daren Bader').
card_number('allosaurus rider'/'EVG', '2').
card_multiverse_id('allosaurus rider'/'EVG', '158100').

card_in_set('ambush commander', 'EVG').
card_original_type('ambush commander'/'EVG', 'Creature — Elf').
card_original_text('ambush commander'/'EVG', 'Forests you control are 1/1 green Elf creatures that are still lands.\n{1}{G}, Sacrifice an Elf: Target creature gets +3/+3 until end of turn.').
card_image_name('ambush commander'/'EVG', 'ambush commander').
card_uid('ambush commander'/'EVG', 'EVG:Ambush Commander:ambush commander').
card_rarity('ambush commander'/'EVG', 'Rare').
card_artist('ambush commander'/'EVG', 'Lucio Parrillo').
card_number('ambush commander'/'EVG', '1').
card_multiverse_id('ambush commander'/'EVG', '157950').

card_in_set('boggart shenanigans', 'EVG').
card_original_type('boggart shenanigans'/'EVG', 'Tribal Enchantment — Goblin').
card_original_text('boggart shenanigans'/'EVG', 'Whenever another Goblin you control is put into a graveyard from play, you may have Boggart Shenanigans deal 1 damage to target player.').
card_image_name('boggart shenanigans'/'EVG', 'boggart shenanigans').
card_uid('boggart shenanigans'/'EVG', 'EVG:Boggart Shenanigans:boggart shenanigans').
card_rarity('boggart shenanigans'/'EVG', 'Uncommon').
card_artist('boggart shenanigans'/'EVG', 'Warren Mahy').
card_number('boggart shenanigans'/'EVG', '54').
card_flavor_text('boggart shenanigans'/'EVG', 'Boggarts revel in discovering new sensations, from the texture of an otter pellet to the squeak of a dying warren mate.').
card_multiverse_id('boggart shenanigans'/'EVG', '157915').

card_in_set('clickslither', 'EVG').
card_original_type('clickslither'/'EVG', 'Creature — Insect').
card_original_text('clickslither'/'EVG', 'Haste\nSacrifice a Goblin: Clickslither gets +2/+2 and gains trample until end of turn.').
card_image_name('clickslither'/'EVG', 'clickslither').
card_uid('clickslither'/'EVG', 'EVG:Clickslither:clickslither').
card_rarity('clickslither'/'EVG', 'Rare').
card_artist('clickslither'/'EVG', 'Kev Walker').
card_number('clickslither'/'EVG', '34').
card_flavor_text('clickslither'/'EVG', 'The least popular goblins get the outer caves.').
card_multiverse_id('clickslither'/'EVG', '157928').

card_in_set('elemental', 'EVG').
card_original_type('elemental'/'EVG', 'Creature — Elemental').
card_original_text('elemental'/'EVG', 'Trample').
card_first_print('elemental', 'EVG').
card_image_name('elemental'/'EVG', 'elemental').
card_uid('elemental'/'EVG', 'EVG:Elemental:elemental').
card_rarity('elemental'/'EVG', 'Common').
card_artist('elemental'/'EVG', 'Anthony S. Waters').
card_number('elemental'/'EVG', 'T1').
card_multiverse_id('elemental'/'EVG', '159048').

card_in_set('elf warrior', 'EVG').
card_original_type('elf warrior'/'EVG', 'Creature — Elf Warrior').
card_original_text('elf warrior'/'EVG', '').
card_first_print('elf warrior', 'EVG').
card_image_name('elf warrior'/'EVG', 'elf warrior').
card_uid('elf warrior'/'EVG', 'EVG:Elf Warrior:elf warrior').
card_rarity('elf warrior'/'EVG', 'Common').
card_artist('elf warrior'/'EVG', 'Dominick Domingo').
card_number('elf warrior'/'EVG', 'T2').
card_multiverse_id('elf warrior'/'EVG', '159047').

card_in_set('elvish eulogist', 'EVG').
card_original_type('elvish eulogist'/'EVG', 'Creature — Elf Shaman').
card_original_text('elvish eulogist'/'EVG', 'Sacrifice Elvish Eulogist: You gain 1 life for each Elf card in your graveyard.').
card_image_name('elvish eulogist'/'EVG', 'elvish eulogist').
card_uid('elvish eulogist'/'EVG', 'EVG:Elvish Eulogist:elvish eulogist').
card_rarity('elvish eulogist'/'EVG', 'Common').
card_artist('elvish eulogist'/'EVG', 'Ben Thompson').
card_number('elvish eulogist'/'EVG', '3').
card_flavor_text('elvish eulogist'/'EVG', '\"No matter how adept our artistic skill, our effigies can never hope to capture the vibrant beauty of a living elf. Perhaps that is truly why we mourn.\"').
card_multiverse_id('elvish eulogist'/'EVG', '158120').

card_in_set('elvish harbinger', 'EVG').
card_original_type('elvish harbinger'/'EVG', 'Creature — Elf Druid').
card_original_text('elvish harbinger'/'EVG', 'When Elvish Harbinger comes into play, you may search your library for an Elf card, reveal it, then shuffle your library and put that card on top of it.\n{T}: Add one mana of any color to your mana pool.').
card_image_name('elvish harbinger'/'EVG', 'elvish harbinger').
card_uid('elvish harbinger'/'EVG', 'EVG:Elvish Harbinger:elvish harbinger').
card_rarity('elvish harbinger'/'EVG', 'Uncommon').
card_artist('elvish harbinger'/'EVG', 'Larry MacDougall').
card_number('elvish harbinger'/'EVG', '4').
card_multiverse_id('elvish harbinger'/'EVG', '158121').

card_in_set('elvish promenade', 'EVG').
card_original_type('elvish promenade'/'EVG', 'Tribal Sorcery — Elf').
card_original_text('elvish promenade'/'EVG', 'Put a 1/1 green Elf Warrior creature token into play for each Elf you control.').
card_image_name('elvish promenade'/'EVG', 'elvish promenade').
card_uid('elvish promenade'/'EVG', 'EVG:Elvish Promenade:elvish promenade').
card_rarity('elvish promenade'/'EVG', 'Uncommon').
card_artist('elvish promenade'/'EVG', 'Steve Ellis').
card_number('elvish promenade'/'EVG', '20').
card_flavor_text('elvish promenade'/'EVG', 'The faultless and immaculate castes form the lower tiers of elvish society, with the exquisite caste above them. At the pinnacle is the perfect, a consummate blend of aristocrat and predator.').
card_multiverse_id('elvish promenade'/'EVG', '159030').

card_in_set('elvish warrior', 'EVG').
card_original_type('elvish warrior'/'EVG', 'Creature — Elf Warrior').
card_original_text('elvish warrior'/'EVG', '').
card_image_name('elvish warrior'/'EVG', 'elvish warrior').
card_uid('elvish warrior'/'EVG', 'EVG:Elvish Warrior:elvish warrior').
card_rarity('elvish warrior'/'EVG', 'Common').
card_artist('elvish warrior'/'EVG', 'Christopher Moeller').
card_number('elvish warrior'/'EVG', '5').
card_flavor_text('elvish warrior'/'EVG', '\"My tales of war are the stories most asked for around the fires at night, but they\'re the ones I care least to tell.\"').
card_multiverse_id('elvish warrior'/'EVG', '158136').

card_in_set('emberwilde augur', 'EVG').
card_original_type('emberwilde augur'/'EVG', 'Creature — Goblin Shaman').
card_original_text('emberwilde augur'/'EVG', 'Sacrifice Emberwilde Augur: Emberwilde Augur deals 3 damage to target player. Play this ability only during your upkeep.').
card_image_name('emberwilde augur'/'EVG', 'emberwilde augur').
card_uid('emberwilde augur'/'EVG', 'EVG:Emberwilde Augur:emberwilde augur').
card_rarity('emberwilde augur'/'EVG', 'Common').
card_artist('emberwilde augur'/'EVG', 'Brandon Kitkouski').
card_number('emberwilde augur'/'EVG', '35').
card_flavor_text('emberwilde augur'/'EVG', 'Legends say a djinn gave the goblins a gift they could never hope to master.').
card_multiverse_id('emberwilde augur'/'EVG', '157932').

card_in_set('flamewave invoker', 'EVG').
card_original_type('flamewave invoker'/'EVG', 'Creature — Goblin Mutant').
card_original_text('flamewave invoker'/'EVG', '{7}{R}: Flamewave Invoker deals 5 damage to target player.').
card_image_name('flamewave invoker'/'EVG', 'flamewave invoker').
card_uid('flamewave invoker'/'EVG', 'EVG:Flamewave Invoker:flamewave invoker').
card_rarity('flamewave invoker'/'EVG', 'Uncommon').
card_artist('flamewave invoker'/'EVG', 'Dave Dorman').
card_number('flamewave invoker'/'EVG', '36').
card_flavor_text('flamewave invoker'/'EVG', 'Inside even the humblest goblin lurks the potential for far greater things—and far worse.').
card_multiverse_id('flamewave invoker'/'EVG', '159053').

card_in_set('forest', 'EVG').
card_original_type('forest'/'EVG', 'Basic Land — Forest').
card_original_text('forest'/'EVG', '').
card_image_name('forest'/'EVG', 'forest1').
card_uid('forest'/'EVG', 'EVG:Forest:forest1').
card_rarity('forest'/'EVG', 'Basic Land').
card_artist('forest'/'EVG', 'Glen Angus').
card_number('forest'/'EVG', '28').
card_multiverse_id('forest'/'EVG', '157946').

card_in_set('forest', 'EVG').
card_original_type('forest'/'EVG', 'Basic Land — Forest').
card_original_text('forest'/'EVG', '').
card_image_name('forest'/'EVG', 'forest2').
card_uid('forest'/'EVG', 'EVG:Forest:forest2').
card_rarity('forest'/'EVG', 'Basic Land').
card_artist('forest'/'EVG', 'John Avon').
card_number('forest'/'EVG', '29').
card_multiverse_id('forest'/'EVG', '157948').

card_in_set('forest', 'EVG').
card_original_type('forest'/'EVG', 'Basic Land — Forest').
card_original_text('forest'/'EVG', '').
card_image_name('forest'/'EVG', 'forest3').
card_uid('forest'/'EVG', 'EVG:Forest:forest3').
card_rarity('forest'/'EVG', 'Basic Land').
card_artist('forest'/'EVG', 'John Avon').
card_number('forest'/'EVG', '30').
card_multiverse_id('forest'/'EVG', '159042').

card_in_set('forest', 'EVG').
card_original_type('forest'/'EVG', 'Basic Land — Forest').
card_original_text('forest'/'EVG', '').
card_image_name('forest'/'EVG', 'forest4').
card_uid('forest'/'EVG', 'EVG:Forest:forest4').
card_rarity('forest'/'EVG', 'Basic Land').
card_artist('forest'/'EVG', 'Rob Alexander').
card_number('forest'/'EVG', '31').
card_multiverse_id('forest'/'EVG', '159041').

card_in_set('forgotten cave', 'EVG').
card_original_type('forgotten cave'/'EVG', 'Land').
card_original_text('forgotten cave'/'EVG', 'Forgotten Cave comes into play tapped.\n{T}: Add {R} to your mana pool.\nCycling {R} ({R}, Discard this card: Draw a card.)').
card_image_name('forgotten cave'/'EVG', 'forgotten cave').
card_uid('forgotten cave'/'EVG', 'EVG:Forgotten Cave:forgotten cave').
card_rarity('forgotten cave'/'EVG', 'Common').
card_artist('forgotten cave'/'EVG', 'Tony Szczudlo').
card_number('forgotten cave'/'EVG', '57').
card_multiverse_id('forgotten cave'/'EVG', '159327').

card_in_set('gempalm incinerator', 'EVG').
card_original_type('gempalm incinerator'/'EVG', 'Creature — Goblin').
card_original_text('gempalm incinerator'/'EVG', 'Cycling {1}{R} ({1}{R}, Discard this card: Draw a card.)\nWhen you cycle Gempalm Incinerator, you may have it deal X damage to target creature, where X is the number of Goblins in play.').
card_image_name('gempalm incinerator'/'EVG', 'gempalm incinerator').
card_uid('gempalm incinerator'/'EVG', 'EVG:Gempalm Incinerator:gempalm incinerator').
card_rarity('gempalm incinerator'/'EVG', 'Uncommon').
card_artist('gempalm incinerator'/'EVG', 'Luca Zontini').
card_number('gempalm incinerator'/'EVG', '37').
card_multiverse_id('gempalm incinerator'/'EVG', '157929').

card_in_set('gempalm strider', 'EVG').
card_original_type('gempalm strider'/'EVG', 'Creature — Elf').
card_original_text('gempalm strider'/'EVG', 'Cycling {2}{G}{G} ({2}{G}{G}, Discard this card: Draw a card.)\nWhen you cycle Gempalm Strider, all Elf creatures get +2/+2 until end of turn.').
card_image_name('gempalm strider'/'EVG', 'gempalm strider').
card_uid('gempalm strider'/'EVG', 'EVG:Gempalm Strider:gempalm strider').
card_rarity('gempalm strider'/'EVG', 'Uncommon').
card_artist('gempalm strider'/'EVG', 'Tim Hildebrandt').
card_number('gempalm strider'/'EVG', '6').
card_multiverse_id('gempalm strider'/'EVG', '158117').

card_in_set('giant growth', 'EVG').
card_original_type('giant growth'/'EVG', 'Instant').
card_original_text('giant growth'/'EVG', 'Target creature gets +3/+3 until end of turn.').
card_image_name('giant growth'/'EVG', 'giant growth').
card_uid('giant growth'/'EVG', 'EVG:Giant Growth:giant growth').
card_rarity('giant growth'/'EVG', 'Common').
card_artist('giant growth'/'EVG', 'Matt Cavotta').
card_number('giant growth'/'EVG', '21').
card_multiverse_id('giant growth'/'EVG', '159037').

card_in_set('goblin', 'EVG').
card_original_type('goblin'/'EVG', 'Creature — Goblin').
card_original_text('goblin'/'EVG', '').
card_first_print('goblin', 'EVG').
card_image_name('goblin'/'EVG', 'goblin').
card_uid('goblin'/'EVG', 'EVG:Goblin:goblin').
card_rarity('goblin'/'EVG', 'Common').
card_artist('goblin'/'EVG', 'Dave Kendall').
card_number('goblin'/'EVG', 'T3').
card_multiverse_id('goblin'/'EVG', '159056').

card_in_set('goblin burrows', 'EVG').
card_original_type('goblin burrows'/'EVG', 'Land').
card_original_text('goblin burrows'/'EVG', '{T}: Add {1} to your mana pool.\n{1}{R}, {T}: Target Goblin creature gets +2/+0 until end of turn.').
card_image_name('goblin burrows'/'EVG', 'goblin burrows').
card_uid('goblin burrows'/'EVG', 'EVG:Goblin Burrows:goblin burrows').
card_rarity('goblin burrows'/'EVG', 'Uncommon').
card_artist('goblin burrows'/'EVG', 'David Martin').
card_number('goblin burrows'/'EVG', '58').
card_multiverse_id('goblin burrows'/'EVG', '157935').

card_in_set('goblin cohort', 'EVG').
card_original_type('goblin cohort'/'EVG', 'Creature — Goblin Warrior').
card_original_text('goblin cohort'/'EVG', 'Goblin Cohort can\'t attack unless you\'ve played a creature spell this turn.').
card_image_name('goblin cohort'/'EVG', 'goblin cohort').
card_uid('goblin cohort'/'EVG', 'EVG:Goblin Cohort:goblin cohort').
card_rarity('goblin cohort'/'EVG', 'Common').
card_artist('goblin cohort'/'EVG', 'Darrell Riche').
card_number('goblin cohort'/'EVG', '38').
card_flavor_text('goblin cohort'/'EVG', 'Akki shells provided good protection when downhill charging became headlong tumbling.').
card_multiverse_id('goblin cohort'/'EVG', '157922').

card_in_set('goblin matron', 'EVG').
card_original_type('goblin matron'/'EVG', 'Creature — Goblin').
card_original_text('goblin matron'/'EVG', 'When Goblin Matron comes into play, you may search your library for a Goblin card, reveal that card, and put it into your hand. If you do, shuffle your library.').
card_image_name('goblin matron'/'EVG', 'goblin matron').
card_uid('goblin matron'/'EVG', 'EVG:Goblin Matron:goblin matron').
card_rarity('goblin matron'/'EVG', 'Uncommon').
card_artist('goblin matron'/'EVG', 'Daniel Gelon').
card_number('goblin matron'/'EVG', '39').
card_flavor_text('goblin matron'/'EVG', 'There\'s always room for one more.').
card_multiverse_id('goblin matron'/'EVG', '157933').

card_in_set('goblin ringleader', 'EVG').
card_original_type('goblin ringleader'/'EVG', 'Creature — Goblin').
card_original_text('goblin ringleader'/'EVG', 'Haste\nWhen Goblin Ringleader comes into play, reveal the top four cards of your library. Put all Goblin cards revealed this way into your hand and the rest on the bottom of your library.').
card_image_name('goblin ringleader'/'EVG', 'goblin ringleader').
card_uid('goblin ringleader'/'EVG', 'EVG:Goblin Ringleader:goblin ringleader').
card_rarity('goblin ringleader'/'EVG', 'Uncommon').
card_artist('goblin ringleader'/'EVG', 'Mark Romanoski').
card_number('goblin ringleader'/'EVG', '40').
card_multiverse_id('goblin ringleader'/'EVG', '157937').

card_in_set('goblin sledder', 'EVG').
card_original_type('goblin sledder'/'EVG', 'Creature — Goblin').
card_original_text('goblin sledder'/'EVG', 'Sacrifice a Goblin: Target creature gets +1/+1 until end of turn.').
card_image_name('goblin sledder'/'EVG', 'goblin sledder').
card_uid('goblin sledder'/'EVG', 'EVG:Goblin Sledder:goblin sledder').
card_rarity('goblin sledder'/'EVG', 'Common').
card_artist('goblin sledder'/'EVG', 'Ron Spencer').
card_number('goblin sledder'/'EVG', '41').
card_flavor_text('goblin sledder'/'EVG', '\"Let\'s play ‘sled.\' Here\'s how it works: you\'re the sled.\"').
card_multiverse_id('goblin sledder'/'EVG', '159049').

card_in_set('goblin warchief', 'EVG').
card_original_type('goblin warchief'/'EVG', 'Creature — Goblin Warrior').
card_original_text('goblin warchief'/'EVG', 'Goblin spells you play cost {1} less to play.\nGoblin creatures you control have haste.').
card_image_name('goblin warchief'/'EVG', 'goblin warchief').
card_uid('goblin warchief'/'EVG', 'EVG:Goblin Warchief:goblin warchief').
card_rarity('goblin warchief'/'EVG', 'Uncommon').
card_artist('goblin warchief'/'EVG', 'Greg & Tim Hildebrandt').
card_number('goblin warchief'/'EVG', '42').
card_flavor_text('goblin warchief'/'EVG', 'They poured from the Skirk Ridge like lava, burning and devouring everything in their path.').
card_multiverse_id('goblin warchief'/'EVG', '157934').

card_in_set('harmonize', 'EVG').
card_original_type('harmonize'/'EVG', 'Sorcery').
card_original_text('harmonize'/'EVG', 'Draw three cards.').
card_image_name('harmonize'/'EVG', 'harmonize').
card_uid('harmonize'/'EVG', 'EVG:Harmonize:harmonize').
card_rarity('harmonize'/'EVG', 'Uncommon').
card_artist('harmonize'/'EVG', 'Rob Alexander').
card_number('harmonize'/'EVG', '22').
card_flavor_text('harmonize'/'EVG', '\"Life\'s greatest lessons don\'t come from focus or concentration. They come from breathing and simply noticing.\"\n—Seton, centaur druid').
card_multiverse_id('harmonize'/'EVG', '159033').

card_in_set('heedless one', 'EVG').
card_original_type('heedless one'/'EVG', 'Creature — Elf Avatar').
card_original_text('heedless one'/'EVG', 'Trample\nHeedless One\'s power and toughness are each equal to the number of Elves in play.').
card_image_name('heedless one'/'EVG', 'heedless one').
card_uid('heedless one'/'EVG', 'EVG:Heedless One:heedless one').
card_rarity('heedless one'/'EVG', 'Uncommon').
card_artist('heedless one'/'EVG', 'Mark Zug').
card_number('heedless one'/'EVG', '7').
card_flavor_text('heedless one'/'EVG', '\"Channel your vitality through me.\"').
card_multiverse_id('heedless one'/'EVG', '158113').

card_in_set('ib halfheart, goblin tactician', 'EVG').
card_original_type('ib halfheart, goblin tactician'/'EVG', 'Legendary Creature — Goblin Advisor').
card_original_text('ib halfheart, goblin tactician'/'EVG', 'Whenever another Goblin you control becomes blocked, sacrifice it. If you do, it deals 4 damage to each creature blocking it.\nSacrifice two Mountains: Put two 1/1 red Goblin creature tokens into play.').
card_image_name('ib halfheart, goblin tactician'/'EVG', 'ib halfheart, goblin tactician').
card_uid('ib halfheart, goblin tactician'/'EVG', 'EVG:Ib Halfheart, Goblin Tactician:ib halfheart, goblin tactician').
card_rarity('ib halfheart, goblin tactician'/'EVG', 'Rare').
card_artist('ib halfheart, goblin tactician'/'EVG', 'Wayne Reynolds').
card_number('ib halfheart, goblin tactician'/'EVG', '43').
card_flavor_text('ib halfheart, goblin tactician'/'EVG', '\"Everybody but me—CHARGE!\"').
card_multiverse_id('ib halfheart, goblin tactician'/'EVG', '157923').

card_in_set('imperious perfect', 'EVG').
card_original_type('imperious perfect'/'EVG', 'Creature — Elf Warrior').
card_original_text('imperious perfect'/'EVG', 'Other Elf creatures you control get +1/+1.\n{G}, {T}: Put a 1/1 green Elf Warrior creature token into play.').
card_image_name('imperious perfect'/'EVG', 'imperious perfect').
card_uid('imperious perfect'/'EVG', 'EVG:Imperious Perfect:imperious perfect').
card_rarity('imperious perfect'/'EVG', 'Uncommon').
card_artist('imperious perfect'/'EVG', 'Scott M. Fischer').
card_number('imperious perfect'/'EVG', '8').
card_flavor_text('imperious perfect'/'EVG', 'In a culture of beauty, the most beautiful are worshipped as gods.').
card_multiverse_id('imperious perfect'/'EVG', '158123').

card_in_set('llanowar elves', 'EVG').
card_original_type('llanowar elves'/'EVG', 'Creature — Elf Druid').
card_original_text('llanowar elves'/'EVG', '{T}: Add {G} to your mana pool.').
card_image_name('llanowar elves'/'EVG', 'llanowar elves').
card_uid('llanowar elves'/'EVG', 'EVG:Llanowar Elves:llanowar elves').
card_rarity('llanowar elves'/'EVG', 'Common').
card_artist('llanowar elves'/'EVG', 'Kev Walker').
card_number('llanowar elves'/'EVG', '9').
card_flavor_text('llanowar elves'/'EVG', 'One bone broken for every twig snapped underfoot.\n—Llanowar penalty for trespassing').
card_multiverse_id('llanowar elves'/'EVG', '158112').

card_in_set('lys alana huntmaster', 'EVG').
card_original_type('lys alana huntmaster'/'EVG', 'Creature — Elf Warrior').
card_original_text('lys alana huntmaster'/'EVG', 'Whenever you play an Elf spell, you may put a 1/1 green Elf Warrior creature token into play.').
card_image_name('lys alana huntmaster'/'EVG', 'lys alana huntmaster').
card_uid('lys alana huntmaster'/'EVG', 'EVG:Lys Alana Huntmaster:lys alana huntmaster').
card_rarity('lys alana huntmaster'/'EVG', 'Common').
card_artist('lys alana huntmaster'/'EVG', 'Pete Venters').
card_number('lys alana huntmaster'/'EVG', '10').
card_flavor_text('lys alana huntmaster'/'EVG', 'From the highest tiers of Dawn\'s Light Palace to the deepest shade of Wren\'s Run, the silver notes of the horn shimmer through the air, and all who hear it feel its pull.').
card_multiverse_id('lys alana huntmaster'/'EVG', '158125').

card_in_set('mogg fanatic', 'EVG').
card_original_type('mogg fanatic'/'EVG', 'Creature — Goblin').
card_original_text('mogg fanatic'/'EVG', 'Sacrifice Mogg Fanatic: Mogg Fanatic deals 1 damage to target creature or player.').
card_image_name('mogg fanatic'/'EVG', 'mogg fanatic').
card_uid('mogg fanatic'/'EVG', 'EVG:Mogg Fanatic:mogg fanatic').
card_rarity('mogg fanatic'/'EVG', 'Uncommon').
card_artist('mogg fanatic'/'EVG', 'Brom').
card_number('mogg fanatic'/'EVG', '44').
card_flavor_text('mogg fanatic'/'EVG', '\"I got it! I got it! I—\"').
card_multiverse_id('mogg fanatic'/'EVG', '157925').

card_in_set('mogg war marshal', 'EVG').
card_original_type('mogg war marshal'/'EVG', 'Creature — Goblin Warrior').
card_original_text('mogg war marshal'/'EVG', 'Echo {1}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Mogg War Marshal comes into play or is put into a graveyard from play, put a 1/1 red Goblin creature token into play.').
card_image_name('mogg war marshal'/'EVG', 'mogg war marshal').
card_uid('mogg war marshal'/'EVG', 'EVG:Mogg War Marshal:mogg war marshal').
card_rarity('mogg war marshal'/'EVG', 'Common').
card_artist('mogg war marshal'/'EVG', 'Wayne England').
card_number('mogg war marshal'/'EVG', '45').
card_multiverse_id('mogg war marshal'/'EVG', '157924').

card_in_set('moonglove extract', 'EVG').
card_original_type('moonglove extract'/'EVG', 'Artifact').
card_original_text('moonglove extract'/'EVG', 'Sacrifice Moonglove Extract: Moonglove Extract deals 2 damage to target creature or player.').
card_image_name('moonglove extract'/'EVG', 'moonglove extract').
card_uid('moonglove extract'/'EVG', 'EVG:Moonglove Extract:moonglove extract').
card_rarity('moonglove extract'/'EVG', 'Common').
card_artist('moonglove extract'/'EVG', 'Terese Nielsen').
card_number('moonglove extract'/'EVG', '24').
card_flavor_text('moonglove extract'/'EVG', 'Diluted, moonglove can etch living tissue. Concentrated, a drop will kill a giant.').
card_multiverse_id('moonglove extract'/'EVG', '159032').

card_in_set('mountain', 'EVG').
card_original_type('mountain'/'EVG', 'Basic Land — Mountain').
card_original_text('mountain'/'EVG', '').
card_image_name('mountain'/'EVG', 'mountain1').
card_uid('mountain'/'EVG', 'EVG:Mountain:mountain1').
card_rarity('mountain'/'EVG', 'Basic Land').
card_artist('mountain'/'EVG', 'David Day').
card_number('mountain'/'EVG', '59').
card_multiverse_id('mountain'/'EVG', '157940').

card_in_set('mountain', 'EVG').
card_original_type('mountain'/'EVG', 'Basic Land — Mountain').
card_original_text('mountain'/'EVG', '').
card_image_name('mountain'/'EVG', 'mountain2').
card_uid('mountain'/'EVG', 'EVG:Mountain:mountain2').
card_rarity('mountain'/'EVG', 'Basic Land').
card_artist('mountain'/'EVG', 'Jeff Miracola').
card_number('mountain'/'EVG', '60').
card_multiverse_id('mountain'/'EVG', '159061').

card_in_set('mountain', 'EVG').
card_original_type('mountain'/'EVG', 'Basic Land — Mountain').
card_original_text('mountain'/'EVG', '').
card_image_name('mountain'/'EVG', 'mountain3').
card_uid('mountain'/'EVG', 'EVG:Mountain:mountain3').
card_rarity('mountain'/'EVG', 'Basic Land').
card_artist('mountain'/'EVG', 'Rob Alexander').
card_number('mountain'/'EVG', '61').
card_multiverse_id('mountain'/'EVG', '157939').

card_in_set('mountain', 'EVG').
card_original_type('mountain'/'EVG', 'Basic Land — Mountain').
card_original_text('mountain'/'EVG', '').
card_image_name('mountain'/'EVG', 'mountain4').
card_uid('mountain'/'EVG', 'EVG:Mountain:mountain4').
card_rarity('mountain'/'EVG', 'Basic Land').
card_artist('mountain'/'EVG', 'Sam Wood').
card_number('mountain'/'EVG', '62').
card_multiverse_id('mountain'/'EVG', '157941').

card_in_set('mudbutton torchrunner', 'EVG').
card_original_type('mudbutton torchrunner'/'EVG', 'Creature — Goblin Warrior').
card_original_text('mudbutton torchrunner'/'EVG', 'When Mudbutton Torchrunner is put into a graveyard from play, it deals 3 damage to target creature or player.').
card_image_name('mudbutton torchrunner'/'EVG', 'mudbutton torchrunner').
card_uid('mudbutton torchrunner'/'EVG', 'EVG:Mudbutton Torchrunner:mudbutton torchrunner').
card_rarity('mudbutton torchrunner'/'EVG', 'Common').
card_artist('mudbutton torchrunner'/'EVG', 'Steve Ellis').
card_number('mudbutton torchrunner'/'EVG', '46').
card_flavor_text('mudbutton torchrunner'/'EVG', 'The oil sloshes against his skull as he nears his destination: the Frogtosser Games and the lighting of the Flaming Boggart.').
card_multiverse_id('mudbutton torchrunner'/'EVG', '157919').

card_in_set('raging goblin', 'EVG').
card_original_type('raging goblin'/'EVG', 'Creature — Goblin Berserker').
card_original_text('raging goblin'/'EVG', 'Haste').
card_image_name('raging goblin'/'EVG', 'raging goblin').
card_uid('raging goblin'/'EVG', 'EVG:Raging Goblin:raging goblin').
card_rarity('raging goblin'/'EVG', 'Common').
card_artist('raging goblin'/'EVG', 'Jeff Miracola').
card_number('raging goblin'/'EVG', '47').
card_flavor_text('raging goblin'/'EVG', 'He raged at the world, at his family, at his life. But mostly he just raged.').
card_multiverse_id('raging goblin'/'EVG', '159326').

card_in_set('reckless one', 'EVG').
card_original_type('reckless one'/'EVG', 'Creature — Goblin Avatar').
card_original_text('reckless one'/'EVG', 'Haste\nReckless One\'s power and toughness are each equal to the number of Goblins in play.').
card_image_name('reckless one'/'EVG', 'reckless one').
card_uid('reckless one'/'EVG', 'EVG:Reckless One:reckless one').
card_rarity('reckless one'/'EVG', 'Uncommon').
card_artist('reckless one'/'EVG', 'Ron Spencer').
card_number('reckless one'/'EVG', '48').
card_flavor_text('reckless one'/'EVG', '\"Release chaos with me!\"').
card_multiverse_id('reckless one'/'EVG', '159050').

card_in_set('siege-gang commander', 'EVG').
card_original_type('siege-gang commander'/'EVG', 'Creature — Goblin').
card_original_text('siege-gang commander'/'EVG', 'When Siege-Gang Commander comes into play, put three 1/1 red Goblin creature tokens into play.\n{1}{R}, Sacrifice a Goblin: Siege-Gang Commander deals 2 damage to target creature or player.').
card_image_name('siege-gang commander'/'EVG', 'siege-gang commander').
card_uid('siege-gang commander'/'EVG', 'EVG:Siege-Gang Commander:siege-gang commander').
card_rarity('siege-gang commander'/'EVG', 'Rare').
card_artist('siege-gang commander'/'EVG', 'Lucio Parrillo').
card_number('siege-gang commander'/'EVG', '32').
card_multiverse_id('siege-gang commander'/'EVG', '157926').

card_in_set('skirk drill sergeant', 'EVG').
card_original_type('skirk drill sergeant'/'EVG', 'Creature — Goblin').
card_original_text('skirk drill sergeant'/'EVG', 'Whenever Skirk Drill Sergeant or another Goblin is put into a graveyard from play, you may pay {2}{R}. If you do, reveal the top card of your library. If it\'s a Goblin permanent card, put it into play. Otherwise, put it into your graveyard.').
card_image_name('skirk drill sergeant'/'EVG', 'skirk drill sergeant').
card_uid('skirk drill sergeant'/'EVG', 'EVG:Skirk Drill Sergeant:skirk drill sergeant').
card_rarity('skirk drill sergeant'/'EVG', 'Uncommon').
card_artist('skirk drill sergeant'/'EVG', 'Alex Horley-Orlandelli').
card_number('skirk drill sergeant'/'EVG', '49').
card_flavor_text('skirk drill sergeant'/'EVG', '\"I order you to volunteer.\"').
card_multiverse_id('skirk drill sergeant'/'EVG', '157931').

card_in_set('skirk fire marshal', 'EVG').
card_original_type('skirk fire marshal'/'EVG', 'Creature — Goblin').
card_original_text('skirk fire marshal'/'EVG', 'Protection from red\nTap five untapped Goblins you control: Skirk Fire Marshal deals 10 damage to each creature and each player.').
card_image_name('skirk fire marshal'/'EVG', 'skirk fire marshal').
card_uid('skirk fire marshal'/'EVG', 'EVG:Skirk Fire Marshal:skirk fire marshal').
card_rarity('skirk fire marshal'/'EVG', 'Rare').
card_artist('skirk fire marshal'/'EVG', 'Greg & Tim Hildebrandt').
card_number('skirk fire marshal'/'EVG', '50').
card_flavor_text('skirk fire marshal'/'EVG', 'He\'s boss because he\'s smart enough to get out of the way.').
card_multiverse_id('skirk fire marshal'/'EVG', '157936').

card_in_set('skirk prospector', 'EVG').
card_original_type('skirk prospector'/'EVG', 'Creature — Goblin').
card_original_text('skirk prospector'/'EVG', 'Sacrifice a Goblin: Add {R} to your mana pool.').
card_image_name('skirk prospector'/'EVG', 'skirk prospector').
card_uid('skirk prospector'/'EVG', 'EVG:Skirk Prospector:skirk prospector').
card_rarity('skirk prospector'/'EVG', 'Common').
card_artist('skirk prospector'/'EVG', 'Doug Chaffee').
card_number('skirk prospector'/'EVG', '51').
card_flavor_text('skirk prospector'/'EVG', '\"I like goblins. They make funny little popping sounds when they die.\"\n—Braids, dementia summoner').
card_multiverse_id('skirk prospector'/'EVG', '159051').

card_in_set('skirk shaman', 'EVG').
card_original_type('skirk shaman'/'EVG', 'Creature — Goblin Shaman').
card_original_text('skirk shaman'/'EVG', 'Skirk Shaman can\'t be blocked except by artifact creatures and/or red creatures.').
card_image_name('skirk shaman'/'EVG', 'skirk shaman').
card_uid('skirk shaman'/'EVG', 'EVG:Skirk Shaman:skirk shaman').
card_rarity('skirk shaman'/'EVG', 'Common').
card_artist('skirk shaman'/'EVG', 'Chippy').
card_number('skirk shaman'/'EVG', '52').
card_flavor_text('skirk shaman'/'EVG', 'In a shaman\'s grimy hands, a concoction of dried Skirk Ridge herbs can become the face of panic.').
card_multiverse_id('skirk shaman'/'EVG', '159055').

card_in_set('slate of ancestry', 'EVG').
card_original_type('slate of ancestry'/'EVG', 'Artifact').
card_original_text('slate of ancestry'/'EVG', '{4}, {T}, Discard your hand: Draw a card for each creature you control.').
card_image_name('slate of ancestry'/'EVG', 'slate of ancestry').
card_uid('slate of ancestry'/'EVG', 'EVG:Slate of Ancestry:slate of ancestry').
card_rarity('slate of ancestry'/'EVG', 'Rare').
card_artist('slate of ancestry'/'EVG', 'Corey D. Macourek').
card_number('slate of ancestry'/'EVG', '25').
card_flavor_text('slate of ancestry'/'EVG', 'The pattern of life can be studied like a book, if you know how to read it.').
card_multiverse_id('slate of ancestry'/'EVG', '159501').

card_in_set('spitting earth', 'EVG').
card_original_type('spitting earth'/'EVG', 'Sorcery').
card_original_text('spitting earth'/'EVG', 'Spitting Earth deals damage equal to the number of Mountains you control to target creature.').
card_image_name('spitting earth'/'EVG', 'spitting earth').
card_uid('spitting earth'/'EVG', 'EVG:Spitting Earth:spitting earth').
card_rarity('spitting earth'/'EVG', 'Common').
card_artist('spitting earth'/'EVG', 'Michael Koelsch').
card_number('spitting earth'/'EVG', '55').
card_flavor_text('spitting earth'/'EVG', '\"Within each of you is the strength of a landslide.\"\n—Kumano, to his pupils').
card_multiverse_id('spitting earth'/'EVG', '159054').

card_in_set('stonewood invoker', 'EVG').
card_original_type('stonewood invoker'/'EVG', 'Creature — Elf Mutant').
card_original_text('stonewood invoker'/'EVG', '{7}{G}: Stonewood Invoker gets +5/+5 until end of turn.').
card_image_name('stonewood invoker'/'EVG', 'stonewood invoker').
card_uid('stonewood invoker'/'EVG', 'EVG:Stonewood Invoker:stonewood invoker').
card_rarity('stonewood invoker'/'EVG', 'Common').
card_artist('stonewood invoker'/'EVG', 'Eric Peterson').
card_number('stonewood invoker'/'EVG', '11').
card_flavor_text('stonewood invoker'/'EVG', 'The Mirari pulses in his veins.').
card_multiverse_id('stonewood invoker'/'EVG', '159324').

card_in_set('sylvan messenger', 'EVG').
card_original_type('sylvan messenger'/'EVG', 'Creature — Elf').
card_original_text('sylvan messenger'/'EVG', 'Trample\nWhen Sylvan Messenger comes into play, reveal the top four cards of your library. Put all Elf cards revealed this way into your hand and the rest on the bottom of your library.').
card_image_name('sylvan messenger'/'EVG', 'sylvan messenger').
card_uid('sylvan messenger'/'EVG', 'EVG:Sylvan Messenger:sylvan messenger').
card_rarity('sylvan messenger'/'EVG', 'Uncommon').
card_artist('sylvan messenger'/'EVG', 'Heather Hudson').
card_number('sylvan messenger'/'EVG', '12').
card_multiverse_id('sylvan messenger'/'EVG', '159035').

card_in_set('tar pitcher', 'EVG').
card_original_type('tar pitcher'/'EVG', 'Creature — Goblin Shaman').
card_original_text('tar pitcher'/'EVG', '{T}, Sacrifice a Goblin: Tar Pitcher deals 2 damage to target creature or player.').
card_image_name('tar pitcher'/'EVG', 'tar pitcher').
card_uid('tar pitcher'/'EVG', 'EVG:Tar Pitcher:tar pitcher').
card_rarity('tar pitcher'/'EVG', 'Uncommon').
card_artist('tar pitcher'/'EVG', 'Omar Rayyan').
card_number('tar pitcher'/'EVG', '53').
card_flavor_text('tar pitcher'/'EVG', '\"Auntie Grub had caught a goat, a bleating doe that still squirmed in her arms. At the same time, her warren decided to share with her a sensory greeting of hot tar . . . .\"\n—A tale of Auntie Grub').
card_multiverse_id('tar pitcher'/'EVG', '157920').

card_in_set('tarfire', 'EVG').
card_original_type('tarfire'/'EVG', 'Tribal Instant — Goblin').
card_original_text('tarfire'/'EVG', 'Tarfire deals 2 damage to target creature or player.').
card_image_name('tarfire'/'EVG', 'tarfire').
card_uid('tarfire'/'EVG', 'EVG:Tarfire:tarfire').
card_rarity('tarfire'/'EVG', 'Common').
card_artist('tarfire'/'EVG', 'Omar Rayyan').
card_number('tarfire'/'EVG', '56').
card_flavor_text('tarfire'/'EVG', '\"After Auntie brushed the soot from her eyes, she discovered something wonderful: the fire had turned the goat into something that smelled delicious.\"\n—A tale of Auntie Grub').
card_multiverse_id('tarfire'/'EVG', '157921').

card_in_set('timberwatch elf', 'EVG').
card_original_type('timberwatch elf'/'EVG', 'Creature — Elf').
card_original_text('timberwatch elf'/'EVG', '{T}: Target creature gets +X/+X until end of turn, where X is the number of Elves in play.').
card_image_name('timberwatch elf'/'EVG', 'timberwatch elf').
card_uid('timberwatch elf'/'EVG', 'EVG:Timberwatch Elf:timberwatch elf').
card_rarity('timberwatch elf'/'EVG', 'Common').
card_artist('timberwatch elf'/'EVG', 'Dave Dorman').
card_number('timberwatch elf'/'EVG', '13').
card_flavor_text('timberwatch elf'/'EVG', 'Even through the Mirari\'s voice, the elves still hear the call of their kinship.').
card_multiverse_id('timberwatch elf'/'EVG', '158118').

card_in_set('tranquil thicket', 'EVG').
card_original_type('tranquil thicket'/'EVG', 'Land').
card_original_text('tranquil thicket'/'EVG', 'Tranquil Thicket comes into play tapped.\n{T}: Add {G} to your mana pool.\nCycling {G} ({G}, Discard this card: Draw a card.)').
card_image_name('tranquil thicket'/'EVG', 'tranquil thicket').
card_uid('tranquil thicket'/'EVG', 'EVG:Tranquil Thicket:tranquil thicket').
card_rarity('tranquil thicket'/'EVG', 'Common').
card_artist('tranquil thicket'/'EVG', 'Heather Hudson').
card_number('tranquil thicket'/'EVG', '27').
card_multiverse_id('tranquil thicket'/'EVG', '159325').

card_in_set('voice of the woods', 'EVG').
card_original_type('voice of the woods'/'EVG', 'Creature — Elf').
card_original_text('voice of the woods'/'EVG', 'Tap five untapped Elves you control: Put a 7/7 green Elemental creature token with trample into play.').
card_image_name('voice of the woods'/'EVG', 'voice of the woods').
card_uid('voice of the woods'/'EVG', 'EVG:Voice of the Woods:voice of the woods').
card_rarity('voice of the woods'/'EVG', 'Rare').
card_artist('voice of the woods'/'EVG', 'Pete Venters').
card_number('voice of the woods'/'EVG', '14').
card_flavor_text('voice of the woods'/'EVG', 'The ritual of making draws upon the elves\' memories and pasts. And elves have long memories and ancient pasts.').
card_multiverse_id('voice of the woods'/'EVG', '159029').

card_in_set('wellwisher', 'EVG').
card_original_type('wellwisher'/'EVG', 'Creature — Elf').
card_original_text('wellwisher'/'EVG', '{T}: You gain 1 life for each Elf in play.').
card_image_name('wellwisher'/'EVG', 'wellwisher').
card_uid('wellwisher'/'EVG', 'EVG:Wellwisher:wellwisher').
card_rarity('wellwisher'/'EVG', 'Common').
card_artist('wellwisher'/'EVG', 'Christopher Rush').
card_number('wellwisher'/'EVG', '15').
card_flavor_text('wellwisher'/'EVG', '\"Close your ears to the voice of greed, and you can turn a gift for one into a gift for many.\"').
card_multiverse_id('wellwisher'/'EVG', '158109').

card_in_set('wildsize', 'EVG').
card_original_type('wildsize'/'EVG', 'Instant').
card_original_text('wildsize'/'EVG', 'Target creature gets +2/+2 and gains trample until end of turn.\nDraw a card.').
card_image_name('wildsize'/'EVG', 'wildsize').
card_uid('wildsize'/'EVG', 'EVG:Wildsize:wildsize').
card_rarity('wildsize'/'EVG', 'Common').
card_artist('wildsize'/'EVG', 'Jim Murray').
card_number('wildsize'/'EVG', '23').
card_flavor_text('wildsize'/'EVG', 'Two times the size, four times the smell.').
card_multiverse_id('wildsize'/'EVG', '159036').

card_in_set('wirewood herald', 'EVG').
card_original_type('wirewood herald'/'EVG', 'Creature — Elf').
card_original_text('wirewood herald'/'EVG', 'When Wirewood Herald is put into a graveyard from play, you may search your library for an Elf card. If you do, reveal that card and put it into your hand. Then shuffle your library.').
card_image_name('wirewood herald'/'EVG', 'wirewood herald').
card_uid('wirewood herald'/'EVG', 'EVG:Wirewood Herald:wirewood herald').
card_rarity('wirewood herald'/'EVG', 'Common').
card_artist('wirewood herald'/'EVG', 'Alex Horley-Orlandelli').
card_number('wirewood herald'/'EVG', '16').
card_flavor_text('wirewood herald'/'EVG', 'The goblins laughed as the elf ran away, until more came back.').
card_multiverse_id('wirewood herald'/'EVG', '158110').

card_in_set('wirewood lodge', 'EVG').
card_original_type('wirewood lodge'/'EVG', 'Land').
card_original_text('wirewood lodge'/'EVG', '{T}: Add {1} to your mana pool.\n{G}, {T}: Untap target Elf.').
card_image_name('wirewood lodge'/'EVG', 'wirewood lodge').
card_uid('wirewood lodge'/'EVG', 'EVG:Wirewood Lodge:wirewood lodge').
card_rarity('wirewood lodge'/'EVG', 'Uncommon').
card_artist('wirewood lodge'/'EVG', 'Anthony S. Waters').
card_number('wirewood lodge'/'EVG', '26').
card_multiverse_id('wirewood lodge'/'EVG', '158111').

card_in_set('wirewood symbiote', 'EVG').
card_original_type('wirewood symbiote'/'EVG', 'Creature — Insect').
card_original_text('wirewood symbiote'/'EVG', 'Return an Elf you control to its owner\'s hand: Untap target creature. Play this ability only once each turn.').
card_image_name('wirewood symbiote'/'EVG', 'wirewood symbiote').
card_uid('wirewood symbiote'/'EVG', 'EVG:Wirewood Symbiote:wirewood symbiote').
card_rarity('wirewood symbiote'/'EVG', 'Uncommon').
card_artist('wirewood symbiote'/'EVG', 'Thomas M. Baxa').
card_number('wirewood symbiote'/'EVG', '17').
card_flavor_text('wirewood symbiote'/'EVG', 'It drinks fatigue.').
card_multiverse_id('wirewood symbiote'/'EVG', '159322').

card_in_set('wood elves', 'EVG').
card_original_type('wood elves'/'EVG', 'Creature — Elf Scout').
card_original_text('wood elves'/'EVG', 'When Wood Elves comes into play, search your library for a Forest card and put that card into play. Then shuffle your library.').
card_image_name('wood elves'/'EVG', 'wood elves').
card_uid('wood elves'/'EVG', 'EVG:Wood Elves:wood elves').
card_rarity('wood elves'/'EVG', 'Common').
card_artist('wood elves'/'EVG', 'Christopher Moeller').
card_number('wood elves'/'EVG', '18').
card_flavor_text('wood elves'/'EVG', 'Every branch a crossroads, every vine a swift steed.').
card_multiverse_id('wood elves'/'EVG', '159038').

card_in_set('wren\'s run vanquisher', 'EVG').
card_original_type('wren\'s run vanquisher'/'EVG', 'Creature — Elf Warrior').
card_original_text('wren\'s run vanquisher'/'EVG', 'As an additional cost to play Wren\'s Run Vanquisher, reveal an Elf card from your hand or pay {3}.\nDeathtouch (Whenever this creature deals damage to a creature, destroy that creature.)').
card_image_name('wren\'s run vanquisher'/'EVG', 'wren\'s run vanquisher').
card_uid('wren\'s run vanquisher'/'EVG', 'EVG:Wren\'s Run Vanquisher:wren\'s run vanquisher').
card_rarity('wren\'s run vanquisher'/'EVG', 'Uncommon').
card_artist('wren\'s run vanquisher'/'EVG', 'Paolo Parente').
card_number('wren\'s run vanquisher'/'EVG', '19').
card_multiverse_id('wren\'s run vanquisher'/'EVG', '158129').
