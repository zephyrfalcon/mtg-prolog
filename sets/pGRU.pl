% Guru

set('pGRU').
set_name('pGRU', 'Guru').
set_release_date('pGRU', '1999-07-12').
set_border('pGRU', 'black').
set_type('pGRU', 'promo').

card_in_set('forest', 'pGRU').
card_original_type('forest'/'pGRU', 'Basic Land — Forest').
card_original_text('forest'/'pGRU', '').
card_image_name('forest'/'pGRU', 'forest').
card_uid('forest'/'pGRU', 'pGRU:Forest:forest').
card_rarity('forest'/'pGRU', 'Basic Land').
card_artist('forest'/'pGRU', 'Terese Nielsen').
card_number('forest'/'pGRU', '1').

card_in_set('island', 'pGRU').
card_original_type('island'/'pGRU', 'Basic Land — Island').
card_original_text('island'/'pGRU', '').
card_image_name('island'/'pGRU', 'island').
card_uid('island'/'pGRU', 'pGRU:Island:island').
card_rarity('island'/'pGRU', 'Basic Land').
card_artist('island'/'pGRU', 'Terese Nielsen').
card_number('island'/'pGRU', '2').

card_in_set('mountain', 'pGRU').
card_original_type('mountain'/'pGRU', 'Basic Land — Mountain').
card_original_text('mountain'/'pGRU', '').
card_image_name('mountain'/'pGRU', 'mountain').
card_uid('mountain'/'pGRU', 'pGRU:Mountain:mountain').
card_rarity('mountain'/'pGRU', 'Basic Land').
card_artist('mountain'/'pGRU', 'Terese Nielsen').
card_number('mountain'/'pGRU', '3').

card_in_set('plains', 'pGRU').
card_original_type('plains'/'pGRU', 'Basic Land — Plains').
card_original_text('plains'/'pGRU', '').
card_image_name('plains'/'pGRU', 'plains').
card_uid('plains'/'pGRU', 'pGRU:Plains:plains').
card_rarity('plains'/'pGRU', 'Basic Land').
card_artist('plains'/'pGRU', 'Terese Nielsen').
card_number('plains'/'pGRU', '4').

card_in_set('swamp', 'pGRU').
card_original_type('swamp'/'pGRU', 'Basic Land — Swamp').
card_original_text('swamp'/'pGRU', '').
card_image_name('swamp'/'pGRU', 'swamp').
card_uid('swamp'/'pGRU', 'pGRU:Swamp:swamp').
card_rarity('swamp'/'pGRU', 'Basic Land').
card_artist('swamp'/'pGRU', 'Terese Nielsen').
card_number('swamp'/'pGRU', '5').
