% Dragon's Maze

set('DGM').
set_name('DGM', 'Dragon\'s Maze').
set_release_date('DGM', '2013-05-03').
set_border('DGM', 'black').
set_type('DGM', 'expansion').
set_block('DGM', 'Return to Ravnica').

card_in_set('advent of the wurm', 'DGM').
card_original_type('advent of the wurm'/'DGM', 'Instant').
card_original_text('advent of the wurm'/'DGM', 'Put a 5/5 green Wurm creature token with trample onto the battlefield.').
card_first_print('advent of the wurm', 'DGM').
card_image_name('advent of the wurm'/'DGM', 'advent of the wurm').
card_uid('advent of the wurm'/'DGM', 'DGM:Advent of the Wurm:advent of the wurm').
card_rarity('advent of the wurm'/'DGM', 'Rare').
card_artist('advent of the wurm'/'DGM', 'Lucas Graciano').
card_number('advent of the wurm'/'DGM', '51').
card_flavor_text('advent of the wurm'/'DGM', 'The consciousness of Mat\'Selesnya does not always spread in peaceful ways.').
card_multiverse_id('advent of the wurm'/'DGM', '369036').
card_watermark('advent of the wurm'/'DGM', 'Selesnya').

card_in_set('ætherling', 'DGM').
card_original_type('ætherling'/'DGM', 'Creature — Shapeshifter').
card_original_text('ætherling'/'DGM', '{U}: Exile Ætherling. Return it to the battlefield under its owner\'s control at the beginning of the next end step.\n{U}: Ætherling is unblockable this turn.\n{1}: Ætherling gets +1/-1 until end of turn.\n{1}: Ætherling gets -1/+1 until end of turn.').
card_first_print('ætherling', 'DGM').
card_image_name('ætherling'/'DGM', 'aetherling').
card_uid('ætherling'/'DGM', 'DGM:Ætherling:aetherling').
card_rarity('ætherling'/'DGM', 'Rare').
card_artist('ætherling'/'DGM', 'Tyler Jacobson').
card_number('ætherling'/'DGM', '11').
card_multiverse_id('ætherling'/'DGM', '368961').

card_in_set('alive', 'DGM').
card_original_type('alive'/'DGM', 'Sorcery').
card_original_text('alive'/'DGM', 'Put a 3/3 green Centaur creature token onto the battlefield.\n//\nWell\n{W}\nSorcery\nYou gain 2 life for each creature you control.\n//\nFuse (You may cast one or both halves of this card from your hand.)').
card_first_print('alive', 'DGM').
card_image_name('alive'/'DGM', 'alivewell').
card_uid('alive'/'DGM', 'DGM:Alive:alivewell').
card_rarity('alive'/'DGM', 'Uncommon').
card_artist('alive'/'DGM', 'Nils Hamm').
card_number('alive'/'DGM', '121a').
card_multiverse_id('alive'/'DGM', '369041').

card_in_set('armed', 'DGM').
card_original_type('armed'/'DGM', 'Sorcery').
card_original_text('armed'/'DGM', 'Target creature gets +1/+1 and gains double strike until end of turn.\n//\nDangerous\n{3}{G}\nSorcery\nAll creatures able to block target creature this turn do so.\n//\nFuse (You may cast one or both halves of this card from your hand.)').
card_first_print('armed', 'DGM').
card_image_name('armed'/'DGM', 'armeddangerous').
card_uid('armed'/'DGM', 'DGM:Armed:armeddangerous').
card_rarity('armed'/'DGM', 'Uncommon').
card_artist('armed'/'DGM', 'David Palumbo').
card_number('armed'/'DGM', '122a').
card_multiverse_id('armed'/'DGM', '368989').

card_in_set('armored wolf-rider', 'DGM').
card_original_type('armored wolf-rider'/'DGM', 'Creature — Elf Knight').
card_original_text('armored wolf-rider'/'DGM', '').
card_first_print('armored wolf-rider', 'DGM').
card_image_name('armored wolf-rider'/'DGM', 'armored wolf-rider').
card_uid('armored wolf-rider'/'DGM', 'DGM:Armored Wolf-Rider:armored wolf-rider').
card_rarity('armored wolf-rider'/'DGM', 'Common').
card_artist('armored wolf-rider'/'DGM', 'Matt Stewart').
card_number('armored wolf-rider'/'DGM', '52').
card_flavor_text('armored wolf-rider'/'DGM', 'Wolf-riders of Selesnya apprentice from a young age. Each rider raises a wolf pup from birth to serve as a mount, hoping that one day the two will share the honor of guarding the Great Concourse.').
card_multiverse_id('armored wolf-rider'/'DGM', '369082').
card_watermark('armored wolf-rider'/'DGM', 'Selesnya').

card_in_set('ascended lawmage', 'DGM').
card_original_type('ascended lawmage'/'DGM', 'Creature — Vedalken Wizard').
card_original_text('ascended lawmage'/'DGM', 'Flying, hexproof').
card_first_print('ascended lawmage', 'DGM').
card_image_name('ascended lawmage'/'DGM', 'ascended lawmage').
card_uid('ascended lawmage'/'DGM', 'DGM:Ascended Lawmage:ascended lawmage').
card_rarity('ascended lawmage'/'DGM', 'Uncommon').
card_artist('ascended lawmage'/'DGM', 'Ryan Yee').
card_number('ascended lawmage'/'DGM', '53').
card_flavor_text('ascended lawmage'/'DGM', 'A lawmage\'s runic script is an act of governance given form: legislation written directly onto the air itself.').
card_multiverse_id('ascended lawmage'/'DGM', '369016').
card_watermark('ascended lawmage'/'DGM', 'Azorius').

card_in_set('away', 'DGM').
card_original_type('away'/'DGM', 'Instant').
card_original_text('away'/'DGM', 'Return target creature to its owner\'s hand.\n//\nAway\n{2}{B}\nInstant\nTarget player sacrifices a creature.\n//\nFuse (You may cast one or both halves of this card from your hand.)').
card_first_print('away', 'DGM').
card_image_name('away'/'DGM', 'faraway').
card_uid('away'/'DGM', 'DGM:Away:faraway').
card_rarity('away'/'DGM', 'Uncommon').
card_artist('away'/'DGM', 'Greg Staples').
card_number('away'/'DGM', '127b').
card_multiverse_id('away'/'DGM', '369042').

card_in_set('awe for the guilds', 'DGM').
card_original_type('awe for the guilds'/'DGM', 'Sorcery').
card_original_text('awe for the guilds'/'DGM', 'Monocolored creatures can\'t block this turn.').
card_first_print('awe for the guilds', 'DGM').
card_image_name('awe for the guilds'/'DGM', 'awe for the guilds').
card_uid('awe for the guilds'/'DGM', 'DGM:Awe for the Guilds:awe for the guilds').
card_rarity('awe for the guilds'/'DGM', 'Common').
card_artist('awe for the guilds'/'DGM', 'Mathias Kollros').
card_number('awe for the guilds'/'DGM', '31').
card_flavor_text('awe for the guilds'/'DGM', 'When the guilds cooperate, the guildless celebrate their peaceful society. When the guilds clash, the guildless just try to keep out of the way.').
card_multiverse_id('awe for the guilds'/'DGM', '369053').

card_in_set('azorius cluestone', 'DGM').
card_original_type('azorius cluestone'/'DGM', 'Artifact').
card_original_text('azorius cluestone'/'DGM', '{T}: Add {W} or {U} to your mana pool.\n{W}{U}, {T}, Sacrifice Azorius Cluestone: Draw a card.').
card_first_print('azorius cluestone', 'DGM').
card_image_name('azorius cluestone'/'DGM', 'azorius cluestone').
card_uid('azorius cluestone'/'DGM', 'DGM:Azorius Cluestone:azorius cluestone').
card_rarity('azorius cluestone'/'DGM', 'Common').
card_artist('azorius cluestone'/'DGM', 'Raoul Vitale').
card_number('azorius cluestone'/'DGM', '136').
card_flavor_text('azorius cluestone'/'DGM', 'Its three sides represent the Sova, judges and arbitrators; the Jelenn, scribes and elocutors; and the Lyev, lawmages and enforcers.').
card_multiverse_id('azorius cluestone'/'DGM', '369017').
card_watermark('azorius cluestone'/'DGM', 'Azorius').

card_in_set('azorius guildgate', 'DGM').
card_original_type('azorius guildgate'/'DGM', 'Land — Gate').
card_original_text('azorius guildgate'/'DGM', 'Azorius Guildgate enters the battlefield tapped.\n{T}: Add {W} or {U} to your mana pool.').
card_image_name('azorius guildgate'/'DGM', 'azorius guildgate').
card_uid('azorius guildgate'/'DGM', 'DGM:Azorius Guildgate:azorius guildgate').
card_rarity('azorius guildgate'/'DGM', 'Common').
card_artist('azorius guildgate'/'DGM', 'Drew Baker').
card_number('azorius guildgate'/'DGM', '146').
card_flavor_text('azorius guildgate'/'DGM', 'The Azorius symbol stares down like a great eye, reminding visitors of the watchful presence of Isperia and her lawmages.').
card_multiverse_id('azorius guildgate'/'DGM', '368974').
card_watermark('azorius guildgate'/'DGM', 'Azorius').

card_in_set('bane alley blackguard', 'DGM').
card_original_type('bane alley blackguard'/'DGM', 'Creature — Human Rogue').
card_original_text('bane alley blackguard'/'DGM', '').
card_first_print('bane alley blackguard', 'DGM').
card_image_name('bane alley blackguard'/'DGM', 'bane alley blackguard').
card_uid('bane alley blackguard'/'DGM', 'DGM:Bane Alley Blackguard:bane alley blackguard').
card_rarity('bane alley blackguard'/'DGM', 'Common').
card_artist('bane alley blackguard'/'DGM', 'Mike Bierek').
card_number('bane alley blackguard'/'DGM', '21').
card_flavor_text('bane alley blackguard'/'DGM', '\"I\'m in the field of procurement, and business is good. The guilds want all kinds of maps and relics these days, though what they want them for I\'m not quite sure.\"').
card_multiverse_id('bane alley blackguard'/'DGM', '369044').

card_in_set('battering krasis', 'DGM').
card_original_type('battering krasis'/'DGM', 'Creature — Fish Beast').
card_original_text('battering krasis'/'DGM', 'Trample\nEvolve (Whenever a creature enters the battlefield under your control, if that creature has greater power or toughness than this creature, put a +1/+1 counter on this creature.)').
card_first_print('battering krasis', 'DGM').
card_image_name('battering krasis'/'DGM', 'battering krasis').
card_uid('battering krasis'/'DGM', 'DGM:Battering Krasis:battering krasis').
card_rarity('battering krasis'/'DGM', 'Common').
card_artist('battering krasis'/'DGM', 'Jack Wang').
card_number('battering krasis'/'DGM', '41').
card_multiverse_id('battering krasis'/'DGM', '368984').
card_watermark('battering krasis'/'DGM', 'Simic').

card_in_set('beck', 'DGM').
card_original_type('beck'/'DGM', 'Sorcery').
card_original_text('beck'/'DGM', 'Whenever a creature enters the battlefield this turn, you may draw a card.\n//\nCall\n{4}{W}{U}\nSorcery\nPut four 1/1 white Bird creature tokens with flying onto the battlefield.\n//\nFuse (You may cast one or both halves of this card from your hand.)').
card_first_print('beck', 'DGM').
card_image_name('beck'/'DGM', 'beckcall').
card_uid('beck'/'DGM', 'DGM:Beck:beckcall').
card_rarity('beck'/'DGM', 'Rare').
card_artist('beck'/'DGM', 'Adam Paquette').
card_number('beck'/'DGM', '123a').
card_multiverse_id('beck'/'DGM', '369063').

card_in_set('beetleform mage', 'DGM').
card_original_type('beetleform mage'/'DGM', 'Creature — Human Insect Wizard').
card_original_text('beetleform mage'/'DGM', '{G}{U}: Beetleform Mage gets +2/+2 and gains flying until end of turn. Activate this ability only once each turn.').
card_first_print('beetleform mage', 'DGM').
card_image_name('beetleform mage'/'DGM', 'beetleform mage').
card_uid('beetleform mage'/'DGM', 'DGM:Beetleform Mage:beetleform mage').
card_rarity('beetleform mage'/'DGM', 'Common').
card_artist('beetleform mage'/'DGM', 'Marco Nelor').
card_number('beetleform mage'/'DGM', '54').
card_flavor_text('beetleform mage'/'DGM', '\"When the Simic say to take command of your life, we don\'t mean just your career.\"\n—Vorel of the Hull Clade').
card_multiverse_id('beetleform mage'/'DGM', '369000').
card_watermark('beetleform mage'/'DGM', 'Simic').

card_in_set('blast of genius', 'DGM').
card_original_type('blast of genius'/'DGM', 'Sorcery').
card_original_text('blast of genius'/'DGM', 'Choose target creature or player. Draw three cards, then discard a card. Blast of Genius deals damage equal to the discarded card\'s converted mana cost to that creature or player.').
card_first_print('blast of genius', 'DGM').
card_image_name('blast of genius'/'DGM', 'blast of genius').
card_uid('blast of genius'/'DGM', 'DGM:Blast of Genius:blast of genius').
card_rarity('blast of genius'/'DGM', 'Uncommon').
card_artist('blast of genius'/'DGM', 'Terese Nielsen').
card_number('blast of genius'/'DGM', '55').
card_flavor_text('blast of genius'/'DGM', 'Ral Zarek\'s brainstorms bring actual thunder and lightning.').
card_multiverse_id('blast of genius'/'DGM', '368985').
card_watermark('blast of genius'/'DGM', 'Izzet').

card_in_set('blaze commando', 'DGM').
card_original_type('blaze commando'/'DGM', 'Creature — Minotaur Soldier').
card_original_text('blaze commando'/'DGM', 'Whenever an instant or sorcery spell you control deals damage, put two 1/1 red and white Soldier creature tokens with haste onto the battlefield.').
card_first_print('blaze commando', 'DGM').
card_image_name('blaze commando'/'DGM', 'blaze commando').
card_uid('blaze commando'/'DGM', 'DGM:Blaze Commando:blaze commando').
card_rarity('blaze commando'/'DGM', 'Uncommon').
card_artist('blaze commando'/'DGM', 'James Ryman').
card_number('blaze commando'/'DGM', '56').
card_flavor_text('blaze commando'/'DGM', 'A true Boros commando only calls for reinforcements so they can clean up the mess.').
card_multiverse_id('blaze commando'/'DGM', '369047').
card_watermark('blaze commando'/'DGM', 'Boros').

card_in_set('blood', 'DGM').
card_original_type('blood'/'DGM', 'Sorcery').
card_original_text('blood'/'DGM', 'Exile target creature card from a graveyard. Put X +1/+1 counters on target creature, where X is the power of the card you exiled.\n//\nBlood\n{R}{G}\nSorcery\nTarget creature you control deals damage equal to its power to target creature or player.\n//\nFuse (You may cast one or both halves of this card from your hand.)').
card_first_print('blood', 'DGM').
card_image_name('blood'/'DGM', 'fleshblood').
card_uid('blood'/'DGM', 'DGM:Blood:fleshblood').
card_rarity('blood'/'DGM', 'Rare').
card_artist('blood'/'DGM', 'Lucas Graciano').
card_number('blood'/'DGM', '128b').
card_multiverse_id('blood'/'DGM', '368991').

card_in_set('blood baron of vizkopa', 'DGM').
card_original_type('blood baron of vizkopa'/'DGM', 'Creature — Vampire').
card_original_text('blood baron of vizkopa'/'DGM', 'Lifelink, protection from white and from black\nAs long as you have 30 or more life and an opponent has 10 or less life, Blood Baron of Vizkopa gets +6/+6 and has flying.').
card_first_print('blood baron of vizkopa', 'DGM').
card_image_name('blood baron of vizkopa'/'DGM', 'blood baron of vizkopa').
card_uid('blood baron of vizkopa'/'DGM', 'DGM:Blood Baron of Vizkopa:blood baron of vizkopa').
card_rarity('blood baron of vizkopa'/'DGM', 'Mythic Rare').
card_artist('blood baron of vizkopa'/'DGM', 'Anthony Palumbo').
card_number('blood baron of vizkopa'/'DGM', '57').
card_multiverse_id('blood baron of vizkopa'/'DGM', '368954').
card_watermark('blood baron of vizkopa'/'DGM', 'Orzhov').

card_in_set('blood scrivener', 'DGM').
card_original_type('blood scrivener'/'DGM', 'Creature — Zombie Wizard').
card_original_text('blood scrivener'/'DGM', 'If you would draw a card while you have no cards in hand, instead draw two cards and lose 1 life.').
card_first_print('blood scrivener', 'DGM').
card_image_name('blood scrivener'/'DGM', 'blood scrivener').
card_uid('blood scrivener'/'DGM', 'DGM:Blood Scrivener:blood scrivener').
card_rarity('blood scrivener'/'DGM', 'Rare').
card_artist('blood scrivener'/'DGM', 'Peter Mohrbacher').
card_number('blood scrivener'/'DGM', '22').
card_flavor_text('blood scrivener'/'DGM', 'Make sure you bleed the fine print.').
card_multiverse_id('blood scrivener'/'DGM', '369030').

card_in_set('boros battleshaper', 'DGM').
card_original_type('boros battleshaper'/'DGM', 'Creature — Minotaur Soldier').
card_original_text('boros battleshaper'/'DGM', 'At the beginning of each combat, up to one target creature attacks or blocks this combat if able and up to one target creature can\'t attack or block this combat.').
card_first_print('boros battleshaper', 'DGM').
card_image_name('boros battleshaper'/'DGM', 'boros battleshaper').
card_uid('boros battleshaper'/'DGM', 'DGM:Boros Battleshaper:boros battleshaper').
card_rarity('boros battleshaper'/'DGM', 'Rare').
card_artist('boros battleshaper'/'DGM', 'Zoltan Boros').
card_number('boros battleshaper'/'DGM', '58').
card_flavor_text('boros battleshaper'/'DGM', 'Leaders shape the minds of their allies. It takes a master to shape the minds of enemies.').
card_multiverse_id('boros battleshaper'/'DGM', '369051').
card_watermark('boros battleshaper'/'DGM', 'Boros').

card_in_set('boros cluestone', 'DGM').
card_original_type('boros cluestone'/'DGM', 'Artifact').
card_original_text('boros cluestone'/'DGM', '{T}: Add {R} or {W} to your mana pool.\n{R}{W}, {T}, Sacrifice Boros Cluestone: Draw a card.').
card_first_print('boros cluestone', 'DGM').
card_image_name('boros cluestone'/'DGM', 'boros cluestone').
card_uid('boros cluestone'/'DGM', 'DGM:Boros Cluestone:boros cluestone').
card_rarity('boros cluestone'/'DGM', 'Common').
card_artist('boros cluestone'/'DGM', 'Raoul Vitale').
card_number('boros cluestone'/'DGM', '137').
card_flavor_text('boros cluestone'/'DGM', '\"In case of fire, treachery, citywide riot, political upheaval, or worldwide societal collapse, break glass.\"\n—Cluestone inscription').
card_multiverse_id('boros cluestone'/'DGM', '368997').
card_watermark('boros cluestone'/'DGM', 'Boros').

card_in_set('boros guildgate', 'DGM').
card_original_type('boros guildgate'/'DGM', 'Land — Gate').
card_original_text('boros guildgate'/'DGM', 'Boros Guildgate enters the battlefield tapped.\n{T}: Add {R} or {W} to your mana pool.').
card_image_name('boros guildgate'/'DGM', 'boros guildgate').
card_uid('boros guildgate'/'DGM', 'DGM:Boros Guildgate:boros guildgate').
card_rarity('boros guildgate'/'DGM', 'Common').
card_artist('boros guildgate'/'DGM', 'Noah Bradley').
card_number('boros guildgate'/'DGM', '147').
card_flavor_text('boros guildgate'/'DGM', 'It promises protection to those in need and proclaims a warning to any who would threaten Ravnican law.').
card_multiverse_id('boros guildgate'/'DGM', '369008').
card_watermark('boros guildgate'/'DGM', 'Boros').

card_in_set('boros mastiff', 'DGM').
card_original_type('boros mastiff'/'DGM', 'Creature — Hound').
card_original_text('boros mastiff'/'DGM', 'Battalion — Whenever Boros Mastiff and at least two other creatures attack, Boros Mastiff gains lifelink until end of turn. (Damage dealt by a creature with lifelink also causes its controller to gain that much life.)').
card_first_print('boros mastiff', 'DGM').
card_image_name('boros mastiff'/'DGM', 'boros mastiff').
card_uid('boros mastiff'/'DGM', 'DGM:Boros Mastiff:boros mastiff').
card_rarity('boros mastiff'/'DGM', 'Common').
card_artist('boros mastiff'/'DGM', 'Kev Walker').
card_number('boros mastiff'/'DGM', '1').
card_multiverse_id('boros mastiff'/'DGM', '369028').
card_watermark('boros mastiff'/'DGM', 'Boros').

card_in_set('breaking', 'DGM').
card_original_type('breaking'/'DGM', 'Sorcery').
card_original_text('breaking'/'DGM', 'Target player puts the top eight cards of his or her library into his or her graveyard.\n//\nEntering\n{4}{B}{R}\nSorcery\nPut a creature card from a graveyard onto the battlefield under your control. It gains haste until end of turn.\n//\nFuse (You may cast one or both halves of this card from your hand.)').
card_image_name('breaking'/'DGM', 'breakingentering').
card_uid('breaking'/'DGM', 'DGM:Breaking:breakingentering').
card_rarity('breaking'/'DGM', 'Rare').
card_artist('breaking'/'DGM', 'Mathias Kollros').
card_number('breaking'/'DGM', '124a').
card_multiverse_id('breaking'/'DGM', '369009').

card_in_set('bred for the hunt', 'DGM').
card_original_type('bred for the hunt'/'DGM', 'Enchantment').
card_original_text('bred for the hunt'/'DGM', 'Whenever a creature you control with a +1/+1 counter on it deals combat damage to a player, you may draw a card.').
card_first_print('bred for the hunt', 'DGM').
card_image_name('bred for the hunt'/'DGM', 'bred for the hunt').
card_uid('bred for the hunt'/'DGM', 'DGM:Bred for the Hunt:bred for the hunt').
card_rarity('bred for the hunt'/'DGM', 'Uncommon').
card_artist('bred for the hunt'/'DGM', 'Karl Kopinski').
card_number('bred for the hunt'/'DGM', '59').
card_flavor_text('bred for the hunt'/'DGM', 'Some see the world as a place of infinite wonder and knowledge. Some see it as an infinite dinner plate.').
card_multiverse_id('bred for the hunt'/'DGM', '368953').
card_watermark('bred for the hunt'/'DGM', 'Simic').

card_in_set('bronzebeak moa', 'DGM').
card_original_type('bronzebeak moa'/'DGM', 'Creature — Bird').
card_original_text('bronzebeak moa'/'DGM', 'Whenever another creature enters the battlefield under your control, Bronzebeak Moa gets +3/+3 until end of turn.').
card_first_print('bronzebeak moa', 'DGM').
card_image_name('bronzebeak moa'/'DGM', 'bronzebeak moa').
card_uid('bronzebeak moa'/'DGM', 'DGM:Bronzebeak Moa:bronzebeak moa').
card_rarity('bronzebeak moa'/'DGM', 'Uncommon').
card_artist('bronzebeak moa'/'DGM', 'James Ryman').
card_number('bronzebeak moa'/'DGM', '60').
card_flavor_text('bronzebeak moa'/'DGM', 'They fight for their home with more loyalty than any conscript.').
card_multiverse_id('bronzebeak moa'/'DGM', '368971').
card_watermark('bronzebeak moa'/'DGM', 'Selesnya').

card_in_set('burn', 'DGM').
card_original_type('burn'/'DGM', 'Instant').
card_original_text('burn'/'DGM', 'Target creature loses all abilities and becomes a 0/1 red Weird until end of turn.\n//\nBurn\n{1}{R}\nInstant\nBurn deals 2 damage to target creature or player.\n//\nFuse (You may cast one or both halves of this card from your hand.)').
card_first_print('burn', 'DGM').
card_image_name('burn'/'DGM', 'turnburn').
card_uid('burn'/'DGM', 'DGM:Burn:turnburn').
card_rarity('burn'/'DGM', 'Uncommon').
card_artist('burn'/'DGM', 'Ryan Barger').
card_number('burn'/'DGM', '134b').
card_multiverse_id('burn'/'DGM', '369080').

card_in_set('call', 'DGM').
card_original_type('call'/'DGM', 'Sorcery').
card_original_text('call'/'DGM', 'Whenever a creature enters the battlefield this turn, you may draw a card.\n//\nCall\n{4}{W}{U}\nSorcery\nPut four 1/1 white Bird creature tokens with flying onto the battlefield.\n//\nFuse (You may cast one or both halves of this card from your hand.)').
card_first_print('call', 'DGM').
card_image_name('call'/'DGM', 'beckcall').
card_uid('call'/'DGM', 'DGM:Call:beckcall').
card_rarity('call'/'DGM', 'Rare').
card_artist('call'/'DGM', 'Adam Paquette').
card_number('call'/'DGM', '123b').
card_multiverse_id('call'/'DGM', '369063').

card_in_set('carnage gladiator', 'DGM').
card_original_type('carnage gladiator'/'DGM', 'Creature — Skeleton Warrior').
card_original_text('carnage gladiator'/'DGM', 'Whenever a creature blocks, that creature\'s controller loses 1 life.\n{1}{B}{R}: Regenerate Carnage Gladiator.').
card_first_print('carnage gladiator', 'DGM').
card_image_name('carnage gladiator'/'DGM', 'carnage gladiator').
card_uid('carnage gladiator'/'DGM', 'DGM:Carnage Gladiator:carnage gladiator').
card_rarity('carnage gladiator'/'DGM', 'Uncommon').
card_artist('carnage gladiator'/'DGM', 'Ryan Barger').
card_number('carnage gladiator'/'DGM', '61').
card_flavor_text('carnage gladiator'/'DGM', 'Not the foulest or bloodiest act at Rix Maadi by a stretch.').
card_multiverse_id('carnage gladiator'/'DGM', '369057').
card_watermark('carnage gladiator'/'DGM', 'Rakdos').

card_in_set('catch', 'DGM').
card_original_type('catch'/'DGM', 'Sorcery').
card_original_text('catch'/'DGM', 'Gain control of target permanent until end of turn. Untap it. It gains haste until end of turn.\n//\nRelease\n{4}{R}{W}\nSorcery\nEach player sacrifices an artifact, a creature, an enchantment, a land, and a planeswalker.\n//\nFuse (You may cast one or both halves of this card from your hand.)').
card_first_print('catch', 'DGM').
card_image_name('catch'/'DGM', 'catchrelease').
card_uid('catch'/'DGM', 'DGM:Catch:catchrelease').
card_rarity('catch'/'DGM', 'Rare').
card_artist('catch'/'DGM', 'Kev Walker').
card_number('catch'/'DGM', '125a').
card_multiverse_id('catch'/'DGM', '368982').

card_in_set('clear a path', 'DGM').
card_original_type('clear a path'/'DGM', 'Sorcery').
card_original_text('clear a path'/'DGM', 'Destroy target creature with defender.').
card_first_print('clear a path', 'DGM').
card_image_name('clear a path'/'DGM', 'clear a path').
card_uid('clear a path'/'DGM', 'DGM:Clear a Path:clear a path').
card_rarity('clear a path'/'DGM', 'Common').
card_artist('clear a path'/'DGM', 'Karl Kopinski').
card_number('clear a path'/'DGM', '32').
card_flavor_text('clear a path'/'DGM', '\"Why do guards always look surprised when we bash them?\" asked Ruric.\n\"I think they expect a bribe,\" said Thar.').
card_multiverse_id('clear a path'/'DGM', '369029').

card_in_set('council of the absolute', 'DGM').
card_original_type('council of the absolute'/'DGM', 'Creature — Human Advisor').
card_original_text('council of the absolute'/'DGM', 'As Council of the Absolute enters the battlefield, name a card other than a creature or land card.\nYour opponents can\'t cast cards with the chosen name.\nSpells with the chosen name you cast cost {2} less to cast.').
card_first_print('council of the absolute', 'DGM').
card_image_name('council of the absolute'/'DGM', 'council of the absolute').
card_uid('council of the absolute'/'DGM', 'DGM:Council of the Absolute:council of the absolute').
card_rarity('council of the absolute'/'DGM', 'Mythic Rare').
card_artist('council of the absolute'/'DGM', 'Zoltan Boros').
card_number('council of the absolute'/'DGM', '62').
card_multiverse_id('council of the absolute'/'DGM', '368999').
card_watermark('council of the absolute'/'DGM', 'Azorius').

card_in_set('crypt incursion', 'DGM').
card_original_type('crypt incursion'/'DGM', 'Instant').
card_original_text('crypt incursion'/'DGM', 'Exile all creature cards from target player\'s graveyard. You gain 3 life for each card exiled this way.').
card_first_print('crypt incursion', 'DGM').
card_image_name('crypt incursion'/'DGM', 'crypt incursion').
card_uid('crypt incursion'/'DGM', 'DGM:Crypt Incursion:crypt incursion').
card_rarity('crypt incursion'/'DGM', 'Common').
card_artist('crypt incursion'/'DGM', 'Svetlin Velinov').
card_number('crypt incursion'/'DGM', '23').
card_flavor_text('crypt incursion'/'DGM', 'The Golgari maze-runner Varolz hungers to prove his fortitude in battle, and his trail of victims feeds other hungers.').
card_multiverse_id('crypt incursion'/'DGM', '369056').

card_in_set('dangerous', 'DGM').
card_original_type('dangerous'/'DGM', 'Sorcery').
card_original_text('dangerous'/'DGM', 'Target creature gets +1/+1 and gains double strike until end of turn.\n//\nDangerous\n{3}{G}\nSorcery\nAll creatures able to block target creature this turn do so.\n//\nFuse (You may cast one or both halves of this card from your hand.)').
card_first_print('dangerous', 'DGM').
card_image_name('dangerous'/'DGM', 'armeddangerous').
card_uid('dangerous'/'DGM', 'DGM:Dangerous:armeddangerous').
card_rarity('dangerous'/'DGM', 'Uncommon').
card_artist('dangerous'/'DGM', 'David Palumbo').
card_number('dangerous'/'DGM', '122b').
card_multiverse_id('dangerous'/'DGM', '368989').

card_in_set('deadbridge chant', 'DGM').
card_original_type('deadbridge chant'/'DGM', 'Enchantment').
card_original_text('deadbridge chant'/'DGM', 'When Deadbridge Chant enters the battlefield, put the top ten cards of your library into your graveyard.\nAt the beginning of your upkeep, choose a card at random in your graveyard. If it\'s a creature card, put it onto the battlefield. Otherwise, put it into your hand.').
card_first_print('deadbridge chant', 'DGM').
card_image_name('deadbridge chant'/'DGM', 'deadbridge chant').
card_uid('deadbridge chant'/'DGM', 'DGM:Deadbridge Chant:deadbridge chant').
card_rarity('deadbridge chant'/'DGM', 'Mythic Rare').
card_artist('deadbridge chant'/'DGM', 'Zoltan Boros').
card_number('deadbridge chant'/'DGM', '63').
card_multiverse_id('deadbridge chant'/'DGM', '369003').
card_watermark('deadbridge chant'/'DGM', 'Golgari').

card_in_set('debt to the deathless', 'DGM').
card_original_type('debt to the deathless'/'DGM', 'Sorcery').
card_original_text('debt to the deathless'/'DGM', 'Each opponent loses two times X life. You gain life equal to the life lost this way.').
card_first_print('debt to the deathless', 'DGM').
card_image_name('debt to the deathless'/'DGM', 'debt to the deathless').
card_uid('debt to the deathless'/'DGM', 'DGM:Debt to the Deathless:debt to the deathless').
card_rarity('debt to the deathless'/'DGM', 'Uncommon').
card_artist('debt to the deathless'/'DGM', 'Seb McKinnon').
card_number('debt to the deathless'/'DGM', '64').
card_flavor_text('debt to the deathless'/'DGM', 'Some of Orzhov\'s cruelest collectors remember their debtors by taste alone.').
card_multiverse_id('debt to the deathless'/'DGM', '369039').
card_watermark('debt to the deathless'/'DGM', 'Orzhov').

card_in_set('deputy of acquittals', 'DGM').
card_original_type('deputy of acquittals'/'DGM', 'Creature — Human Wizard').
card_original_text('deputy of acquittals'/'DGM', 'Flash (You may cast this spell any time you could cast an instant.)\nWhen Deputy of Acquittals enters the battlefield, you may return another target creature you control to its owner\'s hand.').
card_first_print('deputy of acquittals', 'DGM').
card_image_name('deputy of acquittals'/'DGM', 'deputy of acquittals').
card_uid('deputy of acquittals'/'DGM', 'DGM:Deputy of Acquittals:deputy of acquittals').
card_rarity('deputy of acquittals'/'DGM', 'Common').
card_artist('deputy of acquittals'/'DGM', 'James Ryman').
card_number('deputy of acquittals'/'DGM', '65').
card_flavor_text('deputy of acquittals'/'DGM', '\"Everyone deserves a day in court.\"').
card_multiverse_id('deputy of acquittals'/'DGM', '369084').
card_watermark('deputy of acquittals'/'DGM', 'Azorius').

card_in_set('dimir cluestone', 'DGM').
card_original_type('dimir cluestone'/'DGM', 'Artifact').
card_original_text('dimir cluestone'/'DGM', '{T}: Add {U} or {B} to your mana pool.\n{U}{B}, {T}, Sacrifice Dimir Cluestone: Draw a card.').
card_first_print('dimir cluestone', 'DGM').
card_image_name('dimir cluestone'/'DGM', 'dimir cluestone').
card_uid('dimir cluestone'/'DGM', 'DGM:Dimir Cluestone:dimir cluestone').
card_rarity('dimir cluestone'/'DGM', 'Common').
card_artist('dimir cluestone'/'DGM', 'Raoul Vitale').
card_number('dimir cluestone'/'DGM', '138').
card_flavor_text('dimir cluestone'/'DGM', 'It waits in the chill of the undercity, holding nameless secrets for those who dare to touch it.').
card_multiverse_id('dimir cluestone'/'DGM', '369096').
card_watermark('dimir cluestone'/'DGM', 'Dimir').

card_in_set('dimir guildgate', 'DGM').
card_original_type('dimir guildgate'/'DGM', 'Land — Gate').
card_original_text('dimir guildgate'/'DGM', 'Dimir Guildgate enters the battlefield tapped.\n{T}: Add {U} or {B} to your mana pool.').
card_image_name('dimir guildgate'/'DGM', 'dimir guildgate').
card_uid('dimir guildgate'/'DGM', 'DGM:Dimir Guildgate:dimir guildgate').
card_rarity('dimir guildgate'/'DGM', 'Common').
card_artist('dimir guildgate'/'DGM', 'Cliff Childs').
card_number('dimir guildgate'/'DGM', '148').
card_flavor_text('dimir guildgate'/'DGM', 'In the cold corridors of the undercity, misinformation unfurls from Duskmantle like a spreading stain.').
card_multiverse_id('dimir guildgate'/'DGM', '369058').
card_watermark('dimir guildgate'/'DGM', 'Dimir').

card_in_set('dirty', 'DGM').
card_original_type('dirty'/'DGM', 'Sorcery').
card_original_text('dirty'/'DGM', 'Target player discards two cards.\n//\nDirty\n{2}{G}\nSorcery\nReturn target card from your graveyard to your hand.\n//\nFuse (You may cast one or both halves of this card from your hand.)').
card_first_print('dirty', 'DGM').
card_image_name('dirty'/'DGM', 'downdirty').
card_uid('dirty'/'DGM', 'DGM:Dirty:downdirty').
card_rarity('dirty'/'DGM', 'Uncommon').
card_artist('dirty'/'DGM', 'Svetlin Velinov').
card_number('dirty'/'DGM', '126b').
card_multiverse_id('dirty'/'DGM', '369089').

card_in_set('down', 'DGM').
card_original_type('down'/'DGM', 'Sorcery').
card_original_text('down'/'DGM', 'Target player discards two cards.\n//\nDirty\n{2}{G}\nSorcery\nReturn target card from your graveyard to your hand.\n//\nFuse (You may cast one or both halves of this card from your hand.)').
card_first_print('down', 'DGM').
card_image_name('down'/'DGM', 'downdirty').
card_uid('down'/'DGM', 'DGM:Down:downdirty').
card_rarity('down'/'DGM', 'Uncommon').
card_artist('down'/'DGM', 'Svetlin Velinov').
card_number('down'/'DGM', '126a').
card_multiverse_id('down'/'DGM', '369089').

card_in_set('dragonshift', 'DGM').
card_original_type('dragonshift'/'DGM', 'Instant').
card_original_text('dragonshift'/'DGM', 'Until end of turn, target creature you control becomes a 4/4 blue and red Dragon, loses all abilities, and gains flying.\nOverload {3}{U}{U}{R}{R} (You may cast this spell for its overload cost. If you do, change its text by replacing all instances of \"target\" with \"each.\")').
card_first_print('dragonshift', 'DGM').
card_image_name('dragonshift'/'DGM', 'dragonshift').
card_uid('dragonshift'/'DGM', 'DGM:Dragonshift:dragonshift').
card_rarity('dragonshift'/'DGM', 'Rare').
card_artist('dragonshift'/'DGM', 'Svetlin Velinov').
card_number('dragonshift'/'DGM', '66').
card_multiverse_id('dragonshift'/'DGM', '369061').
card_watermark('dragonshift'/'DGM', 'Izzet').

card_in_set('drown in filth', 'DGM').
card_original_type('drown in filth'/'DGM', 'Sorcery').
card_original_text('drown in filth'/'DGM', 'Choose target creature. Put the top four cards of your library into your graveyard, then that creature gets -1/-1 until end of turn for each land card in your graveyard.').
card_first_print('drown in filth', 'DGM').
card_image_name('drown in filth'/'DGM', 'drown in filth').
card_uid('drown in filth'/'DGM', 'DGM:Drown in Filth:drown in filth').
card_rarity('drown in filth'/'DGM', 'Common').
card_artist('drown in filth'/'DGM', 'Seb McKinnon').
card_number('drown in filth'/'DGM', '67').
card_flavor_text('drown in filth'/'DGM', 'The rot farmer\'s first rite of passage is to craft a pair of work stilts.').
card_multiverse_id('drown in filth'/'DGM', '369086').
card_watermark('drown in filth'/'DGM', 'Golgari').

card_in_set('emmara tandris', 'DGM').
card_original_type('emmara tandris'/'DGM', 'Legendary Creature — Elf Shaman').
card_original_text('emmara tandris'/'DGM', 'Prevent all damage that would be dealt to creature tokens you control.').
card_first_print('emmara tandris', 'DGM').
card_image_name('emmara tandris'/'DGM', 'emmara tandris').
card_uid('emmara tandris'/'DGM', 'DGM:Emmara Tandris:emmara tandris').
card_rarity('emmara tandris'/'DGM', 'Rare').
card_artist('emmara tandris'/'DGM', 'Mark Winters').
card_number('emmara tandris'/'DGM', '68').
card_flavor_text('emmara tandris'/'DGM', 'When Selesnya needed a champion, Emmara put the ranks of her guild before herself—making her the perfect choice.').
card_multiverse_id('emmara tandris'/'DGM', '368944').
card_watermark('emmara tandris'/'DGM', 'Selesnya').

card_in_set('entering', 'DGM').
card_original_type('entering'/'DGM', 'Sorcery').
card_original_text('entering'/'DGM', 'Target player puts the top eight cards of his or her library into his or her graveyard.\n//\nEntering\n{4}{B}{R}\nSorcery\nPut a creature card from a graveyard onto the battlefield under your control. It gains haste until end of turn.\n//\nFuse (You may cast one or both halves of this card from your hand.)').
card_image_name('entering'/'DGM', 'breakingentering').
card_uid('entering'/'DGM', 'DGM:Entering:breakingentering').
card_rarity('entering'/'DGM', 'Rare').
card_artist('entering'/'DGM', 'Mathias Kollros').
card_number('entering'/'DGM', '124b').
card_multiverse_id('entering'/'DGM', '369009').

card_in_set('exava, rakdos blood witch', 'DGM').
card_original_type('exava, rakdos blood witch'/'DGM', 'Legendary Creature — Human Cleric').
card_original_text('exava, rakdos blood witch'/'DGM', 'First strike, haste\nUnleash (You may have this creature enter the battlefield with a +1/+1 counter on it. It can\'t block as long as it has a +1/+1 counter on it.)\nEach other creature you control with a +1/+1 counter on it has haste.').
card_first_print('exava, rakdos blood witch', 'DGM').
card_image_name('exava, rakdos blood witch'/'DGM', 'exava, rakdos blood witch').
card_uid('exava, rakdos blood witch'/'DGM', 'DGM:Exava, Rakdos Blood Witch:exava, rakdos blood witch').
card_rarity('exava, rakdos blood witch'/'DGM', 'Rare').
card_artist('exava, rakdos blood witch'/'DGM', 'Aleksi Briclot').
card_number('exava, rakdos blood witch'/'DGM', '69').
card_multiverse_id('exava, rakdos blood witch'/'DGM', '369055').
card_watermark('exava, rakdos blood witch'/'DGM', 'Rakdos').

card_in_set('far', 'DGM').
card_original_type('far'/'DGM', 'Instant').
card_original_text('far'/'DGM', 'Return target creature to its owner\'s hand.\n//\nAway\n{2}{B}\nInstant\nTarget player sacrifices a creature.\n//\nFuse (You may cast one or both halves of this card from your hand.)').
card_first_print('far', 'DGM').
card_image_name('far'/'DGM', 'faraway').
card_uid('far'/'DGM', 'DGM:Far:faraway').
card_rarity('far'/'DGM', 'Uncommon').
card_artist('far'/'DGM', 'Greg Staples').
card_number('far'/'DGM', '127a').
card_multiverse_id('far'/'DGM', '369042').

card_in_set('fatal fumes', 'DGM').
card_original_type('fatal fumes'/'DGM', 'Instant').
card_original_text('fatal fumes'/'DGM', 'Target creature gets -4/-2 until end of turn.').
card_first_print('fatal fumes', 'DGM').
card_image_name('fatal fumes'/'DGM', 'fatal fumes').
card_uid('fatal fumes'/'DGM', 'DGM:Fatal Fumes:fatal fumes').
card_rarity('fatal fumes'/'DGM', 'Common').
card_artist('fatal fumes'/'DGM', 'Kev Walker').
card_number('fatal fumes'/'DGM', '24').
card_flavor_text('fatal fumes'/'DGM', 'Travelers in the undercity dread the horrors and assassins of the Dimir, but nearly as many die from more mundane hazards.').
card_multiverse_id('fatal fumes'/'DGM', '368962').

card_in_set('feral animist', 'DGM').
card_original_type('feral animist'/'DGM', 'Creature — Goblin Shaman').
card_original_text('feral animist'/'DGM', '{3}: Feral Animist gets +X/+0 until end of turn, where X is its power.').
card_image_name('feral animist'/'DGM', 'feral animist').
card_uid('feral animist'/'DGM', 'DGM:Feral Animist:feral animist').
card_rarity('feral animist'/'DGM', 'Uncommon').
card_artist('feral animist'/'DGM', 'Dave Kendall').
card_number('feral animist'/'DGM', '70').
card_flavor_text('feral animist'/'DGM', '\"Channeling the rage of the beast is the easy part. Explaining why you woke up with no memory in a pile of mangled lawmages—that\'s trickier.\"').
card_multiverse_id('feral animist'/'DGM', '368969').
card_watermark('feral animist'/'DGM', 'Gruul').

card_in_set('flesh', 'DGM').
card_original_type('flesh'/'DGM', 'Sorcery').
card_original_text('flesh'/'DGM', 'Exile target creature card from a graveyard. Put X +1/+1 counters on target creature, where X is the power of the card you exiled.\n//\nBlood\n{R}{G}\nSorcery\nTarget creature you control deals damage equal to its power to target creature or player.\n//\nFuse (You may cast one or both halves of this card from your hand.)').
card_first_print('flesh', 'DGM').
card_image_name('flesh'/'DGM', 'fleshblood').
card_uid('flesh'/'DGM', 'DGM:Flesh:fleshblood').
card_rarity('flesh'/'DGM', 'Rare').
card_artist('flesh'/'DGM', 'Lucas Graciano').
card_number('flesh'/'DGM', '128a').
card_multiverse_id('flesh'/'DGM', '368991').

card_in_set('fluxcharger', 'DGM').
card_original_type('fluxcharger'/'DGM', 'Creature — Weird').
card_original_text('fluxcharger'/'DGM', 'Flying\nWhenever you cast an instant or sorcery spell, you may switch Fluxcharger\'s power and toughness until end of turn.').
card_first_print('fluxcharger', 'DGM').
card_image_name('fluxcharger'/'DGM', 'fluxcharger').
card_uid('fluxcharger'/'DGM', 'DGM:Fluxcharger:fluxcharger').
card_rarity('fluxcharger'/'DGM', 'Uncommon').
card_artist('fluxcharger'/'DGM', 'Willian Murai').
card_number('fluxcharger'/'DGM', '71').
card_flavor_text('fluxcharger'/'DGM', 'One part mizzium, two parts elemental energy, eleven parts danger.').
card_multiverse_id('fluxcharger'/'DGM', '368955').
card_watermark('fluxcharger'/'DGM', 'Izzet').

card_in_set('gaze of granite', 'DGM').
card_original_type('gaze of granite'/'DGM', 'Sorcery').
card_original_text('gaze of granite'/'DGM', 'Destroy each nonland permanent with converted mana cost X or less.').
card_image_name('gaze of granite'/'DGM', 'gaze of granite').
card_uid('gaze of granite'/'DGM', 'DGM:Gaze of Granite:gaze of granite').
card_rarity('gaze of granite'/'DGM', 'Rare').
card_artist('gaze of granite'/'DGM', 'Nils Hamm').
card_number('gaze of granite'/'DGM', '72').
card_flavor_text('gaze of granite'/'DGM', 'Golgari reclaimers resent the gorgons, begrudging the loss of so many fine nutrients locked away in stone.').
card_multiverse_id('gaze of granite'/'DGM', '369095').
card_watermark('gaze of granite'/'DGM', 'Golgari').

card_in_set('give', 'DGM').
card_original_type('give'/'DGM', 'Sorcery').
card_original_text('give'/'DGM', 'Put three +1/+1 counters on target creature.\n//\nTake\n{2}{U}\nSorcery\nRemove all +1/+1 counters from target creature you control. Draw that many cards.\n//\nFuse (You may cast one or both halves of this card from your hand.)').
card_first_print('give', 'DGM').
card_image_name('give'/'DGM', 'givetake').
card_uid('give'/'DGM', 'DGM:Give:givetake').
card_rarity('give'/'DGM', 'Uncommon').
card_artist('give'/'DGM', 'Steve Prescott').
card_number('give'/'DGM', '129a').
card_multiverse_id('give'/'DGM', '369097').

card_in_set('gleam of battle', 'DGM').
card_original_type('gleam of battle'/'DGM', 'Enchantment').
card_original_text('gleam of battle'/'DGM', 'Whenever a creature you control attacks, put a +1/+1 counter on it.').
card_first_print('gleam of battle', 'DGM').
card_image_name('gleam of battle'/'DGM', 'gleam of battle').
card_uid('gleam of battle'/'DGM', 'DGM:Gleam of Battle:gleam of battle').
card_rarity('gleam of battle'/'DGM', 'Uncommon').
card_artist('gleam of battle'/'DGM', 'Raymond Swanland').
card_number('gleam of battle'/'DGM', '73').
card_flavor_text('gleam of battle'/'DGM', 'Every soldier returns from battle changed: by hardship, by blood, by a glimpse of glory.').
card_multiverse_id('gleam of battle'/'DGM', '368987').
card_watermark('gleam of battle'/'DGM', 'Boros').

card_in_set('goblin test pilot', 'DGM').
card_original_type('goblin test pilot'/'DGM', 'Creature — Goblin Wizard').
card_original_text('goblin test pilot'/'DGM', 'Flying\n{T}: Goblin Test Pilot deals 2 damage to target creature or player chosen at random.').
card_first_print('goblin test pilot', 'DGM').
card_image_name('goblin test pilot'/'DGM', 'goblin test pilot').
card_uid('goblin test pilot'/'DGM', 'DGM:Goblin Test Pilot:goblin test pilot').
card_rarity('goblin test pilot'/'DGM', 'Uncommon').
card_artist('goblin test pilot'/'DGM', 'Svetlin Velinov').
card_number('goblin test pilot'/'DGM', '74').
card_flavor_text('goblin test pilot'/'DGM', '\"All pilots and prototypes destroyed. Extensive collateral damage inflicted. Conclusion: flawless design.\"\n—Manual of Melek').
card_multiverse_id('goblin test pilot'/'DGM', '368964').
card_watermark('goblin test pilot'/'DGM', 'Izzet').

card_in_set('golgari cluestone', 'DGM').
card_original_type('golgari cluestone'/'DGM', 'Artifact').
card_original_text('golgari cluestone'/'DGM', '{T}: Add {B} or {G} to your mana pool.\n{B}{G}, {T}, Sacrifice Golgari Cluestone: Draw a card.').
card_first_print('golgari cluestone', 'DGM').
card_image_name('golgari cluestone'/'DGM', 'golgari cluestone').
card_uid('golgari cluestone'/'DGM', 'DGM:Golgari Cluestone:golgari cluestone').
card_rarity('golgari cluestone'/'DGM', 'Common').
card_artist('golgari cluestone'/'DGM', 'Raoul Vitale').
card_number('golgari cluestone'/'DGM', '139').
card_flavor_text('golgari cluestone'/'DGM', 'Golgari mold collects whispers, intelligence that is later harvested by Jarad and his high chancellors.').
card_multiverse_id('golgari cluestone'/'DGM', '369023').
card_watermark('golgari cluestone'/'DGM', 'Golgari').

card_in_set('golgari guildgate', 'DGM').
card_original_type('golgari guildgate'/'DGM', 'Land — Gate').
card_original_text('golgari guildgate'/'DGM', 'Golgari Guildgate enters the battlefield tapped.\n{T}: Add {B} or {G} to your mana pool.').
card_image_name('golgari guildgate'/'DGM', 'golgari guildgate').
card_uid('golgari guildgate'/'DGM', 'DGM:Golgari Guildgate:golgari guildgate').
card_rarity('golgari guildgate'/'DGM', 'Common').
card_artist('golgari guildgate'/'DGM', 'Eytan Zana').
card_number('golgari guildgate'/'DGM', '149').
card_flavor_text('golgari guildgate'/'DGM', 'Every part of the gate was reclaimed from forgotten ruins and imbued with new purpose by the Swarm.').
card_multiverse_id('golgari guildgate'/'DGM', '368960').
card_watermark('golgari guildgate'/'DGM', 'Golgari').

card_in_set('gruul cluestone', 'DGM').
card_original_type('gruul cluestone'/'DGM', 'Artifact').
card_original_text('gruul cluestone'/'DGM', '{T}: Add {R} or {G} to your mana pool.\n{R}{G}, {T}, Sacrifice Gruul Cluestone: Draw a card.').
card_first_print('gruul cluestone', 'DGM').
card_image_name('gruul cluestone'/'DGM', 'gruul cluestone').
card_uid('gruul cluestone'/'DGM', 'DGM:Gruul Cluestone:gruul cluestone').
card_rarity('gruul cluestone'/'DGM', 'Common').
card_artist('gruul cluestone'/'DGM', 'Raoul Vitale').
card_number('gruul cluestone'/'DGM', '140').
card_flavor_text('gruul cluestone'/'DGM', 'Forgotten under rocks and vines, Gruul symbols mark the sites of primitive rituals where shamans once chanted to the wild.').
card_multiverse_id('gruul cluestone'/'DGM', '369035').
card_watermark('gruul cluestone'/'DGM', 'Gruul').

card_in_set('gruul guildgate', 'DGM').
card_original_type('gruul guildgate'/'DGM', 'Land — Gate').
card_original_text('gruul guildgate'/'DGM', 'Gruul Guildgate enters the battlefield tapped.\n{T}: Add {R} or {G} to your mana pool.').
card_image_name('gruul guildgate'/'DGM', 'gruul guildgate').
card_uid('gruul guildgate'/'DGM', 'DGM:Gruul Guildgate:gruul guildgate').
card_rarity('gruul guildgate'/'DGM', 'Common').
card_artist('gruul guildgate'/'DGM', 'Randy Gallegos').
card_number('gruul guildgate'/'DGM', '150').
card_flavor_text('gruul guildgate'/'DGM', 'The scarred rubble around the gate is testament to past meetings of the Clans, summits that inevitably devolved into brawls.').
card_multiverse_id('gruul guildgate'/'DGM', '369040').
card_watermark('gruul guildgate'/'DGM', 'Gruul').

card_in_set('gruul war chant', 'DGM').
card_original_type('gruul war chant'/'DGM', 'Enchantment').
card_original_text('gruul war chant'/'DGM', 'Each attacking creature you control gets +1/+0 and can\'t be blocked except by two or more creatures.').
card_first_print('gruul war chant', 'DGM').
card_image_name('gruul war chant'/'DGM', 'gruul war chant').
card_uid('gruul war chant'/'DGM', 'DGM:Gruul War Chant:gruul war chant').
card_rarity('gruul war chant'/'DGM', 'Uncommon').
card_artist('gruul war chant'/'DGM', 'Dave Kendall').
card_number('gruul war chant'/'DGM', '75').
card_flavor_text('gruul war chant'/'DGM', '\"We are the heart of the wild, the fire in its eyes, and the howl in its throat. Come, join the battle to which you were born.\"\n—Kroshkar, Gruul shaman').
card_multiverse_id('gruul war chant'/'DGM', '368956').
card_watermark('gruul war chant'/'DGM', 'Gruul').

card_in_set('haazda snare squad', 'DGM').
card_original_type('haazda snare squad'/'DGM', 'Creature — Human Soldier').
card_original_text('haazda snare squad'/'DGM', 'Whenever Haazda Snare Squad attacks, you may pay {W}. If you do, tap target creature an opponent controls.').
card_first_print('haazda snare squad', 'DGM').
card_image_name('haazda snare squad'/'DGM', 'haazda snare squad').
card_uid('haazda snare squad'/'DGM', 'DGM:Haazda Snare Squad:haazda snare squad').
card_rarity('haazda snare squad'/'DGM', 'Common').
card_artist('haazda snare squad'/'DGM', 'David Palumbo').
card_number('haazda snare squad'/'DGM', '2').
card_flavor_text('haazda snare squad'/'DGM', 'They\'ve been probation officers, army grunts, bounty hunters—anything to make their district safer.').
card_multiverse_id('haazda snare squad'/'DGM', '369077').

card_in_set('haunter of nightveil', 'DGM').
card_original_type('haunter of nightveil'/'DGM', 'Creature — Spirit').
card_original_text('haunter of nightveil'/'DGM', 'Creatures your opponents control get -1/-0.').
card_first_print('haunter of nightveil', 'DGM').
card_image_name('haunter of nightveil'/'DGM', 'haunter of nightveil').
card_uid('haunter of nightveil'/'DGM', 'DGM:Haunter of Nightveil:haunter of nightveil').
card_rarity('haunter of nightveil'/'DGM', 'Uncommon').
card_artist('haunter of nightveil'/'DGM', 'Igor Kieryluk').
card_number('haunter of nightveil'/'DGM', '76').
card_flavor_text('haunter of nightveil'/'DGM', 'The Dimir employ the spirits of Ravnica\'s ancient, nameless dead, guiding their malevolence toward selected victims.').
card_multiverse_id('haunter of nightveil'/'DGM', '369087').
card_watermark('haunter of nightveil'/'DGM', 'Dimir').

card_in_set('hidden strings', 'DGM').
card_original_type('hidden strings'/'DGM', 'Sorcery').
card_original_text('hidden strings'/'DGM', 'You may tap or untap target permanent, then you may tap or untap another target permanent.\nCipher (Then you may exile this spell card encoded on a creature you control. Whenever that creature deals combat damage to a player, its controller may cast a copy of the encoded card without paying its mana cost.)').
card_first_print('hidden strings', 'DGM').
card_image_name('hidden strings'/'DGM', 'hidden strings').
card_uid('hidden strings'/'DGM', 'DGM:Hidden Strings:hidden strings').
card_rarity('hidden strings'/'DGM', 'Common').
card_artist('hidden strings'/'DGM', 'Daarken').
card_number('hidden strings'/'DGM', '12').
card_multiverse_id('hidden strings'/'DGM', '369021').
card_watermark('hidden strings'/'DGM', 'Dimir').

card_in_set('hired torturer', 'DGM').
card_original_type('hired torturer'/'DGM', 'Creature — Human Rogue').
card_original_text('hired torturer'/'DGM', 'Defender\n{3}{B}, {T}: Target opponent loses 2 life, then reveals a card at random from his or her hand.').
card_first_print('hired torturer', 'DGM').
card_image_name('hired torturer'/'DGM', 'hired torturer').
card_uid('hired torturer'/'DGM', 'DGM:Hired Torturer:hired torturer').
card_rarity('hired torturer'/'DGM', 'Common').
card_artist('hired torturer'/'DGM', 'Winona Nelson').
card_number('hired torturer'/'DGM', '25').
card_flavor_text('hired torturer'/'DGM', '\"Follow your passion, I always say. Might as well get paid to do what you love.\"').
card_multiverse_id('hired torturer'/'DGM', '369048').

card_in_set('izzet cluestone', 'DGM').
card_original_type('izzet cluestone'/'DGM', 'Artifact').
card_original_text('izzet cluestone'/'DGM', '{T}: Add {U} or {R} to your mana pool.\n{U}{R}, {T}, Sacrifice Izzet Cluestone: Draw a card.').
card_first_print('izzet cluestone', 'DGM').
card_image_name('izzet cluestone'/'DGM', 'izzet cluestone').
card_uid('izzet cluestone'/'DGM', 'DGM:Izzet Cluestone:izzet cluestone').
card_rarity('izzet cluestone'/'DGM', 'Common').
card_artist('izzet cluestone'/'DGM', 'Raoul Vitale').
card_number('izzet cluestone'/'DGM', '141').
card_flavor_text('izzet cluestone'/'DGM', 'It holds within it an unsolvable riddle. A creative answer yields an invitation to the guild.').
card_multiverse_id('izzet cluestone'/'DGM', '368948').
card_watermark('izzet cluestone'/'DGM', 'Izzet').

card_in_set('izzet guildgate', 'DGM').
card_original_type('izzet guildgate'/'DGM', 'Land — Gate').
card_original_text('izzet guildgate'/'DGM', 'Izzet Guildgate enters the battlefield tapped.\n{T}: Add {U} or {R} to your mana pool.').
card_image_name('izzet guildgate'/'DGM', 'izzet guildgate').
card_uid('izzet guildgate'/'DGM', 'DGM:Izzet Guildgate:izzet guildgate').
card_rarity('izzet guildgate'/'DGM', 'Common').
card_artist('izzet guildgate'/'DGM', 'Noah Bradley').
card_number('izzet guildgate'/'DGM', '151').
card_flavor_text('izzet guildgate'/'DGM', 'Beyond lies the culmination of all Izzet projects, a grand experiment led by the dragon Niv-Mizzet himself.').
card_multiverse_id('izzet guildgate'/'DGM', '368995').
card_watermark('izzet guildgate'/'DGM', 'Izzet').

card_in_set('jelenn sphinx', 'DGM').
card_original_type('jelenn sphinx'/'DGM', 'Creature — Sphinx').
card_original_text('jelenn sphinx'/'DGM', 'Flying, vigilance\nWhenever Jelenn Sphinx attacks, other attacking creatures get +1/+1 until end of turn.').
card_first_print('jelenn sphinx', 'DGM').
card_image_name('jelenn sphinx'/'DGM', 'jelenn sphinx').
card_uid('jelenn sphinx'/'DGM', 'DGM:Jelenn Sphinx:jelenn sphinx').
card_rarity('jelenn sphinx'/'DGM', 'Uncommon').
card_artist('jelenn sphinx'/'DGM', 'Wesley Burt').
card_number('jelenn sphinx'/'DGM', '77').
card_flavor_text('jelenn sphinx'/'DGM', 'Few among the Azorius have ever spoken with the sphinxes, but all have felt their influence.').
card_multiverse_id('jelenn sphinx'/'DGM', '368947').
card_watermark('jelenn sphinx'/'DGM', 'Azorius').

card_in_set('korozda gorgon', 'DGM').
card_original_type('korozda gorgon'/'DGM', 'Creature — Gorgon').
card_original_text('korozda gorgon'/'DGM', 'Deathtouch\n{2}, Remove a +1/+1 counter from a creature you control: Target creature gets -1/-1 until end of turn.').
card_first_print('korozda gorgon', 'DGM').
card_image_name('korozda gorgon'/'DGM', 'korozda gorgon').
card_uid('korozda gorgon'/'DGM', 'DGM:Korozda Gorgon:korozda gorgon').
card_rarity('korozda gorgon'/'DGM', 'Uncommon').
card_artist('korozda gorgon'/'DGM', 'Volkan Baga').
card_number('korozda gorgon'/'DGM', '78').
card_flavor_text('korozda gorgon'/'DGM', '\"Welcome to my collection.\"').
card_multiverse_id('korozda gorgon'/'DGM', '369064').
card_watermark('korozda gorgon'/'DGM', 'Golgari').

card_in_set('krasis incubation', 'DGM').
card_original_type('krasis incubation'/'DGM', 'Enchantment — Aura').
card_original_text('krasis incubation'/'DGM', 'Enchant creature\nEnchanted creature can\'t attack or block, and its activated abilities can\'t be activated.\n{1}{G}{U}, Return Krasis Incubation to its owner\'s hand: Put two +1/+1 counters on enchanted creature.').
card_first_print('krasis incubation', 'DGM').
card_image_name('krasis incubation'/'DGM', 'krasis incubation').
card_uid('krasis incubation'/'DGM', 'DGM:Krasis Incubation:krasis incubation').
card_rarity('krasis incubation'/'DGM', 'Uncommon').
card_artist('krasis incubation'/'DGM', 'Marco Nelor').
card_number('krasis incubation'/'DGM', '79').
card_multiverse_id('krasis incubation'/'DGM', '369052').
card_watermark('krasis incubation'/'DGM', 'Simic').

card_in_set('kraul warrior', 'DGM').
card_original_type('kraul warrior'/'DGM', 'Creature — Insect Warrior').
card_original_text('kraul warrior'/'DGM', '{5}{G}: Kraul Warrior gets +3/+3 until end of turn.').
card_first_print('kraul warrior', 'DGM').
card_image_name('kraul warrior'/'DGM', 'kraul warrior').
card_uid('kraul warrior'/'DGM', 'DGM:Kraul Warrior:kraul warrior').
card_rarity('kraul warrior'/'DGM', 'Common').
card_artist('kraul warrior'/'DGM', 'David Rapoza').
card_number('kraul warrior'/'DGM', '42').
card_flavor_text('kraul warrior'/'DGM', 'The insectile kraul lurk in the tunnels below street level. Many are loyal to the Golgari Swarm, but others follow their own esoteric caste system.').
card_multiverse_id('kraul warrior'/'DGM', '369059').

card_in_set('lavinia of the tenth', 'DGM').
card_original_type('lavinia of the tenth'/'DGM', 'Legendary Creature — Human Soldier').
card_original_text('lavinia of the tenth'/'DGM', 'Protection from red\nWhen Lavinia of the Tenth enters the battlefield, detain each nonland permanent your opponents control with converted mana cost 4 or less. (Until your next turn, those permanents can\'t attack or block and their activated abilities can\'t be activated.)').
card_first_print('lavinia of the tenth', 'DGM').
card_image_name('lavinia of the tenth'/'DGM', 'lavinia of the tenth').
card_uid('lavinia of the tenth'/'DGM', 'DGM:Lavinia of the Tenth:lavinia of the tenth').
card_rarity('lavinia of the tenth'/'DGM', 'Rare').
card_artist('lavinia of the tenth'/'DGM', 'Willian Murai').
card_number('lavinia of the tenth'/'DGM', '80').
card_multiverse_id('lavinia of the tenth'/'DGM', '368983').
card_watermark('lavinia of the tenth'/'DGM', 'Azorius').

card_in_set('legion\'s initiative', 'DGM').
card_original_type('legion\'s initiative'/'DGM', 'Enchantment').
card_original_text('legion\'s initiative'/'DGM', 'Red creatures you control get +1/+0.\nWhite creatures you control get +0/+1.\n{R}{W}, Exile Legion\'s Initiative: Exile all creatures you control. At the beginning of the next combat, return those cards to the battlefield under their owner\'s control and those creatures gain haste until end of turn.').
card_first_print('legion\'s initiative', 'DGM').
card_image_name('legion\'s initiative'/'DGM', 'legion\'s initiative').
card_uid('legion\'s initiative'/'DGM', 'DGM:Legion\'s Initiative:legion\'s initiative').
card_rarity('legion\'s initiative'/'DGM', 'Mythic Rare').
card_artist('legion\'s initiative'/'DGM', 'Jaime Jones').
card_number('legion\'s initiative'/'DGM', '81').
card_multiverse_id('legion\'s initiative'/'DGM', '369066').
card_watermark('legion\'s initiative'/'DGM', 'Boros').

card_in_set('loss', 'DGM').
card_original_type('loss'/'DGM', 'Instant').
card_original_text('loss'/'DGM', 'Creatures you control get +1/+1 until end of turn.\n//\nLoss\n{2}{B}\nInstant\nCreatures your opponents control get -1/-1 until end of turn.\n//\nFuse (You may cast one or both halves of this card from your hand.)').
card_first_print('loss', 'DGM').
card_image_name('loss'/'DGM', 'profitloss').
card_uid('loss'/'DGM', 'DGM:Loss:profitloss').
card_rarity('loss'/'DGM', 'Uncommon').
card_artist('loss'/'DGM', 'Kev Walker').
card_number('loss'/'DGM', '130b').
card_multiverse_id('loss'/'DGM', '369071').

card_in_set('lyev decree', 'DGM').
card_original_type('lyev decree'/'DGM', 'Sorcery').
card_original_text('lyev decree'/'DGM', 'Detain up to two target creatures your opponents control. (Until your next turn, those creatures can\'t attack or block and their activated abilities can\'t be activated.)').
card_first_print('lyev decree', 'DGM').
card_image_name('lyev decree'/'DGM', 'lyev decree').
card_uid('lyev decree'/'DGM', 'DGM:Lyev Decree:lyev decree').
card_rarity('lyev decree'/'DGM', 'Common').
card_artist('lyev decree'/'DGM', 'Kev Walker').
card_number('lyev decree'/'DGM', '3').
card_flavor_text('lyev decree'/'DGM', 'The Azorius have so many codes and statutes that you\'re always in violation of one of them.').
card_multiverse_id('lyev decree'/'DGM', '368965').
card_watermark('lyev decree'/'DGM', 'Azorius').

card_in_set('master of cruelties', 'DGM').
card_original_type('master of cruelties'/'DGM', 'Creature — Demon').
card_original_text('master of cruelties'/'DGM', 'First strike, deathtouch\nMaster of Cruelties can only attack alone.\nWhenever Master of Cruelties attacks a player and isn\'t blocked, that player\'s life total becomes 1. Master of Cruelties assigns no combat damage this combat.').
card_first_print('master of cruelties', 'DGM').
card_image_name('master of cruelties'/'DGM', 'master of cruelties').
card_uid('master of cruelties'/'DGM', 'DGM:Master of Cruelties:master of cruelties').
card_rarity('master of cruelties'/'DGM', 'Mythic Rare').
card_artist('master of cruelties'/'DGM', 'Chase Stone').
card_number('master of cruelties'/'DGM', '82').
card_multiverse_id('master of cruelties'/'DGM', '368981').
card_watermark('master of cruelties'/'DGM', 'Rakdos').

card_in_set('maw of the obzedat', 'DGM').
card_original_type('maw of the obzedat'/'DGM', 'Creature — Thrull').
card_original_text('maw of the obzedat'/'DGM', 'Sacrifice a creature: Creatures you control get +1/+1 until end of turn.').
card_first_print('maw of the obzedat', 'DGM').
card_image_name('maw of the obzedat'/'DGM', 'maw of the obzedat').
card_uid('maw of the obzedat'/'DGM', 'DGM:Maw of the Obzedat:maw of the obzedat').
card_rarity('maw of the obzedat'/'DGM', 'Uncommon').
card_artist('maw of the obzedat'/'DGM', 'Randy Gallegos').
card_number('maw of the obzedat'/'DGM', '83').
card_flavor_text('maw of the obzedat'/'DGM', 'The devout sacrifice themselves to it hoping for escape from their debts to the guild, but many end up owing in the afterlife.').
card_multiverse_id('maw of the obzedat'/'DGM', '368994').
card_watermark('maw of the obzedat'/'DGM', 'Orzhov').

card_in_set('maze abomination', 'DGM').
card_original_type('maze abomination'/'DGM', 'Creature — Elemental').
card_original_text('maze abomination'/'DGM', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)\nMulticolored creatures you control have deathtouch.').
card_first_print('maze abomination', 'DGM').
card_image_name('maze abomination'/'DGM', 'maze abomination').
card_uid('maze abomination'/'DGM', 'DGM:Maze Abomination:maze abomination').
card_rarity('maze abomination'/'DGM', 'Common').
card_artist('maze abomination'/'DGM', 'Yeong-Hao Han').
card_number('maze abomination'/'DGM', '26').
card_multiverse_id('maze abomination'/'DGM', '369050').

card_in_set('maze behemoth', 'DGM').
card_original_type('maze behemoth'/'DGM', 'Creature — Elemental').
card_original_text('maze behemoth'/'DGM', 'Trample\nMulticolored creatures you control have trample.').
card_first_print('maze behemoth', 'DGM').
card_image_name('maze behemoth'/'DGM', 'maze behemoth').
card_uid('maze behemoth'/'DGM', 'DGM:Maze Behemoth:maze behemoth').
card_rarity('maze behemoth'/'DGM', 'Common').
card_artist('maze behemoth'/'DGM', 'Yeong-Hao Han').
card_number('maze behemoth'/'DGM', '43').
card_flavor_text('maze behemoth'/'DGM', 'It returned to the leylines as quickly as it had appeared, leaving only broken bodies to mark its passage.').
card_multiverse_id('maze behemoth'/'DGM', '369091').

card_in_set('maze glider', 'DGM').
card_original_type('maze glider'/'DGM', 'Creature — Elemental').
card_original_text('maze glider'/'DGM', 'Flying\nMulticolored creatures you control have flying.').
card_first_print('maze glider', 'DGM').
card_image_name('maze glider'/'DGM', 'maze glider').
card_uid('maze glider'/'DGM', 'DGM:Maze Glider:maze glider').
card_rarity('maze glider'/'DGM', 'Common').
card_artist('maze glider'/'DGM', 'Yeong-Hao Han').
card_number('maze glider'/'DGM', '13').
card_flavor_text('maze glider'/'DGM', 'The pattern of the Implicit Maze extends in more than two dimensions.').
card_multiverse_id('maze glider'/'DGM', '369014').

card_in_set('maze rusher', 'DGM').
card_original_type('maze rusher'/'DGM', 'Creature — Elemental').
card_original_text('maze rusher'/'DGM', 'Haste\nMulticolored creatures you control have haste.').
card_first_print('maze rusher', 'DGM').
card_image_name('maze rusher'/'DGM', 'maze rusher').
card_uid('maze rusher'/'DGM', 'DGM:Maze Rusher:maze rusher').
card_rarity('maze rusher'/'DGM', 'Common').
card_artist('maze rusher'/'DGM', 'Yeong-Hao Han').
card_number('maze rusher'/'DGM', '33').
card_flavor_text('maze rusher'/'DGM', '\"The maze seems to fight us at every turn, resisting our efforts to solve it.\"\n—Niv-Mizzet').
card_multiverse_id('maze rusher'/'DGM', '369076').

card_in_set('maze sentinel', 'DGM').
card_original_type('maze sentinel'/'DGM', 'Creature — Elemental').
card_original_text('maze sentinel'/'DGM', 'Vigilance\nMulticolored creatures you control have vigilance.').
card_first_print('maze sentinel', 'DGM').
card_image_name('maze sentinel'/'DGM', 'maze sentinel').
card_uid('maze sentinel'/'DGM', 'DGM:Maze Sentinel:maze sentinel').
card_rarity('maze sentinel'/'DGM', 'Common').
card_artist('maze sentinel'/'DGM', 'Yeong-Hao Han').
card_number('maze sentinel'/'DGM', '4').
card_flavor_text('maze sentinel'/'DGM', 'The route has been known for millennia, but only by those with no means to tell it.').
card_multiverse_id('maze sentinel'/'DGM', '368976').

card_in_set('maze\'s end', 'DGM').
card_original_type('maze\'s end'/'DGM', 'Land').
card_original_text('maze\'s end'/'DGM', 'Maze\'s End enters the battlefield tapped.\n{T}: Add {1} to your mana pool.\n{3}, {T}, Return Maze\'s End to its owner\'s hand: Search your library for a Gate card, put it onto the battlefield, then shuffle your library. If you control ten or more Gates with different names, you win the game.').
card_image_name('maze\'s end'/'DGM', 'maze\'s end').
card_uid('maze\'s end'/'DGM', 'DGM:Maze\'s End:maze\'s end').
card_rarity('maze\'s end'/'DGM', 'Mythic Rare').
card_artist('maze\'s end'/'DGM', 'Cliff Childs').
card_number('maze\'s end'/'DGM', '152').
card_multiverse_id('maze\'s end'/'DGM', '369038').

card_in_set('melek, izzet paragon', 'DGM').
card_original_type('melek, izzet paragon'/'DGM', 'Legendary Creature — Weird Wizard').
card_original_text('melek, izzet paragon'/'DGM', 'Play with the top card of your library revealed.\nYou may cast the top card of your library if it\'s an instant or sorcery card.\nWhenever you cast an instant or sorcery spell from your library, copy it. You may choose new targets for the copy.').
card_image_name('melek, izzet paragon'/'DGM', 'melek, izzet paragon').
card_uid('melek, izzet paragon'/'DGM', 'DGM:Melek, Izzet Paragon:melek, izzet paragon').
card_rarity('melek, izzet paragon'/'DGM', 'Rare').
card_artist('melek, izzet paragon'/'DGM', 'Jason Chan').
card_number('melek, izzet paragon'/'DGM', '84').
card_multiverse_id('melek, izzet paragon'/'DGM', '369062').
card_watermark('melek, izzet paragon'/'DGM', 'Izzet').

card_in_set('mending touch', 'DGM').
card_original_type('mending touch'/'DGM', 'Instant').
card_original_text('mending touch'/'DGM', 'Regenerate target creature.').
card_first_print('mending touch', 'DGM').
card_image_name('mending touch'/'DGM', 'mending touch').
card_uid('mending touch'/'DGM', 'DGM:Mending Touch:mending touch').
card_rarity('mending touch'/'DGM', 'Common').
card_artist('mending touch'/'DGM', 'Karla Ortiz').
card_number('mending touch'/'DGM', '44').
card_flavor_text('mending touch'/'DGM', '\"The gift of life must be freely given. Anything less would diminish us all.\"\n—Arin, Selesnya healer').
card_multiverse_id('mending touch'/'DGM', '368957').

card_in_set('mindstatic', 'DGM').
card_original_type('mindstatic'/'DGM', 'Instant').
card_original_text('mindstatic'/'DGM', 'Counter target spell unless its controller pays {6}.').
card_first_print('mindstatic', 'DGM').
card_image_name('mindstatic'/'DGM', 'mindstatic').
card_uid('mindstatic'/'DGM', 'DGM:Mindstatic:mindstatic').
card_rarity('mindstatic'/'DGM', 'Common').
card_artist('mindstatic'/'DGM', 'Johann Bodin').
card_number('mindstatic'/'DGM', '14').
card_flavor_text('mindstatic'/'DGM', '\"It\'s always satisfying to stymie the best efforts of a rival, but the reactions of the Gruul are particularly amusing.\"\n—Bori Andon, Izzet blastseeker').
card_multiverse_id('mindstatic'/'DGM', '368980').

card_in_set('mirko vosk, mind drinker', 'DGM').
card_original_type('mirko vosk, mind drinker'/'DGM', 'Legendary Creature — Vampire').
card_original_text('mirko vosk, mind drinker'/'DGM', 'Flying\nWhenever Mirko Vosk, Mind Drinker deals combat damage to a player, that player reveals cards from the top of his or her library until he or she reveals four land cards, then puts those cards into his or her graveyard.').
card_first_print('mirko vosk, mind drinker', 'DGM').
card_image_name('mirko vosk, mind drinker'/'DGM', 'mirko vosk, mind drinker').
card_uid('mirko vosk, mind drinker'/'DGM', 'DGM:Mirko Vosk, Mind Drinker:mirko vosk, mind drinker').
card_rarity('mirko vosk, mind drinker'/'DGM', 'Rare').
card_artist('mirko vosk, mind drinker'/'DGM', 'Chase Stone').
card_number('mirko vosk, mind drinker'/'DGM', '85').
card_multiverse_id('mirko vosk, mind drinker'/'DGM', '369026').
card_watermark('mirko vosk, mind drinker'/'DGM', 'Dimir').

card_in_set('morgue burst', 'DGM').
card_original_type('morgue burst'/'DGM', 'Sorcery').
card_original_text('morgue burst'/'DGM', 'Return target creature card from your graveyard to your hand. Morgue Burst deals damage to target creature or player equal to the power of the card returned this way.').
card_first_print('morgue burst', 'DGM').
card_image_name('morgue burst'/'DGM', 'morgue burst').
card_uid('morgue burst'/'DGM', 'DGM:Morgue Burst:morgue burst').
card_rarity('morgue burst'/'DGM', 'Common').
card_artist('morgue burst'/'DGM', 'Raymond Swanland').
card_number('morgue burst'/'DGM', '86').
card_flavor_text('morgue burst'/'DGM', '\"Let him in. He\'s on the list.\"\n—Olrich, Rakdos club owner').
card_multiverse_id('morgue burst'/'DGM', '369049').
card_watermark('morgue burst'/'DGM', 'Rakdos').

card_in_set('murmuring phantasm', 'DGM').
card_original_type('murmuring phantasm'/'DGM', 'Creature — Spirit').
card_original_text('murmuring phantasm'/'DGM', 'Defender').
card_first_print('murmuring phantasm', 'DGM').
card_image_name('murmuring phantasm'/'DGM', 'murmuring phantasm').
card_uid('murmuring phantasm'/'DGM', 'DGM:Murmuring Phantasm:murmuring phantasm').
card_rarity('murmuring phantasm'/'DGM', 'Common').
card_artist('murmuring phantasm'/'DGM', 'Peter Mohrbacher').
card_number('murmuring phantasm'/'DGM', '15').
card_flavor_text('murmuring phantasm'/'DGM', '\"The most insidious thing in the world is nonsense that sounds just plausible enough to listen to.\"\n—Lazav').
card_multiverse_id('murmuring phantasm'/'DGM', '369007').

card_in_set('mutant\'s prey', 'DGM').
card_original_type('mutant\'s prey'/'DGM', 'Instant').
card_original_text('mutant\'s prey'/'DGM', 'Target creature you control with a +1/+1 counter on it fights target creature an opponent controls. (Each deals damage equal to its power to the other.)').
card_first_print('mutant\'s prey', 'DGM').
card_image_name('mutant\'s prey'/'DGM', 'mutant\'s prey').
card_uid('mutant\'s prey'/'DGM', 'DGM:Mutant\'s Prey:mutant\'s prey').
card_rarity('mutant\'s prey'/'DGM', 'Common').
card_artist('mutant\'s prey'/'DGM', 'Ryan Barger').
card_number('mutant\'s prey'/'DGM', '45').
card_flavor_text('mutant\'s prey'/'DGM', 'Every street corner is a hunting ground, every citizen potential prey.').
card_multiverse_id('mutant\'s prey'/'DGM', '368988').

card_in_set('nivix cyclops', 'DGM').
card_original_type('nivix cyclops'/'DGM', 'Creature — Cyclops').
card_original_text('nivix cyclops'/'DGM', 'Defender\nWhenever you cast an instant or sorcery spell, Nivix Cyclops gets +3/+0 until end of turn and can attack this turn as though it didn\'t have defender.').
card_first_print('nivix cyclops', 'DGM').
card_image_name('nivix cyclops'/'DGM', 'nivix cyclops').
card_uid('nivix cyclops'/'DGM', 'DGM:Nivix Cyclops:nivix cyclops').
card_rarity('nivix cyclops'/'DGM', 'Common').
card_artist('nivix cyclops'/'DGM', 'Wayne Reynolds').
card_number('nivix cyclops'/'DGM', '87').
card_multiverse_id('nivix cyclops'/'DGM', '369093').
card_watermark('nivix cyclops'/'DGM', 'Izzet').

card_in_set('notion thief', 'DGM').
card_original_type('notion thief'/'DGM', 'Creature — Human Rogue').
card_original_text('notion thief'/'DGM', 'Flash\nIf an opponent would draw a card except the first one he or she draws in each of his or her draw steps, instead that player skips that draw and you draw a card.').
card_first_print('notion thief', 'DGM').
card_image_name('notion thief'/'DGM', 'notion thief').
card_uid('notion thief'/'DGM', 'DGM:Notion Thief:notion thief').
card_rarity('notion thief'/'DGM', 'Rare').
card_artist('notion thief'/'DGM', 'Clint Cearley').
card_number('notion thief'/'DGM', '88').
card_multiverse_id('notion thief'/'DGM', '368973').
card_watermark('notion thief'/'DGM', 'Dimir').

card_in_set('obzedat\'s aid', 'DGM').
card_original_type('obzedat\'s aid'/'DGM', 'Sorcery').
card_original_text('obzedat\'s aid'/'DGM', 'Return target permanent card from your graveyard to the battlefield.').
card_first_print('obzedat\'s aid', 'DGM').
card_image_name('obzedat\'s aid'/'DGM', 'obzedat\'s aid').
card_uid('obzedat\'s aid'/'DGM', 'DGM:Obzedat\'s Aid:obzedat\'s aid').
card_rarity('obzedat\'s aid'/'DGM', 'Rare').
card_artist('obzedat\'s aid'/'DGM', 'Dan Scott').
card_number('obzedat\'s aid'/'DGM', '89').
card_flavor_text('obzedat\'s aid'/'DGM', '\"The Obzedat have revived you with purpose. Don\'t squander their blessing.\"\n—Teysa Karlov, Grand Envoy of Orzhov').
card_multiverse_id('obzedat\'s aid'/'DGM', '369002').
card_watermark('obzedat\'s aid'/'DGM', 'Orzhov').

card_in_set('opal lake gatekeepers', 'DGM').
card_original_type('opal lake gatekeepers'/'DGM', 'Creature — Vedalken Soldier').
card_original_text('opal lake gatekeepers'/'DGM', 'When Opal Lake Gatekeepers enters the battlefield, if you control two or more Gates, you may draw a card.').
card_first_print('opal lake gatekeepers', 'DGM').
card_image_name('opal lake gatekeepers'/'DGM', 'opal lake gatekeepers').
card_uid('opal lake gatekeepers'/'DGM', 'DGM:Opal Lake Gatekeepers:opal lake gatekeepers').
card_rarity('opal lake gatekeepers'/'DGM', 'Common').
card_artist('opal lake gatekeepers'/'DGM', 'Seb McKinnon').
card_number('opal lake gatekeepers'/'DGM', '16').
card_flavor_text('opal lake gatekeepers'/'DGM', 'Sentries guard Ravnica\'s landmarks and outlying districts, unaware of these places\' import to the maze.').
card_multiverse_id('opal lake gatekeepers'/'DGM', '368996').

card_in_set('orzhov cluestone', 'DGM').
card_original_type('orzhov cluestone'/'DGM', 'Artifact').
card_original_text('orzhov cluestone'/'DGM', '{T}: Add {W} or {B} to your mana pool.\n{W}{B}, {T}, Sacrifice Orzhov Cluestone: Draw a card.').
card_first_print('orzhov cluestone', 'DGM').
card_image_name('orzhov cluestone'/'DGM', 'orzhov cluestone').
card_uid('orzhov cluestone'/'DGM', 'DGM:Orzhov Cluestone:orzhov cluestone').
card_rarity('orzhov cluestone'/'DGM', 'Common').
card_artist('orzhov cluestone'/'DGM', 'Raoul Vitale').
card_number('orzhov cluestone'/'DGM', '142').
card_flavor_text('orzhov cluestone'/'DGM', 'A symbol of power and wealth, the only commodities that matter to the Orzhov.').
card_multiverse_id('orzhov cluestone'/'DGM', '369078').
card_watermark('orzhov cluestone'/'DGM', 'Orzhov').

card_in_set('orzhov guildgate', 'DGM').
card_original_type('orzhov guildgate'/'DGM', 'Land — Gate').
card_original_text('orzhov guildgate'/'DGM', 'Orzhov Guildgate enters the battlefield tapped.\n{T}: Add {W} or {B} to your mana pool.').
card_image_name('orzhov guildgate'/'DGM', 'orzhov guildgate').
card_uid('orzhov guildgate'/'DGM', 'DGM:Orzhov Guildgate:orzhov guildgate').
card_rarity('orzhov guildgate'/'DGM', 'Common').
card_artist('orzhov guildgate'/'DGM', 'John Avon').
card_number('orzhov guildgate'/'DGM', '153').
card_flavor_text('orzhov guildgate'/'DGM', '\"We do not believe in hiding our wealth in the shadows. This is Orzhov, and this is how we reward greatness.\"\n—Teysa Karlov, Grand Envoy of Orzhov').
card_multiverse_id('orzhov guildgate'/'DGM', '369079').
card_watermark('orzhov guildgate'/'DGM', 'Orzhov').

card_in_set('phytoburst', 'DGM').
card_original_type('phytoburst'/'DGM', 'Sorcery').
card_original_text('phytoburst'/'DGM', 'Target creature gets +5/+5 until end of turn.').
card_first_print('phytoburst', 'DGM').
card_image_name('phytoburst'/'DGM', 'phytoburst').
card_uid('phytoburst'/'DGM', 'DGM:Phytoburst:phytoburst').
card_rarity('phytoburst'/'DGM', 'Common').
card_artist('phytoburst'/'DGM', 'Izzy').
card_number('phytoburst'/'DGM', '46').
card_flavor_text('phytoburst'/'DGM', 'Phytohydras are banned as houseplants in many residential spires. If left unfed, they find their own exit—usually at the cost of extensive property damage.').
card_multiverse_id('phytoburst'/'DGM', '369054').

card_in_set('pilfered plans', 'DGM').
card_original_type('pilfered plans'/'DGM', 'Sorcery').
card_original_text('pilfered plans'/'DGM', 'Target player puts the top two cards of his or her library into his or her graveyard. Draw two cards.').
card_first_print('pilfered plans', 'DGM').
card_image_name('pilfered plans'/'DGM', 'pilfered plans').
card_uid('pilfered plans'/'DGM', 'DGM:Pilfered Plans:pilfered plans').
card_rarity('pilfered plans'/'DGM', 'Common').
card_artist('pilfered plans'/'DGM', 'Michael C. Hayes').
card_number('pilfered plans'/'DGM', '90').
card_flavor_text('pilfered plans'/'DGM', 'Mirko Vosk hunted anyone with knowledge of the maze, draining the chances of the other guilds.').
card_multiverse_id('pilfered plans'/'DGM', '369099').
card_watermark('pilfered plans'/'DGM', 'Dimir').

card_in_set('plasm capture', 'DGM').
card_original_type('plasm capture'/'DGM', 'Instant').
card_original_text('plasm capture'/'DGM', 'Counter target spell. At the beginning of your next precombat main phase, add X mana in any combination of colors to your mana pool, where X is that spell\'s converted mana cost.').
card_first_print('plasm capture', 'DGM').
card_image_name('plasm capture'/'DGM', 'plasm capture').
card_uid('plasm capture'/'DGM', 'DGM:Plasm Capture:plasm capture').
card_rarity('plasm capture'/'DGM', 'Rare').
card_artist('plasm capture'/'DGM', 'Chase Stone').
card_number('plasm capture'/'DGM', '91').
card_flavor_text('plasm capture'/'DGM', '\"Everything serves a purpose. Even you.\"\n—Vorel of the Hull Clade').
card_multiverse_id('plasm capture'/'DGM', '369069').
card_watermark('plasm capture'/'DGM', 'Simic').

card_in_set('pontiff of blight', 'DGM').
card_original_type('pontiff of blight'/'DGM', 'Creature — Zombie Cleric').
card_original_text('pontiff of blight'/'DGM', 'Extort (Whenever you cast a spell, you may pay {W/B}. If you do, each opponent loses 1 life and you gain that much life.)\nOther creatures you control have extort. (If a creature has multiple instances of extort, each triggers separately.)').
card_first_print('pontiff of blight', 'DGM').
card_image_name('pontiff of blight'/'DGM', 'pontiff of blight').
card_uid('pontiff of blight'/'DGM', 'DGM:Pontiff of Blight:pontiff of blight').
card_rarity('pontiff of blight'/'DGM', 'Rare').
card_artist('pontiff of blight'/'DGM', 'Seb McKinnon').
card_number('pontiff of blight'/'DGM', '27').
card_multiverse_id('pontiff of blight'/'DGM', '369006').
card_watermark('pontiff of blight'/'DGM', 'Orzhov').

card_in_set('possibility storm', 'DGM').
card_original_type('possibility storm'/'DGM', 'Enchantment').
card_original_text('possibility storm'/'DGM', 'Whenever a player casts a spell from his or her hand, that player exiles it, then exiles cards from the top of his or her library until he or she exiles a card that shares a card type with it. That player may cast that card without paying its mana cost. Then he or she puts all cards exiled with Possibility Storm on the bottom of his or her library in a random order.').
card_first_print('possibility storm', 'DGM').
card_image_name('possibility storm'/'DGM', 'possibility storm').
card_uid('possibility storm'/'DGM', 'DGM:Possibility Storm:possibility storm').
card_rarity('possibility storm'/'DGM', 'Rare').
card_artist('possibility storm'/'DGM', 'Jason Felix').
card_number('possibility storm'/'DGM', '34').
card_multiverse_id('possibility storm'/'DGM', '369013').

card_in_set('profit', 'DGM').
card_original_type('profit'/'DGM', 'Instant').
card_original_text('profit'/'DGM', 'Creatures you control get +1/+1 until end of turn.\n//\nLoss\n{2}{B}\nInstant\nCreatures your opponents control get -1/-1 until end of turn.\n//\nFuse (You may cast one or both halves of this card from your hand.)').
card_first_print('profit', 'DGM').
card_image_name('profit'/'DGM', 'profitloss').
card_uid('profit'/'DGM', 'DGM:Profit:profitloss').
card_rarity('profit'/'DGM', 'Uncommon').
card_artist('profit'/'DGM', 'Kev Walker').
card_number('profit'/'DGM', '130a').
card_multiverse_id('profit'/'DGM', '369071').

card_in_set('progenitor mimic', 'DGM').
card_original_type('progenitor mimic'/'DGM', 'Creature — Shapeshifter').
card_original_text('progenitor mimic'/'DGM', 'You may have Progenitor Mimic enter the battlefield as a copy of any creature on the battlefield except it gains \"At the beginning of your upkeep, if this creature isn\'t a token, put a token onto the battlefield that\'s a copy of this creature.\"').
card_first_print('progenitor mimic', 'DGM').
card_image_name('progenitor mimic'/'DGM', 'progenitor mimic').
card_uid('progenitor mimic'/'DGM', 'DGM:Progenitor Mimic:progenitor mimic').
card_rarity('progenitor mimic'/'DGM', 'Mythic Rare').
card_artist('progenitor mimic'/'DGM', 'Daarken').
card_number('progenitor mimic'/'DGM', '92').
card_multiverse_id('progenitor mimic'/'DGM', '369001').
card_watermark('progenitor mimic'/'DGM', 'Simic').

card_in_set('protect', 'DGM').
card_original_type('protect'/'DGM', 'Instant').
card_original_text('protect'/'DGM', 'Target creature gets +2/+4 until end of turn.\n//\nServe\n{1}{U}\nInstant\nTarget creature gets -6/-0 until end of turn.\n//\nFuse (You may cast one or both halves of this card from your hand.)').
card_first_print('protect', 'DGM').
card_image_name('protect'/'DGM', 'protectserve').
card_uid('protect'/'DGM', 'DGM:Protect:protectserve').
card_rarity('protect'/'DGM', 'Uncommon').
card_artist('protect'/'DGM', 'Ryan Barger').
card_number('protect'/'DGM', '131a').
card_multiverse_id('protect'/'DGM', '369024').

card_in_set('punish the enemy', 'DGM').
card_original_type('punish the enemy'/'DGM', 'Instant').
card_original_text('punish the enemy'/'DGM', 'Punish the Enemy deals 3 damage to target player and 3 damage to target creature.').
card_first_print('punish the enemy', 'DGM').
card_image_name('punish the enemy'/'DGM', 'punish the enemy').
card_uid('punish the enemy'/'DGM', 'DGM:Punish the Enemy:punish the enemy').
card_rarity('punish the enemy'/'DGM', 'Common').
card_artist('punish the enemy'/'DGM', 'Slawomir Maniak').
card_number('punish the enemy'/'DGM', '35').
card_flavor_text('punish the enemy'/'DGM', '\"When justice descends, the servant will burn with the master.\"\n—Aurelia').
card_multiverse_id('punish the enemy'/'DGM', '369090').

card_in_set('putrefy', 'DGM').
card_original_type('putrefy'/'DGM', 'Instant').
card_original_text('putrefy'/'DGM', 'Destroy target artifact or creature. It can\'t be regenerated.').
card_image_name('putrefy'/'DGM', 'putrefy').
card_uid('putrefy'/'DGM', 'DGM:Putrefy:putrefy').
card_rarity('putrefy'/'DGM', 'Uncommon').
card_artist('putrefy'/'DGM', 'Igor Kieryluk').
card_number('putrefy'/'DGM', '93').
card_flavor_text('putrefy'/'DGM', '\"We are all decaying, always in a state of near-death. One moment without breath and we begin to break down.\"\n—Cevraya, Golgari shaman').
card_multiverse_id('putrefy'/'DGM', '369073').
card_watermark('putrefy'/'DGM', 'Golgari').

card_in_set('pyrewild shaman', 'DGM').
card_original_type('pyrewild shaman'/'DGM', 'Creature — Goblin Shaman').
card_original_text('pyrewild shaman'/'DGM', 'Bloodrush — {1}{R}, Discard Pyrewild Shaman: Target attacking creature gets +3/+1 until end of turn.\nWhenever one or more creatures you control deal combat damage to a player, if Pyrewild Shaman is in your graveyard, you may pay {3}. If you do, return Pyrewild Shaman to your hand.').
card_first_print('pyrewild shaman', 'DGM').
card_image_name('pyrewild shaman'/'DGM', 'pyrewild shaman').
card_uid('pyrewild shaman'/'DGM', 'DGM:Pyrewild Shaman:pyrewild shaman').
card_rarity('pyrewild shaman'/'DGM', 'Rare').
card_artist('pyrewild shaman'/'DGM', 'Lucas Graciano').
card_number('pyrewild shaman'/'DGM', '36').
card_multiverse_id('pyrewild shaman'/'DGM', '368946').
card_watermark('pyrewild shaman'/'DGM', 'Gruul').

card_in_set('rakdos cluestone', 'DGM').
card_original_type('rakdos cluestone'/'DGM', 'Artifact').
card_original_text('rakdos cluestone'/'DGM', '{T}: Add {B} or {R} to your mana pool.\n{B}{R}, {T}, Sacrifice Rakdos Cluestone: Draw a card.').
card_first_print('rakdos cluestone', 'DGM').
card_image_name('rakdos cluestone'/'DGM', 'rakdos cluestone').
card_uid('rakdos cluestone'/'DGM', 'DGM:Rakdos Cluestone:rakdos cluestone').
card_rarity('rakdos cluestone'/'DGM', 'Common').
card_artist('rakdos cluestone'/'DGM', 'Raoul Vitale').
card_number('rakdos cluestone'/'DGM', '143').
card_flavor_text('rakdos cluestone'/'DGM', '\"Burn. Bleed. Enjoy.\"\n—Cluestone inscription').
card_multiverse_id('rakdos cluestone'/'DGM', '368975').
card_watermark('rakdos cluestone'/'DGM', 'Rakdos').

card_in_set('rakdos drake', 'DGM').
card_original_type('rakdos drake'/'DGM', 'Creature — Drake').
card_original_text('rakdos drake'/'DGM', 'Flying\nUnleash (You may have this creature enter the battlefield with a +1/+1 counter on it. It can\'t block as long as it has a +1/+1 counter on it.)').
card_first_print('rakdos drake', 'DGM').
card_image_name('rakdos drake'/'DGM', 'rakdos drake').
card_uid('rakdos drake'/'DGM', 'DGM:Rakdos Drake:rakdos drake').
card_rarity('rakdos drake'/'DGM', 'Common').
card_artist('rakdos drake'/'DGM', 'Karl Kopinski').
card_number('rakdos drake'/'DGM', '28').
card_flavor_text('rakdos drake'/'DGM', 'Rakdos rioters paint their faces to resemble the drakes\' bloodied muzzles.').
card_multiverse_id('rakdos drake'/'DGM', '369085').
card_watermark('rakdos drake'/'DGM', 'Rakdos').

card_in_set('rakdos guildgate', 'DGM').
card_original_type('rakdos guildgate'/'DGM', 'Land — Gate').
card_original_text('rakdos guildgate'/'DGM', 'Rakdos Guildgate enters the battlefield tapped.\n{T}: Add {B} or {R} to your mana pool.').
card_image_name('rakdos guildgate'/'DGM', 'rakdos guildgate').
card_uid('rakdos guildgate'/'DGM', 'DGM:Rakdos Guildgate:rakdos guildgate').
card_rarity('rakdos guildgate'/'DGM', 'Common').
card_artist('rakdos guildgate'/'DGM', 'Eytan Zana').
card_number('rakdos guildgate'/'DGM', '154').
card_flavor_text('rakdos guildgate'/'DGM', 'Maze-runners who pass through the Rakdos gate leave behind their scruples and a significant amount of skin.').
card_multiverse_id('rakdos guildgate'/'DGM', '368990').
card_watermark('rakdos guildgate'/'DGM', 'Rakdos').

card_in_set('ral zarek', 'DGM').
card_original_type('ral zarek'/'DGM', 'Planeswalker — Ral').
card_original_text('ral zarek'/'DGM', '+1: Tap target permanent, then untap another target permanent.\n-2: Ral Zarek deals 3 damage to target creature or player.\n-7: Flip five coins. Take an extra turn after this one for each coin that comes up heads.').
card_first_print('ral zarek', 'DGM').
card_image_name('ral zarek'/'DGM', 'ral zarek').
card_uid('ral zarek'/'DGM', 'DGM:Ral Zarek:ral zarek').
card_rarity('ral zarek'/'DGM', 'Mythic Rare').
card_artist('ral zarek'/'DGM', 'Eric Deschamps').
card_number('ral zarek'/'DGM', '94').
card_multiverse_id('ral zarek'/'DGM', '369031').

card_in_set('ready', 'DGM').
card_original_type('ready'/'DGM', 'Instant').
card_original_text('ready'/'DGM', 'Creatures you control are indestructible this turn. Untap each creature you control.\n//\nWilling\n{1}{W}{B}\nInstant\nCreatures you control gain deathtouch and lifelink until end of turn.\n//\nFuse (You may cast one or both halves of this card from your hand.)').
card_first_print('ready', 'DGM').
card_image_name('ready'/'DGM', 'readywilling').
card_uid('ready'/'DGM', 'DGM:Ready:readywilling').
card_rarity('ready'/'DGM', 'Rare').
card_artist('ready'/'DGM', 'Zoltan Boros').
card_number('ready'/'DGM', '132a').
card_multiverse_id('ready'/'DGM', '368967').

card_in_set('reap intellect', 'DGM').
card_original_type('reap intellect'/'DGM', 'Sorcery').
card_original_text('reap intellect'/'DGM', 'Target opponent reveals his or her hand. You choose up to X nonland cards from it and exile them. For each card exiled this way, search that player\'s graveyard, hand, and library for any number of cards with the same name as that card and exile them. Then that player shuffles his or her library.').
card_first_print('reap intellect', 'DGM').
card_image_name('reap intellect'/'DGM', 'reap intellect').
card_uid('reap intellect'/'DGM', 'DGM:Reap Intellect:reap intellect').
card_rarity('reap intellect'/'DGM', 'Mythic Rare').
card_artist('reap intellect'/'DGM', 'Steven Belledin').
card_number('reap intellect'/'DGM', '95').
card_multiverse_id('reap intellect'/'DGM', '368958').
card_watermark('reap intellect'/'DGM', 'Dimir').

card_in_set('release', 'DGM').
card_original_type('release'/'DGM', 'Sorcery').
card_original_text('release'/'DGM', 'Gain control of target permanent until end of turn. Untap it. It gains haste until end of turn.\n//\nRelease\n{4}{R}{W}\nSorcery\nEach player sacrifices an artifact, a creature, an enchantment, a land, and a planeswalker.\n//\nFuse (You may cast one or both halves of this card from your hand.)').
card_first_print('release', 'DGM').
card_image_name('release'/'DGM', 'catchrelease').
card_uid('release'/'DGM', 'DGM:Release:catchrelease').
card_rarity('release'/'DGM', 'Rare').
card_artist('release'/'DGM', 'Kev Walker').
card_number('release'/'DGM', '125b').
card_multiverse_id('release'/'DGM', '368982').

card_in_set('render silent', 'DGM').
card_original_type('render silent'/'DGM', 'Instant').
card_original_text('render silent'/'DGM', 'Counter target spell. Its controller can\'t cast spells this turn.').
card_image_name('render silent'/'DGM', 'render silent').
card_uid('render silent'/'DGM', 'DGM:Render Silent:render silent').
card_rarity('render silent'/'DGM', 'Rare').
card_artist('render silent'/'DGM', 'Matt Stewart').
card_number('render silent'/'DGM', '96').
card_flavor_text('render silent'/'DGM', '\"We have confiscated your spells as evidence. Once we conclude our investigation, you may petition to have them returned.\"').
card_multiverse_id('render silent'/'DGM', '369022').
card_watermark('render silent'/'DGM', 'Azorius').

card_in_set('renegade krasis', 'DGM').
card_original_type('renegade krasis'/'DGM', 'Creature — Beast Mutant').
card_original_text('renegade krasis'/'DGM', 'Evolve (Whenever a creature enters the battlefield under your control, if that creature has greater power or toughness than this creature, put a +1/+1 counter on this creature.)\nWhenever Renegade Krasis evolves, put a +1/+1 counter on each other creature you control with a +1/+1 counter on it.').
card_first_print('renegade krasis', 'DGM').
card_image_name('renegade krasis'/'DGM', 'renegade krasis').
card_uid('renegade krasis'/'DGM', 'DGM:Renegade Krasis:renegade krasis').
card_rarity('renegade krasis'/'DGM', 'Rare').
card_artist('renegade krasis'/'DGM', 'Howard Lyon').
card_number('renegade krasis'/'DGM', '47').
card_multiverse_id('renegade krasis'/'DGM', '369033').
card_watermark('renegade krasis'/'DGM', 'Simic').

card_in_set('renounce the guilds', 'DGM').
card_original_type('renounce the guilds'/'DGM', 'Instant').
card_original_text('renounce the guilds'/'DGM', 'Each player sacrifices a multicolored permanent.').
card_first_print('renounce the guilds', 'DGM').
card_image_name('renounce the guilds'/'DGM', 'renounce the guilds').
card_uid('renounce the guilds'/'DGM', 'DGM:Renounce the Guilds:renounce the guilds').
card_rarity('renounce the guilds'/'DGM', 'Rare').
card_artist('renounce the guilds'/'DGM', 'Daarken').
card_number('renounce the guilds'/'DGM', '5').
card_flavor_text('renounce the guilds'/'DGM', '\"Scores will die in the name of peace. That is what you call compromise?\"\n—Gideon Jura, to Aurelia').
card_multiverse_id('renounce the guilds'/'DGM', '368993').

card_in_set('restore the peace', 'DGM').
card_original_type('restore the peace'/'DGM', 'Instant').
card_original_text('restore the peace'/'DGM', 'Return each creature that dealt damage this turn to its owner\'s hand.').
card_first_print('restore the peace', 'DGM').
card_image_name('restore the peace'/'DGM', 'restore the peace').
card_uid('restore the peace'/'DGM', 'DGM:Restore the Peace:restore the peace').
card_rarity('restore the peace'/'DGM', 'Uncommon').
card_artist('restore the peace'/'DGM', 'Kev Walker').
card_number('restore the peace'/'DGM', '97').
card_flavor_text('restore the peace'/'DGM', '\"You can always count on the Azorius to ruin a party.\"\n—Massacre Girl').
card_multiverse_id('restore the peace'/'DGM', '368978').
card_watermark('restore the peace'/'DGM', 'Azorius').

card_in_set('riot control', 'DGM').
card_original_type('riot control'/'DGM', 'Instant').
card_original_text('riot control'/'DGM', 'You gain 1 life for each creature your opponents control. Prevent all damage that would be dealt to you this turn.').
card_first_print('riot control', 'DGM').
card_image_name('riot control'/'DGM', 'riot control').
card_uid('riot control'/'DGM', 'DGM:Riot Control:riot control').
card_rarity('riot control'/'DGM', 'Common').
card_artist('riot control'/'DGM', 'Slawomir Maniak').
card_number('riot control'/'DGM', '6').
card_flavor_text('riot control'/'DGM', '\"Ravnicans need order. If they don\'t have it, we shall bring it to them.\"\n—Isperia').
card_multiverse_id('riot control'/'DGM', '368979').

card_in_set('riot piker', 'DGM').
card_original_type('riot piker'/'DGM', 'Creature — Goblin Berserker').
card_original_text('riot piker'/'DGM', 'First strike\nRiot Piker attacks each turn if able.').
card_first_print('riot piker', 'DGM').
card_image_name('riot piker'/'DGM', 'riot piker').
card_uid('riot piker'/'DGM', 'DGM:Riot Piker:riot piker').
card_rarity('riot piker'/'DGM', 'Common').
card_artist('riot piker'/'DGM', 'Christopher Moeller').
card_number('riot piker'/'DGM', '37').
card_flavor_text('riot piker'/'DGM', 'He doesn\'t care whether he\'s rioting about trade rights or just having a drunken brawl. He wakes up every morning eager to smash some heads.').
card_multiverse_id('riot piker'/'DGM', '368998').

card_in_set('rot farm skeleton', 'DGM').
card_original_type('rot farm skeleton'/'DGM', 'Creature — Plant Skeleton').
card_original_text('rot farm skeleton'/'DGM', 'Rot Farm Skeleton can\'t block.\n{2}{B}{G}, Put the top four cards of your library into your graveyard: Return Rot Farm Skeleton from your graveyard to the battlefield. Activate this ability only any time you could cast a sorcery.').
card_first_print('rot farm skeleton', 'DGM').
card_image_name('rot farm skeleton'/'DGM', 'rot farm skeleton').
card_uid('rot farm skeleton'/'DGM', 'DGM:Rot Farm Skeleton:rot farm skeleton').
card_rarity('rot farm skeleton'/'DGM', 'Uncommon').
card_artist('rot farm skeleton'/'DGM', 'Maciej Kuciara').
card_number('rot farm skeleton'/'DGM', '98').
card_multiverse_id('rot farm skeleton'/'DGM', '368986').
card_watermark('rot farm skeleton'/'DGM', 'Golgari').

card_in_set('rubblebelt maaka', 'DGM').
card_original_type('rubblebelt maaka'/'DGM', 'Creature — Cat').
card_original_text('rubblebelt maaka'/'DGM', 'Bloodrush — {R}, Discard Rubblebelt Maaka: Target attacking creature gets +3/+3 until end of turn.').
card_first_print('rubblebelt maaka', 'DGM').
card_image_name('rubblebelt maaka'/'DGM', 'rubblebelt maaka').
card_uid('rubblebelt maaka'/'DGM', 'DGM:Rubblebelt Maaka:rubblebelt maaka').
card_rarity('rubblebelt maaka'/'DGM', 'Common').
card_artist('rubblebelt maaka'/'DGM', 'Eric Velhagen').
card_number('rubblebelt maaka'/'DGM', '38').
card_flavor_text('rubblebelt maaka'/'DGM', '\"The maaka remain loyal to us in this time of strife. We don\'t even have to feed them—our enemies do.\"\n—Nedja, Gruul shaman').
card_multiverse_id('rubblebelt maaka'/'DGM', '369075').
card_watermark('rubblebelt maaka'/'DGM', 'Gruul').

card_in_set('runner\'s bane', 'DGM').
card_original_type('runner\'s bane'/'DGM', 'Enchantment — Aura').
card_original_text('runner\'s bane'/'DGM', 'Enchant creature with power 3 or less\nWhen Runner\'s Bane enters the battlefield, tap enchanted creature.\nEnchanted creature doesn\'t untap during its controller\'s untap step.').
card_first_print('runner\'s bane', 'DGM').
card_image_name('runner\'s bane'/'DGM', 'runner\'s bane').
card_uid('runner\'s bane'/'DGM', 'DGM:Runner\'s Bane:runner\'s bane').
card_rarity('runner\'s bane'/'DGM', 'Common').
card_artist('runner\'s bane'/'DGM', 'Karl Kopinski').
card_number('runner\'s bane'/'DGM', '17').
card_multiverse_id('runner\'s bane'/'DGM', '368966').

card_in_set('ruric thar, the unbowed', 'DGM').
card_original_type('ruric thar, the unbowed'/'DGM', 'Legendary Creature — Ogre Warrior').
card_original_text('ruric thar, the unbowed'/'DGM', 'Vigilance, reach\nRuric Thar, the Unbowed attacks each turn if able.\nWhenever a player casts a noncreature spell, Ruric Thar deals 6 damage to that player.').
card_first_print('ruric thar, the unbowed', 'DGM').
card_image_name('ruric thar, the unbowed'/'DGM', 'ruric thar, the unbowed').
card_uid('ruric thar, the unbowed'/'DGM', 'DGM:Ruric Thar, the Unbowed:ruric thar, the unbowed').
card_rarity('ruric thar, the unbowed'/'DGM', 'Rare').
card_artist('ruric thar, the unbowed'/'DGM', 'Tyler Jacobson').
card_number('ruric thar, the unbowed'/'DGM', '99').
card_multiverse_id('ruric thar, the unbowed'/'DGM', '369025').
card_watermark('ruric thar, the unbowed'/'DGM', 'Gruul').

card_in_set('saruli gatekeepers', 'DGM').
card_original_type('saruli gatekeepers'/'DGM', 'Creature — Elf Warrior').
card_original_text('saruli gatekeepers'/'DGM', 'When Saruli Gatekeepers enters the battlefield, if you control two or more Gates, you gain 7 life.').
card_first_print('saruli gatekeepers', 'DGM').
card_image_name('saruli gatekeepers'/'DGM', 'saruli gatekeepers').
card_uid('saruli gatekeepers'/'DGM', 'DGM:Saruli Gatekeepers:saruli gatekeepers').
card_rarity('saruli gatekeepers'/'DGM', 'Common').
card_artist('saruli gatekeepers'/'DGM', 'Chris Rahn').
card_number('saruli gatekeepers'/'DGM', '48').
card_flavor_text('saruli gatekeepers'/'DGM', 'In Ravnica\'s wilder districts, nature still stirs, and its protectors still mind their posts.').
card_multiverse_id('saruli gatekeepers'/'DGM', '369088').

card_in_set('savageborn hydra', 'DGM').
card_original_type('savageborn hydra'/'DGM', 'Creature — Hydra').
card_original_text('savageborn hydra'/'DGM', 'Double strike\nSavageborn Hydra enters the battlefield with X +1/+1 counters on it.\n{1}{R/G}: Put a +1/+1 counter on Savageborn Hydra. Activate this ability only any time you could cast a sorcery.').
card_first_print('savageborn hydra', 'DGM').
card_image_name('savageborn hydra'/'DGM', 'savageborn hydra').
card_uid('savageborn hydra'/'DGM', 'DGM:Savageborn Hydra:savageborn hydra').
card_rarity('savageborn hydra'/'DGM', 'Mythic Rare').
card_artist('savageborn hydra'/'DGM', 'Raymond Swanland').
card_number('savageborn hydra'/'DGM', '100').
card_multiverse_id('savageborn hydra'/'DGM', '368952').
card_watermark('savageborn hydra'/'DGM', 'Gruul').

card_in_set('scab-clan giant', 'DGM').
card_original_type('scab-clan giant'/'DGM', 'Creature — Giant Warrior').
card_original_text('scab-clan giant'/'DGM', 'When Scab-Clan Giant enters the battlefield, it fights target creature an opponent controls chosen at random.').
card_first_print('scab-clan giant', 'DGM').
card_image_name('scab-clan giant'/'DGM', 'scab-clan giant').
card_uid('scab-clan giant'/'DGM', 'DGM:Scab-Clan Giant:scab-clan giant').
card_rarity('scab-clan giant'/'DGM', 'Uncommon').
card_artist('scab-clan giant'/'DGM', 'Zoltan Boros').
card_number('scab-clan giant'/'DGM', '101').
card_flavor_text('scab-clan giant'/'DGM', 'For the Gruul, bone-crushing rage is the first priority. Accuracy comes in a distant ninetieth.').
card_multiverse_id('scab-clan giant'/'DGM', '369005').
card_watermark('scab-clan giant'/'DGM', 'Gruul').

card_in_set('scion of vitu-ghazi', 'DGM').
card_original_type('scion of vitu-ghazi'/'DGM', 'Creature — Elemental').
card_original_text('scion of vitu-ghazi'/'DGM', 'When Scion of Vitu-Ghazi enters the battlefield, if you cast it from your hand, put a 1/1 white Bird creature token with flying onto the battlefield, then populate. (Put a token onto the battlefield that\'s a copy of a creature token you control.)').
card_first_print('scion of vitu-ghazi', 'DGM').
card_image_name('scion of vitu-ghazi'/'DGM', 'scion of vitu-ghazi').
card_uid('scion of vitu-ghazi'/'DGM', 'DGM:Scion of Vitu-Ghazi:scion of vitu-ghazi').
card_rarity('scion of vitu-ghazi'/'DGM', 'Rare').
card_artist('scion of vitu-ghazi'/'DGM', 'Willian Murai').
card_number('scion of vitu-ghazi'/'DGM', '7').
card_multiverse_id('scion of vitu-ghazi'/'DGM', '369092').
card_watermark('scion of vitu-ghazi'/'DGM', 'Selesnya').

card_in_set('selesnya cluestone', 'DGM').
card_original_type('selesnya cluestone'/'DGM', 'Artifact').
card_original_text('selesnya cluestone'/'DGM', '{T}: Add {G} or {W} to your mana pool.\n{G}{W}, {T}, Sacrifice Selesnya Cluestone: Draw a card.').
card_first_print('selesnya cluestone', 'DGM').
card_image_name('selesnya cluestone'/'DGM', 'selesnya cluestone').
card_uid('selesnya cluestone'/'DGM', 'DGM:Selesnya Cluestone:selesnya cluestone').
card_rarity('selesnya cluestone'/'DGM', 'Common').
card_artist('selesnya cluestone'/'DGM', 'Raoul Vitale').
card_number('selesnya cluestone'/'DGM', '144').
card_flavor_text('selesnya cluestone'/'DGM', '\"Break it if you must. The loss of one relic is nothing next to the value of enriching us all.\"\n—Suniel the Woodwise').
card_multiverse_id('selesnya cluestone'/'DGM', '369060').
card_watermark('selesnya cluestone'/'DGM', 'Selesnya').

card_in_set('selesnya guildgate', 'DGM').
card_original_type('selesnya guildgate'/'DGM', 'Land — Gate').
card_original_text('selesnya guildgate'/'DGM', 'Selesnya Guildgate enters the battlefield tapped.\n{T}: Add {G} or {W} to your mana pool.').
card_image_name('selesnya guildgate'/'DGM', 'selesnya guildgate').
card_uid('selesnya guildgate'/'DGM', 'DGM:Selesnya Guildgate:selesnya guildgate').
card_rarity('selesnya guildgate'/'DGM', 'Common').
card_artist('selesnya guildgate'/'DGM', 'Howard Lyon').
card_number('selesnya guildgate'/'DGM', '155').
card_flavor_text('selesnya guildgate'/'DGM', 'The Selesyna welcome all to help them heal the city\'s wounds. The price, however, is devotion to the guild and a selfless belief in Trostani.').
card_multiverse_id('selesnya guildgate'/'DGM', '369018').
card_watermark('selesnya guildgate'/'DGM', 'Selesnya').

card_in_set('serve', 'DGM').
card_original_type('serve'/'DGM', 'Instant').
card_original_text('serve'/'DGM', 'Target creature gets +2/+4 until end of turn.\n//\nServe\n{1}{U}\nInstant\nTarget creature gets -6/-0 until end of turn.\n//\nFuse (You may cast one or both halves of this card from your hand.)').
card_first_print('serve', 'DGM').
card_image_name('serve'/'DGM', 'protectserve').
card_uid('serve'/'DGM', 'DGM:Serve:protectserve').
card_rarity('serve'/'DGM', 'Uncommon').
card_artist('serve'/'DGM', 'Ryan Barger').
card_number('serve'/'DGM', '131b').
card_multiverse_id('serve'/'DGM', '369024').

card_in_set('showstopper', 'DGM').
card_original_type('showstopper'/'DGM', 'Instant').
card_original_text('showstopper'/'DGM', 'Until end of turn, creatures you control gain \"When this creature dies, it deals 2 damage to target creature an opponent controls.\"').
card_first_print('showstopper', 'DGM').
card_image_name('showstopper'/'DGM', 'showstopper').
card_uid('showstopper'/'DGM', 'DGM:Showstopper:showstopper').
card_rarity('showstopper'/'DGM', 'Uncommon').
card_artist('showstopper'/'DGM', 'Steve Prescott').
card_number('showstopper'/'DGM', '102').
card_flavor_text('showstopper'/'DGM', 'The audience quickly realized a few things: it wasn\'t a magic trick, there wasn\'t candy in there, and they\'d need new clothes.').
card_multiverse_id('showstopper'/'DGM', '368945').
card_watermark('showstopper'/'DGM', 'Rakdos').

card_in_set('simic cluestone', 'DGM').
card_original_type('simic cluestone'/'DGM', 'Artifact').
card_original_text('simic cluestone'/'DGM', '{T}: Add {G} or {U} to your mana pool.\n{G}{U}, {T}, Sacrifice Simic Cluestone: Draw a card.').
card_first_print('simic cluestone', 'DGM').
card_image_name('simic cluestone'/'DGM', 'simic cluestone').
card_uid('simic cluestone'/'DGM', 'DGM:Simic Cluestone:simic cluestone').
card_rarity('simic cluestone'/'DGM', 'Common').
card_artist('simic cluestone'/'DGM', 'Raoul Vitale').
card_number('simic cluestone'/'DGM', '145').
card_flavor_text('simic cluestone'/'DGM', 'Simic symbols are among the most intricate and fragile, yet few vandalize them for fear of the strange lifeforms that might be released.').
card_multiverse_id('simic cluestone'/'DGM', '368963').
card_watermark('simic cluestone'/'DGM', 'Simic').

card_in_set('simic guildgate', 'DGM').
card_original_type('simic guildgate'/'DGM', 'Land — Gate').
card_original_text('simic guildgate'/'DGM', 'Simic Guildgate enters the battlefield tapped.\n{T}: Add {G} or {U} to your mana pool.').
card_image_name('simic guildgate'/'DGM', 'simic guildgate').
card_uid('simic guildgate'/'DGM', 'DGM:Simic Guildgate:simic guildgate').
card_rarity('simic guildgate'/'DGM', 'Common').
card_artist('simic guildgate'/'DGM', 'Svetlin Velinov').
card_number('simic guildgate'/'DGM', '156').
card_flavor_text('simic guildgate'/'DGM', 'Simic biomancers don\'t concern themselves with guarding a gate. Their ever-present creations are enough to ward off trespassers.').
card_multiverse_id('simic guildgate'/'DGM', '368959').
card_watermark('simic guildgate'/'DGM', 'Simic').

card_in_set('sin collector', 'DGM').
card_original_type('sin collector'/'DGM', 'Creature — Human Cleric').
card_original_text('sin collector'/'DGM', 'When Sin Collector enters the battlefield, target opponent reveals his or her hand. You choose an instant or sorcery card from it and exile that card.').
card_image_name('sin collector'/'DGM', 'sin collector').
card_uid('sin collector'/'DGM', 'DGM:Sin Collector:sin collector').
card_rarity('sin collector'/'DGM', 'Uncommon').
card_artist('sin collector'/'DGM', 'Mike Bierek').
card_number('sin collector'/'DGM', '103').
card_flavor_text('sin collector'/'DGM', '\"That must weigh heavily on your soul. Let me purge it for you.\"').
card_multiverse_id('sin collector'/'DGM', '368968').
card_watermark('sin collector'/'DGM', 'Orzhov').

card_in_set('sinister possession', 'DGM').
card_original_type('sinister possession'/'DGM', 'Enchantment — Aura').
card_original_text('sinister possession'/'DGM', 'Enchant creature\nWhenever enchanted creature attacks or blocks, its controller loses 2 life.').
card_first_print('sinister possession', 'DGM').
card_image_name('sinister possession'/'DGM', 'sinister possession').
card_uid('sinister possession'/'DGM', 'DGM:Sinister Possession:sinister possession').
card_rarity('sinister possession'/'DGM', 'Common').
card_artist('sinister possession'/'DGM', 'Anthony Palumbo').
card_number('sinister possession'/'DGM', '29').
card_flavor_text('sinister possession'/'DGM', '\"You caught something on a cold night\'s stroll? No, I\'d say something caught you.\"\n—Mezim Magrah, civic healer').
card_multiverse_id('sinister possession'/'DGM', '369043').

card_in_set('sire of insanity', 'DGM').
card_original_type('sire of insanity'/'DGM', 'Creature — Demon').
card_original_text('sire of insanity'/'DGM', 'At the beginning of each end step, each player discards his or her hand.').
card_first_print('sire of insanity', 'DGM').
card_image_name('sire of insanity'/'DGM', 'sire of insanity').
card_uid('sire of insanity'/'DGM', 'DGM:Sire of Insanity:sire of insanity').
card_rarity('sire of insanity'/'DGM', 'Rare').
card_artist('sire of insanity'/'DGM', 'Peter Mohrbacher').
card_number('sire of insanity'/'DGM', '104').
card_flavor_text('sire of insanity'/'DGM', 'Its victims become mindless lunatics. Conveniently, that\'s the first step in joining the Cult of Rakdos.').
card_multiverse_id('sire of insanity'/'DGM', '369068').
card_watermark('sire of insanity'/'DGM', 'Rakdos').

card_in_set('skylasher', 'DGM').
card_original_type('skylasher'/'DGM', 'Creature — Insect').
card_original_text('skylasher'/'DGM', 'Flash\nSkylasher can\'t be countered.\nReach, protection from blue').
card_first_print('skylasher', 'DGM').
card_image_name('skylasher'/'DGM', 'skylasher').
card_uid('skylasher'/'DGM', 'DGM:Skylasher:skylasher').
card_rarity('skylasher'/'DGM', 'Rare').
card_artist('skylasher'/'DGM', 'Dan Scott').
card_number('skylasher'/'DGM', '49').
card_flavor_text('skylasher'/'DGM', '\"It preys on those who would spy on our lands, so we never prey on it.\"\n—Kroshkar, Gruul shaman').
card_multiverse_id('skylasher'/'DGM', '369083').

card_in_set('smelt-ward gatekeepers', 'DGM').
card_original_type('smelt-ward gatekeepers'/'DGM', 'Creature — Human Warrior').
card_original_text('smelt-ward gatekeepers'/'DGM', 'When Smelt-Ward Gatekeepers enters the battlefield, if you control two or more Gates, gain control of target creature an opponent controls until end of turn. Untap that creature. It gains haste until end of turn.').
card_first_print('smelt-ward gatekeepers', 'DGM').
card_image_name('smelt-ward gatekeepers'/'DGM', 'smelt-ward gatekeepers').
card_uid('smelt-ward gatekeepers'/'DGM', 'DGM:Smelt-Ward Gatekeepers:smelt-ward gatekeepers').
card_rarity('smelt-ward gatekeepers'/'DGM', 'Common').
card_artist('smelt-ward gatekeepers'/'DGM', 'Daarken').
card_number('smelt-ward gatekeepers'/'DGM', '39').
card_multiverse_id('smelt-ward gatekeepers'/'DGM', '369067').

card_in_set('species gorger', 'DGM').
card_original_type('species gorger'/'DGM', 'Creature — Frog Beast').
card_original_text('species gorger'/'DGM', 'At the beginning of your upkeep, return a creature you control to its owner\'s hand.').
card_first_print('species gorger', 'DGM').
card_image_name('species gorger'/'DGM', 'species gorger').
card_uid('species gorger'/'DGM', 'DGM:Species Gorger:species gorger').
card_rarity('species gorger'/'DGM', 'Uncommon').
card_artist('species gorger'/'DGM', 'Min Yum').
card_number('species gorger'/'DGM', '105').
card_flavor_text('species gorger'/'DGM', '\"We raised eelhawks to control the squidflies, then waspcrabs to prey on the eelhawks. Now what do we do with all these waspcrabs?\"\n—Gulistan, Simic biomancer').
card_multiverse_id('species gorger'/'DGM', '369015').
card_watermark('species gorger'/'DGM', 'Simic').

card_in_set('spike jester', 'DGM').
card_original_type('spike jester'/'DGM', 'Creature — Goblin Warrior').
card_original_text('spike jester'/'DGM', 'Haste').
card_first_print('spike jester', 'DGM').
card_image_name('spike jester'/'DGM', 'spike jester').
card_uid('spike jester'/'DGM', 'DGM:Spike Jester:spike jester').
card_rarity('spike jester'/'DGM', 'Uncommon').
card_artist('spike jester'/'DGM', 'Ryan Barger').
card_number('spike jester'/'DGM', '106').
card_flavor_text('spike jester'/'DGM', 'For the Rakdos, \"suicidal charge\" and \"good showmanship\" are one and the same.').
card_multiverse_id('spike jester'/'DGM', '368972').
card_watermark('spike jester'/'DGM', 'Rakdos').

card_in_set('steeple roc', 'DGM').
card_original_type('steeple roc'/'DGM', 'Creature — Bird').
card_original_text('steeple roc'/'DGM', 'Flying, first strike').
card_first_print('steeple roc', 'DGM').
card_image_name('steeple roc'/'DGM', 'steeple roc').
card_uid('steeple roc'/'DGM', 'DGM:Steeple Roc:steeple roc').
card_rarity('steeple roc'/'DGM', 'Common').
card_artist('steeple roc'/'DGM', 'David Palumbo').
card_number('steeple roc'/'DGM', '8').
card_flavor_text('steeple roc'/'DGM', '\"Sometimes it forgets to loosen its grip before taking flight. I lost my roof that way.\"\n—Ecaban, Boros scout').
card_multiverse_id('steeple roc'/'DGM', '368992').

card_in_set('sunspire gatekeepers', 'DGM').
card_original_type('sunspire gatekeepers'/'DGM', 'Creature — Human Soldier').
card_original_text('sunspire gatekeepers'/'DGM', 'When Sunspire Gatekeepers enters the battlefield, if you control two or more Gates, put a 2/2 white Knight creature token with vigilance onto the battlefield.').
card_first_print('sunspire gatekeepers', 'DGM').
card_image_name('sunspire gatekeepers'/'DGM', 'sunspire gatekeepers').
card_uid('sunspire gatekeepers'/'DGM', 'DGM:Sunspire Gatekeepers:sunspire gatekeepers').
card_rarity('sunspire gatekeepers'/'DGM', 'Common').
card_artist('sunspire gatekeepers'/'DGM', 'Chippy').
card_number('sunspire gatekeepers'/'DGM', '9').
card_flavor_text('sunspire gatekeepers'/'DGM', '\"You will pass with respect, or you will not pass at all.\"').
card_multiverse_id('sunspire gatekeepers'/'DGM', '368949').

card_in_set('tajic, blade of the legion', 'DGM').
card_original_type('tajic, blade of the legion'/'DGM', 'Legendary Creature — Human Soldier').
card_original_text('tajic, blade of the legion'/'DGM', 'Tajic, Blade of the Legion is indestructible.\nBattalion — Whenever Tajic and at least two other creatures attack, Tajic gets +5/+5 until end of turn.').
card_first_print('tajic, blade of the legion', 'DGM').
card_image_name('tajic, blade of the legion'/'DGM', 'tajic, blade of the legion').
card_uid('tajic, blade of the legion'/'DGM', 'DGM:Tajic, Blade of the Legion:tajic, blade of the legion').
card_rarity('tajic, blade of the legion'/'DGM', 'Rare').
card_artist('tajic, blade of the legion'/'DGM', 'James Ryman').
card_number('tajic, blade of the legion'/'DGM', '107').
card_flavor_text('tajic, blade of the legion'/'DGM', '\"I run the maze alone, but I fight with the full might of the Legion.\"').
card_multiverse_id('tajic, blade of the legion'/'DGM', '369098').
card_watermark('tajic, blade of the legion'/'DGM', 'Boros').

card_in_set('take', 'DGM').
card_original_type('take'/'DGM', 'Sorcery').
card_original_text('take'/'DGM', 'Put three +1/+1 counters on target creature.\n//\nTake\n{2}{U}\nSorcery\nRemove all +1/+1 counters from target creature you control. Draw that many cards.\n//\nFuse (You may cast one or both halves of this card from your hand.)').
card_first_print('take', 'DGM').
card_image_name('take'/'DGM', 'givetake').
card_uid('take'/'DGM', 'DGM:Take:givetake').
card_rarity('take'/'DGM', 'Uncommon').
card_artist('take'/'DGM', 'Steve Prescott').
card_number('take'/'DGM', '129b').
card_multiverse_id('take'/'DGM', '369097').

card_in_set('tear', 'DGM').
card_original_type('tear'/'DGM', 'Instant').
card_original_text('tear'/'DGM', 'Destroy target artifact.\n//\nTear\n{W}\nInstant\nDestroy target enchantment.\n//\nFuse (You may cast one or both halves of this card from your hand.)').
card_first_print('tear', 'DGM').
card_image_name('tear'/'DGM', 'weartear').
card_uid('tear'/'DGM', 'DGM:Tear:weartear').
card_rarity('tear'/'DGM', 'Uncommon').
card_artist('tear'/'DGM', 'Ryan Pancoast').
card_number('tear'/'DGM', '135b').
card_multiverse_id('tear'/'DGM', '368950').

card_in_set('teysa, envoy of ghosts', 'DGM').
card_original_type('teysa, envoy of ghosts'/'DGM', 'Legendary Creature — Human Advisor').
card_original_text('teysa, envoy of ghosts'/'DGM', 'Vigilance, protection from creatures\nWhenever a creature deals combat damage to you, destroy that creature. Put a 1/1 white and black Spirit creature token with flying onto the battlefield.').
card_first_print('teysa, envoy of ghosts', 'DGM').
card_image_name('teysa, envoy of ghosts'/'DGM', 'teysa, envoy of ghosts').
card_uid('teysa, envoy of ghosts'/'DGM', 'DGM:Teysa, Envoy of Ghosts:teysa, envoy of ghosts').
card_rarity('teysa, envoy of ghosts'/'DGM', 'Rare').
card_artist('teysa, envoy of ghosts'/'DGM', 'Karla Ortiz').
card_number('teysa, envoy of ghosts'/'DGM', '108').
card_multiverse_id('teysa, envoy of ghosts'/'DGM', '369011').
card_watermark('teysa, envoy of ghosts'/'DGM', 'Orzhov').

card_in_set('thrashing mossdog', 'DGM').
card_original_type('thrashing mossdog'/'DGM', 'Creature — Plant Hound').
card_original_text('thrashing mossdog'/'DGM', 'Reach (This creature can block creatures with flying.)\nScavenge {4}{G}{G} ({4}{G}{G}, Exile this card from your graveyard: Put a number of +1/+1 counters equal to this card\'s power on target creature. Scavenge only as a sorcery.)').
card_first_print('thrashing mossdog', 'DGM').
card_image_name('thrashing mossdog'/'DGM', 'thrashing mossdog').
card_uid('thrashing mossdog'/'DGM', 'DGM:Thrashing Mossdog:thrashing mossdog').
card_rarity('thrashing mossdog'/'DGM', 'Common').
card_artist('thrashing mossdog'/'DGM', 'Ryan Barger').
card_number('thrashing mossdog'/'DGM', '50').
card_multiverse_id('thrashing mossdog'/'DGM', '369020').
card_watermark('thrashing mossdog'/'DGM', 'Golgari').

card_in_set('tithe drinker', 'DGM').
card_original_type('tithe drinker'/'DGM', 'Creature — Vampire').
card_original_text('tithe drinker'/'DGM', 'Lifelink (Damage dealt by this creature also causes you to gain that much life.)\nExtort (Whenever you cast a spell, you may pay {W/B}. If you do, each opponent loses 1 life and you gain that much life.)').
card_first_print('tithe drinker', 'DGM').
card_image_name('tithe drinker'/'DGM', 'tithe drinker').
card_uid('tithe drinker'/'DGM', 'DGM:Tithe Drinker:tithe drinker').
card_rarity('tithe drinker'/'DGM', 'Common').
card_artist('tithe drinker'/'DGM', 'Slawomir Maniak').
card_number('tithe drinker'/'DGM', '109').
card_multiverse_id('tithe drinker'/'DGM', '368970').
card_watermark('tithe drinker'/'DGM', 'Orzhov').

card_in_set('toil', 'DGM').
card_original_type('toil'/'DGM', 'Sorcery').
card_original_text('toil'/'DGM', 'Target player draws two cards and loses 2 life.\n//\nTrouble\n{2}{R}\nSorcery\nTrouble deals damage to target player equal to the number of cards in that player\'s hand.\n//\nFuse (You may cast one or both halves of this card from your hand.)').
card_first_print('toil', 'DGM').
card_image_name('toil'/'DGM', 'toiltrouble').
card_uid('toil'/'DGM', 'DGM:Toil:toiltrouble').
card_rarity('toil'/'DGM', 'Uncommon').
card_artist('toil'/'DGM', 'Nils Hamm').
card_number('toil'/'DGM', '133a').
card_multiverse_id('toil'/'DGM', '369032').

card_in_set('trait doctoring', 'DGM').
card_original_type('trait doctoring'/'DGM', 'Sorcery').
card_original_text('trait doctoring'/'DGM', 'Change the text of target permanent by replacing all instances of one color word with another or one basic land type with another until end of turn.\nCipher (Then you may exile this spell card encoded on a creature you control. Whenever that creature deals combat damage to a player, its controller may cast a copy of the encoded card without paying its mana cost.)').
card_first_print('trait doctoring', 'DGM').
card_image_name('trait doctoring'/'DGM', 'trait doctoring').
card_uid('trait doctoring'/'DGM', 'DGM:Trait Doctoring:trait doctoring').
card_rarity('trait doctoring'/'DGM', 'Rare').
card_artist('trait doctoring'/'DGM', 'Clint Cearley').
card_number('trait doctoring'/'DGM', '18').
card_multiverse_id('trait doctoring'/'DGM', '369034').
card_watermark('trait doctoring'/'DGM', 'Dimir').

card_in_set('trostani\'s summoner', 'DGM').
card_original_type('trostani\'s summoner'/'DGM', 'Creature — Elf Shaman').
card_original_text('trostani\'s summoner'/'DGM', 'When Trostani\'s Summoner enters the battlefield, put a 2/2 white Knight creature token with vigilance, a 3/3 green Centaur creature token, and a 4/4 green Rhino creature token with trample onto the battlefield.').
card_image_name('trostani\'s summoner'/'DGM', 'trostani\'s summoner').
card_uid('trostani\'s summoner'/'DGM', 'DGM:Trostani\'s Summoner:trostani\'s summoner').
card_rarity('trostani\'s summoner'/'DGM', 'Uncommon').
card_artist('trostani\'s summoner'/'DGM', 'Howard Lyon').
card_number('trostani\'s summoner'/'DGM', '110').
card_multiverse_id('trostani\'s summoner'/'DGM', '369072').
card_watermark('trostani\'s summoner'/'DGM', 'Selesnya').

card_in_set('trouble', 'DGM').
card_original_type('trouble'/'DGM', 'Sorcery').
card_original_text('trouble'/'DGM', 'Target player draws two cards and loses 2 life.\n//\nTrouble\n{2}{R}\nSorcery\nTrouble deals damage to target player equal to the number of cards in that player\'s hand.\n//\nFuse (You may cast one or both halves of this card from your hand.)').
card_first_print('trouble', 'DGM').
card_image_name('trouble'/'DGM', 'toiltrouble').
card_uid('trouble'/'DGM', 'DGM:Trouble:toiltrouble').
card_rarity('trouble'/'DGM', 'Uncommon').
card_artist('trouble'/'DGM', 'Nils Hamm').
card_number('trouble'/'DGM', '133b').
card_multiverse_id('trouble'/'DGM', '369032').

card_in_set('turn', 'DGM').
card_original_type('turn'/'DGM', 'Instant').
card_original_text('turn'/'DGM', 'Target creature loses all abilities and becomes a 0/1 red Weird until end of turn.\n//\nBurn\n{1}{R}\nInstant\nBurn deals 2 damage to target creature or player.\n//\nFuse (You may cast one or both halves of this card from your hand.)').
card_first_print('turn', 'DGM').
card_image_name('turn'/'DGM', 'turnburn').
card_uid('turn'/'DGM', 'DGM:Turn:turnburn').
card_rarity('turn'/'DGM', 'Uncommon').
card_artist('turn'/'DGM', 'Ryan Barger').
card_number('turn'/'DGM', '134a').
card_multiverse_id('turn'/'DGM', '369080').

card_in_set('ubul sar gatekeepers', 'DGM').
card_original_type('ubul sar gatekeepers'/'DGM', 'Creature — Zombie Soldier').
card_original_text('ubul sar gatekeepers'/'DGM', 'When Ubul Sar Gatekeepers enters the battlefield, if you control two or more Gates, target creature an opponent controls gets -2/-2 until end of turn.').
card_first_print('ubul sar gatekeepers', 'DGM').
card_image_name('ubul sar gatekeepers'/'DGM', 'ubul sar gatekeepers').
card_uid('ubul sar gatekeepers'/'DGM', 'DGM:Ubul Sar Gatekeepers:ubul sar gatekeepers').
card_rarity('ubul sar gatekeepers'/'DGM', 'Common').
card_artist('ubul sar gatekeepers'/'DGM', 'Volkan Baga').
card_number('ubul sar gatekeepers'/'DGM', '30').
card_flavor_text('ubul sar gatekeepers'/'DGM', 'There are many gates into the undercity, but few ways out.').
card_multiverse_id('ubul sar gatekeepers'/'DGM', '369046').

card_in_set('uncovered clues', 'DGM').
card_original_type('uncovered clues'/'DGM', 'Sorcery').
card_original_text('uncovered clues'/'DGM', 'Look at the top four cards of your library. You may reveal up to two instant and/or sorcery cards from among them and put the revealed cards into your hand. Put the rest on the bottom of your library in any order.').
card_first_print('uncovered clues', 'DGM').
card_image_name('uncovered clues'/'DGM', 'uncovered clues').
card_uid('uncovered clues'/'DGM', 'DGM:Uncovered Clues:uncovered clues').
card_rarity('uncovered clues'/'DGM', 'Common').
card_artist('uncovered clues'/'DGM', 'Jaime Jones').
card_number('uncovered clues'/'DGM', '19').
card_flavor_text('uncovered clues'/'DGM', 'The Implicit Maze winds through Ravnica, connecting arcane landmarks of all ten guilds.').
card_multiverse_id('uncovered clues'/'DGM', '369010').

card_in_set('unflinching courage', 'DGM').
card_original_type('unflinching courage'/'DGM', 'Enchantment — Aura').
card_original_text('unflinching courage'/'DGM', 'Enchant creature\nEnchanted creature gets +2/+2 and has trample and lifelink.').
card_first_print('unflinching courage', 'DGM').
card_image_name('unflinching courage'/'DGM', 'unflinching courage').
card_uid('unflinching courage'/'DGM', 'DGM:Unflinching Courage:unflinching courage').
card_rarity('unflinching courage'/'DGM', 'Uncommon').
card_artist('unflinching courage'/'DGM', 'Mike Bierek').
card_number('unflinching courage'/'DGM', '111').
card_flavor_text('unflinching courage'/'DGM', 'As Trostani\'s influence grew, vitality spread through the Conclave—much to the other guilds\' dismay.').
card_multiverse_id('unflinching courage'/'DGM', '369074').
card_watermark('unflinching courage'/'DGM', 'Selesnya').

card_in_set('varolz, the scar-striped', 'DGM').
card_original_type('varolz, the scar-striped'/'DGM', 'Legendary Creature — Troll Warrior').
card_original_text('varolz, the scar-striped'/'DGM', 'Each creature card in your graveyard has scavenge. The scavenge cost is equal to its mana cost. (Exile a creature card from your graveyard and pay its mana cost: Put a number of +1/+1 counters equal to that card\'s power on target creature. Scavenge only as a sorcery.)\nSacrifice another creature: Regenerate Varolz, the Scar-Striped.').
card_first_print('varolz, the scar-striped', 'DGM').
card_image_name('varolz, the scar-striped'/'DGM', 'varolz, the scar-striped').
card_uid('varolz, the scar-striped'/'DGM', 'DGM:Varolz, the Scar-Striped:varolz, the scar-striped').
card_rarity('varolz, the scar-striped'/'DGM', 'Rare').
card_artist('varolz, the scar-striped'/'DGM', 'Adam Paquette').
card_number('varolz, the scar-striped'/'DGM', '112').
card_multiverse_id('varolz, the scar-striped'/'DGM', '368977').
card_watermark('varolz, the scar-striped'/'DGM', 'Golgari').

card_in_set('viashino firstblade', 'DGM').
card_original_type('viashino firstblade'/'DGM', 'Creature — Viashino Soldier').
card_original_text('viashino firstblade'/'DGM', 'Haste\nWhen Viashino Firstblade enters the battlefield, it gets +2/+2 until end of turn.').
card_first_print('viashino firstblade', 'DGM').
card_image_name('viashino firstblade'/'DGM', 'viashino firstblade').
card_uid('viashino firstblade'/'DGM', 'DGM:Viashino Firstblade:viashino firstblade').
card_rarity('viashino firstblade'/'DGM', 'Common').
card_artist('viashino firstblade'/'DGM', 'Matt Stewart').
card_number('viashino firstblade'/'DGM', '113').
card_flavor_text('viashino firstblade'/'DGM', '\"All that steel doesn\'t make them slower. It makes them bolder.\"\n—Tajic, Blade of the Legion').
card_multiverse_id('viashino firstblade'/'DGM', '369070').
card_watermark('viashino firstblade'/'DGM', 'Boros').

card_in_set('voice of resurgence', 'DGM').
card_original_type('voice of resurgence'/'DGM', 'Creature — Elemental').
card_original_text('voice of resurgence'/'DGM', 'Whenever an opponent casts a spell during your turn or when Voice of Resurgence dies, put a green and white Elemental creature token onto the battlefield with \"This creature\'s power and toughness are each equal to the number of creatures you control.\"').
card_first_print('voice of resurgence', 'DGM').
card_image_name('voice of resurgence'/'DGM', 'voice of resurgence').
card_uid('voice of resurgence'/'DGM', 'DGM:Voice of Resurgence:voice of resurgence').
card_rarity('voice of resurgence'/'DGM', 'Mythic Rare').
card_artist('voice of resurgence'/'DGM', 'Winona Nelson').
card_number('voice of resurgence'/'DGM', '114').
card_multiverse_id('voice of resurgence'/'DGM', '368951').
card_watermark('voice of resurgence'/'DGM', 'Selesnya').

card_in_set('vorel of the hull clade', 'DGM').
card_original_type('vorel of the hull clade'/'DGM', 'Legendary Creature — Human Merfolk').
card_original_text('vorel of the hull clade'/'DGM', '{G}{U}, {T}: For each counter on target artifact, creature, or land, put another of those counters on that permanent.').
card_first_print('vorel of the hull clade', 'DGM').
card_image_name('vorel of the hull clade'/'DGM', 'vorel of the hull clade').
card_uid('vorel of the hull clade'/'DGM', 'DGM:Vorel of the Hull Clade:vorel of the hull clade').
card_rarity('vorel of the hull clade'/'DGM', 'Rare').
card_artist('vorel of the hull clade'/'DGM', 'Mike Bierek').
card_number('vorel of the hull clade'/'DGM', '115').
card_flavor_text('vorel of the hull clade'/'DGM', '\"I used to hurl rocks and eat scraps of meat burned over a fire. Look at what I\'ve become and tell me Simic does not hold infinite possibility.\"').
card_multiverse_id('vorel of the hull clade'/'DGM', '369045').
card_watermark('vorel of the hull clade'/'DGM', 'Simic').

card_in_set('wake the reflections', 'DGM').
card_original_type('wake the reflections'/'DGM', 'Sorcery').
card_original_text('wake the reflections'/'DGM', 'Populate. (Put a token onto the battlefield that\'s a copy of a creature token you control.)').
card_first_print('wake the reflections', 'DGM').
card_image_name('wake the reflections'/'DGM', 'wake the reflections').
card_uid('wake the reflections'/'DGM', 'DGM:Wake the Reflections:wake the reflections').
card_rarity('wake the reflections'/'DGM', 'Common').
card_artist('wake the reflections'/'DGM', 'Cynthia Sheppard').
card_number('wake the reflections'/'DGM', '10').
card_flavor_text('wake the reflections'/'DGM', '\"You see a bird, an oak, a packbeast. I see the potential for an army.\"\n—Lalia, Selesnya dryad').
card_multiverse_id('wake the reflections'/'DGM', '369012').
card_watermark('wake the reflections'/'DGM', 'Selesnya').

card_in_set('warleader\'s helix', 'DGM').
card_original_type('warleader\'s helix'/'DGM', 'Instant').
card_original_text('warleader\'s helix'/'DGM', 'Warleader\'s Helix deals 4 damage to target creature or player and you gain 4 life.').
card_image_name('warleader\'s helix'/'DGM', 'warleader\'s helix').
card_uid('warleader\'s helix'/'DGM', 'DGM:Warleader\'s Helix:warleader\'s helix').
card_rarity('warleader\'s helix'/'DGM', 'Uncommon').
card_artist('warleader\'s helix'/'DGM', 'Greg Staples').
card_number('warleader\'s helix'/'DGM', '116').
card_flavor_text('warleader\'s helix'/'DGM', '\"There is no time to remedy our enemies\' ignorance. Blast it out of them.\"\n—Aurelia').
card_multiverse_id('warleader\'s helix'/'DGM', '369081').
card_watermark('warleader\'s helix'/'DGM', 'Boros').

card_in_set('warped physique', 'DGM').
card_original_type('warped physique'/'DGM', 'Instant').
card_original_text('warped physique'/'DGM', 'Target creature gets +X/-X until end of turn, where X is the number of cards in your hand.').
card_first_print('warped physique', 'DGM').
card_image_name('warped physique'/'DGM', 'warped physique').
card_uid('warped physique'/'DGM', 'DGM:Warped Physique:warped physique').
card_rarity('warped physique'/'DGM', 'Uncommon').
card_artist('warped physique'/'DGM', 'Karl Kopinski').
card_number('warped physique'/'DGM', '117').
card_flavor_text('warped physique'/'DGM', 'The Gruul seek unbounded strength. The Dimir deliver cruel irony.').
card_multiverse_id('warped physique'/'DGM', '369094').
card_watermark('warped physique'/'DGM', 'Dimir').

card_in_set('weapon surge', 'DGM').
card_original_type('weapon surge'/'DGM', 'Instant').
card_original_text('weapon surge'/'DGM', 'Target creature you control gets +1/+0 and gains first strike until end of turn.\nOverload {1}{R} (You may cast this spell for its overload cost. If you do, change its text by replacing all instances of \"target\" with \"each.\")').
card_first_print('weapon surge', 'DGM').
card_image_name('weapon surge'/'DGM', 'weapon surge').
card_uid('weapon surge'/'DGM', 'DGM:Weapon Surge:weapon surge').
card_rarity('weapon surge'/'DGM', 'Common').
card_artist('weapon surge'/'DGM', 'Jason Felix').
card_number('weapon surge'/'DGM', '40').
card_multiverse_id('weapon surge'/'DGM', '369004').
card_watermark('weapon surge'/'DGM', 'Izzet').

card_in_set('wear', 'DGM').
card_original_type('wear'/'DGM', 'Instant').
card_original_text('wear'/'DGM', 'Destroy target artifact.\n//\nTear\n{W}\nInstant\nDestroy target enchantment.\n//\nFuse (You may cast one or both halves of this card from your hand.)').
card_first_print('wear', 'DGM').
card_image_name('wear'/'DGM', 'weartear').
card_uid('wear'/'DGM', 'DGM:Wear:weartear').
card_rarity('wear'/'DGM', 'Uncommon').
card_artist('wear'/'DGM', 'Ryan Pancoast').
card_number('wear'/'DGM', '135a').
card_multiverse_id('wear'/'DGM', '368950').

card_in_set('well', 'DGM').
card_original_type('well'/'DGM', 'Sorcery').
card_original_text('well'/'DGM', 'Put a 3/3 green Centaur creature token onto the battlefield.\n//\nWell\n{W}\nSorcery\nYou gain 2 life for each creature you control.\n//\nFuse (You may cast one or both halves of this card from your hand.)').
card_first_print('well', 'DGM').
card_image_name('well'/'DGM', 'alivewell').
card_uid('well'/'DGM', 'DGM:Well:alivewell').
card_rarity('well'/'DGM', 'Uncommon').
card_artist('well'/'DGM', 'Nils Hamm').
card_number('well'/'DGM', '121b').
card_multiverse_id('well'/'DGM', '369041').

card_in_set('willing', 'DGM').
card_original_type('willing'/'DGM', 'Instant').
card_original_text('willing'/'DGM', 'Creatures you control are indestructible this turn. Untap each creature you control.\n//\nWilling\n{1}{W}{B}\nInstant\nCreatures you control gain deathtouch and lifelink until end of turn.\n//\nFuse (You may cast one or both halves of this card from your hand.)').
card_first_print('willing', 'DGM').
card_image_name('willing'/'DGM', 'readywilling').
card_uid('willing'/'DGM', 'DGM:Willing:readywilling').
card_rarity('willing'/'DGM', 'Rare').
card_artist('willing'/'DGM', 'Zoltan Boros').
card_number('willing'/'DGM', '132b').
card_multiverse_id('willing'/'DGM', '368967').

card_in_set('wind drake', 'DGM').
card_original_type('wind drake'/'DGM', 'Creature — Drake').
card_original_text('wind drake'/'DGM', 'Flying').
card_image_name('wind drake'/'DGM', 'wind drake').
card_uid('wind drake'/'DGM', 'DGM:Wind Drake:wind drake').
card_rarity('wind drake'/'DGM', 'Common').
card_artist('wind drake'/'DGM', 'John Severin Brassell').
card_number('wind drake'/'DGM', '20').
card_flavor_text('wind drake'/'DGM', 'Air currents whip through the city corridors, creating wind tunnels that accelerate urban drakes toward their prey.').
card_multiverse_id('wind drake'/'DGM', '369037').

card_in_set('woodlot crawler', 'DGM').
card_original_type('woodlot crawler'/'DGM', 'Creature — Insect').
card_original_text('woodlot crawler'/'DGM', 'Forestwalk, protection from green').
card_first_print('woodlot crawler', 'DGM').
card_image_name('woodlot crawler'/'DGM', 'woodlot crawler').
card_uid('woodlot crawler'/'DGM', 'DGM:Woodlot Crawler:woodlot crawler').
card_rarity('woodlot crawler'/'DGM', 'Uncommon').
card_artist('woodlot crawler'/'DGM', 'Greg Staples').
card_number('woodlot crawler'/'DGM', '118').
card_flavor_text('woodlot crawler'/'DGM', '\"We learned something from the Simic. Unfortunately for them.\"\n—Nefara, Dimir agent').
card_multiverse_id('woodlot crawler'/'DGM', '369027').
card_watermark('woodlot crawler'/'DGM', 'Dimir').

card_in_set('zhur-taa ancient', 'DGM').
card_original_type('zhur-taa ancient'/'DGM', 'Creature — Beast').
card_original_text('zhur-taa ancient'/'DGM', 'Whenever a player taps a land for mana, that player adds one mana to his or her mana pool of any type that land produced.').
card_first_print('zhur-taa ancient', 'DGM').
card_image_name('zhur-taa ancient'/'DGM', 'zhur-taa ancient').
card_uid('zhur-taa ancient'/'DGM', 'DGM:Zhur-Taa Ancient:zhur-taa ancient').
card_rarity('zhur-taa ancient'/'DGM', 'Rare').
card_artist('zhur-taa ancient'/'DGM', 'Adam Paquette').
card_number('zhur-taa ancient'/'DGM', '119').
card_flavor_text('zhur-taa ancient'/'DGM', 'Many Ravnicans think of nature as orderly groves and elf-tended rot farms. They haven\'t seen the Gruul wilds.').
card_multiverse_id('zhur-taa ancient'/'DGM', '369019').
card_watermark('zhur-taa ancient'/'DGM', 'Gruul').

card_in_set('zhur-taa druid', 'DGM').
card_original_type('zhur-taa druid'/'DGM', 'Creature — Human Druid').
card_original_text('zhur-taa druid'/'DGM', '{T}: Add {G} to your mana pool.\nWhenever you tap Zhur-Taa Druid for mana, it deals 1 damage to each opponent.').
card_first_print('zhur-taa druid', 'DGM').
card_image_name('zhur-taa druid'/'DGM', 'zhur-taa druid').
card_uid('zhur-taa druid'/'DGM', 'DGM:Zhur-Taa Druid:zhur-taa druid').
card_rarity('zhur-taa druid'/'DGM', 'Common').
card_artist('zhur-taa druid'/'DGM', 'Mark Winters').
card_number('zhur-taa druid'/'DGM', '120').
card_flavor_text('zhur-taa druid'/'DGM', '\"Only the decadent think magic should be pristine and without cost.\"').
card_multiverse_id('zhur-taa druid'/'DGM', '369065').
card_watermark('zhur-taa druid'/'DGM', 'Gruul').
