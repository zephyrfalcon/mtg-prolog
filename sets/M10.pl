% Magic 2010

set('M10').
set_name('M10', 'Magic 2010').
set_release_date('M10', '2009-07-17').
set_border('M10', 'black').
set_type('M10', 'core').

card_in_set('acidic slime', 'M10').
card_original_type('acidic slime'/'M10', 'Creature — Ooze').
card_original_text('acidic slime'/'M10', 'Deathtouch (Creatures dealt damage by this creature are destroyed. You can divide this creature\'s combat damage among any of the creatures blocking or blocked by it.)\nWhen Acidic Slime enters the battlefield, destroy target artifact, enchantment, or land.').
card_image_name('acidic slime'/'M10', 'acidic slime').
card_uid('acidic slime'/'M10', 'M10:Acidic Slime:acidic slime').
card_rarity('acidic slime'/'M10', 'Uncommon').
card_artist('acidic slime'/'M10', 'Karl Kopinski').
card_number('acidic slime'/'M10', '165').
card_multiverse_id('acidic slime'/'M10', '189880').

card_in_set('acolyte of xathrid', 'M10').
card_original_type('acolyte of xathrid'/'M10', 'Creature — Human Cleric').
card_original_text('acolyte of xathrid'/'M10', '{1}{B}, {T}: Target player loses 1 life.').
card_first_print('acolyte of xathrid', 'M10').
card_image_name('acolyte of xathrid'/'M10', 'acolyte of xathrid').
card_uid('acolyte of xathrid'/'M10', 'M10:Acolyte of Xathrid:acolyte of xathrid').
card_rarity('acolyte of xathrid'/'M10', 'Common').
card_artist('acolyte of xathrid'/'M10', 'Chris Rahn').
card_number('acolyte of xathrid'/'M10', '83').
card_flavor_text('acolyte of xathrid'/'M10', 'Like all priests, she gives her blessings only to those truly worthy of them.').
card_multiverse_id('acolyte of xathrid'/'M10', '190579').

card_in_set('act of treason', 'M10').
card_original_type('act of treason'/'M10', 'Sorcery').
card_original_text('act of treason'/'M10', 'Gain control of target creature until end of turn. Untap that creature. It gains haste until end of turn. (It can attack and {T} this turn.)').
card_first_print('act of treason', 'M10').
card_image_name('act of treason'/'M10', 'act of treason').
card_uid('act of treason'/'M10', 'M10:Act of Treason:act of treason').
card_rarity('act of treason'/'M10', 'Uncommon').
card_artist('act of treason'/'M10', 'Eric Deschamps').
card_number('act of treason'/'M10', '124').
card_flavor_text('act of treason'/'M10', '\"Rage courses in every heart, yearning to betray its rational prison.\"\n—Sarkhan Vol').
card_multiverse_id('act of treason'/'M10', '191082').

card_in_set('air elemental', 'M10').
card_original_type('air elemental'/'M10', 'Creature — Elemental').
card_original_text('air elemental'/'M10', 'Flying').
card_image_name('air elemental'/'M10', 'air elemental').
card_uid('air elemental'/'M10', 'M10:Air Elemental:air elemental').
card_rarity('air elemental'/'M10', 'Uncommon').
card_artist('air elemental'/'M10', 'Kev Walker').
card_number('air elemental'/'M10', '42').
card_flavor_text('air elemental'/'M10', '\"The East Wind, an interloper in the dominions of Westerly Weather, is an impassive-faced tyrant with a sharp poniard held behind his back for a treacherous stab.\"\n—Joseph Conrad, The Mirror of the Sea').
card_multiverse_id('air elemental'/'M10', '189884').

card_in_set('ajani goldmane', 'M10').
card_original_type('ajani goldmane'/'M10', 'Planeswalker — Ajani').
card_original_text('ajani goldmane'/'M10', '+1: You gain 2 life.\n-1: Put a +1/+1 counter on each creature you control. Those creatures gain vigilance until end of turn.\n-6: Put a white Avatar creature token onto the battlefield with \"This creature\'s power and toughness are each equal to your life total.\"').
card_image_name('ajani goldmane'/'M10', 'ajani goldmane').
card_uid('ajani goldmane'/'M10', 'M10:Ajani Goldmane:ajani goldmane').
card_rarity('ajani goldmane'/'M10', 'Mythic Rare').
card_artist('ajani goldmane'/'M10', 'Aleksi Briclot').
card_number('ajani goldmane'/'M10', '1').
card_multiverse_id('ajani goldmane'/'M10', '191239').

card_in_set('alluring siren', 'M10').
card_original_type('alluring siren'/'M10', 'Creature — Siren').
card_original_text('alluring siren'/'M10', '{T}: Target creature an opponent controls attacks you this turn if able.').
card_first_print('alluring siren', 'M10').
card_image_name('alluring siren'/'M10', 'alluring siren').
card_uid('alluring siren'/'M10', 'M10:Alluring Siren:alluring siren').
card_rarity('alluring siren'/'M10', 'Uncommon').
card_artist('alluring siren'/'M10', 'Chippy').
card_number('alluring siren'/'M10', '43').
card_flavor_text('alluring siren'/'M10', '\"The ground polluted floats with human gore,\nAnd human carnage taints the dreadful shore\nFly swift the dangerous coast: let every ear\nBe stopp\'d against the song! \'tis death to hear!\"\n—Homer, The Odyssey, trans. Pope').
card_multiverse_id('alluring siren'/'M10', '189894').

card_in_set('angel\'s feather', 'M10').
card_original_type('angel\'s feather'/'M10', 'Artifact').
card_original_text('angel\'s feather'/'M10', 'Whenever a player casts a white spell, you may gain 1 life.').
card_image_name('angel\'s feather'/'M10', 'angel\'s feather').
card_uid('angel\'s feather'/'M10', 'M10:Angel\'s Feather:angel\'s feather').
card_rarity('angel\'s feather'/'M10', 'Uncommon').
card_artist('angel\'s feather'/'M10', 'Alan Pollack').
card_number('angel\'s feather'/'M10', '206').
card_flavor_text('angel\'s feather'/'M10', 'If taken, it cuts the hand that clutches it. If given, it heals the hand that holds it.').
card_multiverse_id('angel\'s feather'/'M10', '191245').

card_in_set('angel\'s mercy', 'M10').
card_original_type('angel\'s mercy'/'M10', 'Instant').
card_original_text('angel\'s mercy'/'M10', 'You gain 7 life.').
card_first_print('angel\'s mercy', 'M10').
card_image_name('angel\'s mercy'/'M10', 'angel\'s mercy').
card_uid('angel\'s mercy'/'M10', 'M10:Angel\'s Mercy:angel\'s mercy').
card_rarity('angel\'s mercy'/'M10', 'Common').
card_artist('angel\'s mercy'/'M10', 'Andrew Robinson').
card_number('angel\'s mercy'/'M10', '2').
card_flavor_text('angel\'s mercy'/'M10', '\"Until that day I cursed the angels, those uncaring lights in the sky. They rewarded my scorn with the gift of time, every year of which I\'ve spent in devotion to their names.\"\n—Torian Sha, soul warden').
card_multiverse_id('angel\'s mercy'/'M10', '190549').

card_in_set('ant queen', 'M10').
card_original_type('ant queen'/'M10', 'Creature — Insect').
card_original_text('ant queen'/'M10', '{1}{G}: Put a 1/1 green Insect creature token onto the battlefield.').
card_image_name('ant queen'/'M10', 'ant queen').
card_uid('ant queen'/'M10', 'M10:Ant Queen:ant queen').
card_rarity('ant queen'/'M10', 'Rare').
card_artist('ant queen'/'M10', 'Trevor Claxton').
card_number('ant queen'/'M10', '166').
card_flavor_text('ant queen'/'M10', '\"Kill the queen first, or we\'ll be fighting her drones forever. It is not in a queen\'s nature to have enough servants.\"\n—Borzard, exterminator captain').
card_multiverse_id('ant queen'/'M10', '191063').

card_in_set('armored ascension', 'M10').
card_original_type('armored ascension'/'M10', 'Enchantment — Aura').
card_original_text('armored ascension'/'M10', 'Enchant creature\nEnchanted creature gets +1/+1 for each Plains you control and has flying.').
card_image_name('armored ascension'/'M10', 'armored ascension').
card_uid('armored ascension'/'M10', 'M10:Armored Ascension:armored ascension').
card_rarity('armored ascension'/'M10', 'Uncommon').
card_artist('armored ascension'/'M10', 'Jesper Ejsing').
card_number('armored ascension'/'M10', '3').
card_flavor_text('armored ascension'/'M10', '\"The meadow sprang up beneath me, and I perceived a path from our people\'s defeat to our highest glory.\"').
card_multiverse_id('armored ascension'/'M10', '193738').

card_in_set('assassinate', 'M10').
card_original_type('assassinate'/'M10', 'Sorcery').
card_original_text('assassinate'/'M10', 'Destroy target tapped creature.').
card_image_name('assassinate'/'M10', 'assassinate').
card_uid('assassinate'/'M10', 'M10:Assassinate:assassinate').
card_rarity('assassinate'/'M10', 'Common').
card_artist('assassinate'/'M10', 'Kev Walker').
card_number('assassinate'/'M10', '84').
card_flavor_text('assassinate'/'M10', '\"This is how wars are won—not with armies of soldiers but with a single knife blade, artfully placed.\"\n—Yurin, royal assassin').
card_multiverse_id('assassinate'/'M10', '193745').

card_in_set('awakener druid', 'M10').
card_original_type('awakener druid'/'M10', 'Creature — Human Druid').
card_original_text('awakener druid'/'M10', 'When Awakener Druid enters the battlefield, target Forest becomes a 4/5 green Treefolk creature as long as Awakener Druid is on the battlefield. It\'s still a land.').
card_first_print('awakener druid', 'M10').
card_image_name('awakener druid'/'M10', 'awakener druid').
card_uid('awakener druid'/'M10', 'M10:Awakener Druid:awakener druid').
card_rarity('awakener druid'/'M10', 'Uncommon').
card_artist('awakener druid'/'M10', 'Jason Chan').
card_number('awakener druid'/'M10', '167').
card_flavor_text('awakener druid'/'M10', 'The druids of Mazar treat each seedcone with respect for the warrior it will become.').
card_multiverse_id('awakener druid'/'M10', '189907').

card_in_set('ball lightning', 'M10').
card_original_type('ball lightning'/'M10', 'Creature — Elemental').
card_original_text('ball lightning'/'M10', 'Trample (If this creature would deal enough damage to its blockers to destroy them, you may have it deal the rest of its damage to defending player or planeswalker.)\nHaste (This creature can attack and {T} as soon as it comes under your control.)\nAt the beginning of the end step, sacrifice Ball Lightning.').
card_image_name('ball lightning'/'M10', 'ball lightning').
card_uid('ball lightning'/'M10', 'M10:Ball Lightning:ball lightning').
card_rarity('ball lightning'/'M10', 'Rare').
card_artist('ball lightning'/'M10', 'Trevor Claxton').
card_number('ball lightning'/'M10', '125').
card_multiverse_id('ball lightning'/'M10', '191393').

card_in_set('baneslayer angel', 'M10').
card_original_type('baneslayer angel'/'M10', 'Creature — Angel').
card_original_text('baneslayer angel'/'M10', 'Flying, first strike, lifelink, protection from Demons and from Dragons').
card_first_print('baneslayer angel', 'M10').
card_image_name('baneslayer angel'/'M10', 'baneslayer angel').
card_uid('baneslayer angel'/'M10', 'M10:Baneslayer Angel:baneslayer angel').
card_rarity('baneslayer angel'/'M10', 'Mythic Rare').
card_artist('baneslayer angel'/'M10', 'Greg Staples').
card_number('baneslayer angel'/'M10', '4').
card_flavor_text('baneslayer angel'/'M10', 'Some angels protect the meek and innocent. Others seek out and smite evil wherever it lurks.').
card_multiverse_id('baneslayer angel'/'M10', '191065').

card_in_set('berserkers of blood ridge', 'M10').
card_original_type('berserkers of blood ridge'/'M10', 'Creature — Human Berserker').
card_original_text('berserkers of blood ridge'/'M10', 'Berserkers of Blood Ridge attacks each turn if able.').
card_first_print('berserkers of blood ridge', 'M10').
card_image_name('berserkers of blood ridge'/'M10', 'berserkers of blood ridge').
card_uid('berserkers of blood ridge'/'M10', 'M10:Berserkers of Blood Ridge:berserkers of blood ridge').
card_rarity('berserkers of blood ridge'/'M10', 'Common').
card_artist('berserkers of blood ridge'/'M10', 'Karl Kopinski').
card_number('berserkers of blood ridge'/'M10', '126').
card_flavor_text('berserkers of blood ridge'/'M10', 'They take a blood oath to die in battle. So far none have failed to fulfill the promise.').
card_multiverse_id('berserkers of blood ridge'/'M10', '191071').

card_in_set('birds of paradise', 'M10').
card_original_type('birds of paradise'/'M10', 'Creature — Bird').
card_original_text('birds of paradise'/'M10', 'Flying\n{T}: Add one mana of any color to your mana pool.').
card_image_name('birds of paradise'/'M10', 'birds of paradise').
card_uid('birds of paradise'/'M10', 'M10:Birds of Paradise:birds of paradise').
card_rarity('birds of paradise'/'M10', 'Rare').
card_artist('birds of paradise'/'M10', 'Marcelo Vignali').
card_number('birds of paradise'/'M10', '168').
card_flavor_text('birds of paradise'/'M10', 'Each feather in its tail is a souvenir of an exotic land, a memory condensed in hue.').
card_multiverse_id('birds of paradise'/'M10', '191080').

card_in_set('black knight', 'M10').
card_original_type('black knight'/'M10', 'Creature — Human Knight').
card_original_text('black knight'/'M10', 'First strike (This creature deals combat damage before creatures without first strike.)\nProtection from white (This creature can\'t be blocked, targeted, dealt damage, or enchanted by anything white.)').
card_image_name('black knight'/'M10', 'black knight').
card_uid('black knight'/'M10', 'M10:Black Knight:black knight').
card_rarity('black knight'/'M10', 'Uncommon').
card_artist('black knight'/'M10', 'Christopher Moeller').
card_number('black knight'/'M10', '85').
card_multiverse_id('black knight'/'M10', '190540').

card_in_set('blinding mage', 'M10').
card_original_type('blinding mage'/'M10', 'Creature — Human Wizard').
card_original_text('blinding mage'/'M10', '{W}, {T}: Tap target creature.').
card_first_print('blinding mage', 'M10').
card_image_name('blinding mage'/'M10', 'blinding mage').
card_uid('blinding mage'/'M10', 'M10:Blinding Mage:blinding mage').
card_rarity('blinding mage'/'M10', 'Common').
card_artist('blinding mage'/'M10', 'Eric Deschamps').
card_number('blinding mage'/'M10', '5').
card_flavor_text('blinding mage'/'M10', '\"I carry the light of truth. Do not pity those it blinds, for they never had eyes to see.\"').
card_multiverse_id('blinding mage'/'M10', '190195').

card_in_set('bog wraith', 'M10').
card_original_type('bog wraith'/'M10', 'Creature — Wraith').
card_original_text('bog wraith'/'M10', 'Swampwalk (This creature is unblockable as long as defending player controls a Swamp.)').
card_image_name('bog wraith'/'M10', 'bog wraith').
card_uid('bog wraith'/'M10', 'M10:Bog Wraith:bog wraith').
card_rarity('bog wraith'/'M10', 'Uncommon').
card_artist('bog wraith'/'M10', 'Daarken').
card_number('bog wraith'/'M10', '86').
card_flavor_text('bog wraith'/'M10', 'She died in this swamp, and so will you.').
card_multiverse_id('bog wraith'/'M10', '190165').

card_in_set('bogardan hellkite', 'M10').
card_original_type('bogardan hellkite'/'M10', 'Creature — Dragon').
card_original_text('bogardan hellkite'/'M10', 'Flash (You may cast this spell any time you could cast an instant.)\nFlying\nWhen Bogardan Hellkite enters the battlefield, it deals 5 damage divided as you choose among any number of target creatures and/or players.').
card_image_name('bogardan hellkite'/'M10', 'bogardan hellkite').
card_uid('bogardan hellkite'/'M10', 'M10:Bogardan Hellkite:bogardan hellkite').
card_rarity('bogardan hellkite'/'M10', 'Mythic Rare').
card_artist('bogardan hellkite'/'M10', 'Scott M. Fischer').
card_number('bogardan hellkite'/'M10', '127').
card_multiverse_id('bogardan hellkite'/'M10', '191340').

card_in_set('borderland ranger', 'M10').
card_original_type('borderland ranger'/'M10', 'Creature — Human Scout').
card_original_text('borderland ranger'/'M10', 'When Borderland Ranger enters the battlefield, you may search your library for a basic land card, reveal it, and put it into your hand. If you do, shuffle your library.').
card_first_print('borderland ranger', 'M10').
card_image_name('borderland ranger'/'M10', 'borderland ranger').
card_uid('borderland ranger'/'M10', 'M10:Borderland Ranger:borderland ranger').
card_rarity('borderland ranger'/'M10', 'Common').
card_artist('borderland ranger'/'M10', 'Jesper Ejsing').
card_number('borderland ranger'/'M10', '169').
card_flavor_text('borderland ranger'/'M10', '\"Only fools and bandits use roads.\"').
card_multiverse_id('borderland ranger'/'M10', '189905').

card_in_set('bountiful harvest', 'M10').
card_original_type('bountiful harvest'/'M10', 'Sorcery').
card_original_text('bountiful harvest'/'M10', 'You gain 1 life for each land you control.').
card_first_print('bountiful harvest', 'M10').
card_image_name('bountiful harvest'/'M10', 'bountiful harvest').
card_uid('bountiful harvest'/'M10', 'M10:Bountiful Harvest:bountiful harvest').
card_rarity('bountiful harvest'/'M10', 'Common').
card_artist('bountiful harvest'/'M10', 'Jason Chan').
card_number('bountiful harvest'/'M10', '170').
card_flavor_text('bountiful harvest'/'M10', 'Following the sun\'s cycle, harvest season comes once a year—unless the druids bid the sun behave otherwise.').
card_multiverse_id('bountiful harvest'/'M10', '189913').

card_in_set('bramble creeper', 'M10').
card_original_type('bramble creeper'/'M10', 'Creature — Elemental').
card_original_text('bramble creeper'/'M10', 'Whenever Bramble Creeper attacks, it gets +5/+0 until end of turn.').
card_first_print('bramble creeper', 'M10').
card_image_name('bramble creeper'/'M10', 'bramble creeper').
card_uid('bramble creeper'/'M10', 'M10:Bramble Creeper:bramble creeper').
card_rarity('bramble creeper'/'M10', 'Common').
card_artist('bramble creeper'/'M10', 'Carl Critchlow').
card_number('bramble creeper'/'M10', '171').
card_flavor_text('bramble creeper'/'M10', '\"I\'ve seen the fully grown ones tear a baloth to gory ribbons in seconds.\"\n—Kivi, thornberry harvester').
card_multiverse_id('bramble creeper'/'M10', '189915').

card_in_set('burning inquiry', 'M10').
card_original_type('burning inquiry'/'M10', 'Sorcery').
card_original_text('burning inquiry'/'M10', 'Each player draws three cards, then discards three cards at random.').
card_first_print('burning inquiry', 'M10').
card_image_name('burning inquiry'/'M10', 'burning inquiry').
card_uid('burning inquiry'/'M10', 'M10:Burning Inquiry:burning inquiry').
card_rarity('burning inquiry'/'M10', 'Common').
card_artist('burning inquiry'/'M10', 'Zoltan Boros & Gabor Szikszai').
card_number('burning inquiry'/'M10', '128').
card_flavor_text('burning inquiry'/'M10', 'Jariad burned the midnight oil, burned through scroll after scroll, and then burned down his laboratory.').
card_multiverse_id('burning inquiry'/'M10', '191096').

card_in_set('burst of speed', 'M10').
card_original_type('burst of speed'/'M10', 'Sorcery').
card_original_text('burst of speed'/'M10', 'Creatures you control gain haste until end of turn. (They can attack and {T} even if they just came under your control.)').
card_first_print('burst of speed', 'M10').
card_image_name('burst of speed'/'M10', 'burst of speed').
card_uid('burst of speed'/'M10', 'M10:Burst of Speed:burst of speed').
card_rarity('burst of speed'/'M10', 'Common').
card_artist('burst of speed'/'M10', 'Zoltan Boros & Gabor Szikszai').
card_number('burst of speed'/'M10', '129').
card_flavor_text('burst of speed'/'M10', 'Great generals inspire by example. Goblin chieftains inspire by electrifying the barracks.').
card_multiverse_id('burst of speed'/'M10', '191061').

card_in_set('cancel', 'M10').
card_original_type('cancel'/'M10', 'Instant').
card_original_text('cancel'/'M10', 'Counter target spell.').
card_image_name('cancel'/'M10', 'cancel').
card_uid('cancel'/'M10', 'M10:Cancel:cancel').
card_rarity('cancel'/'M10', 'Common').
card_artist('cancel'/'M10', 'David Palumbo').
card_number('cancel'/'M10', '44').
card_multiverse_id('cancel'/'M10', '189918').

card_in_set('canyon minotaur', 'M10').
card_original_type('canyon minotaur'/'M10', 'Creature — Minotaur Warrior').
card_original_text('canyon minotaur'/'M10', '').
card_image_name('canyon minotaur'/'M10', 'canyon minotaur').
card_uid('canyon minotaur'/'M10', 'M10:Canyon Minotaur:canyon minotaur').
card_rarity('canyon minotaur'/'M10', 'Common').
card_artist('canyon minotaur'/'M10', 'Steve Prescott').
card_number('canyon minotaur'/'M10', '130').
card_flavor_text('canyon minotaur'/'M10', '\"We\'ll scale these cliffs, traverse Brittle Bridge, and then fight our way down the volcanic slopes on the other side.\"\n\"Isn\'t the shortest route through the canyon?\"\n\"Yes.\"\n\"So shouldn\'t we—\"\n\"No.\"').
card_multiverse_id('canyon minotaur'/'M10', '191059').

card_in_set('capricious efreet', 'M10').
card_original_type('capricious efreet'/'M10', 'Creature — Efreet').
card_original_text('capricious efreet'/'M10', 'At the beginning of your upkeep, choose target nonland permanent you control and up to two target nonland permanents you don\'t control. Destroy one of them at random.').
card_first_print('capricious efreet', 'M10').
card_image_name('capricious efreet'/'M10', 'capricious efreet').
card_uid('capricious efreet'/'M10', 'M10:Capricious Efreet:capricious efreet').
card_rarity('capricious efreet'/'M10', 'Rare').
card_artist('capricious efreet'/'M10', 'Justin Sweet').
card_number('capricious efreet'/'M10', '131').
card_flavor_text('capricious efreet'/'M10', '\"You wish for great destruction? It is done, my master.\"').
card_multiverse_id('capricious efreet'/'M10', '189914').

card_in_set('captain of the watch', 'M10').
card_original_type('captain of the watch'/'M10', 'Creature — Human Soldier').
card_original_text('captain of the watch'/'M10', 'Vigilance (Attacking doesn\'t cause this creature to tap.)\nOther Soldier creatures you control get +1/+1 and have vigilance.\nWhen Captain of the Watch enters the battlefield, put three 1/1 white Soldier creature tokens onto the battlefield.').
card_first_print('captain of the watch', 'M10').
card_image_name('captain of the watch'/'M10', 'captain of the watch').
card_uid('captain of the watch'/'M10', 'M10:Captain of the Watch:captain of the watch').
card_rarity('captain of the watch'/'M10', 'Rare').
card_artist('captain of the watch'/'M10', 'Greg Staples').
card_number('captain of the watch'/'M10', '6').
card_multiverse_id('captain of the watch'/'M10', '191070').

card_in_set('celestial purge', 'M10').
card_original_type('celestial purge'/'M10', 'Instant').
card_original_text('celestial purge'/'M10', 'Exile target black or red permanent.').
card_image_name('celestial purge'/'M10', 'celestial purge').
card_uid('celestial purge'/'M10', 'M10:Celestial Purge:celestial purge').
card_rarity('celestial purge'/'M10', 'Uncommon').
card_artist('celestial purge'/'M10', 'David Palumbo').
card_number('celestial purge'/'M10', '7').
card_flavor_text('celestial purge'/'M10', '\"We see clearly the error of your ways. It does not matter that you choose not to.\"\n—Urien of the Lightwielders').
card_multiverse_id('celestial purge'/'M10', '191595').

card_in_set('cemetery reaper', 'M10').
card_original_type('cemetery reaper'/'M10', 'Creature — Zombie').
card_original_text('cemetery reaper'/'M10', 'Other Zombie creatures you control get +1/+1.\n{2}{B}, {T}: Exile target creature card from a graveyard. Put a 2/2 black Zombie creature token onto the battlefield.').
card_first_print('cemetery reaper', 'M10').
card_image_name('cemetery reaper'/'M10', 'cemetery reaper').
card_uid('cemetery reaper'/'M10', 'M10:Cemetery Reaper:cemetery reaper').
card_rarity('cemetery reaper'/'M10', 'Rare').
card_artist('cemetery reaper'/'M10', 'Dave Allsop').
card_number('cemetery reaper'/'M10', '87').
card_flavor_text('cemetery reaper'/'M10', 'Every grave cradles a recruit.').
card_multiverse_id('cemetery reaper'/'M10', '190193').

card_in_set('centaur courser', 'M10').
card_original_type('centaur courser'/'M10', 'Creature — Centaur Warrior').
card_original_text('centaur courser'/'M10', '').
card_first_print('centaur courser', 'M10').
card_image_name('centaur courser'/'M10', 'centaur courser').
card_uid('centaur courser'/'M10', 'M10:Centaur Courser:centaur courser').
card_rarity('centaur courser'/'M10', 'Common').
card_artist('centaur courser'/'M10', 'Vance Kovacs').
card_number('centaur courser'/'M10', '172').
card_flavor_text('centaur courser'/'M10', '\"The centaurs are truly free. Never will they be tamed by temptation or controlled by fear. They live in total harmony, a feat not yet achieved by our kind.\"\n—Ramal, sage of Westgate').
card_multiverse_id('centaur courser'/'M10', '189883').

card_in_set('chandra nalaar', 'M10').
card_original_type('chandra nalaar'/'M10', 'Planeswalker — Chandra').
card_original_text('chandra nalaar'/'M10', '+1: Chandra Nalaar deals 1 damage to target player.\n-X: Chandra Nalaar deals X damage to target creature.\n-8: Chandra Nalaar deals 10 damage to target player and each creature he or she controls.').
card_image_name('chandra nalaar'/'M10', 'chandra nalaar').
card_uid('chandra nalaar'/'M10', 'M10:Chandra Nalaar:chandra nalaar').
card_rarity('chandra nalaar'/'M10', 'Mythic Rare').
card_artist('chandra nalaar'/'M10', 'Aleksi Briclot').
card_number('chandra nalaar'/'M10', '132').
card_multiverse_id('chandra nalaar'/'M10', '191242').

card_in_set('child of night', 'M10').
card_original_type('child of night'/'M10', 'Creature — Vampire').
card_original_text('child of night'/'M10', 'Lifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_first_print('child of night', 'M10').
card_image_name('child of night'/'M10', 'child of night').
card_uid('child of night'/'M10', 'M10:Child of Night:child of night').
card_rarity('child of night'/'M10', 'Common').
card_artist('child of night'/'M10', 'Ash Wood').
card_number('child of night'/'M10', '88').
card_flavor_text('child of night'/'M10', 'A vampire enacts vengeance on the entire world, claiming her debt two tiny pinpricks at a time.').
card_multiverse_id('child of night'/'M10', '191068').

card_in_set('clone', 'M10').
card_original_type('clone'/'M10', 'Creature — Shapeshifter').
card_original_text('clone'/'M10', 'You may have Clone enter the battlefield as a copy of any creature on the battlefield.').
card_image_name('clone'/'M10', 'clone').
card_uid('clone'/'M10', 'M10:Clone:clone').
card_rarity('clone'/'M10', 'Rare').
card_artist('clone'/'M10', 'Kev Walker').
card_number('clone'/'M10', '45').
card_flavor_text('clone'/'M10', 'The shapeshifter mimics with a twin\'s esteem and a mirror\'s cruelty.').
card_multiverse_id('clone'/'M10', '191321').

card_in_set('coat of arms', 'M10').
card_original_type('coat of arms'/'M10', 'Artifact').
card_original_text('coat of arms'/'M10', 'Each creature gets +1/+1 for each other creature on the battlefield that shares at least one creature type with it. (For example, if two Goblin Warriors and a Goblin Shaman are on the battlefield, each gets +2/+2.)').
card_image_name('coat of arms'/'M10', 'coat of arms').
card_uid('coat of arms'/'M10', 'M10:Coat of Arms:coat of arms').
card_rarity('coat of arms'/'M10', 'Rare').
card_artist('coat of arms'/'M10', 'Scott M. Fischer').
card_number('coat of arms'/'M10', '207').
card_flavor_text('coat of arms'/'M10', 'A banner for bonds of blood or belief.').
card_multiverse_id('coat of arms'/'M10', '191314').

card_in_set('consume spirit', 'M10').
card_original_type('consume spirit'/'M10', 'Sorcery').
card_original_text('consume spirit'/'M10', 'Spend only black mana on X. \nConsume Spirit deals X damage to target creature or player and you gain X life.').
card_image_name('consume spirit'/'M10', 'consume spirit').
card_uid('consume spirit'/'M10', 'M10:Consume Spirit:consume spirit').
card_rarity('consume spirit'/'M10', 'Uncommon').
card_artist('consume spirit'/'M10', 'Justin Sweet').
card_number('consume spirit'/'M10', '89').
card_multiverse_id('consume spirit'/'M10', '189893').

card_in_set('convincing mirage', 'M10').
card_original_type('convincing mirage'/'M10', 'Enchantment — Aura').
card_original_text('convincing mirage'/'M10', 'Enchant land\nAs Convincing Mirage enters the battlefield, choose a basic land type.\nEnchanted land is the chosen type.').
card_first_print('convincing mirage', 'M10').
card_image_name('convincing mirage'/'M10', 'convincing mirage').
card_uid('convincing mirage'/'M10', 'M10:Convincing Mirage:convincing mirage').
card_rarity('convincing mirage'/'M10', 'Common').
card_artist('convincing mirage'/'M10', 'Ryan Pancoast').
card_number('convincing mirage'/'M10', '46').
card_flavor_text('convincing mirage'/'M10', '\"Where are we? You must learn to ask more useful questions than that.\"').
card_multiverse_id('convincing mirage'/'M10', '190161').

card_in_set('coral merfolk', 'M10').
card_original_type('coral merfolk'/'M10', 'Creature — Merfolk').
card_original_text('coral merfolk'/'M10', '').
card_image_name('coral merfolk'/'M10', 'coral merfolk').
card_uid('coral merfolk'/'M10', 'M10:Coral Merfolk:coral merfolk').
card_rarity('coral merfolk'/'M10', 'Common').
card_artist('coral merfolk'/'M10', 'rk post').
card_number('coral merfolk'/'M10', '47').
card_flavor_text('coral merfolk'/'M10', 'Merfolk are best known for their subtle magic. But those who try to fight them in their own territory quickly realize that merfolk are hardly defenseless on the field of battle.').
card_multiverse_id('coral merfolk'/'M10', '193749').

card_in_set('craw wurm', 'M10').
card_original_type('craw wurm'/'M10', 'Creature — Wurm').
card_original_text('craw wurm'/'M10', '').
card_image_name('craw wurm'/'M10', 'craw wurm').
card_uid('craw wurm'/'M10', 'M10:Craw Wurm:craw wurm').
card_rarity('craw wurm'/'M10', 'Common').
card_artist('craw wurm'/'M10', 'Richard Sardinha').
card_number('craw wurm'/'M10', '173').
card_flavor_text('craw wurm'/'M10', 'The most terrifying thing about the craw wurm is probably the horrible crashing sound it makes as it speeds through the forest. This noise is so loud it echoes through the trees and seems to come from all directions at once.').
card_multiverse_id('craw wurm'/'M10', '189903').

card_in_set('cudgel troll', 'M10').
card_original_type('cudgel troll'/'M10', 'Creature — Troll').
card_original_text('cudgel troll'/'M10', '{G}: Regenerate Cudgel Troll. (The next time this creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_first_print('cudgel troll', 'M10').
card_image_name('cudgel troll'/'M10', 'cudgel troll').
card_uid('cudgel troll'/'M10', 'M10:Cudgel Troll:cudgel troll').
card_rarity('cudgel troll'/'M10', 'Uncommon').
card_artist('cudgel troll'/'M10', 'Jesper Ejsing').
card_number('cudgel troll'/'M10', '174').
card_multiverse_id('cudgel troll'/'M10', '189898').

card_in_set('darksteel colossus', 'M10').
card_original_type('darksteel colossus'/'M10', 'Artifact Creature — Golem').
card_original_text('darksteel colossus'/'M10', 'Trample\nDarksteel Colossus is indestructible.\nIf Darksteel Colossus would be put into a graveyard from anywhere, reveal Darksteel Colossus and shuffle it into its owner\'s library instead.').
card_image_name('darksteel colossus'/'M10', 'darksteel colossus').
card_uid('darksteel colossus'/'M10', 'M10:Darksteel Colossus:darksteel colossus').
card_rarity('darksteel colossus'/'M10', 'Mythic Rare').
card_artist('darksteel colossus'/'M10', 'Carl Critchlow').
card_number('darksteel colossus'/'M10', '208').
card_multiverse_id('darksteel colossus'/'M10', '191312').

card_in_set('deadly recluse', 'M10').
card_original_type('deadly recluse'/'M10', 'Creature — Spider').
card_original_text('deadly recluse'/'M10', 'Reach (This creature can block creatures with flying.)\nDeathtouch (Creatures dealt damage by this creature are destroyed. You can divide this creature\'s combat damage among any of the creatures blocking or blocked by it.)').
card_first_print('deadly recluse', 'M10').
card_image_name('deadly recluse'/'M10', 'deadly recluse').
card_uid('deadly recluse'/'M10', 'M10:Deadly Recluse:deadly recluse').
card_rarity('deadly recluse'/'M10', 'Common').
card_artist('deadly recluse'/'M10', 'Warren Mahy').
card_number('deadly recluse'/'M10', '175').
card_multiverse_id('deadly recluse'/'M10', '189924').

card_in_set('deathmark', 'M10').
card_original_type('deathmark'/'M10', 'Sorcery').
card_original_text('deathmark'/'M10', 'Destroy target green or white creature.').
card_image_name('deathmark'/'M10', 'deathmark').
card_uid('deathmark'/'M10', 'M10:Deathmark:deathmark').
card_rarity('deathmark'/'M10', 'Uncommon').
card_artist('deathmark'/'M10', 'Steven Belledin').
card_number('deathmark'/'M10', '90').
card_flavor_text('deathmark'/'M10', 'Even the noblest of intentions and strongest of wills inevitably crumble into dust.').
card_multiverse_id('deathmark'/'M10', '190581').

card_in_set('demon\'s horn', 'M10').
card_original_type('demon\'s horn'/'M10', 'Artifact').
card_original_text('demon\'s horn'/'M10', 'Whenever a player casts a black spell, you may gain 1 life.').
card_image_name('demon\'s horn'/'M10', 'demon\'s horn').
card_uid('demon\'s horn'/'M10', 'M10:Demon\'s Horn:demon\'s horn').
card_rarity('demon\'s horn'/'M10', 'Uncommon').
card_artist('demon\'s horn'/'M10', 'Alan Pollack').
card_number('demon\'s horn'/'M10', '209').
card_flavor_text('demon\'s horn'/'M10', 'Its curve mimics the twists of life and death.').
card_multiverse_id('demon\'s horn'/'M10', '191247').

card_in_set('diabolic tutor', 'M10').
card_original_type('diabolic tutor'/'M10', 'Sorcery').
card_original_text('diabolic tutor'/'M10', 'Search your library for a card and put that card into your hand. Then shuffle your library.').
card_image_name('diabolic tutor'/'M10', 'diabolic tutor').
card_uid('diabolic tutor'/'M10', 'M10:Diabolic Tutor:diabolic tutor').
card_rarity('diabolic tutor'/'M10', 'Uncommon').
card_artist('diabolic tutor'/'M10', 'Greg Staples').
card_number('diabolic tutor'/'M10', '91').
card_flavor_text('diabolic tutor'/'M10', 'The wise always keep an ear open to the whispers of power.').
card_multiverse_id('diabolic tutor'/'M10', '190533').

card_in_set('disentomb', 'M10').
card_original_type('disentomb'/'M10', 'Sorcery').
card_original_text('disentomb'/'M10', 'Return target creature card from your graveyard to your hand.').
card_first_print('disentomb', 'M10').
card_image_name('disentomb'/'M10', 'disentomb').
card_uid('disentomb'/'M10', 'M10:Disentomb:disentomb').
card_rarity('disentomb'/'M10', 'Common').
card_artist('disentomb'/'M10', 'Alex Horley-Orlandelli').
card_number('disentomb'/'M10', '92').
card_flavor_text('disentomb'/'M10', '\"Stop complaining. You can rest when you\'re dead. Oh—sorry.\"\n—Liliana Vess').
card_multiverse_id('disentomb'/'M10', '190559').

card_in_set('disorient', 'M10').
card_original_type('disorient'/'M10', 'Instant').
card_original_text('disorient'/'M10', 'Target creature gets -7/-0 until end of turn.').
card_first_print('disorient', 'M10').
card_image_name('disorient'/'M10', 'disorient').
card_uid('disorient'/'M10', 'M10:Disorient:disorient').
card_rarity('disorient'/'M10', 'Common').
card_artist('disorient'/'M10', 'John Matson').
card_number('disorient'/'M10', '48').
card_flavor_text('disorient'/'M10', '\"If it has a brain, even a primitive one, we can most surely scramble it.\"\n—Garild, merfolk mage').
card_multiverse_id('disorient'/'M10', '190171').

card_in_set('divination', 'M10').
card_original_type('divination'/'M10', 'Sorcery').
card_original_text('divination'/'M10', 'Draw two cards.').
card_first_print('divination', 'M10').
card_image_name('divination'/'M10', 'divination').
card_uid('divination'/'M10', 'M10:Divination:divination').
card_rarity('divination'/'M10', 'Common').
card_artist('divination'/'M10', 'Howard Lyon').
card_number('divination'/'M10', '49').
card_flavor_text('divination'/'M10', '\"The key to unlocking this puzzle is within you.\"\n—Doriel, mentor of Mistral Isle').
card_multiverse_id('divination'/'M10', '190172').

card_in_set('divine verdict', 'M10').
card_original_type('divine verdict'/'M10', 'Instant').
card_original_text('divine verdict'/'M10', 'Destroy target attacking or blocking creature.').
card_first_print('divine verdict', 'M10').
card_image_name('divine verdict'/'M10', 'divine verdict').
card_uid('divine verdict'/'M10', 'M10:Divine Verdict:divine verdict').
card_rarity('divine verdict'/'M10', 'Common').
card_artist('divine verdict'/'M10', 'Kev Walker').
card_number('divine verdict'/'M10', '8').
card_flavor_text('divine verdict'/'M10', 'Giants and pit spawn attempted to topple the cathedral\'s pillars. The fine grit of their remains still swirls in the breeze outside.').
card_multiverse_id('divine verdict'/'M10', '189912').

card_in_set('djinn of wishes', 'M10').
card_original_type('djinn of wishes'/'M10', 'Creature — Djinn').
card_original_text('djinn of wishes'/'M10', 'Flying\nDjinn of Wishes enters the battlefield with three wish counters on it.\n{2}{U}{U}, Remove a wish counter from Djinn of Wishes: Reveal the top card of your library. You may play that card without paying its mana cost. If you don\'t, exile it.').
card_first_print('djinn of wishes', 'M10').
card_image_name('djinn of wishes'/'M10', 'djinn of wishes').
card_uid('djinn of wishes'/'M10', 'M10:Djinn of Wishes:djinn of wishes').
card_rarity('djinn of wishes'/'M10', 'Rare').
card_artist('djinn of wishes'/'M10', 'Kev Walker').
card_number('djinn of wishes'/'M10', '50').
card_multiverse_id('djinn of wishes'/'M10', '190198').

card_in_set('doom blade', 'M10').
card_original_type('doom blade'/'M10', 'Instant').
card_original_text('doom blade'/'M10', 'Destroy target nonblack creature.').
card_image_name('doom blade'/'M10', 'doom blade').
card_uid('doom blade'/'M10', 'M10:Doom Blade:doom blade').
card_rarity('doom blade'/'M10', 'Common').
card_artist('doom blade'/'M10', 'Chippy').
card_number('doom blade'/'M10', '93').
card_flavor_text('doom blade'/'M10', 'The void is without substance but cuts like steel.').
card_multiverse_id('doom blade'/'M10', '190535').

card_in_set('dragon whelp', 'M10').
card_original_type('dragon whelp'/'M10', 'Creature — Dragon').
card_original_text('dragon whelp'/'M10', 'Flying\n{R}: Dragon Whelp gets +1/+0 until end of turn. At the beginning of the end step, if this ability has been activated four or more times this turn, sacrifice Dragon Whelp.').
card_image_name('dragon whelp'/'M10', 'dragon whelp').
card_uid('dragon whelp'/'M10', 'M10:Dragon Whelp:dragon whelp').
card_rarity('dragon whelp'/'M10', 'Uncommon').
card_artist('dragon whelp'/'M10', 'Steven Belledin').
card_number('dragon whelp'/'M10', '133').
card_multiverse_id('dragon whelp'/'M10', '191060').

card_in_set('dragon\'s claw', 'M10').
card_original_type('dragon\'s claw'/'M10', 'Artifact').
card_original_text('dragon\'s claw'/'M10', 'Whenever a player casts a red spell, you may gain 1 life.').
card_image_name('dragon\'s claw'/'M10', 'dragon\'s claw').
card_uid('dragon\'s claw'/'M10', 'M10:Dragon\'s Claw:dragon\'s claw').
card_rarity('dragon\'s claw'/'M10', 'Uncommon').
card_artist('dragon\'s claw'/'M10', 'Alan Pollack').
card_number('dragon\'s claw'/'M10', '210').
card_flavor_text('dragon\'s claw'/'M10', 'Though no longer attached to the hand, it still holds its adversary in its grasp.').
card_multiverse_id('dragon\'s claw'/'M10', '191244').

card_in_set('dragonskull summit', 'M10').
card_original_type('dragonskull summit'/'M10', 'Land').
card_original_text('dragonskull summit'/'M10', 'Dragonskull Summit enters the battlefield tapped unless you control a Swamp or a Mountain.\n{T}: Add {B} or {R} to your mana pool.').
card_first_print('dragonskull summit', 'M10').
card_image_name('dragonskull summit'/'M10', 'dragonskull summit').
card_uid('dragonskull summit'/'M10', 'M10:Dragonskull Summit:dragonskull summit').
card_rarity('dragonskull summit'/'M10', 'Rare').
card_artist('dragonskull summit'/'M10', 'Jon Foster').
card_number('dragonskull summit'/'M10', '223').
card_multiverse_id('dragonskull summit'/'M10', '191091').

card_in_set('dread warlock', 'M10').
card_original_type('dread warlock'/'M10', 'Creature — Human Wizard').
card_original_text('dread warlock'/'M10', 'Dread Warlock can\'t be blocked except by black creatures.').
card_first_print('dread warlock', 'M10').
card_image_name('dread warlock'/'M10', 'dread warlock').
card_uid('dread warlock'/'M10', 'M10:Dread Warlock:dread warlock').
card_rarity('dread warlock'/'M10', 'Common').
card_artist('dread warlock'/'M10', 'Daarken').
card_number('dread warlock'/'M10', '94').
card_flavor_text('dread warlock'/'M10', '\"I don\'t know who to pity more: those who flee from my presence or the damned who can withstand it.\"').
card_multiverse_id('dread warlock'/'M10', '190575').

card_in_set('drowned catacomb', 'M10').
card_original_type('drowned catacomb'/'M10', 'Land').
card_original_text('drowned catacomb'/'M10', 'Drowned Catacomb enters the battlefield tapped unless you control an Island or a Swamp.\n{T}: Add {U} or {B} to your mana pool.').
card_first_print('drowned catacomb', 'M10').
card_image_name('drowned catacomb'/'M10', 'drowned catacomb').
card_uid('drowned catacomb'/'M10', 'M10:Drowned Catacomb:drowned catacomb').
card_rarity('drowned catacomb'/'M10', 'Rare').
card_artist('drowned catacomb'/'M10', 'Dave Kendall').
card_number('drowned catacomb'/'M10', '224').
card_multiverse_id('drowned catacomb'/'M10', '191067').

card_in_set('drudge skeletons', 'M10').
card_original_type('drudge skeletons'/'M10', 'Creature — Skeleton').
card_original_text('drudge skeletons'/'M10', '{B}: Regenerate Drudge Skeletons. (The next time this creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_image_name('drudge skeletons'/'M10', 'drudge skeletons').
card_uid('drudge skeletons'/'M10', 'M10:Drudge Skeletons:drudge skeletons').
card_rarity('drudge skeletons'/'M10', 'Common').
card_artist('drudge skeletons'/'M10', 'Daarken').
card_number('drudge skeletons'/'M10', '95').
card_flavor_text('drudge skeletons'/'M10', '\"The dead make good soldiers. They can\'t disobey orders, never surrender, and don\'t stop fighting when a random body part falls off.\"\n—Nevinyrral, Necromancer\'s Handbook').
card_multiverse_id('drudge skeletons'/'M10', '190552').

card_in_set('duress', 'M10').
card_original_type('duress'/'M10', 'Sorcery').
card_original_text('duress'/'M10', 'Target opponent reveals his or her hand. You choose a noncreature, nonland card from it. That player discards that card.').
card_image_name('duress'/'M10', 'duress').
card_uid('duress'/'M10', 'M10:Duress:duress').
card_rarity('duress'/'M10', 'Common').
card_artist('duress'/'M10', 'Steven Belledin').
card_number('duress'/'M10', '96').
card_flavor_text('duress'/'M10', '\"Don\'t worry. I\'m not going to deprive you of all your secrets. Just your most precious one.\"\n—Liliana Vess').
card_multiverse_id('duress'/'M10', '190580').

card_in_set('earthquake', 'M10').
card_original_type('earthquake'/'M10', 'Sorcery').
card_original_text('earthquake'/'M10', 'Earthquake deals X damage to each creature without flying and each player.').
card_image_name('earthquake'/'M10', 'earthquake').
card_uid('earthquake'/'M10', 'M10:Earthquake:earthquake').
card_rarity('earthquake'/'M10', 'Rare').
card_artist('earthquake'/'M10', 'Adrian Smith').
card_number('earthquake'/'M10', '134').
card_flavor_text('earthquake'/'M10', 'They fell screaming into the depths of the chasm like so many pebbles tossed down a well.').
card_multiverse_id('earthquake'/'M10', '190551').

card_in_set('elite vanguard', 'M10').
card_original_type('elite vanguard'/'M10', 'Creature — Human Soldier').
card_original_text('elite vanguard'/'M10', '').
card_first_print('elite vanguard', 'M10').
card_image_name('elite vanguard'/'M10', 'elite vanguard').
card_uid('elite vanguard'/'M10', 'M10:Elite Vanguard:elite vanguard').
card_rarity('elite vanguard'/'M10', 'Uncommon').
card_artist('elite vanguard'/'M10', 'Mark Tedin').
card_number('elite vanguard'/'M10', '9').
card_flavor_text('elite vanguard'/'M10', 'The vanguard is skilled at waging war alone. The enemy is often defeated before its reinforcements reach the front.').
card_multiverse_id('elite vanguard'/'M10', '189902').

card_in_set('elvish archdruid', 'M10').
card_original_type('elvish archdruid'/'M10', 'Creature — Elf Druid').
card_original_text('elvish archdruid'/'M10', 'Other Elf creatures you control get +1/+1.\n{T}: Add {G} to your mana pool for each Elf you control.').
card_first_print('elvish archdruid', 'M10').
card_image_name('elvish archdruid'/'M10', 'elvish archdruid').
card_uid('elvish archdruid'/'M10', 'M10:Elvish Archdruid:elvish archdruid').
card_rarity('elvish archdruid'/'M10', 'Rare').
card_artist('elvish archdruid'/'M10', 'Karl Kopinski').
card_number('elvish archdruid'/'M10', '176').
card_flavor_text('elvish archdruid'/'M10', 'He knows the name of every elf born in the last four centuries. More importantly, they all know his.').
card_multiverse_id('elvish archdruid'/'M10', '191392').

card_in_set('elvish piper', 'M10').
card_original_type('elvish piper'/'M10', 'Creature — Elf Shaman').
card_original_text('elvish piper'/'M10', '{G}, {T}: You may put a creature card from your hand onto the battlefield.').
card_image_name('elvish piper'/'M10', 'elvish piper').
card_uid('elvish piper'/'M10', 'M10:Elvish Piper:elvish piper').
card_rarity('elvish piper'/'M10', 'Rare').
card_artist('elvish piper'/'M10', 'Rebecca Guay').
card_number('elvish piper'/'M10', '177').
card_flavor_text('elvish piper'/'M10', 'From Gaea grew the world, and the world was silent. From Gaea grew the world\'s elves, and the world was silent no more.\n—Elvish teaching').
card_multiverse_id('elvish piper'/'M10', '191317').

card_in_set('elvish visionary', 'M10').
card_original_type('elvish visionary'/'M10', 'Creature — Elf Shaman').
card_original_text('elvish visionary'/'M10', 'When Elvish Visionary enters the battlefield, draw a card.').
card_image_name('elvish visionary'/'M10', 'elvish visionary').
card_uid('elvish visionary'/'M10', 'M10:Elvish Visionary:elvish visionary').
card_rarity('elvish visionary'/'M10', 'Common').
card_artist('elvish visionary'/'M10', 'D. Alexander Gregory').
card_number('elvish visionary'/'M10', '178').
card_flavor_text('elvish visionary'/'M10', 'Before any major undertaking, a Cylian elf seeks the guidance of a visionary to learn the will of the gargantuan ancients.').
card_multiverse_id('elvish visionary'/'M10', '194424').

card_in_set('emerald oryx', 'M10').
card_original_type('emerald oryx'/'M10', 'Creature — Antelope').
card_original_text('emerald oryx'/'M10', 'Forestwalk (This creature is unblockable as long as defending player controls a Forest.)').
card_first_print('emerald oryx', 'M10').
card_image_name('emerald oryx'/'M10', 'emerald oryx').
card_uid('emerald oryx'/'M10', 'M10:Emerald Oryx:emerald oryx').
card_rarity('emerald oryx'/'M10', 'Common').
card_artist('emerald oryx'/'M10', 'Daren Bader').
card_number('emerald oryx'/'M10', '179').
card_flavor_text('emerald oryx'/'M10', 'After the oryx dies, its flesh returns to the earth, but its emerald bones remain as a tribute to the forest.').
card_multiverse_id('emerald oryx'/'M10', '189890').

card_in_set('enormous baloth', 'M10').
card_original_type('enormous baloth'/'M10', 'Creature — Beast').
card_original_text('enormous baloth'/'M10', '').
card_image_name('enormous baloth'/'M10', 'enormous baloth').
card_uid('enormous baloth'/'M10', 'M10:Enormous Baloth:enormous baloth').
card_rarity('enormous baloth'/'M10', 'Uncommon').
card_artist('enormous baloth'/'M10', 'Mark Tedin').
card_number('enormous baloth'/'M10', '180').
card_flavor_text('enormous baloth'/'M10', 'Its diet consists of fruits, plants, small woodland animals, large woodland animals, woodlands, fruit groves, fruit farmers, and small cities.').
card_multiverse_id('enormous baloth'/'M10', '189882').

card_in_set('entangling vines', 'M10').
card_original_type('entangling vines'/'M10', 'Enchantment — Aura').
card_original_text('entangling vines'/'M10', 'Enchant tapped creature\nEnchanted creature doesn\'t untap during its controller\'s untap step.').
card_first_print('entangling vines', 'M10').
card_image_name('entangling vines'/'M10', 'entangling vines').
card_uid('entangling vines'/'M10', 'M10:Entangling Vines:entangling vines').
card_rarity('entangling vines'/'M10', 'Common').
card_artist('entangling vines'/'M10', 'Thomas M. Baxa').
card_number('entangling vines'/'M10', '181').
card_flavor_text('entangling vines'/'M10', 'Every living thing has roots in nature. Sometimes nature gets its roots into you.').
card_multiverse_id('entangling vines'/'M10', '189901').

card_in_set('essence scatter', 'M10').
card_original_type('essence scatter'/'M10', 'Instant').
card_original_text('essence scatter'/'M10', 'Counter target creature spell.').
card_first_print('essence scatter', 'M10').
card_image_name('essence scatter'/'M10', 'essence scatter').
card_uid('essence scatter'/'M10', 'M10:Essence Scatter:essence scatter').
card_rarity('essence scatter'/'M10', 'Common').
card_artist('essence scatter'/'M10', 'Jon Foster').
card_number('essence scatter'/'M10', '51').
card_flavor_text('essence scatter'/'M10', 'Some wizards compete not to summon the most interesting creatures, but to create the most interesting aftereffects when a summons goes awry.').
card_multiverse_id('essence scatter'/'M10', '193742').

card_in_set('excommunicate', 'M10').
card_original_type('excommunicate'/'M10', 'Sorcery').
card_original_text('excommunicate'/'M10', 'Put target creature on top of its owner\'s library.').
card_image_name('excommunicate'/'M10', 'excommunicate').
card_uid('excommunicate'/'M10', 'M10:Excommunicate:excommunicate').
card_rarity('excommunicate'/'M10', 'Common').
card_artist('excommunicate'/'M10', 'Matt Stewart').
card_number('excommunicate'/'M10', '10').
card_flavor_text('excommunicate'/'M10', '\"Our law prohibits capital punishment. We believe even the gravest offenders have the chance to redeem themselves.\"\n—Torian Sha, soul warden').
card_multiverse_id('excommunicate'/'M10', '190173').

card_in_set('fabricate', 'M10').
card_original_type('fabricate'/'M10', 'Sorcery').
card_original_text('fabricate'/'M10', 'Search your library for an artifact card, reveal it, and put it into your hand. Then shuffle your library.').
card_image_name('fabricate'/'M10', 'fabricate').
card_uid('fabricate'/'M10', 'M10:Fabricate:fabricate').
card_rarity('fabricate'/'M10', 'Uncommon').
card_artist('fabricate'/'M10', 'Warren Mahy').
card_number('fabricate'/'M10', '52').
card_flavor_text('fabricate'/'M10', '\"I\'m one masterpiece away from ruling this pathetic world.\"\n—Saldrath, master artificer').
card_multiverse_id('fabricate'/'M10', '189876').

card_in_set('fiery hellhound', 'M10').
card_original_type('fiery hellhound'/'M10', 'Creature — Elemental Hound').
card_original_text('fiery hellhound'/'M10', '{R}: Fiery Hellhound gets +1/+0 until end of turn.').
card_first_print('fiery hellhound', 'M10').
card_image_name('fiery hellhound'/'M10', 'fiery hellhound').
card_uid('fiery hellhound'/'M10', 'M10:Fiery Hellhound:fiery hellhound').
card_rarity('fiery hellhound'/'M10', 'Common').
card_artist('fiery hellhound'/'M10', 'Ted Galaday').
card_number('fiery hellhound'/'M10', '135').
card_flavor_text('fiery hellhound'/'M10', 'Once a barbarian army has conquered a region, their shamans summon hellhounds that keep undesirables out and prisoners in.').
card_multiverse_id('fiery hellhound'/'M10', '191075').

card_in_set('fireball', 'M10').
card_original_type('fireball'/'M10', 'Sorcery').
card_original_text('fireball'/'M10', 'Fireball deals X damage divided evenly, rounded down, among any number of target creatures and/or players.\nFireball costs {1} more to cast for each target beyond the first.').
card_image_name('fireball'/'M10', 'fireball').
card_uid('fireball'/'M10', 'M10:Fireball:fireball').
card_rarity('fireball'/'M10', 'Uncommon').
card_artist('fireball'/'M10', 'Dave Dorman').
card_number('fireball'/'M10', '136').
card_multiverse_id('fireball'/'M10', '191076').

card_in_set('firebreathing', 'M10').
card_original_type('firebreathing'/'M10', 'Enchantment — Aura').
card_original_text('firebreathing'/'M10', 'Enchant creature\n{R}: Enchanted creature gets +1/+0 until end of turn.').
card_image_name('firebreathing'/'M10', 'firebreathing').
card_uid('firebreathing'/'M10', 'M10:Firebreathing:firebreathing').
card_rarity('firebreathing'/'M10', 'Common').
card_artist('firebreathing'/'M10', 'Aleksi Briclot').
card_number('firebreathing'/'M10', '137').
card_flavor_text('firebreathing'/'M10', 'The mage breathed in life-giving air and breathed out death-bringing fire.').
card_multiverse_id('firebreathing'/'M10', '191079').

card_in_set('flashfreeze', 'M10').
card_original_type('flashfreeze'/'M10', 'Instant').
card_original_text('flashfreeze'/'M10', 'Counter target red or green spell.').
card_image_name('flashfreeze'/'M10', 'flashfreeze').
card_uid('flashfreeze'/'M10', 'M10:Flashfreeze:flashfreeze').
card_rarity('flashfreeze'/'M10', 'Uncommon').
card_artist('flashfreeze'/'M10', 'Brian Despain').
card_number('flashfreeze'/'M10', '53').
card_flavor_text('flashfreeze'/'M10', '\"Your downfall was not your ignorance, your weakness, or your hubris, but your warm blood.\"\n—Heidar, Rimewind master').
card_multiverse_id('flashfreeze'/'M10', '190168').

card_in_set('fog', 'M10').
card_original_type('fog'/'M10', 'Instant').
card_original_text('fog'/'M10', 'Prevent all combat damage that would be dealt this turn.').
card_image_name('fog'/'M10', 'fog').
card_uid('fog'/'M10', 'M10:Fog:fog').
card_rarity('fog'/'M10', 'Common').
card_artist('fog'/'M10', 'John Avon').
card_number('fog'/'M10', '182').
card_flavor_text('fog'/'M10', '\"I fear no army or beast, but only the morning fog. Our assault can survive everything else.\"\n—Lord Hilneth').
card_multiverse_id('fog'/'M10', '189919').

card_in_set('forest', 'M10').
card_original_type('forest'/'M10', 'Basic Land — Forest').
card_original_text('forest'/'M10', 'G').
card_image_name('forest'/'M10', 'forest1').
card_uid('forest'/'M10', 'M10:Forest:forest1').
card_rarity('forest'/'M10', 'Basic Land').
card_artist('forest'/'M10', 'Glen Angus').
card_number('forest'/'M10', '246').
card_multiverse_id('forest'/'M10', '191405').

card_in_set('forest', 'M10').
card_original_type('forest'/'M10', 'Basic Land — Forest').
card_original_text('forest'/'M10', 'G').
card_image_name('forest'/'M10', 'forest2').
card_uid('forest'/'M10', 'M10:Forest:forest2').
card_rarity('forest'/'M10', 'Basic Land').
card_artist('forest'/'M10', 'John Avon').
card_number('forest'/'M10', '247').
card_multiverse_id('forest'/'M10', '191406').

card_in_set('forest', 'M10').
card_original_type('forest'/'M10', 'Basic Land — Forest').
card_original_text('forest'/'M10', 'G').
card_image_name('forest'/'M10', 'forest3').
card_uid('forest'/'M10', 'M10:Forest:forest3').
card_rarity('forest'/'M10', 'Basic Land').
card_artist('forest'/'M10', 'Steven Belledin').
card_number('forest'/'M10', '248').
card_multiverse_id('forest'/'M10', '191407').

card_in_set('forest', 'M10').
card_original_type('forest'/'M10', 'Basic Land — Forest').
card_original_text('forest'/'M10', 'G').
card_image_name('forest'/'M10', 'forest4').
card_uid('forest'/'M10', 'M10:Forest:forest4').
card_rarity('forest'/'M10', 'Basic Land').
card_artist('forest'/'M10', 'Jim Nelson').
card_number('forest'/'M10', '249').
card_multiverse_id('forest'/'M10', '191409').

card_in_set('gargoyle castle', 'M10').
card_original_type('gargoyle castle'/'M10', 'Land').
card_original_text('gargoyle castle'/'M10', '{T}: Add {1} to your mana pool.\n{5}, {T}, Sacrifice Gargoyle Castle: Put a 3/4 colorless Gargoyle artifact creature token with flying onto the battlefield.').
card_first_print('gargoyle castle', 'M10').
card_image_name('gargoyle castle'/'M10', 'gargoyle castle').
card_uid('gargoyle castle'/'M10', 'M10:Gargoyle Castle:gargoyle castle').
card_rarity('gargoyle castle'/'M10', 'Rare').
card_artist('gargoyle castle'/'M10', 'Paul Bonner').
card_number('gargoyle castle'/'M10', '225').
card_flavor_text('gargoyle castle'/'M10', 'Most knew of the tower\'s wizard and gargoyle. Few realized which was guardian and which was master.').
card_multiverse_id('gargoyle castle'/'M10', '193754').

card_in_set('garruk wildspeaker', 'M10').
card_original_type('garruk wildspeaker'/'M10', 'Planeswalker — Garruk').
card_original_text('garruk wildspeaker'/'M10', '+1: Untap two target lands.\n-1: Put a 3/3 green Beast creature token onto the battlefield.\n-4: Creatures you control get +3/+3 and gain trample until end of turn.').
card_image_name('garruk wildspeaker'/'M10', 'garruk wildspeaker').
card_uid('garruk wildspeaker'/'M10', 'M10:Garruk Wildspeaker:garruk wildspeaker').
card_rarity('garruk wildspeaker'/'M10', 'Mythic Rare').
card_artist('garruk wildspeaker'/'M10', 'Aleksi Briclot').
card_number('garruk wildspeaker'/'M10', '183').
card_multiverse_id('garruk wildspeaker'/'M10', '191243').

card_in_set('giant growth', 'M10').
card_original_type('giant growth'/'M10', 'Instant').
card_original_text('giant growth'/'M10', 'Target creature gets +3/+3 until end of turn.').
card_image_name('giant growth'/'M10', 'giant growth').
card_uid('giant growth'/'M10', 'M10:Giant Growth:giant growth').
card_rarity('giant growth'/'M10', 'Common').
card_artist('giant growth'/'M10', 'Matt Cavotta').
card_number('giant growth'/'M10', '184').
card_multiverse_id('giant growth'/'M10', '189879').

card_in_set('giant spider', 'M10').
card_original_type('giant spider'/'M10', 'Creature — Spider').
card_original_text('giant spider'/'M10', 'Reach (This creature can block creatures with flying.)').
card_image_name('giant spider'/'M10', 'giant spider').
card_uid('giant spider'/'M10', 'M10:Giant Spider:giant spider').
card_rarity('giant spider'/'M10', 'Common').
card_artist('giant spider'/'M10', 'Randy Gallegos').
card_number('giant spider'/'M10', '185').
card_flavor_text('giant spider'/'M10', 'The giant spider\'s silk naturally resists gravity, almost weaving itself as it blankets the forest canopy.').
card_multiverse_id('giant spider'/'M10', '189895').

card_in_set('glacial fortress', 'M10').
card_original_type('glacial fortress'/'M10', 'Land').
card_original_text('glacial fortress'/'M10', 'Glacial Fortress enters the battlefield tapped unless you control a Plains or an Island.\n{T}: Add {W} or {U} to your mana pool.').
card_first_print('glacial fortress', 'M10').
card_image_name('glacial fortress'/'M10', 'glacial fortress').
card_uid('glacial fortress'/'M10', 'M10:Glacial Fortress:glacial fortress').
card_rarity('glacial fortress'/'M10', 'Rare').
card_artist('glacial fortress'/'M10', 'Franz Vohwinkel').
card_number('glacial fortress'/'M10', '226').
card_multiverse_id('glacial fortress'/'M10', '190562').

card_in_set('glorious charge', 'M10').
card_original_type('glorious charge'/'M10', 'Instant').
card_original_text('glorious charge'/'M10', 'Creatures you control get +1/+1 until end of turn.').
card_first_print('glorious charge', 'M10').
card_image_name('glorious charge'/'M10', 'glorious charge').
card_uid('glorious charge'/'M10', 'M10:Glorious Charge:glorious charge').
card_rarity('glorious charge'/'M10', 'Common').
card_artist('glorious charge'/'M10', 'Izzy').
card_number('glorious charge'/'M10', '11').
card_flavor_text('glorious charge'/'M10', '\"One\'s blade is only as sharp as one\'s conviction.\"\n—Ajani Goldmane').
card_multiverse_id('glorious charge'/'M10', '190196').

card_in_set('goblin artillery', 'M10').
card_original_type('goblin artillery'/'M10', 'Creature — Goblin Warrior').
card_original_text('goblin artillery'/'M10', '{T}: Goblin Artillery deals 2 damage to target creature or player and 3 damage to you.').
card_first_print('goblin artillery', 'M10').
card_image_name('goblin artillery'/'M10', 'goblin artillery').
card_uid('goblin artillery'/'M10', 'M10:Goblin Artillery:goblin artillery').
card_rarity('goblin artillery'/'M10', 'Uncommon').
card_artist('goblin artillery'/'M10', 'Alex Horley-Orlandelli').
card_number('goblin artillery'/'M10', '138').
card_flavor_text('goblin artillery'/'M10', 'Most goblins get their turn firing the catapult, but few achieve the coveted title of Ammunition Holder.').
card_multiverse_id('goblin artillery'/'M10', '191092').

card_in_set('goblin chieftain', 'M10').
card_original_type('goblin chieftain'/'M10', 'Creature — Goblin').
card_original_text('goblin chieftain'/'M10', 'Haste (This creature can attack and {T} as soon as it comes under your control.)\nOther Goblin creatures you control get +1/+1 and have haste.').
card_first_print('goblin chieftain', 'M10').
card_image_name('goblin chieftain'/'M10', 'goblin chieftain').
card_uid('goblin chieftain'/'M10', 'M10:Goblin Chieftain:goblin chieftain').
card_rarity('goblin chieftain'/'M10', 'Rare').
card_artist('goblin chieftain'/'M10', 'Sam Wood').
card_number('goblin chieftain'/'M10', '139').
card_flavor_text('goblin chieftain'/'M10', '\"We are goblinkind, heirs to the mountain empires of chieftains past. Rest is death to us, and arson is our call to war.\"').
card_multiverse_id('goblin chieftain'/'M10', '190558').

card_in_set('goblin piker', 'M10').
card_original_type('goblin piker'/'M10', 'Creature — Goblin Warrior').
card_original_text('goblin piker'/'M10', '').
card_image_name('goblin piker'/'M10', 'goblin piker').
card_uid('goblin piker'/'M10', 'M10:Goblin Piker:goblin piker').
card_rarity('goblin piker'/'M10', 'Common').
card_artist('goblin piker'/'M10', 'DiTerlizzi').
card_number('goblin piker'/'M10', '140').
card_flavor_text('goblin piker'/'M10', 'Once he\'d worked out which end of the thing was sharp, he was promoted to guard duty.').
card_multiverse_id('goblin piker'/'M10', '191074').

card_in_set('gorgon flail', 'M10').
card_original_type('gorgon flail'/'M10', 'Artifact — Equipment').
card_original_text('gorgon flail'/'M10', 'Equipped creature gets +1/+1 and has deathtouch. (Creatures dealt damage by this creature are destroyed. You can divide its combat damage among any of the creatures blocking or blocked by it.)\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('gorgon flail', 'M10').
card_image_name('gorgon flail'/'M10', 'gorgon flail').
card_uid('gorgon flail'/'M10', 'M10:Gorgon Flail:gorgon flail').
card_rarity('gorgon flail'/'M10', 'Uncommon').
card_artist('gorgon flail'/'M10', 'Lars Grant-West').
card_number('gorgon flail'/'M10', '211').
card_multiverse_id('gorgon flail'/'M10', '191597').

card_in_set('gravedigger', 'M10').
card_original_type('gravedigger'/'M10', 'Creature — Zombie').
card_original_text('gravedigger'/'M10', 'When Gravedigger enters the battlefield, you may return target creature card from your graveyard to your hand.').
card_image_name('gravedigger'/'M10', 'gravedigger').
card_uid('gravedigger'/'M10', 'M10:Gravedigger:gravedigger').
card_rarity('gravedigger'/'M10', 'Common').
card_artist('gravedigger'/'M10', 'Dermot Power').
card_number('gravedigger'/'M10', '97').
card_flavor_text('gravedigger'/'M10', 'Oddly, no one questioned the Cabal\'s wisdom in hiring a zombie to work at the cemetery.').
card_multiverse_id('gravedigger'/'M10', '190570').

card_in_set('great sable stag', 'M10').
card_original_type('great sable stag'/'M10', 'Creature — Elk').
card_original_text('great sable stag'/'M10', 'Great Sable Stag can\'t be countered.\nProtection from blue and from black (This creature can\'t be blocked, targeted, dealt damage, or enchanted by anything blue or black.)').
card_first_print('great sable stag', 'M10').
card_image_name('great sable stag'/'M10', 'great sable stag').
card_uid('great sable stag'/'M10', 'M10:Great Sable Stag:great sable stag').
card_rarity('great sable stag'/'M10', 'Rare').
card_artist('great sable stag'/'M10', 'Matt Cavotta').
card_number('great sable stag'/'M10', '186').
card_flavor_text('great sable stag'/'M10', 'Ineffable and incorruptible.').
card_multiverse_id('great sable stag'/'M10', '193759').

card_in_set('griffin sentinel', 'M10').
card_original_type('griffin sentinel'/'M10', 'Creature — Griffin').
card_original_text('griffin sentinel'/'M10', 'Flying\nVigilance (Attacking doesn\'t cause this creature to tap.)').
card_first_print('griffin sentinel', 'M10').
card_image_name('griffin sentinel'/'M10', 'griffin sentinel').
card_uid('griffin sentinel'/'M10', 'M10:Griffin Sentinel:griffin sentinel').
card_rarity('griffin sentinel'/'M10', 'Common').
card_artist('griffin sentinel'/'M10', 'Warren Mahy').
card_number('griffin sentinel'/'M10', '12').
card_flavor_text('griffin sentinel'/'M10', 'Once a griffin sentinel adopts a territory as its own, only death can force it to betray its post.').
card_multiverse_id('griffin sentinel'/'M10', '190170').

card_in_set('guardian seraph', 'M10').
card_original_type('guardian seraph'/'M10', 'Creature — Angel').
card_original_text('guardian seraph'/'M10', 'Flying\nIf a source an opponent controls would deal damage to you, prevent 1 of that damage.').
card_first_print('guardian seraph', 'M10').
card_image_name('guardian seraph'/'M10', 'guardian seraph').
card_uid('guardian seraph'/'M10', 'M10:Guardian Seraph:guardian seraph').
card_rarity('guardian seraph'/'M10', 'Rare').
card_artist('guardian seraph'/'M10', 'Paul Bonner').
card_number('guardian seraph'/'M10', '13').
card_flavor_text('guardian seraph'/'M10', 'Her gaze prevents the petty sins and her sword punishes the rest.').
card_multiverse_id('guardian seraph'/'M10', '193766').

card_in_set('harm\'s way', 'M10').
card_original_type('harm\'s way'/'M10', 'Instant').
card_original_text('harm\'s way'/'M10', 'The next 2 damage that a source of your choice would deal to you or a permanent you control this turn is dealt to target creature or player instead.').
card_first_print('harm\'s way', 'M10').
card_image_name('harm\'s way'/'M10', 'harm\'s way').
card_uid('harm\'s way'/'M10', 'M10:Harm\'s Way:harm\'s way').
card_rarity('harm\'s way'/'M10', 'Uncommon').
card_artist('harm\'s way'/'M10', 'Dan Scott').
card_number('harm\'s way'/'M10', '14').
card_flavor_text('harm\'s way'/'M10', 'Even when fate is already written, there\'s always time to change the names.').
card_multiverse_id('harm\'s way'/'M10', '190166').

card_in_set('haunting echoes', 'M10').
card_original_type('haunting echoes'/'M10', 'Sorcery').
card_original_text('haunting echoes'/'M10', 'Exile all cards from target player\'s graveyard other than basic land cards. For each card exiled this way, search that player\'s library for all cards with the same name as that card and exile them. Then that player shuffles his or her library.').
card_image_name('haunting echoes'/'M10', 'haunting echoes').
card_uid('haunting echoes'/'M10', 'M10:Haunting Echoes:haunting echoes').
card_rarity('haunting echoes'/'M10', 'Rare').
card_artist('haunting echoes'/'M10', 'Nils Hamm').
card_number('haunting echoes'/'M10', '98').
card_multiverse_id('haunting echoes'/'M10', '190577').

card_in_set('hive mind', 'M10').
card_original_type('hive mind'/'M10', 'Enchantment').
card_original_text('hive mind'/'M10', 'Whenever a player casts an instant or sorcery spell, each other player copies that spell. Each of those players may choose new targets for his or her copy.').
card_first_print('hive mind', 'M10').
card_image_name('hive mind'/'M10', 'hive mind').
card_uid('hive mind'/'M10', 'M10:Hive Mind:hive mind').
card_rarity('hive mind'/'M10', 'Rare').
card_artist('hive mind'/'M10', 'Steve Argyle').
card_number('hive mind'/'M10', '54').
card_flavor_text('hive mind'/'M10', '\"All consciousness is one, separated only by a thin veil of the physical.\"\n—Jace Beleren').
card_multiverse_id('hive mind'/'M10', '190556').

card_in_set('holy strength', 'M10').
card_original_type('holy strength'/'M10', 'Enchantment — Aura').
card_original_text('holy strength'/'M10', 'Enchant creature\nEnchanted creature gets +1/+2.').
card_image_name('holy strength'/'M10', 'holy strength').
card_uid('holy strength'/'M10', 'M10:Holy Strength:holy strength').
card_rarity('holy strength'/'M10', 'Common').
card_artist('holy strength'/'M10', 'Terese Nielsen').
card_number('holy strength'/'M10', '15').
card_flavor_text('holy strength'/'M10', '\"Born under the sun, the first child will seek the foundation of honor and be fortified by its righteousness.\"\n—Codex of the Constellari').
card_multiverse_id('holy strength'/'M10', '190538').

card_in_set('honor of the pure', 'M10').
card_original_type('honor of the pure'/'M10', 'Enchantment').
card_original_text('honor of the pure'/'M10', 'White creatures you control get +1/+1.').
card_image_name('honor of the pure'/'M10', 'honor of the pure').
card_uid('honor of the pure'/'M10', 'M10:Honor of the Pure:honor of the pure').
card_rarity('honor of the pure'/'M10', 'Rare').
card_artist('honor of the pure'/'M10', 'Greg Staples').
card_number('honor of the pure'/'M10', '16').
card_flavor_text('honor of the pure'/'M10', 'Together the soldiers were like a golden blade, cutting down their enemies and scarring the darkness.').
card_multiverse_id('honor of the pure'/'M10', '191058').

card_in_set('horned turtle', 'M10').
card_original_type('horned turtle'/'M10', 'Creature — Turtle').
card_original_text('horned turtle'/'M10', '').
card_image_name('horned turtle'/'M10', 'horned turtle').
card_uid('horned turtle'/'M10', 'M10:Horned Turtle:horned turtle').
card_rarity('horned turtle'/'M10', 'Common').
card_artist('horned turtle'/'M10', 'DiTerlizzi').
card_number('horned turtle'/'M10', '55').
card_flavor_text('horned turtle'/'M10', 'It\'s a mistake to say turtles hide in their shells. They merely need time to contemplate how best to bite your head off.').
card_multiverse_id('horned turtle'/'M10', '190178').

card_in_set('howl of the night pack', 'M10').
card_original_type('howl of the night pack'/'M10', 'Sorcery').
card_original_text('howl of the night pack'/'M10', 'Put a 2/2 green Wolf creature token onto the battlefield for each Forest you control.').
card_image_name('howl of the night pack'/'M10', 'howl of the night pack').
card_uid('howl of the night pack'/'M10', 'M10:Howl of the Night Pack:howl of the night pack').
card_rarity('howl of the night pack'/'M10', 'Uncommon').
card_artist('howl of the night pack'/'M10', 'Lars Grant-West').
card_number('howl of the night pack'/'M10', '187').
card_flavor_text('howl of the night pack'/'M10', 'The murderous horrors of Raven\'s Run are legendary, but even that haunted place goes quiet when the night wolves howl.').
card_multiverse_id('howl of the night pack'/'M10', '189908').

card_in_set('howling banshee', 'M10').
card_original_type('howling banshee'/'M10', 'Creature — Spirit').
card_original_text('howling banshee'/'M10', 'Flying\nWhen Howling Banshee enters the battlefield, each player loses 3 life.').
card_first_print('howling banshee', 'M10').
card_image_name('howling banshee'/'M10', 'howling banshee').
card_uid('howling banshee'/'M10', 'M10:Howling Banshee:howling banshee').
card_rarity('howling banshee'/'M10', 'Uncommon').
card_artist('howling banshee'/'M10', 'Andrew Robinson').
card_number('howling banshee'/'M10', '99').
card_flavor_text('howling banshee'/'M10', 'Villagers cloaked the town in magical silence, but their ears still bled.').
card_multiverse_id('howling banshee'/'M10', '190548').

card_in_set('howling mine', 'M10').
card_original_type('howling mine'/'M10', 'Artifact').
card_original_text('howling mine'/'M10', 'At the beginning of each player\'s draw step, if Howling Mine is untapped, that player draws an additional card.').
card_image_name('howling mine'/'M10', 'howling mine').
card_uid('howling mine'/'M10', 'M10:Howling Mine:howling mine').
card_rarity('howling mine'/'M10', 'Rare').
card_artist('howling mine'/'M10', 'Ralph Horsley').
card_number('howling mine'/'M10', '212').
card_flavor_text('howling mine'/'M10', 'The mine\'s riches never end, nor do the moans of the spirits doomed to haunt them.').
card_multiverse_id('howling mine'/'M10', '191315').

card_in_set('hypnotic specter', 'M10').
card_original_type('hypnotic specter'/'M10', 'Creature — Specter').
card_original_text('hypnotic specter'/'M10', 'Flying\nWhenever Hypnotic Specter deals damage to an opponent, that player discards a card at random.').
card_image_name('hypnotic specter'/'M10', 'hypnotic specter').
card_uid('hypnotic specter'/'M10', 'M10:Hypnotic Specter:hypnotic specter').
card_rarity('hypnotic specter'/'M10', 'Rare').
card_artist('hypnotic specter'/'M10', 'Greg Staples').
card_number('hypnotic specter'/'M10', '100').
card_flavor_text('hypnotic specter'/'M10', 'Its victims are known by their eyes: shattered vessels leaking broken dreams.').
card_multiverse_id('hypnotic specter'/'M10', '190566').

card_in_set('ice cage', 'M10').
card_original_type('ice cage'/'M10', 'Enchantment — Aura').
card_original_text('ice cage'/'M10', 'Enchant creature\nEnchanted creature can\'t attack or block, and its activated abilities can\'t be activated.\nWhen enchanted creature becomes the target of a spell or ability, destroy Ice Cage.').
card_first_print('ice cage', 'M10').
card_image_name('ice cage'/'M10', 'ice cage').
card_uid('ice cage'/'M10', 'M10:Ice Cage:ice cage').
card_rarity('ice cage'/'M10', 'Common').
card_artist('ice cage'/'M10', 'Mike Bierek').
card_number('ice cage'/'M10', '56').
card_multiverse_id('ice cage'/'M10', '189920').

card_in_set('ignite disorder', 'M10').
card_original_type('ignite disorder'/'M10', 'Instant').
card_original_text('ignite disorder'/'M10', 'Ignite Disorder deals 3 damage divided as you choose among any number of target white and/or blue creatures.').
card_image_name('ignite disorder'/'M10', 'ignite disorder').
card_uid('ignite disorder'/'M10', 'M10:Ignite Disorder:ignite disorder').
card_rarity('ignite disorder'/'M10', 'Uncommon').
card_artist('ignite disorder'/'M10', 'Zoltan Boros & Gabor Szikszai').
card_number('ignite disorder'/'M10', '141').
card_flavor_text('ignite disorder'/'M10', 'The shaman prowled the academy halls, shaking his head at the pomp and babble of the sages inside. He knew just the spell to liven the place up.').
card_multiverse_id('ignite disorder'/'M10', '190544').

card_in_set('illusionary servant', 'M10').
card_original_type('illusionary servant'/'M10', 'Creature — Illusion').
card_original_text('illusionary servant'/'M10', 'Flying\nWhen Illusionary Servant becomes the target of a spell or ability, sacrifice it.').
card_first_print('illusionary servant', 'M10').
card_image_name('illusionary servant'/'M10', 'illusionary servant').
card_uid('illusionary servant'/'M10', 'M10:Illusionary Servant:illusionary servant').
card_rarity('illusionary servant'/'M10', 'Common').
card_artist('illusionary servant'/'M10', 'Dave Kendall').
card_number('illusionary servant'/'M10', '57').
card_flavor_text('illusionary servant'/'M10', '\"Illusion is a handy but fragile medium. Use it only in a pinch, or against the dim-witted.\"\n—Jace Beleren').
card_multiverse_id('illusionary servant'/'M10', '189897').

card_in_set('indestructibility', 'M10').
card_original_type('indestructibility'/'M10', 'Enchantment — Aura').
card_original_text('indestructibility'/'M10', 'Enchant permanent\nEnchanted permanent is indestructible. (Effects that say \"destroy\" don\'t destroy that permanent. An indestructible creature can\'t be destroyed by damage.)').
card_first_print('indestructibility', 'M10').
card_image_name('indestructibility'/'M10', 'indestructibility').
card_uid('indestructibility'/'M10', 'M10:Indestructibility:indestructibility').
card_rarity('indestructibility'/'M10', 'Rare').
card_artist('indestructibility'/'M10', 'Darrell Riche').
card_number('indestructibility'/'M10', '17').
card_multiverse_id('indestructibility'/'M10', '190568').

card_in_set('inferno elemental', 'M10').
card_original_type('inferno elemental'/'M10', 'Creature — Elemental').
card_original_text('inferno elemental'/'M10', 'Whenever Inferno Elemental blocks or becomes blocked by a creature, Inferno Elemental deals 3 damage to that creature.').
card_first_print('inferno elemental', 'M10').
card_image_name('inferno elemental'/'M10', 'inferno elemental').
card_uid('inferno elemental'/'M10', 'M10:Inferno Elemental:inferno elemental').
card_rarity('inferno elemental'/'M10', 'Uncommon').
card_artist('inferno elemental'/'M10', 'Zoltan Boros & Gabor Szikszai').
card_number('inferno elemental'/'M10', '142').
card_flavor_text('inferno elemental'/'M10', '\"Those who wish to invade our monastery, please take it up with my servant.\"\n—Chandra Nalaar').
card_multiverse_id('inferno elemental'/'M10', '191090').

card_in_set('island', 'M10').
card_original_type('island'/'M10', 'Basic Land — Island').
card_original_text('island'/'M10', 'U').
card_image_name('island'/'M10', 'island1').
card_uid('island'/'M10', 'M10:Island:island1').
card_rarity('island'/'M10', 'Basic Land').
card_artist('island'/'M10', 'Rob Alexander').
card_number('island'/'M10', '234').
card_multiverse_id('island'/'M10', '191387').

card_in_set('island', 'M10').
card_original_type('island'/'M10', 'Basic Land — Island').
card_original_text('island'/'M10', 'U').
card_image_name('island'/'M10', 'island2').
card_uid('island'/'M10', 'M10:Island:island2').
card_rarity('island'/'M10', 'Basic Land').
card_artist('island'/'M10', 'John Avon').
card_number('island'/'M10', '235').
card_multiverse_id('island'/'M10', '191389').

card_in_set('island', 'M10').
card_original_type('island'/'M10', 'Basic Land — Island').
card_original_text('island'/'M10', 'U').
card_image_name('island'/'M10', 'island3').
card_uid('island'/'M10', 'M10:Island:island3').
card_rarity('island'/'M10', 'Basic Land').
card_artist('island'/'M10', 'Scott Bailey').
card_number('island'/'M10', '236').
card_multiverse_id('island'/'M10', '191390').

card_in_set('island', 'M10').
card_original_type('island'/'M10', 'Basic Land — Island').
card_original_text('island'/'M10', 'U').
card_image_name('island'/'M10', 'island4').
card_uid('island'/'M10', 'M10:Island:island4').
card_rarity('island'/'M10', 'Basic Land').
card_artist('island'/'M10', 'Fred Fields').
card_number('island'/'M10', '237').
card_multiverse_id('island'/'M10', '191400').

card_in_set('jace beleren', 'M10').
card_original_type('jace beleren'/'M10', 'Planeswalker — Jace').
card_original_text('jace beleren'/'M10', '+2: Each player draws a card.\n-1: Target player draws a card.\n-10: Target player puts the top twenty cards of his or her library into his or her graveyard.').
card_image_name('jace beleren'/'M10', 'jace beleren').
card_uid('jace beleren'/'M10', 'M10:Jace Beleren:jace beleren').
card_rarity('jace beleren'/'M10', 'Mythic Rare').
card_artist('jace beleren'/'M10', 'Aleksi Briclot').
card_number('jace beleren'/'M10', '58').
card_multiverse_id('jace beleren'/'M10', '191240').

card_in_set('jackal familiar', 'M10').
card_original_type('jackal familiar'/'M10', 'Creature — Hound').
card_original_text('jackal familiar'/'M10', 'Jackal Familiar can\'t attack or block alone.').
card_first_print('jackal familiar', 'M10').
card_image_name('jackal familiar'/'M10', 'jackal familiar').
card_uid('jackal familiar'/'M10', 'M10:Jackal Familiar:jackal familiar').
card_rarity('jackal familiar'/'M10', 'Common').
card_artist('jackal familiar'/'M10', 'Alex Horley-Orlandelli').
card_number('jackal familiar'/'M10', '143').
card_flavor_text('jackal familiar'/'M10', '\"Let the gray-hairs keep their ravens and drakes. My familiar will be a reflection of me: bloodthirsty, with a predator\'s instinct.\"\n—Taivang, barbarian warlord').
card_multiverse_id('jackal familiar'/'M10', '191073').

card_in_set('jump', 'M10').
card_original_type('jump'/'M10', 'Instant').
card_original_text('jump'/'M10', 'Target creature gains flying until end of turn.').
card_image_name('jump'/'M10', 'jump').
card_uid('jump'/'M10', 'M10:Jump:jump').
card_rarity('jump'/'M10', 'Common').
card_artist('jump'/'M10', 'Eric Deschamps').
card_number('jump'/'M10', '59').
card_flavor_text('jump'/'M10', '\"Humanity has always envied the power of flight. So we took it.\"\n—Scytha, aeromage').
card_multiverse_id('jump'/'M10', '193743').

card_in_set('kalonian behemoth', 'M10').
card_original_type('kalonian behemoth'/'M10', 'Creature — Beast').
card_original_text('kalonian behemoth'/'M10', 'Shroud (This creature can\'t be the target of spells or abilities.)').
card_first_print('kalonian behemoth', 'M10').
card_image_name('kalonian behemoth'/'M10', 'kalonian behemoth').
card_uid('kalonian behemoth'/'M10', 'M10:Kalonian Behemoth:kalonian behemoth').
card_rarity('kalonian behemoth'/'M10', 'Rare').
card_artist('kalonian behemoth'/'M10', 'Daarken').
card_number('kalonian behemoth'/'M10', '188').
card_flavor_text('kalonian behemoth'/'M10', '\"Do not fire upon it—it is bad luck. And a waste of arrows.\"\n—Alera Benath, Kalonian ranger').
card_multiverse_id('kalonian behemoth'/'M10', '191394').

card_in_set('kelinore bat', 'M10').
card_original_type('kelinore bat'/'M10', 'Creature — Bat').
card_original_text('kelinore bat'/'M10', 'Flying').
card_first_print('kelinore bat', 'M10').
card_image_name('kelinore bat'/'M10', 'kelinore bat').
card_uid('kelinore bat'/'M10', 'M10:Kelinore Bat:kelinore bat').
card_rarity('kelinore bat'/'M10', 'Common').
card_artist('kelinore bat'/'M10', 'Dave Kendall').
card_number('kelinore bat'/'M10', '101').
card_flavor_text('kelinore bat'/'M10', 'The nesting bats of Kelinore sleep soundly while the sun passes, nestled among their grisly collections.').
card_multiverse_id('kelinore bat'/'M10', '190536').

card_in_set('kindled fury', 'M10').
card_original_type('kindled fury'/'M10', 'Instant').
card_original_text('kindled fury'/'M10', 'Target creature gets +1/+0 and gains first strike until end of turn. (It deals combat damage before creatures without first strike.)').
card_image_name('kindled fury'/'M10', 'kindled fury').
card_uid('kindled fury'/'M10', 'M10:Kindled Fury:kindled fury').
card_rarity('kindled fury'/'M10', 'Common').
card_artist('kindled fury'/'M10', 'Wayne Reynolds').
card_number('kindled fury'/'M10', '144').
card_flavor_text('kindled fury'/'M10', '\"Rage is a dangerous weapon. Your enemies will try to use your anger against you. Use it against them first.\"\n—Ajani Goldmane').
card_multiverse_id('kindled fury'/'M10', '193741').

card_in_set('kraken\'s eye', 'M10').
card_original_type('kraken\'s eye'/'M10', 'Artifact').
card_original_text('kraken\'s eye'/'M10', 'Whenever a player casts a blue spell, you may gain 1 life.').
card_image_name('kraken\'s eye'/'M10', 'kraken\'s eye').
card_uid('kraken\'s eye'/'M10', 'M10:Kraken\'s Eye:kraken\'s eye').
card_rarity('kraken\'s eye'/'M10', 'Uncommon').
card_artist('kraken\'s eye'/'M10', 'Alan Pollack').
card_number('kraken\'s eye'/'M10', '213').
card_flavor_text('kraken\'s eye'/'M10', 'Bright as a mirror, dark as the sea.').
card_multiverse_id('kraken\'s eye'/'M10', '191246').

card_in_set('lava axe', 'M10').
card_original_type('lava axe'/'M10', 'Sorcery').
card_original_text('lava axe'/'M10', 'Lava Axe deals 5 damage to target player.').
card_image_name('lava axe'/'M10', 'lava axe').
card_uid('lava axe'/'M10', 'M10:Lava Axe:lava axe').
card_rarity('lava axe'/'M10', 'Common').
card_artist('lava axe'/'M10', 'Brian Snõddy').
card_number('lava axe'/'M10', '145').
card_flavor_text('lava axe'/'M10', '\"Catch!\"').
card_multiverse_id('lava axe'/'M10', '191084').

card_in_set('levitation', 'M10').
card_original_type('levitation'/'M10', 'Enchantment').
card_original_text('levitation'/'M10', 'Creatures you control have flying.').
card_image_name('levitation'/'M10', 'levitation').
card_uid('levitation'/'M10', 'M10:Levitation:levitation').
card_rarity('levitation'/'M10', 'Uncommon').
card_artist('levitation'/'M10', 'Jim Murray').
card_number('levitation'/'M10', '60').
card_flavor_text('levitation'/'M10', 'The horses of Scytha\'s army must be well trained. A thrown rider would fall miles before meeting the ground.').
card_multiverse_id('levitation'/'M10', '190191').

card_in_set('lifelink', 'M10').
card_original_type('lifelink'/'M10', 'Enchantment — Aura').
card_original_text('lifelink'/'M10', 'Enchant creature\nEnchanted creature has lifelink. (Damage dealt by the creature also causes its controller to gain that much life.)').
card_first_print('lifelink', 'M10').
card_image_name('lifelink'/'M10', 'lifelink').
card_uid('lifelink'/'M10', 'M10:Lifelink:lifelink').
card_rarity('lifelink'/'M10', 'Common').
card_artist('lifelink'/'M10', 'Terese Nielsen').
card_number('lifelink'/'M10', '18').
card_flavor_text('lifelink'/'M10', 'The spoils of war are not measured only in gold and silver.').
card_multiverse_id('lifelink'/'M10', '190180').

card_in_set('lightning bolt', 'M10').
card_original_type('lightning bolt'/'M10', 'Instant').
card_original_text('lightning bolt'/'M10', 'Lightning Bolt deals 3 damage to target creature or player.').
card_image_name('lightning bolt'/'M10', 'lightning bolt').
card_uid('lightning bolt'/'M10', 'M10:Lightning Bolt:lightning bolt').
card_rarity('lightning bolt'/'M10', 'Common').
card_artist('lightning bolt'/'M10', 'Christopher Moeller').
card_number('lightning bolt'/'M10', '146').
card_flavor_text('lightning bolt'/'M10', 'The sparkmage shrieked, calling on the rage of the storms of his youth. To his surprise, the sky responded with a fierce energy he\'d never thought to see again.').
card_multiverse_id('lightning bolt'/'M10', '191089').

card_in_set('lightning elemental', 'M10').
card_original_type('lightning elemental'/'M10', 'Creature — Elemental').
card_original_text('lightning elemental'/'M10', 'Haste (This creature can attack and {T} as soon as it comes under your control.)').
card_image_name('lightning elemental'/'M10', 'lightning elemental').
card_uid('lightning elemental'/'M10', 'M10:Lightning Elemental:lightning elemental').
card_rarity('lightning elemental'/'M10', 'Common').
card_artist('lightning elemental'/'M10', 'Kev Walker').
card_number('lightning elemental'/'M10', '147').
card_flavor_text('lightning elemental'/'M10', '\"A flash of the lightning, a break of the wave,\nHe passes from life to his rest in the grave.\"\n—William Knox, \"Mortality\"').
card_multiverse_id('lightning elemental'/'M10', '191054').

card_in_set('lightwielder paladin', 'M10').
card_original_type('lightwielder paladin'/'M10', 'Creature — Human Knight').
card_original_text('lightwielder paladin'/'M10', 'First strike (This creature deals combat damage before creatures without first strike.)\nWhenever Lightwielder Paladin deals combat damage to a player, you may exile target black or red permanent that player controls.').
card_first_print('lightwielder paladin', 'M10').
card_image_name('lightwielder paladin'/'M10', 'lightwielder paladin').
card_uid('lightwielder paladin'/'M10', 'M10:Lightwielder Paladin:lightwielder paladin').
card_rarity('lightwielder paladin'/'M10', 'Rare').
card_artist('lightwielder paladin'/'M10', 'D. Alexander Gregory').
card_number('lightwielder paladin'/'M10', '19').
card_multiverse_id('lightwielder paladin'/'M10', '193763').

card_in_set('liliana vess', 'M10').
card_original_type('liliana vess'/'M10', 'Planeswalker — Liliana').
card_original_text('liliana vess'/'M10', '+1: Target player discards a card.\n-2: Search your library for a card, then shuffle your library and put that card on top of it.\n-8: Put all creature cards in all graveyards onto the battlefield under your control.').
card_image_name('liliana vess'/'M10', 'liliana vess').
card_uid('liliana vess'/'M10', 'M10:Liliana Vess:liliana vess').
card_rarity('liliana vess'/'M10', 'Mythic Rare').
card_artist('liliana vess'/'M10', 'Aleksi Briclot').
card_number('liliana vess'/'M10', '102').
card_multiverse_id('liliana vess'/'M10', '191241').

card_in_set('llanowar elves', 'M10').
card_original_type('llanowar elves'/'M10', 'Creature — Elf Druid').
card_original_text('llanowar elves'/'M10', '{T}: Add {G} to your mana pool.').
card_image_name('llanowar elves'/'M10', 'llanowar elves').
card_uid('llanowar elves'/'M10', 'M10:Llanowar Elves:llanowar elves').
card_rarity('llanowar elves'/'M10', 'Common').
card_artist('llanowar elves'/'M10', 'Kev Walker').
card_number('llanowar elves'/'M10', '189').
card_flavor_text('llanowar elves'/'M10', 'One bone broken for every twig snapped underfoot.\n—Llanowar penalty for trespassing').
card_multiverse_id('llanowar elves'/'M10', '189878').

card_in_set('looming shade', 'M10').
card_original_type('looming shade'/'M10', 'Creature — Shade').
card_original_text('looming shade'/'M10', '{B}: Looming Shade gets +1/+1 until end of turn.').
card_image_name('looming shade'/'M10', 'looming shade').
card_uid('looming shade'/'M10', 'M10:Looming Shade:looming shade').
card_rarity('looming shade'/'M10', 'Common').
card_artist('looming shade'/'M10', 'Kev Walker').
card_number('looming shade'/'M10', '103').
card_flavor_text('looming shade'/'M10', 'Its form never rests, propelled by some undetectable gale from beyond the gloom.').
card_multiverse_id('looming shade'/'M10', '190550').

card_in_set('lurking predators', 'M10').
card_original_type('lurking predators'/'M10', 'Enchantment').
card_original_text('lurking predators'/'M10', 'Whenever an opponent casts a spell, reveal the top card of your library. If it\'s a creature card, put it onto the battlefield. Otherwise, you may put that card on the bottom of your library.').
card_first_print('lurking predators', 'M10').
card_image_name('lurking predators'/'M10', 'lurking predators').
card_uid('lurking predators'/'M10', 'M10:Lurking Predators:lurking predators').
card_rarity('lurking predators'/'M10', 'Rare').
card_artist('lurking predators'/'M10', 'Mike Bierek').
card_number('lurking predators'/'M10', '190').
card_flavor_text('lurking predators'/'M10', 'All are prey to the eyes of the wild.').
card_multiverse_id('lurking predators'/'M10', '191388').

card_in_set('magebane armor', 'M10').
card_original_type('magebane armor'/'M10', 'Artifact — Equipment').
card_original_text('magebane armor'/'M10', 'Equipped creature gets +2/+4 and loses flying.\nPrevent all noncombat damage that would be dealt to equipped creature.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('magebane armor', 'M10').
card_image_name('magebane armor'/'M10', 'magebane armor').
card_uid('magebane armor'/'M10', 'M10:Magebane Armor:magebane armor').
card_rarity('magebane armor'/'M10', 'Rare').
card_artist('magebane armor'/'M10', 'Izzy').
card_number('magebane armor'/'M10', '214').
card_multiverse_id('magebane armor'/'M10', '191591').

card_in_set('magma phoenix', 'M10').
card_original_type('magma phoenix'/'M10', 'Creature — Phoenix').
card_original_text('magma phoenix'/'M10', 'Flying\nWhen Magma Phoenix is put into a graveyard from the battlefield, it deals 3 damage to each creature and each player.\n{3}{R}{R}: Return Magma Phoenix from your graveyard to your hand.').
card_first_print('magma phoenix', 'M10').
card_image_name('magma phoenix'/'M10', 'magma phoenix').
card_uid('magma phoenix'/'M10', 'M10:Magma Phoenix:magma phoenix').
card_rarity('magma phoenix'/'M10', 'Rare').
card_artist('magma phoenix'/'M10', 'Raymond Swanland').
card_number('magma phoenix'/'M10', '148').
card_multiverse_id('magma phoenix'/'M10', '191397').

card_in_set('manabarbs', 'M10').
card_original_type('manabarbs'/'M10', 'Enchantment').
card_original_text('manabarbs'/'M10', 'Whenever a player taps a land for mana, Manabarbs deals 1 damage to that player.').
card_image_name('manabarbs'/'M10', 'manabarbs').
card_uid('manabarbs'/'M10', 'M10:Manabarbs:manabarbs').
card_rarity('manabarbs'/'M10', 'Rare').
card_artist('manabarbs'/'M10', 'Jeff Miracola').
card_number('manabarbs'/'M10', '149').
card_flavor_text('manabarbs'/'M10', '\"I don\'t know why people say a double-edged sword is bad. It\'s a sword. With two edges.\"\n—Kamahl, pit fighter').
card_multiverse_id('manabarbs'/'M10', '193748').

card_in_set('master of the wild hunt', 'M10').
card_original_type('master of the wild hunt'/'M10', 'Creature — Human Shaman').
card_original_text('master of the wild hunt'/'M10', 'At the beginning of your upkeep, put a 2/2 green Wolf creature token onto the battlefield.\n{T}: Tap all untapped Wolf creatures you control. Each Wolf tapped this way deals damage equal to its power to target creature. That creature deals damage equal to its power divided as its controller chooses among any number of those Wolves.').
card_first_print('master of the wild hunt', 'M10').
card_image_name('master of the wild hunt'/'M10', 'master of the wild hunt').
card_uid('master of the wild hunt'/'M10', 'M10:Master of the Wild Hunt:master of the wild hunt').
card_rarity('master of the wild hunt'/'M10', 'Mythic Rare').
card_artist('master of the wild hunt'/'M10', 'Kev Walker').
card_number('master of the wild hunt'/'M10', '191').
card_multiverse_id('master of the wild hunt'/'M10', '191064').

card_in_set('megrim', 'M10').
card_original_type('megrim'/'M10', 'Enchantment').
card_original_text('megrim'/'M10', 'Whenever an opponent discards a card, Megrim deals 2 damage to that player.').
card_image_name('megrim'/'M10', 'megrim').
card_uid('megrim'/'M10', 'M10:Megrim:megrim').
card_rarity('megrim'/'M10', 'Uncommon').
card_artist('megrim'/'M10', 'Nick Percival').
card_number('megrim'/'M10', '104').
card_flavor_text('megrim'/'M10', '\"It\'s a vicious circle. Stress breeds pain. Pain breeds stress. It\'s simply beautiful, wouldn\'t you agree?\"\n—Vradeen, vampire nocturnus').
card_multiverse_id('megrim'/'M10', '189904').

card_in_set('merfolk looter', 'M10').
card_original_type('merfolk looter'/'M10', 'Creature — Merfolk Rogue').
card_original_text('merfolk looter'/'M10', '{T}: Draw a card, then discard a card.').
card_image_name('merfolk looter'/'M10', 'merfolk looter').
card_uid('merfolk looter'/'M10', 'M10:Merfolk Looter:merfolk looter').
card_rarity('merfolk looter'/'M10', 'Common').
card_artist('merfolk looter'/'M10', 'Austin Hsu').
card_number('merfolk looter'/'M10', '61').
card_flavor_text('merfolk looter'/'M10', 'Merfolk don\'t always know what they\'re looking for, but they\'re certain once they find it.').
card_multiverse_id('merfolk looter'/'M10', '190174').

card_in_set('merfolk sovereign', 'M10').
card_original_type('merfolk sovereign'/'M10', 'Creature — Merfolk').
card_original_text('merfolk sovereign'/'M10', 'Other Merfolk creatures you control get +1/+1.\n{T}: Target Merfolk creature is unblockable this turn.').
card_first_print('merfolk sovereign', 'M10').
card_image_name('merfolk sovereign'/'M10', 'merfolk sovereign').
card_uid('merfolk sovereign'/'M10', 'M10:Merfolk Sovereign:merfolk sovereign').
card_rarity('merfolk sovereign'/'M10', 'Rare').
card_artist('merfolk sovereign'/'M10', 'Jesper Ejsing').
card_number('merfolk sovereign'/'M10', '62').
card_flavor_text('merfolk sovereign'/'M10', '\"The sharks envy our ferocity. The eels envy our cunning.\"').
card_multiverse_id('merfolk sovereign'/'M10', '191087').

card_in_set('mesa enchantress', 'M10').
card_original_type('mesa enchantress'/'M10', 'Creature — Human Druid').
card_original_text('mesa enchantress'/'M10', 'Whenever you cast an enchantment spell, you may draw a card.').
card_image_name('mesa enchantress'/'M10', 'mesa enchantress').
card_uid('mesa enchantress'/'M10', 'M10:Mesa Enchantress:mesa enchantress').
card_rarity('mesa enchantress'/'M10', 'Rare').
card_artist('mesa enchantress'/'M10', 'Randy Gallegos').
card_number('mesa enchantress'/'M10', '20').
card_flavor_text('mesa enchantress'/'M10', 'She shepherds mysteries and dust as others would a flock of sheep.').
card_multiverse_id('mesa enchantress'/'M10', '191378').

card_in_set('might of oaks', 'M10').
card_original_type('might of oaks'/'M10', 'Instant').
card_original_text('might of oaks'/'M10', 'Target creature gets +7/+7 until end of turn.').
card_image_name('might of oaks'/'M10', 'might of oaks').
card_uid('might of oaks'/'M10', 'M10:Might of Oaks:might of oaks').
card_rarity('might of oaks'/'M10', 'Rare').
card_artist('might of oaks'/'M10', 'Jeremy Jarvis').
card_number('might of oaks'/'M10', '192').
card_flavor_text('might of oaks'/'M10', 'May your weapon be no sword forged. May your armor be no metal wrought.\n—Elvish saying').
card_multiverse_id('might of oaks'/'M10', '191316').

card_in_set('mind control', 'M10').
card_original_type('mind control'/'M10', 'Enchantment — Aura').
card_original_text('mind control'/'M10', 'Enchant creature\nYou control enchanted creature.').
card_image_name('mind control'/'M10', 'mind control').
card_uid('mind control'/'M10', 'M10:Mind Control:mind control').
card_rarity('mind control'/'M10', 'Uncommon').
card_artist('mind control'/'M10', 'Mark Tedin').
card_number('mind control'/'M10', '63').
card_flavor_text('mind control'/'M10', '\"Why fight the body when you can dominate the mind that rules it?\"\n—Jace Beleren').
card_multiverse_id('mind control'/'M10', '190197').

card_in_set('mind rot', 'M10').
card_original_type('mind rot'/'M10', 'Sorcery').
card_original_text('mind rot'/'M10', 'Target player discards two cards.').
card_image_name('mind rot'/'M10', 'mind rot').
card_uid('mind rot'/'M10', 'M10:Mind Rot:mind rot').
card_rarity('mind rot'/'M10', 'Common').
card_artist('mind rot'/'M10', 'Steve Luke').
card_number('mind rot'/'M10', '105').
card_flavor_text('mind rot'/'M10', '\"The beauty of mental attacks is that your victims never remember them.\"\n—Volrath').
card_multiverse_id('mind rot'/'M10', '190555').

card_in_set('mind shatter', 'M10').
card_original_type('mind shatter'/'M10', 'Sorcery').
card_original_text('mind shatter'/'M10', 'Target player discards X cards at random.').
card_image_name('mind shatter'/'M10', 'mind shatter').
card_uid('mind shatter'/'M10', 'M10:Mind Shatter:mind shatter').
card_rarity('mind shatter'/'M10', 'Rare').
card_artist('mind shatter'/'M10', 'Michael Sutfin').
card_number('mind shatter'/'M10', '106').
card_flavor_text('mind shatter'/'M10', 'Dark thoughts hatch and twist within the mind, straining to take wing.').
card_multiverse_id('mind shatter'/'M10', '191324').

card_in_set('mind spring', 'M10').
card_original_type('mind spring'/'M10', 'Sorcery').
card_original_text('mind spring'/'M10', 'Draw X cards.').
card_image_name('mind spring'/'M10', 'mind spring').
card_uid('mind spring'/'M10', 'M10:Mind Spring:mind spring').
card_rarity('mind spring'/'M10', 'Rare').
card_artist('mind spring'/'M10', 'Mark Zug').
card_number('mind spring'/'M10', '64').
card_flavor_text('mind spring'/'M10', 'Fragments of thought refract and multiply, surging in a geyser of dizzying insight.').
card_multiverse_id('mind spring'/'M10', '191323').

card_in_set('mirror of fate', 'M10').
card_original_type('mirror of fate'/'M10', 'Artifact').
card_original_text('mirror of fate'/'M10', '{T}, Sacrifice Mirror of Fate: Choose up to seven face-up exiled cards you own. Exile all the cards from your library, then put the chosen cards on top of your library.').
card_first_print('mirror of fate', 'M10').
card_image_name('mirror of fate'/'M10', 'mirror of fate').
card_uid('mirror of fate'/'M10', 'M10:Mirror of Fate:mirror of fate').
card_rarity('mirror of fate'/'M10', 'Rare').
card_artist('mirror of fate'/'M10', 'Alan Pollack').
card_number('mirror of fate'/'M10', '215').
card_flavor_text('mirror of fate'/'M10', 'As the glass shattered, forgotten realities and dead memories came flooding back.').
card_multiverse_id('mirror of fate'/'M10', '193878').

card_in_set('mist leopard', 'M10').
card_original_type('mist leopard'/'M10', 'Creature — Cat').
card_original_text('mist leopard'/'M10', 'Shroud (This creature can\'t be the target of spells or abilities.)').
card_first_print('mist leopard', 'M10').
card_image_name('mist leopard'/'M10', 'mist leopard').
card_uid('mist leopard'/'M10', 'M10:Mist Leopard:mist leopard').
card_rarity('mist leopard'/'M10', 'Common').
card_artist('mist leopard'/'M10', 'John Matson').
card_number('mist leopard'/'M10', '193').
card_flavor_text('mist leopard'/'M10', '\"I can\'t claim to hunt all creatures. There are some things in the jungle that simply can\'t be hunted.\"\n—Garruk Wildspeaker').
card_multiverse_id('mist leopard'/'M10', '189921').

card_in_set('mold adder', 'M10').
card_original_type('mold adder'/'M10', 'Creature — Fungus Snake').
card_original_text('mold adder'/'M10', 'Whenever an opponent casts a blue or black spell, you may put a +1/+1 counter on Mold Adder.').
card_first_print('mold adder', 'M10').
card_image_name('mold adder'/'M10', 'mold adder').
card_uid('mold adder'/'M10', 'M10:Mold Adder:mold adder').
card_rarity('mold adder'/'M10', 'Uncommon').
card_artist('mold adder'/'M10', 'Matt Cavotta').
card_number('mold adder'/'M10', '194').
card_flavor_text('mold adder'/'M10', 'The more you struggle against its coils, the tighter they get.').
card_multiverse_id('mold adder'/'M10', '189909').

card_in_set('mountain', 'M10').
card_original_type('mountain'/'M10', 'Basic Land — Mountain').
card_original_text('mountain'/'M10', 'R').
card_image_name('mountain'/'M10', 'mountain1').
card_uid('mountain'/'M10', 'M10:Mountain:mountain1').
card_rarity('mountain'/'M10', 'Basic Land').
card_artist('mountain'/'M10', 'Rob Alexander').
card_number('mountain'/'M10', '242').
card_multiverse_id('mountain'/'M10', '191402').

card_in_set('mountain', 'M10').
card_original_type('mountain'/'M10', 'Basic Land — Mountain').
card_original_text('mountain'/'M10', 'R').
card_image_name('mountain'/'M10', 'mountain2').
card_uid('mountain'/'M10', 'M10:Mountain:mountain2').
card_rarity('mountain'/'M10', 'Basic Land').
card_artist('mountain'/'M10', 'Nils Hamm').
card_number('mountain'/'M10', '243').
card_multiverse_id('mountain'/'M10', '191403').

card_in_set('mountain', 'M10').
card_original_type('mountain'/'M10', 'Basic Land — Mountain').
card_original_text('mountain'/'M10', 'R').
card_image_name('mountain'/'M10', 'mountain3').
card_uid('mountain'/'M10', 'M10:Mountain:mountain3').
card_rarity('mountain'/'M10', 'Basic Land').
card_artist('mountain'/'M10', 'Karl Kopinski').
card_number('mountain'/'M10', '244').
card_multiverse_id('mountain'/'M10', '191404').

card_in_set('mountain', 'M10').
card_original_type('mountain'/'M10', 'Basic Land — Mountain').
card_original_text('mountain'/'M10', 'R').
card_image_name('mountain'/'M10', 'mountain4').
card_uid('mountain'/'M10', 'M10:Mountain:mountain4').
card_rarity('mountain'/'M10', 'Basic Land').
card_artist('mountain'/'M10', 'Sam Wood').
card_number('mountain'/'M10', '245').
card_multiverse_id('mountain'/'M10', '191401').

card_in_set('naturalize', 'M10').
card_original_type('naturalize'/'M10', 'Instant').
card_original_text('naturalize'/'M10', 'Destroy target artifact or enchantment.').
card_image_name('naturalize'/'M10', 'naturalize').
card_uid('naturalize'/'M10', 'M10:Naturalize:naturalize').
card_rarity('naturalize'/'M10', 'Common').
card_artist('naturalize'/'M10', 'Tim Hildebrandt').
card_number('naturalize'/'M10', '195').
card_flavor_text('naturalize'/'M10', 'If you want to destroy something lifeless, make it live.').
card_multiverse_id('naturalize'/'M10', '189896').

card_in_set('nature\'s spiral', 'M10').
card_original_type('nature\'s spiral'/'M10', 'Sorcery').
card_original_text('nature\'s spiral'/'M10', 'Return target permanent card from your graveyard to your hand. (A permanent card is an artifact, creature, enchantment, land, or planeswalker card.)').
card_first_print('nature\'s spiral', 'M10').
card_image_name('nature\'s spiral'/'M10', 'nature\'s spiral').
card_uid('nature\'s spiral'/'M10', 'M10:Nature\'s Spiral:nature\'s spiral').
card_rarity('nature\'s spiral'/'M10', 'Uncommon').
card_artist('nature\'s spiral'/'M10', 'Terese Nielsen').
card_number('nature\'s spiral'/'M10', '196').
card_flavor_text('nature\'s spiral'/'M10', 'Like nature, the fern spirals back on itself, eternally seeking its own center.').
card_multiverse_id('nature\'s spiral'/'M10', '189917').

card_in_set('negate', 'M10').
card_original_type('negate'/'M10', 'Instant').
card_original_text('negate'/'M10', 'Counter target noncreature spell.').
card_image_name('negate'/'M10', 'negate').
card_uid('negate'/'M10', 'M10:Negate:negate').
card_rarity('negate'/'M10', 'Common').
card_artist('negate'/'M10', 'Jeremy Jarvis').
card_number('negate'/'M10', '65').
card_flavor_text('negate'/'M10', 'Masters of the arcane savor a delicious irony. Their study of deep and complex arcana leads to such a simple end: the ability to say merely yes or no.').
card_multiverse_id('negate'/'M10', '190164').

card_in_set('nightmare', 'M10').
card_original_type('nightmare'/'M10', 'Creature — Nightmare Horse').
card_original_text('nightmare'/'M10', 'Flying\nNightmare\'s power and toughness are each equal to the number of Swamps you control.').
card_image_name('nightmare'/'M10', 'nightmare').
card_uid('nightmare'/'M10', 'M10:Nightmare:nightmare').
card_rarity('nightmare'/'M10', 'Rare').
card_artist('nightmare'/'M10', 'Carl Critchlow').
card_number('nightmare'/'M10', '107').
card_flavor_text('nightmare'/'M10', 'The thunder of its hooves beats dreams into despair.').
card_multiverse_id('nightmare'/'M10', '191319').

card_in_set('oakenform', 'M10').
card_original_type('oakenform'/'M10', 'Enchantment — Aura').
card_original_text('oakenform'/'M10', 'Enchant creature\nEnchanted creature gets +3/+3.').
card_first_print('oakenform', 'M10').
card_image_name('oakenform'/'M10', 'oakenform').
card_uid('oakenform'/'M10', 'M10:Oakenform:oakenform').
card_rarity('oakenform'/'M10', 'Common').
card_artist('oakenform'/'M10', 'Wayne Reynolds').
card_number('oakenform'/'M10', '197').
card_flavor_text('oakenform'/'M10', '\"When the beast cloaks itself in the mighty oak, what good is a bow? When the oak wraps itself around the snarling beast, what good is a hatchet?\"\n—Dionus, elvish archdruid').
card_multiverse_id('oakenform'/'M10', '189922').

card_in_set('open the vaults', 'M10').
card_original_type('open the vaults'/'M10', 'Sorcery').
card_original_text('open the vaults'/'M10', 'Return all artifact and enchantment cards from all graveyards to the battlefield under their owners\' control. (Auras with nothing to enchant remain in graveyards.)').
card_first_print('open the vaults', 'M10').
card_image_name('open the vaults'/'M10', 'open the vaults').
card_uid('open the vaults'/'M10', 'M10:Open the Vaults:open the vaults').
card_rarity('open the vaults'/'M10', 'Rare').
card_artist('open the vaults'/'M10', 'Brian Despain').
card_number('open the vaults'/'M10', '21').
card_flavor_text('open the vaults'/'M10', 'When the vaults are empty, everyone is rich.').
card_multiverse_id('open the vaults'/'M10', '193877').

card_in_set('ornithopter', 'M10').
card_original_type('ornithopter'/'M10', 'Artifact Creature — Thopter').
card_original_text('ornithopter'/'M10', 'Flying').
card_image_name('ornithopter'/'M10', 'ornithopter').
card_uid('ornithopter'/'M10', 'M10:Ornithopter:ornithopter').
card_rarity('ornithopter'/'M10', 'Uncommon').
card_artist('ornithopter'/'M10', 'Franz Vohwinkel').
card_number('ornithopter'/'M10', '216').
card_flavor_text('ornithopter'/'M10', 'Regardless of the century, plane, or species, developing artificers never fail to invent the ornithopter.').
card_multiverse_id('ornithopter'/'M10', '191337').

card_in_set('overrun', 'M10').
card_original_type('overrun'/'M10', 'Sorcery').
card_original_text('overrun'/'M10', 'Creatures you control get +3/+3 and gain trample until end of turn. (If a creature you control would deal enough damage to its blockers to destroy them, you may have it deal the rest of its damage to defending player or planeswalker.)').
card_image_name('overrun'/'M10', 'overrun').
card_uid('overrun'/'M10', 'M10:Overrun:overrun').
card_rarity('overrun'/'M10', 'Uncommon').
card_artist('overrun'/'M10', 'Carl Critchlow').
card_number('overrun'/'M10', '198').
card_multiverse_id('overrun'/'M10', '189906').

card_in_set('pacifism', 'M10').
card_original_type('pacifism'/'M10', 'Enchantment — Aura').
card_original_text('pacifism'/'M10', 'Enchant creature\nEnchanted creature can\'t attack or block.').
card_image_name('pacifism'/'M10', 'pacifism').
card_uid('pacifism'/'M10', 'M10:Pacifism:pacifism').
card_rarity('pacifism'/'M10', 'Common').
card_artist('pacifism'/'M10', 'Robert Bliss').
card_number('pacifism'/'M10', '22').
card_flavor_text('pacifism'/'M10', 'For the first time in his life, Grakk felt a little warm and fuzzy inside.').
card_multiverse_id('pacifism'/'M10', '190574').

card_in_set('palace guard', 'M10').
card_original_type('palace guard'/'M10', 'Creature — Human Soldier').
card_original_text('palace guard'/'M10', 'Palace Guard can block any number of creatures.').
card_first_print('palace guard', 'M10').
card_image_name('palace guard'/'M10', 'palace guard').
card_uid('palace guard'/'M10', 'M10:Palace Guard:palace guard').
card_rarity('palace guard'/'M10', 'Common').
card_artist('palace guard'/'M10', 'Volkan Baga').
card_number('palace guard'/'M10', '23').
card_flavor_text('palace guard'/'M10', '\"I\'m not nearly as bothered by being outnumbered as I am by their foul stench.\"').
card_multiverse_id('palace guard'/'M10', '190534').

card_in_set('panic attack', 'M10').
card_original_type('panic attack'/'M10', 'Sorcery').
card_original_text('panic attack'/'M10', 'Up to three target creatures can\'t block this turn.').
card_image_name('panic attack'/'M10', 'panic attack').
card_uid('panic attack'/'M10', 'M10:Panic Attack:panic attack').
card_rarity('panic attack'/'M10', 'Common').
card_artist('panic attack'/'M10', 'Izzy').
card_number('panic attack'/'M10', '150').
card_flavor_text('panic attack'/'M10', '\"What do they see in the throes of panic? I\'m not sure I want to know.\"\n—Kezim, prodigal pyromancer').
card_multiverse_id('panic attack'/'M10', '191081').

card_in_set('phantom warrior', 'M10').
card_original_type('phantom warrior'/'M10', 'Creature — Illusion Warrior').
card_original_text('phantom warrior'/'M10', 'Phantom Warrior is unblockable.').
card_image_name('phantom warrior'/'M10', 'phantom warrior').
card_uid('phantom warrior'/'M10', 'M10:Phantom Warrior:phantom warrior').
card_rarity('phantom warrior'/'M10', 'Uncommon').
card_artist('phantom warrior'/'M10', 'Greg Staples').
card_number('phantom warrior'/'M10', '66').
card_flavor_text('phantom warrior'/'M10', '\"The construction of a defense is not accomplished by adding bricks.\"\n—Jace Beleren').
card_multiverse_id('phantom warrior'/'M10', '189892').

card_in_set('pithing needle', 'M10').
card_original_type('pithing needle'/'M10', 'Artifact').
card_original_text('pithing needle'/'M10', 'As Pithing Needle enters the battlefield, name a card.\nActivated abilities of sources with the chosen name can\'t be activated unless they\'re mana abilities.').
card_image_name('pithing needle'/'M10', 'pithing needle').
card_uid('pithing needle'/'M10', 'M10:Pithing Needle:pithing needle').
card_rarity('pithing needle'/'M10', 'Rare').
card_artist('pithing needle'/'M10', 'Pete Venters').
card_number('pithing needle'/'M10', '217').
card_flavor_text('pithing needle'/'M10', 'It disables with pinpoint accuracy.').
card_multiverse_id('pithing needle'/'M10', '191592').

card_in_set('plains', 'M10').
card_original_type('plains'/'M10', 'Basic Land — Plains').
card_original_text('plains'/'M10', 'W').
card_image_name('plains'/'M10', 'plains1').
card_uid('plains'/'M10', 'M10:Plains:plains1').
card_rarity('plains'/'M10', 'Basic Land').
card_artist('plains'/'M10', 'Rob Alexander').
card_number('plains'/'M10', '230').
card_multiverse_id('plains'/'M10', '191395').

card_in_set('plains', 'M10').
card_original_type('plains'/'M10', 'Basic Land — Plains').
card_original_text('plains'/'M10', 'W').
card_image_name('plains'/'M10', 'plains2').
card_uid('plains'/'M10', 'M10:Plains:plains2').
card_rarity('plains'/'M10', 'Basic Land').
card_artist('plains'/'M10', 'John Avon').
card_number('plains'/'M10', '231').
card_multiverse_id('plains'/'M10', '191382').

card_in_set('plains', 'M10').
card_original_type('plains'/'M10', 'Basic Land — Plains').
card_original_text('plains'/'M10', 'W').
card_image_name('plains'/'M10', 'plains3').
card_uid('plains'/'M10', 'M10:Plains:plains3').
card_rarity('plains'/'M10', 'Basic Land').
card_artist('plains'/'M10', 'Don Hazeltine').
card_number('plains'/'M10', '232').
card_multiverse_id('plains'/'M10', '191385').

card_in_set('plains', 'M10').
card_original_type('plains'/'M10', 'Basic Land — Plains').
card_original_text('plains'/'M10', 'W').
card_image_name('plains'/'M10', 'plains4').
card_uid('plains'/'M10', 'M10:Plains:plains4').
card_rarity('plains'/'M10', 'Basic Land').
card_artist('plains'/'M10', 'Ryan Pancoast').
card_number('plains'/'M10', '233').
card_multiverse_id('plains'/'M10', '191396').

card_in_set('planar cleansing', 'M10').
card_original_type('planar cleansing'/'M10', 'Sorcery').
card_original_text('planar cleansing'/'M10', 'Destroy all nonland permanents.').
card_first_print('planar cleansing', 'M10').
card_image_name('planar cleansing'/'M10', 'planar cleansing').
card_uid('planar cleansing'/'M10', 'M10:Planar Cleansing:planar cleansing').
card_rarity('planar cleansing'/'M10', 'Rare').
card_artist('planar cleansing'/'M10', 'Michael Komarck').
card_number('planar cleansing'/'M10', '24').
card_multiverse_id('planar cleansing'/'M10', '191599').

card_in_set('platinum angel', 'M10').
card_original_type('platinum angel'/'M10', 'Artifact Creature — Angel').
card_original_text('platinum angel'/'M10', 'Flying\nYou can\'t lose the game and your opponents can\'t win the game.').
card_image_name('platinum angel'/'M10', 'platinum angel').
card_uid('platinum angel'/'M10', 'M10:Platinum Angel:platinum angel').
card_rarity('platinum angel'/'M10', 'Mythic Rare').
card_artist('platinum angel'/'M10', 'Brom').
card_number('platinum angel'/'M10', '218').
card_flavor_text('platinum angel'/'M10', 'In its heart lies the secret of immortality.').
card_multiverse_id('platinum angel'/'M10', '191313').

card_in_set('polymorph', 'M10').
card_original_type('polymorph'/'M10', 'Sorcery').
card_original_text('polymorph'/'M10', 'Destroy target creature. It can\'t be regenerated. Its controller reveals cards from the top of his or her library until he or she reveals a creature card. The player puts that card onto the battlefield, then shuffles all other cards revealed this way into his or her library.').
card_image_name('polymorph'/'M10', 'polymorph').
card_uid('polymorph'/'M10', 'M10:Polymorph:polymorph').
card_rarity('polymorph'/'M10', 'Rare').
card_artist('polymorph'/'M10', 'Robert Bliss').
card_number('polymorph'/'M10', '67').
card_multiverse_id('polymorph'/'M10', '191380').

card_in_set('ponder', 'M10').
card_original_type('ponder'/'M10', 'Sorcery').
card_original_text('ponder'/'M10', 'Look at the top three cards of your library, then put them back in any order. You may shuffle your library.\nDraw a card.').
card_image_name('ponder'/'M10', 'ponder').
card_uid('ponder'/'M10', 'M10:Ponder:ponder').
card_rarity('ponder'/'M10', 'Common').
card_artist('ponder'/'M10', 'Dan Scott').
card_number('ponder'/'M10', '68').
card_flavor_text('ponder'/'M10', 'Tomorrow belongs to those who prepare for it today.').
card_multiverse_id('ponder'/'M10', '190159').

card_in_set('prized unicorn', 'M10').
card_original_type('prized unicorn'/'M10', 'Creature — Unicorn').
card_original_text('prized unicorn'/'M10', 'All creatures able to block Prized Unicorn do so.').
card_first_print('prized unicorn', 'M10').
card_image_name('prized unicorn'/'M10', 'prized unicorn').
card_uid('prized unicorn'/'M10', 'M10:Prized Unicorn:prized unicorn').
card_rarity('prized unicorn'/'M10', 'Uncommon').
card_artist('prized unicorn'/'M10', 'Sam Wood').
card_number('prized unicorn'/'M10', '199').
card_flavor_text('prized unicorn'/'M10', '\"The desire for its magic horn inspires such bloodthirsty greed that all who see the unicorn will kill to possess it.\"\n—Dionus, elvish archdruid').
card_multiverse_id('prized unicorn'/'M10', '189900').

card_in_set('prodigal pyromancer', 'M10').
card_original_type('prodigal pyromancer'/'M10', 'Creature — Human Wizard').
card_original_text('prodigal pyromancer'/'M10', '{T}: Prodigal Pyromancer deals 1 damage to target creature or player.').
card_image_name('prodigal pyromancer'/'M10', 'prodigal pyromancer').
card_uid('prodigal pyromancer'/'M10', 'M10:Prodigal Pyromancer:prodigal pyromancer').
card_rarity('prodigal pyromancer'/'M10', 'Uncommon').
card_artist('prodigal pyromancer'/'M10', 'Jeremy Jarvis').
card_number('prodigal pyromancer'/'M10', '151').
card_flavor_text('prodigal pyromancer'/'M10', '\"What am I looking at? Ashes, dead man.\"').
card_multiverse_id('prodigal pyromancer'/'M10', '191102').

card_in_set('protean hydra', 'M10').
card_original_type('protean hydra'/'M10', 'Creature — Hydra').
card_original_text('protean hydra'/'M10', 'Protean Hydra enters the battlefield with X +1/+1 counters on it.\nIf damage would be dealt to Protean Hydra, prevent that damage and remove that many +1/+1 counters from it.\nWhenever a +1/+1 counter is removed from Protean Hydra, put two +1/+1 counters on it at the beginning of the end step.').
card_first_print('protean hydra', 'M10').
card_image_name('protean hydra'/'M10', 'protean hydra').
card_uid('protean hydra'/'M10', 'M10:Protean Hydra:protean hydra').
card_rarity('protean hydra'/'M10', 'Mythic Rare').
card_artist('protean hydra'/'M10', 'Jim Murray').
card_number('protean hydra'/'M10', '200').
card_multiverse_id('protean hydra'/'M10', '193753').

card_in_set('pyroclasm', 'M10').
card_original_type('pyroclasm'/'M10', 'Sorcery').
card_original_text('pyroclasm'/'M10', 'Pyroclasm deals 2 damage to each creature.').
card_image_name('pyroclasm'/'M10', 'pyroclasm').
card_uid('pyroclasm'/'M10', 'M10:Pyroclasm:pyroclasm').
card_rarity('pyroclasm'/'M10', 'Uncommon').
card_artist('pyroclasm'/'M10', 'John Avon').
card_number('pyroclasm'/'M10', '152').
card_flavor_text('pyroclasm'/'M10', '\"Who\'d want to ignite things one at a time?\"\n—Chandra Nalaar').
card_multiverse_id('pyroclasm'/'M10', '194425').

card_in_set('raging goblin', 'M10').
card_original_type('raging goblin'/'M10', 'Creature — Goblin Berserker').
card_original_text('raging goblin'/'M10', 'Haste (This creature can attack and {T} as soon as it comes under your control.)').
card_image_name('raging goblin'/'M10', 'raging goblin').
card_uid('raging goblin'/'M10', 'M10:Raging Goblin:raging goblin').
card_rarity('raging goblin'/'M10', 'Common').
card_artist('raging goblin'/'M10', 'Jeff Miracola').
card_number('raging goblin'/'M10', '153').
card_flavor_text('raging goblin'/'M10', 'He raged at the world, at his family, at his life. But mostly he just raged.').
card_multiverse_id('raging goblin'/'M10', '191056').

card_in_set('rampant growth', 'M10').
card_original_type('rampant growth'/'M10', 'Sorcery').
card_original_text('rampant growth'/'M10', 'Search your library for a basic land card and put that card onto the battlefield tapped. Then shuffle your library.').
card_image_name('rampant growth'/'M10', 'rampant growth').
card_uid('rampant growth'/'M10', 'M10:Rampant Growth:rampant growth').
card_rarity('rampant growth'/'M10', 'Common').
card_artist('rampant growth'/'M10', 'Steven Belledin').
card_number('rampant growth'/'M10', '201').
card_flavor_text('rampant growth'/'M10', 'Nature grows solutions to its problems.').
card_multiverse_id('rampant growth'/'M10', '189887').

card_in_set('razorfoot griffin', 'M10').
card_original_type('razorfoot griffin'/'M10', 'Creature — Griffin').
card_original_text('razorfoot griffin'/'M10', 'Flying\nFirst strike (This creature deals combat damage before creatures without first strike.)').
card_image_name('razorfoot griffin'/'M10', 'razorfoot griffin').
card_uid('razorfoot griffin'/'M10', 'M10:Razorfoot Griffin:razorfoot griffin').
card_rarity('razorfoot griffin'/'M10', 'Common').
card_artist('razorfoot griffin'/'M10', 'Ben Thompson').
card_number('razorfoot griffin'/'M10', '25').
card_flavor_text('razorfoot griffin'/'M10', 'Like a meteor, it strikes from above without warning. Unlike a meteor, it then carries you off and eats you.').
card_multiverse_id('razorfoot griffin'/'M10', '189891').

card_in_set('regenerate', 'M10').
card_original_type('regenerate'/'M10', 'Instant').
card_original_text('regenerate'/'M10', 'Regenerate target creature. (The next time that creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_first_print('regenerate', 'M10').
card_image_name('regenerate'/'M10', 'regenerate').
card_uid('regenerate'/'M10', 'M10:Regenerate:regenerate').
card_rarity('regenerate'/'M10', 'Common').
card_artist('regenerate'/'M10', 'Rebecca Guay').
card_number('regenerate'/'M10', '202').
card_flavor_text('regenerate'/'M10', '\"This wound shall be like the chills of winter: swiftly forgotten when spring shoots rise.\"').
card_multiverse_id('regenerate'/'M10', '193739').

card_in_set('relentless rats', 'M10').
card_original_type('relentless rats'/'M10', 'Creature — Rat').
card_original_text('relentless rats'/'M10', 'Relentless Rats gets +1/+1 for each other creature on the battlefield named Relentless Rats.\nA deck can have any number of cards named Relentless Rats.').
card_image_name('relentless rats'/'M10', 'relentless rats').
card_uid('relentless rats'/'M10', 'M10:Relentless Rats:relentless rats').
card_rarity('relentless rats'/'M10', 'Uncommon').
card_artist('relentless rats'/'M10', 'Thomas M. Baxa').
card_number('relentless rats'/'M10', '108').
card_multiverse_id('relentless rats'/'M10', '191336').

card_in_set('rhox pikemaster', 'M10').
card_original_type('rhox pikemaster'/'M10', 'Creature — Rhino Soldier').
card_original_text('rhox pikemaster'/'M10', 'First strike (This creature deals combat damage before creatures without first strike.)\nOther Soldier creatures you control have first strike.').
card_first_print('rhox pikemaster', 'M10').
card_image_name('rhox pikemaster'/'M10', 'rhox pikemaster').
card_uid('rhox pikemaster'/'M10', 'M10:Rhox Pikemaster:rhox pikemaster').
card_rarity('rhox pikemaster'/'M10', 'Uncommon').
card_artist('rhox pikemaster'/'M10', 'Steve Prescott').
card_number('rhox pikemaster'/'M10', '26').
card_flavor_text('rhox pikemaster'/'M10', 'When the rhoxes charge, everyone else knows which way to run.').
card_multiverse_id('rhox pikemaster'/'M10', '190576').

card_in_set('righteousness', 'M10').
card_original_type('righteousness'/'M10', 'Instant').
card_original_text('righteousness'/'M10', 'Target blocking creature gets +7/+7 until end of turn.').
card_image_name('righteousness'/'M10', 'righteousness').
card_uid('righteousness'/'M10', 'M10:Righteousness:righteousness').
card_rarity('righteousness'/'M10', 'Uncommon').
card_artist('righteousness'/'M10', 'Wayne England').
card_number('righteousness'/'M10', '27').
card_flavor_text('righteousness'/'M10', 'Sometimes the greatest strength is the strength of conviction.').
card_multiverse_id('righteousness'/'M10', '191341').

card_in_set('rise from the grave', 'M10').
card_original_type('rise from the grave'/'M10', 'Sorcery').
card_original_text('rise from the grave'/'M10', 'Put target creature card in a graveyard onto the battlefield under your control. That creature is a black Zombie in addition to its other colors and types.').
card_image_name('rise from the grave'/'M10', 'rise from the grave').
card_uid('rise from the grave'/'M10', 'M10:Rise from the Grave:rise from the grave').
card_rarity('rise from the grave'/'M10', 'Uncommon').
card_artist('rise from the grave'/'M10', 'Vance Kovacs').
card_number('rise from the grave'/'M10', '109').
card_flavor_text('rise from the grave'/'M10', '\"Death is no excuse for disobedience.\"\n—Liliana Vess').
card_multiverse_id('rise from the grave'/'M10', '190192').

card_in_set('rod of ruin', 'M10').
card_original_type('rod of ruin'/'M10', 'Artifact').
card_original_text('rod of ruin'/'M10', '{3}, {T}: Rod of Ruin deals 1 damage to target creature or player.').
card_image_name('rod of ruin'/'M10', 'rod of ruin').
card_uid('rod of ruin'/'M10', 'M10:Rod of Ruin:rod of ruin').
card_rarity('rod of ruin'/'M10', 'Uncommon').
card_artist('rod of ruin'/'M10', 'Mark Zug').
card_number('rod of ruin'/'M10', '219').
card_flavor_text('rod of ruin'/'M10', '\"My masterpiece is complete—and so shall be my triumph.\"\n—Saldrath, master artificer').
card_multiverse_id('rod of ruin'/'M10', '191339').

card_in_set('rootbound crag', 'M10').
card_original_type('rootbound crag'/'M10', 'Land').
card_original_text('rootbound crag'/'M10', 'Rootbound Crag enters the battlefield tapped unless you control a Mountain or a Forest.\n{T}: Add {R} or {G} to your mana pool.').
card_first_print('rootbound crag', 'M10').
card_image_name('rootbound crag'/'M10', 'rootbound crag').
card_uid('rootbound crag'/'M10', 'M10:Rootbound Crag:rootbound crag').
card_rarity('rootbound crag'/'M10', 'Rare').
card_artist('rootbound crag'/'M10', 'Matt Stewart').
card_number('rootbound crag'/'M10', '227').
card_multiverse_id('rootbound crag'/'M10', '191057').

card_in_set('royal assassin', 'M10').
card_original_type('royal assassin'/'M10', 'Creature — Human Assassin').
card_original_text('royal assassin'/'M10', '{T}: Destroy target tapped creature.').
card_image_name('royal assassin'/'M10', 'royal assassin').
card_uid('royal assassin'/'M10', 'M10:Royal Assassin:royal assassin').
card_rarity('royal assassin'/'M10', 'Rare').
card_artist('royal assassin'/'M10', 'Mark Zug').
card_number('royal assassin'/'M10', '110').
card_flavor_text('royal assassin'/'M10', 'Trained in the arts of stealth, royal assassins choose their victims carefully, relying on timing and precision rather than brute force.').
card_multiverse_id('royal assassin'/'M10', '191318').

card_in_set('runeclaw bear', 'M10').
card_original_type('runeclaw bear'/'M10', 'Creature — Bear').
card_original_text('runeclaw bear'/'M10', '').
card_first_print('runeclaw bear', 'M10').
card_image_name('runeclaw bear'/'M10', 'runeclaw bear').
card_uid('runeclaw bear'/'M10', 'M10:Runeclaw Bear:runeclaw bear').
card_rarity('runeclaw bear'/'M10', 'Common').
card_artist('runeclaw bear'/'M10', 'Jesper Ejsing').
card_number('runeclaw bear'/'M10', '203').
card_flavor_text('runeclaw bear'/'M10', 'Bears aren\'t always as strong and as mean as you imagine. Some are even stronger and meaner.').
card_multiverse_id('runeclaw bear'/'M10', '189888').

card_in_set('safe passage', 'M10').
card_original_type('safe passage'/'M10', 'Instant').
card_original_text('safe passage'/'M10', 'Prevent all damage that would be dealt to you and creatures you control this turn.').
card_first_print('safe passage', 'M10').
card_image_name('safe passage'/'M10', 'safe passage').
card_uid('safe passage'/'M10', 'M10:Safe Passage:safe passage').
card_rarity('safe passage'/'M10', 'Common').
card_artist('safe passage'/'M10', 'Christopher Moeller').
card_number('safe passage'/'M10', '28').
card_flavor_text('safe passage'/'M10', 'With one flap of her wings, the angel beat back the fires of war.').
card_multiverse_id('safe passage'/'M10', '190176').

card_in_set('sage owl', 'M10').
card_original_type('sage owl'/'M10', 'Creature — Bird').
card_original_text('sage owl'/'M10', 'Flying\nWhen Sage Owl enters the battlefield, look at the top four cards of your library, then put them back in any order.').
card_image_name('sage owl'/'M10', 'sage owl').
card_uid('sage owl'/'M10', 'M10:Sage Owl:sage owl').
card_rarity('sage owl'/'M10', 'Common').
card_artist('sage owl'/'M10', 'Cyril Van Der Haegen').
card_number('sage owl'/'M10', '69').
card_flavor_text('sage owl'/'M10', 'Some owls house the souls of archmages of ages past.').
card_multiverse_id('sage owl'/'M10', '190188').

card_in_set('sanguine bond', 'M10').
card_original_type('sanguine bond'/'M10', 'Enchantment').
card_original_text('sanguine bond'/'M10', 'Whenever you gain life, target opponent loses that much life.').
card_first_print('sanguine bond', 'M10').
card_image_name('sanguine bond'/'M10', 'sanguine bond').
card_uid('sanguine bond'/'M10', 'M10:Sanguine Bond:sanguine bond').
card_rarity('sanguine bond'/'M10', 'Rare').
card_artist('sanguine bond'/'M10', 'Jaime Jones').
card_number('sanguine bond'/'M10', '111').
card_flavor_text('sanguine bond'/'M10', '\"Blood is constant. Every drop I drink, someone must bleed.\"\n—Vradeen, vampire nocturnus').
card_multiverse_id('sanguine bond'/'M10', '190190').

card_in_set('seismic strike', 'M10').
card_original_type('seismic strike'/'M10', 'Instant').
card_original_text('seismic strike'/'M10', 'Seismic Strike deals damage to target creature equal to the number of Mountains you control.').
card_first_print('seismic strike', 'M10').
card_image_name('seismic strike'/'M10', 'seismic strike').
card_uid('seismic strike'/'M10', 'M10:Seismic Strike:seismic strike').
card_rarity('seismic strike'/'M10', 'Common').
card_artist('seismic strike'/'M10', 'Christopher Moeller').
card_number('seismic strike'/'M10', '154').
card_flavor_text('seismic strike'/'M10', '\"Life up here is simple. Adapt to the ways of the mountains and they will reward you. Fight them and they will end you.\"\n—Kezim, prodigal pyromancer').
card_multiverse_id('seismic strike'/'M10', '191095').

card_in_set('serpent of the endless sea', 'M10').
card_original_type('serpent of the endless sea'/'M10', 'Creature — Serpent').
card_original_text('serpent of the endless sea'/'M10', 'Serpent of the Endless Sea\'s power and toughness are each equal to the number of Islands you control.\nSerpent of the Endless Sea can\'t attack unless defending player controls an Island.').
card_first_print('serpent of the endless sea', 'M10').
card_image_name('serpent of the endless sea'/'M10', 'serpent of the endless sea').
card_uid('serpent of the endless sea'/'M10', 'M10:Serpent of the Endless Sea:serpent of the endless sea').
card_rarity('serpent of the endless sea'/'M10', 'Common').
card_artist('serpent of the endless sea'/'M10', 'Kieran Yanner').
card_number('serpent of the endless sea'/'M10', '70').
card_multiverse_id('serpent of the endless sea'/'M10', '190169').

card_in_set('serra angel', 'M10').
card_original_type('serra angel'/'M10', 'Creature — Angel').
card_original_text('serra angel'/'M10', 'Flying\nVigilance (Attacking doesn\'t cause this creature to tap.)').
card_image_name('serra angel'/'M10', 'serra angel').
card_uid('serra angel'/'M10', 'M10:Serra Angel:serra angel').
card_rarity('serra angel'/'M10', 'Uncommon').
card_artist('serra angel'/'M10', 'Greg Staples').
card_number('serra angel'/'M10', '29').
card_flavor_text('serra angel'/'M10', 'Her sword sings more beautifully than any choir.').
card_multiverse_id('serra angel'/'M10', '193767').

card_in_set('shatter', 'M10').
card_original_type('shatter'/'M10', 'Instant').
card_original_text('shatter'/'M10', 'Destroy target artifact.').
card_image_name('shatter'/'M10', 'shatter').
card_uid('shatter'/'M10', 'M10:Shatter:shatter').
card_rarity('shatter'/'M10', 'Common').
card_artist('shatter'/'M10', 'Tim Hildebrandt').
card_number('shatter'/'M10', '155').
card_flavor_text('shatter'/'M10', 'Days of planning. Weeks of building. Months of perfecting. Seconds of smashing.').
card_multiverse_id('shatter'/'M10', '191086').

card_in_set('shivan dragon', 'M10').
card_original_type('shivan dragon'/'M10', 'Creature — Dragon').
card_original_text('shivan dragon'/'M10', 'Flying\n{R}: Shivan Dragon gets +1/+0 until end of turn.').
card_image_name('shivan dragon'/'M10', 'shivan dragon').
card_uid('shivan dragon'/'M10', 'M10:Shivan Dragon:shivan dragon').
card_rarity('shivan dragon'/'M10', 'Rare').
card_artist('shivan dragon'/'M10', 'Donato Giancola').
card_number('shivan dragon'/'M10', '156').
card_flavor_text('shivan dragon'/'M10', 'The undisputed master of the mountains of Shiv.').
card_multiverse_id('shivan dragon'/'M10', '191320').

card_in_set('siege mastodon', 'M10').
card_original_type('siege mastodon'/'M10', 'Creature — Elephant').
card_original_text('siege mastodon'/'M10', '').
card_first_print('siege mastodon', 'M10').
card_image_name('siege mastodon'/'M10', 'siege mastodon').
card_uid('siege mastodon'/'M10', 'M10:Siege Mastodon:siege mastodon').
card_rarity('siege mastodon'/'M10', 'Common').
card_artist('siege mastodon'/'M10', 'Matt Cavotta').
card_number('siege mastodon'/'M10', '30').
card_flavor_text('siege mastodon'/'M10', '\"Defending the citadel is easy. I just take these big fellows outside the main gate and let the enemy take a good long look.\"\n—Mastodon keeper').
card_multiverse_id('siege mastodon'/'M10', '193931').

card_in_set('siege-gang commander', 'M10').
card_original_type('siege-gang commander'/'M10', 'Creature — Goblin').
card_original_text('siege-gang commander'/'M10', 'When Siege-Gang Commander enters the battlefield, put three 1/1 red Goblin creature tokens onto the battlefield.\n{1}{R}, Sacrifice a Goblin: Siege-Gang Commander deals 2 damage to target creature or player.').
card_image_name('siege-gang commander'/'M10', 'siege-gang commander').
card_uid('siege-gang commander'/'M10', 'M10:Siege-Gang Commander:siege-gang commander').
card_rarity('siege-gang commander'/'M10', 'Rare').
card_artist('siege-gang commander'/'M10', 'Christopher Moeller').
card_number('siege-gang commander'/'M10', '157').
card_multiverse_id('siege-gang commander'/'M10', '193751').

card_in_set('sign in blood', 'M10').
card_original_type('sign in blood'/'M10', 'Sorcery').
card_original_text('sign in blood'/'M10', 'Target player draws two cards and loses 2 life.').
card_image_name('sign in blood'/'M10', 'sign in blood').
card_uid('sign in blood'/'M10', 'M10:Sign in Blood:sign in blood').
card_rarity('sign in blood'/'M10', 'Common').
card_artist('sign in blood'/'M10', 'Howard Lyon').
card_number('sign in blood'/'M10', '112').
card_flavor_text('sign in blood'/'M10', '\"You know I accept only one currency here, and yet you have sought me out. Why now do you hesitate?\"\n—Xathrid demon').
card_multiverse_id('sign in blood'/'M10', '190537').

card_in_set('silence', 'M10').
card_original_type('silence'/'M10', 'Instant').
card_original_text('silence'/'M10', 'Your opponents can\'t cast spells this turn. (Spells cast before this resolves are unaffected.)').
card_first_print('silence', 'M10').
card_image_name('silence'/'M10', 'silence').
card_uid('silence'/'M10', 'M10:Silence:silence').
card_rarity('silence'/'M10', 'Rare').
card_artist('silence'/'M10', 'Wayne Reynolds').
card_number('silence'/'M10', '31').
card_flavor_text('silence'/'M10', '\"All the wizardry in the world has to pass through one small and easily sealed door.\"\n—Ajani Goldmane').
card_multiverse_id('silence'/'M10', '191083').

card_in_set('silvercoat lion', 'M10').
card_original_type('silvercoat lion'/'M10', 'Creature — Cat').
card_original_text('silvercoat lion'/'M10', '').
card_first_print('silvercoat lion', 'M10').
card_image_name('silvercoat lion'/'M10', 'silvercoat lion').
card_uid('silvercoat lion'/'M10', 'M10:Silvercoat Lion:silvercoat lion').
card_rarity('silvercoat lion'/'M10', 'Common').
card_artist('silvercoat lion'/'M10', 'Terese Nielsen').
card_number('silvercoat lion'/'M10', '32').
card_flavor_text('silvercoat lion'/'M10', 'Hunters of every species on the savannah, silvercoats disdain camouflage in favor of total dominance of the food chain.').
card_multiverse_id('silvercoat lion'/'M10', '193750').

card_in_set('sleep', 'M10').
card_original_type('sleep'/'M10', 'Sorcery').
card_original_text('sleep'/'M10', 'Tap all creatures target player controls. Those creatures don\'t untap during that player\'s next untap step.').
card_first_print('sleep', 'M10').
card_image_name('sleep'/'M10', 'sleep').
card_uid('sleep'/'M10', 'M10:Sleep:sleep').
card_rarity('sleep'/'M10', 'Uncommon').
card_artist('sleep'/'M10', 'Chris Rahn').
card_number('sleep'/'M10', '71').
card_flavor_text('sleep'/'M10', '\"I give them dreams so wondrous that they hesitate to return to the world of the conscious.\"\n—Garild, merfolk mage').
card_multiverse_id('sleep'/'M10', '190186').

card_in_set('snapping drake', 'M10').
card_original_type('snapping drake'/'M10', 'Creature — Drake').
card_original_text('snapping drake'/'M10', 'Flying').
card_image_name('snapping drake'/'M10', 'snapping drake').
card_uid('snapping drake'/'M10', 'M10:Snapping Drake:snapping drake').
card_rarity('snapping drake'/'M10', 'Common').
card_artist('snapping drake'/'M10', 'Todd Lockwood').
card_number('snapping drake'/'M10', '72').
card_flavor_text('snapping drake'/'M10', 'Foul-tempered, poorly trained, and mule-stubborn, the drake is the perfect test of the master\'s will.').
card_multiverse_id('snapping drake'/'M10', '190163').

card_in_set('solemn offering', 'M10').
card_original_type('solemn offering'/'M10', 'Sorcery').
card_original_text('solemn offering'/'M10', 'Destroy target artifact or enchantment. You gain 4 life.').
card_first_print('solemn offering', 'M10').
card_image_name('solemn offering'/'M10', 'solemn offering').
card_uid('solemn offering'/'M10', 'M10:Solemn Offering:solemn offering').
card_rarity('solemn offering'/'M10', 'Common').
card_artist('solemn offering'/'M10', 'Sam Wood').
card_number('solemn offering'/'M10', '33').
card_flavor_text('solemn offering'/'M10', '\"A relic donation is suggested.\"\n\"The suggestion is mandatory.\"\n—Temple signs').
card_multiverse_id('solemn offering'/'M10', '189875').

card_in_set('soul bleed', 'M10').
card_original_type('soul bleed'/'M10', 'Enchantment — Aura').
card_original_text('soul bleed'/'M10', 'Enchant creature\nAt the beginning of the upkeep of enchanted creature\'s controller, that player loses 1 life.').
card_first_print('soul bleed', 'M10').
card_image_name('soul bleed'/'M10', 'soul bleed').
card_uid('soul bleed'/'M10', 'M10:Soul Bleed:soul bleed').
card_rarity('soul bleed'/'M10', 'Common').
card_artist('soul bleed'/'M10', 'Zoltan Boros & Gabor Szikszai').
card_number('soul bleed'/'M10', '113').
card_flavor_text('soul bleed'/'M10', 'Whether the end comes swift or slow, it always comes too soon.').
card_multiverse_id('soul bleed'/'M10', '190557').

card_in_set('soul warden', 'M10').
card_original_type('soul warden'/'M10', 'Creature — Human Cleric').
card_original_text('soul warden'/'M10', 'Whenever another creature enters the battlefield, you gain 1 life.').
card_image_name('soul warden'/'M10', 'soul warden').
card_uid('soul warden'/'M10', 'M10:Soul Warden:soul warden').
card_rarity('soul warden'/'M10', 'Common').
card_artist('soul warden'/'M10', 'Randy Gallegos').
card_number('soul warden'/'M10', '34').
card_flavor_text('soul warden'/'M10', '\"One does not question the size or shape of the grains of sand in an hourglass. Nor do I question the temperament of the souls under my guidance.\"').
card_multiverse_id('soul warden'/'M10', '190571').

card_in_set('sparkmage apprentice', 'M10').
card_original_type('sparkmage apprentice'/'M10', 'Creature — Human Wizard').
card_original_text('sparkmage apprentice'/'M10', 'When Sparkmage Apprentice enters the battlefield, it deals 1 damage to target creature or player.').
card_image_name('sparkmage apprentice'/'M10', 'sparkmage apprentice').
card_uid('sparkmage apprentice'/'M10', 'M10:Sparkmage Apprentice:sparkmage apprentice').
card_rarity('sparkmage apprentice'/'M10', 'Common').
card_artist('sparkmage apprentice'/'M10', 'Jaime Jones').
card_number('sparkmage apprentice'/'M10', '158').
card_flavor_text('sparkmage apprentice'/'M10', 'Sparkmages are performance artists of sorts, but pain is the price of admission.').
card_multiverse_id('sparkmage apprentice'/'M10', '191053').

card_in_set('spellbook', 'M10').
card_original_type('spellbook'/'M10', 'Artifact').
card_original_text('spellbook'/'M10', 'You have no maximum hand size.').
card_image_name('spellbook'/'M10', 'spellbook').
card_uid('spellbook'/'M10', 'M10:Spellbook:spellbook').
card_rarity('spellbook'/'M10', 'Uncommon').
card_artist('spellbook'/'M10', 'Andrew Goldhawk').
card_number('spellbook'/'M10', '220').
card_flavor_text('spellbook'/'M10', '\"Everything the wise woman learned she wrote in a book, and when the pages were black with ink, she took white ink and began again.\"\n—Karn, silver golem').
card_multiverse_id('spellbook'/'M10', '191338').

card_in_set('sphinx ambassador', 'M10').
card_original_type('sphinx ambassador'/'M10', 'Creature — Sphinx').
card_original_text('sphinx ambassador'/'M10', 'Flying\nWhenever Sphinx Ambassador deals combat damage to a player, search that player\'s library for a card, then that player names a card. If you searched for a creature card that isn\'t the named card, you may put it onto the battlefield under your control. Then that player shuffles his or her library.').
card_first_print('sphinx ambassador', 'M10').
card_image_name('sphinx ambassador'/'M10', 'sphinx ambassador').
card_uid('sphinx ambassador'/'M10', 'M10:Sphinx Ambassador:sphinx ambassador').
card_rarity('sphinx ambassador'/'M10', 'Mythic Rare').
card_artist('sphinx ambassador'/'M10', 'Jim Murray').
card_number('sphinx ambassador'/'M10', '73').
card_multiverse_id('sphinx ambassador'/'M10', '191098').

card_in_set('stampeding rhino', 'M10').
card_original_type('stampeding rhino'/'M10', 'Creature — Rhino').
card_original_text('stampeding rhino'/'M10', 'Trample (If this creature would deal enough damage to its blockers to destroy them, you may have it deal the rest of its damage to defending player or planeswalker.)').
card_first_print('stampeding rhino', 'M10').
card_image_name('stampeding rhino'/'M10', 'stampeding rhino').
card_uid('stampeding rhino'/'M10', 'M10:Stampeding Rhino:stampeding rhino').
card_rarity('stampeding rhino'/'M10', 'Common').
card_artist('stampeding rhino'/'M10', 'Steven Belledin').
card_number('stampeding rhino'/'M10', '204').
card_flavor_text('stampeding rhino'/'M10', '\"Sometimes I wonder if our weapons and armor are a step backward.\"\n—Soron, rhox pikemaster').
card_multiverse_id('stampeding rhino'/'M10', '189899').

card_in_set('stone giant', 'M10').
card_original_type('stone giant'/'M10', 'Creature — Giant').
card_original_text('stone giant'/'M10', '{T}: Target creature you control with toughness less than Stone Giant\'s power gains flying until end of turn. Destroy that creature at the beginning of the end step.').
card_image_name('stone giant'/'M10', 'stone giant').
card_uid('stone giant'/'M10', 'M10:Stone Giant:stone giant').
card_rarity('stone giant'/'M10', 'Uncommon').
card_artist('stone giant'/'M10', 'Warren Mahy').
card_number('stone giant'/'M10', '159').
card_flavor_text('stone giant'/'M10', 'A typical day for a giant. A momentous event for a goat.').
card_multiverse_id('stone giant'/'M10', '191094').

card_in_set('stormfront pegasus', 'M10').
card_original_type('stormfront pegasus'/'M10', 'Creature — Pegasus').
card_original_text('stormfront pegasus'/'M10', 'Flying').
card_first_print('stormfront pegasus', 'M10').
card_image_name('stormfront pegasus'/'M10', 'stormfront pegasus').
card_uid('stormfront pegasus'/'M10', 'M10:Stormfront Pegasus:stormfront pegasus').
card_rarity('stormfront pegasus'/'M10', 'Common').
card_artist('stormfront pegasus'/'M10', 'rk post').
card_number('stormfront pegasus'/'M10', '35').
card_flavor_text('stormfront pegasus'/'M10', '\"At summer\'s end, the pegasus herd stampedes across the sky. Their silent footfalls taunt the clouds and bid the rains to come.\"\n—Stormfront fable').
card_multiverse_id('stormfront pegasus'/'M10', '190543').

card_in_set('sunpetal grove', 'M10').
card_original_type('sunpetal grove'/'M10', 'Land').
card_original_text('sunpetal grove'/'M10', 'Sunpetal Grove enters the battlefield tapped unless you control a Forest or a Plains.\n{T}: Add {G} or {W} to your mana pool.').
card_first_print('sunpetal grove', 'M10').
card_image_name('sunpetal grove'/'M10', 'sunpetal grove').
card_uid('sunpetal grove'/'M10', 'M10:Sunpetal Grove:sunpetal grove').
card_rarity('sunpetal grove'/'M10', 'Rare').
card_artist('sunpetal grove'/'M10', 'Jason Chan').
card_number('sunpetal grove'/'M10', '228').
card_multiverse_id('sunpetal grove'/'M10', '191100').

card_in_set('swamp', 'M10').
card_original_type('swamp'/'M10', 'Basic Land — Swamp').
card_original_text('swamp'/'M10', 'B').
card_image_name('swamp'/'M10', 'swamp1').
card_uid('swamp'/'M10', 'M10:Swamp:swamp1').
card_rarity('swamp'/'M10', 'Basic Land').
card_artist('swamp'/'M10', 'Dan Frazier').
card_number('swamp'/'M10', '238').
card_multiverse_id('swamp'/'M10', '191399').

card_in_set('swamp', 'M10').
card_original_type('swamp'/'M10', 'Basic Land — Swamp').
card_original_text('swamp'/'M10', 'B').
card_image_name('swamp'/'M10', 'swamp2').
card_uid('swamp'/'M10', 'M10:Swamp:swamp2').
card_rarity('swamp'/'M10', 'Basic Land').
card_artist('swamp'/'M10', 'Alex Horley-Orlandelli').
card_number('swamp'/'M10', '239').
card_multiverse_id('swamp'/'M10', '191391').

card_in_set('swamp', 'M10').
card_original_type('swamp'/'M10', 'Basic Land — Swamp').
card_original_text('swamp'/'M10', 'B').
card_image_name('swamp'/'M10', 'swamp3').
card_uid('swamp'/'M10', 'M10:Swamp:swamp3').
card_rarity('swamp'/'M10', 'Basic Land').
card_artist('swamp'/'M10', 'Jim Pavelec').
card_number('swamp'/'M10', '240').
card_multiverse_id('swamp'/'M10', '191381').

card_in_set('swamp', 'M10').
card_original_type('swamp'/'M10', 'Basic Land — Swamp').
card_original_text('swamp'/'M10', 'B').
card_image_name('swamp'/'M10', 'swamp4').
card_uid('swamp'/'M10', 'M10:Swamp:swamp4').
card_rarity('swamp'/'M10', 'Basic Land').
card_artist('swamp'/'M10', 'Alan Pollack').
card_number('swamp'/'M10', '241').
card_multiverse_id('swamp'/'M10', '191398').

card_in_set('telepathy', 'M10').
card_original_type('telepathy'/'M10', 'Enchantment').
card_original_text('telepathy'/'M10', 'Your opponents play with their hands revealed.').
card_image_name('telepathy'/'M10', 'telepathy').
card_uid('telepathy'/'M10', 'M10:Telepathy:telepathy').
card_rarity('telepathy'/'M10', 'Uncommon').
card_artist('telepathy'/'M10', 'Matthew D. Wilson').
card_number('telepathy'/'M10', '74').
card_flavor_text('telepathy'/'M10', '\"The most disappointing thing about learning telepathy is finding out how boring people really are.\"\n—Teferi, fourth-level student').
card_multiverse_id('telepathy'/'M10', '189910').

card_in_set('tempest of light', 'M10').
card_original_type('tempest of light'/'M10', 'Instant').
card_original_text('tempest of light'/'M10', 'Destroy all enchantments.').
card_image_name('tempest of light'/'M10', 'tempest of light').
card_uid('tempest of light'/'M10', 'M10:Tempest of Light:tempest of light').
card_rarity('tempest of light'/'M10', 'Uncommon').
card_artist('tempest of light'/'M10', 'Wayne England').
card_number('tempest of light'/'M10', '36').
card_flavor_text('tempest of light'/'M10', '\"Let everything return to its true nature, so that destiny may take its course.\"').
card_multiverse_id('tempest of light'/'M10', '190564').

card_in_set('tendrils of corruption', 'M10').
card_original_type('tendrils of corruption'/'M10', 'Instant').
card_original_text('tendrils of corruption'/'M10', 'Tendrils of Corruption deals X damage to target creature and you gain X life, where X is the number of Swamps you control.').
card_image_name('tendrils of corruption'/'M10', 'tendrils of corruption').
card_uid('tendrils of corruption'/'M10', 'M10:Tendrils of Corruption:tendrils of corruption').
card_rarity('tendrils of corruption'/'M10', 'Common').
card_artist('tendrils of corruption'/'M10', 'Vance Kovacs').
card_number('tendrils of corruption'/'M10', '114').
card_flavor_text('tendrils of corruption'/'M10', 'Darkness doesn\'t always send its minions to do its dirty work.').
card_multiverse_id('tendrils of corruption'/'M10', '193744').

card_in_set('terramorphic expanse', 'M10').
card_original_type('terramorphic expanse'/'M10', 'Land').
card_original_text('terramorphic expanse'/'M10', '{T}, Sacrifice Terramorphic Expanse: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('terramorphic expanse'/'M10', 'terramorphic expanse').
card_uid('terramorphic expanse'/'M10', 'M10:Terramorphic Expanse:terramorphic expanse').
card_rarity('terramorphic expanse'/'M10', 'Common').
card_artist('terramorphic expanse'/'M10', 'Dan Scott').
card_number('terramorphic expanse'/'M10', '229').
card_multiverse_id('terramorphic expanse'/'M10', '190560').

card_in_set('time warp', 'M10').
card_original_type('time warp'/'M10', 'Sorcery').
card_original_text('time warp'/'M10', 'Target player takes an extra turn after this one.').
card_image_name('time warp'/'M10', 'time warp').
card_uid('time warp'/'M10', 'M10:Time Warp:time warp').
card_rarity('time warp'/'M10', 'Mythic Rare').
card_artist('time warp'/'M10', 'Jon Foster').
card_number('time warp'/'M10', '75').
card_flavor_text('time warp'/'M10', 'Just when you thought you\'d survived the first wave.').
card_multiverse_id('time warp'/'M10', '191379').

card_in_set('tome scour', 'M10').
card_original_type('tome scour'/'M10', 'Sorcery').
card_original_text('tome scour'/'M10', 'Target player puts the top five cards of his or her library into his or her graveyard.').
card_first_print('tome scour', 'M10').
card_image_name('tome scour'/'M10', 'tome scour').
card_uid('tome scour'/'M10', 'M10:Tome Scour:tome scour').
card_rarity('tome scour'/'M10', 'Common').
card_artist('tome scour'/'M10', 'Steven Belledin').
card_number('tome scour'/'M10', '76').
card_flavor_text('tome scour'/'M10', 'Genius is overrated, especially when it\'s someone else\'s.').
card_multiverse_id('tome scour'/'M10', '191598').

card_in_set('traumatize', 'M10').
card_original_type('traumatize'/'M10', 'Sorcery').
card_original_text('traumatize'/'M10', 'Target player puts the top half of his or her library, rounded down, into his or her graveyard.').
card_image_name('traumatize'/'M10', 'traumatize').
card_uid('traumatize'/'M10', 'M10:Traumatize:traumatize').
card_rarity('traumatize'/'M10', 'Rare').
card_artist('traumatize'/'M10', 'Greg Staples').
card_number('traumatize'/'M10', '77').
card_flavor_text('traumatize'/'M10', '\"The educated mind is heavy with lore and knowledge. It\'s also the most likely to collapse under its own weight.\"\n—Ambassador Laquatus').
card_multiverse_id('traumatize'/'M10', '190177').

card_in_set('trumpet blast', 'M10').
card_original_type('trumpet blast'/'M10', 'Instant').
card_original_text('trumpet blast'/'M10', 'Attacking creatures get +2/+0 until end of turn.').
card_image_name('trumpet blast'/'M10', 'trumpet blast').
card_uid('trumpet blast'/'M10', 'M10:Trumpet Blast:trumpet blast').
card_rarity('trumpet blast'/'M10', 'Common').
card_artist('trumpet blast'/'M10', 'Carl Critchlow').
card_number('trumpet blast'/'M10', '160').
card_flavor_text('trumpet blast'/'M10', 'Keldon warriors don\'t need signals to tell them when to attack. They need signals to tell them when to stop.').
card_multiverse_id('trumpet blast'/'M10', '191077').

card_in_set('twincast', 'M10').
card_original_type('twincast'/'M10', 'Instant').
card_original_text('twincast'/'M10', 'Copy target instant or sorcery spell. You may choose new targets for the copy.').
card_image_name('twincast'/'M10', 'twincast').
card_uid('twincast'/'M10', 'M10:Twincast:twincast').
card_rarity('twincast'/'M10', 'Rare').
card_artist('twincast'/'M10', 'Christopher Moeller').
card_number('twincast'/'M10', '78').
card_flavor_text('twincast'/'M10', 'Imitation is the most dangerous form of flattery.').
card_multiverse_id('twincast'/'M10', '191322').

card_in_set('undead slayer', 'M10').
card_original_type('undead slayer'/'M10', 'Creature — Human Cleric').
card_original_text('undead slayer'/'M10', '{W}, {T}: Exile target Skeleton, Vampire, or Zombie.').
card_first_print('undead slayer', 'M10').
card_image_name('undead slayer'/'M10', 'undead slayer').
card_uid('undead slayer'/'M10', 'M10:Undead Slayer:undead slayer').
card_rarity('undead slayer'/'M10', 'Uncommon').
card_artist('undead slayer'/'M10', 'Eric Fortune').
card_number('undead slayer'/'M10', '37').
card_flavor_text('undead slayer'/'M10', '\"My motive is not vengeance but mercy. I deliver the soulless back to their severed souls.\"').
card_multiverse_id('undead slayer'/'M10', '190582').

card_in_set('underworld dreams', 'M10').
card_original_type('underworld dreams'/'M10', 'Enchantment').
card_original_text('underworld dreams'/'M10', 'Whenever an opponent draws a card, Underworld Dreams deals 1 damage to him or her.').
card_image_name('underworld dreams'/'M10', 'underworld dreams').
card_uid('underworld dreams'/'M10', 'M10:Underworld Dreams:underworld dreams').
card_rarity('underworld dreams'/'M10', 'Rare').
card_artist('underworld dreams'/'M10', 'Carl Critchlow').
card_number('underworld dreams'/'M10', '115').
card_flavor_text('underworld dreams'/'M10', '\"In the drowsy dark cave of the mind, dreams build their nest with fragments dropped from day\'s caravan.\"\n—Rabindranath Tagore').
card_multiverse_id('underworld dreams'/'M10', '191343').

card_in_set('unholy strength', 'M10').
card_original_type('unholy strength'/'M10', 'Enchantment — Aura').
card_original_text('unholy strength'/'M10', 'Enchant creature\nEnchanted creature gets +2/+1.').
card_image_name('unholy strength'/'M10', 'unholy strength').
card_uid('unholy strength'/'M10', 'M10:Unholy Strength:unholy strength').
card_rarity('unholy strength'/'M10', 'Common').
card_artist('unholy strength'/'M10', 'Terese Nielsen').
card_number('unholy strength'/'M10', '116').
card_flavor_text('unholy strength'/'M10', '\"Born under the moon, the second child will stumble on temptation and seek power in the dark places of the heart.\"\n—Codex of the Constellari').
card_multiverse_id('unholy strength'/'M10', '190546').

card_in_set('unsummon', 'M10').
card_original_type('unsummon'/'M10', 'Instant').
card_original_text('unsummon'/'M10', 'Return target creature to its owner\'s hand.').
card_image_name('unsummon'/'M10', 'unsummon').
card_uid('unsummon'/'M10', 'M10:Unsummon:unsummon').
card_rarity('unsummon'/'M10', 'Common').
card_artist('unsummon'/'M10', 'Izzy').
card_number('unsummon'/'M10', '79').
card_flavor_text('unsummon'/'M10', 'Mages routinely summon creatures from the Æther. It\'s only fair that they occasionally throw one back.').
card_multiverse_id('unsummon'/'M10', '190183').

card_in_set('vampire aristocrat', 'M10').
card_original_type('vampire aristocrat'/'M10', 'Creature — Vampire Rogue').
card_original_text('vampire aristocrat'/'M10', 'Sacrifice a creature: Vampire Aristocrat gets +2/+2 until end of turn.').
card_first_print('vampire aristocrat', 'M10').
card_image_name('vampire aristocrat'/'M10', 'vampire aristocrat').
card_uid('vampire aristocrat'/'M10', 'M10:Vampire Aristocrat:vampire aristocrat').
card_rarity('vampire aristocrat'/'M10', 'Common').
card_artist('vampire aristocrat'/'M10', 'Austin Hsu').
card_number('vampire aristocrat'/'M10', '117').
card_flavor_text('vampire aristocrat'/'M10', '\"I admit, I am a creature of the city. The sights, the nightly excitement, the abundance of fine drink . . .\"').
card_multiverse_id('vampire aristocrat'/'M10', '190563').

card_in_set('vampire nocturnus', 'M10').
card_original_type('vampire nocturnus'/'M10', 'Creature — Vampire').
card_original_text('vampire nocturnus'/'M10', 'Play with the top card of your library revealed.\nAs long as the top card of your library is black, Vampire Nocturnus and other Vampire creatures you control get +2/+1 and have flying.').
card_image_name('vampire nocturnus'/'M10', 'vampire nocturnus').
card_uid('vampire nocturnus'/'M10', 'M10:Vampire Nocturnus:vampire nocturnus').
card_rarity('vampire nocturnus'/'M10', 'Mythic Rare').
card_artist('vampire nocturnus'/'M10', 'Raymond Swanland').
card_number('vampire nocturnus'/'M10', '118').
card_flavor_text('vampire nocturnus'/'M10', '\"Your life will set with the sun.\"').
card_multiverse_id('vampire nocturnus'/'M10', '191384').

card_in_set('veteran armorsmith', 'M10').
card_original_type('veteran armorsmith'/'M10', 'Creature — Human Soldier').
card_original_text('veteran armorsmith'/'M10', 'Other Soldier creatures you control get +0/+1.').
card_first_print('veteran armorsmith', 'M10').
card_image_name('veteran armorsmith'/'M10', 'veteran armorsmith').
card_uid('veteran armorsmith'/'M10', 'M10:Veteran Armorsmith:veteran armorsmith').
card_rarity('veteran armorsmith'/'M10', 'Common').
card_artist('veteran armorsmith'/'M10', 'Michael Komarck').
card_number('veteran armorsmith'/'M10', '38').
card_flavor_text('veteran armorsmith'/'M10', '\"Courage is a proud and gleaming shield. It does not bend or hide from sight. To fight with courage, we must forge in steel.\"').
card_multiverse_id('veteran armorsmith'/'M10', '189923').

card_in_set('veteran swordsmith', 'M10').
card_original_type('veteran swordsmith'/'M10', 'Creature — Human Soldier').
card_original_text('veteran swordsmith'/'M10', 'Other Soldier creatures you control get +1/+0.').
card_first_print('veteran swordsmith', 'M10').
card_image_name('veteran swordsmith'/'M10', 'veteran swordsmith').
card_uid('veteran swordsmith'/'M10', 'M10:Veteran Swordsmith:veteran swordsmith').
card_rarity('veteran swordsmith'/'M10', 'Common').
card_artist('veteran swordsmith'/'M10', 'Michael Komarck').
card_number('veteran swordsmith'/'M10', '39').
card_flavor_text('veteran swordsmith'/'M10', '\"Truth is a straight, keen edge. There are no soft angles or rounded edges. To fight for truth, we must forge in steel.\"').
card_multiverse_id('veteran swordsmith'/'M10', '190194').

card_in_set('viashino spearhunter', 'M10').
card_original_type('viashino spearhunter'/'M10', 'Creature — Viashino Warrior').
card_original_text('viashino spearhunter'/'M10', 'First strike (This creature deals combat damage before creatures without first strike.)').
card_first_print('viashino spearhunter', 'M10').
card_image_name('viashino spearhunter'/'M10', 'viashino spearhunter').
card_uid('viashino spearhunter'/'M10', 'M10:Viashino Spearhunter:viashino spearhunter').
card_rarity('viashino spearhunter'/'M10', 'Common').
card_artist('viashino spearhunter'/'M10', 'Carl Critchlow').
card_number('viashino spearhunter'/'M10', '161').
card_flavor_text('viashino spearhunter'/'M10', 'Viashino lack the majesty of their draconic ancestors, but they lash out with equal rage and twice the spite.').
card_multiverse_id('viashino spearhunter'/'M10', '191072').

card_in_set('wall of bone', 'M10').
card_original_type('wall of bone'/'M10', 'Creature — Skeleton Wall').
card_original_text('wall of bone'/'M10', 'Defender (This creature can\'t attack.)\n{B}: Regenerate Wall of Bone. (The next time this creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_image_name('wall of bone'/'M10', 'wall of bone').
card_uid('wall of bone'/'M10', 'M10:Wall of Bone:wall of bone').
card_rarity('wall of bone'/'M10', 'Uncommon').
card_artist('wall of bone'/'M10', 'Jaime Jones').
card_number('wall of bone'/'M10', '119').
card_multiverse_id('wall of bone'/'M10', '190578').

card_in_set('wall of faith', 'M10').
card_original_type('wall of faith'/'M10', 'Creature — Wall').
card_original_text('wall of faith'/'M10', 'Defender (This creature can\'t attack.)\n{W}: Wall of Faith gets +0/+1 until end of turn.').
card_first_print('wall of faith', 'M10').
card_image_name('wall of faith'/'M10', 'wall of faith').
card_uid('wall of faith'/'M10', 'M10:Wall of Faith:wall of faith').
card_rarity('wall of faith'/'M10', 'Common').
card_artist('wall of faith'/'M10', 'Martina Pilcerova').
card_number('wall of faith'/'M10', '40').
card_flavor_text('wall of faith'/'M10', '\"This is a fine suit, armorsmith. I will be safe from harm. If only you could forge rings to mail the rest of my people . . .\"\n—Lord Hilneth').
card_multiverse_id('wall of faith'/'M10', '190182').

card_in_set('wall of fire', 'M10').
card_original_type('wall of fire'/'M10', 'Creature — Wall').
card_original_text('wall of fire'/'M10', 'Defender (This creature can\'t attack.)\n{R}: Wall of Fire gets +1/+0 until end of turn.').
card_image_name('wall of fire'/'M10', 'wall of fire').
card_uid('wall of fire'/'M10', 'M10:Wall of Fire:wall of fire').
card_rarity('wall of fire'/'M10', 'Uncommon').
card_artist('wall of fire'/'M10', 'Dan Dos Santos').
card_number('wall of fire'/'M10', '162').
card_flavor_text('wall of fire'/'M10', 'Mercy is for those who keep their distance.').
card_multiverse_id('wall of fire'/'M10', '194423').

card_in_set('wall of frost', 'M10').
card_original_type('wall of frost'/'M10', 'Creature — Wall').
card_original_text('wall of frost'/'M10', 'Defender (This creature can\'t attack.)\nWhenever Wall of Frost blocks a creature, that creature doesn\'t untap during its controller\'s next untap step.').
card_first_print('wall of frost', 'M10').
card_image_name('wall of frost'/'M10', 'wall of frost').
card_uid('wall of frost'/'M10', 'M10:Wall of Frost:wall of frost').
card_rarity('wall of frost'/'M10', 'Uncommon').
card_artist('wall of frost'/'M10', 'Mike Bierek').
card_number('wall of frost'/'M10', '80').
card_flavor_text('wall of frost'/'M10', '\"I welcome it. The pasture is cleared of weeds and the wolves are frozen solid.\"\n—Lumi, goatherd').
card_multiverse_id('wall of frost'/'M10', '190160').

card_in_set('warp world', 'M10').
card_original_type('warp world'/'M10', 'Sorcery').
card_original_text('warp world'/'M10', 'Each player shuffles all permanents he or she owns into his or her library, then reveals that many cards from the top of his or her library. Each player puts all artifact, creature, and land cards revealed this way onto the battlefield, then does the same for enchantment cards, then puts all cards revealed this way that weren\'t put onto the battlefield on the bottom of his or her library.').
card_image_name('warp world'/'M10', 'warp world').
card_uid('warp world'/'M10', 'M10:Warp World:warp world').
card_rarity('warp world'/'M10', 'Rare').
card_artist('warp world'/'M10', 'Ron Spencer').
card_number('warp world'/'M10', '163').
card_multiverse_id('warp world'/'M10', '190187').

card_in_set('warpath ghoul', 'M10').
card_original_type('warpath ghoul'/'M10', 'Creature — Zombie').
card_original_text('warpath ghoul'/'M10', '').
card_first_print('warpath ghoul', 'M10').
card_image_name('warpath ghoul'/'M10', 'warpath ghoul').
card_uid('warpath ghoul'/'M10', 'M10:Warpath Ghoul:warpath ghoul').
card_rarity('warpath ghoul'/'M10', 'Common').
card_artist('warpath ghoul'/'M10', 'rk post').
card_number('warpath ghoul'/'M10', '120').
card_flavor_text('warpath ghoul'/'M10', 'The battle was over, but the carnage continued for days.').
card_multiverse_id('warpath ghoul'/'M10', '193740').

card_in_set('weakness', 'M10').
card_original_type('weakness'/'M10', 'Enchantment — Aura').
card_original_text('weakness'/'M10', 'Enchant creature\nEnchanted creature gets -2/-1.').
card_image_name('weakness'/'M10', 'weakness').
card_uid('weakness'/'M10', 'M10:Weakness:weakness').
card_rarity('weakness'/'M10', 'Common').
card_artist('weakness'/'M10', 'Kev Walker').
card_number('weakness'/'M10', '121').
card_flavor_text('weakness'/'M10', '\"For strength to have any meaning, there must also be . . . you.\"').
card_multiverse_id('weakness'/'M10', '190539').

card_in_set('whispersilk cloak', 'M10').
card_original_type('whispersilk cloak'/'M10', 'Artifact — Equipment').
card_original_text('whispersilk cloak'/'M10', 'Equipped creature is unblockable.\nEquipped creature has shroud. (It can\'t be the target of spells or abilities.)\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_image_name('whispersilk cloak'/'M10', 'whispersilk cloak').
card_uid('whispersilk cloak'/'M10', 'M10:Whispersilk Cloak:whispersilk cloak').
card_rarity('whispersilk cloak'/'M10', 'Uncommon').
card_artist('whispersilk cloak'/'M10', 'Daren Bader').
card_number('whispersilk cloak'/'M10', '221').
card_multiverse_id('whispersilk cloak'/'M10', '191078').

card_in_set('white knight', 'M10').
card_original_type('white knight'/'M10', 'Creature — Human Knight').
card_original_text('white knight'/'M10', 'First strike (This creature deals combat damage before creatures without first strike.)\nProtection from black (This creature can\'t be blocked, targeted, dealt damage, or enchanted by anything black.)').
card_image_name('white knight'/'M10', 'white knight').
card_uid('white knight'/'M10', 'M10:White Knight:white knight').
card_rarity('white knight'/'M10', 'Uncommon').
card_artist('white knight'/'M10', 'Christopher Moeller').
card_number('white knight'/'M10', '41').
card_multiverse_id('white knight'/'M10', '189877').

card_in_set('wind drake', 'M10').
card_original_type('wind drake'/'M10', 'Creature — Drake').
card_original_text('wind drake'/'M10', 'Flying').
card_image_name('wind drake'/'M10', 'wind drake').
card_uid('wind drake'/'M10', 'M10:Wind Drake:wind drake').
card_rarity('wind drake'/'M10', 'Common').
card_artist('wind drake'/'M10', 'Steve Prescott').
card_number('wind drake'/'M10', '81').
card_flavor_text('wind drake'/'M10', 'A smaller cousin to the dragon, drakes are often seen at the side of powerful wizards who use them as airborne eyes and ears.').
card_multiverse_id('wind drake'/'M10', '193962').

card_in_set('windstorm', 'M10').
card_original_type('windstorm'/'M10', 'Instant').
card_original_text('windstorm'/'M10', 'Windstorm deals X damage to each creature with flying.').
card_first_print('windstorm', 'M10').
card_image_name('windstorm'/'M10', 'windstorm').
card_uid('windstorm'/'M10', 'M10:Windstorm:windstorm').
card_rarity('windstorm'/'M10', 'Uncommon').
card_artist('windstorm'/'M10', 'Rob Alexander').
card_number('windstorm'/'M10', '205').
card_flavor_text('windstorm'/'M10', '\"We don\'t like interlopers and thieves cluttering our skies.\"\n—Dionus, elvish archdruid').
card_multiverse_id('windstorm'/'M10', '191596').

card_in_set('wurm\'s tooth', 'M10').
card_original_type('wurm\'s tooth'/'M10', 'Artifact').
card_original_text('wurm\'s tooth'/'M10', 'Whenever a player casts a green spell, you may gain 1 life.').
card_image_name('wurm\'s tooth'/'M10', 'wurm\'s tooth').
card_uid('wurm\'s tooth'/'M10', 'M10:Wurm\'s Tooth:wurm\'s tooth').
card_rarity('wurm\'s tooth'/'M10', 'Uncommon').
card_artist('wurm\'s tooth'/'M10', 'Alan Pollack').
card_number('wurm\'s tooth'/'M10', '222').
card_flavor_text('wurm\'s tooth'/'M10', 'A wurm knows nothing of deception. If it opens its mouth, it plans to eat you.').
card_multiverse_id('wurm\'s tooth'/'M10', '191248').

card_in_set('xathrid demon', 'M10').
card_original_type('xathrid demon'/'M10', 'Creature — Demon').
card_original_text('xathrid demon'/'M10', 'Flying, trample\nAt the beginning of your upkeep, sacrifice a creature other than Xathrid Demon, then each opponent loses life equal to the sacrificed creature\'s power. If you can\'t sacrifice a creature, tap Xathrid Demon and you lose 7 life.').
card_first_print('xathrid demon', 'M10').
card_image_name('xathrid demon'/'M10', 'xathrid demon').
card_uid('xathrid demon'/'M10', 'M10:Xathrid Demon:xathrid demon').
card_rarity('xathrid demon'/'M10', 'Mythic Rare').
card_artist('xathrid demon'/'M10', 'Wayne Reynolds').
card_number('xathrid demon'/'M10', '122').
card_multiverse_id('xathrid demon'/'M10', '189885').

card_in_set('yawning fissure', 'M10').
card_original_type('yawning fissure'/'M10', 'Sorcery').
card_original_text('yawning fissure'/'M10', 'Each opponent sacrifices a land.').
card_first_print('yawning fissure', 'M10').
card_image_name('yawning fissure'/'M10', 'yawning fissure').
card_uid('yawning fissure'/'M10', 'M10:Yawning Fissure:yawning fissure').
card_rarity('yawning fissure'/'M10', 'Common').
card_artist('yawning fissure'/'M10', 'Véronique Meignaud').
card_number('yawning fissure'/'M10', '164').
card_flavor_text('yawning fissure'/'M10', 'The mapmaker scowled. He sighed, unrolled his parchment, and painted a large black blob where the Eternal Meadow was once marked.').
card_multiverse_id('yawning fissure'/'M10', '191088').

card_in_set('zephyr sprite', 'M10').
card_original_type('zephyr sprite'/'M10', 'Creature — Faerie').
card_original_text('zephyr sprite'/'M10', 'Flying').
card_first_print('zephyr sprite', 'M10').
card_image_name('zephyr sprite'/'M10', 'zephyr sprite').
card_uid('zephyr sprite'/'M10', 'M10:Zephyr Sprite:zephyr sprite').
card_rarity('zephyr sprite'/'M10', 'Common').
card_artist('zephyr sprite'/'M10', 'Kev Walker').
card_number('zephyr sprite'/'M10', '82').
card_flavor_text('zephyr sprite'/'M10', '\"I\'ve never understood why so many beings lumber around on the ground. There are three dimensions, yet they use only two.\"').
card_multiverse_id('zephyr sprite'/'M10', '190167').

card_in_set('zombie goliath', 'M10').
card_original_type('zombie goliath'/'M10', 'Creature — Zombie Giant').
card_original_text('zombie goliath'/'M10', '').
card_first_print('zombie goliath', 'M10').
card_image_name('zombie goliath'/'M10', 'zombie goliath').
card_uid('zombie goliath'/'M10', 'M10:Zombie Goliath:zombie goliath').
card_rarity('zombie goliath'/'M10', 'Common').
card_artist('zombie goliath'/'M10', 'E. M. Gist').
card_number('zombie goliath'/'M10', '123').
card_flavor_text('zombie goliath'/'M10', '\"Phirax of Blood Ridge has sent a war giant at us? What, do I have to spell it out for you? Kill the giant, scoop out its skull, and drive it back to Blood Ridge. Honestly, what kind of necromancer minions are you?\"\n—Keren-Dur, necromancer lord').
card_multiverse_id('zombie goliath'/'M10', '190545').
