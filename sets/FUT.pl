% Future Sight

set('FUT').
set_name('FUT', 'Future Sight').
set_release_date('FUT', '2007-05-04').
set_border('FUT', 'black').
set_type('FUT', 'expansion').
set_block('FUT', 'Time Spiral').

card_in_set('akroma\'s memorial', 'FUT').
card_original_type('akroma\'s memorial'/'FUT', 'Legendary Artifact').
card_original_text('akroma\'s memorial'/'FUT', 'Creatures you control have flying, first strike, vigilance, trample, haste, protection from black, and protection from red.').
card_first_print('akroma\'s memorial', 'FUT').
card_image_name('akroma\'s memorial'/'FUT', 'akroma\'s memorial').
card_uid('akroma\'s memorial'/'FUT', 'FUT:Akroma\'s Memorial:akroma\'s memorial').
card_rarity('akroma\'s memorial'/'FUT', 'Rare').
card_artist('akroma\'s memorial'/'FUT', 'Dan Scott').
card_number('akroma\'s memorial'/'FUT', '159').
card_flavor_text('akroma\'s memorial'/'FUT', '\"No rest. No mercy. No matter what.\"\n—Memorial inscription').
card_multiverse_id('akroma\'s memorial'/'FUT', '136150').

card_in_set('angel of salvation', 'FUT').
card_original_type('angel of salvation'/'FUT', 'Creature — Angel').
card_original_text('angel of salvation'/'FUT', 'Flash; convoke (Each creature you tap while playing this spell reduces its cost by {1} or by one mana of that creature\'s color.)\nFlying\nWhen Angel of Salvation comes into play, prevent the next 5 damage that would be dealt this turn to any number of target creatures and/or players, divided as you choose.').
card_first_print('angel of salvation', 'FUT').
card_image_name('angel of salvation'/'FUT', 'angel of salvation').
card_uid('angel of salvation'/'FUT', 'FUT:Angel of Salvation:angel of salvation').
card_rarity('angel of salvation'/'FUT', 'Rare').
card_artist('angel of salvation'/'FUT', 'D. Alexander Gregory').
card_number('angel of salvation'/'FUT', '1').
card_multiverse_id('angel of salvation'/'FUT', '130345').

card_in_set('arc blade', 'FUT').
card_original_type('arc blade'/'FUT', 'Sorcery').
card_original_text('arc blade'/'FUT', 'Arc Blade deals 2 damage to target creature or player. Remove Arc Blade from the game with three time counters on it.\nSuspend 3—{2}{R} (Rather than play this card from your hand, you may pay {2}{R} and remove it from the game with three time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, play it without paying its mana cost.)').
card_first_print('arc blade', 'FUT').
card_image_name('arc blade'/'FUT', 'arc blade').
card_uid('arc blade'/'FUT', 'FUT:Arc Blade:arc blade').
card_rarity('arc blade'/'FUT', 'Uncommon').
card_artist('arc blade'/'FUT', 'Shishizaru').
card_number('arc blade'/'FUT', '94').
card_multiverse_id('arc blade'/'FUT', '126193').

card_in_set('arcanum wings', 'FUT').
card_original_type('arcanum wings'/'FUT', 'Enchantment — Aura').
card_original_text('arcanum wings'/'FUT', 'Enchant creature\nEnchanted creature has flying.\nAura swap {2}{U} ({2}{U}: Exchange this Aura with an Aura card in your hand.)').
card_first_print('arcanum wings', 'FUT').
card_image_name('arcanum wings'/'FUT', 'arcanum wings').
card_uid('arcanum wings'/'FUT', 'FUT:Arcanum Wings:arcanum wings').
card_rarity('arcanum wings'/'FUT', 'Uncommon').
card_artist('arcanum wings'/'FUT', 'Carl Frank').
card_number('arcanum wings'/'FUT', '48').
card_flavor_text('arcanum wings'/'FUT', 'Nobles don wings to reach the sky villas of their peers, exchanging them for more fashionable attire upon arrival.').
card_multiverse_id('arcanum wings'/'FUT', '136055').
card_timeshifted('arcanum wings'/'FUT')
.
card_in_set('augur il-vec', 'FUT').
card_original_type('augur il-vec'/'FUT', 'Creature — Human Cleric').
card_original_text('augur il-vec'/'FUT', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nSacrifice Augur il-Vec: You gain 4 life. Play this ability only during your upkeep.').
card_first_print('augur il-vec', 'FUT').
card_image_name('augur il-vec'/'FUT', 'augur il-vec').
card_uid('augur il-vec'/'FUT', 'FUT:Augur il-Vec:augur il-vec').
card_rarity('augur il-vec'/'FUT', 'Common').
card_artist('augur il-vec'/'FUT', 'Daren Bader').
card_number('augur il-vec'/'FUT', '2').
card_flavor_text('augur il-vec'/'FUT', '\"If it would end this exile, my life is yours.\"').
card_multiverse_id('augur il-vec'/'FUT', '132212').

card_in_set('augur of skulls', 'FUT').
card_original_type('augur of skulls'/'FUT', 'Creature — Skeleton Wizard').
card_original_text('augur of skulls'/'FUT', '{1}{B}: Regenerate Augur of Skulls.\nSacrifice Augur of Skulls: Target player discards two cards. Play this ability only during your upkeep.').
card_first_print('augur of skulls', 'FUT').
card_image_name('augur of skulls'/'FUT', 'augur of skulls').
card_uid('augur of skulls'/'FUT', 'FUT:Augur of Skulls:augur of skulls').
card_rarity('augur of skulls'/'FUT', 'Common').
card_artist('augur of skulls'/'FUT', 'Chippy').
card_number('augur of skulls'/'FUT', '63').
card_flavor_text('augur of skulls'/'FUT', 'Within each empty skull, it seeks ways to preserve its own failing undeath.').
card_multiverse_id('augur of skulls'/'FUT', '132224').

card_in_set('aven augur', 'FUT').
card_original_type('aven augur'/'FUT', 'Creature — Bird Wizard').
card_original_text('aven augur'/'FUT', 'Flying\nSacrifice Aven Augur: Return up to two target creatures to their owners\' hands. Play this ability only during your upkeep.').
card_first_print('aven augur', 'FUT').
card_image_name('aven augur'/'FUT', 'aven augur').
card_uid('aven augur'/'FUT', 'FUT:Aven Augur:aven augur').
card_rarity('aven augur'/'FUT', 'Common').
card_artist('aven augur'/'FUT', 'Dan Scott').
card_number('aven augur'/'FUT', '32').
card_flavor_text('aven augur'/'FUT', '\"I have seen the future. It does not include you.\"').
card_multiverse_id('aven augur'/'FUT', '132225').

card_in_set('aven mindcensor', 'FUT').
card_original_type('aven mindcensor'/'FUT', 'Creature — Bird Wizard').
card_original_text('aven mindcensor'/'FUT', 'Flash (You may play this spell any time you could play an instant.)\nFlying\nIf an opponent would search a library, that player searches the top four cards of that library instead.').
card_first_print('aven mindcensor', 'FUT').
card_image_name('aven mindcensor'/'FUT', 'aven mindcensor').
card_uid('aven mindcensor'/'FUT', 'FUT:Aven Mindcensor:aven mindcensor').
card_rarity('aven mindcensor'/'FUT', 'Uncommon').
card_artist('aven mindcensor'/'FUT', 'Rebecca Guay').
card_number('aven mindcensor'/'FUT', '18').
card_multiverse_id('aven mindcensor'/'FUT', '136204').
card_timeshifted('aven mindcensor'/'FUT')
.
card_in_set('barren glory', 'FUT').
card_original_type('barren glory'/'FUT', 'Enchantment').
card_original_text('barren glory'/'FUT', 'At the beginning of your upkeep, if you control no permanents other than Barren Glory and have no cards in hand, you win the game.').
card_first_print('barren glory', 'FUT').
card_image_name('barren glory'/'FUT', 'barren glory').
card_uid('barren glory'/'FUT', 'FUT:Barren Glory:barren glory').
card_rarity('barren glory'/'FUT', 'Rare').
card_artist('barren glory'/'FUT', 'Dave Kendall').
card_number('barren glory'/'FUT', '3').
card_flavor_text('barren glory'/'FUT', '\"The only perfect world is an empty world, with no one to sin or wage war.\"\n—Tarran, magus of the disk').
card_multiverse_id('barren glory'/'FUT', '136048').

card_in_set('baru, fist of krosa', 'FUT').
card_original_type('baru, fist of krosa'/'FUT', 'Legendary Creature — Human Druid').
card_original_text('baru, fist of krosa'/'FUT', 'Whenever a Forest comes into play, green creatures you control get +1/+1 and gain trample until end of turn.\nGrandeur Discard another card named Baru, Fist of Krosa: Put an X/X green Wurm creature token into play, where X is the number of lands you control.').
card_first_print('baru, fist of krosa', 'FUT').
card_image_name('baru, fist of krosa'/'FUT', 'baru, fist of krosa').
card_uid('baru, fist of krosa'/'FUT', 'FUT:Baru, Fist of Krosa:baru, fist of krosa').
card_rarity('baru, fist of krosa'/'FUT', 'Rare').
card_artist('baru, fist of krosa'/'FUT', 'Lucio Parrillo').
card_number('baru, fist of krosa'/'FUT', '142').
card_multiverse_id('baru, fist of krosa'/'FUT', '136155').
card_timeshifted('baru, fist of krosa'/'FUT')
.
card_in_set('bitter ordeal', 'FUT').
card_original_type('bitter ordeal'/'FUT', 'Sorcery').
card_original_text('bitter ordeal'/'FUT', 'Search target player\'s library for a card and remove that card from the game. Then that player shuffles his or her library.\nGravestorm (When you play this spell, copy it for each permanent put into a graveyard this turn. You may choose new targets for the copies.)').
card_first_print('bitter ordeal', 'FUT').
card_image_name('bitter ordeal'/'FUT', 'bitter ordeal').
card_uid('bitter ordeal'/'FUT', 'FUT:Bitter Ordeal:bitter ordeal').
card_rarity('bitter ordeal'/'FUT', 'Rare').
card_artist('bitter ordeal'/'FUT', 'Daarken').
card_number('bitter ordeal'/'FUT', '80').
card_multiverse_id('bitter ordeal'/'FUT', '136049').
card_timeshifted('bitter ordeal'/'FUT')
.
card_in_set('blade of the sixth pride', 'FUT').
card_original_type('blade of the sixth pride'/'FUT', 'Creature — Cat Rebel').
card_original_text('blade of the sixth pride'/'FUT', '').
card_first_print('blade of the sixth pride', 'FUT').
card_image_name('blade of the sixth pride'/'FUT', 'blade of the sixth pride').
card_uid('blade of the sixth pride'/'FUT', 'FUT:Blade of the Sixth Pride:blade of the sixth pride').
card_rarity('blade of the sixth pride'/'FUT', 'Common').
card_artist('blade of the sixth pride'/'FUT', 'Justin Sweet').
card_number('blade of the sixth pride'/'FUT', '19').
card_multiverse_id('blade of the sixth pride'/'FUT', '130676').
card_timeshifted('blade of the sixth pride'/'FUT')
.
card_in_set('blind phantasm', 'FUT').
card_original_type('blind phantasm'/'FUT', 'Creature — Illusion').
card_original_text('blind phantasm'/'FUT', '').
card_first_print('blind phantasm', 'FUT').
card_image_name('blind phantasm'/'FUT', 'blind phantasm').
card_uid('blind phantasm'/'FUT', 'FUT:Blind Phantasm:blind phantasm').
card_rarity('blind phantasm'/'FUT', 'Common').
card_artist('blind phantasm'/'FUT', 'Khang Le').
card_number('blind phantasm'/'FUT', '49').
card_multiverse_id('blind phantasm'/'FUT', '126143').
card_timeshifted('blind phantasm'/'FUT')
.
card_in_set('bloodshot trainee', 'FUT').
card_original_type('bloodshot trainee'/'FUT', 'Creature — Goblin Warrior').
card_original_text('bloodshot trainee'/'FUT', '{T}: Bloodshot Trainee deals 4 damage to target creature. Play this ability only if Bloodshot Trainee\'s power is 4 or greater.').
card_first_print('bloodshot trainee', 'FUT').
card_image_name('bloodshot trainee'/'FUT', 'bloodshot trainee').
card_uid('bloodshot trainee'/'FUT', 'FUT:Bloodshot Trainee:bloodshot trainee').
card_rarity('bloodshot trainee'/'FUT', 'Uncommon').
card_artist('bloodshot trainee'/'FUT', 'Lucio Parrillo').
card_number('bloodshot trainee'/'FUT', '110').
card_flavor_text('bloodshot trainee'/'FUT', '\"Hrrngh! Someday I\'m going to hurl this . . . er . . . roll this . . . hrrngh . . . nudge this boulder right down a cliff.\"').
card_multiverse_id('bloodshot trainee'/'FUT', '130686').
card_timeshifted('bloodshot trainee'/'FUT')
.
card_in_set('bogardan lancer', 'FUT').
card_original_type('bogardan lancer'/'FUT', 'Creature — Human Knight').
card_original_text('bogardan lancer'/'FUT', 'Bloodthirst 1 (If an opponent was dealt damage this turn, this creature comes into play with a +1/+1 counter on it.)\nFlanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)').
card_first_print('bogardan lancer', 'FUT').
card_image_name('bogardan lancer'/'FUT', 'bogardan lancer').
card_uid('bogardan lancer'/'FUT', 'FUT:Bogardan Lancer:bogardan lancer').
card_rarity('bogardan lancer'/'FUT', 'Common').
card_artist('bogardan lancer'/'FUT', 'Jim Murray').
card_number('bogardan lancer'/'FUT', '95').
card_multiverse_id('bogardan lancer'/'FUT', '126161').

card_in_set('boldwyr intimidator', 'FUT').
card_original_type('boldwyr intimidator'/'FUT', 'Creature — Giant Warrior').
card_original_text('boldwyr intimidator'/'FUT', 'Cowards can\'t block Warriors.\n{R}: Target creature\'s type becomes Coward until end of turn.\n{2}{R}: Target creature\'s type becomes Warrior until end of turn.').
card_first_print('boldwyr intimidator', 'FUT').
card_image_name('boldwyr intimidator'/'FUT', 'boldwyr intimidator').
card_uid('boldwyr intimidator'/'FUT', 'FUT:Boldwyr Intimidator:boldwyr intimidator').
card_rarity('boldwyr intimidator'/'FUT', 'Uncommon').
card_artist('boldwyr intimidator'/'FUT', 'Esad Ribic').
card_number('boldwyr intimidator'/'FUT', '111').
card_multiverse_id('boldwyr intimidator'/'FUT', '130672').
card_timeshifted('boldwyr intimidator'/'FUT')
.
card_in_set('bonded fetch', 'FUT').
card_original_type('bonded fetch'/'FUT', 'Creature — Homunculus').
card_original_text('bonded fetch'/'FUT', 'Defender, haste\n{T}: Draw a card, then discard a card.').
card_first_print('bonded fetch', 'FUT').
card_image_name('bonded fetch'/'FUT', 'bonded fetch').
card_uid('bonded fetch'/'FUT', 'FUT:Bonded Fetch:bonded fetch').
card_rarity('bonded fetch'/'FUT', 'Uncommon').
card_artist('bonded fetch'/'FUT', 'Brandon Kitkouski').
card_number('bonded fetch'/'FUT', '50').
card_flavor_text('bonded fetch'/'FUT', 'A well-made homunculus grooms the mind of its master, pruning the thoughts that lead to madness. Few loredelvers survive the study of the infinite without one.').
card_multiverse_id('bonded fetch'/'FUT', '132229').
card_timeshifted('bonded fetch'/'FUT')
.
card_in_set('bound in silence', 'FUT').
card_original_type('bound in silence'/'FUT', 'Tribal Enchantment — Rebel Aura').
card_original_text('bound in silence'/'FUT', 'Enchant creature\nEnchanted creature can\'t attack or block.').
card_first_print('bound in silence', 'FUT').
card_image_name('bound in silence'/'FUT', 'bound in silence').
card_uid('bound in silence'/'FUT', 'FUT:Bound in Silence:bound in silence').
card_rarity('bound in silence'/'FUT', 'Uncommon').
card_artist('bound in silence'/'FUT', 'William Simpson').
card_number('bound in silence'/'FUT', '20').
card_flavor_text('bound in silence'/'FUT', 'A fight put off forever is already won.').
card_multiverse_id('bound in silence'/'FUT', '130334').
card_timeshifted('bound in silence'/'FUT')
.
card_in_set('bridge from below', 'FUT').
card_original_type('bridge from below'/'FUT', 'Enchantment').
card_original_text('bridge from below'/'FUT', 'Whenever a nontoken creature is put into your graveyard from play, if Bridge from Below is in your graveyard, put a 2/2 black Zombie creature token into play.\nWhen a creature is put into an opponent\'s graveyard from play, if Bridge from Below is in your graveyard, remove Bridge from Below from the game.').
card_first_print('bridge from below', 'FUT').
card_image_name('bridge from below'/'FUT', 'bridge from below').
card_uid('bridge from below'/'FUT', 'FUT:Bridge from Below:bridge from below').
card_rarity('bridge from below'/'FUT', 'Rare').
card_artist('bridge from below'/'FUT', 'Greg & Tim Hildebrandt').
card_number('bridge from below'/'FUT', '81').
card_multiverse_id('bridge from below'/'FUT', '136054').
card_timeshifted('bridge from below'/'FUT')
.
card_in_set('centaur omenreader', 'FUT').
card_original_type('centaur omenreader'/'FUT', 'Snow Creature — Centaur Shaman').
card_original_text('centaur omenreader'/'FUT', 'As long as Centaur Omenreader is tapped, creature spells you play cost {2} less to play.').
card_first_print('centaur omenreader', 'FUT').
card_image_name('centaur omenreader'/'FUT', 'centaur omenreader').
card_uid('centaur omenreader'/'FUT', 'FUT:Centaur Omenreader:centaur omenreader').
card_rarity('centaur omenreader'/'FUT', 'Uncommon').
card_artist('centaur omenreader'/'FUT', 'Justin Sweet').
card_number('centaur omenreader'/'FUT', '143').
card_flavor_text('centaur omenreader'/'FUT', 'The wisdom of the omenreaders is legendary. Creatures travel for miles through icy peril to seek their counsel.').
card_multiverse_id('centaur omenreader'/'FUT', '130684').
card_timeshifted('centaur omenreader'/'FUT')
.
card_in_set('char-rumbler', 'FUT').
card_original_type('char-rumbler'/'FUT', 'Creature — Elemental').
card_original_text('char-rumbler'/'FUT', 'Double strike\n{R}: Char-Rumbler gets +1/+0 until end of turn.').
card_first_print('char-rumbler', 'FUT').
card_image_name('char-rumbler'/'FUT', 'char-rumbler').
card_uid('char-rumbler'/'FUT', 'FUT:Char-Rumbler:char-rumbler').
card_rarity('char-rumbler'/'FUT', 'Uncommon').
card_artist('char-rumbler'/'FUT', 'Kev Walker').
card_number('char-rumbler'/'FUT', '96').
card_flavor_text('char-rumbler'/'FUT', 'Elementals flourished as Dominaria healed and new forms of life adapted to the land\'s reinfusion with mana.').
card_multiverse_id('char-rumbler'/'FUT', '130582').

card_in_set('chronomantic escape', 'FUT').
card_original_type('chronomantic escape'/'FUT', 'Sorcery').
card_original_text('chronomantic escape'/'FUT', 'Until your next turn, creatures can\'t attack you. Remove Chronomantic Escape from the game with three time counters on it.\nSuspend 3—{2}{W} (Rather than play this card from your hand, you may pay {2}{W} and remove it from the game with three time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, play it without paying its mana cost.)').
card_first_print('chronomantic escape', 'FUT').
card_image_name('chronomantic escape'/'FUT', 'chronomantic escape').
card_uid('chronomantic escape'/'FUT', 'FUT:Chronomantic Escape:chronomantic escape').
card_rarity('chronomantic escape'/'FUT', 'Uncommon').
card_artist('chronomantic escape'/'FUT', 'Franz Vohwinkel').
card_number('chronomantic escape'/'FUT', '4').
card_multiverse_id('chronomantic escape'/'FUT', '126204').

card_in_set('cloud key', 'FUT').
card_original_type('cloud key'/'FUT', 'Artifact').
card_original_text('cloud key'/'FUT', 'As Cloud Key comes into play, choose artifact, creature, enchantment, instant, or sorcery.\nSpells you play of the chosen type cost {1} less to play.').
card_first_print('cloud key', 'FUT').
card_image_name('cloud key'/'FUT', 'cloud key').
card_uid('cloud key'/'FUT', 'FUT:Cloud Key:cloud key').
card_rarity('cloud key'/'FUT', 'Rare').
card_artist('cloud key'/'FUT', 'Trevor Hairsine').
card_number('cloud key'/'FUT', '160').
card_flavor_text('cloud key'/'FUT', 'The musical sound of its turning triggers the tumblers of revelation.').
card_multiverse_id('cloud key'/'FUT', '136156').

card_in_set('cloudseeder', 'FUT').
card_original_type('cloudseeder'/'FUT', 'Creature — Faerie Spellshaper').
card_original_text('cloudseeder'/'FUT', 'Flying\n{U}, {T}, Discard a card: Put a 1/1 blue Faerie creature token named Cloud Sprite into play with flying and \"Cloud Sprite can block only creatures with flying.\"').
card_first_print('cloudseeder', 'FUT').
card_image_name('cloudseeder'/'FUT', 'cloudseeder').
card_uid('cloudseeder'/'FUT', 'FUT:Cloudseeder:cloudseeder').
card_rarity('cloudseeder'/'FUT', 'Uncommon').
card_artist('cloudseeder'/'FUT', 'Tsutomu Kawade').
card_number('cloudseeder'/'FUT', '33').
card_multiverse_id('cloudseeder'/'FUT', '130309').

card_in_set('coalition relic', 'FUT').
card_original_type('coalition relic'/'FUT', 'Artifact').
card_original_text('coalition relic'/'FUT', '{T}: Add one mana of any color to your mana pool.\n{T}: Put a charge counter on Coalition Relic.\nAt the beginning of your precombat main phase, remove all charge counters from Coalition Relic. Add one mana of any color to your mana pool for each counter removed this way.').
card_first_print('coalition relic', 'FUT').
card_image_name('coalition relic'/'FUT', 'coalition relic').
card_uid('coalition relic'/'FUT', 'FUT:Coalition Relic:coalition relic').
card_rarity('coalition relic'/'FUT', 'Rare').
card_artist('coalition relic'/'FUT', 'Donato Giancola').
card_number('coalition relic'/'FUT', '161').
card_multiverse_id('coalition relic'/'FUT', '125878').

card_in_set('cryptic annelid', 'FUT').
card_original_type('cryptic annelid'/'FUT', 'Creature — Beast').
card_original_text('cryptic annelid'/'FUT', 'When Cryptic Annelid comes into play, scry 1, then scry 2, then scry 3. (To scry X, look at the top X cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_first_print('cryptic annelid', 'FUT').
card_image_name('cryptic annelid'/'FUT', 'cryptic annelid').
card_uid('cryptic annelid'/'FUT', 'FUT:Cryptic Annelid:cryptic annelid').
card_rarity('cryptic annelid'/'FUT', 'Uncommon').
card_artist('cryptic annelid'/'FUT', 'Anthony S. Waters').
card_number('cryptic annelid'/'FUT', '34').
card_multiverse_id('cryptic annelid'/'FUT', '126141').

card_in_set('cutthroat il-dal', 'FUT').
card_original_type('cutthroat il-dal'/'FUT', 'Creature — Human Rogue').
card_original_text('cutthroat il-dal'/'FUT', 'Hellbent Cutthroat il-Dal has shadow as long as you have no cards in hand. (It can block or be blocked by only creatures with shadow.)').
card_first_print('cutthroat il-dal', 'FUT').
card_image_name('cutthroat il-dal'/'FUT', 'cutthroat il-dal').
card_uid('cutthroat il-dal'/'FUT', 'FUT:Cutthroat il-Dal:cutthroat il-dal').
card_rarity('cutthroat il-dal'/'FUT', 'Common').
card_artist('cutthroat il-dal'/'FUT', 'Vance Kovacs').
card_number('cutthroat il-dal'/'FUT', '64').
card_flavor_text('cutthroat il-dal'/'FUT', 'Her blades slashed across the noble\'s shadow. He scoffed, then fell over dead.').
card_multiverse_id('cutthroat il-dal'/'FUT', '126177').

card_in_set('cyclical evolution', 'FUT').
card_original_type('cyclical evolution'/'FUT', 'Sorcery').
card_original_text('cyclical evolution'/'FUT', 'Target creature gets +3/+3 until end of turn. Remove Cyclical Evolution from the game with three time counters on it.\nSuspend 3—{2}{G} (Rather than play this card from your hand, you may pay {2}{G} and remove it from the game with three time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, play it without paying its mana cost.)').
card_first_print('cyclical evolution', 'FUT').
card_image_name('cyclical evolution'/'FUT', 'cyclical evolution').
card_uid('cyclical evolution'/'FUT', 'FUT:Cyclical Evolution:cyclical evolution').
card_rarity('cyclical evolution'/'FUT', 'Uncommon').
card_artist('cyclical evolution'/'FUT', 'Matt Cavotta').
card_number('cyclical evolution'/'FUT', '125').
card_multiverse_id('cyclical evolution'/'FUT', '126169').

card_in_set('dakmor salvage', 'FUT').
card_original_type('dakmor salvage'/'FUT', 'Land').
card_original_text('dakmor salvage'/'FUT', 'Dakmor Salvage comes into play tapped.\n{T}: Add {B} to your mana pool.\nDredge 2 (If you would draw a card, instead you may put exactly two cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_first_print('dakmor salvage', 'FUT').
card_image_name('dakmor salvage'/'FUT', 'dakmor salvage').
card_uid('dakmor salvage'/'FUT', 'FUT:Dakmor Salvage:dakmor salvage').
card_rarity('dakmor salvage'/'FUT', 'Uncommon').
card_artist('dakmor salvage'/'FUT', 'John Avon').
card_number('dakmor salvage'/'FUT', '169').
card_multiverse_id('dakmor salvage'/'FUT', '136053').

card_in_set('darksteel garrison', 'FUT').
card_original_type('darksteel garrison'/'FUT', 'Artifact — Fortification').
card_original_text('darksteel garrison'/'FUT', 'Fortified land is indestructible.\nWhenever fortified land becomes tapped, target creature gets +1/+1 until end of turn.\nFortify {3} ({3}: Attach to target land you control. Fortify only as a sorcery. This card comes into play unattached and stays in play if the land leaves play.)').
card_first_print('darksteel garrison', 'FUT').
card_image_name('darksteel garrison'/'FUT', 'darksteel garrison').
card_uid('darksteel garrison'/'FUT', 'FUT:Darksteel Garrison:darksteel garrison').
card_rarity('darksteel garrison'/'FUT', 'Rare').
card_artist('darksteel garrison'/'FUT', 'David Martin').
card_number('darksteel garrison'/'FUT', '167').
card_multiverse_id('darksteel garrison'/'FUT', '126211').
card_timeshifted('darksteel garrison'/'FUT')
.
card_in_set('daybreak coronet', 'FUT').
card_original_type('daybreak coronet'/'FUT', 'Enchantment — Aura').
card_original_text('daybreak coronet'/'FUT', 'Enchant creature with another Aura attached to it\nEnchanted creature gets +3/+3 and has first strike, vigilance, and lifelink. (Whenever it deals damage, its controller gains that much life.)').
card_first_print('daybreak coronet', 'FUT').
card_image_name('daybreak coronet'/'FUT', 'daybreak coronet').
card_uid('daybreak coronet'/'FUT', 'FUT:Daybreak Coronet:daybreak coronet').
card_rarity('daybreak coronet'/'FUT', 'Rare').
card_artist('daybreak coronet'/'FUT', 'Scott Hampton').
card_number('daybreak coronet'/'FUT', '21').
card_multiverse_id('daybreak coronet'/'FUT', '130635').
card_timeshifted('daybreak coronet'/'FUT')
.
card_in_set('death rattle', 'FUT').
card_original_type('death rattle'/'FUT', 'Instant').
card_original_text('death rattle'/'FUT', 'Delve (You may remove any number of cards in your graveyard from the game as you play this spell. It costs {1} less to play for each card removed this way.)\nDestroy target nongreen creature. It can\'t be regenerated.').
card_first_print('death rattle', 'FUT').
card_image_name('death rattle'/'FUT', 'death rattle').
card_uid('death rattle'/'FUT', 'FUT:Death Rattle:death rattle').
card_rarity('death rattle'/'FUT', 'Common').
card_artist('death rattle'/'FUT', 'Vance Kovacs').
card_number('death rattle'/'FUT', '82').
card_multiverse_id('death rattle'/'FUT', '136043').
card_timeshifted('death rattle'/'FUT')
.
card_in_set('deepcavern imp', 'FUT').
card_original_type('deepcavern imp'/'FUT', 'Creature — Imp Rebel').
card_original_text('deepcavern imp'/'FUT', 'Flying, haste\nEcho—Discard a card. (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)').
card_first_print('deepcavern imp', 'FUT').
card_image_name('deepcavern imp'/'FUT', 'deepcavern imp').
card_uid('deepcavern imp'/'FUT', 'FUT:Deepcavern Imp:deepcavern imp').
card_rarity('deepcavern imp'/'FUT', 'Common').
card_artist('deepcavern imp'/'FUT', 'Scott Altmann').
card_number('deepcavern imp'/'FUT', '83').
card_multiverse_id('deepcavern imp'/'FUT', '132226').
card_timeshifted('deepcavern imp'/'FUT')
.
card_in_set('delay', 'FUT').
card_original_type('delay'/'FUT', 'Instant').
card_original_text('delay'/'FUT', 'Counter target spell. If the spell is countered this way, remove it from the game with three time counters on it instead of putting it into its owner\'s graveyard. If it doesn\'t have suspend, it gains suspend. (At the beginning of its owner\'s upkeep, remove a counter from that card. When the last is removed, the player plays it without paying its mana cost. If it\'s a creature, it has haste.)').
card_first_print('delay', 'FUT').
card_image_name('delay'/'FUT', 'delay').
card_uid('delay'/'FUT', 'FUT:Delay:delay').
card_rarity('delay'/'FUT', 'Uncommon').
card_artist('delay'/'FUT', 'Ron Spears').
card_number('delay'/'FUT', '35').
card_multiverse_id('delay'/'FUT', '132228').

card_in_set('dryad arbor', 'FUT').
card_original_type('dryad arbor'/'FUT', 'Land Creature — Forest Dryad').
card_original_text('dryad arbor'/'FUT', '(Dryad Arbor isn\'t a spell, it\'s affected by summoning sickness, and it has \"{T}: Add {G} to your mana pool.\")\nDryad Arbor is green.').
card_first_print('dryad arbor', 'FUT').
card_image_name('dryad arbor'/'FUT', 'dryad arbor').
card_uid('dryad arbor'/'FUT', 'FUT:Dryad Arbor:dryad arbor').
card_rarity('dryad arbor'/'FUT', 'Uncommon').
card_artist('dryad arbor'/'FUT', 'Eric Fortune').
card_number('dryad arbor'/'FUT', '174').
card_flavor_text('dryad arbor'/'FUT', '\"Touch no tree, break no branch, and speak only the question you wish answered.\"\n—Von Yomm, elder druid, to her initiates').
card_multiverse_id('dryad arbor'/'FUT', '136196').
card_timeshifted('dryad arbor'/'FUT')
.
card_in_set('dust of moments', 'FUT').
card_original_type('dust of moments'/'FUT', 'Instant').
card_original_text('dust of moments'/'FUT', 'Choose one Remove two time counters from each permanent and each suspended card; or put two time counters on each permanent with a time counter on it and each suspended card.').
card_first_print('dust of moments', 'FUT').
card_image_name('dust of moments'/'FUT', 'dust of moments').
card_uid('dust of moments'/'FUT', 'FUT:Dust of Moments:dust of moments').
card_rarity('dust of moments'/'FUT', 'Uncommon').
card_artist('dust of moments'/'FUT', 'Zoltan Boros & Gabor Szikszai').
card_number('dust of moments'/'FUT', '5').
card_multiverse_id('dust of moments'/'FUT', '136207').

card_in_set('edge of autumn', 'FUT').
card_original_type('edge of autumn'/'FUT', 'Sorcery').
card_original_text('edge of autumn'/'FUT', 'If you control four or fewer lands, search your library for a basic land card, put it into play tapped, then shuffle your library.\nCycling—Sacrifice a land. (Sacrifice a land, Discard this card: Draw a card.)').
card_first_print('edge of autumn', 'FUT').
card_image_name('edge of autumn'/'FUT', 'edge of autumn').
card_uid('edge of autumn'/'FUT', 'FUT:Edge of Autumn:edge of autumn').
card_rarity('edge of autumn'/'FUT', 'Common').
card_artist('edge of autumn'/'FUT', 'Jean-Sébastien Rossbach').
card_number('edge of autumn'/'FUT', '144').
card_flavor_text('edge of autumn'/'FUT', '\"At harvest are seeds of next year sown.\"').
card_multiverse_id('edge of autumn'/'FUT', '126132').
card_timeshifted('edge of autumn'/'FUT')
.
card_in_set('emberwilde augur', 'FUT').
card_original_type('emberwilde augur'/'FUT', 'Creature — Goblin Shaman').
card_original_text('emberwilde augur'/'FUT', 'Sacrifice Emberwilde Augur: Emberwilde Augur deals 3 damage to target player. Play this ability only during your upkeep.').
card_first_print('emberwilde augur', 'FUT').
card_image_name('emberwilde augur'/'FUT', 'emberwilde augur').
card_uid('emberwilde augur'/'FUT', 'FUT:Emberwilde Augur:emberwilde augur').
card_rarity('emberwilde augur'/'FUT', 'Common').
card_artist('emberwilde augur'/'FUT', 'Brandon Kitkouski').
card_number('emberwilde augur'/'FUT', '97').
card_flavor_text('emberwilde augur'/'FUT', 'Legends say a djinn gave the goblins a gift they could never hope to master.').
card_multiverse_id('emberwilde augur'/'FUT', '132214').

card_in_set('emblem of the warmind', 'FUT').
card_original_type('emblem of the warmind'/'FUT', 'Enchantment — Aura').
card_original_text('emblem of the warmind'/'FUT', 'Enchant creature you control\nCreatures you control have haste.').
card_first_print('emblem of the warmind', 'FUT').
card_image_name('emblem of the warmind'/'FUT', 'emblem of the warmind').
card_uid('emblem of the warmind'/'FUT', 'FUT:Emblem of the Warmind:emblem of the warmind').
card_rarity('emblem of the warmind'/'FUT', 'Uncommon').
card_artist('emblem of the warmind'/'FUT', 'Paolo Parente').
card_number('emblem of the warmind'/'FUT', '112').
card_flavor_text('emblem of the warmind'/'FUT', '\"It takes but one spark to ignite a parched forest. Our hearts, I fear, are too dry for this new visitor.\"\n—Elek Longbeard').
card_multiverse_id('emblem of the warmind'/'FUT', '136215').
card_timeshifted('emblem of the warmind'/'FUT')
.
card_in_set('epochrasite', 'FUT').
card_original_type('epochrasite'/'FUT', 'Artifact Creature — Construct').
card_original_text('epochrasite'/'FUT', 'Epochrasite comes into play with three +1/+1 counters on it if you didn\'t play it from your hand.\nWhen Epochrasite is put into a graveyard from play, remove it from the game with three time counters on it and it gains suspend.').
card_first_print('epochrasite', 'FUT').
card_image_name('epochrasite'/'FUT', 'epochrasite').
card_uid('epochrasite'/'FUT', 'FUT:Epochrasite:epochrasite').
card_rarity('epochrasite'/'FUT', 'Rare').
card_artist('epochrasite'/'FUT', 'Michael Bruinsma').
card_number('epochrasite'/'FUT', '162').
card_multiverse_id('epochrasite'/'FUT', '136143').

card_in_set('even the odds', 'FUT').
card_original_type('even the odds'/'FUT', 'Instant').
card_original_text('even the odds'/'FUT', 'Play Even the Odds only if you control fewer creatures than each opponent.\nPut three 1/1 white Soldier creature tokens into play.').
card_first_print('even the odds', 'FUT').
card_image_name('even the odds'/'FUT', 'even the odds').
card_uid('even the odds'/'FUT', 'FUT:Even the Odds:even the odds').
card_rarity('even the odds'/'FUT', 'Uncommon').
card_artist('even the odds'/'FUT', 'Shishizaru').
card_number('even the odds'/'FUT', '6').
card_flavor_text('even the odds'/'FUT', '\"They who fall singing the battle hymn of Benalia go not unavenged.\"\n—Adom Capashen, Benalish commander').
card_multiverse_id('even the odds'/'FUT', '136192').

card_in_set('fatal attraction', 'FUT').
card_original_type('fatal attraction'/'FUT', 'Enchantment — Aura').
card_original_text('fatal attraction'/'FUT', 'Enchant creature\nWhen Fatal Attraction comes into play, it deals 2 damage to enchanted creature.\nAt the beginning of your upkeep, Fatal Attraction deals 4 damage to enchanted creature.').
card_first_print('fatal attraction', 'FUT').
card_image_name('fatal attraction'/'FUT', 'fatal attraction').
card_uid('fatal attraction'/'FUT', 'FUT:Fatal Attraction:fatal attraction').
card_rarity('fatal attraction'/'FUT', 'Common').
card_artist('fatal attraction'/'FUT', 'Zoltan Boros & Gabor Szikszai').
card_number('fatal attraction'/'FUT', '98').
card_multiverse_id('fatal attraction'/'FUT', '130699').

card_in_set('festering march', 'FUT').
card_original_type('festering march'/'FUT', 'Sorcery').
card_original_text('festering march'/'FUT', 'Creatures your opponents control get -1/-1 until end of turn. Remove Festering March from the game with three time counters on it.\nSuspend 3—{2}{B} (Rather than play this card from your hand, you may pay {2}{B} and remove it from the game with three time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, play it without paying its mana cost.)').
card_first_print('festering march', 'FUT').
card_image_name('festering march'/'FUT', 'festering march').
card_uid('festering march'/'FUT', 'FUT:Festering March:festering march').
card_rarity('festering march'/'FUT', 'Uncommon').
card_artist('festering march'/'FUT', 'Brandon Kitkouski').
card_number('festering march'/'FUT', '65').
card_multiverse_id('festering march'/'FUT', '126131').

card_in_set('fleshwrither', 'FUT').
card_original_type('fleshwrither'/'FUT', 'Creature — Horror').
card_original_text('fleshwrither'/'FUT', 'Transfigure {1}{B}{B} ({1}{B}{B}, Sacrifice this creature: Search your library for a creature card with the same converted mana cost as this creature and put that card into play. Then shuffle your library. Play only as a sorcery.)').
card_first_print('fleshwrither', 'FUT').
card_image_name('fleshwrither'/'FUT', 'fleshwrither').
card_uid('fleshwrither'/'FUT', 'FUT:Fleshwrither:fleshwrither').
card_rarity('fleshwrither'/'FUT', 'Uncommon').
card_artist('fleshwrither'/'FUT', 'Dave Allsop').
card_number('fleshwrither'/'FUT', '84').
card_multiverse_id('fleshwrither'/'FUT', '132220').
card_timeshifted('fleshwrither'/'FUT')
.
card_in_set('flowstone embrace', 'FUT').
card_original_type('flowstone embrace'/'FUT', 'Enchantment — Aura').
card_original_text('flowstone embrace'/'FUT', 'Enchant creature\n{T}: Enchanted creature gets +2/-2 until end of turn.').
card_first_print('flowstone embrace', 'FUT').
card_image_name('flowstone embrace'/'FUT', 'flowstone embrace').
card_uid('flowstone embrace'/'FUT', 'FUT:Flowstone Embrace:flowstone embrace').
card_rarity('flowstone embrace'/'FUT', 'Common').
card_artist('flowstone embrace'/'FUT', 'Vance Kovacs').
card_number('flowstone embrace'/'FUT', '113').
card_flavor_text('flowstone embrace'/'FUT', '\"The seventh symbol, the Mark of Stone, stores the power of ancient Rath.\"\n—Tome of Elemental Mastery').
card_multiverse_id('flowstone embrace'/'FUT', '132227').
card_timeshifted('flowstone embrace'/'FUT')
.
card_in_set('fomori nomad', 'FUT').
card_original_type('fomori nomad'/'FUT', 'Creature — Nomad Giant').
card_original_text('fomori nomad'/'FUT', '').
card_first_print('fomori nomad', 'FUT').
card_image_name('fomori nomad'/'FUT', 'fomori nomad').
card_uid('fomori nomad'/'FUT', 'FUT:Fomori Nomad:fomori nomad').
card_rarity('fomori nomad'/'FUT', 'Common').
card_artist('fomori nomad'/'FUT', 'Raymond Swanland').
card_number('fomori nomad'/'FUT', '114').
card_multiverse_id('fomori nomad'/'FUT', '126149').
card_timeshifted('fomori nomad'/'FUT')
.
card_in_set('force of savagery', 'FUT').
card_original_type('force of savagery'/'FUT', 'Creature — Elemental').
card_original_text('force of savagery'/'FUT', 'Trample').
card_first_print('force of savagery', 'FUT').
card_image_name('force of savagery'/'FUT', 'force of savagery').
card_uid('force of savagery'/'FUT', 'FUT:Force of Savagery:force of savagery').
card_rarity('force of savagery'/'FUT', 'Rare').
card_artist('force of savagery'/'FUT', 'Dan Scott').
card_number('force of savagery'/'FUT', '126').
card_flavor_text('force of savagery'/'FUT', '\"Life cannot be created and then abandoned. It must be nurtured and fed so that it may express its ripened might.\"\n—Freyalise').
card_multiverse_id('force of savagery'/'FUT', '130713').

card_in_set('foresee', 'FUT').
card_original_type('foresee'/'FUT', 'Sorcery').
card_original_text('foresee'/'FUT', 'Scry 4, then draw two cards. (To scry 4, look at the top four cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_first_print('foresee', 'FUT').
card_image_name('foresee'/'FUT', 'foresee').
card_uid('foresee'/'FUT', 'FUT:Foresee:foresee').
card_rarity('foresee'/'FUT', 'Common').
card_artist('foresee'/'FUT', 'Ron Spears').
card_number('foresee'/'FUT', '36').
card_flavor_text('foresee'/'FUT', 'Wind time\'s watch, and watch time unwind.').
card_multiverse_id('foresee'/'FUT', '130669').

card_in_set('frenzy sliver', 'FUT').
card_original_type('frenzy sliver'/'FUT', 'Creature — Sliver').
card_original_text('frenzy sliver'/'FUT', 'All Sliver creatures have frenzy 1. (Whenever a Sliver attacks and isn\'t blocked, it gets +1/+0 until end of turn.)').
card_first_print('frenzy sliver', 'FUT').
card_image_name('frenzy sliver'/'FUT', 'frenzy sliver').
card_uid('frenzy sliver'/'FUT', 'FUT:Frenzy Sliver:frenzy sliver').
card_rarity('frenzy sliver'/'FUT', 'Common').
card_artist('frenzy sliver'/'FUT', 'Glen Angus').
card_number('frenzy sliver'/'FUT', '85').
card_flavor_text('frenzy sliver'/'FUT', 'The Arturan conjurers took the strange, alien fossil and patterned their own twisted creations in its image.').
card_multiverse_id('frenzy sliver'/'FUT', '126187').
card_timeshifted('frenzy sliver'/'FUT')
.
card_in_set('gathan raiders', 'FUT').
card_original_type('gathan raiders'/'FUT', 'Creature — Human Warrior').
card_original_text('gathan raiders'/'FUT', 'Hellbent Gathan Raiders gets +2/+2 if you have no cards in hand.\nMorph—Discard a card. (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('gathan raiders', 'FUT').
card_image_name('gathan raiders'/'FUT', 'gathan raiders').
card_uid('gathan raiders'/'FUT', 'FUT:Gathan Raiders:gathan raiders').
card_rarity('gathan raiders'/'FUT', 'Common').
card_artist('gathan raiders'/'FUT', 'Paolo Parente').
card_number('gathan raiders'/'FUT', '99').
card_multiverse_id('gathan raiders'/'FUT', '130707').

card_in_set('ghostfire', 'FUT').
card_original_type('ghostfire'/'FUT', 'Instant').
card_original_text('ghostfire'/'FUT', 'Ghostfire is colorless.\nGhostfire deals 3 damage to target creature or player.').
card_first_print('ghostfire', 'FUT').
card_image_name('ghostfire'/'FUT', 'ghostfire').
card_uid('ghostfire'/'FUT', 'FUT:Ghostfire:ghostfire').
card_rarity('ghostfire'/'FUT', 'Common').
card_artist('ghostfire'/'FUT', 'Cyril Van Der Haegen').
card_number('ghostfire'/'FUT', '115').
card_flavor_text('ghostfire'/'FUT', 'Only those gifted with the eye of Ugin, the spirit dragon, can see his fiery breath.').
card_multiverse_id('ghostfire'/'FUT', '136044').
card_timeshifted('ghostfire'/'FUT')
.
card_in_set('gibbering descent', 'FUT').
card_original_type('gibbering descent'/'FUT', 'Enchantment').
card_original_text('gibbering descent'/'FUT', 'At the beginning of each player\'s upkeep, that player loses 1 life and discards a card.\nHellbent Skip your upkeep step if you have no cards in hand.\nMadness {2}{B}{B} (If you discard this card, you may play it for its madness cost instead of putting it into your graveyard.)').
card_first_print('gibbering descent', 'FUT').
card_image_name('gibbering descent'/'FUT', 'gibbering descent').
card_uid('gibbering descent'/'FUT', 'FUT:Gibbering Descent:gibbering descent').
card_rarity('gibbering descent'/'FUT', 'Rare').
card_artist('gibbering descent'/'FUT', 'Ittoku').
card_number('gibbering descent'/'FUT', '66').
card_multiverse_id('gibbering descent'/'FUT', '130690').

card_in_set('gift of granite', 'FUT').
card_original_type('gift of granite'/'FUT', 'Enchantment — Aura').
card_original_text('gift of granite'/'FUT', 'Flash (You may play this spell any time you could play an instant.)\nEnchant creature\nEnchanted creature gets +0/+2.').
card_first_print('gift of granite', 'FUT').
card_image_name('gift of granite'/'FUT', 'gift of granite').
card_uid('gift of granite'/'FUT', 'FUT:Gift of Granite:gift of granite').
card_rarity('gift of granite'/'FUT', 'Common').
card_artist('gift of granite'/'FUT', 'Hideaki Takamura').
card_number('gift of granite'/'FUT', '7').
card_flavor_text('gift of granite'/'FUT', 'The blessing adds both protection to the recipient and weightlessness to the stone.').
card_multiverse_id('gift of granite'/'FUT', '136200').

card_in_set('glittering wish', 'FUT').
card_original_type('glittering wish'/'FUT', 'Sorcery').
card_original_text('glittering wish'/'FUT', 'Choose a multicolored card you own from outside the game, reveal that card, and put it into your hand. Remove Glittering Wish from the game.').
card_first_print('glittering wish', 'FUT').
card_image_name('glittering wish'/'FUT', 'glittering wish').
card_uid('glittering wish'/'FUT', 'FUT:Glittering Wish:glittering wish').
card_rarity('glittering wish'/'FUT', 'Rare').
card_artist('glittering wish'/'FUT', 'John Donahue').
card_number('glittering wish'/'FUT', '156').
card_flavor_text('glittering wish'/'FUT', 'She wished for gold, but not for the strength to carry it.').
card_multiverse_id('glittering wish'/'FUT', '136157').

card_in_set('goldmeadow lookout', 'FUT').
card_original_type('goldmeadow lookout'/'FUT', 'Creature — Kithkin Spellshaper').
card_original_text('goldmeadow lookout'/'FUT', '{W}, {T}, Discard a card: Put a 1/1 white Kithkin Soldier creature token named Goldmeadow Harrier into play with \"{W}, {T}: Tap target creature.\"').
card_first_print('goldmeadow lookout', 'FUT').
card_image_name('goldmeadow lookout'/'FUT', 'goldmeadow lookout').
card_uid('goldmeadow lookout'/'FUT', 'FUT:Goldmeadow Lookout:goldmeadow lookout').
card_rarity('goldmeadow lookout'/'FUT', 'Uncommon').
card_artist('goldmeadow lookout'/'FUT', 'Steve Prescott').
card_number('goldmeadow lookout'/'FUT', '22').
card_multiverse_id('goldmeadow lookout'/'FUT', '130311').
card_timeshifted('goldmeadow lookout'/'FUT')
.
card_in_set('grave peril', 'FUT').
card_original_type('grave peril'/'FUT', 'Enchantment').
card_original_text('grave peril'/'FUT', 'When a nonblack creature comes into play, sacrifice Grave Peril. If you do, destroy that creature.').
card_first_print('grave peril', 'FUT').
card_image_name('grave peril'/'FUT', 'grave peril').
card_uid('grave peril'/'FUT', 'FUT:Grave Peril:grave peril').
card_rarity('grave peril'/'FUT', 'Common').
card_artist('grave peril'/'FUT', 'Daarken').
card_number('grave peril'/'FUT', '67').
card_flavor_text('grave peril'/'FUT', '\"Sometimes the grave does not wait for death before relishing its feast of flesh and bones.\"\n—Ezrith, druid of the Dark Hours').
card_multiverse_id('grave peril'/'FUT', '126159').

card_in_set('grave scrabbler', 'FUT').
card_original_type('grave scrabbler'/'FUT', 'Creature — Zombie').
card_original_text('grave scrabbler'/'FUT', 'Madness {1}{B} (If you discard this card, you may play it for its madness cost instead of putting it into your graveyard.)\nWhen Grave Scrabbler comes into play, if its madness cost was paid, you may return target creature card in a graveyard to its owner\'s hand.').
card_first_print('grave scrabbler', 'FUT').
card_image_name('grave scrabbler'/'FUT', 'grave scrabbler').
card_uid('grave scrabbler'/'FUT', 'FUT:Grave Scrabbler:grave scrabbler').
card_rarity('grave scrabbler'/'FUT', 'Common').
card_artist('grave scrabbler'/'FUT', 'Dave Allsop').
card_number('grave scrabbler'/'FUT', '86').
card_multiverse_id('grave scrabbler'/'FUT', '132211').
card_timeshifted('grave scrabbler'/'FUT')
.
card_in_set('graven cairns', 'FUT').
card_original_type('graven cairns'/'FUT', 'Land').
card_original_text('graven cairns'/'FUT', '{T}: Add {1} to your mana pool.\n{B/R}, {T}: Add {B}{B}, {B}{R}, or {R}{R} to your mana pool.').
card_first_print('graven cairns', 'FUT').
card_image_name('graven cairns'/'FUT', 'graven cairns').
card_uid('graven cairns'/'FUT', 'FUT:Graven Cairns:graven cairns').
card_rarity('graven cairns'/'FUT', 'Rare').
card_artist('graven cairns'/'FUT', 'Anthony S. Waters').
card_number('graven cairns'/'FUT', '175').
card_flavor_text('graven cairns'/'FUT', 'Shamans of Kar-Sengir claim that their sun sets because it can no longer bear the gaze of those pain-carved cliffs.').
card_multiverse_id('graven cairns'/'FUT', '130581').
card_timeshifted('graven cairns'/'FUT')
.
card_in_set('grinning ignus', 'FUT').
card_original_type('grinning ignus'/'FUT', 'Creature — Elemental').
card_original_text('grinning ignus'/'FUT', '{R}, Return Grinning Ignus to its owner\'s hand: Add {2}{R} to your mana pool. Play this ability only any time you could play a sorcery.').
card_first_print('grinning ignus', 'FUT').
card_image_name('grinning ignus'/'FUT', 'grinning ignus').
card_uid('grinning ignus'/'FUT', 'FUT:Grinning Ignus:grinning ignus').
card_rarity('grinning ignus'/'FUT', 'Common').
card_artist('grinning ignus'/'FUT', 'James Kei').
card_number('grinning ignus'/'FUT', '116').
card_flavor_text('grinning ignus'/'FUT', '\"Take care what you offer the ignus. Food, perhaps. Coins. But nothing flammable!\"\n—Stovic, village eccentric').
card_multiverse_id('grinning ignus'/'FUT', '136040').
card_timeshifted('grinning ignus'/'FUT')
.
card_in_set('grove of the burnwillows', 'FUT').
card_original_type('grove of the burnwillows'/'FUT', 'Land').
card_original_text('grove of the burnwillows'/'FUT', '{T}: Add {1} to your mana pool.\n{T}: Add {R} or {G} to your mana pool. Each opponent gains 1 life.').
card_first_print('grove of the burnwillows', 'FUT').
card_image_name('grove of the burnwillows'/'FUT', 'grove of the burnwillows').
card_uid('grove of the burnwillows'/'FUT', 'FUT:Grove of the Burnwillows:grove of the burnwillows').
card_rarity('grove of the burnwillows'/'FUT', 'Rare').
card_artist('grove of the burnwillows'/'FUT', 'David Hudnut').
card_number('grove of the burnwillows'/'FUT', '176').
card_flavor_text('grove of the burnwillows'/'FUT', 'Spring is the most beautiful season in the grove, when the new leaves open from their ember-buds in a race of leaping flames.').
card_multiverse_id('grove of the burnwillows'/'FUT', '130595').
card_timeshifted('grove of the burnwillows'/'FUT')
.
card_in_set('haze of rage', 'FUT').
card_original_type('haze of rage'/'FUT', 'Sorcery').
card_original_text('haze of rage'/'FUT', 'Buyback {2} (You may pay an additional {2} as you play this spell. If you do, put this card into your hand as it resolves.)\nCreatures you control get +1/+0 until end of turn.\nStorm (When you play this spell, copy it for each spell played before it this turn.)').
card_first_print('haze of rage', 'FUT').
card_image_name('haze of rage'/'FUT', 'haze of rage').
card_uid('haze of rage'/'FUT', 'FUT:Haze of Rage:haze of rage').
card_rarity('haze of rage'/'FUT', 'Uncommon').
card_artist('haze of rage'/'FUT', 'Paolo Parente').
card_number('haze of rage'/'FUT', '100').
card_multiverse_id('haze of rage'/'FUT', '130328').

card_in_set('heartwood storyteller', 'FUT').
card_original_type('heartwood storyteller'/'FUT', 'Creature — Treefolk').
card_original_text('heartwood storyteller'/'FUT', 'Whenever a player plays a noncreature spell, each of that player\'s opponents may draw a card.').
card_first_print('heartwood storyteller', 'FUT').
card_image_name('heartwood storyteller'/'FUT', 'heartwood storyteller').
card_uid('heartwood storyteller'/'FUT', 'FUT:Heartwood Storyteller:heartwood storyteller').
card_rarity('heartwood storyteller'/'FUT', 'Rare').
card_artist('heartwood storyteller'/'FUT', 'Anthony S. Waters').
card_number('heartwood storyteller'/'FUT', '127').
card_flavor_text('heartwood storyteller'/'FUT', 'His roots reach deep, nurtured not by soil and rain, but by millennia of experience.').
card_multiverse_id('heartwood storyteller'/'FUT', '132216').

card_in_set('henchfiend of ukor', 'FUT').
card_original_type('henchfiend of ukor'/'FUT', 'Creature — Ogre').
card_original_text('henchfiend of ukor'/'FUT', 'Haste\nEcho {1}{B} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice this permanent unless you pay its echo cost.)\n{B/R}: Henchfiend of Ukor gets +1/+0 until end of turn.').
card_first_print('henchfiend of ukor', 'FUT').
card_image_name('henchfiend of ukor'/'FUT', 'henchfiend of ukor').
card_uid('henchfiend of ukor'/'FUT', 'FUT:Henchfiend of Ukor:henchfiend of ukor').
card_rarity('henchfiend of ukor'/'FUT', 'Common').
card_artist('henchfiend of ukor'/'FUT', 'Nils Hamm').
card_number('henchfiend of ukor'/'FUT', '117').
card_multiverse_id('henchfiend of ukor'/'FUT', '130695').
card_timeshifted('henchfiend of ukor'/'FUT')
.
card_in_set('homing sliver', 'FUT').
card_original_type('homing sliver'/'FUT', 'Creature — Sliver').
card_original_text('homing sliver'/'FUT', 'Each Sliver card in each player\'s hand has slivercycling {3}.\nSlivercycling {3} ({3}, Discard this card: Search your library for a Sliver card, reveal it, and put it into your hand. Then shuffle your library.)').
card_first_print('homing sliver', 'FUT').
card_image_name('homing sliver'/'FUT', 'homing sliver').
card_uid('homing sliver'/'FUT', 'FUT:Homing Sliver:homing sliver').
card_rarity('homing sliver'/'FUT', 'Common').
card_artist('homing sliver'/'FUT', 'Trevor Hairsine').
card_number('homing sliver'/'FUT', '118').
card_multiverse_id('homing sliver'/'FUT', '126162').
card_timeshifted('homing sliver'/'FUT')
.
card_in_set('horizon canopy', 'FUT').
card_original_type('horizon canopy'/'FUT', 'Land').
card_original_text('horizon canopy'/'FUT', '{T}, Pay 1 life: Add {G} or {W} to your mana pool.\n{1}, {T}, Sacrifice Horizon Canopy: Draw a card.').
card_first_print('horizon canopy', 'FUT').
card_image_name('horizon canopy'/'FUT', 'horizon canopy').
card_uid('horizon canopy'/'FUT', 'FUT:Horizon Canopy:horizon canopy').
card_rarity('horizon canopy'/'FUT', 'Rare').
card_artist('horizon canopy'/'FUT', 'Michael Komarck').
card_number('horizon canopy'/'FUT', '177').
card_flavor_text('horizon canopy'/'FUT', 'The great leaves are resilient underfoot. Heavy steps do not bruise them, but release a sweet and spicy scent.').
card_multiverse_id('horizon canopy'/'FUT', '130574').
card_timeshifted('horizon canopy'/'FUT')
.
card_in_set('ichor slick', 'FUT').
card_original_type('ichor slick'/'FUT', 'Sorcery').
card_original_text('ichor slick'/'FUT', 'Target creature gets -3/-3 until end of turn.\nCycling {2} ({2}, Discard this card: Draw a card.)\nMadness {3}{B} (If you discard this card, you may play it for its madness cost instead of putting it into your graveyard.)').
card_first_print('ichor slick', 'FUT').
card_image_name('ichor slick'/'FUT', 'ichor slick').
card_uid('ichor slick'/'FUT', 'FUT:Ichor Slick:ichor slick').
card_rarity('ichor slick'/'FUT', 'Common').
card_artist('ichor slick'/'FUT', 'Zoltan Boros & Gabor Szikszai').
card_number('ichor slick'/'FUT', '68').
card_multiverse_id('ichor slick'/'FUT', '130683').

card_in_set('imperial mask', 'FUT').
card_original_type('imperial mask'/'FUT', 'Enchantment').
card_original_text('imperial mask'/'FUT', 'When Imperial Mask comes into play, if it\'s not a token, each of your teammates puts a token into play that\'s a copy of Imperial Mask.\nYou can\'t be the target of spells or abilities your opponents control.').
card_first_print('imperial mask', 'FUT').
card_image_name('imperial mask'/'FUT', 'imperial mask').
card_uid('imperial mask'/'FUT', 'FUT:Imperial Mask:imperial mask').
card_rarity('imperial mask'/'FUT', 'Rare').
card_artist('imperial mask'/'FUT', 'Christopher Moeller').
card_number('imperial mask'/'FUT', '23').
card_multiverse_id('imperial mask'/'FUT', '136201').
card_timeshifted('imperial mask'/'FUT')
.
card_in_set('imperiosaur', 'FUT').
card_original_type('imperiosaur'/'FUT', 'Creature — Lizard').
card_original_text('imperiosaur'/'FUT', 'Spend only mana produced by basic lands to play Imperiosaur.').
card_first_print('imperiosaur', 'FUT').
card_image_name('imperiosaur'/'FUT', 'imperiosaur').
card_uid('imperiosaur'/'FUT', 'FUT:Imperiosaur:imperiosaur').
card_rarity('imperiosaur'/'FUT', 'Uncommon').
card_artist('imperiosaur'/'FUT', 'Lars Grant-West').
card_number('imperiosaur'/'FUT', '145').
card_flavor_text('imperiosaur'/'FUT', '\"An ancient, powerful force has overtaken the valley. I sympathize for its former inhabitants, but I rejoice for the land itself.\"\n—Olanti, Muraganda druid').
card_multiverse_id('imperiosaur'/'FUT', '130634').
card_timeshifted('imperiosaur'/'FUT')
.
card_in_set('infiltrator il-kor', 'FUT').
card_original_type('infiltrator il-kor'/'FUT', 'Creature — Kor Rogue').
card_original_text('infiltrator il-kor'/'FUT', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nSuspend 2—{1}{U} (Rather than play this card from your hand, you may pay {1}{U} and remove it from the game with two time counters on it. At the beginning of your upkeep, remove a time counter. When you remove the last, play it without paying its mana cost. It has haste.)').
card_first_print('infiltrator il-kor', 'FUT').
card_image_name('infiltrator il-kor'/'FUT', 'infiltrator il-kor').
card_uid('infiltrator il-kor'/'FUT', 'FUT:Infiltrator il-Kor:infiltrator il-kor').
card_rarity('infiltrator il-kor'/'FUT', 'Common').
card_artist('infiltrator il-kor'/'FUT', 'Jon Foster').
card_number('infiltrator il-kor'/'FUT', '37').
card_multiverse_id('infiltrator il-kor'/'FUT', '130694').

card_in_set('intervention pact', 'FUT').
card_original_type('intervention pact'/'FUT', 'Instant').
card_original_text('intervention pact'/'FUT', 'Intervention Pact is white.\nThe next time a source of your choice would deal damage to you this turn, prevent that damage. You gain life equal to the damage prevented this way.\nAt the beginning of your next upkeep, pay {1}{W}{W}. If you don\'t, you lose the game.').
card_first_print('intervention pact', 'FUT').
card_image_name('intervention pact'/'FUT', 'intervention pact').
card_uid('intervention pact'/'FUT', 'FUT:Intervention Pact:intervention pact').
card_rarity('intervention pact'/'FUT', 'Rare').
card_artist('intervention pact'/'FUT', 'Dave Kendall').
card_number('intervention pact'/'FUT', '8').
card_multiverse_id('intervention pact'/'FUT', '130680').

card_in_set('jhoira of the ghitu', 'FUT').
card_original_type('jhoira of the ghitu'/'FUT', 'Legendary Creature — Human Wizard').
card_original_text('jhoira of the ghitu'/'FUT', '{2}, Remove a nonland card in your hand from the game: Put four time counters on the removed card. If it doesn\'t have suspend, it gains suspend. (At the beginning of your upkeep, remove a time counter from that card. When the last is removed, play it without paying its mana cost. If it\'s a creature, it has haste.)').
card_first_print('jhoira of the ghitu', 'FUT').
card_image_name('jhoira of the ghitu'/'FUT', 'jhoira of the ghitu').
card_uid('jhoira of the ghitu'/'FUT', 'FUT:Jhoira of the Ghitu:jhoira of the ghitu').
card_rarity('jhoira of the ghitu'/'FUT', 'Rare').
card_artist('jhoira of the ghitu'/'FUT', 'Kev Walker').
card_number('jhoira of the ghitu'/'FUT', '157').
card_multiverse_id('jhoira of the ghitu'/'FUT', '136194').

card_in_set('judge unworthy', 'FUT').
card_original_type('judge unworthy'/'FUT', 'Instant').
card_original_text('judge unworthy'/'FUT', 'Choose target attacking or blocking creature. Scry 3, then reveal the top card of your library. Judge Unworthy deals damage equal to that card\'s converted mana cost to that creature. (To scry 3, look at the top three cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_first_print('judge unworthy', 'FUT').
card_image_name('judge unworthy'/'FUT', 'judge unworthy').
card_uid('judge unworthy'/'FUT', 'FUT:Judge Unworthy:judge unworthy').
card_rarity('judge unworthy'/'FUT', 'Common').
card_artist('judge unworthy'/'FUT', 'Greg Staples').
card_number('judge unworthy'/'FUT', '9').
card_multiverse_id('judge unworthy'/'FUT', '126164').

card_in_set('kavu primarch', 'FUT').
card_original_type('kavu primarch'/'FUT', 'Creature — Kavu').
card_original_text('kavu primarch'/'FUT', 'Convoke (Each creature you tap while playing this spell reduces its total cost by {1} or by one mana of that creature\'s color.)\nKicker {4} (You may pay an additional {4} as you play this spell.)\nIf the kicker cost was paid, Kavu Primarch comes into play with four +1/+1 counters on it.').
card_first_print('kavu primarch', 'FUT').
card_image_name('kavu primarch'/'FUT', 'kavu primarch').
card_uid('kavu primarch'/'FUT', 'FUT:Kavu Primarch:kavu primarch').
card_rarity('kavu primarch'/'FUT', 'Common').
card_artist('kavu primarch'/'FUT', 'Kev Walker').
card_number('kavu primarch'/'FUT', '128').
card_multiverse_id('kavu primarch'/'FUT', '132218').

card_in_set('keldon megaliths', 'FUT').
card_original_type('keldon megaliths'/'FUT', 'Land').
card_original_text('keldon megaliths'/'FUT', 'Keldon Megaliths comes into play tapped.\n{T}: Add {R} to your mana pool.\nHellbent {1}{R}, {T}: Keldon Megaliths deals 1 damage to target creature or player. Play this ability only if you have no cards in hand.').
card_first_print('keldon megaliths', 'FUT').
card_image_name('keldon megaliths'/'FUT', 'keldon megaliths').
card_uid('keldon megaliths'/'FUT', 'FUT:Keldon Megaliths:keldon megaliths').
card_rarity('keldon megaliths'/'FUT', 'Uncommon').
card_artist('keldon megaliths'/'FUT', 'Philip Straub').
card_number('keldon megaliths'/'FUT', '170').
card_multiverse_id('keldon megaliths'/'FUT', '136046').

card_in_set('knight of sursi', 'FUT').
card_original_type('knight of sursi'/'FUT', 'Creature — Human Knight').
card_original_text('knight of sursi'/'FUT', 'Flying, flanking\nSuspend 3—{W} (Rather than play this card from your hand, you may pay {W} and remove it from the game with three time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, play it without paying its mana cost. It has haste.)').
card_first_print('knight of sursi', 'FUT').
card_image_name('knight of sursi'/'FUT', 'knight of sursi').
card_uid('knight of sursi'/'FUT', 'FUT:Knight of Sursi:knight of sursi').
card_rarity('knight of sursi'/'FUT', 'Common').
card_artist('knight of sursi'/'FUT', 'Cyril Van Der Haegen').
card_number('knight of sursi'/'FUT', '10').
card_multiverse_id('knight of sursi'/'FUT', '130698').

card_in_set('korlash, heir to blackblade', 'FUT').
card_original_type('korlash, heir to blackblade'/'FUT', 'Legendary Creature — Zombie Warrior').
card_original_text('korlash, heir to blackblade'/'FUT', 'Korlash, Heir to Blackblade\'s power and toughness are each equal to the number of Swamps you control.\n{1}{B}: Regenerate Korlash.\nGrandeur Discard another card named Korlash, Heir to Blackblade: Search your library for up to two Swamp cards, put them into play tapped, then shuffle your library.').
card_image_name('korlash, heir to blackblade'/'FUT', 'korlash, heir to blackblade').
card_uid('korlash, heir to blackblade'/'FUT', 'FUT:Korlash, Heir to Blackblade:korlash, heir to blackblade').
card_rarity('korlash, heir to blackblade'/'FUT', 'Rare').
card_artist('korlash, heir to blackblade'/'FUT', 'Daarken').
card_number('korlash, heir to blackblade'/'FUT', '87').
card_multiverse_id('korlash, heir to blackblade'/'FUT', '136208').
card_timeshifted('korlash, heir to blackblade'/'FUT')
.
card_in_set('leaden fists', 'FUT').
card_original_type('leaden fists'/'FUT', 'Enchantment — Aura').
card_original_text('leaden fists'/'FUT', 'Flash (You may play this spell any time you could play an instant.)\nEnchant creature\nEnchanted creature gets +3/+3 and doesn\'t untap during its controller\'s untap step.').
card_first_print('leaden fists', 'FUT').
card_image_name('leaden fists'/'FUT', 'leaden fists').
card_uid('leaden fists'/'FUT', 'FUT:Leaden Fists:leaden fists').
card_rarity('leaden fists'/'FUT', 'Common').
card_artist('leaden fists'/'FUT', 'Shishizaru').
card_number('leaden fists'/'FUT', '38').
card_multiverse_id('leaden fists'/'FUT', '126139').

card_in_set('linessa, zephyr mage', 'FUT').
card_original_type('linessa, zephyr mage'/'FUT', 'Legendary Creature — Human Wizard').
card_original_text('linessa, zephyr mage'/'FUT', '{X}{U}{U}, {T}: Return target creature with converted mana cost X to its owner\'s hand.\nGrandeur Discard another card named Linessa, Zephyr Mage: Target player returns a creature he or she controls to its owner\'s hand, then repeats this process for an artifact, an enchantment, and a land.').
card_first_print('linessa, zephyr mage', 'FUT').
card_image_name('linessa, zephyr mage'/'FUT', 'linessa, zephyr mage').
card_uid('linessa, zephyr mage'/'FUT', 'FUT:Linessa, Zephyr Mage:linessa, zephyr mage').
card_rarity('linessa, zephyr mage'/'FUT', 'Rare').
card_artist('linessa, zephyr mage'/'FUT', 'Jim Murray').
card_number('linessa, zephyr mage'/'FUT', '51').
card_multiverse_id('linessa, zephyr mage'/'FUT', '136198').
card_timeshifted('linessa, zephyr mage'/'FUT')
.
card_in_set('llanowar augur', 'FUT').
card_original_type('llanowar augur'/'FUT', 'Creature — Elf Shaman').
card_original_text('llanowar augur'/'FUT', 'Sacrifice Llanowar Augur: Target creature gets +3/+3 and gains trample until end of turn. Play this ability only during your upkeep.').
card_first_print('llanowar augur', 'FUT').
card_image_name('llanowar augur'/'FUT', 'llanowar augur').
card_uid('llanowar augur'/'FUT', 'FUT:Llanowar Augur:llanowar augur').
card_rarity('llanowar augur'/'FUT', 'Common').
card_artist('llanowar augur'/'FUT', 'Tsutomu Kawade').
card_number('llanowar augur'/'FUT', '129').
card_flavor_text('llanowar augur'/'FUT', '\"I am irrelevant. The future I hold in my hands is everything.\"').
card_multiverse_id('llanowar augur'/'FUT', '132213').

card_in_set('llanowar empath', 'FUT').
card_original_type('llanowar empath'/'FUT', 'Creature — Elf Shaman').
card_original_text('llanowar empath'/'FUT', 'When Llanowar Empath comes into play, scry 2, then reveal the top card of your library. If it\'s a creature card, put it into your hand. (To scry 2, look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_first_print('llanowar empath', 'FUT').
card_image_name('llanowar empath'/'FUT', 'llanowar empath').
card_uid('llanowar empath'/'FUT', 'FUT:Llanowar Empath:llanowar empath').
card_rarity('llanowar empath'/'FUT', 'Common').
card_artist('llanowar empath'/'FUT', 'Warren Mahy').
card_number('llanowar empath'/'FUT', '130').
card_multiverse_id('llanowar empath'/'FUT', '126147').

card_in_set('llanowar mentor', 'FUT').
card_original_type('llanowar mentor'/'FUT', 'Creature — Elf Spellshaper').
card_original_text('llanowar mentor'/'FUT', '{G}, {T}, Discard a card: Put a 1/1 green Elf Druid creature token named Llanowar Elves into play with \"{T}: Add {G} to your mana pool.\"').
card_first_print('llanowar mentor', 'FUT').
card_image_name('llanowar mentor'/'FUT', 'llanowar mentor').
card_uid('llanowar mentor'/'FUT', 'FUT:Llanowar Mentor:llanowar mentor').
card_rarity('llanowar mentor'/'FUT', 'Uncommon').
card_artist('llanowar mentor'/'FUT', 'Trevor Hairsine').
card_number('llanowar mentor'/'FUT', '131').
card_multiverse_id('llanowar mentor'/'FUT', '126166').

card_in_set('llanowar reborn', 'FUT').
card_original_type('llanowar reborn'/'FUT', 'Land').
card_original_text('llanowar reborn'/'FUT', 'Llanowar Reborn comes into play tapped.\n{T}: Add {G} to your mana pool.\nGraft 1 (This land comes into play with a +1/+1 counter on it. Whenever a creature comes into play, you may move a +1/+1 counter from this land onto it.)').
card_first_print('llanowar reborn', 'FUT').
card_image_name('llanowar reborn'/'FUT', 'llanowar reborn').
card_uid('llanowar reborn'/'FUT', 'FUT:Llanowar Reborn:llanowar reborn').
card_rarity('llanowar reborn'/'FUT', 'Uncommon').
card_artist('llanowar reborn'/'FUT', 'Philip Straub').
card_number('llanowar reborn'/'FUT', '171').
card_multiverse_id('llanowar reborn'/'FUT', '136035').

card_in_set('logic knot', 'FUT').
card_original_type('logic knot'/'FUT', 'Instant').
card_original_text('logic knot'/'FUT', 'Delve (You may remove any number of cards in your graveyard from the game as you play this spell. It costs {1} less to play for each card removed this way.)\nCounter target spell unless its controller pays {X}.').
card_first_print('logic knot', 'FUT').
card_image_name('logic knot'/'FUT', 'logic knot').
card_uid('logic knot'/'FUT', 'FUT:Logic Knot:logic knot').
card_rarity('logic knot'/'FUT', 'Common').
card_artist('logic knot'/'FUT', 'Glen Angus').
card_number('logic knot'/'FUT', '52').
card_multiverse_id('logic knot'/'FUT', '126151').
card_timeshifted('logic knot'/'FUT')
.
card_in_set('lost auramancers', 'FUT').
card_original_type('lost auramancers'/'FUT', 'Creature — Human Wizard').
card_original_text('lost auramancers'/'FUT', 'Vanishing 3 (This permanent comes into play with three time counters on it. At the beginning of your upkeep, remove a time counter from it. When the last is removed, sacrifice it.)\nWhen Lost Auramancers is put into a graveyard from play, if it had no time counters on it, you may search your library for an enchantment card and put it into play. If you do, shuffle your library.').
card_first_print('lost auramancers', 'FUT').
card_image_name('lost auramancers'/'FUT', 'lost auramancers').
card_uid('lost auramancers'/'FUT', 'FUT:Lost Auramancers:lost auramancers').
card_rarity('lost auramancers'/'FUT', 'Uncommon').
card_artist('lost auramancers'/'FUT', 'Brandon Kitkouski').
card_number('lost auramancers'/'FUT', '11').
card_multiverse_id('lost auramancers'/'FUT', '126146').

card_in_set('lost hours', 'FUT').
card_original_type('lost hours'/'FUT', 'Sorcery').
card_original_text('lost hours'/'FUT', 'Target player reveals his or her hand. Choose a nonland card from it. That player puts that card into his or her library third from the top.').
card_first_print('lost hours', 'FUT').
card_image_name('lost hours'/'FUT', 'lost hours').
card_uid('lost hours'/'FUT', 'FUT:Lost Hours:lost hours').
card_rarity('lost hours'/'FUT', 'Common').
card_artist('lost hours'/'FUT', 'Jon Foster').
card_number('lost hours'/'FUT', '69').
card_flavor_text('lost hours'/'FUT', '\"Your thoughts do not interest me nearly as much as the absence thereof.\"\n—Gorghul, augur of skulls').
card_multiverse_id('lost hours'/'FUT', '130332').

card_in_set('lucent liminid', 'FUT').
card_original_type('lucent liminid'/'FUT', 'Enchantment Creature — Elemental').
card_original_text('lucent liminid'/'FUT', 'Flying').
card_first_print('lucent liminid', 'FUT').
card_image_name('lucent liminid'/'FUT', 'lucent liminid').
card_uid('lucent liminid'/'FUT', 'FUT:Lucent Liminid:lucent liminid').
card_rarity('lucent liminid'/'FUT', 'Common').
card_artist('lucent liminid'/'FUT', 'Eric Fortune').
card_number('lucent liminid'/'FUT', '24').
card_flavor_text('lucent liminid'/'FUT', 'It is a herald of the sun goddess, projected from the hallowed glass whenever her light passes through.').
card_multiverse_id('lucent liminid'/'FUT', '130691').
card_timeshifted('lucent liminid'/'FUT')
.
card_in_set('lumithread field', 'FUT').
card_original_type('lumithread field'/'FUT', 'Enchantment').
card_original_text('lumithread field'/'FUT', 'Creatures you control get +0/+1.\nMorph {1}{W} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('lumithread field', 'FUT').
card_image_name('lumithread field'/'FUT', 'lumithread field').
card_uid('lumithread field'/'FUT', 'FUT:Lumithread Field:lumithread field').
card_rarity('lumithread field'/'FUT', 'Common').
card_artist('lumithread field'/'FUT', 'Scott Altmann').
card_number('lumithread field'/'FUT', '25').
card_flavor_text('lumithread field'/'FUT', 'Having conquered the Bastion of Suns, the Ota took its light for their own.').
card_multiverse_id('lumithread field'/'FUT', '126186').
card_timeshifted('lumithread field'/'FUT')
.
card_in_set('lymph sliver', 'FUT').
card_original_type('lymph sliver'/'FUT', 'Creature — Sliver').
card_original_text('lymph sliver'/'FUT', 'All Sliver creatures have absorb 1. (If a source would deal damage to a Sliver, prevent 1 of that damage.)').
card_first_print('lymph sliver', 'FUT').
card_image_name('lymph sliver'/'FUT', 'lymph sliver').
card_uid('lymph sliver'/'FUT', 'FUT:Lymph Sliver:lymph sliver').
card_rarity('lymph sliver'/'FUT', 'Common').
card_artist('lymph sliver'/'FUT', 'Ittoku').
card_number('lymph sliver'/'FUT', '26').
card_flavor_text('lymph sliver'/'FUT', 'Its skin remains in a flowing, liquid state that keeps its body coated with potent restorative lymph.').
card_multiverse_id('lymph sliver'/'FUT', '126165').
card_timeshifted('lymph sliver'/'FUT')
.
card_in_set('maelstrom djinn', 'FUT').
card_original_type('maelstrom djinn'/'FUT', 'Creature — Djinn').
card_original_text('maelstrom djinn'/'FUT', 'Flying\nMorph {2}{U}\nWhen Maelstrom Djinn is turned face up, put two time counters on it and it gains vanishing. (At the beginning of your upkeep, remove a time counter from it. When the last is removed, sacrifice it.)').
card_first_print('maelstrom djinn', 'FUT').
card_image_name('maelstrom djinn'/'FUT', 'maelstrom djinn').
card_uid('maelstrom djinn'/'FUT', 'FUT:Maelstrom Djinn:maelstrom djinn').
card_rarity('maelstrom djinn'/'FUT', 'Rare').
card_artist('maelstrom djinn'/'FUT', 'Hideaki Takamura').
card_number('maelstrom djinn'/'FUT', '39').
card_multiverse_id('maelstrom djinn'/'FUT', '136195').

card_in_set('magus of the abyss', 'FUT').
card_original_type('magus of the abyss'/'FUT', 'Creature — Human Wizard').
card_original_text('magus of the abyss'/'FUT', 'At the beginning of each player\'s upkeep, destroy target nonartifact creature that player controls of his or her choice. It can\'t be regenerated.').
card_first_print('magus of the abyss', 'FUT').
card_image_name('magus of the abyss'/'FUT', 'magus of the abyss').
card_uid('magus of the abyss'/'FUT', 'FUT:Magus of the Abyss:magus of the abyss').
card_rarity('magus of the abyss'/'FUT', 'Rare').
card_artist('magus of the abyss'/'FUT', 'Kev Walker').
card_number('magus of the abyss'/'FUT', '70').
card_flavor_text('magus of the abyss'/'FUT', 'He climbs a staircase of falling souls, fighting the continuous pull of the void.').
card_multiverse_id('magus of the abyss'/'FUT', '136033').

card_in_set('magus of the future', 'FUT').
card_original_type('magus of the future'/'FUT', 'Creature — Human Wizard').
card_original_text('magus of the future'/'FUT', 'Play with the top card of your library revealed.\nYou may play the top card of your library.').
card_first_print('magus of the future', 'FUT').
card_image_name('magus of the future'/'FUT', 'magus of the future').
card_uid('magus of the future'/'FUT', 'FUT:Magus of the Future:magus of the future').
card_rarity('magus of the future'/'FUT', 'Rare').
card_artist('magus of the future'/'FUT', 'Anthony Francisco').
card_number('magus of the future'/'FUT', '40').
card_flavor_text('magus of the future'/'FUT', 'Those gifted with the sight have one eye in the present and the other in the future.').
card_multiverse_id('magus of the future'/'FUT', '136051').

card_in_set('magus of the moat', 'FUT').
card_original_type('magus of the moat'/'FUT', 'Creature — Human Wizard').
card_original_text('magus of the moat'/'FUT', 'Creatures without flying can\'t attack.').
card_first_print('magus of the moat', 'FUT').
card_image_name('magus of the moat'/'FUT', 'magus of the moat').
card_uid('magus of the moat'/'FUT', 'FUT:Magus of the Moat:magus of the moat').
card_rarity('magus of the moat'/'FUT', 'Rare').
card_artist('magus of the moat'/'FUT', 'John Avon').
card_number('magus of the moat'/'FUT', '12').
card_flavor_text('magus of the moat'/'FUT', '\"The spirits of the mythic ones ever circle their beloved keep, forbidding entry to all who come with the heavy tread of hate.\"').
card_multiverse_id('magus of the moat'/'FUT', '136148').

card_in_set('magus of the moon', 'FUT').
card_original_type('magus of the moon'/'FUT', 'Creature — Human Wizard').
card_original_text('magus of the moon'/'FUT', 'Nonbasic lands are Mountains.').
card_first_print('magus of the moon', 'FUT').
card_image_name('magus of the moon'/'FUT', 'magus of the moon').
card_uid('magus of the moon'/'FUT', 'FUT:Magus of the Moon:magus of the moon').
card_rarity('magus of the moon'/'FUT', 'Rare').
card_artist('magus of the moon'/'FUT', 'Franz Vohwinkel').
card_number('magus of the moon'/'FUT', '101').
card_flavor_text('magus of the moon'/'FUT', 'Tidal forces of the blood moon wrench and buckle the land, drawing monoliths of stone and soil toward the flaming orb.').
card_multiverse_id('magus of the moon'/'FUT', '136152').

card_in_set('magus of the vineyard', 'FUT').
card_original_type('magus of the vineyard'/'FUT', 'Creature — Human Wizard').
card_original_text('magus of the vineyard'/'FUT', 'At the beginning of each player\'s precombat main phase, add {G}{G} to that player\'s mana pool.').
card_first_print('magus of the vineyard', 'FUT').
card_image_name('magus of the vineyard'/'FUT', 'magus of the vineyard').
card_uid('magus of the vineyard'/'FUT', 'FUT:Magus of the Vineyard:magus of the vineyard').
card_rarity('magus of the vineyard'/'FUT', 'Rare').
card_artist('magus of the vineyard'/'FUT', 'Jim Murray').
card_number('magus of the vineyard'/'FUT', '132').
card_flavor_text('magus of the vineyard'/'FUT', 'Battered and beaten by years of salt and claw, he never ceased to walk, and to seed. Only now, in this time of rebirth, do his seeds take root.').
card_multiverse_id('magus of the vineyard'/'FUT', '136159').

card_in_set('marshaling cry', 'FUT').
card_original_type('marshaling cry'/'FUT', 'Sorcery').
card_original_text('marshaling cry'/'FUT', 'Creatures you control get +1/+1 and gain vigilance until end of turn.\nCycling {2} ({2}, Discard this card: Draw a card.)\nFlashback {3}{W} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('marshaling cry', 'FUT').
card_image_name('marshaling cry'/'FUT', 'marshaling cry').
card_uid('marshaling cry'/'FUT', 'FUT:Marshaling Cry:marshaling cry').
card_rarity('marshaling cry'/'FUT', 'Common').
card_artist('marshaling cry'/'FUT', 'Vance Kovacs').
card_number('marshaling cry'/'FUT', '13').
card_multiverse_id('marshaling cry'/'FUT', '130689').

card_in_set('mass of ghouls', 'FUT').
card_original_type('mass of ghouls'/'FUT', 'Creature — Zombie Warrior').
card_original_text('mass of ghouls'/'FUT', '').
card_first_print('mass of ghouls', 'FUT').
card_image_name('mass of ghouls'/'FUT', 'mass of ghouls').
card_uid('mass of ghouls'/'FUT', 'FUT:Mass of Ghouls:mass of ghouls').
card_rarity('mass of ghouls'/'FUT', 'Common').
card_artist('mass of ghouls'/'FUT', 'Lucio Parrillo').
card_number('mass of ghouls'/'FUT', '88').
card_multiverse_id('mass of ghouls'/'FUT', '126158').
card_timeshifted('mass of ghouls'/'FUT')
.
card_in_set('mesmeric sliver', 'FUT').
card_original_type('mesmeric sliver'/'FUT', 'Creature — Sliver').
card_original_text('mesmeric sliver'/'FUT', 'All Slivers have \"When this permanent comes into play, you may fateseal 1.\" (Its controller looks at the top card of an opponent\'s library, then he or she may put that card on the bottom of that library.)').
card_first_print('mesmeric sliver', 'FUT').
card_image_name('mesmeric sliver'/'FUT', 'mesmeric sliver').
card_uid('mesmeric sliver'/'FUT', 'FUT:Mesmeric Sliver:mesmeric sliver').
card_rarity('mesmeric sliver'/'FUT', 'Common').
card_artist('mesmeric sliver'/'FUT', 'Michael Bruinsma').
card_number('mesmeric sliver'/'FUT', '53').
card_multiverse_id('mesmeric sliver'/'FUT', '136202').
card_timeshifted('mesmeric sliver'/'FUT')
.
card_in_set('minions\' murmurs', 'FUT').
card_original_type('minions\' murmurs'/'FUT', 'Sorcery').
card_original_text('minions\' murmurs'/'FUT', 'You draw X cards and you lose X life, where X is the number of creatures you control.').
card_first_print('minions\' murmurs', 'FUT').
card_image_name('minions\' murmurs'/'FUT', 'minions\' murmurs').
card_uid('minions\' murmurs'/'FUT', 'FUT:Minions\' Murmurs:minions\' murmurs').
card_rarity('minions\' murmurs'/'FUT', 'Uncommon').
card_artist('minions\' murmurs'/'FUT', 'Nils Hamm').
card_number('minions\' murmurs'/'FUT', '71').
card_flavor_text('minions\' murmurs'/'FUT', '\"They are quick to offer advice, those whose mistakes doomed them to undeath. How can I not listen to the wisdom of failure?\"\n—Ratadrabik of Urborg').
card_multiverse_id('minions\' murmurs'/'FUT', '130316').

card_in_set('mistmeadow skulk', 'FUT').
card_original_type('mistmeadow skulk'/'FUT', 'Creature — Kithkin Rogue').
card_original_text('mistmeadow skulk'/'FUT', 'Protection from converted mana cost 3 or greater\nLifelink (Whenever this creature deals damage, you gain that much life.)').
card_first_print('mistmeadow skulk', 'FUT').
card_image_name('mistmeadow skulk'/'FUT', 'mistmeadow skulk').
card_uid('mistmeadow skulk'/'FUT', 'FUT:Mistmeadow Skulk:mistmeadow skulk').
card_rarity('mistmeadow skulk'/'FUT', 'Uncommon').
card_artist('mistmeadow skulk'/'FUT', 'Omar Rayyan').
card_number('mistmeadow skulk'/'FUT', '27').
card_flavor_text('mistmeadow skulk'/'FUT', 'Doyo suspected the boggarts of brewing a plot against his crop, so he scythed away his grain to clear the sightlines.').
card_multiverse_id('mistmeadow skulk'/'FUT', '130346').
card_timeshifted('mistmeadow skulk'/'FUT')
.
card_in_set('molten disaster', 'FUT').
card_original_type('molten disaster'/'FUT', 'Sorcery').
card_original_text('molten disaster'/'FUT', 'Kicker {R} (You may pay an additional {R} as you play this spell.)\nIf the kicker cost was paid, Molten Disaster has split second. (As long as this spell is on the stack, players can\'t play spells or activated abilities that aren\'t mana abilities.)\nMolten Disaster deals X damage to each creature without flying and each player.').
card_first_print('molten disaster', 'FUT').
card_image_name('molten disaster'/'FUT', 'molten disaster').
card_uid('molten disaster'/'FUT', 'FUT:Molten Disaster:molten disaster').
card_rarity('molten disaster'/'FUT', 'Rare').
card_artist('molten disaster'/'FUT', 'Ittoku').
card_number('molten disaster'/'FUT', '102').
card_multiverse_id('molten disaster'/'FUT', '136154').

card_in_set('muraganda petroglyphs', 'FUT').
card_original_type('muraganda petroglyphs'/'FUT', 'Enchantment').
card_original_text('muraganda petroglyphs'/'FUT', 'Creatures with no abilities get +2/+2.').
card_first_print('muraganda petroglyphs', 'FUT').
card_image_name('muraganda petroglyphs'/'FUT', 'muraganda petroglyphs').
card_uid('muraganda petroglyphs'/'FUT', 'FUT:Muraganda Petroglyphs:muraganda petroglyphs').
card_rarity('muraganda petroglyphs'/'FUT', 'Rare').
card_artist('muraganda petroglyphs'/'FUT', 'Scott Altmann').
card_number('muraganda petroglyphs'/'FUT', '146').
card_flavor_text('muraganda petroglyphs'/'FUT', 'Some mages forsake their scrolls and libraries to learn at the feet of ancient trees and sacred stones.').
card_multiverse_id('muraganda petroglyphs'/'FUT', '130614').
card_timeshifted('muraganda petroglyphs'/'FUT')
.
card_in_set('mystic speculation', 'FUT').
card_original_type('mystic speculation'/'FUT', 'Sorcery').
card_original_text('mystic speculation'/'FUT', 'Buyback {2} (You may pay an additional {2} as you play this spell. If you do, put this card into your hand as it resolves.)\nScry 3 (Look at the top three cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_first_print('mystic speculation', 'FUT').
card_image_name('mystic speculation'/'FUT', 'mystic speculation').
card_uid('mystic speculation'/'FUT', 'FUT:Mystic Speculation:mystic speculation').
card_rarity('mystic speculation'/'FUT', 'Uncommon').
card_artist('mystic speculation'/'FUT', 'Trevor Hairsine').
card_number('mystic speculation'/'FUT', '41').
card_multiverse_id('mystic speculation'/'FUT', '126156').

card_in_set('nacatl war-pride', 'FUT').
card_original_type('nacatl war-pride'/'FUT', 'Creature — Cat Warrior').
card_original_text('nacatl war-pride'/'FUT', 'Nacatl War-Pride must be blocked by exactly one creature if able.\nWhenever Nacatl War-Pride attacks, put X tokens into play tapped and attacking that are copies of Nacatl War-Pride, where X is the number of creatures defending player controls. Remove the tokens from the game at end of turn.').
card_first_print('nacatl war-pride', 'FUT').
card_image_name('nacatl war-pride'/'FUT', 'nacatl war-pride').
card_uid('nacatl war-pride'/'FUT', 'FUT:Nacatl War-Pride:nacatl war-pride').
card_rarity('nacatl war-pride'/'FUT', 'Uncommon').
card_artist('nacatl war-pride'/'FUT', 'James Kei').
card_number('nacatl war-pride'/'FUT', '147').
card_multiverse_id('nacatl war-pride'/'FUT', '130588').
card_timeshifted('nacatl war-pride'/'FUT')
.
card_in_set('narcomoeba', 'FUT').
card_original_type('narcomoeba'/'FUT', 'Creature — Illusion').
card_original_text('narcomoeba'/'FUT', 'Flying\nWhen Narcomoeba is put into your graveyard from your library, you may put it into play.').
card_first_print('narcomoeba', 'FUT').
card_image_name('narcomoeba'/'FUT', 'narcomoeba').
card_uid('narcomoeba'/'FUT', 'FUT:Narcomoeba:narcomoeba').
card_rarity('narcomoeba'/'FUT', 'Uncommon').
card_artist('narcomoeba'/'FUT', 'Matt Stewart').
card_number('narcomoeba'/'FUT', '54').
card_flavor_text('narcomoeba'/'FUT', 'It was created by the Iquati as a living memory—one that objects to being forgotten.').
card_multiverse_id('narcomoeba'/'FUT', '136140').
card_timeshifted('narcomoeba'/'FUT')
.
card_in_set('nessian courser', 'FUT').
card_original_type('nessian courser'/'FUT', 'Creature — Centaur Warrior').
card_original_text('nessian courser'/'FUT', '').
card_first_print('nessian courser', 'FUT').
card_image_name('nessian courser'/'FUT', 'nessian courser').
card_uid('nessian courser'/'FUT', 'FUT:Nessian Courser:nessian courser').
card_rarity('nessian courser'/'FUT', 'Common').
card_artist('nessian courser'/'FUT', 'Vance Kovacs').
card_number('nessian courser'/'FUT', '148').
card_multiverse_id('nessian courser'/'FUT', '136138').
card_timeshifted('nessian courser'/'FUT')
.
card_in_set('new benalia', 'FUT').
card_original_type('new benalia'/'FUT', 'Land').
card_original_text('new benalia'/'FUT', 'New Benalia comes into play tapped.\nWhen New Benalia comes into play, scry 1. (Look at the top card of your library, then you may put that card on the bottom of your library.)\n{T}: Add {W} to your mana pool.').
card_first_print('new benalia', 'FUT').
card_image_name('new benalia'/'FUT', 'new benalia').
card_uid('new benalia'/'FUT', 'FUT:New Benalia:new benalia').
card_rarity('new benalia'/'FUT', 'Uncommon').
card_artist('new benalia'/'FUT', 'Richard Wright').
card_number('new benalia'/'FUT', '172').
card_multiverse_id('new benalia'/'FUT', '126198').

card_in_set('nihilith', 'FUT').
card_original_type('nihilith'/'FUT', 'Creature — Horror').
card_original_text('nihilith'/'FUT', 'Fear\nSuspend 7—{1}{B}\nWhenever a card is put into an opponent\'s graveyard from anywhere, if Nihilith is suspended, you may remove a time counter from Nihilith.').
card_first_print('nihilith', 'FUT').
card_image_name('nihilith'/'FUT', 'nihilith').
card_uid('nihilith'/'FUT', 'FUT:Nihilith:nihilith').
card_rarity('nihilith'/'FUT', 'Rare').
card_artist('nihilith'/'FUT', 'Dave Allsop').
card_number('nihilith'/'FUT', '72').
card_multiverse_id('nihilith'/'FUT', '136206').

card_in_set('nimbus maze', 'FUT').
card_original_type('nimbus maze'/'FUT', 'Land').
card_original_text('nimbus maze'/'FUT', '{T}: Add {1} to your mana pool.\n{T}: Add {W} to your mana pool. Play this ability only if you control an Island.\n{T}: Add {U} to your mana pool. Play this ability only if you control a Plains.').
card_first_print('nimbus maze', 'FUT').
card_image_name('nimbus maze'/'FUT', 'nimbus maze').
card_uid('nimbus maze'/'FUT', 'FUT:Nimbus Maze:nimbus maze').
card_rarity('nimbus maze'/'FUT', 'Rare').
card_artist('nimbus maze'/'FUT', 'Jason Chan').
card_number('nimbus maze'/'FUT', '178').
card_flavor_text('nimbus maze'/'FUT', 'To find its center is to find one\'s own.').
card_multiverse_id('nimbus maze'/'FUT', '136045').
card_timeshifted('nimbus maze'/'FUT')
.
card_in_set('nix', 'FUT').
card_original_type('nix'/'FUT', 'Instant').
card_original_text('nix'/'FUT', 'Counter target spell if no mana was spent to play it.').
card_first_print('nix', 'FUT').
card_image_name('nix'/'FUT', 'nix').
card_uid('nix'/'FUT', 'FUT:Nix:nix').
card_rarity('nix'/'FUT', 'Rare').
card_artist('nix'/'FUT', 'Brian Despain').
card_number('nix'/'FUT', '55').
card_flavor_text('nix'/'FUT', '\"There is no shortcut to work done true and well. The Creators learned this to their sorrow, the first time they made the world.\"\n—Githri the Prisoner').
card_multiverse_id('nix'/'FUT', '130564').
card_timeshifted('nix'/'FUT')
.
card_in_set('oblivion crown', 'FUT').
card_original_type('oblivion crown'/'FUT', 'Enchantment — Aura').
card_original_text('oblivion crown'/'FUT', 'Flash (You may play this spell any time you could play an instant.)\nEnchant creature\nEnchanted creature has \"Discard a card: This creature gets +1/+1 until end of turn.\"').
card_first_print('oblivion crown', 'FUT').
card_image_name('oblivion crown'/'FUT', 'oblivion crown').
card_uid('oblivion crown'/'FUT', 'FUT:Oblivion Crown:oblivion crown').
card_rarity('oblivion crown'/'FUT', 'Common').
card_artist('oblivion crown'/'FUT', 'Kev Walker').
card_number('oblivion crown'/'FUT', '73').
card_multiverse_id('oblivion crown'/'FUT', '132219').

card_in_set('oriss, samite guardian', 'FUT').
card_original_type('oriss, samite guardian'/'FUT', 'Legendary Creature — Human Cleric').
card_original_text('oriss, samite guardian'/'FUT', '{T}: Prevent all damage that would be dealt to target creature this turn.\nGrandeur Discard another card named Oriss, Samite Guardian: Target player can\'t play spells this turn, and creatures that player controls can\'t attack this turn.').
card_first_print('oriss, samite guardian', 'FUT').
card_image_name('oriss, samite guardian'/'FUT', 'oriss, samite guardian').
card_uid('oriss, samite guardian'/'FUT', 'FUT:Oriss, Samite Guardian:oriss, samite guardian').
card_rarity('oriss, samite guardian'/'FUT', 'Rare').
card_artist('oriss, samite guardian'/'FUT', 'Michael Komarck').
card_number('oriss, samite guardian'/'FUT', '28').
card_multiverse_id('oriss, samite guardian'/'FUT', '136210').
card_timeshifted('oriss, samite guardian'/'FUT')
.
card_in_set('pact of negation', 'FUT').
card_original_type('pact of negation'/'FUT', 'Instant').
card_original_text('pact of negation'/'FUT', 'Pact of Negation is blue.\nCounter target spell.\nAt the beginning of your next upkeep, pay {3}{U}{U}. If you don\'t, you lose the game.').
card_first_print('pact of negation', 'FUT').
card_image_name('pact of negation'/'FUT', 'pact of negation').
card_uid('pact of negation'/'FUT', 'FUT:Pact of Negation:pact of negation').
card_rarity('pact of negation'/'FUT', 'Rare').
card_artist('pact of negation'/'FUT', 'Jason Chan').
card_number('pact of negation'/'FUT', '42').
card_flavor_text('pact of negation'/'FUT', 'Those who expect betrayal at every turn are seldom disappointed.').
card_multiverse_id('pact of negation'/'FUT', '130701').

card_in_set('pact of the titan', 'FUT').
card_original_type('pact of the titan'/'FUT', 'Instant').
card_original_text('pact of the titan'/'FUT', 'Pact of the Titan is red.\nPut a 4/4 red Giant creature token into play.\nAt the beginning of your next upkeep, pay {4}{R}. If you don\'t, you lose the game.').
card_first_print('pact of the titan', 'FUT').
card_image_name('pact of the titan'/'FUT', 'pact of the titan').
card_uid('pact of the titan'/'FUT', 'FUT:Pact of the Titan:pact of the titan').
card_rarity('pact of the titan'/'FUT', 'Rare').
card_artist('pact of the titan'/'FUT', 'Raymond Swanland').
card_number('pact of the titan'/'FUT', '103').
card_multiverse_id('pact of the titan'/'FUT', '130638').

card_in_set('patrician\'s scorn', 'FUT').
card_original_type('patrician\'s scorn'/'FUT', 'Instant').
card_original_text('patrician\'s scorn'/'FUT', 'If you played another white spell this turn, you may play Patrician\'s Scorn without paying its mana cost.\nDestroy all enchantments.').
card_first_print('patrician\'s scorn', 'FUT').
card_image_name('patrician\'s scorn'/'FUT', 'patrician\'s scorn').
card_uid('patrician\'s scorn'/'FUT', 'FUT:Patrician\'s Scorn:patrician\'s scorn').
card_rarity('patrician\'s scorn'/'FUT', 'Common').
card_artist('patrician\'s scorn'/'FUT', 'John Avon').
card_number('patrician\'s scorn'/'FUT', '29').
card_flavor_text('patrician\'s scorn'/'FUT', 'Lady Farisa\'s contempt of tawdry kitsch grew even more infamous after her death.').
card_multiverse_id('patrician\'s scorn'/'FUT', '136213').
card_timeshifted('patrician\'s scorn'/'FUT')
.
card_in_set('petrified plating', 'FUT').
card_original_type('petrified plating'/'FUT', 'Enchantment — Aura').
card_original_text('petrified plating'/'FUT', 'Enchant creature\nEnchanted creature gets +2/+2.\nSuspend 2—{G} (Rather than play this card from your hand, you may pay {G} and remove it from the game with two time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, play it without paying its mana cost.)').
card_first_print('petrified plating', 'FUT').
card_image_name('petrified plating'/'FUT', 'petrified plating').
card_uid('petrified plating'/'FUT', 'FUT:Petrified Plating:petrified plating').
card_rarity('petrified plating'/'FUT', 'Common').
card_artist('petrified plating'/'FUT', 'Zoltan Boros & Gabor Szikszai').
card_number('petrified plating'/'FUT', '133').
card_multiverse_id('petrified plating'/'FUT', '132223').

card_in_set('phosphorescent feast', 'FUT').
card_original_type('phosphorescent feast'/'FUT', 'Sorcery').
card_original_text('phosphorescent feast'/'FUT', 'Reveal any number of cards in your hand. You gain 2 life for each green mana symbol in those cards\' mana costs.').
card_first_print('phosphorescent feast', 'FUT').
card_image_name('phosphorescent feast'/'FUT', 'phosphorescent feast').
card_uid('phosphorescent feast'/'FUT', 'FUT:Phosphorescent Feast:phosphorescent feast').
card_rarity('phosphorescent feast'/'FUT', 'Uncommon').
card_artist('phosphorescent feast'/'FUT', 'David Hudnut').
card_number('phosphorescent feast'/'FUT', '149').
card_flavor_text('phosphorescent feast'/'FUT', '\"Mushroom Queen sits among her spores,\nEnjoying life and avoiding chores.\"\n—Ibblian pixie ditty').
card_multiverse_id('phosphorescent feast'/'FUT', '126160').
card_timeshifted('phosphorescent feast'/'FUT')
.
card_in_set('pooling venom', 'FUT').
card_original_type('pooling venom'/'FUT', 'Enchantment — Aura').
card_original_text('pooling venom'/'FUT', 'Enchant land\nWhenever enchanted land becomes tapped, its controller loses 2 life.\n{3}{B}: Destroy enchanted land.').
card_first_print('pooling venom', 'FUT').
card_image_name('pooling venom'/'FUT', 'pooling venom').
card_uid('pooling venom'/'FUT', 'FUT:Pooling Venom:pooling venom').
card_rarity('pooling venom'/'FUT', 'Uncommon').
card_artist('pooling venom'/'FUT', 'Ron Spears').
card_number('pooling venom'/'FUT', '74').
card_flavor_text('pooling venom'/'FUT', 'The land can only hold so much poison before it begins to bite back.').
card_multiverse_id('pooling venom'/'FUT', '136211').

card_in_set('putrid cyclops', 'FUT').
card_original_type('putrid cyclops'/'FUT', 'Creature — Zombie Cyclops').
card_original_text('putrid cyclops'/'FUT', 'When Putrid Cyclops comes into play, scry 1, then reveal the top card of your library. Putrid Cyclops gets -X/-X until end of turn, where X is that card\'s converted mana cost. (To scry 1, look at the top card of your library, then you may put that card on the bottom of your library.)').
card_first_print('putrid cyclops', 'FUT').
card_image_name('putrid cyclops'/'FUT', 'putrid cyclops').
card_uid('putrid cyclops'/'FUT', 'FUT:Putrid Cyclops:putrid cyclops').
card_rarity('putrid cyclops'/'FUT', 'Common').
card_artist('putrid cyclops'/'FUT', 'Matt Cavotta').
card_number('putrid cyclops'/'FUT', '75').
card_multiverse_id('putrid cyclops'/'FUT', '126148').

card_in_set('pyromancer\'s swath', 'FUT').
card_original_type('pyromancer\'s swath'/'FUT', 'Enchantment').
card_original_text('pyromancer\'s swath'/'FUT', 'If an instant or sorcery source you control would deal damage to a creature or player, it deals that much damage plus 2 to that creature or player instead.\nAt end of turn, discard your hand.').
card_first_print('pyromancer\'s swath', 'FUT').
card_image_name('pyromancer\'s swath'/'FUT', 'pyromancer\'s swath').
card_uid('pyromancer\'s swath'/'FUT', 'FUT:Pyromancer\'s Swath:pyromancer\'s swath').
card_rarity('pyromancer\'s swath'/'FUT', 'Rare').
card_artist('pyromancer\'s swath'/'FUT', 'Hideaki Takamura').
card_number('pyromancer\'s swath'/'FUT', '104').
card_multiverse_id('pyromancer\'s swath'/'FUT', '136216').

card_in_set('quagnoth', 'FUT').
card_original_type('quagnoth'/'FUT', 'Creature — Beast').
card_original_text('quagnoth'/'FUT', 'Split second (As long as this spell is on the stack, players can\'t play spells or activated abilities that aren\'t mana abilities.)\nShroud (This permanent can\'t be the target of spells or abilities.)\nWhen a spell or ability an opponent controls causes you to discard Quagnoth, return it to your hand.').
card_first_print('quagnoth', 'FUT').
card_image_name('quagnoth'/'FUT', 'quagnoth').
card_uid('quagnoth'/'FUT', 'FUT:Quagnoth:quagnoth').
card_rarity('quagnoth'/'FUT', 'Rare').
card_artist('quagnoth'/'FUT', 'Thomas M. Baxa').
card_number('quagnoth'/'FUT', '150').
card_multiverse_id('quagnoth'/'FUT', '136153').
card_timeshifted('quagnoth'/'FUT')
.
card_in_set('quiet disrepair', 'FUT').
card_original_type('quiet disrepair'/'FUT', 'Enchantment — Aura').
card_original_text('quiet disrepair'/'FUT', 'Enchant artifact or enchantment\nAt the beginning of your upkeep, choose one Destroy enchanted permanent; or you gain 2 life.').
card_first_print('quiet disrepair', 'FUT').
card_image_name('quiet disrepair'/'FUT', 'quiet disrepair').
card_uid('quiet disrepair'/'FUT', 'FUT:Quiet Disrepair:quiet disrepair').
card_rarity('quiet disrepair'/'FUT', 'Common').
card_artist('quiet disrepair'/'FUT', 'Glen Angus').
card_number('quiet disrepair'/'FUT', '134').
card_flavor_text('quiet disrepair'/'FUT', '\"Artifice has always stood upon nature\'s shoulders. Let us watch nature take a turn.\"\n—Freyalise').
card_multiverse_id('quiet disrepair'/'FUT', '132217').

card_in_set('ramosian revivalist', 'FUT').
card_original_type('ramosian revivalist'/'FUT', 'Creature — Human Rebel Cleric').
card_original_text('ramosian revivalist'/'FUT', '{6}, {T}: Return target Rebel permanent card with converted mana cost 5 or less from your graveyard to play.').
card_first_print('ramosian revivalist', 'FUT').
card_image_name('ramosian revivalist'/'FUT', 'ramosian revivalist').
card_uid('ramosian revivalist'/'FUT', 'FUT:Ramosian Revivalist:ramosian revivalist').
card_rarity('ramosian revivalist'/'FUT', 'Uncommon').
card_artist('ramosian revivalist'/'FUT', 'Matt Stewart').
card_number('ramosian revivalist'/'FUT', '30').
card_flavor_text('ramosian revivalist'/'FUT', 'The Ramosian tradition is alive, but its namesake has long since been forgotten.').
card_multiverse_id('ramosian revivalist'/'FUT', '136145').
card_timeshifted('ramosian revivalist'/'FUT')
.
card_in_set('ravaging riftwurm', 'FUT').
card_original_type('ravaging riftwurm'/'FUT', 'Creature — Wurm').
card_original_text('ravaging riftwurm'/'FUT', 'Kicker {4} (You may pay an additional {4} as you play this spell.)\nVanishing 2 (This permanent comes into play with two time counters on it. At the beginning of your upkeep, remove a time counter from it. When the last is removed, sacrifice it.)\nIf the kicker cost was paid, Ravaging Riftwurm comes into play with three additional time counters on it.').
card_first_print('ravaging riftwurm', 'FUT').
card_image_name('ravaging riftwurm'/'FUT', 'ravaging riftwurm').
card_uid('ravaging riftwurm'/'FUT', 'FUT:Ravaging Riftwurm:ravaging riftwurm').
card_rarity('ravaging riftwurm'/'FUT', 'Uncommon').
card_artist('ravaging riftwurm'/'FUT', 'Michael Phillippi').
card_number('ravaging riftwurm'/'FUT', '135').
card_multiverse_id('ravaging riftwurm'/'FUT', '130339').

card_in_set('reality strobe', 'FUT').
card_original_type('reality strobe'/'FUT', 'Sorcery').
card_original_text('reality strobe'/'FUT', 'Return target permanent to its owner\'s hand. Remove Reality Strobe from the game with three time counters on it.\nSuspend 3—{2}{U} (Rather than play this card from your hand, you may pay {2}{U} and remove it from the game with three time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, play it without paying its mana cost.)').
card_first_print('reality strobe', 'FUT').
card_image_name('reality strobe'/'FUT', 'reality strobe').
card_uid('reality strobe'/'FUT', 'FUT:Reality Strobe:reality strobe').
card_rarity('reality strobe'/'FUT', 'Uncommon').
card_artist('reality strobe'/'FUT', 'Dan Scott').
card_number('reality strobe'/'FUT', '43').
card_multiverse_id('reality strobe'/'FUT', '126153').

card_in_set('riddle of lightning', 'FUT').
card_original_type('riddle of lightning'/'FUT', 'Instant').
card_original_text('riddle of lightning'/'FUT', 'Choose target creature or player. Scry 3, then reveal the top card of your library. Riddle of Lightning deals damage equal to that card\'s converted mana cost to that creature or player. (To scry 3, look at the top three cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_first_print('riddle of lightning', 'FUT').
card_image_name('riddle of lightning'/'FUT', 'riddle of lightning').
card_uid('riddle of lightning'/'FUT', 'FUT:Riddle of Lightning:riddle of lightning').
card_rarity('riddle of lightning'/'FUT', 'Common').
card_artist('riddle of lightning'/'FUT', 'Daren Bader').
card_number('riddle of lightning'/'FUT', '105').
card_multiverse_id('riddle of lightning'/'FUT', '126199').

card_in_set('rift elemental', 'FUT').
card_original_type('rift elemental'/'FUT', 'Creature — Elemental').
card_original_text('rift elemental'/'FUT', '{1}{R}, Remove a time counter from a permanent you control or suspended card you own: Rift Elemental gets +2/+0 until end of turn.').
card_first_print('rift elemental', 'FUT').
card_image_name('rift elemental'/'FUT', 'rift elemental').
card_uid('rift elemental'/'FUT', 'FUT:Rift Elemental:rift elemental').
card_rarity('rift elemental'/'FUT', 'Common').
card_artist('rift elemental'/'FUT', 'Ron Spears').
card_number('rift elemental'/'FUT', '106').
card_flavor_text('rift elemental'/'FUT', '\"It\'s difficult to heal a rift after it begins wandering about on its own.\"\n—Teferi').
card_multiverse_id('rift elemental'/'FUT', '130591').

card_in_set('riftsweeper', 'FUT').
card_original_type('riftsweeper'/'FUT', 'Creature — Elf Shaman').
card_original_text('riftsweeper'/'FUT', 'When Riftsweeper comes into play, choose target face-up card that\'s removed from the game. Its owner shuffles it into his or her library.').
card_first_print('riftsweeper', 'FUT').
card_image_name('riftsweeper'/'FUT', 'riftsweeper').
card_uid('riftsweeper'/'FUT', 'FUT:Riftsweeper:riftsweeper').
card_rarity('riftsweeper'/'FUT', 'Uncommon').
card_artist('riftsweeper'/'FUT', 'Brian Despain').
card_number('riftsweeper'/'FUT', '136').
card_flavor_text('riftsweeper'/'FUT', '\"Beings of the rifts are not natural. No longer will these abominations tread upon the body of Gaea.\"').
card_multiverse_id('riftsweeper'/'FUT', '130353').

card_in_set('rites of flourishing', 'FUT').
card_original_type('rites of flourishing'/'FUT', 'Enchantment').
card_original_text('rites of flourishing'/'FUT', 'At the beginning of each player\'s draw step, that player draws a card.\nEach player may play an additional land on each of his or her turns.').
card_first_print('rites of flourishing', 'FUT').
card_image_name('rites of flourishing'/'FUT', 'rites of flourishing').
card_uid('rites of flourishing'/'FUT', 'FUT:Rites of Flourishing:rites of flourishing').
card_rarity('rites of flourishing'/'FUT', 'Rare').
card_artist('rites of flourishing'/'FUT', 'Brandon Kitkouski').
card_number('rites of flourishing'/'FUT', '137').
card_flavor_text('rites of flourishing'/'FUT', '\"Dance, and bring forth the coil! It is an umbilical to Gaea herself, fattening us with the earth\'s rich bounty.\"').
card_multiverse_id('rites of flourishing'/'FUT', '130670').

card_in_set('river of tears', 'FUT').
card_original_type('river of tears'/'FUT', 'Land').
card_original_text('river of tears'/'FUT', '{T}: Add {U} to your mana pool. If you played a land this turn, add {B} to your mana pool instead.').
card_first_print('river of tears', 'FUT').
card_image_name('river of tears'/'FUT', 'river of tears').
card_uid('river of tears'/'FUT', 'FUT:River of Tears:river of tears').
card_rarity('river of tears'/'FUT', 'Rare').
card_artist('river of tears'/'FUT', 'Chris J. Anderson').
card_number('river of tears'/'FUT', '179').
card_flavor_text('river of tears'/'FUT', '\"The Westfolk wept, and their tears wore winding rivers into the cheek of the world.\"\n—Glem the Lonebard, \"Origins of Kholon\"').
card_multiverse_id('river of tears'/'FUT', '126210').
card_timeshifted('river of tears'/'FUT')
.
card_in_set('saltskitter', 'FUT').
card_original_type('saltskitter'/'FUT', 'Creature — Wurm').
card_original_text('saltskitter'/'FUT', 'Whenever another creature comes into play, remove Saltskitter from the game. Return Saltskitter to play under its owner\'s control at end of turn.').
card_first_print('saltskitter', 'FUT').
card_image_name('saltskitter'/'FUT', 'saltskitter').
card_uid('saltskitter'/'FUT', 'FUT:Saltskitter:saltskitter').
card_rarity('saltskitter'/'FUT', 'Common').
card_artist('saltskitter'/'FUT', 'Chippy').
card_number('saltskitter'/'FUT', '14').
card_multiverse_id('saltskitter'/'FUT', '130320').

card_in_set('samite censer-bearer', 'FUT').
card_original_type('samite censer-bearer'/'FUT', 'Creature — Human Rebel Cleric').
card_original_text('samite censer-bearer'/'FUT', '{W}, Sacrifice Samite Censer-Bearer: Prevent the next 1 damage that would be dealt to each creature you control this turn.').
card_first_print('samite censer-bearer', 'FUT').
card_image_name('samite censer-bearer'/'FUT', 'samite censer-bearer').
card_uid('samite censer-bearer'/'FUT', 'FUT:Samite Censer-Bearer:samite censer-bearer').
card_rarity('samite censer-bearer'/'FUT', 'Common').
card_artist('samite censer-bearer'/'FUT', 'William Simpson').
card_number('samite censer-bearer'/'FUT', '15').
card_flavor_text('samite censer-bearer'/'FUT', 'Samite alchemists extracted aromatic traces of healing minerals from the residue of the receding salt dunes.').
card_multiverse_id('samite censer-bearer'/'FUT', '130327').

card_in_set('sarcomite myr', 'FUT').
card_original_type('sarcomite myr'/'FUT', 'Artifact Creature — Myr').
card_original_text('sarcomite myr'/'FUT', '{2}: Sarcomite Myr gains flying until end of turn.\n{2}, Sacrifice Sarcomite Myr: Draw a card.').
card_first_print('sarcomite myr', 'FUT').
card_image_name('sarcomite myr'/'FUT', 'sarcomite myr').
card_uid('sarcomite myr'/'FUT', 'FUT:Sarcomite Myr:sarcomite myr').
card_rarity('sarcomite myr'/'FUT', 'Common').
card_artist('sarcomite myr'/'FUT', 'Michael Bruinsma').
card_number('sarcomite myr'/'FUT', '56').
card_flavor_text('sarcomite myr'/'FUT', '\"A horrible sight, yes, but the sounds . . . Its twanging tendons and grinding gears are almost musical.\"\n—Brudiclad, Telchor engineer').
card_multiverse_id('sarcomite myr'/'FUT', '136212').
card_timeshifted('sarcomite myr'/'FUT')
.
card_in_set('scourge of kher ridges', 'FUT').
card_original_type('scourge of kher ridges'/'FUT', 'Creature — Dragon').
card_original_text('scourge of kher ridges'/'FUT', 'Flying\n{1}{R}: Scourge of Kher Ridges deals 2 damage to each creature without flying.\n{5}{R}: Scourge of Kher Ridges deals 6 damage to each other creature with flying.').
card_first_print('scourge of kher ridges', 'FUT').
card_image_name('scourge of kher ridges'/'FUT', 'scourge of kher ridges').
card_uid('scourge of kher ridges'/'FUT', 'FUT:Scourge of Kher Ridges:scourge of kher ridges').
card_rarity('scourge of kher ridges'/'FUT', 'Rare').
card_artist('scourge of kher ridges'/'FUT', 'Daren Bader').
card_number('scourge of kher ridges'/'FUT', '107').
card_flavor_text('scourge of kher ridges'/'FUT', 'Doom casts its own flickering shadow.').
card_multiverse_id('scourge of kher ridges'/'FUT', '136161').

card_in_set('scout\'s warning', 'FUT').
card_original_type('scout\'s warning'/'FUT', 'Instant').
card_original_text('scout\'s warning'/'FUT', 'The next creature card you play this turn can be played as though it had flash.\nDraw a card.').
card_first_print('scout\'s warning', 'FUT').
card_image_name('scout\'s warning'/'FUT', 'scout\'s warning').
card_uid('scout\'s warning'/'FUT', 'FUT:Scout\'s Warning:scout\'s warning').
card_rarity('scout\'s warning'/'FUT', 'Rare').
card_artist('scout\'s warning'/'FUT', 'Zoltan Boros & Gabor Szikszai').
card_number('scout\'s warning'/'FUT', '16').
card_flavor_text('scout\'s warning'/'FUT', '\"There is no tactic I enjoy more than ambushing the ambushers.\"').
card_multiverse_id('scout\'s warning'/'FUT', '126200').

card_in_set('second wind', 'FUT').
card_original_type('second wind'/'FUT', 'Enchantment — Aura').
card_original_text('second wind'/'FUT', 'Enchant creature\n{T}: Tap enchanted creature.\n{T}: Untap enchanted creature.').
card_first_print('second wind', 'FUT').
card_image_name('second wind'/'FUT', 'second wind').
card_uid('second wind'/'FUT', 'FUT:Second Wind:second wind').
card_rarity('second wind'/'FUT', 'Uncommon').
card_artist('second wind'/'FUT', 'Matt Stewart').
card_number('second wind'/'FUT', '57').
card_flavor_text('second wind'/'FUT', 'Vindr stormchasers believe that the same ancient forces that move the winds can also move mortal minds and hearts.').
card_multiverse_id('second wind'/'FUT', '136158').
card_timeshifted('second wind'/'FUT')
.
card_in_set('seht\'s tiger', 'FUT').
card_original_type('seht\'s tiger'/'FUT', 'Creature — Cat').
card_original_text('seht\'s tiger'/'FUT', 'Flash (You may play this spell any time you could play an instant.)\nWhen Seht\'s Tiger comes into play, you gain protection from the color of your choice until end of turn. (You can\'t be targeted, dealt damage, or enchanted by anything of the chosen color.)').
card_first_print('seht\'s tiger', 'FUT').
card_image_name('seht\'s tiger'/'FUT', 'seht\'s tiger').
card_uid('seht\'s tiger'/'FUT', 'FUT:Seht\'s Tiger:seht\'s tiger').
card_rarity('seht\'s tiger'/'FUT', 'Rare').
card_artist('seht\'s tiger'/'FUT', 'Thomas Gianni').
card_number('seht\'s tiger'/'FUT', '31').
card_multiverse_id('seht\'s tiger'/'FUT', '130347').
card_timeshifted('seht\'s tiger'/'FUT')
.
card_in_set('shah of naar isle', 'FUT').
card_original_type('shah of naar isle'/'FUT', 'Creature — Efreet').
card_original_text('shah of naar isle'/'FUT', 'Trample\nEcho {0} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Shah of Naar Isle\'s echo cost is paid, each opponent may draw up to three cards.').
card_first_print('shah of naar isle', 'FUT').
card_image_name('shah of naar isle'/'FUT', 'shah of naar isle').
card_uid('shah of naar isle'/'FUT', 'FUT:Shah of Naar Isle:shah of naar isle').
card_rarity('shah of naar isle'/'FUT', 'Rare').
card_artist('shah of naar isle'/'FUT', 'Jon Foster').
card_number('shah of naar isle'/'FUT', '119').
card_multiverse_id('shah of naar isle'/'FUT', '136137').
card_timeshifted('shah of naar isle'/'FUT')
.
card_in_set('shapeshifter\'s marrow', 'FUT').
card_original_type('shapeshifter\'s marrow'/'FUT', 'Enchantment').
card_original_text('shapeshifter\'s marrow'/'FUT', 'At the beginning of each opponent\'s upkeep, that player reveals the top card of his or her library. If it\'s a creature card, the player puts the card into his or her graveyard and Shapeshifter\'s Marrow becomes a copy of that card. (If it does, it loses this ability.)').
card_first_print('shapeshifter\'s marrow', 'FUT').
card_image_name('shapeshifter\'s marrow'/'FUT', 'shapeshifter\'s marrow').
card_uid('shapeshifter\'s marrow'/'FUT', 'FUT:Shapeshifter\'s Marrow:shapeshifter\'s marrow').
card_rarity('shapeshifter\'s marrow'/'FUT', 'Rare').
card_artist('shapeshifter\'s marrow'/'FUT', 'Chippy').
card_number('shapeshifter\'s marrow'/'FUT', '58').
card_multiverse_id('shapeshifter\'s marrow'/'FUT', '136214').
card_timeshifted('shapeshifter\'s marrow'/'FUT')
.
card_in_set('shimian specter', 'FUT').
card_original_type('shimian specter'/'FUT', 'Creature — Specter').
card_original_text('shimian specter'/'FUT', 'Flying\nWhenever Shimian Specter deals combat damage to a player, that player reveals his or her hand. Choose a nonland card from it. Search that player\'s graveyard, hand, and library for all cards with the same name as that card and remove them from the game. Then that player shuffles his or her library.').
card_first_print('shimian specter', 'FUT').
card_image_name('shimian specter'/'FUT', 'shimian specter').
card_uid('shimian specter'/'FUT', 'FUT:Shimian Specter:shimian specter').
card_rarity('shimian specter'/'FUT', 'Rare').
card_artist('shimian specter'/'FUT', 'Anthony S. Waters').
card_number('shimian specter'/'FUT', '76').
card_multiverse_id('shimian specter'/'FUT', '126134').

card_in_set('shivan sand-mage', 'FUT').
card_original_type('shivan sand-mage'/'FUT', 'Creature — Viashino Shaman').
card_original_text('shivan sand-mage'/'FUT', 'When Shivan Sand-Mage comes into play, choose one Remove two time counters from target permanent or suspended card; or put two time counters on target permanent with a time counter on it or suspended card.\nSuspend 4—{R}').
card_first_print('shivan sand-mage', 'FUT').
card_image_name('shivan sand-mage'/'FUT', 'shivan sand-mage').
card_uid('shivan sand-mage'/'FUT', 'FUT:Shivan Sand-Mage:shivan sand-mage').
card_rarity('shivan sand-mage'/'FUT', 'Uncommon').
card_artist('shivan sand-mage'/'FUT', 'Dave Kendall').
card_number('shivan sand-mage'/'FUT', '108').
card_multiverse_id('shivan sand-mage'/'FUT', '126201').

card_in_set('skirk ridge exhumer', 'FUT').
card_original_type('skirk ridge exhumer'/'FUT', 'Creature — Zombie Spellshaper').
card_original_text('skirk ridge exhumer'/'FUT', '{B}, {T}, Discard a card: Put a 1/1 black Zombie Goblin creature token named Festering Goblin into play with \"When Festering Goblin is put into a graveyard from play, target creature gets -1/-1 until end of turn.\"').
card_first_print('skirk ridge exhumer', 'FUT').
card_image_name('skirk ridge exhumer'/'FUT', 'skirk ridge exhumer').
card_uid('skirk ridge exhumer'/'FUT', 'FUT:Skirk Ridge Exhumer:skirk ridge exhumer').
card_rarity('skirk ridge exhumer'/'FUT', 'Uncommon').
card_artist('skirk ridge exhumer'/'FUT', 'Michael Phillippi').
card_number('skirk ridge exhumer'/'FUT', '77').
card_multiverse_id('skirk ridge exhumer'/'FUT', '130314').

card_in_set('skizzik surger', 'FUT').
card_original_type('skizzik surger'/'FUT', 'Creature — Elemental').
card_original_text('skizzik surger'/'FUT', 'Haste\nEcho—Sacrifice two lands. (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)').
card_first_print('skizzik surger', 'FUT').
card_image_name('skizzik surger'/'FUT', 'skizzik surger').
card_uid('skizzik surger'/'FUT', 'FUT:Skizzik Surger:skizzik surger').
card_rarity('skizzik surger'/'FUT', 'Uncommon').
card_artist('skizzik surger'/'FUT', 'Lucio Parrillo').
card_number('skizzik surger'/'FUT', '120').
card_multiverse_id('skizzik surger'/'FUT', '136160').
card_timeshifted('skizzik surger'/'FUT')
.
card_in_set('slaughter pact', 'FUT').
card_original_type('slaughter pact'/'FUT', 'Instant').
card_original_text('slaughter pact'/'FUT', 'Slaughter Pact is black.\nDestroy target nonblack creature.\nAt the beginning of your next upkeep, pay {2}{B}. If you don\'t, you lose the game.').
card_first_print('slaughter pact', 'FUT').
card_image_name('slaughter pact'/'FUT', 'slaughter pact').
card_uid('slaughter pact'/'FUT', 'FUT:Slaughter Pact:slaughter pact').
card_rarity('slaughter pact'/'FUT', 'Rare').
card_artist('slaughter pact'/'FUT', 'Kev Walker').
card_number('slaughter pact'/'FUT', '78').
card_flavor_text('slaughter pact'/'FUT', 'Death is only the beginning of the end.').
card_multiverse_id('slaughter pact'/'FUT', '130704').

card_in_set('sliver legion', 'FUT').
card_original_type('sliver legion'/'FUT', 'Legendary Creature — Sliver').
card_original_text('sliver legion'/'FUT', 'All Sliver creatures get +1/+1 for each other Sliver in play.').
card_first_print('sliver legion', 'FUT').
card_image_name('sliver legion'/'FUT', 'sliver legion').
card_uid('sliver legion'/'FUT', 'FUT:Sliver Legion:sliver legion').
card_rarity('sliver legion'/'FUT', 'Rare').
card_artist('sliver legion'/'FUT', 'Ron Spears').
card_number('sliver legion'/'FUT', '158').
card_flavor_text('sliver legion'/'FUT', 'Hidden within the clicking, chittering swarm is a unique mind, still young, but growing more aware as time passes.').
card_multiverse_id('sliver legion'/'FUT', '136146').

card_in_set('sliversmith', 'FUT').
card_original_type('sliversmith'/'FUT', 'Artifact Creature — Spellshaper').
card_original_text('sliversmith'/'FUT', '{1}, {T}, Discard a card: Put a 1/1 Sliver artifact creature token named Metallic Sliver into play.').
card_first_print('sliversmith', 'FUT').
card_image_name('sliversmith'/'FUT', 'sliversmith').
card_uid('sliversmith'/'FUT', 'FUT:Sliversmith:sliversmith').
card_rarity('sliversmith'/'FUT', 'Uncommon').
card_artist('sliversmith'/'FUT', 'John Avon').
card_number('sliversmith'/'FUT', '163').
card_flavor_text('sliversmith'/'FUT', 'After centuries of labor, its creations outnumber those they were to mimic.').
card_multiverse_id('sliversmith'/'FUT', '130329').

card_in_set('snake cult initiation', 'FUT').
card_original_type('snake cult initiation'/'FUT', 'Enchantment — Aura').
card_original_text('snake cult initiation'/'FUT', 'Enchant creature\nEnchanted creature has poisonous 3. (Whenever it deals combat damage to a player, that player gets three poison counters. A player with ten or more poison counters loses the game.)').
card_first_print('snake cult initiation', 'FUT').
card_image_name('snake cult initiation'/'FUT', 'snake cult initiation').
card_uid('snake cult initiation'/'FUT', 'FUT:Snake Cult Initiation:snake cult initiation').
card_rarity('snake cult initiation'/'FUT', 'Uncommon').
card_artist('snake cult initiation'/'FUT', 'Justin Sweet').
card_number('snake cult initiation'/'FUT', '89').
card_multiverse_id('snake cult initiation'/'FUT', '130708').
card_timeshifted('snake cult initiation'/'FUT')
.
card_in_set('soultether golem', 'FUT').
card_original_type('soultether golem'/'FUT', 'Artifact Creature — Golem').
card_original_text('soultether golem'/'FUT', 'Vanishing 1 (This permanent comes into play with a time counter on it. At the beginning of your upkeep, remove a time counter from it. When the last is removed, sacrifice it.)\nWhenever another creature comes into play under your control, put a time counter on Soultether Golem.').
card_first_print('soultether golem', 'FUT').
card_image_name('soultether golem'/'FUT', 'soultether golem').
card_uid('soultether golem'/'FUT', 'FUT:Soultether Golem:soultether golem').
card_rarity('soultether golem'/'FUT', 'Uncommon').
card_artist('soultether golem'/'FUT', 'Paolo Parente').
card_number('soultether golem'/'FUT', '164').
card_multiverse_id('soultether golem'/'FUT', '126213').

card_in_set('sparkspitter', 'FUT').
card_original_type('sparkspitter'/'FUT', 'Creature — Elemental Spellshaper').
card_original_text('sparkspitter'/'FUT', '{R}, {T}, Discard a card: Put a 3/1 red Elemental creature token named Spark Elemental into play with trample, haste, and \"At end of turn, sacrifice Spark Elemental.\"').
card_first_print('sparkspitter', 'FUT').
card_image_name('sparkspitter'/'FUT', 'sparkspitter').
card_uid('sparkspitter'/'FUT', 'FUT:Sparkspitter:sparkspitter').
card_rarity('sparkspitter'/'FUT', 'Uncommon').
card_artist('sparkspitter'/'FUT', 'Dan Scott').
card_number('sparkspitter'/'FUT', '109').
card_multiverse_id('sparkspitter'/'FUT', '130341').

card_in_set('spellweaver volute', 'FUT').
card_original_type('spellweaver volute'/'FUT', 'Enchantment — Aura').
card_original_text('spellweaver volute'/'FUT', 'Enchant instant card in a graveyard\nWhenever you play a sorcery spell, copy the enchanted instant card. You may play the copy without paying its mana cost. If you do, remove the enchanted card from the game and attach Spellweaver Volute to another instant card in a graveyard.').
card_first_print('spellweaver volute', 'FUT').
card_image_name('spellweaver volute'/'FUT', 'spellweaver volute').
card_uid('spellweaver volute'/'FUT', 'FUT:Spellweaver Volute:spellweaver volute').
card_rarity('spellweaver volute'/'FUT', 'Rare').
card_artist('spellweaver volute'/'FUT', 'Bud Cook').
card_number('spellweaver volute'/'FUT', '59').
card_multiverse_id('spellweaver volute'/'FUT', '136032').
card_timeshifted('spellweaver volute'/'FUT')
.
card_in_set('spellwild ouphe', 'FUT').
card_original_type('spellwild ouphe'/'FUT', 'Creature — Ouphe').
card_original_text('spellwild ouphe'/'FUT', 'Spells that target Spellwild Ouphe cost {2} less to play.').
card_first_print('spellwild ouphe', 'FUT').
card_image_name('spellwild ouphe'/'FUT', 'spellwild ouphe').
card_uid('spellwild ouphe'/'FUT', 'FUT:Spellwild Ouphe:spellwild ouphe').
card_rarity('spellwild ouphe'/'FUT', 'Uncommon').
card_artist('spellwild ouphe'/'FUT', 'Scott Altmann').
card_number('spellwild ouphe'/'FUT', '151').
card_flavor_text('spellwild ouphe'/'FUT', '\"You\'d think that their big glowing eyes would be almost hypnotic to look at. In fact, it\'s the other way around—they are entranced by nearly everything they see.\"\n—Balecki, Spellwild druid').
card_multiverse_id('spellwild ouphe'/'FUT', '130659').
card_timeshifted('spellwild ouphe'/'FUT')
.
card_in_set('spin into myth', 'FUT').
card_original_type('spin into myth'/'FUT', 'Instant').
card_original_text('spin into myth'/'FUT', 'Put target creature on top of its owner\'s library, then fateseal 2. (Look at the top two cards of an opponent\'s library, then put any number of them on the bottom of that player\'s library and the rest on top in any order.)').
card_first_print('spin into myth', 'FUT').
card_image_name('spin into myth'/'FUT', 'spin into myth').
card_uid('spin into myth'/'FUT', 'FUT:Spin into Myth:spin into myth').
card_rarity('spin into myth'/'FUT', 'Uncommon').
card_artist('spin into myth'/'FUT', 'David Day').
card_number('spin into myth'/'FUT', '60').
card_multiverse_id('spin into myth'/'FUT', '130344').
card_timeshifted('spin into myth'/'FUT')
.
card_in_set('spirit en-dal', 'FUT').
card_original_type('spirit en-dal'/'FUT', 'Creature — Spirit').
card_original_text('spirit en-dal'/'FUT', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nForecast {1}{W}, Reveal Spirit en-Dal from your hand: Target creature gains shadow until end of turn. (Play this ability only during your upkeep and only once each turn.)').
card_first_print('spirit en-dal', 'FUT').
card_image_name('spirit en-dal'/'FUT', 'spirit en-dal').
card_uid('spirit en-dal'/'FUT', 'FUT:Spirit en-Dal:spirit en-dal').
card_rarity('spirit en-dal'/'FUT', 'Uncommon').
card_artist('spirit en-dal'/'FUT', 'Daren Bader').
card_number('spirit en-dal'/'FUT', '17').
card_multiverse_id('spirit en-dal'/'FUT', '132221').

card_in_set('sporoloth ancient', 'FUT').
card_original_type('sporoloth ancient'/'FUT', 'Creature — Fungus').
card_original_text('sporoloth ancient'/'FUT', 'At the beginning of your upkeep, put a spore counter on Sporoloth Ancient.\nCreatures you control have \"Remove two spore counters from this creature: Put a 1/1 green Saproling creature token into play.\"').
card_first_print('sporoloth ancient', 'FUT').
card_image_name('sporoloth ancient'/'FUT', 'sporoloth ancient').
card_uid('sporoloth ancient'/'FUT', 'FUT:Sporoloth Ancient:sporoloth ancient').
card_rarity('sporoloth ancient'/'FUT', 'Common').
card_artist('sporoloth ancient'/'FUT', 'James Kei').
card_number('sporoloth ancient'/'FUT', '152').
card_multiverse_id('sporoloth ancient'/'FUT', '130323').
card_timeshifted('sporoloth ancient'/'FUT')
.
card_in_set('sprout swarm', 'FUT').
card_original_type('sprout swarm'/'FUT', 'Instant').
card_original_text('sprout swarm'/'FUT', 'Convoke (Each creature you tap while playing this spell reduces its total cost by {1} or by one mana of that creature\'s color.)\nBuyback {3} (You may pay an additional {3} as you play this spell. If you do, put this card into your hand as it resolves.)\nPut a 1/1 green Saproling creature token into play.').
card_first_print('sprout swarm', 'FUT').
card_image_name('sprout swarm'/'FUT', 'sprout swarm').
card_uid('sprout swarm'/'FUT', 'FUT:Sprout Swarm:sprout swarm').
card_rarity('sprout swarm'/'FUT', 'Common').
card_artist('sprout swarm'/'FUT', 'Chippy').
card_number('sprout swarm'/'FUT', '138').
card_multiverse_id('sprout swarm'/'FUT', '136042').

card_in_set('steamflogger boss', 'FUT').
card_original_type('steamflogger boss'/'FUT', 'Creature — Goblin Rigger').
card_original_text('steamflogger boss'/'FUT', 'Other Rigger creatures you control get +1/+0 and have haste.\nIf a Rigger you control would assemble a Contraption, it assembles two Contraptions instead.').
card_first_print('steamflogger boss', 'FUT').
card_image_name('steamflogger boss'/'FUT', 'steamflogger boss').
card_uid('steamflogger boss'/'FUT', 'FUT:Steamflogger Boss:steamflogger boss').
card_rarity('steamflogger boss'/'FUT', 'Rare').
card_artist('steamflogger boss'/'FUT', 'Warren Mahy').
card_number('steamflogger boss'/'FUT', '121').
card_flavor_text('steamflogger boss'/'FUT', '\"Whip the Xs ! Pinch the Os!\nWhat we\'re building, no one knows!\"').
card_multiverse_id('steamflogger boss'/'FUT', '136151').
card_timeshifted('steamflogger boss'/'FUT')
.
card_in_set('storm entity', 'FUT').
card_original_type('storm entity'/'FUT', 'Creature — Elemental').
card_original_text('storm entity'/'FUT', 'Haste\nStorm Entity comes into play with a +1/+1 counter on it for each other spell played this turn.').
card_image_name('storm entity'/'FUT', 'storm entity').
card_uid('storm entity'/'FUT', 'FUT:Storm Entity:storm entity').
card_rarity('storm entity'/'FUT', 'Uncommon').
card_artist('storm entity'/'FUT', 'Lucio Parrillo').
card_number('storm entity'/'FUT', '122').
card_flavor_text('storm entity'/'FUT', 'Dweldian magi don\'t enter the Realm of Thrones when they die. Instead their souls are drawn to great works of magic.').
card_multiverse_id('storm entity'/'FUT', '130675').
card_timeshifted('storm entity'/'FUT')
.
card_in_set('street wraith', 'FUT').
card_original_type('street wraith'/'FUT', 'Creature — Wraith').
card_original_text('street wraith'/'FUT', 'Swampwalk\nCycling—Pay 2 life. (Pay 2 life, Discard this card: Draw a card.)').
card_first_print('street wraith', 'FUT').
card_image_name('street wraith'/'FUT', 'street wraith').
card_uid('street wraith'/'FUT', 'FUT:Street Wraith:street wraith').
card_rarity('street wraith'/'FUT', 'Uncommon').
card_artist('street wraith'/'FUT', 'Cyril Van Der Haegen').
card_number('street wraith'/'FUT', '90').
card_flavor_text('street wraith'/'FUT', 'The lamps on Wyndmoor Street snuff themselves at midnight and refuse to relight, afraid to illuminate what lies in the darkness.').
card_multiverse_id('street wraith'/'FUT', '136205').
card_timeshifted('street wraith'/'FUT')
.
card_in_set('stronghold rats', 'FUT').
card_original_type('stronghold rats'/'FUT', 'Creature — Rat').
card_original_text('stronghold rats'/'FUT', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nWhenever Stronghold Rats deals combat damage to a player, each player discards a card.').
card_first_print('stronghold rats', 'FUT').
card_image_name('stronghold rats'/'FUT', 'stronghold rats').
card_uid('stronghold rats'/'FUT', 'FUT:Stronghold Rats:stronghold rats').
card_rarity('stronghold rats'/'FUT', 'Uncommon').
card_artist('stronghold rats'/'FUT', 'Brandon Kitkouski').
card_number('stronghold rats'/'FUT', '79').
card_flavor_text('stronghold rats'/'FUT', 'In the stronghold\'s dungeons, the shadows hide in the vermin.').
card_multiverse_id('stronghold rats'/'FUT', '136056').

card_in_set('summoner\'s pact', 'FUT').
card_original_type('summoner\'s pact'/'FUT', 'Instant').
card_original_text('summoner\'s pact'/'FUT', 'Summoner\'s Pact is green.\nSearch your library for a green creature card, reveal it, and put it into your hand. Then shuffle your library.\nAt the beginning of your next upkeep, pay {2}{G}{G}. If you don\'t, you lose the game.').
card_first_print('summoner\'s pact', 'FUT').
card_image_name('summoner\'s pact'/'FUT', 'summoner\'s pact').
card_uid('summoner\'s pact'/'FUT', 'FUT:Summoner\'s Pact:summoner\'s pact').
card_rarity('summoner\'s pact'/'FUT', 'Rare').
card_artist('summoner\'s pact'/'FUT', 'Chippy').
card_number('summoner\'s pact'/'FUT', '139').
card_multiverse_id('summoner\'s pact'/'FUT', '130706').

card_in_set('sword of the meek', 'FUT').
card_original_type('sword of the meek'/'FUT', 'Artifact — Equipment').
card_original_text('sword of the meek'/'FUT', 'Equipped creature gets +1/+2.\nEquip {2}\nWhenever a 1/1 creature comes into play under your control, you may return Sword of the Meek from your graveyard to play, then attach it to that creature.').
card_first_print('sword of the meek', 'FUT').
card_image_name('sword of the meek'/'FUT', 'sword of the meek').
card_uid('sword of the meek'/'FUT', 'FUT:Sword of the Meek:sword of the meek').
card_rarity('sword of the meek'/'FUT', 'Uncommon').
card_artist('sword of the meek'/'FUT', 'Franz Vohwinkel').
card_number('sword of the meek'/'FUT', '165').
card_multiverse_id('sword of the meek'/'FUT', '126215').

card_in_set('take possession', 'FUT').
card_original_type('take possession'/'FUT', 'Enchantment — Aura').
card_original_text('take possession'/'FUT', 'Split second (As long as this spell is on the stack, players can\'t play spells or activated abilities that aren\'t mana abilities.)\nEnchant permanent\nYou control enchanted permanent.').
card_first_print('take possession', 'FUT').
card_image_name('take possession'/'FUT', 'take possession').
card_uid('take possession'/'FUT', 'FUT:Take Possession:take possession').
card_rarity('take possession'/'FUT', 'Rare').
card_artist('take possession'/'FUT', 'Michael Phillippi').
card_number('take possession'/'FUT', '44').
card_multiverse_id('take possession'/'FUT', '136199').

card_in_set('tarmogoyf', 'FUT').
card_original_type('tarmogoyf'/'FUT', 'Creature — Lhurgoyf').
card_original_text('tarmogoyf'/'FUT', 'Tarmogoyf\'s power is equal to the number of card types among cards in all graveyards and its toughness is equal to that number plus 1. (The card types are artifact, creature, enchantment, instant, land, planeswalker, sorcery, and tribal.)').
card_first_print('tarmogoyf', 'FUT').
card_image_name('tarmogoyf'/'FUT', 'tarmogoyf').
card_uid('tarmogoyf'/'FUT', 'FUT:Tarmogoyf:tarmogoyf').
card_rarity('tarmogoyf'/'FUT', 'Rare').
card_artist('tarmogoyf'/'FUT', 'Justin Murray').
card_number('tarmogoyf'/'FUT', '153').
card_multiverse_id('tarmogoyf'/'FUT', '136142').
card_timeshifted('tarmogoyf'/'FUT')
.
card_in_set('tarox bladewing', 'FUT').
card_original_type('tarox bladewing'/'FUT', 'Legendary Creature — Dragon').
card_original_text('tarox bladewing'/'FUT', 'Flying, haste\nGrandeur Discard another card named Tarox Bladewing: Tarox Bladewing gets +X/+X until end of turn, where X is its power.').
card_first_print('tarox bladewing', 'FUT').
card_image_name('tarox bladewing'/'FUT', 'tarox bladewing').
card_uid('tarox bladewing'/'FUT', 'FUT:Tarox Bladewing:tarox bladewing').
card_rarity('tarox bladewing'/'FUT', 'Rare').
card_artist('tarox bladewing'/'FUT', 'Aleksi Briclot').
card_number('tarox bladewing'/'FUT', '123').
card_flavor_text('tarox bladewing'/'FUT', 'Despising his siblings as insults to his line, he finds and devours each in turn.').
card_multiverse_id('tarox bladewing'/'FUT', '136139').
card_timeshifted('tarox bladewing'/'FUT')
.
card_in_set('thornweald archer', 'FUT').
card_original_type('thornweald archer'/'FUT', 'Creature — Elf Archer').
card_original_text('thornweald archer'/'FUT', 'Reach (This creature can block creatures with flying.)\nDeathtouch (Whenever this creature deals damage to a creature, destroy that creature.)').
card_first_print('thornweald archer', 'FUT').
card_image_name('thornweald archer'/'FUT', 'thornweald archer').
card_uid('thornweald archer'/'FUT', 'FUT:Thornweald Archer:thornweald archer').
card_rarity('thornweald archer'/'FUT', 'Common').
card_artist('thornweald archer'/'FUT', 'Dave Kendall').
card_number('thornweald archer'/'FUT', '154').
card_flavor_text('thornweald archer'/'FUT', 'Their arrows are tipped with basilisk eyes and fletched with cockatrice feathers.').
card_multiverse_id('thornweald archer'/'FUT', '130630').
card_timeshifted('thornweald archer'/'FUT')
.
card_in_set('thunderblade charge', 'FUT').
card_original_type('thunderblade charge'/'FUT', 'Sorcery').
card_original_text('thunderblade charge'/'FUT', 'Thunderblade Charge deals 3 damage to target creature or player.\nWhenever one or more creatures you control deal combat damage to a player, if Thunderblade Charge is in your graveyard, you may pay {2}{R}{R}{R}. If you do, play it without paying its mana cost.').
card_first_print('thunderblade charge', 'FUT').
card_image_name('thunderblade charge'/'FUT', 'thunderblade charge').
card_uid('thunderblade charge'/'FUT', 'FUT:Thunderblade Charge:thunderblade charge').
card_rarity('thunderblade charge'/'FUT', 'Rare').
card_artist('thunderblade charge'/'FUT', 'Justin Murray').
card_number('thunderblade charge'/'FUT', '124').
card_multiverse_id('thunderblade charge'/'FUT', '130338').
card_timeshifted('thunderblade charge'/'FUT')
.
card_in_set('tolaria west', 'FUT').
card_original_type('tolaria west'/'FUT', 'Land').
card_original_text('tolaria west'/'FUT', 'Tolaria West comes into play tapped.\n{T}: Add {U} to your mana pool.\nTransmute {1}{U}{U} ({1}{U}{U}, Discard this card: Search your library for a card with converted mana cost 0, reveal it, and put it into your hand. Then shuffle your library. Play only as a sorcery.)').
card_first_print('tolaria west', 'FUT').
card_image_name('tolaria west'/'FUT', 'tolaria west').
card_uid('tolaria west'/'FUT', 'FUT:Tolaria West:tolaria west').
card_rarity('tolaria west'/'FUT', 'Uncommon').
card_artist('tolaria west'/'FUT', 'Khang Le').
card_number('tolaria west'/'FUT', '173').
card_multiverse_id('tolaria west'/'FUT', '136047').

card_in_set('tombstalker', 'FUT').
card_original_type('tombstalker'/'FUT', 'Creature — Demon').
card_original_text('tombstalker'/'FUT', 'Flying\nDelve (You may remove any number of cards in your graveyard from the game as you play this spell. It costs {1} less to play for each card removed this way.)').
card_first_print('tombstalker', 'FUT').
card_image_name('tombstalker'/'FUT', 'tombstalker').
card_uid('tombstalker'/'FUT', 'FUT:Tombstalker:tombstalker').
card_rarity('tombstalker'/'FUT', 'Rare').
card_artist('tombstalker'/'FUT', 'Aleksi Briclot').
card_number('tombstalker'/'FUT', '91').
card_multiverse_id('tombstalker'/'FUT', '136041').
card_timeshifted('tombstalker'/'FUT')
.
card_in_set('unblinking bleb', 'FUT').
card_original_type('unblinking bleb'/'FUT', 'Creature — Illusion').
card_original_text('unblinking bleb'/'FUT', 'Morph {2}{U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhenever Unblinking Bleb or another permanent is turned face up, you may scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_first_print('unblinking bleb', 'FUT').
card_image_name('unblinking bleb'/'FUT', 'unblinking bleb').
card_uid('unblinking bleb'/'FUT', 'FUT:Unblinking Bleb:unblinking bleb').
card_rarity('unblinking bleb'/'FUT', 'Common').
card_artist('unblinking bleb'/'FUT', 'Shishizaru').
card_number('unblinking bleb'/'FUT', '45').
card_multiverse_id('unblinking bleb'/'FUT', '130342').

card_in_set('utopia mycon', 'FUT').
card_original_type('utopia mycon'/'FUT', 'Creature — Fungus').
card_original_text('utopia mycon'/'FUT', 'At the beginning of your upkeep, put a spore counter on Utopia Mycon.\nRemove three spore counters from Utopia Mycon: Put a 1/1 green Saproling creature token into play.\nSacrifice a Saproling: Add one mana of any color to your mana pool.').
card_first_print('utopia mycon', 'FUT').
card_image_name('utopia mycon'/'FUT', 'utopia mycon').
card_uid('utopia mycon'/'FUT', 'FUT:Utopia Mycon:utopia mycon').
card_rarity('utopia mycon'/'FUT', 'Uncommon').
card_artist('utopia mycon'/'FUT', 'Anthony S. Waters').
card_number('utopia mycon'/'FUT', '140').
card_multiverse_id('utopia mycon'/'FUT', '130616').

card_in_set('vedalken æthermage', 'FUT').
card_original_type('vedalken æthermage'/'FUT', 'Creature — Vedalken Wizard').
card_original_text('vedalken æthermage'/'FUT', 'Flash (You may play this spell any time you could play an instant.)\nWhen Vedalken Æthermage comes into play, return target Sliver to its owner\'s hand.\nWizardcycling {3} ({3}, Discard this card: Search your library for a Wizard card, reveal it, and put it into your hand. Then shuffle your library.)').
card_first_print('vedalken æthermage', 'FUT').
card_image_name('vedalken æthermage'/'FUT', 'vedalken aethermage').
card_uid('vedalken æthermage'/'FUT', 'FUT:Vedalken Æthermage:vedalken aethermage').
card_rarity('vedalken æthermage'/'FUT', 'Common').
card_artist('vedalken æthermage'/'FUT', 'William Simpson').
card_number('vedalken æthermage'/'FUT', '61').
card_multiverse_id('vedalken æthermage'/'FUT', '130325').
card_timeshifted('vedalken æthermage'/'FUT')
.
card_in_set('veilstone amulet', 'FUT').
card_original_type('veilstone amulet'/'FUT', 'Artifact').
card_original_text('veilstone amulet'/'FUT', 'Whenever you play a spell, creatures you control can\'t be the targets of spells or abilities your opponents control this turn.').
card_first_print('veilstone amulet', 'FUT').
card_image_name('veilstone amulet'/'FUT', 'veilstone amulet').
card_uid('veilstone amulet'/'FUT', 'FUT:Veilstone Amulet:veilstone amulet').
card_rarity('veilstone amulet'/'FUT', 'Rare').
card_artist('veilstone amulet'/'FUT', 'Martina Pilcerova').
card_number('veilstone amulet'/'FUT', '166').
card_flavor_text('veilstone amulet'/'FUT', 'Sometimes it\'s ostentatious to go unseen.').
card_multiverse_id('veilstone amulet'/'FUT', '136149').

card_in_set('venser\'s diffusion', 'FUT').
card_original_type('venser\'s diffusion'/'FUT', 'Instant').
card_original_text('venser\'s diffusion'/'FUT', 'Return target nonland permanent or suspended card to its owner\'s hand.').
card_first_print('venser\'s diffusion', 'FUT').
card_image_name('venser\'s diffusion'/'FUT', 'venser\'s diffusion').
card_uid('venser\'s diffusion'/'FUT', 'FUT:Venser\'s Diffusion:venser\'s diffusion').
card_rarity('venser\'s diffusion'/'FUT', 'Common').
card_artist('venser\'s diffusion'/'FUT', 'Hideaki Takamura').
card_number('venser\'s diffusion'/'FUT', '47').
card_flavor_text('venser\'s diffusion'/'FUT', '\"Each reality is but the dream of another, and each sleeper a god unknowing.\"').
card_multiverse_id('venser\'s diffusion'/'FUT', '136197').

card_in_set('venser, shaper savant', 'FUT').
card_original_type('venser, shaper savant'/'FUT', 'Legendary Creature — Human Wizard').
card_original_text('venser, shaper savant'/'FUT', 'Flash (You may play this spell any time you could play an instant.)\nWhen Venser, Shaper Savant comes into play, return target spell or permanent to its owner\'s hand.').
card_first_print('venser, shaper savant', 'FUT').
card_image_name('venser, shaper savant'/'FUT', 'venser, shaper savant').
card_uid('venser, shaper savant'/'FUT', 'FUT:Venser, Shaper Savant:venser, shaper savant').
card_rarity('venser, shaper savant'/'FUT', 'Rare').
card_artist('venser, shaper savant'/'FUT', 'Aleksi Briclot').
card_number('venser, shaper savant'/'FUT', '46').
card_flavor_text('venser, shaper savant'/'FUT', 'His marvels of artifice pale in comparison to the developing machinery of his mind.').
card_multiverse_id('venser, shaper savant'/'FUT', '136209').

card_in_set('virulent sliver', 'FUT').
card_original_type('virulent sliver'/'FUT', 'Creature — Sliver').
card_original_text('virulent sliver'/'FUT', 'All Sliver creatures have poisonous 1. (Whenever a Sliver deals combat damage to a player, that player gets a poison counter. A player with ten or more poison counters loses the game.)').
card_first_print('virulent sliver', 'FUT').
card_image_name('virulent sliver'/'FUT', 'virulent sliver').
card_uid('virulent sliver'/'FUT', 'FUT:Virulent Sliver:virulent sliver').
card_rarity('virulent sliver'/'FUT', 'Common').
card_artist('virulent sliver'/'FUT', 'Franz Vohwinkel').
card_number('virulent sliver'/'FUT', '155').
card_multiverse_id('virulent sliver'/'FUT', '130644').
card_timeshifted('virulent sliver'/'FUT')
.
card_in_set('whetwheel', 'FUT').
card_original_type('whetwheel'/'FUT', 'Artifact').
card_original_text('whetwheel'/'FUT', '{X}{X}, {T}: Target player puts the top X cards of his or her library into his or her graveyard.\nMorph {3} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('whetwheel', 'FUT').
card_image_name('whetwheel'/'FUT', 'whetwheel').
card_uid('whetwheel'/'FUT', 'FUT:Whetwheel:whetwheel').
card_rarity('whetwheel'/'FUT', 'Rare').
card_artist('whetwheel'/'FUT', 'Cyril Van Der Haegen').
card_number('whetwheel'/'FUT', '168').
card_multiverse_id('whetwheel'/'FUT', '136141').
card_timeshifted('whetwheel'/'FUT')
.
card_in_set('whip-spine drake', 'FUT').
card_original_type('whip-spine drake'/'FUT', 'Creature — Drake').
card_original_text('whip-spine drake'/'FUT', 'Flying\nMorph {2}{W} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('whip-spine drake', 'FUT').
card_image_name('whip-spine drake'/'FUT', 'whip-spine drake').
card_uid('whip-spine drake'/'FUT', 'FUT:Whip-Spine Drake:whip-spine drake').
card_rarity('whip-spine drake'/'FUT', 'Common').
card_artist('whip-spine drake'/'FUT', 'Eric Fortune').
card_number('whip-spine drake'/'FUT', '62').
card_flavor_text('whip-spine drake'/'FUT', '\"I swear the clouds were created just so the drakes would have a place to lurk.\"\n—Kasharri, skyknight').
card_multiverse_id('whip-spine drake'/'FUT', '126178').
card_timeshifted('whip-spine drake'/'FUT')
.
card_in_set('witch\'s mist', 'FUT').
card_original_type('witch\'s mist'/'FUT', 'Enchantment').
card_original_text('witch\'s mist'/'FUT', '{2}{B}, {T}: Destroy target creature that was dealt damage this turn.').
card_first_print('witch\'s mist', 'FUT').
card_image_name('witch\'s mist'/'FUT', 'witch\'s mist').
card_uid('witch\'s mist'/'FUT', 'FUT:Witch\'s Mist:witch\'s mist').
card_rarity('witch\'s mist'/'FUT', 'Uncommon').
card_artist('witch\'s mist'/'FUT', 'Ittoku').
card_number('witch\'s mist'/'FUT', '92').
card_flavor_text('witch\'s mist'/'FUT', '\"Hear us, Overmother. Let the strong thrive under your gelatinous hand, and let the weak writhe under your serrated heel.\"\n—Prayer of the devoted').
card_multiverse_id('witch\'s mist'/'FUT', '132222').
card_timeshifted('witch\'s mist'/'FUT')
.
card_in_set('wrap in vigor', 'FUT').
card_original_type('wrap in vigor'/'FUT', 'Instant').
card_original_text('wrap in vigor'/'FUT', 'Regenerate each creature you control.').
card_first_print('wrap in vigor', 'FUT').
card_image_name('wrap in vigor'/'FUT', 'wrap in vigor').
card_uid('wrap in vigor'/'FUT', 'FUT:Wrap in Vigor:wrap in vigor').
card_rarity('wrap in vigor'/'FUT', 'Common').
card_artist('wrap in vigor'/'FUT', 'John Donahue').
card_number('wrap in vigor'/'FUT', '141').
card_flavor_text('wrap in vigor'/'FUT', 'Some nature mages unknowingly took advantage of the temporal energies still swirling on Dominaria. What they mistook for healing magic was in fact the manipulation of time.').
card_multiverse_id('wrap in vigor'/'FUT', '130331').

card_in_set('yixlid jailer', 'FUT').
card_original_type('yixlid jailer'/'FUT', 'Creature — Zombie Wizard').
card_original_text('yixlid jailer'/'FUT', 'Cards in graveyards lose all abilities.').
card_image_name('yixlid jailer'/'FUT', 'yixlid jailer').
card_uid('yixlid jailer'/'FUT', 'FUT:Yixlid Jailer:yixlid jailer').
card_rarity('yixlid jailer'/'FUT', 'Uncommon').
card_artist('yixlid jailer'/'FUT', 'Matt Cavotta').
card_number('yixlid jailer'/'FUT', '93').
card_flavor_text('yixlid jailer'/'FUT', '\"I have an eternity to know the souls who are bound here, to behold their every facet. But moments from now they will be gagged and masked, and they shall be known by none other.\"').
card_multiverse_id('yixlid jailer'/'FUT', '130702').
card_timeshifted('yixlid jailer'/'FUT')
.
card_in_set('zoetic cavern', 'FUT').
card_original_type('zoetic cavern'/'FUT', 'Land').
card_original_text('zoetic cavern'/'FUT', '{T}: Add {1} to your mana pool.\nMorph {2} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_image_name('zoetic cavern'/'FUT', 'zoetic cavern').
card_uid('zoetic cavern'/'FUT', 'FUT:Zoetic Cavern:zoetic cavern').
card_rarity('zoetic cavern'/'FUT', 'Uncommon').
card_artist('zoetic cavern'/'FUT', 'Lars Grant-West').
card_number('zoetic cavern'/'FUT', '180').
card_flavor_text('zoetic cavern'/'FUT', 'Nothing lives within it, yet there is life.').
card_multiverse_id('zoetic cavern'/'FUT', '132215').
card_timeshifted('zoetic cavern'/'FUT')
.