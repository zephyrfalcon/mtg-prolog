% From the Vault: Exiled

set('V09').
set_name('V09', 'From the Vault: Exiled').
set_release_date('V09', '2009-08-28').
set_border('V09', 'black').
set_type('V09', 'from the vault').

card_in_set('balance', 'V09').
card_original_type('balance'/'V09', 'Sorcery').
card_original_text('balance'/'V09', 'Each player chooses a number of lands he or she controls equal to the number of lands controlled by the player who controls the fewest, then sacrifices the rest. Players discard cards and sacrifice creatures the same way.').
card_image_name('balance'/'V09', 'balance').
card_uid('balance'/'V09', 'V09:Balance:balance').
card_rarity('balance'/'V09', 'Mythic Rare').
card_artist('balance'/'V09', 'Randy Gallegos').
card_number('balance'/'V09', '1').
card_multiverse_id('balance'/'V09', '194966').

card_in_set('berserk', 'V09').
card_original_type('berserk'/'V09', 'Instant').
card_original_text('berserk'/'V09', 'Cast Berserk only before the combat damage step.\nTarget creature gains trample and gets +X/+0 until end of turn, where X is its power. At the beginning of the end step, destroy that creature if it attacked this turn.').
card_image_name('berserk'/'V09', 'berserk').
card_uid('berserk'/'V09', 'V09:Berserk:berserk').
card_rarity('berserk'/'V09', 'Mythic Rare').
card_artist('berserk'/'V09', 'Steve Prescott').
card_number('berserk'/'V09', '2').
card_multiverse_id('berserk'/'V09', '194969').

card_in_set('channel', 'V09').
card_original_type('channel'/'V09', 'Sorcery').
card_original_text('channel'/'V09', 'Until end of turn, any time you could activate a mana ability, you may pay 1 life. If you do, add {1} to your mana pool.').
card_image_name('channel'/'V09', 'channel').
card_uid('channel'/'V09', 'V09:Channel:channel').
card_rarity('channel'/'V09', 'Mythic Rare').
card_artist('channel'/'V09', 'Rebecca Guay').
card_number('channel'/'V09', '3').
card_flavor_text('channel'/'V09', 'To some mages, the meaning of life is less important than how much is left.').
card_multiverse_id('channel'/'V09', '194967').

card_in_set('gifts ungiven', 'V09').
card_original_type('gifts ungiven'/'V09', 'Instant').
card_original_text('gifts ungiven'/'V09', 'Search your library for four cards with different names and reveal them. Target opponent chooses two of those cards. Put the chosen cards into your graveyard and the rest into your hand. Then shuffle your library.').
card_image_name('gifts ungiven'/'V09', 'gifts ungiven').
card_uid('gifts ungiven'/'V09', 'V09:Gifts Ungiven:gifts ungiven').
card_rarity('gifts ungiven'/'V09', 'Mythic Rare').
card_artist('gifts ungiven'/'V09', 'D. Alexander Gregory').
card_number('gifts ungiven'/'V09', '4').
card_multiverse_id('gifts ungiven'/'V09', '194971').

card_in_set('goblin lackey', 'V09').
card_original_type('goblin lackey'/'V09', 'Creature — Goblin').
card_original_text('goblin lackey'/'V09', 'Whenever Goblin Lackey deals damage to a player, you may put a Goblin permanent card from your hand onto the battlefield.').
card_image_name('goblin lackey'/'V09', 'goblin lackey').
card_uid('goblin lackey'/'V09', 'V09:Goblin Lackey:goblin lackey').
card_rarity('goblin lackey'/'V09', 'Mythic Rare').
card_artist('goblin lackey'/'V09', 'Christopher Moeller').
card_number('goblin lackey'/'V09', '5').
card_flavor_text('goblin lackey'/'V09', 'At the first sign of enemy scouts, Jula grinned and strapped on his diplomacy helmet.').
card_multiverse_id('goblin lackey'/'V09', '194973').

card_in_set('kird ape', 'V09').
card_original_type('kird ape'/'V09', 'Creature — Ape').
card_original_text('kird ape'/'V09', 'Kird Ape gets +1/+2 as long as you control a Forest.').
card_image_name('kird ape'/'V09', 'kird ape').
card_uid('kird ape'/'V09', 'V09:Kird Ape:kird ape').
card_rarity('kird ape'/'V09', 'Mythic Rare').
card_artist('kird ape'/'V09', 'Terese Nielsen').
card_number('kird ape'/'V09', '6').
card_flavor_text('kird ape'/'V09', 'Apes can be gentle, contemplative beasts—but context is everything.').
card_multiverse_id('kird ape'/'V09', '194974').

card_in_set('lotus petal', 'V09').
card_original_type('lotus petal'/'V09', 'Artifact').
card_original_text('lotus petal'/'V09', '{T}, Sacrifice Lotus Petal: Add one mana of any color to your mana pool.').
card_image_name('lotus petal'/'V09', 'lotus petal').
card_uid('lotus petal'/'V09', 'V09:Lotus Petal:lotus petal').
card_rarity('lotus petal'/'V09', 'Mythic Rare').
card_artist('lotus petal'/'V09', 'April Lee').
card_number('lotus petal'/'V09', '7').
card_flavor_text('lotus petal'/'V09', '\"Hard to imagine,\" mused Hanna, stroking the petal, \"such a lovely flower inspiring such greed.\"').
card_multiverse_id('lotus petal'/'V09', '194975').

card_in_set('mystical tutor', 'V09').
card_original_type('mystical tutor'/'V09', 'Instant').
card_original_text('mystical tutor'/'V09', 'Search your library for an instant or sorcery card and reveal that card. Shuffle your library, then put the card on top of it.').
card_image_name('mystical tutor'/'V09', 'mystical tutor').
card_uid('mystical tutor'/'V09', 'V09:Mystical Tutor:mystical tutor').
card_rarity('mystical tutor'/'V09', 'Mythic Rare').
card_artist('mystical tutor'/'V09', 'David O\'Connor').
card_number('mystical tutor'/'V09', '8').
card_flavor_text('mystical tutor'/'V09', 'The student searches the world for meaning. The master finds worlds of meaning in the search.').
card_multiverse_id('mystical tutor'/'V09', '194976').

card_in_set('necropotence', 'V09').
card_original_type('necropotence'/'V09', 'Enchantment').
card_original_text('necropotence'/'V09', 'Skip your draw step.\nWhenever you discard a card, exile that card from your graveyard.\nPay 1 life: Exile the top card of your library face down. Put that card into your hand at the beginning of your next end step.').
card_image_name('necropotence'/'V09', 'necropotence').
card_uid('necropotence'/'V09', 'V09:Necropotence:necropotence').
card_rarity('necropotence'/'V09', 'Mythic Rare').
card_artist('necropotence'/'V09', 'Dave Kendall').
card_number('necropotence'/'V09', '9').
card_multiverse_id('necropotence'/'V09', '194977').

card_in_set('sensei\'s divining top', 'V09').
card_original_type('sensei\'s divining top'/'V09', 'Artifact').
card_original_text('sensei\'s divining top'/'V09', '{1}: Look at the top three cards of your library, then put them back in any order.\n{T}: Draw a card, then put Sensei\'s Divining Top on top of its owner\'s library.').
card_image_name('sensei\'s divining top'/'V09', 'sensei\'s divining top').
card_uid('sensei\'s divining top'/'V09', 'V09:Sensei\'s Divining Top:sensei\'s divining top').
card_rarity('sensei\'s divining top'/'V09', 'Mythic Rare').
card_artist('sensei\'s divining top'/'V09', 'Michael Sutfin').
card_number('sensei\'s divining top'/'V09', '10').
card_multiverse_id('sensei\'s divining top'/'V09', '194972').

card_in_set('serendib efreet', 'V09').
card_original_type('serendib efreet'/'V09', 'Creature — Efreet').
card_original_text('serendib efreet'/'V09', 'Flying\nAt the beginning of your upkeep, Serendib Efreet deals 1 damage to you.').
card_image_name('serendib efreet'/'V09', 'serendib efreet').
card_uid('serendib efreet'/'V09', 'V09:Serendib Efreet:serendib efreet').
card_rarity('serendib efreet'/'V09', 'Mythic Rare').
card_artist('serendib efreet'/'V09', 'Matt Stewart').
card_number('serendib efreet'/'V09', '11').
card_flavor_text('serendib efreet'/'V09', 'Summoners of efreeti remember only the power of command, never the sting of regret.').
card_multiverse_id('serendib efreet'/'V09', '194970').

card_in_set('skullclamp', 'V09').
card_original_type('skullclamp'/'V09', 'Artifact — Equipment').
card_original_text('skullclamp'/'V09', 'Equipped creature gets +1/-1.\nWhen equipped creature is put into a graveyard, draw two cards.\nEquip {1}').
card_image_name('skullclamp'/'V09', 'skullclamp').
card_uid('skullclamp'/'V09', 'V09:Skullclamp:skullclamp').
card_rarity('skullclamp'/'V09', 'Mythic Rare').
card_artist('skullclamp'/'V09', 'Luca Zontini').
card_number('skullclamp'/'V09', '12').
card_flavor_text('skullclamp'/'V09', 'The mind is a beautiful bounty encased in an annoying bone container.').
card_multiverse_id('skullclamp'/'V09', '194978').

card_in_set('strip mine', 'V09').
card_original_type('strip mine'/'V09', 'Land').
card_original_text('strip mine'/'V09', '{T}: Add {1} to your mana pool.\n{T}, Sacrifice Strip Mine: Destroy target land.').
card_image_name('strip mine'/'V09', 'strip mine').
card_uid('strip mine'/'V09', 'V09:Strip Mine:strip mine').
card_rarity('strip mine'/'V09', 'Mythic Rare').
card_artist('strip mine'/'V09', 'John Avon').
card_number('strip mine'/'V09', '13').
card_flavor_text('strip mine'/'V09', 'Unlike previous conflicts, the war between Urza and Mishra made Dominaria itself a casualty of war.').
card_multiverse_id('strip mine'/'V09', '194968').

card_in_set('tinker', 'V09').
card_original_type('tinker'/'V09', 'Sorcery').
card_original_text('tinker'/'V09', 'As an additional cost to cast Tinker, sacrifice an artifact.\nSearch your library for an artifact card and put that card onto the battlefield. Then shuffle your library.').
card_image_name('tinker'/'V09', 'tinker').
card_uid('tinker'/'V09', 'V09:Tinker:tinker').
card_rarity('tinker'/'V09', 'Mythic Rare').
card_artist('tinker'/'V09', 'Chris Rahn').
card_number('tinker'/'V09', '14').
card_flavor_text('tinker'/'V09', '\"What separates us from mere beasts is the capacity for self-improvement.\"').
card_multiverse_id('tinker'/'V09', '194980').

card_in_set('trinisphere', 'V09').
card_original_type('trinisphere'/'V09', 'Artifact').
card_original_text('trinisphere'/'V09', 'As long as Trinisphere is untapped, each spell that would cost less than three mana to cast costs three mana to cast.').
card_image_name('trinisphere'/'V09', 'trinisphere').
card_uid('trinisphere'/'V09', 'V09:Trinisphere:trinisphere').
card_rarity('trinisphere'/'V09', 'Mythic Rare').
card_artist('trinisphere'/'V09', 'Tim Hildebrandt').
card_number('trinisphere'/'V09', '15').
card_flavor_text('trinisphere'/'V09', 'Designed to restrain petty thoughts, but used to repress entire empires.').
card_multiverse_id('trinisphere'/'V09', '194979').
