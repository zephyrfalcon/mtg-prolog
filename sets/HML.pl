% Homelands

set('HML').
set_name('HML', 'Homelands').
set_release_date('HML', '1995-10-01').
set_border('HML', 'black').
set_type('HML', 'expansion').

card_in_set('abbey gargoyles', 'HML').
card_original_type('abbey gargoyles'/'HML', 'Summon — Gargoyles').
card_original_text('abbey gargoyles'/'HML', 'Flying, protection from red').
card_first_print('abbey gargoyles', 'HML').
card_image_name('abbey gargoyles'/'HML', 'abbey gargoyles').
card_uid('abbey gargoyles'/'HML', 'HML:Abbey Gargoyles:abbey gargoyles').
card_rarity('abbey gargoyles'/'HML', 'Uncommon').
card_artist('abbey gargoyles'/'HML', 'Christopher Rush').
card_flavor_text('abbey gargoyles'/'HML', '\"Though Serra has not been seen for twenty years, her Gargoyles still watch over our city and punish the guilty.\"\n—Gulsen, Abbey Matron').
card_multiverse_id('abbey gargoyles'/'HML', '3010').

card_in_set('abbey matron', 'HML').
card_original_type('abbey matron'/'HML', 'Summon — Cleric').
card_original_text('abbey matron'/'HML', '{W}, {T}: +0/+3 until end of turn').
card_first_print('abbey matron', 'HML').
card_image_name('abbey matron'/'HML', 'abbey matron1').
card_uid('abbey matron'/'HML', 'HML:Abbey Matron:abbey matron1').
card_rarity('abbey matron'/'HML', 'Common').
card_artist('abbey matron'/'HML', 'Mike Kimble').
card_flavor_text('abbey matron'/'HML', '\"The Matrons are kindly souls, but don\'t ask one for an ale.\"\n—Halina, Dwarven Trader').
card_multiverse_id('abbey matron'/'HML', '3011').

card_in_set('abbey matron', 'HML').
card_original_type('abbey matron'/'HML', 'Summon — Cleric').
card_original_text('abbey matron'/'HML', '{W}, {T}: +0/+3 until end of turn').
card_image_name('abbey matron'/'HML', 'abbey matron2').
card_uid('abbey matron'/'HML', 'HML:Abbey Matron:abbey matron2').
card_rarity('abbey matron'/'HML', 'Common').
card_artist('abbey matron'/'HML', 'Mike Kimble').
card_flavor_text('abbey matron'/'HML', '\"The Matrons still say Serra is coming back. I bet she never even existed.\"\n—Murat, Death Speaker').
card_multiverse_id('abbey matron'/'HML', '3012').

card_in_set('æther storm', 'HML').
card_original_type('æther storm'/'HML', 'Enchantment').
card_original_text('æther storm'/'HML', 'No summon spells may be cast. Any player may pay 4 life to bury Æther Storm. Effects that prevent or redirect damage cannot be used to counter this loss of life.').
card_first_print('æther storm', 'HML').
card_image_name('æther storm'/'HML', 'aether storm').
card_uid('æther storm'/'HML', 'HML:Æther Storm:aether storm').
card_rarity('æther storm'/'HML', 'Uncommon').
card_artist('æther storm'/'HML', 'Mark Tedin').
card_flavor_text('æther storm'/'HML', '\"I do love the rain . . . but this storm feels somehow wrong, Taysir.\"\n—Daria').
card_multiverse_id('æther storm'/'HML', '2935').

card_in_set('aliban\'s tower', 'HML').
card_original_type('aliban\'s tower'/'HML', 'Instant').
card_original_text('aliban\'s tower'/'HML', 'Target blocking creature gets +3/+1 until end of turn.').
card_first_print('aliban\'s tower', 'HML').
card_image_name('aliban\'s tower'/'HML', 'aliban\'s tower1').
card_uid('aliban\'s tower'/'HML', 'HML:Aliban\'s Tower:aliban\'s tower1').
card_rarity('aliban\'s tower'/'HML', 'Common').
card_artist('aliban\'s tower'/'HML', 'Jeff A. Menges').
card_flavor_text('aliban\'s tower'/'HML', '\"The people of Aysen built their roads with stones from these wonders. Aliban would be pleased with their ingenuity.\"\n—Baki, Wizard Attendant').
card_multiverse_id('aliban\'s tower'/'HML', '2985').

card_in_set('aliban\'s tower', 'HML').
card_original_type('aliban\'s tower'/'HML', 'Instant').
card_original_text('aliban\'s tower'/'HML', 'Target blocking creature gets +3/+1 until end of turn.').
card_image_name('aliban\'s tower'/'HML', 'aliban\'s tower2').
card_uid('aliban\'s tower'/'HML', 'HML:Aliban\'s Tower:aliban\'s tower2').
card_rarity('aliban\'s tower'/'HML', 'Common').
card_artist('aliban\'s tower'/'HML', 'Jeff A. Menges').
card_flavor_text('aliban\'s tower'/'HML', '\"Those who hide in hollow towers would do well to remember the rain.\"\n—Reyhan, Samite Alchemist').
card_multiverse_id('aliban\'s tower'/'HML', '2986').

card_in_set('ambush', 'HML').
card_original_type('ambush'/'HML', 'Instant').
card_original_text('ambush'/'HML', 'All blocking creatures gain first strike until end of turn.').
card_first_print('ambush', 'HML').
card_image_name('ambush'/'HML', 'ambush').
card_uid('ambush'/'HML', 'HML:Ambush:ambush').
card_rarity('ambush'/'HML', 'Common').
card_artist('ambush'/'HML', 'Alan Rabinowitz').
card_flavor_text('ambush'/'HML', '\"Don\'t these goons have anything better to do?\"\n—Joskun, An-Havva Constable').
card_multiverse_id('ambush'/'HML', '2987').

card_in_set('ambush party', 'HML').
card_original_type('ambush party'/'HML', 'Summon — Ambush Party').
card_original_text('ambush party'/'HML', 'First strike\nAmbush Party can attack the turn it comes into play on your side.').
card_first_print('ambush party', 'HML').
card_image_name('ambush party'/'HML', 'ambush party1').
card_uid('ambush party'/'HML', 'HML:Ambush Party:ambush party1').
card_rarity('ambush party'/'HML', 'Common').
card_artist('ambush party'/'HML', 'Mark Poole').
card_flavor_text('ambush party'/'HML', '\"Call it a cost of doing business. The best way to protect against thieves is to hire better thieves.\"\n—Eron the Relentless').
card_multiverse_id('ambush party'/'HML', '2989').

card_in_set('ambush party', 'HML').
card_original_type('ambush party'/'HML', 'Summon — Ambush Party').
card_original_text('ambush party'/'HML', 'First strike\nAmbush Party can attack the turn it comes into play on your side.').
card_image_name('ambush party'/'HML', 'ambush party2').
card_uid('ambush party'/'HML', 'HML:Ambush Party:ambush party2').
card_rarity('ambush party'/'HML', 'Common').
card_artist('ambush party'/'HML', 'Mark Poole').
card_flavor_text('ambush party'/'HML', '\"The roads to Koskun Keep are mostly safe these days. Mostly.\"\n—Joskun, An-Havva Constable').
card_multiverse_id('ambush party'/'HML', '2988').

card_in_set('an-havva constable', 'HML').
card_original_type('an-havva constable'/'HML', 'Summon — Constable').
card_original_text('an-havva constable'/'HML', 'An-Havva Constable has toughness equal to 1 plus the total number of green creatures in play.').
card_first_print('an-havva constable', 'HML').
card_image_name('an-havva constable'/'HML', 'an-havva constable').
card_uid('an-havva constable'/'HML', 'HML:An-Havva Constable:an-havva constable').
card_rarity('an-havva constable'/'HML', 'Rare').
card_artist('an-havva constable'/'HML', 'Dan Frazier').
card_flavor_text('an-havva constable'/'HML', '\"Joskun and the other Constables serve with passion, if not with grace.\"\n—Devin, Faerie Noble').
card_multiverse_id('an-havva constable'/'HML', '2960').

card_in_set('an-havva inn', 'HML').
card_original_type('an-havva inn'/'HML', 'Sorcery').
card_original_text('an-havva inn'/'HML', 'Gain 1+* life, where * is equal to the total number of green creatures in play.').
card_first_print('an-havva inn', 'HML').
card_image_name('an-havva inn'/'HML', 'an-havva inn').
card_uid('an-havva inn'/'HML', 'HML:An-Havva Inn:an-havva inn').
card_rarity('an-havva inn'/'HML', 'Uncommon').
card_artist('an-havva inn'/'HML', 'Brian Snõddy').
card_flavor_text('an-havva inn'/'HML', '\"The Inn was ever a steady source of good cheer and kindness. Greatly do I miss it, now that I am gone.\"\n—Ihsan\'s Shade').
card_multiverse_id('an-havva inn'/'HML', '2961').

card_in_set('an-havva township', 'HML').
card_original_type('an-havva township'/'HML', 'Land').
card_original_text('an-havva township'/'HML', '{T}: Add one colorless mana to your mana pool.\n{1}, {T}: Add {G} to your mana pool.\n{2}, {T}: Add {W} to your mana pool.\n{2}, {T}: Add {R} to your mana pool.').
card_first_print('an-havva township', 'HML').
card_image_name('an-havva township'/'HML', 'an-havva township').
card_uid('an-havva township'/'HML', 'HML:An-Havva Township:an-havva township').
card_rarity('an-havva township'/'HML', 'Uncommon').
card_artist('an-havva township'/'HML', 'Liz Danforth').
card_flavor_text('an-havva township'/'HML', 'Folk of good heart will always find a home.').
card_multiverse_id('an-havva township'/'HML', '3035').

card_in_set('an-zerrin ruins', 'HML').
card_original_type('an-zerrin ruins'/'HML', 'Enchantment').
card_original_text('an-zerrin ruins'/'HML', 'Choose a creature type. Creatures of that type do not untap during their controller\'s untap phase.').
card_first_print('an-zerrin ruins', 'HML').
card_image_name('an-zerrin ruins'/'HML', 'an-zerrin ruins').
card_uid('an-zerrin ruins'/'HML', 'HML:An-Zerrin Ruins:an-zerrin ruins').
card_rarity('an-zerrin ruins'/'HML', 'Rare').
card_artist('an-zerrin ruins'/'HML', 'Dennis Detwiller').
card_flavor_text('an-zerrin ruins'/'HML', '\"The An-Zerrins have served me well, ever since I first killed them.\"\n—Baron Sengir').
card_multiverse_id('an-zerrin ruins'/'HML', '2990').

card_in_set('anaba ancestor', 'HML').
card_original_type('anaba ancestor'/'HML', 'Summon — Ghost').
card_original_text('anaba ancestor'/'HML', '{T}: Target Minotaur gets +1/+1 until end of turn.').
card_first_print('anaba ancestor', 'HML').
card_image_name('anaba ancestor'/'HML', 'anaba ancestor').
card_uid('anaba ancestor'/'HML', 'HML:Anaba Ancestor:anaba ancestor').
card_rarity('anaba ancestor'/'HML', 'Rare').
card_artist('anaba ancestor'/'HML', 'Anson Maddocks').
card_flavor_text('anaba ancestor'/'HML', '\"The Ancestors are the wisdom of the tribe and the soul of the Homelands. I am eternally in their debt.\"\n—Taysir').
card_multiverse_id('anaba ancestor'/'HML', '2991').

card_in_set('anaba bodyguard', 'HML').
card_original_type('anaba bodyguard'/'HML', 'Summon — Bodyguard').
card_original_text('anaba bodyguard'/'HML', 'First strike').
card_first_print('anaba bodyguard', 'HML').
card_image_name('anaba bodyguard'/'HML', 'anaba bodyguard1').
card_uid('anaba bodyguard'/'HML', 'HML:Anaba Bodyguard:anaba bodyguard1').
card_rarity('anaba bodyguard'/'HML', 'Common').
card_artist('anaba bodyguard'/'HML', 'Anson Maddocks').
card_flavor_text('anaba bodyguard'/'HML', '\"Not all Minotaurs are tribal. Some are freelance.\"\n—Eron the Relentless').
card_multiverse_id('anaba bodyguard'/'HML', '2992').

card_in_set('anaba bodyguard', 'HML').
card_original_type('anaba bodyguard'/'HML', 'Summon — Bodyguard').
card_original_text('anaba bodyguard'/'HML', 'First strike').
card_image_name('anaba bodyguard'/'HML', 'anaba bodyguard2').
card_uid('anaba bodyguard'/'HML', 'HML:Anaba Bodyguard:anaba bodyguard2').
card_rarity('anaba bodyguard'/'HML', 'Common').
card_artist('anaba bodyguard'/'HML', 'Anson Maddocks').
card_flavor_text('anaba bodyguard'/'HML', '\"Not someone to pick a fight with, unless you\'re fond of pain.\"\n—Halina, Dwarven Trader').
card_multiverse_id('anaba bodyguard'/'HML', '2993').

card_in_set('anaba shaman', 'HML').
card_original_type('anaba shaman'/'HML', 'Summon — Minotaur').
card_original_text('anaba shaman'/'HML', '{R}, {T}: Anaba Shaman deals 1 damage to target creature or player.').
card_first_print('anaba shaman', 'HML').
card_image_name('anaba shaman'/'HML', 'anaba shaman1').
card_uid('anaba shaman'/'HML', 'HML:Anaba Shaman:anaba shaman1').
card_rarity('anaba shaman'/'HML', 'Common').
card_artist('anaba shaman'/'HML', 'Anson Maddocks').
card_flavor_text('anaba shaman'/'HML', '\"The Shamans? Ha They are mere craven cows not capable of true magic.\"\n—Irini Sengir').
card_multiverse_id('anaba shaman'/'HML', '2995').

card_in_set('anaba shaman', 'HML').
card_original_type('anaba shaman'/'HML', 'Summon — Minotaur').
card_original_text('anaba shaman'/'HML', '{R}, {T}: Anaba Shaman deals 1 damage to target creature or player.').
card_image_name('anaba shaman'/'HML', 'anaba shaman2').
card_uid('anaba shaman'/'HML', 'HML:Anaba Shaman:anaba shaman2').
card_rarity('anaba shaman'/'HML', 'Common').
card_artist('anaba shaman'/'HML', 'Anson Maddocks').
card_flavor_text('anaba shaman'/'HML', '\"Few master the powers of the earth as well as do the Shamans of the Minotaurs.\"\n—Baki, Wizard Attendant').
card_multiverse_id('anaba shaman'/'HML', '2994').

card_in_set('anaba spirit crafter', 'HML').
card_original_type('anaba spirit crafter'/'HML', 'Summon — Minotaur').
card_original_text('anaba spirit crafter'/'HML', 'All Minotaurs get +1/+0.').
card_first_print('anaba spirit crafter', 'HML').
card_image_name('anaba spirit crafter'/'HML', 'anaba spirit crafter').
card_uid('anaba spirit crafter'/'HML', 'HML:Anaba Spirit Crafter:anaba spirit crafter').
card_rarity('anaba spirit crafter'/'HML', 'Rare').
card_artist('anaba spirit crafter'/'HML', 'Anson Maddocks').
card_flavor_text('anaba spirit crafter'/'HML', '\"The Spirit Crafters sing of all our people. They sing of those lost, of those found, and of those who are yet to be.\"\n—Onatah, Anaba Shaman').
card_multiverse_id('anaba spirit crafter'/'HML', '2996').

card_in_set('apocalypse chime', 'HML').
card_original_type('apocalypse chime'/'HML', 'Artifact').
card_original_text('apocalypse chime'/'HML', '{2}, {T}: Sacrifice Apocalypse Chime to bury all cards from the Homelands expansion.').
card_first_print('apocalypse chime', 'HML').
card_image_name('apocalypse chime'/'HML', 'apocalypse chime').
card_uid('apocalypse chime'/'HML', 'HML:Apocalypse Chime:apocalypse chime').
card_rarity('apocalypse chime'/'HML', 'Rare').
card_artist('apocalypse chime'/'HML', 'Mark Poole').
card_flavor_text('apocalypse chime'/'HML', '\"One day, or another, perhaps I shall ring my pretty chime . . . loudly, so that all may hear.\"\n—Grandmother Sengir').
card_multiverse_id('apocalypse chime'/'HML', '2900').

card_in_set('autumn willow', 'HML').
card_original_type('autumn willow'/'HML', 'Summon — Legend').
card_original_text('autumn willow'/'HML', 'Cannot be the target of spells or effects.\n{G} Target player may target Autumn Willow with spells or effects until end of turn.').
card_first_print('autumn willow', 'HML').
card_image_name('autumn willow'/'HML', 'autumn willow').
card_uid('autumn willow'/'HML', 'HML:Autumn Willow:autumn willow').
card_rarity('autumn willow'/'HML', 'Rare').
card_artist('autumn willow'/'HML', 'Margaret Organ-Kean').
card_flavor_text('autumn willow'/'HML', '\"We must shake her limbs and rattle her brains.\"\n—Grandmother Sengir').
card_multiverse_id('autumn willow'/'HML', '2962').

card_in_set('aysen abbey', 'HML').
card_original_type('aysen abbey'/'HML', 'Land').
card_original_text('aysen abbey'/'HML', '{T}: Add one colorless mana to your mana pool.\n{1}, {T}: Add {W} to your mana pool.\n{2}, {T}: Add {U} to your mana pool.\n{2}, {T}: Add {G} to your mana pool.').
card_first_print('aysen abbey', 'HML').
card_image_name('aysen abbey'/'HML', 'aysen abbey').
card_uid('aysen abbey'/'HML', 'HML:Aysen Abbey:aysen abbey').
card_rarity('aysen abbey'/'HML', 'Uncommon').
card_artist('aysen abbey'/'HML', 'Liz Danforth').
card_flavor_text('aysen abbey'/'HML', 'Serra\'s gift to her people: a symbol of faith and hope.').
card_multiverse_id('aysen abbey'/'HML', '3036').

card_in_set('aysen bureaucrats', 'HML').
card_original_type('aysen bureaucrats'/'HML', 'Summon — Bureaucrats').
card_original_text('aysen bureaucrats'/'HML', '{T}: Tap target creature with power no greater than 2.').
card_first_print('aysen bureaucrats', 'HML').
card_image_name('aysen bureaucrats'/'HML', 'aysen bureaucrats1').
card_uid('aysen bureaucrats'/'HML', 'HML:Aysen Bureaucrats:aysen bureaucrats1').
card_rarity('aysen bureaucrats'/'HML', 'Common').
card_artist('aysen bureaucrats'/'HML', 'Alan Rabinowitz').
card_flavor_text('aysen bureaucrats'/'HML', '\"I would say that our Bureaucrats are no better than vipers—but I shouldn\'t insult the vipers.\"\n—Murat, Death Speaker').
card_multiverse_id('aysen bureaucrats'/'HML', '3013').

card_in_set('aysen bureaucrats', 'HML').
card_original_type('aysen bureaucrats'/'HML', 'Summon — Bureaucrats').
card_original_text('aysen bureaucrats'/'HML', '{T}: Tap target creature with power no greater than 2.').
card_image_name('aysen bureaucrats'/'HML', 'aysen bureaucrats2').
card_uid('aysen bureaucrats'/'HML', 'HML:Aysen Bureaucrats:aysen bureaucrats2').
card_rarity('aysen bureaucrats'/'HML', 'Common').
card_artist('aysen bureaucrats'/'HML', 'Alan Rabinowitz').
card_flavor_text('aysen bureaucrats'/'HML', '\"All the tortures of my vault of horrors pale in comparison to dealing with those petty Bureaucrats.\"\n—Baron Sengir').
card_multiverse_id('aysen bureaucrats'/'HML', '3014').

card_in_set('aysen crusader', 'HML').
card_original_type('aysen crusader'/'HML', 'Summon — Crusader').
card_original_text('aysen crusader'/'HML', 'Aysen Crusader has power and toughness each equal to 2 plus the number of Heroes you control.').
card_first_print('aysen crusader', 'HML').
card_image_name('aysen crusader'/'HML', 'aysen crusader').
card_uid('aysen crusader'/'HML', 'HML:Aysen Crusader:aysen crusader').
card_rarity('aysen crusader'/'HML', 'Rare').
card_artist('aysen crusader'/'HML', 'NéNé Thomas').
card_flavor_text('aysen crusader'/'HML', '\"A renegade rallying the rabble does not a true Crusader make.\"\n—Irini Sengir').
card_multiverse_id('aysen crusader'/'HML', '3015').

card_in_set('aysen highway', 'HML').
card_original_type('aysen highway'/'HML', 'Enchantment').
card_original_text('aysen highway'/'HML', 'All white creatures gain plainswalk.').
card_first_print('aysen highway', 'HML').
card_image_name('aysen highway'/'HML', 'aysen highway').
card_uid('aysen highway'/'HML', 'HML:Aysen Highway:aysen highway').
card_rarity('aysen highway'/'HML', 'Rare').
card_artist('aysen highway'/'HML', 'NéNé Thomas').
card_flavor_text('aysen highway'/'HML', '\"Tread the roads of righteousness, Murat, lest you lose your way and be consumed.\"\n—Baris, Serra Inquisitor').
card_multiverse_id('aysen highway'/'HML', '3016').

card_in_set('baki\'s curse', 'HML').
card_original_type('baki\'s curse'/'HML', 'Sorcery').
card_original_text('baki\'s curse'/'HML', 'Baki\'s Curse deals 2 damage to each creature for each creature enchantment on that creature.').
card_first_print('baki\'s curse', 'HML').
card_image_name('baki\'s curse'/'HML', 'baki\'s curse').
card_uid('baki\'s curse'/'HML', 'HML:Baki\'s Curse:baki\'s curse').
card_rarity('baki\'s curse'/'HML', 'Rare').
card_artist('baki\'s curse'/'HML', 'Nicola Leonard').
card_flavor_text('baki\'s curse'/'HML', '\"Those who fling spells too quickly should have reason to regret it.\"\n—Baki, Wizard Attendant').
card_multiverse_id('baki\'s curse'/'HML', '2936').

card_in_set('baron sengir', 'HML').
card_original_type('baron sengir'/'HML', 'Summon — Legend').
card_original_text('baron sengir'/'HML', 'Flying\nWhenever a creature is put into the graveyard the same turn Baron Sengir damaged it, put a +2/+2 counter on Baron Sengir.\n{T}: Regenerate target Vampire.').
card_first_print('baron sengir', 'HML').
card_image_name('baron sengir'/'HML', 'baron sengir').
card_uid('baron sengir'/'HML', 'HML:Baron Sengir:baron sengir').
card_rarity('baron sengir'/'HML', 'Rare').
card_artist('baron sengir'/'HML', 'Pete Venters').
card_flavor_text('baron sengir'/'HML', '\"Beast. Defiler. The source of all my pain.\" —Ihsan\'s Shade').
card_multiverse_id('baron sengir'/'HML', '2910').

card_in_set('beast walkers', 'HML').
card_original_type('beast walkers'/'HML', 'Summon — Heroes').
card_original_text('beast walkers'/'HML', '{G} Banding until end of turn').
card_first_print('beast walkers', 'HML').
card_image_name('beast walkers'/'HML', 'beast walkers').
card_uid('beast walkers'/'HML', 'HML:Beast Walkers:beast walkers').
card_rarity('beast walkers'/'HML', 'Rare').
card_artist('beast walkers'/'HML', 'Heather Hudson').
card_flavor_text('beast walkers'/'HML', '\"The Beast Walkers do a great service to Aysen. As humans or as beasts, their heart is yet to Serra true.\"\n—Baris, Serra Inquisitor').
card_multiverse_id('beast walkers'/'HML', '3017').

card_in_set('black carriage', 'HML').
card_original_type('black carriage'/'HML', 'Summon — Carriage').
card_original_text('black carriage'/'HML', 'Trample\nDoes not untap during your untap phase.\n{0}: Sacrifice a creature to untap Black Carriage. Use this ability only during your upkeep.').
card_first_print('black carriage', 'HML').
card_image_name('black carriage'/'HML', 'black carriage').
card_uid('black carriage'/'HML', 'HML:Black Carriage:black carriage').
card_rarity('black carriage'/'HML', 'Rare').
card_artist('black carriage'/'HML', 'David A. Cherry').
card_flavor_text('black carriage'/'HML', '\"The Baron\'s drivers are also driven.\"\n—Chandler').
card_multiverse_id('black carriage'/'HML', '2911').

card_in_set('broken visage', 'HML').
card_original_type('broken visage'/'HML', 'Instant').
card_original_text('broken visage'/'HML', 'Bury target non-artifact attacking creature and put a Shadow token into play.\nTreat this token as a black creature with power and toughness equal to the power and toughness of that attacking creature. Bury Shadow token at end of turn.').
card_first_print('broken visage', 'HML').
card_image_name('broken visage'/'HML', 'broken visage').
card_uid('broken visage'/'HML', 'HML:Broken Visage:broken visage').
card_rarity('broken visage'/'HML', 'Rare').
card_artist('broken visage'/'HML', 'Mike Kimble').
card_multiverse_id('broken visage'/'HML', '2912').

card_in_set('carapace', 'HML').
card_original_type('carapace'/'HML', 'Enchant Creature').
card_original_text('carapace'/'HML', 'Target creature gets +0/+2.\n{0}: Sacrifice Carapace to regenerate creature Carapace enchants.').
card_first_print('carapace', 'HML').
card_image_name('carapace'/'HML', 'carapace1').
card_uid('carapace'/'HML', 'HML:Carapace:carapace1').
card_rarity('carapace'/'HML', 'Common').
card_artist('carapace'/'HML', 'Anson Maddocks').
card_flavor_text('carapace'/'HML', '\"The tougher to crack, the sweeter the snack.\"\n—Kakra, Sea Troll').
card_multiverse_id('carapace'/'HML', '2963').

card_in_set('carapace', 'HML').
card_original_type('carapace'/'HML', 'Enchant Creature').
card_original_text('carapace'/'HML', 'Target creature gets +0/+2.\n{0}: Sacrifice Carapace to regenerate creature Carapace enchants.').
card_image_name('carapace'/'HML', 'carapace2').
card_uid('carapace'/'HML', 'HML:Carapace:carapace2').
card_rarity('carapace'/'HML', 'Common').
card_artist('carapace'/'HML', 'Anson Maddocks').
card_flavor_text('carapace'/'HML', '\"Now, that\'s a fashion statement.\"\n—Devin, Faerie Noble').
card_multiverse_id('carapace'/'HML', '2964').

card_in_set('castle sengir', 'HML').
card_original_type('castle sengir'/'HML', 'Land').
card_original_text('castle sengir'/'HML', '{T}: Add one colorless mana to your mana pool.\n{1}, {T}: Add {B} to your mana pool.\n{2}, {T}: Add {U} to your mana pool.\n{2}, {T}: Add {R} to your mana pool.').
card_first_print('castle sengir', 'HML').
card_image_name('castle sengir'/'HML', 'castle sengir').
card_uid('castle sengir'/'HML', 'HML:Castle Sengir:castle sengir').
card_rarity('castle sengir'/'HML', 'Uncommon').
card_artist('castle sengir'/'HML', 'Pete Venters').
card_flavor_text('castle sengir'/'HML', 'Where hope has no champion, evil rules all.').
card_multiverse_id('castle sengir'/'HML', '3037').

card_in_set('cemetery gate', 'HML').
card_original_type('cemetery gate'/'HML', 'Summon — Wall').
card_original_text('cemetery gate'/'HML', 'Protection from black').
card_first_print('cemetery gate', 'HML').
card_image_name('cemetery gate'/'HML', 'cemetery gate1').
card_uid('cemetery gate'/'HML', 'HML:Cemetery Gate:cemetery gate1').
card_rarity('cemetery gate'/'HML', 'Common').
card_artist('cemetery gate'/'HML', 'Melissa A. Benson').
card_flavor_text('cemetery gate'/'HML', '\"It keeps some out, yes. It also keeps others in\"\n—Grandmother Sengir').
card_multiverse_id('cemetery gate'/'HML', '2913').

card_in_set('cemetery gate', 'HML').
card_original_type('cemetery gate'/'HML', 'Summon — Wall').
card_original_text('cemetery gate'/'HML', 'Protection from black').
card_image_name('cemetery gate'/'HML', 'cemetery gate2').
card_uid('cemetery gate'/'HML', 'HML:Cemetery Gate:cemetery gate2').
card_rarity('cemetery gate'/'HML', 'Common').
card_artist('cemetery gate'/'HML', 'Melissa A. Benson').
card_flavor_text('cemetery gate'/'HML', '\"Just the place for a picnic.\"\n—Murat, Death Speaker').
card_multiverse_id('cemetery gate'/'HML', '2914').

card_in_set('chain stasis', 'HML').
card_original_type('chain stasis'/'HML', 'Instant').
card_original_text('chain stasis'/'HML', 'Tap or untap target creature. Whenever any player uses Chain Stasis to tap or untap a creature, that creature\'s controller may pay {2}{U} to use Chain Stasis to tap or untap any target creature.').
card_first_print('chain stasis', 'HML').
card_image_name('chain stasis'/'HML', 'chain stasis').
card_uid('chain stasis'/'HML', 'HML:Chain Stasis:chain stasis').
card_rarity('chain stasis'/'HML', 'Rare').
card_artist('chain stasis'/'HML', 'Pat Morrissey').
card_flavor_text('chain stasis'/'HML', '\"Here we go again.\"\n—Kakra, Sea Troll').
card_multiverse_id('chain stasis'/'HML', '2937').

card_in_set('chandler', 'HML').
card_original_type('chandler'/'HML', 'Summon — Legend').
card_original_text('chandler'/'HML', '{R}{R}{R}, {T}: Destroy target artifact creature.').
card_first_print('chandler', 'HML').
card_image_name('chandler'/'HML', 'chandler').
card_uid('chandler'/'HML', 'HML:Chandler:chandler').
card_rarity('chandler'/'HML', 'Common').
card_artist('chandler'/'HML', 'Douglas Shuler').
card_flavor_text('chandler'/'HML', '\"Never brag about the latest wonder you\'ve created, students. Chandler might be listening.\"\n—Reveka, Wizard Savant').
card_multiverse_id('chandler'/'HML', '2997').

card_in_set('clockwork gnomes', 'HML').
card_original_type('clockwork gnomes'/'HML', 'Artifact Creature').
card_original_text('clockwork gnomes'/'HML', '{3}, {T}: Regenerate target artifact creature.').
card_first_print('clockwork gnomes', 'HML').
card_image_name('clockwork gnomes'/'HML', 'clockwork gnomes').
card_uid('clockwork gnomes'/'HML', 'HML:Clockwork Gnomes:clockwork gnomes').
card_rarity('clockwork gnomes'/'HML', 'Common').
card_artist('clockwork gnomes'/'HML', 'Douglas Shuler').
card_flavor_text('clockwork gnomes'/'HML', '\"Until I came to the Floating Isle, I never had to oil the servants.\"\n—Baki, Wizard Attendant').
card_multiverse_id('clockwork gnomes'/'HML', '2901').

card_in_set('clockwork steed', 'HML').
card_original_type('clockwork steed'/'HML', 'Artifact Creature').
card_original_text('clockwork steed'/'HML', 'Cannot be blocked by artifact creatures.\nWhen Clockwork Steed comes into play, put four +1/+0 counters on it. At the end of any combat in which Clockwork Steed attacked or blocked, remove one of these counters.\n{X}, {T}: Put X +1/+0 counters on Clockwork Steed. You may have no more than four of these counters on Clockwork Steed. Use this ability only during your upkeep.').
card_first_print('clockwork steed', 'HML').
card_image_name('clockwork steed'/'HML', 'clockwork steed').
card_uid('clockwork steed'/'HML', 'HML:Clockwork Steed:clockwork steed').
card_rarity('clockwork steed'/'HML', 'Common').
card_artist('clockwork steed'/'HML', 'Amy Weber').
card_multiverse_id('clockwork steed'/'HML', '2902').

card_in_set('clockwork swarm', 'HML').
card_original_type('clockwork swarm'/'HML', 'Artifact Creature').
card_original_text('clockwork swarm'/'HML', 'Cannot be blocked by walls.\nWhen Clockwork Swarm comes into play, put four +1/+0 counters on it. At the end of any combat in which Clockwork Swarm attacked or blocked, remove one of these counters.\n{X}, {T}: Put X +1/+0 counters on Clockwork Swarm. You may have no more than four of these counters on Clockwork Swarm. Use this ability only during your upkeep.').
card_first_print('clockwork swarm', 'HML').
card_image_name('clockwork swarm'/'HML', 'clockwork swarm').
card_uid('clockwork swarm'/'HML', 'HML:Clockwork Swarm:clockwork swarm').
card_rarity('clockwork swarm'/'HML', 'Common').
card_artist('clockwork swarm'/'HML', 'Amy Weber').
card_multiverse_id('clockwork swarm'/'HML', '2903').

card_in_set('coral reef', 'HML').
card_original_type('coral reef'/'HML', 'Enchantment').
card_original_text('coral reef'/'HML', 'When Coral Reef comes into play, put four polyp counters on it.\n{0}: Sacrifice an island to put two polyp counters on Coral Reef.\n{U} Tap target blue creature you control and remove a polyp counter from Coral Reef to put a +0/+1 counter on any target creature.').
card_first_print('coral reef', 'HML').
card_image_name('coral reef'/'HML', 'coral reef').
card_uid('coral reef'/'HML', 'HML:Coral Reef:coral reef').
card_rarity('coral reef'/'HML', 'Common').
card_artist('coral reef'/'HML', 'Amy Weber').
card_multiverse_id('coral reef'/'HML', '2938').

card_in_set('dark maze', 'HML').
card_original_type('dark maze'/'HML', 'Summon — Wall').
card_original_text('dark maze'/'HML', '{0}: Dark Maze can attack this turn. At end of turn, remove Dark Maze from the game. Dark Maze cannot attack the turn it comes under your control.').
card_first_print('dark maze', 'HML').
card_image_name('dark maze'/'HML', 'dark maze1').
card_uid('dark maze'/'HML', 'HML:Dark Maze:dark maze1').
card_rarity('dark maze'/'HML', 'Common').
card_artist('dark maze'/'HML', 'Rob Alexander').
card_flavor_text('dark maze'/'HML', '\"The path of faith is fortunately fraught with failure.\"\n—Irini Sengir').
card_multiverse_id('dark maze'/'HML', '2940').

card_in_set('dark maze', 'HML').
card_original_type('dark maze'/'HML', 'Summon — Wall').
card_original_text('dark maze'/'HML', '{0}: Dark Maze can attack this turn. At end of turn, remove Dark Maze from the game. Dark Maze cannot attack the turn it comes under your control.').
card_image_name('dark maze'/'HML', 'dark maze2').
card_uid('dark maze'/'HML', 'HML:Dark Maze:dark maze2').
card_rarity('dark maze'/'HML', 'Common').
card_artist('dark maze'/'HML', 'Rob Alexander').
card_flavor_text('dark maze'/'HML', '\"Stray not into the path of darkness, or be lost forever.\"\n—Baki, Wizard Attendant').
card_multiverse_id('dark maze'/'HML', '2939').

card_in_set('daughter of autumn', 'HML').
card_original_type('daughter of autumn'/'HML', 'Summon — Legend').
card_original_text('daughter of autumn'/'HML', '{W} Redirect to Daughter of Autumn 1 damage dealt to a white creature.').
card_first_print('daughter of autumn', 'HML').
card_image_name('daughter of autumn'/'HML', 'daughter of autumn').
card_uid('daughter of autumn'/'HML', 'HML:Daughter of Autumn:daughter of autumn').
card_rarity('daughter of autumn'/'HML', 'Rare').
card_artist('daughter of autumn'/'HML', 'Margaret Organ-Kean').
card_flavor_text('daughter of autumn'/'HML', '\"Those who say that Serra is dead have never met the Autumn Willow\'s daughters. They are as kind and as generous as Serra would wish us to be.\"\n—Gulsen, Abbey Matron').
card_multiverse_id('daughter of autumn'/'HML', '2965').

card_in_set('death speakers', 'HML').
card_original_type('death speakers'/'HML', 'Summon — Speakers').
card_original_text('death speakers'/'HML', 'Protection from black').
card_first_print('death speakers', 'HML').
card_image_name('death speakers'/'HML', 'death speakers').
card_uid('death speakers'/'HML', 'HML:Death Speakers:death speakers').
card_rarity('death speakers'/'HML', 'Uncommon').
card_artist('death speakers'/'HML', 'Douglas Shuler').
card_flavor_text('death speakers'/'HML', '\"Such innocent little birds. They sing a sweet song, sitting on their fragile branch.\"\n—Grandmother Sengir').
card_multiverse_id('death speakers'/'HML', '3018').

card_in_set('didgeridoo', 'HML').
card_original_type('didgeridoo'/'HML', 'Artifact').
card_original_text('didgeridoo'/'HML', '{3}: Take a Minotaur from your hand and put it directly into play as though it were just summoned.').
card_first_print('didgeridoo', 'HML').
card_image_name('didgeridoo'/'HML', 'didgeridoo').
card_uid('didgeridoo'/'HML', 'HML:Didgeridoo:didgeridoo').
card_rarity('didgeridoo'/'HML', 'Rare').
card_artist('didgeridoo'/'HML', 'Melissa A. Benson').
card_flavor_text('didgeridoo'/'HML', '\"Play the song of he who delivered us. Play the song of Feroz.\"\n—Onatah, Anaba Shaman').
card_multiverse_id('didgeridoo'/'HML', '2904').

card_in_set('drudge spell', 'HML').
card_original_type('drudge spell'/'HML', 'Enchantment').
card_original_text('drudge spell'/'HML', '{B} Remove from the game two target creatures in your graveyard to put a Skeleton token into play. Treat this token as a 1/1 black creature with \"{B} Regenerate\". If Drudge Spell leaves play, bury all Skeleton tokens.').
card_first_print('drudge spell', 'HML').
card_image_name('drudge spell'/'HML', 'drudge spell').
card_uid('drudge spell'/'HML', 'HML:Drudge Spell:drudge spell').
card_rarity('drudge spell'/'HML', 'Uncommon').
card_artist('drudge spell'/'HML', 'NéNé Thomas').
card_multiverse_id('drudge spell'/'HML', '2915').

card_in_set('dry spell', 'HML').
card_original_type('dry spell'/'HML', 'Sorcery').
card_original_text('dry spell'/'HML', 'Dry Spell deals 1 damage to each creature and player.').
card_first_print('dry spell', 'HML').
card_image_name('dry spell'/'HML', 'dry spell1').
card_uid('dry spell'/'HML', 'HML:Dry Spell:dry spell1').
card_rarity('dry spell'/'HML', 'Common').
card_artist('dry spell'/'HML', 'Brian Snõddy').
card_flavor_text('dry spell'/'HML', '\"Wherever water is lacking, all things suffer.\"\n—Autumn Willow').
card_multiverse_id('dry spell'/'HML', '2916').

card_in_set('dry spell', 'HML').
card_original_type('dry spell'/'HML', 'Sorcery').
card_original_text('dry spell'/'HML', 'Dry Spell deals 1 damage to each creature and player.').
card_image_name('dry spell'/'HML', 'dry spell2').
card_uid('dry spell'/'HML', 'HML:Dry Spell:dry spell2').
card_rarity('dry spell'/'HML', 'Common').
card_artist('dry spell'/'HML', 'Brian Snõddy').
card_flavor_text('dry spell'/'HML', '\"My soul thirsts for Serra\'s salvation, as does my body for life\'s breath.\"\n—Ihsan\'s Shade').
card_multiverse_id('dry spell'/'HML', '2917').

card_in_set('dwarven pony', 'HML').
card_original_type('dwarven pony'/'HML', 'Summon — Pony').
card_original_text('dwarven pony'/'HML', '{1}{R}, {T}: Target Dwarf gains mountainwalk until end of turn.').
card_first_print('dwarven pony', 'HML').
card_image_name('dwarven pony'/'HML', 'dwarven pony').
card_uid('dwarven pony'/'HML', 'HML:Dwarven Pony:dwarven pony').
card_rarity('dwarven pony'/'HML', 'Rare').
card_artist('dwarven pony'/'HML', 'Margaret Organ-Kean').
card_flavor_text('dwarven pony'/'HML', '\"I hear Halina\'s got a pony that can count to six—and likes to eat meat.\"\n—Chandler').
card_multiverse_id('dwarven pony'/'HML', '2998').

card_in_set('dwarven sea clan', 'HML').
card_original_type('dwarven sea clan'/'HML', 'Summon — Dwarves').
card_original_text('dwarven sea clan'/'HML', '{T}: At end of combat, Dwarven Sea Clan deals 2 damage to target attacking or blocking creature. Use this ability only if that creature\'s controller controls any islands.').
card_first_print('dwarven sea clan', 'HML').
card_image_name('dwarven sea clan'/'HML', 'dwarven sea clan').
card_uid('dwarven sea clan'/'HML', 'HML:Dwarven Sea Clan:dwarven sea clan').
card_rarity('dwarven sea clan'/'HML', 'Rare').
card_artist('dwarven sea clan'/'HML', 'Amy Weber').
card_flavor_text('dwarven sea clan'/'HML', '\"No dwarf alive can best one of my crew dead.\"\n—Zeki, Reef Pirate').
card_multiverse_id('dwarven sea clan'/'HML', '2999').

card_in_set('dwarven trader', 'HML').
card_original_type('dwarven trader'/'HML', 'Summon — Dwarf').
card_original_text('dwarven trader'/'HML', '').
card_first_print('dwarven trader', 'HML').
card_image_name('dwarven trader'/'HML', 'dwarven trader1').
card_uid('dwarven trader'/'HML', 'HML:Dwarven Trader:dwarven trader1').
card_rarity('dwarven trader'/'HML', 'Common').
card_artist('dwarven trader'/'HML', 'Margaret Organ-Kean').
card_flavor_text('dwarven trader'/'HML', '\"Their definition of ‘fair profit\' is certainly novel.\"\n—Reveka, Wizard Savant').
card_multiverse_id('dwarven trader'/'HML', '3000').

card_in_set('dwarven trader', 'HML').
card_original_type('dwarven trader'/'HML', 'Summon — Dwarf').
card_original_text('dwarven trader'/'HML', '').
card_image_name('dwarven trader'/'HML', 'dwarven trader2').
card_uid('dwarven trader'/'HML', 'HML:Dwarven Trader:dwarven trader2').
card_rarity('dwarven trader'/'HML', 'Common').
card_artist('dwarven trader'/'HML', 'Margaret Organ-Kean').
card_flavor_text('dwarven trader'/'HML', '\"They wouldn\'t sell their own families—at least, not for cheap.\"\n—Zeki, Reef Pirate').
card_multiverse_id('dwarven trader'/'HML', '3001').

card_in_set('ebony rhino', 'HML').
card_original_type('ebony rhino'/'HML', 'Artifact Creature').
card_original_text('ebony rhino'/'HML', 'Trample').
card_first_print('ebony rhino', 'HML').
card_image_name('ebony rhino'/'HML', 'ebony rhino').
card_uid('ebony rhino'/'HML', 'HML:Ebony Rhino:ebony rhino').
card_rarity('ebony rhino'/'HML', 'Common').
card_artist('ebony rhino'/'HML', 'Amy Weber').
card_flavor_text('ebony rhino'/'HML', '\"That Rhino would fetch us a tidy sum, Joven. Perhaps it\'s time to make it ours.\"\n—Chandler').
card_multiverse_id('ebony rhino'/'HML', '2905').

card_in_set('eron the relentless', 'HML').
card_original_type('eron the relentless'/'HML', 'Summon — Legend').
card_original_text('eron the relentless'/'HML', 'Eron the Relentless can attack the turn it comes into play on your side.\n{R}{R}{R} Regenerate').
card_first_print('eron the relentless', 'HML').
card_image_name('eron the relentless'/'HML', 'eron the relentless').
card_uid('eron the relentless'/'HML', 'HML:Eron the Relentless:eron the relentless').
card_rarity('eron the relentless'/'HML', 'Uncommon').
card_artist('eron the relentless'/'HML', 'Christopher Rush').
card_flavor_text('eron the relentless'/'HML', '\"Eron would be much less of a hassle if only he were mortal.\"\n—Reyhan, Samite Alchemist').
card_multiverse_id('eron the relentless'/'HML', '3002').

card_in_set('evaporate', 'HML').
card_original_type('evaporate'/'HML', 'Sorcery').
card_original_text('evaporate'/'HML', 'Evaporate deals 1 damage to each blue creature and white creature.').
card_first_print('evaporate', 'HML').
card_image_name('evaporate'/'HML', 'evaporate').
card_uid('evaporate'/'HML', 'HML:Evaporate:evaporate').
card_rarity('evaporate'/'HML', 'Uncommon').
card_artist('evaporate'/'HML', 'Alan Rabinowitz').
card_flavor_text('evaporate'/'HML', '\"The body is a temple with a foundation of sand.\"\n—Autumn Willow').
card_multiverse_id('evaporate'/'HML', '3003').

card_in_set('faerie noble', 'HML').
card_original_type('faerie noble'/'HML', 'Summon — Noble').
card_original_text('faerie noble'/'HML', 'Flying\nAll Faeries you control get +0/+1.\n{T}: All Faeries you control get +1/+0 until end of turn.').
card_first_print('faerie noble', 'HML').
card_image_name('faerie noble'/'HML', 'faerie noble').
card_uid('faerie noble'/'HML', 'HML:Faerie Noble:faerie noble').
card_rarity('faerie noble'/'HML', 'Rare').
card_artist('faerie noble'/'HML', 'Susan Van Camp').
card_flavor_text('faerie noble'/'HML', '\"Faeries talk all in riddles and tricky bits, ‘cept the Nobles. Now, there\'s some straight talkers.\"\n—Joskun, An-Havva Constable').
card_multiverse_id('faerie noble'/'HML', '2966').

card_in_set('feast of the unicorn', 'HML').
card_original_type('feast of the unicorn'/'HML', 'Enchant Creature').
card_original_text('feast of the unicorn'/'HML', 'Target creature gets +4/+0.').
card_first_print('feast of the unicorn', 'HML').
card_image_name('feast of the unicorn'/'HML', 'feast of the unicorn1').
card_uid('feast of the unicorn'/'HML', 'HML:Feast of the Unicorn:feast of the unicorn1').
card_rarity('feast of the unicorn'/'HML', 'Common').
card_artist('feast of the unicorn'/'HML', 'Dennis Detwiller').
card_flavor_text('feast of the unicorn'/'HML', '\"Some delicacies are not to be savored, save by the callous.\"\n—Baron Sengir').
card_multiverse_id('feast of the unicorn'/'HML', '2918').

card_in_set('feast of the unicorn', 'HML').
card_original_type('feast of the unicorn'/'HML', 'Enchant Creature').
card_original_text('feast of the unicorn'/'HML', 'Target creature gets +4/+0.').
card_image_name('feast of the unicorn'/'HML', 'feast of the unicorn2').
card_uid('feast of the unicorn'/'HML', 'HML:Feast of the Unicorn:feast of the unicorn2').
card_rarity('feast of the unicorn'/'HML', 'Common').
card_artist('feast of the unicorn'/'HML', 'Dennis Detwiller').
card_flavor_text('feast of the unicorn'/'HML', '\"Could there be a fouler act? No doubt the Baron knows of one.\"\n—Autumn Willow').
card_multiverse_id('feast of the unicorn'/'HML', '2919').

card_in_set('feroz\'s ban', 'HML').
card_original_type('feroz\'s ban'/'HML', 'Artifact').
card_original_text('feroz\'s ban'/'HML', 'Summon spells each cost an additional {2} to cast.').
card_first_print('feroz\'s ban', 'HML').
card_image_name('feroz\'s ban'/'HML', 'feroz\'s ban').
card_uid('feroz\'s ban'/'HML', 'HML:Feroz\'s Ban:feroz\'s ban').
card_rarity('feroz\'s ban'/'HML', 'Rare').
card_artist('feroz\'s ban'/'HML', 'Heather Hudson').
card_flavor_text('feroz\'s ban'/'HML', '\"Without the protection of Feroz\'s Ban, I fear the Homelands are lost.\"\n—Daria').
card_multiverse_id('feroz\'s ban'/'HML', '2906').

card_in_set('folk of an-havva', 'HML').
card_original_type('folk of an-havva'/'HML', 'Summon — Folk of An-Havva').
card_original_text('folk of an-havva'/'HML', 'If assigned as a blocker, Folk of An-Havva gets +2/+0 until end of turn.').
card_first_print('folk of an-havva', 'HML').
card_image_name('folk of an-havva'/'HML', 'folk of an-havva1').
card_uid('folk of an-havva'/'HML', 'HML:Folk of An-Havva:folk of an-havva1').
card_rarity('folk of an-havva'/'HML', 'Common').
card_artist('folk of an-havva'/'HML', 'Julie Baroh').
card_flavor_text('folk of an-havva'/'HML', '\"This town\'s only for good folk. The rest can go to the city.\"\n—Joskun, An-Havva Constable').
card_multiverse_id('folk of an-havva'/'HML', '2968').

card_in_set('folk of an-havva', 'HML').
card_original_type('folk of an-havva'/'HML', 'Summon — Folk of An-Havva').
card_original_text('folk of an-havva'/'HML', 'If assigned as a blocker, Folk of An-Havva gets +2/+0 until end of turn.').
card_image_name('folk of an-havva'/'HML', 'folk of an-havva2').
card_uid('folk of an-havva'/'HML', 'HML:Folk of An-Havva:folk of an-havva2').
card_rarity('folk of an-havva'/'HML', 'Common').
card_artist('folk of an-havva'/'HML', 'Julie Baroh').
card_flavor_text('folk of an-havva'/'HML', '\"There are those who accept being told what to do, what to think, and what to say. Then there are the Folk of An-Havva.\"\n—Reyhan, Samite Alchemist').
card_multiverse_id('folk of an-havva'/'HML', '2967').

card_in_set('forget', 'HML').
card_original_type('forget'/'HML', 'Sorcery').
card_original_text('forget'/'HML', 'Target player chooses and discards 2 cards from his or her hand. If that player does not have enough cards in hand, his or her entire hand is discarded. The player then draws as many cards as he or she discarded in this way.').
card_first_print('forget', 'HML').
card_image_name('forget'/'HML', 'forget').
card_uid('forget'/'HML', 'HML:Forget:forget').
card_rarity('forget'/'HML', 'Rare').
card_artist('forget'/'HML', 'Mike Kimble').
card_multiverse_id('forget'/'HML', '2941').

card_in_set('funeral march', 'HML').
card_original_type('funeral march'/'HML', 'Enchant Creature').
card_original_text('funeral march'/'HML', 'When target creature leaves play, that creature\'s controller sacrifices a creature he or she controls. Ignore this effect if that player controls no creatures.').
card_first_print('funeral march', 'HML').
card_image_name('funeral march'/'HML', 'funeral march').
card_uid('funeral march'/'HML', 'HML:Funeral March:funeral march').
card_rarity('funeral march'/'HML', 'Common').
card_artist('funeral march'/'HML', 'Melissa A. Benson').
card_flavor_text('funeral march'/'HML', '\"This party is such fun—but it\'s a shame to mourn just one.\"\n—Irini Sengir').
card_multiverse_id('funeral march'/'HML', '2920').

card_in_set('ghost hounds', 'HML').
card_original_type('ghost hounds'/'HML', 'Summon — Hounds').
card_original_text('ghost hounds'/'HML', 'Attacking does not cause Ghost Hounds to tap. \nIf assigned to block any white creatures or any white creatures are assigned to block it, Ghost Hounds gains first strike until end of turn.').
card_first_print('ghost hounds', 'HML').
card_image_name('ghost hounds'/'HML', 'ghost hounds').
card_uid('ghost hounds'/'HML', 'HML:Ghost Hounds:ghost hounds').
card_rarity('ghost hounds'/'HML', 'Uncommon').
card_artist('ghost hounds'/'HML', 'Jeff A. Menges').
card_multiverse_id('ghost hounds'/'HML', '2921').

card_in_set('giant albatross', 'HML').
card_original_type('giant albatross'/'HML', 'Summon — Albatross').
card_original_text('giant albatross'/'HML', 'Flying\n{1}{U} Bury all creatures that damaged Giant Albatross this turn. The controller of any of those creatures may pay 2 life to prevent that creature from being buried. Effects that prevent or redirect damage cannot be used to counter this loss of life. Use this ability only when Giant Albatross is put into the graveyard from play.').
card_first_print('giant albatross', 'HML').
card_image_name('giant albatross'/'HML', 'giant albatross1').
card_uid('giant albatross'/'HML', 'HML:Giant Albatross:giant albatross1').
card_rarity('giant albatross'/'HML', 'Common').
card_artist('giant albatross'/'HML', 'David A. Cherry').
card_multiverse_id('giant albatross'/'HML', '2942').

card_in_set('giant albatross', 'HML').
card_original_type('giant albatross'/'HML', 'Summon — Albatross').
card_original_text('giant albatross'/'HML', 'Flying\n{1}{U} Bury all creatures that damaged Giant Albatross this turn. The controller of any of those creatures may pay 2 life to prevent that creature from being buried. Effects that prevent or redirect damage cannot be used to counter this loss of life. Use this ability only when Giant Albatross is put into the graveyard from play.').
card_image_name('giant albatross'/'HML', 'giant albatross2').
card_uid('giant albatross'/'HML', 'HML:Giant Albatross:giant albatross2').
card_rarity('giant albatross'/'HML', 'Common').
card_artist('giant albatross'/'HML', 'David A. Cherry').
card_multiverse_id('giant albatross'/'HML', '2943').

card_in_set('giant oyster', 'HML').
card_original_type('giant oyster'/'HML', 'Summon — Oyster').
card_original_text('giant oyster'/'HML', 'You may choose not to untap Giant Oyster during your untap phase.\n{T}: Target tapped creature does not untap during its controller\'s untap phase as long as Giant Oyster remains tapped. During your upkeep, put a -1/-1 counter on that creature. If Giant Oyster becomes untapped or leaves play, remove all these counters from the creature.').
card_first_print('giant oyster', 'HML').
card_image_name('giant oyster'/'HML', 'giant oyster').
card_uid('giant oyster'/'HML', 'HML:Giant Oyster:giant oyster').
card_rarity('giant oyster'/'HML', 'Uncommon').
card_artist('giant oyster'/'HML', 'Nicola Leonard').
card_multiverse_id('giant oyster'/'HML', '2944').

card_in_set('grandmother sengir', 'HML').
card_original_type('grandmother sengir'/'HML', 'Summon — Legend').
card_original_text('grandmother sengir'/'HML', '{1}{B}, {T}: Target creature gets -1/-1 until end of turn.').
card_first_print('grandmother sengir', 'HML').
card_image_name('grandmother sengir'/'HML', 'grandmother sengir').
card_uid('grandmother sengir'/'HML', 'HML:Grandmother Sengir:grandmother sengir').
card_rarity('grandmother sengir'/'HML', 'Rare').
card_artist('grandmother sengir'/'HML', 'Pete Venters').
card_flavor_text('grandmother sengir'/'HML', '\"Rarely have power and madness been so delightfully wed as they have in our dear Grandmother.\"\n—Baron Sengir').
card_multiverse_id('grandmother sengir'/'HML', '2922').

card_in_set('greater werewolf', 'HML').
card_original_type('greater werewolf'/'HML', 'Summon — Lycanthrope').
card_original_text('greater werewolf'/'HML', 'At end of combat, put a -0/-2 counter on all creatures blocking or blocked by Greater Werewolf.').
card_first_print('greater werewolf', 'HML').
card_image_name('greater werewolf'/'HML', 'greater werewolf').
card_uid('greater werewolf'/'HML', 'HML:Greater Werewolf:greater werewolf').
card_rarity('greater werewolf'/'HML', 'Common').
card_artist('greater werewolf'/'HML', 'Dennis Detwiller').
card_flavor_text('greater werewolf'/'HML', '\"The wolf-things are no mere beasts, but the lieutenants of the Baron.\"\n—Taysir').
card_multiverse_id('greater werewolf'/'HML', '2923').

card_in_set('hazduhr the abbot', 'HML').
card_original_type('hazduhr the abbot'/'HML', 'Summon — Legend').
card_original_text('hazduhr the abbot'/'HML', '{X}, {T}: Redirect to Hazduhr the Abbot X damage dealt to any white creature you control.').
card_first_print('hazduhr the abbot', 'HML').
card_image_name('hazduhr the abbot'/'HML', 'hazduhr the abbot').
card_uid('hazduhr the abbot'/'HML', 'HML:Hazduhr the Abbot:hazduhr the abbot').
card_rarity('hazduhr the abbot'/'HML', 'Rare').
card_artist('hazduhr the abbot'/'HML', 'Dan Frazier').
card_flavor_text('hazduhr the abbot'/'HML', '\"Soon, Serra will return and choose the Abbot\'s successor, else we are lost.\"\n—Gulsen, Abbey Matron').
card_multiverse_id('hazduhr the abbot'/'HML', '3019').

card_in_set('headstone', 'HML').
card_original_type('headstone'/'HML', 'Instant').
card_original_text('headstone'/'HML', 'Remove from the game target card in any graveyard.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_first_print('headstone', 'HML').
card_image_name('headstone'/'HML', 'headstone').
card_uid('headstone'/'HML', 'HML:Headstone:headstone').
card_rarity('headstone'/'HML', 'Common').
card_artist('headstone'/'HML', 'David A. Cherry').
card_flavor_text('headstone'/'HML', '\"Your headstone is the last page in the book of your life.\"\n—Murat, Death Speaker').
card_multiverse_id('headstone'/'HML', '2924').

card_in_set('heart wolf', 'HML').
card_original_type('heart wolf'/'HML', 'Summon — Wolf').
card_original_text('heart wolf'/'HML', 'First strike\n{T}: Target Dwarf gains first strike and gets +2/+0 until end of turn. If that Dwarf leaves play this turn, bury Heart Wolf. Use this ability only when attack or defense is announced.').
card_first_print('heart wolf', 'HML').
card_image_name('heart wolf'/'HML', 'heart wolf').
card_uid('heart wolf'/'HML', 'HML:Heart Wolf:heart wolf').
card_rarity('heart wolf'/'HML', 'Rare').
card_artist('heart wolf'/'HML', 'Margaret Organ-Kean').
card_multiverse_id('heart wolf'/'HML', '3004').

card_in_set('hungry mist', 'HML').
card_original_type('hungry mist'/'HML', 'Summon — Mist').
card_original_text('hungry mist'/'HML', 'During your upkeep, pay {G}{G} or bury Hungry Mist.').
card_first_print('hungry mist', 'HML').
card_image_name('hungry mist'/'HML', 'hungry mist1').
card_uid('hungry mist'/'HML', 'HML:Hungry Mist:hungry mist1').
card_rarity('hungry mist'/'HML', 'Common').
card_artist('hungry mist'/'HML', 'Heather Hudson').
card_flavor_text('hungry mist'/'HML', '\"All things must eat, after all. Even the air can hunger.\"\n—Gemma, Willow Priestess').
card_multiverse_id('hungry mist'/'HML', '2969').

card_in_set('hungry mist', 'HML').
card_original_type('hungry mist'/'HML', 'Summon — Mist').
card_original_text('hungry mist'/'HML', 'During your upkeep, pay {G}{G} or bury Hungry Mist.').
card_image_name('hungry mist'/'HML', 'hungry mist2').
card_uid('hungry mist'/'HML', 'HML:Hungry Mist:hungry mist2').
card_rarity('hungry mist'/'HML', 'Common').
card_artist('hungry mist'/'HML', 'Heather Hudson').
card_flavor_text('hungry mist'/'HML', '\"If the air must feed, let it take the self-righteous.\"\n—Murat, Death Speaker').
card_multiverse_id('hungry mist'/'HML', '2970').

card_in_set('ihsan\'s shade', 'HML').
card_original_type('ihsan\'s shade'/'HML', 'Summon — Legend').
card_original_text('ihsan\'s shade'/'HML', 'Protection from white').
card_first_print('ihsan\'s shade', 'HML').
card_image_name('ihsan\'s shade'/'HML', 'ihsan\'s shade').
card_uid('ihsan\'s shade'/'HML', 'HML:Ihsan\'s Shade:ihsan\'s shade').
card_rarity('ihsan\'s shade'/'HML', 'Uncommon').
card_artist('ihsan\'s shade'/'HML', 'Christopher Rush').
card_flavor_text('ihsan\'s shade'/'HML', '\"Ihsan, the weak. Ihsan, the fallen. Ihsan, the betrayer. He has brought shame to the Serra Paladins where none existed before. May his suffering equal his betrayal.\"\n—Baris, Serra Inquisitor').
card_multiverse_id('ihsan\'s shade'/'HML', '2925').

card_in_set('irini sengir', 'HML').
card_original_type('irini sengir'/'HML', 'Summon — Legend').
card_original_text('irini sengir'/'HML', 'White enchantments and green enchantments each cost an additional {2} to cast.').
card_first_print('irini sengir', 'HML').
card_image_name('irini sengir'/'HML', 'irini sengir').
card_uid('irini sengir'/'HML', 'HML:Irini Sengir:irini sengir').
card_rarity('irini sengir'/'HML', 'Uncommon').
card_artist('irini sengir'/'HML', 'Pete Venters').
card_flavor_text('irini sengir'/'HML', '\"That cruel being brings shame to all her fellow Dwarves and misery to all the land. She is Sengir\'s daughter in spirit if not in blood.\"\n—Reveka, Wizard Savant').
card_multiverse_id('irini sengir'/'HML', '2926').

card_in_set('ironclaw curse', 'HML').
card_original_type('ironclaw curse'/'HML', 'Enchant Creature').
card_original_text('ironclaw curse'/'HML', 'Target creature gets -0/-1. That creature cannot be assigned to block any creature with power greater than or equal to the toughness of the creature Ironclaw Curse enchants.').
card_first_print('ironclaw curse', 'HML').
card_image_name('ironclaw curse'/'HML', 'ironclaw curse').
card_uid('ironclaw curse'/'HML', 'HML:Ironclaw Curse:ironclaw curse').
card_rarity('ironclaw curse'/'HML', 'Rare').
card_artist('ironclaw curse'/'HML', 'Dennis Detwiller').
card_multiverse_id('ironclaw curse'/'HML', '3005').

card_in_set('jinx', 'HML').
card_original_type('jinx'/'HML', 'Instant').
card_original_text('jinx'/'HML', 'Target land becomes a basic land type of your choice until end of turn. Draw a card at the beginning of the next turn\'s upkeep.').
card_first_print('jinx', 'HML').
card_image_name('jinx'/'HML', 'jinx').
card_uid('jinx'/'HML', 'HML:Jinx:jinx').
card_rarity('jinx'/'HML', 'Common').
card_artist('jinx'/'HML', 'Mike Kimble').
card_flavor_text('jinx'/'HML', '\"What wizards upset, the land soon rights.\"\n—Gemma, Willow Priestess').
card_multiverse_id('jinx'/'HML', '2945').

card_in_set('joven', 'HML').
card_original_type('joven'/'HML', 'Summon — Legend').
card_original_text('joven'/'HML', '{R}{R}{R}, {T}: Destroy target non-creature artifact.').
card_first_print('joven', 'HML').
card_image_name('joven'/'HML', 'joven').
card_uid('joven'/'HML', 'HML:Joven:joven').
card_rarity('joven'/'HML', 'Common').
card_artist('joven'/'HML', 'Douglas Shuler').
card_flavor_text('joven'/'HML', '\"Joven, you\'re almost as good a thief as I. But Eron hates you even more.\"\n—Chandler').
card_multiverse_id('joven'/'HML', '3006').

card_in_set('joven\'s ferrets', 'HML').
card_original_type('joven\'s ferrets'/'HML', 'Summon — Ferrets').
card_original_text('joven\'s ferrets'/'HML', 'If declared as an attacker, Joven\'s Ferrets gets +0/+2 until end of turn. At end of combat, tap any creatures that blocked Joven\'s Ferrets. Those creatures do not untap during their controller\'s next untap phase.').
card_first_print('joven\'s ferrets', 'HML').
card_image_name('joven\'s ferrets'/'HML', 'joven\'s ferrets').
card_uid('joven\'s ferrets'/'HML', 'HML:Joven\'s Ferrets:joven\'s ferrets').
card_rarity('joven\'s ferrets'/'HML', 'Common').
card_artist('joven\'s ferrets'/'HML', 'Amy Weber').
card_multiverse_id('joven\'s ferrets'/'HML', '2971').

card_in_set('joven\'s tools', 'HML').
card_original_type('joven\'s tools'/'HML', 'Artifact').
card_original_text('joven\'s tools'/'HML', '{4}, {T}: Target creature cannot be blocked except by walls until end of turn.').
card_first_print('joven\'s tools', 'HML').
card_image_name('joven\'s tools'/'HML', 'joven\'s tools').
card_uid('joven\'s tools'/'HML', 'HML:Joven\'s Tools:joven\'s tools').
card_rarity('joven\'s tools'/'HML', 'Uncommon').
card_artist('joven\'s tools'/'HML', 'Nicola Leonard').
card_flavor_text('joven\'s tools'/'HML', '\"If that thief Joven ever shows his head around here again, make sure he leaves without it.\"\n—Eron the Relentless').
card_multiverse_id('joven\'s tools'/'HML', '2907').

card_in_set('koskun falls', 'HML').
card_original_type('koskun falls'/'HML', 'Enchant World').
card_original_text('koskun falls'/'HML', 'During your upkeep, tap target untapped creature you control or bury Koskun Falls.\nNo creature can attack you unless its controller pays an additional {2} whenever that creature attacks.').
card_first_print('koskun falls', 'HML').
card_image_name('koskun falls'/'HML', 'koskun falls').
card_uid('koskun falls'/'HML', 'HML:Koskun Falls:koskun falls').
card_rarity('koskun falls'/'HML', 'Rare').
card_artist('koskun falls'/'HML', 'Rob Alexander').
card_multiverse_id('koskun falls'/'HML', '2927').

card_in_set('koskun keep', 'HML').
card_original_type('koskun keep'/'HML', 'Land').
card_original_text('koskun keep'/'HML', '{T}: Add one colorless mana to your mana pool.\n{1}, {T}: Add {R} to your mana pool.\n{2}, {T}: Add {B} to your mana pool.\n{2}, {T}: Add {G} to your mana pool.').
card_first_print('koskun keep', 'HML').
card_image_name('koskun keep'/'HML', 'koskun keep').
card_uid('koskun keep'/'HML', 'HML:Koskun Keep:koskun keep').
card_rarity('koskun keep'/'HML', 'Uncommon').
card_artist('koskun keep'/'HML', 'Pat Morrissey').
card_flavor_text('koskun keep'/'HML', 'Treachery and anarchy make poor partners.').
card_multiverse_id('koskun keep'/'HML', '3038').

card_in_set('labyrinth minotaur', 'HML').
card_original_type('labyrinth minotaur'/'HML', 'Summon — Minotaur').
card_original_text('labyrinth minotaur'/'HML', 'Creatures Labyrinth Minotaur is assigned to block do not untap during their controller\'s next untap phase.').
card_first_print('labyrinth minotaur', 'HML').
card_image_name('labyrinth minotaur'/'HML', 'labyrinth minotaur1').
card_uid('labyrinth minotaur'/'HML', 'HML:Labyrinth Minotaur:labyrinth minotaur1').
card_rarity('labyrinth minotaur'/'HML', 'Common').
card_artist('labyrinth minotaur'/'HML', 'Anson Maddocks').
card_flavor_text('labyrinth minotaur'/'HML', '\"I doubt any Labyrinth Minotaurs still live—but then again, we Minotaurs are stubborn beings.\"\n—Onatah, Anaba Shaman').
card_multiverse_id('labyrinth minotaur'/'HML', '2947').

card_in_set('labyrinth minotaur', 'HML').
card_original_type('labyrinth minotaur'/'HML', 'Summon — Minotaur').
card_original_text('labyrinth minotaur'/'HML', 'Creatures Labyrinth Minotaur is assigned to block do not untap during their controller\'s next untap phase.').
card_image_name('labyrinth minotaur'/'HML', 'labyrinth minotaur2').
card_uid('labyrinth minotaur'/'HML', 'HML:Labyrinth Minotaur:labyrinth minotaur2').
card_rarity('labyrinth minotaur'/'HML', 'Common').
card_artist('labyrinth minotaur'/'HML', 'Anson Maddocks').
card_flavor_text('labyrinth minotaur'/'HML', '\"Legend says they got a treasure cave, but it don\'t say where to find it.\"\n—Zeki, Reef Pirate').
card_multiverse_id('labyrinth minotaur'/'HML', '2946').

card_in_set('leaping lizard', 'HML').
card_original_type('leaping lizard'/'HML', 'Summon — Lizard').
card_original_text('leaping lizard'/'HML', '{1}{G} Flying and -0/-1 until end of turn').
card_first_print('leaping lizard', 'HML').
card_image_name('leaping lizard'/'HML', 'leaping lizard').
card_uid('leaping lizard'/'HML', 'HML:Leaping Lizard:leaping lizard').
card_rarity('leaping lizard'/'HML', 'Common').
card_artist('leaping lizard'/'HML', 'Amy Weber').
card_flavor_text('leaping lizard'/'HML', '\"I never question the Autumn Willow about her motives, not even when she turns people into Lizards. It\'s her way.\"\n—Devin, Faerie Noble').
card_multiverse_id('leaping lizard'/'HML', '2972').

card_in_set('leeches', 'HML').
card_original_type('leeches'/'HML', 'Sorcery').
card_original_text('leeches'/'HML', 'Target player loses all poison counters. Leeches deals 1 damage to that player for each poison counter removed in this way.').
card_first_print('leeches', 'HML').
card_image_name('leeches'/'HML', 'leeches').
card_uid('leeches'/'HML', 'HML:Leeches:leeches').
card_rarity('leeches'/'HML', 'Rare').
card_artist('leeches'/'HML', 'Alan Rabinowitz').
card_flavor_text('leeches'/'HML', '\"Where our potions and powders fail, perhaps nature will succeed.\"\n—Reyhan, Samite Alchemist').
card_multiverse_id('leeches'/'HML', '3020').

card_in_set('mammoth harness', 'HML').
card_original_type('mammoth harness'/'HML', 'Enchant Creature').
card_original_text('mammoth harness'/'HML', 'Target creature loses flying. If any creature is assigned to block the creature Mammoth Harness enchants or has the creature Mammoth Harness enchants assigned to block it, that creature gains first strike until end of turn.').
card_first_print('mammoth harness', 'HML').
card_image_name('mammoth harness'/'HML', 'mammoth harness').
card_uid('mammoth harness'/'HML', 'HML:Mammoth Harness:mammoth harness').
card_rarity('mammoth harness'/'HML', 'Rare').
card_artist('mammoth harness'/'HML', 'Melissa A. Benson').
card_multiverse_id('mammoth harness'/'HML', '2973').

card_in_set('marjhan', 'HML').
card_original_type('marjhan'/'HML', 'Summon — Serpent').
card_original_text('marjhan'/'HML', 'Does not untap during your untap phase.\nMarjhan cannot attack if defending player controls no islands. If at any time you control no islands, bury Marjhan.\n{U}{U} Sacrifice a creature to untap Marjhan. Use this ability only during your upkeep.\n{U}{U} -1/-0 until end of turn. Marjhan deals 1 damage to target attacking creature without flying.').
card_first_print('marjhan', 'HML').
card_image_name('marjhan'/'HML', 'marjhan').
card_uid('marjhan'/'HML', 'HML:Marjhan:marjhan').
card_rarity('marjhan'/'HML', 'Rare').
card_artist('marjhan'/'HML', 'Daniel Gelon').
card_multiverse_id('marjhan'/'HML', '2948').

card_in_set('memory lapse', 'HML').
card_original_type('memory lapse'/'HML', 'Interrupt').
card_original_text('memory lapse'/'HML', 'Counter target spell. Put that spell on top of its owner\'s library.').
card_first_print('memory lapse', 'HML').
card_image_name('memory lapse'/'HML', 'memory lapse1').
card_uid('memory lapse'/'HML', 'HML:Memory Lapse:memory lapse1').
card_rarity('memory lapse'/'HML', 'Common').
card_artist('memory lapse'/'HML', 'Mark Tedin').
card_flavor_text('memory lapse'/'HML', '\"Um . . . oh . . . what was I saying?\"\n—Reveka, Wizard Savant').
card_multiverse_id('memory lapse'/'HML', '2949').

card_in_set('memory lapse', 'HML').
card_original_type('memory lapse'/'HML', 'Interrupt').
card_original_text('memory lapse'/'HML', 'Counter target spell. Put that spell on top of its owner\'s library.').
card_image_name('memory lapse'/'HML', 'memory lapse2').
card_uid('memory lapse'/'HML', 'HML:Memory Lapse:memory lapse2').
card_rarity('memory lapse'/'HML', 'Common').
card_artist('memory lapse'/'HML', 'Mark Tedin').
card_flavor_text('memory lapse'/'HML', '\"Oh, I had a conscience once. But alas, I seem to have forgotten where I put it.\"\n—Chandler').
card_multiverse_id('memory lapse'/'HML', '2950').

card_in_set('merchant scroll', 'HML').
card_original_type('merchant scroll'/'HML', 'Sorcery').
card_original_text('merchant scroll'/'HML', 'Search your library for a blue instant or interrupt. Reveal that card to all players and put it into your hand. Reshuffle your library afterwards.').
card_first_print('merchant scroll', 'HML').
card_image_name('merchant scroll'/'HML', 'merchant scroll').
card_uid('merchant scroll'/'HML', 'HML:Merchant Scroll:merchant scroll').
card_rarity('merchant scroll'/'HML', 'Common').
card_artist('merchant scroll'/'HML', 'Liz Danforth').
card_flavor_text('merchant scroll'/'HML', '\"There\'s no trade without trust.\"\n—Eron, the Relentless').
card_multiverse_id('merchant scroll'/'HML', '2951').

card_in_set('mesa falcon', 'HML').
card_original_type('mesa falcon'/'HML', 'Summon — Falcon').
card_original_text('mesa falcon'/'HML', 'Flying\n{1}{W} +0/+1 until end of turn').
card_first_print('mesa falcon', 'HML').
card_image_name('mesa falcon'/'HML', 'mesa falcon1').
card_uid('mesa falcon'/'HML', 'HML:Mesa Falcon:mesa falcon1').
card_rarity('mesa falcon'/'HML', 'Common').
card_artist('mesa falcon'/'HML', 'Mark Poole').
card_flavor_text('mesa falcon'/'HML', '\"The faith of Serra is borne on wings of hope.\"\n—Gulsen, Abbey Matron').
card_multiverse_id('mesa falcon'/'HML', '3022').

card_in_set('mesa falcon', 'HML').
card_original_type('mesa falcon'/'HML', 'Summon — Falcon').
card_original_text('mesa falcon'/'HML', 'Flying\n{1}{W} +0/+1 until end of turn').
card_image_name('mesa falcon'/'HML', 'mesa falcon2').
card_uid('mesa falcon'/'HML', 'HML:Mesa Falcon:mesa falcon2').
card_rarity('mesa falcon'/'HML', 'Common').
card_artist('mesa falcon'/'HML', 'Mark Poole').
card_flavor_text('mesa falcon'/'HML', '\"Pretty Falcon, friend to Soraya. Will you send her my greetings?\"\n—Gemma, Willow Priestess').
card_multiverse_id('mesa falcon'/'HML', '3021').

card_in_set('mystic decree', 'HML').
card_original_type('mystic decree'/'HML', 'Enchant World').
card_original_text('mystic decree'/'HML', 'All creatures lose flying and islandwalk.').
card_first_print('mystic decree', 'HML').
card_image_name('mystic decree'/'HML', 'mystic decree').
card_uid('mystic decree'/'HML', 'HML:Mystic Decree:mystic decree').
card_rarity('mystic decree'/'HML', 'Rare').
card_artist('mystic decree'/'HML', 'Liz Danforth').
card_flavor_text('mystic decree'/'HML', '\"Curse Reveka, and curse her coddled conjurers. Their sorcerers\' school shall yet be ours.\"\n—Irini Sengir').
card_multiverse_id('mystic decree'/'HML', '2952').

card_in_set('narwhal', 'HML').
card_original_type('narwhal'/'HML', 'Summon — Narwhal').
card_original_text('narwhal'/'HML', 'First strike, protection from red').
card_first_print('narwhal', 'HML').
card_image_name('narwhal'/'HML', 'narwhal').
card_uid('narwhal'/'HML', 'HML:Narwhal:narwhal').
card_rarity('narwhal'/'HML', 'Rare').
card_artist('narwhal'/'HML', 'David A. Cherry').
card_flavor_text('narwhal'/'HML', '\"Who needs a spear? Ya break off the horn, and ya stab the fish with it. Easy!\"\n—Kakra, Sea Troll').
card_multiverse_id('narwhal'/'HML', '2953').

card_in_set('orcish mine', 'HML').
card_original_type('orcish mine'/'HML', 'Enchant Land').
card_original_text('orcish mine'/'HML', 'When Orcish Mine comes into play, put three ore counters on it. During your upkeep and whenever target land becomes tapped, remove an ore counter from Orcish Mine. When the last ore counter is removed from Orcish Mine, destroy the land Orcish Mine enchants; Orcish Mine deals 2 damage to that land\'s controller.').
card_first_print('orcish mine', 'HML').
card_image_name('orcish mine'/'HML', 'orcish mine').
card_uid('orcish mine'/'HML', 'HML:Orcish Mine:orcish mine').
card_rarity('orcish mine'/'HML', 'Uncommon').
card_artist('orcish mine'/'HML', 'Kaja Foglio').
card_multiverse_id('orcish mine'/'HML', '3007').

card_in_set('primal order', 'HML').
card_original_type('primal order'/'HML', 'Enchantment').
card_original_text('primal order'/'HML', 'During each player\'s upkeep, Primal Order deals 1 damage to that player for each non-basic land he or she controls.').
card_first_print('primal order', 'HML').
card_image_name('primal order'/'HML', 'primal order').
card_uid('primal order'/'HML', 'HML:Primal Order:primal order').
card_rarity('primal order'/'HML', 'Rare').
card_artist('primal order'/'HML', 'Rob Alexander').
card_flavor_text('primal order'/'HML', '\"It\'s strange, but I can feel it when the land is corrupted. I understand why the Autumn Willow is so protective.\"\n—Daria').
card_multiverse_id('primal order'/'HML', '2974').

card_in_set('prophecy', 'HML').
card_original_type('prophecy'/'HML', 'Sorcery').
card_original_text('prophecy'/'HML', 'Reveal the top card of target opponent\'s library to all players. If it is a land, gain 1 life. That opponent then shuffles his or her library. Draw a card at the beginning of the next turn\'s upkeep.').
card_first_print('prophecy', 'HML').
card_image_name('prophecy'/'HML', 'prophecy').
card_uid('prophecy'/'HML', 'HML:Prophecy:prophecy').
card_rarity('prophecy'/'HML', 'Common').
card_artist('prophecy'/'HML', 'Christopher Rush').
card_multiverse_id('prophecy'/'HML', '3023').

card_in_set('rashka the slayer', 'HML').
card_original_type('rashka the slayer'/'HML', 'Summon — Legend').
card_original_text('rashka the slayer'/'HML', 'Can block creatures with flying.\nIf assigned to block any black creatures, Rashka the Slayer gets +1/+2 until end of turn.').
card_first_print('rashka the slayer', 'HML').
card_image_name('rashka the slayer'/'HML', 'rashka the slayer').
card_uid('rashka the slayer'/'HML', 'HML:Rashka the Slayer:rashka the slayer').
card_rarity('rashka the slayer'/'HML', 'Uncommon').
card_artist('rashka the slayer'/'HML', 'Christopher Rush').
card_flavor_text('rashka the slayer'/'HML', '\"Rarely be the prey so haughty as to hunt the hunter.\"\n—Baron Sengir').
card_multiverse_id('rashka the slayer'/'HML', '3024').

card_in_set('reef pirates', 'HML').
card_original_type('reef pirates'/'HML', 'Summon — Ships').
card_original_text('reef pirates'/'HML', 'Whenever Reef Pirates damages any opponent, take the top card of his or her library and put it into his or her graveyard.').
card_first_print('reef pirates', 'HML').
card_image_name('reef pirates'/'HML', 'reef pirates1').
card_uid('reef pirates'/'HML', 'HML:Reef Pirates:reef pirates1').
card_rarity('reef pirates'/'HML', 'Common').
card_artist('reef pirates'/'HML', 'Tom Wänerstrand').
card_flavor_text('reef pirates'/'HML', '\"Corpses for crew don\'t sit well with me. Zeki and his dead ship had best keep their distance.\"\n—Joskun, An-Havva Constable').
card_multiverse_id('reef pirates'/'HML', '2955').

card_in_set('reef pirates', 'HML').
card_original_type('reef pirates'/'HML', 'Summon — Ships').
card_original_text('reef pirates'/'HML', 'Whenever Reef Pirates damages any opponent, take the top card of his or her library and put it into his or her graveyard.').
card_image_name('reef pirates'/'HML', 'reef pirates2').
card_uid('reef pirates'/'HML', 'HML:Reef Pirates:reef pirates2').
card_rarity('reef pirates'/'HML', 'Common').
card_artist('reef pirates'/'HML', 'Tom Wänerstrand').
card_flavor_text('reef pirates'/'HML', '\"Zeki sails with a dead crew not out of villainy, but pragmatism.\"\n—Baron Sengir').
card_multiverse_id('reef pirates'/'HML', '2954').

card_in_set('renewal', 'HML').
card_original_type('renewal'/'HML', 'Sorcery').
card_original_text('renewal'/'HML', 'Sacrifice a land to search your library for a basic land and put it directly into play. This does not count towards your one land per turn limit. Reshuffle your library afterwards. Draw a card at the beginning of the next turn\'s upkeep.').
card_first_print('renewal', 'HML').
card_image_name('renewal'/'HML', 'renewal').
card_uid('renewal'/'HML', 'HML:Renewal:renewal').
card_rarity('renewal'/'HML', 'Common').
card_artist('renewal'/'HML', 'Kaja Foglio').
card_multiverse_id('renewal'/'HML', '2975').

card_in_set('retribution', 'HML').
card_original_type('retribution'/'HML', 'Sorcery').
card_original_text('retribution'/'HML', 'Choose two target creatures controlled by an opponent. Bury one of those creatures, and put a -1/-1 counter on the other. That opponent chooses which creature is buried.').
card_first_print('retribution', 'HML').
card_image_name('retribution'/'HML', 'retribution').
card_uid('retribution'/'HML', 'HML:Retribution:retribution').
card_rarity('retribution'/'HML', 'Uncommon').
card_artist('retribution'/'HML', 'Mark Tedin').
card_multiverse_id('retribution'/'HML', '3008').

card_in_set('reveka, wizard savant', 'HML').
card_original_type('reveka, wizard savant'/'HML', 'Summon — Legend').
card_original_text('reveka, wizard savant'/'HML', '{T}: Reveka deals 2 damage to target creature or player and does not untap during your next untap phase.').
card_first_print('reveka, wizard savant', 'HML').
card_image_name('reveka, wizard savant'/'HML', 'reveka, wizard savant').
card_uid('reveka, wizard savant'/'HML', 'HML:Reveka, Wizard Savant:reveka, wizard savant').
card_rarity('reveka, wizard savant'/'HML', 'Rare').
card_artist('reveka, wizard savant'/'HML', 'Susan Van Camp').
card_flavor_text('reveka, wizard savant'/'HML', '\"It\'s nice to see a sister Dwarf in a position of such power, but why\'d it have to be one of those seafaring muleheads?\"\n—Halina, Dwarven Trader').
card_multiverse_id('reveka, wizard savant'/'HML', '2956').

card_in_set('root spider', 'HML').
card_original_type('root spider'/'HML', 'Summon — Spider').
card_original_text('root spider'/'HML', 'If assigned as a blocker, Root Spider gains first strike and gets +1/+0 until end of turn.').
card_first_print('root spider', 'HML').
card_image_name('root spider'/'HML', 'root spider').
card_uid('root spider'/'HML', 'HML:Root Spider:root spider').
card_rarity('root spider'/'HML', 'Uncommon').
card_artist('root spider'/'HML', 'Mike Kimble').
card_flavor_text('root spider'/'HML', '\"They\'re not half as fierce if they don\'t see you coming.\"\n—Devin, Faerie Noble').
card_multiverse_id('root spider'/'HML', '2976').

card_in_set('roots', 'HML').
card_original_type('roots'/'HML', 'Enchant Creature').
card_original_text('roots'/'HML', 'Tap target creature without flying. That creature does not untap during its controller\'s untap phase.').
card_first_print('roots', 'HML').
card_image_name('roots'/'HML', 'roots').
card_uid('roots'/'HML', 'HML:Roots:roots').
card_rarity('roots'/'HML', 'Uncommon').
card_artist('roots'/'HML', 'Nicola Leonard').
card_flavor_text('roots'/'HML', '\"That which nourishes can also bind.\"\n—Gemma, Willow Priestess').
card_multiverse_id('roots'/'HML', '2977').

card_in_set('roterothopter', 'HML').
card_original_type('roterothopter'/'HML', 'Artifact Creature').
card_original_text('roterothopter'/'HML', 'Flying\n{2}: +1/+0 until end of turn. You cannot spend more than {4} in this way each turn.').
card_first_print('roterothopter', 'HML').
card_image_name('roterothopter'/'HML', 'roterothopter').
card_uid('roterothopter'/'HML', 'HML:Roterothopter:roterothopter').
card_rarity('roterothopter'/'HML', 'Common').
card_artist('roterothopter'/'HML', 'Amy Weber').
card_flavor_text('roterothopter'/'HML', '\"The Roterothopter is as insidious as it is ingenious. It is one of the few creations of our school that I take no pride in.\" —Baki, Wizard Attendant').
card_multiverse_id('roterothopter'/'HML', '2908').

card_in_set('rysorian badger', 'HML').
card_original_type('rysorian badger'/'HML', 'Summon — Badger').
card_original_text('rysorian badger'/'HML', 'If Rysorian Badger attacks and is not blocked, you may choose to have it deal no damage to defending player this turn. If you do so, remove from the game up to two target creatures from that player\'s graveyard. Gain 1 life for each creature removed in this way.').
card_first_print('rysorian badger', 'HML').
card_image_name('rysorian badger'/'HML', 'rysorian badger').
card_uid('rysorian badger'/'HML', 'HML:Rysorian Badger:rysorian badger').
card_rarity('rysorian badger'/'HML', 'Rare').
card_artist('rysorian badger'/'HML', 'Heather Hudson').
card_multiverse_id('rysorian badger'/'HML', '2978').

card_in_set('samite alchemist', 'HML').
card_original_type('samite alchemist'/'HML', 'Summon — Alchemist').
card_original_text('samite alchemist'/'HML', '{W}{W}, {T}: Prevent up to 4 damage to a creature you control. Tap that creature. The creature does not untap during your next untap phase.').
card_first_print('samite alchemist', 'HML').
card_image_name('samite alchemist'/'HML', 'samite alchemist1').
card_uid('samite alchemist'/'HML', 'HML:Samite Alchemist:samite alchemist1').
card_rarity('samite alchemist'/'HML', 'Common').
card_artist('samite alchemist'/'HML', 'Tom Wänerstrand').
card_flavor_text('samite alchemist'/'HML', '\"The wisest of Aysen dwell not in the Abbey, but in the laboratory.\"\n—Taysir').
card_multiverse_id('samite alchemist'/'HML', '3026').

card_in_set('samite alchemist', 'HML').
card_original_type('samite alchemist'/'HML', 'Summon — Alchemist').
card_original_text('samite alchemist'/'HML', '{W}{W}, {T}: Prevent up to 4 damage to a creature you control. Tap that creature. The creature does not untap during your next untap phase.').
card_image_name('samite alchemist'/'HML', 'samite alchemist2').
card_uid('samite alchemist'/'HML', 'HML:Samite Alchemist:samite alchemist2').
card_rarity('samite alchemist'/'HML', 'Common').
card_artist('samite alchemist'/'HML', 'Tom Wänerstrand').
card_flavor_text('samite alchemist'/'HML', '\"Powders and potions are nothing compared to faith.\"\n—Baris, Serra Inquisitor').
card_multiverse_id('samite alchemist'/'HML', '3025').

card_in_set('sea sprite', 'HML').
card_original_type('sea sprite'/'HML', 'Summon — Faerie').
card_original_text('sea sprite'/'HML', 'Flying, protection from red').
card_first_print('sea sprite', 'HML').
card_image_name('sea sprite'/'HML', 'sea sprite').
card_uid('sea sprite'/'HML', 'HML:Sea Sprite:sea sprite').
card_rarity('sea sprite'/'HML', 'Uncommon').
card_artist('sea sprite'/'HML', 'Susan Van Camp').
card_flavor_text('sea sprite'/'HML', '\"No one can catch what won\'t be caught.\"\n—Kakra, Sea Troll').
card_multiverse_id('sea sprite'/'HML', '2957').

card_in_set('sea troll', 'HML').
card_original_type('sea troll'/'HML', 'Summon — Troll').
card_original_text('sea troll'/'HML', '{U}: Regenerate. Use this ability only during a turn in which Sea Troll blocked a blue creature or a blue creature blocked Sea Troll.').
card_first_print('sea troll', 'HML').
card_image_name('sea troll'/'HML', 'sea troll').
card_uid('sea troll'/'HML', 'HML:Sea Troll:sea troll').
card_rarity('sea troll'/'HML', 'Uncommon').
card_artist('sea troll'/'HML', 'Daniel Gelon').
card_flavor_text('sea troll'/'HML', '\"I\'ve seen those Trolls devour a drowning sailor faster than his lungs could fill with water.\"\n—Zeki, Reef Pirate').
card_multiverse_id('sea troll'/'HML', '2958').

card_in_set('sengir autocrat', 'HML').
card_original_type('sengir autocrat'/'HML', 'Summon — Autocrat').
card_original_text('sengir autocrat'/'HML', 'When Sengir Autocrat comes into play, put three Serf tokens into play. Treat these tokens as 0/1 black creatures. If Sengir Autocrat leaves play, bury all Serf tokens.').
card_first_print('sengir autocrat', 'HML').
card_image_name('sengir autocrat'/'HML', 'sengir autocrat').
card_uid('sengir autocrat'/'HML', 'HML:Sengir Autocrat:sengir autocrat').
card_rarity('sengir autocrat'/'HML', 'Uncommon').
card_artist('sengir autocrat'/'HML', 'David A. Cherry').
card_flavor_text('sengir autocrat'/'HML', '\"A thankless job, and a death sentence besides.\"\n—Eron the Relentless').
card_multiverse_id('sengir autocrat'/'HML', '2928').

card_in_set('sengir bats', 'HML').
card_original_type('sengir bats'/'HML', 'Summon — Bats').
card_original_text('sengir bats'/'HML', 'Flying\nWhenever a creature is put into the graveyard the same turn Sengir Bats damaged it, put a +1/+1 counter on Sengir Bats.').
card_first_print('sengir bats', 'HML').
card_image_name('sengir bats'/'HML', 'sengir bats1').
card_uid('sengir bats'/'HML', 'HML:Sengir Bats:sengir bats1').
card_rarity('sengir bats'/'HML', 'Common').
card_artist('sengir bats'/'HML', 'Dan Frazier').
card_flavor_text('sengir bats'/'HML', '\"They are the eyes of the Baron, and they know your fears.\"\n—Ihsan\'s Shade').
card_multiverse_id('sengir bats'/'HML', '2930').

card_in_set('sengir bats', 'HML').
card_original_type('sengir bats'/'HML', 'Summon — Bats').
card_original_text('sengir bats'/'HML', 'Flying\nWhenever a creature is put into the graveyard the same turn Sengir Bats damaged it, put a +1/+1 counter on Sengir Bats.').
card_image_name('sengir bats'/'HML', 'sengir bats2').
card_uid('sengir bats'/'HML', 'HML:Sengir Bats:sengir bats2').
card_rarity('sengir bats'/'HML', 'Common').
card_artist('sengir bats'/'HML', 'Dan Frazier').
card_flavor_text('sengir bats'/'HML', '\"In the face of every one I see the Baron.\"\n—Daria').
card_multiverse_id('sengir bats'/'HML', '2929').

card_in_set('serra aviary', 'HML').
card_original_type('serra aviary'/'HML', 'Enchant World').
card_original_text('serra aviary'/'HML', 'All creatures with flying get +1/+1.').
card_first_print('serra aviary', 'HML').
card_image_name('serra aviary'/'HML', 'serra aviary').
card_uid('serra aviary'/'HML', 'HML:Serra Aviary:serra aviary').
card_rarity('serra aviary'/'HML', 'Rare').
card_artist('serra aviary'/'HML', 'Nicola Leonard').
card_flavor_text('serra aviary'/'HML', '\"Serra, like Feroz, is long since dead. But remember, Daria: her spirit shall survive so long as the Homelands do.\"\n—Taysir').
card_multiverse_id('serra aviary'/'HML', '3027').

card_in_set('serra bestiary', 'HML').
card_original_type('serra bestiary'/'HML', 'Enchant Creature').
card_original_text('serra bestiary'/'HML', 'During your upkeep, pay {W}{W} or bury Serra Bestiary.\nTarget creature cannot attack, block, or use any ability that includes {T} in the activation cost.').
card_first_print('serra bestiary', 'HML').
card_image_name('serra bestiary'/'HML', 'serra bestiary').
card_uid('serra bestiary'/'HML', 'HML:Serra Bestiary:serra bestiary').
card_rarity('serra bestiary'/'HML', 'Common').
card_artist('serra bestiary'/'HML', 'Anson Maddocks').
card_multiverse_id('serra bestiary'/'HML', '3028').

card_in_set('serra inquisitors', 'HML').
card_original_type('serra inquisitors'/'HML', 'Summon — Inquisitors').
card_original_text('serra inquisitors'/'HML', 'If assigned to block any black creatures or any black creatures are assigned to block it, Serra Inquisitors gets +2/+0 until end of turn.').
card_first_print('serra inquisitors', 'HML').
card_image_name('serra inquisitors'/'HML', 'serra inquisitors').
card_uid('serra inquisitors'/'HML', 'HML:Serra Inquisitors:serra inquisitors').
card_rarity('serra inquisitors'/'HML', 'Uncommon').
card_artist('serra inquisitors'/'HML', 'Dennis Detwiller').
card_multiverse_id('serra inquisitors'/'HML', '3029').

card_in_set('serra paladin', 'HML').
card_original_type('serra paladin'/'HML', 'Summon — Paladin').
card_original_text('serra paladin'/'HML', '{T}: Prevent 1 damage to any creature or player.\n{1}{W}{W}, {T}: Attacking does not cause target creature to tap this turn.').
card_first_print('serra paladin', 'HML').
card_image_name('serra paladin'/'HML', 'serra paladin').
card_uid('serra paladin'/'HML', 'HML:Serra Paladin:serra paladin').
card_rarity('serra paladin'/'HML', 'Common').
card_artist('serra paladin'/'HML', 'Pete Venters').
card_flavor_text('serra paladin'/'HML', '\"Would that I could be a Paladin again, but the dead hold no place among their ranks.\"\n—Ihsan\'s Shade').
card_multiverse_id('serra paladin'/'HML', '3030').

card_in_set('serrated arrows', 'HML').
card_original_type('serrated arrows'/'HML', 'Artifact').
card_original_text('serrated arrows'/'HML', 'When Serrated Arrows comes into play, put three arrowhead counters on it.\nDuring your upkeep, bury Serrated Arrows if there are no arrowhead counters on it.\n{T}: Remove an arrowhead counter from Serrated Arrows to put a -1/-1 counter on target creature.').
card_first_print('serrated arrows', 'HML').
card_image_name('serrated arrows'/'HML', 'serrated arrows').
card_uid('serrated arrows'/'HML', 'HML:Serrated Arrows:serrated arrows').
card_rarity('serrated arrows'/'HML', 'Common').
card_artist('serrated arrows'/'HML', 'David A. Cherry').
card_multiverse_id('serrated arrows'/'HML', '2909').

card_in_set('shrink', 'HML').
card_original_type('shrink'/'HML', 'Instant').
card_original_text('shrink'/'HML', 'Target creature gets -5/-0 until end of turn.').
card_first_print('shrink', 'HML').
card_image_name('shrink'/'HML', 'shrink1').
card_uid('shrink'/'HML', 'HML:Shrink:shrink1').
card_rarity('shrink'/'HML', 'Common').
card_artist('shrink'/'HML', 'Liz Danforth').
card_flavor_text('shrink'/'HML', '\"The smallest beast can yet have the greatest heart.\"\n—Gemma, Willow Priestess').
card_multiverse_id('shrink'/'HML', '2979').

card_in_set('shrink', 'HML').
card_original_type('shrink'/'HML', 'Instant').
card_original_text('shrink'/'HML', 'Target creature gets -5/-0 until end of turn.').
card_image_name('shrink'/'HML', 'shrink2').
card_uid('shrink'/'HML', 'HML:Shrink:shrink2').
card_rarity('shrink'/'HML', 'Common').
card_artist('shrink'/'HML', 'Liz Danforth').
card_flavor_text('shrink'/'HML', '\"Do not fear adversity. Let your courage be your strength.\"\n—Onatah, Anaba Shaman').
card_multiverse_id('shrink'/'HML', '2980').

card_in_set('soraya the falconer', 'HML').
card_original_type('soraya the falconer'/'HML', 'Summon — Legend').
card_original_text('soraya the falconer'/'HML', 'All Falcons get +1/+1.\n{1}{W} Target Falcon gains banding until end of turn.').
card_first_print('soraya the falconer', 'HML').
card_image_name('soraya the falconer'/'HML', 'soraya the falconer').
card_uid('soraya the falconer'/'HML', 'HML:Soraya the Falconer:soraya the falconer').
card_rarity('soraya the falconer'/'HML', 'Rare').
card_artist('soraya the falconer'/'HML', 'Dennis Detwiller').
card_flavor_text('soraya the falconer'/'HML', '\"Soraya speaks with the hunters of the air, as do all of her family line.\"\n—Autumn Willow').
card_multiverse_id('soraya the falconer'/'HML', '3031').

card_in_set('spectral bears', 'HML').
card_original_type('spectral bears'/'HML', 'Summon — Bears').
card_original_text('spectral bears'/'HML', 'If Spectral Bears is declared as an attacker and defending player controls no black cards, it does not untap during your next untap phase.').
card_first_print('spectral bears', 'HML').
card_image_name('spectral bears'/'HML', 'spectral bears').
card_uid('spectral bears'/'HML', 'HML:Spectral Bears:spectral bears').
card_rarity('spectral bears'/'HML', 'Uncommon').
card_artist('spectral bears'/'HML', 'Pat Morrissey').
card_flavor_text('spectral bears'/'HML', '\"I hear there are Bears—or spirits—that guard caravans passing through the forest.\" —Gulsen, Abbey Matron').
card_multiverse_id('spectral bears'/'HML', '2981').

card_in_set('timmerian fiends', 'HML').
card_original_type('timmerian fiends'/'HML', 'Summon — Fiends').
card_original_text('timmerian fiends'/'HML', 'Remove Timmerian Fiends from your deck before playing if not playing for ante.\n{B}{B}{B} Sacrifice Timmerian Fiends to bury target artifact that any opponent owns in your graveyard. Put Timmerian Fiends into that opponent\'s graveyard. This change in ownership is permanent. The opponent may ante an additional card to counter this effect.').
card_first_print('timmerian fiends', 'HML').
card_image_name('timmerian fiends'/'HML', 'timmerian fiends').
card_uid('timmerian fiends'/'HML', 'HML:Timmerian Fiends:timmerian fiends').
card_rarity('timmerian fiends'/'HML', 'Rare').
card_artist('timmerian fiends'/'HML', 'Mike Kimble').
card_multiverse_id('timmerian fiends'/'HML', '2931').

card_in_set('torture', 'HML').
card_original_type('torture'/'HML', 'Enchant Creature').
card_original_text('torture'/'HML', 'Choose target creature.\n{1}{B} Put a -1/-1 counter on creature Torture enchants.').
card_first_print('torture', 'HML').
card_image_name('torture'/'HML', 'torture1').
card_uid('torture'/'HML', 'HML:Torture:torture1').
card_rarity('torture'/'HML', 'Common').
card_artist('torture'/'HML', 'Mark Tedin').
card_flavor_text('torture'/'HML', '\"Oh, they scream, right enough, and plead, too—but the Baron keeps no mercy in his vault of horrors.\"\n—Zeki, Reef Pirate').
card_multiverse_id('torture'/'HML', '2932').

card_in_set('torture', 'HML').
card_original_type('torture'/'HML', 'Enchant Creature').
card_original_text('torture'/'HML', 'Choose target creature.\n{1}{B} Put a -1/-1 counter on creature Torture enchants.').
card_image_name('torture'/'HML', 'torture2').
card_uid('torture'/'HML', 'HML:Torture:torture2').
card_rarity('torture'/'HML', 'Common').
card_artist('torture'/'HML', 'Mark Tedin').
card_flavor_text('torture'/'HML', '\"It helps you pass the time until you die.\"\n—Grandmother Sengir').
card_multiverse_id('torture'/'HML', '2933').

card_in_set('trade caravan', 'HML').
card_original_type('trade caravan'/'HML', 'Summon — Caravan').
card_original_text('trade caravan'/'HML', 'During your upkeep, put a currency counter on Trade Caravan.\n{0}: Remove two currency counters from Trade Caravan to untap target basic land. Use this ability only during any opponent\'s upkeep.').
card_first_print('trade caravan', 'HML').
card_image_name('trade caravan'/'HML', 'trade caravan1').
card_uid('trade caravan'/'HML', 'HML:Trade Caravan:trade caravan1').
card_rarity('trade caravan'/'HML', 'Common').
card_artist('trade caravan'/'HML', 'Kaja Foglio').
card_multiverse_id('trade caravan'/'HML', '3032').

card_in_set('trade caravan', 'HML').
card_original_type('trade caravan'/'HML', 'Summon — Caravan').
card_original_text('trade caravan'/'HML', 'During your upkeep, put a currency counter on Trade Caravan.\n{0}: Remove two currency counters from Trade Caravan to untap target basic land. Use this ability only during any opponent\'s upkeep.').
card_image_name('trade caravan'/'HML', 'trade caravan2').
card_uid('trade caravan'/'HML', 'HML:Trade Caravan:trade caravan2').
card_rarity('trade caravan'/'HML', 'Common').
card_artist('trade caravan'/'HML', 'Kaja Foglio').
card_multiverse_id('trade caravan'/'HML', '3033').

card_in_set('truce', 'HML').
card_original_type('truce'/'HML', 'Instant').
card_original_text('truce'/'HML', 'Each player may draw up to two cards. For each card less than two any player draws, that player gains 2 life.').
card_first_print('truce', 'HML').
card_image_name('truce'/'HML', 'truce').
card_uid('truce'/'HML', 'HML:Truce:truce').
card_rarity('truce'/'HML', 'Rare').
card_artist('truce'/'HML', 'Melissa A. Benson').
card_flavor_text('truce'/'HML', '\"The people of Aysen feed us, and we don\'t kill them. That\'s my kind of truce.\"\n—Eron the Relentless').
card_multiverse_id('truce'/'HML', '3034').

card_in_set('veldrane of sengir', 'HML').
card_original_type('veldrane of sengir'/'HML', 'Summon — Legend').
card_original_text('veldrane of sengir'/'HML', '{1}{B}{B} Forestwalk and -3/-0 until end of turn').
card_first_print('veldrane of sengir', 'HML').
card_image_name('veldrane of sengir'/'HML', 'veldrane of sengir').
card_uid('veldrane of sengir'/'HML', 'HML:Veldrane of Sengir:veldrane of sengir').
card_rarity('veldrane of sengir'/'HML', 'Rare').
card_artist('veldrane of sengir'/'HML', 'Susan Van Camp').
card_flavor_text('veldrane of sengir'/'HML', '\"Poor, foolish Veldrane. He goes wherever the Baron wills him. One day, he\'ll go to die.\"\n—Halina, Dwarven Trader').
card_multiverse_id('veldrane of sengir'/'HML', '2934').

card_in_set('wall of kelp', 'HML').
card_original_type('wall of kelp'/'HML', 'Summon — Wall').
card_original_text('wall of kelp'/'HML', '{U}{U}, {T}: Put a Kelp token into play. Treat this token as a 0/1 blue wall.').
card_first_print('wall of kelp', 'HML').
card_image_name('wall of kelp'/'HML', 'wall of kelp').
card_uid('wall of kelp'/'HML', 'HML:Wall of Kelp:wall of kelp').
card_rarity('wall of kelp'/'HML', 'Rare').
card_artist('wall of kelp'/'HML', 'Alan Rabinowitz').
card_flavor_text('wall of kelp'/'HML', '\"Ya can eat it or ya can weave it, but ya can\'t fight in it.\"\n—Zeki, Reef Pirate').
card_multiverse_id('wall of kelp'/'HML', '2959').

card_in_set('willow faerie', 'HML').
card_original_type('willow faerie'/'HML', 'Summon — Faerie').
card_original_text('willow faerie'/'HML', 'Flying').
card_first_print('willow faerie', 'HML').
card_image_name('willow faerie'/'HML', 'willow faerie1').
card_uid('willow faerie'/'HML', 'HML:Willow Faerie:willow faerie1').
card_rarity('willow faerie'/'HML', 'Common').
card_artist('willow faerie'/'HML', 'Susan Van Camp').
card_flavor_text('willow faerie'/'HML', '\"The Autumn Willow cannot be everywhere at once—that\'s what her grandchildren are for.\"\n—Gulsen, Abbey Matron').
card_multiverse_id('willow faerie'/'HML', '2982').

card_in_set('willow faerie', 'HML').
card_original_type('willow faerie'/'HML', 'Summon — Faerie').
card_original_text('willow faerie'/'HML', 'Flying').
card_image_name('willow faerie'/'HML', 'willow faerie2').
card_uid('willow faerie'/'HML', 'HML:Willow Faerie:willow faerie2').
card_rarity('willow faerie'/'HML', 'Common').
card_artist('willow faerie'/'HML', 'Susan Van Camp').
card_flavor_text('willow faerie'/'HML', '\"Swift as the wind and harder to catch— that\'s the Faeries for you.\"\n—Daria').
card_multiverse_id('willow faerie'/'HML', '2983').

card_in_set('willow priestess', 'HML').
card_original_type('willow priestess'/'HML', 'Summon — Faerie').
card_original_text('willow priestess'/'HML', '{T}: Take a Faerie from your hand and put it directly into play as though it were just summoned.\n{2}{G} Target green creature gains protection from black until end of turn.').
card_first_print('willow priestess', 'HML').
card_image_name('willow priestess'/'HML', 'willow priestess').
card_uid('willow priestess'/'HML', 'HML:Willow Priestess:willow priestess').
card_rarity('willow priestess'/'HML', 'Rare').
card_artist('willow priestess'/'HML', 'Susan Van Camp').
card_flavor_text('willow priestess'/'HML', '\"Those of faith are those of strength.\"\n—Autumn Willow').
card_multiverse_id('willow priestess'/'HML', '2984').

card_in_set('winter sky', 'HML').
card_original_type('winter sky'/'HML', 'Sorcery').
card_original_text('winter sky'/'HML', 'Flip a coin; target opponent calls heads or tails while coin is in the air. If the flip ends up in your favor, Winter Sky deals 1 damage to each creature and player. Otherwise, each player draws a card.').
card_first_print('winter sky', 'HML').
card_image_name('winter sky'/'HML', 'winter sky').
card_uid('winter sky'/'HML', 'HML:Winter Sky:winter sky').
card_rarity('winter sky'/'HML', 'Rare').
card_artist('winter sky'/'HML', 'Mike Kimble').
card_multiverse_id('winter sky'/'HML', '3009').

card_in_set('wizards\' school', 'HML').
card_original_type('wizards\' school'/'HML', 'Land').
card_original_text('wizards\' school'/'HML', '{T}: Add one colorless mana to your mana pool.\n{1}, {T}: Add {U} to your mana pool.\n{2}, {T}: Add {W} to your mana pool.\n{2}, {T}: Add {B} to your mana pool.').
card_first_print('wizards\' school', 'HML').
card_image_name('wizards\' school'/'HML', 'wizards\' school').
card_uid('wizards\' school'/'HML', 'HML:Wizards\' School:wizards\' school').
card_rarity('wizards\' school'/'HML', 'Uncommon').
card_artist('wizards\' school'/'HML', 'Pat Morrissey').
card_flavor_text('wizards\' school'/'HML', 'The legacy of Feroz lives on.').
card_multiverse_id('wizards\' school'/'HML', '3039').
