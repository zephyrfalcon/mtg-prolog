% From the Vault: Legends

set('V11').
set_name('V11', 'From the Vault: Legends').
set_release_date('V11', '2011-08-26').
set_border('V11', 'black').
set_type('V11', 'from the vault').

card_in_set('cao cao, lord of wei', 'V11').
card_original_type('cao cao, lord of wei'/'V11', 'Legendary Creature — Human Soldier').
card_original_text('cao cao, lord of wei'/'V11', '{T}: Target opponent discards two cards. Activate this ability only during your turn, before attackers are declared.').
card_image_name('cao cao, lord of wei'/'V11', 'cao cao, lord of wei').
card_uid('cao cao, lord of wei'/'V11', 'V11:Cao Cao, Lord of Wei:cao cao, lord of wei').
card_rarity('cao cao, lord of wei'/'V11', 'Mythic Rare').
card_artist('cao cao, lord of wei'/'V11', 'Gao Jianzhang').
card_number('cao cao, lord of wei'/'V11', '1').
card_multiverse_id('cao cao, lord of wei'/'V11', '244670').

card_in_set('captain sisay', 'V11').
card_original_type('captain sisay'/'V11', 'Legendary Creature — Human Soldier').
card_original_text('captain sisay'/'V11', '{T}: Search your library for a legendary card, reveal that card, and put it into your hand. Then shuffle your library.').
card_image_name('captain sisay'/'V11', 'captain sisay').
card_uid('captain sisay'/'V11', 'V11:Captain Sisay:captain sisay').
card_rarity('captain sisay'/'V11', 'Mythic Rare').
card_artist('captain sisay'/'V11', 'Ray Lago').
card_number('captain sisay'/'V11', '2').
card_flavor_text('captain sisay'/'V11', 'Her leadership forged the Weatherlight\'s finest crew.').
card_multiverse_id('captain sisay'/'V11', '244665').

card_in_set('doran, the siege tower', 'V11').
card_original_type('doran, the siege tower'/'V11', 'Legendary Creature — Treefolk Shaman').
card_original_text('doran, the siege tower'/'V11', 'Each creature assigns combat damage equal to its toughness rather than its power.').
card_image_name('doran, the siege tower'/'V11', 'doran, the siege tower').
card_uid('doran, the siege tower'/'V11', 'V11:Doran, the Siege Tower:doran, the siege tower').
card_rarity('doran, the siege tower'/'V11', 'Mythic Rare').
card_artist('doran, the siege tower'/'V11', 'Mark Zug').
card_number('doran, the siege tower'/'V11', '3').
card_flavor_text('doran, the siege tower'/'V11', '\"Each year that passes rings you inwardly with memory and might. Wield your heart, and the world will tremble.\"').
card_multiverse_id('doran, the siege tower'/'V11', '244674').

card_in_set('kiki-jiki, mirror breaker', 'V11').
card_original_type('kiki-jiki, mirror breaker'/'V11', 'Legendary Creature — Goblin Shaman').
card_original_text('kiki-jiki, mirror breaker'/'V11', 'Haste\n{T}: Put a token that\'s a copy of target nonlegendary creature you control onto the battlefield. That token has haste. Sacrifice it at the beginning of the next end step.').
card_image_name('kiki-jiki, mirror breaker'/'V11', 'kiki-jiki, mirror breaker').
card_uid('kiki-jiki, mirror breaker'/'V11', 'V11:Kiki-Jiki, Mirror Breaker:kiki-jiki, mirror breaker').
card_rarity('kiki-jiki, mirror breaker'/'V11', 'Mythic Rare').
card_artist('kiki-jiki, mirror breaker'/'V11', 'Steven Belledin').
card_number('kiki-jiki, mirror breaker'/'V11', '4').
card_multiverse_id('kiki-jiki, mirror breaker'/'V11', '244666').

card_in_set('kresh the bloodbraided', 'V11').
card_original_type('kresh the bloodbraided'/'V11', 'Legendary Creature — Human Warrior').
card_original_text('kresh the bloodbraided'/'V11', 'Whenever another creature dies, you may put X +1/+1 counters on Kresh the Bloodbraided, where X is that creature\'s power.').
card_image_name('kresh the bloodbraided'/'V11', 'kresh the bloodbraided').
card_uid('kresh the bloodbraided'/'V11', 'V11:Kresh the Bloodbraided:kresh the bloodbraided').
card_rarity('kresh the bloodbraided'/'V11', 'Mythic Rare').
card_artist('kresh the bloodbraided'/'V11', 'Steve Argyle').
card_number('kresh the bloodbraided'/'V11', '5').
card_flavor_text('kresh the bloodbraided'/'V11', 'Each of his twenty-two braids is bound with bone and leather from a foe.').
card_multiverse_id('kresh the bloodbraided'/'V11', '244672').

card_in_set('mikaeus, the lunarch', 'V11').
card_original_type('mikaeus, the lunarch'/'V11', 'Legendary Creature — Human Cleric').
card_original_text('mikaeus, the lunarch'/'V11', 'Mikaeus, the Lunarch enters the battlefield with X +1/+1 counters on it.\n{T}: Put a +1/+1 counter on Mikaeus.\n{T}, Remove a +1/+1 counter from Mikaeus: Put a +1/+1 counter on each other creature you control.').
card_first_print('mikaeus, the lunarch', 'V11').
card_image_name('mikaeus, the lunarch'/'V11', 'mikaeus, the lunarch').
card_uid('mikaeus, the lunarch'/'V11', 'V11:Mikaeus, the Lunarch:mikaeus, the lunarch').
card_rarity('mikaeus, the lunarch'/'V11', 'Mythic Rare').
card_artist('mikaeus, the lunarch'/'V11', 'Steven Belledin').
card_number('mikaeus, the lunarch'/'V11', '6').
card_multiverse_id('mikaeus, the lunarch'/'V11', '259296').

card_in_set('omnath, locus of mana', 'V11').
card_original_type('omnath, locus of mana'/'V11', 'Legendary Creature — Elemental').
card_original_text('omnath, locus of mana'/'V11', 'Green mana doesn\'t empty from your mana pool as steps and phases end.\nOmnath, Locus of Mana gets +1/+1 for each green mana in your mana pool.').
card_image_name('omnath, locus of mana'/'V11', 'omnath, locus of mana').
card_uid('omnath, locus of mana'/'V11', 'V11:Omnath, Locus of Mana:omnath, locus of mana').
card_rarity('omnath, locus of mana'/'V11', 'Mythic Rare').
card_artist('omnath, locus of mana'/'V11', 'Mike Bierek').
card_number('omnath, locus of mana'/'V11', '7').
card_flavor_text('omnath, locus of mana'/'V11', 'It gathers against the coming storm.').
card_multiverse_id('omnath, locus of mana'/'V11', '259295').

card_in_set('oona, queen of the fae', 'V11').
card_original_type('oona, queen of the fae'/'V11', 'Legendary Creature — Faerie Wizard').
card_original_text('oona, queen of the fae'/'V11', 'Flying\n{X}{U/B}: Choose a color. Target opponent exiles the top X cards of his or her library. For each card of the chosen color exiled this way, put a 1/1 blue and black Faerie Rogue creature token with flying onto the battlefield.').
card_image_name('oona, queen of the fae'/'V11', 'oona, queen of the fae').
card_uid('oona, queen of the fae'/'V11', 'V11:Oona, Queen of the Fae:oona, queen of the fae').
card_rarity('oona, queen of the fae'/'V11', 'Mythic Rare').
card_artist('oona, queen of the fae'/'V11', 'Adam Rex').
card_number('oona, queen of the fae'/'V11', '8').
card_multiverse_id('oona, queen of the fae'/'V11', '244668').

card_in_set('progenitus', 'V11').
card_original_type('progenitus'/'V11', 'Legendary Creature — Hydra Avatar').
card_original_text('progenitus'/'V11', 'Protection from everything\nIf Progenitus would be put into a graveyard from anywhere, reveal Progenitus and shuffle it into its owner\'s library instead.').
card_image_name('progenitus'/'V11', 'progenitus').
card_uid('progenitus'/'V11', 'V11:Progenitus:progenitus').
card_rarity('progenitus'/'V11', 'Mythic Rare').
card_artist('progenitus'/'V11', 'Mike Bierek').
card_number('progenitus'/'V11', '9').
card_flavor_text('progenitus'/'V11', 'The Soul of the World has returned.').
card_multiverse_id('progenitus'/'V11', '244675').

card_in_set('rafiq of the many', 'V11').
card_original_type('rafiq of the many'/'V11', 'Legendary Creature — Human Knight').
card_original_text('rafiq of the many'/'V11', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\nWhenever a creature you control attacks alone, it gains double strike until end of turn.').
card_image_name('rafiq of the many'/'V11', 'rafiq of the many').
card_uid('rafiq of the many'/'V11', 'V11:Rafiq of the Many:rafiq of the many').
card_rarity('rafiq of the many'/'V11', 'Mythic Rare').
card_artist('rafiq of the many'/'V11', 'Wayne Reynolds').
card_number('rafiq of the many'/'V11', '10').
card_flavor_text('rafiq of the many'/'V11', 'Many sigils, one purpose.').
card_multiverse_id('rafiq of the many'/'V11', '244669').

card_in_set('sharuum the hegemon', 'V11').
card_original_type('sharuum the hegemon'/'V11', 'Legendary Artifact Creature — Sphinx').
card_original_text('sharuum the hegemon'/'V11', 'Flying\nWhen Sharuum the Hegemon enters the battlefield, you may return target artifact card from your graveyard to the battlefield.').
card_image_name('sharuum the hegemon'/'V11', 'sharuum the hegemon').
card_uid('sharuum the hegemon'/'V11', 'V11:Sharuum the Hegemon:sharuum the hegemon').
card_rarity('sharuum the hegemon'/'V11', 'Mythic Rare').
card_artist('sharuum the hegemon'/'V11', 'Todd Lockwood').
card_number('sharuum the hegemon'/'V11', '11').
card_flavor_text('sharuum the hegemon'/'V11', 'To gain audience with the hegemon, one must bring a riddle she has not heard.').
card_multiverse_id('sharuum the hegemon'/'V11', '244664').

card_in_set('sun quan, lord of wu', 'V11').
card_original_type('sun quan, lord of wu'/'V11', 'Legendary Creature — Human Soldier').
card_original_text('sun quan, lord of wu'/'V11', 'Creatures you control have horsemanship. (They can\'t be blocked except by creatures with horsemanship.)').
card_image_name('sun quan, lord of wu'/'V11', 'sun quan, lord of wu').
card_uid('sun quan, lord of wu'/'V11', 'V11:Sun Quan, Lord of Wu:sun quan, lord of wu').
card_rarity('sun quan, lord of wu'/'V11', 'Mythic Rare').
card_artist('sun quan, lord of wu'/'V11', 'Xu Xiaoming').
card_number('sun quan, lord of wu'/'V11', '12').
card_flavor_text('sun quan, lord of wu'/'V11', '\"One score and four he reigned, the Southland king: / A dragon coiled, a tiger poised below the mighty Yangtze.\"').
card_multiverse_id('sun quan, lord of wu'/'V11', '244663').

card_in_set('teferi, mage of zhalfir', 'V11').
card_original_type('teferi, mage of zhalfir'/'V11', 'Legendary Creature — Human Wizard').
card_original_text('teferi, mage of zhalfir'/'V11', 'Flash\nCreature cards you own that aren\'t on the battlefield have flash.\nEach opponent can cast spells only any time he or she could cast a sorcery.').
card_image_name('teferi, mage of zhalfir'/'V11', 'teferi, mage of zhalfir').
card_uid('teferi, mage of zhalfir'/'V11', 'V11:Teferi, Mage of Zhalfir:teferi, mage of zhalfir').
card_rarity('teferi, mage of zhalfir'/'V11', 'Mythic Rare').
card_artist('teferi, mage of zhalfir'/'V11', 'Volkan Baga').
card_number('teferi, mage of zhalfir'/'V11', '13').
card_flavor_text('teferi, mage of zhalfir'/'V11', 'To save this plane, he must forsake all others.').
card_multiverse_id('teferi, mage of zhalfir'/'V11', '244667').

card_in_set('ulamog, the infinite gyre', 'V11').
card_original_type('ulamog, the infinite gyre'/'V11', 'Legendary Creature — Eldrazi').
card_original_text('ulamog, the infinite gyre'/'V11', 'When you cast Ulamog, the Infinite Gyre, destroy target permanent.\nAnnihilator 4 (Whenever this creature attacks, defending player sacrifices four permanents.)\nUlamog is indestructible.\nWhen Ulamog is put into a graveyard from anywhere, its owner shuffles his or her graveyard into his or her library.').
card_image_name('ulamog, the infinite gyre'/'V11', 'ulamog, the infinite gyre').
card_uid('ulamog, the infinite gyre'/'V11', 'V11:Ulamog, the Infinite Gyre:ulamog, the infinite gyre').
card_rarity('ulamog, the infinite gyre'/'V11', 'Mythic Rare').
card_artist('ulamog, the infinite gyre'/'V11', 'Aleksi Briclot').
card_number('ulamog, the infinite gyre'/'V11', '14').
card_multiverse_id('ulamog, the infinite gyre'/'V11', '261321').

card_in_set('visara the dreadful', 'V11').
card_original_type('visara the dreadful'/'V11', 'Legendary Creature — Gorgon').
card_original_text('visara the dreadful'/'V11', 'Flying\n{T}: Destroy target creature. It can\'t be regenerated.').
card_image_name('visara the dreadful'/'V11', 'visara the dreadful').
card_uid('visara the dreadful'/'V11', 'V11:Visara the Dreadful:visara the dreadful').
card_rarity('visara the dreadful'/'V11', 'Mythic Rare').
card_artist('visara the dreadful'/'V11', 'Brad Rigney').
card_number('visara the dreadful'/'V11', '15').
card_flavor_text('visara the dreadful'/'V11', '\"My eyes are my strongest feature.\"').
card_multiverse_id('visara the dreadful'/'V11', '244677').
