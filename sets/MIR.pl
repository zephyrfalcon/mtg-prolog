% Mirage

set('MIR').
set_name('MIR', 'Mirage').
set_release_date('MIR', '1996-10-08').
set_border('MIR', 'black').
set_type('MIR', 'expansion').
set_block('MIR', 'Mirage').

card_in_set('abyssal hunter', 'MIR').
card_original_type('abyssal hunter'/'MIR', 'Summon — Hunter').
card_original_text('abyssal hunter'/'MIR', '{B}, {T}: Tap target creature. Abyssal Hunter deals to that creature an amount of damage equal to Abyssal Hunter\'s power.').
card_first_print('abyssal hunter', 'MIR').
card_image_name('abyssal hunter'/'MIR', 'abyssal hunter').
card_uid('abyssal hunter'/'MIR', 'MIR:Abyssal Hunter:abyssal hunter').
card_rarity('abyssal hunter'/'MIR', 'Rare').
card_artist('abyssal hunter'/'MIR', 'Steve Luke').
card_flavor_text('abyssal hunter'/'MIR', 'Some smiles show cheer; some merely show teeth.').
card_multiverse_id('abyssal hunter'/'MIR', '3272').

card_in_set('acidic dagger', 'MIR').
card_original_type('acidic dagger'/'MIR', 'Artifact').
card_original_text('acidic dagger'/'MIR', '{4}, {T}: Destroy any non-Wall creature receiving combat damage from target creature this turn. If targeted creature leaves play, bury Acidic Dagger. Use this ability only before defense is chosen.').
card_first_print('acidic dagger', 'MIR').
card_image_name('acidic dagger'/'MIR', 'acidic dagger').
card_uid('acidic dagger'/'MIR', 'MIR:Acidic Dagger:acidic dagger').
card_rarity('acidic dagger'/'MIR', 'Rare').
card_artist('acidic dagger'/'MIR', 'Stuart Beel').
card_multiverse_id('acidic dagger'/'MIR', '3239').

card_in_set('afiya grove', 'MIR').
card_original_type('afiya grove'/'MIR', 'Enchantment').
card_original_text('afiya grove'/'MIR', 'Afiya Grove comes into play with three +1/+1 counters on it.\nDuring your upkeep, put one of these counters on target creature.\nIf Afiya Grove has none of these counters on it, bury it.').
card_first_print('afiya grove', 'MIR').
card_image_name('afiya grove'/'MIR', 'afiya grove').
card_uid('afiya grove'/'MIR', 'MIR:Afiya Grove:afiya grove').
card_rarity('afiya grove'/'MIR', 'Rare').
card_artist('afiya grove'/'MIR', 'Stuart Griffin').
card_multiverse_id('afiya grove'/'MIR', '3374').

card_in_set('afterlife', 'MIR').
card_original_type('afterlife'/'MIR', 'Instant').
card_original_text('afterlife'/'MIR', 'Bury target creature and put an Essence token into play under the control of that creature\'s controller. Treat this token as a 1/1 white creature with flying.').
card_first_print('afterlife', 'MIR').
card_image_name('afterlife'/'MIR', 'afterlife').
card_uid('afterlife'/'MIR', 'MIR:Afterlife:afterlife').
card_rarity('afterlife'/'MIR', 'Uncommon').
card_artist('afterlife'/'MIR', 'Pete Venters').
card_flavor_text('afterlife'/'MIR', 'Facets of life, reflections of the soul.').
card_multiverse_id('afterlife'/'MIR', '3476').

card_in_set('agility', 'MIR').
card_original_type('agility'/'MIR', 'Enchant Creature').
card_original_text('agility'/'MIR', 'Enchanted creature gets +1/+1 and gains flanking (whenever a creature without flanking is assigned to block this creature, the blocking creature gets -1/-1 until end of turn).').
card_first_print('agility', 'MIR').
card_image_name('agility'/'MIR', 'agility').
card_uid('agility'/'MIR', 'MIR:Agility:agility').
card_rarity('agility'/'MIR', 'Common').
card_artist('agility'/'MIR', 'Drew Tucker').
card_multiverse_id('agility'/'MIR', '3425').

card_in_set('alarum', 'MIR').
card_original_type('alarum'/'MIR', 'Instant').
card_original_text('alarum'/'MIR', 'Untap target nonattacking creature. That creature gets +1/+3 until end of turn.').
card_first_print('alarum', 'MIR').
card_image_name('alarum'/'MIR', 'alarum').
card_uid('alarum'/'MIR', 'MIR:Alarum:alarum').
card_rarity('alarum'/'MIR', 'Common').
card_artist('alarum'/'MIR', 'Andrew Robinson').
card_flavor_text('alarum'/'MIR', '\"One timely cry of warning can save nine of surprise.\"\n—Sidar Jabari').
card_multiverse_id('alarum'/'MIR', '3477').

card_in_set('aleatory', 'MIR').
card_original_type('aleatory'/'MIR', 'Instant').
card_original_text('aleatory'/'MIR', 'Play only after defense is chosen.\nFlip a coin; target opponent calls heads or tails while coin is in the air. If the flip ends up in your favor, target creature gets +1/+1 until end of turn.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_first_print('aleatory', 'MIR').
card_image_name('aleatory'/'MIR', 'aleatory').
card_uid('aleatory'/'MIR', 'MIR:Aleatory:aleatory').
card_rarity('aleatory'/'MIR', 'Uncommon').
card_artist('aleatory'/'MIR', 'Kev Walker').
card_multiverse_id('aleatory'/'MIR', '3426').

card_in_set('amber prison', 'MIR').
card_original_type('amber prison'/'MIR', 'Artifact').
card_original_text('amber prison'/'MIR', 'You may choose not to untap Amber Prison during your untap phase.\n{4}, {T}: Tap target artifact, creature, or land. As long as Amber Prison remains tapped, that permanent does not untap during its controller\'s untap phase.').
card_first_print('amber prison', 'MIR').
card_image_name('amber prison'/'MIR', 'amber prison').
card_uid('amber prison'/'MIR', 'MIR:Amber Prison:amber prison').
card_rarity('amber prison'/'MIR', 'Rare').
card_artist('amber prison'/'MIR', 'Donato Giancola').
card_multiverse_id('amber prison'/'MIR', '3240').

card_in_set('amulet of unmaking', 'MIR').
card_original_type('amulet of unmaking'/'MIR', 'Artifact').
card_original_text('amulet of unmaking'/'MIR', '{5}, {T}, Remove Amulet of Unmaking from the game: Remove target artifact, creature, or land from the game. Play this ability as a sorcery.').
card_first_print('amulet of unmaking', 'MIR').
card_image_name('amulet of unmaking'/'MIR', 'amulet of unmaking').
card_uid('amulet of unmaking'/'MIR', 'MIR:Amulet of Unmaking:amulet of unmaking').
card_rarity('amulet of unmaking'/'MIR', 'Rare').
card_artist('amulet of unmaking'/'MIR', 'Kaja Foglio').
card_flavor_text('amulet of unmaking'/'MIR', 'Trade to trade, not to keep.\n—Suq\'Ata motto').
card_multiverse_id('amulet of unmaking'/'MIR', '3241').

card_in_set('ancestral memories', 'MIR').
card_original_type('ancestral memories'/'MIR', 'Sorcery').
card_original_text('ancestral memories'/'MIR', 'Look at the top seven cards of your library. Put two of those cards into your hand and the rest into your graveyard.').
card_first_print('ancestral memories', 'MIR').
card_image_name('ancestral memories'/'MIR', 'ancestral memories').
card_uid('ancestral memories'/'MIR', 'MIR:Ancestral Memories:ancestral memories').
card_rarity('ancestral memories'/'MIR', 'Rare').
card_artist('ancestral memories'/'MIR', 'William Donohoe').
card_flavor_text('ancestral memories'/'MIR', '\"Even the ancient storytellers proclaimed Zhalfir to be dominant over all Jamuraa. Kaervek\'s invasion shocked us all.\"\n—Hakim, Loreweaver').
card_multiverse_id('ancestral memories'/'MIR', '3323').

card_in_set('armor of thorns', 'MIR').
card_original_type('armor of thorns'/'MIR', 'Enchant Creature').
card_original_text('armor of thorns'/'MIR', 'You may choose to play Armor of Thorns as an instant; if you do, bury it at end of turn.\nPlay only on a nonblack creature.\nEnchanted creature gets +2/+2.').
card_first_print('armor of thorns', 'MIR').
card_image_name('armor of thorns'/'MIR', 'armor of thorns').
card_uid('armor of thorns'/'MIR', 'MIR:Armor of Thorns:armor of thorns').
card_rarity('armor of thorns'/'MIR', 'Common').
card_artist('armor of thorns'/'MIR', 'Alan Rabinowitz').
card_multiverse_id('armor of thorns'/'MIR', '3375').

card_in_set('armorer guildmage', 'MIR').
card_original_type('armorer guildmage'/'MIR', 'Summon — Wizard').
card_original_text('armorer guildmage'/'MIR', '{B}, {T}: Target creature gets +1/+0 until end of turn.\n{G}, {T}: Target creature gets +0/+1 until end of turn.').
card_first_print('armorer guildmage', 'MIR').
card_image_name('armorer guildmage'/'MIR', 'armorer guildmage').
card_uid('armorer guildmage'/'MIR', 'MIR:Armorer Guildmage:armorer guildmage').
card_rarity('armorer guildmage'/'MIR', 'Common').
card_artist('armorer guildmage'/'MIR', 'Martin McKenna').
card_flavor_text('armorer guildmage'/'MIR', 'Zhalfir\'s destiny rings in tempered steel.\n—Armorer Guild maxim').
card_multiverse_id('armorer guildmage'/'MIR', '3427').

card_in_set('ashen powder', 'MIR').
card_original_type('ashen powder'/'MIR', 'Sorcery').
card_original_text('ashen powder'/'MIR', 'Put target creature card from an opponent\'s graveyard into play under your control.').
card_first_print('ashen powder', 'MIR').
card_image_name('ashen powder'/'MIR', 'ashen powder').
card_uid('ashen powder'/'MIR', 'MIR:Ashen Powder:ashen powder').
card_rarity('ashen powder'/'MIR', 'Rare').
card_artist('ashen powder'/'MIR', 'Geofrey Darrow').
card_flavor_text('ashen powder'/'MIR', '\"What you call decay, I call ripening. You\'ll gain no sustenance from this harvest.\"\n—Kaervek').
card_multiverse_id('ashen powder'/'MIR', '3273').

card_in_set('asmira, holy avenger', 'MIR').
card_original_type('asmira, holy avenger'/'MIR', 'Summon — Legend').
card_original_text('asmira, holy avenger'/'MIR', 'Flying\nAt the end of each turn, put a +1/+1 counter on Asmira, Holy Avenger for each creature put into your graveyard from play that turn.').
card_first_print('asmira, holy avenger', 'MIR').
card_image_name('asmira, holy avenger'/'MIR', 'asmira, holy avenger').
card_uid('asmira, holy avenger'/'MIR', 'MIR:Asmira, Holy Avenger:asmira, holy avenger').
card_rarity('asmira, holy avenger'/'MIR', 'Rare').
card_artist('asmira, holy avenger'/'MIR', 'Rebecca Guay').
card_flavor_text('asmira, holy avenger'/'MIR', '\"Omens lie in this early harvest\'s fruit.\"\n—Asmira, Holy Avenger').
card_multiverse_id('asmira, holy avenger'/'MIR', '3527').

card_in_set('auspicious ancestor', 'MIR').
card_original_type('auspicious ancestor'/'MIR', 'Summon — Ancestor').
card_original_text('auspicious ancestor'/'MIR', 'If Auspicious Ancestor is put into the graveyard from play, gain 3 life.\n{1}: Gain 1 life. Use this ability only when a white spell is successfully cast and only once for each such spell.').
card_first_print('auspicious ancestor', 'MIR').
card_image_name('auspicious ancestor'/'MIR', 'auspicious ancestor').
card_uid('auspicious ancestor'/'MIR', 'MIR:Auspicious Ancestor:auspicious ancestor').
card_rarity('auspicious ancestor'/'MIR', 'Rare').
card_artist('auspicious ancestor'/'MIR', 'Zina Saunders').
card_multiverse_id('auspicious ancestor'/'MIR', '3478').

card_in_set('azimaet drake', 'MIR').
card_original_type('azimaet drake'/'MIR', 'Summon — Drake').
card_original_text('azimaet drake'/'MIR', 'Flying\n{U} +1/+0 until end of turn. You cannot spend more than {U} in this way each turn.').
card_first_print('azimaet drake', 'MIR').
card_image_name('azimaet drake'/'MIR', 'azimaet drake').
card_uid('azimaet drake'/'MIR', 'MIR:Azimaet Drake:azimaet drake').
card_rarity('azimaet drake'/'MIR', 'Common').
card_artist('azimaet drake'/'MIR', 'Gerry Grace').
card_flavor_text('azimaet drake'/'MIR', '\"A vizier\'s dish, best served drenched in honey and almonds.\"\n—Suq\'Ata royal recipe').
card_multiverse_id('azimaet drake'/'MIR', '3324').

card_in_set('bad river', 'MIR').
card_original_type('bad river'/'MIR', 'Land').
card_original_text('bad river'/'MIR', 'Bad River comes into play tapped.\n{T}, Sacrifice Bad River: Search your library for an island or swamp card. Put that land into play. Shuffle your library afterwards.').
card_first_print('bad river', 'MIR').
card_image_name('bad river'/'MIR', 'bad river').
card_uid('bad river'/'MIR', 'MIR:Bad River:bad river').
card_rarity('bad river'/'MIR', 'Uncommon').
card_artist('bad river'/'MIR', 'Terese Nielsen').
card_multiverse_id('bad river'/'MIR', '3570').

card_in_set('barbed foliage', 'MIR').
card_original_type('barbed foliage'/'MIR', 'Enchantment').
card_original_text('barbed foliage'/'MIR', 'Whenever a creature attacks you, it loses flanking until end of turn.\nWhenever a creature without flying attacks you, Barbed Foliage deals 1 damage to it.').
card_first_print('barbed foliage', 'MIR').
card_image_name('barbed foliage'/'MIR', 'barbed foliage').
card_uid('barbed foliage'/'MIR', 'MIR:Barbed Foliage:barbed foliage').
card_rarity('barbed foliage'/'MIR', 'Uncommon').
card_artist('barbed foliage'/'MIR', 'Mark Poole').
card_multiverse_id('barbed foliage'/'MIR', '3376').

card_in_set('barbed-back wurm', 'MIR').
card_original_type('barbed-back wurm'/'MIR', 'Summon — Wurm').
card_original_text('barbed-back wurm'/'MIR', '{B} Target green creature blocking Barbed-Back Wurm gets -1/-1 until end of turn.').
card_first_print('barbed-back wurm', 'MIR').
card_image_name('barbed-back wurm'/'MIR', 'barbed-back wurm').
card_uid('barbed-back wurm'/'MIR', 'MIR:Barbed-Back Wurm:barbed-back wurm').
card_rarity('barbed-back wurm'/'MIR', 'Uncommon').
card_artist('barbed-back wurm'/'MIR', 'Gary Leach').
card_flavor_text('barbed-back wurm'/'MIR', '\"The Mwonvuli Jungle has given the darkness teeth for tearing the gift of life from the weak.\"\n—Jolrael').
card_multiverse_id('barbed-back wurm'/'MIR', '3274').

card_in_set('barreling attack', 'MIR').
card_original_type('barreling attack'/'MIR', 'Instant').
card_original_text('barreling attack'/'MIR', 'Target creature gains trample until end of turn. That creature gets +1/+1 until end of turn for each creature that blocks it.').
card_first_print('barreling attack', 'MIR').
card_image_name('barreling attack'/'MIR', 'barreling attack').
card_uid('barreling attack'/'MIR', 'MIR:Barreling Attack:barreling attack').
card_rarity('barreling attack'/'MIR', 'Rare').
card_artist('barreling attack'/'MIR', 'Ian Miller').
card_flavor_text('barreling attack'/'MIR', '\"The trollish game of push-fall inspired this impressive tactic.\"\n—Sidar Jabari').
card_multiverse_id('barreling attack'/'MIR', '3428').

card_in_set('basalt golem', 'MIR').
card_original_type('basalt golem'/'MIR', 'Artifact Creature').
card_original_text('basalt golem'/'MIR', 'Basalt Golem cannot be blocked by artifact creatures.\nWhenever Basalt Golem is blocked by any creature, bury that creature at end of combat and put a Stone token into play under the control of the creature\'s controller. Treat this token as a 0/2 artifact creature that counts as a Wall.').
card_first_print('basalt golem', 'MIR').
card_image_name('basalt golem'/'MIR', 'basalt golem').
card_uid('basalt golem'/'MIR', 'MIR:Basalt Golem:basalt golem').
card_rarity('basalt golem'/'MIR', 'Uncommon').
card_artist('basalt golem'/'MIR', 'Scott Kirschner').
card_multiverse_id('basalt golem'/'MIR', '3242').

card_in_set('bay falcon', 'MIR').
card_original_type('bay falcon'/'MIR', 'Summon — Falcon').
card_original_text('bay falcon'/'MIR', 'Flying\nAttacking does not cause Bay Falcon to tap.').
card_first_print('bay falcon', 'MIR').
card_image_name('bay falcon'/'MIR', 'bay falcon').
card_uid('bay falcon'/'MIR', 'MIR:Bay Falcon:bay falcon').
card_rarity('bay falcon'/'MIR', 'Common').
card_artist('bay falcon'/'MIR', 'Una Fricker').
card_flavor_text('bay falcon'/'MIR', '\"Wear the falcon\'s feathers in your hair—\nbe fast as the falcon, bright as the Sun.\"\n—\"Rise on Wings,\" Zhalfirin song').
card_multiverse_id('bay falcon'/'MIR', '3325').

card_in_set('bazaar of wonders', 'MIR').
card_original_type('bazaar of wonders'/'MIR', 'Enchant World').
card_original_text('bazaar of wonders'/'MIR', 'When Bazaar of Wonders comes into play, remove all cards in all graveyards from the game.\nWhenever a spell is played, counter it if a card with the same name is in play or in any graveyard.').
card_first_print('bazaar of wonders', 'MIR').
card_image_name('bazaar of wonders'/'MIR', 'bazaar of wonders').
card_uid('bazaar of wonders'/'MIR', 'MIR:Bazaar of Wonders:bazaar of wonders').
card_rarity('bazaar of wonders'/'MIR', 'Rare').
card_artist('bazaar of wonders'/'MIR', 'Liz Danforth').
card_multiverse_id('bazaar of wonders'/'MIR', '3326').

card_in_set('benevolent unicorn', 'MIR').
card_original_type('benevolent unicorn'/'MIR', 'Summon — Unicorn').
card_original_text('benevolent unicorn'/'MIR', 'Whenever a spell assigns damage to a creature or player, that damage is reduced by 1.').
card_first_print('benevolent unicorn', 'MIR').
card_image_name('benevolent unicorn'/'MIR', 'benevolent unicorn').
card_uid('benevolent unicorn'/'MIR', 'MIR:Benevolent Unicorn:benevolent unicorn').
card_rarity('benevolent unicorn'/'MIR', 'Common').
card_artist('benevolent unicorn'/'MIR', 'David A. Cherry').
card_flavor_text('benevolent unicorn'/'MIR', 'The best use for a unicorn\'s horn is to adorn a unicorn.\n—Femeref adage').
card_multiverse_id('benevolent unicorn'/'MIR', '3479').

card_in_set('benthic djinn', 'MIR').
card_original_type('benthic djinn'/'MIR', 'Summon — Djinn').
card_original_text('benthic djinn'/'MIR', 'Islandwalk (If defending player controls any islands, this creature is unblockable.)\nDuring your upkeep, lose 2 life.').
card_first_print('benthic djinn', 'MIR').
card_image_name('benthic djinn'/'MIR', 'benthic djinn').
card_uid('benthic djinn'/'MIR', 'MIR:Benthic Djinn:benthic djinn').
card_rarity('benthic djinn'/'MIR', 'Rare').
card_artist('benthic djinn'/'MIR', 'Adam Rex').
card_flavor_text('benthic djinn'/'MIR', '\"In all my years I\'ve only met one mage who successfully trapped a djinn—and none who tried and failed.\"\n—Talibah, embermage').
card_multiverse_id('benthic djinn'/'MIR', '3528').

card_in_set('binding agony', 'MIR').
card_original_type('binding agony'/'MIR', 'Enchant Creature').
card_original_text('binding agony'/'MIR', 'For each 1 damage dealt to enchanted creature, Binding Agony deals 1 damage to that creature\'s controller.').
card_first_print('binding agony', 'MIR').
card_image_name('binding agony'/'MIR', 'binding agony').
card_uid('binding agony'/'MIR', 'MIR:Binding Agony:binding agony').
card_rarity('binding agony'/'MIR', 'Common').
card_artist('binding agony'/'MIR', 'Robert Bliss').
card_flavor_text('binding agony'/'MIR', 'A crash of rhinos in my head\n—Femeref expression meaning\n\"a severe head pain\"').
card_multiverse_id('binding agony'/'MIR', '3275').

card_in_set('blighted shaman', 'MIR').
card_original_type('blighted shaman'/'MIR', 'Summon — Cleric').
card_original_text('blighted shaman'/'MIR', '{T}, Sacrifice a creature: Target creature gets +2/+2 until end of turn.\n{T}, Sacrifice a swamp: Target creature gets +1/+1 until end of turn.').
card_first_print('blighted shaman', 'MIR').
card_image_name('blighted shaman'/'MIR', 'blighted shaman').
card_uid('blighted shaman'/'MIR', 'MIR:Blighted Shaman:blighted shaman').
card_rarity('blighted shaman'/'MIR', 'Uncommon').
card_artist('blighted shaman'/'MIR', 'Ian Miller').
card_multiverse_id('blighted shaman'/'MIR', '3276').

card_in_set('blind fury', 'MIR').
card_original_type('blind fury'/'MIR', 'Instant').
card_original_text('blind fury'/'MIR', 'All creatures lose trample until end of turn. Double all combat damage assigned to creatures this turn.').
card_first_print('blind fury', 'MIR').
card_image_name('blind fury'/'MIR', 'blind fury').
card_uid('blind fury'/'MIR', 'MIR:Blind Fury:blind fury').
card_rarity('blind fury'/'MIR', 'Uncommon').
card_artist('blind fury'/'MIR', 'John Coulthart').
card_flavor_text('blind fury'/'MIR', '\"Tell me . . . have you ever sung the Song of Blood?\"\n—Purraj of Urborg').
card_multiverse_id('blind fury'/'MIR', '3429').

card_in_set('blinding light', 'MIR').
card_original_type('blinding light'/'MIR', 'Sorcery').
card_original_text('blinding light'/'MIR', 'Tap all nonwhite creatures.').
card_first_print('blinding light', 'MIR').
card_image_name('blinding light'/'MIR', 'blinding light').
card_uid('blinding light'/'MIR', 'MIR:Blinding Light:blinding light').
card_rarity('blinding light'/'MIR', 'Uncommon').
card_artist('blinding light'/'MIR', 'Hannibal King').
card_flavor_text('blinding light'/'MIR', '\"My shield will bear a shining sun so you will always be with me. / Inlaid with gold, it will shine like glowing embers.\"\n—\"Love Song of Night and Day\"').
card_multiverse_id('blinding light'/'MIR', '3480').

card_in_set('blistering barrier', 'MIR').
card_original_type('blistering barrier'/'MIR', 'Summon — Wall').
card_original_text('blistering barrier'/'MIR', '').
card_first_print('blistering barrier', 'MIR').
card_image_name('blistering barrier'/'MIR', 'blistering barrier').
card_uid('blistering barrier'/'MIR', 'MIR:Blistering Barrier:blistering barrier').
card_rarity('blistering barrier'/'MIR', 'Common').
card_artist('blistering barrier'/'MIR', 'David Ho').
card_flavor_text('blistering barrier'/'MIR', 'I live without food, stand without leg, wound without force, and am harder to fight than to kill. What am I?\n—Femeref riddle').
card_multiverse_id('blistering barrier'/'MIR', '3430').

card_in_set('bone harvest', 'MIR').
card_original_type('bone harvest'/'MIR', 'Instant').
card_original_text('bone harvest'/'MIR', 'Put any number of target creature cards from your graveyard on top of your library.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_first_print('bone harvest', 'MIR').
card_image_name('bone harvest'/'MIR', 'bone harvest').
card_uid('bone harvest'/'MIR', 'MIR:Bone Harvest:bone harvest').
card_rarity('bone harvest'/'MIR', 'Common').
card_artist('bone harvest'/'MIR', 'Greg Simanson').
card_flavor_text('bone harvest'/'MIR', '\"Only fools believe they will face my armies but once.\"\n—Kaervek').
card_multiverse_id('bone harvest'/'MIR', '3277').

card_in_set('bone mask', 'MIR').
card_original_type('bone mask'/'MIR', 'Artifact').
card_original_text('bone mask'/'MIR', '{2}, {T}: Prevent all damage to you from any one source. For each 1 damage prevented in this way, remove the top card of your library from the game.').
card_first_print('bone mask', 'MIR').
card_image_name('bone mask'/'MIR', 'bone mask').
card_uid('bone mask'/'MIR', 'MIR:Bone Mask:bone mask').
card_rarity('bone mask'/'MIR', 'Rare').
card_artist('bone mask'/'MIR', 'D. Alexander Gregory').
card_flavor_text('bone mask'/'MIR', '\"The protection offered comes at a price. How numerous are your kin?\"\n—Purraj of Urborg').
card_multiverse_id('bone mask'/'MIR', '3243').

card_in_set('boomerang', 'MIR').
card_original_type('boomerang'/'MIR', 'Instant').
card_original_text('boomerang'/'MIR', 'Return target permanent to owner\'s hand.').
card_image_name('boomerang'/'MIR', 'boomerang').
card_uid('boomerang'/'MIR', 'MIR:Boomerang:boomerang').
card_rarity('boomerang'/'MIR', 'Common').
card_artist('boomerang'/'MIR', 'Richard Kane Ferguson').
card_flavor_text('boomerang'/'MIR', 'A lie always returns; be careful how you catch it.').
card_multiverse_id('boomerang'/'MIR', '3327').

card_in_set('breathstealer', 'MIR').
card_original_type('breathstealer'/'MIR', 'Summon — Night Stalker').
card_original_text('breathstealer'/'MIR', '{B} +1/-1 until end of turn').
card_first_print('breathstealer', 'MIR').
card_image_name('breathstealer'/'MIR', 'breathstealer').
card_uid('breathstealer'/'MIR', 'MIR:Breathstealer:breathstealer').
card_rarity('breathstealer'/'MIR', 'Common').
card_artist('breathstealer'/'MIR', 'Cliff Nielsen').
card_flavor_text('breathstealer'/'MIR', '\"As a child, I slept with a cloth over my face, to hide from the spirits that might pull the air from my lungs.\"\n—Mosi, Femeref weaver').
card_multiverse_id('breathstealer'/'MIR', '3278').

card_in_set('brushwagg', 'MIR').
card_original_type('brushwagg'/'MIR', 'Summon — Brushwagg').
card_original_text('brushwagg'/'MIR', 'If Brushwagg blocks or is blocked, it gets -2/+2 until end of turn.').
card_first_print('brushwagg', 'MIR').
card_image_name('brushwagg'/'MIR', 'brushwagg').
card_uid('brushwagg'/'MIR', 'MIR:Brushwagg:brushwagg').
card_rarity('brushwagg'/'MIR', 'Rare').
card_artist('brushwagg'/'MIR', 'Ian Miller').
card_flavor_text('brushwagg'/'MIR', '\"Defiantly, the young cyclops popped the brushwagg into his mouth. His cheeks suddenly puffed, his eye bulged, and he was forced to agree with his elder.\"\n—Afari, Tales').
card_multiverse_id('brushwagg'/'MIR', '3377').

card_in_set('builder\'s bane', 'MIR').
card_original_type('builder\'s bane'/'MIR', 'Sorcery').
card_original_text('builder\'s bane'/'MIR', 'Destroy X target artifacts. For each artifact put into the graveyard in this way, Builder\'s Bane deals 1 damage to that artifact\'s controller.').
card_first_print('builder\'s bane', 'MIR').
card_image_name('builder\'s bane'/'MIR', 'builder\'s bane').
card_uid('builder\'s bane'/'MIR', 'MIR:Builder\'s Bane:builder\'s bane').
card_rarity('builder\'s bane'/'MIR', 'Common').
card_artist('builder\'s bane'/'MIR', 'Charles Gillespie').
card_flavor_text('builder\'s bane'/'MIR', 'There is only so much a person may be buried with.').
card_multiverse_id('builder\'s bane'/'MIR', '3431').

card_in_set('burning palm efreet', 'MIR').
card_original_type('burning palm efreet'/'MIR', 'Summon — Efreet').
card_original_text('burning palm efreet'/'MIR', '{1}{R}{R} Burning Palm Efreet deals 2 damage to target creature with flying and that creature loses flying until end of turn.').
card_first_print('burning palm efreet', 'MIR').
card_image_name('burning palm efreet'/'MIR', 'burning palm efreet').
card_uid('burning palm efreet'/'MIR', 'MIR:Burning Palm Efreet:burning palm efreet').
card_rarity('burning palm efreet'/'MIR', 'Uncommon').
card_artist('burning palm efreet'/'MIR', 'Dermot Power').
card_flavor_text('burning palm efreet'/'MIR', 'Even the lion envies the lark.').
card_multiverse_id('burning palm efreet'/'MIR', '3432').

card_in_set('burning shield askari', 'MIR').
card_original_type('burning shield askari'/'MIR', 'Summon — Knight').
card_original_text('burning shield askari'/'MIR', 'Flanking (Whenever a creature without flanking is assigned to block this creature, the blocking creature gets -1/-1 until end of turn.)\n{R}{R} First strike until end of turn').
card_first_print('burning shield askari', 'MIR').
card_image_name('burning shield askari'/'MIR', 'burning shield askari').
card_uid('burning shield askari'/'MIR', 'MIR:Burning Shield Askari:burning shield askari').
card_rarity('burning shield askari'/'MIR', 'Common').
card_artist('burning shield askari'/'MIR', 'Dan Frazier').
card_flavor_text('burning shield askari'/'MIR', '\"Any day that I ride my horse up to my ankles in the remains of my enemies is a good day.\"\n—Akin, seasoned askari').
card_multiverse_id('burning shield askari'/'MIR', '3433').

card_in_set('cadaverous bloom', 'MIR').
card_original_type('cadaverous bloom'/'MIR', 'Enchantment').
card_original_text('cadaverous bloom'/'MIR', 'Choose a card in your hand and remove it from the game: Add {B}{B} or {G}{G} to your mana pool. Play this ability as a mana source.').
card_first_print('cadaverous bloom', 'MIR').
card_image_name('cadaverous bloom'/'MIR', 'cadaverous bloom').
card_uid('cadaverous bloom'/'MIR', 'MIR:Cadaverous Bloom:cadaverous bloom').
card_rarity('cadaverous bloom'/'MIR', 'Rare').
card_artist('cadaverous bloom'/'MIR', 'Alan Rabinowitz').
card_flavor_text('cadaverous bloom'/'MIR', '\"It\'s filthy work, planting them.\"\n—Kifimbo, Shadow Guildmage').
card_multiverse_id('cadaverous bloom'/'MIR', '3529').

card_in_set('cadaverous knight', 'MIR').
card_original_type('cadaverous knight'/'MIR', 'Summon — Knight').
card_original_text('cadaverous knight'/'MIR', 'Flanking (Whenever a creature without flanking is assigned to block this creature, the blocking creature gets -1/-1 until end of turn.)\n{1}{B}{B} Regenerate').
card_first_print('cadaverous knight', 'MIR').
card_image_name('cadaverous knight'/'MIR', 'cadaverous knight').
card_uid('cadaverous knight'/'MIR', 'MIR:Cadaverous Knight:cadaverous knight').
card_rarity('cadaverous knight'/'MIR', 'Common').
card_artist('cadaverous knight'/'MIR', 'Dermot Power').
card_flavor_text('cadaverous knight'/'MIR', 'Grieve for the soul in death dishonored.\n—Shadow Guild saying').
card_multiverse_id('cadaverous knight'/'MIR', '3279').

card_in_set('canopy dragon', 'MIR').
card_original_type('canopy dragon'/'MIR', 'Summon — Dragon').
card_original_text('canopy dragon'/'MIR', 'Trample\n{1}{G} Flying and loses trample until end of turn').
card_first_print('canopy dragon', 'MIR').
card_image_name('canopy dragon'/'MIR', 'canopy dragon').
card_uid('canopy dragon'/'MIR', 'MIR:Canopy Dragon:canopy dragon').
card_rarity('canopy dragon'/'MIR', 'Rare').
card_artist('canopy dragon'/'MIR', 'Alan Rabinowitz').
card_flavor_text('canopy dragon'/'MIR', '\"In the Mwonvuli you must divide your attention between what hangs overhead and what lies underfoot.\"\n—Sidar Jabari').
card_multiverse_id('canopy dragon'/'MIR', '3378').

card_in_set('carrion', 'MIR').
card_original_type('carrion'/'MIR', 'Instant').
card_original_text('carrion'/'MIR', 'Sacrifice a creature: Put into play a number of Maggot tokens equal to the sacrificed creature\'s power. Treat these tokens as 0/1 black creatures.').
card_first_print('carrion', 'MIR').
card_image_name('carrion'/'MIR', 'carrion').
card_uid('carrion'/'MIR', 'MIR:Carrion:carrion').
card_rarity('carrion'/'MIR', 'Rare').
card_artist('carrion'/'MIR', 'Geofrey Darrow').
card_flavor_text('carrion'/'MIR', 'King or herder, the maggots care not.').
card_multiverse_id('carrion'/'MIR', '3280').

card_in_set('catacomb dragon', 'MIR').
card_original_type('catacomb dragon'/'MIR', 'Summon — Dragon').
card_original_text('catacomb dragon'/'MIR', 'Flying\nWhenever Catacomb Dragon is blocked by any nonartifact, non-Dragon creature, that creature\'s power is halved, rounded up, until end of turn.').
card_first_print('catacomb dragon', 'MIR').
card_image_name('catacomb dragon'/'MIR', 'catacomb dragon').
card_uid('catacomb dragon'/'MIR', 'MIR:Catacomb Dragon:catacomb dragon').
card_rarity('catacomb dragon'/'MIR', 'Rare').
card_artist('catacomb dragon'/'MIR', 'David O\'Connor').
card_multiverse_id('catacomb dragon'/'MIR', '3281').

card_in_set('celestial dawn', 'MIR').
card_original_type('celestial dawn'/'MIR', 'Enchantment').
card_original_text('celestial dawn'/'MIR', 'All nonland cards you own that are not in play are white. All nonland permanents you control are white. All lands you control are plains. All colored mana symbols in all costs on all of these cards and permanents are {W}.').
card_first_print('celestial dawn', 'MIR').
card_image_name('celestial dawn'/'MIR', 'celestial dawn').
card_uid('celestial dawn'/'MIR', 'MIR:Celestial Dawn:celestial dawn').
card_rarity('celestial dawn'/'MIR', 'Rare').
card_artist('celestial dawn'/'MIR', 'Liz Danforth').
card_multiverse_id('celestial dawn'/'MIR', '3481').

card_in_set('cerulean wyvern', 'MIR').
card_original_type('cerulean wyvern'/'MIR', 'Summon — Wyvern').
card_original_text('cerulean wyvern'/'MIR', 'Flying, protection from green').
card_first_print('cerulean wyvern', 'MIR').
card_image_name('cerulean wyvern'/'MIR', 'cerulean wyvern').
card_uid('cerulean wyvern'/'MIR', 'MIR:Cerulean Wyvern:cerulean wyvern').
card_rarity('cerulean wyvern'/'MIR', 'Uncommon').
card_artist('cerulean wyvern'/'MIR', 'Gerry Grace').
card_flavor_text('cerulean wyvern'/'MIR', '\"Once, a Quirion asked why I was so cautious when the day was clear. I told him of the sky-blue hide of the cerluean wyvern, and he offered me double if I could conjure a strong tailwind!\"\n—Sisay, Captain of the Weatherlight').
card_multiverse_id('cerulean wyvern'/'MIR', '3328').

card_in_set('chaos charm', 'MIR').
card_original_type('chaos charm'/'MIR', 'Instant').
card_original_text('chaos charm'/'MIR', 'Choose one Target creature is unaffected by summoning sickness this turn; or Chaos Charm deals 1 damage to target creature; or destroy target Wall.').
card_first_print('chaos charm', 'MIR').
card_image_name('chaos charm'/'MIR', 'chaos charm').
card_uid('chaos charm'/'MIR', 'MIR:Chaos Charm:chaos charm').
card_rarity('chaos charm'/'MIR', 'Common').
card_artist('chaos charm'/'MIR', 'Steve Luke').
card_multiverse_id('chaos charm'/'MIR', '3434').

card_in_set('chaosphere', 'MIR').
card_original_type('chaosphere'/'MIR', 'Enchant World').
card_original_text('chaosphere'/'MIR', 'Creatures with flying cannot block creatures without flying.\nCreatures without flying can block creatures with flying.').
card_first_print('chaosphere', 'MIR').
card_image_name('chaosphere'/'MIR', 'chaosphere').
card_uid('chaosphere'/'MIR', 'MIR:Chaosphere:chaosphere').
card_rarity('chaosphere'/'MIR', 'Rare').
card_artist('chaosphere'/'MIR', 'Steve Luke').
card_flavor_text('chaosphere'/'MIR', '\"I dance the nothing dance, / Feet in the air, knives in the ground.\"\n—Femeref chant').
card_multiverse_id('chaosphere'/'MIR', '3435').

card_in_set('charcoal diamond', 'MIR').
card_original_type('charcoal diamond'/'MIR', 'Artifact').
card_original_text('charcoal diamond'/'MIR', 'Charcoal Diamond comes into play tapped.\n{T}: Add {B} to your mana pool. Play this ability as a mana source.').
card_first_print('charcoal diamond', 'MIR').
card_image_name('charcoal diamond'/'MIR', 'charcoal diamond').
card_uid('charcoal diamond'/'MIR', 'MIR:Charcoal Diamond:charcoal diamond').
card_rarity('charcoal diamond'/'MIR', 'Uncommon').
card_artist('charcoal diamond'/'MIR', 'Drew Tucker').
card_multiverse_id('charcoal diamond'/'MIR', '3244').

card_in_set('chariot of the sun', 'MIR').
card_original_type('chariot of the sun'/'MIR', 'Artifact').
card_original_text('chariot of the sun'/'MIR', '{2}, {T}: Target creature you control gains flying and has its toughness reduced to 1 until end of turn.').
card_first_print('chariot of the sun', 'MIR').
card_image_name('chariot of the sun'/'MIR', 'chariot of the sun').
card_uid('chariot of the sun'/'MIR', 'MIR:Chariot of the Sun:chariot of the sun').
card_rarity('chariot of the sun'/'MIR', 'Uncommon').
card_artist('chariot of the sun'/'MIR', 'Gerry Grace').
card_flavor_text('chariot of the sun'/'MIR', '\"Sun follows Moon until she tires, then carries her until she\'s strong / and runs ahead of him again.\"\n—\"Love Song of Night and Day\"').
card_multiverse_id('chariot of the sun'/'MIR', '3245').

card_in_set('choking sands', 'MIR').
card_original_type('choking sands'/'MIR', 'Sorcery').
card_original_text('choking sands'/'MIR', 'Destroy target nonswamp land. If that land is a nonbasic land, Choking Sands deals 2 damage to the land\'s controller.').
card_first_print('choking sands', 'MIR').
card_image_name('choking sands'/'MIR', 'choking sands').
card_uid('choking sands'/'MIR', 'MIR:Choking Sands:choking sands').
card_rarity('choking sands'/'MIR', 'Common').
card_artist('choking sands'/'MIR', 'Roger Raupp').
card_flavor_text('choking sands'/'MIR', '\"The people wiped the sand from their eyes and cursed—and left the barren land to the hyenas and vipers.\"\n—Afari, Tales').
card_multiverse_id('choking sands'/'MIR', '3282').

card_in_set('cinder cloud', 'MIR').
card_original_type('cinder cloud'/'MIR', 'Instant').
card_original_text('cinder cloud'/'MIR', 'Destroy target creature. If a white creature is put into the graveyard in this way, Cinder Cloud deals to that creature\'s controller an amount of damage equal to the creature\'s power.').
card_first_print('cinder cloud', 'MIR').
card_image_name('cinder cloud'/'MIR', 'cinder cloud').
card_uid('cinder cloud'/'MIR', 'MIR:Cinder Cloud:cinder cloud').
card_rarity('cinder cloud'/'MIR', 'Uncommon').
card_artist('cinder cloud'/'MIR', 'JOCK').
card_multiverse_id('cinder cloud'/'MIR', '3436').

card_in_set('circle of despair', 'MIR').
card_original_type('circle of despair'/'MIR', 'Enchantment').
card_original_text('circle of despair'/'MIR', '{1}, Sacrifice a creature: Prevent all damage to any creature or player from any one source.').
card_first_print('circle of despair', 'MIR').
card_image_name('circle of despair'/'MIR', 'circle of despair').
card_uid('circle of despair'/'MIR', 'MIR:Circle of Despair:circle of despair').
card_rarity('circle of despair'/'MIR', 'Rare').
card_artist('circle of despair'/'MIR', 'Scott M. Fischer').
card_flavor_text('circle of despair'/'MIR', '\"Life is a resource that should belong only to the strong.\"\n—Shauku, Endbringer').
card_multiverse_id('circle of despair'/'MIR', '3530').

card_in_set('civic guildmage', 'MIR').
card_original_type('civic guildmage'/'MIR', 'Summon — Wizard').
card_original_text('civic guildmage'/'MIR', '{G}, {T}: Target creature gets +0/+1 until end of turn.\n{U}, {T}: Put target creature you control on top of owner\'s library.').
card_first_print('civic guildmage', 'MIR').
card_image_name('civic guildmage'/'MIR', 'civic guildmage').
card_uid('civic guildmage'/'MIR', 'MIR:Civic Guildmage:civic guildmage').
card_rarity('civic guildmage'/'MIR', 'Common').
card_artist('civic guildmage'/'MIR', 'Andrew Robinson').
card_flavor_text('civic guildmage'/'MIR', 'To condemn the innocent you must first condemn yourself.\n—Civic Guild maxim').
card_multiverse_id('civic guildmage'/'MIR', '3482').

card_in_set('cloak of invisibility', 'MIR').
card_original_type('cloak of invisibility'/'MIR', 'Enchant Creature').
card_original_text('cloak of invisibility'/'MIR', 'Enchanted creature gains phasing and cannot be blocked except by Walls.').
card_first_print('cloak of invisibility', 'MIR').
card_image_name('cloak of invisibility'/'MIR', 'cloak of invisibility').
card_uid('cloak of invisibility'/'MIR', 'MIR:Cloak of Invisibility:cloak of invisibility').
card_rarity('cloak of invisibility'/'MIR', 'Common').
card_artist('cloak of invisibility'/'MIR', 'John Coulthart').
card_flavor_text('cloak of invisibility'/'MIR', '\"Useful . . . if you can find it.\"\n—Pashad ibn Asim, Suq\'Ata trader').
card_multiverse_id('cloak of invisibility'/'MIR', '3329').

card_in_set('consuming ferocity', 'MIR').
card_original_type('consuming ferocity'/'MIR', 'Enchant Creature').
card_original_text('consuming ferocity'/'MIR', 'Play only on a non-Wall creature.\nEnchanted creature gets +1/+0.\nDuring your upkeep, put a +1/+0 counter on enchanted creature. At the end of any upkeep, if that creature has three of these counters on it, bury the creature and it deals to its controller an amount of damage equal to its power.').
card_first_print('consuming ferocity', 'MIR').
card_image_name('consuming ferocity'/'MIR', 'consuming ferocity').
card_uid('consuming ferocity'/'MIR', 'MIR:Consuming Ferocity:consuming ferocity').
card_rarity('consuming ferocity'/'MIR', 'Uncommon').
card_artist('consuming ferocity'/'MIR', 'Scott Kirschner').
card_multiverse_id('consuming ferocity'/'MIR', '3437').

card_in_set('coral fighters', 'MIR').
card_original_type('coral fighters'/'MIR', 'Summon — Merfolk').
card_original_text('coral fighters'/'MIR', 'If Coral Fighters attacks and is not blocked, look at the top card of defending player\'s library. You may choose to put that card on the bottom of that player\'s library.').
card_first_print('coral fighters', 'MIR').
card_image_name('coral fighters'/'MIR', 'coral fighters').
card_uid('coral fighters'/'MIR', 'MIR:Coral Fighters:coral fighters').
card_rarity('coral fighters'/'MIR', 'Uncommon').
card_artist('coral fighters'/'MIR', 'Steve Luke').
card_multiverse_id('coral fighters'/'MIR', '3330').

card_in_set('crash of rhinos', 'MIR').
card_original_type('crash of rhinos'/'MIR', 'Summon — Rhinos').
card_original_text('crash of rhinos'/'MIR', 'Trample').
card_first_print('crash of rhinos', 'MIR').
card_image_name('crash of rhinos'/'MIR', 'crash of rhinos').
card_uid('crash of rhinos'/'MIR', 'MIR:Crash of Rhinos:crash of rhinos').
card_rarity('crash of rhinos'/'MIR', 'Common').
card_artist('crash of rhinos'/'MIR', 'Steve White').
card_flavor_text('crash of rhinos'/'MIR', 'Love is like a rhino, short-sighted and hasty; if it cannot find a way, it will make a way.\n—Femeref adage').
card_multiverse_id('crash of rhinos'/'MIR', '3379').

card_in_set('crimson hellkite', 'MIR').
card_original_type('crimson hellkite'/'MIR', 'Summon — Dragon').
card_original_text('crimson hellkite'/'MIR', 'Flying\n{X}, {T}: Crimson Hellkite deals X damage to target creature. Spend only red mana in this way.').
card_first_print('crimson hellkite', 'MIR').
card_image_name('crimson hellkite'/'MIR', 'crimson hellkite').
card_uid('crimson hellkite'/'MIR', 'MIR:Crimson Hellkite:crimson hellkite').
card_rarity('crimson hellkite'/'MIR', 'Rare').
card_artist('crimson hellkite'/'MIR', 'Gerry Grace').
card_flavor_text('crimson hellkite'/'MIR', '\"Dragonfire forged me a warrior.\"\n—Rashida Scalebane').
card_multiverse_id('crimson hellkite'/'MIR', '3438').

card_in_set('crimson roc', 'MIR').
card_original_type('crimson roc'/'MIR', 'Summon — Roc').
card_original_text('crimson roc'/'MIR', 'Flying\nIf Crimson Roc blocks any creature without flying, Crimson Roc gets +1/+0 and gains first strike until end of turn.').
card_first_print('crimson roc', 'MIR').
card_image_name('crimson roc'/'MIR', 'crimson roc').
card_uid('crimson roc'/'MIR', 'MIR:Crimson Roc:crimson roc').
card_rarity('crimson roc'/'MIR', 'Uncommon').
card_artist('crimson roc'/'MIR', 'Ian Miller').
card_multiverse_id('crimson roc'/'MIR', '3439').

card_in_set('crypt cobra', 'MIR').
card_original_type('crypt cobra'/'MIR', 'Summon — Cobra').
card_original_text('crypt cobra'/'MIR', 'If Crypt Cobra attacks and is not blocked, defending player gets a poison counter. If any player has ten or more poison counters, he or she loses the game.').
card_first_print('crypt cobra', 'MIR').
card_image_name('crypt cobra'/'MIR', 'crypt cobra').
card_uid('crypt cobra'/'MIR', 'MIR:Crypt Cobra:crypt cobra').
card_rarity('crypt cobra'/'MIR', 'Uncommon').
card_artist('crypt cobra'/'MIR', 'Ron Spencer').
card_multiverse_id('crypt cobra'/'MIR', '3283').

card_in_set('crystal golem', 'MIR').
card_original_type('crystal golem'/'MIR', 'Artifact Creature').
card_original_text('crystal golem'/'MIR', 'At end of your turn, Crystal Golem phases out.').
card_first_print('crystal golem', 'MIR').
card_image_name('crystal golem'/'MIR', 'crystal golem').
card_uid('crystal golem'/'MIR', 'MIR:Crystal Golem:crystal golem').
card_rarity('crystal golem'/'MIR', 'Uncommon').
card_artist('crystal golem'/'MIR', 'Mike Dringenberg').
card_flavor_text('crystal golem'/'MIR', '\"Once, a wealthy king commissioned an artificial creature to be crafted from the royal treasury, thinking it a clever ploy to have the jewels guard themselves.\"\n—Azeworai, \"The Golem\'s Wish\"').
card_multiverse_id('crystal golem'/'MIR', '3246').

card_in_set('crystal vein', 'MIR').
card_original_type('crystal vein'/'MIR', 'Land').
card_original_text('crystal vein'/'MIR', '{T}: Add one colorless mana to your mana pool.\n{T}, Sacrifice Crystal Vein: Add two colorless mana to your mana pool.').
card_first_print('crystal vein', 'MIR').
card_image_name('crystal vein'/'MIR', 'crystal vein').
card_uid('crystal vein'/'MIR', 'MIR:Crystal Vein:crystal vein').
card_rarity('crystal vein'/'MIR', 'Uncommon').
card_artist('crystal vein'/'MIR', 'Pat Morrissey').
card_multiverse_id('crystal vein'/'MIR', '3571').

card_in_set('cursed totem', 'MIR').
card_original_type('cursed totem'/'MIR', 'Artifact').
card_original_text('cursed totem'/'MIR', 'Players cannot play any creature abilities requiring an activation cost.').
card_first_print('cursed totem', 'MIR').
card_image_name('cursed totem'/'MIR', 'cursed totem').
card_uid('cursed totem'/'MIR', 'MIR:Cursed Totem:cursed totem').
card_rarity('cursed totem'/'MIR', 'Rare').
card_artist('cursed totem'/'MIR', 'D. Alexander Gregory').
card_flavor_text('cursed totem'/'MIR', 'Pass me from soul to soul / soldier to herder, herder to beast, beast to soil / until I am everywhere. / Then pass me those souls.\n—Totem inscription (translated)').
card_multiverse_id('cursed totem'/'MIR', '3247').

card_in_set('cycle of life', 'MIR').
card_original_type('cycle of life'/'MIR', 'Enchantment').
card_original_text('cycle of life'/'MIR', 'Return Cycle of Life to owner\'s hand: Target creature you summoned this turn is 0/1 until the beginning of your next upkeep. At the beginning of your next upkeep, put a +1/+1 counter on that creature.').
card_first_print('cycle of life', 'MIR').
card_image_name('cycle of life'/'MIR', 'cycle of life').
card_uid('cycle of life'/'MIR', 'MIR:Cycle of Life:cycle of life').
card_rarity('cycle of life'/'MIR', 'Rare').
card_artist('cycle of life'/'MIR', 'Chippy').
card_flavor_text('cycle of life'/'MIR', 'Thus does nature assert herself.').
card_multiverse_id('cycle of life'/'MIR', '3380').

card_in_set('daring apprentice', 'MIR').
card_original_type('daring apprentice'/'MIR', 'Summon — Wizard').
card_original_text('daring apprentice'/'MIR', '{T}, Sacrifice Daring Apprentice: Counter target spell.').
card_first_print('daring apprentice', 'MIR').
card_image_name('daring apprentice'/'MIR', 'daring apprentice').
card_uid('daring apprentice'/'MIR', 'MIR:Daring Apprentice:daring apprentice').
card_rarity('daring apprentice'/'MIR', 'Rare').
card_artist('daring apprentice'/'MIR', 'Kaja Foglio').
card_flavor_text('daring apprentice'/'MIR', 'In front of every great wizard is a doomed apprentice.').
card_multiverse_id('daring apprentice'/'MIR', '3331').

card_in_set('dark banishing', 'MIR').
card_original_type('dark banishing'/'MIR', 'Instant').
card_original_text('dark banishing'/'MIR', 'Bury target nonblack creature.').
card_image_name('dark banishing'/'MIR', 'dark banishing').
card_uid('dark banishing'/'MIR', 'MIR:Dark Banishing:dark banishing').
card_rarity('dark banishing'/'MIR', 'Common').
card_artist('dark banishing'/'MIR', 'Dermot Power').
card_flavor_text('dark banishing'/'MIR', '\"Femeref was guided by the wisdom of the Council of Voices. But in one terrible instant, they were slain, leaving Asmira as the sole voice of leadership.\"\n—Hakim, Loreweaver').
card_multiverse_id('dark banishing'/'MIR', '3284').

card_in_set('dark ritual', 'MIR').
card_original_type('dark ritual'/'MIR', 'Mana Source').
card_original_text('dark ritual'/'MIR', 'Add {B}{B}{B} to your mana pool.').
card_image_name('dark ritual'/'MIR', 'dark ritual').
card_uid('dark ritual'/'MIR', 'MIR:Dark Ritual:dark ritual').
card_rarity('dark ritual'/'MIR', 'Common').
card_artist('dark ritual'/'MIR', 'John Coulthart').
card_flavor_text('dark ritual'/'MIR', '\"The ceremony ended with a voice from the shadows, a voice with vast, ominous power.\"\n—Kifimbo, Shadow Guildmage').
card_multiverse_id('dark ritual'/'MIR', '3285').

card_in_set('dazzling beauty', 'MIR').
card_original_type('dazzling beauty'/'MIR', 'Instant').
card_original_text('dazzling beauty'/'MIR', 'Play only when defense is chosen.\nTarget unblocked creature is considered blocked.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_first_print('dazzling beauty', 'MIR').
card_image_name('dazzling beauty'/'MIR', 'dazzling beauty').
card_uid('dazzling beauty'/'MIR', 'MIR:Dazzling Beauty:dazzling beauty').
card_rarity('dazzling beauty'/'MIR', 'Common').
card_artist('dazzling beauty'/'MIR', 'Harold McNeill').
card_multiverse_id('dazzling beauty'/'MIR', '3483').

card_in_set('decomposition', 'MIR').
card_original_type('decomposition'/'MIR', 'Enchant Creature').
card_original_text('decomposition'/'MIR', 'Play only on a black creature.\nEnchanted creature gains \"Cumulative upkeep—1 life.\"\nIf enchanted creature is put into the graveyard, its controller loses 2 life.').
card_first_print('decomposition', 'MIR').
card_image_name('decomposition'/'MIR', 'decomposition').
card_uid('decomposition'/'MIR', 'MIR:Decomposition:decomposition').
card_rarity('decomposition'/'MIR', 'Uncommon').
card_artist('decomposition'/'MIR', 'Drew Tucker').
card_multiverse_id('decomposition'/'MIR', '3381').

card_in_set('delirium', 'MIR').
card_original_type('delirium'/'MIR', 'Instant').
card_original_text('delirium'/'MIR', 'Play only on target opponent\'s turn.\nTap target creature that player controls. That creature deals to the player an amount of damage equal to its power. The creature neither deals nor receives combat damage this turn.').
card_first_print('delirium', 'MIR').
card_image_name('delirium'/'MIR', 'delirium').
card_uid('delirium'/'MIR', 'MIR:Delirium:delirium').
card_rarity('delirium'/'MIR', 'Uncommon').
card_artist('delirium'/'MIR', 'Terese Nielsen').
card_multiverse_id('delirium'/'MIR', '3531').

card_in_set('dirtwater wraith', 'MIR').
card_original_type('dirtwater wraith'/'MIR', 'Summon — Wraith').
card_original_text('dirtwater wraith'/'MIR', 'Swampwalk (If defending player controls any swamps, this creature is unblockable.)\n{B} +1/+0 until end of turn.').
card_first_print('dirtwater wraith', 'MIR').
card_image_name('dirtwater wraith'/'MIR', 'dirtwater wraith').
card_uid('dirtwater wraith'/'MIR', 'MIR:Dirtwater Wraith:dirtwater wraith').
card_rarity('dirtwater wraith'/'MIR', 'Common').
card_artist('dirtwater wraith'/'MIR', 'Steve Luke').
card_flavor_text('dirtwater wraith'/'MIR', '\"The foul entities of the Uuserk swamp had never before dared to spread their influence to the very heart of Zhalfir.\"\n—Hakim, Loreweaver').
card_multiverse_id('dirtwater wraith'/'MIR', '3286').

card_in_set('discordant spirit', 'MIR').
card_original_type('discordant spirit'/'MIR', 'Summon — Spirit').
card_original_text('discordant spirit'/'MIR', 'At end of target opponent\'s turn, put a +1/+1 counter on Discordant Spirit for each 1 damage dealt to you this turn.\nAt end of your turn, remove all these counters from Discordant Spirit.').
card_first_print('discordant spirit', 'MIR').
card_image_name('discordant spirit'/'MIR', 'discordant spirit').
card_uid('discordant spirit'/'MIR', 'MIR:Discordant Spirit:discordant spirit').
card_rarity('discordant spirit'/'MIR', 'Rare').
card_artist('discordant spirit'/'MIR', 'Alan Rabinowitz').
card_multiverse_id('discordant spirit'/'MIR', '3532').

card_in_set('disempower', 'MIR').
card_original_type('disempower'/'MIR', 'Instant').
card_original_text('disempower'/'MIR', 'Put target artifact or enchantment on top of owner\'s library.').
card_first_print('disempower', 'MIR').
card_image_name('disempower'/'MIR', 'disempower').
card_uid('disempower'/'MIR', 'MIR:Disempower:disempower').
card_rarity('disempower'/'MIR', 'Common').
card_artist('disempower'/'MIR', 'John Matson').
card_flavor_text('disempower'/'MIR', '\"Mangara is gone. Who among you doubts that I have taken his power?\"\n—Kaervek').
card_multiverse_id('disempower'/'MIR', '3484').

card_in_set('disenchant', 'MIR').
card_original_type('disenchant'/'MIR', 'Instant').
card_original_text('disenchant'/'MIR', 'Destroy target artifact or enchantment.').
card_image_name('disenchant'/'MIR', 'disenchant').
card_uid('disenchant'/'MIR', 'MIR:Disenchant:disenchant').
card_rarity('disenchant'/'MIR', 'Common').
card_artist('disenchant'/'MIR', 'Charles Gillespie').
card_flavor_text('disenchant'/'MIR', '\"There are harsher ways to learn the meaning of the word ‘no.\'\"\n—Rashida Scalebane').
card_multiverse_id('disenchant'/'MIR', '3485').

card_in_set('dissipate', 'MIR').
card_original_type('dissipate'/'MIR', 'Interrupt').
card_original_text('dissipate'/'MIR', 'Counter target spell. Remove that spell card from the game.').
card_first_print('dissipate', 'MIR').
card_image_name('dissipate'/'MIR', 'dissipate').
card_uid('dissipate'/'MIR', 'MIR:Dissipate:dissipate').
card_rarity('dissipate'/'MIR', 'Uncommon').
card_artist('dissipate'/'MIR', 'Richard Kane Ferguson').
card_flavor_text('dissipate'/'MIR', '\"If you weren\'t born with it, you don\'t need it.\"\n—Grahilah, former trader of Amiqat').
card_multiverse_id('dissipate'/'MIR', '3332').

card_in_set('divine offering', 'MIR').
card_original_type('divine offering'/'MIR', 'Instant').
card_original_text('divine offering'/'MIR', 'Destroy target artifact. Gain an amount of life equal to that artifact\'s casting cost.').
card_image_name('divine offering'/'MIR', 'divine offering').
card_uid('divine offering'/'MIR', 'MIR:Divine Offering:divine offering').
card_rarity('divine offering'/'MIR', 'Common').
card_artist('divine offering'/'MIR', 'Terese Nielsen').
card_flavor_text('divine offering'/'MIR', '\"Suq\'Ata believe in trade; Femeref, alms; Zhalfirins, steel. Be warned, Kaervek—now we are joined.\"\n—Sidar Jabari').
card_multiverse_id('divine offering'/'MIR', '3486').

card_in_set('divine retribution', 'MIR').
card_original_type('divine retribution'/'MIR', 'Instant').
card_original_text('divine retribution'/'MIR', 'For each attacking creature, Divine Retribution deals 1 damage to target attacking creature.').
card_first_print('divine retribution', 'MIR').
card_image_name('divine retribution'/'MIR', 'divine retribution').
card_uid('divine retribution'/'MIR', 'MIR:Divine Retribution:divine retribution').
card_rarity('divine retribution'/'MIR', 'Rare').
card_artist('divine retribution'/'MIR', 'Charles Gillespie').
card_flavor_text('divine retribution'/'MIR', '\"To the undiscerning eye, justice and vengeance can look the same.\"\n—Rashida Scalebane').
card_multiverse_id('divine retribution'/'MIR', '3487').

card_in_set('drain life', 'MIR').
card_original_type('drain life'/'MIR', 'Sorcery').
card_original_text('drain life'/'MIR', 'For each {B} you spend in addition to the casting cost, Drain Life deals 1 damage to target creature or player. Gain 1 life for each 1 damage dealt, but not more than the toughness of the creature or the life total of the player Drain Life damages.').
card_image_name('drain life'/'MIR', 'drain life').
card_uid('drain life'/'MIR', 'MIR:Drain Life:drain life').
card_rarity('drain life'/'MIR', 'Common').
card_artist('drain life'/'MIR', 'Richard Kane Ferguson').
card_multiverse_id('drain life'/'MIR', '3287').

card_in_set('dread specter', 'MIR').
card_original_type('dread specter'/'MIR', 'Summon — Specter').
card_original_text('dread specter'/'MIR', 'Whenever Dread Specter blocks or is blocked by a nonblack creature, destroy that creature at end of combat.').
card_first_print('dread specter', 'MIR').
card_image_name('dread specter'/'MIR', 'dread specter').
card_uid('dread specter'/'MIR', 'MIR:Dread Specter:dread specter').
card_rarity('dread specter'/'MIR', 'Uncommon').
card_artist('dread specter'/'MIR', 'Kathryn Rathke').
card_flavor_text('dread specter'/'MIR', 'Whatever follows life is incurable.\n—Zhalfirin aphorism').
card_multiverse_id('dread specter'/'MIR', '3288').

card_in_set('dream cache', 'MIR').
card_original_type('dream cache'/'MIR', 'Sorcery').
card_original_text('dream cache'/'MIR', 'Draw three cards. Choose two cards from your hand and put both on either the top or the bottom of your library.').
card_first_print('dream cache', 'MIR').
card_image_name('dream cache'/'MIR', 'dream cache').
card_uid('dream cache'/'MIR', 'MIR:Dream Cache:dream cache').
card_rarity('dream cache'/'MIR', 'Common').
card_artist('dream cache'/'MIR', 'D. Alexander Gregory').
card_flavor_text('dream cache'/'MIR', 'Dreams\' riches are easily spent.\n—Suq\'Ata adage').
card_multiverse_id('dream cache'/'MIR', '3333').

card_in_set('dream fighter', 'MIR').
card_original_type('dream fighter'/'MIR', 'Summon — Soldier').
card_original_text('dream fighter'/'MIR', 'Whenever Dream Fighter blocks or is blocked by a creature, Dream Fighter and that creature phase out.').
card_first_print('dream fighter', 'MIR').
card_image_name('dream fighter'/'MIR', 'dream fighter').
card_uid('dream fighter'/'MIR', 'MIR:Dream Fighter:dream fighter').
card_rarity('dream fighter'/'MIR', 'Common').
card_artist('dream fighter'/'MIR', 'Drew Tucker').
card_flavor_text('dream fighter'/'MIR', 'Not only the sleeping should fear dreams.').
card_multiverse_id('dream fighter'/'MIR', '3334').

card_in_set('dwarven miner', 'MIR').
card_original_type('dwarven miner'/'MIR', 'Summon — Dwarf').
card_original_text('dwarven miner'/'MIR', '{2}{R}, {T}: Destroy target nonbasic land.').
card_first_print('dwarven miner', 'MIR').
card_image_name('dwarven miner'/'MIR', 'dwarven miner').
card_uid('dwarven miner'/'MIR', 'MIR:Dwarven Miner:dwarven miner').
card_rarity('dwarven miner'/'MIR', 'Uncommon').
card_artist('dwarven miner'/'MIR', 'JOCK').
card_flavor_text('dwarven miner'/'MIR', '\"Fetch the pestridder, Paka—we\'ve got dwarves in the rutabagas!\"\n—Jamul, Femeref farmer').
card_multiverse_id('dwarven miner'/'MIR', '3440').

card_in_set('dwarven nomad', 'MIR').
card_original_type('dwarven nomad'/'MIR', 'Summon — Dwarf').
card_original_text('dwarven nomad'/'MIR', '{T}: Target creature with power 2 or less is unblockable this turn.').
card_first_print('dwarven nomad', 'MIR').
card_image_name('dwarven nomad'/'MIR', 'dwarven nomad').
card_uid('dwarven nomad'/'MIR', 'MIR:Dwarven Nomad:dwarven nomad').
card_rarity('dwarven nomad'/'MIR', 'Common').
card_artist('dwarven nomad'/'MIR', 'Mike Kimble').
card_flavor_text('dwarven nomad'/'MIR', 'A dwarf\'s life belongs as much to the land as to the dwarf.\n—Creed of the Stonethrow Clan').
card_multiverse_id('dwarven nomad'/'MIR', '3441').

card_in_set('early harvest', 'MIR').
card_original_type('early harvest'/'MIR', 'Instant').
card_original_text('early harvest'/'MIR', 'Target player untaps all basic lands he or she controls.').
card_first_print('early harvest', 'MIR').
card_image_name('early harvest'/'MIR', 'early harvest').
card_uid('early harvest'/'MIR', 'MIR:Early Harvest:early harvest').
card_rarity('early harvest'/'MIR', 'Rare').
card_artist('early harvest'/'MIR', 'Janine Johnston').
card_flavor_text('early harvest'/'MIR', '\"Tonight we\'ll eat a farewell feast. Cold corn porridge is not enough. / Let\'s peel papayas, pineapples, and mangoes, drink coconut milk, / and bake bananas.\"\n—\"Love Song of Night and Day\"').
card_multiverse_id('early harvest'/'MIR', '3382').

card_in_set('ebony charm', 'MIR').
card_original_type('ebony charm'/'MIR', 'Instant').
card_original_text('ebony charm'/'MIR', 'Choose one Target opponent loses 1 life and you gain 1 life; or remove from the game up to three cards in any player\'s graveyard; or target creature cannot be blocked this turn except by artifact or black creatures.').
card_first_print('ebony charm', 'MIR').
card_image_name('ebony charm'/'MIR', 'ebony charm').
card_uid('ebony charm'/'MIR', 'MIR:Ebony Charm:ebony charm').
card_rarity('ebony charm'/'MIR', 'Common').
card_artist('ebony charm'/'MIR', 'Gerry Grace').
card_multiverse_id('ebony charm'/'MIR', '3289').

card_in_set('ekundu cyclops', 'MIR').
card_original_type('ekundu cyclops'/'MIR', 'Summon — Cyclops').
card_original_text('ekundu cyclops'/'MIR', 'If any creature you control attacks, Ekundu Cyclops also attacks if able.').
card_first_print('ekundu cyclops', 'MIR').
card_image_name('ekundu cyclops'/'MIR', 'ekundu cyclops').
card_uid('ekundu cyclops'/'MIR', 'MIR:Ekundu Cyclops:ekundu cyclops').
card_rarity('ekundu cyclops'/'MIR', 'Common').
card_artist('ekundu cyclops'/'MIR', 'Robert Bliss').
card_flavor_text('ekundu cyclops'/'MIR', '\"Big one I,\nOne big eye.\nOne-eye see\nTwo-eye die!\"\n—Traditional cyclops chant').
card_multiverse_id('ekundu cyclops'/'MIR', '3442').

card_in_set('ekundu griffin', 'MIR').
card_original_type('ekundu griffin'/'MIR', 'Summon — Griffin').
card_original_text('ekundu griffin'/'MIR', 'Flying, first strike').
card_first_print('ekundu griffin', 'MIR').
card_image_name('ekundu griffin'/'MIR', 'ekundu griffin').
card_uid('ekundu griffin'/'MIR', 'MIR:Ekundu Griffin:ekundu griffin').
card_rarity('ekundu griffin'/'MIR', 'Common').
card_artist('ekundu griffin'/'MIR', 'David A. Cherry').
card_flavor_text('ekundu griffin'/'MIR', '\"My goat was a small price to pay for the chance to see the hunting griffin dive and seize it.\"\n—Pashad ibn Asim, Suq\'Ata trader').
card_multiverse_id('ekundu griffin'/'MIR', '3488').

card_in_set('elixir of vitality', 'MIR').
card_original_type('elixir of vitality'/'MIR', 'Artifact').
card_original_text('elixir of vitality'/'MIR', 'Elixir of Vitality comes into play tapped.\n{T}, Sacrifice Elixir of Vitality: Gain 4 life.\n{8}, {T}, Sacrifice Elixir of Vitality: Gain 8 life.').
card_first_print('elixir of vitality', 'MIR').
card_image_name('elixir of vitality'/'MIR', 'elixir of vitality').
card_uid('elixir of vitality'/'MIR', 'MIR:Elixir of Vitality:elixir of vitality').
card_rarity('elixir of vitality'/'MIR', 'Uncommon').
card_artist('elixir of vitality'/'MIR', 'Douglas Shuler').
card_flavor_text('elixir of vitality'/'MIR', '\"Eternal life or your money back.\"\n—Unnamed Suq\'Ata merchant, deceased').
card_multiverse_id('elixir of vitality'/'MIR', '3248').

card_in_set('emberwilde caliph', 'MIR').
card_original_type('emberwilde caliph'/'MIR', 'Summon — Djinn').
card_original_text('emberwilde caliph'/'MIR', 'Flying, trample\nEmberwilde Caliph attacks each turn if able.\nFor each 1 damage Emberwilde Caliph successfully deals, lose 1 life.').
card_first_print('emberwilde caliph', 'MIR').
card_image_name('emberwilde caliph'/'MIR', 'emberwilde caliph').
card_uid('emberwilde caliph'/'MIR', 'MIR:Emberwilde Caliph:emberwilde caliph').
card_rarity('emberwilde caliph'/'MIR', 'Rare').
card_artist('emberwilde caliph'/'MIR', 'Jennifer Law').
card_flavor_text('emberwilde caliph'/'MIR', 'The fire breathes while the air burns.').
card_multiverse_id('emberwilde caliph'/'MIR', '3533').

card_in_set('emberwilde djinn', 'MIR').
card_original_type('emberwilde djinn'/'MIR', 'Summon — Djinn').
card_original_text('emberwilde djinn'/'MIR', 'Flying\nDuring each player\'s upkeep, he or she may pay {R}{R} or 2 life to gain control of Emberwilde Djinn.').
card_first_print('emberwilde djinn', 'MIR').
card_image_name('emberwilde djinn'/'MIR', 'emberwilde djinn').
card_uid('emberwilde djinn'/'MIR', 'MIR:Emberwilde Djinn:emberwilde djinn').
card_rarity('emberwilde djinn'/'MIR', 'Rare').
card_artist('emberwilde djinn'/'MIR', 'Mike Dringenberg').
card_flavor_text('emberwilde djinn'/'MIR', 'A hefty price is no guarantee of quality merchandise.').
card_multiverse_id('emberwilde djinn'/'MIR', '3443').

card_in_set('energy bolt', 'MIR').
card_original_type('energy bolt'/'MIR', 'Sorcery').
card_original_text('energy bolt'/'MIR', 'Energy Bolt deals X damage to target player, or target player gains X life.').
card_first_print('energy bolt', 'MIR').
card_image_name('energy bolt'/'MIR', 'energy bolt').
card_uid('energy bolt'/'MIR', 'MIR:Energy Bolt:energy bolt').
card_rarity('energy bolt'/'MIR', 'Rare').
card_artist('energy bolt'/'MIR', 'Scott Kirschner').
card_flavor_text('energy bolt'/'MIR', 'Life and death are two faces of the same coin.').
card_multiverse_id('energy bolt'/'MIR', '3534').

card_in_set('energy vortex', 'MIR').
card_original_type('energy vortex'/'MIR', 'Enchantment').
card_original_text('energy vortex'/'MIR', 'When you play Energy Vortex, choose target opponent.\nAt the beginning of your upkeep, remove all energy counters from Energy Vortex.\nDuring chosen opponent\'s upkeep, he or she pays {1} for each energy counter on Energy Vortex, or it deals 3 damage to him or her.\n{X} Put X energy counters on Energy Vortex. Use this ability only during your upkeep.').
card_first_print('energy vortex', 'MIR').
card_image_name('energy vortex'/'MIR', 'energy vortex').
card_uid('energy vortex'/'MIR', 'MIR:Energy Vortex:energy vortex').
card_rarity('energy vortex'/'MIR', 'Rare').
card_artist('energy vortex'/'MIR', 'Tom Wänerstrand').
card_multiverse_id('energy vortex'/'MIR', '3335').

card_in_set('enfeeblement', 'MIR').
card_original_type('enfeeblement'/'MIR', 'Enchant Creature').
card_original_text('enfeeblement'/'MIR', 'Enchanted creature gets -2/-2.').
card_first_print('enfeeblement', 'MIR').
card_image_name('enfeeblement'/'MIR', 'enfeeblement').
card_uid('enfeeblement'/'MIR', 'MIR:Enfeeblement:enfeeblement').
card_rarity('enfeeblement'/'MIR', 'Common').
card_artist('enfeeblement'/'MIR', 'John Bolton').
card_flavor_text('enfeeblement'/'MIR', '\"If it is weak, either kill it or ignore it. Anything else honors it.\"\n—Kaervek').
card_multiverse_id('enfeeblement'/'MIR', '3290').

card_in_set('enlightened tutor', 'MIR').
card_original_type('enlightened tutor'/'MIR', 'Instant').
card_original_text('enlightened tutor'/'MIR', 'Search your library for an artifact or enchantment card and reveal that card to all players. Shuffle your library and put the revealed card back on top of it.').
card_image_name('enlightened tutor'/'MIR', 'enlightened tutor').
card_uid('enlightened tutor'/'MIR', 'MIR:Enlightened Tutor:enlightened tutor').
card_rarity('enlightened tutor'/'MIR', 'Uncommon').
card_artist('enlightened tutor'/'MIR', 'Dan Frazier').
card_flavor_text('enlightened tutor'/'MIR', '\"I do not teach. I simply reveal.\"\n—Daudi, Femeref tutor').
card_multiverse_id('enlightened tutor'/'MIR', '3489').

card_in_set('ersatz gnomes', 'MIR').
card_original_type('ersatz gnomes'/'MIR', 'Artifact Creature').
card_original_text('ersatz gnomes'/'MIR', '{T}: Target spell is colorless. Play this ability as an interrupt.\n{T}: Target permanent is colorless until end of turn.').
card_first_print('ersatz gnomes', 'MIR').
card_image_name('ersatz gnomes'/'MIR', 'ersatz gnomes').
card_uid('ersatz gnomes'/'MIR', 'MIR:Ersatz Gnomes:ersatz gnomes').
card_rarity('ersatz gnomes'/'MIR', 'Uncommon').
card_artist('ersatz gnomes'/'MIR', 'Ron Spencer').
card_flavor_text('ersatz gnomes'/'MIR', 'From jungle to sea, from sea to stone, from stone to field, from field to bone. What am I? —Zhalfirin riddle').
card_multiverse_id('ersatz gnomes'/'MIR', '3249').

card_in_set('ether well', 'MIR').
card_original_type('ether well'/'MIR', 'Instant').
card_original_text('ether well'/'MIR', 'Put target creature on top of owner\'s library. If that creature is red, you may choose to put it on the bottom of owner\'s library instead.').
card_first_print('ether well', 'MIR').
card_image_name('ether well'/'MIR', 'ether well').
card_uid('ether well'/'MIR', 'MIR:Ether Well:ether well').
card_rarity('ether well'/'MIR', 'Uncommon').
card_artist('ether well'/'MIR', 'Charles Gillespie').
card_flavor_text('ether well'/'MIR', '\"Whoa—my tail will never fit through that hole!\"').
card_multiverse_id('ether well'/'MIR', '3336').

card_in_set('ethereal champion', 'MIR').
card_original_type('ethereal champion'/'MIR', 'Summon — Avatar').
card_original_text('ethereal champion'/'MIR', 'Pay 1 life: Prevent 1 damage to Ethereal Champion.').
card_first_print('ethereal champion', 'MIR').
card_image_name('ethereal champion'/'MIR', 'ethereal champion').
card_uid('ethereal champion'/'MIR', 'MIR:Ethereal Champion:ethereal champion').
card_rarity('ethereal champion'/'MIR', 'Rare').
card_artist('ethereal champion'/'MIR', 'Terese Nielsen').
card_flavor_text('ethereal champion'/'MIR', '\"If I have learned anything as a wizard it is this: never fight your own battles.\"\n—Waffa, sorcerer of Nyomba').
card_multiverse_id('ethereal champion'/'MIR', '3490').

card_in_set('fallow earth', 'MIR').
card_original_type('fallow earth'/'MIR', 'Sorcery').
card_original_text('fallow earth'/'MIR', 'Put target land on top of owner\'s library.').
card_first_print('fallow earth', 'MIR').
card_image_name('fallow earth'/'MIR', 'fallow earth').
card_uid('fallow earth'/'MIR', 'MIR:Fallow Earth:fallow earth').
card_rarity('fallow earth'/'MIR', 'Uncommon').
card_artist('fallow earth'/'MIR', 'Janine Johnston').
card_flavor_text('fallow earth'/'MIR', '\". . . and when the farmer awoke the next morning, all the seeds from his field were once again in their sacks.\"\n—Afari, Tales').
card_multiverse_id('fallow earth'/'MIR', '3383').

card_in_set('favorable destiny', 'MIR').
card_original_type('favorable destiny'/'MIR', 'Enchant Creature').
card_original_text('favorable destiny'/'MIR', 'As long as enchanted creature\'s controller controls at least one other creature, enchanted creature cannot be the target of spells or effects.\nAs long as enchanted creature is white, it gets +1/+2.').
card_first_print('favorable destiny', 'MIR').
card_image_name('favorable destiny'/'MIR', 'favorable destiny').
card_uid('favorable destiny'/'MIR', 'MIR:Favorable Destiny:favorable destiny').
card_rarity('favorable destiny'/'MIR', 'Uncommon').
card_artist('favorable destiny'/'MIR', 'Thomas Gianni').
card_multiverse_id('favorable destiny'/'MIR', '3491').

card_in_set('femeref archers', 'MIR').
card_original_type('femeref archers'/'MIR', 'Summon — Archers').
card_original_text('femeref archers'/'MIR', '{T}: Femeref Archers deals 4 damage to target attacking creature with flying.').
card_first_print('femeref archers', 'MIR').
card_image_name('femeref archers'/'MIR', 'femeref archers').
card_uid('femeref archers'/'MIR', 'MIR:Femeref Archers:femeref archers').
card_rarity('femeref archers'/'MIR', 'Uncommon').
card_artist('femeref archers'/'MIR', 'William Donohoe').
card_flavor_text('femeref archers'/'MIR', '\"They say a Zhalfirin archer can split the eye of a griffin. Nonsense, of course: they have no faith to guide their darts.\"\n—Nuru, Femeref archer').
card_multiverse_id('femeref archers'/'MIR', '3384').

card_in_set('femeref healer', 'MIR').
card_original_type('femeref healer'/'MIR', 'Summon — Cleric').
card_original_text('femeref healer'/'MIR', '{T}: Prevent 1 damage to any creature or player.').
card_first_print('femeref healer', 'MIR').
card_image_name('femeref healer'/'MIR', 'femeref healer').
card_uid('femeref healer'/'MIR', 'MIR:Femeref Healer:femeref healer').
card_rarity('femeref healer'/'MIR', 'Common').
card_artist('femeref healer'/'MIR', 'Steve Luke').
card_flavor_text('femeref healer'/'MIR', '\"Faith is my shield, and hope is my armor; I am vulnerable only to myself.\"\n—Asmira, Holy Avenger').
card_multiverse_id('femeref healer'/'MIR', '3492').

card_in_set('femeref knight', 'MIR').
card_original_type('femeref knight'/'MIR', 'Summon — Knight').
card_original_text('femeref knight'/'MIR', 'Flanking (Whenever a creature without flanking is assigned to block this creature, the blocking creature gets -1/-1 until end of turn.)\n{W} Attacking does not cause Femeref Knight to tap this turn.').
card_first_print('femeref knight', 'MIR').
card_image_name('femeref knight'/'MIR', 'femeref knight').
card_uid('femeref knight'/'MIR', 'MIR:Femeref Knight:femeref knight').
card_rarity('femeref knight'/'MIR', 'Common').
card_artist('femeref knight'/'MIR', 'Tony Roberts').
card_flavor_text('femeref knight'/'MIR', '\"I will return / with lizard skins for your sandals. Paint your eyes black and wait for me.\" —\"Love Song of Night and Day\"').
card_multiverse_id('femeref knight'/'MIR', '3493').

card_in_set('femeref scouts', 'MIR').
card_original_type('femeref scouts'/'MIR', 'Summon — Scouts').
card_original_text('femeref scouts'/'MIR', '').
card_first_print('femeref scouts', 'MIR').
card_image_name('femeref scouts'/'MIR', 'femeref scouts').
card_uid('femeref scouts'/'MIR', 'MIR:Femeref Scouts:femeref scouts').
card_rarity('femeref scouts'/'MIR', 'Common').
card_artist('femeref scouts'/'MIR', 'Zak Plucinski').
card_flavor_text('femeref scouts'/'MIR', '\"The days before the war were filled with strange portents: scouts and herders spoke of villages reduced to ash, of farmers taken from their fields, and of foul beasts roaming the plains in packs. I know, for I spoke to them all.\"\n—Hakim, Loreweaver').
card_multiverse_id('femeref scouts'/'MIR', '3494').

card_in_set('feral shadow', 'MIR').
card_original_type('feral shadow'/'MIR', 'Summon — Night Stalker').
card_original_text('feral shadow'/'MIR', 'Flying').
card_first_print('feral shadow', 'MIR').
card_image_name('feral shadow'/'MIR', 'feral shadow').
card_uid('feral shadow'/'MIR', 'MIR:Feral Shadow:feral shadow').
card_rarity('feral shadow'/'MIR', 'Common').
card_artist('feral shadow'/'MIR', 'Cliff Nielsen').
card_flavor_text('feral shadow'/'MIR', '\"The night stalkers are such a minor power in Urborg. Their sad eagerness to elevate themselves makes them willing tools in the plundering of little Femeref.\"\n—Kaervek').
card_multiverse_id('feral shadow'/'MIR', '3291').

card_in_set('fetid horror', 'MIR').
card_original_type('fetid horror'/'MIR', 'Summon — Shade').
card_original_text('fetid horror'/'MIR', '{B} +1/+1 until end of turn').
card_first_print('fetid horror', 'MIR').
card_image_name('fetid horror'/'MIR', 'fetid horror').
card_uid('fetid horror'/'MIR', 'MIR:Fetid Horror:fetid horror').
card_rarity('fetid horror'/'MIR', 'Common').
card_artist('fetid horror'/'MIR', 'Gary Leach').
card_flavor_text('fetid horror'/'MIR', '\"Of the six who went down the Uuserek Trail to scout ahead, one returned. She clawed at her eyes and nostrils and sobbed with horror. I was curious about what she saw, but we chose another path.\"\n—Scout Ekemet, final journal').
card_multiverse_id('fetid horror'/'MIR', '3292').

card_in_set('final fortune', 'MIR').
card_original_type('final fortune'/'MIR', 'Instant').
card_original_text('final fortune'/'MIR', 'Take another turn after this one. You lose the game at the end of that turn.').
card_first_print('final fortune', 'MIR').
card_image_name('final fortune'/'MIR', 'final fortune').
card_uid('final fortune'/'MIR', 'MIR:Final Fortune:final fortune').
card_rarity('final fortune'/'MIR', 'Rare').
card_artist('final fortune'/'MIR', 'D. Alexander Gregory').
card_flavor_text('final fortune'/'MIR', 'Want all, lose all.\n—Zhalfirin aphorism').
card_multiverse_id('final fortune'/'MIR', '3444').

card_in_set('fire diamond', 'MIR').
card_original_type('fire diamond'/'MIR', 'Artifact').
card_original_text('fire diamond'/'MIR', 'Fire Diamond comes into play tapped.\n{T}: Add {R} to your mana pool. Play this ability as a mana source.').
card_first_print('fire diamond', 'MIR').
card_image_name('fire diamond'/'MIR', 'fire diamond').
card_uid('fire diamond'/'MIR', 'MIR:Fire Diamond:fire diamond').
card_rarity('fire diamond'/'MIR', 'Uncommon').
card_artist('fire diamond'/'MIR', 'Richard Thomas').
card_multiverse_id('fire diamond'/'MIR', '3250').

card_in_set('firebreathing', 'MIR').
card_original_type('firebreathing'/'MIR', 'Enchant Creature').
card_original_text('firebreathing'/'MIR', '{R} Enchanted creature gets +1/+0 until end of turn.').
card_image_name('firebreathing'/'MIR', 'firebreathing').
card_uid('firebreathing'/'MIR', 'MIR:Firebreathing:firebreathing').
card_rarity('firebreathing'/'MIR', 'Common').
card_artist('firebreathing'/'MIR', 'Mike Kerr').
card_flavor_text('firebreathing'/'MIR', '\"Magic\'s only part of it, my friend. Diet does the rest.\"\n—Pashad ibn Asim, Suq\'Ata trader').
card_multiverse_id('firebreathing'/'MIR', '3445').

card_in_set('flame elemental', 'MIR').
card_original_type('flame elemental'/'MIR', 'Summon — Elemental').
card_original_text('flame elemental'/'MIR', '{R}, {T}, Sacrifice Flame Elemental: Flame Elemental deals an amount of damage equal to its power to target creature.').
card_first_print('flame elemental', 'MIR').
card_image_name('flame elemental'/'MIR', 'flame elemental').
card_uid('flame elemental'/'MIR', 'MIR:Flame Elemental:flame elemental').
card_rarity('flame elemental'/'MIR', 'Uncommon').
card_artist('flame elemental'/'MIR', 'Richard Kane Ferguson').
card_flavor_text('flame elemental'/'MIR', 'Storyteller: By my hand in the fire, this story is a tale of power.\nListeners: May it give you strength.').
card_multiverse_id('flame elemental'/'MIR', '3446').

card_in_set('flare', 'MIR').
card_original_type('flare'/'MIR', 'Instant').
card_original_text('flare'/'MIR', 'Flare deals 1 damage to target creature or player.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_image_name('flare'/'MIR', 'flare').
card_uid('flare'/'MIR', 'MIR:Flare:flare').
card_rarity('flare'/'MIR', 'Common').
card_artist('flare'/'MIR', 'Greg Simanson').
card_flavor_text('flare'/'MIR', '\"In the forest, fires light the sky as black clouds unfold their weight.\"\n—\"Love Song of Night and Day\"').
card_multiverse_id('flare'/'MIR', '3447').

card_in_set('flash', 'MIR').
card_original_type('flash'/'MIR', 'Instant').
card_original_text('flash'/'MIR', 'Choose a creature card from your hand and put that creature into play as though it were just played. Pay the creature\'s casting cost reduced by up to {2}. If you cannot, bury the creature.').
card_first_print('flash', 'MIR').
card_image_name('flash'/'MIR', 'flash').
card_uid('flash'/'MIR', 'MIR:Flash:flash').
card_rarity('flash'/'MIR', 'Rare').
card_artist('flash'/'MIR', 'David Ho').
card_multiverse_id('flash'/'MIR', '3337').

card_in_set('flood plain', 'MIR').
card_original_type('flood plain'/'MIR', 'Land').
card_original_text('flood plain'/'MIR', 'Flood Plain comes into play tapped.\n{T}, Sacrifice Flood Plain: Search your library for a plains or island card. Put that land into play. Shuffle your library afterwards.').
card_first_print('flood plain', 'MIR').
card_image_name('flood plain'/'MIR', 'flood plain').
card_uid('flood plain'/'MIR', 'MIR:Flood Plain:flood plain').
card_rarity('flood plain'/'MIR', 'Uncommon').
card_artist('flood plain'/'MIR', 'Pat Morrissey').
card_multiverse_id('flood plain'/'MIR', '3572').

card_in_set('floodgate', 'MIR').
card_original_type('floodgate'/'MIR', 'Summon — Wall').
card_original_text('floodgate'/'MIR', 'If Floodgate gains flying, bury it.\nIf Floodgate leaves play, it deals to each nonblue creature without flying 1 damage for each two islands you control.').
card_first_print('floodgate', 'MIR').
card_image_name('floodgate'/'MIR', 'floodgate').
card_uid('floodgate'/'MIR', 'MIR:Floodgate:floodgate').
card_rarity('floodgate'/'MIR', 'Uncommon').
card_artist('floodgate'/'MIR', 'Jeff Miracola').
card_flavor_text('floodgate'/'MIR', '\"Quick, Rhirhok—teach me to swim!\"\n—Makht, goblin casualty').
card_multiverse_id('floodgate'/'MIR', '3338').

card_in_set('fog', 'MIR').
card_original_type('fog'/'MIR', 'Instant').
card_original_text('fog'/'MIR', 'Creatures deal no combat damage this turn.').
card_image_name('fog'/'MIR', 'fog').
card_uid('fog'/'MIR', 'MIR:Fog:fog').
card_rarity('fog'/'MIR', 'Common').
card_artist('fog'/'MIR', 'Harold McNeill').
card_flavor_text('fog'/'MIR', '\"More armies have been lost in the confusion of the jungle mists than to any battle.\"\n—Jolrael').
card_multiverse_id('fog'/'MIR', '3385').

card_in_set('foratog', 'MIR').
card_original_type('foratog'/'MIR', 'Summon — Atog').
card_original_text('foratog'/'MIR', '{G}, Sacrifice a forest: +2/+2 until end of turn').
card_first_print('foratog', 'MIR').
card_image_name('foratog'/'MIR', 'foratog').
card_uid('foratog'/'MIR', 'MIR:Foratog:foratog').
card_rarity('foratog'/'MIR', 'Uncommon').
card_artist('foratog'/'MIR', 'Mark Poole').
card_flavor_text('foratog'/'MIR', 'Five hundred years to grow—barely a minute to eat.').
card_multiverse_id('foratog'/'MIR', '3386').

card_in_set('forbidden crypt', 'MIR').
card_original_type('forbidden crypt'/'MIR', 'Enchantment').
card_original_text('forbidden crypt'/'MIR', 'For each card you would draw, instead choose target card in your graveyard and put it into your hand. If you cannot, you lose the game.\nWhenever a card is put into your graveyard, remove that card from the game.').
card_first_print('forbidden crypt', 'MIR').
card_image_name('forbidden crypt'/'MIR', 'forbidden crypt').
card_uid('forbidden crypt'/'MIR', 'MIR:Forbidden Crypt:forbidden crypt').
card_rarity('forbidden crypt'/'MIR', 'Rare').
card_artist('forbidden crypt'/'MIR', 'D. Alexander Gregory').
card_multiverse_id('forbidden crypt'/'MIR', '3293').

card_in_set('forest', 'MIR').
card_original_type('forest'/'MIR', 'Land').
card_original_text('forest'/'MIR', '{T}: Add {G} to your mana pool.').
card_image_name('forest'/'MIR', 'forest1').
card_uid('forest'/'MIR', 'MIR:Forest:forest1').
card_rarity('forest'/'MIR', 'Basic Land').
card_artist('forest'/'MIR', 'Tony Roberts').
card_multiverse_id('forest'/'MIR', '3569').

card_in_set('forest', 'MIR').
card_original_type('forest'/'MIR', 'Land').
card_original_text('forest'/'MIR', '{T}: Add {G} to your mana pool.').
card_image_name('forest'/'MIR', 'forest2').
card_uid('forest'/'MIR', 'MIR:Forest:forest2').
card_rarity('forest'/'MIR', 'Basic Land').
card_artist('forest'/'MIR', 'Tony Roberts').
card_multiverse_id('forest'/'MIR', '3567').

card_in_set('forest', 'MIR').
card_original_type('forest'/'MIR', 'Land').
card_original_text('forest'/'MIR', '{T}: Add {G} to your mana pool.').
card_image_name('forest'/'MIR', 'forest3').
card_uid('forest'/'MIR', 'MIR:Forest:forest3').
card_rarity('forest'/'MIR', 'Basic Land').
card_artist('forest'/'MIR', 'Tony Roberts').
card_multiverse_id('forest'/'MIR', '3568').

card_in_set('forest', 'MIR').
card_original_type('forest'/'MIR', 'Land').
card_original_text('forest'/'MIR', '{T}: Add {G} to your mana pool.').
card_image_name('forest'/'MIR', 'forest4').
card_uid('forest'/'MIR', 'MIR:Forest:forest4').
card_rarity('forest'/'MIR', 'Basic Land').
card_artist('forest'/'MIR', 'Tony Roberts').
card_multiverse_id('forest'/'MIR', '3566').

card_in_set('forsaken wastes', 'MIR').
card_original_type('forsaken wastes'/'MIR', 'Enchant World').
card_original_text('forsaken wastes'/'MIR', 'Players cannot gain life.\nDuring each player\'s upkeep, that player loses 1 life.\nIf Forsaken Wastes is the target of a successfully cast spell, that spell\'s caster loses 5 life.').
card_first_print('forsaken wastes', 'MIR').
card_image_name('forsaken wastes'/'MIR', 'forsaken wastes').
card_uid('forsaken wastes'/'MIR', 'MIR:Forsaken Wastes:forsaken wastes').
card_rarity('forsaken wastes'/'MIR', 'Rare').
card_artist('forsaken wastes'/'MIR', 'Kev Walker').
card_multiverse_id('forsaken wastes'/'MIR', '3294').

card_in_set('frenetic efreet', 'MIR').
card_original_type('frenetic efreet'/'MIR', 'Summon — Efreet').
card_original_text('frenetic efreet'/'MIR', 'Flying\n{0}: Flip a coin; target opponent calls heads or tails while coin is in the air. If the flip ends up in your favor, Frenetic Efreet phases out. Otherwise, bury Frenetic Efreet.').
card_first_print('frenetic efreet', 'MIR').
card_image_name('frenetic efreet'/'MIR', 'frenetic efreet').
card_uid('frenetic efreet'/'MIR', 'MIR:Frenetic Efreet:frenetic efreet').
card_rarity('frenetic efreet'/'MIR', 'Rare').
card_artist('frenetic efreet'/'MIR', 'Thomas Gianni').
card_multiverse_id('frenetic efreet'/'MIR', '3535').

card_in_set('giant mantis', 'MIR').
card_original_type('giant mantis'/'MIR', 'Summon — Mantis').
card_original_text('giant mantis'/'MIR', 'Giant Mantis can block creatures with flying.').
card_first_print('giant mantis', 'MIR').
card_image_name('giant mantis'/'MIR', 'giant mantis').
card_uid('giant mantis'/'MIR', 'MIR:Giant Mantis:giant mantis').
card_rarity('giant mantis'/'MIR', 'Common').
card_artist('giant mantis'/'MIR', 'Randy Gallegos').
card_flavor_text('giant mantis'/'MIR', '\"I hate insects of every sort. The only mercy is that they are generally small.\"\n—Mwani, Mtenda goatherd').
card_multiverse_id('giant mantis'/'MIR', '3387').

card_in_set('gibbering hyenas', 'MIR').
card_original_type('gibbering hyenas'/'MIR', 'Summon — Hyenas').
card_original_text('gibbering hyenas'/'MIR', 'Gibbering Hyenas cannot block black creatures.').
card_first_print('gibbering hyenas', 'MIR').
card_image_name('gibbering hyenas'/'MIR', 'gibbering hyenas').
card_uid('gibbering hyenas'/'MIR', 'MIR:Gibbering Hyenas:gibbering hyenas').
card_rarity('gibbering hyenas'/'MIR', 'Common').
card_artist('gibbering hyenas'/'MIR', 'Una Fricker').
card_flavor_text('gibbering hyenas'/'MIR', 'Hyenas laughing—what\'s the joke?\n—Femeref phrase meaning\n\"that\'s not funny\"').
card_multiverse_id('gibbering hyenas'/'MIR', '3388').

card_in_set('goblin elite infantry', 'MIR').
card_original_type('goblin elite infantry'/'MIR', 'Summon — Goblins').
card_original_text('goblin elite infantry'/'MIR', 'If Goblin Elite Infantry blocks or is blocked, it gets -1/-1 until end of turn.').
card_first_print('goblin elite infantry', 'MIR').
card_image_name('goblin elite infantry'/'MIR', 'goblin elite infantry').
card_uid('goblin elite infantry'/'MIR', 'MIR:Goblin Elite Infantry:goblin elite infantry').
card_rarity('goblin elite infantry'/'MIR', 'Common').
card_artist('goblin elite infantry'/'MIR', 'Robert Bliss').
card_flavor_text('goblin elite infantry'/'MIR', 'They talk a good fight.').
card_multiverse_id('goblin elite infantry'/'MIR', '3448').

card_in_set('goblin scouts', 'MIR').
card_original_type('goblin scouts'/'MIR', 'Sorcery').
card_original_text('goblin scouts'/'MIR', 'Put three Goblin Scout tokens into play. Treat these tokens as 1/1 red creatures with mountainwalk that count as Goblins (if defending player controls any mountains, these creatures are unblockable).').
card_first_print('goblin scouts', 'MIR').
card_image_name('goblin scouts'/'MIR', 'goblin scouts').
card_uid('goblin scouts'/'MIR', 'MIR:Goblin Scouts:goblin scouts').
card_rarity('goblin scouts'/'MIR', 'Uncommon').
card_artist('goblin scouts'/'MIR', 'Geofrey Darrow').
card_flavor_text('goblin scouts'/'MIR', '\"Pathetic—like I wouldn\'t know a goblin painted up to look like a dwarf!\"\n—Pashad ibn Asim, Suq\'Ata trader').
card_multiverse_id('goblin scouts'/'MIR', '3449').

card_in_set('goblin soothsayer', 'MIR').
card_original_type('goblin soothsayer'/'MIR', 'Summon — Goblin').
card_original_text('goblin soothsayer'/'MIR', '{R}, {T}, Sacrifice a Goblin: All red creatures get +1/+1 until end of turn.').
card_first_print('goblin soothsayer', 'MIR').
card_image_name('goblin soothsayer'/'MIR', 'goblin soothsayer').
card_uid('goblin soothsayer'/'MIR', 'MIR:Goblin Soothsayer:goblin soothsayer').
card_rarity('goblin soothsayer'/'MIR', 'Uncommon').
card_artist('goblin soothsayer'/'MIR', 'Robert Bliss').
card_flavor_text('goblin soothsayer'/'MIR', '\"I see a great victory and rivers of blood. And . . . hmm, looks like a spleen.\"\n—Grishnak, goblin soothsayer').
card_multiverse_id('goblin soothsayer'/'MIR', '3450').

card_in_set('goblin tinkerer', 'MIR').
card_original_type('goblin tinkerer'/'MIR', 'Summon — Goblin').
card_original_text('goblin tinkerer'/'MIR', '{R}, {T}: Destroy target artifact. That artifact deals an amount of damage equal to its casting cost to Goblin Tinkerer.').
card_first_print('goblin tinkerer', 'MIR').
card_image_name('goblin tinkerer'/'MIR', 'goblin tinkerer').
card_uid('goblin tinkerer'/'MIR', 'MIR:Goblin Tinkerer:goblin tinkerer').
card_rarity('goblin tinkerer'/'MIR', 'Common').
card_artist('goblin tinkerer'/'MIR', 'Hannibal King').
card_flavor_text('goblin tinkerer'/'MIR', '\"Can they do that?\"\n—Imwita, Zhalfirin artificer, last words').
card_multiverse_id('goblin tinkerer'/'MIR', '3451').

card_in_set('granger guildmage', 'MIR').
card_original_type('granger guildmage'/'MIR', 'Summon — Wizard').
card_original_text('granger guildmage'/'MIR', '{W}, {T}: Target creature gains first strike until end of turn.\n{R}, {T}: Granger Guildmage deals 1 damage to target creature or player and 1 damage to you.').
card_first_print('granger guildmage', 'MIR').
card_image_name('granger guildmage'/'MIR', 'granger guildmage').
card_uid('granger guildmage'/'MIR', 'MIR:Granger Guildmage:granger guildmage').
card_rarity('granger guildmage'/'MIR', 'Common').
card_artist('granger guildmage'/'MIR', 'Dan Frazier').
card_flavor_text('granger guildmage'/'MIR', 'Leave no mouth agape, no stomach unfilled.\n—Granger Guild maxim').
card_multiverse_id('granger guildmage'/'MIR', '3389').

card_in_set('grasslands', 'MIR').
card_original_type('grasslands'/'MIR', 'Land').
card_original_text('grasslands'/'MIR', 'Grasslands comes into play tapped.\n{T}, Sacrifice Grasslands: Search your library for a forest or plains card. Put that land into play. Shuffle your library afterwards.').
card_first_print('grasslands', 'MIR').
card_image_name('grasslands'/'MIR', 'grasslands').
card_uid('grasslands'/'MIR', 'MIR:Grasslands:grasslands').
card_rarity('grasslands'/'MIR', 'Uncommon').
card_artist('grasslands'/'MIR', 'John Avon').
card_multiverse_id('grasslands'/'MIR', '3573').

card_in_set('grave servitude', 'MIR').
card_original_type('grave servitude'/'MIR', 'Enchant Creature').
card_original_text('grave servitude'/'MIR', 'You may choose to play Grave Servitude as an instant; if you do, bury it at end of turn.\nEnchanted creature gets +3/-1 and is black.').
card_first_print('grave servitude', 'MIR').
card_image_name('grave servitude'/'MIR', 'grave servitude').
card_uid('grave servitude'/'MIR', 'MIR:Grave Servitude:grave servitude').
card_rarity('grave servitude'/'MIR', 'Common').
card_artist('grave servitude'/'MIR', 'Adrian Smith').
card_flavor_text('grave servitude'/'MIR', '\"I no longer believe in a final resting place.\"\n—Shauku, Endbringer').
card_multiverse_id('grave servitude'/'MIR', '3295').

card_in_set('gravebane zombie', 'MIR').
card_original_type('gravebane zombie'/'MIR', 'Summon — Zombie').
card_original_text('gravebane zombie'/'MIR', 'If Gravebane Zombie is put into the graveyard from play, put Gravebane Zombie on top of owner\'s library.').
card_first_print('gravebane zombie', 'MIR').
card_image_name('gravebane zombie'/'MIR', 'gravebane zombie').
card_uid('gravebane zombie'/'MIR', 'MIR:Gravebane Zombie:gravebane zombie').
card_rarity('gravebane zombie'/'MIR', 'Common').
card_artist('gravebane zombie'/'MIR', 'Gary Leach').
card_flavor_text('gravebane zombie'/'MIR', '\"A zombie\'s bed must be a lumpy one or he wouldn\'t leave it so often.\"\n—Rana, Suq\'Ata market fool').
card_multiverse_id('gravebane zombie'/'MIR', '3296').

card_in_set('grim feast', 'MIR').
card_original_type('grim feast'/'MIR', 'Enchantment').
card_original_text('grim feast'/'MIR', 'At the beginning of your upkeep, Grim Feast deals 1 damage to you.\nWhenever a creature is put into target opponent\'s graveyard from play, gain an amount of life equal to that creature\'s toughness.').
card_first_print('grim feast', 'MIR').
card_image_name('grim feast'/'MIR', 'grim feast').
card_uid('grim feast'/'MIR', 'MIR:Grim Feast:grim feast').
card_rarity('grim feast'/'MIR', 'Rare').
card_artist('grim feast'/'MIR', 'Mike Kimble').
card_flavor_text('grim feast'/'MIR', '\"Hmmm—midnight snack.\"').
card_multiverse_id('grim feast'/'MIR', '3536').

card_in_set('grinning totem', 'MIR').
card_original_type('grinning totem'/'MIR', 'Artifact').
card_original_text('grinning totem'/'MIR', '{2}, {T}, Sacrifice Grinning Totem: Search target opponent\'s library for any card and put it face up in front of you. That player shuffles his or her library afterwards. You may play the card as though it were in your hand. If you do not play the card by the beginning of your next upkeep, put it into its owner\'s graveyard.').
card_first_print('grinning totem', 'MIR').
card_image_name('grinning totem'/'MIR', 'grinning totem').
card_uid('grinning totem'/'MIR', 'MIR:Grinning Totem:grinning totem').
card_rarity('grinning totem'/'MIR', 'Rare').
card_artist('grinning totem'/'MIR', 'Donato Giancola').
card_multiverse_id('grinning totem'/'MIR', '3251').

card_in_set('hakim, loreweaver', 'MIR').
card_original_type('hakim, loreweaver'/'MIR', 'Summon — Legend').
card_original_text('hakim, loreweaver'/'MIR', 'Flying\n{U}{U} Put target creature enchantment card from your graveyard on Hakim, Loreweaver. Treat that enchantment as though it were just played. Use this ability only during your upkeep and only if there are no enchantments on Hakim.\n{U}{U}, {T}: Destroy all enchantments on Hakim.').
card_first_print('hakim, loreweaver', 'MIR').
card_image_name('hakim, loreweaver'/'MIR', 'hakim, loreweaver').
card_uid('hakim, loreweaver'/'MIR', 'MIR:Hakim, Loreweaver:hakim, loreweaver').
card_rarity('hakim, loreweaver'/'MIR', 'Rare').
card_artist('hakim, loreweaver'/'MIR', 'Alan Rabinowitz').
card_multiverse_id('hakim, loreweaver'/'MIR', '3339').

card_in_set('hall of gemstone', 'MIR').
card_original_type('hall of gemstone'/'MIR', 'Enchant World').
card_original_text('hall of gemstone'/'MIR', 'During each player\'s upkeep, that player chooses a color.\nUntil end of turn, each mana-producing land produces mana of the chosen color instead of its normal color.').
card_first_print('hall of gemstone', 'MIR').
card_image_name('hall of gemstone'/'MIR', 'hall of gemstone').
card_uid('hall of gemstone'/'MIR', 'MIR:Hall of Gemstone:hall of gemstone').
card_rarity('hall of gemstone'/'MIR', 'Rare').
card_artist('hall of gemstone'/'MIR', 'David A. Cherry').
card_multiverse_id('hall of gemstone'/'MIR', '3390').

card_in_set('hammer of bogardan', 'MIR').
card_original_type('hammer of bogardan'/'MIR', 'Sorcery').
card_original_text('hammer of bogardan'/'MIR', 'Hammer of Bogardan deals 3 damage to target creature or player.\n{2}{R}{R}{R} Return Hammer of Bogardan to your hand. Use this ability only during your upkeep and only if Hammer of Bogardan is in your graveyard.').
card_first_print('hammer of bogardan', 'MIR').
card_image_name('hammer of bogardan'/'MIR', 'hammer of bogardan').
card_uid('hammer of bogardan'/'MIR', 'MIR:Hammer of Bogardan:hammer of bogardan').
card_rarity('hammer of bogardan'/'MIR', 'Rare').
card_artist('hammer of bogardan'/'MIR', 'Ron Spencer').
card_multiverse_id('hammer of bogardan'/'MIR', '3452').

card_in_set('harbinger of night', 'MIR').
card_original_type('harbinger of night'/'MIR', 'Summon — Spirit').
card_original_text('harbinger of night'/'MIR', 'During your upkeep, put a -1/-1 counter on each creature.').
card_first_print('harbinger of night', 'MIR').
card_image_name('harbinger of night'/'MIR', 'harbinger of night').
card_uid('harbinger of night'/'MIR', 'MIR:Harbinger of Night:harbinger of night').
card_rarity('harbinger of night'/'MIR', 'Rare').
card_artist('harbinger of night'/'MIR', 'Tom Kyffin').
card_flavor_text('harbinger of night'/'MIR', 'Embrace the Harbinger\n—Suq\'Ata expression meaning\n\"abandon hope\"').
card_multiverse_id('harbinger of night'/'MIR', '3297').

card_in_set('harbor guardian', 'MIR').
card_original_type('harbor guardian'/'MIR', 'Summon — Guardian').
card_original_text('harbor guardian'/'MIR', 'Harbor Guardian can block creatures with flying.\nIf Harbor Guardian attacks, defending player may draw a card.').
card_first_print('harbor guardian', 'MIR').
card_image_name('harbor guardian'/'MIR', 'harbor guardian').
card_uid('harbor guardian'/'MIR', 'MIR:Harbor Guardian:harbor guardian').
card_rarity('harbor guardian'/'MIR', 'Uncommon').
card_artist('harbor guardian'/'MIR', 'Stuart Beel').
card_flavor_text('harbor guardian'/'MIR', '\"Our guardian levies a tax on all traders. Wise travelers pay with a minimum of fuss.\"\n—Qhattib, Vizier of Amiqat').
card_multiverse_id('harbor guardian'/'MIR', '3537').

card_in_set('harmattan efreet', 'MIR').
card_original_type('harmattan efreet'/'MIR', 'Summon — Efreet').
card_original_text('harmattan efreet'/'MIR', 'Flying\n{1}{U}{U} Target creature gains flying until end of turn.').
card_first_print('harmattan efreet', 'MIR').
card_image_name('harmattan efreet'/'MIR', 'harmattan efreet').
card_uid('harmattan efreet'/'MIR', 'MIR:Harmattan Efreet:harmattan efreet').
card_rarity('harmattan efreet'/'MIR', 'Uncommon').
card_artist('harmattan efreet'/'MIR', 'Drew Tucker').
card_flavor_text('harmattan efreet'/'MIR', '\"One moment I was walking along the beach, and the next I was high in the air, staring into a hideous smiling face.\"\n—Tarub, Suq\'Ata sailor').
card_multiverse_id('harmattan efreet'/'MIR', '3340').

card_in_set('haunting apparition', 'MIR').
card_original_type('haunting apparition'/'MIR', 'Summon — Ghost').
card_original_text('haunting apparition'/'MIR', 'Flying\nHaunting Apparition has power equal to 1 plus the number of green creature cards in target opponent\'s graveyard.').
card_first_print('haunting apparition', 'MIR').
card_image_name('haunting apparition'/'MIR', 'haunting apparition').
card_uid('haunting apparition'/'MIR', 'MIR:Haunting Apparition:haunting apparition').
card_rarity('haunting apparition'/'MIR', 'Uncommon').
card_artist('haunting apparition'/'MIR', 'Chippy').
card_flavor_text('haunting apparition'/'MIR', 'Like some foul herdbeast, it grazes on the dead.').
card_multiverse_id('haunting apparition'/'MIR', '3538').

card_in_set('hazerider drake', 'MIR').
card_original_type('hazerider drake'/'MIR', 'Summon — Drake').
card_original_text('hazerider drake'/'MIR', 'Flying, protection from red').
card_first_print('hazerider drake', 'MIR').
card_image_name('hazerider drake'/'MIR', 'hazerider drake').
card_uid('hazerider drake'/'MIR', 'MIR:Hazerider Drake:hazerider drake').
card_rarity('hazerider drake'/'MIR', 'Uncommon').
card_artist('hazerider drake'/'MIR', 'Zina Saunders').
card_flavor_text('hazerider drake'/'MIR', '\"The hazerider danced in the thunderstorm and sang to its music.\"\n—Afari, Tales').
card_multiverse_id('hazerider drake'/'MIR', '3539').

card_in_set('healing salve', 'MIR').
card_original_type('healing salve'/'MIR', 'Instant').
card_original_text('healing salve'/'MIR', 'Target player gains 3 life, or prevent up to 3 damage to any creature or player.').
card_image_name('healing salve'/'MIR', 'healing salve').
card_uid('healing salve'/'MIR', 'MIR:Healing Salve:healing salve').
card_rarity('healing salve'/'MIR', 'Common').
card_artist('healing salve'/'MIR', 'Hannibal King').
card_flavor_text('healing salve'/'MIR', '\"The Femeref look only to the Sun for healing; they have never truly understood life\'s cycle.\"\n—Kifimbo, Shadow Guildmage').
card_multiverse_id('healing salve'/'MIR', '3495').

card_in_set('hivis of the scale', 'MIR').
card_original_type('hivis of the scale'/'MIR', 'Summon — Legend').
card_original_text('hivis of the scale'/'MIR', 'You may choose not to untap Hivis of the Scale during your untap phase.\n{T}: Gain control of target Dragon. If Hivis becomes untapped or you lose control of Hivis, lose control of that Dragon.').
card_first_print('hivis of the scale', 'MIR').
card_image_name('hivis of the scale'/'MIR', 'hivis of the scale').
card_uid('hivis of the scale'/'MIR', 'MIR:Hivis of the Scale:hivis of the scale').
card_rarity('hivis of the scale'/'MIR', 'Rare').
card_artist('hivis of the scale'/'MIR', 'Andrew Robinson').
card_multiverse_id('hivis of the scale'/'MIR', '3453').

card_in_set('horrible hordes', 'MIR').
card_original_type('horrible hordes'/'MIR', 'Artifact Creature').
card_original_text('horrible hordes'/'MIR', 'Rampage 1 (For each creature assigned to block it beyond the first, this creature gets +1/+1 until end of turn.)').
card_first_print('horrible hordes', 'MIR').
card_image_name('horrible hordes'/'MIR', 'horrible hordes').
card_uid('horrible hordes'/'MIR', 'MIR:Horrible Hordes:horrible hordes').
card_rarity('horrible hordes'/'MIR', 'Uncommon').
card_artist('horrible hordes'/'MIR', 'Ian Miller').
card_flavor_text('horrible hordes'/'MIR', 'Few are able to underestimate the hordes.').
card_multiverse_id('horrible hordes'/'MIR', '3252').

card_in_set('igneous golem', 'MIR').
card_original_type('igneous golem'/'MIR', 'Artifact Creature').
card_original_text('igneous golem'/'MIR', '{2}: Trample until end of turn').
card_first_print('igneous golem', 'MIR').
card_image_name('igneous golem'/'MIR', 'igneous golem').
card_uid('igneous golem'/'MIR', 'MIR:Igneous Golem:igneous golem').
card_rarity('igneous golem'/'MIR', 'Uncommon').
card_artist('igneous golem'/'MIR', 'Adam Rex').
card_flavor_text('igneous golem'/'MIR', '\"The creature rose from the mountain, its body weeping fire.\"\n—Afari, Tales').
card_multiverse_id('igneous golem'/'MIR', '3253').

card_in_set('illicit auction', 'MIR').
card_original_type('illicit auction'/'MIR', 'Sorcery').
card_original_text('illicit auction'/'MIR', 'Choose target creature. Each player may bid life for control of that creature. You begin the bidding with a high bid of 0. Proceeding in turn order, each player may top the high bid. The auction ends when the high bid stands. The high bidder loses an amount of life equal to the high bid and gains control of the creature.').
card_first_print('illicit auction', 'MIR').
card_image_name('illicit auction'/'MIR', 'illicit auction').
card_uid('illicit auction'/'MIR', 'MIR:Illicit Auction:illicit auction').
card_rarity('illicit auction'/'MIR', 'Rare').
card_artist('illicit auction'/'MIR', 'Scott Kirschner').
card_multiverse_id('illicit auction'/'MIR', '3454').

card_in_set('illumination', 'MIR').
card_original_type('illumination'/'MIR', 'Interrupt').
card_original_text('illumination'/'MIR', 'Counter target artifact or enchantment spell. That spell\'s caster gains an amount of life equal to the spell\'s casting cost.').
card_first_print('illumination', 'MIR').
card_image_name('illumination'/'MIR', 'illumination').
card_uid('illumination'/'MIR', 'MIR:Illumination:illumination').
card_rarity('illumination'/'MIR', 'Uncommon').
card_artist('illumination'/'MIR', 'David O\'Connor').
card_flavor_text('illumination'/'MIR', '\"A brilliant light can either illuminate or blind. How will you know which until you open your eyes?\"\n—Asmira, Holy Avenger').
card_multiverse_id('illumination'/'MIR', '3496').

card_in_set('incinerate', 'MIR').
card_original_type('incinerate'/'MIR', 'Instant').
card_original_text('incinerate'/'MIR', 'Incinerate deals 3 damage to target creature or player. A creature damaged by Incinerate cannot regenerate this turn.').
card_image_name('incinerate'/'MIR', 'incinerate').
card_uid('incinerate'/'MIR', 'MIR:Incinerate:incinerate').
card_rarity('incinerate'/'MIR', 'Common').
card_artist('incinerate'/'MIR', 'Brian Snõddy').
card_flavor_text('incinerate'/'MIR', '\"Never taunt an embermage, ‘What are you going to do about it?\'\"\n—Akin, seasoned askari').
card_multiverse_id('incinerate'/'MIR', '3455').

card_in_set('infernal contract', 'MIR').
card_original_type('infernal contract'/'MIR', 'Sorcery').
card_original_text('infernal contract'/'MIR', 'Pay half your life, rounded up: Draw four cards.').
card_first_print('infernal contract', 'MIR').
card_image_name('infernal contract'/'MIR', 'infernal contract').
card_uid('infernal contract'/'MIR', 'MIR:Infernal Contract:infernal contract').
card_rarity('infernal contract'/'MIR', 'Rare').
card_artist('infernal contract'/'MIR', 'Roger Raupp').
card_flavor_text('infernal contract'/'MIR', '\"But I signed nothing!\"\n—Taraneh, Suq\'Ata mage').
card_multiverse_id('infernal contract'/'MIR', '3298').

card_in_set('iron tusk elephant', 'MIR').
card_original_type('iron tusk elephant'/'MIR', 'Summon — Elephant').
card_original_text('iron tusk elephant'/'MIR', 'Trample').
card_first_print('iron tusk elephant', 'MIR').
card_image_name('iron tusk elephant'/'MIR', 'iron tusk elephant').
card_uid('iron tusk elephant'/'MIR', 'MIR:Iron Tusk Elephant:iron tusk elephant').
card_rarity('iron tusk elephant'/'MIR', 'Uncommon').
card_artist('iron tusk elephant'/'MIR', 'Tony Roberts').
card_flavor_text('iron tusk elephant'/'MIR', '\"The fury in the lion\'s eye;\nthe patience in the hippo\'s yawn;\nthe pride within the griffin\'s cry\nare one within the iron tusk\'s stride.\"\n—\"Iron Tusk,\" Femeref song').
card_multiverse_id('iron tusk elephant'/'MIR', '3497').

card_in_set('island', 'MIR').
card_original_type('island'/'MIR', 'Land').
card_original_text('island'/'MIR', '{T}: Add {U} to your mana pool.').
card_image_name('island'/'MIR', 'island1').
card_uid('island'/'MIR', 'MIR:Island:island1').
card_rarity('island'/'MIR', 'Basic Land').
card_artist('island'/'MIR', 'Douglas Shuler').
card_multiverse_id('island'/'MIR', '3584').

card_in_set('island', 'MIR').
card_original_type('island'/'MIR', 'Land').
card_original_text('island'/'MIR', '{T}: Add {U} to your mana pool.').
card_image_name('island'/'MIR', 'island2').
card_uid('island'/'MIR', 'MIR:Island:island2').
card_rarity('island'/'MIR', 'Basic Land').
card_artist('island'/'MIR', 'Douglas Shuler').
card_multiverse_id('island'/'MIR', '3582').

card_in_set('island', 'MIR').
card_original_type('island'/'MIR', 'Land').
card_original_text('island'/'MIR', '{T}: Add {U} to your mana pool.').
card_image_name('island'/'MIR', 'island3').
card_uid('island'/'MIR', 'MIR:Island:island3').
card_rarity('island'/'MIR', 'Basic Land').
card_artist('island'/'MIR', 'Douglas Shuler').
card_multiverse_id('island'/'MIR', '3583').

card_in_set('island', 'MIR').
card_original_type('island'/'MIR', 'Land').
card_original_text('island'/'MIR', '{T}: Add {U} to your mana pool.').
card_image_name('island'/'MIR', 'island4').
card_uid('island'/'MIR', 'MIR:Island:island4').
card_rarity('island'/'MIR', 'Basic Land').
card_artist('island'/'MIR', 'Douglas Shuler').
card_multiverse_id('island'/'MIR', '3581').

card_in_set('ivory charm', 'MIR').
card_original_type('ivory charm'/'MIR', 'Instant').
card_original_text('ivory charm'/'MIR', 'Choose one All creatures get -2/-0 until end of turn; or prevent 1 damage to any creature or player; or tap target creature.').
card_first_print('ivory charm', 'MIR').
card_image_name('ivory charm'/'MIR', 'ivory charm').
card_uid('ivory charm'/'MIR', 'MIR:Ivory Charm:ivory charm').
card_rarity('ivory charm'/'MIR', 'Common').
card_artist('ivory charm'/'MIR', 'Gerry Grace').
card_multiverse_id('ivory charm'/'MIR', '3498').

card_in_set('jabari\'s influence', 'MIR').
card_original_type('jabari\'s influence'/'MIR', 'Instant').
card_original_text('jabari\'s influence'/'MIR', 'Play only after combat.\nGain control of target nonartifact, nonblack creature that attacked you this turn and put a -1/-0 counter on it.').
card_first_print('jabari\'s influence', 'MIR').
card_image_name('jabari\'s influence'/'MIR', 'jabari\'s influence').
card_uid('jabari\'s influence'/'MIR', 'MIR:Jabari\'s Influence:jabari\'s influence').
card_rarity('jabari\'s influence'/'MIR', 'Rare').
card_artist('jabari\'s influence'/'MIR', 'Gerry Grace').
card_flavor_text('jabari\'s influence'/'MIR', '\"Common soldiers can\'t understand what a sidar knows: the enemy can be a resource.\"\n—Sidar Jabari').
card_multiverse_id('jabari\'s influence'/'MIR', '3499').

card_in_set('jolrael\'s centaur', 'MIR').
card_original_type('jolrael\'s centaur'/'MIR', 'Summon — Centaur').
card_original_text('jolrael\'s centaur'/'MIR', 'Flanking (Whenever a creature without flanking is assigned to block this creature, the blocking creature gets -1/-1 until end of turn.)\nJolrael\'s Centaur cannot be the target of spells or effects.').
card_first_print('jolrael\'s centaur', 'MIR').
card_image_name('jolrael\'s centaur'/'MIR', 'jolrael\'s centaur').
card_uid('jolrael\'s centaur'/'MIR', 'MIR:Jolrael\'s Centaur:jolrael\'s centaur').
card_rarity('jolrael\'s centaur'/'MIR', 'Common').
card_artist('jolrael\'s centaur'/'MIR', 'Junior Tomlin').
card_flavor_text('jolrael\'s centaur'/'MIR', '\"What need have I for cavalry when I can have horse and rider both in one?\"\n—Jolrael').
card_multiverse_id('jolrael\'s centaur'/'MIR', '3391').

card_in_set('jolt', 'MIR').
card_original_type('jolt'/'MIR', 'Instant').
card_original_text('jolt'/'MIR', 'Tap or untap target artifact, creature, or land.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_first_print('jolt', 'MIR').
card_image_name('jolt'/'MIR', 'jolt').
card_uid('jolt'/'MIR', 'MIR:Jolt:jolt').
card_rarity('jolt'/'MIR', 'Common').
card_artist('jolt'/'MIR', 'John Matson').
card_flavor_text('jolt'/'MIR', '\"There\'s nothing like a sudden shock to clarify one\'s thoughts.\"\n—Pashad ibn Asim, Suq\'Ata trader').
card_multiverse_id('jolt'/'MIR', '3341').

card_in_set('jungle patrol', 'MIR').
card_original_type('jungle patrol'/'MIR', 'Summon — Soldiers').
card_original_text('jungle patrol'/'MIR', '{1}{G}, {T}: Put a Wood token into play. Treat this token as a 0/1 green creature that counts as a Wall.\nSacrifice a Wood token: Add {R} to your mana pool. Play this ability as a mana source.').
card_first_print('jungle patrol', 'MIR').
card_image_name('jungle patrol'/'MIR', 'jungle patrol').
card_uid('jungle patrol'/'MIR', 'MIR:Jungle Patrol:jungle patrol').
card_rarity('jungle patrol'/'MIR', 'Rare').
card_artist('jungle patrol'/'MIR', 'Mark Poole').
card_multiverse_id('jungle patrol'/'MIR', '3392').

card_in_set('jungle troll', 'MIR').
card_original_type('jungle troll'/'MIR', 'Summon — Troll').
card_original_text('jungle troll'/'MIR', '{R} Regenerate\n{G} Regenerate').
card_first_print('jungle troll', 'MIR').
card_image_name('jungle troll'/'MIR', 'jungle troll').
card_uid('jungle troll'/'MIR', 'MIR:Jungle Troll:jungle troll').
card_rarity('jungle troll'/'MIR', 'Uncommon').
card_artist('jungle troll'/'MIR', 'John Bolton').
card_flavor_text('jungle troll'/'MIR', '\"You eat plants; I eat plants. You eat beasts; I eat you.\"\n—Drulvurg, troll king').
card_multiverse_id('jungle troll'/'MIR', '3540').

card_in_set('jungle wurm', 'MIR').
card_original_type('jungle wurm'/'MIR', 'Summon — Wurm').
card_original_text('jungle wurm'/'MIR', 'For each creature assigned to block it beyond the first, Jungle Wurm gets -1/-1 until end of turn.').
card_first_print('jungle wurm', 'MIR').
card_image_name('jungle wurm'/'MIR', 'jungle wurm').
card_uid('jungle wurm'/'MIR', 'MIR:Jungle Wurm:jungle wurm').
card_rarity('jungle wurm'/'MIR', 'Common').
card_artist('jungle wurm'/'MIR', 'Tom Kyffin').
card_flavor_text('jungle wurm'/'MIR', 'Broad as a baobab—and about as smart.').
card_multiverse_id('jungle wurm'/'MIR', '3393').

card_in_set('kaervek\'s hex', 'MIR').
card_original_type('kaervek\'s hex'/'MIR', 'Sorcery').
card_original_text('kaervek\'s hex'/'MIR', 'Kaervek\'s Hex deals 1 damage to each nonblack creature and an additional 1 damage to each green creature.').
card_first_print('kaervek\'s hex', 'MIR').
card_image_name('kaervek\'s hex'/'MIR', 'kaervek\'s hex').
card_uid('kaervek\'s hex'/'MIR', 'MIR:Kaervek\'s Hex:kaervek\'s hex').
card_rarity('kaervek\'s hex'/'MIR', 'Uncommon').
card_artist('kaervek\'s hex'/'MIR', 'Ian Miller').
card_flavor_text('kaervek\'s hex'/'MIR', '\"If the curse does not strike you dead, question your soul.\"\n—Kaervek').
card_multiverse_id('kaervek\'s hex'/'MIR', '3299').

card_in_set('kaervek\'s purge', 'MIR').
card_original_type('kaervek\'s purge'/'MIR', 'Sorcery').
card_original_text('kaervek\'s purge'/'MIR', 'Destroy target creature with casting cost equal to X. If that creature is put into the graveyard in this way, Kaervek\'s Purge deals to the creature\'s controller an amount of damage equal to the creature\'s power.').
card_first_print('kaervek\'s purge', 'MIR').
card_image_name('kaervek\'s purge'/'MIR', 'kaervek\'s purge').
card_uid('kaervek\'s purge'/'MIR', 'MIR:Kaervek\'s Purge:kaervek\'s purge').
card_rarity('kaervek\'s purge'/'MIR', 'Uncommon').
card_artist('kaervek\'s purge'/'MIR', 'Richard Kane Ferguson').
card_multiverse_id('kaervek\'s purge'/'MIR', '3541').

card_in_set('kaervek\'s torch', 'MIR').
card_original_type('kaervek\'s torch'/'MIR', 'Sorcery').
card_original_text('kaervek\'s torch'/'MIR', 'Interrupts that target Kaervek\'s Torch each cost an additional {2} to play.\nKaervek\'s Torch deals X damage to target creature or player.').
card_first_print('kaervek\'s torch', 'MIR').
card_image_name('kaervek\'s torch'/'MIR', 'kaervek\'s torch').
card_uid('kaervek\'s torch'/'MIR', 'MIR:Kaervek\'s Torch:kaervek\'s torch').
card_rarity('kaervek\'s torch'/'MIR', 'Common').
card_artist('kaervek\'s torch'/'MIR', 'John Coulthart').
card_flavor_text('kaervek\'s torch'/'MIR', 'The pulsing heat of the midday Sun burns in the Lion\'s eye.\n—Stone inscription, source unknown').
card_multiverse_id('kaervek\'s torch'/'MIR', '3456').

card_in_set('karoo meerkat', 'MIR').
card_original_type('karoo meerkat'/'MIR', 'Summon — Meerkat').
card_original_text('karoo meerkat'/'MIR', 'Protection from blue').
card_first_print('karoo meerkat', 'MIR').
card_image_name('karoo meerkat'/'MIR', 'karoo meerkat').
card_uid('karoo meerkat'/'MIR', 'MIR:Karoo Meerkat:karoo meerkat').
card_rarity('karoo meerkat'/'MIR', 'Uncommon').
card_artist('karoo meerkat'/'MIR', 'Janine Johnston').
card_flavor_text('karoo meerkat'/'MIR', '\"Be like the meerkat, my daughters: ever vigilant, true to your own, and wary of strangers.\"\n—Nabil Alamat, Suq\'Ata merchant').
card_multiverse_id('karoo meerkat'/'MIR', '3394').

card_in_set('kukemssa pirates', 'MIR').
card_original_type('kukemssa pirates'/'MIR', 'Summon — Pirates').
card_original_text('kukemssa pirates'/'MIR', 'If Kukemssa Pirates attacks and is not blocked, you may choose to have it deal no damage to defending player this turn. If you do, gain control of target artifact that player controls.').
card_first_print('kukemssa pirates', 'MIR').
card_image_name('kukemssa pirates'/'MIR', 'kukemssa pirates').
card_uid('kukemssa pirates'/'MIR', 'MIR:Kukemssa Pirates:kukemssa pirates').
card_rarity('kukemssa pirates'/'MIR', 'Rare').
card_artist('kukemssa pirates'/'MIR', 'JOCK').
card_flavor_text('kukemssa pirates'/'MIR', '\". . . pirates gambled with a djinn and lost the thing more dear than gold.\"\n—\"Love Song of Night and Day\"').
card_multiverse_id('kukemssa pirates'/'MIR', '3342').

card_in_set('kukemssa serpent', 'MIR').
card_original_type('kukemssa serpent'/'MIR', 'Summon — Serpent').
card_original_text('kukemssa serpent'/'MIR', 'Islandhome (If defending player controls no islands, this creature cannot attack. If you control no islands, bury this creature.)\n{U}, Sacrifice an island: Target land an opponent controls is an island until end of turn.').
card_first_print('kukemssa serpent', 'MIR').
card_image_name('kukemssa serpent'/'MIR', 'kukemssa serpent').
card_uid('kukemssa serpent'/'MIR', 'MIR:Kukemssa Serpent:kukemssa serpent').
card_rarity('kukemssa serpent'/'MIR', 'Common').
card_artist('kukemssa serpent'/'MIR', 'Ian Miller').
card_multiverse_id('kukemssa serpent'/'MIR', '3343').

card_in_set('lead golem', 'MIR').
card_original_type('lead golem'/'MIR', 'Artifact Creature').
card_original_text('lead golem'/'MIR', 'If Lead Golem attacks, it does not untap during your next untap phase.').
card_first_print('lead golem', 'MIR').
card_image_name('lead golem'/'MIR', 'lead golem').
card_uid('lead golem'/'MIR', 'MIR:Lead Golem:lead golem').
card_rarity('lead golem'/'MIR', 'Uncommon').
card_artist('lead golem'/'MIR', 'Hannibal King').
card_flavor_text('lead golem'/'MIR', 'Slow and heavy swings the club.').
card_multiverse_id('lead golem'/'MIR', '3254').

card_in_set('leering gargoyle', 'MIR').
card_original_type('leering gargoyle'/'MIR', 'Summon — Gargoyle').
card_original_text('leering gargoyle'/'MIR', 'Flying\n{T}: Leering Gargoyle gets -2/+2 and loses flying until end of turn.').
card_first_print('leering gargoyle', 'MIR').
card_image_name('leering gargoyle'/'MIR', 'leering gargoyle').
card_uid('leering gargoyle'/'MIR', 'MIR:Leering Gargoyle:leering gargoyle').
card_rarity('leering gargoyle'/'MIR', 'Rare').
card_artist('leering gargoyle'/'MIR', 'Dermot Power').
card_flavor_text('leering gargoyle'/'MIR', '\"Only a gargoyle would take such delight in a thunderstorm.\"\n—Qhattib, Vizier of Amiqat').
card_multiverse_id('leering gargoyle'/'MIR', '3542').

card_in_set('lightning reflexes', 'MIR').
card_original_type('lightning reflexes'/'MIR', 'Enchant Creature').
card_original_text('lightning reflexes'/'MIR', 'You may choose to play Lightning Reflexes as an instant; if you do, bury it at end of turn.\nEnchanted creature gets +1/+0 and gains first strike.').
card_first_print('lightning reflexes', 'MIR').
card_image_name('lightning reflexes'/'MIR', 'lightning reflexes').
card_uid('lightning reflexes'/'MIR', 'MIR:Lightning Reflexes:lightning reflexes').
card_rarity('lightning reflexes'/'MIR', 'Common').
card_artist('lightning reflexes'/'MIR', 'Tom Kyffin').
card_flavor_text('lightning reflexes'/'MIR', '\"Inhale. Kill. Exhale.\"').
card_multiverse_id('lightning reflexes'/'MIR', '3457').

card_in_set('lion\'s eye diamond', 'MIR').
card_original_type('lion\'s eye diamond'/'MIR', 'Artifact').
card_original_text('lion\'s eye diamond'/'MIR', 'Sacrifice Lion\'s Eye Diamond, Discard your hand: Add three mana of any one color to your mana pool. Play this ability as a mana source.').
card_first_print('lion\'s eye diamond', 'MIR').
card_image_name('lion\'s eye diamond'/'MIR', 'lion\'s eye diamond').
card_uid('lion\'s eye diamond'/'MIR', 'MIR:Lion\'s Eye Diamond:lion\'s eye diamond').
card_rarity('lion\'s eye diamond'/'MIR', 'Rare').
card_artist('lion\'s eye diamond'/'MIR', 'Margaret Organ-Kean').
card_flavor_text('lion\'s eye diamond'/'MIR', 'Held in the lion\'s eye\n—Zhalfirin saying meaning \"caught in the moment of crisis\"').
card_multiverse_id('lion\'s eye diamond'/'MIR', '3255').

card_in_set('locust swarm', 'MIR').
card_original_type('locust swarm'/'MIR', 'Summon — Swarm').
card_original_text('locust swarm'/'MIR', 'Flying\n{G} Regenerate\n{G} Untap Locust Swarm. Use this ability only once each turn.').
card_first_print('locust swarm', 'MIR').
card_image_name('locust swarm'/'MIR', 'locust swarm').
card_uid('locust swarm'/'MIR', 'MIR:Locust Swarm:locust swarm').
card_rarity('locust swarm'/'MIR', 'Uncommon').
card_artist('locust swarm'/'MIR', 'William Donohoe').
card_flavor_text('locust swarm'/'MIR', 'Better a fierce thunderhead than a sky filled with locusts.').
card_multiverse_id('locust swarm'/'MIR', '3395').

card_in_set('lure of prey', 'MIR').
card_original_type('lure of prey'/'MIR', 'Instant').
card_original_text('lure of prey'/'MIR', 'Play only if an opponent successfully cast a summon spell this turn.\nPut a green summon card from your hand into play as though it were just played.').
card_first_print('lure of prey', 'MIR').
card_image_name('lure of prey'/'MIR', 'lure of prey').
card_uid('lure of prey'/'MIR', 'MIR:Lure of Prey:lure of prey').
card_rarity('lure of prey'/'MIR', 'Rare').
card_artist('lure of prey'/'MIR', 'Andrew Robinson').
card_multiverse_id('lure of prey'/'MIR', '3396').

card_in_set('malignant growth', 'MIR').
card_original_type('malignant growth'/'MIR', 'Enchantment').
card_original_text('malignant growth'/'MIR', 'Cumulative upkeep {1}\nDuring your upkeep, put a growth counter on Malignant Growth.\nDuring target opponent\'s draw phase, he or she draws an additional card for each growth counter on Malignant Growth. For each card that opponent draws in this way, Malignant Growth deals 1 damage to him or her.').
card_first_print('malignant growth', 'MIR').
card_image_name('malignant growth'/'MIR', 'malignant growth').
card_uid('malignant growth'/'MIR', 'MIR:Malignant Growth:malignant growth').
card_rarity('malignant growth'/'MIR', 'Rare').
card_artist('malignant growth'/'MIR', 'Scott M. Fischer').
card_multiverse_id('malignant growth'/'MIR', '3543').

card_in_set('mana prism', 'MIR').
card_original_type('mana prism'/'MIR', 'Artifact').
card_original_text('mana prism'/'MIR', '{T}: Add one colorless mana to your mana pool. Play this ability as a mana source. {1}, {T}: Add one mana of any color to your mana pool. Play this ability as a mana source.').
card_first_print('mana prism', 'MIR').
card_image_name('mana prism'/'MIR', 'mana prism').
card_uid('mana prism'/'MIR', 'MIR:Mana Prism:mana prism').
card_rarity('mana prism'/'MIR', 'Uncommon').
card_artist('mana prism'/'MIR', 'Margaret Organ-Kean').
card_multiverse_id('mana prism'/'MIR', '3256').

card_in_set('mangara\'s blessing', 'MIR').
card_original_type('mangara\'s blessing'/'MIR', 'Instant').
card_original_text('mangara\'s blessing'/'MIR', 'Gain 5 life. \nWhen a spell or ability an opponent controls causes you to discard Mangara\'s Blessing, you gain 2 life and return Mangara\'s Blessing from your graveyard to your hand at end of turn.').
card_first_print('mangara\'s blessing', 'MIR').
card_image_name('mangara\'s blessing'/'MIR', 'mangara\'s blessing').
card_uid('mangara\'s blessing'/'MIR', 'MIR:Mangara\'s Blessing:mangara\'s blessing').
card_rarity('mangara\'s blessing'/'MIR', 'Uncommon').
card_artist('mangara\'s blessing'/'MIR', 'David A. Cherry').
card_multiverse_id('mangara\'s blessing'/'MIR', '3500').

card_in_set('mangara\'s equity', 'MIR').
card_original_type('mangara\'s equity'/'MIR', 'Enchantment').
card_original_text('mangara\'s equity'/'MIR', 'When you play Mangara\'s Equity, choose black or red.\nDuring your upkeep, pay {1}{W} or bury Mangara\'s Equity.\nFor each 1 damage a creature of the chosen color deals to you or a white creature you control, Mangara\'s Equity deals 1 damage to that creature.').
card_first_print('mangara\'s equity', 'MIR').
card_image_name('mangara\'s equity'/'MIR', 'mangara\'s equity').
card_uid('mangara\'s equity'/'MIR', 'MIR:Mangara\'s Equity:mangara\'s equity').
card_rarity('mangara\'s equity'/'MIR', 'Uncommon').
card_artist('mangara\'s equity'/'MIR', 'Alan Rabinowitz').
card_multiverse_id('mangara\'s equity'/'MIR', '3501').

card_in_set('mangara\'s tome', 'MIR').
card_original_type('mangara\'s tome'/'MIR', 'Artifact').
card_original_text('mangara\'s tome'/'MIR', 'When Mangara\'s Tome comes into play, search your library and choose any five cards. Shuffle these cards and put them face down under Mangara\'s Tome. Shuffle your library afterwards.\nIf you lose control of Mangara\'s Tome, remove all cards under it from the game.\n{2}: Instead of drawing a card, put the top card from under Mangara\'s Tome into your hand.').
card_first_print('mangara\'s tome', 'MIR').
card_image_name('mangara\'s tome'/'MIR', 'mangara\'s tome').
card_uid('mangara\'s tome'/'MIR', 'MIR:Mangara\'s Tome:mangara\'s tome').
card_rarity('mangara\'s tome'/'MIR', 'Rare').
card_artist('mangara\'s tome'/'MIR', 'John Bolton').
card_multiverse_id('mangara\'s tome'/'MIR', '3257').

card_in_set('marble diamond', 'MIR').
card_original_type('marble diamond'/'MIR', 'Artifact').
card_original_text('marble diamond'/'MIR', 'Marble Diamond comes into play tapped.\n{T}: Add {W} to your mana pool. Play this ability as a mana source.').
card_first_print('marble diamond', 'MIR').
card_image_name('marble diamond'/'MIR', 'marble diamond').
card_uid('marble diamond'/'MIR', 'MIR:Marble Diamond:marble diamond').
card_rarity('marble diamond'/'MIR', 'Uncommon').
card_artist('marble diamond'/'MIR', 'Jeff Miracola').
card_multiverse_id('marble diamond'/'MIR', '3258').

card_in_set('maro', 'MIR').
card_original_type('maro'/'MIR', 'Summon — Nature Spirit').
card_original_text('maro'/'MIR', 'Maro has power and toughness each equal to the number of cards in your hand.').
card_first_print('maro', 'MIR').
card_image_name('maro'/'MIR', 'maro').
card_uid('maro'/'MIR', 'MIR:Maro:maro').
card_rarity('maro'/'MIR', 'Rare').
card_artist('maro'/'MIR', 'Stuart Griffin').
card_flavor_text('maro'/'MIR', 'No two see the same Maro.').
card_multiverse_id('maro'/'MIR', '3397').

card_in_set('meddle', 'MIR').
card_original_type('meddle'/'MIR', 'Interrupt').
card_original_text('meddle'/'MIR', 'Target spell, which targets a single creature, targets another creature of your choice instead. The new target must be legal.').
card_first_print('meddle', 'MIR').
card_image_name('meddle'/'MIR', 'meddle').
card_uid('meddle'/'MIR', 'MIR:Meddle:meddle').
card_rarity('meddle'/'MIR', 'Uncommon').
card_artist('meddle'/'MIR', 'Brian Snõddy').
card_flavor_text('meddle'/'MIR', '\"Shadimir was as surprised as he was relieved when the blow turned aside.\"\n—Afari, Tales').
card_multiverse_id('meddle'/'MIR', '3344').

card_in_set('melesse spirit', 'MIR').
card_original_type('melesse spirit'/'MIR', 'Summon — Angel').
card_original_text('melesse spirit'/'MIR', 'Flying, protection from black').
card_first_print('melesse spirit', 'MIR').
card_image_name('melesse spirit'/'MIR', 'melesse spirit').
card_uid('melesse spirit'/'MIR', 'MIR:Melesse Spirit:melesse spirit').
card_rarity('melesse spirit'/'MIR', 'Uncommon').
card_artist('melesse spirit'/'MIR', 'Gerry Grace').
card_flavor_text('melesse spirit'/'MIR', 'Like a laughing knife, immaculate and sharp, is the Melesse.').
card_multiverse_id('melesse spirit'/'MIR', '3502').

card_in_set('memory lapse', 'MIR').
card_original_type('memory lapse'/'MIR', 'Interrupt').
card_original_text('memory lapse'/'MIR', 'Counter target spell. Put that spell on top of owner\'s library.').
card_image_name('memory lapse'/'MIR', 'memory lapse').
card_uid('memory lapse'/'MIR', 'MIR:Memory Lapse:memory lapse').
card_rarity('memory lapse'/'MIR', 'Common').
card_artist('memory lapse'/'MIR', 'Rebecca Guay').
card_flavor_text('memory lapse'/'MIR', '\"The caliph was so intelligent he could not remember what he knew.\"\n—Azeworai, \"Three Riddles\"').
card_multiverse_id('memory lapse'/'MIR', '3345').

card_in_set('merfolk raiders', 'MIR').
card_original_type('merfolk raiders'/'MIR', 'Summon — Merfolk').
card_original_text('merfolk raiders'/'MIR', 'Phasing; islandwalk (If defending player controls any islands, this creature is unblockable.)').
card_first_print('merfolk raiders', 'MIR').
card_image_name('merfolk raiders'/'MIR', 'merfolk raiders').
card_uid('merfolk raiders'/'MIR', 'MIR:Merfolk Raiders:merfolk raiders').
card_rarity('merfolk raiders'/'MIR', 'Common').
card_artist('merfolk raiders'/'MIR', 'Steve Luke').
card_flavor_text('merfolk raiders'/'MIR', '\"Turn your back for a second and they just vanish! As if they weren\'t shifty enough before.\"\n—Rhirhok, goblin archer').
card_multiverse_id('merfolk raiders'/'MIR', '3346').

card_in_set('merfolk seer', 'MIR').
card_original_type('merfolk seer'/'MIR', 'Summon — Merfolk').
card_original_text('merfolk seer'/'MIR', '{1}{U} Draw a card. Use this ability only when Merfolk Seer is put into the graveyard from play and only once.').
card_first_print('merfolk seer', 'MIR').
card_image_name('merfolk seer'/'MIR', 'merfolk seer').
card_uid('merfolk seer'/'MIR', 'MIR:Merfolk Seer:merfolk seer').
card_rarity('merfolk seer'/'MIR', 'Common').
card_artist('merfolk seer'/'MIR', 'Steve Luke').
card_flavor_text('merfolk seer'/'MIR', 'The sea leaves no room for tears.\n—Zhalfirin adage').
card_multiverse_id('merfolk seer'/'MIR', '3347').

card_in_set('mind bend', 'MIR').
card_original_type('mind bend'/'MIR', 'Instant').
card_original_text('mind bend'/'MIR', 'Change the text of target permanent by replacing all instances of one color word or basic land type with another. (For example, you may change \"nonred creature\" to \"nongreen creature\" or \"plainswalk\" to \"swampwalk.\")').
card_first_print('mind bend', 'MIR').
card_image_name('mind bend'/'MIR', 'mind bend').
card_uid('mind bend'/'MIR', 'MIR:Mind Bend:mind bend').
card_rarity('mind bend'/'MIR', 'Uncommon').
card_artist('mind bend'/'MIR', 'Mike Dringenberg').
card_multiverse_id('mind bend'/'MIR', '3348').

card_in_set('mind harness', 'MIR').
card_original_type('mind harness'/'MIR', 'Enchant Creature').
card_original_text('mind harness'/'MIR', 'Play only on a red or green creature.\nCumulative upkeep {1}\nGain control of enchanted creature.').
card_first_print('mind harness', 'MIR').
card_image_name('mind harness'/'MIR', 'mind harness').
card_uid('mind harness'/'MIR', 'MIR:Mind Harness:mind harness').
card_rarity('mind harness'/'MIR', 'Uncommon').
card_artist('mind harness'/'MIR', 'John Malloy').
card_flavor_text('mind harness'/'MIR', 'Centuries ago, Mangara won the loyalty of the Quirion not by ruling their minds but by supporting their independence.').
card_multiverse_id('mind harness'/'MIR', '3349').

card_in_set('mindbender spores', 'MIR').
card_original_type('mindbender spores'/'MIR', 'Summon — Wall').
card_original_text('mindbender spores'/'MIR', 'Flying\nWhenever Mindbender Spores blocks any creature, put four fungus counters on that creature. During its controller\'s untap phase, remove a fungus counter from the creature. As long as the creature has any fungus counters on it, it does not untap during its controller\'s untap phase.').
card_first_print('mindbender spores', 'MIR').
card_image_name('mindbender spores'/'MIR', 'mindbender spores').
card_uid('mindbender spores'/'MIR', 'MIR:Mindbender Spores:mindbender spores').
card_rarity('mindbender spores'/'MIR', 'Rare').
card_artist('mindbender spores'/'MIR', 'Ian Miller').
card_multiverse_id('mindbender spores'/'MIR', '3398').

card_in_set('mire shade', 'MIR').
card_original_type('mire shade'/'MIR', 'Summon — Shade').
card_original_text('mire shade'/'MIR', '{B}, Sacrifice a swamp: Put a +1/+1 counter on Mire Shade. Play this ability as a sorcery.').
card_first_print('mire shade', 'MIR').
card_image_name('mire shade'/'MIR', 'mire shade').
card_uid('mire shade'/'MIR', 'MIR:Mire Shade:mire shade').
card_rarity('mire shade'/'MIR', 'Uncommon').
card_artist('mire shade'/'MIR', 'Randy Gallegos').
card_flavor_text('mire shade'/'MIR', '\"We realized the bog had not given up a soul but had come to claim a new one.\"\n—Scout Ekemet, final journal').
card_multiverse_id('mire shade'/'MIR', '3300').

card_in_set('misers\' cage', 'MIR').
card_original_type('misers\' cage'/'MIR', 'Artifact').
card_original_text('misers\' cage'/'MIR', 'At end of target opponent\'s upkeep, if that player has five or more cards in hand, Misers\' Cage deals 2 damage to him or her.').
card_first_print('misers\' cage', 'MIR').
card_image_name('misers\' cage'/'MIR', 'misers\' cage').
card_uid('misers\' cage'/'MIR', 'MIR:Misers\' Cage:misers\' cage').
card_rarity('misers\' cage'/'MIR', 'Rare').
card_artist('misers\' cage'/'MIR', 'Jeff Miracola').
card_flavor_text('misers\' cage'/'MIR', '\"Let him be imprisoned while his possessions wander free.\"\n—Suq\'Ata legal decree').
card_multiverse_id('misers\' cage'/'MIR', '3259').

card_in_set('mist dragon', 'MIR').
card_original_type('mist dragon'/'MIR', 'Summon — Dragon').
card_original_text('mist dragon'/'MIR', '{0}: Flying\n{0}: Loses flying\n{3}{U}{U} Phases out').
card_first_print('mist dragon', 'MIR').
card_image_name('mist dragon'/'MIR', 'mist dragon').
card_uid('mist dragon'/'MIR', 'MIR:Mist Dragon:mist dragon').
card_rarity('mist dragon'/'MIR', 'Rare').
card_artist('mist dragon'/'MIR', 'Al Davidson').
card_flavor_text('mist dragon'/'MIR', '\"Across the plains, farmers who braved the mists began to vanish. Most blamed the goblins; none guessed the truth.\"\n—Hakim, Loreweaver').
card_multiverse_id('mist dragon'/'MIR', '3350').

card_in_set('moss diamond', 'MIR').
card_original_type('moss diamond'/'MIR', 'Artifact').
card_original_text('moss diamond'/'MIR', 'Moss Diamond comes into play tapped.\n{T}: Add {G} to your mana pool. Play this ability as a mana source.').
card_first_print('moss diamond', 'MIR').
card_image_name('moss diamond'/'MIR', 'moss diamond').
card_uid('moss diamond'/'MIR', 'MIR:Moss Diamond:moss diamond').
card_rarity('moss diamond'/'MIR', 'Uncommon').
card_artist('moss diamond'/'MIR', 'Donato Giancola').
card_multiverse_id('moss diamond'/'MIR', '3260').

card_in_set('mountain', 'MIR').
card_original_type('mountain'/'MIR', 'Land').
card_original_text('mountain'/'MIR', '{T}: Add {R} to your mana pool.').
card_image_name('mountain'/'MIR', 'mountain1').
card_uid('mountain'/'MIR', 'MIR:Mountain:mountain1').
card_rarity('mountain'/'MIR', 'Basic Land').
card_artist('mountain'/'MIR', 'John Avon').
card_multiverse_id('mountain'/'MIR', '3580').

card_in_set('mountain', 'MIR').
card_original_type('mountain'/'MIR', 'Land').
card_original_text('mountain'/'MIR', '{T}: Add {R} to your mana pool.').
card_image_name('mountain'/'MIR', 'mountain2').
card_uid('mountain'/'MIR', 'MIR:Mountain:mountain2').
card_rarity('mountain'/'MIR', 'Basic Land').
card_artist('mountain'/'MIR', 'John Avon').
card_multiverse_id('mountain'/'MIR', '3578').

card_in_set('mountain', 'MIR').
card_original_type('mountain'/'MIR', 'Land').
card_original_text('mountain'/'MIR', '{T}: Add {R} to your mana pool.').
card_image_name('mountain'/'MIR', 'mountain3').
card_uid('mountain'/'MIR', 'MIR:Mountain:mountain3').
card_rarity('mountain'/'MIR', 'Basic Land').
card_artist('mountain'/'MIR', 'John Avon').
card_multiverse_id('mountain'/'MIR', '3579').

card_in_set('mountain', 'MIR').
card_original_type('mountain'/'MIR', 'Land').
card_original_text('mountain'/'MIR', '{T}: Add {R} to your mana pool.').
card_image_name('mountain'/'MIR', 'mountain4').
card_uid('mountain'/'MIR', 'MIR:Mountain:mountain4').
card_rarity('mountain'/'MIR', 'Basic Land').
card_artist('mountain'/'MIR', 'John Avon').
card_multiverse_id('mountain'/'MIR', '3577').

card_in_set('mountain valley', 'MIR').
card_original_type('mountain valley'/'MIR', 'Land').
card_original_text('mountain valley'/'MIR', 'Mountain Valley comes into play tapped.\n{T}, Sacrifice Mountain Valley: Search your library for a mountain or forest card. Put that land into play. Shuffle your library afterwards.').
card_first_print('mountain valley', 'MIR').
card_image_name('mountain valley'/'MIR', 'mountain valley').
card_uid('mountain valley'/'MIR', 'MIR:Mountain Valley:mountain valley').
card_rarity('mountain valley'/'MIR', 'Uncommon').
card_artist('mountain valley'/'MIR', 'Kari Johnson').
card_multiverse_id('mountain valley'/'MIR', '3574').

card_in_set('mtenda griffin', 'MIR').
card_original_type('mtenda griffin'/'MIR', 'Summon — Griffin').
card_original_text('mtenda griffin'/'MIR', 'Flying\n{W}, {T}: Return Mtenda Griffin to owner\'s hand and return target Griffin card in your graveyard to your hand. Use this ability only during your upkeep.').
card_first_print('mtenda griffin', 'MIR').
card_image_name('mtenda griffin'/'MIR', 'mtenda griffin').
card_uid('mtenda griffin'/'MIR', 'MIR:Mtenda Griffin:mtenda griffin').
card_rarity('mtenda griffin'/'MIR', 'Uncommon').
card_artist('mtenda griffin'/'MIR', 'Janine Johnston').
card_flavor_text('mtenda griffin'/'MIR', '\"Unlike Zhalfir, the griffin needs no council to keep harmony among its parts.\" —Asmira, Holy Avenger').
card_multiverse_id('mtenda griffin'/'MIR', '3503').

card_in_set('mtenda herder', 'MIR').
card_original_type('mtenda herder'/'MIR', 'Summon — Scout').
card_original_text('mtenda herder'/'MIR', 'Flanking (Whenever a creature without flanking is assigned to block this creature, the blocking creature gets -1/-1 until end of turn.)').
card_first_print('mtenda herder', 'MIR').
card_image_name('mtenda herder'/'MIR', 'mtenda herder').
card_uid('mtenda herder'/'MIR', 'MIR:Mtenda Herder:mtenda herder').
card_rarity('mtenda herder'/'MIR', 'Common').
card_artist('mtenda herder'/'MIR', 'Zina Saunders').
card_flavor_text('mtenda herder'/'MIR', '\"All animals—even humans—can be herded. The trick is to make them think they choose their own destination.\"\n—Mangara').
card_multiverse_id('mtenda herder'/'MIR', '3504').

card_in_set('mtenda lion', 'MIR').
card_original_type('mtenda lion'/'MIR', 'Summon — Lion').
card_original_text('mtenda lion'/'MIR', 'If Mtenda Lion attacks, defending player may pay {U} to have it deal no combat damage this turn.').
card_first_print('mtenda lion', 'MIR').
card_image_name('mtenda lion'/'MIR', 'mtenda lion').
card_uid('mtenda lion'/'MIR', 'MIR:Mtenda Lion:mtenda lion').
card_rarity('mtenda lion'/'MIR', 'Common').
card_artist('mtenda lion'/'MIR', 'Stuart Griffin').
card_flavor_text('mtenda lion'/'MIR', '\"The lion drank that lake right up! In thanks, he gave Siti the power to speak with lions and make them leave the goats alone.\"\n—Afari, Tales').
card_multiverse_id('mtenda lion'/'MIR', '3399').

card_in_set('mystical tutor', 'MIR').
card_original_type('mystical tutor'/'MIR', 'Instant').
card_original_text('mystical tutor'/'MIR', 'Search your library for an instant, interrupt, mana source, or sorcery card and reveal that card to all players. Shuffle your library and put the revealed card back on top of it.').
card_first_print('mystical tutor', 'MIR').
card_image_name('mystical tutor'/'MIR', 'mystical tutor').
card_uid('mystical tutor'/'MIR', 'MIR:Mystical Tutor:mystical tutor').
card_rarity('mystical tutor'/'MIR', 'Uncommon').
card_artist('mystical tutor'/'MIR', 'David O\'Connor').
card_flavor_text('mystical tutor'/'MIR', '\"To the tutors, a ‘poem of sand\' was of little account, a ‘poem of ivory,\' priceless.\"\n—Afari, Tales').
card_multiverse_id('mystical tutor'/'MIR', '3351').

card_in_set('natural balance', 'MIR').
card_original_type('natural balance'/'MIR', 'Sorcery').
card_original_text('natural balance'/'MIR', 'Each player controlling six or more lands sacrifices enough lands to reduce his or her land total to five.\nEach player controlling four or fewer lands may search his or her library for enough basic land to bring his or her land total to five and put those lands into play. Those players shuffle their libraries afterwards.').
card_first_print('natural balance', 'MIR').
card_image_name('natural balance'/'MIR', 'natural balance').
card_uid('natural balance'/'MIR', 'MIR:Natural Balance:natural balance').
card_rarity('natural balance'/'MIR', 'Rare').
card_artist('natural balance'/'MIR', 'John Malloy').
card_multiverse_id('natural balance'/'MIR', '3400').

card_in_set('nettletooth djinn', 'MIR').
card_original_type('nettletooth djinn'/'MIR', 'Summon — Djinn').
card_original_text('nettletooth djinn'/'MIR', 'During your upkeep, Nettletooth Djinn deals 1 damage to you.').
card_first_print('nettletooth djinn', 'MIR').
card_image_name('nettletooth djinn'/'MIR', 'nettletooth djinn').
card_uid('nettletooth djinn'/'MIR', 'MIR:Nettletooth Djinn:nettletooth djinn').
card_rarity('nettletooth djinn'/'MIR', 'Uncommon').
card_artist('nettletooth djinn'/'MIR', 'Janine Johnston').
card_flavor_text('nettletooth djinn'/'MIR', '\"The few of us who survived were denied entry to our camp for two days—so like the pox were the marks we bore.\"\n—Travelogue of Najat').
card_multiverse_id('nettletooth djinn'/'MIR', '3401').

card_in_set('noble elephant', 'MIR').
card_original_type('noble elephant'/'MIR', 'Summon — Elephant').
card_original_text('noble elephant'/'MIR', 'Banding, trample').
card_first_print('noble elephant', 'MIR').
card_image_name('noble elephant'/'MIR', 'noble elephant').
card_uid('noble elephant'/'MIR', 'MIR:Noble Elephant:noble elephant').
card_rarity('noble elephant'/'MIR', 'Common').
card_artist('noble elephant'/'MIR', 'Tony Roberts').
card_flavor_text('noble elephant'/'MIR', '\"Proud am I, / strong am I, / courageous in defending my children / and fierce in punishing what stands in my way.\"\n—\"So the Elephant Speaks,\"\nZhalfirin song').
card_multiverse_id('noble elephant'/'MIR', '3505').

card_in_set('nocturnal raid', 'MIR').
card_original_type('nocturnal raid'/'MIR', 'Instant').
card_original_text('nocturnal raid'/'MIR', 'All black creatures get +2/+0 until end of turn.').
card_first_print('nocturnal raid', 'MIR').
card_image_name('nocturnal raid'/'MIR', 'nocturnal raid').
card_uid('nocturnal raid'/'MIR', 'MIR:Nocturnal Raid:nocturnal raid').
card_rarity('nocturnal raid'/'MIR', 'Uncommon').
card_artist('nocturnal raid'/'MIR', 'John Matson').
card_flavor_text('nocturnal raid'/'MIR', '\"The finest offering of night is not stealth but daring.\"\n—Tetlok, Breathstealer').
card_multiverse_id('nocturnal raid'/'MIR', '3301').

card_in_set('null chamber', 'MIR').
card_original_type('null chamber'/'MIR', 'Enchant World').
card_original_text('null chamber'/'MIR', 'You and target opponent each name any card except a basic land. Those cards cannot be played.').
card_first_print('null chamber', 'MIR').
card_image_name('null chamber'/'MIR', 'null chamber').
card_uid('null chamber'/'MIR', 'MIR:Null Chamber:null chamber').
card_rarity('null chamber'/'MIR', 'Rare').
card_artist('null chamber'/'MIR', 'Douglas Shuler').
card_flavor_text('null chamber'/'MIR', 'Discovered by Femeref mages, the Null Chamber is the ultimate testing ground for the uncreative.').
card_multiverse_id('null chamber'/'MIR', '3506').

card_in_set('pacifism', 'MIR').
card_original_type('pacifism'/'MIR', 'Enchant Creature').
card_original_text('pacifism'/'MIR', 'Enchanted creature cannot attack or block.').
card_first_print('pacifism', 'MIR').
card_image_name('pacifism'/'MIR', 'pacifism').
card_uid('pacifism'/'MIR', 'MIR:Pacifism:pacifism').
card_rarity('pacifism'/'MIR', 'Common').
card_artist('pacifism'/'MIR', 'Robert Bliss').
card_flavor_text('pacifism'/'MIR', 'For the first time in his life, Grakk felt a little warm and fuzzy inside.').
card_multiverse_id('pacifism'/'MIR', '3507').

card_in_set('painful memories', 'MIR').
card_original_type('painful memories'/'MIR', 'Sorcery').
card_original_text('painful memories'/'MIR', 'Look at target opponent\'s hand. Choose one of those cards and put it on top of his or her library.').
card_first_print('painful memories', 'MIR').
card_image_name('painful memories'/'MIR', 'painful memories').
card_uid('painful memories'/'MIR', 'MIR:Painful Memories:painful memories').
card_rarity('painful memories'/'MIR', 'Uncommon').
card_artist('painful memories'/'MIR', 'John Coulthart').
card_flavor_text('painful memories'/'MIR', '\"It is terrible how one brief action can live forever in memory.\"\n—Jolrael').
card_multiverse_id('painful memories'/'MIR', '3302').

card_in_set('patagia golem', 'MIR').
card_original_type('patagia golem'/'MIR', 'Artifact Creature').
card_original_text('patagia golem'/'MIR', '{3}: Flying until end of turn').
card_first_print('patagia golem', 'MIR').
card_image_name('patagia golem'/'MIR', 'patagia golem').
card_uid('patagia golem'/'MIR', 'MIR:Patagia Golem:patagia golem').
card_rarity('patagia golem'/'MIR', 'Uncommon').
card_artist('patagia golem'/'MIR', 'Scott Kirschner').
card_flavor_text('patagia golem'/'MIR', 'It scattered falcons like a lion hunting among jackals.').
card_multiverse_id('patagia golem'/'MIR', '3261').

card_in_set('paupers\' cage', 'MIR').
card_original_type('paupers\' cage'/'MIR', 'Artifact').
card_original_text('paupers\' cage'/'MIR', 'At end of target opponent\'s upkeep, if that player has two or fewer cards in hand, Paupers\' Cage deals 2 damage to him or her.').
card_first_print('paupers\' cage', 'MIR').
card_image_name('paupers\' cage'/'MIR', 'paupers\' cage').
card_uid('paupers\' cage'/'MIR', 'MIR:Paupers\' Cage:paupers\' cage').
card_rarity('paupers\' cage'/'MIR', 'Rare').
card_artist('paupers\' cage'/'MIR', 'Mike Kimble').
card_flavor_text('paupers\' cage'/'MIR', 'A cage may keep you in, but it does not keep the stones out.').
card_multiverse_id('paupers\' cage'/'MIR', '3262').

card_in_set('pearl dragon', 'MIR').
card_original_type('pearl dragon'/'MIR', 'Summon — Dragon').
card_original_text('pearl dragon'/'MIR', 'Flying\n{1}{W} +0/+1 until end of turn').
card_first_print('pearl dragon', 'MIR').
card_image_name('pearl dragon'/'MIR', 'pearl dragon').
card_uid('pearl dragon'/'MIR', 'MIR:Pearl Dragon:pearl dragon').
card_rarity('pearl dragon'/'MIR', 'Rare').
card_artist('pearl dragon'/'MIR', 'Ian Miller').
card_flavor_text('pearl dragon'/'MIR', '\"They knew Bantau was lost when, to hide the pearl he had found on the beach, he swallowed it—and soon grew wings.\"\n—Hakim, Loreweaver').
card_multiverse_id('pearl dragon'/'MIR', '3508').

card_in_set('phyrexian dreadnought', 'MIR').
card_original_type('phyrexian dreadnought'/'MIR', 'Artifact Creature').
card_original_text('phyrexian dreadnought'/'MIR', 'Trample\nWhen Phyrexian Dreadnought comes into play, sacrifice any number of creatures with total power of 12 or more, or bury Phyrexian Dreadnought.').
card_first_print('phyrexian dreadnought', 'MIR').
card_image_name('phyrexian dreadnought'/'MIR', 'phyrexian dreadnought').
card_uid('phyrexian dreadnought'/'MIR', 'MIR:Phyrexian Dreadnought:phyrexian dreadnought').
card_rarity('phyrexian dreadnought'/'MIR', 'Rare').
card_artist('phyrexian dreadnought'/'MIR', 'Pete Venters').
card_multiverse_id('phyrexian dreadnought'/'MIR', '3263').

card_in_set('phyrexian purge', 'MIR').
card_original_type('phyrexian purge'/'MIR', 'Sorcery').
card_original_text('phyrexian purge'/'MIR', 'Pay 3 life per target: Destroy any number of target creatures.').
card_first_print('phyrexian purge', 'MIR').
card_image_name('phyrexian purge'/'MIR', 'phyrexian purge').
card_uid('phyrexian purge'/'MIR', 'MIR:Phyrexian Purge:phyrexian purge').
card_rarity('phyrexian purge'/'MIR', 'Rare').
card_artist('phyrexian purge'/'MIR', 'Robert Bliss').
card_flavor_text('phyrexian purge'/'MIR', 'Only those who have nothing have nothing to fear.').
card_multiverse_id('phyrexian purge'/'MIR', '3544').

card_in_set('phyrexian tribute', 'MIR').
card_original_type('phyrexian tribute'/'MIR', 'Sorcery').
card_original_text('phyrexian tribute'/'MIR', 'Sacrifice two creatures: Destroy target artifact.').
card_first_print('phyrexian tribute', 'MIR').
card_image_name('phyrexian tribute'/'MIR', 'phyrexian tribute').
card_uid('phyrexian tribute'/'MIR', 'MIR:Phyrexian Tribute:phyrexian tribute').
card_rarity('phyrexian tribute'/'MIR', 'Rare').
card_artist('phyrexian tribute'/'MIR', 'John Matson').
card_flavor_text('phyrexian tribute'/'MIR', '\"In your final breath you still have something to offer Phyrexia.\"\n—Afari, Tales').
card_multiverse_id('phyrexian tribute'/'MIR', '3303').

card_in_set('phyrexian vault', 'MIR').
card_original_type('phyrexian vault'/'MIR', 'Artifact').
card_original_text('phyrexian vault'/'MIR', '{2}, {T}, Sacrifice a creature: Draw a card.').
card_first_print('phyrexian vault', 'MIR').
card_image_name('phyrexian vault'/'MIR', 'phyrexian vault').
card_uid('phyrexian vault'/'MIR', 'MIR:Phyrexian Vault:phyrexian vault').
card_rarity('phyrexian vault'/'MIR', 'Uncommon').
card_artist('phyrexian vault'/'MIR', 'Hannibal King').
card_flavor_text('phyrexian vault'/'MIR', '\"The secrets of Phyrexia are expensive. You will pay in brass and bone, steel and sinew.\"\n—Kaervek').
card_multiverse_id('phyrexian vault'/'MIR', '3264').

card_in_set('plains', 'MIR').
card_original_type('plains'/'MIR', 'Land').
card_original_text('plains'/'MIR', '{T}: Add {W} to your mana pool.').
card_image_name('plains'/'MIR', 'plains1').
card_uid('plains'/'MIR', 'MIR:Plains:plains1').
card_rarity('plains'/'MIR', 'Basic Land').
card_artist('plains'/'MIR', 'Tom Wänerstrand').
card_multiverse_id('plains'/'MIR', '3585').

card_in_set('plains', 'MIR').
card_original_type('plains'/'MIR', 'Land').
card_original_text('plains'/'MIR', '{T}: Add {W} to your mana pool.').
card_image_name('plains'/'MIR', 'plains2').
card_uid('plains'/'MIR', 'MIR:Plains:plains2').
card_rarity('plains'/'MIR', 'Basic Land').
card_artist('plains'/'MIR', 'Tom Wänerstrand').
card_multiverse_id('plains'/'MIR', '3587').

card_in_set('plains', 'MIR').
card_original_type('plains'/'MIR', 'Land').
card_original_text('plains'/'MIR', '{T}: Add {W} to your mana pool.').
card_image_name('plains'/'MIR', 'plains3').
card_uid('plains'/'MIR', 'MIR:Plains:plains3').
card_rarity('plains'/'MIR', 'Basic Land').
card_artist('plains'/'MIR', 'Tom Wänerstrand').
card_multiverse_id('plains'/'MIR', '3586').

card_in_set('plains', 'MIR').
card_original_type('plains'/'MIR', 'Land').
card_original_text('plains'/'MIR', '{T}: Add {W} to your mana pool.').
card_image_name('plains'/'MIR', 'plains4').
card_uid('plains'/'MIR', 'MIR:Plains:plains4').
card_rarity('plains'/'MIR', 'Basic Land').
card_artist('plains'/'MIR', 'Tom Wänerstrand').
card_multiverse_id('plains'/'MIR', '3588').

card_in_set('political trickery', 'MIR').
card_original_type('political trickery'/'MIR', 'Sorcery').
card_original_text('political trickery'/'MIR', 'Choose target land you control and target land an opponent controls. Exchange control of those lands.').
card_first_print('political trickery', 'MIR').
card_image_name('political trickery'/'MIR', 'political trickery').
card_uid('political trickery'/'MIR', 'MIR:Political Trickery:political trickery').
card_rarity('political trickery'/'MIR', 'Rare').
card_artist('political trickery'/'MIR', 'Scott Kirschner').
card_flavor_text('political trickery'/'MIR', '\"Politics is a game—move a stone here, move a stone there—except sometimes the stones bleed.\"\n—Shauku, Endbringer').
card_multiverse_id('political trickery'/'MIR', '3352').

card_in_set('polymorph', 'MIR').
card_original_type('polymorph'/'MIR', 'Sorcery').
card_original_text('polymorph'/'MIR', 'Bury target creature. That creature\'s controller reveals cards from the top of his or her library until a creature card is revealed and then puts that creature into play under his or her control as though it were just played. The player shuffles all other revealed cards into his or her library.').
card_first_print('polymorph', 'MIR').
card_image_name('polymorph'/'MIR', 'polymorph').
card_uid('polymorph'/'MIR', 'MIR:Polymorph:polymorph').
card_rarity('polymorph'/'MIR', 'Rare').
card_artist('polymorph'/'MIR', 'Robert Bliss').
card_flavor_text('polymorph'/'MIR', '\"Ahh! Opposable digits!\"').
card_multiverse_id('polymorph'/'MIR', '3353').

card_in_set('power sink', 'MIR').
card_original_type('power sink'/'MIR', 'Interrupt').
card_original_text('power sink'/'MIR', 'Counter target spell unless that spell\'s caster pays an additional {X}. That player draws and pays all mana from lands and mana pool until {X} is paid; he or she may also draw and pay mana from other sources if desired.').
card_image_name('power sink'/'MIR', 'power sink').
card_uid('power sink'/'MIR', 'MIR:Power Sink:power sink').
card_rarity('power sink'/'MIR', 'Common').
card_artist('power sink'/'MIR', 'Charles Gillespie').
card_multiverse_id('power sink'/'MIR', '3354').

card_in_set('preferred selection', 'MIR').
card_original_type('preferred selection'/'MIR', 'Enchantment').
card_original_text('preferred selection'/'MIR', 'At the beginning of your draw phase, look at the top two cards of your library and choose one. Put that card on the bottom of your library, or sacrifice Preferred Selection and pay {2}{G}{G} to draw the card.').
card_first_print('preferred selection', 'MIR').
card_image_name('preferred selection'/'MIR', 'preferred selection').
card_uid('preferred selection'/'MIR', 'MIR:Preferred Selection:preferred selection').
card_rarity('preferred selection'/'MIR', 'Rare').
card_artist('preferred selection'/'MIR', 'Kev Walker').
card_multiverse_id('preferred selection'/'MIR', '3402').

card_in_set('prismatic boon', 'MIR').
card_original_type('prismatic boon'/'MIR', 'Instant').
card_original_text('prismatic boon'/'MIR', 'X target creatures gain protection from a single color of your choice until end of turn.').
card_first_print('prismatic boon', 'MIR').
card_image_name('prismatic boon'/'MIR', 'prismatic boon').
card_uid('prismatic boon'/'MIR', 'MIR:Prismatic Boon:prismatic boon').
card_rarity('prismatic boon'/'MIR', 'Uncommon').
card_artist('prismatic boon'/'MIR', 'Thomas Gianni').
card_flavor_text('prismatic boon'/'MIR', '\"How ironic that the greatest forge of civilization is battle.\"\n—Mangara').
card_multiverse_id('prismatic boon'/'MIR', '3545').

card_in_set('prismatic circle', 'MIR').
card_original_type('prismatic circle'/'MIR', 'Enchantment').
card_original_text('prismatic circle'/'MIR', 'Cumulative upkeep {1}\nWhen you play Prismatic Circle, choose a color.\n{1}: Prevent all damage to you from a source of the chosen color. Treat further damage from that source normally.').
card_first_print('prismatic circle', 'MIR').
card_image_name('prismatic circle'/'MIR', 'prismatic circle').
card_uid('prismatic circle'/'MIR', 'MIR:Prismatic Circle:prismatic circle').
card_rarity('prismatic circle'/'MIR', 'Common').
card_artist('prismatic circle'/'MIR', 'Pete Venters').
card_multiverse_id('prismatic circle'/'MIR', '3509').

card_in_set('prismatic lace', 'MIR').
card_original_type('prismatic lace'/'MIR', 'Instant').
card_original_text('prismatic lace'/'MIR', 'Target permanent becomes the color(s) of your choice. Costs to tap, maintain, or use an ability of that permanent remain unchanged.').
card_first_print('prismatic lace', 'MIR').
card_image_name('prismatic lace'/'MIR', 'prismatic lace').
card_uid('prismatic lace'/'MIR', 'MIR:Prismatic Lace:prismatic lace').
card_rarity('prismatic lace'/'MIR', 'Rare').
card_artist('prismatic lace'/'MIR', 'David O\'Connor').
card_multiverse_id('prismatic lace'/'MIR', '3355').

card_in_set('psychic transfer', 'MIR').
card_original_type('psychic transfer'/'MIR', 'Sorcery').
card_original_text('psychic transfer'/'MIR', 'Compare your life total with target player\'s life total. If the difference is 5 or less and you have at least 1 life, exchange life totals with that player.').
card_first_print('psychic transfer', 'MIR').
card_image_name('psychic transfer'/'MIR', 'psychic transfer').
card_uid('psychic transfer'/'MIR', 'MIR:Psychic Transfer:psychic transfer').
card_rarity('psychic transfer'/'MIR', 'Rare').
card_artist('psychic transfer'/'MIR', 'Dom!').
card_flavor_text('psychic transfer'/'MIR', '\"The memory of your existence will fade like the final stars of morning.\"\n—Kaervek').
card_multiverse_id('psychic transfer'/'MIR', '3356').

card_in_set('purgatory', 'MIR').
card_original_type('purgatory'/'MIR', 'Enchantment').
card_original_text('purgatory'/'MIR', 'Whenever a summon card is put into your graveyard from play, put that card face up under Purgatory.\nDuring your upkeep, you may pay {4} and 2 life to put any card under Purgatory into play as though it were just played.\nIf Purgatory leaves play, remove all cards under if from the game.').
card_first_print('purgatory', 'MIR').
card_image_name('purgatory'/'MIR', 'purgatory').
card_uid('purgatory'/'MIR', 'MIR:Purgatory:purgatory').
card_rarity('purgatory'/'MIR', 'Rare').
card_artist('purgatory'/'MIR', 'Mike Dringenberg').
card_multiverse_id('purgatory'/'MIR', '3546').

card_in_set('purraj of urborg', 'MIR').
card_original_type('purraj of urborg'/'MIR', 'Summon — Legend').
card_original_text('purraj of urborg'/'MIR', 'First strike when attacking\n{B} Put a +1/+1 counter on Purraj of Urborg. Use this ability only when a black spell is successfully cast and only once for each such spell.').
card_first_print('purraj of urborg', 'MIR').
card_image_name('purraj of urborg'/'MIR', 'purraj of urborg').
card_uid('purraj of urborg'/'MIR', 'MIR:Purraj of Urborg:purraj of urborg').
card_rarity('purraj of urborg'/'MIR', 'Rare').
card_artist('purraj of urborg'/'MIR', 'John Matson').
card_multiverse_id('purraj of urborg'/'MIR', '3304').

card_in_set('pyric salamander', 'MIR').
card_original_type('pyric salamander'/'MIR', 'Summon — Salamander').
card_original_text('pyric salamander'/'MIR', '{R} +1/+0 until end of turn. Bury Pyric Salamander at end of turn.').
card_first_print('pyric salamander', 'MIR').
card_image_name('pyric salamander'/'MIR', 'pyric salamander').
card_uid('pyric salamander'/'MIR', 'MIR:Pyric Salamander:pyric salamander').
card_rarity('pyric salamander'/'MIR', 'Common').
card_artist('pyric salamander'/'MIR', 'Tony Roberts').
card_flavor_text('pyric salamander'/'MIR', 'When you\'ve been chased by a dragon, you run from every lizard.\n—Suq\'Ata saying').
card_multiverse_id('pyric salamander'/'MIR', '3458').

card_in_set('quirion elves', 'MIR').
card_original_type('quirion elves'/'MIR', 'Summon — Elves').
card_original_text('quirion elves'/'MIR', 'When you play Quirion Elves, choose a color.\n{T}: Add one mana of the chosen color to your mana pool. Play this ability as a mana source.\n{T}: Add {G} to your mana pool. Play this ability as a mana source.').
card_first_print('quirion elves', 'MIR').
card_image_name('quirion elves'/'MIR', 'quirion elves').
card_uid('quirion elves'/'MIR', 'MIR:Quirion Elves:quirion elves').
card_rarity('quirion elves'/'MIR', 'Common').
card_artist('quirion elves'/'MIR', 'Randy Gallegos').
card_multiverse_id('quirion elves'/'MIR', '3403').

card_in_set('radiant essence', 'MIR').
card_original_type('radiant essence'/'MIR', 'Summon — Spirit').
card_original_text('radiant essence'/'MIR', 'As long as target opponent controls any black permanents, Radiant Essence gets +1/+2.').
card_first_print('radiant essence', 'MIR').
card_image_name('radiant essence'/'MIR', 'radiant essence').
card_uid('radiant essence'/'MIR', 'MIR:Radiant Essence:radiant essence').
card_rarity('radiant essence'/'MIR', 'Uncommon').
card_artist('radiant essence'/'MIR', 'Jennifer Law').
card_flavor_text('radiant essence'/'MIR', '\"Her beauty was the beauty of the Sun at midday, deadly to those who underestimated her strength.\"\n—Azeworai, \"Kenechi\'s Dream\"').
card_multiverse_id('radiant essence'/'MIR', '3547').

card_in_set('raging spirit', 'MIR').
card_original_type('raging spirit'/'MIR', 'Summon — Spirit').
card_original_text('raging spirit'/'MIR', '{2}: Raging Spirit is colorless until end of turn.').
card_first_print('raging spirit', 'MIR').
card_image_name('raging spirit'/'MIR', 'raging spirit').
card_uid('raging spirit'/'MIR', 'MIR:Raging Spirit:raging spirit').
card_rarity('raging spirit'/'MIR', 'Common').
card_artist('raging spirit'/'MIR', 'Scott M. Fischer').
card_flavor_text('raging spirit'/'MIR', '\"Its burning power is more elusive than shade on the Unyaro Plains, more perilous than the horn of a rhino.\"\n—Akin, seasoned askari').
card_multiverse_id('raging spirit'/'MIR', '3459').

card_in_set('rampant growth', 'MIR').
card_original_type('rampant growth'/'MIR', 'Sorcery').
card_original_text('rampant growth'/'MIR', 'Search your library for a basic land card and put it into play, tapped. Shuffle your library afterwards.').
card_first_print('rampant growth', 'MIR').
card_image_name('rampant growth'/'MIR', 'rampant growth').
card_uid('rampant growth'/'MIR', 'MIR:Rampant Growth:rampant growth').
card_rarity('rampant growth'/'MIR', 'Common').
card_artist('rampant growth'/'MIR', 'Pat Morrissey').
card_flavor_text('rampant growth'/'MIR', '\"I\'ve never heard growth before.\"\n—Gowon, Femeref herder').
card_multiverse_id('rampant growth'/'MIR', '3404').

card_in_set('rashida scalebane', 'MIR').
card_original_type('rashida scalebane'/'MIR', 'Summon — Legend').
card_original_text('rashida scalebane'/'MIR', '{T}: Bury target attacking or blocking Dragon. Gain an amount of life equal to that Dragon\'s power.').
card_first_print('rashida scalebane', 'MIR').
card_image_name('rashida scalebane'/'MIR', 'rashida scalebane').
card_uid('rashida scalebane'/'MIR', 'MIR:Rashida Scalebane:rashida scalebane').
card_rarity('rashida scalebane'/'MIR', 'Rare').
card_artist('rashida scalebane'/'MIR', 'Randy Gallegos').
card_flavor_text('rashida scalebane'/'MIR', 'It is not by the blood of dragons that Rashida measures victories, but by the sum of children who no longer wake from nightmares of scales and fire.').
card_multiverse_id('rashida scalebane'/'MIR', '3510').

card_in_set('ravenous vampire', 'MIR').
card_original_type('ravenous vampire'/'MIR', 'Summon — Vampire').
card_original_text('ravenous vampire'/'MIR', 'Flying\nDuring your upkeep, sacrifice a nonartifact creature and put a +1/+1 counter on Ravenous Vampire, or tap Ravenous Vampire.').
card_first_print('ravenous vampire', 'MIR').
card_image_name('ravenous vampire'/'MIR', 'ravenous vampire').
card_uid('ravenous vampire'/'MIR', 'MIR:Ravenous Vampire:ravenous vampire').
card_rarity('ravenous vampire'/'MIR', 'Uncommon').
card_artist('ravenous vampire'/'MIR', 'John Bolton').
card_flavor_text('ravenous vampire'/'MIR', 'Eternal life brings eternal hunger.').
card_multiverse_id('ravenous vampire'/'MIR', '3305').

card_in_set('ray of command', 'MIR').
card_original_type('ray of command'/'MIR', 'Instant').
card_original_text('ray of command'/'MIR', 'Untap target creature an opponent controls and gain control of it until end of turn. That creature is unaffected by summoning sickness this turn. Tap the creature if you lose control of it at end of this turn.').
card_image_name('ray of command'/'MIR', 'ray of command').
card_uid('ray of command'/'MIR', 'MIR:Ray of Command:ray of command').
card_rarity('ray of command'/'MIR', 'Common').
card_artist('ray of command'/'MIR', 'Andrew Robinson').
card_flavor_text('ray of command'/'MIR', '\"Heel.\"').
card_multiverse_id('ray of command'/'MIR', '3357').

card_in_set('razor pendulum', 'MIR').
card_original_type('razor pendulum'/'MIR', 'Artifact').
card_original_text('razor pendulum'/'MIR', 'At the end of each player\'s turn, if that player has 5 or less life, Razor Pendulum deals 2 damage to him or her.').
card_first_print('razor pendulum', 'MIR').
card_image_name('razor pendulum'/'MIR', 'razor pendulum').
card_uid('razor pendulum'/'MIR', 'MIR:Razor Pendulum:razor pendulum').
card_rarity('razor pendulum'/'MIR', 'Rare').
card_artist('razor pendulum'/'MIR', 'Zak Plucinski').
card_flavor_text('razor pendulum'/'MIR', '\"Amazing thing, gravity. It seems to work every time.\"\n—Telim\'Tor').
card_multiverse_id('razor pendulum'/'MIR', '3265').

card_in_set('reality ripple', 'MIR').
card_original_type('reality ripple'/'MIR', 'Instant').
card_original_text('reality ripple'/'MIR', 'Target artifact, creature, or land phases out.').
card_first_print('reality ripple', 'MIR').
card_image_name('reality ripple'/'MIR', 'reality ripple').
card_uid('reality ripple'/'MIR', 'MIR:Reality Ripple:reality ripple').
card_rarity('reality ripple'/'MIR', 'Common').
card_artist('reality ripple'/'MIR', 'Alan Rabinowitz').
card_flavor_text('reality ripple'/'MIR', '\"Can you prepare for the unexpected? No, you cannot. You can prepare only to be surprised.\"\n—Teferi').
card_multiverse_id('reality ripple'/'MIR', '3358').

card_in_set('reckless embermage', 'MIR').
card_original_type('reckless embermage'/'MIR', 'Summon — Wizard').
card_original_text('reckless embermage'/'MIR', '{1}{R} Reckless Embermage deals 1 damage to target creature or player and 1 damage to itself.').
card_first_print('reckless embermage', 'MIR').
card_image_name('reckless embermage'/'MIR', 'reckless embermage').
card_uid('reckless embermage'/'MIR', 'MIR:Reckless Embermage:reckless embermage').
card_rarity('reckless embermage'/'MIR', 'Rare').
card_artist('reckless embermage'/'MIR', 'Tom Kyffin').
card_flavor_text('reckless embermage'/'MIR', 'Many mages have been consumed by their work.').
card_multiverse_id('reckless embermage'/'MIR', '3460').

card_in_set('reflect damage', 'MIR').
card_original_type('reflect damage'/'MIR', 'Instant').
card_original_text('reflect damage'/'MIR', 'Redirect all damage dealt by any one source to that source\'s controller.').
card_first_print('reflect damage', 'MIR').
card_image_name('reflect damage'/'MIR', 'reflect damage').
card_uid('reflect damage'/'MIR', 'MIR:Reflect Damage:reflect damage').
card_rarity('reflect damage'/'MIR', 'Rare').
card_artist('reflect damage'/'MIR', 'Ron Spencer').
card_flavor_text('reflect damage'/'MIR', 'From you to me, so me to you.\n—Femeref children\'s taunt').
card_multiverse_id('reflect damage'/'MIR', '3548').

card_in_set('regeneration', 'MIR').
card_original_type('regeneration'/'MIR', 'Enchant Creature').
card_original_text('regeneration'/'MIR', '{G} Regenerate enchanted creature.').
card_image_name('regeneration'/'MIR', 'regeneration').
card_uid('regeneration'/'MIR', 'MIR:Regeneration:regeneration').
card_rarity('regeneration'/'MIR', 'Common').
card_artist('regeneration'/'MIR', 'Charles Gillespie').
card_flavor_text('regeneration'/'MIR', '\"Death is not a debt I am yet willing to pay.\"\n—Purraj of Urborg').
card_multiverse_id('regeneration'/'MIR', '3405').

card_in_set('reign of chaos', 'MIR').
card_original_type('reign of chaos'/'MIR', 'Sorcery').
card_original_text('reign of chaos'/'MIR', 'Destroy target plains and target white creature, or destroy target island and target blue creature.').
card_first_print('reign of chaos', 'MIR').
card_image_name('reign of chaos'/'MIR', 'reign of chaos').
card_uid('reign of chaos'/'MIR', 'MIR:Reign of Chaos:reign of chaos').
card_rarity('reign of chaos'/'MIR', 'Uncommon').
card_artist('reign of chaos'/'MIR', 'Kathryn Rathke').
card_flavor_text('reign of chaos'/'MIR', '\"The Burning Isles are the home to a dozen lords who claim dominion. Jamuraa will look to only one leader: me.\"\n—Kaervek').
card_multiverse_id('reign of chaos'/'MIR', '3461').

card_in_set('reign of terror', 'MIR').
card_original_type('reign of terror'/'MIR', 'Sorcery').
card_original_text('reign of terror'/'MIR', 'Bury all white creatures or bury all green creatures. Lose 2 life for each creature put into the graveyard in this way.').
card_first_print('reign of terror', 'MIR').
card_image_name('reign of terror'/'MIR', 'reign of terror').
card_uid('reign of terror'/'MIR', 'MIR:Reign of Terror:reign of terror').
card_rarity('reign of terror'/'MIR', 'Uncommon').
card_artist('reign of terror'/'MIR', 'Gary Leach').
card_flavor_text('reign of terror'/'MIR', '\"I don\'t know what takes them; they die around me without time to scream.\"\n—Scout Ekemet, final journal').
card_multiverse_id('reign of terror'/'MIR', '3306').

card_in_set('reparations', 'MIR').
card_original_type('reparations'/'MIR', 'Enchantment').
card_original_text('reparations'/'MIR', 'Whenever target opponent successfully casts a spell that targets you or a creature you control, you may draw a card.').
card_first_print('reparations', 'MIR').
card_image_name('reparations'/'MIR', 'reparations').
card_uid('reparations'/'MIR', 'MIR:Reparations:reparations').
card_rarity('reparations'/'MIR', 'Rare').
card_artist('reparations'/'MIR', 'Douglas Shuler').
card_flavor_text('reparations'/'MIR', '\"Sorry I burned down your village. Here\'s some gold.\"').
card_multiverse_id('reparations'/'MIR', '3549').

card_in_set('restless dead', 'MIR').
card_original_type('restless dead'/'MIR', 'Summon — Skeletons').
card_original_text('restless dead'/'MIR', '{B} Regenerate').
card_first_print('restless dead', 'MIR').
card_image_name('restless dead'/'MIR', 'restless dead').
card_uid('restless dead'/'MIR', 'MIR:Restless Dead:restless dead').
card_rarity('restless dead'/'MIR', 'Common').
card_artist('restless dead'/'MIR', 'Ian Miller').
card_flavor_text('restless dead'/'MIR', 'The rich\'s heirs often thank them after death . . . but preferably not in person.\n—Suq\'Ata epigram').
card_multiverse_id('restless dead'/'MIR', '3307').

card_in_set('ritual of steel', 'MIR').
card_original_type('ritual of steel'/'MIR', 'Enchant Creature').
card_original_text('ritual of steel'/'MIR', 'Draw a card at the beginning of the upkeep of the turn after Ritual of Steel comes into play.\nEnchanted creature gets +0/+2.').
card_first_print('ritual of steel', 'MIR').
card_image_name('ritual of steel'/'MIR', 'ritual of steel').
card_uid('ritual of steel'/'MIR', 'MIR:Ritual of Steel:ritual of steel').
card_rarity('ritual of steel'/'MIR', 'Common').
card_artist('ritual of steel'/'MIR', 'Mark Poole').
card_flavor_text('ritual of steel'/'MIR', '\"Our soldiers are blessed with lizard scales as hard and cold as steel.\"\n—Sidar Jabari').
card_multiverse_id('ritual of steel'/'MIR', '3511').

card_in_set('rock basilisk', 'MIR').
card_original_type('rock basilisk'/'MIR', 'Summon — Basilisk').
card_original_text('rock basilisk'/'MIR', 'Whenever Rock Basilisk blocks or is blocked by a non-Wall creature, destroy that creature at end of combat.').
card_first_print('rock basilisk', 'MIR').
card_image_name('rock basilisk'/'MIR', 'rock basilisk').
card_uid('rock basilisk'/'MIR', 'MIR:Rock Basilisk:rock basilisk').
card_rarity('rock basilisk'/'MIR', 'Rare').
card_artist('rock basilisk'/'MIR', 'Ian Miller').
card_flavor_text('rock basilisk'/'MIR', 'In the eastern reaches of Jamuraa are arrangements of massive stones. No one knows their origins—for certain.').
card_multiverse_id('rock basilisk'/'MIR', '3550').

card_in_set('rocky tar pit', 'MIR').
card_original_type('rocky tar pit'/'MIR', 'Land').
card_original_text('rocky tar pit'/'MIR', 'Rocky Tar Pit comes into play tapped.\n{T}, Sacrifice Rocky Tar Pit: Search your library for a swamp or mountain card. Put that land into play. Shuffle your library afterwards.').
card_first_print('rocky tar pit', 'MIR').
card_image_name('rocky tar pit'/'MIR', 'rocky tar pit').
card_uid('rocky tar pit'/'MIR', 'MIR:Rocky Tar Pit:rocky tar pit').
card_rarity('rocky tar pit'/'MIR', 'Uncommon').
card_artist('rocky tar pit'/'MIR', 'Jeff Miracola').
card_multiverse_id('rocky tar pit'/'MIR', '3575').

card_in_set('roots of life', 'MIR').
card_original_type('roots of life'/'MIR', 'Enchantment').
card_original_text('roots of life'/'MIR', 'When you play Roots of Life, choose islands or swamps.\nWhenever a land of the chosen type that target opponent controls becomes tapped, gain 1 life.').
card_first_print('roots of life', 'MIR').
card_image_name('roots of life'/'MIR', 'roots of life').
card_uid('roots of life'/'MIR', 'MIR:Roots of Life:roots of life').
card_rarity('roots of life'/'MIR', 'Uncommon').
card_artist('roots of life'/'MIR', 'Tony Roberts').
card_multiverse_id('roots of life'/'MIR', '3406').

card_in_set('sabertooth cobra', 'MIR').
card_original_type('sabertooth cobra'/'MIR', 'Summon — Cobra').
card_original_text('sabertooth cobra'/'MIR', 'If Sabertooth Cobra damages a player, he or she gets a poison counter. During that player\'s next upkeep, he or she gets another poison counter unless he or she pays {2} before then to prevent this effect. If any player has ten or more poison counters, he or she loses the game.').
card_first_print('sabertooth cobra', 'MIR').
card_image_name('sabertooth cobra'/'MIR', 'sabertooth cobra').
card_uid('sabertooth cobra'/'MIR', 'MIR:Sabertooth Cobra:sabertooth cobra').
card_rarity('sabertooth cobra'/'MIR', 'Common').
card_artist('sabertooth cobra'/'MIR', 'Andrew Robinson').
card_multiverse_id('sabertooth cobra'/'MIR', '3407').

card_in_set('sacred mesa', 'MIR').
card_original_type('sacred mesa'/'MIR', 'Enchantment').
card_original_text('sacred mesa'/'MIR', 'During your upkeep, sacrifice a Pegasus or bury Sacred Mesa.\n{1}{W} Put a Wild Pegasus token into play. Treat this token as a 1/1 white creature with flying that counts as a Pegasus.').
card_first_print('sacred mesa', 'MIR').
card_image_name('sacred mesa'/'MIR', 'sacred mesa').
card_uid('sacred mesa'/'MIR', 'MIR:Sacred Mesa:sacred mesa').
card_rarity('sacred mesa'/'MIR', 'Rare').
card_artist('sacred mesa'/'MIR', 'Margaret Organ-Kean').
card_flavor_text('sacred mesa'/'MIR', '\"Do not go there, do not go / unless you rise on wings, unless you walk on hooves.\"\n—\"Song to the Sun,\" Femeref song').
card_multiverse_id('sacred mesa'/'MIR', '3512').

card_in_set('sand golem', 'MIR').
card_original_type('sand golem'/'MIR', 'Artifact Creature').
card_original_text('sand golem'/'MIR', 'If a spell or effect controlled by an opponent causes you to discard Sand Golem, put Sand Golem from your graveyard into play at end of turn with a +1/+1 counter on it.').
card_first_print('sand golem', 'MIR').
card_image_name('sand golem'/'MIR', 'sand golem').
card_uid('sand golem'/'MIR', 'MIR:Sand Golem:sand golem').
card_rarity('sand golem'/'MIR', 'Uncommon').
card_artist('sand golem'/'MIR', 'John Matson').
card_multiverse_id('sand golem'/'MIR', '3266').

card_in_set('sandbar crocodile', 'MIR').
card_original_type('sandbar crocodile'/'MIR', 'Summon — Crocodile').
card_original_text('sandbar crocodile'/'MIR', 'Phasing').
card_first_print('sandbar crocodile', 'MIR').
card_image_name('sandbar crocodile'/'MIR', 'sandbar crocodile').
card_uid('sandbar crocodile'/'MIR', 'MIR:Sandbar Crocodile:sandbar crocodile').
card_rarity('sandbar crocodile'/'MIR', 'Common').
card_artist('sandbar crocodile'/'MIR', 'Una Fricker').
card_flavor_text('sandbar crocodile'/'MIR', '\"We\'ve run aground sandbars before, but today, a sandbar ran aground us.\"\n—Kipkemboi, Kukemssa pirate').
card_multiverse_id('sandbar crocodile'/'MIR', '3359').

card_in_set('sandstorm', 'MIR').
card_original_type('sandstorm'/'MIR', 'Instant').
card_original_text('sandstorm'/'MIR', 'Sandstorm deals 1 damage to each attacking creature.').
card_image_name('sandstorm'/'MIR', 'sandstorm').
card_uid('sandstorm'/'MIR', 'MIR:Sandstorm:sandstorm').
card_rarity('sandstorm'/'MIR', 'Common').
card_artist('sandstorm'/'MIR', 'Mike Kimble').
card_flavor_text('sandstorm'/'MIR', 'Better to fight the sand for a few hours than to sleep beneath it forever.\n—Suq\'Ata truism').
card_multiverse_id('sandstorm'/'MIR', '3408').

card_in_set('sapphire charm', 'MIR').
card_original_type('sapphire charm'/'MIR', 'Instant').
card_original_text('sapphire charm'/'MIR', 'Choose one Target player draws a card at the beginning of the next turn\'s upkeep; or target creature an opponent controls phases out; or target creature gains flying until end of turn.').
card_first_print('sapphire charm', 'MIR').
card_image_name('sapphire charm'/'MIR', 'sapphire charm').
card_uid('sapphire charm'/'MIR', 'MIR:Sapphire Charm:sapphire charm').
card_rarity('sapphire charm'/'MIR', 'Common').
card_artist('sapphire charm'/'MIR', 'Steve Luke').
card_multiverse_id('sapphire charm'/'MIR', '3360').

card_in_set('savage twister', 'MIR').
card_original_type('savage twister'/'MIR', 'Sorcery').
card_original_text('savage twister'/'MIR', 'Savage Twister deals X damage to each creature.').
card_first_print('savage twister', 'MIR').
card_image_name('savage twister'/'MIR', 'savage twister').
card_uid('savage twister'/'MIR', 'MIR:Savage Twister:savage twister').
card_rarity('savage twister'/'MIR', 'Uncommon').
card_artist('savage twister'/'MIR', 'Bob Eggleton').
card_flavor_text('savage twister'/'MIR', '\"Frozen, we watched the funnel pluck up three of the goats—pook! pook! pook!—before we ran for the wadi.\"\n—Travelogue of Najat').
card_multiverse_id('savage twister'/'MIR', '3551').

card_in_set('sawback manticore', 'MIR').
card_original_type('sawback manticore'/'MIR', 'Summon — Manticore').
card_original_text('sawback manticore'/'MIR', '{4}: Flying until end of turn\n{1}: Sawback Manticore deals 2 damage to target attacking or blocking creature. Use this ability only once each turn and only if Sawback Manticore is attacking or blocking.').
card_first_print('sawback manticore', 'MIR').
card_image_name('sawback manticore'/'MIR', 'sawback manticore').
card_uid('sawback manticore'/'MIR', 'MIR:Sawback Manticore:sawback manticore').
card_rarity('sawback manticore'/'MIR', 'Rare').
card_artist('sawback manticore'/'MIR', 'Martin McKenna').
card_multiverse_id('sawback manticore'/'MIR', '3552').

card_in_set('sea scryer', 'MIR').
card_original_type('sea scryer'/'MIR', 'Summon — Merfolk').
card_original_text('sea scryer'/'MIR', '{T}: Add one colorless mana to your mana pool. Play this ability as a mana source. {1}, {T}: Add {U} to your mana pool. Play this ability as a mana source.').
card_first_print('sea scryer', 'MIR').
card_image_name('sea scryer'/'MIR', 'sea scryer').
card_uid('sea scryer'/'MIR', 'MIR:Sea Scryer:sea scryer').
card_rarity('sea scryer'/'MIR', 'Common').
card_artist('sea scryer'/'MIR', 'Martin McKenna').
card_multiverse_id('sea scryer'/'MIR', '3361').

card_in_set('sealed fate', 'MIR').
card_original_type('sealed fate'/'MIR', 'Sorcery').
card_original_text('sealed fate'/'MIR', 'Look at the top X cards of target opponent\'s library. Remove one of those cards from the game and put the rest back on top of that player\'s library in any order.').
card_first_print('sealed fate', 'MIR').
card_image_name('sealed fate'/'MIR', 'sealed fate').
card_uid('sealed fate'/'MIR', 'MIR:Sealed Fate:sealed fate').
card_rarity('sealed fate'/'MIR', 'Uncommon').
card_artist('sealed fate'/'MIR', 'Terese Nielsen').
card_flavor_text('sealed fate'/'MIR', 'It\'s good to know more about your enemy\'s fate than your enemy does.').
card_multiverse_id('sealed fate'/'MIR', '3553').

card_in_set('searing spear askari', 'MIR').
card_original_type('searing spear askari'/'MIR', 'Summon — Knight').
card_original_text('searing spear askari'/'MIR', 'Flanking (Whenever a creature without flanking is assigned to block this creature, the blocking creature gets -1/-1 until end of turn.)\n{1}{R} Searing Spear Askari cannot be blocked by only one creature this turn.').
card_first_print('searing spear askari', 'MIR').
card_image_name('searing spear askari'/'MIR', 'searing spear askari').
card_uid('searing spear askari'/'MIR', 'MIR:Searing Spear Askari:searing spear askari').
card_rarity('searing spear askari'/'MIR', 'Common').
card_artist('searing spear askari'/'MIR', 'Richard Kane Ferguson').
card_flavor_text('searing spear askari'/'MIR', 'When the other soldiers of Zhalfir fall, the askaris will yet stand.').
card_multiverse_id('searing spear askari'/'MIR', '3462').

card_in_set('seedling charm', 'MIR').
card_original_type('seedling charm'/'MIR', 'Instant').
card_original_text('seedling charm'/'MIR', 'Choose one Return target creature enchantment to owner\'s hand; or regenerate target green creature; or target creature gains trample until end of turn.').
card_first_print('seedling charm', 'MIR').
card_image_name('seedling charm'/'MIR', 'seedling charm').
card_uid('seedling charm'/'MIR', 'MIR:Seedling Charm:seedling charm').
card_rarity('seedling charm'/'MIR', 'Common').
card_artist('seedling charm'/'MIR', 'Stuart Griffin').
card_multiverse_id('seedling charm'/'MIR', '3409').

card_in_set('seeds of innocence', 'MIR').
card_original_type('seeds of innocence'/'MIR', 'Sorcery').
card_original_text('seeds of innocence'/'MIR', 'Bury all artifacts. Each artifact\'s controller gains an amount of life equal to that artifact\'s casting cost.').
card_first_print('seeds of innocence', 'MIR').
card_image_name('seeds of innocence'/'MIR', 'seeds of innocence').
card_uid('seeds of innocence'/'MIR', 'MIR:Seeds of Innocence:seeds of innocence').
card_rarity('seeds of innocence'/'MIR', 'Rare').
card_artist('seeds of innocence'/'MIR', 'Junior Tomlin').
card_flavor_text('seeds of innocence'/'MIR', '\"I have hidden from the machinations of Zhalfir for centuries. Why should I join your campaign?\"\n—Jolrael').
card_multiverse_id('seeds of innocence'/'MIR', '3410').

card_in_set('serene heart', 'MIR').
card_original_type('serene heart'/'MIR', 'Instant').
card_original_text('serene heart'/'MIR', 'Destroy all local enchantments.').
card_first_print('serene heart', 'MIR').
card_image_name('serene heart'/'MIR', 'serene heart').
card_uid('serene heart'/'MIR', 'MIR:Serene Heart:serene heart').
card_rarity('serene heart'/'MIR', 'Common').
card_artist('serene heart'/'MIR', 'D. Alexander Gregory').
card_flavor_text('serene heart'/'MIR', '\"If magic is your crutch, cast it aside and learn to walk without it.\"\n—Teferi').
card_multiverse_id('serene heart'/'MIR', '3411').

card_in_set('sewer rats', 'MIR').
card_original_type('sewer rats'/'MIR', 'Summon — Rats').
card_original_text('sewer rats'/'MIR', '{B}, Pay 1 life: +1/+0 until end of turn. You cannot spend more than {B}{B}{B} in this way each turn.').
card_first_print('sewer rats', 'MIR').
card_image_name('sewer rats'/'MIR', 'sewer rats').
card_uid('sewer rats'/'MIR', 'MIR:Sewer Rats:sewer rats').
card_rarity('sewer rats'/'MIR', 'Common').
card_artist('sewer rats'/'MIR', 'Martin McKenna').
card_flavor_text('sewer rats'/'MIR', 'You lie down with rats, and the rats run away.\n—Suq\'Ata insult').
card_multiverse_id('sewer rats'/'MIR', '3308').

card_in_set('shadow guildmage', 'MIR').
card_original_type('shadow guildmage'/'MIR', 'Summon — Wizard').
card_original_text('shadow guildmage'/'MIR', '{U}, {T}: Put target creature you control on top of owner\'s library.\n{R}, {T}: Shadow Guildmage deals 1 damage to target creature or player and 1 damage to you.').
card_first_print('shadow guildmage', 'MIR').
card_image_name('shadow guildmage'/'MIR', 'shadow guildmage').
card_uid('shadow guildmage'/'MIR', 'MIR:Shadow Guildmage:shadow guildmage').
card_rarity('shadow guildmage'/'MIR', 'Common').
card_artist('shadow guildmage'/'MIR', 'Mike Kimble').
card_flavor_text('shadow guildmage'/'MIR', 'To keep the dead so others may live.\n—Shadow Guild maxim').
card_multiverse_id('shadow guildmage'/'MIR', '3309').

card_in_set('shadowbane', 'MIR').
card_original_type('shadowbane'/'MIR', 'Instant').
card_original_text('shadowbane'/'MIR', 'Prevent all damage to you or a creature you control from any one source. If that source is black, gain 1 life for each 1 damage prevented in this way.').
card_first_print('shadowbane', 'MIR').
card_image_name('shadowbane'/'MIR', 'shadowbane').
card_uid('shadowbane'/'MIR', 'MIR:Shadowbane:shadowbane').
card_rarity('shadowbane'/'MIR', 'Uncommon').
card_artist('shadowbane'/'MIR', 'Douglas Shuler').
card_flavor_text('shadowbane'/'MIR', 'Light creates shadow; light destroys shadow. Such is the transience of darkness.').
card_multiverse_id('shadowbane'/'MIR', '3513').

card_in_set('shallow grave', 'MIR').
card_original_type('shallow grave'/'MIR', 'Instant').
card_original_text('shallow grave'/'MIR', 'Put the top creature card from your graveyard into play as though it were just played. That creature is unaffected by summoning sickness. Remove the creature from the game at end of any turn.').
card_first_print('shallow grave', 'MIR').
card_image_name('shallow grave'/'MIR', 'shallow grave').
card_uid('shallow grave'/'MIR', 'MIR:Shallow Grave:shallow grave').
card_rarity('shallow grave'/'MIR', 'Rare').
card_artist('shallow grave'/'MIR', 'John Coulthart').
card_flavor_text('shallow grave'/'MIR', 'Good help is hard to dig up.').
card_multiverse_id('shallow grave'/'MIR', '3310').

card_in_set('shaper guildmage', 'MIR').
card_original_type('shaper guildmage'/'MIR', 'Summon — Wizard').
card_original_text('shaper guildmage'/'MIR', '{W}, {T}: Target creature gains first strike until end of turn.\n{B}, {T}: Target creature gets +1/+0 until end of turn.').
card_first_print('shaper guildmage', 'MIR').
card_image_name('shaper guildmage'/'MIR', 'shaper guildmage').
card_uid('shaper guildmage'/'MIR', 'MIR:Shaper Guildmage:shaper guildmage').
card_rarity('shaper guildmage'/'MIR', 'Common').
card_artist('shaper guildmage'/'MIR', 'D. Alexander Gregory').
card_flavor_text('shaper guildmage'/'MIR', 'Shape this world in Zhalfir\'s image.\n—Shaper Guild maxim').
card_multiverse_id('shaper guildmage'/'MIR', '3362').

card_in_set('shauku\'s minion', 'MIR').
card_original_type('shauku\'s minion'/'MIR', 'Summon — Minion').
card_original_text('shauku\'s minion'/'MIR', '{B}{R}, {T}: Shauku\'s Minion deals 2 damage to target white creature.').
card_first_print('shauku\'s minion', 'MIR').
card_image_name('shauku\'s minion'/'MIR', 'shauku\'s minion').
card_uid('shauku\'s minion'/'MIR', 'MIR:Shauku\'s Minion:shauku\'s minion').
card_rarity('shauku\'s minion'/'MIR', 'Uncommon').
card_artist('shauku\'s minion'/'MIR', 'Greg Simanson').
card_flavor_text('shauku\'s minion'/'MIR', '\"My minions serve me in the light; I serve no one.\"\n—Shauku, Endbringer').
card_multiverse_id('shauku\'s minion'/'MIR', '3554').

card_in_set('shauku, endbringer', 'MIR').
card_original_type('shauku, endbringer'/'MIR', 'Summon — Legend').
card_original_text('shauku, endbringer'/'MIR', 'Flying\nShauku, Endbringer cannot attack if there is another creature in play.\nDuring your upkeep, lose 3 life.\n{T}: Remove target creature from the game and put a +1/+1 counter on Shauku.').
card_first_print('shauku, endbringer', 'MIR').
card_image_name('shauku, endbringer'/'MIR', 'shauku, endbringer').
card_uid('shauku, endbringer'/'MIR', 'MIR:Shauku, Endbringer:shauku, endbringer').
card_rarity('shauku, endbringer'/'MIR', 'Rare').
card_artist('shauku, endbringer'/'MIR', 'Pete Venters').
card_multiverse_id('shauku, endbringer'/'MIR', '3311').

card_in_set('shimmer', 'MIR').
card_original_type('shimmer'/'MIR', 'Enchantment').
card_original_text('shimmer'/'MIR', 'When you play Shimmer, choose a land type.\nAll lands of the chosen type gain phasing.').
card_first_print('shimmer', 'MIR').
card_image_name('shimmer'/'MIR', 'shimmer').
card_uid('shimmer'/'MIR', 'MIR:Shimmer:shimmer').
card_rarity('shimmer'/'MIR', 'Rare').
card_artist('shimmer'/'MIR', 'David A. Cherry').
card_flavor_text('shimmer'/'MIR', 'To buy unseen land, pay in unseen coin.\n—Suq\'Ata maxim').
card_multiverse_id('shimmer'/'MIR', '3363').

card_in_set('sidar jabari', 'MIR').
card_original_type('sidar jabari'/'MIR', 'Summon — Legend').
card_original_text('sidar jabari'/'MIR', 'Flanking (Whenever a creature without flanking is assigned to block this creature, the blocking creature gets -1/-1 until end of turn.)\nIf Sidar Jabari attacks, tap target creature defending player controls.').
card_first_print('sidar jabari', 'MIR').
card_image_name('sidar jabari'/'MIR', 'sidar jabari').
card_uid('sidar jabari'/'MIR', 'MIR:Sidar Jabari:sidar jabari').
card_rarity('sidar jabari'/'MIR', 'Rare').
card_artist('sidar jabari'/'MIR', 'Gerry Grace').
card_flavor_text('sidar jabari'/'MIR', '\"Prophecy is whatever one makes of it, but destiny cannot be changed.\"\n—Sidar Jabari').
card_multiverse_id('sidar jabari'/'MIR', '3514').

card_in_set('sirocco', 'MIR').
card_original_type('sirocco'/'MIR', 'Instant').
card_original_text('sirocco'/'MIR', 'Target player reveals his or her hand to all players. For each blue interrupt card that player holds, he or she pays 4 life or discards that card.').
card_first_print('sirocco', 'MIR').
card_image_name('sirocco'/'MIR', 'sirocco').
card_uid('sirocco'/'MIR', 'MIR:Sirocco:sirocco').
card_rarity('sirocco'/'MIR', 'Uncommon').
card_artist('sirocco'/'MIR', 'Alan Rabinowitz').
card_flavor_text('sirocco'/'MIR', '\"Blow, fiery wind, / strip illusion from the eye. / Blow, fiery wind of truth.\"\n—Femeref chant').
card_multiverse_id('sirocco'/'MIR', '3463').

card_in_set('skulking ghost', 'MIR').
card_original_type('skulking ghost'/'MIR', 'Summon — Ghost').
card_original_text('skulking ghost'/'MIR', 'Flying\nIf Skulking Ghost is the target of a spell or effect, bury Skulking Ghost.').
card_first_print('skulking ghost', 'MIR').
card_image_name('skulking ghost'/'MIR', 'skulking ghost').
card_uid('skulking ghost'/'MIR', 'MIR:Skulking Ghost:skulking ghost').
card_rarity('skulking ghost'/'MIR', 'Common').
card_artist('skulking ghost'/'MIR', 'Robert Bliss').
card_flavor_text('skulking ghost'/'MIR', '\"They exist only so long as the living take no notice.\"\n—Kifimbo, Shadow Guildmage').
card_multiverse_id('skulking ghost'/'MIR', '3312').

card_in_set('sky diamond', 'MIR').
card_original_type('sky diamond'/'MIR', 'Artifact').
card_original_text('sky diamond'/'MIR', 'Sky Diamond comes into play tapped.\n{T}: Add {U} to your mana pool. Play this ability as a mana source.').
card_first_print('sky diamond', 'MIR').
card_image_name('sky diamond'/'MIR', 'sky diamond').
card_uid('sky diamond'/'MIR', 'MIR:Sky Diamond:sky diamond').
card_rarity('sky diamond'/'MIR', 'Uncommon').
card_artist('sky diamond'/'MIR', 'D. Alexander Gregory').
card_multiverse_id('sky diamond'/'MIR', '3267').

card_in_set('soar', 'MIR').
card_original_type('soar'/'MIR', 'Enchant Creature').
card_original_text('soar'/'MIR', 'You may choose to play Soar as an instant; if you do, bury it at end of turn.\nEnchanted creature gets +0/+1 and gains flying.').
card_first_print('soar', 'MIR').
card_image_name('soar'/'MIR', 'soar').
card_uid('soar'/'MIR', 'MIR:Soar:soar').
card_rarity('soar'/'MIR', 'Common').
card_artist('soar'/'MIR', 'Tony Roberts').
card_flavor_text('soar'/'MIR', '\"Would you give up your hands to fly? That is what the birds have done.\"\n—The One Thousand Questions').
card_multiverse_id('soar'/'MIR', '3364').

card_in_set('soul echo', 'MIR').
card_original_type('soul echo'/'MIR', 'Enchantment').
card_original_text('soul echo'/'MIR', 'When you play Soul Echo, put X echo counters on it. \nAt the beginning of your upkeep, if there are no echo counters on Soul Echo, bury it; otherwise, target opponent may choose that for each 1 damage dealt to you until your next upkeep, you instead remove 1 echo counter from Soul Echo.\nYou do not lose the game as a result of having less than 1 life.').
card_first_print('soul echo', 'MIR').
card_image_name('soul echo'/'MIR', 'soul echo').
card_uid('soul echo'/'MIR', 'MIR:Soul Echo:soul echo').
card_rarity('soul echo'/'MIR', 'Rare').
card_artist('soul echo'/'MIR', 'Ron Spencer').
card_multiverse_id('soul echo'/'MIR', '3515').

card_in_set('soul rend', 'MIR').
card_original_type('soul rend'/'MIR', 'Instant').
card_original_text('soul rend'/'MIR', 'Bury target creature if it is white.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_first_print('soul rend', 'MIR').
card_image_name('soul rend'/'MIR', 'soul rend').
card_uid('soul rend'/'MIR', 'MIR:Soul Rend:soul rend').
card_rarity('soul rend'/'MIR', 'Uncommon').
card_artist('soul rend'/'MIR', 'Jeff Miracola').
card_flavor_text('soul rend'/'MIR', '\"Since I cannot stop death, I choose to stop life.\"\n—Kaervek').
card_multiverse_id('soul rend'/'MIR', '3313').

card_in_set('soulshriek', 'MIR').
card_original_type('soulshriek'/'MIR', 'Instant').
card_original_text('soulshriek'/'MIR', 'Target creature you control gets +*/+0 until end of turn, where * is equal to the number of creature cards in your graveyard. Bury that creature at end of turn.').
card_first_print('soulshriek', 'MIR').
card_image_name('soulshriek'/'MIR', 'soulshriek').
card_uid('soulshriek'/'MIR', 'MIR:Soulshriek:soulshriek').
card_rarity('soulshriek'/'MIR', 'Common').
card_artist('soulshriek'/'MIR', 'John Bolton').
card_flavor_text('soulshriek'/'MIR', '\"The flesh is a casket for the soul.\"\n—Shauku, Endbringer').
card_multiverse_id('soulshriek'/'MIR', '3314').

card_in_set('spatial binding', 'MIR').
card_original_type('spatial binding'/'MIR', 'Enchantment').
card_original_text('spatial binding'/'MIR', 'Pay 1 life: Target permanent cannot phase out until the beginning of your next upkeep.').
card_first_print('spatial binding', 'MIR').
card_image_name('spatial binding'/'MIR', 'spatial binding').
card_uid('spatial binding'/'MIR', 'MIR:Spatial Binding:spatial binding').
card_rarity('spatial binding'/'MIR', 'Uncommon').
card_artist('spatial binding'/'MIR', 'Dom!').
card_flavor_text('spatial binding'/'MIR', 'Time is the one shackle you can\'t break.\n—Zhalfirin aphorism').
card_multiverse_id('spatial binding'/'MIR', '3555').

card_in_set('spectral guardian', 'MIR').
card_original_type('spectral guardian'/'MIR', 'Summon — Guardian').
card_original_text('spectral guardian'/'MIR', 'As long as Spectral Guardian is untapped, noncreature artifacts cannot be the target of spells or effects.').
card_first_print('spectral guardian', 'MIR').
card_image_name('spectral guardian'/'MIR', 'spectral guardian').
card_uid('spectral guardian'/'MIR', 'MIR:Spectral Guardian:spectral guardian').
card_rarity('spectral guardian'/'MIR', 'Rare').
card_artist('spectral guardian'/'MIR', 'Mike Dringenberg').
card_flavor_text('spectral guardian'/'MIR', '\"A treasure to guard other treasures is worth the highest price.\"\n—Mtai, Civic Guildmage').
card_multiverse_id('spectral guardian'/'MIR', '3516').

card_in_set('spirit of the night', 'MIR').
card_original_type('spirit of the night'/'MIR', 'Summon — Legend').
card_original_text('spirit of the night'/'MIR', 'Flying, trample, protection from black\nFirst strike when attacking\nSpirit of the Night is unaffected by summoning sickness.').
card_first_print('spirit of the night', 'MIR').
card_image_name('spirit of the night'/'MIR', 'spirit of the night').
card_uid('spirit of the night'/'MIR', 'MIR:Spirit of the Night:spirit of the night').
card_rarity('spirit of the night'/'MIR', 'Rare').
card_artist('spirit of the night'/'MIR', 'Cliff Nielsen').
card_multiverse_id('spirit of the night'/'MIR', '3315').

card_in_set('spitting earth', 'MIR').
card_original_type('spitting earth'/'MIR', 'Sorcery').
card_original_text('spitting earth'/'MIR', 'Spitting Earth deals to target creature an amount of damage equal to the number of mountains you control.').
card_first_print('spitting earth', 'MIR').
card_image_name('spitting earth'/'MIR', 'spitting earth').
card_uid('spitting earth'/'MIR', 'MIR:Spitting Earth:spitting earth').
card_rarity('spitting earth'/'MIR', 'Common').
card_artist('spitting earth'/'MIR', 'Brian Snõddy').
card_flavor_text('spitting earth'/'MIR', '\"There are times solid ground gives precious little comfort.\"\n—Talibah, embermage').
card_multiverse_id('spitting earth'/'MIR', '3464').

card_in_set('stalking tiger', 'MIR').
card_original_type('stalking tiger'/'MIR', 'Summon — Tiger').
card_original_text('stalking tiger'/'MIR', 'Stalking Tiger cannot be blocked by more than one creature.').
card_first_print('stalking tiger', 'MIR').
card_image_name('stalking tiger'/'MIR', 'stalking tiger').
card_uid('stalking tiger'/'MIR', 'MIR:Stalking Tiger:stalking tiger').
card_rarity('stalking tiger'/'MIR', 'Common').
card_artist('stalking tiger'/'MIR', 'Terese Nielsen').
card_flavor_text('stalking tiger'/'MIR', 'In the Jamuraan jungles, there is often no separating beauty from danger.').
card_multiverse_id('stalking tiger'/'MIR', '3412').

card_in_set('stone rain', 'MIR').
card_original_type('stone rain'/'MIR', 'Sorcery').
card_original_text('stone rain'/'MIR', 'Destroy target land.').
card_image_name('stone rain'/'MIR', 'stone rain').
card_uid('stone rain'/'MIR', 'MIR:Stone Rain:stone rain').
card_rarity('stone rain'/'MIR', 'Common').
card_artist('stone rain'/'MIR', 'Tony Roberts').
card_flavor_text('stone rain'/'MIR', '\"What rainbow could possibly come of this rain?\"\n—Asmira, Holy Avenger').
card_multiverse_id('stone rain'/'MIR', '3465').

card_in_set('stupor', 'MIR').
card_original_type('stupor'/'MIR', 'Sorcery').
card_original_text('stupor'/'MIR', 'Target opponent discards a card at random, then chooses and discards a card.').
card_image_name('stupor'/'MIR', 'stupor').
card_uid('stupor'/'MIR', 'MIR:Stupor:stupor').
card_rarity('stupor'/'MIR', 'Uncommon').
card_artist('stupor'/'MIR', 'Mike Kimble').
card_flavor_text('stupor'/'MIR', 'There are medicines for all afflictions but idleness.\n—Suq\'Ata saying').
card_multiverse_id('stupor'/'MIR', '3316').

card_in_set('subterranean spirit', 'MIR').
card_original_type('subterranean spirit'/'MIR', 'Summon — Elemental').
card_original_text('subterranean spirit'/'MIR', 'Protection from red\n{T}: Subterranean Spirit deals 1 damage to each creature without flying.').
card_first_print('subterranean spirit', 'MIR').
card_image_name('subterranean spirit'/'MIR', 'subterranean spirit').
card_uid('subterranean spirit'/'MIR', 'MIR:Subterranean Spirit:subterranean spirit').
card_rarity('subterranean spirit'/'MIR', 'Rare').
card_artist('subterranean spirit'/'MIR', 'John Bolton').
card_flavor_text('subterranean spirit'/'MIR', '\"Air hot with sulfur. / Earth heaves beneath me. / Could it be the mountain / ate something bad?\"\n—Dwarven mining song').
card_multiverse_id('subterranean spirit'/'MIR', '3466').

card_in_set('sunweb', 'MIR').
card_original_type('sunweb'/'MIR', 'Summon — Wall').
card_original_text('sunweb'/'MIR', 'Flying\nSunweb cannot block creatures with power 2 or less.').
card_first_print('sunweb', 'MIR').
card_image_name('sunweb'/'MIR', 'sunweb').
card_uid('sunweb'/'MIR', 'MIR:Sunweb:sunweb').
card_rarity('sunweb'/'MIR', 'Rare').
card_artist('sunweb'/'MIR', 'Dan Frazier').
card_flavor_text('sunweb'/'MIR', '\"There is no sweeter music than the wails of a dying dragon.\"\n—Rashida Scalebane').
card_multiverse_id('sunweb'/'MIR', '3517').

card_in_set('superior numbers', 'MIR').
card_original_type('superior numbers'/'MIR', 'Sorcery').
card_original_text('superior numbers'/'MIR', 'Superior Numbers deals to target creature 1 damage for each creature you control in excess of the number of creatures target opponent controls.').
card_first_print('superior numbers', 'MIR').
card_image_name('superior numbers'/'MIR', 'superior numbers').
card_uid('superior numbers'/'MIR', 'MIR:Superior Numbers:superior numbers').
card_rarity('superior numbers'/'MIR', 'Uncommon').
card_artist('superior numbers'/'MIR', 'Geofrey Darrow').
card_multiverse_id('superior numbers'/'MIR', '3413').

card_in_set('suq\'ata firewalker', 'MIR').
card_original_type('suq\'ata firewalker'/'MIR', 'Summon — Wizard').
card_original_text('suq\'ata firewalker'/'MIR', 'Suq\'Ata Firewalker cannot be the target of red spells or effects.\n{T}: Suq\'Ata Firewalker deals 1 damage to target creature or player.').
card_first_print('suq\'ata firewalker', 'MIR').
card_image_name('suq\'ata firewalker'/'MIR', 'suq\'ata firewalker').
card_uid('suq\'ata firewalker'/'MIR', 'MIR:Suq\'Ata Firewalker:suq\'ata firewalker').
card_rarity('suq\'ata firewalker'/'MIR', 'Uncommon').
card_artist('suq\'ata firewalker'/'MIR', 'David O\'Connor').
card_flavor_text('suq\'ata firewalker'/'MIR', '\"Comfortable even in a furnace? I\'ve never met one who\'d accept my wager.\"\n—Talibah, embermage').
card_multiverse_id('suq\'ata firewalker'/'MIR', '3365').

card_in_set('swamp', 'MIR').
card_original_type('swamp'/'MIR', 'Land').
card_original_text('swamp'/'MIR', '{T}: Add {B} to your mana pool.').
card_image_name('swamp'/'MIR', 'swamp1').
card_uid('swamp'/'MIR', 'MIR:Swamp:swamp1').
card_rarity('swamp'/'MIR', 'Basic Land').
card_artist('swamp'/'MIR', 'Bob Eggleton').
card_multiverse_id('swamp'/'MIR', '3562').

card_in_set('swamp', 'MIR').
card_original_type('swamp'/'MIR', 'Land').
card_original_text('swamp'/'MIR', '{T}: Add {B} to your mana pool.').
card_image_name('swamp'/'MIR', 'swamp2').
card_uid('swamp'/'MIR', 'MIR:Swamp:swamp2').
card_rarity('swamp'/'MIR', 'Basic Land').
card_artist('swamp'/'MIR', 'Bob Eggleton').
card_multiverse_id('swamp'/'MIR', '3563').

card_in_set('swamp', 'MIR').
card_original_type('swamp'/'MIR', 'Land').
card_original_text('swamp'/'MIR', '{T}: Add {B} to your mana pool.').
card_image_name('swamp'/'MIR', 'swamp3').
card_uid('swamp'/'MIR', 'MIR:Swamp:swamp3').
card_rarity('swamp'/'MIR', 'Basic Land').
card_artist('swamp'/'MIR', 'Bob Eggleton').
card_multiverse_id('swamp'/'MIR', '3564').

card_in_set('swamp', 'MIR').
card_original_type('swamp'/'MIR', 'Land').
card_original_text('swamp'/'MIR', '{T}: Add {B} to your mana pool.').
card_image_name('swamp'/'MIR', 'swamp4').
card_uid('swamp'/'MIR', 'MIR:Swamp:swamp4').
card_rarity('swamp'/'MIR', 'Basic Land').
card_artist('swamp'/'MIR', 'Bob Eggleton').
card_multiverse_id('swamp'/'MIR', '3565').

card_in_set('tainted specter', 'MIR').
card_original_type('tainted specter'/'MIR', 'Summon — Specter').
card_original_text('tainted specter'/'MIR', 'Flying\n{1}{B}{B}, {T}: Target player chooses a card from his or her hand and then chooses either to discard that card or put it on top of his or her library. If the card is discarded, Tainted Specter deals 1 damage to each creature and player. Play this ability as a sorcery.').
card_first_print('tainted specter', 'MIR').
card_image_name('tainted specter'/'MIR', 'tainted specter').
card_uid('tainted specter'/'MIR', 'MIR:Tainted Specter:tainted specter').
card_rarity('tainted specter'/'MIR', 'Rare').
card_artist('tainted specter'/'MIR', 'Chippy').
card_multiverse_id('tainted specter'/'MIR', '3317').

card_in_set('talruum minotaur', 'MIR').
card_original_type('talruum minotaur'/'MIR', 'Summon — Minotaur').
card_original_text('talruum minotaur'/'MIR', 'Talruum Minotaur is unaffected by summoning sickness.').
card_first_print('talruum minotaur', 'MIR').
card_image_name('talruum minotaur'/'MIR', 'talruum minotaur').
card_uid('talruum minotaur'/'MIR', 'MIR:Talruum Minotaur:talruum minotaur').
card_rarity('talruum minotaur'/'MIR', 'Common').
card_artist('talruum minotaur'/'MIR', 'Pete Venters').
card_flavor_text('talruum minotaur'/'MIR', 'Don\'t insult a Talruum unless your mount is swift.\n—Suq\'Ata saying').
card_multiverse_id('talruum minotaur'/'MIR', '3467').

card_in_set('taniwha', 'MIR').
card_original_type('taniwha'/'MIR', 'Summon — Legend').
card_original_text('taniwha'/'MIR', 'Phasing, trample\nAt the beginning of your upkeep, all lands you control phase out.').
card_first_print('taniwha', 'MIR').
card_image_name('taniwha'/'MIR', 'taniwha').
card_uid('taniwha'/'MIR', 'MIR:Taniwha:taniwha').
card_rarity('taniwha'/'MIR', 'Rare').
card_artist('taniwha'/'MIR', 'Ian Miller').
card_flavor_text('taniwha'/'MIR', '\"Taniwha rolls in its sleep, and the land is awash with the waves.\"\n—Poetics of Hanan').
card_multiverse_id('taniwha'/'MIR', '3366').

card_in_set('teeka\'s dragon', 'MIR').
card_original_type('teeka\'s dragon'/'MIR', 'Artifact Creature').
card_original_text('teeka\'s dragon'/'MIR', 'Flying, trample; rampage 4 (For each creature assigned to block it beyond the first, this creature gets +4/+4 until end of turn.)\nTeeka\'s Dragon counts as a Dragon.').
card_first_print('teeka\'s dragon', 'MIR').
card_image_name('teeka\'s dragon'/'MIR', 'teeka\'s dragon').
card_uid('teeka\'s dragon'/'MIR', 'MIR:Teeka\'s Dragon:teeka\'s dragon').
card_rarity('teeka\'s dragon'/'MIR', 'Rare').
card_artist('teeka\'s dragon'/'MIR', 'Liz Danforth').
card_multiverse_id('teeka\'s dragon'/'MIR', '3268').

card_in_set('teferi\'s curse', 'MIR').
card_original_type('teferi\'s curse'/'MIR', 'Enchant Permanent').
card_original_text('teferi\'s curse'/'MIR', 'Play only on an artifact or creature.\nEnchanted permanent gains phasing.').
card_first_print('teferi\'s curse', 'MIR').
card_image_name('teferi\'s curse'/'MIR', 'teferi\'s curse').
card_uid('teferi\'s curse'/'MIR', 'MIR:Teferi\'s Curse:teferi\'s curse').
card_rarity('teferi\'s curse'/'MIR', 'Common').
card_artist('teferi\'s curse'/'MIR', 'Robert Bliss').
card_flavor_text('teferi\'s curse'/'MIR', '\"Stepping through time is like riding a camel. It may get you where you want to go, but the ride is rarely pleasant.\"\n—Teferi').
card_multiverse_id('teferi\'s curse'/'MIR', '3367').

card_in_set('teferi\'s drake', 'MIR').
card_original_type('teferi\'s drake'/'MIR', 'Summon — Drake').
card_original_text('teferi\'s drake'/'MIR', 'Flying, phasing').
card_first_print('teferi\'s drake', 'MIR').
card_image_name('teferi\'s drake'/'MIR', 'teferi\'s drake').
card_uid('teferi\'s drake'/'MIR', 'MIR:Teferi\'s Drake:teferi\'s drake').
card_rarity('teferi\'s drake'/'MIR', 'Common').
card_artist('teferi\'s drake'/'MIR', 'Kari Johnson').
card_flavor_text('teferi\'s drake'/'MIR', '\"We threw dried fruit to the drakes that rode the air, streaming along the Weatherlight\'s side as we sailed amidst the clouds.\"\n—Sisay, Captain of the Weatherlight').
card_multiverse_id('teferi\'s drake'/'MIR', '3368').

card_in_set('teferi\'s imp', 'MIR').
card_original_type('teferi\'s imp'/'MIR', 'Summon — Imp').
card_original_text('teferi\'s imp'/'MIR', 'Flying, phasing\nWhen Teferi\'s Imp phases out, choose and discard a card.\nWhen Teferi\'s Imp phases in, draw a card.').
card_first_print('teferi\'s imp', 'MIR').
card_image_name('teferi\'s imp'/'MIR', 'teferi\'s imp').
card_uid('teferi\'s imp'/'MIR', 'MIR:Teferi\'s Imp:teferi\'s imp').
card_rarity('teferi\'s imp'/'MIR', 'Rare').
card_artist('teferi\'s imp'/'MIR', 'Una Fricker').
card_flavor_text('teferi\'s imp'/'MIR', 'Made you look.').
card_multiverse_id('teferi\'s imp'/'MIR', '3369').

card_in_set('teferi\'s isle', 'MIR').
card_original_type('teferi\'s isle'/'MIR', 'Legendary Land').
card_original_text('teferi\'s isle'/'MIR', 'Phasing\nTeferi\'s Isle comes into play tapped.\n{T}: Add {U}{U} to your mana pool.').
card_first_print('teferi\'s isle', 'MIR').
card_image_name('teferi\'s isle'/'MIR', 'teferi\'s isle').
card_uid('teferi\'s isle'/'MIR', 'MIR:Teferi\'s Isle:teferi\'s isle').
card_rarity('teferi\'s isle'/'MIR', 'Rare').
card_artist('teferi\'s isle'/'MIR', 'Gerry Grace').
card_flavor_text('teferi\'s isle'/'MIR', 'The three wizards knew only that this isle held a great mystery—and that patience alone would solve it.').
card_multiverse_id('teferi\'s isle'/'MIR', '3576').

card_in_set('telim\'tor', 'MIR').
card_original_type('telim\'tor'/'MIR', 'Summon — Legend').
card_original_text('telim\'tor'/'MIR', 'Flanking (Whenever a creature without flanking is assigned to block this creature, the blocking creature gets -1/-1 until end of turn.)\nIf Telim\'Tor attacks, all attacking creatures with flanking get +1/+1 until end of turn.').
card_first_print('telim\'tor', 'MIR').
card_image_name('telim\'tor'/'MIR', 'telim\'tor').
card_uid('telim\'tor'/'MIR', 'MIR:Telim\'Tor:telim\'tor').
card_rarity('telim\'tor'/'MIR', 'Rare').
card_artist('telim\'tor'/'MIR', 'Kev Walker').
card_flavor_text('telim\'tor'/'MIR', '\"The curious merely amass knowledge. The ambitious use it.\"\n—Telim\'Tor').
card_multiverse_id('telim\'tor'/'MIR', '3468').

card_in_set('telim\'tor\'s darts', 'MIR').
card_original_type('telim\'tor\'s darts'/'MIR', 'Artifact').
card_original_text('telim\'tor\'s darts'/'MIR', '{2}, {T}: Telim\'Tor\'s Darts deals 1 damage to target player.').
card_first_print('telim\'tor\'s darts', 'MIR').
card_image_name('telim\'tor\'s darts'/'MIR', 'telim\'tor\'s darts').
card_uid('telim\'tor\'s darts'/'MIR', 'MIR:Telim\'Tor\'s Darts:telim\'tor\'s darts').
card_rarity('telim\'tor\'s darts'/'MIR', 'Uncommon').
card_artist('telim\'tor\'s darts'/'MIR', 'Kev Walker').
card_flavor_text('telim\'tor\'s darts'/'MIR', '\"People laughed at my darts—once.\"\n—Telim\'Tor').
card_multiverse_id('telim\'tor\'s darts'/'MIR', '3269').

card_in_set('telim\'tor\'s edict', 'MIR').
card_original_type('telim\'tor\'s edict'/'MIR', 'Instant').
card_original_text('telim\'tor\'s edict'/'MIR', 'Remove from the game target permanent you own or control.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_first_print('telim\'tor\'s edict', 'MIR').
card_image_name('telim\'tor\'s edict'/'MIR', 'telim\'tor\'s edict').
card_uid('telim\'tor\'s edict'/'MIR', 'MIR:Telim\'Tor\'s Edict:telim\'tor\'s edict').
card_rarity('telim\'tor\'s edict'/'MIR', 'Rare').
card_artist('telim\'tor\'s edict'/'MIR', 'Kev Walker').
card_flavor_text('telim\'tor\'s edict'/'MIR', '\"Execution awaits the dissenter.\"\n—Kasib ibn Naji, Letters').
card_multiverse_id('telim\'tor\'s edict'/'MIR', '3469').

card_in_set('teremko griffin', 'MIR').
card_original_type('teremko griffin'/'MIR', 'Summon — Griffin').
card_original_text('teremko griffin'/'MIR', 'Banding, flying').
card_first_print('teremko griffin', 'MIR').
card_image_name('teremko griffin'/'MIR', 'teremko griffin').
card_uid('teremko griffin'/'MIR', 'MIR:Teremko Griffin:teremko griffin').
card_rarity('teremko griffin'/'MIR', 'Common').
card_artist('teremko griffin'/'MIR', 'Martin McKenna').
card_flavor_text('teremko griffin'/'MIR', '\"When we\'re aloft, I love to lean over the Weatherlight\'s railing and watch the patterns the griffins\' shadows make on the clouds below.\"\n—Sisay, Captain of the Weatherlight').
card_multiverse_id('teremko griffin'/'MIR', '3518').

card_in_set('thirst', 'MIR').
card_original_type('thirst'/'MIR', 'Enchant Creature').
card_original_text('thirst'/'MIR', 'When Thirst comes into play, tap enchanted creature.\nDuring your upkeep, pay {U} or bury Thirst.\nEnchanted creature does not untap during its controller\'s untap phase.').
card_first_print('thirst', 'MIR').
card_image_name('thirst'/'MIR', 'thirst').
card_uid('thirst'/'MIR', 'MIR:Thirst:thirst').
card_rarity('thirst'/'MIR', 'Common').
card_artist('thirst'/'MIR', 'Roger Raupp').
card_flavor_text('thirst'/'MIR', 'The first of the Thousand Deaths.').
card_multiverse_id('thirst'/'MIR', '3370').

card_in_set('tidal wave', 'MIR').
card_original_type('tidal wave'/'MIR', 'Instant').
card_original_text('tidal wave'/'MIR', 'Put a Wave token into play. Treat this token as a 5/5 blue creature that counts as a Wall. Bury the token at end of any turn.').
card_first_print('tidal wave', 'MIR').
card_image_name('tidal wave'/'MIR', 'tidal wave').
card_uid('tidal wave'/'MIR', 'MIR:Tidal Wave:tidal wave').
card_rarity('tidal wave'/'MIR', 'Uncommon').
card_artist('tidal wave'/'MIR', 'Brian Snõddy').
card_flavor_text('tidal wave'/'MIR', 'When mountains ride the sea, waves live upon the fields.').
card_multiverse_id('tidal wave'/'MIR', '3371').

card_in_set('tombstone stairwell', 'MIR').
card_original_type('tombstone stairwell'/'MIR', 'Enchant World').
card_original_text('tombstone stairwell'/'MIR', 'Cumulative upkeep {1}{B} \nDuring each upkeep, each player puts into play a Tombspawn token for each summon card in his or her graveyard. Treat these tokens as 2/2 black creatures that are unaffected by summoning sickness and count as Zombies. At end of any turn or if Tombstone Stairwell leaves play, bury all of these tokens.').
card_first_print('tombstone stairwell', 'MIR').
card_image_name('tombstone stairwell'/'MIR', 'tombstone stairwell').
card_uid('tombstone stairwell'/'MIR', 'MIR:Tombstone Stairwell:tombstone stairwell').
card_rarity('tombstone stairwell'/'MIR', 'Rare').
card_artist('tombstone stairwell'/'MIR', 'Dom!').
card_multiverse_id('tombstone stairwell'/'MIR', '3318').

card_in_set('torrent of lava', 'MIR').
card_original_type('torrent of lava'/'MIR', 'Sorcery').
card_original_text('torrent of lava'/'MIR', 'Torrent of Lava deals X damage to each creature without flying.\nEach creature gains \"{T}: Prevent 1 damage to this creature from Torrent of Lava.\"').
card_first_print('torrent of lava', 'MIR').
card_image_name('torrent of lava'/'MIR', 'torrent of lava').
card_uid('torrent of lava'/'MIR', 'MIR:Torrent of Lava:torrent of lava').
card_rarity('torrent of lava'/'MIR', 'Rare').
card_artist('torrent of lava'/'MIR', 'Kathryn Rathke').
card_flavor_text('torrent of lava'/'MIR', 'The mountain\'s voice shouts us all down.').
card_multiverse_id('torrent of lava'/'MIR', '3470').

card_in_set('tranquil domain', 'MIR').
card_original_type('tranquil domain'/'MIR', 'Instant').
card_original_text('tranquil domain'/'MIR', 'Destroy all global enchantments.').
card_first_print('tranquil domain', 'MIR').
card_image_name('tranquil domain'/'MIR', 'tranquil domain').
card_uid('tranquil domain'/'MIR', 'MIR:Tranquil Domain:tranquil domain').
card_rarity('tranquil domain'/'MIR', 'Common').
card_artist('tranquil domain'/'MIR', 'D. Alexander Gregory').
card_flavor_text('tranquil domain'/'MIR', '\"An ancient court mage unified the houses into guilds to serve Zhalfir above their own interests. Even during the civil war, the guilds stood together, a testament to the wisdom of Teferi.\"\n—Afari, Tales').
card_multiverse_id('tranquil domain'/'MIR', '3414').

card_in_set('tropical storm', 'MIR').
card_original_type('tropical storm'/'MIR', 'Sorcery').
card_original_text('tropical storm'/'MIR', 'Tropical Storm deals X damage to each creature with flying and 1 damage to each blue creature.').
card_first_print('tropical storm', 'MIR').
card_image_name('tropical storm'/'MIR', 'tropical storm').
card_uid('tropical storm'/'MIR', 'MIR:Tropical Storm:tropical storm').
card_rarity('tropical storm'/'MIR', 'Uncommon').
card_artist('tropical storm'/'MIR', 'Richard Kane Ferguson').
card_flavor_text('tropical storm'/'MIR', '\"After the storm, I found a sea turtle dying on the sands, a blade of grass driven through its shell.\"\n—Travelogue of Najat').
card_multiverse_id('tropical storm'/'MIR', '3415').

card_in_set('uktabi faerie', 'MIR').
card_original_type('uktabi faerie'/'MIR', 'Summon — Faerie').
card_original_text('uktabi faerie'/'MIR', 'Flying\n{3}{G}, Sacrifice Uktabi Faerie: Destroy target artifact.').
card_first_print('uktabi faerie', 'MIR').
card_image_name('uktabi faerie'/'MIR', 'uktabi faerie').
card_uid('uktabi faerie'/'MIR', 'MIR:Uktabi Faerie:uktabi faerie').
card_rarity('uktabi faerie'/'MIR', 'Common').
card_artist('uktabi faerie'/'MIR', 'Junior Tomlin').
card_flavor_text('uktabi faerie'/'MIR', 'Victim of the two-toed faerie\n—Suq\'Ata expression meaning \"broken\"').
card_multiverse_id('uktabi faerie'/'MIR', '3416').

card_in_set('uktabi wildcats', 'MIR').
card_original_type('uktabi wildcats'/'MIR', 'Summon — Wildcats').
card_original_text('uktabi wildcats'/'MIR', 'Uktabi Wildcats has power and toughness each equal to the number of forests you control.\n{G}, Sacrifice a forest: Regenerate').
card_first_print('uktabi wildcats', 'MIR').
card_image_name('uktabi wildcats'/'MIR', 'uktabi wildcats').
card_uid('uktabi wildcats'/'MIR', 'MIR:Uktabi Wildcats:uktabi wildcats').
card_rarity('uktabi wildcats'/'MIR', 'Rare').
card_artist('uktabi wildcats'/'MIR', 'John Matson').
card_multiverse_id('uktabi wildcats'/'MIR', '3417').

card_in_set('unerring sling', 'MIR').
card_original_type('unerring sling'/'MIR', 'Artifact').
card_original_text('unerring sling'/'MIR', '{3}, {T}, Tap an untapped creature you control: Unerring Sling deals an amount of damage equal to that creature\'s power to target attacking or blocking creature with flying.').
card_first_print('unerring sling', 'MIR').
card_image_name('unerring sling'/'MIR', 'unerring sling').
card_uid('unerring sling'/'MIR', 'MIR:Unerring Sling:unerring sling').
card_rarity('unerring sling'/'MIR', 'Uncommon').
card_artist('unerring sling'/'MIR', 'Zak Plucinski').
card_multiverse_id('unerring sling'/'MIR', '3270').

card_in_set('unfulfilled desires', 'MIR').
card_original_type('unfulfilled desires'/'MIR', 'Enchantment').
card_original_text('unfulfilled desires'/'MIR', '{1}, Pay 1 life: Draw a card, then choose and discard a card.').
card_first_print('unfulfilled desires', 'MIR').
card_image_name('unfulfilled desires'/'MIR', 'unfulfilled desires').
card_uid('unfulfilled desires'/'MIR', 'MIR:Unfulfilled Desires:unfulfilled desires').
card_rarity('unfulfilled desires'/'MIR', 'Rare').
card_artist('unfulfilled desires'/'MIR', 'D. Alexander Gregory').
card_flavor_text('unfulfilled desires'/'MIR', '\"Like Day from Night, / I\'ll live my life apart from you, just glimpsing you across the sky, / because you cannot change, my dear, and nor can I.\"\n—\"Love Song of Night and Day\"').
card_multiverse_id('unfulfilled desires'/'MIR', '3556').

card_in_set('unseen walker', 'MIR').
card_original_type('unseen walker'/'MIR', 'Summon — Dryad').
card_original_text('unseen walker'/'MIR', 'Forestwalk (If defending player controls any forests, this creature is unblockable.)\n{1}{G}{G} Target creature gains forestwalk until end of turn.').
card_first_print('unseen walker', 'MIR').
card_image_name('unseen walker'/'MIR', 'unseen walker').
card_uid('unseen walker'/'MIR', 'MIR:Unseen Walker:unseen walker').
card_rarity('unseen walker'/'MIR', 'Uncommon').
card_artist('unseen walker'/'MIR', 'Alan Rabinowitz').
card_flavor_text('unseen walker'/'MIR', '\"To pass through the jungle: silence, courtesy, ferocity, as the occasion demands.\"\n—Kamau, \"Proper Passage\"').
card_multiverse_id('unseen walker'/'MIR', '3418').

card_in_set('unyaro bee sting', 'MIR').
card_original_type('unyaro bee sting'/'MIR', 'Sorcery').
card_original_text('unyaro bee sting'/'MIR', 'Unyaro Bee Sting deals 2 damage to target creature or player.').
card_first_print('unyaro bee sting', 'MIR').
card_image_name('unyaro bee sting'/'MIR', 'unyaro bee sting').
card_uid('unyaro bee sting'/'MIR', 'MIR:Unyaro Bee Sting:unyaro bee sting').
card_rarity('unyaro bee sting'/'MIR', 'Uncommon').
card_artist('unyaro bee sting'/'MIR', 'Pat Morrissey').
card_flavor_text('unyaro bee sting'/'MIR', '\"Much can be learned from the bees about dying for a cause.\"\n—Asmira, Holy Avenger').
card_multiverse_id('unyaro bee sting'/'MIR', '3419').

card_in_set('unyaro griffin', 'MIR').
card_original_type('unyaro griffin'/'MIR', 'Summon — Griffin').
card_original_text('unyaro griffin'/'MIR', 'Flying\nSacrifice Unyaro Griffin: Counter target red spell that assigns damage to you or a creature you control. Play this ability as an interrupt.').
card_first_print('unyaro griffin', 'MIR').
card_image_name('unyaro griffin'/'MIR', 'unyaro griffin').
card_uid('unyaro griffin'/'MIR', 'MIR:Unyaro Griffin:unyaro griffin').
card_rarity('unyaro griffin'/'MIR', 'Uncommon').
card_artist('unyaro griffin'/'MIR', 'Al Davidson').
card_flavor_text('unyaro griffin'/'MIR', 'The griffin\'s shadow gives courage to the righteous and dread to the idle.').
card_multiverse_id('unyaro griffin'/'MIR', '3519').

card_in_set('urborg panther', 'MIR').
card_original_type('urborg panther'/'MIR', 'Summon — Night Stalker').
card_original_text('urborg panther'/'MIR', '{B}, Sacrifice Urborg Panther: Destroy target creature blocking Urborg Panther.\nSacrifice Feral Shadow, Breathstealer, and Urborg Panther: Search your library for Spirit of the Night and put it into play as though it were just played. Shuffle your library afterwards.').
card_first_print('urborg panther', 'MIR').
card_image_name('urborg panther'/'MIR', 'urborg panther').
card_uid('urborg panther'/'MIR', 'MIR:Urborg Panther:urborg panther').
card_rarity('urborg panther'/'MIR', 'Common').
card_artist('urborg panther'/'MIR', 'Cliff Nielsen').
card_multiverse_id('urborg panther'/'MIR', '3319').

card_in_set('vaporous djinn', 'MIR').
card_original_type('vaporous djinn'/'MIR', 'Summon — Djinn').
card_original_text('vaporous djinn'/'MIR', 'Flying\nDuring your upkeep, pay {U}{U} or Vaporous Djinn phases out.').
card_first_print('vaporous djinn', 'MIR').
card_image_name('vaporous djinn'/'MIR', 'vaporous djinn').
card_uid('vaporous djinn'/'MIR', 'MIR:Vaporous Djinn:vaporous djinn').
card_rarity('vaporous djinn'/'MIR', 'Uncommon').
card_artist('vaporous djinn'/'MIR', 'Adam Rex').
card_flavor_text('vaporous djinn'/'MIR', '\"What is taking them so long to fill the waterskins?\"\n—Mwani, Mtenda goatherd').
card_multiverse_id('vaporous djinn'/'MIR', '3372').

card_in_set('ventifact bottle', 'MIR').
card_original_type('ventifact bottle'/'MIR', 'Artifact').
card_original_text('ventifact bottle'/'MIR', '{1}{X}, {T}: Put X charge counters on Ventifact Bottle. Play this ability as a sorcery.\nAt the beginning of your main phase, if Ventifact Bottle has any charge counters on it, tap Ventifact Bottle and remove all charge counters from it to add to your mana pool an amount of colorless mana equal to the number of charge counters removed.').
card_first_print('ventifact bottle', 'MIR').
card_image_name('ventifact bottle'/'MIR', 'ventifact bottle').
card_uid('ventifact bottle'/'MIR', 'MIR:Ventifact Bottle:ventifact bottle').
card_rarity('ventifact bottle'/'MIR', 'Rare').
card_artist('ventifact bottle'/'MIR', 'Ron Spencer').
card_multiverse_id('ventifact bottle'/'MIR', '3271').

card_in_set('viashino warrior', 'MIR').
card_original_type('viashino warrior'/'MIR', 'Summon — Viashino').
card_original_text('viashino warrior'/'MIR', '').
card_first_print('viashino warrior', 'MIR').
card_image_name('viashino warrior'/'MIR', 'viashino warrior').
card_uid('viashino warrior'/'MIR', 'MIR:Viashino Warrior:viashino warrior').
card_rarity('viashino warrior'/'MIR', 'Common').
card_artist('viashino warrior'/'MIR', 'Roger Raupp').
card_flavor_text('viashino warrior'/'MIR', '\"When traveling the Great Desert avoid wearing the scales of lizards, for the Viashino rule the sands and look poorly on the skinning of their cousins.\"\n—Zhalfirin Guide to the Desert').
card_multiverse_id('viashino warrior'/'MIR', '3471').

card_in_set('vigilant martyr', 'MIR').
card_original_type('vigilant martyr'/'MIR', 'Summon — Martyr').
card_original_text('vigilant martyr'/'MIR', 'Sacrifice Vigilant Martyr: Regenerate target creature. \n{W}{W}, {T}, Sacrifice Vigilant Martyr: Counter target spell that targets an enchantment in play. Play this ability as an interrupt.').
card_first_print('vigilant martyr', 'MIR').
card_image_name('vigilant martyr'/'MIR', 'vigilant martyr').
card_uid('vigilant martyr'/'MIR', 'MIR:Vigilant Martyr:vigilant martyr').
card_rarity('vigilant martyr'/'MIR', 'Uncommon').
card_artist('vigilant martyr'/'MIR', 'Rebecca Guay').
card_multiverse_id('vigilant martyr'/'MIR', '3520').

card_in_set('village elder', 'MIR').
card_original_type('village elder'/'MIR', 'Summon — Druid').
card_original_text('village elder'/'MIR', '{G}, {T}, Sacrifice a forest: Regenerate target creature.').
card_first_print('village elder', 'MIR').
card_image_name('village elder'/'MIR', 'village elder').
card_uid('village elder'/'MIR', 'MIR:Village Elder:village elder').
card_rarity('village elder'/'MIR', 'Common').
card_artist('village elder'/'MIR', 'Donato Giancola').
card_flavor_text('village elder'/'MIR', '\"Enchant me with your tale-telling. Tell about Tree, Grass, River, and Wind. / Tell why Truth must fight with Falsehood, and why Truth will always win.\"\n—\"Love Song of Night and Day\"').
card_multiverse_id('village elder'/'MIR', '3420').

card_in_set('vitalizing cascade', 'MIR').
card_original_type('vitalizing cascade'/'MIR', 'Instant').
card_original_text('vitalizing cascade'/'MIR', 'Gain X+3 life.').
card_first_print('vitalizing cascade', 'MIR').
card_image_name('vitalizing cascade'/'MIR', 'vitalizing cascade').
card_uid('vitalizing cascade'/'MIR', 'MIR:Vitalizing Cascade:vitalizing cascade').
card_rarity('vitalizing cascade'/'MIR', 'Uncommon').
card_artist('vitalizing cascade'/'MIR', 'Rebecca Guay').
card_flavor_text('vitalizing cascade'/'MIR', '\"Water is like breathing—necessary for life.\"\n—Asmira, Holy Avenger').
card_multiverse_id('vitalizing cascade'/'MIR', '3557').

card_in_set('volcanic dragon', 'MIR').
card_original_type('volcanic dragon'/'MIR', 'Summon — Dragon').
card_original_text('volcanic dragon'/'MIR', 'Flying\nVolcanic Dragon is unaffected by summoning sickness.').
card_first_print('volcanic dragon', 'MIR').
card_image_name('volcanic dragon'/'MIR', 'volcanic dragon').
card_uid('volcanic dragon'/'MIR', 'MIR:Volcanic Dragon:volcanic dragon').
card_rarity('volcanic dragon'/'MIR', 'Rare').
card_artist('volcanic dragon'/'MIR', 'Janine Johnston').
card_flavor_text('volcanic dragon'/'MIR', 'Speed and fire are always a deadly combination.').
card_multiverse_id('volcanic dragon'/'MIR', '3472').

card_in_set('volcanic geyser', 'MIR').
card_original_type('volcanic geyser'/'MIR', 'Instant').
card_original_text('volcanic geyser'/'MIR', 'Volcanic Geyser deals X damage to target creature or player.').
card_first_print('volcanic geyser', 'MIR').
card_image_name('volcanic geyser'/'MIR', 'volcanic geyser').
card_uid('volcanic geyser'/'MIR', 'MIR:Volcanic Geyser:volcanic geyser').
card_rarity('volcanic geyser'/'MIR', 'Uncommon').
card_artist('volcanic geyser'/'MIR', 'David O\'Connor').
card_flavor_text('volcanic geyser'/'MIR', 'My thunder comes before the lightning; my lightning comes before the clouds; my rain dries all the land it touches. What am I?\n—Femeref riddle').
card_multiverse_id('volcanic geyser'/'MIR', '3473').

card_in_set('waiting in the weeds', 'MIR').
card_original_type('waiting in the weeds'/'MIR', 'Sorcery').
card_original_text('waiting in the weeds'/'MIR', 'For each untapped forest he or she controls, each player puts a Cat token into play under his or her control. Treat these tokens as 1/1 green creatures.').
card_first_print('waiting in the weeds', 'MIR').
card_image_name('waiting in the weeds'/'MIR', 'waiting in the weeds').
card_uid('waiting in the weeds'/'MIR', 'MIR:Waiting in the Weeds:waiting in the weeds').
card_rarity('waiting in the weeds'/'MIR', 'Rare').
card_artist('waiting in the weeds'/'MIR', 'Susan Van Camp').
card_flavor_text('waiting in the weeds'/'MIR', 'The trees have ears, eyes, and teeth.').
card_multiverse_id('waiting in the weeds'/'MIR', '3421').

card_in_set('wall of corpses', 'MIR').
card_original_type('wall of corpses'/'MIR', 'Summon — Wall').
card_original_text('wall of corpses'/'MIR', '{B}, Sacrifice Wall of Corpses: Destroy target creature blocked by Wall of Corpses.').
card_first_print('wall of corpses', 'MIR').
card_image_name('wall of corpses'/'MIR', 'wall of corpses').
card_uid('wall of corpses'/'MIR', 'MIR:Wall of Corpses:wall of corpses').
card_rarity('wall of corpses'/'MIR', 'Common').
card_artist('wall of corpses'/'MIR', 'Gary Leach').
card_flavor_text('wall of corpses'/'MIR', '\"I shall make walls of their flesh, and their blood will be my mortar.\"\n—Kaervek').
card_multiverse_id('wall of corpses'/'MIR', '3320').

card_in_set('wall of resistance', 'MIR').
card_original_type('wall of resistance'/'MIR', 'Summon — Wall').
card_original_text('wall of resistance'/'MIR', 'Flying\nAt the end of any turn in which Wall of Resistance is dealt damage, put a +0/+1 counter on it.').
card_first_print('wall of resistance', 'MIR').
card_image_name('wall of resistance'/'MIR', 'wall of resistance').
card_uid('wall of resistance'/'MIR', 'MIR:Wall of Resistance:wall of resistance').
card_rarity('wall of resistance'/'MIR', 'Common').
card_artist('wall of resistance'/'MIR', 'Harold McNeill').
card_flavor_text('wall of resistance'/'MIR', 'Telim\'Tor could no longer recall the exact number of assaults—but his troops could.').
card_multiverse_id('wall of resistance'/'MIR', '3521').

card_in_set('wall of roots', 'MIR').
card_original_type('wall of roots'/'MIR', 'Summon — Wall').
card_original_text('wall of roots'/'MIR', 'Put a -0/-1 counter on Wall of Roots: Add {G} to your mana pool. Play this ability as a mana source. Use this ability only once each turn.').
card_first_print('wall of roots', 'MIR').
card_image_name('wall of roots'/'MIR', 'wall of roots').
card_uid('wall of roots'/'MIR', 'MIR:Wall of Roots:wall of roots').
card_rarity('wall of roots'/'MIR', 'Common').
card_artist('wall of roots'/'MIR', 'John Matson').
card_flavor_text('wall of roots'/'MIR', 'Sometimes the wise ones wove their magic into living plants; as the plant grew, so grew the magic.').
card_multiverse_id('wall of roots'/'MIR', '3422').

card_in_set('ward of lights', 'MIR').
card_original_type('ward of lights'/'MIR', 'Enchant Creature').
card_original_text('ward of lights'/'MIR', 'You may choose to play Ward of Lights as an instant; if you do, bury it at end of turn.\nEnchanted creature gains protection from a color of your choice. The protection granted by Ward of Lights does not bury Ward of Lights.').
card_first_print('ward of lights', 'MIR').
card_image_name('ward of lights'/'MIR', 'ward of lights').
card_uid('ward of lights'/'MIR', 'MIR:Ward of Lights:ward of lights').
card_rarity('ward of lights'/'MIR', 'Common').
card_artist('ward of lights'/'MIR', 'Mike Dringenberg').
card_multiverse_id('ward of lights'/'MIR', '3522').

card_in_set('warping wurm', 'MIR').
card_original_type('warping wurm'/'MIR', 'Summon — Wurm').
card_original_text('warping wurm'/'MIR', 'Phasing \nDuring your upkeep, pay {2}{G}{U} or Warping Wurm phases out. \nWhen Warping Wurm phases in, put a +1/+1 counter on it.').
card_first_print('warping wurm', 'MIR').
card_image_name('warping wurm'/'MIR', 'warping wurm').
card_uid('warping wurm'/'MIR', 'MIR:Warping Wurm:warping wurm').
card_rarity('warping wurm'/'MIR', 'Rare').
card_artist('warping wurm'/'MIR', 'Scott M. Fischer').
card_flavor_text('warping wurm'/'MIR', 'Even wurms start out small.').
card_multiverse_id('warping wurm'/'MIR', '3558').

card_in_set('wave elemental', 'MIR').
card_original_type('wave elemental'/'MIR', 'Summon — Elemental').
card_original_text('wave elemental'/'MIR', '{U}, {T}, Sacrifice Wave Elemental: Tap up to three target creatures without flying.').
card_first_print('wave elemental', 'MIR').
card_image_name('wave elemental'/'MIR', 'wave elemental').
card_uid('wave elemental'/'MIR', 'MIR:Wave Elemental:wave elemental').
card_rarity('wave elemental'/'MIR', 'Uncommon').
card_artist('wave elemental'/'MIR', 'Zak Plucinski').
card_flavor_text('wave elemental'/'MIR', '\"Consider the wave: how weak the mother yet how strong the child.\"\n—Tywanna, Shaper Guildmage').
card_multiverse_id('wave elemental'/'MIR', '3373').

card_in_set('wellspring', 'MIR').
card_original_type('wellspring'/'MIR', 'Enchant Land').
card_original_text('wellspring'/'MIR', 'When Wellspring comes into play, gain control of enchanted land. \nAt the end of each of your turns, lose control of enchanted land. \nAt the beginning of each of your turns, gain control of enchanted land.').
card_first_print('wellspring', 'MIR').
card_image_name('wellspring'/'MIR', 'wellspring').
card_uid('wellspring'/'MIR', 'MIR:Wellspring:wellspring').
card_rarity('wellspring'/'MIR', 'Rare').
card_artist('wellspring'/'MIR', 'Susan Van Camp').
card_multiverse_id('wellspring'/'MIR', '3559').

card_in_set('wild elephant', 'MIR').
card_original_type('wild elephant'/'MIR', 'Summon — Elephant').
card_original_text('wild elephant'/'MIR', 'Trample').
card_first_print('wild elephant', 'MIR').
card_image_name('wild elephant'/'MIR', 'wild elephant').
card_uid('wild elephant'/'MIR', 'MIR:Wild Elephant:wild elephant').
card_rarity('wild elephant'/'MIR', 'Common').
card_artist('wild elephant'/'MIR', 'Junior Tomlin').
card_flavor_text('wild elephant'/'MIR', '\"I will tell my father\'s stories . . . . How the elephants trampled the leopard cub, and its father, though he knew, killed nine goats instead.\"\n—\"Love Song of Night and Day\"').
card_multiverse_id('wild elephant'/'MIR', '3423').

card_in_set('wildfire emissary', 'MIR').
card_original_type('wildfire emissary'/'MIR', 'Summon — Efreet').
card_original_text('wildfire emissary'/'MIR', 'Protection from white \n{1}{R} +1/+0 until end of turn').
card_first_print('wildfire emissary', 'MIR').
card_image_name('wildfire emissary'/'MIR', 'wildfire emissary').
card_uid('wildfire emissary'/'MIR', 'MIR:Wildfire Emissary:wildfire emissary').
card_rarity('wildfire emissary'/'MIR', 'Uncommon').
card_artist('wildfire emissary'/'MIR', 'Richard Kane Ferguson').
card_flavor_text('wildfire emissary'/'MIR', '\"The efreet is a striding storm with a voice that crackles like fire.\"\n—Qhattib, Vizier of Amiqat').
card_multiverse_id('wildfire emissary'/'MIR', '3474').

card_in_set('windreaper falcon', 'MIR').
card_original_type('windreaper falcon'/'MIR', 'Summon — Falcon').
card_original_text('windreaper falcon'/'MIR', 'Flying, protection from blue').
card_first_print('windreaper falcon', 'MIR').
card_image_name('windreaper falcon'/'MIR', 'windreaper falcon').
card_uid('windreaper falcon'/'MIR', 'MIR:Windreaper Falcon:windreaper falcon').
card_rarity('windreaper falcon'/'MIR', 'Uncommon').
card_artist('windreaper falcon'/'MIR', 'Tony Roberts').
card_flavor_text('windreaper falcon'/'MIR', 'Watching the windreaper\n—Zhalfirin phrase meaning\n\"daydreaming\"').
card_multiverse_id('windreaper falcon'/'MIR', '3560').

card_in_set('withering boon', 'MIR').
card_original_type('withering boon'/'MIR', 'Interrupt').
card_original_text('withering boon'/'MIR', 'Pay 3 life: Counter target summon spell.').
card_first_print('withering boon', 'MIR').
card_image_name('withering boon'/'MIR', 'withering boon').
card_uid('withering boon'/'MIR', 'MIR:Withering Boon:withering boon').
card_rarity('withering boon'/'MIR', 'Uncommon').
card_artist('withering boon'/'MIR', 'Robert Bliss').
card_flavor_text('withering boon'/'MIR', '\"Life for life: this is the immutable law.\"\n—Panya, Granger Guildmage').
card_multiverse_id('withering boon'/'MIR', '3321').

card_in_set('worldly tutor', 'MIR').
card_original_type('worldly tutor'/'MIR', 'Instant').
card_original_text('worldly tutor'/'MIR', 'Search your library for a creature card and reveal that card to all players. Shuffle your library and put the revealed card back on top of it.').
card_first_print('worldly tutor', 'MIR').
card_image_name('worldly tutor'/'MIR', 'worldly tutor').
card_uid('worldly tutor'/'MIR', 'MIR:Worldly Tutor:worldly tutor').
card_rarity('worldly tutor'/'MIR', 'Uncommon').
card_artist('worldly tutor'/'MIR', 'David O\'Connor').
card_flavor_text('worldly tutor'/'MIR', '\"Aselbo soon had the rhino eating from his palm and the snake waiting at his heels.\"\n—Afari, Tales').
card_multiverse_id('worldly tutor'/'MIR', '3424').

card_in_set('yare', 'MIR').
card_original_type('yare'/'MIR', 'Instant').
card_original_text('yare'/'MIR', 'Target creature defending player controls gets +3/+0 until end of turn. That creature may be assigned to block up to three creatures this turn. All blocks must be legal.').
card_first_print('yare', 'MIR').
card_image_name('yare'/'MIR', 'yare').
card_uid('yare'/'MIR', 'MIR:Yare:yare').
card_rarity('yare'/'MIR', 'Rare').
card_artist('yare'/'MIR', 'Ron Spencer').
card_multiverse_id('yare'/'MIR', '3523').

card_in_set('zebra unicorn', 'MIR').
card_original_type('zebra unicorn'/'MIR', 'Summon — Unicorn').
card_original_text('zebra unicorn'/'MIR', 'For each 1 damage Zebra Unicorn deals, gain 1 life.').
card_first_print('zebra unicorn', 'MIR').
card_image_name('zebra unicorn'/'MIR', 'zebra unicorn').
card_uid('zebra unicorn'/'MIR', 'MIR:Zebra Unicorn:zebra unicorn').
card_rarity('zebra unicorn'/'MIR', 'Uncommon').
card_artist('zebra unicorn'/'MIR', 'Margaret Organ-Kean').
card_flavor_text('zebra unicorn'/'MIR', '\"I\'ll capture gentle zebras / for your steeds and fill the stable with every kind of unicorn.\"\n—\"Love Song of Night and Day\"').
card_multiverse_id('zebra unicorn'/'MIR', '3561').

card_in_set('zhalfirin commander', 'MIR').
card_original_type('zhalfirin commander'/'MIR', 'Summon — Knight').
card_original_text('zhalfirin commander'/'MIR', 'Flanking (Whenever a creature without flanking is assigned to block this creature, the blocking creature gets -1/-1 until end of turn.)\n{1}{W}{W} Target Knight gets +1/+1 until end of turn.').
card_first_print('zhalfirin commander', 'MIR').
card_image_name('zhalfirin commander'/'MIR', 'zhalfirin commander').
card_uid('zhalfirin commander'/'MIR', 'MIR:Zhalfirin Commander:zhalfirin commander').
card_rarity('zhalfirin commander'/'MIR', 'Uncommon').
card_artist('zhalfirin commander'/'MIR', 'Stuart Griffin').
card_flavor_text('zhalfirin commander'/'MIR', '\"Command is the act of balancing on a vine and convincing others that it is firm ground.\"\n—Sidar Jabari').
card_multiverse_id('zhalfirin commander'/'MIR', '3524').

card_in_set('zhalfirin knight', 'MIR').
card_original_type('zhalfirin knight'/'MIR', 'Summon — Knight').
card_original_text('zhalfirin knight'/'MIR', 'Flanking (Whenever a creature without flanking is assigned to block this creature, the blocking creature gets -1/-1 until end of turn.)\n{W}{W} First strike until end of turn').
card_first_print('zhalfirin knight', 'MIR').
card_image_name('zhalfirin knight'/'MIR', 'zhalfirin knight').
card_uid('zhalfirin knight'/'MIR', 'MIR:Zhalfirin Knight:zhalfirin knight').
card_rarity('zhalfirin knight'/'MIR', 'Common').
card_artist('zhalfirin knight'/'MIR', 'John Bolton').
card_flavor_text('zhalfirin knight'/'MIR', '\"You returned a warrior. . . . Your hair was cut, your eye tattooed with the red triangle of war.\"\n—\"Love Song of Night and Day\"').
card_multiverse_id('zhalfirin knight'/'MIR', '3525').

card_in_set('zirilan of the claw', 'MIR').
card_original_type('zirilan of the claw'/'MIR', 'Summon — Legend').
card_original_text('zirilan of the claw'/'MIR', '{1}{R}{R}, {T}: Search your library for a Dragon card and put it into play as though it were just played. Shuffle your library afterwards. That creature is unaffected by summoning sickness. Remove the creature from the game at end of any turn.').
card_first_print('zirilan of the claw', 'MIR').
card_image_name('zirilan of the claw'/'MIR', 'zirilan of the claw').
card_uid('zirilan of the claw'/'MIR', 'MIR:Zirilan of the Claw:zirilan of the claw').
card_rarity('zirilan of the claw'/'MIR', 'Rare').
card_artist('zirilan of the claw'/'MIR', 'Andrew Robinson').
card_multiverse_id('zirilan of the claw'/'MIR', '3475').

card_in_set('zombie mob', 'MIR').
card_original_type('zombie mob'/'MIR', 'Summon — Zombies').
card_original_text('zombie mob'/'MIR', 'Zombie Mob comes into play with one +1/+1 counter for each summon card in your graveyard. Remove all of those summon cards from the game.').
card_first_print('zombie mob', 'MIR').
card_image_name('zombie mob'/'MIR', 'zombie mob').
card_uid('zombie mob'/'MIR', 'MIR:Zombie Mob:zombie mob').
card_rarity('zombie mob'/'MIR', 'Uncommon').
card_artist('zombie mob'/'MIR', 'Terese Nielsen').
card_multiverse_id('zombie mob'/'MIR', '3322').

card_in_set('zuberi, golden feather', 'MIR').
card_original_type('zuberi, golden feather'/'MIR', 'Summon — Legend').
card_original_text('zuberi, golden feather'/'MIR', 'Flying\nZuberi, Golden Feather counts as a Griffin. \nAll other Griffins get +1/+1.').
card_first_print('zuberi, golden feather', 'MIR').
card_image_name('zuberi, golden feather'/'MIR', 'zuberi, golden feather').
card_uid('zuberi, golden feather'/'MIR', 'MIR:Zuberi, Golden Feather:zuberi, golden feather').
card_rarity('zuberi, golden feather'/'MIR', 'Rare').
card_artist('zuberi, golden feather'/'MIR', 'Alan Rabinowitz').
card_flavor_text('zuberi, golden feather'/'MIR', '\"If the griffins tell of their gods, perhaps they speak of feathers bright as the Sun.\"\n—Afari, Tales').
card_multiverse_id('zuberi, golden feather'/'MIR', '3526').
