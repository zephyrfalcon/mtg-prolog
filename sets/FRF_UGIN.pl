% Ugin's Fate promos

set('FRF_UGIN').
set_name('FRF_UGIN', 'Ugin\'s Fate promos').
set_release_date('FRF_UGIN', '2015-01-17').
set_border('FRF_UGIN', 'black').
set_type('FRF_UGIN', 'promo').

card_in_set('ainok tracker', 'FRF_UGIN').
card_original_type('ainok tracker'/'FRF_UGIN', 'Creature — Hound Scout').
card_original_text('ainok tracker'/'FRF_UGIN', 'First strike\nMorph {4}{R} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_image_name('ainok tracker'/'FRF_UGIN', 'ainok tracker').
card_uid('ainok tracker'/'FRF_UGIN', 'FRF_UGIN:Ainok Tracker:ainok tracker').
card_rarity('ainok tracker'/'FRF_UGIN', 'Common').
card_artist('ainok tracker'/'FRF_UGIN', 'Evan Shipard').
card_number('ainok tracker'/'FRF_UGIN', '96').
card_flavor_text('ainok tracker'/'FRF_UGIN', '\"The ainok are wild dogs, not fit to join us around the fire.\"\n—Surrak, the Hunt Caller').
card_multiverse_id('ainok tracker'/'FRF_UGIN', '394065').

card_in_set('altar of the brood', 'FRF_UGIN').
card_original_type('altar of the brood'/'FRF_UGIN', 'Artifact').
card_original_text('altar of the brood'/'FRF_UGIN', 'Whenever another permanent enters the battlefield under your control, each opponent puts the top card of his or her library into his or her graveyard.').
card_image_name('altar of the brood'/'FRF_UGIN', 'altar of the brood').
card_uid('altar of the brood'/'FRF_UGIN', 'FRF_UGIN:Altar of the Brood:altar of the brood').
card_rarity('altar of the brood'/'FRF_UGIN', 'Rare').
card_artist('altar of the brood'/'FRF_UGIN', 'Erica Yang').
card_number('altar of the brood'/'FRF_UGIN', '216').
card_flavor_text('altar of the brood'/'FRF_UGIN', 'Supplicants offer flesh and silver, flowers and blood. The altar takes what it will, eyes gleaming with unspoken promises.').
card_multiverse_id('altar of the brood'/'FRF_UGIN', '394066').

card_in_set('arashin war beast', 'FRF_UGIN').
card_original_type('arashin war beast'/'FRF_UGIN', 'Creature — Beast').
card_original_text('arashin war beast'/'FRF_UGIN', 'Whenever Arashin War Beast deals combat damage to one or more blocking creatures, manifest the top card of your library. (Put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)').
card_first_print('arashin war beast', 'FRF_UGIN').
card_image_name('arashin war beast'/'FRF_UGIN', 'arashin war beast').
card_uid('arashin war beast'/'FRF_UGIN', 'FRF_UGIN:Arashin War Beast:arashin war beast').
card_rarity('arashin war beast'/'FRF_UGIN', 'Uncommon').
card_artist('arashin war beast'/'FRF_UGIN', 'Eric Deschamps').
card_number('arashin war beast'/'FRF_UGIN', '123').
card_multiverse_id('arashin war beast'/'FRF_UGIN', '394067').
card_watermark('arashin war beast'/'FRF_UGIN', 'Temur').

card_in_set('arc lightning', 'FRF_UGIN').
card_original_type('arc lightning'/'FRF_UGIN', 'Sorcery').
card_original_text('arc lightning'/'FRF_UGIN', 'Arc Lightning deals 3 damage divided as you choose among one, two, or three target creatures and/or players.').
card_image_name('arc lightning'/'FRF_UGIN', 'arc lightning').
card_uid('arc lightning'/'FRF_UGIN', 'FRF_UGIN:Arc Lightning:arc lightning').
card_rarity('arc lightning'/'FRF_UGIN', 'Uncommon').
card_artist('arc lightning'/'FRF_UGIN', 'Seb McKinnon').
card_number('arc lightning'/'FRF_UGIN', '97').
card_flavor_text('arc lightning'/'FRF_UGIN', 'Lightning burns the dragon\'s path.').
card_multiverse_id('arc lightning'/'FRF_UGIN', '394068').

card_in_set('briber\'s purse', 'FRF_UGIN').
card_original_type('briber\'s purse'/'FRF_UGIN', 'Artifact').
card_original_text('briber\'s purse'/'FRF_UGIN', 'Briber\'s Purse enters the battlefield with X gem counters on it.\n{1}, {T}, Remove a gem counter from Briber\'s Purse: Target creature can\'t attack or block this turn.').
card_image_name('briber\'s purse'/'FRF_UGIN', 'briber\'s purse').
card_uid('briber\'s purse'/'FRF_UGIN', 'FRF_UGIN:Briber\'s Purse:briber\'s purse').
card_rarity('briber\'s purse'/'FRF_UGIN', 'Uncommon').
card_artist('briber\'s purse'/'FRF_UGIN', 'Steve Argyle').
card_number('briber\'s purse'/'FRF_UGIN', '217').
card_flavor_text('briber\'s purse'/'FRF_UGIN', 'The bigger the hand, the higher the price.').
card_multiverse_id('briber\'s purse'/'FRF_UGIN', '394069').

card_in_set('debilitating injury', 'FRF_UGIN').
card_original_type('debilitating injury'/'FRF_UGIN', 'Enchantment — Aura').
card_original_text('debilitating injury'/'FRF_UGIN', 'Enchant creature\nEnchanted creature gets -2/-2.').
card_image_name('debilitating injury'/'FRF_UGIN', 'debilitating injury').
card_uid('debilitating injury'/'FRF_UGIN', 'FRF_UGIN:Debilitating Injury:debilitating injury').
card_rarity('debilitating injury'/'FRF_UGIN', 'Common').
card_artist('debilitating injury'/'FRF_UGIN', 'Slawomir Maniak').
card_number('debilitating injury'/'FRF_UGIN', '68').
card_flavor_text('debilitating injury'/'FRF_UGIN', 'Weakness exists within all who are not dragons.').
card_multiverse_id('debilitating injury'/'FRF_UGIN', '394070').

card_in_set('dragonscale boon', 'FRF_UGIN').
card_original_type('dragonscale boon'/'FRF_UGIN', 'Instant').
card_original_text('dragonscale boon'/'FRF_UGIN', 'Put two +1/+1 counters on target creature and untap it.').
card_image_name('dragonscale boon'/'FRF_UGIN', 'dragonscale boon').
card_uid('dragonscale boon'/'FRF_UGIN', 'FRF_UGIN:Dragonscale Boon:dragonscale boon').
card_rarity('dragonscale boon'/'FRF_UGIN', 'Common').
card_artist('dragonscale boon'/'FRF_UGIN', 'Mark Winters').
card_number('dragonscale boon'/'FRF_UGIN', '131').
card_flavor_text('dragonscale boon'/'FRF_UGIN', 'Only those who have proved themselves worthy are granted a dragon\'s scale.').
card_multiverse_id('dragonscale boon'/'FRF_UGIN', '394071').

card_in_set('fierce invocation', 'FRF_UGIN').
card_original_type('fierce invocation'/'FRF_UGIN', 'Sorcery').
card_original_text('fierce invocation'/'FRF_UGIN', 'Manifest the top card of your library, then put two +1/+1 counters on it. (To manifest a card, put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)').
card_first_print('fierce invocation', 'FRF_UGIN').
card_image_name('fierce invocation'/'FRF_UGIN', 'fierce invocation').
card_uid('fierce invocation'/'FRF_UGIN', 'FRF_UGIN:Fierce Invocation:fierce invocation').
card_rarity('fierce invocation'/'FRF_UGIN', 'Common').
card_artist('fierce invocation'/'FRF_UGIN', 'Tyler Jacobson').
card_number('fierce invocation'/'FRF_UGIN', '98').
card_flavor_text('fierce invocation'/'FRF_UGIN', 'Anger can empower those who embrace it.').
card_multiverse_id('fierce invocation'/'FRF_UGIN', '394072').

card_in_set('formless nurturing', 'FRF_UGIN').
card_original_type('formless nurturing'/'FRF_UGIN', 'Sorcery').
card_original_text('formless nurturing'/'FRF_UGIN', 'Manifest the top card of your library, then put a +1/+1 counter on it. (To manifest a card, put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)').
card_first_print('formless nurturing', 'FRF_UGIN').
card_image_name('formless nurturing'/'FRF_UGIN', 'formless nurturing').
card_uid('formless nurturing'/'FRF_UGIN', 'FRF_UGIN:Formless Nurturing:formless nurturing').
card_rarity('formless nurturing'/'FRF_UGIN', 'Common').
card_artist('formless nurturing'/'FRF_UGIN', 'Cliff Childs').
card_number('formless nurturing'/'FRF_UGIN', '129').
card_multiverse_id('formless nurturing'/'FRF_UGIN', '394073').

card_in_set('ghostfire blade', 'FRF_UGIN').
card_original_type('ghostfire blade'/'FRF_UGIN', 'Artifact — Equipment').
card_original_text('ghostfire blade'/'FRF_UGIN', 'Equipped creature gets +2/+2.\nEquip {3}\nGhostfire Blade\'s equip ability costs {2} less to activate if it targets a colorless creature.').
card_image_name('ghostfire blade'/'FRF_UGIN', 'ghostfire blade').
card_uid('ghostfire blade'/'FRF_UGIN', 'FRF_UGIN:Ghostfire Blade:ghostfire blade').
card_rarity('ghostfire blade'/'FRF_UGIN', 'Rare').
card_artist('ghostfire blade'/'FRF_UGIN', 'Cyril Van Der Haegen').
card_number('ghostfire blade'/'FRF_UGIN', '220').
card_flavor_text('ghostfire blade'/'FRF_UGIN', 'If you fear the dragon\'s fire, you are unworthy to wield it.').
card_multiverse_id('ghostfire blade'/'FRF_UGIN', '394074').

card_in_set('grim haruspex', 'FRF_UGIN').
card_original_type('grim haruspex'/'FRF_UGIN', 'Creature — Human Wizard').
card_original_text('grim haruspex'/'FRF_UGIN', 'Morph {B} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhenever another nontoken creature you control dies, draw a card.').
card_image_name('grim haruspex'/'FRF_UGIN', 'grim haruspex').
card_uid('grim haruspex'/'FRF_UGIN', 'FRF_UGIN:Grim Haruspex:grim haruspex').
card_rarity('grim haruspex'/'FRF_UGIN', 'Rare').
card_artist('grim haruspex'/'FRF_UGIN', 'Seb McKinnon').
card_number('grim haruspex'/'FRF_UGIN', '73').
card_flavor_text('grim haruspex'/'FRF_UGIN', '\"Your enemies may be cowardly, my lord, but one should never call them gutless.\"').
card_multiverse_id('grim haruspex'/'FRF_UGIN', '394075').

card_in_set('hewed stone retainers', 'FRF_UGIN').
card_original_type('hewed stone retainers'/'FRF_UGIN', 'Artifact Creature — Golem').
card_original_text('hewed stone retainers'/'FRF_UGIN', 'Cast Hewed Stone Retainers only if you\'ve cast another spell this turn.').
card_first_print('hewed stone retainers', 'FRF_UGIN').
card_image_name('hewed stone retainers'/'FRF_UGIN', 'hewed stone retainers').
card_uid('hewed stone retainers'/'FRF_UGIN', 'FRF_UGIN:Hewed Stone Retainers:hewed stone retainers').
card_rarity('hewed stone retainers'/'FRF_UGIN', 'Uncommon').
card_artist('hewed stone retainers'/'FRF_UGIN', 'David Seguin').
card_number('hewed stone retainers'/'FRF_UGIN', '161').
card_flavor_text('hewed stone retainers'/'FRF_UGIN', '\"Their origins are shrouded in mystery. I believe they are the protectors of the lost secrets of our world.\"\n—Jilaya, Temur whisperer').
card_multiverse_id('hewed stone retainers'/'FRF_UGIN', '394076').

card_in_set('jeering instigator', 'FRF_UGIN').
card_original_type('jeering instigator'/'FRF_UGIN', 'Creature — Goblin Rogue').
card_original_text('jeering instigator'/'FRF_UGIN', 'Morph {2}{R} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Jeering Instigator is turned face up, if it\'s your turn, gain control of another target creature until end of turn. Untap that creature. It gains haste until end of turn.').
card_image_name('jeering instigator'/'FRF_UGIN', 'jeering instigator').
card_uid('jeering instigator'/'FRF_UGIN', 'FRF_UGIN:Jeering Instigator:jeering instigator').
card_rarity('jeering instigator'/'FRF_UGIN', 'Rare').
card_artist('jeering instigator'/'FRF_UGIN', 'Willian Murai').
card_number('jeering instigator'/'FRF_UGIN', '113').
card_multiverse_id('jeering instigator'/'FRF_UGIN', '394077').

card_in_set('jeskai infiltrator', 'FRF_UGIN').
card_original_type('jeskai infiltrator'/'FRF_UGIN', 'Creature — Human Monk').
card_original_text('jeskai infiltrator'/'FRF_UGIN', 'Jeskai Infiltrator can\'t be blocked as long as you control no other creatures.\nWhen Jeskai Infiltrator deals combat damage to a player, exile it and the top card of your library in a face-down pile, shuffle that pile, then manifest those cards. (To manifest a card, put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)').
card_first_print('jeskai infiltrator', 'FRF_UGIN').
card_image_name('jeskai infiltrator'/'FRF_UGIN', 'jeskai infiltrator').
card_uid('jeskai infiltrator'/'FRF_UGIN', 'FRF_UGIN:Jeskai Infiltrator:jeskai infiltrator').
card_rarity('jeskai infiltrator'/'FRF_UGIN', 'Rare').
card_artist('jeskai infiltrator'/'FRF_UGIN', 'Cynthia Sheppard').
card_number('jeskai infiltrator'/'FRF_UGIN', '36').
card_multiverse_id('jeskai infiltrator'/'FRF_UGIN', '394078').
card_watermark('jeskai infiltrator'/'FRF_UGIN', 'Jeskai').

card_in_set('mastery of the unseen', 'FRF_UGIN').
card_original_type('mastery of the unseen'/'FRF_UGIN', 'Enchantment').
card_original_text('mastery of the unseen'/'FRF_UGIN', 'Whenever a permanent you control is turned face up, you gain 1 life for each creature you control.\n{3}{W}: Manifest the top card of your library. (Put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)').
card_first_print('mastery of the unseen', 'FRF_UGIN').
card_image_name('mastery of the unseen'/'FRF_UGIN', 'mastery of the unseen').
card_uid('mastery of the unseen'/'FRF_UGIN', 'FRF_UGIN:Mastery of the Unseen:mastery of the unseen').
card_rarity('mastery of the unseen'/'FRF_UGIN', 'Rare').
card_artist('mastery of the unseen'/'FRF_UGIN', 'Daniel Ljunggren').
card_number('mastery of the unseen'/'FRF_UGIN', '19').
card_multiverse_id('mastery of the unseen'/'FRF_UGIN', '394079').

card_in_set('mystic of the hidden way', 'FRF_UGIN').
card_original_type('mystic of the hidden way'/'FRF_UGIN', 'Creature — Human Monk').
card_original_text('mystic of the hidden way'/'FRF_UGIN', 'Mystic of the Hidden Way can\'t be blocked.\nMorph {2}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_image_name('mystic of the hidden way'/'FRF_UGIN', 'mystic of the hidden way').
card_uid('mystic of the hidden way'/'FRF_UGIN', 'FRF_UGIN:Mystic of the Hidden Way:mystic of the hidden way').
card_rarity('mystic of the hidden way'/'FRF_UGIN', 'Common').
card_artist('mystic of the hidden way'/'FRF_UGIN', 'Ryan Alexander Lee').
card_number('mystic of the hidden way'/'FRF_UGIN', '48').
card_flavor_text('mystic of the hidden way'/'FRF_UGIN', '\"There are many different paths. The Great Teacher reveals them all.\"').
card_multiverse_id('mystic of the hidden way'/'FRF_UGIN', '394080').

card_in_set('reality shift', 'FRF_UGIN').
card_original_type('reality shift'/'FRF_UGIN', 'Instant').
card_original_text('reality shift'/'FRF_UGIN', 'Exile target creature. Its controller manifests the top card of his or her library. (That player puts the top card of his or her library onto the battlefield face down as a 2/2 creature. If it\'s a creature card, it can be turned face up any time for its mana cost.)').
card_first_print('reality shift', 'FRF_UGIN').
card_image_name('reality shift'/'FRF_UGIN', 'reality shift').
card_uid('reality shift'/'FRF_UGIN', 'FRF_UGIN:Reality Shift:reality shift').
card_rarity('reality shift'/'FRF_UGIN', 'Uncommon').
card_artist('reality shift'/'FRF_UGIN', 'Howard Lyon').
card_number('reality shift'/'FRF_UGIN', '46').
card_multiverse_id('reality shift'/'FRF_UGIN', '394081').

card_in_set('ruthless ripper', 'FRF_UGIN').
card_original_type('ruthless ripper'/'FRF_UGIN', 'Creature — Human Assassin').
card_original_text('ruthless ripper'/'FRF_UGIN', 'Deathtouch\nMorph—Reveal a black card in your hand. (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Ruthless Ripper is turned face up, target player loses 2 life.').
card_image_name('ruthless ripper'/'FRF_UGIN', 'ruthless ripper').
card_uid('ruthless ripper'/'FRF_UGIN', 'FRF_UGIN:Ruthless Ripper:ruthless ripper').
card_rarity('ruthless ripper'/'FRF_UGIN', 'Uncommon').
card_artist('ruthless ripper'/'FRF_UGIN', 'Clint Cearley').
card_number('ruthless ripper'/'FRF_UGIN', '88').
card_multiverse_id('ruthless ripper'/'FRF_UGIN', '394082').

card_in_set('smite the monstrous', 'FRF_UGIN').
card_original_type('smite the monstrous'/'FRF_UGIN', 'Instant').
card_original_text('smite the monstrous'/'FRF_UGIN', 'Destroy target creature with power 4 or greater.').
card_image_name('smite the monstrous'/'FRF_UGIN', 'smite the monstrous').
card_uid('smite the monstrous'/'FRF_UGIN', 'FRF_UGIN:Smite the Monstrous:smite the monstrous').
card_rarity('smite the monstrous'/'FRF_UGIN', 'Common').
card_artist('smite the monstrous'/'FRF_UGIN', 'Greg Staples').
card_number('smite the monstrous'/'FRF_UGIN', '24').
card_flavor_text('smite the monstrous'/'FRF_UGIN', 'The changing of a single moment in Tarkir\'s past altered its future, rewriting its history.').
card_multiverse_id('smite the monstrous'/'FRF_UGIN', '394083').

card_in_set('soul summons', 'FRF_UGIN').
card_original_type('soul summons'/'FRF_UGIN', 'Sorcery').
card_original_text('soul summons'/'FRF_UGIN', 'Manifest the top card of your library. (Put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)').
card_first_print('soul summons', 'FRF_UGIN').
card_image_name('soul summons'/'FRF_UGIN', 'soul summons').
card_uid('soul summons'/'FRF_UGIN', 'FRF_UGIN:Soul Summons:soul summons').
card_rarity('soul summons'/'FRF_UGIN', 'Common').
card_artist('soul summons'/'FRF_UGIN', 'Johann Bodin').
card_number('soul summons'/'FRF_UGIN', '26').
card_flavor_text('soul summons'/'FRF_UGIN', 'Ugin\'s magic reaches beyond the dragons. The clans have adapted it for war.').
card_multiverse_id('soul summons'/'FRF_UGIN', '394084').

card_in_set('sultai emissary', 'FRF_UGIN').
card_original_type('sultai emissary'/'FRF_UGIN', 'Creature — Zombie Warrior').
card_original_text('sultai emissary'/'FRF_UGIN', 'When Sultai Emissary dies, manifest the top card of your library. (Put that card onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)').
card_first_print('sultai emissary', 'FRF_UGIN').
card_image_name('sultai emissary'/'FRF_UGIN', 'sultai emissary').
card_uid('sultai emissary'/'FRF_UGIN', 'FRF_UGIN:Sultai Emissary:sultai emissary').
card_rarity('sultai emissary'/'FRF_UGIN', 'Common').
card_artist('sultai emissary'/'FRF_UGIN', 'Mathias Kollros').
card_number('sultai emissary'/'FRF_UGIN', '85').
card_flavor_text('sultai emissary'/'FRF_UGIN', 'To the Sultai, Ugin\'s power was just one more resource to be exploited.').
card_multiverse_id('sultai emissary'/'FRF_UGIN', '394085').
card_watermark('sultai emissary'/'FRF_UGIN', 'Sultai').

card_in_set('ugin\'s construct', 'FRF_UGIN').
card_original_type('ugin\'s construct'/'FRF_UGIN', 'Artifact Creature — Construct').
card_original_text('ugin\'s construct'/'FRF_UGIN', 'When Ugin\'s Construct enters the battlefield, sacrifice a permanent that\'s one or more colors.').
card_first_print('ugin\'s construct', 'FRF_UGIN').
card_image_name('ugin\'s construct'/'FRF_UGIN', 'ugin\'s construct').
card_uid('ugin\'s construct'/'FRF_UGIN', 'FRF_UGIN:Ugin\'s Construct:ugin\'s construct').
card_rarity('ugin\'s construct'/'FRF_UGIN', 'Uncommon').
card_artist('ugin\'s construct'/'FRF_UGIN', 'Peter Mohrbacher').
card_number('ugin\'s construct'/'FRF_UGIN', '164').
card_flavor_text('ugin\'s construct'/'FRF_UGIN', 'While trapping the Eldrazi on Zendikar, Ugin learned little from Sorin, but he gleaned the rudiments of lithomancy from Nahiri.').
card_multiverse_id('ugin\'s construct'/'FRF_UGIN', '394087').

card_in_set('ugin, the spirit dragon', 'FRF_UGIN').
card_original_type('ugin, the spirit dragon'/'FRF_UGIN', 'Planeswalker — Ugin').
card_original_text('ugin, the spirit dragon'/'FRF_UGIN', '+2: Ugin, the Spirit Dragon deals 3 damage to target creature or player.\n−X: Exile each permanent with converted mana cost X or less that\'s one or more colors.\n−10: You gain 7 life, draw seven cards, then put up to seven permanent cards from your hand onto the battlefield.').
card_first_print('ugin, the spirit dragon', 'FRF_UGIN').
card_image_name('ugin, the spirit dragon'/'FRF_UGIN', 'ugin, the spirit dragon').
card_uid('ugin, the spirit dragon'/'FRF_UGIN', 'FRF_UGIN:Ugin, the Spirit Dragon:ugin, the spirit dragon').
card_rarity('ugin, the spirit dragon'/'FRF_UGIN', 'Mythic Rare').
card_artist('ugin, the spirit dragon'/'FRF_UGIN', 'Chris Rahn').
card_number('ugin, the spirit dragon'/'FRF_UGIN', '1').
card_multiverse_id('ugin, the spirit dragon'/'FRF_UGIN', '394086').

card_in_set('watcher of the roost', 'FRF_UGIN').
card_original_type('watcher of the roost'/'FRF_UGIN', 'Creature — Bird Soldier').
card_original_text('watcher of the roost'/'FRF_UGIN', 'Flying\nMorph—Reveal a white card in your hand. (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Watcher of the Roost is turned face up, you gain 2 life.').
card_image_name('watcher of the roost'/'FRF_UGIN', 'watcher of the roost').
card_uid('watcher of the roost'/'FRF_UGIN', 'FRF_UGIN:Watcher of the Roost:watcher of the roost').
card_rarity('watcher of the roost'/'FRF_UGIN', 'Uncommon').
card_artist('watcher of the roost'/'FRF_UGIN', 'Jack Wang').
card_number('watcher of the roost'/'FRF_UGIN', '30').
card_multiverse_id('watcher of the roost'/'FRF_UGIN', '394088').

card_in_set('wildcall', 'FRF_UGIN').
card_original_type('wildcall'/'FRF_UGIN', 'Sorcery').
card_original_text('wildcall'/'FRF_UGIN', 'Manifest the top card of your library, then put X +1/+1 counters on it. (To manifest a card, put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)').
card_first_print('wildcall', 'FRF_UGIN').
card_image_name('wildcall'/'FRF_UGIN', 'wildcall').
card_uid('wildcall'/'FRF_UGIN', 'FRF_UGIN:Wildcall:wildcall').
card_rarity('wildcall'/'FRF_UGIN', 'Rare').
card_artist('wildcall'/'FRF_UGIN', 'Adam Paquette').
card_number('wildcall'/'FRF_UGIN', '146').
card_flavor_text('wildcall'/'FRF_UGIN', 'A howl on the wind hides many dangers.').
card_multiverse_id('wildcall'/'FRF_UGIN', '394089').

card_in_set('write into being', 'FRF_UGIN').
card_original_type('write into being'/'FRF_UGIN', 'Sorcery').
card_original_text('write into being'/'FRF_UGIN', 'Look at the top two cards of your library. Manifest one of those cards, then put the other on the top or bottom of your library. (To manifest a card, put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)').
card_first_print('write into being', 'FRF_UGIN').
card_image_name('write into being'/'FRF_UGIN', 'write into being').
card_uid('write into being'/'FRF_UGIN', 'FRF_UGIN:Write into Being:write into being').
card_rarity('write into being'/'FRF_UGIN', 'Common').
card_artist('write into being'/'FRF_UGIN', 'Yeong-Hao Han').
card_number('write into being'/'FRF_UGIN', '59').
card_multiverse_id('write into being'/'FRF_UGIN', '394090').
