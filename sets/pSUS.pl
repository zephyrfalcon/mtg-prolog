% Super Series

set('pSUS').
set_name('pSUS', 'Super Series').
set_release_date('pSUS', '1999-12-01').
set_border('pSUS', 'black').
set_type('pSUS', 'promo').

card_in_set('city of brass', 'pSUS').
card_original_type('city of brass'/'pSUS', 'Land').
card_original_text('city of brass'/'pSUS', '').
card_image_name('city of brass'/'pSUS', 'city of brass').
card_uid('city of brass'/'pSUS', 'pSUS:City of Brass:city of brass').
card_rarity('city of brass'/'pSUS', 'Special').
card_artist('city of brass'/'pSUS', 'Mark Tedin').
card_number('city of brass'/'pSUS', '6').
card_flavor_text('city of brass'/'pSUS', '\"Enter this palace-gate and ask the news Of greatness fallen into dust and clay.\"\n—The Arabian Nights, trans. Burton').

card_in_set('crusade', 'pSUS').
card_original_type('crusade'/'pSUS', 'Enchantment').
card_original_text('crusade'/'pSUS', '').
card_image_name('crusade'/'pSUS', 'crusade').
card_uid('crusade'/'pSUS', 'pSUS:Crusade:crusade').
card_rarity('crusade'/'pSUS', 'Special').
card_artist('crusade'/'pSUS', 'Mark Poole').
card_number('crusade'/'pSUS', '4').

card_in_set('elvish champion', 'pSUS').
card_original_type('elvish champion'/'pSUS', 'Creature — Elf').
card_original_text('elvish champion'/'pSUS', '').
card_first_print('elvish champion', 'pSUS').
card_image_name('elvish champion'/'pSUS', 'elvish champion1').
card_uid('elvish champion'/'pSUS', 'pSUS:Elvish Champion:elvish champion1').
card_rarity('elvish champion'/'pSUS', 'Special').
card_artist('elvish champion'/'pSUS', 'Matt Cavotta').
card_number('elvish champion'/'pSUS', '17').
card_flavor_text('elvish champion'/'pSUS', '\"We\'ve endured your slights to Gaea with the grace of tolerance. Now behold the grace of our ferocity.\"').

card_in_set('elvish champion', 'pSUS').
card_original_type('elvish champion'/'pSUS', 'Creature — Elf').
card_original_text('elvish champion'/'pSUS', '').
card_image_name('elvish champion'/'pSUS', 'elvish champion2').
card_uid('elvish champion'/'pSUS', 'pSUS:Elvish Champion:elvish champion2').
card_rarity('elvish champion'/'pSUS', 'Special').
card_artist('elvish champion'/'pSUS', 'Matt Cavotta').
card_number('elvish champion'/'pSUS', '26').
card_flavor_text('elvish champion'/'pSUS', '\"We\'ve endured your slights to Gaea with the grace of tolerance. Now behold the grace of our ferocity.\"').

card_in_set('elvish champion', 'pSUS').
card_original_type('elvish champion'/'pSUS', 'Creature — Elf').
card_original_text('elvish champion'/'pSUS', '').
card_image_name('elvish champion'/'pSUS', 'elvish champion3').
card_uid('elvish champion'/'pSUS', 'pSUS:Elvish Champion:elvish champion3').
card_rarity('elvish champion'/'pSUS', 'Special').
card_artist('elvish champion'/'pSUS', 'Matt Cavotta').
card_number('elvish champion'/'pSUS', '32').
card_flavor_text('elvish champion'/'pSUS', '\"We\'ve endured your slights to Gaea with the grace of tolerance. Now behold the grace of our ferocity.\"').

card_in_set('elvish lyrist', 'pSUS').
card_original_type('elvish lyrist'/'pSUS', 'Creature — Elf').
card_original_text('elvish lyrist'/'pSUS', '').
card_image_name('elvish lyrist'/'pSUS', 'elvish lyrist').
card_uid('elvish lyrist'/'pSUS', 'pSUS:Elvish Lyrist:elvish lyrist').
card_rarity('elvish lyrist'/'pSUS', 'Special').
card_artist('elvish lyrist'/'pSUS', 'Rebecca Guay').
card_number('elvish lyrist'/'pSUS', '5').
card_flavor_text('elvish lyrist'/'pSUS', '\"Make me thy lyre, even as the forest is: What if my leaves are falling like its own!\" - Percy Bysshe Shelley, \"Ode to the West Wind\"').

card_in_set('giant growth', 'pSUS').
card_original_type('giant growth'/'pSUS', 'Instant').
card_original_text('giant growth'/'pSUS', '').
card_image_name('giant growth'/'pSUS', 'giant growth').
card_uid('giant growth'/'pSUS', 'pSUS:Giant Growth:giant growth').
card_rarity('giant growth'/'pSUS', 'Special').
card_artist('giant growth'/'pSUS', 'Terese Nielsen').
card_number('giant growth'/'pSUS', '8').

card_in_set('glorious anthem', 'pSUS').
card_original_type('glorious anthem'/'pSUS', 'Enchantment').
card_original_text('glorious anthem'/'pSUS', '').
card_image_name('glorious anthem'/'pSUS', 'glorious anthem1').
card_uid('glorious anthem'/'pSUS', 'pSUS:Glorious Anthem:glorious anthem1').
card_rarity('glorious anthem'/'pSUS', 'Special').
card_artist('glorious anthem'/'pSUS', 'Raymond Swanland').
card_number('glorious anthem'/'pSUS', '16').
card_flavor_text('glorious anthem'/'pSUS', 'Once heard, the battle song of an angel becomes part of the listener forever.').

card_in_set('glorious anthem', 'pSUS').
card_original_type('glorious anthem'/'pSUS', 'Enchantment').
card_original_text('glorious anthem'/'pSUS', '').
card_image_name('glorious anthem'/'pSUS', 'glorious anthem2').
card_uid('glorious anthem'/'pSUS', 'pSUS:Glorious Anthem:glorious anthem2').
card_rarity('glorious anthem'/'pSUS', 'Special').
card_artist('glorious anthem'/'pSUS', 'Raymond Swanland').
card_number('glorious anthem'/'pSUS', '25').
card_flavor_text('glorious anthem'/'pSUS', 'Once heard, the battle song of an angel becomes part of the listener forever.').

card_in_set('glorious anthem', 'pSUS').
card_original_type('glorious anthem'/'pSUS', 'Enchantment').
card_original_text('glorious anthem'/'pSUS', '').
card_image_name('glorious anthem'/'pSUS', 'glorious anthem3').
card_uid('glorious anthem'/'pSUS', 'pSUS:Glorious Anthem:glorious anthem3').
card_rarity('glorious anthem'/'pSUS', 'Special').
card_artist('glorious anthem'/'pSUS', 'Raymond Swanland').
card_number('glorious anthem'/'pSUS', '31').
card_flavor_text('glorious anthem'/'pSUS', 'The songs of angels are perceived not as words in the ears but as stirrings in the soul.').

card_in_set('lord of atlantis', 'pSUS').
card_original_type('lord of atlantis'/'pSUS', 'Creature — Merfolk').
card_original_text('lord of atlantis'/'pSUS', '').
card_image_name('lord of atlantis'/'pSUS', 'lord of atlantis').
card_uid('lord of atlantis'/'pSUS', 'pSUS:Lord of Atlantis:lord of atlantis').
card_rarity('lord of atlantis'/'pSUS', 'Special').
card_artist('lord of atlantis'/'pSUS', 'Melissa A. Benson').
card_number('lord of atlantis'/'pSUS', '3').
card_flavor_text('lord of atlantis'/'pSUS', 'A master of tactics, the Lord of Atlantis makes his people bold in battle merely by arriving to lead them.').

card_in_set('mad auntie', 'pSUS').
card_original_type('mad auntie'/'pSUS', 'Creature — Goblin Shaman').
card_original_text('mad auntie'/'pSUS', '').
card_first_print('mad auntie', 'pSUS').
card_image_name('mad auntie'/'pSUS', 'mad auntie').
card_uid('mad auntie'/'pSUS', 'pSUS:Mad Auntie:mad auntie').
card_rarity('mad auntie'/'pSUS', 'Special').
card_artist('mad auntie'/'pSUS', 'Wayne Reynolds').
card_number('mad auntie'/'pSUS', '18').
card_flavor_text('mad auntie'/'pSUS', 'One part cunning, one part wise, and many, many parts demented.').

card_in_set('royal assassin', 'pSUS').
card_original_type('royal assassin'/'pSUS', 'Creature — Human Assassin').
card_original_text('royal assassin'/'pSUS', '').
card_image_name('royal assassin'/'pSUS', 'royal assassin1').
card_uid('royal assassin'/'pSUS', 'pSUS:Royal Assassin:royal assassin1').
card_rarity('royal assassin'/'pSUS', 'Special').
card_artist('royal assassin'/'pSUS', 'Tom Wänerstrand').
card_number('royal assassin'/'pSUS', '11').
card_flavor_text('royal assassin'/'pSUS', 'Trained in the arts of stealth, royal assassins choose their victims carefully, relying on timing and precision rather than brute force.').

card_in_set('royal assassin', 'pSUS').
card_original_type('royal assassin'/'pSUS', 'Creature — Human Assassin').
card_original_text('royal assassin'/'pSUS', '').
card_image_name('royal assassin'/'pSUS', 'royal assassin2').
card_uid('royal assassin'/'pSUS', 'pSUS:Royal Assassin:royal assassin2').
card_rarity('royal assassin'/'pSUS', 'Special').
card_artist('royal assassin'/'pSUS', 'Tom Wänerstrand').
card_number('royal assassin'/'pSUS', '20').
card_flavor_text('royal assassin'/'pSUS', 'Trained in the arts of stealth, royal assassins choose their victims carefully, relying on timing and precision rather than brute force..').

card_in_set('sakura-tribe elder', 'pSUS').
card_original_type('sakura-tribe elder'/'pSUS', 'Creature — Snake Shaman').
card_original_text('sakura-tribe elder'/'pSUS', '').
card_first_print('sakura-tribe elder', 'pSUS').
card_image_name('sakura-tribe elder'/'pSUS', 'sakura-tribe elder1').
card_uid('sakura-tribe elder'/'pSUS', 'pSUS:Sakura-Tribe Elder:sakura-tribe elder1').
card_rarity('sakura-tribe elder'/'pSUS', 'Special').
card_artist('sakura-tribe elder'/'pSUS', 'Carl Critchlow').
card_number('sakura-tribe elder'/'pSUS', '12').
card_flavor_text('sakura-tribe elder'/'pSUS', 'There were no tombstones in orochi territory. Slain warriors were buried with a tree sapling, so they would become part of the forest after death.').

card_in_set('sakura-tribe elder', 'pSUS').
card_original_type('sakura-tribe elder'/'pSUS', 'Creature — Snake Shaman').
card_original_text('sakura-tribe elder'/'pSUS', '').
card_image_name('sakura-tribe elder'/'pSUS', 'sakura-tribe elder2').
card_uid('sakura-tribe elder'/'pSUS', 'pSUS:Sakura-Tribe Elder:sakura-tribe elder2').
card_rarity('sakura-tribe elder'/'pSUS', 'Special').
card_artist('sakura-tribe elder'/'pSUS', 'Carl Critchlow').
card_number('sakura-tribe elder'/'pSUS', '21').
card_flavor_text('sakura-tribe elder'/'pSUS', 'There were no tombstones in orochi territory. Slain warriors were buried with a tree sapling, so they would become part of the forest after death.').

card_in_set('sakura-tribe elder', 'pSUS').
card_original_type('sakura-tribe elder'/'pSUS', 'Creature — Snake Shaman').
card_original_text('sakura-tribe elder'/'pSUS', '').
card_image_name('sakura-tribe elder'/'pSUS', 'sakura-tribe elder3').
card_uid('sakura-tribe elder'/'pSUS', 'pSUS:Sakura-Tribe Elder:sakura-tribe elder3').
card_rarity('sakura-tribe elder'/'pSUS', 'Special').
card_artist('sakura-tribe elder'/'pSUS', 'Carl Critchlow').
card_number('sakura-tribe elder'/'pSUS', '27').
card_flavor_text('sakura-tribe elder'/'pSUS', 'There were no tombstones in orochi territory. Slain warriors were buried with a tree sapling, so they would become part of the forest after death.').

card_in_set('serra avatar', 'pSUS').
card_original_type('serra avatar'/'pSUS', 'Creature — Avatar').
card_original_text('serra avatar'/'pSUS', '').
card_image_name('serra avatar'/'pSUS', 'serra avatar').
card_uid('serra avatar'/'pSUS', 'pSUS:Serra Avatar:serra avatar').
card_rarity('serra avatar'/'pSUS', 'Special').
card_artist('serra avatar'/'pSUS', 'Dermot Power').
card_number('serra avatar'/'pSUS', '2').

card_in_set('shard phoenix', 'pSUS').
card_original_type('shard phoenix'/'pSUS', 'Creature — Phoenix').
card_original_text('shard phoenix'/'pSUS', '').
card_image_name('shard phoenix'/'pSUS', 'shard phoenix1').
card_uid('shard phoenix'/'pSUS', 'pSUS:Shard Phoenix:shard phoenix1').
card_rarity('shard phoenix'/'pSUS', 'Special').
card_artist('shard phoenix'/'pSUS', 'Ron Spencer').
card_number('shard phoenix'/'pSUS', '13').

card_in_set('shard phoenix', 'pSUS').
card_original_type('shard phoenix'/'pSUS', 'Creature — Phoenix').
card_original_text('shard phoenix'/'pSUS', '').
card_image_name('shard phoenix'/'pSUS', 'shard phoenix2').
card_uid('shard phoenix'/'pSUS', 'pSUS:Shard Phoenix:shard phoenix2').
card_rarity('shard phoenix'/'pSUS', 'Special').
card_artist('shard phoenix'/'pSUS', 'Ron Spencer').
card_number('shard phoenix'/'pSUS', '22').

card_in_set('shard phoenix', 'pSUS').
card_original_type('shard phoenix'/'pSUS', 'Creature — Phoenix').
card_original_text('shard phoenix'/'pSUS', '').
card_image_name('shard phoenix'/'pSUS', 'shard phoenix3').
card_uid('shard phoenix'/'pSUS', 'pSUS:Shard Phoenix:shard phoenix3').
card_rarity('shard phoenix'/'pSUS', 'Special').
card_artist('shard phoenix'/'pSUS', 'Ron Spencer').
card_number('shard phoenix'/'pSUS', '28').

card_in_set('slith firewalker', 'pSUS').
card_original_type('slith firewalker'/'pSUS', 'Creature — Slith').
card_original_text('slith firewalker'/'pSUS', '').
card_first_print('slith firewalker', 'pSUS').
card_image_name('slith firewalker'/'pSUS', 'slith firewalker1').
card_uid('slith firewalker'/'pSUS', 'pSUS:Slith Firewalker:slith firewalker1').
card_rarity('slith firewalker'/'pSUS', 'Special').
card_artist('slith firewalker'/'pSUS', 'Justin Sweet').
card_number('slith firewalker'/'pSUS', '10').
card_flavor_text('slith firewalker'/'pSUS', 'The Sligh incubate in the Great Furnace’s heat, emerging on Mirrodin’s surface only when the four suns have aligned overhead.').

card_in_set('slith firewalker', 'pSUS').
card_original_type('slith firewalker'/'pSUS', 'Creature — Slith').
card_original_text('slith firewalker'/'pSUS', '').
card_image_name('slith firewalker'/'pSUS', 'slith firewalker2').
card_uid('slith firewalker'/'pSUS', 'pSUS:Slith Firewalker:slith firewalker2').
card_rarity('slith firewalker'/'pSUS', 'Special').
card_artist('slith firewalker'/'pSUS', 'Justin Sweet').
card_number('slith firewalker'/'pSUS', '19').
card_flavor_text('slith firewalker'/'pSUS', 'The Sligh incubate in the Great Furnace\'s heat, emerging on Mirrodin\'s surface only when the four suns have aligned overhead.').

card_in_set('soltari priest', 'pSUS').
card_original_type('soltari priest'/'pSUS', 'Creature — Soltari Cleric').
card_original_text('soltari priest'/'pSUS', '').
card_image_name('soltari priest'/'pSUS', 'soltari priest1').
card_uid('soltari priest'/'pSUS', 'pSUS:Soltari Priest:soltari priest1').
card_rarity('soltari priest'/'pSUS', 'Special').
card_artist('soltari priest'/'pSUS', 'Dany Orizio').
card_number('soltari priest'/'pSUS', '14').
card_flavor_text('soltari priest'/'pSUS', '\"In Rath,\" the priest said, \"there is even greater need for prayer.\"').

card_in_set('soltari priest', 'pSUS').
card_original_type('soltari priest'/'pSUS', 'Creature — Soltari Cleric').
card_original_text('soltari priest'/'pSUS', '').
card_image_name('soltari priest'/'pSUS', 'soltari priest2').
card_uid('soltari priest'/'pSUS', 'pSUS:Soltari Priest:soltari priest2').
card_rarity('soltari priest'/'pSUS', 'Special').
card_artist('soltari priest'/'pSUS', 'Dany Orizio').
card_number('soltari priest'/'pSUS', '23').
card_flavor_text('soltari priest'/'pSUS', '\"In Rath,\" the priest said, \"there is even greater need for prayer.\"').

card_in_set('soltari priest', 'pSUS').
card_original_type('soltari priest'/'pSUS', 'Creature — Soltari Cleric').
card_original_text('soltari priest'/'pSUS', '').
card_image_name('soltari priest'/'pSUS', 'soltari priest3').
card_uid('soltari priest'/'pSUS', 'pSUS:Soltari Priest:soltari priest3').
card_rarity('soltari priest'/'pSUS', 'Special').
card_artist('soltari priest'/'pSUS', 'Dany Orizio').
card_number('soltari priest'/'pSUS', '29').
card_flavor_text('soltari priest'/'pSUS', '\"In Rath,\" the priest said, \"there is even greater need for prayer.\"').

card_in_set('thran quarry', 'pSUS').
card_original_type('thran quarry'/'pSUS', 'Land').
card_original_text('thran quarry'/'pSUS', '').
card_image_name('thran quarry'/'pSUS', 'thran quarry').
card_uid('thran quarry'/'pSUS', 'pSUS:Thran Quarry:thran quarry').
card_rarity('thran quarry'/'pSUS', 'Special').
card_artist('thran quarry'/'pSUS', 'Michael Sutfin').
card_number('thran quarry'/'pSUS', '1').

card_in_set('two-headed dragon', 'pSUS').
card_original_type('two-headed dragon'/'pSUS', 'Creature — Dragon').
card_original_text('two-headed dragon'/'pSUS', '').
card_image_name('two-headed dragon'/'pSUS', 'two-headed dragon').
card_uid('two-headed dragon'/'pSUS', 'pSUS:Two-Headed Dragon:two-headed dragon').
card_rarity('two-headed dragon'/'pSUS', 'Special').
card_artist('two-headed dragon'/'pSUS', 'Sam Wood').
card_number('two-headed dragon'/'pSUS', '9').

card_in_set('volcanic hammer', 'pSUS').
card_original_type('volcanic hammer'/'pSUS', 'Sorcery').
card_original_text('volcanic hammer'/'pSUS', '').
card_image_name('volcanic hammer'/'pSUS', 'volcanic hammer').
card_uid('volcanic hammer'/'pSUS', 'pSUS:Volcanic Hammer:volcanic hammer').
card_rarity('volcanic hammer'/'pSUS', 'Special').
card_artist('volcanic hammer'/'pSUS', 'Christopher Rush').
card_number('volcanic hammer'/'pSUS', '7').
card_flavor_text('volcanic hammer'/'pSUS', 'Cast the weight as though it were a die, to see a rival\'s fate.').

card_in_set('whirling dervish', 'pSUS').
card_original_type('whirling dervish'/'pSUS', 'Creature — Human Monk').
card_original_text('whirling dervish'/'pSUS', '').
card_image_name('whirling dervish'/'pSUS', 'whirling dervish1').
card_uid('whirling dervish'/'pSUS', 'pSUS:Whirling Dervish:whirling dervish1').
card_rarity('whirling dervish'/'pSUS', 'Special').
card_artist('whirling dervish'/'pSUS', 'Alex Horley-Orlandelli').
card_number('whirling dervish'/'pSUS', '15').
card_flavor_text('whirling dervish'/'pSUS', 'Amid this whirling hurricane of blades, there is no calm eye in which the hapless can find shelter.').

card_in_set('whirling dervish', 'pSUS').
card_original_type('whirling dervish'/'pSUS', 'Creature — Human Monk').
card_original_text('whirling dervish'/'pSUS', '').
card_image_name('whirling dervish'/'pSUS', 'whirling dervish2').
card_uid('whirling dervish'/'pSUS', 'pSUS:Whirling Dervish:whirling dervish2').
card_rarity('whirling dervish'/'pSUS', 'Special').
card_artist('whirling dervish'/'pSUS', 'Alex Horley-Orlandelli').
card_number('whirling dervish'/'pSUS', '24').
card_flavor_text('whirling dervish'/'pSUS', 'Amid this whirling hurricane of blades, there is no calm eye in which the hapless can find shelter.').

card_in_set('whirling dervish', 'pSUS').
card_original_type('whirling dervish'/'pSUS', 'Creature — Human Monk').
card_original_text('whirling dervish'/'pSUS', '').
card_image_name('whirling dervish'/'pSUS', 'whirling dervish3').
card_uid('whirling dervish'/'pSUS', 'pSUS:Whirling Dervish:whirling dervish3').
card_rarity('whirling dervish'/'pSUS', 'Special').
card_artist('whirling dervish'/'pSUS', 'Alex Horley-Orlandelli').
card_number('whirling dervish'/'pSUS', '30').
card_flavor_text('whirling dervish'/'pSUS', 'Amid this whirling hurricane of blades, there is no calm eye in which the hapless can find shelter.').
