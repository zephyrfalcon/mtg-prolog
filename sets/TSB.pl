% Time Spiral "Timeshifted"

set('TSB').
set_name('TSB', 'Time Spiral \"Timeshifted\"').
set_release_date('TSB', '2006-10-06').
set_border('TSB', 'black').
set_type('TSB', 'expansion').
set_block('TSB', 'Time Spiral').

card_in_set('akroma, angel of wrath', 'TSB').
card_original_type('akroma, angel of wrath'/'TSB', 'Legendary Creature — Angel').
card_original_text('akroma, angel of wrath'/'TSB', 'Flying, first strike, vigilance, trample, haste, protection from black, protection from red').
card_image_name('akroma, angel of wrath'/'TSB', 'akroma, angel of wrath').
card_uid('akroma, angel of wrath'/'TSB', 'TSB:Akroma, Angel of Wrath:akroma, angel of wrath').
card_rarity('akroma, angel of wrath'/'TSB', 'Special').
card_artist('akroma, angel of wrath'/'TSB', 'Ron Spears').
card_number('akroma, angel of wrath'/'TSB', '1').
card_flavor_text('akroma, angel of wrath'/'TSB', '\"No rest. No mercy. No matter what.\"').
card_multiverse_id('akroma, angel of wrath'/'TSB', '106645').
card_timeshifted('akroma, angel of wrath'/'TSB')
.
card_in_set('arena', 'TSB').
card_original_type('arena'/'TSB', 'Land').
card_original_text('arena'/'TSB', '{3}, {T}: Tap target creature you control and target creature of an opponent\'s choice he or she controls. Each of those creatures deals damage equal to its power to the other.').
card_image_name('arena'/'TSB', 'arena').
card_uid('arena'/'TSB', 'TSB:Arena:arena').
card_rarity('arena'/'TSB', 'Special').
card_artist('arena'/'TSB', 'Rob Alexander').
card_number('arena'/'TSB', '117').
card_multiverse_id('arena'/'TSB', '109739').
card_timeshifted('arena'/'TSB')
.
card_in_set('assault', 'TSB').
card_original_type('assault'/'TSB', 'Sorcery').
card_original_text('assault'/'TSB', 'Assault deals 2 damage to target creature or player.\n//\nBattery\n{3}{G}\nSorcery\nPut a 3/3 green Elephant creature token into play.').
card_image_name('assault'/'TSB', 'assaultbattery').
card_uid('assault'/'TSB', 'TSB:Assault:assaultbattery').
card_rarity('assault'/'TSB', 'Special').
card_artist('assault'/'TSB', 'Ben Thompson').
card_number('assault'/'TSB', '106a').
card_multiverse_id('assault'/'TSB', '109704').
card_timeshifted('assault'/'TSB')
.
card_in_set('auratog', 'TSB').
card_original_type('auratog'/'TSB', 'Creature — Atog').
card_original_text('auratog'/'TSB', 'Sacrifice an enchantment: Auratog gets +2/+2 until end of turn.').
card_image_name('auratog'/'TSB', 'auratog').
card_uid('auratog'/'TSB', 'TSB:Auratog:auratog').
card_rarity('auratog'/'TSB', 'Special').
card_artist('auratog'/'TSB', 'Jeff Miracola').
card_number('auratog'/'TSB', '2').
card_flavor_text('auratog'/'TSB', 'The auratog enjoys eating its wards.').
card_multiverse_id('auratog'/'TSB', '106665').
card_timeshifted('auratog'/'TSB')
.
card_in_set('avalanche riders', 'TSB').
card_original_type('avalanche riders'/'TSB', 'Creature — Human Nomad').
card_original_text('avalanche riders'/'TSB', 'Haste\nEcho {3}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Avalanche Riders comes into play, destroy target land.').
card_image_name('avalanche riders'/'TSB', 'avalanche riders').
card_uid('avalanche riders'/'TSB', 'TSB:Avalanche Riders:avalanche riders').
card_rarity('avalanche riders'/'TSB', 'Special').
card_artist('avalanche riders'/'TSB', 'Edward P. Beard, Jr.').
card_number('avalanche riders'/'TSB', '55').
card_multiverse_id('avalanche riders'/'TSB', '108835').
card_timeshifted('avalanche riders'/'TSB')
.
card_in_set('avatar of woe', 'TSB').
card_original_type('avatar of woe'/'TSB', 'Creature — Avatar').
card_original_text('avatar of woe'/'TSB', 'If there are ten or more creature cards total in all graveyards, Avatar of Woe costs {6} less to play.\nFear\n{T}: Destroy target creature. It can\'t be regenerated.').
card_image_name('avatar of woe'/'TSB', 'avatar of woe').
card_uid('avatar of woe'/'TSB', 'TSB:Avatar of Woe:avatar of woe').
card_rarity('avatar of woe'/'TSB', 'Special').
card_artist('avatar of woe'/'TSB', 'rk post').
card_number('avatar of woe'/'TSB', '37').
card_multiverse_id('avatar of woe'/'TSB', '108929').
card_timeshifted('avatar of woe'/'TSB')
.
card_in_set('avoid fate', 'TSB').
card_original_type('avoid fate'/'TSB', 'Instant').
card_original_text('avoid fate'/'TSB', 'Counter target instant or Aura spell that targets a permanent you control.').
card_image_name('avoid fate'/'TSB', 'avoid fate').
card_uid('avoid fate'/'TSB', 'TSB:Avoid Fate:avoid fate').
card_rarity('avoid fate'/'TSB', 'Special').
card_artist('avoid fate'/'TSB', 'Phil Foglio').
card_number('avoid fate'/'TSB', '73').
card_multiverse_id('avoid fate'/'TSB', '108886').
card_timeshifted('avoid fate'/'TSB')
.
card_in_set('bad moon', 'TSB').
card_original_type('bad moon'/'TSB', 'Enchantment').
card_original_text('bad moon'/'TSB', 'Black creatures get +1/+1.').
card_image_name('bad moon'/'TSB', 'bad moon').
card_uid('bad moon'/'TSB', 'TSB:Bad Moon:bad moon').
card_rarity('bad moon'/'TSB', 'Special').
card_artist('bad moon'/'TSB', 'Jesper Myrfors').
card_number('bad moon'/'TSB', '38').
card_multiverse_id('bad moon'/'TSB', '108818').
card_timeshifted('bad moon'/'TSB')
.
card_in_set('battery', 'TSB').
card_original_type('battery'/'TSB', 'Sorcery').
card_original_text('battery'/'TSB', 'Assault deals 2 damage to target creature or player.\n//\nBattery\n{3}{G}\nSorcery\nPut a 3/3 green Elephant creature token into play.').
card_image_name('battery'/'TSB', 'assaultbattery').
card_uid('battery'/'TSB', 'TSB:Battery:assaultbattery').
card_rarity('battery'/'TSB', 'Special').
card_artist('battery'/'TSB', 'Ben Thompson').
card_number('battery'/'TSB', '106b').
card_multiverse_id('battery'/'TSB', '109704').
card_timeshifted('battery'/'TSB')
.
card_in_set('browbeat', 'TSB').
card_original_type('browbeat'/'TSB', 'Sorcery').
card_original_text('browbeat'/'TSB', 'Any player may have Browbeat deal 5 damage to him or her. If no one does, target player draws three cards.').
card_image_name('browbeat'/'TSB', 'browbeat').
card_uid('browbeat'/'TSB', 'TSB:Browbeat:browbeat').
card_rarity('browbeat'/'TSB', 'Special').
card_artist('browbeat'/'TSB', 'Mark Tedin').
card_number('browbeat'/'TSB', '56').
card_flavor_text('browbeat'/'TSB', '\"Even the threat of power has power.\"\n—Jeska, warrior adept').
card_multiverse_id('browbeat'/'TSB', '107299').
card_timeshifted('browbeat'/'TSB')
.
card_in_set('call of the herd', 'TSB').
card_original_type('call of the herd'/'TSB', 'Sorcery').
card_original_text('call of the herd'/'TSB', 'Put a 3/3 green Elephant creature token into play.\nFlashback {3}{G} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_image_name('call of the herd'/'TSB', 'call of the herd').
card_uid('call of the herd'/'TSB', 'TSB:Call of the Herd:call of the herd').
card_rarity('call of the herd'/'TSB', 'Special').
card_artist('call of the herd'/'TSB', 'Carl Critchlow').
card_number('call of the herd'/'TSB', '74').
card_multiverse_id('call of the herd'/'TSB', '109672').
card_timeshifted('call of the herd'/'TSB')
.
card_in_set('celestial dawn', 'TSB').
card_original_type('celestial dawn'/'TSB', 'Enchantment').
card_original_text('celestial dawn'/'TSB', 'Lands you control are Plains.\nNonland cards you own that aren\'t in play, spells you control, and nonland permanents you control are white.\nYou may spend white mana as though it were mana of any color. You may spend other mana only as though it were colorless mana.').
card_image_name('celestial dawn'/'TSB', 'celestial dawn').
card_uid('celestial dawn'/'TSB', 'TSB:Celestial Dawn:celestial dawn').
card_rarity('celestial dawn'/'TSB', 'Special').
card_artist('celestial dawn'/'TSB', 'Liz Danforth').
card_number('celestial dawn'/'TSB', '3').
card_multiverse_id('celestial dawn'/'TSB', '106639').
card_timeshifted('celestial dawn'/'TSB')
.
card_in_set('claws of gix', 'TSB').
card_original_type('claws of gix'/'TSB', 'Artifact').
card_original_text('claws of gix'/'TSB', '{1}, Sacrifice a permanent: You gain 1 life.').
card_image_name('claws of gix'/'TSB', 'claws of gix').
card_uid('claws of gix'/'TSB', 'TSB:Claws of Gix:claws of gix').
card_rarity('claws of gix'/'TSB', 'Special').
card_artist('claws of gix'/'TSB', 'Henry G. Higgenbotham').
card_number('claws of gix'/'TSB', '107').
card_flavor_text('claws of gix'/'TSB', 'When the Brotherhood of Gix dug out the cave of Koilos they found their master\'s severed hand. They enshrined it, hoping that one day it would point the way to Phyrexia.').
card_multiverse_id('claws of gix'/'TSB', '109690').
card_timeshifted('claws of gix'/'TSB')
.
card_in_set('coalition victory', 'TSB').
card_original_type('coalition victory'/'TSB', 'Sorcery').
card_original_text('coalition victory'/'TSB', 'You win the game if you control a land of each basic land type and a creature of each color.').
card_image_name('coalition victory'/'TSB', 'coalition victory').
card_uid('coalition victory'/'TSB', 'TSB:Coalition Victory:coalition victory').
card_rarity('coalition victory'/'TSB', 'Special').
card_artist('coalition victory'/'TSB', 'Eric Peterson').
card_number('coalition victory'/'TSB', '91').
card_flavor_text('coalition victory'/'TSB', '\"You can build a perfect machine out of imperfect parts.\"\n—Urza').
card_multiverse_id('coalition victory'/'TSB', '109718').
card_timeshifted('coalition victory'/'TSB')
.
card_in_set('cockatrice', 'TSB').
card_original_type('cockatrice'/'TSB', 'Creature — Cockatrice').
card_original_text('cockatrice'/'TSB', 'Flying\nWhenever Cockatrice blocks or becomes blocked by a non-Wall creature, destroy that creature at end of combat.').
card_image_name('cockatrice'/'TSB', 'cockatrice').
card_uid('cockatrice'/'TSB', 'TSB:Cockatrice:cockatrice').
card_rarity('cockatrice'/'TSB', 'Special').
card_artist('cockatrice'/'TSB', 'Dan Frazier').
card_number('cockatrice'/'TSB', '75').
card_multiverse_id('cockatrice'/'TSB', '108912').
card_timeshifted('cockatrice'/'TSB')
.
card_in_set('consecrate land', 'TSB').
card_original_type('consecrate land'/'TSB', 'Enchantment — Aura').
card_original_text('consecrate land'/'TSB', 'Enchant land\nEnchanted land is indestructible and can\'t be enchanted by other Auras.').
card_image_name('consecrate land'/'TSB', 'consecrate land').
card_uid('consecrate land'/'TSB', 'TSB:Consecrate Land:consecrate land').
card_rarity('consecrate land'/'TSB', 'Special').
card_artist('consecrate land'/'TSB', 'Jeff A. Menges').
card_number('consecrate land'/'TSB', '4').
card_multiverse_id('consecrate land'/'TSB', '108801').
card_timeshifted('consecrate land'/'TSB')
.
card_in_set('conspiracy', 'TSB').
card_original_type('conspiracy'/'TSB', 'Enchantment').
card_original_text('conspiracy'/'TSB', 'As Conspiracy comes into play, choose a creature type.\nCreature cards you own that aren\'t in play, creature spells you control, and creatures you control are the chosen type.').
card_image_name('conspiracy'/'TSB', 'conspiracy').
card_uid('conspiracy'/'TSB', 'TSB:Conspiracy:conspiracy').
card_rarity('conspiracy'/'TSB', 'Special').
card_artist('conspiracy'/'TSB', 'Jeff Easley').
card_number('conspiracy'/'TSB', '39').
card_multiverse_id('conspiracy'/'TSB', '108914').
card_timeshifted('conspiracy'/'TSB')
.
card_in_set('craw giant', 'TSB').
card_original_type('craw giant'/'TSB', 'Creature — Giant').
card_original_text('craw giant'/'TSB', 'Trample\nRampage 2 (Whenever this creature becomes blocked, it gets +2/+2 until end of turn for each creature blocking it beyond the first.)').
card_image_name('craw giant'/'TSB', 'craw giant').
card_uid('craw giant'/'TSB', 'TSB:Craw Giant:craw giant').
card_rarity('craw giant'/'TSB', 'Special').
card_artist('craw giant'/'TSB', 'Christopher Rush').
card_number('craw giant'/'TSB', '76').
card_flavor_text('craw giant'/'TSB', 'Harthag gave a jolly laugh as he surveyed the army before him. \"Ho ho ho! Midgets! You think you can stand in my way?\"').
card_multiverse_id('craw giant'/'TSB', '109748').
card_timeshifted('craw giant'/'TSB')
.
card_in_set('dandân', 'TSB').
card_original_type('dandân'/'TSB', 'Creature — Fish').
card_original_text('dandân'/'TSB', 'Dandân can\'t attack unless defending player controls an Island.\nWhen you control no Islands, sacrifice Dandân.').
card_image_name('dandân'/'TSB', 'dandan').
card_uid('dandân'/'TSB', 'TSB:Dandân:dandan').
card_rarity('dandân'/'TSB', 'Special').
card_artist('dandân'/'TSB', 'Drew Tucker').
card_number('dandân'/'TSB', '19').
card_flavor_text('dandân'/'TSB', '\"Catch good today. Start replacing crew tomorrow.\"\n—Faysal al-Mousa, fisher captain, log').
card_multiverse_id('dandân'/'TSB', '106631').
card_timeshifted('dandân'/'TSB')
.
card_in_set('darkness', 'TSB').
card_original_type('darkness'/'TSB', 'Instant').
card_original_text('darkness'/'TSB', 'Prevent all combat damage that would be dealt this turn.').
card_image_name('darkness'/'TSB', 'darkness').
card_uid('darkness'/'TSB', 'TSB:Darkness:darkness').
card_rarity('darkness'/'TSB', 'Special').
card_artist('darkness'/'TSB', 'Harold McNeill').
card_number('darkness'/'TSB', '40').
card_flavor_text('darkness'/'TSB', '\"If I must die, I will encounter darkness as a bride, / And hug it in mine arms.\"\n—William Shakespeare,\nMeasure for Measure').
card_multiverse_id('darkness'/'TSB', '108919').
card_timeshifted('darkness'/'TSB')
.
card_in_set('dauthi slayer', 'TSB').
card_original_type('dauthi slayer'/'TSB', 'Creature — Dauthi Soldier').
card_original_text('dauthi slayer'/'TSB', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nDauthi Slayer attacks each turn if able.').
card_image_name('dauthi slayer'/'TSB', 'dauthi slayer').
card_uid('dauthi slayer'/'TSB', 'TSB:Dauthi Slayer:dauthi slayer').
card_rarity('dauthi slayer'/'TSB', 'Special').
card_artist('dauthi slayer'/'TSB', 'Dermot Power').
card_number('dauthi slayer'/'TSB', '41').
card_flavor_text('dauthi slayer'/'TSB', '\"They have knives for every soul.\"\n—Lyna, Soltari emissary').
card_multiverse_id('dauthi slayer'/'TSB', '107288').
card_timeshifted('dauthi slayer'/'TSB')
.
card_in_set('defiant vanguard', 'TSB').
card_original_type('defiant vanguard'/'TSB', 'Creature — Human Rebel').
card_original_text('defiant vanguard'/'TSB', 'When Defiant Vanguard blocks, at end of combat, destroy it and all creatures it blocked this turn.\n{5}, {T}: Search your library for a Rebel card with converted mana cost 4 or less and put it into play. Then shuffle your library.').
card_image_name('defiant vanguard'/'TSB', 'defiant vanguard').
card_uid('defiant vanguard'/'TSB', 'TSB:Defiant Vanguard:defiant vanguard').
card_rarity('defiant vanguard'/'TSB', 'Special').
card_artist('defiant vanguard'/'TSB', 'Pete Venters').
card_number('defiant vanguard'/'TSB', '5').
card_multiverse_id('defiant vanguard'/'TSB', '108803').
card_timeshifted('defiant vanguard'/'TSB')
.
card_in_set('desert', 'TSB').
card_original_type('desert'/'TSB', 'Land — Desert').
card_original_text('desert'/'TSB', '{T}: Add {1} to your mana pool.\n{T}: Desert deals 1 damage to target attacking creature. Play this ability only during the end of combat step.').
card_image_name('desert'/'TSB', 'desert').
card_uid('desert'/'TSB', 'TSB:Desert:desert').
card_rarity('desert'/'TSB', 'Special').
card_artist('desert'/'TSB', 'Jesper Myrfors').
card_number('desert'/'TSB', '118').
card_multiverse_id('desert'/'TSB', '109713').
card_timeshifted('desert'/'TSB')
.
card_in_set('desolation giant', 'TSB').
card_original_type('desolation giant'/'TSB', 'Creature — Giant').
card_original_text('desolation giant'/'TSB', 'Kicker {W}{W} (You may pay an additional {W}{W} as you play this spell.)\nWhen Desolation Giant comes into play, destroy all other creatures you control. If the kicker cost was paid, destroy all other creatures instead.').
card_image_name('desolation giant'/'TSB', 'desolation giant').
card_uid('desolation giant'/'TSB', 'TSB:Desolation Giant:desolation giant').
card_rarity('desolation giant'/'TSB', 'Special').
card_artist('desolation giant'/'TSB', 'Alan Pollack').
card_number('desolation giant'/'TSB', '57').
card_multiverse_id('desolation giant'/'TSB', '108917').
card_timeshifted('desolation giant'/'TSB')
.
card_in_set('disenchant', 'TSB').
card_original_type('disenchant'/'TSB', 'Instant').
card_original_text('disenchant'/'TSB', 'Destroy target artifact or enchantment.').
card_image_name('disenchant'/'TSB', 'disenchant').
card_uid('disenchant'/'TSB', 'TSB:Disenchant:disenchant').
card_rarity('disenchant'/'TSB', 'Special').
card_artist('disenchant'/'TSB', 'Amy Weber').
card_number('disenchant'/'TSB', '6').
card_multiverse_id('disenchant'/'TSB', '107302').
card_timeshifted('disenchant'/'TSB')
.
card_in_set('disintegrate', 'TSB').
card_original_type('disintegrate'/'TSB', 'Sorcery').
card_original_text('disintegrate'/'TSB', 'Disintegrate deals X damage to target creature or player. That creature can\'t be regenerated this turn. If the creature would be put into a graveyard this turn, remove it from the game instead.').
card_image_name('disintegrate'/'TSB', 'disintegrate').
card_uid('disintegrate'/'TSB', 'TSB:Disintegrate:disintegrate').
card_rarity('disintegrate'/'TSB', 'Special').
card_artist('disintegrate'/'TSB', 'Anson Maddocks').
card_number('disintegrate'/'TSB', '58').
card_multiverse_id('disintegrate'/'TSB', '106636').
card_timeshifted('disintegrate'/'TSB')
.
card_in_set('dodecapod', 'TSB').
card_original_type('dodecapod'/'TSB', 'Artifact Creature — Golem').
card_original_text('dodecapod'/'TSB', 'If a spell or ability an opponent controls causes you to discard Dodecapod, put it into play with two +1/+1 counters on it instead of putting it into your graveyard.').
card_image_name('dodecapod'/'TSB', 'dodecapod').
card_uid('dodecapod'/'TSB', 'TSB:Dodecapod:dodecapod').
card_rarity('dodecapod'/'TSB', 'Special').
card_artist('dodecapod'/'TSB', 'John Howe').
card_number('dodecapod'/'TSB', '108').
card_multiverse_id('dodecapod'/'TSB', '109736').
card_timeshifted('dodecapod'/'TSB')
.
card_in_set('dragon whelp', 'TSB').
card_original_type('dragon whelp'/'TSB', 'Creature — Dragon').
card_original_text('dragon whelp'/'TSB', 'Flying\n{R}: Dragon Whelp gets +1/+0 until end of turn. At end of turn, if this ability has been played four or more times this turn, sacrifice Dragon Whelp.').
card_image_name('dragon whelp'/'TSB', 'dragon whelp').
card_uid('dragon whelp'/'TSB', 'TSB:Dragon Whelp:dragon whelp').
card_rarity('dragon whelp'/'TSB', 'Special').
card_artist('dragon whelp'/'TSB', 'Amy Weber').
card_number('dragon whelp'/'TSB', '59').
card_multiverse_id('dragon whelp'/'TSB', '108810').
card_timeshifted('dragon whelp'/'TSB')
.
card_in_set('dragonstorm', 'TSB').
card_original_type('dragonstorm'/'TSB', 'Sorcery').
card_original_text('dragonstorm'/'TSB', 'Search your library for a Dragon card and put it into play. Then shuffle your library.\nStorm (When you play this spell, copy it for each spell played before it this turn.)').
card_image_name('dragonstorm'/'TSB', 'dragonstorm').
card_uid('dragonstorm'/'TSB', 'TSB:Dragonstorm:dragonstorm').
card_rarity('dragonstorm'/'TSB', 'Special').
card_artist('dragonstorm'/'TSB', 'Kev Walker').
card_number('dragonstorm'/'TSB', '60').
card_multiverse_id('dragonstorm'/'TSB', '108843').
card_timeshifted('dragonstorm'/'TSB')
.
card_in_set('enduring renewal', 'TSB').
card_original_type('enduring renewal'/'TSB', 'Enchantment').
card_original_text('enduring renewal'/'TSB', 'Play with your hand revealed.\nIf you would draw a card, reveal the top card of your library instead. If it\'s a creature card, put it into your graveyard. Otherwise, draw a card.\nWhenever a creature is put into your graveyard from play, return it to your hand.').
card_image_name('enduring renewal'/'TSB', 'enduring renewal').
card_uid('enduring renewal'/'TSB', 'TSB:Enduring Renewal:enduring renewal').
card_rarity('enduring renewal'/'TSB', 'Special').
card_artist('enduring renewal'/'TSB', 'Harold McNeill').
card_number('enduring renewal'/'TSB', '7').
card_multiverse_id('enduring renewal'/'TSB', '108842').
card_timeshifted('enduring renewal'/'TSB')
.
card_in_set('eron the relentless', 'TSB').
card_original_type('eron the relentless'/'TSB', 'Legendary Creature — Human Rogue').
card_original_text('eron the relentless'/'TSB', 'Haste\n{R}{R}{R}: Regenerate Eron the Relentless.').
card_image_name('eron the relentless'/'TSB', 'eron the relentless').
card_uid('eron the relentless'/'TSB', 'TSB:Eron the Relentless:eron the relentless').
card_rarity('eron the relentless'/'TSB', 'Special').
card_artist('eron the relentless'/'TSB', 'Christopher Rush').
card_number('eron the relentless'/'TSB', '61').
card_flavor_text('eron the relentless'/'TSB', '\"Eron would be much less of a hassle if only he were mortal.\"\n—Reyhan, Samite alchemist').
card_multiverse_id('eron the relentless'/'TSB', '108922').
card_timeshifted('eron the relentless'/'TSB')
.
card_in_set('essence sliver', 'TSB').
card_original_type('essence sliver'/'TSB', 'Creature — Sliver').
card_original_text('essence sliver'/'TSB', 'Whenever a Sliver deals damage, its controller gains that much life.').
card_image_name('essence sliver'/'TSB', 'essence sliver').
card_uid('essence sliver'/'TSB', 'TSB:Essence Sliver:essence sliver').
card_rarity('essence sliver'/'TSB', 'Special').
card_artist('essence sliver'/'TSB', 'Glen Angus').
card_number('essence sliver'/'TSB', '8').
card_flavor_text('essence sliver'/'TSB', 'The slivers would survive, even at the expense of every other creature on Otaria.').
card_multiverse_id('essence sliver'/'TSB', '106647').
card_timeshifted('essence sliver'/'TSB')
.
card_in_set('evil eye of orms-by-gore', 'TSB').
card_original_type('evil eye of orms-by-gore'/'TSB', 'Creature — Eye').
card_original_text('evil eye of orms-by-gore'/'TSB', 'Non-Eye creatures you control can\'t attack.\nEvil Eye of Orms-by-Gore can\'t be blocked except by Walls.').
card_image_name('evil eye of orms-by-gore'/'TSB', 'evil eye of orms-by-gore').
card_uid('evil eye of orms-by-gore'/'TSB', 'TSB:Evil Eye of Orms-by-Gore:evil eye of orms-by-gore').
card_rarity('evil eye of orms-by-gore'/'TSB', 'Special').
card_artist('evil eye of orms-by-gore'/'TSB', 'Jesper Myrfors').
card_number('evil eye of orms-by-gore'/'TSB', '42').
card_flavor_text('evil eye of orms-by-gore'/'TSB', 'The highway of fear is the shortest route to defeat.').
card_multiverse_id('evil eye of orms-by-gore'/'TSB', '107278').
card_timeshifted('evil eye of orms-by-gore'/'TSB')
.
card_in_set('faceless butcher', 'TSB').
card_original_type('faceless butcher'/'TSB', 'Creature — Nightmare Horror').
card_original_text('faceless butcher'/'TSB', 'When Faceless Butcher comes into play, remove target creature other than Faceless Butcher from the game.\nWhen Faceless Butcher leaves play, return the removed card to play under its owner\'s control.').
card_image_name('faceless butcher'/'TSB', 'faceless butcher').
card_uid('faceless butcher'/'TSB', 'TSB:Faceless Butcher:faceless butcher').
card_rarity('faceless butcher'/'TSB', 'Special').
card_artist('faceless butcher'/'TSB', 'Daren Bader').
card_number('faceless butcher'/'TSB', '43').
card_multiverse_id('faceless butcher'/'TSB', '108840').
card_timeshifted('faceless butcher'/'TSB')
.
card_in_set('feldon\'s cane', 'TSB').
card_original_type('feldon\'s cane'/'TSB', 'Artifact').
card_original_text('feldon\'s cane'/'TSB', '{T}, Remove Feldon\'s Cane from the game: Shuffle your graveyard into your library.').
card_image_name('feldon\'s cane'/'TSB', 'feldon\'s cane').
card_uid('feldon\'s cane'/'TSB', 'TSB:Feldon\'s Cane:feldon\'s cane').
card_rarity('feldon\'s cane'/'TSB', 'Special').
card_artist('feldon\'s cane'/'TSB', 'Mark Tedin').
card_number('feldon\'s cane'/'TSB', '109').
card_flavor_text('feldon\'s cane'/'TSB', 'Feldon found the first of these canes frozen in the Ronom Glacier.').
card_multiverse_id('feldon\'s cane'/'TSB', '109694').
card_timeshifted('feldon\'s cane'/'TSB')
.
card_in_set('fiery justice', 'TSB').
card_original_type('fiery justice'/'TSB', 'Sorcery').
card_original_text('fiery justice'/'TSB', 'Fiery Justice deals 5 damage divided as you choose among any number of target creatures and/or players. Target opponent gains 5 life.').
card_image_name('fiery justice'/'TSB', 'fiery justice').
card_uid('fiery justice'/'TSB', 'TSB:Fiery Justice:fiery justice').
card_rarity('fiery justice'/'TSB', 'Special').
card_artist('fiery justice'/'TSB', 'Melissa A. Benson').
card_number('fiery justice'/'TSB', '92').
card_flavor_text('fiery justice'/'TSB', '\"The fire of justice burns like nothing else.\"\n—Lovisa Coldeyes, Balduvian chieftain').
card_multiverse_id('fiery justice'/'TSB', '108893').
card_timeshifted('fiery justice'/'TSB')
.
card_in_set('fiery temper', 'TSB').
card_original_type('fiery temper'/'TSB', 'Instant').
card_original_text('fiery temper'/'TSB', 'Fiery Temper deals 3 damage to target creature or player.\nMadness {R} (If you discard this card, you may play it for its madness cost instead of putting it into your graveyard.)').
card_image_name('fiery temper'/'TSB', 'fiery temper').
card_uid('fiery temper'/'TSB', 'TSB:Fiery Temper:fiery temper').
card_rarity('fiery temper'/'TSB', 'Special').
card_artist('fiery temper'/'TSB', 'Greg & Tim Hildebrandt').
card_number('fiery temper'/'TSB', '62').
card_multiverse_id('fiery temper'/'TSB', '108880').
card_timeshifted('fiery temper'/'TSB')
.
card_in_set('fire whip', 'TSB').
card_original_type('fire whip'/'TSB', 'Enchantment — Aura').
card_original_text('fire whip'/'TSB', 'Enchant creature you control\nEnchanted creature has \"{T}: This creature deals 1 damage to target creature or player.\"\nSacrifice Fire Whip: Fire Whip deals 1 damage to target creature or player.').
card_image_name('fire whip'/'TSB', 'fire whip').
card_uid('fire whip'/'TSB', 'TSB:Fire Whip:fire whip').
card_rarity('fire whip'/'TSB', 'Special').
card_artist('fire whip'/'TSB', 'Jeff Miracola').
card_number('fire whip'/'TSB', '63').
card_multiverse_id('fire whip'/'TSB', '108877').
card_timeshifted('fire whip'/'TSB')
.
card_in_set('flying men', 'TSB').
card_original_type('flying men'/'TSB', 'Creature — Human').
card_original_text('flying men'/'TSB', 'Flying').
card_image_name('flying men'/'TSB', 'flying men').
card_uid('flying men'/'TSB', 'TSB:Flying Men:flying men').
card_rarity('flying men'/'TSB', 'Special').
card_artist('flying men'/'TSB', 'Christopher Rush').
card_number('flying men'/'TSB', '20').
card_flavor_text('flying men'/'TSB', 'Saffiyah clapped her hands and twenty flying men appeared at her side, each well trained in the art of combat.').
card_multiverse_id('flying men'/'TSB', '107291').
card_timeshifted('flying men'/'TSB')
.
card_in_set('funeral charm', 'TSB').
card_original_type('funeral charm'/'TSB', 'Instant').
card_original_text('funeral charm'/'TSB', 'Choose one Target player discards a card; or target creature gets +2/-1 until end of turn; or target creature gains swampwalk until end of turn.').
card_image_name('funeral charm'/'TSB', 'funeral charm').
card_uid('funeral charm'/'TSB', 'TSB:Funeral Charm:funeral charm').
card_rarity('funeral charm'/'TSB', 'Special').
card_artist('funeral charm'/'TSB', 'Greg Spalenka').
card_number('funeral charm'/'TSB', '44').
card_multiverse_id('funeral charm'/'TSB', '108895').
card_timeshifted('funeral charm'/'TSB')
.
card_in_set('gaea\'s blessing', 'TSB').
card_original_type('gaea\'s blessing'/'TSB', 'Sorcery').
card_original_text('gaea\'s blessing'/'TSB', 'Target player shuffles up to three target cards from his or her graveyard into his or her library.\nDraw a card.\nWhen Gaea\'s Blessing is put into your graveyard from your library, shuffle your graveyard into your library.').
card_image_name('gaea\'s blessing'/'TSB', 'gaea\'s blessing').
card_uid('gaea\'s blessing'/'TSB', 'TSB:Gaea\'s Blessing:gaea\'s blessing').
card_rarity('gaea\'s blessing'/'TSB', 'Special').
card_artist('gaea\'s blessing'/'TSB', 'Rebecca Guay').
card_number('gaea\'s blessing'/'TSB', '77').
card_multiverse_id('gaea\'s blessing'/'TSB', '108789').
card_timeshifted('gaea\'s blessing'/'TSB')
.
card_in_set('gaea\'s liege', 'TSB').
card_original_type('gaea\'s liege'/'TSB', 'Creature — Avatar').
card_original_text('gaea\'s liege'/'TSB', 'Gaea\'s Liege\'s power and toughness are each equal to the number of Forests you control. As long as Gaea\'s Liege is attacking, its power and toughness are each equal to the number of Forests defending player controls instead.\n{T}: Target land becomes a Forest until Gaea\'s Liege leaves play.').
card_image_name('gaea\'s liege'/'TSB', 'gaea\'s liege').
card_uid('gaea\'s liege'/'TSB', 'TSB:Gaea\'s Liege:gaea\'s liege').
card_rarity('gaea\'s liege'/'TSB', 'Special').
card_artist('gaea\'s liege'/'TSB', 'Dameon Willich').
card_number('gaea\'s liege'/'TSB', '78').
card_multiverse_id('gaea\'s liege'/'TSB', '109744').
card_timeshifted('gaea\'s liege'/'TSB')
.
card_in_set('gemstone mine', 'TSB').
card_original_type('gemstone mine'/'TSB', 'Land').
card_original_text('gemstone mine'/'TSB', 'Gemstone Mine comes into play with three mining counters on it.\n{T}, Remove a mining counter from Gemstone Mine: Add one mana of any color to your mana pool. If there are no mining counters on Gemstone Mine, sacrifice it.').
card_image_name('gemstone mine'/'TSB', 'gemstone mine').
card_uid('gemstone mine'/'TSB', 'TSB:Gemstone Mine:gemstone mine').
card_rarity('gemstone mine'/'TSB', 'Special').
card_artist('gemstone mine'/'TSB', 'Brom').
card_number('gemstone mine'/'TSB', '119').
card_multiverse_id('gemstone mine'/'TSB', '109761').
card_timeshifted('gemstone mine'/'TSB')
.
card_in_set('ghost ship', 'TSB').
card_original_type('ghost ship'/'TSB', 'Creature — Spirit').
card_original_text('ghost ship'/'TSB', 'Flying\n{U}{U}{U}: Regenerate Ghost Ship.').
card_image_name('ghost ship'/'TSB', 'ghost ship').
card_uid('ghost ship'/'TSB', 'TSB:Ghost Ship:ghost ship').
card_rarity('ghost ship'/'TSB', 'Special').
card_artist('ghost ship'/'TSB', 'Tom Wänerstrand').
card_number('ghost ship'/'TSB', '21').
card_flavor_text('ghost ship'/'TSB', '\"That phantom prow split the storm as lightning cast its long shadow on the battlefield below.\"\n—Mireille Gaetane, The Valeriad').
card_multiverse_id('ghost ship'/'TSB', '107294').
card_timeshifted('ghost ship'/'TSB')
.
card_in_set('giant oyster', 'TSB').
card_original_type('giant oyster'/'TSB', 'Creature — Oyster').
card_original_text('giant oyster'/'TSB', 'You may choose not to untap Giant Oyster during your untap step.\n{T}: As long as Giant Oyster remains tapped, target tapped creature doesn\'t untap during its controller\'s untap step, and at the beginning of each of your draw steps, put a -1/-1 counter on that creature. When Giant Oyster leaves play or becomes untapped, remove all -1/-1 counters from the creature.').
card_image_name('giant oyster'/'TSB', 'giant oyster').
card_uid('giant oyster'/'TSB', 'TSB:Giant Oyster:giant oyster').
card_rarity('giant oyster'/'TSB', 'Special').
card_artist('giant oyster'/'TSB', 'Nicola Leonard').
card_number('giant oyster'/'TSB', '22').
card_multiverse_id('giant oyster'/'TSB', '108816').
card_timeshifted('giant oyster'/'TSB')
.
card_in_set('goblin snowman', 'TSB').
card_original_type('goblin snowman'/'TSB', 'Creature — Goblin').
card_original_text('goblin snowman'/'TSB', 'Whenever Goblin Snowman blocks, prevent all combat damage that would be dealt to and dealt by it this turn.\n{T}: Goblin Snowman deals 1 damage to target creature it\'s blocking.').
card_image_name('goblin snowman'/'TSB', 'goblin snowman').
card_uid('goblin snowman'/'TSB', 'TSB:Goblin Snowman:goblin snowman').
card_rarity('goblin snowman'/'TSB', 'Special').
card_artist('goblin snowman'/'TSB', 'Daniel Gelon').
card_number('goblin snowman'/'TSB', '64').
card_flavor_text('goblin snowman'/'TSB', '\"Strength in numbers? Right.\"\n—Ib Halfheart, goblin tactician').
card_multiverse_id('goblin snowman'/'TSB', '107280').
card_timeshifted('goblin snowman'/'TSB')
.
card_in_set('grinning totem', 'TSB').
card_original_type('grinning totem'/'TSB', 'Artifact').
card_original_text('grinning totem'/'TSB', '{2}, {T}, Sacrifice Grinning Totem: Search target opponent\'s library for a card and remove it from the game. Then that player shuffles his or her library. Until the beginning of your next upkeep, you may play that card. At the beginning of your next upkeep, if you haven\'t played it, put it into its owner\'s graveyard.').
card_image_name('grinning totem'/'TSB', 'grinning totem').
card_uid('grinning totem'/'TSB', 'TSB:Grinning Totem:grinning totem').
card_rarity('grinning totem'/'TSB', 'Special').
card_artist('grinning totem'/'TSB', 'Donato Giancola').
card_number('grinning totem'/'TSB', '110').
card_multiverse_id('grinning totem'/'TSB', '109708').
card_timeshifted('grinning totem'/'TSB')
.
card_in_set('hail storm', 'TSB').
card_original_type('hail storm'/'TSB', 'Instant').
card_original_text('hail storm'/'TSB', 'Hail Storm deals 2 damage to each attacking creature and 1 damage to you and each creature you control.').
card_image_name('hail storm'/'TSB', 'hail storm').
card_uid('hail storm'/'TSB', 'TSB:Hail Storm:hail storm').
card_rarity('hail storm'/'TSB', 'Special').
card_artist('hail storm'/'TSB', 'Jeff A. Menges').
card_number('hail storm'/'TSB', '79').
card_flavor_text('hail storm'/'TSB', '\"If they can\'t take the hail, they\'d better stay home.\"\n—Arna Kennerüd, skycaptain').
card_multiverse_id('hail storm'/'TSB', '107296').
card_timeshifted('hail storm'/'TSB')
.
card_in_set('honorable passage', 'TSB').
card_original_type('honorable passage'/'TSB', 'Instant').
card_original_text('honorable passage'/'TSB', 'The next time a source of your choice would deal damage to target creature or player this turn, prevent that damage. If damage from a red source is prevented this way, Honorable Passage deals damage equal to the damage prevented this way to the source\'s controller.').
card_image_name('honorable passage'/'TSB', 'honorable passage').
card_uid('honorable passage'/'TSB', 'TSB:Honorable Passage:honorable passage').
card_rarity('honorable passage'/'TSB', 'Special').
card_artist('honorable passage'/'TSB', 'Jeff Miracola').
card_number('honorable passage'/'TSB', '9').
card_multiverse_id('honorable passage'/'TSB', '108879').
card_timeshifted('honorable passage'/'TSB')
.
card_in_set('hunting moa', 'TSB').
card_original_type('hunting moa'/'TSB', 'Creature — Bird').
card_original_text('hunting moa'/'TSB', 'Echo {2}{G} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhenever Hunting Moa comes into play or is put into a graveyard from play, put a +1/+1 counter on target creature.').
card_image_name('hunting moa'/'TSB', 'hunting moa').
card_uid('hunting moa'/'TSB', 'TSB:Hunting Moa:hunting moa').
card_rarity('hunting moa'/'TSB', 'Special').
card_artist('hunting moa'/'TSB', 'DiTerlizzi').
card_number('hunting moa'/'TSB', '80').
card_multiverse_id('hunting moa'/'TSB', '107282').
card_timeshifted('hunting moa'/'TSB')
.
card_in_set('icatian javelineers', 'TSB').
card_original_type('icatian javelineers'/'TSB', 'Creature — Human Soldier').
card_original_text('icatian javelineers'/'TSB', 'Icatian Javelineers comes into play with a javelin counter on it.\n{T}, Remove a javelin counter from Icatian Javelineers: Icatian Javelineers deals 1 damage to target creature or player.').
card_image_name('icatian javelineers'/'TSB', 'icatian javelineers').
card_uid('icatian javelineers'/'TSB', 'TSB:Icatian Javelineers:icatian javelineers').
card_rarity('icatian javelineers'/'TSB', 'Special').
card_artist('icatian javelineers'/'TSB', 'Scott Kirschner').
card_number('icatian javelineers'/'TSB', '10').
card_multiverse_id('icatian javelineers'/'TSB', '107283').
card_timeshifted('icatian javelineers'/'TSB')
.
card_in_set('jasmine boreal', 'TSB').
card_original_type('jasmine boreal'/'TSB', 'Legendary Creature — Human').
card_original_text('jasmine boreal'/'TSB', '').
card_image_name('jasmine boreal'/'TSB', 'jasmine boreal').
card_uid('jasmine boreal'/'TSB', 'TSB:Jasmine Boreal:jasmine boreal').
card_rarity('jasmine boreal'/'TSB', 'Special').
card_artist('jasmine boreal'/'TSB', 'Richard Kane Ferguson').
card_number('jasmine boreal'/'TSB', '93').
card_flavor_text('jasmine boreal'/'TSB', '\"Peace must prevail, even if the wicked must die.\"').
card_multiverse_id('jasmine boreal'/'TSB', '109764').
card_timeshifted('jasmine boreal'/'TSB')
.
card_in_set('jolrael, empress of beasts', 'TSB').
card_original_type('jolrael, empress of beasts'/'TSB', 'Legendary Creature — Human Spellshaper').
card_original_text('jolrael, empress of beasts'/'TSB', '{2}{G}, {T}, Discard two cards: Until end of turn, all lands target player controls are 3/3 creatures that are still lands.').
card_image_name('jolrael, empress of beasts'/'TSB', 'jolrael, empress of beasts').
card_uid('jolrael, empress of beasts'/'TSB', 'TSB:Jolrael, Empress of Beasts:jolrael, empress of beasts').
card_rarity('jolrael, empress of beasts'/'TSB', 'Special').
card_artist('jolrael, empress of beasts'/'TSB', 'Matthew D. Wilson').
card_number('jolrael, empress of beasts'/'TSB', '81').
card_flavor_text('jolrael, empress of beasts'/'TSB', '\"I need no army. I have Jamuraa.\"').
card_multiverse_id('jolrael, empress of beasts'/'TSB', '108786').
card_timeshifted('jolrael, empress of beasts'/'TSB')
.
card_in_set('kobold taskmaster', 'TSB').
card_original_type('kobold taskmaster'/'TSB', 'Creature — Kobold').
card_original_text('kobold taskmaster'/'TSB', 'Other Kobolds you control get +1/+0.').
card_image_name('kobold taskmaster'/'TSB', 'kobold taskmaster').
card_uid('kobold taskmaster'/'TSB', 'TSB:Kobold Taskmaster:kobold taskmaster').
card_rarity('kobold taskmaster'/'TSB', 'Special').
card_artist('kobold taskmaster'/'TSB', 'Randy Asplund-Faith').
card_number('kobold taskmaster'/'TSB', '65').
card_flavor_text('kobold taskmaster'/'TSB', 'The taskmaster knows that there is no cure for the common kobold.').
card_multiverse_id('kobold taskmaster'/'TSB', '106664').
card_timeshifted('kobold taskmaster'/'TSB')
.
card_in_set('krosan cloudscraper', 'TSB').
card_original_type('krosan cloudscraper'/'TSB', 'Creature — Beast Mutant').
card_original_text('krosan cloudscraper'/'TSB', 'At the beginning of your upkeep, sacrifice Krosan Cloudscraper unless you pay {G}{G}.\nMorph {7}{G}{G} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_image_name('krosan cloudscraper'/'TSB', 'krosan cloudscraper').
card_uid('krosan cloudscraper'/'TSB', 'TSB:Krosan Cloudscraper:krosan cloudscraper').
card_rarity('krosan cloudscraper'/'TSB', 'Special').
card_artist('krosan cloudscraper'/'TSB', 'Ron Spears').
card_number('krosan cloudscraper'/'TSB', '82').
card_multiverse_id('krosan cloudscraper'/'TSB', '109684').
card_timeshifted('krosan cloudscraper'/'TSB')
.
card_in_set('leviathan', 'TSB').
card_original_type('leviathan'/'TSB', 'Creature — Leviathan').
card_original_text('leviathan'/'TSB', 'Trample\nLeviathan comes into play tapped and doesn\'t untap during your untap step.\nAt the beginning of your upkeep, you may sacrifice two Islands. If you do, untap Leviathan.\nLeviathan can\'t attack unless you sacrifice two Islands.').
card_image_name('leviathan'/'TSB', 'leviathan').
card_uid('leviathan'/'TSB', 'TSB:Leviathan:leviathan').
card_rarity('leviathan'/'TSB', 'Special').
card_artist('leviathan'/'TSB', 'Mark Tedin').
card_number('leviathan'/'TSB', '23').
card_multiverse_id('leviathan'/'TSB', '108920').
card_timeshifted('leviathan'/'TSB')
.
card_in_set('lightning angel', 'TSB').
card_original_type('lightning angel'/'TSB', 'Creature — Angel').
card_original_text('lightning angel'/'TSB', 'Flying, vigilance, haste').
card_image_name('lightning angel'/'TSB', 'lightning angel').
card_uid('lightning angel'/'TSB', 'TSB:Lightning Angel:lightning angel').
card_rarity('lightning angel'/'TSB', 'Special').
card_artist('lightning angel'/'TSB', 'rk post').
card_number('lightning angel'/'TSB', '94').
card_multiverse_id('lightning angel'/'TSB', '109746').
card_timeshifted('lightning angel'/'TSB')
.
card_in_set('lord of atlantis', 'TSB').
card_original_type('lord of atlantis'/'TSB', 'Creature — Merfolk Lord').
card_original_text('lord of atlantis'/'TSB', 'Other Merfolk get +1/+1 and have islandwalk.').
card_image_name('lord of atlantis'/'TSB', 'lord of atlantis').
card_uid('lord of atlantis'/'TSB', 'TSB:Lord of Atlantis:lord of atlantis').
card_rarity('lord of atlantis'/'TSB', 'Special').
card_artist('lord of atlantis'/'TSB', 'Melissa A. Benson').
card_number('lord of atlantis'/'TSB', '24').
card_flavor_text('lord of atlantis'/'TSB', 'A master of tactics, the Lord of Atlantis makes his people bold in battle merely by arriving to lead them.').
card_multiverse_id('lord of atlantis'/'TSB', '106642').
card_timeshifted('lord of atlantis'/'TSB')
.
card_in_set('merfolk assassin', 'TSB').
card_original_type('merfolk assassin'/'TSB', 'Creature — Merfolk Assassin').
card_original_text('merfolk assassin'/'TSB', '{T}: Destroy target creature with islandwalk.').
card_image_name('merfolk assassin'/'TSB', 'merfolk assassin').
card_uid('merfolk assassin'/'TSB', 'TSB:Merfolk Assassin:merfolk assassin').
card_rarity('merfolk assassin'/'TSB', 'Special').
card_artist('merfolk assassin'/'TSB', 'Dennis Detwiller').
card_number('merfolk assassin'/'TSB', '25').
card_multiverse_id('merfolk assassin'/'TSB', '108821').
card_timeshifted('merfolk assassin'/'TSB')
.
card_in_set('merieke ri berit', 'TSB').
card_original_type('merieke ri berit'/'TSB', 'Legendary Creature — Human').
card_original_text('merieke ri berit'/'TSB', 'Merieke Ri Berit doesn\'t untap during your untap step.\n{T}: Gain control of target creature as long as you control Merieke Ri Berit. When Merieke Ri Berit leaves play or becomes untapped, destroy that creature. It can\'t be regenerated.').
card_image_name('merieke ri berit'/'TSB', 'merieke ri berit').
card_uid('merieke ri berit'/'TSB', 'TSB:Merieke Ri Berit:merieke ri berit').
card_rarity('merieke ri berit'/'TSB', 'Special').
card_artist('merieke ri berit'/'TSB', 'Heather Hudson').
card_number('merieke ri berit'/'TSB', '95').
card_multiverse_id('merieke ri berit'/'TSB', '108871').
card_timeshifted('merieke ri berit'/'TSB')
.
card_in_set('mindless automaton', 'TSB').
card_original_type('mindless automaton'/'TSB', 'Artifact Creature — Construct').
card_original_text('mindless automaton'/'TSB', 'Mindless Automaton comes into play with two +1/+1 counters on it.\n{1}, Discard a card: Put a +1/+1 counter on Mindless Automaton.\nRemove two +1/+1 counters from Mindless Automaton: Draw a card.').
card_image_name('mindless automaton'/'TSB', 'mindless automaton').
card_uid('mindless automaton'/'TSB', 'TSB:Mindless Automaton:mindless automaton').
card_rarity('mindless automaton'/'TSB', 'Special').
card_artist('mindless automaton'/'TSB', 'Brian Snõddy').
card_number('mindless automaton'/'TSB', '111').
card_multiverse_id('mindless automaton'/'TSB', '108933').
card_timeshifted('mindless automaton'/'TSB')
.
card_in_set('mirari', 'TSB').
card_original_type('mirari'/'TSB', 'Legendary Artifact').
card_original_text('mirari'/'TSB', 'Whenever you play an instant or sorcery spell, you may pay {3}. If you do, copy that spell. You may choose new targets for the copy.').
card_image_name('mirari'/'TSB', 'mirari').
card_uid('mirari'/'TSB', 'TSB:Mirari:mirari').
card_rarity('mirari'/'TSB', 'Special').
card_artist('mirari'/'TSB', 'Donato Giancola').
card_number('mirari'/'TSB', '112').
card_flavor_text('mirari'/'TSB', '\"It offers you what you want, not what you need.\"\n—Braids, dementia summoner').
card_multiverse_id('mirari'/'TSB', '109724').
card_timeshifted('mirari'/'TSB')
.
card_in_set('mistform ultimus', 'TSB').
card_original_type('mistform ultimus'/'TSB', 'Legendary Creature — Illusion').
card_original_text('mistform ultimus'/'TSB', 'Mistform Ultimus is every creature type (even if this card isn\'t in play).').
card_image_name('mistform ultimus'/'TSB', 'mistform ultimus').
card_uid('mistform ultimus'/'TSB', 'TSB:Mistform Ultimus:mistform ultimus').
card_rarity('mistform ultimus'/'TSB', 'Special').
card_artist('mistform ultimus'/'TSB', 'Anthony S. Waters').
card_number('mistform ultimus'/'TSB', '26').
card_flavor_text('mistform ultimus'/'TSB', 'It may wear your face, but its mind is its own.').
card_multiverse_id('mistform ultimus'/'TSB', '111062').
card_timeshifted('mistform ultimus'/'TSB')
.
card_in_set('moorish cavalry', 'TSB').
card_original_type('moorish cavalry'/'TSB', 'Creature — Human Knight').
card_original_text('moorish cavalry'/'TSB', 'Trample').
card_image_name('moorish cavalry'/'TSB', 'moorish cavalry').
card_uid('moorish cavalry'/'TSB', 'TSB:Moorish Cavalry:moorish cavalry').
card_rarity('moorish cavalry'/'TSB', 'Special').
card_artist('moorish cavalry'/'TSB', 'Dameon Willich').
card_number('moorish cavalry'/'TSB', '11').
card_flavor_text('moorish cavalry'/'TSB', 'Members of the elite Moorish Cavalry are very particular about their mounts, choosing only those whose bloodlines have been pure for generations.').
card_multiverse_id('moorish cavalry'/'TSB', '108849').
card_timeshifted('moorish cavalry'/'TSB')
.
card_in_set('mystic enforcer', 'TSB').
card_original_type('mystic enforcer'/'TSB', 'Creature — Human Nomad Mystic').
card_original_text('mystic enforcer'/'TSB', 'Protection from black\nThreshold Mystic Enforcer gets +3/+3 and has flying as long as seven or more cards are in your graveyard.').
card_image_name('mystic enforcer'/'TSB', 'mystic enforcer').
card_uid('mystic enforcer'/'TSB', 'TSB:Mystic Enforcer:mystic enforcer').
card_rarity('mystic enforcer'/'TSB', 'Special').
card_artist('mystic enforcer'/'TSB', 'Gary Ruddell').
card_number('mystic enforcer'/'TSB', '96').
card_multiverse_id('mystic enforcer'/'TSB', '108828').
card_timeshifted('mystic enforcer'/'TSB')
.
card_in_set('mystic snake', 'TSB').
card_original_type('mystic snake'/'TSB', 'Creature — Snake').
card_original_text('mystic snake'/'TSB', 'Flash (You may play this spell any time you could play an instant.)\nWhen Mystic Snake comes into play, counter target spell.').
card_image_name('mystic snake'/'TSB', 'mystic snake').
card_uid('mystic snake'/'TSB', 'TSB:Mystic Snake:mystic snake').
card_rarity('mystic snake'/'TSB', 'Special').
card_artist('mystic snake'/'TSB', 'Daren Bader').
card_number('mystic snake'/'TSB', '97').
card_flavor_text('mystic snake'/'TSB', 'Its fangs are in your flesh before its hiss leaves your ears.').
card_multiverse_id('mystic snake'/'TSB', '109693').
card_timeshifted('mystic snake'/'TSB')
.
card_in_set('nicol bolas', 'TSB').
card_original_type('nicol bolas'/'TSB', 'Legendary Creature — Elder Dragon').
card_original_text('nicol bolas'/'TSB', 'Flying\nAt the beginning of your upkeep, sacrifice Nicol Bolas unless you pay {U}{B}{R}.\nWhenever Nicol Bolas deals damage to an opponent, that player discards his or her hand.').
card_image_name('nicol bolas'/'TSB', 'nicol bolas').
card_uid('nicol bolas'/'TSB', 'TSB:Nicol Bolas:nicol bolas').
card_rarity('nicol bolas'/'TSB', 'Special').
card_artist('nicol bolas'/'TSB', 'Edward P. Beard, Jr.').
card_number('nicol bolas'/'TSB', '98').
card_multiverse_id('nicol bolas'/'TSB', '109754').
card_timeshifted('nicol bolas'/'TSB')
.
card_in_set('orcish librarian', 'TSB').
card_original_type('orcish librarian'/'TSB', 'Creature — Orc').
card_original_text('orcish librarian'/'TSB', '{R}, {T}: Look at the top eight cards of your library. Remove four of them at random from the game, then put the rest on top of your library in any order.').
card_image_name('orcish librarian'/'TSB', 'orcish librarian').
card_uid('orcish librarian'/'TSB', 'TSB:Orcish Librarian:orcish librarian').
card_rarity('orcish librarian'/'TSB', 'Special').
card_artist('orcish librarian'/'TSB', 'Phil Foglio').
card_number('orcish librarian'/'TSB', '66').
card_flavor_text('orcish librarian'/'TSB', '\"Us hungry, need food . . . . Lots of books . . . . Hmm . . . .\"').
card_multiverse_id('orcish librarian'/'TSB', '108833').
card_timeshifted('orcish librarian'/'TSB')
.
card_in_set('orgg', 'TSB').
card_original_type('orgg'/'TSB', 'Creature — Orgg').
card_original_text('orgg'/'TSB', 'Trample\nOrgg can\'t attack if defending player controls an untapped creature with power 3 or greater.\nOrgg can\'t block creatures with power 3 or greater.').
card_image_name('orgg'/'TSB', 'orgg').
card_uid('orgg'/'TSB', 'TSB:Orgg:orgg').
card_rarity('orgg'/'TSB', 'Special').
card_artist('orgg'/'TSB', 'Daniel Gelon').
card_number('orgg'/'TSB', '67').
card_flavor_text('orgg'/'TSB', 'It\'s bigger than it thinks.').
card_multiverse_id('orgg'/'TSB', '106646').
card_timeshifted('orgg'/'TSB')
.
card_in_set('ovinomancer', 'TSB').
card_original_type('ovinomancer'/'TSB', 'Creature — Human Wizard').
card_original_text('ovinomancer'/'TSB', 'When Ovinomancer comes into play, sacrifice it unless you return three basic lands you control to their owner\'s hand.\n{T}, Return Ovinomancer to its owner\'s hand: Destroy target creature. It can\'t be regenerated. That creature\'s controller puts a 0/1 green Sheep creature token into play.').
card_image_name('ovinomancer'/'TSB', 'ovinomancer').
card_uid('ovinomancer'/'TSB', 'TSB:Ovinomancer:ovinomancer').
card_rarity('ovinomancer'/'TSB', 'Special').
card_artist('ovinomancer'/'TSB', 'Kev Walker').
card_number('ovinomancer'/'TSB', '27').
card_multiverse_id('ovinomancer'/'TSB', '108863').
card_timeshifted('ovinomancer'/'TSB')
.
card_in_set('pandemonium', 'TSB').
card_original_type('pandemonium'/'TSB', 'Enchantment').
card_original_text('pandemonium'/'TSB', 'Whenever a creature comes into play, that creature\'s controller may have it deal damage equal to its power to target creature or player of his or her choice.').
card_image_name('pandemonium'/'TSB', 'pandemonium').
card_uid('pandemonium'/'TSB', 'TSB:Pandemonium:pandemonium').
card_rarity('pandemonium'/'TSB', 'Special').
card_artist('pandemonium'/'TSB', 'Pete Venters').
card_number('pandemonium'/'TSB', '68').
card_flavor_text('pandemonium'/'TSB', '\"If we cannot live proudly, we die so!\"\n—Eladamri, Lord of Leaves').
card_multiverse_id('pandemonium'/'TSB', '108862').
card_timeshifted('pandemonium'/'TSB')
.
card_in_set('pendelhaven', 'TSB').
card_original_type('pendelhaven'/'TSB', 'Legendary Land').
card_original_text('pendelhaven'/'TSB', '{T}: Add {G} to your mana pool.\n{T}: Target 1/1 creature gets +1/+2 until end of turn.').
card_image_name('pendelhaven'/'TSB', 'pendelhaven').
card_uid('pendelhaven'/'TSB', 'TSB:Pendelhaven:pendelhaven').
card_rarity('pendelhaven'/'TSB', 'Special').
card_artist('pendelhaven'/'TSB', 'Bryon Wackwitz').
card_number('pendelhaven'/'TSB', '120').
card_flavor_text('pendelhaven'/'TSB', '\"This is the forest primeval. The murmuring pines and the hemlocks . . . Stand like Druids of eld.\"\n—Henry Wadsworth Longfellow,\n\"Evangeline\"').
card_multiverse_id('pendelhaven'/'TSB', '109755').
card_timeshifted('pendelhaven'/'TSB')
.
card_in_set('pirate ship', 'TSB').
card_original_type('pirate ship'/'TSB', 'Creature — Human Pirate').
card_original_text('pirate ship'/'TSB', 'Pirate Ship can\'t attack unless defending player controls an Island.\n{T}: Pirate Ship deals 1 damage to target creature or player.\nWhen you control no Islands, sacrifice Pirate Ship.').
card_image_name('pirate ship'/'TSB', 'pirate ship').
card_uid('pirate ship'/'TSB', 'TSB:Pirate Ship:pirate ship').
card_rarity('pirate ship'/'TSB', 'Special').
card_artist('pirate ship'/'TSB', 'Tom Wänerstrand').
card_number('pirate ship'/'TSB', '28').
card_multiverse_id('pirate ship'/'TSB', '108925').
card_timeshifted('pirate ship'/'TSB')
.
card_in_set('prodigal sorcerer', 'TSB').
card_original_type('prodigal sorcerer'/'TSB', 'Creature — Human Wizard').
card_original_text('prodigal sorcerer'/'TSB', '{T}: Prodigal Sorcerer deals 1 damage to target creature or player.').
card_image_name('prodigal sorcerer'/'TSB', 'prodigal sorcerer').
card_uid('prodigal sorcerer'/'TSB', 'TSB:Prodigal Sorcerer:prodigal sorcerer').
card_rarity('prodigal sorcerer'/'TSB', 'Special').
card_artist('prodigal sorcerer'/'TSB', 'Douglas Shuler').
card_number('prodigal sorcerer'/'TSB', '29').
card_flavor_text('prodigal sorcerer'/'TSB', 'Occasionally, members of the Institute of Arcane Study acquire a taste for worldly pleasures. Seldom do they have trouble finding employment.').
card_multiverse_id('prodigal sorcerer'/'TSB', '108906').
card_timeshifted('prodigal sorcerer'/'TSB')
.
card_in_set('psionic blast', 'TSB').
card_original_type('psionic blast'/'TSB', 'Instant').
card_original_text('psionic blast'/'TSB', 'Psionic Blast deals 4 damage to target creature or player and 2 damage to you.').
card_image_name('psionic blast'/'TSB', 'psionic blast').
card_uid('psionic blast'/'TSB', 'TSB:Psionic Blast:psionic blast').
card_rarity('psionic blast'/'TSB', 'Special').
card_artist('psionic blast'/'TSB', 'Douglas Shuler').
card_number('psionic blast'/'TSB', '30').
card_multiverse_id('psionic blast'/'TSB', '108812').
card_timeshifted('psionic blast'/'TSB')
.
card_in_set('resurrection', 'TSB').
card_original_type('resurrection'/'TSB', 'Sorcery').
card_original_text('resurrection'/'TSB', 'Return target creature card from your graveyard to play.').
card_image_name('resurrection'/'TSB', 'resurrection').
card_uid('resurrection'/'TSB', 'TSB:Resurrection:resurrection').
card_rarity('resurrection'/'TSB', 'Special').
card_artist('resurrection'/'TSB', 'Dan Frazier').
card_number('resurrection'/'TSB', '12').
card_multiverse_id('resurrection'/'TSB', '108885').
card_timeshifted('resurrection'/'TSB')
.
card_in_set('sacred mesa', 'TSB').
card_original_type('sacred mesa'/'TSB', 'Enchantment').
card_original_text('sacred mesa'/'TSB', 'At the beginning of your upkeep, sacrifice Sacred Mesa unless you sacrifice a Pegasus.\n{1}{W}: Put a 1/1 white Pegasus creature token with flying into play.').
card_image_name('sacred mesa'/'TSB', 'sacred mesa').
card_uid('sacred mesa'/'TSB', 'TSB:Sacred Mesa:sacred mesa').
card_rarity('sacred mesa'/'TSB', 'Special').
card_artist('sacred mesa'/'TSB', 'Margaret Organ-Kean').
card_number('sacred mesa'/'TSB', '13').
card_flavor_text('sacred mesa'/'TSB', '\"Do not go there, do not go / unless you rise on wings, unless you walk on hooves.\"\n—\"Song to the Sun,\" Femeref song').
card_multiverse_id('sacred mesa'/'TSB', '106625').
card_timeshifted('sacred mesa'/'TSB')
.
card_in_set('safe haven', 'TSB').
card_original_type('safe haven'/'TSB', 'Land').
card_original_text('safe haven'/'TSB', '{2}, {T}: Remove target creature you control from the game.\nAt the beginning of your upkeep, you may sacrifice Safe Haven. If you do, return each card removed from the game with Safe Haven to play under its owner\'s control.').
card_image_name('safe haven'/'TSB', 'safe haven').
card_uid('safe haven'/'TSB', 'TSB:Safe Haven:safe haven').
card_rarity('safe haven'/'TSB', 'Special').
card_artist('safe haven'/'TSB', 'Christopher Rush').
card_number('safe haven'/'TSB', '121').
card_multiverse_id('safe haven'/'TSB', '107275').
card_timeshifted('safe haven'/'TSB')
.
card_in_set('scragnoth', 'TSB').
card_original_type('scragnoth'/'TSB', 'Creature — Beast').
card_original_text('scragnoth'/'TSB', 'Scragnoth can\'t be countered.\nProtection from blue').
card_image_name('scragnoth'/'TSB', 'scragnoth').
card_uid('scragnoth'/'TSB', 'TSB:Scragnoth:scragnoth').
card_rarity('scragnoth'/'TSB', 'Special').
card_artist('scragnoth'/'TSB', 'Jeff Laubenstein').
card_number('scragnoth'/'TSB', '83').
card_flavor_text('scragnoth'/'TSB', 'It possesses no intelligence, only counter-intelligence.').
card_multiverse_id('scragnoth'/'TSB', '106650').
card_timeshifted('scragnoth'/'TSB')
.
card_in_set('sengir autocrat', 'TSB').
card_original_type('sengir autocrat'/'TSB', 'Creature — Human').
card_original_text('sengir autocrat'/'TSB', 'When Sengir Autocrat comes into play, put three 0/1 black Serf creature tokens into play.\nWhen Sengir Autocrat leaves play, remove all Serf tokens from the game.').
card_image_name('sengir autocrat'/'TSB', 'sengir autocrat').
card_uid('sengir autocrat'/'TSB', 'TSB:Sengir Autocrat:sengir autocrat').
card_rarity('sengir autocrat'/'TSB', 'Special').
card_artist('sengir autocrat'/'TSB', 'David A. Cherry').
card_number('sengir autocrat'/'TSB', '45').
card_flavor_text('sengir autocrat'/'TSB', '\"A thankless job, and a death sentence besides.\"\n—Eron the Relentless').
card_multiverse_id('sengir autocrat'/'TSB', '106638').
card_timeshifted('sengir autocrat'/'TSB')
.
card_in_set('serrated arrows', 'TSB').
card_original_type('serrated arrows'/'TSB', 'Artifact').
card_original_text('serrated arrows'/'TSB', 'Serrated Arrows comes into play with three arrowhead counters on it.\nAt the beginning of your upkeep, if there are no arrowhead counters on Serrated Arrows, sacrifice it.\n{T}, Remove an arrowhead counter from Serrated Arrows: Put a -1/-1 counter on target creature.').
card_image_name('serrated arrows'/'TSB', 'serrated arrows').
card_uid('serrated arrows'/'TSB', 'TSB:Serrated Arrows:serrated arrows').
card_rarity('serrated arrows'/'TSB', 'Special').
card_artist('serrated arrows'/'TSB', 'David A. Cherry').
card_number('serrated arrows'/'TSB', '114').
card_multiverse_id('serrated arrows'/'TSB', '109730').
card_timeshifted('serrated arrows'/'TSB')
.
card_in_set('shadow guildmage', 'TSB').
card_original_type('shadow guildmage'/'TSB', 'Creature — Human Wizard').
card_original_text('shadow guildmage'/'TSB', '{U}, {T}: Put target creature you control on top of its owner\'s library.\n{R}, {T}: Shadow Guildmage deals 1 damage to target creature or player and 1 damage to you.').
card_image_name('shadow guildmage'/'TSB', 'shadow guildmage').
card_uid('shadow guildmage'/'TSB', 'TSB:Shadow Guildmage:shadow guildmage').
card_rarity('shadow guildmage'/'TSB', 'Special').
card_artist('shadow guildmage'/'TSB', 'Mike Kimble').
card_number('shadow guildmage'/'TSB', '46').
card_flavor_text('shadow guildmage'/'TSB', 'To keep the dead so others may live.\n—Shadow Guild maxim').
card_multiverse_id('shadow guildmage'/'TSB', '108864').
card_timeshifted('shadow guildmage'/'TSB')
.
card_in_set('shadowmage infiltrator', 'TSB').
card_original_type('shadowmage infiltrator'/'TSB', 'Creature — Human Wizard').
card_original_text('shadowmage infiltrator'/'TSB', 'Fear\nWhenever Shadowmage Infiltrator deals combat damage to a player, you may draw a card.').
card_image_name('shadowmage infiltrator'/'TSB', 'shadowmage infiltrator').
card_uid('shadowmage infiltrator'/'TSB', 'TSB:Shadowmage Infiltrator:shadowmage infiltrator').
card_rarity('shadowmage infiltrator'/'TSB', 'Special').
card_artist('shadowmage infiltrator'/'TSB', 'Rick Farrell').
card_number('shadowmage infiltrator'/'TSB', '99').
card_multiverse_id('shadowmage infiltrator'/'TSB', '126333').
card_timeshifted('shadowmage infiltrator'/'TSB')
.
card_in_set('sindbad', 'TSB').
card_original_type('sindbad'/'TSB', 'Creature — Human').
card_original_text('sindbad'/'TSB', '{T}: Draw a card and reveal it. If it isn\'t a land card, discard it.').
card_image_name('sindbad'/'TSB', 'sindbad').
card_uid('sindbad'/'TSB', 'TSB:Sindbad:sindbad').
card_rarity('sindbad'/'TSB', 'Special').
card_artist('sindbad'/'TSB', 'Julie Baroh').
card_number('sindbad'/'TSB', '31').
card_multiverse_id('sindbad'/'TSB', '108935').
card_timeshifted('sindbad'/'TSB')
.
card_in_set('sol\'kanar the swamp king', 'TSB').
card_original_type('sol\'kanar the swamp king'/'TSB', 'Legendary Creature — Demon').
card_original_text('sol\'kanar the swamp king'/'TSB', 'Swampwalk\nWhenever a player plays a black spell, you gain 1 life.').
card_image_name('sol\'kanar the swamp king'/'TSB', 'sol\'kanar the swamp king').
card_uid('sol\'kanar the swamp king'/'TSB', 'TSB:Sol\'kanar the Swamp King:sol\'kanar the swamp king').
card_rarity('sol\'kanar the swamp king'/'TSB', 'Special').
card_artist('sol\'kanar the swamp king'/'TSB', 'Richard Kane Ferguson').
card_number('sol\'kanar the swamp king'/'TSB', '100').
card_multiverse_id('sol\'kanar the swamp king'/'TSB', '109723').
card_timeshifted('sol\'kanar the swamp king'/'TSB')
.
card_in_set('soltari priest', 'TSB').
card_original_type('soltari priest'/'TSB', 'Creature — Soltari Cleric').
card_original_text('soltari priest'/'TSB', 'Protection from red\nShadow (This creature can block or be blocked by only creatures with shadow.)').
card_image_name('soltari priest'/'TSB', 'soltari priest').
card_uid('soltari priest'/'TSB', 'TSB:Soltari Priest:soltari priest').
card_rarity('soltari priest'/'TSB', 'Special').
card_artist('soltari priest'/'TSB', 'Janet Aulisio').
card_number('soltari priest'/'TSB', '14').
card_flavor_text('soltari priest'/'TSB', '\"In Rath,\" the priest said, \"there is even greater need for prayer.\"').
card_multiverse_id('soltari priest'/'TSB', '108856').
card_timeshifted('soltari priest'/'TSB')
.
card_in_set('soul collector', 'TSB').
card_original_type('soul collector'/'TSB', 'Creature — Vampire').
card_original_text('soul collector'/'TSB', 'Flying\nWhenever a creature dealt damage by Soul Collector this turn is put into a graveyard, return that card to play under your control.\nMorph {B}{B}{B} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_image_name('soul collector'/'TSB', 'soul collector').
card_uid('soul collector'/'TSB', 'TSB:Soul Collector:soul collector').
card_rarity('soul collector'/'TSB', 'Special').
card_artist('soul collector'/'TSB', 'Matthew D. Wilson').
card_number('soul collector'/'TSB', '47').
card_multiverse_id('soul collector'/'TSB', '106644').
card_timeshifted('soul collector'/'TSB')
.
card_in_set('spike feeder', 'TSB').
card_original_type('spike feeder'/'TSB', 'Creature — Spike').
card_original_text('spike feeder'/'TSB', 'Spike Feeder comes into play with two +1/+1 counters on it.\n{2}, Remove a +1/+1 counter from Spike Feeder: Put a +1/+1 counter on target creature.\nRemove a +1/+1 counter from Spike Feeder: You gain 2 life.').
card_image_name('spike feeder'/'TSB', 'spike feeder').
card_uid('spike feeder'/'TSB', 'TSB:Spike Feeder:spike feeder').
card_rarity('spike feeder'/'TSB', 'Special').
card_artist('spike feeder'/'TSB', 'Heather Hudson').
card_number('spike feeder'/'TSB', '84').
card_multiverse_id('spike feeder'/'TSB', '108924').
card_timeshifted('spike feeder'/'TSB')
.
card_in_set('spined sliver', 'TSB').
card_original_type('spined sliver'/'TSB', 'Creature — Sliver').
card_original_text('spined sliver'/'TSB', 'Whenever a Sliver becomes blocked, that Sliver gets +1/+1 until end of turn for each creature blocking it.').
card_image_name('spined sliver'/'TSB', 'spined sliver').
card_uid('spined sliver'/'TSB', 'TSB:Spined Sliver:spined sliver').
card_rarity('spined sliver'/'TSB', 'Special').
card_artist('spined sliver'/'TSB', 'Ron Spencer').
card_number('spined sliver'/'TSB', '101').
card_flavor_text('spined sliver'/'TSB', '\"Slivers are evil and slivers are sly;\nAnd if you get eaten, then no one will cry.\"\n—Mogg children\'s rhyme').
card_multiverse_id('spined sliver'/'TSB', '109679').
card_timeshifted('spined sliver'/'TSB')
.
card_in_set('spitting slug', 'TSB').
card_original_type('spitting slug'/'TSB', 'Creature — Slug').
card_original_text('spitting slug'/'TSB', 'Whenever Spitting Slug blocks or becomes blocked, you may pay {1}{G}. If you do, Spitting Slug gains first strike until end of turn. Otherwise, each creature blocking or blocked by Spitting Slug gains first strike until end of turn.').
card_image_name('spitting slug'/'TSB', 'spitting slug').
card_uid('spitting slug'/'TSB', 'TSB:Spitting Slug:spitting slug').
card_rarity('spitting slug'/'TSB', 'Special').
card_artist('spitting slug'/'TSB', 'Anson Maddocks').
card_number('spitting slug'/'TSB', '85').
card_multiverse_id('spitting slug'/'TSB', '108855').
card_timeshifted('spitting slug'/'TSB')
.
card_in_set('squire', 'TSB').
card_original_type('squire'/'TSB', 'Creature — Human Soldier').
card_original_text('squire'/'TSB', '').
card_image_name('squire'/'TSB', 'squire').
card_uid('squire'/'TSB', 'TSB:Squire:squire').
card_rarity('squire'/'TSB', 'Special').
card_artist('squire'/'TSB', 'Dennis Detwiller').
card_number('squire'/'TSB', '15').
card_flavor_text('squire'/'TSB', '\"Of twenty yeer of age he was, I gesse.\nOf his stature he was of evene lengthe.\nAnd wonderly delyvere, and of greete strengthe.\"\n—Geoffrey Chaucer,\nThe Canterbury Tales').
card_multiverse_id('squire'/'TSB', '108908').
card_timeshifted('squire'/'TSB')
.
card_in_set('stormbind', 'TSB').
card_original_type('stormbind'/'TSB', 'Enchantment').
card_original_text('stormbind'/'TSB', '{2}, Discard a card at random: Stormbind deals 2 damage to target creature or player.').
card_image_name('stormbind'/'TSB', 'stormbind').
card_uid('stormbind'/'TSB', 'TSB:Stormbind:stormbind').
card_rarity('stormbind'/'TSB', 'Special').
card_artist('stormbind'/'TSB', 'NéNé Thomas & Phillip Mosness').
card_number('stormbind'/'TSB', '102').
card_flavor_text('stormbind'/'TSB', '\"Once, our people could call down the storm itself to do our bidding.\"\n—Lovisa Coldeyes, Balduvian chieftain').
card_multiverse_id('stormbind'/'TSB', '109674').
card_timeshifted('stormbind'/'TSB')
.
card_in_set('stormscape familiar', 'TSB').
card_original_type('stormscape familiar'/'TSB', 'Creature — Bird').
card_original_text('stormscape familiar'/'TSB', 'Flying\nWhite spells and black spells you play cost {1} less to play.').
card_image_name('stormscape familiar'/'TSB', 'stormscape familiar').
card_uid('stormscape familiar'/'TSB', 'TSB:Stormscape Familiar:stormscape familiar').
card_rarity('stormscape familiar'/'TSB', 'Special').
card_artist('stormscape familiar'/'TSB', 'Heather Hudson').
card_number('stormscape familiar'/'TSB', '32').
card_flavor_text('stormscape familiar'/'TSB', 'Each Stormscape apprentice must hand-raise and train an owl before attaining the rank of master.').
card_multiverse_id('stormscape familiar'/'TSB', '106637').
card_timeshifted('stormscape familiar'/'TSB')
.
card_in_set('stupor', 'TSB').
card_original_type('stupor'/'TSB', 'Sorcery').
card_original_text('stupor'/'TSB', 'Target opponent discards a card at random, then discards a card.').
card_image_name('stupor'/'TSB', 'stupor').
card_uid('stupor'/'TSB', 'TSB:Stupor:stupor').
card_rarity('stupor'/'TSB', 'Special').
card_artist('stupor'/'TSB', 'Mike Kimble').
card_number('stupor'/'TSB', '48').
card_flavor_text('stupor'/'TSB', 'There are medicines for all afflictions but idleness.\n—Suq\'Ata saying').
card_multiverse_id('stupor'/'TSB', '108923').
card_timeshifted('stupor'/'TSB')
.
card_in_set('suq\'ata lancer', 'TSB').
card_original_type('suq\'ata lancer'/'TSB', 'Creature — Human Knight').
card_original_text('suq\'ata lancer'/'TSB', 'Haste\nFlanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)').
card_image_name('suq\'ata lancer'/'TSB', 'suq\'ata lancer').
card_uid('suq\'ata lancer'/'TSB', 'TSB:Suq\'Ata Lancer:suq\'ata lancer').
card_rarity('suq\'ata lancer'/'TSB', 'Special').
card_artist('suq\'ata lancer'/'TSB', 'Jeff Miracola').
card_number('suq\'ata lancer'/'TSB', '69').
card_flavor_text('suq\'ata lancer'/'TSB', '\"Never stop \'til you see your lance come out the other side.\"\n—Telim\'Tor').
card_multiverse_id('suq\'ata lancer'/'TSB', '108883').
card_timeshifted('suq\'ata lancer'/'TSB')
.
card_in_set('swamp mosquito', 'TSB').
card_original_type('swamp mosquito'/'TSB', 'Creature — Insect').
card_original_text('swamp mosquito'/'TSB', 'Flying\nWhenever Swamp Mosquito attacks and isn\'t blocked, defending player gets a poison counter. (A player with ten or more poison counters loses the game.)').
card_image_name('swamp mosquito'/'TSB', 'swamp mosquito').
card_uid('swamp mosquito'/'TSB', 'TSB:Swamp Mosquito:swamp mosquito').
card_rarity('swamp mosquito'/'TSB', 'Special').
card_artist('swamp mosquito'/'TSB', 'Nicola Leonard').
card_number('swamp mosquito'/'TSB', '49').
card_multiverse_id('swamp mosquito'/'TSB', '106656').
card_timeshifted('swamp mosquito'/'TSB')
.
card_in_set('teferi\'s moat', 'TSB').
card_original_type('teferi\'s moat'/'TSB', 'Enchantment').
card_original_text('teferi\'s moat'/'TSB', 'As Teferi\'s Moat comes into play, choose a color.\nCreatures of the chosen color without flying can\'t attack you.').
card_image_name('teferi\'s moat'/'TSB', 'teferi\'s moat').
card_uid('teferi\'s moat'/'TSB', 'TSB:Teferi\'s Moat:teferi\'s moat').
card_rarity('teferi\'s moat'/'TSB', 'Special').
card_artist('teferi\'s moat'/'TSB', 'rk post').
card_number('teferi\'s moat'/'TSB', '103').
card_flavor_text('teferi\'s moat'/'TSB', '\"Isolation is not the answer.\"\n—Urza, to Teferi').
card_multiverse_id('teferi\'s moat'/'TSB', '108787').
card_timeshifted('teferi\'s moat'/'TSB')
.
card_in_set('thallid', 'TSB').
card_original_type('thallid'/'TSB', 'Creature — Fungus').
card_original_text('thallid'/'TSB', 'At the beginning of your upkeep, put a spore counter on Thallid.\nRemove three spore counters from Thallid: Put a 1/1 green Saproling creature token into play.').
card_image_name('thallid'/'TSB', 'thallid').
card_uid('thallid'/'TSB', 'TSB:Thallid:thallid').
card_rarity('thallid'/'TSB', 'Special').
card_artist('thallid'/'TSB', 'Ron Spencer').
card_number('thallid'/'TSB', '86').
card_multiverse_id('thallid'/'TSB', '108825').
card_timeshifted('thallid'/'TSB')
.
card_in_set('the rack', 'TSB').
card_original_type('the rack'/'TSB', 'Artifact').
card_original_text('the rack'/'TSB', 'As The Rack comes into play, choose an opponent.\nAt the beginning of the chosen player\'s upkeep, The Rack deals X damage to that player, where X is 3 minus the number of cards in his or her hand.').
card_image_name('the rack'/'TSB', 'the rack').
card_uid('the rack'/'TSB', 'TSB:The Rack:the rack').
card_rarity('the rack'/'TSB', 'Special').
card_artist('the rack'/'TSB', 'Richard Thomas').
card_number('the rack'/'TSB', '113').
card_flavor_text('the rack'/'TSB', 'Invented in Mishra\'s earlier days, the rack was once his most feared creation.').
card_multiverse_id('the rack'/'TSB', '109725').
card_timeshifted('the rack'/'TSB')
.
card_in_set('thornscape battlemage', 'TSB').
card_original_type('thornscape battlemage'/'TSB', 'Creature — Elf Wizard').
card_original_text('thornscape battlemage'/'TSB', 'Kicker {R} and/or {W}\nWhen Thornscape Battlemage comes into play, if the {R} kicker cost was paid, it deals 2 damage to target creature or player.\nWhen Thornscape Battlemage comes into play, if the {W} kicker cost was paid, destroy target artifact.').
card_image_name('thornscape battlemage'/'TSB', 'thornscape battlemage').
card_uid('thornscape battlemage'/'TSB', 'TSB:Thornscape Battlemage:thornscape battlemage').
card_rarity('thornscape battlemage'/'TSB', 'Special').
card_artist('thornscape battlemage'/'TSB', 'Matt Cavotta').
card_number('thornscape battlemage'/'TSB', '87').
card_multiverse_id('thornscape battlemage'/'TSB', '109745').
card_timeshifted('thornscape battlemage'/'TSB')
.
card_in_set('tormod\'s crypt', 'TSB').
card_original_type('tormod\'s crypt'/'TSB', 'Artifact').
card_original_text('tormod\'s crypt'/'TSB', '{T}, Sacrifice Tormod\'s Crypt: Remove target player\'s graveyard from the game.').
card_image_name('tormod\'s crypt'/'TSB', 'tormod\'s crypt').
card_uid('tormod\'s crypt'/'TSB', 'TSB:Tormod\'s Crypt:tormod\'s crypt').
card_rarity('tormod\'s crypt'/'TSB', 'Special').
card_artist('tormod\'s crypt'/'TSB', 'Christopher Rush').
card_number('tormod\'s crypt'/'TSB', '115').
card_flavor_text('tormod\'s crypt'/'TSB', 'The dark opening seemed to breathe the cold, damp air of the dead earth in a steady rhythm.').
card_multiverse_id('tormod\'s crypt'/'TSB', '109716').
card_timeshifted('tormod\'s crypt'/'TSB')
.
card_in_set('tribal flames', 'TSB').
card_original_type('tribal flames'/'TSB', 'Sorcery').
card_original_text('tribal flames'/'TSB', 'Tribal Flames deals X damage to target creature or player, where X is the number of basic land types among lands you control.').
card_image_name('tribal flames'/'TSB', 'tribal flames').
card_uid('tribal flames'/'TSB', 'TSB:Tribal Flames:tribal flames').
card_rarity('tribal flames'/'TSB', 'Special').
card_artist('tribal flames'/'TSB', 'Tony Szczudlo').
card_number('tribal flames'/'TSB', '70').
card_flavor_text('tribal flames'/'TSB', '\"Fire is the universal language.\"\n—Jhoira, master artificer').
card_multiverse_id('tribal flames'/'TSB', '108916').
card_timeshifted('tribal flames'/'TSB')
.
card_in_set('twisted abomination', 'TSB').
card_original_type('twisted abomination'/'TSB', 'Creature — Zombie Mutant').
card_original_text('twisted abomination'/'TSB', '{B}: Regenerate Twisted Abomination.\nSwampcycling {2} ({2}, Discard this card: Search your library for a Swamp card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('twisted abomination'/'TSB', 'twisted abomination').
card_uid('twisted abomination'/'TSB', 'TSB:Twisted Abomination:twisted abomination').
card_rarity('twisted abomination'/'TSB', 'Special').
card_artist('twisted abomination'/'TSB', 'Daren Bader').
card_number('twisted abomination'/'TSB', '50').
card_multiverse_id('twisted abomination'/'TSB', '108848').
card_timeshifted('twisted abomination'/'TSB')
.
card_in_set('uncle istvan', 'TSB').
card_original_type('uncle istvan'/'TSB', 'Creature — Human').
card_original_text('uncle istvan'/'TSB', 'Prevent all damage that would be dealt to Uncle Istvan by creatures.').
card_image_name('uncle istvan'/'TSB', 'uncle istvan').
card_uid('uncle istvan'/'TSB', 'TSB:Uncle Istvan:uncle istvan').
card_rarity('uncle istvan'/'TSB', 'Special').
card_artist('uncle istvan'/'TSB', 'Daniel Gelon').
card_number('uncle istvan'/'TSB', '51').
card_flavor_text('uncle istvan'/'TSB', 'Solitude drove the old hermit insane. Now he only keeps company with those he can catch.').
card_multiverse_id('uncle istvan'/'TSB', '108805').
card_timeshifted('uncle istvan'/'TSB')
.
card_in_set('undead warchief', 'TSB').
card_original_type('undead warchief'/'TSB', 'Creature — Zombie').
card_original_text('undead warchief'/'TSB', 'Zombie spells you play cost {1} less to play.\nZombies you control get +2/+1.').
card_image_name('undead warchief'/'TSB', 'undead warchief').
card_uid('undead warchief'/'TSB', 'TSB:Undead Warchief:undead warchief').
card_rarity('undead warchief'/'TSB', 'Special').
card_artist('undead warchief'/'TSB', 'Greg Hildebrandt').
card_number('undead warchief'/'TSB', '52').
card_flavor_text('undead warchief'/'TSB', 'It has the strength of seven men. In fact, it used to be seven men.').
card_multiverse_id('undead warchief'/'TSB', '108869').
card_timeshifted('undead warchief'/'TSB')
.
card_in_set('undertaker', 'TSB').
card_original_type('undertaker'/'TSB', 'Creature — Human Spellshaper').
card_original_text('undertaker'/'TSB', '{B}, {T}, Discard a card: Return target creature card from your graveyard to your hand.').
card_image_name('undertaker'/'TSB', 'undertaker').
card_uid('undertaker'/'TSB', 'TSB:Undertaker:undertaker').
card_rarity('undertaker'/'TSB', 'Special').
card_artist('undertaker'/'TSB', 'Jeff Easley').
card_number('undertaker'/'TSB', '53').
card_flavor_text('undertaker'/'TSB', 'The weight of death is heavy but not immovable.').
card_multiverse_id('undertaker'/'TSB', '108791').
card_timeshifted('undertaker'/'TSB')
.
card_in_set('unstable mutation', 'TSB').
card_original_type('unstable mutation'/'TSB', 'Enchantment — Aura').
card_original_text('unstable mutation'/'TSB', 'Enchant creature\nEnchanted creature gets +3/+3.\nAt the beginning of the upkeep of enchanted creature\'s controller, put a -1/-1 counter on that creature.').
card_image_name('unstable mutation'/'TSB', 'unstable mutation').
card_uid('unstable mutation'/'TSB', 'TSB:Unstable Mutation:unstable mutation').
card_rarity('unstable mutation'/'TSB', 'Special').
card_artist('unstable mutation'/'TSB', 'Douglas Shuler').
card_number('unstable mutation'/'TSB', '33').
card_multiverse_id('unstable mutation'/'TSB', '108827').
card_timeshifted('unstable mutation'/'TSB')
.
card_in_set('uthden troll', 'TSB').
card_original_type('uthden troll'/'TSB', 'Creature — Troll').
card_original_text('uthden troll'/'TSB', '{R}: Regenerate Uthden Troll.').
card_image_name('uthden troll'/'TSB', 'uthden troll').
card_uid('uthden troll'/'TSB', 'TSB:Uthden Troll:uthden troll').
card_rarity('uthden troll'/'TSB', 'Special').
card_artist('uthden troll'/'TSB', 'Douglas Shuler').
card_number('uthden troll'/'TSB', '71').
card_flavor_text('uthden troll'/'TSB', '\"Oi oi oi, me gotta hurt in \'ere,\nOi oi oi, me smell a ting is near,\nGonna bosh \'n gonna nosh \'n da hurt\'ll disappear.\"\n—Troll chant').
card_multiverse_id('uthden troll'/'TSB', '108806').
card_timeshifted('uthden troll'/'TSB')
.
card_in_set('valor', 'TSB').
card_original_type('valor'/'TSB', 'Creature — Incarnation').
card_original_text('valor'/'TSB', 'First strike\nAs long as Valor is in your graveyard and you control a Plains, creatures you control have first strike.').
card_image_name('valor'/'TSB', 'valor').
card_uid('valor'/'TSB', 'TSB:Valor:valor').
card_rarity('valor'/'TSB', 'Special').
card_artist('valor'/'TSB', 'Kev Walker').
card_number('valor'/'TSB', '16').
card_flavor_text('valor'/'TSB', '\"The cracks widened, the shell crumbled, and Valor spread throughout the land.\"\n—Scroll of Beginnings').
card_multiverse_id('valor'/'TSB', '106628').
card_timeshifted('valor'/'TSB')
.
card_in_set('verdeloth the ancient', 'TSB').
card_original_type('verdeloth the ancient'/'TSB', 'Legendary Creature — Treefolk').
card_original_text('verdeloth the ancient'/'TSB', 'Kicker {X} (You may pay an additional {X} as you play this spell.)\nAll other Treefolk and all Saprolings get +1/+1.\nWhen Verdeloth the Ancient comes into play, if the kicker cost was paid, put X 1/1 green Saproling creature tokens into play.').
card_image_name('verdeloth the ancient'/'TSB', 'verdeloth the ancient').
card_uid('verdeloth the ancient'/'TSB', 'TSB:Verdeloth the Ancient:verdeloth the ancient').
card_rarity('verdeloth the ancient'/'TSB', 'Special').
card_artist('verdeloth the ancient'/'TSB', 'Daren Bader').
card_number('verdeloth the ancient'/'TSB', '88').
card_multiverse_id('verdeloth the ancient'/'TSB', '109688').
card_timeshifted('verdeloth the ancient'/'TSB')
.
card_in_set('vhati il-dal', 'TSB').
card_original_type('vhati il-dal'/'TSB', 'Legendary Creature — Human Warrior').
card_original_text('vhati il-dal'/'TSB', '{T}: Target creature\'s power or toughness becomes 1 until end of turn.').
card_image_name('vhati il-dal'/'TSB', 'vhati il-dal').
card_uid('vhati il-dal'/'TSB', 'TSB:Vhati il-Dal:vhati il-dal').
card_rarity('vhati il-dal'/'TSB', 'Special').
card_artist('vhati il-dal'/'TSB', 'Ron Spencer').
card_number('vhati il-dal'/'TSB', '104').
card_flavor_text('vhati il-dal'/'TSB', '\"Sir, I just thought . . . ,\" explained Vhati.\n\"Don\'t think,\" interrupted Greven. \"It doesn\'t suit you.\"').
card_multiverse_id('vhati il-dal'/'TSB', '109759').
card_timeshifted('vhati il-dal'/'TSB')
.
card_in_set('void', 'TSB').
card_original_type('void'/'TSB', 'Sorcery').
card_original_text('void'/'TSB', 'Choose a number. Destroy all artifacts and creatures with converted mana cost equal to that number. Then target player reveals his or her hand and discards all nonland cards with converted mana cost equal to the number.').
card_image_name('void'/'TSB', 'void').
card_uid('void'/'TSB', 'TSB:Void:void').
card_rarity('void'/'TSB', 'Special').
card_artist('void'/'TSB', 'Kev Walker').
card_number('void'/'TSB', '105').
card_multiverse_id('void'/'TSB', '109677').
card_timeshifted('void'/'TSB')
.
card_in_set('voidmage prodigy', 'TSB').
card_original_type('voidmage prodigy'/'TSB', 'Creature — Human Wizard').
card_original_text('voidmage prodigy'/'TSB', '{U}{U}, Sacrifice a Wizard: Counter target spell.\nMorph {U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_image_name('voidmage prodigy'/'TSB', 'voidmage prodigy').
card_uid('voidmage prodigy'/'TSB', 'TSB:Voidmage Prodigy:voidmage prodigy').
card_rarity('voidmage prodigy'/'TSB', 'Special').
card_artist('voidmage prodigy'/'TSB', 'Christopher Moeller').
card_number('voidmage prodigy'/'TSB', '34').
card_multiverse_id('voidmage prodigy'/'TSB', '108811').
card_timeshifted('voidmage prodigy'/'TSB')
.
card_in_set('wall of roots', 'TSB').
card_original_type('wall of roots'/'TSB', 'Creature — Plant Wall').
card_original_text('wall of roots'/'TSB', 'Defender\nPut a -0/-1 counter on Wall of Roots: Add {G} to your mana pool. Play this ability only once each turn.').
card_image_name('wall of roots'/'TSB', 'wall of roots').
card_uid('wall of roots'/'TSB', 'TSB:Wall of Roots:wall of roots').
card_rarity('wall of roots'/'TSB', 'Special').
card_artist('wall of roots'/'TSB', 'John Matson').
card_number('wall of roots'/'TSB', '89').
card_flavor_text('wall of roots'/'TSB', 'Sometimes the wise ones wove their magic into living plants; as the plant grew, so grew the magic.').
card_multiverse_id('wall of roots'/'TSB', '106662').
card_timeshifted('wall of roots'/'TSB')
.
card_in_set('war barge', 'TSB').
card_original_type('war barge'/'TSB', 'Artifact').
card_original_text('war barge'/'TSB', '{3}: Target creature gains islandwalk until end of turn. When War Barge leaves play this turn, destroy that creature. A creature destroyed this way can\'t be regenerated.').
card_image_name('war barge'/'TSB', 'war barge').
card_uid('war barge'/'TSB', 'TSB:War Barge:war barge').
card_rarity('war barge'/'TSB', 'Special').
card_artist('war barge'/'TSB', 'Tom Wänerstrand').
card_number('war barge'/'TSB', '116').
card_multiverse_id('war barge'/'TSB', '109757').
card_timeshifted('war barge'/'TSB')
.
card_in_set('whirling dervish', 'TSB').
card_original_type('whirling dervish'/'TSB', 'Creature — Human Monk').
card_original_text('whirling dervish'/'TSB', 'Protection from black\nAt end of turn, if Whirling Dervish dealt damage to an opponent this turn, put a +1/+1 counter on it.').
card_image_name('whirling dervish'/'TSB', 'whirling dervish').
card_uid('whirling dervish'/'TSB', 'TSB:Whirling Dervish:whirling dervish').
card_rarity('whirling dervish'/'TSB', 'Special').
card_artist('whirling dervish'/'TSB', 'Susan Van Camp').
card_number('whirling dervish'/'TSB', '90').
card_multiverse_id('whirling dervish'/'TSB', '109683').
card_timeshifted('whirling dervish'/'TSB')
.
card_in_set('whispers of the muse', 'TSB').
card_original_type('whispers of the muse'/'TSB', 'Instant').
card_original_text('whispers of the muse'/'TSB', 'Buyback {5} (You may pay an additional {5} as you play this spell. If you do, put this card into your hand as it resolves.)\nDraw a card.').
card_image_name('whispers of the muse'/'TSB', 'whispers of the muse').
card_uid('whispers of the muse'/'TSB', 'TSB:Whispers of the Muse:whispers of the muse').
card_rarity('whispers of the muse'/'TSB', 'Special').
card_artist('whispers of the muse'/'TSB', 'Quinton Hoover').
card_number('whispers of the muse'/'TSB', '35').
card_flavor_text('whispers of the muse'/'TSB', '\"I followed her song only to find it was a dirge.\"\n—Crovax').
card_multiverse_id('whispers of the muse'/'TSB', '106651').
card_timeshifted('whispers of the muse'/'TSB')
.
card_in_set('wildfire emissary', 'TSB').
card_original_type('wildfire emissary'/'TSB', 'Creature — Efreet').
card_original_text('wildfire emissary'/'TSB', 'Protection from white\n{1}{R}: Wildfire Emissary gets +1/+0 until end of turn.').
card_image_name('wildfire emissary'/'TSB', 'wildfire emissary').
card_uid('wildfire emissary'/'TSB', 'TSB:Wildfire Emissary:wildfire emissary').
card_rarity('wildfire emissary'/'TSB', 'Special').
card_artist('wildfire emissary'/'TSB', 'Richard Kane Ferguson').
card_number('wildfire emissary'/'TSB', '72').
card_flavor_text('wildfire emissary'/'TSB', '\"The efreet is a striding storm with a voice that crackles like fire.\"\n—Qhattib, vizier of Amiqat').
card_multiverse_id('wildfire emissary'/'TSB', '108813').
card_timeshifted('wildfire emissary'/'TSB')
.
card_in_set('willbender', 'TSB').
card_original_type('willbender'/'TSB', 'Creature — Human Wizard').
card_original_text('willbender'/'TSB', 'Morph {1}{U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Willbender is turned face up, change the target of target spell or ability with a single target.').
card_image_name('willbender'/'TSB', 'willbender').
card_uid('willbender'/'TSB', 'TSB:Willbender:willbender').
card_rarity('willbender'/'TSB', 'Special').
card_artist('willbender'/'TSB', 'Eric Peterson').
card_number('willbender'/'TSB', '36').
card_multiverse_id('willbender'/'TSB', '108861').
card_timeshifted('willbender'/'TSB')
.
card_in_set('witch hunter', 'TSB').
card_original_type('witch hunter'/'TSB', 'Creature — Human Cleric').
card_original_text('witch hunter'/'TSB', '{T}: Witch Hunter deals 1 damage to target player.\n{1}{W}{W}, {T}: Return target creature an opponent controls to its owner\'s hand.').
card_image_name('witch hunter'/'TSB', 'witch hunter').
card_uid('witch hunter'/'TSB', 'TSB:Witch Hunter:witch hunter').
card_rarity('witch hunter'/'TSB', 'Special').
card_artist('witch hunter'/'TSB', 'Jesper Myrfors').
card_number('witch hunter'/'TSB', '17').
card_multiverse_id('witch hunter'/'TSB', '108794').
card_timeshifted('witch hunter'/'TSB')
.
card_in_set('withered wretch', 'TSB').
card_original_type('withered wretch'/'TSB', 'Creature — Zombie Cleric').
card_original_text('withered wretch'/'TSB', '{1}: Remove target card in a graveyard from the game.').
card_image_name('withered wretch'/'TSB', 'withered wretch').
card_uid('withered wretch'/'TSB', 'TSB:Withered Wretch:withered wretch').
card_rarity('withered wretch'/'TSB', 'Special').
card_artist('withered wretch'/'TSB', 'Tim Hildebrandt').
card_number('withered wretch'/'TSB', '54').
card_flavor_text('withered wretch'/'TSB', 'Once it consecrated the dead. Now it desecrates them.').
card_multiverse_id('withered wretch'/'TSB', '108890').
card_timeshifted('withered wretch'/'TSB')
.
card_in_set('zhalfirin commander', 'TSB').
card_original_type('zhalfirin commander'/'TSB', 'Creature — Human Knight').
card_original_text('zhalfirin commander'/'TSB', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)\n{1}{W}{W}: Target Knight gets +1/+1 until end of turn.').
card_image_name('zhalfirin commander'/'TSB', 'zhalfirin commander').
card_uid('zhalfirin commander'/'TSB', 'TSB:Zhalfirin Commander:zhalfirin commander').
card_rarity('zhalfirin commander'/'TSB', 'Special').
card_artist('zhalfirin commander'/'TSB', 'Stuart Griffin').
card_number('zhalfirin commander'/'TSB', '18').
card_flavor_text('zhalfirin commander'/'TSB', '\"Command is the act of balancing on a vine and convincing others that it is firm ground.\"\n—Sidar Jabari').
card_multiverse_id('zhalfirin commander'/'TSB', '108868').
card_timeshifted('zhalfirin commander'/'TSB')
.