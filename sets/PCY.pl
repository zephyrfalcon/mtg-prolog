% Prophecy

set('PCY').
set_name('PCY', 'Prophecy').
set_release_date('PCY', '2000-06-05').
set_border('PCY', 'black').
set_type('PCY', 'expansion').
set_block('PCY', 'Masques').

card_in_set('abolish', 'PCY').
card_original_type('abolish'/'PCY', 'Instant').
card_original_text('abolish'/'PCY', 'You may discard a plains from your hand instead of paying Abolish\'s mana cost.\nDestroy target artifact or enchantment.').
card_first_print('abolish', 'PCY').
card_image_name('abolish'/'PCY', 'abolish').
card_uid('abolish'/'PCY', 'PCY:Abolish:abolish').
card_rarity('abolish'/'PCY', 'Uncommon').
card_artist('abolish'/'PCY', 'Kev Walker').
card_number('abolish'/'PCY', '1').
card_flavor_text('abolish'/'PCY', 'As war raged on, the young mages became more direct in their tactics, often at great cost.').
card_multiverse_id('abolish'/'PCY', '24561').

card_in_set('agent of shauku', 'PCY').
card_original_type('agent of shauku'/'PCY', 'Creature — Mercenary').
card_original_text('agent of shauku'/'PCY', '{1}{B}, Sacrifice a land: Target creature gets +2/+0 until end of turn.').
card_first_print('agent of shauku', 'PCY').
card_image_name('agent of shauku'/'PCY', 'agent of shauku').
card_uid('agent of shauku'/'PCY', 'PCY:Agent of Shauku:agent of shauku').
card_rarity('agent of shauku'/'PCY', 'Common').
card_artist('agent of shauku'/'PCY', 'Donato Giancola').
card_number('agent of shauku'/'PCY', '55').
card_flavor_text('agent of shauku'/'PCY', '\"Go ahead. Take it. You\'ll be so very powerful . . . and what harm can it do?\"').
card_multiverse_id('agent of shauku'/'PCY', '24601').

card_in_set('alexi\'s cloak', 'PCY').
card_original_type('alexi\'s cloak'/'PCY', 'Enchant Creature').
card_original_text('alexi\'s cloak'/'PCY', 'You may play Alexi\'s Cloak any time you could play an instant.\nEnchanted creature can\'t be the target of spells or abilities.').
card_first_print('alexi\'s cloak', 'PCY').
card_image_name('alexi\'s cloak'/'PCY', 'alexi\'s cloak').
card_uid('alexi\'s cloak'/'PCY', 'PCY:Alexi\'s Cloak:alexi\'s cloak').
card_rarity('alexi\'s cloak'/'PCY', 'Common').
card_artist('alexi\'s cloak'/'PCY', 'Alan Rabinowitz').
card_number('alexi\'s cloak'/'PCY', '29').
card_flavor_text('alexi\'s cloak'/'PCY', '\"Doesn\'t look like much, does it?\"\n—Alexi, zephyr mage').
card_multiverse_id('alexi\'s cloak'/'PCY', '24579').

card_in_set('alexi, zephyr mage', 'PCY').
card_original_type('alexi, zephyr mage'/'PCY', 'Creature — Spellshaper Legend').
card_original_text('alexi, zephyr mage'/'PCY', '{X}{U}, {T}, Discard two cards from your hand: Return X target creatures to their owners\' hands.').
card_first_print('alexi, zephyr mage', 'PCY').
card_image_name('alexi, zephyr mage'/'PCY', 'alexi, zephyr mage').
card_uid('alexi, zephyr mage'/'PCY', 'PCY:Alexi, Zephyr Mage:alexi, zephyr mage').
card_rarity('alexi, zephyr mage'/'PCY', 'Rare').
card_artist('alexi, zephyr mage'/'PCY', 'Mark Zug').
card_number('alexi, zephyr mage'/'PCY', '28').
card_flavor_text('alexi, zephyr mage'/'PCY', 'The Keldons didn\'t know her name, but they still cursed her in battle.').
card_multiverse_id('alexi, zephyr mage'/'PCY', '24591').

card_in_set('aura fracture', 'PCY').
card_original_type('aura fracture'/'PCY', 'Enchantment').
card_original_text('aura fracture'/'PCY', 'Sacrifice a land: Destroy target enchantment.').
card_first_print('aura fracture', 'PCY').
card_image_name('aura fracture'/'PCY', 'aura fracture').
card_uid('aura fracture'/'PCY', 'PCY:Aura Fracture:aura fracture').
card_rarity('aura fracture'/'PCY', 'Common').
card_artist('aura fracture'/'PCY', 'Rebecca Guay').
card_number('aura fracture'/'PCY', '2').
card_flavor_text('aura fracture'/'PCY', 'Jamuraa\'s rhystic mages treat magic as their garden, uprooting unwanted spells as bothersome weeds.').
card_multiverse_id('aura fracture'/'PCY', '24560').

card_in_set('avatar of fury', 'PCY').
card_original_type('avatar of fury'/'PCY', 'Creature — Avatar').
card_original_text('avatar of fury'/'PCY', 'Flying\nIf an opponent controls seven or more lands, Avatar of Fury costs {6} less to play.\n{R} Avatar of Fury gets +1/+0 until end of turn.').
card_first_print('avatar of fury', 'PCY').
card_image_name('avatar of fury'/'PCY', 'avatar of fury').
card_uid('avatar of fury'/'PCY', 'PCY:Avatar of Fury:avatar of fury').
card_rarity('avatar of fury'/'PCY', 'Rare').
card_artist('avatar of fury'/'PCY', 'rk post').
card_number('avatar of fury'/'PCY', '82').
card_multiverse_id('avatar of fury'/'PCY', '24644').

card_in_set('avatar of hope', 'PCY').
card_original_type('avatar of hope'/'PCY', 'Creature — Avatar').
card_original_text('avatar of hope'/'PCY', 'Flying\nIf you have 3 life or less, Avatar of Hope costs {6} less to play.\nAvatar of Hope may block any number of creatures.').
card_image_name('avatar of hope'/'PCY', 'avatar of hope').
card_uid('avatar of hope'/'PCY', 'PCY:Avatar of Hope:avatar of hope').
card_rarity('avatar of hope'/'PCY', 'Rare').
card_artist('avatar of hope'/'PCY', 'rk post').
card_number('avatar of hope'/'PCY', '3').
card_multiverse_id('avatar of hope'/'PCY', '24564').

card_in_set('avatar of might', 'PCY').
card_original_type('avatar of might'/'PCY', 'Creature — Avatar').
card_original_text('avatar of might'/'PCY', 'Trample\nIf an opponent controls at least four more creatures than you, Avatar of Might costs {6} less to play.').
card_first_print('avatar of might', 'PCY').
card_image_name('avatar of might'/'PCY', 'avatar of might').
card_uid('avatar of might'/'PCY', 'PCY:Avatar of Might:avatar of might').
card_rarity('avatar of might'/'PCY', 'Rare').
card_artist('avatar of might'/'PCY', 'rk post').
card_number('avatar of might'/'PCY', '109').
card_multiverse_id('avatar of might'/'PCY', '24671').

card_in_set('avatar of will', 'PCY').
card_original_type('avatar of will'/'PCY', 'Creature — Avatar').
card_original_text('avatar of will'/'PCY', 'Flying\nIf an opponent has no cards in hand, Avatar of Will costs {6} less to play.').
card_first_print('avatar of will', 'PCY').
card_image_name('avatar of will'/'PCY', 'avatar of will').
card_uid('avatar of will'/'PCY', 'PCY:Avatar of Will:avatar of will').
card_rarity('avatar of will'/'PCY', 'Rare').
card_artist('avatar of will'/'PCY', 'rk post').
card_number('avatar of will'/'PCY', '30').
card_multiverse_id('avatar of will'/'PCY', '24590').

card_in_set('avatar of woe', 'PCY').
card_original_type('avatar of woe'/'PCY', 'Creature — Avatar').
card_original_text('avatar of woe'/'PCY', 'If there are ten or more creature cards total in all graveyards, Avatar of Woe costs {6} less to play.\nAvatar of Woe can\'t be blocked except by artifact creatures and/or black creatures.\n{T}: Destroy target creature. It can\'t be regenerated.').
card_first_print('avatar of woe', 'PCY').
card_image_name('avatar of woe'/'PCY', 'avatar of woe').
card_uid('avatar of woe'/'PCY', 'PCY:Avatar of Woe:avatar of woe').
card_rarity('avatar of woe'/'PCY', 'Rare').
card_artist('avatar of woe'/'PCY', 'rk post').
card_number('avatar of woe'/'PCY', '56').
card_multiverse_id('avatar of woe'/'PCY', '24621').

card_in_set('barbed field', 'PCY').
card_original_type('barbed field'/'PCY', 'Enchant Land').
card_original_text('barbed field'/'PCY', 'Enchanted land has \"{T}: This land deals 1 damage to target creature or player.\"').
card_first_print('barbed field', 'PCY').
card_image_name('barbed field'/'PCY', 'barbed field').
card_uid('barbed field'/'PCY', 'PCY:Barbed Field:barbed field').
card_rarity('barbed field'/'PCY', 'Uncommon').
card_artist('barbed field'/'PCY', 'Carl Critchlow').
card_number('barbed field'/'PCY', '83').
card_flavor_text('barbed field'/'PCY', 'Wherever they went, the Keldons transformed the land into a weapon.').
card_multiverse_id('barbed field'/'PCY', '24634').

card_in_set('blessed wind', 'PCY').
card_original_type('blessed wind'/'PCY', 'Sorcery').
card_original_text('blessed wind'/'PCY', 'Target player\'s life total becomes 20.').
card_first_print('blessed wind', 'PCY').
card_image_name('blessed wind'/'PCY', 'blessed wind').
card_uid('blessed wind'/'PCY', 'PCY:Blessed Wind:blessed wind').
card_rarity('blessed wind'/'PCY', 'Rare').
card_artist('blessed wind'/'PCY', 'Anthony S. Waters').
card_number('blessed wind'/'PCY', '4').
card_flavor_text('blessed wind'/'PCY', '\"The fourth wind of ascension is Anointer, deifying the worthy.\"\n—Keld Triumphant').
card_multiverse_id('blessed wind'/'PCY', '24569').

card_in_set('bog elemental', 'PCY').
card_original_type('bog elemental'/'PCY', 'Creature — Elemental').
card_original_text('bog elemental'/'PCY', 'Protection from white\nAt the beginning of your upkeep, sacrifice Bog Elemental unless you sacrifice a land.').
card_first_print('bog elemental', 'PCY').
card_image_name('bog elemental'/'PCY', 'bog elemental').
card_uid('bog elemental'/'PCY', 'PCY:Bog Elemental:bog elemental').
card_rarity('bog elemental'/'PCY', 'Rare').
card_artist('bog elemental'/'PCY', 'Glen Angus').
card_number('bog elemental'/'PCY', '57').
card_flavor_text('bog elemental'/'PCY', '\"When the bog walks, the wise run.\"\n—Erissa, bog witch').
card_multiverse_id('bog elemental'/'PCY', '24620').

card_in_set('bog glider', 'PCY').
card_original_type('bog glider'/'PCY', 'Creature — Mercenary').
card_original_text('bog glider'/'PCY', 'Flying\n{T}, Sacrifice a land: Search your library for a Mercenary card with converted mana cost 2 or less and put that card into play. Then shuffle your library.').
card_first_print('bog glider', 'PCY').
card_image_name('bog glider'/'PCY', 'bog glider').
card_uid('bog glider'/'PCY', 'PCY:Bog Glider:bog glider').
card_rarity('bog glider'/'PCY', 'Common').
card_artist('bog glider'/'PCY', 'Brian Snõddy').
card_number('bog glider'/'PCY', '58').
card_multiverse_id('bog glider'/'PCY', '24600').

card_in_set('branded brawlers', 'PCY').
card_original_type('branded brawlers'/'PCY', 'Creature — Soldier').
card_original_text('branded brawlers'/'PCY', 'Branded Brawlers can\'t attack if defending player controls an untapped land.\nBranded Brawlers can\'t block if you control an untapped land.').
card_first_print('branded brawlers', 'PCY').
card_image_name('branded brawlers'/'PCY', 'branded brawlers').
card_uid('branded brawlers'/'PCY', 'PCY:Branded Brawlers:branded brawlers').
card_rarity('branded brawlers'/'PCY', 'Common').
card_artist('branded brawlers'/'PCY', 'Scott M. Fischer').
card_number('branded brawlers'/'PCY', '84').
card_flavor_text('branded brawlers'/'PCY', '\"The sword or the lash. Choose.\"\n—Akul, Keldon whip sergeant').
card_multiverse_id('branded brawlers'/'PCY', '24638').

card_in_set('brutal suppression', 'PCY').
card_original_type('brutal suppression'/'PCY', 'Enchantment').
card_original_text('brutal suppression'/'PCY', 'Activated abilities on Rebel cards cost an additional \"Sacrifice a land\" to play.').
card_first_print('brutal suppression', 'PCY').
card_image_name('brutal suppression'/'PCY', 'brutal suppression').
card_uid('brutal suppression'/'PCY', 'PCY:Brutal Suppression:brutal suppression').
card_rarity('brutal suppression'/'PCY', 'Uncommon').
card_artist('brutal suppression'/'PCY', 'Val Mayerik').
card_number('brutal suppression'/'PCY', '85').
card_flavor_text('brutal suppression'/'PCY', '\"They no longer care about their own suffering. Show them something worse.\"\n—Latulla, Keldon overseer').
card_multiverse_id('brutal suppression'/'PCY', '24635').

card_in_set('calming verse', 'PCY').
card_original_type('calming verse'/'PCY', 'Sorcery').
card_original_text('calming verse'/'PCY', 'Destroy all enchantments you don\'t control. Then, if you control an untapped land, destroy all enchantments you control.').
card_first_print('calming verse', 'PCY').
card_image_name('calming verse'/'PCY', 'calming verse').
card_uid('calming verse'/'PCY', 'PCY:Calming Verse:calming verse').
card_rarity('calming verse'/'PCY', 'Common').
card_artist('calming verse'/'PCY', 'Rebecca Guay').
card_number('calming verse'/'PCY', '110').
card_flavor_text('calming verse'/'PCY', 'The chattering forest fell silent as the otherworldly song began.').
card_multiverse_id('calming verse'/'PCY', '24659').

card_in_set('celestial convergence', 'PCY').
card_original_type('celestial convergence'/'PCY', 'Enchantment').
card_original_text('celestial convergence'/'PCY', 'Celestial Convergence comes into play with seven omen counters on it.\nAt the beginning of your upkeep, remove an omen counter from Celestial Convergence. If there are no omen counters on Celestial Convergence, the player with the highest life total wins the game. If two or more players are tied for highest life total, the game is a draw.').
card_first_print('celestial convergence', 'PCY').
card_image_name('celestial convergence'/'PCY', 'celestial convergence').
card_uid('celestial convergence'/'PCY', 'PCY:Celestial Convergence:celestial convergence').
card_rarity('celestial convergence'/'PCY', 'Rare').
card_artist('celestial convergence'/'PCY', 'Ray Lago').
card_number('celestial convergence'/'PCY', '5').
card_multiverse_id('celestial convergence'/'PCY', '24567').

card_in_set('chilling apparition', 'PCY').
card_original_type('chilling apparition'/'PCY', 'Creature — Ghost').
card_original_text('chilling apparition'/'PCY', '{B} Regenerate Chilling Apparition.\nWhenever Chilling Apparition deals combat damage to a player, that player discards a card from his or her hand.').
card_first_print('chilling apparition', 'PCY').
card_image_name('chilling apparition'/'PCY', 'chilling apparition').
card_uid('chilling apparition'/'PCY', 'PCY:Chilling Apparition:chilling apparition').
card_rarity('chilling apparition'/'PCY', 'Uncommon').
card_artist('chilling apparition'/'PCY', 'Ron Spears').
card_number('chilling apparition'/'PCY', '59').
card_flavor_text('chilling apparition'/'PCY', 'Mortal minds shrivel at its touch.').
card_multiverse_id('chilling apparition'/'PCY', '25362').

card_in_set('chimeric idol', 'PCY').
card_original_type('chimeric idol'/'PCY', 'Artifact').
card_original_text('chimeric idol'/'PCY', '{0}: Tap all lands you control. Chimeric Idol becomes a 3/3 artifact creature until end of turn.').
card_first_print('chimeric idol', 'PCY').
card_image_name('chimeric idol'/'PCY', 'chimeric idol').
card_uid('chimeric idol'/'PCY', 'PCY:Chimeric Idol:chimeric idol').
card_rarity('chimeric idol'/'PCY', 'Uncommon').
card_artist('chimeric idol'/'PCY', 'Mark Tedin').
card_number('chimeric idol'/'PCY', '136').
card_flavor_text('chimeric idol'/'PCY', 'After a chimeric idol attacked them, the Keldons smashed all unfamiliar statues.').
card_multiverse_id('chimeric idol'/'PCY', '24680').

card_in_set('citadel of pain', 'PCY').
card_original_type('citadel of pain'/'PCY', 'Enchantment').
card_original_text('citadel of pain'/'PCY', 'At the end of each player\'s turn, Citadel of Pain deals X damage to that player, where X is the number of untapped lands he or she controls.').
card_first_print('citadel of pain', 'PCY').
card_image_name('citadel of pain'/'PCY', 'citadel of pain').
card_uid('citadel of pain'/'PCY', 'PCY:Citadel of Pain:citadel of pain').
card_rarity('citadel of pain'/'PCY', 'Uncommon').
card_artist('citadel of pain'/'PCY', 'Darrell Riche').
card_number('citadel of pain'/'PCY', '86').
card_flavor_text('citadel of pain'/'PCY', '\"Battle is our religion. This fortress is our temple.\"\n—Latulla, Keldon overseer').
card_multiverse_id('citadel of pain'/'PCY', '24643').

card_in_set('coastal hornclaw', 'PCY').
card_original_type('coastal hornclaw'/'PCY', 'Creature — Bird').
card_original_text('coastal hornclaw'/'PCY', 'Sacrifice a land: Coastal Hornclaw gains flying until end of turn.').
card_first_print('coastal hornclaw', 'PCY').
card_image_name('coastal hornclaw'/'PCY', 'coastal hornclaw').
card_uid('coastal hornclaw'/'PCY', 'PCY:Coastal Hornclaw:coastal hornclaw').
card_rarity('coastal hornclaw'/'PCY', 'Common').
card_artist('coastal hornclaw'/'PCY', 'DiTerlizzi').
card_number('coastal hornclaw'/'PCY', '31').
card_flavor_text('coastal hornclaw'/'PCY', 'Jamuraan farmers fear three things above all else: droughts, floods, and hornclaws.').
card_multiverse_id('coastal hornclaw'/'PCY', '24574').

card_in_set('coffin puppets', 'PCY').
card_original_type('coffin puppets'/'PCY', 'Creature — Zombie').
card_original_text('coffin puppets'/'PCY', 'Sacrifice two lands: Return Coffin Puppets to play. Play this ability only during your upkeep, only if Coffin Puppets is in your graveyard, and only if you control a swamp.').
card_first_print('coffin puppets', 'PCY').
card_image_name('coffin puppets'/'PCY', 'coffin puppets').
card_uid('coffin puppets'/'PCY', 'PCY:Coffin Puppets:coffin puppets').
card_rarity('coffin puppets'/'PCY', 'Rare').
card_artist('coffin puppets'/'PCY', 'Arnie Swekel').
card_number('coffin puppets'/'PCY', '60').
card_flavor_text('coffin puppets'/'PCY', 'The ground won\'t accept them.').
card_multiverse_id('coffin puppets'/'PCY', '24618').

card_in_set('copper-leaf angel', 'PCY').
card_original_type('copper-leaf angel'/'PCY', 'Artifact Creature — Angel').
card_original_text('copper-leaf angel'/'PCY', 'Flying\n{T}, Sacrifice X lands: Put X +1/+1 counters on Copper-Leaf Angel.').
card_first_print('copper-leaf angel', 'PCY').
card_image_name('copper-leaf angel'/'PCY', 'copper-leaf angel').
card_uid('copper-leaf angel'/'PCY', 'PCY:Copper-Leaf Angel:copper-leaf angel').
card_rarity('copper-leaf angel'/'PCY', 'Rare').
card_artist('copper-leaf angel'/'PCY', 'Christopher Moeller').
card_number('copper-leaf angel'/'PCY', '137').
card_flavor_text('copper-leaf angel'/'PCY', '\"When Serra made angels, people called her a goddess. My angels are far superior to hers.\"\n—Latulla, Keldon overseer').
card_multiverse_id('copper-leaf angel'/'PCY', '24682').

card_in_set('darba', 'PCY').
card_original_type('darba'/'PCY', 'Creature — Beast').
card_original_text('darba'/'PCY', 'At the beginning of your upkeep, sacrifice Darba unless you pay {G}{G}.').
card_first_print('darba', 'PCY').
card_image_name('darba'/'PCY', 'darba').
card_uid('darba'/'PCY', 'PCY:Darba:darba').
card_rarity('darba'/'PCY', 'Uncommon').
card_artist('darba'/'PCY', 'Heather Hudson').
card_number('darba'/'PCY', '111').
card_flavor_text('darba'/'PCY', 'Legend has it the darba gave up its wings in return for an extra stomach.').
card_multiverse_id('darba'/'PCY', '24657').

card_in_set('death charmer', 'PCY').
card_original_type('death charmer'/'PCY', 'Creature — Mercenary').
card_original_text('death charmer'/'PCY', 'Whenever Death Charmer deals combat damage to a creature, that creature\'s controller loses 2 life unless he or she pays {2}.').
card_first_print('death charmer', 'PCY').
card_image_name('death charmer'/'PCY', 'death charmer').
card_uid('death charmer'/'PCY', 'PCY:Death Charmer:death charmer').
card_rarity('death charmer'/'PCY', 'Common').
card_artist('death charmer'/'PCY', 'David Martin').
card_number('death charmer'/'PCY', '61').
card_flavor_text('death charmer'/'PCY', 'It vents the chill of the grave.').
card_multiverse_id('death charmer'/'PCY', '24599').

card_in_set('denying wind', 'PCY').
card_original_type('denying wind'/'PCY', 'Sorcery').
card_original_text('denying wind'/'PCY', 'Search target player\'s library for up to seven cards and remove them from the game. Then that player shuffles his or her library.').
card_first_print('denying wind', 'PCY').
card_image_name('denying wind'/'PCY', 'denying wind').
card_uid('denying wind'/'PCY', 'PCY:Denying Wind:denying wind').
card_rarity('denying wind'/'PCY', 'Rare').
card_artist('denying wind'/'PCY', 'Tony Szczudlo').
card_number('denying wind'/'PCY', '32').
card_flavor_text('denying wind'/'PCY', '\"The third wind of ascension is Eliminator, clearing Keld\'s path to victory.\"\n—Keld Triumphant').
card_multiverse_id('denying wind'/'PCY', '24596').

card_in_set('despoil', 'PCY').
card_original_type('despoil'/'PCY', 'Sorcery').
card_original_text('despoil'/'PCY', 'Destroy target land. Its controller loses 2 life.').
card_first_print('despoil', 'PCY').
card_image_name('despoil'/'PCY', 'despoil').
card_uid('despoil'/'PCY', 'PCY:Despoil:despoil').
card_rarity('despoil'/'PCY', 'Common').
card_artist('despoil'/'PCY', 'Scott M. Fischer').
card_number('despoil'/'PCY', '62').
card_flavor_text('despoil'/'PCY', 'Nothing could grow where the vile smoke had been, and no one could breathe it and live.').
card_multiverse_id('despoil'/'PCY', '24615').

card_in_set('devastate', 'PCY').
card_original_type('devastate'/'PCY', 'Sorcery').
card_original_text('devastate'/'PCY', 'Destroy target land. Devastate deals 1 damage to each creature and each player.').
card_first_print('devastate', 'PCY').
card_image_name('devastate'/'PCY', 'devastate').
card_uid('devastate'/'PCY', 'PCY:Devastate:devastate').
card_rarity('devastate'/'PCY', 'Common').
card_artist('devastate'/'PCY', 'Greg Staples').
card_number('devastate'/'PCY', '87').
card_flavor_text('devastate'/'PCY', '\"You\'re willing to die for your homeland. I\'m willing to kill for it.\"\n—Latulla, Keldon overseer').
card_multiverse_id('devastate'/'PCY', '24631').

card_in_set('diving griffin', 'PCY').
card_original_type('diving griffin'/'PCY', 'Creature — Griffin').
card_original_text('diving griffin'/'PCY', 'Flying\nAttacking doesn\'t cause Diving Griffin to tap.').
card_first_print('diving griffin', 'PCY').
card_image_name('diving griffin'/'PCY', 'diving griffin').
card_uid('diving griffin'/'PCY', 'PCY:Diving Griffin:diving griffin').
card_rarity('diving griffin'/'PCY', 'Common').
card_artist('diving griffin'/'PCY', 'John Howe').
card_number('diving griffin'/'PCY', '6').
card_flavor_text('diving griffin'/'PCY', 'To outrace the griffin\n—Kipamu expression meaning\n\"to do the impossible\"').
card_multiverse_id('diving griffin'/'PCY', '24548').

card_in_set('dual nature', 'PCY').
card_original_type('dual nature'/'PCY', 'Enchantment').
card_original_text('dual nature'/'PCY', 'Whenever a creature card comes into play, its controller puts a creature token into play that\'s a copy of that creature.\nWhenever a creature card leaves play, remove all tokens with the same name as that creature from the game.\nWhen Dual Nature leaves play, remove all tokens created with it from the game.').
card_first_print('dual nature', 'PCY').
card_image_name('dual nature'/'PCY', 'dual nature').
card_uid('dual nature'/'PCY', 'PCY:Dual Nature:dual nature').
card_rarity('dual nature'/'PCY', 'Rare').
card_artist('dual nature'/'PCY', 'Arnie Swekel').
card_number('dual nature'/'PCY', '112').
card_multiverse_id('dual nature'/'PCY', '24678').

card_in_set('elephant resurgence', 'PCY').
card_original_type('elephant resurgence'/'PCY', 'Sorcery').
card_original_text('elephant resurgence'/'PCY', 'Each player puts a green Elephant creature token into play. Those creatures have \"This creature\'s power and toughness are each equal to the number of creature cards in its controller\'s graveyard.\"').
card_first_print('elephant resurgence', 'PCY').
card_image_name('elephant resurgence'/'PCY', 'elephant resurgence').
card_uid('elephant resurgence'/'PCY', 'PCY:Elephant Resurgence:elephant resurgence').
card_rarity('elephant resurgence'/'PCY', 'Rare').
card_artist('elephant resurgence'/'PCY', 'DiTerlizzi').
card_number('elephant resurgence'/'PCY', '113').
card_multiverse_id('elephant resurgence'/'PCY', '21382').

card_in_set('endbringer\'s revel', 'PCY').
card_original_type('endbringer\'s revel'/'PCY', 'Enchantment').
card_original_text('endbringer\'s revel'/'PCY', '{4}: Return target creature card from a graveyard to its owner\'s hand. Any player may play this ability but only any time he or she could play a sorcery.').
card_first_print('endbringer\'s revel', 'PCY').
card_image_name('endbringer\'s revel'/'PCY', 'endbringer\'s revel').
card_uid('endbringer\'s revel'/'PCY', 'PCY:Endbringer\'s Revel:endbringer\'s revel').
card_rarity('endbringer\'s revel'/'PCY', 'Uncommon').
card_artist('endbringer\'s revel'/'PCY', 'Pete Venters').
card_number('endbringer\'s revel'/'PCY', '63').
card_flavor_text('endbringer\'s revel'/'PCY', 'First death. Then Shauku.').
card_multiverse_id('endbringer\'s revel'/'PCY', '24616').

card_in_set('entangler', 'PCY').
card_original_type('entangler'/'PCY', 'Enchant Creature').
card_original_text('entangler'/'PCY', 'Enchanted creature may block any number of creatures.').
card_first_print('entangler', 'PCY').
card_image_name('entangler'/'PCY', 'entangler').
card_uid('entangler'/'PCY', 'PCY:Entangler:entangler').
card_rarity('entangler'/'PCY', 'Uncommon').
card_artist('entangler'/'PCY', 'D. Alexander Gregory').
card_number('entangler'/'PCY', '7').
card_flavor_text('entangler'/'PCY', '\"Tell me again,\" said Latulla, raising her fist, \"how just one woman held back an entire Keldon platoon.\"').
card_multiverse_id('entangler'/'PCY', '24557').

card_in_set('excavation', 'PCY').
card_original_type('excavation'/'PCY', 'Enchantment').
card_original_text('excavation'/'PCY', '{1}, Sacrifice a land: Draw a card. Any player may play this ability.').
card_first_print('excavation', 'PCY').
card_image_name('excavation'/'PCY', 'excavation').
card_uid('excavation'/'PCY', 'PCY:Excavation:excavation').
card_rarity('excavation'/'PCY', 'Uncommon').
card_artist('excavation'/'PCY', 'Terese Nielsen').
card_number('excavation'/'PCY', '33').
card_flavor_text('excavation'/'PCY', '\"There are secrets out there begging to be rediscovered. We just need to look in the right places.\"\n—Teferi, planeswalker').
card_multiverse_id('excavation'/'PCY', '24586').

card_in_set('excise', 'PCY').
card_original_type('excise'/'PCY', 'Instant').
card_original_text('excise'/'PCY', 'Remove target attacking creature from the game unless its controller pays {X}.').
card_first_print('excise', 'PCY').
card_image_name('excise'/'PCY', 'excise').
card_uid('excise'/'PCY', 'PCY:Excise:excise').
card_rarity('excise'/'PCY', 'Common').
card_artist('excise'/'PCY', 'Joel Biske').
card_number('excise'/'PCY', '8').
card_flavor_text('excise'/'PCY', '\"Creation is no great feat. Anything you make, I can unmake in a heartbeat.\"\n—Alexi, zephyr mage').
card_multiverse_id('excise'/'PCY', '24553').

card_in_set('fault riders', 'PCY').
card_original_type('fault riders'/'PCY', 'Creature — Soldier').
card_original_text('fault riders'/'PCY', 'Sacrifice a land: Fault Riders gets +2/+0 and gains first strike until end of turn. Play this ability only once each turn.').
card_first_print('fault riders', 'PCY').
card_image_name('fault riders'/'PCY', 'fault riders').
card_uid('fault riders'/'PCY', 'PCY:Fault Riders:fault riders').
card_rarity('fault riders'/'PCY', 'Common').
card_artist('fault riders'/'PCY', 'Dave Dorman').
card_number('fault riders'/'PCY', '88').
card_flavor_text('fault riders'/'PCY', 'They turn geological upheaval into tactical advantage.').
card_multiverse_id('fault riders'/'PCY', '24626').

card_in_set('fen stalker', 'PCY').
card_original_type('fen stalker'/'PCY', 'Creature — Nightstalker').
card_original_text('fen stalker'/'PCY', 'Fen Stalker can\'t be blocked except by artifact creatures and/or black creatures as long as you control no untapped lands.').
card_first_print('fen stalker', 'PCY').
card_image_name('fen stalker'/'PCY', 'fen stalker').
card_uid('fen stalker'/'PCY', 'PCY:Fen Stalker:fen stalker').
card_rarity('fen stalker'/'PCY', 'Common').
card_artist('fen stalker'/'PCY', 'Edward P. Beard, Jr.').
card_number('fen stalker'/'PCY', '64').
card_flavor_text('fen stalker'/'PCY', 'As silent as a shadow, and just as hard to flee.').
card_multiverse_id('fen stalker'/'PCY', '24602').

card_in_set('fickle efreet', 'PCY').
card_original_type('fickle efreet'/'PCY', 'Creature — Efreet').
card_original_text('fickle efreet'/'PCY', 'Whenever Fickle Efreet attacks or blocks, flip a coin at end of combat. If you lose the flip, an opponent gains control of Fickle Efreet.').
card_first_print('fickle efreet', 'PCY').
card_image_name('fickle efreet'/'PCY', 'fickle efreet').
card_uid('fickle efreet'/'PCY', 'PCY:Fickle Efreet:fickle efreet').
card_rarity('fickle efreet'/'PCY', 'Rare').
card_artist('fickle efreet'/'PCY', 'Dave Dorman').
card_number('fickle efreet'/'PCY', '89').
card_flavor_text('fickle efreet'/'PCY', 'Flame chooses its own course.').
card_multiverse_id('fickle efreet'/'PCY', '24649').

card_in_set('flameshot', 'PCY').
card_original_type('flameshot'/'PCY', 'Sorcery').
card_original_text('flameshot'/'PCY', 'You may discard a mountain from your hand instead of paying Flameshot\'s mana cost.\nFlameshot deals 3 damage divided as you choose among any number of target creatures.').
card_first_print('flameshot', 'PCY').
card_image_name('flameshot'/'PCY', 'flameshot').
card_uid('flameshot'/'PCY', 'PCY:Flameshot:flameshot').
card_rarity('flameshot'/'PCY', 'Uncommon').
card_artist('flameshot'/'PCY', 'Mark Brill').
card_number('flameshot'/'PCY', '90').
card_multiverse_id('flameshot'/'PCY', '24642').

card_in_set('flay', 'PCY').
card_original_type('flay'/'PCY', 'Sorcery').
card_original_text('flay'/'PCY', 'Target player discards a card at random from his or her hand. Then that player discards another card at random from his or her hand unless he or she pays {1}.').
card_first_print('flay', 'PCY').
card_image_name('flay'/'PCY', 'flay').
card_uid('flay'/'PCY', 'PCY:Flay:flay').
card_rarity('flay'/'PCY', 'Common').
card_artist('flay'/'PCY', 'Matthew D. Wilson').
card_number('flay'/'PCY', '65').
card_flavor_text('flay'/'PCY', 'The Keldons would never admit it, but Greel unsettles them, too.').
card_multiverse_id('flay'/'PCY', '24606').

card_in_set('flowering field', 'PCY').
card_original_type('flowering field'/'PCY', 'Enchant Land').
card_original_text('flowering field'/'PCY', 'Enchanted land has \"{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.\"').
card_first_print('flowering field', 'PCY').
card_image_name('flowering field'/'PCY', 'flowering field').
card_uid('flowering field'/'PCY', 'PCY:Flowering Field:flowering field').
card_rarity('flowering field'/'PCY', 'Uncommon').
card_artist('flowering field'/'PCY', 'Jeff Miracola').
card_number('flowering field'/'PCY', '9').
card_flavor_text('flowering field'/'PCY', 'Some lands are soothing, and their peace can be felt in the soul.').
card_multiverse_id('flowering field'/'PCY', '24552').

card_in_set('foil', 'PCY').
card_original_type('foil'/'PCY', 'Instant').
card_original_text('foil'/'PCY', 'You may discard an island and another card from your hand instead of paying Foil\'s mana cost.\nCounter target spell.').
card_first_print('foil', 'PCY').
card_image_name('foil'/'PCY', 'foil').
card_uid('foil'/'PCY', 'PCY:Foil:foil').
card_rarity('foil'/'PCY', 'Uncommon').
card_artist('foil'/'PCY', 'Bradley Williams').
card_number('foil'/'PCY', '34').
card_multiverse_id('foil'/'PCY', '24587').

card_in_set('forgotten harvest', 'PCY').
card_original_type('forgotten harvest'/'PCY', 'Enchantment').
card_original_text('forgotten harvest'/'PCY', 'At the beginning of your upkeep, you may remove a land card in your graveyard from the game. If you do, put a +1/+1 counter on target creature.').
card_first_print('forgotten harvest', 'PCY').
card_image_name('forgotten harvest'/'PCY', 'forgotten harvest').
card_uid('forgotten harvest'/'PCY', 'PCY:Forgotten Harvest:forgotten harvest').
card_rarity('forgotten harvest'/'PCY', 'Rare').
card_artist('forgotten harvest'/'PCY', 'DiTerlizzi').
card_number('forgotten harvest'/'PCY', '114').
card_multiverse_id('forgotten harvest'/'PCY', '24676').

card_in_set('glittering lion', 'PCY').
card_original_type('glittering lion'/'PCY', 'Creature — Cat').
card_original_text('glittering lion'/'PCY', 'Prevent all damage that would be dealt to Glittering Lion.\n{3}: Until end of turn, Glittering Lion loses \"Prevent all damage that would be dealt to Glittering Lion.\" Any player may play this ability.').
card_first_print('glittering lion', 'PCY').
card_image_name('glittering lion'/'PCY', 'glittering lion').
card_uid('glittering lion'/'PCY', 'PCY:Glittering Lion:glittering lion').
card_rarity('glittering lion'/'PCY', 'Uncommon').
card_artist('glittering lion'/'PCY', 'Don Hazeltine').
card_number('glittering lion'/'PCY', '10').
card_multiverse_id('glittering lion'/'PCY', '24558').

card_in_set('glittering lynx', 'PCY').
card_original_type('glittering lynx'/'PCY', 'Creature — Cat').
card_original_text('glittering lynx'/'PCY', 'Prevent all damage that would be dealt to Glittering Lynx.\n{2}: Until end of turn, Glittering Lynx loses \"Prevent all damage that would be dealt to Glittering Lynx.\" Any player may play this ability.').
card_first_print('glittering lynx', 'PCY').
card_image_name('glittering lynx'/'PCY', 'glittering lynx').
card_uid('glittering lynx'/'PCY', 'PCY:Glittering Lynx:glittering lynx').
card_rarity('glittering lynx'/'PCY', 'Common').
card_artist('glittering lynx'/'PCY', 'Dan Frazier').
card_number('glittering lynx'/'PCY', '11').
card_multiverse_id('glittering lynx'/'PCY', '24549').

card_in_set('greel\'s caress', 'PCY').
card_original_type('greel\'s caress'/'PCY', 'Enchant Creature').
card_original_text('greel\'s caress'/'PCY', 'You may play Greel\'s Caress any time you could play an instant.\nEnchanted creature gets -3/-0.').
card_first_print('greel\'s caress', 'PCY').
card_image_name('greel\'s caress'/'PCY', 'greel\'s caress').
card_uid('greel\'s caress'/'PCY', 'PCY:Greel\'s Caress:greel\'s caress').
card_rarity('greel\'s caress'/'PCY', 'Common').
card_artist('greel\'s caress'/'PCY', 'Chippy').
card_number('greel\'s caress'/'PCY', '67').
card_flavor_text('greel\'s caress'/'PCY', 'Greel\'s grin is far less loathsome than his touch.').
card_multiverse_id('greel\'s caress'/'PCY', '24604').

card_in_set('greel, mind raker', 'PCY').
card_original_type('greel, mind raker'/'PCY', 'Creature — Spellshaper Legend').
card_original_text('greel, mind raker'/'PCY', '{X}{B}, {T}, Discard two cards from your hand: Target player discards X cards at random from his or her hand.').
card_first_print('greel, mind raker', 'PCY').
card_image_name('greel, mind raker'/'PCY', 'greel, mind raker').
card_uid('greel, mind raker'/'PCY', 'PCY:Greel, Mind Raker:greel, mind raker').
card_rarity('greel, mind raker'/'PCY', 'Rare').
card_artist('greel, mind raker'/'PCY', 'Brom').
card_number('greel, mind raker'/'PCY', '66').
card_flavor_text('greel, mind raker'/'PCY', '\"You\'re Latulla\'s gift to me, and I always play with my presents.\"').
card_multiverse_id('greel, mind raker'/'PCY', '24619').

card_in_set('gulf squid', 'PCY').
card_original_type('gulf squid'/'PCY', 'Creature — Beast').
card_original_text('gulf squid'/'PCY', 'When Gulf Squid comes into play, tap all lands target player controls.').
card_first_print('gulf squid', 'PCY').
card_image_name('gulf squid'/'PCY', 'gulf squid').
card_uid('gulf squid'/'PCY', 'PCY:Gulf Squid:gulf squid').
card_rarity('gulf squid'/'PCY', 'Common').
card_artist('gulf squid'/'PCY', 'Wayne England').
card_number('gulf squid'/'PCY', '35').
card_flavor_text('gulf squid'/'PCY', 'If it could capture the power it wastes, it would be invincible.').
card_multiverse_id('gulf squid'/'PCY', '24573').

card_in_set('hazy homunculus', 'PCY').
card_original_type('hazy homunculus'/'PCY', 'Creature — Illusion').
card_original_text('hazy homunculus'/'PCY', 'Hazy Homunculus is unblockable as long as defending player controls an untapped land.').
card_first_print('hazy homunculus', 'PCY').
card_image_name('hazy homunculus'/'PCY', 'hazy homunculus').
card_uid('hazy homunculus'/'PCY', 'PCY:Hazy Homunculus:hazy homunculus').
card_rarity('hazy homunculus'/'PCY', 'Common').
card_artist('hazy homunculus'/'PCY', 'Anthony S. Waters').
card_number('hazy homunculus'/'PCY', '36').
card_flavor_text('hazy homunculus'/'PCY', 'They hide in dreams and whispers and faraway thoughts.').
card_multiverse_id('hazy homunculus'/'PCY', '24572').

card_in_set('heightened awareness', 'PCY').
card_original_type('heightened awareness'/'PCY', 'Enchantment').
card_original_text('heightened awareness'/'PCY', 'As Heightened Awareness comes into play, discard your hand.\nAt the beginning of your draw step, draw a card.').
card_first_print('heightened awareness', 'PCY').
card_image_name('heightened awareness'/'PCY', 'heightened awareness').
card_uid('heightened awareness'/'PCY', 'PCY:Heightened Awareness:heightened awareness').
card_rarity('heightened awareness'/'PCY', 'Rare').
card_artist('heightened awareness'/'PCY', 'Pete Venters').
card_number('heightened awareness'/'PCY', '37').
card_flavor_text('heightened awareness'/'PCY', '\"It startles me how alive one feels on the edge of death.\"\n—Barrin, master wizard').
card_multiverse_id('heightened awareness'/'PCY', '24594').

card_in_set('hollow warrior', 'PCY').
card_original_type('hollow warrior'/'PCY', 'Artifact Creature — Golem').
card_original_text('hollow warrior'/'PCY', 'Hollow Warrior can\'t attack or block unless you tap an untapped creature you control. (This cost is paid as attackers or blockers are declared.)').
card_first_print('hollow warrior', 'PCY').
card_image_name('hollow warrior'/'PCY', 'hollow warrior').
card_uid('hollow warrior'/'PCY', 'PCY:Hollow Warrior:hollow warrior').
card_rarity('hollow warrior'/'PCY', 'Uncommon').
card_artist('hollow warrior'/'PCY', 'Adam Rex').
card_number('hollow warrior'/'PCY', '138').
card_flavor_text('hollow warrior'/'PCY', 'Though it moves like a living thing, anyone can tell it lacks a soul—not to mention mercy.').
card_multiverse_id('hollow warrior'/'PCY', '24679').

card_in_set('infernal genesis', 'PCY').
card_original_type('infernal genesis'/'PCY', 'Enchantment').
card_original_text('infernal genesis'/'PCY', 'At the beginning of each player\'s upkeep, that player puts the top card of his or her library into his or her graveyard. He or she then puts X 1/1 black Minion creature tokens into play, where X is that card\'s converted mana cost.').
card_first_print('infernal genesis', 'PCY').
card_image_name('infernal genesis'/'PCY', 'infernal genesis').
card_uid('infernal genesis'/'PCY', 'PCY:Infernal Genesis:infernal genesis').
card_rarity('infernal genesis'/'PCY', 'Rare').
card_artist('infernal genesis'/'PCY', 'Ron Spencer').
card_number('infernal genesis'/'PCY', '68').
card_multiverse_id('infernal genesis'/'PCY', '24624').

card_in_set('inflame', 'PCY').
card_original_type('inflame'/'PCY', 'Instant').
card_original_text('inflame'/'PCY', 'Inflame deals 2 damage to each creature dealt damage this turn.').
card_first_print('inflame', 'PCY').
card_image_name('inflame'/'PCY', 'inflame').
card_uid('inflame'/'PCY', 'PCY:Inflame:inflame').
card_rarity('inflame'/'PCY', 'Common').
card_artist('inflame'/'PCY', 'Eric Peterson').
card_number('inflame'/'PCY', '91').
card_flavor_text('inflame'/'PCY', 'Wound, maim, kill: it\'s all the same to a Keldon.').
card_multiverse_id('inflame'/'PCY', '24640').

card_in_set('jeweled spirit', 'PCY').
card_original_type('jeweled spirit'/'PCY', 'Creature — Spirit').
card_original_text('jeweled spirit'/'PCY', 'Flying\nSacrifice two lands: Jeweled Spirit gains protection from artifacts or from the color of your choice until end of turn.').
card_first_print('jeweled spirit', 'PCY').
card_image_name('jeweled spirit'/'PCY', 'jeweled spirit').
card_uid('jeweled spirit'/'PCY', 'PCY:Jeweled Spirit:jeweled spirit').
card_rarity('jeweled spirit'/'PCY', 'Rare').
card_artist('jeweled spirit'/'PCY', 'Christopher Moeller').
card_number('jeweled spirit'/'PCY', '12').
card_flavor_text('jeweled spirit'/'PCY', 'As he knows all the world completely, the world refuses him harm.').
card_multiverse_id('jeweled spirit'/'PCY', '24566').

card_in_set('jolrael\'s favor', 'PCY').
card_original_type('jolrael\'s favor'/'PCY', 'Enchant Creature').
card_original_text('jolrael\'s favor'/'PCY', 'You may play Jolrael\'s Favor any time you could play an instant.\n{1}{G} Regenerate enchanted creature.').
card_first_print('jolrael\'s favor', 'PCY').
card_image_name('jolrael\'s favor'/'PCY', 'jolrael\'s favor').
card_uid('jolrael\'s favor'/'PCY', 'PCY:Jolrael\'s Favor:jolrael\'s favor').
card_rarity('jolrael\'s favor'/'PCY', 'Common').
card_artist('jolrael\'s favor'/'PCY', 'Daren Bader').
card_number('jolrael\'s favor'/'PCY', '116').
card_flavor_text('jolrael\'s favor'/'PCY', '\"War destroys; nature renews.\"\n—Jolrael, empress of beasts').
card_multiverse_id('jolrael\'s favor'/'PCY', '24660').

card_in_set('jolrael, empress of beasts', 'PCY').
card_original_type('jolrael, empress of beasts'/'PCY', 'Creature — Spellshaper Legend').
card_original_text('jolrael, empress of beasts'/'PCY', '{2}{G}, {T}, Discard two cards from your hand: Until end of turn, all lands target player controls are 3/3 creatures that are still lands.').
card_first_print('jolrael, empress of beasts', 'PCY').
card_image_name('jolrael, empress of beasts'/'PCY', 'jolrael, empress of beasts').
card_uid('jolrael, empress of beasts'/'PCY', 'PCY:Jolrael, Empress of Beasts:jolrael, empress of beasts').
card_rarity('jolrael, empress of beasts'/'PCY', 'Rare').
card_artist('jolrael, empress of beasts'/'PCY', 'Matthew D. Wilson').
card_number('jolrael, empress of beasts'/'PCY', '115').
card_flavor_text('jolrael, empress of beasts'/'PCY', '\"I need no army. I have Jamuraa.\"').
card_multiverse_id('jolrael, empress of beasts'/'PCY', '24673').

card_in_set('keldon arsonist', 'PCY').
card_original_type('keldon arsonist'/'PCY', 'Creature — Soldier').
card_original_text('keldon arsonist'/'PCY', '{1}, Sacrifice two lands: Destroy target land.').
card_first_print('keldon arsonist', 'PCY').
card_image_name('keldon arsonist'/'PCY', 'keldon arsonist').
card_uid('keldon arsonist'/'PCY', 'PCY:Keldon Arsonist:keldon arsonist').
card_rarity('keldon arsonist'/'PCY', 'Uncommon').
card_artist('keldon arsonist'/'PCY', 'Paolo Parente').
card_number('keldon arsonist'/'PCY', '92').
card_flavor_text('keldon arsonist'/'PCY', '\"Fields can be replanted. Settlements can be rebuilt. Burn it all.\"\n—Latulla, Keldon overseer').
card_multiverse_id('keldon arsonist'/'PCY', '24637').

card_in_set('keldon battlewagon', 'PCY').
card_original_type('keldon battlewagon'/'PCY', 'Artifact Creature').
card_original_text('keldon battlewagon'/'PCY', 'Trample\nKeldon Battlewagon can\'t block.\nWhen Keldon Battlewagon attacks, sacrifice it at end of combat.\nTap an untapped creature you control: Keldon Battlewagon gets +X/+0 until end of turn, where X is the tapped creature\'s power.').
card_first_print('keldon battlewagon', 'PCY').
card_image_name('keldon battlewagon'/'PCY', 'keldon battlewagon').
card_uid('keldon battlewagon'/'PCY', 'PCY:Keldon Battlewagon:keldon battlewagon').
card_rarity('keldon battlewagon'/'PCY', 'Rare').
card_artist('keldon battlewagon'/'PCY', 'Kev Walker').
card_number('keldon battlewagon'/'PCY', '139').
card_multiverse_id('keldon battlewagon'/'PCY', '24683').

card_in_set('keldon berserker', 'PCY').
card_original_type('keldon berserker'/'PCY', 'Creature — Soldier').
card_original_text('keldon berserker'/'PCY', 'Whenever Keldon Berserker attacks, if you control no untapped lands, it gets +3/+0 until end of turn.').
card_first_print('keldon berserker', 'PCY').
card_image_name('keldon berserker'/'PCY', 'keldon berserker').
card_uid('keldon berserker'/'PCY', 'PCY:Keldon Berserker:keldon berserker').
card_rarity('keldon berserker'/'PCY', 'Common').
card_artist('keldon berserker'/'PCY', 'Paolo Parente').
card_number('keldon berserker'/'PCY', '93').
card_flavor_text('keldon berserker'/'PCY', 'They fight beyond thought, beyond fear, and beyond mercy.').
card_multiverse_id('keldon berserker'/'PCY', '24629').

card_in_set('keldon firebombers', 'PCY').
card_original_type('keldon firebombers'/'PCY', 'Creature — Soldier').
card_original_text('keldon firebombers'/'PCY', 'When Keldon Firebombers comes into play, each player sacrifices all lands he or she controls except for three.').
card_first_print('keldon firebombers', 'PCY').
card_image_name('keldon firebombers'/'PCY', 'keldon firebombers').
card_uid('keldon firebombers'/'PCY', 'PCY:Keldon Firebombers:keldon firebombers').
card_rarity('keldon firebombers'/'PCY', 'Rare').
card_artist('keldon firebombers'/'PCY', 'Randy Gallegos').
card_number('keldon firebombers'/'PCY', '94').
card_flavor_text('keldon firebombers'/'PCY', '\"If there isn\'t enough of Jamuraa left to stand on, I will still claim it for Keld.\"\n—Latulla, Keldon overseer').
card_multiverse_id('keldon firebombers'/'PCY', '24648').

card_in_set('latulla\'s orders', 'PCY').
card_original_type('latulla\'s orders'/'PCY', 'Enchant Creature').
card_original_text('latulla\'s orders'/'PCY', 'You may play Latulla\'s Orders any time you could play an instant.\nWhenever enchanted creature deals combat damage to defending player, you may have it destroy target artifact that player controls.').
card_first_print('latulla\'s orders', 'PCY').
card_image_name('latulla\'s orders'/'PCY', 'latulla\'s orders').
card_uid('latulla\'s orders'/'PCY', 'PCY:Latulla\'s Orders:latulla\'s orders').
card_rarity('latulla\'s orders'/'PCY', 'Common').
card_artist('latulla\'s orders'/'PCY', 'Ben Thompson').
card_number('latulla\'s orders'/'PCY', '96').
card_multiverse_id('latulla\'s orders'/'PCY', '24632').

card_in_set('latulla, keldon overseer', 'PCY').
card_original_type('latulla, keldon overseer'/'PCY', 'Creature — Spellshaper Legend').
card_original_text('latulla, keldon overseer'/'PCY', '{X}{R}, {T}, Discard two cards from your hand: Latulla, Keldon Overseer deals X damage to target creature or player.').
card_first_print('latulla, keldon overseer', 'PCY').
card_image_name('latulla, keldon overseer'/'PCY', 'latulla, keldon overseer').
card_uid('latulla, keldon overseer'/'PCY', 'PCY:Latulla, Keldon Overseer:latulla, keldon overseer').
card_rarity('latulla, keldon overseer'/'PCY', 'Rare').
card_artist('latulla, keldon overseer'/'PCY', 'Brom').
card_number('latulla, keldon overseer'/'PCY', '95').
card_flavor_text('latulla, keldon overseer'/'PCY', '\"Everything around her suffers.\"\n—Haddad, Kipamu soldier').
card_multiverse_id('latulla, keldon overseer'/'PCY', '24646').

card_in_set('lesser gargadon', 'PCY').
card_original_type('lesser gargadon'/'PCY', 'Creature — Beast').
card_original_text('lesser gargadon'/'PCY', 'Whenever Lesser Gargadon attacks or blocks, sacrifice a land.').
card_first_print('lesser gargadon', 'PCY').
card_image_name('lesser gargadon'/'PCY', 'lesser gargadon').
card_uid('lesser gargadon'/'PCY', 'PCY:Lesser Gargadon:lesser gargadon').
card_rarity('lesser gargadon'/'PCY', 'Uncommon').
card_artist('lesser gargadon'/'PCY', 'Rob Alexander').
card_number('lesser gargadon'/'PCY', '97').
card_flavor_text('lesser gargadon'/'PCY', 'Trod on by a gargadon\n—Kipamu expression meaning\n\"wiped out\"').
card_multiverse_id('lesser gargadon'/'PCY', '24647').

card_in_set('living terrain', 'PCY').
card_original_type('living terrain'/'PCY', 'Enchant Land').
card_original_text('living terrain'/'PCY', 'Enchanted land is a 5/6 green Treefolk creature that\'s still a land.').
card_first_print('living terrain', 'PCY').
card_image_name('living terrain'/'PCY', 'living terrain').
card_uid('living terrain'/'PCY', 'PCY:Living Terrain:living terrain').
card_rarity('living terrain'/'PCY', 'Uncommon').
card_artist('living terrain'/'PCY', 'Andrew Goldhawk').
card_number('living terrain'/'PCY', '117').
card_flavor_text('living terrain'/'PCY', 'Keldons were accustomed to rugged terrain, but they never expected it to be aggressive.').
card_multiverse_id('living terrain'/'PCY', '24667').

card_in_set('mageta the lion', 'PCY').
card_original_type('mageta the lion'/'PCY', 'Creature — Spellshaper Legend').
card_original_text('mageta the lion'/'PCY', '{2}{W}{W}, {T}, Discard two cards from your hand: Destroy all creatures except for Mageta the Lion. Those creatures can\'t be regenerated.').
card_first_print('mageta the lion', 'PCY').
card_image_name('mageta the lion'/'PCY', 'mageta the lion').
card_uid('mageta the lion'/'PCY', 'PCY:Mageta the Lion:mageta the lion').
card_rarity('mageta the lion'/'PCY', 'Rare').
card_artist('mageta the lion'/'PCY', 'Brom').
card_number('mageta the lion'/'PCY', '13').
card_flavor_text('mageta the lion'/'PCY', 'In the jungle of war there is always a Lion.').
card_multiverse_id('mageta the lion'/'PCY', '24565').

card_in_set('mageta\'s boon', 'PCY').
card_original_type('mageta\'s boon'/'PCY', 'Enchant Creature').
card_original_text('mageta\'s boon'/'PCY', 'You may play Mageta\'s Boon any time you could play an instant.\nEnchanted creature gets +1/+2.').
card_first_print('mageta\'s boon', 'PCY').
card_image_name('mageta\'s boon'/'PCY', 'mageta\'s boon').
card_uid('mageta\'s boon'/'PCY', 'PCY:Mageta\'s Boon:mageta\'s boon').
card_rarity('mageta\'s boon'/'PCY', 'Common').
card_artist('mageta\'s boon'/'PCY', 'Bradley Williams').
card_number('mageta\'s boon'/'PCY', '14').
card_flavor_text('mageta\'s boon'/'PCY', 'When the Lion roars, his cubs grow stronger.\n—Kipamu saying').
card_multiverse_id('mageta\'s boon'/'PCY', '24550').

card_in_set('mana vapors', 'PCY').
card_original_type('mana vapors'/'PCY', 'Sorcery').
card_original_text('mana vapors'/'PCY', 'Lands target player controls don\'t untap during his or her next untap step.').
card_first_print('mana vapors', 'PCY').
card_image_name('mana vapors'/'PCY', 'mana vapors').
card_uid('mana vapors'/'PCY', 'PCY:Mana Vapors:mana vapors').
card_rarity('mana vapors'/'PCY', 'Uncommon').
card_artist('mana vapors'/'PCY', 'Mark Romanoski').
card_number('mana vapors'/'PCY', '38').
card_flavor_text('mana vapors'/'PCY', 'The scent of magic can rob you of your senses, blinding you to the world.').
card_multiverse_id('mana vapors'/'PCY', '24578').

card_in_set('marsh boa', 'PCY').
card_original_type('marsh boa'/'PCY', 'Creature — Snake').
card_original_text('marsh boa'/'PCY', 'Swampwalk (This creature is unblockable as long as defending player controls a swamp.)').
card_first_print('marsh boa', 'PCY').
card_image_name('marsh boa'/'PCY', 'marsh boa').
card_uid('marsh boa'/'PCY', 'PCY:Marsh Boa:marsh boa').
card_rarity('marsh boa'/'PCY', 'Common').
card_artist('marsh boa'/'PCY', 'Heather Hudson').
card_number('marsh boa'/'PCY', '118').
card_flavor_text('marsh boa'/'PCY', 'Though a powerful constrictor, it prefers simply to drown its prey.').
card_multiverse_id('marsh boa'/'PCY', '24664').

card_in_set('mercenary informer', 'PCY').
card_original_type('mercenary informer'/'PCY', 'Creature — Rebel Mercenary').
card_original_text('mercenary informer'/'PCY', 'Mercenary Informer can\'t be the target of black spells or abilities.\n{2}{W} Put target Mercenary card on the bottom of its owner\'s library.').
card_first_print('mercenary informer', 'PCY').
card_image_name('mercenary informer'/'PCY', 'mercenary informer').
card_uid('mercenary informer'/'PCY', 'PCY:Mercenary Informer:mercenary informer').
card_rarity('mercenary informer'/'PCY', 'Rare').
card_artist('mercenary informer'/'PCY', 'Nelson DeCastro').
card_number('mercenary informer'/'PCY', '15').
card_flavor_text('mercenary informer'/'PCY', 'The problem with rebels is that they so quickly reject authority.').
card_multiverse_id('mercenary informer'/'PCY', '24563').

card_in_set('mine bearer', 'PCY').
card_original_type('mine bearer'/'PCY', 'Creature — Soldier').
card_original_text('mine bearer'/'PCY', '{T}, Sacrifice Mine Bearer: Destroy target attacking creature.').
card_first_print('mine bearer', 'PCY').
card_image_name('mine bearer'/'PCY', 'mine bearer').
card_uid('mine bearer'/'PCY', 'PCY:Mine Bearer:mine bearer').
card_rarity('mine bearer'/'PCY', 'Common').
card_artist('mine bearer'/'PCY', 'D. Alexander Gregory').
card_number('mine bearer'/'PCY', '16').
card_flavor_text('mine bearer'/'PCY', '\"The Keldons may have explosive tempers, but I have explosives.\"').
card_multiverse_id('mine bearer'/'PCY', '24555').

card_in_set('mirror strike', 'PCY').
card_original_type('mirror strike'/'PCY', 'Instant').
card_original_text('mirror strike'/'PCY', 'Target unblocked creature deals combat damage to its controller instead of to you this turn.').
card_first_print('mirror strike', 'PCY').
card_image_name('mirror strike'/'PCY', 'mirror strike').
card_uid('mirror strike'/'PCY', 'PCY:Mirror Strike:mirror strike').
card_rarity('mirror strike'/'PCY', 'Uncommon').
card_artist('mirror strike'/'PCY', 'Dave Dorman').
card_number('mirror strike'/'PCY', '17').
card_flavor_text('mirror strike'/'PCY', '\"Enemies can be your weapons or your shields. Simply show them which they are.\"\n—The Teachings of Zho').
card_multiverse_id('mirror strike'/'PCY', '24554').

card_in_set('mungha wurm', 'PCY').
card_original_type('mungha wurm'/'PCY', 'Creature — Wurm').
card_original_text('mungha wurm'/'PCY', 'You can\'t untap more than one land during your untap step.').
card_first_print('mungha wurm', 'PCY').
card_image_name('mungha wurm'/'PCY', 'mungha wurm').
card_uid('mungha wurm'/'PCY', 'PCY:Mungha Wurm:mungha wurm').
card_rarity('mungha wurm'/'PCY', 'Rare').
card_artist('mungha wurm'/'PCY', 'Greg Staples').
card_number('mungha wurm'/'PCY', '119').
card_flavor_text('mungha wurm'/'PCY', 'Cities have crumbled under its weight.').
card_multiverse_id('mungha wurm'/'PCY', '24675').

card_in_set('nakaya shade', 'PCY').
card_original_type('nakaya shade'/'PCY', 'Creature — Shade').
card_original_text('nakaya shade'/'PCY', '{B} Nakaya Shade gets +1/+1 until end of turn unless any player pays {2}.').
card_first_print('nakaya shade', 'PCY').
card_image_name('nakaya shade'/'PCY', 'nakaya shade').
card_uid('nakaya shade'/'PCY', 'PCY:Nakaya Shade:nakaya shade').
card_rarity('nakaya shade'/'PCY', 'Uncommon').
card_artist('nakaya shade'/'PCY', 'Ray Lago').
card_number('nakaya shade'/'PCY', '69').
card_flavor_text('nakaya shade'/'PCY', 'Made strong by reckless magic. Made horrid by reckless strife.').
card_multiverse_id('nakaya shade'/'PCY', '24598').

card_in_set('noxious field', 'PCY').
card_original_type('noxious field'/'PCY', 'Enchant Land').
card_original_text('noxious field'/'PCY', 'Enchanted land has \"{T}: This land deals 1 damage to each creature and each player.\"').
card_first_print('noxious field', 'PCY').
card_image_name('noxious field'/'PCY', 'noxious field').
card_uid('noxious field'/'PCY', 'PCY:Noxious Field:noxious field').
card_rarity('noxious field'/'PCY', 'Uncommon').
card_artist('noxious field'/'PCY', 'Eric Peterson').
card_number('noxious field'/'PCY', '70').
card_flavor_text('noxious field'/'PCY', 'Ultimately, it became a three-way battle: the land attacked both armies.').
card_multiverse_id('noxious field'/'PCY', '24686').

card_in_set('outbreak', 'PCY').
card_original_type('outbreak'/'PCY', 'Sorcery').
card_original_text('outbreak'/'PCY', 'You may discard a swamp from your hand instead of paying Outbreak\'s mana cost.\nChoose a creature type. All creatures of that type get -1/-1 until end of turn.').
card_first_print('outbreak', 'PCY').
card_image_name('outbreak'/'PCY', 'outbreak').
card_uid('outbreak'/'PCY', 'PCY:Outbreak:outbreak').
card_rarity('outbreak'/'PCY', 'Uncommon').
card_artist('outbreak'/'PCY', 'Quinton Hoover').
card_number('outbreak'/'PCY', '71').
card_flavor_text('outbreak'/'PCY', 'This plague chooses its victims.').
card_multiverse_id('outbreak'/'PCY', '24613').

card_in_set('overburden', 'PCY').
card_original_type('overburden'/'PCY', 'Enchantment').
card_original_text('overburden'/'PCY', 'Whenever a player puts a creature card into play, that player returns a land he or she controls to its owner\'s hand.').
card_first_print('overburden', 'PCY').
card_image_name('overburden'/'PCY', 'overburden').
card_uid('overburden'/'PCY', 'PCY:Overburden:overburden').
card_rarity('overburden'/'PCY', 'Rare').
card_artist('overburden'/'PCY', 'John Matson').
card_number('overburden'/'PCY', '39').
card_flavor_text('overburden'/'PCY', 'The Kipamu League knew what Keld refused to acknowledge: the land could support no more.').
card_multiverse_id('overburden'/'PCY', '24597').

card_in_set('panic attack', 'PCY').
card_original_type('panic attack'/'PCY', 'Sorcery').
card_original_text('panic attack'/'PCY', 'Up to three target creatures can\'t block this turn.').
card_first_print('panic attack', 'PCY').
card_image_name('panic attack'/'PCY', 'panic attack').
card_uid('panic attack'/'PCY', 'PCY:Panic Attack:panic attack').
card_rarity('panic attack'/'PCY', 'Common').
card_artist('panic attack'/'PCY', 'Mike Ploog').
card_number('panic attack'/'PCY', '98').
card_flavor_text('panic attack'/'PCY', 'Sometimes everyone decides to go for help at the same time.').
card_multiverse_id('panic attack'/'PCY', '24639').

card_in_set('pit raptor', 'PCY').
card_original_type('pit raptor'/'PCY', 'Creature — Mercenary').
card_original_text('pit raptor'/'PCY', 'Flying, first strike\nAt the beginning of your upkeep, sacrifice Pit Raptor unless you pay {2}{B}{B}.').
card_first_print('pit raptor', 'PCY').
card_image_name('pit raptor'/'PCY', 'pit raptor').
card_uid('pit raptor'/'PCY', 'PCY:Pit Raptor:pit raptor').
card_rarity('pit raptor'/'PCY', 'Uncommon').
card_artist('pit raptor'/'PCY', 'Thomas Gianni').
card_number('pit raptor'/'PCY', '72').
card_multiverse_id('pit raptor'/'PCY', '24612').

card_in_set('plague fiend', 'PCY').
card_original_type('plague fiend'/'PCY', 'Creature — Insect').
card_original_text('plague fiend'/'PCY', 'Whenever Plague Fiend deals combat damage to a creature, destroy that creature unless its controller pays {2}.').
card_first_print('plague fiend', 'PCY').
card_image_name('plague fiend'/'PCY', 'plague fiend').
card_uid('plague fiend'/'PCY', 'PCY:Plague Fiend:plague fiend').
card_rarity('plague fiend'/'PCY', 'Common').
card_artist('plague fiend'/'PCY', 'David Martin').
card_number('plague fiend'/'PCY', '73').
card_flavor_text('plague fiend'/'PCY', 'As the strange plague ravaged Jamuraa, the fiends multiplied.').
card_multiverse_id('plague fiend'/'PCY', '24610').

card_in_set('plague wind', 'PCY').
card_original_type('plague wind'/'PCY', 'Sorcery').
card_original_text('plague wind'/'PCY', 'Destroy all creatures you don\'t control. They can\'t be regenerated.').
card_first_print('plague wind', 'PCY').
card_image_name('plague wind'/'PCY', 'plague wind').
card_uid('plague wind'/'PCY', 'PCY:Plague Wind:plague wind').
card_rarity('plague wind'/'PCY', 'Rare').
card_artist('plague wind'/'PCY', 'Alan Pollack').
card_number('plague wind'/'PCY', '74').
card_flavor_text('plague wind'/'PCY', '\"The second wind of ascension is Reaver, slaying the unworthy.\"\n—Keld Triumphant').
card_multiverse_id('plague wind'/'PCY', '24622').

card_in_set('psychic theft', 'PCY').
card_original_type('psychic theft'/'PCY', 'Sorcery').
card_original_text('psychic theft'/'PCY', 'Look at target player\'s hand, choose an instant or sorcery card from it, and remove that card from the game. You may play the card as though it were in your hand as long as the card remains removed from the game. At end of turn, if you haven\'t played the card, return it to its owner\'s hand.').
card_first_print('psychic theft', 'PCY').
card_image_name('psychic theft'/'PCY', 'psychic theft').
card_uid('psychic theft'/'PCY', 'PCY:Psychic Theft:psychic theft').
card_rarity('psychic theft'/'PCY', 'Rare').
card_artist('psychic theft'/'PCY', 'Don Hazeltine').
card_number('psychic theft'/'PCY', '40').
card_multiverse_id('psychic theft'/'PCY', '24593').

card_in_set('pygmy razorback', 'PCY').
card_original_type('pygmy razorback'/'PCY', 'Creature — Boar').
card_original_text('pygmy razorback'/'PCY', 'Trample').
card_first_print('pygmy razorback', 'PCY').
card_image_name('pygmy razorback'/'PCY', 'pygmy razorback').
card_uid('pygmy razorback'/'PCY', 'PCY:Pygmy Razorback:pygmy razorback').
card_rarity('pygmy razorback'/'PCY', 'Common').
card_artist('pygmy razorback'/'PCY', 'Matt Cavotta').
card_number('pygmy razorback'/'PCY', '120').
card_flavor_text('pygmy razorback'/'PCY', 'As a rite of passage, Kipamu youths taunt razorbacks with long sticks . . . then flee.').
card_multiverse_id('pygmy razorback'/'PCY', '24663').

card_in_set('quicksilver wall', 'PCY').
card_original_type('quicksilver wall'/'PCY', 'Creature — Wall').
card_original_text('quicksilver wall'/'PCY', '(Walls can\'t attack.)\n{4}: Return Quicksilver Wall to its owner\'s hand. Any player may play this ability.').
card_first_print('quicksilver wall', 'PCY').
card_image_name('quicksilver wall'/'PCY', 'quicksilver wall').
card_uid('quicksilver wall'/'PCY', 'PCY:Quicksilver Wall:quicksilver wall').
card_rarity('quicksilver wall'/'PCY', 'Uncommon').
card_artist('quicksilver wall'/'PCY', 'Matt Cavotta').
card_number('quicksilver wall'/'PCY', '41').
card_flavor_text('quicksilver wall'/'PCY', '\"Ogg hit wall. Ogg no kill wall. Ogg hate wall!\"').
card_multiverse_id('quicksilver wall'/'PCY', '24582').

card_in_set('rebel informer', 'PCY').
card_original_type('rebel informer'/'PCY', 'Creature — Mercenary Rebel').
card_original_text('rebel informer'/'PCY', 'Rebel Informer can\'t be the target of white spells or abilities.\n{3}: Put target Rebel card on the bottom of its owner\'s library.').
card_first_print('rebel informer', 'PCY').
card_image_name('rebel informer'/'PCY', 'rebel informer').
card_uid('rebel informer'/'PCY', 'PCY:Rebel Informer:rebel informer').
card_rarity('rebel informer'/'PCY', 'Rare').
card_artist('rebel informer'/'PCY', 'Scott M. Fischer').
card_number('rebel informer'/'PCY', '75').
card_flavor_text('rebel informer'/'PCY', 'The problem with mercenaries is that if you can buy their loyalty, others can too.').
card_multiverse_id('rebel informer'/'PCY', '24617').

card_in_set('rethink', 'PCY').
card_original_type('rethink'/'PCY', 'Instant').
card_original_text('rethink'/'PCY', 'Counter target spell unless its controller pays {X}, where X is its converted mana cost.').
card_first_print('rethink', 'PCY').
card_image_name('rethink'/'PCY', 'rethink').
card_uid('rethink'/'PCY', 'PCY:Rethink:rethink').
card_rarity('rethink'/'PCY', 'Common').
card_artist('rethink'/'PCY', 'Matt Cavotta').
card_number('rethink'/'PCY', '42').
card_flavor_text('rethink'/'PCY', '\"Are you sure you want to do that?\"').
card_multiverse_id('rethink'/'PCY', '24588').

card_in_set('reveille squad', 'PCY').
card_original_type('reveille squad'/'PCY', 'Creature — Rebel').
card_original_text('reveille squad'/'PCY', 'Whenever you\'re attacked, if Reveille Squad is untapped, you may untap all creatures you control.').
card_first_print('reveille squad', 'PCY').
card_image_name('reveille squad'/'PCY', 'reveille squad').
card_uid('reveille squad'/'PCY', 'PCY:Reveille Squad:reveille squad').
card_rarity('reveille squad'/'PCY', 'Uncommon').
card_artist('reveille squad'/'PCY', 'Greg & Tim Hildebrandt').
card_number('reveille squad'/'PCY', '18').
card_flavor_text('reveille squad'/'PCY', '\"To arms! To arms! The Lion roars!\"').
card_multiverse_id('reveille squad'/'PCY', '24559').

card_in_set('rhystic cave', 'PCY').
card_original_type('rhystic cave'/'PCY', 'Land').
card_original_text('rhystic cave'/'PCY', '{T}: Add one mana of any color to your mana pool unless any player pays {1}.').
card_first_print('rhystic cave', 'PCY').
card_image_name('rhystic cave'/'PCY', 'rhystic cave').
card_uid('rhystic cave'/'PCY', 'PCY:Rhystic Cave:rhystic cave').
card_rarity('rhystic cave'/'PCY', 'Uncommon').
card_artist('rhystic cave'/'PCY', 'Rob Alexander').
card_number('rhystic cave'/'PCY', '142').
card_flavor_text('rhystic cave'/'PCY', '\"All mages dip into the same well; some are just in more of a hurry.\"\n—Alexi, zephyr mage').
card_multiverse_id('rhystic cave'/'PCY', '24685').

card_in_set('rhystic circle', 'PCY').
card_original_type('rhystic circle'/'PCY', 'Enchantment').
card_original_text('rhystic circle'/'PCY', '{1}: Any player may pay {1}. If no one does, the next time a source of your choice would deal damage to you this turn, prevent that damage.').
card_first_print('rhystic circle', 'PCY').
card_image_name('rhystic circle'/'PCY', 'rhystic circle').
card_uid('rhystic circle'/'PCY', 'PCY:Rhystic Circle:rhystic circle').
card_rarity('rhystic circle'/'PCY', 'Common').
card_artist('rhystic circle'/'PCY', 'Alan Pollack').
card_number('rhystic circle'/'PCY', '19').
card_flavor_text('rhystic circle'/'PCY', 'The circle buys time—an important commodity in battle.').
card_multiverse_id('rhystic circle'/'PCY', '24562').

card_in_set('rhystic deluge', 'PCY').
card_original_type('rhystic deluge'/'PCY', 'Enchantment').
card_original_text('rhystic deluge'/'PCY', '{U} Tap target creature unless its controller pays {1}.').
card_first_print('rhystic deluge', 'PCY').
card_image_name('rhystic deluge'/'PCY', 'rhystic deluge').
card_uid('rhystic deluge'/'PCY', 'PCY:Rhystic Deluge:rhystic deluge').
card_rarity('rhystic deluge'/'PCY', 'Common').
card_artist('rhystic deluge'/'PCY', 'Pete Venters').
card_number('rhystic deluge'/'PCY', '43').
card_flavor_text('rhystic deluge'/'PCY', '\"I\'ll rain down on you with the power of magic itself. Then we\'ll see who prevails.\"\n—Alexi, zephyr mage').
card_multiverse_id('rhystic deluge'/'PCY', '24576').

card_in_set('rhystic lightning', 'PCY').
card_original_type('rhystic lightning'/'PCY', 'Instant').
card_original_text('rhystic lightning'/'PCY', 'Rhystic Lightning deals 4 damage to target creature or player unless that creature\'s controller or that player pays {2}. If he or she does, Rhystic Lightning deals 2 damage to the creature or player.').
card_first_print('rhystic lightning', 'PCY').
card_image_name('rhystic lightning'/'PCY', 'rhystic lightning').
card_uid('rhystic lightning'/'PCY', 'PCY:Rhystic Lightning:rhystic lightning').
card_rarity('rhystic lightning'/'PCY', 'Common').
card_artist('rhystic lightning'/'PCY', 'Roger Raupp').
card_number('rhystic lightning'/'PCY', '99').
card_multiverse_id('rhystic lightning'/'PCY', '24633').

card_in_set('rhystic scrying', 'PCY').
card_original_type('rhystic scrying'/'PCY', 'Sorcery').
card_original_text('rhystic scrying'/'PCY', 'Draw three cards. Then, if any player pays {2}, discard three cards from your hand.').
card_first_print('rhystic scrying', 'PCY').
card_image_name('rhystic scrying'/'PCY', 'rhystic scrying').
card_uid('rhystic scrying'/'PCY', 'PCY:Rhystic Scrying:rhystic scrying').
card_rarity('rhystic scrying'/'PCY', 'Uncommon').
card_artist('rhystic scrying'/'PCY', 'Roger Raupp').
card_number('rhystic scrying'/'PCY', '44').
card_flavor_text('rhystic scrying'/'PCY', '\"The trick is to ask when no one\'s listening.\"\n—Alexi, zephyr mage').
card_multiverse_id('rhystic scrying'/'PCY', '24577').

card_in_set('rhystic shield', 'PCY').
card_original_type('rhystic shield'/'PCY', 'Instant').
card_original_text('rhystic shield'/'PCY', 'Creatures you control get +0/+1 until end of turn. They get an additional +0/+2 until end of turn unless any player pays {2}.').
card_first_print('rhystic shield', 'PCY').
card_image_name('rhystic shield'/'PCY', 'rhystic shield').
card_uid('rhystic shield'/'PCY', 'PCY:Rhystic Shield:rhystic shield').
card_rarity('rhystic shield'/'PCY', 'Common').
card_artist('rhystic shield'/'PCY', 'Kev Walker').
card_number('rhystic shield'/'PCY', '20').
card_flavor_text('rhystic shield'/'PCY', 'A shield for one is good. A shield for everyone is even better.').
card_multiverse_id('rhystic shield'/'PCY', '24551').

card_in_set('rhystic study', 'PCY').
card_original_type('rhystic study'/'PCY', 'Enchantment').
card_original_text('rhystic study'/'PCY', 'Whenever an opponent plays a spell, you may draw a card unless that player pays {1}.').
card_first_print('rhystic study', 'PCY').
card_image_name('rhystic study'/'PCY', 'rhystic study').
card_uid('rhystic study'/'PCY', 'PCY:Rhystic Study:rhystic study').
card_rarity('rhystic study'/'PCY', 'Common').
card_artist('rhystic study'/'PCY', 'Terese Nielsen').
card_number('rhystic study'/'PCY', '45').
card_flavor_text('rhystic study'/'PCY', 'Friends teach what you want to know. Enemies teach what you need to know.').
card_multiverse_id('rhystic study'/'PCY', '24589').

card_in_set('rhystic syphon', 'PCY').
card_original_type('rhystic syphon'/'PCY', 'Sorcery').
card_original_text('rhystic syphon'/'PCY', 'Unless target player pays {3}, he or she loses 5 life and you gain 5 life.').
card_first_print('rhystic syphon', 'PCY').
card_image_name('rhystic syphon'/'PCY', 'rhystic syphon').
card_uid('rhystic syphon'/'PCY', 'PCY:Rhystic Syphon:rhystic syphon').
card_rarity('rhystic syphon'/'PCY', 'Uncommon').
card_artist('rhystic syphon'/'PCY', 'Ron Spencer').
card_number('rhystic syphon'/'PCY', '76').
card_flavor_text('rhystic syphon'/'PCY', 'Jamuraan wizards are linked together by magic, a bond that only the wicked dare exploit.').
card_multiverse_id('rhystic syphon'/'PCY', '24623').

card_in_set('rhystic tutor', 'PCY').
card_original_type('rhystic tutor'/'PCY', 'Sorcery').
card_original_text('rhystic tutor'/'PCY', 'Unless any player pays {2}, search your library for a card, put that card into your hand, then shuffle your library.').
card_first_print('rhystic tutor', 'PCY').
card_image_name('rhystic tutor'/'PCY', 'rhystic tutor').
card_uid('rhystic tutor'/'PCY', 'PCY:Rhystic Tutor:rhystic tutor').
card_rarity('rhystic tutor'/'PCY', 'Rare').
card_artist('rhystic tutor'/'PCY', 'Dan Frazier').
card_number('rhystic tutor'/'PCY', '77').
card_flavor_text('rhystic tutor'/'PCY', 'Any lock can be undone, any secret revealed.').
card_multiverse_id('rhystic tutor'/'PCY', '24614').

card_in_set('rib cage spider', 'PCY').
card_original_type('rib cage spider'/'PCY', 'Creature — Spider').
card_original_text('rib cage spider'/'PCY', 'Rib Cage Spider may block as though it had flying.').
card_first_print('rib cage spider', 'PCY').
card_image_name('rib cage spider'/'PCY', 'rib cage spider').
card_uid('rib cage spider'/'PCY', 'PCY:Rib Cage Spider:rib cage spider').
card_rarity('rib cage spider'/'PCY', 'Common').
card_artist('rib cage spider'/'PCY', 'Dana Knutson').
card_number('rib cage spider'/'PCY', '121').
card_flavor_text('rib cage spider'/'PCY', 'Everything it sees it considers food—and with that many eyes, it\'s not liable to miss much.').
card_multiverse_id('rib cage spider'/'PCY', '24653').

card_in_set('ribbon snake', 'PCY').
card_original_type('ribbon snake'/'PCY', 'Creature — Snake').
card_original_text('ribbon snake'/'PCY', 'Flying\n{2}: Ribbon Snake loses flying until end of turn. Any player may play this ability.').
card_first_print('ribbon snake', 'PCY').
card_image_name('ribbon snake'/'PCY', 'ribbon snake').
card_uid('ribbon snake'/'PCY', 'PCY:Ribbon Snake:ribbon snake').
card_rarity('ribbon snake'/'PCY', 'Common').
card_artist('ribbon snake'/'PCY', 'Mark Zug').
card_number('ribbon snake'/'PCY', '46').
card_flavor_text('ribbon snake'/'PCY', 'All snakes slither. Only this one soars.').
card_multiverse_id('ribbon snake'/'PCY', '24571').

card_in_set('ridgeline rager', 'PCY').
card_original_type('ridgeline rager'/'PCY', 'Creature — Beast').
card_original_text('ridgeline rager'/'PCY', '{R} Ridgeline Rager gets +1/+0 until end of turn.').
card_first_print('ridgeline rager', 'PCY').
card_image_name('ridgeline rager'/'PCY', 'ridgeline rager').
card_uid('ridgeline rager'/'PCY', 'PCY:Ridgeline Rager:ridgeline rager').
card_rarity('ridgeline rager'/'PCY', 'Common').
card_artist('ridgeline rager'/'PCY', 'Chippy').
card_number('ridgeline rager'/'PCY', '100').
card_flavor_text('ridgeline rager'/'PCY', 'While armies battled across Jamuraa, beasts stalked the high places looking for victims.').
card_multiverse_id('ridgeline rager'/'PCY', '24627').

card_in_set('root cage', 'PCY').
card_original_type('root cage'/'PCY', 'Enchantment').
card_original_text('root cage'/'PCY', 'Mercenaries don\'t untap during their controllers\' untap steps.').
card_first_print('root cage', 'PCY').
card_image_name('root cage'/'PCY', 'root cage').
card_uid('root cage'/'PCY', 'PCY:Root Cage:root cage').
card_rarity('root cage'/'PCY', 'Uncommon').
card_artist('root cage'/'PCY', 'Glen Angus').
card_number('root cage'/'PCY', '122').
card_flavor_text('root cage'/'PCY', 'The Vintara Forest welcomes its enemies with a crushing embrace.').
card_multiverse_id('root cage'/'PCY', '24665').

card_in_set('samite sanctuary', 'PCY').
card_original_type('samite sanctuary'/'PCY', 'Enchantment').
card_original_text('samite sanctuary'/'PCY', '{2}: Prevent the next 1 damage that would be dealt to target creature this turn. Any player may play this ability.').
card_first_print('samite sanctuary', 'PCY').
card_image_name('samite sanctuary'/'PCY', 'samite sanctuary').
card_uid('samite sanctuary'/'PCY', 'PCY:Samite Sanctuary:samite sanctuary').
card_rarity('samite sanctuary'/'PCY', 'Rare').
card_artist('samite sanctuary'/'PCY', 'Ben Thompson').
card_number('samite sanctuary'/'PCY', '21').
card_flavor_text('samite sanctuary'/'PCY', 'Our war is against suffering, not against those who cause it.\n—Samite creed').
card_multiverse_id('samite sanctuary'/'PCY', '24570').

card_in_set('scoria cat', 'PCY').
card_original_type('scoria cat'/'PCY', 'Creature — Cat').
card_original_text('scoria cat'/'PCY', 'Scoria Cat gets +3/+3 as long as you control no untapped lands.').
card_first_print('scoria cat', 'PCY').
card_image_name('scoria cat'/'PCY', 'scoria cat').
card_uid('scoria cat'/'PCY', 'PCY:Scoria Cat:scoria cat').
card_rarity('scoria cat'/'PCY', 'Uncommon').
card_artist('scoria cat'/'PCY', 'Andrew Goldhawk').
card_number('scoria cat'/'PCY', '101').
card_flavor_text('scoria cat'/'PCY', 'Like a volcano, it too can erupt without warning.').
card_multiverse_id('scoria cat'/'PCY', '24636').

card_in_set('search for survivors', 'PCY').
card_original_type('search for survivors'/'PCY', 'Sorcery').
card_original_text('search for survivors'/'PCY', 'Shuffle your graveyard. An opponent chooses a card from it at random. If that card is a creature card, put it into play. Otherwise, remove it from the game.').
card_first_print('search for survivors', 'PCY').
card_image_name('search for survivors'/'PCY', 'search for survivors').
card_uid('search for survivors'/'PCY', 'PCY:Search for Survivors:search for survivors').
card_rarity('search for survivors'/'PCY', 'Rare').
card_artist('search for survivors'/'PCY', 'Mark Romanoski').
card_number('search for survivors'/'PCY', '102').
card_flavor_text('search for survivors'/'PCY', 'There\'s no rest for a Keldon.').
card_multiverse_id('search for survivors'/'PCY', '24641').

card_in_set('searing wind', 'PCY').
card_original_type('searing wind'/'PCY', 'Instant').
card_original_text('searing wind'/'PCY', 'Searing Wind deals 10 damage to target creature or player.').
card_first_print('searing wind', 'PCY').
card_image_name('searing wind'/'PCY', 'searing wind').
card_uid('searing wind'/'PCY', 'PCY:Searing Wind:searing wind').
card_rarity('searing wind'/'PCY', 'Rare').
card_artist('searing wind'/'PCY', 'John Matson').
card_number('searing wind'/'PCY', '103').
card_flavor_text('searing wind'/'PCY', '\"The first wind of ascension is Forger, burning away impurity.\"\n—Keld Triumphant').
card_multiverse_id('searing wind'/'PCY', '24650').

card_in_set('sheltering prayers', 'PCY').
card_original_type('sheltering prayers'/'PCY', 'Enchantment').
card_original_text('sheltering prayers'/'PCY', 'Basic lands each player controls can\'t be the targets of spells or abilities as long as that player controls three or fewer lands.').
card_first_print('sheltering prayers', 'PCY').
card_image_name('sheltering prayers'/'PCY', 'sheltering prayers').
card_uid('sheltering prayers'/'PCY', 'PCY:Sheltering Prayers:sheltering prayers').
card_rarity('sheltering prayers'/'PCY', 'Rare').
card_artist('sheltering prayers'/'PCY', 'Nelson DeCastro').
card_number('sheltering prayers'/'PCY', '22').
card_flavor_text('sheltering prayers'/'PCY', '\"It matters not if the gods hear my prayers. Jamuraa does.\"').
card_multiverse_id('sheltering prayers'/'PCY', '24568').

card_in_set('shield dancer', 'PCY').
card_original_type('shield dancer'/'PCY', 'Creature — Rebel').
card_original_text('shield dancer'/'PCY', '{2}{W} The next time target attacking creature would deal combat damage to Shield Dancer this turn, that creature deals that damage to itself instead.').
card_first_print('shield dancer', 'PCY').
card_image_name('shield dancer'/'PCY', 'shield dancer').
card_uid('shield dancer'/'PCY', 'PCY:Shield Dancer:shield dancer').
card_rarity('shield dancer'/'PCY', 'Uncommon').
card_artist('shield dancer'/'PCY', 'Mike Ploog').
card_number('shield dancer'/'PCY', '23').
card_flavor_text('shield dancer'/'PCY', 'The harder your strike, the stronger her defense.').
card_multiverse_id('shield dancer'/'PCY', '24556').

card_in_set('shrouded serpent', 'PCY').
card_original_type('shrouded serpent'/'PCY', 'Creature — Serpent').
card_original_text('shrouded serpent'/'PCY', 'Whenever Shrouded Serpent attacks, defending player may pay {4}. If he or she doesn\'t, Shrouded Serpent is unblockable this turn.').
card_first_print('shrouded serpent', 'PCY').
card_image_name('shrouded serpent'/'PCY', 'shrouded serpent').
card_uid('shrouded serpent'/'PCY', 'PCY:Shrouded Serpent:shrouded serpent').
card_rarity('shrouded serpent'/'PCY', 'Rare').
card_artist('shrouded serpent'/'PCY', 'Dana Knutson').
card_number('shrouded serpent'/'PCY', '47').
card_flavor_text('shrouded serpent'/'PCY', 'Many travelers have wandered into a fog only to find that it had teeth, claws, and an appetite.').
card_multiverse_id('shrouded serpent'/'PCY', '24595').

card_in_set('silt crawler', 'PCY').
card_original_type('silt crawler'/'PCY', 'Creature — Beast').
card_original_text('silt crawler'/'PCY', 'When Silt Crawler comes into play, tap all lands you control.').
card_first_print('silt crawler', 'PCY').
card_image_name('silt crawler'/'PCY', 'silt crawler').
card_uid('silt crawler'/'PCY', 'PCY:Silt Crawler:silt crawler').
card_rarity('silt crawler'/'PCY', 'Common').
card_artist('silt crawler'/'PCY', 'Arnie Swekel').
card_number('silt crawler'/'PCY', '123').
card_flavor_text('silt crawler'/'PCY', 'If they could replenish the land instead of draining it, Jamuraa would be teeming with them.').
card_multiverse_id('silt crawler'/'PCY', '24655').

card_in_set('snag', 'PCY').
card_original_type('snag'/'PCY', 'Instant').
card_original_text('snag'/'PCY', 'You may discard a forest from your hand instead of paying Snag\'s mana cost.\nPrevent all combat damage that would be dealt by unblocked creatures this turn.').
card_first_print('snag', 'PCY').
card_image_name('snag'/'PCY', 'snag').
card_uid('snag'/'PCY', 'PCY:Snag:snag').
card_rarity('snag'/'PCY', 'Uncommon').
card_artist('snag'/'PCY', 'Ron Spencer').
card_number('snag'/'PCY', '124').
card_multiverse_id('snag'/'PCY', '24668').

card_in_set('soul charmer', 'PCY').
card_original_type('soul charmer'/'PCY', 'Creature — Rebel').
card_original_text('soul charmer'/'PCY', 'Whenever Soul Charmer deals combat damage to a creature, you gain 2 life unless that creature\'s controller pays {2}.').
card_first_print('soul charmer', 'PCY').
card_image_name('soul charmer'/'PCY', 'soul charmer').
card_uid('soul charmer'/'PCY', 'PCY:Soul Charmer:soul charmer').
card_rarity('soul charmer'/'PCY', 'Common').
card_artist('soul charmer'/'PCY', 'Glen Angus').
card_number('soul charmer'/'PCY', '24').
card_flavor_text('soul charmer'/'PCY', 'She seeks the warmth of the living.').
card_multiverse_id('soul charmer'/'PCY', '24611').

card_in_set('soul strings', 'PCY').
card_original_type('soul strings'/'PCY', 'Sorcery').
card_original_text('soul strings'/'PCY', 'Return two target creature cards from your graveyard to your hand unless any player pays {X}.').
card_first_print('soul strings', 'PCY').
card_image_name('soul strings'/'PCY', 'soul strings').
card_uid('soul strings'/'PCY', 'PCY:Soul Strings:soul strings').
card_rarity('soul strings'/'PCY', 'Common').
card_artist('soul strings'/'PCY', 'Daren Bader').
card_number('soul strings'/'PCY', '78').
card_flavor_text('soul strings'/'PCY', 'Everyone is manipulated. It\'s just more obvious with the dead.').
card_multiverse_id('soul strings'/'PCY', '24605').

card_in_set('spiketail drake', 'PCY').
card_original_type('spiketail drake'/'PCY', 'Creature — Drake').
card_original_text('spiketail drake'/'PCY', 'Flying\nSacrifice Spiketail Drake: Counter target spell unless its controller pays {3}.').
card_first_print('spiketail drake', 'PCY').
card_image_name('spiketail drake'/'PCY', 'spiketail drake').
card_uid('spiketail drake'/'PCY', 'PCY:Spiketail Drake:spiketail drake').
card_rarity('spiketail drake'/'PCY', 'Uncommon').
card_artist('spiketail drake'/'PCY', 'Michael Sutfin').
card_number('spiketail drake'/'PCY', '48').
card_flavor_text('spiketail drake'/'PCY', '\"The spell will have to wait,\" said Alexi, pointing up. \"Drakes.\"').
card_multiverse_id('spiketail drake'/'PCY', '24592').

card_in_set('spiketail hatchling', 'PCY').
card_original_type('spiketail hatchling'/'PCY', 'Creature — Drake').
card_original_text('spiketail hatchling'/'PCY', 'Flying\nSacrifice Spiketail Hatchling: Counter target spell unless its controller pays {1}.').
card_first_print('spiketail hatchling', 'PCY').
card_image_name('spiketail hatchling'/'PCY', 'spiketail hatchling').
card_uid('spiketail hatchling'/'PCY', 'PCY:Spiketail Hatchling:spiketail hatchling').
card_rarity('spiketail hatchling'/'PCY', 'Common').
card_artist('spiketail hatchling'/'PCY', 'Greg Staples').
card_number('spiketail hatchling'/'PCY', '49').
card_flavor_text('spiketail hatchling'/'PCY', 'It dodges waves of water to prepare for waves of magic.').
card_multiverse_id('spiketail hatchling'/'PCY', '24581').

card_in_set('spitting spider', 'PCY').
card_original_type('spitting spider'/'PCY', 'Creature — Spider').
card_original_text('spitting spider'/'PCY', 'Spitting Spider may block as though it had flying.\nSacrifice a land: Spitting Spider deals 1 damage to each creature with flying.').
card_first_print('spitting spider', 'PCY').
card_image_name('spitting spider'/'PCY', 'spitting spider').
card_uid('spitting spider'/'PCY', 'PCY:Spitting Spider:spitting spider').
card_rarity('spitting spider'/'PCY', 'Uncommon').
card_artist('spitting spider'/'PCY', 'Edward P. Beard, Jr.').
card_number('spitting spider'/'PCY', '125').
card_multiverse_id('spitting spider'/'PCY', '24666').

card_in_set('spore frog', 'PCY').
card_original_type('spore frog'/'PCY', 'Creature — Frog').
card_original_text('spore frog'/'PCY', 'Sacrifice Spore Frog: Prevent all combat damage that would be dealt this turn.').
card_first_print('spore frog', 'PCY').
card_image_name('spore frog'/'PCY', 'spore frog').
card_uid('spore frog'/'PCY', 'PCY:Spore Frog:spore frog').
card_rarity('spore frog'/'PCY', 'Common').
card_artist('spore frog'/'PCY', 'Donato Giancola').
card_number('spore frog'/'PCY', '126').
card_flavor_text('spore frog'/'PCY', 'The end of one life is merely the beginning of thousands more.').
card_multiverse_id('spore frog'/'PCY', '24654').

card_in_set('spur grappler', 'PCY').
card_original_type('spur grappler'/'PCY', 'Creature — Beast').
card_original_text('spur grappler'/'PCY', 'Spur Grappler gets +2/+1 as long as you control no untapped lands.').
card_first_print('spur grappler', 'PCY').
card_image_name('spur grappler'/'PCY', 'spur grappler').
card_uid('spur grappler'/'PCY', 'PCY:Spur Grappler:spur grappler').
card_rarity('spur grappler'/'PCY', 'Common').
card_artist('spur grappler'/'PCY', 'Randy Gallegos').
card_number('spur grappler'/'PCY', '104').
card_flavor_text('spur grappler'/'PCY', 'Grapplers hunt in packs, driving their victims over steep mountain cliffs.').
card_multiverse_id('spur grappler'/'PCY', '24625').

card_in_set('squirrel wrangler', 'PCY').
card_original_type('squirrel wrangler'/'PCY', 'Creature — Druid').
card_original_text('squirrel wrangler'/'PCY', '{1}{G}, Sacrifice a land: Put two 1/1 green Squirrel creature tokens into play.\n{1}{G}, Sacrifice a land: All Squirrels get +1/+1 until end of turn.').
card_first_print('squirrel wrangler', 'PCY').
card_image_name('squirrel wrangler'/'PCY', 'squirrel wrangler').
card_uid('squirrel wrangler'/'PCY', 'PCY:Squirrel Wrangler:squirrel wrangler').
card_rarity('squirrel wrangler'/'PCY', 'Rare').
card_artist('squirrel wrangler'/'PCY', 'Carl Critchlow').
card_number('squirrel wrangler'/'PCY', '127').
card_flavor_text('squirrel wrangler'/'PCY', '\"Gnawed to death. Bad way to go.\"').
card_multiverse_id('squirrel wrangler'/'PCY', '24672').

card_in_set('steal strength', 'PCY').
card_original_type('steal strength'/'PCY', 'Instant').
card_original_text('steal strength'/'PCY', 'Target creature gets +1/+1 until end of turn. Another target creature gets -1/-1 until end of turn.').
card_first_print('steal strength', 'PCY').
card_image_name('steal strength'/'PCY', 'steal strength').
card_uid('steal strength'/'PCY', 'PCY:Steal Strength:steal strength').
card_rarity('steal strength'/'PCY', 'Common').
card_artist('steal strength'/'PCY', 'D. Alexander Gregory').
card_number('steal strength'/'PCY', '79').
card_flavor_text('steal strength'/'PCY', 'With each grunt of pain from Haddad, Greel\'s grin widened.').
card_multiverse_id('steal strength'/'PCY', '24608').

card_in_set('stormwatch eagle', 'PCY').
card_original_type('stormwatch eagle'/'PCY', 'Creature — Bird').
card_original_text('stormwatch eagle'/'PCY', 'Flying\nSacrifice a land: Return Stormwatch Eagle to its owner\'s hand.').
card_first_print('stormwatch eagle', 'PCY').
card_image_name('stormwatch eagle'/'PCY', 'stormwatch eagle').
card_uid('stormwatch eagle'/'PCY', 'PCY:Stormwatch Eagle:stormwatch eagle').
card_rarity('stormwatch eagle'/'PCY', 'Common').
card_artist('stormwatch eagle'/'PCY', 'Aaron Boyd').
card_number('stormwatch eagle'/'PCY', '50').
card_flavor_text('stormwatch eagle'/'PCY', 'When eagles leave the sky, sailors leave the sea.').
card_multiverse_id('stormwatch eagle'/'PCY', '24583').

card_in_set('sunken field', 'PCY').
card_original_type('sunken field'/'PCY', 'Enchant Land').
card_original_text('sunken field'/'PCY', 'Enchanted land has \"{T}: Counter target spell unless its controller pays {1}.\"').
card_first_print('sunken field', 'PCY').
card_image_name('sunken field'/'PCY', 'sunken field').
card_uid('sunken field'/'PCY', 'PCY:Sunken Field:sunken field').
card_rarity('sunken field'/'PCY', 'Uncommon').
card_artist('sunken field'/'PCY', 'Donato Giancola').
card_number('sunken field'/'PCY', '51').
card_flavor_text('sunken field'/'PCY', 'The land is heavy with promise, the harvest ripe with betrayal.').
card_multiverse_id('sunken field'/'PCY', '24575').

card_in_set('sword dancer', 'PCY').
card_original_type('sword dancer'/'PCY', 'Creature — Rebel').
card_original_text('sword dancer'/'PCY', '{W}{W} Target attacking creature gets -1/-0 until end of turn.').
card_first_print('sword dancer', 'PCY').
card_image_name('sword dancer'/'PCY', 'sword dancer').
card_uid('sword dancer'/'PCY', 'PCY:Sword Dancer:sword dancer').
card_rarity('sword dancer'/'PCY', 'Uncommon').
card_artist('sword dancer'/'PCY', 'Roger Raupp').
card_number('sword dancer'/'PCY', '25').
card_flavor_text('sword dancer'/'PCY', 'Most soldiers think of the sword as a weapon. In the hands of Zho monks, swords are also the strongest of shields.').
card_multiverse_id('sword dancer'/'PCY', '24544').

card_in_set('task mage assembly', 'PCY').
card_original_type('task mage assembly'/'PCY', 'Enchantment').
card_original_text('task mage assembly'/'PCY', 'When there are no creatures in play, sacrifice Task Mage Assembly.\n{2}: Task Mage Assembly deals 1 damage to target creature. Any player may play this ability but only any time he or she could play a sorcery.').
card_first_print('task mage assembly', 'PCY').
card_image_name('task mage assembly'/'PCY', 'task mage assembly').
card_uid('task mage assembly'/'PCY', 'PCY:Task Mage Assembly:task mage assembly').
card_rarity('task mage assembly'/'PCY', 'Rare').
card_artist('task mage assembly'/'PCY', 'Val Mayerik').
card_number('task mage assembly'/'PCY', '105').
card_multiverse_id('task mage assembly'/'PCY', '24651').

card_in_set('thresher beast', 'PCY').
card_original_type('thresher beast'/'PCY', 'Creature — Beast').
card_original_text('thresher beast'/'PCY', 'Whenever Thresher Beast becomes blocked, defending player sacrifices a land.').
card_first_print('thresher beast', 'PCY').
card_image_name('thresher beast'/'PCY', 'thresher beast').
card_uid('thresher beast'/'PCY', 'PCY:Thresher Beast:thresher beast').
card_rarity('thresher beast'/'PCY', 'Common').
card_artist('thresher beast'/'PCY', 'Jeff Easley').
card_number('thresher beast'/'PCY', '128').
card_flavor_text('thresher beast'/'PCY', 'It destroys everything in its path, including the path itself.').
card_multiverse_id('thresher beast'/'PCY', '24658').

card_in_set('thrive', 'PCY').
card_original_type('thrive'/'PCY', 'Sorcery').
card_original_text('thrive'/'PCY', 'Put a +1/+1 counter on each of X target creatures.').
card_first_print('thrive', 'PCY').
card_image_name('thrive'/'PCY', 'thrive').
card_uid('thrive'/'PCY', 'PCY:Thrive:thrive').
card_rarity('thrive'/'PCY', 'Common').
card_artist('thrive'/'PCY', 'Mike Ploog').
card_number('thrive'/'PCY', '129').
card_flavor_text('thrive'/'PCY', '\"We wait and watch, and as we prepare for war, we grow strong.\"').
card_multiverse_id('thrive'/'PCY', '24669').

card_in_set('trenching steed', 'PCY').
card_original_type('trenching steed'/'PCY', 'Creature — Rebel').
card_original_text('trenching steed'/'PCY', 'Sacrifice a land: Trenching Steed gets +0/+3 until end of turn.').
card_first_print('trenching steed', 'PCY').
card_image_name('trenching steed'/'PCY', 'trenching steed').
card_uid('trenching steed'/'PCY', 'PCY:Trenching Steed:trenching steed').
card_rarity('trenching steed'/'PCY', 'Common').
card_artist('trenching steed'/'PCY', 'Greg & Tim Hildebrandt').
card_number('trenching steed'/'PCY', '26').
card_flavor_text('trenching steed'/'PCY', 'The Keldons took several of the steeds as trophies of war. They\'d never seen such a light beast stand so firm.').
card_multiverse_id('trenching steed'/'PCY', '24546').

card_in_set('troubled healer', 'PCY').
card_original_type('troubled healer'/'PCY', 'Creature — Cleric').
card_original_text('troubled healer'/'PCY', 'Sacrifice a land: Prevent the next 2 damage that would be dealt to target creature or player this turn.').
card_first_print('troubled healer', 'PCY').
card_image_name('troubled healer'/'PCY', 'troubled healer').
card_uid('troubled healer'/'PCY', 'PCY:Troubled Healer:troubled healer').
card_rarity('troubled healer'/'PCY', 'Common').
card_artist('troubled healer'/'PCY', 'Terese Nielsen').
card_number('troubled healer'/'PCY', '27').
card_flavor_text('troubled healer'/'PCY', '\"Which is worse: nothing to defend, or no one to defend it?\"').
card_multiverse_id('troubled healer'/'PCY', '24547').

card_in_set('troublesome spirit', 'PCY').
card_original_type('troublesome spirit'/'PCY', 'Creature — Spirit').
card_original_text('troublesome spirit'/'PCY', 'Flying\nAt the end of your turn, tap all lands you control.').
card_first_print('troublesome spirit', 'PCY').
card_image_name('troublesome spirit'/'PCY', 'troublesome spirit').
card_uid('troublesome spirit'/'PCY', 'PCY:Troublesome Spirit:troublesome spirit').
card_rarity('troublesome spirit'/'PCY', 'Rare').
card_artist('troublesome spirit'/'PCY', 'Adam Rex').
card_number('troublesome spirit'/'PCY', '52').
card_flavor_text('troublesome spirit'/'PCY', 'As erratic and turbulent as a summer squall.').
card_multiverse_id('troublesome spirit'/'PCY', '24584').

card_in_set('verdant field', 'PCY').
card_original_type('verdant field'/'PCY', 'Enchant Land').
card_original_text('verdant field'/'PCY', 'Enchanted land has \"{T}: Target creature gets +1/+1 until end of turn.\"').
card_first_print('verdant field', 'PCY').
card_image_name('verdant field'/'PCY', 'verdant field').
card_uid('verdant field'/'PCY', 'PCY:Verdant Field:verdant field').
card_rarity('verdant field'/'PCY', 'Uncommon').
card_artist('verdant field'/'PCY', 'Ron Spears').
card_number('verdant field'/'PCY', '130').
card_flavor_text('verdant field'/'PCY', 'Jolrael tends the land so that the land will tend the beasts.').
card_multiverse_id('verdant field'/'PCY', '24662').

card_in_set('veteran brawlers', 'PCY').
card_original_type('veteran brawlers'/'PCY', 'Creature — Soldier').
card_original_text('veteran brawlers'/'PCY', 'Veteran Brawlers can\'t attack if defending player controls an untapped land.\nVeteran Brawlers can\'t block if you control an untapped land.').
card_first_print('veteran brawlers', 'PCY').
card_image_name('veteran brawlers'/'PCY', 'veteran brawlers').
card_uid('veteran brawlers'/'PCY', 'PCY:Veteran Brawlers:veteran brawlers').
card_rarity('veteran brawlers'/'PCY', 'Rare').
card_artist('veteran brawlers'/'PCY', 'Paolo Parente').
card_number('veteran brawlers'/'PCY', '106').
card_flavor_text('veteran brawlers'/'PCY', 'Death awaits them on both fronts.').
card_multiverse_id('veteran brawlers'/'PCY', '24645').

card_in_set('vintara elephant', 'PCY').
card_original_type('vintara elephant'/'PCY', 'Creature — Elephant').
card_original_text('vintara elephant'/'PCY', 'Trample\n{3}: Vintara Elephant loses trample until end of turn. Any player may play this ability.').
card_first_print('vintara elephant', 'PCY').
card_image_name('vintara elephant'/'PCY', 'vintara elephant').
card_uid('vintara elephant'/'PCY', 'PCY:Vintara Elephant:vintara elephant').
card_rarity('vintara elephant'/'PCY', 'Common').
card_artist('vintara elephant'/'PCY', 'Tony Szczudlo').
card_number('vintara elephant'/'PCY', '131').
card_flavor_text('vintara elephant'/'PCY', 'Trained with the aid of rhystic magic, they become uneasy in its absence.').
card_multiverse_id('vintara elephant'/'PCY', '24652').

card_in_set('vintara snapper', 'PCY').
card_original_type('vintara snapper'/'PCY', 'Creature — Turtle').
card_original_text('vintara snapper'/'PCY', 'Vintara Snapper can\'t be the target of spells or abilities as long as you control no untapped lands.').
card_first_print('vintara snapper', 'PCY').
card_image_name('vintara snapper'/'PCY', 'vintara snapper').
card_uid('vintara snapper'/'PCY', 'PCY:Vintara Snapper:vintara snapper').
card_rarity('vintara snapper'/'PCY', 'Uncommon').
card_artist('vintara snapper'/'PCY', 'Joel Biske').
card_number('vintara snapper'/'PCY', '132').
card_flavor_text('vintara snapper'/'PCY', '\"The snapper is still when all else moves\nAnd strikes when all are dozing.\"\n—Tales of the Vintara Forest').
card_multiverse_id('vintara snapper'/'PCY', '24656').

card_in_set('vitalizing wind', 'PCY').
card_original_type('vitalizing wind'/'PCY', 'Instant').
card_original_text('vitalizing wind'/'PCY', 'Creatures you control get +7/+7 until end of turn.').
card_first_print('vitalizing wind', 'PCY').
card_image_name('vitalizing wind'/'PCY', 'vitalizing wind').
card_uid('vitalizing wind'/'PCY', 'PCY:Vitalizing Wind:vitalizing wind').
card_rarity('vitalizing wind'/'PCY', 'Rare').
card_artist('vitalizing wind'/'PCY', 'Jeff Easley').
card_number('vitalizing wind'/'PCY', '133').
card_flavor_text('vitalizing wind'/'PCY', '\"The fifth wind of ascension is Exalter, fulfilling Keld\'s destiny.\"\n—Keld Triumphant').
card_multiverse_id('vitalizing wind'/'PCY', '24677').

card_in_set('wall of vipers', 'PCY').
card_original_type('wall of vipers'/'PCY', 'Creature — Wall').
card_original_text('wall of vipers'/'PCY', '(Walls can\'t attack.)\n{3}: Destroy Wall of Vipers and target creature it\'s blocking. Any player may play this ability.').
card_first_print('wall of vipers', 'PCY').
card_image_name('wall of vipers'/'PCY', 'wall of vipers').
card_uid('wall of vipers'/'PCY', 'PCY:Wall of Vipers:wall of vipers').
card_rarity('wall of vipers'/'PCY', 'Uncommon').
card_artist('wall of vipers'/'PCY', 'Marc Fishman').
card_number('wall of vipers'/'PCY', '80').
card_flavor_text('wall of vipers'/'PCY', 'What wall can never be climbed, but is always scaled?\n—Nakaya riddle').
card_multiverse_id('wall of vipers'/'PCY', '24609').

card_in_set('well of discovery', 'PCY').
card_original_type('well of discovery'/'PCY', 'Artifact').
card_original_text('well of discovery'/'PCY', 'At the end of your turn, if you control no untapped lands, draw a card.').
card_first_print('well of discovery', 'PCY').
card_image_name('well of discovery'/'PCY', 'well of discovery').
card_uid('well of discovery'/'PCY', 'PCY:Well of Discovery:well of discovery').
card_rarity('well of discovery'/'PCY', 'Rare').
card_artist('well of discovery'/'PCY', 'Alan Rabinowitz').
card_number('well of discovery'/'PCY', '140').
card_flavor_text('well of discovery'/'PCY', 'When you have given everything, then you are capable of anything.\n—Well inscription').
card_multiverse_id('well of discovery'/'PCY', '24684').

card_in_set('well of life', 'PCY').
card_original_type('well of life'/'PCY', 'Artifact').
card_original_text('well of life'/'PCY', 'At the end of your turn, if you control no untapped lands, you gain 2 life.').
card_first_print('well of life', 'PCY').
card_image_name('well of life'/'PCY', 'well of life').
card_uid('well of life'/'PCY', 'PCY:Well of Life:well of life').
card_rarity('well of life'/'PCY', 'Uncommon').
card_artist('well of life'/'PCY', 'Tom Wänerstrand').
card_number('well of life'/'PCY', '141').
card_flavor_text('well of life'/'PCY', 'When you have given everything, then you have everything to gain.\n—Well inscription').
card_multiverse_id('well of life'/'PCY', '24681').

card_in_set('whip sergeant', 'PCY').
card_original_type('whip sergeant'/'PCY', 'Creature — Soldier').
card_original_text('whip sergeant'/'PCY', '{R} Target creature gains haste until end of turn. (It may attack and {T} the turn it comes under your control.)').
card_first_print('whip sergeant', 'PCY').
card_image_name('whip sergeant'/'PCY', 'whip sergeant').
card_uid('whip sergeant'/'PCY', 'PCY:Whip Sergeant:whip sergeant').
card_rarity('whip sergeant'/'PCY', 'Uncommon').
card_artist('whip sergeant'/'PCY', 'Paolo Parente').
card_number('whip sergeant'/'PCY', '107').
card_flavor_text('whip sergeant'/'PCY', 'Keldon soldiers learn to move faster than the whip.').
card_multiverse_id('whip sergeant'/'PCY', '24628').

card_in_set('whipstitched zombie', 'PCY').
card_original_type('whipstitched zombie'/'PCY', 'Creature — Zombie').
card_original_text('whipstitched zombie'/'PCY', 'At the beginning of your upkeep, sacrifice Whipstitched Zombie unless you pay {B}.').
card_first_print('whipstitched zombie', 'PCY').
card_image_name('whipstitched zombie'/'PCY', 'whipstitched zombie').
card_uid('whipstitched zombie'/'PCY', 'PCY:Whipstitched Zombie:whipstitched zombie').
card_rarity('whipstitched zombie'/'PCY', 'Common').
card_artist('whipstitched zombie'/'PCY', 'Mark Tedin').
card_number('whipstitched zombie'/'PCY', '81').
card_flavor_text('whipstitched zombie'/'PCY', 'Pity the tailor.').
card_multiverse_id('whipstitched zombie'/'PCY', '24603').

card_in_set('wild might', 'PCY').
card_original_type('wild might'/'PCY', 'Instant').
card_original_text('wild might'/'PCY', 'Target creature gets +1/+1 until end of turn. That creature gets an additional +4/+4 until end of turn unless any player pays {2}.').
card_first_print('wild might', 'PCY').
card_image_name('wild might'/'PCY', 'wild might').
card_uid('wild might'/'PCY', 'PCY:Wild Might:wild might').
card_rarity('wild might'/'PCY', 'Common').
card_artist('wild might'/'PCY', 'Carl Critchlow').
card_number('wild might'/'PCY', '134').
card_flavor_text('wild might'/'PCY', 'The hunted can become the fiercest hunters.').
card_multiverse_id('wild might'/'PCY', '24661').

card_in_set('windscouter', 'PCY').
card_original_type('windscouter'/'PCY', 'Creature — Ship').
card_original_text('windscouter'/'PCY', 'Flying\nWhenever Windscouter attacks or blocks, return it to its owner\'s hand at end of combat.').
card_first_print('windscouter', 'PCY').
card_image_name('windscouter'/'PCY', 'windscouter').
card_uid('windscouter'/'PCY', 'PCY:Windscouter:windscouter').
card_rarity('windscouter'/'PCY', 'Uncommon').
card_artist('windscouter'/'PCY', 'Brian Snõddy').
card_number('windscouter'/'PCY', '53').
card_flavor_text('windscouter'/'PCY', 'These nimble ships were designed for hit-and-run tactics.').
card_multiverse_id('windscouter'/'PCY', '24585').

card_in_set('wing storm', 'PCY').
card_original_type('wing storm'/'PCY', 'Sorcery').
card_original_text('wing storm'/'PCY', 'Wing Storm deals X damage to each player, where X is twice the number of creatures with flying that player controls.').
card_first_print('wing storm', 'PCY').
card_image_name('wing storm'/'PCY', 'wing storm').
card_uid('wing storm'/'PCY', 'PCY:Wing Storm:wing storm').
card_rarity('wing storm'/'PCY', 'Uncommon').
card_artist('wing storm'/'PCY', 'Heather Hudson').
card_number('wing storm'/'PCY', '135').
card_flavor_text('wing storm'/'PCY', 'A thousand wings beating as one can choke the sky itself.').
card_multiverse_id('wing storm'/'PCY', '25523').

card_in_set('wintermoon mesa', 'PCY').
card_original_type('wintermoon mesa'/'PCY', 'Land').
card_original_text('wintermoon mesa'/'PCY', 'Wintermoon Mesa comes into play tapped.\n{T}: Add one colorless mana to your mana pool.\n{2}, {T}, Sacrifice Wintermoon Mesa: Tap two target lands.').
card_first_print('wintermoon mesa', 'PCY').
card_image_name('wintermoon mesa'/'PCY', 'wintermoon mesa').
card_uid('wintermoon mesa'/'PCY', 'PCY:Wintermoon Mesa:wintermoon mesa').
card_rarity('wintermoon mesa'/'PCY', 'Rare').
card_artist('wintermoon mesa'/'PCY', 'Tom Wänerstrand').
card_number('wintermoon mesa'/'PCY', '143').
card_flavor_text('wintermoon mesa'/'PCY', 'Dark is light and day is night\nWhen the winter moon shines.').
card_multiverse_id('wintermoon mesa'/'PCY', '23318').

card_in_set('withdraw', 'PCY').
card_original_type('withdraw'/'PCY', 'Instant').
card_original_text('withdraw'/'PCY', 'Return target creature to its owner\'s hand. Then return another target creature to its owner\'s hand unless its controller pays {1}.').
card_first_print('withdraw', 'PCY').
card_image_name('withdraw'/'PCY', 'withdraw').
card_uid('withdraw'/'PCY', 'PCY:Withdraw:withdraw').
card_rarity('withdraw'/'PCY', 'Common').
card_artist('withdraw'/'PCY', 'Adam Rex').
card_number('withdraw'/'PCY', '54').
card_flavor_text('withdraw'/'PCY', '\"We outnumber them! Charge! Charge! Hey, where\'d you guys go? Retreat! Retreat!\"').
card_multiverse_id('withdraw'/'PCY', '24580').

card_in_set('zerapa minotaur', 'PCY').
card_original_type('zerapa minotaur'/'PCY', 'Creature — Minotaur').
card_original_text('zerapa minotaur'/'PCY', 'First strike\n{2}: Zerapa Minotaur loses first strike until end of turn. Any player may play this ability.').
card_first_print('zerapa minotaur', 'PCY').
card_image_name('zerapa minotaur'/'PCY', 'zerapa minotaur').
card_uid('zerapa minotaur'/'PCY', 'PCY:Zerapa Minotaur:zerapa minotaur').
card_rarity('zerapa minotaur'/'PCY', 'Common').
card_artist('zerapa minotaur'/'PCY', 'Mark Zug').
card_number('zerapa minotaur'/'PCY', '108').
card_flavor_text('zerapa minotaur'/'PCY', 'There\'s no difference between bone and crystal when it\'s piercing your throat.').
card_multiverse_id('zerapa minotaur'/'PCY', '24630').
