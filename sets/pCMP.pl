% Champs and States

set('pCMP').
set_name('pCMP', 'Champs and States').
set_release_date('pCMP', '2006-03-18').
set_border('pCMP', 'black').
set_type('pCMP', 'promo').

card_in_set('blood knight', 'pCMP').
card_original_type('blood knight'/'pCMP', 'Creature — Human Knight').
card_original_text('blood knight'/'pCMP', '').
card_first_print('blood knight', 'pCMP').
card_image_name('blood knight'/'pCMP', 'blood knight').
card_uid('blood knight'/'pCMP', 'pCMP:Blood Knight:blood knight').
card_rarity('blood knight'/'pCMP', 'Special').
card_artist('blood knight'/'pCMP', 'Matt Cavotta').
card_number('blood knight'/'pCMP', '7').

card_in_set('bramblewood paragon', 'pCMP').
card_original_type('bramblewood paragon'/'pCMP', 'Creature — Elf Warrior').
card_original_text('bramblewood paragon'/'pCMP', '').
card_first_print('bramblewood paragon', 'pCMP').
card_image_name('bramblewood paragon'/'pCMP', 'bramblewood paragon').
card_uid('bramblewood paragon'/'pCMP', 'pCMP:Bramblewood Paragon:bramblewood paragon').
card_rarity('bramblewood paragon'/'pCMP', 'Special').
card_artist('bramblewood paragon'/'pCMP', 'Jim Murray').
card_number('bramblewood paragon'/'pCMP', '11').

card_in_set('doran, the siege tower', 'pCMP').
card_original_type('doran, the siege tower'/'pCMP', 'Legendary Creature — Treefolk Shaman').
card_original_text('doran, the siege tower'/'pCMP', '').
card_first_print('doran, the siege tower', 'pCMP').
card_image_name('doran, the siege tower'/'pCMP', 'doran, the siege tower').
card_uid('doran, the siege tower'/'pCMP', 'pCMP:Doran, the Siege Tower:doran, the siege tower').
card_rarity('doran, the siege tower'/'pCMP', 'Special').
card_artist('doran, the siege tower'/'pCMP', 'Mark Zug').
card_number('doran, the siege tower'/'pCMP', '10').

card_in_set('electrolyze', 'pCMP').
card_original_type('electrolyze'/'pCMP', 'Instant').
card_original_text('electrolyze'/'pCMP', '').
card_image_name('electrolyze'/'pCMP', 'electrolyze').
card_uid('electrolyze'/'pCMP', 'pCMP:Electrolyze:electrolyze').
card_rarity('electrolyze'/'pCMP', 'Special').
card_artist('electrolyze'/'pCMP', 'Zoltan Boros & Gabor Szikszai').
card_number('electrolyze'/'pCMP', '1').

card_in_set('groundbreaker', 'pCMP').
card_original_type('groundbreaker'/'pCMP', 'Creature — Elemental').
card_original_text('groundbreaker'/'pCMP', '').
card_first_print('groundbreaker', 'pCMP').
card_image_name('groundbreaker'/'pCMP', 'groundbreaker').
card_uid('groundbreaker'/'pCMP', 'pCMP:Groundbreaker:groundbreaker').
card_rarity('groundbreaker'/'pCMP', 'Special').
card_artist('groundbreaker'/'pCMP', 'Matt Cavotta').
card_number('groundbreaker'/'pCMP', '8').

card_in_set('imperious perfect', 'pCMP').
card_original_type('imperious perfect'/'pCMP', 'Creature — Elf Warrior').
card_original_text('imperious perfect'/'pCMP', '').
card_first_print('imperious perfect', 'pCMP').
card_image_name('imperious perfect'/'pCMP', 'imperious perfect').
card_uid('imperious perfect'/'pCMP', 'pCMP:Imperious Perfect:imperious perfect').
card_rarity('imperious perfect'/'pCMP', 'Special').
card_artist('imperious perfect'/'pCMP', 'Scott M. Fischer').
card_number('imperious perfect'/'pCMP', '9').

card_in_set('mutavault', 'pCMP').
card_original_type('mutavault'/'pCMP', 'Land').
card_original_text('mutavault'/'pCMP', '').
card_first_print('mutavault', 'pCMP').
card_image_name('mutavault'/'pCMP', 'mutavault').
card_uid('mutavault'/'pCMP', 'pCMP:Mutavault:mutavault').
card_rarity('mutavault'/'pCMP', 'Special').
card_artist('mutavault'/'pCMP', 'Fred Fields').
card_number('mutavault'/'pCMP', '12').

card_in_set('niv-mizzet, the firemind', 'pCMP').
card_original_type('niv-mizzet, the firemind'/'pCMP', 'Legendary Creature — Dragon Wizard').
card_original_text('niv-mizzet, the firemind'/'pCMP', '').
card_image_name('niv-mizzet, the firemind'/'pCMP', 'niv-mizzet, the firemind').
card_uid('niv-mizzet, the firemind'/'pCMP', 'pCMP:Niv-Mizzet, the Firemind:niv-mizzet, the firemind').
card_rarity('niv-mizzet, the firemind'/'pCMP', 'Special').
card_artist('niv-mizzet, the firemind'/'pCMP', 'Todd Lockwood').
card_number('niv-mizzet, the firemind'/'pCMP', '2').

card_in_set('rakdos guildmage', 'pCMP').
card_original_type('rakdos guildmage'/'pCMP', 'Creature — Zombie Shaman').
card_original_text('rakdos guildmage'/'pCMP', '').
card_first_print('rakdos guildmage', 'pCMP').
card_image_name('rakdos guildmage'/'pCMP', 'rakdos guildmage').
card_uid('rakdos guildmage'/'pCMP', 'pCMP:Rakdos Guildmage:rakdos guildmage').
card_rarity('rakdos guildmage'/'pCMP', 'Special').
card_artist('rakdos guildmage'/'pCMP', 'Jeremy Jarvis').
card_number('rakdos guildmage'/'pCMP', '3').

card_in_set('serra avenger', 'pCMP').
card_original_type('serra avenger'/'pCMP', 'Creature — Angel').
card_original_text('serra avenger'/'pCMP', '').
card_first_print('serra avenger', 'pCMP').
card_image_name('serra avenger'/'pCMP', 'serra avenger').
card_uid('serra avenger'/'pCMP', 'pCMP:Serra Avenger:serra avenger').
card_rarity('serra avenger'/'pCMP', 'Special').
card_artist('serra avenger'/'pCMP', 'Scott M. Fischer').
card_number('serra avenger'/'pCMP', '6').

card_in_set('urza\'s factory', 'pCMP').
card_original_type('urza\'s factory'/'pCMP', 'Land — Urza’s').
card_original_text('urza\'s factory'/'pCMP', '').
card_first_print('urza\'s factory', 'pCMP').
card_image_name('urza\'s factory'/'pCMP', 'urza\'s factory').
card_uid('urza\'s factory'/'pCMP', 'pCMP:Urza\'s Factory:urza\'s factory').
card_rarity('urza\'s factory'/'pCMP', 'Special').
card_artist('urza\'s factory'/'pCMP', 'Mark Tedin').
card_number('urza\'s factory'/'pCMP', '5').

card_in_set('voidslime', 'pCMP').
card_original_type('voidslime'/'pCMP', 'Instant').
card_original_text('voidslime'/'pCMP', '').
card_first_print('voidslime', 'pCMP').
card_image_name('voidslime'/'pCMP', 'voidslime').
card_uid('voidslime'/'pCMP', 'pCMP:Voidslime:voidslime').
card_rarity('voidslime'/'pCMP', 'Special').
card_artist('voidslime'/'pCMP', 'Jim Murray').
card_number('voidslime'/'pCMP', '4').
