% Limited Edition Alpha

set('LEA').
set_name('LEA', 'Limited Edition Alpha').
set_release_date('LEA', '1993-08-05').
set_border('LEA', 'black').
set_type('LEA', 'core').

card_in_set('air elemental', 'LEA').
card_original_type('air elemental'/'LEA', 'Summon — Elemental').
card_original_text('air elemental'/'LEA', 'Flying').
card_first_print('air elemental', 'LEA').
card_image_name('air elemental'/'LEA', 'air elemental').
card_uid('air elemental'/'LEA', 'LEA:Air Elemental:air elemental').
card_rarity('air elemental'/'LEA', 'Uncommon').
card_artist('air elemental'/'LEA', 'Richard Thomas').
card_flavor_text('air elemental'/'LEA', 'These spirits of the air are winsome and wild, and cannot be truly contained. Only marginally intelligent, they often substitute whimsy for strategy, delighting in mischief and mayhem.').
card_multiverse_id('air elemental'/'LEA', '94').

card_in_set('ancestral recall', 'LEA').
card_original_type('ancestral recall'/'LEA', 'Instant').
card_original_text('ancestral recall'/'LEA', 'Draw 3 cards or force opponent to draw 3 cards.').
card_first_print('ancestral recall', 'LEA').
card_image_name('ancestral recall'/'LEA', 'ancestral recall').
card_uid('ancestral recall'/'LEA', 'LEA:Ancestral Recall:ancestral recall').
card_rarity('ancestral recall'/'LEA', 'Rare').
card_artist('ancestral recall'/'LEA', 'Mark Poole').
card_multiverse_id('ancestral recall'/'LEA', '95').

card_in_set('animate artifact', 'LEA').
card_original_type('animate artifact'/'LEA', 'Enchant Non-Creature Artifact').
card_original_text('animate artifact'/'LEA', 'Target artifact is now a creature with both power and toughness equal to its casting cost; target retains all its original abilities as well. This will destroy artifacts with 0 casting cost.').
card_first_print('animate artifact', 'LEA').
card_image_name('animate artifact'/'LEA', 'animate artifact').
card_uid('animate artifact'/'LEA', 'LEA:Animate Artifact:animate artifact').
card_rarity('animate artifact'/'LEA', 'Uncommon').
card_artist('animate artifact'/'LEA', 'Douglas Shuler').
card_multiverse_id('animate artifact'/'LEA', '96').

card_in_set('animate dead', 'LEA').
card_original_type('animate dead'/'LEA', 'Enchant Dead Creature').
card_original_text('animate dead'/'LEA', 'Any creature in either player\'s graveyard comes into play on your side with -1 to its original power. If this enchantment is removed, or at end of game, target creature is returned to its owner\'s graveyard. Target creature may be killed as normal.').
card_first_print('animate dead', 'LEA').
card_image_name('animate dead'/'LEA', 'animate dead').
card_uid('animate dead'/'LEA', 'LEA:Animate Dead:animate dead').
card_rarity('animate dead'/'LEA', 'Uncommon').
card_artist('animate dead'/'LEA', 'Anson Maddocks').
card_multiverse_id('animate dead'/'LEA', '48').

card_in_set('animate wall', 'LEA').
card_original_type('animate wall'/'LEA', 'Enchant Wall').
card_original_text('animate wall'/'LEA', 'Target wall can now attack. Target wall\'s power and toughness are unchanged, even if its power is 0.').
card_first_print('animate wall', 'LEA').
card_image_name('animate wall'/'LEA', 'animate wall').
card_uid('animate wall'/'LEA', 'LEA:Animate Wall:animate wall').
card_rarity('animate wall'/'LEA', 'Rare').
card_artist('animate wall'/'LEA', 'Dan Frazier').
card_multiverse_id('animate wall'/'LEA', '232').

card_in_set('ankh of mishra', 'LEA').
card_original_type('ankh of mishra'/'LEA', 'Continuous Artifact').
card_original_text('ankh of mishra'/'LEA', 'Ankh does 2 damage to anyone who puts a new land into play.').
card_first_print('ankh of mishra', 'LEA').
card_image_name('ankh of mishra'/'LEA', 'ankh of mishra').
card_uid('ankh of mishra'/'LEA', 'LEA:Ankh of Mishra:ankh of mishra').
card_rarity('ankh of mishra'/'LEA', 'Rare').
card_artist('ankh of mishra'/'LEA', 'Amy Weber').
card_multiverse_id('ankh of mishra'/'LEA', '1').

card_in_set('armageddon', 'LEA').
card_original_type('armageddon'/'LEA', 'Sorcery').
card_original_text('armageddon'/'LEA', 'All lands in play are destroyed.').
card_first_print('armageddon', 'LEA').
card_image_name('armageddon'/'LEA', 'armageddon').
card_uid('armageddon'/'LEA', 'LEA:Armageddon:armageddon').
card_rarity('armageddon'/'LEA', 'Rare').
card_artist('armageddon'/'LEA', 'Jesper Myrfors').
card_multiverse_id('armageddon'/'LEA', '233').

card_in_set('aspect of wolf', 'LEA').
card_original_type('aspect of wolf'/'LEA', 'Enchant Creature').
card_original_text('aspect of wolf'/'LEA', 'Target creature\'s power and toughness are increased by half the number of forests you have in play, rounding down for power and up for toughness.').
card_first_print('aspect of wolf', 'LEA').
card_image_name('aspect of wolf'/'LEA', 'aspect of wolf').
card_uid('aspect of wolf'/'LEA', 'LEA:Aspect of Wolf:aspect of wolf').
card_rarity('aspect of wolf'/'LEA', 'Rare').
card_artist('aspect of wolf'/'LEA', 'Jeff A. Menges').
card_multiverse_id('aspect of wolf'/'LEA', '140').

card_in_set('bad moon', 'LEA').
card_original_type('bad moon'/'LEA', 'Enchantment').
card_original_text('bad moon'/'LEA', 'All black creatures in play gain +1/+1.').
card_first_print('bad moon', 'LEA').
card_image_name('bad moon'/'LEA', 'bad moon').
card_uid('bad moon'/'LEA', 'LEA:Bad Moon:bad moon').
card_rarity('bad moon'/'LEA', 'Rare').
card_artist('bad moon'/'LEA', 'Jesper Myrfors').
card_multiverse_id('bad moon'/'LEA', '49').

card_in_set('badlands', 'LEA').
card_original_type('badlands'/'LEA', 'Land').
card_original_text('badlands'/'LEA', 'Counts as both mountains and swamp and is affected by spells that affect either. Tap to add either {R} or {B} to your mana pool.').
card_first_print('badlands', 'LEA').
card_image_name('badlands'/'LEA', 'badlands').
card_uid('badlands'/'LEA', 'LEA:Badlands:badlands').
card_rarity('badlands'/'LEA', 'Rare').
card_artist('badlands'/'LEA', 'Rob Alexander').
card_multiverse_id('badlands'/'LEA', '279').

card_in_set('balance', 'LEA').
card_original_type('balance'/'LEA', 'Sorcery').
card_original_text('balance'/'LEA', 'Whichever player has more lands in play must discard enough lands of his or her choice to equalize the number of lands both players have in play. Cards in hand and creatures in play must be equalized the same way. Creatures lost in this manner may not be regenerated.').
card_first_print('balance', 'LEA').
card_image_name('balance'/'LEA', 'balance').
card_uid('balance'/'LEA', 'LEA:Balance:balance').
card_rarity('balance'/'LEA', 'Rare').
card_artist('balance'/'LEA', 'Mark Poole').
card_multiverse_id('balance'/'LEA', '234').

card_in_set('basalt monolith', 'LEA').
card_original_type('basalt monolith'/'LEA', 'Mono Artifact').
card_original_text('basalt monolith'/'LEA', 'Tap to add 3 colorless mana to your mana pool. Does not untap as normal during untap phase, but can be untapped at any time for 3 mana. Tapping this artifact can be played as an interrupt.').
card_first_print('basalt monolith', 'LEA').
card_image_name('basalt monolith'/'LEA', 'basalt monolith').
card_uid('basalt monolith'/'LEA', 'LEA:Basalt Monolith:basalt monolith').
card_rarity('basalt monolith'/'LEA', 'Uncommon').
card_artist('basalt monolith'/'LEA', 'Jesper Myrfors').
card_multiverse_id('basalt monolith'/'LEA', '2').

card_in_set('bayou', 'LEA').
card_original_type('bayou'/'LEA', 'Land').
card_original_text('bayou'/'LEA', 'Counts as both swamp and forest and is affected by spells that affect either. Tap to add either {B} or {G} to your mana pool.').
card_first_print('bayou', 'LEA').
card_image_name('bayou'/'LEA', 'bayou').
card_uid('bayou'/'LEA', 'LEA:Bayou:bayou').
card_rarity('bayou'/'LEA', 'Rare').
card_artist('bayou'/'LEA', 'Jesper Myrfors').
card_multiverse_id('bayou'/'LEA', '280').

card_in_set('benalish hero', 'LEA').
card_original_type('benalish hero'/'LEA', 'Summon — Hero').
card_original_text('benalish hero'/'LEA', 'Bands').
card_first_print('benalish hero', 'LEA').
card_image_name('benalish hero'/'LEA', 'benalish hero').
card_uid('benalish hero'/'LEA', 'LEA:Benalish Hero:benalish hero').
card_rarity('benalish hero'/'LEA', 'Common').
card_artist('benalish hero'/'LEA', 'Douglas Shuler').
card_flavor_text('benalish hero'/'LEA', 'Benalia has a complex caste system that changes with the lunar year. No matter what the season, the only caste that cannot be attained by either heredity or money is that of the hero.').
card_multiverse_id('benalish hero'/'LEA', '235').

card_in_set('berserk', 'LEA').
card_original_type('berserk'/'LEA', 'Instant').
card_original_text('berserk'/'LEA', 'Until end of turn, target creature\'s current power doubles and it gains trample ability. If it attacks, target creature is destroyed at end of turn. This spell cannot be cast after current turn\'s attack is completed.').
card_first_print('berserk', 'LEA').
card_image_name('berserk'/'LEA', 'berserk').
card_uid('berserk'/'LEA', 'LEA:Berserk:berserk').
card_rarity('berserk'/'LEA', 'Uncommon').
card_artist('berserk'/'LEA', 'Dan Frazier').
card_multiverse_id('berserk'/'LEA', '141').

card_in_set('birds of paradise', 'LEA').
card_original_type('birds of paradise'/'LEA', 'Summon — Mana Birds').
card_original_text('birds of paradise'/'LEA', 'Flying// Tap to add one mana of any color to your mana pool. This tap may be played as an interrupt.').
card_first_print('birds of paradise', 'LEA').
card_image_name('birds of paradise'/'LEA', 'birds of paradise').
card_uid('birds of paradise'/'LEA', 'LEA:Birds of Paradise:birds of paradise').
card_rarity('birds of paradise'/'LEA', 'Rare').
card_artist('birds of paradise'/'LEA', 'Mark Poole').
card_multiverse_id('birds of paradise'/'LEA', '142').

card_in_set('black knight', 'LEA').
card_original_type('black knight'/'LEA', 'Summon — Knight').
card_original_text('black knight'/'LEA', 'Protection from white, first strike').
card_first_print('black knight', 'LEA').
card_image_name('black knight'/'LEA', 'black knight').
card_uid('black knight'/'LEA', 'LEA:Black Knight:black knight').
card_rarity('black knight'/'LEA', 'Uncommon').
card_artist('black knight'/'LEA', 'Jeff A. Menges').
card_flavor_text('black knight'/'LEA', 'Battle doesn\'t need a purpose; the battle is its own purpose. You don\'t ask why a plague spreads or a field burns. Don\'t ask why I fight.').
card_multiverse_id('black knight'/'LEA', '50').

card_in_set('black lotus', 'LEA').
card_original_type('black lotus'/'LEA', 'Mono Artifact').
card_original_text('black lotus'/'LEA', 'Adds 3 mana of any single color of your choice to your mana pool, then is discarded. Tapping this artifact can be played as an interrupt.').
card_first_print('black lotus', 'LEA').
card_image_name('black lotus'/'LEA', 'black lotus').
card_uid('black lotus'/'LEA', 'LEA:Black Lotus:black lotus').
card_rarity('black lotus'/'LEA', 'Rare').
card_artist('black lotus'/'LEA', 'Christopher Rush').
card_multiverse_id('black lotus'/'LEA', '3').

card_in_set('black vise', 'LEA').
card_original_type('black vise'/'LEA', 'Continuous Artifact').
card_original_text('black vise'/'LEA', 'If opponent has more than four cards in hand during upkeep, black vise does 1 damage to opponent for each card in excess of four.').
card_first_print('black vise', 'LEA').
card_image_name('black vise'/'LEA', 'black vise').
card_uid('black vise'/'LEA', 'LEA:Black Vise:black vise').
card_rarity('black vise'/'LEA', 'Uncommon').
card_artist('black vise'/'LEA', 'Richard Thomas').
card_multiverse_id('black vise'/'LEA', '4').

card_in_set('black ward', 'LEA').
card_original_type('black ward'/'LEA', 'Enchant Creature').
card_original_text('black ward'/'LEA', 'Target creature gains protection from black.').
card_first_print('black ward', 'LEA').
card_image_name('black ward'/'LEA', 'black ward').
card_uid('black ward'/'LEA', 'LEA:Black Ward:black ward').
card_rarity('black ward'/'LEA', 'Uncommon').
card_artist('black ward'/'LEA', 'Dan Frazier').
card_multiverse_id('black ward'/'LEA', '236').

card_in_set('blaze of glory', 'LEA').
card_original_type('blaze of glory'/'LEA', 'Instant').
card_original_text('blaze of glory'/'LEA', 'Target defending creature can and must block all attacking creatures it can legally block. For example, a normal non-flying target defender can and must block all normal non-flying attackers at once, but it cannot block any flying attackers. Controller of target defender may distribute damage among attackers as desired. Play before defense is chosen.').
card_first_print('blaze of glory', 'LEA').
card_image_name('blaze of glory'/'LEA', 'blaze of glory').
card_uid('blaze of glory'/'LEA', 'LEA:Blaze of Glory:blaze of glory').
card_rarity('blaze of glory'/'LEA', 'Rare').
card_artist('blaze of glory'/'LEA', 'Richard Thomas').
card_multiverse_id('blaze of glory'/'LEA', '237').

card_in_set('blessing', 'LEA').
card_original_type('blessing'/'LEA', 'Enchant Creature').
card_original_text('blessing'/'LEA', '{W} Target creature gains +1/+1 until end of turn.').
card_first_print('blessing', 'LEA').
card_image_name('blessing'/'LEA', 'blessing').
card_uid('blessing'/'LEA', 'LEA:Blessing:blessing').
card_rarity('blessing'/'LEA', 'Rare').
card_artist('blessing'/'LEA', 'Julie Baroh').
card_multiverse_id('blessing'/'LEA', '238').

card_in_set('blue elemental blast', 'LEA').
card_original_type('blue elemental blast'/'LEA', 'Interrupt').
card_original_text('blue elemental blast'/'LEA', 'Counters a red spell being cast or destroys a red card in play.').
card_first_print('blue elemental blast', 'LEA').
card_image_name('blue elemental blast'/'LEA', 'blue elemental blast').
card_uid('blue elemental blast'/'LEA', 'LEA:Blue Elemental Blast:blue elemental blast').
card_rarity('blue elemental blast'/'LEA', 'Common').
card_artist('blue elemental blast'/'LEA', 'Richard Thomas').
card_multiverse_id('blue elemental blast'/'LEA', '97').

card_in_set('blue ward', 'LEA').
card_original_type('blue ward'/'LEA', 'Enchant Creature').
card_original_text('blue ward'/'LEA', 'Target creature gains protection from blue.').
card_first_print('blue ward', 'LEA').
card_image_name('blue ward'/'LEA', 'blue ward').
card_uid('blue ward'/'LEA', 'LEA:Blue Ward:blue ward').
card_rarity('blue ward'/'LEA', 'Uncommon').
card_artist('blue ward'/'LEA', 'Dan Frazier').
card_multiverse_id('blue ward'/'LEA', '239').

card_in_set('bog wraith', 'LEA').
card_original_type('bog wraith'/'LEA', 'Summon — Wraith').
card_original_text('bog wraith'/'LEA', 'Swampwalk').
card_first_print('bog wraith', 'LEA').
card_image_name('bog wraith'/'LEA', 'bog wraith').
card_uid('bog wraith'/'LEA', 'LEA:Bog Wraith:bog wraith').
card_rarity('bog wraith'/'LEA', 'Uncommon').
card_artist('bog wraith'/'LEA', 'Jeff A. Menges').
card_flavor_text('bog wraith'/'LEA', '\'Twas in the bogs of Cannelbrae\nMy mate did meet an early grave\n\'Twas nothing left for us to save\nIn the peat-filled bogs of Cannelbrae.').
card_multiverse_id('bog wraith'/'LEA', '51').

card_in_set('braingeyser', 'LEA').
card_original_type('braingeyser'/'LEA', 'Sorcery').
card_original_text('braingeyser'/'LEA', 'Draw X cards or force opponent to draw X cards.').
card_first_print('braingeyser', 'LEA').
card_image_name('braingeyser'/'LEA', 'braingeyser').
card_uid('braingeyser'/'LEA', 'LEA:Braingeyser:braingeyser').
card_rarity('braingeyser'/'LEA', 'Rare').
card_artist('braingeyser'/'LEA', 'Mark Tedin').
card_multiverse_id('braingeyser'/'LEA', '98').

card_in_set('burrowing', 'LEA').
card_original_type('burrowing'/'LEA', 'Enchant Creature').
card_original_text('burrowing'/'LEA', 'Target creature gains mountainwalk.').
card_first_print('burrowing', 'LEA').
card_image_name('burrowing'/'LEA', 'burrowing').
card_uid('burrowing'/'LEA', 'LEA:Burrowing:burrowing').
card_rarity('burrowing'/'LEA', 'Uncommon').
card_artist('burrowing'/'LEA', 'Mark Poole').
card_multiverse_id('burrowing'/'LEA', '186').

card_in_set('camouflage', 'LEA').
card_original_type('camouflage'/'LEA', 'Instant').
card_original_text('camouflage'/'LEA', 'You may rearrange your attacking creatures and place them face down, revealing which is which only after defense is chosen. If this results in impossible blocks, such as non-flying creatures blocking flying creatures, illegal blockers cannot block this turn.').
card_first_print('camouflage', 'LEA').
card_image_name('camouflage'/'LEA', 'camouflage').
card_uid('camouflage'/'LEA', 'LEA:Camouflage:camouflage').
card_rarity('camouflage'/'LEA', 'Uncommon').
card_artist('camouflage'/'LEA', 'Jesper Myrfors').
card_multiverse_id('camouflage'/'LEA', '143').

card_in_set('castle', 'LEA').
card_original_type('castle'/'LEA', 'Enchantment').
card_original_text('castle'/'LEA', 'Your untapped creatures gain +0/+2. Attacking creatures lose this bonus.').
card_first_print('castle', 'LEA').
card_image_name('castle'/'LEA', 'castle').
card_uid('castle'/'LEA', 'LEA:Castle:castle').
card_rarity('castle'/'LEA', 'Uncommon').
card_artist('castle'/'LEA', 'Dameon Willich').
card_multiverse_id('castle'/'LEA', '240').

card_in_set('celestial prism', 'LEA').
card_original_type('celestial prism'/'LEA', 'Mono Artifact').
card_original_text('celestial prism'/'LEA', '{2}: Provides 1 mana of any color. This use can be played as an interrupt.').
card_first_print('celestial prism', 'LEA').
card_image_name('celestial prism'/'LEA', 'celestial prism').
card_uid('celestial prism'/'LEA', 'LEA:Celestial Prism:celestial prism').
card_rarity('celestial prism'/'LEA', 'Uncommon').
card_artist('celestial prism'/'LEA', 'Amy Weber').
card_multiverse_id('celestial prism'/'LEA', '5').

card_in_set('channel', 'LEA').
card_original_type('channel'/'LEA', 'Sorcery').
card_original_text('channel'/'LEA', 'Until end of turn, you may add colorless mana to your mana pool for 1 life each. These additions are played with the speed of an interrupt. Life spent this way is not considered damage.').
card_first_print('channel', 'LEA').
card_image_name('channel'/'LEA', 'channel').
card_uid('channel'/'LEA', 'LEA:Channel:channel').
card_rarity('channel'/'LEA', 'Uncommon').
card_artist('channel'/'LEA', 'Richard Thomas').
card_multiverse_id('channel'/'LEA', '144').

card_in_set('chaos orb', 'LEA').
card_original_type('chaos orb'/'LEA', 'Mono Artifact').
card_original_text('chaos orb'/'LEA', '{1}: Flip Chaos Orb onto the playing area from a height of at least one foot. Chaos Orb must turn completely over at least once or it is discarded with no effect. When Chaos Orb lands, any cards in play that it touches are destroyed, as is Chaos Orb.').
card_first_print('chaos orb', 'LEA').
card_image_name('chaos orb'/'LEA', 'chaos orb').
card_uid('chaos orb'/'LEA', 'LEA:Chaos Orb:chaos orb').
card_rarity('chaos orb'/'LEA', 'Rare').
card_artist('chaos orb'/'LEA', 'Mark Tedin').
card_multiverse_id('chaos orb'/'LEA', '6').

card_in_set('chaoslace', 'LEA').
card_original_type('chaoslace'/'LEA', 'Interrupt').
card_original_text('chaoslace'/'LEA', 'Changes the color of one card either being played or already in play to red. Cost to cast, tap, maintain, or use a special ability of target card remains entirely unchanged.').
card_first_print('chaoslace', 'LEA').
card_image_name('chaoslace'/'LEA', 'chaoslace').
card_uid('chaoslace'/'LEA', 'LEA:Chaoslace:chaoslace').
card_rarity('chaoslace'/'LEA', 'Rare').
card_artist('chaoslace'/'LEA', 'Dameon Willich').
card_multiverse_id('chaoslace'/'LEA', '187').

card_in_set('circle of protection: blue', 'LEA').
card_original_type('circle of protection: blue'/'LEA', 'Enchantment').
card_original_text('circle of protection: blue'/'LEA', '{1}: Prevents all damage against you from one blue source. If a source does damage to you more than once in a turn, you must pay 1 mana each time to prevent the damage.').
card_first_print('circle of protection: blue', 'LEA').
card_image_name('circle of protection: blue'/'LEA', 'circle of protection blue').
card_uid('circle of protection: blue'/'LEA', 'LEA:Circle of Protection: Blue:circle of protection blue').
card_rarity('circle of protection: blue'/'LEA', 'Common').
card_artist('circle of protection: blue'/'LEA', 'Dameon Willich').
card_multiverse_id('circle of protection: blue'/'LEA', '241').

card_in_set('circle of protection: green', 'LEA').
card_original_type('circle of protection: green'/'LEA', 'Enchantment').
card_original_text('circle of protection: green'/'LEA', '{1}: Prevents all damage against you from one green source. If a source does damage to you more than once in a turn, you must pay 1 mana each time to prevent the damage.').
card_first_print('circle of protection: green', 'LEA').
card_image_name('circle of protection: green'/'LEA', 'circle of protection green').
card_uid('circle of protection: green'/'LEA', 'LEA:Circle of Protection: Green:circle of protection green').
card_rarity('circle of protection: green'/'LEA', 'Common').
card_artist('circle of protection: green'/'LEA', 'Sandra Everingham').
card_multiverse_id('circle of protection: green'/'LEA', '242').

card_in_set('circle of protection: red', 'LEA').
card_original_type('circle of protection: red'/'LEA', 'Enchantment').
card_original_text('circle of protection: red'/'LEA', '{1}: Prevents all damage against you from one red source. If a source does damage to you more than once in a turn, you must pay 1 mana each time to prevent the damage.').
card_first_print('circle of protection: red', 'LEA').
card_image_name('circle of protection: red'/'LEA', 'circle of protection red').
card_uid('circle of protection: red'/'LEA', 'LEA:Circle of Protection: Red:circle of protection red').
card_rarity('circle of protection: red'/'LEA', 'Common').
card_artist('circle of protection: red'/'LEA', 'Mark Tedin').
card_multiverse_id('circle of protection: red'/'LEA', '243').

card_in_set('circle of protection: white', 'LEA').
card_original_type('circle of protection: white'/'LEA', 'Enchantment').
card_original_text('circle of protection: white'/'LEA', '{1}: Prevents all damage against you from one white source. If a source does damage to you more than once in a turn, you must pay 1 mana each time to prevent the damage.').
card_first_print('circle of protection: white', 'LEA').
card_image_name('circle of protection: white'/'LEA', 'circle of protection white').
card_uid('circle of protection: white'/'LEA', 'LEA:Circle of Protection: White:circle of protection white').
card_rarity('circle of protection: white'/'LEA', 'Common').
card_artist('circle of protection: white'/'LEA', 'Douglas Shuler').
card_multiverse_id('circle of protection: white'/'LEA', '244').

card_in_set('clockwork beast', 'LEA').
card_original_type('clockwork beast'/'LEA', 'Artifact Creature').
card_original_text('clockwork beast'/'LEA', 'Put seven +1/+0 counters on Beast. After Beast attacks or blocks a creature, discard a counter. During the untap phase, controller may buy back lost counters for 1 mana per counter instead of untapping Beast; this taps Beast if it wasn\'t tapped already.').
card_first_print('clockwork beast', 'LEA').
card_image_name('clockwork beast'/'LEA', 'clockwork beast').
card_uid('clockwork beast'/'LEA', 'LEA:Clockwork Beast:clockwork beast').
card_rarity('clockwork beast'/'LEA', 'Rare').
card_artist('clockwork beast'/'LEA', 'Drew Tucker').
card_multiverse_id('clockwork beast'/'LEA', '7').

card_in_set('clone', 'LEA').
card_original_type('clone'/'LEA', 'Summon — Clone').
card_original_text('clone'/'LEA', 'Upon summoning, Clone acquires all normal characteristics, including color, of any one creature in play on either side; any enchantments on original creature are not copied. Clone retains these characteristics even after original creature is destroyed. Clone cannot be played if there are no creatures in play.').
card_first_print('clone', 'LEA').
card_image_name('clone'/'LEA', 'clone').
card_uid('clone'/'LEA', 'LEA:Clone:clone').
card_rarity('clone'/'LEA', 'Uncommon').
card_artist('clone'/'LEA', 'Julie Baroh').
card_multiverse_id('clone'/'LEA', '99').

card_in_set('cockatrice', 'LEA').
card_original_type('cockatrice'/'LEA', 'Summon — Cockatrice').
card_original_text('cockatrice'/'LEA', 'Flying\nAny non-wall creature blocking Cockatrice is destroyed, as is any creature blocked by Cockatrice. Creatures destroyed in this way deal their damage before dying.').
card_first_print('cockatrice', 'LEA').
card_image_name('cockatrice'/'LEA', 'cockatrice').
card_uid('cockatrice'/'LEA', 'LEA:Cockatrice:cockatrice').
card_rarity('cockatrice'/'LEA', 'Rare').
card_artist('cockatrice'/'LEA', 'Dan Frazier').
card_multiverse_id('cockatrice'/'LEA', '145').

card_in_set('consecrate land', 'LEA').
card_original_type('consecrate land'/'LEA', 'Enchant Land').
card_original_text('consecrate land'/'LEA', 'All enchantments on target land are destroyed. Land cannot be destroyed or further enchanted until Consecrate Land has been destroyed.').
card_first_print('consecrate land', 'LEA').
card_image_name('consecrate land'/'LEA', 'consecrate land').
card_uid('consecrate land'/'LEA', 'LEA:Consecrate Land:consecrate land').
card_rarity('consecrate land'/'LEA', 'Uncommon').
card_artist('consecrate land'/'LEA', 'Jeff A. Menges').
card_multiverse_id('consecrate land'/'LEA', '245').

card_in_set('conservator', 'LEA').
card_original_type('conservator'/'LEA', 'Mono Artifact').
card_original_text('conservator'/'LEA', '{3}: Prevent the loss of up to 2 life.').
card_first_print('conservator', 'LEA').
card_image_name('conservator'/'LEA', 'conservator').
card_uid('conservator'/'LEA', 'LEA:Conservator:conservator').
card_rarity('conservator'/'LEA', 'Uncommon').
card_artist('conservator'/'LEA', 'Amy Weber').
card_multiverse_id('conservator'/'LEA', '8').

card_in_set('contract from below', 'LEA').
card_original_type('contract from below'/'LEA', 'Sorcery').
card_original_text('contract from below'/'LEA', 'Discard your current hand and draw eight new cards, adding the first drawn to your ante. Remove this card from your deck before playing if you are not playing for ante.').
card_first_print('contract from below', 'LEA').
card_image_name('contract from below'/'LEA', 'contract from below').
card_uid('contract from below'/'LEA', 'LEA:Contract from Below:contract from below').
card_rarity('contract from below'/'LEA', 'Rare').
card_artist('contract from below'/'LEA', 'Douglas Shuler').
card_multiverse_id('contract from below'/'LEA', '52').

card_in_set('control magic', 'LEA').
card_original_type('control magic'/'LEA', 'Enchant Creature').
card_original_text('control magic'/'LEA', 'You control target creature until enchantment is discarded or game ends. You can\'t tap target creature this turn, but if it was already tapped it stays tapped until you can untap it. If destroyed, target creature is put in its owner\'s graveyard.').
card_first_print('control magic', 'LEA').
card_image_name('control magic'/'LEA', 'control magic').
card_uid('control magic'/'LEA', 'LEA:Control Magic:control magic').
card_rarity('control magic'/'LEA', 'Uncommon').
card_artist('control magic'/'LEA', 'Dameon Willich').
card_multiverse_id('control magic'/'LEA', '100').

card_in_set('conversion', 'LEA').
card_original_type('conversion'/'LEA', 'Enchantment').
card_original_text('conversion'/'LEA', 'All mountains are considered plains while Conversion is in play. Pay {W}{W} during upkeep or Conversion is discarded.').
card_first_print('conversion', 'LEA').
card_image_name('conversion'/'LEA', 'conversion').
card_uid('conversion'/'LEA', 'LEA:Conversion:conversion').
card_rarity('conversion'/'LEA', 'Uncommon').
card_artist('conversion'/'LEA', 'Jesper Myrfors').
card_multiverse_id('conversion'/'LEA', '246').

card_in_set('copper tablet', 'LEA').
card_original_type('copper tablet'/'LEA', 'Continuous Artifact').
card_original_text('copper tablet'/'LEA', 'Copper Tablet does 1 damage to each player during his or her upkeep.').
card_first_print('copper tablet', 'LEA').
card_image_name('copper tablet'/'LEA', 'copper tablet').
card_uid('copper tablet'/'LEA', 'LEA:Copper Tablet:copper tablet').
card_rarity('copper tablet'/'LEA', 'Uncommon').
card_artist('copper tablet'/'LEA', 'Amy Weber').
card_multiverse_id('copper tablet'/'LEA', '9').

card_in_set('copy artifact', 'LEA').
card_original_type('copy artifact'/'LEA', 'Enchantment').
card_original_text('copy artifact'/'LEA', 'Select any artifact in play. This enchantment acts as a duplicate of that artifact; enchantment copy is affected by cards that affect either enchantments or artifacts.\nEnchantment copy remains even if original artifact is destroyed.').
card_first_print('copy artifact', 'LEA').
card_image_name('copy artifact'/'LEA', 'copy artifact').
card_uid('copy artifact'/'LEA', 'LEA:Copy Artifact:copy artifact').
card_rarity('copy artifact'/'LEA', 'Rare').
card_artist('copy artifact'/'LEA', 'Amy Weber').
card_multiverse_id('copy artifact'/'LEA', '101').

card_in_set('counterspell', 'LEA').
card_original_type('counterspell'/'LEA', 'Interrupt').
card_original_text('counterspell'/'LEA', 'Counters target spell as it is being cast.').
card_first_print('counterspell', 'LEA').
card_image_name('counterspell'/'LEA', 'counterspell').
card_uid('counterspell'/'LEA', 'LEA:Counterspell:counterspell').
card_rarity('counterspell'/'LEA', 'Uncommon').
card_artist('counterspell'/'LEA', 'Mark Poole').
card_multiverse_id('counterspell'/'LEA', '102').

card_in_set('craw wurm', 'LEA').
card_original_type('craw wurm'/'LEA', 'Summon — Wurm').
card_original_text('craw wurm'/'LEA', '').
card_first_print('craw wurm', 'LEA').
card_image_name('craw wurm'/'LEA', 'craw wurm').
card_uid('craw wurm'/'LEA', 'LEA:Craw Wurm:craw wurm').
card_rarity('craw wurm'/'LEA', 'Common').
card_artist('craw wurm'/'LEA', 'Daniel Gelon').
card_flavor_text('craw wurm'/'LEA', 'The most terrifying thing about the Craw Wurm is probably the horrible crashing sound it makes as it speeds through the forest. This noise is so loud it echoes through the trees and seems to come from all directions at once.').
card_multiverse_id('craw wurm'/'LEA', '146').

card_in_set('creature bond', 'LEA').
card_original_type('creature bond'/'LEA', 'Enchant Creature').
card_original_text('creature bond'/'LEA', 'If target creature is destroyed, Creature Bond does an amount of damage equal to creature\'s toughness to creature\'s controller.').
card_first_print('creature bond', 'LEA').
card_image_name('creature bond'/'LEA', 'creature bond').
card_uid('creature bond'/'LEA', 'LEA:Creature Bond:creature bond').
card_rarity('creature bond'/'LEA', 'Common').
card_artist('creature bond'/'LEA', 'Anson Maddocks').
card_multiverse_id('creature bond'/'LEA', '103').

card_in_set('crusade', 'LEA').
card_original_type('crusade'/'LEA', 'Enchantment').
card_original_text('crusade'/'LEA', 'All white creatures gain +1/+1.').
card_first_print('crusade', 'LEA').
card_image_name('crusade'/'LEA', 'crusade').
card_uid('crusade'/'LEA', 'LEA:Crusade:crusade').
card_rarity('crusade'/'LEA', 'Rare').
card_artist('crusade'/'LEA', 'Mark Poole').
card_multiverse_id('crusade'/'LEA', '247').

card_in_set('crystal rod', 'LEA').
card_original_type('crystal rod'/'LEA', 'Poly Artifact').
card_original_text('crystal rod'/'LEA', '{1}: Any blue spell cast by any player gives you 1 life.').
card_first_print('crystal rod', 'LEA').
card_image_name('crystal rod'/'LEA', 'crystal rod').
card_uid('crystal rod'/'LEA', 'LEA:Crystal Rod:crystal rod').
card_rarity('crystal rod'/'LEA', 'Uncommon').
card_artist('crystal rod'/'LEA', 'Amy Weber').
card_multiverse_id('crystal rod'/'LEA', '10').

card_in_set('cursed land', 'LEA').
card_original_type('cursed land'/'LEA', 'Enchant Land').
card_original_text('cursed land'/'LEA', 'Cursed Land does 1 damage to target land\'s controller during each upkeep.').
card_first_print('cursed land', 'LEA').
card_image_name('cursed land'/'LEA', 'cursed land').
card_uid('cursed land'/'LEA', 'LEA:Cursed Land:cursed land').
card_rarity('cursed land'/'LEA', 'Uncommon').
card_artist('cursed land'/'LEA', 'Jesper Myrfors').
card_multiverse_id('cursed land'/'LEA', '53').

card_in_set('cyclopean tomb', 'LEA').
card_original_type('cyclopean tomb'/'LEA', 'Mono Artifact').
card_original_text('cyclopean tomb'/'LEA', '{2}: Turn any one non-swamp land into swamp during upkeep. Mark the changed lands with tokens. If Cyclopean Tomb is destroyed, remove one token of your choice each upkeep, returning that land to its original nature.').
card_first_print('cyclopean tomb', 'LEA').
card_image_name('cyclopean tomb'/'LEA', 'cyclopean tomb').
card_uid('cyclopean tomb'/'LEA', 'LEA:Cyclopean Tomb:cyclopean tomb').
card_rarity('cyclopean tomb'/'LEA', 'Rare').
card_artist('cyclopean tomb'/'LEA', 'Anson Maddocks').
card_multiverse_id('cyclopean tomb'/'LEA', '11').

card_in_set('dark ritual', 'LEA').
card_original_type('dark ritual'/'LEA', 'Interrupt').
card_original_text('dark ritual'/'LEA', 'Add 3 black mana to your mana pool.').
card_first_print('dark ritual', 'LEA').
card_image_name('dark ritual'/'LEA', 'dark ritual').
card_uid('dark ritual'/'LEA', 'LEA:Dark Ritual:dark ritual').
card_rarity('dark ritual'/'LEA', 'Common').
card_artist('dark ritual'/'LEA', 'Sandra Everingham').
card_multiverse_id('dark ritual'/'LEA', '54').

card_in_set('darkpact', 'LEA').
card_original_type('darkpact'/'LEA', 'Sorcery').
card_original_text('darkpact'/'LEA', 'Without looking at it first, swap top card of your library with either card of the ante; this swap is permanent. You must have a card in your library to cast this spell. Remove this card from your deck before playing if you are not playing for ante.').
card_first_print('darkpact', 'LEA').
card_image_name('darkpact'/'LEA', 'darkpact').
card_uid('darkpact'/'LEA', 'LEA:Darkpact:darkpact').
card_rarity('darkpact'/'LEA', 'Rare').
card_artist('darkpact'/'LEA', 'Quinton Hoover').
card_multiverse_id('darkpact'/'LEA', '55').

card_in_set('death ward', 'LEA').
card_original_type('death ward'/'LEA', 'Instant').
card_original_text('death ward'/'LEA', 'Regenerates target creature.').
card_first_print('death ward', 'LEA').
card_image_name('death ward'/'LEA', 'death ward').
card_uid('death ward'/'LEA', 'LEA:Death Ward:death ward').
card_rarity('death ward'/'LEA', 'Common').
card_artist('death ward'/'LEA', 'Mark Poole').
card_multiverse_id('death ward'/'LEA', '248').

card_in_set('deathgrip', 'LEA').
card_original_type('deathgrip'/'LEA', 'Enchantment').
card_original_text('deathgrip'/'LEA', '{B}{B} Destroy a green spell as it is being cast. This action may be played as an interrupt, and does not affect green cards already in play.').
card_first_print('deathgrip', 'LEA').
card_image_name('deathgrip'/'LEA', 'deathgrip').
card_uid('deathgrip'/'LEA', 'LEA:Deathgrip:deathgrip').
card_rarity('deathgrip'/'LEA', 'Uncommon').
card_artist('deathgrip'/'LEA', 'Anson Maddocks').
card_multiverse_id('deathgrip'/'LEA', '56').

card_in_set('deathlace', 'LEA').
card_original_type('deathlace'/'LEA', 'Interrupt').
card_original_text('deathlace'/'LEA', 'Changes the color of one card either being played or already in play to black. Cost to cast, tap, maintain, or use a special ability of target card remains entirely unchanged.').
card_first_print('deathlace', 'LEA').
card_image_name('deathlace'/'LEA', 'deathlace').
card_uid('deathlace'/'LEA', 'LEA:Deathlace:deathlace').
card_rarity('deathlace'/'LEA', 'Rare').
card_artist('deathlace'/'LEA', 'Sandra Everingham').
card_multiverse_id('deathlace'/'LEA', '57').

card_in_set('demonic attorney', 'LEA').
card_original_type('demonic attorney'/'LEA', 'Sorcery').
card_original_text('demonic attorney'/'LEA', 'If opponent doesn\'t concede the game immediately, each player must ante an additional card from the top of his or her library. Remove this card from your deck before playing if you are not playing for ante.').
card_first_print('demonic attorney', 'LEA').
card_image_name('demonic attorney'/'LEA', 'demonic attorney').
card_uid('demonic attorney'/'LEA', 'LEA:Demonic Attorney:demonic attorney').
card_rarity('demonic attorney'/'LEA', 'Rare').
card_artist('demonic attorney'/'LEA', 'Daniel Gelon').
card_multiverse_id('demonic attorney'/'LEA', '58').

card_in_set('demonic hordes', 'LEA').
card_original_type('demonic hordes'/'LEA', 'Summon — Demons').
card_original_text('demonic hordes'/'LEA', 'Tap to destroy 1 land. Pay {B}{B}{B} during upkeep or the Hordes become tapped and you lose a land of opponent\'s choice.').
card_first_print('demonic hordes', 'LEA').
card_image_name('demonic hordes'/'LEA', 'demonic hordes').
card_uid('demonic hordes'/'LEA', 'LEA:Demonic Hordes:demonic hordes').
card_rarity('demonic hordes'/'LEA', 'Rare').
card_artist('demonic hordes'/'LEA', 'Jesper Myrfors').
card_flavor_text('demonic hordes'/'LEA', 'Created to destroy Dominia, Demons can sometimes be bent to a more focused purpose.').
card_multiverse_id('demonic hordes'/'LEA', '59').

card_in_set('demonic tutor', 'LEA').
card_original_type('demonic tutor'/'LEA', 'Sorcery').
card_original_text('demonic tutor'/'LEA', 'You may search your library for one card and take it into your hand. Reshuffle your library afterwards.').
card_first_print('demonic tutor', 'LEA').
card_image_name('demonic tutor'/'LEA', 'demonic tutor').
card_uid('demonic tutor'/'LEA', 'LEA:Demonic Tutor:demonic tutor').
card_rarity('demonic tutor'/'LEA', 'Uncommon').
card_artist('demonic tutor'/'LEA', 'Douglas Shuler').
card_multiverse_id('demonic tutor'/'LEA', '60').

card_in_set('dingus egg', 'LEA').
card_original_type('dingus egg'/'LEA', 'Continuous Artifact').
card_original_text('dingus egg'/'LEA', 'Whenever anyone loses a land, Egg does 2 damage to that player for each land lost.').
card_first_print('dingus egg', 'LEA').
card_image_name('dingus egg'/'LEA', 'dingus egg').
card_uid('dingus egg'/'LEA', 'LEA:Dingus Egg:dingus egg').
card_rarity('dingus egg'/'LEA', 'Rare').
card_artist('dingus egg'/'LEA', 'Dan Frazier').
card_multiverse_id('dingus egg'/'LEA', '12').

card_in_set('disenchant', 'LEA').
card_original_type('disenchant'/'LEA', 'Instant').
card_original_text('disenchant'/'LEA', 'Target enchantment or artifact must be discarded.').
card_first_print('disenchant', 'LEA').
card_image_name('disenchant'/'LEA', 'disenchant').
card_uid('disenchant'/'LEA', 'LEA:Disenchant:disenchant').
card_rarity('disenchant'/'LEA', 'Common').
card_artist('disenchant'/'LEA', 'Amy Weber').
card_multiverse_id('disenchant'/'LEA', '249').

card_in_set('disintegrate', 'LEA').
card_original_type('disintegrate'/'LEA', 'Sorcery').
card_original_text('disintegrate'/'LEA', 'Disintegrate does X damage to one target. If target dies this turn, it is removed from game entirely and cannot be regenerated. Return target to its owner\'s deck only when game is over.').
card_first_print('disintegrate', 'LEA').
card_image_name('disintegrate'/'LEA', 'disintegrate').
card_uid('disintegrate'/'LEA', 'LEA:Disintegrate:disintegrate').
card_rarity('disintegrate'/'LEA', 'Common').
card_artist('disintegrate'/'LEA', 'Anson Maddocks').
card_multiverse_id('disintegrate'/'LEA', '188').

card_in_set('disrupting scepter', 'LEA').
card_original_type('disrupting scepter'/'LEA', 'Mono Artifact').
card_original_text('disrupting scepter'/'LEA', '{3}: Opponent must discard one card of his or her choice. Can only be used during your turn.').
card_first_print('disrupting scepter', 'LEA').
card_image_name('disrupting scepter'/'LEA', 'disrupting scepter').
card_uid('disrupting scepter'/'LEA', 'LEA:Disrupting Scepter:disrupting scepter').
card_rarity('disrupting scepter'/'LEA', 'Rare').
card_artist('disrupting scepter'/'LEA', 'Dan Frazier').
card_multiverse_id('disrupting scepter'/'LEA', '13').

card_in_set('dragon whelp', 'LEA').
card_original_type('dragon whelp'/'LEA', 'Summon — Dragon').
card_original_text('dragon whelp'/'LEA', 'Flying\n{R} +1/+0 until end of turn; if more than {R}{R}{R} is spent in this way, Dragon Whelp is destroyed at end of turn.').
card_first_print('dragon whelp', 'LEA').
card_image_name('dragon whelp'/'LEA', 'dragon whelp').
card_uid('dragon whelp'/'LEA', 'LEA:Dragon Whelp:dragon whelp').
card_rarity('dragon whelp'/'LEA', 'Uncommon').
card_artist('dragon whelp'/'LEA', 'Amy Weber').
card_flavor_text('dragon whelp'/'LEA', '\"O to be a dragon . . . of silkworm size or immense . . .\"\n—Marianne Moore, \"O to Be a Dragon\"').
card_multiverse_id('dragon whelp'/'LEA', '189').

card_in_set('drain life', 'LEA').
card_original_type('drain life'/'LEA', 'Sorcery').
card_original_text('drain life'/'LEA', 'Drain Life does 1 damage to a single target for each B spent in addition to the casting cost. Caster gains 1 life for each damage inflicted. If you drain life from a creature, you cannot gain more life than the creature\'s toughness.').
card_first_print('drain life', 'LEA').
card_image_name('drain life'/'LEA', 'drain life').
card_uid('drain life'/'LEA', 'LEA:Drain Life:drain life').
card_rarity('drain life'/'LEA', 'Common').
card_artist('drain life'/'LEA', 'Douglas Shuler').
card_multiverse_id('drain life'/'LEA', '61').

card_in_set('drain power', 'LEA').
card_original_type('drain power'/'LEA', 'Sorcery').
card_original_text('drain power'/'LEA', 'Tap all opponent\'s lands, taking all this mana and all mana in opponent\'s mana pool into your mana pool. You can\'t tap fewer than all opponent\'s lands.').
card_first_print('drain power', 'LEA').
card_image_name('drain power'/'LEA', 'drain power').
card_uid('drain power'/'LEA', 'LEA:Drain Power:drain power').
card_rarity('drain power'/'LEA', 'Rare').
card_artist('drain power'/'LEA', 'Douglas Shuler').
card_multiverse_id('drain power'/'LEA', '104').

card_in_set('drudge skeletons', 'LEA').
card_original_type('drudge skeletons'/'LEA', 'Summon — Skeletons').
card_original_text('drudge skeletons'/'LEA', '{B} Regenerates').
card_first_print('drudge skeletons', 'LEA').
card_image_name('drudge skeletons'/'LEA', 'drudge skeletons').
card_uid('drudge skeletons'/'LEA', 'LEA:Drudge Skeletons:drudge skeletons').
card_rarity('drudge skeletons'/'LEA', 'Common').
card_artist('drudge skeletons'/'LEA', 'Sandra Everingham').
card_flavor_text('drudge skeletons'/'LEA', 'Bones scattered around us joined to form misshapen bodies. We struck at them repeatedly—they fell, but soon formed again, with the same mocking look on their faceless skulls.').
card_multiverse_id('drudge skeletons'/'LEA', '62').

card_in_set('dwarven demolition team', 'LEA').
card_original_type('dwarven demolition team'/'LEA', 'Summon — Dwarves').
card_original_text('dwarven demolition team'/'LEA', 'Tap to destroy a wall.').
card_first_print('dwarven demolition team', 'LEA').
card_image_name('dwarven demolition team'/'LEA', 'dwarven demolition team').
card_uid('dwarven demolition team'/'LEA', 'LEA:Dwarven Demolition Team:dwarven demolition team').
card_rarity('dwarven demolition team'/'LEA', 'Uncommon').
card_artist('dwarven demolition team'/'LEA', 'Kev Brockschmidt').
card_flavor_text('dwarven demolition team'/'LEA', 'Foolishly, Najib retreated to his castle at El-Abar; the next morning he was dead. In just one night, the dwarven forces had reduced the mighty walls to mere rubble.').
card_multiverse_id('dwarven demolition team'/'LEA', '190').

card_in_set('dwarven warriors', 'LEA').
card_original_type('dwarven warriors'/'LEA', 'Summon — Dwarves').
card_original_text('dwarven warriors'/'LEA', 'Tap to make a creature of power no greater than 2 unblockable until end of turn. Other cards may be used to increase target creature\'s power beyond 2 after defense is chosen.').
card_first_print('dwarven warriors', 'LEA').
card_image_name('dwarven warriors'/'LEA', 'dwarven warriors').
card_uid('dwarven warriors'/'LEA', 'LEA:Dwarven Warriors:dwarven warriors').
card_rarity('dwarven warriors'/'LEA', 'Common').
card_artist('dwarven warriors'/'LEA', 'Douglas Shuler').
card_multiverse_id('dwarven warriors'/'LEA', '191').

card_in_set('earth elemental', 'LEA').
card_original_type('earth elemental'/'LEA', 'Summon — Elemental').
card_original_text('earth elemental'/'LEA', '').
card_first_print('earth elemental', 'LEA').
card_image_name('earth elemental'/'LEA', 'earth elemental').
card_uid('earth elemental'/'LEA', 'LEA:Earth Elemental:earth elemental').
card_rarity('earth elemental'/'LEA', 'Uncommon').
card_artist('earth elemental'/'LEA', 'Dan Frazier').
card_flavor_text('earth elemental'/'LEA', 'Earth Elementals have the eternal strength of stone and the endurance of mountains. Primordially connected to the land they inhabit, they take a long-term view of things, scorning the impetuous haste of short-lived mortal creatures.').
card_multiverse_id('earth elemental'/'LEA', '192').

card_in_set('earthbind', 'LEA').
card_original_type('earthbind'/'LEA', 'Enchant Flying Creature').
card_original_text('earthbind'/'LEA', 'Earthbind does 2 damage to target flying creature, which also loses flying ability.').
card_first_print('earthbind', 'LEA').
card_image_name('earthbind'/'LEA', 'earthbind').
card_uid('earthbind'/'LEA', 'LEA:Earthbind:earthbind').
card_rarity('earthbind'/'LEA', 'Common').
card_artist('earthbind'/'LEA', 'Quinton Hoover').
card_multiverse_id('earthbind'/'LEA', '193').

card_in_set('earthquake', 'LEA').
card_original_type('earthquake'/'LEA', 'Sorcery').
card_original_text('earthquake'/'LEA', 'Does X damage to each player and each non-flying creature in play.').
card_first_print('earthquake', 'LEA').
card_image_name('earthquake'/'LEA', 'earthquake').
card_uid('earthquake'/'LEA', 'LEA:Earthquake:earthquake').
card_rarity('earthquake'/'LEA', 'Rare').
card_artist('earthquake'/'LEA', 'Dan Frazier').
card_multiverse_id('earthquake'/'LEA', '194').

card_in_set('elvish archers', 'LEA').
card_original_type('elvish archers'/'LEA', 'Summon — Elves').
card_original_text('elvish archers'/'LEA', 'First strike').
card_first_print('elvish archers', 'LEA').
card_image_name('elvish archers'/'LEA', 'elvish archers').
card_uid('elvish archers'/'LEA', 'LEA:Elvish Archers:elvish archers').
card_rarity('elvish archers'/'LEA', 'Rare').
card_artist('elvish archers'/'LEA', 'Anson Maddocks').
card_flavor_text('elvish archers'/'LEA', 'I tell you, there was so many arrows flying about you couldn\'t hardly see the sun. So I says to young Angus, \"Well, at least now we\'re fighting in the shade!\"').
card_multiverse_id('elvish archers'/'LEA', '147').

card_in_set('evil presence', 'LEA').
card_original_type('evil presence'/'LEA', 'Enchant Land').
card_original_text('evil presence'/'LEA', 'Target land is now a swamp.').
card_first_print('evil presence', 'LEA').
card_image_name('evil presence'/'LEA', 'evil presence').
card_uid('evil presence'/'LEA', 'LEA:Evil Presence:evil presence').
card_rarity('evil presence'/'LEA', 'Uncommon').
card_artist('evil presence'/'LEA', 'Sandra Everingham').
card_multiverse_id('evil presence'/'LEA', '63').

card_in_set('false orders', 'LEA').
card_original_type('false orders'/'LEA', 'Instant').
card_original_text('false orders'/'LEA', 'You decide whether and how one defending creature blocks, though you can\'t make a choice the defender couldn\'t legally make. Play after defender has chosen defense but before damage is dealt.').
card_first_print('false orders', 'LEA').
card_image_name('false orders'/'LEA', 'false orders').
card_uid('false orders'/'LEA', 'LEA:False Orders:false orders').
card_rarity('false orders'/'LEA', 'Common').
card_artist('false orders'/'LEA', 'Anson Maddocks').
card_multiverse_id('false orders'/'LEA', '195').

card_in_set('farmstead', 'LEA').
card_original_type('farmstead'/'LEA', 'Enchant Land').
card_original_text('farmstead'/'LEA', 'Target land\'s controller gains 1 life each upkeep if {W}{W} is spent. Target land still generates mana as usual.').
card_first_print('farmstead', 'LEA').
card_image_name('farmstead'/'LEA', 'farmstead').
card_uid('farmstead'/'LEA', 'LEA:Farmstead:farmstead').
card_rarity('farmstead'/'LEA', 'Rare').
card_artist('farmstead'/'LEA', 'Mark Poole').
card_multiverse_id('farmstead'/'LEA', '250').

card_in_set('fastbond', 'LEA').
card_original_type('fastbond'/'LEA', 'Enchantment').
card_original_text('fastbond'/'LEA', 'You may put as many lands into play as you want each turn. Fastbond does 1 damage to you for every land beyond the first that you play in a single turn.').
card_first_print('fastbond', 'LEA').
card_image_name('fastbond'/'LEA', 'fastbond').
card_uid('fastbond'/'LEA', 'LEA:Fastbond:fastbond').
card_rarity('fastbond'/'LEA', 'Rare').
card_artist('fastbond'/'LEA', 'Mark Poole').
card_multiverse_id('fastbond'/'LEA', '148').

card_in_set('fear', 'LEA').
card_original_type('fear'/'LEA', 'Enchant Creature').
card_original_text('fear'/'LEA', 'Target creature cannot be blocked by any creatures other than artifact creatures and black creatures.').
card_first_print('fear', 'LEA').
card_image_name('fear'/'LEA', 'fear').
card_uid('fear'/'LEA', 'LEA:Fear:fear').
card_rarity('fear'/'LEA', 'Common').
card_artist('fear'/'LEA', 'Mark Poole').
card_multiverse_id('fear'/'LEA', '64').

card_in_set('feedback', 'LEA').
card_original_type('feedback'/'LEA', 'Enchant Enchantment').
card_original_text('feedback'/'LEA', 'Feedback does 1 damage to controller of target enchantment during each upkeep.').
card_first_print('feedback', 'LEA').
card_image_name('feedback'/'LEA', 'feedback').
card_uid('feedback'/'LEA', 'LEA:Feedback:feedback').
card_rarity('feedback'/'LEA', 'Uncommon').
card_artist('feedback'/'LEA', 'Quinton Hoover').
card_multiverse_id('feedback'/'LEA', '105').

card_in_set('fire elemental', 'LEA').
card_original_type('fire elemental'/'LEA', 'Summon — Elemental').
card_original_text('fire elemental'/'LEA', '').
card_first_print('fire elemental', 'LEA').
card_image_name('fire elemental'/'LEA', 'fire elemental').
card_uid('fire elemental'/'LEA', 'LEA:Fire Elemental:fire elemental').
card_rarity('fire elemental'/'LEA', 'Uncommon').
card_artist('fire elemental'/'LEA', 'Melissa A. Benson').
card_flavor_text('fire elemental'/'LEA', 'Fire Elementals are ruthless infernos, annihilating and consuming their foes in a frenzied holocaust. Crackling and blazing, they sear swift, terrible paths, leaving the land charred and scorched in their wake.').
card_multiverse_id('fire elemental'/'LEA', '196').

card_in_set('fireball', 'LEA').
card_original_type('fireball'/'LEA', 'Sorcery').
card_original_text('fireball'/'LEA', 'Fireball does X damage total, divided evenly (round down) among any number of targets. Pay 1 extra mana for each target beyond the first.').
card_first_print('fireball', 'LEA').
card_image_name('fireball'/'LEA', 'fireball').
card_uid('fireball'/'LEA', 'LEA:Fireball:fireball').
card_rarity('fireball'/'LEA', 'Common').
card_artist('fireball'/'LEA', 'Mark Tedin').
card_multiverse_id('fireball'/'LEA', '197').

card_in_set('firebreathing', 'LEA').
card_original_type('firebreathing'/'LEA', 'Enchant Creature').
card_original_text('firebreathing'/'LEA', '{R} +1/+0').
card_first_print('firebreathing', 'LEA').
card_image_name('firebreathing'/'LEA', 'firebreathing').
card_uid('firebreathing'/'LEA', 'LEA:Firebreathing:firebreathing').
card_rarity('firebreathing'/'LEA', 'Common').
card_artist('firebreathing'/'LEA', 'Dan Frazier').
card_flavor_text('firebreathing'/'LEA', '\"And topples round the dreary west\nA looming bastion fringed with fire.\"\n—Alfred, Lord Tennyson, \"In Memoriam\"').
card_multiverse_id('firebreathing'/'LEA', '198').

card_in_set('flashfires', 'LEA').
card_original_type('flashfires'/'LEA', 'Sorcery').
card_original_text('flashfires'/'LEA', 'All plains in play are destroyed.').
card_first_print('flashfires', 'LEA').
card_image_name('flashfires'/'LEA', 'flashfires').
card_uid('flashfires'/'LEA', 'LEA:Flashfires:flashfires').
card_rarity('flashfires'/'LEA', 'Uncommon').
card_artist('flashfires'/'LEA', 'Dameon Willich').
card_multiverse_id('flashfires'/'LEA', '199').

card_in_set('flight', 'LEA').
card_original_type('flight'/'LEA', 'Enchant Creature').
card_original_text('flight'/'LEA', 'Target creature is now a flying creature.').
card_first_print('flight', 'LEA').
card_image_name('flight'/'LEA', 'flight').
card_uid('flight'/'LEA', 'LEA:Flight:flight').
card_rarity('flight'/'LEA', 'Common').
card_artist('flight'/'LEA', 'Anson Maddocks').
card_multiverse_id('flight'/'LEA', '106').

card_in_set('fog', 'LEA').
card_original_type('fog'/'LEA', 'Instant').
card_original_text('fog'/'LEA', 'Creatures attack and block as normal, but none deal any damage. All attacking creatures are still tapped. Play any time before attack damage is dealt.').
card_first_print('fog', 'LEA').
card_image_name('fog'/'LEA', 'fog').
card_uid('fog'/'LEA', 'LEA:Fog:fog').
card_rarity('fog'/'LEA', 'Common').
card_artist('fog'/'LEA', 'Jesper Myrfors').
card_multiverse_id('fog'/'LEA', '149').

card_in_set('force of nature', 'LEA').
card_original_type('force of nature'/'LEA', 'Summon — Force').
card_original_text('force of nature'/'LEA', 'Trample\nYou must pay {G}{G}{G}{G} during upkeep or Force of Nature does 8 damage to you. You may still attack with Force of Nature even if you failed to pay the upkeep.').
card_first_print('force of nature', 'LEA').
card_image_name('force of nature'/'LEA', 'force of nature').
card_uid('force of nature'/'LEA', 'LEA:Force of Nature:force of nature').
card_rarity('force of nature'/'LEA', 'Rare').
card_artist('force of nature'/'LEA', 'Douglas Shuler').
card_multiverse_id('force of nature'/'LEA', '150').

card_in_set('forcefield', 'LEA').
card_original_type('forcefield'/'LEA', 'Poly Artifact').
card_original_text('forcefield'/'LEA', '{1}: Lose only 1 life to an unblocked creature.').
card_first_print('forcefield', 'LEA').
card_image_name('forcefield'/'LEA', 'forcefield').
card_uid('forcefield'/'LEA', 'LEA:Forcefield:forcefield').
card_rarity('forcefield'/'LEA', 'Rare').
card_artist('forcefield'/'LEA', 'Dan Frazier').
card_multiverse_id('forcefield'/'LEA', '14').

card_in_set('forest', 'LEA').
card_original_type('forest'/'LEA', 'Land').
card_original_text('forest'/'LEA', 'Tap to add {G} to your mana pool.').
card_first_print('forest', 'LEA').
card_image_name('forest'/'LEA', 'forest1').
card_uid('forest'/'LEA', 'LEA:Forest:forest1').
card_rarity('forest'/'LEA', 'Basic Land').
card_artist('forest'/'LEA', 'Christopher Rush').
card_multiverse_id('forest'/'LEA', '289').

card_in_set('forest', 'LEA').
card_original_type('forest'/'LEA', 'Land').
card_original_text('forest'/'LEA', 'Tap to add {G} to your mana pool.').
card_image_name('forest'/'LEA', 'forest2').
card_uid('forest'/'LEA', 'LEA:Forest:forest2').
card_rarity('forest'/'LEA', 'Basic Land').
card_artist('forest'/'LEA', 'Christopher Rush').
card_multiverse_id('forest'/'LEA', '288').

card_in_set('fork', 'LEA').
card_original_type('fork'/'LEA', 'Interrupt').
card_original_text('fork'/'LEA', 'Any sorcery or instant spell just cast is doubled. Treat Fork as an exact copy of target spell except that Fork remains red. Copy and original may have different targets.').
card_first_print('fork', 'LEA').
card_image_name('fork'/'LEA', 'fork').
card_uid('fork'/'LEA', 'LEA:Fork:fork').
card_rarity('fork'/'LEA', 'Rare').
card_artist('fork'/'LEA', 'Amy Weber').
card_multiverse_id('fork'/'LEA', '200').

card_in_set('frozen shade', 'LEA').
card_original_type('frozen shade'/'LEA', 'Summon — Shade').
card_original_text('frozen shade'/'LEA', '{B} +1/+1').
card_first_print('frozen shade', 'LEA').
card_image_name('frozen shade'/'LEA', 'frozen shade').
card_uid('frozen shade'/'LEA', 'LEA:Frozen Shade:frozen shade').
card_rarity('frozen shade'/'LEA', 'Common').
card_artist('frozen shade'/'LEA', 'Douglas Shuler').
card_flavor_text('frozen shade'/'LEA', '\"There are some qualities—some incorporate things,/ That have a double life, which thus is made/ A type of twin entity which springs/ From matter and light, evinced in solid and shade.\"\n—Edgar Allan Poe, \"Silence\"').
card_multiverse_id('frozen shade'/'LEA', '65').

card_in_set('fungusaur', 'LEA').
card_original_type('fungusaur'/'LEA', 'Summon — Fungusaur').
card_original_text('fungusaur'/'LEA', 'Each time Fungusaur is damaged but not destroyed, put a +1/+1 counter on it.').
card_first_print('fungusaur', 'LEA').
card_image_name('fungusaur'/'LEA', 'fungusaur').
card_uid('fungusaur'/'LEA', 'LEA:Fungusaur:fungusaur').
card_rarity('fungusaur'/'LEA', 'Rare').
card_artist('fungusaur'/'LEA', 'Daniel Gelon').
card_flavor_text('fungusaur'/'LEA', 'Rather than sheltering her young, the female Fungusaur often injures her own offspring, thereby ensuring their rapid growth.').
card_multiverse_id('fungusaur'/'LEA', '151').

card_in_set('gaea\'s liege', 'LEA').
card_original_type('gaea\'s liege'/'LEA', 'Summon — Gaea\'s Liege').
card_original_text('gaea\'s liege'/'LEA', 'When defending, Gaea\'s Liege has power and toughness equal to the number of forests you have in play; when it\'s attacking, they are equal to the number of forests opponent has in play. Tap to turn any one land into a forest until Gaea\'s Liege leaves play. Mark changed lands with counters, removing the counters when Gaea\'s Liege leaves play.').
card_first_print('gaea\'s liege', 'LEA').
card_image_name('gaea\'s liege'/'LEA', 'gaea\'s liege').
card_uid('gaea\'s liege'/'LEA', 'LEA:Gaea\'s Liege:gaea\'s liege').
card_rarity('gaea\'s liege'/'LEA', 'Rare').
card_artist('gaea\'s liege'/'LEA', 'Dameon Willich').
card_multiverse_id('gaea\'s liege'/'LEA', '152').

card_in_set('gauntlet of might', 'LEA').
card_original_type('gauntlet of might'/'LEA', 'Continuous Artifact').
card_original_text('gauntlet of might'/'LEA', 'All red creatures gain +1/+1 and all mountains provide an extra red mana when tapped.').
card_first_print('gauntlet of might', 'LEA').
card_image_name('gauntlet of might'/'LEA', 'gauntlet of might').
card_uid('gauntlet of might'/'LEA', 'LEA:Gauntlet of Might:gauntlet of might').
card_rarity('gauntlet of might'/'LEA', 'Rare').
card_artist('gauntlet of might'/'LEA', 'Christopher Rush').
card_multiverse_id('gauntlet of might'/'LEA', '15').

card_in_set('giant growth', 'LEA').
card_original_type('giant growth'/'LEA', 'Instant').
card_original_text('giant growth'/'LEA', 'Target creature gains +3/+3 until end of turn.').
card_first_print('giant growth', 'LEA').
card_image_name('giant growth'/'LEA', 'giant growth').
card_uid('giant growth'/'LEA', 'LEA:Giant Growth:giant growth').
card_rarity('giant growth'/'LEA', 'Common').
card_artist('giant growth'/'LEA', 'Sandra Everingham').
card_multiverse_id('giant growth'/'LEA', '153').

card_in_set('giant spider', 'LEA').
card_original_type('giant spider'/'LEA', 'Summon — Spider').
card_original_text('giant spider'/'LEA', 'Does not fly, but can block flying creatures.').
card_first_print('giant spider', 'LEA').
card_image_name('giant spider'/'LEA', 'giant spider').
card_uid('giant spider'/'LEA', 'LEA:Giant Spider:giant spider').
card_rarity('giant spider'/'LEA', 'Common').
card_artist('giant spider'/'LEA', 'Sandra Everingham').
card_flavor_text('giant spider'/'LEA', 'While it possesses potent venom, the Giant Spider often chooses not to paralyze its victims. Perhaps the creature enjoys the gentle rocking motion caused by its captives\' struggles to escape its web.').
card_multiverse_id('giant spider'/'LEA', '154').

card_in_set('glasses of urza', 'LEA').
card_original_type('glasses of urza'/'LEA', 'Mono Artifact').
card_original_text('glasses of urza'/'LEA', 'You may look at opponent\'s hand.').
card_first_print('glasses of urza', 'LEA').
card_image_name('glasses of urza'/'LEA', 'glasses of urza').
card_uid('glasses of urza'/'LEA', 'LEA:Glasses of Urza:glasses of urza').
card_rarity('glasses of urza'/'LEA', 'Uncommon').
card_artist('glasses of urza'/'LEA', 'Douglas Shuler').
card_multiverse_id('glasses of urza'/'LEA', '16').

card_in_set('gloom', 'LEA').
card_original_type('gloom'/'LEA', 'Enchantment').
card_original_text('gloom'/'LEA', 'White spells cost 3 more mana to cast. Circles of Protection cost 3 more mana to use.').
card_first_print('gloom', 'LEA').
card_image_name('gloom'/'LEA', 'gloom').
card_uid('gloom'/'LEA', 'LEA:Gloom:gloom').
card_rarity('gloom'/'LEA', 'Uncommon').
card_artist('gloom'/'LEA', 'Dan Frazier').
card_multiverse_id('gloom'/'LEA', '66').

card_in_set('goblin balloon brigade', 'LEA').
card_original_type('goblin balloon brigade'/'LEA', 'Summon — Goblins').
card_original_text('goblin balloon brigade'/'LEA', '{R} Goblins gain flying ability until end of turn. Controller may not choose to make Goblins fly after they have been blocked.').
card_first_print('goblin balloon brigade', 'LEA').
card_image_name('goblin balloon brigade'/'LEA', 'goblin balloon brigade').
card_uid('goblin balloon brigade'/'LEA', 'LEA:Goblin Balloon Brigade:goblin balloon brigade').
card_rarity('goblin balloon brigade'/'LEA', 'Uncommon').
card_artist('goblin balloon brigade'/'LEA', 'Andi Rusu').
card_flavor_text('goblin balloon brigade'/'LEA', '\"From up here we can drop rocks and arrows and more rocks!\" \"Uh, yeah boss, but how do we get down?\"').
card_multiverse_id('goblin balloon brigade'/'LEA', '201').

card_in_set('goblin king', 'LEA').
card_original_type('goblin king'/'LEA', 'Summon — Goblin King').
card_original_text('goblin king'/'LEA', 'Goblins in play gain mountainwalk and +1/+1 while this card remains in play.').
card_first_print('goblin king', 'LEA').
card_image_name('goblin king'/'LEA', 'goblin king').
card_uid('goblin king'/'LEA', 'LEA:Goblin King:goblin king').
card_rarity('goblin king'/'LEA', 'Rare').
card_artist('goblin king'/'LEA', 'Jesper Myrfors').
card_flavor_text('goblin king'/'LEA', 'To become king of the Goblins, one must assassinate the previous king. Thus, only the most foolish seek positions of leadership.').
card_multiverse_id('goblin king'/'LEA', '202').

card_in_set('granite gargoyle', 'LEA').
card_original_type('granite gargoyle'/'LEA', 'Summon — Gargoyle').
card_original_text('granite gargoyle'/'LEA', 'Flying; {R} +0/+1 until end of turn.').
card_first_print('granite gargoyle', 'LEA').
card_image_name('granite gargoyle'/'LEA', 'granite gargoyle').
card_uid('granite gargoyle'/'LEA', 'LEA:Granite Gargoyle:granite gargoyle').
card_rarity('granite gargoyle'/'LEA', 'Rare').
card_artist('granite gargoyle'/'LEA', 'Christopher Rush').
card_flavor_text('granite gargoyle'/'LEA', '\"While most overworlders fortunately don\'t realize this, Gargoyles can be most delicious, providing you have the appropriate tools to carve them.\"\n—The Underworld Cookbook, by Asmoranomardicadaistinaculdacar').
card_multiverse_id('granite gargoyle'/'LEA', '203').

card_in_set('gray ogre', 'LEA').
card_original_type('gray ogre'/'LEA', 'Summon — Ogre').
card_original_text('gray ogre'/'LEA', '').
card_first_print('gray ogre', 'LEA').
card_image_name('gray ogre'/'LEA', 'gray ogre').
card_uid('gray ogre'/'LEA', 'LEA:Gray Ogre:gray ogre').
card_rarity('gray ogre'/'LEA', 'Common').
card_artist('gray ogre'/'LEA', 'Dan Frazier').
card_flavor_text('gray ogre'/'LEA', 'The Ogre philosopher Gnerdel believed the purpose of life was to live as high on the food chain as possible. She refused to eat vegetarians, and preferred to live entirely on creatures that preyed on sentient beings.').
card_multiverse_id('gray ogre'/'LEA', '204').

card_in_set('green ward', 'LEA').
card_original_type('green ward'/'LEA', 'Enchant Creature').
card_original_text('green ward'/'LEA', 'Target creature gains protection from green.').
card_first_print('green ward', 'LEA').
card_image_name('green ward'/'LEA', 'green ward').
card_uid('green ward'/'LEA', 'LEA:Green Ward:green ward').
card_rarity('green ward'/'LEA', 'Uncommon').
card_artist('green ward'/'LEA', 'Dan Frazier').
card_multiverse_id('green ward'/'LEA', '251').

card_in_set('grizzly bears', 'LEA').
card_original_type('grizzly bears'/'LEA', 'Summon — Bears').
card_original_text('grizzly bears'/'LEA', '').
card_first_print('grizzly bears', 'LEA').
card_image_name('grizzly bears'/'LEA', 'grizzly bears').
card_uid('grizzly bears'/'LEA', 'LEA:Grizzly Bears:grizzly bears').
card_rarity('grizzly bears'/'LEA', 'Common').
card_artist('grizzly bears'/'LEA', 'Jeff A. Menges').
card_flavor_text('grizzly bears'/'LEA', 'Don\'t try to outrun one of Dominia\'s Grizzlies; it\'ll catch you, knock you down, and eat you. Of course, you could run up a tree. In that case you\'ll get a nice view before it knocks the tree down and eats you.').
card_multiverse_id('grizzly bears'/'LEA', '155').

card_in_set('guardian angel', 'LEA').
card_original_type('guardian angel'/'LEA', 'Instant').
card_original_text('guardian angel'/'LEA', 'Prevents X damage from being dealt to any one target. Any further damage to the same target this turn can be canceled by spending 1 mana per point of damage to be canceled.').
card_first_print('guardian angel', 'LEA').
card_image_name('guardian angel'/'LEA', 'guardian angel').
card_uid('guardian angel'/'LEA', 'LEA:Guardian Angel:guardian angel').
card_rarity('guardian angel'/'LEA', 'Common').
card_artist('guardian angel'/'LEA', 'Anson Maddocks').
card_multiverse_id('guardian angel'/'LEA', '252').

card_in_set('healing salve', 'LEA').
card_original_type('healing salve'/'LEA', 'Instant').
card_original_text('healing salve'/'LEA', 'Gain 3 life, or prevent up to 3 damage from being dealt to a single target.').
card_first_print('healing salve', 'LEA').
card_image_name('healing salve'/'LEA', 'healing salve').
card_uid('healing salve'/'LEA', 'LEA:Healing Salve:healing salve').
card_rarity('healing salve'/'LEA', 'Common').
card_artist('healing salve'/'LEA', 'Dan Frazier').
card_multiverse_id('healing salve'/'LEA', '253').

card_in_set('helm of chatzuk', 'LEA').
card_original_type('helm of chatzuk'/'LEA', 'Mono Artifact').
card_original_text('helm of chatzuk'/'LEA', '{1}: You may give one creature the ability to band until end of turn.').
card_first_print('helm of chatzuk', 'LEA').
card_image_name('helm of chatzuk'/'LEA', 'helm of chatzuk').
card_uid('helm of chatzuk'/'LEA', 'LEA:Helm of Chatzuk:helm of chatzuk').
card_rarity('helm of chatzuk'/'LEA', 'Rare').
card_artist('helm of chatzuk'/'LEA', 'Mark Tedin').
card_multiverse_id('helm of chatzuk'/'LEA', '17').

card_in_set('hill giant', 'LEA').
card_original_type('hill giant'/'LEA', 'Summon — Giant').
card_original_text('hill giant'/'LEA', '').
card_first_print('hill giant', 'LEA').
card_image_name('hill giant'/'LEA', 'hill giant').
card_uid('hill giant'/'LEA', 'LEA:Hill Giant:hill giant').
card_rarity('hill giant'/'LEA', 'Common').
card_artist('hill giant'/'LEA', 'Dan Frazier').
card_flavor_text('hill giant'/'LEA', 'Fortunately, Hill Giants have large blind spots in which a human can easily hide. Unfortunately, these blind spots are beneath the bottoms of their feet.').
card_multiverse_id('hill giant'/'LEA', '205').

card_in_set('holy armor', 'LEA').
card_original_type('holy armor'/'LEA', 'Enchant Creature').
card_original_text('holy armor'/'LEA', 'Target creature gains +0/+2. {W} Target creature gets extra +0/+1 until end of turn.').
card_first_print('holy armor', 'LEA').
card_image_name('holy armor'/'LEA', 'holy armor').
card_uid('holy armor'/'LEA', 'LEA:Holy Armor:holy armor').
card_rarity('holy armor'/'LEA', 'Common').
card_artist('holy armor'/'LEA', 'Melissa A. Benson').
card_multiverse_id('holy armor'/'LEA', '254').

card_in_set('holy strength', 'LEA').
card_original_type('holy strength'/'LEA', 'Enchant Creature').
card_original_text('holy strength'/'LEA', 'Target creature gains +1/+2.').
card_first_print('holy strength', 'LEA').
card_image_name('holy strength'/'LEA', 'holy strength').
card_uid('holy strength'/'LEA', 'LEA:Holy Strength:holy strength').
card_rarity('holy strength'/'LEA', 'Common').
card_artist('holy strength'/'LEA', 'Anson Maddocks').
card_multiverse_id('holy strength'/'LEA', '255').

card_in_set('howl from beyond', 'LEA').
card_original_type('howl from beyond'/'LEA', 'Instant').
card_original_text('howl from beyond'/'LEA', 'Target creature gains +X/+0 until end of turn.').
card_first_print('howl from beyond', 'LEA').
card_image_name('howl from beyond'/'LEA', 'howl from beyond').
card_uid('howl from beyond'/'LEA', 'LEA:Howl from Beyond:howl from beyond').
card_rarity('howl from beyond'/'LEA', 'Common').
card_artist('howl from beyond'/'LEA', 'Mark Poole').
card_multiverse_id('howl from beyond'/'LEA', '67').

card_in_set('howling mine', 'LEA').
card_original_type('howling mine'/'LEA', 'Continuous Artifact').
card_original_text('howling mine'/'LEA', 'Each player draws one extra card each turn during his or her draw phase.').
card_first_print('howling mine', 'LEA').
card_image_name('howling mine'/'LEA', 'howling mine').
card_uid('howling mine'/'LEA', 'LEA:Howling Mine:howling mine').
card_rarity('howling mine'/'LEA', 'Rare').
card_artist('howling mine'/'LEA', 'Mark Poole').
card_multiverse_id('howling mine'/'LEA', '18').

card_in_set('hurloon minotaur', 'LEA').
card_original_type('hurloon minotaur'/'LEA', 'Summon — Minotaur').
card_original_text('hurloon minotaur'/'LEA', '').
card_first_print('hurloon minotaur', 'LEA').
card_image_name('hurloon minotaur'/'LEA', 'hurloon minotaur').
card_uid('hurloon minotaur'/'LEA', 'LEA:Hurloon Minotaur:hurloon minotaur').
card_rarity('hurloon minotaur'/'LEA', 'Common').
card_artist('hurloon minotaur'/'LEA', 'Anson Maddocks').
card_flavor_text('hurloon minotaur'/'LEA', 'The Minotaurs of the Hurloon Mountains are known for their love of battle. They are also known for their hymns to the dead, sung for friend and foe alike. These hymns can last for days, filling the mountain valleys with their low, haunting sounds.').
card_multiverse_id('hurloon minotaur'/'LEA', '206').

card_in_set('hurricane', 'LEA').
card_original_type('hurricane'/'LEA', 'Sorcery').
card_original_text('hurricane'/'LEA', 'All players and flying creatures suffer X damage.').
card_first_print('hurricane', 'LEA').
card_image_name('hurricane'/'LEA', 'hurricane').
card_uid('hurricane'/'LEA', 'LEA:Hurricane:hurricane').
card_rarity('hurricane'/'LEA', 'Uncommon').
card_artist('hurricane'/'LEA', 'Dameon Willich').
card_multiverse_id('hurricane'/'LEA', '156').

card_in_set('hypnotic specter', 'LEA').
card_original_type('hypnotic specter'/'LEA', 'Summon — Specter').
card_original_text('hypnotic specter'/'LEA', 'Flying\nAn opponent damaged by Specter must discard a card at random from his or her hand. Ignore this effect if opponent has no cards left in hand.').
card_first_print('hypnotic specter', 'LEA').
card_image_name('hypnotic specter'/'LEA', 'hypnotic specter').
card_uid('hypnotic specter'/'LEA', 'LEA:Hypnotic Specter:hypnotic specter').
card_rarity('hypnotic specter'/'LEA', 'Uncommon').
card_artist('hypnotic specter'/'LEA', 'Douglas Shuler').
card_flavor_text('hypnotic specter'/'LEA', '\"...There was no trace/ Of aught on that illumined face...\"\n—Samuel Coleridge, \"Phantom\"').
card_multiverse_id('hypnotic specter'/'LEA', '68').

card_in_set('ice storm', 'LEA').
card_original_type('ice storm'/'LEA', 'Sorcery').
card_original_text('ice storm'/'LEA', 'Destroys any one land.').
card_first_print('ice storm', 'LEA').
card_image_name('ice storm'/'LEA', 'ice storm').
card_uid('ice storm'/'LEA', 'LEA:Ice Storm:ice storm').
card_rarity('ice storm'/'LEA', 'Uncommon').
card_artist('ice storm'/'LEA', 'Dan Frazier').
card_multiverse_id('ice storm'/'LEA', '157').

card_in_set('icy manipulator', 'LEA').
card_original_type('icy manipulator'/'LEA', 'Mono Artifact').
card_original_text('icy manipulator'/'LEA', '{1}: You may tap any land, creature, or artifact in play on either side.').
card_first_print('icy manipulator', 'LEA').
card_image_name('icy manipulator'/'LEA', 'icy manipulator').
card_uid('icy manipulator'/'LEA', 'LEA:Icy Manipulator:icy manipulator').
card_rarity('icy manipulator'/'LEA', 'Uncommon').
card_artist('icy manipulator'/'LEA', 'Douglas Shuler').
card_multiverse_id('icy manipulator'/'LEA', '19').

card_in_set('illusionary mask', 'LEA').
card_original_type('illusionary mask'/'LEA', 'Poly Artifact').
card_original_text('illusionary mask'/'LEA', '{X} You can summon a creature face down so opponent doesn\'t know what it is. The X cost can be any amount of mana, even 0; it serves to hide the true casting cost of the creature, which you still have to spend. As soon as a face-down creature receives damage, deals damage, or is tapped, you must turn it face up.').
card_first_print('illusionary mask', 'LEA').
card_image_name('illusionary mask'/'LEA', 'illusionary mask').
card_uid('illusionary mask'/'LEA', 'LEA:Illusionary Mask:illusionary mask').
card_rarity('illusionary mask'/'LEA', 'Rare').
card_artist('illusionary mask'/'LEA', 'Amy Weber').
card_multiverse_id('illusionary mask'/'LEA', '20').

card_in_set('instill energy', 'LEA').
card_original_type('instill energy'/'LEA', 'Enchant Creature').
card_original_text('instill energy'/'LEA', 'You may untap target creature both during your untap phase and one additional time during your turn. Target creature may attack the turn it comes into play.').
card_first_print('instill energy', 'LEA').
card_image_name('instill energy'/'LEA', 'instill energy').
card_uid('instill energy'/'LEA', 'LEA:Instill Energy:instill energy').
card_rarity('instill energy'/'LEA', 'Uncommon').
card_artist('instill energy'/'LEA', 'Dameon Willich').
card_multiverse_id('instill energy'/'LEA', '158').

card_in_set('invisibility', 'LEA').
card_original_type('invisibility'/'LEA', 'Enchant Creature').
card_original_text('invisibility'/'LEA', 'Target creature can be blocked only by walls.').
card_first_print('invisibility', 'LEA').
card_image_name('invisibility'/'LEA', 'invisibility').
card_uid('invisibility'/'LEA', 'LEA:Invisibility:invisibility').
card_rarity('invisibility'/'LEA', 'Common').
card_artist('invisibility'/'LEA', 'Anson Maddocks').
card_multiverse_id('invisibility'/'LEA', '107').

card_in_set('iron star', 'LEA').
card_original_type('iron star'/'LEA', 'Poly Artifact').
card_original_text('iron star'/'LEA', '{1}: Any red spell cast by any player gives you 1 life.').
card_first_print('iron star', 'LEA').
card_image_name('iron star'/'LEA', 'iron star').
card_uid('iron star'/'LEA', 'LEA:Iron Star:iron star').
card_rarity('iron star'/'LEA', 'Uncommon').
card_artist('iron star'/'LEA', 'Dan Frazier').
card_multiverse_id('iron star'/'LEA', '21').

card_in_set('ironclaw orcs', 'LEA').
card_original_type('ironclaw orcs'/'LEA', 'Summon — Orcs').
card_original_text('ironclaw orcs'/'LEA', 'Cannot be used to block any creature of power more than 1.').
card_first_print('ironclaw orcs', 'LEA').
card_image_name('ironclaw orcs'/'LEA', 'ironclaw orcs').
card_uid('ironclaw orcs'/'LEA', 'LEA:Ironclaw Orcs:ironclaw orcs').
card_rarity('ironclaw orcs'/'LEA', 'Common').
card_artist('ironclaw orcs'/'LEA', 'Anson Maddocks').
card_flavor_text('ironclaw orcs'/'LEA', 'Generations of genetic weeding have given rise to the deviously cowardly Ironclaw clan. To say that Orcs in general are vicious, depraved, and ignoble does not do justice to the Ironclaw.').
card_multiverse_id('ironclaw orcs'/'LEA', '207').

card_in_set('ironroot treefolk', 'LEA').
card_original_type('ironroot treefolk'/'LEA', 'Summon — Treefolk').
card_original_text('ironroot treefolk'/'LEA', '').
card_first_print('ironroot treefolk', 'LEA').
card_image_name('ironroot treefolk'/'LEA', 'ironroot treefolk').
card_uid('ironroot treefolk'/'LEA', 'LEA:Ironroot Treefolk:ironroot treefolk').
card_rarity('ironroot treefolk'/'LEA', 'Common').
card_artist('ironroot treefolk'/'LEA', 'Jesper Myrfors').
card_flavor_text('ironroot treefolk'/'LEA', 'The mating habits of Treefolk, particularly the stalwart Ironroot Treefolk, are truly absurd. Molasses comes to mind. It\'s amazing the species can survive at all given such protracted periods of mate selection, conjugation, and gestation.').
card_multiverse_id('ironroot treefolk'/'LEA', '159').

card_in_set('island', 'LEA').
card_original_type('island'/'LEA', 'Land').
card_original_text('island'/'LEA', 'Tap to add {U} to your mana pool.').
card_first_print('island', 'LEA').
card_image_name('island'/'LEA', 'island1').
card_uid('island'/'LEA', 'LEA:Island:island1').
card_rarity('island'/'LEA', 'Basic Land').
card_artist('island'/'LEA', 'Mark Poole').
card_multiverse_id('island'/'LEA', '293').

card_in_set('island', 'LEA').
card_original_type('island'/'LEA', 'Land').
card_original_text('island'/'LEA', 'Tap to add {U} to your mana pool.').
card_image_name('island'/'LEA', 'island2').
card_uid('island'/'LEA', 'LEA:Island:island2').
card_rarity('island'/'LEA', 'Basic Land').
card_artist('island'/'LEA', 'Mark Poole').
card_multiverse_id('island'/'LEA', '292').

card_in_set('island sanctuary', 'LEA').
card_original_type('island sanctuary'/'LEA', 'Enchantment').
card_original_text('island sanctuary'/'LEA', 'You may decline to draw a card from your library during draw phase. In exchange, until start of your next turn the only creatures that can damage you are those that fly or have islandwalk.').
card_first_print('island sanctuary', 'LEA').
card_image_name('island sanctuary'/'LEA', 'island sanctuary').
card_uid('island sanctuary'/'LEA', 'LEA:Island Sanctuary:island sanctuary').
card_rarity('island sanctuary'/'LEA', 'Rare').
card_artist('island sanctuary'/'LEA', 'Mark Poole').
card_multiverse_id('island sanctuary'/'LEA', '256').

card_in_set('ivory cup', 'LEA').
card_original_type('ivory cup'/'LEA', 'Poly Artifact').
card_original_text('ivory cup'/'LEA', '{1}: Any white spell cast by any player gives you 1 life.').
card_first_print('ivory cup', 'LEA').
card_image_name('ivory cup'/'LEA', 'ivory cup').
card_uid('ivory cup'/'LEA', 'LEA:Ivory Cup:ivory cup').
card_rarity('ivory cup'/'LEA', 'Uncommon').
card_artist('ivory cup'/'LEA', 'Anson Maddocks').
card_multiverse_id('ivory cup'/'LEA', '22').

card_in_set('jade monolith', 'LEA').
card_original_type('jade monolith'/'LEA', 'Poly Artifact').
card_original_text('jade monolith'/'LEA', '{1}: You may take damage done to any creature on yourself instead, but you must take all of it. Source of damage is unchanged.').
card_first_print('jade monolith', 'LEA').
card_image_name('jade monolith'/'LEA', 'jade monolith').
card_uid('jade monolith'/'LEA', 'LEA:Jade Monolith:jade monolith').
card_rarity('jade monolith'/'LEA', 'Rare').
card_artist('jade monolith'/'LEA', 'Anson Maddocks').
card_multiverse_id('jade monolith'/'LEA', '23').

card_in_set('jade statue', 'LEA').
card_original_type('jade statue'/'LEA', 'Artifact').
card_original_text('jade statue'/'LEA', '{2}: Jade Statue becomes a creature for the duration of the current attack exchange. Can be a creature only during an attack or defense.').
card_first_print('jade statue', 'LEA').
card_image_name('jade statue'/'LEA', 'jade statue').
card_uid('jade statue'/'LEA', 'LEA:Jade Statue:jade statue').
card_rarity('jade statue'/'LEA', 'Uncommon').
card_artist('jade statue'/'LEA', 'Dan Frazier').
card_flavor_text('jade statue'/'LEA', '\"Some of the other guys dared me to touch it, but I knew it weren\'t no ordinary hunk o\' rock.\" —Norin the Wary').
card_multiverse_id('jade statue'/'LEA', '24').

card_in_set('jayemdae tome', 'LEA').
card_original_type('jayemdae tome'/'LEA', 'Mono Artifact').
card_original_text('jayemdae tome'/'LEA', '{4}: You may draw one extra card.').
card_first_print('jayemdae tome', 'LEA').
card_image_name('jayemdae tome'/'LEA', 'jayemdae tome').
card_uid('jayemdae tome'/'LEA', 'LEA:Jayemdae Tome:jayemdae tome').
card_rarity('jayemdae tome'/'LEA', 'Rare').
card_artist('jayemdae tome'/'LEA', 'Mark Tedin').
card_multiverse_id('jayemdae tome'/'LEA', '25').

card_in_set('juggernaut', 'LEA').
card_original_type('juggernaut'/'LEA', 'Artifact Creature').
card_original_text('juggernaut'/'LEA', 'Must attack each turn if possible. Can\'t be blocked by walls.').
card_first_print('juggernaut', 'LEA').
card_image_name('juggernaut'/'LEA', 'juggernaut').
card_uid('juggernaut'/'LEA', 'LEA:Juggernaut:juggernaut').
card_rarity('juggernaut'/'LEA', 'Uncommon').
card_artist('juggernaut'/'LEA', 'Dan Frazier').
card_flavor_text('juggernaut'/'LEA', 'We had taken refuge in a small cave, thinking the entrance was too narrow for it to follow. To our horror, its gigantic head smashed into the mountainside, ripping itself a new entrance.').
card_multiverse_id('juggernaut'/'LEA', '26').

card_in_set('jump', 'LEA').
card_original_type('jump'/'LEA', 'Instant').
card_original_text('jump'/'LEA', 'Target creature is a flying creature until end of turn.').
card_first_print('jump', 'LEA').
card_image_name('jump'/'LEA', 'jump').
card_uid('jump'/'LEA', 'LEA:Jump:jump').
card_rarity('jump'/'LEA', 'Common').
card_artist('jump'/'LEA', 'Mark Poole').
card_multiverse_id('jump'/'LEA', '108').

card_in_set('karma', 'LEA').
card_original_type('karma'/'LEA', 'Enchantment').
card_original_text('karma'/'LEA', 'Karma does 1 damage to player for each swamp player has in play. Damage occurs during player\'s upkeep. Affects both players.').
card_first_print('karma', 'LEA').
card_image_name('karma'/'LEA', 'karma').
card_uid('karma'/'LEA', 'LEA:Karma:karma').
card_rarity('karma'/'LEA', 'Uncommon').
card_artist('karma'/'LEA', 'Richard Thomas').
card_multiverse_id('karma'/'LEA', '257').

card_in_set('keldon warlord', 'LEA').
card_original_type('keldon warlord'/'LEA', 'Summon — Lord').
card_original_text('keldon warlord'/'LEA', 'The Xs below are the number of non-wall creatures on your side, including Warlord. Thus if you have 2 other non-wall creatures, Warlord is 3/3. If one of those creatures is killed during the turn, Warlord immediately becomes 2/2.').
card_first_print('keldon warlord', 'LEA').
card_image_name('keldon warlord'/'LEA', 'keldon warlord').
card_uid('keldon warlord'/'LEA', 'LEA:Keldon Warlord:keldon warlord').
card_rarity('keldon warlord'/'LEA', 'Uncommon').
card_artist('keldon warlord'/'LEA', 'Kev Brockschmidt').
card_multiverse_id('keldon warlord'/'LEA', '208').

card_in_set('kormus bell', 'LEA').
card_original_type('kormus bell'/'LEA', 'Continuous Artifact').
card_original_text('kormus bell'/'LEA', 'Treat all swamps in play as 1/1 creatures. Now they can be enchanted, killed, and so forth, and they can be tapped either for mana or to attack. Swamps have no color; they are not considered black cards.').
card_first_print('kormus bell', 'LEA').
card_image_name('kormus bell'/'LEA', 'kormus bell').
card_uid('kormus bell'/'LEA', 'LEA:Kormus Bell:kormus bell').
card_rarity('kormus bell'/'LEA', 'Rare').
card_artist('kormus bell'/'LEA', 'Christopher Rush').
card_multiverse_id('kormus bell'/'LEA', '27').

card_in_set('kudzu', 'LEA').
card_original_type('kudzu'/'LEA', 'Enchant Land').
card_original_text('kudzu'/'LEA', 'When target land is tapped, it is destroyed. Unless that was the last land in play, Kudzu is not discarded; instead, the player whose land it just destroyed may place it on another land of his or her choice.').
card_first_print('kudzu', 'LEA').
card_image_name('kudzu'/'LEA', 'kudzu').
card_uid('kudzu'/'LEA', 'LEA:Kudzu:kudzu').
card_rarity('kudzu'/'LEA', 'Rare').
card_artist('kudzu'/'LEA', 'Mark Poole').
card_multiverse_id('kudzu'/'LEA', '160').

card_in_set('lance', 'LEA').
card_original_type('lance'/'LEA', 'Enchant Creature').
card_original_text('lance'/'LEA', 'Target creature gains first strike.').
card_first_print('lance', 'LEA').
card_image_name('lance'/'LEA', 'lance').
card_uid('lance'/'LEA', 'LEA:Lance:lance').
card_rarity('lance'/'LEA', 'Uncommon').
card_artist('lance'/'LEA', 'Rob Alexander').
card_multiverse_id('lance'/'LEA', '258').

card_in_set('ley druid', 'LEA').
card_original_type('ley druid'/'LEA', 'Summon — Cleric').
card_original_text('ley druid'/'LEA', 'Tap Druid to untap a land of your choice. This action can be played as an interrupt.').
card_first_print('ley druid', 'LEA').
card_image_name('ley druid'/'LEA', 'ley druid').
card_uid('ley druid'/'LEA', 'LEA:Ley Druid:ley druid').
card_rarity('ley druid'/'LEA', 'Uncommon').
card_artist('ley druid'/'LEA', 'Sandra Everingham').
card_flavor_text('ley druid'/'LEA', 'After years of training, the Druid becomes one with nature, drawing power from the land and returning it when needed.').
card_multiverse_id('ley druid'/'LEA', '161').

card_in_set('library of leng', 'LEA').
card_original_type('library of leng'/'LEA', 'Continuous Artifact').
card_original_text('library of leng'/'LEA', 'There is no limit to size of your hand. If a card forces you to discard, you may choose to discard to top of your library rather than to graveyard. If discard is random, you may look at card before deciding where to discard it.').
card_first_print('library of leng', 'LEA').
card_image_name('library of leng'/'LEA', 'library of leng').
card_uid('library of leng'/'LEA', 'LEA:Library of Leng:library of leng').
card_rarity('library of leng'/'LEA', 'Uncommon').
card_artist('library of leng'/'LEA', 'Daniel Gelon').
card_multiverse_id('library of leng'/'LEA', '28').

card_in_set('lich', 'LEA').
card_original_type('lich'/'LEA', 'Enchantment').
card_original_text('lich'/'LEA', 'You lose all life. If you gain life later in the game, instead draw one card from your library for each life. For each point of damage you suffer, you must destroy one of your cards in play. Creatures destroyed in this way cannot be regenerated. You lose if this enchantment is destroyed or if you suffer a point of damage without sending a card to the graveyard.').
card_first_print('lich', 'LEA').
card_image_name('lich'/'LEA', 'lich').
card_uid('lich'/'LEA', 'LEA:Lich:lich').
card_rarity('lich'/'LEA', 'Rare').
card_artist('lich'/'LEA', 'Daniel Gelon').
card_multiverse_id('lich'/'LEA', '69').

card_in_set('lifeforce', 'LEA').
card_original_type('lifeforce'/'LEA', 'Enchantment').
card_original_text('lifeforce'/'LEA', '{G}{G}: Destroy a black spell as it is being cast. This use may be played as an interrupt, and does not affect black cards already in play.').
card_first_print('lifeforce', 'LEA').
card_image_name('lifeforce'/'LEA', 'lifeforce').
card_uid('lifeforce'/'LEA', 'LEA:Lifeforce:lifeforce').
card_rarity('lifeforce'/'LEA', 'Uncommon').
card_artist('lifeforce'/'LEA', 'Dameon Willich').
card_multiverse_id('lifeforce'/'LEA', '162').

card_in_set('lifelace', 'LEA').
card_original_type('lifelace'/'LEA', 'Interrupt').
card_original_text('lifelace'/'LEA', 'Changes the color of one card either being played or already in play to green. Cost to cast, tap, maintain, or use a special ability of target card remains entirely unchanged.').
card_first_print('lifelace', 'LEA').
card_image_name('lifelace'/'LEA', 'lifelace').
card_uid('lifelace'/'LEA', 'LEA:Lifelace:lifelace').
card_rarity('lifelace'/'LEA', 'Rare').
card_artist('lifelace'/'LEA', 'Amy Weber').
card_multiverse_id('lifelace'/'LEA', '163').

card_in_set('lifetap', 'LEA').
card_original_type('lifetap'/'LEA', 'Enchantment').
card_original_text('lifetap'/'LEA', 'You gain 1 life each time any forest of opponent\'s becomes tapped.').
card_first_print('lifetap', 'LEA').
card_image_name('lifetap'/'LEA', 'lifetap').
card_uid('lifetap'/'LEA', 'LEA:Lifetap:lifetap').
card_rarity('lifetap'/'LEA', 'Uncommon').
card_artist('lifetap'/'LEA', 'Anson Maddocks').
card_multiverse_id('lifetap'/'LEA', '109').

card_in_set('lightning bolt', 'LEA').
card_original_type('lightning bolt'/'LEA', 'Instant').
card_original_text('lightning bolt'/'LEA', 'Lightning Bolt does 3 damage to one target.').
card_first_print('lightning bolt', 'LEA').
card_image_name('lightning bolt'/'LEA', 'lightning bolt').
card_uid('lightning bolt'/'LEA', 'LEA:Lightning Bolt:lightning bolt').
card_rarity('lightning bolt'/'LEA', 'Common').
card_artist('lightning bolt'/'LEA', 'Christopher Rush').
card_multiverse_id('lightning bolt'/'LEA', '209').

card_in_set('living artifact', 'LEA').
card_original_type('living artifact'/'LEA', 'Enchant Artifact').
card_original_text('living artifact'/'LEA', 'Put a counter on target artifact for each life you lose. During upkeep you may trade one counter for one life, but you can only trade in one counter each turn.').
card_first_print('living artifact', 'LEA').
card_image_name('living artifact'/'LEA', 'living artifact').
card_uid('living artifact'/'LEA', 'LEA:Living Artifact:living artifact').
card_rarity('living artifact'/'LEA', 'Rare').
card_artist('living artifact'/'LEA', 'Anson Maddocks').
card_multiverse_id('living artifact'/'LEA', '164').

card_in_set('living lands', 'LEA').
card_original_type('living lands'/'LEA', 'Enchantment').
card_original_text('living lands'/'LEA', 'Treat all forests in play as 1/1 creatures. Now they can be enchanted, killed, and so forth, and they can be tapped either for mana or to attack. The living lands have no color; they are not considered green cards.').
card_first_print('living lands', 'LEA').
card_image_name('living lands'/'LEA', 'living lands').
card_uid('living lands'/'LEA', 'LEA:Living Lands:living lands').
card_rarity('living lands'/'LEA', 'Rare').
card_artist('living lands'/'LEA', 'Jesper Myrfors').
card_multiverse_id('living lands'/'LEA', '165').

card_in_set('living wall', 'LEA').
card_original_type('living wall'/'LEA', 'Artifact Creature').
card_original_text('living wall'/'LEA', 'Counts as a wall. {1}: Regenerates.').
card_first_print('living wall', 'LEA').
card_image_name('living wall'/'LEA', 'living wall').
card_uid('living wall'/'LEA', 'LEA:Living Wall:living wall').
card_rarity('living wall'/'LEA', 'Uncommon').
card_artist('living wall'/'LEA', 'Anson Maddocks').
card_flavor_text('living wall'/'LEA', 'Some fiendish mage had created a horrifying wall of living flesh, patched together from a jumble of still-recognizable body parts. As we sought to hew our way through it, some unknown power healed the gaping wounds we cut, denying us passage.').
card_multiverse_id('living wall'/'LEA', '29').

card_in_set('llanowar elves', 'LEA').
card_original_type('llanowar elves'/'LEA', 'Summon — Elves').
card_original_text('llanowar elves'/'LEA', 'Tap to add 1 green mana to your mana pool. This tap can be played as an interrupt.').
card_first_print('llanowar elves', 'LEA').
card_image_name('llanowar elves'/'LEA', 'llanowar elves').
card_uid('llanowar elves'/'LEA', 'LEA:Llanowar Elves:llanowar elves').
card_rarity('llanowar elves'/'LEA', 'Common').
card_artist('llanowar elves'/'LEA', 'Anson Maddocks').
card_flavor_text('llanowar elves'/'LEA', 'Whenever the Llanowar Elves gather the fruits of their forest, they leave one plant of each type untouched, considering that nature\'s portion.').
card_multiverse_id('llanowar elves'/'LEA', '166').

card_in_set('lord of atlantis', 'LEA').
card_original_type('lord of atlantis'/'LEA', 'Summon — Lord of Atlantis').
card_original_text('lord of atlantis'/'LEA', 'All Merfolk in play gain islandwalk and +1/+1 while this card is in play.').
card_first_print('lord of atlantis', 'LEA').
card_image_name('lord of atlantis'/'LEA', 'lord of atlantis').
card_uid('lord of atlantis'/'LEA', 'LEA:Lord of Atlantis:lord of atlantis').
card_rarity('lord of atlantis'/'LEA', 'Rare').
card_artist('lord of atlantis'/'LEA', 'Melissa A. Benson').
card_flavor_text('lord of atlantis'/'LEA', 'A master of tactics, the Lord of Atlantis makes his people bold in battle merely by arriving to lead them.').
card_multiverse_id('lord of atlantis'/'LEA', '110').

card_in_set('lord of the pit', 'LEA').
card_original_type('lord of the pit'/'LEA', 'Summon — Demon').
card_original_text('lord of the pit'/'LEA', 'Flying, trample\nYou must sacrifice one of your own creatures during upkeep or Lord of the Pit does 7 damage to you. You may still attack with Lord of the Pit even if you failed to sacrifice a creature.').
card_first_print('lord of the pit', 'LEA').
card_image_name('lord of the pit'/'LEA', 'lord of the pit').
card_uid('lord of the pit'/'LEA', 'LEA:Lord of the Pit:lord of the pit').
card_rarity('lord of the pit'/'LEA', 'Rare').
card_artist('lord of the pit'/'LEA', 'Mark Tedin').
card_multiverse_id('lord of the pit'/'LEA', '70').

card_in_set('lure', 'LEA').
card_original_type('lure'/'LEA', 'Enchant Creature').
card_original_text('lure'/'LEA', 'All creatures able to block target creature must do so. If a creature has the ability to block more than one creature, Lure does not prevent this. If there is more than one attacking creature with Lure, defender may choose which of them each defending creature blocks.').
card_first_print('lure', 'LEA').
card_image_name('lure'/'LEA', 'lure').
card_uid('lure'/'LEA', 'LEA:Lure:lure').
card_rarity('lure'/'LEA', 'Uncommon').
card_artist('lure'/'LEA', 'Anson Maddocks').
card_multiverse_id('lure'/'LEA', '167').

card_in_set('magical hack', 'LEA').
card_original_type('magical hack'/'LEA', 'Interrupt').
card_original_text('magical hack'/'LEA', 'Change the text of any card being played or already in play by replacing one basic land type with another. For example, you can change \"swampwalk\" to \"plainswalk.\"').
card_first_print('magical hack', 'LEA').
card_image_name('magical hack'/'LEA', 'magical hack').
card_uid('magical hack'/'LEA', 'LEA:Magical Hack:magical hack').
card_rarity('magical hack'/'LEA', 'Rare').
card_artist('magical hack'/'LEA', 'Julie Baroh').
card_multiverse_id('magical hack'/'LEA', '111').

card_in_set('mahamoti djinn', 'LEA').
card_original_type('mahamoti djinn'/'LEA', 'Summon — Djinn').
card_original_text('mahamoti djinn'/'LEA', 'Flying').
card_first_print('mahamoti djinn', 'LEA').
card_image_name('mahamoti djinn'/'LEA', 'mahamoti djinn').
card_uid('mahamoti djinn'/'LEA', 'LEA:Mahamoti Djinn:mahamoti djinn').
card_rarity('mahamoti djinn'/'LEA', 'Rare').
card_artist('mahamoti djinn'/'LEA', 'Dan Frazier').
card_flavor_text('mahamoti djinn'/'LEA', 'Of royal blood among the spirits of the air, the Mahamoti Djinn rides on the wings of the winds. As dangerous in the gambling hall as he is in battle, he is a master of trickery and misdirection.').
card_multiverse_id('mahamoti djinn'/'LEA', '112').

card_in_set('mana flare', 'LEA').
card_original_type('mana flare'/'LEA', 'Enchantment').
card_original_text('mana flare'/'LEA', 'Whenever either player taps land for mana, each land produces 1 extra mana of the appropriate type.').
card_first_print('mana flare', 'LEA').
card_image_name('mana flare'/'LEA', 'mana flare').
card_uid('mana flare'/'LEA', 'LEA:Mana Flare:mana flare').
card_rarity('mana flare'/'LEA', 'Rare').
card_artist('mana flare'/'LEA', 'Christopher Rush').
card_multiverse_id('mana flare'/'LEA', '210').

card_in_set('mana short', 'LEA').
card_original_type('mana short'/'LEA', 'Instant').
card_original_text('mana short'/'LEA', 'All opponent\'s lands are tapped, and opponent\'s mana pool is emptied.').
card_first_print('mana short', 'LEA').
card_image_name('mana short'/'LEA', 'mana short').
card_uid('mana short'/'LEA', 'LEA:Mana Short:mana short').
card_rarity('mana short'/'LEA', 'Rare').
card_artist('mana short'/'LEA', 'Dameon Willich').
card_multiverse_id('mana short'/'LEA', '113').

card_in_set('mana vault', 'LEA').
card_original_type('mana vault'/'LEA', 'Mono Artifact').
card_original_text('mana vault'/'LEA', 'Tap to add 3 colorless mana to your mana pool. Mana Vault doesn\'t untap normally during untap phase; to untap it, you must pay 4 mana. If Mana Vault remains tapped during upkeep it does 1 damage to you. Tapping this artifact can be played as an interrupt.').
card_first_print('mana vault', 'LEA').
card_image_name('mana vault'/'LEA', 'mana vault').
card_uid('mana vault'/'LEA', 'LEA:Mana Vault:mana vault').
card_rarity('mana vault'/'LEA', 'Rare').
card_artist('mana vault'/'LEA', 'Mark Tedin').
card_multiverse_id('mana vault'/'LEA', '30').

card_in_set('manabarbs', 'LEA').
card_original_type('manabarbs'/'LEA', 'Enchantment').
card_original_text('manabarbs'/'LEA', 'Whenever any land is tapped, Manabarbs does 1 damage to the land\'s controller.').
card_first_print('manabarbs', 'LEA').
card_image_name('manabarbs'/'LEA', 'manabarbs').
card_uid('manabarbs'/'LEA', 'LEA:Manabarbs:manabarbs').
card_rarity('manabarbs'/'LEA', 'Rare').
card_artist('manabarbs'/'LEA', 'Christopher Rush').
card_multiverse_id('manabarbs'/'LEA', '211').

card_in_set('meekstone', 'LEA').
card_original_type('meekstone'/'LEA', 'Continuous Artifact').
card_original_text('meekstone'/'LEA', 'Any creature with power greater than 2 may not be untapped as normal during the untap phase.').
card_first_print('meekstone', 'LEA').
card_image_name('meekstone'/'LEA', 'meekstone').
card_uid('meekstone'/'LEA', 'LEA:Meekstone:meekstone').
card_rarity('meekstone'/'LEA', 'Rare').
card_artist('meekstone'/'LEA', 'Quinton Hoover').
card_multiverse_id('meekstone'/'LEA', '31').

card_in_set('merfolk of the pearl trident', 'LEA').
card_original_type('merfolk of the pearl trident'/'LEA', 'Summon — Merfolk').
card_original_text('merfolk of the pearl trident'/'LEA', '').
card_first_print('merfolk of the pearl trident', 'LEA').
card_image_name('merfolk of the pearl trident'/'LEA', 'merfolk of the pearl trident').
card_uid('merfolk of the pearl trident'/'LEA', 'LEA:Merfolk of the Pearl Trident:merfolk of the pearl trident').
card_rarity('merfolk of the pearl trident'/'LEA', 'Common').
card_artist('merfolk of the pearl trident'/'LEA', 'Jeff A. Menges').
card_flavor_text('merfolk of the pearl trident'/'LEA', 'Most human scholars believe that Merfolk are the survivors of sunken Atlantis, humans adapted to the water. Merfolk, however, believe that humans sprang forth from Merfolk who adapted themselves in order to explore their last frontier.').
card_multiverse_id('merfolk of the pearl trident'/'LEA', '114').

card_in_set('mesa pegasus', 'LEA').
card_original_type('mesa pegasus'/'LEA', 'Summon — Pegasus').
card_original_text('mesa pegasus'/'LEA', 'Flying, bands').
card_first_print('mesa pegasus', 'LEA').
card_image_name('mesa pegasus'/'LEA', 'mesa pegasus').
card_uid('mesa pegasus'/'LEA', 'LEA:Mesa Pegasus:mesa pegasus').
card_rarity('mesa pegasus'/'LEA', 'Common').
card_artist('mesa pegasus'/'LEA', 'Melissa A. Benson').
card_flavor_text('mesa pegasus'/'LEA', 'Before a woman marries in the village of Sursi, she must visit the land of the Mesa Pegasus. Legend has it that if the woman is pure of heart and her love is true, a Mesa Pegasus will appear, blessing her family with long life and good fortune.').
card_multiverse_id('mesa pegasus'/'LEA', '259').

card_in_set('mind twist', 'LEA').
card_original_type('mind twist'/'LEA', 'Sorcery').
card_original_text('mind twist'/'LEA', 'Opponent must discard X cards at random from hand. If opponent doesn\'t have enough cards in hand, entire hand is discarded.').
card_first_print('mind twist', 'LEA').
card_image_name('mind twist'/'LEA', 'mind twist').
card_uid('mind twist'/'LEA', 'LEA:Mind Twist:mind twist').
card_rarity('mind twist'/'LEA', 'Rare').
card_artist('mind twist'/'LEA', 'Julie Baroh').
card_multiverse_id('mind twist'/'LEA', '71').

card_in_set('mons\'s goblin raiders', 'LEA').
card_original_type('mons\'s goblin raiders'/'LEA', 'Summon — Goblins').
card_original_text('mons\'s goblin raiders'/'LEA', '').
card_first_print('mons\'s goblin raiders', 'LEA').
card_image_name('mons\'s goblin raiders'/'LEA', 'mons\'s goblin raiders').
card_uid('mons\'s goblin raiders'/'LEA', 'LEA:Mons\'s Goblin Raiders:mons\'s goblin raiders').
card_rarity('mons\'s goblin raiders'/'LEA', 'Common').
card_artist('mons\'s goblin raiders'/'LEA', 'Jeff A. Menges').
card_flavor_text('mons\'s goblin raiders'/'LEA', 'The intricate dynamics of Rundvelt Goblin affairs are often confused with anarchy. The chaos, however, is the chaos of a thundercloud, and direction will sporadically and violently appear. Pashalik Mons and his raiders are the thunderhead that leads in the storm.').
card_multiverse_id('mons\'s goblin raiders'/'LEA', '212').

card_in_set('mountain', 'LEA').
card_original_type('mountain'/'LEA', 'Land').
card_original_text('mountain'/'LEA', 'Tap to add {R} to your mana pool.').
card_first_print('mountain', 'LEA').
card_image_name('mountain'/'LEA', 'mountain1').
card_uid('mountain'/'LEA', 'LEA:Mountain:mountain1').
card_rarity('mountain'/'LEA', 'Basic Land').
card_artist('mountain'/'LEA', 'Douglas Shuler').
card_multiverse_id('mountain'/'LEA', '290').

card_in_set('mountain', 'LEA').
card_original_type('mountain'/'LEA', 'Land').
card_original_text('mountain'/'LEA', 'Tap to add {R} to your mana pool.').
card_image_name('mountain'/'LEA', 'mountain2').
card_uid('mountain'/'LEA', 'LEA:Mountain:mountain2').
card_rarity('mountain'/'LEA', 'Basic Land').
card_artist('mountain'/'LEA', 'Douglas Shuler').
card_multiverse_id('mountain'/'LEA', '291').

card_in_set('mox emerald', 'LEA').
card_original_type('mox emerald'/'LEA', 'Mono Artifact').
card_original_text('mox emerald'/'LEA', 'Add 1 green mana to your mana pool. Tapping this artifact can be played as an interrupt.').
card_first_print('mox emerald', 'LEA').
card_image_name('mox emerald'/'LEA', 'mox emerald').
card_uid('mox emerald'/'LEA', 'LEA:Mox Emerald:mox emerald').
card_rarity('mox emerald'/'LEA', 'Rare').
card_artist('mox emerald'/'LEA', 'Dan Frazier').
card_multiverse_id('mox emerald'/'LEA', '32').

card_in_set('mox jet', 'LEA').
card_original_type('mox jet'/'LEA', 'Mono Artifact').
card_original_text('mox jet'/'LEA', 'Add 1 black mana to your mana pool. Tapping this artifact can be played as an interrupt.').
card_first_print('mox jet', 'LEA').
card_image_name('mox jet'/'LEA', 'mox jet').
card_uid('mox jet'/'LEA', 'LEA:Mox Jet:mox jet').
card_rarity('mox jet'/'LEA', 'Rare').
card_artist('mox jet'/'LEA', 'Dan Frazier').
card_multiverse_id('mox jet'/'LEA', '33').

card_in_set('mox pearl', 'LEA').
card_original_type('mox pearl'/'LEA', 'Mono Artifact').
card_original_text('mox pearl'/'LEA', 'Add 1 white mana to your mana pool. Tapping this artifact can be played as an interrupt.').
card_first_print('mox pearl', 'LEA').
card_image_name('mox pearl'/'LEA', 'mox pearl').
card_uid('mox pearl'/'LEA', 'LEA:Mox Pearl:mox pearl').
card_rarity('mox pearl'/'LEA', 'Rare').
card_artist('mox pearl'/'LEA', 'Dan Frazier').
card_multiverse_id('mox pearl'/'LEA', '34').

card_in_set('mox ruby', 'LEA').
card_original_type('mox ruby'/'LEA', 'Mono Artifact').
card_original_text('mox ruby'/'LEA', 'Add 1 red mana to your mana pool. Tapping this artifact can be played as an interrupt.').
card_first_print('mox ruby', 'LEA').
card_image_name('mox ruby'/'LEA', 'mox ruby').
card_uid('mox ruby'/'LEA', 'LEA:Mox Ruby:mox ruby').
card_rarity('mox ruby'/'LEA', 'Rare').
card_artist('mox ruby'/'LEA', 'Dan Frazier').
card_multiverse_id('mox ruby'/'LEA', '35').

card_in_set('mox sapphire', 'LEA').
card_original_type('mox sapphire'/'LEA', 'Mono Artifact').
card_original_text('mox sapphire'/'LEA', 'Add 1 blue mana to your mana pool. Tapping this artifact can be played as an interrupt.').
card_first_print('mox sapphire', 'LEA').
card_image_name('mox sapphire'/'LEA', 'mox sapphire').
card_uid('mox sapphire'/'LEA', 'LEA:Mox Sapphire:mox sapphire').
card_rarity('mox sapphire'/'LEA', 'Rare').
card_artist('mox sapphire'/'LEA', 'Dan Frazier').
card_multiverse_id('mox sapphire'/'LEA', '36').

card_in_set('natural selection', 'LEA').
card_original_type('natural selection'/'LEA', 'Instant').
card_original_text('natural selection'/'LEA', 'Look at top three cards of any player\'s library. You may opt to rearrange those three cards or shuffle the entire library.').
card_first_print('natural selection', 'LEA').
card_image_name('natural selection'/'LEA', 'natural selection').
card_uid('natural selection'/'LEA', 'LEA:Natural Selection:natural selection').
card_rarity('natural selection'/'LEA', 'Rare').
card_artist('natural selection'/'LEA', 'Mark Poole').
card_multiverse_id('natural selection'/'LEA', '168').

card_in_set('nether shadow', 'LEA').
card_original_type('nether shadow'/'LEA', 'Summon — Shadow').
card_original_text('nether shadow'/'LEA', 'If Shadow is in graveyard with any combination of cards above it that includes at least three creatures, it can be returned to play during upkeep for its normal casting cost. Shadow can attack on same turn summoned or returned to play.').
card_first_print('nether shadow', 'LEA').
card_image_name('nether shadow'/'LEA', 'nether shadow').
card_uid('nether shadow'/'LEA', 'LEA:Nether Shadow:nether shadow').
card_rarity('nether shadow'/'LEA', 'Rare').
card_artist('nether shadow'/'LEA', 'Christopher Rush').
card_multiverse_id('nether shadow'/'LEA', '72').

card_in_set('nettling imp', 'LEA').
card_original_type('nettling imp'/'LEA', 'Summon — Imp').
card_original_text('nettling imp'/'LEA', 'Tap to force a particular one of opponent\'s non-wall creatures to attack. If target creature cannot attack, it is destroyed at end of turn. This tap should be played during opponent\'s turn, before the attack. May not be used on creatures summoned this turn.').
card_first_print('nettling imp', 'LEA').
card_image_name('nettling imp'/'LEA', 'nettling imp').
card_uid('nettling imp'/'LEA', 'LEA:Nettling Imp:nettling imp').
card_rarity('nettling imp'/'LEA', 'Uncommon').
card_artist('nettling imp'/'LEA', 'Quinton Hoover').
card_multiverse_id('nettling imp'/'LEA', '73').

card_in_set('nevinyrral\'s disk', 'LEA').
card_original_type('nevinyrral\'s disk'/'LEA', 'Mono Artifact').
card_original_text('nevinyrral\'s disk'/'LEA', '{1}: Destroys all creatures, enchantments, and artifacts in play. Disk begins tapped but can be untapped as usual. Disk destroys itself when used.').
card_first_print('nevinyrral\'s disk', 'LEA').
card_image_name('nevinyrral\'s disk'/'LEA', 'nevinyrral\'s disk').
card_uid('nevinyrral\'s disk'/'LEA', 'LEA:Nevinyrral\'s Disk:nevinyrral\'s disk').
card_rarity('nevinyrral\'s disk'/'LEA', 'Rare').
card_artist('nevinyrral\'s disk'/'LEA', 'Mark Tedin').
card_multiverse_id('nevinyrral\'s disk'/'LEA', '37').

card_in_set('nightmare', 'LEA').
card_original_type('nightmare'/'LEA', 'Summon — Nightmare').
card_original_text('nightmare'/'LEA', 'Flying\nNightmare\'s power and toughness both equal the number of swamps its controller has in play.').
card_first_print('nightmare', 'LEA').
card_image_name('nightmare'/'LEA', 'nightmare').
card_uid('nightmare'/'LEA', 'LEA:Nightmare:nightmare').
card_rarity('nightmare'/'LEA', 'Rare').
card_artist('nightmare'/'LEA', 'Melissa A. Benson').
card_flavor_text('nightmare'/'LEA', 'The Nightmare arises from its lair in the swamps. As the poisoned land spreads, so does the Nightmare\'s rage and terrifying strength.').
card_multiverse_id('nightmare'/'LEA', '74').

card_in_set('northern paladin', 'LEA').
card_original_type('northern paladin'/'LEA', 'Summon — Paladin').
card_original_text('northern paladin'/'LEA', '{W}{W} and tap: Destroys a black card in play. Cannot be used to cancel a black spell as it is being cast.').
card_first_print('northern paladin', 'LEA').
card_image_name('northern paladin'/'LEA', 'northern paladin').
card_uid('northern paladin'/'LEA', 'LEA:Northern Paladin:northern paladin').
card_rarity('northern paladin'/'LEA', 'Rare').
card_artist('northern paladin'/'LEA', 'Douglas Shuler').
card_flavor_text('northern paladin'/'LEA', '\"Look to the north; there you will find aid and comfort.\"\n—The Book of Tal').
card_multiverse_id('northern paladin'/'LEA', '260').

card_in_set('obsianus golem', 'LEA').
card_original_type('obsianus golem'/'LEA', 'Artifact Creature').
card_original_text('obsianus golem'/'LEA', '').
card_first_print('obsianus golem', 'LEA').
card_image_name('obsianus golem'/'LEA', 'obsianus golem').
card_uid('obsianus golem'/'LEA', 'LEA:Obsianus Golem:obsianus golem').
card_rarity('obsianus golem'/'LEA', 'Uncommon').
card_artist('obsianus golem'/'LEA', 'Jesper Myrfors').
card_flavor_text('obsianus golem'/'LEA', '\"The foot stone is connected to the ankle stone, the ankle stone is connected to the leg stone...\"\n—Song of the Artificer').
card_multiverse_id('obsianus golem'/'LEA', '38').

card_in_set('orcish artillery', 'LEA').
card_original_type('orcish artillery'/'LEA', 'Summon — Orcs').
card_original_text('orcish artillery'/'LEA', 'Tap to do 2 damage to any target, but you suffer 3 damage as well.').
card_first_print('orcish artillery', 'LEA').
card_image_name('orcish artillery'/'LEA', 'orcish artillery').
card_uid('orcish artillery'/'LEA', 'LEA:Orcish Artillery:orcish artillery').
card_rarity('orcish artillery'/'LEA', 'Uncommon').
card_artist('orcish artillery'/'LEA', 'Anson Maddocks').
card_flavor_text('orcish artillery'/'LEA', 'In a rare display of ingenuity, the Orcs invented an incredibly destructive weapon. Most Orcish artillerists are those who dared criticize its effectiveness.').
card_multiverse_id('orcish artillery'/'LEA', '213').

card_in_set('orcish oriflamme', 'LEA').
card_original_type('orcish oriflamme'/'LEA', 'Enchantment').
card_original_text('orcish oriflamme'/'LEA', 'When attacking, all of your attacking creatures gain +1/+0.').
card_first_print('orcish oriflamme', 'LEA').
card_image_name('orcish oriflamme'/'LEA', 'orcish oriflamme').
card_uid('orcish oriflamme'/'LEA', 'LEA:Orcish Oriflamme:orcish oriflamme').
card_rarity('orcish oriflamme'/'LEA', 'Uncommon').
card_artist('orcish oriflamme'/'LEA', 'Dan Frazier').
card_multiverse_id('orcish oriflamme'/'LEA', '214').

card_in_set('paralyze', 'LEA').
card_original_type('paralyze'/'LEA', 'Enchant Creature').
card_original_text('paralyze'/'LEA', 'Target creature is not untapped as normal during untap phase unless 4 mana are spent. Tap target creature when Paralyze is cast.').
card_first_print('paralyze', 'LEA').
card_image_name('paralyze'/'LEA', 'paralyze').
card_uid('paralyze'/'LEA', 'LEA:Paralyze:paralyze').
card_rarity('paralyze'/'LEA', 'Common').
card_artist('paralyze'/'LEA', 'Anson Maddocks').
card_multiverse_id('paralyze'/'LEA', '75').

card_in_set('pearled unicorn', 'LEA').
card_original_type('pearled unicorn'/'LEA', 'Summon — Unicorn').
card_original_text('pearled unicorn'/'LEA', '').
card_first_print('pearled unicorn', 'LEA').
card_image_name('pearled unicorn'/'LEA', 'pearled unicorn').
card_uid('pearled unicorn'/'LEA', 'LEA:Pearled Unicorn:pearled unicorn').
card_rarity('pearled unicorn'/'LEA', 'Common').
card_artist('pearled unicorn'/'LEA', 'Cornelius Brudi').
card_flavor_text('pearled unicorn'/'LEA', '\"‘Do you know, I always thought Unicorns were fabulous monsters, too? I never saw one alive before!\' ‘Well, now that we have seen each other,\' said the Unicorn, ‘if you\'ll believe in me, I\'ll believe in you.\'\" —Lewis Carroll').
card_multiverse_id('pearled unicorn'/'LEA', '261').

card_in_set('personal incarnation', 'LEA').
card_original_type('personal incarnation'/'LEA', 'Summon — Avatar').
card_original_text('personal incarnation'/'LEA', 'Caster can redirect any or all damage done to Personal Incarnation to self instead. The source of the damage is unchanged. If Personal Incarnation is destroyed, caster loses half his or her remaining life points, rounding up the loss.').
card_first_print('personal incarnation', 'LEA').
card_image_name('personal incarnation'/'LEA', 'personal incarnation').
card_uid('personal incarnation'/'LEA', 'LEA:Personal Incarnation:personal incarnation').
card_rarity('personal incarnation'/'LEA', 'Rare').
card_artist('personal incarnation'/'LEA', 'Kev Brockschmidt').
card_multiverse_id('personal incarnation'/'LEA', '262').

card_in_set('pestilence', 'LEA').
card_original_type('pestilence'/'LEA', 'Enchantment').
card_original_text('pestilence'/'LEA', '{B} Do 1 damage to each creature and to both players. Pestilence must be discarded at end of any turn in which there are no creatures in play at end of turn.').
card_first_print('pestilence', 'LEA').
card_image_name('pestilence'/'LEA', 'pestilence').
card_uid('pestilence'/'LEA', 'LEA:Pestilence:pestilence').
card_rarity('pestilence'/'LEA', 'Common').
card_artist('pestilence'/'LEA', 'Jesper Myrfors').
card_multiverse_id('pestilence'/'LEA', '76').

card_in_set('phantasmal forces', 'LEA').
card_original_type('phantasmal forces'/'LEA', 'Summon — Phantasm').
card_original_text('phantasmal forces'/'LEA', 'Flying\nController must spend U during upkeep to maintain or Phantasmal Forces are destroyed.').
card_first_print('phantasmal forces', 'LEA').
card_image_name('phantasmal forces'/'LEA', 'phantasmal forces').
card_uid('phantasmal forces'/'LEA', 'LEA:Phantasmal Forces:phantasmal forces').
card_rarity('phantasmal forces'/'LEA', 'Uncommon').
card_artist('phantasmal forces'/'LEA', 'Mark Poole').
card_flavor_text('phantasmal forces'/'LEA', 'These beings embody the essence of true heroes long dead. Summoned from the dreamrealms, they rise to meet their enemies.').
card_multiverse_id('phantasmal forces'/'LEA', '115').

card_in_set('phantasmal terrain', 'LEA').
card_original_type('phantasmal terrain'/'LEA', 'Enchant Land').
card_original_text('phantasmal terrain'/'LEA', 'Target land changes to any basic land type of caster\'s choice. Land type is set when cast and may not be further altered by this enchantment.').
card_first_print('phantasmal terrain', 'LEA').
card_image_name('phantasmal terrain'/'LEA', 'phantasmal terrain').
card_uid('phantasmal terrain'/'LEA', 'LEA:Phantasmal Terrain:phantasmal terrain').
card_rarity('phantasmal terrain'/'LEA', 'Common').
card_artist('phantasmal terrain'/'LEA', 'Dameon Willich').
card_multiverse_id('phantasmal terrain'/'LEA', '116').

card_in_set('phantom monster', 'LEA').
card_original_type('phantom monster'/'LEA', 'Summon — Phantasm').
card_original_text('phantom monster'/'LEA', 'Flying').
card_first_print('phantom monster', 'LEA').
card_image_name('phantom monster'/'LEA', 'phantom monster').
card_uid('phantom monster'/'LEA', 'LEA:Phantom Monster:phantom monster').
card_rarity('phantom monster'/'LEA', 'Uncommon').
card_artist('phantom monster'/'LEA', 'Jesper Myrfors').
card_flavor_text('phantom monster'/'LEA', '\"While, like a ghastly rapid river,\nThrough the pale door,\nA hideous throng rush out forever,\nAnd laugh—but smile no more.\"\n—Edgar Allan Poe, \"The Haunted Palace\"').
card_multiverse_id('phantom monster'/'LEA', '117').

card_in_set('pirate ship', 'LEA').
card_original_type('pirate ship'/'LEA', 'Summon — Ship').
card_original_text('pirate ship'/'LEA', 'Tap to do 1 damage to any target. Cannot attack unless opponent has islands in play, though controller may still tap. Pirate Ship is destroyed immediately if at any time controller has no islands in play.').
card_first_print('pirate ship', 'LEA').
card_image_name('pirate ship'/'LEA', 'pirate ship').
card_uid('pirate ship'/'LEA', 'LEA:Pirate Ship:pirate ship').
card_rarity('pirate ship'/'LEA', 'Rare').
card_artist('pirate ship'/'LEA', 'Tom Wänerstrand').
card_multiverse_id('pirate ship'/'LEA', '118').

card_in_set('plague rats', 'LEA').
card_original_type('plague rats'/'LEA', 'Summon — Rats').
card_original_text('plague rats'/'LEA', 'The Xs below are the number of Plague Rats in play, counting both sides. Thus if there are 2 Plague Rats in play, each has power and toughness 2/2.').
card_first_print('plague rats', 'LEA').
card_image_name('plague rats'/'LEA', 'plague rats').
card_uid('plague rats'/'LEA', 'LEA:Plague Rats:plague rats').
card_rarity('plague rats'/'LEA', 'Common').
card_artist('plague rats'/'LEA', 'Anson Maddocks').
card_flavor_text('plague rats'/'LEA', '\"Should you a Rat to madness tease\nWhy ev\'n a Rat may plague you...\"\n—Samuel Coleridge, \"Recantation\"').
card_multiverse_id('plague rats'/'LEA', '77').

card_in_set('plains', 'LEA').
card_original_type('plains'/'LEA', 'Land').
card_original_text('plains'/'LEA', 'Tap to add {W} to your mana pool.').
card_first_print('plains', 'LEA').
card_image_name('plains'/'LEA', 'plains1').
card_uid('plains'/'LEA', 'LEA:Plains:plains1').
card_rarity('plains'/'LEA', 'Basic Land').
card_artist('plains'/'LEA', 'Jesper Myrfors').
card_multiverse_id('plains'/'LEA', '294').

card_in_set('plains', 'LEA').
card_original_type('plains'/'LEA', 'Land').
card_original_text('plains'/'LEA', 'Tap to add {W} to your mana pool.').
card_image_name('plains'/'LEA', 'plains2').
card_uid('plains'/'LEA', 'LEA:Plains:plains2').
card_rarity('plains'/'LEA', 'Basic Land').
card_artist('plains'/'LEA', 'Jesper Myrfors').
card_multiverse_id('plains'/'LEA', '295').

card_in_set('plateau', 'LEA').
card_original_type('plateau'/'LEA', 'Land').
card_original_text('plateau'/'LEA', 'Counts as both mountains and plains and is affected by spells that affect either. Tap to add either {R} or {W} to your mana pool.').
card_first_print('plateau', 'LEA').
card_image_name('plateau'/'LEA', 'plateau').
card_uid('plateau'/'LEA', 'LEA:Plateau:plateau').
card_rarity('plateau'/'LEA', 'Rare').
card_artist('plateau'/'LEA', 'Drew Tucker').
card_multiverse_id('plateau'/'LEA', '281').

card_in_set('power leak', 'LEA').
card_original_type('power leak'/'LEA', 'Enchant Enchantment').
card_original_text('power leak'/'LEA', 'Target enchantment costs 2 extra mana each turn during upkeep. If target enchantment\'s controller cannot or will not pay this extra mana, Power Leak does 1 damage to him or her for each unpaid mana.').
card_first_print('power leak', 'LEA').
card_image_name('power leak'/'LEA', 'power leak').
card_uid('power leak'/'LEA', 'LEA:Power Leak:power leak').
card_rarity('power leak'/'LEA', 'Common').
card_artist('power leak'/'LEA', 'Drew Tucker').
card_multiverse_id('power leak'/'LEA', '119').

card_in_set('power sink', 'LEA').
card_original_type('power sink'/'LEA', 'Interrupt').
card_original_text('power sink'/'LEA', 'Target spell is countered unless its caster spends X more mana; caster of target spell can\'t choose to let it be countered. If caster of target spell doesn\'t have enough mana, all available mana from lands and mana pool must be paid but target spell will still be countered.').
card_first_print('power sink', 'LEA').
card_image_name('power sink'/'LEA', 'power sink').
card_uid('power sink'/'LEA', 'LEA:Power Sink:power sink').
card_rarity('power sink'/'LEA', 'Common').
card_artist('power sink'/'LEA', 'Richard Thomas').
card_multiverse_id('power sink'/'LEA', '120').

card_in_set('power surge', 'LEA').
card_original_type('power surge'/'LEA', 'Enchantment').
card_original_text('power surge'/'LEA', 'Before untapping lands at the start of a turn, each player takes 1 damage for each land he or she controls but did not tap during the previous turn.').
card_first_print('power surge', 'LEA').
card_image_name('power surge'/'LEA', 'power surge').
card_uid('power surge'/'LEA', 'LEA:Power Surge:power surge').
card_rarity('power surge'/'LEA', 'Rare').
card_artist('power surge'/'LEA', 'Douglas Shuler').
card_multiverse_id('power surge'/'LEA', '215').

card_in_set('prodigal sorcerer', 'LEA').
card_original_type('prodigal sorcerer'/'LEA', 'Summon — Wizard').
card_original_text('prodigal sorcerer'/'LEA', 'Tap to do 1 damage to any target.').
card_first_print('prodigal sorcerer', 'LEA').
card_image_name('prodigal sorcerer'/'LEA', 'prodigal sorcerer').
card_uid('prodigal sorcerer'/'LEA', 'LEA:Prodigal Sorcerer:prodigal sorcerer').
card_rarity('prodigal sorcerer'/'LEA', 'Common').
card_artist('prodigal sorcerer'/'LEA', 'Douglas Shuler').
card_flavor_text('prodigal sorcerer'/'LEA', 'Occasionally a member of the Institute of Arcane Study acquires a taste for worldly pleasures. Seldom do they have trouble finding employment.').
card_multiverse_id('prodigal sorcerer'/'LEA', '121').

card_in_set('psionic blast', 'LEA').
card_original_type('psionic blast'/'LEA', 'Instant').
card_original_text('psionic blast'/'LEA', 'Psionic Blast does 4 damage to any target, but it does 2 damage to you as well.').
card_first_print('psionic blast', 'LEA').
card_image_name('psionic blast'/'LEA', 'psionic blast').
card_uid('psionic blast'/'LEA', 'LEA:Psionic Blast:psionic blast').
card_rarity('psionic blast'/'LEA', 'Uncommon').
card_artist('psionic blast'/'LEA', 'Douglas Shuler').
card_multiverse_id('psionic blast'/'LEA', '122').

card_in_set('psychic venom', 'LEA').
card_original_type('psychic venom'/'LEA', 'Enchant Land').
card_original_text('psychic venom'/'LEA', 'Whenever target land is tapped, Psychic Venom does 2 damage to target land\'s controller.').
card_first_print('psychic venom', 'LEA').
card_image_name('psychic venom'/'LEA', 'psychic venom').
card_uid('psychic venom'/'LEA', 'LEA:Psychic Venom:psychic venom').
card_rarity('psychic venom'/'LEA', 'Common').
card_artist('psychic venom'/'LEA', 'Brian Snõddy').
card_multiverse_id('psychic venom'/'LEA', '123').

card_in_set('purelace', 'LEA').
card_original_type('purelace'/'LEA', 'Interrupt').
card_original_text('purelace'/'LEA', 'Changes the color of one card either being played or already in play to white. Cost to cast, tap, maintain, or use a special ability of target card remains entirely unchanged.').
card_first_print('purelace', 'LEA').
card_image_name('purelace'/'LEA', 'purelace').
card_uid('purelace'/'LEA', 'LEA:Purelace:purelace').
card_rarity('purelace'/'LEA', 'Rare').
card_artist('purelace'/'LEA', 'Sandra Everingham').
card_multiverse_id('purelace'/'LEA', '263').

card_in_set('raging river', 'LEA').
card_original_type('raging river'/'LEA', 'Enchantment').
card_original_text('raging river'/'LEA', 'When you attack, non-flying defending creatures must be divided as opponent wishes between the left and right sides of the River. You then choose on which side of the River to place each attacking creature, and attacking creatures can only be blocked by flying creatures or those on the same side of the River.').
card_first_print('raging river', 'LEA').
card_image_name('raging river'/'LEA', 'raging river').
card_uid('raging river'/'LEA', 'LEA:Raging River:raging river').
card_rarity('raging river'/'LEA', 'Rare').
card_artist('raging river'/'LEA', 'Sandra Everingham').
card_multiverse_id('raging river'/'LEA', '216').

card_in_set('raise dead', 'LEA').
card_original_type('raise dead'/'LEA', 'Sorcery').
card_original_text('raise dead'/'LEA', 'Return creature from your graveyard to your hand.').
card_first_print('raise dead', 'LEA').
card_image_name('raise dead'/'LEA', 'raise dead').
card_uid('raise dead'/'LEA', 'LEA:Raise Dead:raise dead').
card_rarity('raise dead'/'LEA', 'Common').
card_artist('raise dead'/'LEA', 'Jeff A. Menges').
card_multiverse_id('raise dead'/'LEA', '78').

card_in_set('red elemental blast', 'LEA').
card_original_type('red elemental blast'/'LEA', 'Instant').
card_original_text('red elemental blast'/'LEA', 'Counters a blue spell being cast or destroys a blue card in play.').
card_first_print('red elemental blast', 'LEA').
card_image_name('red elemental blast'/'LEA', 'red elemental blast').
card_uid('red elemental blast'/'LEA', 'LEA:Red Elemental Blast:red elemental blast').
card_rarity('red elemental blast'/'LEA', 'Common').
card_artist('red elemental blast'/'LEA', 'Richard Thomas').
card_multiverse_id('red elemental blast'/'LEA', '217').

card_in_set('red ward', 'LEA').
card_original_type('red ward'/'LEA', 'Enchant Creature').
card_original_text('red ward'/'LEA', 'Target creature gains protection from red.').
card_first_print('red ward', 'LEA').
card_image_name('red ward'/'LEA', 'red ward').
card_uid('red ward'/'LEA', 'LEA:Red Ward:red ward').
card_rarity('red ward'/'LEA', 'Uncommon').
card_artist('red ward'/'LEA', 'Dan Frazier').
card_multiverse_id('red ward'/'LEA', '264').

card_in_set('regeneration', 'LEA').
card_original_type('regeneration'/'LEA', 'Enchant Creature').
card_original_text('regeneration'/'LEA', '{G} Target creature regenerates.').
card_first_print('regeneration', 'LEA').
card_image_name('regeneration'/'LEA', 'regeneration').
card_uid('regeneration'/'LEA', 'LEA:Regeneration:regeneration').
card_rarity('regeneration'/'LEA', 'Common').
card_artist('regeneration'/'LEA', 'Quinton Hoover').
card_multiverse_id('regeneration'/'LEA', '169').

card_in_set('regrowth', 'LEA').
card_original_type('regrowth'/'LEA', 'Sorcery').
card_original_text('regrowth'/'LEA', 'Return any card from your graveyard to your hand.').
card_first_print('regrowth', 'LEA').
card_image_name('regrowth'/'LEA', 'regrowth').
card_uid('regrowth'/'LEA', 'LEA:Regrowth:regrowth').
card_rarity('regrowth'/'LEA', 'Uncommon').
card_artist('regrowth'/'LEA', 'Dameon Willich').
card_multiverse_id('regrowth'/'LEA', '170').

card_in_set('resurrection', 'LEA').
card_original_type('resurrection'/'LEA', 'Sorcery').
card_original_text('resurrection'/'LEA', 'Take a creature from your graveyard and put it directly into play. You can\'t tap it until your next turn.').
card_first_print('resurrection', 'LEA').
card_image_name('resurrection'/'LEA', 'resurrection').
card_uid('resurrection'/'LEA', 'LEA:Resurrection:resurrection').
card_rarity('resurrection'/'LEA', 'Uncommon').
card_artist('resurrection'/'LEA', 'Dan Frazier').
card_multiverse_id('resurrection'/'LEA', '265').

card_in_set('reverse damage', 'LEA').
card_original_type('reverse damage'/'LEA', 'Instant').
card_original_text('reverse damage'/'LEA', 'All damage you have taken from any one source this turn is added to your life total instead of subtracted from it.').
card_first_print('reverse damage', 'LEA').
card_image_name('reverse damage'/'LEA', 'reverse damage').
card_uid('reverse damage'/'LEA', 'LEA:Reverse Damage:reverse damage').
card_rarity('reverse damage'/'LEA', 'Rare').
card_artist('reverse damage'/'LEA', 'Dameon Willich').
card_multiverse_id('reverse damage'/'LEA', '266').

card_in_set('righteousness', 'LEA').
card_original_type('righteousness'/'LEA', 'Instant').
card_original_text('righteousness'/'LEA', 'Target defending creature gains +7/+7 until end of turn.').
card_first_print('righteousness', 'LEA').
card_image_name('righteousness'/'LEA', 'righteousness').
card_uid('righteousness'/'LEA', 'LEA:Righteousness:righteousness').
card_rarity('righteousness'/'LEA', 'Rare').
card_artist('righteousness'/'LEA', 'Douglas Shuler').
card_multiverse_id('righteousness'/'LEA', '267').

card_in_set('roc of kher ridges', 'LEA').
card_original_type('roc of kher ridges'/'LEA', 'Summon — Roc').
card_original_text('roc of kher ridges'/'LEA', 'Flying').
card_first_print('roc of kher ridges', 'LEA').
card_image_name('roc of kher ridges'/'LEA', 'roc of kher ridges').
card_uid('roc of kher ridges'/'LEA', 'LEA:Roc of Kher Ridges:roc of kher ridges').
card_rarity('roc of kher ridges'/'LEA', 'Rare').
card_artist('roc of kher ridges'/'LEA', 'Andi Rusu').
card_flavor_text('roc of kher ridges'/'LEA', 'We encountered a valley topped with immense boulders and eerie rock formations. Suddenly one of these boulders toppled from its perch and sprouted gargantuan wings, casting a shadow of darkness and sending us fleeing in terror.').
card_multiverse_id('roc of kher ridges'/'LEA', '218').

card_in_set('rock hydra', 'LEA').
card_original_type('rock hydra'/'LEA', 'Summon — Hydra').
card_original_text('rock hydra'/'LEA', 'Put X +1/+1 counters (heads) on Hydra. Each point of damage Hydra suffers destroys one head unless R is spent. During upkeep, new heads may be grown for {R}{R}{R} apiece.').
card_first_print('rock hydra', 'LEA').
card_image_name('rock hydra'/'LEA', 'rock hydra').
card_uid('rock hydra'/'LEA', 'LEA:Rock Hydra:rock hydra').
card_rarity('rock hydra'/'LEA', 'Rare').
card_artist('rock hydra'/'LEA', 'Jeff A. Menges').
card_multiverse_id('rock hydra'/'LEA', '219').

card_in_set('rod of ruin', 'LEA').
card_original_type('rod of ruin'/'LEA', 'Mono Artifact').
card_original_text('rod of ruin'/'LEA', '{3}: Rod of Ruin does 1 damage to any target.').
card_first_print('rod of ruin', 'LEA').
card_image_name('rod of ruin'/'LEA', 'rod of ruin').
card_uid('rod of ruin'/'LEA', 'LEA:Rod of Ruin:rod of ruin').
card_rarity('rod of ruin'/'LEA', 'Uncommon').
card_artist('rod of ruin'/'LEA', 'Christopher Rush').
card_multiverse_id('rod of ruin'/'LEA', '39').

card_in_set('royal assassin', 'LEA').
card_original_type('royal assassin'/'LEA', 'Summon — Assassin').
card_original_text('royal assassin'/'LEA', 'Tap to destroy any tapped creature.').
card_first_print('royal assassin', 'LEA').
card_image_name('royal assassin'/'LEA', 'royal assassin').
card_uid('royal assassin'/'LEA', 'LEA:Royal Assassin:royal assassin').
card_rarity('royal assassin'/'LEA', 'Rare').
card_artist('royal assassin'/'LEA', 'Tom Wänerstrand').
card_flavor_text('royal assassin'/'LEA', 'Trained in the arts of stealth, the royal assassins choose their victims carefully, relying on timing and precision rather than brute force.').
card_multiverse_id('royal assassin'/'LEA', '79').

card_in_set('sacrifice', 'LEA').
card_original_type('sacrifice'/'LEA', 'Interrupt').
card_original_text('sacrifice'/'LEA', 'Destroy one of your creatures without regenerating it, and add to your mana pool a number of black mana equal to black creature\'s casting cost.').
card_first_print('sacrifice', 'LEA').
card_image_name('sacrifice'/'LEA', 'sacrifice').
card_uid('sacrifice'/'LEA', 'LEA:Sacrifice:sacrifice').
card_rarity('sacrifice'/'LEA', 'Uncommon').
card_artist('sacrifice'/'LEA', 'Dan Frazier').
card_multiverse_id('sacrifice'/'LEA', '80').

card_in_set('samite healer', 'LEA').
card_original_type('samite healer'/'LEA', 'Summon — Cleric').
card_original_text('samite healer'/'LEA', 'Tap to prevent 1 damage to any target.').
card_first_print('samite healer', 'LEA').
card_image_name('samite healer'/'LEA', 'samite healer').
card_uid('samite healer'/'LEA', 'LEA:Samite Healer:samite healer').
card_rarity('samite healer'/'LEA', 'Common').
card_artist('samite healer'/'LEA', 'Tom Wänerstrand').
card_flavor_text('samite healer'/'LEA', 'Healers ultimately acquire the divine gifts of spiritual and physical wholeness. The most devout are also granted the ability to pass physical wholeness on to others.').
card_multiverse_id('samite healer'/'LEA', '268').

card_in_set('savannah', 'LEA').
card_original_type('savannah'/'LEA', 'Land').
card_original_text('savannah'/'LEA', 'Counts as both plains and forest and is affected by spells that affect either. Tap to add either {W} or {G} to your mana pool.').
card_first_print('savannah', 'LEA').
card_image_name('savannah'/'LEA', 'savannah').
card_uid('savannah'/'LEA', 'LEA:Savannah:savannah').
card_rarity('savannah'/'LEA', 'Rare').
card_artist('savannah'/'LEA', 'Rob Alexander').
card_multiverse_id('savannah'/'LEA', '282').

card_in_set('savannah lions', 'LEA').
card_original_type('savannah lions'/'LEA', 'Summon — Lions').
card_original_text('savannah lions'/'LEA', '').
card_first_print('savannah lions', 'LEA').
card_image_name('savannah lions'/'LEA', 'savannah lions').
card_uid('savannah lions'/'LEA', 'LEA:Savannah Lions:savannah lions').
card_rarity('savannah lions'/'LEA', 'Rare').
card_artist('savannah lions'/'LEA', 'Daniel Gelon').
card_flavor_text('savannah lions'/'LEA', 'The traditional kings of the jungle command a healthy respect in other climates as well. Relying mainly on their stealth and speed, Savannah Lions can take a victim by surprise, even in the endless flat plains of their homeland.').
card_multiverse_id('savannah lions'/'LEA', '269').

card_in_set('scathe zombies', 'LEA').
card_original_type('scathe zombies'/'LEA', 'Summon — Zombies').
card_original_text('scathe zombies'/'LEA', '').
card_first_print('scathe zombies', 'LEA').
card_image_name('scathe zombies'/'LEA', 'scathe zombies').
card_uid('scathe zombies'/'LEA', 'LEA:Scathe Zombies:scathe zombies').
card_rarity('scathe zombies'/'LEA', 'Common').
card_artist('scathe zombies'/'LEA', 'Jesper Myrfors').
card_flavor_text('scathe zombies'/'LEA', '\"They groaned, they stirred, they all uprose,/ Nor spake, nor moved their eyes;/ It had been strange, even in a dream,/ To have seen those dead men rise.\"/ —Samuel Coleridge, \"The Rime of the Ancient Mariner\"').
card_multiverse_id('scathe zombies'/'LEA', '81').

card_in_set('scavenging ghoul', 'LEA').
card_original_type('scavenging ghoul'/'LEA', 'Summon — Ghoul').
card_original_text('scavenging ghoul'/'LEA', 'At the end of each turn, put one counter on the Ghoul for each other creature that was destroyed without regenerating during the turn. If Ghoul dies, you may use a counter to regenerate it; counters remain until used.').
card_first_print('scavenging ghoul', 'LEA').
card_image_name('scavenging ghoul'/'LEA', 'scavenging ghoul').
card_uid('scavenging ghoul'/'LEA', 'LEA:Scavenging Ghoul:scavenging ghoul').
card_rarity('scavenging ghoul'/'LEA', 'Uncommon').
card_artist('scavenging ghoul'/'LEA', 'Jeff A. Menges').
card_multiverse_id('scavenging ghoul'/'LEA', '82').

card_in_set('scrubland', 'LEA').
card_original_type('scrubland'/'LEA', 'Land').
card_original_text('scrubland'/'LEA', 'Counts as both plains and swamp and is affected by spells that affect either. Tap to add either {W} or {B} to your mana pool.').
card_first_print('scrubland', 'LEA').
card_image_name('scrubland'/'LEA', 'scrubland').
card_uid('scrubland'/'LEA', 'LEA:Scrubland:scrubland').
card_rarity('scrubland'/'LEA', 'Rare').
card_artist('scrubland'/'LEA', 'Jesper Myrfors').
card_multiverse_id('scrubland'/'LEA', '283').

card_in_set('scryb sprites', 'LEA').
card_original_type('scryb sprites'/'LEA', 'Summon — Faeries').
card_original_text('scryb sprites'/'LEA', 'Flying').
card_first_print('scryb sprites', 'LEA').
card_image_name('scryb sprites'/'LEA', 'scryb sprites').
card_uid('scryb sprites'/'LEA', 'LEA:Scryb Sprites:scryb sprites').
card_rarity('scryb sprites'/'LEA', 'Common').
card_artist('scryb sprites'/'LEA', 'Amy Weber').
card_flavor_text('scryb sprites'/'LEA', 'The only sound was the gentle clicking of the Faeries\' wings. Then those intruders who were still standing turned and fled. One thing was certain: they didn\'t think the Scryb were very funny anymore.').
card_multiverse_id('scryb sprites'/'LEA', '171').

card_in_set('sea serpent', 'LEA').
card_original_type('sea serpent'/'LEA', 'Summon — Serpent').
card_original_text('sea serpent'/'LEA', 'Serpent cannot attack unless opponent has islands in play.\nSerpent is destroyed immediately if at any time controller has no islands in play.').
card_first_print('sea serpent', 'LEA').
card_image_name('sea serpent'/'LEA', 'sea serpent').
card_uid('sea serpent'/'LEA', 'LEA:Sea Serpent:sea serpent').
card_rarity('sea serpent'/'LEA', 'Common').
card_artist('sea serpent'/'LEA', 'Jeff A. Menges').
card_flavor_text('sea serpent'/'LEA', 'Legend has it that Serpents used to be bigger, but how could that be?').
card_multiverse_id('sea serpent'/'LEA', '124').

card_in_set('sedge troll', 'LEA').
card_original_type('sedge troll'/'LEA', 'Summon — Troll').
card_original_text('sedge troll'/'LEA', 'Troll gains +1/+1 if controller has any swamps in play. {B} Regenerates').
card_first_print('sedge troll', 'LEA').
card_image_name('sedge troll'/'LEA', 'sedge troll').
card_uid('sedge troll'/'LEA', 'LEA:Sedge Troll:sedge troll').
card_rarity('sedge troll'/'LEA', 'Rare').
card_artist('sedge troll'/'LEA', 'Dan Frazier').
card_flavor_text('sedge troll'/'LEA', 'The stench in the hovel was overpowering; something loathsome was cooking. Occasionally something surfaced in the thick paste, but my host would casually push it down before I could make out what it was.').
card_multiverse_id('sedge troll'/'LEA', '220').

card_in_set('sengir vampire', 'LEA').
card_original_type('sengir vampire'/'LEA', 'Summon — Vampire').
card_original_text('sengir vampire'/'LEA', 'Flying\nVampire gets a +1/+1 counter each time a creature dies during a turn in which Vampire damaged it, unless the dead creature is regenerated.').
card_first_print('sengir vampire', 'LEA').
card_image_name('sengir vampire'/'LEA', 'sengir vampire').
card_uid('sengir vampire'/'LEA', 'LEA:Sengir Vampire:sengir vampire').
card_rarity('sengir vampire'/'LEA', 'Uncommon').
card_artist('sengir vampire'/'LEA', 'Anson Maddocks').
card_multiverse_id('sengir vampire'/'LEA', '83').

card_in_set('serra angel', 'LEA').
card_original_type('serra angel'/'LEA', 'Summon — Angel').
card_original_text('serra angel'/'LEA', 'Flying\nDoes not tap when attacking.').
card_first_print('serra angel', 'LEA').
card_image_name('serra angel'/'LEA', 'serra angel').
card_uid('serra angel'/'LEA', 'LEA:Serra Angel:serra angel').
card_rarity('serra angel'/'LEA', 'Uncommon').
card_artist('serra angel'/'LEA', 'Douglas Shuler').
card_flavor_text('serra angel'/'LEA', 'Born with wings of light and a sword of faith, this heavenly incarnation embodies both fury and purity.').
card_multiverse_id('serra angel'/'LEA', '270').

card_in_set('shanodin dryads', 'LEA').
card_original_type('shanodin dryads'/'LEA', 'Summon — Nymphs').
card_original_text('shanodin dryads'/'LEA', 'Forestwalk').
card_first_print('shanodin dryads', 'LEA').
card_image_name('shanodin dryads'/'LEA', 'shanodin dryads').
card_uid('shanodin dryads'/'LEA', 'LEA:Shanodin Dryads:shanodin dryads').
card_rarity('shanodin dryads'/'LEA', 'Common').
card_artist('shanodin dryads'/'LEA', 'Anson Maddocks').
card_flavor_text('shanodin dryads'/'LEA', 'Moving without sound, swift figures pass through branches and undergrowth completely unhindered. One with the trees around them, the Dryads of Shanodin Forest are seen only when they wish to be.').
card_multiverse_id('shanodin dryads'/'LEA', '172').

card_in_set('shatter', 'LEA').
card_original_type('shatter'/'LEA', 'Instant').
card_original_text('shatter'/'LEA', 'Shatter destroys target artifact.').
card_first_print('shatter', 'LEA').
card_image_name('shatter'/'LEA', 'shatter').
card_uid('shatter'/'LEA', 'LEA:Shatter:shatter').
card_rarity('shatter'/'LEA', 'Common').
card_artist('shatter'/'LEA', 'Amy Weber').
card_multiverse_id('shatter'/'LEA', '221').

card_in_set('shivan dragon', 'LEA').
card_original_type('shivan dragon'/'LEA', 'Summon — Dragon').
card_original_text('shivan dragon'/'LEA', 'Flying, {R} +1/+0 until end of turn.').
card_first_print('shivan dragon', 'LEA').
card_image_name('shivan dragon'/'LEA', 'shivan dragon').
card_uid('shivan dragon'/'LEA', 'LEA:Shivan Dragon:shivan dragon').
card_rarity('shivan dragon'/'LEA', 'Rare').
card_artist('shivan dragon'/'LEA', 'Melissa A. Benson').
card_flavor_text('shivan dragon'/'LEA', 'While it\'s true most Dragons are cruel, the Shivan Dragon seems to take particular glee in the misery of others, often tormenting its victims much like a cat plays with a mouse before delivering the final blow.').
card_multiverse_id('shivan dragon'/'LEA', '222').

card_in_set('simulacrum', 'LEA').
card_original_type('simulacrum'/'LEA', 'Instant').
card_original_text('simulacrum'/'LEA', 'All damage done to you so far this turn is instead retroactively applied to one of your creatures in play. If this damage kills the creature it can be regenerated; even if there\'s more than enough damage to kill the creature, you don\'t suffer any of it. Further damage this turn is treated normally.').
card_first_print('simulacrum', 'LEA').
card_image_name('simulacrum'/'LEA', 'simulacrum').
card_uid('simulacrum'/'LEA', 'LEA:Simulacrum:simulacrum').
card_rarity('simulacrum'/'LEA', 'Uncommon').
card_artist('simulacrum'/'LEA', 'Mark Poole').
card_multiverse_id('simulacrum'/'LEA', '84').

card_in_set('sinkhole', 'LEA').
card_original_type('sinkhole'/'LEA', 'Sorcery').
card_original_text('sinkhole'/'LEA', 'Destroys any one land.').
card_first_print('sinkhole', 'LEA').
card_image_name('sinkhole'/'LEA', 'sinkhole').
card_uid('sinkhole'/'LEA', 'LEA:Sinkhole:sinkhole').
card_rarity('sinkhole'/'LEA', 'Common').
card_artist('sinkhole'/'LEA', 'Sandra Everingham').
card_multiverse_id('sinkhole'/'LEA', '85').

card_in_set('siren\'s call', 'LEA').
card_original_type('siren\'s call'/'LEA', 'Instant').
card_original_text('siren\'s call'/'LEA', 'All of opponent\'s creatures that can attack must do so. Any non-wall creatures that cannot attack are destroyed at end of turn. Play only during opponent\'s turn, before opponent\'s attack. Creatures summoned this turn are unaffected by Siren\'s Call.').
card_first_print('siren\'s call', 'LEA').
card_image_name('siren\'s call'/'LEA', 'siren\'s call').
card_uid('siren\'s call'/'LEA', 'LEA:Siren\'s Call:siren\'s call').
card_rarity('siren\'s call'/'LEA', 'Uncommon').
card_artist('siren\'s call'/'LEA', 'Anson Maddocks').
card_multiverse_id('siren\'s call'/'LEA', '125').

card_in_set('sleight of mind', 'LEA').
card_original_type('sleight of mind'/'LEA', 'Interrupt').
card_original_text('sleight of mind'/'LEA', 'Change the text of any card being played or already in play by replacing one color word with another. For example, you can change \"Counters red spells\" to \"Counters black spells.\" Cannot change mana symbols.').
card_first_print('sleight of mind', 'LEA').
card_image_name('sleight of mind'/'LEA', 'sleight of mind').
card_uid('sleight of mind'/'LEA', 'LEA:Sleight of Mind:sleight of mind').
card_rarity('sleight of mind'/'LEA', 'Rare').
card_artist('sleight of mind'/'LEA', 'Mark Poole').
card_multiverse_id('sleight of mind'/'LEA', '126').

card_in_set('smoke', 'LEA').
card_original_type('smoke'/'LEA', 'Enchantment').
card_original_text('smoke'/'LEA', 'Each player can only untap one creature during his or her untap phase.').
card_first_print('smoke', 'LEA').
card_image_name('smoke'/'LEA', 'smoke').
card_uid('smoke'/'LEA', 'LEA:Smoke:smoke').
card_rarity('smoke'/'LEA', 'Rare').
card_artist('smoke'/'LEA', 'Jesper Myrfors').
card_multiverse_id('smoke'/'LEA', '223').

card_in_set('sol ring', 'LEA').
card_original_type('sol ring'/'LEA', 'Mono Artifact').
card_original_text('sol ring'/'LEA', 'Add 2 colorless mana to your mana pool. Tapping this artifact can be played as an interrupt.').
card_first_print('sol ring', 'LEA').
card_image_name('sol ring'/'LEA', 'sol ring').
card_uid('sol ring'/'LEA', 'LEA:Sol Ring:sol ring').
card_rarity('sol ring'/'LEA', 'Uncommon').
card_artist('sol ring'/'LEA', 'Mark Tedin').
card_multiverse_id('sol ring'/'LEA', '40').

card_in_set('soul net', 'LEA').
card_original_type('soul net'/'LEA', 'Poly Artifact').
card_original_text('soul net'/'LEA', '{1}: You gain 1 life every time a creature is destroyed, unless it is then regenerated.').
card_first_print('soul net', 'LEA').
card_image_name('soul net'/'LEA', 'soul net').
card_uid('soul net'/'LEA', 'LEA:Soul Net:soul net').
card_rarity('soul net'/'LEA', 'Uncommon').
card_artist('soul net'/'LEA', 'Dameon Willich').
card_multiverse_id('soul net'/'LEA', '41').

card_in_set('spell blast', 'LEA').
card_original_type('spell blast'/'LEA', 'Interrupt').
card_original_text('spell blast'/'LEA', 'Target spell is countered; X is cost of target spell.').
card_first_print('spell blast', 'LEA').
card_image_name('spell blast'/'LEA', 'spell blast').
card_uid('spell blast'/'LEA', 'LEA:Spell Blast:spell blast').
card_rarity('spell blast'/'LEA', 'Common').
card_artist('spell blast'/'LEA', 'Brian Snõddy').
card_multiverse_id('spell blast'/'LEA', '127').

card_in_set('stasis', 'LEA').
card_original_type('stasis'/'LEA', 'Enchantment').
card_original_text('stasis'/'LEA', 'Players do not get an untap phase. Pay {U} during your upkeep or Stasis is destroyed.').
card_first_print('stasis', 'LEA').
card_image_name('stasis'/'LEA', 'stasis').
card_uid('stasis'/'LEA', 'LEA:Stasis:stasis').
card_rarity('stasis'/'LEA', 'Rare').
card_artist('stasis'/'LEA', 'Fay Jones').
card_multiverse_id('stasis'/'LEA', '128').

card_in_set('steal artifact', 'LEA').
card_original_type('steal artifact'/'LEA', 'Enchant Artifact').
card_original_text('steal artifact'/'LEA', 'You control target artifact until enchantment is discarded or game ends. If target artifact was tapped when stolen, it stays tapped until you can untap it. If destroyed, target artifact is put in its owner\'s graveyard.').
card_first_print('steal artifact', 'LEA').
card_image_name('steal artifact'/'LEA', 'steal artifact').
card_uid('steal artifact'/'LEA', 'LEA:Steal Artifact:steal artifact').
card_rarity('steal artifact'/'LEA', 'Uncommon').
card_artist('steal artifact'/'LEA', 'Amy Weber').
card_multiverse_id('steal artifact'/'LEA', '129').

card_in_set('stone giant', 'LEA').
card_original_type('stone giant'/'LEA', 'Summon — Giant').
card_original_text('stone giant'/'LEA', 'Tap to make one of your own creatures a flying creature until end of turn. Target creature, which must have toughness less than Stone Giant\'s power, is destroyed at end of turn.').
card_first_print('stone giant', 'LEA').
card_image_name('stone giant'/'LEA', 'stone giant').
card_uid('stone giant'/'LEA', 'LEA:Stone Giant:stone giant').
card_rarity('stone giant'/'LEA', 'Uncommon').
card_artist('stone giant'/'LEA', 'Dameon Willich').
card_flavor_text('stone giant'/'LEA', 'What goes up, must come down.').
card_multiverse_id('stone giant'/'LEA', '224').

card_in_set('stone rain', 'LEA').
card_original_type('stone rain'/'LEA', 'Sorcery').
card_original_text('stone rain'/'LEA', 'Destroys any one land.').
card_first_print('stone rain', 'LEA').
card_image_name('stone rain'/'LEA', 'stone rain').
card_uid('stone rain'/'LEA', 'LEA:Stone Rain:stone rain').
card_rarity('stone rain'/'LEA', 'Common').
card_artist('stone rain'/'LEA', 'Daniel Gelon').
card_multiverse_id('stone rain'/'LEA', '225').

card_in_set('stream of life', 'LEA').
card_original_type('stream of life'/'LEA', 'Sorcery').
card_original_text('stream of life'/'LEA', 'Target player gains X life.').
card_first_print('stream of life', 'LEA').
card_image_name('stream of life'/'LEA', 'stream of life').
card_uid('stream of life'/'LEA', 'LEA:Stream of Life:stream of life').
card_rarity('stream of life'/'LEA', 'Common').
card_artist('stream of life'/'LEA', 'Mark Poole').
card_multiverse_id('stream of life'/'LEA', '173').

card_in_set('sunglasses of urza', 'LEA').
card_original_type('sunglasses of urza'/'LEA', 'Continuous Artifact').
card_original_text('sunglasses of urza'/'LEA', 'White mana in your mana pool can be used as either white or red mana.').
card_first_print('sunglasses of urza', 'LEA').
card_image_name('sunglasses of urza'/'LEA', 'sunglasses of urza').
card_uid('sunglasses of urza'/'LEA', 'LEA:Sunglasses of Urza:sunglasses of urza').
card_rarity('sunglasses of urza'/'LEA', 'Rare').
card_artist('sunglasses of urza'/'LEA', 'Dan Frazier').
card_multiverse_id('sunglasses of urza'/'LEA', '42').

card_in_set('swamp', 'LEA').
card_original_type('swamp'/'LEA', 'Land').
card_original_text('swamp'/'LEA', 'Tap to add {B} to your mana pool.').
card_first_print('swamp', 'LEA').
card_image_name('swamp'/'LEA', 'swamp1').
card_uid('swamp'/'LEA', 'LEA:Swamp:swamp1').
card_rarity('swamp'/'LEA', 'Basic Land').
card_artist('swamp'/'LEA', 'Dan Frazier').
card_multiverse_id('swamp'/'LEA', '277').

card_in_set('swamp', 'LEA').
card_original_type('swamp'/'LEA', 'Land').
card_original_text('swamp'/'LEA', 'Tap to add {B} to your mana pool.').
card_image_name('swamp'/'LEA', 'swamp2').
card_uid('swamp'/'LEA', 'LEA:Swamp:swamp2').
card_rarity('swamp'/'LEA', 'Basic Land').
card_artist('swamp'/'LEA', 'Dan Frazier').
card_multiverse_id('swamp'/'LEA', '278').

card_in_set('swords to plowshares', 'LEA').
card_original_type('swords to plowshares'/'LEA', 'Instant').
card_original_text('swords to plowshares'/'LEA', 'Target creature is removed from game entirely; return to owner\'s deck only when game is over. Creature\'s controller gains life points equal to creature\'s power.').
card_first_print('swords to plowshares', 'LEA').
card_image_name('swords to plowshares'/'LEA', 'swords to plowshares').
card_uid('swords to plowshares'/'LEA', 'LEA:Swords to Plowshares:swords to plowshares').
card_rarity('swords to plowshares'/'LEA', 'Uncommon').
card_artist('swords to plowshares'/'LEA', 'Jeff A. Menges').
card_multiverse_id('swords to plowshares'/'LEA', '271').

card_in_set('taiga', 'LEA').
card_original_type('taiga'/'LEA', 'Land').
card_original_text('taiga'/'LEA', 'Counts as both forest and mountains and is affected by spells that affect either. Tap to add either {G} or {R} to your mana pool.').
card_first_print('taiga', 'LEA').
card_image_name('taiga'/'LEA', 'taiga').
card_uid('taiga'/'LEA', 'LEA:Taiga:taiga').
card_rarity('taiga'/'LEA', 'Rare').
card_artist('taiga'/'LEA', 'Rob Alexander').
card_multiverse_id('taiga'/'LEA', '284').

card_in_set('terror', 'LEA').
card_original_type('terror'/'LEA', 'Instant').
card_original_text('terror'/'LEA', 'Destroys target creature without possibility of regeneration. Does not affect black creatures and artifact creatures.').
card_first_print('terror', 'LEA').
card_image_name('terror'/'LEA', 'terror').
card_uid('terror'/'LEA', 'LEA:Terror:terror').
card_rarity('terror'/'LEA', 'Common').
card_artist('terror'/'LEA', 'Ron Spencer').
card_multiverse_id('terror'/'LEA', '86').

card_in_set('the hive', 'LEA').
card_original_type('the hive'/'LEA', 'Mono Artifact').
card_original_text('the hive'/'LEA', '{5}: Creates one Giant Wasp, a 1/1 flying creature. Represent Wasps with tokens, making sure to indicate when each Wasp is tapped. Wasps can\'t attack during the turn created. Treat Wasps like artifact creatures in every way, except that they are removed from the game entirely if they ever leave play. If the Hive is destroyed, the Wasps must still be killed individually.').
card_first_print('the hive', 'LEA').
card_image_name('the hive'/'LEA', 'the hive').
card_uid('the hive'/'LEA', 'LEA:The Hive:the hive').
card_rarity('the hive'/'LEA', 'Rare').
card_artist('the hive'/'LEA', 'Sandra Everingham').
card_multiverse_id('the hive'/'LEA', '43').

card_in_set('thicket basilisk', 'LEA').
card_original_type('thicket basilisk'/'LEA', 'Summon — Basilisk').
card_original_text('thicket basilisk'/'LEA', 'Any non-wall creature blocking Basilisk is destroyed, as is any creature blocked by Basilisk. Creatures destroyed this way deal their damage before dying.').
card_first_print('thicket basilisk', 'LEA').
card_image_name('thicket basilisk'/'LEA', 'thicket basilisk').
card_uid('thicket basilisk'/'LEA', 'LEA:Thicket Basilisk:thicket basilisk').
card_rarity('thicket basilisk'/'LEA', 'Uncommon').
card_artist('thicket basilisk'/'LEA', 'Dan Frazier').
card_flavor_text('thicket basilisk'/'LEA', 'Moss-covered statues littered the area, a macabre monument to the Basilisk\'s power.').
card_multiverse_id('thicket basilisk'/'LEA', '174').

card_in_set('thoughtlace', 'LEA').
card_original_type('thoughtlace'/'LEA', 'Interrupt').
card_original_text('thoughtlace'/'LEA', 'Changes the color of one card either being played or already in play to blue. Cost to cast, tap, maintain, or use a special ability of target card remains entirely unchanged.').
card_first_print('thoughtlace', 'LEA').
card_image_name('thoughtlace'/'LEA', 'thoughtlace').
card_uid('thoughtlace'/'LEA', 'LEA:Thoughtlace:thoughtlace').
card_rarity('thoughtlace'/'LEA', 'Rare').
card_artist('thoughtlace'/'LEA', 'Mark Poole').
card_multiverse_id('thoughtlace'/'LEA', '130').

card_in_set('throne of bone', 'LEA').
card_original_type('throne of bone'/'LEA', 'Poly Artifact').
card_original_text('throne of bone'/'LEA', '{1}: Any black spell cast by any player gives you 1 life.').
card_first_print('throne of bone', 'LEA').
card_image_name('throne of bone'/'LEA', 'throne of bone').
card_uid('throne of bone'/'LEA', 'LEA:Throne of Bone:throne of bone').
card_rarity('throne of bone'/'LEA', 'Uncommon').
card_artist('throne of bone'/'LEA', 'Anson Maddocks').
card_multiverse_id('throne of bone'/'LEA', '44').

card_in_set('timber wolves', 'LEA').
card_original_type('timber wolves'/'LEA', 'Summon — Wolves').
card_original_text('timber wolves'/'LEA', 'Bands').
card_first_print('timber wolves', 'LEA').
card_image_name('timber wolves'/'LEA', 'timber wolves').
card_uid('timber wolves'/'LEA', 'LEA:Timber Wolves:timber wolves').
card_rarity('timber wolves'/'LEA', 'Rare').
card_artist('timber wolves'/'LEA', 'Melissa A. Benson').
card_flavor_text('timber wolves'/'LEA', 'Though many think of Wolves as solitary predators, they are actually extremely social animals. During a hunt they often call to each other, which can be quite unsettling for their prey.').
card_multiverse_id('timber wolves'/'LEA', '175').

card_in_set('time vault', 'LEA').
card_original_type('time vault'/'LEA', 'Mono Artifact').
card_original_text('time vault'/'LEA', 'Tap to gain an additional turn after the current one. Time Vault doesn\'t untap normally during untap phase; to untap it, you must skip a turn. Time Vault begins tapped.').
card_first_print('time vault', 'LEA').
card_image_name('time vault'/'LEA', 'time vault').
card_uid('time vault'/'LEA', 'LEA:Time Vault:time vault').
card_rarity('time vault'/'LEA', 'Rare').
card_artist('time vault'/'LEA', 'Mark Tedin').
card_multiverse_id('time vault'/'LEA', '45').

card_in_set('time walk', 'LEA').
card_original_type('time walk'/'LEA', 'Sorcery').
card_original_text('time walk'/'LEA', 'Take an extra turn after this one.').
card_first_print('time walk', 'LEA').
card_image_name('time walk'/'LEA', 'time walk').
card_uid('time walk'/'LEA', 'LEA:Time Walk:time walk').
card_rarity('time walk'/'LEA', 'Rare').
card_artist('time walk'/'LEA', 'Amy Weber').
card_multiverse_id('time walk'/'LEA', '131').

card_in_set('timetwister', 'LEA').
card_original_type('timetwister'/'LEA', 'Sorcery').
card_original_text('timetwister'/'LEA', 'Set Timetwister aside in a new graveyard pile. Shuffle your hand, library, and graveyard together into a new library and draw a new hand of seven cards, leaving all cards in play where they are; opponent must do the same.').
card_first_print('timetwister', 'LEA').
card_image_name('timetwister'/'LEA', 'timetwister').
card_uid('timetwister'/'LEA', 'LEA:Timetwister:timetwister').
card_rarity('timetwister'/'LEA', 'Rare').
card_artist('timetwister'/'LEA', 'Mark Tedin').
card_multiverse_id('timetwister'/'LEA', '132').

card_in_set('tranquility', 'LEA').
card_original_type('tranquility'/'LEA', 'Sorcery').
card_original_text('tranquility'/'LEA', 'All enchantments in play must be discarded.').
card_first_print('tranquility', 'LEA').
card_image_name('tranquility'/'LEA', 'tranquility').
card_uid('tranquility'/'LEA', 'LEA:Tranquility:tranquility').
card_rarity('tranquility'/'LEA', 'Common').
card_artist('tranquility'/'LEA', 'Douglas Shuler').
card_multiverse_id('tranquility'/'LEA', '176').

card_in_set('tropical island', 'LEA').
card_original_type('tropical island'/'LEA', 'Land').
card_original_text('tropical island'/'LEA', 'Counts as both forest and islands and is affected by spells that affect either. Tap to add either {G} or {U} to your mana pool.').
card_first_print('tropical island', 'LEA').
card_image_name('tropical island'/'LEA', 'tropical island').
card_uid('tropical island'/'LEA', 'LEA:Tropical Island:tropical island').
card_rarity('tropical island'/'LEA', 'Rare').
card_artist('tropical island'/'LEA', 'Jesper Myrfors').
card_multiverse_id('tropical island'/'LEA', '285').

card_in_set('tsunami', 'LEA').
card_original_type('tsunami'/'LEA', 'Sorcery').
card_original_text('tsunami'/'LEA', 'All islands in play are destroyed.').
card_first_print('tsunami', 'LEA').
card_image_name('tsunami'/'LEA', 'tsunami').
card_uid('tsunami'/'LEA', 'LEA:Tsunami:tsunami').
card_rarity('tsunami'/'LEA', 'Uncommon').
card_artist('tsunami'/'LEA', 'Richard Thomas').
card_multiverse_id('tsunami'/'LEA', '177').

card_in_set('tundra', 'LEA').
card_original_type('tundra'/'LEA', 'Land').
card_original_text('tundra'/'LEA', 'Counts as both islands and plains and is affected by spells that affect either. Tap to add either {U} or {W} to your mana pool.').
card_first_print('tundra', 'LEA').
card_image_name('tundra'/'LEA', 'tundra').
card_uid('tundra'/'LEA', 'LEA:Tundra:tundra').
card_rarity('tundra'/'LEA', 'Rare').
card_artist('tundra'/'LEA', 'Jesper Myrfors').
card_multiverse_id('tundra'/'LEA', '286').

card_in_set('tunnel', 'LEA').
card_original_type('tunnel'/'LEA', 'Instant').
card_original_text('tunnel'/'LEA', 'Destroys 1 wall. Target wall cannot be regenerated.').
card_first_print('tunnel', 'LEA').
card_image_name('tunnel'/'LEA', 'tunnel').
card_uid('tunnel'/'LEA', 'LEA:Tunnel:tunnel').
card_rarity('tunnel'/'LEA', 'Uncommon').
card_artist('tunnel'/'LEA', 'Dan Frazier').
card_multiverse_id('tunnel'/'LEA', '226').

card_in_set('twiddle', 'LEA').
card_original_type('twiddle'/'LEA', 'Instant').
card_original_text('twiddle'/'LEA', 'Caster may tap or untap any one land, creature, or artifact in play.').
card_first_print('twiddle', 'LEA').
card_image_name('twiddle'/'LEA', 'twiddle').
card_uid('twiddle'/'LEA', 'LEA:Twiddle:twiddle').
card_rarity('twiddle'/'LEA', 'Common').
card_artist('twiddle'/'LEA', 'Rob Alexander').
card_multiverse_id('twiddle'/'LEA', '133').

card_in_set('two-headed giant of foriys', 'LEA').
card_original_type('two-headed giant of foriys'/'LEA', 'Summon — Giant').
card_original_text('two-headed giant of foriys'/'LEA', 'Trample\nMay block two attacking creatures; divide damage between them however controller likes.').
card_first_print('two-headed giant of foriys', 'LEA').
card_image_name('two-headed giant of foriys'/'LEA', 'two-headed giant of foriys').
card_uid('two-headed giant of foriys'/'LEA', 'LEA:Two-Headed Giant of Foriys:two-headed giant of foriys').
card_rarity('two-headed giant of foriys'/'LEA', 'Rare').
card_artist('two-headed giant of foriys'/'LEA', 'Anson Maddocks').
card_flavor_text('two-headed giant of foriys'/'LEA', 'None know if this Giant is the result of aberrant magics, Siamese twins, or a mentalist\'s schizophrenia.').
card_multiverse_id('two-headed giant of foriys'/'LEA', '227').

card_in_set('underground sea', 'LEA').
card_original_type('underground sea'/'LEA', 'Land').
card_original_text('underground sea'/'LEA', 'Counts as both swamp and islands and is affected by spells that affect either. Tap to add either {B} or {U} to your mana pool.').
card_first_print('underground sea', 'LEA').
card_image_name('underground sea'/'LEA', 'underground sea').
card_uid('underground sea'/'LEA', 'LEA:Underground Sea:underground sea').
card_rarity('underground sea'/'LEA', 'Rare').
card_artist('underground sea'/'LEA', 'Rob Alexander').
card_multiverse_id('underground sea'/'LEA', '287').

card_in_set('unholy strength', 'LEA').
card_original_type('unholy strength'/'LEA', 'Enchant Creature').
card_original_text('unholy strength'/'LEA', 'Target creature gains +2/+1.').
card_first_print('unholy strength', 'LEA').
card_image_name('unholy strength'/'LEA', 'unholy strength').
card_uid('unholy strength'/'LEA', 'LEA:Unholy Strength:unholy strength').
card_rarity('unholy strength'/'LEA', 'Common').
card_artist('unholy strength'/'LEA', 'Douglas Shuler').
card_multiverse_id('unholy strength'/'LEA', '87').

card_in_set('unsummon', 'LEA').
card_original_type('unsummon'/'LEA', 'Instant').
card_original_text('unsummon'/'LEA', 'Return creature to owner\'s hand; enchantments on creature are CARD ed. Unsummon cannot be played during the damage-dealing phase of an attack.').
card_first_print('unsummon', 'LEA').
card_image_name('unsummon'/'LEA', 'unsummon').
card_uid('unsummon'/'LEA', 'LEA:Unsummon:unsummon').
card_rarity('unsummon'/'LEA', 'Common').
card_artist('unsummon'/'LEA', 'Douglas Shuler').
card_multiverse_id('unsummon'/'LEA', '134').

card_in_set('uthden troll', 'LEA').
card_original_type('uthden troll'/'LEA', 'Summon — Troll').
card_original_text('uthden troll'/'LEA', '{R} Regenerates').
card_first_print('uthden troll', 'LEA').
card_image_name('uthden troll'/'LEA', 'uthden troll').
card_uid('uthden troll'/'LEA', 'LEA:Uthden Troll:uthden troll').
card_rarity('uthden troll'/'LEA', 'Uncommon').
card_artist('uthden troll'/'LEA', 'Douglas Shuler').
card_flavor_text('uthden troll'/'LEA', '\"Oi oi oi, me gotta hurt in \'ere,\nOi oi oi, me smell a ting is near,\nGonna bosh \'n gonna nosh \'n da\nhurt\'ll disappear.\"\n—Traditional').
card_multiverse_id('uthden troll'/'LEA', '228').

card_in_set('verduran enchantress', 'LEA').
card_original_type('verduran enchantress'/'LEA', 'Summon — Enchantress').
card_original_text('verduran enchantress'/'LEA', 'While Enchantress is in play, you may immediately draw a card from your library each time you cast an enchantment.').
card_first_print('verduran enchantress', 'LEA').
card_image_name('verduran enchantress'/'LEA', 'verduran enchantress').
card_uid('verduran enchantress'/'LEA', 'LEA:Verduran Enchantress:verduran enchantress').
card_rarity('verduran enchantress'/'LEA', 'Rare').
card_artist('verduran enchantress'/'LEA', 'Kev Brockschmidt').
card_flavor_text('verduran enchantress'/'LEA', 'Some say magic was first practiced by women, who have always felt strong ties to the land.').
card_multiverse_id('verduran enchantress'/'LEA', '178').

card_in_set('vesuvan doppelganger', 'LEA').
card_original_type('vesuvan doppelganger'/'LEA', 'Summon — Doppelganger').
card_original_text('vesuvan doppelganger'/'LEA', 'Upon summoning, Doppelganger acquires all normal characteristics (except color) of any one creature in play on either side; any enchantments on the original creature are not copied. During controller\'s upkeep, Doppelganger may take on the characteristics of a different creature in play instead. Doppelganger may continue to copy a creature even after that creature leaves play, but if it switches it won\'t be able to switch back.').
card_first_print('vesuvan doppelganger', 'LEA').
card_image_name('vesuvan doppelganger'/'LEA', 'vesuvan doppelganger').
card_uid('vesuvan doppelganger'/'LEA', 'LEA:Vesuvan Doppelganger:vesuvan doppelganger').
card_rarity('vesuvan doppelganger'/'LEA', 'Rare').
card_artist('vesuvan doppelganger'/'LEA', 'Quinton Hoover').
card_multiverse_id('vesuvan doppelganger'/'LEA', '135').

card_in_set('veteran bodyguard', 'LEA').
card_original_type('veteran bodyguard'/'LEA', 'Summon — Bodyguard').
card_original_text('veteran bodyguard'/'LEA', 'Unless Bodyguard is tapped, any damage done to you by unblocked creatures is done instead to Bodyguard. You may not take this damage yourself, though you can prevent it if possible.').
card_first_print('veteran bodyguard', 'LEA').
card_image_name('veteran bodyguard'/'LEA', 'veteran bodyguard').
card_uid('veteran bodyguard'/'LEA', 'LEA:Veteran Bodyguard:veteran bodyguard').
card_rarity('veteran bodyguard'/'LEA', 'Rare').
card_artist('veteran bodyguard'/'LEA', 'Douglas Shuler').
card_flavor_text('veteran bodyguard'/'LEA', 'Good bodyguards are hard to find, mainly because they don\'t live long.').
card_multiverse_id('veteran bodyguard'/'LEA', '272').

card_in_set('volcanic eruption', 'LEA').
card_original_type('volcanic eruption'/'LEA', 'Sorcery').
card_original_text('volcanic eruption'/'LEA', 'Destroys X mountains of your choice, and does X damage to each player and each creature in play.').
card_first_print('volcanic eruption', 'LEA').
card_image_name('volcanic eruption'/'LEA', 'volcanic eruption').
card_uid('volcanic eruption'/'LEA', 'LEA:Volcanic Eruption:volcanic eruption').
card_rarity('volcanic eruption'/'LEA', 'Rare').
card_artist('volcanic eruption'/'LEA', 'Douglas Shuler').
card_multiverse_id('volcanic eruption'/'LEA', '136').

card_in_set('wall of air', 'LEA').
card_original_type('wall of air'/'LEA', 'Summon — Wall').
card_original_text('wall of air'/'LEA', 'Flying').
card_first_print('wall of air', 'LEA').
card_image_name('wall of air'/'LEA', 'wall of air').
card_uid('wall of air'/'LEA', 'LEA:Wall of Air:wall of air').
card_rarity('wall of air'/'LEA', 'Uncommon').
card_artist('wall of air'/'LEA', 'Richard Thomas').
card_flavor_text('wall of air'/'LEA', '\"This ‘standing windstorm\' can hold us off indefinitely? Ridiculous!\" Saying nothing, she put a pinch of salt on the table. With a bang she clapped her hands, and the salt disappeared, blown away.').
card_multiverse_id('wall of air'/'LEA', '137').

card_in_set('wall of bone', 'LEA').
card_original_type('wall of bone'/'LEA', 'Summon — Wall').
card_original_text('wall of bone'/'LEA', '{B} Regenerates').
card_first_print('wall of bone', 'LEA').
card_image_name('wall of bone'/'LEA', 'wall of bone').
card_uid('wall of bone'/'LEA', 'LEA:Wall of Bone:wall of bone').
card_rarity('wall of bone'/'LEA', 'Uncommon').
card_artist('wall of bone'/'LEA', 'Anson Maddocks').
card_flavor_text('wall of bone'/'LEA', 'The Wall of Bone is said to be an aspect of the Great Wall in Hel, where the bones of all sinners wait for Ragnarok, when Hela will call them forth for the final battle.').
card_multiverse_id('wall of bone'/'LEA', '88').

card_in_set('wall of brambles', 'LEA').
card_original_type('wall of brambles'/'LEA', 'Summon — Wall').
card_original_text('wall of brambles'/'LEA', '{G} Regenerates').
card_first_print('wall of brambles', 'LEA').
card_image_name('wall of brambles'/'LEA', 'wall of brambles').
card_uid('wall of brambles'/'LEA', 'LEA:Wall of Brambles:wall of brambles').
card_rarity('wall of brambles'/'LEA', 'Uncommon').
card_artist('wall of brambles'/'LEA', 'Anson Maddocks').
card_flavor_text('wall of brambles'/'LEA', '\"What else, when chaos draws all forces inward to shape a single leaf.\"\n —Conrad Aiken').
card_multiverse_id('wall of brambles'/'LEA', '179').

card_in_set('wall of fire', 'LEA').
card_original_type('wall of fire'/'LEA', 'Summon — Wall').
card_original_text('wall of fire'/'LEA', '{R} +1/+0 until end of turn.').
card_first_print('wall of fire', 'LEA').
card_image_name('wall of fire'/'LEA', 'wall of fire').
card_uid('wall of fire'/'LEA', 'LEA:Wall of Fire:wall of fire').
card_rarity('wall of fire'/'LEA', 'Uncommon').
card_artist('wall of fire'/'LEA', 'Richard Thomas').
card_flavor_text('wall of fire'/'LEA', 'Conjured from the bowels of hell, the fiery wall forms an impassable barrier, searing the soul of any creature attempting to pass through its terrible bursts of flame.').
card_multiverse_id('wall of fire'/'LEA', '229').

card_in_set('wall of ice', 'LEA').
card_original_type('wall of ice'/'LEA', 'Summon — Wall').
card_original_text('wall of ice'/'LEA', '').
card_first_print('wall of ice', 'LEA').
card_image_name('wall of ice'/'LEA', 'wall of ice').
card_uid('wall of ice'/'LEA', 'LEA:Wall of Ice:wall of ice').
card_rarity('wall of ice'/'LEA', 'Uncommon').
card_artist('wall of ice'/'LEA', 'Richard Thomas').
card_flavor_text('wall of ice'/'LEA', '\"And through the drifts the snowy cliffs/ Did send a dismal sheen:/ Nor shapes of men nor beasts we ken—/ The ice was all between.\"/—Samuel Coleridge, \"The Rime of the Ancient Mariner\"').
card_multiverse_id('wall of ice'/'LEA', '180').

card_in_set('wall of stone', 'LEA').
card_original_type('wall of stone'/'LEA', 'Summon — Wall').
card_original_text('wall of stone'/'LEA', '').
card_first_print('wall of stone', 'LEA').
card_image_name('wall of stone'/'LEA', 'wall of stone').
card_uid('wall of stone'/'LEA', 'LEA:Wall of Stone:wall of stone').
card_rarity('wall of stone'/'LEA', 'Uncommon').
card_artist('wall of stone'/'LEA', 'Dan Frazier').
card_flavor_text('wall of stone'/'LEA', 'The Earth herself lends her strength to these walls of living stone, which possess the stability of ancient mountains. These mighty bulwarks thwart ground-based troops, providing welcome relief for weary warriors who defend the land.').
card_multiverse_id('wall of stone'/'LEA', '230').

card_in_set('wall of swords', 'LEA').
card_original_type('wall of swords'/'LEA', 'Summon — Wall').
card_original_text('wall of swords'/'LEA', 'Flying').
card_first_print('wall of swords', 'LEA').
card_image_name('wall of swords'/'LEA', 'wall of swords').
card_uid('wall of swords'/'LEA', 'LEA:Wall of Swords:wall of swords').
card_rarity('wall of swords'/'LEA', 'Uncommon').
card_artist('wall of swords'/'LEA', 'Mark Tedin').
card_flavor_text('wall of swords'/'LEA', 'Just as the evil ones approached to slay Justina, she cast a great spell, imbuing her weapons with her own life force. Thus she fulfilled the prophecy: \"In the death of your savior will you find salvation.\"').
card_multiverse_id('wall of swords'/'LEA', '273').

card_in_set('wall of water', 'LEA').
card_original_type('wall of water'/'LEA', 'Summon — Wall').
card_original_text('wall of water'/'LEA', '{U} +1/+0 until end of turn.').
card_first_print('wall of water', 'LEA').
card_image_name('wall of water'/'LEA', 'wall of water').
card_uid('wall of water'/'LEA', 'LEA:Wall of Water:wall of water').
card_rarity('wall of water'/'LEA', 'Uncommon').
card_artist('wall of water'/'LEA', 'Richard Thomas').
card_flavor_text('wall of water'/'LEA', 'A deafening roar arose as the fury of an enormous vertical river supplanted our serenity. Eddies turned into whirling geysers, leveling everything in their path.').
card_multiverse_id('wall of water'/'LEA', '138').

card_in_set('wall of wood', 'LEA').
card_original_type('wall of wood'/'LEA', 'Summon — Wall').
card_original_text('wall of wood'/'LEA', '').
card_first_print('wall of wood', 'LEA').
card_image_name('wall of wood'/'LEA', 'wall of wood').
card_uid('wall of wood'/'LEA', 'LEA:Wall of Wood:wall of wood').
card_rarity('wall of wood'/'LEA', 'Common').
card_artist('wall of wood'/'LEA', 'Mark Tedin').
card_flavor_text('wall of wood'/'LEA', 'Everybody knows that to ward off trouble, you knock on wood. But usually it\'s better to make a wall out of the wood and let trouble do the knocking.').
card_multiverse_id('wall of wood'/'LEA', '181').

card_in_set('wanderlust', 'LEA').
card_original_type('wanderlust'/'LEA', 'Enchant Creature').
card_original_text('wanderlust'/'LEA', 'Wanderlust does 1 damage to target creature\'s controller during upkeep.').
card_first_print('wanderlust', 'LEA').
card_image_name('wanderlust'/'LEA', 'wanderlust').
card_uid('wanderlust'/'LEA', 'LEA:Wanderlust:wanderlust').
card_rarity('wanderlust'/'LEA', 'Uncommon').
card_artist('wanderlust'/'LEA', 'Cornelius Brudi').
card_multiverse_id('wanderlust'/'LEA', '182').

card_in_set('war mammoth', 'LEA').
card_original_type('war mammoth'/'LEA', 'Summon — Mammoth').
card_original_text('war mammoth'/'LEA', 'Trample').
card_first_print('war mammoth', 'LEA').
card_image_name('war mammoth'/'LEA', 'war mammoth').
card_uid('war mammoth'/'LEA', 'LEA:War Mammoth:war mammoth').
card_rarity('war mammoth'/'LEA', 'Common').
card_artist('war mammoth'/'LEA', 'Jeff A. Menges').
card_flavor_text('war mammoth'/'LEA', 'I didn\'t think Mammoths could ever hold a candle to a well-trained battle horse. Then one day I turned my back on a drunken soldier. His blow never landed; Mi\'cha flung the brute over ten meters.').
card_multiverse_id('war mammoth'/'LEA', '183').

card_in_set('warp artifact', 'LEA').
card_original_type('warp artifact'/'LEA', 'Enchant Artifact').
card_original_text('warp artifact'/'LEA', 'Warp Artifact does 1 damage to target artifact\'s controller at start of each turn.').
card_first_print('warp artifact', 'LEA').
card_image_name('warp artifact'/'LEA', 'warp artifact').
card_uid('warp artifact'/'LEA', 'LEA:Warp Artifact:warp artifact').
card_rarity('warp artifact'/'LEA', 'Rare').
card_artist('warp artifact'/'LEA', 'Amy Weber').
card_multiverse_id('warp artifact'/'LEA', '89').

card_in_set('water elemental', 'LEA').
card_original_type('water elemental'/'LEA', 'Summon — Elemental').
card_original_text('water elemental'/'LEA', '').
card_first_print('water elemental', 'LEA').
card_image_name('water elemental'/'LEA', 'water elemental').
card_uid('water elemental'/'LEA', 'LEA:Water Elemental:water elemental').
card_rarity('water elemental'/'LEA', 'Uncommon').
card_artist('water elemental'/'LEA', 'Jeff A. Menges').
card_flavor_text('water elemental'/'LEA', 'Unpredictable as the sea itself, Water Elementals shift without warning from tranquility to tempest. Capricious and fickle, they flow restlessly from one shape to another, expressing their moods with their physical forms.').
card_multiverse_id('water elemental'/'LEA', '139').

card_in_set('weakness', 'LEA').
card_original_type('weakness'/'LEA', 'Enchant Creature').
card_original_text('weakness'/'LEA', 'Target creature loses -2/-1; if this drops the creature\'s toughness below 1, it is dead.').
card_first_print('weakness', 'LEA').
card_image_name('weakness'/'LEA', 'weakness').
card_uid('weakness'/'LEA', 'LEA:Weakness:weakness').
card_rarity('weakness'/'LEA', 'Common').
card_artist('weakness'/'LEA', 'Douglas Shuler').
card_multiverse_id('weakness'/'LEA', '90').

card_in_set('web', 'LEA').
card_original_type('web'/'LEA', 'Enchant Creature').
card_original_text('web'/'LEA', 'Target creature gains +0/+2 and can now block flying creatures, though it does not gain the power to fly.').
card_first_print('web', 'LEA').
card_image_name('web'/'LEA', 'web').
card_uid('web'/'LEA', 'LEA:Web:web').
card_rarity('web'/'LEA', 'Rare').
card_artist('web'/'LEA', 'Rob Alexander').
card_multiverse_id('web'/'LEA', '184').

card_in_set('wheel of fortune', 'LEA').
card_original_type('wheel of fortune'/'LEA', 'Sorcery').
card_original_text('wheel of fortune'/'LEA', 'Both players must discard their hands and draw seven new cards.').
card_first_print('wheel of fortune', 'LEA').
card_image_name('wheel of fortune'/'LEA', 'wheel of fortune').
card_uid('wheel of fortune'/'LEA', 'LEA:Wheel of Fortune:wheel of fortune').
card_rarity('wheel of fortune'/'LEA', 'Rare').
card_artist('wheel of fortune'/'LEA', 'Daniel Gelon').
card_multiverse_id('wheel of fortune'/'LEA', '231').

card_in_set('white knight', 'LEA').
card_original_type('white knight'/'LEA', 'Summon — Knight').
card_original_text('white knight'/'LEA', 'Protection from black, first strike').
card_first_print('white knight', 'LEA').
card_image_name('white knight'/'LEA', 'white knight').
card_uid('white knight'/'LEA', 'LEA:White Knight:white knight').
card_rarity('white knight'/'LEA', 'Uncommon').
card_artist('white knight'/'LEA', 'Daniel Gelon').
card_flavor_text('white knight'/'LEA', 'Out of the blackness and stench of the engulfing swamp emerged a shimmering figure. Only the splattered armor and ichor-stained sword hinted at the unfathomable evil the knight had just laid waste.').
card_multiverse_id('white knight'/'LEA', '274').

card_in_set('white ward', 'LEA').
card_original_type('white ward'/'LEA', 'Enchant Creature').
card_original_text('white ward'/'LEA', 'Target creature gains protection from white.').
card_first_print('white ward', 'LEA').
card_image_name('white ward'/'LEA', 'white ward').
card_uid('white ward'/'LEA', 'LEA:White Ward:white ward').
card_rarity('white ward'/'LEA', 'Uncommon').
card_artist('white ward'/'LEA', 'Dan Frazier').
card_multiverse_id('white ward'/'LEA', '275').

card_in_set('wild growth', 'LEA').
card_original_type('wild growth'/'LEA', 'Enchant Land').
card_original_text('wild growth'/'LEA', 'When tapped, target land provides 1 green mana in addition to the mana it normally provides.').
card_first_print('wild growth', 'LEA').
card_image_name('wild growth'/'LEA', 'wild growth').
card_uid('wild growth'/'LEA', 'LEA:Wild Growth:wild growth').
card_rarity('wild growth'/'LEA', 'Common').
card_artist('wild growth'/'LEA', 'Mark Poole').
card_multiverse_id('wild growth'/'LEA', '185').

card_in_set('will-o\'-the-wisp', 'LEA').
card_original_type('will-o\'-the-wisp'/'LEA', 'Summon — Will-O\'-The-Wisp').
card_original_text('will-o\'-the-wisp'/'LEA', 'Flying; {B} Regenerates').
card_first_print('will-o\'-the-wisp', 'LEA').
card_image_name('will-o\'-the-wisp'/'LEA', 'will-o\'-the-wisp').
card_uid('will-o\'-the-wisp'/'LEA', 'LEA:Will-o\'-the-Wisp:will-o\'-the-wisp').
card_rarity('will-o\'-the-wisp'/'LEA', 'Rare').
card_artist('will-o\'-the-wisp'/'LEA', 'Jesper Myrfors').
card_flavor_text('will-o\'-the-wisp'/'LEA', '\"About, about in reel and rout\nThe death-fires danced at night;\nThe water, like a witch\'s oils,\nBurnt green, and blue and white.\"\n—Samuel Coleridge, \"The Rime of the Ancient Mariner\"').
card_multiverse_id('will-o\'-the-wisp'/'LEA', '91').

card_in_set('winter orb', 'LEA').
card_original_type('winter orb'/'LEA', 'Continuous Artifact').
card_original_text('winter orb'/'LEA', 'Players can untap only one land each during untap phase. Creatures and artifacts are untapped as normal.').
card_first_print('winter orb', 'LEA').
card_image_name('winter orb'/'LEA', 'winter orb').
card_uid('winter orb'/'LEA', 'LEA:Winter Orb:winter orb').
card_rarity('winter orb'/'LEA', 'Rare').
card_artist('winter orb'/'LEA', 'Mark Tedin').
card_multiverse_id('winter orb'/'LEA', '46').

card_in_set('wooden sphere', 'LEA').
card_original_type('wooden sphere'/'LEA', 'Poly Artifact').
card_original_text('wooden sphere'/'LEA', '{1}: Any green spell cast by any player gives you 1 life.').
card_first_print('wooden sphere', 'LEA').
card_image_name('wooden sphere'/'LEA', 'wooden sphere').
card_uid('wooden sphere'/'LEA', 'LEA:Wooden Sphere:wooden sphere').
card_rarity('wooden sphere'/'LEA', 'Uncommon').
card_artist('wooden sphere'/'LEA', 'Mark Tedin').
card_multiverse_id('wooden sphere'/'LEA', '47').

card_in_set('word of command', 'LEA').
card_original_type('word of command'/'LEA', 'Instant').
card_original_text('word of command'/'LEA', 'You may look at opponent\'s hand and choose any card opponent can legally play using mana from his or her mana pool or lands. Opponent must play this card immediately; you make all the decisions it calls for. This spell may not be countered after you have looked at opponent\'s hand.').
card_first_print('word of command', 'LEA').
card_image_name('word of command'/'LEA', 'word of command').
card_uid('word of command'/'LEA', 'LEA:Word of Command:word of command').
card_rarity('word of command'/'LEA', 'Rare').
card_artist('word of command'/'LEA', 'Jesper Myrfors').
card_multiverse_id('word of command'/'LEA', '92').

card_in_set('wrath of god', 'LEA').
card_original_type('wrath of god'/'LEA', 'Sorcery').
card_original_text('wrath of god'/'LEA', 'All creatures in play are destroyed and cannot regenerate.').
card_first_print('wrath of god', 'LEA').
card_image_name('wrath of god'/'LEA', 'wrath of god').
card_uid('wrath of god'/'LEA', 'LEA:Wrath of God:wrath of god').
card_rarity('wrath of god'/'LEA', 'Rare').
card_artist('wrath of god'/'LEA', 'Quinton Hoover').
card_multiverse_id('wrath of god'/'LEA', '276').

card_in_set('zombie master', 'LEA').
card_original_type('zombie master'/'LEA', 'Summon — Lord').
card_original_text('zombie master'/'LEA', 'All zombies in play gain swampwalk and \"{B} Regenerates\" for as long as this card remains in play.').
card_first_print('zombie master', 'LEA').
card_image_name('zombie master'/'LEA', 'zombie master').
card_uid('zombie master'/'LEA', 'LEA:Zombie Master:zombie master').
card_rarity('zombie master'/'LEA', 'Rare').
card_artist('zombie master'/'LEA', 'Jeff A. Menges').
card_flavor_text('zombie master'/'LEA', 'They say the Zombie Master controlled these foul creatures even before his own death, but now that he is one of them, nothing can make them betray him.').
card_multiverse_id('zombie master'/'LEA', '93').
