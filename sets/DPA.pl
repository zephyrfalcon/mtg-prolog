% Duels of the Planeswalkers

set('DPA').
set_name('DPA', 'Duels of the Planeswalkers').
set_release_date('DPA', '2010-06-04').
set_border('DPA', 'black').
set_type('DPA', 'box').

card_in_set('abyssal specter', 'DPA').
card_original_type('abyssal specter'/'DPA', 'Creature — Specter').
card_original_text('abyssal specter'/'DPA', 'Flying (This creature can\'t be blocked except by creatures with flying.)\nWhenever Abyssal Specter deals damage to a player, that player discards a card from his or her hand.').
card_image_name('abyssal specter'/'DPA', 'abyssal specter').
card_uid('abyssal specter'/'DPA', 'DPA:Abyssal Specter:abyssal specter').
card_rarity('abyssal specter'/'DPA', 'Uncommon').
card_artist('abyssal specter'/'DPA', 'Michael Sutfin').
card_number('abyssal specter'/'DPA', '18').
card_flavor_text('abyssal specter'/'DPA', 'To gaze under its hood is to invite death.').

card_in_set('act of treason', 'DPA').
card_original_type('act of treason'/'DPA', 'Sorcery').
card_original_text('act of treason'/'DPA', 'Gain control of target creature until end of turn. Untap that creature. It gains haste until end of turn. (It can attack and {T} this turn.)').
card_image_name('act of treason'/'DPA', 'act of treason').
card_uid('act of treason'/'DPA', 'DPA:Act of Treason:act of treason').
card_rarity('act of treason'/'DPA', 'Uncommon').
card_artist('act of treason'/'DPA', 'Eric Deschamps').
card_number('act of treason'/'DPA', '37').
card_flavor_text('act of treason'/'DPA', '\"Rage courses in every heart, yearning to betray its rational prison.\"\n—Sarkhan Vol').

card_in_set('air elemental', 'DPA').
card_original_type('air elemental'/'DPA', 'Creature — Elemental').
card_original_text('air elemental'/'DPA', 'Flying').
card_image_name('air elemental'/'DPA', 'air elemental').
card_uid('air elemental'/'DPA', 'DPA:Air Elemental:air elemental').
card_rarity('air elemental'/'DPA', 'Uncommon').
card_artist('air elemental'/'DPA', 'Kev Walker').
card_number('air elemental'/'DPA', '1').
card_flavor_text('air elemental'/'DPA', '\"The East Wind, an interloper in the dominions of Westerly Weather, is an impassive-faced tyrant with a sharp poniard held behind his back for a treacherous stab.\"\n—Joseph Conrad, The Mirror of the Sea').

card_in_set('ascendant evincar', 'DPA').
card_original_type('ascendant evincar'/'DPA', 'Legendary Creature — Vampire').
card_original_text('ascendant evincar'/'DPA', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nOther black creatures get +1/+1. \nNonblack creatures get -1/-1.').
card_image_name('ascendant evincar'/'DPA', 'ascendant evincar').
card_uid('ascendant evincar'/'DPA', 'DPA:Ascendant Evincar:ascendant evincar').
card_rarity('ascendant evincar'/'DPA', 'Rare').
card_artist('ascendant evincar'/'DPA', 'Mark Zug').
card_number('ascendant evincar'/'DPA', '19').
card_flavor_text('ascendant evincar'/'DPA', 'His soul snared by an angel\'s curse, Crovax twisted heroism into its purest shadow.').

card_in_set('banefire', 'DPA').
card_original_type('banefire'/'DPA', 'Sorcery').
card_original_text('banefire'/'DPA', 'Banefire deals X damage to target creature or player.\nIf X is 5 or more, Banefire can\'t be countered by spells or abilities and the damage can\'t be prevented.').
card_image_name('banefire'/'DPA', 'banefire').
card_uid('banefire'/'DPA', 'DPA:Banefire:banefire').
card_rarity('banefire'/'DPA', 'Rare').
card_artist('banefire'/'DPA', 'Raymond Swanland').
card_number('banefire'/'DPA', '38').
card_flavor_text('banefire'/'DPA', 'For Sarkhan Vol, the dragon is the purest expression of life\'s savage splendor.').

card_in_set('blanchwood armor', 'DPA').
card_original_type('blanchwood armor'/'DPA', 'Enchantment — Aura').
card_original_text('blanchwood armor'/'DPA', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nEnchanted creature gets +1/+1 for each Forest you control.').
card_image_name('blanchwood armor'/'DPA', 'blanchwood armor').
card_uid('blanchwood armor'/'DPA', 'DPA:Blanchwood Armor:blanchwood armor').
card_rarity('blanchwood armor'/'DPA', 'Uncommon').
card_artist('blanchwood armor'/'DPA', 'Paolo Parente').
card_number('blanchwood armor'/'DPA', '55').
card_flavor_text('blanchwood armor'/'DPA', '\"Before armor, there was bark. Before blades, there were thorns.\"\n—Molimo, maro-sorcerer').

card_in_set('blaze', 'DPA').
card_original_type('blaze'/'DPA', 'Sorcery').
card_original_text('blaze'/'DPA', 'Blaze deals X damage to target creature or player.').
card_image_name('blaze'/'DPA', 'blaze').
card_uid('blaze'/'DPA', 'DPA:Blaze:blaze').
card_rarity('blaze'/'DPA', 'Uncommon').
card_artist('blaze'/'DPA', 'Alex Horley-Orlandelli').
card_number('blaze'/'DPA', '39').

card_in_set('bloodmark mentor', 'DPA').
card_original_type('bloodmark mentor'/'DPA', 'Creature — Goblin Warrior').
card_original_text('bloodmark mentor'/'DPA', 'Red creatures you control have first strike.').
card_image_name('bloodmark mentor'/'DPA', 'bloodmark mentor').
card_uid('bloodmark mentor'/'DPA', 'DPA:Bloodmark Mentor:bloodmark mentor').
card_rarity('bloodmark mentor'/'DPA', 'Uncommon').
card_artist('bloodmark mentor'/'DPA', 'Dave Allsop').
card_number('bloodmark mentor'/'DPA', '40').
card_flavor_text('bloodmark mentor'/'DPA', 'Boggarts divide the world into two categories: things you can eat, and things you have to chase down and pummel before you can eat.').

card_in_set('boomerang', 'DPA').
card_original_type('boomerang'/'DPA', 'Instant').
card_original_text('boomerang'/'DPA', 'Return target permanent to its owner\'s hand.').
card_image_name('boomerang'/'DPA', 'boomerang').
card_uid('boomerang'/'DPA', 'DPA:Boomerang:boomerang').
card_rarity('boomerang'/'DPA', 'Common').
card_artist('boomerang'/'DPA', 'Arnie Swekel').
card_number('boomerang'/'DPA', '2').
card_flavor_text('boomerang'/'DPA', 'Early Jamuraan hunters devised a weapon that would return to its source. Tolarian Æthermancers developed a spell that skipped the weapon entirely.').

card_in_set('cancel', 'DPA').
card_original_type('cancel'/'DPA', 'Instant').
card_original_text('cancel'/'DPA', 'Counter target spell.').
card_image_name('cancel'/'DPA', 'cancel').
card_uid('cancel'/'DPA', 'DPA:Cancel:cancel').
card_rarity('cancel'/'DPA', 'Common').
card_artist('cancel'/'DPA', 'David Palumbo').
card_number('cancel'/'DPA', '3').

card_in_set('cinder pyromancer', 'DPA').
card_original_type('cinder pyromancer'/'DPA', 'Creature — Elemental Shaman').
card_original_text('cinder pyromancer'/'DPA', '{T}: Cinder Pyromancer deals 1 damage to target player.\nWhenever you play a red spell, you may untap Cinder Pyromancer.').
card_image_name('cinder pyromancer'/'DPA', 'cinder pyromancer').
card_uid('cinder pyromancer'/'DPA', 'DPA:Cinder Pyromancer:cinder pyromancer').
card_rarity('cinder pyromancer'/'DPA', 'Common').
card_artist('cinder pyromancer'/'DPA', 'Greg Staples').
card_number('cinder pyromancer'/'DPA', '41').
card_flavor_text('cinder pyromancer'/'DPA', '\"If the whole world burns, I\'ll never grow cold.\"').

card_in_set('civic wayfinder', 'DPA').
card_original_type('civic wayfinder'/'DPA', 'Creature — Elf Warrior Druid').
card_original_text('civic wayfinder'/'DPA', 'When Civic Wayfinder comes into play, you may search your library for a basic land card, reveal it, and put it into your hand. If you do, shuffle your library.').
card_image_name('civic wayfinder'/'DPA', 'civic wayfinder').
card_uid('civic wayfinder'/'DPA', 'DPA:Civic Wayfinder:civic wayfinder').
card_rarity('civic wayfinder'/'DPA', 'Common').
card_artist('civic wayfinder'/'DPA', 'Cyril Van Der Haegen').
card_number('civic wayfinder'/'DPA', '56').
card_flavor_text('civic wayfinder'/'DPA', '\"These alleys are not safe. Come, I can guide you back to the market square.\"').

card_in_set('cloud sprite', 'DPA').
card_original_type('cloud sprite'/'DPA', 'Creature — Faerie').
card_original_text('cloud sprite'/'DPA', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nCloud Sprite can block only creatures with flying.').
card_image_name('cloud sprite'/'DPA', 'cloud sprite').
card_uid('cloud sprite'/'DPA', 'DPA:Cloud Sprite:cloud sprite').
card_rarity('cloud sprite'/'DPA', 'Common').
card_artist('cloud sprite'/'DPA', 'Mark Zug').
card_number('cloud sprite'/'DPA', '4').
card_flavor_text('cloud sprite'/'DPA', 'The delicate sprites carry messages for Saprazzans, but they refuse to land in Mercadia City\'s filthy markets.').

card_in_set('coat of arms', 'DPA').
card_original_type('coat of arms'/'DPA', 'Artifact').
card_original_text('coat of arms'/'DPA', 'Each creature gets +1/+1 for each other creature on the battlefield that shares at least one creature type with it. (For example, if two Goblin Warriors and a Goblin Shaman are on the battlefield, each gets +2/+2.)').
card_image_name('coat of arms'/'DPA', 'coat of arms').
card_uid('coat of arms'/'DPA', 'DPA:Coat of Arms:coat of arms').
card_rarity('coat of arms'/'DPA', 'Rare').
card_artist('coat of arms'/'DPA', 'Scott M. Fischer').
card_number('coat of arms'/'DPA', '90').
card_flavor_text('coat of arms'/'DPA', 'A banner for bonds of blood or belief.').

card_in_set('consume spirit', 'DPA').
card_original_type('consume spirit'/'DPA', 'Sorcery').
card_original_text('consume spirit'/'DPA', 'Spend only black mana on X. \nConsume Spirit deals X damage to target creature or player and you gain X life.').
card_image_name('consume spirit'/'DPA', 'consume spirit').
card_uid('consume spirit'/'DPA', 'DPA:Consume Spirit:consume spirit').
card_rarity('consume spirit'/'DPA', 'Uncommon').
card_artist('consume spirit'/'DPA', 'Justin Sweet').
card_number('consume spirit'/'DPA', '20').

card_in_set('counterbore', 'DPA').
card_original_type('counterbore'/'DPA', 'Instant').
card_original_text('counterbore'/'DPA', 'Counter target spell. Search its controller\'s graveyard, hand, and library for all cards with the same name as that spell and remove them from the game. Then that player shuffles his or her library.').
card_image_name('counterbore'/'DPA', 'counterbore').
card_uid('counterbore'/'DPA', 'DPA:Counterbore:counterbore').
card_rarity('counterbore'/'DPA', 'Rare').
card_artist('counterbore'/'DPA', 'Wayne England').
card_number('counterbore'/'DPA', '5').

card_in_set('crowd of cinders', 'DPA').
card_original_type('crowd of cinders'/'DPA', 'Creature — Elemental').
card_original_text('crowd of cinders'/'DPA', 'Fear\nCrowd of Cinders\'s power and toughness are each equal to the number of black permanents you control.').
card_image_name('crowd of cinders'/'DPA', 'crowd of cinders').
card_uid('crowd of cinders'/'DPA', 'DPA:Crowd of Cinders:crowd of cinders').
card_rarity('crowd of cinders'/'DPA', 'Uncommon').
card_artist('crowd of cinders'/'DPA', 'Carl Frank').
card_number('crowd of cinders'/'DPA', '21').
card_flavor_text('crowd of cinders'/'DPA', 'They envy the life-giving heat so much that they tear it from those who still possess it.').

card_in_set('deluge', 'DPA').
card_original_type('deluge'/'DPA', 'Instant').
card_original_text('deluge'/'DPA', 'Tap all creatures without flying.').
card_image_name('deluge'/'DPA', 'deluge').
card_uid('deluge'/'DPA', 'DPA:Deluge:deluge').
card_rarity('deluge'/'DPA', 'Uncommon').
card_artist('deluge'/'DPA', 'Wayne England').
card_number('deluge'/'DPA', '6').
card_flavor_text('deluge'/'DPA', '\"From the sea came all life, and to the sea it will return. The sooner the better.\"\n—Emperor Aboshan').

card_in_set('demon\'s horn', 'DPA').
card_original_type('demon\'s horn'/'DPA', 'Artifact').
card_original_text('demon\'s horn'/'DPA', 'Whenever a player casts a black spell, you may gain 1 life.').
card_image_name('demon\'s horn'/'DPA', 'demon\'s horn').
card_uid('demon\'s horn'/'DPA', 'DPA:Demon\'s Horn:demon\'s horn').
card_rarity('demon\'s horn'/'DPA', 'Uncommon').
card_artist('demon\'s horn'/'DPA', 'Alan Pollack').
card_number('demon\'s horn'/'DPA', '91').
card_flavor_text('demon\'s horn'/'DPA', 'Its curve mimics the twists of life and death.').

card_in_set('denizen of the deep', 'DPA').
card_original_type('denizen of the deep'/'DPA', 'Creature — Serpent').
card_original_text('denizen of the deep'/'DPA', 'When Denizen of the Deep comes into play, return each other creature you control to its owner\'s hand.').
card_image_name('denizen of the deep'/'DPA', 'denizen of the deep').
card_uid('denizen of the deep'/'DPA', 'DPA:Denizen of the Deep:denizen of the deep').
card_rarity('denizen of the deep'/'DPA', 'Rare').
card_artist('denizen of the deep'/'DPA', 'Jim Pavelec').
card_number('denizen of the deep'/'DPA', '7').
card_flavor_text('denizen of the deep'/'DPA', 'According to merfolk legend, a denizen of the deep swallows the horizon at the end of each day, bringing on the cold blanket of night.').

card_in_set('dragon\'s claw', 'DPA').
card_original_type('dragon\'s claw'/'DPA', 'Artifact').
card_original_text('dragon\'s claw'/'DPA', 'Whenever a player casts a red spell, you may gain 1 life.').
card_image_name('dragon\'s claw'/'DPA', 'dragon\'s claw').
card_uid('dragon\'s claw'/'DPA', 'DPA:Dragon\'s Claw:dragon\'s claw').
card_rarity('dragon\'s claw'/'DPA', 'Uncommon').
card_artist('dragon\'s claw'/'DPA', 'Alan Pollack').
card_number('dragon\'s claw'/'DPA', '92').
card_flavor_text('dragon\'s claw'/'DPA', 'Though no longer attached to the hand, it still holds its adversary in its grasp.').

card_in_set('drove of elves', 'DPA').
card_original_type('drove of elves'/'DPA', 'Creature — Elf').
card_original_text('drove of elves'/'DPA', 'Drove of Elves\'s power and toughness are each equal to the number of green permanents you control.\nDrove of Elves can\'t be the target of spells or abilities your opponents control.').
card_image_name('drove of elves'/'DPA', 'drove of elves').
card_uid('drove of elves'/'DPA', 'DPA:Drove of Elves:drove of elves').
card_rarity('drove of elves'/'DPA', 'Uncommon').
card_artist('drove of elves'/'DPA', 'Larry MacDougall').
card_number('drove of elves'/'DPA', '57').
card_flavor_text('drove of elves'/'DPA', '\"The light of beauty protects our journeys through darkness.\"').

card_in_set('drudge skeletons', 'DPA').
card_original_type('drudge skeletons'/'DPA', 'Creature — Skeleton').
card_original_text('drudge skeletons'/'DPA', '{B}: Regenerate Drudge Skeletons. (The next time this creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_image_name('drudge skeletons'/'DPA', 'drudge skeletons').
card_uid('drudge skeletons'/'DPA', 'DPA:Drudge Skeletons:drudge skeletons').
card_rarity('drudge skeletons'/'DPA', 'Common').
card_artist('drudge skeletons'/'DPA', 'Daarken').
card_number('drudge skeletons'/'DPA', '22').
card_flavor_text('drudge skeletons'/'DPA', '\"The dead make good soldiers. They can\'t disobey orders, never surrender, and don\'t stop fighting when a random body part falls off.\"\n—Nevinyrral, Necromancer\'s Handbook').

card_in_set('dusk imp', 'DPA').
card_original_type('dusk imp'/'DPA', 'Creature — Imp').
card_original_text('dusk imp'/'DPA', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)').
card_image_name('dusk imp'/'DPA', 'dusk imp').
card_uid('dusk imp'/'DPA', 'DPA:Dusk Imp:dusk imp').
card_rarity('dusk imp'/'DPA', 'Common').
card_artist('dusk imp'/'DPA', 'Pete Venters').
card_number('dusk imp'/'DPA', '23').
card_flavor_text('dusk imp'/'DPA', 'Imps are just intelligent enough to have an understanding of cruelty.').

card_in_set('duskdale wurm', 'DPA').
card_original_type('duskdale wurm'/'DPA', 'Creature — Wurm').
card_original_text('duskdale wurm'/'DPA', 'Trample').
card_image_name('duskdale wurm'/'DPA', 'duskdale wurm').
card_uid('duskdale wurm'/'DPA', 'DPA:Duskdale Wurm:duskdale wurm').
card_rarity('duskdale wurm'/'DPA', 'Uncommon').
card_artist('duskdale wurm'/'DPA', 'Dan Dos Santos').
card_number('duskdale wurm'/'DPA', '58').

card_in_set('earth elemental', 'DPA').
card_original_type('earth elemental'/'DPA', 'Creature — Elemental').
card_original_text('earth elemental'/'DPA', '').
card_image_name('earth elemental'/'DPA', 'earth elemental').
card_uid('earth elemental'/'DPA', 'DPA:Earth Elemental:earth elemental').
card_rarity('earth elemental'/'DPA', 'Uncommon').
card_artist('earth elemental'/'DPA', 'Anthony S. Waters').
card_number('earth elemental'/'DPA', '42').
card_flavor_text('earth elemental'/'DPA', 'Earth elementals have the eternal strength of stone and the endurance of mountains. Primordially connected to the land they inhabit, they take a long-term view of things, scorning the impetuous haste of short-lived mortal creatures.').

card_in_set('elven riders', 'DPA').
card_original_type('elven riders'/'DPA', 'Creature — Elf').
card_original_text('elven riders'/'DPA', 'Elven Riders can\'t be blocked except by Walls and/or creatures with flying.').
card_image_name('elven riders'/'DPA', 'elven riders').
card_uid('elven riders'/'DPA', 'DPA:Elven Riders:elven riders').
card_rarity('elven riders'/'DPA', 'Uncommon').
card_artist('elven riders'/'DPA', 'Darrell Riche').
card_number('elven riders'/'DPA', '59').
card_flavor_text('elven riders'/'DPA', '\"Wirewood cannot hide great size. Only with speed and skill can we survive here.\"').

card_in_set('elvish champion', 'DPA').
card_original_type('elvish champion'/'DPA', 'Creature — Elf').
card_original_text('elvish champion'/'DPA', 'Other Elf creatures get +1/+1 and have forestwalk. (They\'re unblockable as long as defending player controls a Forest.)').
card_image_name('elvish champion'/'DPA', 'elvish champion').
card_uid('elvish champion'/'DPA', 'DPA:Elvish Champion:elvish champion').
card_rarity('elvish champion'/'DPA', 'Rare').
card_artist('elvish champion'/'DPA', 'D. Alexander Gregory').
card_number('elvish champion'/'DPA', '60').
card_flavor_text('elvish champion'/'DPA', '\"For what are leaves but countless blades\nTo fight a countless foe on high.\"\n—Elvish hymn').

card_in_set('elvish eulogist', 'DPA').
card_original_type('elvish eulogist'/'DPA', 'Creature — Elf Shaman').
card_original_text('elvish eulogist'/'DPA', 'Sacrifice Elvish Eulogist: You gain 1 life for each Elf card in your graveyard.').
card_image_name('elvish eulogist'/'DPA', 'elvish eulogist').
card_uid('elvish eulogist'/'DPA', 'DPA:Elvish Eulogist:elvish eulogist').
card_rarity('elvish eulogist'/'DPA', 'Common').
card_artist('elvish eulogist'/'DPA', 'Ben Thompson').
card_number('elvish eulogist'/'DPA', '61').
card_flavor_text('elvish eulogist'/'DPA', '\"No matter how adept our artistic skill, our effigies can never hope to capture the vibrant beauty of a living elf. Perhaps that is truly why we mourn.\"').

card_in_set('elvish promenade', 'DPA').
card_original_type('elvish promenade'/'DPA', 'Tribal Sorcery — Elf').
card_original_text('elvish promenade'/'DPA', 'Put a 1/1 green Elf Warrior creature token into play for each Elf you control.').
card_image_name('elvish promenade'/'DPA', 'elvish promenade').
card_uid('elvish promenade'/'DPA', 'DPA:Elvish Promenade:elvish promenade').
card_rarity('elvish promenade'/'DPA', 'Uncommon').
card_artist('elvish promenade'/'DPA', 'Steve Ellis').
card_number('elvish promenade'/'DPA', '62').
card_flavor_text('elvish promenade'/'DPA', 'The faultless and immaculate castes form the lower tiers of elvish society, with the exquisite caste above them. At the pinnacle is the perfect, a consummate blend of aristocrat and predator.').

card_in_set('elvish visionary', 'DPA').
card_original_type('elvish visionary'/'DPA', 'Creature — Elf Shaman').
card_original_text('elvish visionary'/'DPA', 'When Elvish Visionary enters the battlefield, draw a card.').
card_image_name('elvish visionary'/'DPA', 'elvish visionary').
card_uid('elvish visionary'/'DPA', 'DPA:Elvish Visionary:elvish visionary').
card_rarity('elvish visionary'/'DPA', 'Common').
card_artist('elvish visionary'/'DPA', 'D. Alexander Gregory').
card_number('elvish visionary'/'DPA', '63').
card_flavor_text('elvish visionary'/'DPA', 'Before any major undertaking, a Cylian elf seeks the guidance of a visionary to learn the will of the gargantuan ancients.').

card_in_set('elvish warrior', 'DPA').
card_original_type('elvish warrior'/'DPA', 'Creature — Elf Warrior').
card_original_text('elvish warrior'/'DPA', '').
card_image_name('elvish warrior'/'DPA', 'elvish warrior').
card_uid('elvish warrior'/'DPA', 'DPA:Elvish Warrior:elvish warrior').
card_rarity('elvish warrior'/'DPA', 'Common').
card_artist('elvish warrior'/'DPA', 'Kev Walker').
card_number('elvish warrior'/'DPA', '64').
card_flavor_text('elvish warrior'/'DPA', 'As graceful as a deer leaping a stream and as deadly as the wolf waiting in ambush on the other side, elvish warriors are the eyes of the forest as well as its unsheathed claws.').

card_in_set('enrage', 'DPA').
card_original_type('enrage'/'DPA', 'Instant').
card_original_text('enrage'/'DPA', 'Target creature gets +X/+0 until end of turn.').
card_image_name('enrage'/'DPA', 'enrage').
card_uid('enrage'/'DPA', 'DPA:Enrage:enrage').
card_rarity('enrage'/'DPA', 'Uncommon').
card_artist('enrage'/'DPA', 'Justin Sweet').
card_number('enrage'/'DPA', '43').
card_flavor_text('enrage'/'DPA', 'The barbarians call it \"touching the soul of the dragon.\"').

card_in_set('essence drain', 'DPA').
card_original_type('essence drain'/'DPA', 'Sorcery').
card_original_text('essence drain'/'DPA', 'Essence Drain deals 3 damage to target creature or player and you gain 3 life.').
card_image_name('essence drain'/'DPA', 'essence drain').
card_uid('essence drain'/'DPA', 'DPA:Essence Drain:essence drain').
card_rarity('essence drain'/'DPA', 'Common').
card_artist('essence drain'/'DPA', 'Jim Nelson').
card_number('essence drain'/'DPA', '24').
card_flavor_text('essence drain'/'DPA', '\"The elves are right: Death inevitably leads to life. But the truly powerful don\'t just experience this cycle. They control it.\"\n—Crovax, ascendant evincar').

card_in_set('essence scatter', 'DPA').
card_original_type('essence scatter'/'DPA', 'Instant').
card_original_text('essence scatter'/'DPA', 'Counter target creature spell.').
card_image_name('essence scatter'/'DPA', 'essence scatter').
card_uid('essence scatter'/'DPA', 'DPA:Essence Scatter:essence scatter').
card_rarity('essence scatter'/'DPA', 'Common').
card_artist('essence scatter'/'DPA', 'Jon Foster').
card_number('essence scatter'/'DPA', '8').
card_flavor_text('essence scatter'/'DPA', 'Some wizards compete not to summon the most interesting creatures, but to create the most interesting aftereffects when a summons goes awry.').

card_in_set('evacuation', 'DPA').
card_original_type('evacuation'/'DPA', 'Instant').
card_original_text('evacuation'/'DPA', 'Return all creatures to their owners\' hands.').
card_image_name('evacuation'/'DPA', 'evacuation').
card_uid('evacuation'/'DPA', 'DPA:Evacuation:evacuation').
card_rarity('evacuation'/'DPA', 'Rare').
card_artist('evacuation'/'DPA', 'Franz Vohwinkel').
card_number('evacuation'/'DPA', '9').
card_flavor_text('evacuation'/'DPA', '\"Once I supply the breeze, you\'ll see your warriors for the smoke they truly are.\"\n—Alexi, zephyr mage').

card_in_set('eyeblight\'s ending', 'DPA').
card_original_type('eyeblight\'s ending'/'DPA', 'Tribal Instant — Elf').
card_original_text('eyeblight\'s ending'/'DPA', 'Destroy target non-Elf creature.').
card_image_name('eyeblight\'s ending'/'DPA', 'eyeblight\'s ending').
card_uid('eyeblight\'s ending'/'DPA', 'DPA:Eyeblight\'s Ending:eyeblight\'s ending').
card_rarity('eyeblight\'s ending'/'DPA', 'Common').
card_artist('eyeblight\'s ending'/'DPA', 'Ron Spears').
card_number('eyeblight\'s ending'/'DPA', '25').
card_flavor_text('eyeblight\'s ending'/'DPA', '\"Those without beauty are Lorwyn\'s greatest tumor. The winnowers have an unpleasant duty, but a necessary one.\"\n—Eidren, perfect of Lys Alana').

card_in_set('forest', 'DPA').
card_original_type('forest'/'DPA', 'Basic Land — Forest').
card_original_text('forest'/'DPA', 'G').
card_image_name('forest'/'DPA', 'forest1').
card_uid('forest'/'DPA', 'DPA:Forest:forest1').
card_rarity('forest'/'DPA', 'Basic Land').
card_artist('forest'/'DPA', 'Glen Angus').
card_number('forest'/'DPA', '110').

card_in_set('forest', 'DPA').
card_original_type('forest'/'DPA', 'Basic Land — Forest').
card_original_text('forest'/'DPA', 'G').
card_image_name('forest'/'DPA', 'forest2').
card_uid('forest'/'DPA', 'DPA:Forest:forest2').
card_rarity('forest'/'DPA', 'Basic Land').
card_artist('forest'/'DPA', 'John Avon').
card_number('forest'/'DPA', '111').

card_in_set('forest', 'DPA').
card_original_type('forest'/'DPA', 'Basic Land — Forest').
card_original_text('forest'/'DPA', 'G').
card_image_name('forest'/'DPA', 'forest3').
card_uid('forest'/'DPA', 'DPA:Forest:forest3').
card_rarity('forest'/'DPA', 'Basic Land').
card_artist('forest'/'DPA', 'Steven Belledin').
card_number('forest'/'DPA', '112').

card_in_set('forest', 'DPA').
card_original_type('forest'/'DPA', 'Basic Land — Forest').
card_original_text('forest'/'DPA', 'G').
card_image_name('forest'/'DPA', 'forest4').
card_uid('forest'/'DPA', 'DPA:Forest:forest4').
card_rarity('forest'/'DPA', 'Basic Land').
card_artist('forest'/'DPA', 'Jim Nelson').
card_number('forest'/'DPA', '113').

card_in_set('furnace of rath', 'DPA').
card_original_type('furnace of rath'/'DPA', 'Enchantment').
card_original_text('furnace of rath'/'DPA', 'If a source would deal damage to a creature or player, it deals double that damage to that creature or player instead.').
card_image_name('furnace of rath'/'DPA', 'furnace of rath').
card_uid('furnace of rath'/'DPA', 'DPA:Furnace of Rath:furnace of rath').
card_rarity('furnace of rath'/'DPA', 'Rare').
card_artist('furnace of rath'/'DPA', 'John Matson').
card_number('furnace of rath'/'DPA', '44').
card_flavor_text('furnace of rath'/'DPA', 'The furnace awaits the next master who would stoke the fires of apocalypse.').

card_in_set('gaea\'s herald', 'DPA').
card_original_type('gaea\'s herald'/'DPA', 'Creature — Elf').
card_original_text('gaea\'s herald'/'DPA', 'Creature spells can\'t be countered.').
card_image_name('gaea\'s herald'/'DPA', 'gaea\'s herald').
card_uid('gaea\'s herald'/'DPA', 'DPA:Gaea\'s Herald:gaea\'s herald').
card_rarity('gaea\'s herald'/'DPA', 'Rare').
card_artist('gaea\'s herald'/'DPA', 'Jim Murray').
card_number('gaea\'s herald'/'DPA', '65').
card_flavor_text('gaea\'s herald'/'DPA', '\"Gaea sings with the voice of nature rampant: a thousand howls, chitters, and cries, and none of them can be ignored.\"').

card_in_set('giant growth', 'DPA').
card_original_type('giant growth'/'DPA', 'Instant').
card_original_text('giant growth'/'DPA', 'Target creature gets +3/+3 until end of turn.').
card_image_name('giant growth'/'DPA', 'giant growth').
card_uid('giant growth'/'DPA', 'DPA:Giant Growth:giant growth').
card_rarity('giant growth'/'DPA', 'Common').
card_artist('giant growth'/'DPA', 'Matt Cavotta').
card_number('giant growth'/'DPA', '66').

card_in_set('giant spider', 'DPA').
card_original_type('giant spider'/'DPA', 'Creature — Spider').
card_original_text('giant spider'/'DPA', 'Reach (This creature can block creatures with flying.)').
card_image_name('giant spider'/'DPA', 'giant spider').
card_uid('giant spider'/'DPA', 'DPA:Giant Spider:giant spider').
card_rarity('giant spider'/'DPA', 'Common').
card_artist('giant spider'/'DPA', 'Randy Gallegos').
card_number('giant spider'/'DPA', '67').
card_flavor_text('giant spider'/'DPA', 'The giant spider\'s silk naturally resists gravity, almost weaving itself as it blankets the forest canopy.').

card_in_set('goblin piker', 'DPA').
card_original_type('goblin piker'/'DPA', 'Creature — Goblin Warrior').
card_original_text('goblin piker'/'DPA', '').
card_image_name('goblin piker'/'DPA', 'goblin piker').
card_uid('goblin piker'/'DPA', 'DPA:Goblin Piker:goblin piker').
card_rarity('goblin piker'/'DPA', 'Common').
card_artist('goblin piker'/'DPA', 'DiTerlizzi').
card_number('goblin piker'/'DPA', '45').
card_flavor_text('goblin piker'/'DPA', 'Once he\'d worked out which end of the thing was sharp, he was promoted to guard duty.').

card_in_set('goblin sky raider', 'DPA').
card_original_type('goblin sky raider'/'DPA', 'Creature — Goblin Warrior').
card_original_text('goblin sky raider'/'DPA', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)').
card_image_name('goblin sky raider'/'DPA', 'goblin sky raider').
card_uid('goblin sky raider'/'DPA', 'DPA:Goblin Sky Raider:goblin sky raider').
card_rarity('goblin sky raider'/'DPA', 'Common').
card_artist('goblin sky raider'/'DPA', 'Daren Bader').
card_number('goblin sky raider'/'DPA', '46').
card_flavor_text('goblin sky raider'/'DPA', 'The goblin word for \"flying\" is more accurately translated as \"falling slowly.\"').

card_in_set('greenweaver druid', 'DPA').
card_original_type('greenweaver druid'/'DPA', 'Creature — Elf Druid').
card_original_text('greenweaver druid'/'DPA', '{T}: Add {G}{G} to your mana pool.').
card_image_name('greenweaver druid'/'DPA', 'greenweaver druid').
card_uid('greenweaver druid'/'DPA', 'DPA:Greenweaver Druid:greenweaver druid').
card_rarity('greenweaver druid'/'DPA', 'Uncommon').
card_artist('greenweaver druid'/'DPA', 'Justin Sweet').
card_number('greenweaver druid'/'DPA', '68').
card_flavor_text('greenweaver druid'/'DPA', 'The other tribes call them fanatics, but none deny that the Mul Daya elves have an iron-strong bond to some force greater than themselves.').

card_in_set('hill giant', 'DPA').
card_original_type('hill giant'/'DPA', 'Creature — Giant').
card_original_text('hill giant'/'DPA', '').
card_image_name('hill giant'/'DPA', 'hill giant').
card_uid('hill giant'/'DPA', 'DPA:Hill Giant:hill giant').
card_rarity('hill giant'/'DPA', 'Common').
card_artist('hill giant'/'DPA', 'Kev Walker').
card_number('hill giant'/'DPA', '47').
card_flavor_text('hill giant'/'DPA', 'Fortunately, hill giants have large blind spots in which a human can easily hide. Unfortunately, these blind spots are beneath the bottoms of their feet.').

card_in_set('howl of the night pack', 'DPA').
card_original_type('howl of the night pack'/'DPA', 'Sorcery').
card_original_text('howl of the night pack'/'DPA', 'Put a 2/2 green Wolf creature token onto the battlefield for each Forest you control.').
card_image_name('howl of the night pack'/'DPA', 'howl of the night pack').
card_uid('howl of the night pack'/'DPA', 'DPA:Howl of the Night Pack:howl of the night pack').
card_rarity('howl of the night pack'/'DPA', 'Uncommon').
card_artist('howl of the night pack'/'DPA', 'Lars Grant-West').
card_number('howl of the night pack'/'DPA', '69').
card_flavor_text('howl of the night pack'/'DPA', 'The murderous horrors of Raven\'s Run are legendary, but even that haunted place goes quiet when the night wolves howl.').

card_in_set('immaculate magistrate', 'DPA').
card_original_type('immaculate magistrate'/'DPA', 'Creature — Elf Shaman').
card_original_text('immaculate magistrate'/'DPA', '{T}: Put a +1/+1 counter on target creature for each Elf you control.').
card_image_name('immaculate magistrate'/'DPA', 'immaculate magistrate').
card_uid('immaculate magistrate'/'DPA', 'DPA:Immaculate Magistrate:immaculate magistrate').
card_rarity('immaculate magistrate'/'DPA', 'Rare').
card_artist('immaculate magistrate'/'DPA', 'Jim Nelson').
card_number('immaculate magistrate'/'DPA', '70').
card_flavor_text('immaculate magistrate'/'DPA', 'Elves of the immaculate class weave flora into living creatures—sometimes to endorse an elite warrior, sometimes to create a breathing work of art.').

card_in_set('imperious perfect', 'DPA').
card_original_type('imperious perfect'/'DPA', 'Creature — Elf Warrior').
card_original_text('imperious perfect'/'DPA', 'Other Elf creatures you control get +1/+1.\n{G}, {T}: Put a 1/1 green Elf Warrior creature token into play.').
card_image_name('imperious perfect'/'DPA', 'imperious perfect').
card_uid('imperious perfect'/'DPA', 'DPA:Imperious Perfect:imperious perfect').
card_rarity('imperious perfect'/'DPA', 'Uncommon').
card_artist('imperious perfect'/'DPA', 'Scott M. Fischer').
card_number('imperious perfect'/'DPA', '71').
card_flavor_text('imperious perfect'/'DPA', 'In a culture of beauty, the most beautiful are worshipped as gods.').

card_in_set('incinerate', 'DPA').
card_original_type('incinerate'/'DPA', 'Instant').
card_original_text('incinerate'/'DPA', 'Incinerate deals 3 damage to target creature or player. A creature dealt damage this way can\'t be regenerated this turn.').
card_image_name('incinerate'/'DPA', 'incinerate').
card_uid('incinerate'/'DPA', 'DPA:Incinerate:incinerate').
card_rarity('incinerate'/'DPA', 'Common').
card_artist('incinerate'/'DPA', 'Zoltan Boros & Gabor Szikszai').
card_number('incinerate'/'DPA', '48').
card_flavor_text('incinerate'/'DPA', '\"Yes, I think ‘toast\' is an appropriate description.\"\n—Jaya Ballard, task mage').

card_in_set('island', 'DPA').
card_original_type('island'/'DPA', 'Basic Land — Island').
card_original_text('island'/'DPA', 'U').
card_image_name('island'/'DPA', 'island1').
card_uid('island'/'DPA', 'DPA:Island:island1').
card_rarity('island'/'DPA', 'Basic Land').
card_artist('island'/'DPA', 'Rob Alexander').
card_number('island'/'DPA', '98').

card_in_set('island', 'DPA').
card_original_type('island'/'DPA', 'Basic Land — Island').
card_original_text('island'/'DPA', 'U').
card_image_name('island'/'DPA', 'island2').
card_uid('island'/'DPA', 'DPA:Island:island2').
card_rarity('island'/'DPA', 'Basic Land').
card_artist('island'/'DPA', 'John Avon').
card_number('island'/'DPA', '99').

card_in_set('island', 'DPA').
card_original_type('island'/'DPA', 'Basic Land — Island').
card_original_text('island'/'DPA', 'U').
card_image_name('island'/'DPA', 'island3').
card_uid('island'/'DPA', 'DPA:Island:island3').
card_rarity('island'/'DPA', 'Basic Land').
card_artist('island'/'DPA', 'Scott Bailey').
card_number('island'/'DPA', '100').

card_in_set('island', 'DPA').
card_original_type('island'/'DPA', 'Basic Land — Island').
card_original_text('island'/'DPA', 'U').
card_image_name('island'/'DPA', 'island4').
card_uid('island'/'DPA', 'DPA:Island:island4').
card_rarity('island'/'DPA', 'Basic Land').
card_artist('island'/'DPA', 'Fred Fields').
card_number('island'/'DPA', '101').

card_in_set('jagged-scar archers', 'DPA').
card_original_type('jagged-scar archers'/'DPA', 'Creature — Elf Archer').
card_original_text('jagged-scar archers'/'DPA', 'Jagged-Scar Archers\'s power and toughness are each equal to the number of Elves you control.\n{T}: Jagged-Scar Archers deals damage equal to its power to target creature with flying.').
card_image_name('jagged-scar archers'/'DPA', 'jagged-scar archers').
card_uid('jagged-scar archers'/'DPA', 'DPA:Jagged-Scar Archers:jagged-scar archers').
card_rarity('jagged-scar archers'/'DPA', 'Uncommon').
card_artist('jagged-scar archers'/'DPA', 'Paolo Parente').
card_number('jagged-scar archers'/'DPA', '72').

card_in_set('kamahl, pit fighter', 'DPA').
card_original_type('kamahl, pit fighter'/'DPA', 'Legendary Creature — Human Barbarian').
card_original_text('kamahl, pit fighter'/'DPA', 'Haste (This creature can attack and {T} as soon as it comes under your control.)\n{T}: Kamahl, Pit Fighter deals 3 damage to target creature or player.').
card_image_name('kamahl, pit fighter'/'DPA', 'kamahl, pit fighter').
card_uid('kamahl, pit fighter'/'DPA', 'DPA:Kamahl, Pit Fighter:kamahl, pit fighter').
card_rarity('kamahl, pit fighter'/'DPA', 'Rare').
card_artist('kamahl, pit fighter'/'DPA', 'Kev Walker').
card_number('kamahl, pit fighter'/'DPA', '49').
card_flavor_text('kamahl, pit fighter'/'DPA', 'In times when freedom seems lost, great souls arise to reclaim it.').

card_in_set('kraken\'s eye', 'DPA').
card_original_type('kraken\'s eye'/'DPA', 'Artifact').
card_original_text('kraken\'s eye'/'DPA', 'Whenever a player casts a blue spell, you may gain 1 life.').
card_image_name('kraken\'s eye'/'DPA', 'kraken\'s eye').
card_uid('kraken\'s eye'/'DPA', 'DPA:Kraken\'s Eye:kraken\'s eye').
card_rarity('kraken\'s eye'/'DPA', 'Uncommon').
card_artist('kraken\'s eye'/'DPA', 'Alan Pollack').
card_number('kraken\'s eye'/'DPA', '93').
card_flavor_text('kraken\'s eye'/'DPA', 'Bright as a mirror, dark as the sea.').

card_in_set('lightning elemental', 'DPA').
card_original_type('lightning elemental'/'DPA', 'Creature — Elemental').
card_original_text('lightning elemental'/'DPA', 'Haste (This creature can attack and {T} as soon as it comes under your control.)').
card_image_name('lightning elemental'/'DPA', 'lightning elemental').
card_uid('lightning elemental'/'DPA', 'DPA:Lightning Elemental:lightning elemental').
card_rarity('lightning elemental'/'DPA', 'Common').
card_artist('lightning elemental'/'DPA', 'Kev Walker').
card_number('lightning elemental'/'DPA', '50').
card_flavor_text('lightning elemental'/'DPA', '\"A flash of the lightning, a break of the wave,\nHe passes from life to his rest in the grave.\"\n—William Knox, \"Mortality\"').

card_in_set('loxodon warhammer', 'DPA').
card_original_type('loxodon warhammer'/'DPA', 'Artifact — Equipment').
card_original_text('loxodon warhammer'/'DPA', 'Equipped creature gets +3/+0 and has lifelink and trample. (When it deals damage, you gain that much life. If it would deal enough combat damage to its blockers to destroy them, you may have it deal the rest of its damage to defending player.)\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery.)').
card_image_name('loxodon warhammer'/'DPA', 'loxodon warhammer').
card_uid('loxodon warhammer'/'DPA', 'DPA:Loxodon Warhammer:loxodon warhammer').
card_rarity('loxodon warhammer'/'DPA', 'Rare').
card_artist('loxodon warhammer'/'DPA', 'Jeremy Jarvis').
card_number('loxodon warhammer'/'DPA', '94').

card_in_set('lys alana huntmaster', 'DPA').
card_original_type('lys alana huntmaster'/'DPA', 'Creature — Elf Warrior').
card_original_text('lys alana huntmaster'/'DPA', 'Whenever you play an Elf spell, you may put a 1/1 green Elf Warrior creature token into play.').
card_image_name('lys alana huntmaster'/'DPA', 'lys alana huntmaster').
card_uid('lys alana huntmaster'/'DPA', 'DPA:Lys Alana Huntmaster:lys alana huntmaster').
card_rarity('lys alana huntmaster'/'DPA', 'Common').
card_artist('lys alana huntmaster'/'DPA', 'Pete Venters').
card_number('lys alana huntmaster'/'DPA', '73').
card_flavor_text('lys alana huntmaster'/'DPA', 'From the highest tiers of Dawn\'s Light Palace to the deepest shade of Wren\'s Run, the silver notes of the horn shimmer through the air, and all who hear it feel its pull.').

card_in_set('mahamoti djinn', 'DPA').
card_original_type('mahamoti djinn'/'DPA', 'Creature — Djinn').
card_original_text('mahamoti djinn'/'DPA', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)').
card_image_name('mahamoti djinn'/'DPA', 'mahamoti djinn').
card_uid('mahamoti djinn'/'DPA', 'DPA:Mahamoti Djinn:mahamoti djinn').
card_rarity('mahamoti djinn'/'DPA', 'Rare').
card_artist('mahamoti djinn'/'DPA', 'Greg Staples').
card_number('mahamoti djinn'/'DPA', '10').
card_flavor_text('mahamoti djinn'/'DPA', 'Of royal blood among the spirits of the air, the Mahamoti djinn rides on the wings of the winds. As dangerous in the gambling hall as he is in battle, he is a master of trickery and misdirection.').

card_in_set('megrim', 'DPA').
card_original_type('megrim'/'DPA', 'Enchantment').
card_original_text('megrim'/'DPA', 'Whenever an opponent discards a card, Megrim deals 2 damage to that player.').
card_image_name('megrim'/'DPA', 'megrim').
card_uid('megrim'/'DPA', 'DPA:Megrim:megrim').
card_rarity('megrim'/'DPA', 'Uncommon').
card_artist('megrim'/'DPA', 'Nick Percival').
card_number('megrim'/'DPA', '26').
card_flavor_text('megrim'/'DPA', '\"It\'s a vicious circle. Stress breeds pain. Pain breeds stress. It\'s simply beautiful, wouldn\'t you agree?\"\n—Vradeen, vampire nocturnus').

card_in_set('mind control', 'DPA').
card_original_type('mind control'/'DPA', 'Enchantment — Aura').
card_original_text('mind control'/'DPA', 'Enchant creature\nYou control enchanted creature.').
card_image_name('mind control'/'DPA', 'mind control').
card_uid('mind control'/'DPA', 'DPA:Mind Control:mind control').
card_rarity('mind control'/'DPA', 'Uncommon').
card_artist('mind control'/'DPA', 'Ryan Pancoast').
card_number('mind control'/'DPA', '11').
card_flavor_text('mind control'/'DPA', '\"Why fight the body when you can dominate the mind that rules it?\"\n—Jace Beleren').

card_in_set('mind rot', 'DPA').
card_original_type('mind rot'/'DPA', 'Sorcery').
card_original_text('mind rot'/'DPA', 'Target player discards two cards.').
card_image_name('mind rot'/'DPA', 'mind rot').
card_uid('mind rot'/'DPA', 'DPA:Mind Rot:mind rot').
card_rarity('mind rot'/'DPA', 'Common').
card_artist('mind rot'/'DPA', 'Steve Luke').
card_number('mind rot'/'DPA', '27').
card_flavor_text('mind rot'/'DPA', '\"The beauty of mental attacks is that your victims never remember them.\"\n—Volrath').

card_in_set('mind shatter', 'DPA').
card_original_type('mind shatter'/'DPA', 'Sorcery').
card_original_text('mind shatter'/'DPA', 'Target player discards X cards at random.').
card_image_name('mind shatter'/'DPA', 'mind shatter').
card_uid('mind shatter'/'DPA', 'DPA:Mind Shatter:mind shatter').
card_rarity('mind shatter'/'DPA', 'Rare').
card_artist('mind shatter'/'DPA', 'Michael Sutfin').
card_number('mind shatter'/'DPA', '28').
card_flavor_text('mind shatter'/'DPA', 'Dark thoughts hatch and twist within the mind, straining to take wing.').

card_in_set('mind spring', 'DPA').
card_original_type('mind spring'/'DPA', 'Sorcery').
card_original_text('mind spring'/'DPA', 'Draw X cards.').
card_image_name('mind spring'/'DPA', 'mind spring').
card_uid('mind spring'/'DPA', 'DPA:Mind Spring:mind spring').
card_rarity('mind spring'/'DPA', 'Rare').
card_artist('mind spring'/'DPA', 'Mark Zug').
card_number('mind spring'/'DPA', '12').
card_flavor_text('mind spring'/'DPA', 'Fragments of thought refract and multiply, surging in a geyser of dizzying insight.').

card_in_set('molimo, maro-sorcerer', 'DPA').
card_original_type('molimo, maro-sorcerer'/'DPA', 'Legendary Creature — Elemental').
card_original_text('molimo, maro-sorcerer'/'DPA', 'Trample (If this creature would deal enough combat damage to its blockers to destroy them, you may have it deal the rest of its damage to defending player.)\nMolimo, Maro-Sorcerer\'s power and toughness are each equal to the number of lands you control.').
card_image_name('molimo, maro-sorcerer'/'DPA', 'molimo, maro-sorcerer').
card_uid('molimo, maro-sorcerer'/'DPA', 'DPA:Molimo, Maro-Sorcerer:molimo, maro-sorcerer').
card_rarity('molimo, maro-sorcerer'/'DPA', 'Rare').
card_artist('molimo, maro-sorcerer'/'DPA', 'Mark Zug').
card_number('molimo, maro-sorcerer'/'DPA', '74').

card_in_set('moonglove winnower', 'DPA').
card_original_type('moonglove winnower'/'DPA', 'Creature — Elf Rogue').
card_original_text('moonglove winnower'/'DPA', 'Deathtouch (Whenever this creature deals damage to a creature, destroy that creature.)').
card_image_name('moonglove winnower'/'DPA', 'moonglove winnower').
card_uid('moonglove winnower'/'DPA', 'DPA:Moonglove Winnower:moonglove winnower').
card_rarity('moonglove winnower'/'DPA', 'Common').
card_artist('moonglove winnower'/'DPA', 'William O\'Connor').
card_number('moonglove winnower'/'DPA', '29').
card_flavor_text('moonglove winnower'/'DPA', 'Winnowers live to eliminate eyeblights, creatures the elves deem too ugly to exist.').

card_in_set('mortivore', 'DPA').
card_original_type('mortivore'/'DPA', 'Creature — Lhurgoyf').
card_original_text('mortivore'/'DPA', 'Mortivore\'s power and toughness are each equal to the number of creature cards in all graveyards.\n{B}: Regenerate Mortivore. (The next time this creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_image_name('mortivore'/'DPA', 'mortivore').
card_uid('mortivore'/'DPA', 'DPA:Mortivore:mortivore').
card_rarity('mortivore'/'DPA', 'Rare').
card_artist('mortivore'/'DPA', 'Anthony S. Waters').
card_number('mortivore'/'DPA', '30').

card_in_set('mountain', 'DPA').
card_original_type('mountain'/'DPA', 'Basic Land — Mountain').
card_original_text('mountain'/'DPA', 'R').
card_image_name('mountain'/'DPA', 'mountain1').
card_uid('mountain'/'DPA', 'DPA:Mountain:mountain1').
card_rarity('mountain'/'DPA', 'Basic Land').
card_artist('mountain'/'DPA', 'Rob Alexander').
card_number('mountain'/'DPA', '106').

card_in_set('mountain', 'DPA').
card_original_type('mountain'/'DPA', 'Basic Land — Mountain').
card_original_text('mountain'/'DPA', 'R').
card_image_name('mountain'/'DPA', 'mountain2').
card_uid('mountain'/'DPA', 'DPA:Mountain:mountain2').
card_rarity('mountain'/'DPA', 'Basic Land').
card_artist('mountain'/'DPA', 'Nils Hamm').
card_number('mountain'/'DPA', '107').

card_in_set('mountain', 'DPA').
card_original_type('mountain'/'DPA', 'Basic Land — Mountain').
card_original_text('mountain'/'DPA', 'R').
card_image_name('mountain'/'DPA', 'mountain3').
card_uid('mountain'/'DPA', 'DPA:Mountain:mountain3').
card_rarity('mountain'/'DPA', 'Basic Land').
card_artist('mountain'/'DPA', 'Karl Kopinski').
card_number('mountain'/'DPA', '108').

card_in_set('mountain', 'DPA').
card_original_type('mountain'/'DPA', 'Basic Land — Mountain').
card_original_text('mountain'/'DPA', 'R').
card_image_name('mountain'/'DPA', 'mountain4').
card_uid('mountain'/'DPA', 'DPA:Mountain:mountain4').
card_rarity('mountain'/'DPA', 'Basic Land').
card_artist('mountain'/'DPA', 'Sam Wood').
card_number('mountain'/'DPA', '109').

card_in_set('natural spring', 'DPA').
card_original_type('natural spring'/'DPA', 'Sorcery').
card_original_text('natural spring'/'DPA', 'Target player gains 8 life.').
card_image_name('natural spring'/'DPA', 'natural spring').
card_uid('natural spring'/'DPA', 'DPA:Natural Spring:natural spring').
card_rarity('natural spring'/'DPA', 'Common').
card_artist('natural spring'/'DPA', 'Jeffrey R. Busch').
card_number('natural spring'/'DPA', '75').
card_flavor_text('natural spring'/'DPA', '\"Jewels cannot be eaten nor gold drunk. What civilization trades, nature simply provides.\"\n—Molimo, maro-sorcerer').

card_in_set('naturalize', 'DPA').
card_original_type('naturalize'/'DPA', 'Instant').
card_original_text('naturalize'/'DPA', 'Destroy target artifact or enchantment.').
card_image_name('naturalize'/'DPA', 'naturalize').
card_uid('naturalize'/'DPA', 'DPA:Naturalize:naturalize').
card_rarity('naturalize'/'DPA', 'Common').
card_artist('naturalize'/'DPA', 'Tim Hildebrandt').
card_number('naturalize'/'DPA', '76').
card_flavor_text('naturalize'/'DPA', 'If you want to destroy something lifeless, make it live.').

card_in_set('nature\'s spiral', 'DPA').
card_original_type('nature\'s spiral'/'DPA', 'Sorcery').
card_original_text('nature\'s spiral'/'DPA', 'Return target permanent card from your graveyard to your hand. (A permanent card is an artifact, creature, enchantment, land, or planeswalker card.)').
card_image_name('nature\'s spiral'/'DPA', 'nature\'s spiral').
card_uid('nature\'s spiral'/'DPA', 'DPA:Nature\'s Spiral:nature\'s spiral').
card_rarity('nature\'s spiral'/'DPA', 'Uncommon').
card_artist('nature\'s spiral'/'DPA', 'Terese Nielsen').
card_number('nature\'s spiral'/'DPA', '77').
card_flavor_text('nature\'s spiral'/'DPA', 'Like nature, the fern spirals back on itself, eternally seeking its own center.').

card_in_set('negate', 'DPA').
card_original_type('negate'/'DPA', 'Instant').
card_original_text('negate'/'DPA', 'Counter target noncreature spell.').
card_image_name('negate'/'DPA', 'negate').
card_uid('negate'/'DPA', 'DPA:Negate:negate').
card_rarity('negate'/'DPA', 'Common').
card_artist('negate'/'DPA', 'Jeremy Jarvis').
card_number('negate'/'DPA', '13').
card_flavor_text('negate'/'DPA', 'Masters of the arcane savor a delicious irony. Their study of deep and complex arcana leads to such a simple end: the ability to say merely yes or no.').

card_in_set('overrun', 'DPA').
card_original_type('overrun'/'DPA', 'Sorcery').
card_original_text('overrun'/'DPA', 'Creatures you control get +3/+3 and gain trample until end of turn. (If a creature you control would deal enough damage to its blockers to destroy them, you may have it deal the rest of its damage to defending player or planeswalker.)').
card_image_name('overrun'/'DPA', 'overrun').
card_uid('overrun'/'DPA', 'DPA:Overrun:overrun').
card_rarity('overrun'/'DPA', 'Uncommon').
card_artist('overrun'/'DPA', 'Carl Critchlow').
card_number('overrun'/'DPA', '78').

card_in_set('phantom warrior', 'DPA').
card_original_type('phantom warrior'/'DPA', 'Creature — Illusion Warrior').
card_original_text('phantom warrior'/'DPA', 'Phantom Warrior is unblockable.').
card_image_name('phantom warrior'/'DPA', 'phantom warrior').
card_uid('phantom warrior'/'DPA', 'DPA:Phantom Warrior:phantom warrior').
card_rarity('phantom warrior'/'DPA', 'Uncommon').
card_artist('phantom warrior'/'DPA', 'Greg Staples').
card_number('phantom warrior'/'DPA', '14').
card_flavor_text('phantom warrior'/'DPA', '\"The construction of a defense is not accomplished by adding bricks.\"\n—Jace Beleren').

card_in_set('prodigal pyromancer', 'DPA').
card_original_type('prodigal pyromancer'/'DPA', 'Creature — Human Wizard').
card_original_text('prodigal pyromancer'/'DPA', '{T}: Prodigal Pyromancer deals 1 damage to target creature or player.').
card_image_name('prodigal pyromancer'/'DPA', 'prodigal pyromancer').
card_uid('prodigal pyromancer'/'DPA', 'DPA:Prodigal Pyromancer:prodigal pyromancer').
card_rarity('prodigal pyromancer'/'DPA', 'Uncommon').
card_artist('prodigal pyromancer'/'DPA', 'Jeremy Jarvis').
card_number('prodigal pyromancer'/'DPA', '51').
card_flavor_text('prodigal pyromancer'/'DPA', '\"What am I looking at? Ashes, dead man.\"').

card_in_set('rage reflection', 'DPA').
card_original_type('rage reflection'/'DPA', 'Enchantment').
card_original_text('rage reflection'/'DPA', 'Creatures you control have double strike.').
card_image_name('rage reflection'/'DPA', 'rage reflection').
card_uid('rage reflection'/'DPA', 'DPA:Rage Reflection:rage reflection').
card_rarity('rage reflection'/'DPA', 'Rare').
card_artist('rage reflection'/'DPA', 'Terese Nielsen & Ron Spencer').
card_number('rage reflection'/'DPA', '52').
card_flavor_text('rage reflection'/'DPA', 'Vengeance is a dish best served twice.').

card_in_set('rampant growth', 'DPA').
card_original_type('rampant growth'/'DPA', 'Sorcery').
card_original_text('rampant growth'/'DPA', 'Search your library for a basic land card and put that card onto the battlefield tapped. Then shuffle your library.').
card_image_name('rampant growth'/'DPA', 'rampant growth').
card_uid('rampant growth'/'DPA', 'DPA:Rampant Growth:rampant growth').
card_rarity('rampant growth'/'DPA', 'Common').
card_artist('rampant growth'/'DPA', 'Steven Belledin').
card_number('rampant growth'/'DPA', '79').
card_flavor_text('rampant growth'/'DPA', 'Nature grows solutions to its problems.').

card_in_set('ravenous rats', 'DPA').
card_original_type('ravenous rats'/'DPA', 'Creature — Rat').
card_original_text('ravenous rats'/'DPA', 'When Ravenous Rats comes into play, target opponent discards a card.').
card_image_name('ravenous rats'/'DPA', 'ravenous rats').
card_uid('ravenous rats'/'DPA', 'DPA:Ravenous Rats:ravenous rats').
card_rarity('ravenous rats'/'DPA', 'Common').
card_artist('ravenous rats'/'DPA', 'Carl Critchlow').
card_number('ravenous rats'/'DPA', '31').
card_flavor_text('ravenous rats'/'DPA', 'Nothing is sacred to rats. Everything is simply another meal.').

card_in_set('river boa', 'DPA').
card_original_type('river boa'/'DPA', 'Creature — Snake').
card_original_text('river boa'/'DPA', 'Islandwalk (This creature is unblockable if defending player controls an island.)\n{G} Regenerate River Boa.').
card_image_name('river boa'/'DPA', 'river boa').
card_uid('river boa'/'DPA', 'DPA:River Boa:river boa').
card_rarity('river boa'/'DPA', 'Uncommon').
card_artist('river boa'/'DPA', 'Paul Bonner').
card_number('river boa'/'DPA', '80').

card_in_set('roughshod mentor', 'DPA').
card_original_type('roughshod mentor'/'DPA', 'Creature — Giant Warrior').
card_original_text('roughshod mentor'/'DPA', 'Green creatures you control have trample.').
card_image_name('roughshod mentor'/'DPA', 'roughshod mentor').
card_uid('roughshod mentor'/'DPA', 'DPA:Roughshod Mentor:roughshod mentor').
card_rarity('roughshod mentor'/'DPA', 'Uncommon').
card_artist('roughshod mentor'/'DPA', 'Steven Belledin').
card_number('roughshod mentor'/'DPA', '81').

card_in_set('runeclaw bear', 'DPA').
card_original_type('runeclaw bear'/'DPA', 'Creature — Bear').
card_original_text('runeclaw bear'/'DPA', '').
card_image_name('runeclaw bear'/'DPA', 'runeclaw bear').
card_uid('runeclaw bear'/'DPA', 'DPA:Runeclaw Bear:runeclaw bear').
card_rarity('runeclaw bear'/'DPA', 'Common').
card_artist('runeclaw bear'/'DPA', 'Jesper Ejsing').
card_number('runeclaw bear'/'DPA', '82').
card_flavor_text('runeclaw bear'/'DPA', 'Bears aren\'t always as strong and as mean as you imagine. Some are even stronger and meaner.').

card_in_set('sengir vampire', 'DPA').
card_original_type('sengir vampire'/'DPA', 'Creature — Vampire').
card_original_text('sengir vampire'/'DPA', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nWhenever a creature dealt damage by Sengir Vampire this turn is put into a graveyard, put a +1/+1 counter on Sengir Vampire.').
card_image_name('sengir vampire'/'DPA', 'sengir vampire').
card_uid('sengir vampire'/'DPA', 'DPA:Sengir Vampire:sengir vampire').
card_rarity('sengir vampire'/'DPA', 'Rare').
card_artist('sengir vampire'/'DPA', 'Kev Walker').
card_number('sengir vampire'/'DPA', '32').
card_flavor_text('sengir vampire'/'DPA', 'Empires rise and fall, but evil is eternal.').

card_in_set('severed legion', 'DPA').
card_original_type('severed legion'/'DPA', 'Creature — Zombie').
card_original_text('severed legion'/'DPA', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)').
card_image_name('severed legion'/'DPA', 'severed legion').
card_uid('severed legion'/'DPA', 'DPA:Severed Legion:severed legion').
card_rarity('severed legion'/'DPA', 'Common').
card_artist('severed legion'/'DPA', 'Dany Orizio').
card_number('severed legion'/'DPA', '33').
card_flavor_text('severed legion'/'DPA', 'No one in Aphetto answers a knock at the door after sundown.').

card_in_set('shivan dragon', 'DPA').
card_original_type('shivan dragon'/'DPA', 'Creature — Dragon').
card_original_text('shivan dragon'/'DPA', 'Flying\n{R}: Shivan Dragon gets +1/+0 until end of turn.').
card_image_name('shivan dragon'/'DPA', 'shivan dragon').
card_uid('shivan dragon'/'DPA', 'DPA:Shivan Dragon:shivan dragon').
card_rarity('shivan dragon'/'DPA', 'Rare').
card_artist('shivan dragon'/'DPA', 'Donato Giancola').
card_number('shivan dragon'/'DPA', '53').
card_flavor_text('shivan dragon'/'DPA', 'The undisputed master of the mountains of Shiv.').

card_in_set('shock', 'DPA').
card_original_type('shock'/'DPA', 'Instant').
card_original_text('shock'/'DPA', 'Shock deals 2 damage to target creature or player.').
card_image_name('shock'/'DPA', 'shock').
card_uid('shock'/'DPA', 'DPA:Shock:shock').
card_rarity('shock'/'DPA', 'Common').
card_artist('shock'/'DPA', 'Jon Foster').
card_number('shock'/'DPA', '54').
card_flavor_text('shock'/'DPA', '\"Once I saw a goblin shouting at a thunderstorm. He lost the argument pretty quickly.\"\n—Wandering mage').

card_in_set('snapping drake', 'DPA').
card_original_type('snapping drake'/'DPA', 'Creature — Drake').
card_original_text('snapping drake'/'DPA', 'Flying').
card_image_name('snapping drake'/'DPA', 'snapping drake').
card_uid('snapping drake'/'DPA', 'DPA:Snapping Drake:snapping drake').
card_rarity('snapping drake'/'DPA', 'Common').
card_artist('snapping drake'/'DPA', 'Todd Lockwood').
card_number('snapping drake'/'DPA', '15').
card_flavor_text('snapping drake'/'DPA', 'Foul-tempered, poorly trained, and mule-stubborn, the drake is the perfect test of the master\'s will.').

card_in_set('spined wurm', 'DPA').
card_original_type('spined wurm'/'DPA', 'Creature — Wurm').
card_original_text('spined wurm'/'DPA', '').
card_image_name('spined wurm'/'DPA', 'spined wurm').
card_uid('spined wurm'/'DPA', 'DPA:Spined Wurm:spined wurm').
card_rarity('spined wurm'/'DPA', 'Common').
card_artist('spined wurm'/'DPA', 'Carl Critchlow').
card_number('spined wurm'/'DPA', '83').
card_flavor_text('spined wurm'/'DPA', 'Its spines act like the teeth of a saw, scoring distinctive marks in the vegetation of its territory.').

card_in_set('swamp', 'DPA').
card_original_type('swamp'/'DPA', 'Basic Land — Swamp').
card_original_text('swamp'/'DPA', 'B').
card_image_name('swamp'/'DPA', 'swamp1').
card_uid('swamp'/'DPA', 'DPA:Swamp:swamp1').
card_rarity('swamp'/'DPA', 'Basic Land').
card_artist('swamp'/'DPA', 'Dan Frazier').
card_number('swamp'/'DPA', '102').

card_in_set('swamp', 'DPA').
card_original_type('swamp'/'DPA', 'Basic Land — Swamp').
card_original_text('swamp'/'DPA', 'B').
card_image_name('swamp'/'DPA', 'swamp2').
card_uid('swamp'/'DPA', 'DPA:Swamp:swamp2').
card_rarity('swamp'/'DPA', 'Basic Land').
card_artist('swamp'/'DPA', 'Alex Horley-Orlandelli').
card_number('swamp'/'DPA', '103').

card_in_set('swamp', 'DPA').
card_original_type('swamp'/'DPA', 'Basic Land — Swamp').
card_original_text('swamp'/'DPA', 'B').
card_image_name('swamp'/'DPA', 'swamp3').
card_uid('swamp'/'DPA', 'DPA:Swamp:swamp3').
card_rarity('swamp'/'DPA', 'Basic Land').
card_artist('swamp'/'DPA', 'Jim Pavelec').
card_number('swamp'/'DPA', '104').

card_in_set('swamp', 'DPA').
card_original_type('swamp'/'DPA', 'Basic Land — Swamp').
card_original_text('swamp'/'DPA', 'B').
card_image_name('swamp'/'DPA', 'swamp4').
card_uid('swamp'/'DPA', 'DPA:Swamp:swamp4').
card_rarity('swamp'/'DPA', 'Basic Land').
card_artist('swamp'/'DPA', 'Alan Pollack').
card_number('swamp'/'DPA', '105').

card_in_set('talara\'s battalion', 'DPA').
card_original_type('talara\'s battalion'/'DPA', 'Creature — Elf Warrior').
card_original_text('talara\'s battalion'/'DPA', 'Trample\nPlay Talara\'s Battalion only if you played another green spell this turn.').
card_image_name('talara\'s battalion'/'DPA', 'talara\'s battalion').
card_uid('talara\'s battalion'/'DPA', 'DPA:Talara\'s Battalion:talara\'s battalion').
card_rarity('talara\'s battalion'/'DPA', 'Rare').
card_artist('talara\'s battalion'/'DPA', 'Todd Lockwood').
card_number('talara\'s battalion'/'DPA', '84').

card_in_set('terror', 'DPA').
card_original_type('terror'/'DPA', 'Instant').
card_original_text('terror'/'DPA', 'Destroy target nonartifact, nonblack creature. It can\'t be regenerated.').
card_image_name('terror'/'DPA', 'terror').
card_uid('terror'/'DPA', 'DPA:Terror:terror').
card_rarity('terror'/'DPA', 'Common').
card_artist('terror'/'DPA', 'Adam Rex').
card_number('terror'/'DPA', '34').

card_in_set('the rack', 'DPA').
card_original_type('the rack'/'DPA', 'Artifact').
card_original_text('the rack'/'DPA', 'At the end of target opponent\'s upkeep, The Rack deals that player 1 damage for each card in his or her hand fewer than three.').
card_image_name('the rack'/'DPA', 'the rack').
card_uid('the rack'/'DPA', 'DPA:The Rack:the rack').
card_rarity('the rack'/'DPA', 'Uncommon').
card_artist('the rack'/'DPA', 'Nic Klein').
card_number('the rack'/'DPA', '95').

card_in_set('thieving magpie', 'DPA').
card_original_type('thieving magpie'/'DPA', 'Creature — Bird').
card_original_text('thieving magpie'/'DPA', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.) \nWhenever Thieving Magpie deals damage to an opponent, you draw a card.').
card_image_name('thieving magpie'/'DPA', 'thieving magpie').
card_uid('thieving magpie'/'DPA', 'DPA:Thieving Magpie:thieving magpie').
card_rarity('thieving magpie'/'DPA', 'Uncommon').
card_artist('thieving magpie'/'DPA', 'Una Fricker').
card_number('thieving magpie'/'DPA', '16').
card_flavor_text('thieving magpie'/'DPA', 'Other birds collect twigs for their nests. Magpies collect jewels for theirs.').

card_in_set('trained armodon', 'DPA').
card_original_type('trained armodon'/'DPA', 'Creature — Elephant').
card_original_text('trained armodon'/'DPA', '').
card_image_name('trained armodon'/'DPA', 'trained armodon').
card_uid('trained armodon'/'DPA', 'DPA:Trained Armodon:trained armodon').
card_rarity('trained armodon'/'DPA', 'Common').
card_artist('trained armodon'/'DPA', 'Gary Leach').
card_number('trained armodon'/'DPA', '85').
card_flavor_text('trained armodon'/'DPA', 'Armodons are trained to step on things. Enemy things.').

card_in_set('troll ascetic', 'DPA').
card_original_type('troll ascetic'/'DPA', 'Creature — Troll Shaman').
card_original_text('troll ascetic'/'DPA', 'Troll Ascetic can\'t be the target of spells or abilities your opponents control.\n{1}{G}: Regenerate Troll Ascetic. (The next time this creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_image_name('troll ascetic'/'DPA', 'troll ascetic').
card_uid('troll ascetic'/'DPA', 'DPA:Troll Ascetic:troll ascetic').
card_rarity('troll ascetic'/'DPA', 'Rare').
card_artist('troll ascetic'/'DPA', 'Puddnhead').
card_number('troll ascetic'/'DPA', '86').

card_in_set('underworld dreams', 'DPA').
card_original_type('underworld dreams'/'DPA', 'Enchantment').
card_original_text('underworld dreams'/'DPA', 'Whenever an opponent draws a card, Underworld Dreams deals 1 damage to him or her.').
card_image_name('underworld dreams'/'DPA', 'underworld dreams').
card_uid('underworld dreams'/'DPA', 'DPA:Underworld Dreams:underworld dreams').
card_rarity('underworld dreams'/'DPA', 'Rare').
card_artist('underworld dreams'/'DPA', 'Carl Critchlow').
card_number('underworld dreams'/'DPA', '35').
card_flavor_text('underworld dreams'/'DPA', '\"In the drowsy dark cave of the mind, dreams build their nest with fragments dropped from day\'s caravan.\"\n—Rabindranath Tagore').

card_in_set('unholy strength', 'DPA').
card_original_type('unholy strength'/'DPA', 'Enchantment — Aura').
card_original_text('unholy strength'/'DPA', 'Enchant creature\nEnchanted creature gets +2/+1.').
card_image_name('unholy strength'/'DPA', 'unholy strength').
card_uid('unholy strength'/'DPA', 'DPA:Unholy Strength:unholy strength').
card_rarity('unholy strength'/'DPA', 'Common').
card_artist('unholy strength'/'DPA', 'Terese Nielsen').
card_number('unholy strength'/'DPA', '36').
card_flavor_text('unholy strength'/'DPA', '\"Born under the moon, the second child will stumble on temptation and seek power in the dark places of the heart.\"\n—Codex of the Constellari').

card_in_set('unsummon', 'DPA').
card_original_type('unsummon'/'DPA', 'Instant').
card_original_text('unsummon'/'DPA', 'Return target creature to its owner\'s hand.').
card_image_name('unsummon'/'DPA', 'unsummon').
card_uid('unsummon'/'DPA', 'DPA:Unsummon:unsummon').
card_rarity('unsummon'/'DPA', 'Common').
card_artist('unsummon'/'DPA', 'Izzy').
card_number('unsummon'/'DPA', '17').
card_flavor_text('unsummon'/'DPA', 'Mages routinely summon creatures from the Æther. It\'s only fair that they occasionally throw one back.').

card_in_set('verdant force', 'DPA').
card_original_type('verdant force'/'DPA', 'Creature — Elemental').
card_original_text('verdant force'/'DPA', 'At the beginning of each upkeep, put a 1/1 green Saproling creature token into play under your control.').
card_image_name('verdant force'/'DPA', 'verdant force').
card_uid('verdant force'/'DPA', 'DPA:Verdant Force:verdant force').
card_rarity('verdant force'/'DPA', 'Rare').
card_artist('verdant force'/'DPA', 'DiTerlizzi').
card_number('verdant force'/'DPA', '87').
card_flavor_text('verdant force'/'DPA', 'Left to itself, nature overflows any container, overthrows any restriction, and overreaches any boundary.').

card_in_set('vigor', 'DPA').
card_original_type('vigor'/'DPA', 'Creature — Elemental Incarnation').
card_original_text('vigor'/'DPA', 'Trample\nIf damage would be dealt to a creature you control other than Vigor, prevent that damage. Put a +1/+1 counter on that creature for each 1 damage prevented this way.\nWhen Vigor is put into a graveyard from anywhere, shuffle it into its owner\'s library.').
card_image_name('vigor'/'DPA', 'vigor').
card_uid('vigor'/'DPA', 'DPA:Vigor:vigor').
card_rarity('vigor'/'DPA', 'Rare').
card_artist('vigor'/'DPA', 'Jim Murray').
card_number('vigor'/'DPA', '88').

card_in_set('wall of spears', 'DPA').
card_original_type('wall of spears'/'DPA', 'Artifact Creature — Wall').
card_original_text('wall of spears'/'DPA', '(Walls can\'t attack.)\nFirst strike (This creature deals combat damage before creatures without first strike.)').
card_image_name('wall of spears'/'DPA', 'wall of spears').
card_uid('wall of spears'/'DPA', 'DPA:Wall of Spears:wall of spears').
card_rarity('wall of spears'/'DPA', 'Common').
card_artist('wall of spears'/'DPA', 'Christopher Moeller').
card_number('wall of spears'/'DPA', '96').
card_flavor_text('wall of spears'/'DPA', '\"A stick is your friend. A pointed stick is your good friend. An army of pointed sticks is your best friend.\"\n—Onean sergeant').

card_in_set('wall of wood', 'DPA').
card_original_type('wall of wood'/'DPA', 'Creature — Wall').
card_original_text('wall of wood'/'DPA', 'Defender (This creature can\'t attack.)').
card_image_name('wall of wood'/'DPA', 'wall of wood').
card_uid('wall of wood'/'DPA', 'DPA:Wall of Wood:wall of wood').
card_rarity('wall of wood'/'DPA', 'Common').
card_artist('wall of wood'/'DPA', 'Rebecca Guay').
card_number('wall of wood'/'DPA', '89').
card_flavor_text('wall of wood'/'DPA', 'Orcish lumberjacks spent the night sharpening their blades and resting for a day of labor. They awoke to find that the forest had been making its own preparations.').

card_in_set('wurm\'s tooth', 'DPA').
card_original_type('wurm\'s tooth'/'DPA', 'Artifact').
card_original_text('wurm\'s tooth'/'DPA', 'Whenever a player casts a green spell, you may gain 1 life.').
card_image_name('wurm\'s tooth'/'DPA', 'wurm\'s tooth').
card_uid('wurm\'s tooth'/'DPA', 'DPA:Wurm\'s Tooth:wurm\'s tooth').
card_rarity('wurm\'s tooth'/'DPA', 'Uncommon').
card_artist('wurm\'s tooth'/'DPA', 'Alan Pollack').
card_number('wurm\'s tooth'/'DPA', '97').
card_flavor_text('wurm\'s tooth'/'DPA', 'A wurm knows nothing of deception. If it opens its mouth, it plans to eat you.').
