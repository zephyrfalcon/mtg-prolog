% Worldwake

set('WWK').
set_name('WWK', 'Worldwake').
set_release_date('WWK', '2010-02-05').
set_border('WWK', 'black').
set_type('WWK', 'expansion').
set_block('WWK', 'Zendikar').

card_in_set('abyssal persecutor', 'WWK').
card_original_type('abyssal persecutor'/'WWK', 'Creature — Demon').
card_original_text('abyssal persecutor'/'WWK', 'Flying, trample\nYou can\'t win the game and your opponents can\'t lose the game.').
card_first_print('abyssal persecutor', 'WWK').
card_image_name('abyssal persecutor'/'WWK', 'abyssal persecutor').
card_uid('abyssal persecutor'/'WWK', 'WWK:Abyssal Persecutor:abyssal persecutor').
card_rarity('abyssal persecutor'/'WWK', 'Mythic Rare').
card_artist('abyssal persecutor'/'WWK', 'Chippy').
card_number('abyssal persecutor'/'WWK', '47').
card_flavor_text('abyssal persecutor'/'WWK', 'His slaves crave death more than they desire freedom. He denies them both.').
card_multiverse_id('abyssal persecutor'/'WWK', '197869').

card_in_set('admonition angel', 'WWK').
card_original_type('admonition angel'/'WWK', 'Creature — Angel').
card_original_text('admonition angel'/'WWK', 'Flying\nLandfall — Whenever a land enters the battlefield under your control, you may exile target nonland permanent other than Admonition Angel.\nWhen Admonition Angel leaves the battlefield, return all cards exiled with it to the battlefield under their owners\' control.').
card_first_print('admonition angel', 'WWK').
card_image_name('admonition angel'/'WWK', 'admonition angel').
card_uid('admonition angel'/'WWK', 'WWK:Admonition Angel:admonition angel').
card_rarity('admonition angel'/'WWK', 'Mythic Rare').
card_artist('admonition angel'/'WWK', 'Steve Argyle').
card_number('admonition angel'/'WWK', '1').
card_multiverse_id('admonition angel'/'WWK', '197160').

card_in_set('æther tradewinds', 'WWK').
card_original_type('æther tradewinds'/'WWK', 'Instant').
card_original_text('æther tradewinds'/'WWK', 'Return target permanent you control and target permanent you don\'t control to their owners\' hands.').
card_first_print('æther tradewinds', 'WWK').
card_image_name('æther tradewinds'/'WWK', 'aether tradewinds').
card_uid('æther tradewinds'/'WWK', 'WWK:Æther Tradewinds:aether tradewinds').
card_rarity('æther tradewinds'/'WWK', 'Common').
card_artist('æther tradewinds'/'WWK', 'Kieran Yanner').
card_number('æther tradewinds'/'WWK', '24').
card_flavor_text('æther tradewinds'/'WWK', '\"The wind smells of misfortune. Check your knots.\"\n—Anitan, Ondu cleric').
card_multiverse_id('æther tradewinds'/'WWK', '201568').

card_in_set('agadeem occultist', 'WWK').
card_original_type('agadeem occultist'/'WWK', 'Creature — Human Shaman Ally').
card_original_text('agadeem occultist'/'WWK', '{T}: Put target creature card from an opponent\'s graveyard onto the battlefield under your control if its converted mana cost is less than or equal to the number of Allies you control.').
card_first_print('agadeem occultist', 'WWK').
card_image_name('agadeem occultist'/'WWK', 'agadeem occultist').
card_uid('agadeem occultist'/'WWK', 'WWK:Agadeem Occultist:agadeem occultist').
card_rarity('agadeem occultist'/'WWK', 'Rare').
card_artist('agadeem occultist'/'WWK', 'Vance Kovacs').
card_number('agadeem occultist'/'WWK', '48').
card_multiverse_id('agadeem occultist'/'WWK', '198402').

card_in_set('akoum battlesinger', 'WWK').
card_original_type('akoum battlesinger'/'WWK', 'Creature — Human Berserker Ally').
card_original_text('akoum battlesinger'/'WWK', 'Haste\nWhenever Akoum Battlesinger or another Ally enters the battlefield under your control, you may have Ally creatures you control get +1/+0 until end of turn.').
card_first_print('akoum battlesinger', 'WWK').
card_image_name('akoum battlesinger'/'WWK', 'akoum battlesinger').
card_uid('akoum battlesinger'/'WWK', 'WWK:Akoum Battlesinger:akoum battlesinger').
card_rarity('akoum battlesinger'/'WWK', 'Common').
card_artist('akoum battlesinger'/'WWK', 'Adi Granov').
card_number('akoum battlesinger'/'WWK', '71').
card_multiverse_id('akoum battlesinger'/'WWK', '198360').

card_in_set('amulet of vigor', 'WWK').
card_original_type('amulet of vigor'/'WWK', 'Artifact').
card_original_text('amulet of vigor'/'WWK', 'Whenever a permanent enters the battlefield tapped and under your control, untap it.').
card_first_print('amulet of vigor', 'WWK').
card_image_name('amulet of vigor'/'WWK', 'amulet of vigor').
card_uid('amulet of vigor'/'WWK', 'WWK:Amulet of Vigor:amulet of vigor').
card_rarity('amulet of vigor'/'WWK', 'Rare').
card_artist('amulet of vigor'/'WWK', 'Warren Mahy').
card_number('amulet of vigor'/'WWK', '121').
card_flavor_text('amulet of vigor'/'WWK', '\"After years of study, I\'ve learned an important lesson: the relics we watch may be watching us back.\"\n—Anowon, the Ruin Sage').
card_multiverse_id('amulet of vigor'/'WWK', '191577').

card_in_set('anowon, the ruin sage', 'WWK').
card_original_type('anowon, the ruin sage'/'WWK', 'Legendary Creature — Vampire Shaman').
card_original_text('anowon, the ruin sage'/'WWK', 'At the beginning of your upkeep, each player sacrifices a non-Vampire creature.').
card_first_print('anowon, the ruin sage', 'WWK').
card_image_name('anowon, the ruin sage'/'WWK', 'anowon, the ruin sage').
card_uid('anowon, the ruin sage'/'WWK', 'WWK:Anowon, the Ruin Sage:anowon, the ruin sage').
card_rarity('anowon, the ruin sage'/'WWK', 'Rare').
card_artist('anowon, the ruin sage'/'WWK', 'Dan Scott').
card_number('anowon, the ruin sage'/'WWK', '49').
card_flavor_text('anowon, the ruin sage'/'WWK', '\"So many have died in search of that map. And now it appears in the hands of the arrogant child Chandra Nalaar.\"').
card_multiverse_id('anowon, the ruin sage'/'WWK', '177508').

card_in_set('apex hawks', 'WWK').
card_original_type('apex hawks'/'WWK', 'Creature — Bird').
card_original_text('apex hawks'/'WWK', 'Multikicker {1}{W} (You may pay an additional {1}{W} any number of times as you cast this spell.)\nFlying\nApex Hawks enters the battlefield with a +1/+1 counter on it for each time it was kicked.').
card_first_print('apex hawks', 'WWK').
card_image_name('apex hawks'/'WWK', 'apex hawks').
card_uid('apex hawks'/'WWK', 'WWK:Apex Hawks:apex hawks').
card_rarity('apex hawks'/'WWK', 'Common').
card_artist('apex hawks'/'WWK', 'David Palumbo').
card_number('apex hawks'/'WWK', '2').
card_multiverse_id('apex hawks'/'WWK', '197769').

card_in_set('arbor elf', 'WWK').
card_original_type('arbor elf'/'WWK', 'Creature — Elf Druid').
card_original_text('arbor elf'/'WWK', '{T}: Untap target Forest.').
card_first_print('arbor elf', 'WWK').
card_image_name('arbor elf'/'WWK', 'arbor elf').
card_uid('arbor elf'/'WWK', 'WWK:Arbor Elf:arbor elf').
card_rarity('arbor elf'/'WWK', 'Common').
card_artist('arbor elf'/'WWK', 'rk post').
card_number('arbor elf'/'WWK', '95').
card_flavor_text('arbor elf'/'WWK', 'The Mul Daya elves reject their Tajuru kin, calling them arrogant tree-binders who think the roots serve the canopy.').
card_multiverse_id('arbor elf'/'WWK', '197959').

card_in_set('archon of redemption', 'WWK').
card_original_type('archon of redemption'/'WWK', 'Creature — Archon').
card_original_text('archon of redemption'/'WWK', 'Flying\nWhenever Archon of Redemption or another creature with flying enters the battlefield under your control, you may gain life equal to that creature\'s power.').
card_first_print('archon of redemption', 'WWK').
card_image_name('archon of redemption'/'WWK', 'archon of redemption').
card_uid('archon of redemption'/'WWK', 'WWK:Archon of Redemption:archon of redemption').
card_rarity('archon of redemption'/'WWK', 'Rare').
card_artist('archon of redemption'/'WWK', 'Steven Belledin').
card_number('archon of redemption'/'WWK', '3').
card_flavor_text('archon of redemption'/'WWK', 'Until Emeria is freed, he raises his sword in her name.').
card_multiverse_id('archon of redemption'/'WWK', '201575').

card_in_set('avenger of zendikar', 'WWK').
card_original_type('avenger of zendikar'/'WWK', 'Creature — Elemental').
card_original_text('avenger of zendikar'/'WWK', 'When Avenger of Zendikar enters the battlefield, put a 0/1 green Plant creature token onto the battlefield for each land you control.\nLandfall — Whenever a land enters the battlefield under your control, you may put a +1/+1 counter on each Plant creature you control.').
card_first_print('avenger of zendikar', 'WWK').
card_image_name('avenger of zendikar'/'WWK', 'avenger of zendikar').
card_uid('avenger of zendikar'/'WWK', 'WWK:Avenger of Zendikar:avenger of zendikar').
card_rarity('avenger of zendikar'/'WWK', 'Mythic Rare').
card_artist('avenger of zendikar'/'WWK', 'Zoltan Boros & Gabor Szikszai').
card_number('avenger of zendikar'/'WWK', '96').
card_multiverse_id('avenger of zendikar'/'WWK', '201570').

card_in_set('basilisk collar', 'WWK').
card_original_type('basilisk collar'/'WWK', 'Artifact — Equipment').
card_original_text('basilisk collar'/'WWK', 'Equipped creature has deathtouch and lifelink.\nEquip {2}').
card_first_print('basilisk collar', 'WWK').
card_image_name('basilisk collar'/'WWK', 'basilisk collar').
card_uid('basilisk collar'/'WWK', 'WWK:Basilisk Collar:basilisk collar').
card_rarity('basilisk collar'/'WWK', 'Rare').
card_artist('basilisk collar'/'WWK', 'Howard Lyon').
card_number('basilisk collar'/'WWK', '122').
card_flavor_text('basilisk collar'/'WWK', 'During their endless travels, the mages of the Goma Fada caravan have learned ways to harness both life and death.').
card_multiverse_id('basilisk collar'/'WWK', '198356').

card_in_set('battle hurda', 'WWK').
card_original_type('battle hurda'/'WWK', 'Creature — Giant').
card_original_text('battle hurda'/'WWK', 'First strike').
card_first_print('battle hurda', 'WWK').
card_image_name('battle hurda'/'WWK', 'battle hurda').
card_uid('battle hurda'/'WWK', 'WWK:Battle Hurda:battle hurda').
card_rarity('battle hurda'/'WWK', 'Common').
card_artist('battle hurda'/'WWK', 'Christopher Moeller').
card_number('battle hurda'/'WWK', '4').
card_flavor_text('battle hurda'/'WWK', '\"Murasan hurdas were too aggressive to tolerate a harness, so we put them on guard duty. Now the bandits can\'t get within spitting distance of us.\"\n—Bruse Tarl, Goma Fada nomad').
card_multiverse_id('battle hurda'/'WWK', '198394').

card_in_set('bazaar trader', 'WWK').
card_original_type('bazaar trader'/'WWK', 'Creature — Goblin').
card_original_text('bazaar trader'/'WWK', '{T}: Target player gains control of target artifact, creature, or land you control.').
card_first_print('bazaar trader', 'WWK').
card_image_name('bazaar trader'/'WWK', 'bazaar trader').
card_uid('bazaar trader'/'WWK', 'WWK:Bazaar Trader:bazaar trader').
card_rarity('bazaar trader'/'WWK', 'Rare').
card_artist('bazaar trader'/'WWK', 'Matt Cavotta').
card_number('bazaar trader'/'WWK', '72').
card_flavor_text('bazaar trader'/'WWK', '\"No need for a hurda? How about these maps to recently discovered tombs? Or these vials of invisible potion? Very fancy . . .\"').
card_multiverse_id('bazaar trader'/'WWK', '198364').

card_in_set('bestial menace', 'WWK').
card_original_type('bestial menace'/'WWK', 'Sorcery').
card_original_text('bestial menace'/'WWK', 'Put a 1/1 green Snake creature token, a 2/2 green Wolf creature token, and a 3/3 green Elephant creature token onto the battlefield.').
card_first_print('bestial menace', 'WWK').
card_image_name('bestial menace'/'WWK', 'bestial menace').
card_uid('bestial menace'/'WWK', 'WWK:Bestial Menace:bestial menace').
card_rarity('bestial menace'/'WWK', 'Uncommon').
card_artist('bestial menace'/'WWK', 'Andrew Robinson').
card_number('bestial menace'/'WWK', '97').
card_flavor_text('bestial menace'/'WWK', '\"My battle cry reaches ears far keener than yours.\"\n—Saidah, Joraga hunter').
card_multiverse_id('bestial menace'/'WWK', '197843').

card_in_set('bloodhusk ritualist', 'WWK').
card_original_type('bloodhusk ritualist'/'WWK', 'Creature — Vampire Shaman').
card_original_text('bloodhusk ritualist'/'WWK', 'Multikicker {B} (You may pay an additional {B} any number of times as you cast this spell.)\nWhen Bloodhusk Ritualist enters the battlefield, target opponent discards a card for each time it was kicked.').
card_first_print('bloodhusk ritualist', 'WWK').
card_image_name('bloodhusk ritualist'/'WWK', 'bloodhusk ritualist').
card_uid('bloodhusk ritualist'/'WWK', 'WWK:Bloodhusk Ritualist:bloodhusk ritualist').
card_rarity('bloodhusk ritualist'/'WWK', 'Uncommon').
card_artist('bloodhusk ritualist'/'WWK', 'Daarken').
card_number('bloodhusk ritualist'/'WWK', '50').
card_multiverse_id('bloodhusk ritualist'/'WWK', '191570').

card_in_set('bojuka bog', 'WWK').
card_original_type('bojuka bog'/'WWK', 'Land').
card_original_text('bojuka bog'/'WWK', 'Bojuka Bog enters the battlefield tapped.\nWhen Bojuka Bog enters the battlefield, exile all cards from target player\'s graveyard.\n{T}: Add {B} to your mana pool.').
card_first_print('bojuka bog', 'WWK').
card_image_name('bojuka bog'/'WWK', 'bojuka bog').
card_uid('bojuka bog'/'WWK', 'WWK:Bojuka Bog:bojuka bog').
card_rarity('bojuka bog'/'WWK', 'Common').
card_artist('bojuka bog'/'WWK', 'Howard Lyon').
card_number('bojuka bog'/'WWK', '132').
card_multiverse_id('bojuka bog'/'WWK', '197786').

card_in_set('bojuka brigand', 'WWK').
card_original_type('bojuka brigand'/'WWK', 'Creature — Human Warrior Ally').
card_original_text('bojuka brigand'/'WWK', 'Bojuka Brigand can\'t block.\nWhenever Bojuka Brigand or another Ally enters the battlefield under your control, you may put a +1/+1 counter on Bojuka Brigand.').
card_first_print('bojuka brigand', 'WWK').
card_image_name('bojuka brigand'/'WWK', 'bojuka brigand').
card_uid('bojuka brigand'/'WWK', 'WWK:Bojuka Brigand:bojuka brigand').
card_rarity('bojuka brigand'/'WWK', 'Common').
card_artist('bojuka brigand'/'WWK', 'Johann Bodin').
card_number('bojuka brigand'/'WWK', '51').
card_flavor_text('bojuka brigand'/'WWK', '\"I don\'t do tactics. I don\'t do guard duty. I do damage. Take it or leave it.\"').
card_multiverse_id('bojuka brigand'/'WWK', '197159').

card_in_set('brink of disaster', 'WWK').
card_original_type('brink of disaster'/'WWK', 'Enchantment — Aura').
card_original_text('brink of disaster'/'WWK', 'Enchant creature or land\nWhen enchanted permanent becomes tapped, destroy it.').
card_first_print('brink of disaster', 'WWK').
card_image_name('brink of disaster'/'WWK', 'brink of disaster').
card_uid('brink of disaster'/'WWK', 'WWK:Brink of Disaster:brink of disaster').
card_rarity('brink of disaster'/'WWK', 'Common').
card_artist('brink of disaster'/'WWK', 'Alex Horley-Orlandelli').
card_number('brink of disaster'/'WWK', '52').
card_flavor_text('brink of disaster'/'WWK', '\"You always have a choice. That doesn\'t mean you always have a good choice.\"\n—Alubri, Guul Draz gatekeeper').
card_multiverse_id('brink of disaster'/'WWK', '201571').

card_in_set('bull rush', 'WWK').
card_original_type('bull rush'/'WWK', 'Instant').
card_original_text('bull rush'/'WWK', 'Target creature gets +2/+0 until end of turn.').
card_first_print('bull rush', 'WWK').
card_image_name('bull rush'/'WWK', 'bull rush').
card_uid('bull rush'/'WWK', 'WWK:Bull Rush:bull rush').
card_rarity('bull rush'/'WWK', 'Common').
card_artist('bull rush'/'WWK', 'Christopher Moeller').
card_number('bull rush'/'WWK', '73').
card_flavor_text('bull rush'/'WWK', '\"It\'s gone mad! Loose the chains and save what you can!\"').
card_multiverse_id('bull rush'/'WWK', '201560').

card_in_set('butcher of malakir', 'WWK').
card_original_type('butcher of malakir'/'WWK', 'Creature — Vampire Warrior').
card_original_text('butcher of malakir'/'WWK', 'Flying\nWhenever Butcher of Malakir or another creature you control is put into a graveyard from the battlefield, each opponent sacrifices a creature.').
card_first_print('butcher of malakir', 'WWK').
card_image_name('butcher of malakir'/'WWK', 'butcher of malakir').
card_uid('butcher of malakir'/'WWK', 'WWK:Butcher of Malakir:butcher of malakir').
card_rarity('butcher of malakir'/'WWK', 'Rare').
card_artist('butcher of malakir'/'WWK', 'Jason Chan').
card_number('butcher of malakir'/'WWK', '53').
card_flavor_text('butcher of malakir'/'WWK', 'His verdict is always guilty. His sentence is always death.').
card_multiverse_id('butcher of malakir'/'WWK', '197125').

card_in_set('calcite snapper', 'WWK').
card_original_type('calcite snapper'/'WWK', 'Creature — Turtle').
card_original_text('calcite snapper'/'WWK', 'Shroud (This creature can\'t be the target of spells or abilities.)\nLandfall — Whenever a land enters the battlefield under your control, you may switch Calcite Snapper\'s power and toughness until end of turn.').
card_first_print('calcite snapper', 'WWK').
card_image_name('calcite snapper'/'WWK', 'calcite snapper').
card_uid('calcite snapper'/'WWK', 'WWK:Calcite Snapper:calcite snapper').
card_rarity('calcite snapper'/'WWK', 'Common').
card_artist('calcite snapper'/'WWK', 'David Palumbo').
card_number('calcite snapper'/'WWK', '25').
card_multiverse_id('calcite snapper'/'WWK', '191569').

card_in_set('canopy cover', 'WWK').
card_original_type('canopy cover'/'WWK', 'Enchantment — Aura').
card_original_text('canopy cover'/'WWK', 'Enchant creature\nEnchanted creature can\'t be blocked except by creatures with flying or reach.\nEnchanted creature can\'t be the target of spells or abilities your opponents control.').
card_first_print('canopy cover', 'WWK').
card_image_name('canopy cover'/'WWK', 'canopy cover').
card_uid('canopy cover'/'WWK', 'WWK:Canopy Cover:canopy cover').
card_rarity('canopy cover'/'WWK', 'Uncommon').
card_artist('canopy cover'/'WWK', 'Igor Kieryluk').
card_number('canopy cover'/'WWK', '98').
card_multiverse_id('canopy cover'/'WWK', '193970').

card_in_set('caustic crawler', 'WWK').
card_original_type('caustic crawler'/'WWK', 'Creature — Insect').
card_original_text('caustic crawler'/'WWK', 'Landfall — Whenever a land enters the battlefield under your control, you may have target creature get -1/-1 until end of turn.').
card_first_print('caustic crawler', 'WWK').
card_image_name('caustic crawler'/'WWK', 'caustic crawler').
card_uid('caustic crawler'/'WWK', 'WWK:Caustic Crawler:caustic crawler').
card_rarity('caustic crawler'/'WWK', 'Uncommon').
card_artist('caustic crawler'/'WWK', 'Raymond Swanland').
card_number('caustic crawler'/'WWK', '54').
card_flavor_text('caustic crawler'/'WWK', 'Novice explorers can mistake the acid-carved stone lairs for ancient ruins. They rarely survive to learn better.').
card_multiverse_id('caustic crawler'/'WWK', '198376').

card_in_set('celestial colonnade', 'WWK').
card_original_type('celestial colonnade'/'WWK', 'Land').
card_original_text('celestial colonnade'/'WWK', 'Celestial Colonnade enters the battlefield tapped.\n{T}: Add {W} or {U} to your mana pool.\n{3}{W}{U}: Until end of turn, Celestial Colonnade becomes a 4/4 white and blue Elemental creature with flying and vigilance. It\'s still a land.').
card_image_name('celestial colonnade'/'WWK', 'celestial colonnade').
card_uid('celestial colonnade'/'WWK', 'WWK:Celestial Colonnade:celestial colonnade').
card_rarity('celestial colonnade'/'WWK', 'Rare').
card_artist('celestial colonnade'/'WWK', 'Eric Deschamps').
card_number('celestial colonnade'/'WWK', '133').
card_multiverse_id('celestial colonnade'/'WWK', '177545').

card_in_set('chain reaction', 'WWK').
card_original_type('chain reaction'/'WWK', 'Sorcery').
card_original_text('chain reaction'/'WWK', 'Chain Reaction deals X damage to each creature, where X is the number of creatures on the battlefield.').
card_first_print('chain reaction', 'WWK').
card_image_name('chain reaction'/'WWK', 'chain reaction').
card_uid('chain reaction'/'WWK', 'WWK:Chain Reaction:chain reaction').
card_rarity('chain reaction'/'WWK', 'Rare').
card_artist('chain reaction'/'WWK', 'Trevor Claxton').
card_number('chain reaction'/'WWK', '74').
card_flavor_text('chain reaction'/'WWK', '\"We train for the improbable, like lightning striking the same place twice, or striking everywhere at once.\"\n—Nundari, Sea Gate militia captain').
card_multiverse_id('chain reaction'/'WWK', '201573').

card_in_set('claws of valakut', 'WWK').
card_original_type('claws of valakut'/'WWK', 'Enchantment — Aura').
card_original_text('claws of valakut'/'WWK', 'Enchant creature\nEnchanted creature gets +1/+0 for each Mountain you control and has first strike.').
card_first_print('claws of valakut', 'WWK').
card_image_name('claws of valakut'/'WWK', 'claws of valakut').
card_uid('claws of valakut'/'WWK', 'WWK:Claws of Valakut:claws of valakut').
card_rarity('claws of valakut'/'WWK', 'Common').
card_artist('claws of valakut'/'WWK', 'Igor Kieryluk').
card_number('claws of valakut'/'WWK', '75').
card_flavor_text('claws of valakut'/'WWK', '\"Swords may sunder. Shields may fail. My will is stronger than steel, and my flesh and bone far more deadly.\"').
card_multiverse_id('claws of valakut'/'WWK', '193975').

card_in_set('comet storm', 'WWK').
card_original_type('comet storm'/'WWK', 'Instant').
card_original_text('comet storm'/'WWK', 'Multikicker {1} (You may pay an additional {1} any number of times as you cast this spell.)\nChoose target creature or player, then choose another target creature or player for each time Comet Storm was kicked. Comet Storm deals X damage to each of them.').
card_image_name('comet storm'/'WWK', 'comet storm').
card_uid('comet storm'/'WWK', 'WWK:Comet Storm:comet storm').
card_rarity('comet storm'/'WWK', 'Mythic Rare').
card_artist('comet storm'/'WWK', 'Jung Park').
card_number('comet storm'/'WWK', '76').
card_multiverse_id('comet storm'/'WWK', '197766').

card_in_set('corrupted zendikon', 'WWK').
card_original_type('corrupted zendikon'/'WWK', 'Enchantment — Aura').
card_original_text('corrupted zendikon'/'WWK', 'Enchant land\nEnchanted land is a 3/3 black Ooze creature. It\'s still a land.\nWhen enchanted land is put into a graveyard, return that card to its owner\'s hand.').
card_first_print('corrupted zendikon', 'WWK').
card_image_name('corrupted zendikon'/'WWK', 'corrupted zendikon').
card_uid('corrupted zendikon'/'WWK', 'WWK:Corrupted Zendikon:corrupted zendikon').
card_rarity('corrupted zendikon'/'WWK', 'Common').
card_artist('corrupted zendikon'/'WWK', 'John Avon').
card_number('corrupted zendikon'/'WWK', '55').
card_multiverse_id('corrupted zendikon'/'WWK', '197874').

card_in_set('cosi\'s ravager', 'WWK').
card_original_type('cosi\'s ravager'/'WWK', 'Creature — Elemental').
card_original_text('cosi\'s ravager'/'WWK', 'Landfall — Whenever a land enters the battlefield under your control, you may have Cosi\'s Ravager deal 1 damage to target player.').
card_first_print('cosi\'s ravager', 'WWK').
card_image_name('cosi\'s ravager'/'WWK', 'cosi\'s ravager').
card_uid('cosi\'s ravager'/'WWK', 'WWK:Cosi\'s Ravager:cosi\'s ravager').
card_rarity('cosi\'s ravager'/'WWK', 'Common').
card_artist('cosi\'s ravager'/'WWK', 'Zoltan Boros & Gabor Szikszai').
card_number('cosi\'s ravager'/'WWK', '77').
card_flavor_text('cosi\'s ravager'/'WWK', '\"Cosi has many followers. Even the volcano finds a way to do his bidding.\"\n—Zakken, Enclave cleric').
card_multiverse_id('cosi\'s ravager'/'WWK', '201565').

card_in_set('creeping tar pit', 'WWK').
card_original_type('creeping tar pit'/'WWK', 'Land').
card_original_text('creeping tar pit'/'WWK', 'Creeping Tar Pit enters the battlefield tapped.\n{T}: Add {U} or {B} to your mana pool.\n{1}{U}{B}: Until end of turn, Creeping Tar Pit becomes a 3/2 blue and black Elemental creature and is unblockable. It\'s still a land.').
card_first_print('creeping tar pit', 'WWK').
card_image_name('creeping tar pit'/'WWK', 'creeping tar pit').
card_uid('creeping tar pit'/'WWK', 'WWK:Creeping Tar Pit:creeping tar pit').
card_rarity('creeping tar pit'/'WWK', 'Rare').
card_artist('creeping tar pit'/'WWK', 'Jason Felix').
card_number('creeping tar pit'/'WWK', '134').
card_multiverse_id('creeping tar pit'/'WWK', '177520').

card_in_set('crusher zendikon', 'WWK').
card_original_type('crusher zendikon'/'WWK', 'Enchantment — Aura').
card_original_text('crusher zendikon'/'WWK', 'Enchant land\nEnchanted land is a 4/2 red Beast creature with trample. It\'s still a land.\nWhen enchanted land is put into a graveyard, return that card to its owner\'s hand.').
card_first_print('crusher zendikon', 'WWK').
card_image_name('crusher zendikon'/'WWK', 'crusher zendikon').
card_uid('crusher zendikon'/'WWK', 'WWK:Crusher Zendikon:crusher zendikon').
card_rarity('crusher zendikon'/'WWK', 'Common').
card_artist('crusher zendikon'/'WWK', 'Ryan Pancoast').
card_number('crusher zendikon'/'WWK', '78').
card_multiverse_id('crusher zendikon'/'WWK', '180416').

card_in_set('cunning sparkmage', 'WWK').
card_original_type('cunning sparkmage'/'WWK', 'Creature — Human Shaman').
card_original_text('cunning sparkmage'/'WWK', 'Haste\n{T}: Cunning Sparkmage deals 1 damage to target creature or player.').
card_first_print('cunning sparkmage', 'WWK').
card_image_name('cunning sparkmage'/'WWK', 'cunning sparkmage').
card_uid('cunning sparkmage'/'WWK', 'WWK:Cunning Sparkmage:cunning sparkmage').
card_rarity('cunning sparkmage'/'WWK', 'Uncommon').
card_artist('cunning sparkmage'/'WWK', 'Wayne Reynolds').
card_number('cunning sparkmage'/'WWK', '79').
card_flavor_text('cunning sparkmage'/'WWK', '\"I see the weaknesses you hide even from yourself.\"').
card_multiverse_id('cunning sparkmage'/'WWK', '201563').

card_in_set('dead reckoning', 'WWK').
card_original_type('dead reckoning'/'WWK', 'Sorcery').
card_original_text('dead reckoning'/'WWK', 'You may put target creature card from your graveyard on top of your library. If you do, Dead Reckoning deals damage equal to that card\'s power to target creature.').
card_first_print('dead reckoning', 'WWK').
card_image_name('dead reckoning'/'WWK', 'dead reckoning').
card_uid('dead reckoning'/'WWK', 'WWK:Dead Reckoning:dead reckoning').
card_rarity('dead reckoning'/'WWK', 'Common').
card_artist('dead reckoning'/'WWK', 'Svetlin Velinov').
card_number('dead reckoning'/'WWK', '56').
card_flavor_text('dead reckoning'/'WWK', 'Many venture out to hunt for treasures. Others lie in wait to hunt the hunters.').
card_multiverse_id('dead reckoning'/'WWK', '194679').

card_in_set('death\'s shadow', 'WWK').
card_original_type('death\'s shadow'/'WWK', 'Creature — Avatar').
card_original_text('death\'s shadow'/'WWK', 'Death\'s Shadow gets -X/-X, where X is your life total.').
card_first_print('death\'s shadow', 'WWK').
card_image_name('death\'s shadow'/'WWK', 'death\'s shadow').
card_uid('death\'s shadow'/'WWK', 'WWK:Death\'s Shadow:death\'s shadow').
card_rarity('death\'s shadow'/'WWK', 'Rare').
card_artist('death\'s shadow'/'WWK', 'Howard Lyon').
card_number('death\'s shadow'/'WWK', '57').
card_flavor_text('death\'s shadow'/'WWK', 'The shadow of the candle looms tall even as its light grows dim.').
card_multiverse_id('death\'s shadow'/'WWK', '198372').

card_in_set('deathforge shaman', 'WWK').
card_original_type('deathforge shaman'/'WWK', 'Creature — Ogre Shaman').
card_original_text('deathforge shaman'/'WWK', 'Multikicker {R} (You may pay an additional {R} any number of times as you cast this spell.)\nWhen Deathforge Shaman enters the battlefield, it deals damage to target player equal to twice the number of times it was kicked.').
card_first_print('deathforge shaman', 'WWK').
card_image_name('deathforge shaman'/'WWK', 'deathforge shaman').
card_uid('deathforge shaman'/'WWK', 'WWK:Deathforge Shaman:deathforge shaman').
card_rarity('deathforge shaman'/'WWK', 'Uncommon').
card_artist('deathforge shaman'/'WWK', 'Dave Kendall').
card_number('deathforge shaman'/'WWK', '80').
card_multiverse_id('deathforge shaman'/'WWK', '191540').

card_in_set('dispel', 'WWK').
card_original_type('dispel'/'WWK', 'Instant').
card_original_text('dispel'/'WWK', 'Counter target instant spell.').
card_first_print('dispel', 'WWK').
card_image_name('dispel'/'WWK', 'dispel').
card_uid('dispel'/'WWK', 'WWK:Dispel:dispel').
card_rarity('dispel'/'WWK', 'Common').
card_artist('dispel'/'WWK', 'Vance Kovacs').
card_number('dispel'/'WWK', '26').
card_flavor_text('dispel'/'WWK', 'The Jwari winds undo reality as easily as they scatter a pile of leaves.').
card_multiverse_id('dispel'/'WWK', '201562').

card_in_set('dragonmaster outcast', 'WWK').
card_original_type('dragonmaster outcast'/'WWK', 'Creature — Human Shaman').
card_original_text('dragonmaster outcast'/'WWK', 'At the beginning of your upkeep, if you control six or more lands, put a 5/5 red Dragon creature token with flying onto the battlefield.').
card_first_print('dragonmaster outcast', 'WWK').
card_image_name('dragonmaster outcast'/'WWK', 'dragonmaster outcast').
card_uid('dragonmaster outcast'/'WWK', 'WWK:Dragonmaster Outcast:dragonmaster outcast').
card_rarity('dragonmaster outcast'/'WWK', 'Mythic Rare').
card_artist('dragonmaster outcast'/'WWK', 'Raymond Swanland').
card_number('dragonmaster outcast'/'WWK', '81').
card_flavor_text('dragonmaster outcast'/'WWK', '\"Cast out by my tribe, I am a pariah trapped in this mammal\'s carcass. Only my true kin hear my cries.\"').
card_multiverse_id('dragonmaster outcast'/'WWK', '197854').

card_in_set('dread statuary', 'WWK').
card_original_type('dread statuary'/'WWK', 'Land').
card_original_text('dread statuary'/'WWK', '{T}: Add {1} to your mana pool.\n{4}: Dread Statuary becomes a 4/2 Golem artifact creature until end of turn. It\'s still a land.').
card_first_print('dread statuary', 'WWK').
card_image_name('dread statuary'/'WWK', 'dread statuary').
card_uid('dread statuary'/'WWK', 'WWK:Dread Statuary:dread statuary').
card_rarity('dread statuary'/'WWK', 'Uncommon').
card_artist('dread statuary'/'WWK', 'Jason A. Engle').
card_number('dread statuary'/'WWK', '135').
card_flavor_text('dread statuary'/'WWK', 'The last reliable landmark in Tazeem just walked away.').
card_multiverse_id('dread statuary'/'WWK', '205425').

card_in_set('enclave elite', 'WWK').
card_original_type('enclave elite'/'WWK', 'Creature — Merfolk Soldier').
card_original_text('enclave elite'/'WWK', 'Multikicker {1}{U} (You may pay an additional {1}{U} any number of times as you cast this spell.)\nIslandwalk\nEnclave Elite enters the battlefield with a +1/+1 counter on it for each time it was kicked.').
card_first_print('enclave elite', 'WWK').
card_image_name('enclave elite'/'WWK', 'enclave elite').
card_uid('enclave elite'/'WWK', 'WWK:Enclave Elite:enclave elite').
card_rarity('enclave elite'/'WWK', 'Common').
card_artist('enclave elite'/'WWK', 'Igor Kieryluk').
card_number('enclave elite'/'WWK', '27').
card_multiverse_id('enclave elite'/'WWK', '197767').

card_in_set('everflowing chalice', 'WWK').
card_original_type('everflowing chalice'/'WWK', 'Artifact').
card_original_text('everflowing chalice'/'WWK', 'Multikicker {2} (You may pay an additional {2} any number of times as you cast this spell.)\nEverflowing Chalice enters the battlefield with a charge counter on it for each time it was kicked.\n{T}: Add {1} to your mana pool for each charge counter on Everflowing Chalice.').
card_image_name('everflowing chalice'/'WWK', 'everflowing chalice').
card_uid('everflowing chalice'/'WWK', 'WWK:Everflowing Chalice:everflowing chalice').
card_rarity('everflowing chalice'/'WWK', 'Uncommon').
card_artist('everflowing chalice'/'WWK', 'Steve Argyle').
card_number('everflowing chalice'/'WWK', '123').
card_multiverse_id('everflowing chalice'/'WWK', '198374').

card_in_set('explore', 'WWK').
card_original_type('explore'/'WWK', 'Sorcery').
card_original_text('explore'/'WWK', 'You may play an additional land this turn.\nDraw a card.').
card_first_print('explore', 'WWK').
card_image_name('explore'/'WWK', 'explore').
card_uid('explore'/'WWK', 'WWK:Explore:explore').
card_rarity('explore'/'WWK', 'Common').
card_artist('explore'/'WWK', 'John Avon').
card_number('explore'/'WWK', '99').
card_flavor_text('explore'/'WWK', 'An explorer\'s reward is a view of tomorrow\'s possibilities.').
card_multiverse_id('explore'/'WWK', '201578').

card_in_set('eye of ugin', 'WWK').
card_original_type('eye of ugin'/'WWK', 'Legendary Land').
card_original_text('eye of ugin'/'WWK', 'Colorless Eldrazi spells you cast cost {2} less to cast.\n{7}, {T}: Search your library for a colorless creature card, reveal it, and put it into your hand. Then shuffle your library.').
card_first_print('eye of ugin', 'WWK').
card_image_name('eye of ugin'/'WWK', 'eye of ugin').
card_uid('eye of ugin'/'WWK', 'WWK:Eye of Ugin:eye of ugin').
card_rarity('eye of ugin'/'WWK', 'Mythic Rare').
card_artist('eye of ugin'/'WWK', 'James Paick').
card_number('eye of ugin'/'WWK', '136').
card_flavor_text('eye of ugin'/'WWK', 'An eye closes. A race awakens.').
card_multiverse_id('eye of ugin'/'WWK', '197881').

card_in_set('feral contest', 'WWK').
card_original_type('feral contest'/'WWK', 'Sorcery').
card_original_text('feral contest'/'WWK', 'Put a +1/+1 counter on target creature you control. Another target creature blocks it this turn if able.').
card_first_print('feral contest', 'WWK').
card_image_name('feral contest'/'WWK', 'feral contest').
card_uid('feral contest'/'WWK', 'WWK:Feral Contest:feral contest').
card_rarity('feral contest'/'WWK', 'Common').
card_artist('feral contest'/'WWK', 'Daarken').
card_number('feral contest'/'WWK', '100').
card_flavor_text('feral contest'/'WWK', 'The instinct for survival makes enemies of all.').
card_multiverse_id('feral contest'/'WWK', '198362').

card_in_set('fledgling griffin', 'WWK').
card_original_type('fledgling griffin'/'WWK', 'Creature — Griffin').
card_original_text('fledgling griffin'/'WWK', 'Landfall — Whenever a land enters the battlefield under your control, Fledgling Griffin gains flying until end of turn.').
card_first_print('fledgling griffin', 'WWK').
card_image_name('fledgling griffin'/'WWK', 'fledgling griffin').
card_uid('fledgling griffin'/'WWK', 'WWK:Fledgling Griffin:fledgling griffin').
card_rarity('fledgling griffin'/'WWK', 'Common').
card_artist('fledgling griffin'/'WWK', 'Christopher Moeller').
card_number('fledgling griffin'/'WWK', '5').
card_flavor_text('fledgling griffin'/'WWK', '\"We\'re making progress, but its desire to explore still far exceeds its ability.\"\n—Amadi, Halimar griffin-master').
card_multiverse_id('fledgling griffin'/'WWK', '191580').

card_in_set('gnarlid pack', 'WWK').
card_original_type('gnarlid pack'/'WWK', 'Creature — Beast').
card_original_text('gnarlid pack'/'WWK', 'Multikicker {1}{G} (You may pay an additional {1}{G} any number of times as you cast this spell.)\nGnarlid Pack enters the battlefield with a +1/+1 counter on it for each time it was kicked.').
card_first_print('gnarlid pack', 'WWK').
card_image_name('gnarlid pack'/'WWK', 'gnarlid pack').
card_uid('gnarlid pack'/'WWK', 'WWK:Gnarlid Pack:gnarlid pack').
card_rarity('gnarlid pack'/'WWK', 'Common').
card_artist('gnarlid pack'/'WWK', 'Johann Bodin').
card_number('gnarlid pack'/'WWK', '101').
card_multiverse_id('gnarlid pack'/'WWK', '197771').

card_in_set('goblin roughrider', 'WWK').
card_original_type('goblin roughrider'/'WWK', 'Creature — Goblin Knight').
card_original_text('goblin roughrider'/'WWK', '').
card_first_print('goblin roughrider', 'WWK').
card_image_name('goblin roughrider'/'WWK', 'goblin roughrider').
card_uid('goblin roughrider'/'WWK', 'WWK:Goblin Roughrider:goblin roughrider').
card_rarity('goblin roughrider'/'WWK', 'Common').
card_artist('goblin roughrider'/'WWK', 'Jesper Ejsing').
card_number('goblin roughrider'/'WWK', '82').
card_flavor_text('goblin roughrider'/'WWK', 'Astride the bucking creature, Gribble hurtled down the mountainside while his Grotag brethren cheered. It was at that moment that legend of the Skrill Tamer was born.').
card_multiverse_id('goblin roughrider'/'WWK', '194681').

card_in_set('goliath sphinx', 'WWK').
card_original_type('goliath sphinx'/'WWK', 'Creature — Sphinx').
card_original_text('goliath sphinx'/'WWK', 'Flying').
card_first_print('goliath sphinx', 'WWK').
card_image_name('goliath sphinx'/'WWK', 'goliath sphinx').
card_uid('goliath sphinx'/'WWK', 'WWK:Goliath Sphinx:goliath sphinx').
card_rarity('goliath sphinx'/'WWK', 'Rare').
card_artist('goliath sphinx'/'WWK', 'Greg Staples').
card_number('goliath sphinx'/'WWK', '28').
card_flavor_text('goliath sphinx'/'WWK', 'He makes his home on the tallest mountain in Sejiri, where the vista is as endless as his patience.').
card_multiverse_id('goliath sphinx'/'WWK', '197153').

card_in_set('grappler spider', 'WWK').
card_original_type('grappler spider'/'WWK', 'Creature — Spider').
card_original_text('grappler spider'/'WWK', 'Reach (This creature can block creatures with flying.)').
card_first_print('grappler spider', 'WWK').
card_image_name('grappler spider'/'WWK', 'grappler spider').
card_uid('grappler spider'/'WWK', 'WWK:Grappler Spider:grappler spider').
card_rarity('grappler spider'/'WWK', 'Common').
card_artist('grappler spider'/'WWK', 'Austin Hsu').
card_number('grappler spider'/'WWK', '102').
card_flavor_text('grappler spider'/'WWK', 'Creatures of the air wisely avoid the darkest regions of Guul Draz—which is why grappler spiders inhabit the few places touched by the sun\'s rays.').
card_multiverse_id('grappler spider'/'WWK', '198371').

card_in_set('graypelt hunter', 'WWK').
card_original_type('graypelt hunter'/'WWK', 'Creature — Human Warrior Ally').
card_original_text('graypelt hunter'/'WWK', 'Trample\nWhenever Graypelt Hunter or another Ally enters the battlefield under your control, you may put a +1/+1 counter on Graypelt Hunter.').
card_first_print('graypelt hunter', 'WWK').
card_image_name('graypelt hunter'/'WWK', 'graypelt hunter').
card_uid('graypelt hunter'/'WWK', 'WWK:Graypelt Hunter:graypelt hunter').
card_rarity('graypelt hunter'/'WWK', 'Common').
card_artist('graypelt hunter'/'WWK', 'Svetlin Velinov').
card_number('graypelt hunter'/'WWK', '103').
card_flavor_text('graypelt hunter'/'WWK', 'He\'ll cleave wood and bone alike to carve a path for his allies.').
card_multiverse_id('graypelt hunter'/'WWK', '193994').

card_in_set('grotag thrasher', 'WWK').
card_original_type('grotag thrasher'/'WWK', 'Creature — Lizard').
card_original_text('grotag thrasher'/'WWK', 'Whenever Grotag Thrasher attacks, target creature can\'t block this turn.').
card_first_print('grotag thrasher', 'WWK').
card_image_name('grotag thrasher'/'WWK', 'grotag thrasher').
card_uid('grotag thrasher'/'WWK', 'WWK:Grotag Thrasher:grotag thrasher').
card_rarity('grotag thrasher'/'WWK', 'Common').
card_artist('grotag thrasher'/'WWK', 'Jesper Ejsing').
card_number('grotag thrasher'/'WWK', '83').
card_flavor_text('grotag thrasher'/'WWK', 'The goblin fancied himself a rider. The lizard was happy with its new flail.').
card_multiverse_id('grotag thrasher'/'WWK', '191548').

card_in_set('groundswell', 'WWK').
card_original_type('groundswell'/'WWK', 'Instant').
card_original_text('groundswell'/'WWK', 'Target creature gets +2/+2 until end of turn.\nLandfall — If you had a land enter the battlefield under your control this turn, that creature gets +4/+4 until end of turn instead.').
card_first_print('groundswell', 'WWK').
card_image_name('groundswell'/'WWK', 'groundswell').
card_uid('groundswell'/'WWK', 'WWK:Groundswell:groundswell').
card_rarity('groundswell'/'WWK', 'Common').
card_artist('groundswell'/'WWK', 'Chris Rahn').
card_number('groundswell'/'WWK', '104').
card_flavor_text('groundswell'/'WWK', '\"This world will not be tamed.\"').
card_multiverse_id('groundswell'/'WWK', '193998').

card_in_set('guardian zendikon', 'WWK').
card_original_type('guardian zendikon'/'WWK', 'Enchantment — Aura').
card_original_text('guardian zendikon'/'WWK', 'Enchant land\nEnchanted land is a 2/6 white Wall creature with defender. It\'s still a land.\nWhen enchanted land is put into a graveyard, return that card to its owner\'s hand.').
card_first_print('guardian zendikon', 'WWK').
card_image_name('guardian zendikon'/'WWK', 'guardian zendikon').
card_uid('guardian zendikon'/'WWK', 'WWK:Guardian Zendikon:guardian zendikon').
card_rarity('guardian zendikon'/'WWK', 'Common').
card_artist('guardian zendikon'/'WWK', 'John Avon').
card_number('guardian zendikon'/'WWK', '6').
card_multiverse_id('guardian zendikon'/'WWK', '197872').

card_in_set('hada freeblade', 'WWK').
card_original_type('hada freeblade'/'WWK', 'Creature — Human Soldier Ally').
card_original_text('hada freeblade'/'WWK', 'Whenever Hada Freeblade or another Ally enters the battlefield under your control, you may put a +1/+1 counter on Hada Freeblade.').
card_image_name('hada freeblade'/'WWK', 'hada freeblade').
card_uid('hada freeblade'/'WWK', 'WWK:Hada Freeblade:hada freeblade').
card_rarity('hada freeblade'/'WWK', 'Uncommon').
card_artist('hada freeblade'/'WWK', 'Cyril Van Der Haegen').
card_number('hada freeblade'/'WWK', '7').
card_flavor_text('hada freeblade'/'WWK', '\"I toiled in Kazuul\'s mines when I was eight. I think I\'m tough enough for a little hiking.\"').
card_multiverse_id('hada freeblade'/'WWK', '198400').

card_in_set('halimar depths', 'WWK').
card_original_type('halimar depths'/'WWK', 'Land').
card_original_text('halimar depths'/'WWK', 'Halimar Depths enters the battlefield tapped.\nWhen Halimar Depths enters the battlefield, look at the top three cards of your library, then put them back in any order.\n{T}: Add {U} to your mana pool.').
card_first_print('halimar depths', 'WWK').
card_image_name('halimar depths'/'WWK', 'halimar depths').
card_uid('halimar depths'/'WWK', 'WWK:Halimar Depths:halimar depths').
card_rarity('halimar depths'/'WWK', 'Common').
card_artist('halimar depths'/'WWK', 'Volkan Baga').
card_number('halimar depths'/'WWK', '137').
card_multiverse_id('halimar depths'/'WWK', '197787').

card_in_set('halimar excavator', 'WWK').
card_original_type('halimar excavator'/'WWK', 'Creature — Human Wizard Ally').
card_original_text('halimar excavator'/'WWK', 'Whenever Halimar Excavator or another Ally enters the battlefield under your control, target player puts the top X cards of his or her library into his or her graveyard, where X is the number of Allies you control.').
card_first_print('halimar excavator', 'WWK').
card_image_name('halimar excavator'/'WWK', 'halimar excavator').
card_uid('halimar excavator'/'WWK', 'WWK:Halimar Excavator:halimar excavator').
card_rarity('halimar excavator'/'WWK', 'Common').
card_artist('halimar excavator'/'WWK', 'Kev Walker').
card_number('halimar excavator'/'WWK', '29').
card_multiverse_id('halimar excavator'/'WWK', '197129').

card_in_set('hammer of ruin', 'WWK').
card_original_type('hammer of ruin'/'WWK', 'Artifact — Equipment').
card_original_text('hammer of ruin'/'WWK', 'Equipped creature gets +2/+0.\nWhenever equipped creature deals combat damage to a player, you may destroy target Equipment that player controls.\nEquip {2}').
card_first_print('hammer of ruin', 'WWK').
card_image_name('hammer of ruin'/'WWK', 'hammer of ruin').
card_uid('hammer of ruin'/'WWK', 'WWK:Hammer of Ruin:hammer of ruin').
card_rarity('hammer of ruin'/'WWK', 'Uncommon').
card_artist('hammer of ruin'/'WWK', 'Vincent Proce').
card_number('hammer of ruin'/'WWK', '124').
card_flavor_text('hammer of ruin'/'WWK', 'By hammer forged, and by hammer undone.').
card_multiverse_id('hammer of ruin'/'WWK', '197871').

card_in_set('harabaz druid', 'WWK').
card_original_type('harabaz druid'/'WWK', 'Creature — Human Druid Ally').
card_original_text('harabaz druid'/'WWK', '{T}: Add X mana of any one color to your mana pool, where X is the number of Allies you control.').
card_first_print('harabaz druid', 'WWK').
card_image_name('harabaz druid'/'WWK', 'harabaz druid').
card_uid('harabaz druid'/'WWK', 'WWK:Harabaz Druid:harabaz druid').
card_rarity('harabaz druid'/'WWK', 'Rare').
card_artist('harabaz druid'/'WWK', 'Wayne Reynolds').
card_number('harabaz druid'/'WWK', '105').
card_flavor_text('harabaz druid'/'WWK', '\"I care nothing for treasure. The land beckons me, and I cannot rest until my feet have touched every inch of it.\"').
card_multiverse_id('harabaz druid'/'WWK', '194675').

card_in_set('hedron rover', 'WWK').
card_original_type('hedron rover'/'WWK', 'Artifact Creature — Construct').
card_original_text('hedron rover'/'WWK', 'Landfall — Whenever a land enters the battlefield under your control, Hedron Rover gets +2/+2 until end of turn.').
card_first_print('hedron rover', 'WWK').
card_image_name('hedron rover'/'WWK', 'hedron rover').
card_uid('hedron rover'/'WWK', 'WWK:Hedron Rover:hedron rover').
card_rarity('hedron rover'/'WWK', 'Common').
card_artist('hedron rover'/'WWK', 'Jason Felix').
card_number('hedron rover'/'WWK', '125').
card_flavor_text('hedron rover'/'WWK', '\"Now that it has woken up and wandered off, we should go see what it was guarding.\"\n—Gotzen, Bulwark explorer').
card_multiverse_id('hedron rover'/'WWK', '194686').

card_in_set('horizon drake', 'WWK').
card_original_type('horizon drake'/'WWK', 'Creature — Drake').
card_original_text('horizon drake'/'WWK', 'Flying, protection from lands').
card_first_print('horizon drake', 'WWK').
card_image_name('horizon drake'/'WWK', 'horizon drake').
card_uid('horizon drake'/'WWK', 'WWK:Horizon Drake:horizon drake').
card_rarity('horizon drake'/'WWK', 'Uncommon').
card_artist('horizon drake'/'WWK', 'Adi Granov').
card_number('horizon drake'/'WWK', '30').
card_flavor_text('horizon drake'/'WWK', 'Using thermal updrafts and gravity currents, drakes eat and sleep in the air. Some never touch the ground from birth until death.').
card_multiverse_id('horizon drake'/'WWK', '180412').

card_in_set('iona\'s judgment', 'WWK').
card_original_type('iona\'s judgment'/'WWK', 'Sorcery').
card_original_text('iona\'s judgment'/'WWK', 'Exile target creature or enchantment.').
card_first_print('iona\'s judgment', 'WWK').
card_image_name('iona\'s judgment'/'WWK', 'iona\'s judgment').
card_uid('iona\'s judgment'/'WWK', 'WWK:Iona\'s Judgment:iona\'s judgment').
card_rarity('iona\'s judgment'/'WWK', 'Common').
card_artist('iona\'s judgment'/'WWK', 'Mike Bierek').
card_number('iona\'s judgment'/'WWK', '8').
card_flavor_text('iona\'s judgment'/'WWK', 'Beneath the gaze of angels, only the righteous may stand without fear.').
card_multiverse_id('iona\'s judgment'/'WWK', '197880').

card_in_set('jace, the mind sculptor', 'WWK').
card_original_type('jace, the mind sculptor'/'WWK', 'Planeswalker — Jace').
card_original_text('jace, the mind sculptor'/'WWK', '+2: Look at the top card of target player\'s library. You may put that card on the bottom of that player\'s library.\n0: Draw three cards, then put two cards from your hand on top of your library in any order.\n-1: Return target creature to its owner\'s hand.\n-12: Exile all cards from target player\'s library, then that player shuffles his or her hand into his or her library.').
card_first_print('jace, the mind sculptor', 'WWK').
card_image_name('jace, the mind sculptor'/'WWK', 'jace, the mind sculptor').
card_uid('jace, the mind sculptor'/'WWK', 'WWK:Jace, the Mind Sculptor:jace, the mind sculptor').
card_rarity('jace, the mind sculptor'/'WWK', 'Mythic Rare').
card_artist('jace, the mind sculptor'/'WWK', 'Jason Chan').
card_number('jace, the mind sculptor'/'WWK', '31').
card_multiverse_id('jace, the mind sculptor'/'WWK', '195297').

card_in_set('jagwasp swarm', 'WWK').
card_original_type('jagwasp swarm'/'WWK', 'Creature — Insect').
card_original_text('jagwasp swarm'/'WWK', 'Flying').
card_first_print('jagwasp swarm', 'WWK').
card_image_name('jagwasp swarm'/'WWK', 'jagwasp swarm').
card_uid('jagwasp swarm'/'WWK', 'WWK:Jagwasp Swarm:jagwasp swarm').
card_rarity('jagwasp swarm'/'WWK', 'Common').
card_artist('jagwasp swarm'/'WWK', 'Austin Hsu').
card_number('jagwasp swarm'/'WWK', '58').
card_flavor_text('jagwasp swarm'/'WWK', '\"You know how to tell a sapling explorer from an old hardhead? See which one mistakes a swarm of jagwasps for the setting sun.\"\n—Chadir the Navigator').
card_multiverse_id('jagwasp swarm'/'WWK', '191581').

card_in_set('join the ranks', 'WWK').
card_original_type('join the ranks'/'WWK', 'Instant').
card_original_text('join the ranks'/'WWK', 'Put two 1/1 white Soldier Ally creature tokens onto the battlefield.').
card_first_print('join the ranks', 'WWK').
card_image_name('join the ranks'/'WWK', 'join the ranks').
card_uid('join the ranks'/'WWK', 'WWK:Join the Ranks:join the ranks').
card_rarity('join the ranks'/'WWK', 'Common').
card_artist('join the ranks'/'WWK', 'Kekai Kotaki').
card_number('join the ranks'/'WWK', '9').
card_flavor_text('join the ranks'/'WWK', 'Expeditionary houses are notorious for their rivalries. But when adventurers meet in the wilderness, the instinct for mutual survival outweighs petty grudges.').
card_multiverse_id('join the ranks'/'WWK', '194706').

card_in_set('joraga warcaller', 'WWK').
card_original_type('joraga warcaller'/'WWK', 'Creature — Elf Warrior').
card_original_text('joraga warcaller'/'WWK', 'Multikicker {1}{G} (You may pay an additional {1}{G} any number of times as you cast this spell.)\nJoraga Warcaller enters the battlefield with a +1/+1 counter on it for each time it was kicked.\nOther Elf creatures you control get +1/+1 for each +1/+1 counter on Joraga Warcaller.').
card_image_name('joraga warcaller'/'WWK', 'joraga warcaller').
card_uid('joraga warcaller'/'WWK', 'WWK:Joraga Warcaller:joraga warcaller').
card_rarity('joraga warcaller'/'WWK', 'Rare').
card_artist('joraga warcaller'/'WWK', 'Steven Belledin').
card_number('joraga warcaller'/'WWK', '106').
card_multiverse_id('joraga warcaller'/'WWK', '197765').

card_in_set('jwari shapeshifter', 'WWK').
card_original_type('jwari shapeshifter'/'WWK', 'Creature — Shapeshifter Ally').
card_original_text('jwari shapeshifter'/'WWK', 'You may have Jwari Shapeshifter enter the battlefield as a copy of any Ally creature on the battlefield.').
card_first_print('jwari shapeshifter', 'WWK').
card_image_name('jwari shapeshifter'/'WWK', 'jwari shapeshifter').
card_uid('jwari shapeshifter'/'WWK', 'WWK:Jwari Shapeshifter:jwari shapeshifter').
card_rarity('jwari shapeshifter'/'WWK', 'Rare').
card_artist('jwari shapeshifter'/'WWK', 'Kev Walker').
card_number('jwari shapeshifter'/'WWK', '32').
card_flavor_text('jwari shapeshifter'/'WWK', '\"The best expedition would be an entire team of me.\"').
card_multiverse_id('jwari shapeshifter'/'WWK', '198390').

card_in_set('kalastria highborn', 'WWK').
card_original_type('kalastria highborn'/'WWK', 'Creature — Vampire Shaman').
card_original_text('kalastria highborn'/'WWK', 'Whenever Kalastria Highborn or another Vampire you control is put into a graveyard from the battlefield, you may pay {B}. If you do, target player loses 2 life and you gain 2 life.').
card_image_name('kalastria highborn'/'WWK', 'kalastria highborn').
card_uid('kalastria highborn'/'WWK', 'WWK:Kalastria Highborn:kalastria highborn').
card_rarity('kalastria highborn'/'WWK', 'Rare').
card_artist('kalastria highborn'/'WWK', 'D. Alexander Gregory').
card_number('kalastria highborn'/'WWK', '59').
card_flavor_text('kalastria highborn'/'WWK', 'Each of Malakir\'s great families boasts a contingent of nulls appropriate to its rank in society.').
card_multiverse_id('kalastria highborn'/'WWK', '197118').

card_in_set('kazuul, tyrant of the cliffs', 'WWK').
card_original_type('kazuul, tyrant of the cliffs'/'WWK', 'Legendary Creature — Ogre Warrior').
card_original_text('kazuul, tyrant of the cliffs'/'WWK', 'Whenever a creature an opponent controls attacks, if you\'re the defending player, put a 3/3 red Ogre creature token onto the battlefield unless that creature\'s controller pays {3}.').
card_first_print('kazuul, tyrant of the cliffs', 'WWK').
card_image_name('kazuul, tyrant of the cliffs'/'WWK', 'kazuul, tyrant of the cliffs').
card_uid('kazuul, tyrant of the cliffs'/'WWK', 'WWK:Kazuul, Tyrant of the Cliffs:kazuul, tyrant of the cliffs').
card_rarity('kazuul, tyrant of the cliffs'/'WWK', 'Rare').
card_artist('kazuul, tyrant of the cliffs'/'WWK', 'Paul Bonner').
card_number('kazuul, tyrant of the cliffs'/'WWK', '84').
card_flavor_text('kazuul, tyrant of the cliffs'/'WWK', 'Warning to those who scale the cliffs without paying tribute: It\'s a long way down.').
card_multiverse_id('kazuul, tyrant of the cliffs'/'WWK', '197145').

card_in_set('khalni garden', 'WWK').
card_original_type('khalni garden'/'WWK', 'Land').
card_original_text('khalni garden'/'WWK', 'Khalni Garden enters the battlefield tapped.\nWhen Khalni Garden enters the battlefield, put a 0/1 green Plant creature token onto the battlefield.\n{T}: Add {G} to your mana pool.').
card_first_print('khalni garden', 'WWK').
card_image_name('khalni garden'/'WWK', 'khalni garden').
card_uid('khalni garden'/'WWK', 'WWK:Khalni Garden:khalni garden').
card_rarity('khalni garden'/'WWK', 'Common').
card_artist('khalni garden'/'WWK', 'Ryan Pancoast').
card_number('khalni garden'/'WWK', '138').
card_multiverse_id('khalni garden'/'WWK', '197789').

card_in_set('kitesail', 'WWK').
card_original_type('kitesail'/'WWK', 'Artifact — Equipment').
card_original_text('kitesail'/'WWK', 'Equipped creature gets +1/+0 and has flying.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('kitesail', 'WWK').
card_image_name('kitesail'/'WWK', 'kitesail').
card_uid('kitesail'/'WWK', 'WWK:Kitesail:kitesail').
card_rarity('kitesail'/'WWK', 'Common').
card_artist('kitesail'/'WWK', 'Cyril Van Der Haegen').
card_number('kitesail'/'WWK', '126').
card_flavor_text('kitesail'/'WWK', 'Kitesailing is a way of life—and without practice, the end of it.').
card_multiverse_id('kitesail'/'WWK', '197152').

card_in_set('kitesail apprentice', 'WWK').
card_original_type('kitesail apprentice'/'WWK', 'Creature — Kor Soldier').
card_original_text('kitesail apprentice'/'WWK', 'As long as Kitesail Apprentice is equipped, it gets +1/+1 and has flying.').
card_first_print('kitesail apprentice', 'WWK').
card_image_name('kitesail apprentice'/'WWK', 'kitesail apprentice').
card_uid('kitesail apprentice'/'WWK', 'WWK:Kitesail Apprentice:kitesail apprentice').
card_rarity('kitesail apprentice'/'WWK', 'Common').
card_artist('kitesail apprentice'/'WWK', 'Austin Hsu').
card_number('kitesail apprentice'/'WWK', '10').
card_flavor_text('kitesail apprentice'/'WWK', 'Carving lines through the sky, kite sailors map trails for the pilgrims below to follow.').
card_multiverse_id('kitesail apprentice'/'WWK', '194018').

card_in_set('kor firewalker', 'WWK').
card_original_type('kor firewalker'/'WWK', 'Creature — Kor Soldier').
card_original_text('kor firewalker'/'WWK', 'Protection from red\nWhenever a player casts a red spell, you may gain 1 life.').
card_image_name('kor firewalker'/'WWK', 'kor firewalker').
card_uid('kor firewalker'/'WWK', 'WWK:Kor Firewalker:kor firewalker').
card_rarity('kor firewalker'/'WWK', 'Uncommon').
card_artist('kor firewalker'/'WWK', 'Matt Stewart').
card_number('kor firewalker'/'WWK', '11').
card_flavor_text('kor firewalker'/'WWK', '\"A river of lava is just another river to cross.\"').
card_multiverse_id('kor firewalker'/'WWK', '194708').

card_in_set('lavaclaw reaches', 'WWK').
card_original_type('lavaclaw reaches'/'WWK', 'Land').
card_original_text('lavaclaw reaches'/'WWK', 'Lavaclaw Reaches enters the battlefield tapped.\n{T}: Add {B} or {R} to your mana pool.\n{1}{B}{R}: Until end of turn, Lavaclaw Reaches becomes a 2/2 black and red Elemental creature with \"{X}: This creature gets +X/+0 until end of turn.\" It\'s still a land.').
card_first_print('lavaclaw reaches', 'WWK').
card_image_name('lavaclaw reaches'/'WWK', 'lavaclaw reaches').
card_uid('lavaclaw reaches'/'WWK', 'WWK:Lavaclaw Reaches:lavaclaw reaches').
card_rarity('lavaclaw reaches'/'WWK', 'Rare').
card_artist('lavaclaw reaches'/'WWK', 'Véronique Meignaud').
card_number('lavaclaw reaches'/'WWK', '139').
card_multiverse_id('lavaclaw reaches'/'WWK', '171007').

card_in_set('leatherback baloth', 'WWK').
card_original_type('leatherback baloth'/'WWK', 'Creature — Beast').
card_original_text('leatherback baloth'/'WWK', '').
card_image_name('leatherback baloth'/'WWK', 'leatherback baloth').
card_uid('leatherback baloth'/'WWK', 'WWK:Leatherback Baloth:leatherback baloth').
card_rarity('leatherback baloth'/'WWK', 'Uncommon').
card_artist('leatherback baloth'/'WWK', 'Dave Kendall').
card_number('leatherback baloth'/'WWK', '107').
card_flavor_text('leatherback baloth'/'WWK', 'Heavy enough to withstand the Roil, leatherback skeletons are havens for travelers in storms and landshifts.').
card_multiverse_id('leatherback baloth'/'WWK', '197856').

card_in_set('lightkeeper of emeria', 'WWK').
card_original_type('lightkeeper of emeria'/'WWK', 'Creature — Angel').
card_original_text('lightkeeper of emeria'/'WWK', 'Multikicker {W} (You may pay an additional {W} any number of times as you cast this spell.)\nFlying\nWhen Lightkeeper of Emeria enters the battlefield, you gain 2 life for each time it was kicked.').
card_first_print('lightkeeper of emeria', 'WWK').
card_image_name('lightkeeper of emeria'/'WWK', 'lightkeeper of emeria').
card_uid('lightkeeper of emeria'/'WWK', 'WWK:Lightkeeper of Emeria:lightkeeper of emeria').
card_rarity('lightkeeper of emeria'/'WWK', 'Uncommon').
card_artist('lightkeeper of emeria'/'WWK', 'James Ryman').
card_number('lightkeeper of emeria'/'WWK', '12').
card_multiverse_id('lightkeeper of emeria'/'WWK', '189159').

card_in_set('loam lion', 'WWK').
card_original_type('loam lion'/'WWK', 'Creature — Cat').
card_original_text('loam lion'/'WWK', 'Loam Lion gets +1/+2 as long as you control a Forest.').
card_first_print('loam lion', 'WWK').
card_image_name('loam lion'/'WWK', 'loam lion').
card_uid('loam lion'/'WWK', 'WWK:Loam Lion:loam lion').
card_rarity('loam lion'/'WWK', 'Uncommon').
card_artist('loam lion'/'WWK', 'Daniel Ljunggren').
card_number('loam lion'/'WWK', '13').
card_flavor_text('loam lion'/'WWK', 'In Zendikar, today\'s grassland could be tomorrow\'s jungle, and hunting grounds change as quickly as the weather.').
card_multiverse_id('loam lion'/'WWK', '197873').

card_in_set('lodestone golem', 'WWK').
card_original_type('lodestone golem'/'WWK', 'Artifact Creature — Golem').
card_original_text('lodestone golem'/'WWK', 'Nonartifact spells cost {1} more to cast.').
card_first_print('lodestone golem', 'WWK').
card_image_name('lodestone golem'/'WWK', 'lodestone golem').
card_uid('lodestone golem'/'WWK', 'WWK:Lodestone Golem:lodestone golem').
card_rarity('lodestone golem'/'WWK', 'Rare').
card_artist('lodestone golem'/'WWK', 'Chris Rahn').
card_number('lodestone golem'/'WWK', '127').
card_flavor_text('lodestone golem'/'WWK', '\"Somehow it warps the Æther. It brings a strange weight, a blockade in the flow of spellcraft.\"\n—Noyan Dar, Tazeem lullmage').
card_multiverse_id('lodestone golem'/'WWK', '191557').

card_in_set('marsh threader', 'WWK').
card_original_type('marsh threader'/'WWK', 'Creature — Kor Scout').
card_original_text('marsh threader'/'WWK', 'Swampwalk').
card_first_print('marsh threader', 'WWK').
card_image_name('marsh threader'/'WWK', 'marsh threader').
card_uid('marsh threader'/'WWK', 'WWK:Marsh Threader:marsh threader').
card_rarity('marsh threader'/'WWK', 'Common').
card_artist('marsh threader'/'WWK', 'David Palumbo').
card_number('marsh threader'/'WWK', '14').
card_flavor_text('marsh threader'/'WWK', '\"Give me a sturdy rope over the advice of any so-called expert. I can thread the dark wilds better than anyone.\"').
card_multiverse_id('marsh threader'/'WWK', '194718').

card_in_set('marshal\'s anthem', 'WWK').
card_original_type('marshal\'s anthem'/'WWK', 'Enchantment').
card_original_text('marshal\'s anthem'/'WWK', 'Multikicker {1}{W} (You may pay an additional {1}{W} any number of times as you cast this spell.)\nCreatures you control get +1/+1.\nWhen Marshal\'s Anthem enters the battlefield, return up to X target creature cards from your graveyard to the battlefield, where X is the number of times Marshal\'s Anthem was kicked.').
card_first_print('marshal\'s anthem', 'WWK').
card_image_name('marshal\'s anthem'/'WWK', 'marshal\'s anthem').
card_uid('marshal\'s anthem'/'WWK', 'WWK:Marshal\'s Anthem:marshal\'s anthem').
card_rarity('marshal\'s anthem'/'WWK', 'Rare').
card_artist('marshal\'s anthem'/'WWK', 'Matt Stewart').
card_number('marshal\'s anthem'/'WWK', '15').
card_multiverse_id('marshal\'s anthem'/'WWK', '198393').

card_in_set('mire\'s toll', 'WWK').
card_original_type('mire\'s toll'/'WWK', 'Sorcery').
card_original_text('mire\'s toll'/'WWK', 'Target player reveals a number of cards from his or her hand equal to the number of Swamps you control. You choose one of them. That player discards that card.').
card_first_print('mire\'s toll', 'WWK').
card_image_name('mire\'s toll'/'WWK', 'mire\'s toll').
card_uid('mire\'s toll'/'WWK', 'WWK:Mire\'s Toll:mire\'s toll').
card_rarity('mire\'s toll'/'WWK', 'Common').
card_artist('mire\'s toll'/'WWK', 'Alex Horley-Orlandelli').
card_number('mire\'s toll'/'WWK', '60').
card_flavor_text('mire\'s toll'/'WWK', 'As the bridge snapped, Berko was sure he heard the swamp laughing at him.').
card_multiverse_id('mire\'s toll'/'WWK', '194002').

card_in_set('mordant dragon', 'WWK').
card_original_type('mordant dragon'/'WWK', 'Creature — Dragon').
card_original_text('mordant dragon'/'WWK', 'Flying\n{1}{R}: Mordant Dragon gets +1/+0 until end of turn.\nWhenever Mordant Dragon deals combat damage to a player, you may have it deal that much damage to target creature that player controls.').
card_first_print('mordant dragon', 'WWK').
card_image_name('mordant dragon'/'WWK', 'mordant dragon').
card_uid('mordant dragon'/'WWK', 'WWK:Mordant Dragon:mordant dragon').
card_rarity('mordant dragon'/'WWK', 'Rare').
card_artist('mordant dragon'/'WWK', 'Scott Chou').
card_number('mordant dragon'/'WWK', '85').
card_multiverse_id('mordant dragon'/'WWK', '197162').

card_in_set('mysteries of the deep', 'WWK').
card_original_type('mysteries of the deep'/'WWK', 'Instant').
card_original_text('mysteries of the deep'/'WWK', 'Draw two cards.\nLandfall — If you had a land enter the battlefield under your control this turn, draw three cards instead.').
card_first_print('mysteries of the deep', 'WWK').
card_image_name('mysteries of the deep'/'WWK', 'mysteries of the deep').
card_uid('mysteries of the deep'/'WWK', 'WWK:Mysteries of the Deep:mysteries of the deep').
card_rarity('mysteries of the deep'/'WWK', 'Common').
card_artist('mysteries of the deep'/'WWK', 'Véronique Meignaud').
card_number('mysteries of the deep'/'WWK', '33').
card_flavor_text('mysteries of the deep'/'WWK', '\"The Roil is destructive, but not random. It\'s Zendikar\'s way of revealing secrets.\"\n—Ilori, merfolk falconer').
card_multiverse_id('mysteries of the deep'/'WWK', '194699').

card_in_set('nature\'s claim', 'WWK').
card_original_type('nature\'s claim'/'WWK', 'Instant').
card_original_text('nature\'s claim'/'WWK', 'Destroy target artifact or enchantment. Its controller gains 4 life.').
card_first_print('nature\'s claim', 'WWK').
card_image_name('nature\'s claim'/'WWK', 'nature\'s claim').
card_uid('nature\'s claim'/'WWK', 'WWK:Nature\'s Claim:nature\'s claim').
card_rarity('nature\'s claim'/'WWK', 'Common').
card_artist('nature\'s claim'/'WWK', 'Daarken').
card_number('nature\'s claim'/'WWK', '108').
card_flavor_text('nature\'s claim'/'WWK', '\"On this plane, the use of magic can provoke a ferocious reprisal from nature. Whoever the Eldrazi were, their relics left Zendikar forever enraged.\"\n—Jace Beleren').
card_multiverse_id('nature\'s claim'/'WWK', '198357').

card_in_set('nemesis trap', 'WWK').
card_original_type('nemesis trap'/'WWK', 'Instant — Trap').
card_original_text('nemesis trap'/'WWK', 'If a white creature is attacking, you may pay {B}{B} rather than pay Nemesis Trap\'s mana cost.\nExile target attacking creature. Put a creature token that\'s a copy of that creature onto the battlefield. Exile it at the beginning of the next end step.').
card_first_print('nemesis trap', 'WWK').
card_image_name('nemesis trap'/'WWK', 'nemesis trap').
card_uid('nemesis trap'/'WWK', 'WWK:Nemesis Trap:nemesis trap').
card_rarity('nemesis trap'/'WWK', 'Uncommon').
card_artist('nemesis trap'/'WWK', 'Ryan Pancoast').
card_number('nemesis trap'/'WWK', '61').
card_multiverse_id('nemesis trap'/'WWK', '197836').

card_in_set('novablast wurm', 'WWK').
card_original_type('novablast wurm'/'WWK', 'Creature — Wurm').
card_original_text('novablast wurm'/'WWK', 'Whenever Novablast Wurm attacks, destroy all other creatures.').
card_first_print('novablast wurm', 'WWK').
card_image_name('novablast wurm'/'WWK', 'novablast wurm').
card_uid('novablast wurm'/'WWK', 'WWK:Novablast Wurm:novablast wurm').
card_rarity('novablast wurm'/'WWK', 'Mythic Rare').
card_artist('novablast wurm'/'WWK', 'Michael Komarck').
card_number('novablast wurm'/'WWK', '119').
card_flavor_text('novablast wurm'/'WWK', '\"The sun was born within its coils, but it fled to the sky. The wurm hungers for its child, and the world will bear its loss.\"\n—Screed of the Mul Daya').
card_multiverse_id('novablast wurm'/'WWK', '197857').

card_in_set('omnath, locus of mana', 'WWK').
card_original_type('omnath, locus of mana'/'WWK', 'Legendary Creature — Elemental').
card_original_text('omnath, locus of mana'/'WWK', 'Green mana doesn\'t empty from your mana pool as steps and phases end.\nOmnath, Locus of Mana gets +1/+1 for each green mana in your mana pool.').
card_first_print('omnath, locus of mana', 'WWK').
card_image_name('omnath, locus of mana'/'WWK', 'omnath, locus of mana').
card_uid('omnath, locus of mana'/'WWK', 'WWK:Omnath, Locus of Mana:omnath, locus of mana').
card_rarity('omnath, locus of mana'/'WWK', 'Mythic Rare').
card_artist('omnath, locus of mana'/'WWK', 'Mike Bierek').
card_number('omnath, locus of mana'/'WWK', '109').
card_flavor_text('omnath, locus of mana'/'WWK', 'It gathers against the coming storm.').
card_multiverse_id('omnath, locus of mana'/'WWK', '197759').

card_in_set('perimeter captain', 'WWK').
card_original_type('perimeter captain'/'WWK', 'Creature — Human Soldier').
card_original_text('perimeter captain'/'WWK', 'Defender\nWhenever a creature you control with defender blocks, you may gain 2 life.').
card_first_print('perimeter captain', 'WWK').
card_image_name('perimeter captain'/'WWK', 'perimeter captain').
card_uid('perimeter captain'/'WWK', 'WWK:Perimeter Captain:perimeter captain').
card_rarity('perimeter captain'/'WWK', 'Uncommon').
card_artist('perimeter captain'/'WWK', 'Steven Belledin').
card_number('perimeter captain'/'WWK', '16').
card_flavor_text('perimeter captain'/'WWK', '\"We stand between the jaws of chaos and the mantle of order.\"').
card_multiverse_id('perimeter captain'/'WWK', '198361').

card_in_set('permafrost trap', 'WWK').
card_original_type('permafrost trap'/'WWK', 'Instant — Trap').
card_original_text('permafrost trap'/'WWK', 'If an opponent had a green creature enter the battlefield under his or her control this turn, you may pay {U} rather than pay Permafrost Trap\'s mana cost.\nTap up to two target creatures. Those creatures don\'t untap during their controller\'s next untap step.').
card_first_print('permafrost trap', 'WWK').
card_image_name('permafrost trap'/'WWK', 'permafrost trap').
card_uid('permafrost trap'/'WWK', 'WWK:Permafrost Trap:permafrost trap').
card_rarity('permafrost trap'/'WWK', 'Uncommon').
card_artist('permafrost trap'/'WWK', 'Howard Lyon').
card_number('permafrost trap'/'WWK', '34').
card_multiverse_id('permafrost trap'/'WWK', '194703').

card_in_set('pilgrim\'s eye', 'WWK').
card_original_type('pilgrim\'s eye'/'WWK', 'Artifact Creature — Thopter').
card_original_text('pilgrim\'s eye'/'WWK', 'Flying\nWhen Pilgrim\'s Eye enters the battlefield, you may search your library for a basic land card, reveal it, put it into your hand, then shuffle your library.').
card_first_print('pilgrim\'s eye', 'WWK').
card_image_name('pilgrim\'s eye'/'WWK', 'pilgrim\'s eye').
card_uid('pilgrim\'s eye'/'WWK', 'WWK:Pilgrim\'s Eye:pilgrim\'s eye').
card_rarity('pilgrim\'s eye'/'WWK', 'Common').
card_artist('pilgrim\'s eye'/'WWK', 'Dan Scott').
card_number('pilgrim\'s eye'/'WWK', '128').
card_flavor_text('pilgrim\'s eye'/'WWK', 'The kor send their thopter kites to see if the land is in a welcoming mood.').
card_multiverse_id('pilgrim\'s eye'/'WWK', '198367').

card_in_set('pulse tracker', 'WWK').
card_original_type('pulse tracker'/'WWK', 'Creature — Vampire Rogue').
card_original_text('pulse tracker'/'WWK', 'Whenever Pulse Tracker attacks, each opponent loses 1 life.').
card_first_print('pulse tracker', 'WWK').
card_image_name('pulse tracker'/'WWK', 'pulse tracker').
card_uid('pulse tracker'/'WWK', 'WWK:Pulse Tracker:pulse tracker').
card_rarity('pulse tracker'/'WWK', 'Common').
card_artist('pulse tracker'/'WWK', 'Andrew Robinson').
card_number('pulse tracker'/'WWK', '62').
card_flavor_text('pulse tracker'/'WWK', 'Pulse trackers terrorize adventurers, driving them through the jungle toward certain death at the gates of Malakir.').
card_multiverse_id('pulse tracker'/'WWK', '197154').

card_in_set('quag vampires', 'WWK').
card_original_type('quag vampires'/'WWK', 'Creature — Vampire Rogue').
card_original_text('quag vampires'/'WWK', 'Multikicker {1}{B} (You may pay an additional {1}{B} any number of times as you cast this spell.)\nSwampwalk\nQuag Vampires enters the battlefield with a +1/+1 counter on it for each time it was kicked.').
card_first_print('quag vampires', 'WWK').
card_image_name('quag vampires'/'WWK', 'quag vampires').
card_uid('quag vampires'/'WWK', 'WWK:Quag Vampires:quag vampires').
card_rarity('quag vampires'/'WWK', 'Common').
card_artist('quag vampires'/'WWK', 'Jason Felix').
card_number('quag vampires'/'WWK', '63').
card_multiverse_id('quag vampires'/'WWK', '197763').

card_in_set('quest for renewal', 'WWK').
card_original_type('quest for renewal'/'WWK', 'Enchantment').
card_original_text('quest for renewal'/'WWK', 'Whenever a creature you control becomes tapped, you may put a quest counter on Quest for Renewal.\nAs long as there are four or more quest counters on Quest for Renewal, untap all creatures you control during each other player\'s untap step.').
card_first_print('quest for renewal', 'WWK').
card_image_name('quest for renewal'/'WWK', 'quest for renewal').
card_uid('quest for renewal'/'WWK', 'WWK:Quest for Renewal:quest for renewal').
card_rarity('quest for renewal'/'WWK', 'Uncommon').
card_artist('quest for renewal'/'WWK', 'Tomasz Jedruszek').
card_number('quest for renewal'/'WWK', '110').
card_multiverse_id('quest for renewal'/'WWK', '194007').

card_in_set('quest for the goblin lord', 'WWK').
card_original_type('quest for the goblin lord'/'WWK', 'Enchantment').
card_original_text('quest for the goblin lord'/'WWK', 'Whenever a Goblin enters the battlefield under your control, you may put a quest counter on Quest for the Goblin Lord.\nAs long as Quest for the Goblin Lord has five or more quest counters on it, creatures you control get +2/+0.').
card_first_print('quest for the goblin lord', 'WWK').
card_image_name('quest for the goblin lord'/'WWK', 'quest for the goblin lord').
card_uid('quest for the goblin lord'/'WWK', 'WWK:Quest for the Goblin Lord:quest for the goblin lord').
card_rarity('quest for the goblin lord'/'WWK', 'Uncommon').
card_artist('quest for the goblin lord'/'WWK', 'Jesper Ejsing').
card_number('quest for the goblin lord'/'WWK', '86').
card_flavor_text('quest for the goblin lord'/'WWK', 'A shrine to unchecked desire.').
card_multiverse_id('quest for the goblin lord'/'WWK', '198392').

card_in_set('quest for the nihil stone', 'WWK').
card_original_type('quest for the nihil stone'/'WWK', 'Enchantment').
card_original_text('quest for the nihil stone'/'WWK', 'Whenever an opponent discards a card, you may put a quest counter on Quest for the Nihil Stone.\nAt the beginning of each opponent\'s upkeep, if that player has no cards in hand and Quest for the Nihil Stone has two or more quest counters on it, you may have that player lose 5 life.').
card_first_print('quest for the nihil stone', 'WWK').
card_image_name('quest for the nihil stone'/'WWK', 'quest for the nihil stone').
card_uid('quest for the nihil stone'/'WWK', 'WWK:Quest for the Nihil Stone:quest for the nihil stone').
card_rarity('quest for the nihil stone'/'WWK', 'Rare').
card_artist('quest for the nihil stone'/'WWK', 'Mike Bierek').
card_number('quest for the nihil stone'/'WWK', '64').
card_multiverse_id('quest for the nihil stone'/'WWK', '198386').

card_in_set('quest for ula\'s temple', 'WWK').
card_original_type('quest for ula\'s temple'/'WWK', 'Enchantment').
card_original_text('quest for ula\'s temple'/'WWK', 'At the beginning of your upkeep, you may look at the top card of your library. If it\'s a creature card, you may reveal it and put a quest counter on Quest for Ula\'s Temple.\nAt the beginning of each end step, if there are three or more quest counters on Quest for Ula\'s Temple, you may put a Kraken, Leviathan, Octopus, or Serpent creature card from your hand onto the battlefield.').
card_first_print('quest for ula\'s temple', 'WWK').
card_image_name('quest for ula\'s temple'/'WWK', 'quest for ula\'s temple').
card_uid('quest for ula\'s temple'/'WWK', 'WWK:Quest for Ula\'s Temple:quest for ula\'s temple').
card_rarity('quest for ula\'s temple'/'WWK', 'Rare').
card_artist('quest for ula\'s temple'/'WWK', 'Rob Alexander').
card_number('quest for ula\'s temple'/'WWK', '35').
card_multiverse_id('quest for ula\'s temple'/'WWK', '198401').

card_in_set('quicksand', 'WWK').
card_original_type('quicksand'/'WWK', 'Land').
card_original_text('quicksand'/'WWK', '{T}: Add {1} to your mana pool.\n{T}, Sacrifice Quicksand: Target attacking creature without flying gets -1/-2 until end of turn.').
card_image_name('quicksand'/'WWK', 'quicksand').
card_uid('quicksand'/'WWK', 'WWK:Quicksand:quicksand').
card_rarity('quicksand'/'WWK', 'Common').
card_artist('quicksand'/'WWK', 'Matt Stewart').
card_number('quicksand'/'WWK', '140').
card_flavor_text('quicksand'/'WWK', 'Not all deaths are etched with mythic meaning and iconic glory.').
card_multiverse_id('quicksand'/'WWK', '197853').

card_in_set('raging ravine', 'WWK').
card_original_type('raging ravine'/'WWK', 'Land').
card_original_text('raging ravine'/'WWK', 'Raging Ravine enters the battlefield tapped.\n{T}: Add {R} or {G} to your mana pool.\n{2}{R}{G}: Until end of turn, Raging Ravine becomes a 3/3 red and green Elemental creature with \"Whenever this creature attacks, put a +1/+1 counter on it.\" It\'s still a land.').
card_first_print('raging ravine', 'WWK').
card_image_name('raging ravine'/'WWK', 'raging ravine').
card_uid('raging ravine'/'WWK', 'WWK:Raging Ravine:raging ravine').
card_rarity('raging ravine'/'WWK', 'Rare').
card_artist('raging ravine'/'WWK', 'Todd Lockwood').
card_number('raging ravine'/'WWK', '141').
card_multiverse_id('raging ravine'/'WWK', '177583').

card_in_set('razor boomerang', 'WWK').
card_original_type('razor boomerang'/'WWK', 'Artifact — Equipment').
card_original_text('razor boomerang'/'WWK', 'Equipped creature has \"{T}, Unattach Razor Boomerang: Razor Boomerang deals 1 damage to target creature or player. Return Razor Boomerang to its owner\'s hand.\"\nEquip {2}').
card_first_print('razor boomerang', 'WWK').
card_image_name('razor boomerang'/'WWK', 'razor boomerang').
card_uid('razor boomerang'/'WWK', 'WWK:Razor Boomerang:razor boomerang').
card_rarity('razor boomerang'/'WWK', 'Uncommon').
card_artist('razor boomerang'/'WWK', 'Franz Vohwinkel').
card_number('razor boomerang'/'WWK', '129').
card_flavor_text('razor boomerang'/'WWK', 'Few can catch it without losing a finger.').
card_multiverse_id('razor boomerang'/'WWK', '198395').

card_in_set('refraction trap', 'WWK').
card_original_type('refraction trap'/'WWK', 'Instant — Trap').
card_original_text('refraction trap'/'WWK', 'If an opponent cast a red instant or sorcery spell this turn, you may pay {W} rather than pay Refraction Trap\'s mana cost.\nPrevent the next 3 damage that a source of your choice would deal to you and/or permanents you control this turn. If damage is prevented this way, Refraction Trap deals that much damage to target creature or player.').
card_first_print('refraction trap', 'WWK').
card_image_name('refraction trap'/'WWK', 'refraction trap').
card_uid('refraction trap'/'WWK', 'WWK:Refraction Trap:refraction trap').
card_rarity('refraction trap'/'WWK', 'Uncommon').
card_artist('refraction trap'/'WWK', 'Zoltan Boros & Gabor Szikszai').
card_number('refraction trap'/'WWK', '17').
card_multiverse_id('refraction trap'/'WWK', '189163').

card_in_set('rest for the weary', 'WWK').
card_original_type('rest for the weary'/'WWK', 'Instant').
card_original_text('rest for the weary'/'WWK', 'Target player gains 4 life.\nLandfall — If you had a land enter the battlefield under your control this turn, that player gains 8 life instead.').
card_first_print('rest for the weary', 'WWK').
card_image_name('rest for the weary'/'WWK', 'rest for the weary').
card_uid('rest for the weary'/'WWK', 'WWK:Rest for the Weary:rest for the weary').
card_rarity('rest for the weary'/'WWK', 'Common').
card_artist('rest for the weary'/'WWK', 'James Paick').
card_number('rest for the weary'/'WWK', '18').
card_flavor_text('rest for the weary'/'WWK', 'Zendikar is a brutal taskmaster, but sometimes it offers unexpected solace.').
card_multiverse_id('rest for the weary'/'WWK', '189180').

card_in_set('ricochet trap', 'WWK').
card_original_type('ricochet trap'/'WWK', 'Instant — Trap').
card_original_text('ricochet trap'/'WWK', 'If an opponent cast a blue spell this turn, you may pay {R} rather than pay Ricochet Trap\'s mana cost.\nChange the target of target spell with a single target.').
card_first_print('ricochet trap', 'WWK').
card_image_name('ricochet trap'/'WWK', 'ricochet trap').
card_uid('ricochet trap'/'WWK', 'WWK:Ricochet Trap:ricochet trap').
card_rarity('ricochet trap'/'WWK', 'Uncommon').
card_artist('ricochet trap'/'WWK', 'Jaime Jones').
card_number('ricochet trap'/'WWK', '87').
card_flavor_text('ricochet trap'/'WWK', 'In his last seconds, Remhas regretted using his most powerful spell.').
card_multiverse_id('ricochet trap'/'WWK', '191549').

card_in_set('roiling terrain', 'WWK').
card_original_type('roiling terrain'/'WWK', 'Sorcery').
card_original_text('roiling terrain'/'WWK', 'Destroy target land, then Roiling Terrain deals damage to that land\'s controller equal to the number of land cards in that player\'s graveyard.').
card_first_print('roiling terrain', 'WWK').
card_image_name('roiling terrain'/'WWK', 'roiling terrain').
card_uid('roiling terrain'/'WWK', 'WWK:Roiling Terrain:roiling terrain').
card_rarity('roiling terrain'/'WWK', 'Common').
card_artist('roiling terrain'/'WWK', 'Steve Prescott').
card_number('roiling terrain'/'WWK', '88').
card_flavor_text('roiling terrain'/'WWK', '\"The trembling lands ruptured, leaving an open door for things from below to emerge.\"\n—Screed of the Mul Daya').
card_multiverse_id('roiling terrain'/'WWK', '194671').

card_in_set('ruin ghost', 'WWK').
card_original_type('ruin ghost'/'WWK', 'Creature — Spirit').
card_original_text('ruin ghost'/'WWK', '{W}, {T}: Exile target land you control, then return it to the battlefield under your control.').
card_first_print('ruin ghost', 'WWK').
card_image_name('ruin ghost'/'WWK', 'ruin ghost').
card_uid('ruin ghost'/'WWK', 'WWK:Ruin Ghost:ruin ghost').
card_rarity('ruin ghost'/'WWK', 'Uncommon').
card_artist('ruin ghost'/'WWK', 'Jason A. Engle').
card_number('ruin ghost'/'WWK', '19').
card_flavor_text('ruin ghost'/'WWK', 'The Forsaken Ones haunt the ruins that flicker in and out of the spirit realm. Such sites are hard to find and even harder to loot.').
card_multiverse_id('ruin ghost'/'WWK', '198378').

card_in_set('rumbling aftershocks', 'WWK').
card_original_type('rumbling aftershocks'/'WWK', 'Enchantment').
card_original_text('rumbling aftershocks'/'WWK', 'Whenever you cast a kicked spell, you may have Rumbling Aftershocks deal damage to target creature or player equal to the number of times that spell was kicked.').
card_first_print('rumbling aftershocks', 'WWK').
card_image_name('rumbling aftershocks'/'WWK', 'rumbling aftershocks').
card_uid('rumbling aftershocks'/'WWK', 'WWK:Rumbling Aftershocks:rumbling aftershocks').
card_rarity('rumbling aftershocks'/'WWK', 'Uncommon').
card_artist('rumbling aftershocks'/'WWK', 'Tomasz Jedruszek').
card_number('rumbling aftershocks'/'WWK', '89').
card_flavor_text('rumbling aftershocks'/'WWK', '\"I like it here. You always get a little more for your mana.\"\n—Chandra Nalaar').
card_multiverse_id('rumbling aftershocks'/'WWK', '197770').

card_in_set('ruthless cullblade', 'WWK').
card_original_type('ruthless cullblade'/'WWK', 'Creature — Vampire Warrior').
card_original_text('ruthless cullblade'/'WWK', 'Ruthless Cullblade gets +2/+1 as long as an opponent has 10 or less life.').
card_first_print('ruthless cullblade', 'WWK').
card_image_name('ruthless cullblade'/'WWK', 'ruthless cullblade').
card_uid('ruthless cullblade'/'WWK', 'WWK:Ruthless Cullblade:ruthless cullblade').
card_rarity('ruthless cullblade'/'WWK', 'Common').
card_artist('ruthless cullblade'/'WWK', 'rk post').
card_number('ruthless cullblade'/'WWK', '65').
card_flavor_text('ruthless cullblade'/'WWK', 'She wields a blade as bloodthirsty and elegant as herself.').
card_multiverse_id('ruthless cullblade'/'WWK', '194678').

card_in_set('scrib nibblers', 'WWK').
card_original_type('scrib nibblers'/'WWK', 'Creature — Rat').
card_original_text('scrib nibblers'/'WWK', '{T}: Exile the top card of target player\'s library. If it\'s a land card, you gain 1 life.\nLandfall — Whenever a land enters the battlefield under your control, you may untap Scrib Nibblers.').
card_first_print('scrib nibblers', 'WWK').
card_image_name('scrib nibblers'/'WWK', 'scrib nibblers').
card_uid('scrib nibblers'/'WWK', 'WWK:Scrib Nibblers:scrib nibblers').
card_rarity('scrib nibblers'/'WWK', 'Uncommon').
card_artist('scrib nibblers'/'WWK', 'Daren Bader').
card_number('scrib nibblers'/'WWK', '66').
card_multiverse_id('scrib nibblers'/'WWK', '198382').

card_in_set('searing blaze', 'WWK').
card_original_type('searing blaze'/'WWK', 'Instant').
card_original_text('searing blaze'/'WWK', 'Searing Blaze deals 1 damage to target player and 1 damage to target creature that player controls.\nLandfall — If you had a land enter the battlefield under your control this turn, Searing Blaze deals 3 damage to that player and 3 damage to that creature instead.').
card_image_name('searing blaze'/'WWK', 'searing blaze').
card_uid('searing blaze'/'WWK', 'WWK:Searing Blaze:searing blaze').
card_rarity('searing blaze'/'WWK', 'Common').
card_artist('searing blaze'/'WWK', 'James Paick').
card_number('searing blaze'/'WWK', '90').
card_multiverse_id('searing blaze'/'WWK', '193982').

card_in_set('seer\'s sundial', 'WWK').
card_original_type('seer\'s sundial'/'WWK', 'Artifact').
card_original_text('seer\'s sundial'/'WWK', 'Landfall — Whenever a land enters the battlefield under your control, you may pay {2}. If you do, draw a card.').
card_first_print('seer\'s sundial', 'WWK').
card_image_name('seer\'s sundial'/'WWK', 'seer\'s sundial').
card_uid('seer\'s sundial'/'WWK', 'WWK:Seer\'s Sundial:seer\'s sundial').
card_rarity('seer\'s sundial'/'WWK', 'Rare').
card_artist('seer\'s sundial'/'WWK', 'Franz Vohwinkel').
card_number('seer\'s sundial'/'WWK', '130').
card_flavor_text('seer\'s sundial'/'WWK', '\"The shadow travels toward the apex. I predict we will soon see the true measure of darkness.\"').
card_multiverse_id('seer\'s sundial'/'WWK', '201561').

card_in_set('sejiri merfolk', 'WWK').
card_original_type('sejiri merfolk'/'WWK', 'Creature — Merfolk Soldier').
card_original_text('sejiri merfolk'/'WWK', 'As long as you control a Plains, Sejiri Merfolk has first strike and lifelink. (Damage dealt by a creature with lifelink also causes its controller to gain that much life.)').
card_first_print('sejiri merfolk', 'WWK').
card_image_name('sejiri merfolk'/'WWK', 'sejiri merfolk').
card_uid('sejiri merfolk'/'WWK', 'WWK:Sejiri Merfolk:sejiri merfolk').
card_rarity('sejiri merfolk'/'WWK', 'Uncommon').
card_artist('sejiri merfolk'/'WWK', 'Anthony Francisco').
card_number('sejiri merfolk'/'WWK', '36').
card_multiverse_id('sejiri merfolk'/'WWK', '197844').

card_in_set('sejiri steppe', 'WWK').
card_original_type('sejiri steppe'/'WWK', 'Land').
card_original_text('sejiri steppe'/'WWK', 'Sejiri Steppe enters the battlefield tapped.\nWhen Sejiri Steppe enters the battlefield, target creature you control gains protection from the color of your choice until end of turn.\n{T}: Add {W} to your mana pool.').
card_first_print('sejiri steppe', 'WWK').
card_image_name('sejiri steppe'/'WWK', 'sejiri steppe').
card_uid('sejiri steppe'/'WWK', 'WWK:Sejiri Steppe:sejiri steppe').
card_rarity('sejiri steppe'/'WWK', 'Common').
card_artist('sejiri steppe'/'WWK', 'Anthony Francisco').
card_number('sejiri steppe'/'WWK', '142').
card_multiverse_id('sejiri steppe'/'WWK', '197788').

card_in_set('selective memory', 'WWK').
card_original_type('selective memory'/'WWK', 'Sorcery').
card_original_text('selective memory'/'WWK', 'Search your library for any number of nonland cards and exile them. Then shuffle your library.').
card_first_print('selective memory', 'WWK').
card_image_name('selective memory'/'WWK', 'selective memory').
card_uid('selective memory'/'WWK', 'WWK:Selective Memory:selective memory').
card_rarity('selective memory'/'WWK', 'Rare').
card_artist('selective memory'/'WWK', 'Chippy').
card_number('selective memory'/'WWK', '37').
card_flavor_text('selective memory'/'WWK', '\"I of all people know that memory is not a safe place to hide valuables.\"\n—Jace Beleren').
card_multiverse_id('selective memory'/'WWK', '194700').

card_in_set('shoreline salvager', 'WWK').
card_original_type('shoreline salvager'/'WWK', 'Creature — Surrakar').
card_original_text('shoreline salvager'/'WWK', 'Whenever Shoreline Salvager deals combat damage to a player, if you control an Island, you may draw a card.').
card_first_print('shoreline salvager', 'WWK').
card_image_name('shoreline salvager'/'WWK', 'shoreline salvager').
card_uid('shoreline salvager'/'WWK', 'WWK:Shoreline Salvager:shoreline salvager').
card_rarity('shoreline salvager'/'WWK', 'Uncommon').
card_artist('shoreline salvager'/'WWK', 'Igor Kieryluk').
card_number('shoreline salvager'/'WWK', '67').
card_flavor_text('shoreline salvager'/'WWK', '\"They search for treasures that wash up from the depths. Tracking one could lead you to riches. Or in circles.\"\n—Javad Nasrin, Ondu relic hunter').
card_multiverse_id('shoreline salvager'/'WWK', '197877').

card_in_set('skitter of lizards', 'WWK').
card_original_type('skitter of lizards'/'WWK', 'Creature — Lizard').
card_original_text('skitter of lizards'/'WWK', 'Multikicker {1}{R} (You may pay an additional {1}{R} any number of times as you cast this spell.)\nHaste\nSkitter of Lizards enters the battlefield with a +1/+1 counter on it for each time it was kicked.').
card_first_print('skitter of lizards', 'WWK').
card_image_name('skitter of lizards'/'WWK', 'skitter of lizards').
card_uid('skitter of lizards'/'WWK', 'WWK:Skitter of Lizards:skitter of lizards').
card_rarity('skitter of lizards'/'WWK', 'Common').
card_artist('skitter of lizards'/'WWK', 'Warren Mahy').
card_number('skitter of lizards'/'WWK', '91').
card_multiverse_id('skitter of lizards'/'WWK', '197758').

card_in_set('slavering nulls', 'WWK').
card_original_type('slavering nulls'/'WWK', 'Creature — Goblin Zombie').
card_original_text('slavering nulls'/'WWK', 'Whenever Slavering Nulls deals combat damage to a player, if you control a Swamp, you may have that player discard a card.').
card_first_print('slavering nulls', 'WWK').
card_image_name('slavering nulls'/'WWK', 'slavering nulls').
card_uid('slavering nulls'/'WWK', 'WWK:Slavering Nulls:slavering nulls').
card_rarity('slavering nulls'/'WWK', 'Uncommon').
card_artist('slavering nulls'/'WWK', 'Dave Kendall').
card_number('slavering nulls'/'WWK', '92').
card_flavor_text('slavering nulls'/'WWK', 'Having lost their minds, they now want yours.').
card_multiverse_id('slavering nulls'/'WWK', '197859').

card_in_set('slingbow trap', 'WWK').
card_original_type('slingbow trap'/'WWK', 'Instant — Trap').
card_original_text('slingbow trap'/'WWK', 'If a black creature with flying is attacking, you may pay {G} rather than pay Slingbow Trap\'s mana cost.\nDestroy target attacking creature with flying.').
card_first_print('slingbow trap', 'WWK').
card_image_name('slingbow trap'/'WWK', 'slingbow trap').
card_uid('slingbow trap'/'WWK', 'WWK:Slingbow Trap:slingbow trap').
card_rarity('slingbow trap'/'WWK', 'Uncommon').
card_artist('slingbow trap'/'WWK', 'Daniel Ljunggren').
card_number('slingbow trap'/'WWK', '111').
card_flavor_text('slingbow trap'/'WWK', 'The Tajuru elves devised ways to let the forest defend itself.').
card_multiverse_id('slingbow trap'/'WWK', '194017').

card_in_set('smoldering spires', 'WWK').
card_original_type('smoldering spires'/'WWK', 'Land').
card_original_text('smoldering spires'/'WWK', 'Smoldering Spires enters the battlefield tapped.\nWhen Smoldering Spires enters the battlefield, target creature can\'t block this turn.\n{T}: Add {R} to your mana pool.').
card_first_print('smoldering spires', 'WWK').
card_image_name('smoldering spires'/'WWK', 'smoldering spires').
card_uid('smoldering spires'/'WWK', 'WWK:Smoldering Spires:smoldering spires').
card_rarity('smoldering spires'/'WWK', 'Common').
card_artist('smoldering spires'/'WWK', 'Rob Alexander').
card_number('smoldering spires'/'WWK', '143').
card_multiverse_id('smoldering spires'/'WWK', '197785').

card_in_set('smother', 'WWK').
card_original_type('smother'/'WWK', 'Instant').
card_original_text('smother'/'WWK', 'Destroy target creature with converted mana cost 3 or less. It can\'t be regenerated.').
card_image_name('smother'/'WWK', 'smother').
card_uid('smother'/'WWK', 'WWK:Smother:smother').
card_rarity('smother'/'WWK', 'Uncommon').
card_artist('smother'/'WWK', 'Karl Kopinski').
card_number('smother'/'WWK', '68').
card_flavor_text('smother'/'WWK', '\"Before I hire new recruits, I test how long they can hold their breath. You\'d be surprised how often it comes up.\"\n—Zahr Gada, Halimar expedition leader').
card_multiverse_id('smother'/'WWK', '191578').

card_in_set('snapping creeper', 'WWK').
card_original_type('snapping creeper'/'WWK', 'Creature — Plant').
card_original_text('snapping creeper'/'WWK', 'Landfall — Whenever a land enters the battlefield under your control, Snapping Creeper gains vigilance until end of turn.').
card_first_print('snapping creeper', 'WWK').
card_image_name('snapping creeper'/'WWK', 'snapping creeper').
card_uid('snapping creeper'/'WWK', 'WWK:Snapping Creeper:snapping creeper').
card_rarity('snapping creeper'/'WWK', 'Common').
card_artist('snapping creeper'/'WWK', 'Cyril Van Der Haegen').
card_number('snapping creeper'/'WWK', '112').
card_flavor_text('snapping creeper'/'WWK', 'Its vast network of roots and vines keeps it aware of distant dangers.').
card_multiverse_id('snapping creeper'/'WWK', '191547').

card_in_set('spell contortion', 'WWK').
card_original_type('spell contortion'/'WWK', 'Instant').
card_original_text('spell contortion'/'WWK', 'Multikicker {1}{U} (You may pay an additional {1}{U} any number of times as you cast this spell.)\nCounter target spell unless its controller pays {2}. Draw a card for each time Spell Contortion was kicked.').
card_first_print('spell contortion', 'WWK').
card_image_name('spell contortion'/'WWK', 'spell contortion').
card_uid('spell contortion'/'WWK', 'WWK:Spell Contortion:spell contortion').
card_rarity('spell contortion'/'WWK', 'Uncommon').
card_artist('spell contortion'/'WWK', 'Wayne Reynolds').
card_number('spell contortion'/'WWK', '38').
card_multiverse_id('spell contortion'/'WWK', '194714').

card_in_set('stirring wildwood', 'WWK').
card_original_type('stirring wildwood'/'WWK', 'Land').
card_original_text('stirring wildwood'/'WWK', 'Stirring Wildwood enters the battlefield tapped.\n{T}: Add {G} or {W} to your mana pool.\n{1}{G}{W}: Until end of turn, Stirring Wildwood becomes a 3/4 green and white Elemental creature with reach. It\'s still a land.').
card_first_print('stirring wildwood', 'WWK').
card_image_name('stirring wildwood'/'WWK', 'stirring wildwood').
card_uid('stirring wildwood'/'WWK', 'WWK:Stirring Wildwood:stirring wildwood').
card_rarity('stirring wildwood'/'WWK', 'Rare').
card_artist('stirring wildwood'/'WWK', 'Eric Deschamps').
card_number('stirring wildwood'/'WWK', '144').
card_multiverse_id('stirring wildwood'/'WWK', '177560').

card_in_set('stone idol trap', 'WWK').
card_original_type('stone idol trap'/'WWK', 'Instant — Trap').
card_original_text('stone idol trap'/'WWK', 'Stone Idol Trap costs {1} less to cast for each attacking creature.\nPut a 6/12 colorless Construct artifact creature token with trample onto the battlefield. Exile it at the beginning of your next end step.').
card_first_print('stone idol trap', 'WWK').
card_image_name('stone idol trap'/'WWK', 'stone idol trap').
card_uid('stone idol trap'/'WWK', 'WWK:Stone Idol Trap:stone idol trap').
card_rarity('stone idol trap'/'WWK', 'Rare').
card_artist('stone idol trap'/'WWK', 'Jung Park').
card_number('stone idol trap'/'WWK', '93').
card_multiverse_id('stone idol trap'/'WWK', '191552').

card_in_set('stoneforge mystic', 'WWK').
card_original_type('stoneforge mystic'/'WWK', 'Creature — Kor Artificer').
card_original_text('stoneforge mystic'/'WWK', 'When Stoneforge Mystic enters the battlefield, you may search your library for an Equipment card, reveal it, put it into your hand, then shuffle your library.\n{1}{W}, {T}: You may put an Equipment card from your hand onto the battlefield.').
card_first_print('stoneforge mystic', 'WWK').
card_image_name('stoneforge mystic'/'WWK', 'stoneforge mystic').
card_uid('stoneforge mystic'/'WWK', 'WWK:Stoneforge Mystic:stoneforge mystic').
card_rarity('stoneforge mystic'/'WWK', 'Rare').
card_artist('stoneforge mystic'/'WWK', 'Mike Bierek').
card_number('stoneforge mystic'/'WWK', '20').
card_multiverse_id('stoneforge mystic'/'WWK', '198383').

card_in_set('strength of the tajuru', 'WWK').
card_original_type('strength of the tajuru'/'WWK', 'Instant').
card_original_text('strength of the tajuru'/'WWK', 'Multikicker {1} (You may pay an additional {1} any number of times as you cast this spell.)\nChoose target creature, then choose another target creature for each time Strength of the Tajuru was kicked. Put X +1/+1 counters on each of them.').
card_first_print('strength of the tajuru', 'WWK').
card_image_name('strength of the tajuru'/'WWK', 'strength of the tajuru').
card_uid('strength of the tajuru'/'WWK', 'WWK:Strength of the Tajuru:strength of the tajuru').
card_rarity('strength of the tajuru'/'WWK', 'Rare').
card_artist('strength of the tajuru'/'WWK', 'Christopher Moeller').
card_number('strength of the tajuru'/'WWK', '113').
card_multiverse_id('strength of the tajuru'/'WWK', '197875').

card_in_set('summit apes', 'WWK').
card_original_type('summit apes'/'WWK', 'Creature — Ape').
card_original_text('summit apes'/'WWK', 'As long as you control a Mountain, Summit Apes can\'t be blocked except by two or more creatures.').
card_first_print('summit apes', 'WWK').
card_image_name('summit apes'/'WWK', 'summit apes').
card_uid('summit apes'/'WWK', 'WWK:Summit Apes:summit apes').
card_rarity('summit apes'/'WWK', 'Uncommon').
card_artist('summit apes'/'WWK', 'Véronique Meignaud').
card_number('summit apes'/'WWK', '114').
card_flavor_text('summit apes'/'WWK', '\"If you climb Skyfang Peak, avoid the pass. Generations of apes made that trail, and they don\'t tolerate trespassers.\"\n—Sachir, Akoum Expeditionary House').
card_multiverse_id('summit apes'/'WWK', '197864').

card_in_set('surrakar banisher', 'WWK').
card_original_type('surrakar banisher'/'WWK', 'Creature — Surrakar').
card_original_text('surrakar banisher'/'WWK', 'When Surrakar Banisher enters the battlefield, you may return target tapped creature to its owner\'s hand.').
card_first_print('surrakar banisher', 'WWK').
card_image_name('surrakar banisher'/'WWK', 'surrakar banisher').
card_uid('surrakar banisher'/'WWK', 'WWK:Surrakar Banisher:surrakar banisher').
card_rarity('surrakar banisher'/'WWK', 'Common').
card_artist('surrakar banisher'/'WWK', 'Matt Cavotta').
card_number('surrakar banisher'/'WWK', '39').
card_flavor_text('surrakar banisher'/'WWK', 'Surrakar grab things they don\'t want.').
card_multiverse_id('surrakar banisher'/'WWK', '198370').

card_in_set('talus paladin', 'WWK').
card_original_type('talus paladin'/'WWK', 'Creature — Human Knight Ally').
card_original_text('talus paladin'/'WWK', 'Whenever Talus Paladin or another Ally enters the battlefield under your control, you may have Allies you control gain lifelink until end of turn, and you may put a +1/+1 counter on Talus Paladin.').
card_first_print('talus paladin', 'WWK').
card_image_name('talus paladin'/'WWK', 'talus paladin').
card_uid('talus paladin'/'WWK', 'WWK:Talus Paladin:talus paladin').
card_rarity('talus paladin'/'WWK', 'Rare').
card_artist('talus paladin'/'WWK', 'Svetlin Velinov').
card_number('talus paladin'/'WWK', '21').
card_flavor_text('talus paladin'/'WWK', '\"Why fill pockets with gold when you can fill hearts with conviction?\"').
card_multiverse_id('talus paladin'/'WWK', '194672').

card_in_set('tectonic edge', 'WWK').
card_original_type('tectonic edge'/'WWK', 'Land').
card_original_text('tectonic edge'/'WWK', '{T}: Add {1} to your mana pool.\n{1}, {T}, Sacrifice Tectonic Edge: Destroy target nonbasic land. Activate this ability only if an opponent controls four or more lands.').
card_image_name('tectonic edge'/'WWK', 'tectonic edge').
card_uid('tectonic edge'/'WWK', 'WWK:Tectonic Edge:tectonic edge').
card_rarity('tectonic edge'/'WWK', 'Uncommon').
card_artist('tectonic edge'/'WWK', 'Vincent Proce').
card_number('tectonic edge'/'WWK', '145').
card_flavor_text('tectonic edge'/'WWK', '\"We move because the earth does.\"\n—Bruse Tarl, Goma Fada nomad').
card_multiverse_id('tectonic edge'/'WWK', '197855').

card_in_set('terastodon', 'WWK').
card_original_type('terastodon'/'WWK', 'Creature — Elephant').
card_original_text('terastodon'/'WWK', 'When Terastodon enters the battlefield, you may destroy up to three target noncreature permanents. For each permanent put into a graveyard this way, its controller puts a 3/3 green Elephant creature token onto the battlefield.').
card_image_name('terastodon'/'WWK', 'terastodon').
card_uid('terastodon'/'WWK', 'WWK:Terastodon:terastodon').
card_rarity('terastodon'/'WWK', 'Rare').
card_artist('terastodon'/'WWK', 'Lars Grant-West').
card_number('terastodon'/'WWK', '115').
card_multiverse_id('terastodon'/'WWK', '197137').

card_in_set('terra eternal', 'WWK').
card_original_type('terra eternal'/'WWK', 'Enchantment').
card_original_text('terra eternal'/'WWK', 'All lands are indestructible.').
card_first_print('terra eternal', 'WWK').
card_image_name('terra eternal'/'WWK', 'terra eternal').
card_uid('terra eternal'/'WWK', 'WWK:Terra Eternal:terra eternal').
card_rarity('terra eternal'/'WWK', 'Rare').
card_artist('terra eternal'/'WWK', 'Daniel Ljunggren').
card_number('terra eternal'/'WWK', '22').
card_flavor_text('terra eternal'/'WWK', '\"If this world could make a wish, it would be to survive the parasites who loot its treasures and threaten its life force.\"\n—Saidah, Joraga hunter').
card_multiverse_id('terra eternal'/'WWK', '197866').

card_in_set('thada adel, acquisitor', 'WWK').
card_original_type('thada adel, acquisitor'/'WWK', 'Legendary Creature — Merfolk Rogue').
card_original_text('thada adel, acquisitor'/'WWK', 'Islandwalk\nWhenever Thada Adel, Acquisitor deals combat damage to a player, search that player\'s library for an artifact card and exile it. Then that player shuffles his or her library. Until end of turn, you may play that card.').
card_first_print('thada adel, acquisitor', 'WWK').
card_image_name('thada adel, acquisitor'/'WWK', 'thada adel, acquisitor').
card_uid('thada adel, acquisitor'/'WWK', 'WWK:Thada Adel, Acquisitor:thada adel, acquisitor').
card_rarity('thada adel, acquisitor'/'WWK', 'Rare').
card_artist('thada adel, acquisitor'/'WWK', 'Andrew Robinson').
card_number('thada adel, acquisitor'/'WWK', '40').
card_multiverse_id('thada adel, acquisitor'/'WWK', '197973').

card_in_set('tideforce elemental', 'WWK').
card_original_type('tideforce elemental'/'WWK', 'Creature — Elemental').
card_original_text('tideforce elemental'/'WWK', '{U}, {T}: You may tap or untap another target creature.\nLandfall — Whenever a land enters the battlefield under your control, you may untap Tideforce Elemental.').
card_first_print('tideforce elemental', 'WWK').
card_image_name('tideforce elemental'/'WWK', 'tideforce elemental').
card_uid('tideforce elemental'/'WWK', 'WWK:Tideforce Elemental:tideforce elemental').
card_rarity('tideforce elemental'/'WWK', 'Uncommon').
card_artist('tideforce elemental'/'WWK', 'Donato Giancola').
card_number('tideforce elemental'/'WWK', '41').
card_flavor_text('tideforce elemental'/'WWK', 'Ebb and flow, high tide and low, quick as sand and just as slow.').
card_multiverse_id('tideforce elemental'/'WWK', '194012').

card_in_set('tomb hex', 'WWK').
card_original_type('tomb hex'/'WWK', 'Instant').
card_original_text('tomb hex'/'WWK', 'Target creature gets -2/-2 until end of turn.\nLandfall — If you had a land enter the battlefield under your control this turn, that creature gets -4/-4 until end of turn instead.').
card_first_print('tomb hex', 'WWK').
card_image_name('tomb hex'/'WWK', 'tomb hex').
card_uid('tomb hex'/'WWK', 'WWK:Tomb Hex:tomb hex').
card_rarity('tomb hex'/'WWK', 'Common').
card_artist('tomb hex'/'WWK', 'Izzy').
card_number('tomb hex'/'WWK', '69').
card_multiverse_id('tomb hex'/'WWK', '195307').

card_in_set('treasure hunt', 'WWK').
card_original_type('treasure hunt'/'WWK', 'Sorcery').
card_original_text('treasure hunt'/'WWK', 'Reveal cards from the top of your library until you reveal a nonland card, then put all cards revealed this way into your hand.').
card_image_name('treasure hunt'/'WWK', 'treasure hunt').
card_uid('treasure hunt'/'WWK', 'WWK:Treasure Hunt:treasure hunt').
card_rarity('treasure hunt'/'WWK', 'Common').
card_artist('treasure hunt'/'WWK', 'Daren Bader').
card_number('treasure hunt'/'WWK', '42').
card_flavor_text('treasure hunt'/'WWK', '\"The longer the journey, the more one learns.\"\n—Isanke, Halimar seastalker').
card_multiverse_id('treasure hunt'/'WWK', '197968').

card_in_set('tuktuk scrapper', 'WWK').
card_original_type('tuktuk scrapper'/'WWK', 'Creature — Goblin Artificer Ally').
card_original_text('tuktuk scrapper'/'WWK', 'Whenever Tuktuk Scrapper or another Ally enters the battlefield under your control, you may destroy target artifact. If that artifact is put into a graveyard this way, Tuktuk Scrapper deals damage to that artifact\'s controller equal to the number of Allies you control.').
card_first_print('tuktuk scrapper', 'WWK').
card_image_name('tuktuk scrapper'/'WWK', 'tuktuk scrapper').
card_uid('tuktuk scrapper'/'WWK', 'WWK:Tuktuk Scrapper:tuktuk scrapper').
card_rarity('tuktuk scrapper'/'WWK', 'Uncommon').
card_artist('tuktuk scrapper'/'WWK', 'Matt Cavotta').
card_number('tuktuk scrapper'/'WWK', '94').
card_multiverse_id('tuktuk scrapper'/'WWK', '201579').

card_in_set('twitch', 'WWK').
card_original_type('twitch'/'WWK', 'Instant').
card_original_text('twitch'/'WWK', 'You may tap or untap target artifact, creature, or land.\nDraw a card.').
card_image_name('twitch'/'WWK', 'twitch').
card_uid('twitch'/'WWK', 'WWK:Twitch:twitch').
card_rarity('twitch'/'WWK', 'Common').
card_artist('twitch'/'WWK', 'Scott Chou').
card_number('twitch'/'WWK', '43').
card_flavor_text('twitch'/'WWK', '\"The flow is diverted. Your path is clear. What are you waiting for—the cave to bid you welcome?\"\n—Maizah Shere, Tazeem lullmage').
card_multiverse_id('twitch'/'WWK', '198396').

card_in_set('urge to feed', 'WWK').
card_original_type('urge to feed'/'WWK', 'Instant').
card_original_text('urge to feed'/'WWK', 'Target creature gets -3/-3 until end of turn. You may tap any number of untapped Vampire creatures you control. If you do, put a +1/+1 counter on each of those Vampires.').
card_first_print('urge to feed', 'WWK').
card_image_name('urge to feed'/'WWK', 'urge to feed').
card_uid('urge to feed'/'WWK', 'WWK:Urge to Feed:urge to feed').
card_rarity('urge to feed'/'WWK', 'Uncommon').
card_artist('urge to feed'/'WWK', 'Johann Bodin').
card_number('urge to feed'/'WWK', '70').
card_multiverse_id('urge to feed'/'WWK', '198368').

card_in_set('vapor snare', 'WWK').
card_original_type('vapor snare'/'WWK', 'Enchantment — Aura').
card_original_text('vapor snare'/'WWK', 'Enchant creature\nYou control enchanted creature.\nAt the beginning of your upkeep, sacrifice Vapor Snare unless you return a land you control to its owner\'s hand.').
card_first_print('vapor snare', 'WWK').
card_image_name('vapor snare'/'WWK', 'vapor snare').
card_uid('vapor snare'/'WWK', 'WWK:Vapor Snare:vapor snare').
card_rarity('vapor snare'/'WWK', 'Uncommon').
card_artist('vapor snare'/'WWK', 'Trevor Claxton').
card_number('vapor snare'/'WWK', '44').
card_multiverse_id('vapor snare'/'WWK', '194003').

card_in_set('vastwood animist', 'WWK').
card_original_type('vastwood animist'/'WWK', 'Creature — Elf Shaman Ally').
card_original_text('vastwood animist'/'WWK', '{T}: Target land you control becomes an X/X Elemental creature until end of turn, where X is the number of Allies you control. It\'s still a land.').
card_first_print('vastwood animist', 'WWK').
card_image_name('vastwood animist'/'WWK', 'vastwood animist').
card_uid('vastwood animist'/'WWK', 'WWK:Vastwood Animist:vastwood animist').
card_rarity('vastwood animist'/'WWK', 'Uncommon').
card_artist('vastwood animist'/'WWK', 'Raymond Swanland').
card_number('vastwood animist'/'WWK', '116').
card_flavor_text('vastwood animist'/'WWK', 'He calls on the forests for counsel and summons the earth to his cause.').
card_multiverse_id('vastwood animist'/'WWK', '201566').

card_in_set('vastwood zendikon', 'WWK').
card_original_type('vastwood zendikon'/'WWK', 'Enchantment — Aura').
card_original_text('vastwood zendikon'/'WWK', 'Enchant land\nEnchanted land is a 6/4 green Elemental creature. It\'s still a land.\nWhen enchanted land is put into a graveyard, return that card to its owner\'s hand.').
card_first_print('vastwood zendikon', 'WWK').
card_image_name('vastwood zendikon'/'WWK', 'vastwood zendikon').
card_uid('vastwood zendikon'/'WWK', 'WWK:Vastwood Zendikon:vastwood zendikon').
card_rarity('vastwood zendikon'/'WWK', 'Common').
card_artist('vastwood zendikon'/'WWK', 'Rob Alexander').
card_number('vastwood zendikon'/'WWK', '117').
card_multiverse_id('vastwood zendikon'/'WWK', '197870').

card_in_set('veteran\'s reflexes', 'WWK').
card_original_type('veteran\'s reflexes'/'WWK', 'Instant').
card_original_text('veteran\'s reflexes'/'WWK', 'Target creature gets +1/+1 until end of turn. Untap that creature.').
card_first_print('veteran\'s reflexes', 'WWK').
card_image_name('veteran\'s reflexes'/'WWK', 'veteran\'s reflexes').
card_uid('veteran\'s reflexes'/'WWK', 'WWK:Veteran\'s Reflexes:veteran\'s reflexes').
card_rarity('veteran\'s reflexes'/'WWK', 'Common').
card_artist('veteran\'s reflexes'/'WWK', 'Scott Chou').
card_number('veteran\'s reflexes'/'WWK', '23').
card_flavor_text('veteran\'s reflexes'/'WWK', '\"Assume everything wants to kill you. At the very least, assume everything wants what\'s in your pack.\"').
card_multiverse_id('veteran\'s reflexes'/'WWK', '198384').

card_in_set('voyager drake', 'WWK').
card_original_type('voyager drake'/'WWK', 'Creature — Drake').
card_original_text('voyager drake'/'WWK', 'Multikicker {U} (You may pay an additional {U} any number of times as you cast this spell.)\nFlying\nWhen Voyager Drake enters the battlefield, up to X target creatures gain flying until end of turn, where X is the number of times Voyager Drake was kicked.').
card_first_print('voyager drake', 'WWK').
card_image_name('voyager drake'/'WWK', 'voyager drake').
card_uid('voyager drake'/'WWK', 'WWK:Voyager Drake:voyager drake').
card_rarity('voyager drake'/'WWK', 'Uncommon').
card_artist('voyager drake'/'WWK', 'Kieran Yanner').
card_number('voyager drake'/'WWK', '45').
card_multiverse_id('voyager drake'/'WWK', '189181').

card_in_set('walking atlas', 'WWK').
card_original_type('walking atlas'/'WWK', 'Artifact Creature — Construct').
card_original_text('walking atlas'/'WWK', '{T}: You may put a land card from your hand onto the battlefield.').
card_first_print('walking atlas', 'WWK').
card_image_name('walking atlas'/'WWK', 'walking atlas').
card_uid('walking atlas'/'WWK', 'WWK:Walking Atlas:walking atlas').
card_rarity('walking atlas'/'WWK', 'Common').
card_artist('walking atlas'/'WWK', 'Rob Alexander').
card_number('walking atlas'/'WWK', '131').
card_flavor_text('walking atlas'/'WWK', 'Crafted by lullmages and bound to the land, it alters itself to match the tumultuous terrain.').
card_multiverse_id('walking atlas'/'WWK', '198375').

card_in_set('wind zendikon', 'WWK').
card_original_type('wind zendikon'/'WWK', 'Enchantment — Aura').
card_original_text('wind zendikon'/'WWK', 'Enchant land\nEnchanted land is a 2/2 blue Elemental creature with flying. It\'s still a land.\nWhen enchanted land is put into a graveyard, return that card to its owner\'s hand.').
card_first_print('wind zendikon', 'WWK').
card_image_name('wind zendikon'/'WWK', 'wind zendikon').
card_uid('wind zendikon'/'WWK', 'WWK:Wind Zendikon:wind zendikon').
card_rarity('wind zendikon'/'WWK', 'Common').
card_artist('wind zendikon'/'WWK', 'Vincent Proce').
card_number('wind zendikon'/'WWK', '46').
card_multiverse_id('wind zendikon'/'WWK', '197865').

card_in_set('wolfbriar elemental', 'WWK').
card_original_type('wolfbriar elemental'/'WWK', 'Creature — Elemental').
card_original_text('wolfbriar elemental'/'WWK', 'Multikicker {G} (You may pay an additional {G} any number of times as you cast this spell.)\nWhen Wolfbriar Elemental enters the battlefield, put a 2/2 green Wolf creature token onto the battlefield for each time it was kicked.').
card_first_print('wolfbriar elemental', 'WWK').
card_image_name('wolfbriar elemental'/'WWK', 'wolfbriar elemental').
card_uid('wolfbriar elemental'/'WWK', 'WWK:Wolfbriar Elemental:wolfbriar elemental').
card_rarity('wolfbriar elemental'/'WWK', 'Rare').
card_artist('wolfbriar elemental'/'WWK', 'Chippy').
card_number('wolfbriar elemental'/'WWK', '118').
card_multiverse_id('wolfbriar elemental'/'WWK', '189162').

card_in_set('wrexial, the risen deep', 'WWK').
card_original_type('wrexial, the risen deep'/'WWK', 'Legendary Creature — Kraken').
card_original_text('wrexial, the risen deep'/'WWK', 'Islandwalk, swampwalk\nWhenever Wrexial, the Risen Deep deals combat damage to a player, you may cast target instant or sorcery card from that player\'s graveyard without paying its mana cost. If that card would be put into a graveyard this turn, exile it instead.').
card_first_print('wrexial, the risen deep', 'WWK').
card_image_name('wrexial, the risen deep'/'WWK', 'wrexial, the risen deep').
card_uid('wrexial, the risen deep'/'WWK', 'WWK:Wrexial, the Risen Deep:wrexial, the risen deep').
card_rarity('wrexial, the risen deep'/'WWK', 'Mythic Rare').
card_artist('wrexial, the risen deep'/'WWK', 'Eric Deschamps').
card_number('wrexial, the risen deep'/'WWK', '120').
card_multiverse_id('wrexial, the risen deep'/'WWK', '197840').
